---
title: "KMail 1,000,000,000th Second Bug Looms"
date:    2001-09-02
authors:
  - "Dre"
slug:    kmail-1000000000th-second-bug-looms
comments:
  - subject: "Low end machines?"
    date: 2001-09-02
    body: "What about those who don't want/can't run KDE 2.x?\nOlder computers for example. KDE 1.x runs very fast on my Pentium 166 with 48 MB RAM, but KDE 2.x is slow as /sbin/f*sck.ext2\nWill there be a fixed version of KMail 1.x?"
    author: "Stof"
  - subject: "Re: Low end machines?"
    date: 2001-09-03
    body: "1.0.29.1"
    author: "reihal"
  - subject: "Re: Low end machines?"
    date: 2001-09-03
    body: "Erm... Do what I do on a machine with identical specs. Use the newest KMail, but use a different Window Manager. I recommend Blackbox which flies on a P166."
    author: "Drood"
  - subject: "Re: Low end machines?"
    date: 2001-09-09
    body: "This is unbelievably stupid...users of RHL 6.2 cannot upgrade to Kmail 2.2\nwithout a vast swathe of other incompatibilites intruding. Embedding 1.3 in\n2.2 without providing an upgrade path was not a good idea. \n///Peter\n-- \n# rpm -Uiv kmail-2.1.1-1.i386.rpm\nerror: failed dependencies:\n        libDCOP.so.1 is needed by kmail-2.1.1-1\n        libkab.so.3 is needed by kmail-2.1.1-1\n        libkdecore.so.3 is needed by kmail-2.1.1-1\n        libkdefakes.so.3 is needed by kmail-2.1.1-1\n        libkdesu.so.1 is needed by kmail-2.1.1-1\n        libkdeui.so.3 is needed by kmail-2.1.1-1\n        libkfile.so.3 is needed by kmail-2.1.1-1\n        libkhtml.so.3 is needed by kmail-2.1.1-1\n        libkio.so.3 is needed by kmail-2.1.1-1\n        libkjava.so.1 is needed by kmail-2.1.1-1\n        libkparts.so.1 is needed by kmail-2.1.1-1\n        libkspell.so.3 is needed by kmail-2.1.1-1\n        libkssl.so.2 is needed by kmail-2.1.1-1\n        libksycoca.so.3 is needed by kmail-2.1.1-1\n        libmimelib.so.2 is needed by kmail-2.1.1-1\n        libmng.so.0 is needed by kmail-2.1.1-1 #"
    author: "Peter Flynn"
  - subject: "Re: Low end machines?"
    date: 2001-09-10
    body: "Well, you also have to install the rest of the kde package, like kdelibs and kdecore.  And why the hell are you still using redhat 6.2?"
    author: "Alex Valys"
  - subject: "~/Mail folder location"
    date: 2001-09-03
    body: "I want ~/Mail to be ~/.kde/Mail to see my home folder rather clean. Is it possible?"
    author: "Asif Ali Rizwaan"
  - subject: "Re: ~/Mail folder location"
    date: 2001-09-03
    body: "Sure, put this in your kmailrc:\n[General]\nfolders=/path/to/your/home/.kde/Mail"
    author: "anonymous"
  - subject: "Kmail SMTP error help!"
    date: 2001-09-03
    body: "I am trying smtp configuration but it's not working today:\n\nThe error:\n-----\nSending failed:\na SMTP error occured.\nCommand: MAIL\nResponse: 521 Mail not accepted from this domain\n\nThe message will stay in the 'outbox' folder until you either fix the problem (e.g., a broken address) or remove the message from the 'outbox' folder.\n\nNote: other messages will also be blocked by this message, as long as it is in the 'outbox' folder\n\nThe following transport protocol was used:\nsmtp://smtp.mail.yahoo.com:25\n------\n\nPlease help."
    author: "Asif Ali Rizwaan"
  - subject: "Re: Kmail SMTP error help!"
    date: 2001-09-03
    body: "You're not allowed to send mail with Yahoo's STMP server (except, probably, to Yahoo users).  If you could, it'd be an open relay, which is bad."
    author: "KDE User"
  - subject: "Re: Kmail SMTP error help!"
    date: 2001-09-03
    body: "Check your KMAIL configuration. \nOpen KMAIL. Click on Settings- Network.On the \"Sending Mail\" tab, choose the \"SendMail\" option to bring up the Location:/usr/sbin/sendmail.Click \nClick on Apply- OK and you're done.\nI had the same problem and the above solved it.\n\nCheers."
    author: "Ted Olaye"
  - subject: "Re: Kmail SMTP error help!"
    date: 2004-08-22
    body: "THANK'S   Ted Olaye"
    author: "farid"
  - subject: "Re: Kmail SMTP error help!"
    date: 2001-09-03
    body: "For security reasons, Yahoo! requieres you to check your mail before sending. In this way, it can validate your username and password.\n\nRead this:\n\nYahoo! Mail Help - Errors Sending Mail (SMTP)\nhttp://help.yahoo.com/help/us/mail/pop/pop-11.html\n\nAnother solution is to use a SMTP server from four ISP."
    author: "Daniel Mario Vega"
  - subject: "Re: Kmail SMTP error help!"
    date: 2002-12-12
    body: "We are trying to use or scanner for e-mail and faxs,also we are trying to use incrediMail and can't send our e-mail.We some how are using Outlook Express for our yahoo mail.If you can help us we'd appreciate it.Terry & Cheryl Cunningham"
    author: "Cheryl Cunningham"
  - subject: "Re: Kmail SMTP error help!"
    date: 2003-01-02
    body: "needhelp rejistering my e-MAIL ADDRESS."
    author: "eddie tumpach"
  - subject: "Just start the service idnetd"
    date: 2001-09-03
    body: "I disabled the service \"identd\" which caused the above problem, restart identd daemon to get that working and to send messages through yahoo's smtp server :)\n\ncommand:\n\n# service identd start\n\nThanks for the tip about sendmail configuration anyway."
    author: "Asif Ali Rizwaan"
  - subject: "Yahoo requires pop auth"
    date: 2001-09-03
    body: "You can't use Yahoo! smtp servers unless you have been pop-authenticated, thus limiting it's use to Yahoo! members.  \n-Adam"
    author: "Adam Black"
  - subject: "Re: Yahoo requires pop auth"
    date: 2001-11-10
    body: "I CANT DOWNLOAD MESSEGES TO OUTLOOK EXPRESS AND IT SAYS I HAVE TO  POP-AUTHENTICATION CAN YU HELP ME PLEASE?\nTHANKS IN ADVANCED"
    author: "MAFER"
  - subject: "Re: Yahoo requires pop auth"
    date: 2003-01-30
    body: "Cannot send and receive my mails using Eudora /outlook\nexpress.It has been operative until 27/1/03"
    author: "Dr. Sam UdoAkpan"
  - subject: "Re: Yahoo requires pop auth"
    date: 2003-01-30
    body: "My Eudora and Outlook Express programmes cannot \nsend and receive my mails.\nReasons:Connection reset by remote site (10054)\n530 authentification required"
    author: "Dr. Sam UdoAkpan"
  - subject: "Re: Yahoo requires pop auth"
    date: 2005-05-27
    body: "I am having troulbe sending email and keep getting this message.\n\n\nTask 'pop.swbell.yahoo.com - Sending' reported error (0x800CCC78) : 'Unable to send the message. Please verify the e-mail address in your account properties.  The server responded: 530 authentication required - for help go to http://help.yahoo.com/help/us/sbc/dsl/mail/pop/pop-11.html'\n\nWhat do I need to do?\n\nThanks\n\n"
    author: "Bob Franklin"
  - subject: "Re: Yahoo requires pop auth"
    date: 2005-07-21
    body: "when i send an  e mail out and it is scaned through Mcafee viruscsan i get this message protocol error  and it tells me to go here to this site"
    author: "natashaweary"
  - subject: "Re: Yahoo requires pop auth"
    date: 2008-07-27
    body: " 530 authentication required - for help go to http://help.yahoo.com/help/us/sbc/dsl/mail/pop/pop-11.html' Ican`t send mail I keepgetting this now what???"
    author: "Lois Ann Hart"
  - subject: "Re: Yahoo requires pop auth"
    date: 2008-10-01
    body: "I can not send mail with Incredi Mail, what do i need to do"
    author: "Paul Hoffman"
  - subject: "Re: Yahoo requires pop auth"
    date: 2009-01-02
    body: "at present i am unable to send any mail through outlook,while sending mail thorugh outlook i am getting above error,please send solution."
    author: "selvakumar"
  - subject: "Re: Yahoo requires pop auth"
    date: 2005-07-29
    body: "What do I need to do to correct this \n\n\nThe message could not be sent because the server rejected the sender's e-mail address. The sender's e-mail address was 'gbob99@sbcglobal.net'. Subject 'Re: Kari', Account: 'pop.sbcglobal.yahoo.com', Server: 'smtp.sbcglobal.yahoo.com', Protocol: SMTP, Server Response: '530 authentication required - for help go to http://help.yahoo.com/help/us/sbc/dsl/mail/pop/pop-11.html', Port: 25, Secure(SSL): No, Server Error: 530, Error Number: 0x800CCC78"
    author: "bobby gilbert"
  - subject: "Re: Yahoo requires pop auth"
    date: 2006-07-20
    body: "The message could not be sent because the server rejected the sender's e-mail address. The sender's e-mail address was 'lady4332@sbcglobal.net'. Subject 'Fw: Are You a Keeper?', Account: 'POP.sbcglobal.yahoo.com', Server: 'SMTP.sbcglobal.yahoo.com', Protocol: SMTP, Server Response: '530 authentication required - for help go to http://help.yahoo.com/help/us/sbc/dsl/mail/pop/pop-11.html', Port: 25, Secure(SSL): No, Server Error: 530, Error Number: 0x800CCC78\n\n I get this message everytime i try to send a email from outlook express. i can recieve email just cant not send email thur outlook express."
    author: "tammy jones"
  - subject: "Re: Yahoo requires pop auth"
    date: 2006-08-22
    body: "Please help I keep getting the same message as above.\nWhat should I do?"
    author: "Jean"
  - subject: "Re: Yahoo requires pop auth"
    date: 2008-02-28
    body: "I have the same problem with my email as described above. The error message I receive is the same. I can receive email but cannot send email thru outlook express. Please reply."
    author: "John Meletio"
  - subject: "Re: Yahoo requires pop auth"
    date: 2006-09-03
    body: "Here's the solution that worked for SBC DSL using Yahoo, a variation will most likely work for other ISP's as well:\n\n1 GO TO EUDORA AND OPEN THE \"TOOLS\" LIST.\n2 GO DOWN AND CLICK \"OPTIONS\"\n3 CLICK \"GETTING STARTED\" IN LEFT PANEL (IT USUALLY DEFAULTS HERE ANYWAY)\n4 LOOK AT THE BOTTOM PANEL (SMTP SERVER (OUTGOING))\n5 CHANGE YOUR \"SMTP.SBCGLOBAL.YAHOO.COM\" (OR WHATEVER YOU HAVE) TO: MAIL.SBCGLOBAL.NET  (DON'T FORGET TO USE .NET, NOT .COM\n\nThat should fix the problem.  It was driving me nuts too and everyone kept telling me to make sure I had \"allow authentication\" checked.  OH MAKE SURE YOU DO!!!!  Thats necessary too!  Theres also an \"allow authentication\" under the \"sending mail\" icon in the left panel.  Make sure it's checked too.  Good luck!\n"
    author: "Jim"
  - subject: "Re: Yahoo requires pop auth"
    date: 2006-10-02
    body: "Problem occurs when using outlook express. Default email provider is Incredimail. My wife appreciates the whistles and bells Incredimail provides. This has been a real pain and thanks to you I can finally put this one to bed. Don't mean to get long winded, just one more thing I'll share with you. While using Microsoft Outlook and Incredimail I use pop.sbcglobal.com incoming and smtl.sbcglobal.yahoo.com outgoing. Many thanks Jim."
    author: "john vetre"
  - subject: "Re: Yahoo requires pop auth"
    date: 2006-10-07
    body: "Thank goodness for this finding!! I'm still using Eudora 4.3 and was thinking of having to upgrade to 5.1 minimum. After calling SBC-DSL support, which dind't tell me anything more than what's already in their helpfiles, I googled the authentication feature hoping to find a Eudora version requirement. Using MAIL.SBCGLOBAL.NET solved my problem instantly!"
    author: "Alex"
  - subject: "Re: Yahoo requires pop auth"
    date: 2007-01-13
    body: "Thanks so much - this worked!!!! I have two accounts coming into my Outlook, one from my online school, and the other this SBC one. Since we switched to our new provider I had to reconfigure outlook, the directions in the manual didn't work. I could only post to classroom stuff, but no email. It's been making me crazy."
    author: "Cindy"
  - subject: "Re: Yahoo requires pop auth"
    date: 2007-04-23
    body: "THANKS SOOO MUCH... I have been working on this for over an hour.I got the error message, which took me to this site, and everything was fixed instantly.. what a great tip...\nI really appreciate your sending that response to the poster..."
    author: "Darlene"
  - subject: "Re: Yahoo requires pop auth"
    date: 2007-11-10
    body: "This worked!!! I have spent hours upon hours trying to figure this out.  You saved me from going postal (lol).  You are a genius."
    author: "Emma"
  - subject: "Re: Yahoo requires pop auth"
    date: 2007-11-10
    body: "This worked!!! I have spent hours upon hours trying to figure this out.  You saved me from going postal (lol).  You are a genius."
    author: "Emma"
  - subject: "Re: Yahoo requires pop auth"
    date: 2007-11-10
    body: "This worked!!! I have spent hours upon hours trying to figure this out.  You saved me from going postal (lol).  You are a genius."
    author: "Emma"
  - subject: "Re: Yahoo requires pop auth"
    date: 2008-05-05
    body: "Thanks for taking the time to post. My Eudora was working perfectly until several days ago and then it would not send. I called AT&T and they gave me useless information and then transferred me to their \"pay\" for support department. The bottom line, is when I changed SMPT.SBCGLOBAL.NET TO MAIL.SBCGLOBAL.NET, as per you directions, all is well.. Thanks, Thanks, Thanks\nazbud17@aol.com"
    author: "Bob Zupp"
  - subject: "Re: Yahoo requires pop auth"
    date: 2008-07-10
    body: "I tried what you said but am still not allowed to send messages.\nWould appreciate help"
    author: "Sallie"
  - subject: "Re: Yahoo requires pop auth"
    date: 2008-05-06
    body: "Thank You for posting\n"
    author: "cm"
  - subject: "Re: Yahoo requires pop auth"
    date: 2008-08-26
    body: "I want to write to gladly say these instructions are correct. I used these settigs and this will solve the problem stated."
    author: "Brian Rettig"
  - subject: "Re: Yahoo requires pop auth"
    date: 2006-02-19
    body: "I DO NOT UNDERSTAND WHY I AM UNABLE TO SEND AN ADDRESS CHANGE TO MY STATE AGENCY THAT CONTROLS MY LICENSE TO PRACTICE. I WOULD THINK IT WOULD BE A SIMPLE THING BUT I HAVE LEARNED IT IS NOT. WHAT DO I NEED TO DO? THANKS  "
    author: "vmallen"
  - subject: "Re: Yahoo requires pop auth"
    date: 2006-03-31
    body: "please help me fix my email problem.\n                  thank you marilyn carrion"
    author: "marilyn carrion"
  - subject: "Re: Yahoo requires pop auth"
    date: 2006-07-14
    body: "frist off...Yahoo Quit sendign POP3 or POP,along while ago...It had to do with ads not being viewed just to be short and quaint!! So the discontinued POP sending... take care,RICK"
    author: "Rick"
  - subject: "Re: Yahoo requires pop auth"
    date: 2006-09-23
    body: "Help---this is a new computer--just connected and unable to send e-mail"
    author: "Ginny Osmun"
  - subject: "Re: Yahoo requires pop auth"
    date: 2006-12-05
    body: "I'm having trouble also.  Did you get the problem resolved and if so what did you do?"
    author: "Jill Janzen"
  - subject: "Re: Yahoo requires pop auth"
    date: 2007-01-02
    body: "How do I Auth?"
    author: "John Rio"
  - subject: "Re: Yahoo requires pop auth"
    date: 2007-01-20
    body: "I can't send and receive any messages on incredmail"
    author: "shawna"
  - subject: "Re: Yahoo requires pop auth"
    date: 2007-02-07
    body: "OMG!!! it worked! i changed smtp.sbcglobal.yahoo.com to mail.sbcglobal.net and it worked! i can send emails again using my MS Outlook 2002!!!!\n\nTHANKS!!!!!"
    author: "Yola"
  - subject: "Re: Yahoo requires pop auth"
    date: 2007-04-14
    body: "530 authentication required - for help go to http://help.yahoo.com/help/us/sbc/dsl/mail/pop/pop-11.html'"
    author: "mary"
  - subject: "Re: Yahoo requires pop auth"
    date: 2007-05-30
    body: "Wow that's such an informative post - thanks for your input!!\n\nAsswipe."
    author: "Will"
  - subject: "Re: Yahoo requires pop auth"
    date: 2007-06-11
    body: "I can not send or reply to my E-mail!  What is wrong, or what am I doing wrong.\n\nThank you,\n Roz"
    author: "Rosalind Kennedy"
  - subject: "Re: Yahoo requires pop auth"
    date: 2007-06-11
    body: "I can not send or reply to my E-mail!  What is wrong, or what am I doing wrong.\n\nThank you,\n Roz"
    author: "Rosalind Kennedy"
  - subject: "Re: Yahoo requires pop auth"
    date: 2007-07-12
    body: "icannot send emails.  comes up as an error."
    author: "carol e smith"
  - subject: "Re: Yahoo requires pop auth"
    date: 2008-02-12
    body: "I am having the same problem.  What did you do?\n\nThanks"
    author: "EJ"
  - subject: "Re: Yahoo requires pop auth"
    date: 2008-03-10
    body: "i had a hard time receiving email too, by following their instructions.\nBut it turns out that told us to check required SSL in the advanced tab for port 995 to receive, 465 to send. \n\nall have to be done is use their outgoing mail server: smtp.att.yahoo.com correspond to your account with them. and use port 465 without the check for outgoing port.  and it works fine for me.\n"
    author: "Ricketman"
  - subject: "New AT&T-Yahoo DSL Impediment to Sending E-Mail "
    date: 2008-03-27
    body: "--------------------------------------------------------------------------------\n\nI have the current version of Eudora Pro, have been unable to restore my paid mode status (once advised that Eudora no longer provides TS nor accepts paid users), have Windows XPSP2, have used Eudora Pro for 12 years and will fight before I use crap webmail or crap Outlook, and I have AT&T-Yahoo DSL service with webhosting and forwarding by Yahoo (probably with premium service). I recently encountered increasingly more frequent but intermittant error messages with outgoing E-mails that inevitably require 2-4 tries to successfully leave my computer; your error messages give no reason. I have received differing advice from AT&T TS staff who have had me change my incoming and outgoing server info in Eudora from SBCGlobal to ATT but otherwise have made no substantial change. I've been advised that there are new servers and changes but that \"AT&T does not support Eudora\" as if Eudora were the problem. My suspicion is that AT&T unlawfully obstructs my Eudora E-mailing and if the problem isn't somehow fixed I want to confront AT&T in Court and/or before the PUC."
    author: "Robert Burns"
  - subject: "Re: Yahoo requires pop auth"
    date: 2008-09-28
    body: "I have been working for two weeks trying to get my E-Mail to work, I have changed the numbers that you sent me, I can receive but it will not send I get a 500 error I am useing Incredemail can you help me I really need it. Thanks"
    author: "Hubert Young"
  - subject: "Re: Yahoo requires pop auth"
    date: 2008-10-13
    body: "I need help sending out my e-mail."
    author: "james bilderback"
  - subject: "Re: Yahoo requires pop auth"
    date: 2009-01-03
    body: "I had this problem after updating for SSL security with AT&T Yahoo.  After a lot of poking around, I finally figured out that the \"automatic\" updater from AT&T had created a new account which contained all the new information - the pop.att.yahoo.com info.  The old account, which had the sbcglobal.yahoo. information still remained.  For some reason, all the outgoing mail was being sent under this account.  Finally, I tried deleting the old account and now I can send email again.  \n\nWhat I did was go into TOOLS from the Outlook Express menu.  Go down to ACCOUNTS.  It is there that I saw \"Fiona\" (the new account AT&T setup using their automatic Outlook updater) and one that said pop.sbcglobal.yahoo.com.   This second one is what I deleted.  (I figured this out by watching VERY closely the messges that flashed by after I hit the \"send/receive\" button)\n\nAs soon as I did this, I was able to send messages again.  I hope this works for you.  \n\n"
    author: "Fiona"
  - subject: "Re: Yahoo requires pop auth"
    date: 2005-08-17
    body: "How do I remedy this problem?  I can't send out email from Outlook Express since I changed over to sbc hookup. The Outlook is incredibly faster than the sbc yahoo mail. I have dial up."
    author: "Sue"
  - subject: "Re: Yahoo requires pop auth"
    date: 2005-10-19
    body: "Toggle the \"My server requires authentification\" when setting up your account (in the server tab). The same happened to me following installation of PALM OS, do not ask me why..."
    author: "jc"
  - subject: "Re: Yahoo requires pop auth"
    date: 2006-02-27
    body: "Thanks for your advice.  I have spent hours trying to figure this out and your simple solution worked!"
    author: "Ed Hanson"
  - subject: "Re: Yahoo requires pop auth"
    date: 2006-06-30
    body: "I am using Olypia Comedia Master program and although I have signed up to process e-mail, all I get is the message that indicates that pop 530authentication  is required.  What is this and how do I get past this halting problem.  I need to send out pictures and are unable to get past this.\nHelp please.\n"
    author: "James H. McLaughlin"
  - subject: "Re: Yahoo requires pop auth"
    date: 2006-08-02
    body: "Yep - that was the right answer for me too. Thanks a bunch - simple."
    author: "ak"
  - subject: "Re: Yahoo requires pop auth"
    date: 2006-07-15
    body: "i am trying to get incredimail and they say go to this site help"
    author: "Kathi Bevan"
  - subject: "Re: Yahoo requires pop auth"
    date: 2006-08-02
    body: "You need to make sure it sends with secure authentication in outlook or outlook express.  Don't log in with secure authentication but send.  It does work give it a try."
    author: "Andrew Kopp"
  - subject: "Re: Yahoo requires pop auth"
    date: 2006-11-19
    body: "Sorry, i was looking for how to fix the problem, figured it out myself.  I dont know if somebody already answered the question lol skipped to the bottom, but anyways, on outlook express, go to tools > accounts > properties of the yahoo account needing fixing > servers tab > and all you have to do is check the box that says \"my server requires authentication\".  Then just send all messages normally, all works.  =D nice to help for once lmfao"
    author: "Zed"
  - subject: "Re: Yahoo requires pop auth"
    date: 2007-03-29
    body: "add me please"
    author: "oly+nana"
  - subject: "Re: Yahoo requires pop auth"
    date: 2007-03-29
    body: "add me please"
    author: "oly+nana"
  - subject: "Re: Yahoo requires pop auth"
    date: 2003-03-08
    body: "When I got tarifa plana 24hr it said I would get pop, incredimail, ect,ect. I cant seem to use either, what am I doing wrong."
    author: "Angela Lant"
  - subject: "Re: Yahoo requires pop auth"
    date: 2006-08-02
    body: "Please authenticate for my Incredimail Xe service.  Thank you."
    author: "Jennifer Nelsen"
  - subject: "Re: Yahoo requires pop auth"
    date: 2007-04-20
    body: "I have the same setup on my outlook on my desktop as my laptop.  The desktop responds but my laptop does not.\n\nWhat do you recommend?\n"
    author: "Ken Taylor"
  - subject: "Re: Yahoo requires pop auth"
    date: 2007-04-20
    body: "I have the same setup on my outlook on my desktop as my laptop.  The desktop responds but my laptop does not.\n\nWhat do you recommend?\n"
    author: "Ken Taylor"
  - subject: "Re: Yahoo requires pop auth"
    date: 2001-11-12
    body: "I can't figure out how to get POP authenticated by Yahoo either."
    author: "Mona Montgomery"
  - subject: "Re: Yahoo requires pop auth"
    date: 2001-11-19
    body: "hi dear .\nplease notify me what i should do to be yahoo auth.to be able to use stmp servers.\nthanx"
    author: "jad"
  - subject: "Re: Yahoo requires pop auth"
    date: 2002-04-19
    body: "hi dear .\nplease notify me what i should do to be yahoo auth.to be able to use stmp servers.\nthanx"
    author: "mahdieh"
  - subject: "Re: Yahoo requires pop auth"
    date: 2002-04-19
    body: "hi dear .\nplease notify me what i should do to be yahoo auth.to be able to use stmp servers.\nthanx"
    author: "mahdieh"
  - subject: "Re: Yahoo requires pop auth"
    date: 2005-10-19
    body: "no way"
    author: "my"
  - subject: "Re: Yahoo requires pop auth"
    date: 2003-11-23
    body: "i want to use incredimail for my emails but cannot due to no pop authentication. please help."
    author: "kaz"
  - subject: "Re: Yahoo requires pop auth"
    date: 2002-03-21
    body: "How do you obtain authentication for pop ?   K"
    author: "Ken Harris"
  - subject: "Re: Yahoo requires pop auth"
    date: 2002-04-07
    body: "The message could not be sent because the server rejected the sender's e-mail address. The sender's e-mail address was 'dmalette2002@yahoo.com'. Subject 'Rendezvous', Account: 'pop.mail.yahoo.com', Server: 'smtp.mail.yahoo.com', Protocol: SMTP, Server Response: '530 authentication required - for help go to http://help.yahoo.com/help/us/mail/pop/pop-11.html', Port: 25, Secure(SSL): No, Server Error: 530, Error Number: 0x800CCC78"
    author: "Dorothy Malette"
  - subject: "Re: Yahoo requires pop auth"
    date: 2002-05-18
    body: "I am unable to sent my mails because of the error no. 530 authentication is required.  Please help or provide authentication.\n\nregards  "
    author: "rksoni"
  - subject: "Re: Yahoo requires pop auth"
    date: 2002-11-14
    body: "there is an error 530 authentication recquired when sending mail"
    author: "ramil vital"
  - subject: "Re: Yahoo requires pop auth"
    date: 2004-03-15
    body: "I, have the same problem."
    author: "Kenneth Rose"
  - subject: "Re: Yahoo requires pop auth"
    date: 2006-07-14
    body: "Far as I know,since about 2000,Yahoo stopped sending forwarded mail to third party mail habndlers,i.e. Eudora,Opera,etc. no POP3 as far as since then,,now if anyone knows a workaround,please let me know,it wouod be great to have that information for others too... BBBYE< be well  RICK"
    author: "Rick"
  - subject: "Re: Yahoo requires pop auth"
    date: 2007-11-25
    body: "I have Installed windows EP and I can not send any email.It says error server 530 error #ox800ccc78.dont know what to do next.please help"
    author: "mark"
  - subject: "Re: Yahoo requires pop auth"
    date: 2002-12-15
    body: "i don't know wat to do"
    author: "Alfonso D'Agata"
  - subject: "Re: Yahoo requires pop auth"
    date: 2004-01-11
    body: "can't seem to send emails from incredimail and am not sure why."
    author: "Dial Nielson"
  - subject: "Re: Yahoo requires pop auth"
    date: 2003-03-20
    body: "   I HAVE SIGNED ON WITH INCREDIMAIL, BUT IT KEEPS SAYING WHEN I SIGN IN NAME OR PASS WORD FAILED, WHAT SHOULD I DO. OR DOING WRONG."
    author: "LINDA"
  - subject: "Re: Yahoo requires pop auth"
    date: 2007-02-16
    body: "I have been trying to receive e-mails an as well as send.  Nothing comeup on my side or the party I sent them to.  I too am haveing problems with little notes appearing on screen.  Before I decide to just give up maybe you can tell me why it is not accepting my server or password.  Yahoo said that the attempt to sending something to incredimail faild permanently. Why??????"
    author: "Lena "
  - subject: "Re: Yahoo requires pop auth"
    date: 2003-07-06
    body: "please help\n"
    author: "kewal"
  - subject: "Re: Yahoo requires pop auth"
    date: 2003-07-11
    body: "I don't know what I'm supposed to do or what this means.\n\nPamela."
    author: "Pamela Cipriano"
  - subject: "Re: Yahoo requires pop auth"
    date: 2003-07-16
    body: "I have had incredimail with high speed cable and all worked fine. Now I have gone with DSL and nothing works. Mail will not go or recive.  have done about every thing that is passable as far as web pages explantions.Come to think of it how will you respond???"
    author: "Jim"
  - subject: "Re: Yahoo requires pop auth"
    date: 2003-08-15
    body: "I've got the same problem.  How did you correct the 530 authentication requirement?"
    author: "H.K."
  - subject: "Re: Yahoo requires pop auth"
    date: 2004-02-25
    body: "Please help me with pop auth"
    author: "Jan Ryan"
  - subject: "Re: Yahoo requires pop auth"
    date: 2004-04-06
    body: "finally I resolved this problem, if anybody need helps... I get out in this way ..\n\nconfigure mail ----- net -----\nsmtp \nName : John DOe\nhost: smtp.blah blah..( your provider smtp for exaple yahoo italy smtp.mail.yahoo.it)\nautentication required chenk yes( ..and then if your mail is \nJohndoe@yahoo.it) \nlogin John\nPassword ( your mail password)\nno cript message \nauthentication plain\n\npop\nName John Doe\nLogin John\nPassword ( mail password)\nhost pop.mail.yahoo.it\nextras\ncriptography check no\nauthentication  text in plain\n\nin this way i can receive and send mail ..sorry for my english, i hope i've beeen helpful\nEmanuela \n\n\n\n\n"
    author: "Emanuela "
  - subject: "Re: Yahoo requires pop auth"
    date: 2004-05-09
    body: "I am not able to send emails. "
    author: "Alice Maciver"
  - subject: "I got it  !"
    date: 2005-08-18
    body: "hi ...folks...\ni was havin\u00b4the same problem....\nso the only thing  you have to do for sending and receiving emails...\nin your account click .. authentication for the server....\nright !\n"
    author: "Lilly"
  - subject: "Re: I got it  !"
    date: 2005-08-24
    body: "Could you explain to me what to do for the 530 authentication to be able to send out e-mail\nThanks\nBeth"
    author: "Beth Snyder"
  - subject: "Re: Yahoo requires pop auth"
    date: 2005-11-14
    body: "Go into incrediMail and under tools go to accounts.   Highlight your connection and go to properties,  Then go to Servers and under that put a check in my server requires authentication   this wil clear up the 530 authentication error message.  ken"
    author: "kenole"
  - subject: "Re: Yahoo requires pop auth"
    date: 2007-03-28
    body: "Thanks buddy. Your suggestion worked."
    author: "Steve"
  - subject: "Re: Yahoo requires pop auth"
    date: 2007-08-21
    body: "Thank you so much kenole, your suggestion has solved my problem sending emails which has stumped me for 5 days. My internet provider Xtra has just almalgamated with Yahoo and set up this Bubble webmail and since then many Xtra users have been unable to send or receive emails. Their help desk failed to resolve the issue ( once you could actually get through to a human) so cheers, I'm delighted. Gabby"
    author: "Gabby"
  - subject: "Re: Yahoo requires pop auth"
    date: 2006-01-03
    body: "i m enable to understand what to do"
    author: "Raj"
  - subject: "530 Authentication Required"
    date: 2006-04-15
    body: "Am travelling & currently unable to send e-mail & \"530 authentication required\" keeps coming up. Am using Microsoft Outlook & have never had this before.\n\nIs there anyone out there that can help me?\n\nDistressed Ian\n"
    author: "Ian "
  - subject: "Re: 530 Authentication Required"
    date: 2006-05-10
    body: "i get this too and it sucks! plz somebody helllp mee!!"
    author: "Surfingpikachao"
  - subject: "\"I Got It\" had the solution"
    date: 2006-07-07
    body: "Look at the reply called \"I Got It\" from Lilly.  \nhttp://dot.kde.org/999428101/999511388/999537639/1016671970/1018185020/1021711652/1124377653/\nIt solved my problem also.\nIn Outlook Express, select Tools, select Accounts..., choose Mail tab, select mail provider and click the Properties button.  On the Server tab, check the checkbox for \"My Server requires authorization\".\nRetry sending your mail.  It shoulds work fine now.\n\ngood luck...\n\n"
    author: "BR"
  - subject: "Re: \"I Got It\" had the solution"
    date: 2006-08-24
    body: "Hey thank you. It was a little different on my application of Outlook but I got to where I need to find that box to check. Worked great!!"
    author: "Greg"
  - subject: "Re: \"I Got It\" had the solution"
    date: 2006-10-24
    body: "Yikes! I'm in Microsoft Outlook..can you tell me where I can find the box to check? That error is killin' me!\n"
    author: "Amy"
  - subject: "Re: \"I Got It\" had the solution"
    date: 2006-10-30
    body: "for Outlook (non Express):\nTools.....Options......Mail Setup......Send/Receive......Edit......Account Properties........Outgoing Server...then click the \"My outgoing sever (SMTP) requires authentication."
    author: "stealth"
  - subject: "Re: \"I Got It\" had the solution"
    date: 2006-10-30
    body: "This one for DON worked for me....\nGo to Tools->E-mail Accounts, then View or change existing e-mail accounts.\nHighlight the account to change, then click Change button.\nClick More Settings...\nSelect Outgoing Server tab. Check My outgoing server (SMTP) requires authentication.\nClick Log on using, then enter your User Name and Password.\nNOTE: Use the same User name and Password that you would use if you are connecting directly to your Service Provider (for example, www.comcast.net would be the place to go to get your email if you have COMCAST service). If you can successfully log in to your service provider, place the same User Name and Password in the box for Log on using. You should leave Remember password checked, but do not check the box for Log on using Secure Password Authentication (SPA)."
    author: "stealth"
  - subject: "Re: \"I Got It\" had the solution"
    date: 2007-12-06
    body: "Thank you so much.  This fixed my problem.  "
    author: "Anne"
  - subject: "Re: \"I Got It\" had the solution"
    date: 2008-12-16
    body: "Brilliant thanks so much\n"
    author: "James"
  - subject: "Re: \"I Got It\" had the solution"
    date: 2007-08-30
    body: "Thanks Alot, For a month or so since I re-did my e-mail I havent been able to send e-mails to anyone. I checked the box that said requires authorization. It works fine now. THANKS ALOT!!"
    author: "Jamie"
  - subject: "Re: 530 Authentication Required"
    date: 2007-02-20
    body: "I just wanted to thank those that took time to provide the answer.  I've been struggling with this for weeks and Yahoo! was no help.  "
    author: "Bill Davies"
  - subject: "Re: 530 Authentication Required"
    date: 2007-05-24
    body: "I'm so glad I found this site.  Thanks!  This also worked for Outlook. "
    author: "Erica"
  - subject: "Re: 530 Authentication Required"
    date: 2007-06-06
    body: "You guys are great-thanks for the help!!!"
    author: "Chris"
  - subject: "Re: 530 Authentication Required"
    date: 2007-08-14
    body: "Thanks,It worked for me......"
    author: "pramod Deshmane"
  - subject: "Re: 530 Authentication Required"
    date: 2007-10-02
    body: "thanks, this did the trick"
    author: "andy"
  - subject: "Re: 530 Authentication Required"
    date: 2008-05-03
    body: "ThX a lot :)"
    author: "sochan"
  - subject: "Re: 530 Authentication Required"
    date: 2008-09-02
    body: "Thanks you SO MUCH for this help, I have been trying unsuccessfully for two days to work this out!"
    author: "kay"
  - subject: "Re: Yahoo requires pop auth"
    date: 2006-05-23
    body: "When you are at home, you send and receive via the POP server.  When you are on the road, you have to use the SMTP server.  Usually the SMTP server requires authentication.  I can help you if you have Outlook (but it may be slightly different when using Outlook Express).\n\nGo to Tools->E-mail Accounts, then View or change existing e-mail accounts.\nHighlight the account to change, then click Change button.\nClick More Settings...\nSelect Outgoing Server tab.  Check My outgoing server (SMTP) requires authentication.\nClick Log on using, then enter your User Name and Password.\nNOTE:  Use the same User name and Password that you would use if you are connecting directly to your Service Provider (for example, www.comcast.net would be the place to go to get your email if you have COMCAST service).  If you can successfully log in to your service provider, place the same User Name and Password in the box for Log on using.  You should leave Remember password checked, but do not check the box for Log on using Secure Password Authentication (SPA).\n\nI hope this helps.\n\nDon"
    author: "Don Ethen"
  - subject: "Re: Yahoo requires pop auth"
    date: 2006-06-12
    body: "THANKS DON!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n\nIT WORKS!!!!!!!!\n\n\nYOU ROCK MAN:D"
    author: "Gustavo"
  - subject: "Re: Yahoo requires pop auth"
    date: 2006-06-29
    body: "Don,\n\nThank you for the help. Yahoo/Microsoft sent me a note \"The setting is controlled by the check box 'Logon to the incoming server before sending mail'. which of course disable the logon information to be entered - and never worked. Your note made it work! Thanks and let me repeat another relieved user's comments - YOU ROCK!\n\nGabriella\n"
    author: "Gabriella"
  - subject: "Re: Yahoo requires pop auth"
    date: 2006-05-23
    body: "When you are at home, you send and receive via the POP server.  When you are on the road, you have to use the SMTP server.  Usually the SMTP server requires authentication.  I can help you if you have Outlook (but it may be slightly different when using Outlook Express).\n\nGo to Tools->E-mail Accounts, then View or change existing e-mail accounts.\nHighlight the account to change, then click Change button.\nClick More Settings...\nSelect Outgoing Server tab.  Check My outgoing server (SMTP) requires authentication.\nClick Log on using, then enter your User Name and Password.\nNOTE:  Use the same User name and Password that you would use if you are connecting directly to your Service Provider (for example, www.comcast.net would be the place to go to get your email if you have COMCAST service).  If you can successfully log in to your service provider, place the same User Name and Password in the box for Log on using.  You should leave Remember password checked, but do not check the box for Log on using Secure Password Authentication (SPA).\n\nI hope this helps.\n\nDon"
    author: "Don Ethen"
  - subject: "Re: Yahoo requires pop auth"
    date: 2006-08-22
    body: "I have the same problem, using Outlook, and yet your answer hasn't fixed the issue,  I am using a vodafone GSM card, and I have tried the software on two new laptops, both receive but won't send, perhaps the problem lies with MS Outlook/Express?  Do you know how to contact them?\nThanks for your help"
    author: "Richard Ahl"
  - subject: "Re: Yahoo requires pop auth"
    date: 2006-11-06
    body: "cannot send through incredimail"
    author: "Robert.Elliott"
  - subject: "Re: Yahoo requires pop auth"
    date: 2007-01-09
    body: "hi friends.. previously i was also having same problem and i got solved by carrying following procedures...\n\n\nlog on to ms outlook\n\ngo to TOOLS->email account-> change existing account->more setting->outgoing server->check my outgoing server requaire authentification->than check use same setting as my incoming mail server...\n\nhope u got the solution..."
    author: "ASHOK"
  - subject: "Re: Yahoo requires pop auth"
    date: 2007-04-17
    body: "everyone try this, it worked for me thanks"
    author: "bb"
  - subject: "This is the one!!!!!"
    date: 2007-09-30
    body: "just took a long time to find it laid out properly!! Thankyouuuuuuuuuuuuu!"
    author: "Laura"
  - subject: "Re: Yahoo requires pop auth"
    date: 2007-02-11
    body: "Why is this happening all of a sudden?"
    author: "wendy cawthray"
  - subject: "Re: Yahoo requires pop auth"
    date: 2008-12-05
    body: "I am unable to sent my mails because of the error no. 530 authentication is required. Please help or provide authentication.\n\n"
    author: "Jayantibhai Patel"
  - subject: "Re: Yahoo requires pop auth"
    date: 2002-09-13
    body: "I dont want to reply, or read other replies,, I want HELP! and the only help I can find is NOT in ENGLISH. UGH.... yes it's the same 530, requires authentication.... That and it will not connect me,,, always loggon error problems.....HEEEELLLLLLLLPPP"
    author: "A jackson"
  - subject: "Re: Yahoo requires pop auth"
    date: 2002-11-05
    body: "Important: The Yahoo! Mail SMTP server now requires authentication. To turn this setting on, follow these steps: \n\nFrom the Tools menu, choose \"Accounts.\" \nSelect the \"Mail\" tab. \nSelect your Yahoo! Mail account and click \"Properties.\" \nClick on the \"Servers\" tab. \nCheck the box next to \"My Server Requires Authentication.\" \nPlease make sure that \"Log on using Secure Password Authentication\" is NOT checked. \nClick \"OK.\" \n"
    author: "just trying to help"
  - subject: "Re: Yahoo requires pop auth"
    date: 2002-11-15
    body: "THANK U SO MUCH!!!! i spent an hour and a half on the phone with the people at sbc and they couldn't fix it!  just goes to show how little they know!  thanks again!"
    author: "Christina"
  - subject: "Re: Yahoo requires pop auth"
    date: 2003-05-19
    body: "ok i cannot sent for the life of me.it want auth i have tried to do what u say but as iam completly useless on comp iam making some mistake i would be really greatful if u could tell me in real simple terms how i can do this please."
    author: "taz"
  - subject: "Re: Yahoo requires pop auth"
    date: 2003-06-16
    body: "appreciate it!   it's always nice to find something the works and is helpful on the internet.   thanks,  Tim\n"
    author: "Tim"
  - subject: "Re: Yahoo requires pop auth"
    date: 2003-10-26
    body: "OK Thanks this was very helpfull for me.\n"
    author: "Timal"
  - subject: "Re: Yahoo requires pop auth"
    date: 2006-02-05
    body: "I was able to resolve my issue from your reply to someone else. When I put a check mark in the box next to \"My Server Requires Authentication.\" and clicked OK I was able to send email.  Thank you for your help.\n"
    author: "Samuel"
  - subject: "Re: Yahoo requires pop auth"
    date: 2006-09-15
    body: "My understanding is that you have to purchase Yahoo Mail Plus in order for it to work. I have worked on this for days and this is what I came up with."
    author: "Patti"
  - subject: "Re: Yahoo requires pop auth"
    date: 2007-01-01
    body: "thanks u helped me out big time\n"
    author: "justine"
  - subject: "Re: Yahoo requires pop auth"
    date: 2002-12-17
    body: "Have the same problem.  So far no help.  What have you found?"
    author: "Nan"
  - subject: "Re: Yahoo requires pop auth"
    date: 2003-02-04
    body: "Error Message:  \"The connection to the server has failed. Account 'mail.yourdomain.com', Server: 'mail.yourdomain.com', Protocol: SMTP, Port: 25, Secure(SSL): No, Socket Error: 10051, Error Number: 0x800CCC0E.\" \n\nCause/Description: The reason for this error is because many ISP's (Internet Service Providers) are blocking \"Port 25\" which is the port used to send email. They do this as a security precaution to protect against \"spam\" (unsolicited email). More and more ISP's are doing this everyday (none of us like spam!).  Here are just a few of the major ISP's that block Port 25: Earthlink/Mindspring, AT&T, MSN, Verizon, Bellsouth, Roadrunner, and most cable and DSL companies. \n\nSolution:  The way around this is to set your SMTP settings to those of your ISP's. For example: \"mail.cox.com\" rather than \"mail.yourdomain.com\". After doing that, you will be sending mail though your ISP's servers rather than ours.  Keep in mind that nothing else changes in the way your mail is delivered.  Your \"from\" address and your \"reply to\" address will remain the same. \n\nYou will need to contact your ISP and ask for the \"SMTP or out-going mail server address\", to find out exactly what to replace mail.yourdomain.com with.  \n"
    author: "Rajan"
  - subject: "Re: Yahoo requires pop auth"
    date: 2003-03-15
    body: "I contacted my ISP provider and they said that I was using the proper the SMTP setting.  They also said that they did not block port 25.  I continue to get the same error message.  I called MS thinking it could be a problem with Outlook express.  They told me it was my ISP's problem, ISP tells me it is MS problem.  HELP, PLEASE!!!\n"
    author: "Sarah"
  - subject: "Re: Yahoo requires pop auth"
    date: 2003-03-21
    body: "Sarah..I have exactly same problem and I would like to know how did u solve it?"
    author: "Kamal"
  - subject: "Re: Yahoo requires pop auth"
    date: 2003-06-21
    body: "Hey there,\n\nI was just at this website looking for a solution when I found it and I saw your post so I'd thought, being the good natured fella I am, I'd give you the solution to the problem as well.\n\nI have DSL through COX COMMUNICATIONS. And they sent me a nice little e-mail the other day about SPAM that I breezed through and didn't really pay too much attention to. All of a sudden I couldn't send e-mail but, I could recieve it. No problem I thought...probably some glitch...I'll just restart my computer. And about 2 minutes later....I had a freshly rebooted computer with the same exact problem: I couldn't send mail. I figured it was something wrong with Yahoo!\nUpon closer inspection in the e-mail I got from COX it was basically telling me that if you get your e-mail through a third party....like Yahoo!, Excite, MSN, AOL, etc and you get through e-mail programs like Outlook Express, Eudora, etc. that because of SPAM it is going to change the outgoing SMTP address so it cuts down on about 9/10 SPAMS. So since I have a Yahoo! account and because I have Outlook Express I had to change my outgoing SMTP to smtp.east.cox.net\n\nI got that SMTP from the e-mail COX COMMUNICATIONS sent out to me. If you have COX COMMUNICATIONS as well you can try the above SMTP and see if it works. If not, then get in contact with your service provider and ask them what SMTP you can use to send e-mail.\n\nIf that doesn't work I suggest going to church and lighting a candle and pray to the computer gods that one day you'll be able to send mail again.\n\nSeriously, I hope I've helped. Shoot me a holler if you've still got the same problem...\n\n~<null>\n<null>\n"
    author: "<null>"
  - subject: "Re: Yahoo requires pop auth"
    date: 2006-03-08
    body: "Chris\nWhat happens in the instance that I use wanadoo for my mail server, and I use my neighbours wireless network which is yahoo and I get my emails via outlook express. I can receive mail but not send. I'll head off to church soon."
    author: "Jules"
  - subject: "Re: Yahoo requires pop auth"
    date: 2006-07-22
    body: "Well, first, COX COMMUNICATIONS is a Cable company, not DSL.\n\nSecond, a simple test will tell you if they are, in fact, blocking port 25.\n\nHere is how to test it:\nA. Go to Start -> Run -> type: cmd\nB. When the Command prompt appears type: telnet <your mail server address> 25\n(include the spaces exactly as shown above)  examples of server addresses are ones such as smtp.west.cox.net or smtp.sbcglobal.yahoo.com\n\nC. Wait a few seconds. You should see the following: '220 <your mail server address> ESMTP'. This means that it is not blocked.\nD. If you do not see this message, your SMTP is being blocked. \n\nIf they ARE NOT blocking port 25, then you should be able to use whatever outgoing email address you want in OE, Oulook, Eudora, Mac Mail, etc.\n\nIf they ARE blocking port 25, then you could try changing your port, or just use the outgoing mail server settings for their network fot the outgoing side."
    author: "Me"
  - subject: "Re: Yahoo requires pop auth"
    date: 2006-07-28
    body: "Weird, but I am in VA and tried the .east. version above. No luck.  Oddly, .west. worked.  whatever.  At least I have mail.  THanks. "
    author: "Nacho"
  - subject: "Re: Yahoo requires pop auth"
    date: 2003-08-16
    body: "Yes!!! Thank god I found this thread.. I just paid for the Yahoo pop access.. And I will admit as much as COX goes on and on (i have cox digital internet) about trying to block spam, I get more spam in my COX account than any other!! And, I have never used the emails for anything sometimes.. and the spam still got through.. I was fed up and emailed them about the problem, they bascially told me there was nothing they could do, so I moved on to Yahoo, I liked their service on the web and thought I would expand my mailbox and try the pop access out. Ok, so all is fine there, and I try to get it up and running, and I was having major SMTP problems, and I am in Cali, and after reading this I just entered the smtp.west.cox.net\npeople in the midwest, nebraska, etc, would use smtp.central.cox.net, and so on..\n\nGood luck people..!"
    author: "sarah"
  - subject: "Re: Yahoo requires pop auth-So does Excite!"
    date: 2006-01-30
    body: "I was having the same problem with Outlook 6, and the solution (changing the smtp to cox) worked right away. Thanks for the help! "
    author: "jp"
  - subject: "Re: Yahoo requires pop auth"
    date: 2003-09-25
    body: "I am getting the follwoing error message when i tired to setup yahoo mails for outlook express, Can anyone solve this problem.\n\nError:The host 'pop.mail.yahoo.com' could not be found. Please verify that you have entered the server name correctly. Account: 'Yahoo', Server: 'pop.mail.yahoo.com', Protocol: POP3, Port: 110, Secure(SSL): No, Socket Error: 11001, Error Number: 0x800CCC0D.\n"
    author: "Hari"
  - subject: "Re: Yahoo requires pop auth"
    date: 2004-01-06
    body: "You have to pay $19.99 for the remote pop access through yahoo to use yahoo mail through any other program than the yahoo mail in internet explore."
    author: "Alex"
  - subject: "Re: Yahoo requires pop auth"
    date: 2004-01-13
    body: "Hey guys, just a heads up.  There is a program called YahooPOPS (yahoopops.sourceforge.net) that will get around the Yahoo \"Pay for POP\" problem.  It runs as a local POP server on your machine.  Set up Outlook or Express to connect to it as a POP server.  It goes to Yahoo Mail, uses your username/password, and \"screen scrapes\" (gets data via http) all of your mail and delivers it to Outlook.\n\nOnce you have it set up, it is transparent, delivers yahoo mail like a charm.\n\ncheers\n\nAndrew"
    author: "Andrew"
  - subject: "Re: Yahoo requires pop auth"
    date: 2004-02-25
    body: "Andrew, thanks for the infomation about this program - it was just what I was looking for! "
    author: "Joe Sabo"
  - subject: "Re: Yahoo requires pop auth"
    date: 2004-08-30
    body: "Maybe I'm doing this wrong, but I wasn't able to set up the yahooPOPS for the Outlook Express.\n\nWould you be so kind as to lead me thru the steps for this application? I'm new to this type of setup.\n\nThanks,\nRudy"
    author: "Rudy"
  - subject: "Re: Yahoo requires pop auth"
    date: 2004-12-26
    body: "hi actully , these days yahoo pop gets paid..\nso if u want to set up yahoo pop3. u hv to subscibe first at yahoo.\nthanks\nanirudh\nnet warm"
    author: "Anirudh"
  - subject: "Re: Yahoo requires pop auth"
    date: 2008-07-15
    body: "I just paid yahoo $19.99 for the up outlook. Can you tell me step by step what I should put in the pop3 and the smtp boxes.  I will call and cancel my account with yahoo and just use the free one.\n\nThanks a million for your help!\n\nChow,\nE Jelks"
    author: "Edna Jelks"
  - subject: "Re: Yahoo requires pop auth"
    date: 2004-01-15
    body: "This was wonderful advice for getting the SMTP to work.  Very clear and concise.\n\nThank you."
    author: "spike"
  - subject: "Re: Yahoo requires pop auth"
    date: 2004-02-15
    body: "Thanks a bunch for this. It was pissing me off now for about 2 weeks.\n\nThanks again,\n\nSteve"
    author: "Steve Johnson"
  - subject: "Re: Yahoo requires pop auth"
    date: 2004-04-15
    body: "Thank you Rajan.  It works like a charm."
    author: "Valerie"
  - subject: "Re: Yahoo requires pop auth"
    date: 2004-10-01
    body: "The basic problem with this thread is it doesn't distinguish between POP and SMTP. POP is for receiving and SMTP is for sending. The way ISPs are blocking services is very different depending on which type of server you are talking about. 'Blocking port 25' means that you are not going to be able to send emails with their SMTP server. Many ISPs also block the use of any other POP server than their own, which is really annoying.\n\nI have a user that had the problem of not being able to receive emails. We fixed this problem before, but I can't remember how. She is using outlook express. I don't know who her ISP is, which is a highly relevant fact. She already knows to change the SMTP server to the ISPs. I don't use Outlook Express, so I don't even know what the settings look like (I don't run Windows or MAC OS either).\n\nI get POP and SMTP mixed up all the time. Too many acronyms, too little time!\n\n"
    author: "Manchineel"
  - subject: "Re: Yahoo requires pop auth"
    date: 2006-07-05
    body: "Hi Dear,\n\n\nYour configuration is not working for mail"
    author: "Dharmesh Chauhan"
  - subject: "Re: Yahoo requires pop auth"
    date: 2004-10-30
    body: "I got it from http://help.yahoo.com/help/us/mail/pop/pop-06.html \n\n\nYahoo! Mail users can read their email in Outlook Express by subscribing to the Yahoo! Mail Plus service. This service allows you to use Outlook Express or another POP3 client to access and manage your Yahoo! Mail messages. \nOutlook Express allows you to add a new email account to your existing profile. This means you do not have to replace your current settings in order to send and receive Yahoo! Mail messages. Here's how: \n\nFrom the Tools menu, choose \"Accounts.\" \nSelect the \"Mail\" tab. \nClick the \"Add\" button. \nFrom the Add menu, click \"Mail.\" \nIn the text box labeled Display Name, type your name and click \"Next.\" \nIn the Email Address box, type your Yahoo! Mail address (be sure to include \"@yahoo.com\") and click \"Next.\" \nUnder \"My incoming mail server is a...\" select \"POP3.\" \nType \"pop.mail.yahoo.com\" in the Incoming Mail (POP3, IMAP, or HTTP) Server box. \nType \"smtp.mail.yahoo.com\" in the Outgoing Mail (SMTP) Server box. \nClick \"Next.\" \nIn the Account Name box, type your Yahoo! Mail ID (your email address without the \"@yahoo.com\"). \nIn the Password box, type your Yahoo! Mail password. \nIf you want Outlook Express to remember your password, check the \"Remember password\" box. \nDo not check the boxes labeled \"Log on using Secure...\" \nClick \"Next.\" \nClick \"Finish.\"\nImportant: The Yahoo! Mail SMTP server now requires authentication. To turn this setting on: \n\nFrom the Tools menu, choose \"Accounts.\" \nSelect the \"Mail\" tab. \nDouble-click the account labeled \"pop.mail.yahoo.com.\" \nSelect the \"Servers\" tab. \nCheck the box next to \"My Server Requires Authentication.\" \nClick \"OK.\" \nTo control deletion of messages from the Yahoo! Mail Server: \n\nFrom the Tools menu, choose \"Accounts.\" \nSelect the \"Mail\" tab. \nDouble-click the account labeled \"pop.mail.yahoo.com.\" \nSelect the \"Advanced\" tab. \nIn the Delivery section at the bottom of the window, check \"Leave a copy of messages on server\" if you want to save your Yahoo! Mail messages on the Yahoo! Mail server as well as on your local computer. Do not check this box if you want your messages to be deleted from the Yahoo! Mail server once you have received them in Outlook Express. \n\n^______^"
    author: "^___^"
  - subject: "Re: Yahoo requires pop auth"
    date: 2005-10-24
    body: "chao\nchaoo\n"
    author: "Nam"
  - subject: "Re: Yahoo requires pop auth"
    date: 2005-01-03
    body: "Go to the Yahoo! Mail Help pages and check out the help under POP and Forwarding. The Yahoo! server requires Authentication so you need to turn on that setting in your mail program."
    author: "Mitchell"
  - subject: "Re: Yahoo requires pop auth"
    date: 2005-03-16
    body: "I can't find the way to enable the POP access. I have already subscribed the Mail Puls service. But when I integrate it to my BlackBerry a/c, it was rejected.  I suspect that I haven't auth the pop access function. Please shows me where can I do this. Thanks"
    author: "Alfred"
  - subject: "Re: Yahoo requires pop auth"
    date: 2005-02-14
    body: "The connection to the server has failed. Account: 'yahoo.com', Server: 'yahoo.com', Protocol: SMTP, Port: 25, Secure(SSL): No, Socket Error: 10060, Error Number: 0x800CCC0E     its asking me for thepop server incoming & outgoing"
    author: "Nela"
  - subject: "Re: Yahoo requires pop auth"
    date: 2005-04-11
    body: "1.The connection to the server has failed. Account: 'pop.mail.yahoo.com', Server: 'mail.yahoo.com', Protocol: SMTP, Port: 25, Secure(SSL): No, Socket \nError: 10060, Error Number: 0x800CCC0E.\n\n2.The connection to the server has failed. Account: 'pop.mail.yahoo.com', Server: 'mail.yahoo.com', Protocol: POP3, Port: 110, Secure(SSL): No, Socket Error: 10060, Error Number: 0x800CCC0E"
    author: "ZUBIER"
  - subject: "Re: Yahoo requires pop auth"
    date: 2006-11-18
    body: "1.The connection to the server has failed. Account: 'pop.mail.yahoo.com', Server: 'mail.yahoo.com', Protocol: SMTP, Port: 25, Secure(SSL): No, Socket \nError: 10060, Error Number: 0x800CCC0E."
    author: "prashant"
  - subject: "Re: Yahoo requires pop auth"
    date: 2007-04-10
    body: "I am having the same issues.  Did you get yours sorted out?  Would love to hear how if you did?\n\nTa's\n\n:)"
    author: "Leanne"
  - subject: "Re: Yahoo requires pop auth"
    date: 2005-09-21
    body: "Try port 587 instead of 25 that should solve he issues with sending emails.  I am now getting server found but no response errors (0x8004210) when trying to receive emails.  Started having problems mid August 2005.  haven't been able to fix it yet.  any thoughts?????"
    author: "Curt"
  - subject: "Re: Yahoo requires pop auth"
    date: 2005-10-26
    body: "For outgoing mail, I use the free SMTP server program at http://www.softstack.com/ which sits on my PC and communicates directly to the SMTP server of the computer you are sending mail to. No ISP SMTP server needed as when I have email to send, I just start up the free SMTP server on my machine and send the email (hit the send button in OE)  and then close the SMTP server again after it has gone.  It usually works like a charm. Easy to set up too. Cheers, Paul"
    author: "Paul"
  - subject: "Re: Yahoo requires pop auth"
    date: 2005-10-26
    body: "I got really sick with this Yahoo configurations in OE coz i can i recieve my emails but i can't send i think there is some problem with SMTP settings i followed the instructions like configuiring Yahoo mail on OE but did not worked any one help me out!!!!!!!!"
    author: "James"
  - subject: "Re: Yahoo requires pop auth"
    date: 2005-10-26
    body: "James;\nPlease use the SMTP port 587 in place of 25\nYour problem will be solved"
    author: "chow"
  - subject: "Re: Yahoo requires pop auth"
    date: 2005-12-03
    body: "Switching ports isn't a cure-all.  I have the exact same problem.  From looking at the boards, it appears that the common denominator amongst all of us with this problem is Norton AntiVirus."
    author: "Stu Shapley"
  - subject: "Re: Yahoo requires pop auth"
    date: 2006-01-29
    body: "yeah yeah yeah"
    author: "nika"
  - subject: "Re: Yahoo requires pop auth"
    date: 2006-04-29
    body: "AAAAAAAAAAAAh!  i am now bald from this pop 25 stuff!!!  i have a domain name with freeservers and access it via outlook E, all worked fine when i was with comcast. now i'm with cox (west) and they block port 25, i tried 465, 287.... and every other one i found in different help pages..... non work!  calling cox all they could do is set me up a \"cox\" email..... i want my emails to come in as my domain, not xxxxx.cox.net !!!! help help help before i smash my computer to itsy bitsy little pieces!!!!!\nmr softail"
    author: "softail"
  - subject: "Re: Yahoo requires pop auth"
    date: 2006-01-31
    body: "cha cha cha yea wool wol holla"
    author: "mia"
  - subject: "Re: Yahoo requires pop auth"
    date: 2006-01-31
    body: "cha cha cha yea wool wol holla"
    author: "mia"
  - subject: "Re: Yahoo requires pop auth"
    date: 2006-02-04
    body: "yeah thats my name to"
    author: "mia"
  - subject: "Re: Yahoo requires pop auth"
    date: 2006-06-23
    body: "Thanks .... I aam thoughly confused now.Just whrn I thought I knew what to do "
    author: "joan"
  - subject: "Re: Yahoo requires pop auth"
    date: 2006-07-23
    body: "i have yahoo mail id now. and i would like to use this id in my outlook express. Please guide me how to activate this in outlook express."
    author: "Guru"
  - subject: "Re: Yahoo requires SOMETHING CHANGES!"
    date: 2006-09-12
    body: "Something changes!!!!!\nIf you use your ISP SMTP, who receive the mail, can right click on the message/Details and will see X Originating my yourISPaddress@yourISP.com\n\nIn few words, he can see your private e-mail address!\n\n"
    author: "only"
  - subject: "Re: Yahoo requires pop auth"
    date: 2006-10-03
    body: "I am using Yahoo e-mail, cox cable and Outlook.\n\nLast year I had the same problem until I changed SMTP port to 587 in place of port 25. The problem was resolved for nearly a year. Now the problem re-appeared. I get messages on Outlook but my messages ARE NOT SENT. \n\nNow I get the error message:\n\n\"Task 'pop.mail.yahoo.com - Sending' reported error\n(0x8004210B) : 'The operation timed out waiting for a\nresponse from the sending (SMTP) server. If you\ncontinue to receive this message, contact your server\nadministrator or Internet service provider (ISP).'\"\n\nAnyone else is having this problem?\n\nAnyone knows how to resolve this new problem?\n\nThanks \nPete"
    author: "Pete"
  - subject: "Re: Yahoo requires pop auth"
    date: 2007-02-09
    body: "Thank you for this information.  It worked!  I have been trying to resolve this issue for months!  Thank you, thank you, thank you!"
    author: "Sydney Schmidt"
  - subject: "Re: Yahoo requires pop auth"
    date: 2007-06-01
    body: "settings have changed again, just visit their mail page for detailed instructions....see, \"POP3 Settings\"    smtp465 check box    pop3  995 check box"
    author: "cyndi"
  - subject: "Re: Yahoo requires pop auth"
    date: 2007-11-28
    body: "Just find out what happened: Outgoing SMTP port 567 or wharever just stopped working, just had to change it to port 25... was able to sent my e-mails again.\nYou click in: TOOLS/Accounts/<<select your account>>/properties/advanced/Outgoing Mail(SMTP), and change the number to 25. It seems this number has to change everytime the outgoing mails start to fail.\n\n"
    author: "Ericmor"
  - subject: "Re: Yahoo requires pop auth"
    date: 2003-02-04
    body: "Error Message:  \"The connection to the server has failed. Account 'mail.yourdomain.com', Server: 'mail.yourdomain.com', Protocol: SMTP, Port: 25, Secure(SSL): No, Socket Error: 10051, Error Number: 0x800CCC0E.\" \n\nCause/Description: The reason for this error is because many ISP's (Internet Service Providers) are blocking \"Port 25\" which is the port used to send email. They do this as a security precaution to protect against \"spam\" (unsolicited email). More and more ISP's are doing this everyday (none of us like spam!).  Here are just a few of the major ISP's that block Port 25: Earthlink/Mindspring, AT&T, MSN, Verizon, Bellsouth, Roadrunner, and most cable and DSL companies. \n\nSolution:  The way around this is to set your SMTP settings to those of your ISP's. For example: \"mail.cox.com\" rather than \"mail.yourdomain.com\". After doing that, you will be sending mail though your ISP's servers rather than ours.  Keep in mind that nothing else changes in the way your mail is delivered.  Your \"from\" address and your \"reply to\" address will remain the same. \n\nYou will need to contact your ISP and ask for the \"SMTP or out-going mail server address\", to find out exactly what to replace mail.yourdomain.com with.  \n"
    author: "Rajan"
  - subject: "Re: Yahoo requires pop auth"
    date: 2003-10-14
    body: "Well I have setup my Yahoo! in outlook 2000 and somtimes I recieve errors like 0x800ccc0e when i try to download the emails to my computer. How can i fix it ? It happens often but not allways."
    author: "Mentar"
  - subject: "Re: Yahoo requires pop auth"
    date: 2003-11-02
    body: "THANK YOU, MY ISP BLOCKED PORT 25..and in my SMTP I put the port of my ISP and SSL..it worked awsome...thank you very much..your awsome!"
    author: "I DID WHAT YOU SAID!! AND IT WORKED THANK YOU"
  - subject: "Re: Yahoo requires pop auth"
    date: 2003-12-03
    body: "I have Adelphia cable anyone knows the smtp for it?"
    author: "Maria"
  - subject: "Re: Yahoo requires pop auth"
    date: 2006-08-03
    body: "I don't think you should be messing with that.  \nPleas stay back from the PC - "
    author: "Blammer"
  - subject: "Re: Yahoo requires pop auth"
    date: 2003-12-03
    body: "Adelphia: \nSMTP (outgoing) mail server: smtp.blk.adelphia.net\nWeb: http://www.adelphia.com\nPhone:(888)233-5638\nServices Provided: Cable Modem \n--------------------------------------------------------------------------------\n\nAmerica Online (AOL): \nSMTP (outgoing) mail server: AOL does not provide SMTP service. 4Help suggests you use an alternate method of sending e-mail, such as Virginia Tech's WebMail found at https://webmail.vt.edu\nWeb: http://www.aol.com\nServices Provided: Dial-up and Broadband \n--------------------------------------------------------------------------------\n\nBEV.net: \nSMTP (outgoing) mail server: smtp.bev.net\nWeb: http://www.bev.net\nE-mail: bev.office@bev.net\nPhone: (540)231-4786\nServices Provided: DNS support, E-mail services \n--------------------------------------------------------------------------------\n\nBlacksburg.net: \nSMTP (outgoing) mail server: smtp.blacksburg.net\nWeb: http://www.blacksburg.net\nE-mail: sales@blacksburg.net\nPhone: (540)961-4445\nServices Provided: Dial-up \n--------------------------------------------------------------------------------\n\nCitizen's Internet: \nSMTP (outgoing) mail server: smtp.swva.net\nWeb: http://www.swva.net\nE-mail: citizens@swva.net\nPhone: (880)941-0426\nServices Provided: Dial-up, ISDN, DSL, Ethernet \n--------------------------------------------------------------------------------\n\nConnect Me/FOB Services: \nSMTP (outgoing) mail server: 199.3.140.8\nWeb: http://www.connectmeusa.com\nPhone: (540)200-0100\nApartments: \nAbbey Gardens \nApartment Heights \nCollegiate Court \nFairmont Village \nGreen Street \nHouston St. Village \nLane Park \nNorth View \nPheasant Run \nShenandoah \n604 Center St. \nSundance Ridge \nServices Provided: Ethernet \n--------------------------------------------------------------------------------\n\nCox Cable: \nSMTP (outgoing) mail server: smtp.east.cox.net\nWeb: http://roanoke.cox.net\nPhone: (540)776-3848\nServices Provided: Cable Modem \n--------------------------------------------------------------------------------\n\nEarthlink: \nSMTP (outgoing) mail server: smtp.earthlink.net\nWeb: http://www.earthlink.net\nPhone: (800)820-8748\nApartments: Foxridge\nServices Provided: Dial-up, Cable Modem, DSL, Ethernet \n--------------------------------------------------------------------------------\n\nFloydVA.net: \nSMTP (outgoing) mail server: mail.floydva.net\nWeb: http://www.floydva.net\nE-mail: info@floydva.net\nPhone: (540)745-6340\nCustomer support toll-free number: 1-800-290-0461\nService Area: New River Valley \nServices Provided: Local dial-up and national services \n--------------------------------------------------------------------------------\n\nNTC: \nSMTP (outgoing) mail server: smtp.blacksburg.ntc-com.net\nWeb site: http://www.ntc-com.com\nE-mail: info@ntc-com.com\nPhone: (540)961-6154\nApartments: \nAlpha Gamma Rho house \nBerryfield \nBrendon I, II, III Apartments \nCarlton Scott Apartments \nCollegiate Suites \nHunters Ridge \nPatrick Henry Place \nPheasant Run Crossing \nRoanoke Street Apartments \nStonegate Apartments \nSturbridge Square \nTerrace View \nUniversity Place \nUniversity Terrace \nThe Village \nServices Provided: Ethernet \n--------------------------------------------------------------------------------\n\nNTELOS: \nSMTP (outgoing) mail server: mail.ntelos.net\nWeb: http://www.ntelos.net\nPhone: (540)641-1324\nServices Provided: Dial-up, DSL \n--------------------------------------------------------------------------------\n\nOmega Communications: \nSMTP (outgoing) mail server: smtp.i-plus.net\nWeb: http://www.i-plus.net\nE-mail: customercare@i-plus.net\nPhone: (540)633-6327\nServices Provided: Dial-up, Broadband \n--------------------------------------------------------------------------------\n\nTriple Com: \nSMTP (outgoing) mail server: smtp.triplecom.net\nWeb: http://www.triplecom.net\nPhone: (540)552-2600\nApartments: \n\nCedarfield \nThe Chase \nChasewood \nHarrell \nKnollwood \nOakbridge \nTech Terrace \nTerpence \nWilson Street \nServices Provided: DSL, Ethernet \n--------------------------------------------------------------------------------\n\nVerizon \nSMTP (outgoing) mail server: outgoing.verizon.net\nWeb: http://www22.verizon.com\nPhone: (800)567-6789\nServices Provided: DSL \n--------------------------------------------------------------------------------\n\nVTACS: \nSMTP (outgoing) mail server: mail.vtacs.com\nWeb: http://www.vtacs.com\nE-mail: sysop@vtacs.com\nPhone: (540)552-6318\nApartments: Jefferson Apartments\nServices Provided: Ethernet \n\n"
    author: "Maria"
  - subject: "Re: Yahoo requires pop auth"
    date: 2004-04-04
    body: "what should i do i'm getting when trying to send  this when trying to send emails:\n The connection to the server has failed. Account: 'Verizon Mail', Server: 'outgoing.verizon.net', Protocol: SMTP, Port: 25, Secure(SSL): No, Socket Error: 10060, Error Number: 0x800CCC0E"
    author: "lee"
  - subject: "Re: Yahoo requires pop auth"
    date: 2005-03-09
    body: "I have 3 email accounts. My isp is with aol but i use yahoo and incredimail. How do i set up my yahoo mail to go to my incredimail? I want to use incredimail for incoming and outgoing.The pop and smtp and ports are incorrect i think. Not sure if i need to get yahoo plus or not. thanks\nI get error invalid command Authorization failed."
    author: "Sharon"
  - subject: "Re: Yahoo requires pop auth"
    date: 2005-04-29
    body: "Hi, I'm having exactly the same problem you are having.I want to set up my yahoo mail to my incredimail. Pls. let me know when you rectify the problem . Thanks"
    author: "Ugo"
  - subject: "Re: Yahoo requires pop auth"
    date: 2005-04-29
    body: "Hi still have not figured it out yet, i think i may have to pay yahoo the 19.95 and then i'll be able to. Let me know too if you figure it out."
    author: "Sharon"
  - subject: "Re: Yahoo requires pop auth"
    date: 2005-06-17
    body: "Yup that seems to be the only way that it will work.  There might be a way of using it with say hotmail and maybe having the hotmail account do pop of yahoo.  I am not sure.  Maybe there is a way around it.  I think it is wrong of yahoo to do this to everyone.  I know of a few people that stopped using Yahoo altogether just because of this.  \n\n"
    author: "Debra"
  - subject: "Re: Yahoo requires pop auth"
    date: 2008-03-04
    body: "I have paid the 19.95 go get plus yahoo and i still can't use incredimail with yahoo!   i thought paying and going plus with yahoo would solve my problem and it did not.  i am stil having problems with yahoo or incredimail [not sure who is to blame] i have aol, too"
    author: "Dee"
  - subject: "Re: Yahoo requires pop auth"
    date: 2008-03-06
    body: "I'm having the same problem.  I haven't paid for YAHOO's services (and probably won't) but I upgraded to Incredimail XE a while ago and that didn't help.  (Thought I'd share that with you so you didn't pay for that in hopes it would help)  I'll keep looking for the answer myself and will post if I figure it out...  Good luck to you!"
    author: "David"
  - subject: "Re: Yahoo requires pop auth"
    date: 2005-06-02
    body: "yeah..it kills me but I called yahoo and each one of us has a secure pop incoming server address with verification available to us for $19.95 a year.\n\nI can't believe the tech geeks haven't figured this out out yet.\n\n"
    author: "Cassata"
  - subject: "Re: Yahoo requires pop auth"
    date: 2005-09-07
    body: "now hotmail even charges, they should know that's crap (i have other words, but i'll be nice) now it says...\n\"Could not connect to Hotmail as XXXXXXX@hotmail.com.\n\nHotmail no longer allows email access via Outlook Express for free email accounts.\n\nPlease visit http://www.hotmail.com/oe to learn more.\n\n  Account: 'Hotmail', Server: 'http://services.msn.com/svcs/hotmail/httpmail.asp', Protocol: HTTPMail, Server Response: 'Access to Hotmail via Outlook and Outlook Express now requires a subscription. Please sign up at http://upgrade.msn.com', Port: 0, Secure(SSL): No, Error Number: 0x800CCCF6\"\n\nsorry guys, but i guess they hate us, what's the point of having Outlook Express and everything? dumb..."
    author: "Jacknbox"
  - subject: "Re: Yahoo requires pop auth"
    date: 2006-08-15
    body: "Hi,\nAt last I found the POP and SMTP servers for Hotmail. Just add these against POP and SMTP respectively\nPOP3 : pop3hot.com\nSMTP : pop3hot.com\nIt works.\n\n\nRgds,\nNimit"
    author: "redbug"
  - subject: "Re: Yahoo requires pop auth"
    date: 2008-01-14
    body: "do you think that the phone companies of the world maybe forgot whose territories are in the usa? moving from an ameritech to let us say southern bell or att might have something to do with the delima that the msn companies are having? might even be a sprint nextel or some other private band they are messing up.\n\ndo I know how to think?\n\nseems that the http mail boxes are or are not stuffed with someone elses reported junk mail.\n\nluv LEDOFF  "
    author: "Mike Kovack"
  - subject: "I can't use e-mail"
    date: 2003-02-01
    body: "How can I send my e-mail to my friends?"
    author: "wanchengann"
  - subject: "Re: I CAN USE EMAIL AGAIN!!!!!!!!!"
    date: 2003-02-10
    body: "YIPPEEEE  Thanks to your helpful instructions after sitting here for nearly 2 days infront of this computer I have success.\n"
    author: "moya white"
  - subject: "Re: I CAN USE EMAIL AGAIN!!!!!!!!!"
    date: 2007-01-13
    body: "Thanks for the help to set up my email account."
    author: "Anthony DiSalvo III"
  - subject: "Re: Yahoo requires pop auth"
    date: 2003-04-15
    body: "Why when i try to send mesagges via email compairs:\u00b7\t\nIl server ha interrotto la connessione in modo inatteso. Ci\u00f2 potrebbe essere dovuto a problemi relativi al server o alla rete oppure a un lungo periodo di inattivit\u00e0. Account: 'pop.mail.yahoo.it', Server: 'smtp.mail.yahoo.it', Protocollo: SMTP, Porta: 25, Protezione (SSL): No, Numero di errore: 0x800CCC0F\n\nor\n\tImpossibile inviare il messaggio. L'indirizzo di posta elettronica del mittente non \u00e8 stato riconosciuto. L'indirizzo di posta elettronica del mittente \u00e8 \"angelomariella@yahoo.it\". Oggetto '', Account: 'pop.mail.yahoo.it', Server: 'smtp.mail.yahoo.it', Protocollo: SMTP, Risposta del server: '530 authentication required - for help go to http://help.yahoo.com/help/us/mail/pop/pop-11.html', Porta: 25, Protezione (SSL): No, Errore del server: 530, Numero di errore: 0x800CCC78\n\n"
    author: "Angelo Mariella"
  - subject: "Re: Yahoo requires pop auth"
    date: 2003-06-08
    body: "Funnily enough, I came across the same problem with a separate company in Europe. I have ADSL with NTL in the UK, but have just bought a remote dedicated server from a compnay called One and One Internet Ltd. On setting up a brand new domain name and a new e-mail account at that domain name, I was getting the same error message when sending from that account (receiving was OK). BUT all the ideas suggested above were already in place for me. HOWEVER, I decided I WOULD tick the box \"My server requires authentication\" as my experience suggested thst surely it does need it. Sure enough, sending from my own domain name - using ntl's ADSL line - via the server at One and One Internet - now works....although it takes a few seconds to send as the authentication takes place.\n\nHope this helps someone.\n\nDunc, Scotland"
    author: "Duncan Scott"
  - subject: "Re: Yahoo requires pop auth"
    date: 2005-06-09
    body: "when i try to send my email from out look i got  errore html 530 from dell server please mail me about how can i solve the proble.\nthanks"
    author: "rezaur"
  - subject: "Re: Yahoo requires pop auth"
    date: 2003-06-20
    body: "I can still receive messages but seem unable to send messages, what is my repair??"
    author: "Smith Robert K."
  - subject: "Re: Yahoo requires pop auth"
    date: 2004-08-14
    body: "Cause/Description: The reason for this error is because many ISP's (Internet Service Providers) are blocking \"Port 25\" which is the port used to send email. They do this as a security precaution to protect against \"spam\" (unsolicited email). More and more ISP's are doing this everyday (none of us like spam!). Here are just a few of the major ISP's that block Port 25: Earthlink/Mindspring, AT&T, MSN, Verizon, Bellsouth, Roadrunner, and most cable and DSL companies. \n\nSolution: The way around this is to set your SMTP settings to those of your ISP's. For example: \"mail.cox.com\" rather than \"mail.yourdomain.com\". After doing that, you will be sending mail though your ISP's servers rather than ours. Keep in mind that nothing else changes in the way your mail is delivered. Your \"from\" address and your \"reply to\" address will remain the same. \n\nYou will need to contact your ISP and ask for the \"SMTP or out-going mail server address\", to find out exactly what to replace mail.yourdomain.com with. "
    author: "Anand"
  - subject: "Re: Yahoo requires pop auth"
    date: 2003-07-04
    body: "Dear friends... Im going to buy a gun!!!\nThese is my tragic experience!!...\n\nIm in Japan just by one mounth, Now I've got BBYahoo-Japan.\n\nBut.... I riceve my mail with OutLook Express but I cant send!\nAnyone can help me before that I buy a gun????\n\nThese are my error::\n\nImpossibile inviare il messaggio. L'indirizzo di posta elettronica del mittente non \u00e8 stato riconosciuto. L'indirizzo di posta elettronica del mittente \u00e8 \"info@puglianostra.it\". Oggetto 'Test', Account: 'pop.puglianostra.it', Server: 'ybbsmtp.mail.yahoo.co.jp', Protocollo: SMTP, Risposta del server: '521 mail not accepted from this domain', Porta: 25, Protezione (SSL): No, Errore del server: 521, Numero di errore: 0x800CCC78\n\nand\n\nIl server ha interrotto la connessione in modo inatteso. Ci\u00f2 potrebbe essere dovuto a problemi relativi al server o alla rete oppure a un lungo periodo di inattivit\u00e0. Account: 'pop.puglianostra.it', Server: 'ybbsmtp.mail.yahoo.co.jp', Protocollo: SMTP, Porta: 25, Protezione (SSL): No, Numero di errore: 0x800CCC0F"
    author: "Roberto"
  - subject: "Re: Yahoo requires pop auth"
    date: 2004-05-05
    body: "Ciao\nper poter ricevere e mandare mail con outlook devi semplicemente aprire il outlook;dovrai poi modificare il tuo acoount tramite \"strumenti\".segui la procedura ed inserisci pop e smtp quando te lo chiede,questi sono i server di entrata:pop.mail.yahoo.com e di uscita:smtpmail.yahoo.com che dovrai inserire.fatto questo vai a invia e ricevi ed il gioco e' fatto\nBuona fortuna"
    author: "nadim"
  - subject: "Re: Yahoo requires pop auth"
    date: 2006-03-02
    body: "I have problem, cannot send mails to friends...\nA message told me to go to http://help.yahoo.com/help/us/mail/pop/pop-11.html, 25, secure ( ssl ) number server error. 530\nerror number ox800ccc78\n\nSo how can solve this problem.....\n\nplease waiting your help step bye step please.....\n\nThanks"
    author: "asmahan"
  - subject: "Re: Yahoo requires pop auth"
    date: 2008-06-01
    body: "please help"
    author: "steve sands"
  - subject: "Re: Yahoo requires pop auth"
    date: 2003-12-16
    body: "."
    author: "DEGREZ Bruno"
  - subject: "Re: Yahoo requires pop auth"
    date: 2003-12-16
    body: "i can't send messages\ncan you help me please ?"
    author: "DEGREZ Bruno"
  - subject: "Re: Yahoo requires pop auth"
    date: 2004-01-24
    body: "Just setting up Outlook Express. I am receiving e-mails, but can not send any. This error message comes up."
    author: "David Thompson"
  - subject: "Re: Yahoo requires pop auth"
    date: 2004-02-23
    body: "530 authentication required and password not accepted. can you help please??\n"
    author: "steve cairns"
  - subject: "Re: Yahoo requires pop auth"
    date: 2004-02-27
    body: "From the Tools menu, choose \"Accounts.\" \nSelect the \"Mail\" tab. \nSelect your Yahoo! Mail account and click \"Properties.\" \nClick on the \"Servers\" tab. \nCheck the box next to \"My Server Requires Authentication.\" \nClick \"OK.\" \n\ntry this and see if this works."
    author: "mitch"
  - subject: "Re: Yahoo requires pop auth"
    date: 2004-03-14
    body: "send me mail so I  can get this e-mail working."
    author: "Kenneth Rose"
  - subject: "Re: Yahoo requires pop auth"
    date: 2004-04-06
    body: "I keep getting these messages of Errors\nIam not sure is from computer set up or yahoo web site \n\ncould let me know what is going on with these errors\n"
    author: "Julius Yalda"
  - subject: "error in sending mail"
    date: 2004-04-14
    body: "hi\n,\ni am getting following error whaen i send my e mail pl urgently help me\n\nthanks.\n\n\"An unknown error was returned from the SMTP server. Account: 'mail.vsnl.net', Server: 'smtp.vsnl.net', Protocol: SMTP, Server Response: '521 Service not available.', Port: 25, Secure(SSL): No, Server Error: 521, Error Number: 0x800CCC61\""
    author: "aakindia"
  - subject: "Re: error in sending mail"
    date: 2004-08-08
    body: "\"An unknown error was returned from the SMTP server. Server response :556 null byte in data ( Account: mail.comcast.net Error Number 0x800CCC61\""
    author: "Larry Leffall"
  - subject: "Re: error in sending mail"
    date: 2004-08-20
    body: "Well did you figure this out?  I get the same error message (null byte in data) when trying to send my Comcast email through any email program- Outlook, Incredimail, Mozilla, etc.  HELP??!"
    author: "Lyn"
  - subject: "Re: error in sending mail"
    date: 2004-08-20
    body: "just thought you might like to know I found a fix.  It seems to only have\nstarted happening with Comcast smtp server lately.  After much searching it\nis because they are now requiring a ssl connection.\nGo to Outlook and tools menu, use the properties tab of your email account\nand under advanced settings, check the \"the server requires a secure\nconnection (SSL) box.  That's it!\nHope it helps,"
    author: "Larry"
  - subject: "Re: error in sending mail"
    date: 2004-08-20
    body: "Note there are 2(SSL) boxes check both you may have to restart your computer"
    author: "Larry"
  - subject: "Re: error in sending mail"
    date: 2004-08-21
    body: "I did not fully comprehend the solution you describe.  \n\nThe problem occurring is a sender using a windows machine and some version of outlook express is sending from earthlink.net to a recipient at comcast.net and the email bounce back to the sender has the following message:\n\n>     SMTP error from remote mailer after end of data:\n>     host gateway-s.comcast.net [204.127.202.26]: 556 null byte in data\n\nOther senders from other providers do not get bounce backs from the same intended recipient.  Could you explain what the earthlink sender might need to do?\n"
    author: "Frank Doljack"
  - subject: "Re: error in sending mail"
    date: 2004-08-26
    body: "I am encountering the same error 556, null byte in data with some clients that are both using Outlook Express and Windows Xp.  It is with Comcast addresses, in addition to an att.net address.  The odd part is, if the client uses our webmail service to send to the intended recipient they have no problems, so it's localized to Outlook Express.. I'm not sure if our webmail uses ssl or not, but I do not think that enabling ssl will correct the issue... anyone with more info, please post."
    author: "Matt"
  - subject: "Re: error in sending mail"
    date: 2004-11-04
    body: "I'm getting the same message for no understandable reason. However, I'm using Eudora 6.1.1. Might you know of a fix in this program?\n\nThank you in advance for your help.\n\nWill"
    author: "Will Sherwood"
  - subject: "Re: error in sending mail"
    date: 2005-08-22
    body: "I use Eudora with the comcast mailing e-ddress. I was just trying to return a message when I rec'd the \"556 null byte in data\" error message. What's this all about? \n\nNext, I tried sending out an e-mail to a second person and I didn't receive the \"556\" error. \n\nAha, I tried this with mr. number one. Instead of hitting \"reply\" I composed a brand new e-mail to number 1 -- still receiving the \"556 null . . .\" error.\n\nAny help is appreciated. \n\nThanks.  \n\nSkye"
    author: "Skye Wentworth"
  - subject: "Re: error in sending mail"
    date: 2004-08-30
    body: "Out of curiosity, do you happen to have panda antivirus?  \nIf you do, disable it.\n\nI know it sounds goofy, barring your only having the issue with just one domain, but Ive seen this at work before and disabling the antivirus program seems to help."
    author: "heather"
  - subject: "Re: error in sending mail"
    date: 2004-09-01
    body: "Panda is the culprit. I've got a system on the bench and can duplicate the error when using the Autoprotect feature enabled. Disable it, and the messages will go through fine. This is a problem regardless of the email client as I've tried using OE and Netscape. Using a web-based email client bypasses the Panda AV, thus the reason you can use these applications.\n\nHope this helps others that have been beating their heads against the wall with it."
    author: "Keith"
  - subject: "Re: error in sending mail"
    date: 2004-09-06
    body: "Yup.  I've determined the sender installed Panda about the time the problem arose. Seems like this is the root of the problem."
    author: "Frank Doljack"
  - subject: "Re: error in sending mail"
    date: 2004-09-06
    body: "I too have struggling with this problem, and now realize that Panda must certainly be the culprit, as my friends who have been unable to send me emails (they get the \"Null byte in data\" info. when their emails to me get bounced back to them from the Comcast mail server).  I'm now wondering if there's a Panda fix for this, or not.  I've been a Norton Antivirus fan myself, for years."
    author: "Arne Byfuglin"
  - subject: "Re: error in sending mail"
    date: 2004-09-06
    body: "Sorry, I had intended to say that my friends installed Panda (along with a new Windows XP Home, and a new Cable ISP...Cox.net) at the exact time that this started happening.  So.....Panda's gotta be the culprit.  Again, I'm wondering if Panda has fixed this in an update.  I tried poking around at their web site but couldn't find anything that specifically acknowledged this problem and/or a fix for it."
    author: "Arne Byfuglin"
  - subject: "Re: error in sending mail"
    date: 2004-12-25
    body: "Panda was the problem for me...   WinMe, when sending to smtp.comcast.net.  I disabled it, and was able to send email immediately.  :)"
    author: "rich"
  - subject: "Re: error in sending mail"
    date: 2004-09-09
    body: "Hi:\n I am using windows XP sending mail via Netscape/Mozilla and have Panda Anti Virus\nI disabled it and it worked!\nThanks, Nick"
    author: "Nick"
  - subject: "Re: error in sending mail"
    date: 2004-09-21
    body: "err happening to me when using Eudora on Mac OSX -- so I know it's not Panda.  I will go through a different SMTP from comcast for now but if anyone knows a fix for Mac let me know. I have a brand new install - mostly clean except for my time clock software files, and employee payroll stuff, and none of that is running.\n\nThanks if you can help\n\nEric\n\nSundial time clock software\nhttp//www.sundialtime.com\n<a href='http://www.sundialtime.com'>Sundial time clock software</a>"
    author: "Eric Kinney"
  - subject: "Re: error in sending mail"
    date: 2004-09-28
    body: "I have tracked this same issue (on Mac OS X server 10.3.5 - Postfix) and it seems to be related to the signature block (and i think images or formatting in it) when coming from an outlook or outlook express account.  We receive the message when _replying_ to an email from a specific user, and we are relaying through comcast' SMTP server.  So far the only solution has been to remove the entire signature block with the formatting.\n\nHope this helps you all out.\n\nI will be contacting Comcast to complain about the problem, as it is definitely _their_ problem.\n\n-Jon Gleasman\nBottlecap"
    author: "Jon Gleasman"
  - subject: "Re: error in sending mail"
    date: 2004-10-07
    body: "I too am using Mac Eudora (6.1.1, under OS 10.3.5), so I agree that it can not be a Panda virus. I don't think it's Comcast's problem either, as it occurs with my broadband ISP, mail.insightbb.com. Turning on ssl for the smtp server does not solve the problem; in fact, it creates one (since my smtp server does not know what to do with ssl). I agree with Jon Gleasman that it is a problem with Outlook or Outlook Express on Windows, all in my case going through the same Microsoft Exchange V6.0.6556.0 server at Indiana University. I too found removing or deleting part of the signature block allowed the replies to be sent. \n\nIn my case, I'm contacting our network experts; hopefully they will have a solution other than removing all or part of the address block"
    author: "Robert DeVoe"
  - subject: "Re: error in sending mail"
    date: 2004-10-21
    body: "Just got finished troubleshooting the same problem.  I wrote a MFC C++\napplication, using SMTP to send mail & attachments; it worked great for\nmany moons. But, just recently it started giving problems...but only to\nComCast, all the other ISP's we sent attachments to had no problem. We were\nfinally able to use a monitor to capture a good send versus a bad send,\nand discovered the error message was telling the truth.  My program was\nxmitting a fixed length of data in a buffer, and if at the last record,\nthe buffer didn't have the fixed length size of valid data, nulls were being padded in to fill the buffer!  After I modified the program to xmitt just \nwhat was in the buffer, no more ComCast problems!!"
    author: "raykos"
  - subject: "Re: error in sending mail"
    date: 2004-10-27
    body: "I'm having the same problem listed here (mail bounced back with \"556 null byte in data\"), running Eudora on OS X 10.3.5 and Comcast is my provider. I've checked my SSL settings, but they seem to be in order, but that leads me to the second problem: I'm a bit of a spaz with computers, so I still don't know how to fix this issue even after reading all of the helpful tips here. Can anyone explain very s-l-o-w-l-y for an amateur? (I can navigate and follow 'open this' and 'click that' directions).  Thank you!"
    author: "Colleen Rush"
  - subject: "Re: error in sending mail"
    date: 2004-10-29
    body: "After trying to fix/find the fix for error, \"556 null byte in data\", I received this response from Comcast.  It put Outlook Express in travel mode and I can now receive email.  Not sure I like it but it does work for my desktop.\n \n\nThank you for your message concerning the Comcast High-Speed Internet \nservice.\n\nDan, I understand you are experiencing an error using Outlook Express.\n\nReviewing our online forums, I found a fix that should resolve the issue\nyou are experiencing.\n\nDan, you will need to make some minor adjustments to your Outlook \nExpress settings in order to send and receive e-mail. We will need to \nchange your server settings in Outlook Express to that of our travel \nsettings.\n\nFor graphical instructions on how to reconfigure Outlook Express 6.0 \nusing a Windows Operating System for use while traveling, go to http://faq.comcast.net/faq/answer.jsp?name=17717&cat=Email&subcategory=Outlook%20Express\n\nTo reconfigure Outlook Express 6.0 for use when not on the Comcast \nnetwork: \n\n1. Open Outlook Express\n2. Click Tools\n3. Select Accounts\n4. Click the Mail tab. \n5. Highlight the e-mail account you are going to use by clicking on it \nonce\n6. Click Properties\n7. On the General tab, ensure that the email address and Reply address \nare set to your @Comcast.net email address\n8. On the Servers tab, click My Server Requires Authentication and then\nclick Settings. Please note: Do NOT check the \"Log On Using Secure \nPassword Authentication\" box. In the Outgoing Mail Server window, select\nLog On Using, enter your @Comcast.net email address and password and \nclick OK\n9. On the Advanced tab, click This Server Requires A Secure Connection \n(SSL) for both the incoming and outgoing servers\n10. Make sure the port is 465 in the Outgoing mail port number field and\n995 in the Incoming mail port number field\n11. Click Apply\n12. Click OK \n\nYou should now be able to send and receive mail using your Comcast.net \nmail account using Outlook Express\n\n"
    author: "Dan Larsen"
  - subject: "Re: error in sending mail"
    date: 2005-10-15
    body: "Dear sir,\nplease there is a erreor in sending mail.error no ox8ooccc61.please do the the needfull for this."
    author: "travel club"
  - subject: "Re: error in sending mail"
    date: 2005-10-15
    body: "Please complain to your email provider, not here.  This is the newsboard of KDE, a completely unrelated Open Source project. \n\n"
    author: "cm"
  - subject: "Re: error in sending mail"
    date: 2005-11-23
    body: "I am having the same problem. I use Earthlink and my client is Eudora 4.2(old but I'm happy with it.) Sending email to a Comcast.net address gets the\n\"SMTP error from remote mailer after end of data:\nhost gateway-r.comcast.net [204.127.198.26]: 556 null byte in data\"\nerror. If I use webmail, it goes through fine. After reading here, I configured my AVG free edition antivirus NOT to check outgoing mail, and presto, my email gets through."
    author: "Jerry"
  - subject: "Re: error in sending mail"
    date: 2007-01-16
    body: "You have a number (probably a 20) following a space somewhere in the header or attachment name. This translates to hex=null. Get rid of the number, rename the attachment, or put an aplha, like an underscore_ instead of spaces. Poof!"
    author: "t3rock"
  - subject: "Re: error in sending mail"
    date: 2004-08-31
    body: "i got a response on this from comcast as follows (and it worked):\n\nThank you for your message concerning the Comcast High-Speed Internet \nservice.\n\nMany errors in Outlook Express are usually due to incorrect settings. \nItem 10d is the setting which is causing your error.\n\nFor graphical instructions on how to verify your settings in Outlook \nExpress 6.0 using a Windows Operating System, go to http://faq.comcast.net/faq/query.jsp?name=17739\n\nTo verify your settings in Outlook Express 6.0 using a Windows Operating\nSystem:\n\n1.  Open Outlook Express \n2.  Click Tools\n3.  Select Accounts\n4.  Click the Mail tab\n5.  Highlight the account you need to verify by clicking on it once\n6.  Click Properties \n7.  On General tab, verify and correct, if necessary: \na. Mail Account name should be mail.comcast.net, but can be anything\nb. Name should be your name\nc. E-mail address should be your e-mail address: username@comcast.net \nd. The Organization and Reply Address fields should be blank\ne. The box next to \"Include this account...\" must be checked\n8.  Click Apply if any changes were made on the General tab\n9.  Click the Servers tab \n10. In the Server Information section, verify and correct, if necessary:\n\na. My incoming mail server is a \"POP3\" server*\nb. Incoming mail (POP3): mail.comcast.net\nc. Outgoing mail (SMTP): smtp.comcast.net\nd. Put check mark in the box \"my server requires authenication.\n11. In the Incoming Mail Server section, verify and correct, if \nnecessary:\na. Account name should be your username. This is your e-mail address \nwithout the @Comcast.net part\nb. Password should be populated with asterisks (*) Type over them with \nyour known working password\nc. Remember Password is the only box that should be checked\n12. Click Apply if any changes were made on the Servers tab \n13. Click the Connections tab \n14. The \"Always connect to this account using\" box should be checked and\nLocal Area Network should be selected from the pull down menu\n15. Click Apply if any changes were made on the Connections tab\n16. Click the Advanced tab\n17. Verify and correct, if necessary: \na. Outgoing mail (SMTP) port is 25\nb. Incoming mail (POP3) port is 110\nc. Server Timeouts is set at 1 minute\nd. All of the boxes on this tab should be unchecked\n18. Click Apply if any changes were made on the Advanced tab\n19. Click OK\n\n*If not, this account needs to be deleted because it is not configured \nfor use with a POP3 server and that particular configuration cannot be \nchanged. Click Cancel to get back to Internet Accounts, click Remove to \ndelete this account, then click Add to add a new mail account."
    author: "Sun"
  - subject: "Re: error in sending mail"
    date: 2004-09-05
    body: "I am using Mozilla Build #2004083008\nI get this error of \"null byte in data\" only when I try to send a Word file through the Comcast SMTP server.  TXT, PDF, ZIP, etc., are just fine but not DOC.\n\nIt there is a requirement for authentication then be sure that big brother is watching."
    author: "Bruce"
  - subject: "Re: error in sending mail"
    date: 2004-11-14
    body: "I'm having the exact same problem. Did you ever get it figured out?"
    author: "Bob Isman"
  - subject: "Re: error in sendisadng mail"
    date: 2004-09-20
    body: "sdf"
    author: "asdf"
  - subject: "Re: error in sending mail"
    date: 2004-10-28
    body: "In reference to your mail , we would like bring to your attention that you need to connect to the TATA Indicom dial-up service in order to send mails using our SMTP server through any email client.\n \nKindly note that when you are connected to any other ISP/Cable service you need to use the respective SMTP server address provided by the respective service provider\n \n"
    author: "thrikkur"
  - subject: "Re: error in sending mail"
    date: 2006-07-21
    body: "This is just not right! I am unable to send mails from my office account (VSNL) when I'm at home(BSNL). What you are doing is unfair and I believe that many people would agree with me. "
    author: "Rahul"
  - subject: "An error in sending mail through Outlook Express 5"
    date: 2004-11-08
    body: "An unknown error was returned from the SMTP server. Account: 'mail.vsnl.in', Server: 'SMTP.vsnl.in', Protocol: SMTP, Server Response: '521 Service not available.', Port: 25, Secure(SSL): Yes, Server Error: 521, Error Number: 0x800CCC61"
    author: "Sreejith S Sharma"
  - subject: "Re: An error in sending mail through Outlook Expre"
    date: 2004-12-24
    body: "I am also getting this error, can you let me know the answer to this problem\nat my e-mail address. I shall be thankful to you.\nPawan"
    author: "Pawan Kumar"
  - subject: "Re: An error in sending mail through Outlook Expre"
    date: 2005-01-10
    body: "Im having the same problem with smtp.vsnl.net\n\nDid you guys figure out a solution? Kindly help me. Thanks."
    author: "jimit"
  - subject: "Re: An error in sending mail through Outlook Expre"
    date: 2005-10-18
    body: "sir we are unable to send the messages through the outlook express and we are getting the error as\n(An unknown error was returned from the SMTP server. Account: 'mail.vsnl.net', Server: 'smtp.vsnl.net', Protocol: SMTP, Server Response: '521 Service not available.', Port: 25, Secure(SSL): No, Server Error: 521, Error Number: 0x800CCC61)"
    author: "saravanan v n"
  - subject: "Re: An error in sending mail through Outlook Expre"
    date: 2005-10-18
    body: "Please complain to your email provider, not here. This is the newsboard of KDE, an Open Source project that is completely unrelated to your email problems. \n\n"
    author: "cm"
  - subject: "Re: An error in sending mail through Outlook Expre"
    date: 2005-11-26
    body: "An unknown error was returned from the SMTP server. Account: 'mail.vsnl.net', Server: 'smtp.vsnl.net', Protocol: SMTP, Server Response: '521 Service not available.', Port: 25, Secure(SSL): No, Server Error: 521, Error Number: 0x800CCC61"
    author: "Rajasekar"
  - subject: "Re: An error in sending mail through Outlook Expre"
    date: 2005-11-30
    body: "Two emails I sent came back with Protocol Error: Error is 550 Permission denied RCPT to: (?) What do I do? "
    author: "ddgn"
  - subject: "Re: An error in sending mail through Outlook Express 5"
    date: 2005-11-14
    body: "plesse select smtp authentication to solve this problem in acoount setting\n\n\n"
    author: "nadir"
  - subject: "Re: An error in sending mail through Outlook Express 5"
    date: 2006-02-16
    body: "I am using Tata Indicom walky for internet but some time i come across the following error while sending the mail plz give me a solution.\n\n\"An unknown error was returned from the SMTP server. Account: 'mail.vsnl.net', Server: 'smtp.vsnl.net', Protocol: SMTP, Server Response: '521 Service not available.', Port: 25, Secure(SSL): No, Server Error: 521, Error Number: 0x800CCC61\""
    author: "Raju"
  - subject: "Re: An error in sending mail through Outlook Express 5"
    date: 2006-02-16
    body: "!!! 521 Service not available !!!"
    author: "cl"
  - subject: "Re: error in sending mail"
    date: 2005-02-13
    body: "Dear AAkindaia,\n\nThe error which your are getting while sending the mail using the mail client software is having a fix. what you have to do is just.\nTools>Account>mail>Properties>servers>Myserver requires Authentication(click) & Click Settings> Log on using> Give a/c name as your user id & pw click ok. Try this your problem will fix.\n\nRegards\nManish"
    author: "Manish"
  - subject: "Re: error in sending mail"
    date: 2005-02-21
    body: "Hi Manish,\n\nI tried everything i could but unable to send emails. I can only receive emails.\n\nNisha"
    author: "Nisha"
  - subject: "Re: error in sending mail"
    date: 2005-03-03
    body: "Did any of you contact vsnl helpdesk for the same or is it solved as of now?\n\n- SB"
    author: "SB"
  - subject: "Re: error in sending mail"
    date: 2005-04-02
    body: "I am also getting the same error.I took the support from the vsnl also but that also not helpful.I am sending u the vsnl helpdesk number.If u get any solution u please reply me on this mail(devendra_it@yahoo.co.in).\n\nVSNL no. is 08056600121"
    author: "Devendra kumar"
  - subject: "Re: error in sending mail"
    date: 2005-04-27
    body: "Sir,\nsince so many days i am not able to send the message frome microsoft out look express.\ni have already used,\nmail.vsnl.net and smtp.vsnl.net but no response came.\nplease reply and oblige,\nthanking you,"
    author: "BOMI"
  - subject: "Re: error in sending mail"
    date: 2005-05-26
    body: "pls send a mail\n"
    author: "arun"
  - subject: "Re: error in sending mail"
    date: 2005-06-08
    body: "top urgent\n\nrds\nkriba "
    author: "uday"
  - subject: "Re: error in sending mail"
    date: 2005-06-08
    body: "Hello?  \n\nDo you all mistake this KDE news(!) board for a support forum for some broken internet service provider? \n\nHint:  It's not.  Not only is this extremely off-topic here, your chances to get the reply you're looking for are also infinitesimally small. \n\n"
    author: "cm"
  - subject: "Re: error in sending mail"
    date: 2005-07-07
    body: "hi \npls rpl me through mail urg .\n\n An unknown error was returned from the SMTP server. Account: 'mail.vsnl.net', Server: 'smtp.vsnl.net', Protocol: SMTP, Server Response: '521 Service not available.', Port: 25, Secure(SSL): No, Server Error: 521, Error Number: 0x800CCC61"
    author: "sadrul"
  - subject: "Re: error in sending mail"
    date: 2005-08-13
    body: "I am facing same problem"
    author: "nachiketpatil"
  - subject: "Re: error in sending mail"
    date: 2005-08-25
    body: "An unknown error was returned from the SMTP server. Account: 'bgl.vsnl.net.in', Server: 'smtp.vsnl.in', Protocol: SMTP, Server Response: '521 Service not available.', Port: 25, Secure(SSL): No, Server Error: 521, Error Number: 0x800CCC61\n\nThe above error message I am getting.\n"
    author: "Ponnappa"
  - subject: "Re: error in sending mail"
    date: 2005-09-12
    body: "Hey guys!\n\nhttp://internet.vsnl.in/mailservices/popmailsettings.htm\n\nhttp://internet.vsnl.in/mailservices/outlook-express.htm\n\ncheck out!\nmay it will help!"
    author: "Yashodhan"
  - subject: "Re: error in sending mail"
    date: 2005-09-16
    body: "Please respond immediately since I am unable to send mails.\n\n+"
    author: "Soundar"
  - subject: "Re: error in sending mail"
    date: 2005-09-26
    body: "An unknown error was returned from the SMTP server. Account: 'Saharia Group', Server: 'smtp.vsnl.net', Protocol: SMTP, Server Response: '521 Service not available.', Port: 25, Secure(SSL): No, Server Error: 521, Error Number: 0x800CCC61"
    author: "Suman Dutta"
  - subject: "Re: error in sending mail"
    date: 2005-10-15
    body: "please do the needfull for this"
    author: "TRAVEL CLUB"
  - subject: "Re: error in sending mail"
    date: 2005-10-15
    body: "Please complain to your email provider, not here.  This is a newsboard of KDE, a completely unrelated Open Source project. \n\n"
    author: "cm"
  - subject: "Re: error in sending mail"
    date: 2005-11-19
    body: "An unknown error was returned from the SMTP server. Account: 'mail.vsnl.net', Server: 'smtp.vsnl.net', Protocol: SMTP, Server Response: '521 Service not available.', Port: 25, Secure(SSL): No, Server Error: 521, Error Number: 0x800CCC61"
    author: "srinath"
  - subject: "Re: error in sending mail"
    date: 2006-01-06
    body: "I am Not avil to send the e.mail"
    author: "Dr.Govind Varshney"
  - subject: "Re: error in sending mail"
    date: 2006-03-18
    body: "dear sir \nI am getting below error as my email add is inaindia@vsnl.com\nI gave pop23 as mail.vsnl.com its working but smtp smtp.vsnl.com is working \ni enabled my server requires athentication then also I am not able to send \nwhat may be the problem pls chk it send reply immediately to the above mentined address \nthanks and regards \n\nravi v v \nThe SMTP server returned an error. Account: 'Ina India', Server: 'smra.sancharnet.in', Protocol: SMTP, Server Response: 'Not Allowed', Port: 25, Secure(SSL): No, Error Number: 0x800CCC60"
    author: "ravi "
  - subject: "Re: error in sending mail"
    date: 2006-03-18
    body: "An unknown error was returned from the SMTP server. Account: 'Ina India', Server: 'smtp.vsnl.com', Protocol: SMTP, Server Response: '521 Service not available.', Port: 25, Secure(SSL): No, Server Error: 521, Error Number: 0x800CCC61\npls give the solvation for the problem\n"
    author: "ravi "
  - subject: "Re: error in sending mail"
    date: 2006-06-03
    body: "i am unable to send the mail. at the time of sending the mail, it shows the message given hereinabove."
    author: "brij chatrath"
  - subject: "Re: error in sending mail"
    date: 2006-10-19
    body: "U should change the smtp settings as 'mail.vsnl.net'\nbcoz the pop & smtp should be 'mail.vsnl.net' try to change the settings u will be able to send the mails"
    author: "Manju"
  - subject: "Re: error in sending mail"
    date: 2007-02-05
    body: "The Problem seems to be internet connection.\n\nAre you trying to send email from VSNL account, using some other company's internet, like BSNL etc.\n\nIn that case the mail will not be sent. You have to be conected to internet using VSNL account to send email thru vsnl."
    author: "Rajat Jain"
  - subject: "Re: error in sending mail"
    date: 2007-08-23
    body: "> The Problem seems to be internet connection.\n\nThere's no problem if it's a good connection.\n\n> Are you trying to send email from VSNL account, using some other company's      > internet, like BSNL etc.\n\nWhat, if so? BSNL gonna eat VSNL? GRRR.\n\n> In that case the mail will not be sent.\n\nWhy? The mail swallowed by ISPs?\n\n> You have to be conected to internet using VSNL account to send email thru     > vsnl.\n\nOh! Oh! So VSNL is something terrible. Great monster. Oops, I never understood why I could sign-in to yahoo account, possibly because I'm using yahoo connection. </shrudder>"
    author: "catena"
  - subject: "Re: error in sending mail"
    date: 2007-02-13
    body: "I RECEIVED THIS ERROR NOTICE INFORMING ME NONE OF MY EMAILS ARE BEING SENT BY OUTLOOK....HOW DO I GET OFF THIS BLACKLIST....PLEASE HELP MY!!!! BUSINESS RUNS ON EMAIL.\n\n\nThe SMTP server returned an error. Account: 'Jason Hartwyk', Server: 'mail.reliancerelocation.com', Protocol: SMTP, Server Response: '+OK <15091.1171324173@mail9.opentransfer.com>', Port: 110, Secure(SSL): No, Error Number: 0x800CCC60"
    author: "j"
  - subject: "Re: error in sending mail"
    date: 2009-01-17
    body: "When I am trying to send email from my dataone ID, I am receiving the following notification.\n\nPlease help me to solve this.\n\n\n\nThe SMTP server returned an error. Account: 'wmma.dataone.in', Server: 'wmra.dataone.in', Protocol: SMTP, Server Response: 'Not Allowed', Port: 25, Secure(SSL): No, Error Number: 0x800CCC60"
    author: "N. Shahina"
  - subject: "Re: Yahoo requires pop auth"
    date: 2004-09-21
    body: "iam unable to sent my mail"
    author: "neo weiting"
  - subject: "Re: Yahoo requires pop auth"
    date: 2004-10-21
    body: "Please ,now I ask you for an pop authemtification, as a sender; emanuel_poppel@yahoo.co.uk\nThanks,\nProf.Emanuel Poppel\n"
    author: "Prof.Dr.Poppel Emanuel"
  - subject: "Re: pop3"
    date: 2005-08-26
    body: "can't recieve or send on internet explorer"
    author: "beverly schneider"
  - subject: "Re: Yahoo requires pop auth"
    date: 2005-09-06
    body: "you cannot send email because yahoo is now requiring you to authenticate your message before sending it.this is because of email SPAM. to check if your yahoo authentication is enable try to log in to your yahoo account using the internet explorer.notice if you try to compose a new message then send it,yahoo will ask you to authenticate it(like asking you to input the letters or numbers you see on a black box). if it does then write a letter to yahoo tech support to disable that setting. it fixes my yahoo account."
    author: "chubi"
  - subject: "Re: Yahoo requires pop auth"
    date: 2006-01-31
    body: "In an attempt to control unsolicited email (spam), some Internet service providers will block port 25. All email sent via the Internet is routed through port 25, the port used for communication between an email client and an email server. Port 25 blocking allows ISPs to block spam sent out through their networks. If you are experiencing technical difficulties when sending via your email client (such as Outlook), then your ISP may block port 25. For these cases, you will need to change the SMTP port from 25 to 587 in your email client.  \nIf this is not what you needed to know, check out our Online Help Desk. Visit it any time by logging into your Yahoo! Mail account and clicking on the \"Help\" link located in the upper right-hand corner.  \n\nIT WORKS PASS IT ON\n\n"
    author: "Nick"
  - subject: "Try this it worked for me"
    date: 2006-01-31
    body: "In an attempt to control unsolicited email (spam), some Internet service providers will block port 25. All email sent via the Internet is routed through port 25, the port used for communication between an email client and an email server. Port 25 blocking allows ISPs to block spam sent out through their networks. If you are experiencing technical difficulties when sending via your email client (such as Outlook), then your ISP may block port 25. For these cases, you will need to change the SMTP port from 25 to 587 in your email client.  \nIf this is not what you needed to know, check out our Online Help Desk. Visit it any time by logging into your Yahoo! Mail account and clicking on the \"Help\" link located in the upper right-hand corner.  \n"
    author: "Nick"
  - subject: "Re: Yahoo requires pop auth"
    date: 2006-09-26
    body: "what was the solution to this error message?"
    author: "sarah"
  - subject: "Re: Yahoo requires pop auth"
    date: 2008-02-19
    body: "This solved my problem of \"Server Error: 530, Error Number: 0x800CCC78.\"\n\n\nOpen up Outlook Express\nClick on \"Tools\"\nClick \"Accounts\"\nClick \"Mail\"\nClick on your Email account in the dialog box\nClick on \"Properties\"\nClick on \"Servers\"\ncheck the box \"My server requires authentication\"\n\nP.S.: Make sure your password is correct."
    author: "ofatmarine"
  - subject: "Re: Yahoo requires pop auth"
    date: 2008-05-08
    body: "non riesco a configurare outlok express ne window live mail...aiutatemi\n"
    author: "elisabetta"
  - subject: "Re: Yahoo requires pop auth"
    date: 2008-06-11
    body: "I can't send photos.  Message said \"the rejected e-mail add was'jamacky94@yahoo.com.' Subject cannot retrieved my license; Account:'pop3'server 'smtp',Protocol: SMTP server response: 530 authentication required-for help go to http//help.yahoo.com/help/us/mail/pop/pop-11.html,' Port 25 Secure (SSL): No Server.(2 mssgs) \"Your server has unexpectedly terminated the connection. Possible cause for this include server problems, network problems,or a long period of inactivity. Subject question, study;3928 username:Respondent,password:LJNBM;Account:'pop3'server 'smtp',Protocol: SMTP,Port 25,Secure(SSL): No. Error Number 0x800cccoF.  Hoping for agood responses thank you."
    author: "maria luisa cerdena"
  - subject: "Re: Yahoo requires pop auth"
    date: 2008-07-27
    body: "I was conserned about setting up my email and could use alittle help please reply if you could help me in this matter."
    author: "Michael Villary"
  - subject: "Re: Yahoo requires pop auth"
    date: 2008-11-10
    body: "this is test e-mail"
    author: "noniiz"
  - subject: "Re: Yahoo requires pop auth"
    date: 2009-01-06
    body: "Nobody"
    author: "Luciano Simone"
  - subject: "Re: Yahoo requires pop auth"
    date: 2003-12-17
    body: "How do you obtain authentication for pop ?"
    author: "Gabriela Perez"
  - subject: "Re: Yahoo requires pop auth"
    date: 2004-12-10
    body: "Annswer needed  Please  \n"
    author: "Betty "
  - subject: "Re: Yahoo requires pop auth"
    date: 2005-02-01
    body: "server rejects sender email address.\n530 authentication required.\nserver error 530\n"
    author: "khaled"
  - subject: "Re: Yahoo requires pop auth"
    date: 2006-04-10
    body: "Cannot send scanned attachment for E-Mail"
    author: "Frank Yuhas"
  - subject: "Re: Yahoo requires pop auth"
    date: 2002-08-15
    body: "I need 500 authentication.  Please email me with it. I certainly would be thankful for it.  sincerely  Colleen"
    author: "Colleen (Kimball) Davis"
  - subject: "Re: Yahoo requires pop auth"
    date: 2002-10-01
    body: "LIKE IT SAYS IN THE MESSAGE, try reading the following web page:\n\nhttp://help.yahoo.com/help/us/mail/pop/pop-11.html"
    author: "Anonymous"
  - subject: "Re: Yahoo requires pop auth"
    date: 2003-11-17
    body: "HELP"
    author: "Cynthia Cleveland"
  - subject: "Re: Yahoo requires pop auth"
    date: 2002-12-13
    body: "how do you authenticate using a client machine through a proxy machine?\nproxy server machine can send and receive email just fine however the client machines can only receive mail not send because of authenticating.\nI'm figuring i have to somehow make the server do the authenticating for it.  I'm using analog x proxy server and see no where to do that."
    author: "frustrated"
  - subject: "Re: Yahoo requires pop auth"
    date: 2002-12-15
    body: "howhttp://help.yahoo.com/help/us/mail/pop/pop-11.html\n do i send mail?"
    author: "Cynthia Todd"
  - subject: "Re: Yahoo requires pop auth"
    date: 2004-07-24
    body: "I am not able to view yahoo/hotmail mail using analog proxy. pl. let me know if u have any sloution"
    author: "pradeep sachdeva"
  - subject: "Re: Kmail SMTP error help!"
    date: 2002-01-10
    body: "Depending on your ISP see what is their outgoing mail SMTP server:\nSet your Outgoing SMTP to your ISP's SMTP. Do not change your POP settings. \n\nFor your POP setting for yahoo it should be \"pop.mail.yahoo.com\"(in USA)\n\nFor your SMTP setting for your server it should be something in the lines of\n\"SMTP.your_isp.com or net or what ever your ISP root domain is.\n\nYour ISP might not let you send mail through another ISP."
    author: "Eagle"
  - subject: "Re: Kmail SMTP error help!"
    date: 2002-03-05
    body: "http://HELP.YAHOO.COM/help/us/mail/pop/pop-11.html"
    author: "DORU_USA"
  - subject: "Re: Kmail SMTP error help!"
    date: 2002-03-12
    body: "cannot send out email, dont know how to correct problem, please instruct....."
    author: "richard kelp"
  - subject: "Re: Kmail SMTP error help!"
    date: 2002-03-13
    body: "If you are trying to send SMTP via Yahoo! Then you'll need to upgrade to KDE3.  KMail in 2.2 doesn't support SMTP auth, which is what Yahoo! is now requiring."
    author: "Terry G"
  - subject: "Re: Kmail SMTP error help!"
    date: 2002-07-08
    body: "\nMy yahoo account works fine, only when I try to send anything from OUTLOOK \n\nEXPRESS, it shows error--530 authentication required--\n\nI receive messages through OUTLOOK EXPRESS BUT CAN\"T SEND THEM! \n\nHELP PLEASE,\nDiane "
    author: "Diane Scott"
  - subject: "Re: Kmail SMTP error help!"
    date: 2003-05-05
    body: "If you find out before I do let me know.  I have the exact same problem.\n\nTerry"
    author: "Terry"
  - subject: "Re: Kmail SMTP error help!"
    date: 2003-11-27
    body: "Did you find out the problem about the error 530 in outlook express?\nPlease, answer to me.\nMariJoxe"
    author: "MariJoxe"
  - subject: "Re: Kmail SMTP error help!"
    date: 2004-04-15
    body: "A 530 error means that your email server requires authentication.  Your email client settings must be adjusted to do this.\n\nHere's the best resource I've found, including step-by-step instructions for many email programs to provide authentication: \n\nhttp://www.canada.com/members/help.html#530 -- gives information about authentication, and includes a link to the following: \n\nhttp://www.canada.com/members/help.html#SMTPguide -- guides to changing your email client to use SMTP Authentication. \n\nHope this helps!  It took only a minute or two, and did the trick for me (I had the same 530 authentication error problem).  "
    author: "Doug D"
  - subject: "Re: Kmail SMTP error help!"
    date: 2004-09-20
    body: "Thank you thank you thank you!!!! I have had trouble with my outlook express and comcast mail and that canada site helped me. I had been trying to send mail for a month and kept getting the 530 authentication message too. when I went on comcast online help my online tech told me I had to uninstall everything and do it all over again. I thought it was crazy cause my mail was working fine up until a month ago. So I searched the web and found your help info and it worked all I really needed to do was check a box that wasn't checked before. THank you Thank you...\n\nMaria"
    author: "Maria Holbrook"
  - subject: "Re: Kmail SMTP error help!"
    date: 2004-11-06
    body: "I'm having the same problem. Which box did you check?"
    author: "CACNLC"
  - subject: "Re: Kmail SMTP error help!"
    date: 2006-10-11
    body: "Can you please tell me which box you checked for authentication?  The Canada site is down and I'm desperate.\nThanks so much in advance!\n"
    author: "Mike"
  - subject: "Re: Kmail SMTP error help!"
    date: 2005-12-14
    body: "well, i sort of having a problem accessing the site u gave.. I  have the same error and am using smtp.yahoo server.. it asks for authentication and was wondering if the problem is the using this server itself.. thanx\n"
    author: "waleed"
  - subject: "Re: Kmail SMTP error help!"
    date: 2007-10-29
    body: "The canada site doesn't work its under construction. please post the steps."
    author: "ju"
  - subject: "Re: Kmail SMTP error help!"
    date: 2006-08-08
    body: "I checked the canada site, but it is now not working. Are there any other places I can go to resolve the 530 error? \n\nThanks, \nMarcy"
    author: "Marcy"
  - subject: "Fix"
    date: 2007-03-22
    body: "In outlook, go to Tools->Email Accounts\nCheck the \"View or Change E-mail Accounts\"\nClick Next\nSelect the account to modify, I've only got one, so I selected that one and clicked \"Change\"\nClick \"More Settings\"\nClick the Outgoing Server Tab\nCheck the \"My outgoing server requires authentication\"\nClick the Radio button labled \"Use same settings....\"\nThen Click okay.\nYou can test it using the test feature, or just finish up by following the prompts.\n\nHope this helps..\n"
    author: "Josh Stephens"
  - subject: "Re: Fix"
    date: 2007-08-24
    body: "I spent hours with comcast.  This worked thanks for posting the solution"
    author: "Ron Sabala"
  - subject: "Re: Fix"
    date: 2007-09-07
    body: "Great tip! Got us sending e-mails :)\n\nBarbara e Pedro "
    author: "Barbara e Pedro Leite"
  - subject: "Re: Fix"
    date: 2007-09-07
    body: "Great tip! Got us sending e-mails :)\n\nBarbara e Pedro "
    author: "Barbara e Pedro Leite"
  - subject: "Re: Fix"
    date: 2007-10-25
    body: "Thank you friend. I wanted to know the setting for 2 months but you help me in 2 minuits. Thanks. Keep Helping."
    author: "Palash Karmakar"
  - subject: "Re: SMTP error help! - Cannot send via Outlook Exp"
    date: 2005-08-17
    body: "In Outlook Express go to Tools -> Accounts and choose the right account\nGo to Properties\nGo to Server tab\nCheck the box that says \"My server requires authentication\"\nIn Settings \"use same as my incoming mail server\"\nThis fixed the problem for me.\nMaria Holbrook - thanks for the tip..."
    author: "Prakash"
  - subject: "Re: SMTP error help! - Cannot send via Outlook Exp"
    date: 2006-02-01
    body: "Yes...more love to you - thank you for taking the time to post this - much appreciated!!!!"
    author: "Tammy"
  - subject: "Re: SMTP error help! - Cannot send via Outlook Exp"
    date: 2006-05-10
    body: "Thank you so much the goofs at yahoo wanted me to unistall everything. this fixed things in under a minute."
    author: "Burke"
  - subject: "Re: SMTP error help! - Cannot send via Outlook Exp"
    date: 2006-06-08
    body: "Thanks SO much!  I had problems with the Canada site too and these instructions fixed the problem for me too!"
    author: "Cherice"
  - subject: "Re: SMTP error help! - Cannot send via Outlook Exp"
    date: 2006-07-31
    body: "Thanks for the info.  I had the 530 error and found you by googling the problem.  Nailed it and the fix took 30 seconds."
    author: "Tom"
  - subject: "Re: SMTP error help! - Cannot send via Outlook Exp"
    date: 2006-10-23
    body: "Thank you very much for answering the previous questions about the 530 authentication problem.  It helped me, too.\n\nKind Regards,\nRick"
    author: "Rick"
  - subject: "Help, cannot get e-mail to work via outlook"
    date: 2006-11-15
    body: "this is the message I receive:\n\nYour server has unexpectedly terminated the connection. Possible causes for this include server problems, network problems, or a long period of inactivity. Account: 'mail.comcast.net (1)', Server: 'mail.comcast.net', Protocol: SMTP, Port: 25, Secure(SSL): Yes, Error Number: 0x800CCC0F"
    author: "Billy"
  - subject: "Re: SMTP error help! - Cannot send via Outlook Exp"
    date: 2007-02-12
    body: "Thanks for providing something so amazingly simple.  Now I not only know what a 530 Authentication message means, I can fix it!"
    author: "Tom"
  - subject: "Re: SMTP error help! - Cannot send via Outlook Exp"
    date: 2007-04-16
    body: "Thank you. Your explanation helped me in seconds after hours of frustration.\n\nMichelle Z.\n"
    author: "Michelle Z"
  - subject: "Re: SMTP error help! - Cannot send via Outlook Exp"
    date: 2007-05-30
    body: "This is a KDE group, not an Outlook Express one you cum guzzling fucktard"
    author: "Matt"
  - subject: "Re: SMTP error help! - Cannot send via Outlook Exp"
    date: 2007-08-15
    body: "Unbelievable!!! I've been searching for this fix for a while.  Worked like a charm.  Can't thank you enough.  Joseph Pingel"
    author: "Joseph Pingel"
  - subject: "Re: Kmail SMTP error help!"
    date: 2005-09-19
    body: "Wow thanks for someone posting this.  I was about to lose my mind when I couldnt send email from my Verizon account all weekend.  "
    author: "wendy "
  - subject: "Re: Kmail SMTP error help!"
    date: 2005-10-20
    body: "Perrrrrfect!  That took like 30 seconds to fix.  Thanks a million."
    author: "JKhai"
  - subject: "Re: Kmail SMTP error help!"
    date: 2005-11-13
    body: " BIgggggggggggggggg kissssssssssssssssssssssssss for you \n\nThanx alot"
    author: "khaled zaid"
  - subject: "Re: Kmail SMTP error help!"
    date: 2006-05-04
    body: "Thanks - I was going crazy.  TG for Google and your post."
    author: "Outlook user"
  - subject: "Re: Kmail SMTP error help!"
    date: 2006-05-04
    body: "actually what you need to to do is set up remote access.\n\nto do this click on tools ,accounts,mail tab, highlight your \naccount in the white box,click properties,servers tab,\ncheck the box that says My Server Requires Authentication.\nclick on Settings,put a dot next to log on using and type in\nyour complete e-mail address and password.then click ok\nthen click on the advanced tab at the top then put a check in\nboth boxes the server requires a secure connection then change\nthe out going mail smtp to port # 465 the incoming should already \nbe 995 click apply then close and try your e-mail :) :) :):)"
    author: "bcherry"
  - subject: "Re: Kmail SMTP error help!"
    date: 2006-06-23
    body: "I want to say thankyou!!!!!!!!!!!!! a million times and then some. I was so frustrated trying to figure out what happened to my forever working outlook--and just when I was visiting and needed to do email it did this authentication thing. It seems to be working well. Thankyou for taking the time to put this up. carol"
    author: "carol"
  - subject: "Re: Kmail SMTP error help!"
    date: 2006-11-09
    body: "Wow! For 6 months I've been unable to send email, and after searching Comcast help, and talking to friends who I thought knew a thing or two about computers, I Googled 530 Authentication, and fixed it in 30 seconds. Thanks bcherry!"
    author: "Kathy"
  - subject: "Re: Kmail SMTP error help!"
    date: 2006-07-09
    body: "I live in the states and my error message is this;\n\nYour server has unexpectedly terminated the connection. Possible causes for this include server problems, network problems, or a long period of inactivity. Account: 'mail.comcast.net', Server: 'smtp.comcast.net', Protocol: SMTP, Port: 25, Secure(SSL): No, Socket Error: 10053, Error Number: 0x800CCC0F"
    author: "elmcity"
  - subject: "Re: Kmail SMTP error help!"
    date: 2006-08-04
    body: "I am having the same problem. (0x800CCC0F).  It happened last week after several days of rain & then mysteriously corrected itself for 5 or 6 days & then reappeared yesterday when we had a little rain.  Is this coincedental (that's my Guess) Please help John"
    author: "John"
  - subject: "Re: Kmail SMTP error help!"
    date: 2006-10-01
    body: "I got stuck today with the 530 authentication deal.  Thanks so much for the instructions below.  They worked like a charm.\n\nAmy"
    author: "Amy Quick"
  - subject: "Re: Kmail SMTP error help!"
    date: 2007-05-24
    body: "I just contacted comcast with the same problem here was there answer:\nOutlook Express is designed for any email account, not specifically any one provider or type of account. Due to recent spam restrictions and changes to our server policies, third party email accounts can no longer send through our outbound email server\n"
    author: "wendy"
  - subject: "Re: Kmail SMTP error help!"
    date: 2007-05-24
    body: "I just contacted comcast with the same problem here was there answer:\nOutlook Express is designed for any email account, not specifically any one provider or type of account. Due to recent spam restrictions and changes to our server policies, third party email accounts can no longer send through our outbound email server\n"
    author: "wendy"
  - subject: "Re: Kmail SMTP error help!"
    date: 2007-07-15
    body: "Thank you Thank you Thank you.\n\nI am down in Oakland CA (from Oregon) working security for the garbage strike and haven't been able to email out. I went to all the servers helps etc and none of them would give me the answer to this 530 Authentivation BS. But this blog had me hitting the send button in seconds. Thank you "
    author: "Sarah Rodgers"
  - subject: "Re: Kmail SMTP error help!"
    date: 2002-04-04
    body: "I get the same error with my ISP mail account\nBTW it was working with KDE 2.2.1 \n\nAlso Konqueror wants to connect to CVS all the time now, well I do not have CVS installed at all... \n\nAny hacks? \nthanks all in advance"
    author: "alex"
  - subject: "Re: Kmail SMTP error help!"
    date: 2002-10-19
    body: "puedo mandar  unos mensajes y otros no me sale error 530.0x800cc79"
    author: "isabel blanco "
  - subject: "Re: Kmail SMTP error help!"
    date: 2003-06-05
    body: "there  is allways a problem on yahoo launch wats up with that error 15 for da last time \nthanxs kirsty "
    author: "welshy"
  - subject: "Invalid Extension fix"
    date: 2004-01-16
    body: "I got a problem kinda similar that said this:\n\nSending failed:\nThe server did not accept the sender address.\nThe server responded: \"Invalid extension (mySurname) \"\nThe message will stay in the 'outbox' folder until you either fix the problem (e.g. a broken address) or remove the message from the 'outbox' folder.\nNote: Other messages will also be blocked by this message, as long as it is in the 'outbox' folder\nThe following transport protocol was used:\nwww.ntlworld.com\n\nI fixed it by:\n\nGoto configure/identities\nNow select your profile and click modify\nYou should now see your profile...\nSelect the General tab (if it isn't already selected)\nNow to solve this enter an email address that you want the server to respond to.\n\nThat should do it, the problem was that the server needs an address to respond to should something go wrong and its also needed before any mail can be sent by the SMTP protocol.\n\nI knew there was no problem with the ntlworld server because I could telnet into it by typing:\n\ntelnet smtp.ntlworld.com 25\n\nFrom there I could send mail by following the command protocol (type HELP).\n\nAnyway I hope this helped, what pissed me off was that KMail didn't explain this in simple terms for a newbie like me, so simple to sort too - just make the email address a pre-requisite for inbound SMTP accounts."
    author: "Adam Chidgey"
  - subject: "Re: Invalid Extension fix"
    date: 2004-01-20
    body: "I also am getting the same error but in my case, I have my reply email in my General tab. What could be the issue? \n\nlky"
    author: "Lkintrea"
  - subject: "Re: Kmail SMTP error help!"
    date: 2004-07-28
    body: "This message is already old, but I have the same type of problem.\nI can send E-mails with KMail, but only without attachment.\nWith attachment the protocol types out as described in the previous message.\n\nWas this problem solved?\n\nThanks\n\nHenk"
    author: "Henk Koopmans"
  - subject: "Nice White Mouse Pointer for KDE and X windows"
    date: 2001-09-03
    body: "friends I don't know how to share the nice white mouse pointer with you all (KDE users), I am very happy that this mouse pointer increases the beauty of KDE somewhat. This is just a small gift to all of you nice people :)\n\nIf you don't have root previliges: copy this cursor.pcf.gz file to '~/.kde/share/fonts/override', and restart kde.\n\nIf you have root previledges the you can copy this 'cursor.pcf.gz' file to '/usr/X11R6/lib/X11/fonts/misc' folder. \n\nthis mouse pointer I think should be the default mouse pointer of KDE shouldn't it be?\n\nHope you like my little contribution. And where should I put other mouse pointer I will create, eg. white-large, white-extra-large, black, black-large, and black-extra-large? Thanks!"
    author: "Asif Ali Rizwaan"
  - subject: "Re: Mouse Pointers"
    date: 2001-09-04
    body: ">Hope you like my little contribution. And where should I put other mouse pointer I will create, eg. white-large, white-extra-large, black, black-large, and black-extra-large?\n\nYou should subscribe to the KDE-Devel mailing list and send a mail asking if someone could add your cursors to KDE and make a control panel to change the cursor.  Or, better yet, you could make the control panel yourself and send it in!  The KDE developers love people who take the initiative themselves!  It would be a good project to learn KDE development."
    author: "not me"
  - subject: "Re: Mouse Pointers"
    date: 2001-10-29
    body: "mouse cursors"
    author: "rashad al halabi"
  - subject: "How to make a 1.000.000.000 seconds bug ?"
    date: 2001-09-03
    body: "This makes me wonder: How do you at all make a 1.000.000.000 seconds bug ? If it were a 2^32 seconds bug or 2-to-the-power-of-some-other-figure I would understand. But 1.000.000.000 ?\n\nAnyway; I have recently made KMail my default mail program (the IMAP support finally works) and I am really satisfied (so far ;-). Also: The coming unification of the implementation of KMail and KNews is really great news. Sometimes in the future that would be the foundation of a real KDE Outlook killer."
    author: "Claus Rasmussen"
  - subject: "Re: How to make a 1.000.000.000 seconds bug ?"
    date: 2001-09-03
    body: "It's simple, take the number as a string, not as a number. Then limit the length of the string and it just won't fit anymore after 999.999.999. The reason you take it as a string is simply because you want to save it (in a non-binary way)."
    author: "Daniel Naber"
  - subject: "Re: How to make a 1.000.000.000 seconds bug ?"
    date: 2001-09-03
    body: "That seems pretty stupid, the c++ string allows any length.  Bad programming- did the author not see his code being used in 2001?  Can you say evolution?"
    author: "ac"
  - subject: "Re: How to make a 1.000.000.000 seconds bug ?"
    date: 2001-09-03
    body: "it was due to the structure of the index files which only have space for 9 digits for the date (and therefore the problem when reading back in) and nothing to do with the code actually storing the date in kmail itself."
    author: "Aaron J. Seigo"
  - subject: "Re: How to make a 1.000.000.000 seconds bug ?"
    date: 2001-09-05
    body: "I dont't know whether we have a special fucked up version of Outlook here at work of whether I am to stupid, but this damn Outlook here sucks beyond believe.\n\nBye\nAlex"
    author: "aleXXX"
  - subject: "Re: How to make a 1.000.000.000 seconds bug ?"
    date: 2001-09-11
    body: "That version is what microsoft likes to call \"retail\"."
    author: "John"
  - subject: "Meeting scheduled for epoch 1000000000"
    date: 2001-09-05
    body: "In my companies\u00b4 calendar there ist a meeting scheduled on\nSunday from 3:40AM to 3:50AM for all staff.\n\nI wonder who\u00b4s gonna show up :-)"
    author: "Matthias Lange"
  - subject: "knode 0.4 contains also some 10^9 related bug?"
    date: 2001-09-09
    body: "Knode-0.4 used to work great for me as a newsreader, I used to sort the articles by ascending order of date, but I just discovered that all articles from today (September 9, posted after 10^9 seconds) are above the oldest articles (if I change the order to descending, they are below the oldest articles): so it looks like knode-0.4 can't deal with this date either, so is this a known knode-0.4 bug? Is the current knode (the one that comes with 2.2) OK?\n\n<p>It's not a major bug, because most of my NG's are emptied after a few weeks, and then all articles are from after 10^9 and they are sorted correctly once again (BTW it's time to upgrade for me)."
    author: "Bas vd Meer"
---
If you did not heed our <A HREF="http://dot.kde.org/985599243/">warning from six months ago</A> and are still using KMail &lt; 1.0.29.1 (from KDE 1.x), you are advised to please upgrade to KDE 2.2 (or KDE 2.1.x) immediately.  In about a week the bizarre KMail 1,000,000,000th second bug will strike, which can cause corruption of your mail folders.  Details about the bug are available <A HREF="http://kmail.kde.org/bug.html">here</A>.
<!--break-->
<P>
The KMail developers recommend that users update to KDE 2.2, which includes a newer KMail with many other bugs fixed and many new features.  According to KMail developer <A HREF="mailto:daniel.naber@t-online.de">Daniel Naber</A>,
</P>
<BLOCKQUOTE>
The new version of KMail will use old mail files without problems.
However, once the new version is installed one should not switch back to
the old version because of changes in the index files and configuration
file. Just to be sure, we recommend that users make a backup of their mail
folder (~/Mail) and compress all folders before switching to the new
version.
</BLOCKQUOTE>