---
title: "People of KDE: Jing-Jong Shyue"
date:    2001-02-01
authors:
  - "Inorog"
slug:    people-kde-jing-jong-shyue
comments:
  - subject: "Re: People of KDE: Jing-Jong Shyue"
    date: 2001-02-01
    body: "And happy new \"year of the snake\"! (Even if it's a little late...)\n\nThat's another topic, but as a reminder, David Faure will do a KDE presentation in Bruxelles on Sunday February 4th. For those who are interested, see:\nhttp://osdem.org/program/\n\n    Regards"
    author: "Nicolas"
---
It is time again for a friendly meeting at the virtual interview table that <a href="mailto:tink@kde.org">Tink</a> entertains on the <a href="http://www.kde.org/people/people.html">People Behind KDE</a> site. This week's guest is <a href="http://www.kde.org/people/shyue.html">Jing-Jong Shyue</a>, member of KDE's Traditional Chinese translation team. His (and his team's) work in KDE is particularly valuable given the unwitting bias of Western/European cultures sometimes present in software projects such as ours.


<!--break-->
