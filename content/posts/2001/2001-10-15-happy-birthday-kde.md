---
title: "Happy Birthday, KDE!"
date:    2001-10-15
authors:
  - "zapalotta"
slug:    happy-birthday-kde
comments:
  - subject: "Happy Birthday"
    date: 2001-10-15
    body: "My I be the first to say happy birthday!!!!!!!!!!!!!!!!!!!!  KDE!!!"
    author: "hoju"
  - subject: "Congrats."
    date: 2001-10-15
    body: "...and may I be the second. Happy birthday KDE!  Apparently you share the same birthday as <a href=\"http://slashdot.org/article.pl?sid=01/10/14/2024232&mode=nested\">OpenOffice</a>, although OpenOffice is only 1 year old.\n\nKeep up the good work KDE developers, its most appreciated.\n\nacm"
    author: "acm"
  - subject: "Re: Congrats."
    date: 2001-10-15
    body: "Gah, sorry.. here is the link:\n\nhttp://www.openoffice.org/about_us/birthday.html"
    author: "acm"
  - subject: "Congratulation!"
    date: 2001-10-15
    body: "(OK, 43 minutes too late ;-)\n\nFive years?! Really?! Never realised that it was such a long time.\n\nBut many things changed: I remember, when I installed my first 0.4 binary sometime in '97 (you know: Kalles famous c't-article): What a difference between this and fvwm95. This was the first thing I installed alone. I was used to _work_ with SunOS und Linux but installation-things??\n\nThese days I started learning Linux. My programming skills were not good enough to help the project - I was one of these silent users. Today I know much more about Linux and KDE. I try to help and try to promote KDE-Linux.\n\nSo:\n \nHappy Birthday, KDE!"
    author: "Thorsten Schnebeck"
  - subject: "Kool :) but..."
    date: 2001-10-15
    body: "Why KDE is not referred as Kool Desktop Environment, even when It's name was chosen by Matthias E.? I find is very stupid in the tip of the day that \"K\" in \"KDE\" stands for nothing! Let me correct it, it stands for \"Kool\" (cool), the first posting of M.E. has the title \"New Project: Kool Desktop Environment (KDE)\".\n\nHappy Birthday to KDE and every one cheers! BTW I like the new slogan \"KDE 3.0: ready for the Desktop\". I appreciate KDE teams' hard work and dedication. Thank you!"
    author: "Asif Ali Rizwaan"
  - subject: "Re: Kool :) but..."
    date: 2001-10-15
    body: "If you read the rest of that newsgroup, you'll see that they decided not to name it kool desktop environment (good thing too ;)"
    author: "Wiggle"
  - subject: "Re: Kool :) but..."
    date: 2001-10-15
    body: "They changed it to \"K-Rad\", right?  =)"
    author: "Ranger Rick"
  - subject: "Re: Kool :) but..."
    date: 2001-10-15
    body: "haha,  no kidding.\nI'm sure that would really help the corporate acceptance.."
    author: "Leo Spalteholz"
  - subject: "Re: Kool :) but..."
    date: 2001-10-15
    body: "K stands for Kool.\nIn Kool, K stands for nothing..."
    author: "cristian"
  - subject: "Because..."
    date: 2001-10-16
    body: "Kool is a trademark for a brand of cigarettes in the United States."
    author: "Unkool"
  - subject: "Happy Birthday"
    date: 2001-10-15
    body: "Haaapppyyyy Birthday to Yoooou, Haaaapppyyy Birthday to Yooou, Haaapppyyy Birthday Dear KDE, Haaapppyyy Birthday to Yooooou. And many mooooooore! :-D"
    author: "Timothy R. Butler"
  - subject: "Re: Happy Birthday"
    date: 2001-10-15
    body: "For it's a jolly good desktop, for it's a jolly good desktop, for it's a jolly good desktop... and so say all of us!"
    author: "Rob Kaper"
  - subject: "Pictures of the celebrations!"
    date: 2001-10-15
    body: "A couple of dutch KDE developers including yours truly decided to celebrate this big day together. It wouldn't be a true KDE event without my digital camera, so <a href=\"http://ezri.nl.capsi.co\nm/~cap/digicam/2001-10-14-kde5/\">here's an impression</a>. Enjoy!\n"
    author: "Rob Kaper"
  - subject: "Re: Pictures of the celebrations!"
    date: 2001-10-15
    body: "I thought we agreed you weren't going to mention my bowling score ;-)"
    author: "Nielx"
  - subject: "Re: Pictures of the celebrations!"
    date: 2001-10-15
    body: "I hate to be pedantic but I didn't actually mention your score. :)"
    author: "Rob Kaper"
  - subject: "Re: Pictures of the celebrations!"
    date: 2001-10-15
    body: "hej... that was in Nijmegen.... I also live in nijmegen...\nwhere were the big banners ???\ndamn... would have like it to meet you guys..."
    author: "Robbin Bonthond"
  - subject: "Re: Pictures of the celebrations!"
    date: 2001-10-15
    body: "Hmm, too bad You missed our party. It was really great!! We should do it again in the near future!!\nWhere were the banners?\nWell there weren't any. We just looked in all the KDE-mailinglists and irc for Dutch KDE-developers/volunteers, and invited them to the party.\n\nKind regards, Rinse"
    author: "Rinse"
  - subject: "Re: Pictures of the celebrations!"
    date: 2001-10-16
    body: "Great pictures. I'll post mine asap...\nAnd happy birthday to KDE."
    author: "Alfons Hoogervorst"
  - subject: "All good wishes to you...."
    date: 2001-10-15
    body: "Congratulations!!! Happy Birthday, KDE !!!!!!!!!!!!!!!!! You changed my live to the better side... !!!!"
    author: "Frank Seidel"
  - subject: "Happy Birthday, but..."
    date: 2001-10-15
    body: "I use KDE on most of my Linux systems (including my Dell Laptop). Its great, however, its hard to figure out how to do an upgrade - The install information points me all over the place and what worked for a previous update doesn't seem to work for newer updates. I use KDE almost everyday and have for the last 2 1/2 years. I really like it and want to keep it current. Help me do that!"
    author: "Jens Moller"
  - subject: "KDE 1.92"
    date: 2001-10-15
    body: "attached is a screenshot of an early pre-2.0 release. :)"
    author: "Charles Samuels"
  - subject: "Re: KDE 1.92"
    date: 2001-10-15
    body: "Reminds me, I still need to do a checkout of one of those releases and reintroduce the System style as it was back then, with those awesome red/green checkboxes.\n\nI know, it would not be as useful for colourblind people or those from cultures where green/yes and red/no aren't the default meaning, but that doesn't mean it couldn't be in kdeartwork."
    author: "Rob Kaper"
  - subject: "Re: KDE 1.92"
    date: 2001-10-15
    body: "I'll have to see if I can make a few grabs today of one of the several KDE 1.1 machines I still administer ;)"
    author: "raindog"
  - subject: "Re: KDE 1.92"
    date: 2001-10-15
    body: "Does anyone have any pre-1.0 screenshots? Those're the ones I'd like to see :)"
    author: "Rakko"
  - subject: "GNOME....."
    date: 2001-10-15
    body: ".... is still better"
    author: "a guy who likes GNOME"
  - subject: "Re: GNOME....."
    date: 2001-10-15
    body: "I agree, but only because like you, I collect core files and buffer overflows."
    author: ".....Troll snatcha"
  - subject: "Re: GNOME....."
    date: 2001-10-15
    body: "I used to troll around these forums, but now that I actually tried KDE, I've started trolling in Gnotices\n\n\nGNOME IS SUX\nKDE ROOLZ\n\na former GNOME abuser, rehabilitated at gnome users anonymous"
    author: "Wiggle"
  - subject: "Re: GNOME....."
    date: 2001-10-15
    body: "yeah okay. KDE Rox, but Gnome is mightyful (is that a word?) also.\nThere's no point in saying, \"this sux, this rox\".\nBoth Gnome and KDE developers are working very hard.\nSo they deserve all the credit.\nI must add to this that KDE continues to grow with an unseen speed.\nBut then again, loozing eazel was a big disaster for gnome.\nNevertheless the gnome developers continue to work, so they deserve respect."
    author: "iscarioth"
  - subject: "Re: GNOME....."
    date: 2001-10-15
    body: "You don't seem to understand. The whole KDE vs. GNOME thing is a stupid user debate. Developers do not care. That is why GNOME has posted an article on KDE's 5th birthday as well. We should work towards integration, and not fight over everything. Hopefully, when Berlin becomes stable, we will be able to make a new toolkit, combining the advantages of both (with wrappers for the different opinions, of course). I use gnome, but that doesn't mean I don't have KDE installed, that I hate KDE, or that I am in any way using an inferior or superior desktop (I just like the interface better)."
    author: "C. Evans"
  - subject: "Re: GNOME....."
    date: 2001-10-16
    body: "Ok, when Berlin is stable...\nand fast enough for daily work.\nSo, let's say in approx. 10 years ;-)\nAnd then we will have new toolkits, since it's quite different, everything can be 3D, translucent and so on.\nMaybe a real new GUI experience.\n\nBye\nAlex"
    author: "aleXXX"
  - subject: "Re: GNOME....."
    date: 2001-10-16
    body: "I agree that it's a stupid thing to have a GNOME vs. KDE discussion, both desktops are superior to those of that\nsilly product from western US.\n\nHowever, since I installed (or, I tried to install) Nautilius from Eazel I can't use GNOME anymore"
    author: "Franz"
  - subject: "Oh my goddess not again..."
    date: 2001-10-16
    body: "Even Gnotices wish KDE a happy birthday! Doesn't that proof that the entire GNOME vs KDE war doesn't even exists?"
    author: "Stof"
  - subject: "grow up to Mac OS X"
    date: 2001-10-15
    body: "I love KDE! A pity nobody has succeeded yet in porting it to Mac OS X... Therefor, I 'have to' run windowmaker now...\n\nCongratulations KDE, and I hope you may grow up to Mac OS X desktops (and later perhaps even the majority of Windows desktops?)"
    author: "Rene"
  - subject: "Re: Happy Birthday, KDE!"
    date: 2001-10-15
    body: "Five years already? Whoa! Who's got the cake?"
    author: "Whitehawk Stormchaser"
  - subject: "Happy birthday"
    date: 2001-10-15
    body: "Only 5 years and already this good. That is amazing !"
    author: "Sander"
  - subject: "Happy Birthday!!"
    date: 2001-10-15
    body: "To KDE!!"
    author: "Anonymous Coward"
  - subject: "wouweeee .. 5 (in words f i v e) years ?"
    date: 2001-10-15
    body: "Irre.... crazy, in 5 years it has come so far.... HAPPY BIRTHDAY KDE !"
    author: "Thomas"
  - subject: "Cheerz!"
    date: 2001-10-15
    body: "congratulation to all the coders, designers, developers and their girlfriends! lotsa great work, guyz! Keep it going, and windoze is gonna be history soon\n\nbut please, please please... it's so unfriendly to install/upgrade..."
    author: "Divine"
  - subject: "Re: Cheerz!"
    date: 2001-10-15
    body: ">but please, please please... it's so unfriendly to install/upgrade...\n\nThat's the distro's fault. There's really no good way that KDE could provide any sort of installation type tool seperate from xyz distro's packaging system without causing massive mayhem, since the distros are packaging their own binaries anyways. KDE provides only source."
    author: "Carbon"
  - subject: "To the next five years!"
    date: 2001-10-15
    body: "I've missed that original posting because there wasn't a usable news feed here in Namibia. I remember discussing LyX with a friend in Austria in the begin of 1997 by email and then he mentioned that Matthias started another project: KDE. I wasn't much impressed at first but after going to www.kde.org and downloading KDE (note: compiled it on a 486-66 with 16MB) I tried it out and was hooked immediately. It completely changed my view of X programming. I had tried it a coupla times before but always gave up on it. Now, with KDE and Qt, it was so easy to write applications with a pretty GUI. Thanks to TT and the handful of original KDE developers who took this up!\n\nTo the next 5 years of KDE development after which, I guess, this beast will no only be able to notify me when my tea is ready but to make the tea."
    author: "Uwe Thiem"
  - subject: "Re: To the next five years!"
    date: 2001-10-15
    body: "I swear I had my hands on a script which did exactly that, communicate to your eTea-Machine via the parport and print the status messages in KNewsTicker's scrolltext. Pity I can't remember where it is (probably put it in the same dir as the focus-follows-mind script which says 'You really sure you want to type there, making a fool out of yerself?' whenever you've been caught typing in the wrong window)."
    author: "Frerich Raabe"
  - subject: "HAPPY BIRTHDAY"
    date: 2001-10-15
    body: "i am so impressed by the great kde-community. you are so great.\ni love kde and i am using it every day as my default method, handling my machine :-)\n\nand...of course... i am using kde in my trainings as well.\n\nso keep up the goog work.\n- gunnar"
    author: "gunnar"
  - subject: "We love you!"
    date: 2001-10-15
    body: "Just wanted to say that everyone in my office loves KDE!  Not to do any gnome bashing, but you guys are far, far ahead of the game.\n\nWe wish you all the best of luck!\n\nHappy well deserved birthday KDE!\n\n-mofo"
    author: "mofo"
  - subject: "Great Job!!!"
    date: 2001-10-15
    body: "I'm your fan.\n\nThanks a lot."
    author: "Alex"
  - subject: "Gnotices"
    date: 2001-10-15
    body: "They even congratulate KDE on the Gnotices web page. I think this is great as it shows that the people really involved with these two desktop projects are much more mature than the trolls on both sides. Very honorable move, Gnotices!"
    author: "AC"
  - subject: "Happy birthday....now just"
    date: 2001-10-15
    body: "All KDE needs to do now is to slow it a bit, or perhaps do it like th linux kerneland have a bunch that maintainsa branch to get it a bug free as possible. then new features can be added in the experimental tree and when it is released, move into a temporary bug crushing mode, once it has gotten pretty stable move to the next experimental and give old branch into maintanence.....the old way has definatly given KDE all it has but it has also made it buggy. I think that the move could be done after 3.0 and all you would need is to give the maintanence to one person on each subproject.\n\ngood job anyway guys, we just need it more stable."
    author: "Jeremy"
  - subject: "Re: Happy birthday....now just"
    date: 2001-10-16
    body: "> All KDE needs to do now is to slow it a bit, or perhaps do it like th linux > kerneland have a bunch that maintainsa branch to get it a bug free as possible\n\nActually there are \"a bunch\" of people working on the KDE_2_2_BRANCH and fixing as many bugs as possible. Contributors welcome :)"
    author: "Carsten Pfeiffer"
  - subject: "Re: Happy birthday....now just"
    date: 2001-10-16
    body: "yeah, but how long are they going to maintain it? are they going to do like Alan Cox and maintain the code until it is super stable? are we going to see KDE 2.2.10 when KDE 3.4 is out?"
    author: "Jeremy"
  - subject: "Re: Happy birthday....now just"
    date: 2001-10-16
    body: "Probably not ;-)\nAll bug fixes which are applied to the \"stable\" branch are also applied to the \"experimental\" branch. I don't think many developers will continue to use KDE 2.x once KDE 3.0 is released.\nWe will have a KDE 2.2.2, maybe a 2.2.3, and that will probably be all.\nThen look forward to KDE 3 :-)\n\nBye\nAlex\n\nP.S. 2.2.2 will be be very stable and contain very much bug fixes as far as I can say"
    author: "aleXXX"
  - subject: "Re: Happy birthday....now just"
    date: 2001-10-16
    body: "well, thats good to know. when I read about going from 2.2.1 to 3.0 I was thinking \"but the bugs are not cruhed hear yet\" at the rate that bugs are crushed by you guys, 2.2.2 would be very stable and 2.2.3 would be almost perfect I am sure.\n\nI hope 3.0 is faster than 2.2.x...and KHTML needs a speed up, its starting to act like MSHTML.\n\nits sort of funny, you look eveywhere in the OSS community and you see realy good features, you just don't see all the realy good features in one place."
    author: "Jeremy"
  - subject: "congratulations..."
    date: 2001-10-15
    body: "I cheer for all free software projects.  Here's to another five years and the opportunity for more cooperation between the GNOME and KDE projects.\n\njamin p. gray\nFifth Toe Coordinator, GNOME 2 Release Team"
    author: "Jamin P. Gray"
  - subject: "Re: congratulations..."
    date: 2001-10-15
    body: "Indeed, what with the menu formats and organization being nearly the same, it's kind of surprising that hasn't been integrated already. What's with that?"
    author: "Carbon"
  - subject: "Re: congratulations..."
    date: 2001-10-15
    body: "I'm pretty much brand new to Linux. I still have a dual boot setup but decided to ditch windows completely last Saturday. Today I am going to install a new drive and eliminate Windows so that I can sell my 98 and NT 4, if they will bring anything.\n\nAnyway, I like the idea of cooperation between Gnome and KDE, but even as a new user I see a lot of value in each project doing their own thing. I can't believe it is so easy to get cross-compatibilty when they each seem to use significantly different toolkits and have differing ideas about the best way to implent things.\n\nI'd hate to think either project would lose functionality if they spent time trying to be comnpatible instead of working on making their own project outstanding in it's own right. I can use Gnome apps no problem so far even though I mainly run KDE. I don't need drag and drop or between them or other stuff like that.\n\nI think compatibility is overated."
    author: "John Marttila"
  - subject: "Re: congratulations..."
    date: 2001-10-15
    body: "That's just the thing, it's not as though features of KDE or GNOME will go away if compatibility is implemented. Things like dnd and menu compat wouldn't be very hard, since there is already a standard for the first, and they both already use the same format for the second. It's not as though they would need to be source compatible."
    author: "Carbon"
  - subject: "Re: congratulations..."
    date: 2001-10-15
    body: "Could be. I just wanted to put in my two cents that in the cries for interoperability I wanted to cry out that this newbie feels he just doesn't need it that much. Maybe as I get more time as a fully Linux person I'll feel differently.\n\n I only know a small amount about programming and so I don't the issues involved. Used to script things on my windows machine and write small programs in my mech. engineering work, but decided to switch to Linux because I am getting more into Python and want to begin learning c and c++ and I don't want to be confused as to why my programs break when Microsoft decides to change something.\n\nWell, goodbye for now. I am installing a new hard drive and reformatting the old one so as to eliminate Windows NT and 98 from my sytem for good (yes, for VERY good). So I won't be around for a few hours (if all goes well, a day or two if all goes poorly)\n\nThanks again to all Gnu/linux programmers! I've found an OS!"
    author: "John Marttila"
  - subject: "Re: congratulations..."
    date: 2001-10-16
    body: "> Things like dnd and menu compat wouldn't be very hard, since there is already a standard for\n> the first, and they both already use the same format for the second.\n\nActually, KDE uses that XDND standard and AFAIK so does GNOME. I can drag media files from Konqueror to XMMS just fine."
    author: "Rob Kaper"
  - subject: "You made Linux possible for this newbie."
    date: 2001-10-15
    body: "I looked at Red Hat Linux several years ago, but it was too hard for this newbie. Then I heard of KDE, but Red Hat had religion and didn't support it, so Linux was still a hard option. When I discovered Mandrake, with KDE included, all of my Christmasses had come at once and I've loved Mandrake with KDE ever since.\n\nI can't thank all of the good people involved in these projects enough. They're making great history."
    author: "Bob Buick"
  - subject: "Congratulations!! Great Job guys!"
    date: 2001-10-15
    body: "KDE is my desktop of choice!!"
    author: "Trevor"
  - subject: "Congratulations!"
    date: 2001-10-15
    body: "Hi,\n\nit's obvious that KDE has come a long way in those five years. I have got a debian box with KDE, and recently I noticed that my only browser is konqueror!\n\nA few years ago this would not have been possible, since you always had to have netscape 4.7 available just in case something did not work. But now I do not need another browser anymore. It is great that I do not have to rely on bloated motif or ugly xforms or athena widget library applications.\n\nThe only thing I am missing is klyx. I really loved it in kde 1.*, but now there is no up-to-date version available. The current version of lyx still uses xforms, and it is so ugly that I just can't bear it... \nI understand that lyx will be toolkit-independent soon, but since kde will soon be a defacto standard one wonders wether this is really nessecary.\n\nWhat I would like to see for the future: I would *love* to see KDE and Qt move to a platform-independent binary format so that you could have a binary of a KDE application for all platforms supported by KDE. It does not have to be C#, but I think it might be a good idea to move to some kind of virtual machine execution. This does not mean big performance losses, and it would make distributing, installing and upgrading kde applications so much more convenient.\n\nregards and keep up the good work,\n\n    Androgynous Howard"
    author: "Androgynous Howard"
  - subject: "Re: Congratulations!"
    date: 2001-10-16
    body: "Actually, klyx in KDE's cvs did compile (and even work :) with KDE2 and currently KDE3. There was just no release.\n\nI'm wondering if I should fix a few issues and release a new package..."
    author: "Carsten Pfeiffer"
  - subject: "Re: Congratulations!"
    date: 2001-10-16
    body: "That depends on when the toolkit-independent lyx will become available. If it is not much work, please release a new package. KLyx is a real killer application especially for the scientific community. Latex with a decent frontend is much better than word!\n\nregards,\n\n    Androgynous Howard"
    author: "Androgynous Howard"
  - subject: "Re: Congratulations!"
    date: 2001-10-16
    body: "I have seen a lot of people asking for KLyx.\nIt doesn't have be in the official release of KDE, just brush it up and release it on apps.kde.com."
    author: "reihal"
  - subject: "Re: Congratulations!"
    date: 2001-10-17
    body: "PLEASE do!!\n\nI loved klyx in kde 1.x.\nand now I am forced to use the old xforms lyx :-("
    author: "Wiggle"
  - subject: "Re: Congratulations!"
    date: 2001-10-17
    body: "Oh hell, yes.  Seems you are not aware that many of us have been clamouring for KLyX for a *LONG* time now.  The LyX project all but killed KLyX by misleading Matthias into thinking they would do it themselves properly.  Nothing came of it and they are probably now porting to GNOME."
    author: "KDE User"
  - subject: "Feliz cumplea\u00f1os"
    date: 2001-10-16
    body: "Felicidades, llevo con kde practicamente desde que empec\u00e9 con linux y de eso ya hace + de 3 a\u00f1os. Siempre instalo las \u00faltimas versiones y puedo decir que cada d\u00eda os superais, seguid en esa linea.\n\n\ncheli"
    author: "cheli"
  - subject: "Original Vision"
    date: 2001-10-16
    body: "I think what's amazing is how closely KDE today resembles Matthias' original vision as outlined in this posting.  It's rare for a project so expansive to stick so closely to its original goals, especially in the Bazaar.\n\nAnd, looking at this post, you can see why KDE is the best desktop for UNIX: it's original goals had nothing to do with technical whizbang, and everything to do with creating a usable, useful interface that was both attractive and practical.  And that's exactly what KDE is, which is why I love it."
    author: "Adam Wiggins"
  - subject: "Congratulations, and THANK YOU!"
    date: 2001-10-16
    body: "Been using KDE since 1.1 (well, not exactly an \"early adopter\") and continue to be amazed at the fast progress, the professional approach to software design and user orientation, the great localization support, .... the sheer BEAUTY of it!\n\nThank you for bringing this not only to the Linux community but to the world in general!\n\nI do think that Linux is not quite ready for the masses (you just can't tell the average user to _recompile_ stuff to resolve an issue or even expect him to understand what a \"package dependency\" is), _but_ KDE is one of the main ongoing efforts that will eventually help to reach that goal.\n\nAnd Linux _has_ to be become usable for everyone because it's our only chance to maintain freedom in the software world.\n\nThank you to all KDE developers."
    author: "Joerg Sauer"
  - subject: "For 5 years"
    date: 2001-10-16
    body: "Yoy made very very good work and don't stop .\nI Started with 1.0.\nUse only (almost) aplication with K before rest.\nI love KDE i will for ever.\nRun KDE, Run ;-)\n\nBest Regards"
    author: "..."
  - subject: "Happy Birthday!!"
    date: 2001-10-16
    body: "Happy Birthday KDE, I am now a Gnome user, but I have always loved both desktops.  I first used kde when Gnome was in alpha.  Once Gnome stabalized I switched over.  I've gone back to KDE briefly off and on, and am always impressed with new progress you give us gnome people something to work towards (we are coming pretty close).  I love the give and take from the two projects, and without the other one, I don't think either one of the projects would be where they are today.  Happy Birthday once again!!"
    author: "John"
  - subject: "Competition between KDE & Gnome"
    date: 2001-10-17
    body: "I don't understand when people say that their is no need for competition between Gnome and KDE. Their obviously has to be -- it's only natural. Their market the same *Nix people who are looking for a GUI. True, most (including myself), have both Gnome and KDE. But their's just something is GNOME that's overly repulsive (another story on that ;).\n\nCurrently, we can estimate that the *Nix desktop environment is divided at the moment (no one really knows for sure). But what if it was a different story? Say 9 out of every 10 *Nix GUI users used Gnome exclusively?\n\nFace it, deep down inside everyone wants to  be a winner. Ultimately, only one project will be sucessfull based quality of code, financials, and other unknown variables. \n\nRemember, heathly compeition is good.\n\nIn 9-11 years, which one will be still around? KDE or Gnome?\n\nI want back some KDE Pride! Note that I'm not saying integeration is good -- like say have one audio toolkit instead of esound and ARTS.\n\nWhats wrong with saying 'KDE Rulez, Gnome SUX' ?\n\nHow about a little loyalty for a change people?\n\nThanks,\nmy 0.02\u0080"
    author: "MacPlus"
  - subject: "Competition between KDE & Gnome, P. II"
    date: 2001-10-17
    body: "\" I want back some KDE Pride! Note that I'm not saying integeration is good -- like say have one audio toolkit instead of esound and ARTS.\"\n\nSorry for the poor spelling/grammar! That sentace should read:\n\nI want back some KDE Pride! Note that i'm not saying saying that integration isn't  good -- like having 1 audio toolkit instead of both esound and ARTS.\n\nI would also like to continue to elaborate:\n\nIts just more work for the small developer, plus, more importantly, it furthers the divide betteren KDE-apps and Gnome-apps -- which is ultimately very, very bad. Their should not be a K(app_name) and a separate G(app_name). Take, for example, Knapster - Gnapster, [the former] Kaim - Gaim and the _countless_ others that fit this mask. Now that's what I call senselessly redundant. Please, the open-source community needs to stop this and konsolerdate. We need to be good open-source koders, making the best possible software solutions on _any_ platforms -- this will win new konverts. Anyone want guess how many Office-wanna be's their are? \n\nIn short, the KDE and Gnome teams need to be very pround of what their do and do best -- which is make user interfaces (Gnome: keep trying though, one day you'll be good enough:). Other developers need to do what their do best, not repeat K-apps and G-apps.\n\nThanks [again], for listening.\n---\nProud Mac OS X and KDE developer"
    author: "MacPlus"
  - subject: "Re: Competition between KDE & Gnome, P. II"
    date: 2001-10-17
    body: "I tend to agree with you, the purpose of gnome is done (to make Qt gpl'd), but you cannot expect gnome developers to end all of their hard work just like that. So I think the only way for gnome to die off were to make KDE so kick ass that no one used gnome anymore (well, there are a lot more kde users now, but still there is a loyal gnome following). this is of course natural, we do not see stuff like tkdesk or fvwm being developed anymore!!"
    author: "Wiggle"
  - subject: "Re: Competition between KDE & Gnome"
    date: 2001-10-17
    body: "I feel pretty strongly on this : that is complete bull STDOUT. There are no 'markets', because these are NOT businesses selling products! The big difference between a situation where your competition analysis would be relevant and the current one is that even if one desktop is used more then another, a project will only stop when it's developers _want_ to stop it. There's no way that KDE can eliminate the GNOME project, nor vice versa.\n\nThere is some competition, mainly because of different philosophies on how to approach the same problems. But, saying stuff like \"KDE Rulez, Gnome Sux\" only indicates that you don't understand what's going on : the developers are not trying to fight each other, they are doing similar things at the same time simply because of differences of opinion as to how to best get around certain problems. \n\nYes, healthy competition is good, but healthy competition is exactly what we've got going on : sportsmanlike competition. _Not_ \"KDE Rulez, Gnome Sux\", and _not_ \"Yeah, this new project will kill off KDE for sure!\".\n\nBTW, I'm not attempting to troll, but I'm simply totally against MacPlus's opinion here. Just my 0.02 as well."
    author: "Carbon"
  - subject: "Happy Birthday"
    date: 2001-10-17
    body: "You folks have done a very nice job all over the last years. KDE is my favourite desktop environment since pre releases of 1.0.\n\nThank you very much.\n\nMarkus"
    author: "Markus Pietrek"
  - subject: "Thank you very much !"
    date: 2001-10-18
    body: "Hello KDE Team,\n\nCongratulation !\n\nI have just upgraded from KDE 1.12 ( Yes, I mean 1.12 ) to 2.21\nand I just can say: Wow !\n\nI love the Konqueror. It is so fast. \nI love the KDE Tools: e.g. Kate, KDevelop\nKOffice is great.\n\nKeep up your great work.\n\nBye,\nCarsten"
    author: "Carsten Ziepke"
  - subject: "Thank you very much !"
    date: 2001-10-18
    body: "Hello KDE Team,\n\nCongratulation !\n\nI have just upgraded from KDE 1.12 ( Yes, I mean 1.12 ) to 2.21\nand I just can say: Wow !\n\nI love the Konqueror. It is so fast. \nI love the KDE Tools: e.g. Kate, KDevelop\nKOffice is great.\n\nKeep up your great work.\n\nBye,\nCarsten"
    author: "Carsten Ziepke"
  - subject: "None - King"
    date: 2001-10-20
    body: "Well I have used it for 5 weeks and a like it already better that Windows 98.  Any improvement will just widen the gap.\n\nBill"
    author: "Bill Berggren"
---
Five years ago, on October 14th, 1996, <a href="http://www.kde.org/people/matthias.html">Matthias Ettrich</a> delivered his famous <a href="http://groups.google.com/groups?selm=53tkvv%24b4j%40newsserv.zdv.uni-tuebingen.de">newsgroup posting</a> (also 
<a href="http://www.kde.org/announcements/announcement.html">HTMLized</a>), and spawned a new era in the history of desktop environments. I think that a simple look at <a href="http://www.kde.org/">www.kde.org</a> and all its related sites will show everybody just what has happened in the last five years. Congratulations to KDE, and here's to future growth and success! <b>Update: 10/15 10:15 AM</b> by <a href="mailto:navindra@kde.org">N</a>: Rob Kaper <a href="http://dot.kde.org/1003095688/1003109733/">obliges</a> with <a href="http://ezri.nl.capsi.com/~cap/digicam/2001-10-14-kde5/">photos</a> of some developers celebrating this occasion.  Charles Samuel goes nostalgic with <a href="http://dot.kde.org/1003095688/1003119252/snapshot4.png">a screenshot</a> of KDE 2.0pre.  Finally, <a href="http://linuxtoday.com/news_story.php3?ltsn=2001-10-15-003-20-NW-CY-KE&tbovrmode=1">LinuxToday</a>, <a href="http://slashdot.org/articles/01/10/15/0019229.shtml">Slashdot</a>, and even <A href="http://news.gnome.org/1003125527/index_html">Gnotices</a> join us in celebrating this event.

<!--break-->
