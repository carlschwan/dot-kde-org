---
title: "KDE 2.2 Release Schedule Update"
date:    2001-07-17
authors:
  - "Dre"
slug:    kde-22-release-schedule-update
comments:
  - subject: "What about KOffice?"
    date: 2001-07-17
    body: "Wise decision! Waldo, thank you for taking kare of the release schedule in such a professional way. I have still an other kuestion:\n\nWill Koffice also be delayed?\n\nI think it should be. The number of minutes it takes me to get KWord or KPresenter (Koffice 1.1 Beta 3) to krash is still less then 3 withuot particularly heavy testing :-<. \n\nI think the basic functionality is there. we need to koncentrate more on the stability, that's klear ;-).\n\nPS: BTW, may be I am I the only one having these problems. Any comments/experiences on the stability of the varoius packages?"
    author: "Andrea"
  - subject: "Re: What about KOffice?"
    date: 2001-07-17
    body: "> The number of minutes it takes me to get KWord or KPresenter (Koffice 1.1 Beta 3) to krash is still less then 3 withuot particularly heavy testing :-<.\n\nHmm? That's not at all my experience. I'm running KDE 2.2 beta from CVS on a two heavily modified Mandrake 7.2 systems and have installed it from RPM on a new Mandrake 8.0 system with 2.2 beta installed from RPM. Both systems are running the new koffice beta, one from cvs and one from RPM. I have not seen any crashes.\n\nWhat I have seen is absolutely terrible printing! I did a beautiful four page presentation in kword and it took about 20 tries massaging the layout to finally get it to produce a printed copy that did not clip the end of lines. I had to finally insert hard returns in a few places. It's really great until you get to that glaring problem. Aside from that kword is not spectacular but very good for most use and exceptional in a few areas.\n\nOne other thing is that it seems to limit my font selection. I hope they fix the printing and keep on with the rest.\n\nBTW you must have something wrong with your install to have kword croaking like that."
    author: "Eric Laffoon"
  - subject: "Re: What about KOffice?"
    date: 2001-07-17
    body: "Actually I've had a beta 3 build of KWord break on documents with tables that span multiple pages.  It seems to work great on simpler documents.  What would be nice is if the import filter could bet setup to log the document load process that way we could tell which MS Word feature broke KWord.  The entire office suite is comming along nicely, kudos to the programmers.\n\nSD"
    author: "SD"
  - subject: "Re: What about KOffice?"
    date: 2001-07-17
    body: "In fact, the multi-page table was a known problem which David fixed shortly after beta 3...so the good news is that current CVS and the full release should work OK!\n\nAs always, if you have specific documents that (a) are causing you problems and (b) are not confidential, then feedback via bugs.kde.org will draw attention to the problem!\n\nThanks, Shaheed"
    author: "shaheed"
  - subject: "Re: What about KOffice?"
    date: 2001-07-17
    body: "Very cool.  What I can do is replace the text of the document (it's my resume :) with random garbage and post it to bugs if it's still causing problems with the CVS copy.  Off to to compile a fresh batch of code.\n\nThanks\n\nSD"
    author: "SD"
  - subject: "Re: What about KOffice?"
    date: 2001-07-18
    body: "Hmmm... You could write a small \"garbling\" script and put it in \"Help\" maybe, alongside bug reporting. The script would change all letters to \"a\" (or a random one), and all numbers to \"0\" (or a random one).\n\nThen no-one would have a reason not to give you the document, and you would have no reason not to ask for one. You could even add a \"Submit garbled document\" checkbox in the bug reporting tool to boot."
    author: "Jerome Loisel"
  - subject: "Re: What about KOffice?"
    date: 2001-07-18
    body: "If it fails during an attempt to import a word document, then how could one modify what one can't open?\n\nHowever, for other cases, not related to importing, but, say related to printing, such might not be bad idea."
    author: "Gregory W. Brubaker"
  - subject: "Re: What about KOffice?"
    date: 2001-07-19
    body: "Do we really want to optimize for bug reporting ?\nWill this make KOffice look good ?"
    author: "AB"
  - subject: "Re: What about KOffice?"
    date: 2001-07-19
    body: "I think that a stable, featureful KOffice in a few years would look great! Helping users help developers will only improve the quality of the software."
    author: "Carbon"
  - subject: "Re: What about KOffice?"
    date: 2001-07-17
    body: "> Will Koffice also be delayed?\n\n    KOffice is on a seperate schedual, which can be found here: \n\nhttp://developer.kde.org/development-versions/koffice-1.1-release-plan.html\n\n    It should be a fairly similar schedual, but they are \"unlinked\" to allow either to be delayed without holding up the other.  If KDE gets delayed too much, then obviously KOffice won't work without the last betas.  If KOffice gets held up, then KDE gets released with no problem.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: What about KOffice?"
    date: 2001-07-21
    body: "Kut that out!  It's driving me krazy!!!"
    author: "Erik Hill"
  - subject: "Will Mosfets liquid style be included?"
    date: 2001-07-17
    body: "What about Mosfets changes/addons, will they be included, or available somewhere?\n\n--\nAndreas"
    author: "Andreas Joseph Krogh"
  - subject: "Re: Will Mosfets liquid style be included?"
    date: 2001-07-19
    body: "Score: -1, troll :-)"
    author: "Coward"
  - subject: "Locolor-Icons"
    date: 2001-07-17
    body: "I hope this additional time will be used to re-add the intentionally from kdebase removed icons for 8bpp displays!"
    author: "Solaris-User"
  - subject: "Re: Locolor-Icons"
    date: 2001-07-17
    body: "I agree, I would have enjoy to use KDE throught low bandwidth network but the minimum 18 bpp is too much.\nPlease put back the 8bpp display."
    author: "Benoit DUMONT"
  - subject: "Re: Locolor-Icons"
    date: 2001-07-17
    body: "The 8 bpp-icons have just been (re-)added to kdeartwork.\n\nGreetings,\nTackat"
    author: "Tackat"
  - subject: "Re: Locolor-Icons"
    date: 2001-07-18
    body: "/opt/kde2.2/share/icons/hicolor > find . | wc -l\n   1663\n/opt/kde2.2/share/icons/locolor > find . | wc -l\n     31\n\nUhm? You have to install a \"locolor\" _theme_ to get _some_ locolor icons!? I think that's not acceptable! I hardly believe an admin can tell _any_ user of his Solaris network that he has to activate a special theme when new KDE becomes default (under this circumstances I believe it will never become it).\n\nAnd there are many many icons missing. In K-menu (Development, Office, Toys, Word-Processing, Game-Submenus), application icons (e.g. Kate), all new entries of control center, KWord lacks half of it's icons. I stop here to summarize.\n\nBetter think of an automated process which regularly locates and creates all missing locolor-icons by dithering and installs them under $KDEDIR/share/icons/locolor/ .\n\nSorry, Tackat but it looks like a quick bad shot."
    author: "Solaris-User"
  - subject: "Re: Locolor-Icons"
    date: 2001-07-18
    body: "> Uhm? You have to install a \"locolor\" _theme_ \n> to get _some_ locolor icons!? \n> I think that's not acceptable!\n\nActually KDE should switch automatically to the locolor-icontheme if it is available just the same way KDE 2.1 did.\n\n> KWord lacks half of it's icons. I stop here to summarize.\n\nFeel free to take care of this task. The locolor-icons were removed from kdelibs/base because they weren't maintained actively anymore.\n\n> Better think of an automated process which\n> regularly locates and creates all missing \n> locolor-icons \n\nWell in that case dithering the remaining hicolor-icons on the fly is certainly the better solution.\n \nTackat"
    author: "Tackat"
  - subject: "Re: Locolor-Icons"
    date: 2001-07-19
    body: "> Actually KDE should switch automatically to the locolor-icontheme if it is available just the same way KDE 2.1 did.\n\nKDE 2.1 did this!? I tested switching to an 8bpp display with an existing and a new KDE user: Both times KDE 2.2 did NOT install [a part of (the icons)] the existing \"LoColor\" KThemeMgr theme. I think you are confusing something:\n\nAll my KDE 2.1 installations don't contain a \"Locolor.ktheme\"-file. The locolor icons are installed in $KDEDIR/share/icons/locolor as fallback for the IconLoader. There is no kthememgr theme installed which would copy the icons to ~/.kde/share/icons/locolor (robbing the users' quotas). Please change kdeartwork's Makefile to install the icons in $KDEDIR/share/icons/locolor rather than creating a ktheme-file which has to be installed manually!\n\n> Well in that case dithering the remaining hicolor-icons on the fly is certainly the betters olution.\n\nI agree, but I guess we don't will see this included in KDE 2.2. :-("
    author: "Solaris-User"
  - subject: "Re: Locolor-Icons"
    date: 2001-07-20
    body: "> I agree, but I guess we don't will see this <br>\n> included in KDE 2.2. :-(\n<br><br>\nLooks like you guessed wrong.  :-)\n<br><br>\n<a href=\"http://lists.kde.org/?l=kde-cvs&m=99559494116227&w=2\">lists.kde.org</a>\n<br><br>\nGreetings,<br>\nTackat"
    author: "Tackat"
  - subject: "Re: Locolor-Icons"
    date: 2001-07-21
    body: "Great! I noticed today that linking the 'locolor' dir to the 'hicolor' dir will do the same trick inclusive dithering.\n\nBut there is a bug: If you install the locolor-icon theme it stops working! I would expect it to search first user's locolor-icons followed by system-wide locolor-icons and last dithering system-wide highcolor-icons."
    author: "Solaris-User"
  - subject: "Re: Locolor-Icons"
    date: 2001-07-21
    body: "#26618 seems to describe this"
    author: "Solaris-User"
  - subject: "Re: Locolor-Icons"
    date: 2001-07-21
    body: "> But there is a bug: If you install the \n> locolor-icon theme it stops working! I would \n\nIn what sense ?\n\n> expect it to search first user's locolor-icons \n> followed by system-wide locolor-icons and last \n> dithering system-wide highcolor-icons.\n\nYou seem to be still using hicolor as your icon theme. Go to KControl->Look&Feel->Icons and select locolor as your icon theme.\n\nAbout #26618, the application cannot assume that locolor is installed anymore, so this is a problem in the application installation.\n\nGreetings,"
    author: "Antonio Larrosa"
  - subject: "Re: Locolor-Icons"
    date: 2001-07-21
    body: ">> If you install the locolor-icon theme it stops working!\n> In what sense ?\n\nIn the locolor-icon theme missing icons are not dithered anymore. As soon as I apply it e.g. the Kate icon in Kicker changes to \"unknown\" icon. After \"dcop kicker restart\" e.g. the icons of the game-submenus changed to \"unknown\" icon too.\n\n> You seem to be still using hicolor as your icon theme. Go to KControl->Look&Feel->Icons and select locolor as your icon theme.\n\nNo, \"KDE-LoColor, Lowcolor Icon Theme\" is selected. Applying it again, doesn't change anything either."
    author: "Solaris-User"
  - subject: "Re: Locolor-Icons"
    date: 2001-07-22
    body: "> In the locolor-icon theme missing icons are not dithered anymore. As soon as I apply it e.g. \n> the Kate icon in Kicker changes to \"unknown\" icon. After \"dcop kicker restart\" e.g. the icons \n> of the game-submenus changed to \"unknown\" icon too.\n\nAaah, I see. You're right, that's a real problem (but different from #26618). Thanks for reporting it.\n\nIt's fixed now. Please update your locolor theme from the kdeartwork module.\n\nGreetings"
    author: "Antonio Larrosa"
  - subject: "Re: KDE 2.2 Release Schedule Update"
    date: 2001-07-17
    body: "Please don't include kwrite in the distribution unless you add support for at least 100 languages!\n\nSorry, just joking... :)"
    author: "Per Wigren"
  - subject: "ok"
    date: 2001-07-17
    body: "Please give it a rest, Per and company...  You may take the discussions to our <a \nhref=\"http://slashdot.org/article.pl?sid=dot.kde.org&threshold=-1\">free for all forum</a>.\n<p>\nThanks,\n-N."
    author: "Navindra Umanee"
  - subject: "Re: KDE 2.2 Release Schedule Update"
    date: 2001-07-18
    body: "I don't know if kwrite will still in KDE2.2, but I'm waiting for its successor \"Kate\" which seems to be a lot more powerful"
    author: "Jean-Christophe Fargette"
  - subject: "Re: KDE 2.2 Release Schedule Update"
    date: 2001-07-18
    body: "I'm excited about Kate too.  It's a cool name, and should be a neat program.\n\nI would like to see a creative name for a new IM client.  Gnome's GAIM is going through legal battles, when perhaps they could have just thought of a cool name.\n\nI think Kim would be a nice name, standing for KDE Instant Messaging, as Kate stands for KDE Advanced Text Editor.\n\nRichard Stallman said in his recent speech in Boston that coming up with a cool name is part of the fun of developing a program.  I agree.\n\nI would like to see a more sophisticated KDE instant messaging tool, perhaps in 2.3, 2.4, or 3.0.  This would be useful in the fight against proprietary computing environments."
    author: "Benjamin Atkin"
  - subject: "Re: KDE 2.2 Release Schedule Update"
    date: 2001-07-18
    body: "The trouble with GAIM is not the name, but rather the network.  AIM is a proprietary network.  KDE includes an AIM client, called Kit.  It _could_ be renamed to Kim, but the problem of the proprietary network is still there.\n\nI think if KDE is going to include any sort of IM program, it should be a Jabber client.  Jabber is the open-source decentralized IM system (see jabber.org for more info, or jabbercentral.com for clients).\n\nI would suggest including the client I'm working on, Psi, but it requires Qt3.  Konverse would be a better choice, since it is a KDE program.  Maybe it should be included in the KDE distribution?  If it doesn't replace Kit, it should at least be there alongside it.  This would be a great promotion of Jabber as well.  Unfortunately, Konverse (and Psi, for that matter) are not in fully usable states.  But that will change for sure."
    author: "Justin"
  - subject: "Re: KDE 2.2 Release Schedule Update"
    date: 2001-07-18
    body: "I'm excited about Kate too.  It's a cool name, and should be a neat program.\n\nI would like to see a creative name for a new IM client.  Gnome's GAIM is going through legal battles, when perhaps they could have just thought of a cool name.\n\nI think Kim would be a nice name, standing for KDE Instant Messaging, as Kate stands for KDE Advanced Text Editor.\n\nRichard Stallman said in his recent speech in Boston that coming up with a cool name is part of the fun of developing a program.  I agree.\n\nI would like to see a more sophisticated KDE instant messaging tool, perhaps in 2.3, 2.4, or 3.0.  This would be useful in the fight against proprietary computing environments."
    author: "Benjamin Atkin"
  - subject: "Re: KDE 2.2 Release Schedule Update"
    date: 2001-07-18
    body: "sorry for this repeat post.  I saw a quote from Caldera I thought would be interesting that I missed, pushed the back button and impulsively reposted form data.  Please excuse me for this."
    author: "Benjamin Atkin"
  - subject: "KDE features"
    date: 2001-07-17
    body: "I'm sure many people with modem connections will like the new kwm behavior where windows are placed on the desktop where it was launched.  Now someone can switch to a new desktop, start conqueror, and switch back to the window they were working in.  Then in a few seconds their window should be ready with their homepage.  I'll just like the fact that it's the behavior that I expect, since I have a fast computer and a fast network connection :-)\n\nI know this is just a minor improvement, but there are ones all over that will greatly improve KDE.  There are all kinds of reasons why KDE is my favorite interface.\n\nAs for major improvements, Kooka and KDEPrint are most exciting to me.  I hope those working on the KOffice project get WYSIWYG improved soon.  Scanning and printing are particularly important to the desktop user, and I look forward to being able to do digital photography easily with KDE.\n\nIt is amazing how much progress has been made with KDE.  There are so many ease of use, performance, and stability reasons to use it over Windows or Mac OS X, not to mention the sharing of software makes for a better society.\n\nThanks, KDE developers!!!"
    author: "Benjamin Atkin"
  - subject: "Re: KDE features"
    date: 2001-07-18
    body: "I would like to see that too. That programs open on the V desktop that they where started on, even if you change V desktop will it is loading."
    author: "Mark Hillary"
  - subject: "Re: KDE features"
    date: 2001-07-18
    body: "The only problem is that I've gotten so used to the old way, that i forget I've started programs, because they don't appear on-screen :)."
    author: "David G. Watson"
  - subject: "Re: KDE 2.2 Release Schedule Update"
    date: 2001-07-18
    body: "Seeing as how there's already a couple of folks asking about KOffice, I've got a similar kind of status request.  Anybody know what all is going on with Quanta?  Last I looked in the CVS, nothing has been updated for over 2 months.  No updates on the SourceForge page either.\n\nIs the project dead?  As I recall, last major KDE release the developers of Quanta spent a healthy amount of time working on KDE 2.0.  Is that the case this time as well, or should I just give up on hoping for a high quality HTML/PHP editor for KDE?"
    author: "Metrol"
  - subject: "Re: KDE 2.2 Release Schedule Update"
    date: 2001-07-19
    body: "I saw some discussion about this on the core-devel list, they where planning to put Quanta on the next release of kde (2.3 or 3.0 whathever).\nThe authors seem to be occupied with their work, but will restart programming soon (ok, ok, soon is relative, it can be today or next month, I don't know, sorry)."
    author: "Protoman"
  - subject: "Re: KDE 2.2 Release Schedule Update"
    date: 2001-07-18
    body: "This may sound like a flame, but it is just a little glimpse of reality.\n\nOne thing that would really help is if they were to actually TEST the system.  I am still running KDE 2.1, but I wish I were back at 1.1.2, at least that never crashed, and I don't push KDE any more now than I did then, and it is still very slow and unstable.  Word of advice to you KDE developers, TEST THE ENVIRONMENT UNTIL IT BREAKS, THEN TEST IT SOME MORE!!!!  KDE is getting so bad, it is sinking almost to the level of Winblows.  I am a developer, and I want speed and power, I don't care about DCop, which just bogs down the system.  I took one look at the ps -awx output and nearly went insane, the kdeinit fork for dcop is (VSZ)13 MEGS!!!! Now where the hell did they go from 100k in the benchmarks to 13MB in the practice?  And I thought the session manager was supposed to stay out of the way, how does that take up 13 MB of virtual memory?  There must be memory leaks somewhere.  Now, did someone happen to write a \"desktop2kdelnk\" program, the reverse of kdelnk2desktop?\n\nAll I can say now is their actions are getting to be stupid, sacrificing performance, speed, memory, and stability for some more bells and whistles that half of us don't want or need?  That is plain stupid.  I just say they should go back to writing code like in KDE1.1.2, now to fish out my Mandrake 7.1 CD's for the kde1.1.2 source...I know their somewhere around here..."
    author: "Justin Hibbits"
  - subject: "Re: KDE 2.2 Release Schedule Update"
    date: 2001-07-18
    body: "apart from the fact your writing style is not really forum-compatible, you most probably have a problem with your kde installation. kde 2.1 was perfectly stable for me, dcop is not that big here (and i experimented a lot with dcop yet ...)\nif you really think you have discovered bugs, please report them instead of the vague whining you do here. in my experience, kde developers really investigate bug reports.."
    author: "ik"
  - subject: "dcopserver memory usage"
    date: 2001-07-18
    body: "I know you're just trolling, but I want to get rid of some misunderstanding of memory usage. dcopserver's /proc/#pid/status tells me:\n  VmSize:    15500 kB\n  VmLck:         0 kB\n  VmRSS:       860 kB\n  VmData:      340 kB\n  VmStk:        24 kB\n  VmExe:        36 kB\n  VmLib:     14460 kB\nSince you're a programmer you should now that VmLib is shared between all applications. This leaves about 340+24+46=410kB of memory consumed by dcopserver."
    author: "Wilco"
  - subject: "Re: dcopserver memory usage"
    date: 2001-07-21
    body: "Although I don't agree with the style of the original poster, I think he has a point on some respect.\n\nIn my system, I have 30 kdeinit processes running:\n- 3x kicker: 3MB each\n- noatun: 5MB (not active)\n- 2x artsd: 3MB each (not active)\n- 8x konsole: 1.5MB each\n- 3x kdesktop: 1MB each\n- various other things\n\nThis is just the memory used on the heap.  I think for each of these, this is quite a lot in terms of memory consumption.  Also, for some I cannot understand why multiple processes are running, e.g. artsd.  Maybe these are threads...\n\nWhat really scares me, though, is that there is a lot of infrastructure missing:\n- no memory management to track leaks (after 2 months being logged in, even the smallest leak becomes a big problem)\n- no tracing\n\nFor instance, 'artsd' likes produce some deadlock in the kernel (at least that's my theory) and wastes a lot of resources that way.  But there is no other way to support the developer in finding the problem that to say: \"there is a problem\".  How are the odds that this will be fixed?  Not very good, I'd say.  I was hoping that this particular problem will be fixed in 2.2 (I reported it a while ago), but it still appears in beta1.  Having a trace facility in place would help very much by being able to take a trace when the problem occurs. \nThis is a common feature in commercial applications.  Why not in something as complex as KDE?\n\nKnut"
    author: "Knut Stolze"
  - subject: "I thought you actually sounded like a troll."
    date: 2001-07-18
    body: "I thought you actually sounded like a troll.  You need to take a look at your installation.  Even if you're a \"developer\" you likely have not done it correctly.  Please understand that the aim of the KDE project not to produce the same thing as twm.  KDE is a sophisticated environment.  KDE developers try to create the best environment on any platform, and this means there will be some extra space.  Unlike Microsoft, etc.  KDE developers are true innovators and are working with the GCC creators to improve the speed of the program.  They are using realistic requirements to create good systems.\n\nPlease respect KDE developers for their efforts."
    author: "Gilded Rooster"
  - subject: "Re: I thought you actually sounded like a troll."
    date: 2001-07-18
    body: "You say KDE developers are real innovators. I use KDE and I am quite pleased with it. But I wonder which innovations you mean! Is there any cool feature (and usefull, too) that MS had not first?\nI realised this when I did the change from Windows to KDE. My girl friend, who is far away to be a computer specialist, didn't even remarked, that she was working with KDE and not with Windows.\nMaybe there are innovations from the developer's point of view. But the users must look very carefully to see the real differences between Win and KDE. Of course there are thinks working better in KDE, but I wouldn't call them innovations!"
    author: "Marc Spisl\u00e4nder"
  - subject: "Re: I thought you actually sounded like a troll."
    date: 2001-07-18
    body: "Here are some innovations that I haven't seen in Windows (maybe they're in ME or 2K, but I haven't had any desire to try them out):\n\nThe audiocd:/ ioslave - copy mp3 files directly from an audio CD, without the need for an intermediate program (it's done automagically for you).\n\nInternet Keyword Search: put 'gg:foobar' into the konqy location bar, and take a look what it does :). Same works for more other search engines, plus dictionary, etc. It's also configuratble.\n\nKParts: real embedding of whole applications, which \"take over\" the window when you want to edit the embedded data."
    author: "David G. Watson"
  - subject: "Re: I thought you actually sounded like a troll."
    date: 2001-07-19
    body: "OK, the audiocd:/ is an improvement. The next Windows generation will support this and burning CD as well, as far as I know.\nThe \"gg:xxx\" is not bad but, in my eyes, not a breakthrough. If you want to start sofisticated searches you have to go directly to the Google site as well. Of course you could set parameters into the gg-Statement, but then we have a fundamental problem: we must decide: command line or GUI. And KDE-user prefer the GUI, I guess.\nKParts: This does Windows as well. They call it OLE or something like this.\nA really innovation for me is the possibility to access FTP within the open dialog of any application."
    author: "Marc Spisl\u00e4nder"
  - subject: "Re: I thought you actually sounded like a troll."
    date: 2001-07-18
    body: "Text-Previews using mimetype-icon-\"watermarks\" to display filetype + content. Damn I should have patented that idea ;^) !\n\nGreetings,\nTackat"
    author: "Tackat"
  - subject: "Re: I thought you actually sounded like a troll."
    date: 2001-07-19
    body: "Is this really an innovation? And Windows does this as well (I'm not sure, I use Windows very seldom.).\nI admit, it is a nice feature. Specially for me, for whom the optic is very important. If a program looks ugly, it will be a problem for me to use it. \nBy the way: on my system if I change the content of a text file the text preview doesn't update.\nMy opinion is: KDE is not more or less full of innovations than Windows. I could mention as many innovations in Windos as you probably could for KDE: \n* menu items that disappear if they aren't used often\n* the web view for directories which tells you how many free space you have (helpfull for beginners)\n* auto completion in IE"
    author: "Marc Spisl\u00e4nder"
  - subject: "Re: KDE 2.2 Release Schedule Update"
    date: 2001-07-18
    body: "If you feel so strongly that KDE needs more testing, why don't you start building the sources from CVS every few days, and submit some quality bug reports? Better still, seeing as you are a developer, why not debug some of the problems yourself, and submit some quality, well tested patches?\n\nOr if you really don't like the direction KDE development is taking, why not produce your own fork of the KDE 1.1.2 sources, and add the features that you do think are worthwhile? I'm sure no one would be offended, and you would probably attract a number of other developers who feel the same way as you - after all, if you look at apps.kde.com you will see many projects that continue to release new versions based on the 1.1.2 libraries."
    author: "Mark Roberts"
  - subject: "Re: KDE 2.2 Release Schedule Update"
    date: 2001-07-18
    body: "Actually, several people have visited my home page for my new DE, based on a new toolkit, which will be similar to KDE 1.1.2, so the stability will be apparent.  If you know anyone who would like to code this, have them send me some mail.  As for building from CVS, and bug reports/patches, I am too busy to do that, and my computer isn't even on the internet, I am doing everything through CD's and floppys with multi-volume archives.  And, what I've noticed is that it isn't KDE that's really the major issue, it's just really too slow, it's really kdevelop running in KDE, which refuses to place itself correctly on startup, and some other problems that I won't mention, since this isn't a kdevelop forum.\n\nAs a little history, KDE 2.1 Beta was probably more stable for me, just slightly, though.  Maybe the problem is in my compiler....does anyone have any problems using KDE compiled with gcc 2.95.3?\n\nEnough of this, I think I've learned my lesson...don't post messages that sound like flames...if you want it done right, do it yourself...blah, blah, blah...and use IceWM with kdesktop for speed, power, and memory considerations...."
    author: "Justin Hibbits"
  - subject: "Re: KDE 2.2 Release Schedule Update"
    date: 2001-07-18
    body: "Will KDE ever have a fairly standards-compliant + feature-full browser? How about embedding, in the vain of the Galeon project?"
    author: "None"
  - subject: "Re: KDE 2.2 Release Schedule Update"
    date: 2001-07-18
    body: "if you want it, the kdebindings module has an xpart to embed gecko, the same mozilla rendering engine used by galeon. Personally, I prefer KHtml, but if you like gecko, use it :-)."
    author: "Kevin Puetz"
  - subject: "Re: KDE 2.2 Release Schedule Update"
    date: 2001-07-18
    body: "What's really missing in KDE (compared to GNOME) is something like Ximian's Red Carpet. Are there any plans to create something similar for KDE?"
    author: "Havard Bjastad"
  - subject: "Re: KDE 2.2 Release Schedule Update"
    date: 2001-07-18
    body: "Red Carpet is a service provided by Ximian.  This service requires making binary packages for many applications for each distribution.  Such a service requires a lot of boring work and manpower.  Ximian, being a company, has such resources.\n\nHow do you propose a free software project like KDE go about providing such a service?  In fact, you would need a whole team dedicated to providing updates and building binary packages on who knows how many platforms.\n\nIn fact, isn't this the job of your distribution? \n\nWhen Kent completes the KDE installer, perhaps KDE.com can provide a paid service by providing appropriate XML files and binary packages for the various applications listed on apps.kde.com. But the manpower and resource problem remains."
    author: "Navindra Umanee"
  - subject: "Re: KDE 2.2 Release Schedule Update"
    date: 2001-07-18
    body: "Yes, Red Carpet is a service, but I guess what I had in mind was for KDE to create something like the Red Carpet _application_. To my best understanding KDE already provides most of what's needed: Binaries for various distributions.\n\nFor instance, being a RedHat user, I can go to kde.org and download new RPMs. However, what Red Carpet (the application) provides for GNOME, is handling the following tasks for me:\n1. Check for updated RPMs\n2. Let me select which of the new RPMs I want\n3. Download and install the selected RPMs\n\nShouldn't it be possible for KDE to create an application that does the above? I realize that supporting _all_ distributions may be too big of a task, but if you support RPM-based distributions you'll still cover a lot of ground (and maybe someone else will extend the application to support other distributions).\n\nJust to make myself clear: This idea is all based on the binary packages already being available."
    author: "Havard Bjastad"
  - subject: "Re: KDE 2.2 Release Schedule Update"
    date: 2001-07-18
    body: "KDE does not provide the packages.  KDE doesn't make the packages.  The distributions make the packages, and KDE provides space for them on the KDE FTP servers as a courtesy.  KDE provides the sources only.\n\nIt's a resource issue, KDE does not have the resources to provide such a thing.  \n\nGo look at this page where the KDE policy is explained very thoroughly:\n\nhttp://dot.kde.org/986933826/"
    author: "Lauri Watts"
  - subject: "Re: KDE 2.2 Release Schedule Update"
    date: 2001-07-18
    body: "Mandrake8.0 has similar service available and not just for one application (or group of apps) but the whole system. Try it!"
    author: "Alex"
  - subject: "Re: KDE 2.2 Release Schedule Update"
    date: 2001-07-19
    body: "I'd be very interested in taking a look at it! Could you please point me to where I can find more info, or should I just start searching Mandrake's site?"
    author: "Havard Bjastad"
  - subject: "Re: KDE 2.2 Release Schedule Update"
    date: 2001-07-19
    body: "Mandrake8.0 provides only security updates.\nIt does not provide \"feature updates\".\n\nThat is, you get an updated version of a package\nonly if the latest version fixes a security\nbug. If the latest version just adds features,\nthey do not provide it.\n\nOf course, they do provide the latest binaries\nof KDE.\n\nMagnus."
    author: "Magnus Pym"
  - subject: "Re: KDE 2.2 Release Schedule Update"
    date: 2001-07-20
    body: ">Mandrake8.0 provides only security updates.\n>It does not provide \"feature updates\".\n\nNot true, or at least, no longer true. With Mandrake 8.0, MandrakeUpdate now allows you to fetch and install packages from the \"Cooker\" development collection (the equivalent of Red Hat's \"Rawhide\")."
    author: "Kevin Shaum"
  - subject: "Re: KDE 2.2 Release Schedule Update"
    date: 2001-07-19
    body: "SuSE 7.1 provides Online update facility in YAST2 - it does exactly what you described. \n\nYou migth also take a look at autorpm and kpackage, autorpm downloads defined (installed and/or new packages) fist and it's up to you to to install it afterwards manualy or automaticaly\n\nferdinand"
    author: "ferdinand"
  - subject: "Re: KDE 2.2 Release Schedule Update"
    date: 2001-07-19
    body: "Correct me, but SuSE provides only patch-information which fix bugs for their Online Update. There is a lot more available on their FTP-Server than what it offers to you (KDE 2.1, Gnome 1.4, XFree 4.1 to mention)."
    author: "someone"
  - subject: "Re: KDE 2.2 Release Schedule Update"
    date: 2001-07-19
    body: "Yes, that's propably true. Didn't try yet."
    author: "ferdinand"
  - subject: "Python support in KWrite and Kate?!"
    date: 2001-07-18
    body: "Running today's CVS snapshot and it seems very stable!\n\nBut where is the Python support in KWrite and Kate??! Will it be in before the 2.2-release?"
    author: "Per Wigren"
  - subject: "KDE 2.2 Release and Solaris/Sparc audio issues"
    date: 2001-07-19
    body: "Is it planned to fully support Sun Audio in KDE 2.2 Release?\n\nAfter arts finally supports Sun Audio in KDE-2.2Beta1 it would\nbe nice if some other audio-apps would also work as expected.\n\n1. The ogg-vorbis plugins (mpeglib, oggvorbis_artsplugin) don`t consider the endianess\nof the sparc architecture. ov_read() deals with that.\n\n2. Some applications (e.g. mpeglib,kmix) were accessing the audio(ctl) device directly\nby hardcoding the path to the devices. Especially Suns SunRays do not use these.\nThe SunRay-serversoftware uses $AUDIODEV to access the audiodevices.\nFor the audioctl device, one has to append \"ctl\" to the value of $AUDIODEV.\n\n3. mpeglib will not be built by default for   the sparc architecture, but why not? It seems    to work.\n\n4. There are some other (non audio) issues, but I don't remember exactly...\n\n\nGerhard"
    author: "Gerhard Frnke"
---
After some debate last week over whether KDE 2.2 HEAD BRANCH
was ready for a stable release,
<A HREF="http://www.kde.org/people/waldo.html">Waldo Bastian</A>, the KDE 2.2
release coordinator, has
<A HREF="http://www.kde.com/maillists/show.php?li=kde-core-devel&f=1&l=20&sb=mid&o=d&v=list&mq=-1&m=496988">posted</A>
a revised release schedule (also available if you Read More).
The release has been delayed two weeks from Monday, July 16 to August 6.
Though  KDE excels at sticking to published release schedules, it seems
to me that stability and security are the developers' primary concerns.
<EM>And That's A Good Thing</EM><SUP><FONT SIZE=1>TM</FONT></SUP>.

<!--break-->
<P>&nbsp;</P>
<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0>
<TR><TD ALIGN="right"><STRONG>
Subject</STRONG>:&nbsp;</TD><TD>
            KDE 2.2 release: current schedule</TD></TR>
<TR><TD ALIGN="right"><STRONG>
        Date</STRONG>:&nbsp;</TD><TD>
            Mon, 16 Jul 2001 10:30:59 -0700</TD></TR>
<TR><TD ALIGN="right"><STRONG>
       From</STRONG>:&nbsp;</TD><TD>
            Waldo Bastian <bastian@kde.org></TD></TR>
<TR><TD ALIGN="right"><STRONG>
          To</STRONG>:&nbsp;</TD><TD>
            kde-devel@kde.org, kde-core-devel@kde.org, kde-i18n-doc@kde.org, koffice-devel@kde.org</TD></TR>
</TABLE>
<P>
Hiya,
</P>
<P>
This is to keep you up to date with the status of the KDE 2.2 release. I have
added a bit more time in the schedule in order to fix security problems that
exist with our use of https at the moment. I hope that everyone else can use
this time to do some additional bugfixing. Please be very carefull and send
patches of your changes to a mailinglist for review first. *)
</P>
<P>
The up to date schedule for the KDE 2.2 release is as follows:
</P>
<P>
From the <A HREF="http://developer.kde.org/development-versions/kde-2.2-release-plan.html">release plan</A>:
</P>
<P>
<H4>KDE 2.2 final</H4>
</P>
<UL>
<LI><STRONG>Monday July 16</STRONG>.  The HEAD branch goes into deep-freeze. All commits should be posted for review _BEFORE_ commiting.</LI>
<LI><STRONG>Sunday July 29</STRONG>.  Last day for translations,
documentation, icons and critical bugfixes commits.</LI>
<LI><STRONG>Monday July 30</STRONG>.
The HEAD branch of CVS is tagged KDE_2_2_RELEASE and the day is spent testing
the release a final time.</LI>
<LI><STRONG>Tuesday July 31</STRONG>.
The tarballs are made and released to the packagers.
The rest of the week is spent testing the tarballs, packaging and writing the
announcement and changelog. </LI>
<LI><STRONG>Friday August 3</STRONG>.
The source and binary packages are uploaded to ftp.kde.org to give some time
for the mirrors to get them. </LI>
<LI><STRONG>Monday August 6</STRONG>.
Announce KDE 2.2. </LI>
</UL>
<P>
<H4>Frequently Asked Questions</H4>
</P>
<P>
Q) Which packages are included?</P>
<P>
A) The current list of packages that will be in the 2.2 release is:</P>
<UL>
<LI>kdelibs</LI>
<LI>kdebase</LI>
<LI>kdenetwork</LI>
<LI>kdegraphics</LI>
<LI>kdeadmin</LI>
<LI>kdemultimedia</LI>
<LI>kdegames</LI>
<LI>kdeutils</LI>
<LI>kdetoys</LI>
<LI>kdepim</LI>
<LI>kdesdk</LI>
<LI>kdebindings</LI>
<LI>kdevelop </LI>
<LI>kdoc</LI>
<LI>New package: kdeaddons</LI>
<LI>New package: kdeartwork</LI>
</UL>
</P>
<P>
Cheers,
</P>
Waldo
<BR>
--
<BR>
<A HREF="mailto:bastian@kde.org">bastian@kde.org</A> | <A HREF="http://www.suse.com/">SuSE</A> Labs KDE Developer | <A HREF="mailto:bastian@suse.com">bastian@suse.com</A>
<BR>
<P>(<STRONG>Ed</STRONG>:  For those who have not noticed yet, KDE no longer ships with kdesupport, under the theory that modern distributions already provide the programs forming kdesupport.)</P>
