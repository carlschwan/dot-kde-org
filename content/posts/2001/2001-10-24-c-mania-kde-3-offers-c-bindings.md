---
title: "C Mania:  KDE 3 Offers C Bindings"
date:    2001-10-24
authors:
  - "Dre"
slug:    c-mania-kde-3-offers-c-bindings
comments:
  - subject: "Nice"
    date: 2001-10-24
    body: "Troll summary: Gnome vs KDE, C vs C++, etc...\n<!--\nI am glad to see this announcement, hopefully soon kde will reach the usability and stability of the gnome desktop. Hopefully teh kde project can be rewritten in c instead of just offering bindings so they can improve the speed of kde. c code produces far more effecient programs in all cases, especially gui's. this will also make the project more appealing to other developers (this is an area kde is really struggling behind gnome in). well this is definetly good news!\n-->\n"
    author: "DeadPBird"
  - subject: "Re: Nice"
    date: 2001-10-24
    body: "It is assembler which produces far more efficient programs in all cases, but with C it is almost the same.  In fact C++ is a C with some high-level additions. If you want to write a good app you have to write code fast when efficiency is not a matter ( 95% of typical GUI program ), and polish those small critical parts which need it - C++ shines here. Even in those low-level areas it is better than C - when the autor mastered templates and STL he needn't to reinvent sorting procedures, containers etc. If you want to do reasonable GUI development you have to build your own, nonstandard object layer on top of C, as gtk team did ( look how, for example, \"inheritance\" is implemented in gtk. Uff !!!!!! Only masochist can think that C is better for GUI apps  )  I don't like also gtk \"appealing\" to other developers. Question: If you have ten libraries and want to use them from ten script languages how many bindings should you write ? The whole idea with language bindings is BROKEN. The main drawback of C++ is messy and complicated syntax and slow compilers."
    author: "Krame"
  - subject: "Re: Nice"
    date: 2001-10-24
    body: "i strongly doubt, that you can write far more efficient programs in assembler in all cases given the actual processor-design. sure, it is possible, but you will lose your mind in the process. only think of the difficulties you will have with keeping the command pipeline filled and thats not the only issue..."
    author: "cylab"
  - subject: "Re: Nice"
    date: 2001-10-24
    body: "And can you trust the compiler to do that? I am only asking, I don't know.\nWhich compiler is most likely to do this, the gnu or the Intel?"
    author: "reihal"
  - subject: "Re: Nice"
    date: 2001-10-25
    body: "this is obvious for x86 platforms: the intel of course :) just read an article in the ix (german computer magazin) about the intel compiler for linux that said a simple recompile boosts perfomance up to 30% compared to gcc! with optimizations even up to 50%... i think that proves, that it is essential to know the internals of a cpu to write efficient assembler code and beat an optimized compiler might be very hard. (and there is more weird stuff in other cpus than x86...)"
    author: "cylab"
  - subject: "Re: Nice"
    date: 2001-10-25
    body: "What issue IX magazine was that? I can't find it on their website."
    author: "reihal"
  - subject: "Re: Nice"
    date: 2001-10-25
    body: "i didnt find it on the homepage, too.\nit is issue 11/2001 S68. you can check\nhttp://www.heise.de/ix/linux/lx-article.html"
    author: "cylab"
  - subject: "Re: Nice"
    date: 2001-10-24
    body: "I thought we'd stopped having discussions about the relative merits of assembler vs. high level languages about the time punched cards went out of fashion.\n\nYes, of course C is completely unsuitable for writing GUI apps, but that isn't what these bindings are for. They are to allow people to implement bindings for languages which can't call C++ directly, like Objective-C.\n\n\"The main drawback of C++ is messy and complicated syntax and slow compilers.\"\n\nYes, C++ and/or STL doesn't shine, it stinks. Only a (genius level :) ) masochist would think that C++ is a great scripting or RAD language, and that we should give up all efforts to use more suitable languages such as Ruby or Objective-C via language bindings.\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: Nice"
    date: 2001-10-26
    body: "\"Yes, C++ and/or STL doesn't shine, it stinks. Only a (genius level :) ) masochist would think that C++ is a great scripting or RAD language, and that we should give up all efforts to use more suitable languages such as Ruby or Objective-C via language bindings.\"\n\nI meant that a good component model with IDL compiler for generating bindings would be better than maintaining and trying to keep up to date huge bindings. I could be even something like specialized IDL for binding C++ to C for Qt-based programs (without components). So, of course, language bindings are OK, but it should be done \"by hand\"."
    author: "Krame"
  - subject: "Re: Nice"
    date: 2001-10-26
    body: "I see that you use such kind of tool, but you release bindings and not a program for generate bindings.  Is your tool available as a  separate program ?"
    author: "Krame"
  - subject: "Re: Nice"
    date: 2001-10-30
    body: "Yes - see kdebindings/kdejava/koala/kdoc for the tool used for the Java bindings  generation. The C and Objective-C ones are very similar, but I just wanted to combine them all into one tool before checking it into the KDE cvs. I can send the source of the C and Objective-C source conversion tools to anyone interested as an email attachment.\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: Nice"
    date: 2001-10-30
    body: "I didn't type in 200k C/C++ LOC to transmute an api by hand, I adapted a powerful text processing tool to do it.\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: Nice"
    date: 2001-10-24
    body: "1. The API, C++ stucture, KParts, etc. are what make KDE a vastly better desktop to developers than Gnome.\n\n2. What makes KDE (and Gnome) slow is X-Windows.  The sooner we all use DirectFB (www.directfb.org) the better...\n\nChris"
    author: "Chris Kings-Lynne"
  - subject: "DirectFB"
    date: 2001-10-24
    body: "Hee hee...  Did you learn about DirectFB on Slashdot today?  That was my article :-)\n\nI'm excited about DirectFB.  It seems like it could be a real X replacement for Linux desktop usage.  If there was a QT port, it would be perfect.  Perhaps DirectFB and QT/Embedded could get together?  The nice thing is that no one has to give up network-transparent X apps to use DirectFB, since most programs nowadays are GTK or QT and can easily be compiled for X, and there is already an X server available for DirectFB.  That way all those X diehards can continue running their apps remotely while the rest of us have fun with alpha-blended video players :-)"
    author: "not me (aka Spy Hunter)"
  - subject: "Re: DirectFB"
    date: 2001-10-24
    body: "can DFB be run remotly like X can or do you have to use an Xserver on the remote system? also, can you write a program that localy takes advantage of DFB but remotly uses X? if the answer is yes to both I think it has a chance."
    author: "Jeremy"
  - subject: "Re: DirectFB"
    date: 2001-10-24
    body: "With XDirect, DirectFB essentially becomes an X server, capable of displaying X apps that are running remotely on another machine, or even locally on the same machine, just like XFree.  You can continue to use your DirectFB native apps at the same time, but you can't display DirectFB apps remotely.  That would defeat the whole purpose - DirectFB is meant to provide nearly direct access to the hardware for maximum speed.  It might be possible to write a program that selects whether to use DirectFB or X at runtime, I'm not sure.  What would be really cool is if you could dynamically link a KDE program to either QT/X or (hypothetically) QT/DirectFB depending on what you wanted, without a recompile, but I'm not sure if that's possible.  The thing is, though, if you really need apps to run on both DirectFB and X, you can simply compile it for X and use DirectFB's X server."
    author: "them"
  - subject: "Re: DirectFB"
    date: 2001-10-24
    body: "I have a questio thougth... isnt DCOP based on the X11 lib ICE??? if it is, then Qt/DirectFB has a problem."
    author: "Mario"
  - subject: "Re: DirectFB"
    date: 2001-10-24
    body: "libICE works just fine without X11."
    author: "Bernd Gehrmann"
  - subject: "Re: DirectFB"
    date: 2001-10-24
    body: "does the X app benefit from running under XDirect as apposed to XF86? also, I would be cool if it Dynamicly linked the rendering engine at run time. I think it would be great for games and office programs and the base Desktop environment to use only DFB, and admin tools could use X....I think that programers just ned to consider what the app will be used for and program it for the correct rendering engine. programs that are in reality only good for local use such as games and office productivity and internet tools should use DFB, programs that can be realisticly used remotly or localy should use X. I say realisticly because what person in their right mind would play a game on a desktop from a remote machine? it whould be easier to just place the game on an NFS partition and place the link localy.\n\nI think DFB deploied in this way on a linux machine would be very productive and give all the benefit for X apps and DFB apps. to apply each tool to the place it will do the most good."
    author: "Jeremy"
  - subject: "Re: DirectFB"
    date: 2001-10-24
    body: ">does the X app benefit from running under XDirect as apposed to XF86?\n\nYou can run it with DirectFB apps, which I suppose is a benefit, and you can set a global transparency for it, which is kind of pointless.  The point isn't to run X apps better, though.  The point is to be able to make efficient DirectFB native apps, while still keeping backwards compatibility.\n\n>I think it would be great for games and office programs and the base Desktop environment to use only DFB, and admin tools could use X\n\nI think that it would be better to decide what you want to use your system for when you set it up, and choose at that time whether to compile (or get RPMs of) all your apps for DirectFB or X.  Dynamically changing the rendering system at runtime would be way cooler though :-)"
    author: "them"
  - subject: "Re: DirectFB"
    date: 2001-10-24
    body: "Hi,\nI am not exited about DirectFB at all. \nXFree86 as a graphics subsystem is not only an implementation of the X windows system, it also has some features for direct accessing the graphics hardware: DRI for 3D and DGA for 2D for example. Also there is Shared Memory in its different variants (Shared Memory extension, I think XVideo is based on SHmem ...). When your video output is too slow it is almost always the way the app was programed. \nAdditionally, DirectFB runs in the kernel layer, so a bug in your video driver can bring down your whole system like in windows. There is a little performance loss if the Xserver is an additional layer, but a huge gain in stability!!!\nThe reason why we don\u00b4t have proper alpha blending etc. in XFree86 today is the missing rendering model for this type of graphics handling. The core rendering model is based on postscript, but stripped down. It can only do things like diplay recangles, draw lines fill etc. you can\u00b4t do alpha blending with this :(   \nThe beauty of the X window system is that it has only the necessary things in its core the rest happens through extensions. You will always need to draw lines and rectangles etc. The core gives you the basics. \nIf you need feature xyz you write the xyz extension!!! The extension decides whether it is via direct access or networktransparent.  \nThis also helps to get rid of \"stupid ideas\" easily. Some years ago there was PEX for 3D graphics. Today everyone uses GL so you can get rid of the PEX extension -> no bloat! The extension can make feature xyz either available for direct access only like DRI, or over the network like RENDER. If implemented correctly it is as fast as every DirectWhatEver!\n\nPlease understand that XFree86 is more than just an implementation of the X window system. There is absolutely no need to get rid of XFree86!\n\nMarc"
    author: "Marc"
  - subject: "Re: DirectFB"
    date: 2001-10-24
    body: "DFB runs at the kernel level? IIII......think I am going to stick with X :-) I don't like a lot of stuff running in my kernel that is dependant on a totaly seperate project.....besides, Linus would never allow that in since not all the drivers would not be under the Kernel hakers control."
    author: "Jeremy"
  - subject: "Re: DirectFB"
    date: 2001-10-24
    body: ">Additionally, DirectFB runs in the kernel layer, so a bug in your video driver can bring down your whole system like in windows. There is a little performance loss if the Xserver is an additional layer, but a huge gain in stability!!!\n\nThis is totally wrong.  The framebuffer is in the kernel but it is only used for mode switching and a couple of other things, the entire DirectFB library is purely userspace and no more crash-prone than X, much less in fact since it is much smaller.  Anyway, haven't you had your display locked up with X before so you couldn't use your computer?  No one cares if the kernel is still running underneath, its the display that counts.\n\nAnd don't go saying there's \"little performance loss\" either.  You don't know that, and I don't know that, until something else is tried.  I think DirectFB has the potential to be noticably faster, and I think we should try it and see."
    author: "them"
  - subject: "Re: DirectFB"
    date: 2001-10-24
    body: "Using Shared Memory or DGA the performance loss is minimal. Of course alpha blending and such will be slow if there is no appropriate rendering model in your driver and the app has to do the rendering and send the result to the Xserver.\n\nIf you go the networktransparent way (app sends request to Xserver, XServer writes to video memory) speed is reasonable as long as the rendering model supports it. And exactly this is the problem!!! \n\nThere was no development going on for over 10 years, because X was used by companies that produced servers, which mostly don\u00b4t even have graphic cards. \nSo we are missing the proper rendering models, not directness of the video driver. Again, now there is RENDER, DRI, Xvideo, DPS, DGS and many more...\nMany excellent programmers are working on XFree86, a lot of innovation is coming from this project. The missing features will be integrated soon, a long time before DirectFB gets usable.\n\nMarc"
    author: "Marc"
  - subject: "Re: DirectFB"
    date: 2001-10-25
    body: "it sounds like XF86 is an exciting project to work on.....I think you need to get out and advertise more about what is going on at the project....I bet most people just think that you guys are sitting around looking for bugs and writing generic drivers which is where I bet most of this X is slow and bloated crap comes from, the people just don't know enough about the project. what is going on in the setup tools realm? are they working on a nice simple select and enter system where you don't need to know a ton about your hardware?\nwhat about making the resolution settings more dynamic and less buggy when a change occurs? are you working on Direct3d libs? do you think your font server is getting much better?\n\nthanks"
    author: "Jeremy"
  - subject: "Re: DirectFB"
    date: 2001-10-25
    body: "it sounds like XF86 is an exciting project to work on.....I think you need to get out and advertise more about what is going on at the project....I bet most people just think that you guys are sitting around looking for bugs and writing generic drivers which is where I bet most of this X is slow and bloated crap comes from, the people just don't know enough about the project. what is going on in the setup tools realm? are they working on a nice simple select and enter system where you don't need to know a ton about your hardware?\nwhat about making the resolution settings more dynamic and less buggy when a change occurs? are you working on Direct3d libs? do you think your font server is getting much better?\n\nthanks"
    author: "Jeremy"
  - subject: "Re: DirectFB"
    date: 2001-10-24
    body: "I see the main problem with X being slow is the acceleration architecture. For example (I read this in an interview with the Konqueror developers, so yell at them not me if it is wrong) X doesn't accelerate operations on pixmaps. Did you notice that most of the KDE themes are pixmap themes...I switched from using a pixmap theme to the builtin NeXT style, and the apps run a bit faster now. Also, there does seem to be a problem with context switching--you have the X server, windowmanager, applications, and other invisible apps. You have to do a lot of context switching between X, the app, and the windowmanger. Maybe if the WindowManger become part of the X server (i.e. it existed in a library that implemented a set interface so the X server could dlopen it, initialize it, and then run it in a thread). Maybe the pre-emptive patches to the Linux kernel would help, since they appear to reduce context switch time? DirectFB is not the solution! Write me a DirectFB app that will compile on GNU/Linux, all of the BSDs, SCO, Solaris, HP-PA UNIX, etc and I will use it. DirectFB is just like SVGALib, but with acceleration and such. I can see it being used for fast full screen games (I use the heroes of might and magic 3 dynamically linked binary and use the SVGALib display target of SDL because it plays _much_ faster that way). X is good. X is extendable. Most of X is a set of extensions (i.e. GLX, Shape, Render, etc). Is DirectFB able to be extended?..."
    author: "Clinton Ebadi (the unknown_lamer)"
  - subject: "Re: DirectFB"
    date: 2003-02-26
    body: "The window-manager running as a seperate process doesn't make a whole lot of sense to me. It should be a binary module that can be dynamically loaded by the X server. Sure, in that case, a crash in the window manager could cause the Xserver to crash, but then, how often does kwin crash on you? I have a feeling that the overhead of an external window manager has something to do with X's perceived slowness in certain window-resize operations. If you look carefully, you'll see that Qt/X11 is very snappy to actually draw. Even under a complicated style (Liquid) redraw itself is very fast. Actual benchmarks of the X server bear out that it is faster than the GDI for most operations. What really seems to be the hangup is *intelligent* redraw."
    author: "Rayiner Hashem"
  - subject: "Re: DirectFB (or why it won't work)"
    date: 2001-10-24
    body: "Well, I wish the DirectFB good luck, but this won't work..\n\nWhy? because you'll need to go and rewerite drivers from scrach.\n\nSo sure, you can write the Matrox drivers quite easily, and maybe few other cards - but you won't get any co-operation from nVidia, not ATI, nor S3, nor ST Micro (Kyro chip). You also won't have sufficient solution for 3D (DRI on DirectFB == slowness). \n\nAnd if thats ain't enough - it's based on some kernel hooks, which means if this got bug - your machine goes down - Panic mode, and even if you make it stable, Linus (nor Alan Cox) will ever put it inside the main kernel version, so your users will have to patch and recompile the kernel to use this. Not a very good idea..\n\nIts been said before and I say it again - yes, X is old but X is getting better all them. Take for example XFree 3D performance with NVidia drivers and compare it with Windows drivers - try a game on both of them and measure the FPS - you'll see that XFree FPS is maybe 2-4% slower then windows and consider that the entire X is loaded to memory while on Windows it's not.\n\nX had lots of ugly and old code in it - but they're killing old stuff and putting new features and extensions all the time (hence the new Xv extensions with texture support, RandR extension to switch to any resolution/color-depth on the fly, XRenderer support, HP port [coming soon] of true 3D on dual heads [SHS], Mesa improvments for OpenGL 1.3 and 2.0, etc...) - when do u think this DirectFB will have it? in 3 years? who's going to fund such a development? \n\nAnd I'm not mentioning applications of course - if you want it to be used, it got to run old applications (remember Motif? Athena widgets? TCL/TK?) which even if it will support - I can hardly imagine any serious ISV will support it..\n\nTo summorise my respond - X is pretty good solution for graphics, and it keeps being improved. DirectFB roads looks to me more and more like Berlin and GGI tried before (and you know whats with them today). I wish all of them good luck, but I can hardly belive they worth the effort.\n\nThanks,\nHetz"
    author: "Hetz Ben Hamo"
  - subject: "Re: DirectFB (or why it won't work)"
    date: 2001-10-24
    body: "There are already quite a few drivers made.  From the overview PDF:\n\n Matrox G200/G400/G450\n ATI Rage 128\n 3dfx Voodoo3/Banshee\n igs CyberPro (not released to the public)\n S3 Savage 3/4 series (not released to the public)\n NeoMagic\n nVidia TNT/Radeon series\n\nThey seem to be doing quite well so far on getting drivers.  It is true that XFree supports more right now, but the developers of DirectFB seem to be quite dedicated.\n\nAbout your kernel panic comment:  This is NOT TRUE!  Why do so many people think DirectFB is a kernel module or something?  DirectFB is a normal user space library and all the work of hardware acceleration is done in user space.  All it does is call the normal kernel framebuffer which is already in the kernel (yes, approved by Linus and Alan Cox) to set the video mode and do a few other things.  It will NOT bring down your kernel!\n\nBesides, what good does it do you when X crashes and locks your display that the kernel is still running underneath?  You can't recover your work.  The only thing you can do is telnet in and kill X, which kills all your applications.  It might save you a reboot and fsck, but with ext3 in the kernel now, there's no excuse to still be fscking.\n\nAbout old applications:  DirectFB has an X server, so you can still run Motif, Athena, etc.  No problem at all with support for old applications."
    author: "them"
  - subject: "Re: DirectFB (or why it won't work)"
    date: 2002-10-12
    body: ">> There are already quite a few drivers made. From the overview PDF:\n>> nVidia TNT/Radeon series\n\nnVidia is making radeons now?? ;p\n\nand to see what cards are REALLY syupported go to their webpage\n\nhttp://www.directfb.org/modules.xml\n\n"
    author: "Kenny Smith"
  - subject: "Re: DirectFB (or why it won't work)"
    date: 2003-05-27
    body: "Hi,\n\nLook! there it is! OpenGL support on one of the drivers for DirectFB! The Matrox series. Guess that were not just 3 years that passed by since I read this article....were it? And about Nvidia support.... I personally got information and very usefull headerfiles from guy that makes Linux overclocking utils at Nvidia. How's that? Another argument put to the ground. And documentation is still coming.\nDirectFB might be based on the kernel and might crash when the kernel has flaws, but look at the Nvidia drivers! They also have kernel stuff with them that requires to be loaded. \n\nFrameBuffer support is under developement and in 2.6 is it not experimental (as it was in <=2.4)\n\nBtw, did you even read the website? It was meant for embedded stuff. I wouldn't want to see X run on an embedded system. And believe me, I ran it on a 486. For 2 days, after wich I ditched it.\n\n\nOw, here are a few things against X:\n- Why is it network-based if graphics-intense stuff performs like shit?\n- Why is the client-server model flipped on it's back? The client dies when the display-server crashes (wich still is likely to happen)\n- Why do Nvidia drivers often lack SMP support and make people with 4 CPU's run Linux over 1?\n- Why does stuff like MPlayer perform _way_ better on directfb when compared to X?\n- Why is the X-windows protocol known to be more overhead than actual data?\n- Why does linux (read: X) still lack a general look?\n\n"
    author: "Roel"
  - subject: "Re: DirectFB"
    date: 2001-10-24
    body: "I'm not a particular fan of DirectFB.  It's a step backward. The directfb X server misses the point - you lose all the benefits of network transparency for _new_ applications.  This sort of thing will be increasingly, not decreasingly, important - think of wandering into a room with your bluetooth-enabled PDA and having the GUI for equipment in it appear automagically. \n\nThe X Window System is burdened with some legacy crap - but it is being phased out.  The whole font subsystem, for example, has just been reworked.  The XRENDER and forthcoming RandR extensions, in particular, give X whizz-bang eye-candy features like alpha blending etc, and will do it in a hardware accelerated manner on cards that support it (i.e. all modern big-name 3D cards).  And the DRI/GLX subsystem is nice, although AFAIK it's not to the point where across-the-network GL commands are also HW accelerated properly.  The modular redesign that happened in XFree 4 also means that it's easier for 3rd parties to write accelerated display drivers - for example, the NVidia and Matrox modules.\n\nPersonally, I think Display Postscript / PDF (MacOS X quartz) is a nicer way to do it all (since it uses natural units (inches or mm), rather than pixels, so will look good even on 600 DPI Monitor of the Future) - but, guess what? Display Ghostscript for X is coming along nicely too!\n\nAlso, X isn't really slow.  It was a tad slow when it came out.  Back then you were lucky to have 8 MByte on your $20000 computer.   These days, it's not slow, unless you're on a card that isn't accelerated, or have it misconfigured.\n\nOne thing to note is that on Windoze and MacOS, the GUI has its priority bumped so that it pre-empts background tasks.  Try negative renicing the X server for a similar effect on linux, giving a jump in responsiveness.   It even suggests this in the X documentation - most distros don't do it, because they're mainly still destined for servers.   The kernel pre-emption patch at http://www.tech9.net/rml/linux/ will also make your interactive sessions with your computer more pleasant.\n\nOh, and, it's always worth pointing out that the memory usage reported for X in top includes the entire adress space of your graphics card - so if you have a 32Mbyte card the reported size will be at least 32 Mbytes larger than your main system memory usage (possibly even more, since not all cards have a linear address space)"
    author: "David golden"
  - subject: "Re: DirectFB"
    date: 2001-10-24
    body: ">The directfb X server misses the point - you lose all the benefits of network transparency for _new_ applications.\n\nNo you don't:  You write your apps to QT or GTK and use either X or DirectFB depending on what you want.  Notice that practically nobody writes pure X apps anymore, and I don't expect anyone to start writing lots of DirectFB native apps.  DirectFB is just a faster way to display QT or GTK apps, an option for people who want speed and efficiency and don't need network transparency (i.e. the majority of KDE users).  Maybe high-performance games would be written directly to DirectFB but they aren't network transparent anyway so there's no point in writing them for X."
    author: "them"
  - subject: "Re: DirectFB"
    date: 2001-10-25
    body: "(a) I'm not so sure that DirectFB _is_ *significantly* faster than a properly configured X setup, even in the local case.  X has a massive head start in accelerated driver support, and the continuing modularisation of the X server is making it easier and easier to write new drivers.  X has a lot of good design features, as well as some bad ones.  The bad ones are being phased out, and new ones are taking their place.  3/4 of the complaints I hear about X have little basis in fact, and many seem to come from people who have never even RTFM of X.  Qt and Gtk are not actually good examples of toolkits which take advantage of X's strengths - they both go in for a fair bit of wheel-reinvention, since Qt (and newer versions of Gtk) are not targetted solely at X. \n \n(b) Re: Games.  One of the major points of GLX is to allow high performance 3D to be network transparent!  The current DRI and GLX implementation in XFree86 fails at this goal - but it's in the roadmap and the framework is there.  In thoery, one should be able to run an application on one box, and have its display hardware-accelerated 3D using the hardware of a completely different box running an X Server with the GLX extension. No you can't do that yet on linux, but it's a matter of time/developer effort - the standards for it are already written, and certain commercial unix systems have being doing this for years.\n\n(fc) People who do not understand unix^H^H^H^HX are doomed to reinvent it. Poorly."
    author: "David golden"
  - subject: "Re: DirectFB"
    date: 2001-10-25
    body: "> One thing to note is that on Windoze and MacOS, the GUI has its priority bumped > so that it pre-empts background tasks. Try negative renicing the X server for a > similar effect on linux, giving a jump in responsiveness. It even suggests this > in the X documentation - most distros don't do it, because they're mainly still > destined for servers.\n\n\nWhere does it suggest this?  I remember thinking of this on my own but not noticing a real difference (this years ago)\n\nDisplayPostscript is nice yeah but I want DisplaySVG ... combine that with transparency, rendering, decent fonts .... yeehaw! \n\n/usr/X11R6/lib/modules/libaccelsvg.a   \n:-)\n\n\"When do we stop playing \"catchup\" and start to crush the competition?\""
    author: "SVG wanter"
  - subject: "Re: DirectFB"
    date: 2001-10-25
    body: "Well, I use renice -10.   The difference can indeed be very slight, particularly if you're already using a modern computer with lots of mega-hurts :-) and a card that's decently supported under X.  Since the linux scheduler dynamically fiddles with priorities fairly enthusiastically anyway, the difference also isn't as huge as on older UNIX.  However, there is a perceptible difference in many cases. \n\nOne other thing to note is that if you're using xfs for no good reason (as many distros, irritatingly, do), then stop (there's very little need for xfs[ft|tt] these days, now that TrueType  support is integrated into the v4 XFree).   If you renice X itself, then it can end up starving the xfs process a bit, which means you could actually end up making things a little slower."
    author: "David golden"
  - subject: "Re: DirectFB"
    date: 2001-10-25
    body: "how do you stop using it and how would you add new fonts/ how would it effect my current font setup?"
    author: "Jeremy"
  - subject: "Re: DirectFB"
    date: 2001-10-25
    body: "If you are currently still using xfs on a local machine in order to use outline fonts (rather than the legitimate use of it for serving fonts to multiple thin client X servers)\n\nThis will add TT fonts to the old X font mechanism (there's a new, better, non-backward compatible mechanism based on the XRENDER extension for antialiased fonts - setting up that system is a different kettle of fish - but only applications rewritten to use the new extension can take advantage of it - fortunately, that includes KDE :-) )\n\n(a) make sure you are using XFree 4\n(b) Find your XF86Config file. \nIf they're not already present, add to \n<tt>Section \"Module\"</tt>\nthe lines\n<tt>Load \"type1\"</tt>\n(for adobe type 1 outline fonts) and\n<tt>Load \"freetype\"</tt>\n(for truetype fonts)\n\nNow, if you put directories containing truetype fonts\nin the\n<tt>FontPath</tt> lines of \n<tt>Section \"Files\"</tt>,\nthey'll just work (provided you've run <tt>ttmkfdir</tt> in directories in the normal fashion.\n\nRH and Mandrake keep the xfs configuration in /etc/X11/fs/config -\nif you copy the contents of \"catalogue\" section of that file into XF86Config\nas your FontPath, you won't lose anything. - there's also no point keeping\nthe line <tt>Fontpath \"unix:/-1\"</tt> in your XF86Config, as it tells the X Server to talk to xfs.\n\nNow you can disable xfs."
    author: "David golden"
  - subject: "Re: DirectFB"
    date: 2001-10-25
    body: "> One thing to note is that on Windoze and MacOS, the GUI has its priority bumped > so that it pre-empts background tasks. Try negative renicing the X server for a > similar effect on linux, giving a jump in responsiveness. It even suggests this > in the X documentation - most distros don't do it, because they're mainly still > destined for servers.\n\n\nWhere does it suggest this?  I remember thinking of this on my own but not noticing a real difference (this years ago). What would a suitable value be? \n\n-10 ?\n\nDisplayPostscript is nice yeah but I want DisplaySVG ... combine that with transparency, rendering, decent fonts .... yeehaw! \n\n/usr/X11R6/lib/modules/libaccelsvg.a   \n:-)\n\n\"When do we stop playing \"catchup\" and start to crush the competition?\""
    author: "SVG wanter"
  - subject: "Re: DirectFB"
    date: 2001-10-25
    body: "Is it possible to display both GTK+ and QT apps in one DirectFB screen?\nIs DirectFB a full windowing system?"
    author: "Stof"
  - subject: "Re: DirectFB"
    date: 2001-10-25
    body: "There isn't a QT port (yet?).  If there was, you could run GTK and QT apps at the same time, though.  I'm not sure what you mean by a \"full\" windowing system.  What's a partial windowing system?"
    author: "not me"
  - subject: "Re: DirectFB"
    date: 2001-10-29
    body: "Come on.\n\nI think that is time for X to go. Cause it's old and slow, dont give me this crap about apps being slow.\n\nNetowrk trans... who cares, tell me, what app are you using over the net. A real\n X (XP) terminal cost's the same as a new machine (AMD, duron). And you get much better performance, you have sound and you don't wait as long. Why slow the net even more down??? \n\nAnd machine administrators don't need X, just a terminal and vi."
    author: "Matej"
  - subject: "Re: DirectFB"
    date: 2001-10-29
    body: "1. X is not slow. It was not slow 14 years ago when it was created, and\nwith increasing bandwidth, it has only become faster. In fact, I think\nit was Jim Gettys who once said that an XPutImage over a network today\nis as fast as a local XCopyArea in 1987.\n\n2. I care, and I care very much. I use Emacs, matlab, xdvi, gv and\nother programs over the network all the time. What this has to do\nwith the hardware used is beyond me. Running X doesn't imply using an\nX terminal. In fact, most X servers today certainly run on ordinary\nPCs."
    author: "Bernd Gehrmann"
  - subject: "Re: DirectFB"
    date: 2002-04-13
    body: "I run my entire X session over the network, all day every day. I very much care for the network transparency. I get sound over the network too thanks to the EsounD project. And I get the very best in performance, as instead of buying 3 top-of-the-line machines to dot around my house I bought 1 even better server and 3 cheap (no hard disk, weak CPU, low RAM, but very decent gfx card, all standard x86 hardware) terminals. This saved me a packet and got me performance I could not otherwise afford.\n\nAt work we did the same, because the company wasn't going to shell out for \u00a38000's of sun server for each developer, and they would have looked ridiculous underneath our desks. Instead the company bought one such server, and an X terminal for each of us - a custom job of x86 hardware + linux + XFree86 that cost \u00a3250 a piece. A saving of \u00a360,000\n\nJust because YOU don't use the network transparency of X please don't assume no-one does. X network transparency is what makes X stand out above the crowd, and in the right set up, it can save one hell of a lot of money, administration and noise pollution.\n\nAs for DirectFB, I have little use for it right now because I never run any local apps, but its there and its small and it looks so very very good, so job well done and keep up the good work.\n\nIf I ever get a PDA, I want to see it running DirectFB  ;-)\n\n\nP.S. If X is so slow, why do I get better fps running Counter Strike under linux + wine + X than I do under windows?"
    author: "dgym"
  - subject: "Re: DirectFB"
    date: 2003-12-29
    body: "FACT: Encoding/Decoding of any protocol (GLX/X) is an CPU/Bandwidth overhead.\nFACT: For the fastest possable graphics software you need direct access to the underlying graphics hardware.\nFACT: For the fastest possable graphics software you WILL lose network transparency.\nFACT: DRI is NOT network transparent.\nFACT: DRI was created BECAUSE the x server protocol IS to SLOW.\nFACT: Remote GUI Access does not mean you MUST use X protocol.\nStupendus Examples of such are:\n     Remote Desktop Protocol (RDP)\n     Virtual Network Computing (VNC)\nFACT: RDP/VNC Does NOT require you to develop your applications with a specific toolkit to get the network transparency functionality. ( X does )\n\nBOTTOM LINE: Higher performance comes from direct access to the graphics hardware the X protocol cannot give you this, it will ALWAYS BE SLOWER! Network transparent GUI has a place, However it is not between a local user and his local graphics hardware. \n\n\n>> P.S. If X is so slow, why do I get better fps running Counter Strike under linux + wine + X than I do under windows?\n\nBecause it's using DRI, which is NOT network tranparent and gives your application direct access to the graphics hardware by bypassing the OLD and SLOW X protocol.\n\nCheers,\n"
    author: "Thrawn01"
  - subject: "Re: Nice"
    date: 2001-10-24
    body: "> 2. What makes KDE (and Gnome) slow is X-Windows.\n\nThat's not true. There are X11 Programs that run OK even on 486 computers, xfig for example. Why is kontour that much slower than xfig? They both run in X.\n\nYes, X11 adds some overhead, but thats not what makes Gnome and KDE so slow. KDE and Gnome are so slow because the huge number of libraries linked to them. They take some time to resolve symbols at start time and make cache less effective, just because they don't fit in it.\n\nStefan"
    author: "Stefan Heimers"
  - subject: "Re: Nice"
    date: 2001-10-24
    body: "Hear Hear!\n\nYou are completely right.\nBut lets just read slashdot and not repeat all this here.\nHowever, it is an important topic."
    author: "Jos"
  - subject: "slashdot?"
    date: 2001-10-24
    body: "Slashdot has been such insanity for years... how could one actually read slashdot (the comments) and glean anything out of it these days? Do people even bother making informed posts over there anymore?? (no offense to anyone who still does, if anyone does)."
    author: "jers"
  - subject: "Re: slashdot?"
    date: 2001-10-25
    body: "hey, I am a metamoderator over there.........all I have to say about /. is oye......the last 2 weeks the news has sucked, the editors are dumb and the post moderators are a bunch of 1d107 highschool weenies......I got modded down as over rated.......Im sorry, but why is that even a moderation? I earned my posting score or 2 damn it........\n\nless content crappy moderators....I only wish I had thier names when I moderated the moderators."
    author: "Jeremy"
  - subject: "Re: slashdot?"
    date: 2001-10-25
    body: "Chances are the site is being actively astroturfed my microsoft weenies - not necessarily under the official control of MS, but also by lots of VB \"programmers\" and so on whose livelihood depends on the perpetuation of shoddy MS \"solutions\".   You may scoff at such paranoid allegations - but I remember trolling of OS/2 newsgroups, which was eventually linked back to MS employees apparently acting under orders from their employers. \n\nIf you browse at +1, it's still bearable, if you browse at +2, you'll probably miss stuff that's been modded down by wrong-headed moderators."
    author: "David golden"
  - subject: "Re: slashdot?"
    date: 2001-10-25
    body: "they need to have a 20 question test, they need to be tested on all the moderations and if they can not mod the post correctly (acording to taco) then they should not moderate."
    author: "Jeremy"
  - subject: "Re: slashdot?"
    date: 2001-10-26
    body: "I just did a test on slashdot story search:\nMicrosoft: 1530 stories\nRMS: 0 stories!\nGnome: 386 stories\nQt: 0 stories\nTrolltech: 26 stories (which contain the word Qt)\nKDE: 0 stories\n\nI guess\n - their search can't handle small words,\n - or (put conspiracy theory here)"
    author: "Jos"
  - subject: "Always Up To Date Java Bindings!"
    date: 2001-10-24
    body: "Wow, always up to date Java Bindings! I could see it becoming difficult for Sun to resist bundling KDE with Solaris."
    author: "Squink"
  - subject: "Re: Always Up To Date Java Bindings!"
    date: 2001-10-24
    body: "i dont think, that sun will support the bindings. remember that this kind of stuff created serious trouble with microsoft. suns idea of java is of a platform, dales idea of java is of a language. so dont expect sun to be happy!"
    author: "cylab"
  - subject: "Re: Always Up To Date Java Bindings!"
    date: 2001-10-24
    body: "Microsoft tried to change the Java language in incompatible ways such as adding delegates, and they only selectively implemented features of the Java platform, while still wanting to pass it off as standard Java.\n\nThe Qt/KDE Java bindings are not claiming to be an implementation of the Java platform. They are use standard JNI + Java foundation classes. Sun has no problem with Apple's Cocoa Java api for Mac OS X, and the KDE Java bindings are in the same category of software as that.\n\nI read an article recently about the reasons why Sun chose Gnome, and Java wasn't mentioned once. I'm surprised they're not doing more Gnome/Java/Swing integration work, or helping with GTK+ Java bindings.\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: Always Up To Date Java Bindings!"
    date: 2001-10-24
    body: "It is cool to have Java bindings, specially when it needed almost no \"big\" developer effort to do so...\n\nBut I really would prefer if more effort was put into QtAWT (http://developer.kde.org/language-bindings/java/qtawt/developer.html), making it ready (including Swing!).\n\nThat way, we can have the best of both words: our Java applications will be more integrated into KDE, and we won't loose the portability. Also, it'd be one less library to load in memory (the JVM uses Motif)."
    author: "Marcelo"
  - subject: "Re: Always Up To Date Java Bindings!"
    date: 2001-10-25
    body: "'It is cool to have Java bindings, specially when it needed almost no \"big\" developer effort to do so...'\n\nI'm not 14, and I didn't knock these bindings up in a couple of weekends. It did involve \"big\" developer effort - some 15 months in total for C/Objective-C/Java including about 9 months solid for the Java bindings. I needed to work as a contractor on a trading system for a large bank for 3 years to fund this.\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: Always Up To Date Java Bindings!"
    date: 2001-10-25
    body: "I think he meant the \"alway up to date\" bit of the interface. I don't doubt this will make the big difference in the long run. GTK and GNOME have language bindings that involve manual creation of the apropiate class interfaces for any object orientated language and that might be it's downfall because the binding is tracking a (fast at times) moving target. With an automated build process as you describe the bindings can be kept up to date without too much manual interference so they are usable at any time compared to bindings against GTK/GNOME that require a manual update (and then sticking to the basis of that update for all that it binds to).\n\nContgratulations for the great contribution...\nregards\nKarl G\u00fcnter W\u00fcnsch"
    author: "Karl G\u00fcnter W\u00fcnsch"
  - subject: "Re: Always Up To Date Java Bindings!"
    date: 2001-10-26
    body: "\"I needed to work as a contractor on a trading system for a large bank for 3 years to fund this.\"\n\nYou mean you stopped working to get the utility and the bindings? That's terrible! Do you believe your tool and bindings will help you make up for that? For example, can you envision some commercial release for the tool?\n\nOne question about bindings. Would it be possible/feasible to add generation of Python bindings? PyQt seems to be coming along, but PyKDE never made it to KDE 2.X. So that is definitely a problem for me.\n\nThanks for your hard work."
    author: "J\u00e9r\u00f4me Loisel"
  - subject: "Re: Always Up To Date Java Bindings!"
    date: 2001-10-30
    body: "\"You mean you stopped working to get the utility and the bindings? That's terrible! Do you believe your tool and bindings will help you make up for that?\"\n\nYes I hope so! I don't intend to make money out of the bindings directly, only \nout of providing services such as wrapping existing C++ widgets for Java JNI, and Qt/KDE Java consultancy services if it catches on. \n\nI had originally intended to take a rest from contracting at banks for 6 months, and just 'test the water' with open source contributions. I did an Objective-C support patch for KDevelop, and Sandy Meir put up on the KDevelop site (I found that great fun!). I submitted a few KDevelop fixes as patches. \n\nBut the UK government introduced a nasty tax law called 'IR35' attacking freelance contractors. We are no longer allowed to fund software development from doing consultancy work such as contracting at a bank. Hence, I thought this was my last chance... and went for it fulltime from March 1999 onwards.\n\nPyQt and PyKDe are being actively maintained by the Kompany, along with help from third parties. I believe they have a version of Qt 3 designer for python in the works for use in conjunction with the BlackAdder IDE, and PyKDE 2.x bindings have been recently announced. \n\nShawn Gordan and the Kompany are doing a great job in packaging the PyQt bindings  with the BlackAdder IDE. Indeed, I emailed to him saying how nice it would be to do the same thing for the Qt/KDE Java bindings, and got a positive response (they were already thinking about it). It just needs a company like that to get behind the Java bindings in the same way, to make it happen. There are contractual issues to be negotiated with Trolltech when Qt language bindings are used for commercial development.\n\nSo BlackAdder or KDevelop 3.0 with the Java/Python/Ruby Qt/KDE bindings, and Qt 3 Designer Java support, will be a very nice combination. I hope experts on that subject should make as much money as people working on trading systems...\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: Always Up To Date Java Bindings!"
    date: 2001-10-30
    body: "\"...fulltime from March 1999 onwards..\"\n\nCorrection - March 2000 onwards, So 18 months in total of anti-IR35 tax protest.\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: Always Up To Date Java Bindings!"
    date: 2001-10-27
    body: "It's too easy to forget how much effort is put into everything.\n\nI doubt it was meant as lessening your efforts, but rather as praising them: It's too darn easy to have up-to-date Java bindings now, thanx to you. You describe an automated procedure that sounds too good to be true.\n\nThanx to you, we now have one of the very last interoperability issues dealt with.\n\nThanx."
    author: "Christian Lavoie"
  - subject: "Re: Always Up To Date Java Bindings!"
    date: 2001-10-27
    body: ">It's too darn easy to have up-to-date Java bindings now, \n>thanx to you. You describe an automated procedure that \n> sounds too good to be true.\n\nYeah, that was supposed to be the way I phrased the original post. Sorry for any misunderstanding. :-)"
    author: "Marcelo"
  - subject: "Scheme bindings?"
    date: 2001-10-24
    body: "I don't suppose anyone is working on scheme bindings? afaik, gnome has guile and there's always tk, but I'm a kde diehard and I've recently found programming bliss in the world of scheme. Of course, if I could compile python I might be persuaded to settle for that ;) Anyway, I'm just wondering..."
    author: "kdeFan"
  - subject: "Re: Python bindings?"
    date: 2001-10-24
    body: "Python bindings are being worked on. See http://www.thekompany.com/projects/pykde/. The page does say that KDE2 bindings are not available, but they are being worked on too, and AFAIK are quite close to being released."
    author: "chakie"
  - subject: "Re: Visual Basic bindings?"
    date: 2001-10-24
    body: "Together with QDesigner this can unleash a great power.\n\nWell, serious, scripting and Qt wil give nice RAD tools. Just don't integrate all bindings into KHTML and KMail...."
    author: "Jos"
  - subject: "Re: Visual Basic bindings?"
    date: 2001-10-24
    body: "well, first we need to have a stable VB interpreter that is compatable with MS VB, for the most part."
    author: "Jeremy"
  - subject: "Other Python bindings"
    date: 2001-10-24
    body: "Hey, there are some other python bindings with KDE/QT in sourceforge CVS. They are purposedly tiny (in needed memory size) and I expect them to stay this way. They may become quite useful, though. Give it a try if you wish, though my HD has some slightly more recent ones.\nThe project is named PYK and is hosted by sourceforge. Drop me a line here if you have any comment !"
    author: "xav"
  - subject: "Re: Other Python bindings"
    date: 2001-10-24
    body: "to the original poster you absolutely don't need to compile pyhton to write a binary binding for it ! all you need to do is to compile a shared library."
    author: "xav"
  - subject: "Some observations"
    date: 2001-10-24
    body: "The binding generator seems to be a nice hack. However, I have some oppinions about the generated code.\n\n1) Why are C-style casts used in the C++ code? (The bridging code). Why not use reinterpret_cast<> when casting from a C++-type (class) to a C-type (struct)? IIRC Bjarne Stroustrup wrote in some book that for the sake of potential memory alignment problems, cast operators should be used rather than the C-style cast. Although I know he was talking about down casts of virtual classes and objects, but nevertheless, can you really assume that (Type*)expr is the same as reinterpret_cast<Type*>(expr) on all platforms/compilers?\n\n2) The first argument to all methods is the instance pointer to the actual object. How about calling the parameter \"This\" or \"Self\" instead of \"instPointer\"? Yes, I know, it doesn't really matter to the user of the library, but \"This\" looks nicer than \"instPointer\". (And I think its more intuitive when reading the headers, but maybe thats because I'm coming from the C++ world)\n\n3) How about making all wrapper functions inline? The way I see it, the wrappers are only thin thin functions which just passes parameters and return values from and to the real C++ object. An inlined function would do the same trick, with little memory overhead and quite a lot in terms of speed. Since most operations within the functions are casts, I'm sure the compiler will strip away quite a lot of code, resulting in a \"clean C++ call\". I know this sounds silly since inlines are in header files and are processed during compile time (gcc will have a hard time at cracking C++ code in C-mode!), but isn't there an \"extern inline\" modifier which allows one to separate implementation from specification? Maybe that was a gcc-specific hack or maybe I just had a wet dream one night.. ;)\n\nOther than that, this seems to be really nice C-binding! Perhaps now it would be easier to port legacy Motif apps to KDE w/o the expense of making everything C++ correct.\n\nMy 0.02 SEK"
    author: "J\u00f6rgen S"
  - subject: "Re: Some observations"
    date: 2001-10-24
    body: "3) How about making all wrapper functions inline?\n\nYou answered this one yourself :\nbecause...\n> gcc will have a hard time at cracking C++ code in C-mode!\n\n> but isn't there an \"extern inline\" modifier which allows one to separate implementation from specification?\n\nThen it wouldn't be inline anymore, would it ? :-)"
    author: "Guillaume Laurent"
  - subject: "Re: Some observations"
    date: 2001-10-24
    body: "> > but isn't there an \"extern inline\" modifier which allows one to separate implementation from specification?\n\n> Then it wouldn't be inline anymore, would it ? :-)\n\nWell not necessarily. It's not inline text wise, so the compiler would not be able to expand the function during the first pass. But the linker could do some magic to the function itself and expand it where it is called.\n\nFrom http://www.gnu.org/software/libc/CONFORMANCE :\n\"glibc's use of extern inline conflicts with C99: in C99, extern inline\nmeans that an external definition is generated as well as possibly an\ninline definition, but in GCC it means that no external definition is\ngenerated.  When GCC's C99 mode implements C99 inline semantics, this\nwill break the uses of extern inline in glibc's headers.\"\n\nIt looks like there is support for extern inline in C99. However, I am not sure if it is possible to\n\nheader.h: extern inline T1 func(T2);\nand\nimpl.cpp: T1 func(T2 param) { ... }\n\nand get away with it. Does anyone know if it is possible to separate the implementation and specification of an \"extern inline\" function? If not, 3) is not a good suggestion :-)"
    author: "J\u00f6rgen S"
  - subject: "Re: Some observations"
    date: 2001-10-24
    body: "No, the linker can't do magic. It (well, as the name\nimplies ;-) \"links\" object code. Any inlining and\noptimization is done in the compiler on the level\nof an intermediate representation of the code.\n\n'extern' in C99 is something completely different.\nIt specifies that an inline function can be inlined,\nbut *in addition*, a \"real\" instance of the code has\nto be emitted by the compiler. In order to inline\na function, of course the compiler must have access\nto its implementation."
    author: "Bernd Gehrmann"
  - subject: "Re: Some observations"
    date: 2004-04-26
    body: "<p>Sorry, but linkers most certainly can (and should) inline fcn calls at link time. Read this: [Stale Link Removed]</p>"
    author: "John Schultz"
  - subject: "Re: Some observations"
    date: 2001-10-24
    body: "1) Um, I'm afraid I don't know. My C++ knowledge stops at the 1992 vintage ARM. I'm not a C++ fan (that's why I spend my time implementing bindings for languages I like better). I like to keep my C++ as simple as possible.\n\n2) Yes, I agree. The starting point for these C bindings was Roberto Alsina's QtC bindings that he did in 1997 with a python based generator. I changed the source code generator to perl/kdoc, but kept the generated code exactly the same  including the 'instPointer' name. It's never been important enough to change.\n\n3) Code bloat is a problem with inline functions, and the shared libs are already too large. No point in tuning without measuring though, and I doubt if the function calling overhead matters much.\n\nThe best way to port legacy Motif apps is to re-write the front end in KDE, I wouldn't recommend using the C wrappers for programming in C.\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: Some observations"
    date: 2001-10-29
    body: "C Wrapper functions should not be inlined so as to seperate the interface from the implementation.  Also, you want the user to be able to compile with a C compiler - not necessarily a C++ compiler.  That THE point of having a C API, afterall. On your first point, about C++ style casts - they won't work for older C++ compilers that are still in widespread use - I, for one, still use a 5 year old C++ compiler at work.  Nevermind the fact that reinterpret_cast<> generates a lot of runtime code that a normal C-style cast (or a static_cast for that matter) would not.  When the cast is KNOWN to be safe implicity you should never use reinterpret_cast."
    author: "JoeyJoeJoe"
  - subject: "Re: Some observations"
    date: 2001-10-29
    body: "reinterpret_cast should not generate runtime code at all. After all,\nit's just a type reinterpretation in the compiler frontend. What\nyou probably mean is dynamic_cast, which implies a test whether its\nargument is an instance of a certain class."
    author: "Bernd Gehrmann"
  - subject: "slightly premature announcement"
    date: 2001-10-24
    body: "Where is the documentation and example C code to make any kind of use of this?  Richard or Dre, please make more details available on the web!"
    author: "KDE User"
  - subject: "Re: slightly premature announcement"
    date: 2001-10-24
    body: "Yes, you're quite right! I haven't actually written any C programs with the bindings, as I only use them in conjunction with Objective-C. I wouldn't expect anyone to code directly in C, they are more intended to be a way of implementing bindings for languages which can't call C++ directly - eg PHP, Pascal, some Objective-C. I did have an email exchange once with a guy who had written a Qt C program, and we did get it working. I'll try and find it and post it here.\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: slightly premature announcement"
    date: 2001-10-24
    body: "Here is a QtC code sample from Dimi Shahbaz:\n\n-- Richard\n\n#include <stdio.h>\n\n#include <qtc_types.h>\n#include <qt_QApplication.h>\n#include <qt_QButton.h>\n#include <qt_QPushButton.h>\n#include <qt_QString.h>\n#include <qt_Slot.h>\n#include <qt_QObject.h>\n#include <qt_QMainWindow.h>\n#include <qt_QLabel.h>\n#include <qt_QWidget.h>\n\nqt_QApplication *app;\nqt_QWidget *qw;\nqt_QPushButton *qb;\nqt_QLabel *ql;\nqt_QObject *qo;\n\nint eventd(void *send, char *signal, void *recv, char *slot){\n \u00a0 printf(\"qb %d qw %d app %d\\n\", (int)qb, (int)qw, (int)app);\n \u00a0 printf(\"sender %d signal %s receiver %d slot %s\\n\", (int)send,\nsignal, (int)recv, slot);\n \u00a0 return 0;\n}\n\nint main(int argc, char **argv){\n \u00a0 int ret;\n\n \u00a0 app = qt_new_QApplication(argc, argv);\n \u00a0 qt_QObject_registerEventDelegate(eventd);\n\n \u00a0 qw = qt_new_QWidget(0, \"qw\", 0);\n \u00a0 qt_QApplication_setMainWidget(app, qw);\n \u00a0 qt_QWidget_showNormal( qw);\n\n \u00a0 qb = qt_new_QPushButton1( qt_new_QString5(\"Testing\"), qw, \"qb\");\n \u00a0 qt_QPushButton_setGeometry(qb, 10, 10, 60, 60);\n \u00a0 qt_QWidget_showNormal( (qt_QWidget *) qb);\n\n \u00a0 ql = qt_new_QLabel1( qt_new_QString5(\"Label\"), qw, \"ql\", 0);\n \u00a0 qt_QWidget_setGeometry( (qt_QWidget *) ql, 70, 70, 60, 20);\n \u00a0 qt_QWidget_showNormal( (qt_QWidget *) ql);\n\n \u00a0 ret = qt_QApplication_exec(app);\n \u00a0 printf(\"ret %d\\n\", ret);\n\n \u00a0 return ret;\n\n}"
    author: "Richard Dale"
  - subject: "Re: slightly premature announcement"
    date: 2001-10-25
    body: "Oh, THAT is why I never wrote a real signal/slot glue for QtC ;-)"
    author: "Roberto Alsina"
  - subject: "Re: slightly premature announcement"
    date: 2001-10-25
    body: "Hi Roberto\n\nThanks for helping to start all of this...\n\nThe C bindings, like your original QtC bindings, only have signals for the primitive C++ types - double, int, float, char *, etc. That is because I think the signal handling should done in the library at the level above, like the Objective-C bindings library.\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: slightly premature announcement"
    date: 2001-10-25
    body: "Yes, I know. It is just that when I read the example code, I remembered that it is pretty much impossible to write a useful implementation of signal/slots that is not also incredibly ugly to write code for :-)\n\nYes, I agree, it is better if signals/slots are handled in a language that can express it nicely, such as Obj-C and such."
    author: "Roberto Alsina"
  - subject: "Time to stop the Qt/KDE has no bindings myth"
    date: 2001-10-24
    body: "Every time someone writes about Qt/KDE there is this\nwhining \"you have no bindings ,but GTK has.....\".\nNow we have Java/Objective C/C, Python (soon KDE2) \nand Ruby(Only Qt? or KDE too). And I heard something\nof PerlQt a while back. More??\n\nCan someone make a page dedicated to the bindings,\nlike: http://erik.bagfors.nu/gnome/languages.html\nBut nicer looking of course :-)"
    author: "Morty"
  - subject: "Re: Time to stop the Qt/KDE has no bindings myth"
    date: 2001-10-24
    body: "I think this is an exelent idea."
    author: "Mario"
  - subject: "Re: Time to stop the Qt/KDE has no bindings myth"
    date: 2001-10-24
    body: "Well, there isn't a myriad of separately maintained KDE libraries,\nso there is no point in making matrices about what is supported and\nwhat not.\n\nOf course, there is a page dedicated to language bindings on\ndeveloper.kde.org. If you have any improvements to make, patches\nare certainly welcome :-)"
    author: "Bernd Gehrmann"
  - subject: "Java bindings using CNI"
    date: 2001-10-24
    body: "If there any plans to generate java bindings using the new CNI interface to GCJ?\nIMHO CNI is by far a supperior design compared to JNI, and has more in common with QT/KDE dessign (C++ API) than JNI, which is a C API.\n\nRivas."
    author: "R. A. Rivas Diaz"
  - subject: "Re: Java bindings using CNI"
    date: 2001-10-24
    body: "Yes, Thomas Kuhn is doing some great work on this. He has converted the bindings  to use CNI calls for the event handling and slot callbacks, but left the Java native methods still using JNI. CNI is more efficient that JNI, but I don't think anyone using the bindings would notice the difference, even if you converted all the calls to CNI.\n\nOne main advantage of using gcj, is that you can use gdb to debug instead of the toy jdb. gcj/CNI is Great Stuff, and I hope the combination of gcj/gdb/gnu tools/KDE Java bindings might be good enough to start to pull people off C++...\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Will this do anthing for ABI compatibility?"
    date: 2001-10-24
    body: "Would the C bindings to Qt do anything for or to binary compatibility across compilers?"
    author: "J. J. Ramsey"
  - subject: "Re: Will this do anthing for ABI compatibility?"
    date: 2001-10-25
    body: "Actually, yes. A QtC/KDEc program would use a stable ABI accross compilers.\nOf course your QtC/KDEc needs to be compiled with the same compiler as your Qt/kdelibs, but that should be no problem, since now they should come with the package in future versions."
    author: "Roberto Alsina"
  - subject: "Kio C-bindings"
    date: 2001-10-24
    body: "I Think the realy important thing is to port KIO-engine to C so programs like xmms can open smb:/ , http:/ and audiocd:/ like a normal kde-program does. Can't it be implanted in the C-library so every program uses it? It feels unnessesary that every desktop-system needs a complete IO-engine.\n\nSorry foks, for my realy bad english!\n\n  //Jimmy"
    author: "Jimmy Wennlund"
  - subject: "Re: Kio C-bindings"
    date: 2001-10-25
    body: "That is a very good idea, actually. KIO is one of the coolest examples of KDE integration : just by using the standard KDE file dialogs and using the provided classes for i/o, you suddenly have tranpsarent access from every app to every supported resource. I.e., it must have taken about 10 minutes to make kwrite the default html source viewer, since it just has kio open up an http connection to whatever file it wants. I think that Charles or Neil even looked at making it a kernel module at some point, though IIRC that didn't pan out very well :-)\n\nQuestion : would it be possible/advantageous to make KIO accessible as a daemon, so that you could fairly easily code an app to ither use KIO if it's there or use standard i/o if it's not, without a lot of messy compiler checking?"
    author: "Carbon"
  - subject: "Kio C-bindings"
    date: 2001-10-24
    body: "I Think the realy important thing is to port KIO-engine to C so programs like xmms can open smb:/ , http:/ and audiocd:/ like a normal kde-program does. Can't it be implanted in the C-library so every program uses it? It feels unnessesary that every desktop-system needs a complete IO-engine.\n\nSorry foks, for my realy bad english!\n\n  //Jimmy"
    author: "Jimmy Wennlund"
  - subject: "Re: Kio C-bindings"
    date: 2001-10-25
    body: "That is already in this very thingie!\nI am guessing, but I bet all the kio love is already into KDEc (Richard, how do you capitalize the name of the KDE binding?)"
    author: "Roberto Alsina"
  - subject: "Re: Kio C-bindings"
    date: 2001-10-25
    body: "Several of the kio classes are wrapped for C. But they tend to use quite fancy signals, and those aren't currently implemented (the signal handling is Roberto's original code). So we must try harder! On the other hand, I don't think we are that far off - it depends if people want to use the C bindings directly or not. I'd only thought of calling them via other language bindings, so the C runtime is pretty minimal as per the original QtC.\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: Kio C-bindings"
    date: 2001-10-25
    body: "Well, making the signal stuff handle more types is just a matter of doing it.\nI suggest the previous poster (not you, Richard ;-) should take a look. It is pretty much mechanical work! Any C programmer can write it!\n\nNote: C programmer. Which Richard and I ain't ;-)"
    author: "Roberto Alsina"
  - subject: "Great!"
    date: 2001-10-24
    body: "Does this mean I don't have to wait 2 minutes to compile a C++ file that #include <kde/qt/whatever.h>? ;-)\n(no I'm not kidding about the 2 minutes: Pentium 233)\nAnd will this enable better coorporation with GNOME?\nA Bonobo/XParts bridge would be cool."
    author: "Stof"
  - subject: "Re: Great!"
    date: 2001-10-25
    body: "Uh, no, that really doesn't have anything to do with what you're talking about, afaics.\nIt won't make C++ files compile faster, because it doesn't have much to do with C++ compilation. It just allows some other languages to use Qt and KDE classes and services.\n\nI think you mean KParts, not XParts. And, integration between the two would require a component library wrapper, or a universal component framework, not just a language binding. If GNOME used this, it would make KDE libraries a dependancy for compiling GNOME, which would be just insane.\n\nHmmm, for KParts integration though, there is a link farther down that mentions a universal component framework. You might want to take a look at that."
    author: "Carbon"
  - subject: "Re: Great!"
    date: 2001-10-27
    body: "FYI, Bonobo 2 is not GNOME-dependent. The ui stuff that requires GNOME is in a seperate package now."
    author: "Evandro"
  - subject: "Re: Great!"
    date: 2001-10-27
    body: "A component model without ui stuff is pretty uninteresting for\na GUI program."
    author: "Bernd Gehrmann"
  - subject: "Re: Great!"
    date: 2001-10-29
    body: "> A Bonobo/XParts bridge would be cool.\n\n And why would that be? Because you were told from\nGnome.org and incompetent journalists that bonobo \nis cool?\n\nLet pretend for a minute that it actually is cool.\n\nCan you name a Gnome application that uses bonobo\nand puts it to good use? One that we don't have a\nbetter KDE counterpart for already.\n\nWhich? The perpetual beta Evolution? Nautilus? \nSomething else that escapes me?\n\nCrap, utterly crap. How come then bonobo integration \nis your dream wish? Why not ask for better speed/stabi-\nlity/resource friendliness/some new app, etc?\n\nDo you really NEED bonobo integration? Because some\nlamers posting on slashdot told you it was cool? \n\nEven its creator is not in bonobo anymore. Go read\nMiguel De Icaza's site. He now prefers communication\na la manner de .NET.  \n\nIf I had a cent for everytime one asks for Gnome/Bonobo\nintegration I would be running Linux on a Sun Blade."
    author: "Jarvis Cocksucker"
  - subject: "Fortran?"
    date: 2001-10-25
    body: "I know that Fortran isn't as sexy as python or Java, but we do have a lot of users here who used F9x (x=0,5) for numerical computations. Usually, a simple GUI is needed to display graphs and control the main program. Because most compilers in Windows can use the Win32 API, the code is written for that particular API. And it would be great if code could be written for KDE (or QT, for that matter) so that it is portable. F9x has a number of features which would make it possible (I think). Other than that, a good job :-)"
    author: "d-Orb"
  - subject: "Re: Fortran?"
    date: 2001-10-25
    body: "You can write the GUI part in Python or C++, and call Fortran\nroutines for the numerical stuff."
    author: "Bernd Gehrmann"
  - subject: "Re: Fortran?"
    date: 2001-10-25
    body: "Yes, but that defeats the purpose of it! People could well write the\nGUI in C, but they won't: just because there are easy bindings for\nthe Win32 API in F9x, people use that! And yes, I do use python as\na glue mechanism (however, I like GTK+ better than QT for my stuff)"
    author: "d-Orb"
  - subject: "How's class inheritance handled in KDE/C bindings?"
    date: 2001-10-29
    body: "Just curious."
    author: "JoeyJoeJoe"
  - subject: "Re: How's class inheritance handled in KDE/C bindings?"
    date: 2001-10-29
    body: "If it's done like I did it, it is not handled at all.\nIf you want to create a new kind of widget, all you have to do is change its event handlers.\nSo, you should make a new function to draw stuff, and a new function to handle mose events, and keyboard events.\n\nBasically, from a \"regular\" Qt programmer, you are limited to inheriting from QWidget.\n\nUnless, of course, you do the sane thing, and write the tiny piece of C++ that you need to create the inherited class, and then wrap THAT class using Richard's tool.\n\nBut anyway, since C doesn't have any syntax for inheritance, you just have to hack it the hard way.\n\nOh, well, I suppose that adding extra member functions to a class *is* trivial, since it's just a naming convention ;-)"
    author: "Roberto Alsina"
  - subject: "Nice work. Good basis for a QT AWT."
    date: 2001-10-29
    body: "I took a look at the code and at the sample. The java binding seems very\nclean. It seems to be completely OO. \n\nThe JNI functions and their Java counterpart are created automatically.\nI looked at the sample ScribbleWindow.java and it looks like\nQT code in Java. Cool.\n\nI don't know if QT AWT (or better Swing) exists or not, but I think that it \nis a very good start. Such a system make the maintenance of the code a lot\neasier. The \"\"only\"\" thing that need to be done is the mapping between the \n2 APIs.\n\nMarc"
    author: "Marc"
---
<p><a href="mailto:Richard_Dale@tipitina.demon.co.uk">Richard Dale</a> recently pleasantly surprised me (and probably others) by <a href="http://lists.kde.org/?l=kde-devel&amp;m=100383424712136&amp;w=2">announcing</a> that he has committed C bindings for the KDE3/Qt3 libraries to <a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/#dirlist">KDE's CVS</a>. Richard generated the C bindings automatically using a hacked <a href="http://apps.kde.com/uk/0/info/id=559">kdoc</a>, with relatively little manual intervention. According to him, "<em>The bindings wrap about 800 classes [and] 13,000 methods, with 200k [lines of code] of C/C++ generated</em>." The same hacked kdoc can also generate Objective C and Java bindings, and Richard hopes to be able to consolidate generation of these <a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdebindings/">various KDE bindings</a> (Java/Objective C/C) with this one tool. Currently the C bindings for <a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdebindings/kdec/">KDE 3</a> and <a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdebindings/qtc/">Qt 3</a> are in KDE's CVS, with bindings for KDE 2.2.x/Qt 2.3.x awaiting resolution of a dynamic linking/PIC problem which affects the Objective C bindings (please contact Richard if you are knowledgeable in this area).</p>