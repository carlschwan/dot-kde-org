---
title: "KDE@OSDEM.fr, BlackAdder Demo, BiDi in Qt 3.x"
date:    2001-02-07
authors:
  - "numanee"
slug:    kdeosdemfr-blackadder-demo-bidi-qt-3x
comments:
  - subject: "Re: KDE@OSDEM.fr, BlackAdder Demo, BiDi in Qt 3.x"
    date: 2001-02-06
    body: "<p>May I happily recommend Quanta plus to the poster? There are duplicate anchor tags here with only one close. I thought konq was going crazy when it highlighted most of the text on the page and took me to an image.</p>\n</p><a href=\"http://quanta.sourceforge.net/\">quanta.sourceforge.net</a>\n</p>"
    author: "Eric Laffoon"
  - subject: "Re: KDE@OSDEM.fr, BlackAdder Demo, BiDi in Qt 3.x"
    date: 2001-02-07
    body: "Sorry, looks like Andreas fixed it. \n\nThanks,\nNavin.\n"
    author: "Navindra Umanee"
  - subject: "Re: KDE@OSDEM.fr, BlackAdder Demo, BiDi in Qt 3.x"
    date: 2001-02-07
    body: "Does BiDi support in QT & KDE means that we will be able to have fonts from different languages in same document?"
    author: "Zeljko Vukman"
  - subject: "Re: KDE@OSDEM.fr, BlackAdder Demo, BiDi in Qt 3.x"
    date: 2001-02-07
    body: "No, we can do that already. What it means is that we'll be able to handle right-to-left languages (Eg. Arabic) correcly throughtout KDE rather than only in KHTML.\n\nRich."
    author: "Richard Moore"
  - subject: "Re: KDE@OSDEM.fr, BlackAdder Demo, BiDi in Qt 3.x"
    date: 2001-02-07
    body: "\"No, we can do that already.\"\n\nAlready? In KDE editors? If I want to write something in ie. easteauropean I have to set fonts to ISO8859-2, but when I switch keyboard layout and fonts to ie. danish (ISO8859-1) then\nISO8859-2 chars disappear and change to question marks :(, and viceversa. I am talking about the same document. The only editor I can do that is Corels Word Perfect."
    author: "Zeljko Vukman"
  - subject: "Re: KDE@OSDEM.fr, BlackAdder Demo, BiDi in Qt 3.x"
    date: 2001-02-07
    body: "You need unicode fonts if you want to mix text from more than one codepage in a single document.\n\nRich."
    author: "Richard Moore"
  - subject: "Re: KDE@OSDEM.fr, BlackAdder Demo, BiDi in Qt 3.x"
    date: 2001-02-07
    body: "Thanks, Richard :) Of course, stupid me."
    author: "Zeljko Vukman"
  - subject: "Re: KDE@OSDEM.fr, BlackAdder Demo, BiDi in Qt 3.x"
    date: 2001-02-07
    body: "I was wrong. I actually use unicode fonts, and\nnothing - I can not write croatian and danish characters in the same document :("
    author: "Zeljko Vukman"
  - subject: "Re: KDE@OSDEM.fr, BlackAdder Demo, BiDi in Qt 3.x"
    date: 2001-02-07
    body: "I could write happily using east european after:\n\n- setting up xfree 86 4.01 (or only its fonts) and\n- changing the system locale to Hungarian (for example) in KDE 2.0.1\n\ngusthy"
    author: "gusthy"
  - subject: "Re: KDE@OSDEM.fr, BlackAdder Demo, BiDi in Qt 3.x"
    date: 2001-02-07
    body: "I have got the same problem with Japanese fonts.<br>\nSo far, I can only display them in Konqueror, but I cannot enter Japanese text e.g. in KOffice, KWrite, etc.<p>\nIf anyone knows how to get Japanese fonts and input working correctly with (a preferrably unpatched) KDE-2(.0.1), please contact me.\n<br>BTW, I am running SuSE Linux 7.0 at the moment."
    author: "David Banz"
  - subject: "Re: KDE@OSDEM.fr, BlackAdder Demo, BiDi in Qt 3.x"
    date: 2001-02-07
    body: "does anyone know whether it's already possible\nto create arabic documents (right to left) in\nKOffice?\nIf not, is it in the pipeline already?"
    author: "farid"
  - subject: "Re: KDE@OSDEM.fr, BlackAdder Demo, BiDi in Qt 3.x"
    date: 2001-02-09
    body: "No, not yet. KOffice doesn't have right-to-left writing support. But it will probably be possible once QT 3.0 will be released and KOffice will be based on it."
    author: "meni"
  - subject: "Re: KDE@OSDEM.fr, BlackAdder Demo, BiDi in Qt 3.x"
    date: 2001-02-09
    body: "Taking a look at the IMT-2000, I saw that it runs (ugh) opera. Any chance we can convince these guys to switch to Konqueror/Embedded? Also, it mentions it comes with \"Games\". Which games?"
    author: "Carbon"
  - subject: "Re: KDE@OSDEM.fr, BlackAdder Demo, BiDi in Qt 3.x"
    date: 2001-02-09
    body: "Hi Shawn,\n\nabout the BlackAdder demo: I'm a bit shocked to see that Windows is listed as a supported platform while SuSe rpm's aren't available. What's the reason for this?\n\nAlex"
    author: "Alexander Kuit"
  - subject: "Re: KDE@OSDEM.fr, BlackAdder Demo, BiDi in Qt 3.x"
    date: 2001-02-09
    body: "Why would you be shocked?  We did packages for RH, Mandrake, Debian, Slackware and Windows.  We just don't have ever combination of distros available to create packages for, and for some of them we have to rely on outside contractors to do the packages.  In the case of SuSE, the RH RPM's have been working for everyone that tried it, so we haven't gone to the effort of making a package that had so far not been demonstrated to be needed.  If you are finding that this isn't the case, and you know how to create SuSE packages, then drop me a private email and let's see if we can work something out."
    author: "Shawn Gordon"
  - subject: "Re: KDE@OSDEM.fr, BlackAdder Demo, BiDi in Qt 3.x"
    date: 2001-02-09
    body: "Ok. I've never tried to install RH rpm's on my SuSE system, because usually there are specific SuSE rpm's available. But if it works, it's fine. However, I think it would be nice to mention what package should be tried if a distro is not listed. Just for those who don't want to try until they find one that works ;-)"
    author: "Alexander Kuit"
  - subject: "Re: KDE@OSDEM.fr, BlackAdder Demo, BiDi in Qt 3.x"
    date: 2001-02-09
    body: "What would be even better is if all the distro's would stop messing with the environment from release to release.  It's getting pretty ridiculous the sheer number of packages we have to produce so that we support people.  I would say decent coverage would require about 10 packages be generated at a minimum.\n<br><br>\nWe should list it when we know it, but knowing for sure either requires us testing it, and if we could test it, then we could produce the package to begin with, or having someone try it and let us know if it works.  In this case some people tried it and it worked, so we should list it.\n<br><br>\nSend me an email if this doesn't work for you."
    author: "Shawn Gordon"
---
From <a href="mailto:elmoustico@toolinux.com">El Moustico</a>, a quick <a href="http://www.toolinux.com/lininfo/dossiers/salons/osdem/kde2/index.htm">article</a> in French sums up the KDE technology overview given by David Faure at <a href="http://www.osdem.org/">OSDEM</a>.

<a href="http://theKompany.com/">theKompany.com</a> has now made available a <a href="http://www.thekompany.com/products/blackadder/demovers.php3">demo</a> of <a href="http://www.thekompany.com/products/blackadder/">BlackAdder</a> due to the overwhelming number of requests.

In Qt news from last year, but nonetheless quite interesting, <a href="mailto:moshev@easybase.com">Moshe Vainer</a> points us to this <a href="http://www.xfree86.org/pipermail/render/2000-December/000650.html">teaser</a> from Lars Knoll: <i>"All of Qt will support bidi, arabic shaping and unicode compliant non spacing 
marks. The editing component (and a few other widgets as labels) are rich 
text capable in addition."</i>.  Konqueror already supports BiDi, and now the whole of KDE will get it. Nice screenshots too (<a href="http://trolls.troll.no/~lars/unicode1.png">1</A>, <a href="http://trolls.troll.no/~lars/unicode2.png">2</a>).  Another Qt oldie, submitted by our very own <a href="mailto:elter@kde.org">Matthias Elter</a>: <i>"The <a href="http://www.linuxdevices.com/news/NS7556981668.html ">IMT-2000</a> combines the features of a Linux PDA like the Compaq iPAQ with a cell phone and video
camera. The GUI is based on <a href="http://www.linuxdevices.com/news/NS5621773233.html">Qt/Embedded</a>."</i> Wow, and all that was from <i>last</i> year...





<!--break-->
