---
title: "KDE 2.2 Tagged, KDE 3.0 Branch Opened"
date:    2001-08-06
authors:
  - "Dre"
slug:    kde-22-tagged-kde-30-branch-opened
comments:
  - subject: "Thank you!"
    date: 2001-08-06
    body: "Thanks for all the hard work, KDE 2.2 is the best integrated, most comfortable desktop for linux. Looking forward to Three O.\n\nMaarten aka datadevil"
    author: "Maarten Stolte"
  - subject: "Can KFind do case InSEnsiTIVE search"
    date: 2001-08-06
    body: "I think KFind is the worst application KDE has and it was ignored right from KDE 1.1.\n\nI have complaint many times at bugs.kde.org about KFind's inability to do case insensitve search. \n\nIs it still that bad or improved in KDE 2.2? Or do I wait for KDE 3.0 to get KFind do case insensitive search."
    author: "Asif Ali Rizwaan"
  - subject: "Re: Can KFind do case InSEnsiTIVE search"
    date: 2001-08-06
    body: "If you don't like it, fix it!-)"
    author: "Steven"
  - subject: "Re: Can KFind do case InSEnsiTIVE search"
    date: 2001-08-06
    body: "Mine can do case insensitive searches."
    author: "Uwe Thiem"
  - subject: "Re: Can KFind do case InSEnsiTIVE search"
    date: 2001-08-06
    body: "you are free to use the command line toll called \"find\" which is imho the underlying engine used by KFind."
    author: "Darian Lanx"
  - subject: "Re: Can KFind do case InSEnsiTIVE search"
    date: 2001-08-06
    body: "feel free to uncheck \"case sensitive search\"\ncu"
    author: "ferdinand"
  - subject: "Re: Can KFind do case InSEnsiTIVE search"
    date: 2001-08-06
    body: "Mine has a checkbox for case insestivity.\n(kde 2.2)\nIf you dont like it, find another similar tool at www.freshmeat.net. Build one yourself, fix kfind, or use the 'find' command tool."
    author: "Nils O. Sel\u00e5sdal"
  - subject: "Re: Can KFind do case InSEnsiTIVE search"
    date: 2001-08-06
    body: "I find it very bad practice to tell someone \"fix it yourself\" or \"get another tools\" or other non-sense when there seems to be a real bug. This will lead to several programs with different bugs. I would much rather have a single bug free program than use 2 or 3 depending on which bug I'm trying to avoid.\n\nIf a KDE (or other program) has a flaw we should fess up and fix it.\n\nI would suggest you find the author/maintainer and e-mail them directly. Try to have an exact scenario that fails. If that fails and bugs.kde.org isn't helping I would put something out on one of the more common mailing list, but be sure to mention that you exhausted all other avenues first."
    author: "Don P."
  - subject: "Re: Can KFind do case InSEnsiTIVE search"
    date: 2001-08-06
    body: "Read the replys above yours. There are at least two that say KFind *does* have a way to do case insensitive searches.\n\nOne goes as far as to say \"uncheck the 'case sensitive search' box\" or something like that.\n\nSo: no, contacting the author asking him to do something he already did is not a good idea, and reporting it in bugs.kde.org repeatedly is ot a good idea either.\n\nIf you report a bug and it gets closed, see if it fixed. When the bug is closed, there should be an explanation, which may vary (\"that's not a bug\" or \"already implemented in CVS\", for example).\n\nIf you report a bug, and it gets closed and you don't like the explanation, of you feel it is not a real fix, then probably the developer did not understand your bug report, or what you got is as much as you gonna get.\n\nIn either case, reposting the bug is probably useless unless you add new information, or can convince the author of WHY he should do ehat you ask."
    author: "Roberto Alsina"
  - subject: "Re: Can KFind do case InSEnsiTIVE search"
    date: 2001-08-06
    body: "Well, you're right that the original poster did something stupid by asking for a feature which obviously is implemented. Thus, say the poster maybe that ha should just open his eyes, bit...\n\nNevertheless, excuse me that I shout,\n\nPLEASE STOP ANSWERING COMPLAINTS ABOUT SOFTWARE WITH \"USE THE SOURCE FIX IT YOURSELF\". We are not talking on the Linux developper mailing list!\n\nThat's exactly the language used by the people who want Linux (whatever Unix) to stay in their elite coder corner and not in the mainstream. That are the wrong propagandists for KDE, a desktop also for the average and novice user.\n\nI thought that this kind of response was only common on gnotices."
    author: "Alex"
  - subject: "Replies to user requests"
    date: 2001-08-07
    body: "Hi,\n\nactually, that's the way free software works. You have three choices.\n\na) do it yourself\nb) ask nicely if someone's going to do it\nc) PAY SOMEONE (like all Windows users do)\n\nactually four:\n\nd) live without it\n \nIMHO: If there is one thing more disgusting than develpers telling users to get productive themselves, then it's (particularly novice) users trying to force some feature down a developer's throat by nagging and bitching about.\n\nNote: I do not say that all users are like that. But some are, and they are not really helping the cause.\n\n\n-- Jens"
    author: "Jens"
  - subject: "Re: Replies to user requests"
    date: 2001-08-07
    body: "> c) PAY SOMEONE (like all Windows users do)\n\nI like this option.  Unfortunately there's no very direct way to do it; I suppose emailing the author and asking how much he or she would like to implement that feature, or something.  There are also those sites that allow you to post open source jobs and how much you're willing to pay, but I've never been very impressed with their interfaces.\n\nHere's something that might be cool: add a buyfeatures.kde.org or something which allows people to post features or bugfix requests, and how much they are willing to pay.  Other users could add their voice to a particular item by offering more money.  In order to make it \"real\", you'd need to have an interface where they enter a credit card number that is validated, so that people don't shirk out of paying.\n\nIn fact, I like this idea so much that I'd be willing to build the website, and get my company (which happens to provide online credit card processing) to donate the processing for free.  I suspect there may be some legal issues in transfering the money to the proper party, I suppose we'd need to look into this.\n\nWhat do you guys think, good idea?  If so, who should I contact about starting such a project?  Perhaps send this post to one or more of the KDE mailing lists?"
    author: "Adam Wiggins"
  - subject: "Re: Replies to user requests"
    date: 2001-08-07
    body: "This sounds like a really good idea, and probably a first dedicated to a purely open souce project. However there are probably more people who will say that they are interested in paying than will actually sign up with their credit card number."
    author: "CPH"
  - subject: "Re: Replies to user requests"
    date: 2001-08-07
    body: "This is a pretty interesting idea.  It may be better to move this discussion into a more appropriate forum like one of the KDE lists.\n\nJason."
    author: "Jason Tackaberry"
  - subject: "Re: Replies to user requests"
    date: 2001-08-08
    body: "I support this idea, and I'm glad to see that others do also. I think people who contribute to free software projects should be rewarded for their work. This is the most equitable way to give monetary rewards to developers that I can think of.\n\nSuch a system could be built on top of the bug (and wish) reporting system that currently exists (see bugs.kde.org). People could pledge to pay a small amount of money if wishlist item xxx was implemented. When a bugs.kde.org bug is closed it would indicate a buyfeatures.kde.org feature was implemented.\n\nThe main problem I see is that perhaps it is too early for such a system to be implemented. Implementing features can be a lot of work and being offered a small amount of money to implement a difficult feature would be insulting to the developers.\n\nPerhaps it would be better to start with creating a vote-a-wish site, and then later extending it into a site where people could pledge to pay money. There used be a vote-a-wish site and it was very popular but it was closed due to lack of maintenance. Again I think a vote-a-wish site should be tied into the bug tracking system.\n\nAlso please note that once money becomes involved suddenly all the fun can dissappear in something. There can be nasty disagreements (eg over whether a wish was really fulfilled) and the type of people attracted to the project could change for the worst. \n\nHaving said that I really believe an idea like this could work. Email me if you would like to discuss the most appropriate person to contact etc.\n\nBTW: I'm just speaking for myself here."
    author: "Don Sanders"
  - subject: "Re: Can KFind do case InSEnsiTIVE search"
    date: 2001-08-07
    body: "> PLEASE STOP ANSWERING COMPLAINTS ABOUT SOFTWARE WITH \"USE THE SOURCE FIX IT YOURSELF\". We are not talking on the Linux developper mailing list!\n\n> That's exactly the language used by the people who want Linux (whatever Unix) to stay in their elite coder corner and not in the mainstream. That are the wrong propagandists for KDE, a desktop also for the average and novice user.\n\nElite coder corner? I suppose it might be trendy to attempt to alienate someone who does something for free for you but more likely you are misinterpreting what you are being told. Contrary to what you might think those who develop free software do not have unlimited time and resources to do so. I myself am struggling to clear some business issues to return to the massive learning and working effort of free software development. It would be nice to think that my convictions, efforts  and struggles to transform myself into a coder on a level I could be satisfied would not be met with hostility that I was elitist and further had failed to meet your needs.\n\nI suggest you take a little time to read through some of the developer interviews that have been posted here. The main developer on kpresenter is a good example. He learned C++ to follow his vision. At some point in time you must decide if you will contribute to the free software community of merely be a critic.\n\nI can't suggest enough considering the debt we have to those who develop free software. In the end it's not just a matter of price but freedom and extensibility. At some time you're no longer a newie... maybe you'll want to switch off the TV and look at some source code?"
    author: "Eric Laffoon"
  - subject: "Re: Can KFind do case InSEnsiTIVE search"
    date: 2001-08-07
    body: "FWIW:\n\nrevision 1.17\ndate: 2001/03/28 12:45:58;  author: pfeiffer;  state: Exp;  lines: +11 -6\nconfiguration for case [in]sensitive search,\nby Owen Brydon <owen@dot.clara.net>"
    author: "Carsten Pfeiffer"
  - subject: "Re: Can KFind do case InSEnsiTIVE search"
    date: 2001-08-07
    body: "Obiously this person did not see the check-box for case-insensitive\nsearches.  If they are using a 2.2beta-1 or equiv (what I'm using)\nthen I'm not sure why they did not see the check-box.  However,\nthey could be using an older version which either does not have the\nability to do case-insensitive searches, or is not easy to find\nwhere to turn off case-sensitive searches.\nInstead of slaming this person and telling them to \"go and write their own\",\ntry helping them.  That type of pissy attitude does no one good, especially\nwhen it is on a official KDE forum -- besides it being a horribly\nrude thing to say.  Usually the only people I hear saying things like\nthat, generally are people that use and/or develop-for OSS projects.\nI've heard responses of that type in perl, c/c++, mozilla, and gnome irc channels.  Please\ndo not let KDE become like that -- which is one of the reasons I\nliked KDE from the beginning.  When I went in their channel asking them for help,\nI didn't find some asshole bitching at me because I don't know what\nthey know.\nYou have to be able to tollerate your users and your fellow users."
    author: "Weziko"
  - subject: "Re: Can KFind do case InSEnsiTIVE search"
    date: 2001-08-07
    body: "Case insensitivy searching has been in KDE *AT LEAST* since 2.1 as it's the version I'm using and it's available there - and *VERY* easy to find, it's a simple checkbox.\nObviously this person did not check for that feature *FOR MONTHS*, nevertheless he started his message with  \"I think KFind is the worst application KDE has\" and, according to his own post, has flooded bugs.kde.org with duplicate feature requests for a feature that probably had already been implemented at this time.\nHe's one of the persons who don't want to pay for software but want its developers to do what they want - immediately, of course.\nTo make a feature request is okay but a behaviour like his is simply arrogant and people like don't deserve any better treatment.\nAdditionally, if you ask me, he's no real person but a troll.\n\nGunter\n\nPS: I dislike answers like \"implement it yourself\", too, but I even dislike Ali's behaviour."
    author: "Gunter Ohrner"
  - subject: "Re: Can KFind do case InSEnsiTIVE search"
    date: 2001-08-08
    body: "Everyone is being critical of the person that posted the question, yet nobody seems to actually be able to READ the question!  This person is ASKING if the kfind in version 2.2 has case insensitive search capabilities.  If they are asking this, then they obviously haven't tried the 2.2 version.\n\nNot everyone knows how to go about finding out if a feature has been added or a bug has been fixed.  I certainly hope that some of you people don't work for actual software development companies, as I'd quickly drop your software product if I got responses such as this.  I do work for one, and know that often times people are edgy for one reason or the other, and that simply ignoring this, and answering the question can go a long way.  Yes, I understand that the developers aren't getting \"paid\" in money to write this code.  There are other benefits to being an open source coder.  If someone writes open source, and is bitter about things like this, then maybe they should stop?\n\nDon't get me wrong, the tone of the original post was unnecessary.  But that doesn't excuse the bashing that followed.\n\nOh, and to answer the question, based purely on other posts in this thread, I would guess that 2.2 has the feature, so no need to wait for 3.0 version."
    author: "Greg Goodrich"
  - subject: "Re: Can KFind do case InSEnsiTIVE search"
    date: 2001-08-08
    body: "> asking this, then they obviously haven't tried \n> the 2.2 version.\nI guess that if he had just asked if this feature is available in KDE 2.2 noone had reacted this way. He would simply have gotten the answer that this feature was available at least since KDE 2.1 and maybe in KDE 2.0, too. I'm not saying that I like the way some people answered the question, I just say Ali could not expect any other reaction. I'm really sure he's just a troll so let's stop that thread.\n\n  Gunter"
    author: "Gunter Ohrner"
  - subject: "Re: KFind, case InSEnsiTIVE? Yes, yes, and yes!"
    date: 2001-08-15
    body: "I believe for as long as I've used kfind (1.1 or so) it's done case insensitive searches by unchecking the 'case sensitive' box.\n\nEnd of question, end of story!"
    author: "Trevor Semeniuk"
  - subject: "good work"
    date: 2001-08-06
    body: "Thank you for all the hard work. KDE 2.2 will be released next week .. cool, more time to fix .. more stable KDE is ... \n\nDon't know if the decision of going to KDE 3.0 early next year is a good idea .. but all the best ..\n\nyou did it again .. KDE team ..\n\nYOUR DIED HARD FAN\n\nTom"
    author: "Tom"
  - subject: "Re: good work"
    date: 2001-08-06
    body: "I'm not sure about going straight to KDE 3, also. From reading the mailing lists, the idea is to port to QT 3 (which is a good idea), and then sit there for ages because they can keep binary compatibility. The problem is that GCC 3 and GCC 3.1 (which will be out in April 2002) will not be binary compatible. So, as long as KDE 3 comes out after GCC 3.1, everything will be fine."
    author: "Jon"
  - subject: "Re: good work"
    date: 2001-08-06
    body: "We rewarded this issue in the discussion. But the point is that Qt 3 is the platform that development outside of KDE is going to move to right away and it solves so many technical problems that given the time GCC needs, we will provide a complete platform that offers everything. If GCC causes an incompatibility lateron, it is mainly the distributions that need to take care of this. Offers of binary packages of third-party KDE applications will be done by them anyway or appear on apps.kde.com for the particular distributions. But sourcecodewise, you can still compile the tarball on any system like before. To really use the gcc 3.1 compiler on a distro for everything, the whole thing needs to be recompiled, not just KDE (which I think wouldn't be a problem either as the distributions usually provide the compiler as a binary, so they will provide re-compiled packages of KDE as well and what else is needed). Someone who compiles gcc himself can be expected to recompile KDE as well IMHO. The advantages of KDE 3 over 2.2 will be so tremendous that for the little number of people that will run into problems with gcc 3.1 because the want to have the latest stuff but are not able to recognise how their system works codewise, they can always join irc.kde.org #kde to ask for advice whats going wrong on their system by then."
    author: "Ralf Nolden"
  - subject: "Re: good work"
    date: 2001-08-06
    body: "Hi Ralf,\n\nJust out of curiousity, what technical problems does porting to Qt3 solve?\n\nThanks,\nJason."
    author: "Jason Tackaberry"
  - subject: "Re: good work"
    date: 2001-08-06
    body: "you will be able to use data-aware widgets and the other cool new stuff in qt3."
    author: "Jasper"
  - subject: "Re: good work"
    date: 2001-08-06
    body: "What's a data-aware widget?\n\nJason."
    author: "Jason Tackaberry"
  - subject: "Re: good work"
    date: 2001-08-06
    body: "a data-aware widget is a field which 'knows' that that data it contains comes from a database. so when you press the next record button, the field changes to the next record.\n\nThis will make building a database frontend much easier."
    author: "jasper"
  - subject: "Re: good work"
    date: 2001-08-07
    body: "You will have:\n-database support\n-component support (see the tutorial from M.Ettrich, other news)\n-the styles are components which means pure Qt programs will look like KDE apps\n-QSettings works like KConfig that you have a universal way to store settings in Qt apps\n-bidirectional language (bidi) support\n\nRalf"
    author: "Ralf Nolden"
  - subject: "Re: good work"
    date: 2001-08-07
    body: "I'm not clear about what you're saying.\nDo you mean that GCC 3.0 and 3.1 will not be binary compatible with each other, or are you referring to the fact that 2.95 and 3.0 are binary incompatible?\n\nAlso, what versions of KDE are, or will be, able to be compiled with GCC 3.x. I understand that people have experimented with compiling the 2.2 beta under 3.0, but have run into \"issues\"."
    author: "AJ"
  - subject: "Re: good work"
    date: 2001-08-07
    body: "Yes, GCC 3.0 and GCC 3.1 will not be completely binary compatible."
    author: "Jon"
  - subject: "Re: good work"
    date: 2001-08-06
    body: "Don't you guys ever sleep ? :-P"
    author: "Ariya Hidayat"
  - subject: "Re: good work"
    date: 2001-08-06
    body: "No. definetly not. Weekends mean 50 hours KDE coding minimum, workdays mean relax after 3am until you finished thinking about the code of the next feature, then implement."
    author: "Ralf Nolden"
  - subject: "Re: good work"
    date: 2001-08-06
    body: "50 hours on weekends?  Please quit being such slack bastards.  I want my desktop now damnit, and you owe it to us to produce results!\n\nNo,seriously, if this is for real, take a vacation!  Hell, I bet if you start a PayPal account called the \"KDE Vacation Pool\" you'd be surprised what you'd get. :)\n\nCheers,\nJason."
    author: "Jason Tackaberry"
  - subject: "Re: good work"
    date: 2001-08-06
    body: "Ralf: what about your preparations for your exams?\n\nGreetings,\nTackat"
    author: "Tackat"
  - subject: "Re: good work"
    date: 2001-08-06
    body: "Nitpicker ;)\n\n</daniel>"
    author: "Daniel Molkentin"
  - subject: "Re: good work"
    date: 2001-08-07
    body: "damn nitpicker, to be precise. That's my dad's job, not yours :)"
    author: "Ralf Nolden"
  - subject: "Re: good work"
    date: 2001-08-06
    body: "Hallo Ariya! Glad to see, that you are still around here. :) Did you get my Email about Netraider? Can't check it now, cause I'm at work. \n\nI hope you are the Ariya that I think you are. ;)\n\nSorry if not. =)"
    author: "Spark"
  - subject: "Re: good work"
    date: 2001-08-07
    body: "Yep, who else ? :-P"
    author: "Ariya Hidayat"
  - subject: "Re: good work"
    date: 2001-08-09
    body: "Who knows, maybe there are a lot of Ariya Hidayat's in the World. ;)"
    author: "Spark"
  - subject: "Re: good work"
    date: 2001-08-09
    body: "Well, at least Google says it's only me :-P"
    author: "Ariya Hidayat"
  - subject: "Re: good work"
    date: 2001-08-06
    body: "Amen to that. You guys rock. (not worthy)"
    author: "Pablo Liska"
  - subject: "Re: good work"
    date: 2001-08-06
    body: "it rocks! Thank you, guys. I downloaded & installed the rpm's (not a hacker, haha) and showed it to my wife and said \"this is the first real threat to Windows\", speakin from a notahacker-point of view. Vamos arriba!"
    author: "sgipan"
  - subject: "Re: good work"
    date: 2001-08-06
    body: "Where did you get the RPMs? I found nothing on ftp.kde.org and other pages..."
    author: "Steffen Hein"
  - subject: "Re: good work"
    date: 2001-08-06
    body: "easy: \n\ngo to http://www.kde.org/mirrors.html and pick a mirror, go to \"unstable\",\nthen \"2.2beta1\" (or sth. like that), then choose your distro etc. pp.\n\nFor you in Germany here is one of many mirrors: \nftp://ftp.archive.de.uu.net/pub/kde/unstable/2.2beta1/ \n\nhave a lot of fun... (haha) and you'll see: IT ROCKS!\n\nStephan"
    author: "sgipan"
  - subject: "Re: good work"
    date: 2001-08-07
    body: "I meant 2.2 final ;)\n\nBTW: The dot.kde.org server seems to have problems (again). It took >10 minutes to load the page..."
    author: "Steffen Hein"
  - subject: "Slow server (Was Re: good work)"
    date: 2001-08-07
    body: "I think your server is suffering the slashdot effect."
    author: "Tony S"
  - subject: "Slow server (Was Re: good work)"
    date: 2001-08-07
    body: "I think your server is suffering the slashdot effect."
    author: "Tony S"
  - subject: "Re: good work"
    date: 2001-08-06
    body: "easy: \n\ngo to http://www.kde.org/mirrors.html and pick a mirror, go to \"unstable\",\nthen \"2.2beta1\" (or sth. like that), then choose your distro etc. pp.\n\nFor you in Germany here is one of many mirrors: \nftp://ftp.archive.de.uu.net/pub/kde/unstable/2.2beta1/ \n\nhave a lot of fun... (haha) and you'll see: IT ROCKS!\n\nStephan"
    author: "sgipan"
  - subject: "Re: good work"
    date: 2001-08-06
    body: "easy: \n\ngo to http://www.kde.org/mirrors.html and pick a mirror, go to \"unstable\",\nthen \"2.2beta1\" (or sth. like that), then choose your distro etc. pp.\n\nFor you in Germany here is one of many mirrors: \nftp://ftp.archive.de.uu.net/pub/kde/unstable/2.2beta1/ \n\nhave a lot of fun... (haha) and you'll see: IT ROCKS!\n\nStephan"
    author: "sgipan"
  - subject: "Dirk Mueller on KDE 3.0 branch"
    date: 2001-08-06
    body: "See this mail from the brand spanking new release coordinator for more details:<br>\n<a href=\"http://lists.kde.org/?l=kde-core-devel&m=99709779200822&w=2\">lists.kde.org</a>"
    author: "ac"
  - subject: "Excellent work"
    date: 2001-08-06
    body: "You guys are great!  Keep up the good work (but take a vacation too).  I've been following kde from before 1.0, and have been impressed to say the least.\n\nKDE 2.1.1 finally pulled me away from my windowmaker desktop.  I'm excited to see 2.2, and also excited for the next version of KOffice.\n\nYou guys have the best desktop out there (I mean _ANY_ os).  I know a bunch of windows users who are begging me to get them set up with KDE.\n\nCheers,\nPeter"
    author: "Peter Schmiedeskamp"
  - subject: "Does it work on Solaris 8?"
    date: 2001-08-07
    body: "Just wondering -- do you guys check if it compiles (and works) on Solaris before tagging the branch?   I have had a lot of problems compiling KDE 2.1 using Sun's compiler suite (rather than GCC).\n\nNow that UltraSPARC workstations are almost as affordable as PCs, I think a cross build on Solaris/SPARC is a must."
    author: "Alex Keahan"
  - subject: "Re: Does it work on Solaris 8?"
    date: 2001-08-07
    body: "It should... \ni can't affrod a sun workstation.... my computer cost RM 1880! (approx USD1 = RM 3.8, Ringgit Malaysia)"
    author: "Rajan Rishyakaran"
  - subject: "Re: Does it work on Solaris 8?"
    date: 2001-08-09
    body: "You should try to build it with GCC. Most developers don't have access to the Solaris compiler so all the can do to make it work properly is accept patches."
    author: "Evandro"
  - subject: "Re: Does it work on Solaris 8?"
    date: 2001-08-10
    body: "Well, yes, I can build it with GCC, but I don't\nwant to. :)\n\nGCC for x86 is almost as fast as Sun CC for x86,\nbut Sun's SPARC compilers are *significantly*\nbetter than GCC.\n\nDevelopers who can't afford a Sun Forte C/C++\nlicense, can download a 30-day evaluation\n(Try-and-Buy) version.  Sun's workshop is really\nthe best development environment available for\nUNIX, with a built-in run-time access checker,\nmemory leak detector, performance analyzer and\nfull support for multithreaded programs.\nDevelopers should give it a try if only to find\nbugs in their code.\n\nWhere should I send the patches, anyway?"
    author: "Alex Keahan"
  - subject: "Which version off gcc best for compiling KDE 2.2"
    date: 2001-08-07
    body: "Will KDE 2.2 build correctly with gcc 3.0 or is one of the 2.95.x releases considered more reliable?  Which gcc release is best for build KDE?\n\nThanks,\n       Jeff"
    author: "Jeff Lasslett"
  - subject: "Re: Which version off gcc best for compiling KDE 2.2"
    date: 2001-08-07
    body: "My question is if 3.0.1 will be OK to compiled 2.2."
    author: "Fr\u00e9d\u00e9ric L. W. Meunier"
  - subject: "Re: Which version off gcc best for compiling KDE 2.2"
    date: 2001-08-07
    body: "Using 3.0.1 cvs KDE compiles through. And it also works, quite nicely. In fact everything works (didn't try arts though) except the help, because gcc3 generates buggy code for libxml which breaks the complete help system in KDE."
    author: "anonymous"
  - subject: "GNU Autoconf 2.52 ?"
    date: 2001-08-07
    body: "Does it work with this version or you still need 2.13 ?"
    author: "Fr\u00e9d\u00e9ric L. W. Meunier"
  - subject: "thank you"
    date: 2001-08-07
    body: "Just a simple thanks to all those that contributed to stuff I use *every* day.\n\nHopefully, as I get to understand more, I can also submit my little bit."
    author: "Andy S"
  - subject: "Great Appreciation"
    date: 2001-08-07
    body: "I would like to add my thanks to the list of many here for a wonderfull product.  I too use KDE every day and appreciate the effort you put forth in my behalf.  Keep up the good work!!"
    author: "Paul Strange"
  - subject: "COO/President - VajraMedia Corporation"
    date: 2001-08-07
    body: "I wanted to personally thank all of you who have worked on this very important project.\n\nBecause of your committment to excellence, the computing world has an environment which users from all backgrounds can enjoy and be productive.\n\nSincerely,\n\nNicholas Donovan"
    author: "Nicholas Donovan"
  - subject: "structure of KDE CVS tree"
    date: 2001-08-08
    body: "Where i can find something like ftp://ftp.freebsd.org/pub/FreeBSD/branches/-current/src/share/misc/bsd-family-tree or www.freebsd.org\\doc\\en_US.ISO_8859-1\\books\\handbook\\current-stable.html but about KDE?\n\nAnd what i can get through cvsup.kde.org"
    author: "Victor G. Bolshakov"
  - subject: "Just Really Impressed!"
    date: 2001-08-08
    body: "Id just like to say how impressed at how far KDE has come.  I first tried it in mid-1999, and was quickly discouraged at how less friendly it was than Windows.  After writing it off as just too ugly for a couple years I finally tried 2.0 a few months back, and now with the 2.2 beta this beast rocks!  So many of the \"little things\" that I just couldnt do without (like mouse-wheel support, readable fonts, stable drag-n-drop, graphical configuration, etc) might seem trivial to the seasoned linux user but for a newly-converted Windows user they are so important.  It took something like KDE to get me to switch, now I'll never go back!\n\nP.S.  I've been able to figure out how to customize everything except for one thing -- the KDE \"resize window\" mouse cursor icons.  These icons dont seem to be in /usr/X11R6/lib/X11/fonts/misc/cursor.pcf.gz like the pointer.  Any idea where to find them?"
    author: "Ryan Agler"
  - subject: "Re: Just Really Impressed!"
    date: 2001-08-08
    body: "KDE has really an awful lot of configuration\noptions, except when it comes to font sizes :(\n\nInstalling 2.2 I saw that they removed some\nentries in the \"Fonts\" control panel, and now\nI can't even control the _exact_ size of text for file manager icons (konquerer).\n\nHaving 128 dpi resolution, all fonts come out\nway too big.\n\nThere are also many places, where font size is\nfixed, and does not respect user settings: date picker, workplace numbers, etc.\n\nI had to switch back to 2.1.2 again, because of the huge text in the file manager."
    author: "Christoph"
  - subject: "Re: Just Really Impressed!"
    date: 2001-08-08
    body: "File Browsing -> File Manager -> Appearance -> Font Size"
    author: "Tackat"
  - subject: "Re: Just Really Impressed!"
    date: 2001-08-09
    body: "Nice, but it does only offer 5 fixed sizes, with\nthe smallest even to big for my taste."
    author: "Christoph"
  - subject: "Re: Just Really Impressed!"
    date: 2001-08-09
    body: "Try ~/.kde/share/config/kdesktoprc\n\nLook for StandardFont.\n\nI think that's what you what?"
    author: "Anonymous"
  - subject: "Re: Just Really Impressed!"
    date: 2001-08-09
    body: "Please make sure you are really running KDE 2.2. Control Center > File Browsing > File Manager > Appearance now allows to set any Font Size instead of just 5 predefined sizes. The same is true for Konqueror BTW\n\nThanks KDE team, this makes the control panels finally useable on high resolution displays!"
    author: "Magnus Kessler"
  - subject: "Champions of linux desktop"
    date: 2001-08-09
    body: "Well done team. I don't care what anyone says. You're personally responsible for linux on home desktops being a viable option for anyone, and the best option at that. The fact that kde works on any other OS is a bonus to them. Linux is the real winner."
    author: "ck"
  - subject: "Konsole fonts"
    date: 2001-08-09
    body: "Could the new konsole in kde3 support ANSI fonts like VGA, etc? That would be nice :)"
    author: "exceed"
  - subject: "Re: Konsole fonts"
    date: 2001-08-10
    body: ">> Could the new konsole in kde3 support...\n\nIt's gonna damn well have a Schema editor so you can edit and save your Schemas.  Even if I have to code the damn thing.  People talk about \"scratching the itch\" with open source... how about \"wacking the mole\"?  Also, if I can get into KDE enough (read: get enough time, get another assistant programmer) so I feel comfortable writing it, I'm adding exposed dcop functions so you can script things like \"open five shells, telnet to my two primary servers in the first two, and jump to root in the third, set the Schema to pink on three, light blue for the first two, and pale yellow for the default, open emacs in 5\".\n\n(Not that I do that sequence often 8) )\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Konsole fonts"
    date: 2001-08-10
    body: "You can already make custom konsole schema, and save them.  You just can't access it from the konsole menu.  \n\nGo to Control Center : System : Konsole, hit the schema tab.\n\nThis should really be part of the konsole menu (as of 2.2beta1 it isn't).\n\nJason"
    author: "LMCBoy"
  - subject: "Where is KDE 2.2"
    date: 2001-08-13
    body: "Today is August 13, 2001 and I can't get into ftp.kde.org to download it.\n\nI only found it on Suse but I use RedHat.  Anybody know any of the mirrors has KDE 2.2?\n\nThanks,\nSW"
    author: "SW"
  - subject: "Re: Where is KDE 2.2"
    date: 2001-08-13
    body: "I can't find it either.  One would have hoped that dot.kde.org would atleast post information to explain another delay of KDE 2.2"
    author: "Rick J"
  - subject: "Re: Where is KDE 2.2"
    date: 2001-08-13
    body: "That's right, I want my money back :-)"
    author: "Benoit WALTER"
  - subject: "Re: Where is KDE 2.2"
    date: 2001-08-13
    body: "I've been trying ftp.kde.org daily for quite some time in the hope to find 2.2 online. I also can't reach ftp.kde.org today. A definitive ETA would be helpful! Both my Korganizer and Kmail have bugs that prevent me from taking full advantage of these tools and I'm anxious to get the latest RPMS.\n\nI just found Linux-easy.com's daily builds. But they're for Redhat and are for the new KDE3 tree - I'd love to see a Mandrake service that offers weekly builds of KDE!"
    author: "Sean Pecor"
  - subject: "Re: Where is KDE 2.2"
    date: 2001-08-14
    body: "Check www.kde.org, it appears that a server died, breaking KDE FTP (among other things). I hope they can bring it back up, I'm shaking in my boots in anticipation."
    author: "Carbon"
  - subject: "Re: Where is KDE 2.2"
    date: 2001-08-13
    body: "I don't mind waiting for stability, speed, and fewer memory leaks. \n\nI would hope though that someone would post an update here if they were going to miss the date. The fact that there hasn't been any news gives me hope that we will see the release sometime before \"midnight.\" \n\nMatt"
    author: "Matt"
  - subject: "And more thanks"
    date: 2001-08-14
    body: "I've been using KDE on and off for a while now, and though 2.1.2 and it's apps haven't reached top stability yet, it is by far the best desktop around at the moment.\n\nFor example: I was just using kmail to compose a lengthy email and the sodding thing crashed on me. Typical KDE crash I thought, but no! Relauched kmail and up popped my mail-in-progress just as it was before the tempting offer of a back trace. Now that's bloody good design! I can't wait for the stable 2.2.x series! \n\nThank you very much KDE team!"
    author: "tonyl"
---
<A HREF="http://www.kde.org/people/waldo.html">Waldo Bastian</A>, the KDE
2.2 release coordinator, has tagged KDE 2.2 for final release.  Though
KDE 2.2.0 was scheduled for release today, it has been
slightly delayed to increase stability and speed.  The current
schedule is to release KDE 2.2.0 next Monday, August 13.  Future plans
include a KDE 2.2.1 bugfix/translation release, scheduled for September
2001.  But the main development activity will occur in the KDE 3.0 branch,
which will be based on Qt 3.x and is scheduled for release early next year.  Congratulations to all KDE developers for reaching yet another important milestone.  Waldo's
announcement follows.
<!--break-->
<BR><br>
<HR SIZE=1 NOSHADOW WIDTH="95%"><BR>
<TABLE BORDER=0 CELLSPACING=0 CELLPADDING=0>
<TR>
<TR><TD ALIGN="right"><STRONG>From</STRONG>:&nbsp;&nbsp;</TD><TD><A HREF="mailto:bastian@kde.org">Waldo Bastian &lt;bastian@kde.org&gt;</A></TD></TR>
<TR><TD ALIGN="right"><STRONG>To</STRONG>:&nbsp;&nbsp;</TD><TD><A HREF="mailto:kde-devel@kde.org">kde-devel@kde.org</A>, <A HREF="mailto:kde-core-devel@kde.org">kde-core-devel@kde.org</A>, <A HREF="mailto:kde-i18n-doc@kde.org">kde-i18n-doc@kde.org</A></TD></TR>
<TR><TD ALIGN="right"><STRONG>Subject</STRONG>:&nbsp;&nbsp;</TD><TD>KDE 2.2 tagged</TD></TR>
<TR><TD ALIGN="right"><STRONG>Date</STRONG>:&nbsp;&nbsp;</TD><TD>Sun, 5 Aug 2001 18:26:11 -0700</TD></TR>
</TABLE>
<P>
Hiya,
</P><P>
This is to inform you that the final tagging for KDE 2.2 has taken place.
Changes that, at this moment, do not have the KDE_2_2_RELEASE tag will not be
part of KDE 2.2.
</P><P>
<H4>KDE_2_2_BRANCH</H4>
</P><P>
For KDE 2.2.1 a KDE_2_2_BRANCH branch has been opened. To update to this
branch use:
</P><P>
<PRE>
   cvs update -r KDE_2_2_BRANCH
</PRE>
</P><P>
The KDE 2.2 branch remains frozen, that means that all fixes for KDE 2.2.1
should be posted for review first. The message freeze remains in effect for
this branch as well. The KDE 2.2. branch will be released as KDE 2.2.1 in
about a month from now.
</P><P>
<H4>Head</H4>
</P><P>
The HEAD branch will become KDE 3.0 and is open for all your hacking
pleasure. The HEAD branch is the cvs branch that you get by default.
</P><P>
If you want your application to be part of KDE 3.0, _THIS_ is a good time to
move it out of kdenonbeta.
</P><P>
As mentioned before, Dirk Mueller will coordinate the KDE 3.0 release.
</P><P>
<H4>Thanks</H4>
</P><P>
I hereby would like to thank everyone for his or her patience and commitment,
thanks to you KDE 2.2 seems to have become the stable release that we all
wanted it to be.
</P><P>
Thank you very much.
</P>
Cheers,<BR>
Waldo<BR>
-- <BR>
Andrei Sakharov, Exiled 1980-1986, USSR, <A HREF="http://www.aip.org/history/sakharov/">http://www.aip.org/history/sakharov/</A><BR>
Dmitry Sklyarov, Detained 2001-????, USA, <A HREF="http://freesklyarov.org/">http://freesklyarov.org/</A><BR>