---
title: "Linux Accessibility Conference and KDE"
date:    2001-01-10
authors:
  - "jschnapper-casteras"
slug:    linux-accessibility-conference-and-kde
comments:
  - subject: "Re: Linux Accessibility Conference and KDE"
    date: 2001-01-11
    body: "Speaking about accessibility, I just uploaded a preview of kde black & white icons. \nsee: http://home.uwnet.nl/~vita/linux/\nThe preview comes with install and un-install scripts. The black & white icons are useful for monocrome monitors and visually impaired people.                    \nUp till now the project isn't going very well (There are about 150 icons made, well, some 1000+ remain to be done).\nIt might be a good idea to make it a multiple GUI project. I mean, let designers from other projects like GNOME, FreeBSD, etc join in and share the icons. For visually impaired people it would be good to see the same icons in all GUI. Better even, this way there is a change the set will ever be complete."
    author: "Ante Wessels"
---
<i>[Ed: This article was submitted about a month ago but got lost in the queue.]</i><br>
Project <a href="http://ocularis.sourceforge.net/">Ocularis</a> is pleased to announce the Linux Accessibility Conference -- join us for our workgroup on KDE, and more, on 
March 22-23, 2001, Los Angeles Airport, Hilton Hotel, Plaza D. Please read on for more details.
<!--break-->
<h2>The Linux Accessibility Conference</h2>
<p>
<blockquote>
     March 22-23, 2001<br>
     Los Angeles Airport<br>
     Hilton Hotel, Plaza D<br>
</blockquote>
<p>
There will be a workgroup on KDE.
<ul>
<li>Taking place at CSUN's Sixteenth Annual International Conference, which averages 4000 attendees.
<li>Free admission to the Linux Accessibility Conference (not including the price of attending to CSUN, consult <a href="http://www.csun.edu/cod/conf2001/index.html">http://www.csun.edu/cod/conf2001/index.html</a> for admission costs). 
</ul>
<p>
Join us for two days of:
<ul>
<li>Speeches by prominent figures in the free software and accessibility community.  Individuals who have tentatively agreed to speak include Judy Brewer (Director of WAI), Alan Black (Creator of Festival Speech Synthesizer), Peter Korn (Sun Microsystems  GNOME Accessibility Lab), and Aaron Leventhal (Mozilla accessibility).  Other noteworthy people who have said they plan to attend include T.V. Raman (Creator of Emacspeak).
<li>Demonstrations of free software such as Emacspeak, Festival, BRLTTY, and Speechd.
<li>Workgroups on GNOME (run by Sun Microsystems' GNOME Accessibility Lab), KDE, X Windows, Console, Braille, Speech, Internationalization and Localization (i18n and l10n), Internet Applications (Mozilla), and a Universal Accessibility Standard.
<li>Planning and organizing for the future of Linux accessibility.
</ul>
<p>
If you are interested in attending, join the ocularis-announce mailing list by visiting <a href="http://lists.sourceforge.net/mailman/listinfo/ocularis-announce">http://lists.sourceforge.net/mailman/listinfo/ocularis-announce</a>.
<p>
The Linux Accessibility Conference stems from Project Ocularis, a volunteer-run effort to make Linux and the free software world accessible to all.  Visit Project Ocularis at <a href="http://ocularis.sourceforge.net">http://ocularis.sourceforge.net</a>.
<p>
<h3>Mission:</h3>
The mission of the conference is twofold: 
<ol>
	<li>To demonstrate the potential of Linux and free software in the accessibility arena.
	<li>To formulate a course of action for advancing Linux accessibility and to begin to organize interested supporters and developers into working groups focusing on specific topics.
	These topics include: GNOME, KDE, X Windows, Console, Braille, Speech, Internationalization and Localization (i18n and l10n), Internet Applications (Mozilla), and Universal Accessibility Standard.
</ol>
<h3>Who Should Attend:</h3>
<ul>
<li>Companies or developers who want to make their applications more accessible under Linux.
<li>Companies or developers in the AT industry who are interested in better serving impaired users through creating and using free software.
<li>Anyone who is interested in making Linux and the free software world more accessible to all.
</ul>
<h3>Tentative Schedule:</h3>
Thursday, March 23rd:
<ul>
<li>Speakers
<li>Presentations
<li>Demos
<li>Transition (technical overview and introduction to the distinct role of each working group)
<li>Break up into working groups
</ul>

Friday, March, 24th:
<ul>
<li>Reconvene the next morning for another speaker
<li>Hear reports from each of the working groups
<li>Long-term planning
</ul>


<h3>Travel and Accommodations:</h3>
	See <a href="http://www.csun.edu/cod/conf2001/index.html">http://www.csun.edu/cod/conf2001/index.html</a> for information about accommodations and travel.

<h3>Remote Attendance:</h3>
	Join the ocularis-announce mailing list by visiting <a href="http://lists.sourceforge.net/mailman/listinfo/ocularis-announce">http://lists.sourceforge.net/mailman/listinfo/ocularis-announce</a> for details about remote attendance.
<p>
<h4>Interested in Being a Speaker or Involving your Business or Free Software Project?</h4>
Contact:<br>
JP Schnapper-Casteras<br>
Conference Organizer<br>
<A href="mailto:jpsc@users.sourceforge.net">jpsc@users.sourceforge.net</a><br>
Additional contact information available upon request.
<p>
Please forward this announcement to a friend or colleague.
<p>
Linux is a registered trademark of Linus Torvalds.  All other trademarks and copyrights are owned by their respective owners.