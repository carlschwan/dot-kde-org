---
title: "Hancom/theKompany.com Merge Product Lines, Announce Qt-3.0 Office Suite"
date:    2001-08-31
authors:
  - "Dre"
slug:    hancomthekompanycom-merge-product-lines-announce-qt-30-office-suite
comments:
  - subject: "What does it mean for KOffice?"
    date: 2001-08-30
    body: "No more Kivio? And what about ReKall?\nPerhaps Shawn Gordon can explain."
    author: "reihal"
  - subject: "Re: What does it mean for KOffice?"
    date: 2001-09-03
    body: "Yes, Kvivio and ReKall still exist, but they are also selling a supported version, with some additions, under their own product name (as they own the code, they can do that)."
    author: "AArthur"
  - subject: "Good news or bad news?"
    date: 2001-08-30
    body: "I don't know whether this is good news or bad news. Here we have, another office suite coming in town.. that means another set of file formats to take care of. Is TheKompany planning to keep all file formats open? Or are they heading in the same dirty direction of M$ where the file formats are not documented atall!\n\nIts based on Qt3 and we know that KDE 3 isn't coming out anytime soon. That means Qt3 won't be installed by default.. When Qt3 is installed on top of Qt2, what happens?\n\nNow what lies in future for KOffice? What happens to Kivio, ReKall, Aethera? Does the GPL development stop suddenly?\n\nAnyway, definitely a good news that another application suite is coming to Linux and esp. KDE! Star office is too bloated since it uses its own UI library and nobody else used the KDE/Qt framework till now.. So this could head towards success!!\n\nFinally, I just hope they select some better names.. Hancom, QuickSilver etc. are not at all trendy..\n\nsarang"
    author: "sarang"
  - subject: "Re: Good news or bad news?"
    date: 2001-08-30
    body: "I'm sure they will be staticly linked binary's. I'm very excited about this. Finally a real MS Office competitor thats not running wine. The best thing of all is that its Qt so it will fit nicely into my desktop.\n-- \nCraig"
    author: "Craig"
  - subject: "Re: Good news or bad news?"
    date: 2001-08-31
    body: "Staroffice/openOffice is not running Wine\nThe Gnome office proograms also dont depend on wine"
    author: "ac"
  - subject: "Re: Good news or bad news?"
    date: 2001-08-31
    body: "I believe the grandparent said \"a real MS Office competitor.\""
    author: "Joe KDE"
  - subject: "Is it a joke ?"
    date: 2001-08-30
    body: "Have a look at the screenshots on Hancom website.\nThe last ones seem to be Quanta+ and Kivio under winXP ?!!\nWebBuilder is Quanta+ and Painter is Kivio.\nHave they ever read the GPL ?"
    author: "azerty man"
  - subject: "Re: Is it a joke ?"
    date: 2001-08-30
    body: "Sounds like you need to read the gpl friend. theKomapny owns the code and can licence it anyway they want."
    author: "Craig"
  - subject: "Re: Is it a joke ?"
    date: 2001-08-30
    body: "Does the Kompany owns Quanta code ?\nwhat is the Xp screenshot, a port of qt ?\nI don't want to blame this product, I think this is really a good news but I would like to be sure that it is not vaporware or something.\nThanks"
    author: "azerty man"
  - subject: "Re: Is it a joke ?"
    date: 2001-08-30
    body: "Yes they own the code and of course its not vapor ware.\n\nCraig"
    author: "Craig"
  - subject: "Re: Is it a joke ?"
    date: 2001-08-31
    body: ">Does the Kompany owns Quanta code ?\n\nQuanta used to be Free but some of the developers decided to go commercial with it and relicensed it.  I guess theKompany was the company that got it.\n\n>what is the Xp screenshot, a port of qt ?\n\nQT has always been available for Windows, and won't require any porting to work under XP.  Any 100% QT-native program (such as these office programs) will work on any of QT's platforms with a simple recompile (theoretically)."
    author: "not me"
  - subject: "Re: Is it a joke ?"
    date: 2001-08-31
    body: "> Any 100% QT-native program (such as these office programs) will work on any of QT's \n> platforms with a simple recompile (theoretically).\n\nAnd practically!\n\nAs a Qt-windows licence holder, I confirm this. It took 1 day to make a 30000 lines unix Qt program compile under windows, and one week to make it link :-), because we were not experienced with windows lib/dll stuff. Now, it would link the same day it compiles."
    author: "Philippe Fremy"
  - subject: "Re: Is it a joke ?"
    date: 2001-08-31
    body: "I'm interested in this, what build setup were you using? \n\nI'm trying to figure out what I can use as a build environment that will work on both windows and unix with QT apps, and I'm getting the impression that (barring building with cygwin/gcc) autoconf doesn't do this very well? I'd love to be wrong :-)"
    author: "Kevin Puetz"
  - subject: "Re: Is it a joke ?"
    date: 2001-08-31
    body: "It's no joke.  It's a Qt based program (Kivio) so ports to other OS's are no longer just a thought.\n\n-dave\n(kivio developer)"
    author: "Dave Marotti"
  - subject: "Re: Is it a joke ?"
    date: 2001-09-02
    body: "I was working the Hancom booth during the show.  We had HancomSheet running on an iPaq -- handheld.  Hancom told me the port took them about four hours.\n\nBelieve it -- a robust multi-platform office is on the way.  As I was telling the press representatives \"HancomOffice 2.0 is the camel's nose in the Microsoft tent.\""
    author: "Guy Smith"
  - subject: "Re: Is it a joke ?"
    date: 2001-08-30
    body: "they are the copyrightholders. therefore they can rerelease it under a other license."
    author: "jasper"
  - subject: "Re: Is it a joke ?"
    date: 2001-08-31
    body: "Yes they are...did you not read above...\nthekompany is adding kivio, rekall, and quanta+ \nto hancom office.\n\nno Painter is not Kivio...\nEnvision is Kivio...which is actaully a pretty cool name for it.\n\nand yes...that does *APPREAR* to be kivio(envision) running under XP.  \n\nPersonally I think releasing cross platform applications is a good idea.  It gets people to use applications on familiar terms, then they realize, hey these apps are native linux apps! and makes us even more appealing.  \n\nthe world is never going to be a one operating system world...the best way to go about it is to make yourself compatible with a lot of them.  Kind of like Opera.  \n\nThis has been thekompany's plan for a while now...to release their apps on both linux and windows.  Data Architect is already released on both from the beginning.\n\njust my $.02\nhttp://arctic-circle.wso.net"
    author: "Brad C"
  - subject: "Competition between friends"
    date: 2001-08-30
    body: "I'm confused.  So far theKompany and KDE have managed to stay out of each others' way for the most part, but this move puts theKompany and the KDE project in direct competition!  What will happen to KOffice?  Will developers lose interest and switch to using HancomOffice?  Will KOffice continue to improve and eventually replace HancomOffice, hurting theKompany and Hancom?  Do theKompany and Hancom hope to perpetually improve their HancomOffice product so that it is always ahead of the features of KOffice?\n\nSome informed comment would be great here.  David Faure, Shawn Gordon?  Comments?"
    author: "not me"
  - subject: "Re: Competition between friends"
    date: 2001-08-31
    body: "I think your missing it. Koffice is strictly KDE while HancomOffice is a multiplatform solution allowing corporations to easily plug KDE and Linux into there networks. If anything it will help KDE and bring it into the mainstream. I don't think it will be a Koffice competitor. This is truly a good thing.\n \nCraig Black"
    author: "Craig"
  - subject: "Re: Competition between friends"
    date: 2001-08-31
    body: ">I don't think it will be a Koffice competitor. \n\nAnd why not?  They each provide:  a word processor, a spreadsheet, a presentation program, and a bitmap picture editor.  If HancomOffice is better than KOffice, KOffice use will be very low.  If KOffice gets to be better than HancomOffice, everyone will start using KOffice because its free.  That's competition.\n\nAlso, distros such as Mandrake (employer of David Faure, main KOffice coordinator and programmer) may decide to license HancomOffice and stop sponsoring KOffice development.  Programmers who would otherwise have worked on KOffice might decide to simply pay $99 and use HancomOffice.  KOffice could be hurt by this.\n\nNow, I'm not saying that HancomOffice is bad, I'm just confused by theKompany's move here.  So far they have taken steps to keep their programs away from KDE programs, but now they're competing with a KDE project."
    author: "not me"
  - subject: "Re: Competition between friends"
    date: 2001-08-31
    body: "Huh? Mandrake won't ship anything thats not gpl. So that just would'nt happen.\n\nCraig"
    author: "Craig"
  - subject: "Re: Competition between friends"
    date: 2001-08-31
    body: "Well, actually, they ship many applications that are not gpl:\nXfree\nNetscape\nEnlightenment\n\netc."
    author: "oliv"
  - subject: "Re: Competition between friends"
    date: 2001-08-31
    body: "Since when is XFree86 GPL'ed?"
    author: "Stof"
  - subject: "Re: Competition between friends"
    date: 2001-08-31
    body: "you wouldn't be able to download hansonoffice for free, certainly, as part of Mandrake8.1 ISOs.\n\nBut it could be included in the power pack cds, which contain some commercial software.\n\n--john"
    author: "john"
  - subject: "Re: Competition between friends"
    date: 2001-09-02
    body: "Who cares?  I live by the rule that the best idea wins.  If Hancom delivers the best product at a price the market accepts, then they win.  The alternative is for the KOffice folks to ramp-up and meet the challenge."
    author: "Guy Smith"
  - subject: "500MHz recommended"
    date: 2001-08-30
    body: "SYSTEM REQUIREMENTS:\nPentium or better processor (500MHz recommended)\n\nCome on, 500MHz for a stinking Office Suite. The last MS Office I ran (Office97) ran fine on a 486. I would say these guys have some serious optimization problems to sort out."
    author: "Konqi"
  - subject: "Re: 500MHz recommended"
    date: 2001-08-31
    body: "And the minimal requirement is 64Mo, but 64Mo are recommended !!!\n\n> SYSTEM REQUIREMENTS\n> Kernel Release 2.2 or higher/X-Windows \n> Pentium or better processor (500MHz recommended) \n> 64 MB of RAM (64 MB RAM recommended) \n> 400 MB of hard disk space \n> CD-ROM drive"
    author: "Benoit WALTER"
  - subject: "Re: 500MHz recommended"
    date: 2001-09-02
    body: "I ran the preview of HancomOffice on an old Dell 333 (and on a DOS file system install of RH 7.1) and didn't have gave performance problems.  I think the 500mhz recomendation is a safety net."
    author: "Guy Smith"
  - subject: "Re: 500MHz recommended"
    date: 2001-09-12
    body: "Hi, this is a \"service person\" from HancomLinux.\nFirst, I would like to say that HancomLinux was too exaggerated about the system requirements.\nI found out that 200MHz was way enough to run it.\nNow, we have fixed that point on our web site, so you can check it out on \n\nhttp://www.hancom.com/en/company/shopping.html\n\nThanks for correcting a good one.  We will always look and care for your concerns and suggestions.\n\nBy the way, how are you, Guy??  Itz Amy. :)"
    author: "service@hancom.com"
  - subject: "Who says StarOffice is bloated ? ;-)"
    date: 2001-08-31
    body: "Hm, evaluation version of HancomOffice (without TheKompany's products) has 95 Mb!\nWho says StarOffice is bloated ;-)\nI'll stick with KOffice, just hoping that KOffice developers will stick with it too :)"
    author: "antialias"
  - subject: "Re: Who says StarOffice is bloated ? ;-)"
    date: 2001-08-31
    body: "I just installed the 60 days demo of Hancom, and tried to play with it. The word processor is still a wine port and even points you to a c:=\\d rive to save your work.\n\nHancom office feels strange, alien even, compared to StarOffice or KOffice. I feel ther eviewer from LinuxPlanet was quite right when hed ismissed the suite for now.( http://www.linuxplanet.com/linuxplanet/opinions/3274/4/), but quite probably a new version, based on Qt 3.0 would be worth the money."
    author: "Boudewijn Rempt"
  - subject: "filters?"
    date: 2001-08-31
    body: "\"The Word/Sheet/Presenter applications are\n  advertised as outstanding at both importing \n  and exporting the corresponding MS Office\n  formats.\"\n\nAny chance that the Kompany will share their I/O filters with the KOffice crew?\n\nThat would be nice..."
    author: "LMCBoy"
  - subject: "Re: filters?"
    date: 2001-08-31
    body: "The koffice filters are coming along nice. The msoffice filter in kword works really nice over here, an rtf import filter by edwald snel featuring tables/styles will be comitted to cvs soon, and a wordperfect input filter was comitted yesterday or so."
    author: "ik"
  - subject: "Re: filters?"
    date: 2001-08-31
    body: "Good to hear.  I actually haven't tried importing MS formats myself, I was just going on the KOffice announcement, which stated that filters were now the big hurdle... \n\nI agree with the poster who is confused and troubled as to why the Kompany would release a product in direct competition with a KDE product (and perhaps their flagship product at that!)\n\nsigh..."
    author: "LMCBoy"
  - subject: "An Office Suite for Every Day"
    date: 2001-08-31
    body: "Monday: KOffice\nTuesday: OpenOffice\nWednesday: StarOffice\nThursday: Abiword\nFriday: WordPerfect\nSaturday: ApplixWare\nSunday: Lyx\n\nIf I knew a little more English, I could even make a poem about it ...\n\n--Bram"
    author: "bambi"
  - subject: "Nicely integrated with KDE ?"
    date: 2001-08-31
    body: "I'm afraid that it will not be integrated with KDE. The KParts are KDE-only solution, so probably those programs will live its own life - something like a next ( lightweight probably ) StarOffice. I've heard something about COM capabilities of Qt - but I was able to find only something about generating uuid's and  a single pure virtual class. If we have got a simple interface like QOleDocument ( which,  implemented, should result in a cross-platform embeddable component  ) we would be able to write a real cross-platform applications - but now they have to follow a StarOffice path. I think that a very few people will use it on Linux. What a pity !"
    author: "Krame"
  - subject: "Re: Nicely integrated with KDE ?"
    date: 2001-08-31
    body: "Thats the \"universal component\" thing the Trolls work on.\nThere is a presentation by Mathias Ettrich somewhere on the web.\nThis is real cool stuff. Took the ideas behind COM some steps further.\n\nKevin"
    author: "Kevin Krammer"
  - subject: "Odd mix of products"
    date: 2001-08-31
    body: "This seems a little odd to me. I'm not very familiar with Hancom's products, but they look pretty mature. ReKall and Aethera are not yet ready for prime time. Last time I tried Aethera (maybe a month ago) I'm not sure it did much of anything (I remember it crashing, if that counts). I had high hopes for Rekall because I had done some work with KDB and found it to be pretty feature-complete, but when I installed it, I discovered that it had a ways to go, especially WRT the UI. I know it's early in the game, but it seems a little premature to be slapping these partially finished products in with the mature ones (Quanta is very nice) and call it an office suite.\n\nJust my $.05..."
    author: "larryw"
  - subject: "What happens when a GPL app is relicensed?"
    date: 2001-08-31
    body: "What happens if, say, I'm using a GPL app called kdegplapp version 2.1 and the original author and copyright holder decides to relicense to a proprietary license?  Can I branch the code at 2.1 and continue to develop the codebase?  Can the author retroactively decide that I can't use the application?  I'm confused as to what the implications are...\n\nWill"
    author: "William Wise"
  - subject: "Re: What happens when a GPL app is relicensed?"
    date: 2001-08-31
    body: "yes you can take the last GPL version and fork it and do what you want with it"
    author: "me"
  - subject: "Re: What happens when a GPL app is relicensed?"
    date: 2001-09-03
    body: "... except relicense it, naturally... :)"
    author: "jd"
  - subject: "Re: What happens when a GPL app is relicensed?"
    date: 2001-08-31
    body: "No, you can fork the project at any point you like. License changes cannot be retroactively applied: if I release version 0.0.1 under the GPL, that version is permanently under that license (of course, I can release 0.0.1a which is exactly the same, only under a different license -- but that has no effect on the licensing or distribution terms of version 0.0.1). This has happened before -- for example, OpenSSH is based off a (very) old version of SSH, before the license changed to become commercial. A copy of the old version was still available so the OpenBSD people used that -- and SSH Inc. couldn't stop them."
    author: "Neil Conway"
  - subject: "This can only be good news..."
    date: 2001-08-31
    body: "I see no harm that can come from this...\nI'm an Open Racer developer ( a very small one on a very great and much more talented team ), and our goal is to keep the Tux Racer (turning Comercial) code base alive...  We do not damn Tux Racer, we wish it success!\n\nIf we have life our way, we will replace Tux Racer on all Distros (especialy the GPL'd release), while Tux Racer will grow and offer a serious, and cool, comercial offering for those who are willing to pay for it...\nI think Open Source and Comercial can live together...  Until any comercial product overly dominates and gets Greedy...  Then, and only then, is it important that all comercial and Open operations come together and bring an open standard...\n\nIf you are fighting Microsoft, you would see that Open Source and Comercial offerings are a good thing... (However, this might be a distorted view)\n\nIf you are fighting all comercial operations, and really believe this product will enslave the world...  You are even wronger, because Open Source alternatives will not slow..."
    author: "Gregory W. Brubaker"
  - subject: "Re: This can only be good news..."
    date: 2001-08-31
    body: "I will buy a commercial office suite if its good. Bring it on..."
    author: "Rob"
  - subject: "Re: This can only be good news..."
    date: 2001-08-31
    body: "Exactly... Its a question of Time, Price, and quality...\n\nComercial products like this may be better for, say, the city of Largo, or others large groups looking to use linux... than the individual; however, it can only be a good thing...  And hopefully any comercial effort that fails will release Open Source..."
    author: "Gregory W. Brubaker"
  - subject: "why this commercial advertisement on the dot?"
    date: 2001-08-31
    body: "Why was this article not marked with '[Ed: commercial software]' or something, like similiar previous articles? And what's the direct relation to KDE for a pure Qt based office suite that ships software that is no more maintained inside KDE (kivio...) ? I for one don't think this article belongs to the dot. It's a commercial advertisement of a non-KDE software."
    author: "anonymous"
  - subject: "Re: why this commercial advertisement on the dot?"
    date: 2001-08-31
    body: "Because it benefits KDE, and uses the Qt libraries, which are inextricably linked with KDE on the news-wires. Frankly, I think this is a _good_ thing. It'll help two outstanding Linux companies stay above water. It also provides a nice easy migration path to Linux. It's been said before, once you get people used to the applications, what platform they run on is immaterial.\n\nTo be honest, I wish theKompany all the best in this effort. The suite of apps they've lined up looks outstanding for $99 - the Painter app looks like it has all the features of many art apps that are sold for $99 on their own (e.g. Paint Shop Pro). I'd say they should chase the OEM market, particularly the smaller ones, with a vengence. They could really do well out of this."
    author: "Bryan Feeney"
  - subject: "What a challenge for the KOffice team !"
    date: 2001-08-31
    body: "One month ago, I said that I didn't feel that the strategy of the Kompany was clear. Now, it is...\n\nThere is a good size and a bad size. I prefer look at the good size : a new challenge for the KOffice team is to build the best KDE Office. Even, it has to be the first challenge now...\n\nAnd it would not be easy. The Hancom Office page is pretty and some things are very in advance, comparing to KOffice.\n\nThere is a big work to do for enhancing the existing good young apps, for reactiving some sleeping apps (Krayon, Kivio, Quanta Plus...) and for creating some new apps (Katabase...). What an exciting challenge ! I hope that new developpers will be interested and involved !\n\nAnd I hope that it will be a fair challenge, that easy and efficient filters will be built between the two offices...\n\nMy best encouragments for the KOffice team !"
    author: "Alain"
  - subject: "Word?"
    date: 2001-08-31
    body: "Word?? Their word processor is called Word?? Obviously these guys weren't paying attention when KIllustrator got sued for violating Adobe's trademark.."
    author: "Anonymous Troll"
  - subject: "Re: Word?"
    date: 2001-08-31
    body: "The art app could run into trouble too, there's a Corel Painter already out for Windows. And, of course, lets not forget KWord and KPresenter - clearly Hancom's just violated their trademarks ;-)"
    author: "Bryan Feeney"
  - subject: "Re: Word?"
    date: 2001-08-31
    body: "Hancom has had these products out for a while already. I think at least for a couple years.\n\nCraig"
    author: "Craig"
  - subject: "Re: Word?"
    date: 2001-09-02
    body: "One cannot trademark the use of the word \"Word\" as it's been in existance since the birth of the English language; and throughout history has been applied to more uses than you can shake a stick at. \n\nYou're getting over paranoid."
    author: "Macka"
  - subject: "Redhat compatible?"
    date: 2001-08-31
    body: "PLATFORMS\nHancomOffice is installable and executable stably based on the Red Hat and Red Hat compatible Linux distributions such as RedHat 7.0/7.1, Mandrake Linux 7.0/8.0, SuSE Linux 7.0. \nLibraries: same as libraries in the Red Hat and the compatible libraries \nRPM: the latest 4.0.2 rpm version or previous rpm\n\n/me hmmmz ... Redhat 7.0/7.1 and Mandrake 8.0 are using gcc-2.96 and this ABI is NOT compatible with gcc-2.95 or gcc-3.0. Problems ..."
    author: "Hasso Tepper"
  - subject: "ftp.kde.org needs authorization !!!???"
    date: 2001-08-31
    body: "What happened to ftp.kde.org, I just can't get there it always asks for authorization and also I can't login as an anonymous user"
    author: "Asif Ali Rizwaan"
  - subject: "Re: ftp.kde.org needs authorization !!!???"
    date: 2001-08-31
    body: "Probably overloaded. Use mirrors."
    author: "Hasso Tepper"
  - subject: "Shawn Gordon answers all the questions :)"
    date: 2001-08-31
    body: "Let me take an opportunity to try and answer all the questions at once, so this might be a rather long email.\n\n1.  it is \"Rekall\" and not \"ReKall\" :)\n2.  All theKompany products will continue to exist in the stand alone form.\n3.  We will continue to maintain Kivio in KOffice.  Waldo Bastian and I had a nice chat around the end of LinuxWorld on the topic and I think he understands what we are doing.\n4.  Since Kivio is the only application currently in KOffice that is affected by this, it doesn't have any major impact.\n5.  We will statically link Qt3 to the application.  We are doing this with DataArchitect currently, and with Kivio MP in our labs.  Works great, barely adds any size and doesn't mess with your system.\n6.  I had about 3 days to come up with new names for theKompany products for the show, some I like, some I don't - consider them code names for now.\n7.  Hancom can call their application HancomWord because it is MicrosoftWord.  This is a common way to avoid trademark problems in the US to put your company name in front of the application.\n8.  We had never decided to put Rekall into KOffice, and now that it will be Qt only it will never be in KOffice.  If you think about it, there isn't really an advantage to embedding a database app inside a KWord document.\n9.  The current HancomOffice applications have been around for 10 years, they are very mature.  Hancom has a very large number of programmers as well.  HancomWord has run on Linux under Wine until now, the Qt port is almost complete and will be part of HancomOffice 2.0\n10. theKompany holds the license on all of the applications we are using.  In the case of Kivio and Quanta, these have gone through a total rewrite in any case to turn them into Qt specific apps.\n11. Apparently Quanta development has been stalled for some time.  I wasn't aware of this fact when the programmers asked us to take it on.  I had been using them as contractors for work and they are great programmers, but I guess they had to make some money so couldn't work on Quanta.  In any case we have hired them as employees now.  I'm not sure if there will be a free version of Quanta, we've talked about it but just haven't had any time to figure it out yet.\n12. Yes that is Kivio running on WinXP.  Using Qt it is a very easy matter to make the application run on different platforms.\n13. I really am impressed with the work that has gone into KOffice over the years, but you have to be realistic about it.  There is only a couple of people working on it part time.  KOffice is so far behind HancomOffice it will never catch up.  That isn't to say that KOffice isn't what some people will want or need.  KDE is interested in the desktop more than the office space, they want to see applications that people will use on the desktop.  Our focus on Linux is always going to be advocates for KDE and making sure our Qt apps behave and interact properly under KDE.  All of our materials at the LinuxWorld show talked about KDE on Linux.\n14. I'm running HancomOffice on a 200Mhz machine at home.  I'm not sure why the requirements are listed so large.\n15. HancomOffice has an enormous amount of clipart, like 900 pieces, which contributes to its size.  As stand alone applications they aren't that large.\n16. The MS filters are Hancom intellectual property and are very unlikely to be given away.  They do an *excellent* job of import *and* export of MS file formats.  I was very impressed.\n17. Kparts can't be used because of the cross platform nature, so we will sadly be building a lightweight in-process OLE type model on Qt3 to support embedding (sorry if I got some of that wrong, I don't code much anymore).\n18. There is work to do on some of theKompany applications to get ready and now we have the needed resources to accelerate their work.  We should hit our November release target.\n19. Since we will be statically linked to Qt3 there aren't going to be any distribution specific problems.\n\nLet me say in closing that the interest at LinuxWorld was nothing short of astounding.  We had virtually every major player want to partner with us, people wanting to put it in their companies.  I had to do 5 interviews while I was there after our press release.  The booth was busy the entire time, really busy, which couldn't be said for a lot of the booths.  We had people up to the final minutes.  There is a tremendous amount of excitment over this.  As you all know, I stay involved in these talkbacks and in emails.  I'll be glad to answer questions if you have them.\n\nShawn"
    author: "Shawn Gordon"
  - subject: "Re: Shawn Gordon answers all the questions :)"
    date: 2001-08-31
    body: "Not everything can be free \"as in beer\" forever. If there is no money to be made in Linux, no one will be interested in developing software for it, outside of open-source enthusiasts...and let's face it, realistically how much work can one get done while working part-time on a project? \n\nSimply put, there is not enough open-source programmers to fulfill ALL the needs of Linux, so there will be niches here and there where commercial companies can fill in.\n\nThe only issue in this matter that theKompany might hit is the status of Kivio (seems like the free Koffice version will still be maintained) and the demise of free Quanta might cause some bad publicity as well...but if the Hancom suite is really so good, well then let it be....plus someone can always take the last GPL-ed version of Quanta and fork it.\n\nThe bottom line is that it runs on both Mac, Linux and Windows. If it achieves some reasonable penetration on Windows, then moving people to Linux later on becomes much less of an issue, since they can keep their fav office suite.\n\nSo, I guess in the long run, things might even out..\nRegardless of how things turn out, kudos to Shawn for pulling a real rabbit out of a hat.\n\nI wanted to buy MS Office for my wife but at $450 Cdn I said \"forget it\" and got here the beta build of OpenOffice for her spreadsheet and she's quite happy with it. MS software is just so overpriced, that it has a real soft underbelly to it...hit it with a lower price and equal features and compatibility and this could have some success on Windows...with an obvious spillover for Linux as well..."
    author: "Jacek"
  - subject: "Re: Shawn Gordon answers all the questions :)"
    date: 2001-08-31
    body: "i wish you all the best with your newfound home at Hancom, Shawn (and all the other theKompany devs, too)\n\ni think that there currently is room for a quality commercial office suite in the linux market place. many require such a product for work, and are therefore willing to pay for it.\n\nthis marks the end of the \"pat on the back\" part of my message =) i do take issue with two statements you make in your 19 point posting, Shawn:\n\n10. \"In the case of Kivio and Quanta, these have gone through a total rewrite in any case to turn them into Qt specific apps.\" \n\nFor Kivio I really don't think it matters as all the work was obviously sponsered by theKompany, but I would be highly surprised if it was true that Quanta was completely rewritten from scratch. A derivitive work is a derivitive work, even if the result is a very different code base. This doesn't matter if everyone who contributed to Quanta was cool with the switch in license (or their code was removed and replaced, e.g. that part regressed to a CVS patch where their code didn't exist and new code written on that). The claim that it was totally rewritten seems a little .. dubious. even if it is fine because all the developers agreed to the licensing change, there is no reason to misrepresent, right?\n\n\n13. \"I really am impressed with the work that has gone into KOffice over the years, but you have to be realistic about it. There is only a couple of people working on it part time. KOffice is so far behind HancomOffice it will never catch up.\"\n\nthis may have been true before, but i've noticed a marked change in pace. the koffice email list used to be very quiet. progress was slow, but steady. these days there is a lot of development discussion and many more developers working on the various applications. the two that are getting the most attention are kword and kpresenter, and they show it. the graphics apps and kspread have recently gained attention as well.\n\nbe careful when you say an open source project \"will never catch up\". often times that is all they need for inspiration.\n\nhonestly, i think point #13 is wishful thinking. Hancom office will have a window in which it will be better, but that window will not exist forever since the end goal is a well known set of features, not a quickly moving target. Unless, of course, Hancom starts inventing completely new office suite technologies."
    author: "Aaron J. Seigo"
  - subject: "Re: Shawn Gordon answers all the questions :)"
    date: 2001-08-31
    body: "WRT to Quanta.  It is being rewritten, and the two developers are on our staff and are the full license holders.\n\nI'm on the koffice devel lists too, and there is work going on, this is true, but when you are doing something in your spare time, it is usually done in fits and starts for as long as it interests you.  I don't want to bad mouth KOffice, because I have nothing bad to say, but I have watched this over the last couple of years and the cycles are pretty easy to predict now."
    author: "Shawn Gordon"
  - subject: "Re: Shawn Gordon answers all the questions :)"
    date: 2001-08-31
    body: "If you think that KOffice will never catch up, then why so much work is going behind KOffice? If KOffice can't catch up Hancom office, then how can it catch up MSOffice ever? Why anyone will ever switch to KOffice? If KOffice can't get ground then what is the point on developing it further or goal of KOfice is to just provide a alternate inferior office suites? It's better to be involved in a propritary project like hancomoffice than doing hardwork in vain.\nI don't think it is because any one wants a free office suite. There are already free office suites more mature than KOffice."
    author: "vk"
  - subject: "Re: Shawn Gordon answers all the questions :)"
    date: 2001-09-02
    body: "I didn't really express myself well on this point.  I've been swamped since the show and I've done my best to keep up on these talkbacks, but I haven't always had the time to articulate things properly.\n\nKOffice is an impressive piece of work for what it is.  The sad truth is that there are just hardly any programmers working on it at the moment, and I don't know if that is likely to change any time soon.  When you are working on a project in your spare time because you find it fun or interesting, you will probably stop working on it when it ceases to be fun or interesting so work proceeds in fits and starts.  Given this kind of development model it would be very hard for KOffice to reach the same level of functionality as any commercial suite.  The upside however is that it isn't likely to end up with a lot of bloat and meaningless features to try and justify upgrades.\n\nI say these things with all respect and support for KDE and KOffice, and it is very likely that the feature set in KOffice currently will satisfy a good number of users, but not likely to satisfy power users at this stage (you wouldn't believe the things I've seen people do with a spreadsheet).\n\nHopefully that is a better explanation"
    author: "Shawn Gordon"
  - subject: "Re: Shawn Gordon answers all the questions :)"
    date: 2001-08-31
    body: "the past is only an indicator of the future when conditions are similar. in its early days koffice had the extreme disadvantage of working against a fluctuating, unstable set of libraries whose user base was small. side effects of this included fewer developers available, more work required just to keep up with the libraries, core KDE developers were concentrating on the core (surprise) ... and i'm sure there are others. look what happens when even just one of the core developers jumps on an application in the case of David Faure and KWord.\n\ndon't bank on KOffice not making it out of the shoots as a serious suite; instead i would plan on competing with them on the KDE desktop, much as Hancom will with Open/Star Office. though that one is even trickier IMO: more mature, corporate backing, multiplatform, existing user base. \n\nhrm, here's a question for you Shawn: assuming Open/Star Office manages to trim their binary size down to not be a huge whale of a suite, what would you site as the advantages of Hancom vs Star Office?"
    author: "Aaron J. Seigo"
  - subject: "Re: Shawn Gordon answers all the questions :)"
    date: 2001-09-01
    body: "Easy to predict, hum? Well, people like you predicted that since Linux was developed in people's spare time it wouldn't go far and now look at it... Same goes for the KDE project.\n\nIt's funny how after you \"contributed\" to KOffice you now say \"let's be realistic\". What makes you think that Hancom can beat it? That it can beat Microsoft?\n\nIf you think it's the simple fact it's not free software than you should forget about it. Much larger and more complex software (go write a kernel, a C library and a C compiler) than an office suite has been developed and so will KOffice.\n\nIn the meantime the community can use OpenOffice."
    author: "Carg"
  - subject: "Re: Shawn Gordon answers all the questions :)"
    date: 2001-09-01
    body: "I agree, but I'll use Koffice instead, I tried it an I love it.  I don't think enough people used it until now to allow for the very fast bug fixing/development that goes on in KDE.\n\nI can't believe it was only 9Mb...\n\nMany thanks,\n\nMatt"
    author: "Matt"
  - subject: "simple question"
    date: 2001-08-31
    body: "Will you permit improvements into Kivio in KDE CVS that makes the GPL version somehow better than the commercial version by adding some new stencils or functions?"
    author: "ac"
  - subject: "Re: simple question"
    date: 2001-08-31
    body: "We can't really control that can we.  Other people have already added some stencils, well one anyway."
    author: "Shawn Gordon"
  - subject: "Re: Shawn Gordon answers all the questions :)"
    date: 2001-08-31
    body: ">I really am impressed with the work that has gone into KOffice over the years, but you have to be realistic about it. There is only a couple of people working on it part time. KOffice is so far behind HancomOffice it will never catch up.<\n\nSome contradictions here Shawn. How can you be \"impressed with the work gone into KOffice over the years\" and then, at the same time say \"it will never catch up\". You know that KOffice have make tremendous progress in last 8 moths (se here: 8 months) since David Faure came to KOffice project and helped other KOffice developers. Kword, for example, was totally unusable when it was released with KDE 2.0.\nAt the other hand, HancomOffice has been around for about 10 years (see here: 10 years) and how come that it has never been accepted as an reliable office-suite. Something is rotten here.\nIf it is true that HancomWord runs through Wine I can just imagine why it has never been widely accepted by Linux users. I doubt it also that it will ever be accepted on windows platform because there are some free office-suites which are totally free there: StarOffice and 602-Office suite. BTW, 602-Office suite is very small and very good.\nIt sounds a little bit arrogant when you say that KOffice will never catch up. We'll see.\n\n>KDE is interested in the desktop more than the office space, they want to see applications that people will use on the desktop.<\n\nKDE is desktop environment which is IMHO more than pure desktop. Otherwise KDE would ship without Konqueror, Kmail, Knode & other network apps, Games, Multimedia (Music Graphics & Video), KOffice, Utilities etc. etc. What are they if not applications?\nSo, I don't see that KDE is only \"interested in desktop\"."
    author: "antialias"
  - subject: "Re: Shawn Gordon answers all the questions :)"
    date: 2001-08-31
    body: "What you don't know or realize is that HancomWord is enormously popular in Asia and has been for a long time, but it was always a Windows application.  Hancom made the decision a year or two ago to go to Linux and the word processor was the last piece to move away from Windows.  Hancom has never made any serious attempt to penetrate outside of Asia until now, the Korean, Japanese and Chinese markets are very large.  They only made an office suite of it 1 year ago, and it has been selling very well with large contracts signed in all of the above mentioned countries.\n\nI can be impressed with KOffice and respect the work that went in to it and still be honost enough to realize it doesn't have the resources to really go hard core forward.  It will continue to slowly evolve and will certainly be adequate for many uses.\n\nKDE is primarily interested in having the desktop complete and then getting applications on there.  The goal is to have people using KDE.  No one gets KDE so they can use Kmail for example."
    author: "Shawn Gordon"
  - subject: "Re: Shawn Gordon answers all the questions :)"
    date: 2001-09-01
    body: "> No one gets KDE so they can use Kmail for example.\n\nyou are quite wrong on that one. i know many people who do not run the KDE desktop but run the KDE applications. as a great example, a good friend and developer buddy of mine runs blackbox but keeps up with KDE CVS specifically for 3 applications: konsole, kmail and konqueror. he doesn't care for nor use the rest of the software in kdebase."
    author: "Aaron J. Seigo"
  - subject: "Re: Shawn Gordon answers all the questions :)"
    date: 2001-09-01
    body: "I can confirm this. A LOT of users here in Estonia are using KMail, Konqueror and sometimes Koffice apps and Konsole. Mostly they are using Windowmaker or Blackbox as windowmanager."
    author: "Hasso Tepper"
  - subject: "Re: Shawn Gordon answers all the questions :)"
    date: 2001-09-01
    body: "you guys are missing the point that I made, but that's fine."
    author: "Shawn Gordon"
  - subject: "Re: Shawn Gordon answers all the questions :)"
    date: 2001-09-01
    body: "I thihk Shawn's point was misunderstood...\n\nThe point is simple - no one installs KDE just to run for example Konsole, or kate, or kvim.\n\nI'm not making it up - go see people complain why when they want to run Konqueror - it loads the memory with all the services - it is because Konqueror needs every one of them - so it loads them - and people complain about it..\n\nLets be realistic on a specific part - if you use Konqueror, konsole and kmail, then there is no point of running Window Maker or gnome as your primary Window management - does it?\n\nHetz"
    author: "Hetz Ben Hamo"
  - subject: "Re: Shawn Gordon answers all the questions :)"
    date: 2001-09-02
    body: "Yes, you are wrong. Couple of my friends do just that. They download KDE so they can use konqueror and kmail. Actually, for one of them Kmail was THE reason to leave Windows.\n\nThey prefer iceWM as window manager and they are not ready to give it up just because they like some KDE programs. They also don't feel the need for other things like kicker.\n\nI don't know what Shawn's point was, but I know what mine is. You have all kinds of people who use software for all kinds of things in all kinds of ways. We might guess what majority of them likes, but we certainly can't say what no one does."
    author: "Marko Samastur"
  - subject: "Re: Shawn Gordon answers all the questions :)"
    date: 2001-09-02
    body: "You are right about it - but I was talking about the majority ones..\n\nBack at my previous job 2 years ago - I wish I had a program like Konsole under windows..."
    author: "Hetz Ben Hamo"
  - subject: "i can't live without konsole and konqueror"
    date: 2001-09-02
    body: "see name & subject"
    author: "gnome user"
  - subject: "Re: Shawn Gordon answers all the questions :)"
    date: 2001-09-02
    body: "I actually DID get KDE just for Kmail - the vanilla MUAs for Gnome were a pain to set up for POP access. The fact that other nifty stuff also comes with KDE was a happy coincidence."
    author: "Chris"
  - subject: "Re: Shawn Gordon answers all the questions :)"
    date: 2001-08-31
    body: "Ahhh, thank you Shawn.  Very informative as always.\n\n> Let me say in closing that the interest at LinuxWorld was nothing short of astounding.\n> We had virtually every major player want to partner with us, people wanting to put it\n> in their companies. [...] There is a tremendous amount of excitment over this.\n\nCongrats!  Sounds like you made a very wise business decision!  Good luck; I can tell that theKompany is really going places :-)"
    author: "not me"
  - subject: "Re: Shawn Gordon answers all the questions :)"
    date: 2001-08-31
    body: "While I can see all the applications are generally owned by you all, the Email client comes into question.  This was a spin off of Magellan code, and while I have to admit there is very little of it that looks or acts the same now, it was still based on this code.  Therefore, while you could GPL the code, the original copyright owner is still the originator of Magellan code, so can you now relicense it under a closed source license? \n\nYes I do understand that the original Magellan code was BSD, however once you moved from BSD to GPL you lost the right to simply say we can just relicense to our hearts content.  For all the other stuff I wish you all well, but this one thing has me wondering.  It is a dangerous road you are on with the email client, as there was already some infighting between your team in the original author.  \n\nBeyond that, I hope you all define the status of Quanta fairly soon, I am actually considering doing some work with it going forward but want to know if it makes sense to work with the current stuff or to just spin it off to play with until I know what will happen. I feel strongly that KDE needs a free HTML editor as part of its bag of tricks.  \n\nGood luck, and the screen shots look good, I will definitely be watching for this to go live (oh and I am still waiting on some features for your financial software before I purchase)."
    author: "Anthony Moulen"
  - subject: "Re: Shawn Gordon answers all the questions :)"
    date: 2001-08-31
    body: "As the GPL license holder we can dual license or change the license as much as we want, but we can't take back what is out there.  There is very little of the original Magellan code left in Aethera at this stage, it probably would have been easier to start from scratch.  In any case we haven't decided to change the license of Aethera/Quicksilver at this point anyway."
    author: "Shawn Gordon"
  - subject: "Re: Shawn Gordon answers all the questions :)"
    date: 2001-08-31
    body: "Thank you for responding.  I was wondering what the whole Hancom thing was when I saw it at Linuxworld.  Is there a feature list of the office suite anywhere?  I saw the one on the Hancom website, but it just had a few highlights.  Also, it may help to have a comparison between Hancom office and the other office suites available for Linux.  This would really help people know what the advantages are versus the collective Linux community rolling its eyes about yet another office suite."
    author: "dan1123"
  - subject: "Re: Shawn Gordon answers all the questions :)"
    date: 2001-09-01
    body: "Let me add some words to this.\n\nFirst of all I would like to congratulate Hancom and theKompany with their product line-up. It looks very impressive.\n\nAs far as the relation between KDE and Hancom/theKompany goes I guess it is important for people to understand that KDE is basically three things:\n\n1) A desktop runtime environment (kdesktop, kwin, kicker, ksmserver)\n\n2) A development platform (qt/kdelibs)\n \n3) Appplications (konqueror, kmail, kdevelop, koffice, etc...)\n\nI expect to see an increase in the near future of commercial companies like Hancom and theKompany who will support KDE when it comes to point 1) and possibly 2) but who will compete on point 3) because selling commercial applications is their business with which they want to make a living.\n\nIf you look e.g. at Linux word processors you will see koffice, star/openoffice, abiword, Hancom Word and WordPerfect. That's the kind of competition that a healthy market needs and that is so much missing on the MSWindows platform.\n\nFor KDE I can see three major points that we should work on:\n\n*) Advocate KDE as the desktop solution of the future.\n\n*) Make it attractive for commercial software vendors to use the KDE development framework, not only Qt, but kdelibs as well.\n\n*) Keep commercial companies on the tip of their toes by making free applications that kick ass.\n\nCommercial software companies might not like that last point and some of us might not like to see commercial applications competing with our own brain-childs, but instead of wasting time on the differences in interests, we are better of focussing on the things we have in common and work together in those areas.\n\nJust like we can work together on konqueror with some of us preferring vi and some others emacs, with some of us working for SuSE and some others for Mandrake, we can also work together on kivio with some of us preferring KWord and others Hancom Word.\n\nEveryone has its own reasons for working on KDE, it are the things that we have in common that have made KDE successfull.\n\nCheers,\nWaldo"
    author: "Waldo Bastian"
  - subject: "Re: Shawn Gordon answers all the questions :)"
    date: 2001-09-01
    body: "(+5, Insightful)"
    author: "not me"
  - subject: "Re: Shawn Gordon answers all the questions :)"
    date: 2001-09-01
    body: "Well said, Waldo...way to go, Shawn!!"
    author: "Eron Lloyd"
  - subject: "Re: Shawn Gordon answers all the questions :)"
    date: 2001-09-01
    body: "8) As I recall you said that there would be a cut down GPL version of Rekall in KOfiice, is this canceled as well?\n16)If the filters are as good as you say then you may be going somewhere :-). Should i be right to expect that since their apps have been very popular in asia that international language support (Greek in my case :-) ) will be excelent too?\n17) How about a wrapper (in the linux version)0arts/Bonobo, so that your suite could communicate with everything?. Some standards would be nice you know...\n\nI wish you good luck, since you already helped KDE an Linux a lot."
    author: "t0m_dR"
  - subject: "Re: Shawn Gordon answers all the questions :)"
    date: 2001-09-01
    body: "8.  We had been trying to think of a way to put Rekall in KOffice, but had never really solved the problems before deciding to make it Qt based, so as a pure Qt application it doesn't make sense in KOffice anymore.\n\n17. Hadn't thought about greek yet, but there shouldn't be any reason why we can't support it.  Does it have any odd requirements like BiDi or anything?  I would be interested in support it."
    author: "Shawn Gordon"
  - subject: "Rekall"
    date: 2001-09-03
    body: "Hi Shawn,\n\nAfter reading your explanations I haven't really understood what you are exactly planning with Rekall... \nYou said there will be a \"stand alone\" version.\nWill it, to use the terms of your website, remain a \"project\" or will it become a \"product\"? In other words: do we have to pay for the final version?\n\nRegards,\nJuergen\n(who thinks your decision is a good one for your company *and* the Linux World)"
    author: "jsp"
  - subject: "Re: Rekall"
    date: 2001-09-03
    body: "Hi Juergen,\n\nThe plan for Rekall was always to have a commercial product at the end of the day."
    author: "Shawn Gordon"
  - subject: "Re: Rekall"
    date: 2001-09-03
    body: "OK, then I got something wrong.\nI thought your plan was to provide a base version of Rekall for free, while selling commercial plugins for it (I thought I read this somewhere before I started using it).\nBut of course you are free to change your strategies. I just wanted to be enlightened, for I'm using Rekall beta for my PhD and therefore want to know if I can count on it in the future.\n\nRegards,\nJuergen"
    author: "jsp"
  - subject: "Re: Rekall"
    date: 2001-09-03
    body: "The idea was to have a free basic version that didn't include nested forms or reports, something like that.  And then sell the 'pro' version for about $80 USD.  That might well still be the case, we just have been focused on finishing the app first."
    author: "Shawn Gordon"
  - subject: "Re: Rekall"
    date: 2001-09-03
    body: "That's what I meant.\nI am very pleased to hear that you still think about it.\nI think it's a good way to get a widespread userbase (and helpful feedback).\nAnd believe me, I am the least who wouldn't pay $80 for a good, stable product (after having the chance to try it out) ;-)\n\nRegards,\n\nJuergen"
    author: "jsp"
  - subject: "Re: Shawn Gordon answers all the questions :)"
    date: 2001-09-03
    body: "Will it be possible to buy HanComPainter only?\nIt looks nice."
    author: "reihal"
  - subject: "Re: Shawn Gordon answers all the questions :)"
    date: 2001-09-03
    body: "we haven't really discussed it, but I've always thought that making each piece available stand alone would be a good idea."
    author: "Shawn Gordon"
  - subject: "Re: Shawn Gordon answers all the questions :)"
    date: 2001-09-04
    body: "Good, then my personal needs will probably be satisfied with KOffice, complemented with a basic Rekall and HanComPainter."
    author: "reihal"
  - subject: "Re: Shawn Gordon answers all the questions :)"
    date: 2001-09-05
    body: "One question missed ;)\n\nWill the Hancom file formats be documented and open to use? Will HancomOffice be friendly and have import/export filters for open source application (KWord, KSpread, AbiWord, gnumeric, etc.. )?"
    author: "Joerg Baumann"
  - subject: "Re: Shawn Gordon answers all the questions :)"
    date: 2001-09-05
    body: "actually this topic hasn't come up yet so I don't have an answer for you."
    author: "Shawn Gordon"
  - subject: "Hate to say this..."
    date: 2001-09-01
    body: "Not \"KDE is coming to Main Street\", but QT is. And I don't think any KDE fan should like that. Not that QT is bad, of course it's great and all. But it's not a KDE product these guys are planning. Why don't they use all those wonderfull features KDE adds to QT (such as KPrinter etc.)??"
    author: "steven"
  - subject: "Re: Hate to say this..."
    date: 2001-09-01
    body: "I think QT going mainstream is better for KDE in the long run. Why?\n\nWell, As windows developers start coding with QT, when/if apps like Hancom Office become more popular, they'll realize that porting to linux/mac is easy, thus bringing more developers into the fold: \n\nMaking QT a popular choice can do nothing but help KDE. Hell, most open source developers are either dedicated to coding for either KDE or GNOME, or something else.  Windows programmers learning QT will be able to transition easily to KDE, and that's important, because there are still TONS of Windows guys out there than can benefit by having their code work on ALL platforms. I can't tell you how many windows apps that I use in windows are still \"working on linux versions\", with QT, it's relatively easy to write (almost)once, run (almost)anywhere.\n\nAs great a work as the KDE guys have been doing so far with the core desktop, getting the thousands of Windows programmers out there to develop in QT will only help us where we need it most, end user desktop application development for linux/bsd/whatever. I can't tell you how many programmers that I run into dont even KNOW that such a powerful cross-platform toolkit exists, I have to prove it to them! This is a market where the Trolls need to be pushing! QT for everyone, that's what I say! :)\n\nGood luck theKompany!"
    author: "jorge o. castro"
  - subject: "theQompany"
    date: 2001-09-01
    body: "Maybe \"the Qompany\" is a better name :-)\n<p>\nIt is hard this times to get money. Venture capital is over now.\nI think Ximian and VA Linux are trying to make money from Open Source. I readed carefully the ESR letter about Source Forge Enterprise Edition and I agree with his point of view.\nSome people are willing to pay a check in order to have the right to blasfem when things go wrong, even when they arent getting real support (Microsoft fixing bugs? He)."
    author: "V\u00edctor R. Ruiz"
  - subject: "Re: theQompany"
    date: 2001-09-03
    body: "Heh. Isn't Shawn Gordon a bit like Q? He keeps popping up everywhere... :)"
    author: "jd"
  - subject: "Free Software or don't bother..."
    date: 2001-09-01
    body: "I didn't switch to GNU/Linux and KDE merely to see organisations like the Kompany transform it into a propriatory Microsoft Lite environment. For some, including me, the move was more philosophically driven than practically. As such, I have to say that I see the Kompany more as a kind of leech, particularly in the latest announcement, than a userful contributor to everything behind the spirits of the projects they supposedly emerged as supporting.\n\nI wish KOffice the best of luck, because our and our children's intellectual freedom is only assured through a freely developed infrastructure upon which it may be expressed."
    author: "Nick Mailer"
  - subject: "Risk"
    date: 2001-09-01
    body: "I think we are seeing once again that there is a risk having commercial companies mix in with open source. There used to be a very nice program, kmysql. It was taken over by thekompany, and turned into vaporware. We still use it, using the kde1 compatibility libs, and I guess with the database features of qt3, something like it will probably be resurrected. But in the meantime the effect has been largely negative. What I mean is that commercial companies as in this vcase thekompany, can stifle opensource projects with their promises, and that, as nice as they may be, one should not trust them, and I am  including theKompany in this statement."
    author: "Erik Kj\u00e6r Pedersen"
  - subject: "Re: Risk"
    date: 2001-09-01
    body: "What in the world are you talking about?  We never took over kmysql.  What we did try to do was lend resources to it, but as both teams found out the code base for kmysql was going to require a complete re-write so we decided to go off and work on Rekall instead, this was an upfront conversation amoung the teams working.  Please don't try and blame us because a project dies, there are lots of projects that get started and never finished, as a matter of fact that is probably true of most projects.\n\nI'm sorry to hear you think our influence has been negative, but I think your opinion is based on flawed information."
    author: "Shawn Gordon"
  - subject: "Re: Risk"
    date: 2001-09-01
    body: "You may be right. There is nothing that has saddened me quite as much in the kde project as the death of kmysql. I may have had a wrong impression of why that happened. If so I am sorry\n\nErik Kj\u00e6r Pedersen"
    author: "Erik Kj\u00e6r Pedersen"
  - subject: "Re: Risk"
    date: 2001-09-01
    body: "You don't have to be very sorry. It is natural that you had such a reaction because nobody informed you. (happily the Kde1 compatibility libs was some good help)\n\nI see a similar thing in the last KC KDE about KWinTV. The users were not informed that nobody maintened it and the KDE team had not a big attention for maintening it. Happily, a developper involved himself in this program. Thanks to him, but I think that such a care has to be collective, at first. And the first thing to do is to give information...\n\nOf course, there are also many consciensious guys who announce that they stop and search somebody to continue...\nBut perhaps it would be useful to give some recommandations for the KDE developpers, saying particularly that the team has to be advertised when a developper stops to maintain (think also about a sudden die or accident...) \n\nThe more KDE is popular, the more it needs to be considerate towards the durability of its programs..."
    author: "Alain"
  - subject: "Re: Risk"
    date: 2001-09-05
    body: "Perhaps you may use SqlGUI  ? http://apps.kde.com/na/2/info/vid/3762"
    author: "Alain"
  - subject: "Anyone even used 1.5??..."
    date: 2001-09-02
    body: "I've been searching the web in vain for a review of their 1.5 :( The only reviews I've found for any of their stuff in english has been for their original wordprocessor - suffice it to say, most ppl seemed to think that it sucked (been bassed largly around wine) - Can it render fonts better than the likes of staroffice? I'm hoping it'll do anti-aliasing, but it doesn't say anywhere :(\n\nI'm probably one of the few people that would like to buy it - if it supports QT properly, it should (I hope!) support font anti-aliasing and it seems to have a good selection of MS import/export filters (which are unfortunatly important to me.) KWord is good, but it's lacking in export filters, Star Office would be great if I could just read the damn text :\u00ac)"
    author: "dave"
  - subject: "Re: Anyone even used 1.5??..."
    date: 2001-09-03
    body: "Yes I have.  And it does work.  Very well as a matter of fact.  I've been using Hancom's products since HangulWord1.0 (about 10 years ago) on an amiga 500 running a Dos emulator.  The products have continueally been very easy to use even when I didn't speak or read Korean.  I've found that under Linux (Mandrake 8.0 and Redhat 7.0) it's sits stably on my box.  It opens and runs faster than Star Office by a long shot, and has yet to fail in the arena of opening those *%^$ m$.doc files people keep sending me.  I've opened edited and saved M$.doc and Exhell files, sent them to windows users and they have been able to open them without fail.  In short I use it daily.  Have been since they started looking for beta testors.  My wife uses it to enable her to exchange Korean documents with her family and has also found that it works equally well with the Korean version of windows and m$ office.  2.0 doesn't have the import filters yet but when it does I for one will be installing it immediately.\n\nPS if you want to know why Hancom is in M$'s path look here http://linuxtoday.com/stories/10715.html"
    author: "James Sparenberg"
  - subject: "CJK support?"
    date: 2001-09-02
    body: "More interesting is the question, wether they will have support for Chines, Japanese or Korean, because that was Hancom's primary feature. Asian language input is still a problem in KDE2."
    author: "Root_42"
---
During the first day of the
<A HREF="http://www.linuxworldexpo.com/">LinuxWorld Expo</A>,
<A HREF="http://www.hancom.com/en/">Hancom Linux</A> and
<A HREF="http://www.thekompany.com/">theKompany.com</A> became the talk of the show with the news that they are merging their product lines
and releasing a complete Linux/KDE office suite this coming
November.  Dubbed
"<A HREF="http://www.hancom.com/en/product_service/office.html">HancomOffice
2.0</A>", the suite will combine 4 Hancom products
(Word, a word processor, Sheet, a spreadsheet, Presenter, a
presentation program, and Painter, a bitmap drawing program) with
4 theKompany.com products (EasyDB, a personal database management
system familiar to us as
<A HREF="http://www.thekompany.com/projects/rekall/index.php3">reKall</A>,
Envision, a diagram and flowchart drawing tool familiar to us as
the <A HREF="http://www.koffice.org/">KOffice</A> component
<A HREF="http://www.thekompany.com/projects/kivio/">Kivio</A>,
WebBuilder, an HTML/PHP editing tool familiar to us as
<A HREF="http://quanta.sourceforge.net/">Quanta+</A>, and QuickSilver,
a personal information manager familiar to us as
<A HREF="http://www.thekompany.com/projects/aethera">Aethera</A>).
The Word/Sheet/Presenter applications are advertised as outstanding
at both importing and exporting the corresponding MS Office formats.  Because it uses Qt 3.0 the same
boxed set will run on Linux, Mac OS X, Windows and -- yes --
even embedded devices (some planned devices were announced).
Although the products are pure Qt, with 3.0's new features they should
integrate nicely into the KDE desktop.  
Suggested retail price:  $99.  Ready or
not, KDE is coming to Main Street!
<!--break-->
