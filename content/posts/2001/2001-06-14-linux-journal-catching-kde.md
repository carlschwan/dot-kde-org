---
title: "Linux Journal: Catching up with KDE"
date:    2001-06-14
authors:
  - "numanee"
slug:    linux-journal-catching-kde
comments:
  - subject: "Re: Linux Journal: Catching up with KDE"
    date: 2001-06-14
    body: "Wow, that had to be the best KDE review I've ever read.....\nMost of the one's I've seen have always left me with a feeling that the reviewer didn't really understand what he was looking at...\nwhereas Robert has obviously been following KDE pretty closely...\n\nhmmmmm....or does that make him biased?...heh...\n\nanyway......'s always nice to see good positive articles like this....the developers need this sort of pat on the back for the good effort that has gone into KDE over the years..."
    author: "Stuart Herring"
  - subject: "Re: Linux Journal: Catching up with KDE"
    date: 2001-06-15
    body: "> Wow, that had to be the best KDE review I've ever read.....\n\nPerhaps for developpers, not for users.\n\nFor users, it is a bad review with many and many technical words showing it is difficult.\n\nI am surprised that I had never seen a KDE review for users, showing how it is a whole environment with big advantages :\n- very customizable\n- very powerfull\n- a large set of programs\n- free\n\nOf course there are some disadventages, but they are decreasing and they are not critical for most users.\n\nIf you include the next stable Koffice, if you add the usage of vmware or win4lin, it is the best environment for desktops users. Today (or almost, for august/september).\n\nI wait that some reviews will show such a report."
    author: "Alain"
  - subject: "Re: Linux Journal: Catching up with KDE"
    date: 2001-06-14
    body: "I must admit, I do enjoy using GNOME as well as KDE.  But, I tend to work on both Solaris x86 and Linux, which makes it difficult to use GNOME.  The recently released Solaris packages of GNOME from Sun have made it easier, but there are still a TON of bugs which makes it very unstable.  It's great to see that the KDE developers have made cross-platform stability a major concern.  Being able to have such a functional interface combined with things like KDevelop makes my life much easier.  Looking forward to KDE 3!!!!  Keep up the good work!"
    author: "Steve Nakhla"
  - subject: "Re: Linux Journal: Catching up with KDE"
    date: 2001-06-15
    body: "why did sun choose GNOME then?\n\ni don't get - but i couldn't figure out windows either\n\nThilo"
    author: "Thilo Bangert"
  - subject: "Re: Linux Journal: Catching up with KDE"
    date: 2001-06-15
    body: "I don't want to start a flamewar, but I would guess that one reason might be that gtk is under the lgpl whereas qt is under the gpl. That means non-free/closed-source developers don't have to pay anyone to develop for Gnome. You can decide for yourself whether that's good or bad.\n\nAnother possibility, and this is just speculation, is that they prefer C to C++. Could be because of old habits or because of the current state of C++ compilers. Again, I have no basis for this; it's just a thought.\n\nIt could also simply be that they evaluated the technical merits of both DEs and decided that Gnome was, at the time of evaluation, better. After all, both sides claim technical superiority and Sun could have just related to the Gnome camp better."
    author: "kdeFan"
  - subject: "Re: Linux Journal: Catching up with KDE"
    date: 2001-06-15
    body: "Nah... Most likely they were offered membership in the Gnome foundation and thought that promising Gnome as a part of future releases of Solaris would be a great way to get some extra publicity.  I.e. Who could then claim that they are not \"Embracing Open Source\"?\n\nIn short a purely market driven decision.  If Gnome on Solaris doesn't live up to the promise Sun can (and will) say so.  If it works well they will do as they promised and make some extra $$$ in the process.  If KDE offers a far better solution they will make the download available from the FTP site or perhaps even ship a SkunkWorks CD with it on.  For the record there is nothing stooping Sun from adopting BASH as the default shell.  However you still have to add it after installation."
    author: "Forge"
  - subject: "Re: Linux Journal: Catching up with KDE"
    date: 2001-06-14
    body: "Konqueror has been spelt \"Konquerer\" throughout the whole article. I see this (or other variants such as \"Konquer\" etc) all the time. Oh well, a nice review in all other respects :)"
    author: "Haakon Nilsen"
  - subject: "Re: Linux Journal: Catching up with KDE"
    date: 2001-06-15
    body: "Okay, this is one thing that really bugs me.  The word is \"spelled\", not \"spelt\".  Spelt is not a word.  I hate to be a grammar troll, but whenever I see someone critiquing someone else's work when it comes to spelling without spellchecking their work, it really bugs me.\n\nHmm...  How's this for a feature request.  The ability to run a spell check by right clicking on the text box in konquerer?  At the very least, it would rid us of one more excuse for poor spelling on the Internet."
    author: "Chad Kitching"
  - subject: "Re: Linux Journal: Catching up with KDE"
    date: 2001-06-15
    body: "Man that would really help me out i miss spell words on here all the time.\n\nCraig"
    author: "Craig Black"
  - subject: "Re: Linux Journal: Catching up with KDE"
    date: 2001-06-15
    body: "isn't that spelled \"criticizing\" ? :) The spellchecker idea is a nice one though."
    author: "Ralf Nolden"
  - subject: "Re: Linux Journal: Catching up with KDE"
    date: 2001-06-15
    body: "lol"
    author: "Craig Black"
  - subject: "Re: Linux Journal: Catching up with KDE"
    date: 2001-06-15
    body: "no, critisising\nhttp://www.dictionary.com/cgi-bin/dict.pl?term=criticise"
    author: "oliv"
  - subject: "Re: Linux Journal: Catching up with KDE"
    date: 2001-06-17
    body: "Actually, both are correct.\n\nhttp://www.dictionary.com/cgi-bin/dict.pl?term=criticize"
    author: "Chris Bordeman"
  - subject: "spelt: British variant"
    date: 2001-06-15
    body: "Below is what kdict connected to dict.org:2628 says about \"spelt\".\n\nAs far as I know \"spelt\" is an older form of the\np.p. of spell that is still common in British English.\nIs your dictionary really so poor that it omits\nthis information? \n\n Webster's Revised Unabridged Dictionary (1913) [web1913]\n\nSpell \\Spell\\, v. t. [imp.&  p. p. Spelled or Spelt; p. pr. &\n   vb. n. Spelling.] [OE. spellen, spellien, ...]\n   1. To tell; to relate; to teach. [Obs.]\n\n            Might I that legend find, By fairies spelt in mystic\n            rhymes.                               --T. Warton.\n\n   2. To put under the influence of a spell; to affect by a\n      spell; to bewitch; to fascinate; to charm. ``Spelled with\n      words of power.'' --Dryden."
    author: "L. Jensen"
  - subject: "Re: Linux Journal: Catching up with KDE"
    date: 2001-06-15
    body: "While checking his grammar and spelling, perhaps you missed one of your own.  You say, \"whenever I see someone,\" but later in the same sentence, while refering to the same individual, you say, \"without spellchecking their work.\"  Using the word their when refering to one person is incorrect.  You might prefer to use the word his."
    author: "A. C."
  - subject: "Re: Linux Journal: Catching up with KDE"
    date: 2001-06-15
    body: "It is however an increasing common idiom in modern colloquial English -- it avoids specifying a gender like \"him\" does, and doesn't have \"it\"'s overtones of being a non-human object.\n\nThis is part of the continual evolution and simplification of English -- the disappearance of \"whom\"; the disappearance of the \"neither-nor\" construct; widespread confusion over the correct placement of apostophe's (sic); the American trait of 'verbing' the language; and so on.\n\nIn summary: you are both right. :)"
    author: "Jon"
  - subject: "Re: Linux Journal: Catching up with KDE"
    date: 2001-06-15
    body: "In British English it is spelt. In American English it is spelled. Being Brit, I prefer spelt."
    author: "ne..."
  - subject: "Re: Linux Journal: Catching up with KDE"
    date: 2002-03-11
    body: "I find it quite strange that Americans don't bother to check whether a word is spelt differently by the rest of the world before calling us all wrong. I'm not from Britain but I use \"spelled\" in the immediate past and \"spelt\" in the more distant past. "
    author: "Anil"
  - subject: "Re: Linux Journal: Catching up with KDE"
    date: 2004-02-03
    body: "Even in British English, the word \"spelt\" is wrong. It's an anachronism and is no longer used."
    author: "James Gossling"
  - subject: "Re: Linux Journal: Catching up with KDE"
    date: 2006-01-12
    body: "For the record, the word \"spelt\" is perfectly correct in the UK and Australia. In fact the Oxford English Dictionary uses this word (in preference to \"spelled\")all over its website, which I think proves that it is not an anachronism. \n\nhttp://www.askoxford.com/concise_oed/spelt\n\nhttp://www.askoxford.com/asktheexperts/faq/aboutspelling/learnt\n\nhttp://www.askoxford.com/asktheexperts/faq/aboutspelling/perjorative\n\n"
    author: "Fran"
  - subject: "Re: Linux Journal: Catching up with KDE"
    date: 2006-06-04
    body: "I love the 'EVEN in British English'; like it's the rubbish version of English that everybody else has to put up with, rolling their eyes and saying 'Yes dear, that's nice'...."
    author: "passing through"
  - subject: "Re: Linux Journal: Catching up with KDE"
    date: 2006-06-13
    body: "'Spelt' is so familiar to me in England that seing 'is spelled..' looks odd. Which is how I found myself in this conversation, having googled 'spelled' due to context curiosity after seing it used to correct another. eg. '[word] is spelled [word]'\n\nSo, I hold myself up as living proof 'spelt' is not only alive and kicking, but the norm unless somewhere where that part of the language has been altered."
    author: "Nick"
  - subject: "Re: Linux Journal: Catching up with KDE"
    date: 2006-06-13
    body: "And I am aware seing should be 'seeing', that's a habit I personally have. \n\nI'm not perfect, but I do know what's familiar. :P"
    author: "Nick"
  - subject: "Re: Linux Journal: Catching up with KDE"
    date: 2006-07-13
    body: "I myself grew up in the British system of schooling, living overseas in Hong Kong, when I found myself in contention with a scholar friend here in the States of whether 'spelled' or 'spelt' was correct. The latter 'sounds' correct to me, as the previous poster indicated, because from my past experience, 'spelled' does seem odd to me. \n\nHe seemed confused that I couldn't switch between the two, and that it was a convention I needed to adopt living here. But would you write a certain way if it seems incorrect to you? \n\nPlease, getting hung-up on typo's detracts from the discussion."
    author: "wing"
  - subject: "Re: Linux Journal: Catching up with KDE"
    date: 2006-09-24
    body: "I had a good chuckle whilst reading this discussion of 'spelled' v. 'spelt' - and yes,'v' is how the abbreviation for 'versus' is spelt."
    author: "Atlantean"
  - subject: "Re: Linux Journal: Catching up with KDE"
    date: 2007-07-04
    body: "FROM DICTIONARY.COM\n\nspelt    /sp&#603;lt/    \n\u0096verb\na pt. and pp. of spell.\n\n\nAlso, in every other country in the world besides the US, it is spelt: criticising  not criticizing.\n\nIf you are going to be a grammar troll, at least be an informed grammar troll.\n\nMatt. "
    author: "Matt Vermeulen"
  - subject: "Re: Just one thing about  konqueror"
    date: 2001-06-14
    body: "Its great. But only when you have one browser window open. When you go up to five, X just slows down.\n\nOpera does not have this behavior. Ok, opera does not spawn new windows with new links. Maybe thats why.\n\nNetscape used to behave the same way at 4.01.\n\nAlso, considering that world cup next year will be in japan (gmt + infinity), please fix noatun, so we can watch the games live.\n\n\nnalo"
    author: "nalogquin"
  - subject: "Re: Just one thing about  konqueror"
    date: 2001-06-15
    body: "IIRC, this is changed in CVS, where a copy of Konqui is reused. Unfortunately, that means that crash in one copy would cause a crash in all - but seeing that stability is OK in the alpha, it will probably not be a problem by the time of final 2.2.."
    author: "A Sad Person"
  - subject: "Re: Just one thing about  konqueror"
    date: 2001-06-15
    body: "then why are you so sad?"
    author: "me"
  - subject: "Re: Just one thing about  konqueror"
    date: 2001-06-15
    body: "<quote>\n\"Its great. But only when you have one browser window open. When you go up to five, X just slows down.\"\n</quote>\n\nI have seen this too.  I'm not convinced it is\na new process vs. new window issue, becuase the\nway I use konq is to open links in new windows\n(which doesn't launch a new process).  The slowdown is particularly noticible on a slow\nmachine, but it has happened on my Duron 650/128MB box as well.\n\nAnyone else seeing this?"
    author: "me too"
  - subject: "Re: Just one thing about  konqueror"
    date: 2001-06-15
    body: "I use many Konqueror windows (whether through Alt-L-W, Alt-F2, Web-URL, or middle-click), and often.  I certainly use more than a puny 5 of them.  Can't say I've seen this problem.  I've got a Celery 400 w/192M (64M until recently)."
    author: "Navindra Umanee"
  - subject: "Re: Just one thing about  konqueror"
    date: 2001-06-15
    body: "It might be related to lots of animated gifs being shown in all those browser windows. Try\nstopping the animations to see if it makes a difference."
    author: "Carsten Pfeiffer"
  - subject: "Re: Just one thing about  konqueror"
    date: 2001-06-16
    body: "this could be a cause for it, but\ni've also seen the problem. when loading\na very large document in another window\nscrolling in the current window becomes\nalmost impossible.\n\nthreading?"
    author: "Alex"
  - subject: "Re: Linux Journal: Catching up with KDE"
    date: 2001-06-14
    body: "Is IBM still committed on working on ViaVoice ?\nI did not see any news about it in QT 3.0's upcoming release. Anyone has news ?"
    author: "Raphael Borg Ellul Vincenti"
  - subject: "Re: Linux Journal: Catching up with KDE"
    date: 2001-06-16
    body: "Yes, I'd be interested too.\nWas the effort cancelled?"
    author: "mg"
  - subject: "Re: Linux Journal: Catching up with KDE"
    date: 2001-06-15
    body: "And there are some disadvatages too:\n\nQT isn't free for non-GPL programs. It costs about 3000$ and this price will become higher with each new release - serious drawback as linux gains more popularity not in corporate US world but in countries where people earn $3000 a year, how about in-house development, spiral cost of upgrades - aren't there argumens of MS opponents ? Doesn't it break an idea of a free system ?\n\nTrollTech doesn't accepts OpenSource code - which means that some KDE features will be slowly implemented in Qt which leads to doubling similar code in QT and KDE or breaking source compatybility between succesive KDE versions.\n\nIt seems that when I want to buy Kylix, BlackAdder and QT I must pay for QT license three times.\n\nBig companies won't put KDE as a default desktop without a full licence, they won't buy it ( how many developers will be using it ? ), so they are not enthusiastic when funding KDE projects ( theKompany isn't a big company ) \n\nDidn't mention about such details as poor signal/slot infrastructure ( neither effective nor typesafe - it looks like a Perl hack in C++ ) or that funny Windows-like fixed clip area when repaintg ( oh there is difference - in Window I can reset it ).\n\nSome of the above are only minor details but some are serious threats."
    author: "Doubtful"
  - subject: "Re: Linux Journal: Catching up with KDE"
    date: 2001-06-15
    body: "Didn't mention about such details as poor signal/slot infrastructure ( neither effective nor typesafe - it looks like a Perl hack in C++ ) or that funny Windows-like fixed clip area when repaintg ( oh there is difference - in Window I can reset it \n\n...hehehe... so you don't have something comparable to signal/slot (in efficiency) ?"
    author: "ac"
  - subject: "Re: Linux Journal: Catching up with KDE"
    date: 2001-06-15
    body: "> QT isn't free for non-GPL programs. \n\nnon-GPL -> non-OpenSource.\n\n> It costs \n> about 3000$ \n\nYou mean after multiplying by 2?\n\n> and this price will become higher\n> with each new release - serious drawback as\n> linux gains more popularity not in corporate US\n> world but in countries where people earn $3000 a\n> year, how about in-house development, spiral\n> cost of upgrades - aren't there argumens of MS\n> opponents ? Doesn't it break an idea of a free\n> system ?\n\nNo, it actually promotes free software by making\nit economically less attractive to write closed-source software. \n\n> TrollTech doesn't accepts OpenSource code -\n> which means that some KDE features will be\n> slowly implemented in Qt which leads to doubling\n> similar code in QT and KDE or breaking source\n> compatybility between succesive KDE versions.\n\n??? Please explain. How does one lead to the\nother? What has source comaptibility to with it?\n\n> that funny Windows-like fixed clip area when\n> repaintg ( oh there is difference - in Window I\n> can reset it ).\n\nvoid QPainter::setClipping ( bool enable )\n\n Enables clipping if enable is TRUE, or disables\n clipping if enable is FALSE.\n\nCheers,\nWaldo"
    author: "Waldo Bastian"
  - subject: "Re: Linux Journal: Catching up with KDE"
    date: 2001-06-15
    body: "I honestly wonder why you replied to him, his post was in no case worth it! It just didn't really have any content."
    author: "me"
  - subject: "Re: Linux Journal: Catching up with KDE"
    date: 2001-06-15
    body: "It has misleading content, that's always a reason to reply. Especially if the one wants to get some more insight into the things he's stating. Instead of setting up such arguments, one should be very clear about what he writes before, and we're always open for any questions after all that can be asked to get a clear insight before posting wrong statements."
    author: "Ralf Nolden"
  - subject: "Re: Linux Journal: Catching up with KDE"
    date: 2001-06-15
    body: "Thanks man !"
    author: "Doubtful"
  - subject: "Re: Linux Journal: Catching up with KDE"
    date: 2001-06-15
    body: "> You mean after multiplying by 2\nUnix/Windows costs 3000$, Unix only 2000$. Do you know how much cost upgrades ( from 1.4 to 2.0 for example ) ? I could not find this informations on the www.\n\n\n>No, it actually promotes free software by making\n>it economically less attractive to write \n>closed-source software. \n\nYes If you sell programs it probably doesn't matter. But what if I have four or five desktops and I want to write a simple database front-end or other simple scripts with GUI ?  \n\n\n> void QPainter::setClipping ( bool enable )\n> Enables clipping if enable is TRUE, or disables\n> clipping if enable is FALSE.\n\nDoesn't work for me ... Of course I can modify a clipping area but nevertheless it is always clipped to the fixed updating rectangle ( I can't paint outside of it ). This isn't a problem for me because in the repaint event I can post QEvent::User to myself and in QEvent::User ( outside repaint handler ) do the paint work. It is just ... funny. Oh, those details - there is probably nothing better than Qt, but why it is so expensive."
    author: "Doubtful"
  - subject: "Re: Linux Journal: Catching up with KDE"
    date: 2001-06-15
    body: "> > You mean after multiplying by 2\n\n> Unix/Windows costs 3000$, Unix only 2000$. Do\n> you know how much cost upgrades ( from 1.4 to\n> 2.0 for example ) ? I could not find this\n> informations on the www.\n\nAh, I now see that they distinguish between a\nprofessional and enterprise version. The\nprofessional version for a single platform is\n$1550. (I don't think this has changed much over\ntime).\n\nWrt upgrades:\n\"Upgrades: \n\n All new versions of Qt released during the\n support/upgrade period are available to\n customers free of charge.\"\n(http://www.trolltech.com/products/purchase/pricing.html)\n\nThe support/upgrade period is 1 year and you can\nextend that for $480,- / year. \n\nI always thought it was $1550 / year but\nappearantly this $1550,- is one time only.\n\n> >No, it actually promotes free software by\n> >making it economically less attractive to write\n> >closed-source software. \n\n> Yes If you sell programs it probably doesn't\n> matter. But what if I have four or five desktops\n> and I want to write a simple database front-end\n> or other simple scripts with GUI ?\n\nIf you don't plan on selling the program anyway\nyou can just as easy put a GPL/QPL/BSD license on\nit. As long as you don't distribute your program\nthat makes little difference. Saving some bucks\nwas never that easy :-)\n\n> Doesn't work for me ... Of course I can modify a\n> clipping area but nevertheless it is always\n> clipped to the fixed updating rectangle ( I\n> can't paint outside of it ). This isn't a \n> problem for me because in the repaint event I\n> can post QEvent::User to myself and in\n> QEvent::User ( outside repaint handler ) do the\n> paint work. It is just ... funny. \n\nYes.. inside the paint event you can only\npaint within the update rect for some reason. But\nwhy would you want to paint outside of it? Either\nthe rest doesn't need repainting or you should\nhave asked for a larger repaint region in the\nfirst place I would think. Am I missing something?\n\nCheers,\nWaldo"
    author: "Waldo Bastian"
  - subject: "Re: Linux Journal: Catching up with KDE"
    date: 2001-06-15
    body: "> Unix/Windows costs 3000$, Unix only 2000$. Do \n> you know how much cost upgrades ( from 1.4 to \n> 2.0 for example ) ? \nUpgrades are free. The Qt licence is available for all your lifetime and is transferable between developers.\n\n> >No, it actually promotes free software by making\n> >it economically less attractive to write\n> >closed-source software.\n> Yes If you sell programs it probably doesn't \n> matter. But what if I have four or five desktops\n> and I want to write a simple database front-end \n> or other simple scripts with GUI ? \nEither you are on Unix and you can use the GPL Qt.\n\nOr you are on windows and you have to pay. This is normal to pay on windows. You pay for visual C++, you pay for visual Kaffe, you pay for borland, you pay for windows, you pay for office, why wouldn't you pay for Qt ?\n\nThe Qt licence is not very expensive for a corporate company."
    author: "Philippe Fremy"
  - subject: "Re: Linux Journal: Catching up with KDE"
    date: 2001-06-15
    body: "Before last fall  the gnome folks enjoyed beating the drum about kde's \"licensing issues\" . This was there only good argument. Now there are no licensing issues so they really have to reach and sort of manufacture problems to troll about. This is what's going on here. Its a very common argument on slashdot. Its nice to be a KDE backer and not have to make empty arguments like this. The choice between closed or open source is really a strength of the Qt license not its liability. The gnome people know this so they take issue with it in hopes that it will change or they just take the opportunity for just spreading fud about the competitions strength. Anyone that thinks theres no competition is crazy. There are millions of dollars being invested into gnome and they can't afford to have a split linux desktop. So believe me they will take whatever opportunity to spread doubt. This guy might not be doing that but he has picked up on the argument spread by the gnome folks and now is just lipping it.\n\nCraig"
    author: "Craig Black"
  - subject: "Re: Linux Journal: Catching up with KDE"
    date: 2001-06-15
    body: "Yes I agree, this is only one good argument - but it is a strong argument. This is the only reason why RedHat, Sun and HP promotes GNOME."
    author: "Doubtful"
  - subject: "Re: Linux Journal: Catching up with KDE"
    date: 2001-06-15
    body: "I think SUN is having some regrets about there gnome choice. After Eazels demise and seeing the faster pace that kde is progressing even with out the millions and millions being pumped into it. Kde can have proprietary and open source applications gnome can only have open. This is a real problem for them and it will be there undoing. They can't artificially shore themselves up on venture capital forever. Kde will have great commercial applications like Kapital and great open source applications like konqueror and kdevelope. Gnome will be hurting when the VC drys up and they'll be wishing for the old days when they could beat there license drum. For now they can just manufacture lame crap and say the closed license costs to much. This ignores the fact that you can't buy one for gtk at all. They know when the window developers start developing for the Linux desktop it won't be for gnome but for kde. This scares them. Hense the regular anti kde fud on slash dot. When you see them spreading it just laugh them off there days are numbered. I know the desktop will survive but it will be far from main stream and will be dry of millions in VC that has kept them propped up for so long. After last fall when qt became open the only thing that they could do was use other peoples money for development. Gnome lost its native steam and is now burning on fumes."
    author: "Craig Black"
  - subject: "Re: Linux Journal: Catching up with KDE"
    date: 2001-06-15
    body: "> Kde can have proprietary and open source\n> applications gnome can only have open\n\nPlease do some research before making such claims. Gtk+ and the Gnome libraries are under the LGPL, same as the KDE libraries."
    author: "blah"
  - subject: "Re: Linux Journal: Catching up with KDE"
    date: 2001-06-26
    body: "This post is just plain stupid. Did you know that a LGPL license existed? And what made you belive that the gnome project wouldn't be using it?\n\nAnd your bashing the gnome people pretty har there. I would like to say that aggresivity is often a sign of stupidity."
    author: "ac"
  - subject: "Re: Linux Journal: Catching up with KDE"
    date: 2001-06-15
    body: "Yes I agree, this is only one good argument - but it is a strong argument. This is the only reason why RedHat, Sun and HP promotes GNOME."
    author: "Doubtful"
  - subject: "Re: Linux Journal: Catching up with KDE"
    date: 2001-06-15
    body: "There are no more licensing issues.  QT is gpl'ed, end of story.  There's no more drum to beat!"
    author: "Phillip Thurmond"
  - subject: "Re: Linux Journal: Catching up with KDE"
    date: 2001-06-16
    body: "Qt is dual licensed.  This means that UNLIKE with GTK, you are allowed to write non-gpl compatible programs.  Yes, it'll cost you, but isn't having an expensive option better than none at all?"
    author: "Galvatron"
  - subject: "Re: Linux Journal: Catching up with KDE"
    date: 2001-06-16
    body: "You are a dumbass. \n\nGnome/GTK is LGPLed. \n\nSo you can write code with any licence to link against it."
    author: "RObert"
  - subject: "Re: Linux Journal: Catching up with KDE"
    date: 2001-06-15
    body: "K is nice but \"supporting 34 languages\":? - I never able to get konqueror to display Chinese. All I got is square for every character. Netscape could do that. So I believe font is loaded in X. Yes, I searched mailing list but found nothing."
    author: "abemud"
  - subject: "Re: Linux Journal: Catching up with KDE"
    date: 2001-06-15
    body: "Chinese seems to have a problem. I wanted to try it the other day and after installing the proper fonts Netscape was able to show the chinese yahoo correctly but konqueror didn't. Japanese on the other hand seems to work fine.\n\nCheers,\nWaldo"
    author: "Waldo Bastian"
  - subject: "Re: Linux Journal: Catching up with KDE"
    date: 2001-06-16
    body: "try changing the encoding or if that\nfails take a look an knolls anti-aliasing\ntips which has pointers to very nice ttf\nfonts which work prefectly here.\n\nthough why on debian.org i need to change\nthe encoding (iirc might be fixed) i'm not\nsure...\n\nAlex"
    author: "Alex"
  - subject: "a bit off topic"
    date: 2001-06-15
    body: "There seems to be some problem running Borlands JBuilder 5 under KDE, and one of the solutions from borland is to switch to GNOME!\nSee link:\nhttp://community.borland.com/article/0,1410,27307,00.html\n\nWhat were they thinking?"
    author: "DiCkEII"
  - subject: "Re: a bit off topic"
    date: 2001-06-15
    body: "i have problems running every java app under kde (i cant type '(' or ')' anymore). switching to the ibm JDK fixes it for me. those guys were not anti-KDE anyway, they just say 'switch WM' not 'switch desktop'.\n\nbtw i can't see if jdk 1.3.1 solved it, it just segfaults here, both when used in staroffice and when used with together whiteboard. i'd like to switch back to the sun jdk, but this is stopping me."
    author: "ik"
  - subject: "Re: a bit off topic"
    date: 2001-06-16
    body: "Matthias Ettrich once posted an amusing comment about how he kept \"fixing java bugs\" in KWM/KWin and how at every new JDK release, those bugfixes mysteriously broke.  \n\nTurns out the Java guys were putting in \"special code\" in the JDK to \"fix\" the bugfixes in KWM/KWin, and so they kept undoing all the hard work Matthias was putting in to deal with Java.  Apparently the Java people expect windows to behave one way, and Matthias had other (enlightened?) ideas.\n\nWouldn't be too surprising if this situation has blown up in our faces today. FWIW, \"appletviewer http://java.sun.com/\" works fine here on KDE 2.1.1 with java version 1.3.0_02."
    author: "Navindra Umanee"
  - subject: "Re: a bit off topic"
    date: 2001-06-16
    body: "I have IBM-JDK installed on my RH7.1 but even after giving correct path of java in setting, applets doesn't start. only grey box is seen. Can you tell me if I have to do some extra settings ?"
    author: "vk"
  - subject: "Re: a bit off topic"
    date: 2003-12-14
    body: "You need to allow Java support in the Konqueror settings menu.  Under Java/Javascript."
    author: "Danyel"
  - subject: "Re: Linux Journal: Catching up with KDE"
    date: 2001-06-26
    body: "it says window manager not desktop environment."
    author: "ac"
---
<a href="mailto:flemming@valinux.com">Robert Flemming</a> of <a href="http://www.valinux.com/">VA Linux Systems</a> has written <a href="http://noframes.linuxjournal.com/lj-issues/issue87/4728.html">a very nice review of KDE 2.1.1</a> for <a href="http://linuxjournal.com/">Linux Journal</a>.  The review covers everything from anti-aliasing to IO Slaves, and comes complete with obligatory screenshots. <i>"KDE developers may be one step closer to ``konquering'' the desktop with the most recent 2.1.1 release of the K Desktop Environment. The development cycle has intensified since the 1.0 series, bringing new features and stability improvements to users at an ever-increasing rate. In fact, as of this writing, the first alpha version of KDE 2.2 has been released for testing. End users and developers alike will benefit from the newest offering. Currently supporting 34 languages, KDE is poised to answer many of the questions surrounding Linux' viability on the desktop."</i>
<!--break-->
