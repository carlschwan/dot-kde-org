---
title: "Gesture Recognition for KDE"
date:    2001-05-24
authors:
  - "numanee"
slug:    gesture-recognition-kde
comments:
  - subject: "Re: Gesture Recognition for KDE"
    date: 2001-05-24
    body: "Aaaaah, kool!\nMaybe it works even better with a pen-pad, like those from Wacom."
    author: "reihal"
  - subject: "Re: Gesture Recognition for KDE"
    date: 2001-05-24
    body: "I think no !\nI've tested with a DigitalEdge PenPad and it works less good than with mouse.\nAlthough it was easier to go through the configuration steps with my tablet, the results on the screen were less good !\n\nTom"
    author: "Tom"
  - subject: "Re: Gesture Recognition for KDE"
    date: 2001-05-24
    body: "i love it :) a feature I haven't seen on too many other systems (except black&white).. looks promasing!"
    author: "xasto"
  - subject: "Re: Gesture Recognition for KDE"
    date: 2001-05-24
    body: "Speaking of Black & White, I gave it a try some days ago and the mouse movement recognition stuff is really nifty, I've never ever seen any other program which can handle as complex shapes that good.\n\nWas the functionality which is responsible for that feature developed just for Black & White or does it originate from some external source? I can imagine it would be interesting to give it a try."
    author: "Frerich Raabe"
  - subject: "Re: Gesture Recognition for KDE"
    date: 2001-05-24
    body: "I used to follow the lionhead-diaries, until shortly.. If I remember correctly, the idea of gestures was the result of a brainstorming-session.. (therefore I guess it's inhouse, but it's no proof..)"
    author: "xasto"
  - subject: "Re: Gesture Recognition for KDE"
    date: 2001-05-24
    body: "I remember I tried a similar app a three or four years ago on MS Windows. It is useless.\nFirst off all it is useless for launching applications: why to draw when you can click on an icon or even better press a key on your keyboard. Second, I don't have any space on my desktop while working: this means that I have to close some running applications and draw something on my desktop to launch a new app. \nNo thanks, Ctrl + K = Konqueror is running :)"
    author: "Antialias"
  - subject: "Re: Gesture Recognition for KDE"
    date: 2001-05-24
    body: "I don't know whether it's really useful, but it\nis better than you think in at least one way:\nyou don't need to be able to get at the root \nwindow to use the gesture. You just wave the \nmouse wherever it happens to be on the screen."
    author: "gnb"
  - subject: "Re: Gesture Recognition for KDE"
    date: 2001-05-24
    body: ">You just wave the\nmouse wherever it happens to be on the screen.<\n\nWhat if I accidentaly wave a mouse and launch an\napplication? I wave my mouse cursor a lot :), especially when I browse the web."
    author: "Antialias"
  - subject: "Just try it :)"
    date: 2001-05-24
    body: "umm.. but do u wave ur mouse cursor in geometric shapes u define? i didn't think so :-)\n\nJason"
    author: "Jason Katz-Brown"
  - subject: "Re: Just try it :)"
    date: 2001-05-25
    body: "Are we going to draw complex geometric shapes to execute a simple commands? And how many geometric shapes you can remember and associate with commands?\nAnd can you precisely draw your geometric shapes?\nI daubt it. I've just compiled Kgesture and tried it, and I needed 5-6 times to draw a simple geometric shape before it was recognized by Kgesture. Waste of time. Shape looks like this\n|_|  .\nI admit that it would be very usefull to, for example minimize and maximize windows, but IIRC\nthere is no dcop call for these actions, or maybe I am wrong. My sister just called and told me that she had asked on #kde and developers said that there is no dcop call for minimize window."
    author: "Antialias"
  - subject: "Re: Gesture Recognition for KDE"
    date: 2001-05-24
    body: "Opera on windows support this, and all I can say is that I'm now addicted to it... I found myself trying the mouse mouves everywhere, and wondering why it is not working... Especially in Opera under Linux..."
    author: "Holstein"
  - subject: "Re: Gesture Recognition for KDE"
    date: 2001-05-24
    body: "Congratulations, you're one of the lucky people able to use a keyboard or have enough hand/eye coordination to be able to use a standard point and click interface.\n\nNow try thinking about it from the point of view of someone who cant use a keyboard or hasnt got the hand/eye coordination to click on tiny icons on a screen.\n\nYeah I know I should rise to trolls...."
    author: "Sparky"
  - subject: "Re: Gesture Recognition for KDE"
    date: 2001-05-24
    body: ">Now try thinking about it from the point of view of someone who cant use a keyboard or hasnt got the hand/eye coordination to click on tiny icons on a screen.<\n\nAnd that 'someone' has hand/eye coordination to draw circles or different shapes?\n\nBTW, icons don't need to be tiny :)"
    author: "Antialias"
  - subject: "Re: Gesture Recognition for KDE"
    date: 2001-05-25
    body: "Same for me... though I think such features make KDE more complete and I appreciate that functionality like this begins with linux (even if you tried a separate app, it might not integrate as well with the DE).\nBtw, I doubt \"fuzzy logic\" being used (I didn't take the time to look at the code, sorry)."
    author: "Henri"
  - subject: "Re: Gesture Recognition for KDE"
    date: 2001-05-24
    body: "EXCELLENT!\n\nwill i be able to live without it now? :o))))))))))\n\nthanks for this :o)))))\n\nemmanuel :o)"
    author: "emmanuel"
  - subject: "Re: Gesture Recognition for KDE"
    date: 2001-05-24
    body: "Great! I use WayV to launch applications, which doesn't depend on KDE, but it can't talk to apps that are already running except in limited ways, so KGesture is a great improvement on that.\n\nI hope that if/when Gnome gets gesture recognition it's something that can run simultaneously with this and not cause conflicts... although somehow I doubt that."
    author: "MaW"
  - subject: "Re: Gesture Recognition for KDE"
    date: 2001-05-24
    body: "Well, it seems to be calling dcop to do the actual actions, so as long as gnome has a shell-callable scripting interface, I don't see why you would need another app at all.\n\nJust use this one and/or extend it to control GNOME apps."
    author: "Roberto Alsina"
  - subject: "Re: Gesture Recognition for KDE"
    date: 2001-05-24
    body: "That'd be good. Maybe one day I'll figure out how to do it myself."
    author: "MaW"
  - subject: "Re: Gesture Recognition for KDE"
    date: 2001-05-24
    body: "Why not implement REAL gesture, capturing data from a Webcam ?"
    author: "Julio Alvarez"
  - subject: "Re: Gesture Recognition for KDE"
    date: 2001-05-25
    body: "I rememeber that somone tried this some time ago. They made a console app that controlled kpresenter by body movement. I'll look for it in the mail archives."
    author: "J\u00f8rgen Adam Holen"
  - subject: "Re: Gesture Recognition for KDE"
    date: 2001-05-25
    body: "http://lists.kde.org/?l=koffice&m=95229110102768&w=2"
    author: "J\u00f8rgen Adam Holen"
  - subject: "Re: Gesture Recognition for KDE"
    date: 2001-05-24
    body: "> . . . Using KDE's DCOP interface . . .\n\nDoes anyone know where I can find more information on DCOP calls.\nIdeally I would like to find a full list of possible DCOP calls for every KDE2 app.\n\neg.\n\nkwin\n-KWinInterface\n..setCurrentDesktop\n..killWindow\n..??\n\nkdesktop\n-??\n..??\n\netc."
    author: "Risto Treksler"
  - subject: "Re: Gesture Recognition for KDE"
    date: 2001-05-24
    body: "Have you tried using the graphical browser \"kdcop\" or the command-line browser \"dcop\"?.This only applies to running applications though."
    author: "Navindra Umanee"
  - subject: "Re: Gesture Recognition for KDE"
    date: 2001-05-25
    body: "Task oriented documentation would be much better.  For  example if I wanted to programatically move a window to a different desktop I would have had no idea how to actually do it.  kdcop may be useful for a full-on developer but is not really useful for end-user scripting of the environment, which should be one of the strengths of KDE."
    author: "raven667"
  - subject: "And we should be able to do it from python"
    date: 2001-05-26
    body: "from dcop import *\n\nblah blah blah"
    author: "AC"
  - subject: "An interesting mail on the topic."
    date: 2001-05-24
    body: "This is a mail from the author as reported by LinuxToday.  See the attachment for some interesting info."
    author: "KDE User"
  - subject: "Re: An interesting mail on the topic."
    date: 2001-05-24
    body: "i also would like to see more functionality of kde exported to dcop, especially things like konsole, ... i'd like to have a dcop interface, and i would enjoy a bigger konqueror dcop iface (kwin too ..)"
    author: "ik"
  - subject: "huge dcop interface. in konqueror"
    date: 2001-05-25
    body: "It turns out Konqueror has a huger interface than we thought!  Take a look at this mail from David Faure::\n\nhttp://lists.kde.org/?l=kde-core-devel&m=99073694025787&w=2\n\nWhoa..."
    author: "KDE User"
  - subject: "Re: huge dcop interface. in konqueror"
    date: 2001-05-25
    body: "cool, thanks\nit would be even cooler if we didnot need that magic number in it:\n~$ dcop kmix 'qt/unnamed76(KActionCollection, 0x8099214)/help_about_app' activate\n\nsince i guess it will be compile dependant ...\nnow we always need to grep for it ...\n\ngreetings,\nkervel"
    author: "ik"
  - subject: "Re: Gesture Recognition for KDE"
    date: 2001-05-24
    body: "Now that gesture interfaces finally seem to find their way into mainstream GUIs, I guess it's time to reconsider the related other interface method that [warning: bad pun] pops up every now and then in User Interface classes: Pie Menus [http://catalog.com/hopkins/piemenus/index.html]\n\nThere is already an implementation for GTK [http://atrey.karlin.mff.cuni.cz/~0rfelyus/GtkPieMenu/], and I wonder what it would take to integrate something like it in Qt/KDE as well, maybe as a widget theme.\n\nThe biggest problem used to be the size that pie menus took up, but that could now probably avoided by making them transparent."
    author: "Arnd Bergmann"
  - subject: "Re: Gesture Recognition for KDE"
    date: 2001-05-24
    body: "It is a cool idea !\nIt will better to have it in kcontrol !\n\nJP"
    author: "Martin"
  - subject: "Windows Has SenSiva"
    date: 2001-05-24
    body: "This is a great development, when i saw Sensiva, application I was impressed with it and wished that KDE should have had it. I expected that KDE might take three to five years to get there to have this technology; but to my amazement and surprise; KDE's Development is real great. I wonder What will KDE and its apps will do till KDE 3.0 ;)\n\nThanks a lot for your hard and nice work!"
    author: "Asif Ali Rizwaan"
  - subject: "Re: Windows Has SenSiva"
    date: 2001-05-24
    body: "Yeah. KDE's development is going very fast.\n\nBut i'm still waiting for someone (?) to implement that \"focus follows mind\" thing"
    author: "me"
  - subject: "Re: Windows Has SenSiva"
    date: 2001-05-25
    body: "Sloppy focus is close enough to focus follows mind, I think:)  Mice are totally useless without it.  If you get into the habit of following your reading with your mouse cursor, and have sloppy focus on, you've effectively got focus follows mind:)"
    author: "James"
  - subject: "Re: Windows Has SenSiva"
    date: 2003-01-09
    body: "Actually, using a gaze tracker this was already done (albeit as a research prototype) by - I believe it was IBM: The gaze tracker determines where on the screen you're looking and when you move your mouse it will teleport there and move. The cursor only teleports when it's not in your field of vision. This combined with focus on mouse comes pretty close...\nToo bad gaze trackers aren't exactly standard PC apparel."
    author: "Frans Flippo"
  - subject: "Re: Gesture Recognition for KDE"
    date: 2001-05-24
    body: "Cool, you did something microsoft would try.  I guess we have arrive."
    author: "ryan ware"
  - subject: "Graphic bug in KDE?"
    date: 2001-05-24
    body: "Look at the first screenshot, at the border around the selected icon (at left). Not that nice. I have the same problem in my KDE. The border around some selected items is ugly sometimes."
    author: "PE"
  - subject: "Re: Graphic bug in KDE?"
    date: 2001-05-25
    body: "This is *not* a KDE problem.  This problem is either a problem in X or just in the nv(NVidia) X driver.  Anyway AFAIK, it is fixed in the next X release.  If I can find a reference on this problem, I'll post it here."
    author: "Rob"
  - subject: "Re: Graphic bug in KDE?"
    date: 2001-05-25
    body: "http://dot.kde.org/988313536/988326287/988351061/988352473/\n\nLook at the bottom of this page!"
    author: "Rob"
  - subject: "Workaround"
    date: 2001-06-02
    body: "It happens with drivers other than nvidia's. Although I believe it is a X problem I have a workaround in the form of a patch to qt at http://students.fct.unl.pt/~cer09566/qt-2.3.0-dotted_workaround.diff (at least it works for me)."
    author: "Carlos Rodrigues"
  - subject: "Re: Gesture Recognition for KDE"
    date: 2001-05-25
    body: "Congradulations to the KDE developers. Even after i heavly pushed the kde vs. gnome poll on gnotices yesterday kde still has a 10 to 1 lead. Its my option that konqueror has been the killer app that pulled kde in the lead. ALso anti-aliased fonts and the kompany's applications hav'nt hurt either. Ok before all you flower power types get your bell bottoms ruffled. The community is united like a bunch of baseball fans are but i just enjoy rutting for the home team. So way to go guys your doing great. I think when koffice gets in better shape we'll really be looking good.\n\nCraig"
    author: "Craig"
  - subject: "Re: Gesture Recognition for KDE"
    date: 2001-05-25
    body: "Don't get too excited about that poll.  Past credible assessments place KDE and GNOME users head to head of the Unix desktop environments.  Biased poll?  Yep.  There's a reason why professional companies such as Gallup (http://www.gallup.com/) hire mathematicians, statiticians, and linguistics experts to produce a fair representation of the public.  And I can assure you that the 10:1 poll is hardly a statistical representation of all unix users."
    author: "una persona que usar kde y gnome"
  - subject: "Re: Gesture Recognition for KDE"
    date: 2001-05-25
    body: "I think the demiss of eazel and the stability and speed of konqueror and anti aliased fonts in kde have really panned out. I heavyly promoted the poll on gnotices and now its a headline story on newsforge and still kde leads gnome 10 to 1. I know its not a scientific poll but man thats a huge lead. Its not like the poll was a kde poll or anything. I pushed it hard yesterday on gnotices to make sure the gnome guys showed up.\n\nCraig"
    author: "Craig"
  - subject: "Re: Gesture Recognition for KDE"
    date: 2001-05-25
    body: "k... just don't mention what the \"gnome guys\" SAID on gnotices... they flamed u like everyone else! now, i know u are a good person, but ur comments about this poll are evil! You know its biased!\n\nJason"
    author: "Jason Katz-Brown"
  - subject: "Re: Gesture Recognition for KDE"
    date: 2001-05-25
    body: "Who could the poll be biased? It was on here and gnotices and on mandrake forum and newsforge? Come on why can't you see that Knonqueror has pulled kde way ahead.\n\nCraig"
    author: "Craig"
  - subject: "Enough already!"
    date: 2001-05-25
    body: "Did anyone see the poll at http://www.annoyingnewsposter.com??\n\nThe current votes show that Craig is in the lead by a huge margin.  It seems that everybody (KDE users, GNOME users and the general news-thirsty linux crowd) has agreed that Craig's posts are getting annoying..."
    author: "Iva DeNuff"
  - subject: "Re: Enough already!"
    date: 2001-05-25
    body: "YES! CRAIG BE QUIET!\n\n\n/me votes for craig.. thanks for that poll notice iva denuff.... WAIT! i don't NEED to be notified about polls on the dot! KAH!\n\nJason"
    author: "Jason Katz-Brown"
  - subject: "Re: Gesture Recognition for KDE"
    date: 2001-05-25
    body: "Pay no attention to this CRAIG.  He's been trolling gnotices for a few days.  The website that the poll was on has been around for less than a month and has about 25 registered users--I think Craig's probably a 'nom de plume' of the guy who runs that site, and it is clear he has been trying to increase traffic on it.  As a trained statistician, I can assure you that the results of that poll are inaccurate, and most likely not because of poor sampling techniques.  I would really like to get some accurate figures about total users of both systems, because it would be interesting to know how each have grown in the past few years. The truth is, its not a race against each other, and the competition is\nenhancing the usability of both desktops.  If both GNOME and KDE can get a 1 or 2 percent of the Windows users to switch over, this will increase the number of Linux Desktop users incredibly.\n\nPlus, Craig can't write a sentence without misspelling a word and making a grammatical error."
    author: "Concerned Citizen"
  - subject: "Re: Gesture Recognition for KDE"
    date: 2001-05-25
    body: "Well your right about my spelling and wrong about me being somehow connected with the site. I to would be very interested in a scientific sampling of who's using what. I promoted the poll on the gnotices site so as to make it fair. I wanted the poll to be as accurate as it could be so as to get to the truth. Another poll has popped on on http://www.warpedsystems.sk.ca which is another site frequented by mandrake users. It also has a similar result of 10 to 1 ratio of kde users to gnome users. I think theres something to there polls but i don't think there 100% accurate.\n\nCraig"
    author: "Craig"
  - subject: "Re: Gesture Recognition for KDE"
    date: 2001-05-26
    body: "Craig, Mandrake default desktop is KDE. Maybe you should try to ask Debian users."
    author: "Antialias"
  - subject: "\"I just enjoy rutting for the home team\""
    date: 2001-05-26
    body: "I'm assuming you're a non-native English speaker and don't realize how funny that sounds ...."
    author: "AC"
  - subject: "About \"Craig\""
    date: 2001-05-26
    body: "Craig, you are a stupid fuck! You're not as smart as you pretend to be.\nSo I advise you to shut your face. Forgive me my rudeness, but half of the comments are\nposted by Craig and there's not much interesting in it.\nAbout Gnome Vs. KDE... There's no such thing. KDE rules over Gnome, Gnome rules over KDE.\nThe poll was most definately faked. The results are impossible.\nYes, it's a fact that KDE has more users, but that doesn't mean that this poll was unbiased.\nI would estimate a number of 60% KDE users Vs 40% Gnome users.\nBoth window managers rock. KDE is like a female, nice looking, smooth... Gnome is male, more robust look, straight to the point."
    author: "iscarioth"
  - subject: "Re: About \"Craig\""
    date: 2001-05-26
    body: "Are you gay? You sound like it the way you describe gnome.\n\nCraig"
    author: "Craig"
  - subject: "Re: About \"Craig\""
    date: 2001-05-26
    body: "You're really a pissy li'll man aren't you?\nYou got nothing better to do?\nPathetic bitch.\nWait here's a poll:\nWHO WANTS CRAIG BANNED FROM THIS FORUM?\nI vote yes and you will soon notice how popular you are.\nAnd fuck yourself in the ass with a broken bottle\n."
    author: "iscarioth"
  - subject: "Re: About \"Craig\""
    date: 2001-05-26
    body: "craig:\n     - reports figures that are probably biased,\n        and does that twice. okay ,thats a (double)\n        mistake, i think he understood that already.\n      - stayed polite even after being flamed. okay\n        he didnot stay polite now, but neither would\n        i.\n\nyou:\n\n     - use offensive language.\n     - attack craig personally with NO reason.\n        its not because someone makes mistakes\n        he is a 'stupid fuck'\n     - also report figures just out of your thumb,\n        not even coming from a poll.\n\n\nactually, your post (and the ban-craig post) on this already closed subject\nwas totally useless.\n\nplease keep this forum 'sympa', please don't make this forum slashdot II, and please show people\ntheir mistakes in a polite way. everyone will enjoy\nthat.\n\nthank you very much in advance\nkervel"
    author: "ik"
  - subject: "Re: About \"Craig\""
    date: 2001-05-26
    body: "oh ... ignore my post. i wish there was a cancel feature here :)"
    author: "ik"
  - subject: "Re: Gesture Recognition for KDE"
    date: 2001-05-26
    body: "Apparantly, Craig figured out how to Spam PHP-NUKE polls. \nThis slashdot post: http://slashdot.org/comments.pl?sid=01/05/26/126251&cid=39\ndocuments his trolling.  He is giving KDE users a bad name."
    author: "Craig Sleuth"
  - subject: "Re: Gesture Recognition for KDE"
    date: 2001-05-27
    body: "The owner of www.pclinuxonline.com just emailed me and after reviewing the logs confermed all kde votes were real. However he brought the site down shortly after someone ran a script for gnome/enlightenment votes. Don't believe me ask him.\n\nCraig"
    author: "Craig"
  - subject: "KDE/Linux and the medias"
    date: 2001-05-30
    body: "Yes, it's true.\n\nI desagree with the Craig methods. However I don't think he is a cheater, as someones said without real proofs.\n\n=====\n\nThere is another poll in http://freeos.com/\nNow KDE 67%, Gnome 15 % others 18%.\n\nEven in http://www.linuxmag-france.org a \"GNU/Linux & Hurd Magazine\", you have Gnome 46%, KDE 36%, others 22% \n\n=====\n\nI think that KDE is many more used that it is said in the Linux medias. Around 55-70%, and Gnome around 12-25 %.\n\nThe Linux medias want to be neutral. By being neutral, they distort the reality, they give equality between Gnome and KDE. But users choose and a big majority  find that the better tool for their use is KDE.\n\nI think that OS =  kernel + desktop. So there are three OS in Linux : GNU/Linux (Gnome), KDE/Linux and Light Linux (Blackbox, Windowmaker, command line and so). Nowadays many medias speak about GNU/Linux, no one about KDE/Linux, however KDE/Linux is 3  x more used than GNU/Linux...\nVery strange !\n\nI don't think it is very important. Popularity is often snobed. The most important it the youth and the strenght of the KDE team, and the good logistic around, translations, communications and so. It's only a beginning, and great things are going on. Medias are often in late and someday they will discover some evidences...\n\nExcuse me, I am out of topic, but I think it is interesting to - sometimes - have some distance. \n\nIn conclusion, I think that KDE is already more popular that it is said and that one day it will be interesting to use the word KDE/Linux, as (and not as...) GNU/Linux is used. Because there are several Linux realities. GNU/Linux is one of them, but it is not ours.\n\nI also think that choosing a word like KDE/Linux (or Linux/KDE or KLinux) gives promotion for KDE. Words are important in our life...\n\nAnd please, consider that recognition of differences doesn't mean intolerance. I hope that the several Linux realities will live in good terms, that any user may mix them and easily change for the better environment he feels. The first quality of Linux is to give the freedom of choice."
    author: "Alain"
  - subject: "Re: KDE/Linux and the medias"
    date: 2001-05-30
    body: "Well said"
    author: "Craig"
  - subject: "Re: KDE/Linux and the medias"
    date: 2001-06-01
    body: "Hmmm....\n\nYou made up imaginary things (gnu/linux, kde/linux, klinux, gnome/linux) and the such... then you talked and argued with yourself about these new imaginary things.\n\nVery nice. We need people with active imaginations in opensource."
    author: "sam kennedy"
  - subject: "Re: KDE/Linux and the medias"
    date: 2001-06-02
    body: "Hmmm... Perhaps GNU/Linux is a new imaginary thing... However many guys are speaking about it, and not only by talking for themselves.  \n\nThis GNU/Linux has some reality. Linux-KDE too, and for more users. It has now a reality a little similar to the Dos-Win 3.0/3.1 10 years ago (with modern features, of course). \n\nGNU/Linux is recognized by the medias, not Linux-KDE... And we can see some irrealistic things, like some journalists saying that the Linux desktop is dead because Eazel is dead... What active imagination for such ignoring that beside GNU/Linux there is another project, a sort of shadow project without clear name, with a too narrow name ! The KDE desktop is already an essential component of our system and it is growing with more configuration tools. It is many more than a desktop...\n\nIn the facts, Linux-KDE is an alternative to GNU/Linux. Not still in the words... I hope that words will join the reality... (even if GNU/Linux exists without Qt tools, and KDE needs some GNU tools)\n\nHowever I hope that no RMS will come to promote it ;-)... I hope that someday the KDE Team (at first) and/or the promotion team and/or the KDE League will go in this direction..."
    author: "Alain"
  - subject: "Re: KDE/Linux and the medias"
    date: 2001-06-03
    body: "I see your point. So, maybe it should be called GNU/KDE/Linux. But, really, I think we should just simplify it by breaking it down to it's most unique denominator, Linux. It should just be called Linux. GNU ware isn't specific to linux, KDE ware isn't specific to Linux. This operating system is linux. There are many things that work to complete it, however... but, really, the system is just built on top of linux, and always has been built around it. Aggregating each of the names of new system components into the name may be fun, but it really complicates life and creates the false impression that linux is forking."
    author: "Sam Kennedy"
  - subject: "Re: Gesture Recognition for KDE"
    date: 2001-05-26
    body: "So how does it work ? Can I move my arms and interact with computer? I need more equipment for that."
    author: "Marius Andreiana"
  - subject: "Re: Gesture Recognition for KDE"
    date: 2001-05-26
    body: "That reminds me of the story of the guy who tried to fax a document from his computer by printing it out, holding it up to the screen, and pressing the 'quick-fax' key on his keyboard.\n\nNaa, its using your mouse. You move your cursor in special patterns that you set and these trigger dcop signals."
    author: "Carbon"
  - subject: "Re: Gesture Recognition for KDE"
    date: 2001-05-26
    body: "Oh yes, you can kick your computer and it will power down ;=). And when you at graphical login you just have to smile and KDE will be chosen as your default desktop environment :)"
    author: "Antialias"
  - subject: "Like Emacs \"stroke-mode\""
    date: 2001-05-26
    body: "very useful actually.\n\nWhy is voice control discontinued?"
    author: "AC"
  - subject: "So what happened to VOICE CONTROL!???"
    date: 2001-05-26
    body: "It's likely more important"
    author: "AC"
  - subject: "Re: So what happened to VOICE CONTROL!???"
    date: 2001-05-26
    body: "well, i tried a few voice control programs under windows a few years ago, i really liked it from the start, i bought a new mike ... ,but after a while i\n began to dislike it,computer made too much mistakes, and i always had to speak/shout in front of my computer, first it was funny, but later it became \nannoying for me and for the people in my room,\nit was more a .. toy, and look at windows, tons of programs are available, nobody uses them, soundblaster bundled voice assist with one of their cards, but stopped doing so after a while. so i think:\n-when you want descent voice control, you need\n huge amounts of\n programming/linguistic experience  -- not even the commercial world can bring them up apparently --, (human)\n resources  for a (maybe) boring task. i never saw  the perfect voice recognition in commercial world,\n i doubt it will be ever usefull in opensource world.\n- even if you have 'the perfect voice control',  everybody will probably say 'great' and use it a few times, but probably after a while, people will forget\nabout it."
    author: "ik"
  - subject: "Re: Gesture Recognition for KDE"
    date: 2001-06-02
    body: "Great Idea! Will it be possible to navigate in Konqueror (like \"go back in history\" or something) like in opera for windows? Its a really cool feature in opera and I miss it a lot in Konqueror"
    author: "romulus23"
---
<a href="mailto:mpilone@slac.com">Mike Pilone</a> has stepped up to the challenge of implementing <a href="http://www.slac.com/~mpilone/projects/">gesture recognition for KDE</a>. <i>"KGesture uses libstroke to recognize definable gestures, then run an associated command. Using KDE's DCOP interface, KGesture can interact with applications already running, or launch new applications."</i>  You might be forgiven if, like me,  you first thought this was a joke.  KGesture works as advertised, and is almost as fun as the now discontinued <a href="http://www.kiecza.de/daniel/linux/index.html">KVoiceControl</a>, but it does need a little more fuzzy logic before it becomes practical enough.  I did manage to get it to work for simpler gestures -- I can draw an L-shape on my desktop and a dot.kde.org window will pop up.  However, for more complex gestures such as a circle, it takes a little practice and patience to get right. With time, your help and feedback, KGesture is bound to improve. Download it <a href="http://www.slac.com/~mpilone/projects/kgesture//kgesture-0.3.tar.gz">here</a> or view the screenshots (<a href="http://www.slac.com/~mpilone/projects/kgesture/images/configdialog1.png">1</a>, <a href="http://www.slac.com/~mpilone/projects/kgesture/images/configdialog2.png">2</a>, <a href="http://www.slac.com/~mpilone/projects/kgesture/images/gestureeditor1.png">3</a>, <a href="http://www.slac.com/~mpilone/projects/kgesture/images/gestureeditor2.png">4</a>, <a href="http://www.slac.com/~mpilone/projects/kgesture/images/gestureeditor3.png">5</a>, <a href="http://www.slac.com/~mpilone/projects/kgesture/images/gestureeditor4.png">6</a>, <a href="http://www.slac.com/~mpilone/projects/kgesture/images/gestureeditor5.png">7</a>, <a href="http://www.slac.com/~mpilone/projects/kgesture/images/gestureeditor6.png">8</a>, <a href="http://www.slac.com/~mpilone/projects/kgesture/images/gesturerecorder.png">9</a>).
<!--break-->
