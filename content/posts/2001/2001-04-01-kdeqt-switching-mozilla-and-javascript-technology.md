---
title: "KDE/Qt Switching to Mozilla and JavaScript Technology"
date:    2001-04-01
authors:
  - "Dre"
slug:    kdeqt-switching-mozilla-and-javascript-technology
comments:
  - subject: "Re: KDE/Qt Switching to Mozilla and JavaScript Technology"
    date: 2001-04-01
    body: "This sure fooled me for a second :)"
    author: "gallium"
  - subject: "Re: KDE/Qt Switching to Mozilla and JavaScript Technology"
    date: 2001-04-01
    body: "ha - fooled ya :)"
    author: "gallium"
  - subject: "Re: KDE/Qt Switching to Mozilla and JavaScript Technology"
    date: 2001-04-01
    body: "This just in:  Corel performs hostile takeover on Microsoft.  Microsoft will be moving to Ottawa, and Ballmer is fired.  Or not :-)"
    author: "Steve Hunt"
  - subject: "Re: KDE/Qt Switching to Mozilla and JavaScript Technology"
    date: 2001-04-01
    body: "Damn, how can they do something like this? This is plainly stu... Oh wait. What's today's date again?  :)"
    author: "Achim"
  - subject: "Re: KDE/Qt Switching to Mozilla and JavaScript Technology"
    date: 2001-04-01
    body: "This is great!\nImagine getting the incredible speed and efficiency of Mozilla into KDE ;-)"
    author: "konqi"
  - subject: "Re: KDE/Qt Switching to Mozilla and JavaScript Technology"
    date: 2001-04-01
    body: "Don't forget the Javascript! I for one think this is a great idea! This will lower the bar to the level that KDE can be written by anybody... maybe even with Frontpage. Although there is now the possibility that KDE may be difficult to use with legacy konqueror and IE.\n\n:-O"
    author: "Eric Laffoon"
  - subject: "Re: KDE/Qt Switching to Mozilla and JavaScript Technology"
    date: 2001-04-01
    body: "Boy am I glad this was just a joke. For a while there I though Trolltech and KDE were about to do something really really stupid."
    author: "Erik Engheim"
  - subject: "Re: KDE/Qt Switching to Mozilla and JavaScript Technology"
    date: 2001-04-01
    body: "Nah, you could do better than this. I have a good April fools joke for you: Unconfirmed sources claim that they actually have seen the KDE core developers sleep."
    author: "Matt"
  - subject: "Actually.."
    date: 2001-04-01
    body: "Good April's Fool!\n<p>\nBut..\n<br>\nUsing a widget/theme engine which can import CSS would <i>not</i> be a bad idea.\n<p>\nIt would not require a full implementation, but basic CSS would indeed make it easier to create a theme <i>\"with the simple editing of a stylesheet\"</i>.\n<p>\nTo keep performance, the engine could read the CSS defintions once, then convert it to native format for actual use. If KDirWatch notices a change in any of the theme files, there will be a rebuild to native format.\n<p>\nThat way we can have both ease-of-use in creating themes (HTML/CSS is probably more well-known than current theme configuration) and speed, not requiring an actual run-time parser.\n"
    author: "Rob kaper"
  - subject: "Re: KDE/Qt Switching to Mozilla and JavaScript Technology"
    date: 2001-04-01
    body: "Ups, you nearly got me ;-)\nNext year we should write \"KDE will switch to ActiveX\""
    author: "Christoph Cullmann"
  - subject: "Re: KDE/Qt Switching to Mozilla and JavaScript Technology"
    date: 2001-04-01
    body: "It 'd fool many more people if\nKDE team annouced Microsoft licensed\nthe KPart technology for its .net\nframework..."
    author: "Christian"
  - subject: "Re: KDE/Qt Switching to Mozilla and JavaScript Technology"
    date: 2001-04-02
    body: "Like a 04/01 /. posting \"I think Microsoft is stealing GPL'd code\"\n\n:)"
    author: "Michael Jarrett"
  - subject: "Re: KDE/Qt Switching to Mozilla and JavaScript Tec"
    date: 2001-04-03
    body: "An April Fools would have to claim that Microsoft is NOT stealing Open Source code...  :)\n\nWe know for a fact that Microsoft has been raping *BSD for years... But then again, its not like its illegal, (the advertising clause has long been dead)."
    author: "Greg Brubaker"
  - subject: "Re: KDE/Qt Switching to Mozilla and JavaScript Technology"
    date: 2001-04-01
    body: "Recently, Microsoft bought out AOL Time Warner, Netscape's parent company, for one cent.  JavaScript 2.0 will integrate ActiveX.  Meanwhile, the Konqueror will be renamed to Microsoft Netscape Explorer."
    author: "anonymous2point0"
  - subject: "Re: KDE/Qt Switching to Mozilla and JavaScript Technology"
    date: 2001-04-01
    body: "Damn :-)\nI REALLY started swetting there.\n\"Oh no, all my programs have to be rewritten...\"\nThat was a REALLY good one!!"
    author: "Jarl E. Gjessing"
  - subject: "Re: KDE/Qt Switching to Mozilla and JavaScript Technology"
    date: 2001-04-01
    body: "Very funny! I read the whole thing with a big frown but kept thinking \"Well, they *do* seem to know what they're doing... :)"
    author: "EKH"
  - subject: "Re: KDE/Qt Switching to Mozilla and JavaScript Tec"
    date: 2001-04-01
    body: "Indention is in deed very important ;-) ROTFL"
    author: "AC"
  - subject: "Re: KDE/Qt Switching to Mozilla and JavaScript Technology"
    date: 2001-04-01
    body: "If this were posted yesterday, I would be in a bit of a panic."
    author: "dingodonkey"
  - subject: "Re: KDE/Qt Switching to Mozilla and JavaScript Technology"
    date: 2001-04-01
    body: "You programmers think to difficult, as far as I know Intel is developing a ne wgeneration microprocessors that understand plain american so no need anymore for assemblers, compilers and all kinds of esoteric programming languages, it will have an audiointerface and you just tell your computer what you want it to draw on the screen. I heard they will name it after a well-known celebrity from Redmond , gatesbillium, and microsoft will be the only company that will be allowed to sell systems based on that processor, because they are allready familiar with plain american as the only way to do programming (and business)."
    author: "wb"
  - subject: "Re: KDE/Qt Switching to Mozilla and JavaScript Technology"
    date: 2001-04-01
    body: "<a href=\"http://derkarl.org/why_to_tabs.html\">derkarl.org/why_to_tabs.html</a>\n<p>\nfrom the lih lord himself!\n"
    author: "Jason Katz-Brown"
  - subject: "Re: KDE/Qt Switching to Mozilla and JavaScript Technology"
    date: 2001-04-01
    body: "<A HRef=\"http://derkarl.org/oxymorons.phtml\">The Oxymoron Server is fun too!</A>\n"
    author: "reihal"
  - subject: "Re: KDE/Qt Switching to Mozilla and JavaScript Technology"
    date: 2001-04-01
    body: "That used to work?"
    author: "reihal"
  - subject: "Re: KDE/Qt Switching to Mozilla and JavaScript Technology"
    date: 2001-04-01
    body: "its speed and robustness? *laugh,."
    author: "Rickard"
  - subject: "Re: KDE/Qt Switching to Mozilla and JavaScript Tec"
    date: 2001-04-01
    body: "yeah...\n\nThat's probably the best argument and gave me a big laugh ;)\n\nIndeed, as we all know, Mozilla is blazingly fast and extremely stable.\n\nCome on, even on a 1 GHz machine you can watch how this extremely fast rendering engine draws every single element of a medium complex dialog box ;)"
    author: "Singularity"
  - subject: "Re: KDE/Qt Switching to Mozilla and JavaScript Tec"
    date: 2001-04-02
    body: "really? From my experience, Mozilla is pretty fast on my PII 350, and Konquerer isn't any faster and is pretty unstable."
    author: "danny"
  - subject: "Re: KDE/Qt Switching to Mozilla and JavaScript Technology"
    date: 2001-04-01
    body: "Switching to JavaScript and HTML/XML will also close several security holes.  The upcoming JavaScript 2.0 technology will even allow three dimensional images to pop out of your monitor!"
    author: "anonymous2point0"
  - subject: "Re: KDE/Qt Switching to Mozilla and JavaScript Tec"
    date: 2001-04-01
    body: "*lol*\n\ngood one.\n\nSure, I would rather believe a headline somewhat like \"MS about to OpenSource the Win2K codebase\" ;)"
    author: "Singularity"
  - subject: "Re: KDE/Qt Switching to Mozilla and JavaScript Tec"
    date: 2001-04-02
    body: "Better yet : MS leaks root password to all core developer workstations and interdev servers, and installs new web-controlled raid arrays for periodic backups. Quote : \"These work so well, we don't even need tape backups!\" :-)"
    author: "Carbon"
  - subject: "Re: KDE/Qt Switching to Mozilla and JavaScript Technology"
    date: 2001-04-01
    body: "Sorry, this can't be an april fool. It was posted after 12 o'clock. Or the joke is on them."
    author: "Mark Hillary"
  - subject: "Re: KDE/Qt Switching to Mozilla and JavaScript Technology"
    date: 2001-04-02
    body: "It can't be 12:12 AM on April 1st?  Methinks you go to bed too early.  :)"
    author: "SlnkiesRKewl"
  - subject: "Re: KDE/Qt Switching to Mozilla and JavaScript Technology"
    date: 2001-04-01
    body: "Sure glad this was a joke. I could read the headline for a while, but the Dot wouldn't load - it made me so nervous I had to write the KDE-General list to see if anyone knew if this was an April Fool's Day joke or not...\n\n  Whew...\n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "Re: KDE/Qt Switching to Mozilla and JavaScript Technology"
    date: 2001-04-02
    body: "Yeah! A April fool!\n\nJavascript or Java with Mozilla/Netscape is the slowest thing I have never seen!"
    author: "Debian"
  - subject: "Re: KDE/Qt Switching to Mozilla and JavaScript Technology"
    date: 2001-04-03
    body: "Except, of course, Javascript or Java with Konqueror!"
    author: "someone"
  - subject: "Wait a sec..."
    date: 2001-04-02
    body: "I get how the Javascript/XUL stuff would be just plain ridiculous, and hence appropriate for an April Fool's joke, but what about the Mozilla layout engine?  I prefer it to konqueror's any day (it just works better on more sites, period).\n<br><Br>\nStability and speed are no longer issues with mozilla, and I would not mind at least having the _option_ to use Mozilla's layout engine for web surfing in konqueror (or other K* components).\n<br><Br>\nQt support for mozilla (e.g. using Qt for widget drawing instead of GDK) is in the works (I know, I know, this seems like its NEVER going to arrive) but check out bugs <a href=\"http://bugzilla.mozilla.org/show_bug.cgi?id=70511\">70511</a> and <a href=\"http://bugzilla.mozilla.org/show_bug.cgi?id=70509\">70509</a>.  They include patches (which I have not had time to try yet) but I look forward to them anyway.\n"
    author: "dimator"
  - subject: "Re: KDE/Qt Switching to Mozilla and JavaScript Technology"
    date: 2001-04-02
    body: "Ouch! I almost cried! It truely took some time!\n\nbrrrrrr...\n\nSomeone should crash all news sites at april 1st :)"
    author: "Thomas Ritter"
  - subject: "dot.kde.org will be sued"
    date: 2001-04-02
    body: "I have contacted my lawer to sue dot.kde.org for this joke, I've almost got to the hospital!!!!!!!!!"
    author: "ZMichael"
  - subject: "Re: dot.kde.org will be sued"
    date: 2001-04-09
    body: "Send me some money!!!!!!!!!!!!!!"
    author: "Jimmy"
  - subject: "Eh..."
    date: 2001-04-02
    body: "Is this really just a joke?"
    author: "Kirby"
  - subject: "You should post this at Slashdot!"
    date: 2001-04-02
    body: "Then those stupid trolls will finally shuts their mouths about Mozilla being bloated while they don't even know how to use top."
    author: "TrollKiller"
  - subject: "Give me QT Mozilla!"
    date: 2001-04-02
    body: "Even though I choose KDE as my primary desktop, I still launch Mozilla for my web browsing needs. Why? Because Mozilla has much better Javascript support (which will actually benefit Konquerer if this article is true), faster GIF animation rendering, faster DOM implementation, more stable, and most important of all, it renders pages correctly.\n\nI would really like to see Konqerer having the option to use Gecko as the rendering engine. The GNOME camp is doing this with their new file manager, and I would hate it if KDE's HTML rendering engine couldn't keep up with them."
    author: "danny"
  - subject: "Re: Give me QT Mozilla!"
    date: 2001-04-03
    body: "Do you realize how long it's taken the Mozilla programmers to get their codebase to the point that it's at? Do you realize that the KDE developers (mostly Lars Knoll) have managed to bring out a browser with almost the same feature set, in only a few short months?\n<br><br>\nI agree that Mozilla has better JavaScript support for the moment, and *maybe* it has a better DOM implementation, but these issues are under heavy development with Konqueror. I doubt that Mozilla is more stable than Konqueror (it hasn't been for me, anyway) and if you think that Mozilla renders pages more correctly than Konqueror then you obviously haven't looked at KDE.org much. With regards to your comment about animated GIF support - all animated GIFs should be shot on sight anyway.\n<br><br>\nAs for using Gecko instead of KHTML, have you checked out XParts yet? I doubt it. \n<br><br>\n<a href=\"http://trolls.troll.no/~lars/xparts/\">http://trolls.troll.no/~lars/xparts/</a>\n<br><br>\nMaybe you could do some research next time?\n"
    author: "Chris Lee"
  - subject: "Re: Give me QT Mozilla!"
    date: 2001-04-03
    body: "\"Do you realize that the KDE developers (mostly Lars Knoll) have managed to bring out a browser with almost the same feature set, in only a few short months?\"\n\nTo claim that Mozilla & Konquerer have almost the same feature set is narrow-minded. I'm just gonna point out the most obvious example: the Mozilla technology is built around the idea of cross-platform & portability, while Konquerer is only built with KDE in mind. That's why Mozilla has all those XUL, XPCOM, etc., which critics call \"junk\". But hey, I keep a copy of Windows on my computer for things that Linux doesn't do, and guess which browser I use on Windows? It's not Konquerer for sure.\n\n\"if you think that Mozilla renders pages more correctly than Konqueror then you obviously haven't looked at KDE.org much.\"\n\nI'm not sure what you're referring to. Mozilla has always rendered KDE.org fine. It even renders the web designer's mistake where the \"go\" button (next to the search field) is rendered as white text, which is very hard to read (unless you also specify a background for the button). Maybe the web designer should have checked the page with Mozilla instead. Konquerer didn't render the text white as specified, and it couldn't do the hovering effect until KDE 2.1.\n\n\"all animated GIFs should be shot on sight anyway.\"\n\nYou would say that, but what are you gonna do? =) A browser without good animated GIF support seems ridiculous by today's standard. Actually I take back what I said about animated GIF. Visit \"http://www.cts.com.tw/\". I originally thought animated GIF was slowing down Konquerer, but it seems like turning off image loading doesn't help either.\n\n\"As for using Gecko instead of KHTML, have you checked out XParts yet?\"\n\nAh, I remember that. I saw the kdebingds-kmozilla RPM package in KDE 2.1 but couldn't find much information on it. Or maybe I just haven't looked hard enough."
    author: "danny"
  - subject: "Re: Give me QT Mozilla!"
    date: 2001-04-03
    body: "\"It even renders the web designer's mistake where the \"go\" button (next to the search field) is rendered as white text\"\n\nThat sounds awkard. Should be \"the 'go' button is specified to have white foreground\""
    author: "danny"
  - subject: "Re: Give me QT Mozilla!"
    date: 2001-04-03
    body: "Corel has already submitted Qt Mozilla to mozilla.org.  Check bugzilla for details."
    author: "KDE User"
  - subject: "Re: KDE/Qt Switching to Mozilla and JavaScript Technology"
    date: 2001-04-03
    body: "YOU ASSHOLES!!!!!\nplease, i'm not young. i (and my heart) don't find this funny.\n\nps: i almost hit the <switch to gnome> button :)"
    author: "ac"
  - subject: "Re: KDE/Qt Switching to Mozilla and JavaScript Technology"
    date: 2001-04-03
    body: "what would use for web browsing then? :)\n\nbtw, the debian was worst, saying that the new dpl died. not funny."
    author: "Evandro"
  - subject: "Re: KDE/Qt Switching to Mozilla and JavaScript Technology"
    date: 2001-04-03
    body: "Gotta agree on that tasteless debian joke, especially after reading their public statement from the very next day:<br>\n<a href=\"http://lwn.net/daily/deb-loss.php3\">http://lwn.net/daily/deb-loss.php3</a>\n"
    author: "nap"
  - subject: "Re: KDE/Qt Switching to Mozilla and JavaScript Tec"
    date: 2001-04-03
    body: "Although i belive the above was an innocent joke, i think that it is a good idea to borrow some from *zilla projects into K* for it could lead to some realy good and impressive results."
    author: "SHiFT"
  - subject: "Maybe it'd be good..."
    date: 2001-04-03
    body: "Maybe it would not be that bad of an idea.  I actualy read the article and was happy...\n\nOf course, they'd need to make Mozilla run 10 times faster... :)"
    author: "Greg Brubaker"
  - subject: "Re: KDE/Qt Switching to Mozilla and JavaScript Technology"
    date: 2001-04-04
    body: "Man! You really managed to scare me... After reading the caption I opened gnome.org in another konqueror window to start to switch...."
    author: "Diedrich Vorberg"
  - subject: "Re: KDE/Qt Switching to Mozilla and JavaScript Technology"
    date: 2001-04-05
    body: "HA, using a interpreted toolkit and C++ underneath would be almost as bad as python and pyQt."
    author: "robert"
---
According to internal email correspondence, <A HREF="http://www.trolltech.com/">TrollTech</A>, makers of the Qt widget set on which KDE is based, has decided to use <a href="http://www.mozilla.org/js/">JavaScript</a> and <A HREF="http://www.mozilla.org/">Mozilla</A> technology as core components in the next versions of their products.   The KDE core team appears quite enthusiastic about this novel development.  In one of the emails, Matthias Ettrich, KDE founder, explained:  <EM>"Now we're pushing the envelope, making KDE more flexible and themable than ever before.  By replacing C++ with JavaScript and HTML/XML where possible, KDE will become easier to program and to theme, sometimes with the simple editing of a stylesheet!"</EM>.  Most KDE developers agreed. A draft press release is included below. 
<!--break-->
<P>&nbsp;</P>
OSLO - The <A HREF="http://www.kde.org/">K Desktop Environment</A> (KDE), in conjunction with <A HREF="http://www.trolltech.com/">TrollTech</A>, makers of the Qt Cross-Platform development kit, announced that Qt will be switching to JavaScript and <A HREF="http://www.mozilla.org/">Mozilla</A> technology in the next versions of their products.  The change will affect all Qt-based products, including KDE.
</P>
<P>
"It's well known that the K stands for Kuaint," commented KDE project founder Matthias Ettrich in a private email circulated to the core KDE developers. "Now we're pushing the envelope, making KDE more flexible and themable than ever before.  By replacing C++ with JavaScript and HTML/XML where possible, KDE will become easier to program and to theme, sometimes with the simple editing of a stylesheet!  This is the natural conclusion of previous XML User Interface efforts."
</P>
<P>
Almost all developers agreed to replace QPainter and KHTML, key components of the two systems, with Gecko, the Mozilla rendering system.  "The technology", explained Haavard Nord, TrollTech CEO, "has been made possible by our long-awaited switch of QPainter to Gecko technology, which will appear in the next major Qt release (Qt 4.x).  Work on the Qt-3.x branch will stop shortly in favor of Qt 4.x.  We wish the best of luck to KDE, and hope KDE 3.0 will be a great success when it is released with this new technology."
</P>
<P>
Roger Lawrence, Mozilla JavaScript maintainer and <A HREF="http://www.netscape.com/">Netscape</A> employee, noted that the increased usage of JavaScript in Qt and KDE will be a great boon to its speed and robustness.
</P>
<P>
Following Matthias Ettrich's brief announcement, a long series of posts appeared from a variety of developers, almost all supporting the plan.  Discussions are continuing regarding the continuation of
the KDE 2 branch, including suggestions that Waldo Bastian work on a new KDE release schedule which drops any further KDE 2.x releases in favor of an immediate jump to the new technologies.
</P>
<P>
<A HREF="http://perso.mandrakesoft.com/~david/">David Faure</A> argued in favor of branching immediately, and just releasing bug fixes to the 2.1 branch.
</P>
<P>
Charles Samuels, multimedia director for KDE, remarked "I think it's a great idea, as long as they make sure to indent with tabs!  They'd better, or I'll claim my vengeance!"  Samuels was sedated by his nurse at this point.
</P>
<P>
Simon Hausmann, KParts and Konqueror developer, agreed with Charles Samuels, except on the indentation.
</P>
<P>
Waldo Bastian, known KDE curmudgeon, was unavailable for comment.  When we asked "Where's Waldo?" on the kde-core-devel
mailing lists, no response was received, save for a terse "ADMIN: Literature Discussion belongs elsewhere."
</P>
<p>
<b>Update: 04/02 10:50 PM</b> by <a href="mailto:navindra@kde.org">N</a>:  Yes, this is an April Fool's joke.  It was so good, many are still falling for it! Meanwhile, those of you waiting for some real news might want to check out the latest <a href="http://apps.kde.com/nf/1/latest">KDE apps</a>.  I hear new <a href="http://apps.kde.com/nf/1/info/vid/2916">Opera</a> and <a href="http://apps.kde.com/nf/1/info/vid/2918">Aethera</a> betas are out.
</p>