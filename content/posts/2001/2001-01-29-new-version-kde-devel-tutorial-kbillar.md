---
title: "New Version of KDE Devel Tutorial + KBillar"
date:    2001-01-29
authors:
  - "uposter"
slug:    new-version-kde-devel-tutorial-kbillar
comments:
  - subject: "Nice tables, but where are the holes?"
    date: 2001-01-29
    body: "Thank you for the nice tutorial!"
    author: "ac"
  - subject: "Re: Nice tables, but where are the holes?"
    date: 2001-01-30
    body: "Billard tables don't have holes. At least, they\nare not supposed to. You are confusing them with\npool billard tables. ;-)\n\nBTW, good job, Antonio!"
    author: "Uwe"
  - subject: "Re: New Version of KDE Devel Tutorial + KBillar"
    date: 2001-01-29
    body: "What I want to know is when the KDE2 version of\nkpager is going to be as functional as the KDE1\nversion was. I use(d) it alot for managing my\nwindows layout, moving windows, etc. The current\npager in KDE2.0 just sucks by comparison."
    author: "GeekBoy"
  - subject: "Re: New Version of KDE Devel Tutorial + KBillar"
    date: 2001-01-29
    body: "What do you mean not as functional? In KDE-2.0 you have to drag the windows with the middle mouse button, not the RMB. In KDE-2.1 CVS the RMB \"works\" again.\n\nWhat I miss tho is setting window border-style. No, I dont mean \"KDE HiColor Styel\" and such, I mean border-only, no border, etc... And no - krun(or was it kstart?) is not good enough. In KDE-1 you could configure this for windows with sertain titles, xclass etc.. Why isn't it implemented in KWin?\n\n--\nAndreas Joseph Krogh <andreak@nettverk.no>"
    author: "Andreas Joseph Krogh"
  - subject: "OT: KDE2.1beta2"
    date: 2001-01-30
    body: "According to the release schedule, the second beta was supposed to be announced yesterday. I know the binaries are already on the loose, so where's the glitch?"
    author: "ac"
  - subject: "Re: New Version of KDE Devel Tutorial + KBillar"
    date: 2001-01-30
    body: "Anto\u00f1ito es m\u00e1s \"apa\u00f1ao que na\".\nSe nota que estudia (o ha estudiado) matem\u00e1ticas."
    author: "Manuel R\u00f3m\u00e1n"
  - subject: "Re: New Version of KDE Devel Tutorial + KBillar"
    date: 2001-01-30
    body: "Who has a link to this tutorial?"
    author: "Sangohn Christian"
  - subject: "Re: New Version of KDE Devel Tutorial + KBillar"
    date: 2001-01-31
    body: "I downloaded and compiled kbillar last night.  It's nice.  By the way it doesn't need the threaded version of Qt.  It's a little slow on my machine (P200) but fully functional. Now if I had hardware 3d accelleration and one of those spiffy new Athalons...\n\nOne thing that interests me, Antonio, if you a monitoring this story is using kalamaris as a plugin for krayon - the type of plugin that just paints an image on one of krayon's layers.  Actually almost any 3d app that writes to some kind of framebuffer should be able to do this fairly easily.  Being able to enter formulas in kalamaris and the common kde interface would make it a good \"proof of concept\".\n\nOur game of pool or pocket billiards in America is somewhat different.  But, some of the tables I've played on may as well have been designed with kbillar.\n\nOT: Suggested reading for kde developers:\n\n\"How to Win Friends and Influence People\" by Dale Carneige.  \"Blue Book\" by Emily Post."
    author: "John Califf"
---
<a href="mailto:larrosa@kde.org">Antonio Larrosa</a>, active KDE developer and preacher, recently wrote to us about the <a href="http://perso.wanadoo.es/antlarr/tutorial.html">release</a> of a new version (1.1.2) of his popular <a href="http://perso.wanadoo.es/antlarr/tutorial/index.html">tutorial</a> on how to write KDE2 applications. Antonio has also made public a first release of <a href=" http://perso.wanadoo.es/antlarr/kbillar.html">KBillar</a> (take a <a href="http://perso.wanadoo.es/antlarr/kbillar_screenshots.html">look</a> at it), an interesting KDE2 application, that uses a billiards game as a nice pretext for demonstrating the use of mathematics and 3D graphics.







<!--break-->
