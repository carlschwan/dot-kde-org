---
title: "Apps Roundup #2: Focus on Bibletime 1.0"
date:    2001-07-16
authors:
  - "Dre"
slug:    apps-roundup-2-focus-bibletime-10
comments:
  - subject: "What about the other apps?"
    date: 2001-07-16
    body: "I see that there has been LOTS of apps released since the last app roundup.  I know there is a new beta of Aethera, and a few other Kompany apps.  Its good that you focus on an app, but don't forget to \"roundup\" at least a couple other apps that have been released."
    author: "Eric"
  - subject: "Re: What about the other apps?"
    date: 2001-07-17
    body: "Don't forget a new beta version of Qt was released."
    author: "someone"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-16
    body: "Just a quibble: Usually Judaism and Christianity aren't considered a single \"faith.\""
    author: "Rakko"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-17
    body: "> Just a quibble: Usually Judaism and Christianity aren't considered a single \"faith.\"\n\nSure, they're not... however Christianity differs from Judaism in that Christians believe that Christ was the promised messiah where as the Jewish belief does not.\n\nThey are rooted in the same history and the Christian old testament is essentially Hebrew text."
    author: "Eric Laffoon"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-17
    body: "Yes, I understand that. I was only correcting Dre's phrasing of \"the Judaeo-Christian faith.\" If you mean Christian, just say Christian.\n\nRakko, living happily without faith and not wanting to start a flamewar, but annoyed at the overuse of phrases such as \"Judeo-Christian\""
    author: "Rakko"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-17
    body: "I believe he actually said *faiths* as in faiths rooted in Judeo-Christian theory."
    author: "Zach Hartley"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-16
    body: "Please don't include this one in the distribution!\n\nIf v2.0 will include support for at least 10 different religions, ok, go for it, otherwise don't!\n\n// Per Wigren, agnostic"
    author: "Per Wigren"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-16
    body: "Sure, KDE should be a free desktop and <b>not</b> a religious assimilation-software!"
    author: "Andreas Pietzowski"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-16
    body: "Uups, you should also allow <B>lower-case</B> Tags ;-)"
    author: "Andreas Pietzowski"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-16
    body: "Really off-topic, but the \"Allowed HTML\" does not work exactly (or is it my fault? ;-)"
    author: "Andreas Pietzowski"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-17
    body: "If you look just below the comment box you'll notice the \"encoding\" listbox, with \"Plain Text\" selected by default.  For HTML tags to work, you have to select HTML encoding.  However, that has been disabled on this site for a _very_ long time and the admins seem reluctant to let us post in HTML again since the site was defaced (even though HTML comment bodies weren't the problem, the problem was in the name/title fields!)"
    author: "not me"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-16
    body: "Why should this particular application be excluded just because it is religious in nature? It would be like excluding a recipe software because you have something against chefs."
    author: "Felix Rodriguez"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-16
    body: "No, it's not. Religion is the cause of many wars. I have never heard of a war started by chefs because they couldn't agree on recipes.. :)\nAnd BTW, I don't think a recipe program with fixed recipes would be of much use... :)\n\nKDE is distributed and used all over the world. Christianity is the smallest of all world religions. I bet that there are some islam countries that will *BAN* the whole of KDE if Bibeltime is included.\n\n(Disclaimer: My mother tongue is not english and I don't know the english names of most religions, books and even less how to spell them. That's why I have a hard time keeping this debate in english:)"
    author: "Per Wigren"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-16
    body: "I REALLY don't want to start a flamewar, but Christianity is definitely NOT the smallest of the world religions. In fact, if you count all the different types, it's actually the LARGEST. IIRC, there are ~1B Catholics, and ~1B Orthodox/Protestant/Others.\n\nAnd just to show I'm not smoking crack, from http://www.adherents.com/Religions_By_Adherents.html, it says that 33% of the world's population is Christian.\n\nNow, I highly doubt that most of them actually are (just as most people who claim to be Moslem/Buddist/whatever really practice their faith), but going on what people say, doesn't it make sense that there would be a Bible-reading program, and not a Koran-study tool? :)\n\nAnd I HIGHLY doubt that Islamic countries would ban the Christian Bible. After all, they ACCEPT most of it, except for the New Testament.\n\nBesides - who cares if they do? I just saw on Slashdot yesterday that Afghanistan has outlawed the Internet."
    author: "David G. Watson"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-16
    body: "Ok, I stand corrected, but I was referring to those who practice their religion. I'm probably counted as a christian because I'm born in a christian country (Sweden) and I'm baptised (is that the right word?) in church when I was about 3 months old. I also haven't yet left church yet so I'm still paying their taxes.. Yet, I don't consider myself christian."
    author: "Per Wigren"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-16
    body: "<BLOCKQUOTE>if you count all the different types, it's actually the LARGEST.</BLOCKQUOTE>\nIs it really <i>one</i> religion if there are so many \"different types\", and the many members of those different types can't seem get along with one another? For example, Catholics may form the largest portion of Christians, but they aren't exactly buddies with Protestants or Orthodox Church followers. They may have the same <i>fundamental</i> beliefs, but the similarities end there.\n<br>\nAnyway, enough of that rant.\n<br>\nI believe that we have enough religion in our software already (the religion of open source). Do we really need any more? Including this programme in the KDE distro could offend a great many people worldwide (remember, most of the world <i>isn't</i> Christian). We should leave it out and have it freely downloadable on its own just like any other app. People should not be forced to install it if they want KDE.\n<br>\nThere is absolutely no harm in leaving it out, but we could open a whole can of worms if we do include it. If someone wants it, they can install it. If they don't, then fine. Everybody is happy."
    author: "Shrek"
  - subject: "The Dot is messed up!"
    date: 2001-07-16
    body: "Hey, the server's not accepting HTML as it should!\n\nI have to try several times to post messages, since I keep getting Zope errors."
    author: "Shrek"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-17
    body: "You said:\n Is it really one religion if there\n are so many \"different types\", and\n the many members of those different\n types can't seem get along with one\n another? For example, Catholics may\n form the largest portion of \n Christians, but they aren't exactly\n buddies with Protestants or Orthodox\n Church followers. They may have the \n same fundamental beliefs, but the \n similarities end there. \n\nActually the similarities don't end there. While we have our disagreements on certain issues, for the most part Christians can set this aside (there are always exceptions to such rules), and recognize that we truly are one religon - the \none universal Christian church (Catholic means universal, btw). Just so you know my position, I am a evangelical protestant Christian. Now, I will quit, since I don't think that the Dot is a good place to get in a theological discussion, but I just had to point this out as David's figures are very much correct - Christianity is the largest religion according to more than just one poll.\n\nThat aside, I would ask why anyone is getting in an uproar about a simple review of a Bible program. \n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-17
    body: "OK since you brought it up. I'm catholic and i'm buddies and have biblestudy with protestants on a weekly basis. In fact the Catholic church has letters of understanding with several protestant churches. So your statements are a bit misleading. In reality we are one faith."
    author: "craig"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-17
    body: "Yeah, I did exaggerate a bit to make my point. What I'm trying to say is Christianity is nowhere near as cohesive as other large religions like, say, Hinduism (which would have about a billion followers). But enough of that (it's <i>way</i> off topic).\n<br>\nMy main thrust is that there is absolutely no need to include this app in the KDE <i>distribution</i>. Its inclusion would mean that anybody who wants KDE would be forced to install this app (unless they compile tarballs - yuck). Remember, over two thirds (and growing) of the world's population is <i>not</i> Christian, and many Christians are non-practising (especially in more developed nations). Some people may see the forced installation (in that it isn't easy to install KDE without it) as an insult. To get KDE accepted worldwide, we need to make sure that we offend other cultures as little as possible.\n\nPlease don't misunderstand me. I am not anti-Christian, nor am I against this app existing. It <i>should</i> be freely available, just like any other app. However, I don't believe it is important enough to be included in the KDE distribution. If a distro provider like Mandrake wanted to include it, then fine. Including it in KDE, however, could do more harm than good."
    author: "Shrek"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-17
    body: "I think that everyone agreed that it would be cool to have it in a seperate package labeled religion."
    author: "Craig"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-17
    body: "I have no qualms with that."
    author: "Shrek"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-18
    body: "I agree it shouldn't be in the distro, since it is clearly not an app most people would use.  I think the guy who's motivation is simply to censor anything religious, a prime example of liberal intolerance of any thinking not in line with their own narrow views."
    author: "Chris Bordeman"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-18
    body: "By reading the guy's other posts, I don't believe that he wants to censor anything. He was just saying that it shouldn't be in the main distro (maybe in a separate religion package). The concern is that we should not promote one religion over another, lest we offend other faiths. The best way for KDE to not offend anybody is to do nothing and stay secular (as KDE has always done). There is nothing <i>wrong</i> with this app, but people just shouldn't be forced to install it as part of KDE (they should have the choice). That's what I think he was trying to say, so in other words he is in agreement with us."
    author: "Shrek"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-19
    body: "Thank you! My intention was never to start a religious war... :)\n\nI should have been clearer in my first post so I wouldn't get so many flaimbait-answers that I just *HAD* to bite back on.... :-)\n\nMay whatever-you-put-your-faith-in be with you!"
    author: "Per Wigren"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-22
    body: "actually, it is not right to group christianity by churches, like protestantism and catholicism and such. christianity should be defined as believing in the bible and living your life accordingly. some common catholic practices such as praying to mary and the saints are simply not biblical, therefore people that follow such practices should not be considered christian regardless of their \"label.\" i'm not saying that people that label themselves by church cannot be true christians. i just think it can be misleading. as a christian, i don't label myself as \"babtist\" or \"catholic\" or \"protestant\" or anything like that. i label myself as \"christian,\" which means \"follower of christ,\" which is what i am."
    author: "Rick Kreikebaum"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2009-01-04
    body: "I wish someone could put together a \"One Source\" package for the Bible software or any other software groups.  It would save a lot of time searching for the particular programmes.  Leaving the Bible programme out is not offensive to me. I've never found a Bible in a computer store.  However, it would be great to have the \"One Source\" package for whatever it is I need.\n\nIn regards to the comment on \"different types\" of Christianity.  Hmmm.  Many people misunderstand what Christianity is and what religion is.  They are NOT compatible.  Christianity is wrapped up in a person, the Lord Jesus Christ and His provision of our salvation.  Religion is wrapped up in self and what we can do to somehow make ourselves more presentable to a holy God.  Christianity provides the answer to man's problem of sin.  Religion prolongs man's problem of sin and provides to absolute way to find peace with God.  I am grateful that God loved us enough to provide a way to escape the punishment of our sins through faith in Christ, unlike the religions of the world.  I guess you could say that all religions are created equal...equal in failing to make their adherents acceptable to God.  "
    author: "Chris"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-16
    body: "Religion isn't the only cause of wars. We can point to Land, Oil, Money etc. Taking your analogy one step further would mean that we should not include a financial application in KDE because money was used to start a war. Or we should no longer drive cars because Oil is evil and it is the cause of many wars."
    author: "Felix Rodriguez"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-16
    body: "Aw, come on! I have no problem with the program. Linux distributions can include it all they want. I just don't think it's right to include it in the *KDE* distribution! A financial application has lots of uses. Bibletime has only ONE use, and that is to study the christian bible.\n\nWhat if we included a program with the only use to study the satanic black bible (or whatever it is called) instead? Would there be any objections then?"
    author: "Per Wigren"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-16
    body: "Ok I concede the point. Maybe it shouldn't be included in the KDE Distribution because it might offend some people. \n\nI was more concerned about people who where looking to use a good bible-study software with KDE would not no that there was one available.\n\nThe intention was not to force others to use a product that they would not want to use."
    author: "Felix Rodriguez"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-17
    body: "This is no place to spread your anti religous hate!!"
    author: "craig"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-17
    body: "This is no place to spread your anti-(anti-religous) hate!!! ;-)"
    author: "Carbon"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-17
    body: "Ok you got me lol"
    author: "craig"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-17
    body: "isent christiannity the largest of all religions?"
    author: "[Bad-Knees] aka [Wounded-Knee]"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-16
    body: "<shameless plug>\nFunny you should mention that - Cookbook is going into kdenonbeta as soon as I can get my CVS password fixed :). (It's going into nonbeta because of the freeze, not because it's unstable)\n\nCookbook is a KDE recipe program written by yours truly - and it's now translated to German, thanks to Kevin Krammer. You can get it at http://www.mcs.kent.edu/~dwatson/cookbook.html .\n</shameless plug>"
    author: "David G. Watson"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-16
    body: "Why should this particular application be excluded just because it is religious in nature? It would be like excluding a recipe software because you have something against chefs."
    author: "Felix Rodriguez"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-16
    body: "Actually this application should be excluded because it defies the KDE naming conventions, and does not have a \"K\" in its name. If it is immediately renamed to KBibleTime, then perhaps it could be considered.\n\nThank you."
    author: "Anonymous Troll"
  - subject: "Need more K's!"
    date: 2001-07-17
    body: "I agree, and propose Khristianity as the new name.\n<br><br>\nNote that my Jewish calendar application, KLuach, is fully K-compliant.\n<br><br>\n<a href=\"http://www.leeta.net/kluach/\">http://www.leeta.net/kluach/</a>"
    author: "Otter"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-16
    body: "This is silly, if you want support for 8 more religions then contribute it!!\n\nIt is like if you said: I don't want Wine unless it can run 0S/2 programs too.\n\nCome on, my agnostic friend, don't let your faith shadow your mind! ;-)"
    author: "wSaintx"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-16
    body: "Hey, it's (probably) a great program, but because of the international nature of KDE it shouldn't be included in the *distribution*!\n\nPersonally I don't care much about any religion, but a lot of people do! And a lot of people of other religions will feel like it is christian propaganda."
    author: "Per Wigren"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-17
    body: "Thats just anti-Christian propaganda. From your own logic if kde isn't combatale with all languages then we should not release it."
    author: "craig"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-17
    body: "Just on a technical note, since you mention internationality, I'd like to point out BibleTime has much more thorough integration of international features than most programs, even on KDE.  The interface is localizable.  The book names are localizable (and quite easily extended if your locale isn't included already).  And the texts come in your choice of over 40 languages.\n\nI know this isn't really your complaint, but I wanted to point out that it is QUITE international."
    author: "Chris Little"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-17
    body: "Different Religious groups could make there own sword plugins. Should the Bibletime team be punished because they have not done that? Or should the sword project be forced to make Buddhist or Hindu plugins? Of course not. Lets not let your anti Christian prejudice cloud your reasoning."
    author: "craig"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-17
    body: "100% Agreement with Craig. Per should note the following:\n\n1. BibleTime does *not* include the sacred texts themselves. For that, you should go to the SWORD website, download *and compile* the SWORD manager, and then install the appropriate modules.\n\n2. BibleTime can handle any text, really ANY text that conform to the SWORD standard. Furthermore, it uses the Theological Markup Language (ThML), a subset of XML developed at Calvin College for using at the Christian Classic Ethereal Library (http://ccel.org)\n\n3. BibleTime is evidence that a KDE application can handle a very large database of rich data types. In that regard, the breaktroughs of the BibleTime developers can be extended further; and I have especially in mind the educational sector, one of the Holy Grails of the Linux desktop. (How's that for a BibleTime-based encyclopaedia?)\n\nBibleTime is something that should make all of us glad, whether we're religious of not. For my part, I am working on a Spanish translation of it; is the least way in which I can support such undertaking."
    author: "Eduardo Sanchez"
  - subject: "I am against putting this into KDE as well"
    date: 2001-07-17
    body: "While I am personally agnostic (with an atheistic bias), I think including this program could cause problems for KDE users in Arabic and other intolerant countries. (Saudi-Arabia, Lybia, Afghanistan, Cuba)\n\nUnless you rename the program to a neutral name (K Text Study) and provide access to the Koran and maybe other texts, I would keep it seperate. This is not an antireligious opeinion, I don't care about your beliefs at all, but I think it would damage KDE's reputation in non-christian countries with censorship."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: I am against putting this into KDE as well"
    date: 2001-07-18
    body: "That is a really good point, but I doubt this app would become part of the main distro since most people would never have any use for it."
    author: "Chris Bordeman"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-17
    body: "I think a simple solution would be to create an optional KDE-Religion package for all religious software. \n\nI work for a Christian web site  (www.gospelcom.net) and go to Calvin College (where ThML is being developed by Harry Plantinga, one of the CS profs).  I think in the general scheme of getting people to use KDE, having religious applications would be helpful.   I certainly think Bibletime would be more widely used and appreciated than say, the Fractals Generator.  I'm around quite a few Christians that regularly use Bible software; it'd be great to also get them hooked on KDE.  \n\nAlso ThML is geared designed to meet the needs of religious texts in general.  While it has a Christian slant, it has many properties which are useful for marking up other theological texts.  Here's the abstract for the spec:\n\nThis document describes the Theological Markup Language (ThML), an XML markup language for theological texts. ThML was developed for use in the Christian Classics Ethereal Library (CCEL), but it is hoped that the language will serve as a royalty-free format for theological texts in other applications. Key design goals are that the language should be (1) rich enough to represent information needed for digital libraries and for theological study involving multiple, related texts, including cross-reference, synchronization, indexing, and scripture references, (2) based on XML and usable with World Wide Web tools, (3) automatically convertible to other common formats, and (4) easy to learn and use. ThML is defined as an XML DTD that extends the Voyager DTD for HTML. \n\nI also don't think Christains would have any problem with inclusions of texts of other faiths.  Bibletime may serve as a catalyst for other faiths to work on similar software for their texts."
    author: "Scott"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-16
    body: ">> If v2.0 will include support for at least 10 different religions, ok, go for it, otherwise don't!\n\n    I agree.  If:\n\n    Krayon does not include .gif support.  I have a political problem with Unisys and the LZW .gif fiasco.  Also, since a large percentage of the population is colorblind, it should only support greyscale images.\n\n    noatun removes all support and reference to the Franhauffer's MP3 codec.  Since it is not Free, it should be removed.  Ogg Vorbis can remain.\n\n    KDevelop does not support native tools for the One True Language, InterCal. Since it excludes programmers, it should be removed immediatly.\n\n    Now that speech synthesis is just starting to work, KDE will able to be published, but it is fantastically rude and rather nasty to publish it when the blind cannot use it.  We should immediatly take ftp.kde.org offline until this situation is rectified.\n\n    In addition, make sure that those \"10 different religions\" that you mention include dogma from the Church of the Subgenius (Praise \"Bob\"!), the Principia Discordia (Hail Eris!), the Church of the Great Silicon Ghod (Foo), several thousand sect writings, and the whole lot must be reviewed by the Pope and the one chosen representitive of Allah (all of them) to make sure it is allowed to be distributed.\n\n     Oh, and I respect you so much for both the act of censoring an application that many could use, and the incredible strength that is necessaary to stand up to groups like the Scientologists (who are rumored to have killed to protect their secrets) when you demand that their Operating Level Theten III (OT III) be published.\n\n    I'm not saying that this should be placed in the distribution.  But *if* \"we\" (the vague majority of active KDE users and developers) wanted any sort of app like this in KDE, then it takes one religion to start the ball rolling, even if three seconds later, you commit the Torah, the Koran, and the Book of the Subgenius and Revelation X annotation applications.\n\n    I would imagine that most KDE apps are written in one languge at one point.  That doesn't mean their existance or that it is the programmers desire to steer KDE towards German or English only.\n\n     I'm very glad for the existance of this app: it shows that the popular niche applications are being addressed.   It wouldn't bother me at al if it were included in the distro, no more than if KDE Quake were included (which I wouldn't use at all, either).\n\n     For that matter, a very real example: does Kapital support all currency and transaction / account types?  Does that mean it wouldn't make an excellent addition to KOffice?\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "To be continued, soon the TopfferTime..."
    date: 2001-07-16
    body: "What Means KDE ? K Desktop.\n\nThere is no need to Bible or Coran in a desktop. Perhaps something to study all big books, yes...\n\nBut it is out of topic. Nobody of the KDE team has said that this BibleTime would be included in KDE, so no problem.\n\nIf, one day, I create a KDE programm to compare the different versions of Rodolphe T\u00f6pffer comics (In Switzerland, France, Great Britains, USA, Germany, Portugal and so and so), I will be glad to announce it on this site. Be quiet, I would not want to include it in the KDE distribution, of course...\n\nAbout T\u00f6pffer (for me more important than God, of course) : http://pressibus.org/bd/debuts\n\n(and, of course, I am not a minority because a majority of comics fans and historians think that T\u00f6pffer is the comics inventor...)\n\n(and a picture of T\u00f6pffer on the desktop is many more fine than some paragraphs of the bible, of course... so it is more useful for the K Desktop...)"
    author: "Alain"
  - subject: "Re: To be continued, soon the TopfferTime..."
    date: 2001-07-16
    body: "*Sigh*  Some people miss irony, satire, sarcasm *AND* proceed to make the exact same point that I did.  Did you not read my message?  Maybe the bits about \"I doubt it will ever be added, or even considered\" that I now realize I didn't type?  ;) \n\n   Of course, it's 11:34am, the deadline is today, and I've been up all last night working.  :)  I expect anything I post today to be regretted tomorrow, but it feels good right now.\n\n   Besides; the holy duality of comics are Neil Gaiman and Stan Lee.  Wanna get into a religious flamewar over golden age versus modern gritty comics?  (Yes, everything generates controversy... even the \"K\" in KDE... heck, I've been arguing for years with a friend if the belts on the spacesuits at the end of the Rocky Horror Picture Show are silver or gold.  I'm imagine many arguments over religion are much less heated).\n\n    Wait until KDE gets more high profile and it's labeled demonic because it has a vile serpent, symbol of Satan, as its mascot.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: To be continued, soon the TopfferTime..."
    date: 2001-07-16
    body: "Incredible !\n\nSomeone who knows about \"monsieur Vieux-Bois\" and another one which name I presently can't remember !\n\nI know about those because I found original versions at my grandmothers house -- And that is the kind of discoveries that make your day. \n\nAnyway, way OT here.\n\nAside from that it would be great if Bibletime could be made into a generic text study program."
    author: "Hmmm"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-17
    body: "Perfect"
    author: "craig"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-17
    body: "Give me a break. Its an app review up please lets keep anti-religious prejudice out of it."
    author: "craig"
  - subject: "Cool it"
    date: 2001-07-17
    body: "Good one, Per Wigren.  I am an agnostic myself (ie I do not have any particular beliefs or non-beliefs) and yet I was originally very eager to have Bibletime reviewed and featured on the dot.\n\nNobody ever said anything about including Bibletime in the distribution, it is only a REVIEW, so I think you should cool it and let people who ARE interested in this app check it out.  Like, live and let live.\n\nAnd really, even if, and that's a big if, Bibletime ever gets included in KDE, so what?  It'll probably go into the kde-religion package with a dozen other contributions.  So just feel free to ignore it and let other people enjoy the app.  Heck, I might check it out myself.  I'm sure there's something there that could be learned.\n\nIt's all a matter of respect and tolerance.\n"
    author: "Navindra Umanee"
  - subject: "Re: Cool it"
    date: 2001-07-17
    body: "There is a kde-religion package? I didn't know about that! If you include it in such a package it's all fine with me!\n\nI just didn't want it included in kde-utils or something like that..."
    author: "Per Wigren"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-17
    body: "> Please don't include this one in the distribution!\n\nI don't recall any mention of this being suggested to be in the distribution. There are a number of KDE apps not in the distribution. In theory the distributed apps should be the most widely useful and practical and should not be swollen in size unless it can pass the tests. Bibletime would add a lot of size if you included every optional text and lexican. Where exactly would it be put? In libs, base, utils? I doubt it. I can see it going into a general apps module should one materialize, but not the main distribution.\n\n> If v2.0 will include support for at least 10 different religions, ok, go for it, otherwise don't!\n\nI expected to read things here that took different tones amounting to some form of opposition to this software. Excuse me for a second... in ancient Rome Christians were fed to the lions for entertainment... I assume people are also familiar with the holocaust and the suffering of the Jews there in. I think those of these faiths have paid their dues to stand on their own.\n\nNow, just like I get really tired of people suggesting that such and such open source project ought not to have been done, or that programmers on one project ought to go to another... this is a terrible idea. If someone feels strongly about their convictions and wants to produce an electronic means of studying the bible it is quite ludicrous to suggest they ought to now assume the mantle of setting up documentation for the other world religions. Isn't it? \n\nFirst they used another open source project for the texts called SWORD. Second, with all due respect to those of other faiths and good intents, there are a number of extremist sects of other faiths that could be termed, in the most benevolent terms, hostile towards Christians. There are also places in the world that do not allow foreign Christian missionaries to come and provide food, clothing and assistance to their populations while children suffer and die due to religious belief systems so diametricly opposed that they view the Christian's help as interfering with divine judgement. I say this not to point fingers or start a war of beliefs... only to point out that it is absurd to suggest that someone should add such diametricly opposed views to their work.\n\nSomeone who signs off as an agnostic suggesting that people of one belief ought to do the work of others with other belief systems is truly absurd.\n\n> // Per Wigren, agnostic\n\nAnd being an American I recognize your right to Agnosticism. I would hope however that \"good news for modern man\" can now be seen as a good thing for those hoping such a project would reach this stage on KDE. I can only hope that those who likely otherwise preach tolerance for other religions and beliefs can demonstrate that now (not that this post was particularly intolerant but I would bet some will be).\n\nIf you are not a Christian fine. If you are not curious about the historical nature of the bible, fine. It would be really nice to see someone say they're not a Christian but they're happy to see this for those who are though instead of many of the talkbacks I've read that seem to be searching for some reason to be unhappy about this software.\n\nJust like any other piece of software, if you don't like it... don't use it. But from the standpoint of an application for KDE Bibletime is impressive in scale and function compared to the usual KDE app. I think all people who support KDE ought to be proud that such a tool now exists on KDE. As for other religious beliefs... hey, if you believe otherwise nobody is stopping you. Take Bibletime as an encouragement and go for it.\n\nCongrats to the Bibletime team. This is another big win for KDE."
    author: "Eric Laffoon"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-17
    body: "Well said Eric!!!"
    author: "craig"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-17
    body: "> If v2.0 will include support for at least 10\n> different religions, ok, go for it, otherwise\n> don't!\n\nThere is no one hindering other religious groups\nto put their texts throught the sword engine and\nprovide the necessary database.\n\nIn fact, for the Onlinebible (www.onlinebible.org) you can get a translated Koran (a contradiction in itself, by the way, muslim people tend to make sure that the Koran was never translated ... smile)\n\nI understand that Sword took some texts from the Online Bible project."
    author: "Me"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-17
    body: "please do not include bibletime in the kde distribution. if you do i will move to gnome and\ni've been using kde for a *long* time now."
    author: "Simon Kenyon"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-17
    body: "GnomeSword (http://gnomesword.sourceforge.net) will be waiting for you if you switch to GNOME.  It serves the same function, uses the same underlying libraries & content, etc. only it uses GNOME instead of KDE for its app framework.\n\n:)"
    author: "Chris Little"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-17
    body: "Checkmate.  Thanks for that Chris, maybe people can stop being ridiculous now."
    author: "KDE User"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-17
    body: "look chaps.\n\ni live in a country that has been living with a\nquasi-religious war for the last 600 years. people\ndie because of their religion.\n\nnow we can all argue (most far more eloquently\nthan i ever could) that the conflict in ireland\nhas absolutely nothing to do with religion, but\nnobody can argue that it isn't being used as an\nexcuse. and that's good enough for me.\n\nthe computer world is a nice little haven far\nremoved from this gross stupidity. i only wish\nthat it would remain so.\n\ni deeply respect (and indeed admire) people with\nstrongly held religious beliefs. just don't foist\nthem on me, thank you very much.\n\nyours in peace and tolerance\n--\nsimon"
    author: "Simon Kenyon"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-17
    body: "In fact, the conflict in Ireland has nothing\nto do with it at all. It is about the conflict\nbetween the native population and families of\nsettlers who have come from England several\nhundred years ago. It's about the wish for in-\ndependence vs. the wish for strong ties to England.\n\nI find it quite ironic that you mention the word\n'foist'. I have on various mailing lists found the\nBibleTime developer to be one of the most reasonable \npeople. In contrast, what I find in\nthis forum is that that certain people try to\nfoist their anti-religious hatred on others,\nand that in a very insulting way.\n\nIf you are going to use GNOME, just go ahead. I\nam convinced that it is the right camp for people\nwho hate."
    author: "Bernd Gehrmann"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-17
    body: "did you read what i wrote?"
    author: "Simon Kenyon"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-19
    body: "Why not? What gives you he right to judge that a web browser is useful but such a program is not? Konqueror does only display web relevant file types, not Microsoft office documents and such. Shall I conclude that Konqueror should be excluded until it conforms to Microsoft?\n\nIn other words: An application does not have to support everything. If somebody wants to write an app for reading the Koran, he is heartily invited. But as only few readers of the koran will ever read the Bible, a common app is not sensible!\n\nMirko"
    author: "Mirko Kloppstech"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-16
    body: "I'm a Buddhist (I'm serious).  Could I extract the Christian contents and replace them with extracts from the Sutras?  Would that be easy to do?\n\nJohn"
    author: "John"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-17
    body: "At this juncture it would be a major undertaking.  Everything in Sword (BibleTime's underlying backend library) is pretty well hardcoded to the Judeo-Christian Bible and a particular translation of it at that.  (NB: I do mean JUDEO-Christian because we also support a number of strictly Judaistic texts like the Hebrew Bible (OT) and a translation thereof by the Jewish Publication Society.  Also, keying everything to a single translation isn't that bad since 99.9% of the Bible's keys, aka verse numbers, are identical between translations.)\n\nSword is currently at version 1.5.2, in the dev branch leading up to our 1.6.0 stable release.  After that is cleaned up, we'll begin extensive rewrites of many of our classes allowing other books to be added.  You might not see us release works from other faiths, but it should be easily possible to add them once this rewrite is complete (i.e. once 2.0 is released).  Our real motivation behind this is allowing ancillary texts like apocrypha, pseudepigrapha, the Josephus corpus, etc. to our library of texts, but the engine should be open enough to add anything.\n\nWhat you could possibly do is create a dictionary module of the sutras, but you would have to retrieve the entirety of a sutra at once.  It would be inelegant and something of a hack, but it would be searchable I suppose.  See our SF page at http://sourceforge.net/projects/sword for instructions on module creation.\n\n--Chris (from the Sword Project http://www.crosswire.org/sword)"
    author: "Chris Little"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-17
    body: "I must say that I was also surprised when I noticed that despite all of this discussion there's really only been one person to object to Bibletime (Well two, but the other was on grounds of it bloating things.).\n\nTouchy issue, eh?"
    author: "Scott"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-17
    body: "Maybe the others aren't reading dot.kde.org :)\n\n(sorry, I'll quit this now, include it as much as you want as long it's kde-religion or something similar instead of the \"normal\" packages)"
    author: "Per Wigren"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-17
    body: "Our aim was never to get included in the standard KDE.\nBut we have nothing against it if BibleTime gets included in Debian or other Linux distibutions."
    author: "BibleTime developer"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-17
    body: "I agree with you 100% on that one! :)"
    author: "Per Wigren"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-17
    body: "I like the screenshots I hoop the next apps roundup have more schreenshots"
    author: "Underground"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-17
    body: "What?  A comment having to do with the review?  Oh my . . . maybe you should come Aboveground."
    author: "Observer"
  - subject: "Compile error"
    date: 2001-07-17
    body: "Ok, so I decided to stop pissing everyone off and actually try the program.\n\nIt fails to compile though. I use SuSE 7.1RC-Axp (Alpha - the processor:) and KDE 2.2alpha2.\n\n++ -DHAVE_CONFIG_H -I. -I. -I../../.. -I/opt/kde2/include -I/usr/lib/qt2/include -I/usr/X11R6/include   -D_REENTRANT -I/usr/src/tmp/sword/include -DQT_NO_ASCII_CAST  -O2 -fno-exceptions -fno-check-new -Wall -pedantic -W -Wpointer-arith -Wmissing-prototypes -Wwrite-strings -Wno-long-long -fno-builtin -c cswordmoduleinfo.cpp\ncswordmoduleinfo.cpp: In method `CSwordModuleInfo::CSwordModuleInfo\n(CSwordBackend *, SWModule *)':\ncswordmoduleinfo.cpp:49: no matching function for call to\n`CSwordBackend::Version ()'\ncswordmoduleinfo.cpp:50: no matching function for call to\n`CSwordBackend::Version ()'\ncswordmoduleinfo.cpp: In method `const QString\nCSwordModuleInfo::getAboutInformation () const':\ncswordmoduleinfo.cpp:132: warning: ISO C++ forbids variable-size array\n`dummy'\n/usr/src/tmp/sword/include/swfilter.h: In method `void\nSWFilter::setOptionValue (const char *)':\n/usr/src/tmp/sword/include/swfilter.h:79: warning: unused parameter `const char *ival'\n/usr/src/tmp/sword/include/swfilter.h: In method `char\nSWFilter::ProcessText (char *, int, const SWKey *)':\n/usr/src/tmp/sword/include/swfilter.h:92: warning: unused parameter `const SWKey *key'\nmake[4]: *** [cswordmoduleinfo.o] Error 1\nmake[4]: Leaving directory `/usr/src/tmp/bibletime-1.0.1/bibletime/backend/sword_backend'\nmake[3]: *** [all-recursive] Error 1\nmake[3]: Leaving directory `/usr/src/tmp/bibletime-1.0.1/bibletime/backend'\nmake[2]: *** [all-recursive] Error 1\nmake[2]: Leaving directory `/usr/src/tmp/bibletime-1.0.1/bibletime'\nmake[1]: *** [all-recursive] Error 1\nmake[1]: Leaving directory `/usr/src/tmp/bibletime-1.0.1'\nmake: *** [all-recursive-am] Error 2\narkanoid:/usr/src/tmp/bibletime-1.0.1 #"
    author: "Per Wigren"
  - subject: "Re: Compile error"
    date: 2001-07-17
    body: "Hi!\n\nBibleTime requires Sword 1.5.2. Get it at www.crosswire.org and remove the old version from your system.\nThis should work.\n\nGod bless you!\nJoachim"
    author: "BibleTime developer"
  - subject: "Re: Compile error"
    date: 2001-07-17
    body: "I used the 1.5.2 CVS snapshot that you linked under \"requirements\".\n\n> God bless you!\nOk, we'll see if that helps... :)\n\nRegards!"
    author: "Per Wigren"
  - subject: "Re: Compile error"
    date: 2001-07-17
    body: "You're right - the link in the requirements section is wrong.\n\nWe'll correct this,\nJoachim"
    author: "BibleTime developer"
  - subject: "Inclusion of apps in KDE"
    date: 2001-07-17
    body: "Not that this is an issue, but after reading the above discussion I thought it might be usefull information.\n\nFor an application to be part of KDE the following criteria must be met:\n\n* The application must be primarily developed in KDE CVS.\n* The application must be actively maintained.\n* The application must have an appropriate license\n(See http://developer.kde.org/documentation/licensing/policy.html)\n\nNot realy a criteria but more or less following from common sense, distribution of an application should not carry an unreasonable legal risk and, just as we expect everyone involved in KDE to respect others, an application should do that as well.\n\nCheers,\nWaldo"
    author: "Waldo Bastian"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-17
    body: "This app rocks!!!  The professionalism rivals that of commercially available apps on _other_ OSs.  Great job guys.\n\n\nPS. It would be a shame to not see this app available in distros because some people would choose not to install it.  I choose not to install MOST things included in the distros I use!"
    author: "Scribe"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-17
    body: "I agree! It shows that that the open source community can develop not only excellent compilers and desktop environments, but also GREAT Bible study tools... \n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "Why..."
    date: 2001-07-17
    body: "does the fact that this is a Judaeo-Christian application automatically mean that it is necessary to argue against it's inclusion in KDE - even though no one mentioned including it?\n\nPersonally I think Bibletime is a marvelleous work, and would be nice in KDE just to demonstrate the versatility of KDE. \n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "Re: Why..."
    date: 2001-07-17
    body: "Good point lets add it to KDE 2.3. It would make a great addition. Just because one bigot objects is no reson not to.\n\nCraig"
    author: "craig"
  - subject: "Re: Why..."
    date: 2001-07-17
    body: "I'm the most intolerant guy here, right? ;-p\n\nYes, let's include it in a package called kde-religion or something like that!"
    author: "Per Wigren"
  - subject: "Re: Why..."
    date: 2001-07-18
    body: "I think that sounds very good, and something every should be able to settle with (since it doesn't have any translation installed by default anyway).\nReally, it would be impressive to see this, it would show that KDE may not be able to settle open source wars, but at least it can avoid religious wars! :-)\n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "Re: Why..."
    date: 2001-07-17
    body: "Hi there,\n\nwell, this proggy really looks nice, and it might be a good demonstration of what can be done with KDE.\n\nBut: Please do not put every app which just *looks* nice into the official KDE distrib.\n\nThere are already a lot of apps, which are just too specialized, I think, and which are only needed by few people. Wouldn't it be better to make them just optional packages, rather forcing everyone to download / install them? The KDE is already that huge.\n\nAnd btw. is there a good uninstaller for those kde apps, I really never use? Doing this by hand might be dangerous to break the system...\n\ngreez, CDR\n\nPS: Please no djihad here."
    author: "Christian Rahn"
  - subject: "You are pushing it"
    date: 2001-07-17
    body: "Jesus H Christ. If you plagerize my bible writing one more time, I will call the RIAA upon you."
    author: "Jesus H Christ"
  - subject: "Re: You are pushing it"
    date: 2001-07-17
    body: "Now who's pushing it? (Jesus H. Christ@heaven.org?) If you know scripture read: Matthew 24, 5.\nSo, What?"
    author: "B. Wagner"
  - subject: "Re: You are pushing it"
    date: 2001-07-17
    body: "Jesus... I'm just putting a little humor here. Sorry to offend you."
    author: "Jesus H Christ"
  - subject: "Sword"
    date: 2001-07-17
    body: "Interesting naming of the lib though. *SWORD*. But only appropriate, after all.\n\nCU"
    author: "bela"
  - subject: "Re: Sword"
    date: 2001-07-17
    body: "Yes, it is based on Hebrews 4:12 and Ephesians 6:17."
    author: "Eduardo Sanchez"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-17
    body: "Ok. This is my LAST posting in this forum!\n\nCheck out this site: http://www.religioustolerance.org , read the essays and we can stop fighting!\nAnd BibleTime webmaster, please include it in the links-section! It's a really great site, the best I've found! It has objective \"reviews\" of every religion and cult one can think of! They compare opinions and \"rules\" without saying that one is right or wrong. I read a lot of the essays there the last year to be able to debate with my girlfriend if she would try to preach for me... :)\n\nNow, if you want to continue the \"debate\", do so by mail."
    author: "Per Wigren"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-17
    body: "(Im from the Faroe Island - so there might be some spelling errors - my C is better than my english)\n\nIm am a christian who belives that jesus christ is the son of God - and i think its great to have application to help me in my studies - and including it in the distro is to me a good thing. Nobody has to install it, i will, and have - just an older version (a bit buggy - but usable). Excluding it from the distro grounded on the purpos of the program, which is bible study (which should be to any non-christian a book-study as any other), is a restriction. And why are people offended by a bible-study program??\nIf you dont like it, dont install it. If you are a buddist - than hack it to be buddatime:).\nThink of it as freedom to install it or not.\n\n...when the son sets you free, you are truely free.\n\nThank you!\n\n[Bad-Knees]"
    author: "[Bad-Knees] aka [Wounded-Knee]"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-18
    body: "> If you are a buddist - than hack it to be buddatime\n\nSo if your thing is hammers, should you hack it to be HammerTime ?  ;)"
    author: "Billy Nopants"
  - subject: "Re: Apps Roundup #2: Focus on Bibletime 1.0"
    date: 2001-07-19
    body: "My thing is to party, so I guess I should make it PartyTime! :)"
    author: "Per Wigren"
---
The weekly <i>Apps Roundup</i> column has turned out to be not-so-weekly this far, but in the second installment I go much further in-depth and take a closer look at <A HREF="http://www.bibletime.de/">Bibletime</A>, a scripture-study program for those of the Judaeo-Christian faiths.  Because I went crazy with the screenshots, the review is
<A HREF="http://static.kdenews.org/content/bibletime/bibletime.html">available here</A>. Enjoy!



<!--break-->
