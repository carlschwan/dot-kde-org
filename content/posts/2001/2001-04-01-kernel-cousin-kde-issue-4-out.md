---
title: "Kernel Cousin KDE Issue #4 is Out"
date:    2001-04-01
authors:
  - "numanee"
slug:    kernel-cousin-kde-issue-4-out
comments:
  - subject: "Re: Kernel Cousin KDE Issue #4 is Out"
    date: 2001-04-01
    body: "Wouldn't it be a better idea to make KDE actualy start up faster rather than change the start-up splash screen?  How does that fix the problem?"
    author: "John"
  - subject: "Re: Kernel Cousin KDE Issue #4 is Out"
    date: 2001-04-01
    body: "Well, of course it would be better to make KDE start up faster, and people are working towards that eventuality. However, a lot gets done at startup, therefore starting KDE does take some time. Period. \n\nThe problem remains however that many users see \"Starting Window Manager\", watch it take several seconds, and think \"Wow. That KWin is SLOW!\" when in reality several complex applications have started, not just kwin. Letting people know that all of this is going on can help users appreciate that it isn't slow, but rather a lot of processing is occuring. Some of that processing time is spent making the rest of the KDE experience faster, such as preloading KDE libs into memory."
    author: "Aaron J. Seigo"
  - subject: "Re: Kernel Cousin KDE Issue #4 is Out"
    date: 2001-04-01
    body: "The particular problem in the reported case (which made a 950Mhz athlon start as slow as a 350mhz K6) was a configuration problem which stalled some startup code - I don't know whether the \"solution\" ever made it to the mailing list or just did not make it into \"Kernel Cousin\".\n\nTo give people an idea how long kde \"should\" take to start here are some times from my machine (450Mhz PIII, \"warm caches\"):\ndcop :   0.5\nkded :   2.3\nkcminit :      0.4\nwm started : 2.2\nkdesktop:   2.0\nkicker :     1.6\nsession:        0.1 (no program gets started)\n\nIf the relation between these stages is very different, you might want to take a close look at debug output to see if there is some problem.\n\nIf you look at the above numbers you see that it takes a lot longer than you'd want - but that the time is split very finely among a lot of different tasks, so that it is very difficult to \"take a big axe\" to some feature you don't want/need.\n\nI spent a weekend looking for possible speedups and the only measurable thing I found was a faster heuristic for building hash tables in kbuildsycoca, which took out almost 1 second of startup time - at the cost of slightly worse hash characteristics.\n\nmartin"
    author: "Martin Schenk"
  - subject: "Re: Kernel Cousin KDE Issue #4 is Out"
    date: 2001-04-02
    body: "I've been wondering about the startup times myself. This is the conclusion/suggestion that I came to. Since the job of a WM is to provide a UI for the user, the first thing the WM should do is give the user a working UI. Given the 9.1 second example above, it would make sence to have the wm load taking it's 2.2 seconds. No one should complain about that. After that everything else that should be loaded can be loaded in the background.\n\nSure, doing things this way may mean that the first application loaded may take an initial load-time performance hit, but by the time it's loaded, everything else will be too!\n\nNow for a suggestion:\nHow about adding a scrolling buffered window that catches the stdout/stderr messages of the apps running in KDE, including kwm! This way, if I click on an icon and the program fails to run, I can just look in that window and see what happened instead of trying to go to tty12(on my system) and praying that the message didn't scroll off-screen yet."
    author: "Arkain"
  - subject: "Re: Kernel Cousin KDE Issue #4 is Out"
    date: 2001-04-05
    body: "I agree, being in development myself we always have to deal with preceived speed of the app verses true speed.  As a developer I understand that alot is taking place, but as a user I dont want to have to sit and wait.\n\nI also like the idea of having a small window showing what is taking place in the background and think is would be useful if we had the ability to keep it up during the whole session instead of just start up....(I know there are ways to do this now....this is just a sugestion for user friendliness."
    author: "jb3rry"
  - subject: "Re: Kernel Cousin KDE Issue #4 is Out"
    date: 2001-04-07
    body: "I have to disagree with you about giving a working UI first. Currently W2k will give you a working shell very quickly after you complete the login. As a naive user, I get control and launch my first app. Unfortunately the rest of the system libraries have not been loaded. Now there is contention for the disk, and I end waiting longer to use my app than if the system loaded its libraries up, and then launched my app.\n\nAnd yes, I have actually ad-hoc tested this on my W2k box. I timed starting outlook from as soon as I get the shell and from waiting until the disk activity slows down and then starting outlook. I save several seconds by waiting."
    author: "Jesse"
  - subject: "CORBA/ORBit"
    date: 2001-04-01
    body: "I'd love to see GNOME and KDE use the same core ipc mechanism. I'll think this would open up the current 'not invented here' trench war and really benefit both desktops.\nWhat I've read from the list archive the arguments against it are a bit vague, and a bit closed minded.\n\nSomething to look forward to in KDE 3.0?"
    author: "Fredrik"
  - subject: "Re: CORBA/ORBit"
    date: 2001-04-01
    body: "I too would like to see cooperation between the projects and I think that cooperation on the IPC level would be tremendous.\n\nHowever, merely choosing to use ORBit does not a standard make. IMO we do not need a de facto standard that can be changed whenever one of the projects decides to alter course (and we all know that such a thing would NEVER happen, right? ;-)\n\nWhat is needed for meaningful cooperation is an explicit and well defined standard so that inertia isn't the only thing keeping cooperation together.\n\nThat said, the arguments against using ORBit have been not close minded as much as pragmatic. The developers want to know what ORBit will gain us verses the amount of work it will be (and without a defined standard the percieved worth is less). \n\nOf course, the developers are also concentrating right now on making KDE2 the best they can. There are a good number of open bugs remaining as well as features that needs adding or adjusting (and they are doing a great job of it, I might add =). Right now probably isn't the best time to go and change something so deep and at the core of the desktop."
    author: "Aaron J. Seigo"
  - subject: "Re: CORBA/ORBit"
    date: 2001-04-05
    body: "If you didn't know ORBit is a CORBA(2.2?) implimentation.  CORBA is an open standard.  If you would like to learn more about CORBA go to www.omg.org.  The reasont that KDE decied not to use CORBA for IPC is becuase the developers where using the buggy bloated MICO implimentation, ORBit is getting alot of work and is becoming extremely fast and very light on recources, CORBA also add alot of flexibilty, like remote objects.  One of the huge advantatges to using CORBA is that there could eventualy be a CORBA wrap to the Qt and KDE libs.  Apps using GPL Qt would not have to be released under the GPL because they aren't linked to the libs, thus making the Qt libraries truly free to use anyway you want."
    author: "robert"
  - subject: "Re: CORBA/ORBit"
    date: 2001-04-05
    body: "While I appreciate you taking time to point me in the direction of some useful information, I am fully aware of what ORBit is as well as the history of CORBA in KDE.\n\nThat said, my problem is not with using CORBA itself, but in the creation of de facto standards as opposed to defined and agreed upon standards between the parties involved.\n\nAdditionally, I find the concept of end-running someone's licensing wishes by using something like CORBA wrappers to be lacking in ethical meritt."
    author: "Aaron J. Seigo"
  - subject: "Re: CORBA/ORBit"
    date: 2001-04-04
    body: "Did anyone try ACE/TAO instead of ORBit ?\nWell ... TAO has some advantages I think."
    author: "Chafik Moalem"
  - subject: "Re: CORBA/ORBit"
    date: 2001-04-04
    body: "Is this another April Fool's joke ?"
    author: "Anonymous KDE User"
  - subject: "Re: CORBA/ORBit"
    date: 2001-04-04
    body: "If it is they'd sure been cunning, the first post is dated march the 26th and has 16 follow-ups."
    author: "Fredrik"
  - subject: "Re: Kernel Cousin KDE Issue #4 is Out"
    date: 2001-04-01
    body: "I agree completely. This merge would be a boon to application developers and desktop acceptance alike. I encourage all KDE developers to strongly debate the pros and cons of this proposal and then make a decision based on what is best for your users.\n\nBest regards,\n\nDavid"
    author: "David Joham"
  - subject: "Speaking of KDE/GNOME interop"
    date: 2001-04-03
    body: "Does anyone know what I need to install or configure in order to get Konqueror to use a GTK theme? I use GNOME (sorry!) but I like Konqueror as a browser and I also want a consistent look'n'feel to all my apps. I looked in the Konqueror settings dialogs but can't find anything (I use debian and the only KDE packages I have installed are Konqueror+dependencies).\n\nWhat I'd *really* love is if KDE-based applications would automatically \"notice\" that they're actually running under GNOME and pick up the GTK theme if so. Then I'd like GNOME apps to do the same when running under KDE, but that's another complaint and not one anyone here can do anything about...\n\nThanks in advance,\n\nStuart."
    author: "Stuart"
  - subject: "Re: Speaking of KDE/GNOME interop"
    date: 2001-04-03
    body: "Try \"K -> System -> Legacy theme importer\" or run klegacyimport"
    author: "KDE User"
---
<a href="mailto:aseigo@mountlinux.com">Aaron J. Seigo</a> is back with another excellent edition of <a href="http://kt.zork.net/kde/latest.html">KC KDE</a>.  This week, read about <a href="http://www.kde.org/people/matthias.html">Matthias Ettrich</a> advocating the use of <a href="http://orbit-resource.sourceforge.net/">CORBA/ORBit</a> as an alternative to <a href="http://www.rzg.mpg.de/rzg/batch/NEC/sx4a_doc/g1ae04e/chap11.html">ICE</a> for <a href="http://developer.kde.org/documentation/library/2.0-api/dcop/HOWTO.html">DCOP</a> interprocess communication, IMAP in KMail CVS, <a href="http://gallium.n3.net/">new KWin styles and themability improvements</a>, kdeprint updates, offline surfing in <a href="http://www.konqueror.org/">Konqueror</a>, and more. Read it all <a href="http://kt.zork.net/kde/kde20010331_4.html">here</a>.




<!--break-->
