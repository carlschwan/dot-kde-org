---
title: "SECURITY: New KDE Libraries Released"
date:    2001-05-01
authors:
  - "Dre"
slug:    security-new-kde-libraries-released
comments:
  - subject: "Re: SECURITY: New KDE Libraries Released"
    date: 2001-05-01
    body: "Dumb question:\n\nwhat proper rpm syntax to install on stock suse 7.1 with kde 2.1.1?\n\nthanks"
    author: "Dummy"
  - subject: "Re: SECURITY: New KDE Libraries Released"
    date: 2001-05-01
    body: "rpm -Uvh <filename>\n\nwhere filename is the package(s) you've downloaded.\n\nyou can also use yast."
    author: "Evandro"
  - subject: "Re: SECURITY: New KDE Libraries Released"
    date: 2001-05-01
    body: "That is what i do and got message \"conflicts with file from package klibs-1.1.2-217\""
    author: "Dummy"
  - subject: "Re: SECURITY: New KDE Libraries Released"
    date: 2001-05-01
    body: "Just use rpm -Uvh --force --nodeps *.rpm instead.\nRune"
    author: "Rune Laursen"
  - subject: "how to prove if installation was succesfull"
    date: 2001-05-02
    body: "Hello Rune Laursen,\n\ni made what you wrote:\n\n  rpm -Uvh --force --nodeps kdelibs.rpm \n\nkdelibs-devel-1.2.2-0.rpm\n\nbut i am not sure if it worked for two reasons\n\n a: i installed the rpm with the name kdelibs.rpm instead of kdelibs-1.2.2.rpm (i suppose that makes a difference or doesn\u00b4t it)\n\n b: i got lots of warnings like can not\noverwrite *icons* because it is not empty\n\n\nBut my kde is still running<p>\n\nThanks for any help \n\ngo on kde - you are just great"
    author: "gabriel"
  - subject: "Re: SECURITY: New KDE Libraries Released"
    date: 2001-05-01
    body: "try removing this old version of kdelibs by running:\n\nrpm -e klibs-1.1.2-217\n\nit will probably give some dependency errors, because you may have old programs that use the old kdelibs installed. in this case, i recommend you to remove those packages too."
    author: "Evandro"
  - subject: "Re: SECURITY: New KDE Libraries Released"
    date: 2001-05-01
    body: "Isn't that the version of klibs that came with KDE 1.1?  I'm not sure, but you probably need a 2.x version of KDE for the libraries to be applicable."
    author: "Eric Nicholson"
  - subject: "Doesn't work :-("
    date: 2001-05-01
    body: "I get following compilation errors in khtml/dom:\n\nIn file included from libkhtmldom_la.all_cpp.cpp:4:\nhtml_inline.cpp: In method `void DOM::HTMLAnchorElement::blur()':\nhtml_inline.cpp:204: no matching function for call to `DOM::DocumentImpl::focusN\node ()'\nhtml_inline.cpp:205: no matching function for call to `DOM::DocumentImpl::setFoc\nusNode (int)'\nhtml_inline.cpp: In method `void DOM::HTMLAnchorElement::focus()':\nhtml_inline.cpp:211: no matching function for call to `DOM::DocumentImpl::setFoc\nusNode (DOM::ElementImpl *&)'\nIn file included from libkhtmldom_la.all_cpp.cpp:7:\nhtml_document.cpp: In method `DOM::HTMLDocument::HTMLDocument()':\nhtml_document.cpp:42: cannot allocate an object of type `DOM::HTMLDocumentImpl'\nhtml_document.cpp:42:   since the following virtual functions are abstract:\n../../khtml/xml/dom_nodeimpl.h:110:     class DOM::NodeImpl * DOM::NodeImpl::clo\nneNode(bool, int &)\nhtml_document.cpp: In method `DOM::HTMLDocument::HTMLDocument(KHTMLView *)':\nhtml_document.cpp:50: cannot allocate an object of type `DOM::HTMLDocumentImpl'\nhtml_document.cpp:50:   since type `DOM::HTMLDocumentImpl' has abstract virtual \nfunctions\nIn file included from libkhtmldom_la.all_cpp.cpp:16:\ndom_doc.cpp: In method `DOM::Document::Document()':\ndom_doc.cpp:91: cannot allocate an object of type `DOM::DocumentImpl'\ndom_doc.cpp:91:   since the following virtual functions are abstract:\n../../khtml/xml/dom_nodeimpl.h:110:     class DOM::NodeImpl * DOM::NodeImpl::clo\nneNode(bool, int &)\ndom_doc.cpp: In method `DOM::Document::Document(bool)':\ndom_doc.cpp:100: cannot allocate an object of type `DOM::DocumentImpl'\ndom_doc.cpp:100:   since type `DOM::DocumentImpl' has abstract virtual functions\n\nand so on.\nI'm using gcc version 2.95.2 on FreeBSD 4.1.1.\n\nIs it a known problem ?"
    author: "fura"
  - subject: "Re: Doesn't work :-("
    date: 2001-05-01
    body: "This looks like --enable-final breakage. Remove it from the configure arguments, and try again."
    author: "Theo van Klaveren"
  - subject: "Waste of time"
    date: 2001-05-01
    body: "Why are they wasting their time fixing obscure security bugs when they should be trying to catch up to Ximian GNOME 1.4?"
    author: "ac"
  - subject: "Troll"
    date: 2001-05-01
    body: "Is slashdot.org not enough fun anymore?"
    author: "St"
  - subject: "Re: Troll"
    date: 2001-05-01
    body: "To be honest I really think that /. suffers a lot less than here due to the mod system. The trolls have unfortunatly found a new home. Very good troll though."
    author: "Brian"
  - subject: "Re: Waste of time"
    date: 2001-05-01
    body: "It's not a waste of time to fix the bugs. This is the sort of thing that makes KDE a stable, usable desktop for linux so it can help to enduce windows users to move accross.\n\nJoe user won't change from microsoft if he thinks that the alternative is only trying to look good and not to atain stablility. Even though Windows is not as stable, the open source community has to try to dispell the inaccurate belief, among desktop users, that it is full of bugs. The KDE team are doing a good job of working towards this.  \n\nWell done guys, keep it up!"
    author: "ld6772"
  - subject: "can we see you on stage ???"
    date: 2001-05-01
    body: "*ROTFL*...<br>\nthis is really a good one !!! ;)"
    author: "cylab"
  - subject: "Re: Waste of time"
    date: 2001-05-04
    body: "Must be kidding, kde 2.1.1 overhelms Gnome 1.4..."
    author: "saiyine"
  - subject: "Re: SECURITY: New KDE Libraries Released"
    date: 2001-05-03
    body: "Where are all the Mandrake 8.0 binary packages?? *sob*"
    author: "Bj\u00f6rn Svensson"
  - subject: "Re: SECURITY: New KDE Libraries Released"
    date: 2001-05-07
    body: "Great work! I didn't look at the security\naspect but konqueror works much better now.\nIt was my main browser before but sometimes\ndidn't work with a site. I suppose there\nare still sites that don't work but a quick\ntest for a few hours didn't find any. KDE is\nso cool.\n\nGuess I'll have to try Koffice sometime. \n\nDan Clayton"
    author: "Dan Clayton"
---
As we <A HREF="/988157966/">announced</A> last week, the <A HREF="http://www.kde.org/">KDE Project</A> has released kdelibs-2.1.2 to address a security issue and fix some bugs.  Besides fixing the KDEsu security exploit, particularly joyful to many of you who use <A HREF="http://www.konqueror.org/">Konqueror</A> will be the fix of the "protocol for http://x.y.z died unexpectedly" bug.  "Read more" for the full text of the announcement, including a list of changes.

<!--break-->
<P>&nbsp;</P>
<TT>
<P>DATELINE APRIL 30, 2001</P>
<P>FOR IMMEDIATE RELEASE</P> 
</TT>
<H3 ALIGN="center">SECURITY: New KDE Libraries Released</H3>
<P><STRONG>KDE Adds Security and Bug Fixes to Core Libraries</STRONG></P>
<P>April 30, 2001 (The INTERNET).
The <A HREF="http://www.kde.org/">KDE
Project</A> today announced the release of kdelibs 2.1.2,
a security and bugfix release of the core KDE libraries.  The other
core KDE packages, including kdebase, have not been updated.  The KDE Project
recommends that all KDE users upgrade to kdelibs 2.1.2 and KDE 2.1.1.</P>
<P>
This release provides the following fixes:
<UL>
<LI><EM>Security fixes</EM>:</LI>
<UL>
<LI><EM>KDEsu</EM>.  The KDEsu which shipped with earlier releases of KDE 2
writes a (very) temporary but world-readable file with authentication
information.  A local user can potentially abuse this behavior to gain
access to the X server and, if KDEsu is used to perform tasks that require
root-access, can result in comprimise of the root account.</LI>
</UL>
<LI><EM>Bug fixes</EM>:</LI>
<UL>
<LI><EM>kio_http</EM>.  Fixed problems with "protocol for http://x.y.z died unexpectedly" and with proxy authentication with Konqueror.</LI>
<LI><EM>kparts</EM>.  Fixed crash in KOffice 1.1 when splitting views.</LI>
<LI><EM>khtml</EM>.  Fixed memory leak in Konqueror.  Fixed minor HTML
rendering problems.</LI>
<LI><EM>kcookiejar</EM>.  Fixed minor problems with HTTP cookies.</LI>
<LI><EM>kconfig</EM>.  Fixed problem with leading/trailing spaces in
configuration values.</LI>
<LI><EM>kdebug</EM>.  Fixed memory leak in debug output.</LI>
<LI><EM>klineedit</EM>.   Fixed problem with klineedit emitting "return
pressed" twice.</LI>
</UL>
</UL>
<P>
For more information about the KDE 2.1 series, please see the
<A HREF="http://www.kde.org/announcements/announce-2.1.1.html">KDE 2.1.1
press release</A> and the <A HREF="http://www.kde.org/info/2.1.1.html">KDE
2.1.1 Info Page</A>, which is an evolving FAQ about the latest stable release.
Information on using anti-aliased fonts with KDE is available
<A HREF="http://dot.kde.org/984693709/">here</A>.
</P>
<P>
<H4>Downloading and Compiling kdelibs 2.1.2</H4>
</P>
<P>
The source package for kdelibs 2.1.2 (including a diff file against 2.1.1) is
available for free download at
<A HREF="http://ftp.kde.org/stable/2.1.2/distribution/src/">http://ftp.kde.org/stable/2.1.2/distribution/src/</A>
or in the equivalent directory at one of the many KDE ftp server
<A HREF="http://www.kde.org/mirrors.html">mirrors</A>.  KDE 2.1.2 requires
qt-2.2.3, which is available from
<A HREF="http://www.trolltech.com/">Trolltech</A> at
<A HREF="ftp://ftp.trolltech.com/qt/source/">ftp://ftp.trolltech.com/qt/source/</A>
under the name <A HREF="ftp://ftp.trolltech.com/qt/source/qt-x11-2.2.3.tar.gz">qt-x11-2.2.3.tar.gz</A>,
although
<A HREF="ftp://ftp.trolltech.com/pub/qt/source/qt-x11-2.2.4.tar.gz">qt-2.2.4</A>or
<A HREF="ftp://ftp.trolltech.com/pub/qt/source/qt-x11-2.3.0.tar.gz">qt-2.3.0</A>is recommended (for anti-aliased fonts,
<A HREF="ftp://ftp.trolltech.com/pub/qt/source/qt-x11-2.3.0.tar.gz">qt-2.3.0</A>and <A HREF="ftp://ftp.xfree86.org/pub/XFree86/4.0.3/">XFree 4.0.3</A> or
newer is required).
kdelibs 2.1.2 will not work with versions of Qt older than 2.2.3.
</P>
<P>
For further instructions on compiling and installing KDE, please consult
the <A HREF="http://www.kde.org/install-source.html">installation
instructions</A> and, if you encounter problems, the
<A HREF="http://www.kde.org/compilationfaq.html">compilation FAQ</A>.
</P>
<P>
<H4>Installing Binary Packages</H4>
</P>
<P>
Some distributors choose to provide binary packages of KDE for certain
versions of their distribution.  Some of these binary packages for
kdelibs 2.1.2 will be available for free download under
<A HREF="http://ftp.kde.org/stable/2.1.2/distribution/">http://ftp.kde.org/stable/2.1.2/distribution/</A>
or under the equivalent directory at one of the many KDE ftp server
<A HREF="http://www.kde.org/mirrors.html">mirrors</A>. Please note that the
KDE team is not responsible for these packages as they are provided by third
parties -- typically, but not always, the distributor of the relevant
distribution (if you have any questions, please read the
<A HREF="http://dot.kde.org/986933826/">KDE Binary Packages Policy</A>).
</P>
<P>kdelibs 2.1.2 requires qt-2.2.3, the free version of which is available
from the above locations usually under the name qt-x11-2.2.3, although
qt-2.2.4 or qt-2.3.0 is recommended (for anti-aliased fonts,
qt-2.3.0 and XFree 4.0.3 or newer is required).
KDE 2.1.2 will not work with versions of Qt older than 2.2.3.
<P>
At the time of this release, pre-compiled packages are available for:
</P>
<P>
<UL>
<LI><A HREF="http://www.caldera.com/">Caldera</A> eDesktop 2.4: <A HREF="http://ftp.kde.org/stable/2.1.2/distribution/Caldera/eDesktop-2.4/">i386</A></LI>
<!--
<LI><A HREF="http://www.debian.org/">Debian GNU/Linux</A> stable (2.2):  <A HREF="http://ftp.kde.org/stable/2.1.1/distribution/deb/dists/stable/main/binary-i386/">i386</A> and <A HREF="http://ftp.kde.org/stable/2.1.1/distribution/deb/dists/stable/main/binary-powerpc/">PPC</A>; please also check the <A HREF="http://ftp.kde.org/stable/2.1.1/distribution/deb/dists/stable/main/binary-all/">main</A> directory for common files</LI>
<LI><A HREF="http://www.linux-mandrake.com/en/">Linux-Mandrake</A> 7.2:  <A HREF="http://ftp.kde.org/stable/2.1.1/distribution/rpm/Mandrake/7.2/RPMS/">i586</A></LI>
-->
<LI><A HREF="http://www.redhat.com/">RedHat Linux</A>:  7.1:  <A HREF="http://ftp.kde.org/stable/2.1.2/distribution/RedHat/7.1/i386/">i386</A></LI>
<!--
<UL>
<LI>Wolverine:  <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/RedHat/wolverine/i386/">i386</A>; please also check the <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/RedHat/wolverine/common/">common</A> directory for common files</LI>
<LI>7.0:  <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/RedHat/7.0/i386/">i386</A> and <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/RedHat/7.0/alpha/">Alpha</A>; please also check the <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/RedHat/7.0/common/">common</A> directory for common files</LI>
<LI>6.x:  <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/RedHat/6.x/i386/">i386</A>, <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/RedHat/6.x/alpha/">Alpha</A> and <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/RedHat/6.x/sparc/">Sparc</A>; please also check the <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/RedHat/6.x/common/">common</A> directory for common files</LI>
</UL>
-->
<LI><A HREF="http://www.suse.com/">SuSE Linux</A> (<A HREF="http://ftp.kde.org/stable/2.1.2/distribution/SuSE/README">README</A>):
<UL>
<LI>7.1:  <A HREF="http://ftp.kde.org/stable/2.1.2/distribution/SuSE/i386/7.1/">i386</A>, <A HREF="http://ftp.kde.org/stable/2.1.2/distribution/SuSE/sparc/7.1/">Sparc</A> and <A HREF="http://ftp.kde.org/stable/2.1.2/distribution/SuSE/ppc/7.1/">PPC</A></LI>
 
<LI>7.0:  <A HREF="http://ftp.kde.org/stable/2.1.2/distribution/SuSE/i386/7.0/">i386</A>, <A HREF="http://ftp.kde.org/stable/2.1.2/distribution/SuSE/ppc/7.0/">PPC</A>, and <A HREF="http://ftp.kde.org/stable/2.1.2/distribution/SuSE/s390/">S390</A></LI>
<LI>6.4:  <A HREF="http://ftp.kde.org/stable/2.1.2/distribution/SuSE/i386/6.4/">i386</A></LI>
<LI>6.3:  <A HREF="http://ftp.kde.org/stable/2.1.2/distribution/SuSE/i386/6.3/">i386</A></LI>
</LI>
</UL>
<LI>Tru64 Systems:  <A HREF="http://ftp.kde.org/stable/2.1.2/distribution/Tru64/">4.0e,f,g, or 5.x</A> (<A HREF="http://ftp.kde.org/stable/2.1.2/distribution/Tru64/README.Tru64">README</A>)</LI>
<!--
<LI><A HREF="http://ftp.kde.org/stable/2.1/distribution/tar/FreeBSD/">FreeBSD</A></LI>
-->
</UL>
</P>
Please check the servers periodically for pre-compiled packages for other
distributions.  More binary packages may become available over the
coming days and weeks.
</P>
<P>
<H4>About KDE</H4>
</P>
<P>
KDE is an independent, collaborative project by hundreds of developers
worldwide to create a sophisticated, customizable and stable desktop environment
employing a component-based, network-transparent architecture.
KDE is working proof of the power of the Open Source "Bazaar-style" software
development model to create first-rate technologies on par with
and superior to even the most complex commercial software.
</P>
<P>
KDE and all its components are available for free under
Open Source licenses from the KDE <A HREF="http://ftp.kde.org/">server</A>
and its <A HREF="http://www.kde.org/mirrors.html">mirrors</A> and can
also be obtained on <A HREF="http://www.kde.org/cdrom.html">CD-ROM</A>.
As a result of the dedicated efforts of hundreds of translators,
KDE is available in <A HREF="http://i18n.kde.org/teams/distributed.html">34
languages and dialects</A>.  KDE includes the core KDE libraries, the core
desktop environment (including
<A HREF="http://www.konqueror.org/">Konqueror</A>), developer packages
(including <A HREF="http://www.kdevelop.org/">KDevelop</A>), as well as the
over 100 applications from the other standard base KDE packages
(administration, games, graphics, multimedia, network, PIM and utilities).
</P>
<P>
For more information about KDE, please visit KDE's
<A HREF="http://www.kde.org/whatiskde/">web site</A>.
More information about KDE 2 is available in two
(<A HREF="http://devel-home.kde.org/~granroth/LWE2000/index.html">1</A>,
<A HREF="http://mandrakesoft.com/~david/OSDEM/">2</A>) slideshow
presentations and on
<A HREF="http://www.kde.org/">KDE's web site</A>, including an evolving
<A HREF="http://www.kde.org/info/2.1.html">FAQ</A> to answer questions about
migrating to KDE 2.1 from KDE 1.x,
<A HREF="http://dot.kde.org/984693709/">anti-aliased font tutorials</A>, a
number of
<A HREF="http://www.kde.org/screenshots/kde2shots.html">screenshots</A>, <A HREF="http://developer.kde.org/documentation/kde2arch.html">developer information</A> and
a developer's
<A HREF="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/~checkout~/kdelibs/KDE2PORTING.html">KDE 1 - KDE 2 porting guide</A>.
</P>
<HR NOSHADE SIZE=1 WIDTH="90%" ALIGN="center">
<FONT SIZE=2>
<EM>Trademarks Notices.</EM>
KDE and K Desktop Environment are trademarks of KDE e.V.
Linux is a registered trademark of Linus Torvalds.
Unix is a registered trademark of The Open Group.
Trolltech and Qt are trademarks of Trolltech AS.
All other trademarks and copyrights referred to in this announcement are the property of their respective owners.
<BR>
<HR NOSHADE SIZE=1 WIDTH="90%" ALIGN="center">
<TABLE BORDER=0 CELLPADDING=8 CELLSPACING=0>
<TR><TH COLSPAN=2 ALIGN="left">
Press Contacts:
</TH></TR>
<TR VALIGN="top"><TD ALIGN="right" NOWRAP>
United&nbsp;States:
</TD><TD NOWRAP>
Kurt Granroth<BR>
granroth@kde.org<BR>
(1) 480 732 1752<BR>&nbsp;<BR>
Andreas Pour<BR>
pour@kde.org<BR>
(1) 917 312 3122
</TD></TR>
<TR VALIGN="top"><TD ALIGN="right" NOWRAP>
Europe (French and English):
</TD><TD NOWRAP>
David Faure<BR>
faure@kde.org<BR>
(44) 1225 837409
</TD></TR>
<TR VALIGN="top"><TD ALIGN="right" NOWRAP>
Europe (English and German):
</TD><TD NOWRAP>
Martin Konold<BR>
konold@kde.org<BR>
(49) 179 2252249
</TD></TR>
</TABLE>
