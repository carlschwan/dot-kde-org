---
title: "Interview:  Trolltech's President Eirik Eng"
date:    2001-09-24
authors:
  - "Dre"
slug:    interview-trolltechs-president-eirik-eng
comments:
  - subject: "CROSSPLATFORM COMPONENT MODEL"
    date: 2001-09-24
    body: "When ?"
    author: "Krame"
  - subject: "Re: Crossplatform component model"
    date: 2001-09-24
    body: "When you go download the QT3 beta."
    author: "not me"
  - subject: "QT and Via Voice"
    date: 2001-09-24
    body: "What is happening with integrating speech recognition engine Via Voice with QT?"
    author: "John Kintree"
  - subject: "Re: QT and Via Voice"
    date: 2001-09-24
    body: "Via Voice is closed. Sooo, no-one used it, as far as I know. The idea was, i think, people making closed source KDE apps could license and use that."
    author: "Carbon"
  - subject: "GPL version of QT for MacOS X?"
    date: 2001-09-24
    body: "Will there be a GPL version of the QT port to MacOS X, that would enable a user to run any GPL-ed pure-QT app on MacOS X as native?"
    author: "Pedro Ziviani"
  - subject: "Re: GPL version of QT for MacOS X?"
    date: 2001-09-25
    body: "take a wild guess !\n\nmy guess is NO \n\nif you want to run GPL software on darwin why dont you run X ?\nthen you can run GPL QT \n\nsearch for XonX \n\nregards\n\njohn jones"
    author: "john jones"
  - subject: "Re: GPL version of QT for MacOS X?"
    date: 2001-09-28
    body: "Though I don't know anything about porting QT for X Windows to Mac, there are some planning to do this for Windows. Check out:\nhttp://kde-cygwin.sourceforge.net/faq.html\nApparently there is not a whole lot to port - QT obviously does not want to have a lot of platform specific stuff.\n\nCurrently kde-cygwin requires X Windows, but as you can see from the FAQ they are planning to port to native windows."
    author: "Ian M"
  - subject: "Re: GPL version of QT for MacOS X?"
    date: 2001-09-25
    body: "My guess is no.  I believe Qt/Mac will be treated exactly like Qt/Windows.  Here's my detective work as to why:\n\n1) Check out this link: http://www.trolltech.com/company/announce/edumac.html\nApparently, Qt/Mac will be free for academic use.  This would only be relevent if there is no GPL Qt/Mac.\n\n2) See the Non-commercial Qt FAQ: http://www.trolltech.com/developer/faq/noncomm.html\nIt states: \"We released Qt/X11 under those licenses, because it runs as major component on totally Free operating systems...\" and \"Once Microsoft Windows is completely open source, we will reconsider.\"  Since MacOS X is not fully open source, it will be treated like Windows.\n\n3) The Aqua look is heavily guarded by Apple.  If Qt/Mac was GPL, then the Aqua QStyle would be too.  This would go against Apple policy.\n\nThere you have it.\n\nWe might see a \"non-commercial\" version, like they did with Qt/Windows."
    author: "Justin"
  - subject: "qt free for windows"
    date: 2001-09-24
    body: "Why are there no free qt3 beta snapshots for use with MS Visual C++ and why doesn't the qt free 2.3 version work with the author's edition of MSVC++? Is this a limitation from ms or trolltech?"
    author: "me"
  - subject: "Re: qt free for windows"
    date: 2004-01-10
    body: "Because, unfortunately, Qt for windows is ruined by greed.\n\nNew, poor developers like me are stuck with mfc or java on windows.  It is pure torture.  I want out, but I want my windows friends to be able to use my programs.  I hate this economy.  \nBy the way, more open source developers for windows should let Qt know they exist.  Go on, start protesting!  Don't make me steal $2490 from my place of work!"
    author: "Shiloh"
  - subject: "Re: qt free for windows"
    date: 2004-01-10
    body: "Why can't we just assume that a developer under Windows already paid a lot for the Windows license and the license for whatever IDE he uses, and why should he then still be too \"poor\" to do just the same with Qt?"
    author: "Datschge"
  - subject: "Re: qt free for windows"
    date: 2004-07-31
    body: "I didn't pay for a Windows licence and I didn't pay for a MS Visual Studio Licence - they are free for me as I have access to MSDN Academic Alliance. I want to develop free Windows software and share it with other students of my university, but I can't afford to pay thousands of dollars to Trolltech. Why don't the rules of GPL make sense for Trolltech in the Windows environment?\n\nAnd even if I had to pay for the licences, WindowsXP costs me about $100, VC++ is about $150 - how does Trolltech justify ten times this amount just for a GUI?"
    author: "Markus Erlacher"
  - subject: "Re: qt free for windows"
    date: 2004-07-31
    body: "Microsoft let users pay per system, Trolltech let developers pay per supported platform. If you want a development platform without paying any license costs you can easily settle with an all GPL'ed system. MSDN Academic Alliance with its annual cost of US$799 per faculty/department per year isn't that cheap, even more so considering that their EULA excludes any use except educational course offered by the participating institute and related non-commercial research projects.\n\nIf your concern is all the academic area anyway why don't you just use the GPL'ed Qt? Efforts to port it to Windows are there, you could just support them if you think that's what you really need. You could also ask directly at Trolltech if they would be willing to just carbon copy that MSDN Academic Alliance thing including their EULA if that's what is needed to make you happy."
    author: "Datschge"
  - subject: "Re: qt free for windows"
    date: 2004-07-31
    body: "This is exactly what we do: non-commercial research in our institute, but based on a Windows platform. \n\nTrolltech does not provide a GPL'ed Qt for Windows if I see this correctly. And we don't have the time and the money to invest in another platform or port it ourselves.\nBut I found a Qt/Win V2.3 on the Trolltech pages, so I think we just will work with this version, as it is free for Windows and not just an evaluation version. \nBut I think anyhow, that this is off-topic on a KDE forum. I just got here looking in Google for GUI development tools. Another GPL GUI Toolkit I found is Fox, and if Qt 2.3 does not satisfy us, we'll give them a chance."
    author: "Markus Erlacher"
  - subject: "Re: qt free for windows"
    date: 2004-07-31
    body: "Trolltech has a \"Qt Educational License Request Form\" at http://www.trolltech.com/forms/eduprogram.html stating \"The educational license is to be used on school hardware and premises for educational purposes. If you are a professor/teacher and want to let your students use Qt/Windows in your class or course, just fill in the application form below.\" I guess this is what you were looking for?\n\nOther than that there's also a more recent non-commercial version of Qt/Win, version 3.2, is included on the book \"C++ GUI Programming with Qt 3\", see http://www.trolltech.com/download/qt/noncomm.html"
    author: "Datschge"
  - subject: "Re: qt free for windows"
    date: 2005-11-23
    body: "If you have link free download qt you can share it for me,ok?"
    author: "Nhoc"
  - subject: "Re: qt free for windows"
    date: 2005-11-23
    body: "The obvious location is the download section of Trolltech, the maker of Qt: \nhttp://www.trolltech.com/download/qt/windows.html\n\nMake sure you read the page before downloading. \n\n"
    author: "cm"
  - subject: "what are the compiler issues ?"
    date: 2001-09-25
    body: "the title says it all \n\nI am aware that gcc 3.0 wont compile KDE but what are the problems ?\n\nhave anyone tried 3.01 ?\n\nregards\n\njohn jones"
    author: "john jones"
  - subject: "Re: what are the compiler issues ?"
    date: 2001-09-25
    body: "IIRC the problems are in the handling of virtual inheritence and are not addressed by 3.01.\nHave a look in the mail archives for more info.\n\nRich."
    author: "Richard Moore"
  - subject: "Re: what are the compiler issues ?"
    date: 2001-09-25
    body: "I compiled kdesupport, kdelibs and kdebase and it worked, except for artsd (no sound)."
    author: "Evandro"
  - subject: "Re: what are the compiler issues ?"
    date: 2001-09-26
    body: "Yes, aRts is the part of KDE that won't compile.  I think there are subtle bugs in other programs that will cause them to crash, though (KNode?)."
    author: "not me"
  - subject: "qt3.x and binary compatability"
    date: 2001-09-25
    body: "is it true that after the move to qt3.0 there will be no more binary breakage from 3-> 4-> x.y?"
    author: "Jeremy Petzold"
  - subject: "Re: qt3.x and binary compatability"
    date: 2001-09-27
    body: "4.0 will break binary compatibility with 3.x. Many things can be done in a binary-compatible way, but over time the pressure builds and we really have to break binary compatibility. This is a non-issue with major releases since the linker ensures the right libraries are used (like libc5 and libc6 on your Linux machine).\n\nSource compatibility is something else. The 1.x to 2.0 change broken a lot of code (but in easy-to-fix ways). The 2.x to 3.0 change will break very little code. So you can expect that between 3.x and 4.0, source compatibility will be even higher.\n\nBut hey, let's worry about 3.0 for now, not 4.0!!\n\n--\nWarwick"
    author: "Warwick Allison"
  - subject: "MS VC++ requirement"
    date: 2001-09-26
    body: "I would like to know: Why does the Microsoft Windows version of Qt state that it requires Microsoft Visual C++ 6? Is this a technological reason or a market driven reason? Perhaps am I missing something here?\n\nDo the trolls have any plans to move toward other compilers for the windows platform? What about gcc and cygwin? I could convince my employer to pay $400 more for Qt instead of VC++/ATL/MFC but not on top of the price of Visual Studio. They say, \"If we have to buy VC anyways, just write it in MFC or ATL.\"\n\n-pos"
    author: "pos"
  - subject: "Re: MS VC++ requirement"
    date: 2001-09-27
    body: "Borland C++ is also supported (http://www.trolltech.com/products/platforms/bcpp.html)\n\nAs to Microsoft bundling scams, try to convince your boss that she'll save far more than $2000 worth of your development time in just a few weeks of using Qt.\n\nTrolltech had a cool pamphlet at LinuxWorld that showed the different amount of code required with each platform. It should be on trolltech.com somewhere."
    author: "Warwick Allison"
  - subject: "Re: MS VC++ requirement"
    date: 2002-01-12
    body: "I have 1.6 years experience. I am looking for good job for VC++.\nI have submited my resue her.\n\nthanking you.\n\nindrajeet"
    author: "Indrajeet raval"
  - subject: "Re: MS VC++ requirement"
    date: 2002-04-02
    body: "Hi ,\n\nWe have a position for senior vc++ Software Engineer with 2/3 yrs experience.\n\nWith VC++/Matlab/COM/ATL with good database knowledge of Sql Server.\n\nkindly mail me your resume with expectation on abhay@camo.com.\n\nNote: Position is in banglore.\n\nThanks\n\nAbhay Kapale\n"
    author: "Abhay Kapale"
  - subject: "Re: MS VC++ requirement"
    date: 2003-06-05
    body: "sir,\n\n I am a trainee in indian institute of remote sensing, dehradun.I have completed my B.Tech in computer science and PGDiploma in Remote Sensing and GIS.\n I have done a project using VC++  and MATLAB named \"Automated image segmentation and feature extraction \"I have a good knowledge in VC++(Com/ATL/MFC)and SQL Server.Please inform me whether there is any opportunity there for me?Iam eagerly waiting for your reply.\n\nYours sincerely,\n\nSankarsan padhy."
    author: "sankarsan padhy"
  - subject: "questions asked by?"
    date: 2001-09-28
    body: "Were all the questions asked by philippe fremy? \nOr what?"
    author: "rob"
---
<a href="mailto:pfremy@chez.com">Philippe Fremy</a> has conducted the
first part of his <a href="http://dot.kde.org/994553595/">interview</a> with
<a href="http://www.trolltech.com/">Trolltech</a>'s President Eirik Eng.
Read about Trolltech's revenues, employees, partnerships, licensing, origins
and more below.


<!--break-->
<p>&nbsp;</p>
<p>
<h3>Trolltech Questions</h3>
</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;<strong>Philippe Fremy (Q)</strong>.  <em>We are very curious about Trolltech's business model.
Could you detail us your various sources of income:  Qt/Embedded, Windows
and Unix licenses?</em>
</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;<strong>Eirik Eng (A)</strong>.  We typically generate 50% of our income from Duo
Packs -- these are licenses to develop on Windows and Unix.  We also
generate around 20% of sales from Qt/Embedded, and the rest is from
single platform Qt licenses.
</p>
<br>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;<strong>Q</strong>.  <em>How are things financially with the economy
being in the state it is?</em>
</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;<strong>A</strong>.  Things are fine.  We have continued to grow at more
or less the rate we had planned.  We're more or less on budget, and we're
not too worried about making it through the downturn.
</p>
<br>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;<strong>Q</strong>.  <em>Do you feel you are losing money because of
GPL Qt?</em>
</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;<strong>A</strong>.  We had the same rate of growth before the change
in license. In the very
early years, we were afraid that if we GPLed Qt, someone with more
development muscle would create a hostile fork of Qt and, in a sense,
take over our only product. You just don't take any chances with your
only bread and butter.
</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;So, as soon as we felt that we could outrun anyone trying to make a
hostile fork, we switched to using the GPL. The switch did not affect
our customers, and it had very little practical impact on the Open
Source community. But the symbolic effect was astronomical. My inbox was
flooded with &quot;thank you&quot; e-mails for quite some time.
</p>
<br>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;<strong>Q</strong>.  <em>How many employees do you have
at the moment,
where and how many in each department?</em>
</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;<strong>A</strong>.  We currently have around 65 employees (I think--I am losing track now).
There are 35 in engineering, 4 in product management, 4 in
Marketing/Communications, 6 in sales, and the rest in management and
administration.  We currently have three offices:  Our head office in
Oslo, Norway; our embedded development center in Brisbane, Australia; and
our sales and marketing office in Silicon Valley, California.
</p>
<br>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;<strong>Q</strong>.  <em>Did the creation of the KDE project change
something for you? Do you get any revenue from KDE?</em>
</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;<strong>A</strong>.  We don't generate income from KDE directly, but KDE has certainly been
instrumental in our success.  Through KDE, many of our current customers
learned about us.  Many engineers hack on KDE in the evening, and then
go into work in the morning and typically work as a developer.  If they
like Qt, they ask their boss if they can buy it.
</p>
<br>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;<strong>Q</strong>.  <em>Do you have any relations with
the distribution
companies? Does
theKompany and Trolltech have contacts with each other?  What about the
financial issue (does theKompany pay for using Qt?) Does theKompany ask
for new features?</em>
</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;<strong>A</strong>.  We don't have any formal relationships with the
desktop/server
distribution companies, but we have frequent contact with all the major
ones.  In the past we have worked with Caldera (created their Lizard
installation package for them). And, as you might know, SuSE just
published an 1100+ book on Qt in German.
</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;We do work closely with several key embedded Linux distributors, with
partnerships that include reselling Qt/Embedded and Qt Palmtop.
</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;As for theKompany, we work with them, although we don't
have any formal
agreements, except on the distribution of BlackAdder.
</p>
<br>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;<strong>Q</strong>.  <em>How many partnerships do you have?
What is their goal (redistributing Qt license under a special condition)?
Do they work?</em>
</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;<strong>A</strong>.  We have several goals when it comes to partnering.
With Qt Desktop, our
goal is to help it ease into new markets, therefore a partnership in
this area would involve the integration of new features, via third
parties, to make Qt more attractive to certain markets.
</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;With Qt/Embedded, we immediately found that in the embedded space,
manufacturers wanted reassurances that things will work well together.
So we work with embedded Linux distributors and various other suppliers
to make the decision of going with Linux as easy as possible.
</p>
<br>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;<strong>Q</strong>.  <em>Do you have any big deals?
We all know about Opera and Kylix, but do
you have some big company using Qt massively?</em>
</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;<strong>A</strong>.  We do have several big deals, many
of which we
don't publicize.  Several
large Electronic Design Automation (EDA) companies have standardized on
Qt, such as Agilent and Synopsis.  We have many customers in the 3D
graphics industry, where they use Qt to build design tools to be used by
artists.  Aerospace, manufacturing, and medical imaging are all
industries where Qt is doing quite well.  I think one of the coolest is
the fact that the European Space Agency is using a Qt-based program for
satellite simulations.
</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;At the recent LinuxWorld in San Francisco, we were told
that several
companies have started using &quot;Built with Qt technology&quot; as a marketingpoint-without us asking them to!
</p>
<br>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;<strong>Q</strong>.  <em>What inspired Trolltech to start building Qt,
or put another way, what led to the forming of Trolltech?
What feature had the toolkit at the beginning?</em>
</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;<strong>A</strong>.  Haavard Nord, Trolltech's CEO,
and I had been working together with
various cross-platform GUI tools back in 1991. We were both very
disappointed in their quality and were sure we could do it much better.
Haavard went on to write his Masters thesis on GUI design, while I wrote
a C++ GUI toolkit for a Norwegian company. In 1993 he called me up and
suggested that we should join forces and use our experience in GUI
design to write the toolkit that would be the king of toolkits. We had
no customers, no funding and a lot of enthusiasm. Luckily we were both
married to wives who had full-time jobs. We used some savings to rent a
small office and hacked away for a year while our wives fed and cared
for us. It was a great period, we had the luxury of working undisturbed
with something that we were really passionate about. We had seen the
pain of traditional GUI programming, and our goal was to make it
pleasant to program GUIs. I think we have succeeded in that.
</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;The features that Qt 1.0 came with were pretty limited.
 E.g we did not
have a multiline text widget! We recently hired the guy who bought the
first Qt licenses--he occasionally jokes that one of our big selling
features was that Qt could rotate text.
</p>
<br>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;<strong>Q</strong>.  <em>Just curious, how did your company
arrive at the name &quot;Trolltech&quot;?  Do you mind the resulting
nickname of &quot;the trolls&quot; at all?</em>
</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;<strong>A</strong>.  Haavard and I were thinking about what to call
the company for a while.  One night, Haavard had a dream where the company
was called &quot;Trolltech&quot;.  In the dream his wife hated the name,
so he asked her what she thought when he woke up.  She liked it.
The rest is history.
</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;As to being referred to as &quot;Trolls&quot;, we don't
mind.  Trolls are
something very Scandinavian, and in a way provides a title for all of us
to share.  It's kind of a bonding thing.
</p>
<br>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;<strong>Q</strong>.  <em>Who owns Trolltech AS?</em>
</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;<strong>A</strong>.  We do.  That is to say, the employees own 70%
of Trolltech. We have
created a charitable foundation that owns 5% of the company. We have
also received some investments from Borland, The Canopy Group (Lineo,
Caldera, etc), Northzone Ventures, Teknoinvest, and Orkla.
</p>
<br>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;<strong>Q</strong>.  <em>Is Trolltech planning to go public,
with their stock, anytime soon?</em>
</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;<strong>A</strong>.  While we don't have a specific timeline,
going public is certainly an objective of Trolltech.
</p>
<br>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;<strong>Q</strong>.  <em>How does one go about getting a job at TT?
What are the qualifications that you desire for programmers and do they
need degrees in computer science/engineering?  Do you hire based on reviewing
contributions to KDE?</em>
</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;<strong>A</strong>.  We hire based on one thing--how well you can code.
We briefly skim a CV, and then dig deep into the code to see what the
applicant can do.  If we like what we see, we usually fly
the applicant in to Oslo so that they can meet the rest of the team.
</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;We only hire the best, and I mean <em>the</em> best.  Our engineers do
brilliant things.  I am constantly amazed at what they do, and I like to
consider myself to be a pretty good hacker (at least I used to be,
before my hair got too pointy).
</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;When it comes to hiring non-technical people, we usually make them do a
test.  When we hire PR people, for instance, we tell them about an
upcoming press release we have in the works, and then make them write
it.  The hiring process is a rigorous one.
</p>&nbsp;<p>
</p>
<p>
<h3>Technical questions</h3>
</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;<strong>Q</strong>.  <em>What makes Qt so good?</em>
</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;<strong>A</strong>.  Three things:
</p>
<ul>
<li>An excellent team of engineers;</li>
<li>A solid base with which to work from (we could have pushed Qt 1.0 out
in a quarter of the time, but we took our time to build a base that we
could continue to expand); and</li>
<li>Snapshot releases every 24 hours coupled with a committed and
enthusiastic user base that gives us constant feedback.</li>
</ul>
<br>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;<strong>Q</strong>.  <em>What OS/distro/desktop do the Qt developers use?</em>
</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;<strong>A</strong>.  There are a variety of environments in the office.
The developers
typically use Linux, although several use Windows.  There are several
different Linux distros kicking around the office, but only one
desktop--KDE.
</p>
<br>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;<strong>Q</strong>.  <em>There are many Qt advocates here, but, when
pushing it in our companies, we are often confronted with Java, MFC.
Do you have any very sensible argument to help us convince management
to use Qt instead of any other toolkit? Do you have also sensible
arguments for Gtk?</em>
</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;<strong>A</strong>.  We do have several arguments for each.
<ul>
<li>Java: Given the nature of Java, it can't run natively on any given
platform.  There are speed and memory issues associated with Java that
Qt does not have.</li>
<li>MFC:  Two main advantages.  Qt is cross-platform, and, given our
customers/users feedback, much more intuitive and easy to code with.</li>
<li>Gtk: Although Gtk is on Windows as well as X, Qt has a far better cross
platform implementation.  Qt is written in C++, instead of C, has a
company standing behind it, and needs much less code to write the same
app.</li>
</ul>
<br>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;<strong>Q</strong>.  <em>What is Trolltech's position on the full C++
standard? E.g., do you advocate the use of all aspects of the STL,
and if so, are future versions of QT going to be based around the STL?</em>
</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;<strong>A</strong>.  There are still some compiler and memory
usage issues with STL. Many of our users use STL together with Qt
without any problems, there really aren't that many parts of the Qt
API that require conversions to be done.</em>
</p>
<br>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;<strong>Q</strong>.  <em>Further to this, are there plans to get the
signal/slot mechanism of moc into the C++ standard, or do you feel
a preprocessor is a fully satisfactory solution?</em>
</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;<strong>A</strong>.  We are not working actively to make this happen,
although one of our developers has talked to Bjarne Stroustrup about
this possibility.
</p>
<br>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;<strong>Q</strong>.  <em>Displays are getting bigger, and screen elements
are getting lost - e.g., icons must now be produced in multiple sizes.
People also want to use things on smaller screens - e.g., the ipaq.
Are there any plans to make Qt pixel independent, like Fresco or Berlin?</em>
</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;<strong>A</strong>.  No, we currently do not have plans
for this.
</p>
<br>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;<strong>Q</strong>.  <em>When Qt comes to Mac will Linux and Windows
users be able to use the Aqua theme?</em>
</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;<strong>A</strong>.  No, they will not be able to.  Apple is very
protective of the Aqua design, so we will not be implementing it on
other platforms.  Apple has offered their help to promote Qt/Mac, and
we don't feel that going against their wishes will help them or us.
</p>
<br>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;<strong>Q</strong>.  <em>What will be in Qt4?</em>
</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;<strong>A</strong>.  We don't know yet.  We have a few really cool
things we're working on
right now, but we don't know if they are going into later versions of
the 3.x series, or if we'll wait until 4.0.  You will have to just wait
and see.
</p>

