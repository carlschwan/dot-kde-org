---
title: "AtheOS Adopts KHTML For Browser"
date:    2001-08-01
authors:
  - "numanee"
slug:    atheos-adopts-khtml-browser
comments:
  - subject: "AtheOS has nice fonts"
    date: 2001-08-01
    body: "Is this AA?  Looks very nice."
    author: "ac"
  - subject: "Re: AtheOS has nice fonts"
    date: 2001-08-01
    body: "Of course it's AA, that's why it looks all fuzzy and unclear. :)"
    author: "ac2"
  - subject: "Re: AtheOS has nice fonts"
    date: 2001-08-02
    body: "You can turn off AA for small sizes\nI only enable it for italics,bold at all sizes\nand large normal weight fonts\nand it looks great!"
    author: "ac3"
  - subject: "Re: AtheOS has nice fonts"
    date: 2001-08-02
    body: "Can you explain how to do this? You do mean in KDE, right?"
    author: "Peter Backlund"
  - subject: "Re: AtheOS has nice fonts"
    date: 2001-08-02
    body: "<a href=\"http://dot.kde.org/989808269/\">http://dot.kde.org/989808269/</a>\n<br><Br>\nenjoy,<br>\nkervel"
    author: "ik"
  - subject: "Silly name"
    date: 2001-08-01
    body: "Why do they call it Abrowse?\nAt least they could have named it Kbrowse ;-)"
    author: "reihal"
  - subject: "Re: Silly name"
    date: 2001-08-02
    body: "Shoulda called it \"ABrowser\" ... get it? ;-)"
    author: "Spelling Guy"
  - subject: "Re: Silly name"
    date: 2001-08-02
    body: "No...what do you mean? Is it a joke? I don't get it...oh wait, you mean like \"a browser\"? Hehe yeah that was quite funny :-)"
    author: "Peter Backlund"
  - subject: "Re: Silly name"
    date: 2004-01-12
    body: "please send me an msn address name\n\nthanks"
    author: "yasmine"
  - subject: "Re: Silly name"
    date: 2001-08-04
    body: "The \"A\" is meant for \"AtheOS\". Simple isn't it?"
    author: "Rajan Rishyakaran"
  - subject: "Re: Silly name"
    date: 2001-08-08
    body: "I also kind of see it as a tribute to BeOS.  In the beginning, most of the apps for BeOS started with a B or Be.  Kind of cool either way."
    author: "BOSC_Maverick"
  - subject: "Re: Silly name"
    date: 2001-08-08
    body: "What about NetPositive? What about the \"K\" being infront of every KDE app, Windows in front of almost everything in windows and G/ Gnome in front of all Gnome apps. Anyway, i don't really care about the name, i care more about the features etc."
    author: "Rajan Rishyakaran"
  - subject: "great"
    date: 2001-08-01
    body: "It's good to see this, but I could not understand if it uses qt, for what I can tell not.\nSo if it's true, is it possible to port khtml to win/mac etc? Because there will be not restrictions because of QT license."
    author: "Iuri Fiedoruk"
  - subject: "Re: great"
    date: 2001-08-01
    body: "It probably uses Qt-embedded, since Atheos doesn't use X-Window. \n\nQt-embedded doesn't need X but directly the frame-buffer, and also Konqueror as been already ported onto Qt-Embedded.\n\nYes, it should be possible to port  khtml to win/mac, etc."
    author: "aegir"
  - subject: "Re: great"
    date: 2001-08-02
    body: "NO! AtheOS has not any of QT!(There are some qt (qt's tools part)port in AtheOS early)"
    author: "ffxz"
  - subject: "Re: great"
    date: 2001-08-02
    body: "So I'm very impressed..."
    author: "aegir"
  - subject: "Re: great"
    date: 2001-08-01
    body: "IIRC, Kurt Skauen (main devel of AtheOS) wrapped Qt calls so they'd actually call AtheOS's own graphic lib.\n\nNot a simple task, if you ask me..."
    author: "Christian Lavoie"
  - subject: "Re: great"
    date: 2001-08-01
    body: "Wow, that should bring  a whole slew of quality apps to them then."
    author: "ac"
  - subject: "Re: great"
    date: 2001-08-02
    body: "What I did was to first remove everything that didn't compile from Qt-X11 (ie. all the GUI classes) and kept all the classes that did compiled (most of the tool classes like containers, strings, filesystem API wrappers, etc etc). Then I started compiling KHTML. Whenever I got a \"<qxxxxx.h> not found\" error I added a dummy header file with an empty class and when I got Qxxxxx::blabla() not defined I added a dummy member to that class. After a while I had a totally useless but compiling and linking version of KHTML. Then I started to fill in the functions. Most of them ended as very small functions that just called into the native AtheOS GUI toolkit. This aproach made it possible to keep much of the KHTML code unchanged while still get as close to \"native AtheOS\" as possible (no ugly X11 in framebuffer hack or something like that). It also means that the Qt version I ended with only support exactly the classes and functions used by KHTML so it is not a full port. Also in a very few cases where it would have been a lot of work to emulate the Qt semantics of something but very easy to modify KHTML to use a more native-AtheOS method I did modify KHTML instead. I did try to modify as little as possible though to make it as simple as possible to upgrade to newer versions of KHTML\n\nMany thanks to the KHTML team for making such a great piece of software!! Most OSS sources are a total mess but KHTML was very enjoyable to work with."
    author: "Kurt Skauen"
  - subject: "Re: great"
    date: 2001-08-02
    body: "I haven't tried AtheOS yet, but I get the impression that it will be very big in a few years. There aren't many people like Kurt who have the the skills to code a complete OS almost from scratch. Much kudos Kurt!"
    author: "Julian Regel"
  - subject: "Old, lovely Amiga days ..."
    date: 2001-08-01
    body: "I remember the old Amiga days. window manager (workbench I mean), icon styles all look like it. \n\nI *really* love this !!!!!"
    author: "Erhan Bilgili"
  - subject: "Re: Old, lovely Amiga days ..."
    date: 2001-08-02
    body: "Yes - for sentimental reasons I use to boot my good old Amiga 1200 once a year, and still it feels much faster and responsive than for instance this dual PII 450Mhz with 256 meg RAM and KDE 2.\n\nOf course, the comparison is not really fair : the Amiga has an *accelerator card* with four megs of *fast ram* and a *double speed* (28Mhz) CPU. This gives it a 32% better MHz/MB ratio than the hog I'm using right now...\n\nHave a great day!"
    author: "Johan"
  - subject: "Re: Old, lovely Amiga days ..."
    date: 2001-08-02
    body: "Hmm, the name, the icons, the iconeditor in one of the Screenshots.... It really looks like an amiga with some graphics improvements\n\nSteve"
    author: "Steve"
  - subject: "The DotKDE Effect"
    date: 2001-08-01
    body: "the site is still down."
    author: "caatje"
  - subject: "Re: The DotKDE Effect"
    date: 2001-08-01
    body: "I thought of mirroring more of the site but it's quite big, so I gave up on that. At least I included the relevant news bit. You can still check out the screenshot, for what it's worth."
    author: "Navindra Umanee"
  - subject: "Re: The DotKDE Effect"
    date: 2001-08-02
    body: "Still seems to be down(or at least extremely slow).  Still since the server runs Atheos(AFAIK), it's not doing to badly for an OS that is only at 0.3.5!\n\nFor(a little) more information and to download it, try:\nhttp://sourceforge.net/projects/atheos/"
    author: "Rob"
  - subject: "Re: The DotKDE Effect"
    date: 2001-08-02
    body: "The server is now back up. Unfortunately a filesystem got corrupted last night and the server went down :( I noticed that it was gone and tried to ping some of the other machines on our network from home and none of them responded so I concluded that the entire network was down and that some network dude would make it all work again. When I got to work this morning the network was indeed working but the server was dead and I had to rebuild a filesystem to make it work again."
    author: "Kurt Skauen"
  - subject: "AtheOS KDE Port?"
    date: 2001-08-01
    body: "Is there a chance that eventually, KDE would be ported to AtheOS?  What would it take to do this?"
    author: "Steve Hunt"
  - subject: "Re: AtheOS KDE Port?"
    date: 2001-08-01
    body: "There's a chance but I don't see it happening. AtheOS has it's own desktop already.\n\nWhat would take? One developer or two."
    author: "Evandro"
  - subject: "Re: AtheOS KDE Port?"
    date: 2001-08-03
    body: "Not really.  It might be possible, but it would go against the philosophy of AtheOS, which is to make a completely new, \"clean\" OS from scratch, including all the GUI stuff.  A port of KDE wouldn't use AtheOS to its full potential unless KDE was modified to support it, which isn't likely since KDE needs to be cross-platform.  The AtheOS way is just to implement everything themselves (perhaps borrowing some code like KHTML where appropriate, but not copying an entire interface)\n\nFor example, AtheOS has file attributes like BeOS.  To really have file attributes work well, they must be supported by all applications.  No KDE apps use file attributes.  Konqueror wouldn't allow you to see or change them.  An AtheOS port of KDE would have tons of problems like this."
    author: "not me"
  - subject: "Re: AtheOS KDE Port?"
    date: 2001-08-03
    body: "I've looked at the AtheOS GUI a little bit.  I haven't actually used it, but from what I can see, it is around the maturity of KDE 1.X.  Is there a lot of development of it, or is it moving slowly?"
    author: "Steve Hunt"
  - subject: "Re: AtheOS KDE Port?"
    date: 2001-08-04
    body: "> Is there a lot of development of it, or is it moving slowly?\n\nIs this a joke?\nThere is a completely new Kernel (which actually works, has some drivers and has a pretty neat modular structure, no need for recompiling if you just need a new driver), a completely (kinda) new 64 bit journaling file system and a completely new Desktop and a GUI API (already featuring AA fonts, etc) which is almost as matur as KDE 1.0 (as you state). \nThis all was done by ONE person. No patches, no shared work, just ONE person (not because noone wanted to help, but because this person prefers to code it's own ideas atm) and you really ask, if this is moving slowly?? :)"
    author: "Spark"
  - subject: "Re: AtheOS KDE Port?"
    date: 2001-08-04
    body: "Easy there, bud.  I just haven't followed AtheOS development very closely.  I honestly had no idea how many people were working on the project, and how fast it was going.  However, your recent \"post\" didn't really answer my question on how fast development was going.\n\nBTW: I hate to say this, but I don't think that a kernel that can add new drivers is special.  In my opinion, it is necessary.\n\n-Steve"
    author: "Steve Hunt"
  - subject: "Re: AtheOS KDE Port?"
    date: 2001-08-05
    body: "> Easy there, bud.\n\nDidn't you notice my smiley? :)\n\n> However, your recent \"post\" didn't really answer my question on how fast development was going.\n\nSo how should I answer this one to your satisfaction? Development is going at 200 mph? ;) Seriously, I didn't follow it for a long time but I get the impression, that everything is coming along very nicely and fast. The API is already usable, now it needs a lot of driver developers to get some more hardware working. :)\nThe IDE driver will be especially important, cause it will also allow easy cdrom installations (atm you have to use bootdisks and such).\n\n> BTW: I hate to say this, but I don't think that a kernel that can add new drivers is special. In my opinion, it is necessary.\n\nI would agree. That's why I'm not very satisfied with Linux anymore and looking for an alternative. HURD is promising, but AtheOS seems to be much further in development."
    author: "Spark"
  - subject: "Re: AtheOS KDE Port?"
    date: 2001-08-05
    body: "> That's why I'm not very satisfied with Linux anymore\n\nBefore someone gets me wrong:\nIt's because I can't install new binary drivers for my special kernel, not because it's impossible to add drivers. :) I just think that it's not very convenient to have to compile a new kernel, if I want to add a new closed source driver. That just sucks."
    author: "Spark"
  - subject: "Re: AtheOS KDE Port?"
    date: 2001-08-06
    body: "What on earth for?  Koffice would be wonderful, ditto the other KDE apps, but Atheos has its own windowing system.  Making it X-compatible would make much better sense.\n\n(just my thoughts - take them with as big a grain of salt as your like. :)"
    author: "Wesley Parish"
  - subject: "Looks good"
    date: 2001-08-02
    body: "The desktop / windowmanager has a very clean look to it. I like it."
    author: "Jeff"
  - subject: "Re: Looks good"
    date: 2001-08-02
    body: "You can make KDE look like this within about ten minutes, no problem."
    author: "grovel"
  - subject: "pronunciation?"
    date: 2001-08-03
    body: "what is the correct/official way to pronounce AtheOS?"
    author: "D"
  - subject: "Re: pronunciation?"
    date: 2001-08-03
    body: "> what is the correct/official way to pronounce AtheOS?\n\nSpace Meat.  \n\nWith a silent \"T\".\n\n--\nEvan \"Mr. Sleep Deprivation\" E."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: pronunciation?"
    date: 2001-08-08
    body: "I believe it is Athee Oh Ess, kind of like BeOS.  This pronouncation is to differentiate it from the greek work atheos which means athiest."
    author: "BOSC_Maverick"
  - subject: "OT:( About Universal Components )"
    date: 2001-08-03
    body: "After some seaching I found Ettrich's slide show \nabout \"Univesal Components\" here\nhttp://linuxforum.dk/2001/slides/ettrich/components/html/\nDoes someone have a link to speech associated with it ( either sound or text form )"
    author: "ac"
  - subject: "Re: OT:( About Universal Components )"
    date: 2001-08-04
    body: "Thanks for the url, I played a bit with Google, and found Real-streams and OGGs at the same site:\n\nhttp://stream.linuxforum.dk/"
    author: "nap"
  - subject: "Nice looking OS..., but this stinks"
    date: 2001-08-06
    body: "Hi!\n\nI am impressed... But try this link:\nhttp://www.AtheOS.com/atheos/hardware\n\nWhat you get is a Microsoft support link for missing page?!?\n\nAnd they claim this:\n\"...\nThis server for example is running AtheOS. The HTTP server is a AtheOS port of Apache, and most of the content is generated by the AtheOS port of PHP3 and perl. ...\"\n\nAny comments?"
    author: "Zoran"
  - subject: "Re: Nice looking OS..., but this stinks"
    date: 2001-08-06
    body: "Um, perhaps you'd like to point your browser to www.atheos.CX instead."
    author: "nap"
  - subject: "Re: Nice looking OS..., but this stinks"
    date: 2001-08-06
    body: "> Any comments?\n\nComments? That the guy running www.atheos.com  kinda screwed up. He have ripped the entire front-page from my server (at www.atheos.cx) without bothering to change essential information like this. He even left my email-address address at the bottom of the page even though I have never been involved with www.atheos.com.\n\nwww.atheos.com is indead running windows: http://www.atheos.com/about/\n\nThe AtheOS homepage (www.atheos.cx) from where he ripped the front-page have always been running AtheOS.\n\nCheck with netcraft:\n http://uptime.netcraft.com/up/graph/?mode_u=off&mode_w=on&site=www.atheos.cx\n\nOr grab the lates version of nmap and do an OS detection on your own."
    author: "Kurt Skauen"
  - subject: "Netraider 0.0.3"
    date: 2001-08-06
    body: "Yesterday night, I got the new konq-embedded from Simon's Website and compiled a new netraider version. I'm very very impressed by the progress of KHTML. :) I didn't work on Netraider for months, but finally got interest again.\n\nYou can get a precompiled binary here:\nhttp://liebesgedichte.net/Download/netraider-0.0.3-bin.zip\n\nI was too tired and all I remembered was how to do zip files. ;) It might also not work for you if you have an older libstdc++ or Qt.\n\nAnd finally here is a screenshot of a probably upcoming version 0.1. I hope, that I can contact the developer again. :) It's a long time since I switched to Galeon, but I'm back to KHTML now. ;)\n\nhttp://liebesgedichte.net/Snapshots/netraidersnapshot2.png"
    author: "Spark"
---
<a href="mailto:george.russell@clara.net">George Russell</a> wrote in to inform us of the KHTML port to <a href="http://www.atheos.cx/">AtheOS</a>. <i>"The main focus for V0.3.5 [of AtheOS] however has been on the KHTML-based web browser. I have ported the HTML parser/renderer used in the <a href="http://www.konqueror.org/konq-browser.html">Konqueror</a> web browser (KHTML) to AtheOS. KHTML is a very capable HTML parser and renderer that supports both CSS and javascript, and so does the AtheOS web browser. Finally, a high-quality web browser for AtheOS! The browser is part of the 0.3.5 base install and the 0.3.4->0.3.5 upgrade archive."</i>  Now that the dust from the slashdotting has settled, you can actually check out <a href="http://www.atheos.cx/screenshots/shot7.php3">the screenshot</a> (<a href="http://static.kdenews.org/content/atheos-khtml/shot7-mod.png">dot mirror</a>). This has not been the first, and undoubtably, it is not the last port of KHTML to strange new platforms.  So huge kudos go out to the Konqueror/KHTML developers, and all those people out there who helped shape this resource.
<!--break-->
