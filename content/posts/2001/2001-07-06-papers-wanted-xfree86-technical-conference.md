---
title: "Papers Wanted: XFree86 Technical Conference"
date:    2001-07-06
authors:
  - "Inorog"
slug:    papers-wanted-xfree86-technical-conference
comments:
  - subject: "Re: Papers Wanted: XFree86 Technical Conference"
    date: 2001-07-06
    body: "Seems to me a paper on the modular KWin and/or the NetWM implementation would be quite relevant too...\n\n-N."
    author: "Navindra Umanee"
  - subject: "Re: Papers Wanted: XFree86 Technical Conference"
    date: 2001-07-07
    body: "Startup Notification is a good topic."
    author: "ac"
  - subject: "Re: Papers Wanted: XFree86 Technical Conference"
    date: 2001-07-07
    body: "Maybe someone will come up with a way to have large full-colour cursors in X... So the startup notification icons won't have to lag behind the cursor."
    author: "AC"
  - subject: "Color cursors"
    date: 2001-07-07
    body: "Oh, yes, PLEASE talk about full color cursors with alpha channels and animation at this conference. This really is something where X seriously lacks behind!\n\nIt might have been ok to have a 1-Bit cursor twenty years ago, but not today. Other OS's already feature cursors with a shadow! We could have this, too, if we had an alpha channel.\n\nThis is my biggest wish for XFree86."
    author: "me"
  - subject: "Re: Color cursors"
    date: 2001-07-08
    body: "I swear that busy cursor is gonna make me go into an epileptic fit!  *Please* turn this blinking off!  You really don't want to provoke seizures in your users!\n\nThe busy cursor is otherwise cool but it lags too slowly behind."
    author: "ac"
  - subject: "Re: Color cursors"
    date: 2001-07-08
    body: "... so, you'll take the time to complain about it, but you won't take the time to find the option in the Control Centre to turn it off? Look for the 'Startup Notification' tab."
    author: "Jon"
  - subject: "DCOP over ICE"
    date: 2001-07-07
    body: "I would hope DCOP over ICE would not be something KDE shouts about. Its really not something to be proud of. \n \nCORBA would have been a much better approach.\nHopefully ORBit2 can be modified to support DCOP over IIOP... and KDE and GNOME can transition to standardised CORBA interfaces. \n\nI expect a lot of \"CORBA is slow\" replys here. \nJust to pre-empt... \nDCOP does:\nclient> marshal, \nclient> send to Xserver via pipe, \nXserver> send to server via pipe,\nserver> demarshall,\nserver> execute call\n\nCORBA does:\nclient> marshal, \nclient> send to server via pipe, \nserver> demarshall,\nserver> execute call\n\nSo there is less context switching in the CORBA case."
    author: "robert"
  - subject: "Re: DCOP over ICE"
    date: 2001-07-07
    body: "> I would hope DCOP over ICE would not be \n> something KDE shouts about. Its really not\n> something to be proud of. \n\nHmm... I am very proud of the work of the KDE masterminds that created DCOP (among others). This proven to be one of a heck of a technology. It's only 18 months old and it's already largely widespread in KDE apps. It was so even after 1 week. It does the job now! When will end to, it will surely get replaced. Pragmatism."
    author: "Inorog"
  - subject: "Re: DCOP over ICE"
    date: 2001-07-07
    body: "Hi Robert,\n\nYou must be confusing ICE with something else because ICE doesn't use the X server.\n\nAnd yes, CORBA is very similar to DCOP. The difference is that DCOP is aimed at solving KDE's\nneed for desktop communication and CORBA seems to be aimed at multi-vendor distributed computing solutions.\n\nDifferent problems, different solutions.\n\nCheers,\nWaldo"
    author: "Waldo Bastian"
  - subject: "Re: DCOP over ICE"
    date: 2001-07-07
    body: "Part of the reasoning might've been the fact that most of the free CORBA ORBs available when KDE was trying to develop their object model were huge and inefficient, or only supported a subset of the platforms that KDE worked on.  It was assumed that since ICE was pretty much part of the X spec that it'd work around the board.  Turns out that it was somewhat broken on a number of platforms.  These days there are a lot of good ORBs out there that are fairly dependable and fairly inter-compatible with each other.\n\nUnfortunately, just having KDE switch from DCOP to CORBA probably wouldn't solve the object model compatibility problem between KDE and Gnome, since I think they'd still be pretty oblivious to each other.  There's no easy solution to this problem without either one side or the other suffering a lot of pain, or both sides going through a bit a pain to meet in the middle.  I don't know enough about either the KParts or Bonobo/Gnorba developers to know if they'd ever be willing to give up on some of their ideals for the reason of interoperability.\n\nI know a lot of people think that people stressing interoperability between KDE and Gnome are just trolling, but I really feel it's important.  The existance of multiple desktop environments has been a major target for those who want to dismiss the idea of a free desktop environment ever taking root in the mainstream.  The only way to make sure that multiple environments can exist and don't cause problems for customers and developers who would develop for the platform is to make sure that the application will behave as expected regardless of the user's preferred desktop environment."
    author: "Chad Kitching"
  - subject: "Re: DCOP over ICE"
    date: 2001-07-07
    body: ">The only way to make sure that multiple environments can exist and don't cause problems [...] is to make sure that the application will behave as expected regardless of the user's preferred desktop environment.\n\nI say again:  Where do people get the idea that this is not ALREADY the case, right now?  My KDE apps will run under GNOME, my GNOME apps will run under KDE.  I can even make them look the same (KDE will change colors of GNOME apps to match itself, and can even import the GNOME standard theme).  All apps work the same regardless of what desktop environment you use, and even if you don't use one at all!\n\nIf you use a sane distribution like Debian, it will keep your menus in sync, so that GNOME apps get added to your K menu and KDE apps to your GNOME (foot?) menu, and both kinds of apps to your twm menus or whatever oddball WM you use.\n\nAPI-level compatibility between QT and GTK is even being achieved through QGTKWidget and such.\n\nWhere is the big problem?  Are you so intolerant of differences that you simply MUST have all your apps share the same backend code (QT or GTK, CORBA or DCOP), even though the differences aren't really important to the end user as long as he can use the apps?"
    author: "not me"
  - subject: "Re: DCOP over ICE"
    date: 2001-07-07
    body: "I think the ideal world would have you be able to put both GNOME and KDE components into a single application, and have them work and communicate together seamlessly. For this you'd need a common part framework, and a common communication framework. At the moment, they don't even share the same communication protocol.\n\nEven if we moved from DCOP to CORBA, the hard work would still be ahead."
    author: "Jon"
  - subject: "Re: DCOP over ICE"
    date: 2001-07-08
    body: ">I think the ideal world would have you be able to put both GNOME and KDE components into a single application, and have them work and communicate together seamlessly.\n\nMaybe I'm missing something, but I really don't see this as such a big deal.  Is it really that important for Nautilus to be able to embed KWord?  Are there really any KParts that GNOME needs or Bonobo components that KDE needs?  I don't see the need for this sort of thing.\n\nThe only real problem I can think of is if Big Company X decided to port popular application Y to KDE and wanted to have a KPart, then GNOME people wouldn't be able to use the KPart.  I still don't think it's a problem, though, because the application itself would still work.  You just wouldn't be able to embed it in Nautilus or whatever.  Boo hoo."
    author: "not me"
  - subject: "Re: DCOP over ICE"
    date: 2001-07-09
    body: "Are you aware of OLE (or ActiveX, or COM, or whatever they renamed that technology to) components in Microsoft Windows?  These would allow you to embed different documents within each other.  You can have a Paintbrush picture inside your Microsoft Word document, inside a Lotus 123 spreadsheet.  This is much more than just Konqueror or Nautilus using components as file viewers, and will become increasingly important.  While I mean no disrespect to the developers of competing KDE graphics editors, The GIMP seems far more mature than anything KDE currently has, and it would be really nice if it could play nice with KDE apps when it comes to making compound documents.  Without this, creating compound documents either means constantly exporting to a file, and insertting that file, or copying and pasting, which is a lot of work if you need to change what you're pasting many times.\n\nNow, I know KOffice doesn't support this yet, and maybe even the KDE KParts framework doesn't support it yet, but don't kid yourself into thinking it doesn't matter.  It will become a requirement in the future, and I don't want to have to take the tradeoffs one desktop environment would impose on me over the benefits of the other.  Most users wouldn't understand the fact they can't use these two programs together because the authors had a difference in programming ideals."
    author: "Chad Kitching"
  - subject: "Re: DCOP over ICE"
    date: 2001-07-08
    body: "There's more to application interoperability than just widget colours.  There's things like keybindings, various UI preferences in each of the respective control centres.  Then the really hard thing to co-ordinate is the object model that would let you do something like embed lets say a GIMP image inside a KWord document.  Sure, I can run GTK+ apps on KDE and they'll work just fine, but I lose many features that I'd normally get if I only ran KDE or only ran Gnome apps.  And that, is the problem."
    author: "Chad Kitching"
  - subject: "Colors for Gnome apps in KDE?"
    date: 2002-07-02
    body: "I'm running KDE 3.0, and I can't get any of my gnome apps to look like the rest of my KDE apps.  Can someone tell me how this is done?\n\nThanks =)\n-Nilbus"
    author: "Nilbus"
  - subject: "Re: Colors for Gnome apps in KDE?"
    date: 2005-12-27
    body: "    I've got the same problem. I can't make gnome apps look nice inside kde. They have an ugly theme by default. So when I run gnome-themes-manager from KDE , and I choose a theme that looks similar to my KDE theme, works fine: all gnome apps now have the theme i've choosed. But all this is lost when i logout & login again.\n    what can I do to apply these settings for every new KDE session?"
    author: "Santiago"
  - subject: "Re: Colors for Gnome apps in KDE?"
    date: 2007-06-27
    body: "Try gtk-qt-engine (http://www.kde-look.org/content/show.php?content=9714). It works very well for me."
    author: "Riddle"
  - subject: "Re: DCOP over ICE"
    date: 2001-07-08
    body: "DCOP already does far more for KDE than has ever been done on any other desktop.  Where else have you seen power similar to that of DCOP deployed? There is nothing matching the kind of power that DCOP puts in a KDE desktop user's hands.  \n\nNot even your vaporware network-componetized GNOME desktop. DCOP is something to be *extremely* proud of."
    author: "ac"
  - subject: "Re: Papers Wanted: XFree86 Technical Conference"
    date: 2001-07-09
    body: "How about configuration? Windows has good graphical controls for setting bit depth, resolution and drivers on the fly (well, you have to reboot for the drivers.) But having an interface for setting these type of options without editing the config file and restarting the server would be nice."
    author: "Derek Spears"
  - subject: "Re: Papers Wanted: XFree86 Technical Conference"
    date: 2001-07-09
    body: "I agree that this is a very important issue. Many friends of mine have given Linux a try. Often they end up with a non-perfect monitor setup, and then give up on Linux as they have *no clue at all* how to change the resolution.\n\nIn general the way of setting resolutions/freq/depth has to be more straightforward, and KDE, Gnome etc should also include config modules to handle this. Maybe a root-module for setting up a number of resolutions, and then a user-module to choose from these."
    author: "Johan"
---
<a href="http://www.xfree86.org/">The XFree86 Project</a> has announced the <a href="http://www.usenix.org/events/xfree86/cfp/">XFree86 Technical Conference</a>. The conference will take place on November 8th, 2001, in Oakland, California, in a concurrent run with the 5th Annual Linux Showcase &amp; Conference. The last call for papers was put out on July 5th. You have up to July 13th to submit an abstract. I secretly hope that somebody will present a nice talk on the use of libICE in KDE's <a href="http://developer.kde.org/documentation/library/dcop.html">DCOP mechanism</a>. This certainly deserves attention as one of the most interesting developments around the XFree86 project, along with antialiasing, modular drivers for the XServer and TrueType fonts support.
<!--break-->
