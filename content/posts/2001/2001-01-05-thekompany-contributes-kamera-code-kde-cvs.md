---
title: "theKompany Contributes Kamera Code to KDE CVS"
date:    2001-01-05
authors:
  - "Dre"
slug:    thekompany-contributes-kamera-code-kde-cvs
comments:
  - subject: "Re: theKompany Contributes Kamera Code to KDE CVS"
    date: 2001-01-04
    body: "Just wanted to mention that I think the guys at TheKompany are doing a <i>great</i> job!\n<br><br>\nI like the work in kivio very much, and if they continue to supply us with great programs, the future looks bright for Linux and KDE :-)"
    author: "Guus"
  - subject: "Re: theKompany Contributes Kamera Code to KDE CVS"
    date: 2001-01-04
    body: "Thank you for the kind words :).\n\nLook for another Kivio release any day now, we are just trying to get it packaged right now.  Don't worry, we will be announcing it everywhere as usual :).\n\nShawn"
    author: "Shawn Gordon"
  - subject: "Re: theKompany Contributes Kamera Code to KDE CVS"
    date: 2001-01-04
    body: "Say, is any of the scripting code to automatically scan a network/database/etc. and display a flowchart anywhere near stability yet?\nAlso, since Kamera is a KIO slave, then digital camera files will be used by any KDE program, which is a different approach. Will there be any apps specifially to mangage digital photos? Will capability for this be put into pixie?"
    author: "Carbon"
  - subject: "Re: theKompany Contributes Kamera Code to KDE CVS"
    date: 2001-01-04
    body: "What I would envision for the digital camera stuff is that you would open them and manipulate them with Krayon, that was the original idea last summer when we were looking at helping with Krayon.  I don't think pixie is the place for it, but anyone can do whatever they want :).\n<p>\nWe are working on the first set of commercial stencils for Kivio right now, these do *not* include the more extreme ones such as the automatic scanning of various repositories.  We are working on them, but they are much more complex and will require a good amount of Q&A to make sure they meet our standards.  Don't hold me to it, but I would say another few months probably."
    author: "Shawn Gordon"
  - subject: "Re: theKompany Contributes Kamera Code to KDE CVS"
    date: 2001-01-04
    body: "You definitely are putting out good stuff:)\n\nWhen can we expect a preview release of Aethera?:) I recently heard 'within two weeks.'  Is this true?:)"
    author: "James"
  - subject: "Re: theKompany Contributes Kamera Code to KDE CVS"
    date: 2001-01-05
    body: "Thanks James :)\n\nI've avoided talking about Aethera for a while because the UI keeps going through changes and now we are trying to get the icons finished up.  The current state is that the backend is done and tested for the initial release and we are working on the UI.  It *should* be available in 2 weeks.  You will definitely hear about it when we make it available :)."
    author: "Shawn Gordon"
  - subject: "Re: theKompany Contributes Kamera Code to KDE CVS"
    date: 2001-01-05
    body: "I've seen your posts around and around the \"community\" on pretty much all the news sites.  All very helpful and respectful.  You seem to not have an \"agenda\" except to maintain a succesul company while giving back to the community.  You have definately earned most people's trust IMHO and I *really* hope you guys are making some $$ with your \"PowerPlant\" product.  I've been thinking of purchasing it after d/ling the newest kde beta binaries from mandrake and seeing how cool kde studio is.  Just for the utility of automagically maintaining makefiles is good enough, much less all the other stuff (keep working on that code completion feature! ... that's the #1 feature I miss that differentiates from a mere text editor).\n\nAgain, thank you.  It seems odd thanking a business since they typically have their best interests in mind (an agenda) but I know your goals are both for the good of the community and for your company (you understand each benefits the other greatly).  I hope your company is successful is it's endevours and maintains its respect throughout the community.\n\nJim"
    author: "Jim Basilio"
  - subject: "Re: theKompany Contributes Kamera Code to KDE CVS"
    date: 2001-01-05
    body: "Hi Jim,\n\nThanks for your support, I'm glad to hear that we have succeded in our goals, at least so far :).  While we have been selling PowerPlant, we would always like to sell more, so feel free to pick up a copy :), actually I'm thinking of putting out a $10 discount to the KDE community, but I am trying to find the best way to tell people about it without being a spammer.\n<p>\nLook for some more products starting next week."
    author: "Shawn Gordon"
  - subject: "Re: theKompany Contributes Kamera Code to KDE CVS"
    date: 2001-01-05
    body: "I am always happy to see developers take an active intrest in answering questions/concerns and\nproposals sent to them by their customers.\nI am an active user of KDE and am always impressed by the close relationship of the developers and the regular users.\nIt is even greater seeing a commercial entity behave in a similar manner.\nThanks once again for your contribution to making kde more practical on the desktop."
    author: "Kurt Palmer"
  - subject: "Re: theKompany Contributes Kamera Code to KDE CVS"
    date: 2001-01-05
    body: "Hi,\n\nI have to strongly agree with the last post. When I tried KDE Studio a while ago with a Beta of KDE, I had problems compiling it. I sent a question too the kompany's support team and got an answer within minutes. Even further questions were answered right away. I really appreciate this behaviour since it shows a lot of interest. Thanks a lot. \n\n(I hope my words sound as positive as they are intended. Even though my name sounds English, English is not my mother language ;)\n\nGreetings\n\nRichard"
    author: "Richard Stevens"
  - subject: "Re: theKompany Contributes Kamera Code to KDE CVS"
    date: 2001-01-05
    body: "<I>When I tried KDE Studio a while ago with a Beta of KDE, I had problems compiling it. I sent a question too the kompany's support team and got an answer within minutes. Even further questions were answered right away.</I>\n<P>\nI'll second that.  I had problems with a beta from theKompany, and got personal and immediate responses from Shawn Gordon, even over the U.S. Thanksgiving break.  I've been a KDevelop fan since the 1.0 beta's, but I really enjoy the strengths of KDEStudio too, like the auto code completion and method parameter help, and the support for building library projects (not just executables).\n<P>\n- Andy"
    author: "Andy Marchewka"
  - subject: "How to inform about the discount without spamming"
    date: 2001-01-05
    body: "How about putting it in you'r signature file, and/or at the end of you'r posts here.<br> Somthing like: \"Did you know that as a KDE developer <a href=\"http://www.thekompany.com\">The Kompany</a> will give you a $10 discount on <a href=\"http://www.thekompany.com/products/powerplant/index.php3?dhtml_ok=1\">PowerPlant</a>?\"<br>That way annyone reading posts and/or mails in the mailinglists from you (and your developers?) will get the information."
    author: "J\u00f8rgen Adam Holen"
  - subject: "Thank you"
    date: 2001-01-05
    body: "The Kompany are doing a great job.<BR>"
    author: "KDE USER"
  - subject: "Re: Thank you"
    date: 2001-01-05
    body: "Yes, they are, however...\n\nHow can they remain profitable in the long run if they give all their coolest software away? I mean, Shawn has to actually pay people to develop it, it doesn't get done for free like most other open-source software.\n\nYou'd need to sell a LOT of proprietary Kivio stencils to cover the salaries for all the people developing KDE Studio, gPhoto frontends, PyQt, etc.\n\nShawn, I hope you got your finances right!\nWe want to see theKompany stick around and be successful, not run out of initial cash! :-)"
    author: "Jacek"
  - subject: "Re: Thank you"
    date: 2001-01-05
    body: "If you look at most of what we give away, it is infrastructure pieces.  We are building other products on top of this infrastructure, and those will be sold, but then everyone can make use of the infrastructure.  Of course people can help us by supporting us and buying our software :).\n\nWe have an exciting product we will announce next week, hopefully Tuesday, that is based on PyQt that I think many people will be interested in.  This is the grand experiment of the perfect symbiotic relationship, lets all hope it's successfull :)."
    author: "Shawn Gordon"
  - subject: "Re: Thank you"
    date: 2001-01-06
    body: "Hi Shawn, \nI'm really interested in this product based in PyQt, (even I don't have a clue of what it is ;) I really hope the surprise will be a development environment for Python/PyQt applications.\nSince the release of PyQT I've been waiting for an IDE to take advantage of it.\nI'm looking forward for Kylix, but a RAD tool based on Python seems more appealing to me."
    author: "Carlos Miguel"
  - subject: "PyQt/PyKDE RAD/prototyping tool"
    date: 2001-01-06
    body: "One realy great tool for PyQT/PyKDE would be a UIC port making Pyton code from Qt designer , PyUIC. This gives you a great RAD tool, if you want to change language for some reason, you don't have to redo your UI, just recompile with a different ui conmpiler. <br><br>\nKnut"
    author: "Knut M. Johansson"
  - subject: "Re: PyQt/PyKDE RAD/prototyping tool"
    date: 2001-01-06
    body: "Bingo, give the man a prize :)\n\nAnnoncement will be Tuesday."
    author: "Shawn Gordon"
  - subject: "Re: PyQt/PyKDE RAD/prototyping tool"
    date: 2001-01-06
    body: "Hi,\n\nSuch thing as pyuic already exists, as far as I understand it was developed too by Phil Thompson to leverage QT Designer (which in turn was wisely designed to produce language agnostic UI specifications in XML).\n\nI'm using it right know to develop a client-server application, and it helped me build the UI in a snap.\n\nIt would be great if you could integrate UI building, project management, data aware widgets, compilation and packaging from one single place, a la Visual Basic (only of course, using Python instead)."
    author: "Carlos Miguel"
---
Shawn Gordon wrote in with the good news that
<A HREF="http://www.thekompany.com/">theKompany.com</A> has contributed the
source code for Kamera (previously
<A HREF="http://dot.kde.org/976731869">featured</A> on the Dot),
a <A HREF="http://www.gphoto.net/gphoto2/">gPhoto2</A>-based KIO slave for
digital cameras, to the
<A HREF="http://www.kde.org/install-source.html">KDE CVS</A>.  He also
has some updates on Kamera developments.  Read below to find out more.



<!--break-->
<BR>
<blockquote>
<P>
Kamera, KDE2's <A HREF="http://www.gphoto.net/gphoto2/">gPhoto2</A>-based KIO slave for digital cameras, has been integrated into KDE CVS under the <A HREF="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdegraphics/kamera/">'kdegraphics' module</A>. This is now the definitive repository for Kamera and obsoletes the versions previously available at theKompany's <A HREF="http://www.thekompany.com/">website</A>.  theKompany will continue to support the library and provide a home page for Kamera information.  All other developers are welcome to join in.
</P>
<P>
After discussions on the gphoto-devel mailing list among gphoto, KDE and GNOME developers, we have standardised 'camera://' as the URL scheme for digital cameras.  This will avoid confusion between users of the different desktops. The version in KDE CVS conforms to this standard.
</P>
<P>
Please note that Kamera relies on gPhoto2, currently under heavy development and only available via CVS. Therefore there may be times when the driver for your particular camera may not be in a working state, or necessary changes in the gPhoto2 API may break Kamera compilation.
</P>
<P>
<a href="http://www.thekompany.com">theKompany</a> would like to take this opportunity to praise the <a href="http://www.kde.org">KDE</a> team for the terrific
KIO Slave architecture and for their assistance, and especially the <a href="http://www.gphoto.org">gPhoto2</a> developers for their excellent work. Thanks!
</P>
<P>
Shawn Gordon<BR>
theKompany.com
</P>
</blockquote>

