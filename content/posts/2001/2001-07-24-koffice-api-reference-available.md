---
title: "KOffice API Reference Available"
date:    2001-07-24
authors:
  - "rkaper"
slug:    koffice-api-reference-available
comments:
  - subject: "Re: KOffice API Reference Available"
    date: 2001-07-24
    body: "I see api for all the KOffice applications.  Where is the api for the KOffice libraries?"
    author: "KDE User"
  - subject: "Re: KOffice API Reference Available"
    date: 2001-07-25
    body: "I wonder when KoParts will be a part of a standard KDE distribution."
    author: "Krame"
  - subject: "Re: KOffice API Reference Available"
    date: 2001-07-25
    body: "Why ? What for ? Aren't they ?\n\n(PS: I see what you mean by KoParts, but technically those are the classes in kofficecore: KoDocument, KoView etc.)\n\nThis is part of a standard KOffice distribution, which is part of a standard KDE install. I guess I miss your point."
    author: "David Faure"
  - subject: "Re: KOffice API Reference Available"
    date: 2001-07-25
    body: "KOffice doesn't seem to be a part of KDE core distribution. I meant that kofficecore should be included in kdelibs someday. Now I must compile whole KOffice package to get KoParts working - I needn't MSOffice to use OLE.  Now I can't see many KDE application that supports KoParts.\n  Just my opinion."
    author: "Krame"
  - subject: "Re: KOffice API Reference Available"
    date: 2001-07-25
    body: "Perhaps I'm incorrect here, but are KoParts simply KParts of the various KOffice tools? If so, it may be true that you don't need to have MSOffice to have OLE, but you do need MSOffice to embed an Excel document (the OLE component, not the OLE system) in another document.\n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "Re: KOffice API Reference Available"
    date: 2001-07-26
    body: "You can embed Paintshop picture in WordPerfect. Embedding is not bound to MS office."
    author: "Krame"
  - subject: "Re: KOffice API Reference Available"
    date: 2001-07-26
    body: "Well, as I mentioned, and notme mentioned, KOffice doesn't have the architecture only the objects. For instance, to embed a Photoshop picture in WordPerfect, you must have Photoshop and WordPerfect installed.\n\nSame with KOffice, KOffice's KoParts are simply the OLE-like objects. The OLE-like system is KParts, and is built into the base KDE distribution. So, embedding is not bound to KOffice in anyway (infact if you use Konqi, you are taking advantage of KPart embedding for the HTML or File management tools).\n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "Re: KOffice API Reference Available"
    date: 2001-07-26
    body: "\"KOffice doesn't have the architecture only the objects\"\nI don't agree. All interfaces related to embedding documents, hierarchical storage etc. are defined in kofficecore unless we define our own ( based on KParts of course ), which would be incompatible with KOffice.  Of course I understand that KoParts are not ready yet, but I can't  when you say that koffice doesn't contain interfaces but only components.\n\n\"to embed a Photoshop picture in WordPerfect, you must have Photoshop and WordPerfect installed\"\nBut not MSOffice ! You can write your own OLE object without MSOffice installed. It is probably not so important, I can live without KoParts, but I want to explain, what I meant."
    author: "Krame"
  - subject: "Re: KOffice API Reference Available"
    date: 2001-07-26
    body: "Hmm... So KParts can't be embedded in KOffice?"
    author: "Timothy R. Butler"
  - subject: "Re: KOffice API Reference Available"
    date: 2001-07-26
    body: "kofficecore is just our OLE system."
    author: "Krame"
  - subject: "Re: KOffice API Reference Available"
    date: 2001-07-25
    body: "KoParts are just KParts, slightly extended for use in KOffice.  I don't know of any app outside of KOffice that needs or could even use KoParts (except for embedding KOffice apps, which requires KOffice anyway).  All apps outside of KOffice use regular KParts, which are included in the KDE base install and work fine.\n\nP.S.  The decision to remove KOffice from KDE proper was made because they needed to follow seperate release schedules (KOffice 1.0 was a total joke, released too early because it needed to be in sync with a KDE release).  KOffice was part of KDE at one time and it might be again in the future when it matures enough to have a stable release with each KDE release."
    author: "not me"
  - subject: "Re: KOffice API Reference Available"
    date: 2001-07-26
    body: "Right on. Couldn't say it better :)"
    author: "David Faure"
  - subject: "Sounds like OS X"
    date: 2001-07-25
    body: "What's Graphite?  It doesn't seem to have any class descriptions.\n\nP.S.  I'm noticing subtle changes in the dot - keep up the good work guys!  Gosh, now I actually have to think up a descriptive title :-)"
    author: "not me"
  - subject: "Api but no doc"
    date: 2001-07-25
    body: "The API is online but it seriously lacks some comments to explain what it does. The next step is to:\n- add some comments to all the functions\n- describe the architecture: what is the difference between a kpart and a kopart, ...\n- write a tutorial.\n\nBut this first step is already a good one. I hope you'll get some contributors."
    author: "Philippe Fremy"
  - subject: "Re: Api but no doc"
    date: 2001-07-25
    body: "You are right;\nWant to help?"
    author: "Thomas Zander"
  - subject: "Re: Api but no doc"
    date: 2001-07-26
    body: "About method docu: I'm currently preparing a new library called libkotext, containing most of KWord's classes (for reuse in other KOffice apps). While doing that, I'm converting all the method comments to the kdoc style.\nSo, no need for someone else to do this, in kword, it's coming.\n\nThe koffice architecture is described somewhere else on the website, isn't it ? (document/view is the major point here).\n\nOne thing: we have a DESIGN document in kword, that probably doesn't appear in the online docu. Would a @libdoc tag, and renaming to design.h  fix that ? (IIRC that's what we did in kio - can't check right now).\n\nTutorial: about developing a new koffice app ? Actually I'd prefer to see more contributions to the existing applications, so I'm not too fond of that..... Well, new koffice apps are certainly a good thing though !\n\n/me heading to the airport shortly :)"
    author: "David Faure"
  - subject: "Re: Api but no doc/Text classes reuse"
    date: 2001-07-27
    body: "Speaking of class reuse, I noticed that Koffice has alot of graphics engine classes that could really be used to build an uber featurefull set of graphics apps. For instance if there existed a light weight vector graphics engine, it could be used by kchart to do the graphing allowing users to really customize the charts in kchart. The same engine could be extended in kountour and kivio to provide a consistent powerfull method to deal with vector graphics. It could event be used in krayon.\n\nThanks\n\n\nceler"
    author: "celer"
  - subject: "KSpread"
    date: 2001-07-26
    body: "The most important missing feature of all KOffice application is the missing/suboptimal import *and* export filter for MS Office.\n\nFor any Office program to be successful it is essential to integrate seamlessly into the existing landscape As you would expect that with your new cellphone you'd be able to call all your folks - if you can't, or they can't reach you then it's useless.\n\nConrad"
    author: "Conrad"
---
Thomas Zander <a href="http://lists.kde.org/?l=kde-core-devel&m=99592311602872&w=2">has announced</a> that the <a href="http://www.koffice.org/developer/">KOffice API docs</a> are now on-line. Feel free to see this as invitation to <a href="mailto:koffice@kde.org">join the KOffice team</a> and make future versions of the office suite even better than the upcoming 1.1 release.
<!--break-->
