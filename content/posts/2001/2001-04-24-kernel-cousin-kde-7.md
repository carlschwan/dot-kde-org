---
title: "Kernel Cousin KDE #7"
date:    2001-04-24
authors:
  - "Dre"
slug:    kernel-cousin-kde-7
comments:
  - subject: "Re: Kernel Cousin KDE #7"
    date: 2001-04-23
    body: "<b>The link is dead :-(</b><br>\nhttp://kt.zork.net/kde/kde20010420_7.html"
    author: "Guus"
  - subject: "Re: Kernel Cousin KDE #7"
    date: 2001-04-23
    body: "Hey, look at that. It has already been fixed. I guess I was just a little too soon!"
    author: "Guus"
  - subject: "Re: Kernel Cousin KDE #7"
    date: 2001-04-24
    body: "Still not working for me."
    author: "Craig Black"
  - subject: "Re: Kernel Cousin KDE #7"
    date: 2001-04-24
    body: "It worked then it broke again."
    author: "AC"
  - subject: "Re: Kernel Cousin KDE #7"
    date: 2001-04-24
    body: "\"new and (much) improved News Ticker applet\"? KNewsTicker evolves happily in the repository since six months and it's not really much improved - it's the answer to the sense of life and everything, a more verbose version of \"42\". Mothers told me that it taught their children to play piano, it's in the local fire guard and can make coffee. That's much improved, indeed."
    author: "Frerich Raabe"
  - subject: "Re: Kernel Cousin KDE #7"
    date: 2001-04-24
    body: "Ignore Frerich, folks, he's been improving the News Ticker applet, much.  Those const bools will tend to drive people crazy. ;)"
    author: "Charles Samuels"
---
In this week's <A HREF="http://kt.zork.net/kde/kde20010420_7.html">Kernel Cousin KDE</A>, Aaron J. Seigo covers PGP support in the kdenetwork libraries, message scoring in <A HREF="http://devel-home.kde.org/~kmail/index.html">KMail</A>, some KControl restructuring views, comparisons of QSocket and KSocket, extending the KDE address book, Rik Hemsley's <A HREF="/987963955/">KDE widget styles tutorial</A>, using <A HREF="http://www.mozilla.org/">Mozilla</A>'s Gecko as the rendering engine for <A HREF="http://www.konqueror.org/">Konqueror</A>, and which version of <A HREF="http://www.trolltech.com/">Qt</A> to use for compiling from CVS.