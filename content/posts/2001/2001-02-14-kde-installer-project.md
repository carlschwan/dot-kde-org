---
title: "KDE Installer Project"
date:    2001-02-14
authors:
  - "numanee"
slug:    kde-installer-project
comments:
  - subject: "Re: KDE Installer Project"
    date: 2001-02-14
    body: "This is the way to go, Nick! The screenshots show really what people want to have. I don't understand really why you need help. Once people select what packages they want to install, it is just a matter of executing few \"rpm -i\" commands.\nAnother question: this installer seems to propose /opt/kde2 directory, where kde2 should be installed. My current 2.1beta distribution is installed in /usr.\nWhy this mess? Wouldn't be easier to agree on where kde2 should be installed by default, then if someone really wants to have it somewhere else, installer would allow him to do that?"
    author: "Bojan"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-14
    body: "Go ahead, try to convince Redhat to install KDE on any other place other then /usr - the issue has already been arisen and Redhat has an internal (very thick) book which describe what will be installed and where to be installed..\n\nLets hope that one the LSB (Linux Standard Base) will be finally out as version 1.0 - then all of the distributions will be on the same place..\n\nHetz"
    author: "hetz"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-14
    body: "It's not clear that the LSB will force RedHat to change their directory structure.  In fact, it might be so general as to not change a thing.\n<P>\nAlso, related to RedHat:<BR>\nwhat do you do when some of the rpm's are missing?\n<P>\nFor RedHat 6.2:\nThe latest stable branch is missing a kde-graphics rpm file.  \n<P>\nThe BetaR2 version is missing kde-networks.  \n<P>\nIn this case, I think the Installer will be a beautiful way of telling you that you're hosed."
    author: "SK"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-14
    body: "<i>It's not clear that the LSB will force RedHat to change their directory structure. In fact, it might be so general as to not change a thing.</i>\n<p>\nIt might, if Linus Torvalds would support the LSB in such as way that he would deny any distribution not adhering to it to use the Linux trademark."
    author: "Rob Kaper"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-14
    body: "RedHat is the only one to put KDE in the right place.  Though it would be better if there was a /etc/kde"
    author: "robert"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-15
    body: "/etc should only be used for (system-wide) configuration files. The best place for KDE is really /opt/kde. Even though /opt is hardly used, it is a recognized and preferred installation path for many years."
    author: "Rob Kaper"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-15
    body: "I believe that the previous comment wa ment to be funny..."
    author: "J\u00f6rgen Lundberg"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-15
    body: "No, it proposed using /etc/kde for the *rc files, instead of /usr/share/config."
    author: "Peter Putzer"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-15
    body: "/etc is still not the place for KDE rc files.  /etc is for system files.  It should be under\nthe KDE home directory, as with other /opt softwares are.\n\njason"
    author: "Jason Gabler"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-16
    body: "Hmm .. what about for example /etc/smb.conf and /etc/postfix/* .. strictly speaking they are applications, even though they provide services and don't actually have \"user facing\" interfaces.\n\nAnd I notice there is already an /etc/gnome/*\n\nMacka"
    author: "Macka"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-17
    body: "KDe already has configuration files in KDEDIR/share/config and HOME/.kde/share/config. Nothing goes in /etc."
    author: "Triskelios"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-15
    body: "Seems unlikely, Linus has made a point of not supporting or dissing any distribution."
    author: "Carbon"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-15
    body: "Apart from Red Hat 7.0, that is.  Linus called it \"totally broken\" for use as a development platform, referring to the broken gcc snapshot they shipped with 7.0.  They later fixed the error, but he was right to speak out.  RH are getting a bit, um, complacent."
    author: "Jon"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-14
    body: "You obviously have never head of BSD before ;)"
    author: "ian geiser"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-14
    body: "Gah!  Repeat after me: KDE is not a linux distribution.  Let the distributions do this; it's their job.  Distributions ought to have a single tool for adding and removing software.  And I mean adding software that you haven't downloaded yet.  Feeding a previously d/led rpm to a package manager doesnt cut it.  It ought to act like a catalog of software that you can pick stuff you want and it'll D/L the RPMs and handle dependencies.  Debian does this, but not in a newbie-friendly way."
    author: "Ben Ploni"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-14
    body: "Dittos! If this project were an update manager instead, or a \"so you don't forget a step\" automatic build script, it might make a bit more sense, but...\n\nAn installer needs to be a binary to be of any practical use (or what's the point?). So how does it work on FreeBSD? And will it then proceed to install Linux binaries to FreeBSD? Or what about Slackware? Will this install the Redhat-centric kde-admin to Slackware?\n\nXimian screwed *BSD, Slackware and Stampede without even the courtesy of a reach-around, so I hope this project isn't as cavalier..."
    author: "David Johnson"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-14
    body: "People actually use Stampede still? I used to be a developer for Stampede so I can say that.\n\nI've heard that Ximian will support the BSD systems as their next platforms. I dunno about Slackware though (but then again, does Slackware even have dependency checking? No. But the *BSDs do apparently.)"
    author: "Anonymous Coward"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-15
    body: "Ximian really didn't ``screw'' BSD, slack, or stampede, 'cause A) I believe that they roll the changes they make back into the standard gnome project, and B) It's easy enough to compile Helix gnome with the patches on a non redhat/sun/debian system (I'm using it on a linuxfromscratch system) so if one wanted to he could make a BSD port, or slack/stampede package with the only thing missing being the auto updater.<BR><BR>\nOf course, this is only a short term solution, with the long term solution being to create a standard package management system that would be common for UNIX systems (similar to the way that the GNU autotools are cross platform)."
    author: "Caleb Land"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-14
    body: "Yeah. Just use KDE with Debian, and you don't need anything else: kde.debian.net is enough ! I know that KDE developpers must hate the Debian community, but happily war is over now :)"
    author: "Henri"
  - subject: "Stormix!  was Re: KDE Installer Project"
    date: 2001-02-14
    body: "Just started using Stormix after having used SuSE, Red Hat and Mandrake. Stormix has a nice GUI frontend to apt which makes it even easier to use. Apt is great. As has been pointed out, no other dist would be interested in providing this kind of update service..."
    author: "Niklas Lonnbro"
  - subject: "Re: Stormix!  was Re: KDE Installer Project"
    date: 2001-02-15
    body: "Also worth noting:  Stormix has gone belly up but stormpkg lives on in the current unstable package tree.  It really is the best apt front end I have used.\n\nAlso worth noting:  Eazel Nautilas, October GNOME, and KDE 2.1beta2 are all available from the main Debian unstable tree.  Ximian GNOME is available from Ximian's FTP servers and KDE2 for Debian stable is available from kde.tdyc.com."
    author: "raven667"
  - subject: "Re: Stormix!  was Re: KDE Installer Project"
    date: 2001-02-16
    body: "Come on Mandrake will have it with 8.0 I'm sure.\n\nCraig"
    author: "Craig black"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-15
    body: "So you won't use it.  It makes things easier for a lot of people whose distributions (or custom systems) didn't include KDE.  Nobody is forcing you to use it, so don't complain about it.  I'm glad something like this is finally being done, it helps out the newbie.  The real question is will it still work on other kernels besides Linux?"
    author: "dingodonkey"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-14
    body: "Thanks Nick, this will help the KDE project alot.<br>\nThe one suggestion I have is the user should also be able to select individual applications within the packages for installation.<br>  I have no idea if this is even possible when installing from RPMs.  Anyone know if this can be done?<p>\nSD"
    author: "SD"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-14
    body: "Avec :\nrpm  --excludepath <path>\npeut-\u00eatre ???"
    author: "Shift"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-14
    body: "With :\nrpm --excludepath <path>\nPerhaps\n\nSorry I have forgotten that I was on a English site :))"
    author: "Shift"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-14
    body: "Yeah, thats something i d like to have as well. A grapical\nexclude to progs in one rpm i dont need."
    author: "Jay"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-22
    body: "<p>Well, do it the Debian way, or the way Mandrake is going now, by splitting the packages into individual components :) Like, one for each game, for example.\n\n<p>Regards,\n\n<p>Michel"
    author: "Michel"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-14
    body: "<p>Hm. It's very nice to have a KDE installer and I think it may help a lot of people although the Linux distribution's interface/installer should take care about most of this anyway. As for me I always install KDE from the sources :-)... </p>\n\n<p>However, I think it is even more important for KDE to have a nice <b>application</b> installer that looks like this nice KDE installer.</p>\n\n<p>Okay, many people might now say: \"Hey, what do we need this for when we have a nice RPM installer?\" but I believe that this argument doesn't count.</p>\n\n<p>What I think is required is something that looks and works exactly like this nice KDE installer presented here but does not (only) (de-)install KDE itself but also applications, not necessarily limited to KDE apps. What do you think?</p>\n\nRegards, Stefan."
    author: "Stefan Hellwig"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-14
    body: "Yes !!!\nAnd call it kpackage :)\n\nIn France, we have an expression for this :\n\"R\u00e9-inventer la roue\"\n(Invent the wheel again)"
    author: "Shift"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-14
    body: "Sorry, kpackage is taken. It's the package manager for kde. However...how about simply kinstaller? or...kdenow? or gokde?\n\nStill...I was wondering...isn't it simpler to use cvsup? Track the STABLE branch and you should be okay...perhaps some kind of kde-front end? (which would customize the make, install scripts for the user - the user can select which he/she wants to download and install)\n\nJust a suggestion..."
    author: "mukhsein"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-14
    body: "<< Sorry, kpackage is taken. It's the package manager for kde. However...how about simply kinstaller? or...kdenow? or gokde? >>\n\nIt was a joke (ironic) : I know kpackage and I just wanted to say that the idea of Stephan already exists and her name was kpackage."
    author: "Shift"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-16
    body: "lol Now thats funny.\n\nCraig"
    author: "Craig black"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-14
    body: "Has SD said : \nIt will be better if the user can select each application he wants to install.\n\nkteatime is the only application I use in kdetoys for example.\n\nA GUI to install KDE is usefull but if there are no other features, it will be more easy to use :\n\"rpm -i *.rpm \" in the kde_directory_of_your_distrib"
    author: "Shift"
  - subject: "Learn from Ximian-Installer - This is now Flame"
    date: 2001-02-14
    body: "For the first time Installation the Wizard approach is nice. But when you want to update or install additional software it's not the best idea.\n\nHave you ever tryed out the Ximian (formerly Helix) Installer?\n\nIt lists Software in some categories:\n\n* Messages (just some information)\n* Importent Updates (Security issues & stuff)\n* Updates\n* New Software Packages\n\nThey are also working on a new installer. Which can handle so called \"Channles\", which you can offer your software. \n\nIt would be good if you could do following things:\n\n* Manage multiple rpm/dep repositories (sources)\n* Use apt for rpm+dep\n* View packages in the manner described above\n* Show packages to install ordered by sources\n* Show those packages by category\n...\n\nMaybe the updater part should base on the kpackage tool which offers some of the required features yet. But lacks a moderated channel view of packages possible to install.\n\nSo it would be a powerful tool not only for installing kde packages."
    author: "Uwe Klinger"
  - subject: "Re: Learn from Ximian-Installer - This is now Flam"
    date: 2001-02-14
    body: "Their new Red-Carpet program (the next generation of Helix-Update) can handle more than just GNOME packages now. It will supposedly even be able to install KDE packages as well as update my SuSE system, etc.\n\nI think that's pretty damn sweet!"
    author: "Anonymous Coward"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-14
    body: "apps.kde.com has a large number of KDE packages.  If apps.kde.com could provide an XML file or description file of the KDE packages available on that server (including download links, package format, version, conflict information, dependencies), maybe your program could use this to provide up to date install options to the user?"
    author: "ac"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-14
    body: "That's a great idea, really slick!"
    author: "Tom Corbin"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-14
    body: "I have to add here, that RedHat already created such a framework which is used by GnoRPM it uses a RDF files (I think). But in my opinion GnoRPM isn't a much usefull tool. On the other side Debian has index-files used to find out required packages (resolve dependencies). And Ximian has their own XML stuff. \n\nMaybe it's the best to adapt Red-Carpet and build a KDE frontend (see posting of an \"Anonymous Coward\" below) - and maybe extend its functionality. I didn't want to talk about such stuff: But we shouldn't see Gnome or Gnome centric companies as KDE's enemies, but see it as oppurtunity for making things better and learn from each other. Amen."
    author: "Uwe Klinger"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-16
    body: "That brought a tear to my eye. Why can't wejust get along? lol\n\nCraig"
    author: "Craig black"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-16
    body: "That brought a tear to my eye. Why can't wejust get along? lol\n\nCraig"
    author: "Craig black"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-14
    body: "I think Uwe's nailed it.  It really seems to me that\nwhat a lot of people are looking for is something\nvery similar to Ximian's updater/installer, where\nyou have choices of which mirror to use, \ncategories of packages, etc."
    author: "Tom Corbin"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-14
    body: "Yea, Ximian's installer is pretty nice. Have you seen Red-Carpet? I checked out their booth at LWE and saw a demo of Red-Carpet and I practically fell in love! I would install Ximian GNOME just to have Red-Carpet! It would be so nice if KDE had something exactly like it."
    author: "Anonymous Coward"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-16
    body: "No! It would NOT be nice. It would be nice if Red-Carped could do the job. (And it actually can) Why would KDE need to have a installer that does _exactly_ the same like an already existing one?"
    author: "Rijka Wam"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-15
    body: "Yes please, can we have a KDE installer which fucks up your system too. Really, I think this KDE vs Gnome feature-for-feature war is going to far."
    author: "cbcbcb"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-14
    body: "When can we have a red-carpet type of installer so that I dont have to download every single RPMs everytime when I want to upgrade something KDE?"
    author: "phatworm"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-14
    body: "Red-Carpet will allow you to upgrade your KDE software as well, I asked them at LWE ;-)\n\nWhat's cool about it is that they didn't make it GNOME-centric, they have a totally separate backend so maybe you guys could write a KDE front-end to their backend?"
    author: "Anonymous Coward"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-14
    body: "Exactly. If the Ximian team is creating this to update KDE packages as well, this would be an excellent project for KDE and Gnome developers to cooperate on and produce something consistent on the Linux/UNIX platform. Everyone wins."
    author: "Brad Stewart"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-14
    body: "Time for another long post, this is part of a mail I sent to a KDE developer back in early December last year on how I would like to code an Installer when I get time. At this time I had no time due to studying for exams which I still am but they end soon. I hope it covers some good ground and gives the author some new ideas / problems to think about. It's more complicated than just writting a download program for RPMS however and is mainly concerned with compiling from source as that's what I was interested in at the time as infact it's simplier due to different dependancies, libcs etc. Let me try and handle one issue though, people say this is something the distributions should do and I do agree with this except for the fact they are not doing it. Debian is an expection here and my hat is off to them but Debian is non profit and non profit means they have nothing to lose. The other distros rarely issue package updates except for security problems, if you could just buy Mandrake 6 two years ago and then automatically update it, why would anyone buy Mandrake 6.1, 6.2 etc. So, the distro's aren't exactly going to be falling over themselves to provide this feature if it costs them money. Ximian on the other hand.....Ah, just read my earlier post about their installer... So this covers some of the things I thought of for a way to get the KDE packaging sorted out and provide the basis of an installer. Binary distribution presents on big problem, packing the apps for each indivdual architecture that KDE supports and each sperate distribution for each arcitechture... It's got to be platform indpendant so where the orginal mail gos off to binaries and RPMs, I don't think RPM is a good idea as it's redhat derivitive distros only and hence is not on all systems. The best way to go would be to have a KDE apps DB local as mentioned first. It's also a lot bigger project than it first seems if it is to be done well.\n\n***\nOn to packaging, I'd like your ideas on this: One of the \"problems\" I can see with KDE is its packaging. The packages are too large and although they cover a specific area i.e. networking, admin etc I don't want to install all the bits of each package. For example say I wanted to manage my ftpserver via a nice gui, I then also install a user manager, a cron job manager a dat drive tool etc. Say I want an email client (kmail) as most people do, I also get an archcie client, a news reader, a PPP daemon config util, a AOL instant messenger etc etc... \n\nMost users install KDE from distributions and this is \nexactly what they see, a heap of apllications that they don't need and for the most part don't even understand.\n\nI think Linux has too many applications (well, to many similar apps anyway) and too many options. Bizare as it might seem this holds it back from the average user.  I'd say with the current satus of Linux mainly power users and techies use it, these users are the kind that only want to install what they \nneed. Of course this also can be blamed on the distribution makers but we need to make it easier for them as well, the current way KDE make releases isn't that effective.\n\nSolution:\n\n1. KDE core should be maintained as it currently is the 3 main packages:\nkdesupport\nkdelibs\nkdebase\n\nIMHO I think kdesuppport should be phased out if possible but this will take time? Possible solution to getting rid of kdesupport would lie with the distribution makers. I *think* KDE currently needs mimelib and libaudio from kdesupport on my system even though it contains other packages, these aren't \ncompiled by default as I guess I already have them. Surely we can just make these few libs a dependancy for KDE base?\nLook at it this way, we need openssl for crypto support in konqy, this is an important feature, why's it not in kdesupport? What about Lesstif for netscape plug ins or a java implimentation? Of course mimelib and libaudio are version critical to the correct operation of core fuctionality of KDE where as the other libs mentioned above provide \"nice to have\" functionality. \nEven so, I can see this only from two sides, which are:\n\nA) Techie compiling KDE from source. If he/she has the knowledge to compile KDE from source they should have the knowledge to get the dependant packages. If he/she doesn't have the ability  to read figure out why configure complained about a missing package and read the README what are they doing compiling KDE themselves.\n\nB) Average Linux dude that installs from a distribution. The distribution just has to package KDE correctly with the correct libs as a dependancy on the KDE RPMs / APTS / slackwware packages. This can't be that hard, the distro people should get this right (I'd hope or else their in the wrong game), apart from that David Faure / Mosfet are at mandrakesoft, Bero is at \nredhat, surely there are employees of the other major distros involved in the core team that could advise their colleauges.\n\n2. Seperate all the apps out of their current groups so that for example kdenetwork, utils, multimedia etc are no more. A solution to this would be to sort CVS out in to two groups:\n\nA) released: These are apps that are currently maintained, are actively developed and considered up to a high level. Basically 99% of the apps that now make up the various parts if the distribution. New work is commited in to HEAD as it is currently and releases are taged when the author(s) feels the \napp has new fuctionality or a large bug sqashed. Of course releases should be compatable with the current stable core release.  When a new release is made the tag should be packaged, for now this is a source only package in tar.gz \nformat (bare with me, works with the installer..). It must contain a file called KDE_RELEASE, this file has a strict layout or is in XML and contains at least all of the following information:\n\nname\ndescription\nversion\nchanges\nimportance\n\nOnce the tag has been placed and the KDE_RELEASE file added a script can be run against CVS to automatically package it, copy it to the KDE FTP site and remove the old version, it then gets mirrored from the KDE main site. The ftp site would be layed out the same way CVS is:\nftp.kde.org:     |\n                 |\n\n                 - stable\n                         |\n\n                         -kmail-1.9.99.tar.gz\n                         -ark-1.2.3.tar.gz\n                         -konsole-1.0.1.tar.gz\n                         -kuser-1.9.8.tar.gz\n                         -....\n                         -....\n                         -....\n\nOnce every hour a script can be run on the ftp server to extract the KDE_RELEASE file from any new packages if they exist (based on time created I guess). This would update a file called KDE_RELEASES that lives in the root of stable, obviously this file contains up to date information on all released packages.\n\nIn it's self this is a far better way to release KDE for the power users out there who want a stable release but can compile what they need from source and I'm sure it will help the distros to package KDE better. This is not the end goal, the installer is.\n\nThe Installer: The installer is a simple gui app that acts with two functions, firstly it can install KDE once the base packages are in place and secondly it can keep KDE up to date. In the KDE root directory on the local machine lives a file called KDE_INSTALLED, this is a DB (XML / plain text) of \nall packages installed on the machine and is created when kdebase is installed. Its updated by the \"make install\" script when a package is installed or as a post install script from RPM /apt etc. When the installer is run, it loads the KDE_INSTALLED file and asks where it should check for \nnew packages, either locally or the Internet. Locally brings up a location selector, so that for example magazine CDROM can contain packaged updates (a lot of people are still on dial up) or to install 3rd party software (something not released by KDE e.g. kcpuload). The Internet option connects to the ftp.kde.org and downloads a list of current mirrors, the user selects \na mirror and the KDE_RELEASES file is download. The installer compares this with KDE_INSTALLED and shows updates to installed packages + info such as importance, description etc. If the user wants to install a selected package \nthey select download & install, the installer then gets the package and performs a configure, make, make install. This exact commands should be configurable so that --enable-final, install-strip or lib paths could be could be configured for example.\n\nAs for installing KDE, the installer should have two radio buttons one for update that works as described above and only checks for installed packages and the second lists all packages where multiple packages can be selected and installed. Further options are for the installer to live in the system tray and check at regular intervals for updates from their favourite mirror, when an update is availible for an installed package it alerts the user.\n\nThe future: I think up to now, this gives KDE a solid base to expand this idea, sorts out CVS and it certainly lets the users decide what they want to install instead of the ever growing kdenetwork, kdeadmin etc packages. It helps the distributions and is atleast platform independant. The big problem \nwith this is that packages need to be compiled on the users machine so its not perfect for everyone especially joe average. However, if the installer does some checks on the development enviroment first it would satify a lot of KDE users out there as I'm sure most have compilers installed?. The other flaw is that it doesn't allow uninstalls to be performed which is I think an \nimportant feature, this could be handled by having the KDE_INSTALLED DB keeping a list of files that \"make install\" installed and having the app update the DB if it updates a global config or adds files. This is  not really an elegant solution and duplicates functions of other software (RPM \n/APT)\n\nI haven't tought the next bit through 100% but the idea is to have the first generation installer to get the source as above but to build RPMs and install them using KDE's own branch under the RPM DB. This way the packages are able to uninstall via RPM, this can be expanded later to use the installer to \ncheck for updates to bin packages but bin packages create a new set of problems like install paths etc. I personally like RPM but some distros don't so there are politcal issues I guess, I do remember some distro writting an apt to RPM layer so that any packages could be used\n\nB) going back to the CVS stuff, keep the nonbeta part of CVS, might be better to call it unreleased and move it to its own branch (if it's not already). Once a package in unreleased proves it stuff it can be moved to released and a first release can be made. Because everything would be a seperate package \nit means that moving a package from nonbeta doesn't have to bloat up a package group as is the current situation.\n\nI think the installer app should be fairly easy to do and I hope there is someone with the time to impliment it but getting CVS in the shape where this will work might be a little hard?\n\nAnyway, the stuff above is just a few ideas that I can see from the outside, there may be very many reasons why this can't be done, but as I'm not that involved with KDE, I of course don't know about them. If this is some use, please feel free to forward it to the kde developers / mailing lists.\n\nRegards,\n\nDavid."
    author: "David"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-14
    body: "hi,\njust two things to notice:\n\na. loki has done an open sourced installer etc for their games\n\nb. i think, ibs is working for in installer too.\n\nmaybe it could help to look at these too.\n\n-gunnar"
    author: "gunnar"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-17
    body: "sorry ibs should be ibm\n-gunnar"
    author: "gunnar"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-14
    body: "I have some additions:<BR>\n- check what is installed.<BR>\n- download required packages.<BR>\n- send optionaly an e-mail when updates are available.<BR>\n- check for available diskspace.<BR>\n- select option for stable oder newest beta to download/install.<BR>\n- uninstall options for no more used packages.<BR>\n<BR>\nThanks. It's a great idea to make an installer!\n<BR>\nStephan"
    author: "Stephan B\u00f6ni"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-15
    body: "....and a cron-job, that runs on my machine at night to check the web for new packages.\n\nSomething like Autoupdate.  I never used it but what I read was nice."
    author: "Michael"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-14
    body: "<p>Ok, everyone, I need to clear some stuff up with you!</p>\n\n<p> * <strong>This project is Open Source!!! It can be ported to ANY OS!</strong></p>\n\n<p> * <strong>This installer will support MUCH more than just Redhat. I am not Redhat-centric!!! I use a Redhat system, but I understand other people like other distros, so I fully support them. This is where I will also need help. I dont use Debian, *BSD, Slackware.</strong></p>\n\n<p> * <strong>The installer isnt meant to be a replacement for the installs of KDE, but some of you think that the distro should handle it, listen to this:</strong></p>\n<p>     What happens when KDE releases a new version? You have to download the newest version of your distro and install it just for KDE? Hell no! Also, distros dont properly install KDE sometimes, or sometimes they release pre-release versions (IE: Mandrake 7.2).</p>\n<p>   <strong>If you dont like the installer, nor the idea, dont use it!</strong></p>\n\n<p> * <strong> PLEASE, I REPEAT, PLEASE discuss this on my forum that is made JUST for this discussion! <a href=\"http://www.rox0rs.net/forums\">http://www.rox0rs.net/forums</a></strong></p>\n\n<p>This concludes my rant, if I missed something, Please let me know on my forum!!!</p>\n\n<p>      Nick Betcher</p>"
    author: "Nick Betcher"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-14
    body: "The story was posted on apps.kde.org so why can't we discuss it on apps.kde.org?"
    author: "David Johnson"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-15
    body: "Indeed.  Sorry Nick, but you should have expected this.  dot.kde.org is a discussion forum for the KDE community.  Expect the KDE community to discuss important KDE issues right here!\n\nHope you stay with us, I think you have a worthy project on your hands."
    author: "KDE User"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-15
    body: "Nick,\n\nI don't think anyone is attacking you here, they are pointing out features they would like to see and possible issues. You comment on your forum yourself that you will be posting this on the dot and expect some feedback from there, this is what you are getting. It's very difficult to point things out that could be wrong when someone is offering to code a needed application for free so please keep in mind that this meant as constructive: It seems that you are fairly new to software development, maybe it would be better to plan exactly how the installer is going to work, what it will do in the first release and how you can design it so that it's extendable later.\n\nYour comment about porting the installer to any OS doesn't make sense, it will not have to be ported, it should be able to support all of the systems KDE runs on at some point, not in the first release maybe but the design of the application should take this in to account.  \n  \nYour comment about downloading a new Distro when a new KDE is released worries me, KDE has a large and excellent packaging team, when a new release of KDE is made it is packaged by this team so it can be installed simply on each distro. The point people make about the distros is very valid as they should handle it, this task is simple software distribution and hence that's where \"distro\" comes from. See my early post as too why they don't do it.\n\nAlthough its tempting to just fire up QT designer and release some screenshots, there needs to be a plan of attack for the under lying code, what to you want to achive and a basic outline how you will obtain your goal. This really is a big job for a first project if you want to do it right and if you want to have people use your project you need to do some thinking. You said \"If you dont like the installer, nor the idea, dont use it!\", I doubt Nick, many people will because they are telling you want they need, if you don't listen and then provide what they need, they of course won't use it. I think by what you posted above you told a lot of people a lot about yourself and how this project will turn out."
    author: "David"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-14
    body: "Ithink this is great but, what is needed is an installer like <a href=http://www.ximian.com/apps/redcarpet.php3>Ximian's RedCarpet</a> and <a href=http://www.eazel.com>Nautilus'</a> that does dependency checking.  This will go a long way towards getting newbies into the loop.<p>\n\nEven when I use Mandrake's updater, I still sometimes have dependency issues and Mandrake only releases RPM's that they've tested and approved.<p>\n\nNot all good KDE apps are going to make into Mandrake's list so, someone like kdeapps needs to support this installer so that you have a good list to choose from.<p>\n\nM2C<p>\n\nEric"
    author: "Eric Caldwell"
  - subject: "Debian"
    date: 2001-02-14
    body: "Sorry, Debian users do not need such a nonsense."
    author: "Dirk Manske"
  - subject: "Re: Debian"
    date: 2001-02-15
    body: "But a installer which give's you a good frontend an additional information about bugfixes (why should you update, etc.) and about new software and major new versions available would be an interesting addon. The dependency system of the debian port should of course handled by apt..."
    author: "Uwe Klinger"
  - subject: "Re: Debian"
    date: 2001-02-15
    body: "<I>Sorry, Debian users do not need such a nonsense.</I>\n\n<P>Since when has KDE become a debian-only project? Or Linux-only for that matter?</P>"
    author: "GlowStars"
  - subject: "Re: Debian"
    date: 2001-02-15
    body: "KDE is a group of packages. Why need KDE packages a special install frontend? Installation/updating are tasks of the installer of the distribution."
    author: "Dirk Manske"
  - subject: "Re: Debian"
    date: 2001-02-15
    body: "<I>Installation/updating are tasks of the installer of the distribution.</I>\n\n<P>What if there is no distribution?  You are still thinking in linux-terms."
    author: "GlowStars"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-15
    body: "HI! this is a great Idea! I was going to comment that it would be great to have a standard list of distro's supported so a new or intermediate (me) user doesn't need to figure out where the files need to go. but, the screenshots look like it does that allready.\n\nthank you for reading my post!\n\nLance D."
    author: "L.D."
  - subject: "Re: KDE Installer Project"
    date: 2001-02-15
    body: "A nice feature would be this:\nIf the installer downloads all necesary files from the Internet, the user should be able to choose where to put the downloaded files, and when the network connection failes (e.g. modem hangs up during donwload or similar problems) the user should be able to resume the download/installation at a later time. It would also be nice if the installer could work from a local directory, So when a user wants to reinstall kde, he/she doesn't have to download everything again.\n\n(This is something I missed in the Netscape Installer: I didn't know where Netscape downloads the files to, and when the modem hangs up during the download, I had to start all over again.)"
    author: "rinse"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-15
    body: "While I am no brainiac I know this; Windows will forever kick ass on the home user's PC so long as Linux targets techies and power users. People who come home and use their computers don't care about why they can't check their email, just that they can't do it. While I read through others comments about how great it is to install from source I think it was so nice to copy *http:/go-gnome.com* from Ximian's website and get Gnome on my box and have it working without a hitch is amazing for Linux. Users like myself do not want to have to deal with what packages do I need and don't. Linux came with 1,500 Apps! What more could I need? GAMES!!! Give me 3D Fragfest 2001! Granted, I do not need 1500 apps, but at least I know if I do need it, I sure don't have to download it on my crappy modem connection. \nTo the Linux Community: You guys are tackiling the impossible with nothing, and my respect to you. But if you get anything out of this message, simplicity is it."
    author: "bit0101"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-15
    body: "And people who develop for Linux, and target techies and power users, don't care the slightest bit that Windows will forever be on a 'typical end-users' desktop.\n\nI for one don't care at all:)  I found my salvation a year and a half ago.  I'm in heaven."
    author: "James"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-16
    body: "I love Linux, don't get me wrong. I am learning all I can about it and I think it is great for everyone, home and enterprise. My point is that it is this attitude that will stun Linux. The typical end user is everyone, and wouldn't be nice to live in an open source world? While I am glad you found your salvation, there are 35 million other people who haven't. I say let this installer develop, this is one more step forward for Linux and one less step for Windows."
    author: "bit0101"
  - subject: "Re: KDE Installer Project"
    date: 2001-05-01
    body: "bonjour \nj'ai des prol\u00e8mes pour installer linux-Mandrake 7.2 sur mon PC. C'est un AMD Duron 700MHt \nje ne sais pas quel est le probl\u00e8me pourtant j'ai partitionner mon disque\nest ce que vous pouvez m'envoyer un manuel pour installer?\nmerci"
    author: "Aboudi Effa Marie-Paule"
  - subject: "Re: KDE Installer Project"
    date: 2003-03-16
    body: "Il suffit tout simplement de me contacter :Car je suis un specialiste dans le domaine.Je travail sur ce genre de programme a l'Universite Technique de Hamburg-Harburg."
    author: "JOEL   B. ESSOMBA"
  - subject: "Menudrake"
    date: 2001-02-15
    body: "I would love to see an installer that can convert the KDE menus to the Mandrake menufiles.  It's tough when every install (accept a mandrake rpm) makes .desktop files and then then Mandrake blows them all away.\n\nOn a side note, does anybody know of a way to easily convert kde menus to the Mandrake/debian menufiles?  Menudrake is pretty horrible."
    author: "Whitney Battestilli"
  - subject: "Re: Menudrake"
    date: 2001-10-22
    body: "You just never know what you'll find out here in \"internet world\"!!  Love you - your mother"
    author: "Terry Battestilli"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-15
    body: "Could you include a check for free diskspace and recommended memory on the very first installation sheet?\n\nThis way the user can abort immediately (unlike most software on the MS platform) or remove, move or whatever needs to be done if there is not enough diskspace."
    author: "Haakon Meland Eriksen"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-15
    body: "Great idea for newbies like me !\n\nHere is my need :\n\n- I work for a large company so :\n\n- my machine is a Sun workstation with Sun OS\n\n- I don't have 'root' access and need to call my system'admin to complete the installation (setuid...)\n\n- I need to install kde on my own account\n\n- I'm really too much a novice to complete the installation from source code.\n\n- Actually, I'm not very much interested in the technology behind kde/linux/... I'm a simple user.\n\nIs there a way to make the installer so much 'rookie-proof' ?\n\nThanks for your contribution !\n\nDiego"
    author: "Diego"
  - subject: "Slick....."
    date: 2001-02-15
    body: "Very good! Well done! Looks too much like Windows, but realy very solid. I only wish I programed :-(\n\nGood luck!\n\n-Wes"
    author: "Wes yates"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-15
    body: "<p>\"He also needs other developers to get involved, as he is relatively new to C++/OO programming.\"</p>\n\n<p>Please help! I need everyone's help. I just realized last night when I was in bed that more than 1.5 million people are counting on me. ME! Me alone. <strong>/me runs around the room in panic.</strong> Webpage designers, code writers, Qt Designer people, documentors. I need serious help. It cant be done alone. I would appreciate any help at all.</p>\n\n<p>Sincerely,</p>\n<p>     Nick Betcher</p>"
    author: "Nick Betcher"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-16
    body: "Sorry Nick,\n\nread all the posts! KDE DOES NOT NEED AN INSTALLER! Where would we go if every programm had an insataller (like netscape and Staroffice have rightnow). Installer ist distro stuff. You need to handle Solaris *BSD and all the otyher *xes. This is _DEFINITELY_ a responsibillity of the distro. Ximian realized that after they screwed up with their stupid Helix Installer. Don't repeat their mistake!\n\nCheers"
    author: "Rijka Wam"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-15
    body: "Very nice concepts in your screenshots.  One thing I was wondering if you could add (in post 1.0 releases) would be a system to check for necessary libraries and such.  Once that is in place, myself and others could help create automated ways for locating, downloading, compiling, packaging and installing all the missing pieces...  Then build KDE itself.\n\nI see that as a great way of helping KDE end up on HP and Sun machines.  Thoughts?"
    author: "Brandon Darbro"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-15
    body: "yeah , with my 1.4 ghz machine the compile of kde will be a snap ..."
    author: "peter"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-15
    body: "what the installier should also do is :\n\nfind an installed version of KDE (older)\n\nconvert the Userdata (Mail/adressbook/licq)\n\nimport other Userdata (Outlook,ICq)\n\nand give the options to upgrade to the new\nversion then .\n\n\nis this possible ???"
    author: "peter"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-15
    body: "<p>Notice to everyone interested:</p>\n\n<p>A lot of heat is coming from slashdot. One person mentioned that I am new to programming with a very young attitude. I will admit that my programming needs work, but do I see anyone else taking this project up? No. I originally never wanted this much PR. Its horrible. I will go through with this to the end. Please don't dought me. I do appreciate the optimisim from a lot of you.</p>\n\n<p>As for me being young, I admit it. I am 16 years of age. Does that make me unable to write code? Am I doomed from the start just because I'm young? If some of you say 'yes', I would like to please know why. There are a few people my age (and even younger) that create quality KDE applications that are in your local copy of KDE. I wont name names because those people don't want the heat from slashdoters and others commenting on their youth.</p>\n\n<p>Also, I would like to make it clear that this project was never a one-man project. I only wanted this post to stay on the dot so that I could get helpful comments, and people willing to help me write code. Also, being a 16 year-old, I attend High School daily. You may think that is going to put a dent into my programming, but like I said before, this isnt a one-man project.</p>\n\n<p>At this point I appreciate all of your comments, dont get me wrong. Please continue to comment. I wont make anymore complaints about the slashdot ones because I'm not reading them anymore.</p>\n\n<p>If you would like to talk to me, please do so. I will be (active) on IRC from 3:00PM CST to 10:30PM CST on irc.openprojects.net in #kde-users.</p>\n\n<p> Thank you all again, I hope I can fullfil all of your feature requests.</p>\n\n<p>     Nick Betcher</p>"
    author: "Nick Betcher"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-15
    body: "Nick,\ndon't pay any attention to the slashdotters, they have KDE-envy.\nStay here, where you are among friends.\nI am also amazed buy the overwhelming interest in your project, it shows that there is real need for this.  Therefore other, more experienced developers, will come to your help."
    author: "reihal"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-15
    body: "P.S.\n\nRemember the Linus Method: Don't take it too seriously."
    author: "reihal"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-15
    body: "Hey Nick,\n\nI don't think that age is a problem regarding programming skills.  However it's sometimes a problem regarding how to get along with bad comments.  From your postings I would suggest you take it (much more) easy.  You shouldn't tell them \"you don't like, don't take it\".  It's the way around: If you think a comment is not useful for the project ignore it...\nAbout the project:  Before staring to discuss what kind of icon, fonts,... there should be a plan about:\n\n* scope for the first release\n* own backend / existing backend\n* some issues mentioned above...\n\n\nMy opinion:  I would already be glad if I could easily update my KDE 2.0 to the latest stable/unstable version.  Something simple like:\nwhich distro do you have: _______\nwhich kde-version you want: _____\nwhat kind of installation: (developer/basic)\n\nthat would be just fine for version 0.0.1\n\nOf course many people will say, that's too simple.  Then you can say: so let's improve it.  But the point is YOU have something which at least works.  There will be no more discussion about age and so on...."
    author: "Michael"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-16
    body: "Hi Nick,\n\nI don\u00b4t know anything about your prgramming skills and i don\u00b4t know if this project will have success. But i do know that your attempt of writing an installer is much more useful to all of us than the rants of these people. The only thing they can do is critisize others instead of producing some code themselves.\n\nRoland"
    author: "Roland"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-16
    body: "With my 32 years i know that your programming skills are much more better then the mine...."
    author: "Stephan B\u00f6ni"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-17
    body: "Nick: Ignore slashdot's flamers and complainers.  Ignore this forum's flamers and complainers.\n\nIf you think KDE needs an installer, do it.  Ignore all the idiots and self-important blowhards.\n\nDon't bother trying to justify yourself to them, or worrying about what they're saying at all.  Just do it, and if you did well, the accolades and thanks from grateful users will come."
    author: "Neil Stevens"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-15
    body: "1st)  Here's another useful possibility.  If the installer could facilitate having CVS/alpha/beta installs that DON'T replace the stable installs, I myself would be much more eager to install and test new versions and features.   As it stands I'm pretty reticent to get into the CVS stuff, because I don't know enough to get a setup where new libraries coexist with old libraries, etc.  But if the installer could facilitate this, I could be testing a daily build with no problems - it breaks in some way, I just quit KDE2.5.unstable, possibly reboot, and choose KDE2.1.stable from KDM. \n\nI'd also like to voice my support for having the installer help you along with the process of compiling, as compiler optimizations combined with being able to eliminate apps and features you don't need would give everyone a tight, fast KDE desktop to show to their friends\n\n2nd)\nNick,\n\nKudos to you for generating energy and discussion on this much needed subject, even if you never write a single line of code.\n\nCheers,\n\nEric"
    author: "Eric"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-15
    body: "Shouldn't it be text-based ? Imagine a GNOME user with almost no kde libraries at all (at least QT should be there). Or after a problem with X. Or someone that installs Linux/BSD from NULL.\n\nA <b>KDE Updater</b> can be a KDE 1.x application, as its purpose is to update into newer version. But a <b>KDE Installer</b> may only assume that the user might only have X Window."
    author: "Ariya Hidayat"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-16
    body: "you can link it static against Qt like opera.\nthis will work for every X user.\nbut of course the KDE installer shouldn't use the KDE libraries. =)"
    author: "Spark"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-16
    body: "I Just hope that it will work across distributions and platforms. \nI myself have quite a few x86 based systems running Slackware, QNX, FreeBSD (and I'm setting up a new Solaris box as well), an AlphaServer running Slackware and a AlphaStation running Debian. None of those are RPM based, so how will this installer work on my systems - not that I think I myself would use it, but it's something that should be thought about.\nIf the installer is going to be really usefull it should run on all the platforms that KDE runs on and it should be (Linux/*BSD)distribution independant!"
    author: "Jesper Juhl"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-16
    body: "*sigh* (once again) The installer isnt Redhat-centric. It will support much more than RPM based systems. Enough said, hehe."
    author: "Nick Betcher"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-16
    body: "Hi!\nplease separate the languages!\nmost users will only need english + their local language. (see SuSE packages)\ncu ferdinand"
    author: "Ferdinand GAssauer"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-16
    body: "I don't really think an RPM installer interface is needed. What would really be welcome is a GUI for compiling and installing all packages from sources/cvs with automatic updating, cvsup, diff installing..."
    author: "Dan Armak"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-16
    body: "<p>This installer might one day support CVS, but as of now, I am taking it one step at a time. There are plenty of projects and programs out there that will compile and isntall CVS for you. Please look around for them, I dont know any off the top of my head. The best place to probably look for them is in kdenonbeta or kdesdk! kdenonbeta is always full of goodies, like the installer :)</p>\n\n<p>Heh, if you think downloading and installing KDE from the CVS is hard, try maintaining your project in it... *grrr* :)</p>\n\n<p>Bye all!</p>"
    author: "Nick Betcher"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-16
    body: "Hi Nick!\n\nPlease don't listen too much to all these feature requests... I think the secret of any successful project is to Keep It Simple. It's always better to have one thing that works perfectly than a hundred things that work poorly. Other things can always be implemented later on.\n\nAs for CVS, that is NOT for end-users. If you need a fancy GUI installer in the first place, CVS builds are NOT for you. CVS checkouts aren't even guarranteed to compile, damnit ;-)\n\nKeep going and take your time :-)"
    author: "Haakon Nilsen"
  - subject: "Re: KDE Installer Project"
    date: 2001-02-16
    body: "s/isntall/install"
    author: "Nick Betcher"
  - subject: "Cool idea"
    date: 2002-07-01
    body: "Great idea, I actually had posted this idea as a bug, only to get a hurl of abuse from a particular kde developer. :-( .\n\nAnyway, could the installer maybe also have other features like ximian red carpet, such as news on new/updated kde projects, with the ability to download/update them on your computer? And would this be like red carpet where you have a long list where you tick in the nice boxes? If so, great, I can't wait till it's ready.\n\nAlso, instead of calling it plain boring 'KDE installer' , why not call it something like 'KDE Orange Karpet' ?????"
    author: "Max"
---
There have been countless requests from KDE users, on the dot, on the lists, and even <a href="http://linuxtoday.com/news_story.php3?ltsn=2001-02-12-001-04-OP-CY-DT">elsewhere</a>, for a KDE Installer and Updater.  <A href="mailto:nbetcher@usinternet.com">Nick Betcher</a> (aka Error403) has stepped up to the challenge and now needs your help to make this project really happen. His current code is in <a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdenonbeta/kdeinstaller/">CVS</a> and the project is in active development. The install starts off with an <a href="http://www.rox0rs.net/first.jpg">intro/detection</a> screen, prompts the user for the <a href="http://www.rox0rs.net/second.jpg">type of installation</a>, prompts for the <a href="http://www.rox0rs.net/third.jpg">destination</a> of the KDE installation, and then prompts for the <a href="http://www.rox0rs.net/custom.jpg">packages to install</a> (see all the screenshots <a href="http://www.rox0rs.net/kdeinstaller.html">here</a>). The project is based on pure Qt, so that the statically linked binary will be small, and as of now the network code to download RPMs is in CVS. So why does Nick need your help?  It's important to him that users get their input in for this project and that he has a better understanding of what to aim for regarding look & feel as well as packaging issues.  He also needs other developers to get involved, as he is relatively new to C++/OO programming.  Nick has provided a convenient <a href="http://www.rox0rs.net/forums">forum</a> for people to voice their opinion.







<!--break-->
