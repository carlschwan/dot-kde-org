---
title: "ZDNet:  Review of KOffice 1.1"
date:    2001-09-15
authors:
  - "Dre"
slug:    zdnet-review-koffice-11
comments:
  - subject: "Hyphenation?"
    date: 2001-09-15
    body: "They don't even mention the lack of hyphenation in KWord, probably because this feature is not that important for English language users. In Germany (as in other countries with similar long words and compounds like Russia) nobody in an office or corporate environment will seriously consider KWord for professional use without a properly working hyphenation.\n\nMaybe this could be borrowed from TeX?\n\nRegards,\n\nThomas"
    author: "thd"
  - subject: "Re: Hyphenation?"
    date: 2001-09-15
    body: "Just in case this should not be possible, perhaps the freely available\n_Moby_Hyphenator_\nhttp://www.dcs.shef.ac.uk/research/ilash/Moby/\ncould be usefull?"
    author: "Karl-Heinz Zimmer"
  - subject: "Re: Hyphenation?"
    date: 2001-09-15
    body: "Excuse me...\nWhat is exactly Hyphenation ?\n(I'm french and I don't know this word, dictionnary's translation doesn't make sens)\nThanks"
    author: "athom"
  - subject: "Re: Hyphenation?"
    date: 2001-09-15
    body: "It means splitting word into two parts, one part at end of line and other one at beginning of next one."
    author: "proc"
  - subject: "Re: Hyphenation?"
    date: 2001-09-15
    body: "well simply put, its the little dash you put when your words run from a line to another.\n\nIn french : c\u00e9sure.\n\nBTW as far as I am concerned, MS Word should not be considered as having that feature : compared to (La)TeX, it is really a sad joke. But generally typesetting in Word just sucks. In fact, the whole WYSIWYG approach of Word is broken. Thankfully, Koffice is frame-based.\n\nI love KDE, I think it is brilliant, but as for KOffice... Well, certainly it'll become the best office suite in the same way KDE is the best desktop (forget mac OS X) But I simply won't use it until the end result is as brilliantly TypeSetted as A latex doc. Even for small dumb thing like short letters.\n\nAnd yes, I use LyX. (except I reedit the stuff for tables.)\n\nMaybe It is a dumb question, but why can't Tex be used for typesetting by koffice ? Yes, r-time typesetting (for WYSIWYG) would be slow, but that would be a smart use of my extra processor cycles."
    author: "hmmm"
  - subject: "Re: Hyphenation?"
    date: 2001-09-15
    body: "Why should everything have to be compared to Microsoft.\nWith all the legal problems M$ has on its apps and more, I would think they could point out what koffice has to offer compared to gnome office apps or other third party office apps..\nsince when is it always compared to a microsoft standard, since when does M$ make the standards...\nNot everyone uses just M$ office, lots uses other third party office apps because M$ office is so limited.."
    author: "Harold"
  - subject: "Re: Hyphenation?"
    date: 2001-09-15
    body: "Um, he's comparing Koffice to TeX (an open source program), and saying that MS-Office's hyphenation is a joke compared to it. Were you meaning to reply to the post above?"
    author: "Carbon"
  - subject: "Re: Hyphenation?"
    date: 2001-09-15
    body: "> Maybe It is a dumb question, but why can't Tex be used for typesetting by koffice ? Yes, r-time typesetting (for WYSIWYG) would be slow, but that would be a smart use of my extra processor cycles.\n\nKWord haven't good wysiwyg (at least, don't work for me, when I change font), but it is not problem for me. I don't need wysiwyg. And I like the idea using tex for typesetting in kword, without realtime wysiwyg. Maybe as an optional feature."
    author: "Tomas Blaha"
  - subject: "Re: Hyphenation?"
    date: 2001-09-15
    body: "KWord 1.1 does not have true WYSIWYG, but the CVS KWord does and it is working fine AFAIK.  Next release it will be in there."
    author: "not me"
  - subject: "Re: Hyphenation?"
    date: 2001-09-16
    body: "Much better than WYSIWYG is WYIIWYG -- what you *imagine* is like you get. Like TeX. Or like writing HTML instead of using editors like Frontpage. Or like using gri and GMT."
    author: "Roberto"
  - subject: "Re: Hyphenation?"
    date: 2001-09-17
    body: "This is usually called WYSIWYM: What You See Is What You Mean. It is the rallying cry behind Lyx. If you like this, I recommend you try Lyx, which does this very well.\n\nThe KWord developers already know about Lyx. They know what Lyx tries to do and that Lyx does it well. KWord does not work the same way specifically because it fills a different need: the need for a WYSISYG word processor.\n\nPenguin Command is not a very good replacement for Quicken. But then again, it was never meant to be one."
    author: "Jerome Loisel"
  - subject: "Re: Hyphenation?"
    date: 2001-09-16
    body: "You are correct. I'm from the United States and have never felt a need to use hyphenation. I only use it when writing notes on paper and run out of room (I have big handwriting. (-: )\n\nI've read threads about \"borrowing from TeX\" before and it sounded like this would be very complicated."
    author: "Ian"
  - subject: "Re: Hyphenation?"
    date: 2001-09-17
    body: "It shouldn't be too hard to borrow the TeX hyphenation algorithm.  (I'd also suggest the TeX line-breaking algorithm, as it's one of the best out there.)\n\nWhile these algorithms may be more complex than what is currently in use, they are also extremely well-documented."
    author: "Alan Shutko"
  - subject: "Re: Hyphenation?"
    date: 2006-06-05
    body: "There are two types of hyphens. soft hyphen -- end of a line. other ,the hard hyphen which is used to combine two unrelated words put together which make sense together. well-known person, etc."
    author: "Laks"
  - subject: "Re: Hyphenation?"
    date: 2006-06-05
    body: "Some more information: \nhttp://en.wikipedia.org/wiki/Hyphen\nhttp://en.wikipedia.org/wiki/Dash (which I assumed was the same thing, but it isn't)"
    author: "Paul Eggleton"
  - subject: "thesaurus?"
    date: 2001-09-15
    body: "Just found this on freshmeat:\n\nhttp://www.aiksaurus.com/kde/\n\nBut hyphenation is realy a missing feature for a language like German. I dont like automatics in standard writing, so I deeply miss something like a soft-hyphen  (like CTRL+\"-\"). At this time I use koffice for making a report (~50 pages) and a presentation. Both kpresenter and kword needs better support for displaying vector-drawings, maybe an EPS-displayer based on ghostscript. Currently you can only use a Kontour-KPart in KWord. But also there are scaling/displaying \"issues\". I did not found a solution for kpresenter and have to use bitmaps for my illustrations. Last missing thing are independent auto-counters for tables, figures, etc.\nOn the other hand, workung with koffice is intuitive and fun and it is realy stable."
    author: "Thorsten Schnebeck"
  - subject: "Re: thesaurus?"
    date: 2001-09-15
    body: "Every hyphenation module should include soft hyphens, of course. Ventura Publisher even offers to use soft hyphens exclusively for individual paragraphs or the whole text. You can also set the language for all writing tools on a per paragraph basis, including hyphenation, spell and grammar checker -- way to go for KWord as well I think.\n\nThomas"
    author: "thd"
  - subject: "ZDnet incompetent"
    date: 2001-09-15
    body: "\"... whereas StarOffice is a Java-based application.\""
    author: "someone"
  - subject: "Re: ZDnet incompetent"
    date: 2001-09-15
    body: "This is true though."
    author: "Jimmy Saville"
  - subject: "Re: ZDnet incompetent"
    date: 2001-09-15
    body: "Not much of it though. When I last installed Star Office, I didn't have a Java VM and it worked fine. However, it wouldn't run the help program, which was Java based. I really think it's just C++ for the part we actually consider \"Star Office\"."
    author: "David Nusinow"
  - subject: "Re: ZDnet incompetent"
    date: 2001-09-15
    body: "Shut up you wanker."
    author: "Janne"
  - subject: "Re: ZDnet incompetent"
    date: 2001-09-16
    body: "http://www.openoffice.org/FAQs/main_faq_new_p3.html#11"
    author: "someone"
  - subject: "I find it annoying..."
    date: 2001-09-15
    body: "That rest of the world must struggle to fit in to MS-world. I'm talking about those MS-office import/export-filters!\n\nI hope that DOJ mandates that MS opens their proprietary file-formats. What we need is a open standard for files, so text written in Word2000 would look and behave the same in KWord, StarOffice, Abiword... And vice versa of course..."
    author: "Janne"
  - subject: "Re: I find it annoying... why?"
    date: 2001-09-15
    body: "Umm .. they are open, and freely available on MSDN."
    author: "Jimmy Saville"
  - subject: "Re: I find it annoying... why?"
    date: 2001-09-15
    body: "No they are not you liar."
    author: "Janne"
  - subject: "Re: I find it annoying... why?"
    date: 2001-09-16
    body: "Why don't you go troll someone else's message board?"
    author: "Carbon"
  - subject: "Re: I find it annoying... why?"
    date: 2001-09-16
    body: "People, this is not me. Someone is using my name!"
    author: "Carbon"
  - subject: "Re: I find it annoying... why?"
    date: 2001-09-16
    body: "Someone is using *my* name!!! \n\nOh, wait... it's me.\n\n--\nEvan  ;)"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: I find it annoying... why?"
    date: 2001-09-16
    body: "Actually, it is me. I was attempting to subscribe farther down the list, to the post where the guy actually was trolling.\n\nBTW, could someone please implement accounts on KDE News? Otherwise, people can easily fake stuff like above :-)"
    author: "Carbon"
  - subject: "Re: I find it annoying... why?"
    date: 2001-09-30
    body: "Or like this?"
    author: "Carbon"
  - subject: "Re: I find it annoying... why?"
    date: 2001-10-01
    body: "Yes, even like that!"
    author: "Graphite"
  - subject: "Re: I find it annoying... why?"
    date: 2001-09-16
    body: "OK, someone is using my name here! I did NOT write that message!"
    author: "Janne"
  - subject: "Re: I find it annoying... why?"
    date: 2001-09-16
    body: "Oh look, someone is using your name again!"
    author: "Janne"
  - subject: "Re: I find it annoying... why?"
    date: 2001-09-16
    body: "In fact, you can type in any name you like."
    author: "Saddam Hussain"
  - subject: "Re: I find it annoying... why?"
    date: 2001-09-16
    body: "See?"
    author: "William H Gates III"
  - subject: "Re: I find it annoying... why?"
    date: 2001-09-16
    body: "STOP USING MY NAME!!!"
    author: "William H Gates III"
  - subject: "Re: I find it annoying... why?"
    date: 2001-09-16
    body: "What?? That's MY name!"
    author: "Janne"
  - subject: "Re: I find it annoying... why?"
    date: 2001-09-16
    body: "Are you the <i>real</i> Jimmy Saville?"
    author: "Carbon"
  - subject: "Re: I find it annoying... why?"
    date: 2001-09-16
    body: "No, I'm the real Jimmy Saville!"
    author: "Janne"
  - subject: "Re: I find it annoying... why?"
    date: 2001-09-16
    body: "STOP POSTING AS ME!!! STOP USING MY NAME!!"
    author: "Janne"
  - subject: "GROW UP!"
    date: 2001-09-16
    body: "Each and every one of you!"
    author: "Janne"
  - subject: "VBA - just say no!"
    date: 2001-09-15
    body: "KOffice developers, please don't _ever_ inflict VBA on your users ;-)"
    author: "Mark Roberts"
  - subject: "Re: VBA - just say no! (why not?)"
    date: 2001-09-15
    body: "Before flaming me - please read below..\n\nYes, VBA is one of the major reasons that there are those viruses found on Windows world with office. Write few lines - and you got yourself a virus.\n\nBut look on the other side - it allows to people who don't know almost anything about programming write a small \"applications\" which makes MS Office quite usable for secretaries (think about \"smart documents\" which lets you have forms to fill info and be processed with MS Access)...\n\nA smart scripting language could make Koffice very usable for secretaries and staff who need to fill forms, and process documents (multiple recepients documentation for example comes to mind). KDE Developers know very well about security so security implementations could be added here.\n\nHetz"
    author: "Hetz"
  - subject: "Re: VBA - just say no! (why not?)"
    date: 2001-09-15
    body: "DCOP anyone? With the proper DCOP calls built into koffice applications, couldn't you write simple scripts anyways? I.e:\n\n#/bin/sh\ndcop kspread opendoc $1\ndcop kspread setCurrentCell ($2, $3)\ndcop kspread writeCell ($4)\ndcop kspread closedoc $1\n\nI'm not at all familair with the syntax of this sort of thing, but how feasible is it to use shell-scripts/dcop? Also, of course, you could do the same thing in any language with kde bindings, or even any language with shell-out capabilites (which is pretty much all of them)"
    author: "Carbon"
  - subject: "Re: VBA - just say no! (why not?)"
    date: 2001-09-15
    body: "I have heard something called KOScript mentioned before, sounds like it would fit the bill.  Maybe a KOffice developer could clue us in on this?  What is KOScript and how can you use it?"
    author: "not me"
  - subject: "Re: VBA - just say no! (why not?)"
    date: 2001-09-16
    body: "why not use python?"
    author: "Rick Kreikebaum"
  - subject: "Re: VBA - just say no! (why not?)"
    date: 2001-09-17
    body: "why not Perl?     :-)"
    author: "milan svoboda"
  - subject: "Re: VBA - just say no! (why not?)"
    date: 2001-09-17
    body: "Well... either you forgot we're talking about getting secretaries to use it, or you have a sadistic twist. Why not assembly, while we're at it? :-)"
    author: "Jerome Loisel"
  - subject: "Re: VBA - just say no! (why not?)"
    date: 2001-09-16
    body: "Two problems with what you are replying there.\n\n1st - It isn't secretaries that write VBScipt, it is useless idiots that can't program, but feel forever indebted to Microsoft for their break into IT/IS. They are men (where most secretaries are female, and actually work hard for a living) who wear corduroy trousers and call themselves 'IT Technicians'. They will code everything, forever, to only run on Microsoft products, and think that everyone who doesn't think that Bill Gates is the pinacle of human evolution should FOAD if they don't also write code exclusively for MS products. These people should have their comfort blanket taken away and their belief in BillG should be shattered, to destroy the economic advantages they have, and hence cancel any temporary and accidental advantages they may have had in the breeding game that humans play!!! I am talking about that large group whose only lucky break that allowed them to get on the IT wage escalator and lift themselves out of obscurity, was that their boss asked them to learn VBA. They didn't bother to learn the principles of useful programming languages like C, C++, Pascal, Modula2/3/Object Pascal, whatever, they just bought \"The complete moron's 24 hour guide to learning VBA and then harping on about it forever\".\n\n2nd - If there has to be a scripting language incorporated into KOffice, please let it be Python, which without undue antagonism, requires at least a tiny bit of good programming practise to be understood and respected in order to get a program to run.\n\nBest regards,\nMark.\n\nPS I thought for a while that DCOP was a scripting language for KOffice, but after a long search for documentation on it, I have given up, concluding that the only docs on it are intended for developers who might choose to develop for DCOP interfaces, rather than anyone who might find the interface useful as a development platform. Would anyone care to highlight some good resouces for people who would like to use DCOP interfaces rather than those who (pointlessly) create them? Very much looking forward to any responses."
    author: "Mark Roberts"
  - subject: "Re: VBA - just say no! (why not?)"
    date: 2001-09-20
    body: "You already have, what you your are asking for :)\nThere are Python bindings for DCOP in kdebindings module IIRC, so you an use Python to script any KDE app with a DCOP interface, especially apps with such complete interfaces as KOffice apps.\n\nDavid Faure wrote a reply about this to an email on koffice mailing list.\nSubject is Automation of Koffice.\nCheck the archives to find out more.\n\nCheers,\nKevin"
    author: "Kevin Krammer"
  - subject: "Re: VBA - just say no! (why not?)"
    date: 2001-09-20
    body: "You already have, what you your are asking for :)\nThere are Python bindings for DCOP in kdebindings module IIRC, so you an use Python to script any KDE app with a DCOP interface, especially apps with such complete interfaces as KOffice apps.\n\nDavid Faure wrote a reply about this to an email on koffice mailing list.\nSubject is Automation of Koffice.\nCheck the archives to find out more.\n\nCheers,\nKevin"
    author: "Kevin Krammer"
  - subject: "Re: VBA - just say no! (why not?)"
    date: 2001-09-20
    body: "You already have, what you your are asking for :)\nThere are Python bindings for DCOP in kdebindings module IIRC, so you an use Python to script any KDE app with a DCOP interface, especially apps with such complete interfaces as KOffice apps.\n\nDavid Faure wrote a reply about this to an email on koffice mailing list.\nSubject is Automation of Koffice.\nCheck the archives to find out more.\n\nCheers,\nKevin"
    author: "Kevin Krammer"
  - subject: "Re: VBA - just say no!"
    date: 2001-09-15
    body: "As someone who spends his day writing VBA in an all-Microsoft shop, I have to agree.  VBA is a punishment after a fun weekend of Perl or Python.  Seriously, the LANGUAGE is broken, not just the capabilities.  Please KDE-team, don't even try to emulate this crap.  Better functionality can be gained from any of the open-source scripting languages out there, and you can even (gasp!) code it with built-in security.  But you already know that.  Keep up the good work."
    author: "Anonymous Poster"
  - subject: "Re: VBA - just say no!"
    date: 2001-09-15
    body: "I second that. I also have to struggle with VBA all the time at work and it just SUCKS.\nA few times a day I shake my head about language inconsistencys and similar.\nVBA is not at all an easy to handle language like (as already mentioned) Perl or Python.\n\nJust an example... I wondered a few hours, why \nmysub( \"arg1\", arg2\" )\ndidn't work. It told me crap like \"2 arguments expected\" or something like that.\nLater I found out, that I am not allowed to use brackets when it's a procedure (sub), but I HAVE to use brackets if it's a function. Yeah great...\nAnd they tried to tell me, that would make sense."
    author: "Spark"
  - subject: "Re: VBA - just say no!"
    date: 2001-09-15
    body: "> VBA is a punishment after a fun weekend of Perl or Python.\n> Seriously, the LANGUAGE is broken, not just the capabilities.\n> Please KDE-team, don't even try to emulate this crap.\n\nThe problem is a bit more complicated, of course.  Large corporations -- the ones most likely to take the initiative of moving to KOffice because the cost savings are so substantial -- likely have a *lot* of documents they regularly use which make use of the VBA technology.  Whether this is good or bad is not really the issue, the issue is that their migration costs become quite large if they have to manually rewrite these scripts and retrain their users how to use them.\n\nThis of course does not mean that KOffice needs to provide VBA.  What it does mean, however, is that the import filters need to be capable of converting VBA scripts to whatever language (Python/Perl/?) KOffice uses for its scripting language.  With that goal accomplished, desktop users in corporations and at home can open existing documents and work with them \"out of the box\"."
    author: "Dre"
  - subject: "Re: VBA - just say no!"
    date: 2001-09-17
    body: "Maybe theres middle ground... What if the import filter\ntranslates the VBA script to python?"
    author: "Shai"
  - subject: "Re: VBA - just say no!"
    date: 2001-09-17
    body: "Who said we would ?"
    author: "David Faure"
  - subject: "Re: VBA - just say no!"
    date: 2001-09-20
    body: "I was just referring to the bit of the article that describes KOffice's lack of VBA as a bad thing. Personally I think it is great, considering what an awful language BASIC is (some say learning BASIC first damages your ability to learn decent languages later). I recently learned Python for some maintenance I had to do to some Python CGIs. I like the language a lot, so I am going to read up on the Python bindings to KOffice someone mentioned in this thread."
    author: "Mark Roberts"
  - subject: "KLyX: Lost opportunity?"
    date: 2001-09-15
    body: "I don't understand why the KLyX project has been dropped. This was supposed to become a KOffice component at one time. As M. Ettrich once pointed out, the overemphasis on politically correct \"GUI-independence\" has caused the original LyX project to languish. I really wish KLyX would be started up again. It would  complement KWord. And if the unicode extension of TeX/LaTeX (Omega/Lambda) were supported (since Qt3 has better unicode support), KDE could have a killer typesetting application with no competitive product in the traditional office productivity lineup. Imagine typesetting different languages with different direction rules in a single paragraph or sentence. With correct hyphenation, etc! And without becoming a TeXpert. This is an opprtunity to take the lead (in the true M. Ettrich tradition) and not merely imitate M$ or Adobe.\n\nWADR to the LyX developers, forget political correctness. Port LyX to Qt3 and let's move forward!\n\nI know this is slightly off-topic and I personally think it deserves it's own article. I hope that one of the developers can take a few minutes to talk about it. As a scholar (not a programmer) I would be willing to work with a KDE developer to write a university grant proposal to get at least some financial support for such a program. And if Shawn is interested, I'll buy a copy and be it's greatest advertiser!\n\nMore on this later.\nSamawi"
    author: "Samawi"
  - subject: "oops"
    date: 2001-09-15
    body: "didn't finish filling the email address form..."
    author: "Samawi"
  - subject: "Re: oops"
    date: 2001-09-15
    body: "Yeah, that's what I was thinking while looking at LyX. Even though I know zip about TeX, I was able to make fairly detailed documents in it, and was thinking : \"Wow, make this a kpart, and also allow kparts to be embedded into it (perhaps with inserted bitmaps or something like that) and this would be the ultimate office app\"\n\n(btw, replying to this and not above post so that you get email notificaton for responses, in case you wanted that)"
    author: "Carbon"
  - subject: "Re: KLyX: Lost opportunity?"
    date: 2001-09-15
    body: "[slightly OT]\nI agree almost completely with you. I still use KLyX for *all* my schoolwork, and I have to say it prints better documents than \"equivalent\" programs like MS Word and WordPerfect. I don't think, however, that KLyX should be restarted. It does some things very good, but so does the original LyX. And that one is still being developed. Let's not get into a UI-war where everyone writes his own copies of a program, just doing the same work over and over. One could probably add some things as an extension to LyX, but that's entirely different from just implementing the same over and over. What I would like to see (and if possible help, but time is very short these days): Lyx, just like it is now and activily being developed. Then there could be some people writing patches for instance to turn it into a KPart, building further on the LyX-base. That would give a cool application to all Linux-users. Maybe at last the whole codebase could be turned in some sort of backend, with some people writing frontends for it, KDE-based or not.\n[/slightly OT]"
    author: "Jonathan Brugge"
  - subject: "Re: KLyX: Lost opportunity?"
    date: 2001-09-15
    body: "The upcoming Lyx 1.2 release will be compilable with a QT2 frontend.\nHave a look at the lyx-devel mailing list (www.lyx.org) and you'll see that this release is getting very close.\nYes, it's a shame that it's taken so long, due to the GUI-independence thing. Since QT is 'free' for non-commercial use on win32 and linux now, I think that it probably would have been better to go for an all-qt rewrite originally, with the benefit of hindsight, of course. \n\nI recommend anyone who uses KLyx to download the CVS Lyx sources and try out the QT2 version - help the developers iron out the remaining bugs.\n\nI assume they will port it to QT3 pretty soon, so it should fit nicely into the KDE3 architecture, with plugable styles etc."
    author: "J"
  - subject: "Re: KLyX: Lost opportunity?"
    date: 2001-09-16
    body: "This would be cool. Every important document I ever made in Linux was using Lyx (many homeworks, my thesis, ...). I don't think I should even consider writing huge, important papers in KWord. I allways loved Lyx and KLyx and it would be great having it again in KDE3!"
    author: "Tom Pycke"
  - subject: "Re: KLyX: Lost opportunity?"
    date: 2001-09-18
    body: "I just wonder, having LyX working with Qt, then (hopefully) with KPart, do we still need KWord? I think decision for issue like this should be done ASAP."
    author: "reza"
  - subject: "Re: KLyX: Lost opportunity?"
    date: 2001-09-16
    body: "I agree with you. When Lyx GUI-independent finally is released, I really hope that the KDE team will integrate and make Lyx-KDE an important part of KOffice. LaTeX simply rocks when it comes to writing scientific/technical documents!"
    author: "Johan"
  - subject: "Re: KLyX: Lost opportunity?"
    date: 2001-09-20
    body: "I couldnt agree more. KLyX absolutely kicks ass... I dunno what this gui-independence thing is all about but I for one would love to see a KDE (not just bare Qt, I'm talking KPart/KIO/KOtherReallyCoolStuff enabled) based KLyX again. I for one can't imagine what on earth anyone would need KWord for (KParts not playing nice with TeX?)\n\n...regardless, PLEASE take the project up again, whoever maintains it!"
    author: "Somebody"
  - subject: "Re: KLyX: Lost opportunity?"
    date: 2002-12-14
    body: "True, so true. PLEEEASE integrate LyX into KDE and add a Reference Manager such as Endnote that can directly access library catalogs over the web, e. g. Library of Congress."
    author: "probono"
  - subject: "Re: KLyX: Lost opportunity?"
    date: 2002-12-17
    body: "Found this article while searching for any more info on getting the qt\nfrontend to compile on FreeBSD... i found some good tips (URL below),\nbut it still won't compile for me. I too, would *love* to see LyX slurped\ninto the KDE... LyX is just so excellent at what it does (even if a little\nconstraining in some areas -- without resorting to raw LaTeX).\n\nRight now i'd just like to get the thing to compile with qt3, though.\n\nhttp://www.mail-archive.com/lyx-devel@lists.lyx.org/msg47212.html"
    author: "Ragica"
  - subject: "Well #1 and #2 are useless features anyway."
    date: 2001-09-15
    body: "Import and export of documents in MS formats is a wast of time. Luckily the designers of HTML and HTTP weren't constrained by such silly requirements or the web would not exist. Although this is what many Intranets now do. They simply require the use of IE and Word and all MS equipment. IE embeds a word doc viewer. Users don't even know they're viewing a \"Word Document\" when they browser the company \"knowledge base\".\n\nGroups like OpenOffice, AbiWord, Applix, Koffice, Gobe, and the Mozilla group should get together and establish a workable XML based open standard. Maybe if papa Microsoft let's them Corel(WordPerfect) might show up. Don't even invites MS - if they ask nicely to be allowed to attend then maybe .... The compatabilty with MS is a not an idea that will not get ANYONE to switch over in anycase. People do know this don't they?? Perfecting import filters is not the best route to win new users.\n\nThe focus should be on the new next generation document formats. Some sort of mostly working \"conversion tool\" to get the documents *out* of MSWord format might be useful. But demanding perfect import filters is stupid. We all know that what most of these open and commercial applications need is spell checking, hyphenation, thesaurus tools etc. Hopefully Aspell can solve these problems since it can be used by other apps easily and has dictionaries that are easily extendable and cover many languages. And hopefully more of the commercial software producers will be able to find ways to leaverage and cooperate with open source projects.\n\nIMHO grammar checking is a ridiculous and wasteful feature. The advice given by most grammar checkers is absolute crap and I know no one who seriously uses this  kind of tool. If this is a really needed feature a separate application project should be started (hopefully very internationalized) and designed like Aspell to be easily embeddable in other projects. It would hopefully focus on Thesaurus and Synonym features ... with only minimal grammar checking. It would be easiest if this app had a good stable XML format to work with and if professional linguists were on board for the effort.\n\nThe key is to leaverage the superior format so that the above projects can set the STANDARD and not waste time and resources\n\n* All of these applications (most of which run on Windows too) could read documents produced by any of the others (some such as Abiword couldn't easily rely on embedding a component to read an embedded spread sheet and would have to fall back to something else)\n\n* Any of these applications would produce slick XML docs readable by Mozilla browsers\n\n* Any of these applications would be able to save seamlessly to Zope or Apache web stores.\n\n* The XML standard could be easily extended so that video and sound appear.\n\n* A directory based approach to \"compound documents\" (a la NeXT's excellent LaTeX editing enviroment) should be developped this would instantly place all free tools ahead of MS-Office.\n\n* Becasue of the format the documents could be indexed and searchable with XML aware DB applications in a trivial way.\n\nDon't waste any more that n the minimum of time trying to be compatible with Microsoft.\n\nIn kindergarten I notice that usually if the big kid takes his ball home and won't play with the others the others keep playing. Eventually the big kid comes back - chastened and regretful and asks to play with the group."
    author: "Rosie Palm"
  - subject: "Re: Well #1 and #2 are useless features anyway."
    date: 2001-09-15
    body: "Absolutely agree with you.\nI can't stand this \"Import/Export\" issue anymore. Something has to happen.\nAnd I do not care the slightest bit, if Microsoft plays with the group.\nIf they ask to be outsiders, they SHOULD be outsiders.\nYou are very right, the kids shouldn't run after Microsoft and ask for it's ball."
    author: "Spark"
  - subject: "Re: Well #1 and #2 are useless features anyway."
    date: 2001-09-15
    body: ":: Groups like OpenOffice, AbiWord, Applix, Koffice, Gobe, and the Mozilla group should get together and establish a workable XML based open standard. Maybe if papa Microsoft let's them Corel(WordPerfect) might show up. Don't even invites MS - if they ask nicely to be allowed to attend then maybe .... \n\n    Yeah!  And you know what would be cooler?  Is if we all got CAKE!!!\n\n    Seriously.  This is a great idea in an ideal world, but it's not gonna happen.  At the very very least because there is too much content wrapped up in Word documents at the moment.  There is a great inertia, and if you can't bring your training *or* your content over, switching to a different office suite has no incentive.  As it is, the training has to be redone constantly (new versions, new employees), so retraining to KOffice is not a tremendously big deal.  After all, KOffice, WordPerfect, MS Word, Abiword - to someone who wants to write pretty text, they are all pretty much the same.  Drop down of fonts, and four buttons, one with a bold \"B\", an italic \"I\", an underlined \"U\", and one with a picture of a printer on it.  It's amazing how many people don't know how to cut and paste, drag and drop, use undo, or anything like that.\n\n    But they use \"That file on K:/Betty/\" to file for a client who wants to do foo.  I've been in an office where they used chinese documents, and nobody spoke chinese - they just printed out the one that corresponded, without knowing what it was, because \"that's what you do\".\n\n    It's much easier for single users and extremely large corporations (who already deal with structured document rules, both technical and businesswise).  But the vast majority of users are in large to small companies (go figure), and those people need to have a good Word import ability.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Well #1 and #2 are useless features anyway."
    date: 2001-09-15
    body: ":: Groups like OpenOffice, AbiWord, Applix, Koffice, Gobe, and the Mozilla group should get together and establish a workable XML based open standard. Maybe if papa Microsoft let's them Corel(WordPerfect) might show up. Don't even invites MS - if they ask nicely to be allowed to attend then maybe .... \n\n    Yeah!  And you know what would be cooler?  Is if we all got CAKE!!!\n\n    Seriously.  This is a great idea in an ideal world, but it's not gonna happen.  At the very very least because there is too much content wrapped up in Word documents at the moment.  There is a great inertia, and if you can't bring your training *or* your content over, switching to a different office suite has no incentive.  As it is, the training has to be redone constantly (new versions, new employees), so retraining to KOffice is not a tremendously big deal.  After all, KOffice, WordPerfect, MS Word, Abiword - to someone who wants to write pretty text, they are all pretty much the same.  Drop down of fonts, and four buttons, one with a bold \"B\", an italic \"I\", an underlined \"U\", and one with a picture of a printer on it.  It's amazing how many people don't know how to cut and paste, drag and drop, use undo, or anything like that.\n\n    But they use \"That file on K:/Betty/\" to file for a client who wants to do foo.  I've been in an office where they used chinese documents, and nobody spoke chinese - they just printed out the one that corresponded, without knowing what it was, because \"that's what you do\".\n\n    It's much easier for single users and extremely large corporations (who already deal with structured document rules, both technical and businesswise).  But the vast majority of users are in large to small companies (go figure), and those people need to have a good Word import ability.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Well #1 and #2 are useless features anyway."
    date: 2001-09-17
    body: "\"Yeah! And you know what would be cooler? Is if we all got CAKE!!!\"\n\nI'll see what I can do. (wink wink)"
    author: "Jerome Loisel"
  - subject: "Re: Well #1 and #2 are useless features anyway."
    date: 2001-09-15
    body: "I second this. We don't need MS Office crap. Those companies who need it are very much welcome to implement MS Office filters in KOffice. I don't need this shit. Opensource standard is the right way. You are going to waste your energy on MS Office 97, 98, 2000 filters, and then Microsoft is going to change its own standards, and you are going to waste your energy on developing new filters and so on. One example: one of my friends had an Access file created in MS Office 97 (Access 97), and he asked me to change it a little bit (just fonts) and save it for him. I (arggghhh) installed MS Office 2000 Professional, opend his database file, changed it and tried to save it. I got this error: 'This database has been created in older format, you can view it but not save changes'!!!!! What to say?!\nAnd another thing: I doubt that machines (hereby also computers) will ever be able to do grammar checking. Fortunatelly there are no rules for human language\n(human language is poetry, slang, philosophy, emotions - not commands).\n\nP.S. We need more Rosies :)"
    author: "antialias"
  - subject: "Re: Well #1 and #2 are useless features anyway."
    date: 2001-09-21
    body: "Grammar checking has nothing to do with style.  Most people confuse the two.  Grammar checking is useful in environments where clean, concise wording is important.  As I have Majored in English, I can verify that the average person is denounces style in writing is actually confused on what they are talking.\n\nThis is because they lack the knowledge and make assumptions.  They didn't pay attention in class when the teacher discussed Passive Voice verse Active voice."
    author: "Jason"
  - subject: "Re: Well #1 and #2 are useless features anyway."
    date: 2001-09-21
    body: "And there I go and make a mistake...\n\"average person is denounces style\" haha"
    author: "Jason"
  - subject: "Re: Well #1 and #2 are useless features anyway."
    date: 2001-09-16
    body: "developers, are you listening?"
    author: "Rick Kreikebaum"
  - subject: "MS Office filters"
    date: 2001-09-15
    body: "These filters are just as necessary as filters for other Office packages.\nHow are you else able to run Linux on the desktop in an MS Office environment? \nBesides: there are a lot of people still in the dark who prescribe that documents are delivered in MS Office format. \n\nCompatibility is essential, because when compatibility is reached there is no need anymore for a lot of companies to hold on to closed source Office programs. And licence costs drop dramatically. When looking at the numbers it is worldwide ca $ 500 million on Office licences if not more. \nWhat a lot of companies and/or countries could do with (a part of) $500 million... Schools, hospitals, infrastructure (roads, railways and telecom), less layoffs etc., etc. So it's even good for the economy.\n\nTherefore: make KOffice compatible with the rest of the world (yes, even MS, but watch out for beartraps).\n\njust my 2c.\n\nErick"
    author: "Erick Staal"
  - subject: "Re: MS Office filters"
    date: 2001-09-15
    body: "There is already a combined effort to develop MS im- and \nexport filters.  For more info please read the underneath \nmentioned messages:\n\n\nhttp://lists.kde.org/?l=koffice&m=99929179819041&w=2\n> http://cvs.sourceforge.net/cgi-bin/viewcvs.cgi/wvware/wv2/\n>\n> These filters are intended to be shared by KOffice and AbiWord (at least),\n> and will have import and export capability. Write to me privately, and we\n> can figure out a good place for you to start!\n\nhttp://lists.kde.org/?l=koffice&m=100046338303283&w=2\n> > A single filter source-pool for all opensource projects would be\n> > wonderful to achieve.\n> >\n> > I honestly believe filter quality/completeness is one of the largest\n> > barriers to acceptance of koffice/open office/gnumeric into the larger\n> > business eviornment.\n>\n> FWIW, such efforts are already underway for the editors over at\n> wvware.sf.net, and I would would certainly encourage the other apps to do\n> the same. Note that is it essential that we all use a generic framework, as\n> well as individual filters in order to support embedding.\n>\n> The work on libwv2 is intended to be supplemented by libwmf2 for image\n> handling."
    author: "Richard Bos"
  - subject: "Re: MS Office filters"
    date: 2001-09-18
    body: "Perhaps we should invite Microsoft to add KOffice import filters to MSOffice and distribute them as an update to their current office packages.  It would be good publicity for them, showing the world that they can exist in a spirit of co-operation with others, and of course good for us KOffice users. OK I know that this seem unlikely, but it just seems like a good time to drop anti-microsoft destructive rhetoric, define ourselves by what we like rather than what we hate and invite co-operation on that basis."
    author: "nimbus"
  - subject: "Open File Formats...Agreed!"
    date: 2001-09-15
    body: "I totally agree we need open file formats, I have been saying\nthis for a while now...you can read my post about it on slashdot: \nhttp://slashdot.org/comments.plsid=21178&cid=2243938\n\nthe OpenOffice project already has a start on such a thing\nhere: \nhttp://xml.openoffice.org/\n\nI think maybe we should start an Open File Foundation (OFF)???\nand try to get as many different office suites involved like\nKOffice, OpenOffice, Abiword, Gobe, Hancom, etc..."
    author: "Brad C."
  - subject: "Import filters (MS-Word)"
    date: 2001-09-16
    body: "It appears to me that what is needed is a way to import a document created in another word processor and preserve all of the formating information.\n\nTo me, what would really be useful would be an import filter for: PDF and EPS files (perhaps one that could import PS files would also be useful to the extent that this is possible).\n\nSince, for reasons I do not understand, people continue to distribut documents in the native format of a word processor (something that I never do), it would also be helpful if there were an application the worked with GhostScript and WV that could convert a MS-Word document to either EPS or PDF."
    author: "James Richard Tyrer"
  - subject: "Re: Import filters (MS-Word)"
    date: 2001-09-16
    body: "Well, the problem with that is, PDF loses a bit of magic when you convert files to that, both from MS-Office (no more OLE) and from KOffice (no more kparts). PDF and EPS are really only good if you want to preserve an exact printout, which is often the case, but for when you want the reciever to be able to edit the file easily...  \n\nWhat's even worse is, MS standards for PostScripts (and PDF, which is somewhat similar, iirc) differ from everybody else's. Last I heard, some implementations manually set a coordinate for each characters, rather then for groups of text, making parsing and editing a big fat pain."
    author: "Carbon"
  - subject: "Re: Import filters (MS-Word)"
    date: 2001-09-17
    body: "What I am (or was) refering to was sending someone a document, not having several different people work on it in the office.  Do you really want to send someone a document in a wordprocessors native file format?  I realize that MS seems to encourage this behavior, but is it really a good idea?\n\nAnd how is OLE going to work with a system that uses real object/component software?  Remember, OLE is not component software, it is just a simulation.\n\n>>> That is another issue with Koffice.  Why, if this is really going to be component software, do the different applications have different file formats, or do they? -- they have different file extensions!\n\nApparently from what you say, M$ has E3'd PS and PDF as well.  This clearly presents a problem.\n\nI still think that Koffice should encourage what I feel is the correct action, senting a PDF of the document.  So, it should have an import filter for PDF, and to me, this is more important than a M$-Word filter.\n\nWhen ever someone provides me with a (M$) DOC file, I advise them that a PDF would be better.  Quite often, I find out that they are one of those Windows Addicts that don't even understand what a file format is.\n\nIn any case, this feature would be handy so you could load a PDF document into Kword so that you can mark it up."
    author: "James Richard Tyrer"
  - subject: "Re: Import filters (MS-Word)"
    date: 2003-12-15
    body: "PDF seems to me the same bad like DOC.\nMaybe it's just me but I have to wait _very_ long when I need to load pdf viewer, in Linux and in Windows too.\n\nI believe that _if_ you really need to send some document to someone, you should use HTML filter...\n\nI am sorry but I don't feel comfortable with PDF and I never will.."
    author: "Tomas Dean"
  - subject: "Re: Import filters (MS-Word)"
    date: 2005-10-12
    body: "James Richard Tyrer wrote:\nIt appears to me that what is needed is a way to import a document created in another word processor and preserve all of the formating information.\n\nTo me, what would really be useful would be an import filter for: PDF and EPS files (perhaps one that could import PS files would also be useful to the extent that this is possible).\n\n ----\n\nPDF, EPS, and PS would deffinately NOT be a file type to do that.  They can provide a picture of the document (fine for distributing it for viewing only) but are not intended to reconstruct the document in another word processor.  You can make a file that prints pretty close to the origonal from them, but they cannot have named styles and other things that word processors know about the document while you are working on it.\n\nUnfortunately, there is no fully generic word processor transfer file type.  To create one would require a change anytime any word processor added any significant feature.  \nWord's DOC format is commonly used for transfer, because it is the most common format.  \n\n"
    author: "Bill "
  - subject: "Yes, VBA is the worst problem..."
    date: 2001-09-16
    body: ">(such as the ability to import VBA (Visual Basic for Applications) scripts).\nYes, VBA is very used (also with ODBC to work with data bases, and not only\nin Excel, also in Word) , and the KDE Office does not seem (?) to find\nit is important...\n\nI think that the first goal is to allow an automatic import of the VBA scripts.\nThe choice of the language is secondery. I don't think that the basic language\nis to be rejected by principle, it is a language among others languages (and a\nbasic language may be better than VBA)\n\nIf an import in Python is possible, OK, use Python. If it is impossible, please\nwork with the KBasic team...\nI hope that a policy will be defined and shown in the KOffice site.  More\ngenerally, I wish that improvement priorities would be shown on the KOffice\nsite...\n\nA choice may be, of course, that a VBA scripts import will not exist, excepted  the arrival of some strong developpers..."
    author: "Alain"
  - subject: "v1.1 .vs. v10"
    date: 2001-09-17
    body: "Lets not forget that this review is comparing a v1.1 product against a v10 product.  Considering the difference in development time/effort, KOffice is a stunning success so far.  \n\nI think it's encouraging that people are able to post 'reviews' like this. Even if most of it is focusing on what's missing, it does mean that here is a product people are capable of actually using.\n\nWell done KOffice team!\n\nMacka"
    author: "Macka"
  - subject: "Re: v1.1 .vs. v10"
    date: 2001-09-17
    body: "I completely agree with you, but please remember that in the business world they don't care about version number and how far a product it has come in such a short time. \n\nWhen you buy something, you want to buy the best now! KOffice has to be the best solution now, or it will not be taken seriously. KOffice just needs more time and it will mature."
    author: "kde-user"
  - subject: "Re: v1.1 .vs. v10"
    date: 2001-09-17
    body: "I think you put it very well. KOffice has turned out extremely well, especially since that dreadful 1.0 turkey.\n\nAnother point, though: it's interesting that people compare KOffice with MS Offfice. I think it's unavoidable, considering the name, but it's not really fair at all. Compare it with Works instead, and suddenly you get a lot of power for your money ;-)\n\nAs it is, I use KWord on a daily basis, and I would recommend it for ayone with basic writing and DTP needs. For my more complex needs, I still resort to StarOffice.\nKSpread also works very well, but here, I've had some rather serious importing concerning Excel. Partly, I think it's due to Excel allowing almost anything, even when it shouldn't be possible :-)\nKPresenter is OK for most needs, but I have problems with some of my PPT presentations.\n\nOn the whole, I'd say that KOffice is a  <b>fantastic</b> bargain for all home users, schools and other places where money matters, and you don't wan't to go through the work to install Star Office. \n(Especially when it comes to schools, I would almost call it a crime to use public money to teach innocent children how to push all the right Microsoft buttons :-) )"
    author: "G\u00f6ran Jartin"
---
<A HREF="http://www.zdnet.com/">ZDNet</A> has published a <A HREF="http://www.zdnet.com/products/stories/reviews/0,4161,2812025,00.html">review</A> of <A HREF="http://www.koffice.org/">KOffice</A><SUP><SMALL>TM</SMALL></SUP>.
Titled <EM>"KOffice falls short of Microsoft Office standard:  Updated Linux
alternative to Office not ready for corporate big leagues"</EM>, the lengthy
review takes a close and, perhaps surprisingly, fair look at the strengths
and weaknesses of KOffice.  While generally lauding KOffice, its design,
stablity and capabilities, the review identifies four factors which, in
the reviewers' opinion, makes KOffice currently unsuitable for corporate
use:  (1) shortcomings of the MS Office<SUP>&reg;</SUP> import filters; (2) lack
of MS Office export filters; (3) the lack of some KWord features, such as
a thesaurus, automatic spell-checking and (uggghh) grammar checking;
and (4) some missing features in KSpread, some easy to fix (such as
case sensitivity) and some not so easy to fix (such as the ability to
import VBA (Visual Basic for Applications) scripts).  All in all,
surprisingly similar to <A HREF="http://dot.kde.org/999051134/">my own
review</A>.
<!--break-->
