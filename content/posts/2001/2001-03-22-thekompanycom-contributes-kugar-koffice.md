---
title: "theKompany.com Contributes Kugar to KOffice"
date:    2001-03-22
authors:
  - "Dre"
slug:    thekompanycom-contributes-kugar-koffice
comments:
  - subject: "Re: theKompany.com Contributes Kugar to KOffice"
    date: 2001-03-22
    body: "Once more - thanks to theKompany for another great applications to the KDE desktop.\n\nI think it is amazing how much you guys gets done and it is very cool that you take the time to answer questions that pop up both here and elsewhere!\n\nKeep up the good work...\n\n/Joergen"
    author: "Joergen Ramskov"
  - subject: "Re: theKompany.com Contributes Kugar to KOffice"
    date: 2001-03-22
    body: "very cool"
    author: "Mark Hillary"
  - subject: "Re: theKompany.com Contributes Kugar to KOffice"
    date: 2001-03-22
    body: "Will or does Kugar support database connectivity, so that you can create reports based on the results of SQL queries, ala Crystal Reports?\n\nAnother thing that would be nice (if it doesn't currently exist) is if you could export the reports to different formats, using the filters available from KOffice itself. So you would be able to export the reports to Word format for the Linux impaired ;-)\n\nThanks for donating the project Shawn, you rock!\n\n-Steven"
    author: "Steven"
  - subject: "Re: theKompany.com Contributes Kugar to KOffice"
    date: 2001-03-22
    body: "We actually did some proof of concept work with KDE-DB and Kugar where you could embed the query inside the report and then when the report was selected as a file with Konqueror it would execute and display in real time (as long as it didn't take to long to execute).  There are a number of different and interesting things you can do with this idea, and with your idea of making use of the Filters.\n\nHopefully with Kugar as part of KOffice now, more people will be interested in donating time to create some of these neat ideas."
    author: "Shawn Gordon"
  - subject: "ReKall"
    date: 2001-03-22
    body: "...still waiting for it! :-)\n\nKudos guys!"
    author: "t0m_dR"
  - subject: "Re: ReKall"
    date: 2001-03-22
    body: "As I've been saying, the first beta will be the end of March, and we still appear on track for it :)."
    author: "Shawn Gordon"
  - subject: "Re: ReKall"
    date: 2001-03-23
    body: "What's ReKall ?"
    author: "Macka"
  - subject: "Re: ReKall"
    date: 2001-03-23
    body: "ReKall is an MS Access style application for KDE and KOffice.  The first release will be useful, but a bit primative compared to what it will be by summer."
    author: "Shawn Gordon"
  - subject: "Re: ReKall"
    date: 2001-04-04
    body: "OK end of march passed!\n\n:-)\n\nJust KIDDING!\nplease take your time to make the product STABLE and Rich and don't care so much about the time. You have already contributed SO much, in such a short time frame!"
    author: "TechiOS"
  - subject: "Re: theKompany.com Contributes Kugar to KOffice"
    date: 2001-03-22
    body: "A little question, not related to Kugar:\n\nHow is progress on Aethera?\n\nAFAIK there has only been one public beta release - according to your roadmap, there should have been a release in february and in marts?\n\nA quick status would be nice (or an updated website ;)), if possible..."
    author: "Joergen Ramskov"
  - subject: "Re: theKompany.com Contributes Kugar to KOffice"
    date: 2001-03-22
    body: "Well, the sad truth is that we lost one of the Aethera programmers, and another one had to go to the hospital with health problems for a bit, and I had pulled other people to get Kapital, KDE Studio Gold and BlackAdder betas out.\nThe good news is that the next beta is done and we are just getting the packaging together right now.  This release is mostly bug fixes, but also the addition of IMAP support.  From now on we will be putting out releases much more often, probably every few weeks.\n\nYou can't download Aethera right now, so please don't send me an email telling me it doesn't work, we will send an announcement as soon as it's ready.  You can sign up for announcements from us if you want at http://www.thekompany.com/press_media/mailinglists/"
    author: "Shawn Gordon"
  - subject: "Re: theKompany.com Contributes Kugar to KOffice"
    date: 2001-03-22
    body: "I should have also mentioned that the best news was our programmer with the health problems is fine now, and we are thrilled to have him back."
    author: "Shawn Gordon"
  - subject: "Re: theKompany.com Contributes Kugar to KOffice"
    date: 2001-03-22
    body: "Good to hear - it's sad to loose good co-workers.\n\nGood luck with Aethera and keep up the good work!"
    author: "Joergen Ramskov"
  - subject: "Re: theKompany.com Contributes Kugar to KOffice"
    date: 2001-03-22
    body: "In general, I find theKompany rocks!!\n\nI use their KdeStudio product, and I find it slick.\nI demoed their Kapital product, and am considering  buying it.\nAnd the KwiX window decoration is simple BUT cool.\n\nRock on."
    author: "Ernie"
  - subject: "Re: theKompany.com Contributes Kugar to KOffice"
    date: 2001-03-26
    body: "> And the KwiX window decoration is simple BUT cool.\n\n    Hunh?  What's that?  I did a search on theKompany.com (which was down) and on Google.  I haven't heard of it before.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: theKompany.com Contributes Kugar to KOffice"
    date: 2001-03-26
    body: "...and to answer my own question, a search on apps.kde.com found the following:\n\nhttp://apps.kde.com/na/2/info/id/1046\n\n\"Catalin Climov  <xxl at thekompany.com>\nKwiX is a set of window decorations.\"\n\nNifty.  I'm going to try them right now... I've stuck with the default theme (with ModSystem) because it's so clean and simple... this looks nice.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: theKompany.com Contributes Kugar to KOffice"
    date: 2001-03-23
    body: "Neil Hodgson, the original author of Scintilla recently let me know that you were planning to release the Qt port of Scintilla/SciTE as opensource. When is that likely to happen? I would rather spend time incorporating the Scintilla widget into an MDI editor rather than porting it. I haven't seen BlackAdder yet, but I am curious.\n\nRavi"
    author: "Ravi"
  - subject: "Re: theKompany.com Contributes Kugar to KOffice"
    date: 2001-03-23
    body: "We actually have ports of Scintilla to Qt, BeOS, AtheOS and QNX that one of our fine programmers (Catalin) did.  Right now Catalin is working with Ralf on the Scintilla team to get it all worked out.  We've got a ton of stuff we are releasing in the next couple of weeks, so it could be a few weeks.  It's just stupid stuff like getting the web pages set up, and the CVS stuff organized, but everyone is busy right now.  Maybe you want to work on a template designer for Kugar while you are waiting :)."
    author: "Shawn Gordon"
---
<A HREF="http://www.thekompany.com/projects/kugar">Kugar</a> is a business-quality report generator and includes a standalone report viewer and a KPart report viewer. Any KDE application which supports KParts (including the browser <A HREF="http://www.konqueror.org/">Konqueror</A>) thus can embed the report viewing functionality.  This is a great addition to the budding <A HREF="http://www.koffice.org/">KOffice suite</A>.  Hats off to <A HREF="http://www.thekompany.com/">theKompany</A> (and <A HREF="http://www.mutinybaysoftware.com/">Mutiney Bay Software</A>, which started the Kugar project) for donating
this great code to the KDE project!  Read more about Kugar below.




<!--break-->
<P>
<A HREF="mailto:shawn@thekompany.com">Shawn Gordon</A>, CEO of theKompany, explains more about Kugar:
</P>
<blockquote>
<P>
Kugar works by merging application generated data with a template to produce the final report. Both the data and the template are specified using XML. This approach means that applications only need worry about generating the data itself. A template can be referenced via a URL which allows businesses to create a centrally managed template library.
</P><P>
Kugar was originally known as <A HREF="http://www.mutinybaysoftware.com/metaphrast.html">Metaphrast</A> from <A HREF="http://www.mutinybaysoftware.com/">Mutiney Bay Software</A>.  We had identified it as a possible reporting tool for <A HREF="http://www.thekompany.com/products/kapital/index.php3">Kapital</a> and Mutiney Bay said we could take over the project.  Now we would like to make the project generally available by contributing the code to <A HREF="http://www.koffice.org/">KOffice</a>, so that future developers can rely on a reporting tool for their applications.
</P>
<P>
Kugar is currently in version 1.0.  All the current functions work, but it could be improved with the addition of a template designer and more charting widgets.
</P>
</blockquote>

