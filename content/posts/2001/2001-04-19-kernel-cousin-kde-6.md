---
title: "Kernel Cousin KDE #6"
date:    2001-04-19
authors:
  - "ranglerud"
slug:    kernel-cousin-kde-6
comments:
  - subject: "Re: Kernel Cousin KDE #6"
    date: 2001-04-19
    body: "Please keep stuff that you have to 'su' to use seperate from user stuff, alot of UNIX users don't have root access."
    author: "ac"
  - subject: "Re: Kernel Cousin KDE #6"
    date: 2001-04-23
    body: "Why is it that when I click on the 'latest edition' link that I get redirected to the site 'www.camvista.com' and get a 401 error?  When put the mouse pointer over the link, I see the URL 'http://kt.zork.net/kde/kde20010413_6.html' but I sure don't get anywhere by clicking on it.\n\nBTW, this happens for Kernel Cousin KDE #5 as well.  I haven't checked previous editions.\n\nThanks for any help you can give me."
    author: "Daniel Bradshaw"
---
The <a href="http://kt.zork.net/kde/kde20010413_6.html">latest edition</a> of the KDE kernel cousin is out, summarising the latest developments and discussions on the KDE mailing lists.
This issue covers among other things the creation of a kcontrol module for the new KDM, Andreas Beckermann taking over as KDE games maintainer, and some tidbits about KOffice development.
<!--break-->
