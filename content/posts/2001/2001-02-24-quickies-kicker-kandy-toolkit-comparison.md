---
title: "Quickies: Kicker Kandy, Toolkit Comparison"
date:    2001-02-24
authors:
  - "numanee"
slug:    quickies-kicker-kandy-toolkit-comparison
comments:
  - subject: "Re: Quickies: Kicker Kandy, Toolkit Comparison"
    date: 2001-02-23
    body: "I don't really understand the joke on Segfault. Are they implicitely saying that the gnomesters are swine?"
    author: "reihal"
  - subject: "Re: Quickies: Kicker Kandy, Toolkit Comparison"
    date: 2001-02-23
    body: "It's just a parody poking fun at the whole situation and not attacking anyone in particular.  I think it's okay, we can laugh now.  The situation has been resolved.\n\n-n."
    author: "n"
  - subject: "Whoaaaaa....."
    date: 2001-02-26
    body: "Finally i got my dot back :)\n2 days without the dot is like 2 days in /g hell brrrr.....  :)"
    author: "me"
  - subject: "Re: Whoaaaaa....."
    date: 2001-02-26
    body: "Agree!  Our network services provider seems to have pulled a fast one on us (and the rest of their clients).  We're working on redundancy solutions for the future although they'll be at least one more <i>scheduled</i> downtime in the next few months.\n<p>\nCheers,<br>\nNavin."
    author: "Navindra Umanee"
  - subject: "Re: Whoaaaaa....."
    date: 2001-02-26
    body: "Surely, the KDE League member companys can do some promo and mirror the KDE sites?"
    author: "reihal"
  - subject: "Re: Quickies: Kicker Kandy, Toolkit Comparison"
    date: 2001-02-26
    body: "I find it ironic that the Ximian folks, who changed their name from Helixcode to Ximian to have a defensable trademark, saw no problems in trampling on the name of KDE, a fellow non-profit opensource project. Would they be okay if we used their names, logos, etc everywhere and confuse people?\n\nSorry to bring up this tired ole shit again but it really chaps my butt."
    author: "ac"
  - subject: "Re: Quickies: Kicker Kandy, Toolkit Comparison"
    date: 2001-03-14
    body: "So what? Ximian is a company.\nIf you really want to compare, compare KDE with Gnome.\nGnome didn't changed it's name.\nGnome is an opensource project."
    author: "Kill the Trolls!!!!!!!"
  - subject: "What's up with KDE 2.1"
    date: 2001-02-26
    body: "Wasn't KDE 2.1 supposed to be released today?, I really expected some flashy announcement since earlier this morning...all I got was a small note from Linux Planet.\n\nThe mirrors weren\u00b4t ready either...\n\nSome last minute delay?"
    author: "Charles"
  - subject: "Re: What's up with KDE 2.1"
    date: 2001-02-26
    body: "Don't know !! the site seems to be dead.......\nBut, on the FTP, all the files has disapeared !\n\n<b>The package (2.1) for debian are ready for download </b>- snif , i have a mandrake :-("
    author: "gravis"
  - subject: "Re: Quickies: Kicker Kandy, Toolkit Comparison"
    date: 2001-02-27
    body: "Just to be fair on the toolkits you should take off more that gtk_ and gdk_, because code looks like this : <BR><BR>\nGdkRectangle rectangle; <BR>\n<B>'gdk_rectangle_get_foo(rectangle);'</B>\n<BR><BR>\nwhile the C++ does : <BR>\nGdk::Rectangle rectangle; <BR>\n<B>'rectangle.get_foo();'</B> <BR>"
    author: "John"
  - subject: "Re: Quickies: Kicker Kandy, Toolkit Comparison"
    date: 2001-02-27
    body: "Just so you know though, KDE appears to pop up Ximian again.  Guess they couldn't leave it alone.  I thought they were gone then I went to google today and typed in kde and up pops the Ximian ad again.  Just when you thought everyone was going to play nice in the sandbox again..."
    author: "Anthony MOulen"
  - subject: "Re: Quickies: Kicker Kandy, Toolkit Comparison"
    date: 2001-02-27
    body: "Just so you know, Ximian isn't behind this one. Details <a href=\"http://www.kdesucks.com/\">here</a>."
    author: "nap"
  - subject: "Re: Quickies: Kicker Kandy, Toolkit Comparison"
    date: 2001-02-28
    body: "Funny.  Google has filtered out my IP so that when I search for \"kde\" the Ximian link does not pop up for me.  When I search from another IP, the Ximian ad pops up.\n\nI guess they thought I accessed it too many times before and helped finish Ximian's money.\n\nWhat do we do about this now?"
    author: "KDE User"
  - subject: "Re: Quickies: Kicker Kandy, Toolkit Comparison"
    date: 2001-02-28
    body: "Do nothing. It makes clear for everybody that I was right in my first post here."
    author: "reihal"
---
<A href="mailto:coyle@ResExcellence.com">Michael Coyle</a> wrote in point us to <a href="http://www.ResExcellence.com/linux_icebox/02-19-01.shtml">a simple set of tile buttons and panel backgrounds</a>  that <a href="http://www.ResExcellence.com/">ResExcellence</a> has made available for Kicker.  These actually look pretty cool.  Also of potential interest are a couple of recent screenshots (<a href="http://www.cs.kuleuven.ac.be/~bartd/KDE/kcd_detail.jpg">1</a>, <a href="http://www.cs.kuleuven.ac.be/~bartd/KDE/kcd_full_kicker.jpg">2</a>) of the <a href="http://apps.kde.com/nfinfo.php?vid=2517">Kcd applet</a>. In developer news, <a href="mailto:pfremy@chez.com">Philippe Fremy</a> wrote in to point us to an interesting <a href="http://klotski.berlios.de/kde/cmp-toolkits.html">little analysis</a> involving GTK+, Qt and PyQt.  This may be of some interest to developers considering porting a GTK+ app to KDE/Qt. Users might want to go straight to the <a href="http://aquila.rezel.enst.fr/philippe/klotski/">Klotski homepage</a>. Finally, in laugh-it's-funny news, <a href="http://www.segfault.org/">Segfault.org</a> reports that the <a href="http://www.segfault.org/story.phtml?mode=2&id=3a899640-04e6f060">Bacon keyword</a> has been googled.  Poor Jono.







<!--break-->
