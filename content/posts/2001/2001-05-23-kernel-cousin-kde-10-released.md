---
title: "Kernel Cousin KDE #10 Released"
date:    2001-05-23
authors:
  - "rmoore"
slug:    kernel-cousin-kde-10-released
comments:
  - subject: "Re: Kernel Cousin KDE #10 Released"
    date: 2001-05-23
    body: "This is off topic but <a href=\n\"http://www.pclinuxonline.com/\">www.pclinuxonline.com</a> Has a poll on which window manager that you use. KDE is currently 72% and gnome 16%. I think that very cool.\n<br><br>\nCraig"
    author: "Craig"
  - subject: "Re: Kernel Cousin KDE #10 Released"
    date: 2001-05-23
    body: "Looking at the news articles posted at that page, I'm not surprised.  Half of them are about KDE and the other half are about Mandrake.  No wonder no GNOME users go there!\n\nI would be interested in a Slashdot poll about this, or a poll from some other linux-oriented but not desktop-biased site.  That would give much more meaningful results."
    author: "not me"
  - subject: "Re: Kernel Cousin KDE #10 Released"
    date: 2001-05-23
    body: "Slash Dot? Neutral? Come on Slash dot is pretty gnome centric. a linux today poll would be better. \n\ncraig"
    author: "Craig"
  - subject: "Re: Kernel Cousin KDE #10 Released"
    date: 2001-05-23
    body: "Could you please explain to me why it is so \"cool\" to see the Linux community divided? The right word here is \"sad\" I am afraid..."
    author: "freedesktop"
  - subject: "Re: Kernel Cousin KDE #10 Released"
    date: 2001-05-23
    body: "In fact, if you look closely, they are quite united...  behind KDE."
    author: "KDE User"
  - subject: "Re: Kernel Cousin KDE #10 Released"
    date: 2001-05-23
    body: "I would believe that if the poll was taken from a generic mix of Linux people. Unfortunately for you, the poll is taken on a KDE coloured site, so _ofcourse_ KDE turns in the lead there.\n\nStill, I am saddened by the fact that there are lots of 'users' out there (erhm, in here) who are chearing whenever X beats Y (fill in Gnome/KDE for X/Y in any order).\n\nUnite, people, unite!"
    author: "freedesktop"
  - subject: "Re: Kernel Cousin KDE #10 Released"
    date: 2001-05-23
    body: "Hey there Marx chill out. Have you ever been a sports fan and rooted for your team? Actuallty the the kde articles are new. I've never seen so many kde articles. Its not a kde site theres just more kde news right now. Drop your unite crap it sounds stupid. \n\nCraig"
    author: "Craig"
  - subject: "Re: Kernel Cousin KDE #10 Released"
    date: 2001-05-23
    body: ">Drop your unite crap it sounds stupid.\n\nYou, on the other hand, sound like a 10-year-old kid. Stop ruining my reading experience with your pathetic off-topic drivel."
    author: "Annoyed reader"
  - subject: "Re: Kernel Cousin KDE #10 Released"
    date: 2001-05-23
    body: "sissy little girly boy."
    author: "Craig"
  - subject: "Re: Kernel Cousin KDE #10 Released"
    date: 2001-05-23
    body: "It seems my guestimate of your age was about right. \nWell, guess I just have to accept this as the inherent problem/law of creating an easy user interface: The better it becomes the more it gets moronic fans. \nKDE would seem to lead Gnome in usability according to that law."
    author: "Annoyed reader"
  - subject: "Re: Kernel Cousin KDE #10 Released"
    date: 2001-05-26
    body: "Well, you have a definite point, Craig is acting rather like he has a bad case of shit-headiness. But he also has a point in that it's difficult to unite. In a way, gnome was sort of kind of a fork of kde, not in a source code way, such as bsd, but in a divided issue (this being qt's licensing). Now, of course, this has been fixed (despite grumbling on the part of rms), but these people have a quite good desktop enviroment and aren't just going to let it die. imho, though, there isn't any serious competition. Sure, we'd both like to see our personal desktops win, and we both complain endlessly (but not neccessarily seriously) about the other desktop on our respective irc channels. However, in an OSS world, there is nothing to be gained by a project dying. Only the really wacked truly feel that the 'opposing' project should die. It's just too bad that our respective toolkits are so different as to make a full merge nigh-impossible."
    author: "Carbon"
  - subject: "Flexibility good, single desktop bad"
    date: 2001-05-23
    body: "IMHO the flexibility to have multiple desktop environments to suit different people's needs is a Good Thing (tm), as is competition between them, it promotes improvement.\n\nWhat is \"cool\" is that KDE _might_ at the moment be winning this little tug-of-war, depending on which stats you believe. As someone has already said, it is akin to supporting a sports team, just a bit of healthy competition."
    author: "Chris Boyle"
  - subject: "Re: Kernel Cousin KDE #10 Released"
    date: 2001-05-25
    body: "This schizm phenomena isn't necessarily an open source \"problem\".   Large companies (I recently worked for Nortel) often have two or more groups duplicating a research effort in full without much intercommunication.  \n\nSplitting the Linux (hopefully someday multiplatform) interface into two \"rival sanctions\" allows for more variety (read robustness) and friendly competition in both products.  Of course the developers feel close to there product and want to see it win, there's no money involved only pride of a job well done!  \n\n\nCelebrate competition! just don't use Microsoft's definition.  I don't think any of us Linux types are going to make incompatible products for more market share anyway...\n\nSit back with an ice cold beer and a footlong and hope the Konqi's pummel those Ximians!"
    author: "Eric Nichlson"
  - subject: "Re: Kernel Cousin KDE #10 Released"
    date: 2001-05-23
    body: "Polls like that proves nothing.\nIf I put a poll on Gnotices then 85% will be using GNOME."
    author: "dc"
  - subject: "Re: Kernel Cousin KDE #10 Released"
    date: 2001-05-23
    body: "ITS NOT A KDE SITE THERES JUST MORE KDE NEWS RIGHT NOW."
    author: "Craig"
  - subject: "Re: Kernel Cousin KDE #10 Released"
    date: 2001-05-23
    body: "> ITS NOT A KDE SITE THERES JUST MORE KDE NEWS RIGHT NOW.\n\nThat's right. But I think we shouldn't overestimate the results of a poll from a KDE centered site. If you took the same poll on a Gnome site, you would have received a completely different result. ;-))"
    author: "Pain-in-the-neck"
  - subject: "Re: Kernel Cousin KDE #10 Released"
    date: 2001-05-24
    body: "Its not a kde site its a mandrake site and it is posted on the gnome site as well. face it kde rocks.\n\nCraig"
    author: "Craig"
  - subject: "Re: Kernel Cousin KDE #10 Released"
    date: 2001-05-23
    body: "You really think that only 85% on Gnotices would vote for Gnome? Well, KDE must be more pupolar than I had thought ;-)"
    author: "jj"
  - subject: "Re: Kernel Cousin KDE #10 Released"
    date: 2001-05-23
    body: "Actually the poll is posted on gnotices ae well its just that kde rocks!!1\n\nCraig"
    author: "Craig"
  - subject: "Re: PCLinuxonline"
    date: 2001-05-23
    body: "7?? wondering is why a \"PC Linux\" site has graphics so obviously drawn from MacOS X. \n\nAnyway, the window manager on my KDE desktop is WindowMaker so that's how I voted. The site's editors should have voted for the \"What is a window manager?\" option."
    author: "Otter"
  - subject: "Re: PCLinuxonline"
    date: 2001-05-23
    body: "You need to check out the new style called aqua on the theme site.\n\nCraig"
    author: "Craig"
  - subject: "Re: PCLinuxonline"
    date: 2001-05-23
    body: "What I'm wondering is why a \"PC Linux\" site has graphics so obviously drawn from MacOS X. \n\nAnyway, the window manager on my KDE desktop is WindowMaker so that's how I voted. The site's editors should have voted for the \"What is a window manager?\" option."
    author: "Otter"
  - subject: "Re: PCLinuxonline"
    date: 2001-05-24
    body: "Like i said otter it because its based of the aqua style for kde.\nCraig"
    author: "Craig"
  - subject: "Re: Kernel Cousin KDE #10 Released"
    date: 2001-05-25
    body: "pclinuxonline is very KDE centric, I'm sure you would get the result of GNOME at 72% on linuxpower."
    author: "ac"
  - subject: "Re: Kernel Cousin KDE #10 Released"
    date: 2001-05-25
    body: "Its not linux centric at all. In fact the top story was on pan. It looks like its down now. I notice the number of gnome votes jump 600 in acouple of minutes and  then the site crashed. It probably was under attack by the friendly neiberhood gnome troll.\n\nCraig"
    author: "Craig"
  - subject: "Re: Kernel Cousin KDE #10 Released"
    date: 2001-05-23
    body: "I wish the link to http://tick.dhs.org/~deneen/kpart-header.png would work.  I'm trying to understand what a konq_frame header is!"
    author: "KDE User"
  - subject: "Re: Kernel Cousin KDE #10 Released"
    date: 2001-05-23
    body: "ditto"
    author: "Hoju"
  - subject: "Re: Kernel Cousin KDE #10 Released"
    date: 2001-05-23
    body: "yes, its somewhat hard to understand w/out actually seeing it in action.. but let me try anyways =) at the top of the sidebar in konqi there is now a \"box\" that says \"header\" and has a button with an 'x' in it, which of course closes the sidebar when clicked. these headers are now available for any part in konqi to use. attatched is a screen shot."
    author: "Aaron J. Seigo"
  - subject: "Re: Kernel Cousin KDE #10 Released -- Sorry!!"
    date: 2001-05-23
    body: "sorry!\n\nI graduated from the university, and took my computer with me.  :-)\n\nI did not know that it was part of KK!\n\nTick"
    author: "Tick"
  - subject: "Many thanks"
    date: 2001-05-23
    body: "I'd just like to say thanks to Aaron for the work he puts into creating the KCK updates.  They are a truely enjoyable read, and something I look forward to each week."
    author: "Macka"
  - subject: "Re: Many thanks"
    date: 2001-05-23
    body: "Here Here.\nGreat Job"
    author: "Erik Severinghaus"
  - subject: "Re: Kernel Cousin KDE #10 Released"
    date: 2001-05-23
    body: "Can we expect to have mouse gestures in the Konqueror (and if possible in kde in general), as in opera 5.10. Its a very nice feature and makes browsing much faster and fun. Also the Konqueror starts reloading the page if it is asked to go back. How can I ask Konqueror to cahce the visited pages.\n\nThanks to all of the Kde developers. Great work!!"
    author: "Kundan Kumar"
  - subject: "Mouse Gesture Support"
    date: 2001-05-23
    body: "You can try out mouse gestures with KGesture. Get it from http://www.slac.com/~mpilone/projects/ I don't think gestures are much use with a mouse personally (though they're nice with a pen), but I guess there's no accounting for taste. ;-)\n\nRich."
    author: "Richard Moore"
  - subject: "Re: Mouse Gesture Support"
    date: 2003-03-13
    body: "Mouse gesture are dominant in webbrowsers.  In fact, the lack of gesture is the ONLY reason I won't use konq as my primary webbrowser."
    author: "HPV"
  - subject: "Re: Kernel Cousin KDE #10 Released"
    date: 2002-05-13
    body: "What's even better than mouse gestures in my opinion is the functionality of opera that lets you go back by holding the right mouse button then pressing the left button and vice versa for going forward. It speeds up browsing a lot when you don't have to mouse your mouse to the toolbar all the time"
    author: "J"
  - subject: "Re: Kernel Cousin KDE #10 Released"
    date: 2002-05-13
    body: "You can set up Konqueror to go back by just pressing the right mouse button (without moving mouse during pressing)."
    author: "Anonymous"
  - subject: "Another solution"
    date: 2001-05-23
    body: "While the hourglass cursor under application start might\nbe a help for newbies, it seems a kneefall for certain\nnot-quite-multitasking systems like win and mac (pre-X)\n\nIt gives the user the impression that he should wait and not\ncontinue work while the application starts up. Another, and\nin my opinion better, solution would be to make the buttons\nin the panel have a \"launch only one at a time\" option.\nIf this was selected for the given application,\nclicking on the button while the application was running\nwould simply make it active (like clicking on the taskbar\ndoes). If one fancy, a double click could overrule this \nbehavior and start a new instance of the application.\n\nOf course being a \"prochoice\" desktop both features\nshould be optional."
    author: "sune"
  - subject: "Re: Another solution"
    date: 2001-05-23
    body: "Terrific Idea.  I like this solution best.\n\nI still sometimes double-click on desktop icons and end up getting multiple Konqueror windows.  Or sometimes I click and it takes a while to load so I click again and again.\n\nI would love to be able to OPTIONALLY let only one application load at a time from desktop/panel invocation.  \n\nI am not so sure about the changing behavior, letting double clicks override the blocking behavior."
    author: "ed"
  - subject: "\"Working in background\" cursor"
    date: 2001-05-23
    body: "A couple of things:\n\n1) The busy cursor could be made similar to Windows \"working in the background\" busy cursor, where you have both an arrow and an hourglass (though I'd perfer a Motif-style watch instead of an hourglass).  thiw way, users would know the app is launching, but could still work on other things.\n\n2) Although having an option to launch only one instance of an app is a good idea, having it on by default would be irritating, since there are many legitimate reasons a user might want to launch a second instance of an app.  That user might think it's a bug if hitting the icon a second time didn't work.\n\nIf it was off by default, on the other hand, the same users that would think that nothing happened when they hit an icon might not look for this option in preferences.\n\n\n-Karl"
    author: "Karl Garrison"
  - subject: "Re: \"Working in background\" cursor"
    date: 2001-05-23
    body: "So, an interesting way would be the following :\nWhen you launch an app, append to the icon text 'launching' ...\nWhen the app is launched, remove it.\nYous still have the ability to run another instance of the app, but you are informed that the app is being launched. What do you think about it ?"
    author: "Aubanel"
  - subject: "Re: \"Working in background\" cursor"
    date: 2001-05-23
    body: "No I think he means that you can only LAUNCH one instance of the same application at the same time. When it is launched, you can launch a new one. This is a really great idea, much better than global hourglasses."
    author: "\u00d8ystein Heskestad"
  - subject: "Re: \"Working in background\" cursor"
    date: 2001-05-23
    body: "I agree, but I think it can be irritating to block the launch of another instance. IMO, it's better to just append [or why not replace by]'launching...' to the menu entry instead of graying it.\nYou have all the advantages and no inconvenient :\nyou can still launch another app instance, but you are informed than an instance of this program is allready starting=>Only satisfied users !"
    author: "Aubanel"
  - subject: "Re: \"Working in background\" cursor"
    date: 2001-05-24
    body: "Ok, then how do you define 'launching?'  The app would have to cooperate in some way.\n\nPerhaps a better way is to check for 'double-click' and ignore the second click, like Windows does."
    author: "Chris Bordeman"
  - subject: "Re: Another solution"
    date: 2001-05-23
    body: "apparently there is much speculation going on as to the exact incarnation of this feature. i didn't put more detail on it into the  KC since it isn't in final form  yet. however, this is how it currently operates:\n\nwhen you launch an app, a very small icon appears to the bottom right of the pointer (and it \"throbs\" right now, don't know if that will stay though). this leaves your pointer free to do what it would normally do and doesn't interfere with the \"multitasking\" feel of *nix IMO.\n\nas for allowing apps to launch only once, this was actually discussed briefly on the mailing list but is only a partial (and potentially confusing) solution. additionally, since it seems people _want_ most apps to be able to be openned multiple times its wouldn't be very useful in general application. there is actually a KUniqueApplication class, which  allow an app to only appear once at any given time no matter what. this is already used where it makes sense, and not used where it doesn't."
    author: "Aaron J. Seigo"
  - subject: "Re: Another solution"
    date: 2001-05-24
    body: "I am aware of the KUniqueApplication, but I am more thinking\nof the user experience than the technical side. \nKUniqueApplication is really only useful for special\nservice-like things like starting demons as I understand it.\nThe idea I have is more on the UI side, some users really\ndont want to have more than one mp3 player or kpacman\nrunning at the same time. My kids sometime happens to click\nintensively on a button to get an application started and end\nup with several incantations.\n\nI would never suggest the launch single app should be the\ndefault setting, but it would be a nice feature to enable\non certain programs for certain users for making things\nnewbie proof.\n\nIf you are not convinced consider the following not to unrealistic\nscenario:\n Most distros ships with no upper limit on the number of \nprocessors a user can start (and how many people go and\nfiddle with /etc/security/limits.conf ?).\nThis can make quite an embarrising show when you have\ntold everyone how stable Linux is and the first thing they\ndo is trying to launch netscape by clicking away with the\nresult that the machine stalls. If you have patience you\nmight be able to switch to a console and do a killall \nbut still the impression they leave with is \"Gosh! you have\nto invoke a command line just to help Netscape from\nchrashing...\""
    author: "sune"
  - subject: "Re: Another solution"
    date: 2001-05-24
    body: "KUniqueApp is used in various apps in KDE where it makes sense, e.g. KMail. as for the issue of opening a million copies of a program, you can do that in any OS. the feedback cursor tells the person something is happening and hopefully the person is intelligent enough to wait. people know the cursor-based feedback from other systems, it is hard to miss, etc. if it really is an issue for you, you can set user process limits.\n\nanother difficulty with the \"launch only once\" concept is that things can be launched from many different places using many different means. it would be difficult to impossible to make it work perfectly. even just implementing it for kicker buttons/menus wouldn't be trivial."
    author: "Aaron J. Seigo"
  - subject: "Re: Another solution"
    date: 2001-05-25
    body: "Why would you want a \"launch only once\" feature anyway?  A \"Launch only one at a time\" feature would be better.  As soon as the first one is done launching, another can be launched.  This would solve the problem where people click 10 times on the Konqui icon because it hasn't come up yet, but it would still allow for multiple instances.\n\nAlso, the only place where this would be needed would be Kicker buttons and Desktop icons, because they are the only things that can be clicked multiple times very fast."
    author: "not me"
  - subject: "Re: Another solution"
    date: 2001-05-23
    body: "\"Another, and\nin my opinion better, solution would be to make the buttons\nin the panel have a \"launch only one at a time\" option.\nIf this was selected for the given application,\"\n\nIsn't that what KUniqueApplication or whatever it's called in QT is supposed to do? I'm not actually programming C++/QT, but I've read something like that more than once.\n\nJonathan Brugge"
    author: "Jonathan Brugge"
  - subject: "Mouse Pointers Busy Animation?"
    date: 2001-05-23
    body: "Don't we have the technology to manipulate windows' *.cur and *.ani cursor files to be used in our KDE? Even if we prefer not to use them, we can create better Animated Mouse Pointers and Cursor Shapes than ugly windows ;)\n\nI strongly feel that KDE should have:\n\n1. White Mouse Pointers (with variable sizes)\n2. Animated Mouse pointers (busy, etc. actions)\n\nI feel that the black mouse pointer and that stupid looking clock is a blemish on our beautiful K Desktop Environment :) For beauty sake please, please create some Good Looking mouse pointers!"
    author: "Asif Ali Rizwaan"
  - subject: "Re: Mouse Pointers Busy Animation?"
    date: 2001-05-23
    body: "I agree.  Something like the Win2000 3D cursor would be cool."
    author: "Billy Nopants"
  - subject: "Re: Mouse Pointers Busy Animation?"
    date: 2001-05-26
    body: "AFAIK, it isn't currently possible to have alpha-blended stuff in X Windows (such as the Windows 2000 mouse pointer, and the fade-out menu selection thingy, which I actually like). But it surely would be very nice, as the shadow on the mouse pointer makes it stand out from the screen, and be easier to find, imho."
    author: "jliechty"
  - subject: "Re: Mouse Pointers Busy Animation?"
    date: 2001-05-23
    body: "Asif said :\n> I strongly feel that KDE should have:\n> 1. White Mouse Pointers (with variable sizes)\n\nYes, a white mouse pointer seems very important for me. It's psychic ;-), I feel that a white pointer is many more friendly than a dark pointer !\n\n> 2. Animated Mouse pointers (busy, etc. actions)\n\nNot important for me. However some Windows users like very much, children too, and it would be pretty..."
    author: "Alain"
  - subject: "Re: Mouse Pointers Busy Animation?"
    date: 2001-05-23
    body: "White mouse pointers instead of the black ones shouldn't be a big problem, I think one would just have to replace default X cursors font.\nAnimated cursors ( IMHO useless feature, but some users apparently love a lot of eye-candy ) are much more complicated though. X cursors are AFAIK just a two-color pixmap with a mask, nothing more. So to get colored and/or animated cursors some X app would have to simulate it somehow, probably with not very good results ( the busy icon lags behind the cursor when doing remote X, sometimes it lags even locally when the machine is under high load ). Maybe looking at how Enlightenment does its cursor might be a good idea.\n\nWe are the flashy icon. You will be assimilated. Resistance is futile."
    author: "Lubos Lunak"
  - subject: "Re: Mouse Pointers Busy Animation?"
    date: 2001-05-24
    body: "I have to disagree with this. Back when I only used Windows, I heard Mac users talk of the 'ugly white Windows cursor' (the standard Mac OS mouse cursors look, IIRC, much the the ones in X) and couldn't understand what they were talking about. After using X for a while though, I realized the advantages of a black cursor and came to prefer it. On primarily light-colored desktops, the black X cursor is much more visible (due to higher contrast) than a white cursor would be, and therefore is easier to find. Both Windows and KDE have light colored widgets by default and so a darker colored cursor is works best (on all the Windows machines I have to use, I've switched to the black cursor set and have had a much easier time keeping track of the cursor).\n\nIf people still want their white cursors, though, it would make sense to add support for cursor sets, probably in a way similar to how Windows does it - as long as black stays as the default ;)"
    author: "Andrew Medico"
  - subject: "Re: Mouse Pointers Busy Animation?"
    date: 2001-05-24
    body: "Andrew said :\n> the black X cursor is much more visible\n\nA red cursor is many more visible !! \n\nEeeh, yes, a red cursor is a good idea... and less sad than a black one... I would like to try...\n\nThe better is to give the choice to the user (with edition by Kiconedit). And OK for the black cursor by default, it is the unix habit..."
    author: "Alain"
  - subject: "Re: Mouse Pointers Busy Animation?"
    date: 2001-05-24
    body: "> A red cursor is many more visible !!\n\n> Eeeh, yes, a red cursor is a good idea... and less sad than a black one... I would like to try...\n\nSGI's 4DWM defaults to a red cursor, and it's one of the (very few) things I miss going from SGI to KDE.\n\nI would really appreciate a config option for the cursor colour."
    author: "John Pybus"
  - subject: "Re: Mouse Pointers Busy Animation?"
    date: 2001-05-24
    body: "Just take one of the cursor fonts mentioned  below, rename it to cursor.pcf.gz, copy it to  ~/.kde/share/fonts/override and restart KDE. Some versions of KDE will overwrite it during startup, so you should either protect it\nwith \"chattr +i\" or select  \"Peripherals/Mouse/Large Cursors\" in the Control Center first and then overwrite the font in the override directory with the new cursor font.\n\nhttp://www.unet.univie.ac.at/~a8603365/cursor_white.pcf.gz\nhttp://www.unet.univie.ac.at/~a8603365/cursor_large_white.pcf.gz\n\n\nThese fonts are just inverted versions of the original X-window font and\nthe large KDE font, so they are subject to the copyright of the original authors. And here's the\nobligatory screenshot:"
    author: "Melchior FRANZ"
  - subject: "Re: Mouse Pointers Busy Animation?"
    date: 2001-05-24
    body: "Damn, where is it? I mean the screenshot. OK, I put it on my homepage:\n\nhttp://www.unet.univie.ac.at/~a8603365/cursor.png"
    author: "Melchior FRANZ"
  - subject: "Is it a real big deal?"
    date: 2001-05-24
    body: "I don't mean to be a troll.  I really don't.  However, are cursors the most important part of KDE?"
    author: "Steve Hunt"
  - subject: "Re: Mouse Pointers Busy Animation?"
    date: 2003-02-01
    body: "Be lucky you have a mouse pointer.  I just installed Mandrake's 9.0 version and cannot find the mouse pointer;  it was there during the installation.  Any suggestions so I can comment on its attributes?  Thanks."
    author: "Thomas"
  - subject: "Kicker Menus"
    date: 2001-05-23
    body: "Like the console app menu, I would request for a 'Personal' menu, where the user specific applications should be placed, examples are:\n\n1. Address Book\n2. Change Password\n3. KDEPIM\n4. Task Scheduler\n5. etc..."
    author: "Asif Ali Rizwaan"
  - subject: "Re: Kicker Menus"
    date: 2001-05-24
    body: "You can of course do this yourself with the Menu Editor (K -> Configure Panel -> Menu Editor) but I agree that (re)organizing the panel (and kcontrol) layout is almost a never-ending story.\n\nThe Personal menu as you suggest would be very hard. Is a file manager Personal? Is KMail?\n\nBut suggestions are always welcome!\n(I wonder what happens if/when many third party vendors release KDE applications, will they all make their own menu as well?)"
    author: "Rob Kaper"
  - subject: "Re: Kicker Menus"
    date: 2001-05-25
    body: "What Loki does with their games is just put entries in the Games menu - I'm sort of neutral about this, because it would be nice if there were just a Loki Games menu, so that I'd be able to find those games in with all the KDE games.\n\nStar Office, OTOH, has its own menu."
    author: "David Watson"
  - subject: "Re: Kicker Menus"
    date: 2001-05-26
    body: "LOL, I believe staroffice still uses a kdelnk file. Ah, soon we will all be using koffice anyways."
    author: "Carbon"
  - subject: "KDE looks bad (cluttered) at 800x600 resolution"
    date: 2001-05-23
    body: "The dialogs and application windows get truncated (go beyond the screen), since the applications are designed for 1024x768+ resolution. By the way it is not the problem of XF86config file. \n\nPlease see KDE at 800x600, the window sizes are too big and the dialog boxes look real weird. \n\nKWord's interface also doesn't look neat at 800x600, since the window size is too small. (you can see the difference when you switch to 800x600).\n\nThe default kicker menu also look too large (except andrake's) at 800x600.\n\nI don't like big resolution 1024x768+ for normal work. \n\nI request you to kindly do small resolutions a favor :) by adjusting the window sizes and dialog boxes' text/widget arrangement."
    author: "Asif Ali Rizwaan"
  - subject: "Re: KDE looks bad (cluttered) at 800x600 resolution"
    date: 2001-05-23
    body: "Why run at 800*600 theese days? Must look grotty! If you have a computer that is modern enough to run KDE it should really be able to handle 1024*768 at high enough refresh rate."
    author: "Erik"
  - subject: "Re: KDE looks bad (cluttered) at 800x600 resolution"
    date: 2001-05-23
    body: "Did it ever occur to you that 1024x768 is too small for some people.  I.e people with vision problems.  Just saying \"Crank it up... your monitor can handle it\" may not actually be the solution !"
    author: "Billy Nopants"
  - subject: "Re: KDE looks bad (cluttered) at 800x600 resolution"
    date: 2001-05-23
    body: "What exactly is too small for you? Fonts can be changed (K -> Control Center -> Look & Feel -> Fonts). I use 1280*1024 and the main font in Konqueror is 5mm high. I can read it from 2m although I normally read it from 0.3m .. 1m.\n\nYou do know that kicker can have 4 different sizes, don't you? I recomment you to set it to huge.\n\nThe worst problem I have encountered is that the Kwin titlebars don't scale with the font used in them."
    author: "Erik"
  - subject: "Re: KDE looks bad (cluttered) at 800x600 resolution"
    date: 2001-05-24
    body: "Sure, but widgets can't resize, and many application fonts don't use the system sizes.\n\nThis \"it's you, not the program\" attitude is not constructive."
    author: "Chris Bordeman"
  - subject: "Re: KDE looks bad (cluttered) at 800x600 resolution"
    date: 2001-05-24
    body: "Which of your widgets can't resize? At least on my computer just about any widget can be resized. Toolbars can have several sizes, widgets containing text (buttons, menus, tabbooks, treeviews, ...) can resize if you chagne the font size used in them.\n\nI know that some widgets don't resize well for obscene scaling, like going from size 11 to size 32, but that shouldn't be neccessary when increasing the resolution from 800*600 to only 1024*768. I use 1280*1024 and only have to scale from size 11 to size 16.\n\nIf there is I a widget or an application that doesn't scale well or doesn't obey your desktop wide settings, report it at bugs.kde.org. I have done it several times.\n\nAnd if you want to go back to 800*600 (or even 640*480) now and then, use Ctrl+- and Ctrl++ to zoom in and out. Then you will get a virtual desktop that is bigger than the visible part of it. You can scroll it by moving the mouse pointer. Scrolling works fast, even with my crappy cheap and 2 years old card."
    author: "Erik"
  - subject: "Works for me =)"
    date: 2001-05-23
    body: "I use a very small laptop (Fujitsu LifeBook B142), with an LCD that is only capable of 800x600. I wouldn't change it for a bigger one as I like the portability of it.\n\nAs has already been mentioned, kicker's size can be changed. I use 2 \"tiny\" panels, one on the bottom with some launcher buttons, a news ticker and the system tray, and one on the top with the pager, desktop access button and taskbar.\n\nFonts can also be changed for just about everything. I recommend removing any -dpi option in /etc/X11/kdm/Xservers, and having a .gtkrc (as opposed to .gtkrc-kde) that sets sensible font sizes.\n\nSuffice to say with a little effort, KDE can be made to look very nice indeed on a low resolution. Here's a screenshot..."
    author: "Chris Boyle"
  - subject: "too big & nasty"
    date: 2001-06-29
    body: "In the screenshot that you posted there, it is evident that certain kde widgets are just too big and nasty.  I love kde's cleanliness and and i like the clean framework of kde. there are two things that i dont like: 1. compilation always crashes and 2. widgets (ie. the menu bar) are too big and take up too much space (even at 1024x 768)\n\nin konqueror (and much worse in opera), i barely get any screen-space left over to see web-pages. compare that to my setup on windoze/iexplore (see attached screenshot at 1024x768 which gives me insane ammounts of real-estate despite having 6 launcher buttons, 1 start button, and 8 appletts docked in the system tray.\n\nKde could use improvement in this regard (space efficiency).  I think it's one of the main reasons that programs like the gimp and xmms use a toolkit other than kdelibs/qt."
    author: "Pablo Liska"
  - subject: "What about \"Configure Konqueror\" like Dialogs?"
    date: 2001-05-24
    body: "800x600 is a Desktop PC's (windows) standard resolution, and it is a matter of personal choice to use 640x480, 800x600 or 1600x1280 ;)\n\nIf you see Windows, its dialogs looks perfect even at the lowest resolution 640x480. KDE should also be perfect at any resolution in displaying dialogs and windows.\n\nTake for example, in your Konqueror, select Settings->configure Konqueror..., now at 1024x768 the window looks good, but at lower resolutions 800x600 (and lower) it just looks ugly.\n\nKDE is not ugly :)"
    author: "Asif Ali Rizwaan"
  - subject: "Re: What about \"Configure Konqueror\" like Dialogs?"
    date: 2001-05-24
    body: "Then how come the display properies dialog in Windows 98 reaches below the bottom of the screen in 640*480?"
    author: "Erik"
  - subject: "It Doesn't :)"
    date: 2001-05-26
    body: "The display properties dilaog in windows 98 doesn't reaches below, i have myself seen that by switching to that resolution on a windoze pc. What about KDE dialogs at 640x480?"
    author: "Asif Ali Rizwaan"
  - subject: "Re: It Doesn't :)"
    date: 2001-05-26
    body: "There is a setting in Windows that lets you choose between small and large fonts. I guess I tried it with large fonts and you tried it with small fonts. (Windows has other font size settings as well.)"
    author: "Erik"
  - subject: "Re: What about \"Configure Konqueror\" like Dialogs?"
    date: 2001-10-27
    body: "For people with a notebook not capable of viewing 800*600 it is a real problem. I can't see the complete configure-panels of konqueror or the control center. They aren't resizable..\n\nIn kde 2.1 they fitted on my screen, in kde 2.2 they are to large and I can't make them smaller..\n\nI hope the kde team will make a patch for this, so I can make happy use of kde again..\n\n\nRegards,\nChris"
    author: "Chris"
  - subject: "Re: What about \"Configure Konqueror\" like Dialogs?"
    date: 2003-05-23
    body: "Your right. Window's uses 'Dialog Base Units' to calculate the size of it's controls. The reason they do this is so there GUI looks the same under different resolutions. KDE don't do this, they just use Pixel measurements. And they use the same Pixels for all resolution, Which obviously point's to 2 possibilities:\n\n1) They don't want to support 15 Inch and lower Monitors.\n2) They don't know what there doing."
    author: "Mark P."
  - subject: "Re: KDE looks bad (cluttered) at 800x600 resolutio"
    date: 2005-05-21
    body: "Hello everyone, i think my problem is similar so i will leave my question here,\ni am using debian linux and i use KDE 3.3 now i am knew with linux, i got sick of windows, still though, linux is hard for me to understand, because my resolution in linux in 1024x740 when i have all windows and programs closed it looks just fine, just as it should with a resolution of 1024x740 however when a window is open for instance mizilla firefox it looks as if still was in 800x600 the same goes for the start panel text boxes everything, so what am i doing wrong, please kind people help me remember though i am very new at this linux world thing.\nthank you Dario\nand sorry about the grammar is almost 5 in the morning and i need to got to bed"
    author: "Dario"
  - subject: "Re: KDE looks bad (cluttered) at 800x600 resolutio"
    date: 2005-09-09
    body: "I have a program for car parts taht the program is bigger tahn the screen i tried to put a higher resolution, but my computer will not go as low as needed. Thanks"
    author: "nuria escalona"
  - subject: "Kicker Menus Icons"
    date: 2001-05-23
    body: "The main menu of kicker, has small icons, if we could have 22x22 or 24x24 icons there then the menu will look more attractive. \n\nHere is the attachment of windows 98's start menu. KDE has better icons so it will look a million times better than windows."
    author: "Asif Ali Rizwaan"
  - subject: "Re: Kicker Menus Icons"
    date: 2001-05-24
    body: "Nitpicking: application icons are 16x16, 32x32, 48x48, and 64x64."
    author: "Lenny"
  - subject: "Re: Kernel Cousin KDE #10 Released"
    date: 2001-05-23
    body: "Percentages don't matter,what's matters is that there is a choice, no single way is the only way."
    author: "Pieter Philipse"
---
Kernel Cousin KDE #10 has just been released, as usual it describes the latest discussions on the KDE development lists. Topics this week include a new busy cursor implementation, KSpread filter news and the code freeze for KDE 2.2 alpha2. You can find the <a href="http://kt.zork.net/kde/kde20010517_10.html">latest issue here</a>.
<!--break-->
