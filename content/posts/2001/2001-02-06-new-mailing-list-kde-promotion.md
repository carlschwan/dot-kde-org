---
title: "New Mailing List for KDE Promotion"
date:    2001-02-06
authors:
  - "Dre"
slug:    new-mailing-list-kde-promotion
comments:
  - subject: "Re: New Mailing List for KDE Promotion"
    date: 2001-02-06
    body: "That's nice. This mailing list will make it easier even for people like me to support KDE."
    author: "Triskelios"
  - subject: "Re: New Mailing List for KDE Promotion"
    date: 2001-02-06
    body: "Thanks to Torsten for the spiffy new icon! ;)\n\n-N."
    author: "Navindra Umanee"
  - subject: "Re: New Mailing List for KDE Promotion"
    date: 2001-02-06
    body: "I just wonder if anyone can tell if the DOM/CSS/JavaScript/HTML support in Konquerer is mature enough for serious Dynamic HTML development, and if anyone (besides me) have thought about trying to making some popular open source DHTML cross-platform libraries compatible with Konquerer ? This might look a bit off-topic, but it would make Konquerer and KDE a lot more visible and \"serious\" for developers if popular DHTML libraries stated Konquerer/KDE support (Off course it would also make surfing with Konquerer a bit more funny).\n\nYeah - I know this should have been posted on the mailinglist, but I don't want to subscribe it now (problably some other time when I'm a bit more experienced - I'm very new to KDE right now)"
    author: "Stig"
  - subject: "Re: New Mailing List for KDE Promotion"
    date: 2001-04-26
    body: "Yes, Konqueror is definitely mature. Of course there are still a few problems, the most important of which is the lack of support for clipping, but in general Konqueror is excellent. It's better than Opera in the W3C DOM and its CSS support is excellent.\n\nOf course the user basis of Konqueror still has to grow and web developers have to get used to yet another browser. A problem will be that it's only available on Linux/Unix, so that developers without these OS cannot check their sites in it.\n\nFor more info on the W3C DOM support of Konqueror and the other browsers, see http://www.xs4all.nl/~ppk/js/index.html?version5.html\n\nppk"
    author: "Peter-Paul Koch"
---
A new mailing list has been launched for KDE'rs interested in KDE advocacy and promotion.   Possible discussion/action items range from writing articles about KDE for the <A HREF="http://dot.kde.org/">dot</A>, to handing out CDs in computer science centers at universities, to writing stories or reviews.  This forum is for brain-storming ideas and bringing them to fruition.  Those interested in helping to "get the word out" about KDE can subscribe <A HREF="http://lists.kde.com/lists/listinfo/kde-promo">online</A> or by <A HREF="mailto:kde-promo-request@lists.kde.com?subject=subscribe">mail</A>. <b>Update: 02/09 10:55 AM</b> by <a href="mailto:navindra@kde.org">N</a>: The mailing list and current subscribers have been moved, subscribe online <a href="http://master.kde.org/mailman/listinfo/kde-promo">here</a> or by mail <a href="mailto:kde-promo-request@master.kde.org?subject=subscribe">here</a>.

<!--break-->
