---
title: "C'T Releases IOSlave Tutorial"
date:    2001-07-22
authors:
  - "rmoore"
slug:    ct-releases-ioslave-tutorial
comments:
  - subject: "Re: C'T Releases IOSlave Tutorial"
    date: 2001-07-22
    body: "you should go there for the dragon pic alone.  :)"
    author: "ac"
  - subject: "Re: C'T Releases IOSlave Tutorial"
    date: 2001-07-22
    body: "You mean:\n<br><br>\n<a href=\"http://www.heise.de/ct/motive/01/05/p1600.jpg\">http://www.heise.de/ct/motive/01/05/p1600.jpg</a>\n<br><br>\nHe, he, there more stuff (free for personal use only):<br>\n<a href=\"http://www.heise.de/ct/motive/\">http://www.heise.de/ct/motive/</a>\n<br><br>\nOne of my favorite:<br>\n<a href=\"http://www.heise.de/ct/motive/99/18/p1600.jpg\">http://www.heise.de/ct/motive/99/18/p1600.jpg</a>\n<br><Br>\nHave Fun!"
    author: "Thorsten Schnebeck"
  - subject: "Re: C'T Releases IOSlave Tutorial"
    date: 2001-07-23
    body: "These are great. Especially the last one. I have a new standard background now ;-)."
    author: "jj"
  - subject: "Re: C'T Releases IOSlave Tutorial"
    date: 2001-07-22
    body: "Hi\n\nA nice way to check if the 'modularity' of KDE and IOSlave is functioning is to have eight desktops open, each with at least one window going, and then press - Ctrl-Alt-Del.\n\nHere is from my last study in Mandrake 8.1 Cooker, taken from .xsession errors.\nwarning: leaving MCOP Dispatcher and still 69 types alive.                      \nDCOP:  unregister 'knotify'                                                     \nTerminal:  fatal IO error 32 (Broken pipe) or KillClient on X server \":0.0\"     \nTerminal:  fatal IO error 32 (Broken pipe) or KillClient on X server \":0.0\"     \nkdeinit: sending SIGTERM to children.                                           \nkdeinit: Exit.\n\nSuch a thing should not happen in ADA95 but shows what c++ can do.\n\nregards\nguran"
    author: "guran"
  - subject: "Re: C'T Releases IOSlave Tutorial"
    date: 2001-07-23
    body: "That has litle to do with ADA or C++ but more with \napplications not doing session management and not installing an X-IO-error event handler.\n\nAnyway, it may not be the most elegant way but it is just one way to kill processes that are still running at the time the X server shuts down.\n\nApart from that, yes ADA is a much more reliable language than C++. You really shouldn't use KDE for your missile defense system.\n\nCheers,\nWaldo"
    author: "Waldo Bastian"
  - subject: "Re: C'T Releases IOSlave Tutorial"
    date: 2001-07-24
    body: "> warning: leaving MCOP Dispatcher and still 69 <br>> types alive. <br>\n> DCOP: unregister 'knotify' <br>\n> Terminal: fatal IO error 32 (Broken pipe) or<br>\n> KillClient on X server \":0.0\"<br>\n> Terminal: fatal IO error 32 (Broken pipe) or<br>\n> KillClient on X server \":0.0\" <Br>\n> kdeinit: sending SIGTERM to children.<br>\n> kdeinit: Exit.<br>\n<br>\n> Such a thing should not happen in ADA95 but<br>\n> shows what c++ can do.<br>\n<br><br>\nSpeaking of Ada95, what is happening with <a href=\"http://sourceforge.net/projects/qtada/\">this</a> project? (Is guran related to cgouiran?)"
    author: "Erik"
  - subject: "The pedant"
    date: 2001-07-22
    body: "the magazine is named \"c't\" and not \"C'T\"<br>\n--<br>\n<a href=\"http://www.douglasadams.com/dna/pedants.html\">http://www.douglasadams.com/dna/pedants.html</a>"
    author: "Thorsten Schnebeck"
  - subject: "Re: The pedant"
    date: 2001-07-22
    body: "I fixed it in the body of the article, but avoided a title change so as not to confuse some sites that use the RDF.\n\n-N."
    author: "Navindra Umanee"
  - subject: "Re: The pedant"
    date: 2001-07-22
    body: "Whats your RDF address... I get stuff that is about a year old in Nautilus and Evolution."
    author: "AC"
  - subject: "rdf"
    date: 2001-07-23
    body: "One year old?  Wow, that's pretty cool.  Where do you get that from?  Use:\n<br><br>\n<a href=\"http://www.kde.org/dotkdeorg.rdf\">http://www.kde.org/dotkdeorg.rdf</a>\n<br><br>\nor\n<br><br>\n<a href=\"http://dot.kde.org/rdf\">http://dot.kde.org/rdf</a>"
    author: "Navindra Umanee"
  - subject: "Re: rdf"
    date: 2001-07-23
    body: "Thanks :)\nNot sure what rdf it points to, but here is the list of news links it gives me\n\nPeople Behind KDE: Reginald Stadlbauer\nOpen Content KDE Developer Book\nKDE 2.0 released\nPeople Behind KDE: Claudiu Costin\nDevelopment events\nRefreshing winds\nPeople behind KDE: David Faure\nKDE Desktop 2.0 Final Release Candidate Available"
    author: "AC"
  - subject: "Re: rdf"
    date: 2001-07-24
    body: "Okay, I have hunted down the errors and have requested updates from GNOME and Ximian.  Hopefully they'll followup.\n\n-N."
    author: "Navindra Umanee"
  - subject: "Re: rdf"
    date: 2001-07-24
    body: "Is there a RDF for apps.kde.org? I'd like to have this one."
    author: "Paulo Eduardo Neves"
  - subject: "Re: rdf"
    date: 2001-07-24
    body: "Ever read the FAQ at http://apps.kde.com?\n\n| 6. Is there a dynamic newsfeed for new application announcements?\n|\n| Yes, it's available at http://apps.kde.com//news/apps.kde.com.rdf"
    author: "someone"
  - subject: "Re: C'T Releases IOSlave Tutorial"
    date: 2001-07-23
    body: "The german version can be found here:\n\"http://www.heise.de/ct/01/05/242/default.shtml"
    author: "Victor R\u00f6der"
  - subject: "Collection of Tutorials?"
    date: 2001-07-23
    body: "Seems to me that every week or two there is a link here to some sort of useful tutorial.  Is there a site anywhere that compiles these links, along with other kde tutorials floating around the web?\n\nI think this sort of thing would be a very handy resource for people (such as myself) who are learning to do some kde programming."
    author: "Carl R"
  - subject: "Re: Collection of Tutorials?"
    date: 2001-07-23
    body: "On developer.kde.org of course :-)\n<br><br>\n<a href=\"http://developer.kde.org/documentation/tutorials\">http://developer.kde.org/documentation/tutorials</a>\n<br><br>\nCheers,<br>\nWaldo"
    author: "Waldo Bastian"
  - subject: "Re: Collection of Tutorials?"
    date: 2001-07-23
    body: "and a tutorial about kcontrol plugins???????????"
    author: "fan of kde"
  - subject: "Re: Collection of Tutorials?"
    date: 2001-07-25
    body: "Thanks.  I wasn't aware that this page liked to off-site tutorials (like the c'T one) as well.  I suppose that's a pretty good indicator of how much I've actually gone and looked at it!\n\n--c."
    author: "Carl R"
  - subject: "Some ideas"
    date: 2001-07-26
    body: "Cool, this might actually inspire me to write some ioslaves.  Maybe a cvs slave, maybe a SQL slave, or how about a remsh(rcp, rmv...) slave.  Is there an rsync or ssh slave out there?  I thought about an mtools slave, but I'm not sure if it would be usefull, maybe for unix platforms that don't do FAT.  Would an rdf slave be silly?  \n  ...Actually I'm really way too lazy :( to write any of those, but I figured I'd suggest them anyway.  :) \n\n  Another neat concept would be a scripting interface you could use to create ioslaves.  I certainly would play around with that!"
    author: "Martin Fick"
  - subject: "Re: Some ideas"
    date: 2001-07-26
    body: "> Is there an rsync or ssh slave out there?\n\nA combined ssh/scp-slave would be cool: ssh for listing/renaming etc. and scp for drag&drop transfers."
    author: "someone"
---
<a href="http://www.heise.de/ct/">c't magazine</a> has just published an English translation of an <a href="http://www.heise.de/ct/english/01/05/242/">IOSlave tutorial</a> written by Carsten Pfeiffer and Stephan Kulow. The article tells you everything you need to know to add support for new protocols to KDE. Examples covered include slaves that generate HTML and an ioslave for audio CDs. This is a great tutorial, so check it out and start coding!
<!--break-->
