---
title: "People behind KDE: Martin Jones"
date:    2001-06-25
authors:
  - "Inorog"
slug:    people-behind-kde-martin-jones
comments:
  - subject: "Re: People behind KDE: Martin Jones"
    date: 2001-06-25
    body: "Martin, what about porting my favorite sreensaver Galaxy to KDE2? I just love to see those galaxies crash, gives me an illusion of grandeur."
    author: "reihal"
  - subject: "Re: People behind KDE: Martin Jones"
    date: 2001-06-25
    body: "> what about porting my favorite sreensaver\n> Galaxy to KDE2?\n\nI think all xscreensaver are already integrated in the KDE screensaver selection, at least they are for KDE 2.2 (running CVS). I would swear it has been so since 2.0 though..."
    author: "Rob Kaper"
  - subject: "Re: People behind KDE: Martin Jones"
    date: 2001-06-25
    body: "You're right - I think it was in 2.0, but for some reason disappeared in 2.1. Very odd... It will be good to have it back ;)"
    author: "David G. Watson"
  - subject: "Re: People behind KDE: Martin Jones"
    date: 2001-06-26
    body: "It should be all (!!!) xlockmore (the \"real\" xlock) modes. Especially the GL modes are pretty well.\nI did regularly beta testing of xlockmore with the DRI CVS stuff.\n\nRegards,\n\tDieter"
    author: "Dieter N\u00fctzel"
  - subject: "Suggested improvement for Galaxy Screen Saver"
    date: 2004-02-13
    body: "I was watching the Galaxy screen saver for, well, too long today.  I noticed that sometimes, the gravitational acceleration is extreme.  I thougt it would be cool if gravitational sheer was included, tearing 'stars' assunder (turn them into tracers that fade out or something).  \n\nWhat led me to think of this was all of those pretty Hubble photos where galaxies are generally well ordered affairs, even when consuming eachother... I presume they have the appearance of order due both to the scale of astronomy making galactic movements appear to stand still, and the notion that some stars in galaxy colisions are actually torn apart by gravitational sheer... theoretically reducing the number of rogue stars floating about the universe.\n\nSorry if this isn't the proper place to post this comment... just happened by."
    author: "Jerry Carter"
  - subject: "How AMOR get annoying"
    date: 2001-06-25
    body: "I would request that Amor should have any of these options:\n\n1. Kill all instances of Amor (killall -KILL amor) {Stephen Kulow told me that this should be in KSysGuard as <killall -KILL <app name>>) But nothing happend yet.\n\n2. Amor should get disabled after sometime of inactivity, like screensavers get activated after sometime.\n\n3. Amor should also have an option for \"Use Random Amor characters\" for a more lively display of amor.\n\n4. Amor should also use Sounds (small and nice).\n\nHope you like my request. Anyway I like AMOR and don't find it annoying :) Thank you!"
    author: "Asif Ali Rizwaan"
  - subject: "Re: People behind KDE: Martin Jones"
    date: 2001-06-25
    body: "Is he not aware of Kmag?  I think it is nicer than xmag."
    author: "David Walser"
  - subject: "Re: People behind KDE: Martin Jones"
    date: 2001-06-25
    body: "It seems that if an application is not bundled with KDE, it rarely makes it to a distribution.  This will hopefully change with the KDE Fifth Toe package which will bundle third-party applications."
    author: "KDE User"
  - subject: "KDE Fifth Toe package"
    date: 2001-06-26
    body: "KDE Fifth Toe package ? Have you some url about it ? \n\nI hope that Quanta Plus, KonCD and some others are in this package... When will it be available ? With KDE 2.2 ?\n\nIt's certainly a very good thing !"
    author: "Alain"
  - subject: "KDE Spare gear package"
    date: 2001-06-26
    body: "KDE does't have a large footprint."
    author: "anon"
---
The moment has come for the last two interviews <a href="mailto:tink@kde.org">Tink</a> has prepared for us before the great summer break. The first interview features the KDE veteran <a href="http://www.kde.org/people/martin.html">Martin Jones</a>, whose brain children wander daily on the screens of our KDE boxes, in the form of screensavers. You might also be enjoying his adorable little toy dubbed <i><a href="http://www.powerup.com.au/~mjones/amor/index.html">AMOR</a></i>. Martin also speaks to us about his family and his passion for <a href="http://www.trolltech.com/products/qt/embedded/index.html">Qt/Embedded</a>. Enjoy!
<!--break-->
