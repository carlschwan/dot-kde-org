---
title: "KDE-Look.org: EyeCandy for your KDE Desktop"
date:    2001-10-09
authors:
  - "ee."
slug:    kde-lookorg-eyecandy-your-kde-desktop
comments:
  - subject: "congratulations"
    date: 2001-10-09
    body: "This website is top-notch.\n\nCheers,\n-N."
    author: "Navindra Umanee"
  - subject: "Re: congratulations"
    date: 2001-10-09
    body: "Finally you don't have to wait for themes.org coming up again!"
    author: "AC"
  - subject: "Nice indeed."
    date: 2001-10-09
    body: "Very cool stuff."
    author: "AB"
  - subject: "great"
    date: 2001-10-09
    body: "great. i love this site. thanx!\ngunnar"
    author: "gunnar"
  - subject: "Fantastic !"
    date: 2001-10-09
    body: "Nuff said."
    author: "Qwertz"
  - subject: "So long, themes.org, and thanks for the fish!"
    date: 2001-10-09
    body: "When I learned about kde-look.org a couple of days ago, I checked it out and liked what I saw. I then looked at themes.org and noticed that it's down again, with traffic being directed toward the old (\"classic\") site. There are lessons to be learned here, I think. If I may be so bold as to offer the following advice to the kde-look.org maintainers:\n\n\"Lessons to learn from themes.org\":\n1. Quality counts. Don't accept every variation on a theme, just for the numbers. Themes.org was always flooded with submissions, but most of them were just previous themes with a different color scheme. I used to use Enlightenment before switching to KDE, and I can tell you, a truly interesting and creative theme would only come out once every three to six months. All the others were...well, I'm going to get flamed, but they were honestly uncreative crap. For those of you who remember when AbsolutE came out, think of how many clones popped up; most of them just changed the colors around. There were hundreds of themes at e.themes.org, and I looked through all of them, but honestly, the number could have been trimmed down to ten or twenty. Rewarding quality over quantity will have two results: one, you will encourage people to be more creative, resulting in better schemes/themes. Two, you will make life easier on yourself when it comes to update the website/database. I suspect part of themes.org's problem is that it simply has too many themes, most of which are unnecessarily dragging it down.\n\n2. People want themes, not a website experience. The lesson from themes.org is painfully clear: if for some reason, kde-look.org goes down, gets wiped/cracked/whatever, and you have to rebuild from scratch, don't let your fans wait for six months. They will leave. If you have to put up a plain-text site, do it. Don't decide, \"Hmm, well, it's time to update the look of the site, people can wait.\" They won't. Witness the flames on the themes.org message board. I'm not one of those flamers. I was willing to wait for one of two things: either themes.org comes back, or another site takes its place. The second has occured. This surfer is going to take his hit traffic to another site. Sorry. Content is more important to me than layout. Themes.org looks (or did, for the brief amount of time that the new site was up) gorgeous, but there was nothing there. Oops.\n\nI'm sure there's more, but it's still early in the morning. I am looking forward to neurotically checking the site for new themes every day. :) All I ask is that you do ONE thing, and do it well: give us a place to find and post themes. Then you will be greatly loved, and great will be your reward. Thanks, guys!\n\n:Peter"
    author: "Peter Clark"
  - subject: "Re: So long, themes.org, and thanks for the fish!"
    date: 2001-10-09
    body: "Actually, part of the reason for redesigning themes.org is to accomodate your first comment; allowing the person who posted a theme to add variants of it, so if someone has minor changes like recoloring, the original theme author can choose to make them available without cluttering up the main site - minor variants will not be accepted as main theme submissions anymore. (At least that's what I was told by some themes.org people, I could be wrong.)"
    author: "Kenny Graunke"
  - subject: "Re: So long, themes.org, and thanks for the fish!"
    date: 2001-10-09
    body: ">1. Quality counts. <\n\nYeah, but who will be the judge? \n\n>I can tell you, a truly interesting and creative theme would only come out once every three to six months. All the others were...well, I'm going to get flamed, but they were honestly uncreative crap.>\n\nIt is a matter of taste. What is in your opinion 'crap' it is not 'crap' to others. Or better said: what is not crap today it becomes crap tomorrow. I used to use Aqua clones for a couple of weeks, and then I got sick and tired of them. Now I think they are all crap (including Apple's original Aqua style), but many other people don't have the same taste. For them is Aqua style probably the most beautiful theme (or style). And we have to respect that. And Aqua clones are the most downloaded themes & styles on themes.org and will be, I am sure, the most downloaded styles & themes on kde-look.org. \nAbsoluteE is beautiful to you, but (I'm sorry) it is IMHO ugly.\n\n>Rewarding quality over quantity will have two results: one, you will encourage people to be more creative, resulting in better schemes/themes >\n\nI am not so sure. I think we need a really good tutorial on how to create themes and styles (not coded, but pixmap's ones). \n\nP.S. IMHO, kde-look.org is very nice, and it is very good idea to have all possible themes and styles (which can be used in kde) in one place: kde ones, icewm ones, gtk, noatun, icons... :)"
    author: "antialias"
  - subject: "Re: So long, themes.org, and thanks for the fish!"
    date: 2001-10-09
    body: "> Yeah, but who will be the judge? \n\nI think it's pretty easy to draw a minimal line. For one, no blatent rip-offs, changing jsut the color scheme. Different color schemes should be grouped under the parent theme, if at all.\n\n> It is a matter of taste. What is in your opinion 'crap' it is not 'crap' to others. \n\nOk, how about this: if a theme drops off in popularity after a set amount of time (is two-three months reasonable?) it's yanked. In other words, let the users vote with their downloads. Now, granted, some themes are super popular and have their peak, then trail off. Special provisions should be made for these. But can anyone justify keeping a theme that is close to two years old and only has a few hundred downloads, most of them from the first week?\n\n> AbsoluteE is beautiful to you, but (I'm sorry) it is IMHO ugly.\n\nApology accepted. I think AbsolutE is hideous, too. I was using it as an example of something that has been ripped, duplicated, and copied a hundred times.\n\n> I am not so sure. I think we need a really good tutorial on how to create themes and styles (not coded, but pixmap's ones). \n\nHear, hear! I would really love to make a theme with vertical text (the title bar on the left side) but have no idea, because there's no documentation!\n:Peter"
    author: "Peter Clark"
  - subject: "Re: So long, themes.org, and thanks for the fish!"
    date: 2001-10-10
    body: "how about when the theme doesn't even look right because it is coded wrong, missing images, or makes assumptions about color scheme, settings, etc...\n\nI would call those themes \"crap\" without offending anyone's taste. (if not the developer's ego)\n\n-pos"
    author: "pos"
  - subject: "Re: So long, themes.org, and thanks for the fish!"
    date: 2001-10-10
    body: "better call them \"buggy\", this is less insulting\n\nAlex"
    author: "aleXXX"
  - subject: "Re: So long, themes.org, and thanks for the fish!"
    date: 2001-10-10
    body: "I see your point about posting small variations, but I for one like having a lot of variants available. A lot of times I find one or two variants on a main theme that I really like, whereas all the other variants (and the original theme) I don't. It seems like a good solution would be to just list variants under their \"original\" theme (if there is an original even; but then how can you call any Aqua-like KDE theme the original when the original is Apple's?)\n\nAlso, I was just wondering why the B II, KStep, etc. Kwin styles look so *different* from the styles they were patterned after (in this case BeOS and *Step, respectively)? I can understand artistic license, but these styles bear hardly any resemblance to the originals. I'd think one might as well come up with different names for them."
    author: "Rakko"
  - subject: "Re:"
    date: 2001-10-09
    body: "Kde-look.org even support GTK+ themes! They rock!"
    author: "Stof"
  - subject: "Re: (logical)"
    date: 2001-10-09
    body: "kde also supports gtk themes..."
    author: "b"
  - subject: "Re: (logical)"
    date: 2001-10-10
    body: "No it doesn't ...\n\nI have yet to see a theme that was successfully imported."
    author: "Carg"
  - subject: "Re: (logical)"
    date: 2001-10-10
    body: ": I have yet to see a theme that was successfully imported.\n\n    I have.\n\n    On the other hand, I have yet to see you.  \n\n    So you don't exist.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: (logical)"
    date: 2001-10-11
    body: ">  I have.\n\nPlease show me. I've been trying this since KDE 2.0 and couldn't get it right.\n\n>  On the other hand, I have yet to see you.\n\nThank God.\n\n>  So you don't exist.\n\nTroll."
    author: "Carg"
  - subject: "Re: (logical)"
    date: 2001-10-11
    body: ": Troll.\n\n    So says the person who stated that it dosen't work because it dosen't work for him.  I downloaded the KDE 2.1 rpms for SuSE 7.1, and then went to themes.org.  I downloaded about three or so themes, installed them, and then installed them in the Control Center.  It worked fine.  I think I'm using the eazel theme, as everytime I load up a gtk app, it repeats the following seven times:\n \nGtk-WARNING **: Unable to locate loadable module in module_path: \"libeazel-engine.so\",\n \n    But everything *looks* like it's supposed to, according to the corresponding screenshot, and I've not had any problems with xmms or lopster, the only two non-KDE apps I really use  (not that XMMS uses the theme).\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: (logical)"
    date: 2001-10-13
    body: "Could you please post a screenshot?"
    author: "Carg"
  - subject: "Re: (logical)"
    date: 2001-10-11
    body: "Look at attached screenshot of konsole window\n\ntheme is imported pixmap version of eazel-blue\n\nkwin thme is spiffIce through IceWM theme client for kwin"
    author: "Primus"
  - subject: "Re: (logical)"
    date: 2001-10-10
    body: "any pixmap one should work\n\ni've tried 100's of them, they all work\n\ni should mention that in 2.2.x, a lot of it was fixed"
    author: "asasd"
  - subject: "Re: (logical)"
    date: 2001-10-11
    body: "I've tried all of them, none of them work here :(\nI've tried them on all versions of KDE 2xx :(\nWhen I import one it totally screws up my fonts. They become\nso small that they are unreadable. Then I import second one and my fonts\nbecome even smaller etc. \nI've asked many places (also on mailing lists)for help, nobody ever gave me an answer about the problem. I mostly always compile kde sources, but distros rpms\nhave the same problem."
    author: "antialias"
  - subject: "mosfet is there"
    date: 2001-10-09
    body: "try his liquid theme. All quarrel apart, his theme rocks."
    author: "try"
  - subject: "Re: mosfet is there"
    date: 2001-10-10
    body: "The 'liquid' widget style itself is adequate.\nThe menu styling on the other hand is very good -- they should be seperated out from the theme, and put in the base KDE libs so that users can have transparent menus with any style."
    author: "Jon"
  - subject: "Re: mosfet is there"
    date: 2001-10-10
    body: "which would probably bring us back in the quarrel thing"
    author: "try"
  - subject: "Re: mosfet is there"
    date: 2001-10-10
    body: "I compile the theme source code on Mandrake 8.1 but It mentioned errors:\n-- liquidclient.h:5:20: client.h: No such file or directory\n-- liquidclient.cpp:15:28: kwin/workspace.h: No such file or directory\n-- liquidclient.cpp:16:26: kwin/options.h: No such file or directory\n\nany idea the cause of error and how to overcome it?"
    author: "jamal"
  - subject: "Re: mosfet is there"
    date: 2001-10-10
    body: "I had this problem in debian, I just had to install kdebase-dev - the package is not likely to be called that for mandrake, but you're just missing the devel package which includes the KWin stuff."
    author: "Nero"
  - subject: "Re: mosfet is there"
    date: 2001-10-10
    body: "It's kdebase-devel,IIRC ;-)"
    author: "A Sad Person"
  - subject: "Re: mosfet is there"
    date: 2001-10-10
    body: "omfg, that is mosfet,  i recognize the ;-) anywhere!#$!@#\n\ncome back to kde-cvs d00d^!@#&^&\n\nliquid is da shiznit"
    author: "asasd"
  - subject: "Re: mosfet is there...not"
    date: 2001-10-15
    body: "not... download and compiled his liquid engine last week (had to manually move a lot files about to get it to work) and checked his site just now for an update.... It claims he is no longer doing anything with linux....."
    author: "Martijn Dekkers"
  - subject: "Re: mosfet is there"
    date: 2001-10-21
    body: "when i pop up a menu sometimes i see a picture of mosfet.  don't include this in kde!"
    author: "liquid is lame"
  - subject: "Re: mosfet is there"
    date: 2001-10-21
    body: "Nowhere in the code is there a picture of mosfet."
    author: "Liar liar pants on fire"
  - subject: "Re: mosfet is there"
    date: 2003-02-28
    body: "Hi, I have installed the theme, but I don't get 'Liquid' in the Style category of 'Appearance & Themes'. I get all the others (Colors, Window Decoration...) but not the style. What I really want are the transparent menus. Anyone know why this happens?? Thanks.\nDerek."
    author: "Derek"
  - subject: "No Good Theme Tutorials :("
    date: 2001-10-10
    body: "It seems that KDE 2.x is self-satisfied with few style themes. The style themes can be created in two ways:\n\n1. Programmed (fast, but programmers can create only)\n2. Pixmapped (a bit slow but non programmer can create it)\n\nThough Rik or Rikkus has Program/coded tutorial, but we are Lacking Pixmapped tutorial much like Mosfet's, which is still a bit unclear on many things. I had to discover by wasting many hours how to create an icon theme, and still and I am not sure.\n\nSimply, putting it:\n\nUsers can create themes/styles if proper documentation is provided, but those who knows doesn't wish to share like Mosfet!? Please somebody create somewhat quick-tutorial for:\n\n1. Themes (of theme manager)\n2. Pixmap Style\n3. Sound\n4. Icon\n\nso that users can contribute to our beautiful KDE. I would have created one if I knew about it (themes know-how).\n\nBy the way there is a very nice theme 'QNix' which is superbly style coded for KDE.\n\nAnyway a long awaited site is here. Many thanks to the folks who created such intuitive and nice KDE themes site."
    author: "Asif Ali Rizwaan"
  - subject: "Re: No Good Theme Tutorials :("
    date: 2001-10-10
    body: "> Please somebody create somewhat quick-tutorial for:\n> 4. Icon\n\n ftp://ftp.derkarl.org/pub/incoming/icnthm-doc.tar.gz\n\n might save your day :-)\n\nGreetings,\nTackat"
    author: "Tackat"
  - subject: "thanks ...."
    date: 2001-10-10
    body: "Wow, site UI rocks. \nstyle and themes already there rocks.\n\nim so happy.\nthanks thanks thanks.\n\ni like to change my UI when i get bored. its important to me and Xwindow system give us that chance. but i have no talent to create look from my own, ....\n\nplease artist all over the world contribute to KDE.\n\ni think its time to show off what KDE can (be/do) to few remaining skeptics.\n\nthanks again, see ya."
    author: "somekool"
  - subject: "Woohoo!"
    date: 2001-10-10
    body: "My problem with themes.org was always the lack of good quality KDE themes, but this new site is great! Now I'm sticking with KDE!"
    author: "Janus Shelley"
  - subject: "Great work!  (any *Step themes?)"
    date: 2001-10-10
    body: "Great work... very easy to navigate!  \n\nOne question - I'm still pretty new to KDE (after using Gnome for about 2 years) and I'm wondering if anyone has done (or is planning) any *Step-like title bar/window frame kinda like this:\n\nhttp://gtk.classic.themes.org/php/pic.phtml?src=themes/gtk/shots/966548635.jpg\n\nor this\n\nhttp://gtk.classic.themes.org/php/pic.phtml?src=themes/gtk/shots/935252517.jpg\n\nThis simple titlebar look is still my alltime favourite. Thanks!"
    author: "PRR"
  - subject: "Re: Great work!  (any *Step themes?)"
    date: 2001-10-10
    body: "well, there is a KStep window decoration and there is a STep-kde style and theres a Pale Grey color scheme that can make your desktop VERY next-step'ish heres a screenshot:   http://tokyoeye.rgv.net/kdestep.png"
    author: "diamondc"
  - subject: "Thanks!"
    date: 2001-10-10
    body: "Thank you!   Where can I get these 3 window-decoration/schemes now until someone uploads them to KDE-Look?  \n\n\nOne more question for anyone,  are there any Winamp/XMMS style skins for Noatun out there somewhere?"
    author: "PRR"
  - subject: "Re: Thanks!"
    date: 2001-10-10
    body: "i believe the step themes should come by default with the kdebase package.\n\nthere is a winamp skin plugin for noatun listed on the noatun homepage but the link wouldn't work for me last week."
    author: "diamondc"
  - subject: "Re: Thanks!"
    date: 2001-10-10
    body: "NextStep decos:\n\nWell, kstep is already included in kde 2.2, afaik. Also, the next major version of WindowMaker (due VERY soon)'s major new feature is KDE2 comptability :-). \n\nNoatun winamp skins:\n\nYes, it might be included in KDE 3, the noatun plugin is called winskin, and it's in CVS"
    author: "asasd"
  - subject: "QNX theme, check it out."
    date: 2001-10-10
    body: "Don't miss the new qnx theme! It's COOL. Better than liquid (less flashy, better usability)\nComplete with kwin theme, color scheme and so on. This is a real theme, not a pixmap theme!"
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: QNX theme, check it out."
    date: 2001-10-10
    body: "Yeah I agree, I use it too now and it rocks!\n\nNow all i want is a cool icon theme to fit nicely with it =)\n\nBtw, I found that replacing the back and forward buttons from galeon (the mozilla-small theme or something) makes konqueror look alot nicer. these are the small (24x24) blue rounded icons I'm talking about.\n\nHowever, the icon with the arrow pointing up is a little hard to clone (i rotated it 90 degrees left, but it's ugly... the button's gradient isn't right).\n\nCheck the screenshot to see what I mean."
    author: "Richard Stellingwerff"
  - subject: "What about ikon?"
    date: 2001-10-10
    body: "What about the icon theme ikon?\nI think it's nice."
    author: "Peter Simonsson"
  - subject: "Re: QNX theme, check it out."
    date: 2001-10-10
    body: "My guess about your icon :\n- Rotate the left arrow 90\u00b0 clock-wise\n- Mirror it\n\nShould be good !"
    author: "Aur\u00e9lien"
  - subject: "Re: QNX theme, check it out."
    date: 2001-10-10
    body: "Hey,\nThis is really nice.\nI don't know much about qnx, but are the icons exactly the same?\nAny license problems with that?\nI think KDE would come with a different default theme.\nMaybe this one could be the one.\nI really love the antialiasing thing.\nI hope that XFree guys finush with the Sis 630 driver for Xfree 4 soon!"
    author: "lanbo"
  - subject: "Re: QNX theme, check it out."
    date: 2001-10-10
    body: "at home i run a sis 630.. its an onboard video chipset. runs just fine 16-bit color 1280x1024 on my monitor.  this is what lspci says\n\n01:00.0 VGA compatible controller: Silicon Integrated Systems [SiS] SiS630 GUI Accelerator+3D (rev 31)\n\nim using xfree 4.1.0 from debian/unstable\n\n\nshould work =)"
    author: "diamondc"
  - subject: "Re: QNX theme, check it out."
    date: 2001-10-10
    body: "I haven't managed to make it work without the framebuffer.\nIt works but, you know: No QUAKE and no RENDERER extension.\nThat sucks.\nI think it's the combination of sis 630 + TFT what doesn't work.\nAny one has managed to make it work with hardware acceleration?"
    author: "lanbo"
  - subject: "Re: QNX theme, check it out."
    date: 2001-10-10
    body: "Get X 4.1.0.\n\nOn 4.0.3 RENDER didn't work on the 630, but on 4.1.0 it does.\n\nOTOH, the 630 is incredibly slow garbage."
    author: "Roberto Alsina"
  - subject: "Re: QNX theme, check it out."
    date: 2001-10-10
    body: "I'm gonna try that.\nAny special things for the X configuration file, or do the Xconfiguration, and similar things automatically create one?\n\nHey, thanks for your help!\nGracias y ala pa'rriba Roberto! (espanyol o que?)"
    author: "lanbo"
  - subject: "Re: QNX theme, check it out."
    date: 2002-02-03
    body: "I can't stress this enough....\n\nIf you have a 630 chip check out thomas' page at http://www.webit.com/tw\n\nHe has developed (and is developing) DRI for the 630.  Yes you can play Quake, Half Life etc. etc."
    author: "trip_out"
  - subject: "Re: QNX theme, check it out."
    date: 2001-10-11
    body: "The icons shown in the screenshots might be the QNX icons, I don't know, but they don't come with the theme (as far as I can tell) so it doesn't matter."
    author: "not me"
  - subject: "Re: QNX theme, check it out."
    date: 2001-10-11
    body: "Sorry, the icons are not from QNX... I use the 'iKons' icon-theme, and changed only the back, forward, and up icons. All the others are still from the 'iKons' theme."
    author: "Richard Stellingwerff"
  - subject: "integrating gtk apps in a kde desktop ?"
    date: 2001-10-10
    body: "Hi,\n\nJust wondering if someone did a gtk theme that looks _exactly_ like a kde theme (or other way round), so that you could for example see no difference between xchat and kde menus etc. Is that possible ?"
    author: "frido"
  - subject: "Re: integrating gtk apps in a kde desktop ?"
    date: 2001-10-11
    body: "(Warning, this is idle speculation, I have no intention of doing this myself - last time I started, I got a third of the way through a Windows theme importer when I found that there was one in the new version of KDE).\n\n    What might be better is to render out a KDE window, and then \"cut it up\" into pixmaps, and thus autogenerate a pixmap GTK theme that always matches the current KDE theme.  Good idea, ne?\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: integrating gtk apps in a kde desktop ?"
    date: 2001-10-11
    body: "Even more idle speculation:  A GTK theme engine that calls QT to do the drawing?"
    author: "not me"
  - subject: "SVG based themes and icons are a MUST have"
    date: 2001-10-11
    body: "SVG support in KDE is crucial ImageMagick and xml2 are there ... give us smoothly scaleable, gradient and *true* transparency capable icons and UI elements based on SVG, and whacky smooth and scalable antialiased fonts from 8-96point. Let us be as beautiful as Gnome (jimmac.musichall.cz/screenshots) and more .... let us make Apple tremble in it's Aqua boots!!"
    author: "SVG wanter"
  - subject: "Re: SVG based themes and icons are a MUST have"
    date: 2001-10-11
    body: "Might I point out that KDE has \"smooth and scalable antialiased fonts\" right now, while GNOME does not.  Also, I do not see the advantage in SVG icons, and especially not SVG UI elements.\n\nSVG icons would be scaleable.  That is the only good property they would have.  They would be slow and would limit the artistic possibilities in icons (hard to have photorealistic SVG - it would constrain artists to only one style, cartoonish).  SVG UI elements would be -really- slow, and since UI elements are already scaleable to the extent they need to be, there is no reason to make them SVG.  Plus, when you scale a hypothetical SVG UI element or icon, every part of it is scaled equally - _including_ the outlines, which is almost never what you want.  Differing sizes of black outline around icons and UI elements would give an odd, non-integrated look to different components and icons."
    author: "not me"
  - subject: "Re: SVG based themes and icons are a MUST have"
    date: 2001-10-11
    body: "sub-pixel rendered fonts too!  I just LOVE KDE's (I mean Qt's, er, Xft's.., er, freetype's) font renderer!  Just perfect on my Toshiba laptop.\n\nI no longer use Windows on my machine at all, and therefore don't have good anti-aliased fonts anymore.  Anyone have a good idea where I can get \"core web\" fonts?  (Arial, Helvetica, and so on...) I know that it's downloadable from the web, but it's an .exe, and Mandrake 8.1 doesn't seem to have wine set up properly (crashes)..  Attempts to decompress the .exe with various sfx extracting tools have failed...  Any ideas?  I did find a repository of many Mac TTF fonts, but although I got the sit files uncompressed, I don't know what to do with the result...\n\nErik"
    author: "Erik"
  - subject: "Re: SVG based themes and icons are a MUST have"
    date: 2001-10-11
    body: ": Anyone have a good idea where I can get \"core web\" fonts? (Arial, Helvetica, and so on...) \n\n    Do a search for fetchmsttfonts - I know it's installed on SuSE distros.\n\n: I know that it's downloadable from the web, but it's an .exe,\n\n    Use cabextract: http://www.kyz.uklinux.net/cabextract.php3\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: SVG based themes and icons are a MUST have"
    date: 2001-10-11
    body: "I read somewhere (maybe Mandrake Forum) that the .exe file's just a self-extracting zip file, so you should be able to just uncompress it with unzip"
    author: "Alex CG"
  - subject: "Re: SVG based themes and icons are a MUST have"
    date: 2001-10-11
    body: "SVG is the futur of icon !\n\nit's sooooo cool in Mac OS X"
    author: "J"
  - subject: "Re: SVG based themes and icons are a MUST have"
    date: 2001-10-11
    body: "Mac OS X doesn't use SVG for icons at all. Basically Apple is using an iconloader which works quite similar to KDE's. The icons in Mac OS X are pixmaps which are optimized for certain sizes (128x128, 64x64, 32x32, 16x16) and which are being scaled as needed.\nThe difference between KDE and Mac OS X though is that KDE's look and feel is rather symbolic/cartoonish while Apple tries to make Mac OS X more cool.\nUsabilitywise the symbolic/cartoonish aproach is better though.\n\nGreetings,\nTackat"
    author: "Tackat"
  - subject: "Re: SVG based themes and icons are a MUST have"
    date: 2001-10-11
    body: "but ... but in the task bar of mac os X ... the icons are resized dynamicly ... there can\"t be pixmaps ! ... no ?\n\nit's \"vectoriel\" ?"
    author: "J"
  - subject: "Re: SVG based themes and icons are a MUST have"
    date: 2001-10-11
    body: "no... you can dynamically resize pixmaps, but they get 'blocky'."
    author: "Jon"
  - subject: "Re: SVG based themes and icons are a MUST have"
    date: 2001-10-11
    body: "They only get blocky if the scaled size is larger than the size of the existing icons. If there's only a 32x32-Pixel-icon available for a certain application in MacOS X then the icon will appear blocky as well.\n\nMost icons in Mac OS are available in a 128x128-Version though. Therefore most dynamically scaled icons look fine.\n\nGreetings,\nTackat"
    author: "Tackat"
  - subject: "Re: SVG based themes and icons are a MUST have"
    date: 2001-10-12
    body: "The icons of mac os x are so beautiful in http://xicons.macnn.com\n... i 'de like to have icons as beatiful as these on kde :-)"
    author: "J"
  - subject: "Re: SVG based themes and icons are a MUST have"
    date: 2002-06-22
    body: "SVG icons are no good. For small images, pixmaps are much faster. (and may be even smaller too..)\nAnyway svg DOES NOT limit you to a cartoonish style. There are all sorts of filter effects available (and you can create your own, combining some basic modules), witch enables you to make photo-realistic icons.\n\nSVG is revolutionary, BUT it has no graphics state, wich is no good, especially when it comes to animation..\n\nBTW os X ruuuuules\n\n\n"
    author: "jack"
  - subject: "Re: SVG based themes and icons are a MUST have"
    date: 2004-06-19
    body: "I hate to break it to you guys, but the icons in OS X aren't pixmaps.  They are PDF's!!!!  You'll notice that you can actually make an icon fill an entire 23inch cinema display with zero degradation.  Thats why. :*)"
    author: "Steve Jesus"
  - subject: "Re: SVG based themes and icons are a MUST have"
    date: 2006-08-14
    body: "pdf, that is svg :)"
    author: "scoutme"
  - subject: "Re: SVG based themes and icons are a MUST have"
    date: 2001-10-11
    body: "I don't know wtf there is so much bitching about SVG icons. Although I think that SVG support is nice, I do not think that the major \n\nThe transparency aspect of SVGs is a moot point because with xrender and Qt3, we should be able to make any GUI element *truly* transparent/transclusent. SVG transparency is not only fake transparency, but also slow (currently software based in both Qt, KDE, and GNOME implementations).\n\nNot only that, but like GIF, SVG is a __patented__ technology. This time, by apple."
    author: "asasd"
  - subject: "Re: SVG based themes and icons are a MUST have"
    date: 2001-10-11
    body: "I don't think it is patented by Apple.  Go to the www.w3c.org page and see right there on the home page... SVG.  SVG is a recommended standard and not proprietary."
    author: "Eric Thomas"
  - subject: "Re: SVG based themes and icons are a MUST have"
    date: 2001-10-11
    body: "Those statements are not contradictory.\n\nhttp://lists.w3.org/Archives/Public/www-patentpolicy-comment/2001Oct/0555.html"
    author: "Roberto Alsina"
  - subject: "Many thanks"
    date: 2001-10-11
    body: "Thanks to all the developers who have put in the effort and sacrifice to set it up.  I have been waiting so long for themes.org to come back up that i have given up on it.  Now at least we have our very own dedicated site :)"
    author: "Anthony Enrione"
  - subject: "For mac icons"
    date: 2001-10-13
    body: "Someone know how use this : \nhttp://www.cs.msstate.edu/~smithg/ResCafe/\n\ni can't (but i am not a developper)"
    author: "J"
---
Are you looking for <a href="http://www.kde-look.org/index.php?xcontentmode=iconsall">icons</a>, <a href="http://www.kde-look.org/index.php?xcontentmode=wallall">backgrounds</a>, <a href="http://www.kde-look.org/index.php?xcontentmode=colorall">color schemes</a>, <a href="http://www.kde-look.org/index.php?xcontentmode=windowdecoall">window decorations</a>, <a href="http://www.kde-look.org/index.php?xcontentmode=soundsall">system sounds</a>, or <a href="http://www.kde-look.org/index.php?xcontentmode=themesall">entire themes</a>?  <a href="http://www.kde-look.org/">KDE-Look.org</a> ("EyeCandy for your KDE Desktop") provides <a href="http://www.kde-look.org/index.php?xcontentmode=all">exactly that</a>.  While relatively low on quantity, quality is very high.  The layout and interactive nature of this site indicates that it may very well wind up being a central resource for the kind of user who likes to change his colors, icons and look every hour.  It also has sections for skins for <a href="http://www.kde-look.org/index.php?xcontentmode=noatunall">Noatun</a> and <a href="http://www.kde-look.org/index.php?xcontentmode=other">other KDE apps</a>.
<!--break-->
