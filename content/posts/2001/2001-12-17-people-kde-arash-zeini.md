---
title: "People of KDE: Arash Zeini"
date:    2001-12-17
authors:
  - "Inorog"
slug:    people-kde-arash-zeini
comments:
  - subject: "Tea!"
    date: 2001-12-17
    body: "Yes, real programmers drink tea!\n\nUwe"
    author: "Uwe Thiem"
  - subject: "Re: Tea!"
    date: 2001-12-17
    body: "I (sip) agree.  Of course, as I live in the American South, I like mine iced.  Besides... then you can drink it faster.  :)  I've noticed that quite a few KDE developers and regulars who follow the KDE project drink tea.  Now if we want to start a flame war that makes me look like a bigger troll than Craig, what *kind* of tea is the best?  I like to brew a pitcher (because I like it iced) of four black tea bags and a single Earl Grey.\n\n(BTW, Craig - just teasing you, I know you advocate KDE in your own way)\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Tea!"
    date: 2001-12-17
    body: "I like coffee. Everyone knows programmers need caffeine, and lots of it. Of course, I come from the American Northwest, where Starbucks started ;)"
    author: "yu"
  - subject: "Re: Tea!"
    date: 2001-12-17
    body: "Black tea contains about the same level of caffein as coffee does. Mate and green tea contain about 10 times as much caffein as coffee does.\n\nUwe"
    author: "Uwe Thiem"
  - subject: "No"
    date: 2001-12-17
    body: ">Black tea contains about the same level of caffein as coffee does. Mate \n>and green tea contain about 10 times as much caffein as coffee does.\n\nNo, green and black tea contain comparable amounts of caffeine and both have much less than coffee. I don't know about mate.\n\nSee:\nhttp://www.cspinet.org/new/cafchart.htm\n\nI'm also in the tea drinker category -- hot in the morning and evening, iced with meals. I usually just throw four tea bags and maybe some mint leaves in a pitcher of water and leave it in the refrigerator. Brewing is too much work."
    author: "Otter"
  - subject: "Re: No"
    date: 2001-12-17
    body: "Actually, the amount of caffeine in tea is more than the amount of caffeine in coffee. However, that is comparing amounts of tea and coffee in their dried forms. When making a cup of tea or coffee, a lot more caffeine is extracted from the coffee than the tea leves, so the above results are correct as well. So you are both right!"
    author: "ac"
  - subject: "Re: More Caffine in Tea than Coffee"
    date: 2005-12-02
    body: "I suggest you do some more research, or get better sources.\n\nTea, generally has 33% of the caffine in coffee.\n\nGreen Tea has as little as 1/9th the caffine in a brewed, same-size cup, of coffee.\n\nAnd, if you expose the tea to boiling water (i.e. immerse the tea...) for about 30 seconds, then pour off the water, you have naturally decaffinated the tea leaves, getting rid of over 90% of the caffine in the tea.\n\nI work for a tea import/export company, and head their marketing / research department, so I am confident in the above information.  It is summarized from about 550 different research labs / institutes around the world...\n\nThought you'd like to know the score on the caffine issue...\n\nMax"
    author: "max"
  - subject: "Re: More Caffine in Tea than Coffee"
    date: 2006-08-11
    body: "Does REGULAR  tea have 33% more caffine or\ndoes it have 33% less caffine, Which does it have?"
    author: "Francis Zarnowski"
  - subject: "Re: More Caffine in Tea than Coffee"
    date: 2008-02-11
    body: "is tea more caffinated then coffee?\n"
    author: "cloyce grubbs"
  - subject: "Re: More Caffine in Tea than Coffee"
    date: 2007-12-10
    body: "Thanks for having this web site set up.\n\nDon"
    author: "Don Black"
  - subject: "Re: Tea!"
    date: 2002-11-02
    body: "Your comments re caffein content of green tea (10 x coffee) don't agree with my own research on the web.  It is my understanding that sencha (a Japanese variety) contains about 25% of the caffein in regular brewed coffee.  Genmaicha (a Japanese variety with brown rice) has about 5% re brewed coffee.  See www.japanesegreenteaconline.com.  Of course, this is a tea seelling site.  Perhaps you are in the coffee business?"
    author: "Robert Leslie"
  - subject: "Re: Tea!"
    date: 2004-01-20
    body: "mate does not have caffeine"
    author: "dude"
  - subject: "Re: Tea!"
    date: 2005-12-08
    body: "The source from where you got that information about green tea is totally off.  Just the opposite, green tea has 10 times less (about 1/10th) caffeine than coffee.  "
    author: "Alex"
  - subject: "Re: Tea!"
    date: 2006-06-17
    body: "The one bad thing about internet discussion: too many people pass off information as fact without verifying it.  Shame on you, Uwe."
    author: "Justin"
  - subject: "Re: Tea!"
    date: 2006-11-15
    body: "This is a very old message - I'm writing this in 2006 - but according to http://www.stashtea.com/caffeine.htm - green tea has only 1/4 the amount of caffine that is found in coffee.  This is an excellent chart for comparing caffine content in coffee, colas and various teas."
    author: "Susan Flewelling"
  - subject: "Re: Tea!"
    date: 2007-02-28
    body: "Caffeine Content Comparisons \nThe following is the\napproximate caffeine content\nof various beverages Milligrams of Caffeine \nItem     Average per serving       Range     Per ounce* \nCoffee    (5 oz. cup)   80        40 - 170     16.00  \nCola      (12 oz. can)  45        30 - 60       3.75  \nBlack Tea (one tea bag) 40        25 - 110      5.00  \nOolong Tea (one tea bag)30        12 - 55       3.75  \nGreen Tea (one tea bag) 20         8 - 30       2.50  \nWhite Tea (one tea bag) 15         6 - 25       2.00  \nDecaf Tea (one tea bag)  2         1 - 4        0.50  \nHerbal Tea (one tea bag) 0             0        0.00  \n*Assumes 8 ounces of water per tea bag \n"
    author: "Linda"
  - subject: "Re: Tea!"
    date: 2001-12-17
    body: "After lunch, if I need caffeine I go with Twinings Gunpowder Green Tea (loose). I like the \"attractive clear fragrant liquor produced by this fine shotty Gunpowder Tea from the Spring crop.\" \n\nI don't know how much caffeine is in there but I think it is probably a helluvalot."
    author: "pos"
  - subject: "Re: Tea!"
    date: 2008-03-28
    body: "wats the caffeine content per teabag of twinings greentea."
    author: "adetutume"
  - subject: "Re: Tea!"
    date: 2001-12-17
    body: "I *need* diet coke,it's like a drug.Aspartam forever!"
    author: "Anonymus"
  - subject: "Re: Tea!"
    date: 2001-12-17
    body: "You should drink Italian espresso - that's the real coffee ;-)"
    author: "Federico Cozzi"
  - subject: "Re: Tea!"
    date: 2004-12-31
    body: "according to http://food.oregonstate.edu/misc/caffein.html , an article from Oregon State with actual data to back it up, tea has roughly 40-60% of the caffeine of typical brewed coffee."
    author: "flunky"
  - subject: "Re: Tea!"
    date: 2006-07-18
    body: "Green tea has nowhere near the same caffeine level as does Coffee!"
    author: "Alan"
  - subject: "Re: Tea!"
    date: 2006-07-31
    body: "UWE - stop posting supposed facts with so much seeming confidence when in fact you're COMPLETELY off the mark.  Green and white tea have far less caffeine than black tea, which still has less caffeine than coffee. \n\n\"Black tea contains an average of 60 mg caffeine per cup, while green tea contains 36 mg per cup. All teas contains less caffeine than an 8 oz. cup of coffee, which has about 95 mg.\"\nhttp://www.med.umich.edu/umim/clinical/pyramid/tea.htm\n "
    author: "Dara"
  - subject: "Re: Tea!"
    date: 2006-08-14
    body: "The only negative side effect reported from drinking green tea is insomnia due to the fact that it contains caffeine.   However, green tea contains less caffeine than coffee: there are approximately thirty to sixty mg. of caffeine in six - eight ounces of tea, compared to over one-hundred mg. in eight ounces of coffee. Uwe needs to do a little research as his/her comments are full of taurus excreetum."
    author: "Paul"
  - subject: "Re: Tea!"
    date: 2006-09-12
    body: "hmmm.... there's me trying to find some info. on caffeine and i came across this totally un-useful forum. I did find this though: \n\nhttp://www.stashtea.com/caffeine.htm#COMPARE\n\nseemed to answer the questions\n\n - caffeine 80 mgs\n - Black tea 40 mgs\n - Green tea 20mgs\n - herbal tea 0 mgs\n\nnow removing the fact this could still be made up, i'm going to suggest going herbal if worried about caffeine!\n"
    author: "Mr Slim"
  - subject: "Re: Tea!"
    date: 2001-12-17
    body: "Well, I live in Africa. In Namibia to be precise. So the temperatures we have to cope with are similar, though the moisture is not. I like my tea hot, regardless what the thermometer says. ;-)\n\nAs for the kind of tea: It must be high-grown black tea (at least over 2000m) without any additional flavouring. Darjeeling is a good choice. UVA Highlands as well. There used to be very good tea from Kenya but I can't get it around here. Either they export their whole harvest to Europe or the tea gardens in Kenya have gone down the drain like most other things over there. They have got good stuff in Iran as well.\n\n... and of course, good tea doesn't come in teabags!\n\nUwe"
    author: "Uwe Thiem"
  - subject: "Re: Tea!"
    date: 2001-12-17
    body: "Assam for me.\nHot (with a dash of milk though)."
    author: "Corba the Geek"
  - subject: "Re: Tea!"
    date: 2001-12-18
    body: "Where do you live evan i'm in west virgina. I've been drinking lots of coffee with a dash of nut meg in the grounds and half and half cream. I've working outside lately and doing night shift and hot coffee gets me through it.\n\nCraig ie the troll : )"
    author: "Craig"
  - subject: "Re: Tea!"
    date: 2001-12-18
    body: "> I've been drinking lots of coffee with a dash of nut meg in the grounds and half and half cream.\n\nmm, sounds nice\n\n> Craig ie the troll :)\n\nwe agree with something at last! down with tea! :-)"
    author: "zzz"
  - subject: "Re: Tea!"
    date: 2003-11-11
    body: "I've been drinking coffee for a few years and it seems that my throat has become rather sensitive towards it (I think it might be the acidity)Most of the tea's I've tried don't seem to have the same effect as coffee does. Energy drinks seem to do the trick but they are also very acidic. Does anybody have any recomendations?"
    author: "Alex"
  - subject: "Re: Tea!"
    date: 2001-12-24
    body: "I agree.  Hot tea is great, especially the type of hot tea that people drink in Iran.  They use loose tea, not that tea bag crap.  My favorite persian tea is a brand called \"Top\" and in comes in a red bag.  I also like Kenyan tea and actually had the priviledge of visiting Kenya many years ago.  Kenyans put milk in their tea, which is not my \"cup of tea\" (excuse the pun), but it tastes great straight.  Chinese tea, specifically Emperors tea from Hongzhou is good if you like that sort of thing.  Ceylon, Kalimi, Black, oolong, and regular green tea are also good.  People say I'm a bit fanatic when it comes to tea, but if it's in a tea bag it usually doesn't cut the mustard.  Lipton isn't even on the list, but I guess its fine for iced tea.\n\nAs far as coffee goes, Espresso is fantastic after a big greasy meal.  Turkish coffee, never boiled and made inside of an Ibruk, is a good thick concoction and really hits the spot when I'm in a more exotic mood.  Iced coffee, is nice on a hot day.  Specifically, a Vietnamese iced coffee called \"Cafe Sua Da\", which is made from Cafe du Monde and mixed with sweetened condensed milk, is a favorite of mine.  Occasional capuccinos and lattes are not bad if I'm in the mood for them.  I rarely drink regular coffee unless I'm in a place like Denny's which isn't very often.  Overall, espresso is my coffe of choice.  \n\nFinally, Arash Zeini, if you are reading this, I raise my tea glass in your honor.  Maybe one day when our governments stop bickering, we will be able to talk in person about being part of something that unified people instead of dividing them.  Thanks for your contribution to KDE and keep drinking tea!"
    author: "erotus"
  - subject: "Re: Tea!"
    date: 2002-07-02
    body: "I'm drinking tea right now, Earl Grey baby, thats what real programmers drink...\n\nOh yeah, does anyone know how to get the max amount of caffeine in their tea without wasting 17 bags in one cup?"
    author: "Adam"
  - subject: "Re: Tea!"
    date: 2005-10-27
    body: "um, i'm no expert, but i heard a rumor that you actually get the most caffine out of your tea by steeping it less!  not, of course, to the point where natural water has the highest caffine content, but no more than 3 minutes should put you in the nice and strong if what i heard is true.  i'm sure this absolutely could not apply to coffee!"
    author: "casual passerby"
  - subject: "International cooperation"
    date: 2001-12-17
    body: "> In these times of war, blindness and a lot of haterage this great \n> collaborative effort may stand for more than technology and software.\n\nAbsolutely! With all the craziness in the world (several of my friends were killed in the World Trade Center attack while my brother, who works down the street, ran for safety when the second plane hit and got away with just a bad scare; my friend's friend got shot in the West Bank last week; and that's just the people I know) it's made me realize how wonderful it is to be involved in this huge collaboration of  volunteers from all over the world. Arash's government and mine may not be on the best of terms but I'm glad we have one another's work on our computers."
    author: "Otter"
  - subject: "Commodore"
    date: 2001-12-17
    body: "\"~ What was your first computer?\nCommodore 16. I upgraded it to Commodore 64 the next day. \"\n\nThat's probably a good thing. :)"
    author: "Johnny Andersson"
  - subject: "Why win2k apps look better than KDE's [OT]"
    date: 2001-12-18
    body: "It's offtopic but less offtopic than Tea topic :).\n\nHere are my observations of 'Why KDE applications look uglier than Win2k's applications?' Please see 'Style.png'\n\n1. Menubar look consistent with Toolbar\n\n   In Win2k menubar looks exactly like the toolbar, but in KDE it looks sunken or elevated than toolbar and inconsistent and not aligned properly with many styles (except, hicolor default, kde default). The only consistent style we have in KDE is QTCDE with respect to menubar and toolbar consistency.\n\n2. Menubar behaves like toolbar\t\n\n   One can move toolbar in KDE but in Win2k one can even move the 'Menubar' since it (win2k apps) have Handle even for menubar. In KDE it is not possible to move menubar below the toolbar. Though one can't hide the menubar in win2k, but if KDE permits 'settings->show/hide menubar' just like 'setting->show/hide \ntoolbar'. This will permit all kde applicaton to hide menubar.\n\n3. Menubar doesn't show menu items when the space not available: \n\n   if you resize a kde window horizontally small then you can see the toolbar actions get placed in a '>>' menu at the end of toolbar. But in win2k the menubar also place the menus in the '>>' menu, whereas kde applications looks ugly since they can't adjust the menubar. Please see the menubar of 'style.png'.\n\n4. Menubar and toolbars has 4-6 pixel border: \n\n   In win2k you can see the whole window decoration has 6 pixel width window border around the application, and so also it has 4-6 pixel border around the menubar+toolbars, giving it a more neat look. In KDE menubar and toolbars do not have any border so they look a bit weird. When the window is maximized the window border is not visible in win2k giving more space. 'A don't show window border when maximized' option may be good for KDE window decoration.\n\n5. Animated Logos placement: \n\n   In win2k animated Logos are placed on the menubar.  KDE has only one animated konqueror's logo, which is placed on the toolbar. If you horizontally resize konqueror window to small size, you may not see the animated logo. If kde also places 'logo animation' on the menubar (as I also saw it on QNX RTP); then this kind of bugs won't appear.\n\n6. Window Decoration permits more space for resize: \n\n   In win2k you can see 3 diagonal lines /// which allows easy window resize option and also gives a nice look to the window decoration.\n\n7. Status Bar look: \n\n   Win2k has sections in the status bar which serves 2 purposes, one is it gives nice look to the status bar and second is that it allows multiple messages to be placed on the status bar. Kword show this kind of section of statusbar kword shows 'Page 1/1', which look nice than other applicatons' statusbar. Even the statusbar and sections on the statusbar of win2k has 4-6 pixel border/gap.\n\n8. Windows show separators: \n   \n   KDE's toolbars do not show separators, but win2k shows separators to denote logically different sets of toolbar actions."
    author: "Asif Ali Rizwaan"
  - subject: "Re: Why win2k apps look better than KDE's [OT]"
    date: 2001-12-18
    body: "http://www.koffice.org/kformula/pics/kformula2.png\n\nnow thats ugly.\n\ngimme gtk_thin-ice anyday."
    author: "joe99"
  - subject: "Re: Why win2k apps look better than KDE's [OT]"
    date: 2001-12-18
    body: "I posted regarding, simple things which make KDE apps more attractive than they are! Many a times I read that 'i want win2k look and feel but with Linux stability'. You may use 'QTCDE' style."
    author: "Asif Ali Rizwaan"
  - subject: "Re: Why win2k apps look better than KDE's [OT]"
    date: 2001-12-18
    body: "kde is inheritly ugly tho, kde developers dont really know how to make a design pretty *and* usable apps. Look at knetfilter for an example,\n\nhttp://expansa.sns.it:8080/knetfilter/screen2.jpg\n\nbad design, just a plain mess..and it shows on apps.kde.com, since 50% of the users voted against using it. (89 Yes, 75 No)\n\nlets look at the Gnome equiv..\n\nhttp://firestarter.sourceforge.net/shot1.png\n\ncleaner design. \n\neven the kde toolbars are ugly inside koffice apps (or all kde apps) i.e they're too large, too many levels, ..makes koffice looks really bad on lower resolutions. Even at KDE 1.x UI Apps are ugly, all whats happened since then is the release of KDE2 and QT2.2.. same bad UI design."
    author: "joe99"
  - subject: "Re: Why win2k apps look better than KDE's [OT]"
    date: 2001-12-18
    body: "not inherently ugly, but lower resolutions are not used by the developers hence we see bad looks. I reported that kde looks ugly at low resolution and i got reply 'use hi resolution!' :P. windows OTOH looks good even at 640x480 resolution; it seems that Gnome takes care of 800x600 resolution more than KDE."
    author: "Asif Ali Rizwaan"
  - subject: "Re: Why win2k apps look better than KDE's [OT]"
    date: 2001-12-18
    body: "not even higher resolutions can save kde apps from bad design and uglyness."
    author: "joe99"
  - subject: "Re: Why win2k apps look better than KDE's [OT]"
    date: 2001-12-19
    body: "Do you happen to have a point, other then \"Hah, I'm so kewl, I can complain\nambigously and unhelpfully about KDE!\"? If not, then please visit Slashdot.\n\nYes, I know these anti-troll messages don't help, but it lets me work the frustruation\nout of my system. Seriously, do you guys have nothing better to do with your time\nthen hang out on message boards and attempt (rather sucessfully) to annoy\neveryone else there?"
    author: "Carbon"
  - subject: "Re: Why win2k apps look better than KDE's [OT]"
    date: 2001-12-18
    body: "You're a big fan of Win 3.1 aren't you ;)"
    author: "anon"
  - subject: "Re: Why win2k apps look better than KDE's [OT]"
    date: 2001-12-23
    body: "I agree with knetfilter being a mess, so just pick a better firewall configuration tool. Get Guarddog from http://www.simonzone.com/software/guarddog/"
    author: "Carsten Pfeiffer"
  - subject: "Re: Why win2k apps look better than KDE's [OT]"
    date: 2001-12-18
    body: "Personally, i think the standard GUI if KDE is much prettier.\nI'm using a 1024 x 800 res. ofcourse, but still... \nAdd Mosfet's theme, and KDE is amazingly great!\nIt's all a matter of taste, if you don't like it, use your windows\nsystem, or gnome or whatever... I love it :)\n\nSee ya!"
    author: "Verwilst"
  - subject: "Re: Why win2k apps look better than KDE's [OT]"
    date: 2001-12-18
    body: "Personally, i think the standard GUI if KDE is much prettier.\nI'm using a 1024 x 800 res. ofcourse, but still... \nAdd Mosfet's theme, and KDE is amazingly great!\nIt's all a matter of taste, if you don't like it, use your windows\nsystem, or gnome or whatever... I love it :)\n\nLook at the screenshots on http://home.cluesoft.be/verwilst...\nI made those for the windows-using friends, who were wondering what Linux\nreally has to offer as a desktop :o) I don't think you can say the GUI sucks?\n\nJust my 0.2 Euro\n\nSee ya!"
    author: "Verwilst"
  - subject: "Re: Why win2k apps look better than KDE's [OT]"
    date: 2001-12-19
    body: "You know, these little \"Hi, here's a list of what I think KDE should be doing, better\nget on it!\" posts are getting very common. Quick, someone pick a hackish nickname\nfor it so we can make fun of them all over IRC! Mwahahaah!\n\nNo, seriously, this list makes very little sense. In a type of post that's also gotten\ncommon, I'll respond to each point in turn.\n\n1. A menubar is not a toolbar. Why should it look the same?\n2. I can't think of a single reason why anyone would want to move their menubar.\nHowever, that hiding thing sounds nifty. In proxy for Njaard, I'll say\n \"Send a patch!\"\n3. Uh, on my box, the menubar becomes larger horizontally. This seems to make\nmore sense to me, since it shows all the menus onscreen at once.\n4. Showing the border when maximized is a Good Thing (tm). This allows us to easily\nmake a window that is not-quite screen filling, which is great when typing and\nwatching the bottom line of an IRC window, or many other uses.\n5. Menubars are not toolbars. In fact, this exact problem (placing non-menu thingies\nin a _menu_bar) is specfically addressed as a Bad Thing (tm) in the KDE UI Guide.\n6. Try changing the window decoration style to \"ModSystem\", that gives\nyou big honkin resize bars.\n7. \"Status bar gives nice look to the status bar\". Huh?\n8. Yes, yes they do. KDE toolbars do indeed have blank-space seperators.\nTry opening up the toolbar editor sometime."
    author: "Carbon"
  - subject: "Re: Why win2k apps look better than KDE's [OT]"
    date: 2001-12-20
    body: "I never liked the Windows look, and the menu bar is a non issue, actually it may be QT issue as it works that way in other non KDE QT bases apps like Opera and QCad. You can do a lot more to change the appearance of KDE to your liking, out of the box so to speak than you can can with Windows anyway.  As for me. I really prefer the classic Motif look and elect to use the QT Motif style on my KDE/QT apps and the GTK++ notif style on GTK/Gnome apps, it also blends well well with the default TK style, makes me happy. If only LyX would get around to losing that ugly Xforms toolkit.... I like the Motif look not because I'm an old Unix guy who grew up with, but just because it's so plain and simple, like flanell shirts, and broken in jeans, and the old Ford Crown Victoria:) As for the silly little Windows grab stripes in the corner, why bother, the window decoration style on KDE determines how much visible grab are you have for resizing, I opt for the Mwm style because it looks clean and Motif like and has chunky window borders to make resizing easy. To get my favourite look on W2K I've got to buy accessory shreware to enable me to do the customisation I want... It's all a matter of taste. I don't want my desktop to look like Windows if I can possibly help it."
    author: "E. M. Collins"
  - subject: "Re: Why win2k apps look better than KDE's [OT]"
    date: 2006-07-21
    body: "I think KDE 3.5 looks really better! It even has transparancy. !!!"
    author: "Jan"
  - subject: "Applications' window size"
    date: 2001-12-25
    body: "Hello, I am currently running Redhat, and KDE for this post.\nOn this same computer, in another partition, I have Mandrake 8.0, and KDE,\nand the problem is: The applications appear to large, some so big that I have\nto hold the ALT key down and move them around on the screen to get to the \"OK\"\nbutton. This must be some simple setting that I do not know about.\nOn the Redhat/KDE, everything is normal=sized.\nAlso, I have another machine with Mandrake 8.0 and KDE, and everything there\nis normal sized there too.\nOn all machines, the buttons on the Taskbar are the same size, as are the\nicons on the desktop. Just the application boxes are outsized. Examples are:\nKPPP, and Opera. Any help will be appreciated:\nreply to:\ninfo@rapidweather.com"
    author: "Rapidweather"
---
<a href="http://www.kde.org/people/arash.html">Arash Zeini</a> is from Iran and takes part in translating KDE into Farsi. In his dialog with <a href="mailto:tink@kde.org">Tink</a> for this Sunday's column of <a href="http://www.kde.org/people/people.html">People behind KDE</a>, Arash demonstrates that KDE helps lower borders, and facilitates friendships and collaboration instead. Here is a tribute to the diligent and efficacious work of the KDE translators.
<!--break-->
