---
title: "An Analysis of KDE Memory Usage"
date:    2001-09-20
authors:
  - "llunak"
slug:    analysis-kde-memory-usage
comments:
  - subject: "Non-GNU users"
    date: 2001-09-20
    body: "How will this affect us non-GNU users? We who don't use GNU ld, binutils, glibc and so on. Will anything be done to increase speed/decrease memory usage for non-GNU platforms? Will objprelink be available for Sparc?\n\nLots of questions :) Hope you don't mind if a question is a little bit stupid, I don't have that much technical knowledge."
    author: "Loranga"
  - subject: "Re: Non-GNU users"
    date: 2001-09-20
    body: ":: Will objprelink be available for Sparc?\n\nAssuming you're running Solaris (not a perfect assumption, but you don't specify), *if* gcc and KDE are optimized for each other, then you would probably preferentially use the gcc compiler and linker.  Unless you have a reason why not to (I haven't used Solaris heavily since 1995ish, and gcc wasn't really considered an option back then at the place I admined, so I don't know how well it performs on a Sparc).\n\n    Otherwise, this is (simplisticly) a C++ function pointer handling issue with the GNU linker - it may not even be an issue with the Sun supplied linker.  Or it may be an even worse issue. Or there (most likely) may be an entire different set of issues when it comes to optimizing.  Regardless, the code isn't being changed.  It's the simple fact that the KDE team is working so hard on this project that they are identifying performance bottlenecks in system components.  When that happens, you go to the person supplying the system component (in this case the gcc dev team), and present your case, preferably with a proposed solution.  That's been done.  If the Sun compiler/linker has performance bottlenecks as well, it will have to be profiled and presented to Sun.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Non-GNU users"
    date: 2001-09-21
    body: ">>It's the simple fact that the KDE team is working so hard on this project that they are identifying performance bottlenecks in system components. When that happens, you go to the person supplying the system component (in this case the gcc dev team), and present your case, preferably with a proposed solution. That's been done. If the Sun compiler/linker has performance bottlenecks as well, it will have to be profiled and presented to Sun.\n\n\nAre the GCC/GNU tools a part of KDE? \nOf course I understand that the KDE team want their software to perform as well as possible, but why just on GNU/GCC based platforms. Isn't non-GNU platforms important for the KDE team?\n\nMaybe my questions should not have been adressed here, since they don't directly has anything to do with Lubos Lunak's work. That maybe has created a confusing situation. I apologize for that."
    author: "Loranga"
  - subject: "Re: Non-GNU users"
    date: 2001-09-21
    body: "> Are the GCC/GNU tools a part of KDE? \n\nNo, they are separate projects.\n\n> Of course I understand that the KDE team want their\n> software to perform as well as possible, but why just\n> on GNU/GCC based platforms. Isn't non-GNU platforms\n> important for the KDE team?\n\nNow that's a loaded question.  That's like a parent who buys a shirt for a child and the other child says, don't you love me?\n\nRead the article.  SuSE hired him to make these improvements.  SuSE != KDE.  SuSE is a Linux distribution which uses Gcc/binutils.\n\nAnother obvious fact is that the vast majority of KDE users use gcc/binutils.  Not only because most users use Linux, but even on other platforms many users use these tools.  So obviously if you are going to improve a subsytem you would focus on the subsytem that helps the most people.\n\nNow, are these other platforms important to you?  If so, why don't *you* do something about it?  Open Source is not about criticizing people who do work because they don't do the work you prefer. \n\n> Maybe my questions should not have been adressed here, since\n> they don't directly has anything to do with Lubos Lunak's work.\n\nExactly.  Why don't you congratulate him and SuSE on their great work rather than complaining and accusing because they have not done more for free."
    author: "Sage"
  - subject: "Re: Non-GNU users"
    date: 2001-09-21
    body: "(BTW, The first question was rethorical...)\n\nMy questions was just questions, not demandings. Just plain questions.\n\n>Now that's a loaded question.\nI don't understand. My questions was not at all about jealousity, just curiosity!\n\nI'm not accusing anyone. The only one who accuses someone are you accusing me for accusing SuSE and Lubos Lunak. I know fully why Lubos Lunak's work was made (to improve the SuSE distribution), so SuSE can sell more distributions, and make more money. Or is SuSE a non-profit organization? Or maybe a charity organisation? I don't think so.\n\nWhy is it so hard to understand that I'm not accusing anyone? Do you believe the worst of everyone? Man, I just was curious if the KDE team has any thoughts about improving KDE for non-GNU platforms. And just because I was curious, it doesn't mean at all that I demand improvements, as described earlier.\n\nBut: To Lubos Lunak and SuSE: Thanks for improving the KDE desktop.\nHope your improvements results in more SuSE distributions sold, so you can continue your contributions to KDE. \n\nBest regards\nMats Eriksson, aka Loranga"
    author: "Loranga"
  - subject: "Re: Non-GNU users"
    date: 2001-09-21
    body: "The Solaris compiler most likely doesn't need objprelink.  The only reason objprelink helped for the GNU tools was because the GNU tools were badly designed in the first place.  One would hope Sun's Solaris tools are already performing prelinking or something equivelant.  And if not, there's not much KDE can do about it.  The KDE team can talk directly to the GCC team to fix the problems.  I doubt Sun would listen to the KDE team.\n\nYes, non-GNU platforms are important for the KDE team, but they aren't as important as GNU, for the simple fact that more people run KDE on GNU, and more of the KDE developers run GNU themselves."
    author: "not me"
  - subject: "Re: Non-GNU users"
    date: 2001-09-22
    body: ": Are the GCC/GNU tools a part of KDE? \n\nNo, nor are they required for KDE.  That's like saying \"I compiled KDE on Solaris, so 'Is the Sun C Compiler a part of KDE?'\"\n\nKDE is being developed in C++.  If you're loking for some of mythical, ideal parity in support, and gcc worked fine, and the Sun C Compiler had a serious problem, would you prefer that the KDE team not address that?\n\nIt's the same thing with X - various X servers have different issues. KDE is written to be very X independant, while still supporting some nice features (like XFree's multi display xinerama feature).  Would you prefer that KDE fail miserably on a multihead display just because the X server provided with AIX dosen't support xinerama extensions?\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Non-GNU users"
    date: 2001-09-22
    body: "Well, the first question was rethorical... :)\n\nThis is all about standards compliancy, and how/why you should follow standards or\nnot. That's an issue which has been discussed a lot, not just in software\ndevelopment. Why not take part of that on-going debate? I can recommend reading \"The\nDesign of Inquiring Systems\", 1971, chapter 9, by C.West Churchman.\n\nA question is: How can loosely coupled teams like the KDE team or the XFree86 team\nlead the development (and of course, the development of standards), compared when\nlarge companies led the development, either alone or in consortiums together with\nother companies?(1)\n\nThis is quite a new phenomenon in the free software world.\n\nExample: XFree86 implementation of the X11R6.4 standard.\nTaken from the x.org home page (http://www.x.org/current_honor.htm):\n\n\"The XFree86 Project, Inc is a non-profit corporation of the state of Texas, USA.\nThe primary charter is to design, implement, and distribute, as free software, an\nimplementation of the X Window System. The software implements the X Window System\nspecifications for many implementations of the UNIX \u00ae operating system. All products\nof The XFree86 Project, Inc, are freely available, and freely re-distributable.\nMajor supporters of XFree86 include Compaq Computer Corporation, Metro Link, Red Hat\nSoftware, S.u.S.E. GmbH, and many others.\n\"\n\nVoices are now heard among XFree86 users(2) that the X11 specification are old and outdated, and a X12 specification has to be made. What should XFree86 then do? Try to develop a X12 specification with the x.org consortium, or fork the standard and build an own\nstandard?\n\n\nThis discussion can be extended to the KDE project. If KDE wants to make Unix ready\nfor the desktop, then they have to be standards compliant, so it can be implemented on every Unix which also follow the\nrequiring standard(s)(3). If KDE just want to be a hobby project and/or a artefact only running on Linux and some other platforms,\noften required to install (non-standard?) GNU tools, then it is just to do what they want to do.\n\nThe most important thing is to understand why this is not a purely\ntechnical/technological issue, it is rather a philosophical issue!\n\n(1) Some companies are not interested in following standards at all, somebody says\nMS is one of them...\n(2) Just rumours. Tell me if these rumours can be wrong, and why.\n(3) I guess POSIX is one of these standards, but please, don't be naive and focus on a specific standard, think of\nstandards in a higher, more philosophical way.\n\nRegards\nMats Eriksson"
    author: "Loranga"
  - subject: "Re: Non-GNU users"
    date: 2001-09-22
    body: ": If KDE wants to make Unix ready for the desktop, \n: then they have to be standards compliant\n\n    KDE is written in C++. where is it not standards compliant?  Qt is written in C++, and the version that KDE generally links against is the X version, where is it not standards compliant?\n\n: If KDE just want to be a hobby project and/or a \n: artefact only running on Linux and some other \n: platforms, often required to install (non-standard?) \n: GNU tools, then it is just to do what they want to \n: do.\n\n    Where does KDE have any dependencies on GNU tools or Linux?  I've watched the developer list for long stretches of time (I'll admit I don't follow it right now), and I've often seen conversations along the lines of (simplified): \"That CVS commit that was just made breaks the build in AIX\" \"Okay... how about that?\" \"That fixed it\".\n\n    The core KDE developers watch for, and maintain Unix and X (as far as it goes on top of Qt) compatability.  Trolltech seems to do the same.  I really really fail to see where *any* of what you said is relevant.\n\n    The whole objprelink and gcc issue was this:  the KDE team found a bug[1] in the GNU linker.  They profiled and reported it.  End of story.  Of course KDE users using the GNU tools would be interested in this news - it means that their builds of KDE (and any C++ app similar to KDE) will have some nice optimizations.  There's absolutely no issue of \"standards compliancy\" in either a \"technical\" or \"philosophical\" viewpoint.\n\n: please, don't be naive and focus on a specific standard, \n: think of standards in a higher, more philosophical way.\n\n    What standards?  What the heck are you talking about?  I agree that this is an interesting question to raise; the concept of KDE's direction is always in the air everytime someone asks about porting KDE to Qt/Embedded or something along those lines.  But I fail to see how the objprelinker is related at all.\n\n[1] It was a bug report in the sense that it was doing very slow runtime linking at inital execution.  Probably not an issue for most programs... it was just more noticable in an environment where you're running lots of programs by clicking on an icon and waiting.  A bug in the \"wishlist\" category.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Non-GNU users"
    date: 2001-09-23
    body: ":KDE is written in C++. where is it not standards compliant? \nI haven't said that KDE is not standards compliant, but it has to be if it wants to \"make Unix ready for the desktop\".\n\n:What standards? What the heck are you talking about? I agree that this is an interesting question to raise; the concept of KDE's direction is always in the air everytime someone asks about porting KDE to Qt/Embedded or something along those lines. But I fail to see how the objprelinker is related at all.\n\nIt's not related to objprelinker at all. This discussion was about standards in a general, philosophical way, that is, how/why KDE (or other free software projects) should follow standards or not. I'm not talking about a specific issue here, neither a past one, a current one or a future one.\n\nI just wanted to extend the discussion above the everyday discussion. But I'm only accused of bashing KDE (or some KDE developers) for not doing a good job, which I think I haven't done. If someone thinks I have done so, I apologize.\n\nIf you (or someone else) wants to discuss how standardization work can/should be done, you can contact me through my e-mail adress. \n\nRegards\nMats Eriksson"
    author: "Loranga"
  - subject: "clarify"
    date: 2001-09-23
    body: "For me, this thread is closed. Just wanted to clarify that. But you can always e-mail me if you have anything to say."
    author: "Loranga"
  - subject: "Re: Non-GNU users"
    date: 2001-09-20
    body: "I'm not sure. Maybe you should ask <*BSD developers/Sun/whoever develops your system>.\nBTW, there's a difference between objprelink and prelink. Prelink is the tool developed by Jakub Jelinek which I used in the tests. For objprelink details, see the other dot.kde.org article (about 2-3 months old, it's called 'Faster startups' or similar)."
    author: "Lubos Lunak"
  - subject: "What os/kernel?"
    date: 2001-09-20
    body: "What kernel did you use to test test this? Linux 2.4.x is notorious for reporting wrong values for SHARED. Rik van Riel seems to be working on a patch which will solve this (and many, many other) problems but AFIAK it isn't merged yet."
    author: "Erik Hensema"
  - subject: "Re: What os/kernel?"
    date: 2001-09-20
    body: "Even more: 2.4 doesn't even calculate it, I think because it can't be calculated in a good working manner anymore with the new VM-architecture (not sure about this, though). I guess this has been tested by implementing some method in kde_init to get the right values, or maybe just using a debugger / memprofiler."
    author: "Jonathan Brugge"
  - subject: "Re: What os/kernel?"
    date: 2001-09-20
    body: "I quote : \"All the tests were done on SuSE 7.2 (x86) with gcc-2.95.3. I repeated some of the tests also on Mandrake 8.0 with gcc-2.96, which was the newest gcc version I had available, with similar results.\"\nmandrake 8.0 : kernel 2.4.3\nsuse 7.2 : 2.2.4 and 2.2.19 (2.2.4 by default)"
    author: "JSL"
  - subject: "Re: What os/kernel?"
    date: 2001-09-20
    body: "Whooooops, shame on me :-/ I must have missed it, thanks."
    author: "Erik Hensema"
  - subject: "Re: What os/kernel?"
    date: 2001-09-20
    body: "2.4.7 . And I don't think the values reported were wrong. libkdeui contains about 100KB unshared data (as reported by objdump --headers) and when linking against it, the unshared memory usage increased by about 100KB. (I hope that by SHARED you don't mean the value reported as 'shrd' by top. I mean the column in top called SHARED.) I also checked (some of the values) by checking /proc/<pid>/maps ."
    author: "Lubos Lunak"
  - subject: "check memory on solaris or HP-UX but NOT linux"
    date: 2001-09-20
    body: "please check the memory results on solaris or HP-UX before going ahead with this mainly because I dont completely trust linux to report the right results. \n(would you want more than one judge in a race ?)\n\nthere are free tools from both SUN and HP to do this and GCC is supported on both platforms \n\nregards\n\njohn jones"
    author: "john jones"
  - subject: "Re: check memory on solaris or HP-UX but NOT linux"
    date: 2001-09-21
    body: "Much of what's reported in this analysis is the same as what Waldo found when trying to find the bottleneck in KDE app startup time.  Waldo determined that simply linking kdecore would create 162 dirty pages (an x86 page being 4kB, so 648kB).  In his paper, he was primarily concerned with the relocation phase which took some considerable time in loading KDE apps.  This new paper is more concerned with the other sources of dirty pages in KDE apps, such as conflicts (which could be solved by the glibc and gcc teams) and data stored within the image of the library/executable that is modified at runtime (and therefore copied-on-write on Linux).\n\nTesting on HPUX or Solaris would gain them nothing, as having 100% correct numbers mean nothing, only the fact that there are considerable numbers of dirty pages is enough to prove that something needs to be done, and it doesn't really matter one bit if you trust Linux's reports or not."
    author: "Chad Kitching"
  - subject: "Alternatives?"
    date: 2001-09-20
    body: "So for us linux (and other gnu) users this seems like a problem with\ngcc and the linker.\nAre there any alternativ compiler linker and ld.so that can be used\non Linux ?"
    author: "NO"
  - subject: "Re: Alternatives?"
    date: 2001-09-20
    body: "There is a non-free C++ compiler from Intel, and they claim to be not compatible with C++ output from gcc, so linking seems to be different. I  doubt that the speed improvements would justify porting KDE (and all applications) to their compiler though - buying a faster CPU is probably a cheaper solution...<br>\nYou can find the compiler here: <br><a href=\"http://developer.intel.com/software/products/compilers/c50/linux/\">http://developer.intel.com/software/products/compilers/c50/linux/</a>"
    author: "Tim Jansen"
  - subject: "Re: Alternatives?"
    date: 2001-09-21
    body: "If this is better than gcc, perhaps the the people who make the RPMs could get it? We don't all need to own the compiler, most probably use DEBs or RPMs."
    author: "Birger Langkjer"
  - subject: "Re: Alternatives?"
    date: 2001-09-21
    body: "Your entire system would have to be recompiled.  You couldn't just get one RPM that had been compiled with the other compiler, since it's incompatible with GCC."
    author: "not me"
  - subject: "Re: Alternatives?"
    date: 2001-09-21
    body: "Ummmm...no."
    author: "ac"
  - subject: "Re: Alternatives?"
    date: 2001-09-22
    body: "You wouldn't see any benefit unless your entire system was compiled with Intel's compiler.  Earlier in the thread it was stated that the compiler is incompatible with GCC.  If this is true it can't use GCC-compiled libraries, which means programs compiled with it can't run on GCC-compiled systems unless they are statically linked, which is a really dumb thing to do most of the time.  The supposed increased speed would probably be offset by the much larger binary size of a statically linked program.  Especially with KDE programs, since a lot of KDE technology is based on shared libraries.\n\nSo you could, I guess, in theory get one RPM compiled with Intel's compiler.  But the program contained in it would be bloated and memory-hungry, and probably slower for this reason.  There would be no benefit unless your entire system including all your libraries was compiled with Intel's compiler, in which case you could do dynamic linking.  However, you wouldn't be able to use any other currently available RPMs on this new system, so that wouldn't be practical either."
    author: "not me"
  - subject: "So whats the problem"
    date: 2001-09-20
    body: "From a users point of view, I dont see what whats the big deal with 650 KB wasted per application.  Anyone with a system configued with only little memory woud not be using KDE in the first place, they'd be using a more Spartin window manager or none at all right.  I rather the focus be on maximizing speed instead of maximizing memory usage. Perhaps the two are correlated,I don't know, just my thoughts as a KDE user, and a guy with 512 Megs of RAM"
    author: "minty"
  - subject: "Re: So whats the problem"
    date: 2001-09-20
    body: "You are just wrong.\nSomeone with a small system will use a lightweight windows manager and not KDE or Gnome, but he might still want to use some KDE apps and this is exactly the problem as those 650 KB are wasted only when there is no kdeinit in the background to start the application. So those 650 KB per applications will be wasted on those small systems that don't run KDE as their windows manager."
    author: "renaud"
  - subject: "Re: So whats the problem"
    date: 2001-09-20
    body: "Don't forget that filling unnecessary memory takes time, thus reducing speed. And I'd love to see KDE run faster on my 40MB / 133MHz Pentium I. I don't intend buying a new computer any time soon."
    author: "Stefan Heimers"
  - subject: "70% of PC users are using obsolete machines"
    date: 2001-09-21
    body: "I don't know if you've read much about Windows XP, but one of the big IT analysts (I think it was Gartner Group) recently said that 70% of PC users would have to upgrade in order to meet the minimum requirements for running Windows XP.\n\nNow, if memory serves me right, Microsoft's minimum requirements for XP are a 300MHz processor and 128MB of RAM.\n\nSo, you can deduce from this that 70% of PC users either have a processor less than 300MHz or less than 128MB RAM - a huge, huge market that SuSE could easily tap into now that Microsoft has basically given up on them.\n\nPersonally, I run KDE at home on a PII-233 system with 64MB of RAM, and actually it's pretty speedy - but there are areas that could be better.\n\nKudos to SuSE for sponsoring this vital work.  Whilst I'm not sure they can tempt me away from the joys of Debian on my home machine, when I have finally convinced my work to switch to Linux and KDE (not too much longer to go now, I nearly have them convinced!), SuSE will be top of my list of recommended distros.\n\nPS if both SuSE and Mandrake included apt as the main software update tool, you might even convince me to switch my home machine :)"
    author: "Amazed of London"
  - subject: "Re: 70% of PC users are using obsolete machines"
    date: 2001-09-21
    body: "Try urpmi for Mandrake and Suse systems."
    author: "illya_the_great"
  - subject: "Re: 70% of PC users are using obsolete machines"
    date: 2001-09-21
    body: "urpmi is ok, but it is really rather limited compared to apt.  For instance, how do I tell urpmi to fetch a source package, all the dependencies (in source form) then build and install all the packages?\n\nOr, having fetched a package of something from, say, cooker, and all the dependencies, then, when I update, get it to fetch the latest version of that package and its dependencies from cooker again, but only update the rest of the system to the latest _stable_ version?\n\nAll this, and a bunch more, is possible with apt - and in fact it's quite easy.\n\nIf I could use apt to update from release to release as well as the minor security updates that occur during the lifetime of a release, then I would be more than happy to pay a subscription for the service.  I like Debian, but I admit it's not the easiest of distros to use, even with apt - if a nice user-friendly distro like SuSE or Mandrake had apt, then I think I'd be prepared to pay.\n\nThere you go, there's a nice new additional business model for SuSE and Mandrake.  With apt subscriptions they could pull off the same kind of business model that Microsoft wants to move to, but with far more style, panache and power."
    author: "Amazed of London"
  - subject: "Re: 70% of PC users are using obsolete machines"
    date: 2001-09-21
    body: "Apt is in Mandrake Contribs. It works quite well. Mandrake also have urpmi."
    author: "Yama"
  - subject: "Re: 70% of PC users are using obsolete machines"
    date: 2001-09-21
    body: "Yes, I'm aware that Mandrake does have it as an option - which is why I said 'apt as their *MAIN* software update tool' (with added emphasis).  Whilst it does work, as a contrib rpm it won't be supported or integrated, so your average user just isn't going to use it.\n\nSee my other response as to why I think urpmi is rather inferior."
    author: "Amazed of London"
  - subject: "Re: 70% of PC users are using obsolete machines"
    date: 2001-09-21
    body: "Yes, I'm aware that Mandrake does have it as an option - which is why I said 'apt as their *MAIN* software update tool' (with added emphasis).  Whilst it does work, as a contrib rpm it won't be supported or integrated, so your average user just isn't going to use it.\n\nSee my other response as to why I think urpmi is rather inferior."
    author: "Amazed of London"
  - subject: "Re: 70% of PC users are using obsolete machines"
    date: 2001-09-21
    body: "Yeah, you're right. I really wish that Mandrake would support apt like Conectiva does. Urpmi was created when apt wasn't available for RPM. Now that it is, why maintain it? Apt is clearly better, IMHO."
    author: "Yama"
  - subject: "Re: So whats the problem"
    date: 2001-09-21
    body: "> From a users point of view, I dont see what whats the big deal with 650 KB \n> wasted per application. Anyone with a system configued with only little \n> memory woud not be using KDE in the first place, they'd be using a more \n> Spartin window manager or none at all right. I rather the focus be on \n\nBut that's the point: people with a small system do not use KDE *because* it has such a large memory footprint (The rendering speed is limited by the X server, so using a spartan window manager will not yield higher speed).\n\nIf the memory footprint of KDE can be reduced, even people with a Pentium 133 and 32MB ram might be able to use KDE to browse the web. \n\nAnd don't forget that these improvements will benefit not only KDE, but all C++ applications compiled by gcc. So this is a huge improvement for the \"Linux Platform\". KDE is just the first complex C++ application framework that exposes the weaknesses of gcc and binutils.\n\n> maximizing speed instead of maximizing memory usage. Perhaps the two are \n> correlated,I don't know, just my thoughts as a KDE user, and a guy with 512 \n> Megs of RAM\n\nThey are most definitely corellated. Having non-shared code that could be shared will affect the effectiveness of the 2nd level cache, and that is a huge performance factor."
    author: "Androgynous Howard"
  - subject: "Re: So whats the problem"
    date: 2001-09-22
    body: "> The rendering speed is limited by the X server\nI just want to point out this project : http://www.directfb.org (and particularly http://www.directfb.org/xdirectfb.xml)"
    author: "JSL"
  - subject: "Re: So whats the problem"
    date: 2001-09-22
    body: "I think memory waste isn\u00b4t a outrage - but it shouldn\u00b4t be ignored. Memory is very cheap these days, but most computers have a quite low limit of RAM they can use. My box, for example isn\u00b4t old (PIII/500, < 2 years old), and it can\u00b4t use more than the 256 Meg i\u00b4ve installed. I would upgrade to 512 or 1 gig, but i can\u00b4t. I thing it should be fixed, even on boxes with a huge amount of RAM the memory should be used for usefull things. This isn\u00b4t as important than other things, like stability for example - but the developers should keep an eye on it. Ignoring it would be M$ style."
    author: "Ralf"
  - subject: "Re: So whats the problem"
    date: 2001-09-23
    body: "Just try KDE2 on a older box and it's quite obivious, even on FreeBSD, it's very very slow simply because there is far too much disk swapping to do simple things especially on system with < 96megs ram.\n\nKDE2 takes almost 2mins to load on my P166MMX with 32megs ram running FreeBSD 4.3-RELEASE and XFree86 4.1 compared to Windows 95 which only takes 25secs to load everything from the kernel to the gui (FreeBSD takes around 20~30secs from when FreeBSD is booted to when the login prompt appears). Konqueror also performs very badly, taking 1mins to load, compared to Internet Explorer that takes only seconds to load, and surfing is extermenly slow especially compared to Netscape Navigator 4.x (under Windows 95) which has a very old rendering engine which is very uneffective in rednering modern webpages which use lots of tables.\n\nThe bigest problem that causes all of the above problems is KDE2's memory usage. Loading 2 copies of Konqueror, one copy of Kate can see memory usage ballon up to over 64Megs total system memory (32Megs RAM + over 32Megs swap) causing the system to swap nonstop, and it's even worse when you load more copies of Konqueror and other KDE programs.\n\nSince many people (and companies) are using systems with only 64megs ram (or even 32megs) it would be a good idea to opitize KDE by cutting down memory usage across all platforms (instead of focusing on Linux's problems which are THEIR problem in the first place) to encourage people to migrate from Windows over to UNIX[-like] platforms like FreeBSD, NetBSD, OpenBSD, Solaris, Linux and other systems."
    author: "James Pole"
  - subject: "Re: So whats the problem"
    date: 2001-09-23
    body: "THE PROBLEM IS THAT KDE WILL NOT WORK WELL ON MACHINES THAT MANY PEOPLE WHO WOULD LIKE AN ALTERNATIVE TO MSWINDOWS OWN!  LINUX LOSES OUT IF NEW USERS GET  FRUSTRATED AND ARE NOT INTERESTED IN A SYSTEM THAT HAS A STEEP LEARNING REQUIREMENT.\nI'M A NEW USER THAT UPGRADED TO 850 CPU AND 512 MEGS RAM TO GET SO SO PERFORMANCE, BUT I'M DETERMINED TO GET ALONG WITHOUT A MS OS IF I CAN.\nMY OLD SYSTEM WAS A 266 CPU AND 64 MEGS RAM AND KDE WAS TROUBLE WITH EVERY LOG-IN.  I ONLY USE THE MACHINE FOR E-MAIL AND INTERNET ACCESS AS MOST WINDOWS USERS WOULD. WINDOWS 98 DID NOT EVEN RUN WELL ON THE OLD SETUP. \nI USE MANDRAKE LINUX AND IT COMES CLOSE TO BEING THE ANSWER I AM LOOKING FOR, BUT WILL I BE ABLE TO MASTER THE NON GUI PART?   I'M NOT LITERATE IN COMPUTER TECHNICAL DETAILS SO IT MAY BE A STRUGGLE I WON'T WANT TO TAKE ON!\nAN EASY TO UNDERSTAND NEWCOMERS MANUAL WOULD BE NICE IF I KNEW WHERE I COLD GET ONE!!!"
    author: "Elton Wade"
  - subject: "Re: So whats the problem"
    date: 2001-09-24
    body: "Your most serious problem seems to be your broken capslock key."
    author: "someone"
  - subject: "SuSE is hiring!"
    date: 2001-09-20
    body: "Nice to hear that SuSE is still hiring people to do innovative things.\nHopefully it will make KDE3 a little bit snappier ( there isnt a lot left\nto improve judging from Win2ks speed )"
    author: "ac"
  - subject: "Other optimizing tips"
    date: 2001-09-20
    body: "You might also want to look at this page:\n<br><br>\n<a href=\"http://www.kosta.ch/~stefanh/speedup/\">http://www.kosta.ch/~stefanh/speedup/</a>\n<br><br>\nAnd I am looking for corrections or additions to these pages.\n<br><br>\nBye,<br>\n&nbsp;Stefan"
    author: "Stefan Heimers"
  - subject: "Re: Other optimizing tips"
    date: 2001-09-21
    body: "Anytime you want to translate that to English, you just go ahead! :)\n\nSome of us like only know one language."
    author: "Billy Nopants"
  - subject: "Re: Other optimizing tips"
    date: 2001-09-21
    body: "Pity that we know the wrong one this time, eh? ;)"
    author: "Logan Johnson"
  - subject: "Re: Other optimizing tips"
    date: 2001-09-21
    body: "http://babelfish.altavista.com/, crybaby."
    author: "Jon"
  - subject: "Re: Other optimizing tips"
    date: 2001-09-22
    body: "Heh, next time you want a laugh, take a large, techincal article or paper, and run it through babelfish into another language, then back to your own."
    author: "Carbon"
  - subject: "Re: Other optimizing tips"
    date: 2001-09-21
    body: "better than \n#ifdef DEBUG\n  cerr<<\"I am still alive\"<<endl;\n#endif\n\nis\nsomewhere at the beginning of each source file:\n\n#define DEBUG 1\n#define dcerr cerr<<\"SomeClass::\"\n\n...\nint SomeClass::foo()\n{\n   ...\n   dcerr<<\"foo() I am still alive :-)\"<<endl;\n   ...\n}\n\nthis is much better usable\n\nBye\nAlex"
    author: "aleXXX"
  - subject: "Re: Other optimizing tips"
    date: 2001-09-21
    body: "seems I forgot the main point, of course I mean:\n\n#define DEBUG 1\n#define dcerr if (DEBUG==1) cerr<<\"SomeClass::\"\n\nAlex"
    author: "aleXXX"
  - subject: "KDE memory & cpu usage"
    date: 2001-09-21
    body: "It doesn't require that much to discover that KDE works badly with 64MB of RAM ! Windows 2000 works better than Linux/KDE on a Pentium 200 Mhz with 64MB of RAM !\nThe main points that should be addressed (both for a whole Linux distro and KDE) are really memory usage and speed, unless people won't start using Linux/BSD instead of crappy Windows."
    author: "Herv\u00e9 PARISSI"
  - subject: "Re: KDE memory & cpu usage"
    date: 2001-09-21
    body: "We all thank Microsoft for ending this miserable situation by introducing Win XP requiring 128MB of RAM. Microsoft is helping KDE becoming faster than Windows ;-)"
    author: "Stefan Heimers"
  - subject: "Re: KDE memory & cpu usage"
    date: 2001-09-25
    body: "I don't think it's a good idea thinking this way, it should be avoided.\nGcc and KDE team should work to make programs run fast on humble machines, for humble and poor people.\nOne of the advantages of Linux/KDE over windows should be a no-need of upgrades each 2 years.\nThinking as microsoft thinks dosen't help."
    author: "Iuri Fiedoruk"
  - subject: "Re: KDE memory & cpu usage"
    date: 2001-10-02
    body: "no kidding, has microsoft become our teachers?"
    author: "Mark Berry"
  - subject: "Re: KDE memory & cpu usage"
    date: 2001-09-21
    body: "are you kidding... just by loading a freshly installed win2k you use 58 mb ram!\n\n..my linux 2.4.8 kernel takes 29 mb with just xf 4.1 and kde 2.2.1 loaded (debian)\n\nof course KDE isn't snappy on 200 MHz though, but win2k isnt either... and that doesn't mean \"linux\" performs bader... just linux+kde, there are alternatives you know..."
    author: "ealm"
  - subject: "Re: KDE memory & cpu usage"
    date: 2001-09-26
    body: "I don't think so. I have got the P200-MMX/64RAM/S3Virge2MB and it\nworks well with kde2.2.1 under Linux on kernel 2.4.4.\nIt is true, but I can't imagine myself that Windows2000 could work\nin this terms at all :)"
    author: "Alexey Kotovich"
  - subject: "Re: KDE memory & cpu usage"
    date: 2001-09-26
    body: "I don't think so. I have got the P200-MMX/64RAM/S3Virge2MB and it\nworks well with kde2.1 under Linux on kernel 2.4.4.\nIt is true, but I can't imagine myself that Windows2000 could work\nin this terms at all :)"
    author: "Alexey Kotovich"
  - subject: "So, how long till we see a real solution?"
    date: 2001-09-21
    body: "As the title says, approxiately how long until we will see a proper solution to the problem? Are we talking months? years? what? How long will it take to fix/optimise gcc and the linker?\n\n--\nSimon"
    author: "Simon"
---
I was recently hired by <a href="http://www.suse.com/">SuSE</a> to focus on optimizing KDE, and as part of my work, I have written <a href="http://dforce.sh.cvut.cz/~seli/en/linking2/">an analysis of KDE memory usage</a> based on Waldo Bastian's <a href="http://www.suse.de/~bastian/Export/linking.txt">previous paper</a> concerning GNU/Linux linker issues and KDE startup-performance.  As it turns out, the linker issues examined by Waldo do indeed have an effect on the memory usage of C++ applications compiled and linked under GNU/Linux.  The most important number from this paper: About 650KB of memory wasted per KDE application not launched via KDE Init. I have contacted the GCC/binutils developers with this information along with some suggestions, and there has already been one <a href="http://gcc.gnu.org/ml/gcc/2001-09/msg00421.html">response</a> from Jakub Jelinek (of <a href="http://www.redhat.com/">Red Hat</a>), developer of the <a href="http://sources.redhat.com/ml/libc-alpha/2001-06/msg00113.html">prelinker tool</a>. Another <a href="http://lists.kde.org/?l=kde-core-devel&m=100075444227846&w=2">mail</a> has been sent to kde-core-devel with a suggestion as to how we could probably deal with the issue until a full GCC/binutils-based solution is available.
<!--break-->
