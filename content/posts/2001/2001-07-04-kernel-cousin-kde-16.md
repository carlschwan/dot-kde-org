---
title: "Kernel Cousin KDE #16"
date:    2001-07-04
authors:
  - "numanee"
slug:    kernel-cousin-kde-16
comments:
  - subject: "Re: Kernel Cousin KDE #16"
    date: 2001-07-04
    body: "Sounds like some more sweet stuff from the KDE team!  Keep up the great work guys!"
    author: "Brian Ewbank"
  - subject: "FTP via proxy"
    date: 2001-07-04
    body: "I read about the FTP problem in KC. \n\nIt was said that when the control connection is idle for too long while you are downloading a big file, it will be droppen from the masquerading table in the router.\n\nThis is indeed the problem, but the solution is not to try to artificially keep up the connection.\nInstead you need to load the Linux FTP masquerading module into the router's kernel.\n\nThis will make Linux reset the timeout on the control connection whenever data flows over the data connection.\n\nWalter\n\n(The list archive is down and I'm not subscribed to the KDE list in question anyway, so I would appreciate if someone forwards this.)"
    author: "Walter"
  - subject: "Re: FTP via proxy"
    date: 2001-07-05
    body: "For some reason my Cisco router won't load a Linux kernel module and how come other clients on other platforms work?"
    author: "DavidA"
  - subject: "Re: FTP via proxy"
    date: 2001-07-05
    body: "I was assuming that a linux transparent proxy was used. If your Cisco doesn't have a similar way to prevent dropping of NAT entries file a bug report with Cisco!\n\nWalter"
    author: "Walter"
  - subject: "Has anyone tried the KDE2.2 beta1?"
    date: 2001-07-04
    body: "Today I installed the KDE2.2 beta1, but experienced problems with KSycoca and some none existing MIME types. \n\nThe packages are the SuSE 7.1 rpm\u00b4s and were installed completly. KSycoca said something like wrong Version number ... expect version 28...\nActually I couldn\u00b4t do anything. Kicker didn\u00b4t display any app and via Alt+F2 commands didn\u00b4t function. As I started Konqueror it just come up with a message like:\"http is not supported\"\n\nMaybe the rpm\u00b4s are broken?"
    author: "Andi"
  - subject: "Re: Has anyone tried the KDE2.2 beta1?"
    date: 2001-07-05
    body: "try this:\n1. logout\n2. killall kdeinit\n3. killall dcopserver\n4. rm -rf /tmp/ksocket-*\n\nand try again..."
    author: "Adrian"
  - subject: "Re: Has anyone tried the KDE2.2 beta1?"
    date: 2001-07-05
    body: "Thanks Adrian, the remaining ksocket files were the reason. \n\nGood Build, it runs quite stable for more than 8 hours!Once there come up a klaunch problem, which I can\u00b4t reproduce either."
    author: "Andi"
  - subject: "Re: Has anyone tried the KDE2.2 beta1?"
    date: 2001-07-05
    body: "I haven't tried the new 2.2 beta, but I have a tip for any KDE upgrade:\n\nYou should try logging in with a new user, or delete your ~/.kde folder if you don't mind starting fresh.  Often times, old settings files clash with new versions of programs.\n\n-Justin"
    author: "Justin"
  - subject: "Re: Has anyone tried the KDE2.2 beta1?"
    date: 2001-07-05
    body: "Hmm... I never do this. I think I have my .kde dir since some pre 1.95 version.\n\nIs there a technically sound reason behind this advice?"
    author: "Inorog"
  - subject: "Re: Has anyone tried the KDE2.2 beta1?"
    date: 2001-07-05
    body: "unless you are interested in seeing how KDE starts up the first time its run, this is usually not necessary.\n\nin the past, it has happened that certain config files have become corrupted to the point of crashing some app (hopefully this weakness is being fixed with a new check summing class to piggyback on the reading of the file) and that particular config file needed to be removed. sometimes when it is especially difficult to figure out which file is the offender or the user is particularly lazy/not attatched to their settings they will remove all of .kde ...\n\ni have a rather ancient .kde, run KDE2 from CVS and the worst i've had to do is rm a file or two that got corrupted... and as a \"bonus\" it was always pretty obvious which file it was that i should delete  =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Has anyone tried the KDE2.2 beta1?"
    date: 2001-07-05
    body: "try running kbuildsycoca\n\nJason"
    author: "Jason Katz-Brown"
  - subject: "GPL KDE Apps for Windows"
    date: 2001-07-06
    body: "The GPL does prohibit distributing binaries linked to proprietary libraries such as free-beer QT for Windows. One solution would be to distribute source only. A user is allowed to link GPL'd code against whatever she wants, so long as it is not redistributed in binary form. Someone could even write a wrapper app to do the compilation and installation and release it under LGPL or some other license that permits linking against the libraries.\n\nOf course, that only works for someone with all the stuff necessary to compile it, but still... it would be nice for the coolness factor of having KDE running natively under Windows, even if it wasn't tremendously useful for end users. Having the sources available might also help companies who could afford QT licenses and wanted to standardize on a KDE desktop internally, even on their windows boxes. Again, as long as they don't distribute it, the GPL doesn't care."
    author: "Raymond Fingas"
---
This week in <a href="http://kt.zork.net/kde/kde20010629_16.html">KC KDE</a>, Aaron and Rob bring us news about SOAP and DCOP, <a href="http://capsi.com/kmonop/">KMonop</a> vs <a href="http://www.uni-karlsruhe.de/~Daniel.Hermann/Capitalist/">Capitalist</a>, a new <a href="http://www.ipso-facto.demon.co.uk/development/konq-cervisia.png">Cervisia KPart</a> (<a href="http://www.ipso-facto.demon.co.uk/development/konq-cervisia2.png">2</a>), <a href="http://users.swing.be/sw286000/">print services integration</a> into Konqueror, the KDE CVS Geek of the Week (you may now use PrintScreen for those KDE snapshots),  and more.
<!--break-->
