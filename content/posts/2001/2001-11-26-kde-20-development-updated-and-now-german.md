---
title: "KDE 2.0 Development:  Updated, and now in German"
date:    2001-11-26
authors:
  - "Dre"
slug:    kde-20-development-updated-and-now-german
comments:
  - subject: "I think this is the _VERY_ important"
    date: 2001-11-26
    body: "Greetings,\n\tFirst a big tip of the hat to these guys.  FYI, to all of us americans not everyone knows english.  Ralf and I spoke about this some, and it is well known that there are large numbers of developers that speak very little technical english and development docs can sometimes be very hard to understand.  Personally I would love to see a Spanish version of this book.  I am sure there are many developers in South America and Spain that would really be willing to develop for KDE if they had the proper docs.\n\nI had a conversation with a friend of mine from russia a few weeks ago.  The only reason they have not dropped support for MSVC at their university is because toolkits like QT and KDE lack any native russian translations for the API documentation.\n\nI think we have a good example here.\n\nCheers!\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Yes"
    date: 2001-11-26
    body: "There are only german and english books listed on the KDE site.  This is quite incomplete I am sure there are chinese books and japanese books and maybe even french books on KDE.  Let us please try to make the list complete. This is one contribution to the list:\n\nA new Qt/KDE 2 programming book written in Traditional Chinese from\nTaiwan\n \nISBN:   9572085689\nauthor: Chuang, MingChe\npublisher: SoftChina( http://www.softchina.com.tw)\nPages: 432\nhttp://www.ibookpress.com/books/book.asp?ItemId=202219"
    author: "KDE User"
  - subject: "Re: Yes"
    date: 2001-11-27
    body: "Thanks, I added it.\n\nAnyone else?\n\nCheers,\nWaldo"
    author: "Waldo Bastian"
  - subject: "Re: I think this is the _VERY_ important"
    date: 2001-11-26
    body: "Just for the record, this book has been translated into both Spanish and Japanese (see <a href=\"http://www.andamooka.org/article.pl?sid=01/10/09/164245&mode=nested\">http://www.andamooka.org/article.pl?sid=01/10/09/164245&mode=nested</a> [Why isn't HTML available as an encoding when replying to a message? -- <i>mea culpa, ed</i>]).  I'm working on getting electronic versions for Andamooka. :)\n\nDave"
    author: "David Sweet"
  - subject: "Re: I think this is the _VERY_ important"
    date: 2001-11-26
    body: "Hello, David. Your book is fantastic, and I'd like to find out if you have plans on a 2nd edition for KDE 3.0. Any thoughts?\n\nEron"
    author: "Eron Lloyd"
  - subject: "Good book but..."
    date: 2001-11-27
    body: "I bought this book when I knew nothing about QT/KDE programming.  I had done plenty of MFC/win32 programming though.\n\nIt was a good help in learning the basics, but it was far from complete.  I believe it's wrong to have a book about programming without a chapter on printing.  How many programs don't have a print feature?\n\nHow about doing a book on KDE advanced programming techniques.  I would be the first to buy it.\n\nAlso the next version should become more centered around KDevelop and QTDesigner, as your target audience are going to use these programs to code.  Visual C++ books are a good reference of how to do this.\n\nKeep up the good work."
    author: "Paul Hensgen"
  - subject: "Re: Good book but..."
    date: 2001-11-27
    body: ":: I believe it's wrong to have a book about programming without a chapter on printing. How many programs don't have a print feature?\n\n   KDE Printing has only recently really become stable - I would imagine that would be the reason that it hasn't been covered.\n\n   Hopefully that will be addressed when the book is updated for KDE 3.0\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Good book but..."
    date: 2001-11-27
    body: ">How many programs don't have a print feature?\n\nNone, if you have a lot of patience and ksnapshot or gimp :-)\n\n>Also the next version should become more centered around KDevelop and QTDesigner, as your target audience are going to use these programs to code. Visual C++ books are a good reference of how to do this.\n\nI think that's a bad idea, really. Programming is not IDE-centric, and a good IDE will not change the way you code, but rather assist it. I personally use Vim and series of specialized console apps as my IDE, and it does everything KDevelop does, though in different ways. It would be horrible to get a book and have to 'translate' all the menu-based step by step functions, when instead you can use a book that assumes you know the basics of your environment. It can simply instruct you to \"compile your program\" instead of \"click on menu->whatsit->borkbork->compile\"."
    author: "Carbon"
  - subject: "Re: Good book but..."
    date: 2001-11-27
    body: "For Linux/KDE to become really big, you need to increase your target audience.  So saying use vim and the console is unrealistic.\n\nHow many people use/say the use vim and the console just to be considered geeks?  The more developers that convert to KDE the better.  I don't care if the use KDevelop or not, as long as some cool programs get produced.\n\nAdvanced books should just say \"compile your program\", intoductory books like the one mentioned should say \"click on menu->whatsit->borkbork->compile\"."
    author: "Paul Hensgen"
  - subject: "Re: Good book but..."
    date: 2001-11-28
    body: "I meant that it shouldn't be oriented towards a particular IDE, but rather oriented towards KDE programming. The book isn't an introduction on how to program, it's an introduction on how to program KDE.  It isn't a full-force introduction to C++ or OOP or how to use the mouse either, though all those skills are involved. If you want to use KDevelop, it has online documentation that you can read and use to learn KDevelop before learning how to ignore it (that is, to not pay attention to the UI but to your code). The same goes for any advanced IDE. Forcing readers of the book to use KDevelop _lessens_ your target audience from \"programmers\" to \"programmers that use KDevelop\".\n\n>How many people use/say the use vim and the console just to be considered geeks?\nNone, of those who use it properly and don't just pretend to. How many people use KDevelop or MSVC or <insert-any-GUI-IDE-here> just to be considered non-geeks?\n\nAssuming your reader knows how to do something as essential and simple as compiling their project from their IDE of choice does not make a book \"advanced\"."
    author: "Carbon"
---
<a href="mailto:dsweet@andamooka.org">David Sweet</a> wrote in to tell us
that the updated, German version of the excellent (and free) KDE 2 development book,
<a href="http://dot.kde.org/973837747/">KDE 2.0 Development</a>, is now
available
<a href="http://www.andamooka.org/reader.pl?section=dekde20devel">online</a>
at <a href="http://www.andamooka.org/">Andamooka</a>.  David extends his
thanks to Petra Alm and Dirk Louis (translation), Karl Heinz Zimmer and
Carsten Pfeiffer (tech editing) and Boris Karnikowski (Andamooka version).
For those of you who missed it, a dead
tree version of the book can also be purchased
(<a href="http://www.amazon.de/exec/obidos/ASIN/3827259800/kdesktopenvironm">Amazon.de</a>,
<a href="http://www.mut.de/shop/sh-info.asp?ID=3827259800">Markt+Technik</a>).
This might be an opportune time to point out that the KDE website features
<a href="http://www.kde.org/kde-books.html">a nice listing</a> of
available KDE books.

<!--break-->
