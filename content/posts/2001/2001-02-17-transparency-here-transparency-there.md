---
title: "Transparency here, transparency there...."
date:    2001-02-17
authors:
  - "tunrau"
slug:    transparency-here-transparency-there
comments:
  - subject: "Re: Transparency here, transparency there...."
    date: 2001-02-17
    body: "Bah. Emacs can do this with Eterm, do you hear me shouting about it."
    author: "Richard Stallman"
  - subject: "Re: Transparency here, transparency there...."
    date: 2001-02-17
    body: "hmm... and trolls usually hang out at /., but u don't hear us shout about that huh!"
    author: "kde"
  - subject: "Re: Transparency here, transparency there...."
    date: 2001-02-20
    body: "Hmmm. Eterm can even do this without emacs. :-)"
    author: "Mariusz"
  - subject: "Re: Transparency here, transparency there...."
    date: 2001-02-17
    body: "I would like to see semi (or full) transparent pull down menus too.\nAnd I would like transparent windows don't show only the kde background like konsole does, but other windows that are behind it too.\nThis would be a cool eye candy :)"
    author: "Protoman"
  - subject: "Re: Transparency here, transparency there...."
    date: 2001-02-17
    body: "<p>XFree 4.0.2 supposedly is able to do this (fully translucent windows, in which everything behind them, including other windows, shows through), but AFAIK, if the KDE programmers utilized these features, then it would force everybody to upgrade to XFree 4.0.2. This wouldn't be that bad, except the 4.x versions of XFree don't support all of the video cards that the 3.x versions did.</p>\n<p>I suppose by \"transparent pull down menus\" you mean something like the picture at the bottom of the page at this address:<br><a href=\"http://www.xfree86.org/~keithp/render\">http://www.xfree86.org/~keithp/render</a></p>"
    author: "Josh Liechty"
  - subject: "Re: Transparency here, transparency there...."
    date: 2001-02-17
    body: "That's why I think there should be more binaries out there.\nIt's relatively easy to create rpms with and without those features, and on source you can detect X version of course.\nBut lastly seems that creating rpms is a pain to companies and not something they do to gain more happy users.\nI even don't have XFree 4.0.2 simply because there isn't rpms for redhat 6.2, and I don't want to install it with breaking Xconfigurator program (I hate xf86config)."
    author: "Iuri Fiedoruk"
  - subject: "Re: Transparency here, transparency there...."
    date: 2001-02-17
    body: "You realize, of course, that XFree86 has its own graphical setup utilities, right?  You're not forced to use xf86config, or something non-standard like xconfigurator or whatever.  Try XF86Setup on XFree 3, xf86cfg on XFree 4."
    author: "Joe KDE User"
  - subject: "There's also xf86cfg in XFree86 4.0"
    date: 2001-02-17
    body: "The new graphical tool being included with XFree86 4.0, while very much a work in progress, looks promising.  I didn't try it with 4.0.2, but it was only semi-functional in 4.0.1.  But still, it'll make a nice option."
    author: "Cardinal"
  - subject: "Re: Transparency here, transparency there...."
    date: 2001-02-18
    body: "I set up my XF86Config for XFree86 4.0.x with a text editor just by reading 'man XF86Config' for a while, and also using an <a href=\"http://www-sop.inria.fr/cgi-bin/koala/nph-colas-modelines\">online modeline generator script</a>. Even without much previous experience, it was easy. I first installed XFree86 4.0.1 from a binary package at xfree86.org (why are you obsessed with using an rpm? That binary package has an excellent install script), then rebuilt it (this wasn't too hard, either; most of the defaults were fine, so I only had change a few things before 'make World' and 'make install'), and subsequently built 4.0.2 (this time with Xft support and DRI) myself. Now, I am using a cvs copy of 4.0.2 that's based on X11R6.51. In all, it's not particularly hard; there are some good HOWTO's out there."
    author: "Triskelios"
  - subject: "Re: Transparency here, transparency there...."
    date: 2001-02-18
    body: "yes! i think simply editing XF86Config with an editor is the best way to configure x. \ni hate all these graphical-\"frontend\" that makes the whole thing more confusing than it is.\n\nthanks for the address of the modeline-generator. ;)"
    author: "gnu128"
  - subject: "Re: Transparency here, transparency there...."
    date: 2001-02-18
    body: "It's funny to hear this in a GRAPHICAL desktop environmant, but I think that it shouldn't be as hard as it is. Just need to be well designed and correspond to the logic of the text , not try to hide any parameter, and write READABLE and well formatted TEXT parameters."
    author: "Sould not !"
  - subject: "Re: Transparency here, transparency there...."
    date: 2001-02-17
    body: "Licq with the QT-gui plugin compiled with kde support does that on my system. Minimize licq to the task bar and then place your cursor over the system tray icon and it will pop up a transparent tool tip after about 1 sec. Fades in really well also and of course shows windows behind etc. I am running XFree 4.0.2 CVS and compiled licq from source so maybe that's why. I certainly looks cool, I hope this implimentation works the same way."
    author: "David"
  - subject: "Re: Transparency here, transparency there...."
    date: 2001-02-17
    body: "Wow you're right.  On the discussion of cool eye-candy, I guess this means it's also possible to have drop shadows on pulldowns and popups."
    author: "Justin"
  - subject: "Check at runtime"
    date: 2001-02-17
    body: "I see no reason why Qt or KDE couldn't check the XFree capabilities during runtime.  This way, no one would be required to have 4.0.2, but if they do have it then they get the extra features.<br>\n<br>\nNot everything needs to be compiled in.  I was recently installing a TV program, and it checked for my video device <b>in the configure script</b>.  Bad idea.  What if I change cards?<br>\n<br>\nNow that I think of it, KDE's SSL support should be runtime as well.<br>\n<br>"
    author: "Justin"
  - subject: "Re: Transparency here, transparency there...."
    date: 2001-02-19
    body: "As far as I know, for real transparent / translucent windows and menus we will have to wait for keithp's Transparent Window extension.  There's a paper he wrote about it on his website."
    author: "damian"
  - subject: "Re: Transparency here, transparency there...."
    date: 2001-02-19
    body: "<p>AFAIK, this feature (the Render extension, IIRC) is already in XFree 4.0.2. However, I have no idea if it's compiled in by default.</p>"
    author: "Josh Liechty"
  - subject: "Re: Transparency here, transparency there...."
    date: 2001-02-19
    body: "errrrrrrrrr....\n\n\tthere are a lot of strange messages out there. I use xf4.0.2, and the render extension as of now is just AA text (which is already a lot). Translucent windows etc are for later..........\n\n(and if i'm wrong PLEASE let me know, unfortunately i'm quite certain of what i assert)\n\nEmmanuel.sorry.to.be.right :O("
    author: "emmanuel"
  - subject: "Re: Transparency here, transparency there...."
    date: 2001-02-21
    body: "Couldn't it be possible to include this as a module? Or is this kind of stuff too basically?"
    author: "grovel"
  - subject: "Re: Transparency here, transparency there...."
    date: 2001-02-19
    body: "<i>[...] And I would like transparent windows don't show only the kde background like konsole does, but other windows that are behind it too.</i>\n<br><br>\n\nIf you think about it for a moment you would probably realize how annoying that would be. For starters you could easily get confused what window is on top/has the focus. I seriously cannot imagine <b>anyone</b> using this feature permanently."
    author: "Matthijs Sypkens Smit"
  - subject: "Re: Transparency here, transparency there...."
    date: 2001-07-31
    body: "Not if it's well implemented. I was just playing around with tranparency in the Konsole, and I didn't find the right setting right away because some of the schemas show too much of the background. With 50% transparency, it would be easy to see what window is on top."
    author: "Richard Garand"
  - subject: "Re: Transparency here, transparency there...."
    date: 2001-02-28
    body: "Hells yeah!!! We need that so bad.\nOf course, it has to have an optional dim/tint/semi-transparent mode to keep it from getting confusing :)"
    author: "Windigo"
  - subject: "Working transparency"
    date: 2001-10-10
    body: "Ok XFree seems to be able to get translucent windows, but how the hell can we get it to work ? Do we need to recompile all the appz that could deal with this Xfree feature ? where can we find howtos ?"
    author: "Julien chevalier"
  - subject: "Re: Transparency here, transparency there...."
    date: 2001-02-17
    body: "Link to Konqueror screenshot is broken :("
    author: "Baracuda"
  - subject: "Re: Transparency here, transparency there...."
    date: 2001-02-17
    body: "<a href=\"http://www.mts.net/~jhtd/konq_transparent.png\">Try this link for the Konqueror screenshot</a> :)"
    author: "Tobias Schenck"
  - subject: "Re: Transparency here, transparency there...."
    date: 2001-02-17
    body: "Fixed, thanks.\n\n-N."
    author: "Navindra Umanee"
  - subject: "Re: Transparency here, transparency there...."
    date: 2001-02-17
    body: "I'm glade someone is working on eye candy because it sure is fun if nothing else. Hopefully this code will be out sooner then later so we can build it into kde.  I would sure like to see the backround for kicker being transparent along with the menu structure, now that would be great fun don't you think?"
    author: "Michael M Nazaroff"
  - subject: "Re: Transparency here, transparency there...."
    date: 2004-03-25
    body: "is this michale nazaroff from central east?\n"
    author: "jessica"
  - subject: "Pointless"
    date: 2001-02-18
    body: "What's the point? Instead of adding funky transparency, why don't the KDE folks do stuff like: fixing bugs - fixing spelling/grammar mistakes in the UI - fixing capitalization mistakes in the UI - making the UI more consistent - making KDE faster."
    author: "me"
  - subject: "Re: Pointless"
    date: 2001-02-18
    body: "Why? Because the KDE folks are volunteers. They like to hack some fun stuff too from time to time. Besides that the KDE folks DO fix bugs, they DO the other things you mentioned. If that doesn't satisfy you, you could even do those things yourself!"
    author: "Wilco"
  - subject: "Re: Pointless"
    date: 2001-02-23
    body: "Yeah right. First of all, please report those bugs you found."
    author: "Anonymous Coward"
  - subject: "Re: Transparency here, transparency there...."
    date: 2001-02-18
    body: "you are wrong!\n\nhave a look at kde 2.1 it is stable and usable and very fast!\n\nimho it is time to implement good looking things too.\n\nhave a look also on the bug-tracking system of gnome.... they have realy big fixing-problems... and it seems no one kann solve it.\n\nbye\n\nsteve"
    author: "steve qui"
  - subject: "Re: Transparency here, transparency there...."
    date: 2001-02-18
    body: ">>What's the point? Instead of adding funky >>transparency, why don't the KDE folks do stuff >>like: fixing bugs\n>you are wrong! have a look at kde 2.1 it is >stable and usable and very fast!\nAnd who said that? Last time I checked, half of\nthe applications still crashed in the first 40\nseconds of actual usage (i.e. not <b>watching</b>\nthe screen, but clicking on it). The applications\nthat worked seemed to make some strange\nassumptions on what functionality I need and what\nI do not need. For example, there was no ability\nto set arbitrary colors in KTerm. Nor was it\npossible to use large fonts for window titles,\nas the title bars did not change height. This\nlist can continue for ever and ever.\n\n>have a look also on the bug-tracking system of >gnome.... they have realy big fixing-problems... >and it seems no one kann solve it. bye steve\nWas it supposed to be an argument against the\nnecessity to fix bugs?\n\n"
    author: "KDE User"
  - subject: "Re: Transparency here, transparency there...."
    date: 2001-02-18
    body: "you are a troll!"
    author: "steve qui"
  - subject: "Re: Transparency here, transparency there...."
    date: 2001-02-18
    body: "you have your head in the sand, zealot."
    author: "Jimbo"
  - subject: "Re: Transparency here, transparency there...."
    date: 2001-02-19
    body: "Get your head out of your ass and you'll see that this is not true."
    author: "Jim Bob"
  - subject: "Re: Transparency here, transparency there...."
    date: 2001-02-18
    body: "you have your head in the sand, zealot."
    author: "Jimbo"
  - subject: "This is just not true."
    date: 2001-02-22
    body: "You must be doing something very very odd, then, as I and several people in my office have been using KDE 2.1Beta2 since it was released, and I've heard of maybe two or three crashes, total, since that time."
    author: "Uh?"
  - subject: "How about speed"
    date: 2001-02-18
    body: "OK, it's cool to have a beautyfull desktop on the screen to show friends how Linux can look.\nBut don't forget to optimize your code. I use KDE 2.0.1 on an \"old\" pentium 166 and it is quite slow to load and konqueror isn't usable (using mc instead), althought it work great on my Athlon 600."
    author: "renaud"
  - subject: "Re: How about speed"
    date: 2001-02-19
    body: "You should find it quite a bit faster in 2.1, the file views are a lot more efficient.\n\nRich."
    author: "Richard Moore"
  - subject: "Re: How about speed"
    date: 2001-02-19
    body: "How does this compare to IE 5.5 (or Netscape 6.0) running on your P166 and Athlon 600 ?\n\nCheers"
    author: "RE: How about speed"
  - subject: "Re: How about speed"
    date: 2001-02-20
    body: "I don't use them on the P166, it's mainly used to write my reports (with LyX).\nBut with the Athlon, Konqueror work greater than Netscape (don't use IE, i don't like it)."
    author: "renaud"
  - subject: "Re: How about speed"
    date: 2001-02-21
    body: "Of course the matter is the quantity of RAM being used, but on any pc I got IE is faster than konqueror. (this does not mean I can bear running windows more than one hour in a week) :)\n\nA little ot but important for me: has someone thougt of the kde panel being able to become a gnomeish corner panel? I like the afterstep wharf and this is one of the reasons I prefer gnome to kde (!!! a serious reason of course) And what about interchangeability between gnome and kde applets? That would be useful!"
    author: "Nick Name"
  - subject: "Re: How about speed"
    date: 2001-02-19
    body: "The important thing isn't the processor but the amount of memory. If you have at least 48 MB and if you only use KDE-applications the KDE 2.x should perform quite nicely on your system. If it doesn't then you probably use bad RPMs or something else doesn't work properly.\n\nGreetings,\nTackat"
    author: "Tackat"
  - subject: "Re: Transparency here, transparency there...."
    date: 2001-02-19
    body: "This is a great idea and it's not just eye candy but also a great relief on the eyes!  Good on you!!  Thank you!"
    author: "max"
  - subject: "Transparent titlebars..."
    date: 2001-02-21
    body: "Have you seen the shots of Aqua/XFree86?<p>\nhttp://www.mit.edu/~rueckert/XFreeAqua.jpg <br>\nhttp://www.mit.edu/~rueckert/XFreeAqua2.jpg <p>\nI noticed how the Aqua titlebars are transparent, that is actually somewhat usefull as you get to see more of the \"hidden\" application windows when messing around on your workspace - getting a better general feeling of where you can find windows/apps pushed in the background...<p>\nI quickly open *many* windows on the same desktop, being very poor at using multible desktops - so this would be helpful."
    author: "jimbo"
  - subject: "Re: Transparency here, transparency there...."
    date: 2001-02-21
    body: "I like the sound of this. But wouln't it be better if this was supported in Qt of the kde libaries, becasue the way I understand it is that at the moment transparency is being add on a app by app approce.\n\nThis to me seems that it is creating a lot of xtra code, which increase the size of KDE and slows it down. If this feature was added to the wigets in QT, then all the apps could use it, cutting down on the extra code, and also making it easier to implement for new apps that are been developed."
    author: "Mark Hillary"
  - subject: "Re: Transparency here, transparency there...."
    date: 2001-02-23
    body: "I can't understand why people want to do this. The idea of things being transparent may sound cool and may look cool in carefully setup screenshots but it's not very practical is it. What happens when you have multiple windows open or a complex background or one that clashes with your chosen text colour or something with text on it like a console or web page? Everything becomes difficult/impossible to read. I can't read many of the folder names in that screenshot so why would I want to use a program like that?"
    author: "Darren"
  - subject: "Re: Transparency here, transparency there...."
    date: 2002-04-25
    body: "a) It's not really transparent.  It's pseudo-transparent.  Generally it only shows the desktop.  So if you overlap windows you won't see the window underneath it through the top window, just the desktop image.\n\nb) If you use a terminal like Eterm you can not only tell it to be semi-transparent but you can also shade and tint the window.  The shading solves the text color issue.  With a 50% shade you can generally see white text even in areas where the desktop pattern is white.  Though it might be a little light.  But with a darker desktop image you'll have no issues reading anything.\n\nc) Why would you tell an application to run with a text color that clashes with your background image?  I'm assuming that if you're setting transparency on an application like konq that you're overriding the website colors in the first place."
    author: "ResDev"
---
For the last couple days, I've been working on making things transparent in KDE.  This is pretty easy to do, but time consuming.  Most notable success so far is with <A HREF="http://www.mts.net/~jhtd/konq_transparent.png">Konqueror</A>.  Next up, ksirc, and hopefully kicker.  This code cannot go into 2.1, since we are in a feature freeze, but the following version of KDE should be quite transparent! <i>[<b>Ed</b>: Also check out the transparency screenshots for <a href="http://www.ipso-facto.demon.co.uk/kde/kasbar-tng.html">Kasbar TNG</a>. There's some other slick stuff there too.]</i>


<!--break-->
