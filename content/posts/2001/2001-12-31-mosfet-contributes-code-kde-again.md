---
title: "Mosfet Contributes Code to KDE (Again)"
date:    2001-12-31
authors:
  - "Dre"
slug:    mosfet-contributes-code-kde-again
comments:
  - subject: "Thumbnail browser"
    date: 2001-12-31
    body: "Any posiblity of getting the fast thumbnail browser into konq?  Speaking of does gnome use .pics also now?\n\n-Ben"
    author: "Benjamin C Meyer"
  - subject: "Re: Thumbnail browser"
    date: 2001-12-31
    body: "No idea about Gnome but when I was first developing PixiePlus I did code a Konq file view for it. You selected it in the same manner you selected other file view types like \"Detailed List View\", etc..."
    author: "Mosfet"
  - subject: "Re: Thumbnail browser"
    date: 2001-12-31
    body: "Is there anyway that that is sitting somewhere that I can grab?  With my digital camera I frequently download multi-megabyte files that a fast preview like that would rule."
    author: "Benjamin C Meyer"
  - subject: "Re: Thumbnail browser"
    date: 2001-12-31
    body: "PixiePlus has the fast thumbnail viewer. Use that to browse your images. For over 5,000 images I could immediately view all perviously generated thumbnails in < 5 seconds. For only a couple hundred it's a second or less. This is considerably faster than anything else. If the images are multimegabyte and you have a few thousand of them they will still take awhile to generate previews, but you only have to do this once. After that it will be < 5 seconds to view all of them."
    author: "Mosfet"
  - subject: "Re: Thumbnail browser"
    date: 2002-01-01
    body: "Hey mosfet,\n\ncould you do us all a favour and add gphoto2 support to pixieplus ? :) kamera sucks, you may have a look at the new digikam app on apps.kde.com for code.\n\nHappy new year!\n\nRalf"
    author: "Ralf Nolden"
  - subject: "Re: Thumbnail browser"
    date: 2002-01-01
    body: "Sure, if someone sends me a Linux compatible digital camera ;-) I have a scanner so usually take normal photos and scan them in."
    author: "Mosfet"
  - subject: "Re: Thumbnail browser"
    date: 2002-01-01
    body: "Hmpf, I tried all morning to get gphoto2 to work with my new Olympus over USB - it even refuses over /dev/ttyS0 which I use in gphoto 0.4.3 successfully. I mean, beta3 works as far as it recognizes my camera correctly over usb but resets the number of pictures to 0 - which makes this pretty useless.\n\nBut for others, it sure would make sense. Mosfet, you just have to look how digiKam does it (more or less). gphoto2 just is a commandline tool where you write a frontend for; sure there would be testers (I would even help if my camera would work with gphoto2...)\n\nRalf"
    author: "Ralf Nolden"
  - subject: "Re: Thumbnail browser"
    date: 2002-01-01
    body: "Again, if someone sends me a compatible camera I'll do it ;-) I'm not going to write code for something I cannot test :P"
    author: "Mosfet"
  - subject: "Re: Thumbnail browser"
    date: 2002-01-01
    body: ":) I wouldn't either but how do we deal with this *if* my camera works with gphoto2 ? \n\nCould you put pixieplus into a CVS ? BTW does it make use of the scanner dialog in KDE ? (my scanner only has an isa card and I'm too lazy to install it into my other machine to try it, just wanted to know :))\n\nRalf"
    author: "Ralf Nolden"
  - subject: "Re: Thumbnail browser"
    date: 2002-01-01
    body: "Last time I say it: we deal with this by someone sending my a compatible camera so I can test it. I will not add or accept features I can't even test. If you really want it, donate a camera. I'm not going to spend money on something I don't need for an application I give away for free, nor will I accept patches for things I have no idea that work."
    author: "Mosfet"
  - subject: "Re: Thumbnail browser"
    date: 2002-01-01
    body: "William Shakespeare: As You Like It\n\nNevertheless, happy new year :)\n\nRalf"
    author: "Ralf Nolden"
  - subject: "Re: Thumbnail browser"
    date: 2002-01-03
    body: "> nor will I accept patches for things I have no idea that work.\n\nDamn good job you're not in charge of the kernel.\n\"No, I'm not putting that Sparc or IA64 support in lads, I've no idea if it'll work\""
    author: "Mark Johnson"
  - subject: "Re: Thumbnail browser"
    date: 2002-01-03
    body: ">I will not add or accept features I can't even test.\n\nSo you don't care about portability fixes or translations then?"
    author: "ac"
  - subject: "Re: Thumbnail browser"
    date: 2002-01-02
    body: "Why not just add a simple 'Open from Camera' or something that uses the gphoto kioslave (if present).  If the kioslave works, then PixiePlus shouldn't have a problem.   This brings up another issue altogether as KDE should allow a user to 'create' devices in the sense that Floppy, CDROM, Digital Cameras, MP3 Players, etc. can be more easily accessed by application and browsed via Konqueror.  kioslaves provide the grunt work, but I believe there is some definite UI work that could make them easier to use.  I know, the general response will be 'just type gphoto:/' and stuff, but I think it would be cool for KDE to have a way to 'create' these so that there is an actually system item that is browsable directly in all file dialogs and is named according to the system administrator/user's preference... (i.e. browsing to 'Hitachi Digital Camera' in FILE / OPEN to get pictures from a digital camera)\n\nOne of these days, I'll get around to actually creating and using linuxnetworker.com in order to SHOW what I mean :)\n\nAny way.. these are just my point.  HAPPY NEW YEAR!"
    author: "thoreau"
  - subject: "Re: Thumbnail browser"
    date: 2002-01-02
    body: "Well, if pixieplus used ioslaves, and supports d&d, then al you have to do is drag \nthe image from a konqueror view."
    author: "Roberto Alsina"
  - subject: "Sources for thumbnail browser"
    date: 2003-11-17
    body: "Hi,\n Is there any tool(along with C sources) available for automatically converting the images in the directory while it is browsing and showing its thumbnails when a preview option is selected. I am trying to write a lightweight tool to browse my directory which has multiple jpg/gif images in it.\n\nregards,\nSrinivas Rao.M"
    author: "Srinivas Rao.M"
  - subject: "Panorama Picture Merging"
    date: 2002-01-02
    body: "Yes, kio-kamera in HEAD seems to be in a very bad shape :-( A configuration of my IXUS crashes the whole control center - ok - it's beta...\n\nBUT here is my wishlist feature: panorama picture merging.\nMany digicams have a feature of making a sequence of pictures showing you an overlapping area during the shot. Canon has a tool for this merging stuff but -of course- its Mac and M$-Win only. The job is more than an easy crop&merge: you need some gamma-ticks and maybe morphing-things in the merging-zone.\nThat sounds like fun, or? ;-)\n\nHappy New Year\n\nThorsten\n\n--\nP.S.\nI can't spend a camera, but a sequence of such pics ;-)"
    author: "Thorsten Schnebeck"
  - subject: "Panorama Picture Merging"
    date: 2002-01-02
    body: "Yes, kio-kamera in HEAD seems to be in a very bad shape :-( A configuration of my IXUS crashes the whole control center - ok - it's beta...\n\nBUT here is my wishlist feature: panorama picture merging.\nMany digicams have a feature of making a sequence of pictures showing you an overlapping area during the shot. Canon has a tool for this merging stuff but -of course- its Mac and M$-Win only. The job is more than an easy crop&merge: you need some gamma-ticks and maybe morphing-things in the merging-zone.\nThat sounds like fun, or? ;-)\n\nHappy New Year\n\nThorsten\n\n--\nP.S.\nI can't spend a camera, but a sequence of such pics ;-)"
    author: "Thorsten Schnebeck"
  - subject: "Re: Thumbnail browser"
    date: 2001-12-31
    body: "Is there anyway that that is sitting somewhere that I can grab?  With my digital camera I frequently download multi-megabyte files that a fast preview like that would rule."
    author: "Benjamin C Meyer"
  - subject: "Re: Thumbnail browser"
    date: 2002-01-01
    body: "What is .pics?\nIs it the name of a hidden directory where the thumbnails is placed?"
    author: "John Ericson"
  - subject: "Menu Transparency"
    date: 2001-12-31
    body: "Menu Transparency support is in KDE3 CVS. And its finally the real XRender one!"
    author: "dr88dr88"
  - subject: "Re: Menu Transparency"
    date: 2001-12-31
    body: "But that doesn't seem to mean it's updating what's beneath it in realtime?"
    author: "anonymous"
  - subject: "Same as Liquid's. Not XRender transparency."
    date: 2001-12-31
    body: "The KDE CVS code uses the exact same code to perform translucency as Liquid. It's actually based on MegaGradient and uses QPixmap::grabWidget() to take a \"snapshot\" of what is behind the menu before displaying it. What it *does* do is use XRender instead of KPixmapEffect to lighten, darken, or otherwise modify the resulting image if available, but this doesn't speed up the code visibly or change the translucency at all, that is the same. In fact, it will be slower on non-accelerated XRender implementations. So I don't know what you mean by \"real XRender one\". Not to rain on the parade but I think it's unfair to imply that this provides a different translucency than is already available - it doesn't - otherwise I'd be using it ;-)"
    author: "Mosfet"
  - subject: "Re: Menu Transparency"
    date: 2002-01-02
    body: "no matter what you do in X, you can't get real translucent windows. it's only an illusion."
    author: "hash"
  - subject: "Mosfet All Star"
    date: 2001-12-31
    body: "I think Mosfet gets too much attention/publicity compared to others hard-working KDE developers."
    author: "anonymous"
  - subject: "Re: Mosfet All Star"
    date: 2001-12-31
    body: "Don't worry about it! He gets that much attention because he\n_needs_ it (methinks). And I like this behaviour. In german you`d\nsay Paradiesvogel, what means he just don't want to be an ordinary man.\nThis does not say other coders are ordinary people, just different in\ntheir style..."
    author: "not me"
  - subject: "Re: Mosfet All Star"
    date: 2001-12-31
    body: "Yep, I'm loud and noisy >:) Hell, I'm doing this for free and for fun, and I've never shyed away from making waves :)\n\nAnyways, I hope these effects will be put to good use by other developers and helps KDE3. This time instead of making waves I'm trying to show some goodwill to the KDE core team. Hopefully we can start working together on some stuff more often. Honestly, working solo has suited my style more and I'm getting lots more done these days, but that doesn't mean there has to be any conflict and that stuff I code on my own can't be contributed to the KDE project now and then. I've cooled down a lot now."
    author: "Mosfet"
  - subject: "Re: Mosfet All Star"
    date: 2001-12-31
    body: "I think that if someone does something for free, even if he/she gets all the publicity in the world, nobody should complain. So thanks Mosfet and thanks to all the other KDE developers! Oh, and BTW, happy new year to all. ;-)"
    author: "Bojan"
  - subject: "Re: Mosfet All Star"
    date: 2001-12-31
    body: "Cooled down a lot is understatement! :-P\n\nThank you, very glad you and KDE peoples are getting back together somewhat.\nIt's all common goals.  Make the Windows killer!\n\nHappy New Year all of KDE!!!!!!"
    author: "Linux4Green"
  - subject: "Re: Mosfet All Star"
    date: 2001-12-31
    body: "Thanks for contributing the code Mosfet! :-) Like others have said, it's nice to see you and the KDE Core team are working together again. Keep up all the great work, your Liquid style is wonderful, and these contributions sound very good too."
    author: "Timothy R. Butler"
  - subject: "Re: Mosfet All Star"
    date: 2001-12-31
    body: "Perhaps that suits your style better: code by yourself, then give it to someone with CVS access. Hopefully it will work out like that more and more for you. Only a fraction of the people who have KDE ever download and install add-on software. These effects will reach a lot more people this way and that is great.\n\nThanks for the great work! I hope the same happens with Liquid some day. :-)"
    author: "J\u00e9r\u00f4me Loisel"
  - subject: "Re: Mosfet All Star"
    date: 2002-01-01
    body: "Welcome Home, Herr General Feld Marshal Mosfet,\nNo progress in any field is paossible without the \n:\"dreamer or loner\".  \nYour outstanding code for KDE stands on it's own.\n\nI for one, who is nauseated whenever the mighty \nM$ raises it's head, am very happy to see you\nback.  I know now that the elegant KDE will not \nfade away.\n\nEin froehliches Neu Jahr...\n\nEd"
    author: "Edward Rataj"
  - subject: "Sweet"
    date: 2001-12-31
    body: "I don't think it takes much brains to realize this is pretty nice of Mosfet. Thanks man.\n\nDan"
    author: "Dan"
  - subject: "Thanks Mosfet"
    date: 2001-12-31
    body: "Thanks a lot Mosfet this is very cool of you.\n\nCraig"
    author: "Craig"
  - subject: "Great Xmas Gift!"
    date: 2001-12-31
    body: "Thanks Mosfet!  We love you."
    author: "KDE User"
  - subject: "Slideshow editor"
    date: 2001-12-31
    body: "All the Linux graphics programs I've seen make it a pain to initialize a slideshow from 'all the files in a directory'.\n\nThis very nice feature is the way irfanview (windows only, though) works by default.  Open any image, and you can use the spacebar/backspace keys to sequence through the directory.  Or open a thumbnail window and click the thumbs to seed the image window, after which the spacebar/backspace trick continues to work.\n\nThe biggest pain is using Konqueror to view images.    The thumbnail listing isn't TOO slow (once you've got .pics set up), but clicking on a thumbnail to 'open' it means that you need to regenerate the thumbnail list when you close it.\n\nWhich brings me to my Konqueror complaint...  Why must all the 'open' actions default to an embedded KPart?  I find myself right-clicking all the time to avoid this.  Is this configurable?  Bye MIME type?\n\nSorry to sound so bitchy.  Just some suggestions.."
    author: "Rob"
  - subject: "Re: Slideshow editor"
    date: 2001-12-31
    body: "Yes, it's configurable, yes it's by file/mime type.  Open up the config dialog, go to \"file browsing\", \"file associations\", find the file type you want, and then select the \"embedding\" tab.  Go to town :-)"
    author: "David Bishop"
  - subject: "Again, use PixiePlus :)"
    date: 2001-12-31
    body: "Left and right cursor keys go back and forth through the filelist, up and down keys go back and forth through all the images in the current browser folder (exactly what you described :) The fullscreen view mode also has buttons for this.\n\nThe slideshow feature can use either the filelist or all the images in the current folder.\n\nThumbnail viewing is also much better and faster.\n\nNo need to use Windows anymore ;-)"
    author: "Mosfet"
  - subject: "Re: Again, use PixiePlus :)"
    date: 2001-12-31
    body: "PixiePlus is very fast, nice and stable :) so I don't know why you've chosen '0.1' as release version?\nThe only thing I miss in PixiePlus is right click on image and 'Edit with...(for example Gimp)' option (hm, wishful thinking maybe?)."
    author: "antialias"
  - subject: "Re: Again, use PixiePlus :)"
    date: 2002-01-01
    body: "Why not use the KDE functions for that in KRun or KOpenWith like in Konqueror ?\n\nRalf"
    author: "Ralf Nolden"
  - subject: "Re: Again, use PixiePlus :)"
    date: 2002-01-01
    body: "Yes, makes sense, especially since I already use KRun for executing non-image files."
    author: "Mosfet"
  - subject: "Re: Again, use PixiePlus :)"
    date: 2002-01-01
    body: "After I have clicked on an image in Windows (with ACDSee installed) I can slideshow through all images in the folder/directory with the mouse scrollwheel (or the scrollwheel on my new Logitech keyboard).\nNo need to use the thumbnail browser since it loads the full images doublequick."
    author: "reihal"
  - subject: "gqview does this"
    date: 2001-12-31
    body: "It's a gtk program, not Qt or KDE, but gqview makes it pretty easy to do a slideshow.  Two menu clicks or just type \"sv\" while viewing a picture, and it starts a fullscreen slideshow of the current directory.  If you mean an interactive slideshow, you can go into fullscreen mode with \"v\" and left or right click to go forward or back (or arrow up/down.)  It preloads images in whichever direction you're browsing to minimize rendering time (which is slowed down a bit when you use their best quality scaling method to stretch images to fill the screen, as I do.)\n\nPixiePlus looks closer to the comfortable ACDSee mode of image browsing, which I miss a lot, but gqview is quite fast and nice to use, and has a \"find similar\" mode no other Linux image browser seems to have (ACDSee didn't either last I checked.)  gqview also just went 1.0 and is pretty stable.  \n\nCurrently I have to patch each new version to make multiple image selection in browse mode work the way I was used to in ACDSee, but sooner or later my patches will get accepted ;)\n\nI think much of gqview and PixiePlus's functionality may also be found in XnView, which is crossplatform and a little ugly in its X incarnation but works fine and is very mature."
    author: "raindog"
  - subject: "Re: gqview does this"
    date: 2002-01-01
    body: "Similiar image finding will be in 0.2, due next week. It is based on findimagedupe's excellent algorithm and uses a persistant database. It's quite fast :)"
    author: "Mosfet"
  - subject: "Re: gqview does this"
    date: 2002-01-01
    body: "Thanks, looking forward to trying it out.  It'll be nice to see an implementation of my algorithm that doesn't take 18 hours :)  (The gqview guys used their own algorithm, it's fast enough but the results are a little different...)"
    author: "raindog"
  - subject: "Howdy! :)"
    date: 2002-01-01
    body: "Hiya, nice to see you here! I found the algorithm in findimagedupes more accurate. It's really rather clever. The algorithm I used is virtually identical to yours. As a matter of fact, the reason I started porting effects from ImageMagick was to get the necessary blur, normalize, and equalize effects as fast as possible ;-) You'd think it would be slower than averaging blocks of pixels like GQView, but actually it's not. In PixiePlus it's way fast :)\n\nFindimagedupes performs quite well with a small amount of images, but really suffers from a small hash table size. For 47 images PixiePlus takes 11 seconds while findimagedupes takes 20. That's really not bad since it's compiled C++ vs. interpreted code. But when you increase the number to 5,049 images PixiePlus only takes 23 minutes while findimagedupes takes 1hr 16min.\n\nPixiePlus also uses a persitant database, but it's not compatible with findimagedupe's. It's a binary database that also includes timestamps so you know if an image is modified and needs to get a new fingerprint. I could write an import and export to findimagedupes if you think it would be useful. I certainly can see value in a commandline utitlity for this as well as Pixie :)\n\nHere's a comparison of the different methods I wrote for the Pixie docs. I also did a quicky screenshot of the comparison results which uses a nifty tree view (double click on the image to view, right click for a menu). It's at http://www.mosfet.org/compare.png. Of course there is also a 2 step progress dialog for generating/loading fingerprints and comparing ;-)\n\n--------------------------\n\n Notes on simularity comparison algoritm and performance\n\n   Much attention was spent on making finding similiar images perform as\n   fast as possible while still being accurate. Compared with other Unix\n   offerings PixiePlus performs quite well in this aspect.\n\nThere are two main algorithms for finding similiar images. One is used\n   by GQView and ShowImg. It is based on averaging blocks of color\n   channels and comparing the result. This is the obvious algorithm and\n   the one I was originally going to adopt. In GQView it performs with\n   acceptable speed, in ShowImg it does not. I'm not sure why this is\n   since the code is pretty much directly copied from GQView. Either way,\n   it tends to miss some matches found in the other available algorithm.\n\n   The other algorithm isn't based on colors at all but identifying the\n   general patterns in the image. This is the method used by the Perl\n   utility findimagedupes and is the method I adopted for Pixie. What it\n   does is sample an image to a standard size, apply a couple effects to\n   get rid of abnormalities, scale it down again, then convert it into a\nstring of bits suitable for use as a thumbprint. While it sounds like\n   it would take much longer than the above method, in reality it\n   doesn't, and it finds similiar images the other algorithm misses.\n\n   Like findimagedupes and unlike GQView and ShowImg a persistent\n   database of fingerprints is used so you don't have to regenerate\n   fingerprints or calculate image blocks each time you compare images. A\n   binary database is used in order to avoid parsing ASCII\n   representations of the binary thumbprint. Unlike findimagedupes the\n   database also contains timestamps so it can tell if an image has been\n   modified. A large hash table is used and should perform well for\n   folders up to 6,000 images. After that expect performance degragation.\n   Findimagedupes especially suffers from this. On my test machine,\n   (AMDK6 450MHZ/128M), initially comparing 47 large images takes 11 sec\n   on Pixie and 20 sec using findimagedupes. This is really good speed\n   for findimagedupes considering it's in Perl. But when the number of\n   images is increased to 5,049 findimagedupes slows down to 1hr 16min\n   while PixiePlus only takes 23min 14 sec (the fastest of the bunch)."
    author: "Mosfet"
  - subject: "Re: Howdy! :)"
    date: 2002-01-01
    body: "Oh, I don't mind incompatibility if it means PixiePlus can compare 5000+ images in 23 minutes.  Mine was originally meant as a lazy reference implementation and it turned out to be good enough for my mostly small image collections.  Maybe with QT3 the local database file won't even matter anymore if its built in database functionality is worth anything.\n\nBut I may try to do a C++ command line version that uses your database format; someone emailed me C++ compare code using my format (built it but never benchmarked it) so it'd really be a matter of teaching it about your database format and tying in Magick++ support to build the DB.  I've had complaints about my kludgy attempts at providing an interface for GUI programs but now it won't be necessary.  Thanks again.  Neat looking result interface by the way."
    author: "raindog"
  - subject: "Mosfet's _the_ New year Gift :)"
    date: 2001-12-31
    body: "First a Very Happy and Bugfree, Stable, and i18n_ed New year to you all :)\n\nWelcome Mosfet, I know you were not gone from KDE but just took a break; like every developer you are also kept at high esteem by the users. Thanks."
    author: "Asif Ali Rizwaan"
  - subject: "Disappointed to see this..."
    date: 2002-01-02
    body: "I'm very disappointed to see this on the dot.  I'm hoping the only reason this was posted is for lack of better news.  This story is really quite thin, at best it's a quickie - don't people contribute code all the time?  Should every new contribution be accompained by a dot story?  My gut feeling is no, of course not, then the dot would be kde-cvs.  It seems to me the case here is that because it came from Mosfet, it's news, and that's very sad to me.\n\nHe's a good coder and has made very worthwhile contributions to KDE, I don't disagree.  I'll even be more than happy to say that I'm glad you're making contributions again, Mosfet.\n\nHowever, his contributions are certainly no different than any of the other MANY individuals who dedicate their time and energy to the project.  I'm much more interested to read Tink's excellent 'People of KDE' series about the contributors who you DON'T hear about, you know, the ones who aren't in the news all the time because they are mature adults.  Mosfet has long since had his fair share of KDE publicity.\n\nThis whole Mosfet-KDE thing is just turning into an endless soap opera, and I honestly feel keeping it alive is bad for the dot and the KDE project.  I think the risks (i.e. pissing off other contributors who feel slighted because Mosfet gets a lot of very undeserved attention, tainting KDE's reputation as well as OSS as a whole with the negative publicity) far outweigh any benefits (i.e. get a news story up for more traffic, more publicity is always good).  I hate to even feed into it by posting this reply, but I feel it's time to speak up so the editors hear some more opinions.\n\nPlease, please, keep the dot as clean and professional as it usually is, and stop with the Mosfet stories.  If there's merit in them, fine, I'm by no means trying to say he should never be mentioned again.  Instead, in the future, please use the same measure that any other story in the queue receives.\n\nAnyway.  Just MHO.  Flame away, everyone, I'm *very* interested in responses."
    author: "Bill Soudan"
  - subject: "It's easy."
    date: 2002-01-02
    body: "Write stories.  Submit them.  The dot is what you make it.\n\nMosfet was nice enough to write us with details of his work.  It was found interesting and posted to the dot.\n\nWhere are those individuals who are being slighted?  *Please* stand up.  Submit your news to the dot.  Write a story, write us.  You will get just as much consideration as Mosfet did when he submitted this news.\n\n-Anonymous Editor."
    author: "Anonymous Ed"
  - subject: "I think it's a little more difficult..."
    date: 2002-01-02
    body: "Thanks for the response, anonymous editor ;)\n\nI still feel the article was a bit thin, and in the past, it seems this type of news would have been a quickie rather than a whole story.  I'll lean to your judgement here since after all, you're the editor and I'm not.\n\nIn any case, quickie or not, I think it should have been posted without the soap opera twist (e.g. headline reads 'ImageMagik effect code ported to KDE by Mosfet' & kill the lead-in, rather than the current headline & lead-in which *clearly* emphasizes Mosfet's on-off relationship with KDE rather than his actual contribution).  Throw the mention about his 'rocky history' at the end if you absolutely must (though I personally would leave it out).\n\nWhether or not there are individuals who feel slighted, I don't know, I'm only bringing it up as a possibility.  I feel in this case you may be wielding your editorial power to the detriment of the community, because I'm fairly certain there are more than a few important KDE contributors who aren't happy with Mosfet because of his games.  You're doing them a disservice by encouraging him.\n\nThis is a fine line that I think you as editors have been walking for a while, it's really too bad Mosfet has put volunteer editors in this position.  Then again, it's really too bad what Mosfet has done in the past to many of the other KDE volunteers and the reputation of KDE as a whole.  Anyway.  The dot has been walking down one side of the line for quite some time now, posting his news because it's news.   Maybe it's time to walk down the other: don't post the news because quite frankly he doesn't need any more encouragement, or at least post his news more objectively.\n\nMosfet's contributions may be worthy news - his attention-grabbing games are not.  And then in my very biased opinion, his games make his contributions much less worthy of publicity.  I personally would drop his articles on the floor until he demonstrated that he can contribute without all the childish noise, out of respect for the countless other KDE volunteers."
    author: "Bill Soudan"
  - subject: "Re: It's easy."
    date: 2002-01-03
    body: "Why the need for anonymity?"
    author: "Neil Stevens"
  - subject: "Re: It's easy."
    date: 2002-01-06
    body: "So I submitted a story yesterday, but it seems it was dropped. :-("
    author: "someone"
  - subject: "Re: It's easy."
    date: 2002-01-07
    body: "Actually, we have been in contact with Ivan, and since we just heard from him, we will probably make this one a quickie soon.\n\n-N."
    author: "Navindra Umanee"
  - subject: "This is news?"
    date: 2002-01-02
    body: "Why don't you hail all the people (hundreds or thousands) that do actual day to day work on KDE that don't have little fits and go home.  Oh goody, so now we will have even more half done broken code by Mosfet in CVS... Maybe if we're lucky he'll commit something ten minutes before kde3 is released and break it."
    author: "spaz"
  - subject: "Re: This is news?"
    date: 2002-01-02
    body: "If I remember correctly, Mosfet's CVS account was disabled. And he didn't commit this changed himself."
    author: "anonymous"
  - subject: "Re: This is news?"
    date: 2002-01-02
    body: "trolls please leave!"
    author: "trollbuster"
  - subject: "Jealous people"
    date: 2002-01-02
    body: "While most of the comments have been quite positive, it's obvious a few people are jealous of the attention I get. News about me and what I'm doing does get carried a lot, and there is a reason for that. I write graphics code, and people have a lot of interest in that. If people didn't care about what I was doing or writing, no one would carry news about it. I could do whatever I want and people simply wouldn't care. But they do, and I thank the people who have kept track of me and lent their support.\n\nThis is certainly a worthy story. Even not considering I am a fairly visible Linux/Unix developer, the addition of 20 effects to KDE3 would be considered a good story anyways. People are always interested in new graphics stuff, and are always asking about what's going to be new in KDE3. This is good news for both programs that deal with images and widget styles, always a subject people are interested in. Add to the fact that my departure from the KDE team was widely covered and that this is an initial gesture of goodwill on my part and what I hope will be a better relationship - that's even more good news!\n\nPeople complaining about posting this story are totally unjustified and I can't help thinking are full of \"sour grapes\". They don't think anything I do should be covered, no matter how good or cool it is. This is obvious from people like the above saying it's bad to publicize anything about me. Nevermind that it adds cool stuff for KDE and is done in the spirit of cooperation. \n\nThere is a simple solution for these people. If you don't like news about me don't read it. And be ready to not read a lot of news because I am back and working on a lot of interesting new things! ;-) If you feel some other developments should be covered, then submit those! But don't be jealous if people are interested in what I'm doing as well."
    author: "Mosfet"
  - subject: "Re: Jealous people"
    date: 2002-01-02
    body: "Don't let them get you down! I agree, they are just being sour, and as someone above said, stories get on this page when they are interesting enough and they are submitted!\n\nAnyway, now it is on Slashdot, so it obviously is of interest to enough people!"
    author: "Graham"
  - subject: "Re: Jealous people"
    date: 2004-01-08
    body: "jelous people tend to have very insecure minds and are secretly unhappy. i think whats even more worrying is not only can they admit the truth to others, but they cant admit the truth to themselfs. they hate people who find success in their life because they feel they have to compare themselfes to that person. many of my friends hate skinny people, but deep down want to look like this. they may also be really defensive in a situation and can go to the extreme of making others their poccessions. i hope if there is anyone insecure who reads this message knows that knowone hates them , but would like to help.\nadmitting your in the wrong can be the hardest thing to do, but it can save you from being insecure about everything. if you confront your fears then people will notice this and be happy that you have secured the inner you."
    author: "angela"
  - subject: "Re: Jealous people"
    date: 2004-10-31
    body: "your so true about what you write are you a extra jealous \nperson just kidding  hahaaha thanks for the words it's so \ntrue and thanks for letting them know about there jealous\n\nP.S. I got a question what if you don't have nothing and \nthey still act like that peace god bless sly"
    author: "SLY "
  - subject: "Re: Jealous people"
    date: 2005-07-11
    body: "Yes, i definently agree with you are saying.It is so hard sometimes dealling with jealous people.They can get very rude sometimes and they can even embrarass you in situations....\nLet me know how you deal with them please.\n"
    author: "kon"
  - subject: "Re: Jealous people"
    date: 2008-10-18
    body: "You sound like one of them trying to get insight into how people think in order to..."
    author: "S Lane"
  - subject: "Re: Jealous people"
    date: 2006-02-13
    body: "I'm definitely one of those people, who doesn't have much! I live in my mom and dad's home, at fourty-three, and still there are people, who hate the fact, that I could possibly achieve something. And some of the people, of whom I speak, are in my family. I don't understand it, but that's the way it is. If they think, for a minute, that I could get somewhere, there's a problem. And it has always been that way. They can excell, but nobody else can. And eventhough I try to ignore it, it gets to me. My health, has always been poor, which is why I'm in my current situation. But regardless, I refuse to be beaten. I do everything I can, to make things better, for myself. And again, they don't like it. So all I can say, is you're not alone. You don't have to have much, for people to hate you. It's sad, but true! We live in a sick World, with a lot of people, who have nothing better to do, than tear others down. So when you find yourself, in this situation, no that you're worthy, no matter what!    "
    author: "Michael D. Robinson"
  - subject: "Re: Jealous people"
    date: 2006-02-14
    body: "Good point bro. It's tough because if people see a weaker animal rising up they feel their well being is in danger. Cause if a weaker animal is stonger they must be incredibly weak. It's sick but that's the way of animals which is what those people are, animals. They are the weakest of the human race. A human should triumph others abilities and co exist with them. If they consider themselves sophisticated well mannered people then they would get off all fours. Keep living brother. One day they'll be begging you for a chance to taste the fruit you've made. Live on."
    author: "Brother in life"
  - subject: "Re: Jealous people"
    date: 2006-10-13
    body: "I have experienced jealousies in the worst ways. My own brother threatened to take my life and best friends tried to sabotage my personality in order to feel more important. It took me a long time to understand 'the lesson' for me which originated in my family and that was to see why I reacted so badly to jealousies. I agree people who have to respond to others from those feelings are weak within themselves. They attack because they see something in your 'self' that they can't meet. It is basically a primitive emotion - but after many years of struggling with that and constantly attracting those people  I finally realised why I struggle with them. \"The lesson for me has been in 'my responses' albeit anger, because of my family experiences. This is the spiritual approach, but Ihope that by learning from the lesson in application to yourself, you might be able to change the ennergies that keep us trapped from moving forward. Jelous people will always be around - to be more evolved, or stand on two feet, you have to learn strategies to keep yourself distant. Inside and outside... hope this helps\nMove forward humans!"
    author: "Sasha"
  - subject: "Re: Jealous people"
    date: 2007-08-21
    body: "It could be that your best friends where trying to control you, that's why they tried to damage your personality, if the personality of people is healthy (with self esteem, lack of neurosis, social skills, assertiveness, making your own luck, persistable, being honest, responsible and dependable etc) people can do a lot."
    author: "visitor"
  - subject: "Re: Jealous people"
    date: 2007-08-21
    body: "I would like to add a nit more. They wanted to control you out of fear of losing you or you taking opportunities that they could win (e.g. girls you both know or jobs) or ultimately fearing that you will become powerful and rich (then, they would feel worse with their selves because they would compare themselves to you). If you think about it there are many reasons people would want to harm you or stop you really."
    author: "visitor"
  - subject: "Re: Jealous people"
    date: 2008-10-18
    body: "Very deep. I like what you say. It's like the alpha and when the alpha realizes it is no longer the alpha."
    author: "S Lane"
  - subject: "Re: Jealous people"
    date: 2007-09-06
    body: "I will pray for you Michael.  I will ask the Lord God and Jesus Christ to improve your life.  I understand what you are going through because I have had my own struggles with jealousy, whether someone was jealousy of me or I was jealous of them.  I have actually considered suicide many times, but something stops me from doing it.  It is very difficult to maintain happiness when others hate you, and yes this is a sick world with tons of losers, do not worry, in the bible it says that GOD will repay.  The Lord is extremely loving, but punishments are also part of GOD's discipline and uncomfortable situations will arise in anyone's life who tries to bring other people down.  The lord knows everything about everyone(forgive my ignorance if you already know this) and the bad people on this planet will slip somewhere along the timeline.  You can count on it.  In case you ever feel jealous of someone else, do the best you can to move on, it certainly takes a greater person to not stay preoccupied with jealousy because that can improve your character and heart.  Furthermore, what goes around comes around, I have seen it happen to people because those that brought misery to my life were punished, and then I kept succeeding over and over again and my happiness made them feel miserable, and the entire time I knew God was helping me, what an awesome feeling it is to know that jealous people who are being defeated cannot do anything about the situation when God is pushing me forward, I really enjoy them slipping  through the cracks when I see it.  Last but not least, do not give in, even if someone tries to murder you in a hate crime, if someone kills you then he/she will suffer in hell.     "
    author: "Jon T. Drillias"
  - subject: "Re: Jealous people"
    date: 2007-09-28
    body: "Jon, you have impressed me as a very loving person after having read your post. I pray you find all the beauty and magic and love to make your life an incredible journey!\nSemper Fi\nDax"
    author: "Dax Michaels"
  - subject: "Re: Jealous people"
    date: 2008-10-18
    body: "Jon you are very wise. But you also strike me as nieve you have never really known a drug dealer or addict have you? You do not understand what happens to some people you say are going to hell. They are not evil but are lost there is no helping them they have to help themselves. This is where people like you and me run into a problem because we see this and want to help them but there is no helping them and if you try they will hurt you. Just continue on your way and leave them behind and hope that they find their path some day. Have you ever heard the saying only the good die young? Many of these people will take many years to finally find salvation."
    author: "S Lane"
  - subject: "Re: Jealous people"
    date: 2005-05-26
    body: "you are a very smart girl. :)\ngot any more tips on jealous people?"
    author: "Jodie"
  - subject: "Re: Jealous people"
    date: 2006-07-02
    body: "Hello, I live in a bad neighborhood, and the people in the area are so jealous of me because I'm a positive person, and I have positive friends.  I'm in college, I made 2 B's and a A. I just had my baby on June 25th and the whole time I was pregnant they nitpicked at me by driving by rolling their eyes at me, giving my friends mean looks that came over, and all my friends and family suggest that I move, but I've been living here because I don't have to pay rent because it's based on income, and I'm just in school until August.  But it's so bad over here that I can't leave my apartment without being scared, because someone is always outside giving me mean looks."
    author: "Mishleen Ross"
  - subject: "Re: Jealous people"
    date: 2006-07-02
    body: "Hello, I live in a bad neighborhood, and the people in the area are so jealous of me because I'm a positive person, and I have positive friends.  I'm in college, I made 2 B's and a A. I just had my baby on June 25th and the whole time I was pregnant they nitpicked at me by driving by rolling their eyes at me, giving my friends mean looks that came over, and all my friends and family suggest that I move, but I've been living here because I don't have to pay rent because it's based on income, and I'm just in school until August.  But it's so bad over here that I can't leave my apartment without being scared, because someone is always outside giving me mean looks."
    author: "Mishleen Ross"
  - subject: "Re: Jealous people"
    date: 2006-08-22
    body: "I think my best friend is jealous. I graduated from High school this summer, and she's still a junior. She would tell me ure soo lucky. We work together and at work she's been acting like mean and rude. Shes been acting weird lately. She doesnt call me that much, and she doesnt want to hang out. She gives me attitude. I haven't talked to her bacause I trully don't want to stop talikg to her, cuz she's the only friend I have. i also gonna be her baby's Godmother. And I don't want us to stop talking. Why do u think she acts this way?"
    author: "Samantha"
  - subject: "Re: Jealous people"
    date: 2006-08-23
    body: "I totally understand you. Take a moment to look at it this way. If you are a positive person, you are special and rare in the world these days. i learned people hate positivity, they claim it's \"not keeping it real\". when i'm having a challenging day at work and seem a bit cranky, that's when eveyone wants to talk to me and bath in stories of my mishaps. when i'm bright eyed and happy, only other naturally positve people will respond to me, which will be very few.\nmay be your friend is too negative to be your friend anymore. seek friendships that you are very derserving of, and that is positive friendships. and don't let cranky people change you!\n\nlast but not least. how ever your friend is behaving, probably has nothing to do with you OR she doesn't have the courage to tell you if there was something that happened between you 2 that upset her.\nask her: Is there something you want to talk to me about? Did I upset you? "
    author: "alice"
  - subject: "Re: Jealous people"
    date: 2006-10-23
    body: "I thort that jealous friends would do no harm and they would grow in self confidance a stop being jealous.  unfortunatly this is not always the case and one of my (ex)friends has made me life as hard as possible and at one point sent me into depression! she's still not happy as what she intended (to break my boyfriend and me up) didnt happen but she has managed to make my friends not like me for no apparent reason.  When i asked why she did this she has never been able to give me a reason but from running accross the room when she see's me talkin to a boy (when i was single) to copying my hair, make-up, shoes, clothes, the way i talk, walk, carry myself, she has made me hate her."
    author: "ash"
  - subject: "Re: Jealous people"
    date: 2007-06-07
    body: "yeah you are right,\ni have a friend that is jealous of me.we have a music group,and i have the best,\nwhen we sing the chorus of the song and then she sings it louder than me,lots of people say that she's jealous of me,and thats not all,everytime when we go shopping everything i buy she buy too,when i buy a film we pick an actress and i pick the best and my sis picks another good one,so there be's two more girls  left and she picks one girl.But when we are in college she acts acts like the person who i picked behind my back.    "
    author: "clarice"
  - subject: "Re: Jealous people"
    date: 2006-11-06
    body: "i have a close group of friends in my sophomore year in highschool. we always hang out and sometimes some of the girls are shady to me, for what seems like no reason at all. I assume it's just because i annoy them? but maybe its because they are jealous? i am smart and pretty . it makes me feel crappy like no one likes me and they all talk about me or something. what should i do? i cant just ignore one or two because that affects hanging out with everyone. thanks!"
    author: "kaela"
  - subject: "Re: Jealous people"
    date: 2006-12-25
    body: "That happened to me at the beginning of this year. There's either something about you that makes them uber jealous or they're just nasty people at heart. Either way ditch them, they'r not worth losing your sleep over. \nI used to hang out with a group of friends too, til this year it was revealed that 2 in particular, have always hated me, that i annoyed them and 'wouldn't leave them alone' even though the only times i hung out with them or talked to them were during weekdays at ONLY at school! I decided to leave the whole group, eventhough that meant losing about 4 friends in one go, but soon i found a much better group of friends. And i've learnt so much just from that dumb experience. \nGood luck finding new better friends! :)"
    author: "Anon."
  - subject: "Re: Jealous people"
    date: 2007-03-18
    body: "There is one thing i look for in ppl before I want to consider them as friendship material, that is jealous personalities. If you have a feeling that they are jealous of you, than they really are. I met a couple ppl that i never tried to become 'friends' with b/c i just felt that they are always trying to compete with me. I'm a very modest person and I don't like to rub things in. Even if I no I am better at something. I'll try to bring myself to their level. But it always seem they take that as an advantage and try to put me down even more since they have a chance to. For ex: I'm a medical student that studies all the time, when I say I have to study all weekend..this one girl said she never have to study. (i just blew it off and didn't respond to her, but in my mind I was like she just graduate high school..how can she compare herself to me?)And not only that she's always trying to do what i do, like wearing the same outfit, hairstyle, and apply to med school too!! It got soooo annoying that I just ignore this person."
    author: "Christy"
  - subject: "Re: Jealous people"
    date: 2007-03-19
    body: "I was friends with this girl for 8 years. She is a beautiful person on the outside but as I got to know her better on the inside she was very ugly. She was jelouse that I graduated college before her ( that's because I didn't party as much as she did). Last year I've told her that my relationship with my boyfriend at the time, now fiance was getting serious so she thought of something like \"well my friend likes you and if you dump your boyfriend he will treat you like a princess\" I told her are u out of your mind! I am happy and my boyfriend  he is treating me like a princess! I got engaged and she asked me if it was a big stone and I told her that it is a beautiful ring. When she saw it she looked at it and truned her head and siad I don't like round cut, princess cut is more expensive and is this white gold? cuz I like yeallow gold! After bad mouthing my ring she started saying that she got expensive fur coats and rolex watches from her current boyfriend and each was $18,000.00 ... I told her I'm very very happy for you and she kept braging and braging about herself( the thing is she doesn't not have all of those things, it is all talk).......what made me end my relationship with her was that she told my fiance that the ring that we picked out together was not good enough and that I deserve a better one, he told her I let her pick out whatever she wanted and No Diamond will ever show the love that I have for her beacuse it is priceless! She shut her mouth and didn't say anything all night long.....\nI decided to just stop talking to her, I don't need anyone like her in my life.\n"
    author: "rochell"
  - subject: "Re: Jealous people"
    date: 2007-03-19
    body: "I was friends with this girl for 8 years. She is a beautiful person on the outside but as I got to know her better on the inside she was very ugly. She was jelouse that I graduated college before her ( that's because I didn't party as much as she did). Last year I've told her that my relationship with my boyfriend at the time, now fiance was getting serious so she thought of something like \"well my friend likes you and if you dump your boyfriend he will treat you like a princess\" I told her are u out of your mind! I am happy and my boyfriend  he is treating me like a princess! I got engaged and she asked me if it was a big stone and I told her that it is a beautiful ring. When she saw it she looked at it and truned her head and siad I don't like round cut, princess cut is more expensive and is this white gold? cuz I like yeallow gold! After bad mouthing my ring she started saying that she got expensive fur coats and rolex watches from her current boyfriend and each was $18,000.00 ... I told her I'm very very happy for you and she kept braging and braging about herself( the thing is she doesn't not have all of those things, it is all talk).......what made me end my relationship with her was that she told my fiance that the ring that we picked out together was not good enough and that I deserve a better one, he told her I let her pick out whatever she wanted and No Diamond will ever show the love that I have for her beacuse it is priceless! She shut her mouth and didn't say anything all night long.....\nI decided to just stop talking to her, I don't need anyone like her in my life.\n"
    author: "rochell"
  - subject: "Re: Jealous people"
    date: 2007-12-02
    body: "I know I am a guy, and the situation might slightly differ, but this post is so wise. Wish I knew this advice before learning the hard way. \n\nEven better, are those people who would seem the least jealous (big house, attractive spouse, nice car, etc) still seem hateful when you have something they don't (positive attitude, good with people, etc). "
    author: "RR"
  - subject: "Re: Jealous people"
    date: 2007-05-24
    body: "Everyone hates me and I've never put anyone down in my life as far as I can remember.  People are going to great lengths to fuck with me and I do nothing but love them.  The world tends to suck like that, huh?  Consider yourself lucky if you just have had few jealous/insecure encounters with people.  I want to move away from my home right now, it's unhealthy here.  good luck."
    author: "df"
  - subject: "Re: Jealous people"
    date: 2008-01-06
    body: "Please try to hang in there, not everyone is that way. One good friend is better than a bunch of people who suck! If it will help, I will keep you in my prayers.\ncw"
    author: "cwilliams"
  - subject: "Re: Jealous people"
    date: 2007-05-24
    body: "Everyone hates me and I've never put anyone down in my life as far as I can remember. People are going to great lengths to fuck with me and I do nothing but love them. The world tends to suck like that, huh? Consider yourself lucky if you just have had few jealous/insecure encounters with people. I want to move away from my home right now, it's unhealthy here.  I am kind, i can or atleast try to relate to even the craziest a-holes out there, i am ambitious, i don't judge unless i am judged, and i am positive to people.  but everyone wants to be as negative as possible to me."
    author: "df"
  - subject: "Re: Jealous people"
    date: 2007-05-29
    body: "I totally agree that jealous people are insecure but it's so hard when they succeed in trying to bring you down.  For example, I am a pretty, sincere and smart girl who has just graduated with a first, i also had an amazing boyfriend who recently broke up with me but both him and i didnt understand why.  His housemates or \"friends\" were always jealous of him and then our relationship (one of his friend's resented me for not liking him), they have bullied me sayin that im a liar and that no one likes me, and convinced him he shouldnt be with me.  I am so sad that i lost a beautiful sincere man because of people that are supposed to be his friends and were jealous of our happiness because they couldnt find the same.  im truly heartbroken."
    author: "Positive Person"
  - subject: "Re: Jealous people"
    date: 2007-08-01
    body: "I have sent you a blessing and the hope that your bloke comes in light of his senses. you know love, I feel more sorry for him, god his freinds, with friends who would break up your home your friendship, god who needs enemies. I will send you a blessing to heal, but I can't help but think he is the one who is going to need it. You know I think both you and your man have been conned on this one. I hope he realises and I hope he choses to come back to you and get rid of them."
    author: "vicky"
  - subject: "Re: Jealous people"
    date: 2007-06-30
    body: "You are so right. Jealous people tend to have insecure minds and are secretly unhappy.Talk about misery loving company. I've been through many periods of people being jealous of me from toxic friends, my siblings, and hostile co-workers. I think the best way to deal with people who are like that is by maintaining a positive attitude and keeping in mind that your life does not depend on their liking you. There are people out here who will except you just the way you are. I am one for instance. There are many others. Just be happy! Enjoy life!"
    author: "Vanetta White"
  - subject: "Re: Jealous people"
    date: 2008-10-18
    body: "You are very wise I like what you wrote and hope you continue to spread positivity to all that you meet and know. Have you ever heard of that movie pay it forward? I think that possitive attitudes and negative attitudes are like that movie if you continue to be negative you are spreading negativity across the world if you are positive you spread positivity across the world pay it forward spread the word of positivity. Anyone can do it tell the girl at the drive through window her hair looks good or what every. Pay positivity forward it is contagious and makes people feel good about being here. Keep up the good work!"
    author: "S Lane"
  - subject: "Re: Jealous people"
    date: 2007-09-26
    body: "I have never met a jealous person as my husbands neice.  She is always trying to make my husband hate me and go against me.  She doesnt like the fact that he takes me out to dinner, he takes care of our son or he stands up for my rights within his own family.  She always picks an arguement, fight and turns everyone against me.  In the begining i was so naive and I thought she was a friend but later I learned that she was building traps along the way.  I had a hgue family dinner and she had her mom call me and pick a fight about her daughter out of the blue!  my dinner turned out bad and I ended up having a headache that night.  She is soooooooooooooooooooooooooooooooo jealous may God save me!!"
    author: "ZM"
  - subject: "Re: Jealous people"
    date: 2007-09-26
    body: "Hi ZM.  So that your heart does not become consumed with bitterness and you become hateful and angry and eventually depressed and miserable, be sure to forgive her in your heart right away.  If you have become angry, ask God to forgive you.  Now here is what is difficult to understand... ask the Lord to forgive her for her actions (name them specifically).  Then ask the Lord to heal you and to give you compassion for her.  Now, loving her and having compassion for her does not mean that you have to make effort to show her that you love her or always making yourself deal with her presence.  Do not force an interaction with her and keep your interactions with her minimal.  There are obviously issues within her soul that were there before you were around.  Also, be careful not to keep talking about her with other people.  If you find that you often discuss the situation and her with others, then you are hurt and offended.  Unfortunately, this is what difficult people can do to us.  They bring on the offense, and we are the ones who suffer.  That is why you must forgive her IN YOUR HEART.  Forgiving is what God commands us to do.  Read Matthew 18:7-35.  This will encourage you to forgive and not curse the people that offend you.  Everyone has issues in the soul (mind, will, and emotions)and everyone needs help in some way or another.\n\nI am not sure of your belief, but please do not blow off my message because I mention God and the Holy Scriptures.  Many Christians see offenses as \"the bait of Satan\".  Offenses weigh you down in every way.\n\nSo this is what I do.\n\nLord, I forgive (offender) for (whatever was said or done).  God would you please forgive(offender) for (whatever was said or done).  Lord, please forgive me for holding (whatever the offense is) in my heart.  Lord, also please forgive me for judging (offender's) heart.  You are the righteous judge and you know what is going on within them.  Lord, give me compassion for (offender) and help me to love them.  Lord, please heal me from this.\n\nOnce you pray, trust God.  Leave it all in His hands.  Do not expect the person to apologize(although they eventually may sometime much later) nor expect them to change (although they eventually may sometime later).\n\nNEVER EXPECT PEOPLE TO APOLOGIZE BEFORE YOU FORGIVE THEM.  THEY MAY NEVER APOLOGIZE TO YOU.  However, you can release them.\n\nYou see Jesus came to redeem all that was lost.  Unfortunately, because of the ugly nature of people, you can see why we need a Savior.\n\nTake Care."
    author: "CJ"
  - subject: "JEALOUSY IS SUCH AN EVIL THING,"
    date: 2007-10-13
    body: "people who are jealous of others are insecure and hate everyone who is better than them. You are so rigth about it, they can not admitt they are jealous. Jealous people hate people who are more successful because they feel like they have to compare themselves to that person, and they do compare themselves to successful people, they feel very angry and upset about everything in their life and wonder why they aren't like that, they are so unhappy.  \n\njealous people should change that negative view of theirs. they need to change it into positivity, translate it into inspiration, they need to realise this world is small, being jealous is stupid and foolist. \nHowever, seriously wouldn't know how to help them over come their jealousy, think most people go see a therapist and read alot of those self help books. thats why they were written. \n\njealous people can't admitt what they doing is wrong because truth hurts. You know what, they actually blame people who are more successful for their misery, its like its our fault. They need to as themselve is acting the way they do going to make their life better? No it just going to make them more shinking into new lows. \n\nPeople who are jealous of others tend to hang around in groups, so they can pick on others, blame other and feel not guilty, because everyone of them thinks the same way.\n\nSo please stop being jealous of me, just because i'm successful!!!!!\n if I was up myself I would say this to their face, but then I be just as bad as them, but I'm not successful.\n\nremember jealousy is wrong, its not the right thing to do. \n\n\n"
    author: "InvisibleSecret"
  - subject: "Re: JEALOUSY IS SUCH AN EVIL THING,"
    date: 2008-08-27
    body: "Hi I found it on line and I am forwarding it to you.  Maybe you can reply now stating that she is crazy and jealous of everything and everyone.\n\nTina"
    author: "aldina"
  - subject: "Re: Jealous people"
    date: 2008-05-14
    body: "How can I tell if my mother in law is jealous of me, or if she really doesn't like me? She constantly makes cutting remarks about me to others in front of me. Recently on mothers day she said to my friend who was dining with us after I had spoken about my C-section experience that it was my fault that my daughter was breech because I didn't go for any walks while I was pregnant. Of course this is not true. It runs in our family. She had remembered a conversation almost 1 year ago about me telling her that I didn't go for walks while I was pregnant. I couldn't even remember at the moment, but she did. A few months earlier she told this same friend right in front of me that the reason my daughter feet were faced outward was because I allowed her to sleep on her belly. This also is not true. My daughters feet were only like that for a couple of months because she was breech. The doctor even told me. And another thing, I only allow my baby to sleep on her belly until she is sleeping deep, then I move her to her back. I have told her that. She accuses me of everything. There is more, o, so much more. My mind is exhausted.\n\nOh my. I could right a book... Do any of you feel like putting some experiences together on jealous people, and publishing a book? "
    author: "Jenny"
  - subject: "Re: Jealous people"
    date: 2008-05-24
    body: "jealous people really suck! unfortunately they are everywhere and we need to learn how to live with them."
    author: "me"
  - subject: "Re: Jealous people"
    date: 2008-06-03
    body: "Oh my, after my face was fixed up I was normal again ( ok so apparently i look like Marilyn Monroe or something ) and suddenly the currents of jealousy in life hit me hard. I read about Marilyn's life and sadly she got the same treatment from many people. I guess all this hurt so much because my face had healed and I wanted to be accepted by people. I don't mind jealousy, but when people act like they are going to hit me or rip my head off I get scared. Mean comments don't scare me, but tall and angry people do. Ambitious people are the pitts especially, they believe they are only truly human when they have it all. It's really strange stuff. I am also cursed with this serene look on my face like nothing is wrong, even though I stress like nuts every day. I wish I could learn to enjoy the attention, but I'm just not sadistic enough to love making people upset...and no I'm not going to cum my degree, I feel bad about the future ( scared ) and the guy I like can't understand why I get so upset all the time. I'm too ashamed to admit I'm a weakling that can't handle people screaming at me and pulling demon faces at me. Best o Luck. x"
    author: "SILL"
  - subject: "Re: Jealous people"
    date: 2008-07-29
    body: "I was not aware of jealousy until I became a young adult. I am the type of person belive it or not who becomes extremely happy for the unbelievably simplest things in life . At the age of 16 I realised my friends began attacking me for simply no reason in particular. I believe it was brought about by the way other people viewed me. The thing is these haters would bad mouth me and they would follow everything i did (how i wear my clothes and even mimic my personality). Even some girls i notice are angry at me simply because I am not interested in them, nor would I go out of my way just to please. Another observation I made is that jealous people group together and try to take down someone that threatens them the most, and after they go back to hating one another. The fact that they are jealous does not bother me but it does when the try to embarass me in front of others. I am not the type of person to stand an argue but the fact that they try to embarass me, leaves me bothered and angry for not replying.  What should i do? I am not trying to compete with anyone. I am exhausted with being dealing with jealous people I am 22 and all I want to do is enjoy the rest of my youth. Please help!!!!"
    author: "humble"
  - subject: "Re: Jealous people"
    date: 2008-08-01
    body: "My dear,\n\nWe are in a world of cruelty. People just do not want to see other people laughing, or happy so they try by all possible meansto make you sad. You will see that when you do something good they will try to make you unsure. Also you will see that sometimes they will even try to blame ysou or even provoke you. The best thing is to ignore them or if possible ask them if you have done something wrong. \n\nAlso try to not allow yourself to be overcome that menas live further and do not think much about it although it is difficult, but if you are innerly strong you can definitely overcome it.\n\nIt is very difficult to deal with jealous people for they only see darknes in others and they are people who are very pessimistic. Most of the time you can recognise them, by the faces they make, or the comments they say. You will see that when you announce your success, they will never congratulate you or even when they do they will do it in a provocative manner. Enjoy the fact that you have given them a reason to talk about you. LOL\n\nJust be nice to them and kind and friendly and you heap coals on their heads. You let them feel bad for their judgment of you. In fact, you go out of your way to tell them what nice features they have and compliment them on how they dress or the way they look. All they are looking for is approval.\n\nThey will start picking up faults in you, but just pray to God the lmigthy and like I say learn to resist it.\n\nI hope that helps, otherwise you can send me an email.\n\ngreat_ladyforislam@yahoo.com\n\n"
    author: "Naja"
  - subject: "Re: Jealous people"
    date: 2008-12-10
    body: "Thanks alot NAJA I Appreciate what you said very much.THANKS!!!!!!"
    author: "humble"
  - subject: "Re: Jealous people"
    date: 2008-12-10
    body: "Thanks alot NAJA I Appreciate what you said very much.THANKS!!!!!!"
    author: "humble"
  - subject: "Re: Jealous people"
    date: 2008-12-16
    body: "Hi. I don't understand what you are saying. Will you please explain further?\n\nThank you!"
    author: "Jenny"
  - subject: "Re: Jealous people"
    date: 2008-12-16
    body: "Hi. I don't understand what you are saying. Will you please explain further?\n\nThank you!"
    author: "Jenny"
  - subject: "Re: Jealous people"
    date: 2008-12-16
    body: "Hi. I don't understand what you are saying. Will you please explain further?\n\nThank you!"
    author: "Jenny"
  - subject: "Re: Jealous people"
    date: 2008-12-16
    body: "Hi. I don't understand what you are saying. Will you please explain further?\n\nThank you!"
    author: "Jenny"
  - subject: "Re: Jealous people"
    date: 2008-07-31
    body: "Ok so i'm in middle school and people are really jealous of me, it hurts. The whole year in 7th grade people brought me down so hard i had no friends at all.\nI have this small group but they aren't so strong and don't stand up for me as much.. I'm really smart.. Not to mention pretty.. And I'm really positive and I'm nice to everyone and I don't talk bad about ANYONE. My life sucks right now. I don't know what to do...\nAnd the haters got everyone involoved on 'hating me' and it really sucks.. I'm scared to go to 8th grade now.\nI hate facing the torture."
    author: "Ashley"
  - subject: "Re: Jealous people"
    date: 2008-10-18
    body: "Forget about them once you graduate you should leave any never talk to them again they're just jealous of your shining star and wish they could be you. I wouldn't worry about it there is nothing in life to be afraid of. You will see one day that they are just trying to bring you down because they know they can never be what you can be. Just tough through it high school will be over before you know it. I'm 27 now and time has flown by it feels like just yesterday I was in high school and that was almost 10 years ago. You'll realize that too some day. Just be tough and continue to be your self."
    author: "S Lane"
  - subject: "Re: Jealous people"
    date: 2008-10-18
    body: "The reason your small group doesn't stand up for you so much is probably because you are the leader of the group and are the strong one who stand up for them. Continue to protect your group of friends even though they don't seem to protect you they are experiencing the same problems as you and look up to you as they know you are a good person and have much to learn from you... Don't be persuaded by the negative continue to be positive no matter how hard it may be..."
    author: "S Lane"
  - subject: "Re: Jealous people"
    date: 2008-08-21
    body: "      Hey Angela.\n\nYou know, for being a guy, I have alot, I mean,\" alot', of jealousy around myself 24/7. One thing is true, the majority basically is just inconceided by other people whom are better than them in one way or another. But one thing that kills me is that they just purely hate me for the whole package. See I have always and am forever will be a very sincere and loyal person towards others. I was bullied quite alot,too much to even count,in my earlier years up until I was an early teen about 14. For being a guy it does happen, but too much to endure, at the age of 15 I started homeschooling,but I had big scars of the past/school/etc. And at the same time my grandmother my most favorite person had a massive stroke. So being at the age of 14 for me was quite the pain, plus other problems. My father was a bodybuilder at the time, he knew what I was going through. Oneday he introduced to bodybuilding, there my transformation began, 5 years I changed so much,not one old person can recognize me. But the tables turned once again, instead of bullying,random guys/girls/even couples will try to fight with me just to see how,\"strong\", I am plus yell a rude comment my way like today and everyother day. I cannot even make any friends because the trned it still the same with these maniacs. Fake and just simple insecure, can't they think beyond themselves, I mean if they really want to become better in life then they have to work very hard to aquire/attain it. Nothing was just/handed/ over to me, I had to work very very very hard to get where I am today. They need to learn that simple saying nothing is handed over to you just like that, if wishes were horses beggers would be riding them. I mind my buisness, eather they need to get a life, or work hard to attain these type of things, like bodybuilding or other things."
    author: "Ace "
  - subject: "Re: Jealous people"
    date: 2008-10-18
    body: "What you said is so true and represents someone I was very faithfull loyal and trusting of I'm a protector type personality and did many things to protect this person yet this person kept spreading lies about me and put me down to other people to no degree even though I would continually do things better than them and would not rub it in their face but just trying to help. The more I helped the more it made them do these things to me. It was very hurtful to me because it's hard to grasp that someone could be so diabolical, decietfull, dishonest, backstabbing when all you have ever done is help them.\n\nI figured out they are a tweaker, twak, coke head, fake mofo, if you ever suspect someone you know is a tweaker or coke head they probably are get as far away from them as possible they have destroyed their moral fiber and are lost human beings don't let them bring your shining star down... Leave them and don't look back they are a sorry excuse for a human being. Who will continue trying to mainpulate you to get what they want out of you, yet you mean absolutely nothing to them. Stop listeing to their word and look at their actions you will realize right away that they are a piece of ****. Coke heads are great actors and will lie to the very end."
    author: "S Lane"
  - subject: "Re: Jealous people"
    date: 2006-05-18
    body: "Yeah, I understand this issue very well.  There are a few jealous people in my family.  These people are angry at the fact that my mother and her sister have accomplished something with their lives.  They know that we have money and act as if were are close as a family.  I hear what they say behind our backs."
    author: "dj"
  - subject: "Re: Jealous people"
    date: 2006-05-18
    body: "Yeah, I understand this issue very well.  There are a few jealous people in my family.  These people are angry at the fact that my mother and her sister have accomplished something with their lives.  They know that we have money and act as if were are close as a family.  I hear what they say behind our backs."
    author: "dj"
  - subject: "Re: Jealous people"
    date: 2006-06-28
    body: "I am one of seven sibliing. Only one out of the family completed college. I support my family very much. However, I am the youngest. Not mention I paid my own way through college, working full time and attend college at night. graduated as Register Nurse. When conversing on phone with one sister everything is fine. Meet in public with other family members i am ignored. However, another sister does not talk to me at all. She always wanted to be the best in the family. We all have respected that.regardless of their behavior i still respect them. But, I wonder why they behave the way they do. They talk about me behind my back to other family and friends. I lose my older sister 10yrs ago. She had a home. It was not paid off when she passed away. So, We assumed the mortgage. I am paiding that mortgage alone right now. Often ask for assistant, but no one helps. With that I still support my family. I feel lots of jealous. Do you think it could a possiblity?"
    author: "B. Allen"
  - subject: "Re: Jealous people"
    date: 2008-01-26
    body: "It sounds to me that your family is jealous of you.You are doing really well for your self.I have a sister that is jealous of me to.She likes to start trouble for me,like:she will tell a lie to my brother about me,or she will try to do something else.She is really jealous of me because I have nicer things than she does.I have my own place,She lives with my grandmother,her husband,and aunt.All she does is take my grandmothers money and spend it on stuff she dont need like DVDS.She would have nice things to if she wouldnt spend money foolishly.Thats not my fault.I only bother because of my grandmother, if it wouldnt be for my grandmother,I wouldnt bother her her at all.Thats the truth!"
    author: "Tory Everitt"
  - subject: "Re: Jealous people"
    date: 2006-11-12
    body: "this is the epitomy of jealousy. every1 your kid brother your dear mother, your bastard father turns on you like a pack of hounds, and says: u little boy r mental. my worst nightmare. instead of live and let live, what do they do? the rascals lock me up in a psychisatric ward, dope me up and induce me 2 commit suicide. but how often does that turn out successful? if there was GOD, he would answer our prayers, instead we are cursed with psychiatrists asking do u hear voices? NO! still we'll give u drugs just 2 be on the safe side. thats my tragedy. i hadn't even seen or done much of life, and i'm spasticated at 22. nowhere 2 go but a slow death. is this how value today's young generation? "
    author: "ajay chandarana"
  - subject: "Re: Jealous people"
    date: 2006-11-12
    body: "this is the epitomy of jealousy. every1 your kid brother your dear mother, your bastard father turns on you like a pack of hounds, and says: u little boy r mental. my worst nightmare. instead of live and let live, what do they do? the rascals lock me up in a psychisatric ward, dope me up and induce me 2 commit suicide. but how often does that turn out successful? if there was GOD, he would answer our prayers, instead we are cursed with psychiatrists asking do u hear voices? NO! still we'll give u drugs just 2 be on the safe side. thats my tragedy. i hadn't even seen or done much of life, and i'm spasticated at 22. nowhere 2 go but a slow death. is this how value today's young generation? "
    author: "ajay chandarana"
  - subject: "Re: Jealous people"
    date: 2007-02-15
    body: "I must say all of you have very interesting comments and have had extraordinary experiences. I am impressed with all of you that have held on to your dignity despite your negative feedback from others. I too have been a victim of the almighty Jealousy monster and know it's not an easy thing to fight especially when you know in your heart and soul that your not trying to hurt anyone. My family treats me like I think I am better than they are. I live clean and set standards for the way my life is led. I also set an example at work and don't make many mistakes and am criticized for thinking I am better than they are. I press on. Morals and Ethics alike I treat with high regard, don't tell me it's black when I know it is white, and don't try to shove something down my throat that I know is wrong and try to make me accept it as right because I won't do it. I don't think I am better than I others I do think I have more common  sense than most but I don't treat people with disrespect however it doesn't change the way they react towards me. Killing people with kindness doesn't seem to work so I just press on. I have given the matter a lot of thought at times and the conclusion is that they want to advance to the next level as I have but they are so caught up in their world and the things in it that they can't and some won't because it's scary (a change) and that would put them in the same light as me. No one wants to be unpopular. I won't lower my standards just to fit in I refused to do it when I was an insecure teen I won't do it as a 40 yr. old adult. Stay strong to all of you persevere and just remember they live their lives and you need to live yours."
    author: "Rick"
  - subject: "Re: Jealous people"
    date: 2007-12-02
    body: "Dude I can relate to your comments \"black when it is white\" and \"don't shove something down my throat that I know is wrong. \" Man, living a clean, good life makes all the lonely feelings go away anyways, so who cares about about trying to look \"cool\" like you said. "
    author: "RR"
  - subject: "Re: Jealous people"
    date: 2007-11-16
    body: "Smart responses. I have my own experience dealing with jealousy (being the perpetrator and the victim). The short version: basically don't be mean back, but stay away from them as much as possible. \n\nWhen I was a jealous, pitiful small person:\nI hurt people who didn't even deserve it, but my own pettiness and ignorance made me impulsively act that way. I look back and see what were the reasons. My ex-girlfriend was a very materialistic woman who compared me to her other friend's boyfriends in terms of money,status etc and did flirt with other guys, and I think that had some aspect to it. I was also mixed in with the wrong crowd after high school (drugs, alchohol, etc), where all we did was watch tv and put each other down. Past is the past, but the smartest thing to have done would be to have left those losers, break up with that low-self esteem girlfriend (which I finally did thank God) and find positive people to show me the way. I still feel shame every time I think of the people I hurt, even those I apologized to, but realize God forgives. \n\nNow:\nNow I am doing pretty well. I know the direction to go. I cleaned up my act. Starting to get my shit together and am happy and confident. Ditched the lame GF and am having fun. Found good friends. Now I am on the other end (getting attacked) and it is not so great (and also made me realize how obnoxious I was before). Its funny because people who said jealousy can run in the family are SOOO right. My own brother takes every opportunity to mention every error, flaw, mistake and miscalculation I commit. My father is starting to belittle me and trying to make me feel stupid in our conversations. When I start to get into a negative, depressed mood, they start becoming nice and understanding again. I don't blame them because having wrestled with it myself they are in the mindset of competition, comparison, and scarcity. I guess jealous people are not bad inherently, they just have a negative perspective. \n\nLike other people have said, its better to be forgiving and somewhat distant with these types of people. You may even have to play politics and find a good support group in your social circle if things get ugly. This world is indeed dirty. Even at my church people backstab, gossip, etc. Its human nature. When I tried to settle into a new church, these are things that helped me out that might help you too:\n1) keep mouth shut about accomplishments and don't act fake or kiss ass. Most people will be happy for you but there are some who go out of their way to get a public reaction from you. \n2) Have an established value system to treat others as you wish to be treated. Kind acts don't hurt. Compliment if you really mean it. Doing so will give you a basic reputation, so that when gossip starts, it is the gossiper that gets into trouble and not you. \n3) Absolutely, if you feel someone does not like you from the get-go, do not try to be their friend. Ohh ho ho from my experience, this just gives them more invitation to wrong you. Just ignore them if you get a hunch. There are plenty of people who will like positive people. \n4)Don't fight back eye for an eye, but don't tolerate second-class behavior like bossiness, condescending tones, lying, etc. Just walk away and ignore. \n5)people who don't know you but help you anyways are usually the people you want to be friends with. And for some reason, people you admire and like somehow admire and like you too! It's weird but awesome. \n6)if a person is cool to you most of the time but occasionally acts negatively toward you, just ignore that. No one is perfect right? Unless they gossiped and planned something to hurt you LOL. \n7) if there is already an established group, and they are not that outreaching towards you, there might be a reason. "
    author: "RR"
  - subject: "Re: Jealous people"
    date: 2007-11-25
    body: "is there somthing I can do to make it even more uncomfortable for them!\nis there any tricks of the trade\ni hate those basterds"
    author: "t"
  - subject: "Re: Jealous people"
    date: 2007-11-30
    body: "Hmm unfortunately revenge only hurts you in the end, and gives them validation for their behavior. Ignore, avoid, or play politics and expose them. I guess if you can get solid proof of their heinous acts of hatred you can gain the upper hand. What that one poster CJ said helped me tremendously; forgiving and having compassion, but not going out of your way to be nice and expecting them to change overnight (they won't). "
    author: "RR"
  - subject: "Re: Jealous people"
    date: 2008-10-17
    body: "there is a threshold that everyone needs to figure out for themselves... sometimes i think openly confronting the jealous behaviour is empowering and can be a positive thing. At least enough to make them back off or realize they have crossed a line. For me, it helps me regain self respect.  I think that ignoring them or returning bad behaviour with kindness can be interpretted as weakness...depending on the extemity of the ignorance of the jealous person. I think confronting the situation is like shining a light on a dark heart and making them realize you won't tolerate bad behaviour. Many times this either makes them realize their own behaviour...or at least realize the limits that you will no longer tolerate. For those of you whom are persecuted by jealous people... I wish you well in resolving the problem. I also hope that your actions lead to enlightenment for both yourselves and the people that are jealous of you. I think there is a time and a place for \"kicking some ass\" - dispense this wisely. "
    author: "AN"
  - subject: "Don't kid yourself, Mosfet"
    date: 2002-01-02
    body: "First off, I'm not jealous at all of the attention you get.  I've contributed very little around here, so you should get more attention than I.  I don't really think I deserve any attention at all, in fact :)\nSecond, don't tell us we're unjustified to complain.  We can complain all we want, that's what the comments are for, feedback.  Furthermore, I think I justified my complaints rather well, in fact.\n\nHowever, don't go thinking that the only reason you see yourself in the news is because you write great, relevant code.  This is part of the reason, yes, and I won't argue that you shouldn't be recognized for it, because you should.  It's a valuable contribution and KDE is better for your effort and code.  But you're really no different than any of the other KDE volunteers who give up their time & energy for the project.\n\nThe other half of the reason you see yourself in the news is because of the rather immature games you've played with the KDE project and the other developers.  I can't help but think that you're just doing them for attention and publicity.  And in the process, you pissing off other KDE volunteers just like yourself and you are damaging the reputation of the KDE project as a whole.  Ever hear how OSS has that reputation for unprofessionalism and childish flame wars?  Well, congratulations, some of your past actions have only fed that reputation.\n\nYou need to take that into consideration when you freak about a release schedule that everyone else doesn't have a problem following or when you rant on your personal webpage about the project.  You have every right to do whatever you want, yes, but your actions affect many besides yourself.  As a result, you've projected two images of yourself to us: one as the very talented graphics coder, and the second as the childish primadonna programmer.  The second hurts both you, the KDE project, even OSS as a whole.  And I'll be happy to argue that it does more damage than your contributions do good.\n\nThere seems to be a commotion with much that you do, have you noticed that yet?  Even just this simple news story causes strife.  Have you also noticed that there isn't a commotion with most of the other KDE contributors?  Like I said in my post, I really hate to even encourage you by making these posts, but I feel it's time to say something.\n\nIf this is truly a turnaround between you and the KDE project, then that's great, and I honestly hope that it is!  I'll enjoy reading about your contributions.  But if in a few months I see another soap opera story on the dot like this one (e.g. 'Mosfet quits KDE (Again)'), then I'm going to be really disappointed in both you and the dot."
    author: "Bill Soudan"
  - subject: "Re: Don't kid yourself, Mosfet"
    date: 2002-01-02
    body: "Who are you again?"
    author: "Mosfet"
  - subject: "Re: Don't kid yourself, Mosfet"
    date: 2002-01-02
    body: "Does it make my opinion any more or less valid?\n\nI don't have anything to hide, though, so I'm a small-time KDE contributor.  I worked for quite a while on a KDE1 ICQ client (kicq and icqlib, as seen on SourceForge), made a few miscellaneous kdebase improvements here and there in CVS, and published the KDE development news for a few months after Navindra Umanee wasn't able to work on it any longer.  I've watched the mailing lists for a long, long time, so I'm familiar with much of what went on there since about the KDE1.1 days.  Even helped out a little when the dot first started by contributing a few stories.\n\nI wish I could contribute more, but I unforunately don't have the time right now.  Hopefully once I've graduated from school, I will."
    author: "Bill Soudan"
  - subject: "Re: Don't kid yourself, Mosfet"
    date: 2002-01-02
    body: "It doesn't make your opinion any more or less valid. Everyone can have their opinion and act however they want: including me, editors on the dot, etc... \n\nThe thing is *you* feel you can dictate to the editors of the dot and to me what stories should be carried, how I should behave, and chastise everyone for posting a positive news story about me. And people call me arrogant ;-) This is common behavior among many in the Linux community: They feel they should be able to dictate their opinions to others. They feel they should be able to deem what news is appropriate, (even though this is an obviously appropriate story), how developers should behave even though they've contributed little or nothing, and what projects people should work on in their free time. \n\nSo you don't like the fact that the dot posted a story about me and how I am trying to be more positive with the KDE community. Oh well ;-) Hopefully you'll get used to it :)"
    author: "Mosfet"
  - subject: "Re: Don't kid yourself, Mosfet"
    date: 2002-01-02
    body: "Then why did you ask who I was, and use it to try to invalidate my argument? ('even though they've contributed little or nothing')\n\nI hardly think I can dictate.  I was expressing my opinion, and I phrased it as a request, not a command:\n\n'...but I feel it's time to speak up so the editors hear some more opinions.'\n\nand\n\n'Please, please, keep the dot as clean and professional as it usually is, and stop with the Mosfet stories. If there's merit in them, fine, I'm by no means trying to say he should never be mentioned again. Instead, in the future, please use the same measure that any other story in the queue receives.'\n\nIf the editors don't follow my opinions, that's fine.  If I decide I don't like the dot, then I don't need to read it.  But I wanted to make my opinion known because I *do* like the dot and the editors.  From what I know of them, they appreciate well reasoned feedback, and that's what I was trying to provide\n\nI also don't recall telling anyone how they should behave, nor what they should do in their free time.  I even explicity said that I'd enjoy reading future articles about your contributions.  Future articles about you getting into arguments with the rest of the KDE team?  No, I'll be very disappointed that it happened and very disappointed in the editors for posting articles about it.\n\nYou're a very talented guy and a great programmer.  Don't let negative images obscure that, because while in the meantime you may be hurting the KDE project & other volunteers, in the end you're hurting yourself most of all.  You have a 'rocky history' now which you'll need to overcome before some of the KDE team is going to trust you again, and it looks as though you're headed down the right path already, which is great.  I bet given some time and a lack of attitude, you'll no longer see comments like mine and the anonymous others who are upset that there's stories on the dot about you."
    author: "Bill Soudan"
  - subject: "Re: Don't kid yourself, Mosfet"
    date: 2002-01-03
    body: "Bill, you really seem like a dick. I love mosfet. He kicks ass. The fact of the matter is that he is more relevant than you. I don't go to bill.org to download liquid... or anything for that matter. mosfet has made a personality for himself, and if you want to accuse him of doing what he does for publicity, you should back it up with more than a loony paranoid accusation with no evidence."
    author: "Sam Kennedy"
  - subject: "Re: Don't kid yourself, Mosfet"
    date: 2002-01-04
    body: "Not even going to bother with the name calling.  That's just weak.\n\nYou're right, he is more relevant than me, I've never pretended that he\nwasn't.  I even said I didn't feel I deserved any recognition at all.\nSince we're obviously comparing though, no, you won't find Liquid on\nbill.org, but you may go to both:\n\nhttp://sourceforge.net/projects/kicq\nhttp://sourceforge.net/projects/icqlib\n\nfor some KDE code that I've poured many hours into.  KICQ isn't too\npopular any longer, ICQ in general is falling by the wayside, but it used\nto be in the KDE1 days.  On SourceForge's 'Top' lists, kicq is still #16\non the 'Most Active All Time' which shows how much work we put into it and\n#24 on the 'Top Forum Post Counts' which shows how much interest there is\nin it.\n\nI accused him of making noise and pulling stunts on the mailing lists\n(which ultimately hurt the KDE project as a whole) for attention.  Since\nI've watched the mailing lists carefully for a long, long time now, I have\na good feel for how KDE contributors interact, how much work gets done,\nand by whom.\n\nWhile Mosfet is certainly a contributor, he's definitely not one of the\nlargest, yet he seems to receive the most press.  I feel this is\nunfortunate because one of the reasons he receives the most press is\nbecause of what I feel are childish, attention-grabbing antics, not actual\ncode.  I'll reiterate that he should be recognized for his worthwhile\ncontributions, and some of his pubilicity is definitely deserved!  But\nmany other things he has done are not worthy of publicity, and IMHO, to\nreward him for those by an article on the dot is a slap in the face to the other many hard-working KDE contributors.\n\nAs far as evidence goes, I have mailing list histories for kde-devel,\nkde-core-devel, and kde-cvs to back myself up - I would be more than happy\nto provide links to relevant messages if you're interested.  Mail me\nprivately please and I will provide them, I'm not going to bother\ncluttering up the board here.\n\nI've done my research, Sam, over the long history I've had with the KDE\nproject.  Did you do ANY on me before you made that post?"
    author: "Bill Soudan"
  - subject: "Re: Don't kid yourself, Mosfet"
    date: 2002-01-04
    body: "\"I'm trying to show some goodwill to the KDE core team\"\n\"story about me and how I am trying to be more positive with the KDE community\"\n\"this is an initial gesture of goodwill on my part and what I hope will be a better relationship\"\n\nHey Bill, get out ! Don't you see that Mosfet just gives a KDE community a one more chance ! If Mosfet leaves and KDE project fails you will be responsible for it !!\n:)\n\nMosfet:\nI like your work ( is there any way to turn on a detailed file list in Pixie, without any thumbnails  ? ), but some of your comments make me smile. I don't see any KDE community vs. Mosfet, I hear no single voice of KDE community. KDE team just consist of people exactly like yourself. Have you bad relationships with them all ?! Look - KDE community didn't respond. Is it ignoring you ?!!\n\nBill:\nOf course, those contributions are worhty of a dot story. Don't cheat ourselves - we haven't thousands of news we can choose from. I'm often disappointed when I visit the dot and see that there are no new stories, I didn't read a story about inventing a wheel on the dot for a very long time too... Dot is KDE news site after all, and I don't know what kind of stories are you expecting ..."
    author: "Rumcajs"
  - subject: "Re: Don't kid yourself, Mosfet"
    date: 2002-01-04
    body: "You're right Rumcajs, it is nice to see new contributions on the dot.  I'm just trying to argue that I'm disappointed to see this particular one, for a few reasons, one of which is Mosfet's past behavior... I feel stories about Mosfet's behavior are bad publicity, and in this case, I also feel the dot is keeping this bad publicity about KDE alive by posting stories that mention it.  I also think by posting stories about it, the editors are only encouraging Mosfet to pull more stunts like he has in the past, because hey, they always show up on the news sites, right?  Which means more bad publicity for KDE in the future. :(\n\nThe editors obviously thought differently, which is why you see the story in the first place!  But there's a long history here between Mosfet and the KDE team, if you're curious, you can do a lot of research of the KDE mailing lists."
    author: "Bill Soudan"
  - subject: "Re: Don't kid yourself, Mosfet"
    date: 2002-01-06
    body: "Come on Bill, I reckon life would be quite boring if viewed from your inner side !\nBeside talent, humans also happen to cherish salt, laughs and poetry...\n\nThat sometimes help distinguishing from ants, the Venusians told me."
    author: "Germain"
  - subject: "Re: Don't kid yourself, Mosfet"
    date: 2002-01-11
    body: "Hmm...\n\nyou know, I think u're right.\nyour opinion is reasonable and balanced.\nhowever, i'm not 100% sure that the people here get it.\nit's a problem of communication. The \"outside world\" (out of so-called \"OSS community\"), depict \"us\" as childish or so. They're not wrong. People like Mosfet illustrate that. But also, i think it is a great chance in fact that the OSS can allow such taltented (sp.?) people to work, because it would be impossible or very difficult in \"normal\" world (i mean, companies... i think Mosfet working directly for any customer would be a very funny thing - though not impossible maybe).\nnow if this suppose to make news articles from it is another story...."
    author: "Guest"
  - subject: "Re: Jealous son"
    date: 2002-01-02
    body: "Another thing I find really interesting about this scenario is its parallel to the prodigial son story in the Bible.  Everyone who got jealous about this are just like the older son.  As for me, I'd prefer to be the younger son than the older son.  The younger son learned something from his mistake, the older learned nothing.\n\nWelcome back Mosfet, and I'm not saying that you did anything wrong.  How much happier would the dad in the story be if his son came back with 20 times as much wealth as he left with?  \n\nRelationships between KDE and various developers are certain to go up and down.  We all hope that in ups and downs, we all learn something along the way and eventually support each other."
    author: "Jly"
  - subject: "Mosfet Did not posted this _Great_News_"
    date: 2002-01-02
    body: "Well, Andreas Pour submitted the news ;) and 20 new contributions is indeed a good news."
    author: "Asif Ali Rizwaan"
  - subject: "hehehe, is this for real?"
    date: 2002-01-03
    body: "was that really mosfet who posted that? how old is he? 12?\nlet's see:\n\"While most of the comments have been quite positive, it's obvious a few people are jealous of the attention I get. News about me and what I'm doing does get carried a lot, and there is a reason for that. I write graphics code, and people have a lot of interest in that.\"\n\nhehehe, ok, great one, hehehehe. knock-out argument.\n\n\"Even not considering I am a fairly visible Linux/Unix developer, the addition of 20 effects to KDE3 would be considered a good story anyways.\"\n\nhmmm, i'll babel that one, and get back to you on it?\n\n\"This is good news for both programs that deal with images and widget styles, always a subject people are interested in.\"\n\nwell alright partner. \n\n\"Add to the fact that my departure from the KDE team was widely covered and that this is an initial gesture of goodwill on my part and what I hope will be a better relationship - that's even more good news!\"\n\nbless those stars that have brought the world such luck.\n\n\"People complaining about posting this story are totally unjustified and I can't help thinking are full of \"sour grapes\".\"\n\nyou wan't a snack pack with that? \n\n\"They don't think anything I do should be covered, no matter how good or cool it is.\"\n\nthe plot thickens. conspiracy! conspiracy!\n\n\"And be ready to not read a lot of news because I am back and working on a lot of interesting new things!\"\n\nswell.\n\nthose of you responding this guy calling him mosfet (really him?), read the text again; then both you and i can lament the time we wasted responding to cute wittle mosfifet\nkeep it real kid :)"
    author: "benji, the dog"
  - subject: "Re: hehehe, is this for real?"
    date: 2002-01-03
    body: "Heh, yep - you made a real knock-out argument there. And you call me 12? I try to make reasoned responses and all you can reply with is whiny sarcasm with no justification, like a sulking pre-teenanger. People like this commenting on my behavior is an irony all in it's own.\n\nEnough with this, I'm done responding to trolls such as these. Back to work! PixiePlus 0.2 has just been released >:) Quit trolling and go enjoy."
    author: "Mosfet"
  - subject: "Re: hehehe, is this for real?"
    date: 2002-01-03
    body: "If you wanted to impress anyone with your post, I believe you have failed. I suggest you hold off on criticizing other posters' comments until you can (a) capitalize and (b) articulate an opinion.\n\nOr you're a troll. Boring."
    author: "J\u00e9r\u00f4me Loisel"
  - subject: "Re: hehehe, is this for real?"
    date: 2002-01-03
    body: "But he's so right. Calling this a \"well-reasoned response\" is just laughable and makes you think mosfet is indeed 12 years old."
    author: "Jake"
  - subject: "So I'm a little late"
    date: 2002-05-07
    body: "  \n\n    Projects can be so strange, I'm writing a script for a college class and I'm writing a complex script off of the fable: Envious and Avaricious. \nAnyway, Bill and Mosfett (sp?) have set up a great archetype of characters for me to explore. The arrogant Mosfett vs. 2) The man of contrast: Bill.\n\n     Since I like to put a lot of childish behavior (arrogance, immaturity, pettiness) into my scripts I need the man of contrast, the man in the khaki pants, the suit, or just the swell guy that knows what he is talking about. Bill is the voice of reason, and in this case the voice of reason is the voice that looks to me like no one else will listen to. Perfect. Thanks.\n"
    author: "Sean"
  - subject: "Re: Jealous people"
    date: 2007-02-16
    body: "I totally agree. At my school, people are jealous because of my relationship. They are all single and lonely and have nothing else to do but hate on me and my boyfriend. If I wasn't a strong believer in God I would have really hurt someone by now. I am trying to keep a strong, positive attitude and not worry about all the JEALOUS people, because they are very insecure and depressed. I am a very attractive young lady and smart also. I attend church every Sunday. I am a great athlete and just an outgoing, down to earth person. If I were them I would be jealous of me also. They are all so fat and ugly and they can't play sports and they don't attend church every Sunday. So yea love your haters and the jealous people. They should only motivate you to keep doing your thing and living your life to the fullest!!!!"
    author: "Kai"
  - subject: "Re: Jealous people"
    date: 2007-03-12
    body: "Im 16 years old at im a junior at a alabama school. There are so many haters there that i think the school should be name Hater High .  I don't talk 2 any1 because i don't trust any1. Mostly what my haters hate on me about is my car. They are so jealous. Another jealous thing is that i love to look nice and the bitches hate 2 see me look fly. But i never let a hater bring me down it just always keep me motivated."
    author: "Haters go 2 hell"
  - subject: "theme code"
    date: 2002-01-02
    body: "Is mosfet the maintainer of the themeing parts of KDE? If so, does he still actively maintain it, and if not, is there anyone who knows the code as well as he does?\n\nJust curious..."
    author: "Johnny Andersson"
  - subject: "Re: theme code"
    date: 2002-01-02
    body: "I don't code themes in CVS anymore. I did for KDE2 but now do all my style work outside of CVS. There was too much conflict and I prefer doing things independently. I still do work on styles, tho, just not anything included in KDE itself."
    author: "Mosfet"
  - subject: "Re: theme code"
    date: 2002-01-02
    body: "I was thinking about the general plug-in framework, which if I recall correctly were mostly designed by you. Is it considered part of kdecore and maintained by the core developers, or is the code just \"lying\" there? ;)"
    author: "Johnny Andersson"
  - subject: "K-button for Mac menu?"
    date: 2002-01-02
    body: "Mosfet/Daniel if you're out there reading this, I just wanted to say you've done some great work so far, but I'm curious about a small project you had which involved putting the K-button dropdown menu onto the far left of the Mac-like global menu which KDE can do (sorta like where the \"Apple menu\" would be) ... any news on it?  I've run Liquid with the Mac menu (and the default_blue.jpg wallpaper) which makes for a pretty close OSX-like desktop, but I still need to have a d@mn taskbar at the bottom in order to have a K-button menu.  Having a K-button/menu at the top left in the Mac global menu in addition to all the other Mac-like stuff would be great!\n\n(Anyone else have any thoughts to add on this?)"
    author: "PRR"
  - subject: "Re: K-button for Mac menu?"
    date: 2002-01-02
    body: "Me too! I was incredibly excited when I saw that; but I haven't heard anything since."
    author: "Shamyl Zakariya"
  - subject: "Re: K-button for Mac menu?"
    date: 2002-01-03
    body: "It was buggier than a cow turd sitting in a Texas field in August :P I might pick it up again for KDE3."
    author: "Mosfet"
  - subject: "Re: K-button for Mac menu?"
    date: 2002-01-03
    body: "Sorry to hear that, man. I know the feeling, sometimes code should be allowed to die a noble death."
    author: "Shamyl Zakariya"
  - subject: "Re: K-button for Mac menu?"
    date: 2002-01-03
    body: "Please consider it for KDE3.  Is the code you tried available on your site or on CVS somewhere?"
    author: "PRR"
  - subject: "egos and ids, freedom and civility..."
    date: 2002-01-14
    body: "For any case, Mosfet's contributions are appriciated by the community at large..\n\n... and for that he does deserve a big thank-you. It's well earned. Thank-you Mosfet!\n\nAll the KDE developers out there should be granted a big public \"Thank you\", at least every other day ;)\nThe supporting public too! They make it possible for KDE, and OSS in general, to exist the way they do.\n\nTo all of those would like like to comment on how terrible the parent article is for existing, I must reply.. \n\n\"Whatever, *Shrug*, have a nice day.\"\n\nI hope Mosfet and all can appriciate this response. It is a fine one. If a person knows not their own strengths, rudeness in regard to them *will* hurt.\n\nIt is most important; terribly important in fact, that people like Mosfet, whatever history may be, contribute to efforts like KDE. It is also important that we are notified. For those of you who do not see this, I am sorry. I hope that the benefits will someday \"shine light upon balding head.\"\n\nI would like to relate that the precise way in which the article was written, is unfortunate.\nLet the past be folks. There is no need to focus what could have been a purely cheer-worthy announcement, on Mosfets past actions.\n\nI ask that we leave out the uncivil social commentary, and stick to what's important! The forward movment and enhancement of a great array of software.\nThe future will depend upon it.\n\nIt does no good to fret about such things friends.. Let us all rejoice that we now have some great filters to work with, and that one more step has been made, for all mankind. That *is* what this is all about, right? \n\nStraight out of dict (web1913) \"Men can not enjoy the rights of an uncivil and of a civil state together\" - Burke\n\nCheers to all, good things are happening here. Myself and others look forward to more release/feature announcements from all of you, Mosfet included.\n\n-taint\n--Disclaimer: All language errors are the property of taint, and taint alone. They may not be duplicated, replicated, copied, or otherwise distributed without taint's express consent. taint may choose someday to offer these errors under the BSD or GPL license at such time he feels they may be of value to the community.\n\n(taint is not a developer of anything at this time, but he is a user, and fan, of linux and kde, and supports putting them both on the desktops of every user at the multi-national corporation for which he is the Senior Administrator and Developer. He is currently working on acceptable ways the corporation can help projects like KDE. Afterall, it saves the corporation lots of money. He also hopes that this doesn't make him *unworthy*.)"
    author: "taint"
---
Many in the KDE community are aware of some rocky history between
KDE hacker <a href="http://www.mosfet.org/">Mosfet</a> and other KDE developers.
Fortunately, it looks like things have taken a great turn for the better:
Mosfet wrote in to tell us that &quot;<em>I've decided to donate 20 effects I ported to KDE/Qt
for <a href="http://www.mosfet.org/pixie/">PixiePlus</a> to KDE3</em>&quot;.
<a href="http://www.kde.org/people/waldo.html">Waldo Bastian</a> promptly
added them to <a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/">CVS</a>.
The new effects include normalize, equalize, solarize,
threshold, emboss, despeckle, charcoal, rotate, sample, addNoise, blur,
edge, implode, oil paint, sharpen, spread, shade, swirl, wave, and
contrastHSV. All will be available under a
<a href="http://www.kde.com/legal/bsd_type.php">BSD-type license</a> in the
<a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdelibs/kdefx/kimageeffect.cpp?rev=1.30&content-type=text/x-cvsweb-markup">KImageEffect
class</a> in
<a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdelibs/">kdelibs</a>.
According to Mosfet, these effects will be useful not only for image viewers
and editors, but also for things like style engines.
Except for the simple rotate, Mosfet ported the effects from
<a href="http://www.imagemagick.org/">ImageMagick</a> to work directly on
<a href="http://doc.trolltech.com/3.0/qimage.html">QImages</a> and
Qt scanlines.  Nice job, Mosfet!
(For those who have not yet heard the news, PixiePlus is the successor
to Pixie; more information is available
<a href="http://www.mosfet.org/pixie/">here</a>.)


<!--break-->
