---
title: "Quickies: Sphinx for KMail, Rosegarden, KDE/GNOME@ALS"
date:    2001-10-11
authors:
  - "numanee"
slug:    quickies-sphinx-kmail-rosegarden-kdegnomeals
comments:
  - subject: "ALS tuturial"
    date: 2001-10-11
    body: "Just found this in the ALS tuturial list:\n\nKDE \"Hidden Features\" for Power Users\n Kurt Granroth, SuSE Inc\n\nSounds interesting. Will there be a video and/or an online-presentation?\n\nKDE is so much fun: Last weekend I played with a notebook, KPresenter, Dcop, Lirc and an ir-remote-control: Sooo cooool (and useful :-) \n\nBye\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "clever gouvernment"
    date: 2001-10-11
    body: "\"We are happy to announce that the German Federal Agency for IT Security, BSI, has contracted us (Intevation, Klar\u00e4lvdalens Datakonsult and g10 Code) to ensure Free Software support for their email security standard, Sphinx\"\nIf only all european gouvernments were that clever and promoted such initiative instead of relying to new unenforcable laws."
    author: "try"
  - subject: "Re: clever gouvernment"
    date: 2001-10-11
    body: "Well, there are two not-too-seperate reasons why they are doing this.\n\n(1) From a security/secrecety (?) point of view, open source is always better than closed source, because you know what's happening. If you're talking about computers being used in intelligence agencies (and Sphinx is part of a project for a new IT framework to be used by the european intelligence), this is a real issue.\n\n(2) More currently, after Sep. 11, there are serious considerations about the U.S. issuing laws to have a backdoor into security programs. Neither the European Union itself nor european companies want the US sniffing about in their confidential mail, so see (1)\n\n(3) If the U.S. really do go ahead with their plans, the fact that there is a sofware which is guaranteed-backdoor-free is going to be a major selling point for solutions/services focusing around Sphinx and its Open Source Implementation. And these services will most likely come from european companies (not from U.S., for sure), so there's money there, too.\n\n(4) There's more, but you get the idea.\n\nAll in all, it's not some ideology thing, but proves that there are some very valid reasons why one should choose to develop something as an open source project."
    author: "anno"
  - subject: "Re: clever gouvernment"
    date: 2001-10-12
    body: ">security/secrecety (?)\n\nI think the word you're looking for is secrecy.\n\nIMO, that backdoor idea won't go anywhere. Congress is stupid, but they aren't stupid enough to initaite something that widespread without at least getting the opinion of someone who actually understands the technology.\n\nCome on, how are they gonna parse all that data, and how will they tell if a message is encrypted in a way they can't read? \"Why, Officer, that's not encryption, I just have a perl script set to send out random data at random intervals, to check my network.\"\n\nAlso, wasn't Sphinx also the name of some sort of OSS speech-recognition project?"
    author: "Carbon"
  - subject: "Re: clever gouvernment"
    date: 2001-10-13
    body: "\"Congress is stupid, but they aren't stupid enough to initaite something that widespread without at least getting the opinion of someone who actually understands the technology.\"\never heard of \"millenium act\" or software patent ?\nfrom people who understand technology, they re not justified . THey re not manageable either, see the crap the us patent office accept as innovation.\nyet, they exist.\nnever underestimate the power the rich can buy through a corrupted system."
    author: "try"
  - subject: "Yet another idiotic Reviewer"
    date: 2001-10-11
    body: "At least the Register claims to be a sarcastic flame source - they are proud of it and intentionally rip on anything that they \"review\".  LinuxPlanet is apparantly taking after them, reviewing alpha software on anything but a \"roadmap\" basis.  After awhile, my eyes glazed over... the first page is a complaint about how hard it is to compile and install the first alpha release, and that there are so many bugs, it's not ready for primetime.\n\n    Well, what the hell does he think \"alpha\" means?\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Yet another idiotic Reviewer"
    date: 2001-10-11
    body: "Yes, any reviewer must not review any alpha or beta release of any software. And Alphas are the most unstable, i agree."
    author: "Asif Ali Rizwaan"
  - subject: "Re: Yet another idiotic Reviewer"
    date: 2001-10-11
    body: ": Yes, any reviewer must not review any alpha or beta release of any software.\n\n    Oh, no... I'm not saying that.  Just that the reviewer must take into account that large portions of the alpha software may not work, and that it's an early work in progress.  The ethical thing is to make statements like \"when finished\", or \"this is intended to\", and at the end like \"These features may or may not be in the finished version\".  For Beta software, you generally judge the features, and ignore bugs, generally making a note as to if it's a useful beta (\"Although this is beta, it is very stable, and can be used in daily use\").\n\n    I felt bad when Gnome got nailed on their alpha of 2.0, but it was in a site that intentionally goes overboard and flames stuff just for the heck of it.  It's almost a humor site.  LinuxPlanet tried to do a serious review, as if this were final release software.  Ridiculous.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Perhaps you shouldn't glaze over so quickly..."
    date: 2001-10-11
    body: "From the article:\n\nNow. Please do not think for even a second that the above is any criticism of either the alpha or of those putting it together, because it isn't. It's not just an alpha, it's the very first alpha, and the fact that it built uneventfully is itself a remarkable accomplishment. Instead, my observations are merely to let you know before you go out and build it that it has not yet achieved a level of working new stuff to justify tolerating the stuff that's broken. It's important -- very important -- for us mere users to grab early versions and actually use them, day in and day out, for we then provide that many more eyes and that many more test machines. My point is that the KDE3 alpha isn't yet where that's likely to be a useful exercise."
    author: "scott haug"
  - subject: "Re: Perhaps you shouldn't glaze over so quickly..."
    date: 2001-10-11
    body: "Yes well, he could have found a way to tell people that 'this is a work in progress' without spending an entire page moaning about how hard it is to install...\n\nA casual reader could easily glance through the article, miss that paragraph and come away with a very bad impression. Given that he's trying to give a serious review (or preview) it was a seriously poor article."
    author: "Jon"
  - subject: "Re: Yet another idiotic Reviewer"
    date: 2001-10-12
    body: "Well, he does mention in the end that you can't expect more from an alpha.\nWhat I found idiotic however was that in the end he spends a lot of time\nflaming about the missing compatibility between Qt3/KDE3 and Qt2/KDE2.\nWTF? You can have Qt1, Qt2 and Qt3 installed at the same time without any\nproblems and any application will link to whatever version of the library\nit requires! Same with kdelibs1, 2 and 3. Unix is not windows (where this\nsort of thing would be a problem)."
    author: "Marvin"
  - subject: "KMail folder relocation [OT]"
    date: 2001-10-11
    body: "I use RedHat 7.1 and KDE 2.2, I wish to place the default KMail's \"mail\" folder into \"~/.kde/mail\" instead of \"~/mail\". Is it possible? Please advise."
    author: "Asif Ali Rizwaan"
  - subject: "Re: KMail folder relocation [OT]"
    date: 2001-10-11
    body: "http://kmail.kde.org/manual/faq.html#id2792815\" says:\n\n6.4 Can I configure the location of my mail directory?\n\n Exit KMail, make a backup of ~/.kde/share/config/kmailrc, then open it with an editor and add e.g. folders=/home/username/.mail to the ?[General]? section. Then move all your existing folders (including the hidden index files) to the new location. The next time you start KMail it will use /home/username/.mail instead of /home/username/Mail. Note that KMail will lose its filters if you change the mail directory's location but forget to move your existing folders."
    author: "Oliver Strutynski"
  - subject: "Re: KMail folder relocation [OT]"
    date: 2001-10-12
    body: "Thanks, I could not locate that thing in the FAQ's. It worked."
    author: "Asif Ali Rizwaan"
  - subject: "OT: Mosfet??"
    date: 2001-10-12
    body: "What's up with Mosfet? Have you recently looked at his site?!?!?"
    author: "AC"
  - subject: "Re: OT: Mosfet??"
    date: 2001-10-12
    body: "Looks like he's sick of Linux again (but I could be wrong and he just might have got a different job). It happened before. It's a pitty."
    author: "Marko Samastur"
  - subject: "Re: OT: Mosfet??"
    date: 2001-10-12
    body: "But this time he seems to be sick of Linux as a whole and not just the KDE developping policy... sorry, I really like the Liquid style.\n\nNevertheless I have a certain understanding that the rest of the KDE team had some difficulties with him ;)"
    author: "AC"
  - subject: "Re: OT: Mosfet??"
    date: 2001-10-12
    body: "Briefly, he was hired by a company (Future Technology?) that never paid him.  He turned out the last version of Liquid without their name on it, and wrote a warning that he'd have to get a job doing Windows application development to pay the bills. I guess he's one of those \"100% or 0%\" kinda guys - he certainly was a \"we play by my rules, or I'm leaving and taking my ball with me\" kinda person.  Good coder, from what people say, a good fellow, but a bit hard to work with.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "little question about the screenshot section"
    date: 2001-10-12
    body: "in the http://www.kde.org/screenshots/index.html, there are only screenshots of the 2.0 version. Why not 2.2 ?"
    author: "Anonymous"
  - subject: "About quickies on the Dot"
    date: 2001-10-12
    body: "This is strange. Navindra, you often say that you would put up more stories if more people would submit more. Yet it seems that you very much like posting quickies, where several stories are presented together... A contradiction?\n\nPerhaps you try harder to separate your quickies. For example, I have no doubt that the Sphinx thing would have deserved its own story. It means more stories on the site and a more focused discussion. Until you get to the point where you have several stories per day, I would basically hold off on quickies as much as you can.\n\nJust my opinion."
    author: "J\u00e9r\u00f4me Loisel"
  - subject: "Re: About quickies on the Dot"
    date: 2001-10-14
    body: "Quite right, unfortunately these articles had lingered in the queue for a while before I got to them and noone else had time to take care of them either.  Instead of publishing a ton of new articles with outdated content, I thought I'd group them.  Mostly our fault for not taking care of the news in a timely manner.\n\nCheers,\n0N."
    author: "Navindra Umanee"
---
<a href="mailto:jan@intevation.de">Jan-Oliver Wagner</a> writes:
<i>"We are happy to announce that the German Federal Agency for IT Security, BSI, has contracted us (Intevation, Klarälvdalens Datakonsult and g10 Code) to ensure Free Software support for their email security standard, Sphinx. Sphinx basically consists of S/MIME, a PKIX compatible X.509 profile, together with certificate revocation lists (CRLs) based on LDAP. The code developed will be modular allowing inclusion in several MUAs released under the GNU GPL."</i>
Sphinx-enabling <a href="http://kmail.kde.org/">KMail</a> and <a href="http://www.mutt.org/">Mutt</a> are essential goals, see <a href="http://www.gnupg.org/aegypten/">gnupg/aegypten/</a> for more information.

<a href="mailto:glaurent@telegraph-road.org">Guillaume Laurent</a> wrote in to tip us off to the first
release of the "MIDI Sequencer and Notation Editor" by the <a href="http://rosegarden.sourceforge.net/">Rosegarden project</a>.  This <a href="http://sourceforge.net/project/showfiles.php?group_id=4932">alpha version</a> is based on KDE2 (<a href="http://rosegarden.sourceforge.net/screenshots/rg_main.png">rg_main</a>, <a href="http://rosegarden.sourceforge.net/screenshots/rg_score.png">rg_score</a>),
and needs more contributors.  See the <a href="http://www.all-day-breakfast.com/rosegarden/development.html">development history</a> and the <a href="http://rosegarden.sourceforge.net/">project webpage</a> for further information.

dotsphinx wrote in to point out that we are scheduled to interoperate with the Gnomes at <a href="http://www.linuxshowcase.org/">ALS</a>, November 7th.  Follow <a href="http://news.gnome.org/1002599178/">this link</a> to Gnotices for more information.
 
Many, many of you wrote in with this link to an <a href="http://www.osnews.com/printer.php?news_id=161">interview</a> of Linus.  There is some putative relevance to KDE.
 
Finally, dep at LinuxPlanet discovers that <a href="http://www.linuxplanet.com/linuxplanet/opinions/3828/1/">KDE3 is alpha</a>.  The most serious claim is that
the alpha is significantly slower than KDE2, but others appear to have had <a href="http://linuxtoday.com/news_story.php3?ltsn=2001-10-10-002-20-OP-BZ-KE&tbovrmode=1#talkback_area">different experiences</a>.



<!--break-->
