---
title: "Kernel Cousin KDE Issue #3 is Out"
date:    2001-03-25
authors:
  - "numanee"
slug:    kernel-cousin-kde-issue-3-out
comments:
  - subject: "Re: Kernel Cousin KDE Issue #3 is Out"
    date: 2001-03-25
    body: "Good stuff as usual. This issue seems to be missing the archive links though.... ;)"
    author: "KDE User"
  - subject: "Re: Kernel Cousin KDE Issue #3 is Out"
    date: 2001-03-25
    body: "Yes, I forgot to include the archive links. Will do so in Issue #4 (or maybe i'll start a tradition and only put them in even numbered issues?).\n\nI forgot it was Friday until somewhere around midnight. Seeing as I try and get the new issue in sometime around noon Saturday and I do most of the actual writing Friday, I was a bit behind and rushed. And in the hurry, guess what I missed? blah."
    author: "Aaron J. Seigo"
  - subject: "Re: Kernel Cousin KDE Issue #3 is Out"
    date: 2001-03-25
    body: "Thanks for your hard work! I'm enjoying the results."
    author: "kdeFan"
  - subject: "Renaming of \"Kant\""
    date: 2001-03-25
    body: "There seems to be discussion of getting rid of KWrite.. While I totally agree that Kant shld replace KWrite, why isn't Kant being renamed to kwrite??? Think of those millions of users of KDE who are used to Ctrl-F2 kwrite <enter>! When will they know of kant? and if kant is sucessor of kwrite, then why not rename kant to kwrite... \n\nAnd \"kant\" means what?? its such a non-obvious name for an editor!\n\nIf you agree with me, please write to the authors to make this point.. KDE shld keep strict naming of its popular products.\n\nOne more application is kbear.. why isn't it called kftp? Why was ktop renamed? It seems KDE is moving away from obvious names and putting in names which are most difficult to remember!\n\nsarang"
    author: "sarang"
  - subject: "OOPS"
    date: 2001-03-25
    body: "Ctrl-F2 kwrite <enter>\n\nOOPS.. i meant Alt-F2 kwrite <enter> ;)"
    author: "sarang"
  - subject: "Re: Renaming of \"Kant\""
    date: 2001-03-25
    body: "There will be a \"kwrite\" frontend to \"kant\" as far as I know to keep backwards compatibility."
    author: "KDE User"
  - subject: "Re: Renaming of \"Kant\""
    date: 2001-03-25
    body: "I don't mind having all of kant's functionality in kwrite.. there is no need to keep the current kwrite as it is.. thats why i am suggesting to rename kant to kwrite.. Those who need a simple editor always have kedit!"
    author: "sarang"
  - subject: "Re: Renaming of \"Kant\""
    date: 2001-03-25
    body: "the idea seems to be that KWrite will remain a \"seperate\" program... basically just kant w/out all the kant stuff =) (a shell for the kantpart editor, really) ... it will even still be referred to as KWrite (for now)\n\nKant (i think named after the philosopher?) will have the light-ide type bells 'n whistles (project management, etc...)"
    author: "Aaron J. Seigo"
  - subject: "Re: Renaming of \"Kant\""
    date: 2001-03-25
    body: "There's also the point that Kant is a swear word in London (cockney pronunciation of c*nt), so will probably cause a few snickers dahn sarf."
    author: "Dave"
  - subject: "Re: Renaming of \"Kant\""
    date: 2001-03-25
    body: "\"There's also the point that Kant is a swear word in London (cockney pronunciation of c*nt), so will probably cause a few snickers dahn sarf.\"\n\nPoor old Immanuel.\nI don't think this name can be used for pronounciation reasons.\n\nNorthern Europeans should only use the name \"KWrite\" in the presence of people with english as their native language. The developers have probably only communicated in writing."
    author: "reihal"
  - subject: "Re: Renaming of \"Kant\""
    date: 2001-03-26
    body: "Erm, being from london, I can assure you that \nKant is *not* going to get mixed up with c*nt. \n\nSo shut it! ;-)"
    author: "RObert"
  - subject: "Re: Renaming of \"Kant\""
    date: 2005-12-06
    body: "this has got to be the most idiotic discussion i've read in a long time"
    author: "khunt"
  - subject: "Re: Renaming of \"Kant\""
    date: 2001-03-28
    body: "Hi,\nI am the maintainer of Kant/KWrite and have some notes:\n1. An old KWrite style app using KantPart will still exist for all users who love the old KWrite app :)\n2. Renaming Kant to KWrite seems not to be such a good idea, Kant is much more than KWrite (I don't really know how Kant is pronounciated, sorry).\n3. I am open for other nice names ;)\n4. Kant has a plugininterface and all extended features like project stuff, ... are moved to plugins (Kant shouldn't be an IDE, but plugins can make it to whatever you want ;)\n\ncu and thx for the nice feedback"
    author: "Christoph Cullmann"
  - subject: "Re: Kernel Cousin KDE Issue #3 is Out"
    date: 2001-03-25
    body: "Your Kernel Cousins are so darn good, Aaron! I'm glad, not only because there _is_ a KC KDE, but because they are so nicely put together."
    author: "Matt"
  - subject: "Re: Kernel Cousin KDE Issue #3 is Out"
    date: 2001-03-25
    body: "I can only agree, they are a great summary of what's going on, nicely put together.\n\nThanks a lot, Aaron."
    author: "Carsten Pfeiffer"
  - subject: "Re: Kernel Cousin KDE Issue #3 is Out"
    date: 2001-03-25
    body: "Will the font selection widget have support for displaying the last few fonts you've selected at the top of the list?"
    author: "Carbon"
  - subject: "Re: Kernel Cousin KDE Issue #3 is Out"
    date: 2001-03-25
    body: "No! I hate that. At least the way MSWord does it. Having the same font in two places in the same list is nothing but bad UI, IMHO."
    author: "Matt"
  - subject: "Re: Kernel Cousin KDE Issue #3 is Out"
    date: 2001-03-25
    body: "Sorry, but I have to disagree. Normally, I only use about 3 fonts in my normal texts. Picking those out of a list of approx. 50 or s.th. is a hassle. But once used, I can switch between those fonts easily with the top 3 entries of the font selection box, disregarding the rest of the list. \n\nThanks a lot for KC. Helps a lot to keep up with the latest development when you don't have the time to read the mailing lists. Chris"
    author: "Chris Naeger"
  - subject: "Re: Kernel Cousin KDE Issue #3 is Out"
    date: 2001-03-25
    body: "I can see your point about it being bad interface design, but it does save lots of time.  If I'm typing a report in msword, I tend to use symbol and times, nothing else.  If I have to go down the list to select these two each time, it's a pain.\n\nHow about the font selection widget shows fonts in use in the document at the top, and then you must click on 'other fonts' to show a complete list of fonts or something.  That cleans up the UI design and maintains the advantage for those of us who like to have currently used fonts at the top.\n\nPaul"
    author: "Paul Mitcheson"
  - subject: "Re: Kernel Cousin KDE Issue #3 is Out"
    date: 2001-03-25
    body: "I like this idea the best. This would really get rid of all the problems that MS word has, please dont add YAOTTOTF (yet another option to turn on/off this feature). that just makes the UI too overwhelming...\n\nreally though, It would be nice to see one of the 2 suggestions implemented. It would be a huge time saver."
    author: "anon"
  - subject: "Re: Kernel Cousin KDE Issue #3 is Out"
    date: 2001-03-25
    body: "\"I can see your point about it being bad interface design, but it does save lots of time.\"\n\nI submit that an interface that makes a common task take longer than it has to is an example of bad interface design.  If the problem is having the same font listed more than once, why not simply move the most recently used font entries to the top of the list?  That breaks \"alphabetical order\", but if the user is looking for one of the fonts that is out of order they will be immediately apparent at the top of the list.\n\nKyle"
    author: "Kyle Haight"
  - subject: "Re: Kernel Cousin KDE Issue #3 is Out"
    date: 2001-03-26
    body: "The big thing here seems to be the problem with listing something in two places, and breaking alphabetical order. How about this : The last 5 or so used fonts will be highlighted, (but remain in their location in the klist) and the tab order will jump to those fonts before going on to the next ones?"
    author: "Carbon"
  - subject: "Re: Kernel Cousin KDE Issue #3 is Out"
    date: 2001-03-26
    body: "Even better, how about having the font size and decoration remembered on \"OK\" or \"Apply\", and having a *completely seperate* drop down of the recently used (most commonly used?) fonts.\n\nFor instance, I often use 18pt Times New Roman Italic, 11pt Arial Bold, 11pt Times New Roman and 10pt Courier New Bold.  If the dialog could remember those four settings and allow me to pick them quickly (with size and other formatting settings), then it would not only save me picking out from the list of fonts, it would also save me clicking option boxes and setting point size.\n\nAnd yes, often this should be done with styles, but even I, who use styles all the time, often just set the font manually for letters or memos.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Kernel Cousin KDE Issue #3 is Out"
    date: 2001-03-26
    body: "I'm not sure I follow. What dialog? What OK or Apply?"
    author: "Carbon"
  - subject: "Re: Kernel Cousin KDE Issue #3 is Out"
    date: 2001-03-26
    body: "Hmm, just compared the idea I submitted earlier below, and I have to say I like your idea better. Espeically, since it displays the font's currently in the document, instead of the last few selected, you can quickly open a new document, and immediately see which fonts are in use throughout the entire thing. Also, how about expanding this quick list to also include font sizes? Could this be done properly at all?"
    author: "Carbon"
  - subject: "Re: Kernel Cousin KDE Issue #3 is Out"
    date: 2001-03-26
    body: "Why not have 2 pull down menus 1 with all the fonts in alphabetical order another with the last 5 used."
    author: "robert"
  - subject: "Re: Kernel Cousin KDE Issue #3 is Out"
    date: 2001-03-26
    body: "I'll admit that I've liked this feature when I've been forced to use Microsoft Word, but I really prefer the approach used in FrameMaker, which allows the user to define character styles that include font, size, color, and probably other characteristics (it has been awhile since I've used FrameMaker and I don't have access to a copy right now).  I've found that with a good selection of paragraph and character styles in a FrameMaker document or template, I very seldom need to use the font selection menu, and it has the added advantage that if I want to change the font associated with a particular style throughout the document, I can apply the change to all uses of that style without searching through the document to find where it was used."
    author: "Allen Crider"
  - subject: "Re: Kernel Cousin KDE Issue #3 is Out"
    date: 2001-03-27
    body: "You mean that we would include the style definition as part of the document? That sounds like a good idea, then, say, a business could build templates and include the sizes and fonts they want their employees to use as a part of the document."
    author: "Carbon"
---
This week in <a href="http://kt.zork.net/kde/latest.html">KC KDE</a>, <a href="mailto:aseigo@mountlinux.com">Aaron J. Seigo</a> summarises 10 threads, including coverage of the new powerful <a href="http://devel-home.kde.org/~kant/">Kant editor</a> in kdebase,  Palm Pilot ioslaves,  KDE sockssupport, a new font selection widget with <a href="http://malte.homeip.net/kfontcombo/screenie.png">font previews</a>, and more.  Get your fix <a href="http://kt.zork.net/kde/kde20010324_3.html">here</a>.

<!--break-->
