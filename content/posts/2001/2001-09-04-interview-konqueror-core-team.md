---
title: "Interview with the Konqueror core team"
date:    2001-09-04
authors:
  - "mmoeller-herrmann"
slug:    interview-konqueror-core-team
comments:
  - subject: "Remove that link"
    date: 2001-09-04
    body: "Don't, just don't, link to that dump."
    author: "reihal"
  - subject: "Re: Remove that link"
    date: 2001-09-04
    body: "What link?\n\n-N."
    author: "Navindra Umanee"
  - subject: "Re: Remove that link"
    date: 2001-09-04
    body: "Some people apparently do not like Slashdot. Agreed, there's a bunch of losers on there. That's why one can browse at +2 or higher. Problem solved. :-)"
    author: "Rob Kaper"
  - subject: "(OT)Re: Remove that link"
    date: 2001-09-04
    body: "No, not these days.  Too many IE using MS goobers who think their 1337 'cause they surf /. and l.c have moderation rights.  One example is where a poster said Mozilla was written in C.  That post was scored 4....  4 for christ's sake.\n\nClueless.  \n\nReal posts are rated 2 or 3 maybe and by the looks of things will soon be 1 because the real linux hackers sysadms and like who do post will be so over the slashweenie's head that he/she will be ignored down 'cause that slashposer wont understand the content.\n\nSTOP going to /.\nIf you want technology, come to the dot. (been getting a large % of /. anyway)\nIf you want culture, go to K5\nIf you want code, go to sf\nbut please leave /. to lamers, gamers and |-|/\\><0R5"
    author: "you"
  - subject: "KDE development contest"
    date: 2001-09-04
    body: "There is a development contest about KDE at the German linux site <a href=\"http://www.pro-linux.de\">pro-linux.de</a>\n"
    author: "gjw"
  - subject: "The price - the KDE deveopment book"
    date: 2001-09-04
    body: "Isnt it ironic that the best KDE2 developer gets the KDE2 development book?"
    author: "ac"
  - subject: "Re: The price - the KDE deveopment book"
    date: 2001-09-04
    body: "I doubt they search the best KDE 2 developer. It's a contest for beginners which can win it by using tutorials and/or online version of the book. Besides the book is only one possible prize."
    author: "someone"
  - subject: "I would like to know from them...."
    date: 2001-09-04
    body: "What happens with reaktivate.\nI tryied compiling it from cvs without sucess, there are a lot of code errors.\nIs this being worked, or was just a \"summer dream\"? :)\nI wish I could find a rpm to test it :)"
    author: "Iuri Fiedoruk"
  - subject: "Re: I would like to know from them...."
    date: 2001-09-04
    body: "Reaktivate is merely a Konqueror plugin, not developed by the core KHTML/Konqueror developers. Ask Malte (malte at kde dot org) or Niko (wildfox at kde dot org) about it, since they are the brains behind it."
    author: "Rob Kaper"
  - subject: "Not the full core team tho"
    date: 2001-09-05
    body: "The famous Mr Faure is missing."
    author: "ac"
  - subject: "Re: Not the full core team tho"
    date: 2001-09-05
    body: "The famous Mr Knoll was missing too..."
    author: "me"
  - subject: "Re: Not the full core team tho"
    date: 2001-09-05
    body: "this interview was \"held\" on the kfm-devel mailing list. basically, the writer posted a series of questions in an email and whoever felt like answering did so. so it wasn't anything very formal or an attempt for inclusivity. and yet ... all the questions got answered. very \"open source\". =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Not the full core team tho"
    date: 2001-09-06
    body: "Wait a sec, if it was held on a kde mailing list, how is it an \"exclusive\" interview?"
    author: "Carbon"
  - subject: "Re: Not the full core team tho"
    date: 2001-09-07
    body: "It isn't, really.  As someone said on Slashdot, \"An exclusive interview means that the interviewees have agreed not to talk to anyone else, not that you were the only ones to bother intervewing them.\""
    author: "not me"
---
<a href="http://www.osnews.com/">OSnews</A> is running a nice interview with <a href="http://www.konqueror.org/">Konqueror</A> developers Dirk Mueller, Waldo Bastian, Carsten Pfeiffer and Simon Hausmann about what the future holds in store for Konqueror. Most of the covered topics are familiar to the frequent dot.kde.org reader, but some stuff -- such as details of the <a href="http://www.atheos.cx/">Atheos</A> port, font handling, and plans for improved CSS2 support -- is very interesting. One of the most fascinating things about open source is that you get to watch the actual development process. Read the whole interview <A href="http://www.osnews.com/story.php?news_id=82">here</A>.  (The story was also covered by <a href="http://www.slashdot.org/">Slashdot</A>, so masochistic inclined people like me might want to read through <a href="http://slashdot.org/article.pl?sid=01/09/03/1830235&mode=nested">those comments</A> as well.)

<!--break-->
