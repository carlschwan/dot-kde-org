---
title: "PC WORLD: XP vs Mac vs Linux"
date:    2001-11-06
authors:
  - "soehlert"
slug:    pc-world-xp-vs-mac-vs-linux
comments:
  - subject: "Rising the ranks"
    date: 2001-11-05
    body: "Good.  Now to improve:\n\nInstallation:  theKompany where is your installer, why miss the boat. apps.kde.com you can strike a deal with Ximian who has the techhow for this, offer subscription. We forget Linux installation itself....\n\nLook and Feel: Go Gallium!  Go Mosfet! Go Qwertz!  Go Tackat!  Go KDE-Artists! Go KDE-Look.org!  We need more nice default themes too, and better theme management.\n\nPerformance: I would say we beat XP hands down already, puzzling...\n\nKeep going and you will be the winner soon!"
    author: "KDE User"
  - subject: "Re: Rising the ranks"
    date: 2001-11-05
    body: "Performance is still not where it should be!"
    author: "kde user 2"
  - subject: "Re: Rising the ranks"
    date: 2001-11-06
    body: "It's true that performance is still nowhere near where we all would like it to be. But:\n\n1) It's not all in KDE's hands, as Waldo Bastians pointed out a while ago in an article about GNU ld and C++. Let's hope some smart people are looking at it...\n\n2) We beat WinXP on \"normal\" machines. Not everybody has a fancy PIII 2GHz 256MB RAM etc. I tried XP on my 500MHz K6-2 with 192MB and it takes more than 10 minutes just to boot, and even then starting apps is still slooowww. KDE on the other hand is much faster: from cold boot to running (Star)Writer 6b in less than 3 minutes.\n\n3) KDE looks better :-)\n\nGreetz\nSteven"
    author: "Steven"
  - subject: "Re: Rising the ranks"
    date: 2001-11-06
    body: "Well, I'm no Windows fan, but I'm not sure why everyone says WinXP is slow. On my PII 450 MHz with 256 megs of Ram it loads in less than 30 sec. flat, although it's a bit sluggish once it gets going... (isn't that the way Windows *always* is?)"
    author: "Timothy R. Butler"
  - subject: "Re: Rising the ranks"
    date: 2001-11-06
    body: "It's a memory problem. XP is a pig in this regard.\n\n(Well, XP _is_ a pig, but that's another flame altogether.)"
    author: "Christian Lavoie"
  - subject: "Re: Rising the ranks"
    date: 2001-11-06
    body: ">I tried XP on my 500MHz K6-2 with 192MB and it takes more than 10 minutes just to boot, \n\nOh, something's wrong there.  One of the big improvements in XP was a big reduction in boot time.  Most people see a 15-30 second reduction over 98/2000 (like me, P3 500 128 MB) and there are reports of 10 second boots!  I think there was some hardware incompatibility issue there or something, your results are not typical at all.  I also don't notice much of a difference in application start times using XP, and everything is much more stable (except for my %*#$* video drivers, but that's not MS's fault).  Overall, on my machine XP is faster, but KDE is improving with each release while XP is not improving.  I have to agree with you about the look though :-)"
    author: "not me"
  - subject: "Re: Rising the ranks"
    date: 2001-11-06
    body: "Darn it!  I even checked to see if the comment had been added before re-submitting, and it hadn't appeared!  Navindra, I hope you get around to fixing this bug sometime :-)"
    author: "not me"
  - subject: "Re: Rising the ranks"
    date: 2001-11-06
    body: "Who boots?!\n\nThe one good thing about Linux is that I don't _have_ to reboot all the time.  It runs and runs and runs.  The only place I'm using XP is in my VMWare installation, so that I can get access to a few things when I need them.  Otherwise, the switch is on!\n\n. . . tizzyD"
    author: "tizzyD"
  - subject: "Re: Rising the ranks"
    date: 2001-11-06
    body: "..., which is the biggest problem. I see many many computers (especially in companies) that are left on running all the time, although they are rarely needed. Though this might be independent from the OS, I find this to be an alarming thing.\n\nToday, a new computer with a large crt can easily use about 500W of power, and people don't know, because it neither is very bright nor very loud ;)\n\nI think KDE (and other OSs/desktops) should have a default setting that the computer shuts down completely after an idle time of around 2 hours."
    author: "me"
  - subject: "Re: Rising the ranks"
    date: 2001-11-06
    body: "Computers in large companies and universities _should_ be left open. (But not their screens).\n\nThink about Sun's Grid thingamiajig, or queueing systems.\n\nYours Truly,\nThe Sysadmin of the Beowulf cluster of the Biomedical Engineering Department of McGill University"
    author: "Christian Lavoie"
  - subject: "Re: Rising the ranks"
    date: 2001-11-06
    body: "Most computers nowadays have power-saving built in.  I think most new computers that you buy in the store automatically turn off their monitors after 10-20 min. and most go into standby in an hour or so.  The monitor is where most of the power is used so this cuts down on a lot of power consumption.  KDE does support monitor power saving, but I don't think it is turned on by default (this could be for technical reasons though).  Linux supports standby, but KDE can't provide support for it because it must remain platform-independent.  This is the job of distros."
    author: "not me"
  - subject: "Re: Rising the ranks"
    date: 2001-11-07
    body: "Umm, sorry to break this to ya, but XP is just as stable as Linux on most desktop configs. And KDE isn't exactly FreeBSD stable yet either.\n\nPS> This was posted from Konqueror on KDE 2.2.1 ;)"
    author: "Rayiner Hashem"
  - subject: "Re: Rising the ranks"
    date: 2001-11-13
    body: "Booya!  I upgraded my Athlon 950 356mb ram Voodoo 5.  Bad news.. not only was everything I had on the system incompatable.  but I couldn't she my sat internet, (it said that the sofrtware I was using was unstable and will not be run..)  But my lovely Voodoo5 nolonger has opoengl support at all.  GRRRR. n I got the system for games and microsoft took it away.  Oh yes and my sat intyernet is mostly owned but microsoft.  \"Ain't that funny!\"\nSkunkyjay"
    author: "Jason BRower"
  - subject: "Re: Rising the ranks"
    date: 2001-11-07
    body: "I think that it keeps memory images somewhere on the disk or makes some another such tricks. It boots 30s for me, but after I slightly modify its configuration ( adding TV driver ) the boot time increases to 1 min 30 s !  But 10 min it is really too much, you must have something broken."
    author: "Me"
  - subject: "Re: Rising the ranks"
    date: 2001-11-07
    body: "I doubt KDE beats XP on normal machines. XP on my machine (300MHz PII, 256MB) is about as fast as Windows 2000, and both are significantly faster than KDE 2.2.1. XP (and 2K) are a little RAM hungry, but once you give them that, everything is blazing. Try this in XP. Start up Visual Studio and Word at the same time, grab the resize bar and whip the thing back and forth and then quickly close them. Then try the same with KWord and KDevelop. On Windows its quite uneventful while its a rubber-banding mess (not to mention incredible load time) on KDE."
    author: "Rayiner Hashem"
  - subject: "Re: Rising the ranks"
    date: 2001-11-10
    body: "wow! what a miracle! for me win2k eats and eats and eats and eats (RAM of course) i can run kde under 32mb ram, can xp do that?"
    author: "Rajan Rishyakaran"
  - subject: "Re: Rising the ranks"
    date: 2001-11-13
    body: "LOL... XP, 32MB LOLOLOLOLOLOLOL"
    author: "tech2kjason"
  - subject: "Re: Installer"
    date: 2001-11-06
    body: "Yes pleeeeease - an installer like windows has !"
    author: "Robo"
  - subject: "Re: Installer"
    date: 2001-11-07
    body: "There's an installer already, it's called Kpackage and is in the kdeadmin module."
    author: "Evandro"
  - subject: "Re: Installer"
    date: 2001-11-07
    body: "RIIGGGHHHHTTT!!!"
    author: "Chris Bordeman"
  - subject: "Re: Installer"
    date: 2001-11-08
    body: "LEEEFFTTT!!!\nUUPPPPP!!!!\nDOOOOWWWWNNN!!!!\nFORRWWAAARRRRDD!!!!!!\n\nOh, wait, what were we talking about again?"
    author: "Carbon"
  - subject: "Re: Installer"
    date: 2001-11-07
    body: "Isn't that the RPM based one? For windows users, that is just far tooo hard. Have you ever used a windoze installer. It's basically one click installation. I hope KDE will have a install / uninstall program like win and RedCarpet soon."
    author: "Robo"
  - subject: "Re: Installer"
    date: 2001-11-07
    body: "kpackage sits on the packaging system of the underlying distro. In debian, it sits on top of apt, and has no dependancy problems (leading to the one-click installation you so sorely want).\n\nThe major problem with mandrake and redhat is that they don't integrate apt into their distro's. Most likely because it's expensive to maintain a huge package database like debian's if you need to actually pay people to do it, instead of depending on volunteers.\n\nYou can't really form an honest opinion about dependancies in linux until you've used debian."
    author: "Joeri Sebrechts"
  - subject: "Re: Installer"
    date: 2001-11-07
    body: "Well...\n\nMandrake already has an installer: Software Manager.\nIt enables you to install/uninstall any RPM without taking care of the dependencies. By default, it uses the Mandrake CDs as package source but you can add (by clicking.. like on win/mac oses) an FTP source.\n\nTo install an app, you just have to select it in the list, then it'll download the app and dependencies if you use FTP source or ask for the CD if you use CD source. It also can uninstall cleanly and keep you aware of package updates and security fixes.\n\nConclusion, in Mandrake 8.1, you have a software that manages installations FAR more easyly than windows does.\n\nMore over, I think the installation is a distribution's matter, not a KDE's matter.\n\nSo, the problem doesn't exist."
    author: "Julien Olivier"
  - subject: "Re: Installer"
    date: 2001-11-07
    body: "So I don't use Mandrake. Or Debian. If I used Gnome (which I don't) I could do it all via Red Carpet. Windows has Windoze Update. Even Mozilla has an easy self downloaded / installer. Why don't you think it's a KDE's matter? I understand someone is writing and Win-like installer for KDE anyways."
    author: "Robo"
  - subject: "Re: Installer"
    date: 2001-11-08
    body: "According to me, it's a distribution's matter because installing binaries sometimes can corrupt a distribution because they won't go in the good folders. I mean that if KDE installer put KDE in /opt/kde while you distrib puts KDE in /usr, you might have big problems with KDE and maybe with other things too. So, whether all the distributions accept to rely only on this installer and don't make their own packages, OR they (and only them) make the installer for their distribution.\n\nI also think more unification would be great. That could happen if such installers appear. But nowadays, installing KDE is not the same on Mandrake, Debian and Suse (well, I imagine so...).\n\nSo, maybe that's a good idea, if it really WORKS."
    author: "Julien Olivier"
  - subject: "Re: Installer"
    date: 2001-11-07
    body: "No, installing with kpackage is easier. You only have to open the file with konqueror and click the \"Install this package\" button.\n\nOn Windows, the setup program differs in each application available. Users find it intimidating to install new programs."
    author: "Evandro"
  - subject: "Re: Installer"
    date: 2001-11-07
    body: "Ok then. So how do I easily update to the latest KDE then? Where is the (single) \"Install this package\" that I grab and single click to install? Windows has about 2 different installers that I have used, and both are incredibly simple. (Well, windows users are a bit simple aren't they?)"
    author: "Robo"
  - subject: "Re: Installer"
    date: 2001-11-07
    body: "You run Debian, and type \"apt-get install kde.\"  It really is that simple.  I can't speak for other distros, but Debian has this \"installing software\" thing down cold.  If you want great package management, you want Debian.\n\nIf Debian ever gets a decent installer/X configurator, it will simply blow all other distros out of the water as far as I'm concerned."
    author: "not me"
  - subject: "Re: Installer"
    date: 2001-11-08
    body: "Progeny always worked for me, as far as being an easy to install Debian.  After that, you have the ease of apt-get, and you can point apt-get to download from standard debian lists, if you want.\n\nDebian just needs to roll in the Progeny installer into Woody."
    author: "J35u5"
  - subject: "Re: Installer"
    date: 2001-11-08
    body: "And do you get a setup program to update Windows?\n\nNo, you don't. To update Windows components you use Windows Update. On GNU/Linux, you can use APT (Debian and Conectiva), RPMdrake (Mandrake Linux), up2date (Red Hat) or YaST (SuSE)."
    author: "Evandro"
  - subject: "SPEED"
    date: 2001-11-05
    body: "With the speed issues being addressed in KDE, I would like to see another comparison done when KDE 3.0 comes out.\n\n<troll>\nThis comes from New Zealand where anyone who is worth a pinch of salt leaves asap due to not getting payed very much.  To bad they come to OZ.\n</troll>"
    author: "shortfella"
  - subject: "Re: SPEED"
    date: 2001-11-05
    body: "Hey! This is Linux and not Windows, so\n\nfirst stability then speed!\nWorking with a stable program is much faster than working with a so call \"fast\" program (believe me :-)\n\nA second one?\n\nFirst security then usability!\nI don't use any installer that forces me to enter the net as user \"root\". \n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: SPEED"
    date: 2001-11-07
    body: "Well, Windows XP has both stability AND speed, so there!\n\nPS> Yes, I use KDE, but not for speed nor stability."
    author: "Rayiner Hashem"
  - subject: "Re: SPEED"
    date: 2001-11-07
    body: "> Well, Windows XP has both stability AND speed, so there!\n\nBut does it have security? Increased security very often can mean decreased speed and usability. For example, *nix OSs are multi-user, with a clear user/root distunction. This brings enormous security benefits, but may be inconvenient for the average user. Consequently, MS have made WinXP Home into a single-user OS (so the user is effectively running as root all the time). It is well-known that Microsoft applications use secret, internal APIs. Not only is this anti-competitive, it is also a great security risk, since these APIs bypass many security checks in order to gain their increased speed.\n\nA few days ago, someone found a way to use Hotmail to crack into other people's Passport accounts. Would YOU trust a company that seems to have no concern whatsoever for security?"
    author: "Yama"
  - subject: "Re: SPEED"
    date: 2001-11-09
    body: "In theory, Windows has tighter security than any free UNIX. In Windows NT, every single object in the system (thread, process, window, file, etc) can have its own access control list. In practice, Windows is full of security holes, but these exploits can mainly be found in the server code. XP (or any other version of Windows) makes a great workstation OS, but no one in their right mind would use it on a server. As it stands, XP's security is more than enough for desktop machines."
    author: "Rayiner Hashem"
  - subject: "Re: SPEED"
    date: 2001-11-09
    body: "> As it stands, XP's security is more than enough for desktop machines.\n\nI assume that you're referring to XP Professional. XP Home, the version that most people will be using, isn't much different from Win9x in that it encourages people to do everything as root. This is the most insecure thing anyone can do."
    author: "Yama"
  - subject: "Re: SPEED"
    date: 2001-11-06
    body: "<anti-troll>\nPay isnt too bad over here and some of us enjoy the lifestyle... why would we go to Aussie?? Its full of Australians *grin*\n</anti-troll>\n\nAs a comparison, I would have to say that Win2K 'feels' quite a bit snappier on my hardware (PII-500celeron with 256mb Ram) than KDE2.2.1 does (Mandrake 8.1). Obviously XP will be slower than Win2k.\n\nHowever... KDE feels snappier than Win98 on my test machine (P100 notebook with 48mb of RAM)?????? This is a Debian Woody install (no sid, KDE2.1??), so maybe the distro comment is relevant and I should try Debian on my main machine.\n\nIn regards to the article... the guy is from a general PC mag, catering to the general PC populace... so I would expect it to be a fairly glossy overview, rather than indepth."
    author: "KiwiFella"
  - subject: "Re: SPEED"
    date: 2001-11-06
    body: "Yes, distro is quite relevant. This has been debated before (by me =), see http://dot.kde.org/991379935/991388835/991410628/#991416971"
    author: "Christian Lavoie"
  - subject: "Re: SPEED"
    date: 2001-11-06
    body: ">This is a Debian Woody install (no sid, KDE2.1??), so maybe the distro comment >is relevant and I should try Debian on my main machine.\n\nnope, no sid KDE2.1 ,there is however a sid KDE2.2 which is even better :-) (it`s supposed to enter woody soon, but there`s some issue on hppa hindering that IIRC)"
    author: "cobaco"
  - subject: "Re: SPEED"
    date: 2001-11-08
    body: "cheers for the comments guys... It is just a straight woody (kde 2.1) install, though I have jumped up to Sid for a couple of things like XFCE (about the only WM that runs at a decent speed on a poor old p100 with 48mb of ram)... and kde definately loads comparitively quicker (alowing for slow hardware) than on my Mandrake... I had a couple of very frustrating nights with Debian, getting my mind around it again.. would you believe that i actually installed debian as my FIRST distro back about 3 - 4 years ago?? from 18 floppies and download shite off the net on a 14k4 modem... managed to get ppp and X and everything going... :)\nAnyway, back to what I was talking baout... Im seriously thinking about installing debian instead of Mandrake on my main pc... even though Mandrake does have a lot of bells and whistles."
    author: "kiwifella"
  - subject: "Re: SPEED"
    date: 2001-11-08
    body: "hehe, Debian was my first distro to (though I installed it from a CD accompanyning the installing debian Gnu/Linux book), the install is definately _not_ as difficult as rumoured (just have to know your hardware). Still, to bad the new installer isn`t going to be finished in time for woody. \n\nmandrake is a good distro(had a brief forray with it as a newbie before I got broadboand, notting beats apt-get and lots of bandwith :-) \nI suspect Debian has all the bells and whistles you need(though you`ll need to apt-get them first as they`re not installed by default) and the debian kde packages are great(tnx Ivan :-)"
    author: "cobaco"
  - subject: "Re: SPEED(troll reply)"
    date: 2001-11-07
    body: "<troll reply>Uhhmm I hate to break it to you but that very same article was in an australian magazine a few months ago :p so shaddap about the kiwis, you'll disturb their sheep\n</troll reply>"
    author: "Dice"
  - subject: "Re: SPEED -> KDE vs Windows"
    date: 2001-11-08
    body: "sorry my bad english.\n\ni use KDE since kde 1.0. I love it :-) But when i see Windows, i think the gui is not so slow by comparison KDE GUI.\n\ni think the reason is hardware acceleration. most of the windows gui-element are \naccelerated by hardware. 4 years ago i programmed from windows and there was the complete GDI-API in hardware accelerated.\n\nsorry i can't my mind to put into words.\nIch probiere es dehalb einfach in Deutsch weiter :-)\n\ndie meisten Sachen unter Windows sind Hardwarebeschleunigt. Die meisten Grafikkarten sind auch darauf ausgerichtet die WindowsAPI zu beschleunigen. Wenn jetzt QT genauso beschleunigt werden w\u00fcrde, dann glaube ich w\u00fcrde das der gesamten KDE-GUI zugute kommen.\n\nIch meine unter der KDE-GUI ist so ziemlich alles langsam im Vergleich zur WindowsGUI. Angefangen beim Fenster verschieben bis hin zu den Men\u00fcs und Rollovereffekten. (sehe ich an der CPU-Auslastung) Ich liebe alle diese Effekte und hab sie deshalb auch eingeschaltet.\n\nKlar sind unter X auch eine Dinge Hardwarebeschleunigt. Aber das sind nur die primitivsten Zeichenfunktionen. Unter Windows hatte man die eigentliche API direkt in Hardware implementiert. !!!!!!!!! Damals bei GDI hatte man es zum Mindestens so\n\n!!!!! Ich will euch nicht zu Windows bekehren. Hab es seit \u00fcber 3 Jahren kein bisschen mehr benutzt. Aber wenn ich bei uns im B\u00fcro mir mal eine Windowsrechner anschaue dann ist, rein subjektiv gesehen, die GUI wesentlich schneller. Vorrausgesetzt nat\u00fcrlich man hat aktuelle Hardware :-))"
    author: "anonymous"
  - subject: "Re: SPEED -> KDE vs Windows"
    date: 2001-11-14
    body: "Cool, ein deutsch-gemachtes Forum ;-)\n\nDer Speed h\u00e4ngt auch viel mit dem scheiss C++-Compiler unter Linux zusammen. Ich mein tolle Sache das man sowas auf die Beine stellt. Aber der C++ Code des gcc ist fast 40 % langsamer als der von visual C++ geschweige gar von den Intel-Compilern. Hier muss man arbeiten. Sicher ist die QT-Beschleunigung auch eine feine Sache. Toll w\u00e4re es auch wenn Bitstream ihren neuen Fontserver (btx) f\u00fcr Privat-Personen kostenlos zur Verf\u00fcgung stellt. Der ist wahrscheinlich viel schneller als diese lahme X+Xft Kombi. Ich denke Linux w\u00e4hre schneller h\u00e4tte es nur \u00e4hnlich viele kommerzielle Unterst\u00fctzungen erfahren wie Windows (der .NET Compiler entsteht auch wieder in Zusammenarbeit ;-) mit Intel)."
    author: "Sebastian Werner"
  - subject: "Re: SPEED -> KDE vs Windows"
    date: 2002-05-30
    body: "Gruss euch!\nWenn MS mit Intel arbeitet, sollte Linux Comunity mit AMD arbeiten - f\u00fcr AMD w\u00e4re es auch vorteilhaft so auszusehen, als ob sie NEUE FREIE WELT schafft.\nCompiler konnte man schon bei AMD entwickeln.\n\nIlia."
    author: "Bizon"
  - subject: "Distro speed differences"
    date: 2001-11-05
    body: "Having used KDE 2.2.1 on Mandrake 8.1, RedHat 7.1 and Debian Sid I would like to know if anyone can answer this question.\n\nWhy is there such a speed difference between distros?  Has anyone else seen this?\n\nTested on the same Hardware with the same services running and the same kernal (Custom kernal I compile)\n\nk6 2- 500, 128MB Ram\n\nDebian Sid (Fast - max 1 sec to load)\nRedhat 7.1 (max 5 sec to load, min 3 sec to load)\nMandrake 8.1 (Slow max 30 sec to load, min 5 sec to load)\n\nAll are supposed to be compiled with object pre-linking.\n\nDebian compiled with 2.95 and the other two with 2.96..."
    author: "Debian User"
  - subject: "Re: Distro speed differences"
    date: 2001-11-05
    body: "When I say load, I mean load an application.\n\nAll pretty much the same once loaded."
    author: "Debian User"
  - subject: "Re: Distro speed differences"
    date: 2001-11-05
    body: "Were the hdparm settings the same on all distros?  Also, the XFree version can make a difference - I think in older versions the font file had to be re-parsed every time you launched an app.  It's hard for anyone to tell you what the problem is if you don't provide more information on what is different between the distros, like what versions of XFree, etc. they use.\n\nAnd if you find the secret, tell me - I want 1 sec. startups for everything too.  I'm running Debian woody w/ some sid on a similar machine and the only things that start in 1 sec. are Konsole and the Control Center (?!).  Everything else takes longer (Konqueror, KMail, and Kate in particular)."
    author: "not me"
  - subject: "Re: Distro speed differences"
    date: 2001-11-06
    body: "hdparm is set the same for each disto.\n\nXFree 4.0.1 is used with each version, though the patch level may be different I didn't check.\n\nAs for fonts, Mandrake I think does install a few extra fonts, but nothing to confirm the speed difference.  AA used with each disto.\n\nKonq., kmail, etc all only take around 1 sec to load."
    author: "Debian User"
  - subject: "Re: Distro speed differences"
    date: 2001-11-06
    body: "perhaps Qt is compiled with g++ exceptions and KDE is compiled with debug info. just a gues, but i find those two things alone can slow down KDE noticeably."
    author: "Aaron J. Seigo"
  - subject: "Re: Distro speed differences"
    date: 2001-11-06
    body: "Hm.  Here, KMail takes 2.5-3 secs to load, and that's after it has already been started two or three times and all loaded into memory.  Konq takes 3-4 secs also, very noticable.  Most other apps take less time, typically .5-2 secs.  Maybe the KDE developers should start looking at why some people have wildly varying start times, on comparable machines even with the same distro."
    author: "not me"
  - subject: "Re: Distro speed differences"
    date: 2001-11-06
    body: "At least Mandrake have KDE with debug compiled in.\nAnd on my 8.1 the XFree version is 4.1.0."
    author: "Peter Simonsson"
  - subject: "Re: Distro speed differences"
    date: 2001-11-07
    body: "At least for mandrake, your info was horribly incorrect:\nit's X 4.1.0\nit is _not_ objprelinked unless you use texstars rpms (and at least\nsome of the older ones were not objprelinked although it was claimed they were).\nAlso, compiler and kernel differences can play a role.\n\nAs for kmail: I think it is the size of the mailbox that matters a lot and accounts for variability.\n\nDanny"
    author: "Danny"
  - subject: "Re: Distro speed differences"
    date: 2001-11-07
    body: "The kmail in kde 2.1 was too slow to handle my mailboxes with only a couple hundred messages in them. Contrast that with netscape 4.x on windows which was able to parse the exact same mailboxes immediately after the first time, because it made indexes, which kmail obviously doesn't do."
    author: "Joeri Sebrechts"
  - subject: "Kmail"
    date: 2001-11-07
    body: "Sorry if the following sounds lke a troll - it really isn't.\n\nKmail is a total dog. I was a loyal Kmail user from KDE1 up to KDE 2.2. From that point, I became fed up with its slow speed and large memory usage. How can a simple mailer take up 30MB of memory? Since then, I have switched to Sylpheed (Claws edition), which is lightning-quick and only takes up 10MB of RAM."
    author: "Yama"
  - subject: "Re: Kmail"
    date: 2001-11-07
    body: "You have been misled about the memory usage of KMail.\n\nFirst off, whoever compiled your KDE made some mistakes or something (left debugging info in?).  On my system, top reports a value of ~15 MB for \"size\" for KMail.  This doesn't tell the whole story, though.  Much of that memory is not actually part of KMail.  It includes all of the KDE shared libraries, which are shared among all KDE programs as their name implies.  If you are running any other KDE program, these libraries are already in memory and don't contribute to KMail's memory usage in reality.  top reports that KMail is using 11 MB of shared memory, which means that KMail's actual memory usage is 15-11 = 4 MB.  That's much better, don't you think?\n\nThe numbers I get from KPM agree with top.  KSysguard, on the other hand, reports totally different, wacko numbers.  Maybe that's why you thought KMail was using 30 MB of RAM.  Here's how to find out how much memory a KDE program is really using:\n\nRun KPM or top, not KSysGuard.\nSubtract the \"share\" column from the \"size\" column.\n\nAs for slow speed, see the other comments here about that.  Debian sid seems to be the fastest.  On my system, KMail takes almost exactly the same amount of time to start as Konqueror.  It doesn't seem to depend on how many messages I have either.  Kmail quite easily handles 2000+ messages in a folder with no noticable slowdown."
    author: "not me"
  - subject: "Re: Distro speed differences"
    date: 2001-11-07
    body: "I use Madrake 8.0.... on a Dual Celly 400@500 and 512MB RAM...\n\nAnd if I could get *just konsole* up in less then 5-6secs i would give my left foot (otherwise no typing ;-). Kword is so slow to launch that I have forgotten what I wanted to write.... grip is there in a WIZZ (gnome app - uagh!)\n\nI am frustrated by this. So frustrated that when KDE 3.0, g++ 3.0 and the new linker is out I am going to try giving a handbuild LFS a shot (Linux From Scratch... www.lfs.org). If this doesn't work I'll ask my doctor for a huge glass of Valium!!! ;-)\n\n/kidcat\n--\nhardtoreadbecauseitissolong"
    author: "kidcat"
  - subject: "Re: Distro speed differences"
    date: 2001-11-07
    body: "I`m using KDE 2.2.1 on a LFS system /3.0/ and so far I can say it`s much faster than it was on my old system Mandrake 8.1 ... You really should try it, you`ll be surprised ;)) There is a komplete hint how to kompile KDE 2.2.1 with objprelink..."
    author: "maznio"
  - subject: "Re: Distro speed differences"
    date: 2001-11-06
    body: "it could be mandrake and redhat have a 'fuller' kde install: more .desktop and rc files to parse -> slower. i don't know if this should effect speed, but i think it does.\ni wonder if there is a speed difference when you do rm -rf .kde ..."
    author: "ik"
  - subject: "Re: Distro speed differences"
    date: 2001-11-06
    body: "> Redhat 7.1 (max 5 sec to load, min 3 sec to load)\n> All are supposed to be compiled with object pre-linking.\n\nI don't know about the others, but regarding redhat, I can tell you that their KDE rpms do NOT use pre-linking.  I rebuilt them WITH prelinking, and achieved much better load times.  YMMV."
    author: "Rex Dieter"
  - subject: "Re: Distro speed differences"
    date: 2001-11-06
    body: "Neither do Mandrake's. \n(8.2 ones likely will)."
    author: "A Sad Person"
  - subject: "Re: Distro speed differences"
    date: 2001-11-06
    body: "Is there any documentation on why Red Hat and Mandrakesoft didn't use the object pre-linking patch?  I thought it would make sense to make their distros as fast as possible.\n\nWhat's the status of other distros?"
    author: "Debian User"
  - subject: "Re: Distro speed differences"
    date: 2001-11-06
    body: "There have been mysterious crashes when using pre-link."
    author: "Peter Simonsson"
  - subject: "Re: Distro speed differences"
    date: 2001-11-06
    body: "For Mandrake, there have been some discussions on the Cooker list..\nBut basically, I belive they were simply worried about not having adequate\ntime to test it.."
    author: "A Sad Person"
  - subject: "Re: Distro speed differences"
    date: 2001-11-06
    body: ">  k6 2- 500, 128MB Ram\n>  Debian Sid (Fast - max 1 sec to load)\n>  Redhat 7.1 (max 5 sec to load, min 3 sec to load)\n>  Mandrake 8.1 (Slow max 30 sec to load, min 5 sec to load)\n\nWith Mandrake 8.1, I changed from AMD k6 500 (416Mb Ram) to AMD Duron 850 (always 416 Mb Ram) and there was a great difference. Konqueror was 7 x quicker for opening a big Html page... 36 s --> 5 s ! (I also changed the mother board)"
    author: "Alain"
  - subject: "Re: Distro speed differences"
    date: 2001-11-06
    body: "Your factors to consider and debug code, stripped binaries, exception handling, code optimization and machine configuration. The first three all contribute to executable size, and the difference between loading 300K and 3Megs is very noticable. Code optimization can speed things up considerably, but not much in the way of load time. Finally your machine configuration makes huge difference. ext2 vs ext3 vs reiserfs and ata33 vs ata100 all affect the time it takes to transfer the binary from harddisk to memory.\n\nI don't think any of these have object pre-linking. IMHO, just wait until g++ gets the linking crap fixed and don't bother with hacks."
    author: "David Johnson"
  - subject: "Re: Distro speed differences"
    date: 2001-11-07
    body: "Did you install all Systems in parallel in different partitions, or all sequentially in the same partitions?\n\nIf the first is true, which system was on which partition number / disk?\n\nStefan"
    author: "Stefan Heimers"
  - subject: "Arbitrary"
    date: 2001-11-05
    body: "This guy's decisions seem pretty arbitrary.  He doesn't like Linux installs because they lack \"slickness,\" whatever that means.  He prefers the KDE look but that's just personal opinion.  He bashes the Mac's usability, saying it feels \"awkward\" and \"like a throwback to the single-tasking 1980s,\" just because he isn't used to it.  He hates the new XP Start menu without telling us why (personally, I think its an improvement).  He doesn't even attempt to get equivelant systems to test the speed of the OSs.  I think he's just spouting his pre-formed opinions."
    author: "not me"
  - subject: "Honest"
    date: 2001-11-06
    body: "Of course, such a comparison may only be subjective, so arbitrary. Not wholly, but in good part.\n\nThe first interest is the existence of this comparison. It is rare. Why ?\n\nI think that it is done honestly, exposing clearly the reasons of the choice. The reader can feel whether it is objective or subjective, and whether the subjectivity is similar to it's own feeling.\n\nThe bigger lack is the comparison of the prices. Here Linux/KDE is the first, but I hope that anybody knows such a thing...\n\nMe, I am glad to read that in XP \"The new Start button is abominable\". It needs more journalists to say such things about some ridiculous useless flashy \"feature\". What an improvement to put in 2 little columns the content of 1 greater ! And happily this \"feature\" may be disabled, but some ones, irritating as the half/full menus cannot be disabled, it seems... I still wait that some journalist say it is \"abominable\"...\n\nAnd about the Mac, I am OK that the usability now feels old. Perhaps, the usability is in proportion with the number of buttons of the mouse, 1 for Mac OS, 2 for Windows XP, 3 for Linux/KDE (however, for KDE, it will be fully efficient only in version 3.0). When you are accustomed to use 3 buttons, it is a regression to use only two buttons, and worse to use only one... In the other way, when you are accustomed to use 2 buttons, you succeed easily to use 3 buttons, and you feel it is a progress..."
    author: "Alain"
  - subject: "Re: Honest"
    date: 2001-11-06
    body: ">What an improvement to put in 2 little columns the content of 1 greater!\n\nThe main improvement of the XP start menu is that the six most often used applications are automatically put in big giant buttons for easy access.  This is a really nice feature once you get used to it.  Also, the fact that they moved the programs menu down closer to the start button is nice, it means it's easier to hit with the mouse.  They didn't just rearrange it randomly like you seem to think.  I do agree that the menus that hide items are annoying, and I wish they could be disabled.\n\nThe Mac's usability is a matter of personal preference.  Some people prefer it greatly.  I don't think this guy can say absolutely that it is bad.  He should have said that it is different and hard to adjust to if you've never used it before, which is true.\n\nI doubt the usability is proportional to the number of mouse buttons.  You would be surprised how many people use Windows without ever clicking the right mouse button.  For some reason it is hard for some people to grasp."
    author: "not me"
  - subject: "Re: Honest"
    date: 2001-11-09
    body: "As for me, I have switched first from the Amiga, to Linux, and now to Mac OS X 10.1.  I had no problem at all with usability with the Mac.  In fact it seemed simpler and more user friendly than Linux/KDE 2.  Though now I am building another box just for Linux/KDE 3 :)  There is no sense of single-tasking at all, no idea what he's talking about.  Anyway, I find both Linux/KDE and Mac OS X great for ex-Amiga fans, as they both are an integrated GUI with a lot of power \"underneath\".  In fact I see some shared ideas.  For example, this text I'm writing into a web browser text field is being spell-checked as I write.  This is because (I'm sure) that all multi-line text fields used in this system go back to the same code, which is also true of KDE 3 (the new text widget) so therefore features on one can be used on every one.  Anti-aliased text absolutely everywhere (beautiful fonts as well), and all running on BSD.  Actually, (this may be of some interest to some) I have seen a project that proposes to port KOffice to Darwin (Mac OS X's core) such that KOffice will run just fine on Mac OS X.  Since X runs XFree 4.1.0 just fine (and rootless!) and since KDE runs fine on BSD, I don't see this as too painful of a port.  In fact, since QT is soon (or now?) ported to the Mac, one could go two different ways.  First, get KDE to run under XFree on the Mac or get it to run more natively, depending only on the Mac QT.  Wouldn't it be great to beat StarOffice to this platform!  And the KDE environment would automatically inherit all those pretty fonts!\n\nErik"
    author: "Erik Hill"
  - subject: "Re: Honest"
    date: 2001-11-09
    body: ">  The main improvement of the XP start menu is that the six most often used applications are automatically put in big giant buttons for easy access.\n\nIt is useless. My most often used applications are in a launch bar. 1 click instead of 2 clicks using the start menu.\n\n> You would be surprised how many people use Windows without ever clicking the right mouse button.\n\nI know. They have a lack of performance.\n\n> For some reason it is hard for some people to grasp.\n\nWhat is hard ? Only changing old habits. In the same way, how Windows users replace the double click by a simple click ? Few, although it is easier...\n\nYou know, it is possible to use Linux \"a la Mac\", with only one-click mouse and menus on top... If it were so peformant, there were many people working like that. They are very few... When you use 2 ou 3 times the third mouse click, you understand its importance and you use it... Yes, Linux/KDE is the most usable desktop."
    author: "Alain"
  - subject: "Re: Arbitrary"
    date: 2001-11-06
    body: "Well I can't tell you why he said those things but I can offer some guesses based on my experience.\n\nI think the linux installers are getting very good so it's hard to say what he didn't like about them.  I think many are still revealing too much of the underlying process, which is fine for advanced users and should be an option to see at the beginning of the install.  I think one of the best installers I've used is Progeny Debian's installer, of course I think it had some quirks for a lot of people.\n\nThe MacOS (we're talking about about 9.x right?) is very very screwy.  I use them from time to time here at my college because I really like the interface and  IE on the mac is actually pretty damn nice.  The problem is definitely with the multi-tasking, or lack of.  It feels like a dirty hack.  When I scroll pages in the browser everthing else on the screen stops moving.  When one program tries to do something it will often cause other programs to lock up.  And trying to use java applets in the browser is one of the most frightening things I've ever done.  The whole system would stall for about 30 seconds while it was loading the applet.  You can argue that these are problems with IE but I'm pretty sure it's a problem with the way MacOS 9 tries to do multi-tasking.\n\nAs for the default XP start menu, I actually think it's a nice idea to have the most used programs available.  Although I don't like how most of the menu is taken up for advertising microsoft services, like MSN.  And it's very slow.  I have a dual 400mhz celeron system which was fast as hell with linux but the menus in XP feel soooo damn slow.  Even though it's probably just taking 1 or more seconds on average to show a menu that is really too long when you are trying to navigate through categories.  I want them to zip up so I can whiz through them and start a program before I forget what I was trying to run :)\n\nAs for KDE I think it's very nice.  I love the level of integration it has.  It really shows what can be done with open source code.  The only programs in Microsoft land that have anywhere close to that level of integration are microsoft programs.  They don't seem to want anyone else to plug into their system.  Whereas on KDE it's very open for people to integrate stuff right into the desktop, such as making KIO slaves and stuff.  Very nice.  For some reason though I keep coming back to GNOME even though it has pretty much no integration whatsoever, I think I just like the smooth alpha-blended gnome-canvas desktop that nautilus provides, hopefully KDE 3.0 can achieve a desktop like that without being slow as a dog like nautilus :)"
    author: "Travis Emslander"
  - subject: "Re: Arbitrary"
    date: 2001-11-06
    body: ">The MacOS (we're talking about about 9.x right?)\n\nNo, OS X.  OS X runs on a cousin of Linux, so it should have great multi-tasking.\n\nI haven't found slowness of the XP start menu to be a problem, though I have noticed it being a little slower than the old one if you haven't used it in a while.  I suppose on a slower system it could become a problem.\n\n>I just like the smooth alpha-blended gnome-canvas desktop that nautilus provides\n\nHuh?  I didn't know the GNOME desktop had any special features.  What's the difference?"
    author: "not me"
  - subject: "Re: Arbitrary"
    date: 2001-11-06
    body: "OS X has great multitasking alright, but it chews up memory alive. There's a load of stuff on ArsTechnica about it - basically the screen buffer alone comes to 200MB or greater on a reasonably busy session, and then you've got all the processing required for the effects. You basically need a small supercomputer to get it running smoothly, even with the improvements that 10.1 brought.\n\nAs for Gnome, if you run Nautilus you get anti-aliased fonts on your desktop (it looks a bit better than AA with XRender), with a nice rounded bounding box for the filenames, and a translucent blue selection square when you drag the mouse to select several icons at once. It's extremely purdy."
    author: "Bryan Feeney"
  - subject: "Desktop managers"
    date: 2001-11-07
    body: "One reason why Nautilus is so slow is that it tries to do things that GNOME itself cannot do. Anti-aliasing, for example, is a Nautilus-only extension (I assume that other GNOME apps could use it if written to do so) that doesn't use the standard XRender (which would be faster).\n\nNautilus is getting faster with each new release. However, there are several other things that can be done to speed up GNOME (in order from slowest to fastest):\n\n1. Turn off anti-aliasing in Nautilus.\n2. Use GMC to manage your desktop instead of Nautilus (i.e. Nautilus isn't even running in the background).\n3. Turn both GMC and Nautilus off. This will leave you with a bare desktop, but your system should be much quicker.\n\nSpeaking of turning off desktop managers, it would be nice if KDE could have the option to turn off Konqueror's desktop management."
    author: "Yama"
  - subject: "Re: Desktop managers"
    date: 2001-11-07
    body: "simply dont start kdesktop\n\nAlex"
    author: "aleXXX"
  - subject: "Re: Desktop managers"
    date: 2001-11-07
    body: "Easier said than done, since we don't have any idea when or how kdesktop actually gets started.  What is it you have to change to keep kdesktop from being started?"
    author: "not me"
  - subject: "Re: Desktop managers"
    date: 2001-11-07
    body: "If you create an exectuable via the right-click menu in ~/.kde/AutoStart/ and set it to execute \"killall kdesktop\" it'll shutdown the desktop when KDE finishes loading. A bit of a hack I know. Anyone got any better ideas?"
    author: "Bryan Feeney"
  - subject: "I agree (mostly)"
    date: 2001-11-07
    body: "I mostly agree. \n\nThe new XP start button is better in many respects. Accessing common locations in the filehierarchy and launching frequently used apps is faster. I think many people simply dislike it because it is so big. That was my initial thought at least. I didn't see a point in it beeing so big but I quite like it now that I have used it for a while.\n\nThat Mac usability is a throwback to single-tasking in 1980s is plain stupid. It is different yes and takes some time getting used to but there are many benefits that can be pointed out.\n\n1.Just having one menu saves screen realestate.\n2.Through the famous Fitt's law it can be proven that it is faster to access the mac menu than in similar Windows menus.\n3.You can use 2 button mouse with scrollwheel out of the box in OS X. Just plug in your Logitech USB PC mouse and it will work without the need to install any drivers.\n\nAs for OS installation. He does have a point allthough he maybe use the wrong words. OS X is easier than XP and XP is easier than Mandrake. Allthough they are all pretty close to each other. And for the fun of it BeOS is even easier than OS X."
    author: "Erik Engheim"
  - subject: "Great!"
    date: 2001-11-05
    body: "It's nice that KDE is finally getting some credit, especially in the software-dept. - what the author fails to point out, however, is that a Linux distribution comes with all these programs *for free* whereas the respective MS- and non-MS-products for Windows cost thousands of dollars. Because of that, and not because of \"anti-Microsoft\" attitudes, I would expect at least a tie between XP and KDE. OTOH, some Linux usability issues remain unaddressed (installing the boot-manager, detailed hardware config, configuration of XFree86, installation of software, lack of documentation, use of nonstandardized configuration files, ...). So I think the outcome remains fair, since KDE/Linux ;-) still lack a lot of the usability Windows has in many areas."
    author: "FooBar"
  - subject: "Re: Great!"
    date: 2001-11-06
    body: "> some Linux usability issues remain unaddressed (installing the boot-manager,\n> detailed hardware config, configuration of XFree86, installation of software,\n> lack of documentation, use of nonstandardized configuration files, ...)\n\nWhat do you mean \"unaddressed\" ?\nHave you ever used a distro (like Mandrake) ?"
    author: "AC"
  - subject: "Re: Great!"
    date: 2001-11-20
    body: "I have to agree with AC.  Try using Mandrake 7.2, 8.0, or 8.1.  They covered your \"unaddressed\" issues over a year ago.  Some or it even earlier than that.\n\nI am curious... When was the last time you installed or tried any distro of Linux?  If your gonna compare Microsoft's latest OS, the least you can do is compare it to the latest and greatest distro of Linux.\nI like the new Windows XP Gui enviornment, but that is all I like from it."
    author: "Neotrode"
  - subject: "Re: Great!"
    date: 2001-11-20
    body: "Let me correct my questions to:\nWhen was the last time you UPDATED your Linux box?"
    author: "Neotrode"
  - subject: "KDE on my laptop"
    date: 2001-11-06
    body: "I recently got an old laptop (Compaq 5000LTE, P75, 16Mb RAM, 2.1Gb HD) for free. \nNow, i would like to install linux/KDE on it(I run SuSE 7.1/KDE2.2.1 on my desktop machine). I know I can't install Xfree window system as it requires 32Mb \nof RAM. So what are my alternatives, are there any other window systems that KDE would run on? \n\nGraphics require alot of resorces from your computer by default, I know. But it would be nice to be able to scale down things to get them running on slow/old H/W too. \n\nDon't say that I'm stuck with win95 currently installed on the laptop. Please give me suggestions on what to do. Another OS maybe? I'm open for anything as long as it isn't M$ and that it can be downloaded from somewhere for free.   \n\nThanks for your help. Sorry that this comment is a bit off topic.\n\n/Albert"
    author: "Albert Thuswaldner"
  - subject: "Re: KDE on my laptop"
    date: 2001-11-06
    body: "Well, with 16 MB of RAM it's hard but possible.\nUse kernel 2.0x., disable all daemons you don't need (crond, atd, portmapper, sshd, ....), compile the kernel yourself and exclude everything you don't need (do you have a printer on that laptop, no ? well, skip the parallel port, and so on). Reduce the number of terminals. Strip all binaries you have. \nAnd then try X with xfce, KDE is too much.\nForget about staroffice or koffice, try wordperfect instead. I don't know about abiword.\n\nAll in all, it will work, but it won't fly ;-)\nPut more memory in the box.\n\nYou could also have a look at AtheOS, don't know how much this requires.\n\nBye\nAlex"
    author: "aleXXX"
  - subject: "Re: KDE on my laptop"
    date: 2001-11-06
    body: "Yes! XFCE is great! I like GNOME, KDE etc., but I choose XFCE + GNOME panel (not all the time - just running it occasionally) + some KDE apps. Works great, looks great, does not use tons of RAM, and it is really fast. Perhaps you should give it a try.\nP.S. I have installed XFCE (pure - no GNOME or KDE) on my old laptop (p60, 16MB RAM, 80MB hdd (sic!)), so it is not a problem."
    author: "Debian GNU/Linux User"
  - subject: "Re: KDE on my laptop"
    date: 2001-11-06
    body: "xfce + gnome + kde apps makes you load\n- the gnome libs\n- the kde libs\n- some gnome & kde specific daemons\n- xfce specific stuff ?\n\nlooks like something that uses a lot of ram to me"
    author: "ik"
  - subject: "Re: KDE on my laptop"
    date: 2001-11-08
    body: "I have a P75 laptop with 16MB ram.  Because it's sooooo old I can afford to run XFree Version 3.something.  I run Windowmaker on top of it.  Although it's not as nice as KDE2 it does seem to be a lot quicker.  The advice on recompiling your kernel is good.  Take a note of what gcc libs you are using (eg: I use glibc 2.2) go to the XFree website and download the Linux binaries of your chosen XFree version for your libc libraries.  Compile kernel, strip out all unnecessary carp, install Xfree binaries, and compile Window maker sources.  I recommend installing XFree binaries as compiling from source is tricky unless you read the documentation THOUROUGHLY!!!"
    author: "balor"
  - subject: "Re: KDE on my laptop"
    date: 2001-11-06
    body: "If you can let the framebuffer work with your vga card, you cant try the Q palmtop environment."
    author: "b"
  - subject: "Re: Try IceWM"
    date: 2001-11-06
    body: "I prefer IceWM for low memory situations.\n\nhttp://www.icewm.org"
    author: "Scott Patterson"
  - subject: "Performance of KDE"
    date: 2001-11-06
    body: "I'd have to agree with the people responding here. I absolutely love using KDE, and hope to soon be able to help develop it. The only thing that bothers me is that it's kinda slow. You can specialy see this when launching applications. If I compare this to launching for exp. Gnome applications, there is a big difference. KDE is more friendly to its users than any other desktop for Linux, expecialy for Central European users like myself (localisation, keyboard layout etc) and is nicer with all the themes and stuff. But it would benefit from more speed in general and I think it can be done with some code optimisation. Anyway, congratulations to the KDE team on a job well done and keep it up. Hope to see KDE 3 in lightnig speed."
    author: "Tomislav Lukman"
  - subject: "Re: Performance of KDE"
    date: 2001-11-07
    body: "KDE itself is as fast/if not faster than GNOME in launching apps. However, in some compilers (particarily g++), a number of relocations must be done in C++ programs. This is what you are probably noticing. Until this problem with g++ is fixed, you can try using objprelink, which should reduce the number of relocations done. This should, in turn, make apps launch around 30% faster."
    author: "off"
  - subject: "Re: Performance of KDE"
    date: 2001-11-08
    body: "I thought there had to be a catch. Does anyone know where one could find documentation about tuning KDE performance?\nTHX"
    author: "Tomislav Lukman"
  - subject: "OT: Firewall Logs"
    date: 2001-11-06
    body: "Is anyone else seeing things like this:\n\nNov  6 13:37:33 aragorn kernel: FireWall (Invalid): IN=ppp0 OUT= MAC= SRC=213.203.58.36 DST=[my IP] LEN=40 TOS=0x00 PREC=0x00 TTL=239 ID=43179 PROTO=TCP SPT=80 DPT=32858 WINDOW=0 RES=0x00 RST URGP=0\n\nAnd just to say it:\n\n[root@set /]# host 213.203.58.36\n[my IP].in-addr.arpa domain name pointer www.kde.org.\n\nOn top of that, I'm not even at home (which this log paste is coming from), I'm at work tailing my log at home, so I'm browsing the Web or anything.  My box is just sitting there.\n\nAnyone?"
    author: "Xanadu"
  - subject: "Re: OT: Firewall Logs"
    date: 2001-11-07
    body: "You probably have the kNewsTicker running and configured to check www.kde.org for new announcements or some stuff like that."
    author: "Joe Schmoe"
  - subject: "Re: OT: Firewall Logs"
    date: 2001-11-09
    body: "Geeze, I can be such a dolt sometimes.  You are correct, I am running KNewsTicker.  I was thinking of not since it's not clickable anymore, but it's still handy to have running.\n\nThanx man!"
    author: "Xanadu"
  - subject: "Tuneup documentation?"
    date: 2001-11-08
    body: "Seeing all the talk about KDE speed made me ask - Where could one find documentation about tuning KDE performance?"
    author: "Tomislav Lukman"
  - subject: "Re: Tuneup documentation?"
    date: 2001-11-21
    body: "There is an excellent guide to recompiling KDE and other components for performance at http://hints.linuxfromscratch.org/hints/kde.txt.  I'm part-way through this procedure at home (currently recompiling QT with objprelink included)."
    author: "Malcolm Street"
  - subject: "Re: Tuneup documentation?"
    date: 2001-11-21
    body: "It is useful to note that objprelink only speeds up load-time (runtime linking) of c++ objects, *not* the app itself.  Starting new windows will be more responsive, but the difference otherwise is negligible.\n\nIncidentally, if anyone wants 'em, my KDE 2.2.2 RPMs for RH72 are up and are objprelink'd.  =)  ftp://ftp.opennms.org/people/ben/redhat-7.2/\n\nI've also got in there RPMs of some extras like liquid and other style themes, and the kio_fish ioslave, which rocks my freakin' world."
    author: "Ranger Rick"
  - subject: "All Linux and Mac user are MORON!!!"
    date: 2002-01-11
    body: "You cannot compare with XP power..."
    author: "XPDC"
  - subject: "Re: All Linux and Mac user are MORON!!!"
    date: 2002-01-12
    body: "Really?  And I was just talking to my Uncle over X-mas break.  He's an IT person at a large university in my area, and he was talking about how they are removing all of there XP installs, and going back to 2000...  How XP is , and I quote, \"A wothless waste of disk space\", and how, \"2000 is MS's peak, and it's all back downhill after that\"...  By the way, he runs Linux exclusively at home...  Probably even had a great deal to do with me installing Linux...  So, I guess, have fun with your POS XP, and don't ask for help when it fails on you...   ;-)"
    author: "Ill Logik"
  - subject: "Re: All Linux and Mac user are MORON!!!"
    date: 2002-10-02
    body: "Ignore that kid behind the shed.\nJust another cattle-brain who's been sucked into the M$ assimilation whirlpool.\nMORON = \"Mentally Original Ruminating OS Nurturer\"; I'll accept that handle.\n;)\n\n\n"
    author: "Moron"
  - subject: "Re: All Linux and Mac user are MORON!!!"
    date: 2004-04-23
    body: "Really? At least us Mac and Linux users can spell and use grammar correctly.\nAnd a point to say that when those Windows users say \"Duh Mac copied Windows!!\", I like to explain to them that Mac invented a GUI 7 years before Windows 3.1 ;)\nEverything that Apple Computer has done has been somehow stolen from them...the GUI... the WalMart music store....and I agree with the comment about Windows 2000 being the peak of Microsoft, but I prefer the Pro edition.\n"
    author: "Khas"
  - subject: "Re: All Linux and Mac user are MORON!!!"
    date: 2007-08-14
    body: "I would be very sorry to refute your first statement. Its supposed to be \n\n\"At least we Mac and ...\"\n\nAnd, those who use macs and windows sort of really don't know what they are doing. I very much prefer linux, but in the field of hard core graphics work, PC's will be the beast."
    author: "theani"
  - subject: "Re: All Linux and Mac user are MORON!!!"
    date: 2005-10-12
    body: "Oh yeah?   AND WHAT ABOUT BOB ?"
    author: "miklos harsszegi"
  - subject: "Re: All Linux and Mac user are MORON!!!"
    date: 2007-09-05
    body: "have you tried linux or mac?"
    author: "someguy"
---
<a href="http://www.pcworld.co.nz/pcworld/pcw.nsf">PC World New Zealand</a> compares <a href="http://www.pcworld.co.nz/PCWorld/pcw.nsf/UNID/ACC21A8F154C34B0CC256AED006F7668?OpenDocument">WinXP, MacOS X, and Linux/KDE2</a> and concludes that Linux/KDE2
offers the best usability, tieing XP in features/support.  XP does win overall, but we should be proud that our little free operating system/environment built out of passion and dedication has come so far in so short a time.
<!--break-->
