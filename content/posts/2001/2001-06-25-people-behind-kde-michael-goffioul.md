---
title: "People behind KDE: Michael Goffioul"
date:    2001-06-25
authors:
  - "Inorog"
slug:    people-behind-kde-michael-goffioul
comments:
  - subject: "Thank you Michael Goffioul, you look handsome :)"
    date: 2001-06-25
    body: "Thanks a million for creating and improving Kups! Hope KUPS+CUPS will be the printing system of Unix Desktop Environment (i.e., Kool Desktop Environment :) My good wishes to you and best of luck.\n\nI really loved Kruiser, still Konqueror doesn't have kruiser's ease of use; example:\n\n1. Kruiser had (perhaps) MDI interface and each window can be minimized, maximized and closed.\n2. Likewise each window has its own location bar which gives proper folder location.\n3. Kruiser was very fast in displaying files (but slower than xwc file manager).\n\ncould you please help David Faure to add the missing features that Konqueror lacks !? (mentioned above) Thanks again for your hard-work."
    author: "Asif Ali Rizwaan"
  - subject: "Konq .. the slow beast"
    date: 2001-06-25
    body: "That is why I can't use Konq for file viewing. Watching paint dry is quicker than Konq on my LM8 installation on a PII 400 with 192mb RAM.\n\nKonq the browswer is almost kewl .. more support for javascript would make it gr8 as my internet banking is broken.\n\nKonq is like the little train that thought he could. He is in the stage of pulling his way up the hill with determination. When he reaches the top of the hill, he will be the little browser that did beat the best of them.\n\nThree cheers to the little train."
    author: "kde-user"
  - subject: "Re: Konq .. the slow beast"
    date: 2001-07-01
    body: "Yeah KDE and newer distributions of Linux FreeBSD etc are mostly unusable on older machines (I have a pII-233 w/ 256 megs RAM and many apps are almost unusably slow). The problem is worse than with Windows (Win2K's GUI is snappy and fast on the smae machine). I think it's because KDE/Linux developpers get fast machines to work with and there's no real need to check optimize etc so they can sell \"upgrades\" to users of old machines (which is a good thing in other ways).\n\nHopefully when konqui gets to the top of the hill it'll be downhill afterwards and the gravity will speed things up ;-)"
    author: "Slow User"
  - subject: "Re: Konq .. the slow beast"
    date: 2001-07-02
    body: "The problem with KDE is that it is built on top of the XWindows system. It is a 32bit platform built on top of a 16bit architecture. Work needs to be done to replace XWindows completely with some thing faster."
    author: "Philip Neves"
  - subject: "Re: Konq .. the slow beast"
    date: 2002-01-28
    body: "Unfortunately I shall agree, that KDE in general and Konqueror in particular a way too slow. It's not fair to criticise the people who give you software (in all the other ways really nice software) for free :)), but I just can not use it. It just plain drives me mad!!! Watching Konqueror start up is pain, and waiting for it to draw the content of the web pager, is a *real* pain. I even start to like Windows 98 again, it is at least five (if not twenty) times faster. Extremaly slow speed just renders otherwise nice KDE unusable, more like a toy not the real working environment. Please, please somebody, who can do something about it, please do it and you will be mentioned in prayers!"
    author: "critter"
  - subject: "Re: People behind KDE: Michael Goffioul"
    date: 2001-06-26
    body: "Looks like no-one is very excited about the improvements in the printing system :) ...\n\nWell, I am! Finally KDE integrates really well with the lp replacements like CUPS which have been springing up for the last few years. I remember looking at KDE 1 and wishing someone would improve the printing system in just the way Michael has improved KDE 2... It's a pity that it's not given more attention."
    author: "Jon"
  - subject: "Re: People behind KDE: Michael Goffioul"
    date: 2001-06-26
    body: "Has someone of you any hints for me to get my KDE-2.2alpha2 release (SuSE binaries)  running kdevelop?\n\nSome libs are missing...:-(\n\nSunWave1>kdevelop\nkdevelop: error while loading shared libraries: libkdevelopvc.so.2: cannot open\nshared object file: No such file or directory\n\nSunWave1>rpm -qa|grep kdevelop\nkdevelop-2.2alpha2-0\n\nThanks,\n Dieter"
    author: "Dieter N\u00fctzel"
  - subject: "Re: People behind KDE: Michael Goffioul"
    date: 2001-06-27
    body: "That is an error in the SuSE package, an updated one can be found on ftp.suse.com but I'd advise to either grab the CVS version (hint: join us at irc.kde.org on #kde and you'll get help) or wait for the beta package to come out later this week (or monday I guess)\n\nRalf"
    author: "Ralf Nolden"
  - subject: "Re: People behind KDE: Michael Goffioul"
    date: 2001-06-26
    body: "[I tried to post this once, but apparently it never made it.]\n\nIs there or will there be support for LPRng? I use LPRng, and when I tried to set up my printer in KDE, I tried all the LPR and LPD types, and none worked. They misinterpreted the lines in my /etc/printcap file as *all* being the names of printers. I finally picked the option to print through an external program, and that works, except that it doesn't allow me to specify the exact pages I want to print, etc."
    author: "Rakko"
  - subject: "Re: People behind KDE: Michael Goffioul"
    date: 2001-06-26
    body: "<a href=\"http://lists.kde.org/?l=kde-devel&m=99200865721398&w=2\">http://lists.kde.org/?l=kde-devel&m=99200865721398&w=2</a>\n<p>\nrepeat after me: lists.kde.org is your friend =)"
    author: "Aaron J. Seigo"
  - subject: "Re: People behind KDE: Michael Goffioul"
    date: 2001-06-26
    body: "I'm not a LPRng users, so it's difficult for me to do it. I asked several times on KDE lists if anybody could take care of it, because LPRng is probably one of the most widely spread print systems. However I didn't get any feedback.\nFinally I found somebody that is willing to write a complete LPRng plugin to have full support. So, it's on its way, but won't be available in KDE-2.2.\n\nThe current LPRng support is really basic. Actually, kdeprint only support LPRng if it behaves like a plain LPR system. For example:\n  - you must use continuation char '\\' in printcap file\n  - printcap file must be a real file, the piping feature is not supported\n\nI tried to add basic support for these 2 issues in the LPDUNIX plugin, but it needs testing, and I didn't get any feedback so I don't know if it works OK. I think that at least the problem of continuation character is solved. So you should see your printers correctly now.\nThis plugin however won't allow you any management and/or print job control. But you should be able to print.\n\nMichael."
    author: "Michael Goffioul"
---
Long time follower and developer <a href="http://www.kde.org/people/michaelg.html">Michael Goffioul</a> is our final guest on the <a href="http://www.kde.org/people/people.html">People Behind KDE</a> before the summer break. Michael is the one responsible for a very important new feature of KDE 2.2: the printing system. Thank you, Michael, for tackling this thorny issue.
<!--break-->
