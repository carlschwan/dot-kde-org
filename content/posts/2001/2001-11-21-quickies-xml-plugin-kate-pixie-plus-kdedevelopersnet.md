---
title: "Quickies: XML Plugin For Kate, Pixie Plus, KDEdevelopers.net"
date:    2001-11-21
authors:
  - "numanee"
slug:    quickies-xml-plugin-kate-pixie-plus-kdedevelopersnet
comments:
  - subject: "What about KDE-2.2.2 ?"
    date: 2001-11-21
    body: "Theses are good news, but where is KDE-2.2.2 ? It was scheduled for last monday (11/19/01), right ?\n\n\nAnyway, thanks to all the KDE Team"
    author: "Jean-Christophe Fargette"
  - subject: "Re: What about KDE-2.2.2 ?"
    date: 2001-11-21
    body: "If you are using SuSE they are already available:) Just use Yast or go to www.suse.de/en or their ftp and get em!\n\nThanks goes to the KDE themes for a great desktop environment and to SuSE for bringing us the good stuff packaged and ready to roll!!"
    author: "DiCkE"
  - subject: "Re: What about KDE-2.2.2 ?"
    date: 2001-11-22
    body: "How would I use YsST to updata to KDE 2.2.2?\nI could download each rpm seprately, I know, but if ther is a way to do it \nthrough YaST I would love to try it."
    author: "new suse user"
  - subject: "Re: What about KDE-2.2.2 ?"
    date: 2001-11-22
    body: "Ever tried the \"Online Update\" function of Yast2?"
    author: "Root_42"
  - subject: "Re: What about KDE-2.2.2 ?"
    date: 2001-11-22
    body: "Start yast choose: Package Management (Update, Installation, Queries)->Install packages->Source->FTP\n\nType in the path for the packages e.g the supplementary directory on the ftp where the KDE2.2.2 packages is. And then you just need to choose the ones you want installed. You should upgrade all of the packages!"
    author: "DiCkE"
  - subject: "Re: What about KDE-2.2.2 ?"
    date: 2001-11-22
    body: "Thank you very much, it worked!!!"
    author: "new suse user"
  - subject: "Re: What about KDE-2.2.2 ?"
    date: 2001-11-23
    body: "Whould you happen too know if this work behaind a proxy too.\nI have tried but it would not work, and I can`t find the right documention."
    author: "mosfet_fan"
  - subject: "Re: What about KDE-2.2.2 ?"
    date: 2001-11-22
    body: "2.2.2 is already \"finished\". It's in the hands of the packagers."
    author: "Matt"
  - subject: "pixie and Liquid"
    date: 2001-11-21
    body: "I hoop that mosfet join the kde theam I like pixie and Liquid and i hoop they wil be in kde 3.0 are 3.1"
    author: "Underground"
  - subject: "Re: pixie and Liquid"
    date: 2001-11-21
    body: "Mosfet *was* part of the KDE team, but he left after a dispute with the core developers.\n\n   -Tim"
    author: "Timothy R. Butler"
  - subject: "Re: pixie and Liquid"
    date: 2001-11-21
    body: "...so we all hope!"
    author: "ealm"
  - subject: "Re: pixie and Liquid"
    date: 2001-11-21
    body: "well, as he is writing KDE software he is a part of the KDE \"team\". he just isn't storing his work in KDE's central CVS server or coding in direct coordination with the core developers. and that's fine, as it works much better for everyone involved that way. \n\nthere is nothing to prevent you from downloading his apps and using them, or your distribution from including them with their binary packages. if they are popular enough (which they seem to be) and there aren't any legal issues surrounding them (which there doesn't seem to be), i would be quite surprised if at least some distributions didn't create and ship binaries for Mosfet's works."
    author: "Aaron J. Seigo"
  - subject: "Pixie"
    date: 2001-11-21
    body: "Pixie looks great but why is mosfet dressed like a transvestite?"
    author: "Jerry Fallwell"
  - subject: "Re: Pixie"
    date: 2001-11-21
    body: "Yikes whats up with that Donkey? Mosfet your a total sicko."
    author: "Yo Mamma"
  - subject: "Re: Pixie"
    date: 2001-11-21
    body: "The blonde girl, Gwen, is my ex-fiance. She lives on a farm outside St. Louis with lots of animals including a Donkey she took in because it was going to be put to sleep. Your the sicko for making a weird connotation about that..."
    author: "Mosfet"
  - subject: "Re: Pixie"
    date: 2001-11-22
    body: "... ex-fiance....\n\nThe girl is gone, now mosfet is back coding ;-)\nStupid social life\n\nAlex"
    author: "aleXXX"
  - subject: "Re: Pixie"
    date: 2001-11-21
    body: "My standard goth apparel. Your lucky I didn't use the pics I have of me running around the goth clubs in a vynil and metal studded G string... ;-) Plus, it's fun having the people on slashdot try to guess my gender >:) \n\nNow, if we can get back to the *software*... I'm pretty excited about the new Pixie. If you liked the old one this new one is much more usable (the thumbnail browser really rocks), and has a lot of new neat stuff."
    author: "Mosfet"
  - subject: "Re: Pixie"
    date: 2001-11-21
    body: "Hi Mosfet,\n\nThanks for all your work. My question is:\nwhat is there about Pixie that is more \nthan that found in other KDE image-managers\nlike gwenview or showimg? I have not used \nthe latest Pixie (since it is not released\nyet!) but judging from your\ndescriptions and screenshots, all the\nstuff seems \"standard\" for an image-viewer\nthese days. To use a non-KDE application\nas an example, the wonderful gqview \n(http://gqview.sourceforge.net ) has had\nall the features that you list for pixie\nfor the better part of a year, including the\nheads-up when we try to overwrite an already\nexisting image. \n\nIn summary, what features have you planned\nfor pixie that will make it a better \nimage viewer than showimg, gwenview or\ngqview?\n\nMagnus."
    author: "Magnus Pym"
  - subject: "Re: Pixie"
    date: 2001-11-22
    body: "There's a ton of stuff. Read the Pixie main page, but it has an extremely fast demand-loaded thumbnail manager (the fastest that I could find), tons of effects, batch editing, and lots of image management features. Heck, the old version had a pretty impressive feature list compared to things like ShowImg..."
    author: "Mosfet"
  - subject: "Re: Pixie"
    date: 2001-11-22
    body: "Hmm, I should mention this isn't a slight to ShowImg or any of the other applications... it has some interesting features and competition is always good :) For example, while I always thought Pixie was powerful and had lots of good stuff, many people hated it's interface and prefer things like in ShowImg and GwenView. This caused me to totally rewrite the GUI. So, while I think Pixie is nice and has lots of things users will like, the same could be said about the other people's application's, too... Users will just have to try them out and see what they like, which is a pretty good deal for the users, anyways >:)"
    author: "Mosfet"
  - subject: "Re: Pixie"
    date: 2001-11-22
    body: "So true!\n\n I also liked pixie, but I have become accustomed to\na ACDSee/Gqview like interface.\n\n Now that Pixie's getting a GUI like that too, it's great. \n \n One less reason to keep libgtk!\n\n Keep up the good work."
    author: "Bug Powder"
  - subject: "Re: Pixie"
    date: 2001-11-22
    body: "Hey Mosfet,\n\nHow does the new Pixie compare to gqview?  I found gqview to be amazingly fast with generating and displaying thumbnails, as well as maintaining usability while it's generating thumbnails (which I find handy).\n\nAlso, any ETA on the first release?  I'd love to check it out! :)\n\nCheers,\nJason."
    author: "Jason Tackaberry"
  - subject: "Re: Pixie"
    date: 2001-11-22
    body: "Very good! For reasonable directory sizes (a couple hundred images) it's 1 second or less. It takes 4 1/2  seconds, (4,582ms to be exact), to complete loading a directory of 5,000 image thumbnails. If you got more than that you need to start downloading less Pam Anderson pictures ;-) Most of that time is spent getting and sorting directory information, not loading thumbnails itself.\n\nMy system is an AMD450/128M RAM, and the images are on an IDE ReiserFS drive. I'm very happy with it's performance :)\n\nGenerating thumbnails is still expensive, but I've improved the smoothscaling operation (it no longer uses Qt's QImage::smoothScale method in some cases). I've looked at this at the majority of time (around 2/3rds) is spent in the file I/O procedures for the various image formats, not in the application's code itself. This applies to libjpeg, libgif, and libpng. Since virtually all applications use these libraries (both GTK and Qt), thumbnail generation will probably end up around the same speed for most image viewers. Konq is an exception because it uses IOSlaves, but even that is pretty intelligent about sharing the image data between the slave and the browser in order to be quick. Konq's main problem is the way it manages updating it's iconview."
    author: "Mosfet"
  - subject: "Re: Pixie"
    date: 2001-11-22
    body: "Looks cool. Can it compare images to find duplicates? :)"
    author: "Rakko"
  - subject: "Re: Pixie"
    date: 2001-11-22
    body: "Hehe, I just noticed that's something that ShowImg can do that Pixie can't. Pixie always had a sorting option, \"Ascending by size/Same sizes first\", that I used to find duplicates - but it didn't actually compare images. It just stuck all the images that were the same size on top. This is a useful feature tho, so I'll add it now. It can be done rather efficently since you don't have to compare every image to every other image. You just have to compare images that are the same size. This is what I'm adding now.\n\nThe only problem with this is it will only give you exact matches. But this would mostly be used to compare images downloaded from various websites, which may have scaled the image size differently, placed different web site logos on the image, used different optimization or file format, etc... So you may have the same photo, but it is actually a different size and has different image data. In order to compensate for this you'd have to compare the percentage of red, green, or blue in each image, (or HSV - it really doesn't matter), and have an allowable range of difference for it to match. This is why I never implemented exact duplicate finding before: it's not that useful for me.\n\nThis sounds like you'd be stuck comparing each image to every other image again, since you couldn't just compare ones with the same filesize, but that's not true. I occured to me awhile ago that thumbnails are PNG files, and I can encode additional data in the PNG comment field for the thumbnail, (without changing the original comment of course!). So you can encode CRC values for each color channel when generating the thumbnails and then just compare that - not the entire image! This would allow for the \"fuzziness\" I mentioned above while still relatively fast. Comparing every image to every other one is not acceptable, of course."
    author: "Mosfet"
  - subject: "Re: Pixie"
    date: 2001-11-22
    body: "Cool... I'm not exactly sure how gqview does comparison, but I believe it's the same as what findimagedupes does. I believe it first generates CRCs or hashes or something (I don't really know anything about that kind of stuff, just buzzwords ;) ), stores them in a database, and then compares the different CRCs to each other, instead of comparing the files themselves. It works pretty well, and can find similar-but-not-exact matches."
    author: "Rakko"
  - subject: "Re: Pixie"
    date: 2001-11-22
    body: "If it allows for fuzziness then it's storing CRC values or something similiar somewhere. I really like the idea of sticking it in the thumbnail, tho. It's less efficent than keeping the data in a flatfile, (you have to open each one to load it's comment field), but then you don't have the additional overhead of maintaining a seperate index of items. Since this isn't an operation your likely to do often, I like that approach better, but I may change my mind. I'll take a look and see what it does."
    author: "Mosfet"
  - subject: "Re: Pixie"
    date: 2001-11-22
    body: "cool stuff !\n\nbut when do you put online ???\nand why it's not gpl licence  ?\n\nthanks for what you do mosfet.\n\nps : i was thinking that you was a girl :)"
    author: "blitzfr"
  - subject: "Finding duplicate images (was: Pixie)"
    date: 2001-11-25
    body: "Have a look at findimagedupes for ideas on how to implement image comparison\n\nhttp://www.kudla.org/raindog/perl/\n\nMichael"
    author: "Michael Schuerig"
  - subject: "Offtopic: Goth vs Freak"
    date: 2001-11-26
    body: "Hi Mosfet! I love your software, but I hang around at goth- and fetish-clubs every weekend and I don't think your look is very gothic.. It's more what we call the \"chock-rock\" or \"freak\" look. At least on that only picture I've seen of you... :)\nBTW, I'd love to see those vinyl G-string pictures! :D\n(and no, I'm not gay)\n\nRegards\nPer Wigren"
    author: "Per Wigren"
  - subject: "Re: Offtopic: Goth vs Freak"
    date: 2001-11-26
    body: "Mosfet already wrote it's not a picture of him."
    author: "someone"
  - subject: "Re: Offtopic: Goth vs Freak"
    date: 2001-11-26
    body: "Ok! I missed that!"
    author: "Per Wigren"
  - subject: "Re: Offtopic: Goth vs Freak"
    date: 2001-11-27
    body: "The green and blue haired one's are me. The blonde in the Pixie screenshots is Gwen."
    author: "Mosfet"
  - subject: "Re: Offtopic: Goth vs Freak"
    date: 2003-01-07
    body: "I just surfed in, what pics where are they?"
    author: "No clue"
  - subject: "Re: Pixie"
    date: 2001-11-22
    body: "Last time I checked, this was still a free country.\n\nJust love the green hair and matching eyes.\n\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Pixie"
    date: 2001-11-23
    body: "It's not like I don't get flamed enough, a few more won't hurt ;-) \nMosfet.\n\n\nFlame away then"
    author: "mosfet_fan"
  - subject: "KDEdevelopers.net"
    date: 2001-11-21
    body: "They should update the site so that Konqueror hits are not counted as Netscape... :)\n\nhttp://www.KDEdevelopers.net/stats.php"
    author: "scb"
  - subject: "Re: KDEdevelopers.net"
    date: 2001-11-22
    body: "I think so to, but it's a bug in PHP-Nuke (has been for a long time). And it's easy to fix. I did so when I played around with PHP-Nuke localy.\n\nSomewhere there is a counter that counts hits, it test if the browser is a Netscape (or Mozilla) browser before it checks if it's a Konqueror. The Konqueror agentstring contians the words \"Mozilla/5.0 (compatible; ...)\" so PHP-Nuke thinks it's a Mozilla browser, instead they should move the test if it's Konqueror to before the tist if it's Mozilla... That anoying bug has been there for a long time. In KDE 2.1 you could manualy change the useragent string to whatever you'd like. I changed it to Konqueror/2.1, and it worked wonderful, but that's not possible anymore... \n\n/* Syllten happens to be syllten! */"
    author: "Syllten"
  - subject: "Konqueror is badly recognized in many stats"
    date: 2001-11-22
    body: "It is bad for PHP Nuke and many other stats pages.\nIt would be better that \"Mozilla\" would not be wrote in the default Konqueror Agentstring. If there is a bug, it is not in PHP-Nuke and others programs, it is in Konqueror. And it is a very bad thing because Konqueror looks like a very few used browser :-("
    author: "Alain"
  - subject: "Re: Konqueror is badly recognized in many stats"
    date: 2001-11-26
    body: "That's not entirely accurate. Konqueror is listed as Mozilla Compatible because otherwise a good number of sites would choke on it. I've seen many a badly designed site that assumed that if you're not using IE or Netscape, then you must be uncapable of using Flash, Javascript, or sometimes even frames!\n\nThe bug is in PHP-Nuke's agent string parsing because Konqi lists itself as being Mozilla _compatible_, which is not the same thing as being Mozilla."
    author: "Carbon"
  - subject: "Re: Konqueror is badly recognized in many stats"
    date: 2003-02-12
    body: "It's true, this bug isn't Konqueror's fault, because even Internet Explorer for Windows identifies itself as Mozilla. 'Mozilla' appears always as the first word in the UA string (even in the latest IE version), because in the old days when the only JavaScript-compatible browser and market leader was Netscape, webmasters did a check on the browser UA to allow only Netscape users, so IE began identifying itself as Mozilla, which was originally Netscape UA string (Mozilla was the browser code upon which Netscape was built, even nowadays). So browser sniffing isn't good coding practice, never was; but now, when we got standards, browser check isn't necessary anymore; in javascript we should use object detection instead and in server side detection, only UA storage for use in stats. I do only browser checking for a personal site but I don't deny contents. Just change the site interface where the browser does support advanced features.\n\n[db]\nhttp://sci-web.com.ar"
    author: "D.B."
  - subject: "Other cool stuff"
    date: 2001-11-21
    body: "I found this scp/ssh kio slave very usefull and stable. It doesn't use sftp, but uses a combination of ssh and scp. Fun thing is that it also uses the keys you have loaded in ssh-agent AND it uses your ~/.ssh/config. So browsing your home directory from work at home is as simple as fish://yourbox/ (no password if you have keys and config set up right.\n<br><br>\n<a href=\"http://ich.bin.kein.hoschi.de/fish/\">http://ich.bin.kein.hoschi.de/fish/</a>"
    author: "Pyre"
  - subject: "Re: Other cool stuff"
    date: 2001-11-21
    body: "Yeah, kio_fish is awesome...  One of the coolest things I've seen in a while..."
    author: "Ranger Rick"
  - subject: "Re: Other cool stuff"
    date: 2001-11-22
    body: "I use kiofish on a daily basis for accesing my schooldir ... and it definitely rocks !! \n\nfab"
    author: "fab"
  - subject: "Re: Other cool stuff"
    date: 2001-11-22
    body: "Yes, I have to agree. I also think that kio_fish should be included with KDE3, giving the new version a true \"killer app\"."
    author: "Matt"
  - subject: "Re: Other cool stuff"
    date: 2001-11-23
    body: "kde3 also has a kio_sftp, which is even better :-)"
    author: "Kevin Puetz"
  - subject: "Re: Other cool stuff"
    date: 2001-11-22
    body: "Yes, kio_fish and kio_sftp are great stuff. Very useful for korganizer.\n\nI tried to get my konqi-bookmarks from a server via kio-slave, but this seems not to work (read: I found no config option to set a path to the bookmark file).\n\nOne problem with scp-like transfer: You can't copy links, they are changed to real files. Something like a transparent \"tar cfz\" -> \"scp\" -> \"tar xfz\" on directories with kio_fish would be nice :-)\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: Other cool stuff"
    date: 2001-11-22
    body: "> no password if you have keys and config set up right\n\nWhere I live, setting things up the right way would involve keys _with_ passwords. :-)"
    author: "Rob Kaper"
  - subject: "Re: Other cool stuff"
    date: 2001-11-22
    body: "unless you use keyring"
    author: "Martijn"
  - subject: "what is KDEdevelopers?"
    date: 2001-11-22
    body: "I was browsing the site, but could not find a meaningful mission statement.\nWhy is Nick Betcher providing the services for free, now that everyone starts to charge for these things ?\n\nThis is very nice of him."
    author: "ac"
  - subject: "What happened to mosfet's site?"
    date: 2001-11-22
    body: "All it shows is:\nMandrake, pls dont use it ! -WuvB-\n\nDefaced?\n\nClaus"
    author: "Claus Wilke"
  - subject: "Re: What happened to mosfet's site?"
    date: 2001-11-23
    body: "I don't know what's up. The site is down now... I've sent an email to the administrator."
    author: "Mosfet"
  - subject: "Re: What happened to mosfet's site?"
    date: 2001-11-23
    body: "Yeah, looks like I've been hacked :("
    author: "Mosfet"
  - subject: "Site's back up"
    date: 2001-11-23
    body: "I'm still not sure what happened, but mosfet.org is back online. Check out the site for what little more information I know about it."
    author: "Mosfet"
  - subject: "kde222 and objprlnk"
    date: 2001-11-23
    body: "Hi there !\n\nAnyone know how to patch the kde.src.rpm to get objprlnk working? I hearded, that this improves performance a lot and the rpm\u00b4s offerd on ftp.kde.org shouldnt include it.\n\nthx\nRonny"
    author: "Ronny P"
---
<a href="mailto:daniel.naber@t-online.de">Daniel Naber</a> wrote in some time
ago to
inform us of <a href="http://www.danielnaber.de/tmp/">a new XML plugin</a> for
<a href="http://kate.sourceforge.net/">Kate</a>.  <i>"The plugin gives hints about what's allowed at a certain position in
an XML file, according to the file's DTD. It will list possible
elements, attributes, attribute values or entities, depending on the
cursor position. You can also close an open element with a keyboard
shortcut. <a href="http://www.oasis-open.org/docbook/">DocBook</a> is also supported, so hopefully people will use Kate +
XML plugin to write KDE documentation."</i>  This should be useful to the
tons of us working with
<A href="http://www.xml.com/pub/a/98/10/guide1.html">structured data</a> in XML. I gave <a href="http://kxmleditor.sourceforge.net/">KXMLEditor</a> a whirl the other day but although it <a href="http://kxmleditor.sourceforge.net/screenshots.htm">looks nice</a>, it basically ignored my DTD.  I hear <a href="mailto:rich@kde.org">Richard Moore</a> also has <a href="http://www.ipso-facto.demon.co.uk/development/xmelegance/xmelegance.html">XMElegance</a> in the making.  Yup.

In other unreleased software news, <a href="http://www.mosfet.org/">Mosfet</a> wrote
in to point us to his <a href="http://www.mosfet.org/pixie/">PixiePlus</a>
<a href="http://www.mosfet.org/pixie/screenshots.html">screenshot</a> previews.
It's slated to have tons of new stuff and candy.

Finally, <a href="mailto:nbetcher@usinternet.com">Nick Betcher</a> wrote in to point us to
<a href="http://www.KDEdevelopers.net/">www.KDEdevelopers.net</a>, a brand new
PHP-Nuke site providing various free services to KDE developers such as
web hosting (PHP, SQL), email (SMTP, POP3), FTP, DNS, subdomains, etc.  Seems
<a href="http://robbie.kdedevelopers.net/projects.php">Robbie</a>
is already <a href="http://robbie.kdedevelopers.net/">nicely hosted</a> there.
More info at <a href="http://www.KDEdevelopers.net/">the site</a>.
<!--break-->
