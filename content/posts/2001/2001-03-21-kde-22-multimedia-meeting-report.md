---
title: "KDE 2.2 Multimedia Meeting:  Report"
date:    2001-03-21
authors:
  - "Dre"
slug:    kde-22-multimedia-meeting-report
comments:
  - subject: "Re: KDE 2.2 Multimedia Meeting:  Report"
    date: 2001-03-21
    body: "I have a few questions regarding to aRTS.\n\n1. Why does it use MCOP? Isn't ORBit faster?\nORBit has C++ bindings.\n2. I've heard that aRTS uses a lot of CPU resources (30%) even when it's idle.\nIs that true? And if yes, will it improve?"
    author: "AC"
  - subject: "Re: KDE 2.2 Multimedia Meeting:  Report"
    date: 2001-03-21
    body: "1. Evidently not.  Besides nobody wants to deal with CORBA again. MCOP also does streams. But S himself seems to like CORBA, check the webpages for fuller explanations.\nhttp://www.arts-project.org/\n\n2. Evidently it doesn't.  Why don't you try aRts before crapshooting?"
    author: "KDE User"
  - subject: "Re: KDE 2.2 Multimedia Meeting:  Report"
    date: 2001-03-21
    body: "I think no one wants to deal with MICO there's nothing wrong with ORBit."
    author: "Lee"
  - subject: "Re: KDE 2.2 Multimedia Meeting:  Report"
    date: 2001-03-22
    body: "There's a very good discussion of the exact reasons behind aRts using MCOP instead of CORBA or DCOP at the aRts project website.  The URL is:\n<p>\n<a href=\"http://space.twc.de/~stefan/kde/arts-mcop-doc/why-not-dcop.html\">http://space.twc.de/~stefan/kde/arts-mcop-doc/why-not-dcop.html</a>\n<p>\nThe title is \"Why not DCOP\" but it discusses CORBA some as well (aRts originally used CORBA, but switched).  All the reasons seem well thought-out and very logical.  One very good reason cited there is speed:  By some measures, MCOP is 6-8 times as fast as CORBA (and 4 times as fast as DCOP).  MCOP's architecture is also much better suited to some of the tasks that aRts does.  (after all, it was designed specifically for multimedia)\n"
    author: "not me"
  - subject: "Re: KDE 2.2 Multimedia Meeting:  Report"
    date: 2001-03-22
    body: "Looking at some old peformance tests for ORBit (http://developer.gnome.org/doc/whitepapers/ORBit/about-orbit.html).\n\nMICO and ORBit are both ran 10,000 times with MICO taking 22.48 seconds and ORBit 2.93. Or about 444,82 invocations/sec and 3412,97 inv/sec respectively.\n\nThis means (with 1000 to 1500 invocations/sec as reported by Stefan) a factor of 2.243 to 3.372 difference between the tests.\n\nORBit would then get something between 7655 to 11508 invocations per second in Stefan's test (if I haven't miscalculated) compared to MCOP's 8000.\n\nI don't believe performance has anything to do with choosing MCOP over ORBit though. It probably has something to do with compatibility with KDE, ease of use, and familiarity.\n\nBtw, I recall seeing a mail from Elliott Lee that he was focusing on performance in ORBit2.\n\n /mill"
    author: "mill"
  - subject: "Re: KDE 2.2 Multimedia Meeting:  Report"
    date: 2001-03-22
    body: "\"2. Evidently it doesn't. Why don't you try aRts before crapshooting?\"\n\nIt is a QUESTION damnit!!!!\nCan't you THINK before calling a question \"crapshooting\" ?!?!\nIt's clear that YOU are the crapshooting one here!"
    author: "AC"
  - subject: "Re: KDE 2.2 Multimedia Meeting:  Report"
    date: 2001-03-23
    body: "There USED to be performance issues. But in KDE-2.1 arts is really nice and does not use any significant cpu load."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: KDE 2.2 Multimedia Meeting:  Report"
    date: 2001-03-21
    body: "Clever troll"
    author: "konqi"
  - subject: "Re: KDE 2.2 Multimedia Meeting:  Report"
    date: 2001-03-21
    body: "I don't think he's trolling, he's just uninformed. There is some misconception amongst some gnome-centric people I think that orbit is faster. There were also reports to mailing lists (i.e. mandrake) for the kde2.1 beta's that aRts took up a lot of CPU resources.\n\nWhere I work there is a very debian/gnome centric group. When I asked many people why they don't use KDE most replied that it was b/c KDE was inferior. However, the people that have used KDE only ever used the first version of KDE1. Once I introduced konqi to some of them they've been changing their opinion."
    author: "Tormak"
  - subject: "Re: KDE 2.2 Multimedia Meeting:  Report"
    date: 2001-03-21
    body: "Arts _does_ take up a lot of system resources (at least if I can trust my various resource monitors).\n\nI use it and like it, but even when I'm just playing mp3s its sitting there taking a small percentage of my CPU (< 5% of a 750 MHz Duron). I'm sure its more noticable on slower machines."
    author: "JC"
  - subject: "Re: KDE 2.2 Multimedia Meeting:  Report"
    date: 2001-03-21
    body: "Thats normal, Winamp takes 13% on a 416 MHz Celeron."
    author: "reihal"
  - subject: "Re: KDE 2.2 Multimedia Meeting:  Report"
    date: 2001-03-22
    body: "aRts takes up a moderate amount of resources.  Here on my P3 500 MHz, average total CPU usage when playing an MP3 with Noatun is 3 to 3.5%.  Doing nothing, it's about 1%.\n\nWhen I kill artsd, CPU usage goes down to practically 0.  Playing an MP3 with XMMS takes around 0.3-0.5%.  I measured this using KPM.\n\nSo yes, aRts does take more CPU time than other sound players, but it has many more capabilities as well.  Also, aRts is still in development, of course.\n\nI wouldn't worry too much about people with slow computers.  I think that probably the CPU time taken by aRts depends more on the soundcard than your processor speed, and therefore it would remain nearly constant no matter how fast your computer (because all soundcards play at the same rate).  I could be wrong, though :-)"
    author: "not me"
  - subject: "No, YOU are the troll!"
    date: 2001-03-22
    body: "CAN'T YOU SEE THE DIFFERENCE BETWEEN A <B>QUESTION</B> AND A TROLL?!?!?!"
    author: "AC"
  - subject: "Re: KDE 2.2 Multimedia Meeting:  Report"
    date: 2001-03-21
    body: "Super. I wish the best to all the developers! And maybe someone can figure out why a no-error proper tree-compile from source of 2.1 causes my artsd to wig out on a stock Mandrake 7.1 system:\n\nartsd: Symbol `__vt_Q24Arts16SoundServer_base.Q24Arts22SimpleSoundServer_base' has different size in shared object, consider re-linking\nartsd: error in loading shared libraries: artsd: undefined symbol: _t24__default_alloc_template2b1i0.free_list\n:)"
    author: "EKH"
  - subject: "CSL?"
    date: 2001-03-21
    body: "I'm I understanding this correctly, that CSL is the GNOME sound architecture? Does this mean that GNOME apps (like everybuddy) might one day happily play their notifications and sounds while running in KDE?\n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "Re: KDE 2.2 Multimedia Meeting:  Report"
    date: 2001-03-21
    body: "Will there really be *no* gap when playing consecutive songs, or will the gap simply be (much) smaller than it is now?  I ask because I can't imagine that the gap could be completely eliminated without loading at least some of the second file *before* the first one has finished.\n\nNeedless to say, when listening to ripped CDs (ex, a live performance) it would be nice to have absolutely no gap. :)\n\nDave"
    author: "David Sweet"
  - subject: "Re: KDE 2.2 Multimedia Meeting:  Report"
    date: 2001-03-22
    body: "I plan on making this* an option in noatun.\n\n*this, that is, loading the previous file before the current is done playing.\n\nCertain files take a lot of overhead on initial overhead, the new Mikmod plugin for example (libmikmod is just plain dissapointing)"
    author: "Charles Samuels"
  - subject: "Re: KDE 2.2 Multimedia Meeting:  Report"
    date: 2001-03-22
    body: "Speaking of mikmod/libmikmod, does anyone know where its homepage went?  http://mikmod.darkorb.net doesn't seem to work anymore."
    author: "dlr"
  - subject: "Re: KDE 2.2 Multimedia Meeting:  Report"
    date: 2001-03-22
    body: "I looked around quite a bit as well, to no avail.\n\nI think the project may be dead."
    author: "Charles Samuels"
  - subject: "I have a problem with noatun."
    date: 2001-03-22
    body: "while noatun does great, I would like to state here that\nopen source projects for video playing have been\ndissappointing. There are too many different efforts that\ndo not even coordinate.\nresult? none of them can play all the available video formats.\nWhy not work together to give us a player that will play avi, mpeg 1/2/4, divx and asf?\nNoatun is cool but, it too can not play most of the stuff out there.\nFor now you need all of them. xine, mplay, avifile,mtv, xanim????\nTotal Chaos!!!!!!"
    author: "psycogenic"
  - subject: "Re: I have a problem with noatun."
    date: 2001-03-22
    body: "(i'm not a developer just a mailing list reader)\n\nNoatun should be able to but for a few minor\nproblems (that hopefully will be fixed) in mpeglib.\n\nMplayer is the best one i've found it plays everything\nyou noted as far as i'm aware.\n\nAlex"
    author: "Alex"
  - subject: "Re: MPlayer"
    date: 2001-03-22
    body: "Yes, that's right.\n\nMPlayer is the best player I've found so far.\n\nThe only problem is that it's a single-threaded app (is it?) and my driver of my soundcard doesn't support select() somehow :-("
    author: "Rudo Thomas"
  - subject: "Re: I have a problem with noatun."
    date: 2001-03-25
    body: "Actually - if you really want to play stuff like DivX files - try Xine. I tried avifile, MPlayer and xine - xine beats all of them, and version 0.4 is multithreaded - so it takes ~30% of my CPU. I can live with that ;)"
    author: "hetz"
  - subject: "Re: I have a problem with noatun."
    date: 2003-02-17
    body: "xine-1 play's sornsen (aka quicktime5), real, avi,....etc. nothing out there beats that.\n"
    author: "wayne"
  - subject: "Re: I have a problem with noatun."
    date: 2008-08-27
    body: "Actuially... VLC is the one that plays everything, just too many mplayer fans thinks they play everything."
    author: "anonymous"
  - subject: "Hardware Mixers"
    date: 2001-03-22
    body: "I think it would be cool for aRts to support multi-channel hardware mixing.  Nothing would have to change in the abstraction layer between the client apps and artsd, just the way artsd handles /dev/dsp.  Several of the OSS drivers support opening /dev/dsp multiple times and pushing several, independant, streams into it.  Other drivers support /dev/dsp{0,1,2,...}.  I guess this is a \"It would be nice\" type of thing, to have the final output streams mixed in hardware (at no CPU cost, and higher quality), especially considering that it will take a much higher load to cause the sound to break up.  Anyway I'll take a look at the aRts homepage and see if anyone thinks this is a good idea."
    author: "raven667"
  - subject: "Re: Hardware Mixers"
    date: 2001-03-22
    body: "I agree. Having a Sounblaster Live I really don't need arts to start mixing things together. It would be really nice if what you described could be implemented somehow.\n\nGood luck to you all,\n\nJelmer"
    author: "Jelmer Feenstra"
  - subject: "Re: KDE 2.2 Multimedia Meeting:  Report"
    date: 2001-03-22
    body: "I have had problems with large files (or streaming from XMMS). After 20-30 mins, the CPU usage goes skyhigh, and finally causes Noatun or Xmms to jitter the sound. Also, artsd becomes highly unstable and easly crashes when some other sound is sent to it from licq (eg). This problem has been there since 2.0. Is it being addressed ?"
    author: "RE: How about speed"
  - subject: "Re: KDE 2.2 Multimedia Meeting:  Report"
    date: 2001-03-22
    body: "I agree. artsd is extremely flakey when the audio device is being used by other technologies. It doesn't deal with the situation gracefully and causes audio to stop working altogether until it has been HUPped - and sometimes killed altogether. To be honest, I often find it better just to kill artsd completely and just use XMMS with the standard kernel sound drivers etc. I do hope I won't have to do this for much longer."
    author: "Nick Mailer"
  - subject: "Re: KDE 2.2 Multimedia Meeting:  Report"
    date: 2001-03-23
    body: "It has been addressed in 2.1, AFAIK.\n\nI reported that myself and no longer see this - seems to have been massive leakage."
    author: "anon"
  - subject: "Re: KDE 2.2 Multimedia Meeting:  Report"
    date: 2001-03-23
    body: "I have 2.1 now, and the problem is still there. The only way to avoid it is by using artsdsp and making xmms write to /dev/dsp. But I guess all of us want to use noatun."
    author: "RE: How about speed"
  - subject: "Re: KDE 2.2 Multimedia Meeting:  Report"
    date: 2001-03-22
    body: "I hope evenually we get a kde Video and Audio editor like the one advertised on the Mac commercials. I think that would be a very cool addition to kde.\n\nCraig"
    author: "Craig Black"
  - subject: "Re: KDE 2.2 Multimedia Meeting:  Report"
    date: 2001-03-22
    body: "For a KDE Audio editor, you might try Brahms (whose homepage is apparantly down) at http://lienhard.desy.de/mackag/homepages/jan/Brahms/\n\n\nFor Video, I would suggest a non-KDE program, Broadcast 2000, located at http://heroines.sourceforge.net/bcast2000.php3. To give you an idea of its quality, it is also sold packaged with hardware to small TV stations as a turnkey solution.  Unfortunantly, it *does* require the latest and greatest kernel and such - it really does push the latest multimedia features in Linux.  Unfortunantly, until the Linux video system matures and works its way into distros a bit more, it will be difficult for a simple package install... but it does exist.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: KDE 2.2 Multimedia Meeting:  Report"
    date: 2001-03-22
    body: "There's a newer version of Brahms in KDE's CVS. Try that one instead."
    author: "Matt"
  - subject: "Re: KDE 2.2 Multimedia Meeting:  Report"
    date: 2001-03-22
    body: "Thanks i'm i have it installed, i was hopeing that kde would get its own though. It would be nice to drap and drop video into the editor ect.\n\nCraig"
    author: "Craig Black"
  - subject: "Re: KDE 2.2 Multimedia Meeting:  Report"
    date: 2001-03-22
    body: "KDE uses the standard X DnD protocol (that's why it interoperates just fine with GTK/motif/etc apps). I can drag thing from konqi to xchat, xmms, etc with no problems whatsoever.\n\nIf Broadcast 2000 doesn't do drag-n-drop from konqi, it's not because it's not a KDE app, it's because the author doesn't support drag-n-drop period :-). He certainly could, and then it would work from gmc, mozilla, others as well."
    author: "Kevin Puetz"
  - subject: "Re: KDE 2.2 Multimedia Meeting:  Report"
    date: 2001-03-23
    body: "Even if it did and i did'nt check it still would be cool to have a video editor that was kde specific.\n\nCraig"
    author: "Craig Black"
  - subject: "How about fixing noatun?"
    date: 2001-03-22
    body: "I have TONS of stability problems.  After playing a few mp3's I end up having to kill noatun and the 3 copies of arts it leaves around.  I was so annoyed I almost switched back to GNOME.  Then I decided to try XMMS (using artsdsp xmmms -p).  Since then I've had NO problems (even when playing system sounds while the mp3 is playing).\n\nAnyone else get this?\n\nRimmer\n\nPS - also I think the default skin should be smaller... just my 2 cents"
    author: "Rimmer"
  - subject: "Re: How about fixing noatun?"
    date: 2001-03-22
    body: "I'm a recent convert from Gnome 1.0 to kde 2.1 and i think kde is definitely the nicest desktop i've used. but:\n<br><br>\nI have similar problems. first of all, if i start arts via the artswrapper (has to be done as root), i cant get it to suid to something else (or i didnt find how to do it in the docs). <br><br>\nSo, i let arts start under my userid when i start kde, which of course means it doesnt have real-time priority.  When I use either noatun or xmms, (arts or oss driver) the sound gets messed up (cracks, pops, pauses).  This even happens when kde is starting up.. the kde sound that plays usually gets garbled.\nSo, i jack up the buffering on artsd and on the arts driver for xmms which causes my entire system to go berzerk: crashing kde before i get a chance to check my resource usage. it seems the system can't handle the load (K6-2 450 196MB ram, RH7.0).\n<br><br>\nnow, i'm trying artsdsp xmms -p and it's doing much better, though i've gotten a couple of \"pops\" in the sound.  I'm not picky about the sound, but if windows2000 lets me play winamp and get email notification sounds at the same time w/o any pops, i'd hope kde could do the same.\n<br><br>\n* actually, as i was getting ready to post this, i started having some major problems with sound. <b>arts randomly shot up its resource usage to 68% of system resources, and the sound broke up into a million pieces.</b> all i did was submit a form in opera.\n\nThanks."
    author: "Pablo"
  - subject: "Linux sound is just pathetic?"
    date: 2001-03-22
    body: "I have a fairly fast computer (Thunderbird 1100) and I have problems with the sound in both GNOME and KDE.  This sound is fine (when playing an mp3) until I try to move a large file (like a kernel tarball) or open several apps at once.  Then I get lots of static and broken sounds.  Maybe Linux just sucks... anytime I move a large file (or something similar like untar a kernel) the system becomes completely unresponsive.  It doesn't help that GNOME and KDE both use over 80 MB of memory on my system."
    author: "Rimmer"
  - subject: "Re: Linux sound is just pathetic?"
    date: 2001-03-22
    body: "try to enable dma transfer\nex:\nhdparam -c1d1 /dev/hda"
    author: "ac"
  - subject: "THANK YOU"
    date: 2001-03-24
    body: "I hope you see this.\n\nThe problem was fixed (completely) by enabling DMA on my hard drive.  Thanks a bunch!\n\nRimmer"
    author: "Rimmer"
  - subject: "Re: Linux sound is just pathetic?"
    date: 2001-03-22
    body: "try to use a recent 2.4 kernel too"
    author: "ajuin"
  - subject: "Using 2.4.2"
    date: 2001-03-23
    body: "I assume that is recent enough? :)"
    author: "Rimmer"
  - subject: "Re: Using 2.4.2"
    date: 2001-03-23
    body: "You should apply the linux low-latency patches to the kernel.  In fact the Linux kernel has been getting _worse and worse_, not better, when it comes to kernel latency, which is a common cause of audio-dropout.  Basically, I'm with you on this one, Linux sound (and multimedia presentation) just sucks, at the kernel level, due to poor (from the perspective of multimedia needs) task scheduling.  Even the low-latency patches, though they offer a HUGE improvement to real-time scheduled tasks, do not fully solve the problems with Linux and latency (for example, they have no effect on non-real-time scheduled tasks, which, admittedly, by definition, don't need to be as responsive) but the real problem is basically that the Linux kernel is designed to be a server kernel, so that if ever there is a trade-off between responsiveness or raw speed, raw speed always wins.  This is unfortunate, as OS's such as QNX and BeOS show that a responsive kernel can lead to a nicer experience for the user, and a more suitable platform for content production, as well as at least the -impression- that the computer is fast.  I'd love to see a new scheduler in Linux, one with things like extrememly low-latency real-time, smoother scheduling in general, and priority inheritance.  In fact, in an ideal world, you'd see two different kernels out there, which both export a perfectly compatible common API, but \"tuned\" very differently -- one with a scheduler tuned for fast response, and fast thread creation/destruction (like BeOS), the other tuned for raw speed with more emphesis on processes than threads (for big servers).  So that way if you are basically a desktop user (or gamer, artist) you'd use the BeOS-like kernel, but if you are building a server, scientific work-horse, or database back-end, you'd use the current Linux kernel.  In 2.4.2, I've seen reports of 400 (FOUR HUNDRED!!!) millisecond latencies (\"reponse\" times).  FOUR HUNDRED!  We're almost entering Window's 95 territory there!  Well, not that bad, but!!!!  \n\nErik Hill\n\n(do a google search on \"Linux low-latency patches\" and read up."
    author: "Erik Hill"
  - subject: "Re: Linux sound is just pathetic?"
    date: 2003-07-13
    body: "Although enabling DMA might help in these cases, there are still situations in which this problem persists. I am using a USB harddisk, and when I copy large files to or from this device my system becomes unresponsive after a while and sometimes even hangs! Especially when there's another process doing intensive I/O (in my case, grabbing video from a TV capture card) the kernel tends to lock up hard after a while.\n\nSince this problem appears after transferring about the same amount of data as there is free memory in my system, it seems as if the kernel has trouble flushing its write-behind cache on slow devices. I'm no kernel expert but as far as I can see this problem is most likely not USB driver related (I've used both uhci.o and usb-uhci.o, both express the same problem).\n\nAre there other out there that have seen the same behaviour?\n\nMaybe this should be CCed to the kernel people...\n\nkernel 2.4.20\ndistro gentoo\ncpu athlon-xp 2000+\n/fs ext2"
    author: "Shag"
  - subject: "Re: How about fixing noatun?"
    date: 2001-03-23
    body: "I've decided to post another reponse here, as my first one was very deep in the discussion, and I'm afraid that it might get lost in the confusion.\n\nYou are having a problem with one of two possibilities, or both.\n\n#1:  Your windows driver does a better job than your Linux driver of supporting your sound chip's hardware buffers, or\n#2:  You are having trouble with latency in the Linux kernel.\n\n(or, possibly, DMA, but the pages that cover kernel latency also cover that, so anyway...)\n\nSearch on google on \"Linux low-latency patches\" and read up.  These pages explain a LOT.  You can also read my longer rant deeper in the discussion!\n\nErik Hill"
    author: "Erik Hill"
  - subject: "It was the DMA"
    date: 2001-03-24
    body: "Enabling DMA on my hard drive fix my problems :)\nThanks for the help.\n\nRimmer"
    author: "Rimmer"
  - subject: "XMMS aRts plugin"
    date: 2001-03-22
    body: "A lot of people seem to experience troubles with using xmms together with aRts. I suggest that those people have a look at:\nhttp://home.earthlink.net/~bheath/xmms-arts/\n\nThe plugin there will allow xmms to direct its output to aRts. This should resolve the issues of at least some people.\n\n-- \nMatthijs"
    author: "Matthijs Sypkens Smit"
  - subject: "Re: XMMS aRts plugin"
    date: 2001-03-22
    body: "Cool! That's what I searched for. Should be integrated in the xmms dist ... :-)\n\nThorsten"
    author: "tholt"
  - subject: "Re: XMMS aRts plugin"
    date: 2002-03-31
    body: "The link you posted doesn?t exist any longer. Is it only a temporary downtime or do I have to look in another place for the plugin?"
    author: "Stephan"
  - subject: "Re: XMMS aRts plugin"
    date: 2003-06-16
    body: "Since the XMMS plugins site is down, we can't rely on it for links to the various plugins.  I was unable to locate this plugin at first, however a quick look at Google's cache of the plugins page revealed the location of the plugin:\n\nhttp://stukach.com/hosted/m.i.a/xmmsarts/xmmsarts-0.4.tar.bz2 -- Source\nhttp://stukach.com/hosted/m.i.a/xmmsarts/xmmsarts-0.4-1.i586.rpm -- RPM Binary\n\nThe cached page if anyone is interested:\nhttp://www.google.com.au/search?q=cache:GkXxQHuTmX0J:www.xmms.org/plugins_output.html+arts+xmms+plugin&hl=en&ie=UTF-8\n\nHope this helps,\nStuart Longland"
    author: "Stuart Longland"
  - subject: "XMMS aRts plugin problems"
    date: 2003-05-01
    body: "When I start XMMS, it runs and plays a song through aRts, but when it finishes the song, it crashes. It will seek within a song though."
    author: "Erik"
  - subject: "Re: XMMS aRts plugin"
    date: 2004-07-24
    body: "Works perfectly... No more interruptions when I'm listening to music on XMMS :-)"
    author: "feinoM"
  - subject: "quake 2 & 3 and arts"
    date: 2001-03-22
    body: "In my sys quake2 and quake 3 don't work well with arts even with that artsdsp stuff\n(quake 2 crashes and q3 runs without sound)\nthe rest work's great!!\nany tips about quake 2 & 3?"
    author: "ignoto_deo"
  - subject: "Re: quake 2 & 3 and arts"
    date: 2001-03-22
    body: "This is probably not the answer you're looking for, but kill artsd. While running Q3 you'll want to use every processor cycle available instead of \"waisting\" them on arts."
    author: "CAPSLOCK2000"
  - subject: "off topic(?): artsd - am I doing something wrong?"
    date: 2001-03-22
    body: "Am I the only one that never got artsd to work?\n\nartswrapper overloads the CPU (fortunately, it quits in this case) and artsd applications hang up - sooner or later.\n\nMaybe it's an issue of the driver to my Diamond MX300, but e.g. XMMS works well.\n\nAny help apprecited."
    author: "Rudo Thomas"
  - subject: "Re: off topic(?): artsd - am I doing something wrong?"
    date: 2001-06-11
    body: "Hello!\n\nHave you solved your problem already ?\n\nPlease, let me know. Thanks."
    author: "Patrik Beno"
---
<A HREF="mailto:stw@kde.org">Stefan Westerfeld</A> recently <A HREF="http://www.kde.com/maillists/show.php?li=kde-multimedia&f=1&l=20&m=427260">posted</A> a detailed
summary of the KDE 2.2 Multimedia Meeting, held on IRC on March 6.
The meeting focused on <A HREF="http://www.arts-project.org/">aRts</A>, the linchpin of the KDE 2 multimedia architecture.
Topics covered include threading the aRts server; improved error handling
when distributed objects fail; increasing aRts user-friendliness (e.g.,
useful error messages); compatability with GNOME (e.g., providing a
C/CSL interface); improving KMedia2 (noatun, etc.) by removing gaps between
files, allowing for streamed input and embedding video inside noatun;
making aRts effect GUIs (e.g., Synth_FREEVERB) toolkit-independent;
and adding a mixer to aRts (aRts does not currently work with KMix).
For the full report, read below.

<!--break-->
<H2>The KDE2.2 Meeting, IRC discussion summary</H2>
written by <A HREF="mailto:stw@kde.org">Stefan Westerfeld</A>,
2001-03-21
<P>
This summary provides an overview of the things that we talked about
during the KDE2.2 multimedia meeting which took place on IRC (2001-03-06).
If you want to read more than the summary, the full IRC logfile is
<A HREF="http://space.twc.de/~stefan/kde/download/KDE2.2-Multimedia-Meeting.IRCLog">here</A>.
</P><P>
Participants (in no particular order) are listed at the bottom.
</P><P>
<H3>Overview</H3>
</P><P>
Somehow, it could have been called the
<A HREF="http://www.arts-project.org/">aRts</A> development meeting.
Maybe that was a result of talking about the aRts TODO list "first"
(because it was the only document which was available before the meeting),
and then getting to nothing else. Maybe it was a result of me moderating ;).
And finally, maybe it was because aRts is a very central component of the
KDE multimedia development, and thus working on KDE multimedia almost
always implies somehow getting in touch with aRts.
</P><P>
Anyway, the following will try to give the idea what we talked about, and
try to reconstruct the tasks that were assigned.
</P>
<OL>
<LI>aRts Core/SoundServer</LI>
<OL>
<LI>Threading</LI>
<LI>Error handling</LI>
<LI>SoundServer userfriendliness</LI>
<LI>CSL/C API</LI>
</OL>
<LI>KMedia2</LI>
<OL>
<LI>Gapless playing</LI>
<LI>PlayObjects for custom data streams</LI>
<LI>X11 Embedding</LI>
<LI>Kaboodle</LI>
</OL>
<LI>GUIs for aRts objects</LI>
<LI>Mixer issues</LI>
<LI>Conclusion</LI>
</OL>
<P>
<A NAME="artscor"></A><H3>aRts Core/SoundServer</H3>
</P><P>
<H4><A NAME="threading"></A>Threading</H4>
</P><P>
Threading: mpeglib is threaded, decoders are threaded, the first point is
about making aRts threadsafe enough to develop threaded playobjects and such.
It's mostly done anyway, so it will be in KDE2.2 (libmcop-mt).
</P><P>
<H4><A NAME="errorhandling"></A>Error handling</H4>
</P><P>
We discussed error handling in MCOP apps. The problem is that currently when
artsd dies apps segfault unpredictable. This is usually since when artsd died,
remote operations returning object references return null pointers. Calling
<PRE>
        soundserver.outstack().insertTop(someEffect,"name");
</PRE>
will return an "StereoEffectStack::null()" as outstack(), and calling a
method on that will segfault. It should be possible with some coding to
create a callback once proxies (soundserver) become invalid, so that the
applications can be terminated more or less gracefully before they come
in the situation that they can crash.
</P><P>
Tobi volunteered to care about this.
</P><P>
<H4><A NAME="userfriendliness"></A>General userfriendliness</H4>
</P><P>
Jeff Tranter volunteered to care about the following two TODO items:
<UL>
<LI>add kmessagebox style output of error messages (often, people
  don't know why artsd is not running, does not start, doesn't like
  their soundcard,...)</LI>
<LI>add -S option (to change the autosuspend timeout)</LI>
</UL>
</P><P>
Note: Actually, it seems he has already completed this. ;)
</P><P>
<H4><A NAME="cls/c"></A>CSL/C API</H4>
</P><P>
Providing a compatible way to do sound based on aRts in KDE/Gnome would be
nice. It seems that having a pure C implementation for the CLIENT is a good
idea for Gnome. CSL intends to provide that. This doesn't mean that CSL is
replacing the aRts C API (it will be kept for compat reasons but might end
up being a thin CSL wrapper one day).
</P><P>
I am working on CSL a bit, and maybe it will be more ready/popular soon.
</P><P>
<H3><A NAME="kmedia2"></A>KMedia2</H3>
</P><P>
<H4><A NAME="gaplessplaying"></A>Gapless playing</H4>
</P><P>
When playing more than one file with noatun there is a gap of 0.5 seconds
between the current and the next file. We came to the conclusion that noatun
is causing this by polling when a file is done. Luckily for noatun, there
are change notifications in aRts, so that it should be possible to find out
when a file is done using these.
</P><P>
That way, no gap should occur. Noatun will need to implement an MCOP object
(and be a server) to do this.
</P><P>
Charles Samuels volunteered to care about this.
</P><P>
<H4><A NAME="playobjects"></A>PlayObjects for custom data streams</H4>
</P><P>
Arts::PlayObject has as interface for specifying what to play a
<PRE>
   bool loadMedia(string filename)
</PRE>
method. So PlayObjects always play local files, and nothing else. Thats
suboptimal, because you can't for instance get an mp3 from a web server
(via KIO) and play it. The ideal way would be to use KIO, in a KDE process
(for instance noatun), and stream the data to the playobject, like
<PRE>
   [ noatun ]                                  [ artsd ]
     KIO class   ------- [ stream ] ---------> PlayObject
</PRE>
That way, the PlayObject could play anything that KIO can read (and thats
a lot) without artsd linking KIO itself.
</P><P>
There were some suggestions for doing the actual streaming, like
<UL>
<LI>RTP (Internet standard, documented in a RFC2068 and RFC2326)</LI>
<LI>call a method and pass the data (PlayObject->playData(sequence<byte> rawData)</LI>
<LI>http://www.arts-project.org/doc/mcop-doc/async-streams.html (asyncronous
   MCOP streams)</LI>
</UL>
</P><P>
Probably using the MCOP streams will offer the best integration/performance.
They are also used by artscat and artsdsp (and the C API) so they are quite
tested. The only problem will be seeking in a total asynchronous deliver/send
model.
</P><P>
It's a bit undecided who wants to do this. Neil Stevens and/or Charles Samules
might be the developers who are most interested in getting this done.
</P><P>
<H4><A NAME="x11embedding"></A>X11 Embedding</H4>
</P><P>
Currently, noatun will open an extra window when playing videos. The optimal
solution would be that noatun embeds the video image.
</P><P>
Neil Stevens volunteered to take care about this.
</P><P>
<H4><A NAME="kaboodle"></A>Kaboodle</H4>
</P><P>
There was a bit of talk about moving kaboodle to kdemultimedia. Kaboodle is
an alternative media player which is like noatun based on the KMedia2
architecture. It has two differences to noatun: it's small (as lightweight
as possible), and it can embed itself into konqueror.
</P><P>
Nobody was against that, except making it default is probably not such a
good idea (as noatun is by definition more powerful).
</P><P>
It might also be nice to support reading HTML-pages with embedded wav files
and things like that.
</P><P>
Note: Charles told me noatun should be competitive in speed with kadaboodle
after the recent changes (i.e. kdeinit support).
</P><P>
<H3><A NAME="guis"></A>GUIs for aRts objects</H3>
</P><P>
Often, aRts objects (like the freeverb effect) will need a GUI. The GUI should
be sharable between artscontrol and noatun (and other possible aRts aware
apps). An important design restrictions to meet is, that it should be possible
to keep effect GUIs toolkit independant.
</P><P>
So the idea for KDE2.2 might be having a "factory interface", which provides
a way to obtain a GUI for a given effect (like Arts::Synth_FREEVERB). This
factory might
</P><P>
<UL>
<LI>autogenerate a GUI out of hints</LI>
<LI>get the GUI implemented in terms of Arts::Widget, Arts::Poti and such</LI>
<LI>get the completely implemented GUI</LI>
</UL>
</P><P>
For the first two cases, this will be toolkit independant.
Arts::Widget, Arts::Panel, Arts::Poti and so on are interfaces.
</P><P>
There is a "Requires=kde_gui" line in the implementation (.mcopclass file),
so the Requires/Provides mechanism of MCOP will be able to select a fitting
one for each application (i.e. Gtk version for Gtk apps). The GUI will be
built inside the client application and thus be able to interact with the
application toolkit in a natural way.
</P><P>
Of course, the whole GUI issue will not only be useful for effects, but also
for PlayObjects (configure interpolation quality of the mikmod .xm engine),
Instruments, and all other kinds of objects.
</P><P>
I hope to get that done until KDE2.2.
</P><P>
<H3><A NAME="mixer"></A>Mixer issues</H3>
</P><P>
There was quite some discussion about mixers. The facts until now are:
</P><P>
<UL>
<LI>kmix is about the hardware mixer only;</LI>
<LI>aRts doesn't know anything about hardware mixers at all;</LI>
<LI>artscontrol controls only what artsd does, which is among other things,
   controlling the output effect stack, midi instruments and so on; and</LI>
<LI>it might be nice to have aRts simulate a multichannel software mixer with
   effects.</LI>
</UL>
</P><P>
<OL>
<LI>Stefan Schimanski is working on giving kmix pluggable architecture, that
will work on the base of the Qt object model and dynamic loading, so it
might be possible to easily plug aRts into kmix. A "one mixer for everything"
approach might be more useful from a user-point-of-view than the current split.
</LI>
<LI>Still, the general problem might be that aRts itself has no real
"abstraction" for mixers in the IDL. There is the audiomanager thing that
assumes the following:</LI>
<UL>
<LI>there are apps that dynamically come and go</LI>
<LI>there are destinations (in form of busses) where audio might get played or
  recorded</LI>
<LI>you can assign apps to destinations</LI>
</UL>
</P><P>
On the other hand, one possible kmix model for apps would be dynamically
appearing channels per app, i.e. a new quake channel would pop up once you
start quake (whereas the current aRts idea would be more like: there are
eight static audio outputs, audio01 .. audio08, and once you start quake,
you can assign it to one of these channels).
</P><P>
It might be nice to add an IDL based abstraction for "mixers", which will
work the same for hardware and software mixers. This would provide things
like network transparent (and language independant) mixer control. Maybe
it would also make a good addition to the "AudioManager" style mixer
interface we have now.
</P><P>
This abstraction would also make it possible to have different clients
access the mixer abstraction in a unified way. I.e. a Gtk frontend to
the mixer would use the same software/hardware mixer than the KDE frontend.
Turning the volume in the KDE frontend would make the sliders move int he
Gtk frontend as well.
</P><P>
<LI>Entierly getting rid of artscontrol might not be the way to go, as there
are midi related tasks in artscontrol which really don't belong into a
mixer. But somehow unifying mixing would be "user-friendlier" than what
we currently have.</LI>
<LI>aRts model is powerful enough to provide a multichannel, effect based
mixer as software (and thats little code), and it might be possible to add
that to artscontrol. Assignment of programs to channels is trivial. You
can have equalizers (Synth_STD_EQUALIZER), per-channel-effects and so on.
It will work on all applications. It should be implementable with not too
much work (less than 1000 LOC).</LI>
</OL>
</P><P>
Whatever, this is a bit of a are research topic ;), and different things
need to be done.
</P><P>
Stefan Schimanski is working on the kmix plugin architecture (1).
Stefan Gehn wanted to have a look at the GUI side of building a mixer (4).
I can help with getting the necessary backend work done for (4).
</P><P>
<H3><A NAME="conclusion"></A>Conclusion</H3>
</P><P>
From the aRts point of view, the soundserver architecture and the core MCOP
technology seem solid, so what we do here is mostly polishing.
</P><P>
Then, there are services basing on the MCOP/IDL model. The most important
service here is KMedia2, which allows playing arbitary media. The current
IDL interfaces are good for many simple tasks, but more needs to be added
for more complex tasks.
</P><P>
Finally, there are areas which are not yet standarized in any way by the
IDL interfaces. We talked about GUI and mixer, but there are others where
ino work has been done yet (like hard disk recording or video filtering),
and others where more work needs to be done (like midi).
</P><P>
And of course, thats just one side of viewing things. Existing apps, such as
kmix, kmid(i) and games often do not take fully advantage of what aRts can
provide, or have goals/directions in which they develop as well. Sometimes,
integration (as for libkmid or a timidity PlayObject) might be a good idea,
sometimes, adding a bit of "interoperability code" with aRts might be the
right way to go.
</P><P>
As mentioned at the beginning, this meeting looked mostly at aRts.
</P><P>
<A NAME="participants"></A><H3>Participants</H3>
<UL>
  Antonio Larrosa<BR>
  Stefan Schimanski<BR>
  Nikolas Zimmermann<BR>
  Stefan Westerfeld<BR>
  Stefan Gehn<BR>
  Jeff Tranter<BR>
  Lee Jordan<BR>
  Jono Bacon<BR>
  Neil Stevens<BR>
  Achim Bohnet<BR>
  Charles Samuels<BR>
  Manuel Fischer<BR>
  Carsten Pfeiffer<BR>
  Tobi (missing your realname)<BR>
  (... there was a lot of joining and leaving on the channel, so maybe
       I forgot somebody ...)<BR>
</UL>
</P>


