---
title: "People Behind KDE: Anne-Marie Mahfouf"
date:    2001-05-21
authors:
  - "Inorog"
slug:    people-behind-kde-anne-marie-mahfouf
comments:
  - subject: "Evariste Gallois"
    date: 2001-05-21
    body: "After reading this nice interview I was wondering if Anne-Marie had named one of her sons '\u00e9variste'... \n(I don't know if it is a good idea).\n\nCheers,\nCharles\n\nPS : Put the Cheese Cake recipe on CVS..."
    author: "Charles"
  - subject: "Re: People Behind KDE: Anne-Marie Mahfouf"
    date: 2001-05-21
    body: "Has anybody else watched \"Miss Congeniality\"?  You know the part where they are mocking the fact that the answer to \"If you could have one wish, what would it be?\"  is always \"World Peace.\"\n\nThat reminds me of how the answer to \"What application do you think everyone should have?\" is always \"Konqueror.\"\n\nViva la Resistance!"
    author: "Henry Stanaland"
  - subject: "Re: People Behind KDE: Anne-Marie Mahfouf"
    date: 2001-05-21
    body: "I think that question should be rephrased to \"what application not in kdebase should everyone have?\" or something like that. Every KDE user probably *has* Konqueror."
    author: "Rob Kaper"
  - subject: "Re: People Behind KDE: Anne-Marie Mahfouf"
    date: 2001-05-22
    body: "This I agree to... \"What application should all kde-users have?\" <-- that's actually kind of stupid if you list an application in KDE, as every kde-user would already have it..."
    author: "Christian A Str\u00f8mmen [Number1/NumeroUno]"
  - subject: "Re: People Behind KDE: Anne-Marie Mahfouf"
    date: 2001-05-23
    body: "hmm, actually, I keep running into people in IRC (sort of, virtually) that use only bits and pieces from KDE like one has the desktop but not kicker, the other uses only kdevelop, etc...\nThat's why maybe I did not give the question a second thought.\nWhat would be interesting is a survey on what the linux users actually install and use from all the desktops and applications available in a distro.\n\nAs for the interviews, I think they are very nicely done and I read them since the first. I am pleased to be able to know people better, people that I speak to on IRC. The sense of community is enhanced by these interviews."
    author: "Anne-Marie"
  - subject: "Off Topic"
    date: 2001-05-22
    body: "This is off topic, but everytime I post to one of these things I get an e-mail asking me for help smuggling some money from Africa to my account in the US(obviously it's a scam where they will actually transfer money from my account to their account).\n\nHas anybody else been getting these?  This was is from DR.BELLO ABU.   You would think someone trying to steal your money would have the decency to turn off Caps Lock.  Here is a snipet:\n\n\"SOMETIME AGO, A CONTRACT WAS AWARDED TO A CONGLOMERATE OF FOREIGN COMPANIES \nBY MY COMMITTEE ON BEHALF OF (NNPC),THE CONTRACT WAS OVER INVOICED TO THE \nTUNE OF US$24.5 MILLION. THIS WAS DONE DELIBERATELY, THE OVER INVOICING WAS \nA DEAL BY MEMBERS OF MY COMMITTEE TO BENEFIT FROM THE PROJECT, WE NOW DESIRE \nTO TRANSFER THIS MONEY WHICH IS PRESENTLY IN A SUSPENSE ACCOUNT OF THE NNPC \nIN THE DEBT RECONCILIATION COMMITTEE(DRC) INTO AN OVERSEAS ACCOUNT WHICH WE \nEXPECT YOU TO PROVIDE FOR US.\""
    author: "Henry Stanaland"
  - subject: "Re: Off Topic"
    date: 2001-05-22
    body: "I get no such emails, myself, but could be that my university filters spam."
    author: "Navindra Umanee"
  - subject: "KDE Deals with Excellency?"
    date: 2001-05-21
    body: "Nice slogan, Anne-Marie, but I think \"KDE Defines Excellency\" would even be better. :-)"
    author: "Rob Kaper"
  - subject: "Maybe a new format is needed"
    date: 2001-05-22
    body: "While I appreciate these and think they are a good thing, it would be nice if Tink actually interviewed these people.  An interview is a conversation.  Sure, you'll have a set of questions that you might want to reference but what someone answers to one question might affect what you wish to ask them next.  What we have now is people just filling out a questionaire, and it's starting to seem a bit boring.  Everyone gets the same questions.  There aren't any other questions about specifics that they respond with that help to bring out interesting tidbits.\n\nJust on observation."
    author: "MattP"
  - subject: "Re: Maybe a new format is needed"
    date: 2001-05-22
    body: "I have to agree.  Tink is doing a nice job, but every point you mentioned stands.  I guess this is because Tink is mostly into desktop publishing, not really the interviewing business.  So she sends people a questionnaire and then sets up a webpage with the answers... little or no interaction goes on."
    author: "KDE User"
  - subject: "Re: Maybe a new format is needed"
    date: 2001-05-22
    body: "Each of us has this much time into her(his) hands. It is already a Sisyphus job to get people answer their mail with the questionary. Conducting a conversation by e-mail would really be very tedious. Tink does her best to select questions that make sense for all the rather large panoply of very different people that KDE brings together. I believe we should consider us happy to have these and to realize that their contribution at pushing KDE to the public is very high."
    author: "Inorog"
  - subject: "Re: Maybe a new format is needed"
    date: 2001-05-22
    body: "I think it's very interesting to see how different people answer the same questions. How many simularities there are amongst kde-workers but also how different their views can be about the same subject. Asking questions is a good way of taking a peek into the life of a developer/translator/writer and looking a bit farther than just the code. If you want to know about that person there's nothing holding you back, you can always email them and start a personal conversation."
    author: "Tink"
  - subject: "Re: Maybe a new format is needed"
    date: 2001-05-22
    body: "If anyone of you think he/she can do better,can spend a couple a days a week on this, feel free to take over. Although I like working on it and thinking of new questions very much, I'll be happy to step aside if what someone else can offer is a big improvement.\nI'll be expecting your email with details.\n\n--\nTink"
    author: "Tink"
---
Are you looking for proof that the KDE Project is about more than just an assortment of code? Or are you trying to understand the joy and passion that people seem to share for this project? Then you may enjoy <a href="http://www.kde.org/people/annma.html">this interesting interview</a> that Anne-Marie Mahfouf has granted to <a href="mailto:tink@kde.org">Tink</a>. Anne-Marie is interested in KDE documentation, translations and eduware. She is only too right when she says that the documentation and translation teams deserve more of our praise and attention.
<!--break-->
