---
title: "KuickShow for KDE 2.1"
date:    2001-03-16
authors:
  - "numanee"
slug:    kuickshow-kde-21
comments:
  - subject: "Re: KuickShow for KDE 2.1"
    date: 2001-03-16
    body: "Cool.. but if Imlib is faster than Qt image loading, then why dosn't KDE use it everywhere? Even Qt can look at their code and make their code faster!\n\nAlso, wouldn't it be great if konqueror had an option \"View the current directory using KuickShow\" option! konqueror is pretty slow for directories with 100s of images IMHO.\n\ngood work!"
    author: "sarang"
  - subject: "Re: KuickShow for KDE 2.1"
    date: 2001-03-16
    body: "Yeah, why doesn't KDE use Imlib?\n\nBTW, Gnome has developed Gdk-Pixbuf, which is even faster and superiour to Imlib.\nWhy doesn't KDE use Gdk-Pixbuf?"
    author: "Kirby"
  - subject: "Re: KuickShow for KDE 2.1"
    date: 2001-03-16
    body: "> Yeah, why doesn't KDE use Imlib?\n\nYou give a partial answer with your next question : because even Gnome has thrown it away. A more complete answer is that the API really really sucks, and trying to make a reasonable wrapper for C++ is a huge PITA.\n\n> Why doesn't KDE use Gdk-Pixbuf?\n\nIntegration, code consistency, stability, documentation, etc..."
    author: "Guillaume Laurent"
  - subject: "Re: KuickShow for KDE 2.1"
    date: 2001-03-16
    body: "<I>\"because even Gnome has thrown it away.\"</I>\n<p>\n\"Even\"? Anyway, why does KuickShow use it if Imlib is too bad?\n<p>\n<I>\"the API really really sucks\"</I>\n<p>\nThen write a C++ wrapper with a good API.\n<p>\n<I>\"Integration, code consistency, stability, documentation, etc..\"</I>\n<p>\nWhat's wrong with the integration, code consistency and stability?<br>\nJust because it's 0.x doesn't mean it's unstable.\nAnd what's wrong with the documentation?\n"
    author: "Kirby"
  - subject: "Re: KuickShow for KDE 2.1"
    date: 2001-03-16
    body: "> Anyway, why does KuickShow use it if Imlib is too bad?\n\nIf I recall correctly it was ok if all you want to do is load and display images. Trying to manage pixmaps was quite more painful.\n\n> Then write a C++ wrapper with a good API.\n\nBe my guest.\n\n> What's wrong with the integration, code consistency and stability?\n\nWhat's wrong with using a library which depends ona whole slew of others, and has a completely different API than the rest of the KDE ?\n\n> And what's wrong with the documentation?\n\nLast time I looked (a long time ago), the little that existed sucked big time."
    author: "Guillaume Laurent"
  - subject: "Re: KuickShow for KDE 2.1"
    date: 2001-03-17
    body: "<I>\"What's wrong with using a library which depends ona whole slew of others,\"</I>\n\nGDK and Glib are the only dependencies.\nIf you call 2 dependencies \"a whole slew\", then what do you call KDEBase's dependencies?\n\n<I>\"Last time I looked (a long time ago), the little that existed sucked big time.\"</I>\n\nAnd just when was the last time you checked?"
    author: "Kirby"
  - subject: "Re: KuickShow for KDE 2.1"
    date: 2001-03-17
    body: "You're correct, there aren't that many dependencies. However does including GDK and GLib in KDE really makes sense to you ?\n\n> And just when was the last time you checked?\n\nMore than a year ago. But you're right, Raster probably found the time to write a great doc for it while writing imlib2. :-)"
    author: "Guillaume"
  - subject: "Re: KuickShow for KDE 2.1"
    date: 2001-03-17
    body: "Actually you don't need gdk or glib AFAIK. Imlib1 actually consisted of two libraries, one with GDK, one plain Xlib. I used the plain Xlib one.\n\nWith Imlib2, I don't even see any GDK at all."
    author: "Carsten Pfeiffer"
  - subject: "Re: KuickShow for KDE 2.1"
    date: 2001-03-17
    body: "Apparently you lost track of the thread :-)\nThe previous comments were about gdk-pixbuf,\nnot imlib."
    author: "Bernd Gehrmann"
  - subject: "Re: KuickShow for KDE 2.1"
    date: 2001-03-18
    body: "Well, no, I actually thought Kirby was still talking about imlib1 :-).\n\nOh well. That doesn't change much. While I agree that any of imlib[12] or gdk-pixbuf might make sense for an image viewer, using any of them in KDE would be a huge blunder.\n\nThere. :-)."
    author: "Guillaume Laurent"
  - subject: "Re: KuickShow for KDE 2.1"
    date: 2001-03-16
    body: "BTW, Raster has developed Imlib2, which is even faster and superior to both imlib and gdk-pixbuf.\nWhy doesn't KDE and Gnome use Gdk-Pixbuf? ;)"
    author: "oliv"
  - subject: "Re: KuickShow for KDE 2.1"
    date: 2001-03-16
    body: "BTW, Raster has developed Imlib2, which is even faster and superior to both imlib and gdk-pixbuf.\nWhy don't KDE and Gnome use Gdk-Pixbuf? ;)"
    author: "oliv"
  - subject: "Re: KuickShow for KDE 2.1"
    date: 2001-03-16
    body: "hmmm, that is an interesting suggestion.  I would like to turn on \"preview mode\" for graphic files, but that would be too slow in large directories.  I think if the kde folks could integrate imlib or kuickshow into the preview function, that would be great.  I like the preview function now as it is... however, if there is something better then why not?"
    author: "erotus"
  - subject: "Re: KuickShow for KDE 2.1"
    date: 2001-03-16
    body: "I tried Imlib2 (final) for enlightenment-0.17 (alpha version) and I can tell you that it's a lot faster than gdk-pixbuf and imlib1.\n\nImlib2 is appreciate on my slow computer ;-)"
    author: "Jean-Christophe Fargette"
  - subject: "Re: KuickShow for KDE 2.1"
    date: 2001-03-16
    body: "> I tried Imlib2 (final) for enlightenment-0.17 (alpha version) and I can tell you that it's a lot faster than gdk-pixbuf and imlib1.\n>\n> Imlib2 is appreciate on my slow computer ;-)\n\nOhh, Imlib2 is already released? Not a beta? I will certainly use that in the next versions, then.\n\nCheers,\nCarsten"
    author: "Carsten Pfeiffer"
  - subject: "Re: KuickShow for KDE 2.1"
    date: 2001-03-16
    body: "Imlib2 1.0.0 was realeased on October 16, 2000.\nthe latest one, Imlib2 1.0.2 was released on March 2, 2001\nMore details here:\nhttp://sourceforge.net/projects/enlightenment/\nand there\nhttp://sourceforge.net/project/showfiles.php?group_id=2\n\nAlso, avoiding Imlib (1) is good because it's not really maintained, and there has been a fork between Gnomers' version and Raster's version, so getting help for might not be easy in the future."
    author: "oliv"
  - subject: "Re: KuickShow for KDE 2.1"
    date: 2001-03-17
    body: "Concerning speed of Imlib2... for Enlightenment it might make a difference, due to all the effects, animations etc., but in an imageviewer it does not make a big difference. I did a quick port of KuickShow to Imlib2 and it's not obviously faster.\n\nDon't get me started on the new API :-/\n\nRegarding Imlib1: I've been using many versions and the only requirement ever was to use a version >= 1.5. The Xlib version hardly got any changes at all. That said, I do have problems with 1.9.8 under Openwin (Solaris7) currently :}"
    author: "Carsten Pfeiffer"
  - subject: "Re: KuickShow for KDE 2.1"
    date: 2001-03-17
    body: "Because if you where selling an API you wouldn't want to use LGPL code in it, that's the same reason that Qt dosen't use OpenOffice's Open2D printing API."
    author: "John"
  - subject: "Re: KuickShow for KDE 2.1"
    date: 2001-03-20
    body: "Damn right!!!\nQT is GPL not LGPL.\nYou must pay for using it for commercial purposes."
    author: "Hajdi"
  - subject: "Re: KuickShow for KDE 2.1"
    date: 2001-03-16
    body: "I think a tool like KuickShow is really a must-have for KDE, and honestly, I don't really care what lib it is build on, as long as it works nice & quickly.\n\nI do think that unfortunately, the UI of kuickshow is a little flawed. Under windows, I've gotten to know & love ACDSee, IMHO the best image viewer available. Carsten, if you read this, please take a look at the attached screenshot, and if you'd like, let it inspire you ;)\n\nThank you very much for your work, it is highly appreciated!"
    author: "me"
  - subject: "Re: KuickShow for KDE 2.1"
    date: 2001-03-16
    body: "I agree, ACDSee rules.\n It doesn't use any stinking scrollbars either, it uses the hand cursor to shove the image around.\nI wish Konqueror did that!"
    author: "reihal"
  - subject: "Re: KuickShow for KDE 2.1"
    date: 2001-03-16
    body: "Hmm, never smelled a thing when using konqueror to view images !\n\nWell, whatever."
    author: "Jelmer Feenstra"
  - subject: "Re: KuickShow for KDE 2.1"
    date: 2001-03-16
    body: "> I agree, ACDSee rules.\n> It doesn't use any stinking scrollbars either, it uses the hand cursor to shove the image around.\n\nAs does KuickShow btw :) The only problem: I don't have a nice handcursor to show, so you have to scroll with the normal arrow cursor :-/\nAnyone who can provide a nice handcursor?\n\nCheers,\nCarsten"
    author: "Carsten Pfeiffer"
  - subject: "Re: KuickShow for KDE 2.1"
    date: 2001-03-17
    body: "He, that was fast.\nThe hand should be Konqi's green, three-fingered hand, ofcourse.\n \n(I don't like various bars stealing precious screen space)"
    author: "reihal"
  - subject: "Re: KuickShow for KDE 2.1"
    date: 2001-03-17
    body: "> The hand should be Konqi's green, three-fingered hand, ofcourse.\n\nExcellent idea :) Cursors in X can only have two colors tho :} Anyone feel like creating a bitmap of konqi's hand?"
    author: "Carsten Pfeiffer"
  - subject: "Re: KuickShow for KDE 2.1"
    date: 2001-03-18
    body: "take a look at this:\n<br><br>\n<A href=\"http://apps.kde.com/na/2/show/id/1008/ss\">http://apps.kde.com/na/2/show/id/1008/ss</a>\n"
    author: "anonymous"
  - subject: "Re: KuickShow for KDE 2.1"
    date: 2001-03-19
    body: "Thats a Mickey Mouse glove, Konqi have two fingers and thumb, <a href=\"http://www.kde.org/img/logos/kdetshirt_release_small.jpg\">http://www.kde.org/img/logos/kdetshirt_release_small.jpg</a>\n"
    author: "reihal"
  - subject: "Re: KuickShow for KDE 2.1"
    date: 2001-03-19
    body: "how about Qt::pointingHandCursor ?"
    author: "aleXXX"
  - subject: "Re: KuickShow for KDE 2.1"
    date: 2001-03-19
    body: "That's the netscape one (aka the ugly one), right? ;) This one is used to show that you are able to \"click\" on something, e.g. a link, while in KuickShow, I need one showing that you can \"move\" something. Photoshop uses an open hand, and I'd like to use something similar.\n\nI downloaded the other imageviewer as pointed out somewhere above and I'll probably use that one."
    author: "Carsten Pfeiffer"
  - subject: "Re: KuickShow for KDE 2.1"
    date: 2001-03-16
    body: "Hi,\n\nyes, ACDSee has evolved quite a bit since I stopped using it (and Windows) in '98.\n\nThe directory tree from your screenshot might eventually be available automatically with KDE 2.2 (KuickShow uses the kfile library which will probably get a directory tree, soon). I do plan to add thumbnail support as well.\n\nI don't really see the need for the preview tho. The thumbnails do provide a small preview already. If you select a thumbnail, you get a medium-sized preview? Why that? Why not show the full image?\n\nCheers,\nCarsten"
    author: "Carsten Pfeiffer"
  - subject: "Re: KuickShow for KDE 2.1"
    date: 2001-03-16
    body: "Hi Carsten!\n\nThanks for your reply! Since I see you're interested in suggestions, I would like to tell you some more features and things I'd love in KuickShow.\n\nI work at a photographer, and there's a huge archive of pics, so a viewer like this is really a must. First, I think its wrong to have the app split up into several windows at the same time, one of them always resizing when you select a different picture. I think i'd be great when you either had to double click or selecte&ENTER a pic to change from thumbnail preview (with dirTree & preview) to fullscreen single-pic mode with zoom feature.\n\nAlso, another must-have feature is the creation of contact sheets. These are basically thumbnailpages, just for printing. It would be nice to be able to select how many rows & columns of pictures should be on a page, and determining the thumbnail size from that. Also, one should be able to optionally title the pics with their attributes (filename, size, format etc.)\n\nThe preview pane: It does make sense. Besides being configurable (you can kill it), this preview panes obviously allows you to view some more details of the pictures. This is very helpful, since (at least where I work), pictures only differ by a few small details, and this is the place you can find that out, before you have to load the full picture. This really isn't fun when you're working on a 200MB tiff.\n\nErmm, also: Wouldn't this be a great place to integrate thekompany's gphoto io-slave? I dunno how, but it kinda fits in there from the user's perspective...\nOf course, all of KuickShow could also be a KPart, integrated into Konq, but I think this is going a bit too far.\n\nThank you!"
    author: "me"
  - subject: "Re: KuickShow for KDE 2.1"
    date: 2001-03-17
    body: "hmm, ok, I'm sort of convinced about the preview thing :)\n\nFor the contact sheets, I see your need for it and agree it's a nice thing, but it is somewhat specialized (in another thread, it would be called bloat ;) I could add a plugin architecture so those sort of things could be added without affecting others. Actually, there is a kimgalleryplugin for Konqueror, which could be extended to do such things. Currently, it generates an html-page with all the images in one directory.\n\nFor the gphoto-ioslave, I will add support for that and remote files before 1.0"
    author: "Carsten Pfeiffer"
  - subject: "Re: KuickShow for KDE 2.1"
    date: 2001-03-16
    body: "For me, a main focus of any image management program imust be the ability to manage (do things to) large numbers of pictures at once.  I almost never use thumbnails because they consume too much space in the file list widget without providing enough detail to tell me whether I want to include a photo in a selection (to move, delete, whatever.)  \n\nA preview isn't necessarily just a \"medium-sized preview\"; it's the real picture, shrunk if necessary to fit into the preview pane (which in my case is usually about 640x480.)  The benefit, though, is that you can see the pictures, often at full resolution, while extending a selection. \n\nSo rather than having to go down-enter-look-^W a bunch of times and then remember where I wanted the selection to start, I can just go shift-down until I find a picture I don't want to include in the selection, then press shift-up and then the hot key for delete, move or whatever. It makes management of large image collections MUCH easier.\n\nMind you, no X11 image browser that I'm aware of will do this currently.  I had high hopes for both Konqueror/pixie and Nautilus to provide this sort of functionality, but neither to date has done so.  \n\nI wrote a simple patch for GQView, a C/gtk image browser (http://gqview.sourceforge.net) to provide this sort of functionality -- it has a \"preview pane\" which makes up the bulk of the interface, but selecting via the keyboard in the official version doesn't update the preview pane -- but my two patch submissions were never acknowledged.  If anyone has used gqview and would like my patch, I'll gladly email it.  I'd be happy to help out with KuickShow in this regard, but (so far) I am allergic to C++.  \n\nI've also written a program in perl/ImageMagick to find images that look like duplicates, and wanted to port it to C/Imlib some time ago but the documentation for Imlib seemed to be nonexistent.  I think that's another important part of image management and something that (last I knew) ACDSee doesn't even provide.  If you're ever interested in adding that kind of thing to Kuickshow and have trouble figuring out my lame algorithm, drop me a line.\n\nBut anyway, that's why I think a preview pane is more important than even thumbnails in an image management app.\n\nRob"
    author: "raindog"
  - subject: "Re: KuickShow for KDE 2.1"
    date: 2001-03-17
    body: "Another very cool thing with ACDSee: you can operate the slide-show with the scroll-wheel!"
    author: "reihal"
  - subject: "Re: KuickShow for KDE 2.1"
    date: 2001-03-17
    body: "> Another very cool thing with ACDSee: you can operate the slide-show with the scroll-wheel!\n\nIt advances to the next/previous image without stopping the slideshow?"
    author: "Carsten Pfeiffer"
  - subject: "Re: KuickShow for KDE 2.1"
    date: 2001-03-17
    body: "no, using the wheel does stop the slideshow. but in general, using the wheel to navigate in the pics is quite cool."
    author: "anonymous"
  - subject: "Re: KuickShow for KDE 2.1"
    date: 2001-03-17
    body: "Ah, I see. And moving it again makes it continue. Is the direction significant, or is it just an on/off thing?"
    author: "Carsten Pfeiffer"
  - subject: "Re: KuickShow for KDE 2.1"
    date: 2001-03-17
    body: "Huh? nonono, wait. The way it works in ACDSee is this:\n\nYou start a slide show. Then, somewhen, you decide you don't want that slideshow anymore, so you move the mousewheel. ACDSee reacts and shows the next picture when you move the wheel down, and the previous if you move it up. But the slideshow is stopped once you moved the wheel and won't start again until you explicitly tell ACDSee to do so.\n\nActually, this wheel-navigation-feature is quite independent of the slideshow. When you are in single-pic mode (as opposed to thumbnail&dirtree&preview - mode), you can view the next/previous pics with the wheel. Nothing more to it ;)\n\nOf course, the best thing would be to let the user choose whether using the wheel should stop a slideshow..."
    author: "me"
  - subject: "Re: KuickShow for KDE 2.1"
    date: 2001-03-17
    body: ">It advances to the next/previous image without stopping the slideshow?\n\nYes, open a folder with pictures, click on any one of them and scroll through the whole collection with the wheel, extra double-quick! \nOr, left-click on the folder, choose \"Browse with ACDSee\", double-click on any thumbnail, and scroll the collection.\nIt works automatically with Logitech MouseMan Wheel, the only mouse worth buying.\nI strongly advice that you check out this great programmme, free evaluation version.\n\nBy the way, mouse support in KDE 2.1 is just as bad as it was in KDE 1.0 beta, so I don't expect much. After all, *nixers are real men and real men don't use mice ;-) (Yes I know this is an XFree86 thing rather than KDE thing)"
    author: "reihal"
  - subject: "Re: KuickShow for KDE 2.1"
    date: 2001-03-17
    body: "Seems to be a misunderstanding, the rolling of the wheel IS the slide-show."
    author: "reihal"
  - subject: "Re: KuickShow for KDE 2.1"
    date: 2001-03-17
    body: "> Seems to be a misunderstanding, the rolling of the wheel IS the slide-show.\n\nOh, I see. That's just a one-liner, will add that :)"
    author: "Carsten Pfeiffer"
  - subject: "Re: KuickShow for KDE 2.1"
    date: 2001-03-16
    body: "Looks like the My Documents\\My Pictures folder display on Windows 2000 uses the same layout as well."
    author: "Chris Bordeman"
  - subject: "ACDSee"
    date: 2001-03-16
    body: "Definitely.  I think everyone wants an ACDSee style program.  I actually wanted to start a project like this, but it looks like I don't need to now. =)\n\nMy only real quibble is that the UI is a bit strange.  If I use the file dialog, then it doesn't go away when I load an image.  If I launch an image directly, then I don't get the file dialog (is there a way to bring it up?)\n\nBoth Kview and Pixie have similar issues with UI.  Maybe I'm just used to ACDSee.  IMO, the best part about ACDSee was that it was quick and was associated with every image type.  No more loading huge image programs just to view one file.  I just click on one, and navigate with pageUp/pageDown.  ACDSee is actually quite more than that, but I believe most of it is probably unnecessary.  The program has since doubled in size since I last downloaded it.  Bloat anyone?\n\nI wonder if there should be some organization between Kuickshow, Kview, and Pixie.  Kview and Pixie seem to be confused, as they share some features, and not others.  I think we should have just two programs: Kuickshow (which is what Kview probably should be) and Krayon (for editing).\n\nJust something to throw out there! ;)\n\n-Justin"
    author: "Justin"
  - subject: "Re: ACDSee"
    date: 2001-03-17
    body: "> Definitely. I think everyone wants an ACDSee style program. I actually wanted to start a project like this, but it looks like I don't need to now. =)\n\nWell, contributors are always welcome :)\n\n> If I use the file dialog, then it doesn't go away when I load an image. If I launch an image directly, then I don't get the file dialog (is there a way to bring it up?)\n\nYou can press Space to toggle showing the filebrowser. When launching kuickshow with an image as parameter I don't want to see the browser (I give it an image on the commandline to... well, see the image). If you give it a directory on the commandline, it will open the browser in that directory.\n\n> ACDSee is actually quite more than that, but I believe most of it is probably unnecessary. The program has since doubled in size since I last downloaded it. Bloat anyone?\n\nThat's my impression, too. The early versions had all the functionality I needed and it was very fast and loaded quickly. Maybe others need all the new functionality, but for me it was just slowing it down.\n\n> I wonder if there should be some organization between Kuickshow, Kview, and Pixie. Kview and Pixie seem to be confused, as they share some features, and not others. I think we should have just two programs: Kuickshow (which is what Kview probably should be) and Krayon (for editing).\n\nThis differences between these programs are:\n\nKView as a standalone program has about the same functionality as KuickShow. I think it has a few things KuickShow doesn't, like Wallpaper generation for example, but this can be added to KuickShow easily. But, KView also offers an embeddable KPart, used in Konqueror for example. I won't make a KPart of KuickShow, because it just doesn't make sense. Embedded into another app, you lose all the advantages of KuickShow (fullscreen, the filebrowser, switching between the files via PageUp/Down, resizing the image window).\n\nThe author of KView had the idea that we might use KuickShow as the standalone imageviewer and KView as the embeddable part in KDE.\n\nWith Pixie, the main advantages IMHO are the plugins/effects/image editing capabilities. I don't personally need such stuff (when I do, I use Gimp at the moment, and later probably Krayon)."
    author: "Carsten Pfeiffer"
  - subject: "Re: KuickShow for KDE 2.1"
    date: 2001-03-16
    body: "Yep, the Freshmeat entry shows Imlib2 to be at version 1.0.1. BTW when it comes to image viewing, I still find nothing better then PMView. An \"old\" OS/2 favorite.\n\nMarco."
    author: "Marco94"
  - subject: "Re: KuickShow for KDE 2.1"
    date: 2001-03-16
    body: "True. PMView remarkably won best image viewer one year over all windows offerings when everyone thought OS/2 was dead. They also produced a windoze version too. It was one of the best things on OS/2. Well worth looking at for anyone designing an image viewer.\n<p>\n<a href=\"http://www.pmview.com/\">http://www.pmview.com/</a>\n"
    author: "Eric Laffoon"
  - subject: "Re: KuickShow for KDE 2.1"
    date: 2001-03-16
    body: "Oh, man! This rules! :)\n\nSeriously, I think my quest is over. Finally a good imageviewer.\n\nThank you, Carsten!"
    author: "jd"
  - subject: "Re: KuickShow for KDE 2.1"
    date: 2001-03-16
    body: "KuickShow is great and very fast!\n\nI've just noticed a small resizing bug.\n\nSometimes when I move from a very large picture (larger than my desktop) to a smaller picture, the smaller picture has a frame the size of the large picture.  If I reclick the smaller picture, the frame is the correct size.  The opposite happens sometimes when I move from a smaller picture to a very large picture.  The window frame for the large picture is too small and the image is truncated. If I reclick the large picture the frame is the correct size. This doesn't always happen.\n\nI use KuickShow in window mode and I use KWin. Maybe I need to update my KWin."
    author: "KDE User"
  - subject: "Re: KuickShow for KDE 2.1"
    date: 2001-03-17
    body: "Hmm, this is KDE 2.1? I have noticed some problem where the frame could not be made smaller than a certain size, due to the WM dictating a minimum size for the decoration. But those problems you mentioned, I haven't experienced yet. Which decoration do you use and what size is your desktop and the image where this is happening?"
    author: "Carsten Pfeiffer"
  - subject: "Re: KuickShow for KDE 2.1"
    date: 2001-03-17
    body: "So, wouldn't it be a good idea for the trolls to look at the imlib-source and see what makes it so fast? I mean, they don't need to copy the API, do they?\n\nI think it would be of great benefit to KDE, seeing how many pixmaps & png's KDE has to load for icons, web browsing etc.\n\nWhy?"
    author: "anonymous"
  - subject: "New Window Decorations..."
    date: 2001-03-19
    body: "<a href=\"http://master.kde.org/~pfeiffer/kuickshow/screenshots/kuickshow.png\">http://master.kde.org/~pfeiffer/kuickshow/screenshots/kuickshow.png</a>\n<p>\nIt looks like Carsten has also given us a quick view of one of the next set of window decorations for KWIN.\n<p>\nNavindra, perhaps we should have a poll to see what window decorations people are actually using so that some of the lame ones can be left out of future KDE releases.\n<p>\nCheers,\n<p>\nAPW\n"
    author: "APW"
  - subject: "Re: New Window Decorations..."
    date: 2001-03-19
    body: "There are no lame ones.  ;-)"
    author: "Navindra Umanee"
  - subject: "Re: New Window Decorations..."
    date: 2001-03-21
    body: "Those Windows XP ones that you have a shot of are prety lame.  It's nice to know that KDE is back to copying windows."
    author: "Lee"
---
Many of you have been waiting for this one. <a href="http://www.kde.org/people/carsten.html">Carsten Pfeiffer</a> wrote in to tell us about the first release of <a href="http://master.kde.org/~pfeiffer/kuickshow/">KuickShow</a> since KDE 1.1.2 in July 1999. KuickShow is a nice image viewer based on <a href="http://www.rasterman.com/">Rasterman</a>'s <A HREF="ftp://ftp.enlightenment.org/enlightenment/enlightenment/libs/">Imlib</A>, and in case you don't remember, the killer feature of KuickShow was its blazing speed.  You won't be disappointed by the latest release for KDE 2.1 -- I was blown away. It's even noticeably faster than the venerable <A HREF="http://www.trilon.com/xv/xv.html">XV</A> (hint: command line geeks should use <a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdelibs/kinit/?hideattic=1">KDE Init</a> for that extra speed boost, others benefit automatically).  You can get KuickShow <a href="http://master.kde.org/~pfeiffer/kuickshow/">here</a>, view a couple screenshots (<a href="http://master.kde.org/~pfeiffer/kuickshow/screenshots/1.png">1</a>, <a href="http://master.kde.org/~pfeiffer/kuickshow/screenshots/2.png">2</a>, <a href="http://master.kde.org/~pfeiffer/kuickshow/screenshots/kuickshow.png">3</a>), or view the <a href="http://master.kde.org/~pfeiffer/kuickshow/ChangeLog">ChangeLog</a>.  Depending on your feedback, binary RPMs may soon be made available.  Read on for the details from Carsten.




<!--break-->
<p><i>
<a href="mailto:pfeiffer@kde.org">Carsten Pfeiffer</a> writes:</i></p>
<p>
<blockquote>
Some of you might remember an ancient program called KuickShow, a nice and very fast imageviewer for KDE1, last released in July '99. Due to other projects, the significant changes from KDE 1.x to 2.x, and a rewrite of KuickShow internals, the new version has been delayed until now.
<br><br>
At least Imlib didn't change. :) Yes, KuickShow uses Imlib to load and display images,  and that's one of the reasons for KuickShow being so fast. It also supports a lot of image formats, even some Photoshop *.psd files work.
<br><br>
The new KuickShow 0.8 works pretty much the same as the last version. You have a filebrowser to select the images to display. But IMHO, the best way to browse image galleries is to open an image, hit Return to toggle into fullscreen mode and then press PageUp/Down/Home/End to navigate between images. This is all amazingly fast, of course, have I mention that, yet? :) You say you won't download anything before you've seen a screenshot? I wouldn't either, so <a href="http://master.kde.org/~pfeiffer/kuickshow/screenshots/">here</a> they are.
<br><br>
Now if you want to view images kuickly, here's what you need:
<br><br>
<b>Requirements:</b>
<ul>
<li>kdelibs 2.1 (2.0 won't work)</li>
<li>Qt &gt;= 2.2.3</li>
<li>Imlib (together with libjpeg, libpng, libgif/libungif, libtiff)</li>
</ul>
<br>
Oh and don't forget <a href="http://master.kde.org/~pfeiffer/kuickshow/">KuickShow</a>!
Currently there is only a tarball and source-rpm (use --prefix=/where/your/kde/is/installed unless your KDE lives in /opt/kde2) available. <br><br>
I'd like to get some feedback about this version and will try to get some binary RPMs ready if nothing serious needs to be fixed.
<br><br>
If you used the old KuickShow for KDE1, you might stumble over a couple of things:<br>
<ul>
<li>the ".." entry in the filebrowser that took you into the parent directory is no longer available. Just as in the filedialog or in Konqueror, use Alt+Up (or the toolbar button) to change into the parent dir.</li>
<li>the change-directory shortcut & menu entry was removed. Simply start typing a filename or a directory name in the fileview. A small editfield will pop up to assist you.</li>
</ul>
Of course this is all mentioned in the documentation, but I'm betting that nobody's going to read that ;)
<br><br>

Thanks,<br>
Carsten
</blockquote>






