---
title: "People Behind KDE: Werner Trobin"
date:    2001-05-07
authors:
  - "Inorog"
slug:    people-behind-kde-werner-trobin
comments:
  - subject: "KDE Speed"
    date: 2001-05-07
    body: "For all those of you complaining about KDE speed, why not donate a SLOWER computer to Werner?  As a key developer in KOffice, it might help if you can get him to attempt to run KOffice on a 486 instead of an Athlon Thunderbird.\n\n;-)"
    author: "KDE User"
  - subject: "Re: KDE Speed"
    date: 2001-05-08
    body: "Hey, that made me laugh. :) Yes! Send this man some slowness!"
    author: "EKH"
  - subject: "Re: KDE Speed"
    date: 2001-05-08
    body: "Last month I saw Werner commiting to kde-cvs a bunch of patches to make KPresenter work (and especially start) faster. I think that Werner is a right person on a right place (KDE :)."
    author: "krzyko"
  - subject: "Re: KDE Speed"
    date: 2001-05-08
    body: "I've often wondered if there was any program out there to simulate a slow computer on a fast one.  For example, one that took up half of your CPU and RAM so that you could test programs for their speed on, say, a 486 with 32 MB of RAM.  It seems like a tool like that would be good for the KDE developers to have :-)  Programmers usually have fairly up-to-date computers and may not see the performance problems that their users do."
    author: "not me"
  - subject: "Re: KDE Speed"
    date: 2001-05-08
    body: "What's a 486?"
    author: "reihal"
  - subject: "Re: KDE Speed"
    date: 2001-05-08
    body: "Just run Netscape.. that blasted program takes up half the CPU and Memory all the time ;^)"
    author: "Steven Hatfield"
  - subject: "Re: KDE Speed"
    date: 2001-05-08
    body: "You can tell lilo right on startup that you just want to use, say, 32MB of RAM (by adding a plain \"mem=32m\" at lilo's boot prompt). Please read the BootPrompt-HOWTO for further information. This makes KDE crawl, though :}"
    author: "Werner Trobin"
  - subject: "kdeinit"
    date: 2001-05-07
    body: "He mentions kdeinit... What is it exactly?\n\nI know it's used to start up kde in the first place, but it also seems to be used to start any KDE program?\n\nWith ps, my Konquereor session looks like \"kdeinit: konqueror\".\n\nSort of annoying, because then I can't \"killall konqueror\".\n\nWhat's the point of that?"
    author: "AC"
  - subject: "Re: kdeinit"
    date: 2001-05-07
    body: "the majority of the startup time associated with kde apps is the linker resolving all those C++ functions.  However, if the new program is simply a child of kdeinit, the functions are already resolved.  Bottom line: it's a startup speed hack."
    author: "anonymous KDE wizard"
  - subject: "Re: kdeinit"
    date: 2001-05-08
    body: "Can't an application later on change it's argv[0] name to the original one? I remember I could do it in Perl...."
    author: "Toastie"
  - subject: "Re: kdeinit"
    date: 2001-05-08
    body: "Of course, but then you can do `kdekillall konqueror`. But what would you be so cruel?? :-))\n\nAh, BTW, the previous poster asked for speed. Well, kdeinit's role is just this: make sure that KDE binaries start faster than what Linux architecture allows naturally. That's why Werner loves it so much :-)"
    author: "Inorog"
  - subject: "Re: kdeinit"
    date: 2001-05-08
    body: "You can read more about kdeinit in kdelibs/kinit/README and on\n<a href=\"http://www.suse.de/~bastian/Export/linking.txt\">http://www.suse.de/~bastian/Export/linking.txt</a>\n<br><br>\nCheers,<br>\nWaldo"
    author: "Waldo Bastian"
  - subject: "Re: People Behind KDE: Werner Trobin"
    date: 2001-05-08
    body: "It's that c't article again ! Where can I find that article today ? Off course I would greatly prefer an english version (or danish) ;-)"
    author: "Stig"
  - subject: "Re: People Behind KDE: Werner Trobin"
    date: 2001-05-08
    body: ">> He is the first non-fantastic entity to answer the new series of questions \n\nIt took me a few moments to figure out that wasn't an insult.  Over here in America, that phrase equates to \"He is the first non-excellent person to answer...\".  You might change that to \"He is the first non-fantasy entity...\" (assuming that makes sense to readers in the UK).\n\nFantastic is generally a superlative meaning \"very good\".  Fantasy is the common adjective meaning \"having a nature of folklore and magic\".\n\n--\nEvan (Not a grammar flame, just a gentle hint as to the vernacular)"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: People Behind KDE: Werner Trobin"
    date: 2001-05-08
    body: "No, I (an American) got that quickly enough. You way of looking at it did give me a laugh, though. :)"
    author: "EKH"
  - subject: "Re: People Behind KDE: Werner Trobin"
    date: 2001-05-08
    body: "Moin Werner :)\n\nhow come that www.theincrediblemachine.net link is shown \"visited\" already in my konq? ;) Definitely one of the best games! I'd really like to see a Linux version of that."
    author: "gis"
  - subject: "Koffice"
    date: 2001-05-08
    body: "I have also noted that there does not seem to be many developers working on KOffice. A cautionary note here would perhaps be that we shouldn't expect too much of KOffice. \n\nA full office-suit that holds the same standards that MS Office has after ten years or so with development and god knows how many developers is obviously a major undertaking, and it may not be realistic that this emerges with from 2-3 part time developers any time soon. It could also be that OpenOffice realistically may be the best bet for a professional free office suit for Linux. Time will tell."
    author: "will"
  - subject: "...."
    date: 2001-05-08
    body: "Let no man say it cannot be done...\n\nit must be done,\n\n\nand we have UNDERTAKEN TO DO IT!\n\n(ra ra ra!) \n\ngo koffice!\n\nJason ;)"
    author: "Jason Katz-Brown"
  - subject: "Re: Koffice"
    date: 2001-05-08
    body: "Two things you seem to want to ignore:\na) the passion of developers\nb) the talent of developers\nc) (yes, I said two) the lack of interest into screwing the app for the sake of future paid upgrades.\n\nIf a productivity computer will be the same thing as today from now in 5 years, I'd rather bet on koffice being a standard at that time."
    author: "Inorog"
  - subject: "Re: Koffice"
    date: 2001-05-08
    body: "I have every confident in the KDE team.  Look at Konqueror and you'll see the ability of this team.  \n\nPassion and a great team is all it takes to make things come true.\n\n--kent"
    author: "Kent Nguyen"
  - subject: "Developers' platforms"
    date: 2001-05-08
    body: "A maybe offtopic question, but what platforms do the developers use? Do all the (core) developers use Linux on i386, or is there any other platform and OS in use?"
    author: "Loranga"
  - subject: "Re: Developers' platforms"
    date: 2001-05-09
    body: "i know some use FreeBSD. don't know of anyone using other *BSD, Solaris, etc."
    author: "Evandro"
  - subject: "Re: Developers' platforms"
    date: 2001-05-09
    body: "Most KDE developers use some form of Linux.  SUSE, Mandrake, or Redhat are the common.\n\nMore people and company support Linux than FreeBSD. :)\n\nI have friends who use FreeBSD, NetBSD, OpenBSD, JeeBSD, SecureBSD, and various other BSD.  And a few use Solaris.\n\n--kent"
    author: "Kent Nguyen"
---
<a href="mailto:trobin@kde.org">Werner Trobin</a> is a member of the hard working <A HREF="http://www.koffice.org/">KOffice</A> team. He is the first non-fantastic entity to <a href="http://www.kde.org/people/werner.html">answer</a> the new series of questions in <a href="mailto:tink@kde.org">Tink</a>'s weekly interview at the <a href="http://www.kde.org/people/people.html">KDE People page</a>. This is a very refreshing and colourful interview. Go there, read it and enjoy Werner's enthusiasm at being a part of KDE. We're lucky to have such teammates.
Also, please note that, as promised, Tink has added a <EM>People</EM> <a href="http://www.kde.org/people/people.html">index</a> with interviews, quotes and URLs.



<!--break-->
