---
title: "Does Your Online Bank Support Konqueror?"
date:    2001-12-11
authors:
  - "Dre"
slug:    does-your-online-bank-support-konqueror
comments:
  - subject: "KDE3 mention"
    date: 2001-12-11
    body: "Also just noticed <A href=\"http://lists.kde.org/?l=kde-promo&m=100806822230008&w=2\">this one from David</a>, where he points out that sites that don't work with KDE 2.2.2 may work with the upcoming KDE3, thanks to improved JavaScript support."
    author: "Navindra Umanee"
  - subject: "Re: KDE3 mention"
    date: 2001-12-11
    body: "Yes, improving Javascript in Konqueror is the way to attain compatability.  I don't think banks are discriminating directly against it.\n\nAt bankamerica.com, there is a gigantic heap of Javascript code.  I have read through it before, trying to modify it to work with Konqueror just to login (for konq 2.1.1).  The Javascript code works with IE and Netscape (even 4.7, so it does work in Linux).  If Konqueror could understand all of that code properly, then it would work there also.\n\nI think a bigger question here is: why the hell do bank websites need javascript?  I did not feel comfortable reading that huge heap of code.  It's not for additional security is it?  JS won't help you there anyway.  Security through major obscurity?  I actually see no reason for the code, and I really wonder if the bank web programmers are just clueless.  I vote that they just take out this pointless JS code.  Then they'd gain not just Konq compatability, but lynx!"
    author: "Justin"
  - subject: "Re: KDE3 mention"
    date: 2001-12-12
    body: "Bank of America works perfectly with kde2.2.1 and 2.2.2 with me"
    author: "radix"
  - subject: "Re: KDE3 mention"
    date: 2001-12-12
    body: "I just tested it now and you're right!\n\nI'm positive it didn't work in 2.1.1, and I'm pretty sure it didn't work in 2.2.1 either (or maybe it was 2.2.0...).  Maybe bank of america changed their page?  Anyway, it seems to be working.  I even made a payment.  Whee!"
    author: "Justin"
  - subject: "Re: KDE3 mention"
    date: 2001-12-12
    body: "Cripes, it's the other way around for me!  A while back (probably the 2.1 days) I was able to use Bank of America just fine (once I changed my UA string).  However with 2.2.1 and 2.2.2 I haven't been able to log in.  The page asking for Account #, etc, just loops.  I enter the info, and get sent back to the page.  Over and over.  This happens with a 2.2.1 box at home and a 2.2.2 box here at work (happened with 2.2.1 here at work as well).  Works for me in Mozilla, though, so my account is properly set up.  *shrug*"
    author: "KDE User"
  - subject: "Re: KDE3 mention"
    date: 2001-12-12
    body: "Change it back to konqueror"
    author: "radix"
  - subject: "Re: KDE3 mention"
    date: 2001-12-12
    body: "does this \"page\" display with Konq3 it fails due js with 2.2.2. Displays fine with netscape..."
    author: "AC"
  - subject: "HSBC (UK) and KDE3"
    date: 2001-12-11
    body: "Hi,\n\nHSBC (http://www.hsbc.co.uk/) works fine with Konqueror in KDE3.\n\nCheers,\n\n         Jono"
    author: "Jono"
  - subject: "Re: HSBC (UK) and KDE3"
    date: 2001-12-11
    body: "I went to their tech support page & it asks for details on OS & browser.\nThe only OSes listed are Windows & MacOS and the only browsers listed are IE4 & 5 as well as NS4."
    author: "Antonio D'souza"
  - subject: "Re: HSBC (UK) and KDE3"
    date: 2001-12-12
    body: "Works fine w KDE 2.2.2 aswell. (Atlwast I havn't had any problems.)\n\nAlex"
    author: "Alex Willmer"
  - subject: "First Union OK"
    date: 2001-12-11
    body: "While I don't use my banks online functions to their full capacity, First Union's (USA) online services:\n\nhttp://www.firstunion.com/FrontLineLogin.html\n\nseem to work just fine in KDE 2.2.2, and have since I first tried under 2.1.\nAll I've used it for is transferring between accounts and checking my balances, but that's good enough for me, at least."
    author: "Shamyl Zakariya"
  - subject: "Can't login to my bank account at..."
    date: 2001-12-11
    body: "... Credit Suisse\n\nhttps://directnet1.credit-suisse.com/dn5/ident_post.eval?FUNCTION=ANMELDEN\n\nand hope for KDE3 with it's improved JavaScript handling."
    author: "Tobias Hoevekamp"
  - subject: "Postbank (.nl)"
    date: 2001-12-11
    body: "Even the regular site has a problem, some of the frames do not load. This can be circumvented by finding the frame URL in the source and loading it manually.\n\nHave not tested the online banking features yet, but requested login information so I will soon."
    author: "Rob Kaper"
  - subject: "Re: Postbank (.nl)"
    date: 2001-12-12
    body: "The regular site seems to work fine with me in Konqueror.\n\nI've tried the online banking. I installed the latest (version 1.3.#) Java runtime environment from www.blackdown.org. The package includes plugins for Netscape version 4 and for Mozilla based browsers (Galeon, Mozilla). The online banking works with Mozilla and Galeon but not with Netscape 4.7.\n\nKonqeror doesn't work! I'm not sure why? In the Konqeror Netscape-plugins settings, it says to look for plugins in /usr/lib/mozilla/plugins and in /usr/lib/netscape/plugins. Which JAVA plugin is loaded by Konqeror I'm not sure? Maybe if I remove the /usr/lib/netscape/plugins line???"
    author: "Huib Wouters"
  - subject: "Re: Postbank (.nl)"
    date: 2001-12-12
    body: "The regular site seems to work fine with me in Konqueror.\n\nI've tried the online banking. I installed the latest (version 1.3.#) Java runtime environment from www.blackdown.org. The package includes plugins for Netscape version 4 and for Mozilla based browsers (Galeon, Mozilla). The online banking works with Mozilla and Galeon but not with Netscape 4.7.\n\nKonqeror doesn't work! I'm not sure why? In the Konqeror Netscape-plugins settings, it says to look for plugins in /usr/lib/mozilla/plugins and in /usr/lib/netscape/plugins. Which JAVA plugin is loaded by Konqeror I'm not sure? Maybe if I remove the /usr/lib/netscape/plugins line???"
    author: "Huib Wouters"
  - subject: "Re: Postbank (.nl)"
    date: 2003-10-16
    body: "I want to wire money to my son in the Netherlands through Postbank. How do I do this? Do you have a corresponding bank in the US?"
    author: "Johnnie Eiland"
  - subject: "send your submissions in by email"
    date: 2001-12-11
    body: "the web page asks for submissions to be emailed to: olistrut at gmx dot net\n\nwhile enjoyable, posting your bank experience here at theDot might not be the most effective way to get them listed on the web site. =)"
    author: "Aaron J. Seigo"
  - subject: "move to www.konqueror.org ?"
    date: 2001-12-11
    body: "Hi,\n\nI think it would be better and in a more prominent place if this page was located at www.konqueror.org.\n\nAnd, there are \"color codes\" defined on your page, all I saw was black text.\n\nBye\nAlex"
    author: "aleXXX"
  - subject: "Re: move to www.konqueror.org ?"
    date: 2001-12-11
    body: "i talked with oliver about those exact issues and he said that once they have enough reports together they will be moving it to konqueror.org. that is also why the page is so spartan at the moment as it will be moving to the konqueror.org webpage template."
    author: "Aaron J. Seigo"
  - subject: "Re: move to www.konqueror.org ?"
    date: 2001-12-12
    body: "Do you have style sheets enabled? Colors work fine with Konqueror 2.2.2  and Mozilla 0.9.6 and the page validates against HTML 4.0.1, so this seems to be a problem on your side."
    author: "Oliver Strutynski"
  - subject: "Konqueror and Java applets"
    date: 2001-12-11
    body: "Some of the sites listed on the page fail because the java applets employed there do not work fully. I think this is at the moment the biggest problem with using konqueror as an (apart from this issue the best) all purpose web browser.\n\nIn general java applets seem to load/initialise a lot slower in konqueror than in any other browser. Furthermore, since java<->javascript interaction (LiveConnect) does not work, a lot of applets on banking sites do not function correctly.\n\nWhat I would really love to see is that (at least optionally) konqueror would load applets through SUN's java plugin, and that LiveConnect would be implemented so that a plugin could have access to the browser and vice versa."
    author: "Magnus Kessler"
  - subject: "Re: Konqueror and Java applets"
    date: 2001-12-11
    body: "So true. My biggest problem with Konqueror is all the hack it needs to launch my webbank. Since I'm a Linux Newbe, it has taken me so long to get the right Java support for my box fixed right. I guess a lot of users out there has had the same problems with Java like me I would most definitly recomend a better Java embedding in 3.0 than that of now. Is the reason its not working very well now any licenseissues with Sun - or is it possible to have Java support with KDE without having to download it from Sun in the future?"
    author: "Fred"
  - subject: "Re: Konqueror and Java applets"
    date: 2001-12-14
    body: "I completely agree, applets do work but there is no interaction between the applets and the browser at the moment and this breaks many pages that use applets. In my opinion, this is one of the most important limitations of Konqueror at the moment, most other things are nicely standard compliant."
    author: "Marcel Offermans"
  - subject: "Re: Konqueror and Java applets - LiveConnect"
    date: 2002-03-14
    body: "\nis there any work currently being done on adding LiveConnect support to konqueror?"
    author: "geva"
  - subject: "Is there any work to be done on animated gifs?"
    date: 2001-12-11
    body: "Off topic but I would really like to know if this bug is being worked on for Konqueror in KDE 3. Basically the problem is some gif animations using 100% cpu.\nThese bug reports are all related to the problem:\nhttp://bugs.kde.org/db/29/29963.html\nhttp://bugs.kde.org/db/23/23368.html\nhttp://bugs.kde.org/db/29/29196.html\nhttp://bugs.kde.org/db/34/34433.html\nMaybe there's an explanation as to why it's hard to solve?\nNow back on topic my bank is http://nbc.ca This site will only work with IE or Netscape 4.x. Mozilla wont work (navigation menu JavaScript problem), Opera wont work (Tells me I'm navigating using bookmarks), Konqueror gives me blank page (JavaScript again I guess). \nThis means I have to keep Netscape 4.77 around just for one site and to tell you the truth I really don't expect the KDE team to make it work either. I'd much rather the bank to use some form of standard when writing their web site. I've emailed them but I think they are under the impression that there is only one OS out there."
    author: "linuser"
  - subject: "Re: Is there any work to be done on animated gifs?"
    date: 2001-12-11
    body: "as i understand it, the GIF problems are Qt issues. i've heard that Qt3 handles such things better, though i have no idea if this is just rumour or actual fact. =)"
    author: "Aaron J. Seigo"
  - subject: "SSL and Java problems here"
    date: 2001-12-11
    body: "Greetings\n\tI think this is a Java problem here, what fails for me is Java over SSL.\n\n\tI heard some info about this in the past but I have never seen anything come out of it.  Debugging bank web pages is a real problem because as much as I like what David is doing for KDE and KJS I am just not sure I want him tweaking with my bank account ;)\n\nI have contacted them and they seemed to take concern but I have yet to see anything.\n\nCheers\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: SSL and Java problems here"
    date: 2001-12-11
    body: "Well, you're developer yourself, but just to make sure it isn't the usual case:\nyou have the JSSE extension installed as described in the konqueror-java-howto\non konqueror.org?\n\nKevin"
    author: "Kevin Krammer"
  - subject: "Re: SSL and Java problems here"
    date: 2001-12-11
    body: "Java and SSL are a known issue on the PPC, although I have to admit I have not tried again with it since KDE 2.2.0.  Ill wander back there and give it a try.  The blackdown port of Java is lacking sound, ssl and some other minor things.\n\nI have never ateempted to try it on my Athlon, I figure when it works consistantly it will ship default with KDE, kinda how java works now.  \n\nCheers\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Washington Mutual"
    date: 2001-12-11
    body: "I use Washington Mutual for online banking.  There website www.wamu.com works great for me.\n\nI'm able to:\n1)  Login\n2)  Check my account info\n3)  Pay my bills\n\nI'm using KDE 2.2.1\nKonqueror 2.2.1 as well.\n\n--kent"
    author: "Kent Nguyen"
  - subject: "Re: Washington Mutual"
    date: 2001-12-12
    body: "http://www.wamu.com works for me as well. I'm using Konq 2.2.2"
    author: "Jake"
  - subject: "www.osuuspankki.fi"
    date: 2001-12-11
    body: "No-go with Konqueror. Doesn't work in Mozilla either. Have to use Windows :(. I have been planning to complain to them about that."
    author: "Janne"
  - subject: "Re: www.osuuspankki.fi"
    date: 2001-12-11
    body: "Postgirot.se explicitly looks for Netscape or Explorer *+ windows*. If it's the right version of Netscape, but not running under Windows, it just tells me to update my browser.. \n\nI mailed them about this, and they simply stated that most people use Windows. They weren't interested in changing anything.\n\nImportant sites shouldn't be designed by people like that... Because of pure laziness from their side, I have to fire up Windows to pay bills."
    author: "Johnny Andersson"
  - subject: "Re: www.osuuspankki.fi"
    date: 2001-12-12
    body: "Have you tried changing the user agent?  That would get you past their browser detection.  Then you only have to worry about whether or not Konqueror is up to the task of rendering the pages..."
    author: "not me"
  - subject: "Re: www.osuuspankki.fi"
    date: 2001-12-12
    body: "Yes, I have tried to change the user-agent. I can log in there without any problems, but the banking-section doesn't work (the menu where I choose what to do doesn't work). It's made with javascript (if I remember correctly), so the problem can be at Konquerors javascript-implementation."
    author: "Janne"
  - subject: "Re: www.osuuspankki.fi"
    date: 2001-12-12
    body: "It does work with Mozilla. Has always worked for me.\nMake sure you got the newest Mozilla version (including the psm package) installed.\n\nI haven't tried it with Konqueror yet.."
    author: "Joni"
  - subject: "KDE/Qt version of Mozilla?"
    date: 2001-12-12
    body: "I prefer Konq because it uses native widgets, but I'll admit that Mozilla probably has a (somewhat) wider acceptance base.  Has anyone tried to create a gecko based browser for KDE which uses Qt widgets similar to how Galeon uses gecko w. GTK?"
    author: "KDEuser"
  - subject: "Re: KDE/Qt version of Mozilla?"
    date: 2001-12-12
    body: "konq supports kmozilla instead of khtml for webpage rendering"
    author: "radix"
  - subject: "How to enable kmozilla?"
    date: 2001-12-13
    body: "I see that I have kdebindings-kmozilla installed on my system (RH 7.2) but how do I actually enable kmozilla on Konq?"
    author: "KDEuser"
  - subject: "Re: How to enable kmozilla?"
    date: 2002-10-30
    body: "in konq>> settings/ configure Konq , then file associtations.  Type in html, and expand text to find html the only choice.  click that and go to embedding. you should see this list on the bottowm KHTML, KMOZILLA.  if there is no KMOZILLA, click ADD then KMOZILLA.  if ther already is KMOZILLA, select it then push \"move up\".  hope this helps you out"
    author: "standsolid"
  - subject: "Re: How to enable kmozilla?"
    date: 2003-09-23
    body: "I'm on a Redhat 9.0, with apt from freshrpms and only kde (packages provided by kde-redhat).\nI've installed kdebindings (1.3.4), and went to Konqueror config, but there was no trace of kmozilla there (even on the ADD dialog box)...\nAnyone could help me?\n\nRen\u00ea"
    author: "Rene Assano"
  - subject: "Re: KDE/Qt version of Mozilla?"
    date: 2001-12-12
    body: "> I'll admit that Mozilla probably has a (somewhat) wider acceptance base.\n\nI don't think that's that true. In fact, until a few months ago, Mozilla was abolutely horrid. Now, I think there are more Moz users than Konq users, but not by much. I'd have to say that there are still many, many, NS 4.7 users. Opera also has a significant loyal user base."
    author: "jz"
  - subject: "Re: www.osuuspankki.fi"
    date: 2001-12-12
    body: "It doesn't work with me. I have the latest Mozilla installed with apt-get. The menu where you choose what to do doesn't work. Sometimes it seems to be trying to work, but it's unusable for me. I have had more luck with Mozilla than with Konqueror, but neither of them work well enough to be usable.\n\nDunno bout the psm, I'll check it out when I get back to Linux."
    author: "Janne"
  - subject: "Re: www.osuuspankki.fi"
    date: 2001-12-13
    body: "Mozilla-psm is the package that has SSL stuff and all that, so it is necessary.\n\nYou could also try if it would work with a new, clean profile.."
    author: "Joni"
  - subject: "Re: www.osuuspankki.fi"
    date: 2001-12-13
    body: "Mozilla-psm is the package that has SSL stuff and all that, so it is necessary.\n\nYou could also try if it would work with a new, clean profile.."
    author: "Joni"
  - subject: "Re: www.osuuspankki.fi"
    date: 2001-12-14
    body: "> No-go with Konqueror\nWhat happens exactly ?\n\nWith Konqueror from CVS (~ 3.0-beta1), the front page appears rather ok.\nWhat should I test (not having an account there, obviously) ?"
    author: "David Faure"
  - subject: "www.comdirect.de"
    date: 2001-12-12
    body: "I am using konqueror 2.2.1 on Suse 7.3. I had tried to access the german comdirect online bank www.comdirect.de. \n\nAs soon as you press the button 'login direct brokerage' you get a message, \nthat only Microsoft Internet Explorer Version 4.0 or higher, Netscape Communicator Version 4.5 or higher, as well as the usage of Netscape 6 Version 6.1 or higher is possible for access. \n\nAfterwards, it is stated, that you are currently using \"Mozilla/5.0 (compatible; Konqueror/2.2.1)\". I tried to fake a different browser type. But it did not help. \n\nFrom comdirect I got the message, that Java, java script and style sheets have to be activated on Netscape 4 (Version 4.78 works, but not very good).  \n\nHere is the original German message presented when konqueror is used:\n\nHerzlich willkommen bei *comdirect)\n \nHinweis:\nSie benutzen einen Browser, mit dem die Nutzung des Direct Brokerage der comdirect nicht m\u00f6glich ist.\nUm unser Direct Brokerage nutzen zu k\u00f6nnen ben\u00f6tigen Sie einen Microsoft Internet Explorer ab Version 4.0 oder einen Netscape Communicator ab Version 4.5. Au\u00dferdem ist die Nutzung des Netscape 6 ab Version 6.1 m\u00f6glich. Beta-Versionen der o.g. Browser sind nicht zugelassen.\n \nIm Augenblick benutzen Sie \"Mozilla/5.0 (compatible; Konqueror/2.2.1)\" als Web-Browser.\n \nSollten Sie weiterhin Probleme mit dem Zugriff per Internet haben, wenden Sie sich bitte in der Zeit von 8:00 bis 22:00 Uhr an unsere Hotline unter der Rufnummer (01803) 336440.\n \nVielen Dank.\ncomdirect"
    author: "Josy"
  - subject: "Re: www.comdirect.de"
    date: 2001-12-12
    body: ">I tried to fake a different browser type. But it did not help. \n\nAre you sure it worked?  If it says you are using konqueror when you press the button, you didn't change the user agent correctly.  If you change the user agent, they will not be able to tell that you are not using IE except with dirty hacks, and they certainly won't be able to tell that you are using Konqueror."
    author: "not me"
  - subject: "Re: www.comdirect.de - It works!!!"
    date: 2001-12-12
    body: "Your are right, it works!!! \n\nDuring login to direct brokerage (happens, when the button 'login direct brokerage' is pressed), the address changes from 'www.comdirect.de' to 'brokerage.comdirect.de'. \n\nThus the http site/domain name to fake is not 'www.comdirect.de' as I did before, \nbut it is 'brokerage.comdirect.de' or short'comdirect.de'. \n\nThanks!!!"
    author: "Josy"
  - subject: "Re: www.comdirect.de - It works!!!"
    date: 2001-12-12
    body: "Hi,\n\ntell me how far you got with konqi and comdirect.\n\nDue to some JS probs, there's no way in completing\nseveral forms. No brokerage etc.\n\nFor me, konqi2.2.2 is useless on comdirect.\n\n\n\nbye\nmph"
    author: "Michael P. H."
  - subject: "bank?"
    date: 2001-12-12
    body: "This is all and well if the problem is the bank, but if the problem is konqueror itself, and trouble reports are filed and seemingly ignored, pestering the bank will do little good.\n   _\n/Bjorn."
    author: "bjorn"
  - subject: "Re: bank?"
    date: 2001-12-12
    body: "Your bug reports are not being ignored.  Have patience!  I'll bet your bugs will be fixed in KDE 3."
    author: "not me"
  - subject: "Just change the User Agent !"
    date: 2001-12-12
    body: "Don't forget you can foul your bank by changing the 'User-Agent' Konqueror sends to your bank website.\nGo in the settings menu -> Configure Konqueror and then choose the icon 'User-Agent'.\nWorks for me... :)"
    author: "Mickael"
  - subject: "Re: Just change the User Agent !"
    date: 2001-12-12
    body: "This works for a number of sites (should be noted on the summary page), but the problem is that tweaking the UA string really perverts the idea of having a ua identifier in the first place: It makes it impossible to work around peculiarities in specific browsers and does not allow webmasters to generate reliable browser statistics.\n\nThis way Konqueror (and other alternative browsers) will never be able to get a relevant market share, just because nobody realises that people actually use these browsers.\n\nSo this is a work-around, but no solution!"
    author: "Oliver Strutynski"
  - subject: "Re: Just change the User Agent !"
    date: 2001-12-14
    body: "No, the people who are perverting the idea of a UA string are the ones who are making their sites reject unknown browsers simply because they don't match known ones.  They should at least give them a chance, with a warning.  We're simply doing whatever we can to give Konqueror a chance at rendering these webpages.\n\nPersonally, I think the idea of a UA string is somewhat perverted in the first place, but that's a different discussion."
    author: "not me"
  - subject: "Mine Works"
    date: 2001-12-12
    body: "The onlne banking at my bank (Wells Fargo) works very well with Konqueror.\nThe only problem I've had at all in Konqueror complaining that their SSL\ncert being invalid (when it clearly is fine)."
    author: "Russ Steffen"
  - subject: "Re: Mine Works"
    date: 2001-12-14
    body: "If you still get that message with KDE3 CVS/Beta, please file a bug report and I'll look into it asap.  That code had to change thanks to braindead server designs and it hasn't gone through much serious testing.  Anyways, better to be warned on a valid site, than not warned on an invalid site!"
    author: "George Staikos"
  - subject: "Re-Banks website and Konqueror"
    date: 2001-12-12
    body: "Yes my bank web site works with Konqueror 2.2.1\nThe URL is http://www.ibc.com\nThanks to all KDE and its affiliates for a great job."
    author: "Ted Olaye"
  - subject: "Mr."
    date: 2001-12-12
    body: "I do online banking all the time with Konqueror at my Royal Bank.\nWorks beautiful!"
    author: "John Rockefeller"
  - subject: "Wells Fargo"
    date: 2001-12-12
    body: "Woo!  Wells Fargo works, no problem!\n\nI knew I loved that bank for some reason!\n\n\n-Matt"
    author: "hifidel"
  - subject: "this is another one!"
    date: 2001-12-12
    body: "This is not a bank page but a page konq fuck up quite bad. I use kde2.2.1 I think. Might look better in newer but anyway ... make it work plz =)http://www.idg.se/idgse/default.asp"
    author: "Andreas"
  - subject: "Re: this is another one!"
    date: 2001-12-13
    body: "Yup!! Seems to me a big CSS mess... never seen a worse rendered page under konq 2.2.2. :))\nBut I confide in KDE 3.0, of course!"
    author: "Andrea Cascio"
  - subject: "Re: this is another one!"
    date: 2001-12-13
    body: "It's *much* better in KDE3, although there is still a problem: the bar that should be at the bottom is actually on the side."
    author: "ac"
  - subject: "Deutsche Bank 24 works"
    date: 2001-12-12
    body: "The Deutsche Bank 24 has always worked here, it uses a page with no Java, hardly any Javascript, very nice and clean.\n\nhttps://meine.db24.de\n\nThey do seem to have some problem with certificates though at the moment (Konqueror complains, Mozilla complains, Netscape4 and Opera do NOT complain)"
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: Deutsche Bank 24 works"
    date: 2001-12-14
    body: "Interesting.  I get no warning there with 2.2.1 or HEAD.  If you see this problem with KDE _other_than_ 2.2.2, please file a bug report and I'll look at it asap."
    author: "George Staikos"
  - subject: "Re: Deutsche Bank 24 works"
    date: 2002-12-12
    body: "Hello, \nI have been trying to access the Deutsche Bank web page for the last couple of weeks, but couldn't get through. I wonder if they have a new web site address: if anybody knows it, please let me know by sending a message to: daniyar@ifes.elcat.kg\n\nThank you \n"
    author: "Daniyar"
  - subject: "Spanish bank (www.lacaixa.com)"
    date: 2001-12-12
    body: "In the start page ( www.lineaabierta.com ) appears this :\n\n\n\"Netscape Navigator 2.00 o Microsoft Explorer 3.0.\n Sense interpretaci\u00f3 d instruccions \"JavaScripts\" no podr\u00e0 entrar a la \"L\u00ednia Oberta WEB\" \"\n\n\nIt means that , of course, you need netscape or explorer and activate javascript, but if you got active ssl3 it logins but i don't know if it works correctly.The page is a javascript page so i think that it doesn't works correctly maybe kde3.\n\n\nSuse 7.1\nkde 2.2.1"
    author: "rampxxxx"
  - subject: "Re: Spanish bank (www.lacaixa.com)"
    date: 2002-06-12
    body: "CONEXSION"
    author: "JORGE DE JUAN BALLESTER"
  - subject: "Re: Spanish bank (www.lacaixa.com)"
    date: 2008-07-28
    body: "There seems to be a big Lotto Scam useing your esteemed bank, or officials, in your bank as part of this.\n\nPlease let me know if you are aware of this fraudelent activity.\n\nMany thanks\n"
    author: "Graham"
  - subject: "Poalim bank in Israel - clearly Konqueror problem"
    date: 2001-12-12
    body: "My online bank www.bankpoalim.co.il works with Konqueror, but only partially - one frame is shown, others are not, the message is \"Invalid URL\", URL is \"javascript:\"smth. Works with Mozilla & Netscape with no problem, both under Linux & Windows ( excluding some font misconfiguration under Linux, I believe it to be my problem )."
    author: "Alex"
  - subject: "Credit Lyonnais (a French Bank) works"
    date: 2001-12-12
    body: "I use konqueror at http://abclnet.creditlyonnais.fr it works fine ! And even better than Mozilla ! (I can not login with Mozilla 0.9.6 due to javascript errors)"
    author: "Pierre SOUCHAY"
  - subject: "Re: Credit Lyonnais (a French Bank) works"
    date: 2002-02-09
    body: " Comment faire pour consulter mon compte par internet. Merci de me r\u00e9pondre tr\u00e8s rapidement, avant la fermeture des magasins.\n"
    author: "jauze"
  - subject: "Re: Credit Lyonnais (a French Bank) works"
    date: 2002-07-29
    body: "je veut des renseignements sur votre banc . \nmerci ."
    author: "younnes"
  - subject: "Re: Credit Lyonnais (a French Bank) works"
    date: 2002-07-30
    body: "Bonjours\n\n       Je sius Mr/MEDJDOUB Kamel titulaire de compte n\u00b00000419174R cl\u00e9 66 agence n\u00b002800 banque n\u00b030002 credit lyonnais 25 rue SAINT FERREOL MARSEILLE 13000\n\n       Je voudrais s'avoire si c'est possible de faire un transfaire d'argents de mon propre compte \u00e0 un autre compte en Fran\u00e7e par internet.\nen cas de possibilit\u00e9 qu'elle est la proc\u00e9dure la plus rapide en sachant que j'ai un code d'acc\u00e9s perssonel.\n\n        recevez,Madame,Monsieur,mes sinc\u00e8res salutations."
    author: "MEDJDOUB Kamel"
  - subject: "Re: Credit Lyonnais (a French Bank) works"
    date: 2003-07-28
    body: "I would like to know if it is possible to purchase a stuffed lion like the ones Lance Armstrong is awarded on the podium?\n\nThanks,\n\nDeb"
    author: "Deb"
  - subject: "Re: Credit Lyonnais (a French Bank) works"
    date: 2005-07-28
    body: "Did you ever find out how to purchase the stuffed lion?\nThanks"
    author: "E"
  - subject: "Re: Credit Lyonnais (a French Bank) works"
    date: 2006-07-11
    body: "Yes, you can.  Try ebay, search on credit lyonnais lion.  Bring a big wallet!"
    author: "john allen"
  - subject: "Re: Credit Lyonnais (a French Bank) works"
    date: 2003-06-04
    body: "I AM A SWEDISH THAT LIVES IN FRANCE AND HAVE CREDIT LMYONNAIS AS MY BANK. BUT I COULD NOT FIND THE INTERNETBANK ON THE ADRESS ABCLNET?\nTHANKFUL FOR REPLY    LARS EJE LARSSON"
    author: "LARS EJE LARSSON"
  - subject: "Re: Credit Lyonnais (a French Bank) works"
    date: 2005-06-21
    body: "veuillez recevoir ma confirmation de ma demande de virement de 7200e attends votre coup de fil a mon numero de tel 021415513 "
    author: "guerroumi"
  - subject: "commbank oz no"
    date: 2001-12-12
    body: "www.commbank.com.au/netbank\nI think it has the javascript/java problem mentioned in someones earlier post.\nit works with mozilla"
    author: "Tim"
  - subject: "Konqueror works with Washington Mutual Bank in CA"
    date: 2001-12-12
    body: "Just tried Konqueror (KDE 2.2.2) and it works just fine in \ntheir secure mode.  The fonts and panels are slightly off\nbut ok.  If we could just get email to import those pesky\naddresses without problems...\n\nKonqueror is more stable than Mozilla 0.96, Netscape 6.2\nand Opera 6.  Great work...\n\nEd"
    author: "Edward Rataj"
  - subject: "Master"
    date: 2001-12-13
    body: "ASB in New Zealand works well.  NZ was one of the first with online banking ( not a boast, nz is often a test case, we had nation wide eftpos (debit card system) before most countries.  Server side, ASB and subsiduary BankDirect run on JADE, a Java-like(but db inclusive) object pascal based platform  like system.  JADE runs on AIX and (somewhat sadly) NT and is being ported to Linux as we speak. (not a plug, I don't work for jade ;)"
    author: "Patrick Gade"
  - subject: "Master"
    date: 2001-12-13
    body: "ASB in New Zealand works well.  NZ was one of the first with online banking ( not a boast, nz is often a test case, we had nation wide eftpos (debit card system) before most countries).  Server side, ASB and subsiduary BankDirect run on JADE, a Java-like(but db inclusive) object pascal based platform  like system.  JADE runs on AIX and (somewhat sadly) NT and is being ported to Linux as we speak. (not a plug, I don't work for jade ;)\n\nWebsite is www.asbbank.co.nz"
    author: "Patrick Gade"
  - subject: "BankOne (US) certificate problem"
    date: 2001-12-14
    body: "Once you get past the invalid certificate dialog all functions seem to work fine with KDE 2.2.1\n\nwww.bankone.com"
    author: "mem16421"
  - subject: "Re: BankOne (US) certificate problem"
    date: 2001-12-14
    body: "This works fine for me with 2.2.1 and HEAD.  Do you mean 2.2.2?  Please file a bug report if it's for sure 2.2.1 or HEAD and I\"ll look into it asap.  Don't bother filing a report against 2.2.2 though :)"
    author: "George Staikos"
  - subject: "I've also problem with my online bank"
    date: 2001-12-15
    body: "Hello,\nI'm glad someone has my very own problem.\nI only use Konqueror for browsing, and I can enjoy most (not all) web sites, but I have a lot of problems with my bank: www.fineco.it.\nWith mozilla the web site runs fine.\nThe problems I have are:\n- the site uses (at least) 2 frames, refreshing each one independently one from the other. konqueror opens 2 windows, one for a frame, one for the other, and I have to click in both to make things work!\n- I continuosly get the error \"the certificate is valid but does not appear to have been assigned to this server\" asking confirmation before proceeding, so making the browsing of the site a real pain\n\nDoes anyone experiments my problems? I'm sorry I can't give you an url to test because the problems are in the private pages.\nDoes anyone have some work around for my problems?\n\nThanks and keep up the great work, konqueror and all KDE really rocks.\nAlex\n\nP.S.: I run KDE 2.2.2 on a red hat 7.2 box"
    author: "Alex Dusty"
  - subject: "www.merita.fi"
    date: 2001-12-16
    body: "Have always worked with konqueror, However the newest 2.2.2 update broke something in cookie handling (reported it some time ago), so now you have to clean out the old cookies for that site before you can log in again.\nThere is also still a problem with using a proxy for the https-sessions."
    author: "Dag Nygren"
  - subject: "www.merita.fi"
    date: 2001-12-16
    body: "Have always worked with konqueror, However the newest 2.2.2 update broke something in cookie handling (reported it some time ago), so now you have to clean out the old cookies for that site before you can log in again.\nThere is also still a problem with using a proxy for the https-sessions."
    author: "Dag Nygren"
  - subject: "Re: www.merita.fi"
    date: 2001-12-17
    body: "I confirm this, older 2.2.1 worked fine, 2.2.2. doesn't. Mozilla does, but I don't \nwan't to load it's widget set into memory, when I already have KDE and Qt loaded."
    author: "Juha Tuomala"
  - subject: "Czech eBanka doesn't work"
    date: 2001-12-17
    body: "Czech first online bank eBanka doesn't work with Konqueror. After my notification about compatibility between eBanka online system and Opera, Konqueror or Mozilla they added a browser check script and all what I can seenow is BadBrowser.html page. They support IE, Netscape and particulary some versions of Mozilla, because of the Mozilla is build on top of the Netscape ( :-( their words). Again, they answered me, that no support is planned for other browser then IE and Netscape. I don't understand this, they are 4 years on the electronic market and was the more progressive bank in the middle Europe in this area."
    author: "Jiri Kaderavek"
  - subject: "Firstbank of Colorado"
    date: 2003-05-11
    body: "This banks site (www.efirstbank.com) works with Konqueror.\nI got Pay Pal site to work tto, I had to enable cookies.\n\nCiao\nNitman"
    author: "Nitman"
---
<a href="mailto:olistrut@gmx.net">Oliver Strutynski</a> has a mission.
Frustrated by the fact that some online banks discriminate against
<a href="http://www.konqueror.org/">Konqueror</a>, he has
<a href="http://lists.kde.org/?l=kde-promo&m=100782739314312&w=2">announced</a>
a <a href="http://home.in.tum.de/strutyns/banking">website</a> for promoting
Konqueror to banks.  According to the site, &quot;<em>It is the purpose of
this page to collect a list of online banking sites that work with recent
versions of Konqueror or to present work-arounds for using online banking
sites that fail to load correctly on first try</em>&quot;.
If you do engage in advocacy with your bank, please keep in mind the
<a href="http://www.linuxgazette.com/issue14/advocate.html">Linux Advocacy
Guidelines</a> (non-Linux'ers, filter the page through <code>sed -e s'@Linux@MYOS@g'</code>, and it will address you too).  Most countries heavily regulate their banks and thus banks have additional legal obligations and
liabilities in terms of security.  This means that providing support for a particular browser <em>may</em> not necessarily be as simple as it would be for a different category of website; however, if enough customers request it . . . .
<!--break-->
