---
title: "Kernel Cousin KDE Issue #5 is Out"
date:    2001-04-09
authors:
  - "numanee"
slug:    kernel-cousin-kde-issue-5-out
comments:
  - subject: "Wrong name!"
    date: 2001-04-09
    body: "Kant should have been renamed Kan :-)\n\n-- Jaldhar"
    author: "Jaldhar H. Vyas"
  - subject: "Re: Wrong name!"
    date: 2001-04-09
    body: "Ha, that's funny!\n\nI think it sounds kind of funny to call a computer program Kate.  What if it hangs and someone hears you say \"Kill Kate\" as you type the kill command?"
    author: "not me"
  - subject: "Re: Wrong name!"
    date: 2001-04-09
    body: "Nonsense!\nI say\" I love Kate\"  and everybody thinks thats kute."
    author: "reihal"
  - subject: "Re: Wrong name!"
    date: 2003-03-20
    body: "Others might think its qt"
    author: "Anonymous Coward"
  - subject: "Re: Wrong name!"
    date: 2001-04-11
    body: "Hahaha! It's more funny to call it Kenny.\nThem you type \"kill kenny\" and KDE shows a message:\n<b>Oh, my god, they kill Kenny! You bastards!!!</b>"
    author: "Reader"
  - subject: "Re: Wrong name!"
    date: 2001-04-09
    body: "I don't get it.  But I would go for \"Kane\" as in Citizen Kane.  Or \"Khan\" as in Wrath of Khan.  But Kate is pretty good already."
    author: "ac"
  - subject: "Re: Wrong name!"
    date: 2001-04-09
    body: "I don't get it.  But I would go for \"Kane\" as in Citizen Kane.  Or \"Khan\" as in Wrath of Khan.  But Kate is pretty good already."
    author: "ac"
  - subject: "Re: Wrong name!"
    date: 2001-04-17
    body: "No, you can't name it khan. I have been khan in #kde since years ago, and I don't want to be confusing :-)"
    author: "Roberto Alsina"
  - subject: "Re: Kernel Cousin KDE Issue #5 is Out"
    date: 2001-04-09
    body: "Hey, I'm no warrion, I'm a peaceful guy!\n\nIf anything, I a m slightly ashamed about posting in COLA, as everyone knows it's just a watering hole for non-coding flamers.\n\nAt least now I'm a coding flamer again ;-)"
    author: "Roberto Alsina"
---
<a href="mailto:aseigo@mountlinux.com">Aaron J. Seigo</a> has done it again.  This week in <a href="http://kt.zork.net/kde/latest.html">KC KDE</a>, you can read about KDE database connectivity, Kaboodle the light-weight embeddable media player, KPrinter progress, Kant's renaming to <a href="http://devel-home.kde.org/~kate/">Kate</a>, an <a href="http://lists.kde.org/?l=kde-devel&m=98599282811328&w=2">updated KDE/Qt port</a> of the featureful and fast programmer editor <a href="http://fte.sourceforge.net/">FTE</a> (by KDE's very own <a href="http://www.kde.org/gallery/index.html#ralsina">COLA warrior</a>), <a href="http://kmail.kde.org/">KMail</a> improvements, and much more.  This week's edition can be found <a href="http://kt.zork.net/kde/kde20010406_5.html">here</a>.

<!--break-->
