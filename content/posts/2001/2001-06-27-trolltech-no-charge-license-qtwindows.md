---
title: "Trolltech: No-Charge License for Qt/Windows"
date:    2001-06-27
authors:
  - "Dre"
slug:    trolltech-no-charge-license-qtwindows
comments:
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-27
    body: ">Excellent news, but when will we see kdelibs and Konqueror for Windows? \n\nNever I hope..."
    author: "Anonymous Coward"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-27
    body: "> Never I hope...\n\nYou underestimate the power of buzzwords like \"multi-platform\". We should not port all of KDE to Windows, that would be stupid. Put porting one or two of the killer applications could create a lot of positive marketing hype.\n\nUser: \"Wow, this is a cool browser, I like it\"\nUs: \"Like it? We've got a whole desktop just like it! For free!\"\n\nFurthermore it would allow more web developers to test their sites with Konqueror/khtml."
    author: "Rob Kaper"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-28
    body: "That's a briliant stradegy! :) Go for it!"
    author: "dc"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-30
    body: "User: \"Wow, this is a cool browser, I like it\"\n\nHehe, Sure you can have a nice browser on Windows by 'porting' konqueror. But the strenghts of konqueror lies in the (o.a.) application-embedding (aka kparts). I think you would not do it justice, on Windows."
    author: "Thomas Zander"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-27
    body: "One of the more important implications is that this move could bring many Windows developers to KDE/Qt/Linux via Qt/Windows.  Let's hope that a lot of Windows developers decide to jump on this deal."
    author: "KDE User"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-27
    body: "This is purely wishful thinking.  I'd love to believe it were true."
    author: "dingodonkey"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-28
    body: "> This is purely wishful thinking. I'd love to believe it were true.\n\nI don't think so.  Win32 is has a terrible API; the only way people get anything done is by using a visual editor like Visual C++ or adopting another API like Borland C++ Builder which uses VCL.  We already have Kylix from Borland which is practically C++ Builder; it uses QT.  It's not a far walk to Kylix/Win or C++ Builder/QT."
    author: "Andrew"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-07-03
    body: "> Win32 is has a terrible API; the only way people get anything done is by using a visual editor like Visual C++ or adopting another API\n\nI disagree.  All of us Cygwin and Cygwin/XFree86 developers use emacs or vim and we access the Win32 API directly.  Heck, I only started seriously using Cygwin and the Win32 API nine months ago; three months ago I finished rewriting Cygwin/XFree86, so Win32 can't be that hard/bad :)\n\nHarold"
    author: "Harold Hunt"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-27
    body: "I see Windows 2000 (and Windows NT) as no more than a UNIX kernel derivate with a \"Windows look\" (the GUI). I think Windows NT/2000 are good operating systems. However, there are dark clouds ahead.... The coming Windows XP - with registrations by Web, seems to me as a prerequisite to George Orwells future nightmare \"1984\". Big Brother Bill Gates is watching you!\n\nIf Qt/Windows can contribute to development of applications for Windows NT/2000, then the hegemony of Microsoft can be broken. And if MS should be very nasty guys (making the applications incompatible with future Windows incarnations, as they did with Watcom C/C++), people do have an alternative in KDE/Qt/Linux."
    author: "Magne Aga"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-27
    body: "Okay, I'm excited. Muhammed and mountains spring to mind :-)\n\nJust wondering if there would be any problems linking GPLed code like KDE against it though. Shouldn't think so as it's no different to linking against an LGPL library where you don't have to distribute the library source code. IANAL though, so I'll leave this discussion to those who know (and I'm sure this is gonna get discussed A LOT!!) Be nice to have an \"official\" view on this asap to prevent too much speculation.\n\nFor those who are thinking \"yeah, but the underlying OS wouldn't be Open Source\", just think of it as a transitionary step. Once they're all running KDE under windows, it'll be really easy to \"port\" the users over to Linux etc.\n\nThings are starting to get more and more fun :-)"
    author: "Sean"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-27
    body: "Yes, that is true. When you want to win the desktop battle you have to go into the enemies country and occupy it."
    author: "Thomas"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-27
    body: "you said:\n>>Just wondering if there would be any problems\n>>linking GPLed code like KDE against it though.\n>>Shouldn't think so as it's no different to\n>>linking against an LGPL library where you\n>>don't have to distribute the library source\n>>code.\nWell IANAL either of course but I\n\n1) don't think there would be probs with kdelibs, provided it's all LGPL\n\n2) konqueror is GPL and that *might* be a problem .. errr.. depending on where you stood in that whole dynamic-linking-against-non-gpl-qt flamefest.  (my personal opinion on that hot potato being that the GPL is not remotely explicit enough about it and that it will take a court battle to figure if it would even be enforcable.  I wish people would just use a more explicit license.)\n\nThe distinction you do fail to make above is not being *required* to distribute library source vs. not being *able* to (distribute and make various mods to it).\n\nOf course none of this is to stop the konqueror guys from putting some exception clause in their license \"just in case\" or from saying \"whaddaya mean I can't do that? watch *this*\"\n\nIn any event, nice move from the trolls (at troll tech :)"
    author: "blandry"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-27
    body: ">     1) don't think there would be probs with kdelibs, provided it's all LGPL\n \nIt's not all GPL, but what isn't is X, BSD or Artistic licensed.  In any case\nnone of kdelibs should have a problem with the Qt license.\n \n>     2) konqueror is GPL and that *might* be a problem .. errr.. depending on\n>where you stood in that whole dynamic-linking-against-non-gpl-qt flamefest.\n \nNot as big a problem as you might think at first.  If Qt and kdelibs are distributed on\nCD, then the Konqueror part would have to be downloaded separately (GPL only\nprevents you from distributing GPL apps with non-GPL apps, it does not prevent\npeople from using them together).  So if someone made a build of Konqueror\nwith the CygWin glibc libraries, well it wouldn't be the smallest download but\ndefinitely possible enough that many Windows users would try it."
    author: "Dre"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-27
    body: "> So if someone made a build of Konqueror with\n> the CygWin glibc libraries, well it wouldn't\n> be the smallest download but definitely\n> possible enough that many Windows users would\n> try it.\n\nActually it would be the smallest download. Konqueror is..\n\ncap@kira:~$ ls -sh /usr/local/kde2/bin/konqueror\n\n16k /usr/local/kde2/bin/konqueror*\n\nOf course you need to add libkonq to that..\n\n736k /usr/local/kde2/lib/konqueror.so*\n660k /usr/local/kde2/lib/libkonq.so.3.0.0*\n\nWell, that's still a small download considering I did not even strip and gzip anything here.\n\nEverything else what Konqueror uses are kdelibs (khtml, kio, kparts) and some kioslaves, nspluginviwer and kcookiejar, but all in all I'm sure it can be crammed in a pretty small download."
    author: "Rob Kaper"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-28
    body: "> 16k /usr/local/kde2/bin/konqueror*\n> 736k /usr/local/kde2/lib/konqueror.so*\n> 660k /usr/local/kde2/lib/libkonq.so.3.0.0*\n\nUmm.. ldd'ing my konqueror binary gives me a lot more than two libraries...\n\nlibkparts libkfile libksycoca libkio libkdeui libkdesu libkdecore libkdefakes\n\nNow for a Win32 user to want it, they'd need the graphics libraries (libjpeg, libpng, etc.) unless QT provides wrappers for these and other graphics formats.\n\nIt'll be small, sure, but unless I'm missing something, it won't be that small."
    author: "Andrew"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-28
    body: "What someone _really_ needs to do is recompile Konqueror/Embedded.  \n\nKonq/Embedded is a lot smaller than the full Konq, I think.  We could finally see a head-to-head competition between IE and KHTML!"
    author: "not me"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-28
    body: "> Umm.. ldd'ing my konqueror binary gives me a\n> lot more than two libraries...\n\nYah, I made a mistake in thinking the ported kdelibs would already be on the computer. If so, Konqueror would be small - if not, that should increase the size a bit... ;-)"
    author: "Rob Kaper"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-27
    body: "You are overlooking a number of things.\n \nThe QT/Windows non-free license does not allow commercial usage of software using it. You can't port KWord to Windows and let your Windows colleagues at work use it. Period. According to QT at least.\n \nKDE is AFAIK mostly GPL, so it allows commercial usage.\n \nThe fact that FAQ says you can license your code under BSD or Artistic licences does not mean you can simply avoid other restrictions for set by Trolls for QT/non-free.\n \nYou can develop QT/Windows non-free apps only with Microsoft Visual Studio, which brings in it's own set of licensing details.\n \nSo my idea of developing apps at linux and simply cross compiling it for win32 still isn't possible.\n \nSo why not port GPL QT to Win32? I know it probably isn't the easiest thing to do, but we have gtk-win32 and great number of other free windows code lying arround. We also have very capable cross compilers and windows implementations (WINE) to allow us to test our win32 QT code on Linux. Did anybody do the initial research on making the port? Please contact me if you did. I would like to port it, but I'm still not done evaluating various other options. Like polishing gtk-win32, or maybe even trying to make a C++ portable LGPL widget set.\n\nWhat is Trolltech position on this? \nSince they licensed QT under the GPL, they obviously have nothing against it, but probably think that there is no interest among Free software developers to port QT to Windows.\n\nBut I think that Free software developers have a lot to gain and nothing too loose if they port apps to Windows. That way, when most KOffice apps are polished, and when we incrementaly remove the need for non-free Windows apps, we can easily switch to totally Free system."
    author: "Vedran Rodic"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-27
    body: "> You can't port KWord to Windows and let your \n> Windows colleagues at work use it\n\nYes, you can. But you mustn't allow the users to <B>write</B> scripts in KWord (is that possible, i don't know kword). I think disabling scripting support for kword in these cases would be enough.\n\nI refer to Right No. 3 of the license\n\n\"You may use the original versions of the Software to compile, link and run application programs legally developed by you or by others. However, if an application program gives you access to functionality of the Software for development of application programs, reusable components or other software components (e.g. an application that is a scripting wrapper), usage of the application program is considered to be usage of the Software and is thus bound by this license. \""
    author: "Markus Pietrek"
  - subject: "It's the same non-free crap all over again"
    date: 2001-06-27
    body: "No. It only states that it's viral in it's own non-free way. \n\nSo, GPL-ed KDevelop (and KWord!) suddenly becomes licensed under a more restrictive license, which is incompatibile with GPL.\n\nIn No 4. you would believe that this is similiar with GPL, except that it can't be used for producing apps for \"commercial setting\", IE you can't make money from it.\n\nThis contradicts with all real values of the GPL license, since a lot of Free software developers are making money by making Free software. Why wouldn't they? They have to eat. I have to eat too, by making Free software that both Windows and Linux users can use. It's my idea that Linux users should be able to use it. What's wrong with that? I happen to like QT and KDE for purely techincal reasons and I would really hate to be forced to use another toolkit. Free Software is good. I hope we know that. So why Trolltech doesn't? Because their entire business is based on it! So what?? So create a different business, there are plenty of choices now. \n\nThis Free vs. Non-free software is getting really boring and irritating in the same time. Lets just create Free software and be happy with it!\n\nC'mon Trolls. Be friends with everybody:)"
    author: "Vedran Rodic"
  - subject: "Re: It's the same non-free crap all over again"
    date: 2001-06-27
    body: "> In No 4. you would believe that this is \n> similiar with GPL, except that it can't be \n> used for producing apps for \"commercial \n> setting\", IE you can't make money from it.\n\nNo.4 states that \" in a non-commercial setting, to develop application programs, reusable components and other software items\". I don't count the letters the secretary is writing as software items. Therefore, the secretary in a company is allowed to use KWord, but not to write any scripts.\n\nBye"
    author: "Markus Pietrek"
  - subject: "Re: It's the same non-free crap all over again"
    date: 2001-06-27
    body: "I'm sorry. I wasn't talking about KWord only.\n\nBut the license is not GPL compatibile, and KWord is GPL licensed software. Its not BSD or Artistic. KWord would have to change it's license to become compatibile with QT/non-free. I don't want that to happen. Do you?"
    author: "Vedran"
  - subject: "Re: It's the same non-free crap all over again"
    date: 2001-06-27
    body: "It's not fine that a GPL application had to change it's license. But, all current users would not be affected by it (they are working on unix), and some window users would benefit from it, too (they are currently not using kword)\n\nYou are right in saying that's not the best we could wish us, but it is a step in the correct direction.\n\nBye,\nMarkus"
    author: "Markus Pietrek"
  - subject: "It's the same non-free crap all over again"
    date: 2004-04-08
    body: "hi"
    author: "joe"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-27
    body: "> The QT/Windows non-free license does not allow\n> commercial usage of software using it. You can't\n> port KWord to Windows and let your Windows\n> colleagues at work use it. Period. \n> According to QT at least.\n\nNonsense. The license nowhere states anything \nlike this. \n\nYou must not _develop_ programs with Qt in\ncommercial settings. All the license states\nabout _running_ programs is that you most \ncertainly are granted the right to run programs\nlegally developed by others.\n\nHow can this be missunderstood? Have you actually\nread further than the caption \"Qt Non Commercial\"?\n\nRegarding scripting: It depends on the power\nof KWord's scripting engine, but I doubt that\n'writing scripts for kword'  is the same as\n'developing applications with Qt'. So no, there\nis no need to turn the scriptability off.\n\nRegarding the GNU GPL: We clearly state on the\nwebpages and the README that the GNU GPL is not\ncompatible with the Qt Non Commercial License for\nMicrosoft Windows."
    author: "Matthias Ettrich"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-27
    body: "Okay, maybe I said the quoted text under a biased GPL view. But it is the same. \n\nUsers of software have the right to develop the software they are using. Software authors have the right to make their software stay free. GPL is one license that provides that protection. I like that about it.\n\nYour license is clear, but you still can't port KWord to QT/non-free because it's GPLed.\n\nWhy wouldn't my boss pay me to port KWord to Windows? Thankfully, QT is GPLed, so nobody can prevent other developers from porting it to windows. I just think its very redundant to do it, since QT already did it.\n\nTrolltech is contradicting themselves. They release the same toolkit in both GPL license and license that is not GPL compatibile. How would you feel like if Linus Torvalds would say: Okay, the kernel is under a dual license, I can make proprietary changes to it, but you can't, because you didn't pay me for the license. The license is 1M $. Obviously the success of Linux would never happen."
    author: "Vedran Rodic"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-27
    body: "A Question!\n\nIf I as a developer in a \"commercial setting\" make a patch that fixes a bug in QT/GPL, my patch obviously falls under the GPL. Now, GPL does not allow the code under it to be made proprietary. What happens if I somebody from trolltech includes the fix in their tree? \n\nDoes it automatically go in QT/Windows tree? If yes, it can't. And if you say that you are \"rewriting\" every fix that comes from a KDE developer separately for QT/non-free, please don't, it just sounds funny.\n\nMaybe you are paranoid and you are keeping the bugs in QT/non-free?"
    author: "Vedran Rodic"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-27
    body: "As a commercial entity, we have to take licensing and copyright issues very seriously. Send us a patch > 15 lines and you'll find out."
    author: "Matthias Ettrich"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-27
    body: "> Users of software have the right to develop the\n> software they are using. \n\nWrong. Users of Free software have this right.  Users of commercial software have whatever right the copyright holder grants them in the end user licensing agreement.\n\nFYI: commercial users of our software get the entire source code and the right to do modifications for the sake of their products.\n\n> Your license is clear, but you still can't port \n> KWord to QT/non-free because it's GPLed.\n\nYes. Where did you get the impression from that this is what we want to achieve with the Qt Non Commercial Edition? http://www.trolltech.com/products/download/freelicense/noncommercial-dl.html clearly talks about the incompatiblilty with the GPL.\n\n> Trolltech is contradicting themselves. They\n> release the same toolkit in both GPL license and\n> license that is not GPL compatibile. \n\nQt/X11 and Qt/Windows are not the same product.\n\nShow me a company of our size that contributed more GPL'd code to Linux and is still healthy in business and growing. Then release at least as much professional code under the GNU GPL as we did and stay in business. Then come back to me and critize me.\n\nNo need to answer, for me this discussion is closed."
    author: "Matthias Ettrich"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-27
    body: "I know, I know and I know and I'm sorry.\n\nI guess I just wanted to try changing your mind. I will respect your wishes of not wanting QT for Win32 to be Free (this sentence is probably not needed, since I still haven't proven that I can do that). \n\nI'm not some envious bastard wanting to ruin a solid business of decent company just because I can. I feel that QT was kind of forced into GPL and I don't like that fact. \n\nThe only thing I wanted to achieve here is being able to finally say: \n\nOkay I can use this great cross platform toolkit, I can make win32 apps on my linux box and test them with Wine, I don't have to cope with quirky licences anymore, I'm happy with GPL, and I have a complete control of my Free system. It is now a standard GUI toolkit. Everywhere. Lets move on. \n\n\n\nAnd I really want to be able to witness that a another bar has been raised (as linas, the author of gnucash has nicely put it in http://www.linas.org/theory/freetrade.html).\n\nAnyway, I found a couple of interesting articles on cross platform GUI development at http://www.t4p.com/xplat.htm\nand http://www2.linuxjournal.com/lj-issues/issue49/2723.html\n\n\nFriendly, Vedran"
    author: "Vedran"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-28
    body: "> > Your license is clear, but you still can't port\n> > KWord to QT/non-free because it's GPLed.\n>\n> Yes. Where did you get the impression from that this is what we want to\n> achieve with the Qt Non Commercial Edition?\n> http://www.trolltech.com/products/download/freelicense/noncommercial-dl.html\n> clearly talks about the incompatiblilty with the GPL.\n \nJust to clarify a few things.  Nothing prevents anyone from *porting* KWord\nto Qt Non-commercial and, under the proper circumstances, distributing this KWord in binary form.\n \nIf you look at Section 2 of the GPL, it says you are free to modify the\nGPL'd work (KWord) in any way you wish; this would include modifying KWord\n(or Konqueror) to work with Qt Non-commercial.  However, Section 2(b) goes on to say that if you then go\nahead and *distribute* the modified work, you have to include the whole\npackage under a GPL-compatible license (which Qt Non-commercial is not).\nSo the premise has to be that you distribute Qt Non-commerical (together\nwith kdelibs and any other Qt Non-commerical-compatible-licensed software) separately\nfrom KWord/Konqueror/GPL-app.\n \nSome would argue that even if you distribute it separately the Qt\nNon-commerical is still part of the \"whole\" and thus must be GPL-\ncompatible, as they were designed to \"link together\" (this argument came up in the old days before Qt was GPL'd).  However, Section 2 of the GPL also has an exception for this:\n \n    If identifiable sections of that work are not derived from the\n    Program, and can be reasonably considered independent and\n    separate works in themselves, then this License, and its terms,\n    do not apply to those sections when you distribute them as\n    separate works.\n \nIn my view Qt Non-commerical could be considered to satisfy the premise of that clause,\nso you can distribute KWord designed to link to Qt Non-commercial as long\nas the user gets the kdelibs separately (i.e., not from you).\n \nRemember the GPL was written so that people could use GPL'd programs\non proprietary Unixes (particulary proprietary libc's).  However, this\nexception was not meant to permit the proprietary Unix vendors to\ndistribute these GPL'd programs as well; hence the requirement to\ndistribute the GPL and non-compatible works \"as separate works\".\n\nThus, if someone distributes Qt Non-commercial and kdelibs on CD to users (or makes it available for download), and somebody else compiles and distributes KWord/Konqueror/etc. separately to work on this system, then I don't see a GPL violation (though of course a persuasive argument could change my mind)."
    author: "Dre"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-28
    body: "God I hate not having an html option.  So I'm just going to put asterisks around everything you said.\n\n***Section 2 of the GPL also has an exception for this:\n\nIf identifiable sections of that work are not derived from the Program, and can be reasonably considered independent and separate works in themselves, then this License, and its terms, do not apply to those sections when you distribute them as separate works.***\n\nNo it does not.  I just checked the GPL license in the file COPYING for the linux kernel and it's not there.  It bears a startling resemblance to an LGPL passage, but s/Program/Library.  Although I'm sure you didn't mean it, this is a serious piece of misinformation and at least one other person in this topic is citing it.\n \n***Remember the GPL was written so that people could use GPL'd programs on proprietary Unixes (particulary proprietary libc's).***\n\nTrue dat! .. falling under the nebulous \"major components\" exception (the part that begins \"as a special exception, ...\"), which libc almost certainly falls under and many would argue qt/win does not.  Another place the GPL ought to be more explicit.  The \"and so on\" bit is too vague.  Stallman used it to \"attack\" (and I use that term loosely) pre-GPL qt/x11 even tho it was shipped by default in several linux distributions and could therefore be considered (by me) to be a major component.  Nobody would argue that NT's gui libraries weren't a major component.  Almost a chicken-egg problem there.  Yes, I know MS doesn't ship qt/win with their OS (wouldn't that be interesting tho :) .. damn I'm sorry I guess we've all been down this road before.\n\n***However, this exception was not meant to permit the proprietary Unix vendors to distribute these GPL'd programs as well;***\n\nTell that to (old-school) NeXT.  Alternatively, http://www.sun.com/gnome\n\nMan it's late."
    author: "blandry"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-28
    body: "> > Section 2 of the GPL also has an exception for this:\n \n> > If identifiable sections of that work are not derived from the\n> > Program, and can be reasonably considered independent and separate\n> > works in themselves, then this License, and its terms, do not\n> > apply to those sections when you distribute them as separate works.\n>\n> No it does not. I just checked the GPL license in the file COPYING\n> for the linux kernel and it's not there.\n \n[ ... ]\n \nTry http://www.gnu.org/copyleft/gpl.html.\n \n> > Remember the GPL was written so that people could use GPL'd programs on\n> > proprietary Unixes (particulary proprietary libc's).\n>\n> True dat! .. falling under the nebulous \"major components\" exception\n> (the part that begins \"as a special exception, ...\"), which libc almost\n> certainly falls under and many would argue qt/win does not. Another\n> place the GPL ought to be more explicit. The \"and so on\" bit is too\n> vague. Stallman used it to \"attack\" (and I use that term loosely) pre-GPL\n> qt/x11 even tho it was shipped by default in several linux distributions\n> and could therefore be considered (by me) to be a major component.\n \nWell that is the old argument again and you are right that it is tougher\nin this case (since you don't even have the source code to Qt Noncommercial\nto distribute and it would be less of a system component for the reasons you\nstate).  Of course I wouldn't suggest that KDE does this -- Heaven forbid! -- but it's a gray\narea so someone, perhaps an individual, could do it (assuming the app's\ndevelopers don't object).  Anyway, if you use MS Visual Studio I think\neveryone would agree the libc/libstdc++ are system components, so the only\nissue would be Qt Noncommerical itself.  And Qt Noncommerical is not so\nunlike Motif in terms of how convincingly you can say it is a system\ncomponent, and in terms of it not being not distributed by the vendor or\navailable in source code . . . and certainly there were a good number of\nGPL Motif apps."
    author: "Dre"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-29
    body: ">>>Try http://www.gnu.org/copyleft/gpl.html.\ngah!  doh.. I misread.. my deepest apologies.  I guess it really *was* late.\n\nWell, now that my credibility is shot to hell, I'll go ahead and point out the exception to the exception which sez you can't distribute these components and the GPL components 'together'.. I realize this is the point you were making in yer previous posts; and which I also think shoots down my stupid linux distros theory.  Now I just wonder how the heck sun is legally going to bundle gnome.\n\nagain, apologies for the misread."
    author: "blandry"
  - subject: "Yipes!"
    date: 2001-06-27
    body: "I've been reading posts (even past my own) and I was wondering, is it possible to change the licensing for KDE and its components?  It is ludicrous that a license designed to protect developers and encourage software freedom is causing all of us hassles.  Is there a way to change the license on KDE and its apps to a new license?\n\nJust a suggestion :-)"
    author: "Steve Hunt"
  - subject: "Re: Yipes!"
    date: 2001-06-28
    body: "> I've been reading posts (even past my own) and\n> I was wondering, is it possible to change the\n> licensing for KDE and its components? It is\n> ludicrous that a license designed to protect\n> developers and encourage software freedom is\n> causing all of us hassles.\n\nIf it's a problem, just buy a commercial QT license and then release your software as you see fit.  \n\nUnless I'm missing something it really is that simple.  You want a free toolkit, be prepared to make your software noncommercial and have the source available.  If you don't like these restrictions, buy the commercial license and do as you please.  I mean the full-blown version is only USD$2k in onesies; if you're writing commercial software this is practically nothing considering there are zero royalties or renewals."
    author: "Andrew"
  - subject: "Re: Yipes!"
    date: 2001-06-28
    body: "Mmmh. I don't think he meant building comercial\napps with free librairies. I think he was simply astonished by the mess these license questions are making... simply look at this thread ! Quite complicated in fact...\n\nHowever, I disagree with the GPLing librairies. FSF seems to fear someone steals code, and want to reny LGPL. However, I personnaly still consider that as restrictive, and not conform to the original goal, that was liberty. Even if it allows comercial people to use free librairies.\n\nFree movement is not a comercial war (even if there are more and more comercial distros), it's a philosophy and a fight against intellectual  and information monopoly. So why even wondering about commercial apps ?"
    author: "Casteyde"
  - subject: "Re: Yipes!"
    date: 2001-06-28
    body: "EXACTLY!!!  It is ludicrous to have open source code, when the same organization can't work with it, much less any other organization.  The GPL is great for some things, such as smaller independent programs.  However, I don't understand why KDE is licensed under the GPL.\n\nI'm not sure about this, but I think the reason that KDE is under the GPL is because the FSF, or some other organization yelled at the KDE people (as a whole) and influenced them to license the whole thing under the GPL.  However, for such a large and diverse project such as KDE, the GPL is not the right solution IMHO."
    author: "Steve Hunt"
  - subject: "Re: Yipes!"
    date: 2001-06-28
    body: "It seems more like KDE guys weren't aware, how fascistic license GPL really is. See for example.\n\nhttp://mail.gnome.org/archives/gnome-kde-list/1999-May/msg00024.html"
    author: "nap"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-28
    body: "<em>You can develop QT/Windows non-free apps only with Microsoft Visual Studio, which brings in it's own set of licensing details.</em>\n\n<p>Visual Studio restricts how you can license your own works built with VC++? Since when? Can you quote from it's license to clarify?"
    author: "David Johnson"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-07-03
    body: "> So why not port GPL QT to Win32?\n\nAlready (mostly) been done:\nhttp://cygwin.kde.org/\n\nGranted, it isn't a straight port to Win32, but it is a port that runs under Cygwin with Cygwin/XFree86, which are both free software.\n\nBefore anyone gets there GPL in a knot, you should know that Cygwin is distributed under a modified GPL that specifically allows software meeting the Open Source definition to be linked to libcygwin1.a without requiring that said software be licensed under the GPL.  Have a gander at http://cygwin.com/licensing.html for more information.\n\nHarold"
    author: "Harold Hunt"
  - subject: "GPL doesn't allow distribution of non-GPL?"
    date: 2001-06-28
    body: "> GPL only prevents you from distributing GPL apps\n> with non-GPL apps, it does not prevent people\n> from using them together\n\nThen how did SuSE managed to distribute Linux with YaST, which is proprietary?"
    author: "dc"
  - subject: "Re: GPL doesn't allow distribution of non-GPL?"
    date: 2001-06-28
    body: "> > GPL only prevents you from distributing GPL apps\n> > with non-GPL apps, it does not prevent people\n> > from using them together\n>\n> Then how did SuSE managed to distribute Linux with YaST, which is proprietary? \n\nYaST does not link with the kernel; the GPL is viral only in respect of\ncode which is linked with/derived from the GPL code (roughly, read the GPL for the glorious details)."
    author: "Dre"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-27
    body: ">In any event, nice move from the trolls (at troll tech :)\n\nNo kidding!  They've managed to make everyone happy - people who want to write GPL QT apps for personal use now get automatic Windows compatibility, which is a big win; plus they still get to sell QT to the only people who really pay for it - that is, corporations.\n\nAs was pointed out in an earlier thread, much of Trolltech's revenue comes from QT licenses for in-house specialized corporate apps.  Therefore if QT/Win was GPL, corporations would simply GPL their in-house apps and avoid paying Trolltech, and the open-source community wouldn't even benefit because the in-house GPL apps would never make it outside of the companies.  \n\nNow they still make money here because corporations still have to pay even for in-house software, plus they gain more experienced QT developers because of QT's increased popularity (now it's the best free cross-platform widget set) and widespread availability on multiple platforms.  Sounds like a QT/Win win win situation all around :-)\n\nThe only problem I see is now that a binary of QT/Win is freely downloadable, some unscrupulous corporations might just use it despite the license.  It wouldn't be very likely that a corporation doing this would be caught."
    author: "not me"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-27
    body: "> The only problem I see is now that a binary\n> of QT/Win is freely downloadable, some\n> unscrupulous corporations might just use it\n> despite the license. It wouldn't be very\n> likely that a corporation doing this would\n> be caught.\n\nYeah, I have a feeling Trolltech might lose some money with this move.  However, they will probably gain more than they lose.\n\nLots of Windows programs these days are non-commercial.  I've even seen some GPL Windows apps.  It's clear that some Windows developers do agree with free-software, the trouble is they code with the ultra-unportable MFC.  Now all those developers can use Qt instead (and after they first use it they'll never go back).  In other words, Qt usage will go up.  More popularity for Trolltech in the Windows world.\n\nThe only part I'm not too clear on is the GPL aspect, and I should find out soon because I have a GPL Qt/X11 app I'm about to release and now I can make a Windows version too :)  Of course, it's _my_ app so I could change the license for the Windows port if necessary.\n\nI hope they do this for the upcoming Qt/Mac too. Get the world using Qt, get them coding cross-platform from the start.\n\n-Justin"
    author: "Justin"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-27
    body: "Just make it LGPL instead. LGPL's the more sensible license choice, anyway."
    author: "Jon"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-28
    body: "Exactly the same as piracy. They already had the problem with their old licenses...\n\nIn fact, I think companies want to pay for support / development of good stuffs. Even if it's available for download. Piracy is only a matter of people, not of companies, and people take less their company benefits in account than their own wallet... (no VAT, taken from benefits, etc.).\n\nI already think this move is great both for Qt and free software. Windows developers can now build Qt apps easily for fun, and these apps will go naturally to KDE / Linux.\n\nThe other way is simply unbeleivable : why use Konqueror on Windows, where IE is there and (still) by far better ?\n\nWhen you open a door, between two rooms, people of the more crowded one will ever go to the less one... So it's all benefits for free software, and it's all benefits for Qt which becomes a de facto standard."
    author: "Casteyde"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-27
    body: ">Once they're all running KDE under windows, it'll be really easy to \"port\" the users over to Linux etc.\n\nReminds me of something else:\n\"Once people start writing 16-bit Windows apps for OS/2, they'll quickly migrate to OS/2 natively.\""
    author: "kev"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-27
    body: "Why always this relation to OS/2? Linux is not like OS/2!\n\nIn the same way developers are interested in making win32 apps run on linux, they should be even more interested in making linux apps run in windows.\n\nIf I make a commercial game with the engine GPLed and data not, why would I want to restrict myself to Linux only? I would want to make the game for Linux, Win32, Playstation2, Mac OS X, BeOS, whatever. I can do that with cross platform game toolkits like SDL. We don't have something like that for GUI apps. I think we need it. If it turns out that it's not QT and KDE, I will be sad. \n\nThere are other options. ParaGUI that runs on top of SDL, gtk-win32 (not quite there, but fixable), FLTK, and more on http://www.geocities.com/SiliconValley/Vista/7184/guitool.html"
    author: "Vedran Rodic"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-27
    body: "I think that the point was that the OS/2 emulation was so good that people continued to write for windows. Therefore there was a continuing darth of application native to OS/2 and no one ever made the jump...."
    author: "john"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-27
    body: "This changes nothing. This is a very restrictive licence. They should rather change their prices Qt/Linux-155$, Qt/Windows-2845$. It would be great !"
    author: "Ups"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-27
    body: "You must be a very bored person. Go back to where you crawled from.\n\n\nNote: This person is too lazy to give an email adress and posts nothing but negative one sentence articles. Plus you are lying."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-27
    body: "Actually, if you read the faq, trolltech specifically says that the NonCommercial license they are using is incompatible with the GPL, so anything in kde that has gpl code in it cannot use this... which is sad.\n\nIn the end, the only way to wrest control of the desktop from Microsoft is to produce GPL applications for their desktop.  It's easy for Windows users to convert to GPL applications slowly over time.  Once all of their critical applications have been replaced with GPL versions, swapping the operating system out from underneath them will be painless.\n\nI love KDE, but if anyone is going to break Microsoft's monopoly, I think it's going to be Sun's OpenOffice."
    author: "Anonymous Coward"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-27
    body: ">I think it's going to be Sun's OpenOffice.\nI hope not, at least not going at it's current state and path. It's a powerful, very mature suite (iirc 6 years in the running), but its very, very bloated. On my machine, opening MS-Word takes about 5 seconds, where as starting the latest stable build (i.e. StarOffice 5.1a-something) takes about 45 seconds! Most of the bloat is due to the fact that StarOffice decided it would be fun to make their own widget set, printing libs, font loader, staticly built OpenGL port, etc.\n\nNot to be negative, but I don't think it has much of a chance unless much the guys at Sun manage to wean OpenOffice off it's little statically built cornucopia and onto something more likely to already be loaded at login/boot-up time (even if that be GTK+/GNOME)"
    author: "Carbon"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-28
    body: "> Not to be negative, but I don't think it has\n> much of a chance unless much the guys at Sun\n> manage to wean OpenOffice off it's little \n> statically built cornucopia\n\nI'm not suggesting that OpenOffice is the better\ntechnical solution.  Its their evolution strategy\nthat is superior, and actually stands a chance\nin corporate America of taking desktop share\naway from MS.\n\nKDE's proposition: switch your OS and all of\nyour apps simultaneously, deal with all the\ntraining and support issues for the whole thing\nat the same time.\n\nOpenOffice's proposition: slowly replace one\napplication at a time, until all of your\napplications are non-MS, then painlessly swap\noperating systems later.\n\n\nThe only way that KDE (or GNOME) is going to\ntake the desktop market in America is if: \n  - Europe and/or Asia standardize on it\n  - that forces Microsoft to develop compatible\n    file formats so that international firms \n    can communicate.\n  - once the office monopoly is broken, American\n    firms move to open-source to decrease costs\n\nThat might actually happen if European gov'ts\ndecide to standardize on open-source, which a\nlot of them seem to be seriously considering.\n\nBut Sun's strategy is much more threatening to\nMicrosoft."
    author: "Anonymous Coward"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-28
    body: "<\"OpenOffice's proposition: slowly replace one application at a time, until all of your applications are non-MS, then painlessly swap operating systems later.\">\n\nThis is exactly what this QT release might help KDE do.  You could run Konqueror or KWord under Windows, then switch the OS and DE to Linux/KDE later.  If only all of KDE was LGPL!  For a project as large as KDE, re-licensing is really unfeasable."
    author: "not me"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-07-01
    body: ">The only way that KDE (or GNOME) is going to take the desktop market in America is...\n\nI believe that you may be missing the point of Open Source. KDE is not a business or a corporation, and the whole concept of market share isn't applicable. Sure, KDE developers love it when KDE is used by users and by other developers, but unlike a business, KDE has a major advantage : it's progress does not depend that it be used by a certain amount of people. \n\nKDE's continued existence depends totally on whether or not the developers continue to work on it, and this will continue even if the developers themselves are the majority of the people using it! The primary reason that open-source software will (IMHO) almost totally replace commercial software in non-specialized fields is because the developers of Open Source have as much time as they feel like to work on it, since marketing is (rightly so) almost non-existant. \n\nThis is the way capitalism is designed to work, that is, a product will make it or not depending almost entirely on it's quality. Microsoft and (to a lesser degree) Sun are more concerned over whether or not their customers _think_ the product is of high quality, then whether or not it actually is.\n\nFor instance, for all of M$'s raving over how Win2k no longer has DOS, this \"feature\" is in fact a detriment! With a minor amount of registry editing, you can discover that in fact the OS still depends on and can boot into a version of DOS, but now it is almost impossible for the average user to get at it, even if only to run some classic games or for emergency purposes.\n\nThis is why I don't think OpenOffice will last for the long haul. You said yourself that :\n\n>I'm not suggesting that OpenOffice is the better\ntechnical solution\n\nIn Open Source development, what else _is_ there but the quality of the software?"
    author: "Carbon"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-27
    body: "Not quite. It was a negative four sentence article! :-)"
    author: "Carbon"
  - subject: "kde/cygwin"
    date: 2001-06-27
    body: "Hey ,<br>\n<br>\nthere is a project to port kde under cygwin<br>\n* Xfree is running under cygwin <br>\n* the sourforge project is at the following    address:<br>\n<a href=\"http://kde-cygwin.sourceforge.net\">http://kde-cygwin.sourceforge.net</a>"
    author: "jesunix"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-27
    body: "Yeah, but I guess it'd be easier to run KDE/Qt \"natively\" then via the Cygwin/XFree layers. At least it's another option though. The devil is always in the details, and it's those details that are gonna get discussed a lot in the next couple of days/weeks :-)"
    author: "Sean"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-27
    body: "Oops, that should've been posted in reply to the 'kde/cygwin' post."
    author: "Sean"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-27
    body: "No it isn't. Cygwin is an excellent posix layer and because of kde is build basically on unix systems there are only minor changes like missing header and so one. The most problem currently is caused by limitations of the linker, which has to be solved for porting kde2. Additional the xfree-server has made no problem on porting. \nOn the http://kde-cygwin.sf.net discussion faq/discussion forum there are some topice relating for native porting kde/qt. \nAs summary 1) qt isn't a problem, if a developer with good windows api/gui knowledge does this task and 2) an emulation layer for the x dependent kde stuff will be created. On the above website there are links (ntxlib) showing an initial lib for that."
    author: "Ralf Habacker"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-27
    body: "This is simply specatcular, if not for the simple reason that it will shut up a few of the trolling gnomers that like to discredit the validity of using Qt.\n\nIf nothing else, this could help kde in such a manner as requiring a simple underlying library change removing the depency on X.  Once this is accomplished, kde will truly be universal.\n\nTroy Unrau"
    author: "Troy Unrau"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-27
    body: "There's no need to have a go at the Gnomes Troy. That's just flame bait. If someone wants to troll, ignore them, but for goodness sake don't encourage them with leading statements like yours."
    author: "Sean"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-27
    body: "Thats not going to stop them they'll continue to spread FUD. Its all they have anymore."
    author: "craig"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-28
    body: "Craig - be quiet and go away. You annoy the KDE people more than the GNOME people."
    author: "Bob"
  - subject: "Don't blame GNOME"
    date: 2001-06-28
    body: "I've never seen any pro-GNOME trolls at dot.kde.org, but there are lots and lots of pro-KDE trolls at Gnotices.\nBut remember that those trolls do not represent GNOME or KDE.\nThey represent the pro-M$/Slashbot/UltraFlamers community."
    author: "dc"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-27
    body: "This is great news... I'm sure it will help Qt and KDE both get more acceptance...\n\nI wonder what the reason was for this move though. I think it's probably because Troll knew that anyone could take Qt free edition for X and port it to Windows, so they would rather have an official version instead of a rogue version out there.\n\nI haven't read the license yet but I think someone posted that the license was more restrictive than the Qt free edition for X?\n\nIf that is so, they probably looked at their options (eventual free Qt for Windows with less restrictive license, or free Qt for Windows now with a license of their choice) and though the second option was better.\n\nWhat do you guys think?"
    author: "AC"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-27
    body: "Do you want to make QTwin32/GPL too? :)"
    author: "Vedran Rodic"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-27
    body: "I think, like many others that this is a good step.\nmaybee one day, Micro$oft will open source windows(never.. they are too greedy.)\nPerhaps this will make it easier to port software from KDE to windows, and make it easier for windows users to adapt to KDE (I hope we can still change the shell in windoze.. dont use it. hehe)\nthis is the result of a lot of hard work. I am grateful for the work of developers all over the world making things happen. just my 2 cents :)"
    author: "Mark19960"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-28
    body: "It is possible to change shells under Windows.  With Win9x it is a simple matter of changing a line in system.ini (the same way you did in Win 3.x).  In Win NT/2000 it's more of a registry change for your profile, but still can be done.\n\n\nLook at http://floach.pimpin.net for an idea of what replacement shells are already available.  Of course, KDE & Gnome as WinShells would be *really* subversive <g>."
    author: "James E. LaBarre"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-27
    body: "This is great. I would love to have Konqi in Windows for those times I must be in Windows (i.e. like right now <sigh>). At least I wouldn't feel so far from \"home\" if I had my favorite \"furniture.\" ;-)\n\nI might add this should also really increase exposure of QT to Windows developers, which means when they have to do a Linux programming project - they may very well choose QT over GTK, Motif/Lesstif, etc.\n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-27
    body: "Or even better - I'm not a programmer, but from what I've heard, QT is orders of magnitude nicer to code for than MFC. This may hopefully mean that more Windows developers (who wouldn't have even thought about *nix) writing their apps with QT instead of MFC. If this happens, then it's extremely easy for someone to port their app to Linux, BSD, MacOSX etc. etc. etc."
    author: "Matt"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-28
    body: "This is absolutely great news!\n\nThank you so much, Trolltech. I just wonder, might Cygwin be one of the supported compilers in the future?\n\nThe only issue right now is that one has to possess Visual C++, which is quite expensive.\n\nCygwin + Qt would be a great choice for win32 Open Source developers."
    author: "Jeremy M. Jancsary"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-07-03
    body: ">Thank you so much, Trolltech. I just wonder, might Cygwin be one of the supported compilers in the future?\n\n>The only issue right now is that one has to possess Visual C++, which is quite expensive.\n\nI kind of doubt that one is specifically locked into using Visual C++ with Qt NC for Win32.  Just a little bit of Cygwin linker time and/or wizardy would probably allow the compilation of Qt NC apps with Cygwin's gcc.\n\nHarold"
    author: "Harold Hunt"
  - subject: "TROUBLE!"
    date: 2001-06-28
    body: "I mean it.\n<p>\nMicrosoft just changed their developer licenses for all \"Visual XYZ\" products and libraries etc etc, so that you <B>MAY NOT DEVELOP FREE SOFTWARE</B> with Microsoft products. See <a href=\"http://www.theregister.co.uk/content/4/19953.html\">http://www.theregister.co.uk/content/4/19953.html</a> for more details. Here's an except:\n<p>\n\n<i>\"(c) Open Source. Recipient's license rights to the Software are conditioned upon Recipient (i) not distributing such Software, in whole or in part, in conjunction with Potentially Viral Software (as defined below); and (ii) not using Potentially Viral Software (e.g. tools) to develop Recipient software which includes the Software, in whole or in part.\n<br><br>\n\n...For purposes of the foregoing, \"Potentially Viral Software\" means software which is licensed pursuant to terms that: (x) create, or purport to create, obligations for Microsoft with respect to the Software or (y) grant, or purport to grant, to any third party any rights to or immunities under Microsoft's intellectual property or proprietary rights in the Software.\n </i>\n<p>\n\nThis might just be the end of Qt on Windows, if it NEEDS Visual C++. You can either develop proprietary software (and then you must pay for Qt as well!) or you switch to Unix altogether (doesn't need to be Linux)."
    author: "Jens"
  - subject: "Nah..."
    date: 2001-06-28
    body: "Hrm, I didn't follow your links, but if what you say is true you can just use a different compiler.  I have only used Qt/Windows with MS Visual C++, but I have heard it works with cygwin, and I think Trolltech even mentions compatibility with Borland on their website.\n\nNow, the non-commercial Qt/Windows is for VC only right now, but that's probably because it keeps the maintainence down.  I see no reason why Trolltech couldn't make a build for use with other compilers.  If this is truly an issue I'm sure they'll do it.\n\n-Justin"
    author: "Justin"
  - subject: "Re: TROUBLE!"
    date: 2001-06-28
    body: "I think that license only applies to MS's Mobile Internet Toolkit."
    author: "Daniel"
  - subject: "Viral? Haha!"
    date: 2001-06-28
    body: "Commercial licenses are viral too!\nThe Windows license infects everybody who buy a copy of Windows."
    author: "dc"
  - subject: "They did no such thing"
    date: 2001-06-29
    body: "That was a license on a _BETA_ version of the _Mobile Internet Toolkit_. Hardly 'all \"Visual XYZ\" products and libraries etc etc'.\n\nNow, for what they might do in the future; who knows."
    author: "DCMonkey"
  - subject: "Intention of developers"
    date: 2001-06-28
    body: "I think that linking programs written for the Qt libraries to the new Trolltech no charge licensed win32 libraries should be no problem.\n\nIf I write a GPL program using a non-GPL library, for example using Qt/win32 or Motif, I implicitly allow people to link my program to this library. I don't see why it would/should be forbidden to do this. If I want write a Motif application because, for example, I know the API better than of another toolkit, but I'm also a strong GPL advocate, why should I not be allowed to release it under the GPL and make my code available for reuse in other GPL programs?\n\nAnd if I've got an application using the Qt toolkit, I don't see why I can't link it to the non free win32 version of Qt. As long as this library does the same as the linux version and only has a different underlying system API it's no threath to the freedom of this program. It even enlarges this freedom by allowing people to run it on even more systems!\n\nJust my $ 1e-2"
    author: "Konrad Wojas"
  - subject: "Re: Intention of developers"
    date: 2001-06-28
    body: "You're obviously not a lawyer (not a bad thing :-)\n\nLawyers don't care about little things like \"intent\" or \"fairness\" when it comes to contracts like the GPL.  All that matters is the exact wording of the contract itself.  The GPL prohibits distribution of the program without complete source code available for all parts of it, and when QT is distributed along with KDE, it is part of KDE (using the strict definition provided in the GPL itself).  \n\nHowever, as Dre argued very convincingly farther up the page, if QT is distributed seperately from KDE, it is considered a seperate program and is not required to have source code available.  Therefore someone wanting to distribute KDE on Windows must simply distribute QT in a package that doesn't have any GPL'd parts of KDE in it.  \n\nThat's not hard to do; since all of kdelibs is LGPL'd, QT could simply be included there.  Problem solved!\n\nBesides, I find it highly unlikely that any KDE developers are going to start sueing people distributing KDE for Windows.  If no one sues, no one _gets_ sued, even if it is illegal.  Problem solved again!\n\nLooks like the way is clear for people wanting to port KDE to Windows.  Now all someone needs to do is *do* it..."
    author: "not me"
  - subject: "GPL with exception"
    date: 2001-06-28
    body: "I think you can \"pull a Linus\" and say that your program is GPL, but you allow linking to the non-commercial Qt.  In this case you'd have a sort of modified-GPL.\n\nOf course, this only works if every developer in the project agrees on this.  I plan to do that with one of my own projects since I'm the only developer.  Changing the license of a collaberative work like KWord would be more tricky.  But if its one of your own personal apps, I see no reason why you couldn't just release under GPL-with-one-exception."
    author: "Justin"
  - subject: "Re: GPL with exception"
    date: 2001-06-28
    body: "Replying to myself :)\n\nFrom http://www.trolltech.com/products/download/freelicense/noncommercial-dl.html\n\n-----\nIf you wish to port one of the many GPL'd Qt-based Unix applications to another operating system using the Qt non-commercial edition, you need to get that application's copyright holders to add an exception to its license. Similarly, if you develop a new application with the Qt non-commercial edition and wish to license it under the GPL you may wish to add such an exception to your license. The Free Software Foundation has provided the following wording for such exceptions:\n\nAs a special exception, <name of copyright holder> gives permission to link this program with Qt non-commercial edition, and distribute the resulting executable, without including the source code for the Qt non-commercial edition in the source distribution.\n-----\n\nGuess that answers it!"
    author: "Justin"
  - subject: "Legal Question?"
    date: 2001-06-28
    body: "What if a company develops a program under linux with the Qt Free Edition. but uses the software under windows with the Qt Non Commercial Edition.\n\nafter all everybody is allowed to develop using Qt Free Edition. and the non-commercial setting just effect developement and not the usage."
    author: "Jasper"
  - subject: "? Project File .pro is missing"
    date: 2001-06-29
    body: "Hi there,\nI am just wondering where the project file (.pro), stated in the readme.txt, can be found after project generation? I found only the MSVC++ Project workspace and the msvc++ project .dsp file.\n\nAny help is appreciated.\n\nThanks."
    author: "neo"
  - subject: "Re: ? Project File .pro is missing"
    date: 2001-06-29
    body: "If you generated your project using the integration plugin then a .pro file is not generated for you.\n\nThe example that is stated is based on the hello world example in the package (%QTDIR%\\examples\\hello) and is for if you build your applications without using the integration plugin.  \n\nIf you really need a .pro file, then you will have  to create this manually.  You might find that this is easily done by typing progen > project.pro  in the directory with your source as this will create a .pro and include any cpp/header and ui files."
    author: "Andy"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-30
    body: "This is really good news! Now free Qt applications can be ported to Winblows.\n\nThe bad news is that Visual C++ costs a lot of $$$; what about a Borland C++ port so that we can use the freely available Borland C++ 5.5 command line compilers? \n\nhttp://www.borland.com/bcppbuilder/freecompiler/\n\nWhat do you think?"
    author: "Sandro Sigala"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-06-30
    body: "Thats that why i thought. \nI just don't want to buy a compiler to develope free applications..."
    author: "Patrick Gleichmann"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-07-03
    body: "Yes !?!? Why not Borland or GNU C++ Compiler ? Free software with free compilers !"
    author: "Jens Albers"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-07-05
    body: "Well, I was going to say I don't really consider $100 that expensive, which about how much you'll find Microsoft Visual C++ 6.0 Standard for. It's like $92 at Amazon. But reading the page at Trolltech it says \"requiring Microsoft Visual Studio version 6\". Now *that* would be expensive. Why the need for Visual Studio instead of just Visual C++?\n\nIts seems Trolltech is mostly using the compiler for VC++? You can use what ever editor you want. It seems that Qt Designer would take care of whatever 'visual' uses VC++ would have.  Which means that you're mostly using VC++ for it's compiling/debugging features. Which of course begs the qustion why use VC++? There's plenty of other compilers and debugers out there. Oh well."
    author: "Ben Porter"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-07-01
    body: "I'm begining to think people in Trolltech are so  stupid as people in Micro$oft: they release a package to develop *ONLY* free software, but to be developed with a very expensive compiler owned by an anti-free-software company that imposes serious restrictions to free sofware development. Sincerly I can't understand it, there is a big bunch of great free C/C++ compiler for Windows (Borland command-line tools, MingWin, CygWin, RSXNT, ...)(without talk about other OO free compiled or interpreted languages).\n\nWhy, Why ?, I can't understand those mad people...  Oh, what a headache !\n\nPS: Sorry for my English, my natal language is Spanish."
    author: "Juan M. Caravaca"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-07-01
    body: "There is no need to enrich the Windows enviroment with such a good product and keep supporting this non-free products.Whoever wants to program GUI applications in M$-Win can do it with MFC . Open Source and Free Software must be distributed only to those who respect and support the Open Source Software idea and not to all those who keep using products hostile to Linux and open source , products . \nEveryone make his choice . Isn't he ?"
    author: "nestor"
  - subject: "Re: Trolltech: No-Charge License for Qt/Windows"
    date: 2001-08-02
    body: "If I want to write an application I want to make it available to as many people as possible. I choose Linux to do development *but* it has to be said that a lot of people do use Windows. Why should Windows users be denied the advantages of free and open-source software?\n\nAnd why should I use MFC? From what I've seen of it, it sucks, it's horrendously complicated and unstructured, the Fortran of gui toolkits. I'd far prefer use something like Qt or FOX (another cross platform toolkit)."
    author: "Nick Whitelegg"
---
<A HREF="http://www.trolltech.com/">Trolltech</A>, creators of the excellent cross-platform GUI library Qt on which KDE is based, <A HREF="http://www.trolltech.com/company/announce/noncommercial.html">announced</A> today a <A HREF="http://www.trolltech.com/products/download/freelicense/noncomm-license.html">new license</A> for Qt/Windows.  Called the <EM>Qt Non Commercial license version 1.0</EM>, it permits developers of non-commerical software to develop with and distribute the Windows version of Qt for free.  The Qt Non Commercial Edition for Microsoft Windows is a binary-only distribution and requires Microsoft Visual Studio version 6 (<a href="http://www.trolltech.com/products/download/freelicense/noncommercial-dl.html">download</a>, <a href="http://www.trolltech.com/developer/faq/noncomm.html">FAQ</a>).
Excellent news, but when will we see kdelibs and <A HREF="http://www.konqueror.org/">Konqueror</A> for Windows?
<!--break-->
