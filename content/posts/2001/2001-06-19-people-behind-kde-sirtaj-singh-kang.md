---
title: "People behind KDE: Sirtaj Singh Kang"
date:    2001-06-19
authors:
  - "Inorog"
slug:    people-behind-kde-sirtaj-singh-kang
comments:
  - subject: "Re: People behind KDE: Sirtaj Singh Kang"
    date: 2001-06-19
    body: "Hmm.. Over the summer, what about releasing two interviews each week... One on monday and one on Friday?\n\nIf this is too much work for Tink I would gladly help.."
    author: "Christian A Str\u00f8mmen [Number1/NumeroUno]"
  - subject: "Re: People behind KDE: Sirtaj Singh Kang"
    date: 2001-06-19
    body: ">> Konqy, Kandalf [*Ralf Nolden] and a few M$ executives. I can't describe the scene as this is a G-rated site, but think Pulp Fiction.\n\n   Ha!  I can picture it now (but not in a Pulp Fiction sort of way)... Kandalf is sitting on a bench while wearing his blue robe with his staff off to the side. He is slowly feeding some pigeons from a burlap sack of grain.  A few MS exec types wearing dark sunglasses walk up to him.  \n\n   \"Sir, there have been reports that you don't use Windows\".\n\n   Kandalf, ignoring them and still feeding the birds speaks in a slow voice, aged but strong: \"Yes... I use KDE.  I get my work done and have time for the better things in life\".\n\n   MSMiB: \"Sir, the rest of the world uses Windows\".\n\n   Kandalf: \"No, I and many others word process, work on spreadsheets, play games, and surf the internet in a full multimedia experience...\", (looks up at the MSMiB from under his cowl with a slight smile) \"...with a stability and power you could never dream of\".\n\n   MSMiB: (glances at his partners, then back at Kandalf) \"I'm sorry sir.  You're going to have to come with us\".\n\n   Kandalf: (looking down and calmly feeding the pigeons again) \"I don't think so\".\n\n   Suddenly, thirty feet tall (10 meters), Konqi crashes through the bushes behind the bench, takes a deep breath and completely incinerates the MS goons.  The camera drops to the still smoking shoes surrounded by a light dusting of ashes.\n\n   Announcer: \"KDE - it toasts the competition\".\n\n   Now *that* ad would have me dancing and hooting with joy the first time I saw it.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Quick, everyone certify Taj"
    date: 2001-06-20
    body: "<a href=\"http://www.advogato.org/person/taj/\">http://www.advogato.org/person/taj/</a>\n<p>\nHe's already certified Master, but let's give him more points so he can spread them among even more KDE developers.\n<p>\nHelp the KDE takeover of advogato."
    author: "Advogato user"
  - subject: "Less frightening"
    date: 2001-06-20
    body: "Hey \"Targe\" :),\n<p>\nyou look more friendly than this old picture <a href=\"http://www.kde.org/gallery/taj.gif\">ttp://www.kde.org/gallery/taj.gif</a> makes all of us believe. :)"
    author: "Lenny"
  - subject: "Re: Less frightening"
    date: 2001-06-21
    body: "I know. :) you're not the only one."
    author: "kev"
  - subject: "Re: People behind KDE: Sirtaj Singh Kang"
    date: 2001-06-22
    body: "I think there should be a competition on who the bigger Metal God is.  Jono Bacon or Sirtaj, both claim to be, but who really is?"
    author: "Charles Samuels"
  - subject: "Re: People behind KDE: Sirtaj Singh Kang"
    date: 2001-06-22
    body: "Maybe someone could organize a fight to the death at KDE3?"
    author: "Navindra Umanee"
  - subject: "Re: People behind KDE: Sirtaj Singh Kang"
    date: 2001-06-22
    body: "That might work, perhaps a hoedown would be a better idea? :)"
    author: "Charles Samuels"
  - subject: "Re: People behind KDE: Sirtaj Singh Kang"
    date: 2001-06-25
    body: "I will fight the mighty taj any day to prove my status as the official KDE Metal God.\n\nI will slay him with my Jackson Kelly guitar, out-headbang him to Primal Fear and out-mosh him to Slayer.\n\nName the place and date and I will be there. ;-P"
    author: "Jono Bacon"
  - subject: "gauntlet"
    date: 2001-07-03
    body: "Very well, I'll raise the stakes - a marshall amp that goes to 11, an album called \"smell the glove\" and two spontaneously imploding drummers."
    author: "taj"
---
<a href="http://www.kde.org/people/taj.html">Sirtaj Singh Kang</a> is one of the app-producing machines that helped propel Unix into the desktop arena way back in the legendary days of the KDE genesis. His native tongue is Perl and his boot-up sequence involves a quirky coffee-shower combination. We also look forward to Taj's soon-to be-released mysterious Python application that will allegedly forever change the way we open the doors of luxurious buildings. Or something.
<!--break-->
<br><br>
<i>[My final request this week is for all the kind readers of the dot to join together their positive thoughts into a prayer so that Tink doesn't feel the infinite length of the few days that separate her from <a href="http://www.kde.org/people/people.html">the moment of reunion</a> with her daughter for the first time this year. Aummmmm!]</i>
