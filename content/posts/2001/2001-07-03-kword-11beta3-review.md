---
title: "KWord 1.1beta3 review"
date:    2001-07-03
authors:
  - "numanee"
slug:    kword-11beta3-review
comments:
  - subject: "Adobe ichiness"
    date: 2001-07-03
    body: "It appears that Adobe has decided that KIllustrator imposed upon their trademark \"Adobe Illustrator\", and have sued for copyright infrigement! Check the www.koffice.org site, all mention of killustrator on the main page is gone.\n\nPtooie!"
    author: "Carbon"
  - subject: "Re: Adobe ichiness"
    date: 2001-07-03
    body: "That is a bit odd. Can anyone confirm this? (and by the way it would be trademark infringement, not copyright, and most likely they wouldn't have sued, but sent a cease+desist)"
    author: "Matt"
  - subject: "Re: Adobe ichiness"
    date: 2001-07-03
    body: "Oops, you're right, I wasn't paying attention to the legal terms.\n\nAs for it's validity in general, you can see for yourself. Check this out :\n\nhttp://lists.kde.org/?l=koffice-devel&m=99407842431174&w=2"
    author: "Carbon"
  - subject: "They're all on vacation"
    date: 2001-07-03
    body: "Can you believe it? Adobe's offices are completely shut down from July 2 to July 9 for the Independence Day holiday. All the Adobe PR people I called had voice mail saying \"I won't be checking my messages.\"\n\nI even tried calling an outside PR firm that handles some of their stuff and they wouldn't comment on the matter because it was a \"legal issue.\"\n\nTina"
    author: "Tina Gasperson"
  - subject: "They want money from poor Kai"
    date: 2001-07-03
    body: "Adobe is demanding money - 2500 Euros - so they might as well have sued."
    author: "ac"
  - subject: "Re: Adobe ichiness"
    date: 2001-07-06
    body: "AFAIK they did send a \"cease+desist\", or the German equivalent, an \"Abmahnung\". However, they had it sent by a German law firm (maybe they have no in house lawyers, or none with any competence in German trademark law). This law firm bills the adressee of the \"cease+desist\" for the legal fees.\nThe expensive bill is based on the fact that they fixed the value of claim (??? Streitwert) at half a million euro. (Source: http://www.heise.de/newsticker/data/odi-03.07.01-000/ )\n\nOn the other hand, there is at least one law firm in Germany who would have started sending \"cease+desist\" letters (and bills) to all owners of websites in Germany who happen to mention killustrator. So what's happening here is comparatively civilized..."
    author: "Wolfgang Ratzka"
  - subject: "Re: Adobe ichiness"
    date: 2001-07-03
    body: "removed all products of adobe for that."
    author: "el shabazz"
  - subject: "Re: Adobe ichiness"
    date: 2001-07-03
    body: "Did you pay for them?\nIf you havent Adobe doesnt care about you, and if you have, Adobe is already happy.\n\nI think you are the onyl one who will remove a 600$+ program because of something like that ( although the fee without sending a cease and desist letter first is really bad ).\n\nAdobe is correct to defend its trademark. The KDE project has to come up with better names, like they already did with Krayon, etc."
    author: "ac"
  - subject: "Re: Adobe ichiness"
    date: 2001-07-03
    body: ">Adobe is correct to defend its trademark. The KDE project has to come up with better names, like they already did with Krayon, etc.<\n\nBullshit. Adobe Illustrator or Illustrator are Adobe's trademarks. Killustrator is not. It is so simple. Or should we also remove Kword and Kwrite\nbecause of Microsoft, or should I mention www.krayon.co.uk, or www.krayonbox.com, or Krayon petrol jelly for children, or The electric krayon paint shop, or Krayon Technologies, or Krayon Computers, or www.multimania.com/krayon, or Krayon Systems and Design Company, or Krayon's Gallery in Sillicon Valley etc. etc."
    author: "Antialias"
  - subject: "Re: Adobe ichiness"
    date: 2001-07-03
    body: "Adobe Illustrator _and_ Illustrator are Adobes trademarks.\n\nKillustrator is not, but adding one letter doesnt add significant distinction to the name.\n\nYes. KDE will have to rename every application if it doent want to pay lawyers. Just because unfortunately Word has become a household name and synonym for a word processing program doesnt mean that it lost its legal status as a trademark ( if it is one - I dont know ). \n\nThe main problem is that you have trademarks in every country, and almost no mean to check them.\nThe us trademarks and canadian are easily searchable, others are not. And do you have to care about all of them?\nkrayon doesnt seem to be a trademark, at least in the us.\n\nurls and domain names, shop names and program names are not trademarks.\nA trademark is expensive covers onyl a limited number of categories (something like: wood products, software) and gives you a lot of rights once it is issued on your behalf.\n\nbest wishes\nac"
    author: "ac"
  - subject: "Re: Adobe ichiness"
    date: 2001-07-03
    body: ">Killustrator is not, but adding one letter doesnt add significant distinction to the name.<\n\nYou are not right. Adding a letter does significant distinction to a name. That's the point. Or you wont be able to give name to anything at all. And I suggest one more letter to be added to Killustrator. Rename it to Killustraitor = Kill-US-Traitor \n\n(traitor=Etymology: Middle English traitre, from Old French, from Latin traditor, betray, \n1 : one who betrays another's trust or is false to an obligation or duty\n2 : one who commits treason )\n\n>krayon doesnt seem to be a trademark, at least in the us<\n\nDon't tell me we can't face the same problem if\nKrayon Computers, Krayon Technologies or Krayon Systems and Design Company decide to sue KDE because of using the name Krayon."
    author: "Antialias"
  - subject: "Re: Adobe ichiness"
    date: 2001-07-03
    body: "But you have to keep in mind that trademarks are only valid for one field - if someone has a trademark on a pen called Illustrator it doesn't have any bearing on a computer program. So all of those Krayons aren't going to be any problem for KDE because they have nothing to do with computer programs. Adobe is causing trouble because their vector drawing program is called Illustrator."
    author: "David G. Watson"
  - subject: "Re: Adobe ichiness"
    date: 2001-07-03
    body: "Where did you get your law degree? You really don't have much of a clue about the real world, business, or human nature, do you. AC is right on. A single letter change is not protection. Mis-spelling a name is not protection. Would the program have been called KIllustrator if Adobe Illustrator didn't exist? No, it wouldn't. If it was Adobe NosePie, the program would be called KNosePie. Is that fair? Does it really matter? Adobe has full right to sue. Will they win? Again, does it matter? Having the pay legal bills to defend a questionable stance isn't really worth it in my book. I've been waiting for MS to attack KWord for a long time. It WILL happen one day. Why? Because they can, and it will be a problem for KDE and Open Source in general."
    author: "Unintendo"
  - subject: "Re: Adobe ichiness"
    date: 2001-07-03
    body: "IMHO, Microsoft causing trouble over the name KWord would be no problem at all - it would be the best publicity we could ask for. Adobe is a different matter - while they're well known in computing, they are not known to the average man on the street. Microsoft Word vs. KWord is news, Adobe Illustrator vs. KIllustrator is not.\n\nRich."
    author: "Richard Moore"
  - subject: "Re: Adobe ichiness"
    date: 2001-07-04
    body: ">Where did you get your law degree?<\n\nWhat are you talking about?\nA lawyer can't be specialist in all fields. I am specialist in criminal law. Trademarks? Pretty boring stuff. \n\n>You really don't have much of a clue about the real world, business, or human nature, do you.<\n\nAbout real world: stand up, don't hide, be a man,\ntell me your real nick or name.\n\nAbout business: I can remember your business concepts on kde-user mailing list. Not visionary at all. \n\nAbout human nature: should I repeat here what you wrote to me? Very agressive, attacking me personally, my profession...What a human nature.\n\n>A single letter change is not protection. Mis-spelling a name is not protection.<\n\nWho says that it was a single letter change? I hope you are not KIllustrator's developer. Who told you that Killustrator's developers did it deliberately - changed a letter or misspelled it. I doubt it. Illustrator is a common word, used by many people and companies, and KDE people probably thought that adding K to Illustrator would be enough to make distinction from other similar applications (also\nAdobe Illustrator). I understand them and support them. I am a lawyer but not that kind of lawyer employed by Adobe. Threteaning KDE with a lawsuit is immoral for many reasons:\n1. They know KDE is nonprofit\n2. They know KDE has not have money to pay lawyers\n3. They know there are some other graphic apps which have a word 'Illustrator' as part of their names\n4. They applied for a trademark (and got it,.. sic!) for a common used word\n5. They know that their application is not known as 'illustrator' but as Adobe Illustrator.\n\n>Would the program have been called KIllustrator if Adobe Illustrator didn't exist? No, it wouldn't.<\n\nWhy not? Adobe hasn't invented the word 'illustrator'.\n\n>If it was Adobe NosePie, the program would be called KNosePie.<\n\nC'mon. Your underestimate kde developers.\n\n>Adobe has full right to sue.< \nAdobe has not full right to sue. I'll try to explain it later (but, huh, why I'm waisting my time on you?)\n\n>Will they win? Again, does it matter?<\n\nWill they win? I don't know. Nobody knows. But I think KDE have very good arguments here (of course not if KDE admits that it was just a 'single letter change' as you are trying to impose here.\nDoes it matter? It matters to me. Why TF should Adobe have monopol on common used words?\n\n>Having the pay legal bills to defend a questionable stance isn't really worth it in my book.<\n\nThat is one solution (and the best one). Another solution could be some lobbying or even asking for legal assistance (IBM, Compaq, HP, maybe Apple also, companies which are involved in open source movement and can tell Adobe to shut up).\n\n------------------------------------------------\n\n'A trademark is a word, name, symbol or device which is used in trade with goods to indicate the source of the goods and to distinguish them from the goods of others. Trademark rights may be used to prevent others from using a confusingly similar mark, but not to prevent others from making the same goods or from selling the same goods or services under a clearly different mark.'\n\nIs KIllustrator really 'confusingly similar mark'?\nIn my opinion not. In eauropean law practice it is not. If the name was Andobe Illustrator or Ardobe Illustrator etc. I would be the first who would say: what are you doing guys? \n\nBut what is very, very interesting here:\n\n'A trademark includes any word, name, symbol, or device, or any combination, used, or intended to be used, in commerce to identify and distinguish the goods of one manufacturer or seller from goods manufactured or sold by others, and to indicate the source of the goods. In short, a trademark is a brand name.'\n\nLook here: '...used, or intended to be used, in commerce...' and '...to identify and distinguish the goods of one manufacturer or seller from goods manufactured or sold by others...'\n\nKDE is nonprofit, Killustrator is not commercial application, it is not used nor intended to be used in commerce. And this is a good argument in an eventual lawsuit. \n\n-------------------------------------------------"
    author: "Antialias"
  - subject: "Re: Adobe ichiness"
    date: 2001-07-05
    body: "Call it Kunilingus for all I care if it's a good application I'll use it. That is the risk someone took when they decided to call it Killustrator. \n\nIf someone came out with a soft drink called Koka Kola I'm sure they'd get threatened too."
    author: "Sean Pecor"
  - subject: "Re: Adobe ichiness"
    date: 2001-07-06
    body: "except coca cola is not a commonly used word (or words in this case.) illustrator, on the other hand, is."
    author: "Rick Kreikebaum"
  - subject: "Re: Adobe ichiness"
    date: 2001-07-07
    body: "Well, I'd have to say that (unfortunately) it is likely the phrase coca cola is spoken, printed and seen more often than the term term \"illustrator\". In fact, coca cola is found in slightly more documents on Google than illustrator is (586,000 vs. 519,000). \n\nBut I do understand your point. In this age of litigation the question isn't are you \"in the right\", it is \"do you  have enough money and resources to prove your point better than your adversary\"."
    author: "Sean Pecor"
  - subject: "Re: Adobe ichiness"
    date: 2001-07-08
    body: "yup, the world sucks"
    author: "Rick Kreikebaum"
  - subject: "Re: Adobe ichiness"
    date: 2001-07-08
    body: "yup, the world sucks"
    author: "Rick Kreikebaum"
  - subject: "Re: Adobe ichiness"
    date: 2001-07-05
    body: ">Is KIllustrator really 'confusingly similar mark'?\n> In my opinion not. In eauropean law practice >it is not. \n\nWhat is european law? In germany (1993) a mailbox owner \"RainboX\" was sued ( and lost ) by the company \"RainbowBBS\", because of trademark infringement.\n\nAG M\u00fcnchen \"RainbowBBS\" (Az:161 C 4781/93) \nLG N\u00fcrnberg \"RainboX\" (Az: 3 O 42660/93)\n\n>nonprofit argument:\n1.\nRainboX was nonprofit fidonet. The profitability is irrelevant\n\n2.\nThe thretened developer does it for free and is really \"noncommercial\". There are other developers who get paid = commercial.\nDistributions sell it  = commercial \n(just because they sometimes dont demand money doesnt make it free)"
    author: "ac"
  - subject: "Re: Adobe ichiness"
    date: 2001-07-06
    body: "> 'A trademark includes any word, name, symbol, or device, or any combination, used, or intended to be used, in commerce to identify and distinguish the goods of one manufacturer or seller from goods manufactured or sold by others, and to indicate the source of the goods. In short, a trademark is a brand name.'\n\n>Look here: '...used, or intended to be used, in commerce...' and '...to identify and distinguish the goods of one manufacturer or seller from goods manufactured or sold by others...'\n\n>KDE is nonprofit, Killustrator is not commercial application, it is not used nor intended to be used in commerce. And this is a good argument in an eventual lawsuit.\n\n\ni generally agree, but not with that last argument. my argument is:\n\nlook here: '...and to indicate the source of the goods. In short, a trademark is a brand name.'\n\nis \"illustrator\" a brand name? is \"word\" a brand name? i don't think so. if you wore a t-shirt that said \"illustrator\" on it, would anyone know what it meant? or with \"word?\" (btw, is word trademarked?) does the name \"illustrator\" for a software product indicate the source of that software product? if someone said to a severely computer illiterate person \"hey i got a program called illustrator,\" would they know it was from adobe? no. \"illustrater\" does not indicate the source of the goods. and it is not (or shouldn't be) a brand name. therefore, i think  the trademark is invalid and should not have been granted. and i do think that killustrator is a significant distinction because illustrator is a common word, actually means something, and describes the application (as opposed to nosepie).  if nosepie was an application that had nothing to do with noses and pies, if the word nosepie was  trademarked, and if there was a kde version called knosepie, i could see how that would cause problems, because it would be an uncommon, distinguishing name, and therefore would indicate what source it came from if the program was as popular as adobe illustrator. but killustrator describes the program. AND it indicates where it comes from (the kde project, and anyone who is even remotely familiar with it knows that many if not most kde applications start with a k.) there is no way that killustrator could be confused with adobe illustrator or vice versa, because the word \"illustrator\" is so freaking common, and it describes what type of application it is. so adobe should not win. they should lose and be forced to give up \"illustrator\" as a trademark.\n\ni don't agree with your last argument. i could be wrong on this, but maybe they mean that the commerce part only applies to the trademark itself. maybe you could violate a trademark even if your product is not commercial and you're not techinically \"manufacturing\" it. but then again: '...to identify and distinguish the goods of one manufacturer or seller from goods manufactured or sold by others' so i kindof see your point. however, it depends on how you define \"manufacture.\" if the kde team is \"manufacturing\" kde, then there could be an argument IF the products were indeed indistinguishable (which they are not, IMO.) as far as \"commerce\" goes, kde could be considered non-commercial, but i think they could still get sued because of the wording of the argument. >'A trademark includes any word, name, symbol, or device, or any combination, used, or intended to be used, in commerce to identify and distinguish the goods of one manufacturer or seller from goods manufactured or sold by others...' theoretically, the trademark itself is commercial, but you could violate it by just being a manufacturer, or having another \"good\". so theoretically, even if programming is not manufacturing, your product would still be a \"good of others,\" and therefore subject to the patent issues (>'A trademark is a word, name, symbol or device which is used in trade with goods to indicate the source of the goods and to distinguish them from the goods of others.') the trademark is used in trade with a (commercial) good, but who says the \"goods of others\" have to be commercial? hmm... if a trademark can only be used with a traded good and in commerce, then killustrator could not be patented if it wanted to, because it is given away for free. oh, i don't know. i don't know anything about trademark law. i'm just a 17 yo linux enthusiast :)."
    author: "Rick Kreikebaum"
  - subject: "Re: Adobe ichiness"
    date: 2001-07-06
    body: ">...i'm just a 17 yo linux enthusiast :).<\n\nJust? :)\nI wish I was 17 and linux enthusiast ;-)\nI am just Linux enthusiast, heh."
    author: "Antialias"
  - subject: "[OT] UI copyright"
    date: 2001-07-03
    body: "I know this is a little bit off-topic, but the letter from the lawywer to the killustrator developer made me a little bit nervous.\n\nIs there any place where you can seriously discuss / find information on user interface copyright? \n\nI found this, but I dont know how biased they are and how well this reflects current court decisions:\nhttp://lpf.ai.mit.edu/Copyright/copyright.html\nI also found some legal analysis on the net\n\nThe reason for this is as follows:\nI am currently developing a program. I tried it in my own way, but it didnt really work out. The commercial program has some really nice ideas about how to display/control the data, and I simply copied it.\n\nNow my bad conscience hunts me, I feel guilty and I am sure that the day after I put screenshots on sourceforge I will receive a letter from a lawyer.\n\nSo what should I do?\n\nIn particular I \"copied\" one widget, which I think is original:\nIt consists of a listview, which controls the graphical representation of the data.\n\nThe graphical representation is standard, but the ease with which you can manipulate it through the listview is probably novel. I dont know if I would have come up with the same idea without having seen this program before.\n\nIf anyone has examples of programs which have possibly something like this please post them here."
    author: "ac"
  - subject: "Re: [OT] UI copyright"
    date: 2001-07-03
    body: "I haven't heard recently about any company trying to enforce GUI copyrights in that sense so I don't think that is going to be a problem. (Apple has bullied people over it's Aqua style, but it has more to do with the graphics than anything else.\nWhat you perhaps should be wary of, is patenting of the widgets. Adobe has sued Macromedia over a certain kind of widget it has patented. See here:\nhttp://maccentral.macworld.com/news/0008/10.adobe.shtml"
    author: "nap"
  - subject: "Re: [OT] UI copyright"
    date: 2001-07-06
    body: "would microsoft have come up with all their gui ideas if it hadn't been for apple?"
    author: "Rick Kreikebaum"
  - subject: "Re: Adobe ichiness"
    date: 2001-07-03
    body: "A drawing-program should be named Karan d'Ache.\nNo problem."
    author: "reihal"
  - subject: "Re: Adobe ichiness"
    date: 2001-07-03
    body: "Or better Odjebiad Obe"
    author: "Antialias"
  - subject: "Re: Adobe ichiness"
    date: 2001-07-03
    body: "Why don't they simply rename it instead of removing it completely? TAFKAK (The application formerly known as KIllustrator) or Rotartsullik should do it..."
    author: "stew"
  - subject: "Re: Adobe ichiness"
    date: 2001-07-03
    body: "As much as I like KDE as a whole and KOffice, there is one thing that I really, well, almost hate: The habit of naming any program with a \"k\". kedit, kwrite, kmail, kword, koffice.... OK, everybody knows what the program is doing und that it's running under KDE, that's a point.\n\nApart from that it's simply boring and often enough stupid sounding. And it has disadvantages, as this Adobe thing shows. The chances for them are probably not to bad, and even if not, who would be paying the lawyers for the KDE team?\n\nSo, my advice: Take this as an opportunity and look for some creative, unique name! Maybe even starting with \"k\", like \"krayon\", but, please, not simply putting a \"k\" in front of something."
    author: "Ineluki"
  - subject: "Re: Adobe ichiness"
    date: 2001-07-03
    body: "Agreed!\n\nLet's start an application naming 'contest'.  Let's collect everybody's best name and hve a vote.  Sound simple to me since there are lots of great users.."
    author: "kde fan"
  - subject: "Re: Adobe ichiness"
    date: 2001-07-03
    body: "Do you mean a kontest?  :-)\n\nI actaully think silly names based on bad puns give Linux software character.  \n\nIt's one of the true benefits of not have to sell your software.\n\nWhere else would you get a browser that was a play on the competitors' names instead of the latest trendy buzzword?"
    author: "Scott"
  - subject: "Re: Adobe ichiness"
    date: 2001-07-04
    body: "> kedit, kwrite, kmail, kword, koffice.... OK, everybody knows what the program is doing und that it's running under KDE, that's a point. <\n\nTell me.. how would one know these are kde apps before running them?\n\nThanks,\n\nJason"
    author: "Jason Katz-Brown"
  - subject: "Re: Adobe ichiness"
    date: 2001-07-03
    body: "Odd, I've just had a course on patents, trademarks etc - and descriptive names like \n\nthe icecream \"creamy ice\",\nthe car \"fast vehicle\",\nthe illustration program \"illustrator\" or\nthe company \"nano tools\"\n\nare NOT possible to protect using trademarks. This is exactly why most startups have to change their names before going serious (you can't protect the name \"nano tools\", but ZockFoo is perfectly OK).\n\nAt least, that's the way it works in Europe. If this works the way other things work in the US, you can register ANY trademark, but whether it is guilty or not has to be decided in court. Large companies like Adobe will then be able to - very effectively - defend their too descriptive names by simply saying \"boo\" to smaller competitors. \n\nPS: I'm really a physicist, so don't trust me too much on legal matters..."
    author: "Johan"
  - subject: "Where does KDE get legal counsel?"
    date: 2001-07-04
    body: "Is there a volunteer attorney or something like that? Curious because doesn't this usually cost $?"
    author: "Barrett"
  - subject: "Adobe Public Relations Email Addresses? Petition?"
    date: 2001-07-05
    body: "I want to email Adobe explaining my outrage with their activities. However, I cannot find an email address for Adobe on their site (typical). Does someone have one? Is there a petition?"
    author: "gil"
  - subject: "Re: Adobe Public Relations Email Addresses? Petition?"
    date: 2001-07-05
    body: "Someone on slashdot (http://slashdot.org/articles/01/07/04/013249.shtml )  posted a bunch of relevant email addy's (to send POLITE inquiries to). \n\nSomeone also set up a \"Help Kay-Uwe\" page at http://www.mtbwr.com/kay-uwe.html , at the bottom there are email addresses to both the lawyers and adobe."
    author: "Ott"
  - subject: "Re: Adobe Public Relations Email Addresses? Petition?"
    date: 2008-07-14
    body: "now.i want to contact mr.david peters of you office.\nWY..BECAUSE HE SAND ME A MESS. FOR TEST DRYVE PHOTOSHOP FAST TRACK AND HE JAM MY COMPUTER.\nIMPOSSIBLE TO REMOUVE IS 3 MESS.HE SAND ME.\n I DONT NEED YOUR F...FAST TRACK OK AND DONT SAND ME NO MORE MESS."
    author: "robert bolduc"
  - subject: "Re: Adobe Public Relations Email Addresses? Petition?"
    date: 2008-08-14
    body: "I have nothing to do with the comment regarding petition.  I am looking for the e-mail address.  I have been trying to order an adobe acrobat 9 download.  Each time I try I get \"unabel to connect to URL\"  can you help a want to be customer.  Janine"
    author: "janine demings"
  - subject: "Re: Adobe Public Relations Email Addresses? Petiti"
    date: 2008-12-09
    body: "I found a email that might work. \"support@adobe.com\" I sent a email to them and I got a return saying \n\n\"Hi,\n\nYou have replied to an automated e-mail system that is unable to respond to individual questions or comments. \nIf you have further questions, visit the Adobe Support website at www.adobe.com/support/.\n\nThanks & Regards\nAdobe Support\"\n\ntry using somthing like help@adobe.com?\n"
    author: "Zachary"
  - subject: "Re: Adobe ichiness"
    date: 2001-07-05
    body: "While they are being totally and revoltingly obnoxous in the way they went about it I do see their tiny point.\n\nIs it a sensible suggestion that K.....lust..... be merged with Krayon ( lovely name - congratulations to who ever thought of it ) and both packages be distributed under that name?\n\n\nbtw, When this happens it means that they see the competeing product as a threat. Well done!"
    author: "Christopher Sawtell"
  - subject: "Seems like someone has made a \"help Kay-Uwe\" page"
    date: 2001-07-05
    body: "http://www.mtbwr.com/kay-uwe.html\n\nThere's also a slashdot article on the subject at http://slashdot.org/articles/01/07/04/013249.shtml which is where i found that help page."
    author: "Ott"
  - subject: "open source inside joke"
    date: 2001-07-03
    body: "part 5: \n\"I was going to write to some of the most prominent leaders of the world, ..., and 499 CEO of the Fortune 500 companies.\"\n\nHeh!  Heh! :)"
    author: "KDE User"
  - subject: "Re: New KDE dot design"
    date: 2001-07-03
    body: "wow - looks great! :-)"
    author: "Christoph Held"
  - subject: "Re: New KDE dot design"
    date: 2001-07-04
    body: "Yeah - qwertz did some magic on it :-)"
    author: "Tackat"
  - subject: "Re: New KDE dot design"
    date: 2001-07-04
    body: "So much for coinciding with the 2.2beta1 announce.  We all got impatient. :-)\n\nFunny how a little touch from a tortured artist can make a big difference.\n\nCheers,\nNavin."
    author: "Navindra Umanee"
  - subject: "It looks awesome!"
    date: 2001-07-03
    body: "I tried KOffice when it first officially came out. I wasn't that impressed. It needed time to mature. And I have been waiting patiently.\n\nAfter reading this review, I can see that KOffice exceeds my expectations (assuming other apps are as good ad KWord seems to be)! Now, if we only could get 100% compatibility with MS-Office file-formats...\n\nGreat job KOffice-team! I'll be among the first to download Koffice when it gets out of beta-status!"
    author: "Janne"
  - subject: "Re: KWord 1.1beta3 review"
    date: 2001-07-03
    body: "KWord? Isn't a word 'Word' Microsoft's trademark?\nAnd menubar on top of desktop alla MacOS, isn't it something Apple can sue kde for? And Win2000 window decoration? And some new icons that look like WinXP's icons? Arghhhh....\nAdobe hasn't invented word illustrator, there are some other graphic apps which have 'illustrator' as a part of their name for example: Auto-Illustrator\n(Auto-Illustrator is the leading auto-generative vector graphic design package. It is the smarter, more experienced vector version of Autoshop. Intended to sit alongside your existing vector graphics applications, it is most useful when generating abstract graphical artwork for use in web and screen design, printed media and animation/video production.http://www.auto-illustrator.com/main.html ).\nShould I mention F4Illustrator, Periodic Table Illustrator, Tech Illustrator, Tatoo Illustrator\n(... Debi Kienel, better known as \" The Illustrator\" has been tattooing for 24 years\nand is dedicated to quality renderings that are truly works of art. ... hehe), Bible Illustrator for Windows, EZ Illustrator, Equation Illustrator\n(... Equation Illustrator V 1.2.1 Shareware ($20.00). Equation Illustrator V has been\ndesigned to ease the difficult task of combining graphics and complicated ... ), Meet The Illustrator, 'Donald Duck illustrator is dead..',\nIllustrator Pak (100 AWESOME PLUG & PLAY CORELDRAW TEMPLATES!), etc."
    author: "Antialias"
  - subject: "Trademarks on illustrator and Word"
    date: 2001-07-04
    body: "No, MS can't sue KDE over KWord, nor over KOffice... they don't have trademarks on those, and the expressions are too general to be trademarked. Illustrator is another matter: Adobe _has_ a trademark on this, and if they don't defend it, they'll loose it. So it's entirely understandable that Adobe is angry... heck, just call it KVector, it's not gonna hurt.\n(On slashdot, they ran a story about this, there are some nice suggestions for a different name. KOffice people, check it out)"
    author: "Anno1602"
  - subject: "Importing Ms Office files?"
    date: 2001-07-03
    body: "The last time I tried koffice was quite a long time ago when 1.0 was released\n\nI started kword, kspread, and all the others, wrote a couple of random lines and clicked on the icons...WOW it looked cool\n\nAnd then I tried opening msoffice files. I know the format of these files is proprietary, but as I had been able to open & see them with Star office / Abiword / Gnumeric, I expected it to work. Result: segfault.\n\nIf I remember correctly, even trying to save to the .doc format led to a segfault. The UI fellt sluggish when working with large files.\n\nI also remember trying to copy / paste text from a file into kspread...which led to very disappointing results. With gnumeric it worked like a charm.\n\nIt's not a troll..I just want to know whether koffice has improved in these areas. I f yes, then I might give it another chance"
    author: "Joe"
  - subject: "Re: Importing Ms Office files?"
    date: 2001-07-03
    body: "> It's not a troll..\n\nNo, just lazy. Sorry, but or goodness sakes its a free program that comes in binaries for just about any distro (deb, many rpms, etc.)  Just download the thing and try it out!\n\nErik"
    author: "Erik Severinghaus"
  - subject: "Re: Importing Ms Office files?"
    date: 2001-07-03
    body: "Yes, the import filters (in particular for Word 97 and 2000) have much improved.\n\nLooking for help with the other formats though (Excel, Powerpoint, Word-95...). Contributions welcome !!!"
    author: "David Faure"
  - subject: "But did you file any bug reports?"
    date: 2001-07-04
    body: "Joe, if you were one of those who filed problem reports, you might now be sorted out since, when I checked a few days ago, I had closed all MS Word filter bug reports I have received (well, unless they happened to be Word 95 files). If not, then YMMV...\n\nThis is not a dig, and I certainly don't claim to be able to fix everything every time, my point is simply that I can only work with the material I have."
    author: "Shaheed"
  - subject: "Re: KWord 1.1beta3 review"
    date: 2001-07-03
    body: "Koffice still crashed on me when i tried to open a word document. It seems to have gotten a lot better though. I came across this new Linux desktop site http://GUI-Lords.org Cool place to discuss this stuff as well. \n\nCraig"
    author: "craig"
  - subject: "Re: KWord 1.1beta3 review"
    date: 2001-07-04
    body: "You wouldn't happen to be the same Craig that helps *run* gui-lords, would you?  Just wondering.... ;-)  (I have no inside knowledge, just put together the fact that Craig, with the aim username of blackfam972 could quite possibly be the same person as blackfarm@mountain.net.)"
    author: "David Bishop"
  - subject: "Re: KWord 1.1beta3 review"
    date: 2001-07-04
    body: "Well i have submitted a few stories but i have no connection with the site. I don't know anyone there or anything like that. But yes i guess.\n\nCraig"
    author: "craig"
  - subject: "Re: KWord 1.1beta3 review"
    date: 2001-07-03
    body: "I think Koffice really is on track to being a more than adequate suite. I like Kword alot - there are still a few crashes and missing features but this is just a matter of time.\n\nGood luck to everyone involved !"
    author: "Craig"
  - subject: "Re: KWord 1.1beta3 review"
    date: 2001-07-03
    body: "Crashes ? In beta 3 ? I want to hear about them ! :)"
    author: "David Faure"
  - subject: "Re: KWord 1.1beta3 review"
    date: 2001-07-03
    body: "I had a problem with the PI sign. First I couldn't preview before printing, and then when I save to .ps and then print it out, formula with PI was not on the paper.Also, parts of the table have not been printed. Without the formula everything works just fine. One more thing,I have to write many formulas in my documents so I have to turn off anti-aliasing before using KWord because it's impossible to see any formula on the screen."
    author: "jasenko"
  - subject: "Re: KWord 1.1beta3 review"
    date: 2001-07-04
    body: "Make sure to use Qt 2.3.1 if you are using anti-aliasing.A bug in the font metrics was found and fixed, and it was the reason for the wrong placement of the formula text, when anti-aliasing was enabled."
    author: "David Faure"
  - subject: "Re: KWord 1.1beta3 review"
    date: 2001-07-04
    body: "Well I know I should have reported this to the bug page , just haven't quite worked that out yet and assumed that someone had already done so. The main problems I have encountered have been\n\n1 Tables , crashing the prrogram on insertion. I do remember tables working albeit primitively in Kword 1 - i think..\n\n2 Abiword import/export didn't function (not exactly a crash)\n\nThese are the only things I have encountered so far.\n\nRunning SuSE 7.2, KDE 2.1.2 64MB RAM\n\nGood luck with the rest of kword\n\nCraig"
    author: "Craig"
  - subject: "Re: KWord 1.1beta3 review"
    date: 2001-07-04
    body: "Well I know I should have reported this to the bug page , just haven't quite worked that out yet and assumed that someone had already done so. The main problems I have encountered have been\n\n1 Tables , crashing the prrogram on insertion. I do remember tables working albeit primitively in Kword 1 - i think..\n\n2 Abiword import/export didn't function (not exactly a crash)\n\nThese are the only things I have encountered so far.\n\nRunning SuSE 7.2, KDE 2.1.2 64MB RAM\n\nGood luck with the rest of kword\n\nCraig"
    author: "Craig"
  - subject: "Re: KWord 1.1beta3 review"
    date: 2001-07-04
    body: "Well I know I should have reported this to the bug page , just haven't quite worked that out yet and assumed that someone had already done so. The main problems I have encountered have been\n\n1 Tables , crashing the prrogram on insertion. I do remember tables working albeit primitively in Kword 1 - i think..\n\n2 Abiword import/export didn't function (not exactly a crash)\n\nThese are the only things I have encountered so far.\n\nRunning SuSE 7.2, KDE 2.1.2 64MB RAM\n\nGood luck with the rest of kword\n\nCraig"
    author: "Craig"
  - subject: "Re: KWord 1.1beta3 review"
    date: 2001-07-04
    body: "Hello craig this is also craig. You sould send your crash repots to David Faure <david@mandrakesoft.com>\n\nCraig"
    author: "craig"
  - subject: "Re: KWord 1.1beta3 review"
    date: 2001-07-04
    body: "Look guys, don't got bothering David with vague bug reports via email, use bugs.kde.org. It would be very helpful to the developers if your could send backtraces also. To enable this you need to have the GNU compiler collection installed (specifcally GDB, the debugger) and compile qt with -debug + configure kde --enable-debug. If you can't or won't do this either explain in plain clear english how to reproduce the bug or attach a document that causes the crash."
    author: "DavidA"
  - subject: "Great software, but useless for math documents."
    date: 2001-07-03
    body: "I've checked out all 3 betas of Kword 1.1.  I am really impressed by the overall quality of the software, and amazed that people are actually writing and giving away such high-quality stuff. However, as a practical matter, Kword in its current  incarnation is useless for me since the math typesetting is extremely buggy. Many characters and symbols don't show up at all, and when they show up, they displace the text around them so that the overall effect is ugly. Also many characters display on the screen, but don't print out. I am not sure whether these problems are due to user error or not. Anyway, I'm sure that the issues will be ironed out in future releases.\n\nMagnus."
    author: "Magnus Pym"
  - subject: "Try LyX"
    date: 2001-07-04
    body: "For serious mathematical typesetting nothing can realistically hold a candle to Latex. Fortunately, under linux we have LyX (www.lyx.org): it's basically a very powerful interface to Latex (even though it actually goes beyond that). You retain access to 100% of Latex's power, but ~95% of the time you can do things from the gui. For me it's the perfect technical typesetting system, and it improves every day!"
    author: "F. Perez"
  - subject: "Re: KWord 1.1beta3 review"
    date: 2001-07-03
    body: "i have problems with gifs embedded in kword documents: they show nicely on the screen, but in the print (preview) they are invisible. someone has the same problem ?"
    author: "ik"
  - subject: "Re: KWord 1.1beta3 review"
    date: 2001-07-03
    body: "i meant jpeg ... sorry (gif works fine)"
    author: "ik"
  - subject: "Re: KWord 1.1beta3 review"
    date: 2001-07-04
    body: "I have kde 2.2 beta1 (cvs)\nkoffice cvs 03/07/2001\n\nand can't reproduce the problem here."
    author: "thierry"
  - subject: "Re: KWord 1.1beta3 review"
    date: 2001-07-04
    body: "can you try this kwd file ? it triggers the problem for me"
    author: "ik"
  - subject: "Re: KWord 1.1beta3 review"
    date: 2001-07-04
    body: "it triggers another problem btw, calculated cells in an embedded kspread do not show up until you activated it once."
    author: "ik"
  - subject: "Re: KWord 1.1beta3 review"
    date: 2001-07-05
    body: "I try your document and can't reproduce the problem. File->Print preview is correct.\n\nperhaps you have kdelib 2.1.2 ?\nkdelibs 2.2 have a new library for printing.\nand I think koffice use it.\n\n\n>it triggers another problem btw, calculated \n>cells in an embedded kspread do not show up \n>until you activated it once.\nYes, already true. \nYou can submit a bug report.\n\nthank."
    author: "thierry"
  - subject: "Re: KWord 1.1beta3 review"
    date: 2001-07-04
    body: "--With the KIllustrator naming troubles it might be a good time to review all the Koffice suite names to avoid further problems. I think the naming tradition of just putting a K infront of an application type has out lived its coolness. Clever uses of the K like Kapital are still cool but Kword or Koffice should be reconsidered in my opinion. Maybe konqioffice or something like that. I'm not a developer so if I'm way off base let me know. I just think its a good time to review the naming skeem  before the final release.\nCraig Black\nICQ 103920729"
    author: "craig"
  - subject: "Re: KWord 1.1beta3 review"
    date: 2001-07-04
    body: "Renaming KOffice (which shouldn't be necessary, along with KWord), should become KDE Office, IMO.  So you have KDE, and you can add KDE Office.  Thus, a software trademark on KDE should protect both to a limited extent, and we'll be creating a namespace that somebody else won't \"stumble\" into.\n\n    KOffice joins the ranks of MS Office, Corel Office, StarOffice, Open Office and many others.\n\n    KWord fits right in with WordPerfect, WordStar, MS Word, etc.\n\n    KWrite fits in with XYWrite and others.\n\n    And for one, I really like the names.  I can't ever remember how to spell kvivo or Aethera, and they are neigh unpronouncable (in a reliable, singular way) to english speaking persons.  (BTW - if you really want to get anal with names, make them all Japanese, skipping a few characters, and you'll have easy to pronounce names for most of the rest of the world).\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: KWord 1.1beta3 review"
    date: 2001-07-06
    body: "KWrite is named Kate now, I believe, so that breaks that trend.\n\nI agree that KWord is kind of a stinky name. Eventually, it would be a lot more consistent if we didn't put K in front of the names of apps in the K-menu, this makes it easier to find apps in sorted listings. However, I also believe that it's a good idea to leave the K in the exec names, if only so that people don't have to rewrite their DCOP scripts if/when we make this change.\n\n\nKSpread and KPresenter imho have good names, simple and descriptive. They completely ignore the marketing bull and goes straight for a name that's relevant (Outlook? Powerpoint? Excel? Whatever) Krayon is good too, since it actually begins with K as opposed to just slapping it on, and it's relevant too (At least, as relevant as the name Paintbrush).\n\nAny suggestions for renaming KWord? Not that I have any power in the matter myself, mind you. Here's a few ideas:\n\n-KRichText (can't get more descriptive then that :-)\n-KType\n-KPress\n-KPage\n-Kompose\n-KIndite \n(Indite defined by WordNet as \"produce a literary work\")\n-KText"
    author: "Carbon"
  - subject: "Re: KWord 1.1beta3 review"
    date: 2001-07-07
    body: "How about\n\nKompose (Word Process)\nKalculate (Spreadsheet)\nKanvas (Illustrator)\nKrayon (Paint)\nKommunicate (EMail or Instant Messaging)\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: KWord 1.1beta3 review"
    date: 2001-07-07
    body: "Ah, neato names. I really sort of like the functional Kpresenter and kspread names myself, but Kanvas is a very nice name, especially considering that KIllustrator must be renamed for stupid legal reasons."
    author: "Carbon"
  - subject: "Renaming"
    date: 2001-07-05
    body: "There is absolutely no need to rename anything but MAYBE KIllustrator (I'm not sure even about that one).\n\"Office\", \"Word\", \"Write\" and such have been around for years.\nThat's not to say that renaming them \"KDE *****\" wouldn't be to advantage in other respects."
    author: "G\u00f6ran Jartin"
  - subject: "Re: Renaming"
    date: 2001-07-05
    body: "That would be cool as well. I just think the K in front of the application type has out lived its coolness. kdeword would be good.\n\nCraig"
    author: "craig"
  - subject: "A preview? Let's see..."
    date: 2001-07-04
    body: "The preview apparently wasn't written by a genuine beginner, but probably a well-seasoner KWord coder. Am I right?\n\nAll software hates me, so to really dumb-test a program, I need to run it just a few minutes. Let's see... KWord....\n\n1. Damn, I have Finnish language setting, but I want to tell you descriptions in English. But... Hey, KControl doesn't list English! Ok, this is not a KWord problem.\n\n2. Aww, about 40% of the screen icons are broken. Ok, I have a beta version, so this is probably expected.\n\n3. I want to start writing a typical article, with a title, and below that my name and date. I select style...\"Head 1.\" No, that's not good. \"Contents Title\", that must be it. Ok, it's centered, but has silly fancy lines. Ok, it'll do.\n\n4. Name...that's easy to add, and there seems to be an automatic date field too! Good, although the number of automatic variables is rather limited. Where are the 200 function fields? I really like those.\n\n5. I begin writing text. Ouh, the ctrl-arrow-keys don't behave as usual in word processors, nor can I select text with them (together with shift). Not nice at all.\n\n6. Actually, selection seems broken. If I start selecting a new region, it leaves the old selection there. No, it's not a multiple selection feature, but looks like a bug.\n\n7. Selecting something and erasing it by typing or with backspace doesn't seem to work.\n\n8. Let's see if there are any application settings... No.\n\n9. Ok, I'll try adding a KSpread sheet in the document. I draw the frame...what's happening? It doesn't come there. Blah, it's broken. Wait! There's something there, it's just invisible. Ok, nice, now I can add my table.\n\n10. I'll try adding a text frame. Ok, it appears, although the frame is drawn badly. I can edit it, cool. But now I want to change the indentation, but hey, the frame doesn't have its own ruler. Oh, the document ruler seems to control it, but it's just rather misleading.\n\n11. I add a table. Hmm. The screen flashes strangely. It doesn't seem to be possible to move between cells with keyboard. Ehh. Can I edit the borders of the table? ...no. Hmm, the table wants to be 100% wide, can I change that? No. Ok, tables unusable.\n\n(At this point, the screen very often gets messed up, some portions printed in two locations on screen, some portions missing altogether.)\n\n12. What the hell just happened? Most of the toolbars went suddenly empty! Ok, when I minimize and maximize them the icons come back. Phew!\n\n13. I tried to add a picture. A jpeg, anything, but the file dialog didn't want to display any image files, just directories. Perhaps the filters were invalid. A translator's mess-up, perhaps?\n\nOk, maybe that's enough for now. The program didn't crash now, although it did with my previous try. I don't remember what I did then.\nNothing really complex anyhow.\n\nTo summarize, it looks like KWord has a lot of potential, and has many nice features, but is definitely not usable yet. Stop adding features, just clean up the problems, please."
    author: "Marko Gr\u00f6nroos"
  - subject: "Re: A preview? Let's see..."
    date: 2001-07-04
    body: "An addendum:\n\nIt finally crashed when I had posted my review and hit the Quit button."
    author: "Marko Gr\u00f6nroos"
  - subject: "Re: A preview? Let's see..."
    date: 2001-07-04
    body: "Hi, \n\nAre you using the KOffice 1.1 Beta 3 version?\n\n--kent"
    author: "Kent Nguyen"
  - subject: "Re: A preview? Let's see..."
    date: 2001-07-04
    body: "Umm...no. The version seems to be a few months old. Sorry!\n\nDamn, I really should have checked the version first. Oh well."
    author: "Marko Gr\u00f6nroos"
  - subject: "Re: A preview? Let's see..."
    date: 2001-07-04
    body: "Marko, do us all a favour and download the beta 3 and then retype your post with your finding. It'll be nice to see your account of how koffice has progressed in the last few months. I'm sure you'll be surprised :-)"
    author: "DavidA"
  - subject: "Preview with the correct version, I hope..."
    date: 2001-07-04
    body: "Okay, let's see. I'll first check the version... KWord 1.1 (beta 3). Right. I hope this is correct now...\n\n1. I start writing my standard test article, and want to add a title. No title style. Actually, the number of default styles is even less than before. Ok, I format the title manually. Succeeds.\n\n2. I want to customize the keyboard a little at first, because I want to be able to change the paragraph style quickly from keyboard. I open the keyboard editor...I remove ctrl+alt+s from \"Stylist\" and give it to \"Style\" (I guess I can choose the current style with that, right?). No, doesn't work. The keyboard setting appears in the menu, but the menu won't open when I push the keys. This is probably KDE API problem, not KWord? Anyhow, it doesn't work. A pity, this is the key which I always define first in all word processors.\n\n3. I try moving the cursor. Works nicely now.\n\n4. I try selecting text. Works nicely, also when jumping words with ctrl pressed. Good. BUT, when I try to continue the selection over a paragraph break, it messes up. For example, type a line with 30 chars + enter + a second line with 30 chars. Then move to middle of first line, press shift and cursor down. Messes up. Still hold shift and now move back cursor up -> messes even more (also lines below these two).\n\n5. Copying text with ctrl+insert seems to work, but pasting with shift+insert doesn't (although ctrl+v works). I hope this isn't a problem with my global KDE settings. Anyhow, the paragraph to which I paste flashes annoyingly and is sluggish. But, it works which is most important.\n\n6. I create a table. Changing the width isn't very user-friendly, but seems to work. Moving between cells with keyboard is pain. Tab is usually standard key for moving forward, here it's the down-key. No way to move to the cell below?\n\n7. I really would like to edit table borders.\n\n8. There doesn't seem to be floating tables, where you could attach a description below. ...or a way to make auto-numbered tables or figures. You know, \"Figure 6. This is...\"\n\n9. I create a floating text frame. Seems to work, although the table I wrote suddenly vanishes when I move the text frame above it.\n\n10. I create an equation box. Umm... I can't move the cursor in without mouse. Not nice. I try to write something simple. How do I get multiplication dot? The symbol widget seems to list many LaTeX symbols, but how do I insert them to the equation? Just selecting one from the widget doesn't add it. Oh, I can type them, that's nice. Ahh, \\cdot. Ok, excellent. I think this is VERY difficult to grasp for those who haven't used LaTeX...\n\n11. I'll add a KSpread object... EEK! It got stuck! I think that the KSpread frame interacted somehow with the ordinary table I had done earlier, as they touched a little, and it went into a bizarre formatting loop. My machine slowed down, and I had to kill KWord. Restart KWord...\n\n12. I want to try to replicate the problem so I first add a table. Wait, how do I move the cursor after the table now? Not with keyboard. Ok, I'll try mouse. Umm...no. I'll try fiddling with keyboard... Hey, the table disappeared! It's now invisible, although it flickers at times. Ok, I'll try to draw another table. Umm, now the first table came visible. I'll try to replicate the KSpread problem again...no, can't. Ok, it's very sluggish, and moves the ordinary table strangely, but seems to work. I fiddle with it a bit more... Hey, now it's getting stuck. CRASH!\n\n...\n#5  0x408a280b in QObject::inherits () from /usr/lib/qt-2.3.0/lib/libqt.so.2\n#6  0x403e00dd in KMainWindow::toolBarIterator () at eval.c:41\n#7  0x403df931 in KMainWindow::finalizeGUI () at eval.c:41\n#8  0x404c4474 in KXMLGUIBuilder::finalizeGUI () at eval.c:41\n#9  0x40469686 in KXMLGUIFactory::addClient () at eval.c:41\n#10 0x40074706 in KoMainWindow::slotActivePartChanged () at eval.c:41\n#11 0x400b4080 in KoMainWindow::KXMLGUIClient virtual table () at eval.c:41\n...\n\n13. Ok, I restard KWord and try to add image frame. As earlier, the file open dialog doesn't show any image files. I think the filter settings or their Finnish translations are messed up.\n\n14. I try to add a ToC... Ok, the header comes there. But it's empty. Ah, I'll add a few Head 1 -styled lines. Update ToC...still empty. *scratches head* Umm... hey, I can't move the ToC elsewhere, very easily at least. Oh well.\n\n15. Ok, I'll finally add a few footnotes...mmmm...no footnotes? Ok... Well, I guess you could do them with superscripts manually, but...\n\nMaybe this is enough for now. I didn't go through all features, just the most obvious ones. Note that I *never* have used embedded Excel sheets in MS Word, as the ordinary tables are much nicer to use. Embedding isn't that useful, I think. I would like to see the KSpread tables not just floating, but inline, so that you can easily add a title, etc.\n\nTo summarize, the basic functionality was MUCH MUCH better now than in the previous version, and I agree that KWord can now be used for some limited production use for very simple documents. However, many important features are still buggish."
    author: "Marko Gr\u00f6nroos"
  - subject: "Re: Preview with the correct version, I hope..."
    date: 2001-07-05
    body: "> chars. Then move to middle of first line, \n> press shift and cursor down. Messes up. Still > hold shift and now move back cursor up -> \n> messes even more (also lines below these two).\n\nPlease describe a bit more exactly what you mean by \"messing up\" and send a bug report.\n\n> the paragraph to which I paste flashes \n> annoyingly and is sluggish. But, it works \n> which is most important.\n\nFlashing when pasting ?\nWhat CPU do you have ?\nI think this is fixed in the meantime :-)\n\n> 8. There doesn't seem to be floating tables, \n> where you could attach a description below. \n> ...or a way to make auto-numbered tables or \n> figures. You know, \"Figure 6. This is...\"\n\nYeah. Me too.\n\nKeep on testing koffice and send bug reports :-)\n\nBye\nAlex"
    author: "aleXXX"
  - subject: "Re: Preview with the correct version, I hope..."
    date: 2001-07-05
    body: "In your first comment, you complain about a lot of serious general issues. That is, they would have been serious if they had been true.\n\nSince they weren't, you find some not-so-general issues that might (or might not) be true. Then you concede that the program might be used for \"very simple documents\". You turn 145 degrees while trying to make it look like it was the world that turned around you :-)\n\nI'll agree though, that some operations are rather sluggish. I'm sure they will work better in the next release. There are, as well, a number of minor otherirritants.\n\nOn the whole, I'm very impressed - I'm sure that you would have been too, if you hadn't started out with a completely faulty perspective :-)"
    author: "G\u00f6ran Jartin"
  - subject: "Re: Preview with the correct version, I hope..."
    date: 2001-07-06
    body: "Yeh, I'm really sorry for using the wrong version at first.<P>\n\nI don't think that the issues I encountered were not-so-general. I was just trying to write a short technical article, with pictures and tables with a title below. Perhaps a few equations, footnotes, and references. Pretty trivial.<P>\n\nBut anyhow, I'm rather critical about software, because I have to struggle with it so often. World is full of badly working software, and some good features can't always compensate for some annoying problems. Softwares should also be easy to use, and not require half an hour to learn. The real computer-dumb people trying the softwares won't complain, they just get annoyed and give up using Linux.<P>\n\nAnyhow, I think KWord is very promising program, and may be, together with KSpread and other KOffice programs, very important for the success of the entire Linux/GNU movement.<P>\n\nI'd estimate that KWord is about 60% complete. MS Word is perhaps 95%, and usually more stabile and easier to use than KWord, at the moment. StarOffice is perhaps 90-95% complete, although it has a few nice features which MS Word doesn't have, and vise versa.<P>\n\nI usually use KLyX, which is about 90-95% feature-complete (if you count the LaTeX inlines in), for my purposes (usually writing scientific articles), but it may be hellishly difficult to use if you're not familiar with LaTeX. I like the \"Document Style\" thinking in KLyX. KWord doesn't yet have many predefined templates such as \"Letter\" or \"Book\", which would have appropriate paragraph style sets.<P>"
    author: "Marko Gr\u00f6nroos"
  - subject: "Re: Preview with the correct version, I hope..."
    date: 2001-07-06
    body: "Of course, whether the issues are trivial or not depends on what kind of work you do. I'm just a common writer (journalist) who doesn't do much technical stuff other than for fun.\n\nOn the whole, I agree wholeheartedly with your point on software in general. There is a lot left to do on KWord in that regard. On the other hand, that's part of my point, really - if you had started out with the right version, your comment would have been very different, since the software has evolved enormously from 1.0 via 1.1b2 to b3. \n(I gave up completely on 1.0 (it crashed when I looked at it) but 1.1b3 is rock steady and does more or less anything I need - that's why I'm very confident that KWord very soon will be useful even for the computer-dumb :-) )\n\nRegarding your over-all assment of the completeness of the programs, I think you're about right - I think, though, that when KWord reaches 95%, this might turn out to be quite a bit more than 95% of MS Word."
    author: "G\u00f6ran Jartin"
  - subject: "Re: KWord 1.1beta3 review"
    date: 2001-07-04
    body: "Koffice is great but I noticed 1 *major* thing that it needs. I saw that it has PowerPoint and Excel imports but I dont see any MS Word '97 imports in KWord. You definately need that before a final release."
    author: "Boris Stevenson"
  - subject: "Re: KWord 1.1beta3 review"
    date: 2001-07-04
    body: ">I dont see any MS Word '97 imports in KWord. \n\nSomething's wrong then, because KWord most certainly imports MS Word documents (97 or 2000, don't know about XP).  It's one of the most requested features."
    author: "not me"
  - subject: "Already possible !"
    date: 2001-07-04
    body: "I think you need kde 2.2 library.\nHere I can import MsWord document \n(kde2.2beta1 and koffice 1.1beta3)"
    author: "thierry"
  - subject: "Re: KWord 1.1beta3 review"
    date: 2001-07-06
    body: "If you're using kdelibs-2.1.x, just select the doc file after typing '*' in the filter combo, or add '*.doc' to the file type for application/msword in the file-type editor (kcontrol).\n\nThis should be mentionned on the website...."
    author: "David Faure"
  - subject: "How to solve problems about trademarks"
    date: 2001-07-04
    body: "Hi,\n\nI'm having some ideas about how to solve present and future problems regarding trademarks.\n1) How to pay Adobe: put a huge box in Linuxtag, near the KDE booth, to collect money. I'm sure every KDE friend will donate at least $1 !\n2) KOffice is becoming a serious office suite, if we want to compete with commercial office suites we need to register KDE program names.\nWe should register at least:\nKDE, Konqueror, KOffice and all KOffice programs.\nRegistration fees are quite expensive, but the KDE League could help (if it doesn't help KDE this time, what is good for?)"
    author: "Federico Cozzi"
  - subject: "Groupware features"
    date: 2001-07-04
    body: "How does one actually comment a document? All other actions were well documented, this one wasn't....."
    author: "Raph"
  - subject: "Re: Groupware features"
    date: 2001-07-05
    body: "If you look, it wasn't actually comments on a document, but just new frames added with the comments."
    author: "David Watson"
  - subject: "Re: Groupware features"
    date: 2001-07-05
    body: "Yeah, I thought that was a pretty sneaky trick..\n\nI think it would be a lot better if we could see some real groupware functionality... ie seperate out the backend and frontend, and allow some kind of interactive editing over the network. \nMaybe via that DCOP-SOAP bridge. Hm, CORBA sorta looks like it might have been the best choice after all. \n\nWhat I would like to see would be the ability to add comments, and also to \"lock\" an area of the document, then edit it, and this would all show up on everyones displays. \n\nI think this would need a pretty big overhaul for KParts, and DCOP would have to be marshalled over something other than X. Every program that could be embedded would also need the same split. \n\nAnyway, its a nice dream..."
    author: "Robert"
  - subject: "Re: Groupware features"
    date: 2001-07-06
    body: "The backend and the frontend are already separated, that's the whole reason for the Document/View design.\n\nHaving some sort of DCOP interface between the two, to allow remote editing.... is for KOffice 3.0 ;-)\n\nLet's get something working well in the normal case first, the design is there for such things later."
    author: "David Faure"
  - subject: "I certainly hope KSpread works"
    date: 2001-07-05
    body: "People seem to be fairly impressed with KWord.  I hope that as much effort has been put in KSpread.  It's not a serious office suite without a serious spreadsheet."
    author: "Rimmer"
  - subject: "Re: I certainly hope KSpread works"
    date: 2001-07-05
    body: "Looks like you'll get a chance to find that out on my next review. :)\n\n--kent"
    author: "Kent Nguyen"
---
Kent Nguyen has written a very nice and entertaining <a href="http://www.mslinux.com/reviews/koffice.html">review of KWord</a> (dot mirror <a href="http://static.kdenews.org/mirrors/www.mslinux.com/reviews/koffice.html">available</a>) as part of a more extensive <a href="http://www.koffice.org/">KOffice</a> examination.  With the help of an alter-ego or two, as well as some editorial guidance from Tina of <a href="http://www.newsforge.com/">Newsforge</a>, Kent covers everything from KWord frames to component embedding and groupware features, with more than a few illustrative screenshots.  Over the next few weeks, look forward to Kent's reviews of KSpread, KIllu, KPresenter, Kivio, and Krayon.
<!--break-->
