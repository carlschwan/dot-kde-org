---
title: "People Behind KDE: Frerich Raabe"
date:    2001-06-12
authors:
  - "numanee"
slug:    people-behind-kde-frerich-raabe
comments:
  - subject: "Re: People Behind KDE: Frerich Raabe"
    date: 2001-06-12
    body: "\"'s macht soviel Spass wie im Regen ein Schwarzbrot essen.\"\n\nCan anyone provide an English translation of his favorite quote? Thanks!"
    author: "kdeFan"
  - subject: "Re: People Behind KDE: Frerich Raabe"
    date: 2001-06-12
    body: "it's as interesting as eating \"black bread\" while it's raining."
    author: "INAI"
  - subject: "Re: People Behind KDE: Frerich Raabe"
    date: 2001-06-12
    body: "Something like:\n\n\"It`s as much fun as eating a black bread in the rain\"\n\nHaven`t heard of this saying before. Hope this reflects the original sentence to some extent ;-)"
    author: "Oliver Gabel"
  - subject: "Re: People Behind KDE: Frerich Raabe"
    date: 2001-06-12
    body: "indent with spaces? rotfl!"
    author: "me"
  - subject: "Re: People Behind KDE: Frerich Raabe"
    date: 2001-06-12
    body: "\"KDE - the news ticker with integrated desktop environment\"\n\nROTFL - that should be on KDE's home page!!"
    author: "Sean"
  - subject: "Re: People Behind KDE: Frerich Raabe"
    date: 2001-06-12
    body: "It's now a dot fortune cookie!\n\nCheers,\nNavin."
    author: "Navindra Umanee"
  - subject: "Re: People Behind KDE: Frerich Raabe"
    date: 2001-06-12
    body: "It's a well known fact (at least by the core KDE developers) that KNewsTicker is not only the foundation of KDE itself but also the original source of the Linux Kernel sourcecode. I were just a bit irritated that Linus took my old sourcecode without asking me, but I don't care really. It's just funny to see that a kernel which can basically only do XML parsing kicks ass. :-)\n\n(btw - I've been told a company called AT&T decided to design a whole OS on the base of KNewsTicker's scrolling code, I just cannot remember the name...)\n\n- Frerich (who apologizes for commenting on his own interview, but just couldn't resist pointing these interesting facts out)"
    author: "Frerich Raabe"
  - subject: "XFree86, it's all no fun without it"
    date: 2001-06-12
    body: "\"~ Which program would you say every KDE user should have?\n\nXFree86, it's all no fun without it.\"\n\nThis is more true than it maybe sounds. It feels that many free software projects are dependent on XFree86, not just on a X server/client. I run KDE om Solaris 7, and the X which comes with Solaris is not XFree86. I haven't managed to compile programs like kvirc due to this problem.\n\nCorrect me if I'm wrong, and if you can give me some help/advice."
    author: "Loranga"
  - subject: "IceWm Panel/Taskbar is better than Kicker :( [OT]"
    date: 2001-06-12
    body: "I am much annoyed when I can't use keybindings in Kicker menus; e.g., to access KEdit in Editors menu, I would like to use Alt+F2->E->K. keys. And the Tiny button (16x16 square)really annoys me to for clicking on it. The zooming look bizzare.\n\nIf you look at IceWm's Taskbar and its button it is configurable and Rectangular enough to be easily clicked. Themes (icepref), you would agree that Kicker doesn't look that cute at small and tiny sizes.\n\nIn short:\n\n1. Please create keybindings in Kicker menus.\n2. K button should be Rectangular and should be like IceWm for Small and Tiny sizes.\n3. Clock in IceWm also look good and has much more theme-ing capability.\n\nHope kicker will get a face lift in KDE 2.2.1"
    author: "Asif Ali Rizwaan"
  - subject: "Re: IceWm Panel/Taskbar is better than Kicker :( [OT]"
    date: 2001-06-12
    body: "1. It's possible to assign keyboard shortcuts to menu entries in KMenuEdit ( not exactly what you want though ).\n2. Hide the left hide button on Kicker and then just throw your mouse to the lower left corner of the screen. You will have no problems with clicking the K-Menu then."
    author: "Lubos Lunak"
---
 This week, <a href="mailto:tink@kde.org">Tink</a> brings KDE hacker Frerich Raabe under scrutiny in the <a href="http://www.kde.org/people/people.html">People Behind KDE</a> series.  Frerich is responsible for KNewsTicker, the Kicker applet around which, allegedly, most of KDE has been designed and implemented. For more laughs, <a href="http://www.kde.org/people/frerich.html">read the interview here.</a>
<!--break-->
