---
title: "KDE Commit-Digest for 3rd September 2006"
date:    2006-09-04
authors:
  - "dallen"
slug:    kde-commit-digest-3rd-september-2006
comments:
  - subject: "Amazing!"
    date: 2006-09-04
    body: "If there ever was an indicator of the tremendous development taking place in KDE, this has to be it. I'm totally amazed of everything going on, and I'm actually a part of it.  To an outsider it must be unbelievable.\n\nPS. Don't forget to check out KOffice 1.6beta, coming out late this week. :-)"
    author: "Inge Wallin"
  - subject: "Re: Amazing!"
    date: 2006-09-04
    body: "KOffice is amazing software. I'd love to see better MSWord and Oasis compatibility, so I can definitely discard OOo (good stuff as well, but too heavy). Keep up the good work!"
    author: "NabLa"
  - subject: "Re: Amazing!"
    date: 2006-09-04
    body: "Istn't there some MS -> Oasis conversion tool available?"
    author: "ale"
  - subject: "Re: Amazing!"
    date: 2006-09-05
    body: "I'm sure there is, but I don't want to convert my documents with one tool, then open them with KOffice. I want KOffice to do that transparently for me.\n\nExporting to a word file could be interesting, but for the most part I make a point of emailing PDF or RTF files to other users, depending on whether they need to edit it or not. Unafortunately, until Oasis support is not complete on MS Office, I cannot do it in any other way."
    author: "NabLa"
  - subject: "Re: Amazing!"
    date: 2006-09-06
    body: "Sent MsOffice users Oasis files (Koffice default format).   When they complain they can't open it, do the same thing they do to us: tell them to contact their vendor for a fix.   Or you can tell them to get a better product - depending on how they treat you, when you have problems opening their documents.   The former is prefered in general though, as the world would be a better place if Microsoft had Oasis format.\n\nThey see nothing wrong with assuming I can open documents from Microsoft Word, I don't see why we cannot take the same track, assuming they can open Oasis documents with whatever it is they use."
    author: "bluGill"
  - subject: "KOffice - resp. Krita"
    date: 2006-09-04
    body: "Krita astonishes me the most atm:\n\nwatch this:\nhttp://cyrille.diwi.org/images/kritablog/krita-perspectiveduplicate-reencoded.mpeg\n(this was my \"woah\" for today)\n\n\n"
    author: "Thomas"
  - subject: "Kickoff Looking "
    date: 2006-09-04
    body: "I know that Kickoff is a new feature, but will it be included in the next release of the 3.5 series?\n\nI'll be honest when I first saw the screenshot after visiting Beineri's blog (http://www.kdedevelopers.org/node/2283) my initial reaction was to dislike it, but I watched the video of it in action, and I've got to say I'm really excited.  It looks like a really nice way to get at your programs and data.\n\nKeep up the great work y'all."
    author: "bozy"
  - subject: "Re: Kickoff Looking "
    date: 2006-09-04
    body: "it's committed in branches/work, and that is not where kde 3.5 is developed/maintained.\n"
    author: "AC"
  - subject: "Re: Kickoff Looking "
    date: 2006-09-04
    body: "Kickoff look like a nice Start/KDE Menu for sure.\nBut that kind of menu is becoming too bloated. The desktop should be used instead of a \"huge and powerful\" floating menu.\n\nUsing the desktop only as an icon dump is a waste of a precious place on a computer environment.\n\nI hope plasma can bring what I think is the best way to use a desktop computer when not using a console shell.\n\nWhy not have all the features in the taskbar and start menu onto the desktop? It seems a natural place to play with recent documents, launching application, and widgets of all sorts."
    author: "Pierre"
  - subject: "Re: Kickoff Looking "
    date: 2006-09-04
    body: "Why put icons, or anything you might want to get at, on the desktop?  The desktop is where you put what you're working on, you can't see it or interact with it without disrupting what you are currently doing.  This is also the major problem I have with Karamba widgets, why should I have to put away my work before I can see the widgets!?\n\nSo turn off icons on the desktop, get rid of those widgets, and get back to work the pointy haired boss is coming.  :P"
    author: "bozy"
  - subject: "Re: Kickoff Looking "
    date: 2006-09-04
    body: "well, there is already a show desktop button. i'd say ditch the Kmenu, make the desktop an Application Launcher Interface with document search and stuff, and make the kmenu button just slide the apps to the sides and show the desktop."
    author: "superstoned"
  - subject: "Re: Kickoff Looking "
    date: 2006-09-04
    body: "Right click on the k-menu button, choose [remove kmenu menu] and your done."
    author: "AC"
  - subject: "Re: Kickoff Looking "
    date: 2006-09-05
    body: "The argument is speed. One click--> launch app.\n\nA icon launcher should be soemthing like Kuake. Pop up when you move to the down border."
    author: "furangu"
  - subject: "Re: Kickoff Looking "
    date: 2006-09-06
    body: "yakuake does not open when you move your mouse towards a screen edge, it opens when you hit a accelerator key (e.g. F12) You can assign accelerator keys to the icon launcher as well, so not much difference over there\n\n"
    author: "AC"
  - subject: "Re: Kickoff Looking "
    date: 2006-09-04
    body: "it's just a similar ms windows copy.....\nno alot of innovation"
    author: "Marc Collin"
  - subject: "Re: Kickoff Looking "
    date: 2006-09-04
    body: "I see this kind of complaint a lot about KDE.  \n\n...KDE borrowed idea X from desktop Y therefore KDE isn't innovative.\n\nPlease Marc, describe the novel method of accessing programs and files you have in mind.  I'm curious to hear your idea, a method that doesn't borrow from any previous desktop.  My guess is you don't have any such system in mind, you just enjoy being a douche."
    author: "asdf"
  - subject: "Re: Kickoff Looking "
    date: 2006-09-04
    body: "alt-F2"
    author: "hg"
  - subject: "Re: Kickoff Looking "
    date: 2006-09-04
    body: "alt f2 is not replaced by this menu.\nif you don't like it and want to continue using alt f2, just right click on the menu button en choose [remove]\n"
    author: "AC"
  - subject: "Re: Kickoff Looking "
    date: 2006-09-07
    body: "Yep, alt+f2 and web shortcuts are really KDE killer features, along with kio access in file-picker dialogs etc.\n\nWhat's the status of other desktops (OSX, Gnome, Win) implementing that stuff? \n\nYeah, I should use something else than KDE once in a while, just for testing but I don't. I feel guilty ;-)"
    author: "Anonymous"
  - subject: "Re: Kickoff Looking "
    date: 2006-09-04
    body: "Its more bloated than hell (XP Menu) and its clearly based on it. But a floating menu ..?!?!?! You guys are nuts!\nThings should be easy to do and easy to access - this menu is nothing of the like.\nYou cannot make critque without having a better idea? - Try Gnomes menu its much simpler - actually the thing i miss most on KDE. (I know i can edit it to suit me but thats not the point)"
    author: "josel"
  - subject: "Re: Kickoff Looking "
    date: 2006-09-04
    body: "Alway funny to see negative comments about new stuff they haven't even tried out yet (only know it from screenshots...).\n\nIf you think you can do better: go ahead, it's all opensource. The code is with you, make good use of it.\n\nOr stop whining and start using windows 95 or so...\n"
    author: "AC"
  - subject: "Re: Kickoff Looking "
    date: 2006-09-08
    body: "<p>Well, it is perhaps a case of the screenshots? show more variants an they maybe stop complaimenting, add some switch to turn it off and they will be happy..</p>I really hate that kind of stuff around<ul><li>like khtmls' nice new scrolling \"feature\", yeah its pretty but so slow that i rather switched to firefox. is there anything to turn it off? no, just thru conf file, wow, and they call it friendly ;)</li><li>Kate is broken, every f***ng file is opened in new window, broken relatively rencently, who cares about sessions? (maybe in konqueror, but not here)</li><li>kfmclient - broken (at last in mandriva)</li><li>KDM theme is the same song, i was blue in face before i found the kdmtheme package which was not installed (mandriva) but that crapy theme without posibilities of choosing user by clicking on it? no, KDM is not working (for me) since then.</li></ul>my appologies, i guess i am not in good mood today, i really like KDE, but every single bug listed there i reported or found and voted for, nothing happened pretty long time about them, i really should start to learn C++"
    author: "Martin Zboril"
  - subject: "Re: Kickoff Looking "
    date: 2006-09-08
    body: "What did i do wrong? Which tag is not mentioned at the bottom of this textarea? Do me a favor - delete it and ban Me from writing here, I will be much happier then.\nYours truly Martin Zbo&#345;il"
    author: "Martin Zboril"
  - subject: "Re: Kickoff Looking "
    date: 2006-09-04
    body: "i guess you haven't used windows recently?\nkickoff is in no way like the XP menu."
    author: "AC"
  - subject: "Re: Kickoff Looking "
    date: 2006-09-04
    body: "Where is the video?"
    author: "Rajil Saraswat"
  - subject: "Re: Kickoff Looking "
    date: 2006-09-04
    body: "I am also eager to watch that video? Where is it ? Can somebody point us to the appropriate URL ?"
    author: "Nassos Kourentas"
  - subject: "Re: Kickoff Looking "
    date: 2006-09-04
    body: "Click on the \"Kickoff\" link above, click on the screenshot."
    author: "Anonymous"
  - subject: "Re: Kickoff Looking "
    date: 2006-09-04
    body: "http://home.kde.org/~binner/kickoff/sneak_preview.mpg\nor\nhttp://home.kde.org/~binner/kickoff/sneak_preview.ogg\n\ni would like also to mention the similarity between kbfx and kickoff. It seems that they have the same goal: a new kde menu. Which isn't really clear in the post."
    author: "fabo"
  - subject: "Squirrel?"
    date: 2006-09-04
    body: "What's up with the squirrel? \nhttp://commit-digest.org/issues/2006-09-03/moreinfo/579922/#visual"
    author: "carl"
  - subject: "Re: Squirrel?"
    date: 2006-09-04
    body: "uh, that squirrel is, well, scary! Will it become the kde 4 splashscreen? :)"
    author: "me"
  - subject: "LOL"
    date: 2006-09-04
    body: "> Avoid crash if some religion celebrates easter in december\n\nHahaha. He's the best. Made my day.\n"
    author: "Martin"
  - subject: "Re: LOL"
    date: 2006-09-06
    body: "Eastern (greek) catholics for example; in couple hudred years ...."
    author: "Anonymous"
  - subject: "DVB for Kaffeine"
    date: 2006-09-04
    body: "I'm sure it's very nice that Kaffeine now has DVB plugin support, but I'd really like to know what the hell that's supposed to mean.  The Kaffeine web site has no info, and the digest contains only a list of patches with no description.  I wasn't really wanting to have to examine all of the diffs to find out what was done.\n\nI like Kaffeine, and use it a lot - for watching TV using a DVB-S card.  It works very well, even records stuff.  The channel management UI could be done a little better, but as it's only seldomly used it doesn't really bother me, and it certainly isn't as annoying as the volume stupidity, where scrolling the mousewheel up turns the volume down (and down turns it up).\n\nGiven that DVB already works, I'd really like to know what's new.  Anybody know?\n\n-- Steve"
    author: "Steve"
  - subject: "Re: DVB for Kaffeine"
    date: 2006-09-04
    body: "But getting streaming media working with Konqueror/Kaffeine is a real headache."
    author: "ale"
  - subject: "Re: DVB for Kaffeine"
    date: 2006-09-10
    body: "It means that kaffeine can now load a plugin that can do postprocessing on TS packets. There is allready a plugin released by anonymous called \"kaffeine-sc-plugin\". Wonder what it could be :)"
    author: "anon"
  - subject: "Re: DVB for Kaffeine"
    date: 2008-10-16
    body: "Hi Steve, just wondering whether you managed to resolve that issue with the reversed mousewheel behavior - it's extremely annoying to me too, mostly while I'm trying to skip forward or backwards using the mouse. I was looking for a way to customize that, but so far I haven't been able to do so. It'll be nice if I can make the single left click pause the movie too, but that's less important\n"
    author: "TL"
  - subject: "SafeSite malicious website detection"
    date: 2006-09-04
    body: "It is good to see the inclusion of phishing protection in KDE, but I'm slightly concerned by George's comments in the commit:\n\n> The choice and weight of services should be configurable \n> and they should all default to off or prompt for privacy purposes.\n\nI am worried that if George's recommendations were followed they would completely negate the benefits of the phising protection.\n\n- Users who are knowledgeable enough to configure the choice of services and weightings are those who least need phishing protection in the first place.         \n\n- Defaulting to \"off\" seems wrong for the same reason.  Those most at risk from fradulent websites (I'm thinking of PayPal and eBay clones in particular - since they are popular targets) are the same people who are least likely to be aware of KDE's built-in protection, and the need to enable it.\n\n- KDE does not need more prompt dialogs either.    \n\nI also query the reason given - \"privacy purposes\".  The risk of private information being made available to undesireable 3rd parties is much higher if I inadvertantly use a fraudulent website which is designed with the express purpose of stealing sensitive information such as credit card details than simply by making the URLs of sites I visit available to a reasonably trustworthy URL checking service - especially if efforts were made to strip parts of the URL  which may contain sensitive information (eg. query string, or even anything after the domain name).  \n\nLooking at the competition, both IE7 and FireFox enable phishing protection by default."
    author: "Robert Knight"
  - subject: "Re: SafeSite malicious website detection"
    date: 2006-09-04
    body: "I agree completely.  Those of us confident enough to not want/need it will know how to turn it off if we so desire.  Besides, I can't see how anyone would be offended if a warning kept them from being scammed."
    author: "Louis"
  - subject: "Re: SafeSite malicious website detection"
    date: 2006-09-04
    body: "As a follow up, a quick Google of \"SafeSite\" tells me that there are already some .com's and software products associated with that name :-(.  Rethinking the name now may save some confusion later.  Just a thought."
    author: "Louis"
  - subject: "Re: SafeSite malicious website detection"
    date: 2006-09-05
    body: "when a user first starts using konq they'll be asked if they'd like to enable the feature. the reason we can't enable it by default is that it compares urls to an online database, which gives people running that database a window (though they promise not to use it ;) into your surfing habits. handing out your personal information over the internet without asking you is not something we should be doing, don't you think? one question is all it takes and it covers our butts while respecting the privacy of our users."
    author: "Aaron J. Seigo"
  - subject: "Re: SafeSite malicious website detection"
    date: 2006-09-06
    body: "I see both sides...  \n\nThis should DEFAULT to on.  It is the lesser evil.  We should ask the question, but with a big \"Warning, in most cases the dangers from turning this off are greater than the dangers of leaving it on!\", if the user tries to turn it off.   (Perhaps with a full writeup?)\n\nI don't trust the companies that provide this service, but I trust them more than the places they will prevent me from going.   \n\ntrust.com (with appologies to whoever owns that site) might sell personal information, but they won't sell credit card info, or SSNs - so the damange that can be done is less than what can be done at a false paypal.   So Gullible may get some extra spam, but at least it will be for something legitmate.  Gullible will still waste money on things he doesn't need, but at least he gets something for it.\n\nMy arguement is only that this is a lesser evil.   In an ideal world we wouldn't have this discussion because nobody would attempt things like a flase paypal site.   We don't live in an ideal world."
    author: "bluGill"
  - subject: "regarding Nino's check box/icon"
    date: 2006-09-04
    body: "The tick mark is looking too close to a 'v'...\n\nIf you take a pen/pencil and make a check mark you will notice the downstroke is thick and heavy. The upstroke starts out thick but gets lighter and thins quickly to a vanishing point.\n\nFollowing Ninos artistic translucent plastic concept, how about using an Exacto knife to carve the riser to a fine point which then gets lighter in color just because it is thinner?"
    author: "Peter"
  - subject: "Re: regarding Nino's check box/icon"
    date: 2006-09-04
    body: "The way \"suggestions\" process worked in the past:\n\nStages:\n1. Oxygen crew announces that they are open to suggestions\n2. You verbally suggest something which is usually ignored.\n3. You produce actual prototyes / mockups and provide supporting supporting pros and cons.\n4. If you are lucky, your contribution is put in \"contributed by dummies\" folder on SVN\n5. You take this reaction as a hopefull sign and push some more ideas up.\n6. Some Oxygen team members become quickly annoyed, and ask you to buzz off, asking not to disturb their creative process.\n\nSave your time... Don't bother with suggestions."
    author: "Danil Suslik Dotsenko"
  - subject: "Will Kaffeine's DVB work with Phonon and avKode?"
    date: 2006-09-04
    body: "How easy will it be to port Kaffeine's DVB support to Phonon in KDE4? Specifically, I'm interested in using avKode( http://websvn.kde.org/branches/work/avkode/avkode/ ), which is a backend for the supposedly performant ffMPEG ( http://ffmpeg.mplayerhq.hu/ ).\n\nThe author of avKode says that Xine is a\"horrible interface and an error-source\", and that using ffMPEG directly enables \"more powerful features in the long run such as capture and encoding.\" See http://www.kdedevelopers.org/node/2041 .\n"
    author: "AC"
---
In <a href="http://commit-digest.org/issues/2006-09-03/">this week's KDE Commit-Digest</a>: <a href="http://www.kdedevelopers.org/node/2283">Kickoff</a>, the experimental application menu alternative developed by <a href="http://en.opensuse.org/">SuSE</a>, is imported into KDE SVN. Import of the work to support <a href="http://en.wikipedia.org/wiki/Scalable_Vector_Graphics">SVG</a> scalable tilesets in KMahjongg. KViewShell gets support for LZW compressed fax files. <a href="http://www.vandenoever.info/software/strigi/">Strigi</a> gets support for the <a href="http://www.freedesktop.org/wiki/Software/dbus/">D-Bus</a> Inter-Process Communication service, KBFX, a prospective element of <a href="http://plasma.kde.org/">Plasma</a>, gets full support for Strigi. <a href="http://kaffeine.sourceforge.net/">Kaffeine</a> gets <a href="http://en.wikipedia.org/wiki/DVB">DVB</a> plugin support. <a href="http://amarok.kde.org/">Amarok</a> sees fundamental changes in a key statistics technology, along with a name change of the technology to "Amarok File Tracking (AFT)". Development of SafeSite, a network-aware <a href="http://en.wikipedia.org/wiki/Phishing">phishing</a> protection service proceeds. Interface changes in <a href="http://ktorrent.org/">KTorrent</a>.

<!--break-->
