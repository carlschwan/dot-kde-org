---
title: "openSUSE 10.2 Released with KDE 3.5.5"
date:    2006-12-09
authors:
  - "f(apokryphos)"
slug:    opensuse-102-released-kde-355
comments:
  - subject: "Please no more distribution release articles"
    date: 2006-12-09
    body: "What a nonsense to put this on the dot!  Please, stop posting articles\nthat are about some distribution release.  There are enough other sites\nthat report about this.\n"
    author: "Richard"
  - subject: "Re: Please no more distribution release articles"
    date: 2006-12-09
    body: "I agree."
    author: "kobalt"
  - subject: "Re: Please no more distribution release articles"
    date: 2006-12-09
    body: "Is disagree. That is important news. Esp. as each major distro features a KDE derivate release which could inspire KDE original."
    author: "bert"
  - subject: "Re: Please no more distribution release articles"
    date: 2006-12-10
    body: "also, suse does a lot for KDE. it includes and pioneers things not in kde, like the faster startup patches from lubos (where in suse for sometime, until they got into kde 3.5.2) and now the new KDE menu. many suse developers work on kde, all in all i think suse is worth mentioning."
    author: "superstoned"
  - subject: "Re: Please no more distribution release articles"
    date: 2006-12-10
    body: "Oh, in that case; can I please have a dot story of my own when I buy a new cool tshirt as well?\nAfterall, I do code a lot for KDE!\n\nSeriously;\nannouncements of companies releasing (derivatives of) KDE isn't news for the dot. Really; its not."
    author: "Thomas"
  - subject: "Re: Please no more distribution release articles"
    date: 2006-12-10
    body: "> announcements of companies releasing (derivatives of) KDE isn't news for the dot. Really; its not.\n\nGood, solid argument."
    author: "apokryphos"
  - subject: "Re: Please no more distribution release articles"
    date: 2006-12-10
    body: "No its a bad argument.\n\nKDE should promote itself and spreading news about companies and distributions that have adopted KDE is part of that.\n\nSo if a distribution releases with KDE as (one of their) default desktop(s), then it should be posted.\n\nIf we don't, new users will think that KDE is losing momentum, because all distributions are moving to GNOME, according to the news on the Internet..\n\n"
    author: "otherAC"
  - subject: "Re: Please no more distribution release articles"
    date: 2006-12-10
    body: ">>Oh, in that case; can I please have a dot story of my own when I buy a new cool tshirt as well?\n\nNo, but you could announce a cool new line of kde gear at your favorite internet store.\nEspecially if that store donates some of its revenues to KDE."
    author: "otherAC"
  - subject: "Re: Please no more distribution release articles"
    date: 2006-12-10
    body: "when you release kde-code, you have a dot story (koffice), and there is people behind kde, where developers have interviews; and a dot announcement indeed.\n\nhttp://people.kde.nl/ :D"
    author: "superstoned"
  - subject: "Re: Please no more distribution release articles"
    date: 2006-12-15
    body: "I have nothing against news about distributions release, but since we are apparently talking of a distribution with a special relationship to KDE, what I'd like very much is an article about the official reaction and comments of KDE as a project to the Microsoft-Novell deal.\nThis is not an anti-KDE opinion, I'd like the same from GNOME with a little extra addon about their stand on mono, btw.\nDid I miss them?\n"
    author: "anotherAC"
  - subject: "Re: Please no more distribution release articles"
    date: 2006-12-09
    body: "Please tell me why it's nonsense to put this on the dot. As others have expressed, I find it very interesting hearing about new distros which use KDE, PARTICULARLY if they're trying out particular modifications of KDE (as openSUSE has). \n\nThe only annoying thing would be \"beta released! RC released! Final released!\" One post every 6 months (i.e. Kubuntu) or 8 months (openSUSE) about a new version of a distro with KDE is good taste, in my opinion. "
    author: "Francis Giannaros (apokryphos)"
  - subject: "Re: Please no more distribution release articles"
    date: 2006-12-09
    body: "Because it is more about OpenSuse than about KDE. In my opinion."
    author: "kobalt"
  - subject: "Re: Please no more distribution release articles"
    date: 2006-12-09
    body: "I'm not convinced that that's a valid methodology for submitting articles to the dot. For one, because openSUSE and KDE aren't distinct entities or projects: KDE is an upstream and big part of openSUSE. But more importantly, because it's not about whether an article is predominantly about KDE, but whether its content does have some connection (a *strong enough* one) to KDE. I think new Distro releases do (as I said, particularly when they announce specific innovations in KDE)."
    author: "apokryphos"
  - subject: "Re: Please no more distribution release articles"
    date: 2006-12-10
    body: "As long as *somebody* writes up a news item (like apokryphos did), *and* the Dot editors do accept and publish it (like they did), there will be published news items like this on the Dot. Period.\n\nHere is *your* chance to change this: submit your own news items, written by yourself. Simply avoid the type of \"distribution release articles\", and come forward with more interesting stuff. Be creative. \n\nNo need to take issues with apokryphos' initiative. *I*, for one, liked to read this news on the Dot."
    author: "AC"
  - subject: "Re: Please no more distribution release articles"
    date: 2006-12-10
    body: "Besides, a lot of kde people are involved in openSUSE, so news about this distribution is also news about KDE.\n"
    author: "otherAC"
  - subject: "Re: Please no more distribution release articles"
    date: 2006-12-09
    body: "If some distribution news just contains \"we packaged KDE\" I would agree. But this openSUSE release is interesting as it contains newly developed features like the start menu (which may influence KDE4's Plasma). About those the article could have contained some information though."
    author: "Anonymous"
  - subject: "Re: Please no more distribution release articles"
    date: 2006-12-09
    body: "indeed, opensuse is a strong supporter of kde, and has interesting releases with new innovative kde features.\n\nMore then enough reason to post in on the dot.\n"
    author: "AC"
  - subject: "Re: Please no more distribution release articles"
    date: 2006-12-10
    body: "it s good news every1!"
    author: "nope"
  - subject: "another unstable distro"
    date: 2006-12-09
    body: "...sad..."
    author: "anonymous"
  - subject: "Re: another unstable distro"
    date: 2006-12-09
    body: "Well, I am using it since the last beta came out on one computer. So far, I didn't find a single bug. No crashes, no nothing. So SUSE did a very well job here. Of course it helps that KDE 3.5.5 is itself very stable.\n\nAnd I love the new kicker-replacement!"
    author: "Carsten Niehaus"
  - subject: "Re: another unstable distro"
    date: 2006-12-09
    body: "> So far, I didn't find a single bug\n\nThen, you didn't work with it."
    author: "anonymous"
  - subject: "Re: another unstable distro"
    date: 2006-12-09
    body: "Obviously not. Considering you're still going under \"anonymous\" and we have no idea if you're one of the other anonymous' on here, you haven't stated any of the bugs found. So, the situation is a little ridiculous. \n\nPresuming you're the other \"anonymous\" though, it seems quite obvious that the only \"real\" bugs you might have (can't verify) you experienced are very specific. Radeon card (using particular window manager, compiz), particular Laptop. Both problems that wouldn't be experienced by everyone.\n\n> and kicker was not even replaced :o)\n\nWell, the K Menu is part of the KDE panel, kicker, and the K Menu has changed, so it's not entirely wrong at all to say that kicker has been changed/replaced."
    author: "apokryphos"
  - subject: "Re: another unstable distro"
    date: 2006-12-09
    body: "Aha... Strange analysis. What bugs did you find? I am running OS 10.2 now on 2 machines, and even though both machines ran for 6 hours now (after typing my first message) I still haven't found a bug... Following your logic I was idling for 6 hours just waiting for your reply.\nPerhaps OS 10.2 is really stable if you do things like webbrowsing, kdepim, OOo? Perhaps you should start adding real content to your replies? "
    author: "Carsten Niehaus"
  - subject: "Re: another unstable distro"
    date: 2006-12-10
    body: "It's been working fine for me. I have had many problems with many distributions, mainly with sound, but this one went perfectly so far. My favorite environment at the moment is Window Maker, though I have also installed KDE and GNOME and many others. These days, I am using one of my computers for Linux and two laptops for Windows. I'm mainly watching movies and browsing the web and writing emails--what else is there ?:)\n\nI was running 10.2 RC 2 and upgraded thru Yast. It went perfectly, to my surprise.\n\nI'm also running a variant of Ubuntu called Linux Mint that is working almost as well.\n\n"
    author: "Gene Venable"
  - subject: "Re: another unstable distro"
    date: 2006-12-09
    body: "and kicker was not even replaced :o)"
    author: "AC"
  - subject: "Re: another unstable distro"
    date: 2006-12-09
    body: "Oops, KMenu of course :)"
    author: "Carsten Niehaus"
  - subject: "Another troll"
    date: 2006-12-09
    body: "...sad..."
    author: "Anonymous"
  - subject: "Re: another unstable distro"
    date: 2006-12-09
    body: "This is actually the most stable distribution I've tried out in over at least a year. I'm very interested in finding out what's unstable for you; please do expand. "
    author: "apokryphos"
  - subject: "Re: another unstable distro"
    date: 2006-12-09
    body: "Compared to (K)Ubuntu, which crashed practically every day, whether kernel panic from the bcm43xx driver, which works beautfiully in SuSE, only the Kickoff menu has been slightly dodgy, with the drop shadows etc. but who cares.\n\nThey had done a good job compared to 10.1 which was terrible, especially with KDE compared to Gnome. \n\nNetwork Manager still doesn't work, but YAST works brill, including the package management system, no annoying dependancies on ZMD, that is a memory hog.\n\nIt is important for people who aren't knowledgeable about KDE base distributions to find out the best solution recommended, that has been lately recieved, not something outdated.\n\nIt also shows the technological achievements of the latest kde release, whether trivial bugs or otherwise."
    author: "Lparry"
  - subject: "Re: another unstable distro"
    date: 2006-12-09
    body: "What exactly does not work with NetworkManager?"
    author: "Joachim Werner"
  - subject: "Re: another unstable distro"
    date: 2006-12-09
    body: "\"Yast -> Add repositories\" didn't work for years stable. It's like gambling if the repositories are available or not\n\nmadwifi is broken if I suspend2ram my notebook\n\nxorg/xgl/radeon/compiz does have graphic errors (on fedora6, there isn't a single pixel broken)\n\nno program for the volume keys on a thinkpad notebook (like ubuntu)\n\nthat's after 1/2 day using 10.2...\n\nAnd I don't want to wait for 10.3:\n> http://www.suseblog.com/?p=168\n> --- snip ---\n> There are still a lot of bugs open for 10.2 and I\u0092m sure real usage\n> over the time will find some more. We will release via online update\n> security updates for 10.2 as usual and release also the most severe\n> bug fixes. But most bug fixes will only be done for 10.3, our next\n> release coming out next summer.\n> --- snip ---\n"
    author: "anonymous"
  - subject: "Re: another unstable distro"
    date: 2006-12-09
    body: ">>\"Yast -> Add repositories\" didn't work for years stable. It's like gambling if the repositories are available or not\n\nhmm, after 1/2 day of use, you discovered that adding repositories doesn't work for many years?\n\ninteresting ;)\nby the way, i never had any problems adding sources to yast\n\n>> madwifi is broken if I suspend2ram my notebook\n\ndoes madwifi ship with opensuse?\nThats new, since it was removed from 10.1 due to licensing issues.\n\n>>xorg/xgl/radeon/compiz does have graphic errors (on fedora6, there isn't a single pixel broken)\n\nyou mean ATI radeon?\nWell, you can blame ATI for not releasing opens source drivers and for not (yet) supporting Xorg 7.2...\n\n>>no program for the volume keys on a thinkpad notebook (like ubuntu)\n\nHmm, never needed any program to use volume keys....\n\nBut nog offering support for special keys is not the same as being unstable.\n\nonly unstable stuf in your list are about non opensource drivers that are not shipped whith opensuse.\n\n"
    author: "AC"
  - subject: "Re: another unstable distro"
    date: 2006-12-09
    body: "> hmm, after 1/2 day of use, you discovered that adding repositories doesn't work for many years?\n\n1/2 day with 10.2 ;) ... 10.0, 10.1 doesn't work stable too.\n\n> you mean ATI radeon?\n\nWith FC6 it works.\n\n> Hmm, never needed any program to use volume keys....\n\nIt's a useful feature to see the \"hardware\"-volume of my Thinkpad and not only the Software-Mixer volume from Gnome/KDE/whatever.\n"
    author: "anonymous"
  - subject: "Re: another unstable distro"
    date: 2006-12-09
    body: "> With FC6 it works.\n\nWhy? Because Fedora ships an older version of Xorg. openSUSE 10.2 was released a lot more recently, and includes Xorg 7.2."
    author: "apokryphos"
  - subject: "Re: another unstable distro"
    date: 2006-12-09
    body: ">>It's a useful feature to see the \"hardware\"-volume of my Thinkpad and not only the Software-Mixer volume from Gnome/KDE/whatever.\n\nAh, but then you should just install the application that makes this possible.\n\ni'm not familliar with a thinkpad, but i'm sure that it's shipped with an addon cd that gets thinkpad specific features running under linux.\n\n\n>With FC6 it works.\n\nWith 10.1 also, but that's besides the point.\npoint is that opensuse ships with a newer version of xorg, and that ati has not (yet) provided drivers for it.\n\nIt's a bit like installing Vista, while ati does not offer support yet.\n"
    author: "AC"
  - subject: "Re: another unstable distro"
    date: 2006-12-10
    body: "> by the way, i never had any problems adding sources to yast\n\nwell, it's really picky about how you enter the sources, the syntax has to be perfect. one / too many and it won't find anything. this has annoyed me, as it took me some time to figure out how it wanted it's sources put in. now i know, so it works, but i think it should be more smart about it. about the volume keys, well, that's more a hardware thing i guess, tough i'd love to see those work better. i have a laptop with a volume scroll thingy (how's that in english?) and it works only in windows."
    author: "superstoned"
  - subject: "Re: another unstable distro"
    date: 2006-12-10
    body: "and let me add 10.1's package management was totally broken. i spend many hours on it, but it's still a pain in the ass. if i could get rid of zmd... wow!"
    author: "superstoned"
  - subject: "Re: another unstable distro"
    date: 2006-12-10
    body: "superstoned: don't tell me that you are not aware of smart :o)\n"
    author: "otherAC"
  - subject: "Re: another unstable distro"
    date: 2006-12-10
    body: "i am, but should it not work out-of-the-box? how could they have shipped 10.1?"
    author: "superstoned"
  - subject: "Re: another unstable distro"
    date: 2006-12-10
    body: "I guess the release dude did not have enough authority to prevent pushing developers adding the unstable and new zmd into the last beta of opensuse 10.1\n\nBut that did not break the packaging system (at least not that i'm aware of), it only breaks the updater in systemtray\n"
    author: "otherAC"
  - subject: "Re: another unstable distro"
    date: 2006-12-10
    body: "Yeah, you can. There are two Software Management Patterns:\n* ZMD and friends\n* openSUSE software management\n\nNow, although ZMD has actually been fixed and is working pretty well now, you can select the openSUSE SM (which I find a little quicker) and hence you can remove ZMD (and friends) completely. It's a very easy click. Here's a screeny of the Software Management \"Patterns\":\nhttp://en.opensuse.org/Image:Screeny102_software_patterns.jpg"
    author: "apokryphos"
  - subject: "Re: another unstable distro"
    date: 2006-12-09
    body: "Erm, don't interpret that comment incorrectly. *Every* distro release has bugs; it's a case of trying to strike the balance of new software vs. stable release. I'm sure the author of that post doesn't think it's less stable than other Linux distributions that have been released; I'll wager good money on that. \n\nSUSE/openSUSE has usually been *very* good with laptops (well before decent support was added in Kubuntu); true, I've only ever tested a Dell Inspiron and a couple of Fujitsu lappys, but it always detected them flawlessly there. I hear it's sometimes worth changing the keyboard setting in kcontrol if it isn't detected appropriately.\n\n> \"Yast -> Add repositories\" didn't work for years stable. It's like gambling if the repositories are available or not\n\nThis works flawlessly now with every appropriate repository I've tried. It also worked perfectly in 10.0 for me; only problems in 10.1 (which everyone knows about). Have you got an example of a non-working repository? (without using ZMD pattern)"
    author: "apokryphos"
  - subject: "Re: another unstable distro"
    date: 2006-12-09
    body: "> no program for the volume keys on a thinkpad notebook (like ubuntu)\n\nLast time I checked both the HP Compaq and the Lenovo ThinkPad I am using had the hardware volume keys automagically enabled, even with a nice on-screen display, on openSUSE 10.2. This is supposed to work. If it doesn't, please report it as a bug and specify exactly what doesn't work."
    author: "Joachim Werner"
  - subject: "OpenSuSE is good for KDE!"
    date: 2006-12-09
    body: "The OpenSuSE team works fulltime with KDE improving it as they can, soy this article is a must have in the KDE news!!\n"
    author: "antxx"
  - subject: "Re: OpenSuSE is good for KDE!"
    date: 2006-12-19
    body: "what about kickoff, will it become part of KDE4?"
    author: "gerd"
  - subject: "Wrong link!"
    date: 2006-12-09
    body: "This is the correct one:\nhttp://72.14.221.104/search?q=cache:IgLmSxynBjcJ:www.mslinux.org/\n;-)"
    author: "Kevin Kofler"
  - subject: "Re: Wrong link!"
    date: 2006-12-10
    body: "It's now at:\nhttp://mslinux.org/\n(no more www)."
    author: "Kevin Kofler"
  - subject: "Re: Wrong link!"
    date: 2006-12-10
    body: "but not updated :(\n"
    author: "otherAC"
  - subject: "Just installed it today :)"
    date: 2006-12-09
    body: "And it works beautifully. Opensuse really puts alot of effort into these releases, and I can feel it.\n\n-Sam"
    author: "Samuel Weber"
  - subject: "nice"
    date: 2006-12-09
    body: "It's somewhat more responsive and faster than 10.1 (if you don't count the *@!_[#^ package system) and runs stable till now (since beta2). The only thing which bugs me is basket (yes I know, it's not shipped with suse). It crashes randomly and very often, but without being reproduceable. I can't get even a single useful backtrace, it's driving me nuts."
    author: "AJ"
  - subject: "Re: nice"
    date: 2006-12-10
    body: "openSUSE has packages of Basket: they are on the box DVD-9 and of course in the even bigger official FTP repository."
    author: "Anonymous"
  - subject: "Re: nice"
    date: 2006-12-10
    body: "Yes, I know. I meant it's not *installed* by default. "
    author: "AJ"
  - subject: "Re: nice"
    date: 2006-12-11
    body: "Well, you can't expect that everything gets installed by default :o)"
    author: "otherAC"
  - subject: "Re: nice"
    date: 2006-12-10
    body: "Btw, nice package search: http://benjiweber.co.uk:8080/webpin/index.jsp?searchTerm=basket"
    author: "Anonymous"
  - subject: "another unstable distro, part 2"
    date: 2006-12-10
    body: "thanks for your tips, but come on guys, please be more tolerant.\n\nIf I say it's buggy it IS buggy for me. What should I do if I want to install something and this buggy Yast-Tool doesn't want to install it???\n\n> http://img291.imageshack.us/img291/7624/verybuggyyastlr1.png\n\nit does CONTAIN the right media!!!\n\nI can access this rpm with every http program, but Yast think it doesn't exist. Fu** up. This makes me very angry. One reason is Yast and the other reason is that you think I spread lies.\nOne more reasons are people who doesn't find any bugs in Suse 10.2 and heavily use 10.2. I can't imagine this. I think they only use heavily Firefox, Konqui and one Mailer. Ok, if I only use these programs, then I don't find bugs too for hours/days/months/years... ;)\n\nAll in all, Suse 10.2 shouldn't release a distro because they can sell the boxed version BEFORE x-mas.\n\nThx & Greets,\n\nanonymous \"troll\" (if you think I'm a troll, I'm a troll. but I don't lie)"
    author: "anonymous"
  - subject: "Re: another unstable distro, part 2"
    date: 2006-12-10
    body: "http://download.opensuse.org/distribution/10.2/repo/oss\n\n\nLooks like a problem with the server, if I click on the file called directory.yast at that location i get this url:\n\nhttp://gd.tuwien.ac.at/.admin/missing/server.php?name=GD.TUWIEN.AC.AT\n\nSo it is not a problem with opensuse or yast, it's a problem with the software server you want to connect to.\n\nPlease report this to opensuse and/or use another softwareserver."
    author: "otherAC"
  - subject: "Re: another unstable distro, part 2"
    date: 2006-12-10
    body: "> if I click on the file called directory.yast at that location i get this url:\n> http://gd.tuwien.ac.at/.admin/missing/server.php?name=GD.TUWIEN.AC.AT\n\nAnd I'm getting this one...\n> http://ftp.novell.hu/pub/mirrors/ftp.opensuse.org/opensuse/distribution/10.2/repo/oss/directory.yast\n\nAnd now? This doesn't help me at the time, Yast wants to download packages.\n\n> Please report this to opensuse and/or use another softwareserver.\n\nAnd at the next day, I report the next mirror which isn't temporarly avalaible? ;)\n\nI think Suse know this problem, because the software download problem exist since the 1st implementation in Suse. And it is a really common and primitive administration task.\nIf they have a good error handling in Yast, Yast should try the next mirror and don't stop installing software.\n\nI'm getting hopeless and angry if I want to install software, but Yast gambles about installing or refusing the package."
    author: "anonymous"
  - subject: "Re: another unstable distro, part 2"
    date: 2006-12-10
    body: ">>http://ftp.novell.hu/pub/mirrors/ftp.opensuse.org/opensuse/distribution/10.2/repo/oss/directory.yast\n\nAhh, that one works :)\n\nDid you use http://ftp.novell.hu/pub/mirrors/ftp.opensuse.org/opensuse/distribution/10.2/repo/oss/ as yast source?\n\n>>And now? This doesn't help me at the time, Yast wants to download packages.\n\nis that a problem?\n\n>>And at the next day, I report the next mirror which isn't temporarly avalaible? ;)\n\nYup, the more we bug novell with non working mirrors, the more they can do about it ;)\n\n>>I think Suse know this problem, because the software download problem exist since the 1st implementation in Suse.\n\nI'm not aware of that, never had any problems with yast sources.\nsoftware management nog is completely different now then it was in the beginning: now suse looks for mirrors itself, while before it would only use the repository that you have added in yast.\nSo they are working on it ;)\n\n"
    author: "otherAC"
  - subject: "Re: another unstable distro, part 2"
    date: 2006-12-10
    body: "> thanks for your tips, but come on guys, please be more tolerant.\n\nI'm sorry but you were not being very tolerant. You simply dismissed the distro with an anonymous post saying \"it's buggy\". Possibly the most undescriptive statement around.\n\n> If I say it's buggy it IS buggy for me.\n\nOnce again, a useless statement. Since it's not buggy for me, it's not a buggy release; easy. Your screenshot there also doesn't give us enough information, and I can't reproduce it at all. If you're just ranting, then fine; but if you actually think it's a bug please feel free to open up a bug report though and then we can get to the bottom of it. \n\n> One more reasons are people who doesn't find any bugs in Suse 10.2 and heavily use 10.2. I can't imagine this.\n\nYou mentioned one bug that we (and at least I) can't reproduce. Yes, I see the bugs overflowing now. \n\n> All in all, Suse 10.2 shouldn't release a distro because they can sell the boxed version BEFORE x-mas.\n\nErr, yeah, because you know how many people get openSUSE boxes for Christmas.. It was released when it was because of a release schedule that was set very many months before. "
    author: "apokryphos"
  - subject: "Re: another unstable distro, part 2"
    date: 2006-12-10
    body: "> http://img291.imageshack.us/img291/7624/verybuggyyastlr1.png\n\nAdding this URL as repository works fine me. Try to adding directly a mirror instead the sometimes too busy redirector URL."
    author: "Anonymous"
  - subject: "Distribution news on the dot"
    date: 2006-12-10
    body: "This whole discussion about posting distribution news on the dot isn't going anywhere. And being an important one, I hope that the KDE editors read this post.\n\nIt is I think really necessary to implement criteria for a distribution post to be accepted. This \"if someone writes an article it will be accepted\" won't work. We could soon have voluntaries writing release articles about everyone's pet favourite distribution spamming the dot.\n\nSo a distribution release news article should be posted if and only if it has some news related to KDE, not just because it includes KDE 3.5.x.\n\nTaking that into account, Kubuntu's and openSUSE's news did make sense, while Fedora's was completely unwarranted. openSUSE and Kubuntu are, in that order, the distributions that are doing more KDE related work than any other. Actually employing KDE developers is among the best things you can do to support KDE. That said, not every openSUSE or Kubuntu release is interesting KDE-wise. SUSE's 9.x releases would maybe have been less news-noteworthy, KDE wise, independently of being good releases and a good distribution or not. But the last releases where related in a big way. Kubuntu has deployed a new KDE-based debian packet manager (Adept), it ships KControl's alternative System settings, including the new KControl modules from guidance, it has developed and shipped qt4 utilities like Kubuntu Device Database, the installer, etc. openSUSE, apart from the long history of supporting KDE, and its configuration utilities based on qt (YAST), has lately developed important KDE technologies or KDE frontends to new technologies like KNetworkmanager or Kerry, and this last release has an extremely important and relevant to KDE new item: Kickoff a replacement for KMenu which could influence KDE4's design, something that doesn't happen often. Those are interesting, KDE related news, worthy of being posted on the dot. Fedora's and any \"distribution X released with KDE 3.5.x\" are just metoos, kindergarten posts that spamm the dot.\n\nI think the problem with Kubuntu's and openSUSE's announcements where not properly stressing AND explaining the KDE related points. \n\nJust my 2c."
    author: "MikeT"
  - subject: "Re: Distribution news on the dot"
    date: 2006-12-10
    body: "The author of Adept is actually a Red Hat employee:\nhttp://people.redhat.com/prockai/"
    author: "Kevin Kofler"
  - subject: "Re: Distribution news on the dot"
    date: 2006-12-10
    body: "And Fedora has a very strong kde community, which justifies news about fedora on the Dot.\n\nAnd besides that, I think it is important to spread news on the dot about who's shipping KDE with their product. That way, we can show how popular KDE is.\nIf we keep silent about the spreading of KDE among distributions, and other desktop environments don't, people tend to think that we are losing momentum and might choose another product then KDE.\n"
    author: "otherAC"
  - subject: "Re: Distribution news on the dot"
    date: 2006-12-10
    body: "nearly EVERY distribution ships with KDE (some as default, some, like Fedora not). That doesn't justify turning KDE.news into a bad version of distro watch. What exactly learns someone using KDE and Fedora by reading that piece of news? don't you think they know already their favourite distribution released a new version? And what exactly learn the other dot readers? That Fedora released a new version like every 6 months? and ships with the latest stable KDE? They already know that."
    author: "MikeT"
  - subject: "Re: Distribution news on the dot"
    date: 2006-12-10
    body: ">>nearly EVERY distribution ships with KDE\n\nTrue, but still we get threads on the dot about 'kde is losing momentum'\nWe should alway continue to tell the audience that kde is popular, that kde ships with every distro on the market.\n\nlook for example at microsoft: every advertisement about computer systems reads 'vendor X recommends Windows XP'. That looks like a completely bogus recommendation, since every system that vendor X sells comes with XP as only OS installed, and allmost everyone using computers never heard of anything else but Windows..\n\n>>What exactly learns someone using KDE and Fedora by reading that piece of news?\nNothing, but the news is not solely meant for fedora users.\n\n>>And what exactly learn the other dot readers?\nThat Fedora ships KDE as well.\n\nLooks like a bogus message, but to a lot of people, it is not.\nWe need to keep reminding the audience that they can choose from a variety of inux distributions if they want a kde desktop\nIf we don't, than we will loose momentum.\nJust like microsoft keeps reminding their users that they should use Windows, even if those users were never aware that there was something besides Windows available.\n\n\n>>They already know that.\n\nThen we could just stop publishing any news on the dot, because the audience already knows about everything happening around KDE.\n\nBut don't get suprised if KDE starts loosing momenten as soon as we stop publishing news about kde and related projects..\n\n"
    author: "otherAC"
  - subject: "Re: Distribution news on the dot"
    date: 2006-12-10
    body: "Yes but that doesn't mean anything (is he doing it in his free time, for example?).\n\nWhen the next Red Hat (or Fedora) release ships, you don't say, \"mornfall is employed by Red Hat so they support KDE, it deserves to be announced on the dot\"\nWhat matters is \"does this release have anything at all particular that involves KDE apart from shipping with it?\""
    author: "MikeT"
  - subject: "Re: Distribution news on the dot"
    date: 2006-12-10
    body: "Is Mandriva still contribution to KDE? they used to be one of the bigger sponsors (and have invested a lot when they could not affod it). \n\nBTW. I agree on honesty about buggyness. I have installed Kubuntu now in 2 versions on 2 computers, and it works as a dream until something goes wrong with the wireless connection, and the SU system goes completely wrong. This is very bad as you cannot login as root. Searching on the web gives no help: many people suffer from the same problem, nobody really helps, only temporary workarounds. When I reboot 2 times, sometimes the problem is gone. The other workarounds don't even work. \n\nConcluding: the distribution is in my opinion too buggy to show to release, but all reviews are raving. \n\nGeert\n\n"
    author: "Geert"
  - subject: "Re: Distribution news on the dot"
    date: 2006-12-10
    body: "Mandriva still pays aKademy award winner Laurent Montel, who did and does an incredible amount of work for KDE 4 and KOffice 2.0."
    author: "Boudewijn Rempt"
  - subject: "Re: Distribution news on the dot"
    date: 2006-12-10
    body: "Yes I am not saying that SUSE and Kubuntu are the only distributions supporting KDE. Madriva does too. But as I said that doesn't automatically make such a post on the dot meaningful.\n If Madriva's next release does something new with or on top of KDE that not the other 100 distributions do, it will also be interesting to read about it on the dot."
    author: "MikeT"
  - subject: "Re: Distribution news on the dot"
    date: 2006-12-10
    body: "Yep, we should completely forget about mandriva and other distros that ship with kde.\nLet the competition take those distributions over.\n\nAnd when opensuse is the only distribution left that ships with a decent kde, only then it will be justified to post news about it on the dot\n"
    author: "otherAC"
  - subject: "Re: Distribution news on the dot"
    date: 2006-12-10
    body: "Oh, and about the su system: do sudo -s from konsole to get a shell and then give root a password with passwd."
    author: "Boudewijn Rempt"
  - subject: "Re: Distribution news on the dot"
    date: 2006-12-10
    body: "I publish distro news for important KDE distributions where someone submits a story that is about the KDE part of the distribution.  This is what you seem to be describing above and this openSuSE story and the recent Kubuntu one both fall into this description.  The Fedora story doesn't, and shouldn't have been published in my opinion, but other dot editors have their own preferences.\n"
    author: "Jonathan Riddell"
  - subject: "Re: Distribution news on the dot"
    date: 2006-12-10
    body: "Yes we agree. As I said, I think it was a communication problem. Not everyone will  find it extremely interesting that Kubuntu or openSUSE  ship a new version of the distribution with KDE3.5.x. But how about explaining those new nifty qt4 apps or frontends that Kubuntu now has? like the laptop suspend applet, the Device database initiative and the new Guidance modules with a System settings redesign? And what's new in Kerry and KNetworkmanager (which distributions like Kubuntu are evaluating as default for the next version), and some more Kickoff news (as it was already presented in an older dot article, I think).\n"
    author: "MikeT"
  - subject: "Re: Distribution news on the dot"
    date: 2006-12-10
    body: "So the more a distribution derives from the official kde releases, the more it gets justified to get posted on the dot?"
    author: "otherAC"
  - subject: "Re: Distribution news on the dot"
    date: 2006-12-12
    body: "not all derivations are bad. i can't speak about kickoff, haven't yet tried it - but suse has been shipping for a while kde with kicker menu search. it's awesome, it's useful - and from my viewpoint, should have been in default kde installations a long time ago.\n\nalso, such derivations allow for some real life testbeds, so if a feature is demed good enough, i guess it can be implemented in main kde :)"
    author: "richlv"
  - subject: "Nicer Release Page"
    date: 2006-12-12
    body: "Prettier release page with an overview:\nhttp://en.opensuse.org/OpenSUSE_News/10.2-Release"
    author: "apokryphos"
---
openSUSE 10.2, formerly known as SUSE Linux 10.x, <a href="http://lists.opensuse.org/opensuse-announce/2006-12/msg00004.html">has been released</a> with KDE 3.5.5 and KOffice 1.6 (Krita is installed by default). As well as <a href="http://en.opensuse.org/Product_Highlights">the usual latest free and open source software</a>, openSUSE comes with the new <a href="http://en.opensuse.org/Kickoff">KDE Menu "Kickoff"</a>, integrated with the latest <a href="http://en.opensuse.org/Kerry">Kerry Beagle</a>. Some screenshots are available <a href="http://en.opensuse.org/Screenshots/Screenshots_openSUSE_10.2">on the openSUSE wiki</a>.



<!--break-->
