---
title: "KDE Libs Hackers to Meet for KDE Four Core"
date:    2006-06-26
authors:
  - "tmacieira"
slug:    kde-libs-hackers-meet-kde-four-core
comments:
  - subject: "Suse"
    date: 2006-06-26
    body: "There is no doubt that Suse still support KDE :)"
    author: "JC"
  - subject: "Novell & KDE"
    date: 2006-06-26
    body: "They'd better do that, because a lot of users (at least in the home/desktop area) prefer SUSE exactly because of the good KDE support this distro offers.\n\nNevertheless, with 10.1 I have to say that quite a lot of ex-Ximian techologies become core components of SUSE. Which is, IMHO, not always desirable. You have to install more and more Gnome/Mono/whatever stuff. And not all that technology is really ready for the real world (see the desaster of the Red Carpet based packet manager stuff in 10.1).\n\nTherefore I'm really glad, as a devout SUSE user ;-)), that they still go strong for KDE."
    author: "AC"
  - subject: "Re: Novell & KDE"
    date: 2006-06-26
    body: "In 10.1 you can use all YaST package manager parts (including online update) without having Mono-based zmd installed/running. 10.2 will have again an update notification applet which doesn't require zmd (either susewatcher-zypp [1] written by Duncan Mac-Vicar or [2] with libzypp backend) and likely also non-Mono using sophisticated package management command line utilities.\n\n[1] http://lists.opensuse.org/archive/opensuse/2006-Jun/0526.html\n[2] http://en.opensuse.org/KDE_ZMD_Updater_Applet "
    author: "Anonymous"
  - subject: "Re: Novell & KDE"
    date: 2006-06-26
    body: "great, as all new (gtk and/or mono based) tools in Suse 10.1 just plain suck. the update applet only gives some errors, beagle is just dog-slow - i wish Novell would just fire all gnome-related hackers. It is a waste of money, I think suse 10.1 clearly shows that. They did nothing to integrate the new tools with Yast, and are duplicating loads of work. Sorry, but its just plain silly."
    author: "superstoned"
  - subject: "Re: Novell & KDE"
    date: 2006-06-26
    body: "And the bad thing about it: you'r right!\nPackage-management is unstable and extremely slow. To slow for productive usage. (I don't recommend openSuSE 10.1 for this reason)\nBeagle also is slow. Although if beagle get's some fine-tuning, it may become better.\nI also tried banshee. Well - a lot of hype it seems. Although it's a nice application, it's speed is bad also.\nSo my conclusion is that mono-based applications currently tend to be slow..."
    author: "birdy"
  - subject: "Re: Novell & KDE"
    date: 2006-06-30
    body: "You're dead right I'm afraid. Look at the Google Summer of Code person doing a KDE applet for ZMD. He was suitably impressed by what kdelibs had in there, and neither Novell or Suse or him needed to put any effort into doing that.\n\nLook at the resources that Novell are expending on Mono, GTK, low-level Gnome components and applications as well and you can tell how it's all going to end. It's like Microsoft with open soyrce software. Granted, it's Novell's choice, and what their managers get flashed in front of their faces, but it is merely one of the milestones that will sink them (and Suse unfortunately)."
    author: "Segedunum"
  - subject: "Re: Novell & KDE"
    date: 2006-06-26
    body: "I tend to agree with superstoned, though it looks pretty much like flaming if you post such a opinion online ;). So I personally wouldn't use hihs harsh words...\n\nIMHO all this Ximian stuff looked great on the paper, but the technical merits never quite matched their marketing. SUSE 10.1 (and, to state that, I don't bash the distro as a whole, I am still a SUSE fanboy ;) was a proof for this.\n\nI am glad to hear that a) SUSE still backs KDE and b) that some design decisions seem to be re-evalued.\n\nNevertheless, there is still some Gnome Bias in SUSE, if you see, how many Gnome/GTK-dependencies some packages require. (Example: If you want to try out the \"desktop agnostic\" xgl, you get a whole Gnome install in dependencies)."
    author: "AC"
  - subject: "Re: Novell & KDE"
    date: 2006-06-26
    body: "'Example: If you want to try out the \"desktop agnostic\" xgl, you get a whole Gnome install in dependencies'\n\nWell, in their defense here, there's really no point in trying out xgl unless you have a window/composite manager that takes advantage of it. So far, the only such program is compiz, which has been built with the Gnome desktop in mind. All the configuration is done through gconf, which is Gnome, and requires Gnome apps if you want to configure it well. Further, the only working window decorator is the gnome one.\n\nThere's no way to test drive Xgl in a purely KDE setting, at least as of yet."
    author: "Dolio"
  - subject: "Re: Novell & KDE"
    date: 2006-06-26
    body: "> (Example: If you want to try out the \"desktop agnostic\" xgl, you get a whole Gnome install in dependencies).\n\nThat's a simple packaging problem: just move gnome-xgl-settings (which pulls in gnome-control-center and hence \"the whole GNOME\") into a compiz-gnome sub-package. Such splitting has be done before, like for Beagle in 10.1."
    author: "Anonymous"
  - subject: "Re: Novell & KDE"
    date: 2006-06-26
    body: "This might be true. To be honest, I really don't need XGL - I just wanted to play :-).\n\nHowever, in many cases I have the strange feeling that nowadays I'm overly forced to install Gnome components with SUSE (no Gnome bashing intended - I just made the decision to use KDE). When I go through the list of installed packages I always wonder \"why do I need gnome-vfs and stuff like that\"? (I guess it's often because of OpenOffice, to be sure.)"
    author: "AC"
  - subject: "Re: Novell & KDE"
    date: 2006-06-26
    body: "And AIGLX?"
    author: "Jim Der"
  - subject: "Re: Novell & KDE"
    date: 2006-06-26
    body: "well, it was harsh, but if you've been seriously bitten by these things... I expect more from suse. they HAD a great track record, until Ximian chimed in - at least, that's how it seems to me."
    author: "superstoned"
  - subject: "Re: Novell & KDE"
    date: 2006-06-26
    body: "Yeah, that is what is my problem, too:\n\nI was used to buy, yes, really buy, every single version of SUSE since 5.something. For this money (not too much money, admittedly) I got a very well designed, smoothly running distribution, which - for my use - was always better than all other distros on the market at the given time. All was coherent, I could always laugh at the kind of Unix-archaic problems users of other distros had - and KDE was always the default and a good choice.\n\nBut SUSE 10.1 fails to fulfill the quality criterias that I'm used to apply to SUSE distros - and disturbingly, mostly it is because of the integration of Gnome/Ximian technologies.\n\nThis is no flaming, this is the way I see it as a longtime SUSE user. And, but this is another flamewar, I would gladly pay a few bucks every half year to get the (controversially semi-free but always ahead-of-the-pack) SUSE distribution of old times back :(."
    author: "AC"
  - subject: "Re: Novell & KDE"
    date: 2006-06-27
    body: "I completely agree. It seems they threw out the &#8220;if it ain't broke, don't fix it&#8221; for politics. Sad, as SuSE was about technical workmanship before politics :( Maybe (I hope) they'll correct their course for the next release. \n<troll>If I learnt that those ximian employees were looking for jobs, I'd feel something between relieved and happy...</troll>"
    author: "hmmm"
  - subject: "Re: Novell & KDE"
    date: 2006-06-27
    body: "> Sad, as SuSE was about technical workmanship before politics :( \n\nYeah, and Ximian is all about politics. Not a good match. I've tried to use every single version of Evolution for almost 10 years now and i have yet to see it work for 15 minutes of real life use. Where's the quarantee that given 10 more years it would work at some point? What is the amount of $$ that will make it work? Will it ever make it without some serious G rework? Some things in Kontact are arguably more arcane to use, but it generally works well enough for daily use [ with exchange connectivity being the biggest usability concern ] no matter which distribution and/or setup you run it in. If Kontact would only have had fraction of the money that evo has had..\n\nThat said, all comments here about SUSE 10.1 seems perfectly valid to me - luckily, 10.1 is the first SUSE distribution in years I didn't pay for, as it wasn't too hard to see what was coming. It has been extremely annoying to hack around all the zmd issues it has.\n"
    author: "Anonymous Coward"
  - subject: "Re: Novell & KDE"
    date: 2006-06-30
    body: "\"I've tried to use every single version of Evolution for almost 10 years now and i have yet to see it work for 15 minutes of real life use.\"\n\nUmm, it was only started at the tail end of 1999.\nEver think that the problem is not Evolution, but yourself? Many people have no problem getting it to work for more than \"15 minutes of real life use\". You clearly just suck."
    author: "bob"
  - subject: "Re: Novell & KDE"
    date: 2006-06-30
    body: "\"Umm, it was only started at the tail end of 1999.\"\n\nAlmost ten years.\n\n\"Ever think that the problem is not Evolution, but yourself? Many people have no problem getting it to work for more than \"15 minutes of real life use\". You clearly just suck.\"\n\nDepends. Circa 2001 Evolution was just about OK as an e-mail client, because I used it full time. However, the way that it's been expanded since then to incorporate things like groupware functionality has been a bit of a disaster. The software just doesn't have the ability to grow gracefully. It's always running on empty."
    author: "Segedunum"
  - subject: "Re: Novell & KDE"
    date: 2006-07-01
    body: "So, go write an email to Novell and tell them.\n\nTell them, that as an long time costumer, a paying long time costumer, you are disgusted by the way Novell treats KDE and how the Ximian stuff destroyed the reasons to use Suse.\n\nBe open! Tell them!\n\nIf you don't they will continue with that. And we really don't need another gnome-centered distribution.\n\nIf I want to be treated like a noob or eternal noob or idiot (nothing wrong with noobs, but gnome's target audience does more and more look like the 'I need instructions to find my ass' crowd) I can use ubuntu..."
    author: "nrgman@gmx.de"
  - subject: "Re: Novell & KDE"
    date: 2006-06-27
    body: "But please note:\n\n\"_This project is not supported by Novell_\"\n\nSo the community tries to iron out Novell's bad decisions... which is good, but it would be better if Novell would make better decisions..."
    author: "AC"
  - subject: "Re: Novell & KDE"
    date: 2006-06-27
    body: "It's not supported at the moment, but who knows what will happen? Duncan is a Novell employee and the Google SoC projects are of course also mentored by Novell employees."
    author: "Anonymous"
  - subject: "The default is Gnome"
    date: 2006-06-27
    body: "They aren't going strong for KDE\n\n\"When companies come to Novell and say, 'I want to build a fantastic KDE application' on SUSE Linux, we say, 'that's great'. When they want to build a Gnome application, we say that's great again. But when companies come to us and say 'what should I do?' now we can give them them answer. Before we couldn't - we'd say 'you choose' and that's not really helpful. So now we can the default is Gnome.\"\n\nhttp://www.linuxformat.co.uk/modules.php?op=modload&name=News&file=article&sid=326\n\n"
    author: "ac"
  - subject: "Re: The default is Gnome"
    date: 2006-06-27
    body: "The interviewed is a monkey aka Ximian employee, they have been known to lie about Novells GNOME commitment in the past."
    author: "Carewolf"
  - subject: "Re: The default is Gnome"
    date: 2006-06-27
    body: "\"The interviewed is a monkey aka Ximian employee, they have been known to lie about Novells GNOME commitment in the past.\"\n\nWell, that's really your problem if you want to lie about the reality of the situation.  The guy is director of Marketing for Open Source and Linux at Novell and that's official company policy."
    author: "ac"
  - subject: "Re: The default is Gnome"
    date: 2006-06-27
    body: "> The guy is director of Marketing for Open Source and Linux at Novell\n\nSmall correction: he WAS director of marketing for open source and linux at novell. I think he left some weeks ago already."
    author: "monkeypoo"
  - subject: "Re: The default is Gnome"
    date: 2006-06-27
    body: "Nevertheless, he said this words as an official representative. So his statements don't loose their meaning just because he quit. As long as there is no other official representative of Novell saying something else, they are official company policy."
    author: "AC"
  - subject: "Re: The default is Gnome"
    date: 2006-06-29
    body: "But since he quit, it seems as if his own views did not match those of Novell as a whole."
    author: "ale"
  - subject: "Re: The default is Gnome"
    date: 2006-06-30
    body: "As nobody knows why this guy quit (or was quit), your conclusion is not necessarily right, but only one of several possible reasons."
    author: "Lcc"
  - subject: "Re: The default is Gnome"
    date: 2006-06-30
    body: "And a fat lot of good it's going to do Novell as a whole, attempting to make that kind of politically motivated decision over a desktop that doesn't amount to a hill of beans in their current circumstances. And let's face it, it's not based on technological merits. It's based on politics."
    author: "Segedunum"
  - subject: "Re: Novell & KDE"
    date: 2006-06-28
    body: "One other thing to note with regards to Novell's involvement with KDE is that all of the PR material on Novell's web-site about Suse Linux Enterprise Desktop is gnome-based as is all the documentation.\n\nThey have basically relegated KDE to the backburner. I think they are sincerely hoping that it will just go away. I have deployed KDE countless times and know that this decision is a politically motivated move and has little to do with the overall software quality of the KDE project. In fact, it is sad that the Ximian guys seem hell-bent on destroying KDE out of spite. In the recent past, I attended a free software lecture by Miguel de Icaza in Barcelona only to hear him spout venom for over half an hour about how terrible KDE was and how Gnome was the BMW desktop and KDE was a Yugo.\n\nReally freaking sad! I think communities are built on trust and I simply do not trust these guys anymore."
    author: "Gonzalo"
  - subject: "Re: Novell & KDE"
    date: 2006-06-29
    body: "All the documentation? Please check your 'facts': http://www.novell.com/documentation/beta/sled10/"
    author: "Anonymous"
  - subject: "Re: Novell & KDE"
    date: 2006-06-29
    body: "I don't know who is trolling here...\n\nAs it has been said again and again, Novell has NOT (yet?) banned KDE. So your link showing some KDE related documentation doesn't prove anything at all. (You probably will even find e.g. lpd-docs somewhere on the Novell site, even if they have long switched to cups as printer spooler. Thus the availabilty of documentation doesn't mean anything at all.)\n\nBut fact is - and are you a SUSE user? - that they send out very mixed signals concerning the future course of their distro. And sometimes there are pretty big gaps between some official statements and the reality in the current distro."
    author: "AC"
  - subject: "Re: Novell & KDE"
    date: 2006-06-29
    body: "Can't there be a single posting without your drivel following up? :-("
    author: "Anonymous"
  - subject: "Re: Novell & KDE"
    date: 2006-06-30
    body: "Well, as you pick out single, isolated points of other people's complaints and try to invalidate them with arguments (which mostly unfortunately miss the point and don't prove anything), that doesn't seem to surprising to me.\n\nIf you completely forget about this discussion here, you might nevertheless have noticed that on IRC, mailing lists, news sites and so on the number of complaints on the way SUSE is heading has risen _significantly_ since Novell took over and especially since 10.1 has been released. Much more than the usual, unavoidable \"SUSE sux, everything was better in former times\" noise level. The noncritical \"SUSE rulez\" fanboys, on the other hand, have become a bit more hushed.\n\nAnd concerning the point that one Ex-Ximian employee has left Novell: You might have noticed that all the times people leave companies without this affecting the corporate policy. And even if this is so, I can very well remember that also quite a number of veteran SUSE hackers have left Novell in the last months..."
    author: "Lcc"
  - subject: "Re: Novell & KDE"
    date: 2006-06-30
    body: "\"They have basically relegated KDE to the backburner. I think they are sincerely hoping that it will just go away.\"\n\nFor Icaza, I'm afraid not. The desktop situation is not going to change, just like it hasn't for the past six or seven years. In fact it has changed - KDE has got more popular. They may want to jump on this enterprise desktop fantasy world they've perpetually been in, but there it just isn't going to happen.\n\n\"In the recent past, I attended a free software lecture by Miguel de Icaza in Barcelona only to hear him spout venom for over half an hour about how terrible KDE was and how Gnome was the BMW desktop and KDE was a Yugo.\"\n\nHe's done it in lots of places, particularly those that might threaten the crap he's come up with. The DotGNU project has had many such run-ins with him. It really is very, very said, and when you put your side of something he then tries to cast you as a bit of a zealot and that he's somehow 'above it all'. Very sad, and a very strange individual, along with his mate Nat Friedman (and he's attracted a fair share of criticism from many quarters - http://natslies.blogspot.com/). Always thought Icaza was strange, and having seen him live a few times I now know he is. Wouldn't like to discuss anything or work with him."
    author: "Segedunum"
  - subject: "SuSE seems without direction."
    date: 2006-06-27
    body: "I am a SUSE user.  I have been a SUSE user since version 6.1.  I use KDE.  I like it when KDE is supported by companies selling Free Software and related solutions.  I am therefore pleased by Novell choosing to support the KDE Four Core conference, even if the decision came from within the SUSE division.  I am a bit surprised and confused by Novell's decision to publically declare Gnome the default desktop on their new SUSE Linux Enterprise Desktop and accross many press releases, and then to maintain the employment of several KDE related developers and promoting KDE conferences.  I am not at all willing to be anything but thankful for the support of KDE.  I do wish however that what was being said by Novell representatives was clarified a bit in regards to KDE and Gnome, especially when compared to behavior.  \n\nAs for SUSE 10.1, I feel it has some very rough edges.  The package installation capability as available by default was quite bad by most accounts.  I agree with those sentiments.  SUSE 10.1 though was an unofficial beta.  It was the beta for the upcoming SLED 10.  SUSE 10.1 provided Novell a great platform to test package management and upgrade changes, XGL, several of the new Gnome applications (banshee, F-Spot, ...), etc.  Considering the ammount of heavy development going on at Novell around Linux, this was a neccessary step to ensure that the new SLED will be most likely to meet expectations in the enterprise.  Also, remember that this was the first version that was done within the openSUSE system from start to finish.  Right now the openSUSE team is working to complete the build service which will make the entire process more versitile and robust, but I'm sure there will be issues along the way.  All of this is about taking risks and growing.  \n\nNow to Trolltech, I say thank you.  Thank you for QT and your other projects.  Thank you for sponsoring KDE and KDE conferences.  Thank you for paying the wages of KDE developers (Aaron Seigo comes to mind).  \n\nTo everyone not listed in my above rant who works on KDE and will be at KDE Four Core, thank you for your efforts, have a great conference.  Blog please.\n\nJonathan Lee\n-aodhagan "
    author: "aodhagan"
  - subject: "Re: SuSE seems without direction."
    date: 2006-06-27
    body: "Yes, openSUSE ist considered a beta version for the enterprise SUSE. That's OK for me. But they should clearly label it as such.\n\nI installed 10.1 at a friend's computer, who wanted to try out Linux, and I naively trusted that 10.1 would simply  be a logical improvement over 10.0. The mess with the package management and other inconsistencies (NetworkManager, Beagle making the machine unresponsive etc.) drove my friend - no Linux geek obviously, but otherwise quite experienced with computers - away from Linux within a few days (OK, at least to MacOSX and not back to XP ;).\n\nConsidering that many users, espacially in Europe, might try to switch to Linux with the traditionally userfriendly SUSE distro... Novell is on the best way to destroy the reputation of SUSE. Lection: You definitely can't roll out a distro with a completely f***ed up package management! That's a core component! \n\nI don't have the link here, but I remember a Novell bugzilla entry about one of the many package management related bugs, where the developer in charge closed the bug, because he could \"only\" reproduce it on one out of his five (or such) development computers. This sloppy attitude is exactly what SUSE did not have in former days.\n\nProblem is, that Ximian guys are running the Novell Linux department. Hey, if Novell is heading for the enterprise desktop, that's OK and probably a wise decision, but why the hell is e.g. a new MP3-player (banshee) relevant for this strategy? Why are ressources wasted for something like that? Quite obvious because somebody wants a alternative to KDE's amarok. But a MP3 player has nothing to do with enterprise desktops.\n\nJust to add to the rant... but I'm hoping that either SUSE comes back to senses (you might see this sponsoring of the KDE meeting as a sign) or Kubuntu is able to take its place..."
    author: "AC"
  - subject: "Re: SuSE seems without direction."
    date: 2006-06-27
    body: "> Yes, openSUSE ist considered a beta version for the enterprise SUSE. \n\nIt's not. And for sure not because SUSE Enterprise is based on every third or fourth release of it.\n\n> Problem is, that Ximian guys are running the Novell Linux department.\n\nI doubt that. Former Ximian managers like David Patrick have already left Novell."
    author: "Anonymous"
  - subject: "Re: SuSE seems without direction."
    date: 2006-06-27
    body: "> It's not. And for sure not because SUSE Enterprise is \n> based on every third or fourth release of it.\n\nMaybe an Alpha then?\n\n> I doubt that. Former Ximian managers like \n> David Patrick have already left Novell.\n\nOthers haven't, so your point is right but inconclusive."
    author: "aac"
  - subject: "Re: SuSE seems without direction."
    date: 2006-06-27
    body: "> Maybe an Alpha then?\n\nPlease troll elsewhere..."
    author: "Anonymous"
  - subject: "Re: SuSE seems without direction."
    date: 2006-06-27
    body: "c'mon, it's sad, but he is right. After upgrading my suse 10.0 laptop, things got worse in almost every aspect. I've not been able to install any software, the upgrade tool gives cryptic errors. Sorry, but 10.1 is a disastrous release, while 10.0 was already worse than the 9.x series."
    author: "superstoned"
  - subject: "Re: SuSE seems without direction."
    date: 2006-06-27
    body: "I don't know if you are a SUSE user. Only in this case you could maybe take part in this discussion, if not, it is you who is trolling...\n\nI think this discussion is not misplaced here, as this dot news article once again shows Novell's strange ambiguity concerning the SUSE distribution. Not only, but especially, concerning the desktop question. Go for Ximian/Gnome, but give the KDE community some hope sometimes in order not to drive away all the old customers maybe?\n\nMany long time users of SUSE are simply wondering what is going on. Considering the fact that SUSE was always the distro I could wholeheartedly recommend, recent releases have been significant steps backward. Not only because of the desktop question, but also in terms of software engineering quality.\n\nSometimes it seems like only some committed Novell employees keep alive not only KDE, but also the traditional SUSE spirit, against corporate policy. Clarification of this impression still pending."
    author: "AC"
  - subject: "Re: SuSE seems without direction."
    date: 2006-06-27
    body: "> Clarification of this impression still pending.\n\nI couldn't agree more. Facts with SUSE are that:\n- any sense of direction seems completely missing (marketing gives mixed signals and does NOT indicate what the company is aiming for)\n- software engineering quality has dropped significantly\n- amount of software produced has went through the roof (they try to do everything and the kitchen sink)\n\nThis sure sounds like a best possible way to destroy a company. Given a mess like this, why would anyone buy SUSE? What is it good for?\n"
    author: "Anonymous Coward"
  - subject: "Re: SuSE seems without direction."
    date: 2006-06-27
    body: "> I don't know if you are a SUSE user. Only in this case you could maybe take part in this discussion\n\nAs you can see I can take part in this discussion without you knowing who I am and without your accreditation.\n\nIf you don't understand the \"two desktops, GNOME being the default\" message I cannot help you. My guess is that you would rather like to see KDE development, shipping and support stopped at Novell to see your conspiracy theories proven."
    author: "Anonymous"
  - subject: "Re: SuSE seems without direction."
    date: 2006-06-27
    body: "As a SUSE user, following the relevant mailing lists, you would know what the discussion is about. So I didn't intend to insult you, I just wanted to point out that the core of the discussion is maybe better understandable for SUSE users.\n\nIt's not only the desktop question, it's the general lack of coherency and quality of SUSE since Novell took over. As a longtime user of SUSE one has become accustomed to quite high standards. OK, in every release there where one, two minor annoyances (mostly fixed soon thereafter), but recent versions show a really dramatic decrease of quality (still better than many others, though).\n\nConcerning conspiracy theories: Well, the citations of Novell managers have been posted often enough, even here in this thread. Reading them leaves the users pretty disoriented as how to Novell will pursue its distribution. That's no conspiracy, that's fact. And based on the facts, the average annoyed SUSE user is allowed to make his speculations as to what the reasons for this chaos are..."
    author: "AC"
  - subject: "Re: SuSE seems without direction."
    date: 2006-06-29
    body: "I think he ( and I) would like to see KDE as the default, as it used to be, when Suse was a REAL distro ( and arguably, the best).\n\nTo address your previous assertion of the ximian mgmt having left, I think he was referring to the fence-crossers. They are still there and are converting everyone else.\n\nI think you are either too stupid to know the truth or you know it and are trolling KDE boards. I have met RH developers ( I live in Raleigh, NC) - I know how they feel about KDE and what RH and other gnome proponents would like to do to KDE. I have already made up my mind - if Gnome is the sole desktop remaining on linux, I would rather use windows.\n\n"
    author: "vm"
  - subject: "Re: SuSE seems without direction."
    date: 2006-06-29
    body: "And I think that the guy behind \"AC\", \"ac\", \"aac\" and \"Anonymous Coward\" is in reality a developer of a with SUSE competing distribution..."
    author: "Anonymous"
  - subject: "Re: SuSE seems without direction."
    date: 2006-06-30
    body: "who cares ? At this time, I wouldn't worry about competition among linux vendors - the opportunities are plenty and they haven't even scratched the surface.\n\nI think that the problem with linux on desktop is that the US morons have banded together to push the $hitty gnome over KDE for years. Eventually gnome might become 'good enough' but OS-X and windows would have moved on. No customers are clamouring for gnome or even linux on desktop. The intertia and momentum heavily favours windows. Heck, even apple hasn't been able to capture a bigger slice of the desktop - do you think people give a $hit about gnome, or for that matter, KDE ?\n\nThere MIGHT have been a chance for linux if, 5 years ago, RH, Sun and others threw their resources to make KDE better. Perhaps, even buy QT and make it O.S. as they did with open office. But stupidity seems to reign rule among m$ competitors."
    author: "vm"
  - subject: "Re: SuSE seems without direction."
    date: 2006-06-30
    body: "Would the comments be more plausible to you if they would have been posted as \"Paris Hilton\", \"Linux Torvalds\", \"Albert Einstein\" or so ;)? Wouldn't make any difference, because anybody can take on any name to post here, so you could consider taking the nick \"AC\" more honest... ;). \n\nApart from that I think your conspiracy theory is complete and utter nonsense. I'm trying very hard to image Redhat, Ubuntu, Debian and Mandriva software engineers spending their time at dot.kde.org in order to smash SUSE - but I just can't ;). \n\nAnother possibility is that the complaints here about the direction Novell is pushing SUSE conatin some grain of truth?!"
    author: "Another AC"
  - subject: "Re: SuSE seems without direction."
    date: 2006-07-01
    body: "As you say, anyone can pick the name they want and post with it... and obviously it is more honest a true AC that a false \"Bill Gates\"... so here I am... feeling the rechiest man in the world for a sec or two... BTW... I will take advantage of this opportunity and tell everybody a little secret... M$ (The company that I rule) sucks... we keep on top just because we have a LOOOOT of money, we pay hardware folks to make things as obscure as possible for you linux folks and to make almost impossible to get any piece of hardware working on Linux... And remember, we pay a GOOOODDD deal of money to Corel so they stop the development of CorelDRAW suite for linux!!!!\n\n\nJejeje, I RULE THE WORLD!!!!\n\nPD: To probe my identity, I attach a photo of myself! :P"
    author: "Bill Gates"
  - subject: "Re: SuSE seems without direction."
    date: 2006-06-28
    body: ">> Maybe an Alpha then?\n \n> Please troll elsewhere...\n\nI'm not trolling at all. It's just a matter of logic. The guy's proof of non-beta was flawed. And please excuse my language and only mind the matter. Not all (bad) jokes are trolls."
    author: "aac"
  - subject: "Re: SuSE seems without direction."
    date: 2006-06-27
    body: "I fully agree. I'm using S.u.S.E Linux since version 4.4.1 (early 1997) and used every version of Suse on my privat computer as well as on Linux servers. Suse 10.1 is the first verion I can not recommend to neither a linux beginner nor for use on servers due to the many problems: slow package manager, faulty update system, and missing wlan support for many cards. "
    author: "tobias"
  - subject: "Re: SuSE seems without direction."
    date: 2006-07-12
    body: "I do agree with the package management problem. It's valid and it's a big one. It also makes 10.1 feel like a beta release. IMO, this sort of bugs in a distribution are showstoppers, and no one should announce \"victory\" in such a state.\n\nI know it's rather easy to comment on other's people work, but things are quite simple: if your new and shiny piece of software doesn't work, put the old one in and woo everyone with a well marketed upgrade when it does.\n\nAs for the \"why gnome and not kde anymore\" rants, I've read a couple of paragraphs somewhere where it gives an explanation: KDE looks disjoint. It's a technological marvel (and v4 will be even more so), but there is no coherence between projects/apps. The SLED review on\nhttp://osnews.com/story.php?news_id=15103\n make the case: it doesn't look stitched together. That's why banshee popped up: it integrates better with gnome than amarok.\n\nThat said, I hope that kde 4 will address the usability and coherency issues that made gnome the default choice in SLED. personally I see gnome as a short term (a couple of years) perferred option; with KDE coming in with v4, where apps will have the same paradigm and guidelines, better integration (like \"send this ... to mail recipient\" from anywhere you are or beagle search)\n\nAlso, not the \"web 2.0\" trends these days: clear, spaced components, very easy to identify visually. Kde is not like that now. SLED tends to be."
    author: "Laur"
  - subject: "Re: SuSE seems without direction."
    date: 2006-06-27
    body: "\"Default desktop\" doesn't mean \"sole desktop\" so I don't see a discrepancy between speech and behavior."
    author: "Anonymous"
  - subject: "Re: SuSE seems without direction."
    date: 2006-06-28
    body: "It's not just about that, I don't care what desktop is their default. But why do they need to introduce crappy Ximian stuff to the previously working and stable KDE desktop? They're f***ing Suse up, that's what I don't like. And it might just be a coincidence that its mostly GTK/mono stuff that's bad, and it might be a coincidence that with more Ximian influence Suse gets less stable, but it might also be the cause..."
    author: "superstoned"
---
In another event in the series of meetings leading to the KDE 4.0 release, the developers of the core libraries will meet in the <a href="http://en.wikipedia.org/wiki/Norwegian_Wood_(This_Bird_Has_Flown)">Norwegian woods (this bird has flown)</a> from July 1st to July 7th. This meeting, co-sponsored by <a href="http://www.trolltech.com">Trolltech</a> and <a href="http://www.suse.com">SUSE</a>, is labelled KDE Four Core, as it is intended to be the direct successor of the <a href="http://www.kde.org/history/kde-threeb1.php">KDE Three Beta</a> and <a href="http://www.kde.org/history/kde-three.php">KDE Three</a> meetings, that led to the refining of KDE releases 2.0 and 3.0 respectively. But, unlike those, this meeting is only one in a series of "KDE Four" meetings, that started with <a href="http://dot.kde.org/1148680949/">KDE Four Multimedia</a>.






<!--break-->
<p>KDE Four Core has been organised by the <a href="http://dot.kde.org/1139614608/">Technical Working Group</a> and aims at stabilising kdelibs, kdebase and the new module kdepimlibs and to lay foundation to the porting and development of the KDE 4.0 applications. For this reason and to create a group with coherent focus, the number of developers invited was limited to 24, selected among those who are most active in the porting efforts of the target modules. The meeting has been timed so that most of the new technologies are in the Subversion repository already, but sufficient time is left for other meetings before the final "sprint", which is expected to happen during the <a href="http://conference2006.kde.org">aKademy 2006</a> hacking sessions.</p>




