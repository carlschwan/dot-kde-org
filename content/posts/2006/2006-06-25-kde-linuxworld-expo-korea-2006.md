---
title: "KDE at LinuxWorld Expo Korea 2006"
date:    2006-06-25
authors:
  - "ebrucherseifer"
slug:    kde-linuxworld-expo-korea-2006
comments:
  - subject: ":)"
    date: 2006-06-25
    body: "It's really nice to see KDE on Asian Countries :)\n\nI think KDE+linux should improve in some aspects to give better support on asian languages. \n\n* Better CJK input, skim works great but should be integrated in KDE core.\n\n* Asian fonts doesn't look nicely on Linux, in my opinion Gnome has better asian font rendering, but still far from Windows and Mac OS X."
    author: "biquillo"
  - subject: "Re: :)"
    date: 2006-06-26
    body: "I would love it to be able to use chinese in the same easyness under Linux as now under Windows...\n\nMaybe they can even improve it, the windows way of changing keyboard languages is a little irritating, so much, that I actually always remove chinese pinyin support and language bar. And add it by going to controlpanel everytime I need chinese support, which is a lot less than when I need the US-Keyboard layout. \n\nI still don't understand why that 'stupid' language bar, automatically change from time to time to chinese...\n\n"
    author: "boemer"
  - subject: "Re: :)"
    date: 2006-06-26
    body: "Oh, I have interest integrated IM system in KDE core.\nIn Korea, Hwan-Jin, Choi is developer of Korea IM, nabi.\nAlready nabi is good working, but in some KDE applications that is not good.\nI think we need completely integrated IM program in KDE.\nI'll try about them. :) (But I can't make sure when I start.)"
    author: "Cho Sung Jae"
  - subject: "Tenor?"
    date: 2006-06-26
    body: "Why is Tenor mentioned in the presentation? I thought it has been replaced by Strigi. Is this correct?"
    author: "WTAC"
---
On June 5-7th the Korean version of the <a href="http://linuxworldkorea.com">LinuxWorld Conference and Expo</a> series opened its doors for the first time in Seoul. The visitors could learn about various Asian distributions which use KDE including <a href="http://www.haansoft.com/english/">Haansoft</a>.
The Korean team of translators with its head Cho Sung-Jae ran a booth showing the current KDE as well as distributing the brand new <a href="http://www.kubuntu.org/download.php">Kubuntu Dapper CDs</a> to people - which was a great success.
Next to the KDE booth <a href="http://www.kolab.org">Kolab.org</a> was present where Bernhard Reiter explained their Kolab server and KDE's Kontact groupware software to interested visitors.


<!--break-->
<p>The <a href="http://www.kde.or.kr">Korean KDE translators and users group</a> is a small but lively group of enthusiasts. They are meeting regularly on IRC (channel #kde at the <a href="http://www.hanirc.org">HanIRC server</a>) and also in real life each Sunday. It was the first time KDE was presented at an expo in Korea - hopefully we will see more of these events in the future. </p>

<p>
As part of the conference <a href="http://ev.kde.org">KDE e.V.</a>'s president Eva Brucherseifer gave a  keynote on the visions and the secrets behind KDE. As KDE's 10th years anniversary is approaching fast (hint: Oct 14 2006) she used the opportunity to have a look back,
evaluate the status quo and get a glimpse of the future development for KDE4.
The slides are available for <a href="http://websvn.kde.org/*checkout*/trunk/promo/presentations/2006_06_LWE_Korea_en/lwe_korea.odp?rev=551085">download</a>. 
</p>

<p>KDE's presence at LWE Korea 2006 was supported by TKGS, the organisers of LWE Korea, and KDE e.V. Many thanks!</p>

<p>The photo shows the booth crew: Cho Sung-Jae, who organised the KDE booth, Bernhard Reiter showing Kolab.org and the KDE booth team: Matthias Welwarsky, Eva Brucherseifer, Min Chang-Hyun and Hong Chan-Bum.</p>

<img src="http://static.kdenews.org/jr/korea-expo-2006.jpg" width="500" height="375" />







