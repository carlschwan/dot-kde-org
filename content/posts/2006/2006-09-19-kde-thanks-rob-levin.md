---
title: "KDE Thanks Rob Levin"
date:    2006-09-19
authors:
  - "jhall"
slug:    kde-thanks-rob-levin
comments:
  - subject: "what happened?"
    date: 2006-09-19
    body: "did he die or something? I'm never on IRC, so I don't know the news. "
    author: "anon"
  - subject: "Re: what happened?"
    date: 2006-09-19
    body: "23:18 [freenode] -christel(i=christel@freenode/staff/gentoo.christel)- [Global Notice] On the 12th September Rob Levin, known to many as Freenode's lilo, was hit by a car while riding his bike. He suffered head injuries and passed away in hospital on the 16th."
    author: "Cyrille Berger"
  - subject: "Re: what happened?"
    date: 2006-09-21
    body: "I see no reason to exclude murder. \n\nTraffic accidents are common means in some circles."
    author: "fur"
  - subject: "Re: what happened?"
    date: 2006-09-19
    body: "Yes, he unfortunately did die in a trafic accident.\n\nLink to the slashdot story where i first read the news:\nhttp://linux.slashdot.org/article.pl?sid=06/09/16/2152243\n"
    author: "Troels"
  - subject: "Re: what happened?"
    date: 2006-09-19
    body: "He was hit by a car while riding his bicycle on september 12, and died 4 days later in hospital. Ironically, his PC was still on that day, and his IRC client had an away message \"Oops, I slipped away...\""
    author: "Gleb Litvjak"
  - subject: "Re: what happened?"
    date: 2006-09-19
    body: "It was \"Whoops, I stepped away.\", IIRC."
    author: "Eike Hein"
  - subject: "Re: what happened?"
    date: 2006-09-19
    body: "i guess the newsitem misses some information which should be added. personally i knew he died, but you can't really tell from that text above\n\nthanks rob, you'll be missed!"
    author: "fred"
  - subject: "Re: what happened?"
    date: 2006-09-20
    body: "This is less a news story and more a tribute to a generous and forward-thinking individual who has impacted our project profoundly. For a reporting of the plain facts please see wikipedia :)"
    author: "Canllaith"
  - subject: "Background"
    date: 2006-09-19
    body: "http://en.wikipedia.org/wiki/Rob_Levin"
    author: "nobody"
  - subject: "thank you lilo"
    date: 2006-09-19
    body: "thank you for all your time Rob Levin. Rest in peace."
    author: "Vlad Blanton"
  - subject: "Sad"
    date: 2006-09-20
    body: "I can't count on my fingers how many projects I visited on freenode.\nLooks like we should start some serious campain for bike people safety."
    author: "Iuri Fiedoruk"
  - subject: "RIP, lilo"
    date: 2006-09-20
    body: "Rob Levin was a real unsung hero of open-source. His work on OPN/Freenode was a crucial part of the development of the KDE community, the communities around hundreds of other projects and the links between all of them."
    author: "Otter"
  - subject: "Missed"
    date: 2006-09-20
    body: "Rob Levin will be missed. he did a great service to opensource projects everywhere"
    author: "redeeman"
  - subject: "This has happended before (Werner Icking)"
    date: 2006-09-24
    body: "Joerg Anders, author of the famous free KDE music notation editor NoteEdit, wrote in the manual:\n\"I were never able to implement MusiXTeX export without the help of Werner Icking the moderator of the mutex mailing list and excellent MusiXTeX expert. Werner Icking died on the night of February the 8th 2001 in a bicycle accident as he drove home from his workplace.\""
    author: "Erik"
  - subject: "LOL"
    date: 2006-09-29
    body: "Personally, I think it's LOL that he died, but I can see how some jews might be sad."
    author: "Gary Niger"
---
We knew him as lilo.  He was the founder of the Freenode IRC network, a place where many open source projects established a real-time meeting ground. Freenode is where we work, play, and share. It is where many a small idea has grown into a large project. It is where we are all enriched by the experience and diversity of a group of people from many cultures who all have in common a love of open source.




<!--break-->
<p>KDE has benefited greatly from this virtual meeting-hall. We have dozens of channels where ideas are discussed, contributors are mentored and users are supported. Many more recent KDE sub-projects had their start on Freenode and still flourish there. lilo was a common fixture in most of these channels, personally dealing with issues like harassment or spamming, making Freenode a safer environment for all.</p>

<p>What KDE owes to Freenode cannot be expressed easily or lightly.</p>

<p>Thank you Rob Levin. You will be sorely missed.</p>







