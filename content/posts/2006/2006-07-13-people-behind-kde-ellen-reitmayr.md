---
title: "People Behind KDE: Ellen Reitmayr"
date:    2006-07-13
authors:
  - "jriddell"
slug:    people-behind-kde-ellen-reitmayr
comments:
  - subject: "Excellent!"
    date: 2006-07-13
    body: "I've always wanted to know a bit about our Usability Experts! Now to read it..."
    author: "Vlad Blanton"
  - subject: "Usability"
    date: 2006-07-14
    body: "I would like to learn more about \"contribution\" usability.\n\nI mean participation in an open source project usually requires a lot of skills which go beyond simple \"programming\".\n\nYou have to understand how to use CVS, SVN, GIT(is there a KDE client) and so forth.\n\nYou have to understand how the GCC works.\n\netc. etc.\n\nI feel that there are many entrance barriers on the Linux plattform and I have to admit that it is easier to start developing on other plattforms.\n\nBack in the good old days I used an old version of Turbo-C, then tried the Gnu compiler and you know, I was unable to understand it. Things improved since then.\n\nSo it works with many open source software projects: Either you know or documentation will not help you i.e. sink you.\n\nA great tool are samples. An this is perhaps the advantage of open source. You have existing software code and are able to tinker. However opem source success also depends on the ability to recruit new contributors. And here come the role of \"contribution usability\"."
    author: "knut"
  - subject: "Re: Usability"
    date: 2006-07-15
    body: "I can see where you are coming from. I've been poking around the SVN lately and have managed to build kdelibs but don't really know my way around the source quite well enough to successfully find a TODO and just do it. It's not that I have no experience with programming, I'm just not used to something of KDEs size. It'd be nice if we had an adopt a newbie program for those of us that are _seriously_ interested in getting involved, but are looking for a good place to get started and need someone to peek over our shoulders and point us in the right direction. \n\nOn one side we don't want all the experienced devs too tied up especially with the immense amount work to be done for KDE4, but on the otherside the benefits of training people in this way could have a _HUGE_ effect on the future of KDE and indeed Free software in general. "
    author: "Ryan"
---
Today on People Behind KDE we introduce you to <a href="http://people.kde.nl/ellen.html">Ellen Reitmayr</a>, one of KDE and <a href="http://www.openusability.org">OpenUsability.org</a>'s top usability experts.  Ellen has done a lot to help the usability of Kontact and other applications but is now focusing on a consistent user experience for the whole KDE desktop.  In her interview we get to find out about her "denkbrett" and "liebsters".

<!--break-->
