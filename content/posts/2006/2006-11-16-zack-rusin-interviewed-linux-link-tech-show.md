---
title: "Zack Rusin interviewed on the Linux Link Tech Show"
date:    2006-11-16
authors:
  - "Listener"
slug:    zack-rusin-interviewed-linux-link-tech-show
comments:
  - subject: "Wow"
    date: 2006-11-16
    body: "It seems to me that Linux/KDE now really takes off.\n\nbtw: is Zack Rusin related to Mandriva? I mean the star icon."
    author: "gent"
  - subject: "Re: Wow"
    date: 2006-11-16
    body: "Do you mean star icon on the article (next to the text)? I think that is only a icon represents articles related to \"KDE Public Relations and Marketing\" - like double gears icon for developer articles, Qt icon for Qt related articles, etc etc... Correct me if I'm wrong ;)"
    author: "ed"
  - subject: "Audio Quality"
    date: 2006-11-16
    body: "I wish the audio quality wasn't that bad. I'm a big fan of TLLTS, but the audio quality has been declining from bad to horrible. It makes my head hurt.\n\nI cannot physically survive listening to any current TLLTS episode, although I'd love to. What can we do about it?"
    author: "Mark Kretschmann"
  - subject: "Re: Audio Quality"
    date: 2006-11-17
    body: "You are absolutely right, the audio quality is really horrible. I couldn't stand to listen to it for longer than a few minutes either. Is there a transcript available?"
    author: "phmx_"
  - subject: "thanks zack..."
    date: 2006-11-16
    body: "...and TLLTS, nice ~1:45 of interview!\ngood to hear this nice overview mixed with some interesting insights.\n\n_cies.\n\np.s.:\nzack: the day i became a veg, was the day i decided to learn how to cook. i'm not looking back (by now my roommates aren't either).\nguys from TLLTS: what are you guys smoking? i must get some of that shit. ;)\n\n"
    author: "cies breijs"
  - subject: "python and utf8"
    date: 2006-11-17
    body: "zack says he likes ruby but not too much python one of the reason being its unicode handling, the fact that you need to use a module and stuff. Well last time I checked unicode is kind of a bitch too on ruby, if you want to use  strings functions it won't work either you need to use special plugins or modules last time I checked, right?"
    author: "Patcito"
  - subject: "Re: python and utf8"
    date: 2006-11-17
    body: "I think your right about Ruby, though it might have been fixed in a recent minor revision."
    author: "Ian Monroe"
  - subject: "Re: python and utf8"
    date: 2006-11-17
    body: "none at all with a comprehensive solution coming is better than half-assed with a fix maybe coming. sort of explains why ruby resonates with a lot of kde (and other) developers and python simply seems \"ok\""
    author: "Aaron J. Seigo"
  - subject: "Re: python and utf8"
    date: 2006-11-17
    body: "Hi Aaron,\nI gave up on Ruby for now precisely because it doesn't support Unicode natively while Python does, or so I thought... It has a Unicode data type that's fully interchangeable with raw strings for all of its string-related functions (and also with QStrings, which is cool), and I've been using it a lot to fix encoding fuckups in output from other languages (not just Ruby though)... Am I missing something? Have I been doing something wrong with Python and never known it? c_c\nThanks!"
    author: "A.C."
  - subject: "Re: python and utf8"
    date: 2006-11-17
    body: "As far as I know, Python does have native Unicode support, but when you define a string it isn't Unicode, unless specified to be so (with the exception of MacOSX where unicode is always used). However, I don't know Python in depth... I am just as puzzled about this as you are."
    author: "blacksheep"
  - subject: "Re: python and utf8"
    date: 2006-11-17
    body: "Yes, in python you can say unicode('foo') in the same way you can say string('foo'). In future they're planning to merge these two string types into a single unicode version.\n"
    author: "Richard Moore"
  - subject: "Re: python and utf8"
    date: 2006-11-17
    body: "And you can do things like:\n\nu\"Panzenb\u00f6ck\".encode(\"utf8\")\n\nunicode(\"Panzenb\u00f6ck\",\"iso-8859-15\").encode(\"utf8\")\n\"Panzenb\u00f6ck\".decode(\"iso-8859-15\")\n\"foo\".encode(\"utf16\")\n\netc.\n\nSo the unicode type is built into python, or am I missing something?"
    author: "panzi"
  - subject: "Re: python and utf8"
    date: 2006-11-21
    body: "No, you're not missing anything. Ruby's _lack_ of unicode support is one of the more cited reasons people prefer Python instead. Not sure how people have it mixed up the other way round.\n"
    author: "Cerulean"
  - subject: "Re: python and utf8"
    date: 2006-11-17
    body: "Aaron, this is nonsense. Really blathering, thesaurus-necessitating nonsense. Python's unicode support is excellent. Full-featured. Well thought out. Complete. Calling it half-assed... Well, I need my thesaurus again.\n\nI've created a couple of linguistics applications with Python that needed really good unicode support and I never had a problem. Should the class that's currently called \"string\" be called \"bytearray\"? Sure. Should the class that's currently called \"unicode\" be called \"string\"? Sure, that too. Does \"unicode\" provide everything you may need from a string class including regular expressions? Definitely. There are currently two languages that handle Unicode really well: Python and Java. (And there's, as far as I know, one toolkit that does handle unicode really well: Qt.)\n\nNo, Ruby doesn't resonate with _this_ kde developer at all. Ugly syntax, slow, full of hot air... There is nothing in which Ruby improves on its examples, Perl and Python. The reason it resonates with some people is probably that it's new, but then, there'll always be something else that's new for them."
    author: "Boudewijn Rempt"
  - subject: "Re: python and utf8"
    date: 2006-11-17
    body: "\"The reason it resonates with some people is probably that it's new\"\n\nWhat are you talking about?  I've been using Ruby for 5 years now and it's been around since the early to mid 90's.  Just because Ruby on Rails got popular recently doesn't mean that Ruby is \"new\".\n\nIt's fine to rip on the language, but don't go around saying it's \"new\"."
    author: "Anonymous Coward"
  - subject: "Re: python and utf8"
    date: 2006-11-17
    body: "\"What are you talking about? I've been using Ruby for 5 years now and it's been around since the early to mid 90's. Just because Ruby on Rails got popular recently doesn't mean that Ruby is \"new\".\"\n\nYes, but it's only received attention and a lot of development recently in the last few years."
    author: "Segedunum"
  - subject: "Re: python and utf8"
    date: 2006-11-17
    body: "All my ruby loving friends always say: \"but ruby-on-rails!\"\nSo the best thing about ruby seems to be that webframework and it is the only reason so many people choose ruby over python. If ruby-on-rails won't exists, I think there would be much less ruby-lovers. I don't know if this people would use python instead, but the killer feature of ruby is not the language or std-lib itself, its ruby-on-rails.\n(Well, I don't write web applications, so I use python.)"
    author: "panzi"
  - subject: "Re: python and utf8"
    date: 2006-11-17
    body: "That's the killer app, but it's a killer app made possible only by the language itself. I'll admit to not being very familiar with Python, so I'll merely speak to the aspects of Ruby that make Rails possible, or at least prettier -- things like blocks and the ability to catch calls to missing methods and do things with them, for example (part of the way dynamic finders are implemented), as well as the ability to extend existing classes (used by Rails to, for example, extend the Fixnum class so one can write `1.day.from_now' and get a DateTime object that represents 24 hours from now). Rails is well-appreciated because it's so readable, and it tends to be readable at least in part because of Ruby's syntax (which for some reason a lot of people seem prone to describing as horrible) and language features.\n\nThat said, you won't hear me saying `but Ruby on Rails', you'll hear me saying `if you don't like Ruby, look to Rails to see what it's capable of'. I'm using Ruby for simple scripting, as well. And from the little I've seen of Python, the little inconsistencies in the language (when does one use methods vs functions on objects, for example?) would drive me up the wall (and do, when I'm helping students with their homework). But again, my experience with it is very limited.\n\nAs a side note, that has nothing to do with Unicode support in Ruby, which is, indeed, not very good at all. But, the intent, as I understand it, is to address that in Ruby 2, as well as some of the speed issues, and to bring in a new regular expression engine with neat little features like named backreferences. So the future is definitely something to look forward to in the Ruby world."
    author: "Antonio"
  - subject: "Re: python and utf8"
    date: 2006-11-17
    body: "<i>things like blocks</i>\n\nI don't know what that is, so I can't say if python has it.\n\n<i>the ability to catch calls to missing methods and do things with them [...], as well as the ability to extend existing classes</i>\n\nThis is a feature every \"dynamic\" language (should) have, so python has it of course.\n\nWell, for optimisation reasons python does not support changing builtin types (in, long, str) and binding types (if that is the proper term) like QString. But you can of course change derived classes in any way.\n\nAnother neat feature of python are generators (the yield satemetn) and with-blocks (since python 2.5)."
    author: "panzi"
  - subject: "Re: python and utf8"
    date: 2006-11-17
    body: "Generators seem like one specific instance of blocks. I can't say for sure that they are exactly the same. In Ruby, a block is, much the same way, called with yield (and possibly with parameters, such as the statement `yield a, b', which will pass `a' and `b' to the block). Ruby exclusively uses blocks for iteration, however, such that:\n\nfor i in (1..10)\n   p i\nend\n\nIs the same as:\n\n(1..10).each { |i| p i }\n\n(Parameters to a block are specified between || symbols, blocks are delimited either by {} or by do/end).\n\nThese blocks, combined with mixins, let you do neat things. For example, if you provide the `each' method, which takes a block, you can use the Enumerable mixin to get things like:\n* select (return an array of all items for which the block does not evaluate to false)\n* inject (pass a variable around as an accumulator to every instance of a block -- i.e., a sum could be `(1..10).inject( 0 ) { |sum, num| sum += num }'\n* reject (return an array of all items for which the block *does* evaluate to false)\n* collect (an equivalent of Python's map, but done by providing a block to be evaluated; e.g. `(1..10).collect { |number| number * 10 }' to get an array of every number from 1-10 multiplied by 10)\n* .. etc.\n\nI'm also a fan of being able to pass parameters to methods without parentheses in certain circumstances (though I understand why some people hate it):\n\n\"hello\".gsub /[he]/, 'test'"
    author: "Antonio"
  - subject: "Re: python and utf8"
    date: 2006-11-17
    body: "I see. Very interesting. That's a feature which would be great to have in python.\n\nPython uses a other approach to this kind of problems: list-comprehension.\n\nL = [f(i) for i in xrange(1,11)]\n\nFilter the list:\nL = [f(i) for i in xrange(1,11) if g(i)]\n\nIf you don't want to generate a real list you can make a generator:\nL = (f(i) for i in xrange(1,11) if g(i))\n\nWhen the generator will be passed to a function, you can omit the parenthesis:\nfoo(f(i) for i in xrange(1,11) if g(i))\n\nYou can also nest list-comprehensions:\nL = [i + 3 for i in (j * 2 for j in [2,3,4,5]) if i > 7]\n\n-> L = [11, 13]\n\nOf course there are many builtin-function to help you with this stuff:\nmap, zip, filter, sorted, reversed, sum, reduce, min, max, ...\n\nThere is no product function (as there is a sum function), but look how easy it is to write it.\n\nversion 1:\nproduct = lambda sequence: reduce(lambda a,b: a * b,sequence,1)\n\nversion 2:\nimport operator\n\nproduct = lambda sequence: reduce(operator.mul,sequence,1)\n\nproduct([2,3,4])\n-> 24\n\nproduct([2])\n-> 2\n\nproduct([2])\n-> 1\n\nSome further applications:\ntranspose = lambda mtx: zip(*mtx)\ntranspose([(11,12,13),(21,22,23),(31,32,33)])\n-> [(11, 21, 31), (12, 22, 32), (13, 23, 33)]\n\nA bit of a long line, but demonstrating many features:\nfor i, s in map(lambda a,b: (a,b+\"...\"),reversed([1,2,3]),sorted(['foo','bar','egg','spam'])): print i, s\n\noutput:\n3 bar...\n2 egg...\n1 foo...\nNone spam...\n\n\nA other feature of python I like are decorators, but enough with the spamming for now. :)"
    author: "panzi"
  - subject: "Re: python and utf8"
    date: 2006-11-17
    body: "\"No, Ruby doesn't resonate with _this_ kde developer at all. Ugly syntax, slow, full of hot air... There is nothing in which Ruby improves on its examples, Perl and Python. The reason it resonates with some people is probably that it's new, but then, there'll always be something else that's new for them.\"\n\nWell, I think Ruby and Python both have their advantages and disadvantages, and people should make their own minds up which they prefer. I think it's important that we ensure KDE offers first class support for both.\n\nRuby has open classes, including customisable instances (you can add methods to individual instances), iterators that use blocks and yield, continuations, mixins, no need to pass 'self' to instance methods, and plenty enough things to make _self_ kde developer resonate. Ruby is slow, but UIs built with QtRuby or Korundum are not slow because most of the code that gets called is C++ of cources.\n\nTo use Unicode for a QtRuby or Korundum app with Qt Designer, you need to pass a '-KU' option to Ruby, and optionally include a \"require 'jcode'\" in the code. It doesn't have complete Unicode support, but that is not the same as having no Unicode support.\n\nHere are some interesting links that discuss Unicode and Ruby:\n\nhttp://ruphus.com/blog/2005/06/11/ruby-and-unicode/\n\nhttp://redhanded.hobix.com/inspect/closingInOnUnicodeWithJcode.html\n\nhttp://www.tbray.org/talks/rubyconf2006.pdf"
    author: "Richard Dale"
  - subject: "Re: python and utf8"
    date: 2006-11-17
    body: "and for people doing rails:\nhttp://ruby.org.ee/wiki/Unicode_in_Ruby/Rails"
    author: "Patcito"
  - subject: "Re: python and utf8"
    date: 2006-11-17
    body: "\"Ugly syntax\"\n\nI find that really funny having written an awful lot of Python with Zope over the years. Python's syntax is bloody awful. The indentation thing was just one of the acts of lunacy (might be good in some circumstances, but not generally). My comfort level goes up when I start using Ruby and Visual Basic (yes, VB) again. If people want to attract VB developers to desktop level programming then you ain't going to do it with Python.\n\nExplicit 'self' parameter to methods is another thing that I dislike, and is possibly worse than the indentation. The keywords 'this' in Java and 'Me' in VB are used when you need them - within scope. And double underscores to indicate private? No, it just won't do. You are also never going to convince a VB or desktop developer of any kind of this:\n\ndef sayhello():\n       return 'Hello, world!'\n\nfoo = sayhello\n\nprint foo\n<function sayhello at *********>\n\nversus this:\n\ndef sayhello()\n       return 'Hello, world!'\nend\n\nfoo = sayhello\n\nprint foo\nHello, world!=> nil\n\n(Indenting won't come out - guess what's more readable?)\n\nYes. hello() evaluates to 'Hellow World' in Ruby. Novel concept eh?\n\n\"Perl and Python\"\n\nUgly syntax again? Perl?\n\n\"The reason it resonates with some people is probably that it's new\"\n\nNo, it's just that it's plain better - especially if you want to do OO programming. We've gone beyond the initial hype to the stage where everyone asks \"Is it really better?\" The answer is still yes. Frameworks like RoR simply make web apps easier when compared with Java or .Net. Even Joel Spolsky seems to have had an internal argument with an intern perhaps, about Ruby and RoR, to the point where he's having to badmouth Ruby:\n\nhttp://www.joelonsoftware.com/items/2006/09/01.html\n\nYou then get various graphs telling you that Python, or something else, is more in demand than Ruby and that you shouldn't be using it neglecting to point out that Ruby is a relatively young language and framework.\n\nIs Ruby slow in places and does it need to be improved? Yes. Will it be? Considering the interest and development on Ruby, I'd say yes. Does it mean that we should use Python instead? No."
    author: "Segedunum"
  - subject: "Re: python and utf8"
    date: 2006-11-17
    body: "Ok, ok, ok. Let's all calm down. I don't say ruby is bad, I only say python is good and it's definitely not significant worse then ruby and I also think ruby is not significant better than python: python and ruby are much the same. I don't know a lot about ruby, but it seems that both languages are approximately same good.\n\nThings like len(obj) and __* to make something private are ugly indeed. It would be cool if this would change in python 3.0.\n\nBut I actually like that you can distinguish a function call from passing a function/callable object as parameter. How do you pass a non-argument function in ruby as a parameter? Or is ruby like haskell sideeffect free?\n\nThen I have to google what things like mixins and blocks are. First heard of them in this thread."
    author: "panzi"
  - subject: "Re: python and utf8"
    date: 2006-11-17
    body: "I read on wikipedia what those features are: I think in one way or the other they are all supported by python. I don't know if the syntax for those features is \"better\" in ruby but that depends on taste anyway. So the decision between python and ruby is a decision by frameworks/available libraries and taste. :)"
    author: "panzi"
  - subject: "Re: python and utf8"
    date: 2006-11-17
    body: "Forgot to lookup block: seems to by a anonymous method or something like that. Could be very handy in some situations. Well, python dose not support that (AFAIK)."
    author: "panzi"
  - subject: "Re: python and utf8"
    date: 2006-11-18
    body: "\"I don't say ruby is bad, I only say python is good and it's definitely not significant worse then ruby\"\n\nWell, no it's not significantly worse. Think of this in the context of developing desktop apps, and a VB developer coming over to develop KDE apps in a decently clean OO language that's easy to pick up. That's how I'm looking at this.\n\n\"Things like len(obj) and __* to make something private are ugly indeed. It would be cool if this would change in python 3.0\"\n\nI really hope so. All we're talking about here is syntax really. There may be some design decisions in terms of how Ruby views things in an OO world that may come out later when we can more adequately compare Python and Ruby.\n\nAt the moment, syntax arguments get in the way. Python needs to do a bit better there."
    author: "Segedunum"
  - subject: "Re: python and utf8"
    date: 2006-11-17
    body: "\"\"\"The indentation thing was just one of the acts of lunacy \"\"\"\n\nI thought the same, then I tried Python and after 15 minutes I was used to it. Braces and 'end' statements look like clutter to me now.\n\nRuby and Python are more similar to each other than they are dissimilar. Some would argue that the two languages are converging over time.( http://www.c2.com/cgi/wiki?PythonAndRubyAreConverging ) But your example code highlights one of the interesting differences between Ruby and Python, and how they \"think\" about objects. This webpage here does a good job of explaining the two points of view: http://onestepback.org/index.cgi/Tech/Ruby/PythonAndRuby.rdoc\n\nI come from a C/asm/C++/Java background. So Python's take on OOP makes the most sense to me. Function or method calls without brackets just don't feel right at all. While people who are used to VB or Smalltalk might find Ruby a more natural fit.\n\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Re: python and utf8"
    date: 2006-11-17
    body: "> I thought the same, then I tried Python and after 15 minutes I was used to it. Braces and 'end' statements look like clutter to me now.\n\nSame here. :)\n\n> Function or method calls without brackets just don't feel right at all.\n\nI think so, too. But in languages like haskell it is a blessing that you don't have to write brackets! (Otherwise it would be lisp.)\n\nI think it's all a matter of taste. :)"
    author: "panzi"
  - subject: "Re: python and utf8"
    date: 2006-11-18
    body: "\"I thought the same, then I tried Python and after 15 minutes I was used to it. Braces and 'end' statements look like clutter to me now.\"\n\nHands up, yes. I never thought too much of it myself, even when I first started using Python. However, for a VB developer looking to develop some KDE apps in an equivalent language? That's a tough one. We don't want a VB clone by any stretch, but something with enough reasonably clean and English-like syntax with emphasis on decent OO development."
    author: "Segedunum"
  - subject: "Re: python and utf8"
    date: 2006-11-18
    body: "I was a VB developer (3, 5 and 6) when I first met Python, seven or eight years ago. I took to it like a duck to water. It was the equivalent language for the equivalent purpose. Already in 1999, with PyKDE, python the best and most productive way to develope gui applications on Linux.\n\nOf course, Python wasn't just the vb-on-linux that it appeared to me at first: it was the stepping-stone to higher things, like coding C++, which means regularly waiting twenty minutes to see if a bit of code I wouldn't have had to write had I used Python does or does not cause a crash."
    author: "Boudewijn Rempt"
  - subject: "Re: python and utf8"
    date: 2006-11-19
    body: "> I come from a C/asm/C++/Java background. \n> So Python's take on OOP makes the most sense to me.\n\nInteresting. I come from a asm/Java/C/C++/Perl background, and I find Ruby's OOP much more natural... sort of like Java without the misjudgements.(Like no primitive types!) I've never really gotten into Python despite a few tries. It feels like another Perl to me, and while I love Perl as a crazy testbed... I already know one perl.\n\nBut I have had very little need of unicode support, so that is a difference that might make python worthwhile.\n"
    author: "Esben Mose Hansen"
  - subject: "Sound Quality Please"
    date: 2006-11-20
    body: "The Quality is horrible. I can bearly understand. wth?\n\nalso, ONE HOUR ? why are not you doing weekly 20 minutes instead of huge hours we don't have time to listen.\n\n"
    author: "Mathieu Jobin"
  - subject: "Re: Sound Quality Please"
    date: 2006-11-20
    body: "You do realize that Zack was on a phone in Norway while the guys were in PA and VA in the U.S.? TLLTS is usually 2 hours in length with the interviews 45-60 minutes in length. \n\n"
    author: "hackmeister"
  - subject: "Re: Sound Quality Please"
    date: 2006-11-21
    body: "How about using VoIP?"
    author: "kanttu"
  - subject: "Re: Sound Quality Please"
    date: 2006-11-22
    body: "It is voip. The show uses an Asterisk server for the telephone calls. Zack dialed via a phone into the Asterisk conference room. I'll put in for a KDE developer guest extension going forward. "
    author: "hackmeister"
  - subject: "Transcript"
    date: 2006-11-21
    body: "Where can I find a transcript of this interview?"
    author: "Artem S. Tashkinov"
---
Yesterday night Zack Rusin made a guest appearance on <a href="http://tllts.org/">The Linux Link Tech Show</a>. He discussed recent developments in the X.Org project, Qt, KDE, his life in Norway and some other Open Source related issues. It is a lengthy interview that should satisfy those who say Zack's not blogging and responding to questions often enough. Download the recorded <a href="http://tllts.org/mirror.php?fname=tllts_167-11-15-06.ogg">Ogg file</a> or <a href="http://tllts.org/mirror.php?fname=tllts_167-11-15-06.mp3">MP3 file</a>.

<!--break-->
