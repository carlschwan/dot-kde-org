---
title: "People Behind KDE: Erik Kj\u00e6r Pedersen"
date:    2006-07-02
authors:
  - "jriddell"
slug:    people-behind-kde-erik-kjær-pedersen
comments:
  - subject: "30 == oldie?"
    date: 2006-07-02
    body: "So you're an oldie when you're 30?  Hmmm..."
    author: "Inge Wallin"
  - subject: "I got the names wrong all the time..."
    date: 2006-07-02
    body: "A nice interview, thank you very much! It turned out that I got the names wrong all the time - I always thought KDE has only one Pedersen - Jesper Pedersen (the funny danish guy who always knows where you can get some beer for free :-)) and that he does danish translations as well. Oops. :-)"
    author: "Frerich"
  - subject: "Not the oldest contributor"
    date: 2006-07-02
    body: ">>Age: 60 (Probably the oldest contributor to KDE?)\n\nNope, one the Dutch key translators is 67 years old.\n\n"
    author: "Rinse"
  - subject: "kde-oldies"
    date: 2006-07-03
    body: "Is this mailinglist real!?"
    author: "Clarence Dang"
  - subject: "Thanks!"
    date: 2006-07-08
    body: "I just wanted to take a moment and say thanks to the people that do \"People Behind KDE\" and post links to interviews here. I really enjoy learning about the diverse group of people that work on KDE.\n\nSomeday, I hope to be one of those people. :)"
    author: "Elijah Lofgren"
---
Today's interview on <a href="http://people.kde.nl/">People Behind KDE</a> brings us one of <a href="https://mail.kde.org/mailman/listinfo/kde-oldies">KDE's oldest</a> contributors.  This man translates <a href="http://l10n.kde.org/teams/infos.php?teamcode=da">KDE into Danish</a> to help preserve our freedom.  Find out why he turned away from Emacs as we present <a href="http://people.kde.nl/erik.html">Erik Kjær Pedersen</a>.
<!--break-->
