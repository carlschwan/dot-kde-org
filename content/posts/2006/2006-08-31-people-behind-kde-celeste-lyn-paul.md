---
title: "People Behind KDE: Celeste Lyn Paul"
date:    2006-08-31
authors:
  - "jriddell"
slug:    people-behind-kde-celeste-lyn-paul
comments:
  - subject: "no subject"
    date: 2006-08-31
    body: "Celeste :-), your work for KDE is highly appreciated, and I look forward to\nKDE4 to rock the Usability-world, and btw you are a goddammed good-looking girl."
    author: "nobody"
  - subject: "I agree with the first poster..."
    date: 2006-08-31
    body: "You are a very good looking girl. Cuteness and intelligence rolled into a single package. I'm glad to see I'm not the only one who has a bit of a thing for you ;-)\n\nThough I've never spoken to you... just heard about you and seen pics of you...\n\nNow I sound creepy...\n\n-coughs- But hey, at least I'm not hiding my identity like Mr. No Subject ;-)"
    author: "Aaron Krill"
  - subject: "Re: I agree with the first poster..."
    date: 2006-08-31
    body: "Do you have any idea what an ass you sound like?  You don't sound creepy, you are creepy."
    author: "Greg Meyer"
  - subject: "Re: I agree with the first poster..."
    date: 2006-08-31
    body: "Yes, I suppose to certain individuals I am. But those who do not judge me based on my antics, are those who are true friends. If you constantly act in a way to make everybody like you, you are not true to yourself, and never find yourself people you can truly depend on and trust."
    author: "Aaron Krill"
  - subject: "Re: I agree with the first poster..."
    date: 2006-09-01
    body: "You, my friend, are an idiot."
    author: "Stefan T."
  - subject: "Re: I agree with the first poster..."
    date: 2006-09-05
    body: "No he's not. There's definitely something admirable in not trying to hide that you're a creep, when you are."
    author: "mikeyd"
  - subject: "Re: I agree with the first poster..."
    date: 2006-08-31
    body: "If you wanted to impress Celeste or anyone for that matter, do you think publishing it on the dot would get you extra points?\n\nWhy can't you people treat women contributors to open source in a non-sexist manner? Have you thought about how you would feel if you were a woman and people talked about whether you are \"cute\" instead of the quality of your work?\n\nThe fact that this issue comes up every once in a while on the dot is really pathetic. What's worse is that people like you make the rest of the KDE community appear to be equally lacking in social grace, which is why I am posting this and publicly apologizing on your behalf. \n\nYou know what would be really cool? If you reflected on your actions, and you yourself apologized.\n\n\n\n\n"
    author: "Gonzalo"
  - subject: "Re: I agree with the first poster..."
    date: 2006-08-31
    body: "Now you've got all those \"extra points\". That's not fair!\n;)  hehe\n"
    author: "Gabriel Gazz\u00e1n"
  - subject: "Re: I agree with the first poster..."
    date: 2006-08-31
    body: "To make everything fair and non-sexist, we should comment on the appearances of the male KDE devs, too.\n\nI'll help by starting us off... That Aaron Seigo is quite a cutie. ;-)"
    author: "AC"
  - subject: "Re: I agree with the first poster..."
    date: 2006-08-31
    body: "No, no - Zack is the cutie!"
    author: "TooShyToDisclose"
  - subject: "Re: I agree with the first poster..."
    date: 2006-08-31
    body: "na, i agree with parent. aaron is cute. imho Mathias Kretz too, btw. if only i preferred boys over girls... ;-)"
    author: "superstoned"
  - subject: "Re: I agree with the first poster..."
    date: 2006-08-31
    body: "ok, my girlfriend made me say i think SHE's the cutest. aah, well, she is, of course..."
    author: "superstoned"
  - subject: "Re: I agree with the first poster..."
    date: 2006-08-31
    body: "We want pictures :)"
    author: "AC"
  - subject: "Re: I agree with the first poster..."
    date: 2006-08-31
    body: "Um... I didn't make my comments because she's a programmer. I made my comments based on the fact I think she's a very cute, /and/ very intelligent person. I do hope you read my entire comment...\n\nI'm not entirely sure why you feel the need to apologize for me, do you find complementing a woman to be in distaste? If so, I pity you.\n\nCeleste, if you're reading this, I do hope you didn't take what I said in offense... it certainly was not intended as such. If my comment disturbed you in any way, shape, or form please let me know so I may apologize in a proper manner. I shall not apologize simply because others were offended by my opinion, as they were not the intended target."
    author: "Aaron Krill"
  - subject: "Re: I agree with the first poster..."
    date: 2006-08-31
    body: "Personally what really bothers is not the \"sexism\" in the sense of lack of gender-neutrality when some males address (the few existing) females in fields like informatics and computer science, but their complete lack of social skills and common sense. For me what is really strinking is that some people can only either be assexual or creepy, which usually means that they should definitly go out more, make some female friends and learn how to communicate. \n\nAnyway cool interview, great to see people with actual knowledge on usability contributing, instead of just users and coders as it used to be the case. Hope Plasma does turn out to be amazing :) I cannot wait to be able to show of a revolutionary KDE4 desktop with XGL enabled to some friends.\n\n\n"
    author: "RC1"
  - subject: "Re: I agree with the first poster..."
    date: 2006-08-31
    body: "Interesting discussion...  \n\nI certainly do agree with you on your observation. She's incredibly gorgeous, sexy as hell, extremely intelligent, quite charming, super fabulous, sometimes incredibly silly (which I love!).  I could go on...but you get the point I think.  She's also very well respected, knowledgeable, and dedicated in her field.  \n\nYou're definitely not the only one with \"a thing\" for her.  But in case you missed it in the interview...she's taken ;) \n\np.s. See you when I get home tonight Celeste!  And what's up Eli? :) Long time no see   "
    author: "justin"
  - subject: "Re: I agree with the first poster..."
    date: 2006-09-01
    body: "Perfect comment, hehe :) And she is not only taken, but took someone, too ;)"
    author: "Shuby"
  - subject: "And we wonder why there aren't many women in compu"
    date: 2006-08-31
    body: "Thanks to comments like these, for sure."
    author: "Anonymous"
  - subject: "Re: And we wonder why there aren't many women in c"
    date: 2006-08-31
    body: "Interesting, particularly since men in this field tend to objectify women far less than those in other fields. I for one do not objectify women, and my comment reflected that attitude. I said she was intelligent, and that I agreed with the previous poster who said that her work was greatly appreciated. It is, I think she's great at what she does. She's contributed a lot to the interfaces of apps in KDE, and a lot more of her wonderful work will show through in KDE 4.\n\nBut seriously, has the world come to a point where a man cannot express his feelings about a woman, compliment her looks, without be chastised?"
    author: "Aaron Krill"
  - subject: "Re: And we wonder why there aren't many women in c"
    date: 2006-08-31
    body: "But it's especially creepy on the internet because you don't know who's who...\n\nand I'd rather be complimented about my good work then my good looks...\n\naltough... *ahem* :)"
    author: "Darkelve"
  - subject: "Wallpaper"
    date: 2006-08-31
    body: "What's the wallpaper photo of?"
    author: "AC"
  - subject: "Re: Wallpaper"
    date: 2006-08-31
    body: "Berlin, Gendarmenmarkt"
    author: "ac"
  - subject: "RMS vs. Linus"
    date: 2006-08-31
    body: "Hahah love the RMS quote :) I'd choose him!"
    author: "petteri"
  - subject: "Re: RMS vs. Linus"
    date: 2006-08-31
    body: "Me too. Jimmy Walles says he prefers no content to closed content in China and people applaud. RMS says the same and he's a closed minded. Anyway, great interview :-)"
    author: "Alfredo Beaumont"
  - subject: "Thanks"
    date: 2006-08-31
    body: "for your work so far, you helped me a lot with the KHangMan usability study. My most embarassing moment was not to know if you were \"Celeste\" or \"Paul\" (you were not known at that time) in order to answer you!\nYour work on HIG will be a great addition to KDE4 and I personnally like a lot the way you work with other people and the depth of your work. \n\nannma"
    author: "annma"
  - subject: "I don't like her"
    date: 2006-08-31
    body: "I have a kde wish \"move mouse to default button\" http://kde-apps.org/content/show.php?content=30523 . On http://bugs.kde.org/show_bug.cgi?id=114879 there are some replies of CEleste like \"35% isnt a very good rating..\" Well now the wish has 45 % good, but when you look at KDE/windows ratio it will be very small and still we are using KDE. I think she should be more open to comunity requests and KDE should pay a litle less atention to \"usability experts\" and focus on what users want!\n"
    author: "zvonsully"
  - subject: "Re: I don't like her"
    date: 2006-08-31
    body: "\"Usuability expert\" are people who watch others using computer, to see how they interact, to see what problems user have when using software. So I would say \"usuability expert\" are the way KDE can pay attention to user request."
    author: "Cyrille Berger"
  - subject: "Re: I don't like her"
    date: 2006-08-31
    body: "I agree. 'what users want' is mostly limited to what the vocal part of the kde community want, not what MOST users want. besides, most ppl don't KNOW what they want..."
    author: "superstoned"
  - subject: "Re: I don't like her"
    date: 2006-08-31
    body: "Since the ratio starts at 50%, having 45% good means it is not good. And Celeste is actually being nice by saying that \"35% isn't a very good rating\", since 35% is pretty bad. Less than that wouldn't be just bad, it would be outright awful and hated, just like more than 70% is scored by good apps and more than 80% only by those being very good.\n"
    author: "Lubos Lunak"
  - subject: "Re: I don't like her"
    date: 2006-08-31
    body: "> I think she should be more open to comunity requests and KDE should pay a\n> litle less atention to \"usability experts\" and focus on what users want!\n\nYeah, what do users want? And who are the users? bugs.kde.org/kde-apps.org are not representative for the whole user base.\n\nIf KDE would add an option for every possible feature requested on bugs.kde.org or kde-apps.org, we would end up with approximately 30 configure pages per application and 100 for kcontrol.\nNobody would be able to find any option in there.\nNot to mention that it would cause bugs and annoying behaviour in weird combinations of options developers haven't thought about.\n\nThere is a range of sane strategies between \"remove all options\" and \"add all features someone somewhere suggested\". The latter isn't a sane one for sure.\n\nTo comment one of your arguments from kde-apps.org:\n\n> takes the mouse control out of the user for a nanosec(how many clicks can u\n> make in a nano secs ?! )\n\nMore important, the mouse cursor is somewhere else than it was before, which is confiusing and leads to unpredictable behaviour. Also, an accidental click would lead to errors. And why don't you just user the return key?\n"
    author: "lippel"
  - subject: "Re: I don't like her"
    date: 2006-08-31
    body: "> More important, the mouse cursor is somewhere else than it was before, \n> which is confiusing and leads to unpredictable behaviour. Also, an \n> accidental click would lead to errors. And why don't you just user the \n> return key?\n\nI concur. It also about control. Throwing the pointer automatically around the screen causes the user to lose the feeling of control of the system thus decreasing the feeling of trust and easiness of use.\n\nIf somebody really, really, really wants this option, I think it should be done as a separate add-on. "
    author: "R"
  - subject: "Re: I don't like her"
    date: 2006-08-31
    body: "I want this too. Logitech mouse drivers for Windows have this feature and after switching to linux I miss it very much."
    author: "Pietro"
  - subject: "Re: I don't like her"
    date: 2006-08-31
    body: "You're missing the point that good usability advice is based on research, knowledge and experience in the field. \"Democracy\" --especially the kind of democracy where you only count those that are vocal in a very limited sample-- is quite a bad basis for usability advice.\n\nApparently this makes a certain change of culture necessary, but I'm sure it's a change for the better."
    author: "Sebastian K\u00fcgler"
  - subject: "Re: I don't like her"
    date: 2006-08-31
    body: "Ow, and I might add that you should not take this kind of thing personal. :-)"
    author: "Sebastian K\u00fcgler"
  - subject: "Re: I don't like her"
    date: 2006-08-31
    body: "And let's say it was taken to a fair vote, majority rules;  you will still lose, as the rating for that wish is under 50%.  Face it, most people don't like that feature."
    author: "Leo S"
  - subject: "Re: I don't like her"
    date: 2006-08-31
    body: "zvonsully, have you ever used any software that did what you are requesting?\n\nI have once, very briefly, it was a truly horrid experience.\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Who cares if you like her?"
    date: 2006-08-31
    body: "As long as no-one forces the \"move to default button\" I don't care if they implement it. Press the Enter button if you wish to press the default button - it helps you avoid repetetive stress injuries too!\n\nI am one of those 55% who don't want it - we're still the majority :-P"
    author: "Kjetil Kilhavn"
  - subject: "posted on irc"
    date: 2006-08-31
    body: "08:01 <seele> lol @ KDE\n\nReally, the only responses I see on here are \"omg cute.\" and \"omg listens to experts.\" \n\nYes she listens to experts, and she is a pretty strong authority these days due to her day job. And 'what people want' let to 10,000 conflicing configuration options in many applications, including the ones I work on for a living. There is a good reason to deny a user request, especially if it doesn't fit the product vision."
    author: "eli"
  - subject: "Re: posted on irc"
    date: 2006-08-31
    body: "Oh, and yeah she's cute. I say that as someone who, you know, has actually met her in person. She's a mean little rogue too."
    author: "eli"
  - subject: "Re: posted on irc"
    date: 2006-08-31
    body: "> you're write about Ubuntu\n\nThe hackergotchi icon on planetkde doesn't do her justice\nlook like a lip peircing and bad pixelation is making the image look less flattering\nit is a conspiracy to make her look bad!\n\nconversely most hackergotchi icons for the male developers are so flattering they fail to prepare you for quite how fat many developers are in real life"
    author: "Anon"
  - subject: "Re: posted on irc"
    date: 2006-09-01
    body: "He, they say on the internet noone knows you are a dog or fat or male... Who cares?"
    author: "Bello"
  - subject: "cute? hur hur hur"
    date: 2006-08-31
    body: "right - so saying someone is cute is creepy...\n\nWhat if you say she's not cute? I suppose that's rude.\n\nThis is stupid. What is it with everyone and their paranoia? Obviously those that commented on her cuteness were being friendly. Why take that friendliness and then view it through suspicious eyes?\n\nBy the way; \"cute\" is defined as \"attractive or pretty especially in a childish, youthful, or delicate way\". That's not creepy.\n\nThe word \"cute\" would be creepy if it meant \"OMG! Come within reach of me!! HurHurHur!!!\"\n\nKae"
    author: "Kae Verens"
  - subject: "Re: cute? hur hur hur"
    date: 2006-08-31
    body: "Most of you guys are pretty creepy - here in Denmark we would call you \"klam\". You really should get out more and maybe get a girlfriend - maybe even get laid if you get that lucky .\nIt's not that important if she looks good or not - the article is not about a beauty contest so stop commenting her looks!"
    author: "josel"
  - subject: "Re: cute? hur hur hur"
    date: 2006-08-31
    body: "Just for the record, I also think she's cute, but guess what?  My wife mentioned it before I did.  She also thinks that Eben Moglen is cute, but in a different sort of way, and I guess I'd have to agree, although I would say a lot of that impression in my case is based on his personality (having met him him in person, I can say he's a very gem\u00fctlich kind of chap.)\n\nI don't think it's so bad that when someone's picture is posted and their appearance strikes you as pleasant and charming, to mention it.  It *is* possible to appreciate and savor someone's good looks without being sexually charged up about it.  It's very sad that a lot of people minds are so narrowly programmed that they can't separate aesthetic appreciation from their hormonal urges, and therefore it's become dangerous to express any kind of positive affirmation of someone's physical appearance.  For gosh sakes, people, after all, the photo's just a head shot, not a bikini layout.  I dare say a lot of people like the Mona Lisa's appearance too, but I doubt people would complain about that...\n\nI guess my impression of Celeste is that she's a person that I think I would probably find pleasant to be around, and I'm not going to be apologizing for that.  :-)\n\n"
    author: "Peter H."
  - subject: "Re: cute? hur hur hur"
    date: 2006-08-31
    body: "Its no excuse Peter H. Her looks is not relevant in any manner to the article. If i was her i certainly would find comments like  - \"Wow, shes so cute looking\" - and the like as very offensive. \nI think you should apologize for claiming that she is pleasant to be around solely based on the fact that you saw her picture. Thats very rude and offensive and really sexistic - no excuse for that."
    author: "josel"
  - subject: "Re: cute? hur hur hur"
    date: 2006-09-01
    body: "introducing her as an \"American lass\" isn't exactly focussing on her technical abilities either now is it?  \nare the comments such a surprise when even the submitter felt the need to highlight gender?  \n\n"
    author: "Anon"
  - subject: "Re: cute? hur hur hur"
    date: 2006-09-01
    body: "It's very rude and offensive and really sexistic to compliment someone's looks based on a picture? I wasn't going to weigh in at all, but this particular oh-so-overly general comment seems amusing at least and laugh-out-loud ridiculous at worst."
    author: "Antonio Salazar"
  - subject: "Re: cute? hur hur hur"
    date: 2006-09-03
    body: "You just d'ont seem to get it. Wouldnt you hate to have your work judged by how you look? In fact most of you guys didnt even comment on her work but solely on her looks. \nIt's no excuse that you probably are around 20 and \"latino\" - you should know better. And yes its sexistic - not one of you has ever commented on some male developers good looks.\n"
    author: "Josel"
  - subject: "Re: cute? hur hur hur"
    date: 2006-09-04
    body: "Sorry, but that's not what happened. Her work was not judged based on how she looks. Her work and her looks were judged independently. You'll notice that the first comment's cuteness remark is prefixed by \"and btw\"... It's an aside.\n\nIf her looks were totally offtopic, why include a picture of her in the article in the first place?\n"
    author: "AC"
  - subject: "Re: cute? hur hur hur"
    date: 2006-09-01
    body: "You might note that my comment began by saying that I thought she was \"cute\", [not, I might add, \"cute looking\"] and ended by saying that my \"impression\" was that I thought I would find her pleasant to be around.  There is a difference.  The first term is based partially on appearance, and yes, I find her appearance pleasant.  However, this is not the sum total of my 'impression'.  I also find her interests and personality, as expressed in her answers to the interview questions, to be pleasant, and yes, \"cute\".  Someone can be cute in more than just the visual sense--there's many aspects to a pleasant person beyond the arrangement of their atoms.  You can perhaps even supplement \"cute\" with 'charming', 'affable', or even 'witty' to get a better grip on my intended meaning.\n\nSo, as before, I will not be apologizing for that.\n"
    author: "Peter H."
  - subject: "Re: cute? hur hur hur"
    date: 2006-09-01
    body: "How can you say \"Her looks is not relevant in any manner to the article.\"? The article _included_a_photo_of_her_. Commenting on this photo is commenting on part of the article. This is not a case of a purely technical article, and then someone 'dug up' a picture of her from elsewhere.\n\nIf you publish an article about yourself to the world, and you choose to include photos (not to mention setting a casual tone with a section for \"personal questions\"), then you are the one who has opened the discussion to your appearance.\n\nPeople don't tend to comment on the boring and commonplace. She does stand out as attractive among the photos of KDE developers. It's really no different than if, say, one of the male KDE developers had blue hair, and someone said \"Nice hair.\"\n\nJust take it as a friendly comment directed at her and move on with your life. I'm sure she can handle being called cute. She's probably heard it a lot. She doesn't need a bunch of guys riding to her rescue on the dot. Having her play the damsel in distress in your imagination is far more sexist than the original comment ever was."
    author: "AC"
  - subject: "Re: cute? hur hur hur"
    date: 2006-09-01
    body: "I frankly don't give a damn fsck how she looks I care what she does. And, although I'm a GNOME and XP/Classic user, I'd say usability is very important in the *NIX DE community in general."
    author: "AC"
  - subject: "On topic(-ish, i.e. usability)"
    date: 2006-08-31
    body: "Well it's always nice to see these interviews to put faces to names and so on and have a bit more of an idea about the skills/qualifications etc of the people working on kde, so thanks for the interview.\n\nHaving had a brief look at Celeste's recent start menu usability paper I was pleased to see some graphs and so on of real users - I do feel sometimes that things are brought in supposedly in the name of usability but *not* backed up with hard figures like this (of course, studies might not give a true picture, but its better than one person dictating). I'd like to see this applied to for example, the decision to include text under icons in kde 4 and things like the proposed new SUSE start menu (I get the impression in the latter that a study is on-going, i hope we get to see the results).\n\nBtw, I'm not necessarily saying any of these decisions are not good (and if they are bad not blaming Celeste for them as I don't know if she was involved) but (continue to) show us the figures and make usuability a science not an art. That way if a default doesn't suit me I know there *is* a good basis for it (and I can always change it for me, of course)."
    author: "Simon"
  - subject: "Cute."
    date: 2006-08-31
    body: "A little late, but just another poster that thinks she's cute. Can't say I read many of the dot articles (other than commit digests), but from reading planet I believe she's very talented at finding out what users want/expect.\n\nAlso, to those dorks who want mouse snapping to a button (or whatever the hell that annoying feature is), please consider that typically only those who want something changed will speak up. Those content / liking how something works don't tend to say much.\n\nAnd this feature is available in KDE is it not? (not sure) Just enable it if you want it."
    author: "R.James"
  - subject: "Coolo is cuter..."
    date: 2006-09-01
    body: "The sexiest contributor to KDE is by any means of course Coolo!\nNo Paul, no Faure, no Rusin will ever met his appearance.\n\nI have a photo of him when he had a beard on my bedside table... I cannot turn my eyes away.... oh Coolo... wait, what is this woman next to his cheek???"
    author: "Warmy"
  - subject: "Oh my god"
    date: 2006-09-01
    body: "It's not at least surprising that there you can count the women involved in KDE on one hand, given the main topic of the majority of comments in here. Yes, Celeste is cute. No, you don't need to comment on that, and you should know by yourself that it's only counter-productive in any respect.\n\nAnd now go and read Val Henson's \"Encourage Women in Linux\" howto at http://infohost.nmt.edu/~val/howto.html\n\nBastards."
    author: "Jakob Petsovits"
  - subject: "Re: Oh my god"
    date: 2006-09-01
    body: "How exactly, is this counter-productive? This dot article is on an interview with Celeste... There is a -personal- questions section of the interview. Those comments (and mine) are vaguely inline with that section of the interview.\n\nNow if this was an article/summary that was strictly on HIG (and it's not), then yes those comments are innapropriate. Furthermore, Celeste hasn't said yay or nay as to whether she cares about these kinds of comments.\n\nBTW, I asked my gf if comments like this would freak her out. She said only comments along the lines of \"will you marry me\", \"are you single\", etc... The rest she had no problem with. Simple flattery--that's all."
    author: "R.James"
  - subject: "Re: Oh my god"
    date: 2006-09-02
    body: "Ok, this is the situation:\n- Celeste has no problem with this kind of flattery (see her blog)\n- Your girlfriend has no problem with this kind of flattery\n- Many (not all) others do have a problem with this kind of flattery,\n  and are feeling very uneasy in such an environment.\n\nShort pointer to the blog of a prominent female KDE contributor:\nhttp://canllaith.org/?p=4\nand she's just one example out of many.\n\nThe fact that not all women are being scared away by this kind of comments doesn't mean that you can generalize it to all of the other ones. I mean,\n- there's no gains for you in any case\n- few women feel better with a million \"you're so cute\" comments\n- a lot of women don't like it.\n\nWhat's so difficult about just letting it be?"
    author: "Jakob Petsovits"
  - subject: "Re: Oh my god"
    date: 2006-09-01
    body: "You did a pretty good job of pointing out an article that, you know, didn't mention what was done here as a `No'. The above was not a sexual advance, it was a simple compliment for goodness's sake."
    author: "Antonio"
  - subject: "Re: Oh my god"
    date: 2006-09-03
    body: "Jacob  has a nice link. Try http://canllaith.org/?p=4\nMaybe you get it now! Those comments about someones \"cuteness\" are completely innapropriate in this enviroment. If you really feel the urge then send her a email expressing and explaining your feelings about her looks - that will probably be good fun.\n\n\n"
    author: "Josel"
---
Today's <a href="http://people.kde.nl">People Behind KDE</a> features the American lass who is forging the <a href="http://usability.kde.org/hig/current/">KDE 4 Human Interface Guidelines</a>.  Find out the advantage of a hobby against job, what is wrong with Fruit Salad plus the good fortune of one KDE convert as we interview <a href="http://people.kde.nl/celeste.html">Celeste Lyn Paul</a>.


<!--break-->
