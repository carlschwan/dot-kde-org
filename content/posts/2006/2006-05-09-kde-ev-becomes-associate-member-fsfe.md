---
title: "KDE e.V. Becomes Associate Member of FSFE"
date:    2006-05-09
authors:
  - "dmolkentin"
slug:    kde-ev-becomes-associate-member-fsfe
comments:
  - subject: "Re: KDE e.V. Becomes Associate Member of FSFE"
    date: 2006-05-09
    body: "Congratulation! This was definitely a good decision for both, KDE and FSE."
    author: "pinky"
  - subject: "Step 1: Exchange merchandise :)"
    date: 2006-05-09
    body: "On LinuxTag the cooperation was already visible:\n\nhttp://vizzzion.org/?id=viewpic&gcat=Linuxtag2006&gpic=IMG_7521.JPG#images\n\nOn the left fellow KDE marketing dude Sebastian K\u00fcgler wearing the FSFE-shirt.\n\nOn the right the new proud owner of a KDE-shirt, FSFE's Georg Greve."
    author: "Martijn Klingens"
  - subject: "Re: Step 1: Exchange merchandise :)"
    date: 2006-05-09
    body: "When are we seeing RMS in a KDE t-shirt? :)"
    author: "alewar"
  - subject: "Re: Step 1: Exchange merchandise :)"
    date: 2006-05-09
    body: "Probably not, but chances are we are supposed to refer to KDE as GNU/KDE now ;-)"
    author: "Andre Somers"
  - subject: "Re: Step 1: Exchange merchandise :)"
    date: 2006-05-10
    body: "Come on, KDE runs on BSD, Solaris, etc. too you know."
    author: "Ben Morris"
  - subject: "Re: Step 1: Exchange merchandise :)"
    date: 2006-05-10
    body: "Did you miss the smiley?"
    author: "Andre Somers"
  - subject: "Qt license issue"
    date: 2006-05-09
    body: "Without broader industrial support KDE desktop will remain marginal hobby system. And industry will choose Gtk because Qt commercial license presents very unpleasant vendor lock-in to Trolltech.\n\nGPL is quite fine for the base system. However, GPL in the GUI toolkit will distract most companies from touching it and Qt commercial license is problematic and expensive enough even for large companies - they will not be willing pay to develop for niche market when the gains are unsure.\n\nI am sorry to say this, but I see this quite clearly from where I stand (software house)"
    author: "ZACK"
  - subject: "Re: Qt license issue"
    date: 2006-05-09
    body: "could we add a bot filtering out spam like this? I'm tired to read all these \"get two inches larger\" and \"Qt commercial license is too expensive\" threads."
    author: "hf"
  - subject: "Re: Qt license issue"
    date: 2006-05-09
    body: "\"And industry will choose Gtk because Qt commercial license\"\n\nStrange then that the number of commercial applications using Qt are several times higher than that of Gtk. \n\n\n\"Qt commercial license is problematic and expensive enough even for large companies\"\n\nExactly, just as problematic as paying for full time developers I suspect. The license will equal something like the cost of a decent developer for 1-2 weeks. For full time development you make that cost back easily over a year when you consider added productivity and quality. \n"
    author: "Morty"
  - subject: "Re: Qt license issue"
    date: 2006-05-11
    body: "Well, to me it IS too expensive. This summer I'll be writing a small application, and the cost of QT licence is about as much as I hope to earn by selling this application (I'd sell 10 copies/year or so). I originally hoped to write it for Linux and Windows, and tell clients that they can buy Linux version for X$, or that they can buy Windows version for X$ + of course they will have to pay Y$ for windows licence. Just my cunning little plan for converting people to Linux :) So the only trick is writing it for two platforms. So, my options are:\n\n- Pay for commercial QT licence - unacceptable, it's going to take more than year of selling my app to get even\n- GPL my code - well, OK, but how do I prevent one of my clients selling this application to other clients, cheaper than I do? I'm just a freelance programmer, not a buissnesperson who can come up with some mad scheme to prevent it.\n- write in GTK - forget it. I'm not touching GTK ever again\n- Give up my plan of doing something good for the Linux community, and go back to the old \"this program requires Windows Operating System to run\" mantra..."
    author: "ac"
  - subject: "Re: Qt license issue"
    date: 2006-05-11
    body: "\"GPL my code - well, OK, but how do I prevent one of my clients selling this application to other clients, cheaper than I do?\"\n\nNothing, but in most cases(Depending on tha nature of your application of course) it does not matter. The main reason is simply that your clients most likely are NOT in the buisness of selling software in the first place(naure of app etc.) and will not start doing so with your application either. Besides they won't be able to handle things like support, and would not like to have to do it. And if the applications are specialized the most likely customers would be their competitiors, and they would probably not sell usefull tools cheaper to them. "
    author: "Morty"
  - subject: "Re: Qt license issue"
    date: 2006-05-11
    body: "Tough basically. At some point you'll end up paying. You'll pay for Windows licenses, Qt or using GTK and the problems for commercial developers that entails.\n\nI think you should probably accept that you application is just not cost effective."
    author: "Segedunum"
  - subject: "Re: Qt license issue"
    date: 2006-05-11
    body: "You do know of the Trolltechs Small Business program, don't you?\nhttp://www.trolltech.com/products/qt/licenses/licensing/smallbusiness\n\nBy the way: if you really only expect to make about as much as a single Qt licence costs with your application while only selling 10 copies, than I think that:\n1) Your application is probably too cheap, and/or\n2) Your clients are not going to be interested in marketing a program they can't support for even less for a market that seemingly hardly exists. For an average company, a specialized piece of software at a price of &#8364; 250 (about 1/10th of a Qt licence, right?) is practicly free. Really. "
    author: "Andre Somers"
  - subject: "Re: Qt license issue"
    date: 2006-05-11
    body: "What exactly do you mean by &#8364;??? I think you're trying to say \u0080 here. \n\nPlease, is it really too much to ask people to stick to real world currencies instead of inventing new ones? :-)"
    author: "Dot Troll"
  - subject: "Re: Qt license issue"
    date: 2006-05-17
    body: "You may want to consider wxWidgets (http://wxwidgets.org/). I prefer Qt, but it is a lot better than GTK+. Its Python bindings are especially good -- I have used it actually because of this."
    author: "blacksheep"
  - subject: "Re: Qt license issue"
    date: 2006-05-09
    body: "And the relation with the article is....?\n\nYour statement is utter nonsence, of course, but others here have already commented on that. I still think that it is not unreasonable to pay for a toolkit you wish to use commercially, and I think it's very cool to have that same toolkit available to you under a free licence if you want to use it to make open software."
    author: "Andre Somers"
  - subject: "Broken record Broken record Broken record Broken r"
    date: 2006-05-09
    body: "The parent is an anti-KDE, anti-copyleft zealot. We've already dealt with anti-Free Software propaganda a couple of days ago:\n\nhttp://dot.kde.org/1146085435/1146780113/\n\nThe parent is an off topic broken record, and I would suggest that the moderators remove the threads he starts."
    author: "ac"
  - subject: "Re: Qt license issue"
    date: 2006-05-09
    body: "\"Without broader industrial support KDE desktop will remain marginal hobby system. And industry will choose Gtk because Qt commercial license presents very unpleasant vendor lock-in to Trolltech.\"\n\nAnd industry actually supports GTK? I think you need to get out more.\n\n\"I am sorry to say this, but I see this quite clearly from where I stand (software house)\"\n\nI do work in a software house and I don't see it. You clearly dont'. Why? You wrote this:\n\n\"Qt commercial license is problematic and expensive enough even for large companies\"\n\nand even better, this:\n\n\"they will not be willing pay to develop for niche market when the gains are unsure.\"\n\nIf it's a niche market, what on Earth makes you think it's going to be more cost effective with GTK?\n\nNice try, but this has been done to death. No doubt you'll come back in five or six years and copy and paste the same comment."
    author: "Segedunum"
  - subject: "Re: Qt license issue"
    date: 2006-05-10
    body: "It is as wasted as your constants trollish about Mono, I don't see any difference between that troll and you.\n\n"
    author: "Mitarai"
  - subject: "Re: Qt license issue"
    date: 2006-05-11
    body: "\"It is as wasted as your constants trollish about Mono, I don't see any difference between that troll and you.\"\n\nAhhhh. You do make me chuckle. Thank you very much.\n\nThe fact that you see more than a grain of truth in what I've written, want to call it a 'troll' but don't understand how to respond to it in any itemised or structured way, or that you have a strong emotional attachment to Mono (which is not up for discussion here), is quite frankly, not my problem or anyone else's. You'll have to try harder."
    author: "Segedunum"
  - subject: "Re: Qt license issue"
    date: 2006-05-11
    body: "  Maybe the guy who you replied have something of truth on his words and that's why you answerd to him, is the same case and thanks for proving my point.\n\n  And the fact that you have to take sarcasm as a shield simple makes my words stronger, thank you.\n\n\n"
    author: "Mitarai"
  - subject: "Re: Qt license issue"
    date: 2006-05-11
    body: "\"Maybe the guy who you replied have something of truth on his words and that's why you answerd to him, is the same case and thanks for proving my point.\"\n\nGiven the fact that I provided a full itemised answer? Not based on what's been discussed - or not - with you, as the case may be. This is also a KDE site. I don't go on to a Gnome discussions site, talk about their licensing and then talk about how crap GTK and Mono is for commercial developers out there. I could, but I don't.\n\n\"And the fact that you have to take sarcasm as a shield simple makes my words stronger, thank you.\"\n\nSince your words don't even formulate decent English at times and your words consist of 'troll', 'Pity Mono, everybody hates it!' (when Mono wasn't even mentioned) etc. etc., I think you'll find that's a no. Sarcasm as a shield :-). You actually reply to my comment four back and itemise each bit and I will be less sarcastic and give you less of a ribbing. Or maybe I won't.\n\nYou lose because of that and you know it, as always ;-)."
    author: "Segedunum"
  - subject: "Re: Qt license issue"
    date: 2006-05-12
    body: ">>>>\"Given the fact that I provided a full itemised answer? \"\n\nlol, says who?\n\n>>>\"Since your words don't even formulate decent English at times and your words consist of 'troll', 'Pity Mono, everybody hates it!' (when Mono wasn't even mentioned) etc. etc., I think you'll find that's a no. Sarcasm as a shield :-). You actually reply to my comment four back and itemise each bit and I will be less sarcastic and give you less of a ribbing. Or maybe I won't.\"\n\nGlad to see you didn't use sarcam this time, did I hit a sensible nerv in my previus post? I think I did.\n\n\n\n"
    author: "Mitarai"
  - subject: "Re: Qt license issue"
    date: 2006-05-09
    body: "\"GPL is quite fine for the base system. However, GPL in the GUI toolkit will distract most companies from touching it...\"\n\nI don't think your argument has anything to do with GUI toolkits, but libraries vs applications.\n\n\"... and expensive enough even for large companies ...\"\n\nI'm gonna take a guess here and say that you have no clue how much a Qt license costs (that or how much money 'large companies' have).  Opera isn't \"large\" yet they seem to manage pretty will using Qt, explain that one."
    author: "Corbin"
  - subject: "Re: Qt license issue"
    date: 2006-05-10
    body: "The problem is that Opera wasn't really selling and was losing market extremly to FireFox and now they have to give it for free to don't die and despite that its marked share is still pathetic.\n\nIts only hope now relies in handheld devices.\n\n"
    author: "Mitarai"
  - subject: "Re: Qt license issue"
    date: 2006-05-10
    body: "So you're point is that Opera has to give away their (desktop) browser (so they get no money from it), yet they are still able to afford Qt licenses?\n\nI also don't think Opera is losing much market to FireFox."
    author: "Corbin"
  - subject: "Re: Qt license issue"
    date: 2006-05-10
    body: "No, they already had the Qt licenses before they noted the slowdonw in sales, that would a problem of Qt for some that is not friendly with shareware so you can know  how your application will deal with the marked.\n\nNow if you tell me that Opera gave away its software for free because they are good guys and not because a marked strategy then I laugth at your face.\n\n"
    author: "Mitarai"
  - subject: "Re: Qt license issue"
    date: 2006-05-10
    body: "to be fair to corbin, i think you're taking what he said and putting words in his mouth (\".. because they are good guys\"). that's pretty unfair and doesn't lend itself to useful conversation.\n\nand shareware? ugh. most of the software market works rather differently than you suggest. for those who really want to go the \"zero cost proprietary shareware\" route there are other options out there. for almost everyone else, Qt does just fine.\n\ni really don't know of any one-size-fits-absolutely-everyone solution, even the \"free as in beer\" variety."
    author: "Aaron J. Seigo"
  - subject: "Re: Qt license issue"
    date: 2006-05-10
    body: ">>>>most of the software market works rather differently than you suggest. \n\nOh really? how come?\n\n>>>>for those who really want to go the \"zero cost proprietary shareware\" route there are other options out there.\n\nAnd for those who want to spend some fait money too.\n\n>>>>i really don't know of any one-size-fits-absolutely-everyone solution, even the \"free as in beer\" variety.\n\nNeather I, but upsets me when Qt/KDE fanboys try to make it appear like that, discarting GTK merits and btw, people here take GTK as a psedonimun of plain C, I use GTK# all the time and I don't find myself returning to C++ anymore, abd actuallly I think you can be even more productive with it.\n\n\n\n"
    author: "Mitarai"
  - subject: "Re: Qt license issue"
    date: 2006-05-11
    body: "\"Oh really? how come?\"\n\nBecause it becomes extremely apparent you don't actually work in it?\n\n\"And for those who want to spend some fait money too.\"\n\nI'll have to just guess at what that, or indeed, anything else you've written actually means. The message is clear. If Qt isn't going to do it for you then use something else. However, I find it funny that you mention shareware. Your shareware isn't going to get you ten yards in an open source world full of free software :-). You're going to need to come up with something that actually means something, and does something for someone and that means making it of good enough quality and that means investing in your development tools. Qt exists for that purpose, and is funded accordingly.\n\n\"Neather I, but upsets me when Qt/KDE fanboys try to make it appear like that\"\n\nArrrr. There, there. Fanboys? Making it appear like what, exactly? I think you're trying to talk about GTK there when it has never been mentioned by anyone apart from the idiot who commented first. Oh, and you.\n\n\"discarting GTK merits and btw, people here take GTK as a psedonimun of plain C\"\n\nThat's because it is. C is just not object oriented, plain and simple, which is why GTK needs umpteen language bindings for people - which all need to be maintained.\n\n\"I use GTK# all the time\"\n\nBugger off and use it then. I'm not going to get drawn into why it's not a wise decision for anyone who actually develops commercially packaged and licensed software for a living. It's been done. For everyone else who wants to produce the shareware on Windows that everyone loves to hate, there are free beer solutions like Mono and GTK#.\n\n\"and I don't find myself returning to C++ anymore\"\n\nGood for you. However, using C++ and using Qt and C++ are two different things.\n\nI know you have a complex about people not liking Mono (and I've pointed out exactly why I don't like it in the past), and you have some nonsense issues with Qt which really shouldn't effect you (mainly centred around licenses and stuff that doesn't matter at all, like Opera's ability to afford Qt licenses???!!!), but really - go elsewhere."
    author: "Segedunum"
  - subject: "Re: Qt license issue"
    date: 2006-05-11
    body: "You and your long a boring replies, Was I even asking to you?\n\n"
    author: "Mitarai"
  - subject: "Re: Qt license issue"
    date: 2006-05-11
    body: "\"You and your long a boring replies\"\n\nAnd here you lose - yet again. If you can't answer full itemised comments in an adult manner you have no argument, so bugger off.\n\n\"Was I even asking to you?\"\n\nDid Corbin even ask for a reply from you? You could just have said \"I have absolutely no answer to that\" or \"Does not compute\" and been done with it."
    author: "Segedunum"
  - subject: "Re: Qt license issue"
    date: 2006-05-12
    body: "one word:\n\nBoring."
    author: "Mitarai"
  - subject: "Re: Qt license issue"
    date: 2006-05-11
    body: "> Without broader industrial support\n\nWell, you see. The main aim with the GPL is to secure the future for the open source community. Not to get industrial support.\n\nThe GPL does a pretty good job there. And the LGPL does not, unfortunately.\n\nTo let businessess pay for Qt is to make sure that businesses help the OS community, if they want help from the OS community itself. Thus it is a pretty nice scheme, maybe not the best, but certainly better than a LGPL only scheme.\n\nYou have a point though, in that a LGPL license would fit a business better. But that does not come with the quality that a GPL/Pay-license software has, and probably never will.\n\n> GPL in the GUI toolkit will distract most companies\nA pointed out many times before, this argument does not hold. But then again, it does not really matter that much, because the OS community has no interest in being utilized by international enterprises. \n\nQt is a win-win situation."
    author: "Open Source friend"
  - subject: "Re: Qt license issue"
    date: 2006-05-12
    body: "And it just go worse.  Gnome is the official platform of Suse now.  http://www.linuxformat.co.uk/modules.php?op=modload&name=News&file=article&sid=326\n\nToo bad that someone didn't buy out Trolltech years ago and liberalize the license."
    author: "ac"
  - subject: "desktop environment != toolkit"
    date: 2006-05-09
    body: "The fact that many toolkits use GTK to render makes everyone to think it is developed with GTK. GTK apps have a place under KDE while KDE itself is developed with QT and KDE libs. KDE is a desktop environment and you can run GTK, qt, motif whatever backend you like apps, like on windows, where nobody cares which toolkit was used to develop or render it. Problems, e.g. GTK apps not using KDE standard file dialogue and desktop integration and so on are going to be solved by the portland effort of backend integration. If GTK apps do not integrate well into the KDE desktop environment, it is a fault of the toolkit or Gnome zelotism.\n\nKDE is related to QT like xpde is related to kylix, it is the tool used to develop KDE. This does not mean that all applications running under KDE must be Qt based. The fact that some applications are made incompatible by artificial means is not the fault of KDE. "
    author: "Hup"
  - subject: "Re: desktop environment != toolkit"
    date: 2006-05-10
    body: "Yes, but can I legally develop commercial apps for KDE without paying for Qt?\n\nThe linkage would look like:\n\n1. Qt (GPL) <-> KDElibs (LGPL) <-> MyApp (Proprietary)\n2. GTK (LGPL) <-> MyApp (Proprietary)\n\nI suppose you can't really circumvent the GPL like this by inserting an LGPL layer between it and your commercial app? I also suppose this has been discussed >1e3 times before!?\n\nNot feeding the trolls here, but it would be nice if someone could explain this.\n\n"
    author: "Martin"
  - subject: "Re: desktop environment != toolkit"
    date: 2006-05-10
    body: "If you define commercial as \"Not open source\", then the answer is obviously \"no\". The question I've never heard answered by people who think they should be able to do this is: Why would that be nessecairy? Isn't it fair that you pay for a quality toolkit like Qt when you intend to make money off their efforts?\n\nThe point in making the KDE libs LGPL is that there is no way you can get them licenced under another, closed licence. There are many, many people who own part of the copyrights of these libs. Who would you pay for a closed licence? So, in order to be able to create any closed source application that uses the KDE libs, they need to be LGPL. For Qt it is a different story. The copyright holder is clear (Trolltech), and they do have multiple licences available. \n\nNote that nothing stops you from creating a commercial application as open source though! GPL does not prohibit selling your application or otherwise making money off it.\n\nSo no: you can not circumvent GPL by using a LGPL layer and building a commercial application on top of that. In my view, that is a Good Thing (TM)."
    author: "Andre Somers"
  - subject: "Re: desktop environment != toolkit"
    date: 2006-05-10
    body: "\"Can I legally develop commercial apps for KDE without paying for Qt?\"\n\nYes, you can create *commercial* apps (ie. ones that you can sell) using the open source Qt version as long as those apps are also open source. But you can't create closed-source, proprietary apps without paying for a closed-source, proprietary version of Qt.\n\nI think this is the best possible outcome for USERS, because it encourages third-parties to support Free and Open Source Software, either by making their own applications open source or by paying a company (Trolltech) that develops and improves open source tools such as Qt.\n\nFor more info, read this:\n\nhttp://kdemyths.urbanlizard.com/topic/10\nhttp://blogs.qtdeveloper.net/archives/2006/04/30/on-the-lessor-gpl\n"
    author: "ac"
  - subject: "Re: desktop environment != toolkit"
    date: 2006-05-10
    body: "It should be no problem as commercial closed source vendors could pay for the license and everyone else writes GPL. \n\nThe problem of non-GPL software with qt free licenses is non-existant. It is a strawmen.\n\nFurther the intelligent qt licensing scheme creates a valuable environment with an incentive to develop GPL. This contrary to the license variety we see in other toolkit fields.\n"
    author: "Hup"
  - subject: "Re: desktop environment != toolkit"
    date: 2006-05-10
    body: "You're still a derivative of Qt and still have to obey their copyright even if you're doing it \"indirectly\" through another library like this. Qt will still be loaded into your program address space, if you want to be technical you could say your KDElibs becomes GPL when it is linked against or dynamically loads a GPL Qt, it's only LGPL when in isolation or linked against a propriety-licensed Qt. IANAL etc."
    author: "mikeyd"
  - subject: "Re: desktop environment != toolkit"
    date: 2006-05-10
    body: "kdelibs does not become GPL when it loads Qt "
    author: "AC"
  - subject: "Re: desktop environment != toolkit"
    date: 2006-05-12
    body: "So the grandparent is wrong then; from the perspective of licensing\n\ndesktop environment == toolkit\nKDE == Qt\n\nThanks for clarifying.\n"
    author: "Martin"
---
<a href="http://ev.kde.org">KDE e.V.</a>, the organisation that represents
KDE in legal and financial matters, and
the <a href="http://www.fsfeurope.org">Free Software Foundation Europe</a> (FSFE) are proud to announce their
associate status, working together for the promotion and protection of
Free Software on users' desktops in Europe and worldwide. "<em>Together with the KDE e.V. we seek to break the stranglehold on the desktop, give people freedom, and explain to them why this is important, and why they should not give it up again,</em>" said Georg Greve from FSFE.  See the <a href="http://kde.org/announcements/fsfe-associate-member.php">full press release</a> for details.







<!--break-->
