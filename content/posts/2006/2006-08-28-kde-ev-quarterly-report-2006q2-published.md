---
title: "KDE e.V. Quarterly Report 2006Q2 Published"
date:    2006-08-28
authors:
  - "jriddell"
slug:    kde-ev-quarterly-report-2006q2-published
comments:
  - subject: "aKademy?"
    date: 2006-08-28
    body: "I see in the report (thanks for doing these, Allen! it's a good way to keep kind of up-to-date with what happens inside the organization itself, as opposed to the project) that aKademy 2007 is being planned. Have there been any bids put in? Is it being kept under wraps till after this aKademy?"
    author: "Adriaan de Groot"
---
KDE's legal body <a href="http://ev.kde.org">KDE e.V.</a> has published its <a href="http://ev.kde.org/reports/ev-quarterly-2006Q2.pdf">second 2006 quarterly report</a>.  Topics covered include progress organising Akademy 2006, activities from the working groups and sysadmins, events organised and attended and seven new members.  If you have been helping KDE for a while, do consider <a href="http://ev.kde.org/members.php">joining the e.V. membership</a>.



<!--break-->
