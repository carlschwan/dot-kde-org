---
title: "People Behind KDE: Petr Rockai"
date:    2006-06-14
authors:
  - "jriddell"
slug:    people-behind-kde-petr-rockai
comments:
  - subject: "odd behavior"
    date: 2006-06-16
    body: "I'm using MacOS style application menu bar but if i start Adept the menu is directly in the Adept window and not on the top were all the menus from the others app are.\nIs this a bug in Adept?"
    author: "pinky"
  - subject: "Re: odd behavior"
    date: 2006-06-16
    body: "Most likely, no. You're probably running adept as root, which means using the settings of root, not your own."
    author: "Boudewijn"
---
Today's <a href="http://people.kde.nl/">People Behind KDE</a> interview is with the person who made the Debian package manager for KDE <a href="http://web.mornfall.net/adept.html">Adept</a>.  KDE's man at the Czech Red Hat office also spends his free time distracting his fellow KDE developers with games of Wesnoth and Freeciv.  Find out if you can both be sane and like C++ in our <a href="http://people.kde.nl/mornfall.html">interview with Petr Rockai</a>.


<!--break-->
