---
title: "Akademy 2006 is in Dublin this September"
date:    2006-03-15
authors:
  - "agroot"
slug:    akademy-2006-dublin-september
comments:
  - subject: "Who is Marcus Furlong\u00df"
    date: 2006-03-15
    body: "Never heard of Marcus Furlong... While previous aKademys were organized by well known activists and participants of the KDE community, this time there is a white spot on the map.\n\nAnd the link provided by the story\n\n --> https://www.cs.tcd.ie/~furlongm/ <--\n\ntells me close to nothing...\n\nSo... this isn't a joke?"
    author: "ac"
  - subject: "Re: Who is Marcus Furlong"
    date: 2006-03-15
    body: "It's the same team as organised GUADEC in Dublin a few years ago.\n"
    author: "Jonathan Riddell"
  - subject: "Re: Who is Marcus Furlong"
    date: 2006-03-17
    body: "No it is not the same team.  "
    author: "Alan Horkan"
  - subject: "Re: Who is Marcus Furlong"
    date: 2006-03-18
    body: "Gnome is evil. KDE is not evil.\nBoth Gnome and KDE are not bad.\nIt is not the same team.\nWhy not team up?\n"
    author: "Gerd"
  - subject: "Re: Who is Marcus Furlong"
    date: 2006-03-19
    body: "> Gnome is evil. \n\nHuh?  \n\n"
    author: "Alan Horkan"
  - subject: "Re: Who is Marcus Furlong"
    date: 2006-03-19
    body: "Oddly enough, I'll agree.  It was begun primarily, instead of a way to help get KDE legal, to replace KDE with a nearly completely incompatible desktop.  Nowadays its primary purpose seems to be the proprietary-software-friendly desktop.  Another wrinkle is the attention and excitement paid to Mono, which may or may not have legal problems, depending on who you ask.  And unlike KDE, the GNOME project doesn't seem to want to play nice.  With anyone.\n\nI admire GNOME for their work on usability and for much of their work in becoming friendly in other areas, but big jeers for not playing nice and for all the code duplication."
    author: "regeya"
  - subject: "Re: Who is Marcus Furlong"
    date: 2006-03-23
    body: "> No it is not the same team.\n\nAlthough I have been told that Dr. Mads Haar (a Trinity lecturer) has once again done great work to help make things happen.  \n\n"
    author: "Alan Horkan"
  - subject: "Re: Who is Marcus Furlong\u00df"
    date: 2006-03-15
    body: "Am I the only one annoyed by anonymous complainers. :S"
    author: "Ian Monroe"
  - subject: "Re: Who is Marcus Furlong\u00df"
    date: 2006-03-15
    body: "He's a enthusiast KDE user, and contributes to KDE by taking a shitload of work on him. Organising aKademy is a lot of work and we're more than happy to have the local team supporting KDE in this very unique way. I'm looking forward to aKademy 2006.\n\nAnd btw, Marcus is a nice guy and until now, he's excellent to work with.\n\nSo yes, this is not a joke."
    author: "sebas"
  - subject: "Re: Who is Marcus Furlong?"
    date: 2006-03-16
    body: "Thanks a lot for your explanation. I'll trust you on this.\n\n(With the \"joke\" I didn't mean the story per se -- I meant the so called \"link\" that is given for Marcus' home page at tcd.ie; which is only slightly better than a dangling or broken link.)"
    author: "ac"
  - subject: "Re: Who is Marcus Furlong"
    date: 2006-03-16
    body: "OK -- I didn't know who Marcus was either, but obviously he is contributing in a major, major way just by organising this conference. So it is more than a little bit rude to imply that he is a \"joke\" IMHO.\n\nGo Marcus!"
    author: "Martin"
  - subject: "Re: Who is Marcus Furlong"
    date: 2006-03-16
    body: "nobody said \"he\" is a joke.\n\nbut the story was published; his name is linked to a website; such a link commonly suggests to the innocent reader that one can find more info about the person there; but that linked website is basically empty; so that link can only be seen as a kind of joke -- otherwise you wouldn't include it at all in the story."
    author: "s.o."
  - subject: "Re: Who is Marcus Furlong?"
    date: 2006-03-16
    body: "Hello all,\n\nBetter introduce myself properly, my college webpage is automagically\ngenerated and I've never used it as you might have guessed. I am a\npostgraduate student in Trinity College and a long-time KDE user. I've\nbeen using KDE since KDE 1 and I am an active advocate (the usual\nsuspects; family, friends and anywhere I have worked). I'm also going\nto be updating the KDE Ireland website, as Barry has been quite busy\nover the last few months.\n\nWhile I have not been involved in KDE development, I am currently\ndoing research on metadata generation and retrieval and the hope is to\ndevelop a prototype KDE frontend based on this work. What I'm doing is\nsimilar to DBFS (http://ozy.student.utwente.nl/projects/dbfs/) without\nthe SQL part, as we hope to store the keywords at the filesystem\nlevel. This probably all falls under the scope of the Tenor project,\nbut at the moment I am evaluating and experimenting with different\nalgorithms, so desktop integration comes later.\n\nAbout Trinity itself, around the CS department in college there are\nplenty of machines that run KDE, both staff and students. We have a\nlot a Unix machines in all the CS labs and most of the students run\nKDE in any of the classes that I am assistant in. The local aKademy\norganising team consists of a lot of people who are interested in free\nsoftware in general and while I don't think any of them develop for\nKDE (although there is one guy who gets his students to use QT for his\nC++ class), many of them are also happy users and are ready to help\nout in any way possible. Also, there are members of the IFSO who are\nstaff in Trinity and they are delighted that we are hosting the\nconference.\n\nSo apologies for the blank webpage, I'll get around to updating it one of\nthese days. And welcome to everyone who will be in Dublin for aKademy 2006,\nI can't wait to meet so many KDE contributors and I'm sure we'll have a\ngreat time and help make KDE even better than it is now!\n\nMarcus."
    author: "Marcus Furlong"
  - subject: "Re: Who is Marcus Furlong?"
    date: 2006-03-17
    body: "thank you for the infos"
    author: "infopipe"
  - subject: "Re: Who is Marcus Furlong?"
    date: 2006-03-18
    body: "Marcus,\n\nThank you for welcoming everyone to Ireland. I think this upcoming conference will be a real turning point for KDE and the free desktop and I understand and appreciate all the hard work that you will have to put in to organize an event as complex as a KDE conference.\n\nSo thank you for making it happen and pass on our appreciation to the staff and faculty of Trinity College.\n\nRegards,\n\nGonzalo"
    author: "Gonzalo"
  - subject: "Re: Who is Marcus Furlong?"
    date: 2006-03-18
    body: "Were you an undergraduate at Trinity too?  \n\nHow can people get involved and help out?  \n"
    author: "Alan Horkan"
  - subject: "I'm looking forward."
    date: 2006-03-16
    body: "Last year I've missed.\nThis year I'll take the ride.\n"
    author: "woow"
  - subject: "Re: I'm looking forward."
    date: 2006-03-24
    body: "Marcus is not very good at pub quizzes"
    author: "Bunter Chutney"
  - subject: "Galway Oyster Fest"
    date: 2006-03-26
    body: "If you like Oysters and Guiness you might also visit Galway anytime from the 28th Sept to 1st October 2006 and enjoy the Oyster Festival.  http://www.galwayoysterfest.com/ "
    author: "Alan Horkan"
  - subject: "Re: Galway Oyster Fest"
    date: 2006-03-28
    body: "At the bottom of http://dot.kde.org/1142439906/ -Alan Horkan posted a plug for this site.  This does not belong on a computer forum and is NOT the way to promote your wares, if everyone did that...!"
    author: "Mel"
  - subject: "Re: Galway Oyster Fest"
    date: 2006-07-09
    body: "Ah lighten up.\n*hands mel a beer*"
    author: "John Tapsell"
  - subject: "Ooooo Ireland!"
    date: 2006-03-29
    body: "I just got back from Dublin yesterday! (been there since st. patty's day) wow what a city!\n\nDont forget to check out the book of kells while at Trinity, and definitely do the guinness tour!\n\nAlso, eat breakfast, lunch and dinner at Lemon (amazing crepe place, like 3 minute walk from Trinity... on Dawson I think...  do it!)"
    author: "James"
---
The yearly KDE World Summit, Akademy, has found a home for 2006 on the emerald isle. This year,
the multi-day event for contributors to the leading Free Desktop
will be held from September 23nd
to 30th 2006 in beautiful <a href="http://www.visitdublin.com/">Dublin</a>, capital city of 
Ireland. Our hosts will be Ireland's oldest university, <a href="http://www.tcd.ie/">Trinity College</a>. There are three sub-events: a contributors
conference, the <a href="http://ev.kde.org">KDE e.V.</a> annual general assembly and a week long hacking session that offers the opportunity to
discuss all sorts of things face-to-face.
We also look forward to the chance to
mingle with <a href="http://www.kde.ie">local KDE enthusiasts</a>.















<!--break-->
<div style="border: solid thin grey; margin: 1ex; padding: 1ex; width: 400px; float: right">
<img src="http://static.kdenews.org/jr/akademy-2006-da.png" width="400" height="400" />
</div>

<p>"<em>We are looking forward to meeting and sharing ideas with so many
 developers of quality Free Software from the KDE community</em>"
says Glenn Strong, Trinity College Computer Science Lecturer and Chairman of the <a href="http://www.ifso.ie/">Irish Free Software Organisation</a>.</p>

<p>This year, based on a popular vote by the KDE e.V.'s members, Akademy will
be hosted by Trinity College Dublin, one of Dublin's four
universities. A team led by <a href="https://www.cs.tcd.ie/~furlongm/">Marcus Furlong</a> will
organise the conference. From September 23nd
to 30th 2006, KDE enthusiasts from around the world will converge to share
experiences, code and perhaps ale.</p>

<p>"<em>The School of Computer Science and Statistics at Trinity College is
pleased to welcome the KDE Akademy conference to Dublin this year, and
hopes the event will be constructive for all KDE contributors, and
that everyone enjoys and benefits from their stay in Dublin</em>" said
Dr. David Abrahamson, Head of the School of Computer Science and
Statistics, Trinity College.</p>

<p>The preliminary schedule for Akademy 2006 is as follows:</p>

<ul>
<li>     Friday 22nd: Arrival</li>
<li>     Saturday 23rd &amp; Sunday 24th: KDE Contributors Conference</li>
<li>     Monday 25th: KDE e.V. Membership Meeting (members only, see <a href="http://ev.kde.org/members.php">the e.V. members page</a> for how to join)</li>
<li>     Tuesday 26th to Saturday 30th: Hacking session &amp; workshops</li>
<li>     Sunday 1st: Departure</li>
</ul>

<p>A formal call for papers will be put up in due time, but you may wish to
start thinking of posters, position papers, experiences and case studies to
write up for the (peer reviewed) conference part of Akademy. For the
hacking sessions, figure out who you need some quality face-to-face time
with and convince them to come too!</p>






