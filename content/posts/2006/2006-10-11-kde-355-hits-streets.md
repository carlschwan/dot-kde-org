---
title: "KDE 3.5.5 Hits the Streets"
date:    2006-10-11
authors:
  - "jriddell"
slug:    kde-355-hits-streets
comments:
  - subject: "Slack packages"
    date: 2006-10-11
    body: "I'm currently building them. The packages will be available soon.\nSorry for that delay.\n\n"
    author: "JC"
  - subject: "Re: Slack packages"
    date: 2006-10-11
    body: "thanks!"
    author: "SaCuL"
  - subject: "Re: Slack packages"
    date: 2006-10-12
    body: "Having 2 babies at home, change the priorities.\nI hope to show them soon how to run the build scripts :))\n"
    author: "JC"
  - subject: "Re: Slack packages"
    date: 2006-10-12
    body: "Congratulations! Two babies? Are they twins?"
    author: "Jan"
  - subject: "Re: Slack packages"
    date: 2006-10-12
    body: "one is 17 months so still a baby, and the new one is 2 months old. Lots of fun and joy but it's not easy to touch a computer during the night :)\n\nBtw the packages are ready. I want to test them and updload them before the week-end.\n\n"
    author: "JC"
  - subject: "Re: Slack packages"
    date: 2006-10-13
    body: "Thanks for uploading the packages, but do you plan to upload kde for slackware 11 too?"
    author: "Blablablub"
  - subject: "Re: Slack packages"
    date: 2006-10-13
    body: "yes I do.\n\nPat will not build them for now in -current so I will upload them tomorrow.\n"
    author: "JC"
  - subject: "Re: Slack packages"
    date: 2006-10-15
    body: "The slackware 11.0 are there :)"
    author: "JC"
  - subject: "Re: Slack packages"
    date: 2006-10-17
    body: "I cannot find the kdebindings for slack 11.0... please help"
    author: "Blablablub"
  - subject: "Re: Slack packages"
    date: 2006-10-17
    body: "yes it doesn't compile. I'm checking what is the problem"
    author: "JC"
  - subject: "Re: Slack packages"
    date: 2006-10-18
    body: "Something related to python (pyqt) is not compiling. I don't know why.\n\nI will not be able to fix this until next week."
    author: "JC"
  - subject: "Re: Slack packages"
    date: 2006-10-20
    body: "You have to change all 'python configure.py -c' to 'python configure.py -i' in order to compile it with slack 11.0"
    author: "Anonymous Coward"
  - subject: "debian package"
    date: 2006-10-11
    body: "Two days ago I did apt-get upgrade my debian box and noticed that KDE 3.5.5 was already there. Nice to get fresh new release of KDE 3.5.5 ...  Btw, just wondering, before KDE 4 release there will be KDE 3.5.6, 3.5.7 and so on?"
    author: "_fred"
  - subject: "Re: debian package"
    date: 2006-10-11
    body: "I'm also running new KDE on Sid, nice :) New Kopete is really wellcome since it ads some support to jabber mucs (multiuser chats). Shame that there is not much talk on mucs on kdetalk.net server."
    author: "Petteri"
  - subject: "Re: debian package"
    date: 2006-10-11
    body: "Maybe.   This depends on how fast KDE4 gets out the door.  If it turns out that there is only a tiny amount of work left to get KDE4 out, then there will be few KDE 3 releases.  If (more likely) there is still significant work left before KDE4 is ready, then there will be more 3.5 releases as bugs are fixed and features are added (New features are discouraged, but they happen).\n\nThere may even be 3.5 releases after KDE4 is out.   There is speculation that some distributions or companies will hold off on upgrading to KDE4 for a couple years to let the bugs settle out.   In this case there may be enough people working on KDE3 (and enough changes) to justify new releases for a couple years after KDE4.   KDE4 is a large change, and many open source projects have split because some people disagreed with the change, in that case you would see releases for years. (This is a good thing about open source, but it would be bad for KDE in general)\n\nHowever KDE4 is now getting to the point where more people can work with it.   Libraries are close to stable, and so things are likely to soon reach the point where all developers are encouraged to port their applications to KDE4 (many developers are already doing this).  While things will change in the Libraries, many of the future changes will be things that experience with the new APIs shows a need to do things better.   This will cut into KDE 3.5 development.\n\nIn short:  Most likely there will be a couple more releases, but they will become smaller and smaller.   I won't predict when the last one will happen.\n"
    author: "bluGill"
  - subject: "Re: debian package"
    date: 2006-10-11
    body: "Is there an option to take a VMware or bochs or whatever image of KDE pre-4 as a development plattform?"
    author: "bert"
  - subject: "Re: debian package"
    date: 2006-10-11
    body: "Bochs would be way too slow for a development platform.\n\nJust compile \"KDE pre-4\" as another user, as per the instructions on the wiki.\nhttp://wiki.kde.org/tiki-index.php?page=KDE3To4"
    author: "mart"
  - subject: "Re: debian package"
    date: 2006-10-11
    body: "> If (more likely) there is still significant work left before KDE4 is ready\n\nThere is still a lot to do, hence the lack of a release schedule."
    author: "Robert Knight"
  - subject: "Re: debian package"
    date: 2006-10-12
    body: ">>There is speculation that some distributions or companies will hold off on upgrading to KDE4 for a couple years..\n\nCouple of years??!?\nI can understand that distributions will skip kde 4.0, but i don't think they will be waiting years before upgrading to kde4.\n"
    author: "AC"
  - subject: "Re: debian package"
    date: 2006-10-12
    body: "distributions might not wait that long, but things like kubuntu LT (long term) release and people who are actually deploying kde on the desktop, might want to stay away from kde4 for a while.\n\nand thus support for these people would still be very nice and probably needed."
    author: "Mark Hannessen"
  - subject: "LTS is good, very good."
    date: 2006-10-12
    body: "Thank you for showing good common sense.\n\nPlease understand that when you set up school, government or non-profit desktops, people expect to get at leasts five years out of their investment.\n\nIncremental bug-fix releases are much more appreciated by most users of computers than whiz-bang features that appeal to computer enthusiasts. People do not enjoy disruptive changes. This is true for home users as well.\n\nOne thing which every distribution that targets consumers should do is put /home on a separate partition during the installation process. This would make upgrades easier as most people are very concerned about losing their personal email and documents. If they could be assured that an installation wouldn't touch those, you may have more people be willing to try new software.\n\nOne last thing, I have not been much of an Ubuntu/Kubuntu fan until recently. In fact, I still the distribution still needs to improve a lot. But having a supported and free long-term release is the smartest thing that they could have ever done. This was equally the silliest thing that Mandriva, Fedora and Suse have done (18-24 months of support is not sufficient for most people).\n\nAll of these distributions have corporate releases with long-term support, but the pricing is prohibitive for most home users."
    author: "Gonzalo "
  - subject: "Title"
    date: 2006-10-11
    body: "YAY!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    author: "Name"
  - subject: "I like it"
    date: 2006-10-11
    body: "I can 'feel' the updates that were made to KDE. Good job!"
    author: "zvonsully"
  - subject: "kde 3.5.5"
    date: 2006-10-11
    body: "will you build packages for mandriva 2007?"
    author: "david"
  - subject: "Re: kde 3.5.5"
    date: 2006-10-11
    body: "Binary packages are built by the distributions, KDE only ships source code. This means that Mandriva 2007 packages will be available as soon as Mandriva provides them."
    author: "distri"
  - subject: "Excellent work on KHTML"
    date: 2006-10-11
    body: "Thanks for the speed boost :)\n\nIn particular Google Maps and Google Mail feel much faster under KDE 3.5.5 compared with KDE 3.5.4\n\n\n\n\n\n"
    author: "Robert Knight"
  - subject: "Re: Excellent work on KHTML"
    date: 2006-10-12
    body: "BTW, what has happened to maps.google.com? When I go to it with konqueror it says my browser is unsupported and just redirects me... what an insult!"
    author: "pascal"
  - subject: "Re: Excellent work on KHTML"
    date: 2006-10-12
    body: "Works for me. \n\nOn one of my systems it seems I was sending a forged user agent string (Safari). *There* I got the \"unsupported\" warning until I set it back to the standard one. \n\nSo, I suggest you check the user agent string you use.\n\n"
    author: "cm"
  - subject: "less CPU usage.."
    date: 2006-10-11
    body: "it seems like KDE 3.5.5 got some performance improvement. Gkrellm is showing mostly 0% CPU usage, before upgrade from 3.5.4 it wasn't showing 0% for 4 secs.\nkwalletmanager icon in system-tray is showing up again, if an application is using it and it is configured to show.\nmany other anoying bugs fixed, see superkaramba \"download new stuff\"."
    author: "kde4ever"
  - subject: "Re: less CPU usage.."
    date: 2006-10-12
    body: "SuperKaramba also had \"Download new stuff\" in kde 3.5.4. Am I missing something?\n\nI like that my computer now has a hibernate button in the shutdown menu :-)"
    author: "pascal"
  - subject: "Re: less CPU usage.."
    date: 2006-10-12
    body: "Oh yeah! That's cool.\n\nShame neither hibernate nor suspend actually works!"
    author: "Tim"
  - subject: "Re: less CPU usage.."
    date: 2006-10-14
    body: "it will work if you have it set up. did you really believe kde would include kernel stuff and configurations for these things to suspend?"
    author: "redeeman"
  - subject: "compiz"
    date: 2006-10-12
    body: "It seems to be missing from the article, but kde, especially kicker now behave\nquite well with compiz. Running compiz --replace in my debian sid with xorg\n7.1/i810/aiglx just works. :-)"
    author: "ac"
  - subject: "Re: compiz"
    date: 2006-10-12
    body: "Yeah, it seems that pager is now aware of all 4 beryl desktops, and able to change to any of them. However, if I change to another desktop using beryl shortcuts (mouse or keyboard), the pager still shows the previous selected desktop. Will report to bugs.kde now :)"
    author: "NabLa"
  - subject: "Kubuntu packages incomplete?"
    date: 2006-10-12
    body: "It seems the Kubuntu packages are missing \"blinken\" (needed for an update of kdeedu) and have an outdated version of Kopete (needed to upgrade kdenetwork)."
    author: "Olaf Schmidt"
  - subject: "Re: Kubuntu packages incomplete?"
    date: 2006-10-12
    body: "An even bigger problem is that the reason for the failure is not reported by Adept. You need to use the command line or Gtk equivalents to find out."
    author: "Olaf Schmidt"
  - subject: "Re: Kubuntu packages incomplete?"
    date: 2006-10-12
    body: "Hi,\ni got the same problem. It really seems to me that the repositry is not complete. Couldn't find kopete-0.12.3 anywhere."
    author: "gth"
  - subject: "Re: Kubuntu packages incomplete?"
    date: 2006-10-13
    body: "U/X/Kubuntu aren't really that great of a product. It is a pain getting new apps for them. The Ubuntu distros need a \"rolling release\" version. That way you can most always get newest versions of software."
    author: "Rick"
  - subject: "Re: Kubuntu packages incomplete?"
    date: 2006-10-14
    body: "It works perfectly fine now.\n\nOlaf\n"
    author: "Olaf Schmidt"
  - subject: "KDE 3.5 and 3.4 requirements are strange"
    date: 2006-10-12
    body: "http://www.kde.org/info/requirements/3.5.php says \"Qt >= 3.3.2 & < 4.0\" while http://www.kde.org/info/requirements/3.4.php says \"Qt >= 3.3.4\". \n\nWhy is kde-3.5 working with qt-3.3.2 while kde-3.4 is not? Or is the information provided wrong?"
    author: "anon"
  - subject: "Re: KDE 3.5 and 3.4 requirements are strange"
    date: 2006-10-12
    body: "It is possible that something that depended on 3.3.4 code was removed, or changed to work with 3.3.2.  Another possibility is that saying 3.4 depended on 3.3.4 was actually in error, and it really only depended on 3.3.2."
    author: "Corbin"
  - subject: "Re: KDE 3.5 and 3.4 requirements are strange"
    date: 2006-10-13
    body: "Most likely the 3.4 page has a typo."
    author: "ac"
  - subject: "Anti-aliasing broken?"
    date: 2006-10-13
    body: "I just upgraded using kubuntu and fonts weren't anti-aliased in a lot of places.\n\nI went into the control panel and saw that AA was turned on. I fiddled it so it'd let me save the config and did so, then restarted the apps affected and now I'm AA again."
    author: "Richard Jones"
  - subject: "Re: Anti-aliasing broken?"
    date: 2006-10-13
    body: "I, tried to change the default settings for fonts in Kubuntu Dapper, but the only thing that happend was that the fonts started to look really ugly and this is still a problem.... I've tried everything I could but still looks ugly when surfing on the web with Konqueror or Firefox. In Firefox I removed the possibility for the sites to decide the fonts themself as that made the fonts look really ugly ... I think that before we have a good way of using Antialiasing and TT fonts in X11 I cannot get my girlfriend to switch :( She says it looks too ugly...\n\nI know I'm not the only one..."
    author: "David"
  - subject: "Re: Anti-aliasing broken?"
    date: 2006-10-13
    body: "Install fonts from a windows box, then setup firefox fonts like this... http://www.asa.org.uk/NR/rdonlyres/C6E4FAD8-303D-4A4C-AC1E-8F99AD72FC71/0/firefox_fonts_colours.jpg"
    author: "Ash"
  - subject: "Re: Anti-aliasing broken?"
    date: 2006-10-14
    body: "ehmm.. antialiasing is no problem in linux, gtk2 applications do antialiasing, and so does qt/kde applications. you just either dont have any fonts you like, or didnt configure it like you want."
    author: "redeeman"
  - subject: "Re: Anti-aliasing broken?"
    date: 2006-10-16
    body: "Ok, then maybe you can explain to me why antialising looks 10 times better in Windows? I'm even using the very same fonts from my old Windows box but still it's a nightmare in my favorite dist. Kubuntu... Maybe the rendering of the fonts in X11 isn't yet 100% I am well aware that both QT & GTK applications do antialiasing but no way near that of Windows. Maybe Xorg will change all that (soon I hope)\n\nAlot of maybe's :)"
    author: "David"
  - subject: "Unable to retrieve the printer list"
    date: 2006-10-13
    body: "Please see this bug report: http://bugs.kde.org/show_bug.cgi?id=135561\n\nJust click on the Printer Manager on the task bar also get this message.\n\nI thought KDE has a quality control team (http://quality.kde.org/). It looks like at least developers did not even try to print something or just click on the Printer Manager button before the KDE 3.5.5 is released. It's a shame isn't it?  "
    author: "AC"
  - subject: "Re: Unable to retrieve the printer list"
    date: 2006-10-13
    body: "Or they didn't had your problem, just like me. Everything printer-related works fine for me with KDE 3.5.5. Are you using Gentoo, by any chance?"
    author: "Boudewijn Rempt"
  - subject: "Re: Unable to retrieve the printer list"
    date: 2006-10-14
    body: "I don't use Gentoo, I use my own, LFS. My Cups version is cups-1.1.23. According to KDE 3.5 requirement list, you need CUPS >= 1.1.9.\n\nSo I just downloaded all KDE 3.5.5 sources, compiled and installed. Note, the previous version of the KDE was removed before compiling the 3.5.5.\n\nDid you compile KDE 3.5.5 from sources? What's the version of the CUPS?\n\n"
    author: "AC"
  - subject: "Re: Unable to retrieve the printer list"
    date: 2006-10-14
    body: "No, I use Kubuntu packages -- those work. I don't know whether Kubuntu changed things in KDE or CUPS or somewhere else... LFS, even more than Gentoo, basically means you're your own distribution maker, so you're responsible for your own integration between the components of your system. In other words: if you use LFS and have a problem, the onus is on you to prove you didn't create the problem yourself, not on the KDE developers."
    author: "Boudewijn Rempt"
  - subject: "Re: Unable to retrieve the printer list"
    date: 2006-10-14
    body: "So Kubuntu has solved it for you :) Btw, what's the version of the CUPS in your system? "
    author: "AC"
  - subject: "Re: Unable to retrieve the printer list"
    date: 2006-10-14
    body: "That's why I use Kubuntu -- it saves me time for working on Krita (or cleaning out the cellar). My version of cups is 1.2.2. A curious thing is that since KDE 3.5.4 kpdf autmatically prints double-sided on my printer. I didn't know it could do that."
    author: "Boudewijn Rempt"
  - subject: "Re: Unable to retrieve the printer list"
    date: 2006-10-14
    body: "IC, this may be the issue. I use cups-1.1.23. KDE upgraded its support for cups-1.2. So KDE 3.5.5 may be working without any issue with cups 1.2.2. Looks like they did not test with cups-1.1.X. So, I'll try with Cups 1.2.2. Sorry abt your issue, I'm infact upgrading from KDE 3.5.3 to 3.5.5. I did not experience any such problem here. Thank you."
    author: "AC"
  - subject: "Re: Unable to retrieve the printer list"
    date: 2006-10-14
    body: "works on gentoo here"
    author: "redeeman"
  - subject: "Re: Unable to retrieve the printer list"
    date: 2006-10-13
    body: "The KDE quality team is baddly named.   The goal is not testing (though that is very important), but to direct new help to the place where they are needed.   Note that I said help.   They help programmers, translators, writers, testers, artists, and anyone else with a talent that wants to help KDE - but as direction of where to go.\n\nMost of the answers you get from the quality team will be \"Ask your question [some other place]\".    \n\nAn artist who is interested in kids game would find KDEgames uninteresting (as those games are aimed at an older crowd for the most part), but (parts of) kde-edu interesting - but if you are just getting into KDE you may skip that package completely.  KDE-quality will point you in the right direction.\n\nOf course people who want to test KDE are important.  That is why beta and release canidates are released - to get people to test KDE before the general release.  Not enough people test Beta and RC releases in general.  KDE quality will direct you to the kde bugs database, and perhaps the developer to talk to about any bugs you find, but they do not direct testing itself."
    author: "bluGill"
  - subject: "Re: Unable to retrieve the printer list"
    date: 2006-10-14
    body: "I'm using KDE 3.5.5 with CUPS 1.1.23 on Linux.\n\nK-Button->Control Center->Peripherals->Printers works.\n\nWhat is broken is Print System applet on the panel->Print Manager."
    author: "Unga"
  - subject: "Re: Unable to retrieve the printer list"
    date: 2006-10-15
    body: "Those who do not experience this issue, could you please display the first few lines of this file: /usr/share/applications/kde/printers.desktop\n\nThis is how its looks like in mine:\n[Desktop Entry]\nEncoding=UTF-8\nExec=kcmshell printers\nIcon=printmgr\nType=Application\nDocPath=kdeprint/index.html\nMimeType=print/manager\n\nX-KDE-ModuleType=Library\nX-KDE-Library=printmgr\nX-KDE-RootOnly=true\nX-KDE-HasReadOnlyMode=true\n\nName=Printers\nName[af]=Drukkers\n:\n:\nCategories=Qt;KDE;X-KDE-settings-hardware;Settings;"
    author: "Unga"
  - subject: "Re: Unable to retrieve the printer list"
    date: 2006-10-15
    body: "It's identical."
    author: "Boudewijn Rempt"
  - subject: "Speed of screen repaints"
    date: 2006-10-14
    body: "I use kubuntu dapper, and since I got the 3.5.5 packages from kubuntu.org it\nfeels like screen repaints overally got slower, e.g. when scrolling in \nkonqueror, or switching from one desktop to another.\nTab switching in konqueror is supposed to be faster in 3.5.5 but for me it\nfeels slower.\n\nAnyone else ?"
    author: "Yves"
  - subject: "Re: Speed of screen repaints"
    date: 2006-10-14
    body: "i'd say another thing is the culprit. painting is mostly done by qt, and qt isn't updated or changed. there haven't been any changes which would affect painting everywhere. it sounds like \"renderaccel\" \"true\" got removed from your xorg.conf or something... "
    author: "superstoned"
---
The KDE developers are pleased to <a href="http://kde.org/announcements/announce-3.5.5.php">announce the release</a> of KDE 3.5.5.  This release includes plenty of bug fixes and updated translations for 65 languages but also features improvements such as version 0.12.3 of Kopete, sudo in kdesu, CUPS 1.2 support and speed improvements in KHTML.  See the <a href="http://www.kde.org/announcements/changelogs/changelog3_5_4to3_5_5.php">changelog</a> for everything new.  Grab the source from the <a href="http://kde.org/info/3.5.5.php">info page</a>, compile with <a href="http://developer.kde.org/build/konstruct/">Konstruct</a> or get the packages for <a href="ftp://ftp.archlinux.org/extra/os/i686">Archlinux</a>, <a href="http://packages.debian.org/unstable/kde/">Debian Sid</a>, <a href="http://kubuntu.org/announcements/kde-355.php">Kubuntu</a>, <a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.5/Pardus/">Pardus</a> or <a href="http://software.opensuse.org/download/repositories/KDE:/KDE3/">openSUSE</a>.

<!--break-->
