---
title: "Krita Team Seeking Artwork for User Gallery"
date:    2006-11-01
authors:
  - "bcoppens"
slug:    krita-team-seeking-artwork-user-gallery
comments:
  - subject: "More Krita news"
    date: 2006-11-01
    body: "A review of Krita and Gimp can be found here:\n\nhttp://www.linux.com/article.pl?sid=06/10/25/1950242"
    author: "Olaf Schmidt"
  - subject: "Re: More Krita news"
    date: 2006-11-02
    body: "great to see krita start to be recognized for the outstanding work being put into it.\n\ni was at an open source friendly (practically rabid, actually =) coffee shop / book store today and one of people working there said he used ubuntu but kept windows on his machine for photoshop.\n\ni showed him krita and even after demonstrating the caveats, such as the relative slowness of krita (though i completely understand that; the krita team really hasn't had a chance to work on optimization yet. and that's ok: premature optimization is the root of many evils =), he responded with: \"wow. i'm going to have to check out kde.\"\n\n=)"
    author: "Aaron J. Seigo"
  - subject: "Re: More Krita news"
    date: 2006-11-02
    body: "I need to come up with a catchy acronym for this mode of operation.\n\nPragmatic Evangelist Advocates KDE? (PEAK)\nKDE's Own Pragmatic Evangelist Tells Evenly? (KOPETE)\n\nI could waste my whole morning on this effort, and I just might.  What the hell am I talking about?  Not only did Aaron show KDE to someone who might be interested, he did it without (presumably from the story) saying anything about gnome or hiding some of Krita's shortcomings.\n\nNo software is flawless (except what Zack writes).  And there's no reason to bash a similar product.  What's worse: Not mentioning KDE at all, or giving people false expectations?  Or speaking disparagingly of other software to imply superiority (no better than a negative political commercial that nobody likes)?\n\nPerfect example of someone sensibly introducing KDE and its applications.  Krita is great and improving rapidly, it will shine on its own."
    author: "Wade Olson"
  - subject: "great"
    date: 2006-11-02
    body: "What I can fully recommend is Wink, a great freeware tool to create online tutorials as flash animations.\n\nhttp://www.debugmode.com/wink/\n\nThe problems we face with tools like GIMP, Krita etc. is that users need to play with them to get used to the functionality. Therefore you need good example files and you need a blood-and-flesh graphic wizard who explains you what can be done and how it can be done. The real issue is not the lack of functionality (there is a photoshop gap I know) but the lack of a trained and fanatic user community. Graphic editors are powerful tools and often its not easy to make the first steps. There is so many software out there which is used but users don't make use of the functionality as intended. It can be a simple issue like serial letters. Many professional office workers who use word processors use manual edits rather then serial letter functionality because no one ever told them how to use it. Written tutorials and courses are a method. The better education stuff are task-driven online tutorials which show you what options to use in a step-by-step manner. \n\nWhen persons say they like Photoshop more it is often not because photoshop offers xy functionality, but because the learning curve is optimized. Most photoshop users only use basic functionality. For most users Elements or Paintshop would be sufficient. Krita is easy to usee too. GIMP is mature but its unique interface needs learning experience before you can start to be productive. And many concepts inplemented in GIMP are only understood when you used another editor before. Layers etc. are not self-explanatory. You have to explain by example you to use them. I really welcome the approach of the KRITA project here."
    author: "foogar"
  - subject: "Re: great"
    date: 2006-11-02
    body: "If there was such a thing as moderation, \"foogar\" should get 1 BILLION points.  He has so hit the nail on the head with the thought about demonstrating to users functionality with examples!\n\nThis should not only be applied to \"Krita\" but KDE.  For instance, unless someone points out cool things the kio_slaves can do like \"fish://\" how do you realize it can be used?  Reading a lengthy and boring manual?\n\nWe need coherent and examples on how to use Krita, KDE apps or KDE itself like the OP has stated to gets users' minds bent around the cool capabilities these programs provide.  I hope the OP's comment does not go un-noticed!"
    author: "anonymous"
  - subject: "Re: great"
    date: 2006-11-02
    body: "Seems strange, but back then I learned a lot from kde screenshots. It's a cool and easy way showing people whan can be done. And people like screenshots. ;)"
    author: "AJ"
  - subject: "Re: great like the People behind KDE..."
    date: 2006-11-02
    body: "Maybe It could be something like the People behind KDE, every week or every month whatsoever, someone demonstrates some features of a KDE program. Of course with pictures and examples....\n\nCould become popular....\n\n"
    author: "Boemer"
  - subject: "Re: great like the People behind KDE..."
    date: 2006-11-05
    body: "I second this.\n\nNow if only there was a bunch of volunteers... 8-)"
    author: "fp26"
---
With <a href="http://www.koffice.org/krita/">Krita</a>'s recent 1.6 release enhancing its usability for professional artwork, the Krita team is looking into creating a
gallery where Krita users can contribute their art made with it. Any decent gallery needs to be seeded with some initial artwork. So we are asking any
Krita user who might want to show his painting skills, to consider making us a pretty painting. With some luck, it'll get selected to be put on the site.
Read more on how to participate. <strong>Update:</strong> Linux.com has a <a href="http://www.linux.com/article.pl?sid=06/10/23/1853220">review of Krita</a> with comments from its developers.





<!--break-->
<p>
To participate, you first need to make a painting or a picture. When you are satisfied with it, save it in Krita's native file format. This is called 'Krita
Document' in the Save Dialogue, and has the '.kra' extension. If you used lots of regular layers, group layers, adjustment layers and other fancy things, we would
be pleased to see them in the saved file. Then, you need to pick a license to put your image under. We'd very much like this to be an open license, that is: a license
that permits other people to not only look at the file, but also to play around with it (while still giving you credit, of course). An example of a license that would
probably be good, might be the <a href="http://www.gnu.org/licenses/gpl.html">GNU General Public License</a>, but some of the
<a href="http://creativecommons.org/">Creative Commons</a> licenses will also be good if they are open enough. Please make sure that if you use
external material, like photographs made by other people, that you have the proper authority to use them this way.
</p>
<p>
Then, you can mail the document, together with the license information, to the gallery administrators: &lt;krita-ga<span>llery&#64;k</span>de.org&gt;. You can
also add some brief information about your work, like a description, a title, and your name. We will let you know if we select the image to appear on the gallery.
As a small reminder, if you have any problems or suggestions to enhance Krita, <a href="http://bugs.kde.org/">be sure to let us know about them</a>.
</p>




