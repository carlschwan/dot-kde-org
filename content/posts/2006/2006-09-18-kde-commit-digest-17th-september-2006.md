---
title: "KDE Commit-Digest for 17th September 2006"
date:    2006-09-18
authors:
  - "dallen"
slug:    kde-commit-digest-17th-september-2006
comments:
  - subject: "Oxygen team"
    date: 2006-09-17
    body: "I see that only nuno is submitting new oxygen icons , where is the rest of the team ?"
    author: "Mohasr"
  - subject: "Re: Oxygen team"
    date: 2006-09-17
    body: "Wait for Dublin :-)"
    author: "Konqi"
  - subject: "Re: Oxygen team"
    date: 2006-09-17
    body: "The fact the I commit alot does not meen im the only one working it meens that i like to commit has i go along, david and ken work in diferent ways.\nWe hope to upload alot of recent and new artwork in the akademy.  "
    author: "pinheiro"
  - subject: "Re: Oxygen team"
    date: 2006-09-18
    body: "I like what I see in SVN so far though :)  "
    author: "Robert Knight"
  - subject: "Re: Oxygen team"
    date: 2006-09-18
    body: "The tango-like arrows and the very cool battery icons are great! Good work."
    author: "Daniel \"Suslik\" D."
  - subject: "Battery icons"
    date: 2006-09-18
    body: "No trolls intended but about the battery icons set:\n + very attractive\n - shadows hardly noticeable, compare/contrast with shadows on the arrow icons\n - use of 4 bars to represent 5 slices of 20% is not intuitive enough. For instance look at the speed or gas-guage in a car. It does not suddenly turn red when you are approaching a limit, instead the red is always there and you can see your needle getting closer and closer to 'danger'\n - shouldn't the empty battery appear more translucent, showing its empty? Now it looks filled with some kind of gray stuff\n + instead of a murky gray filling use a wrap-around battery 'label' and show it as though we are looking through a clear container at the back/underside of the label"
    author: "Peter"
  - subject: "Re: Battery icons"
    date: 2006-09-18
    body: "its still a frist version \nThe 4 bars is the maximum number of bars i can use in a 16x16 icon 4X2(bars)+5(spaces)=13 px not much space left for the batery itself.\nhas for the other tips you point you are probly correct i will try to make them beter."
    author: "pinheiro"
  - subject: "Re: Battery icons"
    date: 2006-09-18
    body: "Do the icons have to be square?  Perhaps a battery icon would work better on its side and slightly longer than wide (eg. 22x16 or even 32x16).  \n\nThis isn't an Oxygen problem.  I find the small size of the battery status icon in both Windows and GNOME makes it difficult to show anything really useful - so it becomes necessary to mouse over the icon to get a more accurate reading."
    author: "Robert Knight"
  - subject: "Comments available for parts of Commit-Digest"
    date: 2006-09-18
    body: "Could it be arranged so one can comment on parts of the Digest and not globally as here?\n\nFor example viewing the section on k3b commits might inspire a comment, viewing its graphical changes might inspire a different kind of comment and it would be cool to make comments available right there and then.\n\nThe drawback right now is developers may have to wade through tons of off-topic stuff to find anything applicable to their work whereas a look at their work area comments should be hopefully more specific and helpful."
    author: "Peter"
  - subject: "Re: Comments available for parts of Commit-Digest"
    date: 2006-09-18
    body: "Good idea!"
    author: "ac"
  - subject: "Re: Comments available for parts of Commit-Digest"
    date: 2006-09-18
    body: "have a look at http://svntalk.pwsp.net :)"
    author: "Wiedi"
  - subject: "EBN is soooo cool"
    date: 2006-09-18
    body: "Many thanks to Adriaan and the rest of the EBN-team. This site very useful, cool and very professional. I bet a lot of companies that develop software as their core business don't have this awesome QA-tool. Thanks again!"
    author: "LB"
  - subject: "kdevelop flowcharting?"
    date: 2006-09-20
    body: "Helo,\n\nI was hearing a lot of developlement on the kdevelop front, Would like to know if something like this is a possiblity? seems to improve code visualisation/algorithm flow.\n\nyou can see a demo here:\nhttp://www.sgvsarc.com/demo.htm\n\ncheers"
    author: "anonymous"
---
In <a href="http://commit-digest.org/issues/2006-09-17/">this week's KDE Commit-Digest</a>: <a href="http://amarok.kde.org/">Amarok</a> gets the roots of support for the <a href="http://magnatune.com/">Magnatune</a> music store. Work begins on a LiveUI Designer application. Mass import of KBoard code, a lightweight canvas intended for games. Work on supporting the <a href="http://en.wikipedia.org/wiki/XML_Paper_Specification">XML Paper Specification</a> format in <a href="http://kpdf.kde.org/okular/">Okular</a>. Support for multiple galleries in Kipi plugins, used by <a href="http://www.digikam.org/">Digikam</a> and <a href="http://kphotoalbum.org/">KPhotoAlbum</a>. Support for compressed <a href="http://en.wikipedia.org/wiki/Scalable_vector_graphics">Scalable Vector Graphics</a> (SVGZ) in kdelibs. <a href="http://solid.kde.org/">Solid</a> gets Network Management and CPU Monitoring capabilities. Continued improvements in <a href="http://perso.orange.fr/coquelle/karchiver/">KArchiver</a>.


<!--break-->
