---
title: "KDE Regional Groups at aKademy"
date:    2006-09-30
authors:
  - "gventuri"
slug:    kde-regional-groups-akademy
comments:
  - subject: "Ehemm"
    date: 2006-09-30
    body: "Where are the \"KDE user groups\"?\n"
    author: "gerd"
  - subject: "Re: Ehemm"
    date: 2006-10-01
    body: "What do you mean with this? Regional groups include KDE users and developers. Or are you simply asking in which countries exist regional KDE groups?"
    author: "Giovanni Venturi"
---
Last Wednesday the KDE regional groups Birds of a Feather session took place at aKademy 2006, Dublin.  The focus of this BoF session was to share experiences that regional KDE-groups have had in building a community. A regional group is generally country based, e.g. KDE-IT for Italy and KDE-NL for Netherlands. 

<!--break-->
<p>The issues addressed were:</p>

<ul>
 <li>Attracting new community members, especially translators and documentation writers.</li>
 <li>Hosting and maintaining a regional website.</li>
 <li>Organising local events, such as tradeshows, contributor meetings and social community meetings.</li>
</ul>

<p>We discussed our experiences with the maintenance of our local KDE websites. The different regional groups have been found to need a common framework for the website, with a common part shared among regional websites and a regional part managed according the regional group's needs.</p>

<p>The common part is, for example, the news from the dot.kde.org web sites translated into the related regional language. The regional part could be articles on the KDE world (a review on Konqueror, Amarok, ... for example). These articles cannot be present on all regional websites because this should produce the double writing of an article: write it in your language and than translate it in English. This causes a lot of wasted time and not all people that want to give help knows English.</p>

<p>These changes to all KDE regional web sites will not be available soon because the framework is in preparation. It will works in this way: the administrator of a regional group will get the website structure from KDE SVN and apply to his website server. They will be adviced about changes to the PHP code through a mailing list.</p>

<p>At the moment a CMS such as Joomla or Drupal can be considered a good solution because the people who want to add content to the website do not need to know PHP or HTML. They simply insert the article and publish it.</p>
 
<p>KDE Italia divided their site with 2 kinds of content. Some translations of the very important news from dot.kde.org including KDE and some other software releases (Amarok and KOffice releases) and articles on KDE (DCOP, Konqueror) with the possibility to represent a point of reference for users and developers.</p>

<p>KDE Hispano divided the site in three parts. The announce of new software releases (KDE releases, KOffice, etc), howto articles (how to install KOffice 1.6 on Kubuntu Dapper for example) and generic article on KDE.</p>
 
<p>KDE NL has a web site system based on CVS committing.</p>

<p>The possibility of a regional KDE forum is needed to increase the contact with KDE users, but the maintenance of this requires human resource to check the messages removing spam, and avoiding flame war on the forum. At the moment only KDE Hispano has got a forum integrated into Drupal CMS and managed by 2-3 people.</p>

<p>The regional groups should also organise KDE events when possible and try to give feedback to the other regional groups on what has gone well.</p>




