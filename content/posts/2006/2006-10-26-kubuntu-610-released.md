---
title: "Kubuntu 6.10 Released"
date:    2006-10-26
authors:
  - "imbrandon"
slug:    kubuntu-610-released
comments:
  - subject: "K-Menu"
    date: 2006-10-26
    body: "What have they done with the K-Menu?\n(http://kubuntu.org/images/edgydesktop.png )\n\n-Where are the \"most often used applications\" (should be on top of \"all applications\") (This one is very useful!)\n-Where is the incremental search bar? (This one is useful!)\n\nHave they fixed Konqueror filemanager?\n\nSelecting (in Icon mode) file_003.jpg, pressing shift, and selecting e.g. file_023.jpg does not select all files between file_003.jpg and file_023.jpg but only those inside the rectangle defined by file_003.jpg and file_023.jpg."
    author: "Max"
  - subject: "Re: K-Menu"
    date: 2006-10-26
    body: "> -Where are the \"most often used applications\" (should be on top of \"all\n> applications\")\nThe \"Most Often Used Applications\" part of the menu only appears once you have used at least one application. If the screenshot is of a fresh install, no applications will show up there.\n\n> -Where is the incremental search bar?\nThere is no such thing in KDE 3.5.5."
    author: "Thiago Macieira"
  - subject: "Re: K-Menu"
    date: 2006-10-26
    body: "\"Recently used apps\" can be turned. There was an incremental search bar???\n\nThanks go out to the great people on the Kubuntu team. It's been a pleasure to work with you guys (and gals). :-)\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Re: K-Menu"
    date: 2006-10-26
    body: "I like how tight and small that menu is. :)"
    author: "Ian Monroe"
  - subject: "Re: K-Menu"
    date: 2006-10-26
    body: "\"Where is the incremental search bar? (This one is useful!)\"\n\nThats never been in the official KDE, the incremental search bar is provided by a patch developed by SuSE."
    author: "Corbin"
  - subject: "Re: K-Menu"
    date: 2006-10-26
    body: "Why is it still missing in official KDE 3.5.5? It is widely deployed and tested and a it improves usability of the K-Menu.\n\nOr will KDE 3.5.6 feature Suse's new Kickoff Menu?"
    author: "Max"
  - subject: "Re: K-Menu"
    date: 2006-10-26
    body: "Because it's debatable if the search bar really such a great improvement and couldn't be done better. Plus it's a fairly big change, so it has to be tested very thoroughly (also usability wise) to go in. As for Kickoff, it's not debateable for KDE 3.5."
    author: "Daniel Molkentin"
  - subject: "Re: K-Menu"
    date: 2006-10-27
    body: "> Because it's debatable if the search bar really such a great improvement\n\nYou have installed a new application. What do you do? Search all of your K-menu hierarchie? Is Google Picasa under Graphics/editor or Graphics/viewer or Graphics/photography or additional programs or...?\n\nKDE does not support highlighting of newly installed program entries (as WinXP does). So a search bar is the only way to quickly find a random program.\n\n> Plus it's a fairly big change, so it has to be tested very thoroughly (also\n> usability wise) to go in.\n\nIf it's released with OpenSuse than it's tested and used by a wider audience than any other developer-only feature.\n\nLook at Gnome, they are steadily improving. Keeping KDE always on the level of KDE 3.3 or 3.0 does not make things better. The K-menu was nice at KDE 3.4 times but now there are better alternatives. Waiting till everyone has switched to Gnome and then releasing KDE 4.0 won't make many people happy.\n\n> As for Kickoff, it's not debateable for KDE 3.5.\n\nKDE development seems to become more and more drooping. Where is all the innovation gone? If a new feature (like Kickoff) is better than the old one in 96% of all cases and slightly worse in the remaining 4% than it's \"not debateable\" to ship. \n\nWhat about 3.5.6 beta? If Users don't like the new features, one can always revert (or improve!!!) some features. Amarok did the same as the users did not like the new layout.\n\nKDE could be so much better by only taking the low hanging fruits.\n"
    author: "Max"
  - subject: "Re: K-Menu"
    date: 2006-10-27
    body: "Eh? Have you followed the KDE project at all for the last year? Have you completely missed the fact that the developers are working hard on KDE4 which looks like it will bring an insane amount of cool new stuff to KDE?\n\nIt sure sounds like it..."
    author: "Joergen Ramskov"
  - subject: "Re: K-Menu"
    date: 2006-10-27
    body: "Well, it's probably just under graphics. I personally love that their menu is uncluttered and simple. Some subcategories and that's it, not subcategories under subcategories. Kubuntu is the first distribution I met with a nice looking kde with not too many programs and not too many menus."
    author: "terracotta"
  - subject: "Re: K-Menu"
    date: 2006-11-05
    body: "Well, I personally don't like crippled and tiny menus like kbfx and kickoff. I need a menu with subcategories in subcategories so I can sort the entries as much as possible and am able to see the subcategories and the entries together. In kbfx you have to scroll a lot which is just horrible for me (not to mention its super tiny size), I need to have everything visible. And in kickoff (which imho looks much more usable than kbfx) you also have to scroll and you cannot look at the categories and the entries at the same time because the entries panel seems to slide of the categories (that's like the icon view mode in kcontrol, I'm feeling very uncomfortable with it). I hope KDE 4 will have a \"classic\" mode with the current kmenu style (which I like a lot) and a \"normal\" icon-sized button. "
    author: "JaKi"
  - subject: "Re: K-Menu"
    date: 2006-10-27
    body: "KDE 3.x is currently in a \"chilly\" status.  ie.  It there isn't a complete feature freeze in effect, but there are strict conditions under which new features can be added to the KDE 3.5.x series.\n\nThe reason being that most exciting stuff is happening in the KDE 4 world. \n\nHaving said that, distributions are free to make their own modifications to the KDE 3.x series which others can then use.  Suse 10.2 users will see a replacement for the K menu before KDE 4, and hopefully a few other distributions might make use of it as well. "
    author: "Robert Knight"
  - subject: "Re: K-Menu"
    date: 2006-10-28
    body: "And besides k-menu and kickoff, there is also kbfx as menu replacement."
    author: "AC"
  - subject: "Re: K-Menu"
    date: 2006-10-28
    body: "and tasty menu... ;-)"
    author: "superstoned"
  - subject: "Re: K-Menu"
    date: 2007-06-18
    body: "After using SuSE for a while, I found this feature to be very useful ever since I switched to Debian I miss this feature the most so...\n\nIs it possible to apply this patch to other distros like Debian?"
    author: "btag"
  - subject: "Kudos!"
    date: 2006-10-26
    body: "I've been using Kubuntu Edgy for about a week, and I can say it's an impressive work. Very smooth distro :)\n\nI did not like the Dapper release much, but this one feels a lot more polished. Thanks to Riddell, Imbrandon, Hobbsee, and all the others!\n"
    author: "Mark Kretschmann"
  - subject: "Re: Kudos!"
    date: 2006-10-26
    body: "Agreed.  The most useful thing for me is the patch to get rid of media:/ and replace it with /media.\n\nSeriously, this should go into KDE itself.  Accessing media files in KDE is completely crippled in a default install.  Any non-kde app (and even some KDE apps) will choke on a media:/ URL.  Kubuntu fixes this in a very nice way."
    author: "Leo S"
  - subject: "Kioslave hell"
    date: 2006-10-27
    body: "Right. Also the system:/ kioslave conflicts even with the KDE trashcan on my machine. (Not to say anything about amarok/kaffeine/xine/konqueror/openoffice etc etc etc not understanding eachother because of media:/ system:/ home:/ etc). I wonder why these thing are there first of all. While I couldn't find a real use for them, I'm quite confident that some people out there do use them on purpose. But is that use worth breaking all the file system hierarchy standards and forcing all KDE and non-KDE apps to either find a way to cope with this or simply lose compatibility?"
    author: "Gato"
  - subject: "Re: Kioslave hell"
    date: 2006-10-27
    body: "given the average user's difficulty with the unix filesystem, discussions of portability, etc and the gnashing of teeth about it a few years ago, i find these threads ironic.\n\ntrace backwards from where we are today with these urls and discover the reasons why they were added: what was the purpose, what was the motivation, etc.\n\ni'm not overly happy with how it works right now, but the \"run away! ioslaves!\" meme is lacking in appreciation of the whole problem."
    author: "Aaron J. Seigo"
  - subject: "Re: Kioslave hell"
    date: 2006-10-27
    body: ">> given the average user's difficulty with the unix filesystem,\n\nNot quite.  Maybe the average user has difficulty with the cryptic names like /usr and /etc (I know I did) but I don't believe they have difficulty with /home or /media or similarly meaningful paths.\n\n>>  discussions of portability\n\nFair enough.  I see that there is some attraction to presenting a consistent path across platforms.  I think it is misguided though.  Java tried the same thing with their UI and failed miserably.  People don't care that a Java program works the same across platforms, they just notice that it works differently than the other programs on their platform.\n\n>> i'm not overly happy with how it works right now, but the \"run away! ioslaves!\" meme is lacking in appreciation of the whole problem.\n\nI don't fail to appreciate the problem.  I'm saying that the solution is completely unusable for even the simplest use cases, and therefore is not a solution at all.  As I have said many times, a very very common task is for a user to plug in a camera or usb thumb drive and open a file on it.  If they are lucky, it will open in a KDE application that is aware of the media:/ URL and opens it correctly.  If they are unlucky, which is often the case, the application will bring up an error, which will be completely incomprehensible to the user.\n\nKaffeine says something like \"Cannot open remote files\", and any non-kde app doesn't even have a chance at opening it.  \n\nThis is not a little inconvenience for users, this is a huge showstopper that will prevent most users from using files on a removable storage device that isn't set up in /etc/fstab.  These devices (any usb mass storage device) are becoming ubiquitous because they are so useful, but with a stock KDE it is incredibly hard to do anything useful with them. \n\nI'm not saying ioslaves are bad.  Ioslaves rock for resources that are not equivalent to a local path.  fish:// if great, man:// is fine, settings:// is fine because they access resources that are otherwise not easily accessed and everything works inside them.  media:/ and home:/ are bad, because they are just pointing at a local path on the filesystem, and they break applications which try to work with those files. "
    author: "Leo S"
  - subject: "Re: Kioslave hell"
    date: 2006-10-27
    body: "Full ACK!"
    author: "Michael"
  - subject: "Re: Kioslave hell"
    date: 2006-10-27
    body: "> given the average user's difficulty with the unix filesystem\n\nI've set up Linux on my girlfriend's laptop (yes I know, big mistake, never give Linux to non-techies unless you're in for total maintenance). It took some minutes for her to get accustomed to the UNIX filesystem hierarchy - this means /home, for instance, not /etc or /usr. /etc and /usr are just intractable for non-techies and it really doesn't matter whether you expose them as real or imaginary filesystems.\n\nWhile she can certainly browse and use the filesystem, she is defenselessly delivered to the abusive kioslaves. She cannot open the AudioCD icon on the desktop with Amarok because xine won't do audiocd:/ (fixed in the meanwhile by special efforts from Amarok). She must copy all MS Office documents that she touches somewhere in /home, just to make sure that OO.o will understand the URL. And she would possibly never have found this workaround on her own, because error messages are so obscure.\n\nVirtual filesystems are great when they expose remote stuff or when they simplify APIs. But when they start obscuring the real (and working) filesystem from both the user _and_ applications, they simply become imaginary hindrances in the way of everything.\n\nAnd if the UNIX filesystem hierarchy is bad, then by all means improve the standard, don't replace it with proprietary URLs.\n\n"
    author: "Gato"
  - subject: "Re: Kioslave hell"
    date: 2006-10-27
    body: "Well the bottom line is that KDE isn't the level to handle such problems. Its a problem for likes of Kubuntu to solve."
    author: "Ian Monroe"
  - subject: "Re: Kioslave hell"
    date: 2006-10-27
    body: "So KDE should put in these ioslaves, and then each distro should patch them out again?\n"
    author: "Leo S"
  - subject: "Re: Kioslave hell"
    date: 2006-10-27
    body: "media:/ works fine here in kubuntu edgy with konq and opening a file to kaffeine, kmplayer, or amarok. It's really the only way to easily access unmounted media since nothing would be in /media till it was mounted and nothing is automounted unless you tell it to be. You also have to add some entries to your pmount.allow so the non-removable media shows up.\n\n"
    author: "firephoto"
  - subject: "Re: Kioslave hell"
    date: 2006-10-27
    body: "> media:/ works fine here in kubuntu edgy with konq and opening \n> a file to kaffeine, kmplayer, or amarok\n\nIt's not media:/ that's working fine, it's kaffeine, kmplayer and amarok that are working fine again after having found ways to mediate between media:/ and their backends (instead of achieving real progress they needed to fix this). And if media:/ is working fine for you, then try to trash a file from system:/.\n\n> It's really the only way to easily access unmounted media\n\nIt's really not the only way, you can tell kdesktop to show icons for unmounted media. \n\n> since nothing would be in /media till it was mounted\n\nOh yes there would be something, the mountpoints, and konqueror/the file dialog could be smart and give them special treatment, such as device icons and a 'mount' action. Just like media:/, but without the proprietary URLs. \n"
    author: "Gato"
  - subject: "Re: Kioslave hell"
    date: 2006-10-27
    body: "> Oh yes there would be something, the mountpoints, and konqueror/the file\n> dialog could be smart and give them special treatment, such as device icons\n> and a 'mount' action. Just like media:/, but without the proprietary URLs.\n\nEdgy includes patches to add the extra options to the mountpoints in /media, BTW.\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Re: Kioslave hell"
    date: 2006-10-27
    body: "Great. That's really a wise move from Kubuntu."
    author: "Gato"
  - subject: "Re: Kioslave hell"
    date: 2006-10-28
    body: "Um, no. They shouldn't."
    author: "Ian Monroe"
  - subject: "Re: Kioslave hell"
    date: 2006-10-28
    body: "Well, then how do you propose they \"fix\" it?  Kubuntu has done it by replacing media:/ with /media.  Is there a better way?"
    author: "Leo S"
  - subject: "Re: Kioslave hell"
    date: 2006-10-28
    body: "The ways to fix them is:\n- report as bugs OR\n- grab the source, find the problem and fix it, and send a patch.\n\nTreating media://, system:// or home:// as local directories is not that big deal, most KDE applications should be easily fixable. Non-KDE applications are a different issue though and not easily solvable. Just think about fish:// protocol, as it is not known by OpenOffice.org, you cannot simply edit the file on the remote place (I think the KIO system it will download to a temporary directory though, but I didn't check it).\n\n"
    author: "Andras Mantia"
  - subject: "Re: Kioslave hell"
    date: 2006-10-28
    body: "You are missing the point, or points actually. Fixing the problems those IO-slaves create is not the problem, the problem is that they create those kinds of bugs in the first place for no gain whatsoever. They where supposed to bring some kind of usability improvment, which clearly they don't. \n\nNobody can argue that media://cdrom or system:/media/cdrom gives any kind of increased usability, compared to /media/cdrom or /mnt/cdrom. It's even worse with home:// since it points to /home rather than the users home, so instead of ~/myfile you have to use home://username/myfile. \n\nThe only things those IOslaves give are a possibility for subtle bugs, and breaking compability with legacy applications. They should be removed from svn as fast as possible, and never talked about again other than as examples of horrible usability mistakes. \n\nThe other IOslaves, like fish:// as you mentioned, does not have any of this problems. They are quite different in the way they provide extra functionality, not redefining already existing ones. Since OpenOffice is not able to edit remote files anyway, fish:// does not change anything or introduce a bug. While home://username/myfile.odt does, since its actually ~/myfile.odt a simple local file witch should be editable."
    author: "Morty"
  - subject: "Re: Kioslave hell"
    date: 2006-10-28
    body: "Full ACK\n\nI'm a huge fan of KDE's ioslaves except for media:/ which is driving me insane. Autodetection of removable devices is great and necessary, but the path which is given to applications should be /media. Everything else is simply broken."
    author: "anonymous coward"
  - subject: "Re: Kioslave hell"
    date: 2006-10-28
    body: "> Nobody can argue that media://cdrom or system:/media/cdrom gives any kind of increased usability, compared to /media/cdrom or /mnt/cdrom.\n\nIt's way too Linux-specific. I'm used to /vol/local/dvdX for local and /vol/pool/dvdrX for drives at the server. media://dvd is much more simplier and just works, no matter which station you log on. Ok, maybe non-KDE apps have probs with this, but they can't handle fish:// as well.\n\n> It's even worse with home:// since it points to /home\n\nYep, as that's what it is supposed to do. For your own home theres ~. But after you typed /users/bkoffice/8/bentruth a dozend times you'll start loving home://bentruth.\n\nAnd no, I'm not running KDE as root, just Kate via sudo."
    author: "AJ"
  - subject: "Re: Kioslave hell"
    date: 2006-10-29
    body: "> It's way too Linux-specific.\n\nAs someone else already reminded, being the same on all platforms is not always good. Java has swing which looks the same on all platforms. Some developers love this, but the rest of the people either don't know about other platforms, or they're at no advantage anyway. They only notice that it doesn't look as it should on their platform. This is why SWT was introduced, and this why I don't know of any successful Swing app.\n\nSo really, what's the point that I could (mis-)use the same non-standard URL on other operating systems? My problem is that it's non-standard on my system.\n\nBTW, I'm a FreeBSD user, not a Linux user, and I too have /media and /home.\n\n> I'm used to /vol/local/dvdX for local and /vol/pool/dvdrX \n> for drives at the server.\n\nYou represent a very small minority that uses many machines with many different operating systems with servers that do removable media on a regular basis (most servers really don't). This is unlikely KDE's main target, and thus it's not a reason to invent imaginary filesystems.\n\n> media://dvd is much more simplier and just works, \n> no matter which station you log on.\n \nThat's easy, just create a \"/dvd\" symlink on all machines. You can even automate the process provided you can detect the OS from a shell script.\n\n> Ok, maybe non-KDE apps have probs with this, \n> but they can't handle fish:// as well.\n\nNot just non-KDE, but KDE apps too, unless someone patches them to make them work. And maintains the patch, instead of doing cool stuff, and writes some more patches for the new monsters - you don't assume media:/, home:/ and system:/ will be the last of them, do you? Just wait for doc:/info that will point to /usr/share/info and system:/doc/info etc etc. (Don't worry, just kidding)\n \nAs for fish:/, fish is different because it introduces new functionality. Without the fish KIOslave there would be no fish at all ;-). But /media would be there anyway, so no need to replace it with something that doesn't work.\n\n> But after you typed /users/bkoffice/8/bentruth a dozend times \n> you'll start loving home://bentruth\n\nAgain, symlinks are your friends:\n\nln -s /users/bkoffice/8 /home\n\nBesides, the file dialog has autocompletion so it's not that bad anyway. And inventing proprietary URLs just for the sake of shorthand is silly.\n"
    author: "Gato"
  - subject: "Re: Kioslave hell"
    date: 2006-10-29
    body: "I am totally baffled at why you assume that every KDE system on earth should have /media or that the user will even be allowed to create it.\n\nWhat KDE is and was for a long time already, or so I believe, is a desktop for a wide array of systems. One of its strengths being that the KDE framework solves the problems for the applications just fine.\n\nMy understanding is that media:// should work fine for every KDE application and that's OK. Where it does not, it's probably worth to note, that these apps have attempted to improve over the framework, so there is a need, point proven.\n\nStop telling KDE it should not provide its own ways of doing things. We actually like innovation around here.\n\nYours,\nKay\n"
    author: "Debian User"
  - subject: "Re: Kioslave hell"
    date: 2006-10-29
    body: ">> I am totally baffled at why you assume that every KDE system on earth should have /media or that the user will even be allowed to create it.\n\nThe user doesn't create it.  That's up to the distribution/system.\n"
    author: "Leo S"
  - subject: "Re: Kioslave hell"
    date: 2006-10-30
    body: "> I am totally baffled at why you assume that every KDE \n> system on earth should have /media\n\nI don't assume that at all, I only assume that the fact that I have non-standard KDE URL (media:/) that just happens to be the same on other operating systems (that I don't use) is of no use to me.\n\n(And I'm not just egoistically thinking about me, when I say \"no use to me\" it's just a way of speaking.) "
    author: "Gato"
  - subject: "Re: Kioslave hell"
    date: 2006-10-30
    body: "> Stop telling KDE it should not provide its own ways of doing things. \n> We actually like innovation around here.\n\nBy all means I'm not trying to tell KDE not to do. I'm not the one to do that, and besides I too like innovation. I'd like a KDE way of doing things, such as making hardware access 'just work', transparently to the user.\n\nBut media:/ and system:/ and home:/ are not new ways of doing things, they are just new ways of calling things. Same old things, new names, new misunderstandings, new breakage. Worst of all, new filesystems to learn. Real innovation would be getting rid of all filesystems in the user interface, not adding new ones on top of old ones.\n\n"
    author: "Gato"
  - subject: "Re: Kioslave hell"
    date: 2006-10-29
    body: "> That's easy, just create a \"/dvd\" symlink on all machines. You can even automate the process provided you can detect the OS from a shell script.\n \nDetecting the OS is not the issue - always the same. But \"all machines\" is wrong. There are only a few servers, 20 at the moment, but more than 300 Workstations. X session runs on the server, but drives are workstation-local. Good luck with symlinks... ;-) (kdm runs locally, reowns the drive to the user logged in and ncs a script on the server which automounts, kinda nasty but works)\n\n> Not just non-KDE, but KDE apps too, unless someone patches them to make them work.\n\nIf they use the kio api, all kioslave should work or it's a bug to be squashed. I've never had probs with media:/ fish:// sftp:// or camera:/ (at home). system:/ or settings:/ are rarely used, ok, but they still work. audiocd:/ does, too.\n\n> Again, symlinks are your friends:\n> ln -s /users/bkoffice/8 /home\n\nBad luck. You'd have to link /users to /home (not everyone is in backoffice), but it wouldn't help. Not everyone has their homes in /users, but in /u (I'm there) or the Linux-like /home (for some local only logins).\n\n> And inventing proprietary URLs just for the sake of shorthand is silly.\n\nThey are not proprietary, they're well documented and free to use (or ignored)."
    author: "AJ"
  - subject: "Re: Kioslave hell"
    date: 2006-10-30
    body: "> They are not proprietary, they're well documented and \n> free to use (or ignored).\n\nThere are at least two ways of speaking about proprietary. In one way you're right. But there's the other way: a method of software interaction can be called proprietary when it's free and open, but it's not an agreed upon standard and it requires special efforts from the part of other people in order to work. \n\nThis means that even if you give other people the tools to unbreak the compatibility that you have just broken, your URL language is still proprietary until it becomes an agreed upon standard."
    author: "Gato"
  - subject: "Re: Kioslave hell"
    date: 2006-10-29
    body: "You write...:\nYep, as that's what it is supposed to do. For your own home theres ~. But after you typed /users/bkoffice/8/bentruth a dozend times you'll start loving home://bentruth.\n\nIf you'll allow using ~ for your own home, then how is home://bentruth any better than ~bentruth?"
    author: "AC"
  - subject: "Re: Kioslave hell"
    date: 2006-10-28
    body: "> The ways to fix them is: report as bugs\n\nSorry but people only report bug when they are worth fixing. Fixing the bugs introduced by the imaginary filesystems is just absurd. It would only mean piling hacks on top of hacks on top of hacks. And all you'd get is fixing other hacks (the proprietary URLs themselves). Just to get back functionality that has always been there to begin with.\n\nThe real way is not to cure the symptoms, it's to cure the disease. To eliminate the cause."
    author: "Gato"
  - subject: "Re: Kioslave hell"
    date: 2006-10-28
    body: "There is no need for hacks over hacks, only right way of using the KDE API and creating the correct desktop files. But whatever, I tested media:/, system:/ and home:/ URLs with KDE and non-KDE applications mentioned here. Well, amarok, kaffeine, konsole, xine and openoffice has no problems. For those who doesn't understand the URL is translated to the real path. Eg. open a file in oowriter from home:/user and when you try to save it, you will see in the file dialog: \"/home/user\" as the current directory. I'd say they work unless you give a non-working example. And I'd say this proves that if they don't work, it's nothing else, but a bug.\n Just for the record: self-compiled KDE 3.5.5 on SUSE 10.1.\n Oh, and I like the media:/ slave (i don't use the others) as it is much easier to use and remember than some path that can be different on every distribution."
    author: "Andras Mantia"
  - subject: "Re: Kioslave hell"
    date: 2006-10-29
    body: "The reason why some apps don't have problems any longer is that they've been hacked to either translate URLs or copy files to temporary locations etc. And these hacks need to be maintained, and new hacks will need to be written in order to accomodate new needs. \n\nAll of this just for the sake of having the same URL on all distros. And the distros already use the same URL, '/media'! Unless they're not FHS compliant (very old Linux? IRIX?). \n\nAnd even if they did use different locations for mount-points (which they don't), what would be more important: \n\na. having URLs that can be understood by all apps on _your_ system, or \nb. having URLs born incomprehensible (albeit partially resurrected by haunted application developers ;-) ) that look the same on all distros? Who cares about all distros?\n"
    author: "Gato"
  - subject: "Re: Kioslave hell"
    date: 2006-10-29
    body: "Have you ever used the KDE API to write applications? If you stick to KURL everywhere you will have hard time to break media:/ or system:/ or whatever. It just works. And if you don't use KURL e.g because you have to pass the files to a non-KDE application (like xine) just use \"Exec=appname %f\" in you appname.desktop file instead of \"Exec=appname %U\" and KDE will download the file to a temporary location before passing to the application.\nReally, there is no need for hacks unless in very few cases and even than it should be a minor issue to make it work. And for me both a and b is important and I would fix a rather than just throw away something because some apps have issues at this moment. But what apps, please tell me and I will test here as well...\n"
    author: "Andras Mantia"
  - subject: "Re: Kioslave hell"
    date: 2006-10-29
    body: "The problem is that KURL does not really help you in most of the bugs caused by this, the remote IO-slaves have never been a problem. The problem are the   unnecessary local ones like media:/. They have created bugs for every application that have the need for treating local files different then remote files. Lots of applications like Kaffeine, k3b, codeine etc have special handling of cases when these URLs are used to to make them be seen as local files. And none of these applications had this before being notified by a bug report. So it simply does not just work.\n\nAnd it's not about throwing something away because some apps have issues, it's about throwing something away that does not give any benefit over the systems existing solution. And in addition it introduces unnecessary problems and create issues for applications. IO-slaves are great, but they are not the solution to everything. "
    author: "Morty"
  - subject: "Re: Kioslave hell"
    date: 2006-10-29
    body: "There is only one specific things with media:/ and co. they are local even if they don't look so. The only case when an application has to do something special is when it is a KDE application, uses KURL, but doesn't support remote operation. Why? Because if its a KDE application and uses only KURL inside it simply shouldn't care if the KURL points to a remote or local file. If it's a KDE application, but works only with local paths and uses QStrings to store paths, it should specify that it cannot open remote URL's (uses filename %f in the .desktop file). In case of non-KDE applications, the situation is the same.\nSo what is in the case of using KURLs and not supporting remote files? I think this was the case of Kaffeine, Amarok and K3B as all of them pass the paths to third party non-KDE applications who don't no anything about KIOslaves. Well, just check the sources for K3B for the \"hack\":\n  if( !url.isLocalFile() ) {\n    return KIO::NetAccess::mostLocalURL( url, 0 );\n  }\n\nThe hack you are looking for is \"mostLocalURL\". The function is there since KDE 3.5.0, released quite some time ago.\nI simply don't buy the argument that this idea is broken and needs hack in the applications. In case somebody shows me real examples (none-working apps or extra hacks because of this) I can discuss more.\n\nAnother example is from amarok:\n // Note: remove for kde 4 - we don't need to be hacking around KFileDialog,\n // it has been fixed for kde 3.5.3\n else if( protocol == \"media\" || url.url().startsWith( \"system:/media/\" ) )\n {\n ...\n }\n\nSo if there was a bug, it was fixed in 3.5.3. Bugs can exists, this is why they should be reported so they can be fixed either in libraries or applications."
    author: "Andras Mantia"
  - subject: "Re: Kioslave hell"
    date: 2006-10-29
    body: "For KMPlayer, the mostLocalURL isn't such a nice solution. All KParts targeted for KHTML should not use local loops. The problem is that JS setTimeout() or other redirect ways, may fire inside such a local loop, causing a page reload. This will most certainly crash konqueror."
    author: "Koos"
  - subject: "Re: Kioslave hell"
    date: 2006-10-29
    body: "I admit I never heard that you have to be so careful with KParts loaded inside KHTML, but in this case the problem is rather the hack used in NetAccess, than media:/ or mostLocalURL. This simply means you can never use KIO::NetAccess, right? I find this a particular case, wrong interaction between two applications, and in this case I admit a hack (or non-standard solution) is needed, like manually find out if it's a media:/ or whatever URL and translate yourself to the real path."
    author: "Andras Mantia"
  - subject: "Re: Kioslave hell"
    date: 2006-10-30
    body: "> but in this case the problem is rather the hack used in NetAccess, \n> than media:/ or mostLocalURL\n\nFrom a pragmatic point of view it doesn't really matter whose fault it is. An unnecessary feature that triggers bugs in other features is still a mistake, no matter who has written the bugs."
    author: "Gato"
  - subject: "Re: Kioslave hell"
    date: 2006-10-29
    body: "Look, I'm interested in use cases, cause that's where I run into these problems.  As you mention, Kaffeine seems to be fixed, at least in Kubuntu 6.10 I can open a video file from media:/usbstick and it will work (it translates it to /media/usbstick, no idea what it would do on other distros that have no /media)\n\nHere's a big one problem though.  I plug in my flash drive, and want to get there in a konsole window.  Now I could press F4 and it would work (on Kubuntu), but what if I have an already open konsole window?  Intuitively I would type something like cd media:/usbstick.  That obviously doesnt work.  This would make no sense to users.  Yes I know I can use some kde inbetween thing to do it, but I have no idea how, and if I did look it up, I would forget it immediately because it's so obtuse.\n\nNow I want to open a file in helix player.  If I open helix player and try to open a media:/ URL it won't work.  Once again, this makes no sense to users.  Why does a path that works in kaffeine not work in helix-player?  Completely mystifying.\n\nThe point behind all this is that it causes very real problems for a very slight, if any gain.  For some corner cases, the media:/ path is easier to understand, but for many more cases (anyone that doesn't use purely KDE software) it's a very big and very real problem.  \n\nIn my mind, this is a poor tradeoff.  Anyway, I've complained enough about this.  Time to move on :)  Well done Kubuntu for fixing this critical issue!"
    author: "Leo S"
  - subject: "Re: Kioslave hell"
    date: 2006-10-30
    body: "Hm.\n\nOK then you're right, there are correct ways of handling most parts of the problem. But why handle the problem and maintain that handling when it's far easier to not have the problem at all? Why not just use the standard filesystem?"
    author: "Gato"
  - subject: "Re: Kioslave hell"
    date: 2006-10-27
    body: "Given that in (K)(X)Ubuntu Edgy Eft the file system is reduced to /home and /media for non administrator people I don't think it's a problem. Just as someone stated above: if I right-click in a folder in media:// I have no option of opening Konsole (a KDE apllication!!!!) in that same path, where I can do this with /media/...folder...\n\nKioslaves are nice, but kioslaves for hierarchy replacement just don't work.\nI think they handled the filesystem hierarchy very nicely in Edgy Eft."
    author: "terracotta"
  - subject: "Re: Kioslave hell"
    date: 2006-10-27
    body: "> given the average user's difficulty with the unix filesystem\n\nAs a matter of fact I do agree that the filesystem doesn't belong in the user interface at all, but then, kioslaves are filesystems themselves, so what's the point? What's it use to replace a black & white devil that's been known to applications with a pack of blue KDE devils only (sometimes) known to (some) KDE applications?"
    author: "Gato"
  - subject: "Re: Kioslave hell"
    date: 2006-11-05
    body: "I wrote a couple fairly long messages about them around 1998 or so on the KDE mailing list.  I thought that kfm would benefit greatly from using URIs as laid out in the various RFC, which has things like tv:/ and such that had never been implemented.  The idea that a universal name for a resource can be used anywhere is powerful and provides a base needed for advanced use, both automated (i.e., the user never sees it), and practical for the user (i.e., using printers:/ to see all printers available on the network).  Things like Solid in KDE4 can now benefit from static URLs to dynamic resources and subresources.\n\nKeep in mind that KDE is for multiple operating systems.  A printer or cdrom in AIX looks different than on BSD or on Linux.  Soon OSX and possibly Windows will be added to the mix.  It's easy to say \"just use /media\"... if you only want it to work with those specific versions of those specific versions of those specific operating systems that choose to use a /media directory.  But a working KDE desktop on *any* operating system will display their user available resources the same using the URL.  Or at least that's the idea."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Kudos!"
    date: 2006-10-27
    body: "I also agree about the media:/ hell. One of KDE's dumbest features, there is no doubt about it."
    author: "Mark Kretschmann"
  - subject: "Re: Kudos!"
    date: 2006-10-28
    body: "well, it was supposed to fix a real problem. it doesn't, well, that's a mistake. mistakes happen, it's part of evolution. now let's try to get kubuntu's solution in KDE so we can fix it..."
    author: "superstoned"
  - subject: "couple of gripes"
    date: 2006-10-26
    body: "There are two main complaints I have with edgy (I had to switch a while ago thanks to the intel/jmicron sata issue that prevented me sticking with dapper)\n\n1. The system settings applet is missing some (IMHO) important controls from kcontrol. The main one for me is the 'launch feedback' control. The bouncy icon setting drives me nuts so it's litteraly the first thing i change on a fresh install. It's not in SS so you have to run kcontrol from console to fix that. I have nothing against distros reorganizing things to test usability ideas etc, but it really bugs me when that includes dropping functionality coghGNOMEcough to 'improve' usability\n\n2. The hiding of everything except /media and /home in the file dialog is a realy big issue for me. I went to add a mount point for my file server through the GUI and whoops no /mnt even though I'm in the 'advanced' tab of SS and have given the admin password. OK update fstab by hand. Then I go to point Amarok at my newly mounted NFS share, where my music lib is. No /mnt in the file dialog and no inclination to figure out how Amarok's config files work to add that by hand... so off to google and kubuntu forums I go (after first going through SS for 1/2 an hour trying to find a way to override this behaviour) and eventually find that there is something called a .hidden file I can edit to get my file system back. Not exactly an intuitive friendly interface. If they want this (very dubious IMHO) improvement then at least make it overridable in some easy to find way instead of costing experienced users (still the bulk of the userbase) a bunch of time looking to get back functionality we've had for years.\n\n</end rant>\n\nOtherwise, nice graphics improve, supports jmicron properly, starts up REALLY quick, automatix gets you your deliciously evil codecs etc all sorted out (happy halloween... how does one dress up as as the DMCA?) and so far, so stable even through the tail end of the beta stage and using fairly 'cutting edge' hardware."
    author: "borker"
  - subject: "Re: couple of gripes"
    date: 2006-10-26
    body: "Regarding 1), these balancing acts are not easy.  The classic KDE approach of making everything configurable in a huge settings dialog is, in my opinion, definitely not the way forwards.  It becomes a warren in which more important settings ( ie. those settings which a larger proportion of the target users are likely to want to change ) are hard to find and the settings dialogs become hard to test properly.\nI like what the Kubuntu folks are trying to do with System Settings, it isn't surprising that they didn't get it absolutely right first time.\n\nRegarding 2), I think a good compromise would be to separate out the \"home\" and \"media\" folders from the others in the folder view rather than hiding them entirely.  \n\nBy the way - please file bug reports or post feedback on the wiki for both the above if you haven't done so already."
    author: "Robert Knight"
  - subject: "Re: couple of gripes"
    date: 2006-10-27
    body: "Yup, I agree it is a balancing act, but KDE's functionality is one of the fundamental reasons I use it instead of other DEs, so I think for distros to go around removing that which makes KDE the better option for some of us is doing it a disservice. As I said I have no issues with improving the access to functionality... the basic/advanced split in systems settings is an example of that, I just don't want to lose functionality or have to start digging for it in conf files all the time."
    author: "borker"
  - subject: "Re: couple of gripes"
    date: 2006-10-27
    body: "Ooo!  So that's why I can't find my filesystem from some dialogs!\n\nI thought it was a serious bug.\n\nWhat do I have to edit to get rid of that?"
    author: "John Tapsell"
  - subject: "Re: couple of gripes"
    date: 2006-10-27
    body: "have a look in /etc for something called kubuntu-default-settings (typing from memory... its something like that anyway) and there in there is a file called .hidden that file has the list of dirs it will hide from you... remove the ones you want to be able to see"
    author: "borker"
  - subject: "Re: couple of gripes"
    date: 2006-10-27
    body: "I installed edgy at home yesterday. At my office I've still been waiting until things have settled down. I noticed the point #2 you mention just now because I didnt try to open anything before. Ugh!!! What a mess. This spoils my otherwise really great impression of Edgy totally. All my folders have gone. This sucks a lot. I cant even access my /mount folders if I enter them manually. This should really be configurable immediately from the open dialog. This is even worse than Windows where I can still click to show i.e. the Program Files folder. What the hell did they think they are doing - if anything at all? I sincerely hope this gets better again in future versions, otherwise I will keep out looking again for a different distro if they continue GNOMEifying KDE.\n"
    author: "Michael"
  - subject: "Re: couple of gripes"
    date: 2006-10-27
    body: "PS: I just found out how to solve this. Edgy created a \".hidden\" file in \"/\" (root folder). You can remove it in order to show all folders again. Anyway - it's absolutely inexcusable that they didnt warn explicitly before downloading that I wont be able to open files properly if I dont remove the file. If I had upgraded my office machine I could not have used it for work before solving this. This sucks really a lot!"
    author: "Michael"
  - subject: "Re: couple of gripes"
    date: 2006-10-27
    body: "> This is even worse than Windows where I can still click to show i.e. the Program Files folder.\n\nFrom Konq or the file dialog use the \"Show hidden files\" menu item and they are there. Typing in the first part of the path also works fine too.\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Re: couple of gripes"
    date: 2006-10-27
    body: "Not in Konq. I'm talking of the file open dialog. Doesnt work here. I can enter i.e \"/mnt\" or \"/\" and press enter in the drop-down list at the top. No folders appear.\n"
    author: "Michael"
  - subject: "Re: couple of gripes"
    date: 2006-10-27
    body: "You can right-click in the dialog box, select View -> Show hidden files or press F8.\n\nTrue, there should have been more information about this. Even the release notes are silent about this new feature. This would probably be the biggest source of complaints.\n\n... I'm still waiting for that wiki page."
    author: "Jucato"
  - subject: "Re: couple of gripes"
    date: 2006-10-27
    body: "There is this page: <a href='https://wiki.ubuntu.com/KubuntuKDEMedia#preview'>wiki</a> (you can see my comments from a few days before the release at the bottom tagged with ---borker)\n\nI also filed a bug about 2 weeks ago but it was closed pretty much straight away with no action taken."
    author: "borker"
  - subject: "Re: couple of gripes"
    date: 2006-10-27
    body: "Why do you insist on mounting the stuff you mount manually in /mnt (or in /mount as someone else here posted?), while you can mount it in /media, that's what that folder was created for I thought, the name of /media sais a lot more than /mnt or /mount to non-technical people, and it's not hard to find out for technical people. No usability is given up and things have become a lot more clear. In konqueror you can still access the *hidden* files by entering the path in case you need to dive into the hierarchy.\n(ok, I don't understand why typing /usr or /mnt in the file dialog doesn't work anymore, it's clearly something that should be enabled, just like in konqueror, but generally IMHO this solution is great)."
    author: "terracotta"
  - subject: "Re: couple of gripes"
    date: 2006-10-27
    body: "No, /media was created for media devices... cdroms, floppies, usb drives etc. /mnt is where you (/)mount stuff to. I'm not talking about konq, I know I can enter paths in that, but I can't use it to set the lib location in Amarok or point an application at my web source stuff /var or any other number of things I'd like to use my file dialog for.\n\nI personally don't like this solution much, but I wouldn't have to care if it was easy to get rid of. My biggest objection to it is that I lost a big chunk of functionality with no warning and no way to get it back obvious. I shouldn't have to fight my system and scour the web to open a file."
    author: "borker"
  - subject: "Re: couple of gripes"
    date: 2006-10-28
    body: "If /media was created for media devices only, why are hard drives mounted to /media by default, then? What are hard drives? Aren't they media as well? Maybe you meant removable devices? But if /media is for removable devices only, and /mnt is for \"a temporary mounted filesystem\" only (check your FHS), where do you put permanent/standard mounts? Kubuntu has chosen to do that in /media. Most new users would mount to /media (if and when they know how to), anyway.\n\nAnyway, you are supposed to mount to /media, now. If you still prefer to use /mnt,  you can easily make /mnt appear again by editing the /.hidden file and removing /mnt. This will make it visible again in Amarok."
    author: "Jucato"
  - subject: "Re: couple of gripes"
    date: 2006-10-28
    body: "According to the FHS, /media is for removable media:\nhttp://www.pathname.com/fhs/pub/fhs-2.3.html#MEDIAMOUNTPOINT\n\nIt doesn't seem particularly explicit wrt. whether it's exclusively for removeable media though.  Either way, /mnt is still an acceptable place to mount stuff.  It certainly doesn't appear to be deprecated."
    author: "Martin Ellis"
  - subject: "Re: couple of gripes"
    date: 2006-10-28
    body: "Nobody said it wasn't acceptable, but /media is acceptable as well and a lot more clear to what it is used for. Not used hard disk partitions are mounted there as well, while in for example media:/ all hard disks (even the one mounted to / ) are shown there (which is irritating to me, but that's a personal opinion). From the beginning of Ubuntu their preference of mounting stuff to /media was quite clear. The only thing they need to work out now, is a way to show the not mounted optical drives or removable media that are in the drive or in the usb-port to show up like in media:/."
    author: "terracotta"
  - subject: "Re: couple of gripes"
    date: 2006-10-30
    body: "Oh, I interpreted \"Anyway, you are supposed to mount to /media, now\" as a statement that things shouldn't be mounted there.\n\nBesides, even if you do mount hard drives to /media, this isn't about mounting hard disks.  The thread is about mounting NFS volumes."
    author: "mart"
  - subject: "Re: couple of gripes"
    date: 2006-12-19
    body: "Thank you very much for this posting. I just had the same problem. :)\necho > /.hidden"
    author: "panzi"
  - subject: "Fedora Core 6 released too"
    date: 2006-10-27
    body: "Hey, why is this newsworthy and FC6 is not? Fedora also includes KDE! Fedora Core 6 has been released with KDE 3.5.4, but 3.5.5 is being prepared for FC5 and FC6 updates, so we Fedora users will have it soon too. (And unlike Kubuntu, we actually get the new versions as updates, 3.5.5 is going even to FC5.)"
    author: "Kevin Kofler"
  - subject: "Re: Fedora Core 6 released too"
    date: 2006-10-27
    body: "Both are newsworthy. It's just that the people who post to the dot are volunteers, and no one posted anything about fedora. But you can try to..."
    author: "aac"
  - subject: "Re: Fedora Core 6 released too"
    date: 2006-10-27
    body: "I did (right after I posted that comment), let's see if it gets accepted. :-)"
    author: "Kevin Kofler"
  - subject: "Re: Fedora Core 6 released too"
    date: 2006-10-27
    body: "Or it's because the main Kubuntu developer is a dot editor..."
    author: "Anonymous"
  - subject: "Re: Fedora Core 6 released too"
    date: 2006-10-27
    body: "No, it's because somebody from the Kubuntu team (not Jonathan) posted an initial story that was not just a copy from the official announcement and outlined things specifically relevant to KDE. That's the kind of posts that get accepted the fastest, simply because reworking entire announcements takes a lot of time to reedit. It's really that simple."
    author: "Daniel Molkentin"
  - subject: "Re: Fedora Core 6 released too"
    date: 2006-10-27
    body: "Update: I edited and posted the fedora story now."
    author: "Daniel Molkentin"
  - subject: "Re: Fedora Core 6 released too"
    date: 2006-10-29
    body: "thats 4 sure teh reason why kubuntu gets that much place to spread their shit..\nwhether ubuntu nor redhat want support kde.. so why give them this place?\nkde on fedora/kubuntu is just disappointing, i dont kno about suse, but i m sure that new kde menu in suse is not what an usual kde user wants to see..\n\ni d be happy if there was a pure kde distribution, i dont want OOooOooo nor IceWeaselBeaselFoxOnFirePowerXPRebrand31337\nbigger customizings/rebrandings should be done by the original developpers otherwise we get just broken builds (debian/ubuntu repos are full of them)\nmaking kde look like gnome and removing kde-features, breaking the genius localization in kde is what they ve done..\n\nok?\n"
    author: "noone needs me"
  - subject: "Re: Fedora Core 6 released too"
    date: 2006-10-27
    body: "> Hey, why is this newsworthy and FC6 is not? \n\nPerhaps simply because nobody submitted a story about FC6?    \n\nKubuntu had a very strong presence at this year's Akademy and I think there are a lot of KDE people following it closely.\n\nBut heck, if Fedora have done something cool with KDE then please tell the world :)"
    author: "Robert Knight"
  - subject: "Re: Fedora Core 6 released too"
    date: 2006-10-27
    body: "By the way, I haven't given up on packaging KDE 4 snapshots for Fedora yet, I have just been busy. I'll let you know when we get these up. :-)"
    author: "Kevin Kofler"
  - subject: "Re: Fedora Core 6 released too"
    date: 2006-10-27
    body: "> Hey, why is this newsworthy and FC6 is not?\n\nBecause Kubuntu is explicitly KDE-centric. Fedora Core isn't.\nMind, it's very cool that FC supports KDE and I'm sure their developers did a great job with this release. It's just that this site is about KDE *itself*. I have no doubt you love FC, and that's all fine and dandy; but are you certain that the release of any distribution, however great, where KDE is only optional, is newsworthy on a KDE site?\nIf they made KDE the default, then alright, that's newsworthy and I'm wrong. I just heard of no such thing as of yet."
    author: "A.C."
  - subject: "Still buggy"
    date: 2006-10-27
    body: "I'll been using Edgy for some time - now it's far better than month a go, but still far far away from Dappers stability. \nXorg i810 drivers for i855gm still are broken. Have to wait for Edgy+1; \nRight click in Konqueror causes occasional segfaults (don't like subversion desktop entry) (this one really sucks, as it comes up in most inappropriate times, and, yes, I have filled bug in that annoying bugtracker (launchpad));\nAnd there is no KOffice 1.6."
    author: "MZM"
  - subject: "Re: Still buggy"
    date: 2006-10-27
    body: "> And there is no KOffice 1.6.\n\nDoes this mean no Krita?"
    author: "Max"
  - subject: "Re: Still buggy"
    date: 2006-10-27
    body: "nope , krita is installed by default also , koffice 1.6 was just released to late in the cycle to include, it will be avaible via kubuntu.org though soonish\n\n"
    author: "imbrandon"
  - subject: "Re: Still buggy"
    date: 2006-10-28
    body: "KOffice 1.6 has been available to Edgy (and Dapper) since it was released. But like what imbrandon said, it was too late in the development cycle to include it. Here's the link on how to get KOffice 1.6 in Kubuntu:\n\nhttp://kubuntu.org/announcements/koffice-16.php"
    author: "Jucato"
  - subject: "Not spectacular"
    date: 2006-10-27
    body: "I guess it is okay...\n\nMy complaints:\n\n1) The crippled Konqueror profiles. Yes, there are instructions on how to restore the default profiles, but each time the kdebase package is updated the crippled ones reappear. And this breaks things like the \"Home\" icon in Kicker which actually points to the filemanagement Konqueror profile.\n\n2) The power management system, which is a clone of a broken GNOME hack which delegated power management to a tray applet just because the original programmer wanted to use the gconf system for storing the configuration and didn't know how to access that data from a system daemon. \n\nI did my best to get rid of this system, but the \"Suspend\" and \"Hibernate\" buttons are still there in the logout dialog. Of course they crash the system if clicked, even though suspending my machine works using /etc/acpi/sleep.sh.\n\n3) Scribus needs to be started like this; \"LANG=[C/xx_XX] scribus[-ng]\" or it only allows you to use size 0.5 fonts.\n"
    author: "Rick"
  - subject: "native support for klik"
    date: 2006-10-27
    body: "as a happy kubuntu dapper user - dual boot as my work planning software still don't work on Wine- I think if you don't have an internet access; it will be diffucult to upgrade software\nas in thild world countries; net connexion is not granted; imagine that last time tu upgrade amarok ,I go the cybercafe then for home to unstall all those deb for six times it says some libs lack there and there.\n\nI have just a secret dream; just grap one file - like firefox2- go home and instal it; it is really sad that windows people can do that since 1982.\n\nregards\nmimoune "
    author: "djouallah mimoune"
  - subject: "Re: native support for klik"
    date: 2006-10-27
    body: "Well me too an really happy Ubuntu/Kubuntu user, but I don't want to use windows again. Because Linux gives you choice - but windows not. You surely using/used hundreds of software with similar functions on win, but none of it gives you sure stabilty, full scalable solutions, fast help. Yes, linux is young, and still evolving. There problems, errors, unusual package usage - but count it how many years after the community could show up an usable system, and yes - mostly without 'klik-klik'. And put next to it the Ms Windows. 1982 till today is a very long time, but it seems nothing is changed really, all of that - there are many unused possibility.\n\nBut the community trying to give their users freedom, and free choice to use such software and with capabilties wich you requiers.\nTherefore is made modules, plugins, a.s.o. The problem is that you are right, but just a little - because all linux distro's mother is the net.\nI've got also an secret dream. Users are mostly without cry, and living with more help to each other to make every work in digital and non digital life happier. Help if you can. I'm sure you could. Even if only 'just' translating man pages, user guide, documentation to people who can make your wishes...."
    author: "ZoltanH"
  - subject: "Re: native support for klik"
    date: 2006-10-28
    body: "\nThanks for that poetic response but IMHO developer don\u0092t care; just tell me is it difficult just to install klik client by default; then make all those package available ( *.cmg) over the internet in way that we can downloading them using any operating system. Then installing them without worrying about lib dependency problems and all those crap; Because if you forget it 97 % of operating system used as client are windows; and all cyber caf\u00e9 here use windows. \n\nI am really sick that this wonderful operating system are harmed by such simple usability issue; people by nature are wick they don\u0092t really care about a software if it is open or close; they simply want a software that works \n\nCheers\nmimoune\n"
    author: "djouallah momoune"
  - subject: "Re: Debian have a better package manager than Kubu"
    date: 2006-10-28
    body: "Yes. I agree with you frind. I have a nice 30KB/s conection at home, but I had this problem with Kubuntu also. The package manager have a weak dependencies structure. I give up the Kubuntu due this problem.\n\nI was using Kurumin, a brazilian tropical Debian mod. I never had problems with this distro. It is possible to do thousands installs and updates without no problems.\n\nNow I am playing with Mandriva 2007.0 - a dual linux boot. Mandriva is great, wonderfull. But you have a limited package supply, so you need to use de contrib and the plf sources. Plf is really great, but the contrib sometimes have dependencies problems.\n\nI think Debian have the best package manager."
    author: "Brazilian Guy"
  - subject: "Re: Debian have a better package manager than Kubu"
    date: 2006-10-29
    body: "Euh debian and kubuntu have the same package manager..."
    author: "terracotta"
  - subject: "Adept problems after upgrade!"
    date: 2006-10-27
    body: "I upgraded to Edgy from Dapper following these instructions:\n\nUsers of Kubuntu 6.06 LTS can upgrade to 6.10 over the internet by following these instructions:\n\n    * NOTE: This procedure upgrades your system over the Internet, which requires a large download of several hundred megabytes.\n    * In Konqueror go to /etc/apt, right click on sources.list and choose Actions -> Edit as Root\n    * Change all instances of dapper to edgy\n    * Launch a console with KMenu -> System -> Konsole\n    * In the console run: sudo apt-get update\n    * In the console run: sudo apt-get dist-upgrade and follow the prompts to upgrade\n    * In the console run: sudo apt-get install kubuntu-desktop python-qt3 python-kde3 ubuntu-minimal and follow the prompts to install\n    * Reboot your computer\n\nBut after the reboot I have 34 Packages pending for installation (mostly python stuff) but it won't do anything if I try to upgrade either in konsole (apt-get) or via adept... does anybody else have this problem?"
    author: "David"
  - subject: "Re: Adept problems after upgrade!"
    date: 2006-10-28
    body: "If you read the apt-get message closely (if i.e you try to apt-get install a single one of them) it mentions why it cannot install those packages: You have installed packages which prevent installation. This is because python packages have changed names. Before they were called python-2.4-blabla or sth. like that. Now they are only called python-blabla. So you have to apt-get remove all those packages one by one which is a bit tedious unfortunately. Afterwards an apt-get upgrade will install all the remaining python packages properly. Hope this helps ;-)\n"
    author: "Michael"
  - subject: "Re: Adept problems after upgrade!"
    date: 2006-10-30
    body: "Thanks for the answer Michael, I can see that there is some python packages that have the status \"Upgradeable\" but I don't see python-2.x packages that would maybe  break things... I tried to remove python packages but that caused apt to remove almost the whole desktop (all kde packages) and that wasn't really what I wanted :)\n\nIn Konsole I don't get any error message, it just says 34 packages upgradeable and then nothing happens :(\nIs there something else I can do?\n\nThanks in advance,\nDavid "
    author: "David"
  - subject: "Re: Adept problems after upgrade!"
    date: 2006-10-30
    body: "You can try the following: \n\nIn a konsole: \napt-get dist-upgrade \n\nThis will list the packages that are held back. \nHit Ctrl-C to break out of the command if it stopped at a prompt instead of returning to the command line.\n\nThen do \napt-get install list_of_package_names_from_above\n\nIt will tell you what packages it wants to add / remove / upgrade. \nIf it looks ok to you let apt-get do its work by answering \"Y\". \n\n"
    author: "cm"
  - subject: "bug ridden release"
    date: 2006-10-28
    body: "I love Kubuntu, and have been using it since Breezy Badger, but I have to say that Edgy is the buggiest release so far.  The problems I've notice so far...\n\n_ system settings is missing configuration options.\n_ i tried to access a file in /var with kate, but once I reached the root dir, the only directories I could see was /home and /media.\n_ tty1 .. tty6 are corrupted and unusable.\n_ X crashes occasionally while surfing the web, darn i810 drivers\n_ trying to turn on drop shadows via system settings locked up the kde startup sequence.  it was a bit of a chore trying to turn them off without being able to get into kde again.\n_ fonts look awful\n\n... and I've only been using Edgy for one day!\n\nNow this being said, I have confidence that Kubuntu can turn it around.  Also the roughness of this release wasn't a complete surprise, after all this was supposed to be a bleeding edge release."
    author: "asdf"
  - subject: "Re: bug ridden release"
    date: 2006-10-28
    body: "\"i tried to access a file in /var with kate, but once I reached the root dir, the only directories I could see was /home and /media.\"\n\nIf you enabled show hidden files, you'll see all the files and folders."
    author: "weintor"
  - subject: "Re: bug ridden release"
    date: 2006-10-30
    body: "Yes, but then you will see all the files that were really intended to be hidden as well. There is no way to enable the previous behaviour where hidden files are hidden but the root of the file tree is unpruned."
    author: "Rick"
  - subject: "Re: bug ridden release"
    date: 2006-10-28
    body: "System settings is nog missing configuration options, there's just a 'General' and an 'advanced' tab (ok I don't like it this way, but the same options are still there).\n\nthe /home and /media thing is not a bug, it's on purpose, you can still access the directory you like like by entering it in the file dialog."
    author: "terracotta"
  - subject: "Re: bug ridden release"
    date: 2006-10-31
    body: "> the /home and /media thing is not a bug, it's on purpose, you can still\n> access the directory you like like by entering it in the file dialog.\nWell, if this is the case then they've crippled a well working system beyond a level not even the Evil from Redmond would expect of it's users!\n\nWe users are not infants! I don't want to be incapacitaded by my system - especially not when I'm on a FREE desktop system!\n\nSo if it is true that the directory tree is \"pruned\" down to look like last year's Christmas tree:\n\nWhat about directories shared between users? How could I access them? \nHow would I be able to edit sytem files such as in /etc (OK by entering it into the url line - but this woulds a) drive me nuts b) rendering the GUI completely obsolete for that purpose as I'd be light years faster opening a terminal and edit on vi\n\nConrad"
    author: "beccon"
  - subject: "Re: bug ridden release"
    date: 2006-10-31
    body: "\"So if it is true that the directory tree is \"pruned\" down to look like last year's Christmas tree\"\n\nI just wanted to see that simile again and admire it :-)"
    author: "Boudewijn Rempt"
  - subject: "positivity"
    date: 2006-10-28
    body: "(i must admit this post if driven a bit by zacks last blog post)\n\nplease people,\n\nmost of the previous posts are about annoyances, etc. kubuntu is driven by a team of motivated volunteers: you probably did not pay anything for this marvellous product!\n\ninstead of paying for your software the free-software movement basically ask you to do either of the following 3 things:\n 1. use it, be happy and show gratitude\n 2. help to make it better\n 3. don't use it\n\nso, if you don't like it, and your not happy, there is only 2 options left: (1) help or (2) don't use it. (note: these options do not include: \"complain\")\n\ni feel that people are showing even more negativity here than on forums about paid software. why?\n\nthe free-software movement is so open and friendly, you can easily join the forums, mailinglists, wiki's, irc-channels and starthelping out in one way or another... THIS IS HOW ALL THIS FREESOFTWARE GOT TO YOU IN THE FIRST PLACE! PEOPLE HELPED OUT, FIXED, EXTENDED, AND TOOK RESPONSIBILITY.\n\nso if you're just ventilating your compaints, you're WRONG: you can help constructively though.\n\ncies breijs.\n\n\np.s.: these 3 'options' i talked about, i made them up myself."
    author: "cies breijs"
  - subject: "Re: positivity"
    date: 2006-10-29
    body: "Sorry I dont really agree. It's a bit naive IMHO to expect that OSS is somehow exempt from criticism. I use this forum (as many others do) to take a somehow global, overall standpoint on a new release. I do file bugs, but it's still sometimes necessary to view a product as a whole. You just cannot expect this to completely stop. The OS movement wants all people to use their software not only developers. As we have so many computer users in the worlds it's just unrealistic that everyone will file bugs. You would literally be swamped in bug reports.\n\nI'm using Open Source software for many years now. This is of course for a reason. But sometimes a release is just really disappointing and I will continue to speak out in the future if I think so. Kubuntu Breezy was a very, very impressive release but Kubuntu Edgy is one of those very disappointing releases (just like Firefox2). All the new features (Suspend mode, Upstart etc) simply dont work. In KDE there are no 3D effects which made the release a bit more worthwhile in Gnome. And at the end of the day it has caused more trouble than good for me. YMMV though.\n"
    author: "Michael"
  - subject: "Re: MORE POSITIVITY"
    date: 2006-10-30
    body: "so we don't agree... no problem!\n\nyou talk about disappointing releases, i understand your point. but... in my eyes it's very hard to be disappointing when it is free: just don't use it!\nstay with Breezy, firefox-1.5, and go GNOME if you want to.\n\nin my eyes you have no right to complain, you do have the right to help out though!\n\nKDE4 needs people testing the eye-candy that i will sport, see kwin_composite in kde4. firefox had many RC shipped, you seem capable of testing them, and reporting bugs: did you?\n\nAgain: _I_think_ you can't, and shouldn't, complain over freesoftware. But it is just my opinion.\n\nOfcourse we should see freesoftware as a product; see what is misses, where it is bad... But we look at it in order to fix it: not to comaplain."
    author: "cies breijs"
  - subject: "Re: MORE POSITIVITY"
    date: 2006-10-30
    body: "Complaining is a way to help out. It helps people who would be severely hit by the bugs and misfeatures of a certain product steer clear of that. If you don't like what you hear about the Kubuntu release in this thread, you might want to check what people have to say about the Fedora one reported in the following Dot article, and possibly save yourself a lot of grief. For you then, the people complaining added lots of value to the open source experience, by saving you hours and hours of time wasted installing and fighting with Kubuntu."
    author: "Rick"
  - subject: "What sucks sucks"
    date: 2006-11-05
    body: "Come on guys, something is going in the IMHO wrong direction and I cannot talk about it just because it is done by volunteers? Nonsense!\nAfter Novell's sell out to MS I'm more than open to install a new distro, but nothing crippled please, if I wanted that I would use Gnome.\n\nSo exactly as Rick says, \"the people complaining added lots of value to the open source experience, by saving you hours and hours of time wasted installing and fighting with Kubuntu\" - I'm very grateful for all the opinions I hear here and will look now for opinions about Fedora and LinSpire and Mepis etc. to see if I get there an easy = 'non-crippled' KDE distro which does _not_ get in my way by hiding things from me, in the assumption that I'm too stupid to use them.\n\nSo sorry all you nice and friendly volunteers, what you wanna do seems not to be what I need to get my work done in time, but do not despair, there might be others who like dumbed down computers. "
    author: "Matt"
  - subject: "Re: MORE POSITIVITY"
    date: 2006-12-23
    body: "In my marketing class I've been taught that customers who complain are the ones who value your service. Those who don't value your service don't complain; they just move on to other providers.\nI don't mean that complaints are not annoying, cause they sure are. It's just that they ARE part of the business, and they CAN help, in their anoying, noisy way.\nHey I don't know anything about programming, just as I don't know the first thing about making a pizza, I'm not even sure what I like about it, but I'll sure know it when I don't like it.\n"
    author: "bitoy"
  - subject: "Great Release"
    date: 2006-10-28
    body: "I've been running Edgy for about 10 days.  This has been the most\ndelightful upgrade in many years.  Not since Xfree86 itself was\nadded 12 years ago has an upgrade been so fun.\n\nAll the CPU performance stuff is finally in and working, and the\noverall performance is dramatically better.  On my T60p Thinkpad, \nOpen Office, for example, is finally fast enough to use, no longer\nscrambling key-stroke order, and fast enough to use presentation\nanimations.\n\nGnome based apps, like Eclipse 3.3, finally get printing support,\nvery long overdue.\n\nLots of bugs, but they said this would be edgy...  I've found\nmyself w/ 2 sources.list under /etc/apt and symlink to the which \never repository set makes sense.  Edgy repository mostly, but I \nhave had to point back at the dapper repositories in order \nto get working versions of some apps, like squid, which are \notherwise dead-on-arrival.\n\nMark Shuttleworth and his team deserve a congradulations, they're\ndoing great, and often thankless, work.\n\nPhil\n\n\n\n\n \n\n"
    author: "Phil"
  - subject: "kde4 development platform"
    date: 2006-10-29
    body: "that would be nice, but how on earth do i install it? All that happens when I attempt to\n  *aptitude install kde4base-dev*\nis that it complains about unsolvable dependencides and gives in: http://paste.debian.net/15759"
    author: "Anders"
  - subject: "Re: kde4 development platform"
    date: 2006-10-29
    body: "And here is a recipe, though not a very satisfactory one:\n\nremove kubuntu-desktop\nremove speedcrunch\ninstall kde4base-devel\n\nThis works, but it does leave the system in kind of a messy state (from apt-get output):\n\nThe following packages were automatically installed and are no longer required:\n  xserver-xorg libstdc++5 linux-headers-generic wlassistant ttf-gentium python-libxml2 brltty libqt4-qt3support\n  hwdb-client-kde libqt4-core kde-icons-mono ttf-lao kbstate python-qt4 hwdb-client-common libqt4-gui amarok-xine\n  libbrlapi1 openoffice.org amarok python-dbus libglade2-0 libtunepimp3 libqt4-sql fping landscape-client\n  xcursor-themes language-selector-qt xorg libhsqldb-java linux-headers-2.6.17-10 libservlet2.3-java\n  openoffice.org-base python-elementtree libvisual-0.4-0 kde-guidance-powermanager gcc-3.3-base kmousetool\n  libnjb5 kmag bluez-pin linux-headers-2.6.17-10-generic\nUse 'apt-get autoremove' to remove them.\n\nIt also remove python-qt4 -- not a big loss for me since I do not use python (except when coded by others). But hey, I *did get the kde4 packages installed, so I should now be able to jump onto the platform... :)"
    author: "Anders"
---
<a href="http://kubuntu.org/announcements/6.10-release.php">Kubuntu 6.10</a>, codenamed Edgy Eft, is hot on the download mirrors, this release is based on the brand new <a href="http://www.kde.org/announcements/announce-3.5.5.php">KDE 3.5.5</a>. For all your photo needs the award winning <a href="http://www.digikam.org">digiKam</a> is now installed by default and renowned artwork by the beloved Oxygen artist Ken Wimer shines all over. On top of this Kubuntu makes the perfect platform for KDE 4 development and porting KDE 3 applications with all the KDE 4 development libraries available along with Qt 4.2.


<!--break-->
