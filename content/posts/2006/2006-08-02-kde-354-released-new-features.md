---
title: "KDE 3.5.4 Released With New Features"
date:    2006-08-02
authors:
  - "sk\u00fcgler"
slug:    kde-354-released-new-features
comments:
  - subject: "Help spread the news"
    date: 2006-08-02
    body: "I've also submitted the news to digg.com:\nhttp://digg.com/linux_unix/KDE_3_5_4_is_released\nDigg it and help spread the word about KDE."
    author: "Jure Repinc"
  - subject: "Re: Help spread the news"
    date: 2006-08-02
    body: "And right now it's the story at the top of the page in the technology section.\n\nThanks for the digg!\n"
    author: "Wade Olson"
  - subject: "Awww yeah."
    date: 2006-08-02
    body: "I dugg it~"
    author: "Praxxus"
  - subject: "Re: Help spread the news"
    date: 2006-08-05
    body: "Digg in!!!!!"
    author: "gnulinuxman"
  - subject: "Thank you"
    date: 2006-08-02
    body: "As always, big thanks to all involved in making the best desktop that exists today possible.\n\nGreat work guys!"
    author: "JB"
  - subject: "I really hope KDE4 will rock"
    date: 2006-08-02
    body: "I really hope KDE4 will be an outstanding GDE, because IMHO, gnome is becoming more mature, and as the main distro (RED HAT, NOVELL, UBUNTU)has standarized in a way or another on gnome, \n\nso really hope plasma will the next revolution in desktop computing otherwise, I fear kde will be dommed\n\n\nps; thanks for kde3.5.4"
    author: "morphado"
  - subject: "Re: I really hope KDE4 will rock"
    date: 2006-08-02
    body: "I'm not worried about KDE being doomed at all. Anyone I've ever known that uses GNOME on the actual desktop (ie, not just using it to run Red Hat Network Configuration Program or somesuch) ends up running KDE programs on top of it. And moreover, while commercial distributions do tend to favor GNOME, usage trends and users suggest that KDE is actually everyone's favorite free software desktop. My experience would tend to agree :)"
    author: "Chase Venters"
  - subject: "Re: I really hope KDE4 will rock"
    date: 2006-08-04
    body: "I agree that Gnome has improved a lot. So much that I switch between Kubuntu and Ubuntu throughout the week (when I was a die-hard KDE user before).\n\nThe fact is, Gnome has been devoting much more time toward usability & HIG development than KDE. As a whole, when I start ubuntu, I am amazed with the way things seem to fit together. When I run Kubuntu, I'm amazed with detail and feature-set given to each technology underlying the desktop and every app that runs on it.\n\nKDE is a better desktop than Gnome technically and functionally, but Gnome has surpassed KDE in overall usability by having a decent HIG that developers and distro's use and focusing on simplicity in application layout and functionality. \n\nquick disclaimer: I think gnome went a little to far with simplicity with many of there applications. It has made a large part of there desktop unusable for anyone other than the [idea of the] \"average user\" that they design for. Cheerios not Fruityloops.. a paraphrase of something a metacity developer once said\n\nI also realise that if I install kde & gnome in Gentoo that a some of that \"amazement of the way things seem to fit together\" in gnome is gone because it of the large amount of work ubuntu put into unifying the look & feel.  \n\nIf we had a solid & usable HIG that developers and distro's followed for KDE 3 things would be MUCH better, and moving away from relying on 3rd party icons/widget styles (Crystal/Plastik/Lipstik) will also help unify the look-n-feel. Crystal icons look very nice, but are also occasionally confusing (a 13 year old girl recently asked me what the glass of water in the bottom right was... the trash). Plasik/Lipstik is very nice compared to Keramik **pukes**, but it still has it's fair share of annoyances and ugliness.\n\nI am amazed with the way KDE has progressed and grown. I am stunned with the ideas and plans for KDE4 (Plasma, Solid, Oxygen, Phonon, and I want to give a HUGE thank you to everyone who is contributing to the development of the absolute best desktop environment to date. There is a place in my heart for all of you.  I'd also like to thank Celeste Paul and anyone else working along side/with her for making sure that future KDE versions (4+) are focused on usability, HIG, and all those other 3 letter acronym's that stand for a better desktop experience for all of us end users!\n\nThere is a bright and optimistic future for KDE, the open source desktop, free software, open and innovative techology, and everything else KDE stands for/contributes to!\n\nVladislav Blanton"
    author: "Vlad Blanton"
  - subject: "Re: I really hope KDE4 will rock"
    date: 2006-08-02
    body: "Well I'm not quite as pessimistic yet ;)\nLet's talk about that when KDE loses its majority in the \"favorite DE\" votings.\n\nBesides, what's \"an outstanding GDE\"? GNOME Desktop Environment, or what? :D\n(I don't think KDE will become that very soon...)"
    author: "Jakob Petsovits"
  - subject: "Re: I really hope KDE4 will rock"
    date: 2006-08-02
    body: "Yes, Gnome is maturing fast. Now they even have a menu editor and a notepad than can open remote files :-)\n\nSeriously, Gnome has a long way to catch up with KDE. Hopefully someday those that are now supporting Gnome blindly will one day wake up. If not, as KDE has the trust of users it's their loss."
    author: "Derek R."
  - subject: "Re: I really hope KDE4 will rock"
    date: 2006-08-02
    body: "try to catch up with windows first!\ngnomies are our friends !"
    author: "chris"
  - subject: "Re: I really hope KDE4 will rock"
    date: 2006-08-02
    body: "Strictly as a desktop, KDE surpassed Windows a long time ago. Granted there are things that are still better on Windows, but the majority of them are not on the desktop level, where they have been stuck for many years.\n\nAs for Gnome, they might be friends but they are still compiting with KDE."
    author: "Derek R."
  - subject: "Re: I really hope KDE4 will rock"
    date: 2006-08-02
    body: "\"because IMHO, gnome is becoming more mature\"\n\nGnome has been becoming more mature for years, and years, and years......\n\n\"and as the main distro (RED HAT, NOVELL, UBUNTU)has standarized in a way or another on gnome\"\n\nI don't see it making a blind bit of difference to be honest. That may frustrate some people, but there it is."
    author: "Segedunum"
  - subject: "Re: I really hope KDE4 will rock"
    date: 2006-08-04
    body: ">\"and as the main distro (RED HAT, NOVELL, UBUNTU)has standarized in a way or\n> another on gnome\"\n>I don't see it making a blind bit of difference to be honest. That may\n>frustrate some people, but there it is.\n\nRight said. Pushing standards \"top down\" never worked. If it was this way, we would all silently agree on Microsoft Windows being \"the standard\". We'd all have to shut up, step back ten years in time and trying to get used to all the shortcomings of the Windows OS again, which most readers here already can't even remember any more.\n\nTo be honest, even the decision to cripple Gnome in its functionality has been a top-down decision. And it was a decision to win the hearts of the distributors decision makers, not the hearts of their users.\n\nBtw.: I have seen lots of KDE installations in small to medium sized businesses and only one Gnome desktop and this was at SUN Microsystems...\n"
    author: "Thomas"
  - subject: "Re: I really hope KDE4 will rock"
    date: 2007-10-15
    body: "\"and as the main distro (RED HAT, NOVELL, UBUNTU)has standarized in a way or another on gnome\"\n\nThat may be so but for the majority of smart users I also don't think it makes a difference. For those people who still have the unfortunateness of windows in their home what do you do the very second you buy it? Do you enjoy the way that microsoft presets it for you? NO ofcourse not, everyone personalises their OS upon buying. If people want KDE they will simply get it. (and I hope they do :P)"
    author: "witchdoctor"
  - subject: "focus on usability"
    date: 2006-08-02
    body: "Now KDE is coming this far I really hope that usability becomes nr. 1 on the priority list.\n\nWe need more applications with an interface like Amarok.\n\nFor example, why can't I configure a GoogleTalk account in Kopete? As a software guy I know that GoogleTalk uses the Jabber protocol, but my wife for sure doesn't know....\n\nThese kind of 'too-technical-for-the-average-user' stuff must be resolved if KDE wants to compete with OS X for example.\n\nMy 2 cents..."
    author: "Harry"
  - subject: "Re: focus on usability"
    date: 2006-08-02
    body: "Good luck trying to connect to ICQ or MSN on OS X :)\nDo they have GoogleTalk at least?"
    author: "Coolo"
  - subject: "Re: focus on usability"
    date: 2006-08-03
    body: "I haven't tried Adium, but it's said to be great. And building on libgaim, it certainly provides support for those two networks as well.\n\nNot that I wouldn't prefer Kopete nevertheless..."
    author: "Jakob Petsovits"
  - subject: "Re: focus on usability"
    date: 2006-08-03
    body: "Adium looks nice, but is not a default application for MacOS.\n\nSo the situation is not much different: one needs to use an alternative application to connect with google talk.\nIf that is a usability problem, well then all operating systems / grafical environments have a usability problem ;)"
    author: "rinse"
  - subject: "Re: focus on usability"
    date: 2006-08-03
    body: "\"For example, why can't I configure a GoogleTalk account in Kopete? \"\nLet me guess, because it hasn't been implemented yet?\nIts a bit odd to blame the whole desktop for a feature that is absent in 1 application.\n\n[quote]\nAs a software guy I know that GoogleTalk uses the Jabber protocol, but my wife for sure doesn't know....\n[/quote]\nAs a software guy, you could implement google talk in Kopete :)\n\nAnd most people who aren't into software would probably just download the googletalk messenger and use it, or ask someone with more skills if it is possible to use googletalk in kopete..\n"
    author: "rinse"
  - subject: "Re: focus on usability"
    date: 2006-08-03
    body: "I'm using Google Talk with kopete...\n\nYou have to change your connection settings\n\nOverrride default server information:\ntalk.google.com 5223\n\nI think that's about it.\n\n\n\n"
    author: "Jon Orn Arnarson"
  - subject: "Re: focus on usability"
    date: 2006-08-03
    body: "Google Talk works just fine on Kopete, I use it every day with a number of people.  User images, authorization, blocking... it all works perfectly.  You can find the instructions on how to configure it on the Google Talk site.\n\nYou might try reading their site before complaining."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: focus on usability"
    date: 2006-08-03
    body: "Incidentally, that's like complaining \"KMail doesn't work with my ISP\".  You have to enter the mail server, your login and password.  Same goes with Outlook or any other mail program.  You have to get the mail server from your ISP's web site.\n\nFor Google Talk, get the chat server info from Google Talk's web site.\n\nHonestly, that's a pretty lame \"too-technical-for-the-average-user\" example, considering that every mail program needs a server, login and password entered into it as well.  As does any client for Google Talk.\n\nBy the way... Kopete supported Google Talk long before Google Talk was released."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: focus on usability"
    date: 2006-08-03
    body: "You're totally missing his point.\n\nWith Google Talk, you install it, it asks you for your username and password, and it works.\n\nWith Kopete, you install it, then you have to set up an account, then you have to know that you should pick Jabber, then you have to give it server addresses and port numbers, then you have to give it your username and password, and only then does it work.\n\nIt's way more complicated than it should be.  Users don't like dealing with server addresses and port numbers.  And no, it's not like email, because most users get their email set up automatically by their ISPs installation CD, so it's just as easy as putting in your username and password.\n\nYou're completely out of touch with what's complicated for normal users.  Don't assume fiddling around with settings is acceptable.\n\nAnd try not to be so hypocritical.  He *obviously* knows that Google Talk uses Jabber, he said so in his comment.  Telling him to \"read their site before complaining\" only makes it obvious you didn't even read his comment all the way through."
    author: "Jim"
  - subject: "Re: focus on usability"
    date: 2006-08-03
    body: ":: He *obviously* knows that Google Talk uses Jabber,\n\n    And that's why I said it's no more difficult than setting up an email application.  It would be wonderful if Kopete came with a one form Google Talk signup/setup page.  But his question was \"why can't I configure a GoogleTalk account in Kopete\".  Google Talk *themselves* answer his question in their section on setting up other chat programs.\n\n:: You're completely out of touch with what's complicated for normal users. \n\n    No I am not.  It should be improved and be more like the AIM setup -- one page, a couple questions, a link to signup.  But Kopete's support for Google Talk was implemented *before* Google Talk came out, so how can they have a more user friendly interface for something that didn't exist then?  If the next big release of Kopete doesn't have easier support, then I'll agree with you.\n\n    Heck, I agree with him about his wife.  But if he knows what Jabber is, he can set it up.  With the next release his wife may be able to.  Maybe... I know \"normal\" users very well.  I know people who, after two years of trying, haven't figured out how to set up their iPod on their Powerbook.  And that's with several calls to Apple.\n\n    Also, you may overestimate me.  I like KDE because it's easy and I don't want to deal with any of that \"type here and there and run this script and type build make\" crap.  I hate dealing with computers and setting up stuff more than you might understand.  But I don't fault software for supporting something awkwardly until the authors have had a chance to make it more easy to use.  KDE has a good track record of ever-easier configuration.\n\n    My biggest beef is that he's complaining about using X with Y from company Z when company Z's website has simple instructions on how to use X with Y.  Bad example.  Try complaining about the hellish print system or the headache of having to enter a password into some cryptic window to change your clock time."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: focus on usability"
    date: 2006-08-04
    body: "What Kopete needs is an interface that lets the user connect to Google Talk, just like with jabber, msn, icq, etc. etc.\n\nThe fact that kopete does not provide such an interface is not an useability problem, it's just that it has not been implemented yet. \n\nIf Google Talk uses another propietary protocol that is not available in Kopete, you would not complain about usability, but about a missing feature.\n\nBut since Google uses the Jabber Protocol, wich makes it possible to use it in kopete with a workaround, you start complaining about the usability of KDE in general and kopete in particular?\n\nAgain, it is not a usability flaw, it's a feature that has not been implemented.\n\n\n\n"
    author: "AC"
  - subject: "Re: focus on usability"
    date: 2006-08-05
    body: "As mentioned above, Google Talk is a standard Jabber account. It isn't proprietary. I can talk to other Jabber users using it."
    author: "gnulinuxman"
  - subject: "Re: focus on usability"
    date: 2006-08-06
    body: "The protocol is standard, but it operates on a specific server and a port that is slightly off from the common Jabber port, plus it requires specific security settings.  It would make it much easier for users if there was a single \"Username/Password\" + signup link dialog box for Google Talk."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: focus on usability"
    date: 2006-08-05
    body: "<I>why can't I configure a GoogleTalk account in Kopete?</I>\n\nBecause you didn't read the <A HREF=\"http://wiki.kde.org/tiki-index.php?page=Google%20Talk%20support\">Google Talk Support</A> page at the KDE website.\n\nMaybe the next version of Kopete should have a separate entry for GTalk, even if it uses the standard Jabber/XMPP protocol, but currently it's not hard at all to set it up."
    author: "Quique"
  - subject: "Re: focus on usability"
    date: 2006-08-05
    body: "I still use Gaim because it actually shows the right set of smileys for each protocol. I'll start using Kopete if they can stop using the MSN smiley set for every single protocol!"
    author: "gnulinuxman"
  - subject: "KMail Still Broken?"
    date: 2006-08-02
    body: "Sorry to keep drumming this up, but KMail's been in a sorry state for the whole of the KDE 3.5 series. It's basically unusable as a primary mailer; it crashes too much with IMAP and dIMAP, problems that didn't exist in the previous major version.\n\nIf anybody else here has problems with KMail regularly crashing when doing, well, nothing, on a fairly large IMAP or dIMAP account, please file a bug report. This seems to be one of the biggest recent problems:\n\nhttp://bugs.kde.org/show_bug.cgi?id=126715\n\nPlease, don't make me use Thunderbird!"
    author: "Alistair John Strachan"
  - subject: "Re: KMail Still Broken?"
    date: 2006-08-02
    body: "Thunderbird is one of my (few) not KDE native applications.\n\nMy reason? Poor HTML support in KMail. "
    author: "Harry"
  - subject: "Re: KMail Still Broken?"
    date: 2006-08-02
    body: "I guess you mean \"poor html-composing-support in KMail\"? If so, the more specific you are the better. \n<p>\nHere is a list of all open KMail-composer bugs with \"html\" in the subject.\n<p>\n<a href=\"http://bugs.kde.org/buglist.cgi?short_desc_type=allwordssubstr&short_desc=html&long_desc_type=allwordssubstr&long_desc=&product=kmail&component=composer&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&bugidtype=include&bug_id=&votes=&emailassigned_to1=1&emailtype1=substring&email1=&emailassigned_to2=1&emailreporter2=1&emailcc2=1&emailtype2=substring&email2=&changedin=&chfieldfrom=&chfieldto=Now&chfieldvalue=&order=Reuse+same+sort+as+last+time&cmdtype=doit&newqueryname=\">url too long</a>\n<!-- http://bugs.kde.org/buglist.cgi?short_desc_type=allwordssubstr&short_desc=html&long_desc_type=allwordssubstr&long_desc=&product=kmail&component=composer&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&bugidtype=include&bug_id=&votes=&emailassigned_to1=1&emailtype1=substring&email1=&emailassigned_to2=1&emailreporter2=1&emailcc2=1&emailtype2=substring&email2=&changedin=&chfieldfrom=&chfieldto=Now&chfieldvalue=&order=Reuse+same+sort+as+last+time&cmdtype=doit&newqueryname= -->\n<p>\nPlease add comments to there bugs to help the KMail-devels fixing the bugs and therefore your problems.\n\n"
    author: "Carsten Niehaus"
  - subject: "Re: KMail Still Broken?"
    date: 2006-08-02
    body: "You're right. \n\nBut indeed, that's the major problem. Composing. And specifically not a new message (this works rather well), but replying on a message with a HTML body.\n\nKMail just pastes the html text in the composing window, rather than render the orginal HTML part."
    author: "Harry"
  - subject: "Re: KMail Still Broken?"
    date: 2006-08-02
    body: "Is that somehow related to \n\nhttp://bugs.kde.org/show_bug.cgi?id=65646\n\nor\n\nhttp://bugs.kde.org/show_bug.cgi?id=121849\n\nIf not, please open a new bugreport."
    author: "Carsten Niehaus"
  - subject: "Re: KMail Still Broken?"
    date: 2006-08-03
    body: "for me, Kontact/Kmail wasn't broken the whole KDE 3.5.x releases. i am also using imap folders. just, spam-filtering is strange, but i didn't set it up very well.. maybe i have to use APOP protocol, dunno."
    author: "birthday child :P"
  - subject: "Re: KMail Still Broken?"
    date: 2006-08-03
    body: "You probably don't have enough emails in one directory to provoke it.\n\nI've got directories of 6000+ emails and it doesn't handle these very well."
    author: "Alistair John Strachan"
  - subject: "Re: KMail Still Broken?"
    date: 2006-08-04
    body: "> I've got directories of 6000+ emails and it doesn't handle these very well.\n\nI've got directories of 8500+ emails and I'm not seeing a problem (for the entire 3.5 series). \n\nWhich IMAP server are you connecting to? I'm currently connecting to a courier imap server.\n\nNot trying to say you aren't having problems, but it's certainly not as simple as the number of messages."
    author: "Gralbe"
  - subject: "Re: KMail Still Broken?"
    date: 2006-08-05
    body: "Courier IMAP. I'm certain this has nothing to do with the server, it happened on different IMAP servers too."
    author: "Alistair John Strachan"
  - subject: "Re: KMail Still Broken?"
    date: 2006-08-03
    body: "I haven't had any of your problems with KMail during 3.5, actually. And I only use IMAP: one smaller one and one that now contains 241M of mail. My wife's IMAP account even contains 426M of mail."
    author: "parena"
  - subject: "Re: KMail Still Broken?"
    date: 2006-08-03
    body: "I was worried about IMAP support at first too; it looked VERY flaky, and I was disappointed to see that dIMAP was no better.  BUT, I tried it a bit later, and worked at it a bit, and it's been fine now for a LONG time.  I've been using it as my only mail client (and purely IMAP) for... as long as I can remember basically, using it MANY times a day, with many folders, some of which have thousands of mails, and many big attachments, with only the rarest of hanging troubles.  These are fixed by just running pkill on kmail and kio_imap.  All in all, it's far better than the IMAP support in evolution, thunderbird, etc.  I'd recommend giving it another go.  I'm using courier-imap on the server, if it helps."
    author: "Lee"
  - subject: "Re: KMail Still Broken?"
    date: 2006-08-04
    body: "As am I. Since the crashes occur on two PCs (different architectures, too) it seems unlikely that it's my toolchain. I'm fairly confident this bug exists, you've just been lucky."
    author: "Alistair John Strachan"
  - subject: "Re: KMail Still Broken?"
    date: 2006-08-05
    body: "I've been using KMail for a year and I still like it, even the current version. I haven't had any crashes, and it's super-fast--way faster than Thunderbird (ugh, that one is S-L-O-W)."
    author: "gnulinuxman"
  - subject: "open source - open ..."
    date: 2006-08-03
    body: "thank you so much for this nice birthday present on 02.Aug.2006 :)\nlinux, and KDE is so geil, there are uncountable great applications, hope there will be more people get infected by OpenSource ! after some years of mysterious other operating system series, i just need Linux, forever."
    author: "birthday child :P"
  - subject: "Konqueror Improvements"
    date: 2006-08-03
    body: "I happened to see a link on Digg for testing Javascript speed right as my KDE compile was finishing, so I thought I would compare Konqueror's Javascript between 3.5.3 and 3.5.4. 3.5.4 is almost 30% faster than 3.5.3 and about 45% faster than Firefox at the test. I then decided to check my Gmail, and the pages load noticeably faster.\n\nSo thank you for all the hard work and heres to hoping KDE4 is even better!"
    author: "Eric"
  - subject: "Re: Konqueror Improvements"
    date: 2006-08-03
    body: "Can you please post the link. Thanks."
    author: "Someone"
  - subject: "Re: Konqueror Improvements"
    date: 2006-08-03
    body: "<a href=\"http://digg.com/software/Javascript_Showdown_Speeds_of_Opera_9_IE6_and_Firefox_1_5\">This is the link to the digg story about JS performance.</a>\n"
    author: "dAniel"
  - subject: "Re: Konqueror Improvements"
    date: 2006-08-03
    body: "Thanks for the link. I wouldn't put that much weight into results of this benchmark --- while it's a fun toy, lots of the stuff it's measuring isn't likely to matter much. \n\n"
    author: "SadEagle"
  - subject: "Odd bug"
    date: 2006-08-03
    body: "I am currently using 3.5.4 in Kubuntu. Odd thing happening with Konqueror, the location bar and pulldown menu seem to go missing. I installed 3.5.4, restarted KDE, and no location bar. After a couple of restarts, it came back. Then konqueror crashed, and now its missing again. Is anyone seeing this?"
    author: "Mike Young"
  - subject: "Re: Odd bug"
    date: 2006-08-03
    body: "http://kubuntu.org/announcements/kde-354.php says that the Kubuntu packages are broken and shouldn't be used."
    author: "Anonymous"
  - subject: "KDE 3.5.X"
    date: 2006-08-03
    body: "Congratulations for KDE 3.5.4. \n\nI'm using KDE 3.5.3 on Tomahawk Desktop and did not try the latest version of KDE yet. I have following queries regarding that:\n\n1. You get MP3 and Ogg settings under Control Center->Sound & Multimedia->Audio CDs. Where the settings for FLAC? If this is not available for KDE users who intend to convert their CDs to FLAC, is there any plan to include into KDE 3.5.X or KDE 4.X?\n\n2. Can the KDE convert 24bit 96kHz CDs into FLAC at the same rate? I have converted one such CD (www.orangemusicnet.com, Montana Skies) into FLAC, but it turn out to be 16bit 44.1kHz! I worry may be the CD is not recorded into 24bit 96kHz in first place even though its printed on the CD cover. Can others convert 24bit 96kHz CDs at the correct rate? How about converting even higher, such as 24bit 170kHz CDs (Reference Recordings, etc)? \n\n3. Currently the Tomahawk Desktop is based on X.org 6.8.2. If I upgrade the Xorg server to 7.1 and the KDE to 3.5.4, can I get the latest desktop features sports in SUSE Linux Enterprise Desktop 10? Eg. Desktop previews, desktops in a cube, etc. Please see http://www.novell.com/video/bs_06/monday_press_demo.ogg if not sure what I mean.\n\n4. Is there a Screen capture program for KDE which can capture live desktop sessions at high definition resolution, 1280x800, 1280x720 or DVD quality?\n\nTarsier\n"
    author: "Tarsier"
  - subject: "Re: KDE 3.5.X"
    date: 2006-08-03
    body: "1.\nWhat would be the point? FLAC is lossless, the only thing you can adjust is how long it spends on compression, and we have defaulted it to the most sane value.\n\n2.\nI think Audio CD expects real CDs and therefore 16bit 44.1kHz\n\n \n"
    author: "Carewolf"
  - subject: "Re: KDE 3.5.X"
    date: 2006-08-03
    body: "audiocd copying is missing in linux.\nhttp://exactaudiocopy.de/eac3.html\nhttp://exactaudiocopy.de/"
    author: "Nick"
  - subject: "Re: KDE 3.5.X"
    date: 2006-08-03
    body: "Umm, what?\n\nExactly what do you think tools like cdda2wav and cdparanoia do?"
    author: "Justin"
  - subject: "Re: KDE 3.5.X"
    date: 2006-08-04
    body: "Or even better: kaudiocreator, or just use Konqueror to copy the audio cd :)\nNick probably never used linux or kde before :o)"
    author: "AC"
  - subject: "Re: KDE 3.5.X"
    date: 2006-08-05
    body: "Yeah, and of course we can use K3B to make copies of audio CDs. :)"
    author: "gnulinuxman"
  - subject: "Re: KDE 3.5.X"
    date: 2006-08-04
    body: "Thanks for replies. But I'm still not convinced whether KDE/Konqueror could be used to convert 24bit 96kHz CDs (or even higher 24bit 170kHz) into FLAC. Does this means nobody in the KDE community attempt to convert such CDs into FLAC !? :)\n\nI read it somewhere if you create a CUE sheet when you convert CDs into FLAC, you can recreate the CD from FLAC with similar gaps between tracks. But when I create I don't get any such CUE files. That's why I mentioned why don't we have FLAC settings section like for MP3. In fact, if it is possible to select maximum compression when converting into FLAC, we may be able to save couple of gigabytes. At least we can try them and see. Please see this url: http://mac.softpedia.com/progScreenshots/MacFLAC-Screenshot-3989.html"
    author: "Tarsier"
  - subject: "Re: KDE 3.5.X"
    date: 2006-08-06
    body: "24bit 96kHz CD?\n\nCD is a standard with mandatory 44.1kHz and 16bit. What you are refering to sounds more like an AudioDVD or SuperCD, which is unfortunately not supported yet."
    author: "Carewolf"
  - subject: "maintenance release? sureee"
    date: 2006-08-03
    body: "funny how they call this a 'Maintenance Release' and still include so many new features. I thought the whole idea of a Maintenance Release was bug and security fixes."
    author: "Steve Warsnap"
  - subject: "Re: maintenance release? sureee"
    date: 2006-08-03
    body: "It fixes well over 100 bugs (the changelog lists about 160 bugfixes). The new features are \n\na) well tested in 3.5.x\nb) very small\nc) in trunk (the is what will become KDE4)\nd) reviewed in public before being added ('backported') to KDE 3.5.4\n\nThese four conditions make these features safe."
    author: "Carsten Niehaus"
  - subject: "Re: maintenance release? sureee"
    date: 2006-08-03
    body: "Well you are trolling. Its a maintenance release, or can you tell me a list of new super shiny features?\n\nOk now to something more interesting than stupid troll-posts.\n\nI am using the smaller WMs, still fluxbox is my favourite.\nI need all the speed i can get :-)\n\nThat being said, I prefer KDE over Gnome for 2 reasons:\n\n- KDE is easier to handle than Gnome (that starts with compiling, and\ngoes over to do certain tasks. Gnome seems CONFUSED to me about\nhow it handles things)\n- Dcop and scripting support is much better in KDE than in Gnome.\n\n\nThe 2. reason is my main reason why I use KDE. I love scripting.\nI dont want to miss that feature.\n\n"
    author: "shev"
  - subject: "Re: maintenance release? sureee"
    date: 2006-08-03
    body: "At least he was on topic.  You're not; you're just being a dick."
    author: "Louis"
  - subject: "Re: maintenance release? sureee"
    date: 2006-08-03
    body: "IIRC the rules for feature inclusion were slightly relaxed for this release. Due to the long time before 4.0.0 comes out, some features, which a) seemed very useful to have soon, and b) did not seem too \"dangerous\" to add, were allowed.\n\nSo actually it's not a pure Maintenance Release, but given the fixes-to-features-ratio it's still quite reasonable to use that term."
    author: "tfry"
  - subject: "KDE 4.0 info portal"
    date: 2006-08-04
    body: "Thanks for those contributed to the KDE 3.5.4 and I would like to take this opportunity to make a suggestion to KDE project.\n\nWe are now heading for much anticipated KDE 4.0 and since it has serious architectural changes and incorporating new technologies, why not we have a separate page dedicated for the development of KDE 4.0. So that potential developers, users, artists, other contributers, media, distros, etc. could easily follow the developments of the KDE 4.0.\n\nIt seems currently users, media and others are completely kept in the dark of the developments of KDE 4.0.\n\nSagara Wijetunga\nTomahawk Computers"
    author: "Sagara"
  - subject: "Re: KDE 4.0 info portal"
    date: 2006-08-04
    body: "Well, that's technically not true, it's just not all in one site. Check these out:\n\nhttp://pim.kde.org/akonadi/\nhttp://solid.kde.org/\nhttp://phonon.kde.org/\nhttp://plasma.kde.org/\nhttp://www.oxygen-icons.org/\nhttp://appeal.kde.org/\nhttp://developer.kde.org/development-versions/kde-4.0-features.html (possibly not reliable or up-to-date at this stage)\n"
    author: "Paul Eggleton"
  - subject: "Improvements, Stability, New Features"
    date: 2006-08-04
    body: "I am not having a good day and this is my rant:\n\nMy objective assessment is that things are not improving.\n\nWe have the kicker problems (child panels vs. icons, and vertical external taskbar size) which haven't been fixed.\n\nIt would be nice to be able to have icons on the DeskTop without having them rearranged everytime KDE starts up.\n\nThe DeskTop device icons are seriously broken.  The CD doesn't work the same as the floppies.  My CD ROM drive is only recognized as a CD Writer and there seems to be some confusion between a drive and a disk.  The CD only shows on the DeskTop if it has a disk in it (empty drive would be a good option).  But the floppies are there all the time (they are drives not disks).  I have a 3.5 and a 5.25 floppy.  The Control Center makes a distinction between them although for some reason a 3.5 floppy is just a floppy.  Doesn't work on the DeskTop -- they both have the same icon again (used to work once I put the correct devices in fstab).\n\nThe configuration for the DeskTop devices doesn't work correctly (again).\n\nSuddenly Konqueror quit working on some on-line banking and credit card sites.  I found a non-banking site with the same error -- what is \"Error 500\"?\n\nI think that all of these issues are regressions.  This stuff used to work from last week to a few versions ago.\n\nAnd then tonight, Ark crashed and lost the archive with all my on-line banking passwords.  I have also noticed more random crashes of Konqueror in the last two months.\n\nSo, I see the new KDE release as pretty much broken, and I can't help wondering why developers would release stuff before they got it to work correctly.  Do the developers actually use the current release of KDE?\n\nI am also concerned that KDE now relies on DBus and HAL which are not really ready for general release yet and seem to be poorly documented -- are there up to date HOWTOs on these and UDEV anywhere?  My Zip drive mounts fine (I do wonder why it doesn't have an \"eject\") but I get an error message with the CD or the conventional floppies.  I wonder if I can go back to the old methods that worked perfectly.  It is real cool for the system to detect that I inserted a new CD and ask me what to do with it, but it would be nicer if I could click on the icon and it would mount correctly.\n\nSo, I have to think that I accidentally installed the development release rather than the stable release.  If this was the development release, this would be fine because if this stuff is fixed so that it works correctly it will be really nice.  But, I couldn't recommend the current KDE release to anyone and I am wondering what my options are since I can't use something this broken.\n\nBoudewijn appears to be correct that developers can not properly evaluate their own work.  If this is true, there is no hope that TQM can help solve the problem."
    author: "James Richard Tyrer"
  - subject: "Re: Improvements, Stability, New Features"
    date: 2006-08-04
    body: "Checked some of your problems in suse 10.1 with kde 3.5.4, but they are not there?\n"
    author: "AC"
  - subject: "Re: Improvements, Stability, New Features"
    date: 2006-08-04
    body: "I have no doubt that you are correct in this statement - YMMV after all.\n\nSome of the DBus & HAL issues are probably configuration issues.  But, without good documentation and with no GUI configuration for it in Control Center, if it doesn't work there is little that a user can do:\n\nhttp://home.earthlink.net/~tyrerj/kde/DBus00.png\n\nThis isn't exactly a user friendly error message is it?  \n\nXML is making it harder to fix such issues by editing configuration files by hand.  As we go to more and more complicated underlying technologies we must have GUI configuration available for them.\n\nOther problems only occur with specific configurations.\n\nhttp://bugs.kde.org/show_bug.cgi?id=128552\n\nThis is still reported with SuSE 10.0.  It can be fixed in you build from source by using an older version of one file.\n\nAnd the kicker child panel vs. icon issue only occurs with child panels -- especially if you have autohide child panels.  I think that it is only the autohide child panels that cause the DeskTop icons to get rearranged.\n\nDistros may have fixed some of these issues, but should we rely on distros to fix our bugs?"
    author: "James Richard Tyrer"
  - subject: "Re: Improvements, Stability, New Features"
    date: 2006-08-05
    body: "I'm using KDE 3.5.4 on Kubuntu, and even with 3.5.2-3.5.4, I haven't had any of the problems you're describing."
    author: "gnulinuxman"
  - subject: "Re: Improvements, Stability, New Features"
    date: 2006-08-08
    body: "Do you have a 5.25 inch floppy drive?\n\nDo you do on-line banking with the Bank of America?\n\nDo you have a Chase VISA card?\n\nDo you have a CD-ROM drive (i.e. READ only)?\n\nBug 128552 was fixed after the 3.5.4 release.  Have you tried a _vertical_ External TaskBar with \"length set to \"3%\" and: \"Expand as required to fit contents\" checked?\n\nDo you have autohide Child Panels that cover DeskTop icon when they unhide?\n\n"
    author: "James Richard Tyrer"
  - subject: "Re: Improvements, Stability, New Features"
    date: 2006-08-14
    body: "I had no big issues with kde 3.5.4, but my online banking doesn't work anymore, it work great with firefox and used to work with konqueror 3.5.3\n\nI can't connect to any pages of https://directnet.dexia.be/\n\nThe Bank told me they haven't changed anything to their site,...\n\nIs someone able to connect with konqueror 3.5.4\nMaybe a SSL Problem?\n\nAny thought?"
    author: "2cents"
  - subject: "Kubuntu has a working version now."
    date: 2006-08-06
    body: "You can nab it via adding \"deb http://kubuntu.org/packages/kde-latest dapper main\" to your /etc/apt/sources.list file and running \"sudo apt-get update && sudo apt-get dist-upgrade\".  They fixed the remaining issues they had, so now you can actually upgrade and not get screwed with a bad install."
    author: "JVz"
---
The KDE Project today announced the immediate availability of <a href="http://www.kde.org/announcements/announce-3.5.4.php">KDE 3.5.4</a>, a maintenance release for the latest generation of the most advanced and powerful free desktop for GNU/Linux and other UNIXes.  Even while KDE 4 is being prepared, improvements to KDE 3.5 have been made and this release makes them available.  The new features were subject to rigorous quality testing so that KDE 3.5.4 is as stable as the maintenance releases that precede it.







<!--break-->
<p><a href="http://www.kde.org/announcements/changelogs/changelog3_5_3to3_5_4.php">Significant enhancements</a> include improved support for removable devices (users can now mount all devices supported by <a href="http://www.freedesktop.org/wiki/Software/hal">FreeDesktop's HAL</a> and control how it will be done). Multiple holidays can now start on the same date in <a href="http://korganizer.kde.org">KOrganizer</a>. Lots of fixes have been applied to <a href="http://www.konqueror.org">Konqueror</a>'s HTML engine, <a href="http://www.khtml.info">KHTML</a>. The dialog for sending client-side SSL certificates is now more usable, the StartCom SSL certificate was added and <a href="http://knetworkconf.sourceforge.net">KNetworkConf</a> now supports Fedora Core 5 and handles WEP keys better.</p>

<p><a href="http://www.kde.org/info/3.5.4.php#binary">Packages</a> are available for ArkLinux, Fedora, Kubuntu, Pardus Linux, SuSE Linux, Slamd64, amongst others.  You can also <a href="http://www.kde.org/info/3.5.4.php">download the source</a> or have it built for you with <a href="http://developer.kde.org/build/konstruct/">Konstruct</a>.</p>











