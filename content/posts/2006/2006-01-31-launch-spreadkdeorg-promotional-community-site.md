---
title: "Launch of SpreadKDE.org Promotional Community Site"
date:    2006-01-31
authors:
  - "sk\u00fcgler"
slug:    launch-spreadkdeorg-promotional-community-site
comments:
  - subject: "Site's not working"
    date: 2006-01-31
    body: "This site is not displayed correctly in konqueror. The input form for username and password is too long. \n\nIt's working in Firefox.\n"
    author: "hiasll"
  - subject: "Re: Site's not working"
    date: 2006-01-31
    body: "it's working here with Konqueror KDE3.5"
    author: "Patcito"
  - subject: "Re: Site's not working"
    date: 2006-01-31
    body: "If your fonts are too big, that issue might appear. I can reproduce it if I set my font-size to \"Giant, are you blind?!?\".\n\nWe'll have to have a look at it though. Thanks for pointing it out."
    author: "sebas"
  - subject: "Re: Site's not working"
    date: 2006-01-31
    body: "Ok, I pressed \"Shrink Font\" and now it's displayed correctly. But I don't understand it. When I start a session with a new user it doesn't work neither. \n\nBtw: Debian Sid/KDE 3.5.1\n"
    author: "hiasll"
  - subject: "Re: Site's not working"
    date: 2006-01-31
    body: "> If your fonts are too big, that issue might appear. I can reproduce it if I set my font-size to \"Giant, are you blind?!?\".\n\nI am short-sighted and have to say that it's a pita that the combination of browser/web developers is often unable to assure that the content looks fine with the font size the user chooses. I use Firefox with a 20 pt. and 16 pt. minimum font size and don't consider it very large. And yes - the edit fields exceed the size of the left pane."
    author: "Carlo"
  - subject: "Re: Site's not working"
    date: 2006-01-31
    body: "There are two good things to do with your sidebar:\n* relate the sidebar width to the font, eg width: 12em; in the stylesheet\n* use a CSS rule to set the width of the input fields (to 100%). You might need !important to override the rules of the drupal stylesheet."
    author: "Anders"
  - subject: "Re: Site's not working"
    date: 2006-01-31
    body: "I fixed all these issues in the php code for kde.org, but since more and more subsites are moving to different website code, they all appear again now. Oof curse this is fixable, but it requires some attention towards these problems.\n\nOlaf\n"
    author: "Olaf Jan Schmidt"
  - subject: "err"
    date: 2006-01-31
    body: "why konqi's teeth are red?!"
    author: "ac"
  - subject: "Re: err"
    date: 2006-01-31
    body: "It's blood from feasting on a small gnome :-)"
    author: "ac"
  - subject: "Re: err"
    date: 2006-01-31
    body: "good one ;)"
    author: "ac"
  - subject: "Re: err"
    date: 2006-02-01
    body: "I think that's his forked tongue sticking out...?"
    author: "AC"
  - subject: "Re: err"
    date: 2006-02-09
    body: "Why is Konqi sticking his tongue out at me?  Was it something I said?"
    author: "Carl"
  - subject: "Even the story \"dept\" is about this"
    date: 2006-02-12
    body: "Methinks the graphic should definitely be \"fixed\". Image is important in a popularity campaign."
    author: "Hisham"
  - subject: "it's needed all right.."
    date: 2006-01-31
    body: "Even Google is going the way of Gnome:\nhttp://www.theregister.co.uk/2006/01/31/google_goes_desktop_linux/\n"
    author: "Anonymous Coward"
  - subject: "Let's be serious..."
    date: 2006-01-31
    body: "\"Goo-buntu\"? These are just rumours...\n\nHowever, I'm really NOT impressed by the lack of creativity: \"SpreadKDE\" sounds like a direct clone of SpreadFirefox. If we can't get anything original even from the name, good luck with getting decent marketing."
    author: "Giacomo"
  - subject: "Re: Let's be serious..."
    date: 2006-01-31
    body: "What would be the value in picking an original name, rather than one associated with an existing successful grassroots promotion campaign? The tools SpreadKDE offers will be very different to SpreadFirefox, but to me it makes perfect sense to adapt the name. Many people will recognise it, they'll know what SpreadKDE is all about.\n\nOriginality for its own sake isn't a path KDE should ever take."
    author: "Tom Chance"
  - subject: "Re: Let's be serious..."
    date: 2006-02-01
    body: ".. and this is why Tom is so damn valuable to have around: he Get's It =)"
    author: "Aaron J. Seigo"
  - subject: "This needs a link on the front page of KDE.org"
    date: 2006-01-31
    body: "This site, and project really, needs a link on the main www.kde.org site to draw visitors attention.  Personally, I think the Solid project, solid.kde.org, and Phonon both need links off the main page as well.  \n\nI am really excited to see a project like this get underway and hope that even casual KDE supporters will try to contribute.  "
    author: "Jon Lee"
  - subject: "Re: This needs a link on the front page of KDE.org"
    date: 2006-02-01
    body: "plans are being drawn for the publicizing of solid, phonon, etc... they will likely get a website facelift first and then be trotted out one by one as they mature and kde4 draws closer."
    author: "Aaron J. Seigo"
  - subject: "design needs work"
    date: 2006-02-01
    body: "SpreadKDE.org looks like just another KDE corporate website.  Boring same old same old KDE design you see everywhere.\n\nWhy not make it attractive and distinctive like SpreadFirefox.org?  You have to make people excited about KDE, not put them to sleep."
    author: "ac"
  - subject: "Re: design needs work"
    date: 2006-02-01
    body: "We've *love* for someone to come along and make a really attractive design for us! If you're interested, there are a few requirements:\n\n- It obviously has to be standards compliant (XHTML, CSS)\n- It needs to have a top-menu for main items, and a flexible sidebar (i.e. isn't designed with a specific set of boxes in mind)\n- It needs to comply with the KDE CIG (http://wiki.kde.org/tiki-index.php?page=KDE%20CIG) and integrate graphical elements from the Oxygen artwork (http://www.oxygen-icons.org/)\n- It needs to be accessible and usable\n\nSo if anyone wants to take a crack just send the designs to kde-promo@kde.org"
    author: "Tom Chance"
  - subject: "merchandise?"
    date: 2006-02-02
    body: "what about selling merchandise, like t-shirts and cups @ spreadKDE(or giving away for free for developers ;) )?\nyou could make another contest at kde-look to get some ideas... one could offer really different types and styles of t-shirts to attract different kinds of people...\nyou'd have a lot of addvertising for free :)\n\nalso the different kde-projects could make their own (e.g. freebsd-kde or kde-women ...)\n\n"
    author: "hannes hauswedell"
---
The KDE marketing group is pleased to announce the release of
<a href="http://www.spreadkde.org">SpreadKDE.org</a>, the new home for KDE's promotional activities.
Such a hub for marketing activities has been sorely lacking in KDE
until now, and we consider this site a to be a key milestone in
establishing a solid foundation in growing KDE's promotional activity.

<!--break-->
<p><img src="http://static.kdenews.org/danimo/konqi-wants-you-300.png" width="300" height="350" align="right" />
In the past, the KDE community has had no centralized location for
holding promotional materials or listing related tasks.  However, with
the release of <a href="http://www.spreadkde.org">SpreadKDE.org</a>, we hope to meet and exceed those
basic needs by:
<ul>
<li>Providing a singular portal for KDE marketing materials</li>
<li>Fostering a community for those interested in promoting KDE</li>
<li>Aggregating marketing tasks and connecting those that need help with
those who can help</li>
<li>Storing related materials and documentation</li>
<li>Concentrating and steering future KDE marketing goals</li>
<li>Assisting and linking regional KDE groups, from new to established</li>
<li>Coordinating KDE-related events</li>
</ul></li>
</p>

<p>Although quite basic in its initial form, we hope that we can quickly
build out the site through participation.  Remember that this site
belongs to everyone, so please take ownership and:
<ul>
<li><a href="http://www.spreadkde.org">Visit the site, familiarize yourself with SpreadKDE.org, and create an account</a></li>
<li><a href="mailto:kde-promo@kde.org">Provide feedback on the style and content</a></li>
<li><a href="http://www.spreadkde.org/tasks">Review current tasks and consider where you can help</a></li>
<li><a href="http://www.spreadkde.org/handbook">Think about what content should be held on our site</a></li>
<li><a href="https://mail.kde.org/mailman/listinfo/kde-promo">Discuss how we can improve our new home</a></li>
</ul>
</p>
<p>So whether you have technical skills, a marketing background, or are
just interested in lending a hand and learning more, we invite you to
join KDE's reinvigorated promotional community.</p>

<p>A special thanks to those who were instrumental in building the SpreadKDE.org site (Tom Chance, Martijn Klingens, and Sebastian K&uuml;gler) and to those at the <a href="http://www.OSUOSL.org">OSU Open Source Lab</a> for graciously hosting our new home.</p>
