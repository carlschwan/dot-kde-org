---
title: "KDE 3: All About the Apps (Part 4)"
date:    2006-07-30
authors:
  - "cniehaus"
slug:    kde-3-all-about-apps-part-4
comments:
  - subject: "Lmms"
    date: 2006-07-30
    body: "Rosegarden is nice but Lmms is a really powerfull tool. However, still Band in Box shows how software looks like which really serves the interests of the amateurs."
    author: "funbar"
  - subject: "Not everything is good as promised on the website"
    date: 2006-07-30
    body: "SuperKaramba: \"super\"slow and not \"smooth\" (no double buffer) = useless for me\nKaffeine: DVB support is a joke, buggy and slow. They take 100% CPU after 30-up minutes watching DVB-TV\nKDE: is there a way to show bigger image/pdf/video thumbnails on the desktop like in a Konqueror window?\n\nAll in all, KDE 3.5.x is great and I really hope that Qt4/KDE4 feels a lot better with double buffer everywhere.\n"
    author: "anonymous"
  - subject: "Re: Not everything is good as promised on the website"
    date: 2006-07-31
    body: "> KDE: is there a way to show bigger image/pdf/video thumbnails on the desktop like in a Konqueror window?\n\nA \"BoostSize=true\" setting for the desktop!???"
    author: "anonymous"
  - subject: "Re: Not everything is good as promised on the website"
    date: 2006-07-31
    body: "> Kaffeine: DVB support is a joke, ...\n\naddon to Kaffeine: on watching DVB-TV, the sound jumps if my CPU calcs anything...\nVDR can do this A LOT better with sound, NOTHING jumps...\n"
    author: "anonymous"
  - subject: "Trying BasKet?"
    date: 2006-07-31
    body: "If you're trying out BasKet, you really should go with the 0.6.x betas.  They are very usable and much much better than the 0.5 series.  It's now titled BasKet Note Pads for some reason.\n\nExcellent software."
    author: "Evan \"JabberWokky\" E."
  - subject: "Try these in Konqueror"
    date: 2006-07-31
    body: "fish://username@serverhostname\n\nrsync://serverhostname/\n\nwebdav://dav.web.server\n\naudiocd:/\n\n...yes, there are many more. Try matching this with a \"mere\" web browser. (-:"
    author: "Leon Brooks"
  - subject: "Re: Try these in Konqueror"
    date: 2006-08-01
    body: "Don't for get sftp://Serverhostname and smb://serverhostname."
    author: "Robert"
  - subject: "Re: Try these in Konqueror"
    date: 2006-08-02
    body: "svn://anonsvn.kde.org/home/kde/trunk/KDE/ :D"
    author: "superstoned"
  - subject: "Rosegarden / Jack"
    date: 2006-07-31
    body: "I'd love to try Rosegarden but setting up Jack is a pain. I've had minimal success - the one time I managed to get it working the sound quality was awful."
    author: "Mike"
  - subject: "Re: Rosegarden / Jack"
    date: 2006-07-31
    body: "As of about kernel-2.6.12 jack should be pretty easy to setup.  Certainly with most recent distros it just works out of the box.  With My delta 1010lt jack just run perfectly.  I for one would recommend it especially with the likes of rosegarden (although you dont need jack for this to work) ardour and jamin etc, linux audio is pretty damn good.\n\nDave"
    author: "Dave M"
  - subject: "Blogging"
    date: 2006-07-31
    body: "Basket remind me of blogging. Are there offline blogging tools planned for kontakt?"
    author: "hill"
  - subject: "kimdaba"
    date: 2006-07-31
    body: "KimDaBa is a great name! KPhotoAlbum is... ugh."
    author: "kim"
  - subject: "KimDaBa v. Digikam?"
    date: 2006-08-01
    body: "So how to KimDaBa and Digikam compare?  I'm quite happy with the latter; it supports multi-level tagging, is moderately fast, uses a simple SQL structure (so I can write external programs), and talks to my camera. Oh, and it supports other metadata formats (like XMP).\n\nI don't see what features KimDaBa (or whatever) has that aren't already in Digikam.  I'm probably missing an obvious document somewhere; anyone know where?"
    author: "anonymous"
  - subject: "Re: KimDaBa v. Digikam?"
    date: 2006-08-01
    body: "Both are great."
    author: "jmfayard"
  - subject: "Re: KimDaBa v. Digikam?"
    date: 2007-04-21
    body: "New KPhotoalbum 3.0 turns out to be unusuable, at least on my machine,\n(which shouldbe powerfull enough though: 2GB RAM, AMD 64, 500 GB disk space under Gentoo).\nMore precisely, it gets stuck when trying to read in new images, i.e. the process takes 4 days for 3800 pics, and the overall speed of reactions to user queries is anyway below an acceptable level. (Also there is still no mysql support.)\n\nWhat a pitty. Started as a very promising project and seems to be in the state\nof a useless toy now. A staement which meanwhile applies unfortunately to 50% of all KDE tools. \n\n---Niko"
    author: "niko"
---
This is part four of the <a href="http://dot.kde.org/1144660487/">the</a> <a href="http://dot.kde.org/1145962774/">successful</a> <a href="http://dot.kde.org/1146830533/">series</a> <i>All About the Apps</i>, reminding us that while KDE 4 development may be fun, to watch to find great apps working today KDE 3 beats them all.  This time we report on the Linux equivalent of Cubase - Rosengarden, the great Basket, KPhotoAlbum and the next version of KDevelop.









<!--break-->
<div style="border: thin solid grey; float: right; margin: 1ex; padding: 1ex">
<a href="http://basket.kde.org/development.php"><img src="http://static.kdenews.org/jr/basket.png" width="250" height="221" /></a>
</div>

<h2>Basket</h2>

<p>You know Klipper, the advanced KDE-clipboard? You have a large notes.txt-file
on your home-directory? Your monitor is full of yellow notes? If only one
anwser was <i>yes</i> you should have a look at <a href="http://basket.kde.org/">Basket</a>!</p>

<p>The author is currently working on the next version, 0.6, which makes it
easier to handle <a href="http://basket.kde.org/news.php">a lot more data</a>
and has a range of usability improvements while
<a href="http://basket.kde.org/development.php">looking</a> much better.</p>

<h2>KPhotoAlbum</h2>

<p><a href="http://kphotoalbum.org/">KPhotoAlbum</a> is the new name of the
famous KimDaBa. It is a very powerful photomanager with a very strong
tagging-system. In early May 2006, version 2.2 was released featuring many
long awaited features. Currently, there is a
<a href="http://developer.kde.org/summerofcode/soc2006.html">Summer of Code</a> project for KPhotoAlbum which will switch its backend from XML to a much more performant SQL table. Learn more about this great app by watching
<a href="http://kphotoalbum.org/videos/">Flash videos</a> of it.</p>

<div style="border: thin solid grey; float: left; margin: 1ex; padding: 1ex">
<img src="http://static.kdenews.org/jr/rosegarden-wee.png" width="250" height="194" />
</div>

<h2>Rosegarden</h2>

<p>If you're into music composition, <a href="http://www.rosegardenmusic.com">Rosegarden</a>
may be of interest. It's a MIDI sequencer and notation editor, which also has
basic audio capabilities (recording and playback). Connecting with
<a href="http://jackit.sourceforge.net/">Jack</a>, <a href="http://www.ladspa.org/">LADSPA</a> and
<a href="http://dssi.sf.net/">DSSI</a> plugins, featuring a powerful notation editor,
Rosegarden should fit nicely into your home studio. If classical notation
doesn't fit your needs it also offers a "piano roll" editor along with a
percussion editor. The next version should feature lots of improvement in
notation handling, along with other modes like guitar tablature for instance.</p>

<h2>KDevelop</h2>

<p>After more than seven months of hot development the KDevelop-developers are
going into the beta-stage for 3.4. On the 6th of July they released 3.4beta1
including an incredible list of new
<a href="http://www.kdevelop.org/index.html?filename=3.4/features.html">features</a>!
The list is by far too long to post here, it ranges from user interface improvements to <i>a lot</i> of C++ improvements, a better Ruby-parser and Qt 4 support.</p>

<h2>The Future</h2>

<p>With all those exiting applications you should not forget about KDE itself, KDE 3.5.4 will be released very soon! Stay tuned to The Dot.</p>













