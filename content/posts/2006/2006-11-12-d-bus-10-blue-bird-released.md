---
title: "D-Bus 1.0 \"Blue Bird\" Released"
date:    2006-11-12
authors:
  - "dmolkentin"
slug:    d-bus-10-blue-bird-released
comments:
  - subject: "D-Bus"
    date: 2006-11-12
    body: "D-Bus simply rocks. The Qt bindings are really much easier and cleaner than the old KDCOP interface. \n\nI still remember the time when I was working on Solid and compiling Qt bindings directly from D-Bus CVS was a pain ;)"
    author: "Micha\u00ebl Larouche"
  - subject: "Qt3 bindings"
    date: 2006-11-12
    body: "They are still at some 0.6x version right?"
    author: "cartman"
  - subject: "Re: Qt3 bindings"
    date: 2006-11-13
    body: "Kevin (not K\u00e9vin) can probably answer better, but I don't think they've seen much work in recent times. They should still be working for the stuff you may need to do with KDE3 code."
    author: "Thiago Macieira"
  - subject: "Re: Qt3 bindings"
    date: 2006-11-13
    body: "I haven't had time to keep them up to date with D-Bus changes. Since the 1.0 release keeps the API stable, I am planning to get the bindings backport compatible again.\n\nHowever, I haven't yet decided what to do about the issue of more complex datatypes, since the Qt4 bindings code can't be easily backported as its QVariant is way more powerful than Qt3's\n\nMy personal goal is to be able to use the Qt3 bindings for implementing any D-Bus interface we might come up with as part of the Portland DAPI efforts.\n\nAny help welcome :)"
    author: "Kevin Krammer"
  - subject: "Re: Qt3 bindings"
    date: 2006-11-14
    body: "Anyway to get dbus 0.9x working with qt3 instead of having to have a seperate dbus-qt3-0.62 package? Some patch perhaps?"
    author: "ht"
  - subject: "Re: Qt3 bindings"
    date: 2006-11-15
    body: "My system's D-Bus version is 0.94 and the my bindings in SVN seem to work (i.e. compile and the simple example-client works)\n\nI'll have to check more thoroughly later today"
    author: "Kevin Krammer"
  - subject: "Re: Qt3 bindings"
    date: 2006-11-16
    body: "Well, mine don't. That is: stuff compiles, but things like notifications of plugging in a USB stick don't. :-("
    author: "Andr\u00e9"
  - subject: "Re: Qt3 bindings"
    date: 2006-11-16
    body: "Could you send me the code you are using?\n\nI am not sure I did any testing with D-Bus signals yet, but I think they should work.\nMaybe I tested with the OwnerChanged signal, but it has been to long ago to be sure :(\n"
    author: "Kevin Krammer"
  - subject: "Re: Qt3 bindings"
    date: 2006-12-03
    body: "Have you tried the QT3-bindings from here: http://ranger.befunk.com/fink/\n\nI had the same issues with the \"official\" QT3-bindings. Using the above sources with DBus 1.0/HAL 0.5.7 and KDE 3.5.5 seem to work fine here...\n\nDV\n"
    author: "DarkVision"
---
<a href="http://www.freedesktop.org/wiki/Software/dbus">D-Bus 1.0 ("Blue Bird")</a>, the <a href="http://www.freedesktop.org">Freedesktop.org</a> inter-process messaging system has just been released. A collaborative effort between industry and open source developers, D-Bus was created to allow arbitrary applications to easily communicate with each other and exchange data. An additional system daemon allows for communication with system services. D-Bus is known to work on all Unix platforms and has also been ported to Mac OS X, while a Windows port is in progress. This makes D-Bus the ideal messaging system for KDE 4.




<!--break-->
<p>
This first D-Bus major-release will maintain backwards protocol compatibility throughout its entire lifecycle and, just like KDE, its library will maintain API and ABI compatibility until the next major release. The low level library is toolkit agnostic and only depends on an external XML library but many bindings to higher-level toolkits are available, such as Java, glib and Python.
The Qt bindings are available as part of Qt since version 4.2.
"<i>We're hoping that the Qt bindings to D-Bus will allow application and library developers to easily extend their software into this world of exchange of information</i>", says KDE developer Thiago Macieira, who worked extensively on <a href="http://doc.trolltech.com/4.2/intro-to-dbus.html">QtDBus</a> at Trolltech. "<i>We've designed the API under the familiar Qt style and we've made it similar to what KDE developers already knew in DCOP, so as to make the transition easier</i>".
D-Bus-Maintainer John Palmieri adds: "<i>The high quality of the Qt4 D-Bus bindings shows how flexible D-Bus is in fitting into native environments.  In turn the participation of the Qt4 binding team in the development of D-Bus 1.0 has helped bring this release to a quality that would not have been achieved without their involvement</i>".</p>


<p>While KDE 3 already uses D-Bus to talk to  <a href="http://freedesktop.org/wiki/Software/hal">HAL</a> and <a href="http://avahi.org/">Avahi</a>, in KDE 4, D-Bus has completely replaced DCOP as the IPC System of choice for KDE. Its use is also growing, with Kopete recently announcing the <a href="http://www.tehbisnatch.org/2006/11/11/its-like-telepathy/">initial support for Telepathy/Tapioca</a>, via D-Bus, and the accessibility developers studying using it too.

"<i>I really consider this release as a major milestone for KDE 4, since we're now using it everywhere</i>", says <a href="http://solid.kde.org">Solid</a> developer K&eacute;vin Ottens. "<i>Having strong guarantee about compatibility is really important for an IPC system like this, and that's exactly what D-Bus 1.0 offers us</i>". D-Bus serves as infrastructure for Solid</a> to communicate with system services.</p>
