---
title: "Prize Draw for KPhotoAlbum Translators"
date:    2006-12-10
authors:
  - "jriddell"
slug:    prize-draw-kphotoalbum-translators
comments:
  - subject: "Nice!!"
    date: 2006-12-10
    body: "Nice, now i know what i should do this christmas: finishing the KPhotoAlbum translation :)\n\nWich I would have finished anyway, don't worry ;)\n\nIt's nice to see that you show your appreciation for all the hard work that we translators do with this contest!!\n"
    author: "otherAC"
  - subject: "Re: Nice!!"
    date: 2006-12-10
    body: "Kphotoalbum is an offensive name as it is no language neutral naming scheme. "
    author: "Rene"
  - subject: "Re: Nice!!"
    date: 2006-12-10
    body: "Well, its translatable :o)"
    author: "otherAC"
  - subject: "Re: Nice!!"
    date: 2006-12-11
    body: "What about Kimdaba"
    author: "Rene"
  - subject: "Re: Nice!!"
    date: 2006-12-11
    body: "Its also translatable :o)\nYou can translate every kde-name, simply because the names are made available for translation.\n\nI don't see how kimdaba follows international naming scheme's, the only common factor in every language is that it means nothing (although it could be something offensive in some language, like the .kut extension of a kde application in de past, wich was quite offending to Dutch users. I can't imagine that kphotoalbum would mean something offensive in another language...)\n"
    author: "otherAC"
---
<a href="http://kphotoalbum.org/">KPhotoAlbum</a> has entered string freeze for its new release, and author Jesper Pedersen is offering a <a href="http://lists.kde.org/?l=kde-i18n-doc&amp;m=116574496923363&amp;w=2">prize draw for those who complete the translation</a>.  Individuals and teams with 100% of the strings translated will be entered into the draw for $100 to take place on hogmanay alongside the new release.  The prize money is taken from the donations made to the KPhotoAlbum PayPal account and the aim is to show some appreciation for our hard working but often forgotten translators.

<!--break-->
