---
title: "BasKet Note Pads Usability Survey"
date:    2006-12-26
authors:
  - "fploss"
slug:    basket-note-pads-usability-survey
comments:
  - subject: "Crashes"
    date: 2006-12-26
    body: "I'd really like to use the new BasKet but it crashes like crazy. Crashes on close, crashes on menu and so on. I can hardly use it 10 seconds before it crashes. I tried the Edgy Package and I tried compiling it myself to no avail. Guess I have to wait until things have stabilized somewhat."
    author: "Michael"
  - subject: "Re: Crashes"
    date: 2006-12-26
    body: "perhaps you should check your system - its perfectly stable. :)\nIt 100 percent a problem with your system - check compiler/libs !\n"
    author: "wingsofdeath"
  - subject: "Re: Crashes"
    date: 2006-12-26
    body: "Not sure, somethings wrong with the tooltips. As soon as you move your mouse over them, basket exits or crashes. This are some the crashes, but the bts are not useful. I tried to report it to bugs.kde.org but it seems to be down, will try again later. Sys is suse 10.2\n\nUsing host libthread_db library \"/lib/libthread_db.so.1\".\n[Thread debugging using libthread_db enabled]\n[New Thread -1233877296 (LWP 8102)]\n[KCrash handler]\n#9  0x00050004 in ?? ()\n#10 0xb700881c in qScrollEffect () from /usr/lib/qt3/lib/libqt-mt.so.3\n#11 0xb6fee25a in QTipManager::showTip () from /usr/lib/qt3/lib/libqt-mt.so.3\n#12 0xb6fee63a in QTipManager::add () from /usr/lib/qt3/lib/libqt-mt.so.3\n#13 0xb6fee7f2 in QTipManager::add () from /usr/lib/qt3/lib/libqt-mt.so.3\n#14 0xb6fee8cf in QToolTip::tip () from /usr/lib/qt3/lib/libqt-mt.so.3\n#15 0xb7dfd973 in Basket::maybeTip (this=0x8321828, pos=@0x80d2c54)\n    at basket.cpp:2843\n#16 0xb6fed990 in QTipManager::showTip () from /usr/lib/qt3/lib/libqt-mt.so.3\n#17 0xb6fee2ea in QTipManager::qt_invoke () from /usr/lib/qt3/lib/libqt-mt.so.3\n#18 0xb6eb53cd in QObject::activate_signal ()\n   from /usr/lib/qt3/lib/libqt-mt.so.3\n#19 0xb6eb600d in QObject::activate_signal ()\n   from /usr/lib/qt3/lib/libqt-mt.so.3\n#20 0xb71f12b9 in QTimer::timeout () from /usr/lib/qt3/lib/libqt-mt.so.3\n#21 0xb6ed8bdf in QTimer::event () from /usr/lib/qt3/lib/libqt-mt.so.3\n#22 0xb6e56647 in QApplication::internalNotify ()\n   from /usr/lib/qt3/lib/libqt-mt.so.3\n#23 0xb6e574f9 in QApplication::notify () from /usr/lib/qt3/lib/libqt-mt.so.3\n#24 0xb759f1f2 in KApplication::notify () from /opt/kde3/lib/libkdecore.so.4\n#25 0xb6e4b663 in QEventLoop::activateTimers ()\n   from /usr/lib/qt3/lib/libqt-mt.so.3\n#26 0xb6e05bd0 in QEventLoop::processEvents ()\n   from /usr/lib/qt3/lib/libqt-mt.so.3\n#27 0xb6e6d0e0 in QEventLoop::enterLoop () from /usr/lib/qt3/lib/libqt-mt.so.3\n#28 0xb6e6cf76 in QEventLoop::exec () from /usr/lib/qt3/lib/libqt-mt.so.3\n#29 0xb6e5600f in QApplication::exec () from /usr/lib/qt3/lib/libqt-mt.so.3\n#30 0x0804fc56 in main (argc=134970120, argv=0x21) at main.cpp:106\n\n\n\n\n(gdb) run --nocrashhandler --nofork --debug\nStarting program: /opt/kde3/bin/basket --nocrashhandler --nofork --debug\nFailed to read a valid object file image from memory.\n[Thread debugging using libthread_db enabled]\n[New Thread -1234065712 (LWP 31540)]\nCreated the Undo/Redo actions!\nbasket is loading\nbasket is loading\nbasket: [static void StopWatch::check(uint)] Timer (0): 0.85 s\n\nProgram received signal SIGSEGV, Segmentation fault.\n[Switching to Thread -1234065712 (LWP 31540)]\n0xb6848261 in main_arena () from /lib/libc.so.6\n(gdb) bt\n#0  0xb6848261 in main_arena () from /lib/libc.so.6\n#1  0x08351730 in ?? ()\n#2  0x0000039e in ?? ()\n#3  0x00000079 in ?? ()\n#4  0xb6e2823e in QApplication::desktop () from /usr/lib/qt3/lib/libqt-mt.so.3\n#5  0xb6fc025a in QTipManager::showTip () from /usr/lib/qt3/lib/libqt-mt.so.3\n#6  0xb6fc063a in QTipManager::add () from /usr/lib/qt3/lib/libqt-mt.so.3\n#7  0xb6fc07f2 in QTipManager::add () from /usr/lib/qt3/lib/libqt-mt.so.3\n#8  0xb6fc08cf in QToolTip::tip () from /usr/lib/qt3/lib/libqt-mt.so.3\n#9  0xb7dcd0a5 in Basket::maybeTip (this=0x8343ad0, pos=@0x80d347c) at basket.cpp:2843\n#10 0xb6fbf990 in QTipManager::showTip () from /usr/lib/qt3/lib/libqt-mt.so.3\n#11 0xb6fc02ea in QTipManager::qt_invoke () from /usr/lib/qt3/lib/libqt-mt.so.3\n#12 0xb6e873cd in QObject::activate_signal () from /usr/lib/qt3/lib/libqt-mt.so.3\n#13 0xb6e8800d in QObject::activate_signal () from /usr/lib/qt3/lib/libqt-mt.so.3\n#14 0xb71c32b9 in QTimer::timeout () from /usr/lib/qt3/lib/libqt-mt.so.3\n#15 0xb6eaabdf in QTimer::event () from /usr/lib/qt3/lib/libqt-mt.so.3\n#16 0xb6e28647 in QApplication::internalNotify () from /usr/lib/qt3/lib/libqt-mt.so.3\n#17 0xb6e294f9 in QApplication::notify () from /usr/lib/qt3/lib/libqt-mt.so.3\n#18 0xb75711f2 in KApplication::notify () from /opt/kde3/lib/libkdecore.so.4\n#19 0xb6e1d663 in QEventLoop::activateTimers () from /usr/lib/qt3/lib/libqt-mt.so.3\n#20 0xb6dd7bd0 in QEventLoop::processEvents () from /usr/lib/qt3/lib/libqt-mt.so.3\n#21 0xb6e3f0e0 in QEventLoop::enterLoop () from /usr/lib/qt3/lib/libqt-mt.so.3\n#22 0xb6e3ef76 in QEventLoop::exec () from /usr/lib/qt3/lib/libqt-mt.so.3\n#23 0xb6e2800f in QApplication::exec () from /usr/lib/qt3/lib/libqt-mt.so.3\n#24 0x0804fc56 in main (argc=0, argv=0x101) at main.cpp:106\n(gdb) bt full\n#0  0xb6848261 in main_arena () from /lib/libc.so.6\nNo symbol table info available.\n#1  0x08351730 in ?? ()\nNo symbol table info available.\n#2  0x0000039e in ?? ()\nNo symbol table info available.\n#3  0x00000079 in ?? ()\nNo symbol table info available.\n#4  0xb6e2823e in QApplication::desktop () from /usr/lib/qt3/lib/libqt-mt.so.3\nNo symbol table info available.\n#5  0xb6fc025a in QTipManager::showTip () from /usr/lib/qt3/lib/libqt-mt.so.3\nNo symbol table info available.\n#6  0xb6fc063a in QTipManager::add () from /usr/lib/qt3/lib/libqt-mt.so.3\nNo symbol table info available.\n#7  0xb6fc07f2 in QTipManager::add () from /usr/lib/qt3/lib/libqt-mt.so.3\nNo symbol table info available.\n#8  0xb6fc08cf in QToolTip::tip () from /usr/lib/qt3/lib/libqt-mt.so.3\nNo symbol table info available.\n#9  0xb7dcd0a5 in Basket::maybeTip (this=0x8343ad0, pos=@0x80d347c) at basket.cpp:2843\n        message = {static null = {static null = <same as static member of an already seen type>, d = 0x80570c0, static shared_null = 0x80570c0},\n  d = 0x81b10d0, static shared_null = 0x80570c0}\n        rect = {x1 = 34, y1 = 237, x2 = 335, y2 = 255}\n        contentPos = {xp = 121, yp = 243}\n        note = (Note *) 0x836f818\n#10 0xb6fbf990 in QTipManager::showTip () from /usr/lib/qt3/lib/libqt-mt.so.3\nNo symbol table info available.\n#11 0xb6fc02ea in QTipManager::qt_invoke () from /usr/lib/qt3/lib/libqt-mt.so.3\nNo symbol table info available.\n#12 0xb6e873cd in QObject::activate_signal () from /usr/lib/qt3/lib/libqt-mt.so.3\nNo symbol table info available.\n#13 0xb6e8800d in QObject::activate_signal () from /usr/lib/qt3/lib/libqt-mt.so.3\nNo symbol table info available.\n#14 0xb71c32b9 in QTimer::timeout () from /usr/lib/qt3/lib/libqt-mt.so.3\nNo symbol table info available.\n#15 0xb6eaabdf in QTimer::event () from /usr/lib/qt3/lib/libqt-mt.so.3\nNo symbol table info available.\n#16 0xb6e28647 in QApplication::internalNotify () from /usr/lib/qt3/lib/libqt-mt.so.3\nNo symbol table info available.\n#17 0xb6e294f9 in QApplication::notify () from /usr/lib/qt3/lib/libqt-mt.so.3\nNo symbol table info available.\n#18 0xb75711f2 in KApplication::notify () from /opt/kde3/lib/libkdecore.so.4\nNo symbol table info available.\n#19 0xb6e1d663 in QEventLoop::activateTimers () from /usr/lib/qt3/lib/libqt-mt.so.3\nNo symbol table info available.\n#20 0xb6dd7bd0 in QEventLoop::processEvents () from /usr/lib/qt3/lib/libqt-mt.so.3\nNo symbol table info available.\n#21 0xb6e3f0e0 in QEventLoop::enterLoop () from /usr/lib/qt3/lib/libqt-mt.so.3\nNo symbol table info available.\n#22 0xb6e3ef76 in QEventLoop::exec () from /usr/lib/qt3/lib/libqt-mt.so.3\nNo symbol table info available.\n#23 0xb6e2800f in QApplication::exec () from /usr/lib/qt3/lib/libqt-mt.so.3\nNo symbol table info available.\n#24 0x0804fc56 in main (argc=0, argv=0x101) at main.cpp:106\n        app = {<> = {<No data fields>}, <No data fields>}\n        win = (class MainWindow *) 0x80b8408\n        result = -1232834572\n(gdb)\n\n"
    author: "AJ"
  - subject: "Re: Crashes"
    date: 2006-12-26
    body: "I get the same one. Can someone check is this happens with the 32bit version too? I have no partition left so I cannot try it myself.\nLooking at the backtrace I suppose it is more a qt problem than a basket one."
    author: "Separator"
  - subject: "A Workaround for the Crashes"
    date: 2006-12-27
    body: "I checked the code pointed by the backtrace.\nThis is a Qt bug.\nI've got several reports of this one but it seams unresolvable.\n\nBut, THERE IS A WORKAROUND:\n\nStart BasKet Note Pads, don't move the mouse over any basket and go to the menu \"Settings -> Configure BasKet Note Pads... -> Baskets\" and uncheck \"Show tooltips in baskets\".\n\nThis will prevent the crashy code to be executed."
    author: "S\u00e9bastien Lao\u00fbt"
  - subject: "Re: A Workaround for the Crashes"
    date: 2006-12-28
    body: "If it's a Qt bug, how much would it be to bribe the trolls into squashing it? Or is it already fixed? The workaround does well, but it's kinda silly, since you can't see when a note was added anymore, which is one of the most important aspects of basket."
    author: "AJ"
  - subject: "Re: Crashes"
    date: 2006-12-26
    body: "I downloaded the .deb package (v0.6.0) for Kubuntu Dapper from the Basket site, and it crashes almost instantly  :-("
    author: "Quique"
  - subject: "Re: Crashes"
    date: 2006-12-26
    body: "Please take some time to read your post and dont write such nonsense in the future."
    author: "Michael"
  - subject: "the survey crashes too :-)"
    date: 2006-12-26
    body: "basket works for me (kubuntu 6.10 and the deb package distributed from basket's website), but the survey crashes :-) :\n\nWarning: Division by zero in /srv/www/basket.openusability.org/survey/index.php on line 381\n \n Warning: Cannot modify header information - headers already sent by (output started at /srv/www/basket.openusability.org/survey/index.php:381) in /srv/www/basket.openusability.org/survey/common.php on line 984\n \n Warning: Cannot modify header information - headers already sent by (output started at /srv/www/basket.openusability.org/survey/index.php:381) in /srv/www/basket.openusability.org/survey/common.php on line 985\n \n Warning: Cannot modify header information - headers already sent by (output started at /srv/www/basket.openusability.org/survey/index.php:381) in /srv/www/basket.openusability.org/survey/common.php on line 986\n \n Warning: Cannot modify header information - headers already sent by (output started at /srv/www/basket.openusability.org/survey/index.php:381) in /srv/www/basket.openusability.org/survey/common.php on line 987\n \n Warning: Cannot modify header information - headers already sent by (output started at /srv/www/basket.openusability.org/survey/index.php:381) in /srv/www/basket.openusability.org/survey/common.php on line 988\n \n Warning: Cannot modify header information - headers already sent by (output started at /srv/www/basket.openusability.org/survey/index.php:381) in /srv/www/basket.openusability.org/survey/common.php on line 989\n             \netc...\n"
    author: "Guillaume Laurent"
  - subject: "come on"
    date: 2006-12-26
    body: "i spent about 15 minutes clicking this huge set of responses to get some error at the end? never gonna take a survey again..."
    author: "suseu"
  - subject: "Re: come on"
    date: 2006-12-26
    body: "It seems it is fixed, at least I didn't get any error."
    author: "Cyrille Berger"
  - subject: "Re: come on"
    date: 2006-12-26
    body: "no"
    author: "gerd"
  - subject: "Re: come on"
    date: 2006-12-27
    body: "I'm sorry your answers were lost, this seems to be a bug in the survey app (phpsurveyor). I fixed the problem with a workaround because I couldn't reproduce it. Seems to be dependent on specific constellation of answers.\n\nThis should not happen any more (in your case, the progress bar won't work any more, but this does not influence the result).\n\nI would be happy if you tried it again!\n\nFrank"
    author: "Frank Ploss"
  - subject: "Requirements"
    date: 2006-12-27
    body: "You should add a notice concerning the need for having Javascript enabled before starting the survey. Otherwise you only see the next button and no questions."
    author: "Evil Eddie"
  - subject: "Re: Requirements"
    date: 2006-12-27
    body: "Thanks for the suggestion, I added that note."
    author: "Frank Ploss"
  - subject: "Firefox/Konqueror extension ?"
    date: 2006-12-28
    body: "1) I collect a lot of quotes from webpages. I think copying and pasting things from webpages is a tedious task. A firefox and Konqueror extension would greatly reduce this pain.\n\n2) People would want to access their todo lists, baskets from what ever computer they are on. So I guess implementing some kind of a system which uploads the baskets onto the google notebook for example would be a killer.\n\nI have participated in the survey and has given the above comments.\n\nThanks for such a wonderful app :-).\n\nKamesh."
    author: "Kamesh"
  - subject: "Re: Firefox/Konqueror extension ?"
    date: 2007-01-31
    body: "google notebook integration is a MUST_HAVE option for me. Usually I note something at work/school and then read/rework at home."
    author: "Karolus"
  - subject: "Usability?"
    date: 2006-12-28
    body: "speaking of usability, how about making the \"ok\", \"cancel\" and \"apply\" buttons the same size?"
    author: "Steve Warsnap"
  - subject: "Re: Usability?"
    date: 2006-12-29
    body: "It'd make them more similar, which is bad for recognition. For example people get so used to clicking 'the small button' for 'OK' that they mostly don't read the text or look at the icon. This is a Good Thing."
    author: "logixoul"
  - subject: "Re: Usability?"
    date: 2006-12-29
    body: "... which makes it look like crap for my (very personal) eyes.\n\nAnyway, it's the issue widget style deals with. \"OK\", \"Cancel\" and \"Apply\" have same size in Light v3 I'm using. "
    author: "Hasso Tepper"
  - subject: "Re: Usability?"
    date: 2007-01-02
    body: "> For example people get so used to clicking 'the small button' for 'OK' that they mostly don't read the text or look at the icon.\n> This is a Good Thing.\n\nSo, you find a good thing that the most used button is also the *smaller* one??\nYou follow the 'sadistic user interface' guidelines?\n;-)"
    author: "renoX"
  - subject: "Re: Usability?"
    date: 2007-01-03
    body: "Hmm, good point."
    author: "logixoul"
  - subject: "THE SURVEY IS CLOSED NOW"
    date: 2007-01-02
    body: "The BasKet usability survey was closed on January 2nd.\n\nIn two weeks, we collected 233 responses to the survey! That's quite a lot considering the two weeks only the survey was online, and that not everyone who uses BasKet got to hear about the survey.\n\nA first look at the data reveals that there's quite a variety of users who filled out the questionnaire. This means we collected very valuable data to go further.\n\nThanks to everyone who participated!\n\nLook out for further chances to join the BasKet Usability Project on http://basket.openusability.org !"
    author: "Frank Ploss"
---
Users of <a href="http://basket.kde.org/">BasKet Note Pads</a>, an advanced notepad application for the KDE desktop, are called to participate in a usability survey. The survey is carried out by the recently launched <a href="http://basket.openusability.org/">BasKet Usability Project</a>, a sponsored student project in the "Season of Usability" of <a href="http://openusability.org/">OpenUsability.org</a>.



<!--break-->
<p><strong>About the BasKet Usability Project</strong></p>

<p>The aim of the BasKet Usability Project is to improve the usability of the
BasKet Note Pads tool. Usability, as I (the project maintainer) see it, not only depends on the user interface, but also on the underlying assumptions that are made about the users and their context of use. So, knowledge of users, their contexts and claims are of vital importance for improving usability. </p>

<p>Also, the aim of the project is to also involve the users into the
development of BasKet. Besides the survey, in a few weeks users will be able to be co-authors of "scenarios" and "personas", stories about fictional users and their usage scenarios (both present and future). Look out for <a href="http://basket.openusability.org/how-can-you-help/newsletter">news</a> on this.</p>

<p>To find out about further possibilities to participate, visit the
<a href="http://basket.openusability.org/">project homepage</a>.</p>

<p>The BasKet Usability Project is part of a research project for a diploma thesis at the University of Hamburg, Germany.</p>
<p><strong><a href="http://basket.openusability.org/">Go to the survey</a></strong></p>

