---
title: "First Konqueror Bug Triage Day"
date:    2006-09-03
authors:
  - "logixoul"
slug:    first-konqueror-bug-triage-day
comments:
  - subject: "Offtopic"
    date: 2006-09-03
    body: "Does the logo for \"Opera Widgets\" look familiar to anyone...?\n\nhttp://widgets.opera.com/"
    author: "AC"
  - subject: "Re: Offtopic"
    date: 2006-09-03
    body: "KDE e.V. should sue them, shoudln't they? ;P"
    author: "Patrick Trettenbrein"
  - subject: "Re: Offtopic"
    date: 2006-09-03
    body: "KDE only has trademark on gear with K. \n\nThe use of a gear even if it uses the KDE blue background is not protected."
    author: "Carewolf"
  - subject: "Re: Offtopic"
    date: 2006-09-03
    body: "Also others icons like the \"Highest rate\" and \"Recently updated\" look familiar - even more than the logo."
    author: "Anonymous"
  - subject: "Re: Offtopic"
    date: 2006-09-03
    body: "the bad news is that their logo is nicer than ours."
    author: "AC"
  - subject: "Re: Offtopic"
    date: 2006-09-03
    body: "Microsoft uses gears too. ;-))\n\nhttp://i32.photobucket.com/albums/d45/purpurschwarz/micro1.png"
    author: "cobalt"
  - subject: "Re: Offtopic"
    date: 2006-09-04
    body: "My Amiga 1000:  Gears as the hourglass for the last 21 years."
    author: "Evil"
  - subject: "Re: Offtopic"
    date: 2006-09-03
    body: "That's pretty bad. Some people might actually think that Opera (wich is non-Free, as in \"free speech\") is an KDE-project because of that!\n\nAlso, I noticed this in QNX:\n\nhttp://www.hidebehind.com/DBE5AF48\n\nIANAL, but KDE should seriously consider suing QNX and Opera."
    author: "livingdots"
  - subject: "Re: Offtopic"
    date: 2006-09-03
    body: "no we (i) are friedly people we dont sue anyone !"
    author: "ch"
  - subject: "Re: Offtopic"
    date: 2006-09-03
    body: "Starachowice, the city I grew up, uses gears along with its coat of arms for 40 for more years - http://wallops.info/4images/details.php?image_id=733\n\nGears are just so popular motive :)"
    author: "Jaroslaw Staniek"
  - subject: "Re: Offtopic"
    date: 2006-09-03
    body: "Yes, a lot of people use gears. A white gear on a shiny blue square, though...?"
    author: "AC"
  - subject: "Graph and irc bot"
    date: 2006-09-03
    body: "To give you an idea of the progress we made, a graph of unconfirmed Konqi bug counts over the past four years is available at http://bugs.kde.org/reports.cgi?product=konqueror&output=show_chart&datasets=UNCONFIRMED%3A&banner=1\n\nI would also add that we are working on a program to facilitate bug triage and reduce duplication of effort even more.\n\nPersonally, I would like to see us get the count of unconfirmed Konqi bugs down to less than 900 before New Years Day 2006.  I invite all of you to drop by #kde-bugs on irc.freenode.net and lend a hand.  You don't have to dream in C++ to help!\n\nRegards,\n\nMark"
    author: "Mark Taff"
  - subject: "Re: Graph and irc bot"
    date: 2006-09-03
    body: "I joined in quite late at the bug day, but it was fun and quite productive. You should do this again in a couple weeks!"
    author: "me"
  - subject: "Re: Graph and irc bot"
    date: 2006-09-03
    body: "You don't even have to *know* C++! :-) All you need is a fairly recent KDE installation and a bit of patience. Drop into #kde-bugs and we can help you to get started."
    author: "Philip Rodrigues"
  - subject: "Excellent idea!"
    date: 2006-09-03
    body: "I was getting my kde updated after 2 months leave so I could not participate to this event. I would be pleased to join other such events and I encourage all of you to join as it's a great way to get integrated in KDE, to do efficient work with efficient people and to learn more about KDE.\n"
    author: "annma"
  - subject: "Konqueror as Web Browser"
    date: 2006-09-03
    body: "It's nice to see that Konqueror gets some attention.\nWith regard to the upcoming KDE 4 and the competitive, actively developed khtml backend, perhaps soon in better cooperation with Apple, has someone already plans for a dedicated Konqueror web browsing profile?\nImho this could and should be better separated from the features needed for local file browsing. The interface could be tidied a bit and some features for the web could be added, like reopening closed pages as in Opera (and Firefox 2.0 I think). Nothing is wrong with peeking at the competition :-)\nThe technology is there, now Konqueror just needs to persuade the user visually and by interface design that he's got a new favorite Web Browser."
    author: "Anonymous"
  - subject: "Re: Konqueror as Web Browser"
    date: 2006-09-03
    body: "I think Konqueror should indeed borrow a bit more of the opera look, especially in the case where one tab is a website, another is somewhere on the hard drive, some other tab is an ftp-browser, another one a pdf,jpg...viewer..., if you have tabs above the toolbars as opera does there are less problems with shifting between tabs and jumping and humping toolbars. Just my 0,02. But well I guess I should contribute this feature :s."
    author: "terracotta"
  - subject: "I love KDE but"
    date: 2006-09-08
    body: "... konqueror is driving me insane! Don't get me wrong: KDE is (imho) by far the most complete and usable desktop today with a lot of neat and highly integrated features. But I'm getting sick of constant crashes that I experience since KDE 3.2. I filed a bug-report at bugs.kde.org (50350) and ubuntu bugzilla (since I'm using Kubuntu Dapper, a wonderful distribution). I know, developers are mostly overworked and I'm getting everything for free in sense of \"free beer\" (but I donate money from time to time), but altough I'm thankful some bugs are just frustrating since they never get fixed...\n \nWanna see how to crash konqueror by just hitting enter for some time? Download the movie (mpeg through xvidcap, roughly 5 mb) and see it yourself:\n \nhttp://rapidshare.de/files/32358262/test-0000.mpeg.html"
    author: "kubuntu-user"
  - subject: "Re: I love KDE but"
    date: 2006-09-08
    body: "> Du hast die Datei test-0000.mpeg (4889 KB) angefordert. Diese Datei wurde \n> schon 0 mal runtergeladen.\n> Du hast das Downloadlimit f\u00fcr die kostenlose Nutzung erreicht. Willst du \n> mehr runterladen?\n> Hole dir jetzt deinen Premium-Account! Sofortige Download-Freischaltung! \n> (Oder warte 78 Minuten)\n\nI will wait 78 minutes to see Konqi crashing. Or pay for it.  Yeah, right. \nForget rapidshare!\n\n"
    author: "cm"
  - subject: "Re: I love KDE but"
    date: 2006-09-08
    body: "sorry, did not know where to put it elsewhere. you can try it yourself:\n\nload google, enter something inside the search form, keep hitting enter (=submit) like crazy -> crash. maybe it doesn't crash. don't know why. all my hardware is supported and i can reproduce this crash on a fresh, clean installation."
    author: "kubuntu-user"
  - subject: "Re: I love KDE but"
    date: 2006-09-08
    body: "it saying \"62 seconds\" here, by the way. so if you are still interested in seeing konqi krash... ;)"
    author: "kubuntu-user"
---
Last Wednesday was Konqueror Bug Day. The aim was to either confirm or close as many unconfirmed Konqueror bugs as possible, known as bug triage. About 150 bugs were dealt with. Collaboration happened on IRC (#konq-bugs), and <a href="http://wiki.kde.org/tiki-index.php?page=Konqueror+Bugs+Day">on the wiki page</a>. #kde-bugs on irc.freenode.net is already proving fertile ground for planning similar events in near future.  



<!--break-->
<p>The experience gained resulted in communication with the Bugzilla developers, which will hopefully lead to even more efficient working in future events. The current idea is to do a weekly "Bug Triage Weekend", with a different target application each time. This is expected to result in a collaboration involving even more results and more fun.  We hope to have a target number of bugs to triage ("This time we aim to deal with 200 bugs!").</p>


