---
title: "Third Issue of Amarok Weekly Newsletter Released"
date:    2006-12-08
authors:
  - "shiny"
slug:    third-issue-amarok-weekly-newsletter-released
comments:
  - subject: "slider"
    date: 2006-12-08
    body: "Is it possible to move the slider to the top and get a precise slider? I mean, I often listen to long podcasts, recordings of radio shows. And I konfigured the panel that it stays in the background and pops up when I move the mouse to the lower corner of the screen, so I get a real fullscreen mode. What annoys me is that when I pick the amarok slider the panel pops up. I would like to move it to the top.\n\nThe track time data also belongs to the track information window.\n"
    author: "Gerd"
  - subject: "Re: slider"
    date: 2006-12-08
    body: "I don't think this is suitable place for making feature requests. I would say you have much better chances of getting help by posting request on bugzilla, mailing list, or official forum.\n\nAnyway, you can enable player window, it has slider too, and you can move it anywhere on the screen. After going into small mode, removing window border and making \"always on top\", it becomes portable progress slider ;)"
    author: "Ljubomir"
  - subject: "Amarok development"
    date: 2006-12-09
    body: "I am very surprised about the Amarok fast pace of development in relation to other applications that may be consider more important (notice that this is a happy surprised because I am a happy user :)). Why do you think that is?"
    author: "blacksheep"
  - subject: "Re: Amarok development"
    date: 2006-12-09
    body: "Because of motivated development team, I guess. "
    author: "Ljubomir"
  - subject: "Yeah"
    date: 2006-12-11
    body: "I love amarok - such a nice piece of software. I use it for playing most of my audio files.\nReally nice thing is that it supports audio syncing with iPod (though, this feature is broken in 1.4.4, it works fine with other versions)"
    author: "David"
---
<a href="http://ljubomir.simin.googlepages.com/awnissue3">Third issue</a> of Amarok Weekly News talks about cross-desktop media player cooperation, cool new additions to <a href="http://amarok.kde.org">Amarok</a>, and refreshed artwork. And again, it also includes useful tips. Enjoy!





<!--break-->
