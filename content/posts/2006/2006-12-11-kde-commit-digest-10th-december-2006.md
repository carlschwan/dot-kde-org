---
title: "KDE Commit-Digest for 10th December 2006"
date:    2006-12-11
authors:
  - "dallen"
slug:    kde-commit-digest-10th-december-2006
comments:
  - subject: "ODF Master page?"
    date: 2006-12-11
    body: "What's this OpenDocument master page support thing?"
    author: "John Tapsell"
  - subject: "Re: ODF Master page?"
    date: 2006-12-11
    body: "This is a kind of document from open-office.\nOpen-office has several kinds of documents, like normal text-documents.\nIf there are several text-documents (by example from several writers) to joined together, then you make a master-document which call just the several different sub text-documents."
    author: "ronald"
  - subject: "Re: ODF Master page?"
    date: 2006-12-11
    body: "Sounds like you are talking about a 'booklet' document. Where the different documents are the chapters.\nThis is a different thing. Also, I'm not sure if open office supports that at all."
    author: "Thomas Zander"
  - subject: "Re: ODF Master page?"
    date: 2006-12-11
    body: "ODF has master pages, just like many DTP apps do. A master page has a set of layout properties and things like header/footer information.\nEach real page in the document can say it will follow either the standard or a named masterpage. Making that page inherit the master page properties.\n\nSo, in effect, you can open more ODF documents properly in oKular now."
    author: "Thomas Zander"
  - subject: "ksysguard svg graphs really..."
    date: 2006-12-11
    body: "...look beautiful :)\nhttp://commit-digest.org/issues/2006-12-10/files/sensorload15.png"
    author: "Patcito"
  - subject: "Re: ksysguard svg graphs really..."
    date: 2006-12-11
    body: "The background looks horrid though. Gradient is fine, but the \"streaks\" are completely out of place and are distracting. Another case of overdesign with little thought. If it's another Oxygen team creation, I am not surprised."
    author: "Daniel D."
  - subject: "Re: ksysguard svg graphs really..."
    date: 2006-12-11
    body: "Yeah I know the background doesn't work that well.  That's why I wrote the caption:\n\n\"An experimental SVG background for the graph plotters. One idea is to have the background dark with a watermark changing for each graph. For example, a CPU watermark for the CPU chart. Note the anti-aliased rendering in this view. Artists, send in suggestions!\"\n\nI'm not an artist sorry.  The svg in the screenshot was just stolen from a kde game :-D\n\n"
    author: "John Tapsell"
  - subject: "Re: ksysguard svg graphs really..."
    date: 2006-12-11
    body: "\"One idea is to have the background dark with a watermark changing for each graph.\" Rather cool idea.\n\nWill the SVG is question be scaled DISproportionally, or, there is some way to guarantee some floating elements to be proportionally scaled? \n\nExample: background example is scaled to the edges, no matter what the ratio of w/h is, while a logo of CPU is gloating centered and scaled proportionally.\n\nIf there will be a background / fixed-aspect-centered-overlay support, that would be an awesome thing. Artists will probably line up with offerings. :)"
    author: "Da"
  - subject: "Re: ksysguard svg graphs really..."
    date: 2006-12-11
    body: "I suspect that svg's can support this. I don't know either though"
    author: "John Tapsell"
  - subject: "Re: ksysguard svg graphs really..."
    date: 2006-12-11
    body: "Exactly.  \"Proof of Concept\".  Don't get hung up on the example.\n\nThe possibilities are pretty interesting though.  Companies could put their logos in the background.  You could put different images/identifiers in the background to easily identify different servers if you have multiple instances open.  You could have a faint image of the application that's taking the most CPU/RAM/whatever for \"at a glance.\"  You could have a background that goes from green to red based on usage.\n\nGood job."
    author: "Wade Olson"
  - subject: "Re: ksysguard svg graphs really..."
    date: 2006-12-11
    body: "I asked John to made SVG view with a temp SVG stolen from a game. So, don't blame him, he did a good work. I can now do something better (or you can) without having to do the C++ part.\n\n"
    author: "Johann Ollivier Lapeyre"
  - subject: "Re: ksysguard svg graphs really..."
    date: 2006-12-11
    body: "It seems your gratuitous attack on the Oxygen developers was completely unfounded"
    author: "Vigilant"
  - subject: "Re: ksysguard svg graphs really..."
    date: 2006-12-12
    body: "Jeez dude, didn't you read the \"Artists needed\" phrase?\nGive the guy a break. It's people like you who rip a guy on his first beta screenshot. Jeez."
    author: "Joe"
  - subject: "Re: ksysguard svg graphs really..."
    date: 2006-12-12
    body: ".. seeing as it's not an \"oxygen team creation\" and that it was simply a proof of concept with whatever graphics that were nearby used as examples, i think you owe a couple of people an apology.\n\none of the things about *development* is that things are being *developed*. iow, created. these things take time and they are rarely perfect in the first blush. they step their way towards it.\n\nif you have constructive criticisms then please offer them. but do so constructively and with recognition of where we are.\n\ni think the steps forward with ksysguard are great. they lay the foundation for the artists and the developers to work together to make things truly great for a final release. we're still some months off from that."
    author: "Aaron J. Seigo"
  - subject: "Re: ksysguard svg graphs really..."
    date: 2006-12-11
    body: "I like them, too, but the computer-scientist shows through again: When looking at the CPU usage, I don't care whether the CPU usage is 16.67% or just 16%. Or 17%. Even 20% would be fine.\n\nWhy not just do 0, 50 and 100% at the bottom, middle and top?\n\nAlso, I'd like to know what blue and orange are! User and system?"
    author: "ben"
  - subject: "Re: ksysguard svg graphs really..."
    date: 2006-12-11
    body: "Yeah, I totally agree. 16.67 is hard to read."
    author: "Mark Kretschmann"
  - subject: "Re: ksysguard svg graphs really..."
    date: 2006-12-11
    body: "Agreed. 0, 25%, 50%, 75% and 100% would be a good compromise. The colors need explaining too. I'm not sure I understand what the blue and what the red lines are supposed to represent, and I am a scientist! :-). Same with memory usage. What do the blue, red and yellow lines mean? And if it says at the bottom 839.1 MiB used, why don't I see a line representing that?. I guess the tooltips would say more, but it should be understandable at a glance, too.\n\nThat said, despite being early development it looks miles better than the KDE 3.5 version. Great work!"
    author: "MikeT"
  - subject: "Re: ksysguard svg graphs really..."
    date: 2006-12-11
    body: "Thanks for the suggestions :-)\n\nI tweaked the algorithm for the precision and scaling and I think it's a bit better now:\n\nhttp://img452.imageshack.us/img452/4992/sensorload17rw1.png\n\nI know the physical memory and swap use different units, and I'll look into that.\n\nI also set the cpu percentage in the processes list to 0 decimal places, unless it's <1"
    author: "John Tapsell"
  - subject: "Re: ksysguard svg graphs really..."
    date: 2006-12-11
    body: "MiB is probably the better unit for memory since that is what is used in marketing, and most people have 1 or less GiB of RAM.  Of course, MB and GB are even less confusing for most people, but I realize we should probably try to go with the standard."
    author: "Leo S"
  - subject: "Re: ksysguard svg graphs really..."
    date: 2006-12-12
    body: "I would definitely have a thicker line horizontally to represent \"50%\" of whatever. That's how most users are going to figure...0-50-100..."
    author: "Joe"
  - subject: "Re: ksysguard svg graphs really..."
    date: 2006-12-12
    body: "Yes, now it looks better. But I agree that a 50% line is important. 100/5 gives you 20% increments, so it has to be 100/4= 25% increments as I proposed :-)\nAh! and actually displaying 100% on the top would look much better.\n\nI still don't know what the colours represent, and why there isn't a line representing TOTAL usage. \n\nAnd again something else. The average load also has different units than CPU load, and a different scale. It should be both in % and from 0 to 100%. I hope it helps!\n\nAs I said, your development version makes the KDE 3.5 version look like arcane software!"
    author: "MikeT"
  - subject: "Re: ksysguard svg graphs really..."
    date: 2006-12-12
    body: "Hmm, average load isn't a percentage.  Mine often goes to 3 or 4 or so.\n\nThe number of lines is easily changed.  Only 4 lines doesn't quite look right though. I'll talk it over with the artists - but that's just a default configuration now. The algorithm code should work fine.\n\nAs for the colors..  I'm still thinking about that :/  Perhaps someone can come up with a mockup of how they'd like to see the legend on the graph?I could make it separate perhaps, like the gnome one:\n\nhttp://anjuta.sourceforge.net/naba/gnome-system-monitor.jpg"
    author: "John Tapsell"
  - subject: "Re: ksysguard svg graphs really..."
    date: 2006-12-12
    body: "I have started mocking around with more proposed backgrounds. It there a chance of seeing this new updated sysmon in kde 3.5.x tree? If yes, I would gladly finish and offer you a few backgrounds to choose from."
    author: "Which version"
  - subject: "Re: ksysguard svg graphs really..."
    date: 2006-12-12
    body: "Sorry, but there's no way to do this in kde3.  QT4 is a huge a jump forward in capabilities"
    author: "John Tapsell"
  - subject: "Re: ksysguard svg graphs really..."
    date: 2006-12-13
    body: "Well, then I misunderstood what average load is. How is it calculated?\n\nAbout the number of lines, with my suggestion you actually only have 3 lines (25, 50, 75, as 100 and 0 don't count). If that's too little lines, you can actually display 6 lines, but only label 3 (plus 0 and 100). That way, you can see more accurately where the usage stands, but you don't clutter the axis with too many labels.\n\nA legend would be nice, but the GNOME one looks a bit ugly :-) It is also difficult not to take up to much space. Work for the usability and artists guys!\n"
    author: "MikeT"
  - subject: "Re: ksysguard svg graphs really..."
    date: 2006-12-13
    body: "Please make the number of lines configurable on X+Y axes and\nmaybe make the Y range configurable (autosettable vs manual min+max)\n\nJust an idea.\nBen"
    author: "BenAtPlay"
  - subject: "Re: ksysguard svg graphs really..."
    date: 2006-12-14
    body: "All those things are already there, in kde3 and kde4 :-)\n\nChanging from 4 lines to 3 lines is simply changing the default dsettings"
    author: "John Tapsell"
  - subject: "kdepim 3.5.5+ branch?"
    date: 2006-12-11
    body: "I'm wondering about this 3.5.5+ kdepim branch ... shouldn't these people be working on KDE 4 instead? I get that porting is not as sexy as cramming new features into apps, but isn't this just delaying the inevitable? You gotta port at some point, right, and adding new features makes it harder and harder. That whole enterprise seems ludicrous somehow."
    author: "Peter"
  - subject: "Re: kdepim 3.5.5+ branch?"
    date: 2006-12-11
    body: "Please keep improving KDE 3.x applications:\nKDE 3.x applications nearly run anywhere where kdelibs3 is installed. KDE 3 apps run under KDE 4, too! But KDE 4 apps run nowhere but on KDE 4.\nIn the near future many people won't update their desktop or the whole operation system only to run some KDE4 apps. If an application *needs* kde4 features, then do it wiht kde4 technology but otherwise a KDE3 application just has a much larger potential user (and developer) base.\n\nAre firefox and openoffice successful because they use the latest and greatest GUI toolkit (Not) or because they have many features and good usability?"
    author: "Max"
  - subject: "Re: kdepim 3.5.5+ branch?"
    date: 2006-12-13
    body: "KDE4 applications can run just fine under KDE3.  KDE4 applications /can't/ run against kde3's kdelibs, and KDE3 applications /can't/ run against kde4's kdelibs anymore than GTK applications can run against kdelibs instead of gtk.  There is no technical reason that kde3's libs and kde4's libs can't be installed side by side (like how for a LONG time gtk1.x was installed next to gtk2.x)."
    author: "Corbin"
  - subject: "Re: kdepim 3.5.5+ branch?"
    date: 2006-12-11
    body: "No, they *should* not do anything. That is: they should do whatever pleases them most. I think it is very usefull to keep doing some work on the 3.5 series. This version will be the production version for quite some time to come, I think. KDE 4 is far from finished, and it will take a long time before it is mature enough for large scale adoptation. All that thime, KDE 3 must be able to keep satisfying its users."
    author: "Andr\u00e9"
  - subject: "Re: kdepim 3.5.5+ branch?"
    date: 2006-12-11
    body: "Yep, and all other applications (kphotoalbum, digikam, amarok, kst, khotoalbum, whatever) continue developing their kde 3.x versions, so why should the pim guys be restrained from it?"
    author: "otherAC"
  - subject: "Re: kdepim 3.5.5+ branch?"
    date: 2006-12-11
    body: "> No, they *should* not do anything. That is: they should do whatever pleases them most.\n\n\"Should\" in the sense of \"if the KDE project is supposed to have a long-term future and be in a position to address fundamental problems of its PIM applications\". \n\n\n> KDE 4 is far from finished, and it will take a long time before it is mature enough for large scale adoptation.\n\nYes, especially if the developers are short-sighted enough not to work on it because it's more fun to cram new features into a legacy branch.\n\n\nMy remark is about the big picture of shipping KDE 4.0 in a timely manner. The PIM suite is an integral part of KDE 4.0, and KDE 4.0 cannot ship until it comes together. The reason we want KDE 4.0 to ship is that it's a true next generation of the environment that enables new capabilities in these applications that wouldn't have been possible or viable with KDE 3.x technology.\n\nThus from my point of view, the existence of the 3.5.5+ branch is a sign that the developers working on it have lost sight of the big picture, given up on doing the not-as-fun work of getting the Qt4/KDE4 port up and running, and getting 4.0 out of the door.\n\nDidn't KDE have Technology Working Group that was supposed to guide the 4.0 cycle and make sure this doesn't happen? Keep a tight ship?\n\n\n> Yep, and all other applications (kphotoalbum, digikam, amarok, kst, khotoalbum, whatever) continue developing their kde 3.x versions, so why should the pim guys be restrained from it?\n\nThe applications you cite aren't part of the core platform of KDE and not vital to shipping 4.0. The PIM suite also provides a number of platform services (contact database, etc.) that are requirements for other applications to finish their KDE 4.0 ports. The importance of 4.0 PIM coming together is high."
    author: "Peter"
  - subject: "Re: kdepim 3.5.5+ branch?"
    date: 2006-12-11
    body: "But you are suggesting to stop all development in kdepim for more then a year, just because kde 4 is coming.\n\nThat is also not a good idea.\n\nI don't think porting kdepim to kde4 would be such a huge effort that the whole kdepim team should work on it for allmost a year.\n\nBetter is to continue developing kdepim in a stable branch and to port it to kde4 when it becomes neccesary to do so.\n"
    author: "otherAC"
  - subject: "Re: kdepim 3.5.5+ branch?"
    date: 2006-12-11
    body: "> But you are suggesting to stop all development in kdepim for more then a year, just because kde 4 is coming.\n\nNot at all! I propose that instead of developing new features that need to be ported, people work on porting instead, and then add the new features to the KDE 4.0 version, and deliver them with KDE 4.0. If they work on 3.x stuff, the porting gets harder and harder and slowe and slower.\n\n\n> I don't think porting kdepim to kde4 would be such a huge effort that the whole kdepim team should work on it for allmost a year.\n\nI don't think you realize how severely understaffed kdepim (and KDE and FOSS in general) is. Every hand is needed in the KDE 4.0 porting/development effort if it's supposed to get done in a reasonable amount of time. There's no such luxury of letting most of the developers have fun working on features in 3.x tech while a few core guys work on the port ... especially when those core guys don't exist at all but work on 3.x instead. \n\nThat's the problem ..."
    author: "Peter"
  - subject: "Re: kdepim 3.5.5+ branch?"
    date: 2006-12-12
    body: ">>Not at all! I propose that instead of developing new features that need to be ported, people work on porting instead, and then add the new features to the KDE 4.0 version, and deliver them with KDE 4.0. \n\nthat's allmost the same thing: in the eyes of the user, kdepim development will be halted for more then a year..\n\nAnd i think that developing new features on kde4 at this moment is not a good idea, since a lot of the new kde 4 framework is still unfinished. It would be very hard to develop new stuff on a incomplete and unstable framework.\n\nimho it's much better to start porting at a moment that makes sense then to start porting at this time, while it isn't even sure when kde4 will be released.\n\n>>I don't think you realize how severely understaffed kdepim (and KDE and FOSS in general) is.\n\nI'm aware of that\nBut i don't think that a few more features in kdepim would slow down the porting proces that much.\nWhen it's time to port, the kdepim team will port their applications, i'm sure of that.\nMeanwhile you can't force them to do something that they don't want to do or are not capable enough to do.\n"
    author: "otherAC"
  - subject: "Re: kdepim 3.5.5+ branch?"
    date: 2006-12-12
    body: ">And i think that developing new features on kde4 at this moment is not \n>a good idea, since a lot of the new kde 4 framework is still unfinished. \n>It would be very hard to develop new stuff on a incomplete and unstable \n>framework.\n \nThis is not correct, and hasn't been true for a long time, at least six months.  The KDE4.0 API is most definitely stable enough to develop against.  App developers should be porting their apps *now* so that the core developers can use their feedback to stabilize the API even further.\n"
    author: "LMCBoy"
  - subject: "Re: kdepim 3.5.5+ branch?"
    date: 2006-12-12
    body: "This is all well and good, but Ingo and the team know what they are doing, so let's just leave it to them, since they've done such a bang-up job so far..."
    author: "Joe"
  - subject: "Re: kdepim 3.5.5+ branch?"
    date: 2006-12-11
    body: "i agree with your point about how important pim is for KDE 4, but you do seem to neglect the free nature of free software. if they don't like working on unstable software, it's better for them to work on 3.5.x - at least they do something, and they'll (have to?) port it to KDE 4 later on (or immediately)."
    author: "superstoned"
  - subject: "Re: kdepim 3.5.5+ branch?"
    date: 2006-12-11
    body: "Yes, it's volunteer work, and from that end, on a person-by-person basis, I have no right to complain, since I don't pay. On the other hand, the KDE project does state certain ambitions with regards to what it intends to accomplish, and it must be possible to criticize how developer resources are allocated within the project to that end. That doesn't mean I don't recognize I have no basis to demand anything except on the basis of appealing to their honor to deliver on their stated ambitions."
    author: "Peter"
  - subject: "Re: kdepim 3.5.5+ branch?"
    date: 2006-12-12
    body: "As an outsider, it's hard to figure out how developer resources are allocated. Different developers have different skills. You can't just drop every developer on the kde 4 porting project, some are enough skilled to do so, others are not and do other things, like improving kde 3.5.x\n\nYou may find that kde 3.5 is legacy already, but as long as kde4 isn't even on the horizon, it is certainly not.\n\n"
    author: "otherAC"
  - subject: "Re: kdepim 3.5.5+ branch?"
    date: 2006-12-12
    body: "> You can't just drop every developer on the kde 4 porting project, some are enough skilled to do so, others are not and do other things, like improving kde 3.5.x\n\nBut this is backwards.  Generally, porting your app to 4.0 is much less challenging than coding new features.  It's mostly boring stuff: global string-replace and debugging.\n"
    author: "LMCBoy"
  - subject: "Re: kdepim 3.5.5+ branch?"
    date: 2006-12-12
    body: "From a non-developer, obviously.\n\nAnd if you are, shame on you! Do you even KNOW what porting from 3 to 4 is like? No, obviously you haven't a clue."
    author: "Joe"
  - subject: "Re: kdepim 3.5.5+ branch?"
    date: 2006-12-12
    body: "I'm the lead developer of KStars.  We've been developing in trunk for several months now, and have had a mostly-functional 4.0 version for most of that time.\n\nCare to make a retraction?\n"
    author: "LMCBoy"
  - subject: "Re: kdepim 3.5.5+ branch?"
    date: 2006-12-12
    body: ">>Didn't KDE have Technology Working Group that was supposed to guide the 4.0 cycle and make sure this doesn't happen? Keep a tight ship?\n\nNope, what gets developed and how it gets developed is up to the developers, not up to the technical working group.\n\nIt's not a dictatorship that forces developers to do stuff in their precious free time that they don't want to do.\n\n"
    author: "otherAC"
  - subject: "Re: kdepim 3.5.5+ branch?"
    date: 2006-12-12
    body: "> Nope, what gets developed and how it gets developed is up to the developers, not up to the technical working group.\n\nI realize the TWG can't force anybody, but it could weigh in on these people to make them realize what they're doing is pretty stupid. After all, presumably the TWG members were selected for having a certain influence over their fellow developers."
    author: "Peter"
  - subject: "Re: kdepim 3.5.5+ branch?"
    date: 2006-12-12
    body: "There is a lot of stuff going on with kde4, including kdepim. things get ported, new features are developed, etc. etc.\nMeanwhile 3.5 is improved as well, and for some reason you object to that.\n\nAs said before, it's for an outsider hard to figure out who's developing what and why.\nAnd you can't tell what happens with kde4 just by reading the commit-digest.\n\n\nYou think it's stupid to continue improving kde 3.5; well, I think it's stupid to freeze kde 3.5 completely for more then a year, just because there is a new major version coming up.\nI thinks its a much better idea to keep on improving the experience of kde 3.5 for the users with small updates and patches (new stuff or backported from kde4) like kde is doing at the moment.\n\n\nAnd I don't think that the TWG agrees with you that the kdepim team is being stupid.\n\nThe inclusion of new stuff from kdepim in the upcoming kde 3.5.6 is done with the consent of members of other kde teams.\nFrom what i have read on the mailinglists, no-one found what they are doing stupid.\n\n"
    author: "otherAC"
  - subject: "Re: kdepim 3.5.5+ branch?"
    date: 2006-12-12
    body: "To sum it up: I think it's bad to develop 3.5 and 4.0 concurrently because it makes 4.0 harder and take longer, due to more porting overhead, less manpower and less synergy effects. You disagree, that's OK."
    author: "Peter"
  - subject: "Re: kdepim 3.5.5+ branch?"
    date: 2006-12-12
    body: "Peter, please stop blabbing about what you \"think\" and go find out from the KDEPIM people!"
    author: "Joe"
  - subject: "Re: kdepim 3.5.5+ branch?"
    date: 2006-12-12
    body: "That's not really true. KDEPIM has an agreement that all features have to go into trunk as well...Trunk is what moves forward, not 3.5.x...So, the same features go in both places."
    author: "Joe"
  - subject: "Re: kdepim 3.5.5+ branch?"
    date: 2006-12-11
    body: "ASFAIUnderstand: kdelibs4 are not stable enough to make real porting of complex applications without fixing something in kdelibs each 5 minutes."
    author: "m."
  - subject: "Re: kdepim 3.5.5+ branch?"
    date: 2006-12-11
    body: "It's definitely harder, yes. That also makes it less fun, yes. But it's work that needs to be done. And without people working on porting and using the new libs, the libs development loses momentum as well, as those by-product fixes you cite don't happen.\n\nKOffice seems to manage developing against 4 pretty well, as do a few others. "
    author: "Peter"
  - subject: "Re: kdepim 3.5.5+ branch?"
    date: 2006-12-12
    body: "And koffice continues to release 3.x based versions (like 1.6 recently)"
    author: "otherAC"
  - subject: "Re: kdepim 3.5.5+ branch?"
    date: 2006-12-17
    body: "Just bugfix releases, and some backported features for Krita / Kexi.\nMost of the work is going on in the kde4 based version."
    author: "Thomas Zander"
  - subject: "Re: kdepim 3.5.5+ branch?"
    date: 2006-12-12
    body: "This isn't my experience at all.  5 minutes?  Come on.  We've had a policy for a while in trunk that BC can only be broken on Mondays; that speaks to a reasonably good level of stability.\n\nAre there bugs in kdelibs-4.0?  Of course!  All the more reason that we need as many app developers as possible using it, helping to find them.  Now is the time.\n\n"
    author: "LMCBoy"
  - subject: "Re: kdepim 3.5.5+ branch?"
    date: 2006-12-12
    body: "you understand incorrectly. we passed that point some time ago and now many applications are making excellent progress in kde4, ranging from ksysguard to kjots to okular/ligature to kdegames to ....."
    author: "Aaron J. Seigo"
  - subject: "Re: kdepim 3.5.5+ branch?"
    date: 2006-12-12
    body: "good question.\n\nthe kdepim people are working on both 3.5 and 4.0. akonadi is the big push for kde4 and AFAIK there is a hackfest in january for it. look forward to more news then."
    author: "Aaron J. Seigo"
  - subject: "Re: kdepim 3.5.5+ branch?"
    date: 2006-12-12
    body: "the new features in the 3.5.5+ branch consist mostly of patches found on kde-apps.org. These patches are integrated into kdepim3 AND forward ported to kdepim4.\nI hope this info will diminish your dissatisfaction and you will allow the kdepim devs to do their work in the way they think it's the best. If not, well, start up svn and your favourite editor and let the patches flowing in!\n\nAnd whoever told you that kde4 will only be a port to qt4 was lying to you. There are masses of new features in kde4."
    author: "infopipe"
  - subject: "Re: kdepim 3.5.5+ branch?"
    date: 2006-12-12
    body: "> And whoever told you that kde4 will only be a port to qt4 was lying to you. There are masses of new features in kde4.\n\nI didn't contradict that anywhere. The fact is, however, that the porting needs to happen and reach a reasonable percentage of completion before the development of new features on the 4.0 stuff is viable. Unfortunately the porting is also unsexy and less fun than developing new features. But it needs to be done. "
    author: "Peter"
  - subject: "Re: kdepim 3.5.5+ branch?"
    date: 2006-12-12
    body: "Porting only makes sense when it's time to port it.\n"
    author: "otherAC"
  - subject: "Re: kdepim 3.5.5+ branch?"
    date: 2006-12-12
    body: "Jason Harris seems to have a similar view to mine: http://www.kdedevelopers.org/node/2575"
    author: "Peter"
  - subject: "Re: kdepim 3.5.5+ branch?"
    date: 2006-12-12
    body: "For what I understand from this thread, there is a lot of development with kdepim going on in kde4. \nIt's not like kdepim people are just sitting around waiting for something to happen, and meanwhile add some stuff to kde 3.5.6\n"
    author: "otherAC"
  - subject: "Re: kdepim 3.5.5+ branch?"
    date: 2006-12-12
    body: "I certainly hope you're right -- but unfortunately from my end it currently looks exactly like you say in your last sentence.\n\nMaybe the PIM developers should blog more."
    author: "Peter"
  - subject: "Re: kdepim 3.5.5+ branch?"
    date: 2006-12-12
    body: ">>Maybe the PIM developers should blog more.\n\nHmm, that goes off the time they can spend on developing for kde4 ;)\n\n"
    author: "otherAC"
  - subject: "Re: kdepim 3.5.5+ branch?"
    date: 2006-12-12
    body: "> Hmm, that goes off the time they can spend on developing for kde4 ;)\n\nPoint taken ;)."
    author: "Peter"
  - subject: "Regardless of version, thx for working on it!"
    date: 2006-12-12
    body: "To the people who took up the work on PIM package - big thank you for your efforts, regardless of the version you are working on. The + line had seen so many quality tweaks and improvements, from long-needed menu rearrangements, to the greatly needed Korganizer improvements. \n\nI almost lost hope in KDE mail app and was looking into the only 2 other serious projects - Evolution and Thunderbird. Now you keep my hopes up, my big but humble thank you."
    author: "Daniel D."
  - subject: "Nepomuk and strigi"
    date: 2006-12-12
    body: "i still don't understand what Nepomuk is all about yet, but I like the explanation and I am looking foward to all of this things with strigi. I would really like to have this available and so far Beagle just doesn;t cut it for me. "
    author: "tikal26"
  - subject: "Re: Nepomuk and strigi"
    date: 2006-12-12
    body: "be prepared to get happy, strigi is progressing fast. essentially, Nepomuk will make a search with strigi smarter, as it uses additional information. with strigi, you can find a file which contains 'superstoned', but with strigi+Nepomuk you will also find the file you recieved from 'superstoned' and then saved to your harddisk.\n\nstrigi already finds files in zipfiles, and files in attachments and stuff, unlike beagle.\n\nand last but not least, strigi will not just be smarter and more thorough, but also faster and more stable: it's indexing is up to 6 times faster compared to beagle, and uses a load less memory. so yes, it indexes more files (files in archives and attachments), it will index more information (thanx to Nepomuk), and it automatically computes a md5sum for each file (to identify duplicates) but at the same time, it's still faster..."
    author: "superstoned"
  - subject: "Re: Nepomuk and strigi"
    date: 2006-12-12
    body: "... And that sounds too good to be true.\nI look forward to try Strigi. (Haven't bothered to fix i t yet)."
    author: "Lans"
---
In <a href="http://commit-digest.org/issues/2006-12-10/">this week's KDE Commit-Digest</a>: The beginnings of Sega Genesis/Megadrive support in <a href="http://sourceforge.net/projects/gamefu/">Gamefu</a>. kdegames improvements continue with porting and gameplay work in KBackGammon. OpenDocument master page support in <a href="http://okular.org/">Okular</a>. 'Idle time' detection comes to the 'powermanager' module of the <a href="http://kde-apps.org/content/show.php?content=18703">Guidance</a> system utilies. MIDI format support in <a href="http://ktabedit.sourceforge.net/">KTabEdit</a>. The new histogram graphing functionality of <a href="http://www.vandenoever.info/software/strigi/">Strigi</a> continues to be refined. Following <a href="http://pim.kde.org/akonadi/">Akonadi</a>, <a href="http://nepomuk.semanticdesktop.org/">NEPOMUK</a> starts to utilise the power of Strigi. <a href="http://en.wikipedia.org/wiki/Web_Hypertext_Application_Technology_Working_Group">WHATWG</a> audio objects supported in <a href="http://www.khtml.info/">KHTML</a> through <a href="http://phonon.kde.org/">Phonon</a>. Appointment printing work in <a href="http://korganizer.kde.org/">KOrganizer</a>. <a href="http://kross.dipe.org/">Kross</a> scripting infiltrates <a href="http://koffice.kde.org/kword/">KWord</a>.
<!--break-->
