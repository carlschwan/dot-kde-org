---
title: "KOffice 1.5 Released"
date:    2006-04-12
authors:
  - "iwallin"
slug:    koffice-15-released
comments:
  - subject: "Marvellous"
    date: 2006-04-11
    body: "Thank you for this.  I love Koffice, its usabilty, speed and general niceness. "
    author: "gerry"
  - subject: "Debian unstable"
    date: 2006-04-11
    body: "Whuhuu, Debian unstable has also packages already. Good work everyone!"
    author: "petteri"
  - subject: "Re: Debian unstable"
    date: 2006-04-12
    body: "The i18n packages will be uploaded soon too :)\n\nBest regards"
    author: "Isaac Clerencia"
  - subject: "Yes!!!"
    date: 2006-04-11
    body: "Day started with good news! It's really great day, IMHO:)). I love KOffice for it's stability on  mypreferred platform (i386-OpenBSD) and I'm glad that now I have truely KDE-integrated stable (I hope... I believe!:)) office suite! THank you, guys, and thanks again!"
    author: "Vadim Jukov"
  - subject: "KOffice 1.5 Highlights Tour online!"
    date: 2006-04-11
    body: "Together with other KOffice developers I have put together a comprehensive tour through new features and improvements of the KOffice 1.5 release - the most exciting highlights, check it out here:\n\nhttp://www.koffice.org/tours/1.5/index.php"
    author: "Raphael"
  - subject: "Re: KOffice 1.5 Highlights Tour online!"
    date: 2006-04-12
    body: "Brilliant! Thank you!"
    author: "Paul Eggleton"
  - subject: "Koshell"
    date: 2006-04-12
    body: "I dont really understand the purpose of koshell. Its in the way and unbelievable cluttered - its not any help at all. In fact i dont uderstand why much else than kword is included as they are hardly usable. Please concentrate on kword and dump the rest. "
    author: "josel"
  - subject: "Re: Koshell"
    date: 2006-04-12
    body: "Ah the smell of troll in the afternoon. "
    author: "ziggy"
  - subject: "Re: Koshell"
    date: 2006-04-12
    body: "How childish of you to take the easy way out, ziggy boy. The lack of quality of koffice is no secret due to the lack of developers. It would simply be a better idea to concentrate on kword - an excellent and different type of editor- instead of wasting ressources on kpresenter, kexi or whatever. At least reduce koffice to  two or three official apps and get the others out of the way - nobody uses them anyway and they lower the overrall quality. And dump koshell - it does not serve any purpose when its implementet so lousy."
    author: "josel"
  - subject: "Re: Koshell"
    date: 2006-04-12
    body: "c'mon... the lack of quality... that's not true! you might say Koffice doesn't have all features MS office or OO.o have, but it certainly is not bad, it's already very complete, and even has some points in which it surpasses both mentioned office suites. Krita is a wonderfull app, goin' for world domination. kpresenter is very decent, and so are most other components. plus, there is no office suite that has half the integration Koffice has."
    author: "superstoned"
  - subject: "Re: Koshell"
    date: 2006-04-13
    body: "There is an obvious lack of quality even in kword. I dont complain about that - i just complain about the fact that kword is so much better than the other apps in the suit, that the other apps are not in the same league. Since they obviously lack developers - a complaint koffice devs often air - the solution is clear - dump anything other than kword!\nI use word daily at work and dont miss any features in kword og oowriter.\nKoshells problem is the GUI - its really unfriendly the besides being butt-ugly.   I really fail to see the purpose for it - as someone else pointet out -there is kicker why is that shell needed?"
    author: "josel"
  - subject: "Re: Koshell"
    date: 2006-04-13
    body: "Putting aside the ridiculous nature of your conclusions, you make the incorrect assumption that many others have made about open source software which is that if developers weren't working on something you don't want then they would automatically start working on the stuff that you do want. \n\nOpen source developers tend to work on things that they are personally interested in. I'd imagine that many of the KOffice developers would not be as keen to work on KOffice if it only included KWord, so nothing would really be solved by \"dumping\" the other applications."
    author: "Paul Eggleton"
  - subject: "Re: Koshell"
    date: 2006-04-13
    body: "I dont make any assumptions. I just conclude that anything else than kword, that i have tryed, does not live up to kwords quality. Its not that kword is that good either but its usable and fullfills the needs i have with it.\nExactly the same conclusions have been made by others here and in other places. I think its a bad idea to join half-baked apps to a suit just to have a suit. \nYour ridiculous conclusions about what i want do not matter since I did not mention what i want. I dont intend to formulate \"demands\" to people that develop for free."
    author: "josel"
  - subject: "Re: Koshell"
    date: 2006-04-12
    body: "Why don't you *try* some apps before trolling? Get the new release and try, let's say, Kexi and Krita, and tell me they are not good apps.\n\nThen we can talk. The only app I can say is not really up to speed is KSpread. It is not fast and reliable enough for large spreadsheets. But it is getting better, and I would encourage its developers, never say it is crap.\n\nOne of the main advantages of KOffice is the wide range of apps. Different people have different interests. A developer may want to work on the formula engine of KSpread, other in Krita's tablet support and another in KWord footnote support. If you kill the other apps, you will end up with less developers, not more, as they will not automagically change their interests."
    author: "Amadeo"
  - subject: "Re: Koshell"
    date: 2006-04-13
    body: "I'm not trolling. I've been using Koffice since the beginning and KDE since the summer of 98. Some of  you guys here seem to think that any critique is an attack. Sad for you. \nThe thing im saying is dump koffice and concentrate only on a few apps. The only app that works fine is kword so concentrate on that one - the others are an enbaressement. \nI dont use krita - i find it ugly and unpleasent to use. I prefer inkscape and the old killustrator. Kexi i dont see the purpose for it. I dont even  know anyone that uses those type of apps. Is access really used that much in offices? "
    author: "josel"
  - subject: "Re: Koshell"
    date: 2006-04-13
    body: ">I've been using Koffice since the beginning and KDE since the summer of 98.\n\nSo you use it since it was actually unusable...\n\n>The thing im saying is dump koffice and concentrate only on a few apps. The >only app that works fine is kword so concentrate on that one - the others are >an enbaressement.\n\nIf you think they are bad then don't use them. To say to a developer to drop\nsome apps not even caring to tell him why is simply stupid. You say krita and\nkexi are an embarassment. Care to explain why? How do you think they could \nimprove? If you don't have anything constructive to say other than \"I don't know anyone that uses it\" then, better shut up. \n\n>I dont use krita - i find it ugly and unpleasent to use.\n\nAnd who cares what YOU find ugly and umpleasant?\n\n>I prefer inkscape and the old killustrator.\n\nAnd who cares what YOU prefer? KOffice developers keep you from using the\nsoftware you prefer?\n\n>Kexi i dont see the purpose for it.\n\nAmusing. You have the nerve to criticise a program you don't even know the\npurpose of it. If the fact that \"Josel don't see the purpose of it\" were\nenough reason to force devs to drop their apps, I'm afraid that 99% of the\nsoftware in the world would disappear in a fortnight.\n\n>I dont even know anyone that uses those type of apps.\n\nAnd, obviously, you believe that the fact that YOU don't know anyone that uses\nan app is more than enough reason for the developers of that app to drop it.\n\nAmusing.\n\n>Is access really used that much in offices?\n\nGo tell Microsoft to drop Access an concentrate on Word because \"Josel don't\nsee the point of that kind of app\".\n\nYou are a laugh my friend.  "
    author: "monton"
  - subject: "Re: Koshell"
    date: 2006-04-13
    body: "> So you use it since it was actually unusable...\nI used it since before version 1.0. You dont find any complaints from me prior to that. And you wont even find many complaints after that. I just get frustrated with the stupid marketing speek. \n\n>you think they are bad then don't use them. To say to a developer to drop\n>some apps not even caring to tell him why is simply stupid. You say krita and\n>kexi are an embarassment. Care to explain why? How do you think they could\n>improve? If you don't have anything constructive to say other than \"I don't know >anyone that uses it\" then, better shut up. \n\nI dont use them them because i find them bad and their equivalents superior and not so krash prone. I couldnt even open karbon whithout running kbuildsysca - hey im no developer. So no i dont shut up \n\n>And who cares what YOU find ugly and umpleasant?\n>And who cares what YOU prefer?\nI do!\n\n>Amusing.... bla bla bla\nRelax. Thats just my opinion. Im not that important\n\n>You are a laugh my friend.\nNo you are. Im just expressing my opinion. You go bananas because of it? Go eat your pill."
    author: "josel"
  - subject: "Re: Koshell"
    date: 2006-04-14
    body: ">>And who cares what YOU find ugly and umpleasant?\n>>And who cares what YOU prefer?\n>I do!\n>\n>>Amusing.... bla bla bla\n>Relax. Thats just my opinion. Im not that important\n\nWell, I see you got the point: Nobody cares about your opinion. \nThat could be different if you were an informed user who actually knows\nwhat he is talking about. Unfortunately that's not the case..."
    author: "monton"
  - subject: "Re: Koshell"
    date: 2006-04-13
    body: "> Kexi i dont see the purpose for it. I dont even know anyone \n> that uses those type of apps. Is access really used that much in offices?\n\nHere you show your ignorance. Access is widely used by businesses who in many cases don't have in-house professional developers, and find Access much easier to deal with than something like VB for developing simple data-driven applications. Users have been clamouring for a long time now for an open-source Access replacement, and IMO until Kexi came along there wasn't really one piece of software that satisfied that need. In fact Kexi goes further than Access in many areas and may even be easier to use in many respects."
    author: "Paul Eggleton"
  - subject: "Re: Koshell"
    date: 2006-04-13
    body: "Wow. I would like to see some numbers for \"users\" that want Access and especially for end-users in buzinezzzzz that are able to make anything interesting with Access and dont have a developer/geek background. The last cathegory i dont think exist.\nThere is lots of in-house developed Excel stuff but praticaly no Access stuff. I only ever came ocross one Access app - and not developed by business people.\n\nBut this discussion is pointless. Developer develop whatever they want. I simply think that there should be no Koffice since the quality of software other than kword is too low."
    author: "josel"
  - subject: "Re: Koshell"
    date: 2006-04-13
    body: "Josel, your experience is very, very limited. If you have seen only one Access application you simply haven't seen much of the world. In the fourteen years since I translated the first edition of the Access Bible into Dutch I've seen scores of Access applications created by secretaries, sales people, but also engineers and scholars. And programmers, yes, them too. Not to mention the number of Access frontends to Excel spreadsheets. In the real world, both get a lot of use.\n\nAs for thinking that there should be no KOffice, well, make a noise like a hoop and roll away, will you? This silly nonsense is getting tiresome."
    author: "Boudewijn Rempt"
  - subject: "In defense of MS Access"
    date: 2006-04-14
    body: "I am an Oracle developer, but I use Access every day. I have to process data from many different sources, and Access is the lingua franca for all of them.  I can write a complex query in just a few seconds and get the results in an editable recordset. I can write custom functions using regular expressions, xml, and all the VB6 and VBA functions.  I can quickly create interfaces using tree-views, drop-downs, sub-forms etc.  People that denigrate Access just haven't seen it used competently.  My only complaint is that Microsoft has neglected it lately in order to promote sqlsvr and dot-net.  Will KOffice ever provide a coding interface comparable to VBA?"
    author: "Bob Park"
  - subject: "Re: In defense of MS Access"
    date: 2006-04-14
    body: "Comparable to is one of our goals, same as not. Kross is really cool in that it makes it easy to create an object-oriented interface to an application or library then that interface is available from all languages for which there's a Kross interpreter. That's Python and Ruby for now, but there have already been people interested in doing a Basic interpreter or a Javascript interpreter.\n\nBut the object model will in all probability always be different from VBA, because the object model of the underlying application is different."
    author: "Boudewijn Rempt"
  - subject: "Re: Koshell"
    date: 2006-04-14
    body: "In fact, some people would be surprised to know the amount of people who actually earns a living with MS Access."
    author: "monton"
  - subject: "Re: Koshell"
    date: 2006-04-12
    body: "Just because you don't use or want a presentation or database application that is nicely integrated with KDE, doesn't mean that everyone else shares that view."
    author: "Paul Eggleton"
  - subject: "Re: Koshell"
    date: 2006-04-12
    body: "Very strange of you to use Kexi as an example.  Kexi is the program that is truly unique in KOffice, and the one that very many people have been waiting for. Besides, one of the main strengths of KOffice is its breadth and integration, something that many mention as the direct reason to use KOffice from the beginning.\n\nRegarding koshell, we would be interested in hearing how you think it could be improved. "
    author: "Inge Wallin"
  - subject: "Re: Koshell"
    date: 2006-04-12
    body: "> Regarding koshell, we would be interested in hearing how you think it could be improved. \n\nDrop it. It's crappy and useless. :)"
    author: "Carlo"
  - subject: "Re: Koshell"
    date: 2006-04-12
    body: "The problem is that it was the result of users -- just like you -- complaining \"I'd use KOffice if it were more like Outlook\".  (Seriously -- go back in the mailing lists and even on this board).  That integration that you dislike so much?  That's the result of listening to users.\n\nSo, you're a user, eh?"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Koshell"
    date: 2006-04-12
    body: "> So, you're a user, eh?\n\nSure. You aren't!? ;) I'm not interested digging into the history of koshell, but always assumed it should become something similar to the dead old MS Office toolbar. If there was a well defined goal to reach with koshell (no, the Outlook quote doesn't count imho), the result failed to deliver.\n\n> \"I'd use KOffice if it were more like Outlook\".\n\nProbably an example, that it's not always fruitful to  listen to every weird wish/opinion. An office suite simply isn't a PIM.\n\n> That integration that you dislike so much?\n\nNo. Koshell is just wasted space on the desktop. When I want be able to start an applicaton quickly, I add it to Kicker. When I want to insert another (KOffice) object I do it from within the application. I haven't even a clue what you mean with the integration stanza in context of koshell. To me it's a weird, unusable thing I never have used and never heard or read that anyone is using it."
    author: "Carlo"
  - subject: "Re: Koshell"
    date: 2006-04-13
    body: "Hmm\nif there is something you don't like about any aspect of koffice or any kde application you are more than welcome to change it to fit your needs our kde developers code for them selfs and the community as a selfless act something they do not have to do.\n\nAnother option is to pay a developer to implament the changes you wish to be made im sure there is a c++/Qt developer more than willing to take your money.\n\nYou really should not speak bad of the koffice project it is free.\n\nIf you wish to learn about the resources you will need to be able to contribute\nto the koffice project there are to very good books you can get.\n\n1. C++ without fear\n2. C++ GUI Programming with QT3\n3. Just google for all the free c++ and kde/qt tutorials.\n\nUntill you have the motivation to find out what it is like to be in there shoes\ni wouldn't complain about koffices quality.\n\nLast but not least have just a little bit more respect when you post here all\nyour doing is showing everyone the type of jackass you really are :).\n\n\n\n"
    author: "Comtux"
  - subject: "Re: Koshell"
    date: 2006-04-13
    body: "I can't hear this sort of blah, blah anymore. Everyone has an opinion on everything. Calling one to hold his mouth, because he gets something for free is extremely stupid, but unfortunately not uncommon in the FOSS world, when someone isn't able to grok opinions of others.\n\n\n> You really should not speak bad of the koffice project it is free.\n\nI haven't done so. I spoke about a single, minor, unusable part of it. I haven't even a problem with koshell being a part of KOffice. It's just not worth developers time / maintenance.\n\n"
    author: "Carlo"
  - subject: "Re: Koshell"
    date: 2006-04-13
    body: "And just how can YOU tell a developer what is worth his/her time?  If they are developing it, it is obviously worth the time to THEM.  Especially given the fact that they are working on it for free and aren't being mandated by a company to do it.  Just because you see no value in it does not mean there is no value in it."
    author: "cirehawk"
  - subject: "Re: Koshell"
    date: 2006-04-14
    body: "So you're another one, who doesn't know what an opinion is. I tell no one what to do. I simply answered Inge's question from my point of view. What the KOffice developers do is up to them.\n\n\n\n"
    author: "Carlo"
  - subject: "Re: Koshell"
    date: 2006-04-14
    body: "Oh I know what an opinion is.  I also know that most people don't mind criticism or advice if it's viewed as constructive.  Saying someone's work is a waste of time just comes across badly.  I personally never use Koshell, but I won't trash the developers efforts if they choose to develop it."
    author: "cirehawk"
  - subject: "Re: Koshell"
    date: 2006-04-14
    body: "Well if koshell is the part of koffice you do not like then remove it from your copy.\n\nAnd upload the changes somewhere others can also make use of your changes better yet supply the community with a patch.\n\nEnought said."
    author: "Comtux"
  - subject: "Re: Koshell"
    date: 2006-04-14
    body: "You can install KOffice without koshell already (when the distributor supports it or when you compile from source). This wasn't my point anyways."
    author: "Carlo"
  - subject: "Re: Koshell"
    date: 2006-04-15
    body: "No i get your point.\nBut i cant bring myself to agree, see i think ms windows is a useless peice of software and a huge waist of time i fought windows users for years trying to show them alternatives but in the end people just get mad and i learned a valuble lesson if even 1 person finds value in something then everything that had to be endured in the process of its creation was worth it.\nEven if the only person that found any usefullness for say koshell was a single developer then it creation has been justifyed. no matter what!\n\n"
    author: "Comtux"
  - subject: "Problems installing in suse 10"
    date: 2006-04-12
    body: "I am having all kinds of broken dependencies when trying to install this on Suse 10.0.\n\n    libmysqlclient.so.14 is needed by koffice-database-mysql-1.5.0-2\n        libpqxx-2.2.7.so is needed by koffice-database-psql-1.5.0-2\n        kdelibs3-devel is needed by koffice-devel-1.5.0-2\n        libruby.so.1.8 is needed by koffice-ruby-1.5.0-2\n        libgsf-1.so.113 is needed by koffice-wordprocessing-1.5.0-2\n        libwv2.so.1 is needed by koffice-wordprocessing-1.5.0-2\n        libgsf-1.so.113 is needed by libwpd-0.8.4-3\n        libgsf-devel is needed by libwpd-devel-0.8.4-3\n        glib2-devel is needed by libwpd-devel-0.8.4-3\n        libgsf-devel is needed by libwpd-tools-0.8.4-3\n        glib2-devel is needed by libwpd-tools-0.8.4-3\n        libgsf-1.so.113 is needed by libwpd-tools-0.8.4-3\n\nAnybody have any tips?"
    author: "Marc"
  - subject: "Re: Problems installing in suse 10"
    date: 2006-04-12
    body: "Broken dependencies? kdelibs3-devel etc.? You must be joking.\n\n> Anybody have any tips?\n\nInstall the required rpms? Skip packages you don't need (devel, mysql, pysql)?\nUse a packet manager like YaST to resolve/install dependencies?"
    author: "Anonymous"
  - subject: "Re: Problems installing in suse 10"
    date: 2006-04-13
    body: "Did You upgrade via YaST? I did, and all dependencies were resolved (SUSE 10.0, AMD64.\n"
    author: "Olav P."
  - subject: "Re: Problems installing in suse 10"
    date: 2006-04-14
    body: "I am having the exact same problem installing Koffice write.  YAST does not resolve the problems.  I have also tried separately installing the missing links but seem to be in an endless loop of one requiring the other (i.e., libgsf-1.s0o.113 and libwpd-0.8 are both broken links needing each other).\n\nOne reply here hints that only stupid people would have trouble.  Well, it really doesn't matter because I am another user who cannot get Koffice to work and if the Koffice people really want to develop any loyalty they should fix the difficulty.  I've spent several hours trying to get this to work and there is high likelihood that I will not get back to this in the near future.  I'm left with the feeling that Koffice is not worth the bother and I wonder how many other people will feel the same way.\n\nBy the way, I have been programming in many languages and operating systems beginning with IBM 1620 in 1965 so I can usually hack my way through problems.\n\nRegards\nPaul Thomas"
    author: "Paul Thomas"
  - subject: "Re: Problems installing in suse 10"
    date: 2006-04-14
    body: "You have to install KDE 3.5.2 from supplementary repository."
    author: "Anonymous"
  - subject: "Re: Problems installing in suse 10"
    date: 2006-04-14
    body: "Sorry, but try to understand: the KOffice people (meaning me, David, Thomas, Rob, Laurent, Thorsten, Peter, Alfredo, Ariya, Tomas, Jaroslaw and a host of other people) cannot do a thing to fix the SuSE packages. We don't make those packages, creating packages is a whole different kind of expertise, one that a developer doesn't have, in general. \n\nYou're not stupid for not getting the SuSE packages to work, in fact, I, and I'm a KOffice developer, cannot get Krita not to crash when doing color adjustment under SuSE. It works perfectly with Kubuntu...\n\nThat said, SuSE is a great distribution and the distribution I prefer to use on my server and I simply wouldn't be able to create better packages. I can code nor do documentation; I cannot create packages. Other people cannot code; they do documentation, other people can do packages. \n\nBut things get broken, errors do occur. It's impossible to develop software and not depend on external libraries -- unless you're a one hundred  man strong team, paid to do development fulltime, and do everything yourself, like the OpenOffice team -- and even then, I would question the wisdom of doing without libraries. Libraries are a fact of life: and outside our control. Sorry for that."
    author: "Boudewijn Rempt"
  - subject: "Re: Problems installing in suse 10"
    date: 2006-05-15
    body: "I'm a recent \"switcher\" sort of, from Windows to Linux Suse 10.  I am having trouble installing anything on Suse then finding it, and making it work.  It is so much easier in Windows to install a program.  \n\nI am otherwise very impressed with Suse Linux.  I also have high hopes that Linux will soon offer an everyday user a alternative to Windows.  I think Linux is close but for me at least installing \"stuff\" on Linux Suse is a complete puzzle.  \n\nThanks much,\n\nPaul"
    author: "Paul Russell"
  - subject: "Packages for Tomahawk Desktop"
    date: 2006-04-12
    body: "Very good news. Anybody offering package for Tomahawk Desktop? "
    author: "Paul"
  - subject: "Re: Packages for Tomahawk Desktop"
    date: 2006-04-12
    body: "With Tomahawk Desktop, you don't have to wait until somebody package it for you. Just do following:\n\n1. Open a Konsole, type su and login as root\n2. su koffice\n3. uninstall_package koffice\n4. (a) Copy the new koffice-1.5.0.tar.bz2 to your current directory (/usr/src/koffice/) if you have already downloaded the source package. OR\n   (b) If you have not downloaded the source package, just type following and press Enter:\nwget ftp://ftp.kde.org/pub/kde/stable/koffice-1.5.0/src/koffice-1.5.0.tar.bz2\n5. Check the md5sum:\n   md5sum koffice-1.5.0.tar.bz2\n   it should be: 220b6bde28b5ebd5c6621bc87ca2c05f\n6. bunzip2 koffice-1.5.0.tar.bz2\n7. tar xf koffice-1.5.0.tar\n8. cd koffice-1.5.0\n9. ./configure --prefix=/usr --sysconfdir=/etc --disable-debug\n10. make >& koffice-make.log\n11. make install >& koffice-install.log\n12. exit\n13. exit\n14. exit\n\nIts the same concept to upgrade any other package. \n\nGood luck!"
    author: "Anon"
  - subject: "Re: Packages for Tomahawk Desktop"
    date: 2006-04-12
    body: "> With Tomahawk Desktop, you don't have to wait until somebody package it for you.\n> 9. ./configure\n> 10. make\n> 11. make install \n\nWhat a quite unique feature of Tomahawk Desktop!"
    author: "Anonymous"
  - subject: "Re: Packages for Tomahawk Desktop"
    date: 2006-04-12
    body: "well, as Tomahawk installs Koffice now as a user (Koffice) you can just remove all files from this user, and Koffice is removed. or package all files of this user, and you have a package. so it is a bit more clever than the normal 'configure/make/make install', and very unix-like. no central database that can be broken, or stuff like that...\n\ntough i still prefer a decent package management like debian or gentoo have :D"
    author: "superstoned"
  - subject: "Re: Packages for Tomahawk Desktop"
    date: 2006-04-12
    body: "and you package your .tar.gz to a .run, you have a Nvidia style package :)"
    author: "Savithree"
  - subject: "Re: Packages for Tomahawk Desktop"
    date: 2006-04-12
    body: "If you're going to compile anyway, I prefer the Gentoo way:\nemerge -av koffice\n\nLooks a lot simpler than this Tomahawk thing that keeps getting spamvertised here."
    author: "Andre Somers"
  - subject: "Problems in Ubuntu Breezy"
    date: 2006-04-12
    body: "There are some strange problems in Ubuntu Breezy, using KDE 3.5.1\n\nI couldnt use karbon at all, and then i couldnt also use krita. There was a problem related to no colourspaces available. You have to run kbuildsycoca manually, and all the problems are gone.\n\nCan this be a problem with the packages ?\n\nAnyway, thanks for the awesome office.\n\nAnd for those who think that they should concentrate efforts in kword: put a site on the web, get some money, hire a developer and say to him: work with kword, NOW. We all would be benefited (except the developer, i guess)."
    author: "Henrique Marks"
  - subject: "MS format import"
    date: 2006-04-12
    body: "Hi,\n\nSo, how does KOffice's MS Office format (Word, Excel etc.) compared to the ones in OpenOffice 2.0? Are they both using the same? As I understand, OpenOffice 2.0's import converts to OpenDocument - so *in theory* one should be able to use the same import filters. Is this true? Or, is there any plan to do so? \n\nthanks,\nOsho"
    author: "Osho"
  - subject: "Re: MS format import"
    date: 2006-04-12
    body: "KOffice doesn't aim at supporting MS Office format (and we don't have the ressource to do it, Sun has payed full time developpers to work specificaly on that features for some years to achieve a good support in OOo2).\nSo the support isn't good.\nAnd yes in theory we could use the same import filter (in fact libwv2 was an effort to do that, but only koffice use it :( ). But OOo2's filter can't be easily extracted to be used in KOffice. So if you really need .doc/.xls and so on, but preffer to use KOffice, then you could still use OOo2 as a converter between MS Office's files and OpenDocument."
    author: "Cyrille Berger"
  - subject: "Re: MS format import"
    date: 2006-04-12
    body: "Well, libwv2 was an attempt to share .doc filters with Abiword, not OOo. But the Abiword guys then decided to continue using libwv1 and we were left with an incomplete library only KWord was using.\n\nIt's quite impossible to extract the OO .doc filters from OO since they are not implemented as filters, but as another kind of native format -- which means OO.o .doc imports directly into its own data structures, instead of going via OpenDocument.\n\nIn theory it should be possible to script OOo to convert from .doc to .odt and then feed it to KWord in an unobtrusive (but slooooow) way."
    author: "Boudewijn Rempt"
  - subject: "kspread"
    date: 2006-04-12
    body: "Kspread is killing me.  Somewhere in the transition from 1.3 to 1.4 it became ridiculously slow at opening documents, and 1.5 hasn't helped.  http://cvs.sourceforge.net/viewcvs.py/*checkout*/wcuniverse/priv/units/units.csv?rev=1.32 is a .csv containing stats for ships and stations and the like in a space game, and is a file I used to open every day. It weighs in at 319kb, and opens in a couple seconds with kwrite.\n\nWith kspread: the window comes up 2 seconds after clicking the file.  50 seconds after that the import dialog appears, where I get to confirm that commas are the separating value.  250 seconds after that it is loaded and ready for action.  (all of this is on a 1.4 ghz athlon.  No spring chicken, but not a slouch).  That's 5 minutes before I can use the file.  1.3 was nowhere near that bad.  \n\nI hear rumours of kspread being reworked for 2.0.  Please please bring it soon.  And please don't continue the trend, but reverse it :("
    author: "MamiyaOtaru"
  - subject: "Re: kspread"
    date: 2006-04-12
    body: "> It weighs in at 319kb, and opens in a couple seconds with kwrite.\n\nThis may be because kwrite do not try to preprocess the file at all.\n\nI recommend you to look at Kexi for the task; the app is designed and optimized for handling large data chunks faster. Kexi uses improved version of the import dialog you know from KSpread: http://kexi-project.org/pics/1.0/csv_import.png. A small benchmark for your file on Kexi 1.1 alpha using the similar hardware:\n\n* loading the preview dialog: <0.5 seconds (just 6 rows were loaded as Kexi detected 116 or so columns in the file)\n* importing all the data: ~2 seconds\n\nThe drawback in Kexi is that you will not get all editing/formatting features of spreadsheets as the app is relational database environment. \nNote that KSpread 2.0 will most likely reuse the optimized CSV import with Kexi.\n\n--\nregards,\n\nJaroslaw Staniek\nKOffice/Kexi Team\n"
    author: "Jaroslaw Staniek"
  - subject: "Re: kspread"
    date: 2006-04-12
    body: "I've just committed a fix for this. It decreases the loading time for your file from 55 to 3 seconds (after pressing the OK button in the dialog).\n\n"
    author: "Fredrik Edemar"
  - subject: "Re: kspread"
    date: 2006-04-13
    body: "We do plan to rewrite many parts of KSpread, aye, and we do hope to solve all those problems that make it very slow for bigger files - time will tell how much successful our efforts will be."
    author: "Tomas Mecir"
  - subject: "Re: kspread"
    date: 2006-04-13
    body: "I have noticed one bug? since the upgrade. When I choose cell formating and changes the value from \"variable\" to \"2\" Kspread behaves nicely and actually changes precision from variable to 2.\n\nHowever, when I save ande close the file and opens it again the precision format is \"variable\" again.\n\nHas anyone else seen this?\n\n(SUSE 10.0, AMD64)"
    author: "Olav P."
  - subject: "Fails to work on Suse 10 using the rpm packages"
    date: 2006-04-13
    body: "Installed on SuSE 10 Pro, had a bit of truble finding (libgsf-1.13.3-2.2.1.x86_64), but I found it and got everything installed only to receive the error:\nkoffice (lib kofficecore): WARNING: /opt/kde3/lib64/libkofficeui.so.3: undefined symbol: _ZN9KIconView22contentsDragLeaveEventEP15QDragLeaveEvent\nWhen ever I try to start any office product up.\n\nAny ideas?\n\n\n"
    author: "Eric Gandt"
  - subject: "Re: Fails to work on Suse 10 using the rpm packages"
    date: 2006-04-17
    body: "You have to update your KDE to 3.5.x first: http://en.opensuse.org/Updating_KDE_and_other_software_via_YaST"
    author: "Anonymous"
  - subject: "Question about KWord"
    date: 2006-04-13
    body: "Does this release fix bug with overlapping letters, not only on screen but also in Print (preview)? Without it I consider KWord useless.\nExample:\nhttp://img225.imageshack.us/my.php?image=snapshot54xa.png"
    author: "M-Z"
---
The KOffice team is proud to announce KOffice version 1.5. With this release,  KOffice starts its ascent into the office suite hall of fame. This version sports OpenDocument as the default file format, accessibility, a new project planning tool KPlato, professional color support and adjustment layers in Krita and the long awaited Kexi 1.0. You can read more about it in the <a href="http://www.koffice.org/releases/1.5-release.php">press release</a> and the <a href="http://www.koffice.org/announcements/announce-1.5.php">full announcement</a>.  Packages are available for <!-- <a href="http://pkg-kde.alioth.debian.org/">Debian</a>,--> <a href="http://kubuntu.org/announcements/koffice-15.php">Kubuntu</a> and <a href="http://download.kde.org/stable/koffice-1.5.0/SuSE/">SUSE</a>.


<!--break-->
<p>KOffice was the first to support, and now with 1.5 the second office suite to announce full support for OpenDocument (ODF) as the default file format. With two independent implementations of ODF now available it is now a true industry standard. KOffice 1.5 is therefore the version that lets enterprises and organisations all over the world choose the office suite that fits their needs best.</p>

<p>Great care has been taken to ensure interoperability with other office software that supports OpenDocument, most notably OpenOffice.org.  We acknowledge, however, that the ODF support and interoperability is not yet perfect. We hope to be able to quickly identify and fix the incompatibilities that do exist in the upcoming 1.5.1 and 1.5.2 bugfix releases.</p>

<p>KOffice is already the most comprehensive office suite in existence. In addition to the basic productivity applications mentioned above, it also includes a creativity application suite featuring <a href="http://www.koffice.org/krita/">Krita</a>, a paint application with lots of image manipulation features, <a href="http://www.koffice.org/karbon/">Karbon</a>, a vector graphics application and <a href="http://www.koffice.org/kivio/">Kivio</a>, a flowcharting application. Other components are <a href="http://www.koffice.org/kexi/">Kexi</a>, an integrated environment for creating database applications and <a href="http://www.koffice.org/kplato/">KPlato</a>, a program for project management.</p>


