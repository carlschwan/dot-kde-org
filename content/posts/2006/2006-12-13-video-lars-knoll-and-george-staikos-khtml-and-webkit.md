---
title: "Video: Lars Knoll and George Staikos on KHTML and WebKit"
date:    2006-12-13
authors:
  - "liquidat"
slug:    video-lars-knoll-and-george-staikos-khtml-and-webkit
comments:
  - subject: "http://www.khtml.info "
    date: 2006-12-13
    body: "Database error\n A database query syntax error has occurred. This may indicate a bug in the software. The last attempted database query was: \n(SQL query hidden)\n from within function \"LinkCache::addLinkObj\". MySQL returned error \"1146: Table 'db_752zxrweaw.khtml_mw_page' doesn't exist (mysql.key-systems.net)\"."
    author: "BogDan"
  - subject: "Compiling failed :("
    date: 2006-12-13
    body: "Yesterday I tried building WebKit but unfortunately I ran into several problems. Neither with qmake nor with newest cmake did compiling succeed. JavaScriptCore worked after some fixes but the rest failed with missing files and wrong/missing declarations. \nI really hope it will soon work. There are two main reasons why I am interested in WebKit/Unity:\n1. WebKit supports content-editable which means you can have these nice WYSIWYG editors embedded in web-pages. This is really needed for newer wikis, webmailers and web-based content management systems.\n2. It only depends on Qt (like Konq-E) - so I will have a platform independent and powerful html widget where kdelibs are not available.\n\nThanks and keep up your good and cool work :)\n"
    author: "Henning"
  - subject: "I hope we can reach some unity"
    date: 2006-12-13
    body: "I really, really , really hope that somehow the khtml and webkit developers find a concensus. I know that apply might need to open webkit a little bit more to make webkit back to konqueror possible, butI think for the sake of konquereor tha tis what we need. I feel that firefox is really running away with the linux user base. I would love to see webkik put on khtml. Like others have said before I would love to see an engine developed by KDE, Apple and Nokia."
    author: "tikal26"
  - subject: "what is this?"
    date: 2006-12-13
    body: "This seems to be more of a presentation given to a crowd rather than an interview? There might be some clues at the end (I'm currently at 16:38), but where and when was this?"
    author: "ben"
  - subject: "Yahoo is just crazy"
    date: 2006-12-13
    body: "There are so many similar internet pages readily available. Why bother and select a different user agent?\n\nInteresting video BTW - thanks for sharing.\n"
    author: "Michael"
  - subject: "and what?"
    date: 2006-12-13
    body: "could you please end your last sentence? :)"
    author: "Patcito"
  - subject: "Re: and what?"
    date: 2006-12-14
    body: "Yea, I know, was my fault - I rewrote a part and forgot to delete the rewritten part, but it is fixed now :)\n\nAnd I was also a bit wrong about the fact that it is an interview - actually I write the small text above as a note for dot.kde.org and didn't expect that it was taken as the article itself - I thought someone else would write an article about it :D\n\nNext time I will be prepared :)"
    author: "liquidat"
  - subject: "Video doesn't work"
    date: 2007-01-06
    body: "I just see a black rectangle where the video should appear.\nI can hear the audio but can't see any picture.\n\nWhen I go to Yahoo Video to watch the video there, I don't see any video player at all.\n\nWhat kind of crap is that?"
    author: "Evil Homer"
  - subject: "Re: Video doesn't work"
    date: 2007-01-07
    body: "Works for me so the \"crap\" must be in your setup.\n"
    author: "cm"
---
Yahoo! user interface blog hosts an <a href="http://yuiblog.com/blog/2006/12/11/knoll-staikos-video/">interview video with Lars Knoll and George Staikos</a> on <a href="http://www.khtml.info">KHTML</a> and <a href="http://www.webkit.org">WebKit</a>. The video features the history of Konqueror (first 10 minutes) as well as the current development situation (next 10 minutes), an outlook about the possible future and of course a short demo presenting Qt4-WebKit accessing the Yahoo! page and rendering it nicely (last 10 minutes). You need Flash to view the page - if you don't have Flash, read <a href="http://liquidat.wordpress.com/2006/12/13/video-about-khtml-and-webkit/">a short summary of the interview</a>.



<!--break-->
