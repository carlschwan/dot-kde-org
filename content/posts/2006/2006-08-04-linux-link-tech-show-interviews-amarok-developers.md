---
title: "Linux Link Tech Show Interviews Amarok Developers"
date:    2006-08-04
authors:
  - "jriddell"
slug:    linux-link-tech-show-interviews-amarok-developers
---
<a href="http://www.tllts.org/">The Linux Link Tech Show</a> has interviewed Amarok developers Max Howell and Ian Monroe in <a href="http://www.tllts.org/dl.php?episode=151">their latest show</a>.  Max talks about Amarok's many features and what they plan for KDE 4, while Ian explains what the main goals of the Amarok project are.  Start 12 minutes in to skip the technical problems and listen to the interview.





<!--break-->
