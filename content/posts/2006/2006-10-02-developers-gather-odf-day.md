---
title: "Developers Gather for ODF Day"
date:    2006-10-02
authors:
  - "jriddell"
slug:    developers-gather-odf-day
comments:
  - subject: "ODF"
    date: 2006-10-02
    body: "Held is Dg Enterprise IDABC, which means she has little influence on the Commission policy as IDABC is a kind of operation level thinktank, nothing more.\n\nODF needs to be promoted on a higher level. \n\nThe time is right. Some member states such as Spain are good ODF promoters. \n\nThe real task for IDABC will be whether they can defend their European Interoperability Framework against lobby attempts to revise it. At an ealier stage upon requerst, the DG Enterprise guys shot IDABC in the back and declared EIF semiofficial which is the case. But that no steps were taken to officialise it and even mention it in high level documents is really alarming."
    author: "Gurt"
---
Yahoo Business <a href="http://biz.yahoo.com/prnews/061002/sfm056.html">reports on ODF Day at Akademy</a> which brought together developers from KOffice, OpenOffice, IBM, Intel and government.  "<em>Dr. Barbara Held, presently serving as Enterprise and Industry Directorate-General of the European Commission Program for Interchange of Data between Administrations (IDA), stated in her keynote address, 'In the view of the European administrations and Member States, the ODF standard is at the very top of the pile by far from all other proposed open standards.'"</em>

<!--break-->
