---
title: "KDE Joins ODF Alliance"
date:    2006-05-19
authors:
  - "sk\u00fcgler"
slug:    kde-joins-odf-alliance
comments:
  - subject: "odf differences among kword, oowriter, abiword..."
    date: 2006-05-19
    body: "A one page document saved with oowriter, becomes 2 page in kword:\n\nWhen exchanging documents, data loss is obviously not acceptable. We consider losing the layout as a form of data loss as well, and therefore the loss of layout is a serious matter\n\nhttp://www.freesoftwaremagazine.com/articles/odf_programs?page=0,1\n\nhttp://www.freesoftwaremagazine.com/files/nodes/1243/1243.pdf\n\n"
    author: "fast_rizwaan"
  - subject: "Re: odf differences among kword, oowriter, abiword"
    date: 2006-05-19
    body: "Some of the reasons which makes document exchange a layout nightmare are:\n\n1. Page size (A4 or letter)\n2. Font sizes (kword uses X's dpi, whereas oowriter uses 96dpi fixed!)\n3. Margins (kword uses smaller margins)\n\n"
    author: "fast_rizwaan"
  - subject: "Re: odf differences among kword, oowriter, abiword"
    date: 2006-05-20
    body: "I absolutely do not doubt that document exchange between apps is hard, even with a common coument format, but I think your points can be addressed:\n\n1. Surely page size is encoded in the document?\n2. This I think is perhaps the hardest of your points to solve. Qt3 suffered in this area, but looking at some of the bugs that have been fixed recently, stating \"fixed with Qt4\" I look forward to seeing improvements here in future.\n3. Same as point 1 - margins must be encoded in the document, so you'd think it was just a case of paying attention to these values in the code...\n\nBest wishes,\n\nDaveC"
    author: "DaveC"
  - subject: "Re: odf differences among kword, oowriter, abiword"
    date: 2006-05-20
    body: "DPI size makes absolutely no sense in this context. If we're talking about a document that is going to be printed, then DPI is what you *printer* uses, not what your screen uses (or what your program thinks it should use).\n\nIt's actually very simple: if you had a 2\" high page and you had a 72pt letter \"A\" written in the document, then that A has to occupy exactly 50% (one inch) of the page's height. So the word processor program has to appropriately scale the *page* rendering on the screen to match the font sizes.\n\nIn that example, if oowriter thinks it should render a 72pt font in 96 dpi, it'll come out as a 96px-high letter \"A\". The page should therefore be 192px high. If KWord on the other hand knows the setting to be (for example) 102dpi, then it should draw a 102px-high letter \"A\" in a 204px-high page.\n\nIt doesn't matter that they don't have the same on-screen size. It matters that they have the correct relative sizes to the page and that they're printed correctly."
    author: "Thiago Macieira"
  - subject: "Re: odf differences among kword, oowriter, abiword"
    date: 2006-05-20
    body: "I think between KOffice 1.5 and OpenOffice 2.0 there still will be a lot of small differences, but the idea that it is possible, is already half the way. I hope and expect such small issues will get better and better in next versions of the various programs. I read there were still problems with the way formules worked in KOffice in comparison with OpenOffice, those problems also need to get fixed in later versions of the ODF standard, but at least it will still be possible in 10 years from now, to read your old stuff. If it doesn't look totally the same, I can live with it, with what I can't live is not being able to look at the document at all anymore...\n\nThe only think I hope is, that microsoft won't support ODF, and that the plugin will come directly from the ODF people. Which ensures us that Microsoft won't make some small changes to the way they interprete the ODF standard."
    author: "boemer"
  - subject: "Re: odf differences among kword, oowriter, abiword"
    date: 2006-05-21
    body: "Thiago, I totally agree with everything you say.\n\nThe problem with DPI that I saw in the past was that lines were wrapping in different places when I viewed the same kword document using different resolutions. When I printed the document, the print out matched what I saw on the screen, but clearly a document should produce the same printed output irrespective of screen resolution.\n\nI believe the cause of this was QTs handling of font metrics. I understand QT4 offers improvents in this area. The bug I raised about this has now been marked fixed, so I look forward to testing this in future."
    author: "DaveC"
  - subject: "Re: odf differences among kword, oowriter, abiword"
    date: 2006-05-20
    body: "I don't understand your point about point 2: why should the DPI matter?\n\nSay 7mm is the size of the font (I hate the point unit: its name is ambiguous as a point can be mistaken as a pixel: why not use simply mm?), the difference of DPI between two screens just mean that by default the quality of the font is different but the font size and the text flow should be fixed.\n\nEven in the case that the DPI used by the application doesn't match the screen real DPI (a bad idea IMHO), it just means that the zoom is different, the text flow should be the same as the font size / page size ratio is identical."
    author: "renox"
  - subject: "Re: odf differences among kword, oowriter, abiword"
    date: 2006-05-21
    body: "The problem I saw was screen DPI affecting the text flow. Like I said above, this is now apparently fixed, so I will have to test this again."
    author: "DaveC"
  - subject: "Re: odf differences among kword, oowriter, abiword"
    date: 2006-05-21
    body: "to look at another word processor: try opening a ms word file on 2 different computers running different versions of Word. With complex documents, lots of times the formatting of the word document gets messed up.\n\nSame goes for openoffice documents shared between different platforms.\n\nas I see it, it's just a common problem with wordprocessors. If you want true interoperability between platforms etc. try TeX ;)"
    author: "AC"
  - subject: "Also true of different versions of MS Office"
    date: 2006-05-27
    body: "That certainly can happen, sure.  You're right.\n\nAre you aware that it also happens between versions of MS Office?  I've seen plenty of documents that are one page in, say, MS Word 2000, that become two-page documents in MS Word 97 and MS Word XP.  I've seen the same thing happen when MS Word XP is used to make the document, and you then open it in Word 2000 or Word 97.  I've even seen table formatting messed up a bit and need some correction.  All three of these MS Word versions use the same file format.\n\nOver the last four years using it, I've found that OpenOffice.org is often more \"compatible\" with respect to preserving layout of MS Word documents than even MS Word itself.  I work in a big-time Microsoft shop, so I've had lots of opportunities to see this in action.\n\nThis is, by the way, why PDF was invented.  If you need to preserve layout exactly, then you export to PDF (both OpenOffice.org and AbiWord do this directly).  Alternately, for the slightly geeky folks, you also could export, with pretty much any editor or word processor on virtually any UNIX-like system, straight to PostScript (kprinter is WONDERFUL for this) and then just run ps2pdf on it.  Boom, layout preserved."
    author: "Terrell Prude' Jr."
  - subject: "plugin"
    date: 2006-05-20
    body: "The plugin is actually a Openoffice server product."
    author: "hup"
---
The position of the <a href="http://en.wikipedia.org/wiki/Opendocument">OpenDocument Format (ODF)</a> was today strengthened by the <a href="http://www.kde.org">K Desktop Environment</a> (KDE) joining the <a href="http://www.odfalliance.org/">ODF Alliance</a>. KDE joins other partners such as Oracle, SUN Microsystems, Mandriva, IBM and Junta de Andalucia in promoting the OpenDocument Format as a market leader in document exchange and storage.








<!--break-->
<p>
The OpenDocument Format for office applications improves the transparency and compatibility of critical records by ensuring that competition is fostered in the market place by promoting the use of a vendor- and OS-neutral document format to store files.</p>

<p><i>"We believe that the use of OpenDocument, backed by important industry leaders, will prevent content from being lost in propritary formats and will thus bring more freedom to users."</i>, said Eva Brucherseifer, president of the KDE e.V.</p>
<p>
By standardizing workflows around ODF, organisations can open themselves up to more competitive office application offerings, in particular by taking advantage of cost efficient Linux-based free software desktop environments.
</p><p>

<i>"The fact that KOffice provides an independent implementation of the OpenDocument file format, and was able to take part in its specification, proves that OpenDocument is actually a standard, not just a rubber stamp on Sun's OpenOffice file format. What makes an open standard is not merely approval by a committee, but independent implementations."</i>, 
says David Faure, KOffice developer and member of the OASIS technical committee for OpenDocument.</p>


<p>Faure, who has been instrumental in developing the OpenDocument standard has also been working on the implementation of ODF in <a href="http://www.koffice.org">KOffice</a>, KDE's office suite. 
Starting with <a href="http://www.koffice.org/releases/1.5-release.php">version 1.5</a>, KOffice uses ODF as its native file format.</p>

<p>ODF is a royalty free, open standard for storing office documents such as spreadsheets and text processing data on all major desktop operating including Microsoft Windows, MacOS and Linux. Just as the Web provided a way for information to be published, viewed and archived globally, the OpenDocument Format is doing the same for office documents. 
</p><p>







