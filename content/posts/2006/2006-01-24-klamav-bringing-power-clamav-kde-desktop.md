---
title: "KlamAV: Bringing The Power of ClamAV To The KDE Desktop"
date:    2006-01-24
authors:
  - "jriddell"
slug:    klamav-bringing-power-clamav-kde-desktop
comments:
  - subject: "Free AV antivirus"
    date: 2006-01-24
    body: "http://www.free-av.com/antivirus/allinonen.html\n\nhttp://free-av.com/personal/en/unix/antivir-workstation-pers.tar.gz\n\n\n \nLinux / FreeBSD / Solaris - AntiVir PersonalEdition Classic\n \nThe private, non-commercial use of AntiVir Workstation for Linux / FreeBSD / Solaris is free\n\n A registration of the AntiVir PersonalEdition Calssic Workstation for Linux is not necessary any more.\n The licence file is now included in the download tar file.\n\n NEW!\n The scope of services of the cost-free AntiVir PersonalEdition Classic for Linux - FreeBSD/Solaris was adapted to the cost-free AntiVir PersonalEdition Classic Windows by the recent release change: \n \n new graphic user interface (GUI) \nno registration is necessary \nthe functions Mailgate and Milter can be ordered as amenities by the H+BEDV Datentechnik GmbH \n\n\n \nName       Program-Release       Date       File size\n  Workstation  2.1.5   12-06-2005   17,968 MB\n\nI find free av quite good to get rid of windows viruses from other partitions."
    author: "fast_rizwaan"
  - subject: "Re: Free AV antivirus"
    date: 2006-01-24
    body: "I'm sure it's an alright product but this is about clamav and klamav. Besides I can use clam/klam in any fashion I want, ie, personal, business, one user or 100 users and it's still free. Plus I don't have to worry about registering under any circumstances. And the bonus is GPL."
    author: "klaxon"
  - subject: "Re: Free AV antivirus"
    date: 2006-01-24
    body: "GPL is a bonus!?\n\nOMGWTFBBQ!"
    author: "simmel"
  - subject: "Re: Free AV antivirus"
    date: 2006-01-24
    body: "GPL is a huge bonus."
    author: "Yes"
  - subject: "Re: Free AV antivirus"
    date: 2006-01-24
    body: "It might not cost anything but it aint Free!"
    author: "Vlad Blanton"
  - subject: "Re: Free AV antivirus"
    date: 2006-01-25
    body: "Trolls even here... \n\nSome idiots to try to sabotage anti-virus tools. Which is almost exclusively a window problem. "
    author: "Steve"
  - subject: "Re: Free AV antivirus"
    date: 2006-01-25
    body: "Who is a troll, how is he sabotaging anti-virus tools and what has the last sentence to do with anything?\n\nHuh?"
    author: "T. Roll"
  - subject: "Re: Free AV antivirus"
    date: 2007-06-21
    body: "It is indeed free, but on a differant note, its not that good I used it a few years back and I got infected with a few viruses while using it as there Anti-Virus updates do not include some of the more newer viruses. Perhaps it does offer better protection if you pay for it but I'm afraid I don't know on that score.\n\nYour far better off with something like Clam if you have Linux or an Apple Mac and don't want to pay through the nose for anti-virus software.\n\nI have never used and never will use Nortons or McAfee simply because I don't agree with there making you pay for an update every year or so to stay protected.\n\nI have also heard the number one voted best anti-virus of 2006-2007 is NOD 32 from http://www.eset.co.uk"
    author: "enigma"
  - subject: "Re: Free AV antivirus"
    date: 2006-01-25
    body: "Since when do we tolerate such blatant advertising of competing products on Dot? Non-\"libre\" even?!?\n\nSee www.gnu.org for definition of free software"
    author: "another nickname"
  - subject: "klik://klamav"
    date: 2006-01-24
    body: "klik://klamav ...\n\n ....is the fastest way to give this nifty little program a spin.... (and this klik package recipe is one of the most popular ones in our repository).\n\n(The klamav klik recipe includes clamav-0.88; it uses Mandrake-RPMs fetched from ftp://ftp.uni-bayreuth.de/pub/linux/Mandrakelinux/devel/cooker/i586/media/main/ and converts them \"on the fly\", on the klik client side, to form a platform independent single *.cmg file from the ingredient RPMS. It is known to work well on SUSE-9.3, SUSE-10.0, $Debian, $Knoppix, Kanotix and $Ubuntu. If you are new to klik, see  \n\n --> http://klik.atekon.de/wiki/index.php/User%27s_FAQ <--\n\nfor an initial kickoff about how to install the 20 kByte klik client)"
    author: "Kurt Pfeifle"
  - subject: "Is linux AV really useful for protecting linux?"
    date: 2006-01-24
    body: "This is rather funny timing, since just a couple days ago I installed KlamAV to scan my windows partition!  KlamAV is pretty nice, but I honestly don't think antivirus is very important (I don't even have one in windows, and uninstalled McAfee from my laptop the day I got it even before I installed Linux over it).\n\nI'm kinda wondering, has anyone ever found a virus on linux with ClamAV (or any other linux antivirus)?  Downloading some viruses just for it to find doesn't count!  I wanna know if anyone ever found a virus on their linux machine with clamav that they didn't already know about (windows partitions don't count)?"
    author: "Corbin"
  - subject: "Re: Is linux AV really useful for protecting linux?"
    date: 2006-01-24
    body: "I did found a virus on my box 1 year ago..but it was a stupid virus, i removed it manually, but i did use antivirus to see what kind of virus it was...\nI forgot his name, and i got infected cuz of stupid habits..i used the root account a lot, and kept running all binary files i found...forgot what i was looking for. Now i don't use the root that much :)\n\nI still have BitDefender for linux installed on my box for scanning windows partitions from time to time."
    author: "Quamis"
  - subject: "Re: Is linux AV really useful for protecting linux?"
    date: 2006-01-24
    body: ">I wanna know if anyone ever found a virus on their linux machine\n\nI have found several, in my in-box. They were much more numerous before ISPs and mail providers started to spam/virus filter. And my personal spam filter catches most of them now. As a rule I try not to forward them to my windows using contacts. \n\nBut I did not find them by using any antivirus software, although they probably would have found them had I bothered to run any. Using a combination of common sense and comparing suspicious mails to the recent warnings of the latest 'popular' virus is often more than adequate. \n\nMails containing files like some_famous_female_nude.mpeg.exe are usually a dead giveaway:-) Other 'funny' files are archives(which fail in Ark) and other non executables containing stuff like \"This program must be run under Win32\" or \"This program cannot be run in DOS mode\". Easily seen near the start of the file if opened in KWrite/Kate."
    author: "Morty"
  - subject: "Re: Is linux AV really useful for protecting linux?"
    date: 2006-01-25
    body: "AFAIK scanning for virusses on a Linux computer is only neccesary if you use linux in a windows-network and want to avoid infecting the Windows machines by accident, of using the linux-box as SMB-server and want to make sure that the files that the Windows clients put on the box don't contain any virusses.\n\nI recieve a lot of virusses, and don't really care about them :)\nWhen I was using a dual boot with WindowsME, I got in trouble because of this.\nI saved some files on a FAT-partition, one containing a virus..\nWhen booting in Windows, I clicked on the file to open it and got (of course :o) infected.\n\nSo, if you are communicating a lot with Windows computers, it may be a good idea to make sure that you don't pass virusses by accident to the Windows-users.\nBut on the other hand, running Windows without anti virus protection is not a good idea, so any virus you might pass will be detected anyway ;)\n\n\n"
    author: "ac"
  - subject: "Re: Is linux AV really useful for protecting linux?"
    date: 2006-01-25
    body: "I just use clamav with kmail. I like to see how many are tagged as phishing scams. So far over the past 4 months I get at least 3 a week.... all to date has been paypal."
    author: "klaxon"
  - subject: "Re: Is linux AV really useful for protecting linux?"
    date: 2006-01-25
    body: "Well, you can recognise those mails without a virus scanner as wel :)\nI get scam mails from eBay, several English banks, PayPal...."
    author: "ac"
  - subject: "Re: Is linux AV really useful for protecting linux?"
    date: 2006-01-26
    body: "I don't have any active scanning AV software in windows because I only ever boot into windows to play City of Villains (doesn't work in Wine yet).  Though I do run an application level firewall so if I was infected by most of the viruses that are spread around these days I would notice 'ijsfoe98444.jpg.exe' trying to access the internet (or open), since I don't even let most programs that come with windows access the internet (like windows media player, IE, and the smb stuff in windows)."
    author: "Corbin"
  - subject: "Re: Is linux AV really useful for protecting linux"
    date: 2006-01-31
    body: "Hi guys!\n\nI'm planning to setup a Samba file server at work as a replacement for the current w2k server (we use windows only clients). I already have an option in using Sophos  to keep the SMB shares clean, but after reading the previous posts, im not sure \nwhat i should use.\nI have a choice of Sophos, ClamAV or BitDefender. Currently i have Sophos for windows installed on each of the workstations, and it has proven to be very stable and picks up everything i throw at it (i test it with random viruses once a week).\n\nMy question is, what do you guys use to protect your files on a SMB share? Which one is the best to use (meaning which one has the highest detection rate)?"
    author: "tika"
  - subject: "wine & virii"
    date: 2006-01-25
    body: "Do you know which risks one might be curring by associating .exe files with wine and accidentally clicking on an infected one? Are others Windows .exe files infected? If the virus stays in memory will its execution be interrupted by closing wine? etc."
    author: "Mario"
  - subject: "Re: wine & virii"
    date: 2006-01-25
    body: "Some people tried to run several virusses with wine.\nReports about that can be found on the internet.\nAll of them concluded that you can run some virusses with wine, but because wine is not Windows, all virusses could not do any harm. They expected an environment that is not present onder Linux, nor in Wine...\n\n"
    author: "ac"
  - subject: "Re: wine & virii"
    date: 2006-01-25
    body: "Oh damn... That's exactly what happened to me. I was once trying to open an attachment with wine. It came as a self-unpackaging exe file. I got the file inside, but did not notice that the senders zip-tool (whatever) had put some bonus-malware to it. After the unpacking window was gone, the virus stayed in another process. You could easily see that wine was still running using ps... but if you don't suspect it...\n\nAfter finishing work several hours later I accidentally stumbled across a file in my home-dir named \"Britney ... whatever.mpeg.exe\" ... WTF?\nThis was at work in the office! I was getting vewy nervous as I had already forgotten the wine-attachment. Logging in on a different machine using nmap I found my machine infected with several open ports...  O.k. unplug the network cable and try to find that rootkit... As ps is useless here I did not even try to use it ;-)\n\nSome hours later I gave up and did a simple reboot... and it was gone... Only than I realized that it was no rootkit and the windows-exe came back to my mind and I quickly discovered that it is indeed possible to get infected with windows-virri using wine ...only if you are stupid enough...\n\n"
    author: "Thomas"
  - subject: "Re: wine & virii"
    date: 2006-01-25
    body: "serveral open ports?\nyou don't use a firewall??"
    author: "ac"
  - subject: "Re: wine & virii"
    date: 2006-01-26
    body: "Unless you use an application level firewall or have IPTables defaulting to dropping all outgoing connections a firewall won't help you, and on a desktop machine defaulting to dropping all outgoing connections will just waste your time."
    author: "Corbin"
  - subject: "Which AV to run?"
    date: 2006-01-31
    body: "Hi guys!\n\nI'm planning to setup a Samba file server at work as a replacement for the current w2k server (we use windows only clients). I already have an option in using Sophos  to keep the SMB shares clean, but after reading the previous posts, im not sure \nwhat i should use.\nI have a choice of Sophos, ClamAV or BitDefender. Currently i have Sophos for windows installed on each of the workstations, and it has proven to be very stable and picks up everything i throw at it (i test it with random viruses once a week).\n\nMy question is, what do you guys use to protect your files on a SMB share? Which one is the best to use (meaning which one has the highest detection rate in a Linux enviroment)?"
    author: "tika"
---
Linux and BSD forum <a href="http://linuxgangster.org">Linux Gangster</a> has published a guide to <a href="http://linuxgangster.org/modules.php?name=Content&amp;file=viewarticle&amp;id=18">Bringing The Power of ClamAV To The KDE Desktop</a> explaining how you can use <a href="http://klamav.sourceforge.net/klamavwiki/index.php/Main_Page">KlamAV</a> to keep viruses off your system.  "<em>While it's true that very few of the viruses out there can do much damage to a Linux system... you certainly don't want to run the risk of passing them on to your Windows-using friends and family.</em>"




<!--break-->
