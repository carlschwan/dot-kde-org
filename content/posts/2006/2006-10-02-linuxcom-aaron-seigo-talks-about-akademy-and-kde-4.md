---
title: "Linux.com: Aaron Seigo talks about aKademy and KDE 4"
date:    2006-10-02
authors:
  - "jriddell"
slug:    linuxcom-aaron-seigo-talks-about-akademy-and-kde-4
comments:
  - subject: "KDE User Conference"
    date: 2006-10-02
    body: "Does it make sense to split Technical conference and user conference? "
    author: "Gurt"
  - subject: "Re: KDE User Conference"
    date: 2006-10-03
    body: "Yes, it does."
    author: "Adriaan de Groot"
  - subject: "Re: KDE User Conference"
    date: 2006-10-07
    body: "> Does it make sense to split Technical conference and user conference?\n\nI'm not sure that is the right question, certainly it is very useful to have a very techincal developer focussed conference to get certian things done and I think Akademy was successful in that regard.  User conferences are different and useful in their own way but the work can more easily be distributed and similar effects can be achieved by many smaller local meetings and getting KDE people out to booths at existing conferences.  \n\nAs with all things in life balance is needed.  "
    author: "Alan Horkan"
  - subject: "If Aaron *really* loved KDE. . ."
    date: 2006-10-03
    body: "he would replace that 'g' in his name with a 'k'!  How about it, Aaron?  As an added bonus, you could use \"Psycho\" as your nickname."
    author: "Louis"
  - subject: "Re: If Aaron *really* loved KDE. . ."
    date: 2006-10-03
    body: "Or in this case \"Psyko\"?"
    author: "decept"
  - subject: "Same old same"
    date: 2006-10-03
    body: "Phonon, Solid sound like good things, not particularly revolutionary though. But what about Plasma? I've read everything I could find about it and I still have no idea what it's supposed to change.\n"
    author: "Reply"
  - subject: "Re: Same old same"
    date: 2006-10-03
    body: "On the contrary - with enough application support, these two (Solid in particular) have the potential to really improve the desktop experience - things will \"just work\", and better than they do on other operating systems."
    author: "Paul Eggleton"
  - subject: "Re: Same old same"
    date: 2006-10-03
    body: "Not to sound too naive here, but at some level don't hardware vendors still have to produce drivers?  I can see the potential from the application perspective, but for things to just work (and better than they do on other operating systems) I would think up to date hardware drivers will still be needed.  Am I wrong here?\n\nEric....."
    author: "cirehawk"
  - subject: "Re: Same old same"
    date: 2006-10-03
    body: "nope, drivers will be as important as they are now. solid will make it easier for application developers to get information about hardware. this might sound not-so-important, but you can imagine the difference between 'with 5 wel documented lines of code you can know this and that' and 'let's work through a pile of (often badly documented) code to find out how to know this and that, and do so for every platform (and sometimes several times for several kernel versions within a platform) kde supports'.\n\nsame with phonon. if you want to play a sound in an application now, you can use arts - but users will complain if they use esound or gstreamer or xine. so you'll have to build a plugin structure which allows the user to choose between these sound engines. so you have to know all these engines (sometimes badly documented). and you have to update your application every time a new version of each of these engines is released.\nor just forget about sound...\nwith phonon, there are a few well-documented lines - and tadaaa, sound.\n\nand there will be strigi or something like that for indexing/searching, kross for scripting, decibel for comunication, etcetera.\n\nnot very innovative? no, i agree. but it'll lead to way better applications, as developers can easily add these features, and they can spend more time on actually making their apps better. and KDE will have the most elaborate framework compared to other DE's and OS'es (it already has, actually, it'll just get another, huge, boost)."
    author: "superstoned"
  - subject: "Re: Same old same"
    date: 2006-10-03
    body: "\"not very innovative? no, i agree.\"\n\nOn the contrary. I think Solid is the most important thing in KDE 4. It means you'll actually have graphical applications that will reflect properly what hardware you actually have on your system."
    author: "Segedunum"
  - subject: "Re: Same old same"
    date: 2006-10-03
    body: "Exactly.  Number one complaint from a friend of mine about linux:  \"Why can't I see what hardware I have, and what it can't find drivers for?\"  \n\nIt's really tough in Linux to simply find out what the hell the kernel is even doing with hardware.  So if a newbie realizes that the sound doesn't work, they have no idea why.  With solid, I hope they would be able to see nicely what's going on with the hardware.  Is no module loaded at all?  Why?  Is a module loaded, and which one?  Then then have a chance of troubleshooting this sort of stuff."
    author: "Leo S"
  - subject: "Re: Same old same"
    date: 2006-10-03
    body: "\"with phonon, there are a few well-documented lines - and tadaaa, sound\"\n\nYes, but lying beneath the few well-documented lines are the plug-in sound engines.  So, Phonon would be using Xine or GStreamer or whatever underneath.  If that is the case and there is a bug in (Xine|GStreamter|*), then the bug would still rear its ugly head for the user.  The developers, in turn, would need to turn to (Xine|GStreamter|*) to find the bug.  So, while I agree that Phonon is nice to show one unified sound API to KDE developers no matter what sound engine lies underneath, you are still faced with the plug-in problem you noted earlier in your post.\n\nMind you, I'm not bashing Phonon.  Multimedia is simply a difficult problem to solve, given the hardware drivers problems and multiple software engines problems.\n\n"
    author: "coulamac"
  - subject: "Re: Same old same"
    date: 2006-10-03
    body: "The advantage of having Phonon is when the plugin-architecture needs to be changed somehow because of a change in the backends, it doesn't have to be done at least 3 times (Amarok, Kaffeine, KMPlayer, Juk?, probably more all already implement something quite similar to Phonon, but only for themselves)."
    author: "Corbin"
  - subject: "Re: Same old same"
    date: 2006-10-03
    body: "Thanks for the feedback.  Mind you, I'm not a programmer at all so I was in no way saying that these new technologies aren't innovative.  I was trying to clarify my understanding of the benefit of the new technologies.  So this is how I see it.  We still need to push hardware vendors to provide high quality linux drivers for their products.  Solid and Phonon allow the app developers to easily add support for that hardware into their given app, freeing them to concentrate on improving the app.  Allowing app developers to concentrate on innovating is in it's own right innovative.  Sounds good to me.\n\nEric....."
    author: "cirehawk"
  - subject: "Re: Same old same"
    date: 2006-10-03
    body: "Also, the poster was mainly commenting on the lack of discussion regarding Plasma.  I remember when Plasma was first announced, Aaron said he would not reveal much till the very end so others (windows?) wouldn't be able to steal ideas.  I'm hoping this is the reason we're not hearing much at this point.\n\nEric....."
    author: "cirehawk"
  - subject: "Re: Same old same"
    date: 2006-10-03
    body: "For the Plasma scoop, visit this http://wiki.kde.org/tiki-index.php?page=Plasma%3A+What+the+Bleep+Is+It%3F+by+Aaron+J.+Seigo and watch the video for a good measure.\n\nMy Summary:\nQ: Why is it that I don't see the screenshots?\nA: The infrastructure is not ready yet.\n\nQ: What is that Plasma \"infrastructure\" you are talking about?\nA: Simple answer: Imagine SuperKaramba-like eye-kandy and scriptability on accelerated system-wide \"canvas\"\nLonger Answer: Aaron got tired of implementing everyone's visual desires in Kicker IN C++. He saw that pretty much everybody can do a good graphical \"skinning\" job using higher level languages and, his resolution was: backend (rendering, data) will be in C++, you ppl can roll your own UI.\nSince the backend is nowhere near completion, the work on UI is far from began.\nSoo... No Plasma screenies for you."
    author: "Suslik"
  - subject: "Re: Same old same"
    date: 2006-10-03
    body: "I think people want to hear more about use cases than technical blabing. For instance, will Plasma allow me to make icon-application-scripts easily like to be able to drag files there and then when I open with Konqueror they are converted to another format (like OpenDocument to MS Word, which I need a lot)? Will the panel it self be such an application-script? If yes, does this mean you could make application-scripts that have container areas? Would be could if it would allow to do application-scripts that resembled those side-panels of the old MacOS..."
    author: "blacksheep"
  - subject: "Re: Same old same"
    date: 2006-10-04
    body: "That's why I still call Plasma vaporware: they still don't have a plan! You now, just some sketches of what will Plasma allow you to do, what effects will be added, how the desktop will look.\nRight now, Plasma is only a concept, a nice one, but that's it! \n\nI belive KDE4 will be out without Plasma as it was promissed - a new way for doing desktop - but rather will keep the way KDE3 is and then add Plasma later, the day it's completed (I belive it's not soon).\n\nBut yeah, I would love KDE developers show me I'm 100% wrong :)\n"
    author: "Iuri Fiedoruk"
  - subject: "Re: Same old same"
    date: 2006-10-04
    body: "<i>I belive KDE4 will be out without Plasma as it was promissed - a new way for doing desktop - but rather will keep the way KDE3 is and then add Plasma later, the day it's completed.</i>\n\nYou are \"somewhat\" mistaken. The pieces for the future Plasma parts are already in place. The styleclock (http://kde-apps.org/content/show.php?content=14423) author has already agreed to port it to Plasma. Since it appears the 1st language that will make it into Plasma will be JavaScript, consider that a no-brainer.\nIf python will make it into the backend, the crap-load of SuperKaramba applets will almost instantly show up too.\n\nSo, monitoring, time and calendar Plasma applets should be in KDE 4 almost from the start. Other things, like presence, collaboration and contact applets will probably lag, as the data backends for them will be very new in KDE 4."
    author: "Daniel \"Suslik\" D."
  - subject: "Re: Same old same"
    date: 2006-10-04
    body: "I think that the doubting is about \"Plasma as an infrastructure for different workflows on the desktop\", not about \"Plasma as a framework for skinnable super-applets on steroids\". In other words, there seems to be a lack of discussion about behaviour and workflows.\nI very much hope it's not one of those cases where ppl become so enamoured with the technology and the glitz that they gliss about the substance. I think we've all seen too many games and movies revolving around that new-fangled CG effect, and having plot or gameplay as an afterthought. Sadly, this glitzy fascination seem to be polluting the GUI design lately (see Vista's alt+tab as opposed to OS X Expose for an example of useless vs useful sophistication).\nOr maybe I misunderstood the meaning of the Plasma namespace, and its real goal is only to offer the tools, not to redefine the experience."
    author: "Kitty"
  - subject: "Re: Same old same"
    date: 2006-10-04
    body: "Yep, that's it.\nIf the plan was to make Plasma as a new implementation of super-karamba, ok, fine.\nBut what was \"promissed\" was a complete new desktop paradigma with a lot of nice effetcs. I still don't see this coming :)"
    author: "Iuri Fiedoruk"
  - subject: "New widget style?"
    date: 2006-10-04
    body: "Is there any progress for the new default widget style? Few weeks ago I saw about cuckoo (cookoon cookoon cookun or whatever, I can't remember). But I've never heard about it anymore, any update on this (or it was just me who had too deep imagination about KDE4)? IMHO new widget style is good to compete with Windows Vista and GNOME. I'm sure that KDE 4 will rock!!!"
    author: "fred"
  - subject: "Re: New widget style?"
    date: 2006-10-04
    body: "I hope it will look and behave similar to the Polyester widget style which is really great. But striped menus and colored scrollbars should be disabled by default (perhaps less style options at all)."
    author: "Henning"
  - subject: "Re: New widget style?"
    date: 2006-10-04
    body: "Amen to Polyester-isation, brother!\n\nIt offers by far the most comprehensive array of visual choices. Anything, from centered tabs, to exquisite tuning of \"active\" button appearance. It seems the author went around and cut the best parts out of other major contenders. A bang job!"
    author: "Daniel \"Suslik\" D."
  - subject: "Usability/features: could be useful site: XvsXP(?)"
    date: 2006-10-09
    body: "With regards to usability in KDE, it might be useful\nto take a good long read in the articles on this website:\nhttp://www.xvsxp.com/ ; it is a comparison between the \nOperating Systems WindowsXP SP2 and Mac OS X Tiger (10.4.7).\n\nOne thing that already caught my attention, is that, when\nyou have several iconsets installed in KDE, and then switch\nto a different iconset, some icons are 'old' (from the previous\niconset). After, if you want to customize some icons, it's hard\nto find which icons belong to which iconset.\n\nSo I propose that there's some kind of possibilty to sort/select\nicons by iconset when you're in the 'icon picker' (big list of icons).\n\nReading this thing is good, if only because it makes you think about\nthings you might have taken for granted..."
    author: "Darkelve"
---
With aKademy now at a close <a href="http://www.linux.com/article.pl?sid=06/09/28/1534209">Aaron Seigo spoke to Linux.com</a> for a podcast.  "<em>aKademy's conference track, which ran Saturday and Sunday, was different from those of past years, Seigo says. In the past, Seigo says aKademy has had a user-focused conference, but 'this year, we've decided to focus acutely on the technical side of life.' Most of the talks have focused on KDE 4, and speakers have been able to not only discuss their new applications and technologies, but also to demonstrate them.</em>"
<!--break-->
