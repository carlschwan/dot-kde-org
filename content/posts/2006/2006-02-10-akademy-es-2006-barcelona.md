---
title: "aKademy-es 2006 in Barcelona"
date:    2006-02-10
authors:
  - "alarrosa"
slug:    akademy-es-2006-barcelona
comments:
  - subject: "\u00bfIn which languaje will be?"
    date: 2006-02-09
    body: "English? Spanish?"
    author: "An\u00f3nimo"
  - subject: "Re: \u00bfIn which languaje will be?"
    date: 2006-02-09
    body: "I'm going to guess Spanish, judging from the website, and that the meeting is targeted specifically to spanish devs.\n"
    author: "LMCBoy"
  - subject: "Re: \u00bfIn which languaje will be?"
    date: 2006-02-10
    body: "That's right, spanish will be the main language of the event, but other languages will be also welcomed since it's an event for the spanish community, and there are several different languages spoken there. In fact, we expect that translator groups for catalan, basque, galicean, etc. will come too."
    author: "Antonio Larrosa"
  - subject: "Re: \u00bfIn which languaje will be?"
    date: 2006-02-10
    body: "Oh my ****!!\n\nAll these political nonsense we live in Spain coming to geek circles!!!????\n\nAbsurd!\n\nThis about getting the message to the target audience, the broader the better.\nTranslating to any of those languages you cite above won't bring one single user/developer to the KDE camp, since anyone who speaks any of those languages will salso speak Spanish, whether (s)he feels (s)he Spanish, Basque, Catalonian, Galician or whatever.\n\nCome on, I thought we were far away from this mess we currently have to live with in Spain.\n\nBy the way, I was born in Barcelona, I'm living in Galicia and it's not that I am apolicital, anti nationalist; far from that. I like those languages and firmly believe they deserve protection, as I also see as a need the translation of the desktop to all those languages\n\nYet let's not mix things up.\nAm I am wrong or aKademy is mainly focused on developers?\nWhat we need is improving our communication, not adding unnesessary bumps on our road.\nWith this aim, using as few languages as possible (the minimal common denominator) is the way to go.\n\nIn fact, now that I think of it (I haven't yet checked the akademy 2006 site), not having as much presence of English as of Spanish will probably keep away a considerable number of prospective attendants. I hope the Barcelona 2006 home page is as browseable in English as it is in Spanish."
    author: "Javier Marcet"
  - subject: "Re: \u00bfIn which languaje will be?"
    date: 2006-02-10
    body: "Well, it is obvious that you are a troll and i should not feed you, but i could not resist.\n\nPoint 1: This is not Akademy 2006 just Akademy-es 2006, a meeting of all people living in Spain that are related to KDE in any way, that is why there is NO content in English.\n\nPoint 2: Why the fuck do we learn Spanish if learning English or Chinese would be much more wise in number of speakers? Because language is not just a comunication tool, there's much more behind it.\n\nPoint 3: Hope you don't come to Akademy-es 2006 meeting as we don't need any troll.\n\nThanks for listening\n\nGood Night and Good Luck"
    author: "Albert Astals Cid"
  - subject: "Re: \u00bfIn which languaje will be?"
    date: 2006-02-10
    body: "yeah, i tought he was a bit harsh, too... its great ppl translate KDE to any language they want - just great. let them. i rather see it as a positive thing... cooperation is good!"
    author: "superstoned"
  - subject: "Re: \u00bfIn which languaje will be?"
    date: 2006-02-11
    body: "You are right I was a bit harsh. That was unnecessary from my side.\n\nBut please re-read what I initially posted.\nI stated that translation of KDE to all those languages is of great importance.\nI am _not_ against their use, not at all.\nI use some of them myself and understand perfectly the different sentiments a person might have towards his/her mother tongue and whatever languages (s)he later learnt.\nIn fact I love languages, natural and artificial ones.\n\nWe already have enough politics with all the different licenses, to put just one simple example.\nDo we really need to bring another source of conflict? Lose more time with unnecessary complexity?"
    author: "Javier Marcet"
  - subject: "Re: \u00bfIn which languaje will be?"
    date: 2006-02-11
    body: "well, i'm not sure how sensitive it is to you spanish ppl (we dutch aren't used to care much about stuff like this), but i didn't read anything to worry about in the comment from Antonio Larrosa, not even after re-reading it.\n\n\"That's right, spanish will be the main language of the event, but other languages will be also welcomed since it's an event for the spanish community, and there are several different languages spoken there.\"\n\nhow can that be offensive? it's not even an opinion, in any way... how is it political? he says:\n- it is a spanish (eg in spain) event\n- main language is spanish (i can understand that - despite having several other languages (like, btw in the netherlands we have the frisian minority language), most if not all speak spanish - so the easiest way to communicate is doing it in spanish (english would be an option if many foreign ppl show up, but i guess thats not to be expected from a local meeting).\n- and all other ppl are welcome, no matter what language they speak, actually, several languages will be used anyway!\n\nnow, tell me, how even an EXTREMELY sensitive person can be offended by this?"
    author: "superstoned"
  - subject: "Re: \u00bfIn which languaje will be?"
    date: 2006-02-11
    body: "What makes him a troll?\n\nThe fact that he is using common sense. He is NOT against the availability of KDE in as many languages as possible. In fact, he is for it. \n\nWhat he is against is the complexity of a multilingual confernece when there is absolutely no need for it, other than political correctedness, since anyone who speaks Catalan, Euskara or Galician also speaks Spanish. \n\nMany things are lost in translation. And calling someone a troll does not make for strong reasoning. In fact, it sounds like the abdication of any reasoning whatsoever.\n\nPeople need to check their politics at the door. If the main point of a KDE-event is KDE, turning it into a multilingual maze for the sake of it is impractical and silly"
    author: "Manel"
  - subject: "Re: \u00bfIn which languaje will be?"
    date: 2006-02-11
    body: "Manel,\nthanks a lot for explaining with your words what I tried to explain.\n\nWe share the exact same view on the subject.\n\n"
    author: "Javier Marcet"
  - subject: "Re: \u00bfIn which languaje will be?"
    date: 2006-02-11
    body: "It makes him a troll to relate welcoming translators of KDE to languages spoken in Spain to political nonsense. And he's even incorrect, since what first attracted me to KDE was its translation to Basque. And you're also incorrect when you state that everyone that knows catalan, basque or galician also speaks spanish. That's absolutely false. And Antonio did not say that spanish won't be spoken because of that. What he said is that translator teams were welcomed. If I go and meet there other KDE translators, I won't speak with them in Spanish, the same will happen at i18n akademy, at FOSDEM or at RAE."
    author: "Alfredo Beaumont"
  - subject: "Re: \u00bfIn which languaje will be?"
    date: 2006-02-12
    body: "He was talking about the languages used on the conferences, not on the software, you troll."
    author: "blacksheep"
  - subject: "Re: \u00bfIn which languaje will be?"
    date: 2006-02-11
    body: "Albert, I really regret seeing an official KDE developer with your views...\n\n1: All right, I beg you pardon on this one.\nEven so, I _did_ _not_ say anything about the meeting being in Spanish, only that to attract users/developers from other countries, having as much as possible of the meeting in English (computer science's lingua franca, at least among developers, whether you like it or not).\n\n2. Oh my little **** ?!\nYou puzzle me.\nDid I say anything against any of those languages?\nI explicitely detailed that I spoke several of those laguages which people speak in Spain.\nI said I promoted them, and that protection from the authorities is needed, so that the language is not lost, but continues its evolution.\n\nNow let me as you something?\nWhat language are we using here now? Why? Would you really feel more realized as a person, or something like that if we were using Catalonian or Galician yielding the page unreadable for many of those who are indeed reading it.\nYou could say they might as well use any of the free translation engines available, but what's the cost? Information lost, increased complexity, slower communication, more error prone...\n\nI though science had always been about colaboration. Scientists with different backgrounds and origins when working together always use the common lagunage they share.\n\nIt's simply that I don't understand how one could feel any better using his/her preferred language when talking to your mates, even if your mates don't speak it thus needing a translator, while you simply could use the only language your mates know, and which for some uncertain reason you happen to be able to speak, just don't feel like to...\n\n3. Whatever. Call me a troll as much as you want.\nI certainly know that there wouldn't be an agrreement on your position among kde developers.\nNonetheless, as eager as I was to attend it, I begin to lose interest. I'll have to wait for the next _international_ kde event, where good communication is of primary concern.\n\nThis is not poetry, a novel, something which affects your inner feelings, so for the sake of it, yes, I defend the use of _1_ lingua franca among scientists, engineers, hackers..."
    author: "Javier Marcet"
  - subject: "What is an Offical KDE developer"
    date: 2006-02-11
    body: "What is an Offical KDE developer ???? \n\nThis i no company with officials ceos and shit, we are just normal people - all equal !!"
    author: "chris"
  - subject: "Re: What is an Offical KDE developer"
    date: 2006-02-12
    body: "> What is an Offical KDE developer ????\n\nA developer that has a SVN account?"
    author: "blacksheep"
  - subject: "Re: \u00bfIn which languaje will be?"
    date: 2006-02-11
    body: "If something is presented to you in your mother tongue, I'm sure it will reach you much easily than if it is in a foreign language (be it English or Spanish, and no, it doesn't matter if you learn the language from the 1st class or even before, it is still a foreign language). It will certainly bring users to you and maybe even developers, altough minimal English knowledge is anyway required from a developer.\n But why I don't understand you is: if they have the possibility (technical and human power) and the willing to make translations to several languages at the conference, why not do it?? What is wrong doing this? It will certainly not hurt anybody if they can listen the talks in the language they know the best.\nAn English only world - even inside the computer scientists - is boring sometimes. This is why there are local workshops, local organizations (I doubt they speak in English.), localized mailing lists and so on."
    author: "Andras Mantia"
  - subject: "Re: \u00bfIn which languaje will be?"
    date: 2006-03-04
    body: "I don't get why they still carry on with Catalan.  Why not just drop the language already?  People are way too stubborn and with serious NIH syndromes."
    author: "Jo"
  - subject: "Offtopic: Re: \u00bfIn which languaje will be?"
    date: 2006-02-11
    body: "I guess that if there's a Catalan translation group it makes sense to speak Catalan for that group. And the same goes for other languages.\n\nThere's no such thing as a language conflict. In such meetings people meet to cooperate. "
    author: "Daniel"
  - subject: "Re: \u00bfIn which languaje will be?"
    date: 2006-02-18
    body: "Spain is different..."
    author: "An\u00f3nimo"
  - subject: "Re: \u00bfIn which languaje will be? .10 days later ..."
    date: 2006-02-22
    body: "I also think its nonsense to speak in that many languages since all of us speak spanish. This has to be clear, all speak spanish, all understand it, dont let  you be fooled, everyone knows this in spain.\nThe only reason why there is people trying to turn this into a political problem its because thats what they do in all the aspects of live, after all thats what being nationalist its all about. I am also basque and I am astonished to see this fellow saying the only reason he joined kde is because it was translated to basque, thats great dude you should take that farther beyond and stop speaking english, only speak your brand new version of basque not older than 30 years, I would call it an alpha release though. And I also wonder how could you learn programming if its syntax its not in basque, it was not invented within the so called \"Euskal Herria\". By the way its akademy-es not basque nor other nationalist stuff, copy that?, yeah spanish or castillian its spoken by around 332.000.000 do you see it, I think thats reason enough to do it in spanish especially if we are in SPAIN!!, yes STILL SPAIN. If anyone thinks my speech is harsh, think about it twice since in this country there is a lot of people threaten and KILLED by nationalist demanding \"the land they never owned (historically proven)\".\nThis might not be the place to say this but neither it is to demand that in akademy-ES they translate it to \"batua\" or any other thing. It would be better to translate it to english."
    author: "m3g4crux"
  - subject: "Re: \u00bfIn which languaje will be? .10 days later ..."
    date: 2006-03-04
    body: "Mmmmm, it seems that there's some strange magazine that doesn't seem to think so.\nIt's called National Geograhic, take a look at the Genographic Project. Sure they are Catalan nationalists, or Basque, maybe even worse... Spanish nationalists... ugh!\nI think that all this stuff is quite stupid. I speak the same language that all off us. The same that our mothers, fathers, grandmothers, grandfathers, etc. What is funny is that if I have born only 60 quilometers north, right now I will be speaking another language, but by the same way.\nThe aKademy-es is a place of respect, what I'll be speaking tomorrow? Dunno.\nMaybe spanish or catalan or english or japanese or who knows? Who cares? You?\nSure you will not come, so why are so worried about it? Respect us.\nThat is all about: respect. My mother was born in a little town in Jaen, she once told me that I have to learn and speak Catalan. Because we live in Catalonia, a place (right now part of Spain) with it's own culture, traditions and language. Not doing so will be disgrateful with the land that right now is our home.\nThe most ironic think of all is that the first document writed in spanish (castilian, only 74% of spanish population speaks \"spanish\") (las Glosas Emilienses) has two notes at the end. One writed in Basque, the other one in Romance (the langue d'Oc, the sister of Catalan).\nWe've been together for a thousand years, we're a mixture of... nearly everyone! Except the basques.\nI'm sorry but is historically and scientifically a fact that basques has been here thousands of years before the rest of us. And they speak what is probably the oldest language in the world. I'm very proud of it, even I'm not basque.\nYou're correct on one think. People has been killed in Spain, by ETA, the GRAPO, the GAL, Al-Qaeda,etc..., the majority Catalan. \nSome of them like Ernest Lluch for telling that we have to respect our differences and life together in peace.\nAs always ironic, terrible ironic..."
    author: "Francesc Genov\u00e9 Mu\u00f1oz"
  - subject: "Re: \u00bfIn which languaje will be? .10 days later ..."
    date: 2006-03-04
    body: "Why on earth don't the spanish just speak spanish.  Several languages just for sake of it,just because their ancestors spoke it.  Why don't you just revert to latin in that case?  People are so annoying - the advantages gained from a common language far exceed any (if there are any) advantages to having your own small language just for the sake of it."
    author: "Jo"
  - subject: "Re: \u00bfIn which languaje will be? .10 days later ..."
    date: 2006-03-12
    body: "I don't know why humans doesn't speak human, and so they have thousands of languages... in fact I don't know what language is human!\nMeanwhile blame the babilionians for making the tower of Babel or God himself.\nNon ut ames oro, verum ut amare sinas... ;)"
    author: "Francesc Genov\u00e9"
---
Did you think that this year we would not have a KDE event in Spain? Nothing further from reality. <a href="http://www.badopi.org/">Badopi</a> (the LUG of Barcelona) are organising the first <a href="http://wiki.badopi.org/index.php/Kde-es2006">aKademy-es</a> event so you can have an excuse to meet other Spanish KDE developers, users, translators, artists and share some good days with other KDE people.  aKademy-es 2006 is scheduled to be a small event for all KDE users and developers from Spain to attend talks, share experiences, etc. It will take place in Barcelona from Friday 3rd to Sunday 5th of March.




<!--break-->
