---
title: "KDE Commit-Digest for 7th May 2006"
date:    2006-05-07
authors:
  - "dallen"
slug:    kde-commit-digest-7th-may-2006
comments:
  - subject: "coverity"
    date: 2006-05-07
    body: "Is there an open source equivalence product to that coverity tool? \n\nHow much automatic testing is done for KDE? I recently became aware of http://www.englishbreakfastnetwork.org/ which looks pretty kool.\n\nI know that testing is great to improve quality and trigger bug fixes."
    author: "Andy"
  - subject: "Re: coverity"
    date: 2006-05-07
    body: "There is Coverity and Krazy (EBN). Then from time to time some people check KDE with non-public tools. \nBut there are also free (OSS) tools like http://dwheeler.com/flawfinder/ . Quality of the report and the scope of what they check differs a lot, though.\n\n"
    author: "Carsten Niehaus"
  - subject: "Re: coverity"
    date: 2006-05-07
    body: "I found this here \"Coverity's SWAT tool searches for defects in general, including some security issues. It's based on previous work on the Stanford checker, which was implemented by xgcc and the Metal language\".\n\nIf its based on free software why isn't it free software?\n\nhttp://www.mpi-sb.mpg.de/~backes/xGCC/\n\nI found also smatch (for C I guess) http://smatch.sourceforge.net/"
    author: "Andy"
  - subject: "Re: coverity"
    date: 2006-05-07
    body: "And ncc\n\nhttp://freshmeat.net/projects/ncc"
    author: "Andy"
  - subject: "Re: coverity"
    date: 2006-05-08
    body: "Coverity commercial product is not based on gcc anymore, it uses Edison Design Group C/C++ frontend."
    author: "GreatElk"
  - subject: "Re: coverity"
    date: 2006-05-08
    body: "Speaking of englishbreakfastnetwork... Can someone explain the meaning of the name?"
    author: "AC"
  - subject: "Re: coverity"
    date: 2006-05-08
    body: "Reading from what's said on the site I can only assume this is\na hint to \"patiently looking at something\" like you do if you\nstare in your cup of coffee or - in this case - tea which is part\nof an English breakfast. The English breakfast network is a collection\nof servers who do the tedious task of looking at the source code for you.\nHence, this name. If someone who knows how this name came up reads this,\nI'm quite interested if my analysis is right ;-)\n"
    author: "Martin"
  - subject: "Re: coverity"
    date: 2006-05-08
    body: "guess1:\n\nEnglish breakfast is known to be poor.\n\nIt is very simple to get improvements but the British dont get it. They need\nsomeone who checks for the obvious.\n\nguess2:\n\nDevelopers dont have breakfast because they code 16h till 3am. \nBut the machine serves it anyway for them. \nTheir own breakfast, new tasks.\nI assume that it is updated every night, \nso in the morning they learn what went wrong.\n\nguess3:\nKDE Koobar developer meeting in London, the new Framework Kastro is dicussed, it is 9h in the morning, one developer still codes, others get up and call him to join them for breakfast. But the developer just has to register a little domain. \n\nguess4:\n\nBreakfast means it breaks fast, that is here the problem: the tool shows whats broken\n\nEnglish because it is the English language version, first\nthe breakfast scripts were Dutch only.\n"
    author: "And"
  - subject: "Re: coverity"
    date: 2006-05-09
    body: "I tend to register domains when drunk and the name seemed like a good idea at the time. Besides, parts of the English breakfast are easy to name: sausages, eggs, bacon, ... so that gives you hostnames (heck, I even had one machine called 'ketchup' though I doubt the English would forgive me that one). And it's a kind of tea. I like tea. Not English breakfast tea though.\n\nThis message was brought to you by the letter K, the number 4 and the Children's Television Network."
    author: "Adriaan de Groot"
  - subject: "VFAT?"
    date: 2006-05-07
    body: "\"VFAT rewrite\" - what is this about?"
    author: "funzt"
  - subject: "Re: VFAT?"
    date: 2006-05-07
    body: "A (slightly) expanded explanation of the VFAT work in amaroK can be read straight from the developer who is working on the rewrite here: http://amarok.kde.org/blog/archives/87-More-on-the-VFAT-rewrite.html\n\nDanny"
    author: "Danny Allen"
  - subject: "Rosetta"
    date: 2006-05-07
    body: "Mark Shuttleworth said in Wiesbaden he talked to KDE people. What was this talk all about?\n\nAnd his Rosetta tool which seems to have an amazing community around, will it also be used by Kubuntu or once it is open sourced by the KDE translation project? To me it looks much better designed than pottle or Robertsons's IRMA tool, seems to be plone based."
    author: "Mark"
  - subject: "Re: Rosetta"
    date: 2006-05-07
    body: "> What was this talk all about?\n\nBe patient, it will make big news splashes in the next weeks."
    author: "Anonymous"
  - subject: "Re: Rosetta"
    date: 2006-05-07
    body: "you had to do it.. you had to say BIG news!!\nNow I'm curious too :)"
    author: "Iuri Fiedoruk"
  - subject: "Re: Rosetta"
    date: 2006-05-07
    body: "I can tell you in advance...\n\nUbuntu will have a new release, that will make big news splashes. And Kubuntu follows the Ubuntu release cycle, which means it'll probably get mentioned too."
    author: "anon"
  - subject: "Re: Rosetta"
    date: 2006-05-07
    body: "First, Google is your friend:\n\nhttp://www.google.com/search?client=safari&rls=en-us&q=Rosetta+tool+ubuntu&ie=UTF-8&oe=UTF-8\n\nNow, as to what he said to the kde folks, well, more likely than not, he is \ngoing to provide the quick translations for kde as a whole.\nprobably wants to work together on stuff. One of the things that I have noticed \nabout KDE is that it appears to have the best translation capabliities.\nMy guess is that he will make use of KDE effort to help his gnome interface.\nOf course, his work will also speed up the kde interface, this is not a bad thing."
    author: "a.c."
  - subject: "Re: Rosetta"
    date: 2006-05-08
    body: "\"And his Rosetta tool which seems to have an amazing community around, will it also be used by Kubuntu or once it is open sourced by the KDE translation project?\"\n\nI don't see an amazing community around Rosetta.\n\nLooking at the Dutch translation of ubuntu/kubuntu: all files listed in Rosetta have been translated bij gnome-nl and kde-nl using their own tools (kbabel, gtranslator etc), not with Rosetta.\n\nAlso, both projects have reached 100% translation a long time ago, yet Rosetta shows several packages only partially or not translated. Making them available through Rosetta encourages duplicate translation efforts, and will make the applications inconsistent. Especialy because Rosetta cannot provide a good overview of the complete po-file. Thus large po-files, like kstars with 5000 strings, would be hard to keep consistent using Rosetta.\nI've seen this with Linspire, several applications that have been 100% translated for years by our team were provided through IRMA and translated by others as well. \n\nAnother problem with Rosetta is that it does not provide a translation memory.\nWith tools like Kbabel you can translate files up to 20-50% automaticly using a database of previous translations. This saves time. And it keeps translations consistent, because kbabel can show a list of similar strings in other files.\nRosetta does not provide this.\n\nSince Rosetta hides the actual po-file, translators can not easily test their translations in the actual application.\n"
    author: "Rinse"
  - subject: "Re: Rosetta"
    date: 2006-05-08
    body: "You said> Another problem with Rosetta is that it does not provide a translation memory.\n With tools like Kbabel you can translate files up to 20-50% automaticly using a database of previous translations. This saves time. And it keeps translations consistent, because kbabel can show a list of similar strings in other files.\n Rosetta does not provide this.\n----\n\nCannot agree with that, I think these are features provided bz Rosetta as far as I could see. Alternative translations show up. Most webbased tools suck, this tool seems to be good.\n\nThe real problem of translations is of course\n- Anglicism, naming oriented to basic language, no language specific metaphores \n- Entry level for translators / it is not the easy task it should be...\n- translations usually would have to be done or started in a developer version stage\n- lack of good and easy documentation about application translation"
    author: "And"
  - subject: "Re: Rosetta"
    date: 2006-05-08
    body: "\"Cannot agree with that, I think these are features provided bz Rosetta as far as I could see. Alternative translations show up.\"\nthat is correct, but there is no way of telling where they come from, only who used the translation previously.\nAnd to determine if a suggestion from another translation is suitable, one needs to know the context it is used in. With apps like kbabel you can get this information, because the application tells you from wich po-file the suggestion comes, and you can click on it to open the po-file and check the context it is used in..\n\n\ni think in general, tools like rosetta are nice as complementary to other tools, like kbabel, but they still can't compete with them.\n"
    author: "rinse"
  - subject: "Re: Rosetta"
    date: 2006-05-08
    body: "i think in general, tools like rosetta are nice as complementary to other tools, like kbabel, but they still can't compete with them.\n\n//beginner drugs so to speak, which get people involved. The community aspect is important, think of kdelook or kdeapps success.\n\n"
    author: "And"
  - subject: "Re: Rosetta"
    date: 2006-05-09
    body: "But where is that community?\n\nAs said earlier, I don't see a large community around Rosetta\n"
    author: "Rinse"
  - subject: "Re: Rosetta"
    date: 2006-05-09
    body: "KDE has its own translation team, which is something most projects don't have. So please see the comments above in that light. There is more to translate then just the major GUI environments.\n\nAlso; Rosetta automates the uploading of patches to the upstream projects so your concern about duplicating translations should be non-existant. If it is, then its a bug.  You have to consider that KDE _has_ a translation system so Rosetta has to be adjusted for KDE to work, bugs can exist and should be reported.\n\nLast; Kubuntu is a free (speech AND beer) distro, in contrary to Linspire that ships free software but does it opposing any and all principles that build that software.\n\nSee: www.groklaw.net/article.php?story=20060424164142296"
    author: "Thomas Zander"
  - subject: "Re: Rosetta"
    date: 2006-05-09
    body: ">KDE has its own translation team, which is something most projects don't >have. So please see the comments above in that light. There is more to >translate then just the major GUI environments.\n\n\nI'm aware of that, but I don't expect that tools like rosetta will make sure that those tools get translated as well.\nIf i look at the Dutch team on rosetta, most, if not all, of the members come from existing teams (mostly gnome) and have enough work to do to keep their own translations up to date. \n \n\n> Also; Rosetta automates the uploading of patches to the upstream projects so >your concern about duplicating translations should be non-existant.\n\nThat's not 100% true: if someone starts translating koffice into Dutch it will be a duplicate effort, because koffice is already 100% translated. (but not in rosetta). So if the translation goes upstream to our team, it will probably go to /dev/null\n\nAnd how will ubuntu send the files upstream?\nStraight into KDE's SVN, or to the teams?\n\n\nAnother part of concern is the quality of the software translated with Rosetta, if someone decides to translate an kde-application that has'nt been translated yet, what guarantee would we have that he/she will follow the guidelines that our team uses?\n\n \n> Last; Kubuntu is a free (speech AND beer) distro, in contrary to Linspire >that ships free software but does it opposing any and all principles that >build that software.\n\nSpeaking of Linspire, they promised to send the translated applications upstream to the original teams. \nSo far i haven't seen any code from them...\n\n"
    author: "Rinse"
  - subject: "Sounds Good"
    date: 2006-05-07
    body: "All of this sure sounds good all right :-)\n\nI'm especially glad that the Phonon and DBUS plans seems to have worked out, and that _lots_ of new people are working on amaroK."
    author: "one"
---
In <a href="http://commit-digest.org/issues/2006-05-07/">this week's KDE Commit-Digest</a>: <a href="http://scan.coverity.com/">Coverity</a> fixes continue to roll in. <a href="http://amarok.kde.org/">amaroK</a> gets enhanced support for VFAT (ie. Generic Audio) devices. New themes for <a href="http://opensource.bureau-cornavin.com/ktuberling/">KTuberling</a>. Preliminary support for both next-generation disc formats (Blu-ray and HD-DVD) in <a href="http://k3b.org">K3B</a>. KDE 4 changes: More apps ported to <a href="http://www.freedesktop.org/wiki/Software/dbus/">D-BUS</a>. <a href="http://developer.kde.org/~wheeler/juk.html">JuK</a> gets the ball rolling on porting to <a href="http://phonon.kde.org/">Phonon</a>.

<!--break-->
