---
title: "KDE Human Computer Interaction Working Group Formed"
date:    2006-03-15
authors:
  - "kwimer"
slug:    kde-human-computer-interaction-working-group-formed
comments:
  - subject: "Information ought to be accessible easily"
    date: 2006-03-15
    body: "empowering the kde user to access and manipulate information from anywhere and easily would be a nice thing for KDE 4. \n\nlike \"links, bookmarks, important apps/folders\" should be accessible/available to any application and to important dialogs e.g., file open/save dialogs.\n\ni've setup my metabar for easier access to my folders in konqueror but i can't access that in other applications like k3b or other. i miss this quick access functionality in all other kde applications.\n\nI could not move/delete stuff in k3b file/folder view, as I could do with konqueror.\n\nKonqueror is the best application to be followed by all other KDE applications!\n\n"
    author: "fast_rizwaan"
  - subject: "Re: Information ought to be accessible easily"
    date: 2006-03-15
    body: "there IS the universal sidebar, which can embed the metabar. but you're right, there should be more integration in applications. maybe kat/tenor can be more integrated in KDE 4..."
    author: "superstoned"
  - subject: "Re: Information ought to be accessible easily"
    date: 2006-03-15
    body: "Has anything actually come of Tenor?  I haven't heard about it in a long time.  Novell is working on a native KDE frontend to Beagle; maybe the two projects can be merged somehow."
    author: "jaykayess"
  - subject: "Re: Information ought to be accessible easily"
    date: 2006-03-15
    body: "I'm not sure that you got the meaning of the term \"Accessibility\" right, so here's a pointer.\n\nhttp://en.wikipedia.org/wiki/Accessibility"
    author: "sebas"
  - subject: "HCI story again"
    date: 2006-03-15
    body: "Back in the KDE2.0 development series, myself and a few others created an HCI working group.  The mailing lists had decent activity for a month, and we came up with some interface standards that made sense (like menu-bar consistency guidelines).  Our proposals were simply ignored at the time.  Maybe this time someone will actually pay attention.\n\nThat said, KDE's usuability has often been greatly improved by the dedicated work of people like asiego, unilaterally since he just went ahead and coded it.  Unless the HCI group is ready to go ahead an code it, they will not be nearly as effective as single users who can make improvements."
    author: "Troy Unrau"
  - subject: "Re: HCI story again"
    date: 2006-03-15
    body: "Since i start using kde as desktop i always want to do something for kde . I always dream big ,and think of brilliant ideas and how to code them. But when i sit in front of my pc and after writing 20 odd lines of code , i start feeling sleepy and drowsy and then i go out and smoke and then i eat and goto sleep. I have done lots of reverse engineering stuff in windows platform and i am good at both assembly and c.At present i am getting hang of linux environment.\nHopefully , will be able to contribute after kde4 release :-) ,till then i will rest , i am already feeling sleepy .\n\nAnyway , they should come out with a kde4 desktop or lack off look and concept competition if its not 2 late .Has it been already done ? \n\n"
    author: "ksleep"
  - subject: "Re: HCI story again - DATK!"
    date: 2006-03-17
    body: "DATK: Design At The Keyboard Syndrome attacks again.\n\nDo things on paper in your spare time between classes, meetings, on the bus etc. Avoid distractions like typing, worrying about syntax, compilation errors and the like.\n\nHTH."
    author: "B8ating"
  - subject: "Re: HCI story again"
    date: 2006-03-17
    body: "Hmm; before that time I (and others) already had a kde guidelines which was adopted at the KDE2 meeting (and thus before kde2 came out).\nWith those guidelines KDE has worked many years and has come a long way.  Now the KDE environment (both people and code) is ready to go to a next step. The HCI WG is formed for that reason and has gained acceptance by the programmers which goes a long way to make sure issues are actually resolved and realized."
    author: "Thomas Zander"
  - subject: "KMail shortcuts"
    date: 2006-03-16
    body: "Does it mean that from now on pressing Down arrow in the mail list of KMail will move the selection down and pressing Up will move the selection Up ?\n\nIf so, then I welcome this initiative. If not, the effort is wasted. What I mean is KDE project should enforce some standards on developers so that some stubborn and too inovative developers do not create surprisingly behaving software. My idea is someone should define base requirements for GUI design. Applications which will not comply would simply not be included in the KDE software bundle."
    author: "ZACK"
  - subject: "Re: KMail shortcuts"
    date: 2006-03-16
    body: "> Applications which will not comply would simply not be included in the KDE software bundle.\n\nThen please tell me a KDE-based mail programm with a similar feature set like KMail. \n\nThank you!"
    author: "cl"
  - subject: "Re: KMail shortcuts"
    date: 2006-03-16
    body: "We now have the status quo and must find a way to deal with that.  I found that our current policies make it hard to fix menu issues, so perhaps the HCI group needs to work on that.\n\nHowever, ZACK is correct when it comes to new applications to be added to KDE.  They should only be added after the HCI group approves them."
    author: "James Richard Tyrer"
  - subject: "Re: KMail shortcuts"
    date: 2006-03-17
    body: "The KMail feature set is fine (just missing Exchange connector :-). Just the human interaction design is wrong because it DIFFERS from the rest of KDE programs.\n\nThe mail list keyboard hadnling is totaly different e.g. from Konqueror (which certainly is a typical KDE app). The END, HOME, UP, DOWN keys do not work here by default (yes, you can reconfigure this by it should be default) and SHIFT Up/Down does not work at all. It must even have taken an effort to make listview work unexpectadely and for a sad result.\n\nAnd don't take me wrong - I am willing to like KMail. I want it to change so that I can recommend it to others. I don't want it to be another GIMP.\n\nI know that there is a lot of difficult work behind KMail's internals. But it is often a small \"uniportant\" glitch like this that makes people distracted. Take GIMP as typical example - it contains great functions but the GUI is so uncommon/inovative/unlike-to-anything  that many people are looking forward to escape from it to KRita even if KRita is still in its infancy but on a right track.\n\nKMail is default KDE mail app. So it must have a default KDE behavior.\n\n\nSo dear HCI "
    author: "ZACK"
  - subject: "Re: KMail shortcuts"
    date: 2006-03-17
    body: "> I want it to change....\n\nI don't. Now what?\n\n> KMail is default KDE mail app. So it must have a default KDE behavior.\n\nThere are always exceptions. No rule fits all. \n\nIMHO the focus-less keyboard navigation is superior to other email clients."
    author: "cl"
  - subject: "Re: KMail shortcuts"
    date: 2006-03-17
    body: "I think that focus-less keyboard navigation is a very stupid idea. Not because I like it or not but because it is DIFFERENT from the rest of KDE!!!\n\nThe whole KDE interface teaches the user that input goes where the focus is. This is predictable, understandable, usable. And suddenly, user starts KMail, focuses an item,  types a key and something on the other side of the screen changes/moves. Is this an expected behavior?\n\nDon't pretend you don't uderstand what I mean."
    author: "ZACK"
  - subject: "Re: KMail shortcuts"
    date: 2006-03-19
    body: "The three most important things in a GUI are:\n\nConsistency\n\nConsistency\n\nConsistency\n\n"
    author: "James Richard Tyrer"
  - subject: "Re: KMail shortcuts"
    date: 2006-03-27
    body: "first of all i want to congratulate James for his well thought seggustion\nKDE is the closest thing to win98se classic look which is the most established\nformat of GUI, KDE SHOULD EXPLOITE this because it would make it easier for\nall types of windows users \"to as you say switch\" furthermore those who try\nlinux dont want to learn new things because that would take some time so the\nease of use of the classic win95/98 look is there why not exploit it, it makes\nsense!"
    author: "an Architect"
  - subject: "homepage"
    date: 2006-03-20
    body: "Does the hci group have a homepage yet?\n\n"
    author: "waw"
  - subject: "Re: homepage"
    date: 2006-03-22
    body: "No, we don't have a website for the workgroup yet, but all of the participating teams have of course websites:\n\nhttp://accessibility.kde.org/\nhttp://artist.kde.org/\nhttp://docs.kde.org/\nhttp://l10n.kde.org/\nhttp://usability.kde.org/\n\nThe other workgroups don't have websites either, because their main aim is coordination of existing work.\n\nOlaf\n"
    author: "Olaf Jan Schmidt"
  - subject: "Re: homepage"
    date: 2006-03-22
    body: "So, if I've got some ideas/concepts to improve kde, i'll turn to... ?"
    author: "waw"
  - subject: "Re: homepage"
    date: 2006-03-22
    body: "http://www.kde-forum.org/ or http://www.kde-look.org/"
    author: "cl"
  - subject: "Re: homepage"
    date: 2006-03-28
    body: "The main development of KDE is still done on the mailing lists, like it always was. The working groups are there to help collaboration and provide a point-of-contact. So if you have ideas to improve usability, send them to kde-usability@kde.org; if you have ideas to improve the documentation, send them to kde-doc-english@kde.org , etc.\n\nIf you're not sure where to start, try kde-quality@kde.org. They can tell you where to suggest ideas, and what things you can do to help them happen."
    author: "Philip Rodrigues"
  - subject: "Re: homepage"
    date: 2006-04-05
    body: "Ok, I will do as you say, I have just to write it down first.\n\ngru\u00df"
    author: "waw"
---
In order to facilitate current inter- and extra- community communication
and innovation there is need for a Human Computer Interaction
working group (short name "HCI working group") within KDE made up
of core accessibility, usability, internationalization and artist
community members. This group will act not only to strengthen and
improve current processes but also as a catalyst for new ideas.  Following the <a href="http://dot.kde.org/1139614608/">Technical Working Group</a> and <a href="http://dot.kde.org/1131467649/">Marketing Working Group</a> this is the third <a href="http://ev.kde.org/meetings/2005-working-groups-discussion.php">group from KDE e.V.</a>.




<!--break-->
<p>Key members, based upon motivation and effort not name and popularity,
from the different KDE communities involved, have stepped forward to
initiate this effort.</p>

<p>The working group is comprised of e.V. members. At this time the current
members are Celeste Lyn Paul, Nuno Pinheiro, Ellen Reitmayer, Olaf Schmidt, 
Kenneth Wimer, Thomas Zander.</p>

<p>Much of what our working group intends to accomplish is reliant upon
communication and coordination with the other working groups being
formed as well as the overall KDE community.</p>

<p>In one sentence this working group's goal is to create a more
compelling, usable, understandable interface for all people.  This
is, we believe, one of the major goals of KDE.</p>



