---
title: "Novell Contributes Polish Translation of KDE Docs"
date:    2006-06-15
authors:
  - "klichota"
slug:    novell-contributes-polish-translation-kde-docs
comments:
  - subject: "Very nice!"
    date: 2006-06-15
    body: "Thanks a lot Novell!"
    author: "Joergen Ramskov"
  - subject: "Re: Very nice!"
    date: 2006-06-15
    body: "I'm not using suse but I'm going to buy support from Novell for this commit. Thanks again."
    author: "alek"
  - subject: "again polish?"
    date: 2006-06-15
    body: "again polish?\n\ndo we have Novell Russia or smth?.."
    author: "Nick"
  - subject: "Re: again polish?"
    date: 2006-06-15
    body: "AFAIK this depends from local company's decision, not from central in USA."
    author: "mkkot"
  - subject: "Re: again polish?"
    date: 2006-06-16
    body: "Given that Poland suffered from Russia and Germany in the history a lot, I'm quite sure you upset polish people, attributing Poland to Russia. An old joke goes as follows \"Are the Russians and Polish brothers or friends? Brothers - you can choose your friends.\" (Probably wors with Germans as well, but I heard it only this way around.)\n\n\nFor Novell it's a market of 40 million people in the middle of Europe. Having a proper localization is the basis to get a piece of the Cake."
    author: "Carlo"
  - subject: "Re: again polish?"
    date: 2006-06-16
    body: "Huh?! He didn't attribute Poland to Russia.  \n\nIIUC he just wondered why there was *another* contribution to the Polish translations (in another posting I read that there had already been two, one from Mandrake and one from Novell) and he suggested helping the Russian translations. \n\n "
    author: "cm"
  - subject: "Re: again polish?"
    date: 2006-06-16
    body: "yes, the post is very clear once you understand \"again Polish\" is reverse polish notation for \"Polish again\" :)\n"
    author: "ca"
  - subject: "Re: again polish?"
    date: 2006-06-17
    body: "It's normal that non native speakers are making that kind of mistakes. I didn't even noticed this \"again Polish\" and \"Polish again\". In English this makes difference, but for me it is clear that Nick wondered why we have third commercial translation contribution since about half year :)\n\nAnd some other interesting thing. Nick, why you asking about Novell Russia, when you have an Ukrainian e-mail adress? ;)"
    author: "mkkot"
  - subject: "Re: again polish?"
    date: 2006-09-21
    body: "Thursday 15/Jun/2006, @15:53 \n \nGiven that Poland suffered from Russia and Germany in the history a lot, I'm quite sure you upset polish people, attributing Poland to Russia. An old joke goes as follows \"Are the Russians and Polish brothers or friends? Brothers - you can choose your friends \nFirst I didnt get it!! Then I had to think. Good one. Do you know why they make fun of polish people, calling them dumb. I am polish by marriage. I resent that. They things about my African-native american, English self too."
    author: "annie kolakowski"
  - subject: "Re: again polish?"
    date: 2006-09-21
    body: "I meant-----------------They say things about my African, Native American, English too.(ME)"
    author: "annie kolakowski"
  - subject: "Re: again polish?"
    date: 2006-06-16
    body: "here in ukrane we've got books that say ukrainians suffered from russia even more and if you listen to national radio'll know that russia wants to annex ukrane back and forbid ukrainian language :))"
    author: "Nick"
  - subject: "Who did it?"
    date: 2006-06-15
    body: "Did Novell hire some of the kde polish l10n people or was done by a \"third\" party and then contributed back?\n\nIf it was the second case, did you have any problem with used terminology beign different on what you (the kde polish l10n team) use?\n\nAnyway, thanks Novell!"
    author: "Albert Astals Cid"
  - subject: "Re: Who did it?"
    date: 2006-06-15
    body: "Novell didn't hire anyone from Polish translation team. I'm not quite sure that they paid someone else for this translation. I heard that translation has good quality (I didn't check), so maybe some people from Novell have translating this.\n\nProblems with terminology? Sometimes yes, but (agin) I'm not sure if in this case. Previously Polish translation team received contributions from Mandriva Poland and Novell (now it's second time that Novell contributes to translations). And previously we had to check all these files, because they've had many grammar errors, typos and of course terminology differents. Now co-ordinator has submited all files at once, so they must be good :-)\n\nBut maybe he will comment this."
    author: "mkkot"
  - subject: "Re: Who did it?"
    date: 2006-06-16
    body: "It has been done by third party.\n\nThere were some issues with bringing the translation to consistent state with other translations (especially the vocabulary) and the translations were for previous version of KDE and had to be updated. That's why it took so long.\n\nRegards\nKrzysztof Lichota\n"
    author: "Krzysztof Lichota"
---
In the good spirit of cooperation between Novell and KDE, <a href="http://www.novell.com/poland/">Novell Poland</a> contributed a large number of translations of KDE documentation to the <a href="http://kdei18n-pl.sourceforge.net/">Polish localisation team</a>.  The contribution contained 119 translation files and over 5700 translated messages (see <a href="http://websvn.kde.org/branches/stable/l10n/pl/docmessages/kdebase/kicker.po?rev=502497&amp;view=rev">this commit</a>). Due to the long queue of translations it took us a while to process them, but we got them in to the recent KDE 3.5.3.  We would like to thank Novell Poland for this contribution. We hope to see more cooperation in the future to bring the best desktop environment closer to the needs of non-English speaking users.

<!--break-->
