---
title: "Join the KDE Developers at FOSDEM 2006"
date:    2006-01-07
authors:
  - "sk\u00fcgler"
slug:    join-kde-developers-fosdem-2006
comments:
  - subject: ":)"
    date: 2006-01-07
    body: "Sounds exciting, and it would be great to meet the developers, but I don't think I could get out of the country in Feb.  I'm going to be taking a month long road-trip across the U.S. and I'd love to go to some events nearby. If anyone knows of any events in the U.S. that are coming up, please email me! Thanks."
    author: "Vlad Blanton"
  - subject: "Re: :)"
    date: 2006-01-07
    body: "I'll second the US events request :)"
    author: "Greg Martyn"
  - subject: "If it were possible..."
    date: 2006-01-07
    body: "If it were possible for me to get to the meet, I'd ask the developers what it really takes to develop just *one* good quality font package like Tahoma found in the Windows world. That tiny [256kb] font package is so beautiful. Linux even with KDE, does not have anything close to it.\n\nIt's so small, so one wonders why Linux users even with KDE have to put up with blurry fonts or have to download M$ fonts to make their desktops more of a pleasure to look at. Does anyone agree?"
    author: "charles"
  - subject: "Bitstream Vera?"
    date: 2006-01-07
    body: "Personally, I like it even better than the MS fonts."
    author: "Illissius"
  - subject: "Re: Bitstream Vera?"
    date: 2006-01-08
    body: "I second that! Pretty crisp, easily readable at even really small sizes - Vera Sans Mono and Vera Sans are just THE UI fonts. That's one thing we really must thank the GNOME foundation and Bitstream for!"
    author: "Gunter Ohrner"
  - subject: "Re: Bitstream Vera?"
    date: 2006-01-08
    body: "Me too I like these fonts a lot, only when you turn OFF antialiasing they look bad :-("
    author: "ac"
  - subject: "Re: Bitstream Vera?"
    date: 2006-01-12
    body: "\"Doctor, it hurts when I do this ...\"\n\"Then don't do that!\""
    author: "Unanimous Coward"
  - subject: "Re: If it were possible..."
    date: 2006-01-07
    body: "Developing a good font from scratch is completely different from writing an applications, or even designing an icon set. It's much more difficult, demands much in the way of specialized training and very good tools. I used to create fonts for non-western scripts and the only way I could get usable results was by scanning existing type and cleaning it up.\n\nThere's a reasonable open source font design application (http://fontforge.sourceforge.net/). You could give it a try and discover the issues involved."
    author: "Boudewijn Rempt"
  - subject: "Re: If it were possible..."
    date: 2006-01-07
    body: "I wonder if it is possible to development a replacement for TTF that is easier for artists to create fonts to.\n\nWouldn't SVG be up to the task? Maybe an extension for it? There are a few good quality tools for SVG, not so for TTF.\nBesides, SVG supports a lot of other features like gradients, that could really take fonts to another step."
    author: "blacksheep"
  - subject: "Re: If it were possible..."
    date: 2006-01-07
    body: "It's not the format -- ttf is perfectly fine, postscript is perfectly fine -- it's the kind of work. An artists creates something that's going to be gazed at, appreciated for the way it looks. The very best fonts are the one where you literally don't see the quality of the design because all it does is make you read faster.\n\nIt's a completely different trade from coder, icon or logo designer, website designer or visual artist for a game. Artists don't create fonts -- unless we're talking about fantasy fonts that are not usable for reading lots of text in. (And the use of gradients in fonts...)\n\nGood font designers are very rare beasts indeed. Matthew Carter belongs to the select group that includes Hermann Zapf, William Caslon, F.W Goudy and J. van Krimpen.\n\nAll I can say is, try fontforge. Try to design sixty-two letters that work well together. Then try to make them work on screen. It may be the beginning of a life-long passion :-)"
    author: "Boudewijn Rempt"
  - subject: "Re: If it were possible..."
    date: 2006-01-08
    body: "Thank you for the lenghty explanation.\nI had this question for a long time, and it's nice to finally have an answer. :)"
    author: "blacksheep"
  - subject: "Re: If it were possible..."
    date: 2006-01-07
    body: "I tend to disagree. My Dejavu fonts look just as great as your Tahoma. It's default on kubuntu, check it out here:\n http://dejavu.sourceforge.net/\n"
    author: "Patcito"
  - subject: "Re: If it were possible..."
    date: 2006-01-07
    body: "Great! Why not make them default. KDe 4 shall get default fonts so distributors don't mess with the fonts and users blame it on KDE."
    author: "Gert"
  - subject: "Re: If it were possible..."
    date: 2006-01-08
    body: "DejaVu Sans Condensed it's an incredibly good for the desktop. Also FreeSans is a very good font. Try them!"
    author: "Flavio"
  - subject: "Re: If it were possible..."
    date: 2006-01-09
    body: "Hello Flavio,\n\nYour widget theme and icon set look really nice! May I ask which ones they are? Thanks!"
    author: "Anonymous Coward"
  - subject: "Re: If it were possible..."
    date: 2006-01-09
    body: "Lipstik style + Baghira windeco."
    author: "Flavio"
  - subject: "Re: If it were possible..."
    date: 2006-01-11
    body: "I was using freesans and recently (post 3.5.0 upgrade) it changed so there is a ton of vertical spacing.  Any suggestions?"
    author: "pigah"
  - subject: "Re: If it were possible..."
    date: 2006-01-09
    body: "Hello Patcito,\n\nDoes DejaVu make a significant difference with Bitstream Vera? According to their web site, the former is based on the later, only with more glyphs for latin-2 scripts -- which I approve of wholeheartedly! It's just that until Lubos Lunak's optimized version of fontconfig is out to the world, I'm careful about the overhead of adding new fonts. :)\n\nThanks,\n\n-- some random Internet guy"
    author: "Anonymous Coward"
  - subject: "Re: If it were possible..."
    date: 2006-01-09
    body: "Yes, afaik, DejaVu == Bitstream Vera + more glyphs."
    author: "Illissius"
  - subject: "Re: If it were possible..."
    date: 2008-12-12
    body: "you are a homo!"
    author: "mike hunt"
  - subject: "Re: If it were possible..."
    date: 2006-01-07
    body: "there ist bitstream-vera (you know the company bitstream?) they donated this font.\nits included with linux "
    author: "chris"
  - subject: "Re: If it were possible..."
    date: 2006-01-13
    body: "It's in the kernel? Wow!"
    author: "Jeve Stobs"
  - subject: "Come along dammit! it's fun!"
    date: 2006-01-07
    body: "For those who don't know FOSDEM is the biggest and best IMHO FOSS event/conference in Europe. It is really a FOSS developer conference run by/for FOSS developers. It is definitely *not* a tradeshow or a marketing exercise (think more Akademy). It is all about getting people together to exchange ideas and met the faces behind the nicks and sigs.\n\nIf you are in Germany, France or Belgium (especially Belgium!) then you've really got no excuse for not showing up. Put your name up on the Wiki and also where you will be travelling from and when. Chances are that you can meet up with a fellow KDEer on journey to Brussels.\n\n--\nSimon\n"
    author: "Simon Edwards"
  - subject: "Re: Come along dammit! it's fun!"
    date: 2006-01-07
    body: "LinuxTag is also not bad. Imho Tim O'Reilly has the best conference concept. But Fosdem is the most useful one for developers. In my opinion more Fosdem like events have to take place. I am also sure Wizard of OS 4 in Berlin will be great. \n\nHow was Osnabr\u00fcck 4?\n"
    author: "Andre"
  - subject: "Wade?"
    date: 2006-01-12
    body: "If Wade goes to this he better pack me in his suitcase damnit.\n;)"
    author: "Ryan"
---
FOSDEM, the sixth <a href="http://www.fosdem.org/2006">Free and Open source Software Developers' European Meeting</a> will be held on 25 and 26 February 2006 in Brussels. KDE will be present there to socialise, hack and take part in the wider Free Software community.  KDE has reserved a devroom to serve as a central meeting point for the KDE crowd.  We will be holding our own talks which so far include Raphael Langerhorst on KOffice 1.5 and Jonathan Riddell on Kubuntu.










<!--break-->
<div style="border: thin solid grey; padding: 1ex; margin: 1ex; float: right; width: 270px">
<img src="http://static.kdenews.org/jr/fosdem.png" width="270" height="80" />
</div>

<p>KDE's presence at FOSDEM 2006 is being organised by <a href="http://www.kde.nl">KDE-NL</a> and will be coordinated via the <a href="https://mail.kde.org/mailman/listinfo/kde-events-benelux">kde-events-benelux mailing list</a>. There is also a <a href="http://wiki.kde.org/tiki-index.php?page=fosdem2006">KDE FOSDEM 2006 wiki page</a> where you can add your name if you plan to attend. We will be booking beds in a youth hostel for KDE developers so please note on the wiki if you would like to have a bed reserved for you. We will be booking soon to avoid having to sleep in the streets of Brussels, it is cold in February and inexpensive beds might be scarse during such an event, so please put your name down for beds by Monday 16th.</p>

<p>If you would like to give a talk, please send a short note with a summary to the mailing list since there is only a limited amount of slots available.</p>









