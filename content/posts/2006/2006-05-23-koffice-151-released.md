---
title: "KOffice 1.5.1 Released"
date:    2006-05-23
authors:
  - "brempt"
slug:    koffice-151-released
comments:
  - subject: "Mean while in KDE headquarters..."
    date: 2006-05-23
    body: "http://www.alweb.dk/blog/anders/bye_bye_kword"
    author: "Kooba"
  - subject: "Re: Mean while in KDE headquarters..."
    date: 2006-05-23
    body: "Buy_buy_Kooba"
    author: "Grad"
  - subject: "Re: Mean while in KDE headquarters..."
    date: 2006-05-23
    body: ":D\n\nhey, every big app has its crashes. Kword isn't that bad, at least not for me... i have just one reliable crash: type in a url, hit space or enter - and kaboom. just put autosave on 1 min, and don't forget to save (name) a doc as soon as you started it - or there is no autosave (kword should really fix this)."
    author: "superstoned"
  - subject: "Re: Mean while in KDE headquarters..."
    date: 2006-05-28
    body: "If your copy of Koffice/Kword is crashing that oftern, there obviously something really wrong with how you compiled it or how your system is setup.\n\nRemove an 'optimization tweaks' and stick to conservative CFLAGS and recompile; for me, I'm using FreeBSD with the following FLAGS, -Os -pipe -fno-strict-aliasing -funroll-loops -fschedule-insns2 and I have yet to experience a crash or any of the problems I hear people complain about here."
    author: "kaiwai"
  - subject: "kexi patch?"
    date: 2006-05-23
    body: "I understand that because it was too late in the release progress to do anything about it but is there a patch out there to fix whatever this critical error is?"
    author: "Stephen"
  - subject: "Re: kexi patch?"
    date: 2006-05-23
    body: "http://lists.kde.org/?l=koffice-devel&m=114805806010938&q=p3"
    author: "anonymous"
  - subject: "SUSE packages not affected"
    date: 2006-05-23
    body: "\"The KOffice release announcement mentions some post-release discovered issues with Kexi - the SUSE rpms already contain the patches to fix these so don't hesitate to upgrade your KOffice because of that.\"\n\nhttp://www.kdedevelopers.org/node/2035"
    author: "jstaniek"
  - subject: "Re: kexi patch?"
    date: 2006-05-23
    body: "> I understand that because it was too late in the release progress to\n> do anything about it but...\n\nNo, it was not too late. The bug (+ patch) has been discovered on Friday, release has been on Tuesday. Emailing the patch to the thee to ten packagers does not need three days. At least the Suse packages already include the fix."
    author: "Hans"
  - subject: "Re: kexi patch?"
    date: 2006-05-23
    body: "Yes, it was too late. It was a week after the source packages were created for the first time. We recreated the packages two times for other bugs during that week. If we had recreated the source package again, the release wouldn't have been until Friday. Recreating the source packages, asking the packagers to recreate their binarie packages again does take a week. We decided not to wait."
    author: "Boudewijn Rempt"
  - subject: "Re: kexi patch?"
    date: 2006-05-23
    body: "> We recreated the packages two times for other bugs during that week.\n\nJust do it a third time or release an additional RC.\n\n> If we had recreated the source package again, the release wouldn't have been\n> until Friday.\n\nI think the release was an Tuesday, not Firday. (see www.koffice.org, dot.kde.org,...). \n\n> Recreating the source packages, asking the packagers to recreate their\n> binarie packages again does take a week.\n\nNo, it does not take a week. At least you could ask the packagers, how long they would need. But you did not even try. I think that the Kubuntu people would have managed to update the packages and Suse actually *did* include the fix:\n\nFrom the Suse-RPMs changelog:\n\n* Fri May 19 14:00:00 2006 feedback@suse.de\n- Kexi: fix forms plugin not being loaded\n- Kexi: fix possible data loss in forms\n[...]\n\nSo you have the funny situation that the Suse packages are better than the original source tar balls. Thanks, Stephan Binner!"
    author: "Hans"
  - subject: "Re: kexi patch?"
    date: 2006-05-23
    body: "As I said -- instead of telling me what to do, telling me I'm wrong and calling me lazy, do the work yourself. Deal?"
    author: "Boudewijn Rempt"
  - subject: "Re: kexi patch?"
    date: 2006-05-23
    body: "I don't think that anyone is saying that you are lazy.\n\nHowever, when a critical bug is found, the release MUST be delayed to fix it.  To do otherwise is irresponsible."
    author: "James Richard Tyrer"
  - subject: "Re: kexi patch?"
    date: 2006-05-23
    body: "The Debian packages (just uploaded) also contain the fixes :)"
    author: "Isaac Clerencia"
  - subject: "kliks? Sarge .debs? Backports?"
    date: 2006-05-23
    body: "Are there packages for Sarge?\n\n\nIs there a plan to put packages into http://www.backports.org/ ??? \n\nThat would be great, 'cause it would prolly also mean there could be kliks, no?"
    author: "anonymous coward"
  - subject: "Re: kexi patch?"
    date: 2006-05-23
    body: "> So you have the funny situation that the Suse packages are better than the original source tar balls.\n\nCould you please stop publishing these theories, anonymous? Boudewijn Rempt has done good work by releasingthe stuff, and the patches come from me in the same day when the sources of problems were identified. One of the packages was worked out thanks to valuable Stephan's hints.\n\nThe announcement contains a link to the patches in \"issues\" section. \n\nPackagers are not working 24x7, if you're curious and there is not only KOffice in the world to package. On other world you might expect we should close our mouths and do not disclose the problem...\n"
    author: "jstaniek"
  - subject: "Re: kexi patch?"
    date: 2006-05-23
    body: "> Could you please stop publishing these theories, anonymous?\n\nThose are no theories but facts.\nUsing tar.gz => Kexi does not work\nUsing Suse RPMs => Kexi works very well\n\nAll in all: This is a large step for Koffice!\n"
    author: "Hans"
  - subject: "Re: kexi patch?"
    date: 2006-05-31
    body: "And using Suse, RedHat, Mandriva, Debian etc, etc RPMs/DEBs of the Linux kernel are in almost all cases better than the tar.gz of the same version at Kernel.org. Your point is what exactly? "
    author: "Morty"
  - subject: "Kexi Patches: Full Details"
    date: 2006-05-23
    body: "Kexi Patches: Full Details\n\nhttp://kexi-project.org/wiki/wikiview/index.php?PatchesForKexi1.0.1\n\n"
    author: "jstaniek"
  - subject: "Breezy packages?"
    date: 2006-05-23
    body: "Will the Kubuntu packages be backported to Breezy?"
    author: "Thomas Olsen"
  - subject: "Re: Breezy packages?"
    date: 2006-05-23
    body: "You mean: will someone create koffice packages for ubuntu breezy :o)"
    author: "AC"
  - subject: "Re: Breezy packages?"
    date: 2006-05-23
    body: "Exactly ;-) If I knew how to I would be glad to make them myself."
    author: "Thomas Olsen"
  - subject: "Re: Breezy packages?"
    date: 2006-05-24
    body: "One should suspect that if  Jonathan Riddell didn't make the packages, kdelibs in Breezy makes inviable to package koffice-1.5.1 for it..."
    author: "Humberto Massa"
  - subject: "Re: Breezy packages?"
    date: 2006-05-23
    body: "Or switch to Suse, they provide packages for Suse 9.2, Suse 9.3, Suse 10.0 and Suse 10.1. "
    author: "Hans"
  - subject: "Re: Breezy packages?"
    date: 2006-05-23
    body: "Kubuntu->SuSE?\n\nHuhu... scary\n\n:)"
    author: "me"
  - subject: "Re: Breezy packages?"
    date: 2006-05-23
    body: "Yeah, the scary part is you are using Kubuntu instead of Suse in the first place. That is scarrry."
    author: "Abe"
  - subject: "Re: Breezy packages?"
    date: 2006-05-23
    body: "/me looks at his laptop running kubuntu\n/me looks at his desktop running suse\n\nok. so am i, like, doubly screwed? ;)"
    author: "Aaron J. Seigo"
  - subject: "Releasing when there are RC bugs"
    date: 2006-05-23
    body: "The KDE (and particularly KOffice) guys seemed to have really got their marketing and public image worked out well in the last few months, but then I read this:\n\n\"a critical bug discovered too late in the release process to do anything about\"\n\nUmmm.... How can it be too late in the process to do anything about? Just don't send out the release announcement.\n\nIf you have known RC bugs in your release, it doesn't go out. That way you don't look stupid because you don't have to tell some people they should upgrade and others that they shouldn't.\n\n(Remembering why I use Debian...)"
    author: "StuP"
  - subject: "Re: Releasing when there are RC bugs"
    date: 2006-05-23
    body: "come on, many commerical products come out with critical bugs.... :D Look at XP, it's almost ready for release, according to debian standards ;-)"
    author: "superstoned"
  - subject: "Re: Releasing when there are RC bugs"
    date: 2006-05-23
    body: "WinXP probably needs another couple of years of bug fixing before it's ready for release ;)\n\nHere's a common point that's coming up time and time again just recently... this isn't a relative thing (\"at least we're better than M$/Adobe/insert-favourite-company\") this is sometimes about absolutes. \n\nYes, releasing KOffice with only 1 or 2 RC bugs is probably better than most M$ Office releases but it's still crap for the end user. (And have you ever seen a M$ press release advising users of Access not to upgrade to the latest release?) "
    author: "StuP"
  - subject: "Re: Releasing when there are RC bugs"
    date: 2006-05-23
    body: "> Ummm.... How can it be too late in the process to do anything about?\n> Just don't send out the release announcement.\n\nExactly. Could have been as simple as that.\n\nOr even better:\nAfter the bug has been discovered (patch has been available since May, 19.), apply the patch and send emails to the few packagers to redo the packages.\n\n\nPackages are available by now for Kubuntu and Suse. The Sue packages even include the Kexi fix. So only the Kubuntu people had to be emailed.\n\nBut someone was to lazy and thought \"Let's just release it without the bugfix\"..."
    author: "Hans"
  - subject: "Re: Releasing when there are RC bugs"
    date: 2006-05-23
    body: "Hey, Hans! Thanks for volunteering to be our next release manager. Let's see how you juggle a data-loss critical bug in a released version of KSpread versus waiting another week with released."
    author: "Boudewijn Rempt"
  - subject: "Re: Releasing when there are RC bugs"
    date: 2006-05-23
    body: "You by yourself asked \"I'm wondering if we \nshouldn't skip 1.5.1 altogether\". So I don't think that a one or two day delay would have caused much harm."
    author: "Hans"
  - subject: "Re: Releasing when there are RC bugs"
    date: 2006-05-23
    body: "Yes, and? That was one possibility. Discussion on IRC helped me make the decision to release anyway. Of course, today it turns out that the Kexi bug isn't a regression, so the warning wasn't necessary, and we could have released Friday anyway."
    author: "Boudewijn Rempt"
  - subject: "Re: Releasing when there are RC bugs"
    date: 2006-05-23
    body: "I must agree.\n\nWhat is it that causes this problem which has also occurred with KDE?\n\nOne of the advantages of OSS should be one of flexability in the release schedule.  However, there seeems to be some compulsion to kick releases out the door before they are 100% ready.  If this happens often, it will reflect poorly on the quality of our software."
    author: "James Richard Tyrer"
  - subject: "Re: Releasing when there are RC bugs"
    date: 2006-05-23
    body: "Yes. The \"some compulsion\" in this case was a grave bug in KSpread that would cause data loss for users. In the end, the kexi problem turned out to be not all that critical -- that we thought it was critical was just because of a misunderstanding.\n\nAll software will always be released with bugs. If you go for the rigid cmm level 1 quality standards the \"bug\" will be that the software by the time it's ready is no longer what the users need.\n\nIt's impossible to both release early and often and release only when ready -- you can do one or the other, but not both."
    author: "Boudewijn Rempt"
  - subject: "Re: Releasing when there are RC bugs"
    date: 2006-05-25
    body: "Sophistry does not solve quality issues.\n\nNor, does rhetorical devices and logical fallacies.\n\nWhy is it that we keep hearing such rationalizations rather than discussions of how to improve quality?\n\nWhether releasing \"early and often\" or only when the release is ready is not a valid dichotomy.  If you reduce this argument, you are advocating releasing before the release is ready -- clearly not a good idea.  This is NOT a binary proposition; there is plenty of ground in the middle between these extremes.\n\nWhen is a release ready to be released?  This is a good question for which there isn't a good answer.  However, it is easy to tell when a release has been released before it is ready."
    author: "James Richard Tyrer"
  - subject: "Re: Releasing when there are RC bugs"
    date: 2006-05-26
    body: "KSpread had a critical dataloss bug in the previous version, fixed in the new RC. The more the release is delayed, the more people run into the bug, and lose their data. Hence, it is a _very good idea_ to get the next release out as soon as humanly possible. That Kexi also had a critical-but-turned-out-not-to-be bug discovered complicates matters, but as I don't believe that one caused dataloss, it was the right decision to release ASAP. There is -nothing- more important than protecting your users' data (and security).\nIs this difficult to understand? It's certainly not as simple as the \"wait until every bug is fixed\" mantra."
    author: "illissius"
  - subject: "Re: Releasing when there are RC bugs"
    date: 2006-05-26
    body: "Why not releasing another RC?"
    author: "Dominik"
  - subject: "Krita - more than an simple icon editor?"
    date: 2006-05-23
    body: "from the Krita homepage:\n\n\"Now Krita is a capable image editor and a great platform for future development. The current release of Krita (version 1.5.0) has too many features to list them all:\"\n\nSorry but:\nHow can a program be called a \"capable image editor\" if it even lacks a usable blur or sharpen filter? If the save-as-JPEG dialog is only usable by try-and-error? If the auto-contrast filter creates clipped highlights? Those are basic features which should work!\n\nAre there additional plugins (Unsharp mask sharpening, adjustable gaussian blur,...)? I don't think so, as the Krita homepage does not contain the word \"plugin\". :-(\n\nBut all in all, Krita is a promising technology study. If they only would *include* some good plugins or at least good plugin-creation-HOWTOs. (Some time ago I tried to do an unsharp-mask-sharpening script by my own (very simple, just overlay an inverted gaussian blured version of the original image), but Krita even does not support gaussian blur, so even an own script was no help. Had to go back to gimp and digiKam...)\n\n"
    author: "max"
  - subject: "Re: Krita - more than an simple icon editor?"
    date: 2006-05-23
    body: "> How can a program be called a \"capable image editor\" if it even lacks a\n> usable blur or sharpen filter? If the save-as-JPEG dialog is only usable by\n> try-and-error? If the auto-contrast filter creates clipped highlights? Those\n> are basic features which should work!\n\nO.K., I try to improve the sharpen and blur filters.\n\n> But all in all, Krita is a promising technology study. If they only would\n> *include* some good plugins or at least good plugin-creation-HOWTOs. (Some\n> time ago I tried to do an unsharp-mask-sharpening script by my own (very\n> simple, just overlay an inverted gaussian blured version of the original\n> image), but Krita even does not support gaussian blur, so even an own script\n> was no help. Had to go back to gimp and digiKam...)\n\nActually, writing plugins for Krita is quite easy. A good starting point is the pixelize filter (it is in krita/plugins/filters/pixelizefilter). Just copy the files from the pixelizefilter in a new directory, rename them to whatever you like. The central function is KisPixelizeFilter::process where the actual filtering happens. Maybe I write a HOWTO if I find some time. "
    author: "Michael Thaler"
  - subject: "Re: Krita - more than an simple icon editor?"
    date: 2006-05-23
    body: "Blog about Krita plugins:\nhttp://cyrilleberger.blogspot.com/2006/05/krita-plugins-15-1-simple-colors.html\nhttp://cyrilleberger.blogspot.com/2006/05/krita-plugins-15-1-color-transfer.html\n\nExample plugin for Krita:\nhttp://websvn.kde.org/branches/koffice/1.5/koffice/krita/plugins/filters/example/"
    author: "cl"
  - subject: "Re: Krita - more than an simple icon editor?"
    date: 2006-05-23
    body: "We're not a \"they\" -- we're a \"you\". Which means that you can contact us and share your knowledge with us. You say the blur and sharpen filter are unusable -- have you tried coming up with a good description of what a usable sharpen and blur filter need to provide? Have you tried finding good algorithms for us to implement? There's a mailing list, there's irc. And you need to be in time: after feature freeze, nothing can be done. And the last time someone complained about the sharpen filter was after 1.5 had gone into feature freeze. Sorry, but we cannot change the GUI anymore after feature freeze.\n\nI am working on a plug-in howto, Cyrille has an unsharp mask filter and lots of other improvements in his krita-plugins pack, so there's progress, but if you care about improvements, you need to get involved. And that doesn't necessary mean coding; it just means approaching us and sharing your expertise. Random bits of opinion posted on the dot don't help."
    author: "Boudewijn Rempt"
  - subject: "Re: Krita - more than an simple icon editor?"
    date: 2006-05-23
    body: "> You say the blur and sharpen filter are unusable -- have you tried coming up\n> with a good description of what a usable sharpen and blur filter need to\n> provide? Have you tried finding good algorithms for us to implement?\n\nCome on, have you ever used the sharpen filter by yourself?\n\nIt's a simple sharpen filter without any adjustments possible and it's strength is *way* to strong. Good algorithm? Just look at any other image editor, it's called \"unsharp mask\".\n\nGaussian blur? Again, the needed adjustments are lacking. If you click on \"gaussian blur\" you expect a dialog with preview and a control to set the radius.\n\n> Sorry, but we cannot change the GUI anymore after feature freeze.\n\nThen maybe your feature freeze is too strict?! Adding one or two plugins should not cause any regressions. And If I remember correctly, an additional plugin-pack is in the pipeline, too.\n\n> Random bits of opinion posted on the dot don't help.\n\nSeems to help quite a bit :-)\n\n> Cyrille has an unsharp mask filter and lots of other improvements in his\n> krita-plugins pack\n\nWhere can I read more about it? Krita Homepage?\nI don't want to file several bugzilla-entries if Cyrille has working unsharp mask and gaussian blur filters.\n\n"
    author: "Hans"
  - subject: "Re: Krita - more than an simple icon editor?"
    date: 2006-05-23
    body: "> > Sorry, but we cannot change the GUI anymore after feature freeze.\n \n> Then maybe your feature freeze is too strict?! Adding one or two plugins\n> should not cause any regressions.\nTrue, but regressions isn't the only concern, adding plugins means adding strings to translate, and for the sake of translations team we can't do that either.\n\n> > Random bits of opinion posted on the dot don't help.\n> Seems to help quite a bit :-)\nI hope that we will see more bugs report ;) as for the adjustable blur and unsharp mask they were allready in krita-plugins svn for a month ;)\n\n> Where can I read more about it? Krita Homepage?\n> I don't want to file several bugzilla-entries if Cyrille has working unsharp > mask and gaussian blur filters.\nYou can read about them here (or when I blog about them on planetkde.org):\nhttp://websvn.kde.org/trunk/playground/graphics/krita-plugins/\nIt's not userfriendly, but less time consuming than managing a website ;)"
    author: "Cyrille Berger"
  - subject: "Re: Krita - more than an simple icon editor?"
    date: 2006-05-23
    body: "I seldom have time to actually use Krita you know -- I spend way too much time coding, packaging, writing plugin manuals. In any case, if you want me to take enthusiastic action, you had better be a little more friendly and courteous. And I have not changed my plans in any perceptible way by the ill-mannered comments on the dot: I am currently engaged in the depressing work of porting Krita (and KOffice) to Qt4 and that consumes all my time.\n\nIn any case since you seem to know such a lot about the way we work, it surprises me that you don't know why we have such a strict feature freeze: it is to enable the translators to do their work. Their life is difficult enough as it is. \n\nCyrille's cool plugin pack does contain unsharp mask, red eye removal and lots of other cool stuff, but it did already so before \"Max\" began to clamor for it. I seem to remember that work on unsharp mask was started because Larry on the Krita mailing list asked about it and helped us figure out what to do.\n"
    author: "Boudewijn"
  - subject: "Re: Krita - more than an simple icon editor?"
    date: 2006-05-23
    body: "> And I have not changed my plans in any perceptible way by the ill-mannered\n> comments on the dot: I am currently engaged in the depressing work of\n> porting Krita (and KOffice) to Qt4 and that consumes all my time.\n\nWhen KDE arrives, I'm sure 99% of all KDE users will have Qt3 around for some time. There are just too many third party applications which are based on KDE3 and/or Qt3.\nCompatibility with KDE3 and Qt3 apps is a necessity for KDE4.\n\nSo from a user's perspective it's not very important if Krita is a qt4 or qt3 application. (I know that Krita is unfortunately part of the Koffice project... which makes those things more complicated).\n\nSo if porting Krita it Qt4 consumes all your time it could be that nobody will be interested in Krita when KDE4 finally has arrived.\nWhy not attract contributors and developers with a usefull and polished Krita 1.5.x and 1.6?\n\nDon't take me wrong, I don't want to tell you what to do, but I have seen, that I did contribute to DigiKam but not to Krita. Why? DigiKam complies with \"release early, release often\".\n\nFrom a user's perspective it's just much more worthwhile to improve DigiKam's working unsharp mask filter (now it's possible to use a very large radius (>50 pixel) to enhance the contrast of an image) than to file wishlist items for lacking basic features.\n\nKrita could attract many users and developers with a good 1.6 release (or 1.5.1 + additional plugins + scripting/plugin Howto)."
    author: "Hans"
  - subject: "Re: Krita - more than an simple icon editor?"
    date: 2006-05-24
    body: "There will be a 1.6 release, which will be out in october if I remember correctly.  Not all of the KOffice components will have added features in 1.6, but fast development of Krita was one of the driving reasons for us adding a 1.6 release at all."
    author: "Inge Wallin"
  - subject: "Re: Krita - more than an simple icon editor?"
    date: 2006-05-26
    body: "> So from a user's perspective it's not very important if Krita is a qt4 or\n> qt3 application. (I know that Krita is unfortunately part of the Koffice\n> project... which makes those things more complicated).\n \n> So if porting Krita it Qt4 consumes all your time it could be that nobody\n> will be interested in Krita when KDE4 finally has arrived.\n\nShocking arrogance, rudeness, and ignorance are always such a delightful combination. Please, keep up these helpful posts---they really boost the spirits of all who are donating massive amounts of their time to create useful software and giving it to us for free!\n\nHave you perchance followed the near-death of GnuCash because of its foundation on GTK1? And signs of its rebirth given that the GTK2 port is nearing completion? I for one say B.R. and others seem to know what they're doing, and am grateful for the wonders they are achieving.\n"
    author: "anon"
  - subject: "Re: Krita - more than an simple icon editor?"
    date: 2006-05-26
    body: "Heh. I had forgotten about GNUCash. I think KMyMoney is better since it evolved faster (and is runner-up in the Sourceforge awards this year!) as it seems many KDE apps do. IIRC GNUCash made the unfortunate decision of using Scheme as its scripting language. Like aRts, it probably seemed like a good idea at the time."
    author: "a.non"
  - subject: "Re: Krita - more than an simple icon editor?"
    date: 2006-05-23
    body: "I have only three words to answer \"report, report, report\". How are we to guess what the users need if they don't tell us ? So go to bugs.kde.org, fill report, or contact us on kimageshop@kde.org. For instance, for the unsharp mask filter, someone speak about it on the lists, it was too late for the 1.5 release, but in a few days I had it to krita-plugins.\n\n1) for the save as jpeg dialog, I am looking for bug entry, and I can find none related to that, as it is boring job I am waiting for a few people to vote for it...\n2) for the adjustable blur, we should be ashame not to have included it earlier, it was a 5 minutes work... and it will be solved in a few day when I find the time to release krita-plugins\n3) for the plugin HOWTOs, there is one who is included in the krita sources, true, it's a little bit hidden and not complete, but once again we need some input about it to improve it (krita/doc/Developing Krita Plugins.odt for the reference)\n\nSo please, contact us directly either through bugs.kde.org or our lists, it's the best way to help us improve krita."
    author: "Cyrille Berger"
  - subject: "Re: Krita - more than an simple icon editor?"
    date: 2006-05-23
    body: "I'm sorry but the answer \"please tell us about bugs\" works only so far. In my experience people will only report bugs on apps which already look good and behave mostly properly. But below a certain quality threshold, they just won't bother, because if the app looks this bad already, it seems pointless to tell the dev about glaring problems which show up after 5mns of testing.\n\nGiven the abysmal level of user-friendliness of Gimp, I really hope Krita will be a suitable replacement someday, however I'm not holding my breath. So far, trying to use it has been an extremely frustrating experience. It's still very alpha IMHO, and should not be released."
    author: "Guillaume Laurent"
  - subject: "Re: Krita - more than an simple icon editor?"
    date: 2006-05-23
    body: "Yes that is true, but on the other hand a lot of people take the time to comment on the dot or in a blog, where the tracking of such information is very difficult instead of taking the same amount of time to report."
    author: "Cyrille Berger"
  - subject: "Re: Krita - more than an simple icon editor?"
    date: 2006-05-23
    body: "Bugreports and wishlist entries take far more time than comments on the dot..."
    author: "Hans"
  - subject: "Re: Krita - more than an simple icon editor?"
    date: 2006-05-23
    body: "and yet comments on the dot will be forgotten tomorrow while reports on bugs.kde.org are archived properly.\n\nmoreover, it takes a lot of time to fix the bugs. usually much more than filing them. perhaps you could invest a tiny amount of your time to allow the developers to effectively use their time."
    author: "Aaron J. Seigo"
  - subject: "Bugzilla for basic functionality? - NO!"
    date: 2006-05-24
    body: "You don't need bugzilla to remember that Krita needs usable sharpen and blur filters. Those are just basics. If those filters become included into Krita, people will start to write bugreports about details. But not about those basics. That's the real world.\n"
    author: "max"
  - subject: "Re: Bugzilla for basic functionality? - NO!"
    date: 2006-05-24
    body: "Maybe.  But also in the real world the people who don't exercise their right to vote usually complain the most.  They certainly have the right to complain, but chances are things won't change.  If you want change, vote.  Same as here.  You certainly have the right to not report what you consider as basics, and you have the right to complain.  But if people don't report their concerns via proper channels, they can't EXPECT their concerns to be addressed.  That's my .02 cents."
    author: "cirehawk"
  - subject: "Re: Krita - more than an simple icon editor?"
    date: 2006-05-24
    body: "I am not a programmer, just a user.  But I would be willing to bet that developing this program takes a heck of a lot  more time than making bugreports and wishlist entries.  So if you are going to criticize the developers efforts, at least go through the recommended channels for making issues known.  To do otherwise is \"lazy\" and just comes across as complaining.\n\nNow if developers are unresponsive to bug reports, then that's different.  But that doesn't seem to be the complaint."
    author: "cirehawk"
  - subject: "Re: Krita - more than an simple icon editor?"
    date: 2006-06-24
    body: "Shut up and show us the code\nhttp://slashdot.org/features/98/10/13/1423253.shtml"
    author: "Matej Cepl"
  - subject: "Re: Krita - more than an simple icon editor?"
    date: 2006-05-23
    body: "In the middle of all this love, I would like to say:\n\nThank you for Krita.\n\nI have been using it for simple tasks in the last month, and it surely works for me. And by following the efforts of the Krita gang, in the create mailng list (cross platform / toolkit standards for creative apps), in the colorspace area, putting pressure on the Gimp guys, I like you even more. It is not only a great app, it is a great team.\n\nCheers, and don't worry about the naysayers!\n\nAmadeo"
    author: "Amadeo"
  - subject: "Re: Krita - more than an simple icon editor?"
    date: 2006-05-24
    body: "I second this. Just upgraded to ko 1.5.1 and krita has really improved. The tools I use most are now implemented and I'm very satisfied with the UI. I was always unhappy with the gimp and its UI. Krita now does all I need. The gimp wil stay on my computer but I'll use Krita as it just starts up instantly whereas the gimp takes 5 secs to come up. \n\nThanks for Krita!"
    author: "Thomas"
  - subject: "Thanks for your work!"
    date: 2006-05-23
    body: "Dear KOffice developer,\n\nthanks a lot for all your efforts! It is getting better and better, and this is wonderful.\n\n@Hans: Shut up, you lousy demanding taker for free, and get some friend finally, or you will end very lonely at the edge!"
    author: "Officer"
  - subject: "KOFFICE RULES!"
    date: 2006-05-23
    body: "KOffice is great.\n\nWhat isn't great is the amount of bad comments on this article. If you found a bug, file it. If you want to just complain about life in general, go to slashdot or osnews or send your buddies at microsoft an email."
    author: "KOfficeFan"
  - subject: "KOffice is quite nice."
    date: 2006-05-23
    body: "I am skeptical about releasing it with RC bugs, but I trust the devs to make the right choice. Krita's progress has been remarkable and I'm hoping that many of these filters are provided by plugins ala GIMP. I have to chime in requesting a howto somewhere. At the risk of being off-topic, the SoC results are supposed to be in today. When will we find out how KDE fared?"
    author: "sundher"
  - subject: "Re: KOffice is quite nice."
    date: 2006-05-23
    body: "I'm writing a howto... Maybe I should release it before it's done? About soc: Google is going to write the applicants real soon now, and after that, I guess the dot story won't be long."
    author: "Boudewijn Rempt"
  - subject: "Re: KOffice is quite nice."
    date: 2006-05-23
    body: "Nah, release it when it's done and maybe announce it on the dot? That way this sort of flamewar can be averted and no one will complain that the Krita documentation is incomplete or no one made an announcement about it. :) Thanks for the info about SoC and thanks for all your work on Krita. Its progress in the past year has been nothing short of amazing. :-D"
    author: "sundher"
  - subject: "Re: KOffice is quite nice."
    date: 2006-05-24
    body: "> I'm writing a howto... Maybe I should release it before it's done?\n\nOf course. The sooner people can start to write scripts and plugins the more popular Krita will become. Release early, release often!"
    author: "max"
  - subject: "Re: KOffice is quite nice."
    date: 2006-05-24
    body: "What, and have you complain it's only half-complete, inaccuarate and caused you to lose data/time/whatever.  Man, you seem to be an irony free zonee...\n\nJohn."
    author: "Odysseus"
  - subject: "Very good sign"
    date: 2006-05-24
    body: "Koffice and Krita seem to be hitting a critical mass.\n\nStages of development:\n\n1. Great idea sees first implementation. Diehard users, developers and family members with no other choice use software and assist in development.\n\n2. Project progresses to the point of being useful. Users who have watched start using, instantly finding limitations, unfinished features. Comments made are positive, hinting that we may have an (insert well established software here) killer.\n\n3. Project attracts developers who are able to add features easily, and fix bugs. Project hits walls due to lack of infrastructure, mature organisation, possibly limitations in design. Initial enthusiasm carries everything through.\n\n4. Project starts becoming genuinely useful to users, ie. some actually replace other software and use project extensively. Again limitations show up; unfinished or unpolished features, bugs and regressions, organizational teething pains. Comments made tend to be negative, showing a deepening investment in the project, where it's limitations are genuinely painful to the now dedicated users.\n\n5. Project reaches maturity. Not perfect, but very useful. Further development focusses on finish and fit that only can be perfected by use.\n\nLooks like Krita and KOffice are at #4.\n\nWell done everyone. Remember that the negatives come from people who deeply care. Also remember the enormous work (usually given freely) that has gone into the project already. \n\nDerek"
    author: "Derek Kite"
  - subject: "I do agree about Krita"
    date: 2006-05-24
    body: "I already replaced gimp by krita. It has improved a *LOT* after koffice 1.4, it's a good tool that works well for most parts (I still don't like the way selection works) and just do the job.\nUnfortunally I can't say the smae for kwrite, I'm still need to use openoffice for that."
    author: "Iuri Fiedoruk"
  - subject: "Re: I do agree about Krita"
    date: 2006-05-25
    body: "Sorry, I don't believe you. What have you done with gimp? Painting? Then Krita might be a good replacement. But I doubt that anyone doing image manipulation already can replace gimp by Krita. Plugins are lacking, Filters, export filters, ..."
    author: "Min"
  - subject: "Re: I do agree about Krita"
    date: 2006-05-25
    body: "Well, you can belive me yes. :)\nIf you read the original message you will notica #4 is *some users starting to do some work*. It isn't *a lot of users doing some serious work*.\nI'm not using krita as photoshop replacement, I'm just using it as image cuting, resizing, painting yes.\nI'm not a designer or artist, I just need it work some small but usefull (to me work)."
    author: "Iuri Fiedoruk"
  - subject: "Re: I do agree about Krita"
    date: 2006-05-25
    body: "Krita might not have the community that GIMP does but I think Krita's real strength is that its base is strong and its interface isn't a usability nightmare. It already supports 16 bit color and CMYK whereas GIMP is tangled in code that won't let it upgrade from 8. Plugins and filters come with time and community involvement. On a funnier note, Krita's raindrops filter has been ported to GIMP.\n\nhttp://maurits.wordpress.com/2006/05/23/gimp-09-progress/"
    author: "anon"
  - subject: "Re: I do agree about Krita"
    date: 2006-05-25
    body: "That's not gimp, but gimp-sharp -- I never knew such a project existed, I'll check it out."
    author: "Boudewijn Rempt"
  - subject: "Re: I do agree about Krita"
    date: 2006-06-04
    body: "GIMP# (aka gimp-sharp) is not a replacement nor a rewrite of GIMP. It's actually two things: firstly an OO framework which makes it easy to derive new plug-ins with minimal effort. And secondly it wraps the complete GIMP API in C#.\n\nWe also look at other programs like Paint.NET and of course Krita to learn from. GIMP is a very powerful program, but writing plug-ins for it is not much fun. And I should know, because I've written a few :)\n\nWe ported the Raindrops filter because it was very straightforward to do so (from C++ to C#) and we were also interested to learn about the performance differences between the two languages.\n\n(And although I come from the GIMP side, I think Krita is an absolutely marvelous program with a great future ahead!)"
    author: "Maurits Rijk"
  - subject: "If you are not satisfied, make a patch"
    date: 2006-05-24
    body: "I'm sorry but I have to be some kind of rude, with people which are always complaining about.\nHey you don't even pay a cent for this software, and you are allowed to change it to meet your needs. Every project in opensource is made by contributions, people coding at night and working all day, and there's no project with enough human resources to fill all the features, so sometimes things take a lot of time to get on point.\n\nI'm talkig as a user only, but when there's something really annoying I try to fix it, as you can see here: https://bugs.kde.org/show_bug.cgi?id=121337\n\nI don't have skills at C++ and QT (but I will learn for sure), and I've found one way to fix it.\n\nSo what I suggest is read this page http://www.kde.org/jobs/ , and apply!!!\n\nKDE wants you... and needs you!!!"
    author: "Kanniball"
  - subject: "Re: If you are not satisfied, make a patch"
    date: 2006-05-24
    body: "I am seriously and desperately looking for job. I am located in <> and would like to work from home. I have been using linux only for 5 months.\nI know C language , x86(nasm) , C++(never used extensively) . I have gone thru QT documents briefly. \nMost of my programming has been on ms platform. \nAt present i am THINKING of writing bittorrent client for linux platform, which will help me , hopefully get a job ??? . \nI need advice , how should i go about it , should i first join the community and work with them or should i go alone. also how do these great talented respected koffice devlopers make there living? \nI have limited capabilities , will not touch anything related to web or html or xml(until no option left) , but if need a lowlevel protocol communicae , i will do it.\nPlease give me or advise me a small project , which atleast be of some use in kde environment.\nThank you all \n\n\n\n"
    author: "KJOB"
  - subject: "Re: If you are not satisfied, make a patch"
    date: 2006-05-24
    body: "If you're new to the KDE technologies, I really recommend helping out an existing project, rather than try to start from scratch.\n\nHave a look at a project you like, and think of ways to improve it.  Read the mailing list for a bit.  Look at the bugs that are filed against it.  Pick something that looks easy enough, either a bug or a feature you really want.  Try it.  Ask for help when you're stuck.\n\nYou won't have your own application on your CV, but you will be able to show-off those essential team-working skills!\n\nOh, and have fun!"
    author: "mart"
  - subject: "Re: If you are not satisfied, make a patch"
    date: 2006-05-24
    body: "I seriously doubt creating a bittorrent client for Linux will get you a job: free software development experience can be a pro, with some employers, but especially telecommute software development jobs are very thin on the ground and it tend to be the very best developers who can get one. Oh, and expect that most developers will wonder whether you will be spending enough energy on the job if you're also deeply involved in free software.\n\nAs to how I make my living: I've got a 40-50 hour day job coding custom applications, some in Java, some in Javascript, some in Python. None of the KOffice developers is exactly paid to work on KOffice, though two are sponsored to do work on KDE/KOffice."
    author: "Boudewijn Rempt"
  - subject: "Re: If you are not satisfied, make a patch"
    date: 2006-05-24
    body: "Thanks for your sincere advise both mart, Boudewijn Rempt .\nI think i'll join the mailing list , and will check if i can be of any help there and work up from there and what will happen will happen. \nThanks all. \nExcuse me for the offtopic.\nbye\n\n\n\n "
    author: "KJOB"
  - subject: "Re: If you are not satisfied, make a patch"
    date: 2006-05-25
    body: "> At present i am THINKING of writing bittorrent client for linux platform\n\nAnother? KTorrent is the best, and there are also others for Linux."
    author: "Anonymous"
  - subject: "Re: If you are not satisfied, make a patch"
    date: 2006-05-24
    body: "When I use a software with bugs, I don't waste my precius time fixing it, I use a better one instead."
    author: "OK_Boy"
  - subject: "Re: If you are not satisfied, make a patch"
    date: 2006-05-24
    body: "Have fun using your better software."
    author: "cirehawk"
  - subject: "krita reviewed"
    date: 2006-05-25
    body: "Hi,\n\nI have tested Krita 1.5 for ebv4linux:\nhttp://www.ebv4linux.de/modules.php?name=News&file=article&sid=31\n(Sorry, German only)\n\nKrita got 5 from 10 points. Deeply missing are selection or alpha layers, basic dynamic and color filters. For multi-monitor usage it would be nice to dock the separate tear-off tool windows together. More useful than the current filter gallery would be a preview of different parameters of a single filter. Scripts seems not to have a GUI option. \nKritas good features are CMS support, RAW support, tablet support, multi color space painting engine and scripting/in process controlling.\n\nBye\n\n  Thorsten \n\n"
    author: "Thorsten Schnebeck"
  - subject: "Re: krita reviewed"
    date: 2006-05-25
    body: "Um... Selections _are_ layers, basically. I'm not sure what color filters are missing: there are color adjustment filters that work on any channel. You can use PyQt to QtRuby to provide a gui in your scripts without problems. Not sure what \n\nDocking of tool windows outside the main window is dependent on what Qt offers. I am not going to spend time working around or against the toolkit, life is too short for that.\n\nAnd for the good points, don't forget the adjustment layers. They didn't work all that well in the first beta, but are quite reliable in 1.5.0."
    author: "Boudewijn Rempt"
  - subject: "Re: krita reviewed"
    date: 2006-05-25
    body: "Hi Boudewijn,\n\nCan I save selections into layers? Can I switch them on and off, to use them when I need it?\nMissing filters: white balance, level(!), contrast, brightness, HSV control.\nKrita has a (BTW wrong named) gradation tool but its far more handy to have sliders for contrast, gamma, saturation etc and not only generic gradation filters for channels. Krita has some auto tools to stretch the levels but manual control rules. The color tool has only 8bit selections range independet to the used color space.\n\nAdjustment layers (AL) are named in the review but they would be better, if you can combine them with an alpha mask. I'm not sure if AL scales UI-wise. If I write a cool filter as Kross-Script can I use it as AL? Or are AL only for build-in functions?\n\nAs I follow Kritas development since years I know that the basic are now there and working but there are still some interface problems to present the user the needed function.\n\nPlease go one. Krita is on a good way.\n\nBye\n\n  Thorsten "
    author: "Thorsten Schnebeck"
---
The KOffice team today released the <a href="http://www.koffice.org/releases/1.5.1-download.php">first bug-fix release</a> in their 1.5 series. Critical bugs in KSpread, KWord and Krita were fixed, thanks to the helpful input of our users. We also have updated languages packs. You can read more about it in the <a href="http://www.koffice.org/releases/1.5.1-release.php">press release</a> and the <a href="http://www.koffice.org/announcements/announce-1.5.1.php">full announcement</a>. A <a href="http://www.koffice.org/announcements/changelog-1.5.1.php">full changelog</a> is also available. Currently, you can download binary packages for <a href="http://kubuntu.org/announcements/koffice-151.php">Kubuntu</a> and <a href="http://download.kde.org/stable/koffice-1.5.1/SuSE/">SUSE</a>. 




<!--break-->
