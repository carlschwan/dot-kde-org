---
title: "Mark Shuttleworth Becomes the First Patron of KDE"
date:    2006-10-16
authors:
  - "dallen"
slug:    mark-shuttleworth-becomes-first-patron-kde
comments:
  - subject: "How?"
    date: 2006-10-15
    body: "Even after reading the info provided at the link, I still don't know how to apply.  I also found no way to get at this info from the main KDE web page.  Is it intentionally hard to give money?"
    author: "Louis"
  - subject: "Re: How?"
    date: 2006-10-15
    body: "Click the \"help us continue to serve the KDE community\" link on the story above. It's detailed there and in the rules of procedure.\n\nAlso note that you can easily donate money (of any amount whatsoever) via PayPal. Just click \"Donations\" on the KDE e.V. webpage. This information is also linked from \"Supporting KDE\" on the main website (http://www.kde.org/support/)."
    author: "Thiago Macieira"
  - subject: "Re: How?"
    date: 2006-10-16
    body: "I had trouble paying for my aKademy accommodation this year because I don't have a credit card anymore, and in the end the e.V. paid. I was going to pay them back via a donation, but when I went to the e.V. page it was in German - maybe I missed the English one. I have a Maestro debit card, but it's surprisingly useless for actually paying for anything via the web. If you have a paypal account how do you put money into it without a credit card - is it possible?"
    author: "Richard Dale"
  - subject: "Re: How?"
    date: 2006-10-16
    body: "If you enter some details about your bank in your Paypal account options, you should be able to transfer money in directly from your bank account and then pay."
    author: "Paul Eggleton"
  - subject: "Re: How?"
    date: 2006-10-16
    body: "I guess you currently have an account at a Spanish bank, which means both your bank as well as the one of e.V. are on the Euro zone and are not allowed to take higher fees for transfers into other Euro countries than they would charge for transfers within the country.\n\nThis usually means there is not transfer cost.\n\nSee the section labelled \"Money Transfer\" on the e.V. site"
    author: "Kevin Krammer"
  - subject: "Re: How?"
    date: 2006-10-16
    body: "No, it's not DETAILED there.  How do I apply?  Where is the form?  Who do I contact?  There are important questions unanswered or very vague."
    author: "Louis"
  - subject: "Re: How?"
    date: 2006-10-16
    body: "It's all explained here:\n\nhttp://ev.kde.org/corporate/supporting_members.pdf\n\nMark, thank you very much. I believe KDE 4 will bring a complete revolution to the unix/Linux desktop and financial resources are key to making it happen.\n\nI also hope to see Kubuntu receive enough structural support to be able to deploy it with confidence. As brilliant as Jonathan is, Kubuntu still feels too much like a one-man show (and yes I know about the other contributors). Right now, Ubuntu sees much more Q/A than Kubuntu and it would be nice to see Kubuntu given a chance to rise and shine, so to speak.\n\nI have no doubt that you will do the right thing once you actually see the true potential that KDE4 could bring.\n\nYou are a true visionary. The world needs more people like you."
    author: "Gonzalo"
  - subject: "Re: How?"
    date: 2006-10-16
    body: "Again, I have read this document.  It does not tell me how or where to get an application.  Am I the only one who actually read it, or am I missing something that is obvious to everyone else.  Can someone give me a step-by-step or something?  Maybe I'll just give up (as I'm sure many others will without a bit more clarity and simplicity).  Good luck with this in its current state."
    author: "Louis"
  - subject: "Re: How?"
    date: 2006-10-16
    body: "You're right. The specific information on who to contact is missing.\n\nThat should be the KDE e.V. board (kde-ev-board@kde.org)."
    author: "Thiago Macieira"
  - subject: "Re: How?"
    date: 2006-10-16
    body: "Thiago, Louis is right here. It's detailed in the document, but there says \u00abusing the form provided by the KDE e.V.\u00bb, and there is not any form in the linked websites (I suppose that it will be added soon, though)."
    author: "Anonymous Coward"
  - subject: "Yay"
    date: 2006-10-15
    body: "Mark Shuttleworth, the KDE community thanks you for your support! By the way, you're pretty cute!"
    author: "AC"
  - subject: "Re: Yay"
    date: 2006-10-16
    body: "Yay too.\n\nMark is one is the guys who we can trust to only help KDE and not abuse it, only a pity, he chose Gnome back then, but time will make both more equal righted in Ubuntu I guess.\n\nKay"
    author: "Debian User"
  - subject: "Re: Yay"
    date: 2006-10-19
    body: "When Mark originally chose Gnome one of the primary driving factors was the 6-month release schedule (which *Ubuntu syncs with). He actually switched to using KDE/Kubuntu on his own machines two releases ago. So no worries for KDE users, \"sabdfl\" is one of you."
    author: "Anonymous Coward"
  - subject: "$$$$$"
    date: 2006-10-16
    body: "In the economic aspect I am in agreement with the designation of Mr. Shuttleworth, but in the ideological one and the development of free software in general I do not believe that it contributes much."
    author: "NeoCalderon"
  - subject: "Re: $$$$$"
    date: 2006-10-16
    body: "Everyone comes to the table with something; development skills, ideas, organising talents or money.\n\nWhat concerned me with Mr Shuttleworth was his ability to use his resources to apply his vision beyond what anyone else could do. Hire 25-30 key developers and say this is what I want to do. Pick favorites, exclude others to their detriment.\n\nWhat has happened is a complement to Mr Shuttleworth; he hasn't done harm, in fact his contribution has improved free software.\n\nDerek"
    author: "dkite"
  - subject: "Re: $$$$$"
    date: 2006-10-16
    body: "I disagree. I think he has done way more to free software than just contributing money. It's one thing to spend money for 20 sports cars or 10 houses or to spend it for a vision. He is one of the few who has a vision and if Africa as a whole and many other developing countries should ever come to substantial wealth it will be through cheap IT infrastructure. It may sound cynical but in the long term this is even more important than bread and medicine because it gives people a chance to earn enough money to buy bread and medicine for themselves. IT infrastructure does absolutely nothing to solve problems right now or in the very near future though so I know this is often frowned upon. To spend money on the Ubuntu vision is therefore far more visionary than we can usually expect from millionaires. Please consider again why he chose the African word \"ubuntu\" for his distro.\n"
    author: "Martin"
  - subject: "Re: $$$$$"
    date: 2006-10-16
    body: "That's a refreshingly different view.\n\nI guess time will show you right or wrong,\nbut it's good to realize there are different\napproaches to solving a problem, even ones that\naren't immediately obvious."
    author: "Darkelve"
  - subject: "Re: $$$$$"
    date: 2006-10-17
    body: "Maybe a different view outside of Africa, but a fairly common one shared on the continent. Which is why those of us who live here very much grok some of what Mark is trying to achieve.\n\nIt's back to the old saw of giving somebody fish versus teaching them how to fish. Except, of course, that Mark is trying to go beyond that in that he's also trying to teach Africans how to make their own rods and, if need be, dig their own fishing ponds.\n"
    author: "Eug\u00e9ne Roux"
  - subject: "Thanks Mark!"
    date: 2006-10-16
    body: "Thanks! Not just for supporting KDE but for everything you're doing for the open source world!"
    author: "Joergen Ramskov"
  - subject: "First patron, yeah?.. Hope not the ONLY one"
    date: 2006-10-16
    body: "Being both a Christian and a KDE user, I pray Mr. Shuttleworth does not decide to make a \"KDE Christian Edition\" or something... :-/"
    author: "Trueash"
  - subject: "Re: First patron, yeah?.. Hope not the ONLY one"
    date: 2006-10-16
    body: "Errr... Am I the only one not getting this comment? What has Mark becomming a KDE Patron to do with Christianity?"
    author: "Andr\u00e9"
  - subject: "Re: First patron, yeah?.. Hope not the ONLY one"
    date: 2006-10-16
    body: "Patron can also refer to a patron saint, which is a Christian concept.\n\nThat's not the intended context here, but the other poster might not know about the other possible meanings of patron"
    author: "Kevin Krammer"
  - subject: "Re: First patron, yeah?.. Hope not the ONLY one"
    date: 2006-10-16
    body: "There is also a Chistian Ubuntu. It is a pointless distro, so as a Christian and Ubuntu user, I agree."
    author: "What"
  - subject: "Re: First patron, yeah?.. Hope not the ONLY one"
    date: 2006-10-16
    body: "Christian Ubuntu is not an official Ubuntu distribution.\n\n"
    author: "Reply"
  - subject: "Re: First patron, yeah?.. Hope not the ONLY one"
    date: 2006-10-17
    body: "Hard to tell, looking at the name."
    author: "Terracotta"
  - subject: "Re: First patron, yeah?.. Hope not the ONLY one"
    date: 2006-10-16
    body: "There already is one based on Kubuntu: http://www.ichthux.com/\nNot sure why anybody would have problems with this in Free software world."
    author: "Lure"
  - subject: "Re: First patron, yeah?.. Hope not the ONLY one"
    date: 2006-10-16
    body: "Not sure why anybody would have problems with this in Free software world....\n\n Eh! .-) Just read the negative comments here then\n\n\nhttp://www.newsforge.com/article.pl?sid=05/11/03/1643243&tid=31\nhttp://software.newsforge.com/software/06/04/14/1535251.shtml\nhttp://lxer.com/module/forums/t/22421/\nhttp://lxer.com/module/forums/t/22448/\n\nto have an idea. It's funny how much lack of thinking and bigotry is hidden even\nin the most apparently \"progressive\" and tolerant communities.\n\n"
    author: "Christian KDE user"
  - subject: "Re: First patron, yeah?.. Hope not the ONLY one"
    date: 2006-10-17
    body: "Dear Christian KDE user.\n\nThe ideals of justice and equality predate christianity. They shall also postdate it. May I suggest that you leave the narrow confines of your religion to one side and let software heads get on with software. How dare you criticise parts of this community as being bigoted and lacking in thinking. I am willing to lay my life on the line that the people who gravitate towards free software are a lot more tolerant than your average christian. Damn you to hell and your rabbit-ear quotes round progressive. Get back to your church and pray to the lord that he'll bless you with some enlightenment.\n\nOh by the way if you infer a note of condemnation from this post be sure to add it to your ever increasing list of \"people who made negative comments about my stupid beliefs\" \n\nAll the best, \n    A. Non. Believer"
    author: "anon y mouse"
  - subject: "Re: First patron, yeah?.. Hope not the ONLY one"
    date: 2006-10-19
    body: "Hmm... I can't tell... is this a joke? It's a bit too strong for someone to seriously believe. (I think?) But some of the comments are a bit nasty.\n\nI did appreciate the irony of \"damn you to hell\" though. :)"
    author: "Joe"
  - subject: "Re: First patron, yeah?.. Hope not the ONLY one"
    date: 2006-12-22
    body: "If you wonder why he might put quotation marks around progressive or tolerant, look at your post.  A hypocrite is you."
    author: "?"
  - subject: "Re: First patron, yeah?.. Hope not the ONLY one"
    date: 2007-08-30
    body: "That was uncalled for. As an agnostic Christian, KDE user, and Ubuntu user, I damn you to nil!\n\nNow to get back on topic... I think that this is a good thing. Shuttleworth is friggin' rich!"
    author: "Tuna"
  - subject: "Re: First patron, yeah?.. Hope not the ONLY one"
    date: 2006-10-19
    body: "Parhaps bigotry is the most common thing in this world."
    author: "Rocco"
  - subject: "Re: First patron, yeah?.. Hope not the ONLY one"
    date: 2007-09-09
    body: "Well there's certainly no shortage of it!"
    author: "Tuna"
  - subject: "More KDE in Edubuntu?"
    date: 2006-10-16
    body: "I hope that Mark will push to include more KDE when needed in *buntu distributions. For example Edubuntu is based on Gnome but includes all of KDE-Edu. That makes Edubuntu developers a bit unhappy. They find kde-i18n packages too big for example (and they are big). Now that Mark is a patron, he can open more dialog between KDE-Edu and Edubuntu in order to \n1) not duplicate work for the sake of 1 widget library \n2) find solutions to include all packages that teachers need (1 CD is very restricted for shipping an educationaldistribution) kde-i18n packages can also be stripped to keep only relevant translations.\n3) make freedesktop.org progress to be sure that all software support the same standards and thus happily live together\n\nannma"
    author: "annma"
  - subject: "Thank You Mark!!!"
    date: 2006-10-16
    body: "Thank You! You are the Patron of so many more things than KDE of course :-)"
    author: "Dmitry"
  - subject: "Dear Mark"
    date: 2006-10-17
    body: "Dear Mark ,\nI couldn`t express with words ,how much I appreciate ,what you`ve done for the free software , you`ve opened the gates to the world of linux to me ,and not just me ,to many people like me.It`s not just the money ,that makes this special ,keep in mind that kde wasn`t succesfull because ot money ,it was succesfull ,because the concept ,the idea ,that it was based on.I admire both kde and kubuntu ,I hope that this will be a start of a good friendship between the two.\n\nKeep up the good work.\n\nA kubuntu linux user"
    author: "DjDarkman"
  - subject: "$5000 a year to be a patron"
    date: 2006-10-25
    body: "and why is everyone so enamoured of this guy ?\n"
    author: "vm"
  - subject: "Re: $5000 a year to be a patron"
    date: 2008-01-08
    body: "Reading his bio (http://www.markshuttleworth.com/biography) is enough for me to understand why many people would be enamoured by him.  The guy has done a lot more than I, or most of us, to affect people; and his philanthropic outlook is refreshing, even inspiring when coupled with the drive and talent he had displayed.\n\nIf you're questioning his motives, or his ability I think you'd be hard pressed to make a case.  If you're jealous, you should either accept that you choose to assist the world in a more humble way (, which is not a bad thing,) or perform great public feats yourself."
    author: "Stephen E. Baker"
---
There are many ways to support and contribute to the KDE project. Traditional contributions have seen the fusion of code, artwork, translations and documentation that has come to define KDE. As the project celebrates its 10th anniversary, we can all reflect on the enormous success achieved when people and organisations with a vast variety of different backgrounds and skills join forces.  For people and organisations who wish to contribute to KDE by providing financial support in an ongoing manner, the <a href="http://ev.kde.org/">KDE e.V.</a> now offers <a href="http://ev.kde.org/supporting-members.php">the new Supporting Members scheme</a>.  KDE e.V. is both excited and proud to announce Mark Shuttleworth, founder of Canonical, as our first Patron of KDE. 












<!--break-->
<div style="float: right; border: thin solid grey; padding: 1ex; margin: 1ex; width: 250px">
<a href="http://www.flickr.com/photos/foobarbaz/141521711/"><img border="0" src="http://static.kdenews.org/jr/mark-shuttleworth-linuxtag-2.jpg" width="250" height="374" /></a><br />
Mark at LinuxTag earlier this year
</div>

<p><a href="http://www.canonical.com/">Canonical Ltd</a> is the company behind the wildly-successful <a href="http://www.ubuntu.com/">Ubuntu</a> family of GNU/Linux distributions, including the KDE-specialised <a href="http://www.kubuntu.org/">Kubuntu</a>.  Our first supporting member was announced at the <a href="http://dot.kde.org/1160834616/">10 years anniversary party</a>.</p>

<p>Mark says of his patronage of KDE, "<em>With the growing importance of Kubuntu within the Ubuntu family, the time is right to support the project that makes Kubuntu possible - KDE. For this and many other reasons, I am very proud to become the first of hopefully many Patrons of KDE.</em>"</p>

<p>Patrons of KDE may display the "Patron of KDE" logo on their website and any other material for as long as they are a Patron of KDE and will be listed on the KDE e.V. website, if they so wish. This is the highest level of membership available within KDE e.V., and will allow KDE e.V. to continue its work supporting and maintaining the structures of development.</p>

<p>Of course, aside from financial matters, sponsors of KDE are a vital part of the vibrant community outreach and relations scheme - feedback from all our supporters helps to shape our shared goals and future development.</p>

<p>KDE e.V. wishes to thank all its <a href="http://www.kde.org/support/donations.php">current supporters</a>, and would like to invite all interested parties to <a href="http://ev.kde.org/supporting-members.php">help us continue to serve the KDE community</a>.</p>











