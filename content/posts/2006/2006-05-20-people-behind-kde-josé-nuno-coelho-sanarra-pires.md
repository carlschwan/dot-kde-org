---
title: "People Behind KDE: Jos\u00e9 Nuno Coelho Sanarra Pires"
date:    2006-05-20
authors:
  - "jriddell"
slug:    people-behind-kde-josé-nuno-coelho-sanarra-pires
comments:
  - subject: "Not only GUI translation is green..."
    date: 2006-05-19
    body: "That's awesome! 100% of the KDE _docs_ translated to Portuguese: http://l10n.kde.org/stats/doc/stable/pt/index.php"
    author: "jstaniek"
  - subject: "Just one thing..."
    date: 2006-05-19
    body: "Just one thing... thanks Nuno Pires and Pedro Morais for the great job!\n100% KDE translations should be very dificult to translate and to maintain :-)\n\nAs a portuguese KDE user, thanks again!"
    author: "Carlos Gon\u00e7alves"
  - subject: "You guys  rule!!!"
    date: 2006-05-20
    body: "Major congrats again to Pedro and Jos\u00e9 you guys rule :)"
    author: "pinheiro"
  - subject: "Where's the hideout place?"
    date: 2006-05-20
    body: "Hi! Is there any portuguese LUG?\n\nCheers,\nPedro Alves\n"
    author: "Pedro Alves"
  - subject: "Re: In development"
    date: 2006-05-20
    body: "Yes, but we are still developing it.\nYou can contact us if you want.\n\nCarlos Gon\u00e7alves: carlos@pinguix.com\nNuno Pinheiro: nuno@oxygen-icons.org"
    author: "Carlos Gon\u00e7alves"
  - subject: "KDE Rulez"
    date: 2006-05-23
    body: "Keep the good work guys you rock. Since the beginning of my experience with Linux and with the GUI KDE I've seen in almost every program your translations. I'm a translator too, helping in the Ubuntu community, since I've never understand how to translated the KDE packages, maybe it's my laziness or less free time that I've got but's let's try to learn something new.\nCongratulations for your good work.\nA very happy KDE user"
    author: "Jos\u00e9 Paulo Matafome Oleiro aka Matafome"
  - subject: "Re: KDE Rulez"
    date: 2006-05-23
    body: "Hey!\nCheck this: http://kde-pt.homelinux.org/"
    author: "Carlos Gon\u00e7alves"
  - subject: "Re: KDE Rulez"
    date: 2006-05-28
    body: "Carlos you're always on the front line. Keep helping the Linux users, as you do always.\nThanks for your help, when I've got problems with my Vodafone Mobile Connect Card."
    author: "Jos\u00e9 Paulo Matafome Oleiro aka Matafome"
---
KDE's Portugese translation is showing an impressive amount of green in the <a href="http://l10n.kde.org/stats/gui/trunk/pt/index.php">translation statistics</a>.  To find out more about the man who has helped make our winning translation team <a href="http://people.kde.nl/">People Behind KDE</a> interviewed <a href="http://people.kde.nl/jose.html">José Nuno Coelho Sanarra Pires</a>, a 'mood chamelion' who has the best name out of any KDE developer we've interviewed so far.

<!--break-->
