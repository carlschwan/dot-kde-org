---
title: "Trolltech Releases Preview of Qt for Java"
date:    2006-07-28
authors:
  - "jriddell"
slug:    trolltech-releases-preview-qt-java
comments:
  - subject: "Classpath"
    date: 2006-07-28
    body: "And does it support Gnu Classpath/GCJ?"
    author: "funbar"
  - subject: "Re: Classpath"
    date: 2006-07-28
    body: "The preview is based on JDK 1.5 from Sun and we require quite a bit of the 1.5 language features to work, like generics for type safe signal emittions. We intend to make it work but Gnu Classpath and GCJ, but we haven't begun that work yet."
    author: "Gunnar Sletta"
  - subject: "GCJ with Qt already exists"
    date: 2006-07-28
    body: "Bernhard Rosenkraenzer from ArkLinux has already made a version of GCJ that draws graphical objects using Qt."
    author: "anon"
  - subject: "Re: GCJ with Qt already exists"
    date: 2006-07-28
    body: "That is a very different topic.  Using Qt as a toolkit for drawing the native version of Swing calls is not what we're talking about here."
    author: "anonymous"
  - subject: "Re: Classpath"
    date: 2006-07-28
    body: "> And does it support Gnu Classpath/GCJ?\n\nNo it does not. The original Java is faster, more stable and more feature complete than classpath"
    author: "max"
  - subject: "Re: Classpath"
    date: 2006-07-28
    body: "> No it does not. \n\nMind if I believe the previous poster, who is the main author, over your black/white opinion?\n\n> The original Java is faster, more stable and more feature complete \n> than classpath\n\nNone of which is relevant, and a sentence like this is only spreading Fear Uncertainty and Doubt (FUD). Please refrain from trolling over an off topic subject.\n\nThanks."
    author: "Thomas Zander "
  - subject: "Re: Classpath"
    date: 2006-07-28
    body: "<I>None of which is relevant, and a sentence like this is only spreading Fear Uncertainty and Doubt (FUD). Please refrain from trolling over an off topic subject.</I><P>The feature completion is in fact very relevant, as you can see from the first response. It's not a question of FUD when it is sadly very true that open source java is far behind sun java. It's a problem that we need to solve, and crying FUD only gets in the way."
    author: "mikeyd"
  - subject: "Re: Classpath"
    date: 2006-07-29
    body: "> The feature completion is in fact very relevant, as you can see from the first response.\n\nYes, feature completion is relevant, but nobody except Gunnar went into features, everybody else was just talking about works/doesn't work. The QT Java bindings need Java 1.5 and GCJ just doesn't support that yet (gcj-eclipse branch will lead to that). GNU Classpath is on 99.32% Java 1.4 compliance and was over 90% on generics branch for a long time now (but the test suite on http://www.kaffe.org/~stuart/japi/ is down right now {there was talk about integrating generics branch into head some time ago, maybe that's it}) and Google SOC is sponsoring a project to plug one of the biggest missing chunks of code, javax.management.*.\nSo if you ask me, by the time GCJ can handle Java 1.5 GNU Classpath should be able to handle the QT Java bindings too.\n\n> It's not a question of FUD when it is sadly very true that open source java is far behind sun java.\n\nWhat exactly do you expect? Java is a Sun programming language.\n\nAbout GCJ being slower then Suns Java implementation, that is irrelevant. There is lots of potential in GCJ for speed that isn't exhausted right now and there is some interesting embedded work going on were I don\u0092t see Suns Java implementation going. \n"
    author: "Anon Imous"
  - subject: "Re: Classpath"
    date: 2006-07-29
    body: "http://www.kaffe.org/~stuart/japi/htmlout/h-jdk14-classpath.html\n99.32%\n\nArchive.org \n* Oct 2004: 67.25%\n* Mar 8 2005: 77.38%\n* end of march 78%\n\nhttp://www.kaffe.org/~stuart/japi/htmlout/h-jdk15-classpath.html\n92%\n\nhttp://www.klomp.org/mark/classpath/LinuxTag2006.odp\nhttp://developer.classpath.org/mediation/ClasspathPresentations\nhttp://developer.classpath.org/mediation/ClasspathShowcase\nhttp://developer.classpath.org/mediation/FreeSwingTestApps\n\n\n\n"
    author: "funbar"
  - subject: "Re: Classpath"
    date: 2006-07-29
    body: "> The original Java is faster, more stable and more feature complete\n> than classpath\n\n>>None of which is relevant, and a sentence like this is only spreading Fear >>Uncertainty and Doubt (FUD). Please refrain from trolling over an off topic >>subject.\n\nOf course it's relevant and true.  Just because it doesn't jibe with your political ideology is what is irrelevant.  You need to stop trolling."
    author: "ac"
  - subject: "Web Java applets"
    date: 2006-07-28
    body: "May this ease the integration of Java applets in Konqueror? Currently it uses Lesstif/Motif for display..."
    author: "Thomas"
  - subject: "Re: Web Java applets"
    date: 2006-07-28
    body: "No, it won't. Applets can't use Qt unless its on a company intranet or something where everyone can have the library installed."
    author: "Ian Monroe"
  - subject: "Re: Web Java applets"
    date: 2006-07-29
    body: "Corrected: Applets can't use Qt unless you have the library installed.\n\nNobody said anything about company intranets being a prerequisite for installing the library.\n"
    author: "Trejkaz"
  - subject: "Re: Web Java applets"
    date: 2006-07-30
    body: "I think it only uses Lesstif/Motif if the Applet use AWT instead of Swing. So it's not up to Konqueror how Applets will be displayed."
    author: "Iridias"
  - subject: "Where is Qt for .NET?"
    date: 2006-07-28
    body: "Bindings for C#/.NET/Mono were great ..."
    author: "Hans"
  - subject: "Re: Where is Qt for .NET?"
    date: 2006-07-28
    body: "Read the FAQ.  These bindings show that they are possible and Trolltech will probably do them if a business case makes sense."
    author: "anonymous"
  - subject: "interesting name"
    date: 2006-07-28
    body: "This is nice news.  I don't really care much for Java personally, but I can see the advantages of having Qt Java bindings, and how others find this as a welcome addition.  \n\nOff topic a little, but it's an interesting product name.  Jambi is also a name of a song on the new Tool album.  Also a province in Indonesia.  So I am not sure I get how they came up with it."
    author: "Latem"
  - subject: "Re: interesting name"
    date: 2006-07-28
    body: "\"So I am not sure I get how they came up with it.\"\n\nJambi is the island just to the northwest of Java."
    author: "Brandybuck"
  - subject: "Re: interesting name"
    date: 2006-07-29
    body: "nitpicky_mode_on:\nSumatra is the island. Jambi is a province in Sumatra.\n"
    author: "ariya"
  - subject: "Re: interesting name"
    date: 2006-07-29
    body: "http://ariya.blogspot.com/2006/07/jambi-and-java.html"
    author: "ariya"
  - subject: "Good, but not convinced it is easier to program wi"
    date: 2006-07-28
    body: "I have programmed Java for many years, and just read through the whitepaper. I think it is a good thing Qt is available for Java - it will make the KDE environment richer over time, when people can contribute with Java apps. However, I am not convinced the signal-slot mechanism is better than the listener pattern used by Swing etc.\n\nThis example is given in the whitepaper. The second argument is ugly in my eyes - a String with code in it?\n\nslider.valueChanged.connect(spinBox, \"setValue(int)\");\n\nThat the Qt example is shorter does not matter - the number of lines of code is irrelevant. \n\nI am not convinced that Qt does offer better tools for the programmer. However, hopefully the result for the user is still worth it - Qt apps have a better \"feel\" than Swing apps for the most time. \n"
    author: "claes"
  - subject: "Re: Good, but not convinced it is easier to program wi"
    date: 2006-07-28
    body: "This is not true. Code lines matter. If you have less code, there is less code to maintain and the probability of bugs is reduced. And the signal-slot mechanism of Qt is WAY better than the ugly listener pattern in Java. The Listener pattern is just a hack around the missing feature of real events in Java. Nevertheless, the Qt solution is not type-safe which is not so good. A type-safe signal-slot mechanism would be the best solution, but this had to be built into the language I guess, so Trolltech will not be able to fix this."
    author: "Jan"
  - subject: "Re: Good, but not convinced it is easier to progra"
    date: 2006-07-28
    body: "Java has the ability to reference a method through its reflection API.  I wonder why this wasn't used - my guess it so that the interface better matches the C++ interface."
    author: "AlfredNilknarf"
  - subject: "Re: Good, but not convinced it is easier to progra"
    date: 2006-07-28
    body: "The Qt mechanism *is* typesafe. It's not statically typed, but that's something entirely different. And most certainly it uses Java`s reflection API."
    author: "ac"
  - subject: "Re: Good, but not convinced it is easier to progra"
    date: 2006-07-29
    body: "Java's way of providing access to methods using reflection is through java.lang.Class.getMethod(String name, Class argsTypes ...). This means that to get to value(int) you would have to write\n\ntry {\n    button.connect(x, QSlider.class.getMethod(\"value\", Integer.class);\n} catch (Exception e) {\n    // Handle exception\n}\n\nWe felt that the \"value(int)\" approach was nicer because its less to type and we can give constructive exceptions like NoSuchSlotException which is a runtime exception, so you don't have to explicitly catch it for every connect statement.\n\nHad Java provided a way of getting function references directly, like they have with the .class operator we would of course have used it. In lack of such an operator the string is a good alternative. At least it worked pretty good for Qt for the last 12 years ;-)"
    author: "Gunnar Sletta"
  - subject: "Re: Good, but not convinced it is easier to progra"
    date: 2006-07-29
    body: "The listener pattern is one of the most clumsy and awkward parts of the Java language. Indeed, it injures an already overly verbose language. The signal-slot mechanism is an elegant solution which is infinitely better than writing event-listeners in Java."
    author: "gfranken"
  - subject: "Re: Good, but not convinced it is easier to progra"
    date: 2006-07-29
    body: "Any particular reason you think it is awkward and clumsy? Using the listener pattern (aka Observer) really allows you to split your business logic from your view. Combine observer with Mediator in a Java app and you have a loosly coupled, easily maintainable app.\n\nIt is indeed annoying when people put their ActionListener implementation in the same class as their view and differentiate with strings in an else/if to determine which button/menu was used but that is the problem of the programmer, not Java. (Hint: search for \"How to use Actions\" on Google for a much better way).\n\n"
    author: "Mike"
  - subject: "Re: Good, but not convinced it is easier to progra"
    date: 2006-07-30
    body: "The use of listener classes and the use of the MVC pattern are two entirely different things. Listeners are a method of communication between code modules. They can be used in the MVC pattern to connect events in the view to the controller, but so can other methods such as Delphis method pointers or C#'s delegates. However listener classes make it more difficult to use the MVC pattern because they dramatically increase the boiler-plate code necessary to connect a GUI event to a controller's handler. \n\n"
    author: "Bryan Feeney"
  - subject: "Re: Good, but not convinced it is easier to progra"
    date: 2006-07-28
    body: "The only significant difference between this and the C++ API in this case is that C++, thanks to its preprocessor, can beautify the fact that what you're passing in is a String. If I'm not mistaken (and I may well be), the call SIGNAL( setValue(int) ) would simply be turned into a String, as above, by a preprocessor macro."
    author: "Antonio"
  - subject: "Re: Good, but not convinced it is easier to program wi"
    date: 2006-07-28
    body: "The \"string\" you mention is indeed a string. It isn't a method call, it is the *name* of a method call, so the QObject dispatcher can get the message to the right place at run time. The SLOT() macro creates the string in C++, but Java doesn't have a preprocessor.\n\nIf the string bothers you, don't think of it as code, think of it as a name. In the example above, you're sending an int value to the slot named \"steValue(int)\"."
    author: "Brandybuck"
  - subject: "Re: Good, but not convinced it is easier to progra"
    date: 2006-07-29
    body: "You have just demonstrated a problem with this, spelling. :)\n\nSo if you get the method name wrong, what happens then? I guess there is no way to find out at compile time?"
    author: "someone"
  - subject: "Re: Good, but not convinced it is easier to progra"
    date: 2006-07-29
    body: "Right.  That is the only problem you could possibly find with this.  Lack of compile time errors for typos.  I don't think that is a big price to pay for such a powerful and concise tool like signals/slots, but to each his own."
    author: "manyoso"
  - subject: "Re: Good, but not convinced it is easier to progra"
    date: 2006-08-01
    body: "I hope you're not one of those \"if it compiles then ship it\" type of people! Most of us out here in the real world at least check to see if it runs first. And when it runs we get a warning message about an unknown slot."
    author: "Brandybuck"
  - subject: "Good, but not convinced it is easier to program wi"
    date: 2006-07-28
    body: "I have programmed Java for many years, and just read through the whitepaper. I think it is a good thing Qt is available for Java - it will make the KDE environment richer over time, when people can contribute with Java apps. However, I am not convinced the signal-slot mechanism is better than the listener pattern used by Swing etc.\n\nThis example is given in the whitepaper. The second argument is ugly in my eyes - a String with code in it?\n\nslider.valueChanged.connect(spinBox, \"setValue(int)\");\n\nThat the Qt example is shorter does not matter - the number of lines of code is irrelevant. \n\nI am not convinced that Qt does offer better tools for the programmer. However, hopefully the result for the user is still worth it - Qt apps have a better \"feel\" than Swing apps for the most time. \n"
    author: "claes"
  - subject: "who cares?"
    date: 2006-07-29
    body: "yeah... but who cares about java?\nafter all its not free."
    author: "hannes hauswedell"
  - subject: "Re: who cares?"
    date: 2006-07-29
    body: "\nI care."
    author: "Henry"
  - subject: "Re: who cares?"
    date: 2006-07-29
    body: "> yeah... but who cares about java?\n> after all its not free.\n\nThere's a free implementation which won't take much longer anymore to be a complete replacement for the official Sun Java, and that one is also going to be open sourced sometime. There's a huge army of Java programmers, and it's really better to have them in than to tell them to do C++ instead."
    author: "Jakob Petsovits"
  - subject: "Re: who cares?"
    date: 2006-07-29
    body: ">yeah... but who cares about java?\n>after all its not free.\n\nJava is more free than Qt."
    author: "ac"
  - subject: "Re: who cares?"
    date: 2006-07-30
    body: "hahahahahahaha,\n\nAh, needed a good laugh.  Which part of the GPL makes QT less free than Sun's 'the source is closed' Java, of Kaffes GPL Java?\n\njohn."
    author: "Odysseus"
  - subject: "Re: who cares?"
    date: 2006-07-30
    body: "Hahaha, I'm talking about the real world and not your little fantasy GPL world.  "
    author: "ac"
  - subject: "Re: who cares?"
    date: 2006-08-01
    body: "Out here in the real world we had just slightly over a decade when distributing Java or JRE for FreeBSD was illegal. If you wanted to run someone's Java app you had to bootstrap and build your own Java first. But even with commercial Qt you're still allowed to distribute the complete runtimes with your app."
    author: "Brandybuck"
  - subject: "What about making Suns Java a fix part of KDE?"
    date: 2006-07-29
    body: "As far as I know Java will soon be real open source software and I think that KDE could benefit greatly from supporting Java. There really is a vast army of Java developers out there and many of the appreciate open source software like I do. A main advantage of Java/Eclipse over C++/KDevelop or plain C is that greatly improves your productivity as a developer. I've personally worked first with GTK under C which seriously sucked and then with KDevelop which was quite okay for me. When I started to work for a new company I was forced to switch from KDevelop to Eclipse and I must admit that this was a very good decision. I'm now able to implement in the same time twice the functionality than I did before and that with far fewer bugs too. It's really fun to work with Java. In recent years Java wasn't well known for it's desktop applications. The applications were slow and looked pretty awkward. That has drastically changed with Java 5 and will even more with Java 6. Under Windows Java applications look perfectly integrated and on a modern PC you won't notice the speed difference to a native application. Unfortunately, this doesn't work as well under KDE. Currently Java applications use this ugly metal look. To make things worse I believe that the next Java will use GTK to draw the widgets. Anyway, I think it would be a great chance for KDE to fully embrace Java. \n\n    "
    author: "Anon"
  - subject: "Re: What about making Suns Java a fix part of KDE?"
    date: 2006-07-29
    body: "It uses Metal by default but you can change the look and feel. There is a very nice looking and free one called Looks at:\n\nhttp://www.jgoodies.com \n"
    author: "Mike"
  - subject: "Re: What about making Suns Java a fix part of KDE?"
    date: 2006-07-29
    body: "Don't be to quick to dismiss KDevelop.  We plan on having excellent Java support for KDevelop 4.  Jakob has already written us a great parser for the language."
    author: "manyoso"
  - subject: "Re: What about making Suns Java a fix part of KDE?"
    date: 2006-07-29
    body: "How about IntelliJ support?"
    author: "Mike"
  - subject: "Re: What about making Suns Java a fix part of KDE?"
    date: 2006-07-29
    body: "Ignore the above post, was meant to be a response to another post...can't seem to delete/edit posts here."
    author: "Mike"
  - subject: "Pretty much irrelevant"
    date: 2006-07-29
    body: "This might have made a difference 3 or 4 years ago, but at this time it won't make much difference.  Mustang will finally have decent native L&F and SWT/JFace/RCP is already well established.  You have to pay Trolltech for non-GPL compatible usage, and open source desktop developers have for all practical purposes ignored Java.  Sorry, Trolltech, you're late to market. "
    author: "ac"
  - subject: "Re: Pretty much irrelevant"
    date: 2006-07-30
    body: "Hahaha, is this Mr. Gartner himself or just another frustrated nay sayer? What's your source for \"decent native L&F\" for Mustang? And if Java has such well established rich client GUI technologies, where are the mainstream rich client applications that use it?\n\nSwing is far away from native L&F, it's designed to be a Java GUI, not to bridge different native GUIs. SWT is nicer, but very simplistic compared to Qt, and at this point far from being cross platform. It's a pure Windows technology, with some 2nd class experimental ports. Run Eclipse on Linux and see for yourself."
    author: "yaac"
  - subject: "Re: Pretty much irrelevant"
    date: 2006-07-30
    body: "\"What's your source for \"decent native L&F\" for Mustang?\"\n\nA recent Mustang build on my hardrive\n\n\"And if Java has such well established rich client GUI technologies, where are the mainstream rich client applications that use it?\"\n\nHehe, and Qt won't change that either.  You inadvertantly admitted it's a waste of time.\n\n\"Swing is far away from native L&F, it's designed to be a Java GUI, not to bridge different native GUIs.\"\n\nYou're clueless.  There's emulation of native L&F and actual heavyweight components for native L&F, and then there's a middle ground like what Mustang is using to use some of the native APIs to get a L&F.\n\n\"SWT is nicer, but very simplistic compared to Qt, and at this point far from being cross platform.\"\n\nWhat's the point of your lies?\n\n\"Run Eclipse on Linux and see for yourself.\"\n\nI have and it runs fine."
    author: "ac"
  - subject: "Re: Pretty much irrelevant"
    date: 2006-07-31
    body: "Relax, people have very different understandings of \"decent\" or \"fine\". If Mustang's \"native\" L&F is good enough for you, than I might fully believe that for you Eclipse runs fine on your 64bit Linux machine, or on your Mac. \n\nUsing some native APIs for painting is not enough for L&F. It isn't even enough for L alone (think geometry management), but it definitely isn't enough for F. It might be good enough for you, but it can hardly fool Microsoft power users.\n\nThe Wwing in Mustang will probably be good enough for some in-house tools, but for retail applications? Time will tell. Java is big enough a market for more than one solution."
    author: "yaac"
  - subject: "Re: Pretty much irrelevant"
    date: 2006-08-01
    body: "Java won't be used for retail applications regardless of whether there are Qt bindings.\n\nOnce again, nobody cares about Java Qt bindings.  Windows people will just be using .NET and open source desktop people just don't use Java."
    author: "ac"
  - subject: "Weird, seems like a forced idea"
    date: 2006-07-29
    body: "Why put in sooo much effort to enter a saturated market when there are other languages and platform dying for real GUI support?\n\nI mean, java has AWT, SWING, SWT, Eclipse RCP, GTK bindings, etc, etc\n\nIf Trolltech put in the same effort at all to support QtRuby or PyQt, it would quickly become the default GUI development system.\n\nJust my $0.02\nBen"
    author: "Ben"
  - subject: "Re: Weird, seems like a forced idea"
    date: 2006-07-30
    body: "It's a pure business decision, the Java market is way bigger than Ruby and Python put together. Getting a few percentages of the Java market(the closed source part) will generate more revenue, in the same way regular Qt does(The saturated market comment was used when that was introduced too). \n\nBesides, the existing Python bindings for Qt are already widely used and mature. Making any inroad there harder, and they will essentially compete against themselves. While the existing Ruby bindings don't have the track record in stability and completeness of it's Python counterpart, it has come a long way and it's still improving. \n "
    author: "Morty"
  - subject: "Re: Weird, seems like a forced idea"
    date: 2006-08-02
    body: "\"While the existing Ruby bindings don't have the track record in stability and completeness of it's Python counterpart, it has come a long way and it's still improving.\"\n\nI think the Qt3 version of QtRuby is complete, stable and directly comparable with PyQt. They are both pretty much feature complete and cover the KDE apis too. It makes more sense to compare the Qt4 versions of PyQt and QtRuby, and I get the impression that they are at a similar stage.\n\nNote that Ruby book sales have just surpassed perl book sales, after overtaking python sales a few months ago, so don't underestimate the coming popularity of the language...\n\n-- Richard"
    author: "Richard Dale"
  - subject: "my 2 cent"
    date: 2006-07-31
    body: "i agree too little too late in the sence of cookies in the cookie jar, it is a great idea dont get me wronge.. just a few years behind like said, 4 or even 2 years ago would have been key but the fact that they are doing ti now is a) a Great idea and B)Well Over due!"
    author: "anon2"
  - subject: "What about the KDE libraries"
    date: 2006-08-01
    body: "Will KDE also provide the KDE libraries in Java. When will I use KPrinter from Java. "
    author: "Mike Tammerman"
  - subject: "Qt Jambi"
    date: 2007-10-02
    body: "I found it to be good looking, but slower than other toolkits.\nHopefully this will mature over time.\n\nNote to those \"not interested in Java\", omfg, can you please leave this forum, why are you posting such comments if you are not even using it? It's like me leaving an opinion in a Cobol forum... just a waste of space and time (yes, the entire continuum...)\n"
    author: "Developer too"
---
Trolltech has released a preview of the long awaited Java bindings for Qt 4.  "<em>Qt Jambi technology integrates Qt with the Java programming language, providing new possibilities for both Java and C++ programmers.  This technology enables Java developers to take advantage of the powerful features of Qt from within Java Standard Edition 5.0 and Java Enterprise Edition 5.0.</em>"  More information on <a href="http://www.trolltech.com/company/newsroom/announcements/press.2006-07-14.7914556269/">the Jambi press release</a> and tech details in the <a href="http://www.trolltech.com/developer/downloads/qt/jambi-whitepaper-a4">Jambi whitepaper</a>.  To get your copy sign up to the <a href="http://www.trolltech.com/jambitechpre">preview licence</a> (final release <a href="http://www.trolltech.com/developer/knowledgebase/faq.2006-07-21.1262524505/">will be also available under an open source license</a>) and <a href="http://www.trolltech.com/jambitechpre-download">download</a>.  There is an <a href="http://www.trolltech.com/developer/faqs/Qt Jambi">FAQ</a>
and Trolltech wants to get your feedback on the <a href="http://lists.trolltech.com/qt-jambi-interest/">qt-jambi-interest</a> list.
<b>Update:</b> Trolltech developer Gunnar Sletta provides <a href="http://blogs.qtdeveloper.net/archives/2006/07/28/qt-jambi-preview-released/">more details in his blog</a>.

<!--break-->
