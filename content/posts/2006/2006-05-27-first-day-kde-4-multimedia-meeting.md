---
title: "First Day KDE 4 Multimedia Meeting"
date:    2006-05-27
authors:
  - "j(superstoned)"
slug:    first-day-kde-4-multimedia-meeting
comments:
  - subject: "amaroK 2"
    date: 2006-05-27
    body: "I'm heartly and vibing waiting for amaroK 2 =D\nKeep up the free bar!"
    author: "etherea`"
  - subject: "Re: amaroK 2"
    date: 2006-05-27
    body: "Yeah, especially since I might want a free beer or two!"
    author: "Someone"
  - subject: "Re: amaroK 2"
    date: 2006-05-27
    body: "Until Amarok is not a part of core kde multimedia package I will not use it."
    author: "Artem S. Tashkinov"
  - subject: "Re: amaroK 2"
    date: 2006-05-27
    body: "It isn't part of the core kde multimedia package. So cheers."
    author: "Ian Monroe"
  - subject: "Re: amaroK 2"
    date: 2006-05-27
    body: "amaroK will never be part of the core kde multimedia package. It is in extragear/multimedia because the developers don't want to follow the KDE release cycle.\nI don't see any reason why that would be bad. You get bug fixes fast, and the number of new features between releases is not overwhelming you (like in some core KDE apps)."
    author: "Firetech"
  - subject: "Re: amaroK 2"
    date: 2006-06-02
    body: "I wish Kopete had done things that way, and stayed out of kdenetwork.  Sometimes things change too fast with the protocols it uses, and full KDE releases don't come often enough."
    author: "MamiyaOtaru"
  - subject: "Re: amaroK 2"
    date: 2006-05-27
    body: "When Amarok becomes a part of the core kde multimedia package I will stop using it (and KDE).\nKDE should become a Desktop Environment (hence the name KDE) and not a monster package consisting of a desktop environment and a gazillion of additional applications.\n\nIf it's too hard to install third party applications then this problem has to be resolved, but bloating KDE is no solution.\n"
    author: "max"
  - subject: "Re: amaroK 2"
    date: 2006-05-27
    body: "I totally agree with you. Furthermore, I really hate to do an apt-get install kde and have installed for example kpilot, kstart, kalzium and a hundrer of other apps that I dont want. I am no saying that this apps sucks, they are great, but I dont want them. It would be grate to do an apt-get install kde and have only the desktop environment... then, any other app that one would like could be installed manually... Even all the games are installed, that are not utils o functionality apps... or the education apps... these are apps that most of us dont use..."
    author: "Erne"
  - subject: "Re: amaroK 2"
    date: 2006-05-27
    body: "> It would be grate to do an apt-get install kde and have \n> only the desktop environment...\n\nTry Kubuntu. You can install and deinstall every single application via apt. E.g. I kept three games I like and removed the rest without any problems."
    author: "Ascay"
  - subject: "Re: amaroK 2"
    date: 2006-05-27
    body: "I don't know what distro you are using, but in all likelihood, that \"kde\" is a metapackage that is meant to do exactly what it is doing: install ALL OF KDE.\n\nYou probably can instal lpiecemeal if you want (of course, then you can complain that you must choose between hundredds and hundreds of packages).\n\nAnd after all... what's the problem with installing all of KDE? What does it cost you? 1GB of disk? So, about .35 euros? If it takes you 10 minutes to sort out what packages you want, you are already losing money."
    author: "Roberto Alsina"
  - subject: "Re: amaroK 2"
    date: 2006-05-28
    body: "Or maybe you have to figure out how your package manager works, kde is a meta package, so is kdebase, but a much smaller one that just \"includes the nucleus of KDE, namely the minimal package set necessary to run KDE as a desktop environment\", http://packages.debian.org/unstable/kde/kdebase\nhttp://packages.debian.org/unstable/kde/kde shows what the kde meta package consists of."
    author: "Peppelorum"
  - subject: "Re: amaroK 2"
    date: 2006-05-27
    body: "Sorry but I  do not share your point of view. \nSince I am starting to use Linux (SUSE 8.0) I found KDE a very good graphic desktop manager, even the many additionals ( Kword and Co.). I am still using it, fascinating from the development, improvements, functionality and stability reached. \nI am not aware if amaroK 2.0 will become a part of the KDE core or not, if the functionality, stability, improvements and eye-candy of all this system will remain better than today, then welcome. By the way I am a happy user of amarok 1.4 \n\nI can only say keep this good work, showing all the world that the main improvements for the IT are comming from the open source comuniy. \n\nJuan Ehrenhaus"
    author: "Juan Ehrenhaus"
  - subject: "thanks for the report superstoned"
    date: 2006-05-27
    body: "Interesting to see the entity known as superstoned on this forum is a real live KDE contributor! =)"
    author: "KDE User"
  - subject: "Re: thanks for the report superstoned"
    date: 2006-05-27
    body: "ehm... well, at your service ;-)"
    author: "superstoned"
  - subject: "timeline?"
    date: 2006-05-27
    body: "When will KDE4 be out?  The project has been too quiet about this!  Is it hard to port KDE3 to Qt4?  Why is this taking so long?  It will be bad if Qt5 is out before KDE4 is done."
    author: "curious"
  - subject: "Re: timeline?"
    date: 2006-05-27
    body: "The first public preview release (aka alpha) is scheduled for October, 2006. It's been announced on .kde recently.\n\n> Is it hard to port KDE3 to Qt4?\n\nIt's not that really hard, the real problem is that KDE4 will bring many new truly exciting technologies meant to ease and improve your desktop experience (not to forget about OpenGL based desktop). Just look through KDE4 goals at kde's wiki: http://wiki.kde.org/KDE+4+Goals\n\n> It will be bad if Qt5 is out before KDE4 is done.\n\nBe sure Qt5 will come out in the near future. I foresee Qt5 will not come before the end of 2006."
    author: "Artem S. Tashkinov"
  - subject: "Re: timeline?"
    date: 2006-05-27
    body: "Any Software engineer will tell you that doing two things at once is a bad idea.\n\nIt was suggested that we first port KDE-3.5.x to Qt4 and release KDE-4.0.0.\n\nUnfortunately, those that wanted to do two things at once and make large changes to KDE for KDE-4.0.0 won the argument so that is what we are trying to do.\n\nI still fear that this will cause large problems.  However, I hope to be proven wrong."
    author: "James Richard Tyrer"
  - subject: "Re: timeline?"
    date: 2006-05-27
    body: "if you want KDE 3.5 to be binary compattible to the other KDE 3.x releases (and yes, that's what KDE is commited to do) you can't port to Qt 4. And KDE 4.1 needs to be binary compattible to 4.0, if not - developers can't use it and trust their work will be worth anything after a year or 2.... So you can't refactor big things after KDE 4.0.\n\nWhat you say might be right - but it is not like KDE has a choice - if they want to keep developers on board..."
    author: "superstoned"
  - subject: "Re: timeline?"
    date: 2006-05-28
    body: "Perhaps come clarification is needed.\n\nWhat I, and others, suggest is that KDE-3.5.x be ported to QT-4 thus creating KDE-4.0.0.  This could also include fixes for known problems in KDELibs which would break binary compatibility if fixed since this is the only time when they can be fixed.  But, the radical new changes should be delayed till the port was completed.\n\nThen after this was up and running, that new features that are added to create KDE-4.1.0."
    author: "James Richard Tyrer"
  - subject: "Re: timeline?"
    date: 2006-05-27
    body: "Dear JRT, what would users gain from having a KDE 4.0 which basically is a KDE 3.5? Not much. Even worse, independant KDE apps would not run due to binary incompatibility.\n\nAnd if you have a close look at the transitions happening you will find that developing of KDE 4 is indeed done as two steps in series, first porting to Qt 4/KDElibs 4 and then developing new features/frameworks. The only thing that is left out is a release of the ported state, as the value/cost relationship is way too low.\n\nSo please stop making uninformed claims (and foremost stop doing so as self claimed engineer, for sake of your fellow engineers' reputation)..."
    author: "Else"
  - subject: "Re: timeline?"
    date: 2006-05-28
    body: "even if they port kde3.5 to qt4, kde should be able to take advantage of the optimizations and other new 'under the hood' features of qt4\n\nkde on qt4 may be faster, leaner and consume less memory\n\nso, there are somethings one will get just by porting kde to qt4"
    author: "raj"
  - subject: "Re: timeline?"
    date: 2006-05-28
    body: "James, as you point to \"Any Software Engineer\" - I can point to one or more practicing software engineers who have seen the transition between KDE 1->2 and 2->3, and can vouch for the ability of the KDE team to do two things at once (a jump in Qt and a jump in KDE functionality). Are you going to withdraw your statement now, or add some new qualifications on what sort of software engineers you mean?"
    author: "taj"
  - subject: "Re: timeline?"
    date: 2006-05-28
    body: "No, I will not withdraw my opinion because it is my opinion and Free Software is about Free Speech.  \n\nThere was a disagreement about this and I don't think that others on the side I am supporting have changed their opinions.  Time will tell if our concerns are valid or if we were simply being too cautious.\n\nWhat I am saying is that it isn't a good idea to do two things at once when modifying software (in this case porting to a new tool kit and making major changes to the software), and I hope that there aren't any problems and delays."
    author: "James Richard Tyrer"
  - subject: "Re: timeline?"
    date: 2006-05-29
    body: "Fair enough re: free speech, my primary purpose was to mention that your statement is factually incorrect. You are certainly breaking no laws by continuing to stand by it.\n\nThere will almost certainly be problems and delays, and as a Software Engineer that shouldn't unduly cause you concern, as the alternative is worse. The next time the KDE cycle allows for BC breakage, and significant framework redesign always causes BC breakage, it will almost be time for a Qt5-induced BC breakage. If the cycle is changed to allow BC breakage in mid-stream, that will only allow another period of problems and delays.\n\nSum total period of problems and delays will almost certainly be higher than if it is simply done now, especially as Qt 4 provides so many opportunities to get the redesign done while porting (MVC, for example)."
    author: "taj"
  - subject: "Re: timeline?"
    date: 2006-05-29
    body: "Your \"any software engineer\" argument has been bunked several times before by others and I will do it again. I'm a software engineer and I think that what is going on right now is a good idea. So there.\n\nWhat your suggestion means is that KDE-4.1 would be incompatible with KDE-4.0 which is a really Bad Idea(tm). Only the linux kernel folks get away from breaking everything between minor releases (I have no idea why).\n\nDo you really think that any developer/distributor would touch a release that  is incompatible with the previous release and superceded by an incompatible release just after a few months? Now any software engineer will tell you that THAT is a bad idea. KDE-4.0 would end up being a non-release while everybody is waiting for the \"real\" release of KDE-4.1 which integrates all the new stuff. Plus it would create a really annoying mess."
    author: "atte"
  - subject: "Re: timeline?"
    date: 2006-05-29
    body: "> Only the linux kernel folks get away from breaking everything between minor releases (I have no idea why).\n\nAnd the GCC folks."
    author: "illissius"
  - subject: "Re: timeline?"
    date: 2006-05-28
    body: "I fear for KDE4 because now, some important parts of it are just vaporware.\nLook at plasma, there isn't at this point a plan for the new interface in KDE4, just a wish to change the desktop for a mor emodern concept.\nI went to a presentation in FISL 7 where Aaron Sigo showed just a icon the had menus when you put the mouse over it with options like open, delete, etc. Besides the concept being failed in my option (I belive it should open a menu for it, not over the icon) is is so few concepts until now.\nProbally the interface of plasma will require a lot of coding, but I don't see plasma being done soon, so when october arrives we'll probally see only a kwin running without kicker.\n\nMy point? KDE probally will be out only on second half of 2007, I belive by the end of the year, so don't keep holding your breath until then.\nPS: There is nothing wrong with it, maybe with a large extra time, developers can fix long standing problems on popular KDE problems (kopete could use a lot of help and speed improved) or start adding new and cool features.\n\nI have faith KDE4 will be a great thing, but I don't have faith it will arrive soon enought."
    author: "Iuri Fiedoruk"
  - subject: "bury these custom icons..."
    date: 2006-05-27
    body: "> Usability has always been an important focus in amaroK, but it's \n> hard to get consensus about certain issues.\n\nPlease bury these custom icons, tell your artists to complete the standard icon sets and keep consistency at a high level."
    author: "Anonymous Coward"
  - subject: "Re: bury these custom icons..."
    date: 2006-05-27
    body: "Those custom Icons are cool.\nIf your brain is overstrained by those good looking icons you can always switch to the default icons.\n\nBut please don't try to deprive other people of those nice icons.\nThey make amarok look much better and yes - in my opinion they even improve usability. Usability does *not* mean that every program has to look as boring as kedit."
    author: "max"
  - subject: "Re: bury these custom icons..."
    date: 2006-05-27
    body: "let's fight about this, max... i know where you are now... :D"
    author: "superstoned"
  - subject: "Re: bury these custom icons..."
    date: 2006-05-28
    body: "Speaking of custom icons, when i installed 1.4 beta, the custom icons were default; and when i installed 1.4 final, the kde icons were default.\nDid the amaroK team change this, or should i 'blame' the packager?"
    author: "Rinse"
  - subject: "Re: bury these custom icons..."
    date: 2006-05-28
    body: "Changed temporarily because the theme is still incomplete. We hope to get it completed for 1.4.1."
    author: "Mark Kretschmann"
  - subject: "Re: bury these custom icons..."
    date: 2006-05-27
    body: "Perhaps the icons are \"way cool\" but to be a KDE application the developers should (also) supply a set of icons in the current default style (CrystalSVG for KDE 3.x.y) and a set of HiColor (generic|unthemed)icons.\n\nThe current KDE icon theme should be used by default if running with the KDE desktop.\n\nOther than the above, I see nothing wrong with also providing custom icons which the user can use if they want."
    author: "James Richard Tyrer"
  - subject: "Re: bury these custom icons..."
    date: 2006-05-28
    body: "> The current KDE icon theme should be used by default if running with the KDE desktop.\n\n++"
    author: "Carlo"
  - subject: "Re: bury these custom icons..."
    date: 2006-05-30
    body: "A clarification on the subject of custom icon themes and programmers.\n\nWe have been trying for the last six or seven months to find someone who are able to create these icons for us, please do! The squad are all coders (or in my case someone who is unable artistically to create CrystalSVG style icons because my own style is so different from that), and have really tried hard to find artists.\n\nThe real problem is that this is no small task - the current (and incomplete i might add) list of icons we're missing can be found on our wiki: http://amarok.kde.org/amarokwiki/index.php/Artist_team:Icons\n\nAt K3M, however, Thomas Zander and i managed to finally reach an understanding on the subject - He has offered to function as an intermediate between the amaroK team and the KDE Artists team. Missing currently is for the icon list to be completed (with good descriptions for each icon, so that the artists can actually figure out what the icons are to be used for) - i have stepped forward to create (or rather finalise) this list, and hope to be able to do this within the week."
    author: "Dan Leinir Turthra Jensen"
  - subject: "Now that you're all together"
    date: 2006-05-27
    body: "I'd like to know how are KDE 4 multimedia  apps going to deal with restricted media formats. The situation right now as I see it is the following:\n\n- If you haven't got installed the correct media codec, then when you play the multimedia file say in amarok or kaffeine, it fails and the media player tries to go to the next file.\n- If you have a list of for example mp3 files, then amarok just keeps showing 2 or 3 OSD per second, and consuming quite a lot of cpu.\n- In order to install the correct media codec, you need to take care of your self. Depending on your distro, there's always multiple different ways to solve the problem, but something certain is that it will be a troublesome experience.\n\nWe cannot fix the problem by just saying \"that's a problem that each distribution should overtake by itself\". Instead this is the solution I suggest:\n\nJust copy what Firefox do with multimedia plugins like Flash! When a file couldn't be opened, give a OSD notice to the user saying something similar to \"This song is in MP3 format, but it couldn't be played. Please CLICK HERE install support for MP3.\"\n\nAfter that, a popup window shows up, which by using (for example) khotnewstuff downloads a list of the apps available for each distro that resolve the dependency attached with a short description and whatever is needed to each one. For example if there's one MP3 codec which probably infringes some USA patents, warning the user \"If you live in USA, please don't install this codec if you don't want to have codecs, use the Linspire codec instead\". Also you can give a short description of why the codec was not installed \"MP3 is a restricted media format and that's why Ubuntu Dapper Linux does not come with support for it\". \n\nIf installation ends successfully, ideally the MP3 will play without neededing to restart amarok (just like after installing flash plugin in firefox ;-). \n\nThis \"multimedia support installer\" could be generally available to multiple application: konqueror (for i.e. flash support), kaffeine, amarok.."
    author: "Eduardo Robles Elvira"
  - subject: "Re: Now that you're all together"
    date: 2006-05-27
    body: "Perhaps we need an API for this task :\n\n - define interfaces to add new software, and let the distributors implement them. After that all Apps will be able to install software a defined way.\n\nIts like Phonon , define the API , and then implement backends\n\nthe interface should :\n - install by keyword\n - install deps\n - and so on\n\n\n\n"
    author: "ch"
  - subject: "Re: Now that you're all together"
    date: 2006-05-27
    body: "Perhaps we need an API for this task :\n\n - define interfaces to add new software, and let the distributors implement them. After that all Apps will be able to install software a defined way.\n\nIts like Phonon , define the API , and then implement backends\n\nthe interface should :\n - install by keyword\n - install deps\n - and so on\n\n\n\n"
    author: "ch"
  - subject: "Re: Now that you're all together"
    date: 2006-05-27
    body: "read the report this evening/night from today :D"
    author: "superstoned"
  - subject: "Nooooooooooooo!!!!"
    date: 2006-05-27
    body: "Repeat with me:\nsecurity, security, security.\n/home mounted with noexec.\nsecurity, security, security.\n\nThe Only True Way (tm) of installing codecs (AND browser plugins!!) is thru your distro's package manager! Anything else is a threat! Seriously.\n\nSo, if you use Dapper and you try to play an MP3, just show the following message:\n\nFor Dapper to play MP3s, you should go to Adept, enable multiverse, and install the package gstreamer0.10-plugins-ugly-multiverse (possibly with links for opening adept, or dcop instructions to do so -- requiring the user to give his sudoing password, of course.)\n\nHTH"
    author: "Humberto Massa"
  - subject: "Re: Nooooooooooooo!!!!"
    date: 2006-05-28
    body: "Yeah, joe blow users that just want to play some music will really love that. Besides, everyone knows that the \"one true way\" is only to install things you've personally reviewed the source of and compiled yourself. Everything else forces reliance on untrusted 3rd parties. Besides, you forgot that /var, /tmp, and /root (at a minimum) must also be mounted noexec."
    author: "bah"
  - subject: "Banshee, the next best thing to Linux iTunes"
    date: 2006-05-27
    body: "http://gnomedesktop.org/node/2690"
    author: "Banshee"
  - subject: "Re: Banshee, the next best thing to Linux iTunes"
    date: 2006-05-27
    body: "Banshee, the next best iTunes clone for Linux. Yay!"
    author: "Max Howell"
  - subject: "Re: Banshee, the next best thing to Linux iTunes"
    date: 2006-05-27
    body: "amaroK as a full-blown audio player is certainly superior to banshee, however banshee still has great daap support which is something that amaroK still lacks. Really nice to see that there's a Google SoC project on this though; hoping it will all go through. :)"
    author: "apokryphos"
  - subject: "Re: Banshee, the next best thing to Linux iTunes"
    date: 2006-05-27
    body: "Maybe, but AmaroK is even better than iTunes :-)\nBut thanks god, Amarok runs perfectly under Gnome, too."
    author: "max"
  - subject: "Re: Banshee, the next best thing to Linux iTunes"
    date: 2006-05-27
    body: "1. Podcasting is better under iTunes.\n\n2. Indeed, who cares about toolkits and libraries."
    author: "And"
  - subject: "Re: Banshee, the next best thing to Linux iTunes"
    date: 2006-05-27
    body: "Um, if you could please explain why and how itunes is superior with Podcasting, i would be pleased to improve this support.  Have you used 1.4? we have increased the usability and feature set of podcasting dramatically."
    author: "sebr"
  - subject: "Weasel-casting"
    date: 2006-05-27
    body: "That's what we all want. Really."
    author: "Max Howell"
  - subject: "DRM-crippled Banshee has no copyleft protection"
    date: 2006-05-27
    body: "Banshee devs are sell-outs to the Free Software cause. By releasing Banshee under the extremely weak MIT X11 License, they are welcoming DRM-crippled plugins so that the entertainment cartel can handcuff users and rip them off.\n\nNo thanks!"
    author: "AC"
  - subject: "Re: DRM-crippled Banshee has no copyleft protection"
    date: 2006-05-27
    body: "Who cares, everybody can still use Amarok or Songbird or lTunes.\n\nBut indeed, it is a weakness and a risk of Banshee. "
    author: "And"
  - subject: "Re: DRM-crippled Banshee has no copyleft protection"
    date: 2006-05-28
    body: "It's not a weakness, it's done on purpose to attract DRM corporations. If it does, it's fulfilled its purpose. Why on earth would it be MIT/X11 otherwise?\n"
    author: "wee"
  - subject: "Re: DRM-crippled Banshee has no copyleft protectio"
    date: 2006-05-29
    body: "Why do you hate freedom?"
    author: "ac"
  - subject: "Re: Banshee, the next best thing to Linux iTunes"
    date: 2006-05-27
    body: "\"Submitted by stro on Thu, 2006-05-25 07:57. Gnome Multimedia \nI want an audio player that will do everything that Apple's iTunes does..... including working with my iPod ..... and do it natively on Linux. That's a tall order. I've tried many fine Linux programs..... KDE's amaroK, RealPlayer 10 for Linux, Xine, etc. .... but none have scratched my itch.\"\n\nAs usual, dirty anti-campaigning on Gnome's side. A clear example that Gnome is unethical software or a least the former Ximian staff promoted an unethical culture. \"..but none have scratched my itch.\" Get real!\n\nNo, I will not use Banshee, perhaps Juk."
    author: "Itchy "
  - subject: "Re: Banshee, the next best thing to Linux iTunes"
    date: 2006-05-27
    body: "\"\"I want an audio player that will do everything that Apple's iTunes does..... including working with my iPod ..... and do it natively on Linux. That's a tall order. I've tried many fine Linux programs..... KDE's amaroK, RealPlayer 10 for Linux, Xine, etc. .... but none have scratched my itch.\"\n\nAs usual, dirty anti-campaigning on Gnome's side. A clear example that Gnome is unethical software or a least the former Ximian staff promoted an unethical culture. \"..but none have scratched my itch.\" Get real!\"\"\n\nWell, I really scratched my head at that one considering that amaroK ticks all of those boxes for me.\n\nIt just seems as though Banshee came about as a Mono project via some guys at Novell because they just didn't have anything that could compete with amarok. Notice that no one at Novell had to start amaroK as a project, but resources or developer time into it. Oh, the power of KDE development!"
    author: "Segedunum"
  - subject: "Wow"
    date: 2006-05-27
    body: "Hi there.\n\nI really love KDE and especially amaroK. *thumbs up*. Keep on that excellent  work....\nAnd by the way: I CAN wait!!!"
    author: "FloS"
  - subject: "Amarok requirements"
    date: 2006-05-27
    body: "(Posted anonymously because I get enough spam already.  I miss the days when posting your e-mail address in your .sig was without \"consequences\".)\n\nI hope I read the above in-correctly: that Amarok v2 will require MySQL or some other DB back-end.\n\nPlease, don't.  Just make this an option.  Amarok should just be an audio player, not some swiss-army-knife-style big honking application.\n\nAnd as for usability, quite a few of your users *do* have suggestions for you, listen to them too, not just the \"experts\".  End-users know what confuses them, what makes their life easier.  Not everything they request is frivolous, even if you think it's \"oldskool\" (sp? whatever).\n\nAnd one last thing: make sure one does not have to re-compile to change an option.  Ok, maybe not all options require a recompile, but to add support for engine xyz, I had to do it recently.  If the Amarok devs can look into this, it would be great!\n\nCheers.\n\n/AC\n\n"
    author: "Anonymous Coward"
  - subject: "Re: Amarok requirements"
    date: 2006-05-27
    body: "Regarding the database: that's the whole point, that amaroK is often *packaged* with, say, the MySQL database required, even though it's a totally optional (compile-time) piece of functionality. By default, sqlite is used and doesn't require much overhead. But because amaroK is packaged with MySQL as a dependency, the developers get grief for being overly complicated, even if the source doesn't require it."
    author: "Adriaan de Groot"
  - subject: "Re: Amarok requirements"
    date: 2006-05-27
    body: "Even if built with mysql support, amarok doesn't require you to install the MySQL database server.\n\nIt may require you to have the client libraries installed (which take all of 4MB of your disk), but probably that should be optional too, if amarok uses dlopen instead of linking.\n\nOn the other hand, if you don't enable mysql support at compile time, and you want to use mysql, then you are SOL and have to rebuild from source.\n\nOr keep two 99% identical packages in the distro's repos.\n\namarok with mysql enabled == good."
    author: "Roberto Alsina"
  - subject: "Re: Amarok requirements"
    date: 2006-05-27
    body: "Bingo!  You just mentioned something that I mentioned in my original post: having options that are configurable only at compile time.  In your reply, you mention MySQL support.  Isn't there a way to make it as some sort of plug-in option or something that can just be checked on or off in a dialogue box?\n\nIf not, can you or any other amarok developer explain why?\n\nThanks.\n\n/AC"
    author: "Anonymous Coward"
  - subject: "Re: Amarok requirements"
    date: 2006-05-28
    body: "But what's the problem with mysql support in amaroK?\nMy version of amarok (comes from guru) has support for mysql, postgresql and sqlite.\nBy default is uses sqlite, and i left it that way :)\ni don't have mysql or postgre on my system, and i am not bothered by the fact that amarok has build in support for both database servers.\n"
    author: "Rinse"
  - subject: "Re: Amarok requirements"
    date: 2006-05-28
    body: "I think you missed my point.\n\nWhat I'm saying is that you have to do a (re)compile amarok to get support for x, y or z.  Instead of dropping a plug-in in a designated folder and re-starting the app.\n\nMySQL support in amarok is not a problem in itself, it's *how* you enable it.\n\nAnd weither (sp?) it is MySQL support or AAC/aacPlus decoding capability you want to add to amarok, an end-user should not have to re-compile the application to add to it.  Not everyone has the environment necessary to recompile amarok installed on his/her desktop.  In a attempt to get a functional amarok on my Kubuntu Dapper box, I downloaded the source, and the amount of stuff I had to install was surprising.  G++, Ruby, various devel. libs, etc.  I was able to get \"configure\" to stop complaining, but how-many end-users would be able to do so?  Not everyone is a geek.\n\nCheers.\n"
    author: "Anonymous Coward"
  - subject: "Re: Amarok requirements"
    date: 2006-05-28
    body: "Ah ok.\nI never compile applications myself, and i think that users shouldn't be bothered with compiling applications.\nLooking at amarok: i see no reason why a packager would compile it without support for mysql/postgre.\nThey just should not let the package depend on it.\n\nimho compiling amarok with mysql/postgre without actually depending it in the rpm/deb/whatever package is much better than providing amarok without mysql and demanding the user to lookup/download/install seperate plugins.\n\n"
    author: "AC"
  - subject: "Usability and users"
    date: 2006-05-27
    body: "End users aren't as knowledgeable as you think. I don't mean that derogatorily either. The best analogy is to a patient coming in with a sore throat. You, the doctor, listen to his complaints and ideas. He thinks it's strep (bacterial). You do the tests and it's not. Instead it's a viral infection but he still wants antibiotics and doesn't understand why won't give them (a common occurrence in medicine). \n\nUsers know that something *is* wrong but they're not always sure how to mend it. They may know something confuses them but their suggestion to fix it might actually make things worse. It's worth listening to users, of course, to know something is confusing or needs tweaking. It's also good to keep their suggestions in mind, but people trained in an area, like usability, probably deserve more credibility for their expertise e.g. http://lists.kde.org/?l=kde-usability&m=114652287701617&w=2"
    author: "sundher"
  - subject: "Re: Usability and users"
    date: 2006-05-27
    body: "My point is that end-users are like those mine-sniffing dogs: they don't know how to defuse the mine, but they sure can find them.  End-users can't always suggest the best way to fix a problem, but they sure can point to real problems better than developers.  That is why a developer should never do Q-A testing, because he or she will unconsciously (sp?) avoid potential problem areas and will too often test an application a specific way, \"The One True Way The Application Is Meant To Be Used\"(tm).\n\nEnd-users (or \"real\" testers) will often do things the devs never thought about.  I see it frequently at work -- the proportion of applications that break very quickly once they leave the dev shop is staggering.\n\nThis being said, sometimes, a request by an end-user comes from a real need.  For example, I did request some time ago for the WinAmp-like amarok player window to have the option of being made bigger (especially its volume control), because I found it too small (sign of me getting older, I guess) and hard to control.  I am sure the Winamp & XMMS devs must have gotten the same feedback from their users, because both of them have the \"double-size\" option.  Simple request, real need also seen elsewhere.  That would not have made things worse for amarok.\n\nThat is why I say again, don't always dismiss what end-user say.  They can have needs that (possibly younger) developers don't.\n\nCheers.\n\n/ac"
    author: "Anonymous Coward"
  - subject: "Re: Usability and users"
    date: 2006-05-28
    body: "What you said was \"And as for usability, quite a few of your users *do* have suggestions for you, listen to them too, not just the \"experts\". End-users know what confuses them, what makes their life easier.\" This is what I was responding to.\n\nAll users have suggestions. Most of them do come from a real need. Not all of their suggestions are good. I didn't dismiss what end users say in fact I said that it's worth keeping their suggestions in mind and listening to their problems. You put \"experts\" in quotations and that tends to imply a dismissal of knowledge and training. (Just because I can change my oil doesn't mean I'm going to dismiss what a mechanic \"expert\" says.) Usability experts have been trained in usability and their opinions should be given more weight. Everyone on /. and their cousin can lecture on usability. Few know what their talking about. (For real hilarity, the economic/science story comments are especially prone to modded up ignorance.)\n\nI think there might be some miscommunication here. When you said \"experts\" it was imprecise and I took it to mean usability experts since \"experts\" was referring to the clause before the comma when you mentioned usability (And as for usability,. . . not just the experts). If you meant experts as devs, then we're not really arguing since I think devs aren't always the best source of usability advice (/. again) and when they don't listen to users or actual experts you can end up with UI nightmares. *coughgimpcough*\n\n"
    author: "sundher"
  - subject: "Re: Usability and users"
    date: 2006-05-28
    body: "I have put the work _experts_ between quote because the last 10-16 years has seen an influx of non-technical people, individuals without any technical instincts in the field.  Think about all of these paper MCSEs, and before that all these paper CNEs.  Think about all these \"internet designers\" that have zero technical skills (like your typical paper MCSE).\n\nToo many so-called usability experts have produced UI abominations for me to take the title \"expert\" at face value.  Too many of them end up thinking like geeks and forget how Normal People(tm) think.\n\nThere *are* true UI experts, and they can do very good work.  But like these \"internet professionals\", there are too many, huh, charlatans out there for me not to be skeptical at first.  That was why I said \"experts\".\n"
    author: "Anonymous Coward"
  - subject: "Windows-Port"
    date: 2006-05-27
    body: "> Max Howell talked about moving to amaroK 2 and the Windows \n> port that will come with that.\n\nVery cool! For converting people from Windows to Linux it is quite important that they can still use the same application. And a cool crossplatform mediaplayer that can substitute iTunes (without the DRM...) e.g. is a big plus."
    author: "Ascay"
  - subject: "Re: Windows-Port"
    date: 2006-05-27
    body: "> For converting people from Windows to Linux it is quite important \n> that they can still use the same application.\n\nIt doesn't work like that. The correct phrasing would be:\n\n\"For converting people from Windows Media Player to amaroK it is quite important that they can still use the same operating system.\"\n\nIt would be a real help if windows apps got ported to linux, but the pposite makes no sense for linux (although it certainly can make sense for the apps that get ported to windows).\n\namaroK is exactly the only thing that has proved capable to make my windows friends curious about linux. It ill be great for it to run on windows, but n the long term this is a great disadvantage to both Linux and Free Software.\n\nAnd don't assume that spreading a Free app will make people appreciate Freedom. They already appreciate freeware and that's all that amaroK, openoffice etc. will mean to them.\n\n\n"
    author: "Doubt"
  - subject: "Re: Windows-Port"
    date: 2006-05-27
    body: "i'm afraid i agree with you. i still kind'a think porting KDE to windows might not be the smartest thing the project ever did. on the other hand, it might bring in more help and (commercial) support, and of course knowledge..."
    author: "superstoned"
  - subject: "Re: Windows-Port"
    date: 2006-05-27
    body: "It's smart for KDE, and we will probably see more KDE apps, both proprietary _and_ free. But it's probably bad for the free world, and in that case it might also backlash against KDE.\n\nNow this is really a difficult topic, and I won't say more :-)\n"
    author: "Doubt"
  - subject: "Re: Windows-Port"
    date: 2006-05-27
    body: "Linux will always be our priority, we love Linux too much :)"
    author: "Max Howell"
  - subject: "Re: Windows-Port"
    date: 2006-05-29
    body: "But well, if it's good for the app, then it's good for the users!\nJust hope this won't slow down linux's version developpement too much.\n\nAnd it's true that many of my friends are switching to linux thanks to amaroK among others."
    author: "Ricard"
  - subject: "Re: Windows-Port"
    date: 2006-05-28
    body: "thats a real ignorant way of thinking.\n\nits simple: people switch when the barrier to do so is low enough. people don't switch if its to much work.\nso what do you have to do to lower the amount of work needed to switch? the easiest way is to only switch part of the environment. of course, that works especially well if its the hidden part that is switched ;).\n\nbut now we have a problem! can we realy wait for microsoft to port IE, their (windows) media player, the msn messenger, outlook and Office to your desired plattform?\n\nunfortunately thats realy unlikely to happen ;).\n\nso if you don't get their software on your platform, you have to get them to use your software on their platform. thats realy the only way to lower the effort for switching later.\n\nthere is no doubt that porting windows apps to other platforms would be way better. but still, just wait for things to happen will get you even less than porting your desktop stuff to windows.\n\n\nso other platforms like linux don't realy have a choice if they want more users. they need to make switching easy, and the only way to do that IS compatibility."
    author: "SiberianHotDog"
  - subject: "Re: Windows-Port"
    date: 2006-05-28
    body: "> thats a real ignorant way of thinking.\n\nSir, out of sheer ignorance I do wish to ask you a question:\n\n> its simple: people switch when the barrier to do so is low enough. \n> people don't switch if its to much work.\n\nYou seem to assume that the first sentence follows from the second. Why that?\n\n'people don't switch if its too much work' - right, that's what 'too much' means after all\n\n'people switch when the barrier to do so is low enough'. Wrong. People switch when there is a reason to switch that they can comprehend. They don't just switch because it's easy. And they very rarely see an advantage in an _operating_system_ (they have mostly no clue as to what that is). \n\nBut then, I don't necessarily think that something is wrong with the windows port. I just Doubt. It's good to ask questions. It's bad to call people 'ignorant', unless you know exactly why you do it.\n\n\n"
    author: "Doubt"
  - subject: "Re: KDE"
    date: 2006-05-27
    body: "I am a FreeBSD user and I love KDE its nice and beauty becouse its too simply ... I hear that KDE 4 can be more simple to use? What is good of this is that there are new technologies about phonon and more, ... I am not programmer but sometimes i create a UI dialogs and run it with kmdr-executor (in slackware) ... I think that QT is more powerfull than Gtk+  Lukas."
    author: "skywaker"
  - subject: "Re: Windows-Port"
    date: 2006-05-28
    body: "Why is it so important to convert people from Windows to Linux?  If you care about that then its not a subsitute for iTunes but a subsitute for Windows mediaplayer that is needed. They just need to make it simpler to use - i dont think that amarok cuts it in terms of simplicity."
    author: "josel"
  - subject: "Re: Windows-Port"
    date: 2006-05-28
    body: "i think applications like kaffeine, kplayer and kmplayer are more like a windows media player replacement.\n"
    author: "Rinse"
  - subject: "phonon's presentation"
    date: 2006-05-28
    body: "As a dev of a \"media producing\" KDE app (Rosegarden), I must say I really like what I've seen on Matthias Kretz's slides. Clear and practical goals, no \"we will allow configuring chorus+reverb+delay on the system bell\" stuff... Much much better than aRts. I hope this will all go nicely."
    author: "Guillaume Laurent"
  - subject: "Re: phonon's presentation"
    date: 2006-05-28
    body: "If phonon is as easy to program as Matthais's slides promise, it'll be a big breakthough for KDE. \n\nHopefully, it'll be powerful enough to construct general filter graphs for audio/video. That would make building WYSIWYG video/music apps soooo easy.\n\nCheers,\nBen"
    author: "Ben"
  - subject: "Last.fm radios in amaroK"
    date: 2006-05-28
    body: "Hello !\n\nI wrote an amaroK plugin to play last.fm radios in amaroK 1.*! You can find it here: http://kde-apps.org/content/show.php?content=39883"
    author: "Thesa"
  - subject: "amarok 2.0"
    date: 2006-05-28
    body: "I'd find it cool if Amarok 2.0 gets a Skin-Engine like Winamp 5 or Xmms.\n\nAndreas"
    author: "Andreas"
  - subject: "Re: amarok 2.0"
    date: 2006-05-29
    body: "No. Please don't use those skins on any application in KDE.\nA consistent look of my applications are very very important to me.\nWhen it was a new feature to skin your applications (thinking of WinAmp 1.x) it was cool to have different skins.\nNowadays there are so many applications that don't use the common look of the toolkit's widgets - and most of the time it is plainly ugly.\n\nConclusion: Never ever make KDE applications skinnable - that's what the Qt-Style API is for.\nIt'd waste developer resources and most the time doesn't even look good.\n\nJust my 0.02 EUR"
    author: "Christian Nitschkowski"
  - subject: "Re: amarok 2.0"
    date: 2006-05-29
    body: "I'm going to use skins on all KDE applications."
    author: "ac"
  - subject: "Re: amarok 2.0"
    date: 2006-05-29
    body: "Well, I use\no Noatun/KJofol whenever I don't use the playlist very often. It is just nice to look at it when noatun works in the background. Yes, sometimes skinning could be nice indeed.\no JuK, because it is just simple and works. The visual layout of the NON-skinned interface is just asthetic, well-ordered, simple and clean (though sometimes it hangs when starting the sound engine)\no Amarok - just when noatun or juk have trouble in playing a song. I used Amarok 0.x until they introduced this animated listbox selection and the ugly sidebar. It may have all functions someone wants to have. The problem is: The very few additional functions compared with JuK are *at least for me* less significant compared with the visual appearance and the usuability point of view. \n\nIndeed, I believe the Amarok team should listen to users complaining about the look of their application. JuK already showed that it is possible to look great without skinning, but sometimes I still miss the abilities of KJofol. \n\nThese are my 2 eurocents"
    author: "sebastian"
  - subject: "Unlikely"
    date: 2006-05-30
    body: "The odds are low that amaroK will ever have a skinning engine."
    author: "Max Howell"
  - subject: "Re: Unlikely"
    date: 2006-09-11
    body: "Right, it will only have a theme-ing engine."
    author: "rebuilt"
  - subject: "Re: amarok 2.0"
    date: 2008-01-21
    body: "i figure this thread isnt still alive, but i find it very interesting, so whatever.\nive never heard anyone against skins like that before, its very intriguing. though i guess windows programs dont have as customizable graphics as kde... if thats what was in fact bein talked about. i dont really know much about it.\ni thought about this, and i think that the thing is with skins, at least for winamp when i used them, is that they werent just for different looks. ive always thought that a really good skinner would have his/her own skin that he could use extremely easy, the buttons being exactly where he wanted, etc. sure, the old winamp skins were fun, but i think that all media players should have a really good skinning engine, just in case you absolutely cant stand the way things are laid out. obviously it takes a lot for the colors and visuals to be unbearable, and that happens pretty rarely."
    author: "aerion"
---
In the rainy Netherlands, eighteen KDE hackers have been working in the <a href="http://www.wallsteijn.nl">Annahoeve</a> on Multimedia for the fourth incarnation of KDE. This report outlines the meeting topics, and the results of interesting presentations and explains how KDE developers outbid each others marshmallow records.
















<!--break-->
<div style="float: right; border: thin solid grey; padding: 1ex; margin: 2ex"><a href="http://static.kdenews.org/danimo/k3m_day1.jpg"><img src="http://static.kdenews.org/danimo/k3m_day1_small.jpg" width="300" height="220" border="0" /></a><br /><a href="http://static.kdenews.org/danimo/k3m_day1.jpg">This is where Phonon and amaroK 2 meet</a></div>

<p>The meeting officially started at noon, but the organization and Matthias, Martin, Alexandre, Florian and G&#225;bor were already at the Annahoeve at 9:30. We had things up and running rather quickly and some smalltalk went on. Marshmallows were put on the table and Adriaan told everybody they could earn a marshmallow if they were able to stick 10 pieces into their mouth. Sebastian got close.</p>
<p>The afternoon started with a brief keynote, followed by presentations about Phonon and amaroK. Matthias Kretz talked about the basic design goals of Phonon (<a href="http://static.kdenews.org/danimo/k3m-vir.pdf">see his slides</a>). It should provide the multimedia support that 80% of the applications need in a simple way; the other 20% are simply not its focus. For example, if an application needs precise control at sample level, it is better off talking to a media framework directly. Generally speaking, most applications just need some basic things like playing or streaming video and audio. Matthias further explained how effects worked, and what to consider when building a back-end, and demonstrated some some code examples.
</p>
<p>Max Howell talked about moving to amaroK 2 and the Windows port that will come with that. The amaroK hackers are really excited about amaroK 2 and they really want to move on and work on it. A core redesign is needed to prevent regressions from coming up in new releases. This weekend will be used to plan large parts of this redesign. </p>

<p>Max also pointed out some issues with packaging, mostly due to packagers making weird choices when building amaroK packages like depending on MySQL. These things should be solved by making packaging amaroK easier and by more communication with the packagers. 
</p>
<p>Other topics were <a href="http://last.fm">Last.fm</a> and Audioscrobbler integration. Last.fm offers more services that amaroK doesn't yet make use of, it can be used to give and share arbitrary labels and other information about songs - the amaroK developers want to add support for this. Last.fm also offers personal radio streams, with for example your favorite music, or music listened to by people with a similar taste. Integration in amaroK will allow you to skip songs in the stream, and Last.fm even learns from that.</p>

<p>Usability has always been an important focus in amaroK, but it's hard to get consensus about certain issues. Still the plan is to do some serious work for amaroK 2 with the help of the two usability experts available at the multimedia meeting. Being able to meet face to face makes usability discussions much easier - as you're less likely to step on someone's toe if you can see them...</p>

<p>After a great dinner, the hacking continued. Lively discussions and silently hacking were intermingled, and the resulting code was committed.
</p>
<p>All in all, we came to an important conclusion:<br />
<em>The important thing in Free Software is not the free beer; it is the Free bar which ensures the beer will STAY free.
</em>
</p>
















