---
title: "KOffice: Summer of Code Students Deliver the Goods"
date:    2006-09-05
authors:
  - "brempt"
slug:    koffice-summer-code-students-deliver-goods
comments:
  - subject: "Wow"
    date: 2006-09-05
    body: "Wow, seems like KOffice is getting features that does not even exist in MS Office or OO."
    author: "ale"
  - subject: "Re: Wow"
    date: 2006-09-05
    body: "It has had them for a long time.  Neither OO.o nor MS Office has any equivalent of Krita."
    author: "Inge Wallin"
  - subject: "Re: Wow"
    date: 2006-09-05
    body: "What? Isn't the gimp a part of gnome's oo.org , and photoshop a component of windows?"
    author: "marce"
  - subject: "Re: Wow"
    date: 2006-09-05
    body: "Nope.  OpenOffice.org is an office suite totally independent of Gnome, Gimp is not a part of it and photoshop is not a part of MS Office."
    author: "Inge Wallin"
  - subject: "Re: Wow"
    date: 2006-09-05
    body: "Nevermind, I tried to be ironic on my previous post.\nPls apologize for any inconvenience."
    author: "marce"
  - subject: "Re: Wow"
    date: 2006-09-05
    body: "Ok, but don't let this become a habit.  *stern look*  :-)"
    author: "Inge Wallin"
  - subject: "Re: Wow"
    date: 2006-09-06
    body: "It's hard to tell, because some people REALLY do think any software remotely associated with GTK+ is part of GNOME. To these people Firefox, GIMP and OO.org are all GNOME apps. "
    author: "Brandybuck"
  - subject: "Re: Wow"
    date: 2006-09-06
    body: "But OO.org wrote its own widget set from scratch which it uses on all platforms. There is no GTK usage there at all.\n\nWhat surprises me is why so many people think that there are just two widget sets; Qt and Gtk. And everything not Qt must therefor be Gtk. Which is pretty far from the truth :)"
    author: "Thomas"
  - subject: "Re: Wow"
    date: 2006-09-06
    body: "Just add KDE dialogues and Crystal Icons and people feel at home.\n\nBut honestly, a wxwidgets backend for QT would improve things dramtically."
    author: "furangu"
  - subject: "No can do"
    date: 2006-09-08
    body: "wxWidgets allows unrestricted commerical use licenced under the LGPL so a Qt version of wxWidgets is legally impossible."
    author: "wxWidgets Dev"
  - subject: "Re: No can do"
    date: 2006-09-08
    body: "If you are saying that you can't link GPL software and LGPL software, you are very confused."
    author: "James Richard Tyrer"
  - subject: "Re: No can do"
    date: 2006-09-14
    body: "Most of kdelibs is LGPL.  GPL can be linked to LGPL just fine.  Why do you think there are businesses like The Kompany (i.e.- http://www.thekompany.com/home/) that creates semi-closed source software that uses kdelibs?\n\nBobby"
    author: "Bobby"
  - subject: "Re: Wow"
    date: 2006-09-06
    body: "Is Skype a KDE-app then? Or everything that uses Qt? Wow..."
    author: "ale"
  - subject: "Re: Wow"
    date: 2006-09-05
    body: "yeah, i'm impressed. i mean - helloooooh, version support!!!!! that's soooo cool... i hope it has/will have a good gui, a timeline and it'll keep the undo after subsequent savings - these things i've been looking for for a long time.\n\ni would also love some way to attach/embed some text snippets, alternatives, to (parts of) documents. i often have a second or third alternative for a paragraph with text - and i'd love to be able to save them some way. but i guess that's a little too much to wish for, and there are much things which have higher priority  ;-)\n\n"
    author: "superstoned"
  - subject: "Re: Wow"
    date: 2006-09-06
    body: "Fill a bug about it ;)\nIt seems like a cool idea for me!"
    author: "Rafa&#322; Malinowski"
  - subject: "Re: Wow"
    date: 2006-09-13
    body: "It would be awesome to have some sort of versioning system for documents... like when you save, you can save it as a more recent version, but still keep all the previous incarnations in the same file. Then have a view mode that lets you see the document with all the previous versions at the same time, in a heirarchical view, in reverse chronological order... maybe even let you pick out, paragraph by paragraph, the version you want to keep, then at the end, view what you picked as a coherent document. It might help to have some sort of plugin that runs diff on each section of text against every other section, so as to weed out duplicate paragraphs. This would be quite awesome."
    author: "kephnos"
  - subject: "some thoughts"
    date: 2006-09-05
    body: "Congratulations to all of the five! It's great to hear about these developments and potential new developers!\n\nI've heard that the bezier-code won't be used in krita2.0, since that will use flake, and that will somehow replace the current bezier code. I hope that it will live on in flake?!\n\nI think the layers/parts-widget still needs a lot of tweaking: First, it wastes a lot of space, like many such widgets in koffice. The \"layers\"-tab is too high, then there's too much vertical space before the \"normal\" combobox. There needs to be some room between the listwidget and the buttons below. Then, it doesn't really need a statusbar. The information for each layer (thumbnail, layername, the eye, the lock) looks really cluttered at one point, while there is LOTS of space to the right. And it goes on and on.\n\nI realize its development code, so this is just meant as constructive criticism!\n\nthanks!\n\n"
    author: "me"
  - subject: "Re: some thoughts"
    date: 2006-09-05
    body: "The statusbar you see in the screenshot is Krita's statusbar :-). As for the layout, we hope we'll be able to have it tighter once KDE4 settles a bit. As it is now, it's really hard. Sometimes buttons appear (like to the right of the opacity spinbox), for instance."
    author: "Boudewijn Rempt"
  - subject: "Re: some thoughts"
    date: 2006-09-05
    body: "Please, is there a way to replace the current \"spinboxes\" with something similar to what Adobe products use?\n\nI mean, values appearing just as underlined colored numbers, over which the user can drag left/right to decrease/increase them, or just click on them to manually enter it's numeric value?\n\nIt, both, saves a lot of screen space, and is a much practical solution than the currently implemented one.\n\nThanks!\n\np.d. I'm really amazed at Krita's development pace and direction. A great work you guys are doing, indeed! :)\n"
    author: "Gabriel Gazz\u00e1n"
  - subject: "Re: some thoughts"
    date: 2006-09-05
    body: "Just getting a drop-down slider was already really hard! We took the example for that from Photoshop 5.5 for OS X. Coding custom widgets like these takes a lot of time, so I cannot promise we'll be able to have the time to code something like you mention."
    author: "Boudewijn"
  - subject: "Re: some thoughts"
    date: 2006-09-05
    body: "I know, I know, I was asking just in case...  ;)\n\nWhat would be really great is if Qt libraries themselves implemented such a widget, don't you think?\nAnd in the end, it maybe worth it for Trolltech if they want to attract Adobe to choose Qt for their Linux port of Photoshop.  ;) ;)\n\nThanks anyway for all your efforts!\n"
    author: "Gabriel Gazz\u00e1n"
  - subject: "Re: some thoughts"
    date: 2006-09-05
    body: "Doesn't Adobe use QT?"
    author: "furangu"
  - subject: "Re: some thoughts"
    date: 2006-09-05
    body: "I think only for Photoshop Album or something like that.\n"
    author: "Gabriel Gazz\u00e1n"
  - subject: "Re: some thoughts"
    date: 2006-09-05
    body: "> I mean, values appearing just as underlined colored numbers, over which the user can drag left/right to decrease/increase them, or just click on them to manually enter it's numeric value?\n\nCan you provide the screenshot of how this looks like in Adobe programs, and explain the functionality in a bit more details? Maybe someone will feel inspired and start hacking...\n"
    author: "ac"
  - subject: "Re: some thoughts"
    date: 2006-09-05
    body: "woohoo!   :)))\nwell, sorry...  ;)\nhere is an image with 4 screenshots of an effects palette showing how it looks. The last two are perhaps not as frequently used, but I put it here for reference.\n\nThe widget is really simple, so simple I would say it's almost the absense of it what makes it so great:\n- If I want to change a parameter calles, let's say, Brightness. The program presents it's name and to the right the value, there's no input box, just the underlines value, as if it were an Internet link.\n- If I press left mouse button over the value and drag to the left, I decrease the value (pressing Shift while doing this, will change the value by ten units increments).\n- If I press left mouse button over the value and drag to the right, I increase the value (pressing Shift while doing this, will change the value by ten units increments).\n- If I single press left mouse button over the value and release it, then the value appears surrounded by a white box (with the value selected) to let me input a new value to replace the current one. (At this moment, pressing right mouse button allows the standard Cut, Copy, Paste operations with the value).\n- If I single press right mouse button over the value a context menu appears offering to setup the limits the \"virtual slider\" (let's call it that way) moves between when dragging to change values, and also to reset this parameter to the default value.\n\nBasically that would be all. If you have any doubt I am willing to help.\nAnd thanks for just trying! :)\n"
    author: "Gabriel Gazz\u00e1n"
  - subject: "Re: some thoughts"
    date: 2006-09-05
    body: "I forgot to mention...\nit doesn't show on the screenshots, but when the mouse pointer is over the value it is shaped almost equal as when you are using an Internet browser (a hand with the index finger pointing upwards) but it also shows tiny left-right pointing arrows to the sides of the index finger, to indicate the direction of possible movement. Once the user starts dragging, the hand disappears and only the tiny arrows remains on the screen.\n\n"
    author: "Gabriel Gazz\u00e1n"
  - subject: "Adobe patents/copyright on widgets?"
    date: 2006-09-06
    body: "I'd be careful. I know Adobe has in the past sued people like Macromedia for creating similar interfaces to its products..."
    author: "Anon"
  - subject: "Re: Adobe patents/copyright on widgets?"
    date: 2006-09-06
    body: "And then it bought them out :)\nDon't worry, Adobe has not enough money to buy KDE.org..."
    author: "Jaroslaw Staniek"
  - subject: "Re: Adobe patents/copyright on widgets?"
    date: 2006-09-08
    body: "There can't be any copyright on this. FLTK is using such wifgets a while now. I really like the concept. Agreed, I don't like the way KDE is mostly using MS Windows like widgets. Toolkits like FLTK provide some astonishingly well though widgets, take the file selectors for example - just intuitive and simple."
    author: "sebastian"
  - subject: "Re: some thoughts"
    date: 2006-09-06
    body: "I'm personally less then impressed by these widgets. They are quite unlike anything I ever saw and I'm pretty sure many will not see it as something they can alter at all without being trained to do so.\n\nAnd in the end the small buttons are not taking a lot of space at all so the advantages are pretty small to begin with."
    author: "Thomas Zander"
  - subject: "Re: some thoughts"
    date: 2006-09-06
    body: "These widgets are *very* nicely usable. To be honest, I think opensource lacks a little in the widget-department. For example, there are STILL input fields for INTs from 0 to 100 that have up-and-down-arrows. How useless is that?\n\nIt would make sense if one could hold the field and then move the mouose to adjust the value, but who actually taps these two buttons?\n\nHonestly, there's a lot of stuff, especially annoying in krita, karbon14 etc.\n\nI know this sounds harsh, and we all know that I won't commit any coolSliderThingy.{h,cpp} to svn myself, but please, try these widgets first! You'll see, training is not needed!"
    author: "me"
  - subject: "Re: some thoughts"
    date: 2006-09-06
    body: "The Adobe widget looks like a subclassed QLabel with the mouseMove/mousePress/mouseRelease events overriden. It shouldn't be too hard to override. The difficult part would be to get the context menu working right IMHO.\n\nCheers\nBen"
    author: "BenAtPlay"
  - subject: "Re: some thoughts"
    date: 2006-09-06
    body: "> And in the end the small buttons are not taking a lot of space at all so the advantages are pretty small to begin with.\n\nIt's true that those small up and down buttons do not take up much space, but I think they are entirely useless. Something like a QSlider is much better suited to vary a quantity over a fixed range. It seems to me that this proposed widget is basically a slider without actually showing it. How about temporarily displaying the slider when it's in \"drag mode\"?\n\nBy the way, what is this context menu good for? If I understand you correctly, you can already enter a specific value by simply clicking on the widget,"
    author: "ac"
  - subject: "Re: some thoughts"
    date: 2006-09-06
    body: "- The \"Edit Values...\" option in the context menu mainly allows the user to redefine the span of the slider (its M\u00ednimum and Maximum values). This is sometimes useful in certain cases where the default span is so big that you lose precision while dragging (perhaps changing from 1 to 2 is already a great change in the effect, but as the slider goes from -1000 to + 1000, each time you drag the value changes too much and the slider turns \"incontrolable\", so lowering the span of the slider increases this precision while dragging and makes it comfortable again).\nIn theory the span should be preset by the programmer to \"useful\" values, but often users use effects in situations not foreseen by the programmer, so I guess is in these cases when this option is more appreciated.\n\n- The other useful option in the context menu, is one allowing to \"Reset\" the value to their default state. Good after you've been experimenting and want to return safe home.  :)\n\n\np.s. I completely agree on the current state of sliders in Qt/KDE, I never use them, to me they are useless and I end entering values by hand, which is very disrupting, workflow wise.\n\"Lightwave 3D\" and \"3ds max\" are good examples of programs with good and useful sliders, but to be honest since Adobe introduced this kind of \"nano\" slider I prefer it over everything else, as they've had a very strong impact in how the (same) applications feel by improving the workflow tremendously.\n"
    author: "Gabriel Gazz\u00e1n"
  - subject: "Re: some thoughts"
    date: 2006-09-07
    body: ">  p.s. I completely agree on the current state of sliders in Qt/KDE\n\nI really meant the up and down buttons to the right of numeric input boxes, not the sliders.\nMy confusion came from the fact that in some programs (e.g. Autodesk 3ds Max) this buttons function as sliders when dragging up and down over them, so I tend to see them as sliders too.\n"
    author: "Gabriel Gazz\u00e1n"
  - subject: "Re: some thoughts"
    date: 2006-09-06
    body: "I thing that the beauty of this kind of \"hidden slider\" is that the precision is not restricted to the small screen space where the slider resides.\n\nLet me explain myself:\nIf you put a visual slider widget on the screen you have to make it \"small\" (I still haven't seen widgets with a screen wide extension) :)\nBut this small extension constraints you to put all the possible values the slider could take, from one extreme to the other, so I could end with a 3cm long widget for changing values from -500 to +500, or -2000 to +2000, so this will make the slider behave with a very low precision, mining its usefulness.\n\nWith this \"hiden slider\" paradigm the extension is also \"virtual\", so in theory it could be made as long as necessary to make the slider progress in a much more soft way, making it more useful.\nBesides that, if the user wants to enter a value directly, he still can.\n\nThe other thing I would like to refute, is that this kind of solution is not intuitive.\nIn this Internet era, if there is something users (even novice ones) know, is that if there is a colored underlined word in the middle of the screen, they could click on it to make something happen.\nIn this case, what needs to be made clear to the user is that they can not only press on it, but to also drag on it from side to side, and I think that this is the main reason for Adobe chosing to show a cursor with an index pointing hand with left and right arrows when over the \"hidden slider\".\n\nFinally, I teach Adobe and 3D animation programs since more than 5 years ago, and let me say this is one of the features people love at first sight in the new versions of Adobe programs, perhaps followed by the new dockable option palettes (something Krita already has! kudos!)\nI really think this is the way to go, not only for Qt/KDE, but in general for interfaces.\nBut of course it's just my opinion.\n\nCheers\n"
    author: "Gabriel Gazz\u00e1n"
  - subject: "Re: some thoughts"
    date: 2006-09-06
    body: "Do you have a screenshot of this widget somewhere?"
    author: "Ben Meyer"
  - subject: "Re: some thoughts"
    date: 2006-09-06
    body: "it's above at the end of one of my messages"
    author: "Gabriel Gazz\u00e1n"
  - subject: "Re: some thoughts"
    date: 2006-09-08
    body: "I tried to create a spinbox that you can control using your mouse. Its only a hack, you have to be a little careful with the mouse, I should probably make it slower.\n\nIn my opinion, its a lot nicer than to use those small buttons (phew!) or enter a number with the keypads.\n\ngreetings,\nben"
    author: "me"
  - subject: "PNGs"
    date: 2006-09-05
    body: "Are those PNGs _completely_ uncompressed?"
    author: "Robert"
  - subject: "Yes"
    date: 2006-09-05
    body: "They should have used gimp :-))\n\nEven MS Office would not produce 1MByte+ screenshots.\nOpening those PNGs and re-saving them as PNGs reduces size by >90%."
    author: "max"
  - subject: "Re: Yes"
    date: 2006-09-05
    body: "I didn't found any 1MByte screenshots (maybe someone fixed them), and loading them and saving back with either krita or the gimp give the same results as the compression is done by libpng..."
    author: "Cyrille Berger"
  - subject: "kformula edition"
    date: 2006-09-05
    body: "Just a small question, is it possible to use kformula without using the mouse? I means to do something like OOmath2 or latex. It's so more easy to write the mathematical formula directly."
    author: "ramses"
  - subject: "Re: kformula edition"
    date: 2006-09-05
    body: "It's on the todo list of the developers ;)"
    author: "Cyrille Berger"
  - subject: "Re: kformula edition"
    date: 2006-09-05
    body: "There are some features that allow to avoid mouse to some point. Take a look to handbook's Chapter 5: http://docs.kde.org/development/en/koffice/kformula/advanced.html , specially Name Insertion and Definite Integrals and Limits.\n\nAs Cyrille has said, further improvements are already in the todo list :-)"
    author: "Alfredo Beaumont"
  - subject: "Version Control"
    date: 2006-09-05
    body: "Nice addition! I hope this evolves to something like tools in software version management (like SVN or GIT) and also like visual viewing of differences and version merging (KDiff3 integration?).\n"
    author: "KDE Use"
  - subject: "Thanks..."
    date: 2006-09-05
    body: "I want to thank the participants and google as well for its sponsorship.\n\nWhile KDE is in a huge transition right now, when the dust settles, which I think will be about a year from now, we should have one pretty amazing desktop.\n\nHere's to hoping it materializes in all its glory."
    author: "Gonzalo"
  - subject: "Re: Thanks..."
    date: 2006-09-05
    body: "I think we've already got one pretty amazing desktop :)\nCan't wait for it to get even better though!"
    author: "Dustin"
  - subject: "Automatic Heading Recognition"
    date: 2006-09-06
    body: "Hope it does not only work with\n1.\n1.1.\n1.1.1.\n1.1.1.1.\n1.1.1.1.1.\n1.1.1.1.1.1.\n1.1.1.1.1.2.\n1.1.1.1.2.\n1.1.1.2.\n1.1.2.\n1.2.\n2.\n\nbut also the other big header scheme:\nA.\nI.\n1.\na)\naa)\n(1)\n(2)\nbb)\nb)\n2.\nII.\nB.\n\nSometimes you have to use another level below (1), (2): Greek letters alpha, beta, gamma. More levels aren't recommended, though. Still, I've seen a variant with the levels below (1), (2) labelled (a) and (aa).\nI strongly hope the header variant with mixed numbering works, too. Certain branches of study strongly recommend or require the second variant because it's laid out more clearly (the scheme is always the same - aside from the non standardized forms when it becomes nested too deep - and you know at which level you are without deciphering a long range of numbers).\n\nAdditionally, it would be totally cool if KWord could automatically generate a table of contents from these headings (with indentation, page number and space filled in between) like:\nA. Header......1\n\u00a0\u00a0I. Header......2\n\u00a0\u00a0\u00a0\u00a01. Header...4\nB. Header.......4\n\nI think MS Office can do it (by referencing an index; the headers then get recognized after they get assigned to a formatting level -- manually!)\nI tried to do the same in OpenOffice but didn't succeed. Don't know if OO can do it or not. Anyways, doing it by hand is quite painful with copying all headers, getting the formatting right and changing page page numbers till the end.\n\nIf that would work, academic/scientific essays would almost write themselves ;-)"
    author: "Anonymous"
  - subject: "Re: Automatic Heading Recognition"
    date: 2006-09-06
    body: "I've gotten this to work in OOo using styles.  Styles can be very powerful if used correctly, and you can use different numbering systems as you indicated.  I, too, am hoping for this feature in KWord.  It would be nice if, by KDE 4, I could be using all the KDE apps full-time.  The progress is amazing.  Great work all around."
    author: "Louis"
  - subject: "Re: Automatic Heading Recognition"
    date: 2006-09-09
    body: "MS Word can also base the structure of the TOC on user-defined styles.\nA style can base where in the TOC strcture the header will appear, this is a powerful feature of MS Office that I tend to use all the time.\n\nOther styles such as \"Invisible 1, 2, 3\" won't show up in the TOC by default."
    author: "Steve"
  - subject: "KWord 2.0 - wishfull thinking... (tables)"
    date: 2006-09-06
    body: "This might be very off-topic, but I hope that Koffice (K-Office?) 2.0 will have full-fledged table support, like in MS-Word.  I use them for formatting, and use them extensively in my CV, along with styles (bad habit I picked up way back when on some *nix-based typsetting system whose name I forget... damn, I getting old!).  Anywho, I haven't seen any Free alternative that could slurp my CV and keep its layout: the document would almost litterally kill the program I was using.\n\nMaybe it is because MS uses some incredibly convoluted & obfuscated way to store its documents, I don't know, but if one of the Kdeveloppers (sorry) could get KWord 2.0 to make sense out of MS Word's tables & Styles, I could get even closer to getting rid of my last Windows box.\n\n"
    author: "Anonymous Coward"
  - subject: "Re: KWord 2.0 - wishfull thinking... (tables)"
    date: 2006-09-08
    body: "I think KWord is a mix of desktop publishing and word processor. It is based on frames. To this should not be a problem for you. :-)"
    author: "ale"
  - subject: "KOffice Instruction Player"
    date: 2006-09-08
    body: "These are awesome results, if you feel inspired and also want to code a KOffice project, we are looking for someone who wants to develop Instruction Player. It would record KOffice mouse actions and audio files at the same time. Users could then record how they they created a painting, photo manipulation or office documents. It would be played like a commented macroscript and step by step the user can mimic the master. Learn by doing!\n\nThis would ease deployment of KOffice tremendously as users can self train how to complete tasks without reading and studying, simply by doing the same as shown. Such functionality has also been shown to create a big group of followers recording and showing of how they reached certain results.\n\nCommercial developers have already shown it's possible to record Qt actions, if you want to make this community tool a reality please read the enclosed concept, offer your coding skills and find advice on the KOffice email list http://www.kde.org/mailinglists/index.php#koffice-devel\n"
    author: "Dennis"
---
Under the KDE umbrella, the <a href="http://koffice.kde.org">KOffice</a> project took part in the <a href=""http://code.google.com/summerofcode.html>2006 Summer of Code</a> with four participants. And not only that, but the <a href="http://www.programmeerzomer.nl/">Dutch Programmeerzomer</a>, sponsored by Finalist, also selected a KOffice project. The summer is over, the season of mists and long hacking nights has arrived and the question that's obviously in everyone's mind is, have these five delivered? -- and, more importantly, will Gabor, Alfredo, Emanuele, Thomas and Fredrik continue hacking on KOffice?







<!--break-->
<h3>Emanuele Tamponi: Krita</h3>
<div style="float: right; border: thin solid grey; padding: 1ex; margin: 2ex"><a href="http://static.kdenews.org/danimo/emanuele.jpg"><img src="http://static.kdenews.org/danimo/emanuele_small.jpg" width="180" height="281" /></a><br /><a href="http://static.kdenews.org/danimo/emanuele.jpg">Emanuele Tamponi</a></div>

<p>Emanuele Tamponi, an 18 year old student from Calangianus, Sardinia
had the ambitious plan to add three important features to Krita:</p>

<ul>
<li>a bezier curve tool with stroking using Krita brushes</li>
<li>a selection tool based on the bezier curve painting tool</li>
<li>a magnetic outline selection tool</li>
</ul>

<p>Emanuele himself says:</p>

<blockquote>
My SoC application was split in 3 parts: firstly I had to code a framework
to handle curves (that are lists of points, managed by "special points" or
"pivots"); then I had to deploy a tool for Bezier Curves and for Magnetic
Outline Selection based on the Framework.
</blockquote>
<blockquote>
Bezier Curves are curves created by applying a recursive function on a set
of up to four points: the result is a smooth curve, ideal for all types of
design (from cars to fonts...).
</blockquote>

<div style="text-align:center">
<a href="http://static.kdenews.org/danimo/krita_bezier1.png">
<img src="http://static.kdenews.org/danimo/krita_bezier1_small.png" width="480" height="320"></a>

<a href="http://static.kdenews.org/danimo/krita_bezier2.png">
<img src="http://static.kdenews.org/danimo/krita_bezier2_small.png" width="480" height="265"></a>
</div>

<blockquote>
Magnetic Outline Selection (MOS for short) is like the "Magnetic Lasso"
tool from Photoshop or the "Select part of the image" (Scissors tool) from
the GIMP. My MOS just behaves better than both:
</blockquote>
<blockquote>
First I compared two tools for the "intelligent scissors": Photoshop one
and gimp's one.
</blockquote>
<blockquote>
1) Photoshop follows your mouse while *moving* (not just dragging) it,
adding other control points when the mouse is getting too far from the
last control point. This way, it can be fast (just small area of the image
are computed) and precise (because it *follows* the mouse, so it follows
the edge that *you* want to select).
</blockquote>
<blockquote>
2) Gimp does it in a more "standard" way: it calculates the edge between
two clicks of the mouse. It's fast because it calculate the edge only once
per click, but it's not precise because you can't see the curve generated
*while* you follow the edge.
</blockquote>
<blockquote>
But gimp gives you the possibility to *edit* (moving or deleting) already
inserted control points, whereas Photoshop can only delete the last
inserted control point, without other types of editing *during* the line
makeup (with Photoshop you can edit the control points *after* you close
the curve, and this is not always the right solution).
</blockquote>
<blockquote>
My MOS mixes both: it follows the mouse as Photoshop does, but if you want
to edit a control point, you just hit "Ctrl" and you switch to Editing
Mode, then edit, add or delete the control point you want, and when you're
done, you can either it "Ctrl" again to return to "Automatic
(Photoshop-like) Mode", or hit "Enter" or the button on the Options Widget
to end the selection.
</blockquote>

<div style="text-align:center">
<a href="http://static.kdenews.org/danimo/krita_mos1.png">
<img src="http://static.kdenews.org/danimo/krita_mos1_small.png" width="352" height=""40"></a>

<a href="http://static.kdenews.org/danimo/krita_mos2.png">
<img src="http://static.kdenews.org/danimo/krita_mos2_small.png" width="510" height="346"></a>
</div>

<p>Emanuele was mentored by Bart Coppens and did indeed finish his project on
time and on spec, and is now getting ready to port his work to KOffice 2.0.
His tools will first be release in KOffice 1.6 -- real soon now!</p>

<h3>Alfredo Beaumont: KFormula</h3>

<div style="float: right; border: thin solid grey; padding: 1ex; margin: 2ex"><a href="http://static.kdenews.org/danimo/alfredo.jpg"><img src="http://static.kdenews.org/danimo/alfredo_small.jpg" width="180" height="239" /></a><br /><a href="http://static.kdenews.org/danimo/alfredo.jpg">Alfredo Beaumont</a></div>

<p>Alfredo Beaumont Sainz, from <a href="http://en.wikipedia.org/wiki/Gasteiz">Gasteiz</a>,
Basque Country, worked on adding MathML
to Kformula. He studies <a href="http://www.ehu.es">telematics<a/> and
<a href="http://www.uned.es">philosophy</a>:

<blockquote>
My SoC
project consisted in adding full native support of MathML / OpenDocument 
format to KFormula. There were also some related tasks that needed to be 
accomplished, such as improving mathematical font support, rewriting user 
interface to take advantage of new functionalities allowed by MathML and 
extending / rewriting old native format code for backwards compatibility.
</blockquote>
<blockquote>
I think that project was quite successful, even if I haven't reach 100% of the 
objectives. We have now native support of both MathML and OpenDocument, with 
an internal layout that resembles very well these formats' layout. We have 
achieved nearly full support (more than 70% of tests passed, comparing to 20% 
of old KFormula and 22% of OOMath2). New font engine has been developed, 
replacing old TeX font support with new Unicode font support. KFormula now 
includes Arev fonts with great mathematical support by Tavmjong Bah, and 
allows the user to choose whichever Unicode font they have installed. That's 
all I did during SoC period, but till then I have been working in the fourth 
task, and some UI changes have already been added. All these features can be 
seen in action in KOffice 1.6 beta that will be released in a week. Still 
improved MathML / ODF support, refined UI and extended old format are 
expected for final 1.6 release.
</blockquote>
<blockquote>
A preview of the work done can be seen in the screenshot: Navier's equation 
with each part of the equation highlighted with a different background.
</blockquote>

<div style="text-align:center">
<a href="http://static.kdenews.org/danimo/kformula-navier.png">
<img src="http://static.kdenews.org/danimo/kformula-navier_small.png" width="330" height="212"></a>
</div>

<p>Alfredo is now the official maintainer of KFormula. His mentor was David Faure.</p>

<h3>Fredrik Edemar: KWord</h3>

<p>Fredrik Edemar, a computer science student from Uppsala, Sweden. has
have worked on two smaller projects: automatic heading recognition in KWord 
and version support for all KOffice applications. Both these new features 
will be included in KOffice 2.0.</p>

<blockquote>KWord now recognizes headings and puts them in a tree view. This makes it 
easier for a user to get an overview of large documents. From the list, it is 
possible to move and delete headings.</blockquote>

<div style="text-align:center">
<a href="http://static.kdenews.org/danimo/kword_headings.png">
<img src="http://static.kdenews.org/danimo/kword_headings_small.png" width="374" height="350"></a>
</div>

<blockquote>Version support can be very handy if several people are writing in one
document. Let us assume that the first user saves the file, and the next 
wants to make changes. Then she can create a new version with the old 
content, do her changes and finally save her work. In that way it is possible 
to later go back and see what the first writer saved. The versions are off 
course stored in the OpenDocument file format.</blockquote>

<div style="text-align:center">
<a href="http://static.kdenews.org/danimo/kchart_versions.png">
<img src="http://static.kdenews.org/danimo/kchart_versions_small.png" width="430" heigth="344"></a>
</div>

<p>Fredrik was mentored by Boudewijn Rempt. As soon as the current sweeping redesign of KWord's
frame system is done, Fredrik will port the header feature to KWord 2.0; the versions feature was
developed in 2.0 already.</p>

<h3>G&aacute;bor Lehel, KOffice core</h3>

<p>Mentored by Cyrille Berger, Gabor Lehel coded a second-generation widget for all of KOffice, based
on his work for Krita's layers widget.</p>

<blockquote>
Many of KOffice's applications use a similar concept for dividing documents up into parts.
Krita and Karbon have layers, KPresenter has slides, KWord has pages. I wrote
a widget to display a list of these in a uniform way across KOffice 2.0 applications.
</blockquote>

<blockquote>
It is implemented as a QAbstractItemDelegate and likely a QAbstractItemView subclass,
and each application can provide their own QAbstractItemModel subclass, tailored to
the application's data format. Two main view modes have been created: a thumbnail view with only
large thumbnails and possibly a name / label below them (as in KPDF), which would be most
suitable for KWord and KPresenter, and a detailed view, with smaller thumbnails and
information next to them, including editable properties (as in Krita 1.5), which would
be most suitable for Krita, Karbon and Kivio. But all applications can show both modes,
so it's possible to get really large layer previews in Krita or detailed meta information
about pages in KWord.
</blockquote>

<div style="text-align:center">
<a href="http://static.kdenews.org/danimo/krita_layers.png">
<img src="http://static.kdenews.org/danimo/krita_layers_small.png" width="430" height="205"></a>
</div>

<h3>Thomas Schaap: KOffice Core</h3>

<p>Thomas Schaap studies in Delft, the Netherlands and was selected by Finalist to participate
in the Dutch Programmeerzomer. Having wanted to work on KOffice for quite some time, he grabbed
this opportunity to really get into KOffice. Brad Hards was the real, practical mentor,
while Boudewijn Rempt was his pro-forma mentor.</p>

<p>The project was to provide support for encrypted documents in KOffice. The big difficulty, which is still not completely solved, is compatibility with OpenOffice encrypted
documents. Thomas not only had to learn KOffice and its development culture, but also needed to learn
about OpenOffice and find his way on their mailing lists. And then, of course, Thomas needed to
work in KOffice trunk. The day his project began was also, coincidentally, the first day that KOffice
2.0 actually compiled against KDE 4!</p>

<p>But despite these difficulties and a very tight schedule,
he managed to finish all core functionality and get it checked in -- as Thomas says himself: "I did
a good job of, the KOffice guys are really happy with what I did." Next stage: signed documents!</p>

<h3>Conclusion</h3>

<p>Five projects, five successes! Thanks Thomas, G&aacute;bor, Fredrik, Alfredo and Emanuele for your
work, and thanks Google and Finalist for organizing these projects!</p>




