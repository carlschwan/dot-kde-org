---
title: "KDE Multimedia Meeting in the Netherlands"
date:    2006-05-19
authors:
  - "j(superstoned)"
slug:    kde-multimedia-meeting-netherlands
comments:
  - subject: "Is this the same Novell "
    date: 2006-05-19
    body: "that hates KDE and only wants Gnome on its desktop?\n\nJust thought I'd ask...\n\nThank you to all the sponsors, I remain grateful to everyone that makes KDE what it is. \n\n"
    author: "gerryg"
  - subject: "Re: Is this the same Novell "
    date: 2006-05-19
    body: "no, it's the same novell that loves kde, but wanted to make gnome default in their products.\n\nDunno if that is still the case when i look at suse 10.1: most new technologies are kde-based (kerry, knetworkmanager, etc..), and they ship an old gnome-version (gnome 2.12, while gnome 2.14 has been out for several months now)\n"
    author: "AC"
  - subject: "Re: Is this the same Novell "
    date: 2006-05-20
    body: "I would say it is the Novell who bought Ximian and Suse and made the mistake to get infiltrated by Ximian fanatics."
    author: "hup"
  - subject: "Re: Is this the same Novell "
    date: 2006-05-19
    body: "> Is this the same Novell\n> that hates KDE and only wants Gnome on its desktop?\n\nI didn't know such a Novell exists. Yeah, maybe they've got a slight preference for GNOME thanks to the Ximian people, but they're still a strong supporter of KDE as well. Don't bring on the FUD, instead, let's all live together in collaborative harmony! ;-)"
    author: "Jakob Petsovits"
  - subject: "GPL vs. LGPL"
    date: 2006-05-19
    body: "They and other commercial organisations seem to favour GNOME because the GNOME licenses (LGPL as opposed to GPL) make it possible for them to eventually develop close-source products on top of that desktop.  KDE is (now) more free in this respect -- as Stallman himself has said, LGPL represents a retreat for Free Software.  However, if that *is* a large part of their reasoning, then I think it's a hard position to justify.  No one should be steering the technology choices on Free Desktops on any grounds except technology and Freedom, imho."
    author: "Lee"
  - subject: "Re: Is this the same Novell "
    date: 2006-05-19
    body: "This is the same Novell that probably sponsors more KDE developers than anyone else (three of the seven in the KDE technical working group are employed by SUSE/Novell), the same Novell that has created SUSE Linux 10.1 with a wealth of kde features and a very strong desktop environment. \n\nSo, it's really a bit silly to make ridiculous and totally baseless statements as the one above."
    author: "apokryphos"
  - subject: "Advanced audio features in amaroK-upon-Phonon"
    date: 2006-05-19
    body: "Will Phonon support advanced audio features, such as gapless audio playback? amaroK uses stuff like that, and amaroK is _VERY_ important in the KDE world..."
    author: "NabLa"
  - subject: "Re: Advanced audio features in amaroK-upon-Phonon"
    date: 2006-05-19
    body: "several amaroK developers work on Phonon as well, so i expect that amaroK won't loose functionality because of phonon ;)\n"
    author: "AC"
  - subject: "Re: Advanced audio features in amaroK-upon-Phonon"
    date: 2006-05-19
    body: "Gapless playback doesn't work on Amarok with GStreamer or XINE engines, even on gapless media such as oggs. \nThis bug has been confirmed by multiple people, and lies with the backend people, so you need to ask XINE or GStreamer if they will add this support, rather than Phonon."
    author: "Mike Arthur"
  - subject: "Re: Advanced audio features in amaroK-upon-Phonon"
    date: 2006-05-19
    body: "The problem is not actually with Amarok itself, which does it's best to do the crossfading, but with issues in the respective engines, I believe. Even with gapless playback, you need to preload the audiobuffer with the next file's decoded content, which means it needs to be opened well before the previous one ends.\n\nWith the new version of libxine that came out recently, crossfading and gapless playback works perfectly for me, without any changes to Amarok."
    author: "John Hughes"
  - subject: "Ex-Ximian employees working to destroy KDE"
    date: 2006-05-19
    body: "It was Greg Mancusi-Ungaro, Ximian's director of marketing and now Novell's, who said that Novell will standardize on gnome:\n\nhttp://linux.slashdot.org/article.pl?sid=05/11/05/1620206\n\nThis was a terrible blunder by Novell since it alienated many of its customers who are KDE users, but it was a win for the ex-Ximian employees who want to strangle Novell.\n\nJust goes to show that what's good for Ximian is really hurting Novell's reputation. If I were Novell, I'd throw out the whole bunch of primates -- Ximian has always been all hype and no products. I've never seen so much incompetence in my life! If Novell keeps Ximian, they'll go bankrupt. \n\nHistorically, Ximian has resorted to ditry Microsoft-like tactics against KDE, such as sponsoring Google ads to lure people to Ximian's site when someone searches for a KDE-related word:\n\nhttp://www.linuxplanet.com/linuxplanet/opinions/3015/1/"
    author: "Bill"
  - subject: "Re: Ex-Ximian employees working to destroy KDE"
    date: 2006-05-19
    body: "Now that's nasty, although not at least surprising.\n\nI think that's what you get when don't want to admit having created the inferior alternative :-D"
    author: "wi"
  - subject: "Re: Ex-Ximian employees working to destroy KDE"
    date: 2006-05-20
    body: "Ximian aren't those the same that started GNOME in the first place. KDE was already there, instead of supporting it, the started there own stuff.\n\nThis ofcourse has advantages, like KDE and GNOME looking at each others stuff, for Ideas, why not? But KDE already has Windows and OSX to look at how things are presented differently. I think if GNOME would never have existed, maybe some people would have made some KDE settings better (like basic settings and advanced settings?, so as to hide lots of extra settings). On the other hand, I don't understand those at GNOME, first they throw a lot of settings out, like putting a default which nobody knows how to change it, except by using gedit or so (like windows registry). And now in the next version they are adding lots of configuration dialogs, what KDE had for a long time...\n\nA little competition in the desktop marked is ofcourse okay, but I think the overall picture hurts linux as a desktop, because the programs that are made outside of KDE just make a quick layer to GNOME components, to be able to work under linux (see Firefox, OpenOffice), instead to KDE-Qt. And even here would it really matter if OpenOffice was based on Qt directly? Firefox until today when they speak about Linux support they only mean GNOME support, which doesn't make the program any better...\n\nI'm looking forward for KDE 4, where some parts would also work on Windows, hopefully it will make KDE more known outside of the linux world. Still hoping to be able to play KPatience on Windows too!!!"
    author: "boemer"
  - subject: "Re: Ex-Ximian employees working to destroy KDE"
    date: 2006-05-20
    body: "\"Ximian aren't those the same that started GNOME in the first place. KDE was already there, instead of supporting it, the started there own stuff.\"\n\nWhat you're forgetting is that KDE was not a free desktop at the time. By your logic you should support CDE, as it was already there.\n\nGNU sponsered two projects to solve the KDE situation: a free desktop (GNOME) and a free replacement for Qt (Harmony was the name, I believe). \n\nQt is now free, but I guess you weren't around when that was not the case.\n"
    author: "Reply"
  - subject: "as fun as it is ..."
    date: 2006-05-19
    body: "given the article, perhaps we could concentrate on the multimedia strides that will come out of this meeting and all the hard work everyone is putting into it rather than going on about the issues as we may perceive them within novell.\n\nwithin the context of this meeting, all i have to say to novell is \"thank you for supporting the efforts of the kde and open source multimedia efforts through your sponsorship of this event\"\n\nditto for all the others sponsors. there has been a serious amount of coin put on the table to make this happen and a huge amount of the volunteer coordinator's time and energy. it's pretty cool that this sort of stuff is happening. in past years such a thing was just a nice dream. \"imagine hosting multi-day meetings costing five figures with 15+ people from all over the world to work on specific aspects of kde\". this used to happen for kde in general (aKademy/kastle, the kde2 meeting, etc...), but now it's also happening for subprojects pim, multimedia, artwork ... viewed as a year-over-year trend, we're gaining steam.\n\nexciting times.\n\nhope the MM people have an -awesome- and productive time in achtmaal"
    author: "Aaron J. Seigo"
  - subject: "Re: as fun as it is ..."
    date: 2006-05-20
    body: "I agree. \n\nNovell is a business and they make money by providing their customers with what the customers want.  So, if customers want KDE, I expect that Novell will continue to provide those customers with KDE.  Obviously, they must have thought that some customers would prefer Ximian/GNOME or they wouldn't have bought the company.  I expect to see Novell continuing to provide both desktops and supporting development of both desktops as long as customers want them.\n\nNo conspiracy theories please. :-)"
    author: "James Richard Tyrer"
---
Multimedia in KDE has been in the news lately, especially <a href="http://phonon.kde.org">Phonon</a>, the new multimedia framework for KDE 4. Phonon still needs a lot of work, as do the applications which are going to use it. So, in the spirit of the previous <a href="http://dot.kde.org/1117213451">KDE PIM meeting</a>, Annahoeve in Achtmaal, The Netherlands, will again be visited by a group of KDE developers. From Friday the 26th to Sunday the 28th of May, more than 15 developers from 4 continents will have a unique chance to talk about and work on Multimedia in KDE.





<!--break-->
<div style="border: thin solid grey; padding: 1ex; margin: 1ex; float: right">
<img src="http://static.kdenews.org/jr/kde-multimedia-kdepim.jpg" width="300" height="200" /><br />
KDE PIM Meeting
</div>

<p>The aim of the meeting will be to make the ever so invasive role of multimedia less of a burden to manage for users and developers. With the great service and ambiance that Annahoeve provides, the developers, supported by two usability experts, can focus on the things they came for: discussing, thinking and creating. Meeting and working in personal can and will enable them to lay the groundwork for new innovations and will provide a bonding experience. At the meeting, a multimedia roadmap for KDE 4 will be drawn. Also, usability work on several multimedia applications like amaroK 2 and Kaffeine will be done. Phonon could use some backends, and many applications have to be ported. </p>

<p>Phonon will enable features like per-application volumes, easy use of several soundcards or playing over a network. Also, a lot of duplication will be avoided. Currently, many applications like Amarok and Kaffeine support several multimedia engines through a plugin-like structure. With Phonon applications do not have to do this themselves. Adding rich multimedia support like audio and video will be much easier, thanks to Phonon. Finally, binary compatibility can be guaranteed for the lifetime of KDE 4. </p>

<p>We would like to thank the following sponsors for making it possible by paying for the travel expenses and supplying hardware and lodgings.</p>

<p><img src="http://static.kdenews.org/jr/kde-multimedia-sponsors.png" /><br />
Sponsors of the KDE Multimedia Meeting</p>



