---
title: "Planning For 10 Years of Linux Desktop"
date:    2006-09-03
authors:
  - "jhoh"
slug:    planning-10-years-linux-desktop
comments:
  - subject: "Don't you mean 10 years of KDE?"
    date: 2006-09-03
    body: "When I saw this post, I was thinking of ten years of freedesktop.org ... but it's not been that long, has it?  Good job I didn't blog that."
    author: "Matt Smith"
  - subject: "Re: Don't you mean 10 years of KDE?"
    date: 2006-09-03
    body: "i propose changing the title to \"10 years of THE Free Desktop\" just to upset our gnome friends :)"
    author: "AC"
  - subject: "Re: Don't you mean 10 years of KDE?"
    date: 2006-09-03
    body: "well, read the gnome site - they usally refer to gnome as 'the free desktop' or 'the linux desktop'. like it's the only choice... so i guess we should do that once in a while as well ;-)"
    author: "superstoned"
  - subject: "Re: Don't you mean 10 years of KDE?"
    date: 2006-09-03
    body: "Because your neighbour is naughty does not mean you are to get naughty, too, right? Trust the people to find out who is cheating too much at them, and who is not (that much)... \nJust behave, like you want others to behave. Alikes will come together. And who likes overcheaters around? :)"
    author: "Shuby"
  - subject: "Re: Don't you mean 10 years of KDE?"
    date: 2006-09-03
    body: "What... are there other desktops? ;) "
    author: "Derek R."
  - subject: "Re: Don't you mean 10 years of KDE?"
    date: 2006-09-03
    body: "Only a few. But maybe a rumor."
    author: "Shuby"
  - subject: "Re: Don't you mean 10 years of KDE?"
    date: 2006-09-03
    body: "None other that would matter ... ;)"
    author: "Patrick Trettenbrein"
  - subject: "Re: Don't you mean 10 years of KDE?"
    date: 2006-09-03
    body: "Ok, fixed. The original article that J\u00f6rg submitted did actually have the \"Planning for 10 Years of Linux Desktop\". To avoid confusion and flamewars and to maximize attention we actually decided for that one. Additionally if you look at the original announcement that's what it was mostly about. Of course it will be a great day for Free Software :-)  "
    author: "Torsten Rahn"
  - subject: "Re: Don't you mean 10 years of KDE?"
    date: 2006-09-03
    body: "Attaching \"linux\" to KDE is not the best idea, IMVHO. KDE has been a desktop for X11, not only Linux, and starting with KDE 4 it will be much more than just Unix."
    author: "Anonymous Coward"
  - subject: "10 years of KDE!"
    date: 2006-09-03
    body: "But Linux has been the major and also most deployed target platform. So the last 10 years KDE has been a/the free desktop for Linux. :)\n\nBut sure, this is about 10 years KDE!\nWhich means to everyone something different, for most surely something very good! And with KDE 4 supporting more platforms than ever the spread of meaning will even grow!"
    author: "Shuby"
  - subject: "DevDays"
    date: 2006-09-03
    body: "Trolltech DevDays is in Munich on the 11th and 12th. So if you're going to DevDays, consider making it a long weekend and head over to Stuttgart."
    author: "Brandybuck"
  - subject: "Editor/Writer for LXer.com"
    date: 2006-09-05
    body: "Many Congratulations and Thank You's from me personally. \n\nI think that KDE is THE Desktop Enviroment and I know that I speak for others when I say..PLEASE DON'T STOP!! :-)\n\nKDE makes the experience of using Linux an absolute joy. Period."
    author: "Scott R. Ruecker"
---
10 years ago, on October 14th 1996, Matthias Ettrich <a href="http://www.kde.org/announcements/announcement.php">announced a project</a>
to create a complete and consistent GUI for the prospering Linux
operating system. The project grew and matured and now it is 2006 and KDE
is one of the largest Free Software projects. To celebrate this
anniversary the KDE project encourages the community to <a href="http://wiki.kde.org/10+years+linux+desktop">organise events
all over the world</a>, to meet and party and celebrate Free Software. The
main event will take place in Stuttgart, Germany on October 13th (we'll
party into October 14th) when the KDE e.V. invites the community,
business partners and friends of KDE to celebrate this outstanding
achievement. Other events are scheduled in the Netherlands (Nijmegen) and
Portugal.  For further information and questions you can visit #kde-anniversary on irc.kde.org or subscribe to <a href="https://mail.kde.org/mailman/listinfo/kde-promo">kde-promo</a>.










<!--break-->
