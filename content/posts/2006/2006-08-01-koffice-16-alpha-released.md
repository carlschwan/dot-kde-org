---
title: "KOffice 1.6 Alpha Released"
date:    2006-08-01
authors:
  - "iwallin"
slug:    koffice-16-alpha-released
comments:
  - subject: "Make it stable, devs!"
    date: 2006-08-01
    body: "I like the pace of KOffice development but it's very annoying that KOffice applications often crash when doing simple operations."
    author: "Artem S. Tashkinov"
  - subject: "Re: Make it stable, devs!"
    date: 2006-08-01
    body: "Thanks. Maybe you're lucky man or able to track down crashes more easily than others? In any case, what about contacting the devs using proper tools, i.e. http://bugs.kde.org ?\n\ndot.kde.org is not a place for such contribution\n"
    author: "Jaroslaw Staniek"
  - subject: "Re: Make it stable, devs!"
    date: 2006-08-01
    body: "We really, really need a small team of dedicated users to do regression testing for KOffice. Regression testing is not something easily automated, and not something developers should do -- because developers know the right path through the application. Ideally, we'd start with a set of regression tests based on existing bug reports augmented by a set of regression tests that cover all features of all KOffice apps, and have real people exercise those tests during the beta's.\n\nI even started writing a web application for that, something like bugzilla but for tests and with statistics about the best tested app, the team that's got the most complete coverage and things like that. I might pick up on that in September.\n\nBut even right now, I hope that you'll be among the people who download the alpha's and beta's, test it rigorously and mail your results to the mailing list or put them in bugzilla. Or contact the KOffice hackers on irc about it."
    author: "Boudewijn"
  - subject: "Re: Make it stable, devs!"
    date: 2006-08-01
    body: "But it is somehow frustrating to test and report problems, if really heavy bugs are not fixed for a long time. One example, which makes kspread unusable with formulas: 114635 The bug is in 1.4.2, 1.5, 1.5.1, 1.5.2 \nAnd even bug fix releases of koffice are gone public with such critical things open!\nSo a kspread user is forced to use other programs (because you can't save your data in a looping application) and will not continue to report bugs in kspread in future."
    author: "gpl"
  - subject: "Re: Make it stable, devs!"
    date: 2006-08-01
    body: "That's where this team of regression testers is different from ordinary users reporting bugs: their task is to make sure problems are caught before release, and that regressions (where things that used to work don't work anymore) are caught, too. This is an effort to help the developers reach good and dependable releases.\n\nOf course, alpha and beta releases are the places where regressions should be caught. It's more than folly, it's actively malicious to review alpha releases as if they were full releases like Linux Format did with KOffice 1.5 and then to decry the bugginess of applications. The regression testing team should concentrate their work during the beta phase.\n\n(Oh, and from the bug report you mention, it's clear that work is being done on it, it's just that it looks hard to fix. It's not being ignored...)"
    author: "Boudewijn"
  - subject: "Re: Make it stable, devs!"
    date: 2006-08-01
    body: "I wouldn't mind testing KOffice if it is simple enough to put beta version next to a stable one. \nI'll explain:\nWith OpenOffice, for example, you could install both OO-1.1 (stable) and OO-1.9.x (unstable) for the same user, and simply run one or the other. The setting of one version would not affect the other, since they sit in different directory (~/.openoffice-<version>).\n\nIs it that simple with KOffice?\nCan you give a brief description of how to do that, and point me to the right web page where it is explaind?\n\nregards,\nyuval."
    author: "yuval"
  - subject: "Re: Make it stable, devs!"
    date: 2006-08-01
    body: "http://klik.atekon.de/\n\nEasy to install and remove. Doesn't touch your core system."
    author: "sundher"
  - subject: "Re: Make it stable, devs!"
    date: 2006-08-01
    body: "I am running FC5, and usually installing software with yum (i.e. RPMs).\nI never used Klick.\nHow does Klick integrates with Yum?\nCan I be sure that the system will stay consistent, (in terms of libraries, path, rpm database, etc. ) if using both Yum and Klick?"
    author: "yuval"
  - subject: "Re: Make it stable, devs!"
    date: 2006-08-01
    body: "Klik doesn't actually install (well other than klik itself, which is really small) anything on your system (everything stays in a .cmg file, which is a compressed file system image).  Klik won't interfere with Yum at all.\n\nThough I'm not sure if klik is the solution to this problem."
    author: "Corbin"
  - subject: "Re: Make it stable, devs!"
    date: 2006-08-01
    body: ">>How does Klick integrates with Yum?\nI does not.\nKlik does not install anything, it fetches the packages from internet and creates a cmg image (comparable to dmg from macosX). To run koffice, simply click on the cmg-image. Klik will mount the image and start the applcation that is inside it. When you close the application, the image will be unmounted.\n\n\n\n>> Can I be sure that the system will stay consistent, (in terms of libraries, path, rpm database, etc. ) if using both Yum and Klick?\n\nYes, if you want to remove an application that was installed with klik, you simply remove the corresponding cmg-image. That's it. Klik does not alter any systemfiles or libraries.\nAFAIK klik does not even touch your personal settings (e.g. files in ~/.kde), but creates new ones. So running koffice 1.6 alfa does not in any way interfere with koffice 1.5 that you installed with rpm/yum\n\n"
    author: "Rinse"
  - subject: "Re: Make it stable, devs!"
    date: 2006-08-02
    body: "But, doesn't klik programs still use the same configuration files than installed versions? If that's the case, running a newer version of an already installed program might change the settings in a way not supported by the installed one."
    author: "jesjimher"
  - subject: "Re: Make it stable, devs!"
    date: 2006-08-02
    body: "So I install Klik.\nWhen I Klik on KDE/KOffice, I am asked if I want to download many packages.\nTwo of them are:\nhttp://debian.tu-bs.de/debian/pool/main/k/koffice/kword_1.3.5-4.sarge.3_i386.deb http://debian.tu-bs.de/debian/pool/main/k/koffice/koffice_1.3.5-4.sarge.3_all.deb\n\nIt doesn't seem to me as version 1.6a, is it? \nIt seems more like version 1.3 ..."
    author: "yuval"
  - subject: "Re: Make it stable, devs!"
    date: 2006-08-03
    body: "I do it normaly that way;\n\n1. Create a new user.\n2. Install the software in the home-directory of the new user.\n3. Use kdesu (or sudo to don't get asked for a password) to execute the software installed as another user in the session of the user you are currently \nlogged in as.\n"
    author: "Sebastian Sauer"
  - subject: "Re: Make it stable, devs!"
    date: 2006-08-05
    body: "Regression testing is only one part of it. You need defined testcases, synchronised with implementet changes, to do the this kind of testing or otherwise its meaningless.\nRegressiontesting from a user standpoint is relatively easy and fast to do. \"Real testing\" is much harder and time consuming and i would really love ot see a testset that could test the intended functionality of the system. For this there is a need of a tool like Mercury Test Director. Is there any equivalent tool in the Open Source world?\nIf there is a  would certainly volunteer to write testcases and do some more formal testing.\n"
    author: "josel"
  - subject: "Watercolor and other paint tools in Krita"
    date: 2006-08-01
    body: "A while ago, it was said that tools to make watercolor painting and other kinf of Painter-like tools was planned, even being developped for Krita. The goal was to differentiate from Gimp in providing such tools aimed at creating content.\nAll the recent additions seem on the contrary to be around photoshop-like features, closer to what Gimp has to offer (going beyong in places, like CMYK support).\nDid the idea of supporting this type of tools was dropped?"
    author: "Richard Van Den Boom"
  - subject: "Re: Watercolor and other paint tools in Krita"
    date: 2006-08-01
    body: "No, not dropped, but we've had a lot of setbacks in that area. \n\nI actually have most of a chinese brush simulation done and  the beginning of a fun oilpaint brush. But I got kind of burned out after the 1.5 release and didn't do much towards 1.6 or 2.0. Which means it didn't get finished. And Leonardo, an Italian hacker got run over by a car before he could commit his watercolor stuff, and he's been out of action for a couple of months now, although he's expected to come through fine. The watercolor stuff that's already in Krita doesn't want to dry for some reason, and I cannot find the problem, nor can Bart Coppens. And before I got burned out, I was permanently distracted by bug fixing, release management, trying to come up with a shared openraster file format and lots of other things. Cyrille has been working on a Corel Painter style programmable brush, but I don't that that's done either, otherwise it'd be in 1.6.\n\nKrita is really ready for painterly stuff: the core supports any type of colorspace, regularly running filters to simulate drying and all that. Now it just needs to be finished, debugged and included."
    author: "Boudewijn"
  - subject: "Re: Watercolor and other paint tools in Krita"
    date: 2006-08-01
    body: "Wow, thats a lot of bad luck all at once.\n\nWell, maybe I'll poke around the code and try putting some fixes in.\n\nCheers,\nBen"
    author: "BenAtPlay"
  - subject: "Re: Watercolor and other paint tools in Krita"
    date: 2006-08-01
    body: "Ugh. Just when I thought I had an unlucky day myself I thought I might read some KDE news to cheer me up...  Well, that didnt quite work except that\nothers seem to be even more unlucky. \n\nThe internet is somehow a strange place. All those people seem so close but on a bad day or if you feel burnt out they all feel distant. That's because the net transports information so much better than emotion and sympathy. Some appreciating nod or slap on the back - this is all lost in communication.  Hope you know what I mean. So I'm taking the chance to note here for all those working on Open Source software and especially KDE :\n\nThanks so much for all the work you put into this. So many people around me (including myself) are using it every day and have come to rely on it for their work and private use. I've already edited countless images with Krita so I sincerely hope you are aware that there are LOTS of people out there you have never heard off that really appreciate your work and that we all know very well that it's not granted someone puts so much work into sth. during their spare time. If you feel burnt out please take it seriously and take your time off. Nothing is more precious than one's health.\n\n"
    author: "Martin"
  - subject: "Re: Watercolor and other paint tools in Krita"
    date: 2006-08-01
    body: "Martin, well said.  +1 for me!\n"
    author: "Richard"
  - subject: "Re: Watercolor and other paint tools in Krita"
    date: 2006-08-02
    body: "Thank you very much for the feedback. I understand very well that open source developers may feel exhausted and tired of working on such big projects at some point. Seems quite normal to me and I hope you didn't take my questions as some sort of criticizing. I know it can be quite hard indeed, especially when bad luck comes on top of it.\nI cannot offer help except bug tracking, but I'm sure people will step in at some point to provide help. Good luck and thanks for what's already done."
    author: "Richard Van Den Boom"
  - subject: "How to help"
    date: 2006-08-02
    body: "This is directed at to all people out there...\n\nYou write:\n\"I cannot offer help except bug tracking...\"\n\nI don't think people really understand how big a help this really is for the developers. When somebody creates a short, to the point and concise bug report, complete with how to reproduce a bug, it gives the developer a well defined task to work with. It is much, much faster to fix such a bug than to hear that \"so-and-so feature doesn't work\":\n\nI'd also like to point out that there are several other ways that you can help even if you are not programmers. You can write documentation, you can create templates, you can create graphics, you can do UI design. You can also help spread the word or write magazine articles.\n\nNothing of this is as difficult as you might think. I know that some of the people in KDE development are totally awesome (and Boudewijn is one that comes to mind), but most people are pretty average. They dedicate some hours a week to KDE and still lots and lots of things get done. It's because of the numbers that we can make the software grow, not because we have a few dedicated souls who do all the work themselves.\n\nThis is a very good time to start developing for KOffice, even if you are unsure about your own abilities. We have several developers who started out without knowing much to start with, but who quickly got very productive. Join us in #koffice on IRC, or look at our website, http://www.koffice.org/.  There is a special section for developers where a number of simpler tasks, so called Junior Jobs, are listed. I can guarantee that we are a friendly bunch, and that we will have suggestions on where you could start to wet your feet.\n"
    author: "Inge Wallin"
---
Swiftly following the latest bugfix release for KOffice 1.5, the KDE Project today announced the release of KOffice 1.6 alpha.  This is the first preview release for KOffice 1.6, scheduled for release this October. <a href="http://www.koffice.org/">KOffice</a> is an integrated office suite with more components than any other suite in existence.  KOffice 1.6 is mainly a feature release for Krita and Kexi while the new <a href="http://dot.kde.org/1149518002/">revolutionary KOffice 2.0</a> is being developed, Read the <a href="http://www.koffice.org/announcements/announce-1.6-alpha1.php">full announcement</a> and <a href="http://www.koffice.org/announcements/changelog-1.6alpha1.php">the changelog</a> for more details or read on for the full article.





<!--break-->
<p>This release specifically introduces the following highlights:</p>
<ul>
<li><b>Lots of new exciting features for Krita and Kexi</b>
<br />
Krita and Kexi has gained many new features and enhancements of old features. There are too many to list here, so for more details look in the announcement, or
  <a href="http://www.koffice.org/announcements/changelog-1.6alpha1.php">the complete changelog</a>
</li>
<li><b>OpenDocument Support in KFormula</b>
<br />
KFormula has received a great workover. Among the most important changes is
almost complete support for the OpenDocument file format which is now
the default format.
</li>
<li><b>Scripting support in KSpread</b>
<br />
 KSpread, the spreadsheet program, now supports scripting in Python and Ruby just like Krita and Kexi. This is done through the KOffice scripting engine
Kross.
</li>
</ul>

<p>Of course, there are also fixes for a number of bugs and introduces
a number of lesser features.</p>

<p>This is a technology preview, and will only be available in source format.
The sources can be downloaded from <a href="http://download.kde.org/download.php?url=unstable/koffice-1.6-alpha1/koffice-1.5.90.tar.bz2">the KDE download servers</a>, and compiled on 
any unix machine with kdelibs 3.3 or newer (not 4.0). See the announcement and the source code itself for more details.</p>

<p>The final release of KOffice 1.6 will not only be more polished, but will also contain several features that were not yet finished for this preview.  This is a very good time for developers who have wished to contribute to KOffice to come in and have a look around.</p>


