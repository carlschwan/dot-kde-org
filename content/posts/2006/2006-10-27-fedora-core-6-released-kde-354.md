---
title: "Fedora Core 6 Released with KDE 3.5.4"
date:    2006-10-27
authors:
  - "kkofler"
slug:    fedora-core-6-released-kde-354
comments:
  - subject: "glibc/linker changes"
    date: 2006-10-27
    body: "I have kubuntu edgy and fedora core 6 on my laptop. KDE programs seem to start\na little faster on core 6, probably due to the DT_GNU_HASH option, and also\nnewer fontconfig.\n\nOpenoffice coldstart seems the same on both ( around 14 sec.), but warmstart\ndiffers a lot:\n\nEdgy:  ~3 seconds\nCore6: ~1 second \n\n:-)\n\nCan't wait for feisty, which should get these improvements as well.\n"
    author: "ac"
  - subject: "Working on KDE 4 snapshot packages"
    date: 2006-10-27
    body: "Just FYI, I'm finishing my KDE 4 snapshot packages for FC5 and FC6, so we can keep up with OpenSuSE and Kubuntu. ;-) I'll let you know when they'll be ready."
    author: "Kevin Kofler"
  - subject: "KDE 3.5.5 for FC6"
    date: 2006-10-27
    body: "KDE 3.5.5 for FC6 is already available in KDE-RedHat repo (http://kde-redhat.sourceforge.net). Right now it is in \"testing\" but soon (next week?) will be in \"stable\"."
    author: "Christian"
  - subject: "Re: KDE 3.5.5 for FC6"
    date: 2006-10-27
    body: "will it be available with the DT_GNU_HASH option too?"
    author: "Bruno Patini Furtado"
  - subject: "Re: KDE 3.5.5 for FC6"
    date: 2006-10-27
    body: "The FC6 packages at kde-redhat are probably already built with DT_GNU_HASH, as this is the default for all RPMs built on or for FC6."
    author: "Kevin Kofler"
  - subject: "Re: KDE 3.5.5 for FC6"
    date: 2006-10-28
    body: "It's now also in FC5/FC6 updates-testing."
    author: "Kevin Kofler"
  - subject: "Re: KDE 3.5.5 for FC6"
    date: 2006-10-31
    body: "And now in FC5/FC6 updates."
    author: "Kevin Kofler"
  - subject: "i maybe missed something"
    date: 2006-10-27
    body: "i maybe missed something but why is it here? this is nothing speciall about KDE, or is it? lots of distros have  been released before with the rencent KDE in it, like Mandriva or Kubuntu, but does they get here? no, so what's a big deal?"
    author: "blbecek"
  - subject: "Re: i maybe missed something"
    date: 2006-10-27
    body: "This is exactly the question I asked to my mind. There is no KDE related announcement.\nWhen they post here, are they doing the same on x.org, gnome, kernel.org, etc etc ???\n\n"
    author: "JC"
  - subject: "Re: i maybe missed something"
    date: 2006-10-27
    body: "Just clarifying, it's not \"they\" who posted this newsitem here, it's me. I don't speak for the Fedora Project, I'm not a Fedora Ambassador."
    author: "Kevin Kofler"
  - subject: "Re: i maybe missed something"
    date: 2006-10-27
    body: "I am intrested in more news of that kind. Thanks for posting it here.\n\nI would like to see a KDE information page where you can select your distribution and automatically get references to the latest official or inofficial distribution package pages."
    author: "Hans"
  - subject: "Re: i maybe missed something"
    date: 2006-10-27
    body: "Try DistroWatch.com"
    author: "Greg M"
  - subject: "Re: i maybe missed something"
    date: 2006-10-27
    body: "IMO, the Fedora project is one of the most important distributions out there. Fedora also seem to be on the right track (if not a head of others) in terms of system optimization. So, apart from the lack of an excellent support to KDE ( which is being provided by kde-redhat community ); I believe Fedora should get all the attention it needs.\n\n-- I'm not the ambassador of fedora either :-) --\n\nThank you for posting."
    author: "Kefah Issa"
  - subject: "Re: i maybe missed something"
    date: 2006-10-27
    body: "Apparently, there is some Fedora fan that whined in the previous story that Kubuntu has announcements on dot.kde.org but not Fedora. The site admins, being nice, have indulged him. Mind, I'm all for being nice and all, but doesn't Fedora default to GNOME? What's next, an announcement for every distro under the sun that happens to have some KDE stuff somewhere in its package repository?"
    author: "Blargh..."
  - subject: "Re: i maybe missed something"
    date: 2006-10-27
    body: "I think the post is relevant because of the popularity Fedora has. But indeed, Fedora's KDE has not the same treatment of GNOME, in fact, KDE seems to have no polishing at all at Fedora."
    author: "Bruno Patini Furtado"
  - subject: "Re: i maybe missed something"
    date: 2006-10-27
    body: "That's pretty much my point. Fedora is not *quite* the most popular distro out there, and if you count the market share of people who actually use KDE under Fedora, that number drops much, much lower still. And I thought this site was for KDE news, not a duplicate of distrowatch.\nIf this was about pressuring Fedora into stopping to treat KDE like a second-class citizen, though, it'd be a different thing. Are the Fedora-using people here willing to have a go at it?"
    author: "Blargh..."
  - subject: "Re: i maybe missed something"
    date: 2006-11-06
    body: "Fedora is a community run distro. It is not controlled by evil mind-flayers from RH.\n\nIf there is something you dont like about the KDE packages, its up to you fix them, or at least just quietly use your distro of choice without spreading FUD. "
    author: "OMG"
  - subject: "Re: i maybe missed something"
    date: 2006-10-27
    body: "The lack of polishing is exactly why I like KDE on Fedora. You get KDE, not FOO distro's version of KDE."
    author: "Greg M"
  - subject: "Re: i maybe missed something"
    date: 2006-10-28
    body: "This is why I'm not using fedora. I like and want to use Linux. Not fedora."
    author: "JC"
  - subject: "Re: i maybe missed something"
    date: 2006-10-27
    body: "Hmm, I think that came out as more aggressive than I intended it. Sorry, guys. I guess I'm a little miffed. I thought this site was intended for KDE news; I really can't see why generic distro announcements should be posted here, especially distros with a marked focus on other environments than KDE. It's cool that the Fedora guys are doing a good job with generic optimizations and such; but there are a lot of distros that are doing cool things and I'd rather not have this site turn into a sounding board for their respective fans. :/"
    author: "Blargh..."
  - subject: "Re: i maybe missed something"
    date: 2006-10-27
    body: "Ah, there, here's a much better wording of just what I meant:\nhttp://www.kdedevelopers.org/node/2486\nI knew there had to be someone out there with the ability to express my feelings in better terms than I do. :)"
    author: "Blargh..."
  - subject: "Re: i maybe missed something"
    date: 2006-10-27
    body: "You can install FC6 with only KDE (uncheck GNOME, check KDE, bingo!)."
    author: "Kevin Kofler"
  - subject: "Re: i maybe missed something"
    date: 2006-10-27
    body: "I know, and I never said you can't. Please kindly don't deform what I said. Thing is, you can remove GNOME and install KDE in about 95% of the distros out there. And many of them are doing a better job of packaging KDE than Fedora does, for that matter. Shouldn't THOSE get to announce their releases here, rather than one whose sub-par treatment of KDE has probably caused more harm to its image than not?\n\n(BTW: I know what you're thinking, and no, the distro I currently use isn't one of those. I use Gentoo, and its packaging of KDE is approximative at best. And I have no desire to see Gentoo announcements appear on this site.)"
    author: "Blargh..."
  - subject: "Re: i maybe missed something"
    date: 2006-10-27
    body: "If you really like KDE, you won't use Fedora (if you have the choice). :)\nSo many other distros do a much better job with it, it would have no point to use Fedora if the only reason is to use KDE."
    author: "Bruno Patini Furtado"
  - subject: "Re: i maybe missed something"
    date: 2006-10-27
    body: "I'm running KDE on Fedora since FC1, and did before on RHL. And I know I'm not the only one. Heck, the University of Vienna's Mathematics department has all their computers on Red Hat family distros (it used to be RHL, then Fedora, now it's the RHEL-derived Scientific Linux) with KDE as default. So I fail to see how this is not a viable option."
    author: "Kevin Kofler"
  - subject: "Re: i maybe missed something"
    date: 2006-10-27
    body: "I guess you mean: https://www.scientificlinux.org/ \n\n\nThat sounds indeed *really* cool (I am a chemist, so that is what *I* want on many computer ;-) To quote the german Wikipedia:\n\n\"Scientific Linux ist eine Linux-Distribution, die auf der Linux-Distribution Red Hat Enterprise Linux der Firma Red Hat aufbaut, und zu dieser kompatibel ist. Die Distribution wird vor allen Dingen von Entwicklern am Fermilab und am CERN weiterentwickelt.\""
    author: "Carsten Niehaus"
  - subject: "Re: i maybe missed something"
    date: 2006-11-01
    body: "I dropped RedHat/Fedora since Core 1 for its \"gnomocentrizm\". And for the same reason news about distros like these should not appear on the KDE news site."
    author: "NoNoFedora"
  - subject: "Re: i maybe missed something"
    date: 2006-11-06
    body: "Where do you classify distros that dont default to anything?\n\nReally, do try to grow up."
    author: "OMG"
  - subject: "Re: i maybe missed something"
    date: 2006-10-27
    body: "Erm - Kubuntu was covered by the message just before this one.\nAnd it is related to KDE: KDE is included in the release, and although it is a second class citizen in Fedora Core you can perfectly work with it. I do :)\n\nAnd why not post such stuff here? It gives a bit back to the distributions who care about including KDE and shipping it."
    author: "liquidat"
  - subject: "Re: i maybe missed something"
    date: 2006-10-28
    body: "Well, consider http://kde-redhat.sourceforge.net as the K in RedHat just like kubuntu is the K in Ubuntu.  \nKevin Kofler and his team are doing an amazing job for kde users that needs/have to run Fedora , without the team there would be no kde in Fedora or at least not as good. So thanx Kevin and all the kde-redhat team for bringing KDE to Fedora/RedHat which by the way is still one of the most used distro outthere so it's cool to have a KDE presence on it."
    author: "Patcito"
  - subject: "Re: i maybe missed something"
    date: 2006-10-28
    body: "Hey, give credit where credit is due. :-) kde-redhat is run by Rex Dieter, not me! I have not produced a single published kde-redhat package yet. (I have KDE 4 snapshot packages almost ready though. Hopefully I'll find the time to do the final polishing soon so Rex can push them to the repository.) I actually run the Fedora-provided KDE 3 packages, so I'm not involved in kde-redhat's KDE 3 packaging."
    author: "Kevin Kofler"
  - subject: "Re: i maybe missed something"
    date: 2006-10-28
    body: "that is not true. kubuntu is an oficial ubuntu project. The site you refer to has nothing to do with redhat or fedora. redhat's official position on kde is that right now they are tolerating it because of its demand but we are working to get rid of kde if we can. we are doing our best, in concert with ubuntu and novell, to market the hell outta gnome so it will be possible in the future."
    author: "vm"
  - subject: "Re: i maybe missed something"
    date: 2006-10-29
    body: "Troll"
    author: "Anonymous"
  - subject: "Re: i maybe missed something"
    date: 2006-10-30
    body: "Whether the commenter is misrepresenting Red Hat's strategic position or not, the technical aspects of Red Hat's KDE packages speak for themselves: bizarre application defaults (yes, let's use some broken GNOME document viewer instead of kpdf), missing applications in the default install (Kontact replaced with Evolution). It surprises me that you don't get Nautilus as the default file manager, although it actually is the default if you open directories in Firefox. And this is their Enterprise Linux, not some Fedora version that you can claim is \"unpolished\" or whatever other excuse comes to mind.\n\nRed Hat have been doctoring their KDE packages for a very long time, and the resulting deliverables can't usually be regarded as improvements. I guess that's why the independent KDE package scene is so vibrant, although the dependency chasm between the two sets of packages was so wide when I last ran Red Hat it was quite a scary move to make the switch."
    author: "The Badger"
  - subject: "Re: i maybe missed something"
    date: 2006-11-05
    body: "Go to http://kde-redhat.sourceforge.net and read the disclaimer."
    author: "vm"
  - subject: "Re: i maybe missed something"
    date: 2006-11-05
    body: "Do you think that a major linux distro such as redhat, which is making more profits than any other linux company, wouldn't hire their own people to package KDE for their distro ? Does it have to be packaged by independent people eager to see KDE available for fedora/redhat ?\n\nI live in Raleigh, NC and have been to RH HQ several times - don't call me a troll. I have met many RH developers at Triangle LUG also."
    author: "vm"
  - subject: "Optimisation stuff"
    date: 2006-10-28
    body: "Can someone explain what this DT_GNU_HASH thing is about?"
    author: "forrest"
  - subject: "Re: Optimisation stuff"
    date: 2006-10-28
    body: "http://sourceware.org/ml/binutils/2006-06/msg00418.html"
    author: "AC"
  - subject: "Re: Optimisation stuff"
    date: 2006-10-28
    body: "Dynamic linking (runtime linking of shared libraries) is twice as fast, which means dynamically-linked programs (i.e. pretty much all programs in Fedora) start up faster."
    author: "Kevin Kofler"
  - subject: "Re: Optimisation stuff"
    date: 2006-10-28
    body: "and crash twice faster ;o)"
    author: "JC"
  - subject: "Win + Key doesn't work in Fedora Core 6/X.org 7.1"
    date: 2006-10-29
    body: "Does anyone experience the same issue? http://forums.fedoraforum.org/showthread.php?t=133920\n\nThe problem is that my KDE is unable to intercept 'Win + Key' keys combination.\n\nThis is how it happens: in Keyboards Shortcuts kcm module I choose to modify any global shortcut and e.g. press Win + Esc. I see \"Win + \" on the screen but when I press 'Esc', the only 'Esc' key got entered into the \"Primary Shortcut\". And yes I've already tried enabling \"Multikey\" mode with no success. It looks like my X.org 7.1 server doesn't treat Win key as a modifier - so it doesn't allow \"Win\" key to be pressed together with any other key.\n\nPlease, give me an idea how to solve this problem."
    author: "Artem S. Tashkinov"
  - subject: "Re: Win + Key doesn't work in Fedora Core 6/X.org 7.1"
    date: 2006-10-30
    body: "I had the exact same problem with my FC5 and  I never found out what the problem was. If I used kontrolpanel and changed a few keys and pressed apply then things would work. Or if I restarted X things most often also worked.\n\nIt aint exact sceince but it just might help untill someone comes along with a bulletproof soolution :-)\n\nIn the mean time I will try and find out why my core 2 CPU messes things up in FC6 :-("
    author: "Morten Slott Hansen"
  - subject: "Re: Win + Key doesn't work in Fedora Core 6/X.org "
    date: 2006-10-30
    body: "I had this issue too with FC5. Win + <somekey> didn't work for a while, but now it seems all right. It looks like it started to work after I did \"yum upgrade\" some time ago."
    author: "Dovydas Sankauskas"
  - subject: "Various bugs"
    date: 2006-11-14
    body: "FC6 broke ALSA support for my SB Audigy LS. Visor (Sony Clie) connectivity still doesn't work. Upgrade broke KDE startup - ``yum update'' fixed the issue.  Upgrade hangs randomly on my HP/Compaq laptop. Yuck."
    author: "szlam"
---
Fedora Core 6 (<a href="http://en.wikipedia.org/wiki/Zod">Zod</a>) <a href="http://www.redhat.com/archives/fedora-announce-list/2006-October/msg00008.html">has been released</a> this week. Among other current software (Kernel 2.6.18.1, glibc 2.5, GCC 4.1.1, X.Org X11 7.1), Fedora Core 6 includes <a href="http://www.kde.org/announcements/announce-3.5.4.php">KDE 3.5.4</a>. <a href="http://www.kde.org/announcements/announce-3.5.5.php">KDE 3.5.5</a> will be available as an official update soon.



<!--break-->
