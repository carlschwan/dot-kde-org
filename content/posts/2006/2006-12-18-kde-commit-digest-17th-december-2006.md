---
title: "KDE Commit-Digest for 17th December 2006"
date:    2006-12-18
authors:
  - "dallen"
slug:    kde-commit-digest-17th-december-2006
comments:
  - subject: "Koffice 2 interface element"
    date: 2006-12-17
    body: "Looks kool."
    author: "gerd"
  - subject: "Re: Koffice 2 interface element"
    date: 2006-12-17
    body: "Defintely. Hope it will make its way in kdelibs soon."
    author: "AC"
  - subject: "Re: Koffice 2 interface element"
    date: 2006-12-19
    body: "Hey Casper, this is great!\nNow we're this close to having true Adobe style virtual sliders for any type of parameter in KDE.\nhttp://dot.kde.org/1157452184/1157459925/1157461336/1157475411/\n\nGo! Go! Go! \n;))))\n"
    author: "Gabriel Gazz\u00e1n"
  - subject: "The clones strike back !"
    date: 2006-12-17
    body: "There are 2 bug killers named Sebastian Trueg. Cloning is definitively the solution for the bug squashing team. \nLight sabers are too old school..\n"
    author: "Shamaz"
  - subject: "Re: The clones strike back !"
    date: 2006-12-17
    body: "Heh, I investigated, and it seems Sebastian Trueg (or himself and a crazed fan ;)) is operating from two bugzilla accounts this week: trueg k3b org and sebastian trueg de\n\nSee https://bugs.kde.org/weekly-bug-summary.cgi\n\nDanny"
    author: "Danny Allen"
  - subject: "phonon"
    date: 2006-12-17
    body: "Not to critize, but isn't the whole point of phonon to support apps like kaffeine? Shouldn't that effort be spent on moving to phonon?"
    author: "AC"
  - subject: "Re: phonon"
    date: 2006-12-17
    body: "I totally agree with you, when i read this i immediatly said \"WTF!? What about phonon then!? It is going to be the same sh**t as arts and that's why kaffeine is not going to be ported to it?!\""
    author: "erne.castro"
  - subject: "Re: phonon"
    date: 2006-12-18
    body: "phonon is completely unlike arts in every way since they are completely different types of solutions.\n\nto application developers they look similar: the provide multimedia APIs. from there things differ dramatically.\n\nphonon provides UI elements and is much more \"KDE-esque\" in API design. however, it provides approximately zero actual multimedia playing/catpuring functionality internally; for that it relies on things like NMM or gstreamer or whatever Phonon backend you're running talks to.\n\naRts, on the other hand, was the sort of animal you'd write a Phonon back end for. it was also less KDE-like in its API and didn't provide much in the way of UI help."
    author: "Aaron J. Seigo"
  - subject: "Re: phonon"
    date: 2006-12-19
    body: "It think it was more about: why gstreamer backend in Kaffeine when gstreamer should be addressed from Kaffeine via the phonon layer.\n\nthat is:\nKaffeine   --> phonon -> gstreamer, nmm, ...\n\nthe story is read as\nKaffeine    --> (phonon -> gstreamer, nmm, ...),\n                gstreamer\n                      "
    author: "fred"
  - subject: "Re: phonon"
    date: 2006-12-22
    body: "I don't think Kaffeine is part of kdemultimedia, so they can do whatever they want really.  Kaffeine with a regular gstreamer backend might be good so that GNOME users use it without needing more than kdelibs and Qt."
    author: "Matt"
  - subject: "Re: phonon"
    date: 2006-12-22
    body: "> Kaffeine with a regular gstreamer backend might be good so that GNOME users use it without needing more than kdelibs and Qt.\n\nSince Phonon is part of kdelibs, there will be no difference dependency-wise\n\n\n\n"
    author: "Kevin Krammer"
  - subject: "Re: phonon"
    date: 2006-12-17
    body: "You forget that kaffeine has a release cycle different from that of KDE.\nYou want to wait over a year for kaffeine to support the newer gstreamer?\nPorting to phonon would also mean porting to KDE4/Qt4.\nAlso I don't think that phonon's API is completely finalized nor is it ready for apps to start their porting, Of course someone please correct me if I'm wrong."
    author: "Stephen"
  - subject: "Re: phonon"
    date: 2006-12-17
    body: "You are wrong :)\n\nSeriously; Phonon is one of the first components in KDELibs that was deemed stable various months ago. Using the framework is very much appreciated. If you find something missing Matthias told us he is very much looking for constructive critisism.\n\nSo, in effect, I think that starting work on a KDE4 based version should happen rather sooner than later. As 'sooner' will allow you to actually give feedback if something in the libs API is not perfect.\n\nAlso; I was under the impression that Kaffeine didn't release that often. The latest (according to freshmeat) is already some 7 months ago.\nIn that light, I'd say they devs should finish up the KDE3 version; put it out the door as soon as possible, and focus on the KDE4 one.\n\nBut thats just my opinion :)"
    author: "Thomas Zander"
  - subject: "2"
    date: 2006-12-17
    body: "mine, too! get Kaffeine the perfect player it always wanted to be..."
    author: "eMPe"
  - subject: "Re: phonon"
    date: 2006-12-17
    body: "Kaffeine is mostly a Xine frontend.\n\nI don't see any problem in porting kaffeine to phonon after kde4 is released and proven.\n"
    author: "otherAC"
  - subject: "Re: phonon"
    date: 2006-12-18
    body: "one catch: it's hard to prove a library without apps using them.\n\n4.0 will also be less fun to use if there are fewer apps, meaning fewer people  are likely to spend time with 4.0 apps and features .. meaning it's harder to prove.\n\ni sounded the horn about a year ago that apps should probably do one or two more kde3 releases and then look to do a kde4 release at that point... so i understand the ideas behind the conservative approach; but we're pretty much at the point that working on your kde4 app makes sense. =)"
    author: "Aaron Seigo"
  - subject: "Re: phonon"
    date: 2006-12-18
    body: "> but we're pretty much at the point that working on your kde4 app makes sense. =)\n\nAre we? For what length of application release cycles? I can tell you that I won't start porting my apps until there is a clear message, that KDE 4 will come out in 3 months or so. For others the threshold will be different, depending on how often they usually release new versions. But I seriously doubt, too many outside-of-KDE's-release-cycle-apps will start getting ported as long as there isn't even a tentative release schedule for KDE4.\n\nOf course we don't want to shut off KDE4 development too early, and probably it would be highly unlikely for a release schedule to actually be met on the spot. Still, if at least there were some goal posts such as \"API changes (not additions) in kdelibs should be finished around MM/DD/YYYY. This deadline may be pushed ahead by two weeks at a time, as long as a majority on kde-core-devel thinks more changes are needed. First beta release should be prepared around MM/DD/YYYY\"), etc. Then application developers would at least have something to base their guesses on."
    author: "tfry"
  - subject: "Re: phonon"
    date: 2006-12-18
    body: "3 months to port and adapt. i suppose either your apps are simple, you have a good amount of time to work on them or you're just a really fantastic developer. =)\n\nand yes, the threshold will vary from person to person. but for any non-trivial app i think now is a good time to get going on kde4. you'll want time to adapt to qt4 and the new stuff in kde4.\n\nit will also help us test API changes, porting tools, etc. IOW, it will help get KDE closer to the 4.0 mark.\n\nand yes, i agree that we need a clear release schedule at this point. the time has come for that too."
    author: "Aaron J. Seigo"
  - subject: "Re: phonon"
    date: 2006-12-17
    body: ">>Also; I was under the impression that Kaffeine didn't release that often. The latest (according to freshmeat) is already some 7 months ago.\n\nAccording to kde-apps.org[1] last update was november 26\nand according to the website of kaffeine[2], the release before november was september 08.\n\nI think you should use more reliable sources :o)\n\n[1]http://kde-apps.org/content/show.php?content=9802\n[2]http://kaffeine.sourceforge.net/"
    author: "otherAC"
  - subject: "Re: phonon"
    date: 2006-12-18
    body: "> You want to wait over a year for kaffeine to support the newer gstreamer?\n\npersonally, i think that one more kde3 release would make sense for an app like kaffeine and then a start on the kde4 port .. it won't be over a year before a 4.0 release is made."
    author: "Aaron Seigo"
  - subject: "Re: phonon"
    date: 2006-12-18
    body: "only if phonon can offer verything that kaffeine needs.\ndunno if that is the case?"
    author: "otherAC"
  - subject: "Re: phonon"
    date: 2006-12-18
    body: "given what kaffeine does and what was in the phonon demo at aKademy (e.g. a video playing with audio controlled by a seek and volume slider ;) i'd say \"yep\".\n\ni understand the backends need some more work, but i'm guessing that if apps started using it more and dropped their own backend efforts to coordinate on one set of phonon backends that would help things a lot."
    author: "Aaron Seigo"
  - subject: "Re: phonon"
    date: 2006-12-18
    body: "> given what kaffeine does and what was in the phonon demo at aKademy (e.g. a > video playing with audio controlled by a seek and volume slider ;) i'd \n> say \"yep\".\n\nNo, this is only a Kaffeine side-effect. Kaffeine is meant to be a front-end to the zillion xine options. Media playback is only one of these options.\n<g>\n\nI seriously hope that Codeine is ported to Phonon, personally I do not much care about Kaffeine.\n \n"
    author: "Davide Ferrari"
  - subject: "Re: phonon"
    date: 2006-12-18
    body: "if phonon can't support what kaffeine does (and i'm a daily user of kaffeine; i've even contributed patches to it), then we need to know *now* not after 4.0 comes out.\n\nkde4 media apps really shouldn't need to write to a given backend like we've done in the past. it's fragile, it's time consuming and isn't portable."
    author: "Aaron J. Seigo"
  - subject: "Re: phonon"
    date: 2006-12-18
    body: "I did that gst-0.10 port to see it in action.\nWas not so much efforts (a few hours) to see that it's still far from xine.\n\nMoving to Phonon is in our todo, off course. But at that moment we are concentrating on a thread-safe solution for xine's X vo plugins (xcb seems to be the way to go).\n\nBut after having given kde4 a try, i do not expect it to be released soon, so we should have some month for the kde4 port :)\nAnd btw, atm #phonon has nearly zero traffic :)\n\nOh, and yes Aaron, Kaffeine does a little more than playing a video file with sound control and seek bar. For example, get a dvb-t stick (or more), plug it in and start Kaffeine ;)"
    author: "Christophe Thommeret"
  - subject: "Re: phonon"
    date: 2006-12-18
    body: "i'm aware of the various things kaffeine does. it even plays cd's. whether that's a good thing or not is another question ;) but there it is: lots of things it can do and the developers like it that way. cool.\n\nif there are things that phonon isn't covering for kaffeine, we need to know sooner rather than later so it can be determined if there is a good way to provide support for that (or not)\n\ni really like how people are pretending to kick the tires, but are doing so without so much as really looking at them let alone hitting them with their shoes. ;)"
    author: "Aaron J. Seigo"
  - subject: "Re: phonon"
    date: 2006-12-18
    body: ">we need to know sooner rather than later\n\nI, of course, understand that, just let us some time to look at phonon (and kde/qt4). I've just compiled kde4 to give Juk/Phonon a try but unfortunately Juk failed to compiled, so rather than spending time on that i searched the web for a Phonon test app but had no luck. And zero traffic on #phonon didn't help that much. So will have to do it myself. np. Just need time.\n\nAnd btw, our work on xine' X video out plugins should benefit phonon-xine as well.\n\nP.S.\nSorry but with my poor english, i don't understand that tires/shoes \"m\u00e9taphore\". ?\n\n"
    author: "Christophe Thommeret"
  - subject: "Kicking tires..."
    date: 2006-12-19
    body: "Experienced drivers can gauge the pressure by kicking the tires and feeling how hard or soft they feel.\n\nThis metaphor has been in use since long to describe when one tries some device for the first time."
    author: "Gr8teful"
  - subject: "Re: phonon"
    date: 2006-12-18
    body: "First I don't think that you can compete with kaffeine in the simple-video-player market. The key features are imho morely stuff like watching dvds and dvb.\nOf course we don't want to lose basic capabilities and there will be a kde4 kaffeine with support for phonon ... but this doesn't mean that we can drop our current backends.\nIt doesn't seem like phonon supports dvd menus or transparent osd (nearly needed for dvb) in the near future (i may be wrong) - furthermore we want to provide continuous releases to the systems our users have at the moment. I think this is also part of the reason we don't live in kde main but in extragear ...\nCurrently some fresh air has come and development will intensify, so expect good stuff soon ;-)\nI hope this could clearify the situation a bit ..."
    author: "Christoph Pfister"
  - subject: "Re: phonon"
    date: 2006-12-19
    body: "so one of the kaffeine backends would be a front end to backends? *heads spins* that seems a little ...... inefficient, from both a coding and a performance stand point.\n\nwhat would be great is if the work everyone puts into perfecting their backends goes into perfecting the backends for phonon. now, the question is if that is realistically achievable.\n\nyou mention transparent OSDs. i don't see what that has to do with phonon at all? this sounds more like a job for a nice OSD class in kdelibs, unless i'm misunderstanding what you're driving at.\n\ndvd menus sounds like something that the video widgets should support out of the box where available (e.g. not every phonon backend may be able to support that).\n\nas for continuous release, yes, of course that makes sense. the question is when one can/should switch over to kde4 development so that users aren't stuck halfway between kde4 and kde3 due to your app. part of this is that the kde project needs to put out some better timelines for app devels such as yourself to sync with, but getting app developers to work on kde4 apps as we edge closer to a 4.0 will actually help us get 4.0 out sooner with greater confidence.\n\nimho it would absolutely suck if 4.0 released with a phonon that couldn't do what kaffeine needs simply because the kaffeine developers didn't engage with phonon until it was too late. remember, kde is your platform, too, simply by nature of your app using it. just because your app is a 3rd party app doesn't mean you have no ability (or even to some degree responsibility) to improve or influence the parts of the project that you rely on."
    author: "Aaron J. Seigo"
  - subject: "Re: phonon"
    date: 2006-12-19
    body: "After reading your KDE4 porting arguments I realize some of these apply to my application as well. I'm working on KMess (www.kmess.org), a MSN Messenger client for KDE.\n\nOver a half year or so we'd like to implement multimedia stuff. That would involve Solid to access webcams, Phonon to handle audio streams in both directions, and perhaps Decibel as well. I'd love to leave KDE3 with another good release, but implementing these features without Solid/Phonon seams like a waste of time too. A release schedulle or SVN status page would help a lot.\n\nThis still leaves me with the question whether KMess should be ported to KDE4, or try to implement webcam/audio for a final KDE3 release.. :-/"
    author: "Diederik"
  - subject: "Re: phonon"
    date: 2006-12-19
    body: "I think it's time for a clear roadmap for kDE4, so that developers can decide whether it's a good idea to release new stuff in kde 3.x series or port the application to kde4.\n"
    author: "otherAC"
  - subject: "significant figits"
    date: 2006-12-17
    body: "John Tapsell committed a change to /trunk/KDE/kdebase/workspace/ksysguard/gui/SensorDisplayLib/ProcessModel.cc:\n\nShow the CPU usage as just \"32%\" \"5%\" and \"0.4%\", so to just 1 or 2 s.f.\nAs suggested by a \"ben\" on dot.kde.org\n\nWoohoo! Thanks a lot for listening, John!\n\nWhat does s.f. mean, \"significant [f,d]igits\"?"
    author: "ben"
  - subject: "Re: significant figits"
    date: 2006-12-17
    body: "Significant Figures (might be more of a British thing ;))\n\nDanny"
    author: "Danny Allen"
  - subject: "Re: significant figits"
    date: 2006-12-17
    body: "\"Significant figures\" I presume.  "
    author: "Robert Knight"
  - subject: "Re: significant figits"
    date: 2006-12-18
    body: "Thanks :-)\nHere's a screenshot:\n\nhttp://img142.imageshack.us/img142/698/sensorload19ou3.png"
    author: "John Tapsell"
  - subject: "Re: significant figits"
    date: 2006-12-18
    body: "Nice!  Those little cpu/mem usage bars look really nice!  Makes it much easier to see what is hogging all your cpu or memory at a glance."
    author: "Leo S"
  - subject: "Re: significant figits"
    date: 2006-12-18
    body: "Good stuff, John. Way to go :)"
    author: "Mark Kretschmann"
  - subject: "Soild?"
    date: 2006-12-18
    body: "I noticed that guidance and knetworkmanager didn't use Solid for power and network management. What's up with that?"
    author: "Hagai"
  - subject: "Re: Soild?"
    date: 2006-12-18
    body: "I quess guidance and knetworkmanager is for kde 3.5.x and you can't use Solid before kde 4."
    author: "kanttu"
  - subject: "Re: Soild?"
    date: 2006-12-18
    body: "guidance simply hasn't been ported to KDE 4 yet.\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "About small SVG images"
    date: 2006-12-18
    body: "SVG as we know is supposed to be the solution for the problem of keeping multiple icons of varying sizes for the same widget. But even SVG has issues looking nice when they are reduced to 22x22 or even 32x32.\n\nI was thinking about CorelDraw this week-end and recalled it had the capability of mixing SVG and BMPs in the same drawing. What if SVGs degraded to BMP at small sizes?"
    author: "Peter"
  - subject: "Re: About small SVG images"
    date: 2006-12-19
    body: "In terms of icons, it is a given that more simple and recognisable SVGs/BMPs be made for smaller sizes. At least thats what is done with Tango.\n\nI have not heard about storing the smallscale BMPs as part of the SVGs however. Will need to check the SVG spec about that."
    author: "Schalken"
  - subject: "Re: About small SVG images"
    date: 2006-12-19
    body: "Not sure how long ago the entry was (not thaaat long ago my fuzzy ol memory recalls) but Zack Rusin blogged (http://zrusin.blogspot.com/) about this and proposed a pretty clever solution that mimicked some of the techniques used in keeping fonts readable at different sizes (not a GFX guy, won't try and go into detail about what he said for fear of mucking it up)"
    author: "borker"
---
In <a href="http://commit-digest.org/issues/2006-12-17/">this week's KDE Commit-Digest</a>: A new interface element, the 'viewbar', makes its debut; continued work on <a href="http://wiki.koffice.org/index.php?title=Flake">Flake</a> and <a href="http://kross.dipe.org/">Kross</a>-based scripting within <a href="http://koffice.org/">KOffice</a>. Continued refinements in KSysGuard. Much work to improve support for <a href="http://en.wikipedia.org/wiki/VPN">VPN</a> connections in <a href="http://en.opensuse.org/Projects/KNetworkManager">KNetworkManager</a>, with KNetworkManager being moved from playground/ to extragear/. <a href="http://kaffeine.sourceforge.net/">Kaffeine</a> begins porting to <a href="http://gstreamer.freedesktop.org/">GStreamer</a> 0.10. <a href="http://edu.kde.org/kgeography/">KGeography</a> extends its global coverage with a handful of new country maps. KWin4 and Kolf begin their transition towards improved and scalable (SVG) graphics. Commits start to flow in the <a href="http://dot.kde.org/1165100724/">Student Mentoring program</a>. Support for <a href="http://daniel.molkentin.de/blog/index.php?/archives/59-Password-shadowing-Pimp-My-Plastique.html">bullet-aliased passwords</a> across KDE.
<!--break-->
