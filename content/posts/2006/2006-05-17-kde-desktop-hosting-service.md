---
title: "KDE Desktop Hosting Service"
date:    2006-05-17
authors:
  - "ccooper"
slug:    kde-desktop-hosting-service
comments:
  - subject: "I beat them to it :)"
    date: 2006-05-16
    body: "Funny.  I just set up my own one at home with exactly that combination (Kubuntu and NX). "
    author: "cm"
  - subject: "Re: I beat them to it :)"
    date: 2006-05-16
    body: "Nice!\nBut why is that funny?"
    author: "AC"
  - subject: "Re: I beat them to it :)"
    date: 2006-05-17
    body: "I meant something along the lines of \"interesting coincidence\".  Or maybe not. Both are great projects, after all. "
    author: "cm"
  - subject: "Re: I beat them to it :)"
    date: 2006-05-17
    body: "funny = strange!"
    author: "fast_rizwaan"
  - subject: "Cosmopod"
    date: 2006-05-17
    body: "Doesn't Cosmopod provide the same, but free.\n\nThe same 1GB space and free e-mail, plus more.\nIt's ads sponsored project. And this come you have to pay.\n\nWhere is the difference?\n\nE@zyVG\nlinux.wordpress.com"
    author: "E@zyVG"
  - subject: "Re: Cosmopod"
    date: 2006-05-17
    body: "it's a different company :o)"
    author: "AC"
  - subject: "Roll your own"
    date: 2006-05-17
    body: "I prefer to own a computer. Today computers are less expensive, more powerfull, more connected and easier to use. When IPv6 gets to an usable point and broadband hits all homes, there won't be any reason for this services, or perhaps for any services. I would take this more like a toy to play with and as a technology showcase. Anyway, I hope they do well."
    author: "Sebasti\u00e1n Ben\u00edtez"
  - subject: "Re: Roll your own"
    date: 2006-05-17
    body: "Still, companies like google, linspire and microsoft are introducing more and more online applications...."
    author: "AC"
  - subject: "Re: Roll your own"
    date: 2006-05-17
    body: "True. Some of them are quite nice, like google's calendar. But I still prefer to own my data and I see we are reaching an era were everyone would own some sort of computer. If that computer will be just a browser and a wifi connection is to be seen. But I don't see desktop applications going anywere or web applications replacing what now works fine just because they are cool or are built with the latest web invention (cough, ajax, cough)."
    author: "Sebasti\u00e1n Ben\u00edtez"
  - subject: "Re: Roll your own"
    date: 2006-08-25
    body: "It is about the time you spent on tweaks, upgrades, security issues and all that. \n\nI know, firsthand, a lot of people whom are willing to pay good money if paying that, means they are assured to have an Always Working System. \nYes, /you/ know how to keep kubuntu in the air, but good chances your accountant, your boss, or even your mom and dad lack these skills. If all they need to worry about, is to pay some company a few bucks a week/month/year, and get all the applications and features, without all the hassle, sure they will go for that.\n\nSo yes, there are very good reasons for this services. They need finetuning (what if I want to mail the pictures on my digital camera... etc) and they need to become more widespread. But they will fill a large gap.\n\nB\u00e8r"
    author: "Services"
  - subject: "Online spying..."
    date: 2006-05-17
    body: "These kind of online storage will help Authoritarian Governments and Businesses to spy on the citizens more easily to control and sell, like they do now with e-mails. they can now read everything you've created, saved on these online disks!"
    author: "fast_rizwaan"
  - subject: "Re: Online spying..."
    date: 2006-05-17
    body: "Well, most classified documents of Dutch government institutes (like the police, justice department and federal intelligence) are already available for download via limewire etc, so why not share our information with them as well :o)"
    author: "AC"
  - subject: "Re: Online spying..."
    date: 2006-05-17
    body: "The question is, do you really care if people know about it?  If no, then no reason to be paranoid.  If yes, why are you storing it on someone else's server?  "
    author: "Corbin"
  - subject: "it's fast, it's really fast"
    date: 2006-05-18
    body: "what machine is this service running on? Any techn. specs?\n\nOpenoffice starts in no time at all. I've a big machine here and still it takes 2 secs to start OO2.\n\nThey give you full CLI access, that's great, too\n\nCosmopod is more something to play around with (it's free, it shows ads, it's slower), but InQubs offer seems to be more for the professional user (clean default KDE desktop, CLI access, seems to run on a very fast machine, all apps start instantly, and you have to pay for it)\n\nHad to generate a new dsa key before connecting to InQub worked (modify the corresponding .nxs file in ~/.nx/config at <option key=\"Public Key\" value=\"DSA  KEY goes here\">)\n\nbtw.: After trying to use the \"Gnome session\" option I gave up. The Gnome desktop just crashed on me several times and became unresponsive."
    author: "Thomas"
  - subject: "Re: it's fast, it's really fast"
    date: 2006-05-28
    body: "Where did you get that from?\n\nCosmoPOD are the innovators in the space and are leagues ahead of InQubs offering.  With InQub, you have to download the NXClient software. CosmoPOD now offers immediate access to your desktop, via a browser plugin. There are also rumours that CosmoPOD is preparing for something really big. I am sticking with CosmoPOD. InQub is merely a copy cat. They will never catch CosmoPOD.\n\n"
    author: "John"
  - subject: "Updated Pricing"
    date: 2006-05-27
    body: "The pricing for this service was recently updated, now subscribers apparently can choose between a BASIC plan for $29.95/year and a PRO plan for $59.95/year.\n\n1 GB online storage & an online desktop for about $2.50/month doesn't sound bad for me!\n"
    author: "Cameron Houber"
---
<a href="http://www.inqub.net/netcubicle.html">InQub Ltd</a> offers personal remote KDE desktops on <a href="http://www.kubuntu.org/">Kubuntu</a> using <a href="http://www.nomachine.com/">NoMachine's</a> NX technology for bandwidth savings and connection encryption for a small monthly charge. Each account is comes with 1 GB of home directory storage and is customisable by the respective user. This service represents an interesting approach of working with KDE without installation and maintenance issues, especially for GNU/Linux newbies and users travelling a lot.  Users can get a week's <A href="https://www.inqub.net/netcubicle/freetrial/">free trial</a> of the service.




<!--break-->
