---
title: "Audio Interviews from aKademy 2006"
date:    2006-10-24
authors:
  - "dallen"
slug:    audio-interviews-akademy-2006
comments:
  - subject: "Don't want to seem a nag..."
    date: 2006-10-24
    body: "and I know there's a lot of work involved, but will we be seeing the session videos any time soon?  I some sessions I really want to see, like David Faure's KDE4 developer how-to.\n\nJohn."
    author: "Odysseus"
  - subject: "Re: Don't want to seem a nag..."
    date: 2006-10-24
    body: "I'm afraid that I'm also going to have to nag ;) Please tell me that have they haven't been lost - I was really looking forward to pretty much all of them :("
    author: "Anon"
  - subject: "Nice!"
    date: 2006-10-24
    body: "Nice interviews, nice insights into current KDE topics, nice entertainment on my way to work today, thanks! KDE://radio, RadioTux, Linux Action Show... quite a lot podcast stuff about KDE at the moment, great.\n\nAnd I'd like to know what the creepy background sounds in the interview with Pradeepto were. :)"
    author: "Ascay"
  - subject: "Re: Nice!"
    date: 2006-10-24
    body: "Would you believe Halloween music?  Were you terrified?\n\nIn all the interviews, finding a consistently quiet place to do interviews was not trivial.  Room were taken or locked, hallways busy, computers labs were meant to be silent, etc.\n\nAll of the doors in the buildings were notoriously squeaky - so what you're hearing are doors being opened and closed.  Joseph Gaffney who did a great job minimizing the external noises during mastering, made the PETA-unfriendly comment in asking \"Was a whale getting beaten with a dying seal?\"  Send a can of oil to Trinity college; future podcasters will thank you for it.\n\nI tried to kidnap developers and drag them to dark, silent, remote locations - but most were too busy drinking, coding, fraternizing, or just used glowing Trolltech pens to find their way back.\n"
    author: "Wade Olson"
---
The landmark event of the KDE calendar, the KDE World Conference, continues to surprise even its most excitable fans with the emergence of six audio interviews recorded at <a href="http://conference2006.kde.org/">aKademy 2006</a>, in Dublin, Ireland. The interviews all feature prominent current contributors, and cover a diverse and interesting mix of topics relevant to the present and future of the KDE project.  To get future audio features automatically, subscribe to one of the <a href="http://radio.kde.org/">KDE://radio</a> RSS feeds: <a href="http://radio.kde.org/pub/konqcast/konqcast.rss">Ogg</a>, <a href="http://radio.kde.org/pub/konqcast/konqcast_mp3.rss">MP3</a>.  Read on for a summary of the interviews.














<!--break-->
<ul>
<li><b><a href="http://radio.kde.org/pub/konqcast/014_matthias_kretz.ogg">Matthias Kretz: Open Source Desktop Multimedia</a></b><br />
Phonon is a hot topic when it comes to the Free sofwtare desktop and KDE4. Wade Olsen takes a few minutes to talk with Matthias Kretz on this topic while in Dublin.</li>

<li><b><a href="http://radio.kde.org/pub/konqcast/013_jens_herden.ogg">Jens Herden: KhmerOS and KDE In Asia</a></b><br />
Living in Cambodia and working on both Quanta and KhmerOS, Jens Herden shares his insights on KDE, open source and Asia with Wade Olsen at this year's aKademy.</li>

<li><b><a href="http://radio.kde.org/pub/konqcast/012_jos_van_den_oever.ogg">Jos van den Oever: The Search For Strigi</a></b><br />
Indexing engine author Jos van den Oever explores the possibilities of Strigi and KDE4 with Wade Olsen as a means to find and explore information.</li>

<li><b><a href="http://radio.kde.org/pub/konqcast/011_oxygen_artists.ogg">Ken Wimer and David Vignoni: KDE 4's New Look</a></b><br />
Two members of KDE's talented art team, Ken Wimer and David Vignoni, chat about art and beauty with Wade Olsen in Dublin while at aKademy 2006.</li>

<li><b><a href="http://radio.kde.org/pub/konqcast/010_cornelius_schumacher.ogg">Cornelius Schumacher: Organising Akademy</a></b><br />
KDE developer and KDE e.V. board member Cornelius Schumacher speaks with Wade Olsen in Dublin about the efforts put into organising aKademy 2006 to make it a successful and worthwhile event.</li>

<li><b><a href="http://radio.kde.org/pub/konqcast/009_pradeepto_bhattacharya.ogg">Pradeepto Bhattacharya: KDE.in(dia)</a></b><br />
Pradeepto Bhattacharya and Wade Olsen discuss Pradeepto's work with the KDE India regional group, KDE Asia and aKademy 2006.</li></ul>

<p>In related news Daniel Molkentin did <a href="http://www.linuxpodcast.de/index.php?id=69">an interview on RadioTux</a> in German, talking about Phonon, Plasma and KDE-PIM.</p>











