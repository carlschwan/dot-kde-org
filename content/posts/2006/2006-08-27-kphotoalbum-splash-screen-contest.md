---
title: "KPhotoAlbum Splash Screen Contest"
date:    2006-08-27
authors:
  - "jriddell"
slug:    kphotoalbum-splash-screen-contest
comments:
  - subject: "Naming"
    date: 2006-08-27
    body: "I think you changed the name for worse. KPhotoAlbum is English language. Kimdaba was language neutral. It is further so ugly to have two consonants: 'KP'"
    author: "furangu"
  - subject: "Re: Naming"
    date: 2006-08-27
    body: "Sorry. Actually three\n\n'KPH'\n\nIt is a misconception to believe that a name has to explain the program. We buy our funiture at Ikea and like the strange names. Our cars are not called MFamilyCar or DTransporter.\n\nI expect the menu entry to look like \"KPhotoAlbum (A Photo Album for KDE)\"... happended before.\n\nI don't want Amarok to become \"KMusicBox\" or K3B to be renamed as \"KMediaBurn\"\n\nWhat's the difference of 'iTunes' and 'KFoo'? \n'i' is no consonant. "
    author: "furangu"
  - subject: "Re: Naming"
    date: 2006-08-27
    body: "true true..."
    author: "Giovanni"
  - subject: "Re: Naming"
    date: 2006-08-27
    body: "Full ACK!"
    author: "Martin"
  - subject: "Re: Naming"
    date: 2006-08-28
    body: "I think all IKEA names has a meaning in Swedish :-)\n\nLike this announcement.\nhttp://www.ikea.com/ms/en_GB/ikea_near_you/repairaction_lycksele.html\n\nLycksele is not only an IKEA furniture it is a city too.\n\nhttp://www.lycksele.se/templates/Page.aspx?id=6942\n"
    author: "RogerL"
  - subject: "Re: Naming"
    date: 2006-08-27
    body: "Ohhh yes, come on, complain about the name change a lot will you. I spent the first 4 year of KimDaBa development hearing people complain about it, then I asked for suggestions for a new name, and went with what most people liked.\n\nTrust me, input like yours, is what really make my day, and make me want to both spent money and endless hours working on open source development.\n\nOnce again, thanks for your continued rant about naming.\n\nPS: No I dont disagree with you, I did actually like the name KimDaBa a lot."
    author: "Jesper Pedersen"
  - subject: "Re: Naming"
    date: 2006-08-27
    body: "Just don't bother Jesper, some people don't have any better things to do than ranting on websites. Remember the 99.99% of the rest that think your work is great."
    author: "uga"
  - subject: "Re: Naming"
    date: 2006-08-28
    body: "I'm sorry but this sounds like you were not happy with changing the name in the beginning. Thats too bad. There are and always will be people who have a different oppinion from yours, or mine or anybody.\nStick to whats you think is best is all you can do. In the end its your work!\n\nOf course it would be rediculous to switch the name back again. This is not what I want to say. I just feel a bit sorry for you because you liked KimDaBa (which I did too) and had to change it because of the pressure of the people.\n\nKeep up your good work.\nI will keep using KPhotoAlbum whatever name it will have. \nEven: BillGatesFan or OpenSourceMustDie ;)\n\n\nadrian\n"
    author: "adrian"
  - subject: "Re: Naming"
    date: 2006-08-27
    body: "As long as many users don't know KDE/Linux it might be a good idea to have descriptive names for applications. I remember how weird applications' names looked to me when I was new to Linux many years ago.\nNames like \"Microsoft Word\" may be boring and not cool in any way, but they are easy for newcomers. They also claim \"THIS is the application to use for this particular problem domain!\".\nI think it's good to give important applications more creative/cool names because people will hear them often enough to remember their names.\nAs a programmer, you have to decide honestly whether your application is generally important or not. Oh, and there is nothing wrong with making less important applications - some people will need them!"
    author: "maelcum"
  - subject: "Re: Naming"
    date: 2006-08-27
    body: "Some of the most useful apps in kde got the most weird and meaningless names. Just think about the applications you most use. Still, people just learn them easy.  The problem happens generally when the word is sooo weird that one cannot tell how to pronounce or spell it. \n\nKeep it simple, keep it nice. Btw, and no, you can't name any application \"microsoft word\", since you could nicely get sued, no matter how easy it'd be for \"newcomers\" to learn the word."
    author: "uga"
  - subject: "Re: Naming"
    date: 2006-08-28
    body: "I always pronounced in Kim Day Bay. I figured it meant K Image Database"
    author: "Aaron Krill"
  - subject: "Re: Naming"
    date: 2006-08-28
    body: "yup, I thought that's what it meant too. I didn't mean to say kimdaba was hard to pronounce or anything. I just meant to say that the only wrong names are those that one cannot pronounce. Other than that, I see no problem, and kimdaba is no exception. Well, except, I'd pronounce it \"Keem Dah Bah\". Depending on your native language you end up pronouncing very differently..."
    author: "uga"
  - subject: "Re: Naming"
    date: 2006-08-29
    body: "Actually, KimDaBa is pronounced Kee-daa-baa (the M is silent :). Don't ask."
    author: "Marc Mutz"
  - subject: "Re: Naming"
    date: 2006-08-29
    body: "I've been using KDE for a few years now and I really appreciate its \"KAppName\" naming convention, especially if the name itself is also descriptive (KPDF, KMail, KSnapshot...). I rarely use the menu and I usually launch commands from alt+F2 or directly from Konsole, thus I have to memorise the names... or do I? After all, when I forget the program's name, I can always type k and press [tab] (there are a few apps that don't follow this convention, and sadly I quickly forget about their existence...).\n\nThat's how it looks in KDE... now let's take a peek at Gnome, as I have recently been forced to use it on a thin client for 3 months. Application names range from a well known Nautilius to (recently renamed, I believe) ekiga, totem or evince... now what kind of names are these? Kinda makes you wonder how these people name their children... Ok, let's stick to the topic. It's practically impossible to memorise these names (and I'm talking from a position of a \"newcomer\" here, because since Gnome 1.x a lot of names changed or I have simply forgotten them) and thus it's impossible to find many useful applications. Where's the clipboard for example? Maybe there isn't one, but if there is, it's pretty well concealed.\n\nTo cut the rant and sum up: KDE developers, please, PLEASE don't follow the Gnome's path of renaming applications to random and meaningless names!\n\nAs a side note: please don't follow the Gnome's \"usability\" path, which effectively aims at hiding all the useful stuff from the user. It's mad!\n\n[NOTE: This post by no means intended as a flamebait, please don't reply if you want to elaborate on KDE vs. Gnome topic]"
    author: "bojster"
  - subject: "DigiKam"
    date: 2006-08-27
    body: "Whatever happened to digikam? Why is everyone suddenly ogling over this KPhotoAlbum stuff?"
    author: "Trever Fischer"
  - subject: "Re: DigiKam"
    date: 2006-08-27
    body: "uhm? digikam is progressing just fine. These are two separate projects with completely different approaches. Nothing wrong with competing applications, right? I hope you don't mean that since you use digikam everyone else should, too ;)))"
    author: "uga"
  - subject: "Who's the Judge"
    date: 2006-09-01
    body: "\nIs the author going to be the only judge or will it be won by popular vote?  So far I think the submissions from Jaroslav Holan are awesome."
    author: "Henry"
  - subject: "Re: Who's the Judge"
    date: 2006-09-08
    body: "I think we have a winner. :)"
    author: "Lans"
---
Image management application <a href="http://kphotoalbum.org/">KPhotoAlbum</a> has launched a <a href="http://kphotoalbum.org/splashscreen.htm">splash screen contest</a>.  The contest comes with a prize of $100US straight from author  Jesper's PayPal account.  Some early designs are on the contest page already.  The contest runs until September 15th, and after that the KPhotoAlbum community will vote on which one will be used for the next release.

<!--break-->
