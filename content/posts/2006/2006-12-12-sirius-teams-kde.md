---
title: "Sirius Teams Up with KDE"
date:    2006-12-12
authors:
  - "sk\u00fcgler"
slug:    sirius-teams-kde
comments:
  - subject: "I can't resist it ..."
    date: 2006-12-12
    body: "Are you Sirius??"
    author: "BadJoker"
  - subject: "Re: I can't resist it ..."
    date: 2006-12-12
    body: "No."
    author: "I. M. Weasel"
  - subject: "Nice"
    date: 2006-12-12
    body: "Good to see the first supporting members. Hopefully more will follow.\n\nBut when can people buy personal supporting memberships?"
    author: "Carewolf"
  - subject: "Re: Nice"
    date: 2006-12-12
    body: "You mean an individual membership? You can do that already!\nPlease read\n\nhttp://ev.kde.org/getinvolved/supporting-members.php\n\nthoroughly. And here's the link to the application form:\n\nhttp://ev.kde.org/resources/supporting_member_application.pdf\n\nPS: A warm welcome to the Sirius Corporation in the KDE world and thanks a lot for your support! "
    author: "MaXeL"
  - subject: "Re: Nice"
    date: 2006-12-13
    body: "The application form only has individual patrons. Individual supporting members is still not possible."
    author: "Carewolf"
  - subject: "Re: Nice"
    date: 2006-12-13
    body: "No. You are right that the application form isn't exactly \"supportive\" here and needs to be changed.\nHowever the application form is just a template that _can_ be used and also can be changed according to your needs.\nHowever legally it's the \"Rules of Procedure For Supporting Members\" that counts:\n\nhttp://ev.kde.org/rules/supporting_members.pdf\n\nand that one says \"Every entity, natural person, company and authority can become a Supporting Member of KDE e.V. according to the articles of association. [...]. Entities and individuals can become a Supporting Member by applying eletronically to the Board of KDE e.V. using the form provided by the KDE e.V. [...]. Membership Fees are as follows: Individuals: EUR 100/USD 120 per year. [...]\"\n\nSo even if the form isn't exactly correct here I'd suggest the following: Use the application form as is and strike out \"Organization\" each time and replace it by \"Name\". For \"Membership Type\" strike out \"Corporate\" and replace it by \"Individual\" instead. Fill out the information needed and send the whole form to:\n\nhttp://ev.kde.org/contact.php\n\n"
    author: "MaXeL"
  - subject: "Re: Nice"
    date: 2006-12-13
    body: "We're currently in the course of setting up infrastructure and workflows for the Supporting Membership programme. While it is true that Individuals can become a Supporting Member, we decided to first concentrate on companies and getting the programme right in that respect. Administration of individual Supporting Members has more overhead compared to companies, and thus needs better workflows and infrastructure, which is what we just started working on. We have to take care that the fee connected to the Membership at the least covers the costs of handling the administrative work of the Supporting Membership programme, achieving this is obviously easier for higher membership fees."
    author: "Sebastian K\u00fcgler"
  - subject: "Mark"
    date: 2006-12-12
    body: "Mark Taylor? I remember the name from the European softpat debate. \"Opensource Consortium\", a fake grassroot lobby group from IBM? The ones with the \"Open Source compromise\" during the first reading?\n\nPerpetrators need a community project which justifies their money grabbing from the government. And sure they will find one. What will they contribute? Ah, I see:\na \"Plug-in based quality assessment platform\". Holy shit.\n\n...Europe's leading ... experts ..  leading ... and leaders in the Open Source industry.\n\nI want IT to be \"leader-free\". \n\nI am not against public money, but why do always the wrong guys get it, instead of those who do the work and make a difference?"
    author: "Snidok"
  - subject: "Re: Mark"
    date: 2006-12-12
    body: "You're talking about Graham Taylor from Open Forum Europe (OFE). Sirius is run by Mark Taylor, who is also President of the Open Source Consortium (OSC). \n\nSee Red Hat's view on OFE's support for software patents at https://www.redhat.com/advice/speaks_openeurope.html. OFE almost certainly works at the behest of proprietary vendors. The OSC works with the the guys at FFII to lobby against software patents in Europe."
    author: "dogStar"
  - subject: "Robots and digital watches"
    date: 2006-12-12
    body: "This is great news but I can't shake the feeling that they're merely a forerunner to the Sirius Cybernetics Corporation (http://en.wikipedia.org/wiki/Sirius_Cybernetics_Corporation) :D"
    author: "Matt Williams"
  - subject: "Sirius"
    date: 2006-12-12
    body: "So is XM going to align itself with Gnome?"
    author: "AC"
  - subject: "Re: Sirius"
    date: 2006-12-13
    body: "Different companies actually.  I was confused at first also, but then I clicked on the link to the company website.  Definitely different companies."
    author: "Anonymous Coward"
  - subject: "thank you sirius"
    date: 2006-12-12
    body: "Thanks you Sirius, and congratulations to both parties!"
    author: "Vlad"
  - subject: "KDE and Commercial Contributors"
    date: 2006-12-13
    body: "Making the KDE Commercial Contributors friendly surely encourages further Commercial contributors to join the KDE, create employment opportunities for KDE developers and KDE users to benefit with additional sophisticated desktop features.\n\nTo achieve this goal, first and foremost KDE must make it completely legal to ***extend the functionality of KDE*** with Commercial contributions (preferably by way of plugins, etc). \n\nYou know, normally Commercial contributors do not release source code of their contributions (ie. License is not GPL), therefore, it should not break the law if a commercial plugin requires to link with whatever related KDE.\n\nExample, lets say, a commercial contributor wants to extend the functionality of Konqueror, by way of a plugin, to filter files in a different way, it should be in first place the functionality of Konqueror can be extended by way of a plugin, second, it should be legal to link with KDE.\n\nHope to see the upcoming KDE 4.0 completely extendable by way of commercial plugins."
    author: "Sagara"
  - subject: "Re: KDE and Commercial Contributors"
    date: 2006-12-13
    body: "Why? \n\nThere is no other platform in existence where there exist any significant number of plugins extending the functionality, commercial or otherwise. A few specalized applications have a market for them, but thats application dependant not in the hands of the desktop anyway. Besides Konqueror already supports commercial browser plugins, like flash. So the biggest part of the Commercial Plugin Contributors market is already supported by Konquerors handling of browser plugins.\n\nCommercial enteties makes applications, not pluggins. And KDE has always had complete support for commercial applications using the KDE libraries(in a legal way, also for non GPL applicationsd). \n"
    author: "Morty"
  - subject: "Re: KDE and Commercial Contributors"
    date: 2006-12-13
    body: "Actually, it's perfectly legal to extend KDE functionality with commercial contributions.  What's /not/ so legal, and should /remain/ illegal, is depriving users of the four software freedoms when it comes to code extending KDE.  Indeed, Trolltech's Qt, on which KDE is built, is very commercial software that respects user freedoms.\n\nThe four freedoms are the freedom to run the program for any purpose (freedom 0), the freedom to study how the program works and adopt it to your needs (freedom 1, access to the source is required for this), the freedom to share this program you are so happy about with others (wanting to share what we are excited about is an entirely human and natural reaction, and should be encouraged, not prohibited, this is freedom 2), and the freedom to improve the program and release the improvements back to the community from which you yourself received the original code (this is freedom 3, and again, a license which does not prohibit the sharing of source code is required here).  If these freedoms are to be depended upon, they must be irrevocable as long as the person in question has done nothing to abridge those freedoms for others.  The GPLv2, with v3 looking to be even stronger in this regard, goes a long way toward ensuring these freedoms for users and distributors of software that chooses it as a license.\n\nIn my news signature I have a quote from Richard Stallman. \"Every nonfree program has a lord, a master -- and if you use the program, he is your master.\"  I prefer to be treated as a human being, my freedoms respected, not as a slave, whose freedoms as a human being are trampled on a whim of the master.  Thus, I won't have software, commercial or not, on any computer of /mine/, if it fails to respect my freedoms in this regard.  Freedomware yes, slaveryware no.\n\nIt would be a very sad thing indeed to see KDE reduce the strength of the freedoms it guarantees its users and supporters in regard to its own code.  One would rather hope those guarantees would be strengthened.\n\nDuncan"
    author: "Duncan"
  - subject: "Re: KDE and Commercial Contributors"
    date: 2006-12-13
    body: "Last time I checked, kdelibs was licensed under the LGPL, exactly for the reasons you're stating."
    author: "Sebastian K\u00fcgler"
  - subject: "Re: KDE and Commercial Contributors"
    date: 2006-12-15
    body: "And why do you think this is not the case at the moment? The KDE libs are LGPL-ed, so you can already create the plugins you're talking about (if you wanted to). Of course, this doesn't go for every program that runs on KDE or even that is destributed with KDE, and rightly so I think. "
    author: "Andr\u00e9"
  - subject: "Cool!"
    date: 2006-12-13
    body: "Thanks for supporting KDE!"
    author: "Joergen Ramskov"
  - subject: "Re: Cool!"
    date: 2006-12-14
    body: "This is good news, but aside from money, will there be any \n... well ... goodie for the end user such as\n\nbug fixing?"
    author: "shev"
  - subject: "Re: Cool!"
    date: 2006-12-16
    body: "Yes. The membership fees of the Supporting Membership programme will be used for example for covering travel expenses for developers, so they can attend meetings, or organising developer meetings themselves. Those meetings have an immediate positive effect on the development process.\n\n"
    author: "Sebastian K\u00fcgler"
---
In a move to promote the KDE desktop in the Enterprise, the UK's Open 
Source experts, <a href="http://www.siriusit.co.uk/">Sirius Corporation</a>, have become a <a href="http://ev.kde.org/supporting-members.php">Supporting Member</a> of the KDE project.  Sirius' commitment to KDE is our second supporting membership and follows Canonical's recent patronage of the project.  <a href="http://www.siriusit.co.uk/">Sirius</a> and KDE are joint participants in <a href="http://www.sqo-oss.eu/">SQO-OSS</a>, an EU-funded project 
that assesses the quality of Open Source code.











<!--break-->
<p>
Mark Taylor, CEO, Sirius Corporation said: <em>"KDE is a high-quality 
desktop that has always had a strong following in the Open Source 
community. By becoming a sponsor of the project, we now want to raise 
its profile in the Enterprise."</em>
</p>
<p>
Sebastian K&uuml;gler of the <a href="http://ev.kde.org">KDE e.V.</a> Board said: <em>"KDE is happy to welcome 
Sirius as one of Europe's leading Open Source experts in the circuit of 
KDE's Supporting Members. For KDE, this agreement manifests the next 
natural step in the improvement of relationships between KDE as the 
leading Free Desktop and leaders in the Open Source industry."</em>
</p>

<p>KDE e.V. wishes to thank its current supporters, and would like to invite all interested parties to <a href="http://ev.kde.org/getinvolved/supporting-members.php">help us continue to serve the KDE community</a>.






