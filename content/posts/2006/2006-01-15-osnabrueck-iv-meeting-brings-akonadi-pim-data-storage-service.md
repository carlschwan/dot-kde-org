---
title: "Osnabrueck IV Meeting Brings \"Akonadi\"  PIM Data Storage Service"
date:    2006-01-15
authors:
  - "tadam"
slug:    osnabrueck-iv-meeting-brings-akonadi-pim-data-storage-service
comments:
  - subject: "What about fixing the remaining datalos bugs first"
    date: 2006-01-15
    body: "Bug 104956: dimap: sudden mail loss\nhttp://bugs.kde.org/show_bug.cgi?id=104956\nBug 87163: kaddressbook empties resource on some conditions (data lost)\nhttp://bugs.kde.org/show_bug.cgi?id=87163"
    author: "Anonymous Coward"
  - subject: "Re: What about fixing the remaining datalos bugs first"
    date: 2006-01-15
    body: "Great idea, we accept patches. Keep them coming."
    author: "Till Adam"
  - subject: "Re: What about fixing the remaining datalos bugs first"
    date: 2006-01-15
    body: "very arrogant, how you sound here, till!\n\nok -- i know what to do now. i'll switch to thunderbird. the risk to loose mail can't be higher there. and after all, thunderbird runs just fine on my kde desktop."
    author: "ac"
  - subject: "Re: What about fixing the remaining datalos bugs first"
    date: 2006-01-15
    body: "hmm, you paid Till to work on kmail?"
    author: "ac"
  - subject: "Re: What about fixing the remaining datalos bugs first"
    date: 2006-01-19
    body: "I have never had a data loss with KMail, but whenever I tried Mozilla Mail or Mozilla Thunderbird, I ended with pretty big mess, and yes I lost some emails. Enjoy!\n\nMatej"
    author: "And you won't be much better"
  - subject: "Re: What about fixing the remaining datalos bugs f"
    date: 2006-01-15
    body: "Yes, I'm afraid this does sound arrogant. We all know how F/OSS works, but not everyone has the skills to send patches. And it's silly to do one more refactoring/feature while data loss bugs remain."
    author: "Unanimous Coward"
  - subject: "Re: What about fixing the remaining datalos bugs f"
    date: 2006-01-15
    body: "If you don't have the slightest idea about the codebase of KMail, then you better don't tell the developers what to do or how easy it might be to fix this and that.\nKMail needs a refactoring very urgently. In fact, the problem that it has so many bugs is based on internal structures in KMail which haven't been changed for years due to other high priority issues like IMAP support.\n\nAnyway, whoever wants to participate in the work to make KMail better is welcome. Every patch is welcome. But please understand that the development capacity is way too low, so it's all about setting priorities which may be different from what outsiders think."
    author: "Andreas Gungl"
  - subject: "Re: What about fixing the remaining datalos bugs f"
    date: 2006-01-16
    body: "Actually, this is one of my biggest gripes with the open source world.  First, it is compared to closed source and proponents say it's better.  Then, when someone points out some problems, someone pops up with \"fix it yourself, loser!\"\n\nYou are absolutely correct, users cannot expect to dictate how and what volunteers work on.  In the same way, however, you can't expect users to stay users when they are told to fix a bug themselves or shut up.\n\n"
    author: "Josephus"
  - subject: "Re: What about fixing the remaining datalos bugs f"
    date: 2006-01-16
    body: "You need to distinguish.\nThere is hardly that one entity named \"the open source world\", that means that there's also no ambiguity when there are seemingly contradicting statements.\nThese are different statement from different people, with respect to different problems.\n\nSo try to gain some insight before generalizing.\nI also don't take your comment as opinion of \"the user base\".\n\nYes, often it means you have to know a certain background only known to insiders, i.e. the ones dealing with the stuff. But your parent post sums it up well.\nSo there's another thing to learn: patience. Perhaps the hardest part. Lamenting won't bring one any further."
    author: "Phase II"
  - subject: "Re: What about fixing the remaining datalos bugs f"
    date: 2006-01-16
    body: "The big difference between closed and open source is that open source users have the possibility to fix bugs themselves, or hire others to do it for him.\nWith closed source you can only pay the original author and hope that he will help you.\n\nI agree Tills answer wasn't 100% diplomatic. It should have been. \"Sure, just let me know where I should send the bill\"."
    author: "CAPSLOCK2000"
  - subject: "Re: What about fixing the remaining datalos bugs f"
    date: 2006-01-16
    body: "If this was Microsoft Outlook or Apple Mail, what do you think you'd be doing?\n\nComplaining in several Mac/Windows forums until, someday, hopefully you'd be able to buy a version that doesn't have the bug you're complaining about.\n\nYou should appreciate that you have a direct channel of communication with the developers; but don't expect them to work on *your* priorities. This is the way it works with software, Free or not. What gets fixed is the priority of the people working on the software; the difference with Free software is that anyone has the ability to become a developer and work on their own priorities.\n"
    author: "Reply"
  - subject: "Re: What about fixing the remaining datalos bugs f"
    date: 2006-01-15
    body: "Akonadi is the way out of the current situation with KMail. It will increase the abstraction of protocol handling, caching and the actual handling and viewing of mails, which will ease the situation for contributors and lead to more reliable and readable code.\n\nAs for the mail losses, they are so rare and bizarre. Still I hadn't have a single have one in the last year even though I tried hard to reproduce several reports, and Till and others tried even harder. \n\nI'd claim that KMail is no less stable than other email clients, and yes we are frustrated with some of the phenomenons in the current code base, but that proves that it is a good idea to invest in Akonadi rather than hunting bizarre bugs that are hardly reproduceable. Of course that does *not* mean that we don't care or that we will completely give up. We can of course hunt those bugs a lot better if someone donated time (by analyzing the problem) or money.\n\nBut I won't buy blackmail-style arguments like \"fix this or I will switch to xy\". Either you do switch or you stick with the application. Really, it's that simple. It won't, however, affect reality. If you still don't belive me, I have some news for you: We have limited resources which work on a voluntary basis, unless paid by somebody. There are enough ways to pay people or donate money or time to help fixing whatever bothers you."
    author: "Daniel Molkentin"
  - subject: "Re: What about fixing the remaining datalos bugs f"
    date: 2006-01-15
    body: "> Yes, I'm afraid this does sound arrogant. \n\nAnd telling people what to do with their free contribution to you isn't?\n\n> We all know how F/OSS works, but not everyone has the skills to send patches. \n\nSkills are learned and require effort. A more appropriate statement would be that almost no one has developed the skills to send patches. Most are content to \"consume\" without contributing anything, but some people do compensate by assuming a leadership role and telling developers what to do instead.\n\n> And it's silly to do one more refactoring/feature while data loss bugs remain.\n\nGreat analysis! So given that it's unlikely even the most titanic effort can alleviate all bugs then all refactoring and new features should stop. Never mind that new features will attract more users and developers and refactoring can end up fixing fixing bugs by replacing buggy code.\n\nWhen given the choice between an utterly clueless critic and someone actively contributing their time to work on making the software it seems painfully obvious to me who is better qualified to direct the project.\n\nI would also like to say that if you want bugs fixed and your strategy is to antagonize developers then you're not very bright. We all want to fix our bugs but we're only going to listen to pompous condescending tripe for money, and some of us won't do it for any price. I would ask that if you lack that much in the social skills you not try to make it more unpleasant for volunteer developers to fix bugs. There is an old saying, \"you'll catch more flies with honey than you will with vinegar\". You want to shame somebody into fixing something? Send them a few Euro and tell them how much you love their program... except for this bug... \n\nHey, you can run Windows and see if you can even talk with a developer and how many years it will be before they fix a bug OR add a feature. You want a say? Contribute enough to be in the discussion. Otherwise politely submit your bugs and stifle yourself."
    author: "Eric Laffoon"
  - subject: "Re: What about fixing the remaining datalos bugs f"
    date: 2006-01-16
    body: "> but not everyone has the skills to send patches\n\nThen you have the option to pay someone for the bugfix."
    author: "Anonymous"
  - subject: "Re: What about fixing the remaining datalos bugs first"
    date: 2006-01-15
    body: "Till,\n\nwhile you are right and all, doesn't it qualify as Developer Trolling? ;-)\n\nI mean, you knew what kind of reaction that answer creates.\n\nHow about:\n\nSorry, we are too few and too bad. If you are better, join us. If you can't, I fear we all will have to live with what we do.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: What about fixing the remaining datalos bugs first"
    date: 2006-01-15
    body: ">How about: Sorry, we are too few and too bad.\n\nI hope you are kidding. Till is one of the most skilled and qualified developers I've ever met. If you think you can do better, show me the bug and I will award you a burger king crown.\n\nDaniel"
    author: "Daniel Molkentin"
  - subject: "Re: What about fixing the remaining datalos bugs first"
    date: 2006-01-16
    body: "That doesn't matter. You (or, for that matter, Till) may be the greatest programmer on Earth but even so a bit of humility won't hurt you. If you think you only want to talk to other l337 c0d0rz you may find at some point that no one is left who wants to talk to *you* :^)\n\nIncidentally, my personal impression is that KMail 1.9 is decidedly a step down from the previous version as far as stability is concerned. Right now I have a message which causes a fatal memory leak in the IMAP4 ioslave -- something that never happened to me before. I'm now trying to trim down the message in question so I can file a bug report without a 200K attachment. Or do you recommend I should just send the bug report to /dev/null instead, since I'm not going to send a fix along with the report, and the developers can't be bothered to look at it anyway because they're so busy designing next year's bugs?\n\nAnselm"
    author: "Anselm Lingnau"
  - subject: "Re: What about fixing the remaining datalos bugs first"
    date: 2006-01-16
    body: "Hey Anselm,\n\n> Incidentally, my personal impression is that KMail 1.9 is decidedly \n> a step down from the previous version as far as stability is concerned.\n\nAgreed. For various reasons most of us have not been able to spend as much time as we would like on KMail, and thus it suffers. Especially before the 3.5 release there was hardly anyone around to do maintenance and quality control, and it shows. We are not successful in recruiting new developers, and the old ones are busy and frustrated with the legacy code base, which makes them less inclined to spend their spare time fixing bugs, critical or otherwise.\n\nNow, we can either keep going like that, and let the experienced developers walk away from a failing project, taking all they've learned with them, waiting for the next whizz-kid to come along and write a groupware suite from scratch, or we can try to use our experience to create a foundation on which building something that actually solves the new requirements and old problems is both possible and fun again. If it's not fun, people won't do it for fun, and if it's not possible, even those who are motivated to work on stuff without immediate gratification, or those who are paid to see past that, will not attempt it.\n\nWe are, of course, a bunch of socially inept brainiacs with overinflated egos, and thus have to blame ourselves for alienating contributors, but we truly believe that if we don't take the opportunity KDE 4 gives us to rework the fundamentals, and if we don't concentrate our very scarce resources on that, even at the expense of stability and bugfixing, KDE PIM will not be able to compete, grow and remain fun to work on for the next generation of (hopefully less arrogant) hackers.\n\nAs far as my personal lack of humility goes, I'm sorry to say that I've lost most of the respect I initially had for our user base after years of seeing no significant contribution from the vast majority of them, a general attitude of consumerism and an irritating sense of entitlement, to the point of less than friendly suggestions as to how I should best spend my spare time to maximize their benefit. That has made me irritable and defensive, as shown by my knee jerk reaction to the first comment. But of course I also know I rock, so humility does not come naturally. ;)\n\nThanks for favoring us with a name, btw, unlike the other posters, that makes giving someone respect so much easier.\n\nTill"
    author: "Till Adam"
  - subject: "Re: What about fixing the remaining datalos bugs first"
    date: 2006-01-17
    body: "Hey, \n\nI'd like to thank you very much for the good work you are doing. Yes, there might be bugs (even annoying bugs) but KMail is a wonderful application and I love using it.\n\n\nJonathan\n"
    author: "Jonathan"
  - subject: "Re: What about fixing the remaining datalos bugs f"
    date: 2006-01-18
    body: "I have used KMail for many years with great success; I have seen a very few bugs, and by and large, KMail has been one of the most valuable applications I have ever used. Thank you very much for bringing that about. No need to be frustrated with an allegedly too disordered (??) code base - it's a great application, and that is what counts.\n\nYes, it is true that I am always wary of potential new bugs from one version to the next, and I will probably take a while before I upgrade to anything from KDE 3.4.x ... but that has nothing to do with lack of respect for the developers, to whom I am deeply indebted.\n\nFinally - it is true that it would be nice (from a user's perspective) if bugs were simply fixed over time, and I am sure many are. And, I do submit bug reports if I find something ... which I consider giving back to the project, and by no means do I intending to kick or annoy the developers. Best of luck with KDE 4!\n\nvb"
    author: "Volker Blum"
  - subject: "Re: What about fixing the remaining datalos bugs f"
    date: 2006-01-18
    body: "Till,\n\nI make sweet-sweet love to Kmail all day and kiss her all over during the night. You know how I roll, playa."
    author: "Joe"
  - subject: "Re: What about fixing the remaining datalos bugs first"
    date: 2006-01-15
    body: "> while you are right and all, doesn't it qualify as Developer Trolling? ;-)\n> I mean, you knew what kind of reaction that answer creates.\n\nYeah, I guess you're right. Can't help myself, sometimes. \n\n> Sorry, we are too few and too bad. If you are better, join us. If you can't, \n> I fear we all will have to live with what we do.\n\nNot sure that qualifies as less arrogant. Funnier, though. ;) \n\nTill"
    author: "Till Adam"
  - subject: "Re: What about fixing the remaining datalos bugs first"
    date: 2006-01-16
    body: "The one qualifying as most arrogant are undoubtedly the initial comment, and had you used the funnier variant I doubt the poster had understood the significance of that one either."
    author: "Morty"
  - subject: "Re: What about fixing the remaining datalos bugs first"
    date: 2006-01-16
    body: "-agreed-\n\nas non-developer, i try to respect developers, cuz THEY are the ones spending lots of time on making what i use beter and a pleasure to use..."
    author: "superstoned"
  - subject: "Re: What about fixing the remaining datalos bugs first"
    date: 2006-01-15
    body: "pfff. try to command some1 what to do wif their free time. lol. if tjey like to do that staff, fine, go ahead. <br>\ni cannot remember a new kde (-app) version ever left me dissapointed. ever. <br><br> so thanks every1 involved making my email-app rock, keep up the good work and dont feed the trolls!"
    author: "elveo"
  - subject: "Re: What about fixing the remaining datalos bugs first"
    date: 2006-01-17
    body: "I do not know whether this is the case here, but sometimes, codebases get so convoluted that bugs are very hard to discover. In those cases, you first have to refactor before you can find the bug (or eradicate it during the refactoring process)."
    author: "PC Squad"
  - subject: "Re: What about fixing the remaining datalos bugs first"
    date: 2006-01-20
    body: "> What about fixing the remaining datalos bugs first \n\nThis pointless and crude remark started one of the worst chain of posts I've seen in quite a while :-)\n\nSure, there are a few things about Kmail that aren't perfect. I've never lost any data, but I'm not fond of the way you move up and down between messages, and a few other things.\nBut, on the whole, Kmail is one of my must-haves. I use it many times every day and have done so for years. \nI think Till and others do a magnificent work and, yes, I contribute to the development of KDE every year - both by paying for distributions and by contributing to different projects. (Actually, Windows and pirated apps would have costed me far less by now :-) :-) )\n\nAs someone else noted, there's something special by the way KDE (and other OSS) spreads around the world, and I'm happy to pay my part as long as I can (since I'm no programmer). Even if I could afford to pay for Windows, there are many others that can't.\n\n/Goran J\nVarberg, Sweden\n"
    author: "G\u00f6ran Jartin"
  - subject: "sounds great."
    date: 2006-01-16
    body: "the most intriging point here (i mean the article, not the sucky first comment that had to be about the troublesome past instead of the brigh future ;-) ) is \"Akonadi is hoped to be shared by the Gnome and Mozilla teams\" - if this backend is general enough to be used by all these projects, it will not only attract more developers, but also prove its quality (its harder to write something usable by different apps...).\n\nkudos to the dev's for starting such a project!"
    author: "superstoned"
  - subject: "Re: sounds great."
    date: 2006-01-16
    body: "GNOME already has this, its call eds and is in pretty wide use in the desktop."
    author: "bardo"
  - subject: "Re: sounds great."
    date: 2006-01-16
    body: "EDS cannot do mail. The limitations of EDS are precisely the reason why this project is also interesting to the Evolution developers."
    author: "Till Adam"
  - subject: "Re: sounds great."
    date: 2006-01-16
    body: "I am a Evolution hacker. I know for a fact that we have been trying to make eds a datastore for mails and have given up due to the complexity. Also bonobo does not scale very well with huge amounts of data ( calendar and tasks have much lesser data than mail stores ). Our few attempts with dbus haven't been conculsive to prove if it does any better. I for one am very keen on this project. "
    author: "Shreyas Srinivasan"
  - subject: "The truth that I know and some of you don't !!!"
    date: 2006-01-16
    body: " I have met Till personally couple of months back and since then we have been in the loop constantly on email, irc and chat. Those who donot know him and have posted all crap (I must say a load of crap has been posted by few posters) will surely in for a surprise once they come to know Till.\n\n Till is perhaps one of the most helpful persons around in the KDE circles. He has actually tutored me and answered to my infinite questions without a single aaargh! or oh noes!. Till knows that, I know that and few more people know that very well that how helpful Till has been in formation of KDE-India.\n\n It has never occured that he has not answered to my mail or my call on the chat, I repeat NEVER!!! Read that again, NEVER!\n\n All he said that was, perhaps in jest, dude nice bug hunt, help us fix it.\n\n Do you guys even know how many hackers are right now involved in KMail? You would be surprised to know the actual number because then you would wonder - man, how on earth do they work on KMail ( and KDE-Pim in general ) with such scanty resources.\n\n As Shreyas pointed out, I heard him talk at the same place same time I met Till, dbus and other technologies have their own share of problems and I donot think there is a better judge of the same than Shreyas as he is one of the core Evolution hackers.\n\n  So people donot just talk because you have to. You might just scare away a really good programmer from a cool project.\n\n  Cheers!\n "
    author: "Pradeepto Bhattacharya"
  - subject: "Re: The truth that I know and some of you don't !!"
    date: 2006-01-16
    body: "Very interesting!  I did not realize that Till was Indian until now."
    author: "KDE User"
  - subject: "Re: The truth that I know and some of you don't !!"
    date: 2006-01-17
    body: ">Very interesting! I did not realize that Till was Indian until now.\n\n    No he is not an Indian. What has that got to do with it anyways? He had helped and still helps the KDE India community and I am sure he helps a way lot more people that we donot know about.\n"
    author: "Pradeepto Bhattacharya"
  - subject: "Re: The truth that I know and some of you don't !!"
    date: 2006-01-18
    body: "Because sometimes it's interesting to picture people behind the name?\nDon't be so sensitive. I doubt he was going to break out with any racial hatred."
    author: "Joe"
  - subject: "Re: The truth that I know and some of you don't !!"
    date: 2006-01-18
    body: "And maybe because it's something nice with the fact that most OSS projects span around the whole world :)"
    author: "Jo \u00d8iongen"
---
For the fourth consecutive year a group of <a href="http://pim.kde.org">KDE PIM</a> developers followed the gracious invitation of <a href="http://www.intevation.de">Intevation GmbH</a> to meet at their headquarters in Osnabr&uuml;ck, Germany on the first weekend in January. As in the past years, the face-time proved very productive especially since everyone felt that with KDE 4 the time for more fundamental changes has come. By the end of the meeting the group had agreed on a vision and initial architecture for a unified, extensible storage service for PIM data and metadata, allowing all applications on the desktop fast and reliable access as well as powerful search capabilities. This service, codenamed "<a href="http://pim.kde.org/development/meetings/osnabrueck4/architecture.php">Akonadi</a>", together with intiatives like <a href="http://plasma.kde.org">Plasma</a> and <a href="http://solid.kde.org">Solid</a> will form the basis of an 
exciting KDE 4 experience.







<!--break-->
<div style="float: right; border: thin solid grey; padding: 1ex; margin: 2ex"><a href="http://pim.kde.org/development/meetings/osnabrueck4/group.php"><img src="http://static.kdenews.org/danimo/os4_group_small.jpg" width="200" height="150" /></a><br /><a href="http://pim.kde.org/development/meetings/osnabrueck4/group.php">The obligatory group photo:<br /> same procedure as every year</a></div>
<p>Akonadi is hoped to be shared by the Gnome and Mozilla teams as well as 
any other software dealing with email, calendaring, contact management, 
and related data, on the free desktop. Initial reactions from non-KDE 
PIM developers have been positive and the KDE team is looking forward 
to their contributions. Following the adoption of <a 
href="http://www.opensync.org">OpenSync</a> as a common syncing platform, this 
project is another conscious step for KDE towards closer cooperation 
and sharing of infrastructure.</p>

 
<p>Everyone who attended Osnabr&uuml;ck IV would like to thank the Intevation 
team for their continued support of KDE PIM, and for creating and 
keeping alive the fine tradition of these meetings. Notes from the 
meeting can be found <a 
href="http://pim.kde.org/development/meetings/osnabrueck4/overview.php">here</a>.</p>







