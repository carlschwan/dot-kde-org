---
title: "KDE 3.5.4 VMware Image Available"
date:    2006-08-08
authors:
  - "Anonymous"
slug:    kde-354-vmware-image-available
comments:
  - subject: "Why not use Qemu images?"
    date: 2006-08-08
    body: "Why not use Qemu images?\nVMware is not free software, but qemu is."
    author: "Ali3n"
  - subject: "Re: Why not use Qemu images?"
    date: 2006-08-08
    body: "because standard qemu is slow, the accelerator kqemu is non-free, and qvm86 isn't fully functional yet"
    author: "Tux"
  - subject: "Re: Why not use Qemu images?"
    date: 2006-08-08
    body: "can't build it with gcc4"
    author: "Marc Collin"
  - subject: "Re: Why not use Qemu images?"
    date: 2006-08-09
    body: "Vmware player is free.  So is what used to be called GSX... \n\nMay not be OSS, but its free to use."
    author: "ziggy"
  - subject: "Re: Why not use Qemu images?"
    date: 2006-08-09
    body: "According to qemu changelog, it supports VmWare images starting with version 0.6.1."
    author: "anonymous"
  - subject: "Re: Why not use Qemu images?"
    date: 2006-08-09
    body: "I might be wrong, but between Workstation 4 and 5.5 VMWare changed the format."
    author: "Giacomo"
  - subject: "Re: Why not use Qemu images?"
    date: 2006-08-10
    body: "VMware Server is free.\nI am using it on a windows xp mashine since the first beta of VMware Server.\nYou can download it here http://www.vmware.com/download/server/ "
    author: "Thony Johansson"
  - subject: "Re: Why not use Qemu images?"
    date: 2006-08-11
    body: "> VMware Server is free.\n\nbut not Free Software. that's what the parent meant (and wrote, modulo caps)."
    author: "andrew"
  - subject: "Re: Why not use Qemu images?"
    date: 2006-08-18
    body: "Actually .. VMWare Server is free now, as is VMWare player...."
    author: "Rick Lossner"
  - subject: "Re: Why not use Qemu images?"
    date: 2006-08-20
    body: "There are several reasons to use or not to use both either Qemu or VMware.  It is mainly just preference.  Personally, I prefer QEMU. It is a little harder to set up, but once it is, the fact that I can just click on a Linux/Windows/Mac/BSD icon on my desktop and a fully functional OS pop up in a dedicated window is nice, but,  again, mainly preference. Also, I would recommend the May 2006 issue of Linux Journal to those who want a more in depth look at each.  It also covers Xen and User-Mode Linux."
    author: "Digital Vampire"
  - subject: "Re: Why not use Qemu images?"
    date: 2006-08-26
    body: "Thanks for the info on linux journal. It was a good read!"
    author: "winter"
  - subject: "Superkaramba/liquid wheather not working"
    date: 2006-08-08
    body: "I downloaded and ran it.  Looks good, but superkaramba has a problem with\nthe liquid wheather theme.  I can provide the backtrace if that is useful.\nI have the same configuration at home and there superkaramaba/liquid\nwheather is working fine.  So, at 1st glance it looks a problem with the\nvmware image.\n\n"
    author: "Richard"
  - subject: "Re: Superkaramba/liquid wheather not working"
    date: 2006-08-08
    body: "Liquid Weather v13.0 RC2 runs just fine for me with that image."
    author: "Anonymous"
  - subject: "Screenshots"
    date: 2006-08-09
    body: "http://shots.osdir.com/slideshows/slideshow.php?release=716&slide=1"
    author: "Anonymous"
  - subject: "Thanks"
    date: 2006-08-10
    body: "Thank-you for making this available. I am pleased to have KDE 3.5.4 on SUSE Linux 10.1 running on my Microsoft Windows XP machine using VMPlayer. It was good to try something other than Fedora which I have on my Linux machine. YAST is pretty spiffy. Thanks, again."
    author: "jcwinnie"
  - subject: "Problems with image of KDE 3.5.4"
    date: 2006-08-12
    body: "I run the image in VMware Workstation 5.5.1. \n- In Yast2 it is not possible to change the keyboard layout. The module does not start. No messages.\n- It is not possible to start Sax2 (in order to change the layout there). No messages.\n- If I go back to runlevel 3 (Init 3), it is not possible to log in:\n# user: root\n# password: toor\n# error in service module\n\nAny ideas or suggestions? \n"
    author: "Achim"
  - subject: "Re: Problems with image of KDE 3.5.4"
    date: 2006-08-17
    body: "Hi,\n\nI have the same problems with this image.. Does any one know whether there are other forums where this problem can be discussed?\n\nThanks in advance"
    author: "Coen"
  - subject: "Re: Problems with image of KDE 3.5.4"
    date: 2006-08-17
    body: "\"sux\" in a terminal under KDE and run \"sax2 -r\""
    author: "Anonymous"
  - subject: "Re: Problems with image of KDE 3.5.4"
    date: 2006-08-17
    body: "> # error in service module\n\n\"su\" in a terminal under KDE and run \"touch /var/log/lastlog\""
    author: "Anonymous"
  - subject: "Re: Problems with image of KDE 3.5.4"
    date: 2006-08-29
    body: "thanks it works well"
    author: "Tom"
  - subject: "Problems with online update & online installation"
    date: 2006-08-23
    body: "The already installed installation sources seem to be false, but I cannot correct them. Online update shows items to be updated, but on start online update crashes an disappears"
    author: "ktrott"
  - subject: "Updating SuSE"
    date: 2006-08-30
    body: "I seem to be unable to find/add the correct update repositories. The mirror included in yast contains older software than what is installed. I've read documentation but every mirror I add has the same problem. Any ideias ?"
    author: "Rodrigo Fernandes"
---
A <a href="http://developer.kde.org/~binner/vmware/">VMware Player image of KDE 3.5.4</a> with KOffice 1.5.2 running on <a href="http://www.opensuse.org/">SUSE Linux</a> 10.1 is <a href="http://www.kdedevelopers.org/node/2226">now available</a>. The image is fully functional and can be upgraded and tweaked as needed. The <a href="http://www.vmware.com/products/player/">VMware Player</a> necessary to run it can be <a href="http://www.vmware.com/download/player/">downloaded for free</a> and is available for Microsoft Windows and Linux.


<!--break-->
