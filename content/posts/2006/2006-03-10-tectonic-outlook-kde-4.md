---
title: "Tectonic: An Outlook on KDE 4"
date:    2006-03-10
authors:
  - "uthiem"
slug:    tectonic-outlook-kde-4
comments:
  - subject: "The other gem - HP chooses Linspire over Ubuntu fo"
    date: 2006-03-10
    body: "THe other, not least interesting gem in that mag was the article about HP choosing Linspire (KDE-land) over Ubuntu (you know..) for their laptop line for South Africa. page 7"
    author: "Daniel D."
  - subject: "Re: The other gem - HP chooses Linspire over Ubunt"
    date: 2006-03-10
    body: "Ubuntu can be moved to KDE quite easily. It's called Kubuntu. I guess the desktop is not the main reason for HP's choice."
    author: "oliv"
  - subject: "Re: The other gem - HP chooses Linspire over Ubunt"
    date: 2006-03-10
    body: "Yep\n\nthough I am really amazed of how much more popular Ubuntu is as compared to Kubuntu, even though KDE as a Desktop is extremely popular. I guess it is the fact that: first, Ubuntu presents an extremely well integrated Gnome desktop. Second: Kubuntu is a more recent project, and it is still lagging behind as a project (Despite Mark S's commitment to Kubuntu as a first class citizen)."
    author: "KubuntuUser(exMandrake)"
  - subject: "Re: The other gem - HP chooses Linspire over Ubunt"
    date: 2006-03-10
    body: "Simple.  The overall project is \"Ubuntu\" to most people, so that's the one that gets talked about.  To fix it, Ubuntu should NOT be GNOME, but rather, the overall project, with Gubuntu and Kubuntu as sub-species.\n\nThe recent announcement that KDE should become an equal alternative to GNOME in (K)Ubuntu seems to show that this was an oversight, probably due to shuttleworth's earlier unfamiliarity with KDE.  The least they could do is try to redress the balance and allow free and equal choice by users of which desktop they go with.  I'd be surprised if that ever happens, though :("
    author: "Lee"
  - subject: "Re: The other gem - HP chooses Linspire over Ubunt"
    date: 2006-03-10
    body: "absolutely:\n\napt-get install kubuntu-desktop\n\n-> will convert an installed ubuntu to kubuntu\n"
    author: "ac"
  - subject: "Unwarranted attack on Free Software and GPLv3"
    date: 2006-03-10
    body: "While the magazine seems professionally made, and I'm grateful that they did a feature story on KDE 4, I can't help but cringe at their attack on version 3 of the GPL and Free Software in general. In the opinion piece on page 7, this magazine falsely quotes Richard Stallman and Eben Moglen as saying that \"making money from software is a a great evil\". This is definitely NOT the Free Software Foundation's opinion at all -- in fact, Richard Stallman wrote an essay explaining why selling Free & Open Source is perfectly OK and even encouraged! \n \nhttp://www.fsf.org/licensing/essays/selling.html \n \n \nThis magazine also refers to copyleft (forced sharing) protections as viral, reminiscent of Microsoft CEO Steve Ballmer's characterization of Linux as \"a cancer\": \n \nhttp://slashdot.org/articles/01/06/01/1658258.shtml \n \n \nThe opinion paper also fails to mention the dangers of Digital Restrictions Management (DRM), also known as Treacherous Computing and Handcuffware. The reason we need the anti-DRM provisions in GPLv3 is to make sure the user controls his/her computer, not some unscrupulous corporation (remember the Sony Rootkit: http://yro.slashdot.org/article.pl?sid=05/11/13/1419206) \n \nhttp://www.gnu.org/philosophy/can-you-trust.html \nhttp://www.eff.org/IP/DRM/guide/ \n \n \nI'm willing to give the magazine the benefit of the doubt and say that opinion piece is pure sarcasm. But they better stop spreading this FUD, because someone who doesn't know better might actually believe it."
    author: "ac"
  - subject: "Re: Unwarranted attack on Free Software and GPLv3"
    date: 2006-03-10
    body: "I'm with you. There is something fishy with this mag. Why do they associate free software with that despicable mass murderer?"
    author: "Mr X"
  - subject: "Re: Unwarranted attack on Free Software and GPLv3"
    date: 2006-03-10
    body: "This might show a gap of political knowledge, twice.\n\nSome people are honoring Lenin as leader of an revolution. And this magazine is using his picture as reference to an revolution for it's audience. Audience starting with free software is often the poor, young and empowering part of peoples. They take [free|opensource|non profit] software as cultural gift from firstworld's part of mankind.\n\nAcknowledging magazines' failures related to GPL and giving some hints for better PR, you're well done with informing about European history in the 1st quarter of 20th century."
    author: "saxe"
  - subject: "Re: Unwarranted attack on Free Software and GPLv3"
    date: 2006-03-10
    body: "\"I'm willing to give the magazine the benefit of the doubt and say that opinion piece is pure sarcasm.\"\n\nDuh - well spotted. The entire article is pure sarcasm from beginning to end. Here are some of the not-so-subtle hints that I, the author, was being sarcastic when I wrote this:\n\n\"Awake Comrades!\"\n\"Comrade Stallman...\" \n\"Your humble narrator need hardly remind you of the Great Evil of trying to make money from software.\"\n\"The Great Leap Forward of '89\"\n\"Our great forerunners Lenin and Marx...\"\n\"Your compulsory weekly education classes...\"\n\nAnd so on and so on. If you're stupid enough to even think that this is something other than an obvious parody of typical Communist literature, that's not my problem. \n"
    author: "camperman"
  - subject: "Re: Unwarranted attack on Free Software and GPLv3"
    date: 2006-03-10
    body: "I have to agree the sarcasm was *very* obvious:\n\n\"Millions of companies have gone bankrupt\ntrying to make money from it and over 100\ngovernments have collapsed trying to oppose\nit - which is, of course, just as we intended.\"\n"
    author: "Erik"
  - subject: "Re: Unwarranted attack on Free Software and GPLv3"
    date: 2006-03-10
    body: "Asserting false statements about the GPL and Free Software, even as a joke, can be very detrimental to its public image (and to the image of the magazine, for that matter). Remember, Microsoft has packs of lawyers/propagandists ready to pick up on any misconception and use it to smear KDE, Linux and other Open Source projects. They have done it in the past (see Ballmer\u0092s statement), and they\u0092ll do it again.\n\nIt is counterproductive to scare businesses into thinking the GPL has \u0093anti-profit\u0094 clauses, when that is obviously wrong. The GPL is about protecting the user\u0092s rights to control his/her computer. \n\nBolshevik communism caused enormous suffering and the deaths of millions of people, so it is no wonder that a significant portion of the world has an allergic reaction to it. Associating bolshevik communism with Free Software will only cause those people to passionately reject Free Software right off the bat. \n\nAssociating Free Software with a positive historical force (ie. Martin Luther King Jr. or Ghandi) would be more appropriate. But leave out the sarcasm, since it might be construed as disrespecting the legacies of those historical figures."
    author: "ac"
  - subject: "Re: Unwarranted attack on Free Software and GPLv3"
    date: 2006-03-11
    body: "A community which has lost the ability to parody itself isn't a particularly attractive community to be a part of.  You don't have to like the article, or the particular humor that it's using, but not everything published in the Linux world need be propaganda pieces.  Or are you suggesting that we should forgo humor \"for the good of the party\"?  ;-)"
    author: "Scott Wheeler"
  - subject: "Re: Unwarranted attack on Free Software and GPLv3"
    date: 2006-03-11
    body: "Following the rather heated debate on this article, we have published it on our website (http://www.tectonic.co.za/view.php?id=914). We believed the article to be quite obviously satirical. Please let us know what you think.\n\nThe Tectonic Team"
    author: "Jason Norwood-Young"
  - subject: "Re: Unwarranted attack on Free Software and GPLv3"
    date: 2006-03-12
    body: "I think your article on KDE 4 was very good.\n\n(Stallmanism isn\u00b4t my cup of tea, I follow comrade Linus.)"
    author: "reihal"
  - subject: "apt-get install humor"
    date: 2006-03-11
    body: "Sorry comrade that a pitiful magazine of the enemy is trying to taint our vision. Rest assured that we will resist, we will fight. Let's forge an E-Jihad against those infidels. How do they dare to insult what is most dear to us? \n\nDon't they know that our diety-inspired GPL is holy to our beliefs? God speaks to us through his and only prophet Stallman, may Allah protect him from these GPL-less infidels.\n\nYes, we must carry on our plan! Anything that doesn't spit pure propaganda of the GPL and FSF is the enemy! They're either with us or aganist US!\n\nIn the spirit of the 3rd coming of the almight GPL, I hereby require all followers to gather and issue an E-Fatwa that will put a definite stop to this heresay!\n"
    author: "RMS"
  - subject: "finally someconcrete information"
    date: 2006-03-10
    body: "I think that this arcticle give a better idea of what KDe is about it gives some \nexamples of what wecan expect and sets the expecationt really high. I also get the impresion that they have a clear roadmap. They already had XGL in plasma. Plus I am no longer afraid that Kde was going to do the rest of the XGL stuff and just copy the effects"
    author: "tikal26"
  - subject: "I'm still afraid..."
    date: 2006-03-10
    body: "I like some of the things I see in KDE 4, but somehow I'm still a little afraid, because the stakes a placed so very high, and ofcourse the expectation are getting higher and higher. I hope it won't be disappointment, like so many part II or III of movies (like Matrix, Starwars).\n\nWhat would be a nice idee, would be to provide a theme that looks like the GNOME team, disables all configuration options (like GNOME). Don't put it as default, but than everyone can make there own choice... As Default would be nice a 'sane' configuration somewhere between GNOME and KDE 3.5, with nice defaults... But ofcourse the possibility to extend or rape the user experience...\n\nOne last thing I hope, is they will separate the konqueror (or how they will call it) from the desktop, I think it is too stupid like windows, one explorer crashes, your whole desktop is gone...."
    author: "Boemer"
  - subject: "Re: I'm still afraid..."
    date: 2006-03-10
    body: "\"One last thing I hope, is they will separate the konqueror (or how they will call it) from the desktop\"\n\nKonqueror doesn't control the desktop as far as I know.  Maybe you mean that if one konqueror (file manager only by default) window crashes, they all dissapear.  You can change that in the settings under performance.  It will take longer to open a new window though since it will have to start a new process."
    author: "Leo S"
  - subject: "Re: I'm still afraid..."
    date: 2006-03-10
    body: "He probably mistakes Konqueror with Nautilus :o)"
    author: "ac"
  - subject: "Re: I'm still afraid..."
    date: 2006-03-11
    body: "I think I'm confusing Nautilus with Konqueror, but just wanted to be sure! That's one of the most irrating things, I know of under windows....\n\nThe moment Krusader has all the missing features I need, like I'm used to under Total Commander, I won't be much visiting windows...."
    author: "boemer"
  - subject: "Re: I'm still afraid..."
    date: 2006-03-10
    body: ">As Default would be nice a 'sane' configuration...., with nice defaults.\n\nThe defaults in KDE 3.5 are already both sane and nice, and they \"just work\" if you want to word it like that. The logic used when people basicly claim that having fewer options somheow makes the default better, are seriously flawed. \n\n>separate the konqueror from the desktop\n\nAre you sure you are not mixing up KDE with Gnome? As I recall they are the one using their filemanager, Nautilus, to draw the desktop.\n"
    author: "Morty"
  - subject: "Re: I'm still afraid..."
    date: 2006-03-10
    body: "> The defaults in KDE 3.5 are already both sane and nice,\n\nReally?\n\nLines between menu, toolbars and content (should be disabled). Maximize button directly next to the close button (one or two spaces have to be inserted)\n\nNo \"keep above\" button in the window title bars.\n\nBad and inconsistent konqueror configurations. konqueror --profile simplebrowser does not harmonize with the other profiles (use simple browser profile, switch to your home directory and try to switch \"detailed\" icon view)\n\n\nand so on.\n\n\n"
    author: "max"
  - subject: "Re: I'm still afraid..."
    date: 2006-03-10
    body: ">> The defaults in KDE 3.5 are already both sane and nice,\n>Really?\n\nYes, and the defaults should be nice and sane for the majority of users, your personall preferences on the other hand may not be. But if you have some settings where a change would make a improvement for the majority of users, mailing a polite and nicely reasoned explanation and argument for why it should change to the developers will have them consider it.\n\n\n>Lines between menu, toolbars and content (should be disabled).\n\nPersonally I prefer the default, depends if your or my personall preference is preffered by the majority:-)\n\n\n>Maximize button directly next to the close button (one or two spaces have to be inserted)\n\nDefenatly a personall preference of yours, the majority of desktops does it the same way as KDE. But I can agree to it beeing wrong, close should be in the left corner with minimize and maximze in the right corner like the Laptop decoration. A strong personall preference I'd guess, and I'm not sure if it's advisable to base the defaults on it even if it's the most useable and userfriendly approach.\n\n\n>No \"keep above\" button in the window title bars.\n\nYour preference again, it's not that widely used and lots of users do never need it. Besides it's quikly avalible in the right click menue and easily added to the window title bar for users with special needs.\n\n\n>use simple browser profile, switch to your home directory and try to switch \"detailed\" icon view\n\nWhy would the majority of users (or any user) use the simple (web)browser profile to view their home directory in \"detailed\" icon view?\nBesides it's a well known fact that there some nastyness hidden in the Konqueror profile handling, but it does not really affect the default settings and the majority of users. They use the browser profile for browsing the web, and the filemanger profile for managing files."
    author: "Morty"
  - subject: "Re: I'm still afraid..."
    date: 2006-03-11
    body: ">>Lines between menu, toolbars and content (should be disabled).\n\n> Personally I prefer the default, depends if your or my personall preference\n> is preffered by the majority:-)\n\nIn KDE 3.5.2 \"no lines\" are default :-)\n\n>Maximize button directly next to the close button (one or two spaces have to\n> be inserted)\n \n> Defenatly a personall preference of yours, the majority of desktops does it\n> the same way as KDE.\n\n*Every* HID Guide says: Do not put destructive buttons directly next to often used buttons. I don't want to place the close button on the left hand side. I only want some millimeters of space between close and maximize.\n\n>use simple browser profile, switch to your home directory and try to switch\n> \"detailed\" icon view\n \n> Why would the majority of users (or any user) use the simple (web)browser\n> profile to view their home directory in \"detailed\" icon view?\n\nMaybe you want to access an ftp-server when you are webbrowsing? ftp://ftp.kde.org ...\nMaybe you happen to click on the \"home\" button?\nmaybe you want to do some copy&paste between ftp.kde.org and a new tab with your download directory?\n\nThere is *no* reason to close the webbrowser and start a new filemanager. That's a KOnqueror feature. Unfortunately there is one profile which makes this feature unusable."
    author: "max"
  - subject: "Re: I'm still afraid..."
    date: 2006-03-11
    body: ">>the majority of desktops does it the same way as KDE.\n\nI was a little hasty there. XPs Luna theme does, but not the windows classic theme:-)\n\n>*Every* HID Guide says: Do not put destructive buttons directly next to often used buttons.\n\nCan agree to that, but when it comes to window decorations it looks like few cares:-)\n\n>I don't want to place the close button on the left hand side.\n\nWhat can I say, you should try. When you get used to it you wouldn't want it another way:-). And remember to drop the menu button, it's redundant since a right click on the titlebare gives you the same.\n\n>I only want some millimeters of space between close and maximize.\n\nAll windowdecorations in KDE allows for customizing button positions in the titlebar, just open the dialog and drop buttons and spacers the way you want them. \n\nIf you really want it as default, open kdebase/kwin/clients/plastik/plastikclient.cpp and change line 66 from 'return \"HIAX\";' to 'return \"HIA_X\";' make a patch and send it to the maintainers. Or you can simply try to convince them to do it, as Aron said it's work in progress."
    author: "Morty"
  - subject: "Re: I'm still afraid..."
    date: 2006-03-10
    body: "> Lines between menu, toolbars and content (should be disabled)\n\nhad you said this a month ago, it would've perhaps been insightful. but i beat you to it with a commit that changes this for 3.5.2 =P\n\nwhat often escapes people is that kde is:\n\na work in progress -> we don't starve you for 3-5 years for a new release and then hope we \"got it right\" and you're stuck with that for better or worse. we release constant incremental changes. those changes are usually improvements, but sometimes they are not. the progress over time, however, is a very compelling march towards perfection. now, perfection is an unachievable but we can move towards it.\n\na toolkit upon which others build -> you may note that virtually every distribution modifies in some way the window decorations. suse puts a space between the close and maximize buttons, for instance. \n\n> No \"keep above\" button in the window title bars\n\nwhy should this be on by default?!\n\n> konqueror --profile simplebrowser does not harmonize with the other profiles\n\ni agree this profile could be even better. but it's another -optional- profile. as such it will be -different-. that's sort of the point. it's a little odd to complain that configuration possibilities result in making things different.\n\nwell... yes. ;)"
    author: "Aaron Seigo"
  - subject: "Re: I'm still afraid..."
    date: 2006-03-11
    body: ">> Lines between menu, toolbars and content (should be disabled)\n\n> i beat you to it with a commit that changes this for 3.5.2 =P\n \nAs usuall it's simply a case of the developers to know/care/agree or any combination thereof, and the changes come. As opposed to this example the developers sometimes need feedback to keep the work in progress improving, so help them by interacting:-)\n\nAs stated above, I kind of like the old way. Is it still possible to keep the old behavior or do I have to learn to live with the new way? \n\nRegardless, I kind of like your mission of removing unnecessary lines:-) Way to go Aron. Any chance of getting rid of the 3 pixel line between the statusbar and windowdecoration border in Konqueror?"
    author: "Morty"
  - subject: "Re: I'm still afraid..."
    date: 2006-03-12
    body: ">  Is it still possible to keep the old behavior or do I have to learn to live\n> with the new way?\n\nit's full configurable. the KDE way(tm)\n\n>  Any chance of getting rid of the 3 pixel line between the statusbar and\n> windowdecoration border in Konqueror?\n\nunfortunately not before kde4. qt3 doesn't let us style the tab widget properly, but qt4 does. i suppose we need to save SOME of our mojo for kde4, right? ;)\n\n"
    author: "Aaron J. Seigo"
  - subject: "Profile mess"
    date: 2006-03-11
    body: ">> Lines between menu, toolbars and content (should be disabled)\n \n> had you said this a month ago, it would've perhaps been insightful.\n> but i beat you to it with a commit that changes this for 3.5.2 =P\n\nI'm bet that most users won't see your change because everyone already has \"show lines = true\" in its config files and updated RPMs normally don't change the local configuration files of the users.\n\nI think it would make sense if KDE would more often discard outdated local configuration files because manual changes applied to them often make no sense anymore. (That's a general issue. Of course it does not apply for KDE 3.5.1 -> 3.5.2)\n\n>> No \"keep above\" button in the window title bars\n>\n> why should this be on by default?!\n\nWhy not? Put it in your window title bar (on the left side) and you will soon miss it if it is not there. It's very useful when watching TV, having one editor on top of the other windows,... (yes, you can do a right click, but a button is much more convient).\nOnly because MS Windows does not support \"keep above\" does not mean KWIN should hide this useful feature. :-)\n\n> i agree this profile could be even better. but it's another -optional-\n> profile. as such it will be -different-. that's sort of the point.\n> it's a little odd to complain that configuration possibilities result\n> in making things different.\n\nThat's not my point. When using the normal Webbrowsing profile Konqueror switches (?) tho the normal file manager profile when accessing local files or FTP-Servers. konqueror --profile simplebrowser looses the funktionality to switch between FTP/local usage and webbrowsing.\n\nAdditionally you can't open the \"simplebrowser\" profile from within konqueror. Or more exactly: You can, but it looks differently to \"konqueror --prof...\".\n\nWhat's the difference between e.g.:\n\nkfmclient openProfile filemanagement\nand\nkonqueror --profile filemanagement\n\nThe first one toolbar with buttons and address field while the second one shows two toolbars, one with many(!) buttons and one with the address field!?\n"
    author: "max"
  - subject: "Re: Profile mess"
    date: 2006-03-12
    body: "> I'm bet that most users won't see your change because everyone already has\n> \"show lines = true\" in its config files\n\ndefaults shouldn't be written to the file. most will see the change right away.\n\n> Put it in your window title bar (on the left side) and you will soon miss it\n> if it is not there.\n\nand most users won't care. default for everyone, configurability for the rest of us."
    author: "Aaron J. Seigo"
  - subject: "Re: I'm still afraid..."
    date: 2006-03-10
    body: "Kill unnessasary apps e.g.\n\n- Do we need an independend ControlCenter App? Or should configuration accesed via the browwser, so it can be easily modified and enhanced?\n\n- OKular sounds promising. No KGhostview, KPdf, etc. any more. One app for DVI, PS, PDF.\n\n- Kate, Kwrite, Kedit. One has to go.\n\n- a single good recording software, something like Audacity. The current software is bullshit.\n\n- Gstreamer as the default framework\n\n- Get Knights into KDegames "
    author: "gerd"
  - subject: "Re: I'm still afraid..."
    date: 2006-03-10
    body: "Excluding apps won't be necessary anymore. KDE will choose the best application for what you want to do.\n\nAs they write in the article. Finding out what app does what you want to do -- won't be necessary.."
    author: "KDe User"
  - subject: "Re: I'm still afraid..."
    date: 2006-03-10
    body: ">Gstreamer as the default framework\n\nEVERYTHING but this!"
    author: "ac"
  - subject: "Re: I'm still afraid..."
    date: 2006-03-10
    body: "I agree with this, I've NEVER EVER had gstreamer actually work on my system, it craps out constantly and is just a total pain.  Phono will allow the people that want to use gstreamer to use gstreamer, and the people like probably the majority of users that don't want to have gstreamer anywhere near their desktop will be able to use one of the others (like xine)."
    author: "Corbin"
  - subject: "Re: I'm still afraid..."
    date: 2006-03-10
    body: "> - Do we need an independend ControlCenter App?\n\nYes, definitely.\n\n> Or should configuration accesed via the browwser, so it can be easily modified and enhanced?\n\nNo, thank you. KDE based config is far better than browser based. Now, if it's made available as a (secondary) choice, that'd be good. But KDE based config needs to be the default.\n\n> - OKular sounds promising. No KGhostview, KPdf, etc. any more. One app for DVI, PS, PDF.\n\nChoice is good. Keep them all, except those that get badly outdated and unmaintained (3+ years).\n\n> - Kate, Kwrite, Kedit. One has to go.\n\nSame as above: choice is good.\n\n> - a single good recording software, something like Audacity. The current software is bullshit.\n\nYes (but why \"single\"?), and also video editing. There was a nice effort in the form of Kdenlive, but their last version is from two years ago. KDE should be able to do about 10 times better than crappy Kino.\n\n> - Gstreamer as the default framework\n\nWhen I am given a choice between way too bad and far too bad, I routinely refuse to make the choice. In this case this would mean developing a very simple KDEMM backend that would meet only the most basic requirements for audio playback, and making it the default. If the user wants to switch to gst, it will then be their choice, not KDE's.\n\nBesides, is there *really* nothing better than Gstreamer or Arts? We should look really hard, there *must* be something better, I just can't believe things can be that bad in year 2006."
    author: "guau"
  - subject: "Re: I'm still afraid..."
    date: 2006-03-10
    body: "> Choice is good. Keep them all, except those that get badly outdated and unmaintained (3+ years).\n\nYou do realize that when you say \"keep 'em\" you should really say \"port 'em to QT4\"?"
    author: "ac"
  - subject: "Re: I'm still afraid..."
    date: 2006-03-11
    body: "As for KDEnlive, have a look at kdenlive.sf.net. It seems to be being resurrected, and looks quite good at the moment. Transitions, effects, titles, etc. All that is really needed is more stability and rendering..."
    author: "CraigD"
  - subject: "Re: I'm still afraid..."
    date: 2006-04-02
    body: "\"Besides, is there *really* nothing better than Gstreamer or Arts? We should look really hard, there *must* be something better, I just can't believe things can be that bad in year 2006.\"\n\nWhat exactly is wrong with the current gstreamer?"
    author: "tuna"
  - subject: "Re: I'm still afraid..."
    date: 2006-03-10
    body: "My understanding is that kedit is just hanging around because it's able to do bidirectional text and kwrite/kate can't. Once kwrite/kate can it would be able to go the way of the dodo. As for kate and kwrite, they both serve very different userbases."
    author: "Greg"
  - subject: "Re: I'm still afraid..."
    date: 2006-03-10
    body: "> Do we need an independend ControlCenter App?\n\nyes. it's a concept that actually works very well for most people. note that every desktop environment has one? that in itself isn't enough of a justification of course, but it does cause one to paus eand ask, \"why?\" in this case the answer is \"because it works\"\n\n> Or should configuration accesed via the browwser\n\nnot without fundamentally changing the browser. otherwise the access to the settings gets fairly lost (so the browser would have to become -the- place people go to start everything and expose the various start points effectively and transparently, which just doesn't work very well in practice); and the browser concept makes getting to settings harder than even our current annoying control center =)\n\n> oKular sounds promising\n\nwell, oKular -is- kpdf really. and yes, it does sound promising. if it delivers perhaps we won't need other apps. we'll see.\n\n> Kate, Kwrite, Kedit. One has to go\n\nah, my favourite old saw. perhaps if kate actually became a good lightweight code editor we could drop kwrite. personally, i'm impressed with where kate went in 3.5 (away from being a mini-ide and towards \"simply\" being a code editor), so who knows.\n\nkedit is completely different, so lumping it in is pretty silly =)\n\n> a single good recording software, something like Audacity. \n> The current software is bullshit.\n\nagreed. who's going to write it? because that's how open source works. we don't mandate \"someone write a non-bullshit recording package. now!!!!!!\" and *poof* it appears. rather someone, such as yourself, says to themselves, \"wow. all this recording software is bullshit. let's make something non-bullshit.\" and -then- it appears.\n\n> Gstreamer as the default framework\n\nand possibly end up where we are with aRts in 6 years time? no thanks. remember that aRts was the gstreamer of its day. phonon makes so much more sense here: abstract it away and we get portability and future proofing. now system integrators can decide. or better: you can! you like gstreamer? go for it.\n\nto be honest, i've yet to use gstreamer and be satisfied. it does a lot of things right, and hopefully that will continue and one day it will be as awesome as it promises to be. but it's neither stable nor fast in my experience. for playing media, i've had much better experience with xine. \n\nwith phonon, all that hardly matters. ah, much better!\n\n> Get Knights into KDegames\n\nagain, this isn't how it works. the Knights maintainer/developers need to want to be included in KDE and start the process. we don't just suck in people's software like passing alien invaders hovering over farm houses ;)"
    author: "Aaron Seigo"
  - subject: "Re: I'm still afraid..."
    date: 2006-03-11
    body: "My question is, we know Phonon is going to be an abstraction but that doesn't mean some one have to write pluggins for arts, GStreamer, Xine, etc. to be used with Phonon and what is going to be the default plugging? I mean, in witch one KDE developers will spend more concentration? i doubd you can do the same for all so please, tell us witch one?\n\n"
    author: "rmx"
  - subject: "Re: I'm still afraid..."
    date: 2006-03-11
    body: ">what is going to be the default plugging? \n>I mean, in witch one KDE developers will spend more concentration? \n>i doubd you can do the same for all so please, tell us witch one?\n\nI guess the answer should be something like the most stable and best suited one:-) But seriously it's to early to tell, pick the one best suited to your usage and help out with bugreports. \n\nThe choices as I see them are, for the ones looking for performance or low resource usage aKode/Xine. For advanced and network capabilities, NMM. For those wanting to use Gnome multimedia applications GStreamer(everything else Gnome still uses ESD, so additional solutions are needed for that).\n\nSince it looks like the NMM workgroup are planing putting manpower behind getting a Phonon-NMM backend, it starting to look like one of the sweeter alternatives. \nhttp://vir.homelinux.org/blog/index.php?/archives/22-Phonon-NMM-Backend.html"
    author: "Morty"
  - subject: "Re: I'm still afraid..."
    date: 2006-03-12
    body: "at this point is looks like there certainly will be backends for nmm, gstreamer and arts. get more people involved and more backends will emerge"
    author: "Aaron J. Seigo"
  - subject: "Re: I'm still afraid..."
    date: 2006-03-10
    body: "The only time Konqueror (actually KHTML) \"runs\" on the desktop is if you enable the kwebdesktop.\nSame for Windows."
    author: "blacksheep"
  - subject: "Re: I'm still afraid..."
    date: 2006-03-11
    body: "Well if you could tell me how to disable that under windows...?\n\nI just found out, you need to do a lot to enable it under KDE."
    author: "boemer"
  - subject: "Re: I'm still afraid..."
    date: 2006-03-12
    body: "In Windows, it comes disabled by default as well. (I'm talking about the usage of the Internet Explorer engine here.)"
    author: "blacksheep"
  - subject: "Re: I'm still afraid..."
    date: 2006-03-10
    body: "> What would be a nice idee, would be to provide a theme that looks like the\n> GNOME team, disables all configuration options (like GNOME).\n\npersonally i think we're best leaving that trick to GNOME, to be honest. they can pander to that segment of the market and do a good job of it while we pander to the rest of the market who either want or requires flexibility (and do a good job of that)\n\nthe technical and manpower overhead vs actual benefits of such an ability to hide all config options is almost certainly not worth the returns. the effect on documentation, support and bug counts would be non-negligible."
    author: "Aaron Seigo"
  - subject: "Re: I'm still afraid..."
    date: 2006-03-11
    body: "That with GNOME was only a small joke...\n\nSo when there is an other conversation between GNOME and KDE users, we can just say, well just use that theme, and you will be able to do as less as you're used to...\n\nThe first part was the real question... "
    author: "boemer"
  - subject: "Re: finally someconcrete information"
    date: 2006-03-10
    body: "ok, don't let this become a kde gnome thing. This is about what KDE 4 is about. I think the ideas and the goals are there, no we just need the actual implementation. By the way does since plasma has xgl does that mean that they are rewritting X also? "
    author: "tikal26"
  - subject: "Re: finally someconcrete information"
    date: 2006-03-10
    body: "it was a cool article and generally nice. a few inaccuracies crept it, but that's hard to avoid when commenting on a moving target like kde4 at this point in its development.\n\nfor instance, Qt4 does not have support for C# and while we are watching XGL we haven't written any XGL-specific code in plasma yet.\n\ngenerally a good read and, as you noted, no we are not simply going to copy the effects in XGL. right now XGL is an interesting demo but has next to no positive impact for actual users trying to get actual productivity done."
    author: "Aaron Seigo"
  - subject: "Re: finally someconcrete information"
    date: 2006-03-11
    body: "Aaron:\nIt is nice to know that you are not going to copy and that you are looking into making XGL not just about eye candy but about productivity. Keep up the good work."
    author: "tikal26"
  - subject: "Kudos to the KDE PR people"
    date: 2006-03-10
    body: "I just wanted to express my respect for the KDE PR people and everybody who's been actively promoting KDE. As someone who has tried to promote a not-for-profit organistion's work, too, I know how difficult getting the word out can be if you have limited financial capacity. Nevertheless, it seems that KDE has been able to drum up a regular hype about KDE 4, and that's quite impressive. Now KDE 4 just gotta deliver.\n\n"
    author: "PC Squad"
  - subject: "Re: Kudos to the KDE PR people"
    date: 2006-03-10
    body: "I don\u00b4t think there have been any drumming at all yet.\nIt\u00b4s all about user expectations."
    author: "reihal"
  - subject: "Odd magazine"
    date: 2006-03-10
    body: "I agree with others that there's something not quite right about this magazine. I get the feeling that this sort of thing doesn't really help open source's image."
    author: "Ben Morris"
  - subject: "Re: Odd magazine"
    date: 2006-03-10
    body: "Yeah. This thing doesn't look professional at all (what about reusing the Lenin image for the gpl article and the final advertising?) Also, they lure people with the KDE 4 article, and it turns out to be almost the LAST one, when every decent magazine would place the cover article at the beginning (as much as possible). And instead, what is the first article about? GPL3, and it's basically an attack on the entire FS movement, with BIG remarks about the gpl being \"viral\". \n\nAlso, in the first editorial page, the author defends the sort fo RIAA and MPAA in their outrageous behaviour of suing everybody in sight.\n\nThis magazine is, at best, camouflaged FUD. I would strongly recommend to remove this from the Dot."
    author: "Giacomo"
  - subject: "Re: Odd magazine"
    date: 2006-03-10
    body: "The KDE4 article was pretty good though (least IMHO). That's the only thing revelant to the dot, right?"
    author: "thothonegan"
  - subject: "Re: Odd magazine"
    date: 2006-03-10
    body: "The GPL *is* viral. Maybe you don't like the disease connotations, but the fact is that the GPL was designed to \"infect\" any software it touches. As a user it doesn't matter, but as a developer you must actively avoid GPL source code or risk all of your own original software being affected.\n\nI myself have seen 10 to 20 line snippets of GPLd code in a proprietary kernel for an embedded project I work on. Some naive developer saw a routine they liked online, assumed that \"free\" meant \"unencumbered\", and used it. Yet that tiny code snippet is enough to force the entire kernel under the GPL. To you the GPL developer, \"viral\" may sound like a pejorative. But to the embedded systems companies being threatened and sued for GPL violations, it is a very fitting word.\n\np.s. An attack on the GPLv3 proposal is NOT an attack on \"the entire FS movement\"."
    author: "Brandybuck"
  - subject: "Re: Odd magazine"
    date: 2006-03-10
    body: "A common misconception. It's copyright law that's viral."
    author: "camperman"
  - subject: "Re: Odd magazine"
    date: 2006-03-11
    body: "That has nothing to do with the GPL, but the incompetence of the programmer (or more likely, they were lazy and thought they could 'cheat').  You don't blame an author of the original text when someone else plagiarizes their work, do you?\n\nViolating the GPL is no different than if you had violated any other copyright."
    author: "Corbin"
  - subject: "Re: Odd magazine"
    date: 2006-03-11
    body: "They can't force the entire kernel under the GPL.  It's a copyright violation.  At most, they would be made to stop shipping until the offending code was taken out."
    author: "Rick"
  - subject: "Re: Odd magazine"
    date: 2006-03-10
    body: "Are you an idiot? Can't you understand when someone is being sarcastic without appending a smiley to the text?"
    author: "blacksheep"
  - subject: "New link to the article"
    date: 2006-03-11
    body: "Can't read the article. Does anyone where it went?"
    author: "petteri"
  - subject: "Re: New link to the article"
    date: 2006-03-11
    body: "The article is still available on thier site, though in a different place.  Maybe they prefer you to register before downloading thier publication?  It is free subscribe to thier electronic publication, however!\n\nhttp://tectonic.magnumip.co.za/index_pdf.php"
    author: "uncle_steve"
  - subject: "My wish: the 'social' desktop"
    date: 2006-03-14
    body: "My wish for a next-gen desktop: what I like to think of as the 'social' desktop\n\nNot sure if 'social' covers the bill, but what it comes down to is: using available metadata , combined with user-provided metadata to make it easier for people to get into contact.\n\nLet us say I have a bunch of pictures in a folder /home/myself/pictures/ ; from the photo metadata (e.g. the keyword 'go-carting' appears 100x in a folder of 900 photographs) the system can deduce a 'hobby' (perhaps checking with a database list of possible hobbies) of mine is probably to go-cart, and fill it in automatically in my profile (or at my confirmation, if I enabled the service). If I message 'Tom' a lot, it might add that name to my list of friends in my profile.\n\nGoing further, it could allow me to fill in things myself, complete them or correct them, and attributing certain values to items which then can be used for comparison. E.g. similar to the plugin for amaroK, I could rate songs, make my playlist with ratings available over the network (to everyone, to select persons, or not at all) and then people with similar taste could see which other songs I like.\n\nWhat is very important in this is, what I like to call 'chains of trust'. You trust your husband/wife 100% (or perhaps 99%), your best friend 95% and your neighbour 80%. Which makes it so that the rating of the friend of your friend also increases on the 'trust' bar. A whole host of categories could be created this way, but trust is certainly one of the most important ones. The next person in the chain (friend of friend of friend) gets an even lower boost on the 'trust chain'. There are numerous applications for this: everyone likes to know if someone is likely to be trusted. You could use it to determine if a web source for getting packages from, is safe, or at least get a warning if a lot of people don't trust it.\n\nThe possibilities seem endless: looking for a job, dating, looking for members to make a music band (e.g. 'I need a singer in the town of X', at which time the application may contact people in that town looking for a position as a singer, at least if he has enabled the ability of being contacted).\n\nOf utmost importance here is openness and security: all your personal stuff is going to be centralized here! So a strong form of encryption and sane security polices are paramount.\n\nOne problem I see with this is: anonimity is very important, but you cannot really 'check' if the anonimous person is who s/he claims to be. And the consequences for identity theft could be very grave.\n\nStill, I think this would hold awesome potential, finally unlocking the power of connected 'communities' and the ability to have the information I need (seller X has delivery at 40%, service at 20% and price at 80%, seller Y delivery 80%; service 90% and price 60%), right on my desktop.\n\nI know it's a confusing concept, but I hope I got my point across.\n\n"
    author: "Anonymous"
  - subject: "Re: My wish: the 'social' desktop"
    date: 2006-03-17
    body: "You might want to look at the DBFS project. Its a database file system project.\nhttp://ozy.student.utwente.nl/projects/dbfs/"
    author: "Jisaku Jien"
  - subject: "Re: My wish: the 'social' desktop"
    date: 2006-03-21
    body: "Exactly, but now add to this functionality like:\n\nIM-like contacting (if allowed)\nP2P-like search on people's 'desktops' (the files they want to make public)\nEncryption, possibly with public and private keys\n...\n\nin a nicely integrated package/system and then we're getting somewhere :)"
    author: "Anonymous"
  - subject: "impressive"
    date: 2006-03-23
    body: "The more I read about KDE 4, the more impressed I am, and the more eagerly I anticipate its arrival! KDE has always seemed to integrate itself into my workflow (instead of the other way around), and after reading about Plasma, I am truly excited. It's time for a paradigm shift, and KDE 4 sounds like just what we need."
    author: "Erich"
---
South African Free Software magazine <a href="http://www.tectonic.co.za">Tectonic</a> published a <a href="http://www.tectonic.co.za/TectonicJanFeb2006.pdf">preview of KDE 4</a>. Written by AJ Venter of South African distribution <a href="http://www.getopenlab.com">OpenLab</a> the article describes how the KDE developers are going to "<em>rewrite the desktop rules with KDE 4</em>" with technologies such as <a href="http://plasma.kde.org">Plasma</a>, <a href="http://solid.kde.org">Solid</a> and of course <a href="http://www.trolltech.com">Qt 4</a>.  The article concludes that "<em>when
KDE4 comes out, it will spell the end of the traditional way of using a computer</em>".




<!--break-->
