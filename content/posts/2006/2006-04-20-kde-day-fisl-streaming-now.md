---
title: "KDE Day at FISL Streaming Now"
date:    2006-04-20
authors:
  - "jriddell"
slug:    kde-day-fisl-streaming-now
comments:
  - subject: "..."
    date: 2006-04-19
    body: "/me cries out loud because he couldn't attend to the event..."
    author: "Taupter (Cl\u00e1udio Pinheiro - Kopete)"
  - subject: "Re: ..."
    date: 2006-04-20
    body: "i miss that :( dude im furius!\ncan some one please point me to a link were to find a recording of that interview? "
    author: "pinheiro"
  - subject: "Link Is Slow"
    date: 2006-04-20
    body: "The LInk is slow right now, so i will try later, that is the only thing, we need to get people to record them and put the videos on *good* servers"
    author: "Luke Parry"
  - subject: "Link Dead?"
    date: 2006-04-21
    body: "Someone got the video on another Server? Seems the link is dead and i'd really like to see it.\n\nDavid"
    author: "David"
---
The <a href="http://fisl.softwarelivre.org/7.0/www/?q=en">International Free Software Forum</a> is currently being held in Porto Alegre, Brazil.  <a href="http://www.infomediatv.com.br">InfomediaTV</a> are now <a href="http://infomediatv.terra.com.br/fisl/">streaming KDE day</a> hosted by Helio Castro and starring many of KDE's best developers.  The streaming uses a Java applet or open the <a href="http://infomediastreaming.terra.com.br:8000/v4l.ogg">Ogg Theora stream</a> directly.




<!--break-->
