---
title: "KDE on US TV's \"Heroes\""
date:    2006-09-29
authors:
  - "jriddell"
slug:    kde-us-tvs-heroes
comments:
  - subject: "kopete a star?"
    date: 2006-09-29
    body: "funny those shots are after a user creates an account, but kopete is annoyingly complex... there's no  quick logon, passwords. to start using kopete one has to struggle with \"account creation!\"\n\nYeah, it is a multiprotocol and do all the fancy stuff but it is no easy!\n\nsimply put, it should give 3 options:\n\n1. username/account\n   [ ] create account / remember user\n2. password\n   [ ] save password for future use\n3. service/protocol (as drop down list, or a sidebar list)\n   [ ] Add account to kopete account database...\n\nthe above is what i call simplicity and efficiency. But complex developers can't create simple interfaces, right? ;) (no offence)\n\nobserve the check boxes, they really help to quickly use and create an account.\n\ntill kopete get's a simpler and user-friendly interface like konversation, at least before creating an account, i'd assume konversation as the super-hero :)"
    author: "fast_rizwaan"
  - subject: "Re: kopete a star?"
    date: 2006-09-29
    body: "I don't know, I find it remarkably easy compared to any other client out there.  gaim, miranda im, all of them - they just seem overly complicated.  Usually because they tried to oversimplify.\n\nAnd kudos to Eike Hein for catchin that one :)"
    author: "Joseph M. Gaffney aka CuCullin"
  - subject: "Re: kopete a star?"
    date: 2006-09-29
    body: "I also find it really easy to use. And I tend to skip applications that I feel is just a bit hard to configure. Life is to short for configuring.\nThanks kopete team!!"
    author: "Jarl Gjessing"
  - subject: "Re: kopete a star?"
    date: 2006-09-29
    body: "Funny, I just try the account creation dialog (in 0.12.2), and it does exactly that. It's a two steps dialog, first the protocol, second the user name, and then depending on the protocol, there are extra options (in other tabs) that you can safely ignore (if you want) without preventing the use of your newly created account."
    author: "Cyrille Berger"
  - subject: "Re: kopete a star?"
    date: 2006-09-29
    body: "I couldn't agree more.\nI've already discussed this in another thread here some time ago\nhttp://dot.kde.org/1150660194/1150660976/1150670453/\nbut with little success I must admit. It seems hackers can't easily see what difficulty really is, when perceived from the point of view of \"normal\" users.\nPerhaps is in those cases where the so called \"usability experts\" should appear in the scene?\n\nAnyway, at this time I found it really hard to explain to someone that Kopete should include a mode in which it did a temporary connection, without keeping any data (contact list, history, etc.) from that connection after the user logged out.\n\nToday Kopete is only useful living inside the primary machine of a user, out of that case it is completely useless. i.e. forget about deploying it in a big office or a cybercafe.\n\nLet's hope someday they could be convinced about the problem.\n\nGabriel\n"
    author: "Gabriel Gazz\u00e1n"
  - subject: "Re: kopete a star?"
    date: 2006-09-30
    body: "Well I think you should re-read that thread.\nAbout your personal computer, create a Guest account for your friends, I have many friends that do this even on windows. I never let anyone use my own account for privacy and security reasons.\nIf you're in an institution, use kiosk. \n\nSo what's the problem?\n\nPlus  with vista and its search function that reveals all your private emails/conversation etc... I think everyone will create a guest account in the future. So just be ahead of other and create a guest account :) (it takes 5 seconds on konsole and a minute with kusers)."
    author: "Patcito"
  - subject: "Re: kopete a star?"
    date: 2006-09-30
    body: "> So what's the problem?\n\nThis kind of answer is THE problem.\nSee, I'm not a newbie. I know exactly what I want, and why I do.\n\nI think many developers (certainly not all of them, but a lot actually) should try and put themselves in the shoes of \"real\" users more frequently and seriously than they currently do.\nAfter all what's all that buzz about \"usability experts\"? and why are they so needed these days in the OSS world?\nOne could attempt to theorize that's because there must be some kind of \"problem\" with the natural ability of many developers in reaching the actual needs of the users.\nSo perhaps listening more carefuly, could be a good start.\n\nTo the point of that answers I received a while back.\nVery often, a perfectly logical and technically correct solution is not the best one for users.\nI'm not saying the answers given to me were not correct ones or that they didn't tend to solve in one way or another the problem described.\nBut what I really had a hard time transmiting then (and now) is that this solutions were not the best nor the expected ones for an overwhelming lot of users around the globe. And I just can't be anything short of amazed at the inhability some people has to even have a grasp of it.\n\nHaving said this, please undestand that I really respect, care and applaud the hard work all coders put into OSS and I'm just trying to point out what I see is a serious problem that's currently affecting the ability of OSS --in this case Kopete and KDE-- to reach the masses.\n\n(Sorry for the long post)\n\nGabriel\n"
    author: "Gabriel Gazz\u00e1n"
  - subject: "Re: kopete a star?"
    date: 2006-09-30
    body: "I have noticed that Usability Experts, as they are called, will do a much better job of identifying a UI's weak points and developing a plan for improved user interaction.  This is because they are experts in the field. Programmers aren't usability experts (in most cases, atleast), because of this, usability experts provide an important piece of the OSS world and KDE in particular.\n\nHaving developers put themselves in the shoes of \"real\" users more frequently and seriously than they currently do is a good thing, but usability experts are still needed for their knowledge and experience in the field.\n\nAlso, I think you would agree Gabriel, that usability experts are perfect for developing new/improved HIG's (like we are currently doing).  Once most KDE developers follow the new KDE HIG, then they will already be putting \"themselves in the shoes of \"real\" users\".  Notice how most Gnome apps have the same look-n-feel? That is the function of the HIG.\n\nHope that cleared a few points. Nonetheless, I think that you raise a number of important points.\n\nThank you to all the people helping & working towards an *even* better KDE!  I am looking forward to the future of what is currently my favorite (and I think it is the best) environment to work in.\n\nvlad"
    author: "Vlad"
  - subject: "Re: kopete a star?"
    date: 2006-09-30
    body: "Yeah, I know what you're talking about, and I agree.\nWhen I talk about usability experts I usually put the name in quotation marks just because I see them as \"real users with a degree\". :)\n\nI don't know exactly what makes a usability expert an expert, and if my history using and teaching how to use computers since the 80s is of any receipt, I must not be that far from being one of them. But, hey, it's really hard to be heard without the title hanging on my wall!  ;)\n\nCheers.\nGabriel\n"
    author: "Gabriel Gazz\u00e1n"
  - subject: "Re: kopete a star?"
    date: 2006-10-01
    body: "HCI is a field of computer science like many others. Very interesting one I might add.\n\nIt's possible to do a good job based only on experience, but if you had a technical background you'd be much better at it. At least that's usually the case.\n"
    author: "Reply"
  - subject: "Re: kopete a star?"
    date: 2006-09-30
    body: "No problem for your very long post. But you're not replying to any of my points, you're just talking about dev's inability to listen some time.\n\nThe point is that Kopete is really trying to integrate with KDE, that means linking with kaddressbook, kcalandar and kmail with kabc and stuff. I think that this is really a good thing, people keep criticizing FOSS desktop about its lack of integration and once we get there we get told that integration is not good. Just because windows doesn't do it this way doesn't mean it's the way to go.\nPlus as I said in my previous post,  mac and vista desktop are getting there too, a desktop where every app can communicate with one another with search functionalities too. That is really cool but it means one thing, when all your private data are easily accessible from your account you don't want people to use it and that's what's happenning with Kopete but wait for KDE4 and its search function and it'll get worse. People will be able to type your girlfriend/boyfriend name in a box and read all your messenger conversations, see picture she/he sent you etc... :)\n\nThat's where we heading, so if you think kopete's way to link with your addressbook sucks then just wait for what's coming next cause it'll be all over the place on mac/vista/kde4/gnome etc...\n\nSo the solution indeed is to create a guest account, maybe future distro will come with a default guest account that delete its .kde at every log out. Other than that I don't see other solutions for private use (cause of course you have kiosk in institutions).\n\n"
    author: "Patcito"
  - subject: "Re: kopete a star?"
    date: 2006-10-02
    body: "jaja... I'm afraid I've set a standard. Your's has an equal extension than mine! ;)\n\nWith respect to application integration, I'm definitely in favor of it, even in Kopete.\nBut what I'm asking for is an even more basic (and non opposite, by the way) need than integration itself.\nI see the kind of integration we're talking about here much as a \"luxury\", whilst being able to easily (can I repeat it: easily) and securely do mobile instant messaging is something I realise is essential to most people today and Kopete is lacking in that area completely.\n\nToday Kopete is very competitive in the segment of integrated multiprotocol IMs, but it's totally out of the game with respect to mobile IM. Period.\nTo be able to attract users in need of mobile IM comunication, Kopete should include a special mode in which it allowed a user to initiate a IM sesion without having to create a user or anything like that and, equally important, one in which the program didn't save any data from the IM user after he has logged out!\n\nI don't know if this feature is too difficult to implement, but the fact is that it isn't there today, and users needing to use mobile IM apps now will go to another application, not Kopete.\nBesides that, I can't code a single line, if not I'd probably start doing something about it myself. Sadly all I can do is just to expose the fact.\n\nI hope someone's listening.  :-/\n\n\n"
    author: "Gabriel Gazz\u00e1n"
  - subject: "Re: kopete a star?"
    date: 2006-10-02
    body: "I think you are right.  I don't see why you have problems convincing anyone about this.  Back in the day, I used to run AOL AIM straight from AOL's webpage.  I think it was a java applet.  You just entered your username and password.  When you were done, the java applet went away.  The end.\n\nMaybe hackers don't understand because they don't have real life 3 dimensional friends with whom to share their computers.  Don't get mad, I'm just kidding!  I'm a developer myself.  But seriously, I could easily see a \"Guest Login\" option somewhere in the file menu.\n\n"
    author: "Henry"
  - subject: "Re: kopete a star?"
    date: 2008-09-02
    body: "I can't believe nobody mentioned this, but insulting developers is an awful way to get them to do what you want and what they don't want to do."
    author: "Rob"
  - subject: "Re: kopete a star?"
    date: 2006-10-02
    body: "Well, perhaps a good way to make somebody do something about it could be to support the entry in Kopete's bugzilla in which this functionality is suggested with as many votes as possible:\nhttp://bugs.kde.org/show_bug.cgi?id=129829\n\nthx!"
    author: "Gabriel Gazz\u00e1n"
  - subject: "Re: kopete a star?"
    date: 2006-10-04
    body: "It depends on which msn client your're most familiar with. I've used kopete since the very first time I made my msn passport, and it's much more user-friendly to me than msn messenger."
    author: "M\u00e1rcio"
  - subject: "gratulation"
    date: 2006-09-29
    body: "world domination proceeding as planned ;-)"
    author: "cobalt"
  - subject: "Re: gratulation"
    date: 2006-09-29
    body: "What did you expect? We are unlike some other losers, who spend all their hard-earned money bribing everyone coming around the corner and are still not there. In fact, they are struggeling to even stay afloat and have to resort to more useless methods like lying and suing. Maybe I feel pitty for them. Maybe."
    author: "AJ"
  - subject: "Re: gratulation"
    date: 2006-09-29
    body: "wengophone 2.0 is a bit better than kopete and both are unstable and silly implementations..  one thing is to kno..  kubuntu is brokenware, suse bloatware and kubuntu is also premockupware"
    author: "kubuntu/kopete/"
  - subject: "Re: gratulation"
    date: 2006-09-29
    body: "And you are idiotware."
    author: "Patrick Starr"
  - subject: "Geeks"
    date: 2006-09-29
    body: "The scene in question has a real good looking girl getting undressed in the background, and you people notice the computer UI!\n\nhttp://www.eikehein.com.nyud.net:8090/kde/heroes/k de-on-heroes3-hd.png\nhttp://www.eikehein.com.nyud.net:8090/kde/heroes/k de-on-heroes6-hd.png"
    author: "Charles Hill"
  - subject: "Re: Geeks"
    date: 2006-09-29
    body: "Every television show that premiered this year had a \"real good looking girl getting undressed\" in it at some point or other.  Heck, NCIS has one doing so every week in the opening titles, and I'll bet it's not alone.  This one had KDE."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Geeks"
    date: 2006-09-29
    body: "That's what I was thinking!\nAlthough, I did notice the old K-Menu icon before the girl.\nSomeone send them Kubuntu!"
    author: "Pootie Tang"
  - subject: "Re: Geeks"
    date: 2006-09-30
    body: "Psssst!\n\nKeep this on the DL OK?\n\nKDE is themable.  You don't have to use the default theme (Kubuntu, Mandriva or otherwise) if you don't want.  In fact, you can use a Mandriva Theme on Kubuntu if you so desire.\n\nNow remember.  Keep it on the down lowl."
    author: "Scott"
  - subject: "Also on Criminal Minds!"
    date: 2006-09-29
    body: "KDE was also on Criminal Minds this week!\nThey did not show much of the screen, mostly just a terminal session.\nBut in the terminal session they showed a path which included the \".kde\" directory."
    author: "Zot"
  - subject: "Finding Nemo"
    date: 2006-09-30
    body: "In the making of Finding Nemo (on the dvd of the film), you can see screenshots of KDE, while artists are working on developing the fish etc.."
    author: "AC"
  - subject: "old old old"
    date: 2006-09-30
    body: "looks like kde 2.x\nold old old\n\nThey probably needed something that didn't look like mac or windows and looked cool and geeky.\n\nAnyway, I really enjoy using KDE 3.x.. much better than 2.x, doesn't have the geeks-only feel to it, and is best environment to work in.... IN THE WORLD\n\nmwahaha\n\nlove kde!\n\nVlad"
    author: "Vlad"
  - subject: "Re: old old old"
    date: 2006-09-30
    body: "It is Mandriva. Probably 2005LE which came with HP laptops, judging from the Galaxy theme.\nIf that's the case, it should be KDE 3.2.3\n"
    author: "Gabriel Gazz\u00e1n"
---
Konversation developer Eike Hein noticed KDE <a href="http://www.eikehein.com/kde/heroes/">appearing on new US drama Heroes</a>.  This follows previous outings on <a href="http://dot.kde.org/1067616574/">24</a>, <a href="http://groups.google.com/group/comp.windows.x.kde/msg/3100d0f3062fac2a?hl=en&lr=&amp;ie=UTF-8">Alias</a> and the making of <a href="http://dot.kde.org/1070079794/">Lord of the Rings</a>.  Kopete is the star of the episode, but you don't have to be a hero to make use of KDE's multi-protocol instant messenger.

<!--break-->
