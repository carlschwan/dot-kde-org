---
title: "KDE Look and Feel for Java Preview"
date:    2006-04-13
authors:
  - "numanee"
slug:    kde-look-and-feel-java-preview
comments:
  - subject: "Dreams come true  :)"
    date: 2006-04-13
    body: "yes! java applications like limewire, azureus, Vocabulary Builder 1.33,(http://www.geocities.com/rorokimdim/) would blend into KDE.\n\nA real deserving thing for KDE. thanks Sekou Diakite! the screen shots look good."
    author: "fast_rizwaan"
  - subject: "Re: Dreams come true  :)"
    date: 2006-04-13
    body: "It does look good, and seems a clever hack. \n\nAzureus uses the SWT GUI (which most commonly uses GTK on *nix for native widgets---yuck) rather than the native Java Swing GUI, so I don't think this project is going to help there. Maybe the other two apps though.\n\n"
    author: "Tim Middleton"
  - subject: "Re: Dreams come true  :)"
    date: 2006-04-13
    body: "If the app is written in java-gtk2 then it will use the gtk2-qt plugin in kde 3.5.  At least Sunrise Desktop[1] does.\n\n\n[1] http://www.sf.net/projects/sunrisexp"
    author: "Ed"
  - subject: "Re: Dreams come true  :)"
    date: 2006-04-18
    body: "Is it really a hack or just the way it has to be done ?"
    author: "Felix"
  - subject: "Limewire system tray icon"
    date: 2006-04-13
    body: "Does anyone know if it's possible to make Limewire show a system tray icon in KDE? It definitely works in Windows, and Limewire claims to support it on Linux (GNOME maybe?)\n\nHas anyone had any success with it?\n"
    author: "Dima"
  - subject: "Re: Limewire system tray icon"
    date: 2006-04-13
    body: "limewire runs nicely in my kde systray.\nI did not do anything to get it there, Limewire places an icon in kde's systemtray by default."
    author: "AC"
  - subject: "Re: Limewire system tray icon"
    date: 2006-04-13
    body: "Limewire doesn't show a systemtray icon on my desktop (which was annoying because the default behavior when you try and close it was to go to the system tray, so I would have to kill the process then).  Last tested on KDE 3.5.1 and Limewire 4.8.1."
    author: "Corbin"
  - subject: "Re: Limewire system tray icon"
    date: 2006-04-14
    body: "I installed limewire-pro 4.11 on my suse 10.0 system and noticed that there's a kde system tray icon for it when it's running. I assumed that's just how it works..."
    author: "Joel"
  - subject: "efficiency"
    date: 2006-04-13
    body: "> A KDE application is created offscreen and never showed.\n\nIsn't that kind of a waste of memory? Also, copying pixmaps from Qt to Java is probably slow, too...\n"
    author: "Dima"
  - subject: "Re: efficiency"
    date: 2006-04-13
    body: "Well, obviously the Gtk-Qt engine does it too, and that one's not performing too bad after all. Seems like it's actually manageable."
    author: "Jakob Petsovits"
  - subject: "Classpath"
    date: 2006-04-13
    body: "gnu Classpath makes really good progress. Why not include KDE bindings?"
    author: "Jaffa"
  - subject: "Doesn't work?"
    date: 2006-04-13
    body: "Anyone else got this error?\n\n[franz@his2274 dl]$ java -jar KdeLAF.jar\nInvalid or corrupt jarfile KdeLAF.jar\n\nObviously the jar file is'nt really a jar file...\n\nLet's see:\n[franz@his2274 dl]$ tar xzvf KdeLAF.jar\nkdelafworker\n[franz@his2274 dl]$ \n\nUps ;-)"
    author: "anoninuss"
  - subject: "Still missing a RAD tool..."
    date: 2006-04-13
    body: "Okay looks nice, but I'm still missing a good Rapid Application Development Tool, like (Visual Studio .NET or Delphi). \n\nMonoDevelop looks okay, still a little too gnomisch, KDevelop3 looks okay, but still feels like Visual Studio 6.0 for C++.... Probably didn't try it enough.\n\nIt is just the way someone used to programing under windows looks at the tools under Linux... You can say a lot about how bed windows is, I'll probably give you right in a lot of things, else I wouldn't be looking to linux.\n\nBut I'm still having a little problems starting programming under Linux, or under KDE for that part. Installed Qt4 and doing some basic programming with KATE. But I'm still looking for something that invites me to programm..... (Under Linux ofcourse)...."
    author: "boemer"
  - subject: "Re: Still missing a RAD tool..."
    date: 2006-04-13
    body: "You mean you want to develop for KDE or in Java? For Java the tools are excellent,  try NetBeans or Eclipse. KDE tools are also suberb, I liked KDevelop (though I never used it for GUI apps) and I believe it now has form designer integration. Admitedly it's not quite Visual Studio 2005 by it's good none the less."
    author: "David"
  - subject: "Re: Still missing a RAD tool..."
    date: 2006-04-13
    body: "You probably hate java, but Eclipse and IntelliJ are the best IDEs out there. If you are addicted to IDEs, have a look at these. "
    author: "rjw"
  - subject: "Re: Still missing a RAD tool..."
    date: 2006-09-02
    body: "I personally like jEdit too ..."
    author: "Cody Ray"
  - subject: "Source code?"
    date: 2006-04-14
    body: "For a free and open source project, this doesn't seem very free or open source. I need to compile from scratch for the AMD64 architecture..."
    author: "Alistair John Strachan"
  - subject: "Re: Source code?"
    date: 2006-04-14
    body: "Hello Alistair,\nI think that's this is a misunderstanding,\nI really don't understand why you think this is not a free software.\nFrom the begining, the licence is GPL, the subversion repository is open to anonymous access, and since yesterday, the source code .tgz is directly on the web site.\n\nBy the way, I do not own an X86-64 System, if your willing to compile/test the native part of the project to X86-64 it would be great.\n\nS\u00e9kou.\n"
    author: "S\u00e9kou DIAKIT\u00c9"
  - subject: "Re: Source code?"
    date: 2006-04-14
    body: "It was because there is no mention of SVN, or source access on the linked webpage. I now see a 'contribute' link, which is OK of course. Many thanks!\n\nI will feed back to you if I have any problems."
    author: "Alistair John Strachan"
  - subject: "Same app in SWT, Swing, and Qt"
    date: 2006-04-15
    body: "A while back I wrote a simple mp3/ogg/wav player in java. I wrote different frontends to it to get to know the various toolkits better. It has an SWT frontend for my native looking windows fun, a Swing frontend for cross platformness, and a Qt/KDE frontend for fitting in to my KDE desktop.\n\nUsing the Qt frontend, the Gtk-Qt engine for GTK and the KdeLAF, I can make all three frontends use the same Qt style, with varying degrees of success.\n\nThis pic is the results, with some commentary. KdeLAF has a little ways to go before I would replace the Qt frontend with it.\n\nhttp://img130.imageshack.us/my.php?image=toolkitthemecomparisons8tm.png\n\n******************\nedit: the Swing version looks just fine when using metal or ocean or some other more complete LAF, if metal can be called 'fine' ;) Icons work and all. \n\n(this was a repost of a comment I made on the matter on osnews.com)"
    author: "MamiyaOtaru"
  - subject: "Re: Same app in SWT, Swing, and Qt"
    date: 2006-04-16
    body: "Very cool!  Thanks for posting your comparison.  Attaching your graphic without the ad. =)\n"
    author: "ac"
  - subject: "Re: Same app in SWT, Swing, and Qt"
    date: 2006-04-20
    body: "Thanks for making it a link :)  I totally missed the option to do that.\n\nFrom kdeLAF's homepage: \n2006/04/17 : Adding slider widget.\n\nOne of the issues taken care of then!  I'll have to try out the new build.  Still waiting on the splitter and frames afaict."
    author: "MamiyaOtaru"
  - subject: "looks great"
    date: 2006-04-15
    body: "Hi,\nIts great, but a developer don't care what tool he is using, it is the convenience that matter. Lesser time to market the better, development of good looking screens Good, but what about Enterprise Apps Development, Already people have rejected the idea of using multiple IDE for same project. Just remember java is a chosen platform for EAD.\n\n\nBest Regards\nhttp://www.vivarem.com\n\n"
    author: "constantine"
  - subject: "L&F vs. functionality"
    date: 2006-04-17
    body: "I've always felt that some people don't understand (not the developers of KDELandF, in particular... just developers in general) that making a GUI *look* similar isn't what the goal should be.  IMHO, the overlooked, and yet keystone, component is complex datatype support in the clipboard.\n\nNo interface, in my opinion, has yet to match NeXTSTEP in terms of sophisticated clipboard support.  You could drag any data, from any application, to any other.  KDE is getting pretty close to NeXTSTEP's functionality, but I've never had any luck with non-trivial (plain text) copying of data dragging between KDE and Java apps -- say, Konqueror and GalleryRemote -- and this is why I almost never use Java GUI apps.  Well, that and the VM being a memory hog.\n\n--- SER"
    author: "Sean"
  - subject: "KdeLaf and NetBeans"
    date: 2006-04-21
    body: "The KdeLaf calls JToolbar.addImpl method sooner than other look and feels and as a result yesterday's build from http://www.netbeans.org used to fail with\n\njava.lang.NullPointerException\n        at org.openide.awt.Toolbar.addImpl(Toolbar.java:616)\n        at java.awt.Container.add(Container.java:390)\n        at org.freeasinspeech.kdelaf.ui.QToolBarUI.installComponents()\n        at javax.swing.plaf.basic.BasicToolBarUI.installUI(:128)\n        at javax.swing.JComponent.setUI(JComponent.java:652)\n        at javax.swing.JToolBar.setUI(JToolBar.java:159)\n        at org.openide.awt.Toolbar.setUI(Toolbar.java:1178)\n        at javax.swing.JToolBar.updateUI(JToolBar.java:170)\n        at javax.swing.JToolBar.<init>(JToolBar.java:136)\n        at javax.swing.JToolBar.<init>(JToolBar.java:97)\n        at javax.swing.JToolBar.<init>(JToolBar.java:85)\n        at org.openide.awt.Toolbar.<init>(Toolbar.java:157)\n\nI have fixed that\nhttp://openide.netbeans.org/source/browse/openide/loaders/src/org/openide/awt/Toolbar.java?r1=1.39&r2=1.40\n\nSo if you want to try the KdeLaf with NetBeans get the latest sources or download tomorrow's daily build.\n"
    author: "Jarolsav Tulach"
---
Sekou Diakite has released an alpha version of a <a href="http://kdelaf.freeasinspeech.org/">KDE Look and Feel for Java</a>.  This is an interesting step forward in Linux/Unix desktop integration since Java applications can now use the KDE/Qt libraries for drawing Java widgets and even directly use existing KDE widgets such as the file or color choosers.  See the webpage for further details of this accomplishment including future plans and, of course, screenshots.


<!--break-->
