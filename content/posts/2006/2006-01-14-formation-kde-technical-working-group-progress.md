---
title: "Formation of the KDE Technical Working Group in Progress"
date:    2006-01-14
authors:
  - "sk\u00fcgler"
slug:    formation-kde-technical-working-group-progress
comments:
  - subject: "Is this about bus number?"
    date: 2006-01-13
    body: "This seems to be about reducing the \"bus number\" for releases, isn't it? I mean, there are people already doing all of the things that the Technical Group is about -- Coolo does releases, whoever does the work decides, and dependencies just kind of happen. The only benefit I can see here is that we KDE won't depend on Coolo's vacation time for releases."
    author: "[ac]"
  - subject: "Re: Is this about bus number?"
    date: 2006-01-14
    body: "You know, it didn't occur to you, that Coolo may be the one wanting this to happen?"
    author: "Debian User"
  - subject: "Re: Is this about bus number?"
    date: 2006-01-15
    body: "The \"bus number\" plays a role. What's more important is to take away a bit of the pressure from the shoulders of whoever is the release manager and spread that more evenly. Another point is adding a bit more legitimacy to the role of release manager,there is currently not a really good process to elect the release manager. Being a release manager is a lot of work as it is already, ending up in disputes about release decisions doesn't help to motivate. Hopefully the TWG will prevent coolo from jumping in front of the bus in the first place ;-) \n"
    author: "Waldo Bastian"
  - subject: "Companies and Technical Working Group"
    date: 2006-01-14
    body: "I think the Technical Working Group will be a great addition to the KDE decision making progress.\n\nNevertheless, I would have liked to have a provision in the charter similar to what Gnome has. No more than 2 members of the TWG can be working for the same company. The more KDE commercial success grows, the more companies working with KDE will want to control it.\n\nIt is great that Trolltech hires all these KDE hackers. The more professional programmers we have, the more professional and polished our desktop will become. The TWG will rightfully be staffed by these full-time KDE hackers that are the only ones who have a deep enough knowledge of the project to be able to make sensible and complex technical decisions. But these hackers have a double fealty for KDE and for their employer and we must protect ourselves of any hijacking of the project.\n\nAnother point that is not clear in the charter is the position of the release manager. Can the release manager(s) be (a) member(s) of the TWG ? What is the situation if a conflict arise between the release manager and the TWG. Release managers have today a lot of power in KDE and their power is generally well accepted. The charter strips most of the power of the release manager. I guess that ex-release managers are happy with that and did not like making decision based on a mixed and unconclusive e-mail threads but it worked quite well.\n\nCheers,\nCharles"
    author: "Charles de Miramon"
  - subject: "Re: Companies and Technical Working Group"
    date: 2006-01-14
    body: "You said...\nNo more than 2 members of the TWG can be working for the same company. The more KDE commercial success grows, the more companies working with KDE will want to control it.\n\nMaybe I don't get it, but if some company seeks to control KDE by hiring good developers to hack on KDE color me unconcerned."
    author: "yawn"
  - subject: "Re: Companies and Technical Working Group"
    date: 2006-01-14
    body: "na, it wouldn't be good. look at gnome, they had (and have) some problems with this. if red hat wants something, but novell wants the other direction, what do you get? decisions aren't made on technical merit alone, but on buzzwords and marketing value. that's bad, as one of the strengths of kde is the fact it generally makes TECHNICAL sound decisions, without 'political/corporate' influence."
    author: "superstoned"
  - subject: "Re: Companies and Technical Working Group"
    date: 2006-01-15
    body: "I can't remember when Red Hat wanted something, Novell wanted something else, and GNOME went with the less technically sound decision. Could you please provide any examples?"
    author: "Reply"
  - subject: "Re: Companies and Technical Working Group"
    date: 2006-01-16
    body: "Well, if you followed the gnome archieves, you couldn't have missed it :o)"
    author: "ac"
  - subject: "Re: Companies and Technical Working Group"
    date: 2006-01-16
    body: "Ah... the good old \"i'm making stuff up\".\n\nok then. ;)\n"
    author: "Reply"
  - subject: "Re: Companies and Technical Working Group"
    date: 2006-01-14
    body: "I think the TWG is the releasing managing group, no more single release manager for the whole huge project.\n\nAs for members working for a single company: I don't really see this as an issue since the charter defines the TWG's job being mainly communication and coordination with much importance put onto preserving the current community culture. Furthermore the TWG members are elected for only one year, and the time can be even shortened if at least seven eV members demand an earlier election.\n\nReading the whole charter makes me assume that only selfless people will actually bother to run for that position which isn't bad, and I'm sure that will even work in KDE."
    author: "ac"
---
The first Technical Working Group for KDE is now being formed, with elections due over the next few weeks. The Group will help the hundreds of KDE contributors come to technical decisions and smooth processes such as major releases. It will also provide technical guidance to KDE contributors. Seven members of the <a href="http://ev.kde.org">KDE e.V.</a> will be elected for an initial six months. After this period the Group will be evaluated, and if it proves successful elections will take place once every year. All members of the KDE e.V. are invited to take part in the election. If you are a contributor to KDE and you would like to take part in the decision making process, you are welcome to <a href="http://ev.kde.org/members.php">apply for membership</a>. Further details can be found in <a href="http://ev.kde.org/corporate/twg_charter.php">the Technical Working Group's charter</a>. Read on for a little more background.
<!--break-->
<p>The formation of the Working Groups had been decided on at the <a href="http://ev.kde.org/meetings/2005.php">recent membership council</a> held at aKademy 2005 last August in Malaga. The details of the Working Group formation were worked out during an <a href="http://ev.kde.org/meetings/2005-working-groups-discussion.php">open meeting</a>. The Technical Working Group will be the second Working Group to be announced, after the Marketing Working Group was <a href="http://dot.kde.org/1131467649/">formed in November 2005</a>.</p>

<p>The purpose of the Technical Working Group is to define and execute the official software releases of KDE, to provide guidance on technical decisions and to make sure that the Open Source development process is being maintained. The responsibilities of the Technical Working Group include release management, guiding the decision making process about external and inter-module dependencies, KDE-wide technical infrastructure and technical guidance. All decisions will be made in the spirit of the Open Source development process and will be well-documented.  Furthermore, the TWG will coordinate actions amongst people acting as KDE representatives, and function as a representative itself. The Technical Working Group will involve the KDE community in the decision making process at all times. Discussions will generally be held on the relevent mailing list, usually <a href="http://lists.kde.org/?l=kde-core-devel&r=1&w=2">kde-core-devel</a>.</p>