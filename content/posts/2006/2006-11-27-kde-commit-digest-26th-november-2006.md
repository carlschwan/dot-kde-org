---
title: "KDE Commit-Digest for 26th November 2006"
date:    2006-11-27
authors:
  - "dallen"
slug:    kde-commit-digest-26th-november-2006
comments:
  - subject: "dolphin screenshots anyone?"
    date: 2006-11-27
    body: ":)"
    author: "Patcito"
  - subject: "Re: dolphin screenshots anyone?"
    date: 2006-11-27
    body: "ok, here's one:\nhttp://commit-digest.org/issues/2006-11-26/files/dolphin.png "
    author: "Patcito"
  - subject: "Re: dolphin screenshots anyone?"
    date: 2006-11-27
    body: "The NeXT-based navigation bar looks (and I bet it feels) pretty cool. Can you also create bookmarks just as easily as in NeXT -- that is to d&d navigation icons into the bookmarks side bar? That would be sweet! Keep up the good work!"
    author: "blacksheep"
  - subject: "Re: dolphin screenshots anyone?"
    date: 2006-11-27
    body: "I agree. Konqueror should have a shelf !"
    author: "zeroheure"
  - subject: "Re: dolphin screenshots anyone?"
    date: 2006-11-27
    body: "And column mode browsing too.."
    author: "Richard Dale"
  - subject: "Tree View?"
    date: 2006-11-28
    body: "Does Dolphin have a Tree View? A Tree View is very useful because it shows file hierarchy and gives context. Plus, Windows Explorer has a Tree View, so most newbies will expect one."
    author: "AC"
  - subject: "Re: Tree View?"
    date: 2006-11-28
    body: "> A Tree View is very useful because it shows \n> file hierarchy and gives context.\n\nAll views are useful because they do something the others don't. But I hope there won't be an extra toolbar button for each of the views, or else we're back to KDE3.\n\n> Plus, Windows Explorer has a Tree View, so most newbies will expect one.\n\nHm I'm quite sure that many Windows user only use the file dialog and \"My Computer\" to browse their filesystem, not a full blown Windows Explorer, which is either ignored or seen as an advanced tool. Anyway, let's not just assume 'most newbies this' and 'most newbies that', this is unscientific."
    author: "gato"
  - subject: "Re: Tree View?"
    date: 2006-11-28
    body: "You probably have been to long under Linux, haven't you?\n\nThe full blown Windows Explorer is a good thing to have a quick view over your filesystem. Don't know anyone who doesn't use it. To be honest, I don't know anyone who just uses the normal window with big icons, and some blue bar to the left of it. Except those who don't know there is anything else...\n"
    author: "boemer"
  - subject: "Re: Tree View?"
    date: 2006-11-28
    body: "i.e., newbies? ;-)\n\nI'll be honest, even though I'm aware that the full-blown Windows Explorer is available, I don't use it, because I consider Explorer to be a basic fs handling tool. In fact, I try to avoid using it at all (partly by using Linux :-P) I'm also not a huge fan of deep hierarchies and thus tend not to need to dig too deep to get to things.\n\nAnyway, the point is, to someone who's only used a computer a little, they've seen My Computer and My Documents links, and those point to the `normal window with big icons, and some blue bar to the left of it'. So that's what they know ;-)"
    author: "Antonio"
  - subject: "Re: dolphin screenshots anyone?"
    date: 2006-11-27
    body: "I really hope the non-typable address bar never gets into konqueror.  It never ceases to annoy me - if I know the location of the folder it's so much quicker to type it in.  Just because you have pretty pictures doesn't mean you should never *ever* use the keyboard..."
    author: "strider"
  - subject: "Re: dolphin screenshots anyone?"
    date: 2006-11-27
    body: "I just tried dolphin 0.7, and the fact is that you can permanently enable the \"classic\" address bar by using the leftmost button or Ctrl-L. I also always type the location myself if I know the location since it is much quicker, but I found that the new style can be useful, too. And new address bar is being designed by kde team + dolphin author ;)"
    author: "eds"
  - subject: "Re: dolphin screenshots anyone?"
    date: 2006-11-27
    body: "The non-typable address bar is probably going to prove more useful for the average user. (i.e. family members). While I agree with what you are saying about the usefulness being able to type stuff in.\n\nA long time ago in a land far way, IRIX, the OS that ran on SGI's hardware featured a GUI with a file path widget that combined the \"row of buttons\" type address bar with a normal text edit widget. You can see it in these screenshots:\n\nhttp://www.mediascape.com/fontix-1.html\n\nBy clicking on the little buttons above the parts of the path, you could quickly move around your path. Maybe the Dolphin developers can borrow some of that idea and merge it in.\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Re: dolphin screenshots anyone?"
    date: 2006-11-27
    body: "The dolphin address bar is already editable AFAIK."
    author: "aac"
  - subject: "Re: dolphin screenshots anyone?"
    date: 2006-11-27
    body: "In that case, you might as well make the parts of the path themselves clickable, I'd say. No need to add buttons above them. Just make the path parts hyperlinks in themselves."
    author: "Andre Somers"
  - subject: "Re: dolphin screenshots anyone?"
    date: 2006-11-27
    body: "But if you did that then you would not be able to click on the path to place the cursor so that you can edit the path. IRIX combined an editable path plus buttons to quickly jump to a parent directory.\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Re: dolphin screenshots anyone?"
    date: 2006-11-28
    body: "Good point. Probably solvable using a modifyer key, but that would have the downside of making the feature less visisble. Ah, well, I guess there are people more creative than me that can come up with something a bit more visually appealing than the IRIX solution yet easy to use and unobtrusive for other uses. "
    author: "Andr\u00e9"
  - subject: "Re: dolphin screenshots anyone?"
    date: 2006-11-28
    body: "Did you EVEN TRY IT FIRST? Before blabbing about \"Non-edit\"?\n\nJust hit Cntl-L or click on the /\n\nJesus, ya wimp."
    author: "Joe"
  - subject: "Re: dolphin screenshots anyone?"
    date: 2006-11-28
    body: "And this \"hit\" is what sucks about those things. Similar to gnome thing."
    author: "m."
  - subject: "Re: dolphin screenshots anyone?"
    date: 2006-11-28
    body: "I'd like to clarify: it sucks not because it is similar to gnome but because it is the same concept."
    author: "m."
  - subject: "Re: dolphin screenshots anyone?"
    date: 2006-11-28
    body: "> I'd like to clarify: it sucks not because it is similar \n> to gnome but because it is the same concept.\n\nI neither understand what you think sucks, nor what the difference is between 'similar' and 'same concept' which are, well, so similar.\n\nI always 'hit' F6 before I edit any path in konq nowadays, and that doesn't suck at all, so what's the point?"
    author: "gato"
  - subject: "Re: dolphin screenshots anyone?"
    date: 2006-11-28
    body: "I don't see what can be done. In this implementation I have to read and remember."
    author: "m."
  - subject: "Re: dolphin screenshots anyone?"
    date: 2006-11-29
    body: "i really don't get what's so bad about this concept... The only time you have to touch more buttons than you would with the current method is if you tab your way to the location bar... Otherwise you still have to click somewhere (the address line, here you just click on a button in stead), or tap [ctrl][l], which is already the default for activating the address line in Konqueror. Sorry, but i don't get it :)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: dolphin screenshots anyone?"
    date: 2006-11-28
    body: "pff... it sure smells like gnome crap! "
    author: "Dddd"
  - subject: "Re: dolphin screenshots anyone?"
    date: 2006-11-27
    body: "You can check out screenshots from the Dolphin File Manager website.\n\nhttp://enzosworld.gmxhome.de/\n\nI've been using it for a couple weeks now, and it is very nice.  Well worth your time if you're wanting to try something new.\n\nCheers."
    author: "beck"
  - subject: "Re: dolphin screenshots anyone?"
    date: 2006-11-27
    body: "Just install it:\nftp://ftp.gwdg.de/pub/opensuse/repositories/KDE:/Community/KDE_Qt_SUSE_Linux_9.3/i586\nftp://ftp.gwdg.de/pub/opensuse/repositories/KDE:/Community/KDE_Qt_SUSE_Linux_10.0/i586\nftp://ftp.gwdg.de/pub/opensuse/repositories/KDE:/Community/KDE_Qt_SUSE_Linux_10.1/i586\n"
    author: "Max"
  - subject: "Krusader"
    date: 2006-11-27
    body: "Honestly, what for do we need Dolphin, when there is Krusader?\n\nIt's the best filemanager I've seen on linux by far...\n...the twin-panel concept is still the best for such tasks!\n\nKrusader should be the default filemanager in KDE4! Seriously! "
    author: "fish"
  - subject: "Re: Krusader"
    date: 2006-11-27
    body: "i don't think, i prefer to use konqueror"
    author: "marc collin"
  - subject: "Re: Krusader"
    date: 2006-11-28
    body: "krusader is great.\n\nkrusader is a power-tool.\n\nkrusader is not about to die, it will be available for KDE4, as far as i know.\n\n> Honestly, what for do we need Dolphin, when there is Krusader?\n\nsince it is not much more user (maybe more appropriate: 'beginner') friendly than konquerer is now."
    author: "cies breijs"
  - subject: "Re: Krusader"
    date: 2006-11-28
    body: "I love Krusader for those times when twin pane is better than single pane (comparing directories, etc), but the UI needs an update.  I keep hitting Del and nothing happens, then I remember its F8 instead.  I know its supposed to be mc compliant, but wouldn't it be better to be KDE compliant???"
    author: "Odysseus"
  - subject: "Re: Krusader"
    date: 2006-11-28
    body: "i think it's very valuable to have applications that address different audiences. for those who love/need an mc type app krusader does great. there's no reason to take that away from people. it also helps lessen the pressure to become more mc-like on apps like konqueror or dolphin from krusader's user base."
    author: "Aaron J. Seigo"
  - subject: "Re: Krusader"
    date: 2006-11-28
    body: "I think MC is a too simple and ancient program. The idea is okay, but I also think it should look around at programs like Total Commander and the like. Or at least give the option of changing the default keyboard layout to konqueror like or something like that...\n\nI only use MC because sometimes there isn't anything better, but even Norton Commander in its later years was much better...."
    author: "boemer"
  - subject: "Re: Krusader"
    date: 2006-11-28
    body: "The typical Krusader user has the skills to install it on his own."
    author: "Davide Ferrari"
  - subject: "Re: Krusader"
    date: 2006-11-29
    body: "> It's the best filemanager I've seen on linux by far..\n\nBut it's slow (archive files), ugly and not 100% usable like Total Commander. Punkt.\n"
    author: "anonymous"
  - subject: "go LANCOM Systems"
    date: 2006-11-27
    body: "> thanks to LANCOM Systems for gratiously providing me with a \n> 11a Access Point to try stuff out\n\nyou guys rock!"
    author: "anonymous"
  - subject: "Stop fixing bugs"
    date: 2006-11-27
    body: "Oi!  Even when I'm fixing a bug every day, I still don't get on the top 10.\n Stop fixing bugs everybody and let us mere mortals have a chance of fame :P\n\n"
    author: "John Tapsell"
  - subject: "Re: Stop fixing bugs"
    date: 2006-11-29
    body: "Well, you could start with fixing 10 bugs a day :o)"
    author: "AC"
  - subject: "Re: Stop fixing bugs"
    date: 2006-12-01
    body: "Heh, in reality it's all about triage - I closed all those bugs as WORKSFORME because, well, they did, and the reporter didn't reply in a month :-)"
    author: "Philip Rodrigues"
  - subject: "Dolphin Waters"
    date: 2006-11-27
    body: "For what it's worth, I have to congratulate the KDE4 developers.\n\nI have been wondering and rather worrying a long time about the 'setting home button' issue for Konqueror and how on earth they ever thought to solve the volumes of hideous problems that it brought up for KDE4. In the end and after reading many arguments about this, I do believe that the more separated approach (file management and web browsing) will allow for a much more user friendly solution overall (while not removing power user features).\n\nMost importantly, I sincerely believe that file management and browsing will become more intuitive, friendly and consistant then ever before in KDE land. I am very happy to see what the developers are doing in this department and KDE4. KDE4 is simply going to rock if you even begin to think about ALL the other projects and development going on."
    author: "ac"
  - subject: "Re: Dolphin Waters"
    date: 2006-11-27
    body: "I do certainly hope they will keep in the possibility of using konq as both a file manager and a browser (and a pdf viewer for that matter), its what i love most about kde."
    author: "Marius"
  - subject: "Re: Dolphin Waters"
    date: 2006-11-27
    body: "actually , that is what i most dislike in kde.\n\nkonqueror is really great , but i hate to have shared bookmark's and shared toolbar and all those settings.\n\ni want a file manager with bookmarks to places in my computer\n\nand a web browser with bookmarks to places on the web.\n\nalso , toolbar settings are of course diferent.\n\nwhat i want from konqueror is just that separation"
    author: "yagami"
  - subject: "Re: Dolphin Waters"
    date: 2006-11-28
    body: "And what about a bookmark to a pdf file on an ftp server on my local network?  Where does that fit in?  If that counts as a web browser bookmark, then why is a pdf on my local server different from a pdf on my machine?  Conceptually it's all the same.\nSame for smb:// - where does that fit in?  Where are you going to draw the lines?\n\nIn my opinion, separating the two is just pointless."
    author: "John Tapsell"
  - subject: "Re: Dolphin Waters"
    date: 2006-11-28
    body: "just because you can draw a line at multiple levels does not mean that it is pointless to draw a line.\n"
    author: "anonymous"
  - subject: "Re: Dolphin Waters"
    date: 2006-11-28
    body: "...but it does not negate the question either. So: where *do* you draw that line then? What makes it conceptually that different? \n\nFor me, it would be ideal if there would be a Konqueror webbrowser edition that focusses on that, and a filemanager konqueror like we have now that can handle and preview basicly anything you like from anywhere you like. For me, that works very well, but I do agree with a lot of people that all this functionality clutters the UI for webbrowsing. And yes, I know a profiles go a long way to solving that, but maybe a thin shell around KHTML could do an even better job."
    author: "Andr\u00e9"
  - subject: "Re: Dolphin Waters"
    date: 2006-11-28
    body: "Well, it's simple:\n- the ftp server wants to be viewed in a file-manager-like view, so navigating to it is a case for the file manager, say, for example, Dolphin.\n- the pdf file is a pdf file, and as such opened with the pdf viewer by default, which would be oKular or Ligature, depending on how that issue works out.\n- For bookmarking it, I could imagine you\n  a) to bookmark the containing folder in the file manager, or\n  b) to bookmark the pdf file with the pdf viewer's bookmark functionality,\n     if there is such a thing.\n  c) and because of a web browser's embed-by-default policy, it should be just\n     as possible to paste the url into the location bar and create a standard\n     web browser bookmark.\n\nThose are clear lines, don't differentiate between remote and local places, and you still got all of the opportunities."
    author: "Jakob Petsovits"
  - subject: "Re: Dolphin Waters"
    date: 2006-11-29
    body: "What if right click an ftp link from a web page, and chose bookmark this link?\n\nWhat about an html file in my home directory?  What about an rtf file ?  What about an html file bookmark on an ftp directory?\n\n\n"
    author: "John Tapsell"
  - subject: "Re: Dolphin Waters"
    date: 2006-11-30
    body: "> What if right click an ftp link from a web page, and chose bookmark this link?\n\nWell you bookmark that location in your browser, then, right? And if you open it, it opens in your browser, given it's an embeddable mimetype. If it's not and you open it nevertheless, I guess the browser will ask you if you want to open it with the assigned app, or save it to disk, or cancel. Like it does now.\n\n> What about an html file in my home directory?\n\nI don't see why you couldn't bookmark it in your browser. And because of the fact that it's an html file, the file manager opens it in your browser by default.\n\n> What about an rtf file ?\n\nIf opened from the file manager, it will be opened with KWord.\nIf opened from the browser, it will be embedded as a KPart.\nSure you can bookmark it in the browser.\n\n> What about an html file bookmark on an ftp directory?\n\nWhat's the difference to an html file bookmark over http? ftp is just another protocol, not a distinction between web page and directory. It's not like only the file manager can do ftp, both browser and file manager can. You just need to get away from the notion that the protocol defines the view, because it doesn't.\nThe mimetype defines the view:\n- folder-like structures are viewed with the file manager.\n- files opened from the file manager are opened with a seperate app.\n- file manager bookmarks will be opened like they would normally be opened if you (double-)click them in the file manager.\n- html files (or any other web contents) are viewed with the browser, as it is the seperate app for html files.\n- files opened from the browser are embedded if possible, if not they are saved to disk or opened with the seperate app.\n- browser bookmarks will be opened like they would normally be opened if you follow a link in a web page.\n\nThat's all you need to consider. No artificial distinction between protocols. No extra configuration on how to open specific mimetypes. Just those few simple rules."
    author: "Jakob Petsovits"
  - subject: "Re: Dolphin Waters"
    date: 2006-11-28
    body: "> but i hate to have shared bookmark's and shared toolbar and all those settings.\n\nThe bookmark implementation sucks in many ways (only global, not view specific, no shortcuts/keywords, no way to quickly enter a search term to find a \"lost\" bookmark or get a short list of hopefully matching ones, no automatic testing/reporting for dead urls, no way to mark url a,b,... to be copied to depth X for offline use and updated as necessary when online), but this has nothing to with Konqueror itself."
    author: "Carlo"
  - subject: "Re: Dolphin Waters"
    date: 2006-11-27
    body: "I second that one. I don't think anyone could get away with crippling our beloved konq that way. The fact that konq so easily combines local/remote information gathering and program launching makes it the perfect shell for the online age.\n\nI even clung to IE on windows for a long time rather than switching to firefox mainly because I knew that if I had a window open, I could use it to find *any information* I wanted... then my friend showed me konq :P\n\nI also think konq's design as a general purpose browsing/launching tool is probably a big factor in why KDE's kioslaves and kparts are so good today. "
    author: "David Laban"
  - subject: "Re: Dolphin Waters"
    date: 2006-11-28
    body: "From reading the development progress, I trust it will keep that possibility and IMO the developers themselves know and understand this very well. I think Dolphin however will introduce a new perspective on problems and how to solve them without anyone having to give up the 'universal' nature of Konqueror. KDE4 does face some important decisions in relation to how it does want to tackle some issues with Konqueror in a logical and consistant way. It is in there where we might find some creative surprises we did not foresee without Dolphin in there. That's why somehow I am very positive and trusting about this development."
    author: "ac"
  - subject: "Re: Dolphin Waters"
    date: 2006-11-28
    body: "i don't think we'll be removing konqueror any time soon. it will remain as our power user tool and share a ton of code with dolphin and the file dialog"
    author: "Aaron J. Seigo"
  - subject: "Re: Dolphin Waters"
    date: 2006-11-28
    body: "Will it go to the new kde-power package? ;-)"
    author: "gato"
  - subject: "i agree with the parent..."
    date: 2006-11-28
    body: "i agree with the parent, totally.\n\ni like file browsing in konq, but i'd prefer:\n\na filemanager with a little previewing and web-enabledness\nplus\na webbrowser that also does some local file browsing/management\n\nthan the current setup that (as the parent show nicely) comes with some proven to be persistent design problems.\n\nWELCOME DOLPHIN!"
    author: "cies breijs"
  - subject: "Yay!"
    date: 2006-11-27
    body: "I equal Laurent Montel in number of commits - finally!  Do I get a prize? :)\n\nActually, I think all of my commits were just refactoring changes."
    author: "Clarence Dang"
---
In <a href="http://commit-digest.org/issues/2006-11-26/">this week's KDE Commit-Digest</a>: Dolphin, an alternative file manager, is imported into KDE SVN. Work on session management in <a href="http://www.kontact.org/">Kontact </a>becomes visible with the implementation of state remembering for tabs in <a href="http://akregator.sourceforge.net/">aKregator</a>. <a href="http://www.mailody.net/">Mailody</a> gets a better SMTP implementation, with authentication support. Many functionality improvements in <a href="http://okular.org/">Okular</a>. An experimental generic API for integration of more online music store services (following the example of the Magnatune implementation) is proposed and developed in <a href="http://amarok.kde.org/">Amarok</a>. Continued speed and memory optimisations in <a href="http://koffice.org/">KOffice</a> and KDE 4 (via. kdelibs).
<!--break-->
