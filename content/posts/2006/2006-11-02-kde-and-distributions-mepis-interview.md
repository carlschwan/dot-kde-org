---
title: "KDE and Distributions: MEPIS Interview"
date:    2006-11-02
authors:
  - "hnordin"
slug:    kde-and-distributions-mepis-interview
comments:
  - subject: "We plan on better integration with OS X."
    date: 2006-11-02
    body: "How? Seeing as this is an interview with a distro maintainer the first that comes to mind is shipping KDE with a theme that looks/acts like OS X but to mention it as the plans for the distros future it seems that just that isn't really much to talk about."
    author: "Sean"
  - subject: "Re: We plan on better integration with OS X."
    date: 2006-11-02
    body: "Perhaps they mean Rendezvous/Zeroconf? "
    author: "Paul Eggleton"
  - subject: "Re: We plan on better integration with OS X."
    date: 2006-11-02
    body: "I found it very intresting. And of course each distribution has to develop an unique profile. MEPIS does fantastic work and it is important to develop good relationships with the distribution guys which make so much happen. \n"
    author: "hans"
  - subject: "What?"
    date: 2006-11-02
    body: "Everyone and his dog condemned the other two distro stories, and less then a week later you guys approve this?  What the [self-censored] happened to the dot?"
    author: "Thomas Zander"
  - subject: "Re: What?"
    date: 2006-11-02
    body: "Because the previous two posts were strictly ads. While this interview has a plus-value in term of information, more than \"my distribution is cool and includes the last version of KDE !\""
    author: "Cyrille Berger"
  - subject: "Re: What?"
    date: 2006-11-02
    body: "I agree, this is a reasonable distro story.  \n\nI'd be interested to know what kind of customisations and adaptations MEPIS makes to KDE - since KDE is such a strong part of their identity, I would expect them to interact more than this article makes clear.  Or is it simply a question of manpower, and vanilla KDE is of high enough quality for them.\n"
    author: "Bille"
  - subject: "Re: What?"
    date: 2006-11-06
    body: "I have used MEPIS since Dec. 2004 and as far as KDE goes I nixed it and use Fluxbox on MEPIS 3.4.3 and it's great. Like most Linux Distros it has it's snafus but overall I wouldn't switch unless I experience better and that has not happened yet. 3.4.3 uses pure Debian Repositories. Take care, JBoman\nPS..Aside from other comments the MEPIS Lovers forum is superb for helping Newbies...I know...I was one."
    author: "JBoman"
  - subject: "Re: What?"
    date: 2006-11-06
    body: "Actually commenting on this post.\n\nMepis works till the first time you try to update it. \tby: xJlM \nI've never had any luck installing Mepis, and after encountering snotty comments about newbies while simply browsing their website, I'll never try again. Too many other good distros out there to limit myself to closed-minded smugness like what i found with Mepis.\n\nThe guy that wrote this post is way off base.MEPISlovers is the most newbie friendly site that I have ever found.I'm a MEPIS guide and moderator at MEPISlovers.We don't put up with snotty posters or distro bashing.We have all been newbies once.I have yet to see any badmouthing of newbies or anyone else(the odd troll flaming will go til someone clamps down)We,as moderators,will lock threads that are causing arguments.MEPISlovers site is very polite and friendly too.\nMEPIS is one of the easiest linux operating systems to install.If anyone has install trouble all you have to do is ask at MEPISLovers and you will get an answer very quickly.There are a lot of MEPIS users at MEPISlovers that eagerly await questions asked by someone.It's like a race to see who can answer the question first.\nAs far as MEPIS working until you upgrade it.Nothing could be further from the truth.Download SimplyMepis 6.0 and you won't need to upgrade very much if at all.It is good practice to upgrade one package at a time.That way you can undo the upgrade if you need to.\nTo install MEPIS you would do well to create your own custom partitions.You can learn how to do that by reading the guides in this link.\nhttp://www.mepisguides.com/   Some good guides here.\nThe guide on creating custom partitions is good reading.Lots of pics too.\n\nI would challenge the poster to provide some links to the alleged 'snotty' remarks that he spoke of.He won't be able to.\nSee you at MEPISlovers!"
    author: "mepnoob2005"
  - subject: "Re: What?"
    date: 2007-02-27
    body: "OK, I'll see if I get some help with this one ;-)\nI have tried to get a working ADSL connection with SimplyMepis 6.5.b6, no luck.\nNo problem with Ubuntu, Mandriva One 2007, SuSe 10 or Puppy, so why the difficulty with 'Simply' Mepis (anything but simple).\nI have tried on 2 computers both have same result... it should just work with minimal or no configuration needed.But it won't.\nTried both configuration utilities."
    author: "GrayNZ"
  - subject: "Re: What?"
    date: 2006-11-02
    body: "Correct.\n\nThis interview is simply one in a larger series of interviews with KDE-centric distros.  Recently we've posted interviews with Sabayon, PC-BSD, Red Flag and Slax.\n\nYou'll notice that we always ask the same questions, to allow readers to compare and contrast distro maintainers' opinions.\n\nThere is no hidden agenda here; just trying to introduce people to new/different distros, reveal their past/present/future, show the strength of the KDE community in these fantastic results, and give some well deserved recognition.\n\nFortifying the communication channel with KDE-friendly distros during the long development cycle of 4.0 is certainly a good thing.\n\nThanks to Henrik for posting this interview.  If people would like to see their KDE distro of choice interviewed, just let us know."
    author: "Wade Olson"
  - subject: "Re: What?"
    date: 2006-11-03
    body: "Please do it for Kubuntu. "
    author: "tabrez"
  - subject: "Re: What?"
    date: 2006-11-06
    body: "Seconded."
    author: "Matt"
  - subject: "Re: What?"
    date: 2006-11-02
    body: "I think, the difference is that this is focused on KDE, and contains information of interest to KDE devs. By contrast, the other distro stories were just that, distro stories."
    author: "Benoit Jacob"
  - subject: "Re: What?"
    date: 2006-11-03
    body: "You're just bitter because BSD is dying."
    author: "ac"
  - subject: "Whatever..."
    date: 2006-11-02
    body: "Wait... this is the guy who was quick to defend someone who violated the GPL, and accused Matthew Garret of doing \"something the blind followers of an unethical hacker of the 1970s would be encouraged to do\"? (http://www.mepis.org/node/10965)\n\nNot sure I can be bothered with anything he has to say now."
    author: "mart"
  - subject: "you started this distro?"
    date: 2006-11-02
    body: "\"I started MEPIS in November 2002 because I wasn't satisfied with the distros I had used.\"\n\n\"Our number one target is non-technical computer users who want an easy path away from Windows.\"\n\nSo why didn't you improve one of the 1000 already existing distros? Instead of creating another one with the same goals as probably half of them."
    author: "au"
  - subject: "Re: you started this distro?"
    date: 2006-11-02
    body: "Well summer 2002 was when I picked my distro (Gentoo) so I remember the situation then pretty well. The distros back then sucked. If you wanted new stuff you had to go with Gentoo or with a RPM distro which forced reinstalls to update. There wasn't a happy medium, of an easy-to-use distro that was also had some apt-get goodness. This is the situation that both Ubuntu and MEPIS grew out of I'd guess.\n\nAnd distros aren't like applications where you can just go an and add stuff. I'm an outsider to distros, but they always seem to be more political and have more organizational inertia then your average open source project."
    author: "Ian Monroe"
  - subject: "KPlayer"
    date: 2006-11-02
    body: "Warren, have you tried KPlayer? I think it is what you are looking for."
    author: "elp"
  - subject: "Codeine FTW"
    date: 2006-11-02
    body: "Codeine is the best video player. :)\n\nThough it lacks TV support, dunno if thats a deal breaker or not."
    author: "Ian Monroe"
  - subject: "\"OnTheGO?\""
    date: 2006-11-12
    body: "I'd like to hear more about this \"OnTheGo feature\". \n\nHow does it work? How well does it work? Anything ranging from implementation details to whether it only works with MEPIS, thus allowing you to move between MEPIS KDE desktops but not between MEPIS and other distros that you might encounter.\n\nThanks,\n\nGonzalo"
    author: "Gonzalo"
---
The <a href="http://www.mepis.org/">MEPIS distribution</a> has been one of the bigger KDE-centric distributions around for some years now, created to make desktop GNU/Linux easier to use. As part of our KDE and Distributions series founder and main contributor Warren Woodford talks to KDE Dot News about the history and current vision of the distribution.






<!--break-->
<h2>Past</h2>

<h3>Can you tell us about the history of your distribution?</h3>

<p>I started MEPIS in November 2002 because I wasn't satisfied with the distros I
had used.  I've been working with GUIs since 1984 and I know what I like and
expect from an OS and a desktop.  Trolltech had the tools and KDE had the
desktop but, in my opinion, everyone was missing the mark when putting it all
together to create a user experience.  So I decided to do MEPIS.  I didn't
know if anyone would like the results, including me.</p>

<h3>Why did you choose KDE and which version of KDE did you first implement?</h3>

<p>KDE and Qt reminded me of the NeXT desktop and NeXT AppKit, which I liked a
lot. Programming in Qt/KDE was easy because the concepts were so similar to
NeXT (now Mac OS X).  I think I first used 2.2.</p>

<h3>How did you find initial support for a new distro?</h3>

<p>I found everything I needed in the documentation.</p>

<h3>What could KDE have done better to help new distros use KDE?</h3>

<p>I can't think of anything.</p>

<h3>What were your first impressions about KDE's documentation and community?</h3>

<p>I'm happy with the documentation. But some apps seems to have ample
documentation, while others seem to have very little.  I wish the
implementation were more even.</p>

<p>I've met some of the community members at shows, and they're a great crowd.</p>

<h2>Present</h2>

<h3>How closely do your releases depend on KDE releases?</h3>

<p>We sync releases with KDE, since KDE is the largest part of what the user
sees.  Typically we wait for KDE to settle a bit after a new release and then
we roll out a new MEPIS.</p>

<h3>Do you have a clear target audience for your distro?</h3>

<p>Our number one target is non-technical computer users who want an easy path
away from Windows.  But often our users are technical and experienced.  For
many of them, computers are a bit of a hobby, a way to learn new things and a
way to socialise with others.</p>

<p>A recent poll on the MEPISLovers forum indicated that we have users starting at 2
years of age going up to 90 and that the curve is flat from about 30 to 60,
or maybe it was 70.  In other words, MEPIS seems to appeal to people of all
ages.</p>

<h3>Do you have any user feedback mechanism? If so, what feedback do they
have about KDE?</h3>

<p>We get feedback from the MEPIS forums, the MEPISLovers forums, direct email,
and reviews written by MEPIS supporters.  MEPIS users usually have little to
say specifically about KDE.  It's there, it works, they like it.</p>

<p>In what ways do you customise the version of KDE that ships with your distro?</p>

<p>We choose which packages to include and we tweak the defaults.  Otherwise, we
let KDE be KDE.  It's an incredibly flexible and adjustable desktop.  We
don't feel a need to rewrite it.</p>

<p>Our OnTheGo feature, which gives the user an encrypted desktop running on a
USB key, uses some custom magic at login time.</p>


<h3>What are the biggest strengths of KDE for your distro?</h3>

<p>The many many well integrated KDE pieces that we can choose from to make the
MEPIS desktop.</p>

<p>Your commitment to quality.</p>


<h3>What are the biggest weaknesses?</h3>

<p>Media device support.  It continues to get better, but it still isn't good
enough for very non-technical users.  I'm hoping that'll be cleared up in
4.0.</p>

<p>And media players.  The current choices all have strengths and weaknesses.
None of them are a clear winner.  It reminds me of K3B 3 years ago, close but
no cigar.  K3B got their act together, and now they kick butt.  Hopefully,
the same will happen to KMPlayer or Kaffeine or some other app.</p>


<h3>What KDE applications are the most popular among your users?</h3>

<p>Amarok, Kmail, K3B, Digikam, Konqueror</p>

<h3>Do you feel that you have a good relationship with the KDE community?</h3>

<p>Yes.  Everything's fine.</p>

<h2>Future</h2>

<h3>What feature would you as a distro maintainer like to see in KDE?</h3>

<p>As I indicated above, I'd like to see a world-class app for playing video
files and streams.  Maybe KMPlayer with more style, a few more features, and
a better UI (like K3B).</p>

<h3>Is the extended 4.0 release cycle an issue for your distro?</h3>

<p>No, it'll give us time to catch up on other work.</p>

<h3>What are you most looking forward to about the 4.0 release?</h3>

<p>I don't know yet.  Every time you have a major release I find lots of new
goodies to play with. :)</p>

<h3>Do you plan any involvement in the beta/RC releases of the 4.0 release?</h3>

<p>To the degree that we have time.  Some members of the MEPIS community are
always among the first to try new KDE releases.  I'm sure they'll be
involved.</p>

<h3>Any other plans for your distro in the future?</h3>

<p>64 bit seems to be inevitable.</p>

<p>We plan on better integration with OS X.</p>



