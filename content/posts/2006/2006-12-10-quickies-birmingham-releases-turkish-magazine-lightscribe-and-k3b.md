---
title: "Quickies: Birmingham, Releases, Turkish Magazine, LightScribe and K3b"
date:    2006-12-10
authors:
  - "jriddell"
slug:    quickies-birmingham-releases-turkish-magazine-lightscribe-and-k3b
comments:
  - subject: "Birmingham Library Project"
    date: 2006-12-10
    body: "Errr ... now I'm confused: Isn't that the same project that was canceled a few weeks ago, due to some strange reasons (\"improper media support\" of the Linux sytems, \"lost\" reference installations,  no expertise in that aera, project leader changed job during the project and works now for an outsourcing company that promotes a \"educational initiative\" with Microsoft)?"
    author: "furanku"
  - subject: "Re: Birmingham Library Project"
    date: 2006-12-10
    body: "No it wasn't cancelled. Somebody had come up with a study to tell us all how much Windows XP would have been, but no, the project wasn't cancelled."
    author: "Segedunum"
  - subject: "CMake at 2.4.5"
    date: 2006-12-10
    body: "Erm, CMake is already at version 2.4.5 now. "
    author: "cm"
  - subject: "Re: CMake at 2.4.5"
    date: 2006-12-10
    body: "Story updated.\n"
    author: "Jonathan Riddell"
  - subject: "Mainstream (US/UK) media bias"
    date: 2006-12-10
    body: "As soon as it was known that gnome was dumped, the media claims that the project was scrapped, instead of giving KDE credit for reviving it.\n\n"
    author: "vm"
  - subject: "(was: \"Mainstream (US/UK) media bias\")"
    date: 2006-12-10
    body: "I'm not so sure what the current status of the project in B'ham is. My impression is that it is dumped altogether for now. \n\nThe news about the cancellation of the project is a few weeks old (IIRC, October 2006?).\n\nThe PDF with the case study dates \"9th of March 2006\", therefor is considerably older than the news about the end of the project.\n\nLooks like not even KDE was able to rescue the supposedly \"professional\" migration experts once they messed up the story.\n\nAny British Linux user in the know of more details about it?"
    author: "confused reader of news"
  - subject: "Re: (was: \"Mainstream (US/UK) media bias\")"
    date: 2006-12-10
    body: "well, if you look at the bottom line: the cost for the migration and subsequent 5 year maintenance is higher than XP upgrade and maintenance. Guess why it was cancelled."
    author: "Matthias Welwarsky"
  - subject: "Re: (was: \"Mainstream (US/UK) media bias\")"
    date: 2006-12-11
    body: "Take a deeper look at the figures that show that Windows is 'cheaper'."
    author: "Robert"
  - subject: "Re: (was: \"Mainstream (US/UK) media bias\")"
    date: 2006-12-10
    body: "But look at this article, this is rather fresh ;-)\n\nhttp://www.techworld.com/opsys/news/index.cfm?newsid=7459"
    author: "Also confused"
  - subject: "Re: (was: \"Mainstream (US/UK) media bias\")"
    date: 2006-12-10
    body: "It hasn't actually been cancelled."
    author: "Segedunum"
  - subject: "Re: (was: \"Mainstream (US/UK) media bias\")"
    date: 2006-12-10
    body: "The project was not cancelled, and in fact has been expanded. \n\nOriginally it was a year long trial, to test whether going Linux was a real alternative. The figures quoted by Microsoft was at a special discount rate, and would have been a \u00a3100,000 cheaper than going Linux.\n\nThe head of IT said they are going to go the Linux route anyway, as the long term benefits are greater, and its a onetime penalty to change to Linux.\n\nThe original trial was claimed to be a failure, but in fact it had just reached the end of the trial phase. They didn't get as much done as they wanted, as the IT guys had no real experiance with Linux and where a little over ambitious.\n\nTHe linux and Open Source adoption is going ahead, the open source stuff they installed is still in use, and more departments are going to be added."
    author: "LeonScape"
  - subject: "Re: (was: \"Mainstream (US/UK) media bias\")"
    date: 2006-12-10
    body: "PS this is probably the most interesting bit for KDE...\n\n\"The case study also detailed the many frustrations involved in approaching an unfamiliar desktop technology, including the discovery that key applications wouldn't run on Linux and usability problems with the original Gnome interface. At one point, realising that most of the usability issues were attributable to Gnome, which had taken three months to configure, staff ripped out Gnome and replaced it with KDE. The new interface was up and running within a week.\"\n\nSo configuration is king, although I expect most of the problems where acutally inexperiance."
    author: "LeonScape"
  - subject: "Nice feedback for kde at page 19"
    date: 2006-12-10
    body: "Especially the feedback at page 19 is interesting for kde:\n\n\"Following a number of interoperability issues raised during the pilot (see\nSection 6.2), BCC decided to replace their chosen desktop manager,\nGNOME, with KDE. Having spent three months developing the SUSE 9.3\nconfiguration with GNOME, the technical team was able to undertake the\nsame work with KDE in less than a week.\"\n"
    author: "Richard"
  - subject: "some quotes from the report"
    date: 2006-12-10
    body: "Oh, I love this report :-)\n\nFirst they make a survey to decide what to do:\n\n\"A customer survey asked users their opinions of these five\nopen source configurations, based on the look and usability of the desktop.\nThe criteria considered were:\n   * Familiarity of the desktop\n   * Recognition of icons and menu options\n   * Ease of logging in\n   * Ease of opening applications from the desktop screen\n   * Ease of moving between applications\nAs a result of this survey the desktop manager chosen was GNOME.\"\n\nA-Ha. But there's trouble ahead:\n\n\"Following a number of interoperability issues raised during the pilot (see\nSection 6.2), BCC decided to replace their chosen desktop manager,\nGNOME, with KDE. Having spent three months developing the SUSE 9.3\nconfiguration with GNOME, the technical team was able to undertake the\nsame work with KDE in less than a week. This demonstrates a substantial\ngain in skills. However it means that the pilot on the public PCs has been\nrunning with a different desktop configuration to that which will be rolled \nout. As a consequence, a second pilot phase will be undertaken with a KDE\ndesktop.\"\n\nSo it's 3 months to configure a GNOME desktop, but less than a week for KDE. And of course _all_ of that is due to \"gain in skills\" of the technical team.\n\nRelated to the \"Interoperability problems\":\n\n\"An early problem identified was that when\nusers double-clicked on an icon to open an application, there was no symbol\nto indicate that the application was opening. This led to users opening\nmultiple versions of the same application, which slowed the PCs down and\ncaused frustration. This issue was resolved when BCC switched from\nGNOME to KDE.\"\n\nYay, startup notification saved our a*se. But this passage is also very interesting:\n\n\"BCCs' initial choice of GNOME as the desktop manager was largely dictated\nby user preferences based on 'look and feel' (See Section 5.1). Given a fuller\nset of decision criteria, the choice of desktop manager may have been\ndifferent: there was already some in-house experience with KDE; the user\npreference for GNOME was slight.\"\n\nIt's allowed to guess why people prefer GNOME over KDE judging by first looks.\n"
    author: "Matthias Welwarsky"
  - subject: "Re: some quotes from the report"
    date: 2006-12-11
    body: "Weird, I think KDE looks much better then Gnome at first glance.\nMaybe because i prefer function over form...\n\nMaybe BCC should have used a funkier window theme and panel:\nhttp://www.linuxjournal.com/article/7889\nhttp://www.kbfx.org/\n\ninstead of going with the default."
    author: "BenAtPlay"
  - subject: "Basket is great"
    date: 2006-12-10
    body: "It won me over when it had a treelist to organize notes instead of tabs.\nI already moved my todo, links and other *.txt stuff over and yet it doesn't end with just text notes. It's so much better to stay organized and it keeps the clutter minimized.\nI'm still on a alpha version of 0.6 but I can't wait to try out the final."
    author: "Anonymous"
  - subject: "ARGH! Bouncing Cursor !!! :D : D :D"
    date: 2006-12-10
    body: "THE worst thing *ever*"
    author: "chrissel"
  - subject: "Re: ARGH! Bouncing Cursor !!! :D : D :D"
    date: 2006-12-11
    body: "which you can switch off ;)\n"
    author: "otherAC"
  - subject: "Re: ARGH! Bouncing Cursor !!! :D : D :D"
    date: 2006-12-13
    body: "How do you disable it? I would love nothing more!!\n"
    author: "LinuxNoob2007"
  - subject: "Re: ARGH! Bouncing Cursor !!! :D : D :D"
    date: 2006-12-13
    body: "In the control center. \n\nLook'n'feel -> Startup Notification -> Busy Cursor \n\n"
    author: "cm"
  - subject: "Re: ARGH! Bouncing Cursor !!! :D : D :D"
    date: 2006-12-11
    body: "my mum loves the bouncing cursor :) it's one of the few things she doesn't hate."
    author: "Chani"
  - subject: "Read Carefully "
    date: 2006-12-11
    body: "Oh dear,\n\nif you read (no need to read carefully!!!) over the report @\nhttp://www.opensourceacademy.org.uk/solutions/casestudies/birminham-city-council/file\nyou will find many mistakes. For example page 42 paragraph 9.1.\n\"... number of rows in a worksheet &#8211; in OpenOffice 2.0, Calc worksheets are\nlimited to 32,000 rows, compared to 65,536 with Excel 2003 ...\"\nWhat? 32,000 for OpenOffice 2.0? They should do their homework before you publish such crap. Another example:\n\"...However, Unix/Linux operating systems had many more vulnerabilities reported than Windows over 2005\u00b3&#8311;.\" Such statements with no explanation my confuse. Many vulnerabilities were classified as \"not serious\" in contrast to the fewer of MS which changed the situation regarding the security issue. This issue was widely discussed and there were different opinions about that. Again, do your homework!\nI'm sure if you read the text you will find something wrong or at least something to discuss."
    author: "proof"
  - subject: "Re: Read Carefully "
    date: 2006-12-11
    body: "You can comment about the report on their website.\nFeel free to do so ;)\n"
    author: "otherAC"
---
Birmingham City Council released a <a href="http://www.opensourceacademy.org.uk/solutions/casestudies/birminham-city-council/">case study</a> for their open source desktop trial.  Buried in the 67 page document is the reason for choosing KDE: quick to configure and the bouncing launch feedback cursor. *** For developers <a href="http://www.trolltech.com/company/newsroom/announcements/press.2006-11-27.6478552402">Trolltech released Qt 4.2.2</a> and Kitware released <a href="http://www.cmake.org/">CMake</a> 2.4.5.  For users <a href="http://basket.kde.org/">Basket 0.6</a> makes your clipboard fun. *** Turkish speakers can read about the history of KDE in new online magazine <a href="http://www.enixma.org">Enixma</a>. *** Finally, showing that free software can work with commercial, CD label buring app <a href="http://www.lacie.com/company/news/news.htm?id=10293">LightScribe announced support for K3b</a>.  Quotes from Sebastian Trueg within.


<!--break-->
