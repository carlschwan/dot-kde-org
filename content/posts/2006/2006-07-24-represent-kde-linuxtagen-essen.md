---
title: "Represent KDE at Linuxtagen in Essen"
date:    2006-07-24
authors:
  - "cniehaus"
slug:    represent-kde-linuxtagen-essen
comments:
  - subject: "Well I have a suggestion..."
    date: 2006-07-26
    body: "> If you are interested in helping us man a stall\nTake RMS. After all he is a stallman. ;)"
    author: "He who had a clown for breakfast"
  - subject: "RMS had been there"
    date: 2006-08-04
    body: "funny, Mr RMS knows the location, he gave a talk there about the danger of software patents in europe.\n"
    author: "Sven"
---
On September the 9th and 10th (Saturday and Sunday) the <a href="http://www.come2linux.org">Essener Linuxtage</a> will take place in the <a href="http://www.uni-duisburg-essen.de/">University of Essen</a> in Germany. KDE will have a stall there, but needs more representatives. If you are interested in helping us man a stall or giving a talk, then please <a href="https://mail.kde.org/mailman/listinfo/kde-events-de">contact us</a>.  Read the <a href="http://www.poweroftwo.de/wiki/index.php/Programm">programme</a> (in German) for more information.


<!--break-->
