---
title: "Scribus Team in the Spotlight"
date:    2006-12-14
authors:
  - "aprokoudine"
slug:    scribus-team-spotlight
comments:
  - subject: "Scribus rules"
    date: 2006-12-14
    body: "Usability improvements: \n* the default background fill color for boxes should be more convenient.\n* autoscaling of images to image frames should be more convenient.\n* Scribus should support a MAC OS X top menu settings which works\nwith all other KDE applications.\n* if frame error are detected by the pdf export the print export check should be\nautomatically invoked.\n* The font engine and line wrap...\n\nScribus is a very good tool. It makes a lot of fun to use it, small, small glitches though. Thanks a lot for the great work you have done."
    author: "Bert"
  - subject: "Re: Scribus rules"
    date: 2006-12-14
    body: "> * Scribus should support a MAC OS X top menu settings which works\n with all other KDE applications.\n\nActually Scribus is not a KDE application, but a Qt one..."
    author: "DeeJay1"
  - subject: "Re: Scribus rules"
    date: 2006-12-14
    body: "Why?"
    author: "Bert"
  - subject: "Re: Scribus rules"
    date: 2006-12-14
    body: "Because they wanted to be independent of kdelibs, mainly to be cross-platform, I guess. With KDE 4 being just as cross-platform as Qt is, I could imagine that the Scribus developers reconsider this decision, but for now it seems to make a whole lot of sense."
    author: "Jakob Petsovits"
  - subject: "Re: Scribus rules"
    date: 2006-12-14
    body: "will they change the name to \"skribus\"??"
    author: "timmy"
  - subject: "Re: Scribus rules"
    date: 2006-12-14
    body: "Let's hope not."
    author: "Tim"
  - subject: "Scribus moving from a Qt to a KDE app?"
    date: 2006-12-14
    body: "I don't know how often this question arises, but I remember a discussion some time ago that the team would like to use KDE/the kdelibs, only the required effort was hindering it in the way that it was not high priority.\n\nWith all the advantages kdelibs offer, the port of Scribus to Qt4 where much of the code will probably get overhauled and the availability of kdelibs on Windows, meaning having the benefits of being a KDE app on all platforms, are there currently plans doing the port? Is it already on the roadmap?"
    author: "Anonymous"
  - subject: "Re: Scribus moving from a Qt to a KDE app?"
    date: 2006-12-21
    body: "Well, simply creating a KApplication object, rather than a QApplication, will give you better integration with KDE in respect to look'n feel (like the MacOS bar people mentioned), session management, among others. Why not doing just that? Using KDE dialogs for file handling and printing will be a bit more work, but it's still trivial stuff."
    author: "blacksheep"
  - subject: "Re: Scribus rules"
    date: 2006-12-14
    body: "For issues like this we have a user friendly tracker: http://bugs.scribus.net Like the interview says, we have our eyes constantly on the bug tracker. \n\n>* Scribus should support a MAC OS X top menu settings which works\n>with all other KDE applications.\n\nScribus is NOT a KDE app. KDE friendly yes :) We enjoy a lot of cooperation from KDE and Koffice folks in particular. Scribus works very well on a KDE desktop and there are KDE specific features like Kfontinstaller which are very useful for Scribus users. \n\nThat said, we will port Scribus to Qt 4.2+, during 1.3.5 which will solve some Mac specific issues with Scribus. "
    author: "mrdocs"
  - subject: "Re: Scribus rules"
    date: 2006-12-14
    body: "Anyway, the top menu setting works not with Scribus which is a real pitty when you use it under KDE in MacMenu Mode."
    author: "Frauke"
  - subject: "Scribus"
    date: 2006-12-14
    body: "I tried to use version 1.3.2 some time ago. Though it was impressive what has already been achieved it was still a long shot away from what I would call suitable for professional work. Usable for school magazines, flyers, yes - but usable for high-cost advertising? Certainly not. Scribus deserves kudos though for doing away with the myth that OpenSource isnt able to do desktop publishing somehow by design. An application like Scribus was unheard-of before in the OpenSource world. And it's coming along nicely. I'm trying it out from time to time and see it's progressing amazingly fast. \n"
    author: "Michael"
  - subject: "Re: Scribus"
    date: 2006-12-14
    body: "> Usable for school magazines, flyers, yes - but usable for high-cost advertising? Certainly not. \n\nI don't get it. Granted I haven't been doing full page glossy ads, but I have done partial page ads in local publications and smaller ads in national publications. I thought mine looked pretty good and they brought in business. I guess \"high cost\" is relative. Paying over $1000 for an ad may not be high cost, but it's not cheap either. So far I have not seen any reason not to continue to use Scribus as we scale up to more expensive ads. Maybe one of us is missing something."
    author: "Eric Laffoon"
  - subject: "Re: Scribus"
    date: 2006-12-15
    body: "You are not necessarily \"missing\" something. It depends on what you want to achieve. There is already a lot you can achieve with Scribus. But with \"professional\" I mean >50.000 EUR so I'm talking about a different category here. And in this category the price of software becomes somewhat irrelevant. In order to convince my colleagues to use Scribus instead of InDesign it simply has to be on par in every respect. This is _by far_ not the case - like it or not. I'm not saying it could never be there. But right now:\n\n- Coprehensive table support\n- PSD support\n- PANTONE and HKS names out of the box\n- General usability\n- Still lots of bugs with complicated layouts\n\nSome of these things will only need a bit more time. Others are more difficult to achieve because of stupid patents and proprietary stuff.\n"
    author: "Michael"
  - subject: "Re: Scribus"
    date: 2006-12-15
    body: "> - PSD support\n\nMakes me wonder how well you actually know what Scribus can do for you :) When I say that Scribus supports RGB/CMYK PSD and handles its layers, blending modes and clipping paths (and 1.3.4cvs supports duotone PSD), do you mean support for some other PSD features?"
    author: "Alexandre"
  - subject: "Re: Scribus"
    date: 2006-12-16
    body: "I just tried it out. Didnt work great the last time I tried it. Works really great now I agree. My problem with PSD support under Linux is still that it only supports features up to Photoshop 6 though. More and more often I get Photoshop CS files which appear completely scrambled in GIMP, Krita, Scribus though. This is because Adobe pulls a M$ here I know but something _is_ already imported so the basic binary structure of the file should be clear. I'm wondering: Is it really that difficult to figure out the missing parts or is it more that those features (like layer effects) are not all implemented yet properly?"
    author: "Martin"
  - subject: "Re: Scribus"
    date: 2006-12-19
    body: "They say, reverse engineering is a job for young guys who live in countries where it's not legally prohibited :)\n\nFirst of all, this is a quite long and boring work for someone who has much spare time. Then you need native features that match features of PS to make it reasonable supporting stuff like layer effects. "
    author: "Alexandre"
  - subject: "Ease Of Use"
    date: 2006-12-15
    body: "I've played with Scribus from time to time, but I'm confused by some basic usability.  When I start a new document and select a template, say a 3-fold template, I expect to see the page split up into 3 areas.  However, all I get is an empty page, which is the same as I get when I select a single page layout.  Am I doing something wrong, or is my expectation of how things should look and act wrong?"
    author: "cirehawk"
  - subject: "Re: Ease Of Use"
    date: 2006-12-15
    body: "I suppose you create a doc with only one page, right? You should add some more pages then to see how it's layouted ;)"
    author: "subik"
  - subject: "Re: Ease Of Use"
    date: 2006-12-15
    body: "Yes, I create a doc with one page.  That's what a 3-fold is right?  Taking one page and folding it into three sections.  Unless you're saying each page represents one panel of the 3-fold?  Now that would surely be a little different.  Well to me at least.  It may be a common thing among real publishers."
    author: "cirehawk"
  - subject: "developers' job positions"
    date: 2006-12-15
    body: "as a student i consider such info interesting, thanks..."
    author: "anonymous-from-linux.org.ru"
---
<a href="http://www.scribus.net">Scribus</a> is known as the most mature open source WYSIWYG page layout application. This interview with members of the Scribus core team focuses on upcoming releases 1.3.4 and 1.3.5, standards in pre-press, success stories and many other important issues. This article was originally <a href="http://www.linuxgraphics.ru/readarticle.php?article_id=24">published in Russian</a> for <a href="http://www.linuxgraphics.ru">linuxgraphics.ru</a>.




<!--break-->
<p>Let's face the fact - people are pretty tired of the mythical "Year of Linux on the desktop". Many can barely believe that open source software can be used by artists for real work. At the same time whole publishing houses have switched to GNU/Linux. More specifically, Nonluoghi Libere Edizioni <a href="http://software.newsforge.com/article.pl?sid=06/04/26/1716257&amp;tid=150&amp;tid=39">has been using open source applications</a> from the very beginning, but PressPeople is a completely different story. </p>

<p>This Portuguese publishing company delivers several monthly and weekly magazines in different formats. Those magazines are devoted to subjects like cooking, teenage life and society, for example. Each issue has a circulation of 20,000 copies or more. PressPeople already use GNU/Linux for all their desktop work except desktop publishing.  Thus, twenty plus workstations are used for a variety of tasks like writing, image selection and other editing. They migrated to GNU/Linux with system integrator <a href="http://angulosolido.pt/">Angulo Sólido </a>, for stability and cost efficiency reasons. </p>

<p>A pilot project is running for migrating an existing print workflow to <a href="http://www.scribus.net/">Scribus</a>. It started with teaching sessions with end users and then moved on to practical tests with Scribus 1.3.0, where some issues were found. Gustavo Homem, an Angulo Sólido representative, says that most of them were related the GUI usability. He noted PDF exporting capabilities of Scribus have shown no relevant problems so far. </p>

<p>As a result an issue list with client priorities and wishes was produced. After some analysis the system integrator specialists got in touch with one of the most active Scribus developers, Craig Bradney. From this, Angulo Sólido sponsored Craig for some enhancements and bug fixes. This worked out very well: all the improvements to the Scribus code were then made available on the 1.3.3.x and 1.3.4 branches. </p>

<p>The pilot project is progressing successfully, pages are already being printed, as parts of a full colour magazine are already made with Scribus and  one of the magazines will be totally migrated by the end of the year. So you may anticipate an update on this story in a couple of months. Now we talk to the Scribus developers.</p>

<ul>
<li><b>Peter Linnell</b> (further as PL) - started with testing early versions of the application, began writing documentation for 0.5.0 (thus, <em>mrdocs</em> nick), he does QA testing, builds RPMs; works as DTP/IT consultant for publishing companies.</li>
<li><b>Craig Bradney</b> (further as CB) - started with testing before 1.1.1, did major refactoring of source code, now mostly does bug fixing, enhancements 	implementation; is employed as a desktop and data centre support technician.</li>
<li><b>Andreas Vox</b> (further as AV) - started by creating the first native port of Scribus 1.3.0 to Mac OS X. Andreas is now working on the internals of the new text system; works as a software developer</li>
<li><b>Jean Ghali</b> (further as JG) - responsible for the native Windows port, works on colour management and is employed as a software engineer for a printing company.</li>
</ul>

<p><b>Seeing Scribus in action in publishing houses is great, but who are the typical users of Scribus according to your experience?</b> </p>

<p><i>PL</i>: We have no statistics, our users are too broad to generalise -  beginners to professionals. </p>

<p><b>Which real life usage of Scribus was most impressive for you?</b> </p>

<p><i>PL</i>: For me most impressive is seeing Scribus adopted by commercial newspapers and reaction of groups I demo Scribus to, showing them several books produced entirely with Scribus - a couple are fine art grade books. </p>

<p><i>AV</i>: I like all publications listed in <a href="http://wiki.scribus.net/index.php/Success_stories">Scribus success stories</a> for this year, for example.  Actually, anyone who did a Scribus document of more than 30 pages is my hero :) </p>

<p><b>Do you have some usual prioritisation of tasks for Scribus development?</b> </p>

<p><i>PL</i>: The roadmap defines it pretty well, but along the way new things come along or better fixes get put. Jean Ghali has been doing lots of improvements to SVG import and colour management, which are much better than before. Sometimes, it is simply a developer's itch or a new feature request that someone looks at and says, "I'll do that now". </p>

<p><b>What does testing of release candidates look like? Is it somehow automated? How often do you manage to test output from Scribus on hi-end RIPs?</b> </p>

<p><i>CB</i>: We do use code profiling and checking tools of course. Some open source like Valgrind and other proprietary ones too. There is no automated testing yet, though it is planned. Experience helps us to know where to look for issues with new code. Plus, we now have several thousand test files either purpose built or some which users have submitted as test cases with bugs. Both give us a realistic range of files which exercise Scribus in all of its capabilities. </p>

<p>Moreover, we are lucky to have a small, but dedicated and adventurous team of users to test the newest unstable code, and access to a range of special software like PitStop and RIPs to check on output. Now that Scribus is actually being used by end users for commercial printing for a wide variety of tasks, we know the output is correct or we would quickly hear from end users. </p>

<p>In the few cases where there were issues, we have a) found older hardware/software at the commercial printer an issue, b) end users misunderstanding PDF/X-3 and c) If there was an issue with Scribus' output it is a high priority for the team. We basically stop coding until the bug is fixed, but this has only been for one or two corner cases. </p>

<p><b>A while ago at <a href="http://rants.scribus.net/">Scribus Developer Blog</a> you <a href="http://rants.scribus.net/2006/07/12/yay-gotcha-4000/">provided</a> some quite impressive charts from the bug tracker. How do you manage to keep open/closed ratio that high?</b> </p>

<p><i>AV</i>: I think as a whole the team is very good about managing and prioritising bugs. We generally test each case quickly after submission. In the case of features, we tend to fix the easy ones quickly and move on... Some features need to wait as other parts of the code may need rewriting or new classes added before a feature can be implemented properly. </p>

<p>CB: We chase bugs constantly. The tracker is open 24x7 on my development boxes and we go scouring through the tracker regularly looking for older requests or bugs that based on more recent code developments are now "low-hanging fruit". It can be hard to drop the count, with the onset of so many new users on Windows and OS X, the request count has jumped quickly. However, our 1.3.x roadmap covered a large scope of requests for both the novice and pro publisher so we are on track to close a lot of issues when we get to the later 1.3.x releases.</p>

<p><b>Some of Inkscape developers are working on a <a href="http://lib2geom.sourceforge.net/">high level library for maths</a> with regards to vector graphics that can be reused by other apps. Are there such projects in Scribus (or planned)?</b> </p>

<p><i>PL</i>: Not at the moment, but of course we would be interested if it were useful for Scribus and I am sure we would have good cooperation from the Inkscape developers, as we have for a long while. As a whole I think all of the developers who work on open source graphics applications are quite collegial.</p>

<p><b>Some experienced users of Scribus did a thorough usability test of GUI a while ago. What is the status of addressing/fixing issues that were discovered?</b> </p>

<p><i>PL</i>:We have done some of the changes already. Some will need to wait for underlying code changes. We also discovered some recommendations would need to wait for the improved widgets in Qt 4. There are some other usability efforts under way with third parties, though nothing to announce yet. Certainly, we do care quite a bit about usability in the sense of trying to balance a need to have sometimes advanced specialised functionality, without overwhelming new users with options which cannot be easily understood. </p>

<p>However, 'usability' is often a misused word, especially when it comes to some open source applications. To me usability has been a code word by some to strip an application of a) configuration options or b) blindly follow the mantra that simpler is better. Well, yes to a point. I've used a number of desktop environments - some your readers may not even have heard of, but I've been frustrated more than once with the OS X GUI too :) </p>

<p>A powerful application like Scribus, a CAD program, or <a href="http://quanta.kdewebdev.org/">Quanta</a>, for example, pre-supposes some knowledge of the subject at hand. If you don't understand HTML or CAD, these kinds of applications will just frustrate you. It was not until I learned some HTML and PHP that I began to appreciate the efficiency and power of Quanta. Now that I do understand the subject, I find Quanta a wonderfully intuitive app. But I am guessing usability experts may not always like it in the traditional sense.</p>

<p>Getting back to Scribus, our usability is not just focused on making Scribus accessible to new users - that I think we do well enough in the main. We also need to consider what is efficient for an experienced user and also those who may have lots of experience with other page layout applications.</p>

<p><i>AV</i>: Talking of OS X, the Qt 4 port will also allow to fix some remaining GUI issues with Scribus/Aqua (like menu handling, slowness, colour shifts, missing installer etc.) in 1.3.5.</p>

<p><b>Imagine that the roadmap is done. Where would you go further to? Would you experiment with workflow and usability like the developer of <a href="http://dtpblender.instinctive.de/">dtpblender</a> or would you keep adding import/export filters?</b> </p>

<p><i>PL</i>: Given what we have outlined for the current roadmap, I would imagine we would look towards enhancing automation and work on refinements of the importers. Importers are notoriously difficult to get 100% perfect and file specs change over time.</p>

<p><i>AV</i>: If the roadmap is done, we'll just write another roadmap! :-)</p>

<p><b>Which open source applications for the printing industry are currently missing, and which of them are critical? Is the Scribus team planning to address these issues and develop such applications, e.g. a Pitstop analog or a high-quality imposition tool?</b> </p>

<p><i>PL</i>: There are a handful of special tools which I think will more than likely remain proprietary for a while. Why? They have a small audience and the developers themselves need to have a lot of specialised knowledge about printing, imaging and PDF/PS. As for imposition, we are going to implement it a bit later directly in Scribus. </p>

<p><b>What do you think about Microsoft's initiative to replace PDF with XPS, the XPS itself and its strategy of semi-opening specs? Is import of OpenXML documents planned to be implemented in Scribus using existing specs?</b> </p>

<p><i>PL:</i> XPS has some interesting features, but as yet, we will have to see what the uptake is. PDF is not going away any time soon, there is too much serious investment at least in the printing industry in PDF. It solves many problems which were painful and expensive to overcome in the past. When I mean investment, I am speaking of the ways printing companies have built their entire printing plant around PDF. Moreover, PDF for print continues to a) have better more sophisticated tools b) it is being extended with functionality like JDF (Job Definition Format) c) the specs are evolving to adapt to more usable, consistent colour management, for example. </p>

<p>As for OpenXML, there are already some limited means of importing it in Scribus and that certainly will improve in the future. </p>

<p><b>Scribus doesn't support exporting to PDF 1.6 yet, which means, for instance, that OpenType fonts cannot be embedded and thus text is automatically converted to outlines. So, what plans do you have for support of PDF 1.6?</b> </p>

<p><i>PL</i>: We keep up with any features we see that users will see of benefit. We don't have a release date from companies like Adobe but for example, we have already seen some things in the PDF 1.7 spec that are interesting and we will support such features as required. Right now, we support a large part of the features of the more common and widely used PDF versions. So, having a combo-box item for "PDF 1.6" doesn't make as much sense as a better text system now. OTF embedding is very new in prepress, not all of the  hardware supports it. We will address things like this at a later stage of development.</p>

<p>CB: We want to support the requirements of users, not just chase numbers. Some of the versions of PDF target certain use cases that are not necessarily beneficial to the typical Scribus user. Having said that, we follow the PDF specification releases closely to check for new and interesting features.</p>

<p><b>One of well-known "limitations" of Scribus is no licensed Pantone swatches out of box. Is it a real showstopper for DTP? What are the prospects of the <a href="http://create.freedesktop.org/wiki/index.php/Open_Color_Standard">OpenColor initiative?</a></b> </p>

<p><i>PL</i>: This is somewhat of a red herring. Spot colours are used less often than one might think. For corporate logos or for certain jobs, yes it is essential, but there is nothing preventing a designer to manually specify the inks. Even then, Scribus can import proprietary named inks, which is what spot colours really are - inks with proprietary names. Scribus does this following the published PS and PDF specs which define how spot colours are used. Scribus' support for named spots is very thorough with no limit to the number of "plates" used. The separation previewer is limited to 16, I think, owing to the limit of the <i>tiffsep</i> device in Ghostscript. The next version of Ghostscript, I believe, will remove this theoretical limit.</p>

<p>While we would welcome working with ink companies around the world, it is honestly not something we have expended a great deal of effort. However, the behaviour of some companies seems down-right silly. Offering open source applications access to the colour palettes would only generate <b>more</b> sales of their inks and more support for their inks.</p>

<p><b>Which features of the new 1.3.4 release are most exciting for you?</b> </p>

<p><i>PL</i>: The next text layouter, which will be refined even more in 1.3.5+, simply put, makes text look so much nicer. The transparency effects and the new character styles are two others. Plus, pre-press needs such as bleeds, crop marks, registration and more are all in this release. This is going to be a big release. There are <b>lots</b> of smaller changes and updates. </p>

<p><b>As I understand it, new transparency effects have become possible only with Cairo 1.2.x that can be chosen instead of libart at <i>./configure</i> stage. You might have read a report on the results of a Cairo/Arthur benchmark. According to it Arthur shows better results with regards to speed. Are you going to support this engine as well?</b> </p>

<p><i>PL</i>: Yes, we've seen this report. When Cairo has reached rough parity in speed with libart, we may make it the default engine for Scribus. We are going to port Scribus to Qt 4.2 during the v1.3.5 development cycle. We may remove support for libart and add support for Arthur. We'll keep support for both of them, though we don't know yet for how long. </p>

<p><b>Could you please tell more about new the text system? What improvements should we anticipate?</b> </p>

<p><i>AV</i>: The new text system brings both internal and user visible changes: a) refactoring of existing code to make it more manageable, b) implementation of a new linebreaker which optimises whole paragraphs, c) implementation and use of a shaping engine which allows ligatures and advanced OpenType features for Latin text and provides infrastructure for international shaping. Most of the nice visible features will appear only in 1.3.5. Version 1.3.4 will only support the new style system (char styles, for instance) and optical margins, maybe special spaces, word tracking and better justification will come along, but no promises. </p>

<p><b>So, that means that with 1.3.5 we are going to have specific OpenType features supported?</b> </p>

<p><i>AV:</i> One by one and not all at once. OpenType features should be applied in a certain order, and some are language/script specific, some are discretionary, e.g. swash glyphs or minuscule numerals. The OpenType spec suggests an order for the features, which should be on by default and which depend on language/script. So we'll first pull in <a href="http://www.freedesktop.org/wiki/Software_2fHarfBuzz">HarfBuzz</a> to get access to the OpenType tables, then implement the default features, then some of the most requested user-selectable ones. </p>

<p><b>Colour management is already quite mature in Scribus. Are you planning any further substantial changes?</b> </p>

<p><i>JG</i>: Well, yes - a different internal design. Basically we will abstract colour management. In the future, that will allow us to provide support for several colour engines. I think for example to <a href="http://www.apple.com/macosx/features/colorsync/">ColorSync</a> on OS X and <a href="http://www.argyllcms.com/">ArgyllCMS</a> on Linux. The first benefit of this new design for users will be colour management enabled by default. We plan also to implement colour managed RGB output, one effect being better printing capabilities on inkjets.</p>

<p><b>How would you rate the current impact of Scribus on printing industry? Do you have some kind of vision for Scribus? </b></p>

<p><i>PL</i>: Hard for us to know about impact in the printing industries. Mostly, we are under the radar because our PDF is highly conformant and usually "Just Works". As for a vision, I think it's simply to make the best page layout app which runs on Linux, Mac OS X and Windows and to do things which are innovative and which differentiate us from the rest. Not just different, but cool and useful like the way Scribus handles interactive PDF.</p>



