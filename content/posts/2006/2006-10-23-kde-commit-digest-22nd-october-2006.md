---
title: "KDE Commit-Digest for 22nd October 2006"
date:    2006-10-23
authors:
  - "dallen"
slug:    kde-commit-digest-22nd-october-2006
comments:
  - subject: "Kopete+telepathy+Decibel = good"
    date: 2006-10-23
    body: "This looks like a good thing. The major problem of kopete is that it have a VERY INCOMPELTE implementation of protocols, no icq file transfer, buggy msn file transfer, no msn voice, incomplete (and most times absent because not compiled in) google talk voice... well you got the idea.\nIf telepathy works as it says on it's homepage and support well all those protocols, and being a freedesktop project that can receive contributuins from other than kde/qt users/programmer, it really will improve kopete a lot, and the team can focus on what really matters, the interface."
    author: "Iuri Fiedoruk"
  - subject: "Re: Kopete+telepathy+Decibel = good"
    date: 2006-10-23
    body: "This is indeed an awesome thing. Kopete was always the best-looking client. \nBut, devs seem to be really tired of catching up to yet another version of MSN, Oscar etc file-transfer protocol - so, understandably, there were a lot of unfunctioning things. Here's to hoping that having one common, working backend will make a lot of happy KDE hackers."
    author: "Daniel \"Suslik\" D."
  - subject: "OpenWengo backend for Kopete SIP?"
    date: 2006-10-23
    body: "OpenWengo (http://OpenWengo.org) is a Free & Open Source SIP program that uses Qt for its graphic user interface and that has the backing of France's largest Voice IP provider. I think it would be great to include OpenWengo's SIP functionality into Kopete."
    author: "AC"
  - subject: "Re: OpenWengo backend for Kopete SIP?"
    date: 2007-07-02
    body: "i absolutely agree, all of the cool features that openwengo has should be implemented to kopete. The wider support to every kind of IM and SIP of course.\nIt should be a communication center where you are able to get in touch in every available way in only one interface regarding e-mails, chat, IRC(!), landline and mobile sip calls, calls to msn and other networks, (it would be nice to make it to Skype as well somehow!)"
    author: "Z"
  - subject: "Re: OpenWengo backend for Kopete SIP?"
    date: 2007-09-16
    body: "It would be great to at least fully support jingle in Jabber out of the box, so I could use Kopete for calls via, e.g., talk.google.com\n\nAnd yes, SIP/RTP support is very welcome as well ;)\n\nCurrently I use Kopete for Jabber/ICQ/MSN/Yahoo/IRC, Jabbin for gtalk, and twinkle for SIP. Three different programs, it's not good, is it =)"
    author: "magesor"
  - subject: "Re: Kopete+telepathy+Decibel = good"
    date: 2006-10-24
    body: "IMHO there's one oft overlooked way to solve the protocol catchup issue.  Let someone else do it.  Make Kopete a pure Jabber/XMPP client and use the available transports."
    author: "ac"
  - subject: "Re: Kopete+telepathy+Decibel = good"
    date: 2006-10-23
    body: "The support is incomplete, but if you compare with other OSS projets... kopete has the best yahoo support, good with msn. no others OSS projects has sound with MSN or Yahoo afaik. For icq file transfer, there was a SoC about it, and there are work in progress for msn file transfert.\n\nYes, telepathy is a good long term thing, but i don't expect too much about it, because kopete has today a better support of protocols.\n\n "
    author: "Johann Ollivier Lapeyre"
  - subject: "Re: Kopete+telepathy+Decibel = good"
    date: 2006-10-23
    body: "Yes, but you see, now kopete developers can port code to telephaty, and plus receive code from other people. If we could agree with other good IM-Clients like Gaim, Sim-IM and Psi to work on a common protocol library set, all features would be there in no time."
    author: "Iuri Fiedoruk"
  - subject: "Re: Kopete+telepathy+Decibel = good"
    date: 2006-10-23
    body: "That would be, indeed, most wise."
    author: "NabLa"
  - subject: "Re: Kopete+telepathy+Decibel = good"
    date: 2006-10-23
    body: "From the introduction:\n\"Also, we will be making our protocol plugin available as a Telepathy Connection Manager. That means you'll be able to use our protocol plugin in \nany Telepathy client, such as Landell, Tapioca, and soon, in Gossip.\"\n\nI'll say the Kopete developers have alraday decide to do that, so other Telepathy client can just pitch in and help.\n"
    author: "Morty"
  - subject: "Re: Kopete+telepathy+Decibel = good"
    date: 2006-10-24
    body: "Does Tapioca depend on glib and gstreamer?\n\nGlib not really a big problem, but gstreamer, what about audio framework independence? "
    author: "ac"
  - subject: "Links aren't working"
    date: 2006-10-23
    body: "The links to Diffs and View Visual Changes aren't working."
    author: "asdf"
  - subject: "visual changes"
    date: 2006-10-23
    body: "too bad the view visual changes links dont work to see the new oxygen carddecks"
    author: "ReTyPe"
  - subject: "Re: visual changes"
    date: 2006-10-23
    body: "The links to moreinfo/ and visual changes for each revision are now working.\n\nDanny"
    author: "Danny Allen"
  - subject: "Strigi"
    date: 2006-10-23
    body: "Is stringi to compete with beagle on the KDE platform?"
    author: "Petteri"
  - subject: "Re: Strigi"
    date: 2006-10-23
    body: "That seems likely.  Strigi was popular at aKademy, and as a search solution for KDE 4, it has some important advantages (I am trying to recall from discussions at aKademy - others can correct me):\n\n- It is intended to be fast and small.\n- It provides useful tools such as facilities to read various types of archives recursively\n- The developers are keen on integrating Strigi into various parts of KDE.\n\nIf I remember correctly, the parts of Strigi which are not visible to the end user are not tied to Qt or KDE though."
    author: "Robert Knight"
  - subject: "Re: Strigi"
    date: 2006-10-23
    body: "This is pure c++, so no mono, and still gnome/kde ready."
    author: "Johann Ollivier Lapeyre"
  - subject: "Re: Strigi"
    date: 2006-10-23
    body: "yes, it is. it's much faster, uses less memory, doesn't hog your cpu as much as beagle does, and can index files-in-files (like the files inside a zipfile which is attached to a email).\nit doesn't support as many filetypes yet, tough, and well, still needs some work here and there."
    author: "superstoned"
  - subject: "Re: Strigi"
    date: 2006-10-24
    body: "What's the difference between Strigi and Nepomuk? Are they competing or cooperating or both (in what way)?"
    author: "Patcito"
  - subject: "Re: Strigi"
    date: 2006-10-24
    body: "Yeah wha tis the diff and how are they supoe to work togheter or against each other"
    author: "tikal26"
  - subject: "Re: Strigi"
    date: 2006-10-24
    body: "On a really simplified level you can see Strigi as a data source for Nepomuk.\n\nStrigi extracts information from files and creates relations between these information and the files it got them from.\n\nNepomuk can then use this relation data with relations from other sources to create additional relations each single source might not have enough input for.\n\n"
    author: "Kevin Krammer"
  - subject: "Nepomuk"
    date: 2006-10-23
    body: "Wow! Judging from the picture ( http://nepomuk.semanticdesktop.org/xwiki/bin/download/Main1/Images/bubbles2.png )\nNepomuk will be everything I dreamed a 'social semantic desktop' could be...\n\nNow if this could become relatively widespread once its stable, that would be awesome. Very promising!!"
    author: "Darkelve"
  - subject: "Re: Nepomuk"
    date: 2006-10-23
    body: "Hi, could you explain what it is and where you want to use it for? I read the website but still don't understand what it is\n\nThanks"
    author: "AC"
  - subject: "Re: Nepomuk"
    date: 2006-10-23
    body: "this might help:\nhttp://dot.kde.org/1113428593/\n\nand this:\nhttp://appeal.kde.org/wiki/Tenor\n\nor this :D\nhttp://www.kdedevelopers.org/node/2279"
    author: "superstoned"
  - subject: "Woohoo!"
    date: 2006-10-23
    body: "Use a QGuardedPtr for the KPIM::ProgressItem in KMAccount. \n Fixes a bazillion crashes when dereferencing deleted items. \n \n BUG: 105701 \n BUG: 114953 \n BUG: 114197 \n BUG: 117475 \n BUG: 118083 \n BUG: 128131 \n BUG: 129007 \n BUG: 133023 \n BUG: 133745 \n \nThanks Andreas Kling!"
    author: "crashed"
  - subject: "Re: Woohoo!"
    date: 2006-10-24
    body: "Still doesn't fix 126715, which seems to affect more users ;-("
    author: "Alistair John Strachan"
  - subject: ".doc writing support, about time..."
    date: 2006-10-23
    body: "This is one of those stupid things that help the open source image, but doesn't actually do anything.\n\nMore than once I've sent my resume to someone, in .rtf format, who instantly assumed that I was a Bill Gates hater.   By saving as .doc (or if I had remembered to rename .rtf to .doc as this path does automaticly) I could avoid the assocation with all those idiots who don't know what to hate.    \n\nI hate working with Microsoft Windows (but I will if I'm forced to - thus I'm writing this in IE).   I do not hate Bill Gates, I have never met him, but I suspect if I did we would get along just fine.  I'm any easy going person, and most people can get along with me.   (That isn't to say everyone, there are a few people who can't stand me, but not too many) I get tired of explaining this to everyone.\n\nI wish I had thought to do this myself years ago."
    author: "bluGill"
  - subject: "Re: .doc writing support, about time..."
    date: 2006-10-23
    body: "> More than once I've sent my resume to someone, in .rtf format, who instantly \n> assumed that I was a Bill Gates hater. By saving as .doc (or if I had \n> remembered to rename .rtf to .doc as this path does automaticly) \n> I could avoid the assocation with all those idiots who don't know what to hate.\n\nI don't buy that, I really don't.  I would expect a reasonable person to accept whatever format I give them as long as it works with the software they use.  Microsoft Word can open both RTF, plain text and HTML files as far as I am aware.\n\n"
    author: "Robert Knight"
  - subject: "Re: .doc writing support, about time..."
    date: 2006-10-23
    body: "Why send you resume in an editable format at all? Isn't PDF much more suitable for these kinds of documents?"
    author: "Andre Somers"
  - subject: "Re: .doc writing support, about time..."
    date: 2006-10-24
    body: "Unfortunately a lot of company explicitely ask for .doc file. Mostly because they run it throught software that will extract part of the cv to fill a database with what interest them, and also because they know nothing else.\n\nAs for saving .doc (using the rtf filter), as OOo does it, and Ms Word as well when you choose to save for older version of Ms Word, we figure out that we could do the same after all :)"
    author: "Cyrille Berger"
  - subject: "Cardsets"
    date: 2006-10-23
    body: "> my first card set :)\n\n...and we all hope it's the last too! :P"
    author: "Anonymous Coward"
  - subject: "Wrong link for Telepathy"
    date: 2006-10-23
    body: "In the second paragraph, the link named Telepathy points to Kopete's homepage ;)"
    author: "Andrei"
---
In <a href="http://commit-digest.org/issues/2006-10-22/">this week's KDE Commit-Digest</a>: the <a href="http://dot.kde.org/1161090032/">location for aKademy 2007 is Glasgow, Scotland</a>. The KDE backbone of the <a href="http://nepomuk.semanticdesktop.org/xwiki/bin/Main1/">NEPOMUK</a> research project has been imported into KDE SVN. A GUI editor for database lookup columns has been added in <a href="http://www.kexi-project.org/">Kexi</a>. More SVG card sets are added to the resurgent KDE games for KDE 4. User interface enhancements in <a href="http://kst.kde.org/">Kst</a> and <a href="http://edu.kde.org/kalzium/">Kalzium</a>. Multimedia file tag handling improvements in <a href="http://amarok.kde.org/">Amarok</a> (.wav) and <a href="http://www.vandenoever.info/software/strigi/">Strigi</a> (.mp3). Strigi-enhanced versions of the standard find and grep utilities introduced. Three utilities from a suite of ODBC and database tools surface in kdenonbeta/.



<!--break-->
