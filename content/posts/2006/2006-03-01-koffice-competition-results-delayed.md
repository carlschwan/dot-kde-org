---
title: "KOffice Competition Results Delayed"
date:    2006-03-01
authors:
  - "brempt"
slug:    koffice-competition-results-delayed
comments:
  - subject: "What about kpresenter ?"
    date: 2006-03-01
    body: "It crashes even more often than krita"
    author: "robert"
  - subject: "Re: What about kpresenter ?"
    date: 2006-03-01
    body: "So, and have you helped out by telling us about the crashes? As I blogged about recently (http://www.valdyas.org/fading/index.cgi/hacking/koffice/slip.html), developers cannot do a thing about a crash they don't know about, and we don't get as many crashes as users do, because we know what the code is supposed to do. Do us a favour, and help out by notifying us of crashes.\n\nOh, and about KPresenter -- it has received a lot of loving care over the past year, so unless you're talking about the first beta, your experiences are wildly out of date."
    author: "Boudewijn Rempt"
  - subject: "Re: What about kpresenter ?"
    date: 2006-03-02
    body: "Well I can't personally mention anything specific, but I would like to say that the support in the 1.4x versions for MS Office's Powerpoint format is terrible. Any presentation that I've tried to open in it has come out completely rendered improperly, whereas OpenOffice can not only deal with the files, it can also save them in the same format."
    author: "hb"
  - subject: "Re: What about kpresenter ?"
    date: 2006-03-02
    body: "There has been a Google summer-of-code project to improve the quality of powerpoint import, but I haven't tested the results myself. I seldom need to edit other people's presentations, and when creating my own presentations MS file support doesn't matter, of course.\n\nThe problem with supporting MS file formats is that if all KOffice developers were to devote all their time to the MS file filters, we still wouldn't have fully working filters, and we wouldn't have a working KOffice either."
    author: "Boudewijn Rempt"
  - subject: "KOffice"
    date: 2006-03-02
    body: "I think all the KOffice developers are doing a fantastic job and I think that KOffice 2 will be even better (especially with the QT 4 libs)."
    author: "Ian Whiting"
  - subject: "M$ import fuss"
    date: 2006-03-03
    body: "omigod,\nwhy the hell all those 'but-I-want-my-docs/xls-ppts-imported-natively' addicts just don't give up whining ?\nremember: it is _NOT_ the responsibility of OSS developers why those crappy formats are still undocumented and have to be reverse-engineered.\nthere is really better work to be done, as said.\nas for practical issues, yes, I sometimes just use OO or anything else to get M$ files converted so that I can process them, which I do using koffice then.\nkoffice is clearly the suite of choice, lightweight, easy to use, getting better at a tremendous speed, in short: koffice rocks.\n"
    author: "hoernerfranz"
---
The <a href="http://www.koffice.org/competition/guiKOffice2.php">KOffice UI competition</a> has received a large number of very innovative and detailed entries. Unfortunately KOffice 1.5 is a little delayed and still needs to be released. So all hands are on deck to make sure 1.5 has a perfect OpenDocument implementation and no Krita crashes. That means that the jury (Inge and me, Boudewijn) have decided to postpone our final decision until Sunday, instead of tomorrow, as we need more time to be able to treat all the great contributions fairly.





<!--break-->
