---
title: "Qt 4.2 Release Candidate Issued"
date:    2006-09-09
authors:
  - "msmith"
slug:    qt-42-release-candidate-issued
comments:
  - subject: "Wow"
    date: 2006-09-09
    body: "I'm using Qtopia snapshots on weekly basis to develop gfx applications on a MIPS based processor. Well, that's it: it r0cks!!!! Every week I go watching the diffs between snapshots and Qtopia core has recently been evolving at a fast pace. Yesterday you thought 'that feature is missing', today you check and you have it!\nI think I will settle on this RC now, keep on rocking the game with the power of Trolls behind my back ;-)\n"
    author: "koral"
  - subject: "QDate - An Oppotunity Lost..."
    date: 2006-09-10
    body: "I was initially quite excited to see that the new and improved QDate actually supported dates of Julian Day 1 and greater (i.e. any date after 1 January 4713 B.C), but was then dissapointed to find it doesn't support dates before that.  Sure it covers most of the useful range, but if they were going to do a major re-write then why not support it?  If ExtDate in KDEedu libs can do from -50,000 BC to +50,000 AD, then why not QDate?  Ah well, at least they gave us public to/from JD calls, and most of KDE will benefit from it and the new calendar widget.\n\nJohn."
    author: "Odysseus"
  - subject: "Re: QDate - An Oppotunity Lost..."
    date: 2006-09-10
    body: "Who is Julian exactly and why does this matter?"
    author: "AC"
  - subject: "Re: QDate - An Oppotunity Lost..."
    date: 2006-09-10
    body: "http://www.google.com/search?q=julian+calendar"
    author: "Anonymous Coward"
  - subject: "Re: QDate - An Oppotunity Lost..."
    date: 2006-09-12
    body: "No no. Julian calendar is different. What this jd is is the Julian Day Number, used by astronomers. See: http://en.wikipedia.org/wiki/Julian_day_number\n\nStrictly speaking, the internal storage of a date in QDate is as a Julian Day Number, corresponding to 1200 UTC of the given Gregorian (current calendar system) date.\n\nI am really happy to see this feature implemented in Qt 4.2, since it was I who requested it! :) As to dates before 4713 BCE, well we don't use them really, do we? In fact, though KStars provides for dates before that, I really don't approve, since planetary and stellar positions for such early dates are not known with such accuracy at such far epochs (points of time). Charting such positions should be ethically accompanied with a warning of low accuracy or even high inaccuracy. Otherwise it only creates a false wonder in the users' minds of KStars' plotting prowess.\n\nErgo, 4713 BCE is *more* than enough."
    author: "Shriramana Sharma"
  - subject: "4.2rc"
    date: 2006-09-10
    body: "Damn, what a bunch of changes. Don't these guys ever sleep? I was happy to get all the 4.1 changes into my head, there come the trolls and throw out a new load for me to start over.\nI like the new QGraphicsView, though."
    author: "orkzwlm"
  - subject: "still no SVG icon support?"
    date: 2006-09-12
    body: "that is a little disappointing."
    author: "Applaus, Applaus"
  - subject: "Re: still no SVG icon support?"
    date: 2006-09-12
    body: "Maybe it got missed in the change log. There's a SVG Icon engine in src/plugins/iconengines/svgiconengine/. Is that what you are looking for?"
    author: "Girish"
---
Trolltech <a href="http://www.trolltech.com/company/newsroom/announcements/press.2006-09-01.2663807442">has issued a release candidate</a> of Qt 4.2 under an evaluation licence.  This version features CSS-like widget styling capability, a new 2D canvas class called QGraphicsView, text completion, new calendar and font selection widgets, and new desktop integration features.  Detailed list of new features  on <a href="http://doc.trolltech.com/4.2/qt4-2-intro.html">the Qt 4.2 intro page</a>, get it from the <a href="http://www.trolltech.com/developer/downloads/qt/qt42-rc">download page</a>.


<!--break-->
