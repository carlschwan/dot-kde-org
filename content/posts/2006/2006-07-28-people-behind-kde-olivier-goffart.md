---
title: "People Behind KDE: Olivier Goffart"
date:    2006-07-28
authors:
  - "jriddell"
slug:    people-behind-kde-olivier-goffart
comments:
  - subject: "Wireless networking"
    date: 2006-07-27
    body: "<i>Now that I have a laptop, I'm missing a fast tool to quickly switch from one network to another.</i>\n\nTry knetworkmanager.  It is nothing short of amazing.  Finally something that is not only as good as the windows method, but better.  This was something I missed for a very long time as all the previous similar programs were quite clunky to use or just didn't work at all for me."
    author: "Leo S"
  - subject: "Re: Wireless networking"
    date: 2006-07-28
    body: "Does knetworkmanager work well with ndiswrapper?  I have to use that to use my wireless.."
    author: "cirehawk"
  - subject: "Re: Wireless networking"
    date: 2006-07-28
    body: "It works for me. I am using opensuse 10.1 and a Hama wlan adapter with RT2500 Chipset. Strangely it works only when I use knetworkmanager. When I try the old way without knetworkmanager I get no connection between adapter and router."
    author: "Roland"
  - subject: "Re: Wireless networking"
    date: 2006-07-28
    body: "I never tried it with ndiswrapper, since the open source broadcom drivers worked with less hassle for me.  The only thing that doesn't work is the signal strength.  Every network shows as full strength."
    author: "Leo S"
  - subject: "Re: Wireless networking"
    date: 2006-07-28
    body: "I heard something a few months ago about open source broadcom drivers.  You mean they are in a usable state?  Where would I find these drivers?  I would definitely rather use a native linux driver than ndiswrapper."
    author: "cirehawk"
  - subject: "Re: Wireless networking"
    date: 2006-07-29
    body: "The answer is just a few key strokes away!\nAlt+F2\nggl:opensource broadcom drivers"
    author: "Albert Astals Cid"
  - subject: "Good answer"
    date: 2006-07-28
    body: "What is your favourite place in the world?\n::1\n\n+10 Geek points for this answer :)"
    author: "Anony Moose"
  - subject: "Re: Good answer"
    date: 2006-07-28
    body: "And what does ::1 mean for non geeks like me.\n"
    author: "Anonymous"
  - subject: "Re: Good answer"
    date: 2006-07-28
    body: "Well, I assume that the answer is based on the saying \"There's no place like home\", except that his answer can be read as \"There's no place like localhost\" - ::1 is the ipv6 equivalent of 127.0.0.1 (aka localhost) in ipv4."
    author: "Michael Jahn"
  - subject: "Re: Good answer"
    date: 2006-07-28
    body: "humm, a user of ipv6..."
    author: "funbar"
  - subject: "Re: Good answer"
    date: 2006-07-31
    body: "You are a true geek, sir.\n\nMy mouse clicks for you.\n\nBen"
    author: "BenAtPlay"
  - subject: "Re: Good answer"
    date: 2006-07-31
    body: "sorry, but this phrase is just \"too old\" and \"too well known\" to be geeky. "
    author: "Dani"
  - subject: "KDE and Freedom"
    date: 2006-08-03
    body: "Why is KDE being developed?  Its a proprietary shell.  To promote true freedoms shouldnt you guys be using and developing for GNOME.  I have yet to see a true commercial or important softrware application target the QT and KDE platforms."
    author: "ChrisA"
  - subject: "Re: KDE and Freedom"
    date: 2006-08-03
    body: "> I have yet to see a true commercial or important softrware application target the QT and KDE platforms.\n\nGoogle Earth is one such application."
    author: "Anonymous"
  - subject: "Re: KDE and Freedom"
    date: 2006-08-07
    body: "> Its a proprietary shell.\nYou seem to have been infected by a troll. Please take the following vaccine, so you don't contaminate others: http://kdemyths.urbanlizard.com/topic/10. Thank you."
    author: "TrollDetector2000"
---
Today's star of <a href="http://people.kde.nl">People Behind KDE</a> is a member of what was once described as "the younger generation of Kopete developers".  This man talks Messenger and Jabber nativly but only communicated on IRC thanks to Babelfish.  Learn about the trials of a Kopete developer in our <a href="http://people.kde.nl/olivier.html">interview with Olivier Goffart</a>.


<!--break-->
