---
title: "People Behind KDE: Allan Sandfeld Jensen"
date:    2006-09-14
authors:
  - "tbaarda"
slug:    people-behind-kde-allan-sandfeld-jensen
comments:
  - subject: "thank you"
    date: 2006-09-14
    body: "thank you for the wonderful PBK.. as usual :)"
    author: "Vlad Blanton"
  - subject: "Trip complaints?"
    date: 2006-09-14
    body: "Tell her you could live in the States and you have to be on opposite coasts at least 4 weeks per year. The distance is a helluva lot farther than her getting on the rail and visiting you in a short time."
    author: "Marc Driftmeyer"
  - subject: "Hi"
    date: 2006-09-14
    body: "Hi Allan! Your work on KDE is much appreciated! By the way, I think you're cute. ;-)"
    author: "AC"
  - subject: "Re: Hi"
    date: 2006-09-14
    body: "> By the way, I think you're cute. ;-)\n\nI completely agree with you !"
    author: "ac"
  - subject: "Re: Hi"
    date: 2006-09-14
    body: "This is obviously a try to discourage cursory comments as could be read after Celeste's featuring on PBK. I don't know if that's a good and proper way, but it's worth a try, I think.\n\nSo ignoring his technical merits and stuff: he surely is a hot rod. A man shouldn't be ashamed to be complimented on his looks.\n\ncu, tom"
    author: "TunaTom"
  - subject: "Re: Hi"
    date: 2006-09-14
    body: "Allen, don't let these guys intimidate you. The fact  that you're such a hot looker does not hinder you coding skills at all and I would love to join you on a keyboard sometime. '-) "
    author: "Anonymous admirer"
  - subject: "Re: Hi"
    date: 2006-09-15
    body: "Having met the guy in person, I can assure you all, he has a class 'A' bottom."
    author: "Max Howell"
  - subject: "Re: Hi"
    date: 2006-09-15
    body: "I demand photographic proof! :-)"
    author: "AC"
  - subject: "Which protocols support seeking now?"
    date: 2006-09-14
    body: ">I have committed my random access KIO to trunk which means you can read and write anywhere in a remote file if the protocol supports it, usefull for seeking and stuff.\n\nIs there a list of protocols that support seeking? I'm particularly interested about SSH (fish:/), and Samba (smb:/), and FTP."
    author: "AC"
  - subject: "Re: Which protocols support seeking now?"
    date: 2006-09-14
    body: "Only file in SVN and http in my branch.\n\nI am planning on looking into fish the next time I have spare time and Coolo promised to do Samba."
    author: "Carewolf"
  - subject: "Re: Which protocols support seeking now?"
    date: 2006-09-15
    body: "Thanks for this information!"
    author: "AC"
---
Tonight in the two-weekly <a href="http://people.kde.nl/">People Behind KDE</a> series we are featuring <a href="http://people.kde.nl/allan.html">Allan Sandfeld Jensen</a>. He is a KDE core developer, mostly active for <a href="http://www.khtml.info">KHTML</a> and KDE multimedia. After reading the interview you will know what his personal "carewolf" looks like, together with all other personal things you have to know about this developer.

<!--break-->
