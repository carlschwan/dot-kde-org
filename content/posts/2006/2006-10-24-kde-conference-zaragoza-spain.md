---
title: "KDE Conference in Zaragoza, Spain"
date:    2006-10-24
authors:
  - "acid"
slug:    kde-conference-zaragoza-spain
---
The Spanish KDE developers together with HispaLinux and ZaragozaWireless are organising a <a href="http://zaragozawireless.org/jornadaskde/">KDE Conference in Zaragoza, Spain</a> on 4th and 5th of November. It is oriented towards people with good computer knowledge who want to get started with KDE programming and for users that want to know the present and future of graphical environments.  The talks will include <a href="http://www.kdedevelopers.org/blog/1186">Antonio Larrosa</a> talking about the KDE project and how to get involved, <a href="http://people.warp.es/~isaac/blog/">Isaac Clerencia</a> about KDE Ruby &amp; KDE 4 and <a href="http://tsdgeos.blogspot.com/">Albert Astals</a> about KPDF/Okular and "<em>the life and death of a bug</em>".  The talks will be in Spanish and the entrance is free but limited to 50 people due to capacity constraints.




<!--break-->
