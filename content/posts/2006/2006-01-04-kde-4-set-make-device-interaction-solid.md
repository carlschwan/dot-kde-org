---
title: "KDE 4 Set to Make Device Interaction Solid"
date:    2006-01-04
authors:
  - "steam"
slug:    kde-4-set-make-device-interaction-solid
comments:
  - subject: "Good Title"
    date: 2006-01-04
    body: "Heh, first thing I noticed was the title play between Plasma and Solid.  Neat.  Only problem is that we can't use Liquid as a title for anything... silly apple."
    author: "Troy Unrau"
  - subject: "Re: Good Title"
    date: 2006-01-04
    body: "But they could use Fluid if they wanted."
    author: "Ryan"
  - subject: "Re: Good Title"
    date: 2006-01-04
    body: "good idea!!! now lets try to come up with a project that fits the name :D"
    author: "superstoned"
  - subject: "Re: Good Title"
    date: 2006-01-04
    body: "I suggest we call the new audio framework Ksteamer!"
    author: "Niels"
  - subject: "Re: Good Title"
    date: 2006-01-05
    body: "Given the connotation of steamer (at least in some uses) that would be a really horrible name.  Hmm, maybe we could retroactively give that name to arts :P\n\nAnyway, I suppose you mean Kstreamer, and probably as somewhat of a joke, but check it out.  Stream, fluid...kind of a relationship there.  Interesting :o)"
    author: "David Walser"
  - subject: "Re: Good Title"
    date: 2006-01-05
    body: "Naa, but considering Plasma, Solid, and the proposed Fluid, I strongly propose Gas!"
    author: "Imous Anon"
  - subject: "Re: Good Title"
    date: 2006-01-05
    body: "That's taken... By the GNU Assembler.\n"
    author: "SadEagle"
  - subject: "Re: Good Title"
    date: 2006-01-05
    body: "Damn, I wanted to ask everybody if they had gas, now everybody dose already. "
    author: "Imous Anon"
  - subject: "Re: Good Title"
    date: 2006-01-05
    body: "How about just dropping the confusing K and calling it Steamer?"
    author: "Jason"
  - subject: "Re: Good Title"
    date: 2006-01-04
    body: "Remember that GLOcean windec?  How about helpful, but not distracting fluid like animation hints for style/windec/menus/desktop/etc.\n\nJust a thought."
    author: "Ryan"
  - subject: "Re: Good Title"
    date: 2007-06-29
    body: "The audio component is called \"Phonon\", and can be found at http://phonon.kde.org ."
    author: "riddle"
  - subject: "Re: Good Title"
    date: 2006-01-04
    body: "We could still use Bose-Einstein condensate.  \n\nhttp://bose-einstein-condensate.kde.org/, how's that for a project URL?   ;-) \n\n"
    author: "cm"
  - subject: "Re: Good Title"
    date: 2006-01-04
    body: "Gas is not a good idea."
    author: "reihal"
  - subject: "Re: Good Title"
    date: 2006-01-05
    body: "What, you don't want servers to pass Gas everytime someone downloads KDE?"
    author: "David Walser"
  - subject: "Re: Good Title"
    date: 2006-01-06
    body: "Its not gas, its another state of matter entirely. :)"
    author: "Ian Monroe"
  - subject: "Re: Good Title"
    date: 2006-01-05
    body: "But there is a database named Solid database (http://www.solidtech.com/). Might run into some trademark issues."
    author: "Rob"
  - subject: "Re: Good Title"
    date: 2006-01-05
    body: "Solid is a plain English word, so I don't expect trademark issues :o)"
    author: "rinse"
  - subject: "Re: Good Title"
    date: 2006-01-05
    body: "Just like \"windows\" :D"
    author: "Ryan Winter"
  - subject: "Re: Good Title"
    date: 2006-01-06
    body: "Hehe good point. \n\nIn this case its not really an issue since Solid isn't a seperate product."
    author: "Ian Monroe"
  - subject: "Re: Good Title"
    date: 2006-01-05
    body: "Why not use \"air\" or \"aerate\"?\n\nDave"
    author: "David Pastern"
  - subject: "Re: Good Title"
    date: 2006-01-05
    body: "nike has air, but in case aerate has been taken, try air8"
    author: "bob dagit"
  - subject: "Re: Good Title"
    date: 2006-01-05
    body: "but let's forget about ice9, it has been taken"
    author: "bob dagit"
  - subject: "Sweet"
    date: 2006-01-05
    body: "What about a project \"Sweet\" for Suse Yast integration. At least parts which are not SuSe specific could get merged. I think of stuff like the partitioning tool."
    author: "Gerd"
  - subject: "Re: Sweet"
    date: 2006-01-05
    body: "The partition tool is nothing more than a frontend for GNU Parted.\nSo, i guess it makes more sense to write a KDE-gui for Parted then to port the YaST-module.."
    author: "rinse"
  - subject: "Re: Sweet"
    date: 2006-01-06
    body: "Mandrake has by far the best Linux partitioner that I'm aware of\n\nIf only it was easier to get QTParted to support reiserfs.."
    author: "Anonymous"
  - subject: "Re: Good Title"
    date: 2006-01-05
    body: "You forgot Oxygen.\n"
    author: "Jonathan Patrick Davies"
  - subject: "Re: Good Title"
    date: 2006-01-06
    body: "I know the \"K-naming\" has gone into disrepute, but I like it, and \"liKuid\" should work.\n\nDuncan"
    author: "Duncan"
  - subject: "Re: Good Title"
    date: 2006-01-27
    body: "We could also announce an innovative, exciting, revolutionary new project called 'Vapour', and then never release anything at all ..."
    author: "Jonathan Weaver"
  - subject: "Portability?"
    date: 2006-01-04
    body: "Notice the reverse definition they're using for portability: \"Each platform providing the necessary Solid backends will be supported.\" In other words, Solid does not support platforms, instead platforms must support Solid. This is insane. Solid needs to examine how other crossplatform software has been written. Imagine how non-crossplatform Qt would be if Trolltech had this philosophy and left it up to Microsoft and Apple to do their own native ports.\n\nI fully realize how difficult it is to make a crossplatform hardware/device layer.  But when \"Solid is specifying the features the underlying system must provide\", it's doing nothing more than passing the buck.\n\nI'm hoping they just have their definition wrong."
    author: "Brandybuck"
  - subject: "Re: Portability?"
    date: 2006-01-04
    body: "What they mean (I think) is that the platforms will provide something like HAL on linux and then a backend for Solid will be developed for that platform using the available technologies, I don't think they are saying that on Windows or OS X that they would rely on microsoft and apple to do it, they are going to rely on the developers on that platform primarily (though probably many Solid developers will work on many platforms).\n\nI think \"Solid is specifying the features the underlying system must provide\" means stuff like the OS must have some way for apps to be notified of hardware or network changes, preferably through means other than the app being forced to periodically poll the status."
    author: "Corbin"
  - subject: "Re: Portability?"
    date: 2006-01-04
    body: "indeed. there is already a HAL backend, and i don't think its written by HAL developers..."
    author: "superstoned"
  - subject: "Re: Portability?"
    date: 2006-01-04
    body: "perhaps clearer wording would be \"Additional platforms can be supported by writing a Solid backend for that target.\" who actually does the work, which is what you seem to be hung up on, hardly matters. either a solid back end exists .... or it doesn't. =)\n\n> But when \"Solid is specifying the features the underlying system must\n> provide\", it's doing nothing more than passing the buck.\n\nit's not passing the buck, it's defining the needs of the desktop. if those requirements aren't well defined, how can anyone be expected to fill them? this is how HAL originally got going (and why it didn't exist earlier): they sat down and figured out what the desktop needed and wrote code to fill those requirements.\n\nso now we have a list of requirements. if a platform already provides those facilities it's simply a matter of writing a Solid backend for it. otherwise the platform needs to be improved first before Solid can be ported to it.\n\nthis is really no different than requiring a network stack to be able to use a network-centric app like kopete or requiring that the system (hardware and OS) offers hardware meters before you can show CPU temperatures in a GUI. =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Portability?"
    date: 2006-01-05
    body: "It seems that Solid is trying to specify a standard, but Solid isn't a standard. What if the underlying platform doesn't choose to accept the imposition of that standard? What if a platform decides it likes another desktop and decides not to implement any of those kernel features Solid tells it to? What if some kindly soul at KDE writes those features, but that platform won't accept them?\n\nThis is a concern to me for two reasons. First, I don't use Linux. Second, I refuse to use proprietary video drivers. Will KDE be fully functional for me? If not, I'll be moving to a different desktop."
    author: "Brandybuck"
  - subject: "Re: Portability?"
    date: 2006-01-05
    body: "a) Solid is an API for device/network detection and hardware information. It's features are provided through backends like HAL.\n\nb) If the platform decides not offer any hardware probing features in its kernel, it's there problem, right?\n\nc) If the platform does offer the needed kernel features, it just a matter of either porting HAL or writing a new backend for Solid. Solid neither specifies the implementation nor the API of those kernel features. This is all handled by the Solid backend.\n\nd) I'm sure KDE will also function on platforms that don't provide the necessary functionality. You will just miss certain features.\n\ne) Solid features have *nothing* to do with proprietary video drivers"
    author: "Christian Loose"
  - subject: "Re: Portability?"
    date: 2006-01-05
    body: "This is why that quote is so confusing, because it is stating that the backends must conform to the Solid specification. In other words, a platform can provide sufficient hardware probing features, but Solid won't use them because they didn't follow the Solid specification. Hopefully that's not what their portability definition means, but that is how it is written.\n\np.s. Sorry about the video driver reference. I had recently read about XGL only supporting proprietary drivers, and I conflated the two hardware issues in my mind."
    author: "Brandybuck"
  - subject: "Re: Portability?"
    date: 2006-01-06
    body: "Well, if there are hardware probing features that don't do what Solid needs, then no it may not support the platform. If it's just that the platform doesn't do things in the exact same way as Linux does, then that's the purpose of Solid. Systems like solid and hal are just a way to let people developing application ignore the specifics of an individual OS, just like QT mostly allows you to ignore whether your app is for Windows, Mac OSX, or Linux.\n\nSolid wouldn't be saying exactly how the platform has to behave, it just specifies what information it needs to be able to get somehow to work properly. Some OS's may be more efficient about providing it than others though. "
    author: "Greg"
  - subject: "Re: Portability?"
    date: 2006-01-05
    body: "Do you have any doubts that all relevant plattforms will support it?"
    author: "Gerd"
  - subject: "Re: Portability?"
    date: 2006-01-05
    body: "It's the only way it can be done. HAL is a back-end, but the only way it can be used in different operating systems is if it is actually there - which is only Linux at the moment. There is a lot of OS dependant stuff when it comes to hardware communication. Likewise, Solid can only be used if the Solid backends are there.\n\nAnother way of wording this is to say that you port the necessary Solid backends to each platform and they will then be supported ;-)."
    author: "Segedunum"
  - subject: "Re: Portability?"
    date: 2006-01-06
    body: "What they mean is, that in Linux Solid can use the HAL backend, but it can't in FreeBSD or NetBSD or Solaris. So, on each of those *platforms*, a different Solid backend would need to be implimented.\n\nIt has nothing to do with drivers or hardware.\n\n"
    author: "I think you mis-understand platforms...."
  - subject: "Re: Portability?"
    date: 2006-01-07
    body: "> Solid needs to examine how other crossplatform software has been written.\n\nActually, take a look at GCC and most other cross platform tools and you will discover this is exactly how they work. Firefox is the same way, and so is Mono. I don't know about OpenOffice, but I would expect that to be the same, too. The proper way to develop any cross-platform application is to abstract away the differences, which means writing a driver for each platform that has a specified API, and any platform that provides that API should run the application."
    author: "Anonymous"
  - subject: "yet another layer ?"
    date: 2006-01-04
    body: "Hi,\ni'm a big kde fan but i'm wondering why there is the need for such a low-level library integrated in a high-level component : the desktop. I have the feeling that solid will be another layer on the top of hal and dbus and that the knowledge base will profit kde users only. I imagine this is not the purpose of the developers but the article sounds a bit like that.\n\nCould you explain the need of implementing such a lib into kde if i'm wrong ? I'm not sure i've understood the whole thing."
    author: "polux"
  - subject: "Re: yet another layer ?"
    date: 2006-01-04
    body: "An example use of this technology is that when you plug in your MP3 player, amaroK would recognize it and add it to the list of media devices. Without something like Solid or media:/, amaroK would have to depend on HAL directly (which is what Banshee does).\n\nReally I would consider the DE to be part of the operating system, as it plays roles given to the OS in MS or Mac.\n\nAnd yea, Solid is a KDE library..."
    author: "Ian Monroe"
  - subject: "Re: yet another layer ?"
    date: 2006-01-05
    body: "\"Really I would consider the DE to be part of the operating system\"\n\nBut the Unix desktop is NOT part of the operating system, no matter how often Microsoft and Apple tell you otherwise. The Unix model is an onion, with the desktop being a layer on top of lower level layers. Because of this, KDE can run on *ANY* Unix system with an X11 server. Yes, the desktop does need to access kernel services and other low level resources, but that doesn't mean you have to tightly couple the desktop to the operating system the way Microsoft and Apple do."
    author: "Brandybuck"
  - subject: "Re: yet another layer ?"
    date: 2006-01-05
    body: "\"KDE can run on *ANY* Unix...\"\n\nRight, but not any Unix provides HAL or D-Bus, for this reason we need yet another abstraction layer.\n"
    author: "Tobias K\u00f6nig"
  - subject: "Re: yet another layer ?"
    date: 2006-01-06
    body: "Right I'm saying the definition of \"operating systems\" now encompasses the functions of a DE. It isn't a stretch to say that providing to developers easy access to multimedia, hardware, and an HTML viewer are functions of an operating system. I'm not saying that this somehow means that DE's need to be unportable and stuck on a kernel. \n\nI mean, if you look at the functionality KDE provides they are often because on the other platforms Qt resides they are already provided by the OS."
    author: "Ian Monroe"
  - subject: "Re: yet another layer ?"
    date: 2006-01-09
    body: "In a Java VM, you get the core interpreter, which basically knows about the byte code and the underlying OS. But no Java Runtime or SDK comes without a huge set of libraries (handling IO, ZIP files, GUI stuff), but Java would be long dead if it did not come with them. They are bundled with the JDK, but not part of the core system, and yet Java specific. some of them have OS-dependent back-ends too.\nSolid will be that set of libs shipping with for a given OS, for KDE apps. And who knows, future may lead to have Gnome and KDE sync up on this topic too :-)"
    author: "FreddyTheTeddy"
  - subject: "Re: yet another layer ?"
    date: 2006-01-04
    body: "if there would be no solid, every application has to support several different ways of getting hardware information. it would mean a lot duplication of effort..."
    author: "superstoned"
  - subject: "Re: yet another layer ?"
    date: 2006-01-04
    body: "it's actually more of a \"mid-level\" library much like Qt is: it sits between low level (usually OS-specific, often non-portable) implementations and desktop GUI apps. this provides both abstraction (and portability) and a more friendly API.\n\nthis way a KDE app has exactly one API to deal with for hardware instead of one per platform. superkaramba, ksim, ksysgaurd and others have all duplicated efforts trying to achieve this for that one app to varying degrees of success.\n\nas a bonus, the API is much like the ones KDE/Qt developers are already familiar with. they don't have to learn yet another API style, they can remain portable and start interacting better with hardware for a minimal time investment."
    author: "Aaron J. Seigo"
  - subject: "Re: yet another layer ?"
    date: 2006-01-04
    body: "HAL is Linux specific.  KDE isn't, and thus Solid isn't."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: yet another layer ?"
    date: 2006-01-05
    body: "Correction: HAL is portable software with only a Linux backend available at the time.\n\nSun is working on porting it to Solaris, and there are people working on a FreeBSD port too.\n"
    author: "Reply"
  - subject: "Re: yet another layer ?"
    date: 2006-01-05
    body: "Honestly this is a core misconception of the Linux Desktop. You cannot leave low level stuff to distributors or set your desktop environment on it in an abstract manner.\n\nIt is time for the desktop environments to get into the dirty low-level stuff because only desktop environments can unify the mess the distributors diversity created."
    author: "Malte"
  - subject: "fantastic"
    date: 2006-01-04
    body: "The plan for Solid appears to be addressing all the weaknesses of media:/. KDE4 is going to freakin' rock."
    author: "Ian Monroe"
  - subject: "Re: fantastic"
    date: 2006-01-04
    body: "yes, it's certainly the \"natural\" evolution of the media:/ concept. massive kudos to ervin.\n\nthere's also \"alpha\" code already in svn for solid =)"
    author: "Aaron J. Seigo"
  - subject: "Distributions Extinction"
    date: 2006-01-04
    body: "I dream of a day when all distributions will disappear. Thanks to Solid, we won't even a hardware control center, everything will work perfectly with zero configuration. For example, as I plugged in my Speedtouch USB Modem, KDE tried to copy my configuration from Windows, username, password and provider, but I won't have Windows, so from my local settings, KDE will pick my country by default and ask me to fill in two fields, username and password, also in a list, there  is the list of all countries, but because KDE knows where this modem is generally used, it put those countries before any ( Yeah, If I press Ctrl and click on any region of the windows I can have access to very detailled settings ) finally because KDE knows I am not paranoiac and that I use BSD, It will select the right firewall security level.\nThanks to Gas, KDE can guess my artistic preferences from my English style in KMail, the way I type, the applications I use and how often I use my desktop, Gas will select a theme that I would have never tought of !\nSolidification is a distro-generator, it will open one text field where I will write \"Complete LAMP Server KMines slick theme\", Solidification will ask me to confirm the new packages list and configuration and will ask me to add personal files. After 10 minutes, Solidification will have downloaded the packages from a web server and build and installable live-cd ! I will also try for fun \"office internet multimedia hobby programming on 512Mb usb key\", \"My desktop\", \"Enlightenment desktop\", \"Media center with all ogg of <my preferred artist>\", wonderful !\nThanks to Fluid, my KDE Linux will run as fast as turning on the TV.\nFire is my virtual tutor, it makes me creative and efficient: it helps me to logout when I am tired, to take my medicins, to avoid Slashdot, to organize my self, to uninstall Fortune and to advise me.\nIce will be a programs repository it helps me choose and install the right program for my works.\nHappy will be me. KDE is fantastic !"
    author: "Youcha"
  - subject: "Re: Distributions Extinction"
    date: 2006-01-04
    body: "Somebody has to package software and distribute base packages, updates and bug fixes.  That's the (literal) job of a distributor, aka distro.  There's plenty of room for competition.  They are the \"open administrators\" versus the \"open developers\" of KDE (or Linux, or BSD, or whatever)... they are complementary and do all the work so, for the user, it just works."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Distributions Extinction"
    date: 2006-01-05
    body: "Very good post! Not only ideas are good, bit it even avoid taking drugs to think as higher as you can! :-)\nI even like all the names: Plasma, Solid, Gas, Fluid, Fire, Ice.\nHow about the subprojects? Like Solid/Iron, Solid/Chromium, Gas/Steam, Fire/Flame, Fire/Pyro, Ice/Freeze [kalzium comes handy in naming!].\nAnyone willing to start implementing Fire? The idea isn't that bad.\n"
    author: "nemeca"
  - subject: "Re: Distributions Extinction"
    date: 2006-01-05
    body: "How about 'Korundum' as a mineral name then?"
    author: "Richard Dale"
  - subject: "Re: Distributions Extinction"
    date: 2006-01-07
    body: "> Anyone willing to start implementing Fire? The idea isn't that bad.\n\nIt's already implemented by Microsoft... it's called Clippy.\n\nSo called \"intelligent\" agents just don't work out very well in real life."
    author: "another nickname"
  - subject: "Re: Distributions Extinction"
    date: 2006-01-05
    body: "> I dream of a day when all distributions will disappear. Thanks to Solid, we won't even a hardware control center, everything will work perfectly with zero configuration. For example, as I plugged in my Speedtouch USB Modem, KDE tried to copy my configuration from Windows, username, password and provider, but I won't have Windows, so from my local settings, KDE will pick my country by default and ask me to fill in two fields, username and password, also in a list, there is the list of all countries, but because KDE knows where this modem is generally used, it put those countries before any ( Yeah, If I press Ctrl and click on any region of the windows I can have access to very detailled settings ) finally because KDE knows I am not paranoiac and that I use BSD, It will select the right firewall security level.\n\nThe main problem is DB. If we had data about your modem we could do something. For example, look at the kppp internet providers' list. It's nearly empty. Look at templates for KOffice applications. It's the same problem. Nobody want to contribute data, because it's harder than configure every new installation (it's not to often).\n\n> Thanks to Gas, KDE can guess my artistic preferences from my English style in KMail, the way I type, the applications I use and how often I use my desktop, Gas will select a theme that I would have never tought of !\n\nNot all people write in English :). A lot of research should be done for this program, it's academic project.\n\n> Solidification is a distro-generator, it will open one text field where I will write \"Complete LAMP Server KMines slick theme\", Solidification will ask me to confirm the new packages list and configuration and will ask me to add personal files. After 10 minutes, Solidification will have downloaded the packages from a web server and build and installable live-cd ! I will also try for fun \"office internet multimedia hobby programming on 512Mb usb key\", \"My desktop\", \"Enlightenment desktop\", \"Media center with all ogg of <my preferred artist>\", wonderful !\n\nIt could be done. Some LiveCD master for Kubuntu, for example. Debian tags can help. Select skeleton(server, client console, client X, client KDE). Add packages according to tags and package names. Select data to add to CDs or DVDs (documents, music, photos, ...). After all of this run script (that already exists) to build your custom distro. It's need Express Installer (in the terms of Kubuntu) in the skeleton. It could need visual choose of installer options (don't ask them during install of custom distro).\n\n> Thanks to Fluid, my KDE Linux will run as fast as turning on the TV.\n\nAfter some years new generation TV turning on about 2 minutes... :)\n\n> Fire is my virtual tutor, it makes me creative and efficient: it helps me to logout when I am tired, to take my medicins, to avoid Slashdot, to organize my self, to uninstall Fortune and to advise me.\n\nFor me, it's should be passive popup. Just information: \"time to rest\", \"too many blog reading\", \"day summary: kdevelop 7h, konqueror web 20min, kopete 20min, amarok 10min, konsole 10min\"...\nIt could be done some way in KDE3.\n\n> Ice will be a programs repository it helps me choose and install the right program for my works.\n\nThe same: visual program with debian tags. Adept, if I remember name correctly, but it's need tag autocomplection.\n\n> Happy will be me. KDE is fantastic !\n"
    author: "anonymous"
  - subject: "Re: Distributions Extinction"
    date: 2006-01-05
    body: "There will always be distributions to make packages that will definitely behave well with each other."
    author: "Robert"
  - subject: "Editing; SuSEplugger"
    date: 2006-01-04
    body: "Solid seems to me to be promising. But the first thing I noticed about the web site is that the text is not as clear or as grammatical as it could be. Perhaps it was written by a speaker of English as a second language. Maybe it would be a good idea for a native speaker of English to volunteer to edit the text.\n\nCould somebody who knows about both SuSEplugger and Solid explain to me the difference in functionality between the two? Also, what is the licensing for SuSEplugger? Could SuSEplugger code be used to speed the development of Solid?\n"
    author: "Paul Leopardi"
  - subject: "Re: Editing; SuSEplugger"
    date: 2006-01-05
    body: "I used SUSEplugger with kde 3.4 and earlier, but since kde 3.5 I use kio_media instead.\n"
    author: "rinse"
  - subject: "the knowledge base"
    date: 2006-01-04
    body: "I understand that the Solid libs needs to be KDE libs and that's great.\nBut how about sharing the knowledge base with other projects, I think the best way to do this would be to host it on freedesktop or make it available to other projects that use a different backend than Solid. That way more people will be able to send feedback, the knowledge base will grow bigger and so faster. I guess that this will be possible as Solid will be opensource every other projects will be able to implement the Knowledge base API part. But maybe it would be better to organize something about it now with gnome and other freedesktop projects.\n What do you think?"
    author: "Patcito"
  - subject: "Re: the knowledge base"
    date: 2006-01-05
    body: "I don't think this is really necessary. Solid will use HAL servies etc. (depending what's available) and offer a KDEfied API for it. freedesktop.org so far is very Linux centric, while the purpose of Solid is to offer an abtracted API for accessing lower level information which can only be received in different way on different systems/platforms. The info of course is beeing shared since the code for that all is and will be in KDE's open SVN."
    author: "ac"
  - subject: "Qt bindings"
    date: 2006-01-05
    body: "Is it possible for things like the Qt-DBUS bindings, poppler-qt etc to be moved into kdesupport or equivalent? I really hate having to download svn code from another server just to try out things that are necessary, not optional ;)\n\nAny other suggestions welcome, I'm currently using kdesvn-build to do all the hard work."
    author: "ac"
  - subject: "Re: Qt bindings"
    date: 2006-01-05
    body: "I don't think that the dot is the right place to discuss such things."
    author: "Christian Loose"
  - subject: "Re: Qt bindings"
    date: 2006-01-05
    body: "Well, the whole poppler (or dbus, whatever) project isn't going to move into kdesupport (any more than they would move into a Gnome archive, or an enlightenment archive), and the bindings are closely tied the underlying project. Also note that there are no KDE dependencies in the poppler-qt bindings, so why should it be tied to KDE? Sure, it will be used by KDE, but also by other, non-KDE, projects.\n\nIt is much easier to maintain the bindings in conjunction with the underlying code, and freedesktop.org is a good place to work on such projects.\n\n"
    author: "Brad Hards"
  - subject: "Re: Qt bindings"
    date: 2006-01-05
    body: "kdesupport started out as a repository for dependency libs as a convenience to developers so that people didn't have to hunt down packages across the internet.\nActual development always took place elsewhere, kdesupport just acted as mirror that got updated once in a while."
    author: "Waldo Bastian"
  - subject: "Re: Qt bindings"
    date: 2006-01-06
    body: "What about making it easy for KDE developers to use the bindings as well? Why should I have to check-out some svn code from a non-KDE repository just to get a working development KDE? Sure, I can do it. Do I want to? No.\n\nLook, if kpdf relies on poppler-qt and I have to check it out of freedesktop.org, I just wont bother. I'll live without and use xpdf or something else. It's not going to affect the little bugfixes and enhancements I want to do in other parts of KDE.\n\nBut when it comes to Qt-dbus, that's a different matter and I hope that it's integrated into the KDE svn."
    author: "ac"
  - subject: "Re: Qt bindings"
    date: 2006-01-06
    body: "> But when it comes to Qt-dbus, that's a different matter and I hope that it's\n> integrated into the KDE svn.\n\nI'm working on that. It'll take time, so volunteers are welcome.\n\nCurrently it's being hosted on the KDE Subversion server pending freedesktop.org bureaucracy."
    author: "Thiago Macieira"
  - subject: "Re: Qt bindings"
    date: 2006-01-06
    body: "Heh, well with SVN you can stick external SVN sources into the tree. That would be a clean way to do it. Having stuff in SVN that requires manually syncing seems kind of in defiance of the point of downloading from SVN... to get the most-up-to-date."
    author: "Ian Monroe"
  - subject: "Re: Qt bindings"
    date: 2006-01-06
    body: "> Having stuff in SVN that requires manually syncing seems kind of in defiance of the point of downloading from SVN\n\nActually not for those external libs. Our stuff often doesn't work with the in-development versions of those libs (e.g. HAL/DBUS and media kioslave).\n\nSo having a snapshot of those libs in our SVN that is known to work is IMHO a good idea."
    author: "Christian Loose"
  - subject: "KMail"
    date: 2006-01-05
    body: "Does this mean kde4's kmail will not try to download my e-mail anymore when I unplug the cable (And WILL download it again when I plug it in)? Same for akregator, ... ?"
    author: "Bruno"
  - subject: "Re: KMail"
    date: 2006-01-05
    body: "Yes I believe that this is the general idea. Any cable which you can poll for connection status would encourage this type behavior.\n\nEg. if you connect a beamer to your laptop Solid would detect this. As you usualy give presentations on beamers KDE might directly open up KPresenter on that screen or Impress. KOffice would be a sane default but OO.o might be selected by quite a few people. And when you disconnect the beamer then your Kpresenter would minimize on the standard screen. Of course if you enjoy watching movies on large screens on of the kde movie players might open full screen. Sane defaults unlimited customization.\n\nThis is something that would capture the bussiness scene by storm. The amount of time that people look stupid on any desktop os trying to get a beamer to work for presentations is huge!\n\n"
    author: "jerven"
  - subject: "Re: KMail"
    date: 2006-01-05
    body: "yes.\n\nthe kmail developers could of course offer this functionallity NOW, but they would have to do lots of work to support the various distributions and other unixes. with solid, it would be easy. so it is much more likely they will do it..."
    author: "superstoned"
  - subject: "KMail network-aware"
    date: 2006-01-05
    body: "Hi,\nit is already now possible to have kmail network-aware. All you need is a working hotplug/ifplugd installation, then you can put in /etc/network/interfaces:\n\niface eth0 inet dhcp\n        down dcop --all-users --all-sessions kmail KMailIface stopNetworkJobs\n        up sleep 5; dcop --all-users --all-sessions kmail KMailIface resumeNetworkJobs\n\nOf course, you need Debian to have this comfort ;)\n\nIneiti"
    author: "Ineiti"
  - subject: "I love you guys"
    date: 2006-01-05
    body: "That is all :)"
    author: "Janne"
  - subject: "Suse plugger"
    date: 2006-01-05
    body: "Hope Suse also will contribute code.\n\nI would like to see a real YaSt integration into KDE, so other distributions can benefit from the great progress Suse provided us."
    author: "Gerd"
  - subject: "Re: Suse plugger"
    date: 2006-01-05
    body: "Please, do keep in mind that not everybody likes SuSE's YaST.\n\nI always prefer plain vi for editing my config files, and don't like graphical configuration utilities for things more serious than desktop theme or favorite screensaver selection. KControl provides that and only that and that is sufficient. Also, I don't believe that I am the only one who thinks that way.\n\nBtw, YaST is open-source and it is written in Qt (or has Qt backend if you prefer to say that way). I don't see any problem in integrating YaST into any distribution that wants it.\n\nBrcha"
    author: "Filip Brcic"
  - subject: "Re: Suse plugger"
    date: 2006-01-05
    body: "I do agree, YaST to me is somewhat of a pain. What I dislike most about it is that it reads the config files rather than checking the actual status of say a device. What good is a tool, which is nothing more than a graphical frontend to configuration? KControl in that case is sufficient, most distros offer their own config tools which are way better, no YaST for me please. But hey, we're all using open operating systems, some Suse Linux, some FreeBSD, some might even be using something more exotic. So there, please yourself. As for solid, sure it sounds cool. Let's see what it turns out to be when we're there."
    author: "Ron"
  - subject: "Re: Suse plugger"
    date: 2006-01-05
    body: "Integrating YaST more into KDE does not make it distribution independent. \nThe underlying technology of YaST is the key here: can it be easily used on other platforms, or is it tied to the structure of SUSE?"
    author: "rinse"
  - subject: "Re: Suse plugger"
    date: 2006-01-05
    body: "mostly tied to suse, i'm sure. some debian devs are trying to port it to debian, but it seems to be a lot of work...\n\nsuse DOES contribute to KDE, a lot, but not really with yast. KDE shouldn't even think about integrating something like Yast in KDE, imho (and they're smart: they don't). if Solid is here, there might be some work based on it to configure stuff, and that's great. but until then - the distro's are responsible for configuration of hardware and the underlying system. Suse does it great, with Yast, and most other distro's like mandriva have excelent tools, too."
    author: "superstoned"
  - subject: "Re: Suse plugger"
    date: 2006-01-05
    body: "..And we have Guidance for platform independent configurations :)"
    author: "rinse"
  - subject: "Sounds Cards"
    date: 2006-01-05
    body: "This would be perfect for exposing all of the sound cards installed in a computer!\n\nLike (I'm sure) lots of other KDE users, I have more than one sound card installed. So everytime I install a new multimedia application, I have to spend loads of time configuring apps to use the right sound card (which also involves working out the device name of the second soundcard).\n\nWith Solid, one could just select the sound card from a nice pulldown list. And there's already code in kmix in kdemultimedia for detecting sound cards on a variety of different systems - which could be moved to Solid.\n"
    author: "David Saxton"
  - subject: "Re: Sounds Cards"
    date: 2006-01-05
    body: "Jep , Many people have onboard sound , and one (good) normal soundcard "
    author: "chri"
  - subject: "Exactly What's Needed"
    date: 2006-01-05
    body: "I've got to hand it to you all. This is exactly what's needed, and you're 'making it happen'."
    author: "Segedunum"
  - subject: "I love Jackit, arts, and other sound daemons."
    date: 2006-01-11
    body: "I would like to see NON-KDE apps be able to automatically be redirected \"hijack the audio stream) to artsd or jackd without having to use a artsdsp program: Arts and Jackit are great programs, but are often overlooked because people do not like the concept of abstraction. The great thing about them is the redirection of audio input/output which I use for recording all the time. Infact, when both daemons are working well it seems to be more effective than ASIO streams in windows.\n\nArtsd also needs to have some sort of automatic buffer/latency optimization built in, because most of the problem with Arts has to do with system load vs. buffer size.\n\n-Ron "
    author: "paperclip"
  - subject: "Re: I love Jackit, arts, and other sound daemons."
    date: 2007-06-29
    body: "Phonon will replace aRts and use whatever sound daemon you want."
    author: "riddle"
  - subject: "Device driver API?"
    date: 2006-01-12
    body: "I was wondering about device API's.. Right now, the Kopete webcam functions use the Video4Linux API of the kernel directly. Will Solid also provide abstraction to handle such devices with a nice KDE-style API?"
    author: "Diederik van der Boor"
  - subject: "fewe"
    date: 2007-01-03
    body: "fewfwe\nhttp://www.google.com/search?hl=en&q=cat&lr="
    author: "fef"
  - subject: "Re: fewe"
    date: 2007-01-03
    body: "<a href=\"\">dfgdg</a>"
    author: "fef"
---
After a lot of hacking behind the scenes, a new initiative to improve KDE's interaction with network and hardware devices has been launched. <a href="http://solid.kde.org">Solid</a> will provide a robust basis for the dynamic modern desktop in KDE, which needs to be aware of available hardware and networks, paving the way for innovative functionality. Users should see  KDE applications taking advantage of Solid in KDE 4, from the most basic <a href="http://plasma.kde.org">Plasma</a> applets and complex applications to desktop-wide awareness. Developers will be able to take advantage of a robust, flexible and portable API and will be integrated into the Plasma engine.  It will make use of existing technologies like HAL. Solid will also include a <a href="http://solid.kde.org/cms/1072">knowledge base</a> providing a way for users to easily provide feedback on incorrect behaviour.



<!--break-->
<p>Nobody can be certain yet as to how Solid will be used by developers, but according to <a href="http://people.kde.nl/kevin.html">K&eacute;vin Ottens</a>, Solid project lead, "<em>Solid will be a giant leap for KDE. For example, the desktop will be able to deal wisely with your computer hibernating. You'd want network interfaces to go down and for network-enabled applications to gracefully handle the disconnection; USB devices should be synced to avoid data loss</em>". In the long run we can even imagine turning on a bluetooth video screen and having every multimedia application provide you with the option of remote playback.</p>

<p>Not all of the ideas described so far will make it into KDE 4.0, but with more developers for the APIs, the Plasma engine and the knowledge base web site, much more can be achieved. Application developers are also encouraged to experiment with Solid, which will help it mature faster. If you would like to get involved in this exciting area of the free desktop visit the Solid web site and <a href="http://solid.kde.org/cms/1086">get in touch</a> with <a href="http://solid.kde.org/cms/1009">the team</a>.</p>



