---
title: "KDE and Business: Intevation Interview"
date:    2006-03-11
authors:
  - "wolson"
slug:    kde-and-business-intevation-interview
comments:
  - subject: "Typos"
    date: 2006-03-11
    body: "\u00c4ypten -> \u00c4gypten (Egypt!)\nOsnabrck -> Osnabr\u00fcck"
    author: "MW"
  - subject: "Re: Typos"
    date: 2006-03-11
    body: "And question \"Are there other elements of Intevation work in the KDE community that should have more visibility?\" is not highlighted.\n\nThanks for the good interview."
    author: "sgiessl"
  - subject: "Re: Typos"
    date: 2006-03-12
    body: "Regarding public geodata\n\nhttp://rejectinspire.publicgeodata.org/"
    author: "And"
---
In the first in a series of articles, KDE Dot News will cover businesses with past and present involvement that are vital to KDE's ongoing success.  Today, we interview Bernhard Reiter and Bernhard Herzog of <a href="http://www.intevation.net/">Intevation</a>, a company
that has long been helpful with KDE application development and quality
assurance.



<!--break-->
<h2>Past</h2>

<p><strong>Please introduce yourselves</strong></p>

<p><strong>Bernhard Reiter (ber):</strong> I co-own and manage Intevation 
together with my friends Jan-Oliver Wagner and Frank Koormann.
The company allows me to spend work-time on general Free Software topics
while it also is completely focused on Free Software.</p>

<p>I have been Vice-President of the <a href="http://www.ffii.org/">FFII</a> in the past and currently I coordinate the activities of the <a href="http://www.fsfeurope.org/">FSFE</a> in Germany.</p>

<p>Also I am a member of the <a href="http://www.grass-verein.de/">German Grass User Association</a> and 
active within the <a href="http://www.linuxverband.de/">German Free Software Business Association</a>.
My education is in Applied Systems Science (German University Diplom)
and in Geography (Master of Science from University Wisconsin-Milwaukee).</p>

<p><strong>Bernhard Herzog (bh):</strong> I'm an employee at Intevation.  My job title is basically "Lead Developer".</p>

<p>My educational background is physics with a "Diplom" from a German
university.  I have been programming for more than 20 years now,
starting with Basic and assembler on 1980s home computers, later C++ and
C and now mostly Python and C.</p>

<p>In the Free Software community I am probably best known for my 
Free Software vector drawing program <a href="http://skencil.org/">Skencil</a>.


<p><strong>What were the initial reasons to begin Intevation and what were the
initial goals?</strong></p>

<p><strong>ber:</strong> Being system scientists, the three founders could easily see that
computers were becoming increasingly important, 
both for society and business productivity. 
Having endured a lot of bad software without apparent technical reasons,
we understood that Free Software has advantages for 
users and producers alike.</p>

<p>Jan, Frank and I wanted to do better software 
and founded Intevation in late 1999.
We had polished the idea in the years before.
The aim was: provide service and software to customers 
that we would want to buy and use ourselves.
To reach this goal, we completely focused on Free Software,
new development methods, strategic consulting and usability design.</p>

<p>Because we all had software project experience in this area,
our first projects were in the area of <a href="http://great-er.intevation.org/">GIS and scientific modelling</a>.
We also founded <a href="www.freegis.org">Free GIS</a>, to show that it is possible to
strategically progress one area, by providing and overview.</p>

<p><strong>How did you join Intevation?</strong></p>

<p><strong>bh:</strong> I had exchanged a few emails with Bernhard R because of some bug in
Skencil in 1999, if I recall correctly.  I later met him at the LinuxTag
2000 in Stuttgart.  As it turned out, Intevation was looking for a
software developer, I happened to be looking for a job and a few months
later I was hired to be the first employee of Intevation.</p>


<p><strong>At what point did Intevation begin involvement with KDE and what was the reasoning?</strong></p>

<p><strong>ber:</strong> In 2001 we won a tender by the German Federal Agency for IT-Security (BSI) to create a Free Software email solution that could do signatures
and encryption according to their standards (an S/MIME subset).
We teamed up with g10code and Klar&auml;vdalens Datakonsult for the bid
and implemented this within GnuPG, mutt and KMail as this was what
the BSI liked. This is documented on the <a href="http://www.gnupg.org/aegypten/">&Auml;ypten</a> webpage.</p>


<p><strong>What were early results of KDE interaction and why did you continue with KDE?</strong></p>

<p><strong>ber:</strong> The project was a success and we got a couple of other contracts
with the BSI and other customers afterwards.
There was a followup on the &Auml;ypten project and the contracts
which led us to create <a href="http://www.kolab.org/">Kolab</a>, which is an email and groupware solution
with a native, smart KDE client.</p>

<p><strong>What was the inception and growth of the KDE PIM meetings in Osnabrck.</strong></p>

<p><strong>ber:</strong> During the first contract for Kolab, we invited KDE PIM developers
to Osnabrck to improve communication and the technical integration
of &Auml;ypten and Kolab. </p>

<p>I remember some KDE developers which were not in our team being sceptical. 
It was very good that the BSI allowed us to talk to the developers 
during the implementation of the technology.</p>

<p>So the meeting actually had a schedule and additionally I offered
to help as a moderator to best use the time close together talking,
not just quietly hacking.</p>

<p>Some developers found it strange to "close the laptops" for the meeting part :)</p>

<p>Obviously the group liked the meeting, thus we did it again.
And the third and fourth time we got approached by the KDE PIM team
to provide space and bandwidth for a meeting, and we were glad doing this.
While the first two meetings were to some extent paid by our contracts, 
the developers themself carried 
most of the costs for the last two summits.</p>

<p>And they find it very productive to talk together now.</p>


<h2>Present</h2>

<p><strong>What's your current relationship and focus with the KDE community?</strong></p>

<p><strong>ber:</strong> We continue to maintain the Kolab and &Auml;ypten solutions for our customers,
so whenever we need to fix a bug or add a feature in the KDE part, 
we are making sure that it gets back to the mainstream development.
This means testing Kontact and its components a lot
and verifying bug fixes.</p>

<p><strong>bh:</strong> We also build binary packages of KDEPIM and related non-KDE components
such as the GnuPG-based crypto backend for our customers and the
GNU/Linux distributions they use.  While the current KDE releases come
with most of that out of the box, sometimes the customers need features
that are not enabled by default or the version that comes with the
distribution is a bit too old.  These packages also see some quality control, 
especially the parts that are important for Kolab and &Auml;ypten,
which is something that the distributor will likely not test as thoroughly, 
if at all.</p>

<p><strong>How do you feel that your business benefits KDE 
   and how does KDE benefit your business?</strong></p>

<p><strong>bh:</strong> As Bernhard said, if we make changes to Free Software we use we try to
give them back to the project in question so that it will be included in
their official versions.  This is not always easy, but in the case of
KDE it works pretty well, because such changes are made by the
well-known KDE hackers at Klar&auml;vdalens so that the changes are done in
a way acceptable to KDE from the start.</p>

<p>Also, our customers pay us for those changes.  That means they are
features and bug fixes that are really needed by businesses and other
organisations.  They give us an insight into what they need and how they
work with e.g. Kontact. This will make KDE a better system.</p>


<p><strong>What were the topics at this year's KDEPIM meeting in Osnabrck [*]
   and what did you hope to accomplish?</strong></p>

<p><strong>ber:</strong> The meeting is completely there for the KDEPIM developers.
We hope that they enjoyed their stay in Osnabrck and reached their goals.
One goal was a vision for the PIM framework in KDE 4.</p>

<p>In the end of course, everybody will profit from the higher quality software;
and everybody includes Intevation.</p>

<p>Intevation is known for KDE PIM and for your help with quality assurance 
   and testing. Are there other elements of Intevation work 
   in the KDE community that should have more visibility?</p>

<p><strong>ber:</strong> During the time I coordinated the &Auml;ypten and Kolab contracts,
I have noticed that this work did not get much recognition within KDE.
These contracts made it possible to put excellent design in practise,
e.g. the new groupware approach by Martin Konold from erfrakon and
a modular security backend by Werner Koch from g10code.
A lot code work for KDE was done, e.g. by Till Adam, David Faure and Bo Thorsen 
from Klar&auml;vdalens Datakonsult.
As coordinator I influenced what was happening.
And I kept the project going which meant more value for KDE.</p>

<p>When seen from a wider angle: Intevation doing this contract is 
like building a bridge between corporate users and the KDE projects.</p>

<p>KDE is part of the Free Software community and Intevation 
does a lot to promote this community in general.
We support and participate in quite a number of organisations.
Early on I mentioned some. For example
within the LinuxVerband we, among others, made sure that KDE remains 
an option for the Free Software business desktop.
Also, Intevation's yearly award for achievements of Free Software 
at the University of Osnabrck went to Carsten Niehaus 
for the KDE application Kalzium, a few weeks ago.</p>

<p><strong>Any thoughts on recent KDEPIM events such as <a href="http://www.opensync.org/">OpenSync</a>?</strong></p>

<p><strong>bh:</strong> Collaboration with other projects on infrastructure that is not KDE specific 
is always a good idea.  Having recently tested KPilot with
regard to syncs between various Palms and Kontact -- functionality that
several of our Kolab customers want -- I can see that the basic
features are there but there is clearly room for improvements.  </p>

<p>Shared infrastructure like OpenSync can help with this because it will increase
the number of people working on it with various devices.</p>

<p><strong>Are you satisfied with your involvement with KDE to date?  Are there things
that you've learned for working with the Community?  Things that you
hope KDE has learned from working with giving companies like Intevation?</strong></p>

<p><strong>ber:</strong> Naturally, we would be happy to have more customers with KDE related work.
Also we consider ourselves part of the Community of KDE and Free Software.
The KDE project is unique in that it is quite huge and managed
to produce a lot of good applications with C++, which was surprising to me.
But as I learnt from David Faure, Qt helps a lot to make C++ nicer,
like being more dynamic at runtime.</p>

<p>I think many people in KDE meanwhile realise that
an interest of a company need not clash
with interests of the project. Everybody needs to be honest and
keep the communication going.  It is a special skill
of Intevation to work with the existing communities 
and at the same time reach results for customers.</p>

<h2>Future</h2>

<p><strong>Does Intevation plan on future KDE involvement?</strong></p>

<p><strong>ber:</strong>
As a service oriented company, we depend on our customers.
We will continue to support the KDE Kolab client with &Auml;ypten functionality
for quite a while, I think, as customers want our support and are satisfied 
with it. </p>

<p>With &Auml;ypten and Kolab, Intevation has shown that we can
create innovative solutions with KDE and integrate the right partners.
This qualifies us for other IT challenges.
Somebody just has to contract us.</p>

<p>One specific project we aim for is a KDE Kolab Client on Windows.
(If you are an organisation willing to fund this, let us know!)</p>


<p><strong>Does Intevation have any special plans for applications or testing efforts      
on anticipated KDE 4.0 release?</strong></p>

<p><strong>ber:</strong>
Not yet, as customers do not think about deploying KDE 4 at this stage.</p>

<p><strong>What can KDE do to improve working with Intevation or other businesses?</strong></p>

<p><strong>ber:</strong>
It is important to keep in mind when communicating with us or other companies
we usually see different problems affecting hundreds of users. </p>

<p>So our feedback brings in a different perspective.</p>

<p>As I am basically selling KDE to some organisations, I can see high level
arguments which are unrelated to code, but still are important for KDE,
if it wants keep the critical mass as a mainstream desktop environment.
KDE has gotten behind in corporate mindshare because company people had
no one to talk to. Communication needs to be more structured for companies
to interface with KDE efficiently.</p>

<p>I think that the idea of the technical working group is a good start.</p>

<p><strong>bh:</strong>
From a more technical point of view, especially testing and packaging, I
can see two things that would make our job easier.  One is the packaging
of the translations, the other is quality control.</p>

<p>KDE has one repository for all translations for all parts of KDE.  This
invariably leads to distributors having one package for each language
that contains the translations for all of the KDE packages.  When we
package kdepim we usually have to provide at least German translations
and that means we have to produce a kde-i18n-de package tailored to the
customers Linux distribution because it also has to contain the
translations for all the other parts of KDE.</p>

<p>As for quality control, I would like to see KDE adopt more automatic
tests.  KDEPIM for instance contains some tests, but they cover only a
small part of the functionality.  During the &Auml;ypten and Kolab projects
it happened occasionally that bugs that were already fixed were
introduced again later.  At least some of these could have been
prevented with automatic tests.  KDE seems to be improving in this area,
though.  The move to Qt4 could help with this, too, because it
emphasises the Model-View-Controller pattern which helps to separate the
program logic from the user interface.</p>




