---
title: "First Issue of Amarok Weekly Newsletter Released"
date:    2006-11-16
authors:
  - "shiny"
slug:    first-issue-amarok-weekly-newsletter-released
comments:
  - subject: "YAY"
    date: 2006-11-16
    body: "finally Amarok has a newsletter, keep up the Good work!"
    author: "TheFuzzball"
  - subject: "isn't it too much?"
    date: 2006-11-16
    body: "I am wondering if there is enough stuff to write about every week about amarok.\nIn my opinion a KDE Weekly Newsletter would be a better place to write about amarok, so the topics would not be too slight due to fill the newsletter.\n\nBut I will read it at least for some time to look where it it going. :)"
    author: "Fred"
  - subject: "Re: isn't it too much?"
    date: 2006-11-16
    body: "No worries, I don't think this will really turn out into a weekly thing. Probably more like monthly. It's perhaps a bit of a misnomer."
    author: "Mark Kretschmann"
  - subject: "Re: isn't it too much?"
    date: 2006-11-16
    body: "I hope it doesn't become like the Wine weekly newsletter though... which currently  is three months late... x-) well, at least they're getting lots of work done in the meantime."
    author: "Darkelve"
  - subject: "Re: isn't it too much?"
    date: 2006-11-16
    body: "I promise you it won't become boring. As Mark said, it doesn't really have to be weekly. It will depend on amount of happenings and spare time of editors. But I still think there will be lots of things to write about: new topics, continuation of old interesting topics, official Amarok announcements (like bug squashing day, call for donations or artists, new release announcements, etc.. ), maybe some interesting irc and forum topics. You know, newsletter is not _that_ long. :)"
    author: "Ljubomir"
  - subject: "Re: isn't it too much?"
    date: 2006-11-16
    body: "Yes, but if it's not weekly, why do you call it weekly?\n\nThat's pretty, erm, stupid and misleading in my opinion! o.O"
    author: "fish"
  - subject: "Re: isn't it too much?"
    date: 2006-11-17
    body: "If it won't come out weekly, I'll stop calling it weekly. But for now it's weekly :). We'll see."
    author: "Ljubomir"
  - subject: "free music"
    date: 2006-11-16
    body: "I am wondering whether it makes sense to ship Amarok with a small collection of Free Music."
    author: "gent"
  - subject: "Re: free music"
    date: 2006-11-16
    body: "Well we did that for the Live CDs. \n\nBut it seems like a good idea for the distros to include one or two local tracks."
    author: "Ian Monroe"
  - subject: "Re: free music"
    date: 2006-11-20
    body: "Nice idea, but no one would like it.\n\nYou simply cannot choose a selection of music which no one will find utterly irritating. There's no accounting for taste.\n\nIt would be nice if there was a free music download site though. Something that could be integrated like Magnatunes, but free. There is enough free music in existence for this to work OK, but I don't know how the bandwidth would get paid for. I suppose you could have a DemocracyPlayer-style bittorrent system, but then things are getting a bit overcomplex..."
    author: "BCMM"
  - subject: "rss feed"
    date: 2006-11-17
    body: "Hello to everybody!\n\nI really love Amarok, and I think that the newsletter is a great way to know what's happening in Amarok's development (I mean, more in depth than in the Changelog from svn).\nI have just visited the newsletter site and just thought that given that there are links such as http://ljubomir.simin.googlepages.com/awnissue1#store which refer to a particular article in the newsletter, it wouldn't be very difficult to add a rss feed thing. Am I right? I don't know much about that but I've seen a rss xml file and it is just a list of url's with a \"user friendly\" name for them.\nIt would be really cool and easier for users to know when a new issue of the newsletter is out and if it is really interesting to them.\n\nBest regards, Jos\u00e9"
    author: "Jos\u00e9 Su\u00e1rez"
  - subject: "Re: rss feed"
    date: 2006-11-19
    body: "Yup, but all articles belong to one issue of newsletter (and physically to one HTML file), so I see no sense in RSS feed, unless it lists the newsletter itself, but then why RSS?. How do you imagine it?"
    author: "Ljubomir"
  - subject: "Re: rss feed"
    date: 2006-11-27
    body: "This RSS would only inform about new release of AmarokWeaklyNewsletter. So when I see thet RSS is updated I will go to the site and read AWN. :)\n\nI was thinking about the same when I posted this -> http://amarok.kde.org/forum/index.php/topic,13361.0.html\n\n"
    author: "patpi"
  - subject: "KsCD"
    date: 2006-11-18
    body: "I would love to see some way to make KsCD obsolete via Amarok. I tested numerous Installations on different PCs over the years and what annoyed me most was KsCD, because it only played CDs after magic configuration trial and error. However, no problem to play the same CD with Kaffeine. \n\nThese misconfigurations are real show stoppers of Linux, they annoy users who think their soundcard is not properly detected etc.\n\nAnd KsCD looks so ugly. Yes, I want the functionality to get merged with Amarok. iTunes is a good example how to get it right. Esp. the import function of iTunes is usability king. "
    author: "Heiner"
  - subject: "Re: KsCD"
    date: 2006-11-18
    body: "Well, the Amarok 1.4 series is playing AudioCD's just fine, using xinelib. (Including automatic CDDB lookup)"
    author: "Mark Kretschmann"
  - subject: "Re: KsCD"
    date: 2006-11-18
    body: "I am using Kubuntu 6.06 and you see KDE glitches all over the place.\n\nWhat I really would like to see is a KsCD-free distribution.\n\nI am very glad that a Mac OS X style topmenu works but unfortunately only for real KDE applications including Amarok. But even QT applications and OpenOffice are badly integrated. These are small glitches I experienced in the first 5 hours and they left a very bad impression. Add to that MPs is not played but Amarok does not tell you why or help you to solve the problem..."
    author: "Heiner"
  - subject: "Re: KsCD"
    date: 2006-11-19
    body: "And you do release that Kubuntu 6.10 has been released; those issues might have been corrected already :)"
    author: "Kaiwai"
  - subject: "Your confusing KDE to do things it is not"
    date: 2006-11-19
    body: "Hello,\n\ndo you realize that QT applications and OpenOffice are not KDE programs and as such naturally not well integrated? Everything works fine as long as you are using KDE programs. KDE can hardly change how other programs display their menus just to give an example. BTW: OpenOffice is hardly more than a stop gap until KOffice works well enough. For me it does many cases, but I know it does not for everybody yet. Although it will some day maybe.\n\nAnd the lack of replay for MP3, is hardly a KDE problem, but something nobody can legally distribute without paying royalty to Fraunhofer Institute. When you \"bought\" Kubuntu, who did you expect to give some of your money to them? \n\nAnd as far as I know, Amarok is not even a KDE program. Certainly not one that KDE the project controls. But that doesn't matter in this, Amarok doesn't deal itself with the MP3 decoding, but only gstreamer or xine, or whatever backend it is. When these got ripped out MP3 support by Kubuntu, what is Amarok to deal with it?\n\nYours, \nKay\n\n"
    author: "Debian User"
  - subject: "Re: KsCD"
    date: 2006-11-19
    body: "KsCD is handy in that rare case when you have a lot of CD and none of media files.\nBut it obviously could go to kde-extra or something.\n\nIn new Amarok in Kubuntu (1.4 ) there is a pop-up that explains how to obtain MP3 support, so that is fixed.\n"
    author: "pilpilon"
  - subject: "Re: KsCD"
    date: 2006-11-20
    body: "Use the audiocd:/ kio-slave in Konqueror.  Also in the newer versions of Amarok theres built-in support for CDs."
    author: "Corbin"
---
In the <a href="http://ljubomir.simin.googlepages.com/awnissue1">first issue of the Amarok Weekly Newsletter</a>, we talk about <a href="http://magnatune.com">Magnatune.com music store</a> integration and security, search inside lyrics, a new <a href="http://gstreamer.org/">GStreamer</a>-based engine, support for user-definable labels and promotional activities. Enjoy!



<!--break-->
