---
title: "KDE e.V. Quarterly Report 2005Q4 Released"
date:    2006-02-15
authors:
  - "jriddell"
slug:    kde-ev-quarterly-report-2005q4-released
comments:
  - subject: "kpdf"
    date: 2006-02-16
    body: "links are not clickable on kpdf with kde 3.5.1"
    author: "Patcito"
  - subject: "Re: kpdf"
    date: 2006-02-16
    body: "Offtopic, this.\n\nBut they are! Worksforme. Using packages provided by SUSE. (You are required to have a PDF that does indeed feature such links. Do you have?)"
    author: "anonymous coward"
  - subject: "links not clickable in KDE 3.5.1 Debian Sid"
    date: 2006-02-16
    body: "Tested both PDFs with KPDF, Xpdf, Acrobat reader. Either they are just blue highlighted text or the tool used to produce the PDF (in this case GNU Ghostscript 7.07) stripped out the links."
    author: "Marc J. Driftmeyer"
  - subject: "Re: links not clickable in KDE 3.5.1 Debian Sid"
    date: 2006-02-16
    body: "that's cool then I guess it's just bad generation, not kpdf's fault. They should have used Scribus to generate the pdf :)"
    author: "Patcito"
  - subject: "Why not change \"e.V.\" to \"Foundation\"?"
    date: 2006-02-16
    body: "The \"e.V.\" part of the name sounds a little strange, and it doesn't follow any convention of standard English. For example, why is V capitalized and not e?\n\nI think this is important from a fundraising and publicity standpoint -- potential donors are much more accustomed to giving to \"Foundations\" than to \"e.V.'s\". They might be worried that a donation to an \"e.V.\" organization is not charitable and tax-exempt.\n\nThe \"e.V.\" is also a stumbling block when you're trying to make KDE a household word. Just imagine how journalists will butcher it when trying to quote the official KDE representatives!"
    author: "ac"
  - subject: "Re: Why not change \"e.V.\" to \"Foundation\"?"
    date: 2006-02-16
    body: "\"e. V.\" is a legal German appendix for registered associations (\"eingetragener Verein\"). It applies to KDE e.V. since it's registered in Germany. KDE e.V. is supposed to work in the background and not to become a \"household word\", so I don't know why you make any fuss about its name."
    author: "ac"
  - subject: "Re: Why not change \"e.V.\" to \"Foundation\"?"
    date: 2006-02-18
    body: "In fact, in Germany it is not uncommon to look for the \"e.V.\" before donating any money to an organisation. The \"e.V.\" ensures charity and social engagement and also monitors a governmental aid for the organisation (mostly tax reduction). So maybe it's strange for non-germans but any german will agree that you have to add it to the name - especially for marketing issues! \n\n"
    author: "Waldi"
  - subject: "Re: Why not change \"e.V.\" to \"Foundation\"?"
    date: 2006-02-18
    body: "No, the \"e.V.\" does not ensure charity, that's only optional. Take the ADAC, biggest german car drivers association, they are an \"e.V.\", too. And charity is not, what they are known for, and do. If you do charity with an e.V., you have to apply for this status with the governmental structures, to get the financial aid."
    author: "KDE User"
  - subject: "On becoming a KDE e.V. member"
    date: 2006-02-20
    body: "I would like to be one but does it require travelling to KDE e.V. meetings and aKademy to make a meaningful contribution?  It looks like a lot of decisions are made in person rather than over the internet."
    author: "a kde developer"
  - subject: "Re: On becoming a KDE e.V. member"
    date: 2006-02-22
    body: "Some decisions are taken online, some are taken in person during meetings. The most important of those is the General Assembly, which takes place once a year and coincides with aKademy.\n\nActive members are required to participate and are required to attend at least one General Assembly every 3 years. You can find all that information in the KDE e.V. statutes: http://ev.kde.org/corporate/statutes.php.\n\nIf you want to become a member, see this: http://ev.kde.org/members.php"
    author: "Thiago Macieira"
---
KDE e.V. has released its <a href="http://ev.kde.org/reports/ev-quarterly-2005Q4.pdf">second quarterly report [PDF]</a> covering the activities of KDE's legal body for the last three months.  This quarter saw the first meeting of the new KDE e.V. board and of course the creation of the <a href="http://dot.kde.org/1139614608/">Technical Working Group</a>.  You can also find the <a href="http://ev.kde.org/reports/ev-quarterly-2005Q3.pdf">first quarterly report [PDF]</a> from October 2005 which includes a financial report.  All long term contributors to KDE are encouraged to <a href="http://ev.kde.org/members.php">become KDE e.V. members</a>.



<!--break-->
