---
title: "People Behind KDE: Frans Englich"
date:    2006-04-22
authors:
  - "tbaarda"
slug:    people-behind-kde-frans-englich
comments:
  - subject: "Hmm"
    date: 2006-04-23
    body: "Frans Englich = Understatement incarnated"
    author: "Hen Tai"
  - subject: "Looking into the future..."
    date: 2006-04-23
    body: "I really enjoyed the interview and Frans reinforced some ideas that I have been maturing for a while. KDE has a wonderful technical foundation and excels at many things, but it would be great if even more emphasis was placed on completeness of software and documentation as well as on regression testing.\n\nThis is why I am also both very trouble and excited by KDE 4.0. While some of the promised features seem amazing, I have no faith whatsoever in re-writes. Joel Spolsky, has written extensively on this topic as have others.\n\nI have an incredibly reliable and useful desktop today with KDE 3.5 and I can only think that KDE might have jumped the gun a little early with KDE 4.0. I think 4.0 should have remained a long term project, with the bulk of development focused on fit and polish of the existing KDE desktop and applications. \n\nWe are within very short distance of the promised land with KDE 3.5 and KDE 4.0, as appealing as it sounds, may prove too big a distraction. My feeling is that it will not be in a usable state, i.e, something which I can deploy widely and confidently until at least early or mid 2008. That is a long time in software engineering terms and I fear that this may damage KDE's long term prospects by dint of reduced mindshare to both our Gnome friends and Microsoft's Vista, because as bad as that may turned out to be, Microsoft's marketing dollars might be able to do enough damage that we may not get another opportunity till 2010.\n\nOf course, I have no moral authority to tell a group of my favorite developers what they must work on, other than the weight of my arguments and feedback that is grounded in a profound appreciation of their work."
    author: "Gonzalo"
  - subject: "Re: Looking into the future..."
    date: 2006-04-23
    body: "well, the work on KDE definitely has some disadvantages. on the other hand, if it goes well, many of the problems you mention will be fixed by 'just' switching to KDE 4. KDE 4 is a huge step forward, and i think they shouldn't wait."
    author: "superstoned"
  - subject: "Re: Looking into the future..."
    date: 2006-04-24
    body: "And the great thing about KDE's open source/free software nature is that, if you think something should be done, you have the code, the documentation and the community to help you do it.\n\nInterested in testing? Brad Hards has written a tutorial on using Qt's QTestLib class to create unit tests for KDE 4 libraries (every class in KDE 4's kdelibs should have a unit test): http://developer.kde.org/documentation/tutorials/writingunittests/writingunittests.html\n\nInterested in helping squash bugs in stable versions? Bug triage is a great way to help with that, and you don't need to be a coder to get involved:\nhttp://quality.kde.org/develop/howto/howtobugs.php\n\nDocumentation? The docs team (for end-user docs) are always happy to get more volunteers, or if you prefer writing developer documentation, Adriaan de Groot has a great API documentation howto:http://developer.kde.org/documentation/library/howto.php\n\nSo, there are plenty of ways to get involved and make KDE even better, and to help put emphasis on the things you think are important. If you'd like any help getting started, drop a message to kde-quality@kde.org or to me (phil at kde dot org), and we'll point you in the right direction."
    author: "Philip Rodrigues"
  - subject: "I also feel this way..."
    date: 2006-04-23
    body: "I think there is room (and most possibly a need) for a 3.6 release focused exclusively in new application features and bug fixes/stability improvements of KDE's core.\n\nKDE in the state in which it is today is being deployed in a great variety of places and all that users will need solutions to their problems and little improvements here and there tomorrow, not in a year or more.\n\nKDE 4 sounds really something worth the wait, no doubt; but can KDE community afford to hold its breath for so long? I guess not...\n\nGreetings,\nGabriel\n"
    author: "Gabriel Gazz\u00e1n"
  - subject: "Re: I also feel this way..."
    date: 2006-04-23
    body: "\"3.6 release\"\n\n\nThere seem to have been others who felt like this....\n\nhttp://lists.kde.org/?t=113813458000001&r=3&w=2\n\n....but the decision went against it."
    author: "anonymous coward"
  - subject: "Re: I also feel this way..."
    date: 2006-04-23
    body: "One of the interesting developments in the mailing list link you gave us is that the developers talk about KDE 3.5 code back in January as being \"old code\", when it had been released recently. Well code doesn't really age, but the fact of the matter is that what I consider the major distributions (Suse, Mandriva, Kubuntu, Debian), do not have yet a stable shipping version with the \"old 3.5 code.\"\n\nI must note that in this comment and my earlier one, I speak not as a developer but as an admin in the trenches. \n\nReading the mailing-list, and the comments from the Quanta and Krita developers, it's very hard for me to believe that much effort will go into the maintenance of KDE 3.5 base or apps, which will be what many schools and companies deploy with a 3-5 years of expected usage. I know maintenance isn't sexy or fun, but KDE is being used in the real world where sexy isn't as high a priority as stable and predictable. My fear is that future bugs agaisnt kde 3.x will be rejected as \"won't fix\", update to KDE 4.x, simply because most developers will have moved on and not be interested in the ardous work of maintaining a code base which they have in essence left behind.\n\nTo put this against the right backdrop, I will be running the first release of KDE 4.0 on my laptop, but I know that none of my clients will until it has been sufficiently tested. As I said earlier, I cannot see KDE 4.0 being offered by any \"enterprise\" distribution before 2008.\n\nThis leaves me with the feeling that there won't be much incentive to fix any existing problems. "
    author: "Gonzalo"
  - subject: "Re: I also feel this way..."
    date: 2006-04-23
    body: "\"My fear is that future bugs agaisnt kde 3.x will be rejected as \"won't fix\", update to KDE 4.x\"\n\nIf you look at the past, many bugs found in kde were backported to older version as well. Patches did not only apply on the latest version of kde, but on about 5 previous releases as well (some even went back to kde 2.x).\n\nFurthermore, when using KDE on the enterprise desktop, one might expect that the company uses an enterprise distribution that comes with a 5 year service. So if a bug is found in an enterprise edition of KDE, wich can't be fixed by kde developers, then the company that distributed the enterprise version can fix it.\n(as it is all opensource :)"
    author: "AC"
  - subject: "Re: I also feel this way..."
    date: 2006-04-23
    body: "Yes, 3.5 will become a \"secondary\" branch with \"old code\". In some areas it might not see big changes, but it will certainly won't be abandoned code. For many month to come it will receive bugfixes and minor improvements, but the branch is feature frozen, so don't expect new development there. \n\n(There might be areas that are more or less abandoned, like VPL in Quanta, but even there I try to fix critical bugs, like crashes.)"
    author: "Andras Mantia"
  - subject: "Re: I also feel this way..."
    date: 2006-04-23
    body: "we could continue making kde 3.x releases, but then we'd almost certainly never get kde4 out the door. at some point we need(ed) to make a cut and start work on kde4. there are some architectural issues in kde3 that require breaking binary compatibility and reworking some components, so sticking with the kde3 branch would result in a progressively worse solution as time wears on. so kde3 is now in maintenance mode with only bug fixes and well tested important features making it in while we work on kde4."
    author: "Aaron J. Seigo"
  - subject: "Re: I also feel this way..."
    date: 2006-04-23
    body: "The main problem for a KDE 3.6 would be to find a release manager.\n\nStephan Kulow had told that he would not want to be the one releasing a KDE 3.6. And until now, despite similar discussions for a KDE 3.6 release having happened a few times, nobody (especially anybody with serious enough knowledge) has volunteered.\n\nAdditionally the current situation makes that if there would be any KDE 3.6, it would certainly be released *after* KDE4.\n\nHave a nice day!"
    author: "Nicolas Goutte"
---
Tonight the <a href="http://people.kde.nl/">People Behind KDE</a> interview series brings to you a half-interview with <a href="http://people.kde.nl/englich.html">Frans Englich</a>, only half because he could not find a photo to submit. This man is a KDE developer whose most recent work is on KDOM and XSLT.  Because the <a href="http://commit-digest.org">Commit Digest</a> is back at the weekend, People Behind KDE will be moving to a new mid-week slot from now on.




<!--break-->
