---
title: "Trolltech Releases Qt 4.2 Technology Preview"
date:    2006-07-04
authors:
  - "jriddell"
slug:    trolltech-releases-qt-42-technology-preview
comments:
  - subject: "GNOME"
    date: 2006-07-04
    body: "> including dbus bindings, Glib eventloop support, switchable button order and a Clearlooks-like style.\n\nSounds GNOME to me."
    author: "G-Man"
  - subject: "Re: GNOME"
    date: 2006-07-04
    body: "Yes, isn't it great? Gnome has a long way to go before it has as good support for KDE."
    author: "claes"
  - subject: "Re: GNOME"
    date: 2006-07-04
    body: "Thats the whole point.  Not all KDE & Qt apps are used solely in KDE.  The Glib event loop will allow you to write plugins for GTK apps using Qt/KDE and vice versa.  The switchable button order will make Qt apps look more in place in Gnome (and OS X?), and hopefully Gnome will add a similar feature to make GTK/Gnome apps look more in place on a KDE desktop.  The Clearlooks style also increases the esthetic appeal of Qt apps to Gnome users."
    author: "Corbin"
  - subject: "Re: GNOME"
    date: 2006-07-04
    body: "seems like most cross-desktop integration work comes from KDE and Qt. the Qt-gtk theme offers visual integration, there is a tool which makes Gnome apps use the KDE file dialogs, a project exists to make it easier for non-KDE apps to use KDE technologies, Qt apps can now use the glib eventloop (aRts even depended on glibc) and now Qt apps change their buttonorder and have a Gnome theme... I'd love to see some initatives from the 'other' side."
    author: "superstoned"
  - subject: "Re: GNOME"
    date: 2006-07-04
    body: "How about this: \nKDE reverts to double-clicking, and Gnome changes button order\n"
    author: "claes"
  - subject: "Re: GNOME"
    date: 2006-07-05
    body: "You don't have to change the defaults, just give an option to change the settings.  KDE can already do both single and double clicking, and now Qt 4.2 has the functionality to set the button order to fit with the desktop.  It wouldn't be hard for Gnome to do the same, for the users sake."
    author: "Corbin"
  - subject: "Re: GNOME"
    date: 2006-07-05
    body: "Actually it would be very hard for GNOME to do the same.. They don't have any infrastructure. To change button-order originally they had to change it manually in all applications. \n\nFrom an outside look there doesn't seem to be much push to improve GNOME infrastructure, they aiming at being a collection of random applications forever."
    author: "Carewolf"
  - subject: "Re: GNOME"
    date: 2006-07-05
    body: "Well, they aim for integration, but the hard way: much work to individually redesign every single application so it adheres to their (detailled) standards. Yes, it doesn't get less efficient, but on the other hand - they have big corporate sponsors which seemingly rather pay for incremental enhancements to single applications than invest money in a serious overhaul of the Gnome infrastructure.\n\nI think lots of money are wasted on evolution, OO.o, Firefox and Gnome. This money would be more efficient if it was spend on KDE, and make it possible, in a way even force Gnome to do some serious restructuring on their framework, leading to a better Linux Desktop in general."
    author: "superstoned"
  - subject: "Re: GNOME"
    date: 2006-07-05
    body: "\"To change button-order originally they had to change it manually in all applications.\"\n\nThat, my friend, is because the change was not just as simple as [Ok] [Cancel] changing to [Cancel] [Ok]\n\nFor example if you try to close a modified document in GEdit you get\n[Close without Saving] [Cancel] [Ok]\n\nWhich when you flick it round is [Ok] [Cancel] [Close without Saving] when it should be [Ok] [Close without Saving] [Cancel].\n\nSimply flipping the order of the buttons is easy (and indeed Gtk+ has had an option to do this for the last 2 major releases), but there is more to it than that."
    author: "bob"
  - subject: "Re: GNOME"
    date: 2006-07-05
    body: "Yeah! Just look at qt 4.2 how difficult that is! i doubt the gnome guys can do something like that :P."
    author: "SiberianHotDog"
  - subject: "Re: GNOME"
    date: 2006-07-06
    body: "Let's look at it when it's released."
    author: "Reply"
  - subject: "Re: GNOME"
    date: 2006-07-06
    body: "well, there is the preview release, so you can already look at it now ;). i don't think they are changing much there, at leased not the current functionality."
    author: "SiberianHotDog"
  - subject: "Re: GNOME"
    date: 2006-07-07
    body: "Wasn't gtkmm supposed to be GNOME's version of Qt? At the very least I remember comparisons between the two."
    author: "sundher"
  - subject: "Re: GNOME"
    date: 2006-07-14
    body: "No, gtkmm are the C++ bindings to GTK."
    author: "Andreas Tunek "
  - subject: "Re: GNOME"
    date: 2006-07-05
    body: "GNOME already has these features.  Double and single clicking has always been there, and selectable button order has been in GTK+ for a few years now (since v2.6)."
    author: "Antifud"
  - subject: "Re: GNOME"
    date: 2006-07-05
    body: "> and selectable button order has been in GTK+ for a few years now (since v2.6).\n\nStop demonstrating your lack of clue in the public."
    author: "asdf"
  - subject: "Re: GNOME"
    date: 2006-07-05
    body: "> Stop demonstrating your lack of clue in the public.\n\nObviously... you're not a golfer."
    author: "Antifud"
  - subject: "Re: GNOME"
    date: 2006-07-06
    body: "What is the application that allows Gnome apps to use the KDE File dialogs?"
    author: "Darin Manica"
  - subject: "Re: GNOME"
    date: 2006-07-08
    body: "It's called KGtk: http://www.kde-look.org/content/show.php?content=36077\n\nI like it a lot. I use it to make Firefox use the KDE file picker instead of the GNOME one.\n\nYou'll have to manually compile it though, which means installing a lot of kde-dev packages."
    author: "Elijah Lofgren"
  - subject: "Re: GNOME"
    date: 2006-07-08
    body: "What is the application that allows KDE apps to use the GTK File dialogs?"
    author: "Steve Warsnap"
  - subject: "Re: GNOME"
    date: 2006-07-08
    body: "The Gnome camp hasn't made one yet (nor have they made a kde style that allows you to use the gtk style)."
    author: "Corbin"
  - subject: "Re: GNOME"
    date: 2006-07-05
    body: "> The switchable button order will make Qt apps look more in place in Gnome and OS X?), and hopefully Gnome will add a similar feature to make GTK/Gnome apps look more in place on a KDE desktop.\n\nCurrent GTK+ version already support switchable button order (they did it for Gimp/Windows though)."
    author: "Anonymous"
  - subject: "Re: GNOME"
    date: 2006-07-05
    body: "Switchable button ordering has been in KDE for a while now:\nhttp://wiki.kde.org/tiki-index.php?page=Secret+Config+Settings#id757816"
    author: "ciasa"
  - subject: "Re: GNOME"
    date: 2006-07-07
    body: "By the way, D-BUS isn't GNOME, it was heavily inspired by DCOP, and is a freedesktop.org 'standard'.  Also KDE4's code has almost completely transitioned to dbus from dcop."
    author: "Corbin"
  - subject: "QDesktopServices"
    date: 2006-07-04
    body: "I am so proud :)\nThey named a class after one of my projects"
    author: "Kevin Krammer"
  - subject: "Wish List Item: Font Metrics"
    date: 2006-07-04
    body: "I would still like to see Qt support choosing the font metrics:\n\n1.  Hinted at Screen Resolution (current method)\n\n2.  Hinted at Printer Resolution (Windows compatibility mode)\n\n3.  Not Hinted (PostScript mode)\n\nIs there some reason that this isn't possible?"
    author: "James Richard Tyrer"
  - subject: "Re: Wish List Item: Font Metrics"
    date: 2006-07-04
    body: "Is there some reason you are posting this here?\n\nI mean i can post it in my jobs' internal mailing list and will have almost the same effect, zero.\n\nIf you want to communicate with the trolls use the proper channels."
    author: "Albert Astals Cid"
  - subject: "Re: Wish List Item: Font Metrics"
    date: 2006-07-05
    body: "http://blogs.qtdeveloper.net/archives/2005/12/22/printing-and-wysiwyg/"
    author: "James Richard Tyrer"
  - subject: "Re: Wish List Item: Font Metrics"
    date: 2006-07-05
    body: "> Is there some reason that this isn't possible?\n\nIt is possible."
    author: "Thomas Zander"
  - subject: "QFileSystemWatcher"
    date: 2006-07-04
    body: "A lot (or maybe all?!?!) of the new features in 4.2 look awesome (QGraphicsView for plasma, DBUS support, 'undo framework', etc).  I have a couple questions about the QFileSystemWatcher class (I already looked in the docs but it didn't say...), 1. Does it use inotify when available (it looks like it does, since it supports monitoring files), and 2. if you monitor the directory /foo, and a file is modified in /foo/bar/moo does the signal thats emitted point to the file, or '/foo'?  If it points to the actual file (or if theres a way to make it do that), it would make it extremely easy to make a program I've been working on idly some.\n\nAll in all it looks great, and I would just like to thank all the trolls out there that contributed to making Qt as great as it is today!"
    author: "Corbin"
  - subject: "Re: QFileSystemWatcher"
    date: 2006-07-04
    body: "Looking at the archiving it looks like it DOES use inotify on Linux, though still not sure if you can use it for what I wanted exactly."
    author: "Corbin"
  - subject: "Re: QFileSystemWatcher"
    date: 2006-07-05
    body: "Does it use the NTFS Change Journal on windows? What does it use on MacOSX? I wrote a qt-program that manually interfaces to inotify and ntfs-cj, that was quite some work. Damn you trolls, couldn't you be six months faster?\n\nwell, great work!"
    author: "me"
  - subject: "Qt Company"
    date: 2006-07-05
    body: "Surely, the primary rationale for GNOME integration is probably the fact that most of the Major Distributions (SuSE, Fedora, Ubuntu) are now using GNOME by default and prefer vendors to write Gtk+ applications, for desktop integration purposes.\n\nI imagine Trolltech have recognised this trend and do not wish to discourage potential customers from investing in Qt; integration with the GNOME desktop is critical.\n\nFortunately, this also gives us a greater migration path for people using GNOME that would instead like to use KDE. If KDE continues to be better integrated and provide superior technical solutions, but concentrates on seamless integration with GNOME and improved UI consistency, people will switch back to it. I believe that this is strategically important for KDE."
    author: "Alistair John Strachan"
  - subject: "Re: Qt Company"
    date: 2006-07-05
    body: "Surely, the primary rationale for GNOME integration is probably the fact that most of the Major Distributions (SuSE, Fedora, Ubuntu) are now using GNOME by default...\n\n\nUbuntu is no major distribution.\nSuse is a KDE distribution and Nat Friedman does not decide how users use it. \nFedora is no desktop distribution.\n"
    author: "fumbar"
  - subject: "Re: Qt Company"
    date: 2006-07-05
    body: "LOL I especially enjoyed \"Fedora is no desktop distribution\"\n\nFolks, don't buy the popcorn. There are hundreds of Linux distros and if you take the time to look at all of them I expect you'll still find most offering KDE as the default. However most offer a choice of desktops. User surveys may rate Ubuntu very highly (though there is also Kubuntu) but every reputable survey I've seen continues to favor KDE on the desktop. The most recent LinuxQuestions.org survey, which is probably the best online survey around, put KDE in the decisive lead while demonstrating it is hardly a KDE centric group on many applications. \n\nAs far as toolkit choice goes it's a lot more complex than just what is \"business friendly\". If you have unlimited developers resources, developers using GTK or want to turn around and sell proprietary enhanced versions of open source projects then GTK is an obvious choice. If developer resources are more critical, you have developers using Qt or you are looking to do purely GPL and/or proprietary development tracks Qt is more attractive. If you make a purely technical evaluation or you want to integrate well across all platforms Qt is also a logical choice.\n\nIt is important to realize that in the world of free software, as much as some loud voices may claim otherwise, it is impossible to legislate by fiat what desktop the community will use. Likewise the success of Trolltech indicates that what a few large companies do has no bearing on thousands of companies with different criteria. The coolest thing is that the work done on Qt is awesome and the work we do on KDE 4 will be likewise awesome and is sure to get noticed."
    author: "Eric Laffoon"
  - subject: "Re: Qt Company"
    date: 2006-07-06
    body: "Ubuntu, maybe not yet, but it's the fastest growing. This isn't a perfect metric, but see:\n\nhttp://distrowatch.com/\n\nThe poll on the side. Denying that these distributions are important for KDE is just plain stupid -- we want as many people as possible using KDE, so we have as many developers as possible, and more bug reports, so KDE continues to grow faster. I think ignoring a sizeable potential userbase is a dangerous thing to do.."
    author: "Alistair John Strachan"
  - subject: "Re: Qt Company"
    date: 2006-07-06
    body: "That 'poll' is only a measure of how many people went to the ubuntu page on distrowatch.  I've been to only 2 distro pages on distrowatch and that was to the ubuntu page (I don't use ubuntu), and the SuSE page (I didn't use SuSE for till about 6 months after that).  I will admit that Ubuntu has far more publicity than any other distro out there, but that doesn't make them the most popular, the most used, or the 'best' (for values of 'best' that aren't defined as 'most publicized')."
    author: "Corbin"
  - subject: "Re: Qt Company"
    date: 2006-07-06
    body: "I wouldn't go by distrowatch numbers - they are heavily skewed.\n\nI agree that SUSE is a major distro but draw a distinction - most SUSE users, I am pretty sure, are KDE users. Now, SLED, theone that 'supports' gnome, is different - it is NOT a major distro.\n\nUbuntu has a lot of marketing behind it - some by Shuttleworth, and a lot by gnome people. Some of that marketing is rubbing off but I actually haven't met anyone using ubuntu. In fact, most people even remotely interested in linux haven't heard of it. Ubuntu is NOT a major distro by any reckoning.\n\nFedora IS a major distro, unfortunately. But people switch between fedora and  debian  and perhaps other distros as well. And yes, fedora's KDE support is not optimal. The issue is that RH supports fedora and most of redhat's people work on gnome ( at least it used to be). But RH's gnome doesn't look any better than their KDE, unlike SLED 10.0."
    author: "vm"
  - subject: "Re: Qt Company"
    date: 2006-07-07
    body: "Excuse me? Ubuntu IS a major distro, it is quite widespread and you can check on several fora you'll always find questions about ubuntu, not to mention the ubuntuforums, or the fud that's been spread by debian devs about ubuntu (compare it with what's been said about xandros which doesn't give as much back to debian is ubuntu does). Ok, Kubuntu is a bit the little brother though in my opinion the better distro of the two. You can measure a linux distro with the negativ news and well in this area it is equally mentioned as suse, fedora and mandrake..."
    author: "Terracotta"
  - subject: "Re: Qt Company"
    date: 2006-07-05
    body: "Just like most free (as freedom) projects have to adapt to non-free projects (for example, most GNU projects have to distribute Windows ports, but little Windows projects port to Linux), Qt (as a more free project here because of GNU GPL) has to adapt to Gtk (as a less free project here because of GNU Lesser GPL).  I don't think the number of Gtk projects adapt to Qt can be big."
    author: "less-freedom GTK"
  - subject: "Re: Qt Company"
    date: 2006-07-05
    body: "There is no license restriction preventing the adapting process between LGPL and GPL programs. Note also that the KDE libraries are LGPL. If you want to talk about real obstacles there is the fact of KDE being able to load an X program as a KPart while there is no GNOME counterpart because there is no complementary technology available to adapt. At least with DBUS there is finally the potential for an IPC between desktop applications."
    author: "Eric Laffoon"
  - subject: "Re: Qt Company"
    date: 2006-07-10
    body: "What major distributions? Suse isn't. Fedora isn't. Ubuntu isn't. They represent a miniscule amount of users.\n\n\"Fortunately, this also gives us a greater migration path for people using GNOME that would instead like to use KDE.\"\n\nWell, they'll be doing it through good Qt technology, not through the recommended Gnome routes ;-). Qt will still integrate better with KDE though.\n\n\"If KDE continues to be better integrated and provide superior technical solutions, but concentrates on seamless integration with GNOME and improved UI consistency, people will switch back to it.\"\n\nKDE and Qt can't integrate seamlessly with Gnome. There's been some integration to an extent with GTK, but the integration that can be done with Qt can only go so far - which provides people with the integration they need. If most of the best apps used on KDE and Gnome are Qt based ones........"
    author: "Segedunum"
  - subject: "Re: Qt Company"
    date: 2007-09-27
    body: "Theres a quote from from the Paul Simon song 'The boxer' that goes:\n\"a man hears what he wants to hear and disregards the rest\"\nTo say the sort of things included in most of the replies to this post so far means you have a very closed mind and narrow view.\n\nI make no comment on the quality of KDE or Gnome but to say that gnome doesn't have many users or that industry support is not important show a high level of blindness on your part.\n\nAlso, what developers like is not necessarily what users like...\nI use Gnome. I am a user. I like it. Please don't attack me, bite me or any other nasty thing. I think all you KDE guys are awesome and doing a great job and I love it that qt4 integrates much better into the gnome desktop. (I love Amarok and Use Rosegarden) \n\nBest regards, Caleb."
    author: "Caleb"
  - subject: "OMG CSS"
    date: 2006-07-05
    body: "\"Widgets can be styled using stylesheets that follow a syntax similar to that used by Cascading Style Sheets (CSS) for HTML.\"\n\nThis is frickin' awesome. Do you realize what this means? Konqueror will be able to (realistically) support XUL and Mozilla plugins. Sweet!"
    author: "LuckySandal"
  - subject: "Re: OMG CSS"
    date: 2006-07-05
    body: "Given how Firefox plugins tend to leak memory, I'm hoping it just becomes easier to design plugins/extensions for konqueror so the active KDE community can make their own."
    author: "anony"
  - subject: "Re: OMG CSS"
    date: 2006-07-05
    body: "Umm... Javascript (the language used to write most firefox extensions) is garbage-collected; it cannot leak memory."
    author: "LuckySandal"
  - subject: "Re: OMG CSS"
    date: 2006-07-05
    body: "Javascript can most certainly leak memory! Pure Javascript objects may be garbage collected but XPCOM objects such as DOM nodes are reference counted. If you create a circular reference, which you can easily do by accident, the objects will not be garbage collected.\n\nI don't think memory leaks are the cause of Firefox's heavy memory consumption though, more likely it is because of cached images and html."
    author: "Erik Martino Hansen"
  - subject: "Re: OMG CSS"
    date: 2006-07-07
    body: "\"it cannot leak\"\n\nFamous last words..."
    author: "Brandybuck"
  - subject: "Re: OMG CSS"
    date: 2006-07-09
    body: "Memory leaks and garbage collection are nearly unrelated. In fact memory leaks are one of the biggest problems in application development, regardless of the memory management model.\n\nTrust me, I am working on a large java based desktop application and get bitten consistently by memory leaks, mainly because of static references. For most of them I don't have any control, since they are buried deep into third patry code. I spend days and weeks with my profiler to find the paths to this static references and to incorporate fixes to cut the chain, which is not an easy task, since garbage collected languages don't have destructors - HURRAY!"
    author: "cylab"
  - subject: "Re: OMG CSS"
    date: 2006-07-13
    body: "\"since garbage collected languages don't have destructors - HURRAY!\"\n\nNot true. Take a look at DigitalMars D (http://www.digitalmars.com/d/). Better than Java, much better than C++, consists of the best practices from both worlds."
    author: "demise"
  - subject: "Re: OMG CSS"
    date: 2006-07-06
    body: "Actually, this is pretty much completely irrelevant as far as konqueror is concerned.\n"
    author: "SadEagle"
  - subject: "Re: OMG CSS"
    date: 2006-07-07
    body: "I was looking at ways that this new extension to Qt might eventually do, not what it immediately does."
    author: "LuckySandal"
---
"<em>Trolltech <a href="http://www.trolltech.com/company/newsroom/announcements/press.2006-06-26.0683224314/">announced the release</a> of a technology preview of Qt 4.2 &#8211; the upcoming new version of its leading framework for high performance cross-platform application development &#8211; to its commercial and open source developer community for feedback.  The final release of Qt 4.2 is currently scheduled for the fourth quarter of 2006.</em>"  4.2 adds a <a href="http://doc.trolltech.com/4.2/graphicsview.html">new canvas</a>, <a href="http://doc.trolltech.com/4.2/qsvgrenderer.html">SVG support</a> and improved integration with GTK,  CUPS and <a href="http://doc.trolltech.com/4.2/qtdbus.html">DBus</a>.  Their <a href="http://doc.trolltech.com/4.2/qt4-2-intro.html">What's New</a> document includes the full details or just <a href="ftp://ftp.trolltech.com/qt/source/qt-x11-preview-opensource-src-4.2.0-tp1.tar.gz">download it directly</a>.



<!--break-->
