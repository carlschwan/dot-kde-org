---
title: "KDE Commit-Digest for 10th September 2006"
date:    2006-09-11
authors:
  - "dallen"
slug:    kde-commit-digest-10th-september-2006
comments:
  - subject: "First post!"
    date: 2006-09-10
    body: "Kudos to KDevelop developers!!!\n... and thanks Danny for the digest :)"
    author: "Ratta"
  - subject: "KDevelop"
    date: 2006-09-10
    body: "Great to see all the amazing work on KDevelop.  I can't wait until version 4.0.  I've tried to use the 3.x line, but the code completion just wasn't working all that well and I found the interface a little too complex.  It looks like KDevelop 4 will be the product that sways me from Visual Studio on Windows.  \n\nIf I was Trolltech, I would hire one of the KDevelop authors to work on it full time.  They could really use a good, multiplatform IDE that is optimized for Qt applications.  Would be a big selling point from what I've heard on forums."
    author: "Leo S"
  - subject: "Re: KDevelop"
    date: 2006-09-10
    body: "Yeah KDevelop looks much improved! If it only had good Python autocomplete support then it would be my primary editor...."
    author: "Devon"
  - subject: "Re: KDevelop"
    date: 2006-09-12
    body: "Yah, great idea. A version of kdevelop for windows would be a great thing (TM) for a lot of people I know.\nThere is just one thing I think should be changed: the use of autoconf/automake. It should at least hidden for the programmer who dosen't want to deal with it.\nMost windows users just expect pressing F9 and having the program running ASAP, as you can do in Delphi or Dev-C++."
    author: "Iuri Fiedoruk"
  - subject: "kdevelooooop!"
    date: 2006-09-10
    body: "Sweet stuff you're doing there! I find all the codemodel-abstract-syntax-tree-generation-talk a little abstract, but I'm looking forward to nice call graphs and autocompletion :)\n\nI *really* like the teamwork idea, but while reading about the custom implementation and the special required server, it felt like David Nolden should have talked to the kopete guys: wouldn't it be cool if this was integrated with kopete and instead of the custom server, a jabber/aol/msn/irc-account would do it? Then again, I probably have no clue about all this. Thank you very much, David!"
    author: "ben"
  - subject: "Re: kdevelooooop!"
    date: 2006-09-11
    body: "It all isn't as complicated as it sounds. For example, you can open your own local server by using just a single checkbox within the user-interface. \n\nThe advantage of an instant-messenger-protocol might be that there wouldn't be a server to set up, that's true. The advantage of an own server-implementation is that better and safer user-management is possible(you can flag some users as trusted, untrusted, etc.), it is much more customizable for the future, it may deliver better performance, and we have more freedom in the kind of data we send.\n\nWell maybe I'll check how far it would be possible to additionally use jabber or something, some time in the future, because generally I like the idea too. :)\n\ngreetings, David"
    author: "David Nolden"
  - subject: "Re: kdevelooooop!"
    date: 2006-09-11
    body: "Jabber is eXtensible.. XMPP. You can make it do all you need it to. If you want to flag people as trusted or not, you add the data you client needs, and voila, you have trusted or not trusted. Please look into Jabber. VERY good tech there. It may not have the edge in performance, but otherwise jabber is very nice, and you will gain in not having to write and debug a bunch of communications code and can leverage advances being made in Jabber (voice etc) and users can have a common roster. (Inkscape is implementing its collaboration using jabber.)"
    author: "Jonathan Dietrich"
  - subject: "Re: kdevelooooop!"
    date: 2006-09-13
    body: "I can tell you that kopete team has an open mind, and i think both Gof (oliver goffart) and darkshock (michael larouche) will like the idea. They maintain jabber protocol, and will be at dublin ;) "
    author: "johann Ollivier Lapeyre"
  - subject: "Coolest.  Release Announcement.  Ever."
    date: 2006-09-11
    body: "Nice one, Max..."
    author: "Odysseus"
  - subject: "Kdevelop"
    date: 2006-09-11
    body: "KDevelop would be a really great application if they didnt remove the ordinary \"new\" function. You immediately have to specify a file. This prevents any use as a simple editor which is a shame because it has great language support, type-ahead etc. Grrrr.\n"
    author: "Martin"
  - subject: "KDE helps me"
    date: 2006-09-11
    body: "go out from the windows. so...Best wishes for you~~"
    author: "Jerome"
  - subject: "The Ark must sink!"
    date: 2006-09-12
    body: "I'm really happy to hear that we are shifting to KArchiver from Ark! Ark is really primitive and low in functionality. It also crashes a lot when we use it from the context menu."
    author: "Shriramana Sharma"
  - subject: "Re: The Ark must sink!"
    date: 2006-09-12
    body: "The reason I don't drop my Windows box is that PKZip allows me to 'repair' damaged or truncated zip files, particularly usefull for incomplete downloads. Can KArchiver do that? Then, indded, I would be impressed."
    author: "sebastian"
  - subject: "Re: The Ark must sink!"
    date: 2006-09-14
    body: "Do you know about dosbox even freedos ?\nI mean PKZip is a dos apps and should work fine in one of these.\nbye"
    author: "sebastien"
  - subject: "Re: The Ark must sink!"
    date: 2006-09-14
    body: "zip -FF (and zip -F) are just as nice as pkzipfix."
    author: "Roberto Alsina"
  - subject: "Re: The Ark must sink!"
    date: 2006-09-14
    body: "Hey, wow! I didn't know that. Last time I checked gzip which can't do that. It seems you only need to ask somebody. Thanks"
    author: "sebastian"
---
In <a href="http://commit-digest.org/issues/2006-09-10/">this week's KDE Commit-Digest</a>: Work begins on Ruby language support in <a href="http://www.kdevelop.org/">KDevelop</a> 4. Work continues in the KReversi code rewrite. <a href="http://edu.kde.org/kalzium/">Kalzium</a> gets functionality to visually show the country an element was discovered in. Automatic regression testing for <a href="http://kate-editor.org/">Kate</a>. Mimetype and metadata support for the <a href="http://en.wikipedia.org/wiki/XML_Paper_Specification">XML Paper Specification</a> format. <a href="http://www.vandenoever.info/software/strigi/">Strigi</a> can now use outside applications to index files outside its core scope, such as <a href="http://en.wikipedia.org/wiki/Pdf">PDF</a> files. KJots gets greatly improved find and replace functionality. Many improvements in supporting different archive formats in <a href="http://perso.orange.fr/coquelle/karchiver/">KArchiver</a>.

<!--break-->
