---
title: "People Behind KDE: Boudewijn Rempt"
date:    2006-03-01
authors:
  - "jriddell"
slug:    people-behind-kde-boudewijn-rempt
comments:
  - subject: "Krita Rocks!"
    date: 2006-03-01
    body: "Krita Rocks: http://www.koffice.org/krita/krita-fosdem2006.pdf\n\n...but\nwhere ist the Klik:// -able Krita SVN nightly version?"
    author: "Martin"
  - subject: "Re: Krita Rocks!"
    date: 2006-03-01
    body: "\"where ist the Klik:// -able Krita SVN nightly version?\"\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\nGood question indeed  ;-)\n\nMartin, as soon as you can point me to someone who builds (nightly, or weekly, or monthly...) installable .deb binaries (or RPMs, .tgzs, .bz2s, .packages....) I will create a klik recipe that gives you the klik-able thingie.\n\nThe next update to klik://krita-1.5-beta is due in a few days. It will give you the Beta2 snapshot (the current ones are at Beta1 still). Maybe we can convince the packager of the Beta .debs to build updates every week until the final release. Let's see..\n\nCheers,\nKurt"
    author: "pipitas"
  - subject: "Nice interview"
    date: 2006-03-01
    body: "yes, indeed Krita rocks! Incredible to see how much development has been put in KOffice overall and in Krita in particular.\nThanks Boud for sharing a bit of your life with us, it's always nice to get to know people better. Congratulations on your lovely family!"
    author: "annma"
  - subject: "vital vertical-maximise feature"
    date: 2006-03-01
    body: "Absolutely true! I don't know of any other window manager with the cool middle-click-is-vertical-maximise feature.\nBoudewijn, bedankt!"
    author: "jos"
  - subject: "Re: vital vertical-maximise feature"
    date: 2006-03-01
    body: "I didn't know the feature existed until I read the interview yesterday."
    author: "Bram Schoenmakers"
  - subject: "Re: vital vertical-maximise feature"
    date: 2006-03-01
    body: "Thats one of the great features that make the day easier, I use it all the time. It's brother, the left-click-is-horizontal-maximise feature, is not that usefull to me at least."
    author: "Morty"
  - subject: "Re: vital vertical-maximise feature"
    date: 2006-03-01
    body: "Oh, horizontal-maximize is useful for Konsole etc., where long lines (think of compile logs) will then not result in line breaks :)\n\nThanks Boudewijn!"
    author: "Another happy {M,L}MB user"
  - subject: "Very good stuff"
    date: 2006-03-01
    body: "Amazing application, and one of the cases where the hacker (Rempt) seems like a truly genuine and very nice guy. :) \n\nReading the interview was refreshing; thank you for your hacking on KDE!"
    author: "Francis"
  - subject: "minicli"
    date: 2006-03-01
    body: "Never heard of ALT+F2 being called the minicli before. Thought it was just a plain old run location dialog. This is one of the things I like about KDE, finding little bits and pieces of useful functionality now and then."
    author: "foo"
  - subject: "An Orthodox in Deventer"
    date: 2006-03-01
    body: "I'm wondering how and why Boudewijn became an Orthodox ? Being an Orthodox seems to me so opposite to the Dutch mind, especially in Deventer, the hometown of Gert Groote. \n\nBeing a Dutch Orthodox is a living paradox. Descendant of people who loved to smash statues of the Virgin Mary in churches, now believing there is something essentially holy in an icon. \n\nMaybe we should start a theological debate between Olaf Schmidt, Boudewijn and Jonathan Ridell on 'Trinity, Transsubstantation and Free Software'"
    author: "Charles de Miramon"
  - subject: "Re: An Orthodox in Deventer"
    date: 2006-03-01
    body: "Sure. And I can \"moderate\", since I think they are all wrong in the same measure ;-)"
    author: "Roberto Alsina"
  - subject: "Re: An Orthodox in Deventer"
    date: 2006-03-01
    body: "Personally, I prefer to think \"right in the same measure\" -- but then, I'm a cup-half-full kind of person :-)."
    author: "Boudewijn Rempt"
  - subject: "Re: An Orthodox in Deventer"
    date: 2006-03-01
    body: "I'm not particularly Dutch, my ancestry being compounded of equal parts of German, Danish, Swedish, Belgian, Swiss and some other nations, and Deventer is only my home town because I couldn't afford a house in Haarlem. But I've got nothing but the greatest respect for Geert Groote. And I always wanted my parents to detour through Chartres on the way home from our holidays in France.\n\nBut I've grown up in a family of church-avoiders (although the church they avoided going to was strictly the Nederlands Hervormde Kerk, others were out of the question). So when I started studying I started looking around. I studied Chinese, Pali, Tibetan and Sanskrit, but also visited the church of my forefathers -- the Nederlands Hervormde Kerk. In the end, Buddhism didn't agree with me, and I was not intellectually strong enough to feel at home in a protestant church, with all the through-the-mind only that I experienced there.\n\nOrthodoxy was exactly right for me: welcoming, easy to get into (in a pleasingly antiquarian way), and still held out the promise of intellectual rigors to come (Greek! Russian! Old Church Slavonic!). Of course, I couldn't have predicted at the time that the intellectual rigors would keep me from attending the Libre Graphics meeting... I have to be in Brussels for a theology course the same weekend the LGM is being held:-)."
    author: "Boudewijn Rempt"
  - subject: "Re: An Orthodox in Deventer"
    date: 2006-03-01
    body: "Thanks for this glimpse of the Itinerarium mentis Balduini in Deum. After you master Slavonic, I'll send you a Coptic grammar :-) \n\nPity Deventer, once the Mecca of the Netherlands and now a place where you end if you cannot afford Haarlem (which is the place where you end if you cannot afford Amsterdam) ;-)\n\nWe are all waiting for your blog about Brussels theology."
    author: "Charles de Miramon"
  - subject: "Re: An Orthodox in Deventer"
    date: 2006-03-01
    body: "Amsterdam is nice to visit and to buy books; but I have never entertained any desire to actually live there. Anyway, Coptic... Coptic has been on my list for some time."
    author: "Boudewijn Rempt"
  - subject: "Krita absolutely kicks ass"
    date: 2006-03-01
    body: "I used Krita few days ago for the first time (made some quick and dirty mockups). And I can safely say that it absolutely kicks ass! Thank you Boudewijn for your hard work on Krita!"
    author: "Janne"
  - subject: "thank you for being here"
    date: 2006-03-01
    body: "The interview was very impressive and enjoyable to read, you are a fascinating human being. Thank you so much for being around."
    author: "Raphael"
  - subject: "vertical maximize"
    date: 2006-03-01
    body: "I love the vertical maximize feature!\n"
    author: "Ian Monroe"
  - subject: "nice desk"
    date: 2006-03-04
    body: "While comparing boudewijns desk to mine it's hard to recognize that both serves for the same thing. But boudewijns desk looks definetly the way I want the desk of the devs look like who care for the applications i'm using. ;-) "
    author: "marc"
  - subject: "kwin and drag and drop failure :("
    date: 2006-03-06
    body: "it is not possible to drag and drop a file/folder/selection from a maximized window to a non-maximized window. as soon as we click to drag, the window focus changes to the maximized window, hiding the non-maximized window.\n\ntry this:\n\n1. open konqueror $HOME\n2. open one more instance of say konqueror /tmp\n3. maximize $HOME window\n4. make /tmp window smaller and in front of the maximized window (*without* always on top feature)\n5. just try dragging a file/folder to the small window from the maximized HOME window (which is visibile)\n6. as soon as we click on the maximized window's content/file/folder to \"start the dragging\" \n7. small window loses focus.\n8. and now where to drop the item we've selected?\n\nThe whole idea of drag and drop is failed in KDE due to KWin focus on click. \n\nThe window focus should be changed only on \"mouse button *release*\" and not on click!\n\nI did report a bug (with KDE 3.3, and it is not fixed yet even in KDE 3.5.1), perhaps KDE developers don't use drag and drop.\n\n"
    author: "fast_rizwaan"
---
Tonight's <a href="http://people.kde.nl">People Behind KDE</a> interview is with the author of the most active program in KDE, <a href="http://www.koffice.org/krita">Krita</a>.  Not only has he made the premier free software painting application he has also added the vital vertical-maximise feature to KWin.  Find out what the most inspirational thing is for a hacker fixing his bugs in our <a href="http://people.kde.nl/boud.html">interview with Boudewijn Rempt</a>.


<!--break-->
