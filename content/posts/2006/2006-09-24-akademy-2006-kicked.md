---
title: "aKademy 2006 Kicked Off"
date:    2006-09-24
authors:
  - "sk\u00fcgler"
slug:    akademy-2006-kicked
comments:
  - subject: "Akademy Day 1"
    date: 2006-09-24
    body: "\"Aaron has undulated his wantonness\"\nhttp://people.fruitsalad.org/adridg/bobulate/index.php?/archives/270-Frist-psot-at-aKademy.html\n\nGet more from PlanetKDE while it is hot\nhttp://planetkde.org/\n\nPlease do try to make sure to post lots of links to these things as they wont be on PlanetKDE for long.  "
    author: "Alan Horkan"
  - subject: "Video's"
    date: 2006-09-24
    body: "Video's anyone?"
    author: "LB"
  - subject: "Re: Video's"
    date: 2006-09-24
    body: "all the presentations are being video taped (with audio this time! ;) ... once transcoded they will be uploaded somewhere for public consumption. this may take a couple days since the orga team is quite busy atm."
    author: "Aaron J. Seigo"
  - subject: "Re: Video's"
    date: 2006-09-24
    body: "I hope a codec better than Theora will be used this time. Have fun btw! :)"
    author: "cartman"
  - subject: "Re: Video's"
    date: 2006-09-24
    body: "What is wrong with Ogg Theora? And what is better? XVID perhaps?"
    author: "Carsten Niehaus"
  - subject: "Re: Video's"
    date: 2006-09-24
    body: "Ogg Theora video is real low quality, XViD or H.264 is better and both support by ffmpeg ( which is free software )."
    author: "cartman"
  - subject: "Re: Video's"
    date: 2006-09-24
    body: "But they are patent encumbered. Ogg Theora is much better (more Free) choice."
    author: "Mikos"
  - subject: "Re: Video's"
    date: 2006-09-25
    body: "Several presenters were unable to keep the microphone out of their mouth, which gave really poor quality audio, even for those sitting in the lecture theatre.  \nWe will need to wait and see how good the audio will be.  "
    author: "Alan Horkan"
  - subject: "Re: Video's"
    date: 2006-09-24
    body: "The talks are recorded. No live-stream but will be encoded and available later."
    author: "Anonymous"
  - subject: "Re: Video's"
    date: 2006-09-24
    body: "I've also been asking myself where are the webcasts. I sent an e-mail to aKademy 2006 team and they say that there are no webcasts, but they are recording the talks and will post videos later. So no worries. We will not be able to see it live, but we will be able to see it.\n\nBTW, you can also digg this story:\nhttp://digg.com/linux_unix/aKademy_2006_kicked_off"
    author: "Jure Repinc"
  - subject: "Transcripts available"
    date: 2006-09-24
    body: "Hi everyone. Im attending some talks and writing transcripts for all which are not there but interested in the talks. I have more transcripts in the pipeline and will publish them asap.\nMy blog: http://shyru.blogspot.com\n\nGoing to the next talk now..."
    author: "Shyru"
  - subject: "Mr"
    date: 2006-10-10
    body: "if only i could have gone, pity im on the other side of the world"
    author: "Minime"
---
<a href="http://conference2006.kde.org">aKademy 2006</a> has been kicked off at the <a href="http://www.tcd.ie">Trinity College in Dublin</a>. The first two days consist of the contributors conference with a <a href="http://akademy.kde.org/conference/program.php">fully packed programme</a> of presentations on aspects such as the community, KDE 4, cross-desktop collaboration and KDE &amp; the Free Desktop in Asian countries.  On Monday, members of the KDE e.V., KDE's legal body will hold their General Assembly. From Tuesday to Saturday will be the time to work -- mostly on KDE 4 -- and to team up for BoF sessions to discuss all kinds of issues in person. Read the <a href="http://www.kde.org/announcements/akademy2006-kicked-off.php">official announcement for full details</a>.  Tonight the participants are enjoying free pizza and Guinness thanks to Nokia who sponsored the <a href="http://kubuntu.org/~jriddell/2006-09-23-akademy-nokia-social.jpg">social event</a> at the university sports pavilion.

<!--break-->
