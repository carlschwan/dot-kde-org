---
title: "Jim Bublitz Talks About PyKDE"
date:    2006-09-10
authors:
  - "jriddell"
slug:    jim-bublitz-talks-about-pykde
comments:
  - subject: "How about KOffice libs support?"
    date: 2006-09-10
    body: "I've been planning a new KDE app that I'd like to do in Python for various resaons (mostly because I suck at C++), but I'm wanting to use the Kexi database back-end as a core componant, with perhaps other componants frm KWord and KSpread.  Do you have any plans to add the KOffice libs to PyKDE?  How hard would it be for me to generate the bindings if I needed to?\n\nJohn."
    author: "Odysseus"
  - subject: "Re: How about KOffice libs support?"
    date: 2006-09-10
    body: "Since I am somewhat a KOffice fan, let me provide my point of view here;\n\nKexi does provide with KexiDB ( located in the svn at koffice/kexi/kexidb/* ) a very nice library that could be reused even outside of Kexi. Applications like Showimg and KPhotoAlbum are using it already today. I guess the most difficult thing is to maintain the sip-files since KexiDB still improves a lot and that may mean, that it's needed to adopt the sip-files from time to time. On the other hand, it's worth it cause KexiDB is just great :) An alternate may to don't have sip-files against KexiDB direct rather then against the Kross-module for KexiDB. The advantage is, that I'll maintain the module for a long time and that I'll try to keep the interfaces as compatible as possible even if the underlying KexiDB changes. With Kexi 2.0 based upon Qt4 the module wan't depend on Kross any longer and is in fact only a collection of from QObject inherited classes that offer signals and slots to deal with KexiDB. Cause the module is a installed library, it may another alternate to try to load the module direct into python and access the signals and slots with PyQt/PyKDE's signal and slot implementation.\n\nFor KSpread and KWord it's more difficult cause they are whole applications including GUI-functionality rather then libraries. So, I guess you like to reuse e.g. kostore+kotext (located in the svn at koffice/libs/(kostore|kotext)/* or maybe flake?) to be able to e.g. deal with OpenDocument files? or do you like to embed the whole application including the GUI into your PyKDE-app (if that's the case, I would propose to use the with PyKDE provided KPart-technology)? or do you like to control the applications from within a script (then just use dcop/dbus also included in PyQt/PyKDE)? or...?"
    author: "Sebastian Sauer"
  - subject: "Re: How about KOffice libs support?"
    date: 2006-09-10
    body: "Bindings for an application would be different than bindings for kdelibs. For kdelibs (PyKDE), you want to access almost everything. For an app, you just want to be able to manipulate or extend the app, because the app already knows how to do what it does (display a spreadsheet, maintain a DB, etc). A full set of bindings on all of KOffice, or even a component like KSpread would be huge and not that useful.\n\nA few years ago I did have a Python interface to KSpread. If the API is well-documented, it's fairly easy to do. The way I did it was to write a C++ wrapper that created the classes/objects that I thought made an intuitive interface for Python programming. Those end up being mostly a few lines of code per method, calling into the underlying API. Then I did Python bindings for the C++ wrapper and added the menu stuff/plugin interface to make it accessible from KSpread.\n\nThe problem (as noted in another reply) is that application authors need to be free to restructure their apps - KSpread changed a lot since I wrote a Python interface to it. So you either need some close co-ordination and maintenance, or something like OpenOffice, which has a built-in BASIC-like language whose interface you could write your particular language interface against.\n\nIt isn't especially difficult to do, but you need a couple different skills to do it. The easiest way is to write a C++ wrapper that's easy to generate bindings for - that rationalizes the programming environment for users so they can write in terms of how they view an app instead of how the app's authors view it. Bindings themselves are pretty easy - most are auto-generated in some fashion, and if there was a demand (and some programmer-hours available), those tools could be made available. Right now, most are personal tools without docs and only work in the author's environment. Sip is pretty easy to develop files for manually too, although there's a learning curve.\n\nThis is exactly the kind of thing I was addressing in one of the last answers above, and I'd really like to see it pursued. I've made a couple of starts, but never got to anything releaseable."
    author: "Jim Bublitz"
  - subject: "Re: How about KOffice libs support?"
    date: 2006-09-10
    body: "> The way I did it was to write a C++ wrapper that created the\n> classes/objects that I thought made an intuitive interface for Python programming.\n\nAs I understand it, this can't work anymore since gcc4 since only application objects will not export their symbols making it impossible to link to the app.\nThis is displayed by the fact that apps hardly install header files, only libs do. So, not only would compiling the code be pretty hard, linking would be impossible.\n\nKOffice provides a scripting backend for this reason. Although admittedly the support for various apps need work."
    author: "Thomas Zander"
  - subject: "Re: How about KOffice libs support?"
    date: 2006-09-10
    body: "I think a scripting backend would be preferable. I also think it takes some very careful design - as much as the UI - for usability. That's a comment, not a criticism, as I haven't looked at KOffice source in any detail lately. As I mentioned above, OpenOffice has a Python-UNO bridge available (as well as interfaces for other languages), but the organization isn't especially intuitive - not something VBasic users would find appealing (for Python it has other problems, like which Python version is supported and where the interpreter has to be located, at least last time I looked).\n\nThe other extreme is something like Borland's Quattro or early Excel versions (haven't used Excel since probably 1.0), which had very simple macro languages that non-programmers could use by either writing or recording macros, and people were scripting Lotus 123 spreadsheets 20 years ago.\n\nIt isn't necessary to link to the app if it has a plugin interface, which KOffice apps had when I did my bindings. KOffice is still built on libraries, and libraries have to export symbols. Most of the interesting stuff is probably in the .so libs.\n\nJust doing a quick look at the source, it looks like KSpread::Sheet.setText() would be available from libkspreadcommon.so, for example. Whether I could get access to a particular Sheet object via a plugin is a different question and I haven't checked that out, but it seems likely I could. \n\nBut I'd be very reluctant to create and publish an interface that KOffice developers might change radically in a newer version - both out of courtesy and respect for the needs of the developers as well as the kind of workload it would create for the interface maintainer. For example, the formula code for KSpread has changed completely, and probably for good reasons.\n"
    author: "Jim Bublitz"
  - subject: "Re: How about KOffice libs support?"
    date: 2006-09-10
    body: "Also KSpread 2.0 has already nice wrappers. This are the DBUS-bindings (SheetAdaptor, ViewAdaptor, etc.) and they are not only maintained by the KSpread-developers, but I guess (or at least I hope :) there interfaces will stay compatible for a longer time. So, to reuse exact those interfaces and if they do not fullfit your needings extend them, sounds for me like the best solution. That's btw the same way the kross-kspread-binding went. In 1.6 it was mainly such a C++ wrapper, while in 2.0 it just deals with the dbusadaptor's and therefore minimizes code-duplications and the needing to maintain those additional code.\n"
    author: "Sebastian Sauer"
  - subject: "Re: How about KOffice libs support?"
    date: 2006-09-10
    body: "An additional question;\nmay you are able to offer a link to the PyKSpread 1.1 work? Well, I know that they do not work, but maybe someone (like e.g. me :) likes to take a look at them :) A lot of thanks in advance."
    author: "Sebastian Sauer"
  - subject: "Re: How about KOffice libs support?"
    date: 2006-09-10
    body: "Send me an email address and I'll either email you a tarball (around 600K) or post it someplace you can FTP it (let me know which yiu prefer).\n\njbublitz@nwinternet.com\n"
    author: "Jim Bublitz"
  - subject: "Re: How about KOffice libs support?"
    date: 2006-09-10
    body: "I'm spouting off a little unintelligently here, since I don't keep up with all of the KDE development. From the little I know, DBUS will probably be a big help in KDE4 and apparently KO2.\n\nI wasn't aware of kross before - it looks excellent, and is exactly what I'd like to see, especially being a component of KOffice. If you're interested in using sip (which is already part of kdebindings, thanks to Simon Edwards), I'd be happy to answer any questions, or provide other assistance. Or you can sign up for the PyKDE mailing list at http://mats.imk.fraunhofer.de/mailman/listinfo/pykde - it covers PyKDE, PyQt and sip (it's also one of the KDE mailing lists). \n\nIn reference to your followup below - I'll see if I can find the package. I did have some correspondence with a KOffice developer a few years ago about it. I think the code is for KOffice 1.1 or 1.2, so there's little chance it will work, but it shouldn't be hard to follow. If I find it, I'll post a message here and we can work out some way to transfer it - it's pretty small.\n"
    author: "Jim Bublitz"
  - subject: "Re: How about KOffice libs support?"
    date: 2006-09-14
    body: "What does DBUS offer us that we didn't have in DCOP.  I know it allows for networked calls; but besides that what do we gain?\n\nBobby"
    author: "Bobby"
  - subject: "Re: How about KOffice libs support?"
    date: 2006-09-16
    body: "What does DBUS offer? Well, you kinda got me, because I'm no expert on it. My understanding is that it's fairly similar to DCOP, but has wider support so that I can more easily do IPC between KDE and Qt or Python programs (or GTK or other things) that don't know anything about KDE. And networking, as you mentioned. Both of those are things I already do, but this looks nicer.\n\nBeyond that, I suppose I'm hoping that since KDE is now a lot farther along with stuff like DCOP or D-Bus, that the implementations will be more generally useful.\n\nI'm just looking at it as a choice that KDE developers seem to have made - I couldn't really argue the merits of one over the other."
    author: "Jim Bublitz"
  - subject: "Thanks for PyKDE!"
    date: 2006-09-10
    body: "Let me be the first one who says thanks for the great work done on PyKDE! It just rocks to be able to write complete KDE applications in Python :)"
    author: "Sebastian Sauer"
  - subject: "Re: Thanks for PyKDE!"
    date: 2006-09-10
    body: "My pleasure (most of the time). And of course most of the credit goes to Phil Thompson, and another portion to all of the other contributors and mailing list posters."
    author: "Jim Bublitz"
  - subject: "Re: Thanks for PyKDE!"
    date: 2006-09-12
    body: "Can I just tag onto this thread to say thanks as well?\n\nI've had a lot of fun with PyKDE over the past three-or-so years; though, as people should find quite obvious from your comment in the interview, I probably didn't start out with PyKDE in the same manner as most people. ;-)\n\nI hope to continue having fun with PyKDE and KDE 3 until the new version for KDE 4 appears, then I'll start having fun embedding Python in all sorts of new components and applications! :-)\n\nThanks, once again!"
    author: "David Boddie"
  - subject: "Timezones"
    date: 2006-09-10
    body: "Regarding the screenshot, why on earth would the Kubuntu installer care what city you were in? I've never understood this.\n\nLast time I installed it, I didn't see a way to just pick a timezone. I had to pick a city.\n\nOf course, they didn't have my city. So I'm left wondering which city that shares my timezone I should pick.\n\nFor example, why is there a selection for both Toronto and Montreal, if both these cities are in the Eastern time zone?\n\nMy best guess has been that this was an attempt to be user-friendly, perhaps for users that don't know their own timezone? But I think almost everyone knows their timezone. Microsoft, for example, isn't afraid to let people pick a timezone directly.\n\nI'm left to choose between two equally-wrong choices because of the over-specific question, when all I really want is to specify something like EST5EDT... :-)"
    author: "AC"
  - subject: "Re: Timezones"
    date: 2006-09-10
    body: "The timezones are caled respectively America/Toronto and America/Montreal and they are standard names (posix at least).\n\nOn other similar installers, there is also a way to select a UTC-basd timezone (haven{ t tried this one)."
    author: "Roberto Alsina"
  - subject: "Re: Timezones"
    date: 2006-09-10
    body: "It is to find a timezone and make a good guess at the spoken language and keyboard layout.\n\nWe did indeed consider it easier to click your closest city on a map.\n\nThe exact cities used come from debconf and I know the debian-installer developers have done a lot of research to pick which ones are necessary.  I'd guess Toronto and Montreal are there because Montreal will default to French and azerty while Toronto will default to English and qwerty.\n"
    author: "Jonathan Riddell"
  - subject: "Re: Timezones"
    date: 2006-09-10
    body: "Actually we use QWERTY keyboard in Quebec(thus Montreal), so this is a keyboard variant difference in this case\n\nMontreal -> cf keyboard variant\nTorono -> us keyboard variant."
    author: "Micha\u00ebl Larouche"
  - subject: "Re: Timezones"
    date: 2006-09-10
    body: "I have zoneinfo for New York and Detroit. Surely they share a common timezone, spoken language, and keyboard?"
    author: "AC"
---
Following our <a href="http://dot.kde.org/1155075248/">interview last month</a> with Phil Thompson on <a href="http://www.riverbankcomputing.co.uk/pyqt/">PyQt</a>, we spoke with the maintainer of <a href="http://www.riverbankcomputing.co.uk/pykde/">PyKDE</a> to discover the status of our own Python bindings.  Read on for Jim Bublitz talking about how he was suckered into maintaining PyKDE, why you should use it and what his plans for the future are.











<!--break-->
<div style="border: thin solid grey; float: right; margin: 1ex; padding: 1ex; width: 300px">
<a href="http://static.kdenews.org/jr/ubiquity.png"><img src="http://static.kdenews.org/jr/ubiquity-wee.png" width="300" height="257" alt="ubiquity screenshot image" /></a><br />
Kubuntu's new installer, Ubiquity, uses PyKDE.
</div>

<p><strong>Please introduce yourself and your role in KDE.</strong></p>

<p>I'm Jim Bublitz. I maintain PyKDE - the Python language bindings for KDE.
 
<p><strong>What is PyKDE and why should I use it?</strong></p>

<p>PyKDE allows you to access most of the essential classes and methods of kdelibs
 from Python. I use it myself because it allows me to develop good-looking graphical
 applications that are KDE compatible, and allows me to do it from Python, which I
 find to be a much quicker and easier development environment than C++.</p>
 
<p>I like having Python bindings for KDE (as opposed to just Qt/PyQt) because KDE
 provides additional more convenient and higher level widgets, dialogs, etc.
 than Qt alone does. With GUI applications, speed is often not an issue, and
 I doubt I'd see much improvement by going to C++. Most of PyKDE actually
 runs in C++ space anyway - it's just invoking methods that takes place
 in Python.</p>
 
<p><strong>How did PyKDE start?</strong></p>

<p>Phil Thompson (who doe PyQt and created sip, which underlies PyQt and PyKDE) did
 the original PyKDE for KDE 1.x.</p>
 
<p><strong>How did you get involved?</strong></p>

<p>In 2001, I was developing an application using PyKDE for KDE 1.x about the
 time KDE 2.0 and Qt 2.0 were released. There was a minor problem with the
 existing PyKDE version and it wasn't compatible with KDE 2.0, so I posted a
 message to the PyKDE mailing list. Phil responded that he was no
 longer able to maintain PyKDE or generate a version for KDE 2.0, and the
 short of it is, he suckered me into doing PyKDE for KDE 2.0 - "just do the
 parts you need and someone will pick up the rest" - it's not possible to do
 PyKDE that way. You have to do the whole thing. Anyway, Phil provided a lot
 of help and support for my initial relase and subsequent releases.</p>
 
<p>I'm probably not the best person to maintain PyKDE. I'm not a professional
 programmer (but somewhat more knowledgeable than a "hobbyist"), and I
 run a non-software business and have other commitments that make it
 difficult to keep PyKDE current at all times. However, no one else has
 volunteered to do it, so I keep at it (and enjoy it).</p>
 
<p><strong>How much work is it to maintain?</strong></p>

<p>It was pretty obvious from the beginning that keeping up with frequent
 KDE releases would be the biggest problem, so after I did the initial
 release manually, I started working on automating the process. It takes
 about 10 minutes to generate a new PyKDE version automatically, and
 maybe another couple of hours to touch up the errors from some of the
 minor bugs in my software that I haven't gotten around to fixing. At one
 point in time, I could have written a script to download the KDE source
 and dump out a finished PyKDE with no intervention, but a few things
 have broken since then.</p>
 
<p>Phil has continually worked to improve sip, and it seems like every
 improvement breaks PyKDE in some way. On the other hand, a lot of the
 improvements have been done to support C++ features in KDE that Qt
 doesn't require, so I've added to Phil's workload a lot too.</p>
 
<p>The time consuming part is back-testing against different KDE versions and
 different distributions. One of the nice things about the way Phil has
 designed sip and the way we've evolved the build process and source distributions
 is that it's relatively painless to have a single set of files build for all
 combinations of Qt and KDE versions. That's important, because users and
 distributions are never in sync with the versions they use, for example,
 Debian Stable vs the latest SuSE release. For quite a while, we maintained
 backward compatibility from Qt and KDE 2.0 up through Qt and KDE 3.x
 versions. Even now, PyKDE should build with any KDE 3.x version and compatible
 Qt version. In addition, we have to worry about a range of Python and
 gcc versions as well.</p>
 
<p>There are still some users running KDE 3.0, for example, and I like the fact
 that we can continue to support them. I'm currently running KDE 3.4 in production,
 and only recently switched to that because of a hardware upgrade, but PyKDE includes
 all of the changes through KDE 3.5.2 and should build with 3.5.3 and 3.5.4.</p>
 
<p>Debugging can be very time consuming because of the layers of code, the long PyKDE
 build times (especially when I have to relegate it to a slower machine), and
 unfamiliarity with some of the more complicated parts of the KDE API.</p>
 
<p>A large number of people have made significant contributions to PyKDE - either
 reporting bugs, requesting features, writing examples, or working through coding problems.
 The THANKS file for PyKDE has a lot of names, and I've probably missed a few too. Of course
 PyKDE wouldn't exist at all without Qt, KDE and all the developers who have contributed
 to those.</p>
 
<p><strong>How complete is PyKDE?</strong></p>

<p>It covers most of the major components of kdelibs. I've dropped support recently
 for a few things like kjs and kdesu, because they didn't seem to be very useful from Python,
 and nobody was using them that I'm aware of. Some things, like kparts, still
 have a few problems that need addressing, and, for example, a user recently
 contributed some nice enhancements to DCOP support. Some of the less-used
 parts probably don't get the testing they need, but support is there for
 kdecore, kdeui, kio, kfile, kmdi and most of the other kdelibs components.</p>
 
<p>I haven't computed the stats for PyKDE lately, but my automation stuff
 generates something like 60,000 lines of intermediate code (*.sip files) and
 all of PyKDE is well over 1 million lines of C++ code (generated by sip from
 the intermediate code). And it's about as compact as it could be.</p>
 
<p><strong>Do you have any plans for KDE 4?</strong></p>

<p>At present I plan to implement PyKDE for KDE 4. The hard part - sip and PyQt
 support - has already been accomplished by Phil. I've looked at Qt 4, but am far
 from familiar with it at the moment, and from what I can tell, KDE 4's design
 still needs to "settle down" a little before it makes sense to look at doing
 bindings for it.</p>
 
<p>PyKDE will probably lag the KDE 4 release by a little - maybe a month of two.
 There are a number of problems with working alpha and beta releases, and
 it's probable that sip or PyQt will need some adjustments, but I don't
 expect any major delays.</p>
 
<p>KDE 4 will also be a good excuse to rethink PyKDE and improve the process
 of creating PyKDE, and there have been <a href="http://mats.imk.fraunhofer.de/pipermail/pykde/2006-September/014001.html">some discussions lately</a> on the PyKDE
 mailing list.</p>
 
<p><strong>What is the relationship between the versions of PyKDE on riverbankcomputing.co.uk and the version in kdebindings?</strong></p>

<p>I've never been able to schedule my PyKDE work reliably enough to stay in sync
 with KDE's release schedules, so I've avoided putting PyKDE in the KDE CVS. Fortunately,
 Simon Edwards was able to repackage PyKDE and maintain the kdebindings part of
 it, and I really appreciate the work he's put in to that.</p>
 
<p>The kdebindings version lags my version of PyKDE by a little, but since KDE has matured,
 the changes between versions of kdelibs have become less significant. I don't see that
 as a problem for 99% of potential users or applications.</p>
 
<p><strong>Are you paid for your work?</strong></p>

<p>No, but I use PyKDE to write applications for my business (mostly accounting and similar
 stuff at the moment, but other things in the past), so I get a "payoff" that way.</p>
 
<p><strong>Do you know of any exciting users of PyKDE?</strong></p>

<p>I've been pleased to see PyKDE-based apps popping in various places, and a little
 embarrassed because of some of the problems in keeping PyKDE up to date and fixing
 various problems. Simon's work has been important in providing a consistent platform
 that allows people to distribute PyKDE-based apps. I haven't kept track in detail of
 who's using it and where.</p>
 
<p>One piece of code that I haven't had time to look at yet is something Troy Melhase
 has just made available at <a href="http://code.google.com/p/pykde-sampler">Google Code</a>. It's a demo
 of some of PyKDE's capabilities. Troy contributed some of the example code that
 comes with PyKDE, and I'm sure pykde-sampler is very well done.</p>
 
<p><strong>What is Eric?</strong></p>

<p><a href="http://www.die-offenbachs.de/detlev/eric3.html">Eric</a> is an IDE/debugger for Python development, particularly PyQt and PyKDE development, but
 not limited to those. It includes support for every software tool ever invented, even
 Bicycle Repairman, CVS and subversion, and Qt Designer. Its creator and maintainer is
 Detlev Offenbach, which, from the amount and quality of output, is probably a
 programming collective rather than one individual. A Qt 4 version <a href="http://mats.imk.fraunhofer.de/pipermail/pykde/2006-September/013985.html">will be available soon</a>.</p>
 
<p>I'm just an Eric fan and user - not developer.</p>

<p><strong>Would you like to see more use of PyKDE in KDE?</strong></p>

<p>Not so much PyKDE as increased use and easier interfacing to Python (and other
 languages, like perl and Ruby) across KDE and KOffice. Generating bindings with sip is
 almost trivially easy (once you get over the learning curve), but, for example,
 most KDE plugins require loading a C++ (.so) lib - a lot of people writing
 in Python either don't know or don't want to deal with C++.</p>
 
<p>For something like panel applets, we (mostly David Boddie) were able to figure out a
 way to write a single .so lib and fool kicker into using it to load any Python-based
 applet, but in general that approach doesn't work for most KDE apps, although it probably
 could. It's also possible to develop tools that automatically create the .so and .la
 files for Python programmers, but I'd prefer a cleaner approach.</p>
 
<p>DCOP is one way to accomplish some of that, but it often doesn't expose enough of
 an application's API to be really useful. Plugins with bindings that understand
 parts of an app's API can be a lot more useful, although something like OpenOffice's
 Python-UNO bridge is pretty daunting for novice programmers and complicated to use
 for just about anyone. KDE as a platform seems a lot cleaner and easier to use in
 that way.</p>
 
<p>My goal would be to be able to use any major language the way Windows users can use
 VBasic with that platform and its applications, or, hopefully, better. I certainly
 would like more people to use PyKDE, but for some things (like scripting a spreadsheet),
 PyKDE involves a lot of unnecessary overhead.</p>










