---
title: "Second KDE 4 Developers Snapshot Available"
date:    2006-11-02
authors:
  - "jriddell"
slug:    second-kde-4-developers-snapshot-available
comments:
  - subject: "Hope this is the time for plasma development?"
    date: 2006-11-02
    body: "hi all\n\nAs I understand from the main developer of plasma the KDE foundations ( QT, KDElibs) was not ready to implement all those revolutionary ideas, but now I hope the needed stack are here so as users we would hopefully expect some concrete code to be shown.\n\nCheers\nmimoune\n"
    author: "djouallah mimoune"
  - subject: "Re: Hope this is the time for plasma development?"
    date: 2006-11-02
    body: "Where did you heard that ?"
    author: "JC"
  - subject: "Re: Hope this is the time for plasma development?"
    date: 2006-11-02
    body: "I am just a fanatic planetkde reader; and the last presentation given by SEIGO about the state of plasma. But as he use a rather an unconventional English for me; perhaps I missed the meaning of his writing.\n\nmimoune"
    author: "djouallah mimoune"
  - subject: "Re: Hope this is the time for plasma development?"
    date: 2006-11-02
    body: "Facts:\n- SEIGO ;) didn't really include much visual buzz in his presentation.\n- Plasma in SVN has less code than one would hope it had.\n\nOpinions:\n- SEIGO sacrificed much of his time to work on other aspects of KDE while leaving Plasma a bit behind.\n- I trust SEIGO to turn the ship around and make plasma rock the boat. Do it, SEIGO. Do it. Do it."
    author: "Jakob Petsovits"
  - subject: "Re: Hope this is the time for plasma development?"
    date: 2006-11-02
    body: "heh.. indeed... all things in the time they need to occur, neither before nor later.\n\n(how's that for zen? did i hit it?)\n\nbtw, readers of my blog will attest that writing my name in capital letters is highly ironic. =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Hope this is the time for plasma development?"
    date: 2006-11-02
    body: "more help needed for plasma?"
    author: "sure"
  - subject: "Re: Hope this is the time for plasma development?"
    date: 2006-11-03
    body: "#btw, readers of my blog will attest that writing my name in capital letters is highly ironic. =) #\n\nSorry man just I have a limited access time for the internet where I work ; ( for today I have just 20 minutes) so reading an article, look up some words then making respond is a rather tough task, so no time to polishing my writing.\n\nAgain sorry Seigo\n\nmimoune\n"
    author: "djouallah mimoune"
  - subject: "Re: Hope this is the time for plasma development?"
    date: 2006-11-05
    body: "i'm sure he doesn't mind... so i hope your response wasn't serious as well, as aaron sure would feel sorry for you feeling sorry...\n\nanyway, i think your first comment was right on track, aaron didn't do very much on plasma because the framework just wasn't there. tough he also uses a lot of time for other stuff - and that's not a bad thing of course.\n\nbtw there are other ppl helping out with plasma, or at least they are going to. check plasma.kde.org for a who-is!"
    author: "superstoned"
  - subject: "Re: Hope this is the time for plasma development?"
    date: 2006-11-05
    body: " 'and that's not a bad thing of course'.\n\nSometimes I feel it is dommage that high technical developers, get occupied by less technical affairs (although very important by the way) which can done by other people, I mean with so little resources manpower are so precious.\n \nMimoune\n\nPS : This is  perhaps a drawback of free speech in the internet, every jackass thinks he has ideas. \n"
    author: "djouallah momoune"
  - subject: "Re: Hope this is the time for plasma development?"
    date: 2006-11-03
    body: "What IS the deal with that, anyway?  Were your little fingers severed in some crazy Chinese-fingercuff incident, or what?"
    author: "Louis"
  - subject: "Great!"
    date: 2006-11-02
    body: "This is great! It works, more or less. I was going to post from Konqueror 3.80, but it doesn't work *that* well yet :-)\n\nIf you want to try it out, remember to run KDE4 programs _only_ as a separate user (I created the user kde4test) so you don't run the risk of trashing your KDE3 setup."
    author: "Martin"
  - subject: "Screenshots?"
    date: 2006-11-02
    body: "Are there any visible new features? I'm using OS X so I'm not able to do a test run with KDE4. Has anybody made some screenshots yet?\n\nCheers,\nPeter"
    author: "Peter O."
  - subject: "Re: Screenshots?"
    date: 2006-11-02
    body: "It is supposed to look equal to kde3 but with KDE4 which makes it much faster and easy to code ;)\n\nCheers"
    author: "Damnshock"
  - subject: "Re: Screenshots?"
    date: 2006-11-03
    body: "You mean \"with Qt 4\", right?"
    author: "Kevin Kofler"
  - subject: "Re: Screenshots?"
    date: 2006-11-03
    body: "There have been OS X snapshots posted already. Without requiring X11 even. (KDE 4 will support native Qt/Mac, and there are already snapshots of this work.)"
    author: "Kevin Kofler"
  - subject: "Re: Screenshots?"
    date: 2006-11-03
    body: "DMGs for you to try out: http://ranger.users.finkproject.org/kde/index.php/Home\nScreenshot: http://ranger.befunk.com/gallery2/v/misc/screenshots/konqueror-20060826.png.html"
    author: "Kevin Kofler"
  - subject: "Re: Screenshots?"
    date: 2006-11-03
    body: "am i wrong or are there still font kerning problems on QT4 ? konqueror on that screenshot seems to have bad kerning ...\n"
    author: "anon"
  - subject: "Re: Screenshots?"
    date: 2006-11-04
    body: "Yes, the problem is still there. I've been told they are looking for the problem but so far they don't understand what is wrong."
    author: "Med"
  - subject: "Re: Screenshots?"
    date: 2006-11-04
    body: "What about the Windows port? I'd love to use Konqueror on Windows!"
    author: "AC"
  - subject: "Re: Screenshots?"
    date: 2006-11-04
    body: "Maybe that is the way to win the Desktop. First win the Windows Desktop with KDE applications and then convince them that they don't need Windows to run them.\n"
    author: "jupp"
  - subject: "Re: Screenshots?"
    date: 2006-11-04
    body: "If you want to convince them of that, you'll first need to explain to them what Windows is, and that's already too complicate. And what KDE is, which is even more complicate (Windows users will probably never know what KDE is, even if they use KDE apps). \n\nAnd after you've convinced them that it's important for them to know what Windows and KDE are, they will tell you that \"they don't need Linux to run them\".\n\nWelcome to reality. Reality is cruel, harsh, not made for dreamers."
    author: "ac"
  - subject: "Re: Screenshots?"
    date: 2006-11-04
    body: "Crap. The people using windows are not all huge \nidiots how they are sometimes depicted.\n\nMany are just gamer freaks, they dont care \ntoo much about something as long as it \nruns their games. \n\nEven my windoze friends understand what KDE is.\n\nJust a GUI.\n\nThats it. Even if its not correct. It is correct\nenough.\n\nUnless you'd want to make things more complicated by \ndescribing more and more details ... ;)"
    author: "shev"
  - subject: "Re: Screenshots?"
    date: 2006-11-05
    body: "You don't need to explain what KDE is.\n\nThe KDE project consists of many applications which are useful in their own right.  You can introduce people to free software one program at a time, as their needs arise.\n\nFor example, one of the things I often do when \"rescuing\" a problematic Windows box is to install Firefox, and give the client a brief overview of its benefits over what they had before - chiefly that it provides safer web browsing with fewer popup adverts and also tabbed browsing.  It is also very likely that there will be some extension for Firefox which provides a nifty feature that is very useful for the person in question.  Adblock, spell checkers and the flash blocker are good examples.\n\nA good reason to start with the web browser is that it is where a lot of people spend a lot of their time.  \n\nI think that if you were to replace the following applications on a Windows PC with free software, cross platform equivalents you would go a long way to easing the transition to a completely free software environment.\n\n- Web Browser (Internet Explorer -> Firefox / Konqueror)\n- Office software (Microsoft Office -> OpenOffice / KOffice)\n- Chat program (MSN Messenger -> Kopete / Gaim)\n- Music Player (Windows Media Player / iTunes -> Rhythmbox / Banshee / amarok)\n- Video Player (Windows Media Player -> Totem / KMPlayer / Kaffeine / Helix )\n- Mail program (Outlook -> Kontact / Evolution / Thunderbird )\n- Small non-commercial games ( Windows games -> anything in KDE games )\n\nDon't underestimate the importance of the last one.  I don't play many non-console games on their PC, but many of my friends do."
    author: "Robert Knight"
  - subject: "Re: Screenshots?"
    date: 2006-11-05
    body: "- video editor?\n- Flash editor?"
    author: "Hartmut"
  - subject: "Re: Screenshots?"
    date: 2006-11-05
    body: "I was the original poster of the comment wrt Windows port.\n\nBasically I'd say why port to Windows? Because you can! Not only that, also because now with QT4 it is easier and possible. There has been KDE/Cygwin and I ran it before.\n\n(And yes I have ran many KDE and GNOME releases on various OSes from the beginning ages of KDE 1.2.x)\n\nSure, there are some people who wouldn't switch, nor run it, nor like it, etcetera. Some would though. Some run Linux AND Windows. Some HAVE to run Windows. For their work for example. If you can run Linux for your work, hobby, from your parents basement, good! Not everyone can though. Keep it in mind.\n\nNow, Windows lacks a good browser. There if Firefox, but Konqueror could compete here (as there is Safari which killed IE for Mac). OO.o/GIMP could use competition too. Imagine KOffice on Windows.\n\nMany GTK programs run on Windows (I use them daily). Hardly any QT programs run on Windows though. I feel this is a missed opportunity by the KDE/QT people and I thought that with QT4 for Windows they realized that. Maybe they did and I speak too fast, who knows.\n\nPeople who run say Firefox on Windows would be familiar with Firefox on Linux. The same would be true for Konqueror. That is just an advantage.\n\n(And Explorer sucks TBH and LiteStep wouldn't cut it.)"
    author: "AC"
  - subject: "Re: Screenshots?"
    date: 2006-11-05
    body: ">>Hardly any QT programs run on Windows though.\n\nLots of Qt application run on windows, but as user most of the times you won't notice that your actually using a Qt application.\n"
    author: "AC"
  - subject: "Re: Screenshots?"
    date: 2006-11-05
    body: "Yeah of course many QT applications run on Windows. That used to be only proprietary (and usually commercially) ones.\n\nNow we can also get the open source and free ones on Windows. Those are the ones I'm refering to, obviously. E.g. Konqueror, Koffice, KDE.\n\n@ Kevin Kofler, thank you very much for the link! Exactly what I was searching for. Seems its still stuck on KDELibs. The Cygwin/X port was more complete. One could actually run whole KDE using that but is just Cygwin/X of course whereas this is native.\n\nBtw, when I switched to Linux I was happy that I had a familiar browser back then: Netscape Communicator (and a bit later, Mozilla although still Milestones). It meant I didn't have to relearn, and I could use this under both GNOME and KDE (back then I preferred KDE). I believe it really does help/contribute. Although they're different projects one could also check out the websites and FAQs of the Cygwin and WINE projects for arguments involving this concept."
    author: "AC"
  - subject: "Re: Screenshots?"
    date: 2006-11-05
    body: "Nah, they don't need to know or want to know.\nThey just need to know that it will works as expected.\nLet me explain.\n\nBasically it started like this:\n- Switch from IE to Firefox over a 2 year period.\n (He was getting way too much spyware. Why did you download/install this?\n I don't know I followed instruction on the page... type of user)\n- Switch \"mail client\" to Hotmail. (could have been Gmail nowadays) \n- Removed IE icon from his Windows computer\n- Switch Word to OpenOffice\n- Ensure his favorites old DOS games works on Linux DOS emulator.\n\nThen one day, I installed Linux on his laptop,\nconfigured it and for all he knows it works the same,\nwith KDE on a Win9x theme.\n\nAs long as: he can type and print documents, play his DOS games,\nsurf the internet and read his email all day, he doesn't care.\n\nHe doesn't really know the difference and that's a good news.\nEnd users should not need to know, as long as things works as expected.\nEnd users don't care that their VCR are a FPGA, Linux box or something else,\nas long as PLAY, RECORD, REWIND, FWD, STOP, ON/OFF works as expected.\n\nSo, to make a point, you can first transpose the user to free applications,\nthen if all they use are free applications then the OS under it doesn't matter,\nas long as \"it continues to works as expected\".\n"
    author: "fp26"
  - subject: "Re: Screenshots?"
    date: 2006-11-05
    body: "They don't need to run Linux to help it, if they use Open-source apps on windows they help Linux anyway, because when they use Open-source apps they use open-standards, and we all know that closed-formats and closed-standards are the only real showstoppers for linux."
    author: "terracotta"
  - subject: "Re: Screenshots?"
    date: 2006-11-06
    body: "Not in all worlds.  In the home yes, but in the office (other than the small office) it is IS, in particual the CIO who needs convincing.  If they are already running free programs, and they have some linux expertise in house (the latter is easier to get than most people think), switching to linux will happen when someone mentions the word \"Budget\".   Microsoft Windows + office is expensive, when you are talking about thousands of seats.    Linux makes more sense in this space.\n\nDon't forget that linux is multi-user from the ground up (Microsoft Windows almost is, but there are missing parts even today).  Linux was designed for remote administration, Microsoft Windows wasn't.   There are other advantages to linux in a large corporation as well.    \n\nLinux doesn't not come with the Microsoft license agreement.   This makes things easier legally in some cases.   At work I've had to accept several license agreements from Microsoft as part of automatic updates.   However I am a contractor, not an employee, and thus not authorised to accept legal agreements.    I've always wondered what would happen the above situation when to court for some reason.   Not to mention several license agreements might conflict with other laws, but since nothing has been setteled in court, it isn't clear what happens.\n\nWhen/if any of the above becomes important companies will start moving to linux.   KDE is ready, and having versions that run on Microsoft Windows can ease the transisition."
    author: "Hank Miller"
  - subject: "Re: Screenshots?"
    date: 2006-11-05
    body: "Some people are working on it, see: http://www.kdelibs.com"
    author: "Kevin Kofler"
  - subject: "Re: Screenshots?"
    date: 2006-12-22
    body: "People who code linux stuff do so for the fun of it, not for strategic reasons such as \"conquering windows\"."
    author: "ROlf"
  - subject: "kwin-composite"
    date: 2006-11-03
    body: "Do these (especially the kubuntu ones) packages contain kwin-composite?"
    author: "ac"
  - subject: "Re: kwin-composite"
    date: 2006-11-03
    body: "probably not,\nsince it is a branch (not in the trunk) at the moment."
    author: "cies breijs"
  - subject: "Re: kwin-composite"
    date: 2006-11-03
    body: "Many of the fancier new developments are actually still in branches at this time."
    author: "Eike Hein"
  - subject: "Kubuntu Live CD with KDE 3.80.2"
    date: 2006-11-07
    body: "I am just waiting (lazily) for a Kubuntu Live CD I could try off or even install with the latest KDE developer snapshot.\n\nthanks"
    author: "somekool"
---
The <a href="http://kde.org/info/3.80.2.php">second KDE 4 developers snapshot</a> is now available.  This 3.80.2 release includes source from all the KDE modules.  Application developers are strongly advised to work primarily on KDE 4 from now on.  This release builds with Qt 4.2.0 and 4.2.1 (but not the 4.2 preview).  Packages are available for <a href="http://kubuntu.org/announcements/kde4-3.80.2.php">Kubuntu</a> and currently working through the <a href="http://software.opensuse.org/download/repositories/KDE:/KDE4/">SUSE buildservice</a>.




<!--break-->
