---
title: "The New and Improved KDE-Artists.org is Live!"
date:    2006-05-09
authors:
  - "jtheobroma"
slug:    new-and-improved-kde-artistsorg-live
comments:
  - subject: "Attribution Police"
    date: 2006-05-08
    body: "It looks pretty spiffy, indeed (modulo CSS bugs when you use fonts larger than they expect). The content is fresh and interesting (would be even more so if my artisty skills were better than that of pond scum). \n\nBut if I may put on my attribution police hat (two blue pixels) -- the site kind of forgets to have *any* \"contact us\" link or email address, and it's missing basic \"KDE is a trademark of the KDE e.V.\" and similar attribution. The \"site credits\" doesn't even mention KDE!\n\nOpen Collab has some interesting subjects already. http://open-collab.org/course/view.php?id=30 Gap analysis is right up kde-promo's alley I think."
    author: "Adriaan de Groot"
  - subject: "Re: Attribution Police"
    date: 2006-05-09
    body: "(blush) Thank you for the reminder:)"
    author: "Janet Theobroma"
  - subject: "Excellent"
    date: 2006-05-09
    body: "Great job with the sites. I'll be sure to register and start using KDE artists soon :)"
    author: "Vlad Blanton"
  - subject: "Broken link"
    date: 2006-05-09
    body: "There's a broken in link at the bottom of the kde-artists page where it says: \"Powered by Drupal\u00a9\". Last word is a link to htp://drupal.org"
    author: "Anonymous"
  - subject: "Re: Broken link"
    date: 2006-05-09
    body: "Thank you for pointing this out. :)"
    author: "landy"
  - subject: "Useability Issues"
    date: 2006-05-09
    body: "Hi\n\nI have two issues with the site:\n\n1) Every link I seem to click on asks me 'Do I wish to participate in this project?' before allowing me to see that projects content. How will I know if I wish to particpate in the project before I have been allowed to browse around in it and see what it is about? Also its extremely unclear what 'participating in a project' actually means? Am I going to be subscibed to a mailing list by doing so? Will free milk be delivered to my door every day? \n\n2) It seems every time I click on something it spawns a new window. Please make links stay in active window (I can always right click and say open link in new tab / window).\n\nOh, and thanks for taking the time to do this site! Keep up the good work!\n\n"
    author: "Tim"
  - subject: "Re: Useability Issues"
    date: 2006-05-09
    body: "\"1) Every link I seem to click on asks me 'Do I wish to participate in this project?' before allowing me to see that projects content. How will I know if I wish to participate in the project before I have been allowed to browse around in it and see what it is about?\"\n\nThis is a great question and it is on our to do list--> http://open-collab.org/mod/dfwiki/view.php?id=114. It is the line very cleverly disguised as \"Provide a solution for the forced subscription.\" Which I now realize means absolutely nothing to anybody but me. I have taken the liberty of adding your comment to our to do list. :)\n\n\"Also its extremely unclear what 'participating in a project' actually means? Am I going to be subscribed to a mailing list by doing so?\"\n \nI see your point and it is a good one. The solution that is devised will have to take this into account.\n\n\"Will free milk be delivered to my door every day?\"\n\nHmmm, you never know, I mean that is a remote possibility. We would of course have to figure in any additional taxes, tariffs, import and export fees on top of packaging that may apply. But I think if we all put our heads together we can come up with something! :D\n\nThank you for your input. :) Comments like these help to move the site out of Beta mode. We really do appreciate the collaboration. ;)"
    author: "lando"
  - subject: "Kollaboration"
    date: 2006-05-09
    body: "It was really sad to see Kollaboration forums go. One could just ask for an icon or icons for a particular program, and great ideas started pouring in right away. I see no such thing at Open-Collab. Is that community just gone or did it move somewhere? I couldn't find many old threads either..."
    author: "yaac"
  - subject: "Re: Kollaboration"
    date: 2006-05-09
    body: " We are still in the process of moving many of the posts from Kollaboration over to Open-Collab.org. If you are interested in getting involved with that effort you can go here--> http://open-collab.org/course/view.php?id=35  For a bit more in depth explanation as to what inspired the change can be found here--> http://open-collab.org/mod/resource/view.php?id=325 \n As for artwork requests and such that you refer to we are setting up a \"project page\" just for that purpose. \n One of the reasons behind the Beta release is to give people like your self who used Kollaboration an opportunity to help shape Open-Collab.org. Whether it is by good questions asked, constructive comments offered or rolling up your sleeves and digging in. \n We appreciate all the input thank you.\n  "
    author: "lando"
  - subject: "Hmm"
    date: 2006-05-09
    body: "These sites are... complicated.\n\nthere should not be 40 little \"jump to top\" icons on a site with almost no content per page.\n\nthey're nice, though. good luck."
    author: "albium"
  - subject: "Hmm?"
    date: 2006-05-09
    body: "\"These sites are... complicated.\n\nthere should not be 40 little \"jump to top\" icons on a site with almost no content per page.\"\n\nI am not completely sure as to what you are referring. Would you be willing to clarify a bit more? "
    author: "lando"
---
<a href="http://kde-artists.org">KDE-Artists.org</a> is back up sporting a brand new look and feel. We have created a much more community centric site where those interested can submit news, tutorials, links and more. There is also a new feature we call Studios. This is a new twist on blogging that is completely focused on the creation of art and showcasing the process of creation. Artists who are interested will be able to share their artwork with others and talk about how they created it, and what influences them in their creative process.









<!--break-->
<div style="border: solid thin grey; float: right; padding: 1ex; margin: 1ex; width: 315px">
<img src="http://static.kdenews.org/jr/kde-artists-new.png" width="315" height="217" border="0" />
</div>

<p>Kollaboration retires and <a href="http://open-collab.org">Open-Collab.org</a> is born...</p>

<p>Open-Collab.org has been built to replace the now retired Kollaboration forum. The new site is jammed packed full of goodness. It offers a universal space to dream, collaborate and work together toward common goals. If you are a developer or maintainer of an existing F/OSS application or project and are interested in interfacing with other community minded people Open-Collab.org is a great place to do this. It is also a place to educate would be contributers on what is needed in your project, how to contribute, as well as a place to seek documentation. If you are a F/OSS enthusiast then Open-Collab.org is the place to go to share your ideas and concepts that you feel could improve F/OSS.</p>

<div style="border: solid thin grey; padding: 1ex; margin: 1ex; width: 315px">
<img src="http://static.kdenews.org/jr/open-collab.png" width="315" height="217" border="0" />
</div>








