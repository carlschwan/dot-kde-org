---
title: "KDE e.V. Hardware Fundraiser Week"
date:    2006-07-19
authors:
  - "dmueller"
slug:    kde-ev-hardware-fundraiser-week
comments:
  - subject: "Open up the wallets people..."
    date: 2006-07-19
    body: "Hooray for some spiffy new hardware!\n\nEveryone, give what you can, its easy.  Even the damn near broke (*raises hand*) can give a little bit.\n\ne.V. guys, what about adding some Google Checkout as another option for those who don't want to use PayPal?  Just a thought.  Good luck with the fundraising!"
    author: "Joseph M. Gaffney aka CuCullin"
  - subject: "Re: Open up the wallets people..."
    date: 2006-07-19
    body: "Or a \"KDE fellowhip\". FSFE seems to have success here."
    author: "Itchy "
  - subject: "done."
    date: 2006-07-19
    body: "I sent 10 euros. Not much, but I assumed you'd do the same, then it'll be enough."
    author: "me"
  - subject: "If it's for KDE, you don't need to ask twice"
    date: 2006-07-20
    body: "I recognize I don't use to donate to free software projects, but if it's for KDE, you don't need to ask for it twice. :) So I've already added my contribution. I hope you can easily reach the ammount and get new good hardware for the server. Good luck!"
    author: "V\u00edctor Fern\u00e1ndez"
  - subject: "Another way to help"
    date: 2006-07-20
    body: "Could another way to help could be to use kbugbuster app instead of a web browser? \n\nIn theory this should make the server less busy (no need to present the data and the interface, only actual data has to be sent)."
    author: "Jaroslaw Staniek"
  - subject: "Re: Another way to help"
    date: 2006-07-20
    body: "IIRC, kbugbuster just does screen scraping of the page, so it will still place the same load on the server as loading a page from bugs.kde.org. "
    author: "Matt Rogers"
  - subject: "Re: Another way to help"
    date: 2006-07-20
    body: "Matt, except you can perform many actions offline and then \"commit\" the changes in one go, at least it looks as such for me. \n\nkbugbuster supporting SOAP or so looks like appealing idea to me... The app could be a bit improved for usability and then connectede to \"report a bug\" menu item by default in KDE4...\n"
    author: "Jaroslaw Staniek"
  - subject: "Time to give something back :)"
    date: 2006-07-20
    body: "As a very happy KDE user, I have absolutely no problem with a little donation. The KDE software is far better than any of the software I've bought while still using Windows. Good luck with pimping up the server!"
    author: "Bouke Woudstra"
  - subject: "Of Course!"
    date: 2006-07-20
    body: "I donated also a little bit. Please keep on donating. I'm sure any extra money sent to the e.V. will not be wasted, even if the new server is already bought!\n\nRight Eva? Keep it going! :)\n"
    author: "David"
  - subject: "Me too :)"
    date: 2006-07-21
    body: "Just a little donation of 10 euros... Kde deserves even more, I know.. :-)"
    author: "Dario Massarin"
  - subject: "Indeed"
    date: 2006-07-21
    body: "Just put a few euros their way - KDE really does make a mockery of some software I've purchased in the past.\n\nGo KDE!"
    author: "Greg Loscombe"
  - subject: "done"
    date: 2006-07-21
    body: "donated 5&#8364; right now..."
    author: "kubuntu-user"
  - subject: "ok, some from me..."
    date: 2006-07-23
    body: "ok, i'll add a 35 euro's... i hope for some nice hardware!"
    author: "superstoned"
  - subject: "My 10er is underway"
    date: 2006-07-24
    body: "I send 10. Hope you get enough for good new Hardware.\nHermann"
    author: "Hermann"
  - subject: "First FOSS donation"
    date: 2006-07-24
    body: "I usually donate ~100 e per year for various human rights & environmental organizations (Amnesty, Friends of Earth, etc). That's notable amount of money for a student like me. I've never donated to free software movement, although been using FOSS for several years. So, today I made my first donation to KDE. Hope it will help to make the world a better place :-)\n"
    author: "Yaggo"
---
It's hot and you're melting? The KDE.org hardware infrastructure owned by KDE e.V. is melting as well! Out of the desperate need to upgrade our current disk RAID, we need new hard drives. If you have visited <a href="http://bugs.kde.org/">bugs.kde.org</a> any time the last couple of months, you've noticed that this site often responds extremely sluggish. To improve the situation, we need to employ a new server, but need some more money for the hard drives for this beast! If you can help us with that, please consider <a href="http://bugs.kde.org/donate.cgi">to donate to the fundraiser</a>.  All amounts happily accepted through <a href="http://www.kde.org/support/support.php">Paypal and other means</a>.









<!--break-->
