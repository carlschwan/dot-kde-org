---
title: "Scribus 1.3.3 Release - Printemps"
date:    2006-03-30
authors:
  - "mrdocs"
slug:    scribus-133-release-printemps
comments:
  - subject: "Printemps"
    date: 2006-03-30
    body: "Just for reference, \"Printemps\" is French for \"Spring\" (as in, the season).\n\nLong live Scribus!"
    author: "James"
  - subject: "Re: Printemps"
    date: 2006-03-30
    body: "thats funny, i was just about to ask \"why do all scribus release names are in french?\". Is the release dude french or just learning?"
    author: "Patcito"
  - subject: "Re: Printemps"
    date: 2006-03-30
    body: "I hope nobody here needs help for such a simple word."
    author: "Flavio"
  - subject: "Re: Printemps"
    date: 2006-03-30
    body: "I know I did. And it is a shame to admit I had French classes in school for a few years. :P"
    author: "blacksheep"
  - subject: "Re: Printemps"
    date: 2006-03-30
    body: "Grrrr. Why is this a simple word? For you perhaps, for me not. It doesnt even closely resemble any word for spring in any language I know."
    author: "Martin"
  - subject: "Re: Printemps"
    date: 2006-03-31
    body: "Well, as a native french speaker, and english learner, I must admit that the word \"Printemps\" is not that evident to understand for non-frenchies...\n\nBy the way, its pronounced \"Pray-tah\"... although an english speaker would probably break his jaw trying to say it with the \"really right\" pronounciation :)\n"
    author: "PaT"
  - subject: "Re: Printemps"
    date: 2006-04-01
    body: "Well, \"printemps\" looks like the words in the other romanic languages\n- primavera (espanol, italian)\n- primavera (portugese)\n- prim&#259;var&#259; (roumanian)\n- printempo (esperanto)\n\nnot very far for english neither :\nprin-temps == prime-time"
    author: "jmfayard"
  - subject: "Re: Printemps"
    date: 2006-04-02
    body: "As a spanish speaker, printemps looks as much as primavera as the word printing in english does.\n\nOr like pringoso."
    author: "Roberto Alsina"
  - subject: "Re: Printemps"
    date: 2006-04-03
    body: "I speak Portuguese and I wouldn't get it. But after you mentioned, I can see some resemblance."
    author: "blacksheep"
  - subject: "Re: Printemps"
    date: 2006-03-31
    body: "So after Libert\u00e9 and \u00c9galit\u00e9, no Fraternit\u00e9 ? No Choucroute either ? I am quite surprised by printemps, but well, spring is here at least/last.\n\nSo what's next... 1.3.4 will be \"Galerie Lafayette\" ? \"Redoute\" ?"
    author: "oliv"
  - subject: "Re: Printemps"
    date: 2006-03-31
    body: "The joke will be lost on any non-french, non Paris or suburbs living french, I'm afraid.\nAs \"Printemps\" is one of the big (and expensive) Paris store, so is \"Galeries Lafayette\" or \"Redoute\"."
    author: "Ookaze"
  - subject: "Re: Printemps"
    date: 2006-04-01
    body: "Printemps, Redoute, Fnac and many more are own by the same guy.\nSo no need to advertise a thief. The price are the highest than every where else in the world.\n\nI prefer Printemps as the season. Nice to see the snow melting :)"
    author: "JC"
  - subject: "Re: Primavera"
    date: 2006-04-02
    body: "I am from Brasil. I never saw snow in my hole life.\nI have no idea how snows looks like.\n:-)"
    author: "Nelson Rubens"
  - subject: "Re: Primavera"
    date: 2006-04-04
    body: "Well... snow. It is mostly white. It is also cold and now it is even very wet as it is melting away. There is plenty of it left still here in the southest part of Finland.\n:-)"
    author: "hebe"
  - subject: "I just used Scribus"
    date: 2006-03-30
    body: "I just used Scribus 1.2.4 to layout some booklets for a family event, and wow!  They came out looking great, and it was no effort at all!  And, I was using an outdated version...Can't wait to try this one.  Thanks, Scribus Team!"
    author: "Benjamin Kudria"
  - subject: "Re: I just used Scribus"
    date: 2006-03-30
    body: "I write freelance for a UK computer mag, Micro Mart (http://www.micromart.co.uk). Today they began a 5 part series by me on Scribus, and I persuaded the editor to let me submit the piece in PDF from Scribus (normally I send text and pics and they lay it out) It looks OK, in fact if I could afford to use the same font it would look perfect!\n\nNow they want an 8 week 3 page per week series on using Scribus with a simple 'test' at the end, and this is a mainstream PC user mag not esoteric Linux developer publication.  We're getting there!\n\nCheers,\nPhil"
    author: "Phil Thane"
  - subject: "opentype features"
    date: 2006-03-30
    body: "Does Scribus 1.3.x support opentype features, e.g. \"locl\"? It seems Scribus 1.2.x has hot support for them."
    author: "Andrey V. Panov"
  - subject: "Re: opentype features"
    date: 2006-03-30
    body: "\"hot\" is really \"not\". "
    author: "Andrey V. Panov"
  - subject: "Re: opentype features"
    date: 2006-03-30
    body: "A lot more coming soon... :)"
    author: "Craig Bradney"
  - subject: "Good over-all effort"
    date: 2006-03-30
    body: "The page layouts come excellent. However the interface seems somewhat less mature. Petty display problems on the windows build are showing up. And yes! it is quite intolerant towards fonts."
    author: "Manik Chand"
  - subject: "Re: Good over-all effort"
    date: 2006-03-30
    body: "Thanks, yes there are some display issues, but we are basically rewriting the whole display system, coming in 1.3.4-1.3.5 is the new text layout and rendering system. Yes, we are particularly strong in our stance on fonts.. if its broken at all, we reject it (due to the fact that everything we produce gets tested by commercial RIPS etc). We will be changing this in future to allow single glyphs in fonts to be broken and rejecting only those."
    author: "Craig Bradney"
  - subject: "I'm thrilled with Scribus"
    date: 2006-03-30
    body: "I'm pleased there is a new release. This product just gets better. I have used it to print thousands of flyers and with great results! Keep up the great work!"
    author: "Kevin Colyer"
  - subject: "deb packages"
    date: 2006-03-30
    body: "Am I blind, or are there really no deb packages on the download site?"
    author: "Frank"
  - subject: "Re: deb packages"
    date: 2006-03-30
    body: "Try http://debian.scribus.net. Information is there on how to setup your repositories etc in your system for various forms of Debian."
    author: "Craig Bradney"
  - subject: "Any chance of KDE integration?"
    date: 2006-03-30
    body: "I'd really like to use e.g. kioslaves with scribus. I appreciate they want to remain crossplatform - maybe when kde4 is out they can become a true KDE app?"
    author: "mikeyd"
  - subject: "international input"
    date: 2006-03-31
    body: "I cannot type in Russian in this version, it seems international input in X is broken."
    author: "Andrey V. Panov"
  - subject: "Re: international input"
    date: 2006-03-31
    body: "This input work on another computer, so it may be configuration specific."
    author: "Andrey V. Panov"
  - subject: "Otf fonts in pdf"
    date: 2006-03-31
    body: "As I can see Scribus embed text with OTF/CFF fonts into pdf as bitmaps. Have you plans to embed outlined OTF/CFF fonts into pdf?"
    author: "Andrey V. Panov"
  - subject: "Re: Otf fonts in pdf"
    date: 2006-04-05
    body: "Correction, Scribus exports OTF as scalable vector outlines. \n\nYes, there is a great deal of work going in 1.3.4/1.3.5 to support far more advanced OTF features. :)"
    author: "mrdocs"
  - subject: "No Debian for 1.3.3."
    date: 2006-03-31
    body: "I followed the direction on the debian page posted above and I get sribus 1.2.4.1. Anyone else having the same problem"
    author: "tikal26"
  - subject: "Re: No Debian for 1.3.3."
    date: 2006-04-02
    body: "No, I didn't get the same problem. Of course, you did notice that you had two packages and the one providing 1.3.3 was the scribus-cvs one, didn't you ?"
    author: "W."
  - subject: "transparent png's"
    date: 2006-03-31
    body: "Is it planned to support transparent png's and so on in the future?"
    author: "psy"
  - subject: "Re: transparent png's"
    date: 2006-04-03
    body: "Transparent pngs are supported by scribus already!\n\nNote: the default background colour for an image frame is white, so when you insert the transparent png, you see the white image frame background. If you want your png to have a truly transparent look, seeing thru the image frame as well, you need to set the image frame's background to none.\n"
    author: "Jonathan Dietrich"
  - subject: "does it support ODF?"
    date: 2006-04-04
    body: "I couldn't find the answer searching http://www.scribus.net/ and http://wiki.scribus.net/ for \"ODF\" (no matches) and \"Open Document\" (odd results).\n\nhttp://opendocumentfellowship.org/Applications/Scribus says Scribus 1.3.1 only \"Supports import of text/draw files into text and image frames\"."
    author: "S Page"
  - subject: "Re: does it support ODF?"
    date: 2006-04-05
    body: "Scribus has a built in .odt importer for OASIS documents, along with an .odw importer for drawings. OpenOffice.org 1.x formats are also imported .sxw .sxd\n\nThere are detailed notes on the importers in the builtin help browser."
    author: "mrdocs"
  - subject: "Without CUPS"
    date: 2006-04-04
    body: "When configuring I get this: WARNING: \n\nlibcups could not be found. Printing functionality will be limited.\n\nWhat are the limitations?  Do I need to install CUPS even though I don't use it to print?\n\nNote that I don't use CUPS because it fails to find fonts.\n"
    author: "James Richard Tyrer"
---
The Scribus Team is pleased to announce the release of Scribus 1.3.3, codenamed "Printemps".  The 1.3.3 release is the fourth development version working towards a new stable 1.4. Within this release period over 200 bugs and feature requests were completed mostly focused on useability and correctness. See the <a href="http://www.scribus.net/index.php?name=News&amp;file=article&amp;sid=117">release announcement</a> for some of the major changes.  Download GNU/Linux source, RPMS and Debs from <a href="http://sourceforge.net/project/showfiles.php?group_id=125235&amp;package_id=136924&amp;release_id=404799">Scribus Sourceforge downloads</a> or get pre-built versions for <a href="http://windows.scribus.net">Windows</a> or <a href="http://aqua.scribus.net">Mac OS X</a>.



<!--break-->
