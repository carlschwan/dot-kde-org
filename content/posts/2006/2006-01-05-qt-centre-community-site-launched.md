---
title: "Qt Centre Community Site Launched"
date:    2006-01-05
authors:
  - "numanee"
slug:    qt-centre-community-site-launched
comments:
  - subject: "pagerank"
    date: 2006-01-05
    body: "Who would of thought pagerank would cause so many unintentional problems. Wiki spamming and now this.\n\nAnyways, I've found Qt Forum to be helpful in the past, I'll forgive the en_GB spelling. ;)"
    author: "Ian Monroe"
  - subject: "Re: pagerank"
    date: 2006-01-11
    body: "Heh, Don't you mean en_RestOfTheWorldThatIsNotAmerica spelling?  :-)"
    author: "AC"
  - subject: "Re: pagerank"
    date: 2006-01-11
    body: "hey, don't forget that americaIsAContinent :-)"
    author: "MandrakeUser"
  - subject: "Re: pagerank"
    date: 2006-01-11
    body: "um, technically americaIsTwoContinents ;-)"
    author: "Mark"
  - subject: "Re: pagerank"
    date: 2006-01-13
    body: "So is Australia ;)"
    author: "mabinogi"
  - subject: "how about kde-forum.org?"
    date: 2006-01-06
    body: "are you going to abandon it too? in that case will you create a KDEcentre? or just put a kde section on QtCentre?"
    author: "Patcito"
  - subject: "Re: how about kde-forum.org?"
    date: 2006-01-06
    body: "I'm asking this because the last post on old qtforum.org says that the admin of kde-forum.org is the same that caused troubles on old qtforum.org"
    author: "Patcito"
  - subject: "Re: how about kde-forum.org?"
    date: 2006-01-06
    body: "QtCentre does have a KDE forum section."
    author: "Ian Monroe"
  - subject: "Re: how about kde-forum.org?"
    date: 2006-01-06
    body: "One forum, yes."
    author: "Martin"
  - subject: "Hijacked?"
    date: 2006-01-06
    body: "How is this possible? What happened and who is the hijacker?\n"
    author: "reihal"
  - subject: "Re: Hijacked?"
    date: 2006-01-06
    body: "On the bottom of this page is more explaination: http://www.qtcentre.org/"
    author: "leeghoofd"
  - subject: "Re: Hijacked?"
    date: 2006-01-06
    body: "It doesn\u00b4t say who this \"Sigma\" is or why the former owner (who is that?) sold the sites. Strange going-ons.\n"
    author: "reihal"
  - subject: "Re: Hijacked?"
    date: 2006-01-06
    body: "Strange indeed. Why is the identity of Sigma (and everyone else involved for that matter) kept a secret? If the sites & domains were sold, those transactions must have taken place in the Real World where people usually have names?"
    author: "Martin"
  - subject: "Re: Hijacked?"
    date: 2006-01-06
    body: "What good would it do to give you the names?  You can look up the DNS registration of the domains btw."
    author: "I Should Be Working"
  - subject: "Re: Hijacked?"
    date: 2006-01-06
    body: "Yes, what good would it ever do to give \"you\"; the public, any information whatsoever? It is pointless, I agree. Comrade Joe had it all figured out."
    author: "ac"
  - subject: "Re: Hijacked?"
    date: 2006-01-06
    body: "Erm?  The information is public.  Read the articles, the links and comments.  Comprehend and think instead of spouting dogma."
    author: "I Should Be Working"
  - subject: "Re: Hijacked?"
    date: 2006-01-06
    body: "For the unsmart ppl, I do:\n\nhttp://dot.kde.org/1107647369/ -> Christian Kienle\nwhois kde-forum.org -> Herbert Feiler\n\nLook like kde-forum.de is also suffer and is own by Mr Feiler."
    author: "KDE User"
  - subject: "Re: Hijacked?"
    date: 2006-01-07
    body: "I am more interested in what he is. He seems well financed, by himself or others? Is what he does illegal or just immoral?\n\nHerbert Feiler\n95445 Bayreuth\n\nHe runs quite a few websites:\nLinux-Web.de \nGentoo-Forum \nKDE-Forum \nSlackware-Forum \nLinux-Onlineshop\nubuntu-forum.de \n\nand have registerd gnome sites: \"ich habe seit l\u00e4ngere zeit schon die domains gnomeforum.de und gnome-forum.de registriert.\"\n"
    author: "reihal"
  - subject: "Re: Hijacked?"
    date: 2006-01-09
    body: "> Is what he does illegal or just immoral?\n\nIANAL, but I think that unless you can prove intent, you have no basis for a suit. And that would be extremely difficult, since they can just say that those links are just ads. Even if you found a comment from them admiting it, it'd be hard to track to them due to the anonymous nature of the internet.\n\nIf you could prove intent, you might have ground for fraud. But I think that very, very much depends on the country."
    author: "blacksheep"
  - subject: "Re: Hijacked?"
    date: 2006-01-06
    body: "Hijacked is maybe the wrong word.  The domains were sold to someone who paid big money for them.  The names of everyone involved is known."
    author: "I Should Be Working"
  - subject: "Re: Hijacked?"
    date: 2006-01-06
    body: "ok, but they are now used to increase the googlepagerank of several totally unrelated sites, instead of doing what they should do - be a Qt or KDE forum..."
    author: "superstoned"
  - subject: "Re: Hijacked?"
    date: 2006-01-06
    body: "You're right and even the software is slowly breaking but nobody cares.  It's only there to keep up appearances."
    author: "KDE User"
  - subject: "Who can be trusted?"
    date: 2006-01-06
    body: "Now, which KDE sites can be trusted with our time and effort? Only ones owned by the KDE e.V.? Then which are those?"
    author: "Martin"
  - subject: "Who can be trusted (2)"
    date: 2006-01-06
    body: "Ironically, the new Qt Centre contains no information on who is behind the site. There isn't a single name displayed anywhere, except for the strange welcome message being signed by Witold Wysota. \n\nWho owns this thing? What \"license\" applies to information found on the forum and who owns the posts?"
    author: "Martin"
  - subject: "Re: Who can be trusted (2)"
    date: 2006-01-06
    body: "Whois pulls up that the domain is owned by Trolltech. I think we can trust them to not sell their stuff to pagerank spammers. ;)\n\nI'd assume the posts on the forum are owned by the posters, thats how forums are usually."
    author: "Ian Monroe"
  - subject: "Re: Who can be trusted (2)"
    date: 2006-01-06
    body: "As for \"who is behind\":\nhttp://www.qtcentre.org/forum/showgroups.php\n\nMoreover our names are in the commented article, you might have missed them. Besides, all people who visited QtForum know us, we used to moderate that site and our names are still there.\n\nWho owns QtCentre? Well.. we made sure none of us is able to sell it, because we don't have legal ownership to the domain name :) We spoke to both Trolltech and KDE e.V. and decided the Trolls should own the domain. BTW. You can access QtCentre through the secondary domain name, too: QtCenter.org. Axel's name is there in the whois database.\n\nWhat \"license\" applies to information found on the forum and who owns the posts?\n\nWell... according to me, the one who wrote the post, owns it -- it is called \"intelectual property\", right? :) And I know I'm not alone in that opinion, as we discussed the subject in our small \"founding group\".\n\nTo be honest, I don't know where do all these suspicions come from. We have nothing to hide, I think we are \"recognized\" by the community, so you can't say we came from nowhere. If you have questions, PM me or anyone from the QtCentre admin team or ask your questions on QtCentre or QtForum, we'll be happy to answer them to the best of our knowledge."
    author: "Witold Wysota"
  - subject: "Re: Who can be trusted (2)"
    date: 2006-01-06
    body: "<i>>Well... according to me, the one who wrote the post, owns it -- it is called \"intelectual property\", right? :)</i>\n\nWrong, it's called copyright."
    author: "BeS"
  - subject: "Re: Who can be trusted (2)"
    date: 2006-01-06
    body: "Right :) My mistake, and I shouldn't have made _that one_..."
    author: "Witold Wysota"
  - subject: "Re: Who can be trusted (2)"
    date: 2006-01-07
    body: "I was not one bit suspicious myself in fact. I was merely concluding from the preceding debacle that one should know the answer to these questions before investing one's time in any online community. It was an implicit, though admittedly provocative, suggestion that the answers to those questions be made more prominent on the new site. Thanks for posting them here, though.\n\nAs for who owns the posts it is not at all clear I think, given the unconditional copyright statement at the bottom of each page at the site."
    author: "Martin"
  - subject: "Re: Who can be trusted (2)"
    date: 2006-01-09
    body: "> ask your questions on QtCentre or QtForum\n\nAnd these means only questions relating to the situation, 'cause at http://www.qtforum.org/post/64878/lastpost.html it has been posted:\n\nAs of NOW, all of us, the moderators team (see below) will STOP ANSWERING TO QUESTIONS ON THIS FORUM/DOMAIN.\n\nPost all other questions on qtcentre.org - it's a great site."
    author: "Shriramana Sharma"
  - subject: "please add jabber in profiles"
    date: 2006-01-07
    body: "in the \"Instant Messaging\" section please add jabber/gtalk support please, I think you had the same request on the old qtforum back then :)"
    author: "Patcito"
---
<a href="mailto:wysota@qtcentre.org">Witold Wysota</a> wrote in to inform us of the newly launched <a href="http://www.qtcentre.org/">Qt Centre</a> which is being billed as the ultimate Qt community site.  With the support of <a href="http://www.trolltech.com/">Trolltech</a>, Witold and former Qt Forum administrators, moderators and fans Axel Jaeger, Daniel Kish, Kevin Krammer, Johan Thelin,  Jacek Piotrowski and Michael Goettsche have banded together to form the new site after learning that the <a href="http://www.qtforum.org/" rel="nofollow">Qt Forum</a> as well as <a href="http://www.kde-forum.org/" rel="nofollow">KDE-Forum.org</a> had been <a href="http://www.qtforum.org/post/64878/lastpost.html" rel="nofollow">hijacked</a> for the purposes of boosting the Google Page Rank of unrelated external sites and have otherwise become neglected.  If you had any public links to either Qt Forum or KDE-Forum.org please consider removing them or using <a href="http://www.google.com/webmasters/bot.html#www">the nofollow attribute</a>.  Let us hope that the new Qt Centre flourishes!






<!--break-->
