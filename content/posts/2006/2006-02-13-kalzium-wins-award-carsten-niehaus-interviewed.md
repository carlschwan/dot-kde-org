---
title: "Kalzium Wins Award; Carsten Niehaus Interviewed"
date:    2006-02-13
authors:
  - "prodrigues"
slug:    kalzium-wins-award-carsten-niehaus-interviewed
comments:
  - subject: "Congrats! ... and a couple corrections"
    date: 2006-02-12
    body: "Good article and congradulations on the award Carsten! I use Kalzium some for my chemistry class. The type-ahead feature sounds great... I'd use that just for convenience. :)\n\nCorrections:\n\n\"What was you most brillant hack\" should be \"what was your most brilliant hack\"\n\nAnd also there are two sections that the response is formatted in the same big blue font that the question is in. Not that it matters that much :)\n\n-Sam"
    author: "Sam Weber"
  - subject: "Re: Congrats! ... and a couple corrections"
    date: 2006-02-12
    body: "and \"12 January 2006\" should be \"12 February 2006\" :)"
    author: "ac"
  - subject: "What is missing?"
    date: 2006-02-12
    body: "the interview says that many programs @ his school still run windows. I just want to know what programs are this ? ist there still stuff missing ?\n\n\nbtw. for electronic classes there ist ktechlab (http://ktechlab.org/)\n\nch"
    author: "chris"
  - subject: "Re: What is missing?"
    date: 2006-02-13
    body: "All the big schoolbook vendors only produce software for Windows, sometimes for Mac. So each and every single school-book software is missing (there are many such programs, perhaps about 3 per schoolbook)\n\nThe situation with 3d-viewers for proteins and other big molecules is now much butter than two years ago, but still those for windows are oftern better.\n\nD-GISS. D-GISS is a software almost every (at least german) school has but which doesn't work in linux as it is somehow based on MS-Access. It is not used for teaching but more a database. Still, I would really like to be able to run D-GISS. It doesn't even install in wine.\n\nThere are many more, especially the situation with school-book software is a shame. And the vendors don't even answer email when you contact them about it.\n\nBtw: About the drawing tools: Egon Willighagen pointed me to \"his\" java based drawing tool. It doesn't crash and is the best I know for Linux, but still ACDLabs is much better. Sorry that I have to say that :("
    author: "Carsten Niehaus"
  - subject: "Re: What is missing?"
    date: 2006-02-14
    body: "It looks nice and may in certain situations be usefull as a teaching tool, but in many ways ktechlab are a toy. Besides electronics are one of the areas where Linux support is good, several of the major vendors have Linux versions of their tools.\n\nIn other areas the situation are not that good, increased addoption of Java has improved the situation some. And you can even find helpfull tools in the form of Java applets scattered around the net. Usually very specialized, but usefull to help explain/understand different concepts. Like this collection: http://www.falstad.com/mathphysics.html "
    author: "Morty"
  - subject: "Wikipedia"
    date: 2006-02-12
    body: "Was wikipedia support ever added to kalzium?  Or is it still planned/just a wish?"
    author: "John Tapsell"
  - subject: "Re: Wikipedia"
    date: 2006-02-13
    body: "We need the Webapi of MediaWiki first, so it is a KDE4-task."
    author: "Carsten Niehaus"
  - subject: "Re: Wikipedia"
    date: 2006-02-13
    body: "No, you dont need KDE4 - look how Amarok did it :-)"
    author: "Hans"
  - subject: "Re: Wikipedia"
    date: 2006-02-13
    body: "Amarok is only parsing the html the wikipedia spits out, removes the wikipedia-stuff and displays the content. What if the Wikipedia changes the html? Amarok would need to be patched. Also, the integration is much more than just displaying an article. "
    author: "Carsten Niehaus"
  - subject: "Re: Wikipedia"
    date: 2006-02-14
    body: "Wikipedia has special url to get the raw data for an article.  I'd have to look it up, but it's something like ?raw"
    author: "John Tapsell"
  - subject: "Re: Wikipedia"
    date: 2006-02-14
    body: "?action=raw for the raw wiki text e.g.\nhttp://en.wikipedia.org/wiki/RMS_Titanic?action=raw\n\nThis is however not really useful, because you get the unformatted wiki text.\n"
    author: "Jos"
  - subject: "Re: Wikipedia"
    date: 2006-02-14
    body: "Why is that not really useful? I thought the pre-processed data is what was wanted to begin with? (That this needs to be processed first to embed images and links etc. is obvious, but this also offers added flexibility.)"
    author: "ac"
  - subject: "Re: Wikipedia"
    date: 2006-02-14
    body: "We want far more, for example an API for:\n\n\"give me an article for the word \"Enzyme\". If that article is available in one of these languages return in one of these languages (in order of listing), if not return the english article [de,fr,it]\"\n\nThat is not possible now. Or\n\n\"Give me articles related to the article \"Enzyme\"\"\n\nI hope you can now see we need much more then \"give me article 'x'\"."
    author: "Carsten Niehaus"
  - subject: "ktechlab"
    date: 2006-02-13
    body: "I tried compiling it...\ngot some errors:\n\n(...........)\nmake[3]: Entering directory `/root/Downloads/ktechlab-0.3/src'\nsource='itemgroup.cpp' object='itemgroup.o' libtool=no \\\ndepfile='.deps/itemgroup.Po' tmpdepfile='.deps/itemgroup.TPo' \\\ndepmode=gcc3 /bin/sh ../admin/depcomp \\\ng++ -DHAVE_CONFIG_H -I. -I. -I.. -I../src -I../src/drawparts -I../src/electronics -I../src/electronics/components -I../src/electronics/simulation -I../src/flowparts -I../src/gui -I../src/languages -I../src/mechanics -I../src/micro -I/opt/kde/include -I/usr/lib/qt/include -I/usr/X11R6/include   -DQT_THREAD_SUPPORT  -D_REENTRANT  -Wnon-virtual-dtor -Wno-long-long -Wundef -ansi -D_XOPEN_SOURCE=500 -D_BSD_SOURCE -Wcast-align -Wconversion -Wchar-subscripts -Wall -W -Wpointer-arith -Wwrite-strings -O2 -Wformat-security -Wmissing-format-attribute -fno-exceptions -fno-check-new -fno-common  -c -o itemgroup.o `test -f 'itemgroup.cpp' || echo './'`itemgroup.cpp\nitemgroup.cpp: In member function `void ItemGroup::slotDistributeHorizontally()\n   ':\nitemgroup.cpp:239: error: ISO C++ forbids declaration of `multimap' with no\n   type\nitemgroup.cpp:239: error: template-id `multimap<double, Item*>' used as a\n   declarator\nitemgroup.cpp:239: error: parse error before `;' token\nitemgroup.cpp:241: error: `DIMap' undeclared (first use this function)\nitemgroup.cpp:241: error: (Each undeclared identifier is reported only once for\n   each function it appears in.)\nitemgroup.cpp:244: error: `ranked' undeclared (first use this function)\nitemgroup.cpp:244: error: `make_pair' undeclared in namespace `std'\nitemgroup.cpp:249: error: ISO C++ forbids declaration of `DIMap' with no type\nitemgroup.cpp:249: error: uninitialized const `DIMap'\nitemgroup.cpp:249: error: parse error before `::' token\nitemgroup.cpp:250: error: parse error before `::' token\nitemgroup.cpp:250: error: name lookup of `it' changed for new ISO `for' scoping\nitemgroup.cpp:243: error:   using obsolete binding at `it'\nitemgroup.cpp:250: error: `rankedEnd' undeclared (first use this function)\nitemgroup.cpp:252: error: base operand of `->' has non-pointer type `\n   QValueListIterator<GuardedItem>'\nitemgroup.cpp:263: error: parse error before `::' token\nitemgroup.cpp:265: error: base operand of `->' has non-pointer type `\n   QValueListIterator<GuardedItem>'\nitemgroup.cpp:265: error: base operand of `->' has non-pointer type `\n   QValueListIterator<GuardedItem>'\nitemgroup.cpp:265: error: base operand of `->' has non-pointer type `\n   QValueListIterator<GuardedItem>'\nitemgroup.cpp:268: error: base operand of `->' has non-pointer type `\n   QValueListIterator<GuardedItem>'\nitemgroup.cpp:249: warning: unused variable `const int DIMap'\nitemgroup.cpp: In member function `void ItemGroup::slotDistributeVertically()':\nitemgroup.cpp:285: error: ISO C++ forbids declaration of `multimap' with no\n   type\nitemgroup.cpp:285: error: template-id `multimap<double, Item*>' used as a\n   declarator\nitemgroup.cpp:285: error: parse error before `;' token\nitemgroup.cpp:290: error: `make_pair' undeclared in namespace `std'\nitemgroup.cpp:295: error: ISO C++ forbids declaration of `DIMap' with no type\nitemgroup.cpp:295: error: uninitialized const `DIMap'\nitemgroup.cpp:295: error: parse error before `::' token\nitemgroup.cpp:296: error: parse error before `::' token\nitemgroup.cpp:296: error: name lookup of `it' changed for new ISO `for' scoping\nitemgroup.cpp:289: error:   using obsolete binding at `it'\nitemgroup.cpp:298: error: base operand of `->' has non-pointer type `\n   QValueListIterator<GuardedItem>'\nitemgroup.cpp:309: error: parse error before `::' token\nitemgroup.cpp:311: error: base operand of `->' has non-pointer type `\n   QValueListIterator<GuardedItem>'\nitemgroup.cpp:311: error: base operand of `->' has non-pointer type `\n   QValueListIterator<GuardedItem>'\nitemgroup.cpp:311: error: base operand of `->' has non-pointer type `\n   QValueListIterator<GuardedItem>'\nitemgroup.cpp:314: error: base operand of `->' has non-pointer type `\n   QValueListIterator<GuardedItem>'\nitemgroup.cpp:295: warning: unused variable `const int DIMap'\nmake[3]: *** [itemgroup.o] Error 1\nmake[3]: Leaving directory `/root/Downloads/ktechlab-0.3/src'\nmake[2]: *** [all-recursive] Error 1\nmake[2]: Leaving directory `/root/Downloads/ktechlab-0.3/src'\nmake[1]: *** [all-recursive] Error 1\nmake[1]: Leaving directory `/root/Downloads/ktechlab-0.3'\nmake: *** [all] Error 2\nbash-2.05b#\n\nAny ideas?\n\nI really want to use this for simulating circuits and things"
    author: "Forrest McKerchar"
  - subject: "Re: ktechlab"
    date: 2006-02-13
    body: "If you want a quick&dirty solution, try to remove the -ansi parameters in the generated Makefile files.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: ktechlab"
    date: 2006-02-13
    body: "I removed all instances of -ansi from all the generated Makefiles, but the errors didn't go away.\n\nUpgraded to gcc-3.4 and the errors disapeared :-)"
    author: "Forrest McKerchar"
  - subject: "still off by one"
    date: 2006-02-13
    body: "7 years of vim and still one keystroke too much... s/:wq/:x/ ;-)"
    author: "Michael Brade"
  - subject: "Re: still off by one"
    date: 2006-02-13
    body: "Yes, that is the only one thing I cannot remember in vim :) I will always use :wq, sorry ;-)\n\n:wq"
    author: "Carsten Niehaus"
  - subject: "HTML Tags missing"
    date: 2006-02-13
    body: "In the Interview there are some </h3> HTML Tags missting.\nLook at \"Which text editor do you use? Why?\" for example.\nThe whole opart is written as a headline.\n\nCalle"
    author: "Calle"
  - subject: "JUST CONGRATULATIONS"
    date: 2006-02-13
    body: "to Carsten and all other ppl that contributed to Kalzium and the other kde-edu developers! you guys are doing a great job, its very cool to show off your apps and i'm sure they are being used, and will be used even more :D"
    author: "superstoned"
  - subject: "Congrats to everyone"
    date: 2006-02-14
    body: "And perhaps when KDE 4 arrives you can create a \"Pro\" version and earn some compensation?\n\nhttp://digitalscience.free.fr/\n\nThis company does a nice one for OS X."
    author: "Marc J. Driftmeyer"
  - subject: "Re: Congrats to everyone"
    date: 2006-02-14
    body: "I know that table from the screenshots, looks pretty nice, yes.\n\nIf somebody want to donate money: I have a bank-account and an amazon-wishlist (german amazon). It would be really nice to recieve a DVD or two of course :-) But Kalzium will always be free as in beer and as in freedom."
    author: "Carsten Niehaus"
  - subject: "Thank you"
    date: 2006-02-15
    body: "Just a big \"Thank you!\" to Carsten and all the kdeedu developers for the amazing applications they are creating.  My wife and I educate our children at home, and the KDE programs are proving extremely useful.  Please keep up the good work!\n"
    author: "David Joyce"
  - subject: "TeX?"
    date: 2006-02-15
    body: "What does he mean there is no Linux program for scientific drawing of compounds? That's just silly, so what has all the chemical physicists used all those years when the rest of the scientific world has been writing in TeX/LaTeX? Probably something like XyMTeX, that's what."
    author: "Jonas"
  - subject: "Re: TeX?"
    date: 2006-02-15
    body: "Jonas, of course you can draw them. I studied chemistry myself without touching  non-free software. I used xfig, inkscape, latex, xdrawchem and so on. But those tools are absolutly not usable for a regular chemistry teacher. If you want Linux in School you need software for teachers and students alike.\nFurthermore, ACDLabs is *much* better than any !windows solution out there. It is easy to use, fast, high quality, supports all kinds of calculations, has a very good 3d-mode, names molecules for you and so on. *That* is what we need, not a ChemTex-solution where you need to read 10 howtos to draw Acetylesalicyleacid!"
    author: "Carsten Niehaus"
---
A <a href="http://edu.kde.org/kalzium/">Kalzium</a> double bill today, with Kalzium gaining recognition in OsnaBrück University's annual prize giving, and this week's <a href="http://people.kde.nl">People Behind KDE</a> interview.  Find out everything you wanted to know about chemistry, the small print on toothpaste, and why not to visit Bavaria in the People Behind KDE <a href="http://people.kde.nl/carsten-niehaus.html">interview with Carsten Niehaus</a>, author of Kalzium. Read on for details of the prize.











<!--break-->
<div style="border: thin solid grey; padding: 1ex; margin: 1ex; width 466px; float: right;">
<a href="http://static.kdenews.org/jr/kalzium.png"><img src="http://static.kdenews.org/jr/kalzium-thumb.png" width="466" height="254" border="0" /></a>
</div>

<p>KDE this week gained further recognition from the wider world.  In an awards 
ceremony at Osnabrück University in Germany, <a href="http://www2.uni-osnabrueck.de/pressestelle/mitteilungen/Detail.cfm?schluessel_nummer=41&schluessel_jahr=2006">Carsten Niehaus won the 
Intevation Prize for Achievements in Free Software</a> for his work on Kalzium, KDE's interactive periodic table. The judges from Intevation praised the interactive features of Kalzium which help students by making facts easily discoverable. The prize was open to all past and present 
members of the University.</p>

<p>Accepting a cheque for &#8364;750, Carsten said "<em>It is an honour to be rewarded for my project Kalzium. I created it to have a good tool for my own use, but  with a great community behind me I was able to develop something for others that I am proud of. A prize like this helps to keep up the motivation to improve Kalzium to be the tool of choice for teacher and student alike!</em>" Kalzium, which takes its name from the German for the element calcium, supports many advanced features, including plotting data from all elements to show trends in the mass or atomic size for example. Its ease of use and range of features have won users in Osnabrück as well as further afield: Egon 
Willighagen, lead developer of <a href="http://blueobelisk.org">BlueObelisk</a> and <a href="http://cdk.sf.net">CDK</a>, says, "<em>Kalzium brings the 
core chemistry in an easy-to-browse way to the desktop</em>".</p>

<p>Kalzium is part of the <a href="http://edu.kde.org">KDE Edutainment</a> project, which provides educational 
software for all ages. "<em>I am really pleased to see a KDE-Edu program getting another award. Kalzium's success is due to Carsten's constant efforts to improve his software and I am very proud to see him getting this award</em>", Anne-Marie Mahfouf, a member of the KDE Edutainment team, said.</p>









