---
title: "Ark Linux 2006.1-rc1 Released with KDE 3.5.1"
date:    2006-03-21
authors:
  - "brosenkraenzer"
slug:    ark-linux-20061-rc1-released-kde-351
comments:
  - subject: "KDE 3.5.2"
    date: 2006-03-21
    body: "is it late...? where's my newly polished kde 3.5.2?"
    author: "andrew"
  - subject: "Re: KDE 3.5.2"
    date: 2006-03-21
    body: "Where has 3.52 been released? I do not see it on the kde.org homepage."
    author: "Shriramana Sharma"
  - subject: "Re: KDE 3.5.2"
    date: 2006-03-21
    body: "rpm for suse ???????"
    author: "vicko"
  - subject: "Re: KDE 3.5.2"
    date: 2006-03-22
    body: "Even 3.5.1 packages from SUSE are riddled with annoying bugs. Some KDEs, some SUSEs. Kontact crashes on exit, kicker crashes on exit, kates embedded terminal does not redraw whole window, kates 'find in files' always opens new file instead of already open one, kio_ldap is completely dead ('internal error'), kdesu's password saving does not work, ..\n"
    author: "Anonymous Coward"
  - subject: "Re: KDE 3.5.2"
    date: 2006-03-22
    body: "\nWell, since they're unsupported packages, I guess maybe I can live with them.\n\nWhat makes me wonder is that the 10.1 release seems to only work on KDE for backwards compatibility! (actually that would be forwards compatibility ;-) , but whatever). They provide KDE frontends to (most) of the new tools, but the new tools themselves are never KDE.\n\nBtw, has it just become impossible to run a Qt only SuSE system? I mean ZenUpdater, which isn't even 'backwards compatible'. (Not that I'm running a Qt-only system myself, but that's not the point)."
    author: "ac"
  - subject: "Re: KDE 3.5.2"
    date: 2006-03-25
    body: "Pragmatic approach: subscribe to suse-security-announce, stop zen-updater, rczmd stop"
    author: "Anonymous"
  - subject: "Re: KDE 3.5.2"
    date: 2006-03-23
    body: "I've never had problems like Kontact crashing on exit."
    author: "Segedunum"
  - subject: "Re: KDE 3.5.2"
    date: 2006-04-06
    body: "Try switching to PCLinuxOS download and instal or run from disk (install pretty easy) this distro auto updates easily and when updated it runs KDE 3.5.2 even prior to 3.5.2 this distro ran perfectly on two computers at home (and still does} with no problems. Plus KDE 3.5.2 looks pretty!!!"
    author: "David F"
  - subject: "Re: KDE 3.5.2"
    date: 2006-03-21
    body: "KDE 3.5.2 has been given to packagers first for one week as usual."
    author: "Anonymous"
  - subject: "Re: KDE 3.5.2"
    date: 2006-03-22
    body: "There is no such thing as KDE 3.5.2 yet (if any distribution claims to have it, that's a lie, and probably a semi-random snapshot of the 3.5 SVN branch) -- but if it is released soon, it will be part of 2006.1 final.\nWe already include various fixes from post-3.5.1 SVN.\n"
    author: "Bernhard Rosenkraenzer"
  - subject: "Re: KDE 3.5.2"
    date: 2006-03-22
    body: "KDE 3.5.2 is tagged in SVN - it may be retagged before release as it already happened but it's far from a \"semi-random snapshot\"."
    author: "Anonymous"
  - subject: "Tomahawk is already offering KDE 3.5.1"
    date: 2006-03-21
    body: "Everybody else is offering Firefox, OpenOffice, etc? How does Ark Linux different from others? Is it just packages more recent?\n\nWhat defenses Ark Linux deployed to stop virus, worm, Phishing/Pharming, remote hacking/hijacking attacks, etc. If you are looking forward to something new than package versions, its worth have a look at the Tomahawk Desktop (www.tomahawkcomputers.com), the multimedia Linux OS for desktops and laptops.\n\nSee this DesktopLinux.com article (http://desktoplinux.com/news/NS7069459557.html) to get a good understanding on Tomahawk Desktop."
    author: "Anonymous"
  - subject: "Re: Tomahawk is already offering KDE 3.5.1"
    date: 2006-03-21
    body: "http://www.arklinux.org/\nhttp://en.wikipedia.org/wiki/Ark_Linux\nGood places to start.\n\nThe Tomahawk site isn't very good for comparing it against other distros, since most of the stuff on the site is about Linux in general, with very little about whats unique with Tomahawk specifically.  Also the screenshot section shows very little other than wallpapers."
    author: "Corbin"
  - subject: "Re: Tomahawk is already offering KDE 3.5.1"
    date: 2006-03-21
    body: "Mmmm, spam. If, as it appears, tomahawk is a gnu/linux distro with distrbution of gpld software (kde, linux to name two) i suggest they review their software license because it doesn't look like the gpl to me"
    author: "anon"
  - subject: "Interesting Distro"
    date: 2006-03-22
    body: "The package management is neat...but I wouldn't ever use it.  It\ndoesn't actually solve any of my real package management problems."
    author: "Stanaland"
  - subject: "Re: Tomahawk is already offering KDE 3.5.1"
    date: 2006-03-22
    body: "Ark Linux is different in various ideas -- generally improvements to make the system easy to install, learn and use for everyone while not taking away the features power users need, and remaining 100% Free Software.\nFor example, Ark Linux allows you to install packages without bothering to become root, through a special PAM setup that allows the main user to run graphical setup tools as root automatically.\nAlso, Ark Linux is 100% KDE centric.\n\nAll Linux systems are pretty much safe from virus/worm/... attacks - Ark Linux doesn't doesn't need to do anything special there.\n\nMost of the \"unique features\" this Tomahawk thing boasts about sound like marketing misinformation, and many are actually bad ideas.\nHow do you implement dependency resolution if you don't keep a package database? e.g. a user didn't install GTK, but wants to use gimp. Not a problem for most -- \"apt-get install gimp\" (or yum/smart/urpm/.... on other distributions) will take care of it. But without keeping track of dependencies?\n\nOther than that (and the fact that they claim generic Linux features as unique to their OS), their distribution doesn't look too bad either."
    author: "Bernhard Rosenkraenzer"
  - subject: "Multimedia support?"
    date: 2006-03-21
    body: "Can someone tell me if this distribution has MP3/DVD out of the box? What about nvidia closed drivers?"
    author: "blacksheep"
  - subject: "Re: Multimedia support?"
    date: 2006-03-22
    body: "It has MP3/DVD/MPEG-4/... out of the box.\n\nNo closed source mess in a base install, but you can just \"apt-get install nvidia\" on a running system."
    author: "Bernhard Rosenkraenzer"
  - subject: "Ark Linux"
    date: 2006-03-27
    body: "What's the point of using ark linux when lots of other distributions ship KDE? take gentoo, take freebsd, take pkgsrc from NetBSD which can be crosscompiled on lots of other architectures, take Slackware even.\n</spam>\nArk Linux is just a poor-piss distribution. The \"developers\" if you can call them like that, are not even able to create a decent installer. It's some sort of TCL/Tk interface which sucks a lot. It simply doesn't work. I couldn't get it installed and I simply dropped it. It's not worth it.\nI think a curses-based installation menu (or even dialog-based) is everything the Ark devs need. Anyway, I don't give a shit on your crappy distribution. You say you ship the latest version of KDE and the fucking installer crashes like crazy."
    author: "Andrei"
  - subject: "Re: Ark Linux"
    date: 2006-03-28
    body: "i HAVE to ask - are you entirely sure you're talking about the right distribution here? To me it seems one of two things have happened here: You were stupid enough to press the topmost button on the installer (the one with the big, red letters saying \"THIS WILL ERASE EVERYTHING ON YOUR SYSTEM!\") or well, you simply haven't got a clue what you're on about :)\n\nYes, it crashes from time to time on some hardware configurations (it's a zero-budget distribution, they simply don't have the ENORMOUS resources (mention i did not say money) required to test this sort of thing (something which is available to e.g. Gentoo, Fedora, Mandriva, Ubuntu) - and since the users don't report back the sort of problems you have encountered hey, how do you suspect they might get known about?). And, just to point out a piss-poor supposition made in your ranty and flame-bait prone post: The installer is based on Qt/Embedded and written in C++."
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: Ark Linux"
    date: 2006-03-28
    body: "Weeh, i took the flamebait and ran around the block with it, it's miiiine now! ;D *ducks*"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Ark really shows the power of qt"
    date: 2006-03-28
    body: " Ark is a fantastic distro which is designed to show off qt at it's finest, and IMO does just that. Sure suse, slackware, and most any distro will run kde...but Ark makes it stand out. The lack of gtk makes the apps integrate seamlessly and truely centralize the settings. Props to the devels :)"
    author: "Storyteller"
---
KDE distribution <a href="http://www.arklinux.org/">Ark Linux</a> has <a href="http://www.arklinux.org/index.php?option=com_content&task=view&id=7&Itemid=1&lang=en">released the first release candidate</a> of version 2006.1.  It includes the latest KDE 3.5.1, OpenOffice.org 2.0.2 with full KDE integration and Xorg 7.0.  Starting with this version, Ark Linux uses <a href="http://amarok.kde.org/">amaroK 1.4 Beta</a> as its default music player.  KOffice 1.5 Beta is also part of this release with Krita as the graphics editor.  Ark Linux is available for <a href="http://www.arklinux.org/index.php?option=com_content&task=view&id=5&Itemid=2&lang=en">free download</a>.






<!--break-->
