---
title: "KOffice 1.5 beta 1 Released"
date:    2006-02-01
authors:
  - "iwallin"
slug:    koffice-15-beta-1-released
comments:
  - subject: "Madness?"
    date: 2006-02-01
    body: "Man, the dot is going frenzy. Which drugs are those marketing people on to produce four (and counting) news articles in such a short time? Ah well, I enjoy it :)"
    author: "Jakob Petsovits"
  - subject: "Re: Madness?"
    date: 2006-02-01
    body: "Hmm, I actually did this without the help of the MWG. But just you wait until the final release of 1.5.  Then I will take help from them, and we will really rock the boat!"
    author: "Inge Wallin"
  - subject: "Koffice2.0 and   Qt4"
    date: 2006-02-01
    body: "Will Koffice2 use Qt4?\nWhat are Kexi advantages/disadvantages over MS Access or FileMake?"
    author: "Patcito"
  - subject: "Re: Koffice2.0 and   Qt4"
    date: 2006-02-01
    body: "Yes, KOffice 2.0 will use Qt 4.\n\n"
    author: "Robert Knight"
  - subject: "Re: Koffice2.0 and   Qt4"
    date: 2006-02-01
    body: "If you want a full comparison of kexi to other db apps :\n\nhttp://www.kexi-project.org/wiki/wikiview/index.php?KexiComparisons"
    author: "Cyrille Berger"
  - subject: "Re: Koffice2.0 and   Qt4"
    date: 2006-02-01
    body: "Two small comments for that page:\n\n>MS Access is far more mature and feature-rich,\n>its development consumed dozens of man-years.\nI would think hundreds or thousands of man-years.\n\n>[...]introducing dependency for multimegabyte Java\n>package and increasing its startup speed.\n\"speed\" should be \"time\"."
    author: "Niels"
  - subject: "Re: Koffice2.0 and   Qt4"
    date: 2006-02-02
    body: ">>dozens of man-years.\n>I would think hundreds or thousands of man-years.\nYeah. But we don't know for sure cause it's closed source :-)\nAnyway. I guess dozens is Ok too cause it sounds very neutral (dozens*n).\n \n>>increasing its startup speed.\n>\"speed\" should be \"time\".\nFixed.\n\nThanks Niels!"
    author: "Sebastian Sauer"
  - subject: "Re: Koffice2.0 and   Qt4"
    date: 2006-02-01
    body: "> patent-free software\n\nHow can you tell it? Does it mean that no patents are registered for kexi? Ok, this is true.\nOr does it mean that it does not infringe some software patents? This is false for sure. in USA there are about 300,000 sw patents, and every program that is not trivial is infringing some hundred (thousands?) of them. Even \"hello world\" displayed in a ctr screen infringes one, AFAIR.\nEurope has not sw patents yet, even if they try again to intruduce them with the \"eurpoean patent armonization\" directive (Community patent), or something like that, but European Patent Office has already released around 30,000, so...\nhttp://news.zdnet.co.uk/business/0,39020645,39248676,00.htm\nkeep an eye upon www.ffii.org for forecoming info, and read daily http://wiki.ffii.org/SwpatcninoEn for an updated source of info about sw patents.\nHelp fight software patents!\n\n"
    author: "Marco Menardi"
  - subject: "Re: Koffice2.0 and   Qt4"
    date: 2006-02-01
    body: "Thanks for community support. I think the most important issue now would be to try to organise an US movement.\n\nSee also this consultation\nhttp://www.uspto.gov/web/offices/com/sol/notices/70fr75451.pdf "
    author: "aranea"
  - subject: "Re: Koffice2.0 and   Qt4"
    date: 2006-02-01
    body: "> What are Kexi advantages/disadvantages over MS Access or FileMake\n\nbesides all the feature check lists, here are the ones that matter to me: it runs on Free Software operating systems so i'm not locked into to either Windows or MacOS (depending on which poison pill i choose to choke down). secondly, it *is* Free Software itself which is a much stronger guarantee for me to my data. and finally, it's a KDE app, which means it works, looks and integrates like i expect my apps to do on KDE. there's also a windows port of it out there =)"
    author: "Aaron J. Seigo"
  - subject: "KDE/Koffice-Native DB frontend was long overdue."
    date: 2006-02-01
    body: "KDE/Koffice-Native DB frontend was long overdue. I am eternally greatful to the devs for going this direction. Especially, for making SQLite accessible to My/PostgreSQL-dim-wits like me.\n\n"
    author: "Daniel \"Suslik\" D."
  - subject: "But has the word processor improved?"
    date: 2006-02-01
    body: "I just finished reading an article where Kword was rated only 3/10.\nJohn"
    author: "John Fabiani"
  - subject: "Re: But has the word processor improved?"
    date: 2006-02-01
    body: "Do you have the URL of that article?  Feedback from reviews can be very helpful for the developers*\n"
    author: "Robert Knight"
  - subject: "Re: But has the word processor improved?"
    date: 2006-02-01
    body: "Not sure that this is the one that was referred to but there was an article in the UK magazine Linux Format which comaprd office suites (spreadsheets in particular) and kspread came out as the worst one. I'm not a big spreadhseet user so I couldn't really comment."
    author: "Jon Scobie"
  - subject: "Re: But has the word processor improved?"
    date: 2006-02-01
    body: "Yeah, we read that review... The reviewer sure had fun. Pity there were a couple of factual inaccuracies, and an even bigger pity that apparently kspread-kchart integration was broken on SuSE."
    author: "Boudewijn Rempt"
  - subject: "Re: But has the word processor improved?"
    date: 2006-02-01
    body: "yeah, someone mentioned the article as well. wow, they where negative about Koffice... imho not really a fair comparison. kspread might not be better than OO.o calc, but its not like 2/10 vs 8/10 (more like 5/10 7/10). imho."
    author: "superstoned"
  - subject: "Re: But has the word processor improved?"
    date: 2006-02-01
    body: "KWord has improved a lot: there have been many stability fixes, usability fixes, lots of bugs closed, not to mention OpenDocument made the default native file format and the accessability improvements: http://www.koffice.org/announcements/changelog-1.5beta1.php#kword. I doubt whether it would be enough for the reviewer, but we are very happy that KWord is actively maintained again. And my daughters are very happy because now they've got a word processor that runs on their laptops and lets them create their schoolpapers easily. "
    author: "Boudewijn Rempt"
  - subject: "Klik"
    date: 2006-02-01
    body: "When I try to install the klik client using\n\n  wget klik.atekon.de/client/install -O -|sh\n\nit asks me for the root password. Why is that?"
    author: "ac"
  - subject: "Re: Klik"
    date: 2006-02-01
    body: "\"Why does the klik client install process ask for the root password?\"\n-----\n\nBecause the klik client needs to add entries into your /etc/fstab which will allow loopmounting of the klik *.cmg files (which are similar to compressed ISO image file systems, and need mounting to be accessed) with non-root privileges. It is a safe thing to allow.\n\nIf you have more questions about klik, here is a pretty good FAQ collection:\n\n --> http://klik.atekon.de/wiki/index.php/User's_FAQ <--"
    author: "klik-er"
  - subject: "Re: Klik"
    date: 2006-02-01
    body: "Thanks for the link, this FAQ is very useful. What I'm still confused by is that when I typed that command, I think there was some text on the screen about \"respecting other people's privacy\" or something like that... where did this come from? I don't see it in the install script...\n"
    author: "ac"
  - subject: "Re: Klik"
    date: 2006-02-01
    body: "The install script uses \"sudo\". And sudo produces this message. You can probably try to reproduce it on the commandline, by typing \"sudo ls -l\" in a konsole window...."
    author: "klik-er"
  - subject: "Re: Klik"
    date: 2006-02-01
    body: "I see... well, that's one (not so useful) fact I learned today. Thanks!"
    author: "ac"
  - subject: "ONE bad thing/look about KOffice 1.5 beta"
    date: 2006-02-01
    body: "Start KWord or KSpread or Kpresenter (any koffice app), The \"Recent Document\" and \"Create New Document\" interface is *very confusing*.\n\nI feel that KOffice applications should start with a default BLANK document (kword = blank document, kspread = blank sheet etc.) which could make things faster.\n\nThe \"Open/Recent document/creat new\" integrated interface is very confusing. Icons look terrible. \n\nit is the start (first appearance) of koffice applications which are not user-friendly and aesthetically good. I'm perplexed with where to click? There are no guiding factor.\n\nSorry to complain but please take users' survey in the \"New document\" interface, which I believe could be improved with Some Nice icons and colors, and RichText (bold, italic, underline).\n\nApart from that KOffice have matured quite a bit. Opendocuments work like charm :)"
    author: "fast_rizwaan"
  - subject: "Re: ONE bad thing/look about KOffice 1.5 beta"
    date: 2006-02-01
    body: "have a look"
    author: "fast_rizwaan"
  - subject: "Re: ONE bad thing/look about KOffice 1.5 beta - "
    date: 2006-02-01
    body: "Hmm, I like it!\n\nAlso there is the checkbox \"Always use this template\". When you check it, don't you get then what you proposed - always opening directly as a blank doc?"
    author: "Matt"
  - subject: "ah lol"
    date: 2006-02-01
    body: "yeah those icons suck BIG time *G\nwhatsoever, appreciation for the efforts, 1.5 gold will rock. nice to see I can use OO and KOffice as available and open the stuff.\nAnd please for 2.0, someone should pcik some ideas from Lee's post\nhttp://dot.kde.org/1135283071/1135335762/\nI love OpenSource. It's the superior way of getting things done with a number of intelligent beings."
    author: "zero08"
  - subject: "Re: ONE bad thing/look about KOffice 1.5 beta"
    date: 2006-02-01
    body: "OMG, those icons are... are... horrible :|"
    author: "ac"
  - subject: "Re: ONE bad thing/look about KOffice 1.5 beta"
    date: 2006-02-01
    body: "PLEASE submit better ones... (and if any artist is reading this: you're free to go ahead, please!)"
    author: "superstoned"
  - subject: "Re: ONE bad thing/look about KOffice 1.5 beta"
    date: 2006-02-02
    body: "If the \"always use this template\" checkbox does what I think it does, it should be labeled \"always use this template on startup\" or so.\n\n"
    author: "Roland"
  - subject: "Re: ONE bad thing/look about KOffice 1.5 beta"
    date: 2006-02-02
    body: "On the menu on the left, there is:\n\nRecent Documents\nBlank Document\nCards and Labels\nEnvelopes\nCustom Document\n\nIf by \"Blank Documents\" it's meant to create a new document with no template, why is there a Memorandum template inside it?\n\nOne way of solving it, would be to have something like this:\nRecent Documents\nBlank Document\nOffice Documents *\nCards and Labels\nEnvelopes\nCustom Document\n\nThis way, Blank Document allways gives a blank 1-page file. Office Documents would have the templates for memorandums, business letters, etc.\n\nNon-business templates could be also categorized in new categories: like Home Documents, etc..\n\nOn the right, after Blank Document is chosen, an icon for it is shown with some text and a button, like:\n\n[ICON]\nBlank Page\n[BUTTON]Use this Template\n\nWouldn't it be easier to grasp if it was like:\n\n[ICON]\nBlank Page\n[BUTTON]Create new Document\n\nbut this would give problems when creating new envelopes, and the button talking about Documents. But it could look like this:\n\n[ICON]\nBlank Page\n[BUTTON]Create new Blank Page Document\n\nbut this could make a giant button... What about just \"Create New\"?\n\nIn fact, the fact that this screen's purpose is to create a new document is not very apparent, and only is told as such in the description for the chosen template, like:\n\"Create a blank US Letter document\""
    author: "jukabazooka"
  - subject: "Re: ONE bad thing/look about KOffice 1.5 beta"
    date: 2006-02-01
    body: "Actually, we have taken a users' survey for this. That is, there has been a professionally done usability study with real people and a real usability experts especially on this area. It turns out that the old template/open/recent dialog was confusing; that the new one was better but needed improvements -- and Thomas Zander and Peter Simonssen made those improvements.\n\nBut, as other people have said, you need only one mouse-click to get the blank document of your choice: check Always Use this Template.\n\nNot that I don't expect this feature to lead to much more discussion. It's bound to haunt us forever, even though it is an objective improvement."
    author: "Boudewijn Rempt"
  - subject: "Re: ONE bad thing/look about KOffice 1.5 beta"
    date: 2006-02-01
    body: "i don't understand the fuzz, at first sight i already thought it was much better than the previous dialogue. and as ppl (and i'm human too :D) mostly don't like new things, i think that's a strong hint at the great usabillity of this embedded startpage."
    author: "superstoned"
  - subject: "Re: ONE bad thing/look about KOffice 1.5 beta"
    date: 2006-02-01
    body: "I also like this all in one approach, but we need to Label it like \"Choose a Document/spreadsheet/Presentation Type to Create\".\n\nAnd we need Evarlodo for the icons, they look *just scaled* could be svg icons used there would look better.\n\nand \"use this template\" confuses me... It would be better to have simple english (as average joe user can't understand english vocabular above 6th grade ;)\n\ninstead of \"Template\" which is geeky, consider \"Document Type\" or \"Spreadsheet type\" or \"Presentation Type\".\n\n\"Always use this Document Type\" -> Blank Document... Colorful document... etc.,\n\nThe Recent Documents should *not* be part of the \"Create Document\" List.\n\nThe most missing feature in the page is \"OK\" \"Cancel\" \"Create\" buttons. \"use this template\" doesn't make any sense at least to me."
    author: "fast_rizwaan"
  - subject: "Re: ONE bad thing/look about KOffice 1.5 beta"
    date: 2006-02-01
    body: "I actually think I like it. It's an improvement over the dialogue in any case, and if a good selection of templates will be provided, it should be a better interface than most.\n "
    author: "Luciano"
  - subject: "Re: ONE bad thing/look about KOffice 1.5 beta"
    date: 2006-02-01
    body: "At first I expected a 'blank document' to be only white page, not to include also templates. The window use both 'blank document' and template to refer to the same thing: this is a bad IMHO, choose one term and use it.\nBut of course the hard part is selecting the \"right\" word, I'd say 'Document Type' , Template would be better of course, but not everyone know what a template is..\n\nAnother thing which I dislike is that the first column mix opening an existing document and creating a new one. I would split the first column in two parts: a title 'Open existing document' which would contain: 'Recent document' and 'Browse existing document', the second part would have the title 'Create new document with' and contain: 'Document Types' 'Card and label Types' 'Envelope types' 'Custom Types'.\n\nAs for the \"ugly\" scaled icon, I couldn't care less myself..\n\n"
    author: "renox"
  - subject: "Re: ONE bad thing/look about KOffice 1.5 beta"
    date: 2006-02-02
    body: "As someone who uses KWord several times a day in my normal business flow, I really like the dialog.  It allows me to pick the type of document or an older document as soon as I open KWord.\n\nIt's different, and people don't like that, but people who really *use* KWord seem to agree it's nice.  And if you only fire it up occasionally, it's not a difficult to understand concept, it's just not like the word processor they are used to using.\n\nKeep it!  I invoice, send memos, type up letters and print HPA cards every day through the dialog.  It is genuinely useful."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: ONE bad thing/look about KOffice 1.5 beta"
    date: 2006-02-01
    body: "I also don't like this kind of interface. In particular I don't like the \"Use This Template\" button: not only is it hard to find on the page (usually thing like \"OK/Accept\" and \"Cancel\" are near the bottom of the window), but the wording completely confused me. Is a blank page a template? I guess from a programmers' point of view it is, but I don't think grandma will want to use any template just to write a short letter..."
    author: "ac"
  - subject: "Re: ONE bad thing/look about KOffice 1.5 beta"
    date: 2006-02-01
    body: "I'd say, use it for a while. Don't check Always use this template too soon. Don't forget it isn't just about templates; it's also a much nicer way of getting at your recently used documents. And check the custom document option, that one is very nice, too.\n\nAnd, of course, we need much more and much better templates. If there's a template for \"Birthday Card with Image of your choice\" or \"Cat Fancier's Newsletter\" or \"Seasonal Letter with Choice of Decorations\", starting with templates will be much nicer.\n\nAnd -- it's not just about KWord. With an application like KPresenter, the templates are already much more useful. Personally, I've set KSpread to use a blank spreadsheet, but then, I'm not doing spreadsheets for a job, and I can imagine that anyone who needs to monthly create a set of spreadsheets with reports would like to have those as templates on startup."
    author: "Boudewijn Rempt"
  - subject: "Re: ONE bad thing/look about KOffice 1.5 beta"
    date: 2006-02-01
    body: "Agreed, I really don't like the new dialog.  Opened up kword and had a big wth moment.  I had to sit and stare for a moment before I knew what to do, and that's even after looking at the image posted earlier.  \n\nI didn't find it too bad once one knows where things are, but it gives a terrible first impression.  \n\nIt's still messed up though.  If I press file->new, why on earth would I want to open an existing document?  That is what file->open or file->open recent is for.  Yet, if I opened kword and selected an existing document from the initial dialog, once I press file->new it gives me a list of recent document.  hurr?  um.. *new*?\n\nMy preference: the new dialog should not have a recent files category.  The opening dialog should have a choice to open an existing file or create a new.  Choosing 'new' brings up the new dialog (without recent files of course) and choosing 'open existing' brings up a dialog with recent files and a way to browse to other ones.  \n\nHaving the file->new dialog and the opening dialog be the same is rather ugly.\n\nOnce again I find a reason to detest \"usability\" studies.  This new dialog seems to me the same as Gnome's reversed dialog button order.  Some study said it was for the best, so something different than what everyone is used to is implemented.  Studies be damned, the most usable thing is what people are used to.  \n\nThis dialog, by being different, has already shot itself in the foot.  Joe user isn't going to want to hear how he will get used to it and it will be marginally better when he does.  Heck, *I* don't.\n\n/downgrades"
    author: "MamiyaOtaru"
  - subject: "Re: ONE bad thing/look about KOffice 1.5 beta"
    date: 2006-02-02
    body: "Yeah and we listen to what the users are saying. That was why it was changed in the first place and I guess it will change sooner or later again (just cause Software is a moving target and never is perfect :-)\n"
    author: "Sebastian Sauer"
  - subject: "Re: ONE bad thing/look about KOffice 1.5 beta"
    date: 2006-02-02
    body: "> Studies be damned, the most usable thing is what people are used to. \n\nSadly progress will never be made if nobody attempts to break the mould occassionally.  Microsoft obviously agree with this, hence the new Office 12 UI.  The new startup dialog is not perfect or final, and I agree that we should make life easier for those who prefer not to use it.\n\n>Joe user isn't going to want to hear how he will get used to it and it will be marginally better when he does.\n\nYou are not \"Joe user\", so I don't know how you can claim to represent their needs.  I agree that the new dialog is not aesthetically pleasing at the moment (hopefully we can fix that for the final release).\n\n> My preference: the new dialog should not have a recent files category. \n\nThe Recent Files section is actually very useful in practice, and is probably the most fundamental aspect of the new startup dialog."
    author: "Robert Knight"
  - subject: "Re: ONE bad thing/look about KOffice 1.5 beta"
    date: 2006-02-01
    body: "I agree completely !\n\nBlank documents are so much better for everyone as a start point.\n\nThose dialogs are so annoying and useless."
    author: "Dom"
  - subject: "Re: ONE bad thing/look about KOffice 1.5 beta"
    date: 2006-02-02
    body: "I third this.   KWord should be good enough to create suggestion from what the user wants based on contextual clue when the user type in the document.  \n\nFor example, when the user type:\n\nFeburary 2, 2006\n\n\nDear ...\n\nImmediately, KOffice will on the bottom to show the possible templates the user can choose from.  The first suggestion is a letter.  Never force the focus out of the document canvas like clippy.  Let the template appear naturally in on the bottom while the user type away."
    author: "Zero"
  - subject: "Thanks for the feedback"
    date: 2006-02-02
    body: "and cause we are listen to what our users are saying;\nhttp://bugs.kde.org/show_bug.cgi?id=121233\n"
    author: "Sebastian Sauer"
  - subject: "KOffice rocks!"
    date: 2006-02-01
    body: "KOffice rocks! The koffice-guys did a great job. KOffice is gaining momentum at an incredible pace. Since 1.2 it's my office-package of choice. And it's getting better and better with every release. Keep on focusing on your benefits - fast, easy to use\nHaving \"full\" OpenDocument support will be so cool to use. KPlato may be the first usable free project-planing tool for Unix. Krita is currently replacing gimp more and more (for me).\nI can't wait to try the new version...\nThanks a lot for your hard work!\n"
    author: "birdy"
  - subject: "Re: KOffice rocks!"
    date: 2006-02-01
    body: "One way to get a feeling how fast the development is really racing ahead is to compare the changelog from KDE 3.5.1 (>100 developers) with the one for KOffice 1.5 beta1 (~15 developers).  I haven't actually done a real line count, but my feeling is that the one for KOffice is actually bigger."
    author: "Inge Wallin"
  - subject: "Re: KOffice rocks!"
    date: 2006-02-01
    body: "I'm quite sure that we have around 30 KOffice devs at the moment, which is about twice as much as we had a year ago ( http://dot.kde.org/1107478403/ ). Yes, the developer base is growing at a rapid pace. It's exciting.\n\nBut the comparison still holds true: there are less KOffice developers than KDE developers and we have more changes. (although KDE has a minor release and we have a major release)"
    author: "Raphael"
  - subject: "Re: KOffice rocks!"
    date: 2006-02-01
    body: "bugfix release != feature release"
    author: "Christian Loose"
  - subject: "Re: KOffice rocks!"
    date: 2006-02-01
    body: "i can't help but wonder why kplato was created? i guess taskjuggler would be an excelent complement to Koffice - it is already very mature, and i can't find anything but highly positive revieuws... did the Koffice guys have a talk with the taskjuggler guys, or considder forking?"
    author: "superstoned"
  - subject: "Re: KOffice rocks!"
    date: 2006-02-01
    body: "Taskjuggler is an engine without a GUI.  KPlato is a GUI with an (at this point) limited engine.  You connect the dots... :-)\n\n(There is a TODO item to merge them.)"
    author: "Inge Wallin"
  - subject: "Re: KOffice rocks!"
    date: 2006-02-01
    body: "Taskjuggler an engine without a GUI???\n\nInge Wallin, you are ill-informed then. Just try to convince yourself by quickly running \n\n --> sh $HOME/.klik taskjuggler <--\n\nor \n\n --> klik://taskjuggler <--\n\nWhat I see here coming up is a GUI, and even a beautiful KDE-ified one (instead of the former web interface TaskJuggler threw against my aestetic feelings.\n"
    author: "klik-er"
  - subject: "Re: KOffice rocks!"
    date: 2006-02-01
    body: "You are right, I was too quick here.  Sorry.\n\nThe fact still remains, though, that we want the TaskJuggler engine in KPlato :-)"
    author: "Inge Wallin"
  - subject: "Re: KOffice rocks!"
    date: 2006-02-02
    body: "sorry, i didn't know kplato might be worked into a TaskJuggler user :D\n\nhope you guys can use parts of the current KDE/Qt TaskJuggler frontend... it is already very complete and looks very usefull (i didn't need a full tool like that, but it seems to be very powerfull)"
    author: "superstoned"
  - subject: "Re: KOffice rocks!"
    date: 2006-02-04
    body: "That's great!\n\nJust make sure that you tell the world when a klik://PlatoJuggler is ready so I can add it to my collection of kliks  :-)"
    author: "klik-er"
  - subject: "Re: KOffice rocks!"
    date: 2006-02-02
    body: "Sure, there is GUI for displaying the projects' contents, but there is no GUI for editing. In fact, the Taskjuggler docs contain references to this, claiming that due to lack of graphical editing components one can create much bigger projects. But this means that Taskjuggler can not be a real replacement for eg. MS Project - of course IT managers might probably want to learn TJ language, but I do not think that any other project manager would do it :-)"
    author: "Piotr Gawrysiak"
  - subject: "Re: KOffice rocks!"
    date: 2006-02-02
    body: "hmm.. i've always had the impression that they are two different tools for different types of jobs.\n\nkplato (and ms project, for that matter) are great for small to medium sized projects that are generally fairly linear and need timelining and basic resource management.\n\ntaskjuggler and similar software products are designed for large engineering-style projects where you have a lot of resources (people) working on a number of parts of a project simultaneously and where managing the resources is more complex and important than managing the timeline.\n\ntimelining versus resource management, that's generally how i find the split. but perhaps that's just me? +)"
    author: "Aaron J. Seigo"
  - subject: "Re: KOffice rocks!"
    date: 2006-02-02
    body: "Not entirely just you. I'm not really interested in project management -- but I have used KPlato for planning Krita development to some extent. Of course, what's missing is an uncertainty generator and knowledge of exam periods in a wide variety of countries, but it was admirably suited for my purpose.\n\nTask juggler comes into its own when you've got scores of \"resources\"."
    author: "Boudewijn Rempt"
  - subject: "Re: KOffice rocks!"
    date: 2006-02-01
    body: "It's not like KPlato was started yesterday. It already exists since 2001:\nhttp://webcvs.kde.org/koffice/kplato/main.cc?rev=1.18&view=log.\n\nThere was just a long pause in its development. ;-)"
    author: "Christian Loose"
  - subject: "Screenshots of Kexi 1.0 beta 1"
    date: 2006-02-01
    body: "http://kexi-project.org/screenshots.html\n"
    author: "Jaroslaw Staniek"
  - subject: "Live CD?"
    date: 2006-02-01
    body: "Is there one?"
    author: "aranea"
  - subject: "Re: Live CD?"
    date: 2006-02-01
    body: "there is a first test release which is based on Kubuntu Breezy\n\nYou'll find it at:  \nftp://ftp.bay13.net/pub/kubuntu-devel/kubuntu351+koffice15b1.iso\n\n\nCheers,\namu"
    author: "Andreas Mueller "
  - subject: "Re: Live CD?"
    date: 2006-02-03
    body: "is that cd german?!?!?"
    author: "superstoned"
  - subject: "Import/Export filters"
    date: 2006-02-01
    body: "First of all, a million thanks to all koffice devs. It's stuff like this, that really help Desktop linux become a reality. I can finally drop the turtle that it OOo :)\n\nOne thing that I doubt, is whether Koffice uses the *same* import/export filters for the shitty MS document formats as OOo ?"
    author: "Ahmed Kamal"
  - subject: "Re: Import/Export filters"
    date: 2006-02-01
    body: "No it doesn't - the filters are KOffice's own (I'm not entirely sure about the MS Word one though).  If you do a lot of work with complex Microsoft Office documents, OpenOffice will be better for you at the moment.\n\nAn alternative is to convert them using OpenOffice and then use KOffice to work with them :)"
    author: "Robert Knight"
  - subject: "Kexi + Scripting!!!"
    date: 2006-02-01
    body: "This is simply amazing! Better doc`s and small companies can use it already.\n\nThanks to all KOffice developers!"
    author: "Cookiem"
  - subject: "kchart is still messed up"
    date: 2006-02-02
    body: "kchart forces you to do some really strange things to display a pie chart.  I keep wondering if it will ever change..  have a look at the image:\nhttp://img494.imageshack.us/my.php?image=piechartmess2ev.png\n\nSeems to be getting worse, I don't remember having to leave a blank row before.  "
    author: "MamiyaOtaru"
  - subject: "Re: kchart is still messed up"
    date: 2006-02-02
    body: "Hi,\n\nI made that change.  The first Row is assumed to be column labels (eg. \"Month\" , \"Sales\").  The leftmost Column is assumed to be row labels (eg. \"January\", \"February\" etc.)\n\nUnder KChart 1.4, you had to manually click on the table headers to set up labels for series or rows.  This means it was slow because you couldn't do it easily via the keyboard.  I also found it non-intuitive, since in spreadsheets etc. the first column and last row are usually used for such labels.\n\nThe other thing is that the default data layout has been changed from data in rows to data in columns, ie:\n\nMonth | Sales\nJan   | X\nFeb   | Y\n\nInstead of:\n\nMonth | Jan | Feb\nSales | X   | Y\n\nThe rationale is that most tables of data are presented in the first format rather than the second (which is why almost all spreadsheet packages have many more rows than columns), so KChart now expects data in the first format by default.\n\nI will see what I can do to make the KChart Data Editor more intuitive for the final KOffice 1.5 release.  Thanks for the feedback."
    author: "Robert Knight"
  - subject: "Re: kchart is still messed up"
    date: 2006-02-02
    body: "Yes, KChart is a bit messed up in the beta, I'm sorry for that.  The reason is that we got a completely new engine just before the beta, and I didn't have time to fix all the other TODOs. I promise you that you will have a easy and intuitive data editor until the final release of 1.5.\n\nAside from that, a pie chart is not very good to display two-dimensional data. Try the ring chart instead.  "
    author: "Inge Wallin"
  - subject: "Re: kchart is still messed up"
    date: 2006-02-03
    body: "Thank you for your responses.  The link is to a pic of the equivalent situation in 1.4.  http://img133.imageshack.us/my.php?image=piechart14mess4ps.png\n\nIt was nice to have the labels separate from the data, but I see your reasoning for changing it.  Having to click on each one could be a bit of a pain.  Still, something to separate it from the data itself, a label above it calling it the \"label column/row\", or maybe the row/column with the labels could be a different color, something.\n\nRing charts were mentioned.  They suffer from the same affliction as pie charts (at least in 1.4, to which I have just temporarily downgraded).  At any rate, pie charts remain popular, so I hope both can be fixed.  Best of luck, I look forward to the next beta!\n"
    author: "MamiyaOtaru"
  - subject: "Re: kchart is still messed up"
    date: 2006-02-03
    body: "I called kchart 1.5 a horrible regression in the picture in the parent post, sorry for that.  I made it straight after downgrading and before checking if there were any responses to my original post.  I then read those and it explained what had happened and your reasoning, and I forgot to change the pic.  Anyway, keep at it!"
    author: "MamiyaOtaru"
  - subject: "Kubunutu KWord Broken"
    date: 2006-02-05
    body: "Any word on when there will be a fixed Kubuntu package for KWord?\n\n(It was compiled with some visual debugging features enabled, which is really neat to look at, but kind of makes it less useful)."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Kubunutu KWord Broken"
    date: 2006-02-05
    body: "Try \n\n  klik://kword-1.5-beta\n\nWorks really well, also on Kubuntu (and gets updated every few days, even without an official beta. The current version has fixes from SVN for bugs that were only discovered after the first Beta was released)."
    author: "klik-er"
  - subject: "Re: Kubunutu KWord Broken"
    date: 2006-02-06
    body: "I use KWord in an office environment.  I don't want to test it other than to see if the beta was stable enough for actual use... a nice weekend project.\n\nIt wasn't, so I reverted back to the stable version and will wait until it's RC to try again.\n\nThanks, though."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Kubunutu KWord Broken"
    date: 2006-02-06
    body: "Are you aware that klik (http://klik.atekon.de/) enables you to use the Beta (and upcoming RC) without the need to de-install the stable version?\n\nGive it a try. Visit \n\n--> http://klik.atekon.de/wiki/index.php/KOffice-1.5.0_DistroTable \n--> http://klik.atekon.de/wiki/index.php/User%27s_FAQ\n\nfor some details..."
    author: "Kurt Pfeifle"
  - subject: "Re: Kubunutu KWord Broken"
    date: 2006-02-07
    body: ":)  Yes, I'm aware.  Thanks.\n\nI think it's a testament to KOffice that there are people who don't have time to test new versions because they are too busy using it for real work."
    author: "Evan \"JabberWokky\" E."
---
The KDE Project today announced the release of KOffice 1.5 beta 1, the first preview release for KOffice 1.5, scheduled for release this March. <a href="http://www.koffice.org/">KOffice</a> is an integrated office suite with more components than any other suite in existence.  Never before has a new version of KOffice brought this many exciting new features including <a href="http://www.koffice.org/kexi/">Kexi</a> 1.0 and the first release of project management application <a href="http://www.koffice.org/kplato/">KPlato</a>. Read the <a href="http://www.koffice.org/announcements/announce-1.5-beta1.php">full announcement</a> and <a href="http://www.koffice.org/announcements/changelog-1.5beta1.php">the changelog</a> for more details or read on for the full article.








<!--break-->
<p>This release specifically introduces the following highlights:</p>
<ul>
<li><b>OASIS OpenDocument as the Default File Format</b>
<br />
KOffice now uses <a href="http://en.wikipedia.org/wiki/OpenDocument">OASIS OpenDocument</a> as the default file format for the productivity applications KWord, KSpread, and KPresenter.  In the final 1.5 release, the charting application KChart will also use OpenDocument as the default format.
</li>
<li><b>Enhanced Accessibility for Users with Disabilities</b>
<br />
The decision of the Commonwealth of Massachussetts to base its future document format on open standards started a great debate with many different people and organization taking part. One major aspect is <a href="http://blogs.zdnet.com/BTL/?p=2163">accessibility</a>. KOffice now supports enhanced accessibility through mouseless operation and text-to-speech.</li>
<li><b>Start of a Unified Scripting Approach</b>
<br />
This version of KOffice features a start of a unified scripting solution called Kross. Kross provides cross-language support for scripting (thus its name) and at present supports Python and Ruby.</li>
<li><b>First Major Release of Kexi (1.0)</b>
<br />
KOffice 1.5 contains the much expected final 1.0 version of Kexi, a data 
management application which is the KOffice counterpart to MS Access or 
FileMaker. It is designed from the ground up as a standard KDE database 
application.</li>
<li><b>Project management application <a href="http://www.koffice.org/kplato/">KPlato</a> </b>
<br />
KOffice 1.5 is the first official release to include KPlato. KPlato is a project management application that lets the user control project schedules and resource use. It is included in KOffice 1.5 as a technology preview and full functionality is expected for version 2.0.</li>
</ul>

<p>Of course, it also fixes countless bugs and introduces a lot of smaller new features.</p>

<p>Packages are available for <a href="http://pkg-kde.alioth.debian.org/koffice-1.5-sarge/">Debian Sarge</a>, <a href="http://pkg-kde.alioth.debian.org/koffice-1.5/">Debian Sid</a>, <a href="http://kubuntu.org/announcements/koffice-15beta1.php">Kubuntu</a> and <a href="http://download.kde.org/unstable/koffice-1.5-beta1/SuSE/">SuSE</a>.</p>

<p>Throughout the beta period, to make testing easier, the <a href="http://klik.atekon.de">Klik</a> developer team will provide up to date Klik packages for all of KOffice. Klik packages may be used without installation and run on multiple GNU/Linux platforms without disturbing the system's native package manager. See the <a href="http://klik.atekon.de/wiki/index.php/KOffice-1.5.0_DistroTable">KOffice Klik wiki page</a> for details of the bundles and supported distributions.</p>







