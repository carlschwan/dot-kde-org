---
title: "KOffice Design Competition Winner"
date:    2006-03-06
authors:
  - "brempt"
slug:    koffice-design-competition-winner
comments:
  - subject: "funny"
    date: 2006-03-06
    body: "ok see this in 5 years :-)"
    author: "ch"
  - subject: "congrats martin!"
    date: 2006-03-06
    body: "http://developers.slashdot.org/article.pl?sid=06/03/05/217248"
    author: "ac"
  - subject: "Hope it's not sour grapes, but..."
    date: 2006-03-06
    body: "I'm hugely disappointed; I sent my PDF entry to three email addresses, even went to the IRC channel for confirmation of receiving my entry, and it's still not shown on the results page.  I wonder if it was ever received.\n\nI don't know if my idea sucked or was plain and obvious, but it's a huge bummer it wasn't even in the running and I never knew it.  I can't believe all this time I've been sitting here thinking they were reading it.  I put a lot of work into it.  I wonder what the heck happened. :-(  Mine was an interface reorganization with an emphasis on a context-sensitive area to keep things familiar and free of clutter (first thing to go was that horrible toolbar).\n\nSince it doesn't matter now, I offer it to the world:  <a href=\"http://www.scaredlittleboy.org/uploads/KOffice%202%20Interface.pdf\">Click here to read my entry in original PDF form</a> if you want to check it out.  Let me know what you think.  It's nothing revolutionary, but it's not intended to be.  These crazy experimental office interfaces are exactly what the user <i>doesn't</i> need.\n\nMan, what a disappointment that they never even got it.  Figures.  But hey, I offer mine here as GPL too--if someone wants to use it for something, go right ahead."
    author: "Preston"
  - subject: "Re: Hope it's not sour grapes, but..."
    date: 2006-03-06
    body: "Hmm, thought HTML tags were allowed.  Just copy and paste this into your address bar:\n\nhttp://www.scaredlittleboy.org/uploads/KOffice%202%20Interface.pdf\n\nAnyway, congratulations to the winner."
    author: "Preston"
  - subject: "Re: Hope it's not sour grapes, but..."
    date: 2006-03-06
    body: "8mb pdf is really huge :-) , but anyway we live in a world where you download movies >600mb very fast.\n\nbut i like the overall quality and style of your article - because i cant judge on the contents i wont say anything about it , but the presentation is top.\n\n"
    author: "chris"
  - subject: "Re: Hope it's not sour grapes, but..."
    date: 2006-03-06
    body: "I did send a version with smaller resolution illustrations that reduced the size to about 4MB.  Oh, well. :)"
    author: "Preston"
  - subject: "Re: Hope it's not sour grapes, but..."
    date: 2006-03-06
    body: "Preston, I never received your submission -- but possibly Inge didn't manage to get it through to me. I tried to mail you an apology today, but my mail to your gmail account bounced. In any case, my apologies for the mess-up. There was nothing intentional to it! Would you your submission to figure on the page with all the submissions?"
    author: "Boudewijn Rempt"
  - subject: "Re: Hope it's not sour grapes, but..."
    date: 2006-03-06
    body: "No apology necessary; these things happen. :) I emailed to you, Ingwa, and the main contest address, but it's likely the file was just too darned big.  I never received an error, so I assumed the mail had gone through.  Based on the direction taken by the winning entry, I suspect I wouldn't have won the competition regardless, but I appreciate the offer to list it on the results page.  It was a fun competition to work on anyway, even if I never actually competed!"
    author: "Preston"
  - subject: "Re: Hope it's not sour grapes, but..."
    date: 2006-03-06
    body: "Done!"
    author: "Boudewijn Rempt"
  - subject: "Re: Hope it's not sour grapes, but..."
    date: 2006-03-06
    body: "Sorry to know that Mr.Preston. However you should have tried to split the article to parts. Your article is about 8Mb and I am trying to download it since morning. \nFor submitting the article I had to take much pains too. The competition address simply bounced my mail back. Inge could not receive from his mailbox and Boud got the mail fortunately and sent an acknowledgement.\nHowever Ideas like yours and others in a multi-page PDF does not solve any purpose. They should be developer friendly. A classified and searcheable collection of articles as a web-site could have been better if the developers at all needed to refer them to refresh the stale and beaten ideas followed since long. Probably a wiki model with guest-editing disabled could have done a better job. \n\nManik Chand Patnaik"
    author: "Manik Chand"
  - subject: "Re: Hope it's not sour grapes, but..."
    date: 2006-03-06
    body: "Maybe it's because you wrote your proposal in Pages...\n\nJust kidding. Your ideas look pretty good, and I'm sorry they never made it to the results list."
    author: "anon"
  - subject: "Re: Hope it's not sour grapes, but..."
    date: 2006-03-06
    body: "I feel sorry for your work being lost in the mail, especially since it really looks quite nice. Hopefully there will be a more robust submission procedure next time..."
    author: "Martin"
  - subject: "Congrats to all :)!"
    date: 2006-03-06
    body: "Even though we now have \"one true winner\", I do hope that some of the best ideas in other suggestions will be used as well. I espesially like Moritz Zimmermanns ideas as well as Sergiy Kudryk's suggestions (since the content is the most important thing, why not remove everything else but the content? Work in full-screen mode)."
    author: "Janne"
  - subject: "combine ideas ?"
    date: 2006-03-07
    body: "These are all great ideas - could multiple styles be implemented and then the user could choose which one they liked best - and they could switch in the middle of the document? \n\nWhat I liked best were the new icons - they were a vast improvement over the current one - I also liked the simplified interfaces - not having to look through long menus and dialogs to find simple options like export to html, pdf, or save and breaking things up into logical breaks like font, image, table buttons and then you could have detachable palettes that pop up when you click on them.\n\nAll great improvements and I am impressed with their creativity and their immense effort and thoroughness and most of all licensing these as GPL."
    author: "Benjamin Huot"
  - subject: "Flip windows for edit properties"
    date: 2006-03-08
    body: "I think that the 'flip windows for edit properties' in Airas Kirejevas proposition is very smart and innovative."
    author: "Charles de Miramon"
  - subject: "Re: Flip windows for edit properties"
    date: 2006-03-08
    body: "It has been used before in Sun's Looking Glass demo's -- but it's something that we might want to implement kde-wide. Same with a scratch area for notes."
    author: "Boudewijn Rempt"
  - subject: "Re: Flip windows for edit properties"
    date: 2006-03-09
    body: "\"It has been used before in Sun's Looking Glass demo's\"\n\nNot to mention in Dashboard-widgets in OS X.\n\nBTW, why isn't this piece of news in the fronpage of kde.org?"
    author: "Janne"
  - subject: "Some great ideas"
    date: 2006-03-09
    body: "I really like the ideas from:\n\nJeroen Vaes\nand\nMoritz Zimmermann\n\nbest.\n\nOne should not need a menubar anymore for doing things, and IF than it's better putting it somewhere, where it does not waste space. Also the idea with the statusbar from Jeroen is great, a zoom slider makes more sense there than a combobox in the toolbar."
    author: "Martin Stubenschrott"
  - subject: "Please don't go with a palettes interface."
    date: 2006-03-09
    body: "I teach daily at least on 5 Adobe products and constantly see that novice people, just don't know what to do with this kind of interface!\nThe clutter people ended with drove them in many times to \"lose\" functionality (be it overlapped under another windows, be it out of screen after dragging the window too much) and to don't find how to bring it back.\nI was begging Adobe to go away from this concept of \"floating palettes\", which fortunately for my pupils they have finnally done for the new versions of most of the prodct line (Premiere Pro, After Effects, Encore DVD, Audition).\nSo please, I tell you, if you want to improve Koffice interface, just don't go with this paradigm, it's not for the better. If you use palettes, better they be like \"panels\" dockable on the sides or bottom of the interface, but by no means floting by default.\n\nWhile on the subject, I'd like to referto a common problem I see in Linux interfaces, they tend to do buttons and widgets way too large, that end occupying too much screen real estate. Please understand that people with small screen sizes should also be able to use the programs comfortably. This is evident in the case of Krita's tool panels... on a 1024x768 screen the pannels leave just a very small space left for image display and editing, which should be the main purpose of the program! I see they are improving on this lately, but much more effort should be put in order to really use the least space possible for putting on screen the necessary controls (use smaller fonts, smaller thumbnails, smaller buttons) See Adobe products for this, in this dept. they've done it right.\n\nFinally, I know this is not the best place to send this comments, but I just don't know where todo it, so please forward them to the apropriate places. thx!\n\nGabriel\n"
    author: "Gabriel Gazz\u00e1n"
  - subject: "Don't go with a \"floatting\" palettes interface"
    date: 2006-03-09
    body: "that is.\n:)"
    author: "Gabriel Gazz\u00e1n"
  - subject: "Re: Don't go with a \"floatting\" palettes interface"
    date: 2006-03-09
    body: "I think docking palettes help a lot; And I personally like them a lot. But whenever I look at Photoshop on Windows I get annoyed by the way the palettes don't move with the window and are in general a pain. I tend to hide them in the nice palette dock that I discovered through one of our submissions. \n\nThat's not to say Qt's docker windows are perfect. If you look at a commercial application like Bibblepro (which is really, really cool btw), then you can see that they have the same agony with dockers that I have.\n\nAnd the size of the widgets inside the palettes is indeed a problem. There's only so much you can do with small fonts and small thumbnails. The widgets themselves tend to be bigger than necessary. But apart from creating completely new buttons, sliders (done one for Krita), text boxes and list boxes, there's not much to do. We cannot even do a fixed width layout because translations will change the width from language to language.\n\nAnyway: the best place to send comments about Krita is the friendly krita developer mailing list (http://mail.kde.org/mailman/listinfo/kimageshop)."
    author: "Boudewijn Rempt"
  - subject: "Re: Don't go with a \"floatting\" palettes interface"
    date: 2006-03-10
    body: "thanks for the reply.\nI also noticed widgets in all Qt/KDE apps are too big, is this a problem of Qt design, you say?\n\nIn particular, the value input widget is a huge waste of space, I love that tiny solution found by Adobe: just displaying the number, with the ability to drag right/left over it to increase/decrease the amount, or to simply click over the value to be able to change it \"by hand\".\nCan't something like this be donde using Qt?\n\n"
    author: "Gabriel Gazz\u00e1n"
  - subject: "Re: Please don't go with a palettes interface."
    date: 2006-03-09
    body: "Yes, it would be much better if KOffice maintained the dockable panels it already has (see Krita, for example).\n\nIt's good to explore new interfaces, but one of the best things of the KOffice workspace is that it is tightly integrated, tabbed and docked (in a way much better than most Windows programs, for that matter): IMHO, it could and should evolve, but while keeping these very good UI elements.\n\nFor example, one of the things I hated most when I used the Classic Mac OS was the chaos of windows and floating palettes everywhere: Mac OS X is much better on this front - but KDE/KOffice is even better! :-)\n\nThe winner's concept of a future KOffice interface, anyway, seems to be rather compatible with a good \"dockable\" integrated workspace, like today's one..."
    author: "Sven"
  - subject: "trying too hard"
    date: 2006-03-09
    body: "This whole contest strikes me as doing too much, going for the radical overhaul when there are plenty of small things that would make a world of difference.\n\n1) make it fit (as in WordPerfect)\n\n2) good integration with the address book.  KWord needs the ability to integrate with the address book in a manner that lets one print an envelope by choosing an entry in the address book.  Right now in OOo one has to mess with mail merge and databases, and I don't think KWord is any better.  A simple way to merge in addresses in the header of a letter and an optional envelope (or just envelope) by choosing an entry in the address book, without having to mess with database fields, would be very welcome.  Again, this is something WordPerfect does right.\n\nIt's not an either/or proposition of course.  I hold out hope that little things like this can be included even in the event of a total UI redesign.  Please don't lose sight of stuff like that."
    author: "MamiyaOtaru"
  - subject: "Outside the Box"
    date: 2006-03-12
    body: "I congratulate MP for his entry.\n\nHe has some good ideas, but I wanted to specifically recognize his ideas which indicate clearly that the office suite paradigm is a box.  I have previously expressed my ideas for an office \"suite\" which would be totally outside the box and it didn't go anywhere.\n\nUnfortunately, many users can not seem to think outside the box. IIRC, radical ideas like the Trapeze spreadsheet have not done well in the market.  However that idea lives on in the WordPerfect spreadsheet functions (NOT Quattro Pro)  \n\nSo, my random thought is that it probably would not be a good idea to implement his user interface ideas although what he says about the fact that the underlying structure of KOffice is very true.\n\nWhat I would suggest is that we keep the traditional structure of KOffice while improving its internal integration (and convert the current apps to KParts) and then implement his ideas in a more radical way in a new application which does NOT use separate applications but rather uses a single framework to work on the document with the KParts.  The framework would not be a large program since all it would do would be to load, save, and print files.  It would call the KParts to do all the work.\n\nThis would require a new file format which could include all types of content -- something like HTML.  It appears that XML would be quite suitable for this and would mostly use the existing OASIS file formats.\n\nCurrently, GoBe Productive has been moderately successfull in the marketplace.  Although I do note that the promised Linux version has never materialized. It appears to me that we could make a better unified office \"suite\" framework using the KOffice apps as KParts."
    author: "James Richard Tyrer"
---
Martin Pfeiffer has won the <a href="http://www.koffice.org/competition/guiKOffice2.php">competition for KOffice 2 GUI and functionality design</a>. All entries are available under the GPL license at <a href="http://www.koffice.org/competition/gui1results.php">the results page</a>. His entry was chosen from among the eighteen submissions because of its innovative, ground-breaking approach to workflow and document handling. Across the board, the entries were of a high quality and demonstrated eagerness to think outside the established office suite paradigm.



<!--break-->
<p>Some general trends were clear across the entries: a palette-based
interface was a recurring theme, for instance. That's something we've
seen before in Adobe applications or on NextSTEP, but not generally
applied to Office software. And in many cases, palettes were only the beginning. Not all suggestions were entirely practical or even feasible, but that was exactly the intention of this competition: to break through the familiar mold and get wild, new ideas.</p>

<p>In the end the jury was in complete agreement. But all contestants have reason to be proud of their work and the jury is very grateful for their entries. We especially wish to commend those people whose native language is not English for their courage. They labored under the additional handicap of an unfamiliar language and still had the courage to share their ideas with us.</p>




