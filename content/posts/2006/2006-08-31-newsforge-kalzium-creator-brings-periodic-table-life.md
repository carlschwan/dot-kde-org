---
title: "NewsForge: Kalzium Creator Brings the Periodic Table to Life"
date:    2006-08-31
authors:
  - "jriddell"
slug:    newsforge-kalzium-creator-brings-periodic-table-life
comments:
  - subject: "great"
    date: 2006-08-31
    body: "great article! and Carsten did a wonderfull job on Kalzium - the KDE 4 version is gonna be very cool with the openGL stuff ;-)"
    author: "superstoned"
  - subject: "Re: great"
    date: 2006-08-31
    body: "Are there any specific plans for OpenGL stuff?  I've not heard of any concrete plans."
    author: "Mark Williamson"
  - subject: "Re: great"
    date: 2006-08-31
    body: "Well, you can already see part of it working in SVN. Currently, the rendering of molecules is implemented and already quite optimized, with a special focus on machines without hardware acceleration. The things that remain to be done are: a comfortable mouse-based 3d navigation system, the ability to display some information like angles and element names in the 3D view, and a few other things. We also plan to do the same for crystal structures -- currently there is only a molecule viewer, but we'll also add a crystal viewer.\n\nYou can read more about that in Carsten's blog or in some commit-digest."
    author: "Benoit Jacob"
  - subject: "Re: great"
    date: 2006-08-31
    body: "For the interested: This is the URL to my blog\n\nhttp://cniehaus.livejournal.com/\n\nThese two might be interesting WRT to OpenGL.\n\nhttp://cniehaus.livejournal.com/23572.html\nhttp://cniehaus.livejournal.com/24404.html"
    author: "Carsten Niehaus"
  - subject: "Re: great"
    date: 2006-09-01
    body: "Wow, pretty molecules! :-)\n\nI'm curious: who do you see as the users you're aiming for with Kalzium?  It looks like it's acquiring features that would be useful to progressively more advanced students of chemistry - would you say this is true?\n\nThe ability to work with chemical equations would certainly have been useful to me back when I studied chemistry in school, but I understand in Kalzium that's now supported too :-)\n\nCool stuff!  Had you considered providing any wikipedia integration, by the way?  Obviously Kalzium can offer many things that wikipedia can't, but it'd maybe not be too hard to provide links or integrated views of wiki content for those who like to read more verbose descriptions of things, or research related information?  Just an idea, anyhow."
    author: "Mark Williamson"
  - subject: "Re: great"
    date: 2006-09-01
    body: "I don't know about the current status but it is on the list of ideas for integration of KDE and Wikipedia:\n\nhttp://meta.wikimedia.org/wiki/KDE_and_Wikipedia"
    author: "cm"
  - subject: "Re: great"
    date: 2006-09-01
    body: "> who do you see as the users you're aiming for with Kalzium?\n\nAs a teacher: Students (uni+school). But of course also just everybody who needs access to chemical information. All the new features are more for the professional, yes. But I try to keep the GUI simple, so that shouldn't be a problem.\n\n"
    author: "Carsten Niehaus"
  - subject: "Re: great"
    date: 2006-09-05
    body: "I should also point out with Benoit and Carsten's responses, that some of the Kalzium 3D Open GL code is going into a new Qt/KDE project for more advanced viewing/editing of molecules, crystals, and other chemistry data.\n\nThis is code-named \"Avogadro\" and we're working pretty closely with the Kalzium project to share code as much as possible. (Hopefully there will eventually just be one OpenGL widget.)\n"
    author: "Geoff Hutchison"
  - subject: "Schrodinger's equation"
    date: 2006-08-31
    body: "Hi,\n  It would be neat if it used schrodinger's equation and showed what the atom looks like and the distribution of the electrons.\n\n:-)\n\nJohn"
    author: "John Tapsell"
  - subject: "Re: Schrodinger's equation"
    date: 2006-08-31
    body: "He was planning on using that equation to hook up Kalzium to Kat, but that project may or may not be dead.\n"
    author: "Wade Olson"
  - subject: "Re: Schrodinger's equation"
    date: 2006-08-31
    body: "Hook both up to the G System and an instance of Kolab, pour a fresh cup of really hot tea into the CD drive, and they will come to life. \n\n<maniacal frankensteinian laughter />\n\n"
    author: "cm"
  - subject: "Re: Schrodinger's equation"
    date: 2006-08-31
    body: "I talked with Jos about Stringe (the \"successor\" of Kat). As Jos is currently doing the DBus-binding-stuff I will add support for it soon (as soon as Jos finished it and I learned DBus :)"
    author: "Carsten Niehaus"
  - subject: "Re: Schrodinger's equation"
    date: 2006-08-31
    body: "Man, how rare is it when you can make a bad pun on Schrodinger's cat (http://en.wikipedia.org/wiki/Schrodingers_cat) and an accurate statement by accident.  Good times.\n"
    author: "Wade Olson"
  - subject: "Re: Schrodinger's equation"
    date: 2006-09-02
    body: "hey, Jos got the dbus bindings more or less working. it won't be long until he does another release, and hopefully, by then, you can have a look at it..."
    author: "superstoned"
  - subject: "Gratz!"
    date: 2006-08-31
    body: "Congrats to Carsten on the great publicity. And, since it's from their department, I can't resist posting (for those who haven't seen them) <a href=\"http://www.dhmo.org/facts.html\">the facts about dihydrogen monoxide</a>. :-)"
    author: "Philip Rodrigues"
  - subject: "Re: Gratz!"
    date: 2006-08-31
    body: "...and so Phil learnt why he should use the preview button. D'oh!"
    author: "Philip Rodrigues"
  - subject: "Re: Gratz!"
    date: 2006-09-01
    body: "You shouldn't be posting links to such dangerous stuff on the dot, be the topic of the article related to chemistry or not.  I've heard that this stuff was recently even banned from planes in the UK. \n\n\n\n\n\n;-)"
    author: "cm"
  - subject: "Kalzium vs Periodic Table 4"
    date: 2006-08-31
    body: "http://www.synergycreations.com/periodic/index.html\n\nWhen Kalzium trumps this application then we'll have something to really celebrate."
    author: "Marc Driftmeyer"
  - subject: "Re: Kalzium vs Periodic Table 4"
    date: 2006-08-31
    body: "Could you be more specific? What features does that program have that are missing in Kalzium?"
    author: "ac"
  - subject: "Re: Kalzium vs Periodic Table 4"
    date: 2006-09-02
    body: "http://www.reanimality.com/hidden/Quick_Reference.jpg\n\nThe equation tool in Kalzium is nice and the application has matured immensely. Competition breeds best of breed. I expect with the resources possible from KDE that Kalzium can really be an example of an useful Scientific application any university/graduate student/professional could leverage.\n\nConstructive criticism and challenge always brings out the best."
    author: "Marc Driftmeyer"
  - subject: "Re: Kalzium vs Periodic Table 4"
    date: 2006-09-01
    body: "You are an asshole, whether you know it or not!\n\nIn case you don't know it, let me explain why:\n\n1) You fail to explain yourself fully and offer nothing in the form of constructive criticism.\n\n2) The intent of your post is to rain on somebody else's parade, rather than improve an existing application. If the latter had been your intent, you would have filed bugs or wish requests in a timely and ongoing manner with the developers.\n\nSo get a life while you can because people around you must surely know how much it stinks to be surrounded by someone who can only express vitriol towards others."
    author: "Me or somebody like me"
  - subject: "Re: Kalzium vs Periodic Table 4"
    date: 2006-09-01
    body: "He's just jealous :o)"
    author: "AC"
  - subject: "Re: Kalzium vs Periodic Table 4"
    date: 2006-09-01
    body: "Please tell me what that app is able to do what Kalzium can't. And believe me, Kalzium has several features that app has not ;-)\n\nWhat Kalzium is missing is for example the customizable gradient-view (http://www.synergycreations.com/periodic/screenshots/deltahf.png), but I am not sure if that doesn't just make the UI more complex... But I do want the log-scale, yes.\nThen the \"Element box\" is configurable\". Kalzium offers two default \"element boxes\" and I think that is enough. Of course, I could add a third or make it configurable as well... Not sure if that is really needed...\n\nhttp://www.synergycreations.com/periodic/screenshots/deltahf.png is possible in Kalzium as well, it just looks better ;-)\n\nDon't tell me you like this dialog better http://www.synergycreations.com/periodic/screenshots/pd-isotope.jpg than Kalzium's (for example http://edu.kde.org/kalzium/pics/screen2.png).\n\nI would love to have this one... But I'd need help there. http://www.synergycreations.com/alchemist/index.html"
    author: "Carsten Niehaus"
  - subject: "Update on the Equation Solver"
    date: 2006-09-01
    body: "For one, I finally added a screenshot of it to the homepage:\n\nhttp://edu.kde.org/kalzium/pics/kalzium-eq-solver.png\n\nThen I got confimation that the solver is finally working in Debian SID and probably also in Debian Testing."
    author: "Carsten Niehaus"
  - subject: "teaching"
    date: 2006-09-01
    body: "I always asked myself what's the use of periodic table applications. \n\nMaybe it makes sense to integrate videos etc. about the elements and their reaction groups or add real educational content e.g. sample experiments."
    author: "furangu"
  - subject: "Re: teaching"
    date: 2006-09-01
    body: "well, there are about 1000 to 2000 experiments of which I own descriptions (books, printouts, schoolbooks). Which of those do you film? If \"all\" is the answer kalzium will have about 2 gig data.\nif you have nice video-material about an element or something, please publish it in the wikipedia (commons.wikipedia.org), Kalzium could make use of it in that case.\nI don't own a videocam, therefor I need free content for those things."
    author: "Carsten Niehaus"
---
NewsForge reports on <a href="http://software.newsforge.com/software/06/08/14/2052256.shtml?tid=130&amp;tid=138">how Kalzium was created</a>.  '<em>As a teacher in Lower Saxony, Germany, one of Niehaus' main goals when developing Kalzium (the German word for Calcium) was to write an application that was both a teaching and a learning tool. "I want to be able to demonstrate things and I also want my students to be able to learn things from Kalzium and to use it as a reference," he says.'</em>


<!--break-->
