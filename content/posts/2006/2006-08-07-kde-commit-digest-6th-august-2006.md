---
title: "KDE Commit-Digest for 6th August 2006"
date:    2006-08-07
authors:
  - "dallen"
slug:    kde-commit-digest-6th-august-2006
comments:
  - subject: "AAahhh"
    date: 2006-08-06
    body: "Always great to find out a new commit digest has been posted :) \n\nThanks a lot!"
    author: "Bart Verwilst"
  - subject: "Re: AAahhh"
    date: 2006-08-07
    body: "And again with a Dutch translation!!\n\nKeep up the good work guys!!\n\nmay many other languages follow!"
    author: "AC"
  - subject: "unity"
    date: 2006-08-07
    body: "it would be cool to \"replace\" khtml with Unity (yes I know it\u00b4s based on it :) so that we can take advantage of all the cool stuff coming from Apple. Especially since most khtml devs work for Apple now so we can get that kind of page http://diverged.org/thumbnail/ to work properly with konqueror.\n\nthanx for the good work!"
    author: "Pat"
  - subject: "Re: unity"
    date: 2006-08-07
    body: "This page seems to work fine in Konqueror 3.5.4. What problem are you having?"
    author: "Alistair John Strachan"
  - subject: "Re: unity"
    date: 2006-08-07
    body: "did you click on the thumbnails?"
    author: "Pat"
  - subject: "Re: unity"
    date: 2006-08-07
    body: "I did. They grow to full size. What was supposed to happen?"
    author: "dolio"
  - subject: "Re: unity"
    date: 2006-08-07
    body: "Seems to work fine here too. KDE 3.5.4. In Firefoy & Konqueror Thumbnails grow to full size"
    author: "MaBu"
  - subject: "Re: unity"
    date: 2006-08-07
    body: "Apple is merging WebKit with KHTML for Unity.\n\nhttp://webkit.opendarwin.org/blog/"
    author: "Marc Driftmeyer"
  - subject: "Re: unity"
    date: 2006-08-07
    body: "That's not an accurate summary of things. It's been ages since Apple merged something from our tree that wasn't given to them directly or by a 3rd party.\n\n"
    author: "SadEagle"
  - subject: "Re: unity"
    date: 2006-08-07
    body: "It works fine here on Konqueror 3.5.3 as well (the images will zoom in and out fine)."
    author: "Corbin"
  - subject: "Desktop search"
    date: 2006-08-07
    body: "I'm confused. There's kat, strigi and a bunch of beagle-frontends. What's the way desktop search in KDE will go? Is there a search framework that will be integrated into KDE4?\nBeagle - most mature but dog slow?\nKat - dead?\nStrigi - nice beginning, but needs loooots of work?\nTenor - very advanced ideas but very few code?\n\nAny chance we see a usable desktop search (integrated) in KDE4, or do we have to continue using kfind?"
    author: "birdy"
  - subject: "Re: Desktop search"
    date: 2006-08-07
    body: "You forgot about kerry :)\nhttp://kde-apps.org/content/show.php?content=36832"
    author: "AC"
  - subject: "Re: Desktop search"
    date: 2006-08-07
    body: "He mentioned that - \"a bunch of beagle-frontends\"."
    author: "AC"
  - subject: "Re: Desktop search"
    date: 2006-08-07
    body: "Most of KDE 4 is just vaporware. I don't want to be a troll but is the true right now. I've heard a lot of things about the new shiny and improved KDE 4 will be, with innovative things like Gecko HTML rendering, the Plasma engine, the awesome Tenor that will never appear, the new usable UI, the new color scheme, etc, etc."
    author: "onakani"
  - subject: "Re: Desktop search"
    date: 2006-08-07
    body: "> Most of KDE 4 is just vaporware.\n\nOf course it is, at this time. There istn't even an alpha version out yet, but there will be."
    author: "Ale"
  - subject: "Re: Desktop search"
    date: 2006-08-07
    body: "They're working hard on the basics right now, but it does seem to be coming together for the most part.  I agree on Tenor, though.  That's the only search engine I have any interest in using.  This whole idea of using huge CPU/IO resource to index your files just so that it won't take huge CPU/IO to look something up later is quite useless for my needs.  Obviously others may have different workflows and might find it useful, but for me, no.  The idea of each app communicating and sharing hints about what to might be useful to you at that moment though, even if you haven't remembered it yourself... that's next generation computing stuff.  GNOME's dashboard project looked incredible, except that it was .NET based.  I just hope the KDE folks can pull this off eventually :)"
    author: "Lee"
  - subject: "Re: Desktop search"
    date: 2006-08-07
    body: "just read the digest, a first tool to create Plasmoids for plasma has been imported - i wouldn't call that vapor ware. Gecko HTML rendering is not that much needed, especially if KTHML and WebCore can unite through Unity. and it was never promised for KDE 4. \n\nTenor is an idea, never had much code. Strigi is actually almost ready to receive some input and work to achieve tenor-like capabilities. who's volunteering? some input from the person who thought up tenor, Scott Wheeler, would be great. anyone knows if he could help, ask 'em to join on #kat...\n\nStrigi is ATM one of the fastest, if not THE fastest indexer/crawler, and has some unique capabilities like traversing through archives. It needs some help, esp in the gui area, and other apps which are willing to start using it for their internal database needs. Kalzium is interesting, btw..."
    author: "superstoned"
  - subject: "Re: Desktop search"
    date: 2006-08-07
    body: "Well you didn't mention Tracker http://freedesktop.org/wiki/Software_2fTracker .\n\nIt is in fact the fastest search framework out there and already has more features that Strigi."
    author: "Sunny Sachanandani"
  - subject: "ooops  I  thought kde3.5 is over"
    date: 2006-08-07
    body: "you have to expect such a silly question when a newbie begun to read commit-digest;)\n\n\nmy question is kde3.5.4 is the last serie in kde3.5 so why I read about changes commited in kde3.5.3 \n\nanyway happy to read at last there is some REAL code about plasma"
    author: "morphado"
  - subject: "Re: ooops  I  thought kde3.5 is over"
    date: 2006-08-08
    body: "There is going to be a KDE 3.5.5. It will probably include Kopete 0.12.2, and I'm sure the developers are going to find other bugs to purge."
    author: "Jakob Petsovits"
  - subject: "Re: ooops  I  thought kde3.5 is over"
    date: 2006-08-08
    body: "The release-plan has just been updated, 3.5.5 will be released in october (post aKademy). There will probably even be more releases."
    author: "Carsten Niehaus"
---
In <a href="http://commit-digest.org/issues/2006-08-06/">this week's KDE Commit-Digest</a>: Support for <a href="http://en.wikipedia.org/wiki/PostScript">PostScript</a> page deletion and editing of metadata in KViewShell, and for using a SQL backend with <a href="http://kphotoalbum.org/">KPhotoAlbum</a> (feature derived from KexiDB). <a href="http://www.vandenoever.info/software/strigi/">Strigi</a> gets support for <a href="http://en.wikipedia.org/wiki/Inotify">inotify</a>. Plasmagik, an application to assist developers in making "Plasmoids" (<a href="http://plasma.kde.org/">Plasma</a> applets), is imported into KDE SVN. Rendering development work continues in the <a href="http://dot.kde.org/1152645965/">Unity</a> web rendering engine. Work stars on a "Magnetic Outline Selection" tool for <a href="http://www.koffice.org/krita/">Krita</a>.

<!--break-->
