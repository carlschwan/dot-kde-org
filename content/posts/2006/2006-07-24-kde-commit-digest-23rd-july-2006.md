---
title: " KDE Commit-Digest for 23rd July 2006"
date:    2006-07-24
authors:
  - "dallen"
slug:    kde-commit-digest-23rd-july-2006
comments:
  - subject: "KDE 3.5.4 :))"
    date: 2006-07-23
    body: "I see a lot of bugfixes !\nhttp://www.kde.org/announcements/changelogs/changelog3_5_3to3_5_4.php\nGreat work!"
    author: "zvonsully"
  - subject: "Re: KDE 3.5.4 :))"
    date: 2006-07-24
    body: "reads like *breakthrough* technology :-)\n\n*joking*"
    author: "ch"
  - subject: "Re: KDE 3.5.4 :))"
    date: 2006-07-24
    body: "I agree. I especially like seeing all the KHTML fixes.\n\nAlso, it's nice to see the fix for Bug 128610: kscreensaver does not launch screensaver after x minutes.\n"
    author: "Elijah Lofgren"
  - subject: "Re: KDE 3.5.4 :))"
    date: 2006-07-26
    body: "I for one welcome our KDE 3.5-patching overlords."
    author: "JVz"
  - subject: "On the Webkit news."
    date: 2006-07-24
    body: "So is this basically an admission that KHTML is being dumped in favor of Webkit? More's the pity."
    author: "anon"
  - subject: "Re: On the Webkit news."
    date: 2006-07-24
    body: "Looks more like KHTML will (possibly) be resynced up with WebKit, which would greatly increase the number of developers working directly on the code."
    author: "Corbin"
  - subject: "Re: On the Webkit news."
    date: 2006-07-24
    body: "given that webkit is a khtml fork which has had a good amount of cross pollination of patches with khtml, if it does occur (there's still more work to be done before that is a for sure thing of course) it mostly means bringing a greater number of developers working on the same branch of the code.\n\nwhat are you concerned might be lost in this? (your message is a bit vague on the details =)"
    author: "Aaron J. Seigo"
  - subject: "Re: On the Webkit news."
    date: 2006-07-24
    body: "IIRC, after the apples had decided to use KHTML, the problem with webkit was that they only cared about some fubared websites being displayed correctly and added lots of quick hacks to webkit, making the code more complicated and cluttered and slowly killing KHTMLs clean design.\n\nI have no clue of KHTML(s design), this is just what I remember. Not true?"
    author: "me"
  - subject: "Re: On the Webkit news."
    date: 2006-07-24
    body: "it is quite true that the kde khtml devs had a higher code quality standard in certain places than the apple webkit khtml devs did. not to the point of making webkit khtml unhackable or a nightmare. and working together should help improve that while bringing more people to bear on the codebase. this should result in more bugfixes (ours and theirs), more features (ours and theirs) and more code correctness.\n\nor it could simply lead nowhere and we maintain the khtml root branch forever. we'll see in the next 4-6 months."
    author: "Aaron J. Seigo"
  - subject: "Re: On the Webkit news."
    date: 2006-07-25
    body: "The question is: What breaks khtml?\n\nMaybe I am wrong but page rendering is not really a problem anymore. I did not come across pages which broke, some rendered better. Speed is a problem but as processor power grows solved by time.\n\nToday we have four mature engines and all work pretty well.\n\nThe problems of konqueror are usabilitywise. E.g. ugly popups. This is what annoys users, not khtml. Khtml is good enough. So if webkit results in more patches even better. I wonder why nobody tried to write a safari clone yet."
    author: "Itchy "
  - subject: "Re: On the Webkit news."
    date: 2006-07-25
    body: "> but page rendering is not really a problem anymore\n\nyes, things are -very- good in khtml these days. there are still websites that bork, however (such as my hotel's gateway page last week which prevented me from getting an ip address). beyond rendering there are performance improvements and feature enhancements such as richtext editing and canvas.\n\n> if webkit results in more patches even better\n\nexactly the point =)"
    author: "Aaron J. Seigo"
  - subject: "Re: On the Webkit news."
    date: 2006-07-28
    body: "They have. It's called Shiira. It's based on WebKit and Cocoa.\n\n<a href=\"http://hmdt-web.net/shiira/en\">http://hmdt-web.net/shiira/en</a>"
    author: "Marc Driftmeyer"
  - subject: "Re: On the Webkit news."
    date: 2007-06-12
    body: "> Speed is a problem but as processor power grows solved by time.\n\nI don't give a **** what processor power is nowadays, I want software to run nicely on a 400Mhz."
    author: "Nicolas"
  - subject: "Did you not get the Memo?"
    date: 2006-07-24
    body: "> KPhotoAlbum is the new name for KimDaBa\n\nWhat was so wrong with the old name that a change of capitalisation couldn't fix?  I can sort-of understand changing as it's not entirely obvious, but surely you could have found something better than yet another KWhatIAmName???  I think we're all in general agreement that we're past that now, surely?  It's OK for the desktop utilities that are part of the main project, but for an add-on app a little originality wouldn't go astray.\n\nIf you're going to make the effort of re-naming everything, make sure it's worth the effort.  I'm sure the peanut gallery can come up with a few suggestions?  In a foreign language perhaps?  Heck, feel free to drop the K entirely!  Maybe run a competition, winner gets their name in the 'About' dialog?\n\nJohn."
    author: "Odysseus"
  - subject: "Re: Did you not get the Memo?"
    date: 2006-07-24
    body: "I agree. KPhotoAlbum is a very generic KDE application name, while KimDaBA has a certain unique ring to it and sounds much more like a full-blown application. KimDaBa is easier to pronounce, to type, to remember and is not so repelling for non-KDE users. I'm usually not the one to complain about stuff like that, but that change is not a good idea, IMHO. "
    author: "Michael Jahn"
  - subject: "Re: Did you not get the Memo?"
    date: 2006-07-24
    body: "These \"unusual\" names have a big advantage: If you have trouble, just put it in google and you won't be flooded with results. Besides, strange names are easier to remember since they stand out. And hey, there funny."
    author: "AJ"
  - subject: "Re: Did you not get the Memo?"
    date: 2006-07-24
    body: "I really had my troubles with remembering Kimdarba. It's like trying to remember some names from cultures that I'm not so familiar with -- like thai city names or asian dishes. So with respect to this I disagree.\n\nApart from that: People complained before the name change. There was a chance to voice up and suggest a better name to the author. And now some people complain again. Have we really turned into a group of complainers?\n\nI don't think this is even worth a discussion. KPhotoalbum is unique and catchy enough to appear in google without any chance to mix it up with something else. And it's immediately clear what it's meant for.\n\nLong live KPhotoalbum!\n"
    author: "Monkey Boy"
  - subject: "Re: Did you not get the Memo?"
    date: 2006-07-24
    body: "Well, I took the chance and suggested to stay at KimDaBa but I was overruled. This is ok with me, majority decides. But KPhotoAlbum may be unique, but searches get more fuzzy and may left the leading K out, since this makes it a regular word, and voila you're in trouble."
    author: "AJ"
  - subject: "don't kill my sweet heart kde3.5"
    date: 2006-07-24
    body: "hello\n\nI read about the future release plan of KDE3.5.4 it says tagged on 23-07-06, but where is the plan for further release of KDE3.5 please don't tell me that 3.5.4 is the last release\n\n\nI really love my kde3.5"
    author: "morphado"
  - subject: "Re: don't kill my sweet heart kde3.5"
    date: 2006-07-24
    body: "If you love 3.5, then keep using it!  You don't have to stop using it just because no further releases are planned.  \n\n"
    author: "LMCBoy"
  - subject: "Re: don't kill my sweet heart kde3.5"
    date: 2006-07-24
    body: "That's was really a funny answer."
    author: "Ignacio Monge"
  - subject: "Re: don't kill my sweet heart kde3.5"
    date: 2006-07-25
    body: "I think people should care about the long lag between 3.5.4 and upcoming release of kde4,specially when most distribution who care about stability will wait till kde4 become really stable let says at last kde4.0.1 \n\nGnome people should be really happy"
    author: "MORPHADO"
  - subject: "Re: don't kill my sweet heart kde3.5"
    date: 2006-07-25
    body: "It happened before: people were stuck on KDE 1.1.1 for _ages_ before KDE 2.0 came out, and it didn't have too much of an effect. Some people moved over to Gnome which had gained theming support, but they moved back to KDE when it came out. I imagine the same will happen once more, although I'm just moving _back_ from Ubuntu to Debian Sarge running KDE, as that is the system that works for me.\n\nAs for distributions waiting for stability, that's a bit of a joke: the only distributions that wait are Debian and the enterprise desktops (RHEL, SLED). Pretty much everyone else (Fedora, OpenSuSE, *Ubuntu) jumps right in at the earliest opportunity. I wouldn't be surprised if there were some installable live-CDs floating around with KDE 4-beta when it ships."
    author: "Bryan Feeney"
  - subject: "New file in SVN causes exception in ViewCVS Diff"
    date: 2006-07-25
    body: "I clicked the _Diff_ link to read the flake-overview, and http://websvn.kde.org/trunk/koffice/libs/flake/flake-overview?r1=564922&r2=564923 displayed \"ViewCVS Exception   Invalid path(s) or revision(s) passed to diff\"\n\nThe r1 revision number is bogus.  This may be happening because it's a new file, not a change. The exception also happens when I click the _Diff_ link for other new files, e.g. new Arev font\nhttp://websvn.kde.org/branches/koffice/1.6/koffice/lib/kformula/fonts/Arev.ttf?r1=565151&r2=565152\nI'm not sure how to get ViewCVS's diff mode to do the right thing for a new file.\n\nIt would be nifty if the commit-digest could tell it's a new file so instead of saying \"Thomas Zander committed a change to /trunk/koffice/libs/flake/flake-overview: ... _Diff_\" it said \"commited new file(s) ... _View_\" with a link to View instead of Diff.\n\nI really appreciate these summaries!"
    author: "S Page"
---
In <a href="http://commit-digest.org/issues/2006-07-23/">this week's KDE Commit-Digest</a>: <a href="http://www.kdevelop.org/">KDevelop</a> gets new configuration framework functionality. The start of a Satellite tracks feature in <a href="http://edu.kde.org/kstars/">KStars</a>. Support for <a href="http://en.wikipedia.org/wiki/Pdf">PDF</a> data extraction, and speed optimisations in <a href="http://www.vandenoever.info/software/strigi/">Strigi</a>. New features in <a href="http://kphotoalbum.org/">KPhotoAlbum</a> (KPhotoAlbum is the new name for KimDaBa). Perspective grid support in <a href="http://www.koffice.org/krita/">Krita</a>, with the implementation of a Bezier tool becoming feature-complete. More work on unit conversions in <a href="http://krecipes.sourceforge.net/">KRecipes</a>. Porting of KRDC to KDE 4.

<!--break-->
