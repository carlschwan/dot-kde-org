---
title: "Interview with kde-redhat Project Leader Rex Dieter"
date:    2006-01-03
authors:
  - "jriddell"
slug:    interview-kde-redhat-project-leader-rex-dieter
comments:
  - subject: "KDE"
    date: 2006-01-02
    body: "\"No, and they can't really. We include some features that Red Hat/Fedora simply cannot (due to licensing and/or trademarks). However, there is a movement to move KDE out of Fedora Core and into Fedora Extras so that it can be produced and maintained by the community (instead of exclusively by Red Hat). See the Fedora KDE wiki page.\"\n\nHow smart you Americans are. Move out KDE...\n\nDespite the fact that RedHat distribute their own versions of KDE which fail the high standards of KDE, why are all the other distributors able to provide KDE properly? \n\nRex, your work is very nice and useful. Thank you! Perhaps quality of KDE will convince RedHat that the desktop market is worth a second try."
    author: "Bert"
  - subject: "Re: KDE"
    date: 2006-01-02
    body: "\n> Perhaps quality of KDE will convince RedHat that the desktop market is worth a second try.\n\nLOL! What's your second guess? They have been pouring eons of money into Gnome for ages now and can't (human nature) possibly stop now.\n\nMoving KDE to unsupported territory via 'Unleash KDE' hassle is just a trial to not upset too many people about this. Trust me on this, they are not KDE friendly people in any way. They try to steer as many people away from it as possible.\n\nSadly, to me it looks like SUSE is now taking the same path. But as usual, they are few steps behind. Lets hope i'm wrong though.\n"
    author: "Anonymous Coward"
  - subject: "Re: KDE"
    date: 2006-01-02
    body: "\"Sadly, to me it looks like SUSE is now taking the same path. But as usual, they are few steps behind. Lets hope i'm wrong though.\"\nWell, you're wrong. :P\n\nEven though Novell has chosen to use GNOME as their default desktop environment on their enterprise products (SUSE Linux Enterprise Server, Novell Linux Desktop...), KDE is still included and works just as well as it did when it was the default desktop environment, if you want KDE on Novell's enterprise products, you just choose it when it asks you whether to install GNOME or KDE. So Novell supports both environments, just to quote an interview with Greg Mancusi-Ungaro:\n\"We want to continue to do the right thing for customers, we're gonna continue to ship both, the change that we made in our public stance, is really a very minor one I think in the long whole.\"\n\nAs far as the good old \"SUSE Linux\" goes, it will still have KDE as it's default desktop, and KDE and GNOME will be given equal focus, so it will still be the good old SUSE Linux we've known for years."
    author: "Troels Just"
  - subject: "Re: KDE"
    date: 2006-01-03
    body: "\n> As far as the good old \"SUSE Linux\" goes, it will still have KDE as it's default desktop, and KDE and GNOME will be given equal focus\n\nHardly. Gnomes framework mostly sucks, it takes ten times the money to get it working in any decent way."
    author: "Anonymous Coward"
  - subject: "Re: KDE"
    date: 2006-01-03
    body: ">Moving KDE to unsupported territory via 'Unleash KDE' hassle is just a trial to\n>not upset too many people about this. Trust me on this, they are not KDE friendly\n>people in any way. They try to steer as many people away from it as possible.\n\nIt's worth mentioning that Fedora, as a whole, is unsupported by Red Hat.  So moving it to Extras makes no difference from a support statement.\n\nAs for being KDE friendly, traditionally this is true.  However quite a few developers are tiring of what Gnome is becoming and are switching to KDE."
    author: "Josh Boyer"
  - subject: "Re: KDE"
    date: 2006-01-03
    body: "which developers you speak of?"
    author: "brother"
  - subject: "Re: KDE"
    date: 2006-01-03
    body: "me obviously ;-)\n"
    author: "ac"
  - subject: "Re: KDE"
    date: 2006-06-07
    body: "KDE is not much different in appearance since the BlueCurve move which was implemented awhile back. I see little difference between the initial look but see many differences in configuration for the desktops. The QT issue from earlier days does make one weary regarding depending on one desktop alone and increasing its user base. My reluctance is rather in how I percieve KDE and its functionality and feel.\nIf Red Hat discourages or encourages KDE usage is not known by me. I see both choices given updates frequently.\n\nRegarding support, both Core and Extras packages use the same bugzilla reporting site. If it is busted, reports to bugzilla should be resolved upstream or downstream.\n\nI am tiring of GNOME \"simplification\" but am not compelled to switch to KDE because of it. Currently, I am using xfce as a desktop. Next session will be another choice, GNOME is my highly used choice.\n\nRegarding Rex and what he can add to KDE development/maintenence for Fedora, the influence and dediction to work in both directions with KDE development and integration with Fedora should be a highly successful venture.\n\nJim Cornette"
    author: "Jim Cornette"
  - subject: "Re: KDE"
    date: 2006-01-02
    body: "Looking at these screenshots of the latest Fedora:\n\nhttp://shots.osdir.com/slideshows/slideshow.php?release=513&slide=38\n\nmakes me wonder whether Redhat doesn't go out of its way to make their version of KDE as unappealing as possible. Notice the lack of background image, the absurdly small fonts, the stone-age window/button theme, and the error messages. \n\nAlso notice how GNOME on Fedora copied KDE's default Plastik color theme (light blue window borders and white letters/buttons), but they felt obligated to change that default on KDE just so that it looks out of place.\n\nRex, thanks for showing Redhat users what KDE can really do."
    author: "Vlad C."
  - subject: "Re: KDE"
    date: 2006-01-03
    body: "\n\n\"\nmakes me wonder whether Redhat doesn't go out of its way to make their version of KDE as unappealing as possible. Notice the lack of background image, the absurdly small fonts, the stone-age window/button theme, and the error messages.\n\"\n\nWhere are your bug reports?\n\n\n\n\n\"\nAlso notice how GNOME on Fedora copied KDE's default Plastik color theme (light blue window borders and white letters/buttons), but they felt obligated to change that default on KDE just so that it looks out of place.\"\n\nGNOME didnt copy any theme. The plastik color theme isnt original in anyway. Tons of themes before it use the same color scheme."
    author: "Creo"
  - subject: "Re: KDE"
    date: 2006-01-04
    body: "\"Where are the bug reports?\"\n\nHow about here?\n\nhttp://bugzilla.redhat.com/bugzilla/136533"
    author: "amadeo"
  - subject: "Re: KDE"
    date: 2006-01-05
    body: "\nIs that the best you could come up with?. If so then it clearly shows that Fedora is atleast no less good than any other distribution since such bugs are part of every distribution out there. Which distribution do you want me to show similar reports?\n\n"
    author: "Jack"
  - subject: "Re: KDE"
    date: 2006-01-03
    body: "\"Despite the fact that RedHat distribute their own versions of KDE which fail the high standards of KDE, why are all the other distributors able to provide KDE properly?\"\n\nWhats unproper about KDE packages in Fedora apart from the patent and licensing encumbered stuff <b> which have been left out </b> btw..."
    author: "Jack"
  - subject: "There is still hope!"
    date: 2006-01-02
    body: "I loved the picture! \nAs long as there are families, there will be a future!\n;-)\n\n/Goran"
    author: "G\u00f6ran Jartin"
  - subject: "There is still sweet hope!"
    date: 2006-01-03
    body: "that's a sweet family :)"
    author: "fast_rizwaan"
  - subject: "A zillion thanks"
    date: 2006-01-03
    body: "I started getting really annoyed back in the days of early kde3 (I think) and RH9 when I couldn't find any RPMs for the latest and greatest KDE releases.  Now, thanks to the kde-redhat project I get a simple yum update about 2 weeks after KDE presses get finished.  (I have to wait for stable for, um, stability purposes).\n\nNo longer have I had to go searching for RPMs that didn't exist or try to self compile (which is always possible, but painful).  KDE-RedHat is one of the most helpful projects out there!"
    author: "Wes Hardaker"
  - subject: "Re: A zillion thanks"
    date: 2006-01-11
    body: "How about:\n\nwget http://developer.kde.org/build/konstruct/stable/konstruct-stable.tar.bz2\ntar xvjf konstruct-stable.tar.bz2\ncd konstruct/meta/kde\nmake install\n\n:-)"
    author: "piters"
  - subject: "Rex Rocks"
    date: 2006-01-03
    body: "I use Fedora Core for my everyday work, and it is only thanks to Rex if I can use the best Desktop System and some of the best software apps that are out there!!!\n\nRex Rocks!!!\n"
    author: "Lorenzo"
  - subject: "Re: Rex Rocks"
    date: 2006-01-12
    body: "Thanks Rex, your work is greatly appreciated.\nI've been using KDE-Redhat stable and testing\nfor several years.\n\nI don't think I could stay with Fedora without it."
    author: "Scott Murray"
  - subject: "thanks rex"
    date: 2006-01-21
    body: "thanks, kde-redhat is fantastic\n\nsean,\nIreland"
    author: "sean"
---
Rex Dieter has been making the <a href="http://kde-redhat.sourceforge.net/">unofficial KDE Red Hat</a> packages for some years now.  Since this is a service depended upon by thousands of Red Hat users to get their required latest build of KDE, KDE Dot News interviewed Rex to find out how he got started, why the need for the project exists and how he makes the packages.  Read on for the interview.




<!--break-->
<div style="border: thin solid grey; padding: 1ex; margin: 1ex; width: 303px; float: right">
<img src="http://static.kdenews.org/jr/redhat-packages-rex-deiter.jpg" width="303" height="249" /><br />
Rex with some of his packages
</div>

<p><strong>Please introduce yourself and your position in KDE</strong></p>
 
<p>I am System Administrator for the Department of Mathematics at the University of Nebraska Lincoln. I hold no official KDE position other than that of packager, bugzilla/mailing-list lurker, and contributor of unofficial core packages. </p>
 
 
<p><strong>What does the KDE Red Hat project provide?</strong></p>
 
<p>KDE Red Hat primarily provides KDE core packages (kdelibs, kdebase, etc), 3rd party KDE application packages (amarok, k3b...), and mailing lists for our user community. </p>
 
 
<p><strong>Why is there a need for the KDE Red Hat project?</strong></p>
 
<p>When I started the project, there was quite a bit of animosity toward Red Hat from the KDE community. I set out to change that. It's hard to say how far we've come, but I think it's safe to say that we have built a growing, active, and enthusiastic user and developer community.</p>
 
 
<p><strong>How did you start with making these packages?</strong></p>
 
<p>Way way back, when I was using FVWM, I found some KDE 1.0 packages for Red Hat 5.2 built by Bernhard "bero" Rosenkraenzer (now of <a href="http://www.arklinux.org">ArkLinux</a> fame). Now, it wasn't very good by today's standards, but that's when I first became interested in KDE. It wasn't until KDE 2.0 that I started packaging KDE for our Red Hat Linux systems at work by mostly stealing... err, borrowing from Bero's Red Hat rawhide packages. By the time KDE 3.0 came around, I was getting pretty good at creating up-to-date KDE packages and started to think it might be nice to share at least some of this work with the public. So I came up with the idea of founding the kde-redhat project at sourceforge.net, and have been active ever since.</p>
 
 
<p><strong>How many people work on this?</strong></p>
 
<p>There are two project admins, Bob Tanner (who thought to register kde-redhat.org and taught me CVS basics) and myself. We currently have 7 or 8 project developers, who have CVS write access. In addition, we gladly accept package contributions from others as well.</p>
 
 
<p><strong>How much time do you spend on this?</strong></p>
 
<p>I spend anywhere between 5-20 hours per week, reading mailing lists, scouring bugzilla reports, and building packages.</p>
 
 
<p><strong>What are the differences between your packages and those from Red Hat?</strong></p>
 
<p>As time has passed, the differences are getting smaller and smaller. These days, the main differences are that we provide enhancements to KDE that Red Hat either can't enable (i.e., licensing/trademarks) or hasn't included (yet). For the latter bit, I've been trying to use kde-redhat as a test-case to Red Hat to include some or all of our improvements, and, for the most part, Red Hat has been very good accepting suggestions and enhancement requests. Sometimes it is still a slow process, so that usually means kde-redhat's packaging is still usually a step or two ahead.</p>
 
 
<p><strong>Do you know how many people use this service?</strong></p>
 
<p>I don't know, not by any measure remotely accurate anyway. OK, here's one wild statastic: 920421 RPMs were downloaded from our main repository, <a href="http://apt.kde-redhat.org/">http://apt.kde-redhat.org/</a> last month. I guess that probably adds up to few folks.</p>
 
 
<p><strong>Have Red Hat ever suggested your packages become the official KDE packages for Red Hat?</strong></p>
 
<p>No, and they can't really. We include some features that Red Hat/Fedora simply cannot (due to licensing and/or trademarks). However, there is a movement to move KDE out of Fedora Core and into Fedora Extras so that it can be produced and maintained by the community (instead of exclusively by Red Hat). See <a href="http://fedoraproject.org/wiki/KDE">the Fedora KDE wiki page</a>. If anything ever comes of that, I've committed to assist in any way I can.</p>
 
 
<p><strong>You offer both apt and yum sources, what are the differences between these two systems?</strong></p>
 
<p>apt and yum are two tools that accomplish approximately the same thing: package management and dependency solver. Turns out development on apt (for rpm) has ended, so at the moment, it appears to be an application whose time has come. In the meantime, apt's previous maintainer moved on to develop a new tool called <a href="http://labix.org/smart">Smart</a>, which appears to be very promising.</p>
 
<p><strong>Where do your get your install of Red Hat Enterprise from?</strong></p>
 
<p>I've used both <a href="http://www.whiteboxlinux.org/">WhiteboxLinux</a> and <a href="http://www.centos.org/">CentOS</a>, but I exclusively use CentOS these days.</p>
 
<p><strong>What hardware do you use to make these packages?</strong></p>
 
<p>I build packages using mock on two 2Ghz Pentium 4 boxes, and occasionally use an Athlon 2800+ running VirtualPC under Windows XP for the occasional manual package build or debugging session.</p>



