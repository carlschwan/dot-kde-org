---
title: "KOffice 1.5.2 Released"
date:    2006-07-14
authors:
  - "iwallin"
slug:    koffice-152-released
comments:
  - subject: "Digg.com and thanks!"
    date: 2006-07-14
    body: "I found about his on digg.com first:\nhttp://digg.com/linux_unix/KOffice_1.5.2_Released\nSo you can also digg the story there so more people will know about this.\n\nAnyways, thank you very much for working on KOffice. It is simply great office suite. I've switched from OOo and all is quite well. There are some incompatibilities with ODF files but generaly it is OK."
    author: "Mark"
  - subject: "Re: Digg.com and thanks!"
    date: 2006-07-14
    body: "This can't be emphasized enough: You can help to improve KOffice, if you file bug reports about the ODF incompabilities, which are not in bugs.kde.org yet. At best, you attach also a minimal test file.\n\nThe time of the developers is limited. If our users help us to find the bugs and describe the way to reproduce them, we're able to fix the issues a lot faster. Just do it! ;-)"
    author: "Stefan Nikolaus"
  - subject: "Re: Digg.com and thanks!"
    date: 2006-07-15
    body: "Now, there is a method to find crashes \n\n* you write a script which fetches random files in the specific format from google, import them to you application until it crashes. Then you found a file which crashes the program. "
    author: "gerd"
  - subject: "Re: Digg.com and thanks!"
    date: 2006-07-15
    body: "Good idea, but I guess it is more useful to find \"simple\" scenarios where an application fails. That aids debugging a lot."
    author: "pharaoh"
  - subject: "Re: Digg.com and thanks!"
    date: 2006-07-17
    body: "Well, truth to be told, I expected faster reaction to incompatibility-with-OOo bug# 115273 <https://bugs.kde.org/show_bug.cgi?id=115273>. I have even reinstalled both KOffice and OOo to update report for newer version, but I still not get anybody interested. Oh well."
    author: "Matej Cepl"
  - subject: "KOffice 1.5.2 in Debian"
    date: 2006-07-14
    body: "I'm uploading KOffice 1.5.2 packages to Debian right now, they should be available tonight in your nearest mirror ;)\n\nBest regards"
    author: "Isaac Clerencia"
  - subject: "Re: KOffice 1.5.2 in Debian"
    date: 2006-07-14
    body: "Great, I hope this bugfixed version quickly finds its way into Debian Etch, been waiting for Krita 1.5 on this ppc for a while. Too bad klik does not work on my ppc then I could run it tonight as soon as you upload."
    author: "Dennis"
  - subject: "Re: KOffice 1.5.2 in Debian"
    date: 2006-07-14
    body: "It looks like I overrated my upload bandwidth, and I've just been able to upload now. It should hit debian mirrors tomorrow. If you're too impatient you can try it as soon as it hits incoming.debian.org :) I really hope the Krita ppc bug is gone.\n\nBest regards"
    author: "Isaac Clerencia"
  - subject: "Re: KOffice 1.5.2 in Debian"
    date: 2006-07-16
    body: "it is gone."
    author: "Cyrille Berger"
  - subject: "Ark Linux packages available as well"
    date: 2006-07-14
    body: "<a href=\"http://www.arklinux.org/\">Ark Linux</a> users can simply \"apt-get dist-upgrade\" to get koffice 1.5.2.\n\nOthers in need of RPMs may try their luck with the packages found in\n<a href=\"http://arklinux.osuosl.org/dockyard-devel/\">our repository</a>.\n"
    author: "Bernhard Rosenkraenzer"
  - subject: "KOffice gets better"
    date: 2006-07-14
    body: "Interesting to see how KOffice gets more and more stable. Is it stable enough for writing essays, papers and large documents? Books with lots of images in it?\n\nFeaturewise, 1.5 already has enough fetures to be more advanced than MS Works.\n\nThe chart function in KSpread is a bit difficult to use though."
    author: "Ale"
  - subject: "Re: KOffice gets better"
    date: 2006-07-15
    body: "I use Kword for essays and smaller papers. I'm not sure about really big documents, but it works for me."
    author: "superstoned"
  - subject: "Crashes"
    date: 2006-07-15
    body: "I wonder why software continues to have severe bugs. It is nice that crashes are fixed in 1.5.2 but this also means that 1.5.1 is buggy."
    author: "Martin"
  - subject: "Re: Crashes"
    date: 2006-07-15
    body: "LOL wow, you made my day!\n\nUnfortunately it is a reality the software is buggy. Some more, some less. Show me one piece of software with more than 5 lines of code which isn't!?!\n\nMore important is that there is an organized way to fix these bugs. And KDE / KOffice does have it. \n\nAlso you, like every user, can report bugs, document them, show a testcase (which you obviously have when you did experience a bug, just try to reduce it to the minimum required to trigger the bug.) Then this bug will be history quickly.\n\nI'm using KOffice (mainly KWord and KSpread) in production for a long time, and I can see that it has reached a very good level by now, and that without the bloat which makes me hesitating to even start OO. There are still a few cases where I need other features, not yet available in KOffice, but they get less and less. \n\nKeep on going you KOffice guys, your effort is very much appreciated!\n\n"
    author: "Matt"
  - subject: "metalink for KOffice 1.5.2"
    date: 2006-07-15
    body: "a .metalink for KOffice is available from http://www.metalinker.org/samples/koffice-1.5.2.tar.bz2.metalink\n\naria2 (http://aria2.sourceforge.net/) is a good client.\n\n(metalinks uses mirrors & checksums automatically for downloads)."
    author: "Ant Bryan"
  - subject: "congrates on the new release"
    date: 2006-07-15
    body: "...especially on Krita, as I use it for most of the graphics job :)\n\nThanks,\nWei Mingzhi"
    author: "Wei Mingzhi"
  - subject: "Re: congrates on the new release"
    date: 2006-07-16
    body: "I am testing Koffice and against other opensource office suite I am still not able to read properly some ppt Files. \nI mean the import of such a MS files is still buggy. \nIs this a behavior for other users too?\nThx for attention\nregrads\nJ.-L"
    author: "Juan Ehrenhaus"
  - subject: "Re: congrates on the new release"
    date: 2006-07-19
    body: "since ther are no change in kpresenter that means probably that I'll have the same problem with thi new version to open odp file where the object with a transparent background a little bit opac and a font withe a white color was not keep when open with kpresenter so all my text was black on a dark background...\nI have to make a sample file and to fill a bug but I didn't take time to do it (I saw this problem last week in a conference)"
    author: "humufr"
  - subject: "Why?"
    date: 2006-07-21
    body: "Why use this over OpenOffice?"
    author: "Anonymous Coward"
  - subject: "Re: Why?"
    date: 2006-07-21
    body: "Because it integrates better with KDE, its applications start up faster, and in the case of Krita, Kexi and KPlato, because there is no equivalent application in OpenOffice.org or the equivalent is not nearly as good.\n\nI have both on my machine but I prefer to use KOffice where possible because of these things."
    author: "Paul Eggleton"
  - subject: "Re: Why?"
    date: 2006-07-22
    body: "Openoffice is slow as a dog and also uses a lot of memory. Besides the pop-ups in Writer everytime when I click in a table are a pain in the ass. There are other pop-ups too, but as I don't often open it, I can't tell you what they are.\n\nKoffice doesn't do this, it just does what it is told to do. And that's is perfect with me. A lot of people have difficulties with openoffice/ms-office because office 'knows' what their user wants and does things automatically. But what if the user doesn't want this behaviour at all? They cry for help all the time.\n\nAbiword does just fine too, but since I use KDE, Kword is my choice of preference."
    author: "Bouke Woudstra"
---
The KOffice team today released the <a href="http://www.koffice.org/releases/1.5.2-release.php">second bug-fix release</a> in their 1.5 series. Several crash bugs were fixed, as well as a PowerPC issue in Krita and of course many smaller issues.  There are also updated languages packs and a totally new language: Traditional Chinese. You can read more about it in the <a href="http://www.koffice.org/announcements/announce-1.5.2.php">full announcement</a>. A <a href="http://www.koffice.org/announcements/changelog-1.5.2.php">full changelog</a> is also available. Currently, you can download binary packages for <a href="http://kubuntu.org/announcements/koffice-152.php">Kubuntu</a> and <a href="http://download.kde.org/stable/koffice-1.5.2/SuSE/">SUSE</a>. 


<!--break-->
