---
title: "KOffice 1.6 Released"
date:    2006-10-17
authors:
  - "iwallin"
slug:    koffice-16-released
comments:
  - subject: "Bugfixes"
    date: 2006-10-17
    body: "I'd like to know whether this fixes the formatting bugs in KWord that has actually stopped me from using it. It's just things like suddenly rearranging text in the wrong way, not being able to position things properly etc. I've had to use AbiWord and OO.o which have their own bugs and are slower.\n\nAnyway, it's good to see the new release and the new features - Krita is the best free image editor along with GIMP, and Kexi is IMHO better than OOo Base."
    author: "Andrew Donnellan"
  - subject: "Re: Bugfixes"
    date: 2006-10-17
    body: "No, it probably doesn't: KWord had some crash fixes and a bit of OpenDocument compliance work done on it, but no major surgery on the text layout engine -- which is what it would take to fix things like the infamous table bugs or the printing metrics. However, those should all be fixed in 2.0, which will use the newer and much more capable Qt 4 text layout engine. Check out Thomas Zander's weblog st http://www.kdedevelopers.org/blog/91/ to see some screenshoty goodness... or you can build KDE 4 and koffice 2.0 to see for yourself. More help -- or just more eyes trying out the code and reporting bugs while they're still new and easy to fix -- is always appreciated! ^_^"
    author: "BKS"
  - subject: "fantastic - thank you"
    date: 2006-10-17
    body: "Whatever else - has anyone noticed the speed with which the applications open now? Nigh-on instantaneous.  Amazing, thank you.  "
    author: "Gerry"
  - subject: "Krita"
    date: 2006-10-17
    body: "Simply great. Krita is the _first_ OpenSource app supporting CMYK. The importance of this cant be stressed enough. Now I can use Krita for editing print images already. For web editing I still resort to GIMP though Krita is coming along nicely. Imagine one day I could open and edit images via KIO directly on a webserver via FISH or FTP. Amazing. My windows colleagues cant do that. Right now the only thing for web editing I'm really missing in Krita is a true Color-to-alpha filter like in GIMP and a better canvas resize dialog where I can select if the existing image is centered to the left, right, bottom, or top on the new canvas when I resize say a 10x10 canvas to a 16x16 canvas. In GIMP I can freely drag around the image on the new surface before clicking OK. I'm really missing this here. Anyway, thanks for the LOT of work they put already into Krita!! It certainly has not gone unnoticed for me.\n"
    author: "Michael"
  - subject: "Re: Krita"
    date: 2006-10-17
    body: "Just wonder will krita support raw format of each digital camera?\n"
    author: "younker"
  - subject: "Re: Krita"
    date: 2006-10-17
    body: "It already does -- using dcraw for now. For 2.0, I'll likely switch to Hubert Figuire's libopenraw."
    author: "Boudewijn Rempt"
  - subject: "Re: Krita"
    date: 2006-10-18
    body: "Do you mean that you can directly import the Bayer CFA image or that you simply use an external library to convert to a interpolated pixel image."
    author: "James Richard Tyrer"
  - subject: "Re: Krita"
    date: 2006-10-18
    body: "We don't use a library right now; we exit to use dcraw, and that generates an 8 or 16 bit rgba image."
    author: "Boudewijn Rempt"
  - subject: "Re: Krita"
    date: 2006-10-18
    body: "So, there is no support for raw images."
    author: "James Richard Tyrer"
  - subject: "Re: Krita"
    date: 2006-10-18
    body: "There is exactly as much support for raw images as any other application that can load raw images has. There are no applications that work directly on the unchanged data from a raw camera; every application needs to convert it first. And actually, most application use dcraw or dcraw-derived code for that, even Adobe applications. Some applications, like Lightzone may do that transparently; others make it explicit. But there is no application where, when you load a raw image, the colorpicker gives you the uninterpolated data, for instance."
    author: "Boudewijn Rempt"
  - subject: "Re: Krita"
    date: 2006-10-18
    body: "> There is exactly as much support for raw images as any other application that \n> can load raw images has. There are no applications that work directly on the \n> unchanged data from a raw camera.\n\nWhat do those programs that come with the cameras do?"
    author: "James Richard Tyrer"
  - subject: "Re: Krita"
    date: 2006-10-18
    body: "No, there is raw support. Maybe you did not understand correctly or Boudewijn Rempt was unclear.\n\nYou can open Raw files the same way as Jpeg or PNG files. When opening Raw files an additional dialog (with preview) opens where you can adjust white point, contrast,...\n\nOf course Krita can use 16 bit colors with raw files.\n\nJust download Krita and test yourself.\n\nIf krita uses it's own raw processing or a lib or an external dcraw should not matter als long as it works. And it works."
    author: "max"
  - subject: "Re: Krita"
    date: 2006-10-18
    body: "It may just be a semantics question, but if an external program produces an RGBA image which is _derived_ from the RAW image then it does not load the RAW data directly and you can not use Krita to tweak how the RAW image is converted into an RGB image.  I can't really see how this is an improvement over a JPEG image unless this is the only way to get a lossless image (if the camera doesn't offer Huffman JPEG).\n\nClearly, the term 'support' isn't being used in the same way as it is when you say that Krita supports CMYK.\n\nAdobe says that PhotoShop supports DNG image files but I have not used it and don't know exactly how it works.  I don't know if PS can do the same things as the proprietary software that ships with cameras.  But, if it doesn't, they what is the advantage of the DNG format?\n\nTo gain the full advantage of using DNG (or other RAW image) files, you need an application which supports interactive manipulation of the actual RAW data.\n\n"
    author: "James Richard Tyrer"
  - subject: "Re: Krita"
    date: 2006-10-18
    body: "Just because you can not see the solution doesn't mean it doesn't work.\n\nAs an engineer; you should first test the setup before commenting on it."
    author: "ac"
  - subject: "Re: Krita"
    date: 2006-10-18
    body: "I'm really not inclined to buy a copy of Adobe Shop and Windows just to try it.\n\nI am looking for an Open Source solution to working with DNG (and other RAW) image formats.\n\nAs I presume you know, images using the Bayer CFA system have 1/2 of the pixels for Green, 1/4 of the pixels for Red, and 1/4 of the pixels for Blue.  This means that 2/3 of the color information is missing and must be recovered by some method.\n\nhttp://en.wikipedia.org/wiki/Bayer_filter\n\nSo converting a RAW file to a full RGB image does not use a fixed algorithm like decompressing a PNG does.  There are various methods for filling in the missing pixel values and various paramaters for the values for antialias filters, etc..\n\nTherefore, what a serious photographer needs is a photo application that directly supports DNG (or other RAW) format."
    author: "James Richard Tyrer"
  - subject: "Re: Krita"
    date: 2006-10-18
    body: "There is _no_ open source application that uses an in-memory representation of the original raw data when applying filters and such. Every open source application uses dcraw; recently, however, digikam and Hubert Figuiere have been working on replacing dcraw with a library to load the raw data. As far as I can tell, the data is still represented in memory as rgb pixels. What you say you need, you actually do not need at all.\n\nIt is, theoretically, possible to write a Krita colorspace plugin that sees the components of the raw data as a single pixel and that renders to an rgb pixel for use on screen; I'm not sure how useful or pointless that would be. But, apart from the sophisticated interpolation methods that make up so much of dcraw's source code and that are a major reason for opening raw images take quite a bit of time, it wouldn't be hard to do. It would be cool do -- but as I said, possibly quite pointless.\n\nStill -- the raw image is the negative. You don't work on the negative; you work on a representation of a negative. In real-life photography, that representation is caused by the rays of the lamp going through the negative: it's those rays that you modify with filters, and the chemical processes in the emulsion layer on the paper that you modify with nasty chemicals.\n\nIn digital photography, you work on a rendered version of the raw data, and apply your filters. You do not lose important possibilities that way, because you can create another initial exposure by converting it using other options a second time."
    author: "Boudewijn Rempt"
  - subject: "Re: Krita"
    date: 2006-10-18
    body: "Probably should have mentioned that I am a serious photographer.\n\nSo, yes, I do want the same digital capabilities that professionals have available in proprietary programs.\n\nI have also suffered through DSP in engineering college so I understand the limitations of interpolation -- interpolation can make aliasing worse.  I would find it interesting to find out if deconvolution would actually work better than interpolation -- theory indicates that it would but it might not be practical.  Obviously, deconvolution could only be done on a PC due to the amount of computation required.\n\nIt is also probable that using homomorphic deconvolution to remove noise would work better on the RAW camera data before the missing chroma data was restored by whatever method since this can only amplify the noise."
    author: "James Richard Tyrer"
  - subject: "Re: Krita"
    date: 2006-10-18
    body: "professional programs don't work different! only the algorithms used may be better(raw converting isn't easy, as you noted).\n\nmany professional photographers convert their raw images in a special application.\n\neven apples aperture, that does every other operation dynamically without destroying the image, converts raw-images on import and stores it in rgb format in its database (though it also keeps a copy of the original in raw).\n\n\nbut thats just how destructable image processing works. thats how photoshop works. non-destructable workflow just didn't land in the 2d world yet ;).\n\nmaybe thats a point that will be addressed in 2.0(hint :P)? they at leased should kick out the painting tools that work directly on the image, and use stroke paths instead. making filters only work as adjustment layers should be easy =).\n\n"
    author: "ac"
  - subject: "Re: Krita"
    date: 2006-10-19
    body: "In case this wasn't clear, after you import a raw image file, it is going to be in an RGB layer but will only have the CFA pixels which actually exist.\n\nExactly which operations would be appropriate on the CFA data are something which I would have to consider.\n\nObviously, operations on the tonal range and tonal curves (with defaults supplied by the DNG file) would be needed.  You need to do something about dead pixels.  Then there is noise reduction and the operations needed to convert to a full pixel image.  There are various ways to interpolate, and this requires an antialias filter (which I would like to see adjustable).  Deconvolution uses only an FFT filter to do everything -- an FFT filter can implement a perfect brick wall filter.  There is also the Moire issue.\n\nBut, there would be a point where you had produced a full pixel RGB image and would no longer work on the CFA image which could be discarded."
    author: "James Richard Tyrer"
  - subject: "Re: Krita"
    date: 2006-10-17
    body: "Dont forget Scribus, a QT based page layout app. It has had CMYK support for a few years and has most of the prepress abilities that designers would want. \n\nWe published a few pages of a news paper using it (it has very nice python automation abilities) and we never had any pdf problems.\n\ncheck www.scribus.net\n"
    author: "Kobus"
  - subject: "Re: Krita"
    date: 2006-10-17
    body: "How can you compare an app that can create CMYK images (by painting or converting to it) with an app that can simply handle it.\nThe point of the poster is very true, Krita is the first Free app that is able to do CMYK (in all sorts of bitdepths even)."
    author: "ac"
  - subject: "Re: Krita"
    date: 2006-10-18
    body: "In case you didn't know, Scribus is a professional desktop publishing application. It does all sorts of black magic related to colour profiles and the like."
    author: "Martin"
  - subject: "Re: Krita"
    date: 2006-10-18
    body: "Sure. It can handle CMYK. Which I said. But how you can honestly say that its in the same league is beyond me."
    author: "ac"
  - subject: "Re: Krita"
    date: 2006-10-24
    body: "They have completely different purposes, and are hard to compare. Scribus is a great program that I actually use WAY more than Krita, which I am just starting to play with a bit more, but have already lost work due to a crash while working with a manipulation layer, and seeing as most of my work is either for web or black and white, I continue to use GIMP to get the work done. Hopefully Krita will continue to improve and I will be able to make the switch. Thanks for all of your hard work."
    author: "Jonathan Dietrich"
  - subject: "Re: Krita"
    date: 2006-10-18
    body: "Since my screen is RGB and my printer driver requires RGB input, just exactly what is the supposed great advantage of so called CMYK images?\n\n"
    author: "James Richard Tyrer"
  - subject: "Re: Krita"
    date: 2006-10-18
    body: "For you none, so don't use it. You can remove the plugin, or, if you're compiling, skip installing the cmyk plugins. Whatever floats your boat,"
    author: "Boudewijn Rempt"
  - subject: "Re: Krita"
    date: 2006-10-18
    body: "Most higher end printers (including moderately good consumer ones) use CMYK to print.  If you were planning on having a professional printer print the image, then you would want to do it in CMYK."
    author: "Corbin"
  - subject: "Re: Krita"
    date: 2006-10-18
    body: "This is not true.  It is a common misconception, but it isn't the case.  Most color printers use CMYK dye ink to print with.  However, the drivers do not use a CMYK input.  If you supply CMYK data it is first converted into RGB data before printing.\n\nThe reason for this is that the CMYK printing process is non-linear and has some color failure (dyes are not perfect).  It is the printer driver that handles these issues when the driver converts from RGB to CMYK for a specific printer.\n\nA professional print shop probably has software that can manipulate the image directly in CMYK, but you should still provide them with a RGB image unless you have software which is designed to work with the specific printer being used."
    author: "James Richard Tyrer"
  - subject: "Re: Krita"
    date: 2006-10-18
    body: "Oh, man. I know you mean well, and think you are always right, but really, this makes absolutely no sense at all.\n\nPlease, talk to some experts before you talk in public.  People that have worked in the printing industry for many many years will tell you a quite different story.\n\nHint; ask about what a 'RIP' requires and outputs."
    author: "ac"
  - subject: "Re: Krita"
    date: 2006-10-18
    body: "Please talk to a real expert before you tell people that making their own CMYK separations rather than having the printer driver do it will somehow improve their output.\n\nIf you are saying that I am wrong about something, it would be appreciated if you explained what you think it is.\n\nWith Linux, the RIP we use for printing is usually GhostScript combined with GIMP Print (being replaced by Gutten Print).  This combination accepts an RGB image and produces a CMYK output which is specific to the printer being used.\n\nYes, as I said, commercial print shops may manipulate the CMYK image directly, but that isn't what I was talking about in my posting that started this thread.  What I asked was if CMYK was of any use to someone using their own printer connected to their PC and using the standard Linux printing RIP (GhostScript/GIMP Print).  Referr back to Corbin's posting which I was responding to.  This was not about commercial print shops it is about Linux printer drivers."
    author: "James Richard Tyrer"
  - subject: "JRT, Ghostscript, Gimp-Print and Gutenprint"
    date: 2006-10-19
    body: "You can't even *spell* correctly either one of Ghostscript, Gimp-Print or Gutenprint.  \n\n[And since your spelling in general is above avarage, this fact raises the strong suspicion that you *don't know* either of the 3 programs' working very well, which means your comments about this topic have to be taken with a spoonful of salt...]"
    author: "AC"
  - subject: "Re: JRT, Ghostscript, Gimp-Print and Gutenprint"
    date: 2006-10-19
    body: "I have: gutenprint-5.0.0-rc3 installed on my system.  \n\nI am a terrible speller and rely on the spell checker which corrected the misspelling of 'gutten'. :-D"
    author: "James Richard Tyrer"
  - subject: "JRT and his CMYK scepticism"
    date: 2006-10-19
    body: "<i>\"This combination accepts an RGB image and produces a CMYK output which is specific to the printer being used.\"</i>\n\nAgain, not true in that total generalization you are using.\n\nGutenprint (formerly Gimp-Print) may be the most important printer driver family. But for HP models, there is HPLIP.\n\nAlso, if your printer is PostScript-enabled, your Linux printing will bypass Ghostscript entirely.\n\nWhatever your verdict about CMYK for Linux imaging and printing is, JRT: I'm glad the Krita developers implemented support for CMYK (and continue to enhance it), and I'm glad you have no say in the process. (Please don't hate me for having said that.)"
    author: "AC"
  - subject: "Re: JRT and his CMYK scepticism"
    date: 2006-10-19
    body: "Notice the qualifying word: \"usually\"?\n\nObviously, if you are using a GhostScript \"device\" (regular or IJS), you don't use GutenPrint and if you have a PostScript printer, you don't run a RIP on your computer.\n\nThis is not my verdict about CMYK, it is fact that you don't need it to make prints on the color printer attached to your PC.  The reason is that the printer drivers need RBG input. \n\nThe ability of Krita to handle other image formats is probably useful for some purposes -- you might need to import an existing image in such a format -- but it doesn't have any purpose in personal color printing.  "
    author: "James Richard Tyrer"
  - subject: "Re: JRT and his CMYK scepticism"
    date: 2006-10-19
    body: "Er... From the first moment you started I already said, no, cmyk is not useful for _you_. Just forget about it. We didn't code that feature for you, and if I could code it so that it wouldn't show up on your computer, I would do that. It's not on topic for you. You don't need the feature -- that's fine! I don't need it either. I haven't got a colour printer, even.\n\nAnd I really hate to say it -- it's a dreadful thing to do -- but, despite the fact that I do believe you mean well and want the best, I think you act in a way that is actively detrimental to the progress of free software. Indirectly, actually, you're the reason I am the maintainer of Krita: you, in a very real sense, have driven away the previous maintainer by tiring him out. Us Krita maintainers, we may be wrong, proud, stubborn, irrascible, foolish and uninformed. But we're doing a job we're not paid for and I believe we are furthering the progress of free software. The environment we work in is touchy-feely, delicate and complex. Have a care.\n\nPlease. Can it. Stop it. You can make yourself useful to the cause of free software in another capacity, perhaps, once you learn how a free software community works. I'd like to help you with that, but you really need to learn to go with the flow. For instance, with the mindset you are displaying, you would be a wonderful regression tester, something that we are missing sorely. The person who notes that y.y doesn't do what x.x did, but in fact crashes on it -- we really need that. Miereneuken, as we say in Dutch. (Strictly untranslatable!) We don't need someone persistently insisting that feature A is not useful and computationally impossible feature B is necessary and insists on belaboring the obvious (as long as they are not regressions!). \n\nGnothi seauton -- know yourself."
    author: "Boudewijn"
  - subject: "Re: JRT and his CMYK scepticism"
    date: 2006-10-19
    body: "Oops -- intermittent wifi network problems plaguing me..."
    author: "Boudewijn"
  - subject: "Re: JRT and his CMYK scepticism"
    date: 2006-10-21
    body: "Skepticism is a useful function.  You get that with an engineer whether you want it or not.  Cheerleading does not result in a better product.\n\nThe problem is not my skepticism but rather some ACs that are more determined to prove that I am wrong about something than to engage in a useful discussion of the product.\n\nWe still have ACs that are determined that they need CMYK for purposes which, as you said in a rather sarcastic way, it is totally unneeded.  They also appear to think that just having CMYK will provide the features which I would really like to see.\n\nIt is not my fault that ACs are not interested in a useful discussion.  It has previously been suggested that I ignore them and hope that they go away.  Perhaps this is the best idea, but it is not my nature to do so."
    author: "James Richard Tyrer"
  - subject: "Re: JRT and his CMYK scepticism"
    date: 2006-10-19
    body: "Er... From the first moment you started I already said, no, cmyk is not useful for _you_. Just forget about it. We didn't code that feature for you, and if I could code it so that it wouldn't show up on your computer, I would do that. It's not on topic for you. You don't need the feature -- that's fine! I don't need it either. I haven't got a colour printer, even.\n\nAnd I really hate to say it -- it's a dreadful thing to do -- but, despite the fact that I do believe you mean well and want the best, I think you act in a way that is actively detrimental to the progress of free software. Indirectly, actually, you're the reason I am the maintainer of Krita: you, in a very real sense, have driven away the previous maintainer by tiring him out. Us Krita maintainers, we may be wrong, proud, stubborn, irrascible, foolish and uninformed. But we're doing a job we're not paid for and I believe we are furthering the progress of free software. The environment we work in is touchy-feely, delicate and complex. Have a care.\n\nPlease. Can it. Stop it. You can make yourself useful to the cause of free software in another capacity, perhaps, once you learn how a free software community works. I'd like to help you with that, but you really need to learn to go with the flow. For instance, with the mindset you are displaying, you would be a wonderful regression tester, something that we are missing sorely. The person who notes that y.y doesn't do what x.x did, but in fact crashes on it -- we really need that. Miereneuken, as we say in Dutch. (Strictly untranslatable!) We don't need someone persistently insisting that feature A is not useful and computationally impossible feature B is necessary and insists on belaboring the obvious (as long as they are not regressions!). \n\nGnothi seauton -- know yourself."
    author: "Boudewijn"
  - subject: "Re: Krita"
    date: 2006-10-18
    body: "> A professional print shop probably has software that can manipulate the image directly in CMYK, but you should still provide them with a RGB image unless you have software which is designed to work with the specific printer being used.\n  \nI hate to tell you, but you are soooooo wrong. I have submitted magazine ads for my business to around half a dozen publications and I can tell you in no uncertain terms they *REQUIRE* CMYK images. Manipulating RGB to CMYK is not the issue. Color matching the conversion is. As such professional printers have had experience with RGB to CMYK producing inconsistent results and customer issues. It's all about color matching which is why there is color matching software like lcms. In fact you can grab a few inkjet printers and print the same photo and notice differences. While the RGB in your image is specific, the conversion to CMYK can be somewhat arbitrary based on drivers. The professional printers use CMYK as that is how the ink goes on the paper. There is no probably about it. They will require CMYK. It's apples in, apples out, not oranges in, apples out. If a professional printer doesn't require CMYK that would throw up a #FF0000 flag for me. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: Krita"
    date: 2006-10-18
    body: "Yes, color matching is the issue but is no need for CMYK to do color matching.\n\nYes, there are shops that won't do the RGB to CMYK conversion for you.  And in that case, I stand by what I said that you must have software support for the print process that they are using.\n\nIf you don't adjust the CMYK separations to match their printing process, making them yourself simply won't result in any better output than using RGB.\n\nYes, different inkjet printers will produce different results.  It would be very helpful if we had a way to preview these results on the screen.  Since inks and papers differ (and with them the color gamut) it is probably not possible to get perfect matches from printer to printer.  So, what we need is software that will give an accurate preview for every printer. \n\nThe objective of printing is to produce an RGB image.  The best way to do this is to match a supplied RGB image.  IIUC, the first problem here is that this supplied image must use color matching methods to conform it to the gamut of the printer.  The second issue is desaturation of the image due to what in photo printing is called color failure (the fact that the dyes aren't perfect CMY -- specifically that the curves overlap slightly).\n\nI would say exactly the opposite about a professional printer.  If they tell you to make the separations, they are saying that they will not do the job of matching the output to what you want."
    author: "James Richard Tyrer"
  - subject: "Re: Krita"
    date: 2006-10-18
    body: "Your are basically right, some things a bit misleading. The facts:\n\n1) Printing on your home-printer is done via RGB. No need for CMYK here.\n\n2) The gamut of RGB is larger than CMYK. CMYK editing is necessary in professional production in order to prevent you from using colors during photo editing which cannot be printed later. This is not an issue at home, because you usually dont insert graphics or logos which must match a specific color but are simply retouching your photos. In the latter case, the color matching is better done later on to prevent color information loss.\n\n3) The \"K\" (Black) in CMYK contains valuable information for printing which just isnt available in RGB. Negation of RGB would simply result in CMY. How much gray component is removed or added is represented by the K. Manipulation of this is done in professional printing and therefore CMYK editing is required though I agree this is a bit overkill with your average home-printer.\n\n4) Simply doing RGB to CMYK conversion yourself will not yield any quality improvement. The opposite is true.\n\n5) The corporate identity of many companies require that images, graphics etc match a very specific CMYK color. You can only guarantee that everything fits together nicely if you use CMYK images throughout in a document.\n\nSo to sum it all up: CMYK is absolutely required in professional print production for people who know what they are doing. If you just want to\nretouch you holiday photos and give them to a print-office afterwards you shouldnt use this because you must know a lot about the production process, paper, etc to do it right. So it's better here to leave these issues to the print-office.\n\n\n\n\n"
    author: "Michael"
  - subject: "Re: Krita"
    date: 2006-10-19
    body: "Good, an expert.  My area of expertise is photographic color printing NOT 4 color ink printing.  And it is a real bitch to match colors there also.\n\nRe: #2: now we have established that I will be using RGB to drive my color printer, it does appear that a useful feature would be if there was some way to tell what was out of gamut for printing on my inkjet or a color laser printer at Kinko's.  And, as I said, a color print preview would be very helpful.  I know where out of gamut lies: too bright or too saturated, but don't know exactly where the line is."
    author: "James Richard Tyrer"
  - subject: "Re: Krita"
    date: 2006-10-19
    body: "Yes, that would be a useful feature in many apps indeed. In the mean time I can give you this hint in case you didnt think of it before:\nConvert to CMYK and back to RGB. Subtract both images by layer filter. You get a map of problematic areas. This is where CMYK is helpful even in your case. Isnt it nice? We've now closed the circle of the discussion here ;-)\n"
    author: "Michael"
  - subject: "Bingo! (I hope JRT sees a \"CMYK light\" now)... :-)"
    date: 2006-10-19
    body: "Bingo!"
    author: "AC"
  - subject: "Re: Krita"
    date: 2006-10-19
    body: "I don't think that that is going to make any difference since there is a direct correspondence between RGB and CMYK (for a given level of black removal).  Changing from one to the other isn't going to indicate the reduced gamut of the printer which is a function of ink on paper vs. phosphors on the screen, not RGB vs. CMYK."
    author: "James Richard Tyrer"
  - subject: "Re: Krita"
    date: 2006-10-20
    body: "There isnt a direct correspondence if you use color profiles which you should. Without them RGB to CMYK conversion is not possible in a meaningful way."
    author: "Michael"
  - subject: "Re: Krita"
    date: 2006-10-22
    body: "This (color profiles) is something that I need a better understanding of.  I am the first to admit that I do not fully understand it.  However, I think that I know what I would like the software to do.\n\nYou can make an approximate (linear) screen preview of what a color print will look like with the matrix operation:\n\n  Image(preview) = [M]*Image(file) + [B]\n\nwhere [M] is a 3x3 matrix and [B] is a 3 element vector.  This function is applied (pixel by pixel) to the RGB vector for each pixel.\n\n[M] makes the transform for the smaller gamut and color failure and [B] sets the black level.\n\nI have not found any information as to how to produce these two paramaters for a given screen gamut (e.g. sRGB) and a color profile.\n\nLinear is just a first approximation.  A gamma corrected approximation (e.g. Gamma != 1) would be better.  And, the best approximation is going to be non-analytic (curves not a linear or exponential function)."
    author: "James Richard Tyrer"
  - subject: "JRT misundersts of parent postings (always!)"
    date: 2006-10-19
    body: "\"Yes, there are shops that won't do the RGB to CMYK conversion for you.\"\n\nNo, James Richard, that was not the crux of the question. \n\nThe point is: even those shops which are able to do the CMYK-->RGB (and offer it as a service) -- them don't like it. Not at all. Because the results is less than stellar. And because the shops then have to deal with customers who complain and blame the shop's personell, equipment, expertise, customer care, knowhow for the bad quality and demand their money back...."
    author: "AC"
  - subject: "JR needs to understand concept of color management"
    date: 2006-10-19
    body: "\"So, what we need is software that will give an accurate preview for every printer.\" \n\n\nJames Richard: that software's function is called \"Color Management\", using color profiles. \n\nTo be more precise, it means that your monitor needs to be supported by that color management as well. \n\nWhy? Because you need to get an RGB-based monitor [even different models/brands of RGB monitors!] to display the same color impression to the human eye as a CMYK-based printer [even different models/brands of CMYK and CcMmYyKk printers!]\n\nAll devices that \"display\" an image (RGB on screen, CMYK on printed media) need to be *adjusted* first. And need adjustment individually even, one for one. Even if you have 10 monitors of the same brand and model, they may have differences in their display, due to factory deviations, ya know?\n\n(And Color Management software can never work perfectly anyway. Because some real life color shades may be *impossible* to display on an RGB screen. Which however may display just fine on a glossy paper printed with a 7 color inkjet. That's what is called the different \"gamut\" of the respective color spaces, James Richard. It's like the speed for driving your car: you \"adjust\" it with pressing the accelerator pedal. But you can never adjust it for going 300 mph, to match a Formula 1 racer, can you? However, you can adjust it to go slow, and with the same speed behind a bicyclist, and make both appear to go at the very same pace.)\n"
    author: "AC"
  - subject: "Re: JR needs to understand concept of color management"
    date: 2006-10-19
    body: "Yes, I (basically) know how professional calibrated pre-press color management works.  And, I know that it can't work perfectly because ink and phosphors are not perfect.\n\nHowever, just because we don't have this in our standard software doesn't mean that we can't have anything.  If we know that gamut that a printer can represent and apply this to a screen gamut (e.g. sRGB), we can display an approximation of what the printed page will look like.  Naturally, if your monitor isn't adjusted correctly, this is going to vary, but it should give some idea of the reduction in the gamut when printing."
    author: "James Richard Tyrer"
  - subject: "JR, color gamut is not just about \"inks and paper\""
    date: 2006-10-19
    body: "\"Since inks and papers differ (and with them the color gamut) [...]\"\n\nJames Richard, I see you have some idea about color gamut already. That's an excellent level educate yourself a bit more beyond these basics. I suggest you start up Konqueror and type \"wp:gamut\", \"wp:RGB\", \"wp:CMYK\" and \"wp:color management\" into the address field and do a bit of read up.\n\nIt will teach you that even one RGB monitor differs to the next one. And one CMYK printer model to its neighbour. And a Scanner to the next.\n\nAll of these need to be adjusted with a color management software to match each other as closely as possible, if you do some sort of professional printing. (Most home PC users may not need it -- serious amateur photographers however do long for it!)\n\nThat's why CMYK support in Krita is absolutely essential. And I tell you one more thing: CMYK support in Krita would be absolutely useless if it wasn't accompanied with color management support (which is, I believe, in this case supplied by the LCMS library).\n\nPlease stop bemoaning CMYK support in Krita just because you own an RGB printer  (or a CMYK printer, whose crappy driver uses RGB as input.)"
    author: "AC"
  - subject: "Re: JR, color gamut is not just about \"inks and paper\""
    date: 2006-10-19
    body: "The color gamut of a printed page is determined by the ink and the paper.\n\nWhat this means is that the range of colors that can be produced is determined by the ink an paper.\n\nThe color gamut of you monitor can be interpreted in two ways:\n\n1.  The actual colors which are being displayed.\n\n2.  The range of off monitor colors which FF0000, 00FF00, 0000FF represents.\n\nIIUC, it is #2 that color management software addresses.  This works just the same with RGB since the color gamut is always an RGB function -- in the ideal case it is a triangle between a Red, a Green, and a Blue point.\n\n> Please stop bemoaning CMYK support in Krita just because you own an RGB \n> printer (or a CMYK printer, whose crappy driver uses RGB as input.)\n\nI am not bemoaning anything.  I did seek to point out that you don't need CMYK to print on the printer attached to your PC.  If the driver accepting RGB input is \"crappy\" then all of our Linux drivers based on GhostScript are \"crappy\".\n\nI am amazed at this superstition about CMYK.  \"The truth is out there.\"  You use the \"crappy\" driver to convert a linear RGB image to a (printer specific) non-linear CMYK image which I hope is compensated for the color failure of the inks.  The reason that you use the \"crappy\" driver to do this is that it has the needed information specific to the printer which is needed to do this.  If you made your own CMYK separations in Krita and were able to directly print them on your printer, you would NOT be happy with the results -- the \"crappy\" driver (usually GutenPrint) does much better."
    author: "James Richard Tyrer"
  - subject: "Re: Krita"
    date: 2006-10-19
    body: "\"The objective of printing is to produce an RGB image.\" \n\nNo, it's not.\n\nThe objective of (quality) printing is to produce an image on paper that matches as closely as possible the intentions of the artist, the photographer, the document writer or the user. (And this intention may well be \"the printout and all its color shades must exactly match the picture I can see and adjust on screen\".)\n\nThe colors used could be based RGB, sRGB, CMYK, CcMmYyK and many more spaces...\n\nJames Richard, the world does not evolve around your RGB fixpoint only. Like it or not."
    author: "AC"
  - subject: "Re: Krita"
    date: 2006-10-19
    body: "Well, my eyes see RGB.  Therefore, ... ."
    author: "James Richard Tyrer"
  - subject: "Re: Krita"
    date: 2006-10-20
    body: "\"Well, my eyes see RGB.\"\n\nTrue. Mine as well.\n\nBut the \"world\" of digital imaging and printing does not only consist of our eyes, but also of paper, inks, toners, monitors, rays + tubes and more, which all influence the outcome.\n\nIn the end, there is again light reflected from a printed image which enters our eyes and is detected by our RGB-only light detectors.... OK, in this sense, the world *does* indeed orbit around RGB, and I must retract -- no, rather modify -- my above statement:\n\n# The world orbits around RGB, but there are other\n# components to this world but just that RGB axle."
    author: "AC"
  - subject: "Re: Krita"
    date: 2006-10-20
    body: "Not really.   Your eyes see RGB+gray scale.   Cones and Rods and all that.  I'm color blind, so I don't really see RGB, more like R1R2B, where R2 is somewhere between red and green - a very different color from red. There are many different types of color blindness (hundreds at least), but only 10% of the population have enough to matter.   Even those who are not color blind have some shifting from \"normal\".    So you think you see RGB because you are not color blind, but the colors are see are unlikely to be \"a scientific standard\" (AFAIK no such thing exists) RGB.\n\nYour perifferial vision is mostly black and white, your vision in front of you is color.   And there is also that blind spot.  Depending on how you look at an image you get different combinations.  Your brain is smart enough to fill things in so that you don't realize this is happening, but it does. \n\nOdds are you knew the above already, but never considered it.  You should.\n\nCMYK is the best chance we have to cover all that complexity, which is why professionals use it.   It still doesn't cover people like me very well, but it is pretty close for 90% of the people.   When colors are really important professionals will buy an ink in that color, but each ink requires a separate pass through the press (some presses can do more than one color in a pass), and each pass will be a little misalligned from the previous, so this is rarely done anymore as CMYK is almost always close enough and there is no missalignment problem.\n\nA few years back my brother was assigned to calibrate a $60,000 CMYK printer.  To some Pantone (tm) color - the closest that printer could do was visually different from the standard.  A $100 inkjet was able to get it perfect (at least as far as anyone could visually see, color blind or not).  So lets not pretend that CMYK is perfect, but it is what professionals use.    \n\n(P.S.   Anyone know when Microsoft is going to update their stupid browser to get spell check - something Konqueor has had for year? I wish I was allowed to use something better here)"
    author: "bluGill"
  - subject: "Re: Krita"
    date: 2006-10-21
    body: "> ... the closest that printer could do was visually different from the \n> standard. A $100 inkjet was able to get it perfect ...\n\nThis is an inherent problem with CMY or CMYK printing.  In CMY photo printing it is called color failure.  I presume that with ink that this varies from ink set to ink set.  The problem is that the secondary color dyes aren't perfect.  The Magenta ink absorbs some Green and the Cyan ink absorbs some Yellow.  The Yellow ink is usually not a problem.  So, what happens is that when you combine two of the CMY inks, you get a little Black.  With photo printing you have to make masks to compensate for this.  With CMYK it is simpler.  A printer driver that compensates for color failure subtracts this extra Black from the K channel.  However, this still means that there is a maximum for saturation and brightness for the primary colors -- the reduced gamut.  With ink printing using more colors helps eliminate this problem."
    author: "James Richard Tyrer"
  - subject: "JRT and his RGB/CMYK understanding"
    date: 2006-10-19
    body: "Most printers (even cheap inkjets) have their inks and toners in CMYK.\n\nYes, your screen is RGB.\nAnd yes, *some* printer drivers use (and require) RGB input.\n\nBut *no* -- don't think that means \"there is RGB in my computer all over the place\".\n\nWhat format your computer uses internally is something else (\"CoreGraphics Engine\" [CGE] for Mac OS X, \"Graphic Device Input\" [GDI] for Windows, and PostScript for Unix/Linux) as originating format that goes to the printer driver..."
    author: "AC"
  - subject: "Re: Krita"
    date: 2006-10-19
    body: "I concur on the great progress on krita. Last time I tried it (I believe it was 1.5), it was pretty frustating (and I made a negative comment here), but it has obviously made *a lot* of progress. Congratulations and thanks, here's to hoping I can forget about Gimp soon.\n"
    author: "Guillaume Laurent"
  - subject: "To report a bug or file a wish"
    date: 2006-10-17
    body: "Please use http://bugs.kde.org rather than commenting here, so your report will be noticed. Thanks."
    author: "Jaroslaw Staniek"
  - subject: "Vote!"
    date: 2006-10-17
    body: "I want to encourage everyone to vote for features KOffice lacks of. If the missing feature is not yet filed in bugs.kde.org, don't hesitate to do this yourself. Developers are busy with coding, you know. ;-)\nMost developers have a look at these wishes and the number of votes to see where KOffice or any other KDE app needs improvement. Using bugs.kde.org ensures, that your wishes don't get lost due to the fading memories of the species called software developer.\nThe same applies for bugs.\n\nSome notes about the voting system: You can assign up to 20 voting points to a wish and up to 100 points per product, e.g. KSpread. So you can give 20 points to each of the five most important missing features of KSpread, for example. If you consider more features as important, distribute the voting points among them.\n\nHappy voting!"
    author: "Stefan Nikolaus"
  - subject: "Re: Vote!"
    date: 2006-10-17
    body: "I would rather they work on fixing all the bugs that make KWord, KSpread, and KPresenter unusable. Sadly, if you want to do some actual work rather than just play around with a cool app, OOO is still the only choice on Linux. We honestly tried to stick with KWord at least, but in the end it is just plain not feasible. Import/export from/to MS or at least OOO formats is one showstopper for example. I will try out Krita next time I need to edit an image though."
    author: "Former KOffice user"
  - subject: "Re: Vote!"
    date: 2006-10-17
    body: "Bugs are fixed by developers, but KOffice lacks of enough manpower. Hence, we need to prioritize and that are votes for (even though developers are aware of the main issues)."
    author: "Stefan Nikolaus"
  - subject: "Re: Vote!"
    date: 2006-10-17
    body: "many of the bugs you experience are due to limitations in Qt 3.x, and will be fixed in the 2.0 release. And don't count on better MS Whatever importfilters, they don't want to waste time on that. OO.o support isn't even needed, as OO.o and Kword use the same fileformat. both do still have some bugs in there, but i guess that's something which will be fixed as time passes."
    author: "superstoned"
  - subject: "Nice picture!"
    date: 2006-10-17
    body: "Yayy!! The picture on KOffice website and Cyrille's blog is really nice, looks clean and professional :)"
    author: "ac"
  - subject: "Thanks."
    date: 2006-10-17
    body: "First of all, thanks a lot for your work on KOffice.\n\nI download each release and try to use KOffice for daily use at work.\nThe suite improves with every release and this is really impressive!\n\nBut, at end, I still go sadly back to OO.o with all its defects...\n\nWhat I lack more are good doc and xls export filters.\nIt's really sad, but I often need them.\n\nIt would be a dream if we could split OO.o import/export filters in a library and include them in KOffice.\n\nAs I am not a developer, I don't know if it would be theoretically feasible.\nI can ...pray and propose a bounty if someone is interested...\n\nThanks once more. Emanuele."
    author: "Emanuele Gissi"
  - subject: "Re: Thanks."
    date: 2006-10-17
    body: "http://ariya.blogspot.com/2006/04/why-koffice-not-using-openofficeorgs.html "
    author: "Anonymous"
  - subject: "Re: Thanks."
    date: 2006-10-17
    body: "For Word you have the RTF filter.\nNothing special with it, just try to save in Word 2003 in 2000 format and you also only get RTF. If you prefer another ending, than do like Word and change .RTF to .DOC.\n\nFor Excel it's missing, known issue."
    author: "Philipp"
  - subject: "Re: Thanks."
    date: 2006-10-17
    body: "The files you want to import are usually from *other* people. And to tell a customer to send you a standardized format (doc/xls/ppt is a standard for him and he knows no other formats anyway) or how he could transform it into something you can import appears to be... not really the best way."
    author: "Ascay"
  - subject: "Re: Thanks."
    date: 2006-10-17
    body: "You should anyway.   If enoguh customers scream at Microsoft that their partners are having trouble opening documents, maybe Microsoft will change to a good format (opendoc...) that everyone can open.\n\nThere are enough non-Microsoft office products in use that if every user of them made sure to point out the problems they have opening Microsoft documents to partners, the partners would also scream.   Loud enough to get Microsoft's attention.\n\nYes I know not everyone can do this.   I specified partners for a reason.   You can't inconvience custmers.   You can inconvience partners a little, and suppliers had better fit to your needs or you will find a new supplier.   (You can even write this into contracts and bid requirements in some cases)\n\nMicorsoft's .doc format is a mess.   There is good reason that they use .rtf to save to formats for older versions of word - they cannot understand the format themselves.    "
    author: "bluGill"
  - subject: "the announce has been translated by a robot ?"
    date: 2006-10-17
    body: "I clicked on the \"announce\" link, then went to the French translation... it is full of mistakes (one per line, and I am not talking about typos here). I think it is far better not to put online any translation than to put a very bad one. I guess it was done by some automatic translation program (google trans, babelfish, etc.)."
    author: "oliv"
  - subject: "Re: the announce has been translated by a robot ?"
    date: 2006-10-17
    body: "I'm the coordinator of annoucement translations and I'm sorry the translation is low quality. I sent the annoucement to be translated to a person, if that person used an automatic translator, that is bad. I will check with him."
    author: "Albert Astals Cid"
  - subject: "Re: the announce has been translated by a robot ?"
    date: 2006-10-20
    body: "I don't know if the page has been updated but now the translation is very good for me."
    author: "turman"
  - subject: "Command-line scripting"
    date: 2006-10-17
    body: "> With this release, KOffice also introduces command-line scripting where, for\n> example, spreadsheet documents can be automatically manipulated with scripts\n\nI tried to find some documentation on this feature and how to use it but didn't find anything on the koffice website.\n\nDoes anyone know where I could find some documentation on how to use this feature ?"
    author: "Anonymous"
  - subject: "Re: Command-line scripting"
    date: 2006-10-17
    body: "For Krita, see the manual: http://docs.kde.org/development/en/koffice/krita/scripting.html."
    author: "Boudewijn Rempt"
  - subject: "Re: Command-line scripting"
    date: 2006-10-18
    body: "A small example for KSpread is up at http://kross.dipe.org/KSpreadInvoiceExample.tar.gz\n\nDo we have doxygen API references for 1.6 up somewhere? They are nice to look at the functions + examples for scripting too."
    author: "Sebastian Sauer"
  - subject: "Re: Command-line scripting"
    date: 2006-10-18
    body: "and some more examples;\nhttp://www.kde-files.org/index.php?xcontentmode=617\nhttp://www.kde-files.org/index.php?xcontentmode=618\n"
    author: "Sebastian Sauer"
  - subject: "Re: Command-line scripting"
    date: 2006-10-18
    body: "and for Kexi, you may also like to look at;\nhttp://www.kexi-project.org/wiki/wikiview/index.php?ScriptingHandbook"
    author: "Sebastian Sauer"
  - subject: "Dead in the water"
    date: 2006-10-17
    body: "It's good to see KOffice being upgraded from time to time, but I get the impression that the 1.x branch is officially dead in the water. There are issues that won't get fixed until the 2.x branch, such as support for OpenDoc/Word etc... Without them KOffice is unusable by anyone that needs to inter-operate with people who use MSOffice or OpenOffice. This is the reason I gave up on KOffice recently and moved to OpenOffice."
    author: "Mike"
  - subject: "Re: Dead in the water"
    date: 2006-10-17
    body: "Sorry, but what are you talking about? MS Word support won't ever get better, they don't want to waste time on proprietary sh*t. And they shouldn't. \n\nOpenDocument has been supported since quite some time, it's the default fileformat, so that's not a 2.0 thing as well. It's true 2.0 will fix some longstanding issues, but those are mostly layout issues in Kword, and have nothing to do with fileformat support."
    author: "superstoned"
  - subject: "Re: Dead in the water"
    date: 2006-10-17
    body: "KOffice was one of the first office suites to have native support for OpenDocument, also in a lot of areas KOffice is closer to the ODF standard than OOo, though both suites are still working on it.\n\nAnd 1.6 release and 2.0 release were being developed at the same time, now that 1.6 is out the door, unless theres plans for a 1.6.1 release, all the effort is going to go to 2.0."
    author: "Corbin"
  - subject: "Re: Dead in the water"
    date: 2006-10-18
    body: "There will be a 1.6.1 release and possibly also a 1.6.2. Those releases will only have bugfixes and no new features.  However, errors in OpenDocument handling (a.k.a the OOo) are considered bugs so incompatibilities with OOo should be reported.  It's very likely they will be fixed in the 1.6 series.\n\nRegarding XLS and DOC support, it's not exactly that we \"don't want to waste time with that shit\", it's more like \"we have nobody who has prioritized it high enough\".  We would love better XLS and DOC filters, but at this time we have nobody who works on them."
    author: "Inge Wallin"
  - subject: "KOffice for Windows"
    date: 2006-10-17
    body: "So when is KOffice for Windows coming out?"
    author: "Alex"
  - subject: "Re: KOffice for Windows"
    date: 2006-10-17
    body: "With version 2.0, probably at the same time as the Linux and OS X versions. Although that will, as always, depend on people using those operating systems helping out.\n\nThe most recent Windows I own is 3.11, and the most reason Windows I have access to through my workplace is Windows 2000..."
    author: "Boudewijn Rempt"
  - subject: "Re: KOffice for Windows"
    date: 2006-10-18
    body: "If you want to have a look for yourself without a newer Windows, you could still test with ReactOS (www.reactos.org), it should probably be far enough to run the KOffice2 suite, as it already handles OpenOffice for example."
    author: "Anonymous"
  - subject: "Re: KOffice for Windows"
    date: 2006-10-18
    body: "I guess what Boudewijn was trying to say is that none of us developers really care enough for windows to go the extra mile to install another OS. Choosing between spending our time on improving KOffice prober or doing auxillary testing we choose KOffice any time.\n\nBut given a concrete bug report on how to make it work on windows (patch even more appreciated) I suspect we would spend some time on it. And as Boudewijn said: With 2.0/QT4/KDE4 we will work on windows almost out of the box, so chances are great that it will happen then."
    author: "Casper Boemann"
  - subject: "Re: KOffice for Windows"
    date: 2006-10-18
    body: "Now, Casper, that's a very callous attitude.  I know some developers care very much that KOffice will work on Windows.  I am fairly sure that Jaroslaw Staniek, for instance will make sure that it works well when the time comes.\n\nI myself care also, but unfortunately I have never developed on Windows so I wouldn't have any clue about how to do it."
    author: "Inge Wallin"
  - subject: "Re: KOffice for Windows"
    date: 2006-10-18
    body: "In fact it works already since some time on MacOS and there are already some older packages around. See\nhttp://ranger.users.finkproject.org/kde/index.php/Home\nhttp://dot.kde.org/1073009304/\nhttp://www.racoonfink.com/archives/000700.html\n"
    author: "Sebastian Sauer"
  - subject: "Do not port to windows!"
    date: 2006-10-18
    body: "http://www.fefe.de/nowindows/"
    author: "MSLinux"
  - subject: "Re: Do not port to windows!"
    date: 2006-10-19
    body: "that ship has sailed\nwindows ports will happen sooner or later \nthe trick will be to balance the risks and rewards\npromoting standards will be one of the biggest rewards\n\nporting KOffice and getting more people using OpenDocument will be good for everyone.  wouldn't you love it if you didn't have to tell people not to send Microsoft word documents?   \n\ni look forward to a khtml/webkit browser on windows so that web developers will have one less excuse for not testing their web pages and following standards.  I would strongly favour not porting Konqueror to windows because of the risks of it not being as good as on KDE and giving people the wrong idea.  having a standalone khtml/webkit browser instead would help promote standards without risking any of KDEs reputation.  unfortunately the swift web browser project seems to have stalled due to financial difficulties.  \n\napplications which help promote standards should definately be at the front of the queue when it comes to windows ports"
    author: "Alan Horkan"
  - subject: "Re: KOffice for Windows"
    date: 2006-10-18
    body: "Some people care, some do not.\n\nLike Boudewijn Rempt, the only version of Microsoft Windows I have legal access to is 3.1 (if the disks are still good and I can find a working floppy drive...)   I am forced to use Windows XP at work, and I hate it.   I personally am not going to put any effort into getting Koffice working on Microsoft Windows - in the end I would have something I have no use for.\n\nI will allow others to do that work if they want.    I even wish them luck.   But I'm not interested in helping.   Many other Koffice developers are the same.  (note, though it appears I'm lumping myself in with Koffice developers, I have not actually submitted a patch, only played with the source a little)   The question is will those who do care about Microsoft Windows care enough to put the work in to make everything work?    It appears there won't be much work, and in that case they will do it.   However if it turns out there are major work involved those are care may not care enough to do the work."
    author: "bluGill"
  - subject: "Re: KOffice for Windows"
    date: 2006-10-18
    body: "I personally don't care to spend any time developing Quanta for Windows. In the past it would have meant thousands in software licenses including a copy of the MS compiler and months of dedicated developer effort in porting. None of our team likes Windows and everyone especially dislikes developing on it. \n\nHaving said that it doesn't matter. It's like OS X or BSD... Once somebody is building on it they let us know where our code doesn't build and specific guidelines to make sure the code is cross platform. This usually amounts to very little effort on our part. So we focus on what we wanted to do in the first place, making our application as good as we can, and others make sure we are building on other platforms. In KDE 4 Windows is just another platform. \n\nIn my opinion having our applications on Windows is a good start for migrating people to free software... The Qt4/KDE4 scenario is the all the difference in our support for Windows. "
    author: "Eric Laffoon"
  - subject: "Re: KOffice for Windows"
    date: 2006-10-24
    body: "Well said."
    author: "Well"
  - subject: "Krita - Marketing vs. Reality"
    date: 2006-10-18
    body: "\"Krita Becomes Usable for Professional Image Work\n\n[...]With features such as magnetic selection, effect layers, colour model independence and full scriptability, it has risen to become what is probably the best free image editing program today.\"\n\nKrita looks nice on paper (nice architecture, nice concepts,...) but let's have a closer look:\n\nBest free image editing program?\nDoes it have 16bit? - Yes\nDoes at least the most basic filters (like unsharp mask, Gaussian blur,..) work with 16 bit? - No\n\nThat's a joke, isn't it? I just tested the raw-import feature (it works basically) but then it's impossible to sharpen the image because \"unsharp mask\"  (the only adjustable sharpen filter in Krita) only supports 8 bit/color.\n\nWhy is Gimp still extremely popular despite the lack of >8bit/color, color management, CMY,...? It's *not* because of the cool features of Qt4 (gimp uses older versions of gtk), it's also *not* because of it's intuitive ;-) GUI but it's *because* _the_basics_just_work_ and it has very *many* filters and scripts.\n\nKrita lacks filters, plugins, intuitive color adjustment tools,...\n\nThat's the main disadvantage of Krita at least from a point of view of an end user. All those rewrites and new Qt4 and KDE4 features _won't__make__Krita__any__better if color adjustments, performance and all the other filter plugins are worse than the corresponding plugins from Gimp or Digikam.\n\nAlways saying \"wait for the next version\" does not work. Who will start writing plugins and scripts for Krita if Krita 1.6 will be a short living bugfix only branch (as krita 1.5 was) and Krita 2.0 will probably be incompatible again with existing Krita 1.6 plugins?\n\nAfter Krita 1.5 was released (professional photo manipulation software, now with scripting support...) I tried to write a script to do \"unsharp mask\" sharpening. But argh - Krita had no funktion to do gaussian blur (that's a basic and needed for nearly everything...) - so no script.\n\nShould I write a vignetting and distortion correction plugin for Krita 1.6? I don't think that's a good idea because Krita 1.6 won't see any significant updates anymore (only some bugfixes, if I remember correctly) and Krita 2.0 is too far away (I won't install KDE 4 before KDE 4.0.2 or so).\n\nGimp does not have those strong dependencies to special KDE or Gnome versions....\n\nWhy not remove the feature freeze from Krita 1.6 and have a nice and long living Krita 1.6, 1.7 branch?"
    author: "max"
  - subject: "Re: Krita - Marketing vs. Reality"
    date: 2006-10-18
    body: "> and it has very *many* filters and scripts.\n\nKrita is much newer than GIMP. Give it some time.\n\n> Gimp does not have those strong dependencies to special KDE \n> or Gnome versions....\n\nThat's because GIMP is already fairly well established and its development (as far as I know) is not moving at a particularly fast pace anymore. When they move to the GEGL core however you can bet that GIMP will depend on much newer libraries in order to increase functionality.\n\n> Why not remove the feature freeze from Krita 1.6 and have \n> a nice and long living Krita 1.6, 1.7 branch?\n\nBecause there aren't enough developers to work on significant features in Krita 1.x *and* Krita 2.0 at the same time. Qt 4 offers a much more powerful toolset for developers to work with, and to make Krita the best painting tool it has to be developed to take advantage of Qt 4's new structures and features. I think a lot of people don't fully appreciate this yet, and I guess it won't be obvious to many until KDE 4 is released with lots of cool new stuff that Qt 4 has made possible.\n\n> Should I write a vignetting and distortion correction plugin \n> for Krita 1.6? I don't think that's a good idea because Krita 1.6 \n> won't see any significant updates anymore (only some bugfixes, if \n> I remember correctly) and Krita 2.0 is too far away \n\nKrita 1.6 will be around for a while in terms of usage, and if you have any intention of using it at all, surely putting work into a plugin now won't be completely wasted if there are still users around to make use of it. Chances are porting a plugin to 2.0 won't be all that hard when it does finally get released either."
    author: "Paul Eggleton"
  - subject: "Re: Krita - Marketing vs. Reality"
    date: 2006-10-19
    body: "> Krita is much newer than GIMP. Give it some time.\n\nOf course. But then they should not call Krita the \"best free image editing program\".\n\n>Because there aren't enough developers to work on significant features in\n> Krita 1.x *and* Krita 2.0 at the same time.\n\nThen I would concentrate on the Krita 1.5 code base. But I'm not a developer.\n\n> Qt 4 offers a much more powerful toolset for developers to work with, and to\n> make Krita the best painting tool it has to be developed to take advantage\n> of Qt 4's new structures and features.\n\nWrong, you do not need Qt4 to make a good painting tool. Painter, Photoshop 6, Picasa, Bibble,... all use older and simple toolkits. A image editor does not become magically better only because of Qt4's new painting engine or file dialog. It's all about Workflow, Usability, image editing capabilities and Plugins.\n\n> I think a lot of people don't fully appreciate this yet, and I guess it\n> won't be obvious to many until KDE 4 is released with lots of cool new stuff\n> that Qt 4 has made possible.\n\nForget this cool new stuff until the basics work. If Workflow, Usability, image editing capabilities and Plugins are top notch then it's time to think about requiring a new Qt or KDElibs version.\n \n"
    author: "Max"
  - subject: "Re: Krita - Marketing vs. Reality"
    date: 2006-10-19
    body: "> But I'm not a developer.\n\nWhich is why I think you may not appreciate that sometimes in software development, you have to stop working on the currently released version of a piece of software in order to pave the way for future development. Staying working on old versions does have its benefits, but you may spend a lot of time implementing features that would be much quicker to implement once you have been able to rework the underlying structures, which you can't do without risking the stability of the application. In a development version (Krita 2.0 / KDE 4 in this case) you are allowed to change a lot of stuff under the hood that might break things initially but will eventually result in a stronger base for future work. At some point you just have to make that leap.\n\n> Wrong, you do not need Qt4 to make a good painting tool. \n> Painter, Photoshop 6, Picasa, Bibble,...\n\nThat's a somewhat specious argument - they just wrote their own code rather than relying on a toolkit that is already built. Let's not forget either that all of those products (I assume, since I know of all of them except Painter) were developed by commercial entities with significant amounts of money to spend on development resources, unlike Krita. Some of them, such as Photoshop, have been around for many years and thus are much further along than Krita. It's not a particularly fair comparison.\n\nI wasn't suggesting that Qt 4 is a magic bullet, either - it just provides a much more capable foundation than Qt 3.\n\n> Forget this cool new stuff until the basics work.\n\nPeople have different ideas about what the \"basics\" are. Some of the things you consider \"basics\" might not necessarily be trivial to implement."
    author: "Paul Eggleton"
  - subject: "Re: Krita - Marketing vs. Reality"
    date: 2006-10-18
    body: "Yes, you should write plugins for 1.6. There is a plugin writing manual, there's nothing to stop you :-). You can release it separately and then, when 2.0 stabilizes contact us about it. We can then port it and put it in the main tree for 2.0. I think that Krita 1.6 will live for at least a year from now, and there will be one or two bug fix releases. \n\nBy the way: the downgrade message for unsharp mask with 16/bit channel images is a bug: in fact, all convolution filters work just fine with 16 bits/channel images and don't downgrade. (Except for lab, apparently -- but that already works in trunk, too.)"
    author: "Boudewijn Rempt"
  - subject: "Re: Krita - Marketing vs. Reality"
    date: 2006-10-18
    body: "> when 2.0 stabilizes contact us about it. We can then port it and put it \n> in the main tree for 2.0.\n\nOne of our developers ported his plugins to the trunk codebase in no time. I estimate he spent 1 or 2 hours per plugin of quite reasonable size.\n\nI think its good that you (Max) care and focus on the things that are needed. I'd love to see you put your foot down and convert your activism to productive output.\n\nThanks\n \n"
    author: "Thomas Zander"
  - subject: "Re: Krita - Marketing vs. Reality"
    date: 2006-10-18
    body: "Oh, and really: here's a hard offer. I will do the porting work for everyone who builds a fun and useful filter for Krita 1.6 and wants his filter in Krita's main distribution."
    author: "Boudewijn Rempt"
  - subject: "Re: Krita - Marketing vs. Reality"
    date: 2006-10-19
    body: "I will come back on you.\n\nMaybe I'm writing a usable Lens Correction filter or try to port Mplayers high performance unsharp mask filter and bicubic scaling filter."
    author: "Max"
  - subject: "Re: Krita - Marketing vs. Reality"
    date: 2006-10-19
    body: "I'm looking forward to it!"
    author: "Boudewijn"
  - subject: "Re: Krita - Marketing vs. Reality"
    date: 2006-10-18
    body: "> Should I write a vignetting and distortion correction plugin for Krita 1.6?\nwhats' wrong with the one allready included ? as for color adjustement, we only miss level, but then nothing prevents you to add it to 1.6, the change to 2.0 and Qt4 won't make it hard to port filters, for them, most of the internals of krita 2.0 will stay the same as 1.6."
    author: "Cyrille Berger"
  - subject: "Re: Krita - Marketing vs. Reality"
    date: 2006-10-19
    body: ">> Should I write a vignetting and distortion correction plugin for Krita 1.6?\n> whats' wrong with the one allready included ?\n\nIt's unusable. Have you ever tried to adjust more than one or two photos with it?\nHint: Take a digital camera with zoom and take 100 photos with different zoom levels and different apertures. Then try to adjust those photos with Krita...\n\nDistortion depends on camera model and focal length. Vignetting depends on camera model, focal length and aperture.\n\nDoes this filter read those informations from the exif tags? No. Does the filter at least support user defined profiles (e.g. distortion correction for a some camera model at 35mm, 50, 80 mm focal length)? - No\n\nFor each photo you have to fiddle with at least three numerical values until it looks right.\n\nHow to do it right? Look at Bibble or just guess/calculate the distortion values from vertical lines.\n\n\n> as for color adjustement, we only miss level,\n\n\"We only miss level?\"\nHow do I increase or decrease saturation? How do I remove points from the Brightness/Contrast curve?\nSuggestion: Combine \"Color Adjustment\" with \"Brightness/Contrast\" as nearly all other programs do.\nHow can I easily adjust the color temperature. \"Click white point\"...\n\n\n> but then nothing prevents you to add it to 1.6, the change\n> to 2.0 and Qt4 won't make it hard to port filters, for them,\n> most of the internals of krita 2.0 will stay the same as 1.6.\n\nAs I wrote: Krita 1.6 is missing many small, but needed features and it does not seem those features will make it into Krita 1.5.x. And KDE 4.0.2 is still very far away (KDE 4.0.2 is the earlyest I and many other people will be able to switch to Krita 2.0) which unfortunately makes Digikam or Gimp 2.3.x/2.4 more appealing.\n\nBtw.: Do you plan to improve the image preview in the filter dialogs in 1.5.x? Will it be possible to use Digikam-Image-Plugins in Krita 2.0?\n\nBut keep up the very good work(!) and don't let you demoralize by someone who would prefer an improved Krita 1.7 much more than Yet-another-kde-4.0-only application."
    author: "Max"
  - subject: "Re: Krita - Marketing vs. Reality"
    date: 2006-10-19
    body: "I do hope I'll find a way to reuse the digikam plugins for 2.0; probably not the kipi plugins since Krita is not the application to manage your photo collection with. I tried for 1.5, but I got hopelessly confused by which part of digikam I would have to fake to make the plugins work: there is not a really clear interface that I can just implement for digikam to access krita's image data. Especially when selections come into play. I thought it would be necessary to actually copy Krita's non-rectangular selection data onto a new transparent image, and then send it to digikam's plugins.\n\nYou can remove points from the brightness contrast curve by right-clicking them (I believe -- I actually use Krita most for painting, not for photo retouching. That's more Cyrille's lookout)."
    author: "Boudewijn"
  - subject: "Re: Krita - Marketing vs. Reality"
    date: 2006-10-19
    body: "> You can remove points from the brightness contrast curve by right-clicking\n> them (I believe -- I actually use Krita most for painting, not for photo\n> retouching. That's more Cyrille's lookout).\n\nDoes not work here (Krita 1.6.0)"
    author: "Max"
  - subject: "Re: Krita - Marketing vs. Reality"
    date: 2006-10-19
    body: "Hm, then its probably another key... I know it's possible, I just don't ever get the time to actually do it :-). Maybe something for a thorough usability review some time soon before the next release?"
    author: "Boudewijn"
  - subject: "Re: Krita - Marketing vs. Reality"
    date: 2006-10-19
    body: "simply select the point and press delete on the keyboard"
    author: "Casper Boemann"
  - subject: "Re: Krita - Marketing vs. Reality"
    date: 2006-10-23
    body: "Thanks, I did not see that it is possible to select points. The visual feedback is not easily to see. The lack of a context menu did ot help, too."
    author: "max"
  - subject: "Re: Krita - Marketing vs. Reality"
    date: 2006-10-20
    body: "Hi Boudewijn, how are you ?\n\nThere is a clear interface for all image plugins used by digiKam image editor and showfoto. Look the ImageIface class implementation from trunk :\n\nhttp://websvn.kde.org/trunk/extragear/graphics/digikam/utilities/imageeditor/editor/imageiface.h?rev=563320&view=auto\n\nBut since digiKam support 16 bits/color/pixel and advanced photograph metadata, we don't use QImage to store image data but a dedicaced container named DImg using a similar syntax than QImage :\n\nhttp://websvn.kde.org/trunk/extragear/graphics/digikam/libs/dimg/dimg.h?rev=557529&view=auto\n\nIf you need help to support digikam image plugins in Krita, let's me hear...\n\nGilles Caulier"
    author: "caulier gilles"
  - subject: "Re: Krita - Marketing vs. Reality"
    date: 2006-10-20
    body: "I'm fine -- much better than this summer :-). I'd love some help interfacing between Krita and Digikam! On the krita side, all that's necessary is to create a digikam-filter filter plugin that converts the krita data to dimg data and back and that can access the digikam filter settings dialogs."
    author: "Boudewijn Rempt"
  - subject: "Re: Krita - Marketing vs. Reality"
    date: 2006-10-20
    body: "And me, i'm very tired. digikam 0.9.0 is now in beta3 and we working hard to release final before Christmast 2006 (:=)))\n\nThere are a lot of new features, like geolocalization of picture, Metadata editing, etc...\n\nThe krita digikam-filter need to be linked with shared libdigikam.la library to running. it's not a problem for you ?\n\nAh, before to forget, i have tested last krita 1.6-beta1 release provided by Mandriva 2007, and i have found a little problem with PNG files : the text chunck are definitivly lost after editing. For photograph, this is _VERY_ important to preserve this data (easy to do using libpng) since ImageMagick/GraphicsMagick/digiKam/ExifTool use the same way to store EXIF/IPTC/XMP to this area like a bytearray. You just need to save in memory tese bytearray and rewrite is into target PNG file. Look \"PNG TextualData Tags\" list from this page :\n\nhttp://www.sno.phy.queensu.ca/~phil/exiftool/TagNames/PNG.html\n\nNota : the same way can be done if you want to convert without lost metadata from a PNG image to TIFF or JPEG file (and vis-versa).\n\nGilles\n\n\n\n"
    author: "Caulier Gilles"
  - subject: "Re: Krita - Marketing vs. Reality"
    date: 2006-10-20
    body: "I wouldn't mind; it would be an optional dependency or something like that. The exif thing is for Cyrille to fix :-). I'll get back to you in a few weeks -- when you've had time to rest a bit. I'm generally speaking quite jealous of your coding speed!"
    author: "Boudewijn Rempt"
  - subject: "Re: Krita - Marketing vs. Reality"
    date: 2006-10-21
    body: ">I wouldn't mind; it would be an optional dependency or something like that. >The exif thing is for Cyrille to fix :-). I'll get back to you in a few weeks >-- when you've had time to rest a bit\n\nWell digikam 0.9.0 final release is planed just before Christmast. We can talking about by mail directly without problem and when you want.\n\n> I'm generally speaking quite jealous of your coding speed!\n\n(:=))) thanks. Working on digiKam & co is very important for me (and for all photographs witch use linux, i hope)...\n\nGilles Caulier"
    author: "Caulier Gilles"
  - subject: "Re: Krita - Marketing vs. Reality"
    date: 2006-10-20
    body: "> How to do it right? Look at Bibble or just guess/calculate the distortion\n> values from vertical lines.\nWow just telling me this without looking to shout at me would have been so much nicer :D Except that is useless as bibble isn't available for free, so I can't test it. And while I do a little photography, I never correct the distortion as my numeric camera does it automaticaly for me, so nice input on the plugins is allways welcome :)\n\n \n"
    author: "Cyrille Berger"
  - subject: "Re: Krita - Marketing vs. Reality"
    date: 2006-10-23
    body: "> Except that is useless as bibble isn't available for free, so I can't test it.\n\nYou can test it for a month (or two weeks).\n\nJust download the rpm or deb, it works on nearly every x86 or x86-64 distribution. \nhttp://download.bibblelabs.com/download/distribution_list?download_data%5Bfirstname%5D=&download_data%5Blastname%5D=&download_data%5Bcompany%5D=&download_data%5Bemail%5D=&commit=Continue\n\nAdditionally the learning videos (on the same download page as above) might be interesting for you.\n"
    author: "max"
  - subject: "Re: Krita - Marketing vs. Reality"
    date: 2006-10-22
    body: "I would be the first to admit that cheerleading doesn't really help much.  However, Krita is still a work in progress.  \n\nI hope that it has now reached the point where I can do enough work on it to help with the debugging efforts.\n\nSo, if it is usable, try to use it and report what doesn't work yet.  This is needed since without a user base, it will only do what the developers think to test."
    author: "James Richard Tyrer"
  - subject: "Re: Krita - Marketing vs. Reality"
    date: 2006-11-06
    body: "\"Does at least the most basic filters (like unsharp mask, Gaussian blur,..) work with 16 bit? - No.\"\n\nActually... They do, mostly, but there was a spelling mistake in the code that meant that a warning was spuriously displayed when applying these filters to 16 bit rgba code. Fixed for 1.6."
    author: "Boudewijn Rempt"
  - subject: "Simple gradient in Krita"
    date: 2006-10-19
    body: "Can someone tell me how to do this in Krita?\n\nI picked two colors. Now I want to fill a selection with a linear gradient where the one color fades to the other from one side to the other. The default gradient gives me something where the one color is in the center and the other at both edges and I simply can not adjust anything. The custom gradient does not seem to work for me.\n\nAnd there is another thing: In Photoshop they have an infobox. When I start a gradient fill (or a line or whatever) with my first mouse click it shows the position of this first mouse click and the current position of the mouse as well as its relative coordinates with respect to the first click (including arclength, angle in degrees and rad...) Is there such a box in Krita as well?"
    author: "sebastian"
  - subject: "Re: Simple gradient in Krita"
    date: 2006-10-19
    body: "Hm, the custom gradient did work when I just tested it -- and the gradient Krita painted went nicely from left to right, from orange to green. \n\nWe haven't got an infobox yet -- but we would love to :-)"
    author: "Boudewijn Rempt"
  - subject: "Re: Simple gradient in Krita"
    date: 2006-10-19
    body: "> The custom gradient does not seem to work for me.\n\nWorks for me (Krita 1.6.0)"
    author: "Max"
  - subject: "Wow"
    date: 2006-10-20
    body: "there are over 100 comments!!\n\nAnyway...\nI really hope Kexi does take off.\nKudos to all the devs!!\n\nI also really hope KWord is da bomb in KOffice 2.0. I see it as KDE's potential 'killer app' (not mentioning Amarok, K3B, etc :)).\n\nCheers"
    author: "forrest"
---
The KOffice team is proud to <a
href="http://www.koffice.org/announcements/announce-1.6.php">announce</a> the <a
href="http://www.koffice.org/releases/1.6-release.php">1.6.0 release</a> of its office suite.  This release is mostly a feature release of Krita and Kexi, but also contains major enhancements to the <a href="http://www.odfalliance.org/">OpenDocument</a> and <a href="http://www.w3.org/Math/">MathML</a> support of KFormula and new scripting functionality. This version also contains a vastly improved version of KPlato, our project planning application.  Download packages for <a href="http://kubuntu.org/announcements/koffice-16.php">Kubuntu</a>, <a href="http://download.kde.org/stable/koffice-1.6.0/SuSE/">SuSE</a> or you can try before you install with the <a href="http://www.koffice.org/download/kofficelivecd.php">KOffice 1.6 live CD</a>.











<!--break-->
<p>The 1.6 release is intended mainly as a feature release for the two fastest developed components: Krita and Kexi. However, other components are being actively developed too, with astonishing results. The highlights of this release are:</p>

<ul>
<li><b>Krita Becomes Usable for Professional Image Work</b><br />
Krita and its maintainer Boudewijn Rempt <a href="http://dot.kde.org/1159194107/">won the aKademy Award for "Best Application"</a> at this year's KDE conference in Dublin.  With features such as magnetic selection, effect layers, colour model independence and full scriptability, it has risen to become what is probably the best free image editing program today.</li>

<li><b>Lots of New Features in Kexi</b><br />
Kexi, the desktop database application competing with MS Access, is the other application in KOffice that is already the best of its kind. Kexi has received over 270 improvements since KOffice 1.5. With this release, Kexi gains such features as the ability to <a href="http://www.kdedevelopers.org/node/2163">handle images</a>, <a href="http://www.kexi.pl/media/en/compact_db/">compact the database</a>, automatic datatype recognition and Kross scripting tools.</li>

<li><b>KFormula Implements OpenDocument and MathML</b><br />
The formula editor of KOffice now supports OpenDocument and MathML and uses it as its default file format.  It also surpasses the equivalent component in OpenOffice.org, scoring 70% on the <a href="http://www.w3.org/Math/testsuite">W3C MathML test suite</a> compared to 22% for OpenOffice.org Formula. We see this as one example where the work to provide a very well-structured codebase of KOffice pays off to create a superior support for the existing standard.</li>

<li><b>Scripting Support in KSpread, Krita and Kexi</b><br />
KOffice 1.6 brings scripting to a new level with scripting functionality in KSpread, Krita and Kexi.  Scripting is provided through the cross-language script bridge <a href="http://dot.kde.org/1152490640/">Kross</a>, which enables KOffice to be scripted in Python and Ruby with possible future extensions through Javascript and Java.  With this release, KOffice also introduces command-line scripting where, for example, spreadsheet documents can be automatically manipulated with scripts to create <a href="http://www.robweir.com/blog/2006/09/odf-twenty-patterns-of-use.html">many new usecases</a>.</li>
</ul>

<p>
This will be the last non-bugfix release until version 2.0, which will build on Qt 4 and KDE 4 technology. For more information, see the <a
href="http://www.koffice.org/announcements/announce-1.6.php">full announcement</a>, the <a
href="http://www.koffice.org/releases/1.6-release.php">detailed release notes page</a>,  and the complete <a
href="http://www.koffice.org/announcements/changelog-1.6.php">list
of changes</a>.</p>








