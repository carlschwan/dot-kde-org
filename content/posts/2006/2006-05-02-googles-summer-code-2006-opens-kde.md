---
title: "Google's Summer of Code 2006 Opens with KDE"
date:    2006-05-02
authors:
  - "jriddell"
slug:    googles-summer-code-2006-opens-kde
comments:
  - subject: "quality"
    date: 2006-05-02
    body: "All these project proposal sound like something which will get done anyway. Why not something kool, I mean\n\nIntegration stuff:\n* kpdf plugin for Firefox\n* KControl module for Wine\n* KDe dialogues in GTK apps\n* VBA implementation (cmp. OpenOffice)\n* Tutorial for KDE programming with Python\n* KDE Java backends\n* QT rendering for Firefox\n* Windows data integration for dual-booters (MyMusic, MyFiles)\n* bibtex engine for KWord\n* latex2odf converter\n* dictionary tool with wiktionary integration\n\n* Usage Talkback compiles (applications which report and analyse how they are used)\n* Podcast \"production\" tool\n* Torrent-based Podcasting\n* Screen recording\n* vcard tool\n* KDE Membership and finance management tool for associations\n* usability in KDE security\n* KDE user database / user map (cmp. gnome and debian developer map)\n* webbased translation tool\n* KDE presenter templates\n* KDE R Frontend\n* Knights extentions and integration\n* uxtheme conversion tool"
    author: "gerd"
  - subject: "What about fonts?"
    date: 2006-05-02
    body: "What about one or two good crisp fonts like Microsoft's Tahoma, Times New Roman or Courier? KDE could default to one of those. On the Windows platform, these fonts are clear, sharp and crisp. On Linux however, I find them blurry and not a pleasure to look at. To put it better, we need a better font management/rendering paradigm on Linux. Question is: Do you agree? "
    author: "cb"
  - subject: "Re: What about fonts?"
    date: 2006-05-02
    body: "Font rendering is pretty great if you run a distro that sets it up right (or you can do it yourself).  With KDE4, that'll improve even further, I hear.  You might want to check out the DejaVu font (family?), which is Bitstream Vera with wider support for unicode."
    author: "Lee"
  - subject: "Re: What about fonts?"
    date: 2006-05-02
    body: "I read a while ago, they are doing something for GTK, with would include make fonts go way beyond what windows offers. Would be nice if that would work for KDE too..."
    author: "boemer"
  - subject: "Re: What about fonts?"
    date: 2006-05-03
    body: "Ah... I've been using the Veras since they came out.  I had wondered about DejaVu.  I noticed they seemed to be the same."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: What about fonts?"
    date: 2006-05-03
    body: "DejaVu is based on Vera, but contains glyphs for all the Latin scripts, Greek, Cyrillic, Armenian and partially Arabic scripts. It also covers (or plans to cover) mathematical symbols. The Serif variant has some proper italic characters, instead of oblique. If you only need Latin characters, you may stick with Vera, I suppose, but I find the extended glyph coverage handy, say, to browse wikipedia, where the need of greek letters is often needed."
    author: "Luciano"
  - subject: "Re: What about fonts?"
    date: 2006-05-02
    body: "There is some activity around DejaVu font family. It borrowed the base from the Bitstream Vera fonts (which lacks many non-latin chars). The latest DejaVu fonts are very good, provide Arial-like, Times-like, and Courier-like fonts and  are shipped, at least, with latest OpenSuse.\n\nA lot of effort goes into making that family THE FONT family for open source desktop."
    author: "Daniel \"Suslik\" D."
  - subject: "Re: What about fonts?"
    date: 2006-05-02
    body: "Fonts can look *far* better on Linux than on Windows XP. Very similar to Mac-OS in fact. \n\nMaybe some distributions do not have the correct font configuration out of the box."
    author: "Meneer Dik"
  - subject: "Re: What about fonts?"
    date: 2006-05-03
    body: "This is so true and so little known!\nPeople complaining about fonts on Linux, READ THIS:\nOpen the KDE control panel and go to the Fonts settings. Click on the \"Configure...\" button. In the \"Hinting style\" combo choose \"None\". The setting will take effect only on newly started applications.\nNow you should have a nicely smoothed Vera or DejaVu font. If you still don't like it get a different font.\nIn the attachment, KDE using the default OSX font, Lucida Grande."
    author: "Flavio"
  - subject: "Re: What about fonts?"
    date: 2006-05-03
    body: "Or you can use fonts that haven't got broken hinting and get beautyfull fonts like Windows or OSX by enabling font-hinting. "
    author: "Carewolf"
  - subject: "Re: What about fonts?"
    date: 2006-05-03
    body: "I think the problem is that good fonts and/or font settings are not included *by default* in KDE. In other words, if I were to grab an SVN snapshot and build it, it would not have the best fonts.\n\nSomeone with SVN access should take a couple of hours and ensure that the KDE default fonts are indeed the very best free fonts available."
    author: "ac"
  - subject: "Re: What about fonts?"
    date: 2006-05-02
    body: "Times is no font suitable. Times was developed for English newspapers. It was a mistake to use the font for computing from a font development perspective."
    author: "humhum"
  - subject: "Re: What about fonts?"
    date: 2006-05-03
    body: "Then we need a good serif font to replace it."
    author: "James Richard Tyrer"
  - subject: "Re: What about fonts?"
    date: 2006-05-03
    body: "> Then we need a good serif font to replace it.\nGentium, Charis SIL or DejaVu Serif are pretty good hinted serif fonts."
    author: "Moyogo"
  - subject: "Re: What about fonts?"
    date: 2007-07-27
    body: "i agree 100% the fonts in KDE are not pleasing to look at for the most part and MS fonts look much better.  it would also be good to have times new roman for writing papers, documents, etc. for people who don't know anything outside of MS"
    author: "puter"
  - subject: "Re: quality"
    date: 2006-05-02
    body: "The wiki is editable by anyone -- why don't you add those ideas yourself?"
    author: "ac"
  - subject: "Re: quality"
    date: 2006-05-02
    body: "KDE Membership and Finance Management Tool?\n\nI guess that would mean... if they're not actively involved in the KDE community, they don't get paid? ;)"
    author: "Lee"
  - subject: "Re: quality"
    date: 2006-05-02
    body: "Vereinsverwaltung - Association management tool probably"
    author: "humhum"
  - subject: "Re: quality"
    date: 2006-05-02
    body: "> * kpdf plugin for Firefox\n\nNot a KDE project. KPDF works fine in Konqueror. And I think there is a plugin for Firefox to load KParts. It's up to them to make use of it.\n\n> * KControl module for Wine\n\nGood idea, but could be rather limited for a full project.\n\n> * KDe dialogues in GTK apps\n\nI do not understand. Besides, our goal is to make KDE applications better, not GTK apps.\n\n> * VBA implementation (cmp. OpenOffice)\n\nI'd much rather see JavaScript in the KOffice suite. The project to implement VBA would be too big for Summer of Code.\n\n> * Tutorial for KDE programming with Python\n\nSummer of Code must produce Code.\n\n> * KDE Java backends\n\nThose already exist.\n\n> * QT rendering for Firefox\n\nNot a KDE project. It would be a Mozilla project.\n\n> * Windows data integration for dual-booters (MyMusic, MyFiles)\n\nLooks too simple for a Summer of Code project.\n\n> * bibtex engine for KWord\n\nGood idea.\n\n> * latex2odf converter\n\nGood idea, probably coupled with the last one.\n\n> * dictionary tool with wiktionary integration\n\nI think this has been discussed and deemed impractical.\n \n> * Usage Talkback compiles (applications which report and analyse how they are used)\n\nGood idea, but this is suggested again and again. Implementing this is a good idea, but who is going to read the data that it produces? Please figure that out first before implementing this.\n\n> * Podcast \"production\" tool\n> * Torrent-based Podcasting\n\nI don't understand these.\n\n> * Screen recording\n\nGood idea, probably requires some deep X knowledge. Would also present some interesting problems regarding event compression.\n\nNote: there are some commercial tools that do that in KDE.\n\n> * vcard tool\n\nHuh? That's too vague.\n\n> * KDE Membership and finance management tool for associations\n\nCould be... but its usage would be so limited I'd rather see SOC money used for other things.\n\n> * usability in KDE security\n\nYou'd need to elaborate.\n\n> * KDE user database / user map (cmp. gnome and debian developer map)\n\nWe already have the contributor map.\n\n> * webbased translation tool\n\nNot a KDE project.\n\n> * KDE presenter templates\n\nSummer of Code must produce Code.\n\n> * KDE R Frontend\n\nWhat's R?\n\n> * Knights extentions and integration\n\nWhat's Knights?\n\n> * uxtheme conversion tool\n\nWhat's uxtheme?"
    author: "Thiago Macieira"
  - subject: "Re: quality"
    date: 2006-05-03
    body: "Given who you are in the KDE community, I am quite surprised that you are so unfamiliar with the items in this list as you are."
    author: "Mineri Devangu"
  - subject: "Re: quality"
    date: 2006-05-03
    body: "Huh? Really no need to bring this on a personal level. If you have something to say about the listed items then please do so, but abstain from attacking persons. Thanks."
    author: "Marco K"
  - subject: "Re: quality"
    date: 2006-05-03
    body: "There is a difference between ad hominems and just pointing out something as such."
    author: "Jui Kai"
  - subject: "Re: quality"
    date: 2006-05-03
    body: "A lot of those items he was \"unfamiliar with\" don't really seem connected to KDE.\n\n\"R\" is statistics, nothing to do with KDE. Knights appears to be a chess program unaffiliated with the KDE Project. uxtheme seems to be a Windows XP thing. Podcasting has never been particularly popular in the OSS community. Maybe those things would be useful in KDE, but I wouldn't be surprised if 90% of the KDE community doesn't care about any of them.\n\nSo, yeah, what the heck are you talking about?"
    author: "EY"
  - subject: "There is a R Qt/KDE frontend"
    date: 2006-05-03
    body: "It's called rkward (http://rkward.sourceforge.net). Still a little unstable, but usable."
    author: "Luca Beltrame"
  - subject: "Re: quality"
    date: 2006-05-03
    body: "http://www.r-project.org/\nA statistical power house for professionals, though without GUI, so no SPSS replacement for social scientists. There is a project I think\n\nKnights is the best and stable chess program for KDE but discontinued. Hope it will get ported to 4.\n\nuxtheme -- I think Frank Richter did something in the Wine project, \nconversion of Win-themes to KDE themes probably or theme bridge, quite a lot win-themes out there. \n\nPodcasting is like Ruby, it wins. It is no big issue in the KDE community because existing tools suck. Try iTunes on your Mac. Podcasting == audio blogging. Very simple from a technology perspective: mp3/pdf/ogg etc. + special RSS format\n\nE.g. German chaosradio podcast\nthttp://chaosradio.ccc.de/podcasts.html\n\nor O'Reilly Oscon05 talks\nhttp://www.itconversations.com/series/oscon2005.html\n\nBBC offers nice podcasts\n\ndaily language courses etc.\n\nor see http://www.podcast.net/\n\ntorrent-podcasts Oh well, this breaks the only real weakness of podcasting and will be required for videopodcasts. which means when you offer an mp3 of 10 megabyte and you get 10 000 subscribers its 100 GB for you as a provider of these services. The same applies when you file is 110 MB, so torrents like protocols would save bandwidth and the first tool which implements it wins.\n"
    author: "Gerd"
  - subject: "Re: quality"
    date: 2006-05-03
    body: "About the KDE dialogs in GTK programs:\nI think that this would be a huge improvement for KDE as well as for GTK programs. GTK programs will look much more like KDE programs with that simple addition. If we do this with GTK-QT, we may be able to have (almost) full integration between GTK apps and KDE.\n\n"
    author: "Alex Lowe"
  - subject: "Re: quality"
    date: 2006-05-03
    body: "Integration between GTK apps and KDE will occur when Gtk apps support kisolaves. In fact, a KDE dialog maybe unusable if the Gtk app can't access to the floppy or Cdrom by kioslaves (they are what KDE dialog shows)."
    author: "ibc"
  - subject: "Re: quality"
    date: 2006-05-03
    body: "Integration between GTK apps and KDE will occur when Gtk apps support kisolaves. In fact, a KDE dialog maybe unusable if the Gtk app can't access to the floppy or Cdrom by kioslaves (they are what KDE dialog shows)."
    author: "ibc"
  - subject: "Re: quality"
    date: 2006-05-04
    body: ">About the KDE dialogs in GTK programs:\n\nI really can't see the reason for all this facination with intergrating GTK+ applications flawlessly in KDE. If you absolutly need to run a GTK+ application, the GTK-Qt theme makes it look less out of place. If you want thighter intergration perhaps it's time the GTK camp do something for intergration too. \n\nAll told the sugestion of using KDE dialogs in GTK+ are nothing but lots of work for very little gain. As I see it, next to no GTK+ application exist which does not have a KDE eqvivalent beeing just as good or even better. I guess the only exceptions are Incsacpe and Kino. And I think making Karbon as good as Incscape are less hard and difficult work, than making GTK+ applications use KDE dialogs in a stable manner(and keeping it in sync with changes in GTK+). Basicly it's a question of using the development resources wisely. "
    author: "Morty"
  - subject: "Re: quality"
    date: 2006-05-03
    body: "R is a statistics program\nhttp://www.r-project.org/\n\nIt would also be great to have very good frontends for other open source math programs like maxima, yacas and octave."
    author: "JA"
  - subject: "Re: quality"
    date: 2006-05-03
    body: "> > * VBA implementation (cmp. OpenOffice)\n> I'd much rather see JavaScript in the KOffice suite. The project to implement VBA would be too big for Summer of Code.\n\nKOffice has a framework for scripting languages (called kross) that allow exporting the same API to any potential script language (currently it only supports python and ruby, but JavaScript would probably be a two days work at most), but only Krita and Kexi use it currently. Some work has been made to have it integrate in KSpread.\n\nAs for VBA, well : 1) it would be pretty easy to add support for basic interpreter (like gambas for instance) 2) the VBA's API has a lot of problem (bloat, unstability, etc...) which we want to avoid in KOffice.\n\nBut we would welcome anyone willing to write the kross binding to KWord, KPresenter and add more functions to the KSpread one."
    author: "Cyrille Berger"
  - subject: "Re: quality"
    date: 2006-05-02
    body: "> * KDe dialogues in GTK apps\n\nIt's just the file dialog, but take a look at http://www.kde-apps.org/content/show.php?content=36077\n\nI've tried it in Firefox, and it does seem to work."
    author: "Dhraakellian"
  - subject: "Re: stuff"
    date: 2006-05-03
    body: "KPDF plugin for Firefox: Interesting idea but not general enough.  How about a NSPlugin for all KParts that work in Konqueror.\n\nKDe dialogues in GTK apps: Would QTK+ be too much of a kludge?\n\nQT rendering for Firefox: this is in the source tree but it is still alpha.\n\nWindows data integration for dual-booters: We have that, but it ONLY works for KDE apps.  This is only a bug that needs fixing.\n\nAnd my ideas,\n\nXDG integration of icon theme selection and widget themes.  IIUC we are going to have MIME types in KDE-4 but someone could backport it to 3.6.\n\nA SMIL authoring tool.\n\nGoogle heal thyself.  Google needs to have everything it puts on the web be 100% W3C & ECMA compliant.  Note that this works both ways.  If they can't do everything that they want to with existing W3C stuff then they can contribute to the W3C standards."
    author: "James Richard Tyrer"
  - subject: "Re: stuff"
    date: 2006-05-03
    body: "I think there already is an NSPlugin that loads KParts. I remember seeing this more than once appear in bugs.kde.org because Konqueror loaded the nsplugins KPart, which runs nspluginviewer, embeds it and then loads the NSPlugin that loads the target KPart (KGhostviewPart). I've seen it because it crashed.\n\nOf course, it's completely unnecessary to load nspluginviewer to load a KPart, since Konqueror can display a KPart directly. But Firefox could use it. I'm not sure if it would be a KDE or Mozilla SOC project though."
    author: "Thiago Macieira"
  - subject: "Re: quality"
    date: 2006-05-03
    body: "The other way around it makes sense:\n\nThe Konqueror blocked popup notice is as fat, misplaced and therefore annoying as a propup -> make it like Firefox (small and near to the url bar). Same for the seamless find function os Firefox.\n\nThe ad blocking functionality doesn't compare to Firefox plugins (adblock plus, rip) and is not reachable from the popupmenu. The current solution is (and that only for single box installations) as good as a proxy with filtering capabilities.\n\nThe bookmark system is very dated - no live bookmarks, no keywords, no engine that creates backups of sites as requested, tests for unreachable sites and informs the user after a specified number of tries/weeks about the dead bookmarks.\n\nMaking the popupmenu configureable by the user would be a huge improvement.\n\n\n\nThat would be Konqueror usability improvements. Rendering is fine both with Gecko and KHTML, but I don't know a reason to use Firefox with KHTML under the hood."
    author: "Carlo"
  - subject: "What has become of last years projects?"
    date: 2006-05-02
    body: "There are many interesting and cool projects are on last years list, full NX integration, speech recognition, oKular, VoIP and Video Conference support for Kontact, ... But since last years SoC ended I never heard of these projects anymore.\n\nWhat happend to them?\n\nIt looks like a general problem with these Summer of Code projects. Most of them remain in that state the student left them forever. "
    author: "furanku"
  - subject: "Re: What has become of last years projects?"
    date: 2006-05-03
    body: "Very true.  Hard to get excited about anything this year when there are few visible results from last year.  It might be a good idea to pick up some of the stuff that was left undone.  Getting things done a bit at a time each summer isn't totally ideal, but perhaps better than letting things rot.  If it was a good idea last summer, I daresay it still is."
    author: "MamiyaOtaru"
  - subject: "Re: What has become of last years projects?"
    date: 2006-05-03
    body: "full NX integration never got off the ground. The student had to abandon the project very early.\n\nSpeech recognition was finished and merged into KDE (KHotKeys). oKular is still in development. KCall also is there already, but hasn't seen much attention since the end of last year's SOC.\n\nThe one project I'd particularly like to see picked up is Kamion."
    author: "Thiago Macieira"
  - subject: "Re: What has become of last years projects?"
    date: 2006-05-05
    body: "My own project was variable analysis in kdevelop.  I did what I said I would, but that's maybe 10% of the work needed.\nAt the moment I'm putting all my kde time into ksysguard instead.  It is nice to be able to play with the more fun things, but there are far more urgent things to fix in kde first ;-(\n"
    author: "John Tapsell"
  - subject: "Things I would love to see."
    date: 2006-05-02
    body: "1.) True autocompletion support for Python in KDevelop.\n2.) True WYSIWYG print output, particularly with respect to fonts. (If you don't believe me, do some serious looking in Bugzilla; and you will see the complaints.)\n3.) More DCOP interfaces.\n4.) Font DPI parity between GNOME and KDE. (Why is it that the same fonts, at supposedly the same size, render differently in KDE and GNOME. Gtk+/GNOME applications can look terrible when started in KDE without gnome-settings-daemon?)\n5.) Full Esperanto localization. ;-)"
    author: "Robert de Gaulle"
  - subject: "Re: Things I would love to see."
    date: 2006-05-03
    body: "> True autocompletion support for Python in KDevelop\n100% correct autocompletion for dynamic typed languages is not possible.\nBut for most cases good guess can be made.\n\nDo you want to pick up this project? Just send a mail to kdevelop-devel@kdevelop.org (subscribers only) and you'll surely find a mentor.\n"
    author: "adymo"
  - subject: "Re: Things I would love to see."
    date: 2006-05-03
    body: "\"True autocompletion support for Python in KDevelop\n 100% correct autocompletion for dynamic typed languages is not possible.\n But for most cases good guess can be made.\"\n\nYes, it's true you can't do it statically just by parsing the source of the program. But you can do it dynamically while the program is running using python or ruby introspection. \n\nSo when you're using Ruby's irb shell it has code completion because it's interacting with running code. If it was possible to provide a graphic front end for irb or python, so you could develop code as it was running like a Smalltalk environment, that would be a killer application. But unfortunately it would probably be too ambitious for a SOC project."
    author: "Richard Dale"
  - subject: "Re: Things I would love to see."
    date: 2006-05-03
    body: "We can't get DPI parity between GNOME and KDE. The bug is in GNOME incorrectly handling and changing DPI in X11, but the bug has been closed as WONTFIX, since it only bothers KDE that they mistreat the standard."
    author: "Carewolf"
  - subject: "Re: Things I would love to see."
    date: 2006-05-03
    body: "I haven't had any luck finding this in the bugzilla database. Could you please provide a link to it, as I would be very interested in reading about it?"
    author: "Robert de Gaulle"
  - subject: "Re: Things I would love to see."
    date: 2006-05-03
    body: "Summary:\n\nWindows is totally broken in this. Regardless of your actual screen size, it sets the resolution to 96 DPI.\n\nMonitor manufacturers very often write wrong dimension information in their devices' EDID data. This means the monitor size is often incorrectly detected.\n\nX detects the size automatically and calculates the DPI correctly (if the monitor EDID was correct). You can override the values if they are incorrect.\n\nKDE and Qt-only applications make use of the DPI to translate the font size from points to pixels. Remember that 72pt = 1 inch. So, at 96 dpi, a 72pt font should be 96 pixels high.\n\nMany web designers are careless and write webpages that assume the text size is constant. This is obviously bogus (think of accessibility for an instant!), so the page layout completely breaks if your dimension doesn't match 96 DPI.\n\nKHTML enforces a minimum of 96 DPI by scaling the fonts if the monitor settings are < 96 dpi. This is only done so that webpages are shown reasonably correct.\n\nGNOME couldn't care less what the monitor size is and follows on the Windows footsteps. But it does much worse and SETS the X configuration to 96 dpi. This causes the weird behaviour of KDE applications changing font sizes after you start some GNOME applications.\n\nBoth sides say it's WONTFIX. We're in deadlock."
    author: "Thiago Macieira"
  - subject: "My suggestions"
    date: 2006-05-03
    body: "\n0) The Twinkle SIP client is already pretty incredible, but it needs some usability love. Work on it so that it can become the standard VoIP client on KDE. Improve its Kontact integration and extend the range of SIP services included in its setup wizard. Eventually, all KDE users and developers should be able to call each other out of the box for free when they install KDE. This would do wonders for KDE project collaboration and closeness among the KDE community.\n\n1) Further extend the OpenDocument support in all KOffice applications. KDE 1.5's support is a good beginning, but its rendering and use of documents produced by openoffice is still far from seamless. Feature-wise, Koffice is already very impressive. \n\n2) Work on the stability of Koffice. Assign somebody to do ongoing unit testing and bug squatting in kword, kpresenter and kspread.\n\n3) A professional citation and bibliography management application that integrates with Kword. Tellico already has some bibliography management functionality, but Tellico is a general collection management application and in trying to be all things to all people doesn't do the bibliography management as well as it could. We could call it \"Kcite\".\n\n4) Improve the multimedia features of Digikam. The current pki filters are cumbersome and have never worked properly for me.\n\n5) Kpilot/Opensync functionality. This shit needs to just work. Send a student 10 or 15 pdas and make sure that he cannot leave his room until they all work seamlessly with Kontact.\n\n6) Better tested integration of Kontact and the groupware suites, particularly egroupware. Egroupware has remained as one of the top-5 projects in sourceforge for quite some time and has one of the most impressive and easiest to deploy applications, but kontact's support for it is clunky and buggy.\n\n7) Location-based awareness for all applications. Create location-based configurations for all applications. When I am home and I switch to my home AP, my default printer should switch to the right one as should my network-mounted shares and so forth.\n\n8) Some further work-flow and usability love for Kontact. For instance, Kontact's naming of its own features is far too technical for everyday users. Many people often don't realize what great features lay hidden behind many KDE applications because their naming simply baffles them. I write this as a linguist and translator. I realize that Google may be more interested in hiring developers than people of my training, but a software application is a lot more than just its code. With the increased awareness that the KDE4 efforts are having about interface design, maybe opening one slot or two for graphic artists and translators would be a good thing to do for the SOC positions."
    author: "Gonzalo"
  - subject: "Re: My suggestions"
    date: 2006-05-03
    body: "<i>5) Kpilot/Opensync functionality. This shit needs to just work. Send a student 10 or 15 pdas and make sure that he cannot leave his room until they all work seamlessly with Kontact.</i>\n\n\n Where are you going to *get* 10 or 15 PDA's? It's not like any manufacturer has ever supported syncing on Linux in any way at all. These things aren't free (as in beer), you know. The number of people working on syncing worldwide is very low (including OpenSync, perhaps half a dozen) and it's just a very much no-fun thing to do (testing is a pain because you need a device, usage patterns vary wildly, the devices change their data support irregularly, etc.)\n"
    author: "Adriaan de Groot"
  - subject: "Re: My suggestions"
    date: 2006-05-03
    body: "I am sure we could get corporate sponsors to donate the needed hardware if there was a serious proposal to make said hardware work with Linux.\n\nAt the end of the process, the hardware could either be returned or if desired by donor company, it could then be given to people who complete specific KDE project goals or any other arrangement that the KDE foundation and donyouor of hardware finds suitable.\n\n10 PDAs x $300 or $400 is not that much money.\n\nSo, by and large, this is doable."
    author: "Gonzalo"
  - subject: "SharePortal for KDE and unison KIO"
    date: 2006-05-03
    body: "If we want to compete with Microsoft, we must have better tools for user friendly collaborative work.\n\nAdding a wiki component in the Kollab stack tailored for knowledge workers which integrates well with KOffice and Kontact. For example using the shared Kollab addressbook, this wiki would creates pages for every employee of your team where you could easily drag and drop Kword files, save from KWord into a special page of the wiki.\n\nUnison is a nice application that I use everyday to synchronize my notebook and my main computer. The command line interface is geeky and the gtk frontend clumsy. Adding synchronization in the heart of KDE with a kio slave, a kcontrol module and a framework for reconciliation of files would be nice."
    author: "Charles de Miramon"
  - subject: "Re: SharePortal for KDE and unison KIO"
    date: 2006-05-06
    body: "Well, as far as I can tell (not much experience with it), NetBeans has a good collaboration plugin, so perhaps better integration with Java apps in general would be helpful.  Probably will become more viable if Sun releases their J2SDK under a free license or when GNU Classpath finishes up support for Swing."
    author: "JVz"
  - subject: "libwv3?"
    date: 2006-05-03
    body: "How about doing something that will *really* make a difference?\n\nRip out the OOO code that imports MS formats, and also code that saves those internal structures to ODF.\n\nCan't trash liwv2 fast enough."
    author: "yaac"
  - subject: "Re: libwv3?"
    date: 2006-05-03
    body: "In that case you could as well rename OOo to libwv3 directly. >_>"
    author: "ac"
  - subject: "Re: libwv3?"
    date: 2006-05-04
    body: "Congratulations, you're the 3456389475639845th guy suggesting this. Its just not feasible.\n\nAnd with MS \"opening\" their office formats, I don't see how to motivate someone to reverse-engineer the old binary formats."
    author: "me"
  - subject: "Re: libwv3?"
    date: 2006-05-04
    body: "> Its just not feasible.\n\nThe reason being...? Is the code spread all over the whole thing? That being Sun creature and all, it's still hard to believe that the design can be *that* awful.\n\nWhen people say \"not feasible\", it usually really means \"we are too lazy to even try\". And then when somebody really takes on the challenge, things turn out to be perfectly feasible. How many times we heard DVD menus are not possible in MPlayer, and heard it from the main devs themselves? Guess what, now the patch is there, and it won't take long for it to go in.\n\nEven if MS does \"open\" its formats, the old formats will be around for a very long time, and if anybody hopes to just ignore the problem in KOffice, then we'll be stuck with OOO for years to come I'm afraid."
    author: "yaac"
  - subject: "Re: libwv3?"
    date: 2006-05-04
    body: "I've tried and got nowhere. But since that means that \"I'm too lazy\", please try yourself. Get onto the koffice-devel mailing list. Checkout the KOffice and OO sources. Start producing code. If you don't you're not only too lazy too try, you're too lazy to discover that \"not feasible\" is not the same as \"too lazy too try\". \n\nIn other words, \"yet another anonymous coward\", get coding, or shut up in the presence of people who know more than you.\n\nOn the other hand, if you manage to prove me wrong, I will announce the fact of my being wrong from the rooftops.\n\nBut I bet you won't prove me wrong."
    author: "Boudewijn Rempt"
  - subject: "Re: libwv3?"
    date: 2006-05-04
    body: "No need to take it so personally. Note that I said \"usually\", so it doesn't apply in all cases.\n\nStill, the fact that you tried and got nowhere doesn't mean \"it's just not feasible\". You just didn't try hard enough. Again, please don't take this as an offence. I am not blaming you for anything.\n\nThe only thing I am saying is that if somebody wants it bad enough, they will do it. That somebody won't be me, because I don't want it bad enough. Last week I finally gave up and installed OOO.\n\nSo why not use this as a challenge for anyone who wants to take on it? That's what SoC is for, right?"
    author: "yaac"
  - subject: "Re: libwv3?"
    date: 2006-05-06
    body: "Could you be the mentor for such proposal?"
    author: "blacksheep"
---
Google's <a href="http://code.google.com/soc/">Summer of Code</a> has opened for student applications, and KDE is again seeking students to mentor over the holidays.  Our <a href="http://wiki.kde.org/tiki-index.php?page=KDE%20Google%20SoC%202006%20ideas">ideas page</a> lists some of the projects you could work on, or you are encouraged to come up with your own.  <a href="http://developer.kde.org/summerofcode/soc2005.html">Last year</a> we had 24 students working on KDE projects, one of the highest numbers of any project and gained important projects like Okular.  For more information see the <a href="http://code.google.com/soc/studentfaq.html">student FAQ</a> and <a href="http://code.google.com/soc/student_step1.html">participant signup page</a>.  <strong>Update:</strong> be quick, the deadline is midnight UTC ending next Monday 8th May.







<!--break-->
