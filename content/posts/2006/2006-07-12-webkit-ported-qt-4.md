---
title: "WebKit Ported to Qt 4"
date:    2006-07-12
authors:
  - "gstaikos"
slug:    webkit-ported-qt-4
comments:
  - subject: "Sharpness"
    date: 2006-07-11
    body: "It may be just the quality of the screenshots, but they look a bit vague, almost out of focus. Is the a real phenomenon (maybe due to Qt4's drawing?) or is it an artifact of the screenshots and nothing to worry about?"
    author: "Andre Somers"
  - subject: "Re: Sharpness"
    date: 2006-07-11
    body: "Did you click on them?  They're quite sharp, minus the antialiasing on the text which is supposed to make it look a bit less sharp I guess. :-)"
    author: "George Staikos"
  - subject: "Re: Sharpness"
    date: 2006-07-11
    body: "Of course I clicked on them. It is the text I am worried about. Antialiasing is fine, but this looks out of focus, and would be very tiresome on the eyes. I tried reading the KDE page screenshot, and I'm glad my KDE 3.5.3's Konqueror renders a sharper page...\nBack to my question: can I conclude this effect is real then? "
    author: "Andre Somers"
  - subject: "Re: Sharpness"
    date: 2006-07-11
    body: "Just so I understand, you are \"concerned\" about the ?fonts? in a pre-alpha port of a web browser to a new platform?  It can't render flash yet either. OMG!"
    author: "anonymous"
  - subject: "Re: Sharpness"
    date: 2006-07-11
    body: "Are you looking at those screenshots on a CRT? The screenshots are using subpixel rendering which definately will look odd on a CRT ..."
    author: "Torsten Rahn"
  - subject: "Re: Sharpness"
    date: 2006-07-11
    body: "Well, I'm using a TFT and it really hurts my eyes looking at those screenshots. So either my pixel arrangement is different from the poster's or the poster has a really wacky setup which he got used to and actually thinks is sharper than what he had before ;-)"
    author: "Quintesse"
  - subject: "Re: Sharpness"
    date: 2006-07-12
    body: "I don't like the fonts either, but i'm sure its a setting, just like it is now... you can configure the look of your fonts ;-)"
    author: "superstoned"
  - subject: "Re: Sharpness"
    date: 2006-07-12
    body: "You're looking at a screenshot of subpixel anti-aliasing, not actual anti-aliasing."
    author: "Brandybuck"
  - subject: "Re: Sharpness"
    date: 2006-07-12
    body: "I think it's the subpixel arrangement.  If I'm an inch or so from the screen, it appears that the subpixels are \"reversed\" from where they should be... i.e., the red is on the right side of the pixel and that's down the right side of the character creating a halo effect on that side.  Same goes for the left.\n\nI'm sure it looks great if your screen physically matches the subpixel layout."
    author: "Evan \"JabberWokky\" E."
  - subject: "eyes in pain, full subpixel hinting"
    date: 2006-07-13
    body: "I'm using full hinting here, and the screenshots look actually very painful to my eyes. It's as if they were horizontally blurred"
    author: "((o) (o))"
  - subject: "Re: eyes in pain, full subpixel hinting"
    date: 2006-07-13
    body: "Perhaps the hinting was set up differently on the machine where the screenshots were taken. Different monitors sometimes require different sub-pixel hinting styles due to the way the screen is laid out."
    author: "Paul Eggleton"
  - subject: "Re: Sharpness"
    date: 2006-07-12
    body: "The font is terrible bad. I have a look on different systems with different monitors. Pls check your eyes.\n\n"
    author: "anonymous"
  - subject: "Re: Sharpness"
    date: 2006-07-12
    body: "you first :)"
    author: "AC"
  - subject: "Re: Sharpness"
    date: 2006-07-12
    body: "that \"sharpness\" seems to be of nvidia sharpness settings. not just fonts but all elements look sharper (white contrast around black icons, fonts, elements).\n\n"
    author: "fast_rizwaan"
  - subject: "Qt 4 on windows"
    date: 2006-07-11
    body: "Are there plans or interests to have it run on Windows?\nBecause of the depency on Qt4 only it should not be so hard to port."
    author: "win"
  - subject: "Re: Qt 4 on windows"
    date: 2006-07-12
    body: "All of KDE 4 is intended to run on Windows, so I'm sure it will be.\n"
    author: "Michael Dean"
  - subject: "Re: Qt 4 on windows"
    date: 2006-07-13
    body: "Hehe,\n\nnot all of KDE, but most of the apps and i.e. this for sure.\n\nE.g. the WM will not run on Windows as Windows has already a WM."
    author: "tempa"
  - subject: "khtml"
    date: 2006-07-11
    body: "Sounds like Webkit support will make support easier and will free ressources."
    author: "pentamax"
  - subject: "AJAX Support?"
    date: 2006-07-11
    body: "Please, oh, please tell me this will make Konqueror have good support for AJAX pages. If so I might just have to learn C++ and help out X-D."
    author: "Devon"
  - subject: "Re: AJAX Support?"
    date: 2006-07-11
    body: "What exactly do you mean by 'good support for AJAX pages'? Konqueror does support XMLHttpRequest, the only problem is that various 'hacks' are used to make DOM-manipulation cross-browser, and once again, people conclude your browser is either IE (which does not support all W3C's DOM methods), or Mozilla/FireFox (which has some oddities as well).\n\nI mainly develop on Konqueror, using the W3 methods (using AJAX as well), and 'hack' around the various buggy implementations found in other browsers."
    author: "Peter"
  - subject: "Re: AJAX Support?"
    date: 2006-07-12
    body: "Well http://alpha.qunu.com/ doesn't work right, http://www.bestbuy.com's menus don't pop down after a mouseover (not AJAX but its in JavaScript I think), and http://www.bluedot.us's edit Dot feature doesn't work right. None of them are sporting the W3C seal of approval so I guess they might not be W3C standards compliant. Note that all of those work in Firefox 1.5 and I thought that Firefox 1.5 would be sticking just with the standards and not pulling a Microsoft on us but maybe not.\n\nbtw I might make a ebuild for the Konqueror SVN. What folders would be the ones I would want to work with? The SVN layout confuses me :/"
    author: "Devon"
  - subject: "Re: AJAX Support?"
    date: 2006-07-12
    body: "BestBuy's site works fine here (Konqi/KDE 3.5.3), though alpha.qunu.com didn't load right for me.  Also the 'J' in AJAX is 'JavaScript' (\"Asynchronous JavaScript and XML\" is the whole acronym)."
    author: "Corbin"
  - subject: "Re: AJAX Support?"
    date: 2006-07-12
    body: "Konqueror itself had a moderately annoying bug in XMLHttpRequest back in KDE 3.4 where it was appending a null byte to the end of every POST request (bug 113393). Can't say that any of the other browsers have been very fun to work with either, though."
    author: "EY"
  - subject: "Re: AJAX Support?"
    date: 2006-08-07
    body: "Did you even check to see if the bug you mentioned was fixed or not ?"
    author: "DA"
  - subject: "Re: AJAX Support?"
    date: 2006-07-12
    body: "me too ;) the only reason I'm sometimes forced to use firefox is those fancy websites that do weird complicated things with javascript n'stuff. I'd much rather do everything in konq; firefox reminds me more of windows every time I try to use it :("
    author: "Chani"
  - subject: "Re: AJAX Support?"
    date: 2006-07-12
    body: "I'm in the same boat. I pretty much only use Konqueror for the kde: and qt: shortcuts now or when I'm doing something like uploading a file (KDE's file dialog is so much easier)."
    author: "Ian Monroe"
  - subject: "HTMLElement.prototype ... workarround"
    date: 2006-07-13
    body: "http://alpha.qunu.com/\nin Konqueror-3.5.3 I get\nFehler: http://alpha.qunu.com/qunu.js: ReferenceError: Can't find variable: HTMLElement\n[...]\n\nLine 532: {HTMLElement.prototype.removeNode=....\nhmm ... we know that HTMLElement prototyping doesn't work out of the box in KTHML-based Browsers - so it seems like they didn't even test their stuff in Safari!\n\nBut we are lucky - there is a simple workarround:\nhttp://www.codingforums.com/archive/index.php?t-60406.html\n\nAjax problems are manly caused by small javascript-cross-browser-differences. But there are well done libraries like http://www.mochikit.com to fix this.\n\nregards, Jan"
    author: "Jan"
  - subject: "Re: HTMLElement.prototype ... workarround"
    date: 2006-07-13
    body: "Of course it didn't work --- it's non-standard behavior that's basically an implementation detail of Gecko! Comparable code for Konqueror would be to use \nwindow[\"[[Element.prototype]]\"].  Unfortunately typical \"Web 2.0\" webmasters seem to think that IE and Mozilla are the only browsers, forcing other browsers to emulate things like this, and also non-standard language extensions like getters/setters (typical pattern seems to be to use Mozilla extensions to emulate IE extensionw).  This does, BTW work in upcoming 3.5.4 to some extent (just for the base classes, KHTML doesn't have separate prototypes for divs and such); and to a fuller extent in development version of Safari.\n\n\n"
    author: "SadEagle"
  - subject: "Re: AJAX Support?"
    date: 2007-03-25
    body: "why Konqueror is not compatible with gmail? "
    author: "Ludvic"
  - subject: "Re: AJAX Support?"
    date: 2007-03-25
    body: "Have you tried setting a different user agent?\n\nOne of the widely known issues ith GMail is that they have a broken browser check and seem to not detect capable Konqueror/KHTML versions correctly."
    author: "Universe"
  - subject: "Impact on KDOM2"
    date: 2006-07-12
    body: "Which impact does this have on KDOM2 and KSVG2? I think KDOM2 has a really great concept behind.\nIs Webkit beeing ported to that, or would we \"loose\" that development efforts if we would go WebKit directly/only? Can anyone involved shed some light?\n\nThanks!"
    author: "Shyru"
  - subject: "Re: Impact on KDOM2"
    date: 2006-07-12
    body: "Just going out on a limb: since KSVG2 is based on top of KDOM2, and KSVG2 is being ported to Webkit, it would make sense that KDOM2 is as well.\n\nI am not sure, however."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Impact on KDOM2"
    date: 2006-07-12
    body: "Rob Buis, one of the developers behind KSVG2 is currently working in the Webkit camp. The most up-to-date version of KSVG2 resides in Apple's repository. Rob told me they plan to port the \"good\" parts back to KDE.\n\nIn general, all code concerning XML & HTML is chaotic. I fully understand anyone who can't track it(I don't, I didn't know about unity until now). There's KSVG2, Patternist, KHTML, KHTML2, KDOM, WebCore, and the list goes on.\n\nPersonally i'm interested in Patternist, currently residing in KDOM. It is an XPath 2.0/XQuery 1.0/XSL-T 2.0 implementation(not complete though, but good on its way). Once integrated, it will provide those technologies to the whole of KDE. And Konqueror will have features other browsers are light-years from acquiring.\n\nFor those interested, Patternist's API documentation is here:\n\nhttp://patternist.sf.net/documentation/API/\n\n\nCheers,\nFrans\n\n(KDOM/Patternist developer)\n"
    author: "Frans Englich"
  - subject: "Re: Impact on KDOM2"
    date: 2006-07-12
    body: "I also heard about patternist some time ago, and i really think it is a great base for anything dealing with XPath/XQuery/XSL. I always thought that patternist would be used in KDOM2 and thus also be used in KHTML2/KSVG2. Is this wrong? (You stated it resides in KDOM)\nThe next question is: if KSVG2 is developed inside WebKit, is KDOM2 also in WebKit? I was always under the impression that KSVG2 and KHTML2 relied on KDOM2 as a common base. Is this also wrong?\n\nAnyone care to enlighten us more? Any information would be greatly appreciated!"
    author: "Shyru"
  - subject: "Re: Impact on KDOM2"
    date: 2006-07-12
    body: "Hi,\n\nIt is all a bit hard to explain :) This describes our experiences best:\n\nhttp://lists.kde.org/?l=kfm-devel&m=114099614807147&w=2\n\nPlease drop by in #ksvg sometime if you need more info.\nCheers,\n\nRob."
    author: "Rob Buis"
  - subject: "Re: Impact on KDOM2"
    date: 2006-07-13
    body: "\nPatternist resides in KDOM(that is, kdenonbeta/kdom/patternist) but does not have KDOM nor Webkit as dependency. The only essential dependencies are QtCore and kdecore(although Patternist did have a significant dependency on the KDOM library once).\n\nSo, the dependency goes the other way around; KHTML will depend on Patternist in order to get XSL-T support, for example. Similarly, if KOffice will want to use XSL-T for its import/export filter(or whatever), it will link to Patternist.\n\nThe good part is that Patternist isn't hard coded on a particular tree implementation(which for example KHTML/KDOM/Webkit/Unity is) meaning it's possible to query anything if one sub-classes a couple of classes. QDom, KHTML's tree, or pretty much any hierarchical data.\n\nThe idea is to move Patternist into kdelibs/patternist since it is the lowest denominator of everything, but it /might/ be some other place will be more suitable now with Unity and all, although I currently doubt it.\n\n\nFrans\n"
    author: "Frans Englich"
  - subject: "Re: Impact on KDOM2"
    date: 2006-07-13
    body: "\nSome more comments:\n\n* No, Patternist doesn't use KDOM's tree implementation. KDOM has an implementation of W3C's DOM XPath Module. That is, the implementation acts as glue code adapting Patternist's APIs into the DOM API(which is a very limited subset compared to Patternist's). So, KDOM depends on Patternist in order to supply the XPath DOM API.\n\n* No KDOM2 exists. KDOM, kdenonbeta/kdom, exists, which originally was derived from KHTML during KDE 3.x(kdelibs/khtml). Nikolas Zimmermann(one of the main hackers on KDOM) have said he consider KDOM dead because work is taking place elsewhere. However, KDOM contains tons of code that can be integrated elsewhere(such as XPointer, OASIS XML Catalog 1.1, XInclude 1.0 and DOM 3 Core implementations).\n\n* KSVG2 is the rewrite of \"KSVG\". KSVG is the one used in KDE 3.x, in kdegraphics. KSVG is not relevant in these discussions.\n\n* KSVG2 is(or was) being integrated into kdelibs/khtml in  branches/work/khtml-svg(by Nikolas and Rob). However, it's been a while since that was active so perhaps it will start all over now with Unity and that general KHTML development has proceeded.\n\n* The project called \"KHTML2\", kdenonbeta/khtml2, is pretty much dead as far as I know. It was an effort to put KHTML on top of KDOM. Carewolf(Allen, the CSS hacker) have expressed it's no way that's going into kdelibs. It's also in general rather old and obsolete and not into the loop of recent developments.\n\n* When Apple was in the early stages of opening up their repository and acquiring SVG support by using KSVG2, they ported needed parts from KDOM into Webkit(IIRC) and the two bases were mutually synced for some time at the beginning. But KDOM as a whole is not in Webkit. However, the code bases are quite similar in this area so KSVG2 operates directly on top of Webkit.\n\nAll this is very confusing as one can tell, and I many times think it's rather unproductive because of all the projects in action where little synchronization and planning is taking place. In several cases decisions have been made which has down right hurt other projects.\n\nAnd of course, I can be wrong above or have missed something, it's not easy to track all this :)\n\n\nFrans"
    author: "Frans Englich"
  - subject: "General question: relationships"
    date: 2006-07-12
    body: "Sorry, but I don't get the bigger picture about the relationship between KHTML, KJS, WebKit, Unity etc.\nCould someone please explain, what relies on what, and where the distinct parts are used? (A link would do as well.)"
    author: "Andy"
  - subject: "Re: General question: relationships"
    date: 2006-07-12
    body: "A long time ago Apple forked KHTML and KJS, creating WebKit. Since then both codebases has seen lots of development, some code they share and in other areas they differ. In short Unity is a project trying to merge the two again, making a more unified codebase."
    author: "Morty"
  - subject: "Didn't the same guys port Gecko to Qt?"
    date: 2006-07-12
    body: "Some time ago there were great announcements of a Qt-based Firefox but nothing happened about this exciting project. I guess we will unfortunately experience the same now :-( \nI am happy if you prove me wrong..."
    author: "Henning"
  - subject: "Re: Didn't the same guys port Gecko to Qt?"
    date: 2006-07-12
    body: "One of the reasons why the gecko/firefox/Qt port failed was the stubbornness\nof the mozilla ppl wrt giving cvs write access to some kde devs.\n\nc.f.:\nhttps://bugzilla.mozilla.org/show_bug.cgi?id=265484\n\n\nThis eventually made the interest in a Qt-port of firefox die, unfortunately...\n\nProbably the fear that a 100% Qt-port would be a much better experience than\nthe default gtk2-port was one of the reasons..."
    author: "ac"
  - subject: "Re: Didn't the same guys port Gecko to Qt?"
    date: 2006-07-12
    body: "Actually:\nhttps://bugzilla.mozilla.org/show_bug.cgi?id=297788\n"
    author: "ac"
  - subject: "Re: Didn't the same guys port Gecko to Qt?"
    date: 2006-07-12
    body: "That is an amazing bug report."
    author: "Ian Monroe"
  - subject: "Re: Didn't the same guys port Gecko to Qt?"
    date: 2006-07-12
    body: "And here was I thinking it was another OMG-Qt-is-not-truly-Free kind of issue.\n\nI\u0092d love to have a Qt \u0092fox\u0085"
    author: "Ralesk"
  - subject: "Re: Didn't the same guys port Gecko to Qt?"
    date: 2006-07-12
    body: "I love their word \"bureaucracity\". It sums up the situation nicely."
    author: "Brandybuck"
  - subject: "Re: Didn't the same guys port Gecko to Qt?"
    date: 2006-07-12
    body: "It looks like the mozilla 'dudes' require some 'respect' from Dirk. Dirk hasn't proved that he is a capable hacker. There is no evidence that he knows anything about HTML, or web browsers in general. Maybe he is a nutter who would go apeshit if given CVS access?\n\nHey, wtf I am talking about?!\n\nThey are refusing to give access to a top khtml hacker, who knows more about good code than those muppets do (judging by the state of gecko), because they fear he might change the button ordering. OMG.\n\nThe fact is Mozilla is entrenched with GNOME 'supporters'. The code has significantly dipped in quality over the last year, and now I know why.\n\nThankfully Dirk et al have given up on the gecko port and are instead trying to unify webkit/khtml. Much better codebase and hopefully there will be less GNOME 'tards to deal with."
    author: "ac"
  - subject: "I don't see the problem!"
    date: 2006-07-13
    body: "Actually I don't see any problems here. Those people never said that they won't give out a CVS or SVN account to Dirk or Zack, all they asked for is to file in some patches to bugzilla so the officials can verify them and then commit them. Later on they promised to give out the asked accounts.\n\nBut then on the other hand... QT support was basicly non existing in Mozilla or the codes have been that old that nothing could be used anymore... So technically nothing could go worse if they would have granted CVS access to Dirk and Zack to maintain this part.\n\nThe most Vocal guy on that list had a NOVELL brand in his name...."
    author: "Sashimi"
  - subject: "Re: I don't see the problem!"
    date: 2006-07-13
    body: "The point is that Dirk is already a proven hacker.\n\nThe guy with the Novell brand in his name was offering to help make it happen.\n\nCause anyways...\nDEVELOPERS DEVELOPERS DEVELOPERS DEVELOPERS\nSeems like the Firefox project was acting like it was a privilege to work on Firefox, when really its the project that is privileged to have developers. :)"
    author: "Ian Monroe"
  - subject: "Re: I don't see the problem!"
    date: 2006-07-14
    body: "\"The most Vocal guy on that list had a NOVELL brand in his name....\"\n\nThe guy who had NOVELL was actually offering to help.\n\n"
    author: "Dick"
  - subject: "fork?"
    date: 2006-07-15
    body: "\"They are refusing to give access to a top khtml hacker..\"\n\nWhy not just fork out?"
    author: "pharaoh"
  - subject: "Re: Didn't the same guys port Gecko to Qt?"
    date: 2006-07-13
    body: "The KDE hackers haven't been shown to write good code despite insisting on perfection. . .Which is it? :-p"
    author: "sundher"
  - subject: "Gnustep"
    date: 2006-07-12
    body: "Are there other tools like Gnustep possible to get integrated in order to reimplement the full Apple API in KDE. This would mean that we could use native Apple Webkit and could leave development to Apple."
    author: "gerd"
  - subject: "Re: Gnustep"
    date: 2006-07-16
    body: "We NOW can do that. Webkit is frontend independent. We just need to maintain this Qt frontend, which should be really simple."
    author: "blacksheep"
  - subject: "silly questions"
    date: 2006-07-12
    body: "hello guys, \nI am a newbie, and like newbie I like to read the commit-digest, can someone explain to me the meaning of BUZZ, and what is the difference between trunk and brunch, I know that the stable version is KDE3.5.X and the developpement one is KDE4.\n\nand for the next kde ( aka kde4) are you going to split at last the file manager from the web browser( konqueror)\n\nthanks"
    author: "morphado"
  - subject: "Re: silly questions"
    date: 2006-07-12
    body: "Where did you read the word BUZZ? Buzz is usually used to express that a lot of noise is made around something.\n\ntrunk is the latest and greatest of something in development.\nbranch (I guess you didn't mean brunch) is a, well, development-branch of something else. For example, in KDE we have a branch for KDE 3.5.x and KDE 3.4.x while KDE4 is developed in trunk."
    author: "AC"
  - subject: "Re: silly questions"
    date: 2006-07-12
    body: "Hi,\n\"Buzz\" is the name I give to a rating that I generate from several components, including commit activity, mentions on webpages, and short-term news and discussion (eg. blogs). These are added together to produce the score. Of course, the scores are not completely scientific, and are meant as a way to measure \"popularity\" relative to the other calculated scores.\n\nThanks,\nDanny\n\np.s. Glad to hear that you enjoy reading the digest. :)"
    author: "Danny Allen"
  - subject: "What role is Apple playing in Unity?"
    date: 2006-07-12
    body: "..well, subject says it all. Are they funding developers? Do they activly work on Unity? Or are they just taking what comes out of it...? Neither of these?"
    author: "Psychotron"
  - subject: "Re: What role is Apple playing in Unity?"
    date: 2006-07-12
    body: "It is entirely a KDE project right now.  If we use this library in KDE, it will be a joint effort with Apple, Nokia, and others."
    author: "George Staikos"
  - subject: "Re: What role is Apple playing in Unity?"
    date: 2006-07-16
    body: "Apple won't of course take what comes out of this because they couldn't care less about a Qt frontend for Webkit. But this will end up benefit them as well cause we can now ditch KHTML in favor of Webkit and focus on only one codebase."
    author: "blacksheep"
  - subject: "separate browser ?"
    date: 2006-07-14
    body: "call it unity, call it ksafari or kafari, but please release some new browser using WebKit. I don't know if its a good idea for KDE to switch from KHTML. but its definitely a good idea to bring a new WebKit based browser for Linux, BSD and Windows.\n\nthanks\n\n"
    author: "somekool"
  - subject: "Re: separate browser ?"
    date: 2006-07-14
    body: "I would call it TourBus. Less exotic than a Safari, but available in more places ;)"
    author: "Carewolf"
  - subject: "Re: separate browser ?"
    date: 2006-07-16
    body: "LOL"
    author: "superstoned"
  - subject: "Re: separate browser ?"
    date: 2007-12-08
    body: "Konqueror will be ported to WebKit. No new browser will be made."
    author: "Michael"
  - subject: "Re: separate browser ?"
    date: 2007-12-08
    body: "Incorrect in multiple ways.\n"
    author: "SadEagle"
  - subject: "Khtml loveletter "
    date: 2006-07-17
    body: "Konqueror is \"my\" browser for ages now, using it every day. I am developing web apps using konqueror first, checking other browsers later. I am convinced by its good-naturedness. I have seen other browsers fail in obscure ways, even firefox, even safari, where khtml just works fine. Note, this is addressed to khtml, not kjs. \n\nDevs out there, keep up the good work, but don't mess things up, please. From a user's perspective, I have the impression that khtml is just fine as it is, but javascript and dom needs to catch up. So it seems as though merging with webkit is too radical as a cure. Issues can be pin-pointed quite precisely and they are already in the bugs database, I think. Don't pull out the merge sledgehammer as \"the next logical step\".\n\n\n"
    author: "micha"
  - subject: "Re: Khtml loveletter "
    date: 2006-07-18
    body: "The few big things that make it hard is: \n Rich text editing, which is very hard and something WebCore has\nand:\n The same bugs as WebCore, we are assumed by websites to act the same or not detected at all. We need to be detected and support the same bugs as what we are detected as."
    author: "Carewolf"
  - subject: "WebKit is Apple not KHTML"
    date: 2006-08-14
    body: "\"WebKit is a derivative of the KHTML engine developed by Apple Computer Inc.\"\n\nThis sentance is badly worded - it makes it sound like Apple wrote KHTML. Isnt this more correct? :\n\n\"Apple Computer Inc's Webkit is a derivative of the KHTML engine developed by KDE programmers\"\n\nJust my 5c..."
    author: "Tim Sutton"
  - subject: "Last info"
    date: 2006-10-28
    body: "Since Oct-23 2006 Unity is no more on branches/work (deleted on commit 598456), but it has been imported on the official Apple repository! See http://webkit.org.\nCongratulations to everyone involved, big things can now happen. ^_^\n"
    author: "epo"
  - subject: "Using Unity on non KDE platforms"
    date: 2006-11-03
    body: "This is a fabulous development! We have a large Qt based app that runs on Linux (GNOME) and Mac OS X, and we're looking for a solution to embed a webbrowser in that app. I'm wondering if Unity/WebKit is the answer. Specifically:\n\n - Can Unity run on a Linux GNOME desktop? If so, does it require running a bunch of KDE services as root first (I've compiled everything up under GNOME but am having troubles running testunity), and \n\n - Can you use the new WebKit sources to embed a webbrowser in a Qt app on Mac OS X?\n\nIf there's a more appropriate forum to ask these questions then just let me know! Thanks.\n\nMartin."
    author: "Martin Reddy"
  - subject: "Re: Using Unity on non KDE platforms"
    date: 2006-11-04
    body: "> Can Unity run on a Linux GNOME desktop?\n\nAny Qt or KDE program can run on a GNOME desktop, so why wouldn't a program using Unity?\n\n> If so, does it require running a bunch of KDE services as root first\n\nKDE never requires any service to be run as root\n\n> If there's a more appropriate forum to ask these questions then just let me know!\n\nhttp://webkit.org/contact.html"
    author: "Kevin Krammer"
---
Today the KDE team announces a new project to re-synchronize our HTML engine,
<a href="http://www.khtml.info">KHTML</a>, with the <a href="http://webkit.opendarwin.org/">WebKit</a> engine.  Code named Unity, the project has so far focused on porting the WebKit engine to Qt 4 with minimal changes to the existing code-base.  WebKit is a derivative of the KHTML engine developed by Apple Computer Inc.























<!--break-->
<div style="float: right; border: thin solid grey; padding: 1ex; margin: 2ex"><a href="http://static.kdenews.org/jr/unity-kde.png"><img src="http://static.kdenews.org/jr/unity-kde-wee.png" width="250" height="232" /></a><br /><a href="http://static.kdenews.org/jr/unity-kde.png">Unity-Demo showing the KDE Website</a></div>

<p>The initial work for this project was done by four KDE core developers at the
<a href="http://dot.kde.org/1151271635/">KDE Four Core meeting</a> last week in Trysil, Norway.  The contributors were
Dirk Mueller, Zack Rusin, Simon Hausmann, and George Staikos.  The project also builds on lead-up work done over the past year by George Staikos and Maksim Orlovich, which synchronized the KDE and WebKit Javascript engines.</p>

<p>At this stage Unity is a research project to determine the feasibility of
merging much of the KHTML work done over the past few years into WebKit.  This
will provide us a means to synchronizing our engines.  There are no concrete
plans to replace KDE's current KHTML component, which is also used by <a href="http://konqueror.kde.org">Konqueror</a> to render HTML pages, with Unity.  Any such decision will be left to the KHTML and KDE core
development teams in the upcoming months. It is dependent on many factors such
as our ability to keep the engines synchronized over time, our ability to
produce a high-performance, stable, and complete KPart, our level of comfort
with the new code-base, and our ability to come to a suitable working
arrangement with the other WebKit contributors.</p>

<div style="float: left; border: thin solid grey; padding: 1ex; margin: 2ex"><a href="http://static.kdenews.org/jr/unity-google.png"><img src="http://static.kdenews.org/jr/unity-google-wee.png" width="250" height="179" /></a><br /><a href="http://static.kdenews.org/jr/unity-google.png">Unity-Demo rendering Google</a></div>

<p>With respect to the technical work, our efforts have resulted in a Qt 4 based WebKit library that is able to render a variety of web pages quite nicely. There is still a considerable amount of work to do before it can be considered a complete browser engine on our platform but the basic foundations are complete.  The KDE build system, cmake, is integrated into the WebKit sources, rendering uses Qt's graphics facilities, and a simple test driver has been developed.  A KDE layer will be added to integrate the engine with the desktop facilities provided by KDE as well as creating a KPart which can be loaded by any KDE application requesting a handler for HTML.</p>

<p>Anyone wishing to join the effort should contact the developers on the
<a href="https://mail.kde.org/mailman/listinfo/kfm-devel">kfm-devel mailing list</a>.  Source code is accessible in KDE's <a href="http://websvn.kde.org/branches/work/unity/">Subversion repository</a>.</p>

<p>For press queries about this and other KDE announcements see the <a href="http://www.kde.org/contact/representatives.php">KDE press contacts</a> page.</p>
<br clear="all" />



