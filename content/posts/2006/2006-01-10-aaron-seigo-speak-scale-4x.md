---
title: "Aaron Seigo to Speak at SCALE 4x"
date:    2006-01-10
authors:
  - "irabinovitch"
slug:    aaron-seigo-speak-scale-4x
comments:
  - subject: "\"work-flow\" interfaces"
    date: 2006-01-12
    body: "My main interest these days is RIAs(Rich Internet Applications), and specifically Flash.  I've been playing around with the latest alpha Flex 2 and Flex Builder 2 (Zorn), and have been very impressed.  I have a traditional fat client background (SWT, MFC, gtk+...), but when I look at the concept of a view state in Flash, I've been convinced this is the future of ALL applications.  I think the theory is that you don't want to overload your user with extraneous information, so that like in DHTML (no, i won't use the word AJAX), your making your div appear and disappear with various content.  I could expand on this, but I'm kind of tired right now."
    author: "Rick"
  - subject: "Re: \"work-flow\" interfaces"
    date: 2006-01-12
    body: "Theres a few fundamental flaws with RIAs, the first one being is absolute vendor lock-in.  In a theoretical example say you use 'Web Office Pro' for all your word processing needs.  What would you do if Web Office Pro's development grinds to a halt and contains serious bugs?  You would have to rely on the vendor to give you some sort of migration path to one of their competitors, which would probably mean saving each and every file you made using Web Office Pro to your local computer, and then subscribing with another company and uploading everything back online (and hoping that one can read the other's documents).\n\nThe second one is say you travel around the country and/or internationally a lot, and you have a laptop.  On a train or plane you generally won't have an internet connection, so you would be unable to get online to use Web Office Pro, so you would be unable to do any work unless you had an internet connection.\n\nThe third flaw is then instead of the company getting to charge you once and then never again till you upgrade, now the companies get to charge you a subscription fee.\n\nThose 3 reasons are why I don't think RIAs should or could expand to big things (without just using marketing buzz with words like AJAX).  If anyone of my points are flawed I would like to understand why."
    author: "Corbin"
  - subject: "Re: \"work-flow\" interfaces"
    date: 2006-01-12
    body: "I think you are right. People have been talking about the web taking over the applications' world for over a decade now, and it hasn't happened.\n\nI do think that some generic services like groupware, email and the like might go the RIA more and more (see the success of efficient webmail services like GMail, it really makes sense)"
    author: "KubuntuUser(exMandrake)"
  - subject: "Re: \"work-flow\" interfaces"
    date: 2006-01-29
    body: "Well, it would be difficult to fault you, because your reasonement is of course right..\nBut it is only valid in the case you showed, there are other situations:\n-a rich client may make sense inside an enterprise (ok, it's more of a case of Rich Intranet Solution than Internet but don't underestimate the size of this market)\n-for an app where Internet is necessary. The point two is not anymore a limitation then."
    author: "renox"
  - subject: "What's old is now new"
    date: 2006-01-12
    body: "NeXT was all about Workflow interfaces and Services to build upon apps which provided an atmosphere of complimentary applications that collaborated and extended the power of the workflow by offering Services.\n\nI'd definitely find this a plus in KDE 4. I hope it is as well designed in the UI as it is in NeXTSTEP/Openstep."
    author: "Marc J. Driftmeyer"
  - subject: "Re: What's old is now new"
    date: 2006-01-13
    body: "I don't think NeXT was about Workflow interfaces. To me that means being able to set up a kind of 'state machine' where one activity like ocr'ing a document would trigger another event like sending an email to a supervisor. That has always been specialist software and never built in to any mainstream desktop environment including NeXT.\n\nKDE does have services already - have a look under KDEDIR/share/services and you will see lots of files ending in .desktop. What it doesn't have is a nice services menu that all apps have, along with the concept of the AppKit's 'First Responder' which allows services to interact with the 'currently selected thing'. On the other hand KDE's KParts are like NSBundles on steriods, and nearly all KDE apps are constructed with KPart components, just like NeXT or Cocoa apps are made up of bundles. In my opinion the KDE framework is the only one to equal the AppKit, better in some ways and not as good in others. \n\nI don't think drag and drop works as well as it did in OpenStep because of limitations of X-Window, and I do find using KDE to feel more modal because it doesn't have the same ubiquitous drag and drop. Unfortunately, most people assume drag and drop is hard to use, because they've never tried a decent implementation."
    author: "Richard Dale"
  - subject: "Re: What's old is now new"
    date: 2006-01-13
    body: "\"In my opinion the KDE framework is the only one to equal the AppKit, better in some ways and not as good in others.\"\n\nCould you explain this a little further? It`s interesting.\n"
    author: "reihal"
  - subject: "Re: What's old is now new"
    date: 2006-01-13
    body: "Well one example is NSBundles vs. KParts, they both are wrappers around some dynamically loaded code. As bundles are written in Objective-C, they can contain 'categories' which allows you to add or override methods in an existing class, and even already loaded instances will be able to use the new methods. On the other hand, KParts are located by the KTrader mechanism which has a powerful and flexible query language to search for resources matching the specified criteria. The search mechanism for bundles is simpler and not as powerful, it just allows you to specify directories to look in for a named bundle.\n\nTo connect components KDE uses signals and slots, and the AppKit uses outlets and actions. A signal can have multiple arguments, and it can be connected to multiple slots. An action can only have one argument, and it can only be connected to a single outlet. On the other hand, Interface Builder is much better than the Qt3 designer and a still a bit better than the Qt4 one. It is less modal and allows you to instantiate an instance and connect things to it, and then the instance gets archived out into the .nib file, and reconstituted when you load the .nib file in your app.\n\nKDE uses DCOP to allow apps and scripts to communicate with each other, whereas the AppKit uses Distributed Objects. DO is more complicated and allows you to do more, but DCOP is simpler to use especially from non-C++ languages.\n\nKDE is based on C++, and Cocoa is written in Objective-C a more dynamic language which doesn't suffer from binary incompatibility (BIC) problems. On the other hand Objective-C has an unusual method/argument naming syntax taken from Smalltalk. This makes Objective-C code easier to read than C++, but also makes it harder to get a nice api from bindings to non-Smalltalk like languages. In theory Objective-C should be slow, but in practice large Objective-C AppKit apps have excellent performance - much, much better than java for instance.\n\nThe AppKit has always had a floating point coordinate system with an alpha-channel, something that Qt has only got with Qt4 and KDE won't have until KDE 4. The 3D architecture of Mac OS X Cocoa is a long, long way ahead of any competitor, but Apple haven't made all that much use of it so far.\n\nIn some ways Mac OS X isn't as good as NeXT was. Scrollbars are no longer on the left where they belong, no tear off menus, no space saving vertically stacked top level menus, no shelf on the Finder and no equivalent to minimising windows to a postage stamp size (which was really handy). \n\nOh well, at least things aren't going backwards like they did with Java Swing, and we getting back to where we were for desktop development productivity 10 years ago now with KDE.."
    author: "Richard Dale"
  - subject: "Re: What's old is now new"
    date: 2006-01-15
    body: "I've anything to say, but to compliment you on the most informative post I've seen for a good while. ;)"
    author: "blacksheep"
  - subject: "No screenies - thats a killer"
    date: 2006-01-13
    body: "That blatant 'no' answer to the screenie was a killer. Remember there are thousands (millions?) of excited KDE junkies out here dying to keep track of how kde4 develops without neccessarily wanting to build svn ourselves......\n\nSo be kind to us an give us a regular eyecandy fix! :-)\n\nKeep up the good work!"
    author: "Tim Sutton"
---
<a href="http://aseigo.blogspot.com/">Aaron Seigo</a> will be presenting at <a href="http://www.socallinuxexpo.org/">SCALE 4x</a>, the 2006 Southern California Linux Expo on February 11-12 in Los Angeles.  His presentation will cover the next KDE release and how the <a href="http://plasma.kde.org">Plasma</a> project is looking to reinvigorate the desktop experience by centering the desktop on workflow-centric interfaces.   Additionally KDE will have a booth on the expo hall floor.  SCALE 4x's discounted <a href="http://www.socallinuxexpo.org/order/order.html">early bird registration</a> is open until January 15, 2006.  For 40% off use the promo code "KDE".


<!--break-->
