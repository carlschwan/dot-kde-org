---
title: "KDE Commit-Digest for 8th October 2006"
date:    2006-10-09
authors:
  - "dallen"
slug:    kde-commit-digest-8th-october-2006
comments:
  - subject: "Bugs"
    date: 2006-10-08
    body: "- link to Visual Changes don't work\n- using a localized version, the Commits line in Statistics is broken\n\nthanks as always, danny!"
    author: "AC"
  - subject: "Re: Bugs"
    date: 2006-10-08
    body: "Just fixed the visual changes files, and the localisations will be broken for a few days, as i've had to make some radical changes to support some cool new features - look out for them possibly next week.\n\nThanks,\nDanny "
    author: "Danny Allen"
  - subject: "Talking about bugs..."
    date: 2006-10-08
    body: "is there any way to view the new KDE web pages including the digest in Internet Explorer? I use 6.0 here, kde.org's menu is far below the text and the design of the digest is, well, completely messed up... "
    author: "sebastian"
  - subject: "Re: Talking about bugs..."
    date: 2006-10-08
    body: "now i dont know any of this, but i would bet that you have hit several bugs in IE."
    author: "redeeman"
  - subject: "Re: Talking about bugs..."
    date: 2006-10-11
    body: "Actually, W3C finds that there are two errors in the page:\n\n1. Error Line 4844 column 5: end tag for \"TABLE\" omitted, but its declaration does not permit this.\n</div>\nYou forgot to close a tag, or\nyou used something inside this tag that was not allowed, and the validator is complaining that the tag should be closed before such content can be allowed.\n The next message, \"start tag was here\" points to the particular instance of the tag in question); the positional indicator points to where the validator expected you to close the tag. \n\n2. Info Line 45 column 0: start tag was here.\n<table style=\"text-align: left; width: 99%;\" border=\"0\" cellpadding=\"0\" cellspac"
    author: "James Richard Tyrer"
  - subject: "Re: Talking about bugs..."
    date: 2006-10-09
    body: "IE 6.0 has many issues and incompatibilities and is the websites developers nightmare, though it is good to point out that the kde site doesn't work in IE6 :) It should definetely be addressed. Nonetheless, I recommend downloading Firefox for a better web experience in Windows."
    author: "Vlad"
  - subject: "Re: Talking about bugs..."
    date: 2006-10-09
    body: "or Opera :)"
    author: "AC"
  - subject: "Re: Talking about bugs..."
    date: 2006-10-09
    body: "Or iceweasel ;)"
    author: "AC"
  - subject: "Re: Talking about bugs..."
    date: 2006-10-09
    body: "why should bugs in a crappy application be the worries of kde developers? why should microsofts shit-on-people behavior be rewarded?"
    author: "redeeman"
  - subject: "Re: Talking about bugs..."
    date: 2006-10-09
    body: "<i>why should bugs in a crappy application be the worries of kde developers?</i>\n\nBecause it's installed on all those 80% of world computers we targeting. Before we move in, we, at least, have to look presentable."
    author: "Daniel \"Suslik\" D."
  - subject: "Re: Talking about bugs..."
    date: 2006-10-10
    body: "When I produced the digest, the vast majority of browsers were Konqueror, followed second by mozilla/firefox, a sizeable number of safari, and a very small number of IE. Fewer IE hits than serverlog spams.\n\nDanny's 80% is konq/safari.\n\nDerek"
    author: "dkite"
  - subject: "Re: Talking about bugs..."
    date: 2006-10-10
    body: "Well, I would say for the digest IE compatibility is not important (*imo* most people loking at this already have KDE or will know that IE sucks) but for the kde.org homepage I think it is an issue."
    author: "Simon"
  - subject: "Re: Talking about bugs..."
    date: 2006-10-09
    body: "Just install http://dean.edwards.name/IE7/ for the poor users that have to use internet explorer."
    author: "testerus"
  - subject: "screenvids"
    date: 2006-10-08
    body: "How is it possible to make screen recordings from KDE4? I assume that termininal solutions will enable to pipe the visual stream in a video file and then ad sound and so on but who knows how to do it.\n\nScreenshots are great but its often more important to demonstrate functionality. What I would love to see more video tutorials. Video tutorials are part of the ruby success. Now that Google started with Google video a great streaming service or Youtube and others offer hosting the methods don't adapt yet to the new situation.\n\nPodcasting, Video, it is a pity that Open Source lags so much behind in the fields which drive internet development to the next stage. I am very glad the Phonon offers a solution to consolidation of multimedia infrastructure.\n\n"
    author: "bert"
  - subject: "Re: screenvids"
    date: 2006-10-09
    body: "It's simple, we lack the manpower to produce those video, we barely have the manpower for producing text manuals. But if anyone wants to start to produce such videos, then I am sure we can come up with a solution on how to distribute them to the KDE users."
    author: "Cyrille Berger"
  - subject: "Re: screenvids"
    date: 2006-10-09
    body: "Well I guess a video-producing tool should be simple to use.\n\nSo the next time you as a dev, use it, just record that :-)"
    author: "shev"
  - subject: "Re: screenvids"
    date: 2006-10-09
    body: "You're wrong that it's just a matter of clicking \"record\" button to get an acceptable result. The task requires some sort of scenario and postprocessing. I do that sometimes using a proprietary tool (because it's cheaper for me than playing with vnc streams), but it's obviously task for other contibutors, not for core devs, so let's not even try to waste their time asking them to do that.\n\n"
    author: "Jaroslaw Staniek"
  - subject: "Re: screenvids"
    date: 2006-10-10
    body: "But tools do you use?\n\nA video camera next to your keyboard which records what you do?\nOr real screen recording?\n\nWhat production tools are available for Linux?"
    author: "heiner"
  - subject: "Re: screenvids"
    date: 2006-10-10
    body: "You can use free as in beer Wink - http://www.debugmode.com/wink/\nIt's portable, even of course I hope there will be a good F/OSS editing tool one day.\n\nHowever so far in most cases I use Camtasia Studio that's purchased (in the company I work for) to produce commercial tutorials.\n"
    author: "Jaroslaw Staniek"
  - subject: "ScreenKast and Captorials.com"
    date: 2006-10-10
    body: "Check out ScreenKast and Captorials.com:\n\nhttp://www.captorials.com/\nhttp://www.kde-apps.org/content/show.php?content=41938\n"
    author: "AC"
  - subject: "Confusion about KBoard"
    date: 2006-10-09
    body: "Just for clarification: KBoard is _not_ a game canvas, but a generic board game application which _uses_ a lightweight canvas by Maurizio Monge, (temporarily) dubbed KGameCanvas.\nScreenshots can be found here: http://kboard.sourceforge.net.\n"
    author: "Paolo Capriotti"
  - subject: "Python vs Ruby"
    date: 2006-10-09
    body: "Well, I dont want to start a flamewar of course, but from a standpoint of experience, I think Python would be the better default. A lot more people know Python because it's older and it is used in a lot more different area's. I use it, for example in sysadmin automation. I don't know Ruby, so I don't know how well Ruby can do similar tasks, but i'm a lazy guy. I want to learn and use one scripting language wich is as versatile as possible and after some investigation back then, my choice was Python. Ruby did not exist back then."
    author: "Fred"
  - subject: "Python vs Ruby"
    date: 2006-10-09
    body: "Well, I dont want to start a flamewar of course, but from a standpoint of experience, I think Python would be the better default. A lot more people know Python because it's older and it is used in a lot more different area's. I use it, for example in sysadmin automation. I don't know Ruby, so I don't know how well Ruby can do similar tasks, but i'm a lazy guy. I want to learn and use one scripting language wich is as versatile as possible and after some investigation back then, my choice was Python. Ruby did not exist back then."
    author: "Fred"
  - subject: "Re: Python vs Ruby"
    date: 2006-10-09
    body: "Nobody developing Ruby RAD support for KDE wants a default language chosen, and we think Python and Ruby should both be first class citizens in KDE4. Choose whichever you prefer! \n\nThe discussion on kde-core-devel about a default language is being led largely by people who haven't contributed to kdebindings, kdevelop or scripting support in koffice apps, and don't know either python or ruby very well."
    author: "Richard Dale"
  - subject: "Re: Python vs Ruby"
    date: 2006-10-09
    body: "I think this is a case of whomever writes the code gets to decide. Thats why its important to make it as easy as possible for multiple scripting languages to coexist. (That's what open source is all about :) \n\nReally, it's all about having a solid based to work off of which I hope Kross turns out to be... It's already looking pretty good... without too much of the LCD dragging it down.\n\nCheers,\nBen\n\n\n"
    author: "BenAtPlay"
  - subject: "Re: Python vs Ruby"
    date: 2006-10-10
    body: "I completely agree on this, Ruby and Python should both be first class in KDE 4. I don't care about Javascript."
    author: "Isaac Clerencia"
  - subject: "Re: Python vs Ruby"
    date: 2006-10-11
    body: "I think everyone who works on PyKDE feels the same way. I don't want favored status for Python any more than I do for Ruby. Personally, I don't even want any major part of KDE (outside of kdebindings) to have dependencies on Python or any other language other than C++ - it isn't necessary IMO, and it seems to piss some people off. \n\nBasically, there are a couple minor things that would make KDE easier to write scripting tools for that nobody has mentioned in any of the discussions. Otherwise, if someone wants to write a major app in Python, Ruby, Lua or Z80 Assembler (my personal favorite - like a lot of people, I could code in that in hex), then stick it in kdebindings or some place else where the people who don't want additional language dependencies don't have to have them, and the people who want them can have them. Seems simple to me.\n\nThe rest is just getting people to do the work of writing the interfaces/wrappers/bindings. I think it's mostly a bootstrap problem - the people who would love to use that stuff don't want to/can't write it, and most of the people who can write it don't see any use for it.\n"
    author: "Jim Bublitz"
  - subject: "Re: FORTRAN vs Java"
    date: 2006-10-09
    body: "Well, I dont want to start a flamewar of course, but from a standpoint of experience, I think FORTRAN would be the better default. A lot more people know FORTRAN because it's older and it is used in a lot more different area's. I use it, for example in sysadmin automation. I don't know Java, so I don't know how well Java can do similar tasks, but i'm a lazy guy. I want to learn and use one programming language wich is as versatile as possible and after some investigation back then, my choice was FORTRAN. Java did not exist back then.\n\n[Note: No flamewar intended here either. Just couldn't resist. Apologies in advance to Fred.]"
    author: "Derf"
  - subject: "Re: FORTRAN vs Java"
    date: 2006-10-09
    body: "A good FORTRAN programmer can write FORTRAN code in any language..."
    author: "Darkelve"
  - subject: "Re: FORTRAN vs Java"
    date: 2008-01-11
    body: "A Good JAVA programmer, never need FORTRAN for nothing, jejeje."
    author: "Java Student"
  - subject: "Re: Python vs Ruby"
    date: 2006-10-09
    body: "I personally know python, and not Ruby.   I like Python.    I personally know and respect several programmers who only know Ruby and like it.   I know a few programmers who know both well, and they tend to like both (with one being prefered, but there is no pattern for which is prefered).\n\nI know no programmer who still likes Perl after learning either Ruby or Python.   (They will still user perl for 1 liners because sometimes perl can do in one like what Ruby/Python will need 10, but for anything more than 2 lines of perl they will use Python/Ruby because they are better)  Same goes for most other scripting languages - people still use them, but when they use Ruby or Python when there is a choice.\n\nWith that in mind, I'm willing to learn Ruby when I need to.   If you are a programmer you will find it easy to learn both Ruby and Python as the need comes.\n\nExcept for Lisp programmers, once you grok Lisp you never want anything else, but few programmers grok Lisp."
    author: "bluGill"
  - subject: "Ruby's syntax too similar to Perl"
    date: 2006-10-10
    body: "R"
    author: "AC"
  - subject: "Ruby..."
    date: 2006-10-10
    body: "...is a  great language. If you dont know it already I recommend looking at it now. I havent yet worked much in either Python nor Ruby yet so I'm quite neutral. I never liked the Python indentation thing. YMMV though, some say it's actually quite useful.\nRuby is worth a closer look IMHO because the syntax is clean and very powerful. You can do things you always dreamed of doing without the language getting in the way. Ruby is a programming language made from a real programmer without any ideologic limitations. As a PHP programmer I found it very interesting reading about Ruby syntax. BTW: I hope PHP-QT/KDE will finally become a reality. PHP is one of the world's most wide-spread scripting languages. Support for KDE programming would be great. Before anyone whines here: This does not mean reduced support for Python or Ruby but added support. I happen to like PHP quite a bit.\n"
    author: "Martin"
  - subject: "Re: Ruby..."
    date: 2006-10-10
    body: "> Ruby is a programming language made from a real programmer without any ideologic limitations.\n\nsorry, I don't understand you... what ideologic limitations have other language programmers???"
    author: "ZeD"
  - subject: "Re: Ruby..."
    date: 2006-10-10
    body: "\"any ideologic limitation\" referred to the language itself not to the programmers using it. Some \"barriers of the mind\" in PHP for example:\n\n* PHP developers decided to never implement operator overloading. This is in a way ideologic (I'm not saying good or bad ideology here) because you wouldnt have to use it. So they are doing it because they think they must teach you how to write good applications.\n\n* 2nd example:\nforeach ($array as &$item) {\n  $item=3;\n}\n$item=4;\n\nAfter running this all array elements will have changed to 3. Right? Wrong. Somehow PHP devs decided that it's cool that $item will live on as a reference to the last element outside the loop. This renders this type of loop completely useless.\n"
    author: "Martin"
  - subject: "Re: Ruby..."
    date: 2006-10-10
    body: "> Somehow PHP devs decided that it's cool that $item will live on as a \n> reference to the last element outside the loop. This renders this \n> type of loop completely useless.\n\nEh? No it doesn't. It just makes a piece of code such as the one you suggest not work - you can still use that loop type, just don't expect to be able to reuse the variable afterwards."
    author: "Paul Eggleton"
  - subject: "KPhotoAlbum"
    date: 2006-10-10
    body: "KPhotoAlbum naming sucks, anglosaxon takeover..."
    author: "pops"
  - subject: "Stop using K in all applications!"
    date: 2006-10-11
    body: "Why not stop using the letter K in all applications in KDE4?\nWould it be too hard to find the applications otherwise?\nI already see Phonon (not Kphonon) and plasma (not Kplasma) so why not continue with the rest?"
    author: "David"
  - subject: "Re: Stop using K in all applications!"
    date: 2006-10-11
    body: "Phonon and Plasma are not applications, they are infrastructure. I haven't yet noticed a trend in applications to do away with 'K' based puns etc."
    author: "Richard Dale"
  - subject: "Re: Stop using K in all applications!"
    date: 2006-10-11
    body: "No they are not applications but still no k in the names.\nI'm just wondering if people still want the k's in KDE applications?\nOr Am I the only one who thinks it looks a bit silly?!\n\nThe K doesn't make it a better application :)"
    author: "David"
  - subject: "Re: Stop using K in all applications!"
    date: 2006-10-11
    body: "\"The K doesn't make it a better application :)\"\n\nNor does 'i', 'win' '32', 'g', 'q', or 'e'.  Its just a branding thing pretty much."
    author: "Corbin"
  - subject: "Re: Stop using K in all applications!"
    date: 2006-10-11
    body: "That's true, I think good applications will always find their way to the users without the need for k-branding! Like Amarok, ManDVD.\n"
    author: "David"
  - subject: "Re: Stop using K in all applications!"
    date: 2006-10-11
    body: "You mean like amaroK , right :) ?\n"
    author: "Lubos Lunak"
  - subject: "Re: Stop using K in all applications!"
    date: 2006-10-11
    body: "it's Amarok now... ;-)"
    author: "superstoned"
---
In <a href="http://commit-digest.org/issues/2006-10-08/">this week's KDE Commit-Digest</a>: KBoard, a game canvas, gets several new chess-based themes, whilst KSokoban gets many new levels. <a href="http://kphotoalbum.org/">KPhotoAlbum</a> imports the winning entry from its Splashscreen Contest. Krazy and apidox (parts of the <a href="http://www.englishbreakfastnetwork.org/">EBN test suite</a>) move from playground into the kdesdk module. KBlog, a library to interface with various blogs, is imported into the PIM playground in KDE SVN. Work begins on a <a href="http://gstreamer.freedesktop.org/">GStreamer</a> backend for <a href="http://phonon.kde.org/">Phonon</a>. More work on <a href="http://chat.yahoo.com/">Yahoo Chatroom</a> support in <a href="http://kopete.kde.org/">Kopete</a>. <a href="http://www.kexi-project.org/">Kexi</a> Query Designer supports data sorting in design and SQL view. Painting experiments with Chinese brushes in <a href="http://www.koffice.org/krita/">Krita</a>.
<!--break-->
