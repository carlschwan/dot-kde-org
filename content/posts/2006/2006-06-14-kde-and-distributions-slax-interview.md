---
title: "KDE and Distributions: SLAX Interview"
date:    2006-06-14
authors:
  - "wolson"
slug:    kde-and-distributions-slax-interview
comments:
  - subject: "Using SLAX for system rescue"
    date: 2006-06-14
    body: "Personally I have been using SLAX for few years now for rescuing systems as I tend to be kinda aggressively updating my system packages, and once in a while systems tend to break, and installing older RPM, re-merging and/or reconfiguring /etc files become pretty simple and fast to do with such LiveCD as SLAX.\n\nJust too bad some of the systems I maintain have 64-bit systems on them, and as I can't chroot on them from 32-bit SLAX it leaves me no choise but to use other LiveCD for them (actually using RR64 LiveDVD for now)... So maybe there might some day me a SLAX64 (I bet this question resurfaces from time to time)?\n\nAnd of course one other major usage has been going about booting SLAX to non-expecting \"the other OS\" user's computers (giving excuses like \"I really can't use my WebMail from non-Linux system, heh). ;-)"
    author: "Nobody"
  - subject: "Wobbly?"
    date: 2006-06-14
    body: "What's he mean \"wobbly windows\"? Anybody got a URL?"
    author: "AC"
  - subject: "Re: Wobbly?"
    date: 2006-06-14
    body: "he means in other words the upcoming versions of X.org that is X7.1 which technically speaking has already been released for several months, however only three or so distributions have implemented it. In other words he is looking forward to X7.2 to be released to the end of the year, in the same time window as KDE is slated, a tad bit before perhaps.\n\nAh what the heck see for yourself go to google - kororaa"
    author: "lo"
  - subject: "Re: Wobbly?"
    date: 2006-06-14
    body: "Wobble:\nhttp://linuxfr.org/redirect/45617.html\n"
    author: "LMCBoy"
  - subject: "Re: Wobbly?"
    date: 2006-06-14
    body: "Thi XGL is really fine. I'm using it daily and my CPU is less used and memory too, but I can't use Xorg 7.1 because nvidia binary drivers still aren't compatible with it. "
    author: "MaBu"
  - subject: "WOW"
    date: 2006-06-14
    body: "According to Tomas, it seems the best thing about KDE is the look. I have to disagree about that..."
    author: "LOL"
  - subject: "Re: WOW"
    date: 2006-06-14
    body: "=)\n\nwe could just short circuit your comment quite easily with sth like:\n\nforeach (opionion)\n{\n    express(!opinion);\n}\n\nreally, it's not surprising there are people who dissent. for any given opinion. the real finesse is knowing when it matters a rat's ass to dissent publicly, which usually comes down to having an interesting reason for doing so. that can range from \"huge numbers of people agree with me\" to \"i have an interesting insight\".\n\nbut simple \"i dissent\"? *boring*"
    author: "Aaron J. Seigo"
  - subject: "a better packaging system ?"
    date: 2006-06-14
    body: "I am not sure I understand what you want, because on most distribution I have tryed, you can install kword (and most other kde apps) independentely, so I had say it's more a distribution problem than a kde problem."
    author: "Cyrille Berger"
  - subject: "Re: a better packaging system ?"
    date: 2006-06-14
    body: "I don't think you understood what he meant. Huge source packages like koffice-$VERSION.tar.gz makes difficult to create individual packages for the applications they contain, like kword. Sometimes, it would easier to have libkoffice-$VERSION.tar.gz and indivual source packages. Another example is how difficult it is to create a JuK binary package from the kdemultimedia source package.\nIs it more undestandable ?"
    author: "lezard"
  - subject: "Re: a better packaging system ?"
    date: 2006-06-14
    body: "> Another example is how difficult it\n> is to create a JuK binary package\n> from the kdemultimedia source package.\n\nIt's easy if you use svn2dist."
    author: "mart"
  - subject: "Re: a better packaging system ?"
    date: 2006-06-14
    body: "I have to agree with Tomos. I find this the most annoying aspect of KDE. With each release, the download files of both KDE and Koffice get ever bigger, as the developers add ever more features/programs. If everyone used every feature/program, that wouldn't be a problem, but they don't, so in effect it's just so much bloat. It's particularly bad with the bugfix releases: only a small number of files have actually changed, yet you have to download the whole lot, even if you only use a small number of programs - this is horribly inefficient. \n\nI look forward to an improved packaging system in KDE4!"
    author: "Peter Robins"
  - subject: "Re: a better packaging system ?"
    date: 2006-06-14
    body: "As far as I'm concerned, you won't get it for KOffice. Packaging the source tarballs for a release is done by developers, not by packagers. Packagers then package the source into as many different installation packages as they want for the distribution they work for. \n\nI, as a KOffice developer and release dude, simply don't have the time to split the KOffice source into a package for every application. Preparing a KOffice release already takes four full days: one for the tagging/packaging of the source, one for the tagging/packaging of the languages and two more for preparing the changelog, release notes, press release, coordinating with packagers, translators, news sites. \n\nIt may be inconvenient for you, but if you want to install from source, you might just as well install from a subversion checkout, and if you don't want to install from source, your distribution will provide separated packages for you. And if you're working for a distribution and want KOffice source packages delivered in separate packages (and have agreement from the other distributors that they want it that way too), you might volunteer to take over the source packaging process from the developers -- i.e, me.\n\nTime is like money: it's hard to believe that someone else hasn't more to give."
    author: "Boudewijn Rempt"
  - subject: "Re: a better packaging system ?"
    date: 2006-06-14
    body: "As for a user, if he wants to select apps he should use a distribution that allows it. If he builds it, he can use kdesvn for that. (you are not force to use trunk or to download the source for all application)\n\nAs for packagers, well downloading one tarball or a dozen of tarball, I am not sure it will be a winning situation. And if you consider the amount of work needed to release KDE and/or KOffice and that is usually done by one person, I really doubt that is worth to split KDE in smaller package. And I also heard complaints from people about xorg being split in too much packages, personnaly, I do find KDE to be well balanced with thematic packages."
    author: "Cyrille Berger"
  - subject: "Re: a better packaging system ?"
    date: 2006-06-15
    body: "I didn't know about svn2dist, I'll definitely check it out!\nWhat I meant by kword (or juk if you wish) is the following:\nI wish to compile it from sources.\nI wish to compile only kword/juk and all indispensable dependencies, nothing else.\nFor me (as a distribution packager) would be useful something like 'make juk' and 'make juk_install', for example.\n"
    author: "Tomas M"
  - subject: "Re: a better packaging system ?"
    date: 2006-06-15
    body: "The ability to only compile some part of the KDE packages has been there forever(Or at least since KDE2). You have to exclude the parts you don't want, just set DO_NOT_COMPILE=\"the ones you dont need\". \n\n "
    author: "Morty"
  - subject: "Re: a better packaging system ?"
    date: 2006-06-19
    body: "This is not working as expected, or I don't understand how to use it correctly."
    author: "Tomas M"
  - subject: "<3 Slax :)"
    date: 2006-06-14
    body: "Slax is great. It is really good as a rescue kit and it allows me to use linux on my parent's computer (BTW I noticed Slax seemed faster there than their windows XP)\n\nI was using ubuntu and after finding and trying out slax I had to switch to kubuntu. There is no better advertisement for KDE."
    author: "Vexorian"
  - subject: "Re: <3 Slax :)"
    date: 2006-06-14
    body: "May be you have not seen Tomahawk Desktop :)"
    author: "Paul"
  - subject: "Slax with KDE 3.5.4"
    date: 2006-06-15
    body: "I like Slax, it is fast and useful. I missed Slax with KDE 3.5.x series. Could you please have next version of Slax with next version of KDE ;) thanks!"
    author: "fast_rizwaan"
  - subject: "Re: Slax with KDE 3.5.4"
    date: 2006-06-15
    body: "Hey! You forgot to complain about printing quality!"
    author: "Roll, T."
  - subject: "Easy to Remaster"
    date: 2006-06-20
    body: "Slax has been one of my favorite livecds because it is fast and easy to customize.  Some friends got fed up with \"that other operating system\" and asked me to install Linux.  I installed Slax with extra applications (Slax modules) and created a livecd that was almost identical to what was installed on their desktop.  No matter how bad they trash their system, they can boot to their custom livecd and have their familiar desktop and applications.\n\nTomas - keep up the good work."
    author: "Kudos to Slax"
---
How much do you really know about all of the various distributions that
have KDE as their desktop default?  With the flexibility of KDE and our
applications, distributions are able to focus on different goals, target
different users, and develop different personalities.  Every couple of weeks, KDE Dot News will interview a distro maintainer to discuss the history of their distribution, what they are focusing on now and what their goals are in the future.  To begin with we talk to <a href="http://slax.linux-live.org/credits.php">Tomas Matejicek</a>, the
founder and maintainer of <a href="http://slax.linux-live.org/">SLAX</a>,
a LiveCD based from Slackware.



<!--break-->
<p>
The primary goal of SLAX is "to provide a wide collection of useful
software while keeping the CD image small enough to be written to a
185MB CD medium (small 8cm CD). SLAX boots directly from the CD or
USB devices and it provides a full featured Linux operating system."  You
can learn more about SLAX on <a href="http://slax.linux-live.org/features.php">their features page</a> and
download one of its versions and modules on <a href="http://slax.linux-live.org/download.php">their download page</a>.
</p>

<h2>Past</h2>

<p><strong>Can you tell us about the history of your distribution?</strong></p>

<p>
<strong>Tomas:</strong>
SLAX started back in 2002. It was just a test to make Slackware Linux
run from CD, but when several people found it useful then I decided to
start the project.
</p>

<p><strong>Why did you choose KDE and which version of KDE did you first implement?</strong></p>
<p>
<strong>Tomas:</strong>
I chose KDE because Slackware installs KDE into /opt so it was
very easy to distinguish KDE files and the rest of the system. Moreover
I like the look of KDE, in my opinion it has the best icons
available. The version of KDE was 3.1.
</p>

<p><strong>How did you find initial support for a new distro?</strong></p>
<p>
<strong>Tomas:</strong>
I didn't find any. I tried sourceforge.net, but they refused to provide
web space for SLAX. I had to setup my own server and pay for it.
</p>


<p><strong>What could KDE have done better to help new distros use KDE?</strong></p>
<p>
<strong>Tomas:</strong>
I would suggest a better packaging system. For example, when I wish to
install KWord, I must install all the rest of the KOffice package. This
is usually not any problem for end user, but for distro packagers it's
important. Nobody wants useless files.
</p>

<p><strong>What were your first impressions about KDE's documentation and community?</strong></p>
<p>
<strong>Tomas:</strong>
It's huge! :)
</p>

<h2>Present</h2>

<p><strong>How closely do your releases depend on KDE releases?</strong></p>
<p>
<strong>Tomas:</strong>
In the past, it has been very close. Nowadays it doesn't depend on it,
but it's always nice to have the latest version of KDE in SLAX, of course.
</p>

<p><strong>Do you have a clear target audience for your distro?</strong></p>
<p>
<strong>Tomas:</strong>
All users who need very small system with KDE.
</p>

<p><strong>Do you have any user feedback mechanism?  If so, what feedback do they have about KDE?</strong></p>
<p>
<strong>Tomas:</strong>
Yes, users talk to me personally by email, or by using our web-based forum.
They say only good things about KDE, they like it because it looks so
good. Moreover I choose only KDE applications for SLAX so the whole
desktop seems very coherent, all apps look the same. Users like it, of course there are still some people who suggest FluxBox :)
</p>

<p><strong>In what ways do you customise the version of KDE that ships with your distro?</strong></p>
<p>
<strong>Tomas:</strong>
I'm using Slackware's packages only for Arts, Qt, KDEBase and KDELibs.
The rest is recompiled because I want to include only some
applications, not the whole packages. For example, I include KWord,
KSpread and KPresenter in SLAX but nothing else from the KOffice package.
</p>

<p><strong>What are the biggest strengths of KDE for your distro?</strong></p>
<p>
<strong>Tomas:</strong>
The design. The icons. The amount of applications shipped with KDE.
</p>

<p><strong>What are the biggest weaknesses?</strong></p>
<p>
<strong>Tomas:</strong>
The applications shipped with KDE :) As I said, it's hard to compile
KWord alone. This should be improved IMHO.
</p>

<p><strong>What KDE applications are the most popular among your users?</strong></p>
<p>
<strong>Tomas:</strong>
I don't know. SLAX uses only KDE applications as default so it's hard to
say. I think that Kopete is very important, KPlayer (mplayer frontend)
and K3b (burning software).
</p>

<p><strong>Do you feel that you have a good relationship with the KDE community?</strong></p>
<p>
<strong>Tomas:</strong>
I don't know. I don't talk with KDE people much. But KDE are
using SLAX as a base for the official KDE Live CD (Klax) and I like it :)
</p>

<h2>Future</h2>

<p><strong>What feature would you as a distro maintainer like to see in KDE?</strong></p>
<p>
<strong>Tomas:</strong>
Easily customisable package building.  Possibility to compile only one application from the package (again, the previously mentioned KWord, for example). Maybe this is already possible but I didn't find any documentation. The DONT_COMPILE variable doesn't work well for me.
</p>

<p><strong>Is the extended 4.0 release cycle an issue for your distro?</strong></p>
<p>
<strong>Tomas:</strong>
Not at all.
</p>

<p><strong>What are you most looking forward to about the 4.0 release?</strong></p>
<p>
<strong>Tomas:</strong>
Wobbly windows by using 2D acceleration (without 3D cards).
</p>

<p><strong>Do you plan any involvement in the beta/RC releases of the 4.0 release?</strong></p>
<p>
<strong>Tomas:</strong>
Sure I test every beta/RC, moreover sometimes I release KDE RC versions
with SLAX too.
</p>

<p><strong>Any other plans for your distro in the future?</strong></p>
<p>
<strong>Tomas:</strong>
Sure, a lot of plans, but not about KDE :)
</p>
