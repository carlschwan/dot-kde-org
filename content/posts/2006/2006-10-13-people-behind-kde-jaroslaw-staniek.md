---
title: "People Behind KDE: Jaroslaw Staniek"
date:    2006-10-13
authors:
  - "jriddell"
slug:    people-behind-kde-jaroslaw-staniek
comments:
  - subject: "Breading crickets?!"
    date: 2006-10-13
    body: "Does he deep-fry those crickets before feeding them to his chameleon? ;-)"
    author: "Louis"
  - subject: "Re: Breading crickets?!"
    date: 2006-10-13
    body: "Of course not, it's usually important that the \"meal\" is in motion. Example movie:\n\nhttp://www.youtube.com/watch?v=rr5_6PbLkdE&NR\n"
    author: "Jaroslaw Staniek"
  - subject: "Re: Breading crickets?!"
    date: 2006-10-13
    body: "Jaroslaw, I was just kidding about the misspelling:\n\nbread - v. to cover with crumbs, as in cooking preparation.\nbreed - v. to produce offspring.\n\nI used to own a lizard also, so I know all about their feeding habits. :-)  I was just having a bit of a laugh."
    author: "Louis"
  - subject: "Re: Breading crickets?!"
    date: 2006-10-13
    body: "haha, OK, already fixed :)"
    author: "Jaroslaw Staniek"
  - subject: "QT/KDE on Cygwin"
    date: 2006-10-14
    body: "Mr. Staniek says \"Under windows 2000 I heavily use and depend on the Cygwin bash shell\".\n\nMe too, so I tried qt-win-opensource-src-4.2, but I couldn't get it to work under Cygwin; qmake wouldn't build.  \"qt4 does not support cygwin yet and requires some patches.\"   http://lists.trolltech.com/qt-interest/2005-08/thread00444-0.html\n\nThere is a Qt version for mingw, I'll try that next.\n\nKDE on Mac and KDE on Windows are very important projects to get people to Qt on a FOSS O.S."
    author: "S Page"
  - subject: "Re: QT/KDE on Cygwin"
    date: 2006-10-14
    body: "I'm happily not involved in Windows coding, but I'm familiar with the projects. First of all it is not Qt not supporting Cygwin, it is the other way around. Cygwin typically lags behind the latest KDE releases. Beyond that I'm not sure if it ever will because the Qt4 windows package you are citing is a native GPL windows package. Cygwin provides X on Windows. Qt4 for windows is native AFAIK so that would mean no Cygwin. What Jaroslaw has been working on is KDE3 on Windows ported from Linux to Windows. KDE 4 will start off with a native windows GPL Qt.\n\nThis is similar to the Mac story where Fink (IIRC) was the library to run an X server on OS X and then a native GPL version of Qt was released. So if you're concerned about Qt/KDE on Mac and Windows introducing our software to people I'd say a lot more of them are going to be exposed if they don't have to set up an X server to run the software."
    author: "Eric Laffoon"
  - subject: "Re: QT/KDE on Cygwin"
    date: 2006-10-15
    body: "Yeah, I know Cygwin offers an X11 server (XOrg 6.8.99) on Windows, I'm not talking about that.  I just wanted to build the Qt for Windows *source* code using the build tools that I've already installed under Cygwin (g++, GNU make, etc. in the Cygwin UNIX-like environment) rather than the MinGW tools (g++, mingw32-make, etc. in a DOS-like environment)  Even though the commands are similar, I realize it's non-trivial to support both, e.g. configure output has\n  Build is done in............C:\\cygwin\\home\\spage\\qt-win-opensource-src-4.2.0\n  Install prefix............../home/spage/qt-copy\n  Headers installed to........\\home\\spage\\qt-copy\\include\nDarn Windows path separators and drive letters! :-)\n\n(I occasionally ran ranger rick's fine KDE 3/Fink work on a company Mac laptop.)"
    author: "S Page"
  - subject: "Re: QT/KDE on Cygwin"
    date: 2006-10-14
    body: "By mentioning \"Cygwin\" I mean \"running Cygwin bash shell for convenience\", not linking against cygwin libs or running X11 apps. And yes, I talked about KDE3 development. I wish we could use Cygwin bash with Qt4 as well.\n"
    author: "Jaroslaw Staniek"
  - subject: "OT: I think I'm not the only one..."
    date: 2006-10-14
    body: "who wants to start a \"Happy Birthday\" thread.\n\nThank you _all_ KDE developers and contributors for ten amazing years.\nOne of the best of KDE is it comunity. So, this is also the right time to celebrate ourself :-)\n\nBye\n\n  Thorsten (user since a 0.x beta of KDE) :-)"
    author: "Thorsten Schnebeck"
  - subject: "Re: OT: I think I'm not the only one..."
    date: 2006-10-14
    body: "Right. Happy Birthday KDE, and long live thy dragon!\n\nUser since 3.0"
    author: "more"
---
On tonight's <a href="http://people.kde.nl/">People Behind KDE</a> we present a coder who has been the driving force behind not only the premier free software database client, Kexi, but also single handedly ported kdelibs to win32.  Find out how he achieved such feats of development heights while still breeding a herd of crickets for his chameleon in our interview with <a href="http://people.kde.nl/jaroslaw.html">Jaroslaw Staniek</a>.



<!--break-->
