---
title: "Amarok Weekly News Issue 4 Released"
date:    2006-12-31
authors:
  - "Ljubomir"
slug:    amarok-weekly-news-issue-4-released
comments:
  - subject: "Happy new year.."
    date: 2006-12-31
    body: "..to all our users! Have a great party, folks :)"
    author: "Mark Kretschmann"
  - subject: "Re: Happy new year.."
    date: 2006-12-31
    body: "What time is it at your end? We still have to wait for new year about 365 days... ;-)"
    author: "AJ"
  - subject: "cool!"
    date: 2007-01-01
    body: "I love amarok, the weekly newsletter (delights me more than a cherry!) and my collection of songs. For the former 2: compliments guys!\nBtw, what's that 'labelling dialog' that is shown under the 'stars' screenshot? What is it supposed to do? I find that reeeally cool!\n"
    author: "alphabet"
  - subject: "Re: cool!"
    date: 2007-01-01
    body: "It is used for labeling your tracks. Every track in Amaroks database can be given an arbitrary number of user-defined labels. They are used for creating playlists,   and easy locating files. For example, I assign \"Guitar Hero\" label to every song in my collection which is available in Guitar Hero PS2 game, \"Interesting\" label to song which I hear for first time, but I would like to hear it again, and \"Cover\" label to tracks which are not preformed by their original artists. It's cool. :)"
    author: "Ljubomir"
  - subject: "V4L Radio"
    date: 2007-01-02
    body: "I'm not an Amarok user, but it looks interesting. Does Amarok has radio support via V4L?"
    author: "Max"
  - subject: "Re: V4L Radio"
    date: 2007-01-03
    body: "Nope."
    author: "Ljubomir"
---
Late but worthy -- that's how one can call <a href="http://ljubomir.simin.googlepages.com/awnissue4">this issue of Amarok Weekly News</a>. It talks about new or updated Amarok features, and continues to provide tips and links to interesting scripts. As a bonus, kind of a New Year gift, we provide you an <a href="http://ljubomir.simin.googlepages.com/awn.rss">experimental RSS feed</a>, for your pleasure. Enjoy!





<!--break-->
