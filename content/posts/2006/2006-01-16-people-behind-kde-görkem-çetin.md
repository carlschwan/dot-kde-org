---
title: "People Behind KDE: G\u00f6rkem \u00c7etin"
date:    2006-01-16
authors:
  - "jriddell"
slug:    people-behind-kde-görkem-çetin
comments:
  - subject: "neat"
    date: 2006-01-16
    body: "nice  interview.\nBy the way  I  just ran  across  that aaron interview here:\nhttp://www.linuxdevcenter.com/pub/a/linux/2006/01/12/kde4.html?page=2\nand some mockup/screenshots  of  KDE4:\nhttp://vladoboss.softver.org.mk/mg2/index.php?list=5"
    author: "Patcito"
  - subject: "Re: neat"
    date: 2006-01-16
    body: "Why did you link to the second interview page?\n\n> and some mockup/screenshots of KDE4\n\nSome wannabe mockups, if you think KDE4 will look this then you're wrong."
    author: "Anonymous"
  - subject: "Re: neat"
    date: 2006-01-16
    body: "So KDE 4 will be even better?"
    author: "Andre"
  - subject: "Re: neat"
    date: 2006-01-16
    body: "let's hope so ;-)\nthe screenshots are kind'of 'research' for kde 4. so some elements might get into kde 4, while others won't."
    author: "superstoned"
  - subject: "TarasBulba"
    date: 2006-01-16
    body: "yay, does he knows ukrainian? :)"
    author: "Nick"
  - subject: "are there also some app-DEVs ?"
    date: 2006-01-17
    body: "it would be cool if there will be some turkish kde devs lurking around in #kde-devel oder #kde4-devel .-|"
    author: "chri"
  - subject: "Re: are there also some app-DEVs ?"
    date: 2006-01-17
    body: "Yes there is ;)"
    author: "cartman"
  - subject: "Re: are there also some app-DEVs ?"
    date: 2006-01-17
    body: "ok one person from turkey - i see some potential in this country with 69.660.559 (July 2005) inhabitants :-) Perhaps more eork to get people interested in kde is needed !"
    author: "chris"
  - subject: "turkey with bird flu"
    date: 2006-01-21
    body: "they r just making money with open source software..\nsomeone should stop it.. all those companies are destroying our world..\nsure you can make money, but not with artwork.. thats stupid.. get paid for service, for professional data-bindings, storage (oracle..) and so on.. \n\nthink about it.. \n\nthe situation is comparable to selling Che-T-Shirts: paradoxum ad absurdum"
    author: "noname"
---
Tonight's interview on <a href="http://people.kde.nl">People Behind KDE</a> is with one of the heros of KDE localisation.  This man leads the <a href="http://www.kde.org.tr/">KDE Turkey</a> group and translates KDE into Turkish.  For KDE 4 he plans to get 100% Turkish support but how did he get a t-shirt with Che on it and why does he feel an affiliation towards the KDE Run Command dialogue?  Find out as People Behind KDE presents <a href="http://people.kde.nl/gorkem.html">Görkem Çetin</a>.




<!--break-->
