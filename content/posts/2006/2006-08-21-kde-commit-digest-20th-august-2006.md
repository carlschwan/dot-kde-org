---
title: "KDE Commit-Digest for 20th August 2006"
date:    2006-08-21
authors:
  - "dallen"
slug:    kde-commit-digest-20th-august-2006
comments:
  - subject: "Thanks for this digest"
    date: 2006-08-20
    body: "The KDE project is great. This will be the future of desktop computing.\n\nLong live Penguin."
    author: "Brazil Guy"
  - subject: "Oxygen icons"
    date: 2006-08-20
    body: "Can someone post a screenshot of KGet with the new Oxygen icons? Please!"
    author: "tux"
  - subject: "Re: Oxygen icons"
    date: 2006-08-20
    body: "I haven't got a screenshot, but here are (at least) some of the icons:\nhttp://commit-digest.org/issues/2006-08-20/moreinfo/573367/#visual\n\nDanny"
    author: "Danny Allen"
  - subject: "Re: Oxygen icons"
    date: 2006-08-23
    body: "Why are Oxygen icons being installed as CrystalSVG?"
    author: "James Richard Tyrer"
  - subject: "Re: Oxygen icons"
    date: 2006-08-21
    body: "I thought I read about ktorrent, kget etc. are to be included into KIO (as ioslaves), and that there will finally be some kind of central UI/listview for managing running transfer jobs (pausing, resuming, changing priority etc.)\n\nAm I wrong, or will kget lose its own UI?"
    author: "me"
  - subject: "Re: Oxygen icons"
    date: 2006-08-23
    body: "basically, getting oxygen should be a no-brainer for curious people ...\n\nsvn co svn://anonsvn.kde.org/home/kde/trunk/playground/artwork/Oxygen/theme/svg/ does the trick quite fine\n\n"
    author: "Martin"
  - subject: "Unity"
    date: 2006-08-21
    body: ">with changes coming to kwizard, kconfig, khtml (the rather exciting unity stuff) \nDoes this mean that the Unity stuff will replace khtml?"
    author: "Bob"
  - subject: "Re: Unity"
    date: 2006-08-21
    body: "'Unity stuff'? this is just temporary code name for WebKit's Qt4 backend. It's been already folded in Apple repositories by SVG people.\n\nlikely it can give birth to an external project? (eg. KPart alternative to khtml?). Question is if it can gather non-Apple developers followers to become a serious option (hacking WebKit is like working in a Cupertino sweatshop but without the measel wages :-)\n\n"
    author: "Sean"
  - subject: "Re: Unity"
    date: 2006-08-21
    body: "> 'Unity stuff'? this is just temporary\n> code name for WebKit's Qt4 backend.\n\nActually, it is a frontend. WebKit is the backend.\nSorry for being picky. :)"
    author: "blacksheep"
  - subject: "Re: Unity"
    date: 2006-08-21
    body: "> if it can gather non-Apple developers followers\n\nyou mean like the kde developers who did the initial port and have continued to work on it? ;)"
    author: "Aaron J. Seigo"
  - subject: "Re: Unity"
    date: 2006-08-21
    body: "Actually the kde developers who did the initial port Dirk, Zack and George haven't touched it since, but the KSVG people are moving it forward right now (or backwards since it is now in Apple SVN where no one else can touch it!!)."
    author: "Carewolf"
  - subject: "Default Icons"
    date: 2006-08-23
    body: "> \"use always the current iconset for all icons\"\n\nNOT a good idea.  JR has apparently spend considerable time renaming a lot of CrystalSVG icons to HiColor so that they will install as HiColor.  Now the current iconset is going to change and someone will have to do the work over again.\n\nThe reasonable conclusion is that we need to come up with some way to avoid this unless it was somebody's intention to force everyone to use CrystalSVG.\n\nThe standard says:\n<<\nThe lookup is done first in the current theme, and then recursively in each of the current theme's parents, and finally in the default theme called \"hicolor\" (implementations may add more default themes before \"hicolor\", but \"hicolor\" must be last).\n>>\n\nAdding more default themes before HiColor is also a bad idea unless it is configurable.  Users (that don't use CrystalSVG) are complaining that that the Icon Loader doesn't properly fall back to HiColor -- that they get CrystalSVG rather than HiColor.  Gento even patches to try to fix this but I was advised that it doesn't work.\n\nA  suggested solution to both of these issues is that additional icon themes should be added AFTER HiColor as a backup to HiColor and that this needs to be configurable (and DeskTop specific).\n\nIf that is done: if we don't have an icon in the chosen icon theme then the fall back is first to the inherited themes, second to HiColor and then if we are missing the icon in HiColor the themes in the backup list will be searched.  Normally, the first one on the list would be the current default.\n\nIf this is done users not wanting CrystalSVG icons will get them ONLY if a HiColor icon does not exist.  This is what I, and other users, expect to happen.\n"
    author: "James Richard Tyrer"
  - subject: "Re: Default Icons"
    date: 2006-08-23
    body: "I would like to see a tool or script which checks for Iconset completeness, so developers of iconset understand what was missing.\n\n"
    author: "hill"
  - subject: "Re: Default Icons"
    date: 2006-08-24
    body: "That is a good suggestion.\n\nHowever, before we do that, we need a list of the icons which we need a iconset to have, and that is the hard part."
    author: "James Richard Tyrer"
  - subject: "Re: Default Icons"
    date: 2006-08-25
    body: "I'm pretty sure aseigo was working on this:\n\nhttp://aseigo.blogspot.com/2006/06/tagging-icons.html"
    author: "Parkotron"
---
In <a href="http://commit-digest.org/issues/2006-08-20/">this week's KDE Commit-Digest</a>: As the <a href="http://developer.kde.org/summerofcode/soc2006.html">Summer Of Code</a> draws to a conclusion, functional code imports and work in the avKode Phonon backend, KDevelop Teamwork and Advanced Session Management projects. Work begins on version 2 of the <a href="http://kross.dipe.org/readme.html">Kross</a> scripting framework. More work on video file support in <a href="http://kphotoalbum.org/">KPhotoAlbum</a>. New features and streamlining in <a href="http://konversation.kde.org/">Konversation</a> and <a href="http://konsole.kde.org/">Konsole</a>. New <a href="http://oxygen-icons.org/">Oxygen</a> icons and other improvements in <a href="http://kget.sourceforge.net/">KGet</a>. The introduction of wizards to automate many tasks in <a href="http://www.kmobiletools.org/">KMobileTools</a>. Initial porting to KDE 4 of the console-based kdepim tools, with <a href="http://kopete.org/">Kopete</a> 0.12 moved into the KDE 3.5 branch. Experiments in fast <a href="http://en.wikipedia.org/wiki/PDF">PDF</a> parsing in <a href="http://www.vandenoever.info/software/strigi/">Strigi</a>.

<!--break-->
