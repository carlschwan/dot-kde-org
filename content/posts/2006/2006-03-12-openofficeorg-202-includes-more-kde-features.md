---
title: "OpenOffice.org 2.0.2 Includes More KDE Features"
date:    2006-03-12
authors:
  - "jholesovsky"
slug:    openofficeorg-202-includes-more-kde-features
comments:
  - subject: "KDE Firefox"
    date: 2006-03-12
    body: "I like OpenOffice with the KDE look and feel... But I would love it even more when there would be a Firefox with KDE look and feel. (And NO Konqueror is not yet 100% exchangable with Firefox, even when it passes the ACID2, it still needs a RichText Editor option).\n\nA long time ago (in a galaxy far far away...) I saw a KDE version of a very old Mozilla (Firefox?) Version.\n\nWish I had enough KDE/QT knowledge, then I would have done it myself....\nBut as long that this isn't the case, to those who can: KEEP UP THE GOOD WORK!!!"
    author: "boemer"
  - subject: "Re: KDE Firefox"
    date: 2006-03-13
    body: "Firefox/Qt is a good example of something that has been done 90%, but the\nmost important 10% are still missing... Probably partly due to mozilla\npeople not wanting to give cvs-write access to some KDE devs.\n\nA sad story, especially if you remember that Firefox/Qt was (re)created/announced\nin August 2004, one and a half year ago.\n\nAll of that work (the 90%) was done in 1 (one) week.\n\nSince then it has seen no more work, mainly due to the cvs-problem mentioned above...\n\nThere are talented hackers around which want(ed) to continue the port, but probably (unfortunately) they have lost the interest, for the mentioned reasons...\n\nMaybe Mozilla-people are just scared to the point that, if the Qt-port would be complete, they could notice that betting on gtk+ for the past years was not really a good idea...\n"
    author: "ac"
  - subject: "Re: KDE Firefox"
    date: 2006-03-13
    body: "I hope this might chance as Qt4 becomes more widespread. As Qt4 exists for OSX, Win32 and Linux, it might be quite a good idea to use it as a toolkit for firefox, as it look and feel is more integrated."
    author: "ACII"
  - subject: "Re: KDE Firefox"
    date: 2006-03-13
    body: "Here's the bug report in Firefox's bugzilla that asks Firefox to use the KDE filepicker when running in KDE:\nhttps://bugzilla.mozilla.org/show_bug.cgi?id=298848\n\nHowever, the best solution would be to iron out the bugs and usability issues in Konqueror web browser (http://Konqueror.org) so that we have a KDE solution with true desktop integration."
    author: "A C"
  - subject: "Re: KDE Firefox"
    date: 2006-04-13
    body: "Konqueror is not (and will never be) a replacement for Firefox.  Among other things, it doesn't have:\n\n1) FF's vast extension library\n2) commercial plugin support\n3) a large installed user base (important for web devs)\n4) that certain jene se qua of a gecko engine\n\nKonqueror shouldn't have all these things.  They're different browsers with different approaches and strengths.  I use Konqueror at times and am grateful for it.  For my daily browsing however, nothing but FireFox will do."
    author: "eelliott"
  - subject: "Re: KDE Firefox"
    date: 2006-06-09
    body: "To give Firefox 1.5 the KDE filepicker for \"save-as\" actions in a KDE environment, I have used the following work-around:\n\n1) locate the file nsFilePicker.js. \nFor me (working with suse 10.1) this is in the directory:\n/usr/lib/firefox/components/ \n2) To be sure: make a backup copy of that file\n3) Edit the file nsFilePicker.js and search the following code fragment: \n<BLOCKQUOTE>\n//@line 278 \"/usr/src/packages/BUILD/mozilla/xpfe/components/filepicker/src/nsFilePicker.js.in\"\n                                    \"\",\n//@line 280 \"/usr/src/packages/BUILD/mozilla/xpfe/components/filepicker/src/nsFilePicker.js.in\"\n</BLOCKQUOTE>\n\nNow replace the line with:\n\"\",                       \nby:                         \nFILEPICKER_CONTRACTID,\n\n4) As a last step we have to reset the chrome register.\nFor this I add & subsequently delete an item under tools/extensions of Firefox. (there must be amore intelligent way ...)\n5) Enjoy your KDE style filepicker\n\nRegards, Paul."
    author: "Paul"
  - subject: "Re: KDE Firefox"
    date: 2006-03-13
    body: "While it is true that Firefox does use the GTK+ toolkit, it is not GNOME compatable either.\n\nIt would probably be easier to integrate it into the KDE desktop if it conformed to the FreeDeskTop.org standards."
    author: "James Richard Tyrer"
  - subject: "Re: KDE Firefox"
    date: 2006-03-13
    body: "Well if it doesn't integrate with GNOME, I keep wondering why it wants to start natilus (something that remines me of a filemanager from 10 years ago atleast.., no features).\n\nIf I could work with natilus it would be find to, but just opening a konqueror filemanager or make it optional what will be opened from the download manager, would make firefox under linux a totally differnt experience...\n\nA Plastic theme for firefox already exists. So even if it remeans GTK+ only, the only big problem is the fixed filemanager integration."
    author: "boemer"
  - subject: "Re: KDE Firefox"
    date: 2006-03-14
    body: "> I keep wondering why it wants to start natilus ...\n\nThis is configurable somewhere, it is not built into Firefox!\n\nIIUC, Firefox-1.5 reads the XDG MIME database which is not GNOME although GNOME uses it.  Details should be available on freedesktop.org.\n\nAlso, Firefox and Thunderbird have an ECMA script file: \"prefs.js\" which controls some stuff.  It is located in a subdirectory of: ~/.mozilla or ~./thunderbird.  You should NOT edit this file, but rather place your changes in a \"user.js\" file in the same directory and restart the app.\n\nWhen I click on a link in Thunderbird, it opens the link in Konqueror.  This took three lines in the Firefox \"user.js\" file:\n\nuser_pref(\"network.protocol-handler.app.ftp\", \"/usr/local/bin/konqueror-URL\");\nuser_pref(\"network.protocol-handler.app.http\", \"/usr/local/bin/konqueror-URL\");\nuser_pref(\"network.protocol-handler.app.https\", \"/usr/local/bin/konqueror-URL\");\n\nThis points to a short Bash script: \"konqueror-URL\".\n\nI presume that similar configuration can be done for Firefox."
    author: "James Richard Tyrer"
  - subject: "Re: KDE Firefox"
    date: 2006-03-14
    body: "\"Well if it doesn't integrate with GNOME, I keep wondering why it wants to start natilus\"\n\nFirefox does listen to Gnome settings.\nIf you make kmail default in Gnome, firefox will start kmail when clicking on a mailto: URL\n\nFirefox does not listen to KDE settings.\nIf you make kmail default in KDE, firefox will either start whatever was default in Gnome on your system, or give the \"brilliant and very user friendly\" error message that the protocol 'mailto' is unknown.\n\nTo get firefox use the mailapplication on platforms other then gnome, one needs to hack into 'about:config' and create a proper string for it.\n"
    author: "AC"
  - subject: "Re: KDE Firefox"
    date: 2006-03-14
    body: "> Firefox does not listen to KDE settings.\n\nWhy?\nThe Firefox people are waiting for those patches.\nIt's not forbidden for KDE developers to collaborate with other projects."
    author: "Martin"
  - subject: "Re: KDE Firefox"
    date: 2006-03-15
    body: "As I said, Firefox listens to XDG MIME info:\n\nhttp://www.freedesktop.org/wiki/Standards_2fshared_2dmime_2dinfo_2dspec\n\nand KDE doesn't use this yet."
    author: "James Richard Tyrer"
  - subject: "Re: KDE Firefox"
    date: 2006-03-14
    body: "There's probably a more \"right\" way to do this, but I managed to get firefox to open konqueror rather than nautilus by changing /usr/share/applications/nautilus-folder-handler.desktop.\n\nJust changed \n\nExec=nautilus --no-desktop %U \n\nto \n\nExec=konqueror %U\n\nI would assume that there is an override for these settings in your home directory structure but I didn't get around to finding it.\n\nHope this helps\n\nRyan"
    author: "Ryan Fowler"
  - subject: "Re: KDE Firefox"
    date: 2006-03-15
    body: "Make a file \" in: \"~/.mozilla/firefox/default.<key>/\" called: \"user.js\" and put something like this in it:\n\nuser_pref(\"network.protocol-handler.app.mailto\", \"/usr/local/bin/thunderbird\");\n\nonly have the app you want (with the full path) in place of thunderbird.\n\nRestart Firefox and it should work.\n\n"
    author: "James Richard Tyrer"
  - subject: "Re: KDE Firefox"
    date: 2006-03-21
    body: "I keep wondering the same thing every time it shows me that horrible gnome/gtk file dialog.  But I've got to agree with what other people have said, I'd much rather have Konqueror get competitive since Mozilla has consistently put out an inferior version on Linux than other platforms (at least windows) because of their use of gnome/gtk.\n\nAnd while we're on programs that we need natively on Qt/KDE, how about a proper high-end paint program?  GIMP suffers from all the same integration issues that Firefox does as well as having a development team that consistently shows that they have no clue what so ever.  Blaming gtk when your software doesn't work is inexcusable.  Lack of tablet support in any professional paint package, whether wannabe or not, is unacceptable on any level.  And after wasting years screwing around porting to gtk2 instead of actually implementing features or fixing bugs you've got no excuse.\n\nUnfortunately I don't have enough knowledge of C++ or Qt to do any of this work myself, but I'm really learning toward getting some.  Because a full featured web browser and paint program are the two things that keep me from being able to do all of my work on Linux."
    author: "hpv"
  - subject: "Re: KDE Firefox"
    date: 2006-03-21
    body: "In the first place, the Gimp has tablet support. In the second place, ever heard about Krita? We've been putting lots and lots of work into a high-end paint application using Qt and KDE for a couple of years now."
    author: "Boudewijn Rempt"
  - subject: "Re: KDE Firefox"
    date: 2006-03-20
    body: "If KDE people want to blame the lack of a port on mozilla.org not giving them special treatment, that's their choice.  Zack Rusin had CVS access long ago, the controversy came along with Dirk Mueller decided that he shouldn't have to go through the same process that everyone else has gone through."
    author: "mc"
  - subject: "Re: KDE Firefox"
    date: 2006-03-13
    body: "Very much agreed. It would be really great to have a Firefox which integrated into KDE"
    author: "Mark"
  - subject: "Re: KDE Firefox"
    date: 2006-03-13
    body: "\"it still needs a RichText Editor option\"\n\nTry using Markdown instead.  You'll eventually get to a point where it's cumbersome to enter rich text in any other way.\n\n"
    author: "Trejkaz"
  - subject: "Re: KDE Firefox"
    date: 2006-03-13
    body: "I need richtext for some people who don't know HTML, and they are very very happy with it. There is an option to look at the source (very primitive alas) so I can do anything that isn't normally possible with an richtext editor.\n\nBut because of this, it keeps me fixed on firefox (or seamonkey). \n\nI would love to be able to do it with safari or konqueror for that part. And I know they are working on something in safari, and are or have been working on a cursor for konqueror, but to my knowledge it is still far away from a fully working example.\n\nI don't really know what meltdown is, when I looked at some examples, it looks more like just writing text and html...."
    author: "boemer"
  - subject: "Re: KDE Firefox"
    date: 2006-03-14
    body: "What is the \"RichText Editor\" option in Firefox? I can't seem to find it. Is this something like NVu?"
    author: "Brandybuck"
  - subject: "Re: KDE Firefox"
    date: 2006-05-14
    body: "There is an option to use old XUL dialog. Otherwise Opera is a good alternative."
    author: "Utwig"
  - subject: "Openoffice with KDE open-file dialog"
    date: 2006-03-12
    body: "This is really nice to see. I am wondering however if anyone has come across the following issue. When I enable the openoffice-kde integration on Suse 10 or 9.3, the open file dialog is terribly slow. Using Openoffice.org's native file dialogs, the same operations are lightning fast.\n\nSo has anyone else come across this and is there anything that can be done about it?\n\nFinally, does anyone know whether Suse plans to provide upgrade packages for Suse 9.3 and Suse 10?\n\nThanks,\n\nGonzalo"
    author: "Gonzalo"
  - subject: "Re: Openoffice with KDE open-file dialog"
    date: 2006-03-13
    body: "\"So has anyone else come across this and is there anything that can be done about it?\"\n\nNoticed it as well, must be the overhead of the kde-integration package.\n\n \n\" Finally, does anyone know whether Suse plans to provide upgrade packages for Suse 9.3 and Suse 10?\"\n\nthey won't\nsuse only provides upgrade packages for suse packages that contain bugs. \nAnd suse offers some additional upgrades (supplementary directories on the ftp server), but they don't include openoffice.org\n\n \n"
    author: "ac"
  - subject: "Re: Openoffice with KDE open-file dialog"
    date: 2006-03-13
    body: "Use apt and add \"suse-projects\" to the repository list. It has 2.0.2 right now."
    author: "a c"
  - subject: "Re: Openoffice with KDE open-file dialog"
    date: 2006-03-13
    body: "Of course Suse offers updated Openoffice (and some other applications):\n\nftp://ftp.tu-chemnitz.de/pub/linux/suse/ftp.suse.com/projects/OpenOffice.org\nftp://ftp.tu-chemnitz.de/pub/linux/suse/ftp.suse.com/projects/apache\nftp://ftp.tu-chemnitz.de/pub/linux/suse/ftp.suse.com/projects/mozilla\nftp://ftp.tu-chemnitz.de/pub/linux/suse/ftp.suse.com/projects/kernel\nftp://ftp.tu-chemnitz.de/pub/linux/suse/ftp.suse.com/people/meissner/wine\nftp://ftp.tu-chemnitz.de/pub/linux/suse/ftp.suse.com/projects/gcc\nftp://ftp.tu-chemnitz.de/pub/linux/suse/ftp.suse.com/suse/i386/supplementary"
    author: "max"
  - subject: "Re: Openoffice with KDE open-file dialog"
    date: 2006-03-13
    body: "2.0.2-0.3 is Release Candidate 3 !!!\nyou can check the discription inside the rpm-file."
    author: "thewho"
  - subject: "Re: Openoffice with KDE open-file dialog"
    date: 2006-03-14
    body: "Then download 2.0.2-1, that's the final release.\n\nftp://ftp.tu-chemnitz.de/pub/linux/suse/ftp.suse.com/projects/OpenOffice.org/10.0-i386/2.0.2-1"
    author: "max"
  - subject: "Re: Openoffice with KDE open-file dialog"
    date: 2006-03-14
    body: "ah, finally it's online :)\nthanks"
    author: "thewho"
  - subject: "Re: Openoffice with KDE open-file dialog"
    date: 2006-03-14
    body: "That's because OO.o starts an external app \"kdefilepicker\" when you want to open files, so this incurs all the usualy KDE app start-up delays each time you access files! Notice also, that when the file selector comes up it is a top-level window (gets a taskbar entry, etc) with a 'X' icon, and not the OO.o icon. (They could've at least made the kdefilepicker \"transient\" for the OO window)\n\nAlso, the themeing is not that brilliant, notice how the toolbar icons, or pushbutton text, don't move when the item is pressed - even if the KDE theme says they should."
    author: "Craig"
  - subject: "Re: Openoffice with KDE open-file dialog"
    date: 2006-03-14
    body: "\"That's because OO.o starts an external app \"kdefilepicker\" when you want to open files, so this incurs all the usualy KDE app start-up delays each time you access files! Notice also, that when the file selector comes up it is a top-level window (gets a taskbar entry, etc) with a 'X' icon, and not the OO.o icon.\"\n\nSo, that's the reason OO.o doesn't full support kioslaves yet, I guess.\nDoes anybody how is things going related to this subject?"
    author: "Marcelo Barreto Nees"
  - subject: "Re: Openoffice with KDE open-file dialog"
    date: 2006-03-14
    body: "It isn't. Even if the filepicker was launched from within OpenOffice.org, as oposite from an external script (to avoid linkage), there wouldn't be any changes in that regard. KDE filepicker would return either way the very same URL. You either have to add KIO support for OOo, or, better yet, make filesystem be in userspace on Linux like in a microkernel (ie. FUSE)."
    author: "blacksheep"
  - subject: "Gtk Programs"
    date: 2006-03-13
    body: "GTK programs also need to be better integrated into KDE. KDE should not be about toolkits. I mean whxy shouldn't Abiword when executed under KDE use the KDE-file dialogue. It must be possible to reimplement the KDE filedialogue for GTK. "
    author: "gerd"
  - subject: "Re: Gtk Programs"
    date: 2006-03-13
    body: "and in your mind what is the Gtk+ project's role supposed to be in all this?"
    author: "Aaron J. Seigo"
  - subject: "Re: Gtk Programs"
    date: 2006-03-13
    body: "> what is the Gtk+ project's role supposed to be in all this?\n\"Sabotage\" :-)\n\nActually, fact is that today there are important apps for gnome and kde and when Gnome would go, we could expect gtk to get visually merged into KDE.\n\nSo the formula KDE app sphere = qt is wrong despite that KDE is qt based. And the formula gtk = gnome is flawed.\n\nThe clean solution is a patch for the latest gtk/gtk+, let's call it ktk which replaces the file dialogue. When you compile programs with ktk patched gtk you will see no difference in behaviour.\n\nAnother \"ugly hack\" for the same is http://www.kde-look.org/content/show.php?content=36077\n\n\n"
    author: "gerd"
  - subject: "Re: Gtk Programs"
    date: 2006-03-13
    body: "Perhaps Rudy kan take care of this :o)"
    author: "ac"
  - subject: "Re: Gtk Programs"
    date: 2006-03-14
    body: "> The clean solution is a patch for the latest gtk/gtk+\n\nnow who do you think has the ability to apply this patch upstream?\n\nkde people have actually looked at this in the past and IIRC, at least when they did that a year or two ago, the gtk+ calls for the file dialogs are not well suited to being used with ours. API concept conflicts, in essence.\n\nwhat i'm getting at is that there's more to this world than KDE. we can do only so much, and to be honest we've really been leading when it comes to integration issues. look at who's taken the common event loop stuff most seriously, for instance (hint: Qt). then look at IPC, multimedia in KDE4, etc... hell, the whole RUDI concept came from a KDE guy.\n\nwhile i agree that more seamlessness is an excellent goal that we should work towards, it takes more than just KDE and to be honest i think we've been doing more than our job in this regard."
    author: "Aaron J. Seigo"
  - subject: "Re: Gtk Programs"
    date: 2006-03-14
    body: "In layman's language, that means that the GTK people would rather integrate GTK with Windows than they would with KDE, even though it is totally possible for them to do so, for reasons which are best known to them. It certainly isn't a licensing issue, which some of the Eclipse people have really hidden behind.\n\nI have to say I'm not keen on the concept of RUDI (a layer to target multiple desktops and XFCE, which no ISV or company will go for), and the only part of it I like is trying to solve the practical problem of binary compatibility (although trying to solve that may end up with the former being possible!). It just tries to go too far to somewhere which is just ultimately not possible in my opinion, and from all I've read.\n\nThe effort going into sensible integration, and ideas for integration where possible, currently all seems to be one way. If you point out something that \"Just Works\" inevitably there is a licensing issue or something else as to why it shouldn't be used or shouldn't be done."
    author: "Segedunum"
  - subject: "Re: Gtk Programs"
    date: 2006-03-15
    body: "\"kde people have actually looked at this in the past and IIRC, at least when they did that a year or two ago, the gtk+ calls for the file dialogs are not well suited to being used with ours. API concept conflicts, in essence.\"\n\nI mean a reimplementation in GTK with no KDE dependencies and using the gtk calls/interface.\nA clone of KDE file dialogue so to speak. KTK as GTK people will probably not accept the patch and it would be wasted time to battle on that front."
    author: "gerd"
  - subject: "Re: Gtk Programs"
    date: 2006-03-13
    body: "It too think that a replacement for the standard gtk dialogs of gtk programms running within kde would be nice. That stupid gtk file dialog in eclipse is really annoying me. "
    author: "Anon"
  - subject: "Re: Gtk Programs"
    date: 2006-03-13
    body: "Eclipse is another kettle of fish altogether... hopefully I can explain it correctly.\n\nEclipse uses SWT, which is a whole new toolkit.  SWT, however, provides hooks so you can implement it with native look and feel.  Any of the widgets that exist in SWT, but not natively, will just be rendered by SWT.\n\nBecause of this Eclipse can now use a lot of Windows widgets when running on Windows and GTK widgets elsewhere.\n\nThe Qt-SWT bridge has already been written, but was never released because of licence incompatabilities.  I know there was a sort-of campaign to have the two parties come to a compromise, but it kind of fizzled I guess.\n\nSo if the legal issues can be bridged, a full KDEized version of Eclipse is technically very possible because of the way SWT allows native widgets to render certain parts of it.\n\nAzareus is also a SWT gui, which could benefit from the same.\n\nL."
    author: "ltmon"
  - subject: "Re: Gtk Programs"
    date: 2006-03-13
    body: "Trolltech already gave the go-ahead to use Qt with SWT on the condition that SWT was under a legitimate open source licence.  The question of legal issues is more of a FUD tactic that was more than likely due to the code not actually having been written.\n\nGPLv3 is supposed to play better with patents though (which was the supposed incompatibility between GPLv2 and CPL) so perhaps once that goes final we'll go and try and get them to give a new excuse for not releasing it.\n"
    author: "Trejkaz"
  - subject: "Re: Gtk Programs"
    date: 2006-03-15
    body: "> The question of legal issues is more of a FUD tactic that was more than likely \n> due to the code not actually having been written.\n\nAs far as I know, there is at least a eSWT implementation for Qt Embedded available for OEMs with IBMs WebSphere Everyplace Micro Environment. So, IBM should have some code for SWT on Qt.\n\nBut I also heard some rumor, that the legal issues at least for SWT on Qt Embedded are solved. But I don't know anything more specific."
    author: "Anony Mouse"
  - subject: "Re: Gtk Programs"
    date: 2006-03-13
    body: "I saw this the other day, but never tried it:\nhttp://kde-apps.org/content/show.php?content=36077\n\nAn interesting concept nevertheless."
    author: "[Knuckles]"
  - subject: "Re: Gtk Programs"
    date: 2006-03-14
    body: "\"GTK programs also need to be better integrated into KDE. KDE should not be about toolkits. I mean whxy shouldn't Abiword when executed under KDE use the KDE-file dialogue.\"\n\nThis is a problem for the GTK people, not for KDE. At the moment, ironically, integration with Windows seems to be more a priority for them than integration with KDE. Many people want to deny that integration of GTK and KDE is possible, using the usual, same old, same old reasons, licensing issues etc. etc. but it is perfectly possible."
    author: "Segedunum"
  - subject: "Re: Gtk Programs"
    date: 2006-03-26
    body: "Patched GTK needs not go upstream. As long as KDE maintains a patched GTK, I bet many distros will provide the patched version to the users."
    author: "jenfoong"
  - subject: "Has OO.org fixed the list bug yet?"
    date: 2006-03-13
    body: "Does anyone know if OO.org 2.0.2 has fixed the list bug yet? I can't make heads or tails of the bug report as to whether this has been fixed.\n\nhttp://www.openoffice.org/issues/show_bug.cgi?id=52127"
    author: "David P James"
  - subject: "Re: Has OO.org fixed the list bug yet?"
    date: 2006-03-13
    body: "nope :("
    author: "fast_rizwaan"
  - subject: "Crystal icon"
    date: 2006-03-15
    body: "just want to note at this place too, that the icons for printdefault and printpreview are not consistent with other KDE print icons.\n\nOO - printdefault = KDE - printpreview\n\nIMHO this needs to be corrected."
    author: "ferdinand"
  - subject: "OpenOffice still confuses klipper"
    date: 2006-03-15
    body: "I still want to see openoffice not to confuse klipper anymore. Whenever i select an textobject an image of the selected textobject gets copied to the clipboard and not only once. Same for just selecting images. I read about in 3.5.2 you will be able to disable images completly in klipper but of course this does not fix the bug in OOo but at least it is a work-around :o/\n\nAnyway... 2.0.2 integrates nicely into my KDE and looks really nice :o)\nDV\n"
    author: "DarkVision"
  - subject: "KDE integration still wacky in OO"
    date: 2006-03-16
    body: "The OO can now use the KDE file picker - great. However, the KDE file picker is opened as a standalone application (it has its own button on the taskbar with ugly X icon and is not modal to the OO window). If I close the main OO window the file picker remains open (!!!) and after I select a file with it and click OK than it crashes.\n\nVery sad."
    author: "ZACK"
  - subject: "Re: KDE integration still wacky in OO"
    date: 2006-03-17
    body: "That is so to avoid linkage (making OOo even more heavy) and make things more configurable. I think worse than that is that other protocols than file:/ won't work."
    author: "blacksheep"
---
<a href="http://www.openoffice.org">OpenOffice.org</a> 2.0.2 has been announced.  Among other new features, fixes, and improvements, this version contains the <a href="http://opensource.bureau-cornavin.com/kab/">KDE Addressbook Connector</a> by Éric Bischoff, and <a href="http://artax.karlin.mff.cuni.cz/~kendy/ooo/icons/status">Crystal icons</a> from KDE, many newly created by Nuno Pinheiro and Robert Wadley.  The Crystal icon set for OOo is not yet complete have a look at the <a href="http://artax.karlin.mff.cuni.cz/~kendy/ooo/icons/status">status page</a> if you are interested in helping. Both the KDE Addressbook Connector and the Crystal icons were already available in <a href="http://www.go-oo.org">ooo-build</a>, so you may already have them if your distribution uses ooo-build as a base for the OpenOffice.org packages.


<!--break-->
