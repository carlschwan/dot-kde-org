---
title: "PC-BSD Interview"
date:    2006-09-05
authors:
  - "hnordin"
slug:    pc-bsd-interview
comments:
  - subject: "PCBSD with KDE is cool"
    date: 2006-09-05
    body: "Tight KDE integration in PC-BSD, and BSD does seem very stable. But performance wise it is a bit slower on 1.7Ghz system. I tried PC-BSD for KDE's sake, and found very good stability and integration. Because of KDE I felt really comfortable. KDE Does make Unix a Desktop OS :)\n\nGood Luck with PC-BSD and KDE in future :)"
    author: "Fast_Rizwaan"
  - subject: "Re: PCBSD with KDE is cool"
    date: 2006-09-05
    body: "KDE may have earned a record as a memory hog. It's not even the fastest in the shed in a decent system but still. Default performance/look&feel settings on PC-BSD are faster then Windows XP self managed. I first used PC-BSD /w KDE 3.5.x on a 500Mhz machine that used to run XP Professional. MUCH faster !\n\nFor speed, all roades lead away from DE's and into WM's :-)\n\n"
    author: "TerryP"
  - subject: "Re: PCBSD with KDE is cool"
    date: 2006-09-06
    body: "I run KDE, all the goodies turned on, using many apps at the same time, many WWW tabs in Konqueror, etc. and I don't have any problem with memory usage. I have 512MB of RAM, and looking at memory prices these days it's really not a huge amount. I have to wonder if I would really be okay with the upcoming Windows Vista, since this is actually the minimum requirement!\n\nP.S. I don't plan on ever installing Windows again, this is just comparison... got to preserve my reputation here!"
    author: "Jack H"
  - subject: "Re: PCBSD with KDE is cool"
    date: 2006-09-06
    body: "I assume geekwise installation of PCBSD compensates a whole lot of windows desktops... "
    author: "Itchy "
  - subject: "Great but still..."
    date: 2006-09-07
    body: "First of all let me say that this is really a great distro. It installed to my laptop and after loading my fixed DSDT (as I need to to with all distros) I had battery status, network monitoring and everything else working (well I haven't tried wireless but nevertheless).\n\nSetting up ADSL/PPPoE also proved to  be rather easy, although no GUI was available for this.\n\nAll in all this really _much_ better than K/Ubuntu, which I have dropped the day before yesterday, after having huge problems with PPPoE (this came on the background of catastrophic system updates, braindead CPU throttling effectively killing my machine, Kontact crashing on every system startup etc. etc.) As a matter of fact, if they keep this course, PC-BSD and probably DesktopBSD (never seen it live though) will probably be better than any Linux distros in one year.\n\nBut now I'll try SUSE 10.1. Sure it's broken, but PC-BSD's PBIs are practically broken all of them. And I really like the _concept_ of PBI, it's just that the are very poorly put together, and vital apps have great defects.\n\nOf course I could just ditch PBI and install from ports, but that's too error prone.\n\nOMG how did I end up writing so much?\n\nAnyway bottomline is that the quality of PBIs needs to be greatly improved, otherwise really good stuff.\n"
    author: "alpha"
  - subject: "Re: Great but still..."
    date: 2006-09-08
    body: "\"Of course I could just ditch PBI and install from ports, but that's too error prone.\"\n\nActually, I find the ports to be the best/most robust method of installing utilities as it does it all for you. \nThe PBI's are ok for things like KOffice, Gimp, Firefox... The ports are much better for adding shells, server components, and such.\n\nTBM"
    author: "TBM"
  - subject: "thanks for vmware image"
    date: 2006-09-11
    body: "The main problem I had with PC-BSD is that it couldn't find the kernel when I installed it on my PC but it works great in VMware. If it could only read my Mac formatted Lacie Drive it would be perfect. I have had no problems with PBIs - they work great. I was a little nervous when they said like Windows installers, but they don't ask stupid questions like where do you want to put it, do you want a desktop icon, etc. Great stability. I have also used it with one of the boxs (Blackbox/Fluxbox/OpenBox) that was in the pbi directory. Works great! Thanks for the vmware image. I really like how your updates are in packages so I don't have a problem like what happenned with X-windows update with Ubuntu. I am going to have to set Ubuntu to not do automatic updates. With PC-BSD I can actually wait a little and see if there are problems as the updates come all together. When I get a new Intel Mac and when Vmware is out for the Mac, I look forward to running it on Apple hardware."
    author: "Benjamin Huot"
  - subject: "untrue"
    date: 2006-09-14
    body: "\"Software packages live independent of the operating system, self-contained in their own directories, where they do no harm\"\ni am really sorry but thats a lie!\npcbsd has some kind of system update packges, that update your kernel, your bsd-base and installed ports like kde.\nnow if you never touch anything that might be ok, but if you compiled a custom kernel (which is NOT an exotic thing to do on freebsd - compared to linux), or maybe even update your bsd-base to get new drivers earlier, than this automatic update, which only announces a kde-update can really screw your entire system.\nbelieve me!\ni've seen it!\n\naltogether the pcbsd modifications interfere heavily with the freebsd-core. you should not expect to be able to apply freebsd-knowledge to pcbsd. \nit might work, but only until tthe next upgrade!!!\n\nfor a consistent and standardised operating system i really recommend DesktopBSD, which has a kde-based installer and extra tools, but which does f*ck with your operating system base or the kernel."
    author: "hannes hauswedell"
  - subject: "Re: untrue"
    date: 2006-09-19
    body: "No... it is actually true...\n\n  Installing software from ports involves \"Dependancies\"!\n\n  Installing software from PBI's does not.\n\nReally though...  I don't believe PC-BSD is aiming(target users) for people who know as much as you do.  PC-BSD is more for...  your Mom and Dad, your daughter's Kindergarten teacher, a secretary, etc...\n\nI hear a lot of people complain about configuring PC-BSD for some special kernel etc...  They don't seem to realize that they are NOT the intended audience.  If you want to do that stuff just install FreeBSD and maybe add the port for DesktopBSD and configure away.  That's a whole different market.\n\nI support a location with a FreeBSD Server or two and I configure on those.  But  the school teachers and the custodian are running on PC-BSD as standard as I can keep it.  They love it!  I love it!\n\nKeep it simple.  PC-BSD is not for the few Geeks... It's for the Masses!\n\nStrben\nNetwork System Admin.\nVA, US.\n"
    author: "Strben"
  - subject: "PC-BSD for the masses"
    date: 2006-10-03
    body: "I agree that PC-BSD is not for the hacking types. I am a Slackware user and my complaint is that it just isn't something I can recommend for the basic user. Linux, and FreeBSD, etc., requires too much hands on. However, PC-BSD is not that and I like it! To me, after just a short time trying it, I think it is right on track for the intended users - the masses.\nI will now recommend this for my Windows-using friends!"
    author: "bleeds"
---
PC-BSD is one of the newest additions to the BSD family. The focus for this project is to create a user-friendly desktop experience based on FreeBSD and it has quickly garnered attention from media and the community. Kris Moore founder and lead developer of PC-BSD took some time off to answer a few questions about the past and current state of the project in general and its relation to KDE in particular.





<!--break-->
<div style="border: thin solid grey; float: right; padding: 1ex; margin: 1ex">
<img src="http://static.kdenews.org/jr/pcbsd.png" width="90" height="86" />
</div>

<h2>Past</h2>

<h3>Can you tell us about the history of your distribution?</h3>

<p>PC-BSD was initially released as 0.5 Beta about a year ago, April 2005.
I chose to begin development with the goal of making a FreeBSD-based
desktop OS, with a custom software installation method called PBI or
PC-BSD Installer. Instead of a true "distro" with numerous ports or
programs being apart of the base system, PC-BSD is by default a
Operating System only. Software packages live independent of the
operating system, self-contained in their own directories, where they do
no harm or cause dependency issues.</p>

<h3>Why did you choose KDE and which version of KDE did you first implement?</h3>

<p>The first Beta release of PC-BSD used KDE 3.4, and since then we have
been patching the system, bringing us now up to the latest 3.5.3.</p>


<h3>How did you find initial support for a new distro?</h3>

<p>The support from the community was awesome! As one of the first real
Desktop oriented FreeBSD systems on the market, there was a ton of
interest.</p>

<h3>What could KDE have done better to help new distros use KDE?</h3>

<p>Probably by providing more information about the text file
configurations of KDE, and where and what is in each of the files. A lot
of time was spent trying to figure out everything in the
.kde/share/config directories, and figuring out how to manipulate them
to suit our needs.</p>

<h3>What were your first impressions about KDE's documentation and community?</h3>

<p>Fixing some specific bugs/tasks were often made easy by searching the
docs to see if others had run into those particular problems. Community
support has also been good, with lots of KDE fans pleased to see a
FreeBSD/KDE based Operating System.</p>

<h2>Present</h2>

<h3>How closely do your releases depend on KDE releases?</h3>

<p>Not too closely at the moment. Typically there is enough stuff for us to
work on release-wise to keep up busy, regardless what KDE version is
available at the moment. However, when a new KDE version makes it out, I
 put it on the list to be checked, and included with the next revision/online update for PC-BSD.</p>

<h3>Do you have a clear target audience for your distro?</h3>

<p>Yes, our Operating System is targeted at folks who like the stability/security that Unix and Open Source has to offer, but don't wish to learn
new methods of software installation or system management from their
traditional OS. By developing the PBI system, which keeps software
programs separate from the core OS, we have been able to fulfill this
important need. Now a user no longer has to worry about dependency
issues, or waste the time compiling software from source, or
troubleshooting it when things go wrong.</p>

<h3>Do you have any user feedback mechanism?  If so, what feedback do they have about KDE?</h3>

<p>Our PC-BSD forums are very popular, and we get a fair amount of
feedback. For the most part, KDE is working fine, and meets the needs of
the operating system as a whole. Often problems reported are simply
configuration issues that we are working out here with the latest
patches. (Except for issues with screensavers not launching properly
under 3.5.3 of KDE :)</p>


<h3>In what ways do you customise the version of KDE that ships with your distro?</h3>


<p>We make it a point not to do any code customisation of KDE, to make it
as easy to maintain, and keep up to date as possible. All of the
customisation we perform are simply configuration tricks and scripts to
make KDE perform the functions we need it to. For example, getting KDE
to popup with notifications when a CD has been inserted into the drive,
etc.</p>

<h3>What are the biggest strengths of KDE for your distro?</h3>

<p>Consistency. KDE across the board, has the feel of a polished and
consistent user experience. The wealth of programs included offer a nice
starting point for a minimal "OS" only type distro, such as PC-BSD.
Having KDE based on Qt is also a huge plus, since that is what we use for
our custom tool development.</p>


<h3>What are the biggest weaknesses?</h3>

<p>It seems the memory usage is a tad much at times, and optimisation is
also necessary for some older systems. Probably also trying to make sure
that programs run under "root" with kdesu appear the same visually as
regular user processes.<?p>


<h3>What KDE applications are the most popular among your users?</h3>

<p>Kopete is probably one of the most popular and we also use Konqueror as our
default web-browser, and it works well. K3b is also a popular download.</p>

<h3>Do you feel that you have a good relationship with the KDE community?</h3>

<p>Honestly, I haven't had much contact with the KDE community yet. Most of
the work done on PC-BSD/KDE has been fairly straight forward, and
there is a lot of information available online. However, we would like
to attract some KDE/Qt developers to the project, since that is an area
we always could use help with.</p>

<h2>Future</h2>

<h3>What feature would you as a distro maintainer like to see in KDE?</h3>

<p>Maybe a more powerful kicker, with more features that can make it
XP'ish, or like what the folks at <a href="http://www.kbfx.org">KBFX</a> are doing. Also, some work on ensuring that applications look &amp; feel consistent. With so many different libraries available, it is a bit strange sometimes to see so many programs open that look complete different.</p>

<p>Another BIGGIE is making sure KDE keeps up the FreeBSD support, since
our distro is based on FreeBSD 6, it is nice when we don't have to spend lots
of time troubleshooting KDE problems or bugs under FreeBSD :)</p>

<h3>Is the extended 4.0 release cycle an issue for your distro?</h3>

<p>We will wait patiently for 4.0 :) However, a lot of the things we're
hearing about it makes us excited for the future!</p>


<h3>What are you most looking forward to about the 4.0 release?</h3>

<p>Plasma sounds like a winner here. A better looking, slick desktop is
always a plus!</p>

<h3>Do you plan any involvement in the beta/RC releases of the 4.0 release?</h3>

<p>Not for the released copies of PC-BSD, however, I'm positive there will
be those of us who download and test it out on our own here.</p>

<h3>Any other plans for your distro in the future?</h3>

<p>We always have some ideas that we are throwing around. Since we are
based on FreeBSD, we are still looking into some things such as a
workable Flash solution, integrating Konqueror with plugins better, etc.</p>




