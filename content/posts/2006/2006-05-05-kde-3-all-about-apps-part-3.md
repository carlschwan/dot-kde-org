---
title: "KDE 3: All About the Apps (Part 3)"
date:    2006-05-05
authors:
  - "cniehaus"
slug:    kde-3-all-about-apps-part-3
comments:
  - subject: "Bad link"
    date: 2006-05-05
    body: "You might want to check the target of the last picture-link...\n\nAnyways, great series!"
    author: "Titus"
  - subject: "tellico"
    date: 2006-05-05
    body: "To manage my bibliography, I use tellico that semms to offer more functionnality. For example I can link a pdf to an entry and add extra fields to entries. I've added a field keyword of type array, and then I can open a view that group entries by keyword (see snapshot)."
    author: "bobuse"
  - subject: "Re: tellico"
    date: 2006-05-05
    body: "Tellico was in the first part of the series:\n\nhttp://dot.kde.org/1144660487/"
    author: "Carsten Niehaus"
  - subject: "Re: tellico"
    date: 2006-05-09
    body: "Yes, but not as a bibliographic tool."
    author: "Evan \"JabberWokky\" E."
  - subject: "What about LyX?"
    date: 2006-05-05
    body: "If LaTeX tools are already part of this issue, why did you forget LyX? After all, this is the prime tool for LaTeX power in the average user's hand, and the Qt version is the best version they have."
    author: "Pete"
  - subject: "Re: What about LyX?"
    date: 2006-05-05
    body: "Kile gives you total control on your LaTeX document, while at the same time \nmaking typing it extremely more convenient than with a simple text editor.\n\nMaybe for documents with a lot of pictures, tables and other stuff, LyX might\nappear more friendly to someone, but if you know a bit of LaTeX go for Kile; it's amazing."
    author: "Klaus"
  - subject: "Re: What about LyX?"
    date: 2006-05-05
    body: "But what if you don't know LaTeX?"
    author: "Brandybuck"
  - subject: "Re: What about LyX?"
    date: 2006-05-05
    body: "Even if you do know a bit of LaTeX (like me), I find it so much more convenient to type in a WYSIWYM-Environment. You get a text display close (obviously and intentionally not identic) to the printed product, you get a nice formula editor (including AMS features), graphics and tables as already mentioned, and you get support for lots of addon packages available on CTAN. The power of LaTeX is not taken away (from references through ligatures everything is available in the GUI), and for those rare advanced cases you can use plain latex source as well, if you insist on it. With my LaTeX-knowledge, I usually only have the need to edit the preamble in order to customize the document to my needs, and the actual editing happens as comfortably as in a word processor -- with the remarkable distinction that the output happens in LaTeX-quality. In addition, I can \"sell\" LyX to non-techie average users who would (rightly so, given their experience) be frightened by LaTeX source code."
    author: "Pete"
  - subject: "Re: What about LyX?"
    date: 2006-05-06
    body: "Well, i started with LyX myself. Now i use Kile.\nAnd about \"selling\" a app to average users..: a friend started using LaTeX and Kile after watching me writing a document in Kile (we were preparying a lecture). And after a day he learned everything he needed to know about writing proper scientific documents. I only had to help him install Linux. So far about \"selling\" :)\n\nI do miss a LyX-Style formula Editor in Kile, especially when dealing with big formulas, but for the rest i prefer Kile-Style editing."
    author: "Worf"
  - subject: "About Digikam & Krita"
    date: 2006-05-05
    body: "I'd be wonderful if these two applications could work together to\nachieve effects/plugin compatibility.\nI don't know how hard would this be or if it is feasible at all,\nbut for sure it'd be a great asset for KDE as a graphics editing platform.\nAnd seeing that both apps are now supporting the same advanced\nfeatures as 16 bits per channel, etc. makes one dream this could\neventually happen! :)\n\nIf both Digikam and Krita could use the same effects, perhaps, there could be a central place where they could reside so that the user would just need to install new effects to that location and the programs would automatically use them.\nI know... I'm just dreaming...  :)\n\nGreetings,\nGabriel\n"
    author: "Gabriel Gazz\u00e1n"
  - subject: "Re: About Digikam & Krita"
    date: 2006-05-05
    body: ">  there could be a central place where they could reside \n>  so that the user would just need to install new effects \n>  to that location and the programs would automatically use them\n\nThere are too many central places already. If the user needs to put them somewhere, it means the programs don't automatically use them.\n"
    author: "ac"
  - subject: "Re: About Digikam & Krita"
    date: 2006-05-06
    body: "Just because the user needs to copy a file in, doesn't mean that a program can't automatically use that file.\n"
    author: "Trejkaz"
  - subject: "Re: About Digikam & Krita"
    date: 2006-05-07
    body: "I think perhaps you're talking about Kipi plugins.\nThe idea of Kipi was a great one, but without support for 16 bits, nor other color spaces than RGB, in the end it proved not much useful for professional editing (and painting, ...I know) programs, like Krita intends to be.\n\nWith respect to the concept you expose, that says if users need to place the plugins in \"a location\", its use could not be called \"automatic\"... well, I don't really understand it.\nThe idea is clear, a place for graphics editing plugins to reside, where any program (API compatible) could look for to find extensions to its native functions.\nSounds bad to you?\nTo me it sounds like something convenient that could save many man-hours of \"reinventing the wheel\" kind of work.\nIf it can be done starting from current digiKam ImageEditor plugins it's another story, of course.\n\nGreetings,\nGabriel\n"
    author: "Gabriel Gazz\u00e1n"
  - subject: "Re: About Digikam & Krita"
    date: 2006-05-05
    body: "DigiKam already split most of their image processing and utility tools out into 2 packages, kipi and digikam-plugins.  A number of other programs such as Gwenview and Kimdaba use the kipi library, so those resources are very much shared.  I understand that the Digikam-plugins rely a lot on the internal image model of digikam, and so krita may find them hard to reuse (and vice versa).  However, many of the Digikam filters and the like are based on calling other projects like cimg, or adaptations of various gimp plugins, so there is some reuse there too.\n\nJohn."
    author: "Odysseus"
  - subject: "Re: About Digikam & Krita"
    date: 2006-05-06
    body: "Digikam already has a plugin system that it shares with other apps.\n\nBut really Krita is such a different kind of app then Digikam, doesn't make much sense to share plugins with it. If you want a Digikam-esque image editor, try showfoto. It rocks."
    author: "Ian Monroe"
  - subject: "Re: About Digikam & Krita"
    date: 2006-05-06
    body: "More than digikam-esque, it is digikam!  Or rather the photo-editor part of digikam as a standalone app for those who want the editing without the file managment.  You got to admire the digikam guys for their architecture and focus on re-use :-)\n\nJohn"
    author: "Odysseus"
  - subject: "Re: About Digikam & Krita"
    date: 2006-05-07
    body: "I don't agree.\ndigiKam is implementing a lot of image editing functions as plugins, and these functions are also very common in everyday use of a program like Krita, Gimp or Photoshop. We're talking about such things as color correction, cropping and rotation of images, blur & sharpenning. This is the bread and butter of every image editing program.\n\nWith respect to Kipi plugins I know they exist, but for some reason they don't support 16 bit images and other advanced features, so I think there's not much future in them for professional use.\n\nGreetings,\nGabriel\n\n"
    author: "Gabriel Gazz\u00e1n"
  - subject: "Re: About Digikam & Krita"
    date: 2006-05-07
    body: "Well I've never used krita, but I can understand your point of view.\nSo a good way to proceed could be to implement in kipi-plugins the\n16 bit images support and allow in suach a way all the kipi host \napplications to use it - adding a new one krita :).\nKipi is trying to have a new life and going on with the developers\nold and new that can help. New plugins and plugin maintainers and \nnew kipi features are more than welcome. \nCould it be this the way to?\n\nAngelo "
    author: "Angelo Naselli"
  - subject: "Spelling error"
    date: 2006-05-05
    body: "you wrote:\n\"QtiPlot is a tool tha is useful\"\n- - - - - - - - - - - - - -^^^^ \n\nhope that helps\n"
    author: "markus"
  - subject: "Ideas for Part IV and V wanted"
    date: 2006-05-05
    body: "If you have an idea which apps should be covered in the next parts just list them in a reply to this post. Current ideas are \n\n* Rosengarden and \n* Kopete 0.12 and\n* Konversation 0.19 and\n* basket."
    author: "Carsten Niehaus"
  - subject: "Re: Ideas for Part IV and V wanted"
    date: 2006-05-05
    body: "Lmms"
    author: "Gerd"
  - subject: "Re: Ideas for Part IV and V wanted"
    date: 2006-05-05
    body: "Another interesting app could be KTorrent.\nThe upcoming Kubuntu 6.06 needs seeders :D\n\nFor developers KDevelop would surely be a nice app to learn about.\nAnd maybe KDiff3 and Kompare."
    author: "J\u00fcrgen Nagel"
  - subject: "Re: Ideas for Part IV and V wanted"
    date: 2006-05-06
    body: "Yes, KDevelop 3.4 will bring some improvements for KDE 3 users :-)"
    author: "birdy"
  - subject: "Re: Ideas for Part IV and V wanted"
    date: 2006-05-05
    body: "basKet is very very good. Looks like the author is enthusiastic about the project and looks to me as though it could go very very far. =) Couldn't we get an interview with the guy? ;-)"
    author: "apokryphos"
  - subject: "Re: Ideas for Part IV and V wanted"
    date: 2006-05-05
    body: "* kruler\n* kdict"
    author: "m."
  - subject: "Re: Ideas for Part IV and V wanted"
    date: 2006-05-06
    body: "qalculate\nkomparator\nlabplot"
    author: "AC"
  - subject: "Re: Ideas for Part IV and V wanted"
    date: 2006-05-06
    body: "kmymoney"
    author: "vrabcak"
  - subject: "Re: Ideas for Part IV and V wanted"
    date: 2006-05-06
    body: "http://rkward.sourceforge.net/\nhttp://labplot.sourceforge.net/"
    author: "anders"
  - subject: "Re: Ideas for Part IV and V wanted"
    date: 2006-05-06
    body: "I think\nhttp://www.englishbreakfastnetwork.org/\n\nis a service which is of great interests. \n\nIt is for janitorial work, right?"
    author: "Andy"
  - subject: "Re: Ideas for Part IV and V wanted"
    date: 2006-05-06
    body: "EBN is for developers, not for users. So this doesn't fit into this series. But yes, EBN is pretty cool."
    author: "Carsten Niehaus"
  - subject: "Re: Ideas for Part IV and V wanted"
    date: 2006-05-07
    body: "I think this is a whole category for coverage:\n\n* valgrind\n* the tools Mark Shuttleworth spoke about in wiesbaden like translation tools: Launchpad\n* KBabel"
    author: "Andy"
  - subject: "Re: Ideas for Part IV and V wanted"
    date: 2006-05-07
    body: "I guess Krename isn't exactly a killer app, but I find it highly useful."
    author: "Heja AIK"
  - subject: "Re: Ideas for Part IV and V wanted"
    date: 2006-05-08
    body: "Well, showing off some of the more funky features of KRename, like its ability to handle TagLib supported meta information, would show it as one such - i generally use amaroK's functionality for the same, but it is a really powerful thing :)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Thank you Kile!"
    date: 2006-05-06
    body: "Kile helps me a lot with my BSc thesis ;) Amazing tool!"
    author: "Rezza"
  - subject: "What about kimdaba"
    date: 2006-05-08
    body: "Kimdaba is another great application for managing pictures.\nThe great feature in it is the categorization of pictures under various categories.\n\nAh! Just figured out that Kimdaba is now known as KPhotoAlbum\n\nhttp://ktown.kde.org/kphotoalbum/\n\nRitesh"
    author: "Ritesh Raj Sarraf"
---
KDE 3.5 is a vivid platform. We looked at some reasons why  <a href="http://dot.kde.org/1144660487/">three weeks ago</a> and also <a href="http://dot.kde.org/1145962774/">last week</a>. Today, we look at the photo-manager digiKam, the plotting application QtiPlot, the LaTeX-dreamteam Kile and KBibTeX and the upcoming KDE 3.5.3 release.
























<!--break-->
<h2>digiKam</h2>

<div style="border: solid thin grey; float: right; padding: 1ex; margin: 1ex">
<a href="http://static.kdenews.org/jr/all-about-apps-digikam.png"><img src="http://static.kdenews.org/jr/all-about-apps-digikam-wee.png" width="250" height="176" border="0" /></a>
</div>
<p>If you have a digital SLR or other high-quality digital camera, you have probably noticed that the photos stored on it are not just simple JPEGs. High-end cameras offer the RAW file format, which stores the unmodified data from the cameras sensors. This means no quality is lost, and you can get 100% clarity out of your pictures. Many cameras also offer 12 bit and 16 bit colourspaces. Unfortunately, GIMP doesn't support these colourspaces and also cannot handle <a href="http://www.littlecms.com/">LCMS</a> colour schemes. </p>

<p>Now, KDE provides not only Krita which supports both 16 bit colours and colour profiles, but also <a href="http://www.digikam.org">digiKam</a> which fully supports these advanced features!</p>

<p>There is also now the possibility to geo-reference your pictures.  Have a <a href="http://www.digikam.org/?q=node/104">look</a>.  As you can see, you can mark where a picture was taken. For more pictures take a look at the new <a href="http://www.digikam.org/?q=image">galleries</a> of digiKam 0.9!</p>

<p>If you want to shorten the time until digiKam 0.9 is finished you can test <a href="http://www.digikam.org/?q=node/117">0.8.2</a> which includes some small improvements over 0.8.1 including CMYK colourspace for .jpg-pictures.</p>

<h2>Kile</h2>
<div style="border: solid thin grey; float: left; padding: 1ex; margin: 1ex">
<a href="http://static.kdenews.org/jr/all-about-apps-kile.png"><img src="http://static.kdenews.org/jr/all-about-apps-kile-wee.png" width="250" height="170" border="0" /></a>
</div>

<p><a href="http://kile.sourceforge.net/">Kile</a> is KDE's LaTeX-editor of choice. Version 1.9 was released on March 17 and makes Kile even better. The author not only fixed over 50 bugs, but also made Kile easier to use. The two most noteworthy changes are <i>auto-completion of (La)TeX commands</i> and <i>compiling, converting and viewing your document with one click</i>. Finally, KBibTex can now be used from within Kile.</p>

<br clear="all" />
<h2>QtiPlot</h2>

<p><a href="http://soft.proindependent.com/qtiplot.html">QtiPlot</a> is a tool that is useful for everyone who needs scientific plots ready for publication. It supports <a href="http://soft.proindependent.com/error_bars.html">error bars</a>, <a href="http://soft.proindependent.com/fit.html">value fitting</a>, <a href="http://soft.proindependent.com/multi_peak_fit.html">multi-peak fitting</a> and many other advanced techniques for analysing your data. Since November 2005, the author has released no less then eight new versions, each providing you with even more features and of course many bugfixes.</p>

<h2>KBibTeX</h2>

<div style="border: solid thin grey; float: right; padding: 1ex; margin: 1ex">
<a href="http://static.kdenews.org/jr/all-about-apps-kbibtex1.png"><img src="http://static.kdenews.org/jr/all-about-apps-kbibtex1-wee.png" width="250" height="170" border="0" /></a>
</div>

<p>Apropros <a href="http://www.unix-ag.uni-kl.de/~fischer/kbibtex/">KBibTeX</a>, a BibTeX editor for KDE which is now integrated in Kile. Last week, the author released <a href="http://www.unix-ag.uni-kl.de/~fischer/kbibtex/download.html">version 0.1.4</a> which offers you an incredible list of <a href="http://www.unix-ag.uni-kl.de/~fischer/kbibtex/changelog.html">changes</a>. While the version 0.1.4 might sounds like experimental code I can promise you that KBibTeX is indeed very mature.
If you are looking for screenshots <a href="http://kile.sourceforge.net/screenshots.php">go here</a>!</p>

<!--
<div style="border: solid thin grey; float: left; padding: 1ex; margin: 1ex">
<a href="http://static.kdenews.org/jr/all-about-apps-kbibtex2.png"><img src="http://static.kdenews.org/jr/all-about-apps-kbibtex2-wee.png" width="250" height="170" border="0" /></a>
</div>
-->

<h2>KDE 3.5.x itself</h2>

<p>Last not least, KDE itself is a reason why KDE 3.5 is alive! Until KDE 3.5, when a KDE-release was frozen in preparation to be released, new features  and new user-visible text ("strings") were not allowed in later releases in the series in order to make sure that no new bugs were introduced and no translations were broken. For KDE 3.5 this is different. </p>

<p>Since the KDE 4.0 development cycle will take longer than typical due to the scope of the changes and improvements being undertaken, the developers want KDE 3.5.x to be the best possible KDE for users in the meantime. Therefore, KDE 3.5.1 and 3.5.2 not only fixed a lot of bugs (see <a href="http://www.kde.org/announcements/changelogs/changelog3_5to3_5_1.php">3.5.1-fixes</a> <a href="http://www.kde.org/announcements/changelogs/changelog3_5_1to3_5_2.php">3.5.2-fixes</a>), but the developers were also allowed to change some strings in KDE so that many usability-improvements and updates in documentation found their way into KDE 3.5.x.</p>

<div style="border: solid thin grey; float: left; padding: 1ex; margin: 1ex">
<a href="http://static.kdenews.org/jr/all-about-apps-kbibtex2.png"><img src="http://static.kdenews.org/jr/all-about-apps-kde-desktop.png" width="300" height="228" border="0" /></a>
</div>

<p>KDE 3.5.3 will even include some new features. When a new feature is already in the upcoming KDE 4-code, well-tested and known to work in KDE 3.5, the author of the code can ask for inclusion in KDE 3.5.3. If no other developer objects, the new feature can be added. Therefore, you will see some new stuff in <a href="http://www.kde.org/announcements/changelogs/changelog3_5_2to3_5_3.php">KDE 3.5.3</a>. (This list is constantly being updated and not yet complete.)</p>

<p>Stay tuned for KDE 3.5.3 at the end of the month!</p>








