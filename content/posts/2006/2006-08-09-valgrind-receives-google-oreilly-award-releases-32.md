---
title: "Valgrind Receives Google-O'Reilly Award; Releases 3.2"
date:    2006-08-09
authors:
  - "cniehaus"
slug:    valgrind-receives-google-oreilly-award-releases-32
comments:
  - subject: "Valgrind rules"
    date: 2006-08-09
    body: "Valgrind has proven to be a very, very good tool indeed. It's actually so good that we develop on Windows, but profile our applications with Valgrind on Linux!\n\nCongratulations to Julian Seward for his awesome work!"
    author: "Harry"
  - subject: "Congratulations and thanks!"
    date: 2006-08-09
    body: "Congratulations for Julian! Enjoy the champagne. Valgrind is a great tool to have in your shed. It surely saved me a couple of head-aches looking for memory leaks and without exaggeration, I can say that I could speed up my code with 30% thanks to its profiling capbilities. In fact, a print-out of a profiling diagram created with KCachegrind using Valgrind data is still on my white board to trigger the curiosity of people entering my office in the hope to convince them to use Valgrind ;-)"
    author: "gert"
  - subject: "Congrats; typo in text"
    date: 2006-08-10
    body: "Congratulation to Julian from here as well!\n\nThe message above contains a typo: it should say \"and the addition of the popular Callgrind\" instead of cachegrind. Cachegrind has been shipped with valgrind forever, only callgrind was available as a separate package, which has changed in valgrind-3.2 where it was included.\n\nWay to go!"
    author: "cstim"
  - subject: "Re: Congrats; typo in text"
    date: 2006-08-10
    body: "Fixed.\n"
    author: "Jonathan Riddell"
  - subject: "Valgrind! great tool!"
    date: 2006-08-10
    body: "Congratulations!\nI have just utilized this tool for short time, but it already helped\nme solve very critical issues and save me lots of time.\nMy grateful thanks to the great tool and author!"
    author: "Caisheng Liu"
  - subject: "valgrinding"
    date: 2006-08-10
    body: "We use it in Amarok, its quite helpful. But (understandably) very slow - good to hear about the speed improvements. :)"
    author: "Ian Monroe"
  - subject: "You are the best Julian"
    date: 2007-05-04
    body: "Once I learned valgrind there was no going back... \n\n"
    author: "Pedram Nimreezi"
---
Julian Seward, father of the the famous <a href="http://valgrind.kde.org">Valgrind</a>, an opensource tool for debugging and profiling your applications, won this years Google-O'Reilly Open Source Award for "Best Toolmaker". This years ceremony was the second of the annual <a href="http://google-code-updates.blogspot.com/2006/07/and-winners-are.html">event</a>. Congratulations, Julian! In other news, <a href="http://valgrind.org/docs/manual/dist.news.html">Valgrind 3.2</a> has been released. The two most notable changes are huge speed and memory gains in Memcheck (up to 30% faster) and the addition of the popular Callgrind.  Additionally, the valgrind-based profiler frontend <a href="http://kcachegrind.sourceforge.net/cgi-bin/show.cgi">KCacheGrind</a> is available as a seperate package.


<!--break-->
