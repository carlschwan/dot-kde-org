---
title: "First Development Snapshot of KDE4: \"Krash\""
date:    2006-08-19
authors:
  - "sk\u00fcgler"
slug:    first-development-snapshot-kde4-krash
comments:
  - subject: "All I can say is ..."
    date: 2006-08-19
    body: "I feel like a kid in a candy store, time to start compiling ;)"
    author: "imbrandon"
  - subject: "Re: All I can say is ..."
    date: 2006-08-19
    body: "Beware, this is a candy store only half filled and with some pretty old candies in between. Nothing, really nothing spectacular by now. Only on parts of the API. Something you see when coding, not compiling."
    author: "Anonymous"
  - subject: "Re: All I can say is ..."
    date: 2006-08-19
    body: "> Beware, this is a candy store only half filled and with some pretty old candies in between.\n\nLOL. Well put.\n\n"
    author: "Martin"
  - subject: "Just a warning"
    date: 2006-08-19
    body: "Before anyone goes ahead and compiles/runs this, please be aware that it will look exactly like KDE 3.5 - except for being broken in lots of places.\n\nThe good news for developers is that this is a reasonably solid code base for diving into KDE / Qt 4 hacking.  By solid I mean that it actually compiles reliably and most programs at least start without crashing ;)\n\n\n\n"
    author: "Robert Knight"
  - subject: "Screenshot"
    date: 2006-08-19
    body: "Can I see some screenshot anywhere?\n\nThanks! :)"
    author: "Pinucset"
  - subject: "Re: Screenshot"
    date: 2006-08-19
    body: "Given that the GUI looks essentially the same as the current 3.5.x release, I don't think screenshots would show you anything interesting."
    author: "CPT"
  - subject: "Re: Screenshot"
    date: 2006-08-19
    body: "Imagine KDE 3 with more crash dialogs.\n\nRich."
    author: "Richard Moore"
  - subject: "Re: Screenshot"
    date: 2006-08-19
    body: "ooops, so what's for plasma I hope aaron are making real progress "
    author: "morphado"
  - subject: "Re: Screenshot"
    date: 2006-08-20
    body: "I think you might be interested in latest Aaron's blog entry: http://aseigo.blogspot.com/2006/08/cooking-belaying-delaying.html , he talks about plasma toward the end."
    author: "Med"
  - subject: "Re: Screenshot"
    date: 2006-08-22
    body: "Just look at a 3.5 install.. there you go..\n\nThey stated that the eye candy has not changed, yet... its all underneath."
    author: "ziggy"
  - subject: "Decibel"
    date: 2006-08-19
    body: "What is it? I never heard of this project before.."
    author: "bsander"
  - subject: "Re: Decibel"
    date: 2006-08-19
    body: "Not sure if there are more uptodate news anywhere:\n\n http://decibel.basyskom.org/"
    author: "Carsten Niehaus"
  - subject: "Re: Decibel"
    date: 2006-08-19
    body: "Visit http://decibel.kde.org"
    author: "Anonymous"
  - subject: "Re: Decibel"
    date: 2006-08-19
    body: "Yeah, that link was also in the posting, but it has nothing for users, only developers, and that information is kind of unclear to me :("
    author: "bsander"
  - subject: "Re: Decibel"
    date: 2006-08-19
    body: "Tapioca is a Voip framework developed by Nokia. Using the \"example\" application I was able to use Google Talk voice for the first time on my GNU/Linux system. :)"
    author: "Anonymous Coward"
  - subject: "Tenor?"
    date: 2006-08-19
    body: "I remember that the \"Tenor\" Contexual Linking Engine (http://dot.kde.org/1113428593/) was at one point going to be one of the cornerstones of KDE4.  Is there any progress on that, or has it been (un)officially canned? It would be a shame if that were the case :("
    author: "Anon"
  - subject: "Re: Tenor?"
    date: 2006-08-19
    body: "There's a 'strigi' desktop search project that seems to be advancing fast, but contextual linkage is more than full-text search. Actually a full-text index doesn't contain explicit contextual information of any kind, and it can only be used to infer textual resemblances.\n\nWhat I mean is you can notice that two files contain similar words, but you can't, for instance, notice that they are usually edited simultaneously. (Which would be nice to know within 'Open...' stuff).\n\nTo answer your question, yes, contextual linkage in KDE 4 is dead, unless somebody can tell us more..."
    author: "aaa"
  - subject: "Re: Tenor?"
    date: 2006-08-19
    body: "well, dead is a big word, there is still some thinking about it. but nothing concrete, no."
    author: "superstoned"
  - subject: "Re: Tenor?"
    date: 2006-08-19
    body: "Something like this for KDE4 will come. But I would bet not for KDE 4.0."
    author: "Anonymous"
  - subject: "Re: Tenor?"
    date: 2006-08-19
    body: "All that I found with Google is that Tenor \"Chief Architect\" Scott Wheeler is leaving SAP Linux Labs..."
    author: "MM"
  - subject: "Re: Tenor?"
    date: 2006-08-19
    body: "Which is hardly relevant given that SAP never paid him to work on KDE."
    author: "Daniel Molkentin"
  - subject: "Re: Tenor?"
    date: 2006-08-19
    body: "Didn't knew about that. On the other hand its still the only information that looks relevant. The official kde.org site does not give any hint on the status of neither Kat, nor Tenor; the latest information I found is dated 2005. SVN activity is at a low level (latest update 4 month ago).\n\nIMHO, the fact that Scott leaves a Linux related job in favor of a more multimedia related one (whatever this means in detail), _may_ be more relevant than it looks at a first glance. In his blog he wrote: \"KDE has slipped to the background of late and like many aging ... F/OSS hackers I'm left wondering if that's a real transformation -- a shift in priorities -- or simply a phase that will be revisited once life settles down a bit...\" (http://www.kdedevelopers.org/blog/72, 06/29/2006), even if it just means that instead of Tenor he prevers to work on Phonon (http://www.kdedevelopers.org/node/2007) or something else any time in the near future.\n\nMaybe this is the time to look for some assistance for Scott?\n"
    author: "MM"
  - subject: "Re: Tenor?"
    date: 2006-08-21
    body: "I suppose I can drop in some details here:\n\nDaniel was correct that SAP never significantly supported my personal KDE related projects.  (There were however a few times that there were features or fixes that SAP needed inside of KDE that I was allowed to work on, but we're talking about maybe one week of KDE work per year.)\n\nAt my current job I'm doing cross-platform pro-audio stuff, which also has nothing to do with KDE, but for the moment I rather enjoy.\n\nSponsorship for my KDE work has never been something that I've sought.  KDE is one of my hobbies and I'm fine with it staying that way.  (Not to mention that I have to have normal full-time employment to remain in Germany where I've been for several years.)\n\nSo, then what's with Tenor?\n\nLike I said above, KDE is my hobby.  Sometimes I feel like working on it, sometimes I don't.  (My life has also been really busy in the last few months, but really it's more of a matter of motivation than time.)  When I don't feel like working on it, I don't.  It's really that simple.\n\nThe desire to work on Tenor for me comes in waves.  A couple months back I spent a few weeks hacking on it again.  I haven't touched it since then.\n\nSo, will Tenor be in KDE 4?\n\nMaybe.  Really it depends on if and when I feel like hacking on it or if someone else decides to pick it up and run with it.  Think of it as a surprise.  ;-)\n\nOne thing that some people who ask me about this find interesting is this graph:\n\nhttp://developer.kde.org/~wheeler/images/taglib-development.png\n\nThat, aside from being my first (and likely only) time to play with the Perl bindings for Qt, was the number of changes going into TagLib before I moved it into KDE's CVS.  Every line is about a month (30 days).  The main difference between how I've hacked on TagLib vs. Tenor is just that I kept very quiet about TagLib before I was ready to release.  That's been my modus oparandi for most of what I have developed.  (For instance, I was already using JuK as my day-to-day player before anyone else knew it existed.)\n\nHowever, just from a searching perspective, I'm pretty excited about Strigi.  I've talked a bit with the developers and almost all of the work that they're doing is orthogonal to the interesting parts of Tenor and the design of Strigi impresses me more than Kat did (both from an API and information retrieval perspective).  They've got multiple backends and building one that used the Tenor store would not be terribly difficult.\n\nHmm, I'll probably blog this as well since most of the activity on this article is from a couple days back.  Hopefully this clears up some of the current Tenor ambiguity."
    author: "Scott Wheeler"
  - subject: "Re: Tenor?"
    date: 2006-08-21
    body: "Blogged here:\n\nhttp://www.kdedevelopers.org/node/2279"
    author: "Scott Wheeler"
  - subject: "Re: Tenor?"
    date: 2006-08-21
    body: "Many thanks for your response. I'm really happy and delighted to read that Tenor is definitely not dead, as some people claim ;-)\n\nOne thing I am wondering about is, whether it is too early to change KDE apps to support Tenor, as soon as it is there.\n\nI don't know if the API is already there, but even if not, it may be interresting to see what happens, if there's a DCOP (or DBUS) service that accepts notifications (e.g. from KMail saying that 'this' document has been 'sent' from 'xy' by 'email'), so everyone (may it be Tenor or any program else) is able to connect to that service for listening what happens on the K desktop.\n\nE.g., someone may find that it would be a good thing if KDE apps would also send notifications on what documents they are now going to open, and which one they are going to close (and to utilize this in some kind of 'task menu', that shows currently open documents, instead of 'tasks').\n\nI'm not sure if this is the way to go. Is DCOP/DBUS a viable solution for this kind of task? Or is a library better? Is it too early to create an API now or does it already exist?\n\nHowever, thank you for answering, and for your work."
    author: "MM"
  - subject: "Re: Tenor?"
    date: 2006-08-19
    body: "Seems like Tenor is not so important now that there's a decent KDE frontend to Beagle.  I've been using it in SuSE and it's quite nice.\n\nhttp://www.kdedevelopers.org/node/1820"
    author: "jayKayEss"
  - subject: "Re: Tenor?"
    date: 2006-08-19
    body: "This is not the point at all. Beagle is a desktop search engine. Tenor is/was supposed to be a contextual linkage engine, see my previous post for the difference among the two."
    author: "aaa"
  - subject: "Re: Tenor?"
    date: 2006-08-19
    body: "Isn't that a file system job?"
    author: "hill"
  - subject: "Re: Tenor?"
    date: 2006-08-19
    body: "Ok, its been a while since I've posted here, what the HECK is with that crazy anti-spam thing? Thats funny...\n\nIt isn't at the moment, but it should be. Unfortunately the three most used fileystsme (Ext3, XFS, and my favorite, ReiserFS) don't support this, and don't support plugins. Reiser4 does, but nobody uses it since it's not in the kernel yet.\n\nBut then someone would still have to write a plugin for Reiser4 to make it handle contextual linkage, which would majorly increase read/write time for the filesystem and also increase the filesystem's footprint. \n\nFor now its best left to userspace, where we can pick and choose what gets what kind of information."
    author: "Aaron Krill"
  - subject: "Re: Tenor?"
    date: 2006-08-20
    body: "No, Tenor was meant to be able to search in ways such as 'file John sent me' and be able to see all files that you received from 'John' (whether you got them via an email in KMail, or over IM in Kopete).  Stuff like that beagle can't do without lots of help, because the programs (in this case, kmail and kopete) would need to include the functionality to say either in the extended attributes of the file, or tell some central DB that 'John sent this file'.  The file system has no way to know which program created/modified the file, much less that it was John that sent the file to you.\n\nSo the difference:\nWith Beagle you search for stuff in the file (eg, contains the phrase \"the dogs are weird!\"), with Tenor you would search for stuff ABOUT the file(eg, \"file I sent John\" or \"file I downloaded from svn.kde.org\")."
    author: "Corbin"
  - subject: "Re: Tenor?"
    date: 2006-08-20
    body: "Personally, I don't want to even touch that Mono thing. In Gnome camp they are desperate to use Mono because it's clear that GTK and C are not leading them anywhere, but KDE has already a very nice and not encumbered development framework. Beagle solves an interesting problem, but a pure KDE solution is still needed IMHO."
    author: "Derek R."
  - subject: "Re: Tenor?"
    date: 2006-08-20
    body: "C is not THAT bad, and mono uses gtk# for it's gui.\nthe only real benifit i see is that this will give you a object orientated langauge."
    author: "Mark Hannessen"
  - subject: "Re: Tenor?"
    date: 2006-08-20
    body: "http://www.glscube.org/ is a good alternative, because it's real."
    author: "idoric"
  - subject: "GLScube"
    date: 2006-08-27
    body: "GLScube sounds good and appears more elaborate than Strigi, but as other posters note, the issue is getting the semantic details to the search subsystem automatically.  If I save an attachment from an e-mail message, I want \"the system\" to set all the \"Sent by John\" and \"Subject of original e-mail\" metadata; there's no way I'm going to enter all that metadata myself every time I click Save.\n\nAlso: \"glscubefs... is a user-level file system that encapsulates the features of GLS\u00b3 in a traditional file system interface\".  Another day, another VFS!\n\nEventually there will have to be standard APIs for apps to provide metadata on save and other events."
    author: "skierpage"
  - subject: "Re: GLScube"
    date: 2006-08-28
    body: "Re: Also: \"glscubefs... is a user-level file system that encapsulates the features of GLS\u00b3 in a traditional file system interface\". Another day, another VFS!\n\nWhat's wrong with that? It only allows application that doesn't use directly GLS3' interface to use search functionality in it's file open dialogs...\n\nThe GLS doesn't use Fuse for indexing files.\n"
    author: "Milan"
  - subject: "Win32"
    date: 2006-08-19
    body: "How about a Win32 snapshot? I think I heard that mingw and MSVC compile kdelibs fine at this point (although D-BUS might still be an issue)"
    author: "Darkstar"
  - subject: "Re: Win32"
    date: 2006-08-20
    body: "hehe, without really knowing it, I would think the source is all you need to compile it in a windows environment."
    author: "pascal"
  - subject: "Re: Win32"
    date: 2006-08-21
    body: "Take a look at http://www.kdelibs.com/wiki/index.php/Main_Page\n\nkdelibs4 compiles out of the box with msvc and mingw (there may be errors due to some commits, but normally fixed in a few days).\n\nCurrently the biggest problem is dbus (http://sf.net/projects/windbus) - when this works all kdelibs-test need to be fixed for win32."
    author: "ChristianEhrlicher"
  - subject: "Portland"
    date: 2006-08-19
    body: "http://portland.freedesktop.org/wiki/IntegrationTasks\n\nDoes the Krash preview already implement the new Portland intergration stuff?"
    author: "hill"
  - subject: "Re: Portland"
    date: 2006-08-19
    body: "This stuff is implemented by the XDG utils not by the desktop itself. The utils are currently in beta and can be downloaded from the portland site.\n"
    author: "Richard Moore"
  - subject: "SUSE rpms"
    date: 2006-08-19
    body: "Read http://www.kdedevelopers.org/node/2271"
    author: "Anonymous"
  - subject: "kde on Win ? "
    date: 2006-08-19
    body: "is there a chance, that one glory day we'll see KDE running natively on Windows? because I miss it every time I am forced (you know, *the evil forces*) to use a Windowsmachine. i miss Koffice and Knotes and Kalarm and Kontact and most of all I miss my beloved Konqueror, because it is much better, faster and much more convenient than Firefox."
    author: "cobalt"
  - subject: "Re: kde on Win ? "
    date: 2006-08-19
    body: "Yes, this is planned for KDE 4. The development code already compiles on win32.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: kde on Win ? "
    date: 2006-08-19
    body: "thank you. :-))"
    author: "cobalt"
  - subject: "Re: kde on Win ? "
    date: 2006-08-19
    body: "Are we talking Cygwin here or is it the real deal?"
    author: "bsander"
  - subject: "Re: kde on Win ? "
    date: 2006-08-19
    body: "i meant \"the real deal\" and if I got it right, Richard meant the same."
    author: "cobalt"
  - subject: "Re: kde on Win ? "
    date: 2006-08-19
    body: "Yes, that's right. TrollTech released a GPL version of Qt 4 for win32 which makes it possible for us to support it natively in KDE too.\n"
    author: "Richard Moore"
  - subject: "Re: kde on Win ? "
    date: 2006-08-19
    body: "my english ist poor, but wouldn't that be a nice name for the Windows-KDE: \"KDE 4 The real deal\"? ,-)"
    author: "cobalt"
  - subject: "Re: kde on Win ? "
    date: 2006-09-01
    body: "No!  That would imply that the original KDE was inferior somehow."
    author: "Lee"
  - subject: "No way!"
    date: 2006-08-20
    body: "I wish it will never happen, there is enough work to do on kde4 itself and porting to win32 is sabotage. Here is a short list of reasons for those who still doubt http://www.fefe.de/nowindows/\n\nAnd if it ever happened, the win32 version would always remain a second-class citizen with more bugs, which would give bad publicity to the whole KDE project.\n"
    author: "bye"
  - subject: "Re: No way!"
    date: 2006-08-20
    body: "I agree that if it's too much work to make it as good as the *nix version and if's going to give a bad name to KDE it shouldn't be done.\n\nThe 'sabotage' argument is nonsense IMHO. First, because if you want people to leave Windows you just have to give them something better. No free OS is there yet, even if KDE is far superior as a desktop. And second, as long as open formats are used, I couldn't care less if my neighbour uses Windows or not. Promoting Free Software and open formats in the Windows platform *is* a good thing."
    author: "Derek R."
  - subject: "Re: No way!"
    date: 2006-08-20
    body: "There is now way, you could stop the convergence of operating systems. They are converging on several levels:\n\na) Virtualization. Every OS can run any other mainstream OS now.\nb) Emulation. You can run Linux programs on Windows and Windows programs on Linux fairly well now.\nc) Porting. More and more software starts to work on both platforms.\n\nAt the end, when people will look at things like cost, there is no way, a non-free OS can compete except where there is \"piracy\".\n\nBeing on Windows will attract a huge host of application developers, that will benefit KHTML (konqueror), etc. the mind share of many people is there now, and picking them up where they are, and not where they should be, is the right thing to do. Like working on Koffice :-)\n\nWhen KOffice runs on Windows, this will HURT the company Microsoft so bad, you cannot imagine. OpenOffice is a monster, KOffice may just be what people want.\n\nOne has to wonder, how long people can sustain to develop costly software where there is free alternatives that are tendentially better.\n\nAnd I suspect, KDE on Windows will see a influx of developers who pick up with their know how. Most of the hard issues are solved by QT already, Many of the Linuxisms have been seen in BSD and Solaris supports. So it's not going to be too bad...\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: No way!"
    date: 2006-08-21
    body: "word!"
    author: "cobalt"
  - subject: "Re: No way!"
    date: 2006-09-01
    body: "The other possibility, and the more likely one, is that KDE on windows would move ahead, just due to the number of developers available there, and that KDE/*ix would get left behind.  This happens with emulators that are available on both windows and *ix, such as UAE."
    author: "Lee"
  - subject: "Re: kde on Win ? "
    date: 2006-08-28
    body: "Unfortunately so"
    author: "Bobby Rockers"
  - subject: "Plasma"
    date: 2006-08-19
    body: "I am very excited to see some progress, but I must say there is little known about the plasma project. The plasma project website has been idle for some time and no one seems to post updates or screenshots on planetkde. \n\nSo my question to the developers is: Is it possible to post more preview screenshots or updates about plasma. Maybe some mockups about how it will look like and/or a technical document on functional decisions made. In my point of view, those two should be the starting point of development. It started nicely with the mockups, but which one made it and which one did not?\n\nMaybe I'm missing some documents which are already available, but when I would've been a developer, a technical document on how it should look like would help me a lot. :)"
    author: "Dennie"
  - subject: "Re: Plasma"
    date: 2006-08-20
    body: "Rejoice:\n\nhttp://slicker.sourceforge.net/\nhttp://www.slicker.org/index.php?page_id=110&language=en\nhttp://www.slicker.org/index.php?page_id=111&language=en\n"
    author: "Andr\u00e9 Goddard Rosa"
  - subject: "Re: Plasma"
    date: 2006-08-21
    body: "Yes, that's Slicker, not Plasma. Yes, some Slicker code is used in Plasma, but the concepts of Slicker and Plasma won't be the same (as I've been told by many on IRC and the mailinglists)."
    author: "Dennie"
  - subject: "waiting"
    date: 2006-08-19
    body: "I think its better to wait for later version :-)"
    author: "matt2ss"
  - subject: "Forget about Temor, Welcome Nepomuk"
    date: 2006-08-20
    body: "The european union is funding a \u008017 million project to build a \"Temor killer\" and Mandriva is one of the companies working at it. All the project will be released as open source:\n\"the Social Semantic Desktop[...] is a new computing paradigm that provides an advanced way to create, automate and structure information. The Social Semantic Desktop brings three major changes: the availability of contextual information on users' desktops, the shift from hierarchical to semantic storage of information, and advanced ways of assisting users in their day-to-day usage of computers. One common way to define the Social Semantic Desktop is to bill it as the technologies that allow computers to gather information in the same manner as the human brain does.\"\n\nHere is Mandriva press release about it:\nhttp://www.mandriva.com/en/company/press/pr/mandriva_to_participate_in_nepomuk_social_semantic_desktop\n\nhere is the project page:\nhttp://nepomuk.semanticdesktop.org/xwiki/bin/Main1/\n\nNepomuk is real, mandriva is paying full-time developers to work on this for kde4. So hope is not dead for contextual search engine for KDE4 :)"
    author: "Pat"
  - subject: "Re: Forget about Temor, Welcome Nepomuk"
    date: 2006-08-20
    body: "oh and I forgot:\n\"NEPOMUK will develop a comprehensive framework for the Social Semantic Desktop and will integrate with common desktop tools and environments, including KDE, GNOME, Mozilla and Eclipse. Furthermore, NEPOMUK will actively integrate with open source user and developer communities.\""
    author: "Pat"
  - subject: "Re: Forget about Temor, Welcome Nepomuk"
    date: 2006-08-20
    body: "And they will bring world peace. And cook coffee."
    author: "Anonymous"
  - subject: "Re: Forget about Temor, Welcome Nepomuk"
    date: 2006-08-20
    body: ">And they will bring world peace. And cook coffee.\n\nyep, and you should also expect money for nothing and chicks for free"
    author: "Pat"
  - subject: "Re: Forget about Temor, Welcome Nepomuk"
    date: 2006-08-22
    body: "And free Dire Straits mp3 downloads..."
    author: "Anonymous"
  - subject: "Again on Tenor"
    date: 2006-08-20
    body: "Has anyone checked out gsl3 (http://www.glscube.org)?"
    author: "MM"
  - subject: "Re: Again on Tenor"
    date: 2006-08-20
    body: "gsl3 and Tenor don't appear to be the same thing.  gsl3 requires YOU to tag the files, with Tenor it would be the application tagging the files (with information about the file, like for something you download off a website, Konqueror would include a tag on the file pointing to that website, for an attachment you got in an email and saved KMail would include a tag showing who sent it to you, etc).  Tenor would need the applications to be modified to 'tag' the file with the extra information."
    author: "Corbin"
  - subject: "Re: Again on Tenor"
    date: 2006-08-20
    body: "gsl3 of course let the user tag his or her files himself (BTW, what's wrong with that?), but what can stop KDE apps (or any other) to create some standard tags (e.g. KMail creates a tag \"this document has been sent to you by email from ...\", ...)? Maybe gsl3 does not provide the API to do this yet, but as it's Free Software, this should not be a showstopper.\n\ngsl3 does not provide much meta data and full text filters; but KDE has them. What would be nice is a wrapper around them so gsl3 can make use of them (I think Kiten already does the same for it's various indexing backends, but I don't know if CLucene is one of them).\n\nHowever, one of the main question may be if gsl3 is live or dead...\n"
    author: "MM"
  - subject: "Krash ;)"
    date: 2006-08-22
    body: "Funny you are reusing this name for another as-much-important milestones in KDE history. Wasn't Krash 1.89 the first 2.0 preview. I think 4.0 will change the face of KDE as much as 2.0 did.\n\nkeep it up guys ! awesome work.\n\n"
    author: "somekool"
  - subject: "Re: Krash ;)"
    date: 2006-08-22
    body: "Yeh I think you're right. I knew the codename \"Krash\" sounded familiar....\n\nCherios"
    author: "Johann Assam"
  - subject: "Speed of KDE4 snapshot?"
    date: 2006-09-11
    body: "I know that KDE4 is still unsable and probably won't complete any benchmark tests but it would be interesting to see if the speed gains that have everyone is waiting for as the port to QT4 is largely done in the libraries at least (or so i gather).\nHas anyone tried this port on their system and has there been any significant gain in speed? (I am just curious)\nHope that it is a lot quicker, not that KDE is overly sluggish on my system, but i look forward to the first alpha's or beta's of KDE4 :)"
    author: "rob"
---
Today, KDE <a href="http://kde.org/info/3.80.1.php">releases a first developer snapshot</a> of the 
upcoming KDE4 release. This snapshot is meant as a reference for developers 
who want to play with parts of the new technology KDE4 will provide, those 
who want to start porting their applications to the new KDE4 platform and 
for those that want to start to develop applications based on KDE4. This 
snapshot is not for end users, there is no guarantee that it will be stable, 
the interfaces are subject to changes at any time. The changes that have gone 
into the development version this snapshot is based on have all happened under 
the hood, little is visible yet. Now it is up to application developers to use 
the new possibilities. While this snapshot will probably not be what kdelibs 
will finally look like, it should give a fair idea of what to expect.




<!--break-->
<p>Developers can start porting their applications using this snapshot and 
investigate the new exciting technology. Highlights of this snapshot include:</p> 

<ul>
	
	<li>An initial port of <strong>kdelibs</strong>, <strong>kdebase</strong> and <strong>kdepimlibs</strong> to <a href="http://doc.trolltech.com/4.2/qt4-intro.html">Qt 4</a>, 	
		providing the developer with a <a href="http://zrusin.blogspot.com/2006/08/magic.html">wealth</a> 
		<a href="http://zrusin.blogspot.com/2006/08/svg-on-graphicsview.html">of</a> 
		<a href="http://zrusin.blogspot.com/2006/08/fun-with-svg.html">new</a> 
		<a href="http://zrusin.blogspot.com/2006/07/more-blurring.html">possibilities</a>. 
		This snapshot uses a preview of the <a 
		href="http://doc.trolltech.com/4.2/qt4-2-intro.html">upcoming Qt 
		4.2</a>. </li>

	<li><strong><a href="http://wiki.kde.org/tiki-index.php?page=DBUS">DBus</a></strong> will be the 
		Inter-Process-Communication protocol used for KDE 4. This snapshot contains an 
		initial implementation. With the use of DBus, KDE will feature improved 
		interoperability with other applications on the Free Desktop. Porting applications to DBus is explained <a href="http://wiki.kde.org/tiki-index.php?page=Porting%20KDELibs%20to%20D-Bus">on the porting wiki page</a>.</li>

	<li><strong>Phonon</strong> (<a 
		href="http://www.englishbreakfastnetwork.org/apidocs/apidox-kde-4.0/kdelibs-apidocs/phonon/html/index.html">documentation</a>) is another central feature of KDE4 providing a 
		unified multimedia backend that offers an easy way for application 
		developers to add multimedia capabilities to their applications.</li>

	<li><a href="http://www.cmake.org"><strong>CMake</strong></a> (<a 
		href="http://www.cmake.org/Wiki/CMake_FAQ">FAQ</a>) is the new buildsystem used for KDE4.</li>

</ul>

<p>Questions about KDE4 can be answered on various mailing lists such as <a 
href="https://mail.kde.org/mailman/listinfo/kde-devel">kde-devel</a> and <a href="https://mail.kde.org/mailman/listinfo/kde-buildsystem">kde-buildsystem</a>, as 
well as on #kde4-devel on irc.kde.org. Documentation for getting up to speed 
with KDE4 development is available from <a href="http://wiki.kde.org/tiki-index.php?page=KDE4">a number</a> <a href="http://developer.kde.org/build/trunk.html">of sources</a>.</p>

<p>Work is continuing on other pillars of KDE4, such as our <a href="http://plasma.kde.org">Plasma</a> desktop, <a href="http://solid.kde.org">Solid</a> hardware layer, <a href="http://www.oxygen-icons.org">Oxygen</a> artwork theme and <a href="http://decibel.kde.org/">Decibel</a> communication architecture.</p>

<p>KDE development has never before been as exciting as it is today, so start 
hacking today!</p>






