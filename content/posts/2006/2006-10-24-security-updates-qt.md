---
title: "Security Updates for Qt"
date:    2006-10-24
authors:
  - "msmith"
slug:    security-updates-qt
comments:
  - subject: "slackware packages"
    date: 2006-10-26
    body: "Are there slackware packages available?"
    author: "forrest"
  - subject: "Re: slackware packages"
    date: 2006-10-26
    body: "There is a Slackware package of 3.3.7 <a href=\"http://packages.slackware.it/package.php?q=11.0/qt-3.3.7-i486-1_slack11.0\">here</a> in the Slackware patches repository.  As for Qt 4, Slackware has never issued packages of it."
    author: "Matt Smith"
---
Trolltech has <a href="http://www.trolltech.com/company/newsroom/announcements/press.2006-10-19.5434451733">issued new patch releases</a> of all current versions of Qt.  Versions 3.3.7, 4.1.5 and 4.2.1 fix a security flaw which can be triggered by transforming specially-prepared pixmaps.  It is a recommended update, although no real-world security issues exploiting these flaws have been recorded so far.   Distributions are also releasing updated packages, note that distributions may release security-patched updates without changing the Qt version number.




<!--break-->
