---
title: "KDE at LinuxWorld Toronto 2006"
date:    2006-05-10
authors:
  - "Inorog"
slug:    kde-linuxworld-toronto-2006
comments:
  - subject: "XFree86"
    date: 2006-05-11
    body: "Interesting to hear that XFree86 was \"disbanded\" in 2003.\n\nMakes a person wonder who released version 4.5 in March of 2005.\n\nhttp://www.xfree86.org/releases/rel450.html"
    author: "James Richard Tyrer"
  - subject: "Re: XFree86"
    date: 2006-05-11
    body: "who cares as everyone else uses x.org."
    author: "Hup"
  - subject: "Re: XFree86"
    date: 2006-05-11
    body: "Beats me. They did disband:\n\nhttp://www.newsforge.com/software/04/01/02/0042218.shtml?tid=132"
    author: "Roberto Alsina"
  - subject: "Re: XFree86"
    date: 2006-05-11
    body: "It appears that many people have misunderstood that announcement:\n\nhttp://www.xfree86.org/developer/coreteam.html\n\nfor more info, see: \"In the Spirit of Open:  No More Core\"\n\non this page: http://www.xfree86.org/xnews/#core\n\nThe project continues except that it is no longer tightly controlled and (mis)managed by a core team."
    author: "James Richard Tyrer"
  - subject: "OT: SUSE, take 100"
    date: 2006-05-11
    body: "You have to feel sorry for SUSE:\nhttp://www.suseblog.com/?p=96\nhttp://www.linuxformat.co.uk/modules.php?op=modload&name=News&file=article&sid=326\n..\n\nThey're still trying to cover up the 'SUSE leaving KDE' PR fiasco. But the\nfact is, they did leave KDE in favor of inferior technology due to politics.\nAnd they're going to pay for this for a long, long time :/. OpenSUSE will\nstay a joke 'community wise' (SUSE community is KDE based) and when someone\ncomes up with other _viable_ KDE centric distro users are likely to flee as\nwell.\n\nThe problem is that RH and SUSE have managed to raise the cost of setting up\na distro high enough (this is done by not documenting & writing half finished\ncode) that it's not too likely for someone to get viable any time soon :/\n"
    author: "foo"
  - subject: "Re: OT: SUSE, take 100"
    date: 2006-05-11
    body: "My concern is that ISVs and distributors \"standardise\" on this inferior technology to the detriment of KDE. KDE depends on both vendor-paid and hobbyist hackers to succeed, and this requires market penetration. Right now KDE has it. But if it is abandoned for no good technical reasons, where does that leave the KDE project?\n\nI'm confident that everybody's aware of this potential problem and the KDE developers are going to make KDE 4.0 the most standards adherent, usable, powerful desktop interface ever released."
    author: "Alistair John Strachan"
  - subject: "Re: OT: SUSE, take 100"
    date: 2006-05-14
    body: "Novell's director of marketing, Greg Mancusi-Ungaro, is a GNOME zealot who used to  have the same position in Ximian before it was acquired by Novell. Ximian is the most awful company you can imagine, and they've always tried to smear and kill KDE:\n\nhttp://www.linuxnovice.org/main_focus.php3?VIEW=VIEW&t_id=161"
    author: "ac"
---
<a href="http://people.kde.org/cristian.html">Cristian Tibirna</a> represented KDE at <a href="http://www.lwnwexpo.plumcom.ca/">LinuxWorld Conference and Expo Toronto 2006</a>. Here follows his report of this event.








<!--break-->
<p>
At the request of KDE's representative for North America, George Staikos, and thanks to the sponsoring granted by <a href="http://plumcom.ca">Plum Comm. Canada</a>, the organizers of the Toronto LWCE, I was able to go to Toronto on April 25th, and to give a three hours tutorial:  <a href="http://ktown.kde.org/~ctibirna/2006-lwet/lwet2006-tibirna-kde-desktop-tutorial-FULL.pdf">Why KDE for the desktop</a> and a one hour conference:
<a href="http://ktown.kde.org/~ctibirna/2006-lwet/lwet2006-tibirna-desktop-hold.pdf">Take a Hold of the Rapidly Maturing Linux Desktop</a>.

<p>
I also wanted to run a KDE stand, but it turned out not to be possible for me (given work-related constraints) to stay through both days as required.</p>

<p>
My tutorial and conference didn't gather full rooms but I think the conference as a whole was a bit less active than usual. I found the experience interesting nevertheless. After the tutorial, two engineers came to me from Xerox, who showed to me the new printing driver dialog that they have written for Linux, which their colleague, David Salgado, had already mentioned to me at the Atlanta printing summit but didn't have the means to demo it to me.
Also, the president of a local software development company (Advidi of Nepean, Ontario), came to me after the tutorial and told me I helped him a lot to make a better idea and inform his choice for a development framework. I strongly hope that I helped him choose KDE ;-).</p>

<p>
Many of the attendees were eager to take home one of the Kubuntu CDs that <a href="http://www.kdedevelopers.org/blog/57">Jonathan Riddell</a> had kindly delivered to me beforehand.</p>

<p>
On the show floor, the usual commercial players were present but I only saw a small number of stalls for free software or community organisations. John Hall was at the Linux International booth but I didn't find an opportunity to meet him and make my greetings. Of the KDE people, Waldo Bastian was there and he did presentations on <a href="http://portland.freedesktop.org">Portland</a> and ODF in the on-floor conferences series.</p>

<p>
Overall, it was an interesting experience and for me an occasion to re-confirm the high profile of Free Software in general and KDE in paticular here in Canada.
</p>




