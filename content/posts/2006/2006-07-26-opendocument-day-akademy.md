---
title: "OpenDocument Day at aKademy"
date:    2006-07-26
authors:
  - "jriddell"
slug:    opendocument-day-akademy
comments:
  - subject: "which one"
    date: 2006-07-26
    body: "Akademy or aKademy? for once :)"
    author: "Hamster"
  - subject: "aKademy!"
    date: 2006-07-26
    body: "In Ludwigsburg 2004, when we first used this code name for the big annual KDE meeting, we deliberately picked the \"aKademy\" spelling, for various reasons. The most valid one is this:\n\n* it makes the term look more uniq, and therefor easier to differentiate   from the more generic words \"academy\" or \"Akademy\" (which are valid   words in more than one language).\n\nSo if someone a bit familiar in KDE parses \"aKademy\" inside a larger chunk of test he'd know more quickly about the specific meaning of this word. (It's also easier to establish as a \"household name\" if you use a slightly modified spelling...)\n\nCheers,\nKurt  (who does not know if anyone has changed that \"policy\" in the meantime)"
    author: "Kurt Pfeifle"
  - subject: "Re: aKademy!"
    date: 2006-07-26
    body: "I don't know... but I'm tired of constantly fixing it back... :)"
    author: "Navindra Umanee"
  - subject: "Re: aKademy!"
    date: 2006-07-26
    body: "Or shorter:\n\naK5"
    author: "funbar"
  - subject: "Re: aKademy!"
    date: 2006-07-26
    body: "That sounds more like a weapon to me ;)\n(Think AK-47)"
    author: "Firetech"
  - subject: "Re: aKademy!"
    date: 2006-07-26
    body: "wouldn't that be:\n\naK6, or aK2006\n\n?"
    author: "Vlad Blanton"
  - subject: "Re: aKademy!"
    date: 2006-07-26
    body: "aK + 5 letters"
    author: "funbar"
  - subject: "Re: aKademy!"
    date: 2006-07-26
    body: "It's also quite tiresome to get two notifications from akregator(RSS) every time because the title has changed :-("
    author: "pascal"
  - subject: "Akademy, aKademy!"
    date: 2006-07-29
    body: "YOu may as well have called it aKDEmy for all the fucking sense it makes\n"
    author: "Anon"
---
This year at aKademy, Tuesday 26th September will be <a href="http://conference2006.kde.org/codingmarathon/opendocumentday.php">OpenDocument Day</a>.  The OpenDocument format (ODF) is a Free document file format for saving and exchanging office documents.  KOffice was the first office suite to support OpenDocument and other programs have been following suit.  OpenDocument Day at aKademy offers software developers interested in ODF to exchange ideas, build relations and collaborate on all things ODF in an informal setting. Interested parties are being invited from several organisations and companies with an interest in the future of document exchange.  For more information see the aKademy <a href="http://wiki.kde.org/tiki-index.php?page=OpenDocument+%40+aKademy+2006">OpenDocument Day wiki page</a>.



<!--break-->
