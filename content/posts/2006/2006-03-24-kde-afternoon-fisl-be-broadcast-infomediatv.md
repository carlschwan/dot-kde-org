---
title: "KDE Afternoon at FISL to be Broadcast by InfomediaTV"
date:    2006-03-24
authors:
  - "hcastro"
slug:    kde-afternoon-fisl-be-broadcast-infomediatv
comments:
  - subject: "Have a nice time there"
    date: 2006-03-23
    body: "I hope all of you will enjoy the forum. Say hello to everyone I might know there. Unfortunately I only remember Huberto's name, and a special hi goes to him and his wife. ;-)"
    author: "Andras Mantia"
  - subject: "Re: Have a nice time there"
    date: 2006-03-24
    body: "Same here, I'll watch it from a remote place like last year. Ga\u00fachos, n\u00e3o deixem a oportunidade de ir a uma churrascaria com eles...\n"
    author: "Josef Spillner"
  - subject: "Damn"
    date: 2006-03-23
    body: "The funny thing is that FISL will happen in one extremity of the country almost completely opposite to me. As Brazil is such a _huge_ country (more than 4000Km wide in _any_ direction) and air tickets are pretty expensive here some people interested in the event will simply not come (me included). Maybe when FISL happens in a more central place (Bras\u00edlia, Rio de Janeiro, Belo Horizonte) it will be cheaper. For now it's closed to less-money-powered people. :("
    author: "Taupter (Cl\u00e1udio Pinheiro - Kopete)"
  - subject: "Re: Damn"
    date: 2006-03-24
    body: "Well, it's impossible to make happy greeks and troyians :)\nFor me, that liv here in Rio Grande do Sul, it's really good that it happens here, and it's also a good thing it is NOT on \"big centers\" like Rio on S\u00e3o Paulo, those places already have lots of good conventions, let one of those just be kept here in south :)\n\nBut you see, the reason FISL is here is because our previous government from labor party was the first one to really invest and embrace open source and linux, when the state government changed, the labor party took to federal power and kept investing in FISL instead of the state. After that lots of other open source events poped around the country and another states as Paran\u00e1 support open-source, but as a matter of tradition, we have the honror to hold FISL.\n\nBut I really hope that someday not only we have lots of importante free-software events around the country with friends from other countries coming, but more brazilian developers in lots of cities and states get famous enought to be considered stars as those who are coming for FISL."
    author: "Iuri Fiedoruk"
  - subject: "Re: Damn"
    date: 2006-03-27
    body: "Well, Ol\u00edvio Dutra did a good job, and we need more people like him here in Brazil. We developers must foment this kind of mindset and organize ourselves this kind of events, even in regional scale, but spending US$2,000.00 without crossing country's borders just to attend to FISL is a PITA and way too much for Brazilian citizen's financial reality. When I calculated those values my hair went up far higher than Maestro Pletzkaya's (from Tangos E Trag\u00e9dias' fame - N\u00f3s nascemos na Esb\u00f3rnia! HA!). I even nourished the idea of doing that travel as a hitchhiker. What shoul I do? Cry, maybe. Maybe curse Gol, TAM and Varig. Maybe just sftu and distille my envy alone. :)\nI hope these who go to FISL enjoy it a lot, really. These are just ramblings from a money-impaired floss developer. ;)"
    author: "Taupter (Cl\u00e1udio Pinheiro - Kopete)"
  - subject: "KDE VODCasts?"
    date: 2006-03-23
    body: "On that note... are there any KDE Vodcasts yet?  Or KDE Vodcast aggregators/media managers?  Akregator is great for news, but it doesn't really cut it for media.  The best I can find in the Linux world is PenguinTV, but it's still a little rough around the edges and is pretty GNOME-centric."
    author: "Jel"
  - subject: "Firefox-Qt"
    date: 2006-03-23
    body: "No news on that subject...?\n\n\n/duck\nIt would certainly shine with Qt4.1 :-)\n"
    author: "ac"
  - subject: "Re: Firefox-Qt"
    date: 2006-10-24
    body: "No, but we are wait!"
    author: "MetaMorfoziS"
  - subject: "Re: Firefox-Qt"
    date: 2007-10-29
    body: "Maybe with Qt 4.3... :P\nNo news?\nI want firefox and thunderbird compiled with qt...\n"
    author: "Andrea"
  - subject: "Re: Firefox-Qt"
    date: 2007-10-29
    body: "Since Firefox 3 will focus in best-possible environment integration, some people haveb been talking about Qt again:\n\nhttp://blog.mozilla.com/faaborg/2007/10/10/the-firefox-3-visual-refresh-system-integration/\n\nhttp://steelgryphon.com/blog/?p=108\nSome truth here:\nhttp://steelgryphon.com/blog/?p=108#comment-107274"
    author: "Yves"
  - subject: "Re: Firefox-Qt"
    date: 2008-05-15
    body: "There are some news on this subject:\nhttp://blog.vlad1.com/2008/05/06/well-isnt-that-qt/"
    author: "Diego"
  - subject: "Re: Firefox-Qt"
    date: 2008-08-06
    body: "\"Mozilla Qt Port is available for testing\"\n\n\"Great work has been done by the Mozilla and Nokia mobile browser teams. As result we have a working Qt port based on the latest Mozilla trunk 1.9.x.\"\n\n\"The port is fully compatible with the official Qt 4.4 release. It is also ready to run Firefox3.x and the TestQEmbed reference UI:\"\n\nhttp://browser.garage.maemo.org/news/10/"
    author: "Yves"
  - subject: "I'll be there :)"
    date: 2006-03-24
    body: "I live near Porto Alegre (4 hours by bus) and will be there to meet KDE people. I've met Helio and Mattias there in 2004, but wasn't able to go there in 2005 (lack of money).\n\nI hope everbody that comes likes our country and state (Rio Grande do Sul the southest one, near uruguay and argentina).\n\nThe FISL is a lot of fun and there are a lot of talk. I had two surprises in 2004, two of my virtual friends (from the time of scifi moo, a lambda moo we have on our university here) where found to be a Slackware Evangelist (Piter Punk) and the chief of OpenOffice in Brazil (Ghost Rider 2099). It's good to go there and meet people who like the same things you do and are all very educated and friendly.\n\nI hope a lot of people come this year!!"
    author: "Iuri Fiedoruk"
  - subject: "I will not be there :("
    date: 2006-03-24
    body: "Hey people, that's a nice news! I will watch by infomedia TV, better than nothing, but probrably will not be there!\n\n"
    author: "srs"
  - subject: "ogg Theora would be the best option"
    date: 2006-03-26
    body: "Which format would use InfomediaTV for this? \nI hope they uses ogg Theora instead of mpeg that uses now."
    author: "phoen"
  - subject: "Video stream"
    date: 2006-03-27
    body: "Is there somewhere I can download the video?  My spanish is extremely rusty so I can't navigate the site very well.  Thanks in advance."
    author: "sleepkreep"
  - subject: "Re: Video stream"
    date: 2006-03-27
    body: ">My spanish is extremely rusty so I can't navigate the site very well\n\nThis is probally due the fact that us brazilians kind of speak in PORTUGUESE, and our capital ISN'T Buenos Aires ;)\n\nSorry, but there is this thing, that most americans (I'm not saying you are one of them) just belives all latin america are like Mexico (nothing wrong with our mexican friends also). But this is already a kind of joke here.\n\nAnyway, you should wait after the KDE day on FISL, that will be next month, to get the video."
    author: "Iuri Fiedoruk"
---
The <a href="http://fisl.softwarelivre.org/7.0/www/?q=en">International Free Software Forum</a> will be held in Porto Alegre, Brazil from April 19 to 26.  On the first day of the event KDE will be holding KDE Afternoon, a talk show hosted by developer Helio Castro and broadcast on Brazilian TV show InfomediaTV.  The show will gather together some of KDE's best developers: Aaron Seigo, George Staikos and Zack Rusin.  You can follow coverage of the Forum at the <a href="http://www.infomediatv.com.br">InfomediaTV site</a>, including the eye-candy of our developers at KDE Afternoon.



<!--break-->
<h3>KDE Afternoon</h3>

<ul>
<li>Helio Castro:
KDE developer, research and development at Mandriva and KDE's multimedia
Phonon advocate, primary contact for KDE in Latin America.</li>
<li>George Staikos:
KDE developer and one of the authors of the Konqueror browser's infrastructure. Leading the efforts to standardise security for webbrowsers.</li>

<li>Aaron Seigo:
KDE developer sponsored by Trolltech. Author of Plasma, the project to make the KDE 4 desktop.</li>

<li>Zack Rusin:
KDE and X.org developer. Author of the new Exa layer of X.org and many important enhancements in modern graphic technologies in KDE.</li>
</ul>

<p>InfomediaTV is a program specialized in the IT area, which is transmitted to Rio de Janeiro and São Paulo at NGT, an open UHF channel. All productions are also available at the program's website.</p>



