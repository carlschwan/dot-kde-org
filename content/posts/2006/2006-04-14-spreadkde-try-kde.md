---
title: "SpreadKDE: Try KDE"
date:    2006-04-14
authors:
  - "canllaith"
slug:    spreadkde-try-kde
comments:
  - subject: "Promoting KDE through VNCCasts"
    date: 2006-04-14
    body: "I've been working on VNCCasts, a place where you can watch people work and play on their computers.  You can chat with them and other viewers as well.\n\nhttp://vnccasts.com/\nhttp://www.mobuzz.com/shows/2867.html\n\nPerhaps this would be a good way to promote KDE?  \n\nSome ideas:\n\n* it could be used to promote KDE apps\n* it could be used to demonstrate new and/or obscure features of KDE apps\n* it could be used to understand how people use KDE apps on a daily\nbasis (great for usability issues)\n* it could be used to get help while using a KDE app (you can chat\nwith your viewers)\n* it could be used to demonstrate KDE development\n* it could be used to get help while developing a KDE app\n\nIn case you are not willing to broadcast from your computer, please contact me and I could do it on your behalf.\n\n"
    author: "Amir Michail"
  - subject: "Re: Promoting KDE through VNCCasts"
    date: 2006-04-14
    body: "BTW, I'm actually trying to get a startup going ASAP.\n\nVNCCasts is one possibility for a startup and could provide thousands of live interactive broadcasts that are just as varied as what you might watch on TV.\n\nOther things that I'm considering for a startup:\n\ntargetyournews.com: tell people something new without spamming everyone\ntoptaggers.com: rewards top taggers for improved social news/search\n\nLike VNCCasts, Target Your News could be used to promote KDE.  (Incidentally, Target Your News should be much improved next week and will make use of del.icio.us data.)\n\nI'm looking for startup partners, so please let me know if you might be interested in pursuing a startup soon."
    author: "Amir Michail"
  - subject: "Live CD"
    date: 2006-04-14
    body: "May I suggest PCLinuxOS (http://www.pclinuxos.com) as another option for the live CD's?  Its great strength is that it comes fully configured with libcss, win32-codecs, java, nVidia/ATI drivers, etc, which is bound to leave a better impression with switchers than many other live CD's that don't.  It also has KOffice on the live CD instead of Ooo.\n\nThe current live CD version is 0.92 which only has 3.4, but 3.5 is in the repo and will be in the next version 0.93 due out soon, and which should also include KOffice 1.5.\n\nJohn."
    author: "Odysseus"
  - subject: "Re: Live CD"
    date: 2006-04-14
    body: "Thanks, I have missed this one. Downloading...\n\nIt would be perfect with Klik preinstalled.\n"
    author: "reihal"
  - subject: "Re: Live CD"
    date: 2006-04-15
    body: "I agree. I tried several distros before trying PCLinuxOS and found many of them to be lacking in fit and finish. PCLinuxOS has done a nice job with getting the commonly needed applications and drivers to make it completely usable right off the bat. In my case, I appreciate that they included the Nvidia driver, beautiful fonts, java and flash. I have found only a few small bugs that can be lived with and recommend it to anyone, especially users like myself who don't want to be bothered tweaking code. I agree that it would be nice to see them include klik in their next distribution."
    author: "Jim Ketchum"
  - subject: "CD Boot howto"
    date: 2006-04-14
    body: "It could be possible to setup a page showing how to boot from cdrom all (at least, all the major) different bios? this is one of the most F.A.Q. when i tell a friend to try a live CD: \"Yes, but.. how should i boot from cdrom?\"\n(of course i can help too, i know some keystrokes for ASUS, Asrock, and some laptops)"
    author: "RockMan"
  - subject: "Re: CD Boot howto"
    date: 2006-04-14
    body: "As this is not a KDE-specific question but a rather general one, perhaps there is already a page somewhere in the Internet describing this (but I do not know it either).\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: CD Boot howto"
    date: 2006-04-14
    body: "A lot of the Live CD home pages have descriptions on how to do this, e.g. PCLinuxOS has http://www.pclinuxonline.com/wiki/PCLinuxOSLiveCD .  Not quite as graphical as you might like, but it's a bit hard to take screenshots of the BIOS :-)  Perhaps someone handy with digital camera could take a shot or two?\n\nJohn."
    author: "Odysseus"
  - subject: "klik bundles do not include *all* dependencies..."
    date: 2006-04-14
    body: "\"Each Klik package is a single file that contains the complete application and the libraries it needs to run.\"\n--------\nSorry, this is not entirely true.\n\nklik bundles do not include *all* dependencies, but only the direct ones.\n\nTypically, to test klik-ified KDE apps, you'll need to have the kdelibs installed as a *system* package (RPM, .deb,...). In most cases version 3.3.x will be good enough, but sometimes you'd need even newer ones.\n\nPlease correct this on the \"Try KDE\" page. Otherwise, this page rox!"
    author: "s.o."
  - subject: "Re: klik bundles do not include *all* dependencies..."
    date: 2006-04-14
    body: "\"klik bundles do not include *all* dependencies, but only the direct ones.\"\n\nwhich is quite logic, if a KLIK package contained all dependencies, it would be a complete OS :o)"
    author: "AC"
  - subject: "Re: klik bundles do not include *all* dependencies"
    date: 2006-04-19
    body: "Which is why the phrase 'all dependancies' is not found anywhere in that sentence. "
    author: "canllaith"
  - subject: "Another LiveCD"
    date: 2006-04-14
    body: "Hi, I suggest the new version of Kororaa. It has KDE with XGL patches, so it does some pretty cool things. :D\n\nhttp://kororaa.org/\n\nI've used it and it works great and fun to play with. ^^ I know XGL is experimental but it might be a way to show users the future of Linux with the several technologies out there trying to do 3D in X. :)\n\nCheers."
    author: "Jon"
  - subject: "Bughunting"
    date: 2006-04-15
    body: "Are there kind of talkback versions of KDE?\n\nI also would like to know whether the sorting algorithm of the file dialog is fixed in KDE 3.5. In KDE 3.4.2 I get the bug that when I scroll through the files in the file dialogue with the keyboard the selection \"jumps\", this happens esp. when large filenames are in the directory. So the sorting algorithm of the displayed filenames and the scroll routine seems to be different."
    author: "Gerd"
  - subject: "Certaily worth looking at"
    date: 2006-04-18
    body: "This is not a live CD, but certaily worth looking at: http://desktoplinux.com/news/NS7069459557.html"
    author: "Paul"
---
<a href="http://www.spreadkde.org/try_kde">Try KDE</a> is a new resource listing ways that you can try out KDE without commiting to a full GNU/Linux or BSD install. It includes links to live cds, VMware player images and Klik bundles as well as links to KDE desktops available over NX, with explanations of these technologies. It is linked to from the <a href="http://www.kde.org">KDE frontpage</a> and will be updated regularly as more resources are discovered. You the community can help us out, by sending your comments and suggestions to the email address listed at the foot of the Try KDE page.



<!--break-->
