---
title: "Kommander Releases, Plugin and Updates Site"
date:    2006-09-30
authors:
  - "elaffoon"
slug:    kommander-releases-plugin-and-updates-site
comments:
  - subject: "More new stuff"
    date: 2006-09-30
    body: "Hi all. I may not be able to respond much for a few days as I make my way home from aKademy but I have also added several articles including making your first Kommander dialog and a new project management release. Even with not very much noise at all we've seen hundreds of downloads this week. Thanks everyone for your support. Kommander is going to get better and better and the number of apps will start to grow a lot faster as we announce more news. Stay tuned!"
    author: "Eric Laffoon"
  - subject: "Tips"
    date: 2006-09-30
    body: "Preamble:\nI really really hope KDE gets a scripting layer in place. Plasma seems to be it, but KDE 4 is probably 1-2 years away from being as \"stable\" and feature-rich as KDe 3.4, so...\n\nThe point:\nI am really unhappy with the way scripting gets on the desk today.\n- SuperKaramba is in kdeutils-extra and Suse package is not even tagged as one containing SuperKaramba in it. Royal pain to get it when I need it.\n- Kommander is even worse off - web-related package is the last place I would look for it.\nSince neither is installed by default (in Suse at least) finding any of the two is pain, pain, pain.\n\nI really hope Kommander and SuperKaramba teams would unite and package both projects under a kde3-desktopscripting or something similar. This would increase the chances of the projects being installed by default, or , at least, be found much easier. Guys, push for it, otherwise you'll be just banging your head against the brick wall, trying to push this into the mainstream.\n\nOne other thing about Kommander in particular: At least on Suse - doubleclicking a kmdr file opens it up in EDITOR. What gives? Also, I can't package supplimentary files with kmdr and have it \"execute,\" unpack, run.\n\nI really wanted to use Kommander for Karambino Installer (http://kde-apps.org/content/show.php?content=44324) but no matter from which side you look at it: Ease of script+supportingfiles distribution; Ease of running for the user; Availability of the backend framework - none of it is pretty.\n\nI really really want to see an Available, and End-User Friendly scripting framework in KDE 3.x series. Plz, guys, shape up."
    author: "Daniel \"Suslik\" D."
  - subject: "Re: Tips"
    date: 2006-10-01
    body: "Hello,\n\nyou are aware that the packaging is all Suse's work and that they never really been good at it? Use something where programs are packaged under their names and all separately. Debian or alikes do it, hell even Gentoo gets this right.\n\nAs to the scripting, there is python-kde which allows for much. Other than that there is little hope before KDE4. \n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: Tips"
    date: 2006-10-01
    body: "Well, for KDE 3.x, I have no clue what will happen, but I agree on the 'push kommander to kdelibs' for KDE 4.... superkaramba is dead (long live plasma etc) so don't count on it's authors to spend time on it, but they're working on plasma."
    author: "superstoned"
  - subject: "Re: Tips"
    date: 2006-10-02
    body: "\"Other than that there is little hope before KDE4.\"\n\nWell ... it would be cool to have it though, given that \nI believe KDE4 is a long way, but KDE3 is here, works \nperfect, happily allows dcop to do all kind of useful \nstuff (even from within scripts).\n\nIn general I think scripting ways will become very \nvery important even for more \"normal\" users."
    author: "shev"
  - subject: "Re: Tips"
    date: 2006-10-03
    body: "This is lovely. This will go in the unread dustbin as there are several stories above and this post is beyond useless.\n\n> The point:\n\nRant rant... Who cares. Installed by default is relative. It's part of the official KDE core release packages. Talk to your distribution packager. Where it is now is where it started and had we not put it there it would have been very hard to get it in the official packages.\n\n> I really hope Kommander and SuperKaramba teams would unite and package both projects under a kde3-desktopscripting or something similar.\n\nYikes! Karamba is a hack of the desktop that will cease to exist with Plasma in KDE 4. It uses Python pretty much exclusively. That's not to say I don't like it, but it's like suggesting Firefox should unite with Eclipse. Do you even know what Kommander is? SuperKaramba is for putting visuals and data on your desktop, sans widgets. Kommander is for building small to medium applications using widgets and embedded scripting.\n\n> One other thing about Kommander in particular: At least on Suse - doubleclicking a kmdr file opens it up in EDITOR. \n\nThis is no doubt because the packagers feel more secure with this. We have a plan for future releases to be able to allow this behavior with unknown dialogs while insuring known secure installs always work.\n\n> Also, I can't package supplimentary files with kmdr and have it \"execute,\" unpack, run.\n\nI can't help it if you chose to bitch about your ignorance instead of read the press release and download the packages. I had really hoped while I was spending 30 hours in transit from aKademy there would at least be some intelligent comments.\n\n> I really really want to see an Available, and End-User Friendly scripting framework in KDE 3.x series. Plz, guys, shape up.\n\nYou know you could actually go to our web site, read the tips and even subscribe to the mailing list and ask the developers questions directly. We're working on these things. I'm doing it taking time away from my business and sponsoring a developer with my own money to do the work. For this you say shape up?! Grow up! I wish you had sent money so I could give it back. It would be my way of saying go F*** yourself for chosing to bitch and lecture me instead of reading the docs, downloading the packages, and asking for help. \n\nI just got off a 30 hour airline experience from hell after a week of $10 a meal for mediocre food and a shower I could spit harder than with a goal of supporting my users and you're too lazy to do anything but tell me I messed up the parts I didn't even do. Thank you. Go away please or stop being so lame! "
    author: "Eric Laffoon"
  - subject: "Kommander and Workflow (and Kross)"
    date: 2006-10-06
    body: "They seem to overlap.  Can Kommander build a UI for a Workflow automation that has some scripting based on Kross?"
    author: "S Page"
---
The <a href="http://kommander.kdewebdev.org/">Kommander</a> team is proud to announce a new development <a href="http://kommander.kdewebdev.org/releases.php#unstable">release</a> which has some bug fixes but most importantly a new text editor. Along with this we are releasing two new <a href="http://kommander.kdewebdev.org/releases.php#plugins">plugins</a> for <a href="http://kommander.kdewebdev.org/changes.php?releasenum=3">databases</a> and HTTP forms. We have also updated our site with an <a href="http://kommander.kdewebdev.org/writings.php">article and tutorial section</a> starting out with an <a href="http://kommander.kdewebdev.org/writings.php?article=intro">Introduction to Kommander</a>. We also have a <a href="http://developer.kdewebdev.org/kommander/">development news section</a>. More is in the works to be released in the coming week. Remember Kommander is developed almost entirely through sponsorship funding and actually saw little development for several months partly due to funding shortages. Your <a href="http://kdewebdev.org/donate.php">financial support</a> is welcome. 




<!--break-->
<p>The new release is marked as unstable, but it is mostly bug fixes except for the new text editor. Kommander has the same editor part as Kate and Quanta now with new highlighting, bracket balancing and undo/redo. It can also do line numbers and bookmarks, though the current editor model causes persistence issues, it is still a huge improvement. As this release is backwards compatible with existing versions we strongly recommend it to everyone writing Kommander dialogs.</p>

<p>Over a year ago we decided to develop a plugin to show this potential in Kommander. We decided on hk_classes for a database plugin. At aKademy last year there was a bug preventing release. Then for both funding and personal reasons Michal was unable to work on it. Now a year later it is ready and we also have a new HTTP form class that allows you to send HTTP headers. It has been successfully used to make a text messaging tool to prevent us from having to find where the provider put the web form this week.</p>

<p>There have been a lot of Kommander apps showing up on <a href="http://www.kde-apps.org/">kde-apps.org</a> and likely a lot of people have questions. We know a lot of people producing dialogs need a little help with some tutorials and tips to make them more usable. So we have the new <a href="http://kommander.kdewebdev.org/writings.php">section for articles, tutorials and tips</a>. Look for more on that soon.</p>

<p>We have mentioned that Kommander development was inactive for several months, but we have discussed starting again with Michal because it is important for KDE to have this tool. If you are enjoying this tool or a program written with it please consider giving your <a href="http://kdewebdev.org/donate.php">financial support</a> to the project. Thanks to all who have encouraged us and made applications with Kommander.</p>



