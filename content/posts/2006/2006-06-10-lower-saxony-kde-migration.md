---
title: "Lower Saxony KDE Migration"
date:    2006-06-10
authors:
  - "wolson"
slug:    lower-saxony-kde-migration
comments:
  - subject: "Lower Saxony KDE Migration"
    date: 2006-06-10
    body: "One cannot help those who refuse help.\n\nYeah, i guess you have to read between the lines. ;-)\n\n--Stefan\n"
    author: "Stefan Teleman"
  - subject: "Re: Lower Saxony KDE Migration"
    date: 2006-06-12
    body: "You can run KDE on Solaris. Why did they have to replace the operating system? Are there any other applications in the game which are running on Linux only?"
    author: "Andreas"
  - subject: "Re: Lower Saxony KDE Migration"
    date: 2006-06-12
    body: "I think you need to understand that the desktop choice was not the only one to be made. There was a conscious decision pro Linux and contra Solaris, so it wasn't just that they wanted KDE instead of CDE.\n\nI actually think the desktop choice was only of secondary importance. They wanted Linux to replace Solaris, and then started to looking out for a desktop to replace CDE. I'm sure they would not have gone through the pain of a migration (12.000 machines!) because they wanted a fancier look ;)\n"
    author: "Matthias Welwarsky"
  - subject: "Re: Lower Saxony KDE Migration"
    date: 2006-06-12
    body: "> I actually think the desktop choice was only of secondary\n> importance\n\nAFAIK, the two requirements for desktop migration were:\n1. An Open Source Operating System\n2. Support for KDE\n\nOpenSolaris qualified for requirement #1, for all intents and\npurposes. It did not qualify for requirement #2,since Sun does\nnot (want to) support KDE.\n\nIt is quite baffling to see that Sun is willing to forego loyal\nand important customers, and lose significant marketshare,\njust because it is unwilling to consider KDE as a supported\ndesktop. My own personal preferences aside, this makes no \nbusiness sense at all.\n\nHopefully this will encourage Sun to reconsider its position\nwith respect to KDE.\n\n"
    author: "Stefan Teleman"
  - subject: "Re: Lower Saxony KDE Migration"
    date: 2006-06-12
    body: "With McNealy gone, it is possible that Sun will go back to doing tech, rather than ego building. However, I personally, prefer to see Linux make it. Sun had their time to compete against MS and lost. In fact, even MS does not consider solaris (even any of the unixes or macOS) a real threat. I agree with them."
    author: "a.c."
  - subject: "Just seen this awsome news on Digg"
    date: 2006-06-10
    body: "Great news. I've just seen this on digg.com. I sure hope other governments will follow this example and we see more Linux and KDE powered desktops."
    author: "Mark"
  - subject: "The days of Windows..."
    date: 2006-06-10
    body: "...will soon be over."
    author: "ale"
  - subject: "Re: The days of Windows..."
    date: 2006-06-10
    body: "> ...will soon be over.\nBecause they moved from Solaris 8?\n\nlg\nErik"
    author: "Erik"
  - subject: "Re: The days of Windows..."
    date: 2006-06-10
    body: "> Because they moved from Solaris 8?\n\nBecause administrations everywhere are slowly starting to realize that office-IT is possible without Windows or MS Office."
    author: "ale"
  - subject: "Re: The days of Windows..."
    date: 2006-06-10
    body: "The migrated computers didn't run Windows or MS Office."
    author: "Atys"
  - subject: "Re: The days of Windows..."
    date: 2006-06-10
    body: "but what would they've migrated to instead? Solaris 10?"
    author: "heimo-p"
  - subject: "Re: The days of Windows..."
    date: 2006-06-10
    body: "They actually did it without Windows. So who learns what?\n\nlg\nErik\n"
    author: "Erik"
  - subject: "Re: The days of Windows..."
    date: 2006-06-10
    body: "No, but they could have migrated _to_ Windows.\n\nObviously, KDE was a good alternative."
    author: "ale"
  - subject: "Re: The days of Windows..."
    date: 2006-06-11
    body: "Wow, you're really insistent. Probably, if you hear about some people switching from drinking coffee to drinking tea, you will immediately proclaim the end of Coca-Cola.\n\nOne just wonders what would have had to happen for you to find that the days of Solaris are over."
    author: "Atys"
  - subject: "Re: The days of Windows..."
    date: 2006-06-11
    body: "> Wow, you're really insistent\nWell, it's a forum for discussion, istn't it? Thank you for the feedback on my post here. :-)\n\n> the end of Coca-Cola.\nWhen it comes to food and drink, it is all about personal taste. What office IT solution you get at your job-computer is mostly not your choice.\n\n> the days of Solaris are over\nYou can use KDE on Solaris too, so I don't really see the relationship there."
    author: "ale"
  - subject: "Re: The days of Windows..."
    date: 2006-06-12
    body: "> > the days of Solaris are over\n>You can use KDE on Solaris too, so I don't really see the relationship there.\n\nIt's so obvious that it's weird that you question it: The migration decreases the number of installed Solaris system by 12000. It doesn' affect the number of installed Windows systems at all. That's what is usually called a \"fact\"."
    author: "Anton"
  - subject: "Re: The days of Windows..."
    date: 2006-06-12
    body: "What's your point?\n\nI think the days of Windows/Ms Office are over, simply because people start migrating to a KDE dekstop, not a Windows desktop. But sometimes from Windows.\n\nI don't see Solaris as a desktop OS. But maybe you're right, maybe the days of Solaris are over too. But then it wouldn't have much to do with the desktop.\n\nWe're talking about desktop environments here, remember..."
    author: "ale"
  - subject: "tax authority"
    date: 2006-06-10
    body: "I dont get it, why the tax authority? They are usually the last ones to care about money."
    author: "AJ"
  - subject: "Re: tax authority"
    date: 2006-06-10
    body: "And so? Money is not the only reason to pick Free Software over proprietary.\n"
    author: "AC"
  - subject: "Link to the authority (german)"
    date: 2006-06-10
    body: "Here a link to the authority's newsite: http://www.ofd.niedersachsen.de/master/C21590269_N9551_L20_D0_I636.html"
    author: "Hans"
  - subject: "12,000 KDE desktops can't print nicely!"
    date: 2006-06-10
    body: "Did Lower Saxony  people knew that \"KDE can't print nicely!\" before migration? Which will be supposed to be fixed by QT4 and KDE 4, expected by a year at soonest. Till then they should use Gnome/Openoffice for printing. TTF printouts are crappy with KDE Print System. They should wait the migration till KDE 4 comes out, or perhaps they don't use printing at all that's why they migrated to KDE 3.x and QT3.x.\n\nsee:\nhttp://bugs.kde.org/show_bug.cgi?id=128912"
    author: "fast_rizwaan"
  - subject: "Re: 12,000 KDE desktops can't print nicely!"
    date: 2006-06-10
    body: "I guess this problem is really bothering you, otherwise you would have assumed (as many of us are assuming) that they would have checked this in their _two years_ of planning and testing."
    author: "taj"
  - subject: "Re: 12,000 KDE desktops can't print nicely!"
    date: 2006-06-10
    body: "man,... shouting about your unfixed bug reports won't do you any good. At least I would care even less the more you shout.\n\nNow... not sure what all that fuzz is about (my printing goes fine, even if pdf output isn't as good as I'd like). BUT. Using KDE as desktop, my friend, doesn't mean using koffice as office tool, and using openoffice doesn't mean you are not using KDE. KDE == desktop, openoffice==app that runs just fine under KDE.\n\n"
    author: "silly you"
  - subject: "Re: 12,000 KDE desktops can't print nicely!"
    date: 2006-06-11
    body: "> Now... not sure what all that fuzz is about (my printing goes fine, even if pdf output isn't as good as I'd like). BUT. Using KDE as desktop, my friend, doesn't mean using koffice as office tool, and using openoffice doesn't mean you are not using KDE. KDE == desktop, openoffice==app that runs just fine under KDE.\n\nOK Koffice is not KDE but what about kwrite, kate, kchmviewer...?\n\nHere's proof, see the PDF file it is printed with TTF installed, you can see the PDF text is ugly!\n\nIn the Attachment you can find Times.ttf, Times.pfb, UGLY_Printout.pdf and NICE_Printout.pdf, and you yourself can try this by reading \"UGLY_print.txt\" and \"Nice_Printout.txt\"\n\nLet's see how does your KDE application behaves!"
    author: "fast_rizwaan"
  - subject: "Re: 12,000 KDE desktops can't print nicely!"
    date: 2006-06-11
    body: "why would they want to use ttf-fonts in a text editor like kwrite?\n\nand why would running openoffice under kde affect the printing quality of openoffice documents?\n"
    author: "rinse"
  - subject: "Re: 12,000 KDE desktops can't print nicely!"
    date: 2006-06-11
    body: "the printout with kwrite using ttf-fonts is of minor quality, but still perfectly readable.\n\nI don't need my text-editor to produce first-class pdf-files using ttf-fonts, as long as the result is somewhat fine (but far from perfect).\n\nStill I _do_ sometimes want first-class printouts using ttf-fonts. Scribus gives me very nice results using times.ttf\n"
    author: "Thomas"
  - subject: "Re: 12,000 KDE desktops can't print nicely!"
    date: 2006-06-11
    body: "There are some problems with the kerning of ttf fonts in Qt on printed documents.\nIt can be easily avoided, like rizwaan shows in his tar archive. \n\nI think in koffice this is might be a problem, but with other applications, like kwrite, you don't need special ttf-fonts for printing, the default fonts of Linux will do just fine.\nThe tax authority does not use koffice, so they are not bothered with this issue.\n\n"
    author: "Rinse"
  - subject: "Re: 12,000 KDE desktops can't print nicely!"
    date: 2006-06-11
    body: "> Till then they should use Gnome/Openoffice for printing.\n\nWell I found printing in GNOME to be more broken than in KDE and that's just one part of what doesn't work in GNOME.\n\n- GIF Pictures print black in GNOME\n- Printing Support in GNOME is unmaintained\n- Evolution can't print WYSIWYG\n- Gedit2 prints black squares for hard tabs\n- You can't print two or more pages on one page using Evince\n\nOpenOffice has it's own printing dialog and system and doesn't rely on GNOME at all. Same applies for Firefox for example."
    author: "AC"
  - subject: "Re: 12,000 KDE desktops can't print nicely!"
    date: 2006-06-12
    body: "GTK+ 2.10 will have a new printing framework which should improve things considerabely in the GNOME world. So it is a bit wrong to say that printing support is unmaintained."
    author: "Andreas Tunek"
  - subject: "Re: 12,000 KDE desktops can't print nicely!"
    date: 2006-06-12
    body: "> So it is a bit wrong to say that printing support is unmaintained.\n\nIt's not wrong to say this. You can not expect people to wait until the new printing framework to show up and well supported under GNOME. GNOME the so called 'enterprise ready' Desktop Environment has to work now and people want the stuff to simply work so they can get their work done. The current printing framework is left in a broken state and thus is not really useful for todays needs and people using GNOME."
    author: "ac"
  - subject: "Re: 12,000 KDE desktops can't print nicely!"
    date: 2006-06-12
    body: "So what should the GNOME/GTK developers do? They identified the printing problems and decided that the best solution would be to put in a lot of the printing functionality in GTK instead of the way it is handled now. Of course it would be great if some people would continue work on the current methods, but it seems the GNOME/GTK developers are more interested in implementing a really good printing support for their next release in 4 months.\n\nOf course, GNOME users and the world in general would be happier if printing worked great today, but the world unfortunately is not perfect."
    author: "Andreas Tunek "
  - subject: "Re: 12,000 KDE desktops can't print nicely!"
    date: 2006-06-12
    body: "> but it seems the GNOME/GTK developers are more interested in implementing a\n> really good printing support for their next release in 4 months.\n\nThey said the same with the current \"broken\" implementation and yet they failed with that. Now we see another really good printing idea and it will probably fail again as usually. Good ideas, badly implemented, declared a failure afterwards and then someone comes up with yet another brilliant idea, which ends the same way. Half implemented, broken, declared a failure afterwards and so on."
    author: "ac"
  - subject: "Re: 12,000 KDE desktops can't print nicely!"
    date: 2006-06-13
    body: "They should use KDE and stop this nonsense.\n\nOr stop trolling KDE forums and shilling for gnome - instead go and fix that hairball."
    author: "vm"
  - subject: "Re: 12,000 KDE desktops can't print nicely!"
    date: 2006-06-14
    body: "Just for future information so I do not troll again, what part of my message was trolling?"
    author: "Andreas Tunek"
  - subject: "Re: 12,000 KDE desktops can't print nicely!"
    date: 2006-06-14
    body: "I think, what the previous writer wanted to tell you is. You should stop writing about that other crap desktop on KDE lists... And basicly. I do agree with him."
    author: "ac"
  - subject: "Re: 12,000 KDE desktops can't print nicely!"
    date: 2006-06-11
    body: "Well, For my work i totally migrated to KDE (3.3  and then 3.5) from Windows and ... i had NO problems with my printer and my documents and images prints out well with KOffice and OpenOffice2. And.. now I can use the software i need on a computer tat can't run Windows XP on it (low RAM and slow CPU)!"
    author: "Fabio"
  - subject: "Re: 12,000 KDE desktops can't print nicely!"
    date: 2006-06-13
    body: "Use Type 1 fonts, they certainly don't have any printing problems...\n\nDisclaimer: small font sizes with Type 1 fonts can look pretty bad on your monitor unless you have very high DPI or are using anti-aliasing (personally, I find anti-aliasing to be blurry, so I just turn up the zoom in KWord)\n\nTrivia: Windows substitutes Type 1 fonts (if they are available) for the core TrueType set when printing.  Maybe this says something.  : )"
    author: "Tyson"
  - subject: "Germans using KDE and SuSE"
    date: 2006-06-12
    body: "Who would've thunk?"
    author: "Rick"
  - subject: "Re: Germans using KDE and SuSE"
    date: 2006-06-12
    body: "Well, both have been started in Germany, so what else would they use :o)"
    author: "AC"
  - subject: "Re: Germans using KDE and SuSE"
    date: 2006-06-12
    body: "RedHat?"
    author: "Rick"
  - subject: "Re: Germans using KDE and SuSE"
    date: 2006-06-13
    body: "I live in Raleigh, NC and I don't use that POS RH.\n\nI'd sooner run solaris or FreeBSD.\n\nOr slackware. At least slackware is free, more current, faster."
    author: "vm"
  - subject: "Re: Germans using KDE and SuSE"
    date: 2006-06-13
    body: "Yeah, I'd rather use BSD too.  It's not savaged by that non-free GPL."
    author: "Rick"
  - subject: "Re: Germans using KDE and SuSE"
    date: 2006-06-13
    body: "well, lets flame, then. \n\nThe situation with the BSD-vs-GPL is comparable with freedom at large: we can't be ALL free. If I was totally free to do whatever I wanted, I could impair YOUR freedom. So the law restricts MY freedom to protect YOURS.\n\nThat's why the GPL has to restrict the freedom of the user of the code, to protect the freedom of OTHER people. BSD code can be non-free, it can be used to take away the user's freedom. Impossible with GPL. So who's more free?"
    author: "superstoned"
  - subject: "Re: Germans using KDE and SuSE"
    date: 2006-06-13
    body: "Wrong place for this; If you insist on trolling, please jump to /. or a dvorack column. Thank you.\n"
    author: "a.c."
  - subject: "Lower Saxony"
    date: 2006-06-18
    body: "How good that KDE has Low Saxon support"
    author: "gerd"
---
ZDNet reports on a recent <a href="http://news.zdnet.co.uk/0,39020330,39274196,00.htm">German Linux migration</a> by the tax authority in Lower Saxony which has made the decision to migrate an impressive 12,000 desktops to SuSE Linux using KDE.  The project, which is now in process converting 300 desktops daily, moves
systems from Solaris x86 version 8, which the organisation has been running since 2002.  The migrations are reported as going well thus far.
KDE's <a href="http://extragear.kde.org/apps/kiosktool/">Kiosk</a> desktop customisation, source code access and licensing costs were cited as key reasons for the decision.  Congratulations to all involved, and best of luck going forward with this effort!


<!--break-->
