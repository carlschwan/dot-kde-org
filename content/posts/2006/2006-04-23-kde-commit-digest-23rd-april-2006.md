---
title: "KDE Commit-Digest for 23rd April 2006"
date:    2006-04-23
authors:
  - "dallen"
slug:    kde-commit-digest-23rd-april-2006
comments:
  - subject: "thanks"
    date: 2006-04-23
    body: "\"this week's KDE Commit-Digest\" is always something to look foreward to. thanks again.\n"
    author: "toonmuylkens"
  - subject: "Hard to read"
    date: 2006-04-23
    body: "I'm glad that the digest is back, but I find it unnecessary hard to read. Why the fonts are so big? Why entire paragraphs are in italic? The old layout was all but great, but at least it was easy on the eyes..."
    author: "anonymous"
  - subject: "Re: Hard to read"
    date: 2006-04-23
    body: "Fortunately it is just a stylesheet and can be disabled:\nKonqueror: View / Use Stylesheet / Basic Page Style\nFirefox: View / Page Style / No Style\nOpera: View / Style / User mode"
    author: "testerus"
  - subject: "Re: Hard to read"
    date: 2006-04-23
    body: "Try the Options menu item. You can change the appearance of the page. I agree with you that entire paragraphs in italics are not easy on the eyes, but you can change that on the options page."
    author: "Andre Somers"
  - subject: "Re: Hard to read"
    date: 2006-04-24
    body: "Well, I am glad that, for once, the fonts on a web page are not 3 pixels high :)\nWith nowadays laptop especially, it is very painful to read many pages were the webmasters think that font sizes of 8 is good."
    author: "oliv"
  - subject: "Number of new files"
    date: 2006-04-23
    body: "What's up with the number of new files statistic?  This week, 19,000, last week, 21,000.  I find it hard to believe that there are 40,000 new files in SVN over the course of 2 weeks.  \nWhat does this number really mean?"
    author: "Leo S"
  - subject: "Re: Number of new files"
    date: 2006-04-24
    body: "I think these huge number of new files may be coming from projects (like solid, phonon, plasma, etc) being added to KDE's SVN.  Though with numbers that high it seems somewhat odd."
    author: "Corbin"
  - subject: "Re: Number of new files"
    date: 2006-04-24
    body: "Perhaps copy/move/rename is counted too, as for SVN it means adding a file.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Portabel"
    date: 2006-04-23
    body: "It is quite intresting to see so much focus on portability to other plattforms. "
    author: "Hen Tai"
  - subject: "Re: Portable"
    date: 2006-04-23
    body: "Windows, in particular.  KDE on Windows will change the landscape of the free software world."
    author: "Spy Hunter"
  - subject: "Re: Portable"
    date: 2006-04-24
    body: "\nOr it might kill KDE itself by killing Linux and ending the symbiosis upon which both KDE and Linux have so far relied.\n\nNot saying that it will. Just another theory. I really have no interest in KDE on windows beyond specific applications and only with the intent of using those applications to smooth the transition AWAY from windows."
    author: "Gonzalo"
  - subject: "Re: Portable"
    date: 2006-04-24
    body: "I would be interested in KDE Kontact on Windows. When I could have an mailclient on both windows and linux, would make me a lot more happy... Firefox ist okay, but Mozilla Mail or Thunderbird is in my opinion not a really good alternativ to outlook.\n\nKPatience / Klondite, so at last I can play it also when I'm running under windows.\n\nAnd ofcourse last but not least, KFireworks.... I would be interested to see if it runs smoother under Linux than under Windows... I somehow have the feeling that you can't watch anything under Windows without sometimes have a small delay in it, I don't noticed such things under Linux (playing DVD or watching a screensaver)..\n\n"
    author: "boemer"
  - subject: "Re: Portable"
    date: 2006-04-24
    body: "I used to think like this, but the success of Firefox and OpenOffice made me reconsider this in a different light. In the, end if a person is running 90% free software (windows kernel + kde + open office + firefox + samba + ... ), then they are 90% free (and she'll have no problems on completing the switch anytime). Much better than anything. And believe me, if this was to happen massively, it would really hurt microsoft (a company I profoundly dislike, but I disgress ...) to the point of virtually killing it.\n\nCheers :-)"
    author: "KubuntuUserExMandrake"
  - subject: "Re: Portable"
    date: 2006-04-25
    body: "Then again, Firefox and OpenOffice are also good examples of the potential downsides. In both cases, there's considerable overhead for cross-platform support and integration issues with all supported platforms. It took a long while for Firefox to become a proper Gnome application (which they consider themselves to be, according to Asa dotzler), a proper Mac application (still not fully there yet), etc. - and I think we all know the convoluted history of OpenOffice-in-KDE.\n\nSimilarly, being a good Windows application is more than just running and rendering. It means adhering to certain platform conventions. Cross-platform KDE applications will be torn between the platforms they support, just as Firefox and OpenOffice are.\n\nBottom line, my concern is that we are going to see more applications for KDE-the-libraries than for KDE-the-desktop-platform, weakening the latter as a result. Can't be good for free software either."
    author: "Eike Hein"
  - subject: "Re: Portable"
    date: 2006-04-25
    body: "Firefox and openoffice are not good examples, since they are legacy applications.\nBoth application existed on linux years before kde and gnome became a reality.\n\nIn the linux years before kde/gnome every application used their own toolkit/api/framework/etc on Linux. And that is what firefox and openoffice still do.\n"
    author: "AC"
  - subject: "Re: Portable"
    date: 2006-04-25
    body: "That's not terribly relevant, I fear. The problems are UI conventions, both appearance and more importantly behavior, and interoperability with other common technologies on the supported platforms. Whether you're a Mozilla XUL or a kdelibs app, you're still dancing on several weddings doing cross-platform. Which leads back to my above post."
    author: "Eike Hein"
  - subject: "Re: Portable"
    date: 2006-04-25
    body: "True, but when openoffice was developed, there was nothing on linux to interoperate with.\nThat's quite a different situation compared to kde on windows. \nWhile openoffice/firefox now need to deal with UI conventions and interoperability that was not there when they started, KDE can deal with them from the very beginning.\nAnother advantage is that KDE relies on Qt, which already interoperates good with windows etc..\n"
    author: "ac"
  - subject: "os is irrelevant"
    date: 2006-04-24
    body: "I believe that as web applications and open source multiplatform software become the norm, the operating system becomes irrelevant. Which would be in Linux's (or some libre OS) advantage, as well as the multiplatform software. One week you start Windows, you edit a document in KOffice, play some music in amaroK and check your mail in GMail. The next week your employer switches to Linux and... you do exactly the same thing.\n\nMircosoft is well aware of this. :)"
    author: "Ian Monroe"
  - subject: "Re: Portable"
    date: 2006-04-26
    body: "I don't think it will, for the simple reason that if people are using KDE, they'll go for the cheapest OS on which to do so."
    author: "mikeyd"
  - subject: "Re: Portable"
    date: 2006-04-25
    body: "\"Windows, in particular. KDE on Windows will change the landscape of the free software world.\"\n\nNo it won't"
    author: "Segedunum"
  - subject: "Re: Portabel"
    date: 2006-04-24
    body: "At what cost?\n\n* Slower development due to build problems and a wider range of compilers being supported?\n\n* Less tight integration with the OS?\n\n* Less tight integration between different KDE applications (since they are supposed to be able to work standalone on windows, not to be tightly nit together)?\n\n* Less features due to that appliations cannot rely on i.e. window manager integration and x server features?"
    author: "Fredrik"
  - subject: "Re: Portabel"
    date: 2006-04-25
    body: " * Slower development due to build problems and a wider range of compilers being supported?\n\npossible..\n \n * Less tight integration with the OS?\n\nwhat integration? \nKDE already runs on several platforms, zo thight integration with e.g. Linux is not an option.\n \n * Less tight integration between different KDE applications (since they are supposed to be able to work standalone on windows, not to be tightly nit together)?\n\nif they work standalone on Windows, they are not KDE applications, KDE applications depend on KDE, no matter on what OS they are running.\nand it's the dependency on KDE that nits the applications thightly together\n \n * Less features due to that appliations cannot rely on i.e. window manager integration and x server features?\n\nNo, i don't see why applications cannot use x server features etc. It would only mean that those features are not available on Windows...\n\nFor example, Abiword uses the gnome-printmanager on Linux. It does not on Windows ;)\n"
    author: "AC"
  - subject: "Re: Portabel"
    date: 2006-04-25
    body: "> if they work standalone on Windows, they are not KDE applications, KDE applications depend on KDE, no matter on what OS they are running.\nand it's the dependency on KDE that nits the applications thightly together\n\nRight now, being \"a KDE applications\" means \"uses kdelibs\". As it happens, using KDE-the-libraries currently also largely means targeting KDE-the-desktop as well. However, these \"KDE applications\" often don't actually have any dependencies beyond kdelibs.\n\nNow, work is being done turning kdelibs into a cross-platform solution, allowing these apps to run on other platforms. Reiterating what I wrote earlier, deploying competently in an alien desktop goes beyond merely executing and painting: to succeed on Windows or OS X, an application has to adapt to their conventions to a degree and interoperate well with their facilities.\n\nThus, application teams that chose to use kdelibs to support platforms other than KDE may face a development overhead that lowers the amount of time they can spend on KDE integration. Having most of the popular applications available for several desktops may also prove detrimental to the continued development of KDE platform conventions and behaviors in other ways.\n\nI'm not saying we shouldn't do it (too late for that, anyhow) or that there aren't any potential benefits. But we should be going in aware of the risks."
    author: "Eike Hein"
  - subject: "Re: Portabel"
    date: 2006-04-25
    body: "The fact that kdelibs is available for windows does not mean that *every* kde application needs to be able to run on windows as wel.\n\nIt's just a new optional possibility for software developers, not something that is mandatory in order to create kde applications.\n\nThe only overhead i see is for kdelibs itself, kdelibs developers now need to develop for windows as well.\n"
    author: "ac"
  - subject: "Re: Portabel"
    date: 2006-04-25
    body: "> It's just a new optional possibility for software developers, not something that is mandatory in order to create kde applications.\n\nHence I wrote \"for teams that chose to\"."
    author: "Eike Hein"
  - subject: "Re: Portabel"
    date: 2006-04-25
    body: "Ah, but isn't that their problem?\n\nTeams that choose to support many platforms now use Qt only, in the future they can use kdelibs as wel.\n\nSo i guess porting kdelibs to windows may reduce the overhead voor applications that want to support several platforms (since kdelibs takes away a lot of work), and insures integration with the linux desktop better then Qt only can.\n\nSo, why is porting to kdelibs a bad idea?\nIs it because more application developers may be tempted to make their apps cross platform, and we should prevent that by not making kdelibs available for windows?"
    author: "AC"
  - subject: "Re: Portabel"
    date: 2006-04-26
    body: "> Ah, but isn't that their problem?\n\nThe long-term viability of KDE as a desktop platform is a concern for many, I wager. It's fate is largely determined by the health of its application base (which implements and evolves its conventions). I tried to outline the connection above.\n\n\n> So i guess porting kdelibs to windows may reduce the overhead voor applications that want to support several platforms (since kdelibs takes away a lot of work), and insures integration with the linux desktop better then Qt only can.\n\nYou've certainly got a point here: The risk of prominent KDE applications finding themselves unable to spend as many resources as they used to on targeting the KDE desktop may be more than compensated for by new projects chosing kdelibs as their framework - which they might not have done without cross-platform support - and ending up providing competent KDE support. That's what we'll have to focus on making as attractive and easy as possible.\n\n\n> Is it because more application developers may be tempted to make their apps cross platform, and we should prevent that by not making kdelibs available for windows?\n\nCome on, now you're purposefully ignoring what I wrote earlier: \"I'm not saying we shouldn't do it (too late for that, anyhow) or that there aren't any potential benefits. But we should be going in aware of the risks.\" was unclear?"
    author: "Eike Hein"
  - subject: "Yay"
    date: 2006-04-23
    body: "Yay. Now with diffs. :-)"
    author: "AC"
  - subject: "View mode in Konqueror messed up!"
    date: 2006-04-24
    body: "I'm wondering, if anybody tries to fix the kioslave bug in Konqueror. (http://bugs.kde.org/show_bug.cgi?id=108542) Konqueror always switches back to icon view when using media:/ or smb:/ kioslaves. For a file manager this is a major bug, but broken since at least last summer. How it comes, that nobody has fixed that?"
    author: "Rob "
  - subject: "Re: View mode in Konqueror messed up!"
    date: 2006-04-24
    body: "I suppose that the answer has to be found \"somewhere\" in the simple fact that KDE would need more volunteer developers.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: View mode in Konqueror messed up!"
    date: 2006-04-25
    body: "I tried this, but it's nog bugging me?\n\nif i have a treeview and go to media:/cdrom, i get back to icon view.\nThen i select treeview again in the toolbar, and i'm back to browsing in treeview...\n\n"
    author: "AC"
  - subject: "Re: View mode in Konqueror messed up!"
    date: 2006-04-25
    body: "tried it again, now by clicking on mycomputer.desktop or trash.desktop\n\nNothing weird happens, the new windows with trash:/ or media:/ are using the treeview...\n"
    author: "AC"
  - subject: "Re: View mode in Konqueror messed up!"
    date: 2006-04-25
    body: " \"if i have a treeview and go to media:/cdrom, i get back to icon view.\n Then i select treeview again in the toolbar, and i'm back to browsing in treeview...\"\n\nYou just described the bug. You have tree view selected and using media:/cdrom it switches back to icon view but should stay in tree view."
    author: "Rob "
  - subject: "Re: View mode in Konqueror messed up!"
    date: 2006-04-25
    body: "ah, i guess konqueror defaults to the settings of [filebrowser profile] when typing media:/\nif i make 'treeview' default in the [file browser profile], konqueror opens media in treeview mode..\n\ni also noticed something about 'having to restart konqueror in order to get treeview back'\n\ndidn't notice that one either..\n"
    author: "AC"
---
In <a href="http://commit-digest.org/issues/2006-04-23/">this week's KDE Commit-Digest</a>: KDE 4 porting continues at great pace, with more applications able to be compiled with <a href="http://www.cmake.org/">CMake</a> daily. Portability fixes for non-X11 platforms. KDiskManager, a KDE 4 application for disk management -- based on <a href="http://solid.kde.org">Solid</a> -- is imported into KDE SVN.


<!--break-->
