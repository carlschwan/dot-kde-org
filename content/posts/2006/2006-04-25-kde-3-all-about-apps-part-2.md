---
title: "KDE 3: All About the Apps (Part 2)"
date:    2006-04-25
authors:
  - "cniehaus"
slug:    kde-3-all-about-apps-part-2
comments:
  - subject: "Kudos to Twinkle"
    date: 2006-04-25
    body: "It's a really great app and it should deserve more visibility! It probably needs some enhancements on the UI side but it's by far the best KDE-aware SIP app!\nAn integration with Kopete would create a killer app for KDE."
    author: "Davide Ferrari"
  - subject: "Re: Kudos to Twinkle"
    date: 2006-04-25
    body: "I fully agree.  Integration with e.g. kopete could be taken 1 level further\nby creating a presence deamon http://wiki.kde.org/tiki-index.php?page=presence+deamon\n\n"
    author: "Anon"
  - subject: "yaBi"
    date: 2006-04-25
    body: "> yaBi is Yet Another Beagle search Interface. It is written in Python and looks just great.\n\nHonestly, it looks craptastic, with a typo in the title bar text, scary file URLs with three consecutive slashes, somewhat unorthodox use of the button widget and that tacky drop shadow effect for the text. This is supposed to be a reason to make me love KDE?\n\nFrom a PR POV, minus points for the horrible compression ratio of that screenshot, and the color scheme (a consistent color scheme for all screenshots in the article would be the minimum; using the KDE default desirable).\n\nSorry for this harsh rant, but it's a honest one. Obviously both the author of yaBi and this article put a fair amount of time into both works and shared it freely to the benefit of others, and should be commended for that. But in a promotional feature, one should aim for a certain quality standard when it comes to appearance, and be selective enough not to highlight products that are not ready for prime time."
    author: "Agitated Coward"
  - subject: "Re: yaBi"
    date: 2006-04-25
    body: "Perhaps I'm too tired, but what's the typo in the title bar? I can't find it. Is it the space before the '?'?\n\n(By the way, if this program adds value to the KDE desktop, a few redundant '/''s sounds like something that's not worth to comment on. It'll get fixed in the next minor release).\n\n\n"
    author: "Apollo Creed"
  - subject: "Re: yaBi"
    date: 2006-04-25
    body: "Um, what's wrong with three slashes (except for the aesthetics)? file:// for the protocol and /whatever for the path."
    author: "Sten Sture"
  - subject: "Re: yaBi"
    date: 2006-04-28
    body: "not that I find it ugly, but removing something useless is always good.\nI just try in konqueror and file: is the protocol and actually gets removed automatically. so path should simple be  /home/user/file\nnothing else IMHO.\n"
    author: "somekool"
  - subject: "Re: yaBi"
    date: 2006-04-26
    body: "Though as a GUI desinger I agree with many points, \"craptastic\" is really a bit too much for me and wont help anybody here. I hope your IMHO very harsh post doesnt overshadow some very valid points about GUI design (classic \"geek\" mistakes which sometimes can (I'm not saying it does here!) completely spoil an otherwise great open-source app):\n\n1) It's generally not a good idea to use well-known widgets in \"unothodox\" (as the parent put it) ways. A button is a button. A drop-down list a drop-down list. It is *really* useful in terms of GUI design to think twice if you cant offer the same functionality with standard widgets.\n2) Drop-shadows are bad. They belong to the childhood days of computer design where it was great achievement to put one text slightly shifted behind another. Now, we all can hopefully see a bit more clearly: This is just illegible nonsense. Don't do it please - it doesnt even look great.\n3) Button labels. Generally, check all labels of buttons again. Do they contain words that your mum would understand (i.e. Control deamon, backend)? No? Is there really no chance to rephrase this? Think twice here ;-)\n\nThe URLs are also I problem I agree. Sth. like that just shouldn't be\nexposed to the end-user. IMO it often helps to look at well-known interfaces\nand ask if you cant get a bit more similarity to those.\nIn this case, I would propose some kind of e-mail interface. Split the\nwindow in two parts. The top part contains just the list of search results\n(like the subject lines of all found emails). If you click your way through\nyou see more content in the bottom part. The top part might contain\nmultiple columns depending on the type of search results. If you are looking\nfor e-mails it should have a from column, etc. The filenames are not\ngenerally useful for everybody. They should be hidden for e-mails IMO.\n\nBTW: Who the heck has invented those horrible +/- interfaces I'm seeing\nincreasingly more often? Adept (package manage of Kubuntu) uses this\ntoo. This is bad, bad, bad! You click on an entry and suddenly all other\nentries shift to the bottom to make space for additional data. Please\nalways use a seperate part of the screen to show this information and\ndont shift items around all of a sudden. Imagine you are in a supermarket\nand there are two bottles of wine next to each other. You want to take\nboth of them. First you reach for the first bottle. The moment you touch\nit with your finger the other bottle slides away and makes room for\na sign \"9.50 EUR, DRY, RED\". Ridiculous and unusable, but sometimes such ideas seem to be normal in GUI design.\n\nHope my post isnt offensive but taken as a help to improve this app from a GUI point of view.\n\n\n\n\n\n\n\n\n"
    author: "Martin"
  - subject: "Re: yaBi"
    date: 2006-04-26
    body: "***...Adept (package manage of Kubuntu) uses this\ntoo. This is bad, bad, bad! You click on an entry and suddenly all other\nentries shift to the bottom to make space for additional data.***\n\n\nDon't forget that this behaviour doesn't allow selecting more than one item, at least in adept. No way of selecting several packages, to perform same action in all of them..."
    author: "NabLa"
  - subject: "Re: yaBi"
    date: 2006-04-26
    body: "Agreed, KDE may certainly excel in some areas over GNOME but this isn't one of them.\n\nI mean have a look at the Beagle GNOME app: http://nat.org/best.png and tell me the KDE app compares..."
    author: "Daemon"
  - subject: "Re: yaBi"
    date: 2006-04-26
    body: "Thankfully KDE has Kerry, which compares very well to Best.\n\nhttp://www.kde-apps.org/content/pre2/36832-2.png"
    author: "Anonymous"
  - subject: "Re: yaBi"
    date: 2006-05-01
    body: "To fix the '/' in paths, just use KURL.pathOrURL(). It does the right thing."
    author: "Aur\u00e9lien"
  - subject: "Re: yaBi"
    date: 2006-05-07
    body: "Thank's for all feedback."
    author: "Joel Mandell"
  - subject: "Scribus links are b0rken (as well as dead!)"
    date: 2006-04-25
    body: "Scribus links are b0rken (as well as dead!). Please fix!"
    author: "ac"
  - subject: "Re: Scribus links are b0rken (as well as dead!)"
    date: 2006-04-25
    body: "We had some unplanned, but needed maintenance going on part of yesterday and today. All will be back up in a few hours."
    author: "mrdocs"
  - subject: "Re: Scribus links are b0rken (as well as dead!)"
    date: 2006-04-25
    body: "How can we fix the broken server of the scribus project?\n\nAnyway, they told us that it was down for unscheduled maintainance.\nOnce it's up again, the links will work just fine.\n\nCheers,\n  Daniel"
    author: "Daniel Molkentin"
  - subject: "Re: Scribus links are b0rken (as well as dead!)"
    date: 2006-04-25
    body: "I do not think a link like 'http://www.scribus.net/index.php?name=News&file=article&sid=117%22' is valid, server down or not...."
    author: "anonymous coward"
  - subject: "Re: Scribus links are b0rken (as well as dead!)"
    date: 2006-04-25
    body: "When I wrote that part of the article I tested the links and they worked."
    author: "Carsten Niehaus"
  - subject: "Re: Scribus links are b0rken (as well as dead!)"
    date: 2006-04-25
    body: "I believe you. But someone introduced at least one typo in there when it was put online."
    author: "anonymous coward"
  - subject: "Re: Scribus links are b0rken (as well as dead!)"
    date: 2006-04-25
    body: "time will tell :o)"
    author: "ac"
  - subject: "Re: Scribus links are b0rken (as well as dead!)"
    date: 2006-04-25
    body: "It is all back up and the links are valid :)"
    author: "mrdocs"
  - subject: "Re: Scribus links are b0rken (as well as dead!)"
    date: 2006-04-26
    body: "Yes, they are now all valid, because the invalid one from before:\n\n--> 'http://www.scribus.net/index.php?name=News&file=article&sid=117%22'\n\nhas been changed to a valid one:\n\n--> 'http://www.scribus.net/index.php?name=News&file=article&sid=117'"
    author: "anonymous coward"
  - subject: "KDE apps need to consider 800x600 resolution"
    date: 2006-04-25
    body: "The minimum suppported KDE screen resolution is 800x600, and kerry's window is too big for the *supported* screen resolution. it should at least be resizeable. \n\nI love kerry as much I love rlocate. it's fast and get's the information we need instantly. thanks Binner for kerry!"
    author: "fast_rizwaan"
  - subject: "Re: KDE apps need to consider 800x600 resolution"
    date: 2006-04-25
    body: "While I agree with you: Kerry is not official part of KDE, and only those need to adhere to the KDE-rules. Kerry is \"just a KDE-app\". If it will move to an official modules (like kdebase) it has to be modified."
    author: "Carsten Niehaus"
  - subject: "I agree."
    date: 2006-04-26
    body: "There are Control Panel modules that do not adjust well to small resolutions. There should be more care put into this I think.\nThere are lots and lots of machines out there using 800 x 600."
    author: "Gabriel Gazz\u00e1n"
  - subject: "Sudoku"
    date: 2006-04-25
    body: "Great app for sudoku is Ksudoku."
    author: "MaBu"
  - subject: "Some questions on Guidance and Kcontrol"
    date: 2006-04-26
    body: "Could the Openusability.org people not take a look at guidance and help out a bit? I think the application would gain a great deal if it received some love from usability professionals.\n\nI have a number of other questions on Guidance. It seems to be a replacement for Kcontrol. Is it? Does it work anywhere else outside Kubuntu?\n\nWhat's the status of the kcontrol cleanup that was initiated a while ago? Has that now been left for KDE 4.0 as well?"
    author: "Gonzalo"
  - subject: "Re: Some questions on Guidance and Kcontrol"
    date: 2006-04-26
    body: "Guidance is actually meant to somewhat be the opposite of KControl.  Guidance is meant to configure the lower stuff in the system like Xorg, partitioning, etc (at least thats what I know, I've never used it)\n\nAlso I believe it will work outside of Kubuntu (they are just the first distro to include it)."
    author: "Corbin"
  - subject: "Re: Some questions on Guidance and Kcontrol"
    date: 2006-04-26
    body: "> Could the Openusability.org people not take a look at guidance and help out a bit?\n\nhttp://www.openusability.org/reports/?group_id=166\n\n> It seems to be a replacement for Kcontrol. Is it?\n\nNo. Guidance is a collection of configuration/utility modules for handling mounts, partitions and drivers, users and groups, services/daemons and Xorg configuration. These modules run inside any container applications, like kcontrol, that supports KCModules.\n\n> Does it work anywhere else outside Kubuntu?\n\nIt did run on Mandriva and Gentoo, but that was a while ago and I have not tried it since. Getting Guidance running on other distributions is usually a matter of making sure that any filesystem paths used are correct for the target distribution.\n\ncheers,\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Re: Some questions on Guidance and Kcontrol"
    date: 2006-04-26
    body: "The kcontrol changes were never planned for 3, but for 4."
    author: "Benjamin Meyer"
  - subject: "What happen with kat?"
    date: 2006-04-26
    body: "beagle is there....... but what happen with kat?\n"
    author: "Marc Collin"
  - subject: "Re: What happen with kat?"
    date: 2006-04-26
    body: "seems development has seriously slowed down. pity, i hope it gets up to speed again - until then, and until KDE 4 is out, beagle will be all we have. slow, heavy, crashy and eating memory."
    author: "superstoned"
  - subject: "Re: What happen with kat? - KIOCLucene !"
    date: 2006-04-26
    body: "KIOCLucene is equivalent of Beagle for KDE, not KAT, this is a littel bit different cathegory. \n\nhttp://kioclucene.objectis.net/\nhttp://kde-apps.org/content/show.php?content=23874\n\nSad thing KIOLucene is nearly unknown, contrary to Beagle, even in KDE world!\nIt definately needs more promotion.\n\nKIOLucene is built upon the same library - Lucene - like Beagle. Just uses C++ port not Mono port like Beagle. It's more or less wrapper for KDE - like Beagle for Gnome/Mono. They can do the same things.\n\nKIOLucene has no HTML result renderer, but apps like Kerry and yaBi should be adapted to KIOLucene very easily.\n\nKAT and KIOCLucene promised to cooperate recently - but not much happened for a while. Metadata extracting is the right area for sharing the code, KIOClucene is here little bit hard to configure.\n\n"
    author: "Espinosa"
  - subject: "Re: What happen with kat? - KIOCLucene !"
    date: 2006-04-26
    body: "Go KIOLucene! ;) That looks great, mebbe the KDE promo people need to talk this one up, C++ is great compared to using Mono\n\n"
    author: "mobtek"
  - subject: "KOffice behaviour"
    date: 2006-04-26
    body: "Hi,\n\nfrom time to time I try to use Kspread for calculating my tables. I normally use Openoffice Calc with the OpenDocument format (since it's available).\nSo, I installed the new 1.5 version of kspread and saw the same strange behaviour as every time. When scrolling down and up again (by pulling the scrollbar...erm ...thing (don't know the name of the moving bar in scrollbar)) the grid disappears on some places. Every time I repeat it, some other borders are gone. Ok, that's a cosmetic issue, but annoying although.\nThen I opened one of my OOo-OpenDocument-Spreadsheets and was wandering how badly the interoperability is actually working.\nSome borders (not the grid but the borders I drew by myself) were missing, the width and height of the cells differed and finally the rounding of all numbers had been cut off.\nI don't know which application did cause that trouble but my aqqaintance said he saw the disappearing grid while scrolling some versions before and he wondered why it is still there.\nWell, it looks better than OOo and is faster as well but for me it is no alternative at all at the moment.\n\nKind regards"
    author: "Fred"
  - subject: "Re: KOffice behaviour"
    date: 2006-04-26
    body: "Hi,\n\nlittle time, short answer:\n- the precision issue was fixed recently\n- the grid painting issue was fixed 5 minutes ago by me\n- I see the issue with the not saved borders (KSpread's issue).\n  I think, I know the code part which causes this. So, I'm confident\n  to also fix this before 1.5.1.\n\nIf you encounter more problems, file bug reports on bugs.kde.org, please. Going this way you'll help us a lot.\n\nThanks for reporting,\nStefan\n"
    author: "Stefan Nikolaus"
  - subject: "Re: KOffice behaviour"
    date: 2006-04-26
    body: "Well, that was really fast. :D\nBecause of the age of the disapearing-grid-behaviour I had never thought that it would be solvable that fast. Has nobody filed that as a bug before?\nI usually write bugreports ... shame on me not doing it this time. :)\nThank you for fixing. I think 1.5.1 could be usable for me. :)\n\nKind regards"
    author: "Fred"
  - subject: "Re: KOffice behaviour"
    date: 2006-04-27
    body: "Well after that I decided for the first time I would file a bug report  :)\n\nhttp://bugs.kde.org/show_bug.cgi?id=126321\n\nI wonder if it will be fixed by 1.5.1.."
    author: "Forrest McKerchar"
  - subject: "SuperKaramba"
    date: 2006-07-30
    body: "SuperKaramba feels a bit slow, so it's a bit useless.\nIt's not that great apps that it looks like (on screenshots)"
    author: "anonymous"
---
Two weeks ago, you <a href="http://dot.kde.org/1144660487/">read</a> about several apps which keep KDE 3.5 alive. Today's issue of the mini-series provides even more reasons to love KDE. Covered applications include <a href="http://koffice.kde.org/krita/">Krita</a>, the image and painting application, <a href="http://www.simonzone.com/software/guidance">Guidance</a>, a configuration tool, frontends to Beagle and finally <a href="http://www.scribus.net">Scribus</a>, the Qt-based DTP application.































<!--break-->
<h2>KOffice</h2>
<p>Have you ever looked for a perfectly integrated, mature, good looking and feature-rich office-suite for KDE? Do you think OpenOffice is too slow? Then have a look a <a href="http://www.koffice.org">KOffice 1.5</a>.
The most important improvement in version 1.5 is the support for <a href="http://en.wikipedia.org/wiki/OpenDocument">OpenDocument</a>. Instead of using its own file formats, KOffice now uses the same format as applications like OpenOffice. This means guaranteed interoperability with other Office suites and makes OpenDocument a real standard.</p>
<p>Despite having more components than any other office suite, things are improving very quickly. Version 1.5 features OpenDocument as the default file format, introduction of a unified scripting framework, enhanced accessibility for users with disabilities, the first major release of Kexi and a technology preview of KPlato, a new project management application. You can read more about it in the full <a href="http://www.koffice.org/announcements/announce-1.5.php">announcement</a>.
Even though the next big step is KOffice 2, scheduled for early 2007,  Krita and <a href="http://www.koffice.org/kexi">Kexi</a>, KOffice's database-frontend, (and maybe some more members of the KOffice family) will release feature-improved 1.6 versions.</p>

<h2>Krita</h2>
<p>Why do I mention <a href="http://koffice.org/krita/">Krita</a> when it is part of KOffice? Krita improved so much in the last few month that it deserves its own chapter!
Now, Krita is a full-featured pixel-based image application with layer support, full colorspace independence including support for L*a*b and CMYK, a versatile plug-in based architecture. Furthermore, it share a scripting-engine named Kross with other KOffice-applications, which means you can write <a href="http://www.ruby-lang">Ruby</a> and <a href="http://www.python.org">Python</a> scripts for it!</p>

<div style="width: 300px; border: solid grey thin; margin: 1ex; padding: 1ex; float: right">
<a href="http://static.kdenews.org/danimo/displayconfig-2.png">
<img src="http://static.kdenews.org/danimo/displayconfig-2_small.png" width="300" height="262" /></a><br />
<a href="http://static.kdenews.org/danimo/displayconfig-2.png">
Guidance's displayconfig even provides options for gamma correction.</a>
</div>

<h2>Guidance</h2>
<p>Guidance is a KDE-configuration tool written in Python. The day KDE 3.5.0 was released the Guidance-Team released version 0.5.0. Since then, they have released not less than six 0.6-releases, including hugely improved configuration tools for dual head setups and big improvements to the already known tools. Check out the latest <a href="http://www.simonzone.com/software/guidance">screenshots</a>!</p>

<h2>SuperKaramba</h2>

<p>Since <a href="http://netdragon.sourceforge.net/ssuperkaramba.html">SuperKaramba</a> was released with KDE 3.5.0 for the first time, many new themes have been improved or added. For an overview about SuperKaramba's capabilities in the current KDE 3.5-version read <a href="http://netdragon.sourceforge.net/release_notes_0_37.html">this announcement</a>.

<div style="width: 200px; border: solid grey thin; margin: 1ex; padding: 1ex; float: left" clear="all">
<a href="http://static.kdenews.org/danimo/superdoku_small.png">
<img src="http://static.kdenews.org/danimo/superdoku_small.png" width="200" height="216" /></a><br />
<a href="http://static.kdenews.org/danimo/superdoku.png">
SuperKaramba even provides games.</a>
</div>

If you are searching for new widgets to play with have a look on <a href="http://www.kde-look.org/index.php?xcontentmode=38">kde-look.org</a>.
Sudoku is becoming more and more popular. With <a href="http://www.kde-apps.org/content/show.php?content=34902">Su-per-Doku</a> Superkaramba even provides you with a great-looking game to play on your desktop!</p>

<h2>Twinkle -- Free VoiP</h2>
<p><a href="http://www.twinklephone.com/">Twinkle</a> is a free VoiP-software which enables you to talk to anyone using the SIP-Protokol. Twinkle is a KDE-application (<a href="http://www.xs4all.nl/~mfnboer/twinkle/index.html">screenshots</a>) and for example integrates with your addressbook!</p>

<h2>KSt</h2>
<p><a href="http://kst.kde.org/">KSt</a> is a real-time data viewing and plotting tool. In March 2006, the first bugfix release of KSt 1.2, version 1.2.1, was released. Version 1.3 is scheduled for May 2006. The list of features in KSt is huge: have a look at the homepage or at the <a href="http://kst.kde.org/screenshots.html">screenshots</a> to get an impression of this vivid project!</p>

<div style="width: 300px; border: solid grey thin; margin: 1ex; padding: 1ex; float: right">
<a href="http://static.kdenews.org/danimo/yabi.jpg">
<img src="http://static.kdenews.org/danimo/yabi_small.png" width="300" height="221" /></a><br />
<a href="http://static.kdenews.org/danimo/yabi.jpg">
yaBi is one of two new KDE applications for Beagle.</a>
</div>

<h2>Kerry and yaBi</h2>
<p>I bet you know Google Desktop. Well, for KDE you can now use <a href="http://en.opensuse.org/Kerry">Kerry</a>, a KDE-frontend to Beagle, the desktop search engine. <a href="http://kde-apps.org/content/show.php?content=33222">yaBi</a> is Yet Another Beagle search Interface. It is written in Python and <a href="http://kde-apps.org/content/preview.php?preview=3&id=33222&file1=33222-1.jpg&file2=33222-2.jpg&file3=33222-3.jpg&name=yaBi+-+beagle+search+client">looks just great</a>.</p>

<h2>Scribus - DTP for Linux</h2>
<p>For a long time there were no good DTP-applications available for Linux. Some years ago, <a href="http://www.scribus.net">Scribus</a> started to be good enough for many usecases.</p>
<p>While Scribus 1.2 is already used by many professional designers and even offers <a href="http://www.scribus.net/index.php?name=News&file=article&sid=79">commercial support</a>, the current development series with its latest offspring <a href="http://www.scribus.net/index.php?name=News&file=article&sid=117">1.3.3</a> offers huge improvements:</p>

<p>The developers have put a lot of effort into restructuring the codebase, so you will notice a tremendous speedup: in certain situations even tenfold! The PDF and PS export is is now commercial grade, including full support for the new PDF X/3 format. Color is a top priority in many commercial settings; the authors of Scribus are aware of this and so have improved colour-management greatly. Also, last but not least, there are now native ports for both MacOS and Windows which should make a transition to Scribus even easier!</p>



