---
title: "Ark Linux 2006.1 Released with KDE 3.5.4"
date:    2006-08-03
authors:
  - "brosenkraenzer"
slug:    ark-linux-20061-released-kde-354
comments:
  - subject: "Suggestions to increase participation in Ark Linux"
    date: 2006-08-03
    body: "Here are some suggestions for increasing participation in Ark Linux development:\n1) Set up a developer mailinglist so new developers can figure out whats \"hot\" and  what needs to be done for the next release.\n\n2) Set up commit log mailinglist so that developers know what other developers are doing (avoids duplicate work, increases efficiency, etc).\n\n3) Set up a user mailinglist so that users can discuss problems, wishes, and ways to promote Ark Linux.\n\nMake sure you host the archive locally and at the Mailing List Archive:\nhttp://marc.theaimsgroup.com/\n\nI realize that you have forums and a nifty web IRC app, but those aren't substitues for mailinglists that people can *subscribe* to and *search* the archives of."
    author: "AC"
  - subject: "Re: Suggestions to increase participation in Ark Linux"
    date: 2006-08-03
    body: "Hi,\nthanks for the suggestions!\nThe mailing lists you're suggesting already exist (mailing lists and forums are gated to each other -- subscribe at http://forum.arklinux.org/) and commit logs (build-watch at arklinux dot org) -- and we're on irc (irc.freenode.net #arklinux) all day, so coordination works.\n\nGetting the lists archived at marc in addition to in the forums is a good idea though - will do!\n"
    author: "Bernhard Rosenkraenzer"
  - subject: "Re: Suggestions to increase participation in Ark"
    date: 2006-08-03
    body: "Getting lists archived by marc is unfortunately not easy. I've asked them twice to archive our Amarok mailing lists (now hosted @ KDE), but never got a reply.\n"
    author: "Mark Kretschmann"
  - subject: "Re: Suggestions to increase participation in Ark"
    date: 2006-08-03
    body: "http://www.mail-archive.com\n\nEasier on the eyes, too."
    author: "foo"
  - subject: "No links seem to be working..."
    date: 2006-08-03
    body: "I tried to grab the iso, via ftp, http, and then torrent, for the live cd, and then the install iso, and none work.  Are the servers getting bashed right now? Why are the torrents missing?\n\nThanks, I'm anxious to check out the release, and appreciate your efforts with dvdrtools :)"
    author: "Joseph M. Gaffney aka CuCullin"
  - subject: "Re: No links seem to be working..."
    date: 2006-08-03
    body: "the isos are nowhere to be found.  Usually it's the other way around, the isos find their way to the mirrors and everybody scrambles to get them before the official announcement.  In this case, we have an announcement and no isos."
    author: "oggb4mp3"
  - subject: "Re: No links seem to be working..."
    date: 2006-08-03
    body: "Yes, we made the mistake of making the announcement after everything was ready on our server, without waiting for the mirrors to catch up (and due to traffic limits, all downloads are from mirrors).\n\nNext time we'll know better. ;)"
    author: "Bernhard Rosenkraenzer"
  - subject: "Re: No links seem to be working..."
    date: 2006-08-03
    body: "arklinux-live-2006.1.iso.torrent\n\nStill MIA, though the regular install torrent is there."
    author: "Joseph M. Gaffney aka CuCullin"
  - subject: "Re: No links seem to be working..."
    date: 2006-08-03
    body: "Looks like the combined load of rsyncs from the mirrors, seeding the torrents and the website were sufficient to get the box into heavy swapping (it's not quite dead -- still responds to ping and 1 char/minute over ssh, too slow for a quick fix though).\n\nWe're currently working on a fix (switching to a more lightweight CMS, rate-limiting rsync even for mirrors), everything should be ready soon.\n\n(Yes, we're aware of the fact that we shouldn't be doing all those things from the same box, and particular not from a low-end box like ours, and especially not one we don't have physical access to --- but we have to live with what we can get for $0)."
    author: "Bernhard Rosenkraenzer"
  - subject: "Re: No links seem to be working..."
    date: 2006-08-03
    body: "Speaking of dvdrtools, I'm currently working on a rewrite that uses QtCore -- and as usual, contributors are wanted..."
    author: "Bernhard Rosenkraenzer"
  - subject: "rpmhandler"
    date: 2006-08-03
    body: "Anyone have more info on rpmhandler? I'm curious as whether its just (not that this might not be useful) a frontend for something else or a real management system like apt-rpm/smart. I must admit I haven't looked too hard (only google and the ark website) but I haven't found anything on it. I'll probably download the live cd in the first instance as I don't have a spare machine to play with at present and so that won't be possible to play with it as a package handler.\n\nArk looks quite interesting, it's been on my list of distros to check for a while."
    author: "Simon"
  - subject: "Re: rpmhandler"
    date: 2006-08-03
    body: "rpmhandler is a simple application that handles the rpm mimetypes in Konqueror -- if you click on an rpm (or a set of rpms) on a website/ftp server/local filesystem, it launches a frontend that grabs the package through KIO and then installs it.\n\nThe backend work is done by rpm.\n\nFor packages in our own repositories, we use apt/kynaptic (and are currently playing with adept -- it's almost working)."
    author: "Bernhard Rosenkraenzer"
  - subject: "Re: rpmhandler"
    date: 2006-08-03
    body: "Ah, right - thanks for the info. It sounds like it could be quite useful, especially for someone coming from windows and used to clicking to launch an installer. The issue (I'm sure you're aware of) is if there are dependencies to be met - though I guess the user can easily enough install them with apt-get or kynaptic if they're in your repositories. It would be nice if all that could be transparent though. Smart which I use on suse since 10.1 can accept an arbitrary url for a package and install it with dependencies, but only via the command line.\n\nI've played with adept a bit (though mostly just used apt-get) setting up kubuntu on a housemate's pc and adept is looking promising"
    author: "Simon"
  - subject: "Re: rpmhandler"
    date: 2006-08-03
    body: "rpmhandler doesn't handle dependencies for now (if there are dependency problems, it displays them and offers cancel and ignore options).\nThe next version will probably have a slightly more intelligent approach and throw any missing dependencies at \"apt-cache search\" to see if it can be resolved.\n\nsmart has some interesting features, but I don't like its implementation at all - to start with I think it's a bad idea to write a low-level tool like a package manager in python (I'm not opposed to python in general -- just for things like a package manager: how do you use a python based package manager to repair an installation where python of one of the loads of libraries it uses is broken?).\n\nWe've been thinking about copying some features of smart to apt (or alternatively reimplementing smart in C++) though."
    author: "Bernhard Rosenkraenzer"
  - subject: "Re: rpmhandler"
    date: 2006-08-03
    body: "Yeah, I was really surprised when I first looked into smart and saw python mentioned, it does seem inappropriate for such a fundamental system tool. I'm very encouraged by your suggestion of using apt to assist with dependencies in rpmhandler if necessary. suse has an install with yast option following click on (only local, I think) rpms but this seemed like overkill so i generally just tried a command line install with rpm and went to yast if dependencies were an issue. A lightweight tool that uses just rpm if there are no dependencies has a lot of potential.\n\nDownloading Ark linux now, so I'll have a play in a bit. I'm looking for something quite light and very much desktop oriented for a laptop (that I haven't bought yet) and this certainly looks interesting."
    author: "Simon"
  - subject: "KDEfied OO.org"
    date: 2006-08-03
    body: "http://shots.osdir.com/slideshows/original.php?release=467&slide=20\n\nNice but the icons have too different sizes and the esp. the selected carriage return is to large. It creates no good impression because of these small design flaws."
    author: "knut"
  - subject: "Re: KDEfied OO.org"
    date: 2006-08-04
    body: "...starting openoffice.org....\n\nLooking at the icons, they al have the same size."
    author: "AC"
  - subject: "Re: KDEfied OO.org"
    date: 2006-08-04
    body: "OSdir's screenshots are from a very old release (a release candidate of 2005.1 -- more than a year old).\n\nIt still looks similar, but not exactly the same.\n\nAnd fully KDEified is not just about icons, it's also (and even more so) about KDE file open/close and print dialogs, using the KDE widget style, integrating with KAddressBook, etc."
    author: "Bernhard Rosenkraenzer"
  - subject: "To login or not to login?"
    date: 2006-08-04
    body: "Hi, while using the previous version of Ark, i noticed two strange things:\n\n1 - Ark does not create any passwords, so you can't login as user, nor as root.\nBut you can logout. And when youre in KDM, there is no way to login again. Why is that?\n\n2 - there is a root console in K-menu. When starting it, one will be prompted for a root password.\nBut there is no root password.. So why does the console require one?\n"
    author: "AC"
  - subject: "Re: To login or not to login?"
    date: 2006-08-04
    body: "Hi,\n\n> 1 - Ark does not create any passwords, so you can't login as user,\n> nor as root. But you can logout.\n\nThat is because we think most desktop users don't want to bother with accounts, they just want a system that works out of the box and doesn't need them to switch users every time they want to do something that requires root\naccess -- so by default, there's an autologin user account with access to launching admin tools (and konsole) as root. A related FAQ entry is here:\nhttp://www.arklinux.org/index.php?option=com_simplefaq&task=answer&Itemid=21&catid=14&aid=5\n\nSince we think that some people will definitely want a more traditional user setup, we aren't taking out the normal login/logout functionality -- it's just not active/useful in a default install.\n\n> 2 - there is a root console in K-menu. When starting it, one will be\n> prompted for a root password.\n\nNo, it doesn't prompt for a root password -- not unless you've created a new user account first and are using that, without giving it privileges to launch root konsoles without password. (You use the included \"kapabilities\" tool to grant root access to certain applications to different users).\n\nA FAQ entry explaining what to do if you've killed the default user without setting up a proper replacement is here:\nhttp://www.arklinux.org/index.php?option=com_simplefaq&task=answer&Itemid=21&catid=14&aid=22"
    author: "Bernhard Rosenkraenzer"
  - subject: "Re: To login or not to login?"
    date: 2006-08-04
    body: "Thanks for your reply.\n\nGiven the fact that my system behaved differently then the way it should behave, i  figured that there must be something wrong with my installation.\ni re-installed ark on another hard disk, and everything is working fine now, no more unexpected logins :)\n\nThanks again\n"
    author: "AC"
  - subject: "Re: To login or not to login?"
    date: 2006-08-05
    body: "Btw, some more information on how the security system works can be found here:\n\nhttp://wiki.arklinux.org/index.php/Ark_Security_System"
    author: "Bernhard Rosenkraenzer"
---
The <a href="http://www.arklinux.org/">Ark Linux</a> team is pleased to announce the immediate availability of the KDE centric desktop distribution <a href="http://www.arklinux.org/index.php?option=com_content&amp;task=view&amp;id=16">Ark Linux 2006.1</a> - including KDE 3.5.4, Amarok 1.4.1, Kopete 0.12.1 with a hotfix for connecting to ICQ after the latest protocol change, a fully KDE-ified OpenOffice.org 2.0.3, and X.org 7.1.1.  Another highlight in this release is the KDE based "rpmhandler", a tool that makes installing 3rd party packages easier than ever before.




<!--break-->
<p>Ark Linux 2006.1 is available both as an installation CD and a Live CD.</p>

<p>Ark Linux is a Free Software project that exists and grows only because of the help of many volunteers that donate their time and effort. Ark Linux is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, helping with support, writing documentation, translations, promotion, money, hardware, etc. All contributions are gratefully appreciated and eagerly accepted. We look forward to hearing from you soon.</p>


