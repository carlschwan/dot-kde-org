---
title: "LinuxDevCenter.com: Previewing KDE 4"
date:    2006-01-16
authors:
  - "liquidat"
slug:    linuxdevcentercom-previewing-kde-4
comments:
  - subject: "KDE 4 alpha?"
    date: 2006-01-20
    body: "when can we expect KDE 4 alpha/beta? dying to see the new KDE 4 features in action :)"
    author: "fast_rizwaan"
  - subject: "Re: KDE 4 alpha?"
    date: 2006-01-20
    body: "You have a bit of a wait as the visually exciting parts won't start presenting themselves for a few months.  Some of it may come earlier, but a lot of work is going into libs which doesn't show up until applications start using it."
    author: "Ryan"
  - subject: "Re: KDE 4 alpha?"
    date: 2006-01-20
    body: "Will SuSE Linux 10.2 have KDE 4?"
    author: "Guy"
  - subject: "Re: KDE 4 alpha?"
    date: 2006-01-20
    body: "No. Expect KDE4 to be released at the beginnig of 2007. OpenSuSE 10.2 will be released in Sep./Oct. 2006. But OpenSuSE 10.3 may include it.\nBe patient...\n"
    author: "birdy"
  - subject: "Re: KDE 4 alpha?"
    date: 2006-01-20
    body: "Where do you get that release date from? AFAIK, there is no release date, all that I've seen posted anywhere is sometime in the second half of 2006. I personally don't really have any expectations yet since I haven't seen much of a roadmap - so I expect it'll be released when it's ready :)\n\nOf course, I hope that will be sooner rather than later :)"
    author: "Joergen Ramskov"
  - subject: "Re: KDE 4 alpha?"
    date: 2006-01-20
    body: "i would prefer a final KDE 4 before 2007, but as aaron said we can expect a beta in the fall of 2006, i wouldn't count on it. yeah, sucks... ;-)\n\n"
    author: "superstoned"
  - subject: "Small \"typo\""
    date: 2006-01-20
    body: "In he second page, where it says\n\"theming is shared by Windows, the desktop, and panels\"\nit probably should read\n\"theming is shared by windows, the desktop, and panels\"\nsince we are talking about windows, not the OS."
    author: "Hugo Costelha"
  - subject: "GStreamer as multimedia system of choice?"
    date: 2006-01-20
    body: "IMHO, GStreamer is not a great choice of multimedia backend. I tried using it in amaroK as the default engine for a few months, and I was not very satisfied with its performance. It seemed prone to skip and rather unresponsive. Now that I am using the Xine backend, I like it much better.\n\nAre there still plans to implement the multimedia backend with a plugin-based system? If that were the case, I'm sure someone (maybe even me! I might be good enough at programming by that point) would write a Xine plugin, and then I would be happy."
    author: "Jack H"
  - subject: "Re: GStreamer as multimedia system of choice?"
    date: 2006-01-20
    body: "yes xine plugin is good, but what about recording and mixing stuff? jack is good too..."
    author: "fast_rizwaan"
  - subject: "Re: GStreamer as multimedia system of choice?"
    date: 2006-01-20
    body: "The plugin-based system is still on and in the works. It even changed name this week, from kdemm to Phonon:-)\n\nIf you want to use Jack as a multimedia solution, you have to use it together with something else like Xine. Since Jack is audio only. "
    author: "Morty"
  - subject: "Use a GPL multimedia backend to prevent DRM"
    date: 2006-01-20
    body: "Stick with Xine because it is GPL and all its plugins (including DRM-infested ones) would _have_ be open source. That means anybody can remove the DRM portion and be left with a fully-functional plugin.\n\nThe same cannot be said about GStreamer, which is LGPL and whose DRM plugins will most likely be proprietary, closed-source, and controlled by the big media corporations. Furthermore, any multimedia frontend wanting to use GStreamer would need to append exemptions to the GPL to specifically allow this. I hope Amarok, KMPlayer, and Kaffeine developers _don't_ compromise the GPL just so that the RIAA can ensalve the users of those applications.\n\nThe only way to keep DRM's nasty claws off KDE is to ensure that users can change the code, and the only way to do that is to chose a tool that is GPL.\n\nOther GPL'ed alternatives include MPlayer and VLC, so there are plenty of choices beside GStreamer."
    author: "anonymous"
  - subject: "Re: Use a GPL multimedia backend to prevent DRM"
    date: 2006-01-23
    body: "*** FUD ALERT ***\n\nSo let me get this straight. If these theoretical companies that would make plugins for linux to play media with drm were forced to link to a GPL library would, instead of deciding to not develop a plugin at all, make a fully GPL and open source plugin.\n\nAllow me to clue you in. No company is going to develop linux based media plugins because none of them care, and even if they would they will would rather not provide one at all than provide a GPL one. So, if you want to run only FOSS software then do not use any closed plugins that will never be made for GStreamer. Use the open ones that will likely be developed instead.\n\nFrom a programming standpoint the GStreamer API is more suited as a backend for a full multimedia framework. Using it is a wise choice."
    author: "MrGrim"
  - subject: "Re: Use a GPL multimedia backend to prevent DRM"
    date: 2006-01-29
    body: "I have to agree with Grim on this one. And given the choice between software that doesn't play my media files at all given to one that plays them, but using non-OS software, I dare say the latter is a LOT more useful. At least to me wanting to listen to music I paid for. Feel free to flame me for not being enough of an idealist...\n\nIf you don't like DRM, you're not going to make it go away by making music it affects unplayable to Linux-users, but by offering a better alternative."
    author: "David Vallner"
  - subject: "Re: Use a GPL multimedia backend to prevent DRM"
    date: 2006-01-29
    body: "You first ought to ensure something you buy works in your environment, instead adapting your environment to everything what your bought stuff ask for."
    author: "ac"
  - subject: "Isn't porting KDE to other OSes good for Linux ?"
    date: 2006-01-20
    body: "I'm not quite sure what to think about this, but I imagine the following scenario if KDE4 gets ported to Windows :\n\nI guess people who don't know a thing about Linux and use only Windows might try that \"KDE4\" thing they hear about because it looks great. They'll try it on Windows, get used to it and happy, and then they might realize they don't need Windows at all anymore. A friend that knows Linux could very well replace their OS for Linux, under the cover. Then the switch would be much more subtle.\n\nIn other words, my first impression is that porting to Windows would be a great thing for Linux because it seems to me that the main reason for not switching is familiarity and fear of drastic change. If the look and user interaction is first changed (with KDE for Windows), and then the OS, the change will be softer.\n\nBesides, even if they never change for Linux, it will, like Firefox, be a great demonstration of what Open Source can achieve and teach people Linux is much more than a black screen with obscure text commands (those are in fact very efficient and pleasant to use, but that is another story)."
    author: "Dom"
  - subject: "Re: Isn't porting KDE to other OSes good for Linux"
    date: 2006-01-20
    body: "Sure!but this needs big advertisements on Kde(4).maybe something like \"Spread the Firefox\" which was really usefull!\nI really think advertisement is needed for Open Source software, all we know that Open Source softwares are powerfull but the others dont know because theyre watching advertisiments.\nsorry for my bad English."
    author: "Emil Sedgh"
  - subject: "Re: Isn't porting KDE to other OSes good for Linux ?"
    date: 2006-01-20
    body: "\"... and then they might realize they don't need Windows at all anymore\"\n\nAnd how exactly are they supposed to realize that? And then why would they care?\n\n\"If the look and user interaction is first changed (with KDE for Windows), and then the OS, the change will be softer.\"\n\nOr rather there will be question: \"why I am supposed to change OS if  I have all good apps (including kde) already and after switch I will miss some Windows-only stuff?\"\n\n\"... it will, like Firefox, be a great demonstration of what Open Source can achieve ... \"\n\nWith the small problem that only small percentage of Firefox users have any idea what Open Source is. For all others Firefox is just one of thousands free apps available for Windows and is not demonstration of anything.\n\n"
    author: "Jakub Stachowski"
  - subject: "Re: Isn't porting KDE to other OSes good for Linux"
    date: 2006-01-20
    body: "I think KDE should be about providing a good desktop, good applications and a good application development framework - not about limiting people's choice of platform to promote Linux. Isn't the strength of Open Source that it provides people with what they want, where they want it, never mind the politics?\n\nPersonally, I recently had to switch to Windows on my laptop after using Linux+KDE since 1999. Next time I get a new computer, I intend to get a Mac. If there were native versions of KDE apps on those platforms, I would definitely use them. \n\n(Windows is infinitely inferior to Linux+KDE, I've really realized that since I swiched.. having KDE apps and libs available would ease the pain considerably).\n"
    author: "Apollo Creed"
  - subject: "Re: Isn't porting KDE to other OSes good for Linux"
    date: 2006-01-20
    body: "well, part of what KDE is is also about FREEDOM. it's sad many people don't get that (yet). i never fail to mention it to people i introduce to linux/KDE/free software."
    author: "superstoned"
  - subject: "Re: Isn't porting KDE to other OSes good for Linux"
    date: 2006-01-21
    body: "\"I think KDE should be about providing a good desktop, good applications and a good application development framework - not about limiting people's choice of platform to promote Linux\"\n\nYeah, but the argument goes both ways. Some Free Software people think that it is actually bad to release software for Windows and other proprietary systems because you are supporting them, and making their close (and in MS case even evil) OS better.\n\nPersonally, I used to think like these people. But:\n\n1) I changed jobs and I am forced to use Windows. It sucks, it is inferior, but I have no choice whatsoever. And I run as much free software as possible.\n\n2) Experience shows that releasing free software for proprietary OS's actually undermines them, because a user who is already using free software and loves it is more likely to later switch to a 100% free OS.\n\nThis last reason is the most powerfull to me. And I can't see the time when KDE/Winblows makes it and I can start using it at work\n\nCheers !"
    author: "MandrakeUser"
  - subject: "Re: Isn't porting KDE to other OSes good for Linux"
    date: 2006-01-24
    body: "At school I am forced to use Windoze and the school admin has specifically stopped me from bringing in Live CDs. So at school I use as many free software tools as I can. Once I show others that they are good, and I tell them that these tools originated on GNU/Linux, they realise that it is better than what they think and are more open to switching.\n\nAlso for people who are forced to use M$ and proprietary software, being able to bring KDE into the workplace or school can give them a familiar and much more powerful environment, without having to switch OS (which they can't).\n\nandrew"
    author: "ajdlinux"
  - subject: "Re: Isn't porting KDE to other OSes good for Linux"
    date: 2006-01-21
    body: ">>... and then they might realize they don't need Windows at all anymore\"\n>And how exactly are they supposed to realize that? And then why would they care?\n\nMy boss was hesitating for choosing Linux or Windows as a server. I was trying to tell him the benefits of using Linux but since he knew almost nothing about Linux and free software, he feared that somehow, provided that I was maybe the only person he met in his life who was speaking about open source. I sounded marginal to him.\n\nOne day he heard about firefox, tried it (for Windows of course), and loved it. Then he was much more interested to hearing what I had to say about Linux.\n\nThat's one of the many ways it can happen.\n\nI was not saying that one day people would wake up with an illumination coming from the sky. But that's one thing that would be added to ease the process of opening minds of people."
    author: "Dom"
  - subject: "Re: Isn't porting KDE to other OSes good for Linux"
    date: 2006-01-22
    body: "That's exactly point 2) in my post right above yours :-)"
    author: "MandrakeUser"
  - subject: "Re: Isn't porting KDE to other OSes good for Linux"
    date: 2006-01-22
    body: "I dont know if this logic can be applied to a majority of users but it certainly has applied to me. I am an old school Linux user from the days of slackware console only ilk and left Linux for Windows soon after. I started getting into open source on Windows wtih apps like the Kmeleon web browser a few years back. From there I went to Firefox and then Open office. Getting involved in those projects naturally led to looking at Linux. About a year and a half ago I decided to try a dual boot Mandrake system. Soon after I found myself hardly ever booting to XP. End result ... for over a year I have been Linux only. No doubt the learning curve is steep. And getting some things to work correctly can be a hair pulling experience. But it is almost always worth the trouble. No more Spy ware! No more viruses! And a visulay beautiful and long term stable system that makes me  look like a geek god to freinds family and neibors alike!  (Boy do I have them fooled :)!!  I revel as I listen to the never ending stories of OS woe from users of the windows community. Virus this spyware that. Uncontrollablle popups of a dubious and often vulgar nature. Sometimes I have to admiit to an odd feeling of guilt!  Whats the point of this long winded diatribe (With questionable spelling and grammer no doubt) It's that, at least for me, The journey to Linux began wtih Linux apps ported to windows.\n\nOne thing I do find curiuos though is the duplication of efforts in the open source community. As a community that is ever in need of volunteer effort this seems an especially agrievous error in structure. Take KDE for example. When I think KDE I think graphical desktop environment. Well thats great but why develop a web browser when you have the excellent Firefox. Why develop an office  suite when you have Open office. This is not too say the KDE apps aren't excellent cause they are and have come a long way to being so. Its just that it seems KDE as a graphical desktop environment would be that much more advanced and bug free if those developers were working on its core functions instead of on apps that are already the main focus of other groups that do it better. Just a passing thought. Just wondering. \n\nThanks to all who contribute to KDE as you have allowed me to be MS free."
    author: "Jim Muccio"
  - subject: "Re: Isn't porting KDE to other OSes good for Linux"
    date: 2006-01-22
    body: "Thanks for the input Jim\n\n> One thing I do find curiuos though is the duplication of efforts in the open\n> source community. As a community that is ever in need of volunteer effort \n> this seems an especially agrievous error in structure. \n\nThe key here is: there is no such a thing like a structure. You have a miriad\nof people volunteering their time to do whatever pleases them. This is the\ndriving force. The strong, meaningfull projects survive.\n\n> When I think KDE I think graphical desktop environment. Well thats great but > why develop a web browser when you have the excellent Firefox. \n\nBecause Konqueror is MUCH more than a web browser. It is a technological\nwonder. As a web browser, however, it has had some of the features that\nFirefox introduced to the Mozilla suite much earlier than Firefox even\nstarted development. The gecko engine has been mature for a while, granted.\nBut KHTML (konqueror's engine) has always given it a run for its money.\n\nYou can do many, many things in Konqueror, including visualizing man\npages in a great way (type \"man:ls\" in the location bar), info pages,\nit has so many protocolos that I wouldn't know where to start. It can\nembed any application using KParts. Such as Cervisia to browse CVS projects.\nIt let's browse your files (the filesystem). It serves very well as an\nftp client. It lets you access other machines through ssh (fish:/).\nNow there is a \"zeroconf\" protocol or \"ioslave\")\n\nWell, I would go here for more\nhttp://en.wikipedia.org/wiki/Konqueror\n\nCheers!\n"
    author: "MandrakeUser"
  - subject: "Re: Isn't porting KDE to other OSes good for Linux"
    date: 2006-01-23
    body: ">why develop a web browser when you have the excellent Firefox. \n>Why develop an office suite when you have Open office.\n\nOne of the main reasons are historical, but there are of course several others. When development of Konqueror and KOffice started Firefox and OO did not exist, other than in the form of their closed source ancestors Netscape and StarOffice. And when Mozilla(Firefox) was open sourced, it took a long time before it became usable. When Mozilla 0.9 was released KDE was in the 2.1 series, and quite frankly Konqueror was a better browser back then. "
    author: "Morty"
  - subject: "Scripting languages"
    date: 2006-01-20
    body: "\"introducing a standard application automation (scripting) system that centers around ECMAScript (aka JavaScript)\"\n\nWill it still be possible to use other languages, such as Python, Perl, Ruby or whatever comes next?"
    author: "ac"
  - subject: "Re: Scripting languages"
    date: 2006-01-20
    body: "IIRC it was said that python would be the flagship scripting language for\nsuperkaramba and plasma?\n\nIs that still true?"
    author: "ac"
  - subject: "Re: Scripting languages"
    date: 2006-01-20
    body: "Yes it will:\n\"Plasma will support writing add-ons, or \"applets\", in a number of programming languages. At the entry level, if you can design a web page and sling a little Javascript, you'll be able to write a Plasma applet. Python, Java, Ruby and C++ developers will also be able to select to use their favorite language to create additions.\"\n\nhttp://plasma.kde.org/cms/1029"
    author: "Fabio R."
  - subject: "Re: Scripting languages"
    date: 2006-01-21
    body: "yes, we'll ship python (and probably ruby and java) kits for plasma as well. js is just what will be shipped integrated, the rest will be plugins (for a variety of reasons, from overhead to developer community size)"
    author: "Aaron J. Seigo"
  - subject: "Re: Scripting languages"
    date: 2006-01-21
    body: "Yea, but the SVGt bits will be using ECMA.  (The standard)   But that won't stop anyone from writing other kinds of script integrations.  :)  Go SVGt!  I can't wait for SVGt emoticons in Kopete and Psi!  :)"
    author: "angrymike"
  - subject: "What about killer-apps?"
    date: 2006-01-21
    body: "I read there were chances for some \"killer-apps\" like e.g. a backup/export-tool that apps could register to in order to supply information on the whereabouts of their data and config-files, so that the user can easily pick the components to be backed-up/exported and set-up a schedule in order to get askes to insert a CD to burn the backup to.\n\nThe other killer-app was a network-manager that puts kwifi, kinternet, kvpnc and alike into one app.\n\nhttp://www.gnome.org/projects/NetworkManager/\nhttp://www.redhat.com/magazine/003jan05/features/networkmanager/"
    author: "Sven"
  - subject: "Re: What about killer-apps?"
    date: 2006-01-22
    body: "both things are being worked on, yes."
    author: "superstoned"
  - subject: "Re: What about killer-apps?"
    date: 2006-01-22
    body: "here one of them: http://www.kde-apps.org/content/show.php?content=34206"
    author: "superstoned"
  - subject: "funny, this page ..."
    date: 2006-01-21
    body: "doesn't open right in newest firefox.\n\nBut no problems in IE 6"
    author: "keber"
  - subject: "Re: funny, this page ..."
    date: 2006-01-21
    body: "Same here!"
    author: "mfv"
  - subject: "Re: funny, this page ..."
    date: 2006-01-21
    body: "perhaps someone has to remove the TRICK, where its done that IE displays it right.\n\nif its normal html code,  i think firefox would display it perfect."
    author: "chris"
  - subject: "Re: funny, this page ..."
    date: 2006-01-23
    body: "It works just peachy for me with Konqueror 3.5.0. Haven't tried it with IE or Firefox. "
    author: "SuSE User"
  - subject: "Xgl"
    date: 2006-01-22
    body: "how about Xgl? can we expect it to be released on spring 2007 too? or do we have to wait longer? it's getting amazing... :)"
    author: "litb"
---
There is an <a href="http://www.linuxdevcenter.com/pub/a/linux/2006/01/12/kde4.html">interview with Aaron J. Seigo</a> on <a href="http://www.linuxdevcenter.com">LinuxDevCenter.com</a>. John Littler visited one of Aaron's presentations and asked him about what he was shown, the current work on KDE 4, the goals of KDE and about the KDE port to windows. He describes the technologies to be used in KDE 4 and describes some of his plans for <a href="http://plasma.kde.org">Plasma</a> which he describes that "<em>the central concepts are workflow and beauty.</em>"






<!--break-->
