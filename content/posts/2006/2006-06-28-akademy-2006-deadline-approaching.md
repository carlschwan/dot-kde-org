---
title: "aKademy 2006 Deadline Approaching"
date:    2006-06-28
authors:
  - "agroot"
slug:    akademy-2006-deadline-approaching
comments:
  - subject: "KDE in Community Centers in South Florida"
    date: 2006-06-28
    body: "Hi,\n\nI led a team of people that deployed KDE-based desktops at community centers in South Florida. Our efforts were reported and recognized by the local media. \n\nI am currently in Spain and would love to share what we learned in the process of deploying and managing these desktops over the last three years. Among other things, we did considerable user testing of both Gnome and KDE and decided to use KDE because of KDE's smaller learning curve, its Kiosk management tool, and its much better behavior and performance while being used on thin clients.\n\nAnyway, Is there any kind of funding available that would cover plane tickets and accomodation?"
    author: "Gonzalo"
  - subject: "Re: KDE in Community Centers in South Florida"
    date: 2006-06-28
    body: "Very cool.  Where in South Florida?  Details, for those of us who know the area!"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: KDE in Community Centers in South Florida"
    date: 2006-06-28
    body: "Liberty City and Downtown Miami, primarily."
    author: "Gonzalo"
  - subject: "Re: KDE in Community Centers in South Florida"
    date: 2006-06-29
    body: "> Anyway, Is there any kind of funding available that would cover plane tickets\n> and accomodation?\n\nit really depends. please send in an abstract and note that you will likely require travel assistance and note where you would be flying in from along with estimated costs. if your talk gets accepted then it can be looked into further. no promises, but unless an abstract is submitted it certainly won't happen."
    author: "Aaron J. Seigo"
---
The deadline for <a href="http://conference2006.kde.org/">aKademy 2006</a> submissions is fast approaching. We are still looking for technical contributions, community success stories, tales of interoperability, industrial innovation and integration and cross-desktop creativity. The <a href="http://conference2006.kde.org/conference/call.php">Call For Papers</a> says that a 300 word abstract and a short bio is needed to secure your place for consideration by the programme committee. Send them in!

Conference registration will open soon. The website says "in june", and it will.


<!--break-->
