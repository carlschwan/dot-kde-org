---
title: "First KOffice 1.6 Maintenance Release Available"
date:    2006-11-30
authors:
  - "iwallin"
slug:    first-koffice-16-maintenance-release-available
comments:
  - subject: "Packages"
    date: 2006-11-30
    body: "FYI, it seems packages for ArchLinux has been available since 7.11."
    author: "lucke"
  - subject: "Re: Packages"
    date: 2006-11-30
    body: "That hardly can be the final 1.6.1 version including security fix."
    author: "Anonymous"
  - subject: "OT: Birmingham selects a DE the hard way"
    date: 2006-11-30
    body: "Techworld: Birmingham City Council claims open-source success\n\n\"The case study also detailed the many frustrations involved in approaching an unfamiliar desktop technology, including the discovery that key applications wouldn't run on Linux and usability problems with the original Gnome interface. At one point, realising that most of the usability issues were attributable to Gnome, which had taken three months to configure, staff ripped out Gnome and replaced it with KDE. The new interface was up and running within a week.\"\n\nhttp://www.techworld.com/opsys/news/index.cfm?newsid=7459"
    author: "Plain Troll"
  - subject: "Re: OT: Birmingham selects a DE the hard way"
    date: 2006-11-30
    body: "\"At one point, realising that most of the usability issues were attributable to Gnome, which had taken three months to configure, staff ripped out Gnome and replaced it with KDE. The new interface was up and running within a week.\"\n\nMust... resist... temptation... to... say... I... told... you... so...\n\nGNOME has a lot of good interface and usability ideas. But they take them to extremes, and often create more problems then they claim to solve."
    author: "Brandybuck"
  - subject: "Re: OT: Birmingham selects a DE the hard way"
    date: 2006-12-01
    body: "\"ideas usability and interface good of lot a has GNOME\"\n\nDo you understand me?  No / Yes ? ;)"
    author: "Miquel"
  - subject: "Re: OT: Birmingham selects a DE the hard way"
    date: 2006-12-01
    body: "Actually, I was talking to some people about this: How they set this up was that they had 3 systems: one running gnome, one running KDE and one running XP. People were then allowed to test them and say which they liked most. GNOME was the most popular but the sysadmins were KDE users, so they changed the system afterwards to KDE.\n\nTake that with as much pinches of salt as required..."
    author: "pete"
  - subject: "Re: OT: Birmingham selects a DE the hard way"
    date: 2006-12-01
    body: "\"Some people\", should have been \"some people involved in this\", not just random people I met in the street."
    author: "pete"
  - subject: "Re: OT: Birmingham selects a DE the hard way"
    date: 2006-12-01
    body: "for sure, I would claim the same but then ask me, why the admins actualy tried to get it working with GNOME for 3 months before... anyway, that they still needed 1 week to have it done with KDE shows imho clearly, that they don't used KDE before :)"
    author: "Sebastian Sauer"
  - subject: "Re: OT: Birmingham selects a DE the hard way"
    date: 2006-12-03
    body: "This isn't particularly surprising. Gnome appeals to many users very much during the first few minutes. It only becomes apparent later on that its environment only offers way too limited functionality even for the common user. Obviously that's what happened here.\n\n> People were then allowed to test them and say which they liked most. \n> GNOME was the most popular but the sysadmins were KDE users, so they \n> changed the system afterwards to KDE.\n\nI laughed a lot about this. That's a not so clever version of the \"Oooh, KDE appeals to geeks only\"-troll. Couldn't you come up with something better next time if you want to badmouth KDE? \nThank you."
    author: "Kandalf"
  - subject: "Re: OT: Birmingham selects a DE the hard way"
    date: 2006-12-01
    body: "It would be good if someone from kde-usability team would contact\nthem and ask what was good in KDE to not get rid of it in the future.\n(And what was bad in Gnome to not acquire accidentally bad habits.)\n"
    author: "m."
  - subject: "Krita vs packagers"
    date: 2006-11-30
    body: "From the README file located at ftp.kde.org/pub/kde/stable/koffice-1.6.1/SuSE/README:\n\n#################\n# SUSE Linux 10.0\n\nhttp://software.opensuse.org/download/repositories/KDE:/Backports/SUSE_Linux_10.0/\ncontains also packages for SUSE Linux 10.0 but note that the koffice-illustration package does NOT contain Krita as the distribution's libcms is too old to build it.\n\n# SUSE Linux 9.3\n\nhttp://software.opensuse.org/download/repositories/KDE:/Backports/SUSE_Linux_9.3/\ncontains also packages for SUSE Linux 9.3 but note that the koffice-illustration package does NOT contain Krita as the distribution's libcms is too old to build it.\"\n######################\n\nSo no Krita for all distributions but the most recent ones?\n\nAnd only because of a feature (colour management), *NOT* used by most users.\n\nShouldn't it be possible to build Krita\na) without liblcms\nb) an version of liblcms included in Krita's source tree\nc) an old version of liblcms?\n\n"
    author: "Max"
  - subject: "Re: Krita vs packagers"
    date: 2006-11-30
    body: "about c): Actually Krita can build and run against the \"old version of liblcms\" but it's considered as faulty by the Krita developers and they don't want Krita to be blamed for these errors - hence they added the version check in \"configure\"."
    author: "Anonymous"
  - subject: "Re: Krita vs packagers"
    date: 2006-11-30
    body: "> a) without liblcms\n> b) an version of liblcms included in Krita's source tree\n> c) an old version of liblcms?\n\nto a) and c) no, lcms is more to us than colormanagement, krita use a lot of function inside lcms, so yes we could replace them with our own implementation; but that's a lot of work. And why would we want to reenvent the wheel ?\nto b) no again, seriously the problem is a suse problem, why can't suse packagers propose a backport of lcms along with krita ? like other distributions are doing ? That's the real question.\n\nAnd I am pretty sure you would have notice the problem, as I did notice, and I don't care that much about color management."
    author: "Cyrille Berger"
  - subject: "Re: Krita vs packagers"
    date: 2006-11-30
    body: "> seriously the problem is a suse problem, why can't suse packagers propose a backport of lcms along with krita ?\n\na) because it's \"a lot of work\" to make everything work run for more distributions than the next and current one.\nb) because SUSE has the policy to not update system libraries, risking breakage of other applications, for a newer version of some application.\n\nIt's sad that you see it as problem that SUSE provides backports (for more than one release) and respecting the decision of the Krita developers (as we easily could have patched out the configure version check)."
    author: "binner"
  - subject: "Re: Krita vs packagers"
    date: 2006-11-30
    body: "Hi Stefan,\n\nWouldn't it be possible to include a \"local\" lib_lcms into the krita-RPM?\n\nI know that it would be somewhat against the clean unix philosophy, but it would not only help Krita, but also several other programs for which Suse provides update-packages (amarok, gimp-unstable, samba,...).\n\n\n\n"
    author: "Max"
  - subject: "Re: Krita vs packagers"
    date: 2006-11-30
    body: "see point a)"
    author: "binner"
  - subject: "Re: Krita vs packagers"
    date: 2006-12-01
    body: "I you had patched out the version check and released krita, I would have been seriously displeased. In fact, if that would have happened, I would preferred SUSE to chose another name for the applications since I would not want to deal with the flak I would get in bugzilla alone."
    author: "Boudewijn"
  - subject: "Re: Krita vs packagers"
    date: 2006-12-01
    body: "Both the SUSE and Krita devel approaches to this seem quite reasonable imho. The large number of backports that we do get on SUSE are much appreciated and upstream developers should not have to waste time investigating bugs produced by distro patching of their applications."
    author: "Simon"
  - subject: "Re: Krita vs packagers"
    date: 2006-11-30
    body: "> So no Krita for all distributions but the most recent ones?\n\nRight, do you know any distribution other than SUSE which provides backports for older releases than the most recent one at all? And next week you can mention also Krita 1.6.1 backports for the pre-recent version (then 10.1) ;-)..."
    author: "binner"
  - subject: "Re: Krita vs packagers"
    date: 2006-12-01
    body: "> Right, do you know any distribution other than SUSE which provides backports\n> for older releases than the most recent one at all?\n\nSuse is not only competing with other Linux distributions but also with Windows. If the newest Paintshop Pro would only run under Windows XP SP2 than something would be wrong.\n\nEven if you don't look at Windows, Krita still is an image manipulation program. The linux-versions of the competing programs Google Picasa and Bibble-lite/pro all run under nearly all distributions.\n\nI know that it's hard to make good packages but maybe a standard procedure to include updated local libs into open-suse RPMs could be developed. This would make it much more easy to create Suse 9.3 packages of Kirta, xine, amarok, Gimp, gutenprint,...\n\nIt's no wonder that klik:// is quite popular.\n\nBut independently from this: keep up the good work!\n"
    author: "Max_"
  - subject: "Re: Krita vs packagers"
    date: 2006-12-01
    body: "I don't know about Photoshop, but Adobe Premiere Pro 2.0 requires SP2."
    author: "FreqMod"
  - subject: "Re: Krita vs packagers"
    date: 2006-12-01
    body: "Sorry, i read Photoshop, but the point is still valid. "
    author: "FreqMod"
  - subject: "Re: Krita vs packagers"
    date: 2006-12-01
    body: "Photshop CS2 requirements are \"Microsoft\u00ae Windows\u00ae 2000 with Service Pack 4, or Windows XP with Service Pack 1 or 2\" so it sure does require a recent version, especially considering that service packs aren't really windows upgrade but a collection of security patches. So you can't use anymore the latest photoshop on windows 98. And the main difference is also that linux distribution are free, so you can upgrade then for free if you want the latest software.\nThe klick approach of packaging is good only for testing. Because having one version of each libraries available with each application, have a lot of problem : disk space, each time there is a security update you need to update each the library in each application.\n "
    author: "Cyrille Berger"
  - subject: "Re: Krita vs packagers"
    date: 2006-12-03
    body: "Do you know the difference in age between Suse 10.1 (Suse 10.0 is too old!) and Windows XP SP1? Service Pack 1 has been released on 9/2002, so the latest and greatest Photoshop runs on a more than 4 years old Windows. Other graphics programs run on even older Windows versions.\nOn he other hand, the second most recent suse distribution is too old to run Krita. \nSuse 10.0 is roughly one year old and too old, while Win XP SP1 is roughly four years old and enough to run Photoshop CS2. Do you see the difference?\n\nIf I had to guess, I would say that probably 90% of all Linux installations are to old for Krita. A good way to limit your user- and developer-base."
    author: "max"
  - subject: "Re: Krita vs packagers"
    date: 2006-12-03
    body: "Yeah, well, what do you propose? And more importantly, what do you actually propose to do _yourself_? I mean, every time KOffice is released you're griping about Krita on the dot, but that seems to be all."
    author: "Boudewijn Rempt"
  - subject: "Re: Krita vs packagers"
    date: 2006-12-03
    body: "My suggestion is to include a working snapshot of lib_lcms into Krita's/Koffic's source tree. Digikam did the same with dcraw and got rid of all dependency problems.\n"
    author: "Max"
  - subject: "Re: Krita vs packagers"
    date: 2006-12-04
    body: "dcraw isn't a library."
    author: "Boudewijn Rempt"
  - subject: "Re: Krita vs packagers"
    date: 2006-12-04
    body: "Does it make any difference?\nKrita could do instead of\n# load system wide lib_lcms.so\n\n# load /opt/kde3/libs/kritas_lib_lcms.so\n\n"
    author: "Max"
  - subject: "Debian packages available"
    date: 2006-12-01
    body: "As we approach the Etch release, I've asked for permission to upload KOffice 1.6.1 and haven't got an answer yet (I guess it'll be positive). In the meantime you can get KOffice 1.6.1 for Debian at:\ndeb http://people.debian.org/~isaac/upload/ ./\n\nBest regards"
    author: "Isaac Clerencia"
  - subject: "Re: Debian packages available"
    date: 2006-12-01
    body: "What about packages for Woody?"
    author: "Tim"
  - subject: "Re: Debian packages available"
    date: 2006-12-01
    body: "LOL"
    author: "NabLa"
---
The KOffice team today released the <a href="http://www.koffice.org/news.php#itemKOffice161released">first bug-fix release</a> in their 1.6 series. Many bugs in Kexi and Krita as well as in most other components were fixed, thanks to the helpful input of our users. We also have updated languages packs. You can read more about it in the <a href="http://www.koffice.org/announcements/announce-1.6.1.php">announcement</a>, and the <a href="http://www.koffice.org/releases/1.6.1-release.php">release notes</a>. A <a href="http://www.koffice.org/announcements/changelog-1.6.1.php">full changelog</a> is also available. Currently, you can download binary packages for <a href="http://kubuntu.org/announcements/koffice-161.php">Kubuntu</a> and <a href="http://download.kde.org/stable/koffice-1.6.1/SuSE/">SUSE</a>.




<!--break-->
