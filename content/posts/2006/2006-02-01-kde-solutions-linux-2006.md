---
title: "KDE at Solutions Linux 2006"
date:    2006-02-01
authors:
  - "cmiramon"
slug:    kde-solutions-linux-2006
comments:
  - subject: "medintux?"
    date: 2006-02-01
    body: "Where is the download link for Medintux?"
    author: "ac"
  - subject: "Re: medintux?"
    date: 2006-02-01
    body: "Alternative site for CCAM View (which is part of MedinTux):\n\nhttp://upsis.club.fr/MedinTux"
    author: "spacegoret"
---
<a href="http://www.solutionslinux.fr/fr/index.php">Solutions Linux</a> is currently taking place in Paris -- January 31st to February 2nd. KDE is participating in the <a href="http://www.solutionslinux.fr/fr/exposer_associations.php">Pavillon des associations</a> and we're running a booth over there.  We are welcoming present and future users to our demonstration of the latest stable version of KDE. We are also showcasing various riches of the KDE universe including <a href="http://www.koffice.org/krita/">Krita</a>, the fast evolving KOffice painting application, <a href="http://medintux.org/">Medintux</a>, a cross-platform Qt GPL patient management framework for General Practitioners and Accident and Emergencies Departments, as well as <a href="http://www.dental-on-line.com/">Dental On Line</a>, a complete commercial <a href="http://www.riverbankcomputing.co.uk/pykde/index.php">PyKDE</a> solution for dentists.












<!--break-->

