---
title: "KDevelop Authors Talk About Their Coming Releases"
date:    2006-08-04
authors:
  - "jriddell"
slug:    kdevelop-authors-talk-about-their-coming-releases
comments:
  - subject: "Thanks!"
    date: 2006-08-04
    body: "wow this was awesome to read. Very entertaining, and i cannot wait to see how your collaborative coding ideas will come out. \n\nDo you use kdevelop3 to produce kdevelop4? how do the layers crossover there?\nyou guys do a great job, and using your program is not only a good way to code.. but its very educational, it takes all the best ideas and tools and lets beginners pick them up.\n\n\n\n"
    author: "Robert Guthrie"
  - subject: "Re: Thanks!"
    date: 2006-08-04
    body: "I personally use KDevelop 3.4 to work on KDevelop4. I can no longer live and develop without my beloved IDE ;)\nWe have basic support for CMake projects in 3.4 so that development for KDE4 is absolutely possible.\n"
    author: "adymo"
  - subject: "Re: Thanks!"
    date: 2006-08-04
    body: "Sounds great. Could you please post your setup for work with cmake? \nI have tried to create new cmake project from menu but with no success. Using 3.4 from SVN. "
    author: "vrabcak"
  - subject: "Re: Thanks!"
    date: 2006-08-04
    body: "Well, that's just about generating the project file with \"cmake -G KDevelop3\" and opening it in KDevelop."
    author: "Alexander Dymo"
  - subject: "Why?"
    date: 2006-08-04
    body: "We need to concentrate on making KDevelop 4 work well with at least four languages: C++, Java, C# and Ruby\n\n\u00bfwhy C# and not lenguage as php, perl, python all them open source?.\n\n"
    author: "Anonimous"
  - subject: "Re: Why?"
    date: 2006-08-04
    body: "I agree with Aanonymous.\n\nI'd like to see at least python supported besides ruby.\nPhp and perl would not hurt either."
    author: "Anonymous"
  - subject: "Re: Why?"
    date: 2006-08-04
    body: "Just adding a vote for Python. If I have time I'll try and contribute something, but realistically it's not going to happen."
    author: "mikeyd"
  - subject: "Re: Why?"
    date: 2006-08-04
    body: "In order to support Python, the best way would be to write a grammar file for kdevelop-pg.  If someone is willing to take that on, rock it!  However, the current developers are not interested in doing this."
    author: "manyoso"
  - subject: "Re: Why?"
    date: 2006-08-08
    body: "One more for Python, please. :)\n\nAs for the grammar files... Aren't they available right on the Python site?\n-> http://docs.python.org/ref/grammar.txt"
    author: "A.C."
  - subject: "Re: Why?"
    date: 2006-08-04
    body: "I'd also like to see python support!\nIs it hard to write a grammer file?"
    author: "panzi"
  - subject: "Re: Why?"
    date: 2006-08-04
    body: "It is not a trivial task, that is for sure.  Jakob can give you more information on what is involved.  You'd need some pretty good computer science skills though."
    author: "manyoso"
  - subject: "Re: Why?"
    date: 2006-08-04
    body: "Er... computer science skills? How in the world did I manage to write those two parsers, then? No, really, I didn't have _any_ parsing skills when I started.\n\nParsing theory and the kdevelop-pg parser generator were a black box for me until after I finished the parser. It's not really hard, you just need to practice it a little. Currently, I can't think of any special hints to give away, but if you need assistance, don't hesitate to mail me.\n(jpetso ]at[ gmx.at)"
    author: "Jakob Petsovits"
  - subject: "Re: Why?"
    date: 2006-08-04
    body: ">why C# and not lenguage as php, perl, python all them open source?\n\nBecause someone wanted to work on C# and, so far, no one has expressed interest in developing and maintaining PHP, Perl or Python.\n\nIt's not a decision, it's a question of man power."
    author: "teatime"
  - subject: "Re: Why?"
    date: 2006-08-07
    body: "Quanta has excellent PHP support. Because both KDevelop and Quanta will be using the same framework this is covered, and in particular it seems if you're using PHP you're probably doing web development so various other supported features will be nice. My understanding from previous discussions is that there will be the ability to utilize features from either applications as it will all be plugins and in theory should all play nice.\n\nAs for other languages supported by Quanta, like KDevelop, you just need the definition files to make it work. Some people wanted ColdFusion support. Eventually they put it together and we supported them with instruction and parser enhancements. We welcome this for Perl or other languages."
    author: "Eric Laffoon"
  - subject: "Re: Why?"
    date: 2006-08-04
    body: "Ok, let's recap...\n\n- PHP is Quanta's domain. They are working hard on good PHP support, and with Quanta 4 based on the KDevelop Platform, it might not be too hard to get their PHP features fed back into KDevelop, if we need it.\n\n- Perl is a maintenance nightmare. I don't have the slightest desire to help a language that should have never existed in the first place. (Biased opinion. Don't reply if you're a Perl fan ;)\n\n- Python already has a great KDE-based IDE, which is Eric3. I don't see the point in duplicating their work, only because KDevelop must do everything that's possible, even if there's already a good solution for it.\n\n- KDevelop does have good support for an Open Source scripting language, Ruby, and will even improve it with KDevelop 4.\n\n- And finally, C# is lacking. If it stays the way it is, Richard Dale's C# bindings for Qt/KDE (Qyoto/Kimono) will only be used by a few die-hard C# fans because of missing IDE support. C# is a cool language, and has a good mixture between ease of use, performance and popularity, and imho it \"just feels right\". Mono is Open Source too, and major distributors are backing it. I don't want KDevelop to lose users to MonoDevelop just because there's no C# support.\n\nSummary:\nWe can have all of it! There's a lot of IDE power available for all kinds of (Open Source) languages right now, and I chose to take something on that's falling behind until now."
    author: "Jakob Petsovits"
  - subject: "Re: Why?"
    date: 2006-08-06
    body: "\"PHP is Quanta's domain. They are working hard on good PHP support, and with Quanta 4 based on the KDevelop Platform, it might not be too hard to get their PHP features fed back into KDevelop, if we need it.\"\n\nRight. If all goes well it will be that way that if you load a C++ project file in Quanta it will act as KDevelop if you have KDevelop installed as well. ;-)\n \n"
    author: "Andras Mantia"
  - subject: "Re: Why?"
    date: 2006-08-09
    body: "Just as a point of input, there are two types of PHP developers: ones who are approaching it from HTML oriented view (in which case Quanta would be excellent), and developers who approach it from a code oriented view, keeping the interface and all associated HTML far away from the code.  This is especially true in shops where a graphic designer works on implementing the interface and there is a complicated back end.  In that case PHP development occurs more through breakpoints, error logs and other code oriented features and Quanta is a mismatch to what is needed."
    author: "Anon"
  - subject: "Link not working"
    date: 2006-08-04
    body: "Link to Kdevelop4 screenshot (http://dot.kde.org/1154654739/kdevelop4.png) does not work.\n\nAnd BTW, big thanks to KDevelop developers. I use it extensively and love it! :)\n"
    author: "Krzysztof Lichota"
  - subject: "Re: Link not working"
    date: 2006-08-04
    body: "Fixed."
    author: "binner"
  - subject: "A little whinge (sorry!)"
    date: 2006-08-04
    body: "KDevelop is pretty impressive, but I've always found the fact that it started with 30 toolbar buttons and 14 tabs to be a bit intimidating. Could it be possible to re-organise the IDE along the lines of NetBeans or Visual Studio so there's not such a bewildering amount of information on-screen at one time? \n\nOtherwise though, keep up the good work. I've always been amazed at how far I can get with KDevelop using just the usual wizards."
    author: "Bryan Feeney"
  - subject: "Re: A little whinge (sorry!)"
    date: 2006-08-04
    body: "Settings->Configure Plugins...\n\nRemove what you don't want/use."
    author: "teatime"
  - subject: "Re: A little whinge (sorry!)"
    date: 2006-08-04
    body: "Or as Adam said a \"messy soup of features that do not always integrate well with each other\". :)"
    author: "Ian Monroe"
  - subject: "Re: A little whinge (sorry!)"
    date: 2006-08-04
    body: "One of the big opportunities for KDevelop 4 is to clean up our UI.\n\nPersonally, I want it to work like XCode.  Did you notice that Qt 4 designer works like XCode?  With the top-level and docked modes?\n\nWe will also be providing a perspectives feature that will blow everything else out of the water.  How?  It will be fully configurable.  You define your own perspectives in addition to the pre-defined ones.  Work the way you want!"
    author: "manyoso"
  - subject: "clutter"
    date: 2006-08-04
    body: "Is it possible to get or take smaller fonts in the nested windows. The screenshot you provided for KDevelop4 clearly shows the problem which applies to many KDE programs. Manybe its not the font itself but distance."
    author: "knut"
  - subject: "Re: clutter"
    date: 2006-08-04
    body: "Sure, it is possible.  Perhaps we'll have a font configuration setting for plugins."
    author: "manyoso"
  - subject: "love to see snippet support"
    date: 2006-08-04
    body: "i'd love to see snippet support in kde(velop)\ngedit has recently integrated snippets as a plugin and it works quite nice. textmate though is king in this area. everyone should take a look at it's <a href=\"http://macromates.com/screencasts\">podcastmovies</a> to see snippets in action!\n\ncollaborative programming would be great too, as seen in <a href=\"http://www.codingmonkeys.de/subethaedit/collaborate.html\">SubEthaEdit</a>\n\nso still lots of work to do :)"
    author: "Anonymous"
  - subject: "Re: love to see snippet support"
    date: 2006-08-04
    body: "great, when previewing my comment it said html links would be supported, guess that's not so true :)"
    author: "Anonymous"
  - subject: "Re: love to see snippet support"
    date: 2006-08-04
    body: "Take a closer look.. <A> is supported, but not <A .*>.. :) (made the same mistake)"
    author: "dAniel"
  - subject: "Re: love to see snippet support"
    date: 2006-08-04
    body: "Recent post on kdevelop-devel has someone working on a new snippets plugin for kdevelop 4."
    author: "manyoso"
  - subject: "Re: love to see snippet support"
    date: 2006-08-04
    body: "If you are considering snippet support, please look at the work being done with basket. http://basket.kde.org/development.php \n\nThis program is the king of desktop snippet stuff. You can keep all kinds of stuff in it. Very useful. Development version has integration with Kontact and Konqueror. Would be nice to be able to have it integrate into KDevelop and then be able to load additional KDev specific, or even project specific baskets."
    author: "Jonathan Dietrich"
  - subject: "Re: love to see snippet support"
    date: 2006-08-06
    body: "Just want to say this is a great idea. I am using basket more and more as a versatile notetaker, and it's quite amazing. Come to think of it, any kind of integration of basket and other KDE apps is a great idea, as well."
    author: "Anonymous"
  - subject: "Re: love to see snippet support"
    date: 2006-08-11
    body: "You may want to check out the following proposal/RFE for an extension of the snippets support in KDevelop: http://bugs.kde.org/show_bug.cgi?id=106165"
    author: "Guest"
  - subject: "don't you forget about me (la la la....)"
    date: 2006-08-04
    body: "<i>My personal goal is to make KDevelop 4 so good that core KDE developers like Zack Rusin (Emacs guy) and Aaron Seigo (Vim guy) just have to switch. Zack and Aaron will tell you that this is not an easy goal to accomplish. I'll be very happy if KDevelop 4 becomes the release known for converting the KDE core developers into KDevelop users.</i>\n\nLoved reading this article and like this as a goal for KDev, I just would also like to suggest that you keep new to KDE developers in mind with KD4. \n\nI've been coding for a long time but it's been ages since i wrote C++ and I've never previously written KDE/Qt code. I know that CMake is coming and that it's files should be easier for humans to parse, but going by my KDE3 experience so far, learning the KDE build system and dealing with the MOC and UI files and generally coming to grips with build process has been tricky. I'd love to see KDevelop, as much as possible/practical, be as educational as possible for new KDE / Qt developers, obviously with the ability to still be as stream lined and uninvasive for experienced developers as well. \n\nOne idea for this type of thing could be an (optionally) more verbose project creation wizard that not only lets you set up all the starting parameters for a project but seeks to explain what you should be thinking about when setting a given parameter and how it relates to the overall KDE build process etc.\n\nI guess overall, the easier it is for people such as myself to pick up KDE/Qt development the quicker we can get to actually helping out rather than just writing trivial apps as a learning exercise."
    author: "borker"
  - subject: "Re: don't you forget about me (la la la....)"
    date: 2006-08-04
    body: "I don't think the two are mutually exclusive.  By in large, core KDE developers just want something that works too.\n\nMattr is already working on CMake support in our project manager.  Hopefully you'll have to edit by hand as little as possible, but if you do, it should be easier to grok.\n\nWe are really trying to concentrate on simplifying things.  For instance, in current KDevelop 3.x we have 4 or 5 different places where you can set environment variables:\n\n1. The Make configuration.\n2. The Configure configuration.\n3. The Run configuration.\n4. The Qt configuration.\n\nIn KDevelop 4, you'll have ONE place to set and see all of your environment variables.  Every sub process that KDevelop 4 launches will use these environment variable settings.  And the embedded konsole should also use them.  That's my goal at least.  We want to make things as simple and straightforward as possible, but we don't want to lose the power."
    author: "manyoso"
  - subject: "Re: don't you forget about me (la la la....)"
    date: 2006-08-04
    body: "I agree completely, I don't think the needs of the experts and those of novices are mutually exclusive at all and certainly simplify make params etc will help everyone across the experience spectrum. I guess what I'm getting at is that it would be neat if KDevelop did a bit more hand holding for those of us who need it, whilst staying out of the way of those who don't.\n\nOne of the things I've loved about picking up Qt/KDE was how quickly you can  make  non-trivial applications, but I found learning Qt/KDE was a weird reverse... rather than writing code first and then learning the complexities of the build process later, as required, I found myself needing to come up to speed with the build process first and then write code. Not being overly familiar with the GNU tool chain didn't help speed up this process either. I found it to be a frustrating blocker to actually writing code and it wasn't helped by most of the available tutorials being great on coding advice but very slim on build advice, so when I wanted to go from 'hello world' to something a bit more 'real world' I ran smack into the build system and all my momentum died completely. Anything that could help a beginner like me out in the way of teach as you go type stuff would be great.\n\nThis is all just wish list stuff of course... I love kdevelop and kde coding in general. I can't wait until I can finally start to contribute something."
    author: "borker"
  - subject: "Re: don't you forget about me (la la la....)"
    date: 2006-08-05
    body: "The same happened with me. Most of the QT/KDE tutorials totally ommit the building process and or KDevelop/QT-Designer integration..."
    author: "Pepe Corvina"
  - subject: "Re: don't you forget about me (la la la....)"
    date: 2006-08-06
    body: "Hi, i wrote this inspired by your comment:\n\nhttp://www.kdedevelopers.org/node/2224\n\nCheers!"
    author: "manyoso"
  - subject: "Re: don't you forget about me (la la la....)"
    date: 2006-08-08
    body: "Nice one, thats a handy resource :)"
    author: "borker"
  - subject: "Used KDevelop 2.X, found KDevelop 3.X too hard..."
    date: 2006-08-06
    body: "... Will KDevelop 4 be usable at all?\n\nI used KDevelop to get started with GluCat ( http://glucat.sf.net ). GluCat is essentially a C++ template library which knows nothing about GUIs, KDE or otherwise. It primarily depends on the Boost libraries. It ships with a suite of about 20 test and example programs.\n\nTo build GluCat using KDevelop 2.X, I had to do some considerable hacking on the build files: \nto reflect the directory structure of one source library directory and many test programs,\nto ensure that the deliverable was a (source-code) template library and not a program, shared library or archive,\nto allow the building of both debug and speed test versions of the test programs,\nto allow the optional use of hash_map instead of std::map, \nto change the source library dependency from MTL to Boost,\nto track changes in GNU autotools, Intel C++, gcc and Boost,\netc.\n\nSo, anyway, I have a lot of hackery in my build files, much of which KDevelop 2.X neither understood nor supported.\nAnd the files still contained *lots* of KDE-specific stuff that my project did not need nor want and which just makes them more complicated and difficult to understand and change. (The files for GluCat 0.1.9 are still this complicated).\n\nI was therefore very excited by the prospect of KDevelop 3.X. Maybe now my project structure would be supported and I could get rid of all the hacks in my build files.\n\nUnfortunately, my confidence in KDevelop 3.X was shaken by bug 78978. ( http://bugs.kde.org/show_bug.cgi?id=78978 )\nThis bug was a show stopper. All memory on my PC was consumed. The bug took more than 2 years to be fixed. In the meantime I stopped using KDevelop and went back to using Kate, Doxygen and the Cervisia view in the File Manager.\n\nAnd still, as far as I know, KDevelop does not properly support the development of a non-GUI C++ template library which includes a suite of test programs. By *properly support*, I mean facilitate development by providing exactly what is needed for the build process without lots of KDE-dependent clutter.\n\nMaybe someone in the KDevelop team could use GluCat as a test case for KDevelop 4? This might tempt me to start using it again.\n"
    author: "Paul Leopardi"
  - subject: "Re: Used KDevelop 2.X, found KDevelop 3.X too hard..."
    date: 2006-08-08
    body: "I understand you might want to do other stuff, and you'd have to learn a new app, but wouldn't contributing to KDevelop help you as well? If spending a few days on KDevelop would make your own work more efficient, wouldn't that be worth it?"
    author: "superstoned"
  - subject: "Re: Used KDevelop 2.X, found KDevelop 3.X too hard..."
    date: 2006-08-08
    body: "In short, yes, it will be more usable. I'll add glucat to my list of projects to test with to allow me to see how it works with other points of view."
    author: "Matt Rogers"
  - subject: "version control integration"
    date: 2006-08-09
    body: "I would love to see version control integration done in the same way as M$ Visual Studio and Visual SourceSafe do it.\n\nFrom the file view you can check out/in files and editor even asks whether to check out the file if it was not checked out and you start editing it.\n\nAnd this behaviour should be independent of the versioning system in use. So cvs, svn and others would all share the same common way of checking in/out easily. (Of course there are other tasks specific to each vcs, but all share this check out/in mechanism and that is the most commonly used action anyway)\n\nAlso quick way to diff the local version to the latest version should be easily accessible."
    author: "tommi uimonen"
  - subject: "Re: version control integration"
    date: 2006-08-09
    body: "> not checked out and you start editing it.\n\n\nEDIT:\n\nnot checked out when you start editing it."
    author: "tommi uimonen"
---
<a href="http://www.kdevelop.org">KDevelop</a> is the premier Free integrated development environment.  The project is currently working towards KDevelop 3.4 with a bunch of new features and a major new version KDevelop 4.  To find out what's coming up in one of KDE's most important projects KDE Dot News spoke to three of the authors about their current work and future plans.













<!--break-->
<div style="text-align: center; float: right; border: thin solid grey; margin: 1ex; padding: 1ex">
<img src="http://static.kdenews.org/jr/kdevelop-mattr.jpg" width="148" height="207" /><br />
Matt Rogers<br />
<img src="http://static.kdenews.org/jr/kdevelop-adam-treat.jpg" width="148" height="194" /><br />
Adam Treat<br />
<img src="http://static.kdenews.org/jr/kdevelop-alex.jpg" width="200" height="144" /><br />
Alexander Dymo
</div>

<p><strong>Please introduce yourself and your role in KDevelop.</strong></p>
 
<p><strong>Matt:</strong> Hi, I'm Matt Rogers and I'm KDevelop's lead maintainer.</p>
 
<p><strong>Adam:</strong> Hi, I'm Adam Treat and I hack on many parts of KDevelop. For the 3.x series, I've worked on the C++ language part, code completion, split view of header/source and various parts of the shell and library. For KDevelop 4, I've been hacking on the C++/Java language parts, the background parser, the codemodel, the documentview, the codeview, the shell, refactoring the lib into kdevplatform and various parts of the GUI.</p>
 
<p><strong>Alex:</strong> My name is Alexander Dymo. I've been a KDevelop developer since 2002 and maintainer and release coordinator of KDevelop 3.x series starting from 3.1. I have worked on many parts of KDevelop and authored the documentation plugin. Also I initially added the "Platform" word to KDevelop which makes it possible to have KDevAssistant and Kuanta running on the same codebase as KDevelop.</p>
 
<p>Currently I'm helping Matt in maintainership and working on KDevelop 4 user interface. Also I'm sponsored to work on new Ruby language support.</p>
 
<p><strong>What's going to be new in KDevelop 3.4 and when will it be released?</strong></p>
 
<p><strong>Matt:</strong> There's going to be some support for Qt 4 based projects, as well as some nice improvements to the C++ code completion system. The code completion is faster, less crashy, and supports more language features. We don't know exactly when the release is going to be yet, since we'd like for it to stabilise a bit more and allow time for the translators to translate things.</p>
 
<p><strong>Adam:</strong> I'm concentrating pretty heavily on KDevelop 4 right now, but I know that Andreas Pakulat, Vladimir Prus, Alexander Dymo, Dave Nolden and others have put quite a bit of work into various bits such as Qt 4/KDE 4 development support, the debugger, code completion, UI improvements and an assortment of bug fixes.</p>
 
<p><strong>Alex:</strong> Like Matt and Adam said, KDevelop 3.4 is going to be an improvement in almost all areas.</p>

<p><strong>Why is there a need for KDevelop 4?</strong></p>
 
<p><strong>Matt:</strong> There is a need because KDevelop 3.x won't run on KDE 4. :) KDevelop 4 is really the next evolution in KDevelop's lifecycle. The KDevelop 3.x codebase has served us well for a long time, and now it's time to build on what we've learnt from the KDevelop 3.x series and build on top of it. </p>
 
<p><strong>Adam:</strong> As Matt says, we need to convert the code to the Qt 4/KDE 4 platform. That is number one. However, I feel that this provides a tremendous opportunity to really improve KDevelop by an order of magnitude or more. KDevelop 3.x was itself a very big leap from the 2.x series, but it is beginning to show its age with a pile of features and plugins whose quality varies quite a bit. The result is a very powerful IDE, but one which has grown into a kind of messy soup of features that do not always integrate well with each other. </p>
 
<p>For KDevelop 4, I want to make the IDE work very well within its core competency: KDE/Qt development. This requires, amongst other things, robust and solid language parts to enable consistent and workable code completion. This is where Roberto Raggi's new C++ parser, the basis behind the Qt 3 --> Qt 4 porting tool, will serve us well. Jakob Petsovitz has been very busy creating new parsers for Java and C# based on Raggi's kdevelop-pg tool, which was itself created from the template provided by the aforementioned C++ parser. Alexander Dymo is also slated to use kdevelop-pg to create a Ruby parser. Together, this should give KDevelop 4 a powerful set of language parts. I can't wait to see the Java language part in action with Trolltech's new Qt Java bindings.</p>
 
<p><strong>Alex:</strong> We just have to hack on something cool, right ;) KDevelop4 is an excellent opportunity to break things and build them again. :)</p>
 
<p><strong>What is the current state of KDevelop 4?</strong></p>
 
<p><strong>Matt:</strong> It's pretty raw right now. There are some basic things that work. You can open a file, make some edits, use the KDE 4 Konsole, and some other stuff, but it's not really very useful for normal development at the moment.</p>
 
<p><strong>Adam:</strong> It compiles, it links, and it works for some definition of 'works'. It is very alpha, but you can already get a sense that a lot of change is involved. The new documentview and codeview parts are in a preliminary state. The new configuration framework and associated KCM dialogues are in place. Given some more improvements and Matt finally wrestling the CMakeLib into place it will really begin to take shape.</p>
 
<p><strong>Alex:</strong> It's like in the state when I first joined the team. That was the 3.0 alpha 2 time when the main feature of KDevelop was the ability to crash :) Of course I'm not serious, but I like that feeling of the application in its infancy.</p>
 
<p><strong>Are a lot of the internals of KDevelop 3 going to be changed for KDevelop 4?</strong></p>
 
<p><strong>Matt:</strong> Yes. I wouldn't call what we're doing to the internals of KDevelop 4 a rewrite, since we're not just wiping the slate clean and starting over, but we're making some really deep changes in the KDevelop architecture so that it becomes more useful for other developers who are looking to develop an IDE like application and want something to build off. We're also going through and doing a lot of code cleanups, consolidation, etc.</p>
 
<p><strong>Adam:</strong> In a word, yes. The KDevelop platform library is undergoing a huge refactoring and API cleanup. New classes have been introduced to make integration of the C++/Java/C# language parts easier. Right now, I am working on merging the library and the shell and reworking the API. The new configuration framework and project file(s) have also introduced a big change.</p>
 
<p><strong>Alex:</strong> Yes, quite a lot but the idea behind our architecture remains the same. I think that KDevelop changed more radically between version 2 and version 3.</p>
 
<div style="float: left; border: thin solid grey; margin: 1ex; padding: 1ex">
<a href="http://static.kdenews.org/jr/kdevelop4.png"><img src="http://static.kdenews.org/jr/kdevelop4-wee.png" width="300" height="210" border="0" /></a><br />
KDevelop 4 with Qt Designer Integration
</div> 
<p><strong>What cool new features do you have currently working?</strong></p>
 
<p><strong>Matt:</strong> CMake support is partially working, although I can't hit the magic shortcut and have things build yet. The new codeview is nice. Adam's done a really good job with abstracting that in such a way so that'll work with more than one language very easily.</p>
 
<p><strong>Adam:</strong> The new configuration framework allows every setting that KDevelop offers to be set on a per project basis. We've also created a dichotomy between the global project file and the local project file. The global project file will not contain machine specific paths or other local configuration settings. Instead, the local settings are saved to a file in a hidden directory. Finally project managers will be able to commit the project file to their source control repository which multiple developers can then share.</p>
 
<p>The codeview part is also working although it is buggy. It is language agnostic and relies upon the language parts to publish a codemodel and delegate that the codeview part happily displays. It makes use of Qt's QSortFilterProxyModel and the Interview framework to provide filters of both the individual translation unit's codemodel and the aggregated codemodel. For instance, you can now filter the codeview part to view only the contents of the currently focused document or the aggregated whole of the project's translation units.</p>
 
<p>Probably the coolest feature so far is Matt's seemless Qt 4 designer integration. It really blows what we had in KDevelop 3.x out of the water.</p>
 
<p><strong>What new features do you plan to include?</strong></p>
 
<p><strong>Matt:</strong> the sky's the limit here really. One of our developers is working on a teamwork mode as a Summer of Code project. There's a lot of work that's gone in to that. I have really high hopes that it'll be a feature that gets used and helps make developers more productive. I'll also start looking (eventually) at some of the workflow issues in the KDevelop 3.x series (at least for me) that keeps me from being productive as I could be and trying to eliminate them. Those issues mainly involve not being able to do a few things in the IDE itself and having to go out to a terminal to do what I want. I could rattle off a few more, but then there would be nothing left for anybody else to answer. :)</p>
 
<p><strong>Adam:</strong> Hmm, soooo much... But the major feature, I hope, is that KDevelop 4 will concentrate very heavily on getting the crucial things right. We need to concentrate on making KDevelop 4 work well with at least four languages: C++, Java, C# and Ruby. We need to concentrate on making sure stuff like code completion and refactoring are robust rather than providing every app template imaginable. Beyond that, I hope KDevelop 4 will feature major new refactoring support. The language parts should really rock... The seemless Qt4 designer integration... I'd really like to see the UI simplified to take after XCode and Qt 4 designer... The perspectives feature we have planned should blow you away...</p>
 
<p>My personal goal is to make KDevelop 4 so good that core KDE developers like Zack Rusin (Emacs guy) and Aaron Seigo (Vim guy) just have to switch. Zack and Aaron will tell you that this is not an easy goal to accomplish. I'll be very happy if KDevelop 4 becomes the release known for converting the KDE core developers into KDevelop users.</p>
 
<p><strong>Alex:</strong> There's no limit of features but this time we'll have to do our job better and not cram every single little feature in our source. My own plans include new UI with perspectives and new Ruby language support with quite a lot of cool features for Rails development.</p>
 
<p><strong>Tell us about the meeting you had last year.</strong></p>
 
<p><strong>Matt:</strong> It was the birth of KDevelop 4. I wasn't there, so I can't say much else.</p>
 
<p><strong>Adam:</strong> Didn't attend. Sorry.</p>
 
<p><strong>Alex:</strong> Some time in the past (IIRC that was during Akademy 2004 in Ludwigsburg) I promised that we (KDevelop Team) would meet in Ukraine. I kept my promise and we (me, Roberto Raggi, Harald Fernengel, Ian Geiser, Richard Dale and Jens Herden of Quanta fame) actually met and had a one-day conference and a hacking session in Kiev. The conference itself did not attract much attention from local public but during the hacking session we had a lot of fun, tequilla and vodka and of course produced a lot of code. Roberto Raggi was hacking like mad and implemented the parser generator we now use to build language parsers. Harald Fernengel was busy porting KDevelop3 to Qt4 and integrating MVC pattern into KDevelop source code. Ian Geiser and myself were working on new approach to Qt3 qmake support, Richard Dale started his work on new Ruby parser.</p>
 
 <p>It was decided in Ludwigsburg to join the efforts of KDevelop and Quanta teams and use the common source base. Therefore we also invited Quanta developers to come. Unfortunately Andras Mantia could not make it but Jens Herden actually did and his presence and patient reminders actually made us think about the needs of Quanta.</p>
 
<p><strong>What is it like developing for the moving platform that is KDE 4?</strong></p>
 
<p><strong>Matt:</strong> It's not too hard now that the kdelibs4_snapshot has gone away. That was perhaps the biggest thing. We want to move quickly to the newer technologies since we've been the ones requesting them a lot of the time so not having to worry about the snapshot is better for us.</p>
 
<p><strong>Adam:</strong> Quite enjoyable actually. It was rough towards the very beginning, but things have stabilised somewhat now. I'm quite thrilled developing with Qt 4 and all of the improvements to the underlying libraries.</p>
 
<p><strong>What is the collaborative mode you have planned?</strong></p>
 
<p><strong>Matt:</strong> The teamwork mode, as it's called, will allow developers to review a set of patches in real time. Collaborative editing on one or more files is also planned, but I don't know when that will be done. Currently, patch review is commonly done on a mailing list, and while this method works, it can take a long time to get all the changes a patch makes discussed, reviewed, approved, etc. The teamwork mode will allow a group of developers to do a patch review in near real-time, making that process much faster than it currently is. </p>
 
<p><strong>What improvements or changes are being made to buildsystem support?</strong></p>
 
<p><strong>Matt:</strong> There is going to be a tonne of stuff. Autotools and CMake will be supported in the beginning, and the goal is to allow the developer to do everything from within the IDE. Ease of use is key here. With KDevelop 3.x, if you want to add a check for a package using the Autotools, you've got to edit the configure.in.in file, know all the syntax, etc. With KDevelop 4, my goal is to allow the user to right click on the project, choose 'Add an external dependency' and basically create the config check from a nice dialogue rather than having to edit files.</p>
 
<p><strong>Adam:</strong> The project manager part is going to provide a GUI abstraction of the underlying buildsystem. At least, that is the plan. You will no longer see a visual difference between using the Automake, QMake, and CMake build systems.</p>
 
 <p>I'd also like to take this opportunity to ask for some help. We are looking for a good Windows hacker to help us provide general testing of the KDevelop 4 build on Windows and perhaps an MSBuild target. Similarly, if we could find one or more Apple hackers who want to see KDevelop 4 work well on the Mac, it would be a real plus. If you are interested in helping, please get in contact with us via the kdevelop-devel list or on freenode: channel #kdevelop.</p>
 
<p><strong>What new languages are being added?</strong></p>
 
<p><strong>Matt:</strong> Adam Treat and Jakob Petsovits are working on Java and C# support.</p>
 
<p><strong>Adam:</strong> Currently C++, Java and C#, but Alexander and Richard are slated to give us a kickass Ruby language part too AFAIK. The Java and C# parts are not producing a codemodel at this time, but Jakob, Hamish and I are working on it.</p>
 
<p><strong>Alex:</strong> kdevelop-pg parser generator that Roberto Raggi wrote allows us to push our language support to their limits. With kdevelop-pg we have AST creation with the parser for free, AST walker class for free. Today thanks to Jakob Petsovits we also have backtracking and error recovery for our parsers. That all enables us to implement better language parsers and integrate them into KDevelop faster than we could have done before.</p>
 
<p><strong>What is the new ideal library?</strong></p>
 
<p><strong>Matt:</strong> That's Alexander's area, although I will take a moment and plug the Eclipse-like perspectives we're going to be adding to KDevelop 4.</p>
 
<p><strong>Alex:</strong> Starting from KDevelop 3 alpha4 it was decided to use the KMDI library to serve its GUI needs. And that was our worst decision in my mind. KMDI enabled us to have four UI modes so KDevelop could look like Borland Delphi, MS Visual Studion and IntelliJ IDEA at the same time. But KMDI also threw us back to the stone age of programming mostly because of high complexity and low maintainability of the library code. In reality the majority of our users were picking up either toplevel (Delphi-alike) or ideal (IntelliJ IDEA-alike) mode. Therefore I started to work on new UI library implementation which was called Ideal. The design goals were simplicity, code readability and maintainability. Some parts of the new library were ready for KDevelop 3.3 and KDevelop 3.3 users could enjoy additional "Simplified Ideal" mode. The improved version of the mode is now the default for KDevelop3.4. I continued the work for KDevelop 4 and now it's under heavy development. New version has new features under the hood.</p>

<p>Already working is the support for areas (read Eclipse-like perspectives). And also most cool features from former toplevel mode are being integrated into the library so that our multihead users are going to be happy with KDevelop 4 too.</p>
 











