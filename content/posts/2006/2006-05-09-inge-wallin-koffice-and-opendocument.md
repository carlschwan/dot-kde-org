---
title: "Inge Wallin on KOffice and OpenDocument"
date:    2006-05-09
authors:
  - "acid"
slug:    inge-wallin-koffice-and-opendocument
comments:
  - subject: "Thanks"
    date: 2006-05-09
    body: "Thank you Albert for putting this up on the dot.  I intended to do it myself, but I have had a very straining weekend with sick kids and not much network availability.  \n\nRegarding the interview, it would be nice to hear peoples opinion about my \"konquer the world with KOffice\" views..."
    author: "Inge Wallin"
  - subject: "Re: Thanks"
    date: 2006-05-09
    body: "> Another is print preview where we use the KDE application kpdf.\n\nYou're not quite correct here. The kdeprint preview application is used, but it is KGhostview by default."
    author: "Carlo"
  - subject: "Re: Thanks"
    date: 2006-05-09
    body: "But it will be oKular?"
    author: "Andy"
  - subject: "Re: Thanks"
    date: 2006-05-09
    body: "yes, in KDE 4. kghostview and kpdf (and a third one if i'm not mistaken) will be removed."
    author: "superstoned"
  - subject: "Re: Thanks"
    date: 2006-05-09
    body: "kdvi?"
    author: "MK"
  - subject: "Nice interview"
    date: 2006-05-09
    body: "Nice interview Inge. You made some very valid points.\n\nNow that ODF is an ISO standard, I'm sure we will get a some point a import / export MsOffice / ODF Open Source library. Maybe from the ConsortiumInfo people as a subproduct of their MsOffice plugin.\n\nThe day this library works and is re-used in KOffice, the userbase will grow."
    author: "Charles de Miramon"
  - subject: "Re: Nice interview"
    date: 2006-05-09
    body: "I came around the plugin OpenOpenOffice from phase n which (will) allow Microsoft Office to read/write in the format OpenDocument. It basically talks to either a publicy available (for simplicity for the user) or an intranet server (when privacy matters) with a running instance of OpenOffice which converts the file and send it back to the plugin in the right format.\n\nSee http://o3.phase-n.com\n\nI don't know it Koffice could do it (I'm not a programmer), but if yes, it could be a useful workaround of KOffice's bad support for the bad .DOC formats, no ?"
    author: "jmfayard"
  - subject: "Re: Nice interview"
    date: 2006-05-11
    body: "It could be, and using KIO it would be easy to integrate it, I think. The KIO slave could come in place of the C# /.Net component they plan to build for Microsoft Office. "
    author: "Andre Somers"
  - subject: "Re: Nice interview"
    date: 2006-05-09
    body: "Hey, I didn't think of that, but I bet you are right.  This should be a cross-desktop, cross-OS, cross-office suite project.  If Gary Edwards' MS Office ODF filter turns out to be open source, then we could use that as a base for a stand-alone ODF/MSO filter."
    author: "Inge Wallin"
  - subject: "I share your opinion"
    date: 2006-05-09
    body: "Yeah, I think too that KOffice is the *future* in the world of office suites. The advantages that come from the use of great technologies such as Qt and kdelibs will be more and more evident as the time goes on :)\n\nBtw, thanks for your work on koffice :)"
    author: "Dario Massarin"
  - subject: "KOffice"
    date: 2006-05-09
    body: "Does KOffice scale as a development project?\n\nI mean, look at Koffice 1.4 which was a kind of technology preview. Thinks are getting better but move slowly slowly."
    author: "Hup"
  - subject: "Re: KOffice"
    date: 2006-05-09
    body: "As a developer, I've rather got the impression that things move very quickly. The difference between 1.4 and 1.5 is enormous."
    author: "Boudewijn Rempt"
  - subject: "Re: KOffice"
    date: 2006-05-09
    body: "No, 1.4 was not a technology preview.  However it included Kexi as a tech preview, and Kexi is now considered stable and usable.\n\nIn 1.5, you have KPlato (the K Planning Tool - a project management application) as a technology preview, which will be fully operational in 1.6 or 2.0 at the latest.\n\nIf you want a measure of development speed, look at <a href=\"http://www.koffice.org/announcements/announce-1.5.php\">the announcment of KOffice 1.5</a>, and tell me again what speed it's developing.  Remember also that even though the great number of new features, the 1.5 release was short of features that it could have had if the OpenDocument support hadn't taken so much energy.\n\nThe development of the next major version, 2.0, will include porting all of KOffice to Qt4 which is another big time and energy sink.  However, after <b>that</b>, there are no more stumbling blocks, and feature development will race full speed ahead.\n\nMark my words:  KOffice is the office suite of the future."
    author: "Inge Wallin"
  - subject: "Re: KOffice"
    date: 2006-05-09
    body: "i knew that the first time i used it, and compared it to OO.o - really, koffice did have its flaws, and it still does, but it surely has the future. a good framework beats lots of money ;-)"
    author: "superstoned"
  - subject: "Re: KOffice"
    date: 2006-05-10
    body: "Well i think Koffices (mainly kword) strength, is its simplicity and ease of use. Hopefully developers will concentrate on doing few things very well and dump tech previews ;-)\nBTW congrats with kwords use of odf - so far it works rather well for me."
    author: "josel"
  - subject: "Re: KOffice"
    date: 2006-05-09
    body: "The projects contributer count grew quite a lot, but not for KWord.  That and the fact that I have been travelling for 3 months means that its growth is a bit disproportionate to the rest.\nSo, if you look just at KWord you'll notice its slow, but steady.  And probably in no comparison to KWord 2.0 ;)\n\nNaturally, KOffice could always have more attention from new people!"
    author: "Thomas Zander"
---
<a href="http://www.consortiuminfo.org/">ConsortiumInfo.org</a> is carrying <a href="http://www.consortiuminfo.org/standardsblog/article.php?story=20060505081533186">an interesting interview</a> about <a href="http://www.koffice.org">KOffice</a> called <i>"The Evolving ODF Environment: Spotlight on KOffice"</i>. In this interview, Inge elaborates on ODF support, KOffice vs OOo and KOffice future developments. The OpenDocument format, recently defined as an ISO standard, is the new default format in <a href="http://dot.kde.org/1144780418/">KOffice 1.5</a>.






<!--break-->
