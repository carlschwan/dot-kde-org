---
title: "Jes Hall on Linux Link Tech Show"
date:    2006-03-09
authors:
  - "jriddell"
slug:    jes-hall-linux-link-tech-show
comments:
  - subject: "Jes' voice"
    date: 2006-03-09
    body: "Okay, this might sound a bit weird, but does anybody else think that Jes sounds a lot like Kaylee of Firefly/Serenity?"
    author: "PC Squad"
  - subject: "Re: Jes' voice, I don't like her picture"
    date: 2006-03-09
    body: "I am not not sure about the voice, but I don't like the picture given in the interview, I prefer the first I have seen, make a search in google image, looks more natural.\n\nNB: sorry for my silly english"
    author: "morphado"
  - subject: "what happened with 'this month in SVN'?"
    date: 2006-03-09
    body: "Nice interview, but it was quite long, didn't follow it to the end. I also had to max out the volume of my speakerset to be able to hear it well, apparently the show is recorded at a very low volume?\n\nWhat I wonder about is why don't hear much from Jess Hall anymore? Haven't seen any new weblog posts since november 2005, and we haven't seen the 'this month in SVN' posts for some time either. That's a pity, I really appreciated 'this month in SVN'. Doesn't Jess Hall have the time to write it anymore?"
    author: "F for Fragging"
  - subject: "Re: what happened with 'this month in SVN'?"
    date: 2006-03-09
    body: "As I'm a key addict, I ask myself this question as well.\n\nI'm pretty sure that the involves the current stage of KDE4 development.  A bit premature right now, unless you want 4 straight months discussing lib changes, complete with screen dumps of her konsole panel. \n\nLike any good TV show, I'm sure you'll see new episodes starting in the summer/fall."
    author: "Wade Olson"
  - subject: "Re: what happened with 'this month in SVN'?"
    date: 2006-03-09
    body: "> I'm pretty sure that the involves the current stage of KDE4 development. A bit premature right now, unless you want 4 straight months discussing lib changes, complete with screen dumps of her konsole panel.\n\nYou know, I'd like that ;-).\n\nWell, basically, there's still a need for a \"KDE Traffic\" / \"SVN Digest\". "
    author: "Eike Hein"
  - subject: "Re: what happened with 'this month in SVN'?"
    date: 2006-03-09
    body: "I think the reason is current stage of KDE development:\n\n- in 3.5 branch this is mostly bugfixes, nothing exciting\n- in trunk it is mostly very low level work, I suspect 90% of readers wouldn't understant it\n\nThings should change in 3-4 month - I hope :)"
    author: "m."
  - subject: "Re: what happened with 'this month in SVN'? "
    date: 2006-03-09
    body: "That's exactly it. I have plenty of stuff to write about KOffice and Kopete, but most of the rest of KDE progress I either haven't had the time to research, or it's stuff that's fairly dull from an end user perspective - making KDE work under Qt4. When there is more to write about I'll start writing it again :)\n"
    author: "canllaith"
  - subject: "test"
    date: 2006-03-16
    body: "this is a test, I repeat this is only a test of my linux akregator software. you may now return to your daily mail cheking routines, thank you."
    author: "k1dn00b"
  - subject: "learn about KDE before you do an interview !! "
    date: 2006-03-17
    body: "learn about KDE before you do an interview !! \n\nyou made yourself a TOTAL fool by not being able to some basic KDE questions.\n\nYou even weren't able to answer about questions about your digital cam, which seems to be your \"hobby\"\n\n*sigh*\n\nGo home n00b! Use Microsoft or Mac OS X or something ..."
    author: "TranceDude"
---
Following last week's <a href="http://dot.kde.org/1141257826/">interview with Zack Rusin</a> the <a href="http://tllts.info/">Linux Link Tech Show</a> will be interviewing <a href="http://people.kde.nl/jes.html">Jes Hall</a>.  The interview starts at 21:00EST Wednesday which is 02:00UTC Thursday.  She will be discussing KDE 4, Plasma, her work on KDE documentation and photography.  <strong>Update:</strong> <a href="http://www.tllts.info/dl.php?episode=126">Episode 126</a> now available for download, Jes starts 42 minutes in.





<!--break-->
