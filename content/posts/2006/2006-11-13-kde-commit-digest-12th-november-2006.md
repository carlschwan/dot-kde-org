---
title: "KDE Commit-Digest for 12th November 2006"
date:    2006-11-13
authors:
  - "dallen"
slug:    kde-commit-digest-12th-november-2006
comments:
  - subject: "KDE 4 apps on Windows and Mac OS X"
    date: 2006-11-13
    body: "For those who don't know this yet: \n\nhttp://people.mandriva.com/~lmontel/screenshot-kde4/images.html\n\nThanks Laurent Montel for the screenshots and also all those KDE Windows team!\n\nAlso thanks Danny for providing weekly digest!\n\n"
    author: "fred"
  - subject: "Re: KDE 4 apps on Windows and Mac OS X"
    date: 2006-11-13
    body: "Why do the developers want to port KDE apps to Windows or Mac?  I'm just curious because my gut feeling is that it weakens incentives to move to a free OS.  Which I feel weakens the free software movement.  It seems to me that we should use KDE and its apps as an advertisement to show off just how great software can be when everyone pitches in where they and when they can.  I could be totally off base, and everyone is entitled to scratch whatever itch they may have.\n\nKeep up the great work."
    author: "asdf"
  - subject: "Re: KDE 4 apps on Windows and Mac OS X"
    date: 2006-11-13
    body: "That's easy, there are enough people that e.g. must work with MS on their\ncorperate computer/notebook.  I would love that have kontact running next to \nMS word and powerpoint.  And no, vwware crossover and the like are not the \nanswer for.  It's most likely going to be easier to install a kde app than\npartioning the notebook and have linux installed.  The latter is not allowed\nby many corperate helpdesks anyway.\n\nThis is one reason.  The other reason is that it makes it possible to let\npeople become used to kde applications one at a time.  Once they are used\nto kde apps, migrating to linux will be a breeze.\n\nHaving kde apps run in MS natively is definately the way to go!\n\n"
    author: "Richard"
  - subject: "Re: KDE 4 apps on Windows and Mac OS X"
    date: 2006-11-13
    body: "Development wise it's more attractive to write code that can run on multiple operating systems as well. So in theory it should/will attract more developers to the KDE project."
    author: "Danny Brain"
  - subject: "Re: KDE 4 apps on Windows and Mac OS X"
    date: 2006-11-13
    body: "and finally dev-cpp (gcc-based ide for windows written in delphi itself) will be replaced by kdevelop"
    author: "anonymous-from-linux.org.ru"
  - subject: "Re: KDE 4 apps on Windows and Mac OS X"
    date: 2006-11-13
    body: "Oh yeah!\nBut I hope they find some way to hide the automake/autoconfigure scripts, because windows users aren't used to it and probally will run in fear if they see it :)"
    author: "Iuri Fiedoruk"
  - subject: "Re: KDE 4 apps on Windows and Mac OS X"
    date: 2006-11-13
    body: "Ahh but KDE 4 uses CMake.  :)"
    author: "am"
  - subject: "Re: KDE 4 apps on Windows and Mac OS X"
    date: 2006-11-13
    body: "KDevelop 4 will support both Automake and CMake with a graphical interface.\nMy guess is that everyone will like CMake better :-D"
    author: "Jakob Petsovits"
  - subject: "Re: KDE 4 apps on Windows and Mac OS X"
    date: 2006-11-13
    body: "But there is already a better alternative to dev-cpp for windows: codeblocks http://www.codeblocks.org/\nAnd that is also available for linux!"
    author: "panzi"
  - subject: "Re: KDE 4 apps on Windows and Mac OS X"
    date: 2006-11-15
    body: "Thanks for the info, I'll take a look on it."
    author: "Iuri Fiedoruk"
  - subject: "Re: KDE 4 apps on Windows and Mac OS X"
    date: 2006-11-13
    body: "Being dependent on an operating system and its applications is more involved then you obviously think. People can not just stop using Windows and start using Linux AND stop using all the apps they are used to and start using new ones. Stopping smoking cold turkey is also very hard and why there are nicotine patches for those that want to do so gradually.\n\nBeing able to switch to KPDF on Windows instead of Acrobat, and using KOffice on Windows instead of MSOffice makes it easier to one day choose some unix and KDE since they are cheaper and better than their current OS, and the software they already use even works better on it."
    author: "Thomas Zander"
  - subject: "OT: Nicotine Addiction"
    date: 2006-11-13
    body: "> Stopping smoking cold turkey is also very hard and \n> why there are nicotine patches for those that want to do so gradually.\n\nAs a matter of fact it's been quite extensively confirmed that stopping smoking cold turkey is easier than using nicotine patches. Besides, the data seems to suggest that only those smokers that have never developed an addiction to nicotine can quit while using the patches. In other words, the patches reinforce the addiction and make it impossible to quit for nicotine addicts.\n\nSee this for more details:\nhttp://whyquit.com/\nhttp://whyquit.com/joel/Joel_03_02_cold_turkey.html\n\n(This post is really off-topic, but given the dangers within the opinion you're perpetuating, I had to...)\n \n"
    author: "Gato"
  - subject: "Re: OT: Nicotine Addiction"
    date: 2006-11-13
    body: "Yeah, I also quit using this site, so really kudos to whyquit.com\n\nAnd I also did it cold turkey, and after 2 weeks it really wasn't that hard anymore, but most friends who try to stop by decreasing their cigarettes/day came back to their old amount of cigs very quickly."
    author: "Martin Stubenschrott"
  - subject: "Re: OT: Nicotine Addiction"
    date: 2006-11-16
    body: "Not everyone is so strong willed as you apparently are.\nI've heard many people that stop cold turkey and then 1 or 2 months later there is a party and they just <b>have</b> to light one up. Just because that person doesn't have the craving for the drug anymore doesn't mean that person is able to say 'no'.\nIts not only about the drug addiction, its about learning to change your habits so you don't feel like you are in detox for the rest of your life.\n\nAnyway, this is indeed of topic.\nMy earlier post was only based on the fact that stopping cold turkey is indeed 2 weeks of living hell.  At any such point people will be able to stop and get back to their old ways of doing things.\nIts the same with switching to Linux, if you have to replace 100% of the apps you used then its not going to be fun until you mastered their replacements.\nPeople who tried this and failed will most probably not try again any time soon."
    author: "Thomas Zander"
  - subject: "Re: KDE 4 apps on Windows and Mac OS X"
    date: 2006-11-13
    body: "while i see no point to using KPDF on windows (there are already just fine pdf viewers there) i can with KOffice due to the ODF."
    author: "Aaron J. Seigo"
  - subject: "Re: KDE 4 apps on Windows and Mac OS X"
    date: 2006-11-14
    body: "There are?  Please enlighten me!  Acrobat is big and sluggish, and the rest are quite clunky to use (foxit, sumatra). KPDF rocks.  I wish I could run it on Windows."
    author: "Leo S"
  - subject: "Re: KDE 4 apps on Windows and Mac OS X"
    date: 2006-11-14
    body: "i wasn't meaning to say any particular pdf reader for windows was better or on par with kpdf in any particular way (or vice versa). a lot of kde software is simply better than what most people use on a day to day basis. \n\nthe question is whether there is a strategic benefit to having okular on windows. there certainly is a strategic benefit to koffice and open office (ODF support), but what is the benefit for okular on windows? the benefit i can see is that some users will enjoy using it on windows.\n\nwhat is the cost to the okular team?\nwhat is the cost to other KDE projects, particularly the desktop workspace?\nwhat is the cost to other Free software projects?\n\nthese questions are a non-trivial one to answer. i won't even attempt to do so here, but it's one that i feel most people don't ever ask themselves. it's all \"i want, i want!\" without much thought to the consequences of fulfilling that desire."
    author: "Aaron J. Seigo"
  - subject: "Re: KDE 4 apps on Windows and Mac OS X"
    date: 2006-11-15
    body: "The situaton is even more complicate. People are used to apps and work flows on Windows that are so stone-age, buggy and slow that it isn't funny anymore. If you tell them \"Hey you deserve something better than pain and frustration\" they panic because I don't give them the answers they want to get:\n\nThey ask:\n\"How do I use Norton Internet Security? It makes the internet so secure if I have it. On a friends computer it was able to block thousands of attacks and it did find all viruses.\"\nI tell them:\n\"Forget about antivirus, antispyware and personal firewall software. They don't solve your computer problems they make them even more worse. There are easy to use systems that inmediatly release you from that burden knowing about such things.\"\n\nThey ask:\n\"Hey where do I get the crack/serial for $software? Do you have $software-professional-version? My copied installation CD has a copy protection and does not work.\"\nI tell them:\n\"Why do you care about serials/cracks and installation medias with copy protection? Why do you ask for just one single programm? There are systems that have eveything ready for you just a mouse click away. And you don't need to know about serials/cracks. And of course you can update the whole system including every app with a single mouse click or even automatically. No need to separately look for updates for every app you have.\"\n\nI could go ahead...\n\nWindows people have accepted so much horrible things as an inchangeable law of nature/god that you can pull them completely off the track if you answer such things. As none wants to realize that he wasted a lot of his time with unnecessary thing they don't believe you most of the time but believe the opposite in order to feel comfortable just for the moment. So they think \"Linux is a confusing complicated system for evil virus programmers that is not useable by honest people because it even lacks Norton Internet Security!\" I'm not kidding. I have heard this more than once.\n\nThe problem is that Linux is so much ahead of Windows that you cannot gradually switch to it as you need to change some fundamental things in your painfully trained useless habits.\n\nLinux apps on Windows won't help attracting more users to Linux because Windows inherits and enforces a totally different mentality. Linux apps on Windows will help average people to lower their pain but not removing their pain. As removing these pain means looking into the mirror of awareness people won't do it if they just can stand the pain or can lower them quick without awareness.\n\nI am not talking about people that already did look into the mirror and that appreciate Linux but are forced to use Windows. For these people Linux apps on Windows are a great help but you don't need to convince convinced people but not yet convinced people if you want to increase market share of Linux desktops.\n"
    author: "Arnomane"
  - subject: "Re: KDE 4 apps on Windows and Mac OS X"
    date: 2006-11-15
    body: ">> the benefit i can see is that some users will enjoy using it on windows.\n\nOne could argue that is the major motivator for open source software in general.  Someone will enjoy using it.\n\n>> what is the cost to the okular team?\n\nHopefully nothing.  Of course, developers that are not interested in Windows wouldn't be doing the work to port it.  Only if there is a developer interested in a windows version would it even get done in the first place.\n\n>> what is the cost to other KDE projects, particularly the desktop workspace?\nwhat is the cost to other Free software projects?\n\nValid points, but equally valid is the question of what is the _benefit_ to each of those groups?  Cross platform apps have a potential for more users, more bug reporters, more potential developers.  Same with the switching issue, there might be some people that don't bother switching to Linux, since they can use KDE apps on Windows as well, but for all we know there are just as many that will use this to gradually switch to free software, and then switch OS as a last step."
    author: "Leo S"
  - subject: "Re: KDE 4 apps on Windows and Mac OS X"
    date: 2008-01-25
    body: "Adobe's PDF reader is buggy. It randomly stops printing and emailing PDFs and doesn't exactly allow much customization or preference setting. I have a whole office of users running Windows because the ERP software demands it, calling me out because they can't print reports that are downloaded as PDF files. Every time, I tell them to log out and log in and do it again, it works. Adobe Reader is buggy.\n\n... and a 30MB installer.\n\nBring Okular to Windows!\n\nOn another note - imagine if a Linux-savvy admin could replace a user's programs one by one. Out goes I.E., in comes Firefox. Out goes Adobe Reader, in comes Okular. Out goes Outlook, in comes practically anything else. Out goes Office, in comes OpenOffice.org. Out goes Winamp, in comes Amarok. Finally, out goes Windows, in comes Kubuntu - with Firefox, Okular and OpenOffice. Instead of one big jump, the user would be presented with several small adjustments to their workflow. Their software doesn't change when they move from Windows to Linux. This is the route to happy users.\n\nBring Okular to Windows, please."
    author: "Stephen Martindale"
  - subject: "Re: KDE 4 apps on Windows and Mac OS X"
    date: 2008-09-08
    body: "I HATE Acrobat with a passion ! Where I work PDF files are flung across the network all day, everyday. Its a constant thing. Acrobat does not help me get the job done, it gets in my way ! It pops up with that distiller error and yes it has been upgraded, re-installed and what not. Ever tried printing something with bar codes in it with Acrobat ? >:@ Ever tried combining documents what weren't saved with Acrobat ? >:@\n\nWhen I do reports that have to be sent to two company directors at the end of every week, they expect to get a PDF with annotations, the last thing I want or need is Acrobat stuffing me around. Its not just the bugs, the false positives (deciding I'm a pirate and refusing to work >:@), and the general user-unfriendliness (how long did it take me to figure out how to create a yellow text box with black borders and write some text in it in Acrobat 8 simply because I wasn't familiar with the keyboard shortcuts and Adobe couldn't be bothered with decently featured, accessible/discoverable default toolbars ???? Don't even ask... >>>:@). Usability with Acrobat is a joke.\n\nWhatever you do don't try and be like Acrobat - Simply offer the most commonly used features in a nice, easily discoverable way. THATS a winner.\n\nMost of the time all I want to do is put a red line through some text, or highlight something, or create a text box to write in... I don't need Acrobat's better part of 1 gigabyte install for what I need it to do.\n\nOkular looks very promising to me. *cue the angelic singing and the fragrance of Vanilla* (hey, I like vanilla)."
    author: "Euraran"
  - subject: "Re: KDE 4 apps on Windows and Mac OS X"
    date: 2008-09-08
    body: "I HATE Acrobat with a passion ! Where I work PDF files are flung across the network all day, everyday. Its a constant thing. Acrobat does not help me get the job done, it gets in my way ! It pops up with that distiller error and yes it has been upgraded, re-installed and what not. Ever tried printing something with bar codes in it with Acrobat ? >:@ Ever tried combining documents what weren't saved with Acrobat ? >:@\n\nWhen I do reports that have to be sent to two company directors at the end of every week, they expect to get a PDF with annotations, the last thing I want or need is Acrobat stuffing me around. Its not just the bugs, the false positives (deciding I'm a pirate and refusing to work >:@), and the general user-unfriendliness (how long did it take me to figure out how to create a yellow text box with black borders and write some text in it in Acrobat 8 simply because I wasn't familiar with the keyboard shortcuts and Adobe couldn't be bothered with decently featured, accessible/discoverable default toolbars ???? Don't even ask... >>>:@). Usability with Acrobat is a joke.\n\nWhatever you do don't try and be like Acrobat - Simply offer the most commonly used features in a nice, easily discoverable way. THATS a winner.\n\nMost of the time all I want to do is put a red line through some text, or highlight something, or create a text box to write in... I don't need Acrobat's better part of 1 gigabyte install for what I need it to do.\n\nOkular looks very promising to me. *cue the angelic singing and the fragrance of Vanilla* (hey, I like vanilla)."
    author: "Euraran"
  - subject: "Re: KDE 4 apps on Windows and Mac OS X"
    date: 2006-11-13
    body: "During work I have to work on Windows. I would love to use Quanta, Kate or KOffice instead of all the MS apps.\nIt would also be nice advertisement.\n\nI'm also a software developer and from my perspective I would like to see my software used as much as possible. So if there is a chance to get it to run on additional OS's with no or just a bit of additional work I would love to do it."
    author: "Norbert"
  - subject: "Re: KDE 4 apps on Windows and Mac OS X"
    date: 2006-11-13
    body: "Being non present on Windows and Mac is hurting KDE. No name recognition, limited pool of users and their problems."
    author: "m."
  - subject: "Re: KDE 4 apps on Windows and Mac OS X"
    date: 2006-11-13
    body: "Jep, I don't get this eather. I would cladly switch to Windows if it had Amarok, Kmail and Konqueror. Those (and Emacs) are the apps why I installed Linux on my workplace."
    author: "as"
  - subject: "Re: KDE 4 apps on Windows and Mac OS X"
    date: 2006-11-14
    body: "What makes Windows so much more attractive for you?"
    author: "Leo S"
  - subject: "Re: KDE 4 apps on Windows and Mac OS X"
    date: 2006-11-15
    body: "more games and applications and hardware vendor support... i still wouldn't run it because i prefer a world with Free Software, but i understand him very well... as a Desktop Environment, Windows sucks, imho. but many ppl rather work with such a rubbish piece of software and be able to play their favorite games and use their favorite applications, apparently."
    author: "superstoned"
  - subject: "Re: KDE 4 apps on Windows and Mac OS X"
    date: 2006-11-13
    body: "Because those who are doing the work want to.   This is open source, he who does the work decides how it is done.\n\nFrom what I can tell the main koffice developers generally (there may be exceptions) don't care about Microsoft Windows support or Mac OSX support at all.  However there are some side contributors that care more about other OS support than the main application development.   They wouldn't bother adding new features to koffice, but they will bother to port, so we get a port.\n\nAs for those who don't care about other OSes, they care about code quality, and it is generally agreed amoung good programers that the more systems (Both OS and CPU) supported, the better the code is - just because it forces you to fix some potential nightmares that you can ignore if you limit your platform (Both maintaince problems, and also bugs that are 1 in 10,000 on one system by everytime on the other).   So the main koffice developers get better code, which they care about, and the other platform developers get another office suite to choose from.\n\nEveryone wins.    (Note that world domination is only as issue to immature people who have not discovered compition is good)"
    author: "Hank Miller"
  - subject: "Re: KDE 4 apps on Windows and Mac OS X"
    date: 2006-11-15
    body: "> it is generally agreed amoung good programers that the more systems (Both OS and CPU) supported, the better the code is\n\nSource ? Adding lotsof #ifdef win32 hardly makes the code better in practice.\n"
    author: "boing"
  - subject: "Re: KDE 4 apps on Windows and Mac OS X"
    date: 2006-11-15
    body: "Good programmers do not use #ifdef for that purpose.\n\nYes I know there is a lot of code out there that uses #ifdef for things like that.   There is however NO good code that uses #ifdef for that purpose.\n\nGood code uses (often writes) abstraction layers (a good thing) for those few areas where things are different.  The abstraction layers once written can abstract even more things.   \n\nKIO slaves are an abstraction layer on file operations.   You just write a different KIO slave for Windows file operations (if they are different, I don't know the Win32 API well enough to know).   As a bonus you can now write a KIO slave for ftp, and get transparent file operations on remote files.   (Yes I know KIO slaves were designed long before Windows compatability was dreamed of.   Expand the idea for the other areas where there are compatability issues)\n\nBesides, most of the compatability issues SHOULD be taken care of by qt anyway."
    author: "Hank Miller"
  - subject: "Re: KDE 4 apps on Windows and Mac OS X"
    date: 2006-11-15
    body: "I am developing a small RTS game with AI in SDL, and porting it to windows helped me to find a LOT of bugs that Linux just ignored - probally due to different gcc/mingw versions. Thje last gcc version also helps a lot, because dosen't allow some minor errors in code - as a void pointer being cast as a invalid object. So in my little experience, yes, porting helps the quality of the code.\n\nAnyway, anybody knows how to solve the problem of SDL include without ifdefs? (in linux SDL.h, in windows SDL/SDL.h). Thanks in advance :)"
    author: "Iuri Fiedoruk"
  - subject: "Re: KDE 4 apps on Windows and Mac OS X"
    date: 2006-11-15
    body: "Shouldn't it be SDL/SDL.h on both platforms?\n\nSounds like the include path directives are different if it isn't"
    author: "Kevin Krammer"
  - subject: "Re: KDE 4 apps on Windows and Mac OS X"
    date: 2006-11-16
    body: "On *nix you use sdl-config for the path, ie.\ngcc `sdl-config --cflags` -o somecode.o -c somecode.c\ngcc `sdl-config --libs` -o someapp somecode.o\n\nSeveral other things behave like this as well including KDE itself (kde-config). In this case the --cflags gives \"-I/usr/include/SDL -D_GNU_SOURCE=1 -D_REENTRANT\" on my system, the initial -I param changes the nature of the include statement to SDL.h not SDL/SDL.h, you can just stick with SDL/SDL.h if you want but on *NIX this can bite you on the arse later if someone installs SDL into a prefix (eg. /usr/local making the path /usr/local/include/SDL/SDL.h) and if that path is not in GCC's known include directories (/usr/local/include usually isn't) then they'll get a 'cannot find SDL/SDL.h' when they try to compile on that system. The best way to handle it is generally autoconf, it should also be able to locate the directory on Winslows as well although it will probably require a Cygwin environment to run."
    author: "AR"
  - subject: "Re: KDE 4 apps on Windows and Mac OS X"
    date: 2006-11-14
    body: "There are two reasons:\n- First much more people take notice of KDE and its programs and use them.\n- Many users mean much more feedback which is rewarding and gives programmers better focus about what is perceived as important. (Feedback is very important, because it makes programmers feel appreciated and in return they invest more time in their programs.)\n- Finally we are able to compete with the real competition - there are more windows programs for every task than most people are aware of. KDE is ready for that, so lets do it.\n\n"
    author: "Joachim"
  - subject: "Re: KDE 4 apps on Windows and Mac OS X"
    date: 2006-11-16
    body: "For me, personally, I'd love to use several\napplications under windows.\n\nFirst thing I thought was: wow! Now I can use\nKonqueror under windows!! I don't like Windows File Explorer,\ntried Directory Opus but it was *too* much, so I'm glad \nfinally it seems like I might be running Konqueror on Windows.\n\nAlso, after a moment of reflection, K3B and Amarok would be\ngreat additions too. \n\nAs long as the applications are recognizable as open source ones,\nI don't think there's much to worry about. After all, someone\nwho sets this up on Windows will already have done some research beforehand."
    author: "Darkelve"
  - subject: "Re: KDE 4 apps on Windows and Mac OS X"
    date: 2006-11-13
    body: "Thanks for the screenshots, but I *really* think they should use the qt windows theme by default when run under windows."
    author: "Martin Stubenschrott"
  - subject: "Re: KDE 4 apps on Windows and Mac OS X"
    date: 2006-11-13
    body: "ah, the best part about open source projects and being open for all to see is how people just can't wait to share every little bit of information that is being discussed. *rolls eyes*\n\nwould it be too much to ask to actually have people wait for the project to speak for itself?\n\nlook at the thread that ensued here. lots of questions, lots of 'answers'. the accuracy is mixed and the level of interest (and concern) is high. please, before going off and trying to figure out just what the crazy people of kde have in mind and in plan give us a chance to put this information together and share it with everyone ourselves.\n\ni would safely say we can expect something in december on this."
    author: "Aaron J. Seigo"
  - subject: "Re: KDE 4 apps on Windows and Mac OS X"
    date: 2006-11-15
    body: "Waiting without talking about it is no fun, see Apple trying to close gossip sites ;)"
    author: "Iuri Fiedoruk"
  - subject: "Re: KDE 4 apps on Windows and Mac OS X"
    date: 2006-11-14
    body: "Kate under Windows :-) I have been missing that!"
    author: "WinKate"
  - subject: "Re: KDE 4 apps on Windows and Mac OS X"
    date: 2007-08-19
    body: "I agree with those who say that users of ms-windows will not change to Linux because of that, but will use KDE and apps. as part of ms-windows and will thank billy boy for such great and payless apps. \nFor me the world isn't the same any more. It is going cold down my neck. So I have to think about including Linux specific resources in my apps. \nThat will improve ms-windows three times and I think it is turning around and users will go back to ms-windows because they can now play their games and use the best apps. without rebooting and changing the system.\n\nBad, bad world ... has anyone a cord ?\n\nThat KDE is running under Mac OSX is no secret (Yellow Dog dist.) and I would be proud to see my apps. running under OSX, because Apple cooperates with Linux and does many good changes to KDE- and Linux OS-code (e.g. to Konqueror for taking code for Safira).\n\nBut Linux and KDE is changing too fast, so billy boy will never be able to follow.\nIn future the response on ms-windows will be! - is that something to eat? - What does it taste?"
    author: "bastl"
  - subject: "typo"
    date: 2006-11-13
    body: "s/11/12/"
    author: "anon"
  - subject: "Re: typo"
    date: 2006-11-16
    body: "Fixed, thanks!\n\nDanny"
    author: "Danny Allen"
  - subject: "Digest"
    date: 2006-11-13
    body: "One link of 603095 is broken.\n\nhttp://websvn.kde.org/trunk/playground/base/guidance/powermanager/icons/battery-charging-070.png?r1=603094&r2=603095"
    author: "gerd"
  - subject: "Re: Digest"
    date: 2006-11-13
    body: "The whole of http://websvn.kde.org seems to be down to me...\n\nDanny"
    author: "Danny Allen"
  - subject: "Okular and Ligature"
    date: 2006-11-13
    body: "just checked them both. Ligature is seriously faster than Okular, and it's text selection works. on the other hand, Okular has the annotations and stuff (tough it's not that great yet, and slow). Okular shows nicely with a red square where you are in the document, but the thumbnail preview 'jumps' while it moves smoothly in Ligature. \n\nnow there's the darwinian stuff, and anyone should work on whatever he/she likes, but it both apps are so much alike in interface and everything, it's not funny anymore. i wonder how much code they share?\n\nI know when okular was started as a summer of code project, there was no interest in sharing code with what now is called Ligature (great name, btw). i think that was a missed opportunity. but guys, can't you talk again? those apps don't just have an overlapp (eg Krita and Gimp) but they are really really almost the same. now okular wants to get in kdegraphics, but i would say NO. sorry, but two apps, not just DOING the same, but also LOOKING the same and WORKING the same is just plain silly. one of the two can get in, and Ligature was simply first..."
    author: "superstoned"
  - subject: "Re: Okular and Ligature"
    date: 2006-11-13
    body: "btw, i just showed those apps to my girlfriend, she thought you guys needed to get laid... maybe that would help on the cooperation front?!?"
    author: "superstoned"
  - subject: "Re: Okular and Ligature"
    date: 2006-11-13
    body: "unless your girlfriend is willing to help right there, this is not a very constructive comment.\n\nI do agree the existence of both apps is very unfortunate, though."
    author: "ben"
  - subject: "Re: Okular and Ligature"
    date: 2006-11-13
    body: "well, how could she help? the developers must find a common ground, and try to help each other instead of duplicating the work. i don't know if this is possible for them, and i'd rather see two enthousastic teams working on two apps which do the same in the same way, than 2 teams fighting and not having fun working on a great app.\nbut it'd be great if things would work out, and i'd like to ask them to try again...\n\n"
    author: "superstoned"
  - subject: "Re: Okular and Ligature"
    date: 2006-11-13
    body: ">>> she thought you guys needed to get laid\n\n>> unless your girlfriend is willing to help right there\n\n> well, how could she help?\n\n\n\nLOL!"
    author: "AC"
  - subject: "Re: Okular and Ligature"
    date: 2006-11-15
    body: "Ok... that was NOT what i meant ;-)\ntough i'm sure she's willing to give em a makeover, at least she loves 'beauty and the nerd'  :D"
    author: "superstoned"
  - subject: "Re: Okular and Ligature"
    date: 2006-11-13
    body: "This is what I call \"user support\"... ;-)"
    author: "Pino Toscano"
  - subject: "Re: Okular and Ligature"
    date: 2006-11-13
    body: "> now okular wants to get in kdegraphics, but i would say NO. sorry, but two apps, \n> not just DOING the same, but also LOOKING the same and WORKING the same is just \n> plain silly.\n\nYour points are quite pointless. If we would have patched the kpdf to be okular directly in trunk, okular would _already_ be in kdegraphics.\n\n> one of the two can get in, and Ligature was simply first...\n\nROTFL. I didn't know in KDE there were the rules \"first come, first served\" and \"the first is better\".\nThanks for enlight me dude ;-)"
    author: "Pino Toscano"
  - subject: "Re: Okular and Ligature"
    date: 2006-11-13
    body: "But there is the rule \"never change a working system (unless a change makes things miles better)\". While both Okular and Ligature are great, none of them is miles better than the other, so why change?\n\nPlease do not forget, by requesting to have Okular in kdegraphics you are implicitly asking to remove Ligature. So please say this out loud and stay with the answers :) And then, there are advantages for being a member of the extragear collection! And after all distribution are free to decide which program they install by default.\n\nKDE is not about better marketing, but about better technology. So decisions should be based on the latter."
    author: "Slubs"
  - subject: "Re: Okular and Ligature"
    date: 2006-11-13
    body: "> none of them is miles better than the other\n\nhaving seen both, i would personally judge this to be an incorrect statement. i know which one of the two has historically served me better and which right now provides more interesting features: kpdf which became okular.\n\n> by requesting to have Okular in kdegraphics you are implicitly asking to\n> remove Ligature\n\nthis is a very good point. i don't see anything wrong with this either. as you said:\n\n> here are advantages for being a member of the extragear collection"
    author: "Aaron J. Seigo"
  - subject: "Re: Okular and Ligature"
    date: 2006-11-13
    body: "now KPDF always did a great job for me as well, but i tried both in trunk, and they sure look rather evenly to me. Ligature is seriously faster, and has a working text selection. and Okular has the annotation support (which doesn't work great, yet).\n\nanyway, i'd love to see the authors meet and talk about this, or something. try to find a common ground. they seem to want (almost) the same thing, as the apps DO almost the same thing in almost the same way, looking the same. it's hard to imagine there being big technical disputes which can't be solved..."
    author: "superstoned"
  - subject: "Re: Okular and Ligature"
    date: 2006-11-14
    body: "> and they sure look rather evenly to me\n\ni did a fairly extensive survey of both of them the other day and there are significant functionality gaps that remain. see my email on k-c-d about this if you're curious.\n\n> Ligature is seriously faster\n\ni haven't noticed this. in what ways did it feel faster for you?\n\n> a working text selection\n\nas does okular. i did find a bug in this feature the other day. i told pino about it so i assume it'll get fixed up here.\n\n>  i'd love to see the authors meet and talk about this\n\nagreed.\n\n> big technical disputes which can't be solved\n\nat this point i don't think it has anything to do with technical issues."
    author: "Aaron J. Seigo"
  - subject: "Re: Okular and Ligature"
    date: 2006-11-15
    body: "> > a working text selection\n\n> as does okular. i did find a bug in this feature the other day. i told pino \n> about it so i assume it'll get fixed up here.\n\nTry it with multicolumn text, and you will see that there is still quite some\ndifference.\n\nGreetings,\nWilfried Huss"
    author: "Wilfried Huss"
  - subject: "Re: Okular and Ligature"
    date: 2006-11-15
    body: "indeed. select the text in okular, watch your cpu and see it lag behind your mouse... and then try to copy the text anywhere (doesn't work, it's only selecting).\n\nbut the ability to highlight a piece of text (wich is saved over close/open) in okular is great..."
    author: "superstoned"
  - subject: "Re: Okular and Ligature"
    date: 2006-11-15
    body: ">> Ligature is seriously faster\n> i haven't noticed this. in what ways did it feel faster for you?\n\njust scroll and see your cpu usage. SLOWLY scrolling brings KPDF to full 100% cpu, and if you use autoscroll (shift-downarrow) it isn't even able to load the next page (on a 2 ghz proc!) before you're almost scrolled past it. okular does things a bit better, but start both ligature and okular, and scroll. in ligature, the cpu goes barely up, okular goes to 100% immediately.\n\nI also love the smart behavior of ligature when hitting space (one page up/down, smoothly scrolling and with a red line), in okular i love the way you see where you'r pageview is...\n\nanyway, i think KDE should probably start some kind of competition between them. Only one can get in KDE 4, and the best will win. in the end, let the TWG decide who won.\n\nif they're smart, they'll re-use as much as possible from each other's code base, giving features to ligature and speedups to okular...."
    author: "superstoned"
  - subject: "Re: Okular and Ligature"
    date: 2006-11-13
    body: "you're right, okular was Kpdf and ligature is just the new name of kviewshell. so what do you think, should we give users two apps which can do almost exactly the same? should one be removed/left out? or should the developers at least TRY to find some common ground, at least share as much as possible or something?"
    author: "superstoned"
  - subject: "Re: Okular and Ligature"
    date: 2006-11-16
    body: "I think the developers should stop making this a contest on who has the longest d* and start working together like adults.\n\nThere is room for only one of them in kde proper."
    author: "Thomas Zander"
  - subject: "Re: Okular and Ligature"
    date: 2006-11-13
    body: "You forget to consider one very important aspect of Okular, even if it was a sumer of code project it not a new project. It's a extention and continus development of the good old KPdf. It's developed under a new name since it now does more than PDFs."
    author: "Morty"
  - subject: "Re: Okular and Ligature"
    date: 2006-11-13
    body: "Not that it really matters, but Ligature/Kviewshell also predates KPDF\nby a couple of years."
    author: "Wilfried Huss"
  - subject: "Re: Okular and Ligature"
    date: 2006-11-13
    body: "> just checked them both. Ligature is seriously faster than Okular, and it's\n> text selection works. on the other hand, Okular has the annotations and stuff\n> (tough it's not that great yet, and slow). Okular shows nicely with a red\n> square where you are in the document, but the thumbnail preview 'jumps' while\n> it moves smoothly in Ligature. \n\nThank you for trying out Ligature.\n\nThere is also a version of Ligature for KDE3 which is functionally equal\nwith the KDE4 version, only more stable. We are trying to make a release\nof this version in the next 2 months. So that also KDE3 gets a decent\nmulti-format document reader.\n\nUntil then it can be found in svn at:\n\nhttp://websvn.kde.org/branches/work/kviewshell-0.7/\n\nGreetings,\nWilfried Huss (Ligature maintainer)"
    author: "Wilfried Huss"
  - subject: "Re: Okular and Ligature"
    date: 2006-11-14
    body: "Nice!  Nice to see these features come to KDE3, which will be shipped for quite a long time still."
    author: "Leo S"
  - subject: "Re: Okular and Ligature"
    date: 2006-11-14
    body: "Hello,\n\nas far as I see, there are substantial differences in Ligature and oKular when \nit comes to DVI and PDF files. I feel that these are quite important for a \nnumber of people.\n\nCorrect me if I am wrong, but it seems that oKular does not support the \nfollowing features \n\nDVI:\n- inverse and forward search, an important feature for people writing TeX \ndocuments, and an important feature of, e.g. the kile editor that uses \nkdvi/kviewshell\n\n- it seems that oKular uses old-fashioned Metafont fonts, instead of modern \nType1 or TrueType\n\n- Exporting DVI files to other formats (Text, PS, PDF, ...)\n\n- Graphic file inclusion\n\nDJVU:\n- removing/inserting pages\n\nPS:\n- removing/inserting pages\n\n- editing PDF metadata\n\n\nPDF:\n- setting of page backgrounds\n\n- removing/inserting pages\n\n- editing PDF metadata\n\n- inverse and forward search, as with DVI, for people using the TeX \ntypesetting system\n\nBest,\n\nStefan."
    author: "Stefan Kebekus"
  - subject: "Re: Okular and Ligature"
    date: 2006-11-16
    body: "Are these feature easy to port from one app to the other?"
    author: "Thomas Zander"
  - subject: "wow"
    date: 2006-11-13
    body: "on a more positive note:\nLOVE to the kdegames developers!!! great work on konquest (i love that app, and the fact it didn't scale has alwasy been an annoyance. now it's great, even tough it's not yet finished). same with kpat and kmahjongg, ksame, and the others. it's really amazing how good they look ;-)"
    author: "superstoned"
  - subject: "Re: wow"
    date: 2006-11-13
    body: "and WHY don't i get lots of comments on this? i mean, talking about ligiture and okular might be nice, but this is worth some comments. they are doing a wonderfull job, really. every few days i update my KDE 4 svn, to see what they've been doing, and things start to look better, get new functionality, get faster etc etc.\n\nagain: LOVE!\nand peace to the okular and ligiture ppl... i didn't mean to hurt anybody, both apps are a great work. too good, actually, i wouldn't want to be in the shoes of the ppl having to choose one of these. i'd rather see them work out their (must be few) differences..."
    author: "superstoned"
  - subject: "Re: wow"
    date: 2006-11-14
    body: "Thanks, THAT will give us even more energy and motivation."
    author: "Johann Ollivier Lapeyre"
  - subject: "Re: wow"
    date: 2006-11-14
    body: "Well, seems like whining and complaining is much more fun for these people than applauding the devs doing such great jobs in providing first class software to the users.\nSo: A big Thank You to all participants !"
    author: "infopipe"
  - subject: "Re: wow"
    date: 2006-11-13
    body: "Well, now it's finished. There is also a SVG background, a nicer grid...\nI switched to QGraphicsView instead of a custom QWidget, that's making the creation of effects far easier. But the commit happened too late for this SVN commit digest.\nThe switch to QGraphicsView is really important for instance, prepare for surprises in any other game switching to it. Take something simple, the SVG background. It wasn't possible with the custom QWidget... Ok, possible but hard. Now the problem is not : \"how to do\", it's \"what to do\".\nScreenshot : http://www.pinaraf.info/konquestQGV.png\n\nBTW, I'd like to add something : the hidden work, mostly done by Inge Wallin, is extremely important... It's not just \"separation of gameboard -> gamelogic, gameview\". It means that later we'll be able to have for instance multiple-leve AI players."
    author: "Pierre Ducroquet"
  - subject: "Re: wow"
    date: 2006-11-15
    body: "i must be doing something wrong, then, if i click 'end round' for the first turn, the game is finished in a few seconds and i don't get a chance to play anything at all :D\nbut the looks are finished, and it's great indeed.\n\nman, i'm looking forward to KDE 4 :D"
    author: "superstoned"
  - subject: "Re: wow"
    date: 2006-11-15
    body: ":)\n\nThe look is far to be finished, i expect to have something better at the end. You should take the screenshot as a work in progress... And if the limiting factor is the time of course (i have a real/money job for the food...), KDE4 is not for tomorrow."
    author: "Johann Ollivier Lapeyre"
  - subject: "Icons"
    date: 2006-11-13
    body: "/trunk/playground/base/guidance/powermanager:  \nNew icons thanks to Nuno and Ken of Oxygen fame, remove pid when registering power-manager with a dirty hack  \n \n\nErrrr...\nIt looks an almost exact copy of the Winamp icon.."
    author: "AC"
  - subject: "Re: Icons"
    date: 2006-11-14
    body: "And the text in the battery applet even resemble your name when power is plugged in!\nThat's funny! They're copying you!"
    author: "Davide Ferrari"
  - subject: "Re: Icons"
    date: 2006-11-14
    body: "LOL. You made my day with that joke."
    author: "Michael"
  - subject: "Re: Icons"
    date: 2006-11-14
    body: "I was there (at Dublin) when he draw it, and it was not a copy."
    author: "Johann Ollivier Lapeyre"
  - subject: "Status of Kat"
    date: 2006-11-17
    body: "I saw the program Kat listed under the buzz section of this digest, however, according to kat's wikipedia article ( http://en.wikipedia.org/wiki/Kat_Desktop_Search_Environment ), the project is dead.\n\nWhich makes me wonder, what exactly is the current status of kat?"
    author: "TH"
  - subject: "Re: Status of Kat"
    date: 2006-11-19
    body: "I haven't seen regular commits to Kat for a long time, so I am assuming that Kat is either 'dead' or 'on hold'.\n\nHappily however, there is a lot being done for Strigi, which at this point in time looks to have the best chance of becoming the de-facto search solution for KDE 4 desktops."
    author: "Robert Knight"
---
In <a href="http://commit-digest.org/issues/2006-11-12/">this week's KDE Commit-Digest</a>: KViewShell is renamed Ligature. <a href="http://okular.org/">Okular</a> gets support for Text and Line annotations. KSame and Konquest start their conversion to SVG graphics. Marble gets enhanced support for presenting and displaying geographical data interactively, and showing national flags. <a href="http://www.mailody.net/">Mailody</a>, the alternative email client, continues to develop at a rapid pace. <a href="http://telepathy.freedesktop.org/">Telepathy</a> support in <a href="http://kopete.kde.org/">Kopete</a> starts to emerge from experiment towards a usable implementation. <a href="http://kile.sourceforge.net/">Kile</a> gets scripting support, with improvements to scripting across <a href="http://www.koffice.org/">KOffice</a>. <a href="http://koffice.kde.org/kpresenter/">KPresenter</a> receives export to text document (OpenDocument) functionality. Improvements in the Magnatune music store facility in <a href="http://amarok.kde.org/">Amarok</a>.

<!--break-->
