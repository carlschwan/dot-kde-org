---
title: "Waldo Bastian and John Cherry Speak About aKademy on LugRadio"
date:    2006-10-13
authors:
  - "slangridge"
slug:    waldo-bastian-and-john-cherry-speak-about-akademy-lugradio
---
The <a href="http://www.lugradio.org/episodes/62">latest LugRadio episode</a> features interviews with Waldo Bastian and John Cherry from before <a href="http://conference2006.kde.org">aKademy 2006</a> speaking about what they hoped to get from the conference and what they'll be talking about in their keynotes.  Waldo talks about the <a href="http://portland.freedesktop.org/">Portland Project</a> and what desktop cooperation is all about, and John talks about the state of the Linux desktop from <a href="http://www.osdl.org">OSDL</a>'s point of view. More importantly, they both talk about what they hoped to get from aKademy this year and why the conference is important. The team also discuss why conferences in general are important, and whether there is a big difference between a corporate conference and a community event. 
<!--break-->
