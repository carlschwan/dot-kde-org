---
title: "OSWC and Akademy-es Report"
date:    2006-03-29
authors:
  - "alarrosa"
slug:    oswc-and-akademy-es-report
---
The <a href="http://www.opensourceworldconference.com">Open Source World Conference</a> and <a href="http://wiki.badopi.org/index.php/Kde-es2006">aKademy-es</a> finished some weeks ago, but many of you were probably waiting to read about them, so here is a small report. The OSWC, held in Málaga (a very active city with respect to opensource thanks in part to the work of the <a href="http://www.linux-malaga.org">Linux-Malaga</a> LUG and home to last year's Akademy conference) had lots of KDE representation, with talks given including KDE development, Kontact and the KDE India community.











<!--break-->
<div style="width: 300px; border: solid grey thin; margin: 1ex; padding: 1ex; float: right">
<img src="http://static.kdenews.org/jr/akademy-es.jpg" width="300" height="225" /><br />
KDE Developers together with developers from the Apache Foundation and OpenGroupware.org
</div>

<p>The OSWC is not a developer conference, but that wasn't a problem to get many Linux and Open Source contributors together. There was a conference area with many tracks running at the same time, as well as an exhibition area with stalls from Linux distributors including regional distributions from different local governments in Spain, Linux related companies and Linux &amp; Open Source associations.  More than 500 Kubuntu CDs given away thanks to Jonathan Riddell. Speakers included Till Adams on Kontact, Alexander Dymo on the Open Source development model (<a href="http://www.opensourceworldconference.com/malaga06/es/uploads/ponencias/jueves/tematicas/ALEXANDER_DYMO.pdf">Slides</a>), Pedro Jurado on FreeNX, Andreas Mantia about Kommander and Quanta, Joseph Spilner about the KDE Community and myself, Antonio Larrosa talking about KDE architecture.</p>

<p>Nobody could say that they were bored at the place. There was plenty of <a href="http://developer.kde.org/~larrosa/photos/OSWC-2006/IMG_0833.JPG">booths to see</a>, plenty of <a href="http://developer.kde.org/~larrosa/photos/OSWC-2006/IMG_0863.JPG">people to meet from different projects</a>, plenty of <a href="http://developer.kde.org/~larrosa/photos/OSWC-2006/IMG_0826.JPG">talks to attend</a> and during the nights, plenty of <a href="http://developer.kde.org/~larrosa/photos/OSWC-2006/IMG_0844.JPG">parties to go to</a>. It was a definite success for bringing KDE to the rest of the world.  See the <a href="http://developer.kde.org/~larrosa/photos/OSWC-2006/images.html">photo collection</a>.</p>

<p>Akademy-es started soon after the OSWC finished. This was the first time that nearly all Spanish KDE contributors met together, and we met in Barcelona thanks to the organisation of Victor Barba from Badopi.</p>

<p>During the Saturday, 4th March, Albert Astals (KPDF), Isaac Clerencia (Debian Qt KDE Team), Jaime Robles (Spanish KDE Translations) and me, filled a whole day of talks about translation and internationalisation as well as user and development talks.  We didn't get finished until two o'clock in the morning. There was even more public than expected so the rest of the Kubuntu CDs were given away and everyone had a terrific time as you can see in the <a href="http://people.warp.es/~jorge/fotos/index.php?galerie=Akademy-ES">photo gallery</a>.</p>

<p>In brief, two very well organised events where KDE was well represented and where the public could see many different parts of KDE in action.</p>










