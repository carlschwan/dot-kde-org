---
title: "mEDUXa 1.0 Ready to Take Over the Canary Islands"
date:    2006-11-24
authors:
  - "a(toscalix)"
slug:    meduxa-10-ready-take-over-canary-islands
comments:
  - subject: "Menu layout"
    date: 2006-11-24
    body: "I find it interesting the way they have configured the elementary school desktop (escritorio primaria). Funny layout. The older ones get a more conventional desktop though (escritorio secundaria)"
    author: "MiktT"
  - subject: "Re: Menu layout"
    date: 2006-11-24
    body: "Yes, we have spent more time on the desktop for little kids because they don't have such a thing on windows and it is really dificult for them to use a regular desktop. We hope to recieved a good response from those students, demostrating that linux desktops in general, and KDE in particular, is the answer for specific groups of users with specific demands due to the customization features. For this purpose, kiosk mode is a killer one."
    author: "toscalix"
  - subject: "Re: Menu layout"
    date: 2006-11-24
    body: "How do those desktop cards/groups work? Looks really good."
    author: "MM"
  - subject: "Re: Menu layout"
    date: 2006-11-24
    body: "It's just a custom desktop background with the icons arranged on top of it. \n\nIt is possible to customise KDE pretty completely. We got rid of the KDE Menu, got rid of Kicker, increased the size of the application toolbars, used a bigger mouse cursor all without needing to change any code. The settings can be setup in Kiosk mode profiles and easily managed centrally. Even non-KDE apps like GCompris and TuxPaint really fit it well too because they use non-standard UI elements like big buttons, and don't bother with ordinary fiddly (for 5-8 year olds) application menus."
    author: "Richard Dale"
  - subject: "Re: Menu layout"
    date: 2006-11-25
    body: "Do you think it will be possible to allow all this with settings so parents and other people (libraries, cyber cafe, ...) could easily set up such a desktop for their kids? It has been my idea to do so when we started KDE-Edu and I'd really like this for KDE4.\nMaybe (probably) we should have a working meeting to see how to work on all that before KDE4. \nMeduxa is exactly how I would have dealt with things, there is a lot of work and thoughts behind Meduxa and it fits with all ages needs. \nCongrats to the team!"
    author: "annma"
  - subject: "Re: Menu layout"
    date: 2006-11-25
    body: "Hi Annma yes at present we can configure KDE for kids like mEDUXa, but I think we can do something much better for KDE4 (and the OLPC project). So lets organise a meeting to discuss what we can do - ask Agustin the leader of the mEDUXa project about this."
    author: "Richard Dale"
  - subject: "Re: Menu layout"
    date: 2006-11-25
    body: "Thanks"
    author: "MM"
  - subject: "screenshots"
    date: 2006-11-24
    body: "can we see what it looks like?"
    author: "hipie"
  - subject: "Re: screenshots"
    date: 2006-11-24
    body: "Yes, click on the images on the right of the press release page here:\n\nhttp://www.grupocpd.com/archivos_documentos/info_meduxa/meduxa_project_released/PloneArticle_view\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Very good job!"
    date: 2006-11-24
    body: "Just great! I love seeing things like that which make use of the advantages that only opensource software provides.\n\nYou might want to take a look at Kids icon set by Everaldo.\nhttp://www.everaldo.com/downloads.htm\nThey are nice for little kids:)"
    author: "Leonidas"
  - subject: "I can't believe..."
    date: 2006-11-24
    body: "... so many schools on the Canary Islands???\n\nHere in Germany, we've much less schools related to the population figure...\n"
    author: "anonymous"
  - subject: "Re: I can't believe..."
    date: 2006-11-26
    body: "Yes, it looks like a fake."
    author: "ben"
  - subject: "Re: I can't believe..."
    date: 2006-11-26
    body: "And you base that statement on what? \n\n\nHere are a few independent numbers I've found: \n\nThe overall population is about 2 million people  (see http://en.wikipedia.org/wiki/Canary_islands). \n\nThe European average fraction of students w.r.t. the overall population is between 20% and 25%. This number includes university students and other types, too, though\n(see http://www.eurydice.org/portal/page/portal/Eurydice/PubPage?pub=052DE&h=61&page=1&SizeCode=null&SVG=TRUE (German article)) \n\n\nThe number of 300000 students in *schools* on the Canary islands given in the article would mean a fraction of 15% of the overall population.  Looks completely reasonable to me.\n\n\nDivided by the number of schools (1100) that makes about 270 students per school.  \n\nAnd there is one PC for 9 students.  Or 32 PCs per school (which is about one or two classrooms for IT courses). \n\nDoes this really sound that unplausible?  \n\n"
    author: "cm"
  - subject: "Re: I can't believe..."
    date: 2006-11-26
    body: "Yes you're right, indeed the Canary Islands really exist and have lots of schools. This is the home page of the Canarian Government's info about the schools (in Spanish).\n\nhttp://www.educa.rcanaria.es/\n\n"
    author: "Richard Dale"
  - subject: "Re: I can't believe..."
    date: 2006-11-27
    body: "> Yes you're right, indeed the Canary Islands really exist and have lots of schools.\n\nRichard, that's comforting. I'm sure I wasn't imagining you at aKademy. I know you live in the Canary Islands and I can confirm you really exist too. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: I can't believe..."
    date: 2006-11-26
    body: "No wonder. Remember PISA ?"
    author: "pisastar"
  - subject: "Download?"
    date: 2006-11-24
    body: "Is there somewhere I can download a copy of the student system?\n\nI'd very much like to try it in action!"
    author: "Anders"
  - subject: "Re: Download?"
    date: 2006-11-24
    body: "We're very keen to get as many people trying it as possible and giving feedback as it's all Free Software, but I don't think we've got the infrastructure for public downloads yet. It will need a small amount of work to remove dependencies such as Active Directory authentication and so on, and then put it on a suitable server for download. \n\nI'm quite excited about opening it out as a community project, and we going to organise a WikiMedia based EduWiki for public discussions on integrating with other educational distributions like Skol Linux and so on. Maybe Agustin, the project leader can comment on how long he thinks that will take to get going."
    author: "Richard Dale"
  - subject: "use of KVocTrain"
    date: 2006-11-25
    body: "For the past three years I have been trying to find support (actually a programmer) for an improvement in KVocTrain:\n\nA vocabulary test screen which shows the results at the end of the query. \n\nWe are using the programme at our vocational school in Bad Nauheim (Germany) and the lack of this feature is not very helpful in getting more of my colleagues to use the Linux installation in our language lab.\n\nRichard, what do you think about this extension? \nHow do teachers in the Canary Islands use KVocTrain?\nWhat are their wishes for KVocTrain?\n\nThanks for your ideas!"
    author: "Markus"
  - subject: "Re: use of KVocTrain"
    date: 2006-11-25
    body: "Hi Marcus,\n\nDo you have a mockup about the result screen? I am\u00a0very interested in GUI ideas for such a screen. Implementation is not a problem, design is. I am\u00a0interested in it for several edu applications.\nAbout KVocTrain: did you try KWordQuiz? KvocTrain unfortunately is not maintained and the code is very old. It makes it very difficult to add new features. Peter the KWordQuiz maintainer did his best to work on KVocTrain and I did some small things as well. But what it needs is a real maintainer and defining goals fpor KVocTrain which are not KWordQuiz ones.\nCheers,\nAnne-Marie"
    author: "annma"
  - subject: "Re: use of KVocTrain"
    date: 2006-11-25
    body: "Thanks for your interest Anne-Ma!\n\nWe are using the advanced features of KVocTrain: units, comparison forms, example sentences, conjugations. That makes using KWordQuiz difficult. (Together with a colleague I am the author of the Basic vocabulary German-English file, which you can download via the \"get new vocabulary\" menu item.)\n\nI have done a design of the exam screen including ideas how to implement it (conceptwise not codewise as I haven't got a clue of programming). I'm posting my qt-designer files and my thoughts about it on the kde-edu mailing list, subject \"vocabulary test screen\".\n\nI was wondering, if participating in Goggle's summer of code (too late for this year, I'm afraid) or something like a competition would be a reasonable idea to get the coding started. Is this realistic?"
    author: "Markus"
  - subject: "Re: use of KVocTrain"
    date: 2006-11-25
    body: "If KVocTrain isn't maintained, but is still regarded as useful, I would like to see if we can rewrite it in either Python or Ruby for KDE4. I think PyKDE and Korundum are perfect matches for writing edu apps, whereas C++ is over complicated and too much work."
    author: "Richard Dale"
  - subject: "Re: use of KVocTrain"
    date: 2006-11-25
    body: "Richard, if it isn't maintained (you may ask Peter Hedlund) and you'll try to rewrite it, I volunteer for testing it.\n\nThis is because I regard it as very useful in my English classes - and so do my pupils (the ones that like English as a foreign language for sure)."
    author: "Markus"
  - subject: "Re: use of KVocTrain"
    date: 2007-01-17
    body: "I'm doing my best to maintain KVocTrain. I'm currently working on the port to KDE4. It includes creating a KEduVocDocument library in libkdeedu that is shared by several edu apps. The port also includes a switch to a model/view interface for the vocabulary table (applies also to KWordQuiz). The code base is indeed pretty complex and a non-standard user interface is used in some places that needs to be changed.\n\nAny help with this work is of course greatly appreciated. People gladly ask for new features to the program, but are there maybe also some things that might be removed? As an example, KVocTrain has some intricate code for copying and pasting different formats of delimited text. Isn't a spreadsheet application more suitable for that type of thing?"
    author: "Peter Hedlund"
---
The Education, Culture and Sports Department of the Spanish Canary Island's regional goverment have <a href="http://www.grupocpd.com/archivos_documentos/info_meduxa/">released mEDUXa 1.0</a>.  mEDUXa is a Free Software GNU/Linux distribution developed for educational purposes based on <a href="http://kubuntu.org">Kubuntu</a>. It will be deployed on 35,000 computers in 1100 schools, which represents 325,000 possible users (25,000
teachers and 300,000 students) in the Canary Islands state schools. mEDUXa comes with different profiles, configured thanks to KDE's <a href="http://extragear.kde.org/apps/kiosktool/">Kiosk mode</a>. One of mEDUXa's major feature is the profile for young pupils (from 4 to 8 years old).




<!--break-->
