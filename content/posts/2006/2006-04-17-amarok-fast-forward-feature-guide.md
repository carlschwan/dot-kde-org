---
title: "amaroK \"Fast Forward\" Feature Guide"
date:    2006-04-17
authors:
  - "J\u00f3zef"
slug:    amarok-fast-forward-feature-guide
comments:
  - subject: "Great work"
    date: 2006-04-17
    body: "The new features sound fantastic. Amarok was already great, now its going to be even better. I am looking forward to the gapless playback."
    author: "Ian Whiting"
  - subject: "Looks very promising"
    date: 2006-04-17
    body: "I must admit that I initially really disliked amarok and much preferred the simplicity of Juk, but the amarok devs are slowly winning me over.\n\nI still often prefer Juk for simple music listening, but sometimes, it is nice to be able to view lyrics as you listen to a song. Amarok also seems to impress the windows-using public a great deal, so I guess they must be doing something right.\n\nThe listed features do sound incredible. Anybody know when this version of Amarok will be released and whether Suse will include it in 10.1?\n\n"
    author: "Gonzalo"
  - subject: "Re: Looks very promising"
    date: 2006-04-17
    body: "Very likely SuSE 10.1 will be released before amaroK 1.4.\nBut there are inofficial KDE updates from SuSE on their ftp which usually contain the latest Version of amaroK."
    author: "Sven Krohlas"
  - subject: "Re: Looks very promising"
    date: 2006-04-17
    body: "indeed, and amarok 1.4  beta3 is already available for suse 10.1, suse10.0, and suse9.3(tnx to guru)\nhttp://linux01.gwdg.de/~pbleser/rpm-navigation.php?cat=Sound/amarok/"
    author: "AC"
  - subject: "mirror?"
    date: 2006-04-17
    body: "Is there a morrord version of this document? http://rokymotion.pwsp.net has timeout!"
    author: "panzi"
  - subject: "Re: mirror?"
    date: 2006-04-17
    body: "s/morrord/mirrord/"
    author: "panzi"
  - subject: "Re: mirror?"
    date: 2006-04-17
    body: "As we have not yet officially released it: no. ;-)"
    author: "Sven Krohlas"
  - subject: "Re: mirror?"
    date: 2006-04-18
    body: "s/mirrord/mirrored/ ?"
    author: "MM"
  - subject: "Re: mirror?"
    date: 2006-04-21
    body: "\"man sed\", n00b!"
    author: "Gentoo"
  - subject: "The Website is down!"
    date: 2006-04-17
    body: "hi. I would love to join your conversation about these \"new features\" but the website is down, t'would be nice if I didn't get \"An error occurred while loading http://rokymotion.pwsp.net:\nTimeout on server\n Connection was to rokymotion.pwsp.net at port 80\", ta."
    author: "TheFuzzball"
  - subject: "Re: The Website is down!"
    date: 2006-04-17
    body: "well, yeah, its down. same with amarok.kde.org :("
    author: "superstoned"
  - subject: "Mirror"
    date: 2006-04-17
    body: "Well, for those looking for a mirror, there's always old google cache: http://tinyurl.com/jh9lx\n"
    author: "ricky"
  - subject: "Re: Mirror"
    date: 2006-04-17
    body: "non-stripped version at http://tinyurl.com/et3h3 is better\n"
    author: "jose"
  - subject: "Sad to see decision with iconset being kept"
    date: 2006-04-18
    body: "It's a little sad to see that the decision to have their own (non-KDE) iconset by default was not overturned: I find that very hard to believe, particularly because of the popularity of the bug report on it. For those who aren't aware of it, add votes to the bug if you aren't happy with the decision and its implications:\n\nBug 125295: Disable custom icons by default\nhttp://bugs.kde.org/show_bug.cgi?id=125295\n\nAs everyone has said, this totally breaks down one of the things that KDE tries to prize itself in: slick integration by consistency through its applications. It's a shame that the amaroK devs think themselves \"powerful\" enough to ignore the users' requests, now. Very sad."
    author: "apokryphos"
  - subject: "Why You Shouldn't Be Sad"
    date: 2006-04-18
    body: "amaroK is good because we disregard the vocal minority and do what is best for the product.\n\nAlthough I agree we shouldn't theme undo/redo. Otherwise most of the themed-buttons don't have a KDE icon, until now we've used icons that look inappropriate if you use an icon set other than crystal.\n\nThe hilarious thing is, if markey had never blogged on this, nobody would be moaning about it now, and come the release we'd probably all be praised for making amaroK more usable because now the icons on the interactive widgets make sense.\n\nEvery month I'm more and more tempted to do less and less development in the open, and just realise betas to a restricted audience. But this will never happen, don't worry about it. I'm just tempted to suggest it to markey."
    author: "Max Howell"
  - subject: "Re: Why You Shouldn't Be Sad"
    date: 2006-04-18
    body: "I think the new icons are great. They fit the application better then the default one.\nAnd besides, amaroK has an option to switch back to the default kde style...\n"
    author: "AC"
  - subject: "Re: Why You Shouldn't Be Sad"
    date: 2006-04-18
    body: "/Although I agree we shouldn't theme undo/redo. Otherwise most of the themed-buttons don't have a KDE icon, until now we've used icons that look inappropriate if you use an icon set other than crystal./\nIf that were the case there'd be no problem with changing them. The play/pause etc. are used eveywhere that plays media.\n/The hilarious thing is, if markey had never blogged on this, nobody would be moaning about it now, and come the release we'd probably all be praised for making amaroK more usable because now the icons on the interactive widgets make sense./\nBollocks. I'd be complaining straight away; not only do the new icons not fit in, they look ugly too.\n/Every month I'm more and more tempted to do less and less development in the open, and just realise betas to a restricted audience. But this will never happen, don't worry about it. I'm just tempted to suggest it to markey./\nWhat, because being in the open forces you to actually listen to your users?"
    author: "mikeyd"
  - subject: "Huge Ears Focused On Our Users"
    date: 2006-04-19
    body: "> > Every month I'm more and more tempted to do less and less development \n> > in the open, and just realise betas to a restricted audience.\n>\n> What, because being in the open forces you to actually listen to your users?\n\nActually it's comments like that this that make me pretend I dislike open development. Some of you guys are horrible and make unecessary comments designed to be offensive. Reasonable and pleasant debate will be responded to in kind.\n\nPeople seem to forget that amaroK is an application for KDE, but not a KDE-application. That is; we are not distributed with the core for a reason. We like to be a little different, we _always_ have. I joined the project at version 0.6.0, and did so because amaroK was trying to push boundies, and emphasised form, usability and aesthetics slightly higher than other KDE projects. We like to look pretty, and yes this does mean we deviate from the KDE look slightly.\n\nAlso I have to be honest. The play/pause/etc. icons suck in all KDE icon themes I've seen. I prefer the new amaroK ones, and the consistentency of the icon set makes amaroK seem more polished and more professional, which reflects well on KDE as a whole.\n\nI'll reiterate one of my previous points. We fully expect the new icons to be 70% well received. If this is not the case we'll change our policy with respect to the icons. My personal impression is 90% of the people complaining, haven't built an amaroK that uses them, and judgements based on screenshots are rarely convincing.\n\nBut may I emphasise again, we will change the policy on icons if everyone hates them. We always listen to our users, even though people like you consistently spread FUD that we don't. We just don't listen to the vocal minority as they are NOT our users. They are just a small percentage of them.\n\nTo clarify, the vocal minority are the people who make loud comments on message boards they know we read. Our actual users send us suggestions and pleasant requests on irc and through email, or discuss amaroK innocently on boards they don't think we frequent."
    author: "Max Howell"
  - subject: "Re: Why You Shouldn't Be Sad"
    date: 2006-04-18
    body: "for as far as KDE had no icon for actions in amaroK - of course its cool you created them.\n\nand i also like the new icons\n\nthe icons you guys used for actions which already had an icon, on the other hand, DO make KDE less consistent, and i think it shouldn't be on by default, how nice these icons are - they don't look that good with certain styles, color schemes and iconsets users use. the idea behind a global icon theme is simply destroyed. same with the special color scheme amaroK used to have - it was nice, but just didn't fit sometimes. \n\nnow i'd rather see a option (maybe even in the first-time wizard) to choose amaroK's own look - whatever, make it more themable. but don't use it by default, not because most users won't like it - but because it won't fit for those that 'stood up' and changed the theme themselves. \n\n**if you took the time to change the look & feel of KDE, you should be rewarded: apps should look the way you choose**\n\ni think THAT should be the reason NOT to put this theme on by default.\n\ninconsistency - hmmm, important, but hey - who wouldn't know how to use amarok just because it looks a bit different...\nbut those that changed things GLOBALLY, be it shortcuts, toolbar settings, fonts, colors of icons - they won't like this, they might even be upset. some of them will be able to change it but most won't even think that's possible!\n\ni know, if you disable the custom icons, most users won't see them - only the ones where no KDE icon is available. that doesn't feel right. on the other hand, why not try to get the icons which are not in KDE, officially included!"
    author: "superstoned"
  - subject: "Re: Why You Shouldn't Be Sad"
    date: 2006-04-18
    body: "Part of the problem is that \"idea behind global icon themes\" has major a problem. There is a real cost that must be paid for that level of configurability, and it is paid for by applications like amaroK losing control of its look, resulting in a mismatch of styles.\n\nAs far as getting icons into KDE, KDE 4.0 is the next new version of KDE that we will depend on. That won't be for a while. We've been depending on KDE 3.3 for a while now. Its just not a viable solution, amaroK often has a need for additional icons and we can't wait the year until amaroK depends on a newer version of KDE."
    author: "Ian Monroe"
  - subject: "Re: Why You Shouldn't Be Sad"
    date: 2006-04-18
    body: "Absolute application-specific configurability cannot be used as a justification for having the new icons enabled by *default*. Consistency is vital, particularly in KDE, and it really isn't for no reason. User-experience and familiarity is dramatically altered by it. Needless to say, I think it's a small sacrifice to not allow such application-specific configurability in order to maintain consistency. "
    author: "apokryphos"
  - subject: "Re: Why You Shouldn't Be Sad"
    date: 2006-04-18
    body: "Its good to have you back Max, you always bring clarity. :)"
    author: "Ian Monroe"
  - subject: "Re: Why You Shouldn't Be Sad"
    date: 2006-04-18
    body: "I'm sorry to say, but I can't see any justification for why we're only objecting to this because we saw it in beta. That's not true at all. \n\nAlso, I really don't think it's a \"vocal minority\" when there are many prominent KDE hackers *and* experts who disagree with such a move, and needless to say many users who have thought about it. \n\nI don't really think that anyone has argued that the icons themselves aren't aesthetically pleasing: we're quite agreed on that. I think they would, however, look extremely unfitting with some settings, and the fact that they have aesthetic appeal in absolutely *no way* justifies deviating from such a vital KDE principle (which is there for, yes, a reason). KDE is slick because it's so easily themeable; amaroK's decision greatly deviates from such a principle, and it seems to me like they're ignoring the idea on pretty unsubstantiated presuppositions. "
    author: "apokryphos"
  - subject: "Re: Why You Shouldn't Be Sad"
    date: 2006-04-19
    body: "I often envy OSX and XP. They can make their apps look great easily because there is bascially one colour theme and icon set; ie. one look to theme to. XP does some great things with subtle blues and oranges. Orange is the compliment to blue, and thus they have effectively two highlight colours, the normal highlight is dark blue, and you see it when you select things. The orange is used as a kind of super-highlight. In the start-menu it is used as a descriptive text colour. Before someone says it, I also dislike the blue theme on XP, it is unpleasant over time, and although distinctive, it feels unprofessional. I just admire the way they have made the most of what they are stuck with.\n\nKDE only has one highlight colour, and because the colour theme is not fixed, we can't just figure out another highlight colour by generating a suitable compliment using the GIMP. Instead we have had to learn some sophisticated colour modification techniques to try and do such things. And it's hard and doesn't always work, but we still do better than many other KDE apps that use fixed colours and assume the list-view, etc. will have a white or near-white background. Eg, Cervisia.\n\nKDE is too flexible. Fact. It impedes our progress to make unique applications that look as good as they do on OSX and XP.\n\nOf course I also know the strength of KDE is that it has consistency, everywhere. But we are not a KDE application, we are an application that uses KDE. So we try to be a little different. We love KDE still, or we'd have built amaroK on GTK, so we try to have the best of both worlds.\n\nAnyway, to the point. Quite simply, we must have some custom icons, and yes you are right, they will not suit all KDE colour-themes and skins. So what do we do? It seems a moot point to remove the play/pause icons as we still have to have 15 custom icons to represent our other exclusive features that don't have appropriate icons in a KDE-icon-set.\n\nI maintain we should use the KDE undo/redo icons though as these are common and standard actions. Play/pause/et al. are less commonly used and known, and also they represent the media-player, changing them gives us identity."
    author: "Max Howell"
  - subject: "Re: Why You Shouldn't Be Sad"
    date: 2006-04-19
    body: "> And it's hard and doesn't always work, but we still do better than many other KDE apps that use fixed colours and assume the list-view, etc. will have a white or near-white background. Eg, Cervisia\n\nCervisia doesn't use fixed colors. They are configurable in the settings dialog.\n\n> as we still have to have 15 custom icons to represent our other exclusive features that don't have appropriate icons in a KDE-icon-set.\n\nThat's what other applications do. They provide the icons in crystal style that are not offered by the standard crystal icon set. Other icon sets can than choose to override those icons."
    author: "cl"
  - subject: "Re: Why You Shouldn't Be Sad"
    date: 2006-04-25
    body: "> Cervisia doesn't use fixed colors. They are configurable in the settings \n> dialog.\n\nCongrats on totally missing my point. Good point on the crystal scheme thing though, however we still want to look different, if you don't respect that then I think I'm through caring."
    author: "Max Howell"
  - subject: "Re: Why You Shouldn't Be Sad"
    date: 2006-04-19
    body: "> amaroK is good because we disregard the vocal minority and do what is best for the product.\n\nYour arrogance makes me sick."
    author: "Anonymous Coward"
  - subject: "Re: Why You Shouldn't Be Sad"
    date: 2006-04-22
    body: "Correct me if i'm wrong but most if not all amarok devs are volunteers creating a wonderful product for your enjoyment. How dare you talk to them like that. Have you no sense of gratitude. What ever your feelings on the issue at hand it is not fair to resort to name-calling.\n\n-rjs."
    author: "Robin Shannon"
  - subject: "Re: Why You Shouldn't Be Sad"
    date: 2006-04-28
    body: "your cowardly anonymity makes me laugh."
    author: "illogic-al"
  - subject: "Re: Sad to see decision with iconset being kept"
    date: 2006-04-18
    body: "As far as I can see, amarok's new icon-theme is by far no complete icon-theme. It's just play/pause/stop/... that are replaced. These icons are not common in other KDE applications. So it's no real break of consistency IMHO."
    author: "birdy"
  - subject: "Re: Sad to see decision with iconset being kept"
    date: 2006-04-18
    body: "There are quite a few icons (see the icon page on the wiki), and these icons really *are* used in other applications. Even if it was I really don't think it would justify deviating from a standard (which isn't held for no reason). "
    author: "apokryphos"
  - subject: "Re: Sad to see decision with iconset being kept"
    date: 2006-04-23
    body: "I don't like these icons, either. I was annoyed ever since amarok started to use the cartoonish player buttons, which really stick out in my otherwise clean (= boring :-) desktop. But I don't complain. I just fixed that on my computer and keep enjoying amaroK that way. Thanks to the great KDE architecture it's possible to exchange about any images/sounds/whatever. KDE always searches all paths in $KDEDIRS, so you can override any global file in your home directory. For the player icons, for example, you just need to add your favorite icons as $KDEHOME/share/apps/amarok/images/{b_next,b_pause,b_play,b_prev.png,b_stop}.png\n\nThese are mine: http://members.aon.at/mfranz/amarok_btns.tar.bz2 [911 Bytes] (old standard KDE icons, actually). This is as it looked before http://members.aon.at/mfranz/amarok-bad.png [16 kB], and as it looks now http://members.aon.at/mfranz/amarok-good.png [12 kB]. Of course, some will find my style ugly, too, but ...   :-}\n\nPS: I really tried to make the links clickable. But there was only the \"Plain Text\" option in the drop-down, and <a> etc. didn't work."
    author: "Melchior FRANZ"
  - subject: "Re: Sad to see decision with iconset being kept"
    date: 2006-04-24
    body: "I like them, I think they are simply far better that the default ones, but if you don't like them you can simply come back! Where's the problem?\nThank you amarok developers. :)"
    author: "Giovanni"
  - subject: "Summer of Code"
    date: 2006-04-18
    body: "http://code.google.com/soc/mentorfaq.html\n\nAny chances for intrested Amarok coders to get summer of code funds?"
    author: "Hen Tai"
  - subject: "Songbird"
    date: 2006-04-18
    body: "What do you think about Songbird?\n\nhttp://www.songbirdnest.com\n\nIt seems to be a Mozilla-based iTunes ripoff but at least in the fields of API for commercial services they are more ambitious.\n\n"
    author: "Hen Tai"
  - subject: "Re: Songbird"
    date: 2006-04-19
    body: "looks nice, however: they promise to be cross platform, but only offer downloads for windows?"
    author: "AC"
  - subject: "Re: Songbird"
    date: 2006-04-19
    body: "It is currently proof of concept but looks promising for a 0.1."
    author: "gerd"
  - subject: "A good program getting better.."
    date: 2006-04-19
    body: "I've always found Amarok to be a \"shining star\" amongst Linux apps in general. I am very pleased with the latest version (1.3.8 in my case on Gentoo). The developers have made some great enhancements to what was already a good program.\n\nI was very excited to see support for flash disc based players such as my Samsung. However, it (Amarok) seems to have a conflict with hal and or udev in KDE. When the player is plugged in (USB) KDE auto-mounts it to my desktop. Amarok recognizes that it's there too (wahoo!), but insists that I mount it when I hit the \"Connect\" button even though it is already mounted and I can browse it in Konqueror just fine. If anyone knows a way around this, I'd be much obliged.\n\nThanks again to the Amarok team. Great job!"
    author: "peterservo"
  - subject: "Re: A good program getting better.."
    date: 2006-04-19
    body: "Hmm, need to check this with my own samsung mp3 player :o)\nafaik amarok 1.4 can see the difference between automounted devices and manually mounted devices.\n"
    author: "AC"
  - subject: "Re: A good program getting better.."
    date: 2006-04-19
    body: "It is indeed, however this is a new feature, i'm not entirely sure that it was actually included in beta3c :) But it /is/ what finally made me able to use my usb mp3/wma/ogg player thingie ;)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "amarok getting 'fat'"
    date: 2006-04-19
    body: "i tried amarok in the beginning and it was ok, i liked the fetched covers and stuff, but i think over time it has gotten a little bloated. there is so much stuff, so many features that you (or at least I ) dont need. \nwith amarok getting bigger and bigger it is really becoming a memory-monster and losing points on simple things.\nit might attract windows users' attention and i think that is good, when kdelibs are ported it could help spread oss to more desktops, but i am not considiring it for personal use any longer.\njuk on the other hand i have really learned to love. it has proven to be less memory-hungry than amarok, it supports gstreamer, too, it doesnt have useless stuff like an ipod menu (or tab or how do you call these modern widgets on the left?) - if you dont have an ipod - and it doesnt spend resources calculating what it thinks will be my next favorite song.\nALSO it is possible to create dynamic playlists from folders, which amarok (for some reason) cannot.\ni just hope that with the amarok hype going on, developement on juk is not suspended."
    author: "hannes hauswedell"
  - subject: "Re: amarok getting 'fat'"
    date: 2006-04-19
    body: "Wonderful world of choice! You have just made the exact point of why both amaroK and JuK exist - because you like JuK's simplicity, and others like the much more advanced features of amaroK :)\n\nJust as a random tidbit - the iPod browser is finally not just an iPod browser any longer. The idea has always been to include support for other devices, and now that has come :)\n\nDon't worry, JuK and amaroK share some things, but they're developed by two different teams of developers, so do not worry, neither will be in any way neglected :)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: amarok getting 'fat'"
    date: 2006-04-19
    body: "Oh yeah, and amaroK actually can make smart playlists based on folders :) (a dynamic playlist in amaroK is a tad different, more akin to iTunes' party mode) The full path name can be used as a condition, so that's possible :)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "amaroK and JuK"
    date: 2006-04-19
    body: "JuK will continue on into KDE 4.  I think one of the significant \"identity\" issues with JuK at the moment is that at this point is that it's partially defined in terms of its differences to amaroK and in KDE 4 I plan to solidify that a bit more.\n\nThe basic plans for the future of JuK are to try to kind of make ease of use the central focus for KDE 4.  In concrete terms that means polishing the interface a bit, trying to think more in work flows.  Depending on time there are some new features that I'd like to add as well but they're more about functionality than \"interface candy\".  (For those that want that, this is and will be amaroK and that is in fact a good thing.)\n\nMy personal reaction to amaroK is always kind of funny.  A lot of people really love the bells and whistles, for me there's something of a visual overload; there's always this weird psychological reaction that the closest thing I can compare it to is claustrophobia and I have a desire to close it.  (Again, just to be clear -- I don't mean this as criticism of amaroK -- it's surging popularity clearly indicates that for a lot of people its interface is quite comfortable, but JuK will stay around for the folks like me that like things a bit leaner.  :-) )"
    author: "Scott Wheeler"
  - subject: "Re: amaroK and JuK"
    date: 2006-04-19
    body: "great!\n\none little thing about juk: what happened to the old renamer? would it be possible to change the new one so that no new folders are created or files moved, only the names of the files are changed. i have a rather complex sorting hierarchy in my collection and dont want things moved around, i just want \"artist - audio track1.ogg\" to be converted to values from the tag...\n\nthanks for your work\n\n\np.s.: \"visual overload\" is a good explanation of the amarok-effect ;)"
    author: "hannes hauswedell"
  - subject: "Re: amaroK and JuK"
    date: 2006-04-19
    body: "And we not-unoften close bug reports with the suggestion that they should \"just use JuK.\" ;)"
    author: "Ian Monroe"
  - subject: "KIO Slaves and TranKoding"
    date: 2006-04-19
    body: "I tried the beta version in the Debian experimental repository.  It's great to see that playing audio CDs now works.  The iPod generalizations are also very nice.\n\nOne thing I would have liked is if it had just used the fact that the KIO audiocd slave provides MP3 (and OGG) encoded tracks.  This should have made it easy for amaroK to directly copy tacks off of the CD to any external media player.\n\nThe external TransKode script thing just feels a bit like a hack... : )"
    author: "T-Bone"
  - subject: "Re: KIO Slaves and TranKoding"
    date: 2006-04-19
    body: "I haven't tried it yet, but afaik this already works in amarok\n\nTo do so: \n- put a audio cd in the drive\n- open the tab 'files' in amaroK\n- type 'audiocd:/'\n- open the mp3 folder and select the files you want to transfer\n- right click and select 'add to media device que'\n- go to the media device tab\n- transfer the files in the que to your mp3 device\n\n"
    author: "AC"
  - subject: "DDOS"
    date: 2006-04-19
    body: "Just FYI, amarok.kde.org is slow because the server has been massively DDOS attacked for the last 24 hours. The problem is being worked on, sorry for the inconvenience.\n"
    author: "Mark Kretschmann"
  - subject: "Wow!"
    date: 2006-04-24
    body: "That looks freaking impressive. Seems like I won't feel like I'm in a maze any longer when I (will) start up amaroK"
    author: "Anonymous"
  - subject: "toggleable browsers"
    date: 2006-04-24
    body: "It's great that we can now hide browsers we don't use.  All I really want is the context browser, plus a file browser which starts in my music directory.  Using it like this just crashed on me, but I'm sure you guys will work that stuff out soon enough :)\n\nBUT... does it just hide those browsers?  Would it be possible to reduce startup time and memory footprint by not loading disabled browsers at all?\n"
    author: "Lee"
  - subject: "Re: toggleable browsers"
    date: 2006-04-25
    body: "Unfortunately not really, I tried to make that sort of thing possible once but too many bits of amaroK expect the browser instances to be instantiated. Unforunately amaroK will never be really lean on memory use. Not that getting rid of the browsers would help that much."
    author: "Max Howell"
---
For those of you living on the bleeding edge, I discovered a document describing <a href="http://rokymotion.pwsp.net/promowiki/index.php/SVN_preview">features recently added to amaroK "Fast Forward" 1.4</a> in SVN. Although this document is a work in progress, it should give you a good idea of what to expect in the next major <a href="http://amarok.kde.org/">amaroK</a> release.



<!--break-->
