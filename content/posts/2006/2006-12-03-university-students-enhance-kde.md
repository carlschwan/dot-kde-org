---
title: "University Students to Enhance KDE"
date:    2006-12-03
authors:
  - "jhall"
slug:    university-students-enhance-kde
comments:
  - subject: "Great idea"
    date: 2006-12-03
    body: "I really like the idea of this university. Taking trained people to improve their skills and free software products at the same time. I'm pretty sure some of them will keep working later on the project in which they started.\n\nWe, students haven't got lots of free time (generally), but I think that we sometimes have that point of i-won't-move-from-here-till-finished (and no students too, for sure :P).\n\nI haven't read any code of KOffice or something, but on KDE there is no lack of documentation at all, and Qt has a very complete documentation in general.\n\nTo sum up, keep up the good work guys, and I'm very happy with this kind of ideas... brilliant.\n\nBye !!"
    author: "Rafael Fern\u00e1ndez L\u00f3pez"
  - subject: "Re: Great idea"
    date: 2006-12-04
    body: "> I haven't read any code of KOffice or something, but on KDE\n> there is no lack of documentation at all, and Qt has a very\n> complete documentation in general.\n\nWell I have experienced the lack of it. The kde-core libs are well documented, but the other libs are not.\n\nTry implementing a KHTMLPart for instance, and updating the HTML page with DOM manipulation methods from C++. I couldn't figure it out with the documentation, and actually had to dive into Kopetes code to find an example of how it works. Turns out you could just assign a DOM node objects to a HTMLElement class. This is not documented at all, making it almost impossible to use a KHTMLPart in your app."
    author: "Diederik"
  - subject: "Re: Great idea"
    date: 2006-12-04
    body: "Well it's open source right. So if you found out how to do, why don't YOU add the    documentation to the source code??"
    author: "cl"
  - subject: "Re: Great idea"
    date: 2006-12-12
    body: "That's a problem for us developers developing under open source. We often do not document our software when it is needed and thus, the next developer that uses our works may have to miss out on some nifty but undocumented feature."
    author: "Jisaku Jien"
  - subject: "Excellent!"
    date: 2006-12-03
    body: "Very good, keep it going guys, we are with you. :)\n\nBest Regards"
    author: "KDE fan"
  - subject: "Fantastic"
    date: 2006-12-03
    body: "Fantastic news! btw, something like a year ago our university did the same with a 4-devel team working on the Kexi-codebase and it was a full success (so,  learned a lot specialy about the advantages of eXtremProgramming, new code for Kexi and a very nice grades :) May the source be with you all!"
    author: "Sebastian Sauer"
  - subject: "Re: Fantastic"
    date: 2006-12-03
    body: "btw, http://www.klear.org (KDE DVB TV app) was also at least partly developed within our university and from talking with friends who study at other universities, the KDE-framework is very much in use there too as fast, easy, powerful and portable rich client platform. Specialy the nice codebase, the community and the XP-way it's developed are ideal to extend the own knowledge-base."
    author: "Sebastian Sauer"
  - subject: "Create KDE-marketing programme to spread this ?"
    date: 2006-12-03
    body: "This is truly excellent news!\n\nIs there a marketing programme within the KDE marketing group to promote KDE-based projects to universities and colleges worldwide?\n\nIf there could be a central repository of information, benefits and tasks associated with having a KDE-project as part of either the curriculum during the computer programming courses, or as their final project, it would be great for both the code base and the generic know-how of KDE. \n\nCurrent KDE-users could front the matter in their local alumnis and thus try to get one KDE project in every university/college?\n\nAnyone? Where's the appropriate place to volunteer if such work is already underway? (most likely is :-))"
    author: "Halim"
  - subject: "Re: Create KDE-marketing programme to spread this ?"
    date: 2006-12-03
    body: "In my school, the guys interested in computers (the team managing our network) are only Gnome supporters ... but I'm trying to convert them, I'm confident ;)"
    author: "Marc"
  - subject: "Re: Create KDE-marketing programme to spread this "
    date: 2006-12-03
    body: "It's my understanding that Kevin will be writing up a 'how-to' for getting Universities involved in KDE once this has all finished and he's seen how it turns out :)"
    author: "canllaith"
  - subject: "enough with java in the universities!"
    date: 2006-12-03
    body: "I threw the idea to my software engineering prof. and he didn't even reply me... I'm so bored of Java projects too..."
    author: "peter"
  - subject: "Re: enough with java in the universities!"
    date: 2006-12-03
    body: "+1 ^^"
    author: "Thibault Normand"
  - subject: "Great code base"
    date: 2006-12-03
    body: "A student I know was given the task of writing a spreadsheet application that had a special function \"foo\" (I can't remember what it was). I suggested instead that he implement foo on top of Kspread. He was enthusiastic when I showed him the beautiful code structure. I don't think anything came out of this one, but the sheer beauty of some of the code in KDE makes it almost as appealing to add to KDE as starting from scratch ;-). In addition, you get to solve some new problems rather than reinventing the wheel, and your code potentially gets out in the real world.\n\nFor these reasons, I think KDE should be a first choice for programming projects in universities everywhere."
    author: "Martin"
  - subject: "It would be really great if this effort could ..."
    date: 2006-12-04
    body: "It would be really great if this effort could bring Umbrello up to the level of Dia, so that serious users could finally switch over to Umbrello. Dia has some basic functionality concerning layout and scaling that Umbrello still misses, see \nhttp://bugs.kde.org/show_bug.cgi?id=94081\n\n\"Dia has solved the scaling problem and it works great. This is a necessary functionality, which means I will use Dia because of this regardless how many other features Umbrello has. Please look how Dia does it. I will explain it short: \n  1. The diagram can be expanded in any direction. In Umbrello it seems like it can only be expanded right and down. \n  2. The user selects the paper orientation and the number of pages horizontally and vertically. (Right click on the diagram, select File, Page setup..., Scaling, Fit to <x> by <y>.) \n  3. Dia scales the diagram to fit on the specified paper layout. The page boundaries are shown as coloured lines.\""
    author: "Erik"
  - subject: "TaskJuggler vs kplato vs mrplanner "
    date: 2006-12-04
    body: "sad to see three projects for the same purpose, and no one of them can make a difference compared to closed source software !!\n\nPrimavera still has a bright future for their products, just imagine 4000$ for a 5 years old,16 bit software.\n\n\nanyway,hope you a great success"
    author: "djouallah mimoune"
  - subject: "Re: TaskJuggler vs kplato vs mrplanner "
    date: 2006-12-04
    body: "It is actually very easy to make a major impact on this and projects such as kivio. Simply start importing and exporting the MS format for the associated programs. Interestingly, if somebody took the time to determine the format, then the code could be used by multiple projects (kde and gnome)."
    author: "a.c."
  - subject: "Diplomarbeiten"
    date: 2006-12-06
    body: "In Germany all it would need is a company which advertises the offer to do your master thesis on KDE and provides some money."
    author: "gerd"
---
A group of students at the <a href="http://www.ups-tlse.fr">Paul Sabatier University</a> in Toulouse will be collaborating on the KDE projects <a href="http://www.koffice.org/kplato/">KPlato</a> and <a href="http://uml.sourceforge.net/">Umbrello</a> as part of their <a href="http://www.iup-ups.ups-tlse.fr/isi">Institut Universitaire Professionalisé en Ingénierie des Systèmes Informatiques</a> (Professional Institute of Computer Software Engineering) course of study.








<!--break-->
<p>Students undertaking the course of study engage in a group project at the end of each year, with a focus on project planning, architecture, coding and testing phases. Traditionally, the proposed projects were very focused on process and project management. Kévin Ottens, KDE developer and a PhD student at the University proposed the addition of Open Source projects to the proposal, and was instrumental in arranging the collaboration. The students were given a choice between the more traditional style java based projects usually offered by the University and a KDE project, and chose to work on a KDE project as part of their course of study. When asked what version of KDE they wished to work on, all chose KDE4 (trunk).</p>

<p>11 fourth year students have already started, with 4 or 5 more third year students to join each team mid project. The students are being mentored by KDE developers, Jonathan Riddell for Umbrello, and, Thomas Zander and Will Stephenson for KPlato. While the KPlato maintainer, Dag Andersen, is unable to find the time to mentor, he has been very supportive and the students report that they are pleased with his feedback.</p>

<p>The students currently involved are:</p>

<ul><li>KPlato team
<ul><li>Gohar Avetisyan</li>
<li>Frédéric Becquier</li>
<li>Frédéric Lambert</li>
<li>Alexis Ménard</li>
<li>Nicolas Micas</li>
<li>Florian Piquemal</li></ul>
</li>
</ul>

<ul><li>Umbrello team
<ul><li>Caroline Bourdeu d'Aguerre</li>
<li>Hassan Kouch</li>
<li>Florence Mattler</li>
<li>Thibault Normand</li>
<li>Pierre Pettera</li></ul>
</li></ul>

<p>When asked his opinion about the projects, Henri Massié the director of the institute stated that "This collaboration with the KDE community is a good thing for us. It is refreshing to see our students so motivated by these kind of projects. I wish it will be a success and hope that we will be able to reiterate this collaboration next year".</p>

<p>Kévin Ottens graciously consented to interview the students about the projects.</p>

<p>Kévin: Why did you choose to work on these projects?</p>

<p>Thibault: Avoid working on yet another Java based project!<br/>
Frédéric L.: I wanted to see something new than a Windows based workspace.<br/>
Florence: It's an original project domain wise.<br/>
Florian: Mainly curiosity, and that's a good opportunity to switch to Linux.<br/>
Alexis: We're sure that it'll be useful to people.<br/>
Pierre and Caroline: Free Software and being part of a community!!<br/>
Nicolas: Because it looks fun, and at the same time it will allow to improve my resume.</br>
Florian and Frédéric B.: To improve our skills, and learn Qt.<br/>
Pierre: Because I'd like a kde.org mail address. :-)<br/>
Gohar: To improve my C++ skills.</p>

<p>Kévin: You already contacted people involved in your respective projects. Did you feel well accepted?</p>

<p>Caroline and Thibault: Yes! We're very happy!<br/>
Florence: We got a lot of feedback and proposals.<br/>
All: Warmly!!! We were welcomed warmly.</p>


<p>Kévin: How did you perceive the development environment setup?</p>

<p>Florence: Not enough documentation.<br/>
Frédéric L.: Well, the documentation is okay for the base, but I perceived a lack of documentation for the other modules like koffice.<br/>
Florian: Not exactly easy, but really interesting, I learnt a lot!</br>
Thibault: It's a change, it looks less like simply applying what we learn
during lectures.</p>


<p>Kévin: And for the future? How do you feel what will happen in your projects next?</p>

<p>Alexis: I'm impatient to work more with the code itself!<br/>
Caroline: I don't feel confident yet.<br/>
Florence: Yes, that's quite a lot of code to learn.<br/>
Gohar: I feel confident, it doesn't look unfeasible.<br/>
Nicolas: Moreover we have tremendous community to find help!<br/>
Hassan: Only "KJoy"! :-)</p>

<p>The enthusiasm of the students is clear to see, and KDE can only benefit from the contribution of this talented team. KDE warmly welcomes the students of the Paul Sabatier University and thanks the University and Kévin Ottens for providing this opportunity. Special thanks also go to the mentors for their dedication to KDE.</p>







