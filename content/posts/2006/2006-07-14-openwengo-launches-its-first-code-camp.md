---
title: "OpenWengo Launches Its First Code Camp"
date:    2006-07-14
authors:
  - "paljord"
slug:    openwengo-launches-its-first-code-camp
comments:
  - subject: "Deadline passed."
    date: 2006-07-13
    body: "According to the 'Official Rules' document on their website, the deadline for applications was July 9th.  :("
    author: "Robert Knight"
  - subject: "OpenWengo SIP backend for Kopete/KDE?"
    date: 2006-07-14
    body: "There was a suggestion on the KDE Decibel mailinglist to have OpenWengo libraries provide SIP functionality to Kopete and other KDE apps:\n\nhttp://mail.kde.org/pipermail/decibel/2006-June/000001.html\n\nFrom personal experience I can say that OpenWengo is a better VoIP client than Skype for anyone with limited bandwidth. Not to mention that OpenWengo is 100% Free & Open Source and licensed under the GPL :-)"
    author: "AC"
  - subject: "Re: OpenWengo SIP backend for Kopete/KDE?"
    date: 2006-07-14
    body: "Not to mention v2 will support video calling which Skype for Linux doesn't, and that it will allow you to pick your phone-out provider.\n\nJohn."
    author: "Odysseus"
  - subject: "Re: OpenWengo SIP backend for Kopete/KDE?"
    date: 2006-07-14
    body: ">From personal experience I can say that OpenWengo is a better VoIP client than   Skype for anyone with limited >bandwidth.\n\nAre you sure? AFAIK, and according to the FAQ of Skype, it is usable with a dialup connection, a throughput of 33,6 Kbps being enough. For Wengo, however, you'll need a broadband connection, with at least 128 Kbps. \n\nAm I wrong? Is there something new with Wengo? If so, some changes are necessary for some Wikipedia pages."
    author: "Anonymous"
  - subject: "Re: OpenWengo SIP backend for Kopete/KDE?"
    date: 2006-07-15
    body: "I have family in Eastern Europe who have a slow DSL connection. After 5-10 minutes speaking on Skype, their voice becomes metallic to the point that it's not understandable. With Wengo, I can speak for hours without any degradation in sound quality (which is really good, btw)."
    author: "AC"
  - subject: "Silliest. Name. Ever."
    date: 2006-07-14
    body: "No wait, this was Joomla..\n\n;)\n"
    author: "Mark Kretschmann"
  - subject: "Re: Silliest. Name. Ever."
    date: 2006-07-14
    body: "or 'Amarok' :-p\n\n(still, if openwengo was as good as Amarok I'd be very happy)"
    author: "Simon"
---
<a href="http://www.openwengo.org">OpenWengo</a> has launched its first <a href="http://www.openwengo.org/index.php/openwengo/public/homePage/openwengo/public/codecamp">Code Camp</a>. As OpenWengo GUI uses Qt 4 some of the projects involve Qt programming including building a "Round trip XUL editor using Qt Designer" and "Writing a Qt widget able to embed several different web browsers engines".   Winners will get a &euro;3500 reward.


<!--break-->
