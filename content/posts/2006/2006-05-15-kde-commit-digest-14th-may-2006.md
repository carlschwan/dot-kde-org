---
title: "KDE Commit-Digest for 14th May 2006"
date:    2006-05-15
authors:
  - "dallen"
slug:    kde-commit-digest-14th-may-2006
comments:
  - subject: "Phonon Rocks!"
    date: 2006-05-14
    body: ".... nuff said!"
    author: "Captain Phonon"
  - subject: "Re: Phonon Rocks!"
    date: 2006-05-14
    body: "It sure does. Looks like porting apps to phonon is cool these days ;-)"
    author: "superstoned"
  - subject: "\"polishing for amaroK 1.4\""
    date: 2006-05-14
    body: "Will the bug 110137 ever be fixed?\n\n\"xine skips songs in playlist when using random\"\n\nAmarok is a great player... But skipping songs completely at random is unacceptable, in my opinion. (And marking the bug \"RESOLVED\" as \"WORKSFORME\" doesn't help, either.)\n"
    author: "Dima"
  - subject: "Re: \"polishing for amaroK 1.4\""
    date: 2006-05-14
    body: "well, works fine for me :o)\nno problems with amarok 1.4 and xine.\n\nI had some xine errors with previous builds of amarok 1.4, but not with the current one."
    author: "AC"
  - subject: "small typo"
    date: 2006-05-14
    body: "Hi,\n\nthe following typo happened in today's digest:\n\"Dirk Mueller committed changes in /trunk/KDE/kdelibse:\"\n\nNote the 'e' at the end. This keeps the diff viewer from working correctly.\n\nRegards,\nErik"
    author: "Erik"
  - subject: "open source or free software?"
    date: 2006-05-14
    body: "When I visit the phonon (http://phonon.kde.org/)and solid (http://solid.kde.org/) web site I can read on the bottom:\n<KDE Icon> ...This is an open source project\n\nthen when I click on the KDE Icon (http://www.kde.org/) I get \"KDE is a powerful Free Software graphical desktop environment...\"\nIt would be great to have some kind of consistency don't you think?"
    author: "Patcito"
  - subject: "Re: open source or free software?"
    date: 2006-05-15
    body: "All free software is open source (no the other way around), so I fail to see an inconsitency."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: open source or free software?"
    date: 2006-05-15
    body: "Aren't a few things not Free as in GNU?  I think some of the artwork is released under more open (less \"sharing required\") licenses so that commercial apps can use them.  I may be wrong with KDE3, but I'm sure that at one point there were bits that were licensed under more \"public domain-ish\" licenses like Artistic or BSD."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: open source or free software?"
    date: 2006-05-15
    body: "Those licenses are definitely free software - read RMS' 4 freedoms again."
    author: "mikeyd"
  - subject: "Re: open source or free software?"
    date: 2006-05-16
    body: "No.  GNU won't even use the term Open Source.  They're different in common use, even if originally intended to mean the same thing.  Bruce Perens, who co-invented the term open source, has also asked people to stop using it."
    author: "Lee"
  - subject: "Thanks for the digest! :-)"
    date: 2006-05-15
    body: "Thanks Danny! I couldn't imagine all that good stuff going on if not reading your digest. Thanks a lot!!!!"
    author: "koral"
  - subject: "taglib?"
    date: 2006-05-15
    body: "From the amaroK release polishing: \"Tagging support for all major audio formats (OGG, MP3, MP4, FLAC and RM)\"\n\nIs amaroK using \"taglib\" or is that homebrew? \n"
    author: "Marco Krohn"
  - subject: "Re: taglib?"
    date: 2006-05-15
    body: "It's using taglib, which has added support for plugins. amarok handles mp4/mp4/aac, rm/rma and wma through homebrew taglib plugins."
    author: "ac"
  - subject: "Re: taglib?"
    date: 2006-05-15
    body: "thanks for the quick reply, that sounds wonderful. Also the new amaroK features sound great and look visually appealing - thanks a lot! "
    author: "Marco Krohn"
  - subject: "Wiki"
    date: 2006-05-15
    body: "Just visited the wiki pages of KOffice and amaroK. \n\nhttp://wiki.kde.org/tiki-index.php?page=koffice2roadmap\n\nhttp://amarok.kde.org/amarokwiki/index.php/What's_New_in_1.4\n\nCan't KOffice wiki not use the same beautiful style as Amarok? I know that both projects use different wiki software (amarok seems to use mediawiki, which is the base for wikepedia) and KOffice (tiki?). However, porting a couple of pages (i.e. using a different syntax) is not too hard and IMHO worth it (in this case count me in as a helping hand).\n\nBTW thanks Danny for the KDE commit digest and Inge for his PR for Koffice!"
    author: "Marco Krohn"
  - subject: "Re: Wiki"
    date: 2006-05-15
    body: "well koffice uses the kde wiki which is indeed a tikiwiki. And I don't think there will be a change to mediawiki in the future, the admin of the kde's wiki is also a tikiwiki dev ;)"
    author: "Cyrille Berger"
  - subject: "Re: Wiki"
    date: 2006-05-15
    body: "Like the amarok guys koffice could use any other wiki ... and setting up a new mediawiki is pretty easy these days (though making it as nice as the amarok's one will probably take a bit longer). "
    author: "MK"
  - subject: "Re: Wiki"
    date: 2006-05-15
    body: "Sure we could. But this is just a tool for developers to coordinate. There's no reason to prettify it, and hence no reason to spend even a little time on changing wiki's."
    author: "Boudewijn Rempt"
  - subject: "Re: Wiki"
    date: 2006-05-15
    body: "The request to switch KDE to mediawiki has been brought up many times. Basically, it's unlikely to happen. Personally, I prefer mediawiki since the syntax is cleaner and it tends to look prettier, but the current maintainer of the Kde-Wiki, Luci, is also a tikiwiki dev so the odds of switching are slim."
    author: "ac"
  - subject: "SVG Icon"
    date: 2006-05-15
    body: "Does this mean we can use SMIL Animations in Icons for KDE 4?  :-)"
    author: "am"
  - subject: "Re: SVG Icon"
    date: 2006-05-16
    body: "That would be horrible :)  Animations for selected icons might be nice, but I'd hate to see the all my file icons animating!  I guess it could be done well, but the coordination it would take, in terms of timing the animations and matching colors etc., for a whole (unfixed) set of icons that can be added to by third parties would be pretty tough to get right."
    author: "Lee"
  - subject: "Sorting bug"
    date: 2006-05-15
    body: "I have a problem with the Kfiledialogue which probably relates to the file sorting algorith.\n\nThe Cursors \"jumps\". It is an old bug and it esp. occurs when I have folders which long filenames e.g. podcasts.\n\nI thought it was fixed because it is so obvious but I just installed KDE 3.5.1 for SuSe 10.1 and I wondered why it is still there. So I ask you, was this bug fixed in a later version or is it still in? \n"
    author: "Andre"
  - subject: "Re: Sorting bug"
    date: 2006-05-16
    body: "I'm experiencing this also, using 3.5.2 on Suse 10.0."
    author: "me"
  - subject: "New HiColor icons?"
    date: 2006-05-15
    body: "I get a 404 when trying to view my new icons :-( when I try to view them here:\n\nhttp://commit-digest.org/issues/2006-05-14/moreinfo/538392/#visual\n\n"
    author: "James Richard Tyrer"
  - subject: "Re: New HiColor icons?"
    date: 2006-05-16
    body: "Correct, the directory 'pics' does not exist in SVN"
    author: "AC"
  - subject: "Re: New HiColor icons?"
    date: 2006-05-17
    body: "That is not the way SVN works!\n\nThe directory exists at Revision 538392, and it is still there.\n\nIf you doubt this, please look:\n\nhttp://websvn.kde.org/branches/KDE/3.5/kdeartwork/pics/?rev=538392\n\nWhat I am pointing out is that there is a bug somewhere:\n\nhttp://websvn.kde.org/branches/KDE/3.5/kdeartwork/pics/hicolor/hi22-action-tab_breakoff.png?r1=538391&view=markup\n\nGives 404.  But, you get the image if you use the correct URLs.  E.G.:\n\nhttp://websvn.kde.org/branches/KDE/3.5/kdeartwork/pics/hicolor/hi22-action-tab_breakoff.png?rev=538392&view=markup\n\n"
    author: "James Richard Tyrer"
---
In <a href="http://commit-digest.org/issues/2006-05-14/">this week's KDE Commit-Digest</a>: release polishing for <a href="http://amarok.kde.org/">amaroK</a> 1.4. New sounds for <a href="http://opensource.bureau-cornavin.com/ktuberling/">KTuberling</a>. KDE 4 changes include the <a href="http://commit-digest.org/issues/2006-04-30/#introduction">proposed kdepimlibs module</a> is created. New SVG icon engine based on QsvgEngine. New capabilities added to <a href="http://solid.kde.org/">Solid</a>. Applications with simple audio needs start to migrate to <a href="http://phonon.kde.org/">Phonon</a>.


<!--break-->
