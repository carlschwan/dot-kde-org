---
title: "Qt Jambi Developer Contest Announced"
date:    2006-11-09
authors:
  - "eblomfeldt"
slug:    qt-jambi-developer-contest-announced
comments:
  - subject: "Qt app in Java? great idea to expends Qt market."
    date: 2006-11-09
    body: "Now, can I write Qt applications in J2ME? Because the only reason I have to learn and write Java as of now. It's because of cellphones. If I could use Qt on J2ME that be the greatest news in years.\n\n"
    author: "somekool"
  - subject: "Re: Qt app in Java? great idea to expends Qt market."
    date: 2006-11-11
    body: "Even if there is a Qt J2ME edition (dunno), you would have to require your users to install it in their cellphones because it isn't bundled in any. This probably kills your use for it, no?\nAnyway, I find the idea of such a featurefull GUI toolkit like Qt for J2ME silly."
    author: "blacksheep"
  - subject: "Re: Qt app in Java? great idea to expends Qt market."
    date: 2006-11-12
    body: "Well, not on devices based on Qtopia. There Jambi would allow access to the platform's native GUI."
    author: "Kevin Krammer"
  - subject: "Irrelevant"
    date: 2006-11-09
    body: "I think the various distro news are borderline (I guess there's a fair amount of KDE folks who are interested in anew Kubuntu release or something), but this? What the heck has Qt Jambi that's interesting to a sufficiently large part of the readers here to be worth being posted?\n"
    author: "Frerich"
  - subject: "Re: Irrelevant"
    date: 2006-11-09
    body: "a high quality officially supported set of bindings to the toolkit we use isn't relevant?\n\nwell, i suppose if you aren't a developer or someone interested in technical advances then it's not interesting."
    author: "Aaron J. Seigo"
  - subject: "Re: Irrelevant"
    date: 2006-11-10
    body: "> a high quality officially supported set of bindings to the toolkit we use isn't relevant?\n\nI cannot see why and how bindings to Java are interesting. What is the motivation for a customer to buy Qt licenses (the licensing model would be very interesting to see) instead of using existing Java stuff (given that SWT kicks ass). J2EE is out of scope for Qt. What market is this intended for? Is the feeling that Java developers would choose Qt for just the API? Please enlighten me."
    author: "missingthepoint"
  - subject: "Re: Irrelevant"
    date: 2006-11-10
    body: "> I cannot see why and how bindings to Java are interesting\n\nthe same reason any bindings are interesting: people prefer that language for a specific task and would like to use the superior Qt API particularly if they are familiar with it already.\n\nwould you be pissing and moaning if this were python or ruby bindings for Qt? there are other desktop toolkit options for python and ruby after all.\n\n> given that SWT kicks ass\n\nand yet i keep hearing from people who work with java how much it doesn't kick ass. *shrug*\n\n> What market is this intended for?\n\ngood example is a company i know in the city i live in: they write c++ apps using Qt for their imagery work which is a core part of their business. they also have some people working in java on technologies that interface with the web; there is a bunch of servlet code and there is also some client side gui code. they're not overly happy with what they get with java for client side computing and are very familiar with Qt's api. for them it makes a lot of sense to look into this as they get \"the best of both worlds\" in their opinion: the quality and clarity of Qt and the language of choice."
    author: "Aaron J. Seigo"
  - subject: "Re: Irrelevant"
    date: 2006-11-11
    body: "> Is the feeling that Java developers would choose Qt for just the API?\n\nYes.\n\nThis is about bringing the Qt API to a wider audience. In that sense it's of less interest to us because we already know how good the Qt API is. But it's always nice to know that the word is being spread.\n\nIt's not about changing the way existing users of the Qt API use it. While I guess there will be some people wanting to move from C++ to Java who now have one less barrier in their way, those people fortunate enough to be programming in Python or Ruby aren't going to throw away all that productivity by reverting to Java.\n\n"
    author: "Phil Thompson"
  - subject: "it's not about Jamba"
    date: 2006-11-09
    body: "it's about Jambi ....\n...dear"
    author: "Thomas"
  - subject: "Re: Irrelevant"
    date: 2006-11-10
    body: "Creating a new binding for Qt can reveal still hidden bugs or design flaws (although i suppose there arn't that many left)."
    author: "AJ"
  - subject: "What about Kde Jambi ? :)"
    date: 2006-11-09
    body: "Before somebody says \"C++ > Java\" or \"Java < foo\" or even \"java == evil Sun proprietary software\"; I'd like to say that Qt Jambi is a good thing. \n\nSun is about to release the jdk 6 with an open source license (and it may be GPL). It will support several script languages including groovy, jruby, javascript, python and sleep (see https://scripting.dev.java.net/ ), and it is quite fast (yes, I know it's not as fast as c++).\n\nThe next step is to make \"Kde Jambi\" (based on Qt Jambi, since it will be open source), and with only ONE kde bindings, it will be possible to use several languages. By the way, jruby + \"Kde Jambi\" might be another approach to Korundum.\n\nI hope the Kde-bindings team will study this idea :)"
    author: "shamaz"
  - subject: "Re: What about Kde Jambi ? :)"
    date: 2006-11-10
    body: "does it work with Gnu Classpath"
    author: "ben"
  - subject: "Re: What about Kde Jambi ? :)"
    date: 2006-11-10
    body: "> does it work with Gnu Classpath\n\nCertainly, that's the whole point of writing Java bindings ;-)\n\nOfftopic: there are seemingly credible rumors that Sun's JDK is going to be GPLed very soon now, which means the Sun's Java will more than likely switch to the same licensing model as Qt and MySQL."
    author: "Iron Gato"
  - subject: "Re: What about Kde Jambi ? :)"
    date: 2006-11-11
    body: ">Offtopic: there are seemingly credible rumors that Sun's JDK is going to be GPLed very soon now, which means the Sun's Java will more than likely switch to the same licensing model as Qt and MySQL.\n\nYou mean people will need to pay 3000 bucks to Sun to write proprietary apps?"
    author: "Patcito"
  - subject: "Re: What about Kde Jambi ? :)"
    date: 2006-11-11
    body: "We don't know that yet."
    author: "ok"
  - subject: "Re: What about Kde Jambi ? :)"
    date: 2006-11-11
    body: "> You mean people will need to pay 3000 bucks to Sun \n> to write proprietary apps?\n\nNo, it's actually much more clevere, they will need to pay 3000 bucks to ME in order to write proprietary apps.\n\nAlthough I admit it's quite shameful to expect money from people who expect money from people."
    author: "Iron Gato"
  - subject: "Re: What about Kde Jambi ? :)"
    date: 2006-11-11
    body: "\"The next step is to make \"Kde Jambi\" (based on Qt Jambi, since it will be open source), and with only ONE kde bindings, it will be possible to use several languages. By the way, jruby + \"Kde Jambi\" might be another approach to Korundum.\"\n\nYes, once Sun's language gets a Free Software license I hope it encourages more languages to be used on the platform. Of course the Qyoto/Kimono C# bindings for Mono will also allow some interesting experiments with IronPython, Ruby/CLR, Nermerle and other CLR based languages."
    author: "Richard Dale"
  - subject: "Re: What about Kde Jambi ? :)"
    date: 2006-11-13
    body: "Seems like we will know about the license today :)\nhttp://www.sun.com/2006-1113/feature/index.jsp"
    author: "shamaz"
  - subject: "Re: What about Kde Jambi ? :)"
    date: 2006-11-13
    body: "It' now official !!!! \\o/\nhttp://www.sun.com/2006-1113/feature/story.jsp\nJava is GPLv2. This is a great day :) (at least for me)"
    author: "shamaz"
  - subject: "Re: What about Kde Jambi ? :)"
    date: 2006-11-13
    body: "Yep, quite a pleasant shock. I wish we had more like that."
    author: "Iron Gato"
  - subject: "License?"
    date: 2006-11-13
    body: "I can't find an info on the Trolltech site about which license(s) will be used to publish Jambi when it is finished. Can I assume it will be the same deal  as Qt?\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Re: License?"
    date: 2006-11-14
    body: "There will be an open source version of Qt Jambi.\nhttp://www.trolltech.com/developer/knowledgebase/faq.2006-07-21.1262524505/"
    author: "shamaz"
---
Trolltech has <a href="http://www.trolltech.com/company/newsroom/announcements/press.2006-10-24.8142844618">announced the Qt Jambi Developer Contest</a>, which is now open to all developers following the release of the third Technology Preview (TP) of Qt Jambi. The contest is aimed at encouraging both Java and Qt programmers to try out the new features available in the Qt Jambi TP3. This third and final technology preview is built on the newly-released Qt 4.2, giving Java programmers access to powerful new Qt features like the powerful 2D graphics canvas (Qt Graphics View) and simplified application styling through Widget Stylesheets.



<!--break-->
<p>
To enter, developers are asked to create a Qt Jambi-based demo application and submit their entry via. email to qtjambi-contest@trolltech.com. Entries will be accepted until the 15th December 2006 deadline, and one winner will receive a 2.0GHz Apple MacBook®.  
</p>
<p>
For more details and submission guidelines, please visit <a href="http://www.trolltech.com/developer/downloads/qt/qtjambi-techpreview">the Qt Jambi Technology Preview Page</a>. Good luck!
</p>






