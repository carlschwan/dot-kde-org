---
title: "KDE and Distributions: Red Flag Interview"
date:    2006-07-11
authors:
  - "wolson"
slug:    kde-and-distributions-red-flag-interview
comments:
  - subject: "So much for the \"interview\""
    date: 2006-07-11
    body: "Not to be too harsh, but could we get a real interview?\n\nI mean, look at these answers:\n\nWhat are you most looking forward to about the 4.0 release?\n\nHJ: A completely new and exciting user experience. \n\nDo you plan any involvement in the beta/RC releases of the 4.0 release?\n\nHJ: We hope so. We will do more bugfixes for KDE 4. \n\nAny other plans for your distro in the future?\n\nHJ: We have plans to implement some new features for the KDE 4.0 series, so we have some new ideas.\n\nWell, what are these new features? What is your internal development structure like? Is the current internationalization provided by KDE good-enough? Do you contribute your own i18n efforts back?\n\n"
    author: "Me"
  - subject: "Re: So much for the \"interview\""
    date: 2006-07-11
    body: "Point well taken.  \n\nInterviewees are in fact prompted to be verbose during the interview process.\n\nPlease keep in mind that I purposefully ask similar questions to allow the personalities of the interviewees and their respective distros to show.  \n\nLanguage barriers and fluency may vary between distros and regions.  Also culturally, some interviewees may be more gregarious and some more guarded.\n\nFinally, Red Flag is a new distro that we're trying to establish a better relationship with, and we're just laying the foundation now.  This interview is just one element of this process; don't expect Red Flag to tell their life story - we're just getting to know each other. :)\n\nBut many have asked to learn more about Red Flag, as they're incredibly prominent in a flourishing region.  Rumor has it :) that they will be in attendance at aKademy, so if you're there, you have every chance to learn more and introduce yourself.\n\nThanks for the feedback.  With some of the next distros in line for this interview series, you may see some more detailed answers to your liking. \n"
    author: "Wade"
  - subject: "Re: So much for the \"interview\""
    date: 2006-07-11
    body: "It's like talking to a joker :( This joker's depth of knowledge in Linux/KDE seems very very shallow."
    author: "AC"
  - subject: "Re: So much for the \"interview\""
    date: 2006-07-11
    body: "Huang JianZhong is a very approachable person.  If people have specific technical questions to ask about Red Flag, we can certainly ask here in the forum, or we can alternatively schedule a follow-up technical interview.\n\nLike 'Ask Slashdot' style - what do people want to know?  For example, some i18n questions were asked above.  \n\nDepending on question universality or applicability, a technical section can be added to my interviews.  Although these *are* purposefully meant to be a bit more introductory and light-hearted.  Maybe we'll just end up posting all their code to satisfy everyone. :)"
    author: "Wade"
  - subject: "Re: So much for the \"interview\""
    date: 2006-07-11
    body: "I am intrrested whether there is a demand for Linux internet cafe solutions in China and what software for administration they would recommend."
    author: "gerd"
  - subject: "Re: So much for the \"interview\""
    date: 2006-07-11
    body: "We are trying.\n\nAs you know, there is some difficult to communicate between us, for example, the different language, the different custom.\n\nAs in DAM2 meeting said, We need change this&#12290;\n\nabout new features, for example, we are developing a Network Management Framework based on kde(not similar with NetworkManager),  Tag support for Konqueror File Management, A set of config tools under Kcontrol framework, also some gui improvement(for example, I post something in kde-look.org and kde-apps.org)\n \nat first, I think we need know each other.\n\n"
    author: "cjacker"
  - subject: "Re: So much for the \"interview\""
    date: 2006-07-11
    body: "and,\n\nXGL kwin integration for (kde-3.5.x)\n\nA ShareDaemon(based on dbus) and KDE gui support for easy shareing with folders and printers.\n\nBetter Chinese support for Qt4 (for example bold chinese font, underline issue), after Freetype-2.1.10, there is FT_GlyphSlot_Embolden Api, But it can not works very well with non-antialias Chinese font (cario and libXft also had these problems). So here we need some works.\n\nKaffeine javascript interface for embeded in konqueror.\n\n......"
    author: "cjacker"
  - subject: "Re: So much for the \"interview\""
    date: 2006-07-11
    body: "also, we had done something with gecko engine and konqueror integration.\n\nWith qt glib main event loop patch, We can merge gtk and qt widget together, with xembed protocol, we can use gtk widget directly in a qt form.\n\nso we write a kpart named Kmozpart based on gecko engine and make it works with Konqueor.\n\nBut it died, because KDE 4 will release this year, I think based on kde4 is a better choice.\n\n"
    author: "cjacker"
  - subject: "Re: So much for the \"interview\""
    date: 2006-07-11
    body: "Thank you Huang JianZhong, it's great to hear more about what Red Flag is doing with KDE. I look forward to meet you later this year in Dublin at aKademy."
    author: "Waldo Bastian"
  - subject: "File sharing with KIO-FUSE"
    date: 2006-07-11
    body: "> A ShareDaemon(based on dbus) and KDE gui\n> support for easy shareing with folders\n\nHave you considered basing the file-sharing solution on KIO-FUSE?\n\nhttp://wiki.kde.org/tiki-index.php?page=KIO+Fuse+Gateway\nhttp://fuse.sourceforge.net/\n\nHere are some details about the benefits of using FUSE (Filesystem in Userspace):\n\nhttp://rlove.org/talks/guadec_rlove_fuse_2006.odp\nhttp://lists.freedesktop.org/archives/portland/2006-July/000655.html"
    author: "Vlad"
  - subject: "Re: So much for the \"interview\""
    date: 2006-07-11
    body: "XGL-Kwin would be great, is there somewhere we can check to keep track of progress?\n\nA distro that ships so many copies could be a great source of new features and testing.  I'd also be excited if a bit of manpower could be thrown at some of the more popular (unpopular?) bugs in KDE's bugzilla."
    author: "MamiyaOtaru"
  - subject: "Re: So much for the \"interview\""
    date: 2006-07-11
    body: "You might consider checking out Codeine as a simple alternative to Kaffeine, it also has a KPart. http://kde-apps.org/content/show.php?content=17161\n\nIts great to hear about all the great work your doing. I would be very interested in check out kwin+XGL. :)"
    author: "Ian Monroe"
  - subject: "Re: So much for the \"interview\""
    date: 2006-07-14
    body: "Everthing you mentioned are worthless until they are fully contributed back to community."
    author: "James Su"
  - subject: "Re: So much for the \"interview\""
    date: 2006-07-11
    body: "\"Tag support for Konqueror File Management\"\nsweet! sounds like they may be working on one of my wish list items [ http://bugs.kde.org/show_bug.cgi?id=113636 ].  I hope this finds its way back to core."
    author: "Jonathan Dietrich"
  - subject: "Re: So much for the \"interview\""
    date: 2006-07-11
    body: "Become informed culture wise.\n\nSome stereotypes are true.  Asians are compulsively polite.  \n\nAbsolutely nothing wrong with that, it is just their culture and we should understand it."
    author: "James Richard Tyrer"
  - subject: "Childish !!!"
    date: 2006-07-11
    body: "OMG, Is this the level of a Linux distro maintainer? \n\nWade, why don't you interview Tomahawk Desktop developers? Tomahawk is also from Asia and looks like they are doing something."
    author: "Paul"
  - subject: "Re: Childish !!!"
    date: 2006-07-11
    body: "Sorry for that, But I think this is a dist interview, not a technical interview:-D\n\n"
    author: "cjacker"
  - subject: "Re: Childish !!!"
    date: 2006-07-11
    body: "Indeed. I don't think it was that bad or something, just short. Nobody would read a 10-page interview anyway ;-)\n\nThanks for the work you do on KDE, guys! The interview might be short, but the work is appreciated ;-)"
    author: "superstoned"
  - subject: "Re: Childish !!!"
    date: 2006-07-11
    body: "I'm sure Tomahawk is doing great work as well but Red Flag is much more significant, being the most popular Linux distribution in China. See also http://www.eweek.com/article2/0,1895,1900214,00.asp"
    author: "Waldo Bastian"
  - subject: "Re: Childish !!!"
    date: 2006-07-11
    body: "Huang, please ignore the notoriously lamenting Anonymous Coward fraction on this site. It's not representative of KDE for sure. These people are, after all, cowards for a reason.\n\nI enjoyed reading about Red Flag Linux. Thank you.\n\n\nPS: I think this comic illustrates very well what happens with people on the internet:\n\nhttp://www.penny-arcade.com/comic/2004/03/19\n\n"
    author: "Mark Kretschmann"
  - subject: "Re: Childish !!!"
    date: 2006-07-11
    body: "zOMG PONIES"
    author: "Ian Monroe"
  - subject: "Re: Childish !!!"
    date: 2006-07-11
    body: "\"Wade, why don't you interview Tomahawk Desktop developers? Tomahawk is also from Asia and looks like they are doing something.\"\n\nOh please..... It's nice to hear about some people, who you don't normally hear of, doing something with KDE. They're not going to come in with all sorts of ideas about this that and the other straight away, and then there's the language barrier as well.\n\nI thought this was a nice introduction to let us know who they are and that they're using KDE to do good things. In the future there may be more interviews with more detail in there, but this was more of an introduction. It's important for KDE to reach out to users and potential contributors in this way for them to feel comfortable with KDE and vice versa."
    author: "Segedunum"
  - subject: "Childish !!!"
    date: 2006-07-13
    body: "OMG, paul is a dick!!!11!1\n\nIs this the level of a person who has nothing to offer but comments anyway?"
    author: "Max Howell"
  - subject: "More technical information can be found here"
    date: 2006-07-11
    body: "http://www.linux-ren.org/modules/newbb/viewforum.php?forum=21\n\nBut, it only has chinese version.\n\n"
    author: "cjacker"
  - subject: "A distribution to watch"
    date: 2006-07-11
    body: "I think it would be great if we could get to know each other better (at all?). The western world is mostly disconnected from the asian world. From the comments of cjacker, Red Flag is doing a lot of KDE related work. It is a shame that we didn't know that untill now.\n\nWith their commitment to KDE, their incredible Chinese market share, and the amazing potencial size of the chinese market, Red Flag Linux is surely a distribution to watch!\n\nGreetings"
    author: "Miq"
  - subject: "Re: A distribution to watch"
    date: 2006-07-11
    body: "I think that a more exciting and looking promising Linux distro is definitely Sabayon Linux at http://www.lxnaydesign.net ;)\n\njust my 2 cents"
    author: "Paul"
  - subject: "Re: A distribution to watch"
    date: 2006-07-11
    body: "A little bird told me that you might be happy about the next interview in this case."
    author: "Wade Olson"
  - subject: "Re: A distribution to watch"
    date: 2006-07-13
    body: "> [...] their incredible Chinese market share [...]\n\nActually most of the GNU/Linux users here are _not_ using the Red Flag GNU/Linux. Only the government will buy a lot copy of it (but whether they actually _use_ it or not is a separate issue).\n\nThat distribution is heavily tainted & freedom-subtracted (as it contains a whole bunch of proprietary fonts, drivers and others), and you'll also face many trouble with installing other software (as there are no apt, yum or so).\n\nBut if they are honest to contribute to KDE, I think it's still a good thing. I just hope they are not attempting to keep KDE under their monopolic control (from cjacker: the only way to solve all the problems of GNU/Linux is to keep it under the monopolic control of a big company). J/K though :)\n\nWei Mingzhi alias Whistler\n"
    author: "Whistler"
  - subject: "Re: A distribution to watch"
    date: 2006-07-13
    body: "Oh and forgot to mention: there are actually _not_ many GNU/Linux users in China. Everyone is using unauthorized copies of Microsoft Windows, except free software zealots like me :)"
    author: "Whistler"
  - subject: "Re: A distribution to watch"
    date: 2006-07-13
    body: "I want to clarify that it's absolutely wrong for you to say that everyone in China is using unauthorized copies of MS windows. On what ground do you say so? \n\nYou know why some of the Chinese users are using the distributions other than Redflag Linux? Coz they think that the moon abroad is much brighter than the one in China.\n\nAbout the above statistics of market share, you can get it from IDC.\n\nat last,As Chinese, I am shame on you.\n\n\n\n\n\n"
    author: "cjacker"
  - subject: "Re: A distribution to watch"
    date: 2006-07-14
    body: "> You know why some of the Chinese users are using the distributions other\n> than Redflag Linux? Coz they think that the moon abroad is much brighter\n> than the one in China.\n\nLOL, this is interesting: everyone who uses GNU/Linux but don't use yours are traitors. Doesn't you mean this?\n\nI use Debian GNU/Linux because it isn't freedom-subtracted, and it's better... uh, more suitable for me. If you can give a distribution which is better than this one, I'll use yours too. But you don't, so no way.\n\n> About the above statistics of market share, you can get it from IDC.\n\nI don't think any statistics can have unauthorized copies count. So that is the case.\n\nAnd I don't trust them either."
    author: "Whistler"
  - subject: "Re: A distribution to watch"
    date: 2006-07-13
    body: "I want to clarify that it's absolutely wrong for you to say that everyone in China is using unauthorized copies of MS windows. On what ground do you say so? \n\nYou know why some of the Chinese users are using the distributions other than Redflag Linux? Coz they think that the moon abroad is much brighter than the one in China.\n\nAbout the above statistics of market share, you can get it from IDC.\n\nAt last, as Chinese, shame on you.\n\n\n\n\n\n"
    author: "cjacker"
  - subject: "Re: A distribution to watch"
    date: 2006-07-14
    body: "I could have added an \"almost\" before \"everyone\". Sorry for miswording.\n\nBut you have to admit the problems. It's not about nationalism, it's about being honest.\n\nAs for what you have done for free software, I would like to thank for you contribution. It's true that I have contributed less than you (but not none!), but that doesn't mean I have to thank you for _everything_.\n\nWhistler\n\nPS: please stop insulting me at linuxfans.org, as I did not insult you.\n"
    author: "Whistler"
  - subject: "Re: A distribution to watch"
    date: 2006-07-14
    body: "OK, as someone may misread this, just a clarification:\n\nI don't say that I'm in any way \"different\" from the Chinese, as I'm Chinese too. I just mean that there is a problem, and we should face it instead of fooling others as well as ourselves. And please don't try to find other things in my bad wording.\n\nI hope this is the end of the thread.\n\nThanks\nWhistler\n"
    author: "Whistler"
  - subject: "Re: A distribution to watch"
    date: 2006-07-14
    body: "\"I just hope they are not attempting to keep KDE under their monopolic control (from cjacker: the only way to solve all the problems of GNU/Linux is to keep it under the monopolic control of a big company). J/K though :)\"\nyes, this is my sign in linuxfans.org, you also forgot that I said, any company is ok.\n\nI works with Linux for 9 years, also works for FreeBSD for 2 years.\n\nI know where we had troubles. I know why users can not use linux easily.\n\nYou know nothing about this.\n\nI remember what I said, I also take responsibility for what I said.\n\ndo you know what is ABI compatibility?\ndo you know the troubles that ISV faces?\n\n.....\nYou just stay home, play linux, and think yourself is so cool. \n\nWhat had you done for Linux?\n\n\nanyway, you can say anything you like.\n\nBut remember, \"Love your motherland, Do something for your motherland, if you think something is not so good, take effort to change your motherland, this is  the difference between people and animal.\"\n\n\n\n"
    author: "cjacker"
  - subject: "Re: A distribution to watch"
    date: 2006-07-14
    body: "Please don't insult me like this. I _do_ know about these things.\n\nEspecially, don't assume everyone who disagrees with you are all idiots who just \"play linux, and think yourself is so cool\". I don't \"play (with)\" GNU/Linux, and I don't think myself if \"cool\" to use GNU/Linux either.\n\nThanks"
    author: "Whistler"
  - subject: "Re: A distribution to watch"
    date: 2006-07-14
    body: "> But remember, \"Love your motherland, Do something for your motherland, if you think something is not so good, take effort to change your motherland, this is the difference between people and animal.\"\n\nOh and I forgot to tell you: I do love my nation, and I won't tolerate someone saying something like \"all Chinese are ****\". But that doesn't me I have to deny all of the problems in this nation."
    author: "Whistler"
  - subject: "Re: A distribution to watch"
    date: 2006-07-14
    body: "Don't you think it's very ridiculous arguing such silly things on a public international technical forum?\n\nPlease just shut up and back to code.\nNothing is important than your contribution back to Free and Open Source Software  community."
    author: "James Su"
  - subject: "Exciting!"
    date: 2006-07-11
    body: "This the first real info I've seen about this potentially enormously important distro. A breakthrough in China, however small, would mean the input from lots of talented people.\n\nLots of kudos to Huang JianZhong and his company - his follow-up comments indicates that there are a lot of interesting stuff already in the pipeline.\n\nG\u00f6ran J\nSweden"
    author: "G\u00f6ran Jartin"
  - subject: "Red Flag"
    date: 2006-07-11
    body: "What interests me most is.\n\na) How big is the market for exile Chinese?\n\nb) Are chinese developers skilled enough?"
    author: "pentamax"
  - subject: "Re: Red Flag"
    date: 2006-07-11
    body: "I assume you mean emigrant Chinese. :P"
    author: "Ian Monroe"
  - subject: "Re: Red Flag"
    date: 2006-07-11
    body: "Or perhaps \"expatriate\"."
    author: "Paul Eggleton"
  - subject: "Gecko for Konqueror?"
    date: 2006-07-11
    body: "I know people still ask for this, but I hope most energy continues to go into KHTML. It's fantastic -- very _very_ fast, secure and standards compliant.\n\nWorks with almost everything."
    author: "Chase Venters"
  - subject: "Re: Gecko for Konqueror?"
    date: 2006-07-13
    body: "+1, but i miss ctrl+z opera function (undo last tab close) and ability to show only cached images / load uncached images for current tab (found in opera7)"
    author: "Nick"
---
<a href="http://www.redflag-linux.com/eindex.html">Red Flag</a> Desktop Linux is the leading <a href="http://distrowatch.com/table.php?distribution=redflag">distribution</a> in China and surrounding regions. Its goal is to provide the most professional desktop product available. It has more than an 80% desktop share in the Chinese linux market, and over one million copies are shipped each year with KDE as its only desktop environment.  Huang JianZhong, a Senior Manager in the Desktop Product R&D Department of Red Flag, speaks below about the history of Red Flag Linux and their relationship with KDE.  In 2006, Red Flag Linux has been visible by joining the <a href="http://www.osdl.org/">Open Source Development Labs</a> and their
<a href="http://www.asianux.com/announcement_redflag.php">ongoing work</a> with
<a href="http://en.wikipedia.org/wiki/Asianux">Asianux</a>.


<!--break-->
<div style="float: right; border: thin solid grey; padding: 1ex; margin: 1ex">
<img src="http://static.kdenews.org/jr/redflag.gif" width="72" height="49" />
</div>

<h2>Past</h2>

<strong>Can you tell us about the history of your distribution? (How/when/why)</strong>
<p>
<strong>Huang JianZhong:</strong>
Red Flag Linux was launched in 1999 by the
<a href="http://www.ios.ac.cn/english/index_english.htm">Institute of Software</a>,
<a href="http://english.cas.cn/Eng2003/page/home.asp">China Academy of Science (CAS)</a>.
We hoped to promote Linux in China.
</p>

<p><strong>Why did you choose KDE and which version of KDE did you first implement?</strong></p>
<p>
<strong>HJ:</strong>
At that time, KDE was more stable than GNOME. The KDE version was 1.1.1.
</p>

<p><strong>How did you find initial support for a new distro?</strong></p>
<p>
<strong>HJ:</strong>
We got some support from DEC China and Founder China.  Of course, the
biggest support was from Institute of Software,
<a href="http://en.wikipedia.org/wiki/Chinese_Academy_of_Sciences">CAS</a>.
</p>

<p><strong>What could KDE have done better to help new distros use KDE?</strong></p>
<p>
<strong>HJ:</strong>
More flexible modular packages and performance improvements. Also, a
reduction of memory footprint.
</p>

<p><strong>What were your first impressions about KDE's documentation and community?</strong></p>
<p>
<strong>HJ:</strong>
Great and complete, but no Chinese version.
</p>

<h2>Present</h2>

<p><strong>How closely do your releases depend on KDE releases?</strong></p>
<p>
<strong>HJ:</strong>
Historically, we wanted to keep the API of our release as stable as possible,
So typically we kept the version of KDE unchanged for long periods of time.  We
would then backport some APIs and features from the newest KDE release.

Now, we have decided to follow the official release because we find that the
newest version of KDE is stable enough and fixes more bugs than our work.
Also, it always provides many new features desired by our customers.
</p>

<p><strong>Do you have a clear target audience for your distro?</strong></p>
<p>
<strong>HJ:</strong>
All Chinese users who want to use Linux and KDE.
</p>

<p><strong>Do you have any user feedback mechanism? If so, what feedback do they have about KDE?</strong></p>
<p>
<strong>HJ:</strong>
Yes, a powerful service network, bugzilla and forums.
</p>
<p>
In general, the feedback from customers is good: they have a
comfortable desktop environment. More feedback focuses on
hardware drivers and compatibility. But, if we can make KDE faster, that
is great.
</p>

<p><strong>In what ways do you customise the version of KDE that ships with your distro?</strong></p>
<p>
<strong>HJ:</strong>
We never change the APIs and Core Libraries for compatibility reasons.  Most work
focuses on "ease of use" and bugfixes.  Also, we make an effort to work on GUI
improvements and application enhancements.
</p>

<p><strong>What are the biggest strengths of KDE for your distro?</strong></p>
<p>
<strong>HJ:</strong>
The interface is similar to (forgive me) Windows, so the commercial
users have no need to change their situation.  As for computer fans, they
can customize the desktop as what they want; KDE provides a great configuration system.
</p>

<p><strong>What are the biggest weaknesses?</strong></p>
<p>
<strong>HJ:</strong>
Applications such as a Firefox KDE version (see below).
</p>

<p><strong>What KDE applications are the most popular among your users?</strong></p>
<p>
<strong>HJ:</strong>
Kontact, Kopete, Amarok, K3b, Kaffeine and Konqueror as a file
manager (everybody use it).
</p>

<p><strong>Do you feel that you have a good relationship with the KDE community?</strong></p>
<p>
<strong>HJ:</strong>
I am not sure, but we are trying to contribute more to the KDE community.
</p>

<h2>Future</h2>

<p><strong>What feature would you as a distro maintainer like to see in KDE?</strong></p>
<p>
<strong>HJ:</strong>
Integration between Konqueror and Gecko engine.  Better KOffice ODF support and
interoperability with OO.o.  However, we hope for a pure KDE environment.
</p>


<p><strong>Is the extended 4.0 release cycle an issue for your distro?</strong></p>
<p>
<strong>HJ:</strong>
Umm...well, we have plans to adopt KDE 4 in our next release; so we are
waiting for it.
</p>

<p><strong>What are you most looking forward to about the 4.0 release?</strong></p>
<p>
<strong>HJ:</strong>
A completely new and exciting user experience.
</p>

<p><strong>Do you plan any involvement in the beta/RC releases of the 4.0 release?</strong></p>
<p>
<strong>HJ:</strong>
We hope so. We will do more bugfixes for KDE 4.
</p>

<p><strong>Any other plans for your distro in the future?</strong></p>
<p>
<strong>HJ:</strong>
We have plans to implement some new features for the KDE 4.0 series, so we have some new ideas.
</p>

