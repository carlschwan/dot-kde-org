---
title: "amaroK 1.4 Beta 1 Released"
date:    2006-02-15
authors:
  - "roKymotion"
slug:    amarok-14-beta-1-released
comments:
  - subject: "SVN Version"
    date: 2006-02-14
    body: "...or just use the svn-version and enjoy even more bugfixes :-)\n\nhttp://amarok.kde.org/amarokwiki/index.php/AmaroK-svn\n\nDon't forget to install some *-devel RPMs (or debs) if the compilation fails."
    author: "max"
  - subject: "a klik version !"
    date: 2006-02-15
    body: "i just discovered that klik works very well for the 1.4 beta! w00000t!!  ;-)\n\njust try  \n\n  klik://amarok-svn-nightly\n\n(first i was scared because it downloaded a shitload of .deb packages onto my suse-10.0 system. but then i was re-assured in the #klik channel that this is ok, and klik would convert the .debs into its own format [i.e. one single .cmg file that is easy to get rid of if the app doesnt work well]. but it __does__ work well, extremely well even! :)\n\nSee also http://amarok-svn-nightly.klik.atekon.de/ for more details."
    author: "anonymous coward"
  - subject: "running literally within seconds using klik"
    date: 2006-02-15
    body: "this is truly amazing. just went to the klik site, and had amarok 1.4 beta running within seconds, while still keeping suse's original stable amarok version as well. wish all betas were offered this way. "
    author: "marcj"
  - subject: "Re: running literally within seconds using klik"
    date: 2006-02-15
    body: "\"this is truly amazing [...] had amarok 1.4 beta running within seconds [...] wish all betas were offered this way.\"\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n<AOL>Yesssssssssss!</AOL>"
    author: "ac"
  - subject: "performance .... "
    date: 2006-02-15
    body: "Definitely amarok is the more featurefull player out there, and impressive how quick it became so. The only issue for me right now is that it takes lot's of ram and cpu. And being on a laptop isn't much convenient ... Let's hope with this relese things are going better performance/resource utilization wise."
    author: "doom"
  - subject: "Re: performance .... "
    date: 2006-02-15
    body: "You may find that having a large playlist will cause this, using the Random Mix dynamic mode gives you more power and (i find) a more pleasant listening experience than simply loading the entire collection and listening to that. Also, if you have a large collection (currently i believe this means anything above around a couple thousand tracks) using MySQL is the faster option :)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: performance .... "
    date: 2006-02-15
    body: "On my laptop amarok needs 0% - 1% of CPU.\nThat's very good!\n\nHowTo:\n\n* Select Xine as output plugin\n* Don't use any visualization plugin\n* Set the internal spectrum visualisation to 30 or 20 fps (or completely deactivate it)\n"
    author: "max"
  - subject: "Re: performance .... "
    date: 2006-02-15
    body: "That doesn't work for me, however, I find switching to arts engine will about quarter the CPU usage."
    author: "mikeyd"
  - subject: "Re: performance .... "
    date: 2006-02-15
    body: "\"featureful\""
    author: "libgnazi"
  - subject: "It is true?..."
    date: 2006-02-15
    body: "That now amaroK will have a dependency on libgpot? a gnome librarie?\n"
    author: "JoeSixPack"
  - subject: "Re: It is true?..."
    date: 2006-02-15
    body: "libgpot, yes. The gnome stoner's library. Share and enjoy!"
    author: "markey"
  - subject: "Re: It is true?..."
    date: 2006-02-15
    body: "Don't spread FUD markey! Its not gnome, its glib. A glib abstraction layer for my pots and pans is just what I needed. \n\n:P"
    author: "Ian Monroe"
  - subject: "Re: It is true?..."
    date: 2006-02-15
    body: "I means it was created by gnome developers for the use of GTK\n\nright?\n\nSo it is  technicly a gnome library\n\n"
    author: "JoeSixPack"
  - subject: "Re: It is true?..."
    date: 2006-02-15
    body: "And you are a troll."
    author: "Anonymous"
  - subject: "Re: It is true?..."
    date: 2006-02-15
    body: "Of course it's approaching insanity to suggest that a lib shouldn't be used just because it happens to be created by gnome developers, but if you really have a problem with it, just think like this: We're using their man-hours! They are programming a library, just to have it snatched up by us for our benefit - and the keep on developing it, the fools, not realizing that our apps benefit from it! Mohahaha...!!\n"
    author: "Apollo Creed"
  - subject: "Re: It is true?..."
    date: 2006-02-15
    body: "I'm a KDE fan, but Gnome devs are not fools, Sir :-)\n"
    author: "MM"
  - subject: "Re: It is true?..."
    date: 2006-02-15
    body: "Sort of the same way that TagLib is a KDE library, sure! That was developed for use with the KDE app JuK, but... that doesn't make it an actual KDE library - like for example libkio is :) Don't feed the troll, i know, but there you have it :)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: It is true?..."
    date: 2006-02-16
    body: "I means it was created by gnome developers for the use of GTK\n\nk-people being allergic to g-libraries is plain stupid.\n\nHow old are we?\n\nIf there is a good library around, you are stupid if you're not using it in case it makes sense!!!!!"
    author: "ac"
  - subject: "Re: It is true?..."
    date: 2006-02-16
    body: "I never said I was against it, read again.\n\nIt is naturall for you to atack people w/o understanding it first?\n\n"
    author: "JoeSixPack"
  - subject: "Re: It is true?..."
    date: 2006-02-16
    body: "Well, I am afraid, that answer to your question is just in Joe SixPack's statement :-)"
    author: "Matej Cepl"
  - subject: "Re: It is true?..."
    date: 2006-02-16
    body: "glib as I understand it is just a library adding some OOP-like features to C. Its used by GTK and Gnome but otherwise isn't related."
    author: "Ian Monroe"
  - subject: "Re: It is true?..."
    date: 2006-02-15
    body: "Erm, it already more-or-less depends on gstreamer."
    author: "mikeyd"
  - subject: "Re: It is true?..."
    date: 2006-02-15
    body: "No.\nAmarok will have a dependancy on libgpod only if you want the IPod management features. Because gtkpod is a GTK program does not mean libgpod is. But yes, it was created by Gnome devs.\nAs is glib, which is used by lots of framework libraries used by KDE."
    author: "Ookaze"
  - subject: "Extended Info?"
    date: 2006-02-15
    body: "What's this thing on the playlist tab supposed to be? It's always blank for me. (I'd bugreport but I don't know what it's actually meant to be doing)."
    author: "mikeyd"
  - subject: "Re: Extended Info?"
    date: 2006-02-15
    body: "It's not yet working for normal playlists, but try it with Podcasts."
    author: "markey"
  - subject: "Impressive"
    date: 2006-02-15
    body: "Amarok .. KDE's.. killer app. Looking forward to the stable release."
    author: "VisezTrance"
  - subject: "Re: Impressive"
    date: 2006-02-22
    body: "While I'm sure the parent is considered a troll, boy do I agree.\n\nEver since AmaroK ex-histed, I have always considered alternatives related to the fact that it was unstable. It can simply hang, crash or lose your playlist like a snap. Now allot of releases later, these are still major issues. How is this possible on multiple distributions and over such a long time, such trivial and obvious problems? That and how God aw-full slow changing simply tracks is.\n\nSorry to sound so critical, but I think features are nice, but a solid foundation would not hurt either. Reporting bugs is nice and all, but I have begun to seriously question the skills behind the project after this long a time. If I was part of the AmaroK team I would feel shame for the stability and speed part. Frankly I consider a book example of how unstable software is excusing itself. At the same time I detect that anyone who would dare criticize speed or stability is likely to be considered a traitor or something. Well, suck it. It's a great app, but what the hell are you guys doing on the stability and speed department?"
    author: "ac"
  - subject: "Re: Impressive"
    date: 2006-02-23
    body: "You were obviously not using the Xine backend. Any other backend turns amaroK into a bloody mess. Especially GStreamer."
    author: "SR"
  - subject: "Re: Impressive"
    date: 2006-03-12
    body: "\"Killer app\" really refers to a different kind of application that no other system has, I think.\n\nHowever, in the sense you mean it, KDE is full of \"killer apps\".  Akregator is the best RSS aggregator on any platform; KMail is the best email client; KOrganizer is more usable than iCal but with more features than Outlook's calendar, and combined with KArm, really makes light work of projects; Amarok is probably the best audio player on any platform; Konqueror has some issues and it probably isn't the most compatible, but in terms of speed and usability and increasing my productivity, it's easily the best browser; KPDF is fully featured and faster than Adobe's own PDF viewer; K3b is the best burning software on free desktops, competing with Nero on Windows, and easily crushing Nero for Linux; Adept (in its ubuntu incarnation) is easily the most powerful installation and software management system on any platform... I could go on.  Suffice to say, when the day comes that I give up Debian-based distros and KDE-based desktops, we'll be in a totally new generation of software."
    author: "Lee"
  - subject: "greatest player (music manager)"
    date: 2006-02-15
    body: "nice work - most under the hood \n\nthx for the greatest player on all Osses\n\nch"
    author: "ch"
  - subject: "Just like always, tunepimp kills it..."
    date: 2006-02-15
    body: "Both /usr/lib/tunepimp and /usr/local/lib/tunepimp are .4.x. One is .4.0 and the /usr/local/lib copy is .4.02\n\nDebian testing/sid\n\n\n/usr/local/include/tunepimp/tp_c.h: In member function `int\n   KTRMRequestHandler::startLookup(KTRMLookup*)':\n/usr/local/include/tunepimp/tp_c.h:645: error: too few arguments to function `\n   int tp_AddFile(void*, const char*, int)'\n/home/coolian/tmp/amarok-svn/amarok/src/ktrm.cpp:77: error: at this point in\n   file\n/home/coolian/tmp/amarok-svn/amarok/src/ktrm.cpp: In constructor `\n   KTRMRequestHandler::KTRMRequestHandler()':\n/home/coolian/tmp/amarok-svn/amarok/src/ktrm.cpp:137: error: `tp_SetUseUTF8'\n   undeclared (first use this function)\n/home/coolian/tmp/amarok-svn/amarok/src/ktrm.cpp:137: error: (Each undeclared\n   identifier is reported only once for each function it appears in.)\n/home/coolian/tmp/amarok-svn/amarok/src/ktrm.cpp:139: error: invalid conversion\n   from `void (*)(void*, void*, TPCallbackEnum, int)' to `void (*)(void*,\n   void*, TPCallbackEnum, int, TPFileStatus)'\n/home/coolian/tmp/amarok-svn/amarok/src/ktrm.cpp: In member function `virtual\n   void KTRMLookup::collision()':\n/home/coolian/tmp/amarok-svn/amarok/src/ktrm.cpp:612: error: base operand of\n   `->' has non-pointer type `artistresult_t'\n/home/coolian/tmp/amarok-svn/amarok/src/ktrm.cpp:613: error: base operand of\n   `->' has non-pointer type `albumresult_t'\n/home/coolian/tmp/amarok-svn/amarok/src/ktrm.cpp:614: error: base operand of\n   `->' has non-pointer type `albumresult_t'\nError creating ./amarok/src/ktrm.lo. Exit status 1.\n\nERROR: Compilation wasn't successful. amaroK was NOT installed/upgraded.\n"
    author: "Joe"
  - subject: "Re: Just like always, tunepimp kills it..."
    date: 2006-02-16
    body: "Use 0.3.x\n"
    author: "Dan"
  - subject: "Packages available not just for kubuntu"
    date: 2006-02-15
    body: "Despite the obvious bias of the author, there are 1.4 beta packages at least available for Mandriva 2006 and there is an e-build for Gentoo also.  Arch will have a PKGBUILD soon and I'm sure the SUSE aftermarket packagers are working on something."
    author: "oggb4mp3"
  - subject: "Lyrics tab"
    date: 2006-02-15
    body: "Could be worth to get rid off the lyrics tab and move it to a more appropriate position. Esp. in some non-english languages and on small screens the upper tab looks bad at first sight and \"lyrics\" is always bound to a song and hardly in use. "
    author: "gerd"
  - subject: "Re: Lyrics tab"
    date: 2006-02-15
    body: "You have any ideas of where it should go?\n\n3 tabs doesn't seem too onerous."
    author: "Ian Monroe"
  - subject: "Re: Lyrics tab"
    date: 2006-02-15
    body: "See mockup\n\nLyrics and the name of the songwriter as simple contextual links"
    author: "gerd"
  - subject: "Re: Lyrics tab"
    date: 2006-02-16
    body: "This proposal gets my official backing :). I would like to see tab-free Amarok. It would make the UI a lot less cluttered. And like the mockup showed, it could be done.\n\nRunning 1.4-SVN as we speak, and it works beautifully BTW :)."
    author: "Janne"
  - subject: "Re: Lyrics tab"
    date: 2006-02-16
    body: "I also support this suggestion.\nIt looks much better than now.\nI think that the two tabs: Lyrics and Artist can be integrated in a single one, like this mockup shows."
    author: "Josep"
  - subject: "Re: Lyrics tab"
    date: 2006-02-16
    body: "This in an interesting concept.\n\nMy only question/concern would be navigating after clicking on one of these URL's."
    author: "Dan"
  - subject: "Re: Lyrics tab"
    date: 2006-02-17
    body: "It can be implemented with a \"<- Back\" link, in the same way that AmaroK already does that with the Artist Related section if you have activated in the last.fm preferences.\nSee the screenshot below to see what I refer."
    author: "Josep"
  - subject: "Re: Lyrics tab"
    date: 2006-02-18
    body: "That does look good\n"
    author: "Danni Coy"
  - subject: "Re: Lyrics tab"
    date: 2006-02-22
    body: "Boa is the artist, Duvet is the song!"
    author: "Mario"
  - subject: "libgpod dependency is a good thing?"
    date: 2006-02-15
    body: "but a KDE dependency so that 'delete' will send the files to the trash instead of it being gone forever is a bad thing?\n\nYeah that makes loads of sense..."
    author: "Corbin"
  - subject: "Re: libgpod dependency is a good thing?"
    date: 2006-02-16
    body: "The dependency on libgpod is optional, if you want ipod support, you install libgpod.\n\nIT would be a lot harder to make a dependancy optional in order to send a file to the trash, although, I wonder how challenging it would be to do in a script, does kde have a dcop call for movetotrash? anyone know?"
    author: "Dan"
  - subject: "Re: libgpod dependency is a good thing?"
    date: 2006-02-17
    body: "Why should a dependency on libgpod be a bad thing?\nIt's a good library with only a few (normal) dependecies. What would be the benefit of reinventing the wheel?"
    author: "birdy"
  - subject: "new look for the current track marker..."
    date: 2006-02-15
    body: "imho the old look for the current track marker was better (more integrated in the UI...)\n\nif you think so... http://bugs.kde.org/show_bug.cgi?id=122065  :-)"
    author: "DanaKil"
  - subject: "Re: new look for the current track marker..."
    date: 2006-02-15
    body: "\"old look\" doesn't have any meaning. Its gone through a few iterations."
    author: "Ian Monroe"
---
Fresh from the amaroK development squad, comes  <a href="http://amarok.kde.org/content/view/69/66/" title="http://amarok.kde.org/content/view/69/66/">the first beta release of amaroK 1.4</a>, the 'Fast Forward' series. This generation has a lot of shiny new features and eye-candy. amaroK development has literally been in fast forward mode since November, the <a href="http://websvn.kde.org/trunk/extragear/multimedia/amarok/ChangeLog?rev=508559&amp;view=markup" title="http://websvn.kde.org/trunk/extragear/multimedia/amarok/ChangeLog?rev=508559&amp;view=markup">changelog</a> is actually one of the longest in project's history.  Read on for the new features.




<!--break-->
<div style="border: thin solid grey; width: 300px; margin: 1ex; padding: 1ex; float: right">
<img src="http://static.kdenews.org/jr/amarok-14.png" width="300" height="228" />
</div>

<h4>Hot New Features</h4>
<p>
Since work started on amaroK 1.4, several features have been repeatedly requested, via the amaroK forum, IRC and mailing lists, as being essential to amaroK users.
</p><p>
That said, amaroK now includes the ability to tag (and add to the collection) WMA, MP4/AAC and RealMedia (RA,RV,RM) files.
</p><p>
Another long-standing item from the wishlist is gapless playback, and this has finally been implemented, along with audio CD support (and CDDB lookups) for the popular xine engine.
</p><p>
By far the biggest change in amaroK 1.4 is the reworked media device system. Apple iPod support has been greatly improved (thanks to libgpod), and iRiver/ifp and generic media devices are now supported, although the latter is still a little unstable.
</p><p>
Along with these major changes, amaroK's user interface has been cleaned up and simplified, and much work has been done under the hood to improve performance and stability.
</p><p>
The team now asks everybody to hunt and squash bugs, for your own enjoyment as well as to make 1.4 even more stable than 1.3 already was. A <a href="http://amarok.kde.org/amarokwiki/index.php/1.4-Known_Issues">list of known issues</a> is available. 
</p><p>
<a href="http://kubuntu.org/announcements/amarok-1.4beta1.php">Packages are available for Kubuntu</a> or <a href="http://amarok.kde.org/amarokwiki/index.php/Download">grab the source</a> to compile it yourself.  For more information how to help and where to get amaroK 1.4 beta 1 see <a href="http://amarok.kde.org">the amaroK website</a>.</p>



