---
title: "Amarok Live Calling for Artwork"
date:    2006-08-07
authors:
  - "rteam"
slug:    amarok-live-calling-artwork
comments:
  - subject: "amarok live cd"
    date: 2006-08-08
    body: "Which are the requirements to get your music on the cd (except the license)? Are there any preferences for style, length or origin? Do you have to pay for it? It`s a rather nice promo so I guess there are strict rules."
    author: "orboruk"
  - subject: "Re: amarok live cd"
    date: 2006-08-08
    body: "Well it should be good music :) Just send a sample to us, and we'll check it out."
    author: "Mark Kretschmann"
  - subject: "Re: amarok live cd"
    date: 2006-08-11
    body: "It basically has to sound good (although this is highly subjective, so this means sounds good to us), be freely redistributable under something not very restrictive like the Creative Commons Attribution - No Derivatives license and be provided in a high quality format so we can do our own encoding for quality control purposes.  There is no need to pay as there is a mutual benefit;  the artist promotes Amarok at the same time Amarok promotes the artist.\n\n"
    author: "Greg Meyer"
  - subject: "Where's the V1.2 ISO ?"
    date: 2006-10-31
    body: "While the process of getting V1.4 ready for release goes on, there is (still) no ISO of any version available for download at http://amaroklive.com/ :-(\n\nDoes anyone know where an ISO of any Amarok Live version whatsoever may be downloaded from ?\n\nIs V1.2 still at http://amaroklive.com/ somewhere ?\n\n(I know 1.4 will be so much better, but I have an evangelising use for V1.2 *now*)\n"
    author: "Nick Boyce"
---
The <a href="http://amarok.kde.org">Amarok project</a> has announced an artwork contest for their upcoming live CD, Amarok Live, for fancy new version 1.4.  The contest includes among other things bootsplash screens, wallpapers and Amarok splash screens.  Also the Amarok Live team would like to have a complete set with similar style for all the items listed in the <a href="http://amarok.kde.org/wiki/Amarok_Live:1.4_Artwork">Amarok wiki</a>. This artwork contest will be opened for submission until midnight UTC ending September 1st.  Amarok Live 1.4 will feature the latest Amarok release from the 1.4 'Fast Forward' series and about 20 songs from <a href="http://www.magnatune.com">Magnatune</a> and artists, who are distributing their music in a free way.








<!--break-->

