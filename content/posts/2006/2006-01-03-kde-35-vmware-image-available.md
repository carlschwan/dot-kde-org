---
title: "KDE 3.5 VMware Image Available"
date:    2006-01-03
authors:
  - "santoniades"
slug:    kde-35-vmware-image-available
comments:
  - subject: "awesome!"
    date: 2006-01-03
    body: "This is a great way to promote KDE! (and SuSE!)\n\nI'm already running 3.5, but I'll be sure to download this image to play with it on SuSE."
    author: "Bill"
  - subject: "QEMU"
    date: 2006-01-03
    body: "A nice free-as-in-speech alternative to VMware is QEMU. QEMU is available in your favorite package management tool ;-) or at http://fabrice.bellard.free.fr/qemu/. \nI used QEMU to try out the Klax KDE 3.5-alpha live CDs before I was brave enough to install it on my real system. "
    author: "Anonymous"
  - subject: "Re: QEMU"
    date: 2006-01-03
    body: "Free? The accelerator module is not (available as source) and without it QEMU is slow/unusable."
    author: "Anonymous"
  - subject: "Re: QEMU"
    date: 2006-01-03
    body: "There is dkms-qvm86 which is an open source accelerator, not as fast as kqemu, but it works well."
    author: "Mooby"
  - subject: "Re: QEMU"
    date: 2006-01-03
    body: "Slow, yes, but unusable? I wouldn't say so."
    author: "mikeyd"
  - subject: "Re: QEMU"
    date: 2006-01-03
    body: "VMWare Player is free, so it's what I use.  However, I have QEMU as well to create virtual machines, but it's a bit of a cumbersome program."
    author: "John"
  - subject: "Re: QEMU"
    date: 2006-01-03
    body: "This is targeted to Windows users so QEMU doesn\u00b4t apply.\nUse VMware Player."
    author: "reihal"
  - subject: "Re: QEMU"
    date: 2006-01-03
    body: "Qemu *does* apply because it Qemu runs on Linux, FreeBSD, Mac OS X and Windows. "
    author: "Me"
  - subject: "Re: QEMU"
    date: 2006-01-03
    body: "I have been using both Qemu and Vmware for a long time now, and I like Qemu for beeing free software (without kqemu of course). Nevertheless performance- and stability wise, VMware player is just miles ahead, so it could offer a much better Linux/KDE experience to new users wanting to try out before installing on real hardware.\nI think it was a wise decision to offer a VMware-player image in this case.\n"
    author: "yope"
  - subject: "Re: QEMU"
    date: 2006-01-04
    body: "What are the advantages of running KDE with it instead of coLinux - http://www.colinux.org/ ?"
    author: "Anonymous Coward"
  - subject: "Re: QEMU"
    date: 2006-01-04
    body: "it's easier to use. and it's faster too. plus, it runs on windows and linux. -- colinux is for windows only (AFAIK). and rather difficult to setup as compared to vmware-player."
    author: "ac"
  - subject: "Re: QEMU"
    date: 2006-01-04
    body: "For example you need additionally a VNC (slow) or X-Server for coLinux.\n\n"
    author: "Anonymous"
  - subject: "Screen Resolution"
    date: 2006-01-04
    body: "Is it possible to change the screen resolution? Sax2 or the Yast X-module don't start up.\n"
    author: "XCG"
  - subject: "Re: Screen Resolution"
    date: 2006-01-05
    body: "Try xorgcfg  or  xorgconfig as root on the commandline"
    author: "rinse"
  - subject: "Re: Screen Resolution"
    date: 2006-01-05
    body: "SaX2 doesn't work well with the enhanced VMware X drivers so you have to change xorg.conf manually for that. When trying VMware Player ran with a resolution of up to 2048x15?? for me."
    author: "binner"
  - subject: "Re: Screen Resolution"
    date: 2006-01-07
    body: "Try calling SaX2 with parameter 'r', read \"sax2 -r\". And if you change the mouse driver in xorg.conf from \"mouse\" to \"vmmouse\" afterwards you can move the mouse outside the VMware Player window without pressing the escape shortcut again."
    author: "binner"
  - subject: "256M Myth"
    date: 2006-01-05
    body: "Binner - thanks for the image, it's of great help. But please change that 256M memory to say something like 512M + - come on neither Windows nor Suse 10 run fine with 128Mb each!"
    author: "furfaar"
  - subject: "Re: 256M Myth"
    date: 2006-01-05
    body: "You can change the maximal used memory within the VMware player, no? And defaulting to 512MB would exclude all users of machines with less or equal 512MB.\n\n> come on neither Windows nor Suse 10 run fine with 128Mb each!\n\nThe site says \"more than 256MB\" memory and the virtual machine defaults to 256MB.\n"
    author: "binner"
  - subject: "Re: 256M Myth"
    date: 2006-01-05
    body: "oops! You meant to say 256M for the Virtual Machine - I interpreted that as 256M for the machine. Sorry my bad!"
    author: "furfaar"
  - subject: "Please provide md5sums for the downloads"
    date: 2006-01-06
    body: "KDE_3.5_on_SUSE_Linux_10.tar.bz2 (537MB)\nKDE_3.5_on_SUSE_Linux_10.zip (588MB)\n\nare the two files provided for download. I request that md5sums be provided so we can verify our downloads. Please notify me when they are added so I can make a note of them.\n\nThanks for allowing me to access SUSE 10 / KDE 3.5 even during the few times I start Windows."
    author: "Shriramana Sharma"
  - subject: "Re: Please provide md5sums for the downloads"
    date: 2006-01-06
    body: "Added."
    author: "binner"
  - subject: "Re: Please provide md5sums for the downloads"
    date: 2006-01-09
    body: "Clicking on the md5 files results in the tar.bz2 or zip file getting downloaded. Please check and fix.\n\nP.S: Can you remove my mail ID from my posts? I am getting spammed. Thanks. (I authorize you \"binner\" to modify my post if it is necessary to do that.)"
    author: "Shriramana Sharma"
  - subject: "Re: Please provide md5sums for the downloads"
    date: 2006-01-09
    body: "> Clicking on the md5 files results in the tar.bz2 or zip file getting downloaded. \n\nNo, the links are correct."
    author: "binner"
  - subject: "Re: Please provide md5sums for the downloads"
    date: 2006-01-12
    body: "The links are correct, but I think the redirect is busted (or something weird is going on).  There's no corresponding .md5 file on the destination of the redirect.\n\n> wget 'http://developer.kde.org/~binner/vmware/KDE_3.5_on_SUSE_Linux_10.tar.bz2.md5'\n--14:14:01--  http://developer.kde.org/~binner/vmware/KDE_3.5_on_SUSE_Linux_10.tar.bz2.md5\n           => `KDE_3.5_on_SUSE_Linux_10.tar.bz2.md5'\nResolving developer.kde.org... 131.246.120.250\nConnecting to developer.kde.org|131.246.120.250|:80... connected.\nHTTP request sent, awaiting response... 302 Found\nLocation: http://bluemchen.kde.org/vmware/KDE_3.5_on_SUSE_Linux_10.tar.bz2 [following]\n--14:14:02--  http://bluemchen.kde.org/vmware/KDE_3.5_on_SUSE_Linux_10.tar.bz2\n\n"
    author: "Anonymous Coward"
  - subject: "Can I access my existing SUSE ReiserFS partitions?"
    date: 2006-01-06
    body: "If I install VMWare Player and this SUSE 10 machine on Windows, can I access my ReiserFS (3.6) partitions residing on the same hard disk? This is what I plan to use this new innovation for. Do all partitions on the HDD get automounted or can I mount my /dev/sda8 /home partition manually? Thanks."
    author: "Shriramana Sharma"
  - subject: "Re: Can I access my existing SUSE ReiserFS partitions?"
    date: 2006-01-06
    body: "No, those are outside of your virtual machine."
    author: "binner"
  - subject: "Re: Can I access my existing SUSE ReiserFS partitions?"
    date: 2006-01-09
    body: "> No, those are outside of your virtual machine.\n\nCan't I do\n\nsu\nmkdir /mysusepart\nmount -t auto /dev/sda8 /mysusepart"
    author: "Shriramana Sharma"
  - subject: "Re: Can I access my existing SUSE ReiserFS partiti"
    date: 2006-01-10
    body: "No, you have to actually add the raw partitions or raw drive to the virtual machine before it will see them, and I don't think VMWare Player lets you make changes like that.  You can do it with the full VMWare though..."
    author: "mabinogi"
  - subject: "Re: Can I access my existing SUSE ReiserFS partiti"
    date: 2006-04-10
    body: "Correct. With VMWare Workstation this is possible. It's even possible (though not trouble-free) to boot up your existing installation, without installing anything to any virtual disk. The free (beer) VMWare player will not allow you to make the necessary configuration in the GUI - though if you have a Virtual Machine, that's set up to do this correctly on your specific setup VMWare Player will run that Virtual Machine.\nTo create (and use) such a VM you could look at the currently free VMWare Server Beta. I do not know if it will remain free after the Beta period without features beeing removed, but as of now, this is possible."
    author: "Christoph Wiesen"
  - subject: "Re: Can I access my existing SUSE ReiserFS partitions?"
    date: 2006-01-06
    body: "check out colinux, i've used it to access my Linux partitions (fed up with ext3 windows drivers) You can then transfer to windows as well with samba."
    author: "crache"
  - subject: "Torrents"
    date: 2006-01-07
    body: "Just a short note that I added links to torrent which someone gracefully set up."
    author: "binner"
  - subject: "Screenshots"
    date: 2006-01-17
    body: "Can anyone show how this looks , linux within windows?  and what must i think with \"slow\" ?"
    author: "kaka"
  - subject: "what is the localhost login and password?"
    date: 2006-02-22
    body: "what is the localhost login and password?"
    author: "long"
  - subject: "How to get the root password ?"
    date: 2006-07-12
    body: "Im experimental with vmware.. i have a lot of linux & windows installed\n\nMy question is .. whats the root password ? i wanna install apt and other programs and i cant :(... \n\nis there a way to recovery the password without a cd rom ?\n\n\n"
    author: "Morgan"
  - subject: "Re: How to get the root password ?"
    date: 2006-07-13
    body: "How about reading the documentation/web page?"
    author: "Anonymous"
---
Stephan Binner has released a <a href="http://developer.kde.org/~binner/vmware/">VMware Player image of KDE 3.5</a> with KOffice running on <a href="http://www.opensuse.org/">SUSE Linux</a> 10.  The image is fully functional and can be upgraded and tweaked as needed. The version of VMware necessary to run it can be <a href="http://www.vmware.com/products/player/">downloaded for free</a>.  Stephan <a href="http://www.kdedevelopers.org/node/1720">blogs</a> "<em>It's a fully working installation so don't forget to install and try some additional KDE applications - and to run the security update when you're asked to. ;-)</em>"
<!--break-->
