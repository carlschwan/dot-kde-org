---
title: "KDE Commit-Digest for 21st May 2006"
date:    2006-05-22
authors:
  - "dallen"
slug:    kde-commit-digest-21st-may-2006
comments:
  - subject: "Keep it up!"
    date: 2006-05-22
    body: "Danny, thanks for this commit digest, please keep up the work, it's always an interesting read and I bet it's also usefull in many ways!\n\nThanks :)"
    author: "David"
  - subject: "Thanks"
    date: 2006-05-22
    body: "Thanks, Danny, great read."
    author: "a"
  - subject: "Great the commit digest is back :-)"
    date: 2006-05-22
    body: "Great as usual, please keep up the excellent work Danny!\n\none minor issue: \"Bug 44662: Find window too large\" seems to be wrongly linked."
    author: "MK"
  - subject: "Re: Great the commit digest is back :-)"
    date: 2006-05-22
    body: "Fixed, thanks!\n\nDanny"
    author: "Danny Allen"
  - subject: "archivereader"
    date: 2006-05-22
    body: "What is archivereader?"
    author: "a"
  - subject: "Re: archivereader"
    date: 2006-05-22
    body: "\"Initial commit of archivereader. This code will simplify reading of\narchive files in Qt code via an implementation of the Qt 4.1 class\nQAbstractFileEngine.\"\n\nSeems like its meant for reading archive files (tar,gz,zip,rar,etc.).  Though why would it need a client server architecture?"
    author: "Corbin"
  - subject: "Re: archivereader"
    date: 2006-05-23
    body: "part of a new implementation of kat-like functionallity. think beagle."
    author: "superstoned"
  - subject: "Linux Specific Hacks???"
    date: 2006-05-22
    body: "I noticed a couple of commits there that seemed to be very Linux specific, e.g. assuming HFS+ support in Kernel.  I thought KDE4 was to be OS neutral, or at least agnostic?\n\nJohn."
    author: "odysseus"
  - subject: "Re: Linux Specific Hacks???"
    date: 2006-05-22
    body: "It is possible to add platform specific code while still not giving up platform independence. You just have to do two things:\n1. Deactivate that code when building it for another platform, and\n2. Don't depend on it.\n\nIf the above-mentioned code adheres to those principles (and I think it does) then KDE can make use of Linux-specific features, but still works without those on other platforms."
    author: "Jakob Petsovits"
  - subject: "R.I.P. aRts"
    date: 2006-05-22
    body: "R.I.P. aRts. You served us well."
    author: "Artem S. Tashkinov"
  - subject: "Re: R.I.P. aRts"
    date: 2006-05-22
    body: "aRts was a big steaming pile of garbage. Thank God that the KDE devs finally had the good sense to toss out that rubbish.\n\nGood riddance, aRts!"
    author: "Multimedia Guy"
  - subject: "Hyperlinks"
    date: 2006-05-22
    body: "to Danny:\nTry to make hyperlinks more distinguishable from ordinary text. At present there's no visual difference between them (except if you move mouse over a link - if you know where)\n\nAlso, why removal of KPDF? It wasn't so bad, I think. Will be integrated into oKular?"
    author: "keber"
  - subject: "Re: Hyperlinks"
    date: 2006-05-22
    body: "Yes, oKular will do everything that KPDF could do before (and more!).\nNothing to worry about."
    author: "Jakob Petsovits"
  - subject: "Re: Hyperlinks"
    date: 2006-05-22
    body: "I'm happy to hear that! KPdf has evolved into a very competent program for showing presentations and I would hate to loose that. I use it on a regular basis for presentations created with pdflatex and the beamer package. \n\nThe only feature i miss in the presentation mode of KPdf is the ablility to show embedded videos. It is capable of launching an external video viewer, e.g. Xine,  by following a link in the PDF file, but it would be even better to be able to show video clips as part of the pdf pages.\n"
    author: "\u00d6rjan Ekeberg"
  - subject: "FAM/dnotify/inotify/QSocketNotifier"
    date: 2006-05-22
    body: "I just read about the QSocketNotifier class in that commit digest, being able to watch files, too.\n\nDirk Mueller removed dnotify support since 2.4 will be obsolete with KDE 4. Are there plans to rebuild the notification system upon QSocketNotifier completely? Or isn't it a complete replacement?\n\n\nlg\nErik"
    author: "Erik"
---
In <a href="http://commit-digest.org/issues/2006-05-21/">this week's KDE Commit-Digest</a>: Huge optimisations in ksysguard. <a href="http://solid.kde.org/">Solid</a> switches to <a href="http://www.cmake.org">CMake</a>. <a href="http://www.arts-project.org/">aRts</a>, <a href="http://kpdf.kde.org/">KPDF</a> removed in trunk/, whilst <a href="http://developer.kde.org/summerofcode/okular.html">oKular</a> continues to be developed as its replacement. <a href="http://amarok.kde.org">amaroK</a> gets support for Creative Zen devices. coreapps/ module created (as proposed on kde-core-devel). More work on supporting Intel compilers.

<!--break-->
