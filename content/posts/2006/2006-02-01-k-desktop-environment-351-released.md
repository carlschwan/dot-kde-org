---
title: "K Desktop Environment 3.5.1 Released"
date:    2006-02-01
authors:
  - "sk\u00fcgler"
slug:    k-desktop-environment-351-released
comments:
  - subject: "nice surprise"
    date: 2006-01-31
    body: "great! thank you all!"
    author: "Tom"
  - subject: "Commit digest"
    date: 2006-02-01
    body: "Thanks to the developers for making KDE even more stable. These updates are very useful while we're waiting for KDE 4. Does anyone know what happened to the Commit Digest or the \"This month in SVN\" series? Will they be continued?"
    author: "ac"
  - subject: "Re: Commit digest"
    date: 2006-02-01
    body: "\"This month in SVN\", not sure, I assume that it would resume when there's more to report than \"Making this work better with Qt 4\". ;)\n\nThere's a lot of rearchitecting and porting and breakage occuring all at once.  Not much interesting for users quite yet in SVN.\n\nAs far as the Commit Digest, from my understanding the author has been very swamped for time and no one has stepped up to take his place.  I think if anyone here with enough time for poring over even a few mailing lists would meet with the gratitude of thousands of people if they could step up and help Derek out.\n\nRegards,\n - Michael Pyne"
    author: "Michael Pyne"
  - subject: "RoadMAP"
    date: 2006-02-01
    body: "hi ive seen theres no Roadmap for 4.0 - it was discussed @ core-devel that there will be some sort of techology preview , when the basic porting ist done.\n\nCan one can please come up with a possible release plan ? someone has the courage ?"
    author: "chris"
  - subject: "Re: RoadMAP"
    date: 2006-02-01
    body: "There's not a single roadmap but some roadmaps on several pages.\nTake a look at:\n- http://appeal.kde.org\n- http://plasma.kde.org\n- http://solid.kde.org\nThat might be interesting to you.\n"
    author: "trooper"
  - subject: "Re: RoadMAP"
    date: 2006-02-01
    body: "ok i see ,\n\nfor solid : http://solid.kde.org/cms/1002\n\nand so on. perhaps you cant add dates now, but it would be nice to *have* dates."
    author: "chris"
  - subject: "Re: RoadMAP"
    date: 2006-02-01
    body: "eheheh , i just had the IDEA : we use Kplato (released with koffice 1.5) Project Management Application , and use the function \"EXPORT to RoadMAP\" :-)\n\n\n"
    author: "chris"
  - subject: "Re: RoadMAP"
    date: 2006-02-02
    body: "The Technical Working Group is still being formed. Once it starts working coming up with a roadmap will very likely be one of its first tasks. Ask again in about a month."
    author: "mahoutsukai"
  - subject: "google makes sad decision"
    date: 2006-02-01
    body: "http://slashdot.org/article.pl?sid=06/01/31/1519224\n\nWhy Google not use KDE and Kubuntu?"
    author: "ac"
  - subject: "Re: google makes sad decision"
    date: 2006-02-01
    body: "it's just another google-non-story. you know, like how they were going to release a web-based office suite? google is the Hot Commodity right now so every other story is \"google is doing <insert idea here>! OMFG!\"\n\nwait all you want, i don't expect to see GoogleOS anytime soon."
    author: "Aaron J. Seigo"
  - subject: "Re: google makes sad decision"
    date: 2006-02-01
    body: "They have denied it already. They do have a Ubuntu based distro they use internally, but they'll not be releasing it.\n\nRead more here: http://slashdot.org/comments.pl?cid=14609148&sid=175746&tid=217\n\nThere's also an article on arstechnica.com"
    author: "Joergen Ramskov"
  - subject: "Re: google makes sad decision"
    date: 2007-02-15
    body: "your not alowed to sware u said OMFG which  means o my fucking god \n\ngod is a good man hehe"
    author: "Ellen"
  - subject: "Wow! 63 languages for KDE 3.5.1"
    date: 2006-02-01
    body: "The KDE Internationalization and Translator Team must be the biggest and most reliable one of any of the Free and Open Software Projects!\n\nMan... 63 languages!! That is certainly more than Microsoft and Apple and Sun can muster (maybe not even if they pool together all their languages).\n\nCongratulations to that gigantic achievement. At least on _that_ field, KDE and the Unix desktop is ahead of all proprietary competition on its long road to world domination.\n\nThe only thing I wonder: didn't the newly installed KDE marketing group members know that X.Y.1 releases traditionally do feature translations, translations, translations?? It is hard to believe that they do so much underestimate the efforts of the many, many industrious translators making KDE the success it is around the world."
    author: "I'm a believer"
  - subject: "Re: Wow! 63 languages for KDE 3.5.1"
    date: 2006-02-01
    body: "I agree, we should stress that much more. It's a great effort that is being made, and it cannot be underestimated.\n\nKudos to the translation teams making KDE happen in your language!"
    author: "Sebastian K\u00fcgler"
  - subject: "Re: Wow! 63 languages for KDE 3.5.1"
    date: 2006-02-01
    body: "> The KDE Internationalization and Translator Team must be the biggest and\n> most reliable one of any of the Free and Open Software Projects!\n\nOK, I am not sure whether Debian is not complete in this respect, but\n$ aptitude search openoffice.org-l10n | wc -l\n\ngives me here 53, whereas\n\n$ aptitude search kde-i18n | wc -l\n\ngives 66.\n\nSo, maybe you are actually right. :-)\n\nMatej"
    author: "Matej Cepl"
  - subject: "Re: Wow! 63 languages for KDE 3.5.1"
    date: 2006-02-01
    body: "what about gnome?"
    author: "superstoned"
  - subject: "Re: Wow! 63 languages for KDE 3.5.1"
    date: 2006-02-04
    body: "I don't know what are the packages in Debian, but \nhttp://www.gnome.org/i18n/ has also 52 supported and partially supported languages (supported is more than 80 % translated -- 39 languages, partially supported more than 50 % -- 13 languages). it seems that they are doing better than us.\n\nI have downloaded whole similar data for KDE (of course, they are better presented :-);  <http://i18n.kde.org/stats/gui/stable/index.php>) into OpenOffice (yeah, I know, but KSpread doesn't support this yet) and made a table <http://www.ceplovi.cz/matej/tmp/kde-i18n.ods>. The table says that there are 25 languages supported above 80 %, and 12 languages above 50 %. It seems that in both aspects we are doing worse than Gnome.\n\nSorry,\n\nMatej"
    author: "Matej Cepl"
  - subject: "Re: Wow! 63 languages for KDE 3.5.1"
    date: 2006-02-04
    body: "GNOME is a lot smaller than KDE."
    author: "ac"
  - subject: "kicker crash not really fixed ?"
    date: 2006-02-01
    body: "Hi all\n\nI am still seeing kicker crashes on logout (3.5.1 for Kubuntu Dapper) ... am I the only one ?\n\nThis is the bug:\nhttp://bugs.kde.org/show_bug.cgi?id=113242\n\nCheers!"
    author: "KubuntuUserExMandrake"
  - subject: "Re: kicker crash not really fixed ?"
    date: 2006-02-01
    body: "there are several completely different crashes there. if you are suffering from the xim related crash, there is nothing i can do about it. get rid of libqxim from your system or move to a version of it that doesn't crash.\n\nif it's a different crash, i'd love to hear about it."
    author: "Aaron J. Seigo"
  - subject: "Smart Pointers ?"
    date: 2006-02-01
    body: "Thanks a lot Aaron\n\nI'll check and report in bugzilla if I find anything interesting\n\nBTW, does Qt provide smart pointers ? It would be great to have\na solid smart pointer implementation in the kdelibs, right ? I do \nlots of c++ programming for work, and if you ask me what's the \nsingle most important missing feature in the STL, I would say\npolymorphic smart pointers. Maybe a wrap around the boost reference\ncounting smart pointers would do. Maybe boost's work should become\npart of the STL. But in any case, it would be greate to be able\nto forget about deleteing NEW allocated memory, we'd gain a lot in terms\nof robutness !\n\nCheers!"
    author: "KubuntuUser(exMandrake)"
  - subject: "Re: Smart Pointers ?"
    date: 2006-02-01
    body: "In kdelibs there is KSharedPtr:\nhttp://websvn.kde.org/trunk/KDE/kdelibs/kdecore/ksharedptr.h?rev=501697&view=auto"
    author: "cl"
  - subject: "Re: Smart Pointers ?"
    date: 2006-02-01
    body: "That's exactly it ! Thanks a lot for the pointer ;-)\n\nIs it being used extensible ? It would make sense to _only_ use KSharedPtr's as a policy. The only drawback is you need to inherit from QSharedData in most of your classes, but the advantages are gigantic !"
    author: "KubuntuUser(exMandrake)"
  - subject: "Re: Smart Pointers ?"
    date: 2006-02-01
    body: "i'm using KSharedPtr quite a lot in kicker, though that's not the issue with the crashes people were seeing in 3.5.0 at all."
    author: "Aaron J. Seigo"
  - subject: "Re: Smart Pointers ?"
    date: 2006-02-01
    body: "Oh, I see, I took a quick look at the patch you posted for the bug, and I got the impression that using a smart ptrs would have avoided the crash, but it was just a quick impression and obviously wrong. Cheers , and thanks for the great work !\n:-)"
    author: "KubuntuUser(exMandrake)"
  - subject: "Re: kicker crash not really fixed ?"
    date: 2006-02-03
    body: "Just for the record, yeah, I am having the XIM crash :-(\nThank you Aaron !"
    author: "KubuntuUserExMandrake"
  - subject: "coooooool"
    date: 2006-02-01
    body: "yahoo mail buttons works ok now again :)"
    author: "Patcito"
  - subject: "Bluetooth !"
    date: 2006-02-01
    body: "Will Bluetooth be integrated ??? not in kdelibs but somewhere in the default release ???? \n\n^^^^^^^^^^ this is mostly std. and kde has superb bluetooth support (kde-bluetooth)... "
    author: "chris"
  - subject: "Re: Bluetooth !"
    date: 2006-02-01
    body: "chelcicky:~$ apt-cache search bluetooth kde\nkdebluetooth - KDE Bluetooth Framework\nkdebluetooth-irmcsync - IrMCSync Konnector for kitchensync\nqobex - Swiss army knife for the OBject EXchange (obex) protocol\nchelcicky:~$\n\nWhat about this?\n\nMatej"
    author: "Matej Cepl"
  - subject: "Re: Bluetooth !"
    date: 2006-02-01
    body: "i know - i use both , but i think they are so handy they should be in the default install - or released as offical kde package"
    author: "chris"
  - subject: "Re: Bluetooth !"
    date: 2006-02-01
    body: "Well, the developers of KBluettooth have probably wanted to have different release cycle than the main KDE, and I think in KDE 4 many other apps are going to be extracted from the base release."
    author: "tpr"
  - subject: "Re: Bluetooth !"
    date: 2006-02-01
    body: "But something like : \"Bluetooth Device found\" (this solid stuff -(SOLID)), Would you like (mind) to install kde-bluetooth ? <yes|no>  : kexecute call_distro_install kde-bluetooth.\n\n\n(something like that is base functionality)"
    author: "chris"
  - subject: "Re: Bluetooth !"
    date: 2006-02-02
    body: "KDE Bluetooth Framework (http://kde-bluetooth.sourceforge.net/) still in beta-1 for nearly an year! "
    author: "Sagara"
  - subject: "KDE vs gnome vs openoffice screen resolution!"
    date: 2006-02-01
    body: "Nope, I am not starting a flamebait... but comparing the DPI used by KDE, GNOME, Openoffice, xfce.\n\nGnome uses 96dpi screen resolution, Openoffice.org uses 96dpi screen resolution, MS Windows uses 96dpi screen resolutions, but KDE uses 75x75 screen resolution or non-standard ones 84x84 etc.,\n\nThe fonts look 20% (twenty percent) smaller in KDE applications and Openoffice documents and kword documents look different (koffice documents being smaller font size due to small screen resolution)\n\nCould we consider KDE's default Screen resolution to 96x96dpi for better visual compatibility with Gnome, XFCE, Openoffice and other websites which expects 96dpi as screen resolution (due to M$).\n\nIf we start KDE applications in XFCE then you can see *fonts* of KDE apps look *too big* (due to XFCE using 96dpi and KDE using 75dpi).\n\nI wonder is there a freedesktop.org standard for screen resolution, which could make fonts look similar in all Desktop Environments?"
    author: "fast_rizwaan"
  - subject: "Re: KDE vs gnome vs openoffice screen resolution!"
    date: 2006-02-01
    body: "DPI is a function of the X server.  In fact, for a proper DPI setting, let X figure it out by asking your monitor.  Mine's set at 90x84 which is correct, because X knows the size of my monitor and the resolution I'm running at.\n\nEither way, though, you can run KDE at 96x96 if you want.  There's nothing stopping you."
    author: "KDE User"
  - subject: "Re: KDE vs gnome vs openoffice screen resolution!"
    date: 2006-02-01
    body: "open an opendocument with kword and that same document with openoffice 2.0.x, you can see that document looks different... setting dpi to 96 makes kword and openoffice.org's document look same.\n\nA document created should look same either or kde or gnome or ms windows. I am suggesting 96dpi as default for KDE (which can be configured from kcontrol to increase/decrease the dpi for BIG screens and Small Screen respectively.)\n\n"
    author: "fast_rizwaan"
  - subject: "Re: KDE vs gnome vs openoffice screen resolution!"
    date: 2006-02-01
    body: "Actually, this is something that is very broken in Gnome -- which blithely disregards the dpi settings of X11 (which you can set yourself, in /etc/kde3/kdm/kdmrc (if you're using kdm and Kubuntu, at least):\n\nServerArgsLocal=-dpi 96 -nolisten tcp\n\nOr which X11 can figure out from the size and resolution of your monitor. My laptop panel is 1680x1050 15\", but there are also 21\" monitors with that resolution. It stands to reason that pixels are differently sized on those examples, so if you want accurate font sizes (wysisyg, remember), your desktop environment needs to take that into account."
    author: "Boudewijn Rempt"
  - subject: "Re: KDE vs gnome vs openoffice screen resolution!"
    date: 2006-02-01
    body: "I've filed a bug about something that seems quite similar. Could you guys check this out and see if it also fails for you? If it doesn't, it's obviously something to do with my setup so I'll close the bug..\n\nhttps://bugs.kde.org/show_bug.cgi?id=120061"
    author: "Gogs"
  - subject: "Re: KDE vs gnome vs openoffice screen resolution!"
    date: 2006-02-01
    body: "You've got a minimum font size of 10 set in your konqueror settings. If you set that to two, everything is nicely scaled. And yes, this is a very welcome feature of konqueror."
    author: "Boudewijn Rempt"
  - subject: "Re: KDE vs gnome vs openoffice screen resolution!"
    date: 2006-02-01
    body: "How do you know what my minimum font size is set to? (It's NOT 10 btw!)"
    author: "Gogs"
  - subject: "Re: KDE vs gnome vs openoffice screen resolution!"
    date: 2006-02-01
    body: "He (and you it seems) assumed the page you linked to was showing fonts' point size.  It isn't, it is showing the pixel size.  If you switch to point size (choose 'pt' next to 'font size') all the fonts size 8 and lower will be the same for you, exactly as one would expect from your konqueror settings."
    author: "MamiyaOtaru"
  - subject: "Re: KDE vs gnome vs openoffice screen resolution!"
    date: 2006-02-02
    body: "Ok, can you explain to me why when my minimum font size is set to 8, and in KDE control centre my font selection is Tahoma 7, that that page displays font size 8 (8pt, not 8px) smaller than my Tahoma 7 fonts in my menus...?\n\nI'm not being sarcastic here, I genuinely believe that something is wrong somewhere with how kde scales/displays fonts, but if it's my ignorance then I'd like it explained to me where I'm going wrong....."
    author: "Gogs"
  - subject: "Re: KDE vs gnome vs openoffice screen resolution!"
    date: 2006-02-01
    body: "Is it really broken in Gnome ?\nIIRC in my case, in Gnome preferences, you can choose to keep what the X server says or choose your own.\nAs a matter of fact, I don't have the problem discussed here in Gnome nor in KDE at home (and I'm at 96 dpi at 1600x1200 on a CRT 22\").\nBut perhaps that's because it was configured a long time ago in KDE, I don't know."
    author: "Ookaze"
  - subject: "Re: KDE vs gnome vs openoffice screen resolution!"
    date: 2006-02-01
    body: "I disagree with this entirely. Windows is going to be changing this behavior with Vista to be the true DPI of the monitor instead of the hack they have now. I have no idea why gnome is using that stupid hack but I want the dpi to be set correctly. A 16pt font should be the same size on any size display regardless of resolution. Increased resolution gives greater accuracy in rendering it does not change the size of any element. KDE behaves correctly by default for this. I can run kde on a high dpi monitor and have no problems at all. However with windows you need special driver hacks to make it work. I expect you will need the same hacks on gnome to make it do the right thing.\n\nDisplays are getting higher rez and higher dpi all the time and so going back to a hack about the way people are used to is just a horrible idea. KDE needs to stay the way it is now and the problem will sort itself out over time. Actually if other systems did things correctly there would not be a good reason to run in anything other then the max resolution that the device you where using supports since that would give a better quality image."
    author: "Kosh"
  - subject: "Re: KDE vs gnome vs openoffice screen resolution!"
    date: 2006-02-02
    body: "> Displays are getting higher rez and higher dpi all the time and so going back to a hack about the way people are used to is just a horrible idea.\n\nYou are exactly right, but is there any free-standard for DPI (dots per inches) for free-desktops like KDE, GNome, XFCE?:\n\nI can't find any at: http://freedesktop.org/wiki/Standards\n\nperhaps freedesktop did not notice dpi issues with \"font sizes\" with different desktops.\n\nThere are millions of Monitors which will take another 5 years to change, till then KDE applications' documents and text look different from other desktops! and this is also a horrible idea!\n\nMaking 96dpi by default (with KDE) make font sizes in harmony with other desktops and applications (openoffice), consistency could be achieved.\n\nLetting users decide to have (96 or Autodetected resolution) settings in kcontol will solve the issue for good."
    author: "fast_rizwaan"
  - subject: "Re: KDE vs gnome vs openoffice screen resolution!"
    date: 2006-02-02
    body: "> You are exactly right, but is there any free-standard for DPI (dots per inches) for free-desktops like KDE, GNome, XFCE?:\n\nIt seems you are totally confused. DPI isn't something you agree on. It's determined by dividing width (or height) of the display area in pixels, by width (or height) of the display area in inches.\n\nSo a 14.1 inches wide by 11.3 inches high monitor with a resultion of 1280x1024\nhas a dpi of 91x91.\n\n> Making 96dpi by default (with KDE) make font sizes in harmony with other desktops and applications \n\nNo, it will make the font size wrong on many monitors.\n\n> Letting users decide to have (96 or Autodetected resolution) settings in kcontol will solve the issue for good\n\nOthers should already how do achieve this."
    author: "Christian Loose"
  - subject: "Re: KDE vs gnome vs openoffice screen resolution!"
    date: 2006-02-01
    body: "On my system, KDE uses 96 DPI. Why? Because that's what my monitor is! If you're not getting the right DPI, it's because your X server is misconfigured. This isn't KDE's fault.\n\nThere is no standard DPI. It depends on your monitor."
    author: "Brandybuck"
  - subject: "Re: KDE vs gnome vs openoffice screen resolution!"
    date: 2006-02-01
    body: "DPI is an X Window System property and has nothing to do with KDE!\n\nTo setup the DPI to be precisely 96x96 dpi each time, you need to add a line into you xorg.conf\n\nLocate Section \"Monitor\" and add the following lines before EndSection:\n\n# DisplaySize 270 203     # 1024x768 96dpi\n# DisplaySize 338 254     # 1280x960 96dpi\n# DisplaySize 338 270     # 1280x1024 96dpi\n# DisplaySize 370 277     # 1400x1050 96dpi\n# DisplaySize 423 370     # 1600x1400 96dpi\n\nUncomment the resolution you normally use...\n\nTo get MS fonts working properly...try looking at the following guide:\n\nhttp://www.ubuntuforums.org/showthread.php?t=20976&highlight=cleartype\n\nIt's Ubuntu(Debian) specific but the instructions for the DPI stuff can be carried out on any linux box.\n\nBy the way, if you're going to use the Virtual or Viewport keywords then the above DisplaySize will not take effect. Also, for Nvidia graphics card, there is an Option called \"UseEdidDpi\" which needs to be set to \"FALSE\" for your DisplaySize option to to take effect.\n\nFor more info see: \nhttp://download.nvidia.com/XFree86/Linux-x86/1.0-8174/README/32bit_html/appendix-y.html\n"
    author: "Jinesh Choksi"
  - subject: "Re: KDE vs gnome vs openoffice screen resolution!"
    date: 2006-03-06
    body: "Hey man,\n\nLong time!\n\nEmail me!"
    author: "Pritesh Chauhan"
  - subject: "Since I've switched to Kubuntu..."
    date: 2006-02-01
    body: "...from SuSE I never really figured out how is the best way to simply update only KDE. They say on Kubuntus download page to add the sources to \nsources.list. But how do I go from there? If I click on Upgrade all in\nAdept, everything is updated, not only KDE. And I dont want to select\neach and every package and select update.\n"
    author: "Martin"
  - subject: "Re: Since I've switched to Kubuntu..."
    date: 2006-02-01
    body: "you could try updating kubuntu-desktop or another kde metapackage that depends on everything KDE has to offer. Maybe you need to do it from the command line."
    author: "bsander"
  - subject: "Re: Since I've switched to Kubuntu..."
    date: 2006-02-01
    body: "Hi,\n\nsimply add the repository to your /etc/apt/sources.list and type:\n\nsudo apt-get update\nsudo apt-get upgrade\n\nIt will install your newly added KDE. If you want only KDE and NOTHING else, you could comment the rest of the sources out and then enter above commands, I guess, but why would you do that?"
    author: "Stephan"
  - subject: "Re: Since I've switched to Kubuntu..."
    date: 2006-02-01
    body: "At least in Debian, there is (IIRC) a kde meta package."
    author: "cl"
  - subject: "Best Way? Switch Back To Suse! (NT)"
    date: 2006-02-03
    body: "adfdsfasdf"
    author: "Martin"
  - subject: "Re: Since I've switched to Kubuntu..."
    date: 2006-03-24
    body: "You need to enable the 'universe' repository in /etc/apt/sources.list, then the package tree won't break and KDE will upgrade."
    author: "Gjermund Stensrud"
  - subject: "What? No SuSE version?"
    date: 2006-02-01
    body: "SuSE are usually one of the first with packages. Not good."
    author: "Jon Scobie"
  - subject: "Re: What? No SuSE version?"
    date: 2006-02-01
    body: "Take a look at the README in the SuSe directory on ftp.\nThey are having trouble with the package builder server because of the next release of OpenSuse (provally it's very high CPU/Mem Usage or crashed). But they made packages for Koffice 1.5 beta 1 :)"
    author: "Iuri Fiedoruk"
  - subject: "Re: What? No SuSE version?"
    date: 2006-02-01
    body: "You should reread the README: there are no problems with the next release of SUSE Linux, crashes or CPU/memory usage problems. The only problem is building packages for \"supplementary\" which is an overlay over older released distributions."
    author: "binner"
  - subject: "Re: What? No SuSE version?"
    date: 2006-02-01
    body: "Yeah, but now that Mantel has left the building i'm not so sure anymore."
    author: "Anonymous Coward"
  - subject: "Re: What? No SuSE version?"
    date: 2006-02-02
    body: "I didn't read until the finish, but - and sorry for my poor english - my idea was that due to other packages being prepared, there wasn't enought machine power to build the 3.5.1 rpms. Sure, as the server is building the rpms for next opensuse, they won't load it with kde packages also ;)\n\nThat's what I tryied, and shamelfull failed, to say, sorry :)"
    author: "Iuri Fiedoruk"
  - subject: "Re: What? No SuSE version?"
    date: 2006-02-02
    body: "> Sure, as the server is building the rpms for next opensuse, they won't load it with kde packages also ;)\n\nYou're still failing to understand, the load is not the problem."
    author: "binner"
  - subject: "SuSE is DEAD!!!!!!!!!"
    date: 2006-02-02
    body: "Wake up guys, SuSE is DEAD!\n\nRead following if you are not informed:\n\n1. http://linux.slashdot.org/article.pl?sid=05/11/05/1620206\n\n2. http://www.theinquirer.net/?article=27580\n\n3. http://linux.slashdot.org/article.pl?sid=05/11/09/1935253\n\nOnce the CSA (Chief Software Architect) is resigned, it's a dead man walking.\n\n\nBtw. watch out Tomahawk Desktop (http://www.tomahawkcomputers.com/). It is rumored they are waiting KDE 3.5.1 is officially released to release a much improved desktop."
    author: "DistroWatch"
  - subject: "Re: SuSE is DEAD!!!!!!!!!"
    date: 2006-02-02
    body: "Mantel was not the Chief Software Architect, not even an architect at all. And your other links are obsolete too: http://www.novell.com/prblogs/index.php?title=kde_and_gnome&more=1&c=1&tb=1&pb=1"
    author: "Anonymous"
  - subject: "Re: SuSE is DEAD!!!!!!!!!"
    date: 2006-02-02
    body: "No, they are not obsolete. Link you posted is just Novell PR department trying to hide the fact that when it comes to KDE, SUSE is dead.\n\nBut can't blame them, really. Middleware control is everything, and this time it looks like giving too much control over the middleware to single instance (TT) is finally hurting KDE. Strategic blow-up on KDEs part..\n"
    author: "Anonymous Coward"
  - subject: "Re: SuSE is DEAD!!!!!!!!!"
    date: 2006-02-02
    body: "it's dangerous to confuse excuses for reasons, which is what you're doing here.\n\nTT's relationship to Qt may be one of the excuses in play (there are others i've heard) but it's not even close to the reason for how things are playing out within Novell."
    author: "Aaron J. Seigo"
  - subject: "Re: SuSE is DEAD!!!!!!!!!"
    date: 2006-02-02
    body: "> it's dangerous to confuse excuses for reasons, which is what you're doing here.\n\nFor some part maybe, but I'm watching this from within a huge corporation that is using Linux left and right (and its gaining ground every day, big time). Currently (almost) all roads lead to G, mostly thanks to KDE not being considered 'free' desktop due to TTs control of it."
    author: "Anonymous Coward"
  - subject: "Re: SuSE is DEAD!!!!!!!!!"
    date: 2006-02-03
    body: "<I>Currently (almost) all roads lead to G, mostly thanks to KDE not being considered 'free' desktop due to TTs control of it.</I>\n\nSure, megacorps like the one you probably work for can't stand it when they can't lock in their users through proprietary software and draconian DRM schemes.\n\nKDE is more free than gnome because KDE is GPL and it ensures that users have the source code to make changes as they see fit. This prevents draconian DRM schemes that the backers of gnome (read: megacorps) want to use to subjugate their clientel. Any developer who works on GNOME is a fool because they are only making the problem worse by refusing to stand up to the megacorps and start licensing their programs (including libraries) as GPL."
    author: "anonymous"
  - subject: "Re: SuSE is DEAD!!!!!!!!!"
    date: 2006-02-03
    body: "> KDE is more free than gnome because KDE is GPL\n\nGPL being one thing. Plus, KDE is quality code (due to Qt perhaps) that does not require you to invest eons of money to debug it to be able to use it. That's what I call free, so I agree 100%. \n\nBut there's nothing that will make the upper management to understand this. Bugs in the software are not their problem, they can get fixed with money. Control is everything.\n"
    author: "Anonymous Coward"
  - subject: "Re: SuSE is DEAD!!!!!!!!!"
    date: 2006-02-04
    body: "\"For some part maybe, but I'm watching this from within a huge corporation that is using Linux left and right (and its gaining ground every day, big time). Currently (almost) all roads lead to G, mostly thanks to KDE not being considered 'free' desktop due to TTs control of it.\"\n\nAh, yet another mythical person working inside another mythical corporation using Gnome ;-). You do realise that none of these companies care about Trolltech's relationship with KDE like you do. If that were the case they'd all be ditching Windows right, left and centre."
    author: "Segedunum"
  - subject: "Re: SuSE is DEAD!!!!!!!!!"
    date: 2006-02-02
    body: "> Link you posted is just Novell PR department trying to hide the fact that when it comes to KDE, SUSE is dead.\n\nYou're wrong. The same amount of coders are working on KDE at SUSE than before and the full desktop continues to be shipped as supported option than before."
    author: "binner"
  - subject: "Re: SuSE is DEAD!!!!!!!!!"
    date: 2006-02-02
    body: "Hope so. However, I have my doubts. Microsoft is still shipping SFU (Services For Unix) as well.\n"
    author: "Anonymous Coward"
  - subject: "Re: SuSE is DEAD!!!!!!!!!"
    date: 2006-02-03
    body: "If it is free (GPL) you have control, isn\u00b4t that the whole point with GPL?\n"
    author: "reihal"
  - subject: "Re: SuSE is DEAD!!!!!!!!!"
    date: 2006-02-03
    body: "> giving too much control over the middleware to single instance (TT) is finally hurting KDE\n\nYou must mean giving control to the OS community, since it's all GPL."
    author: "KDE User"
  - subject: "Re: SuSE is DEAD!!!!!!!!!"
    date: 2006-02-03
    body: "It may be obsolete but for the first time, no KDE binaries from SuSE. That's a fact. SuSE can come with some bullshit execuse but action speaks loud.\n\nSometimes big companies use some companies as front companies to buy other companies to slowly kill the competition.  "
    author: "Anonymous Coward"
  - subject: "Re: What? No SuSE version?"
    date: 2006-02-02
    body: "You can't say it is dead when the beta2 of SUSE Linux 10.1 ( of last week) already had KDE 3.5.1 (although beta)!!! And you can see it again on the beta3 released today."
    author: "Hugo Costelha"
  - subject: "Re: What? No SuSE version?"
    date: 2006-02-03
    body: "Yes, it will ship with SuSE 10.1 but will it be maintained? when KDE 3.5.2 comes out, will they have RPM packages ready for it? That is looking pretty doubtfull at this point.\n\nNovell has decided to not include KDE as the default desktop in favor of a lesser product, is this what we can expect from Novell? Lesser products from now on with respect to SuSE?"
    author: "Just User"
  - subject: "Re: What? No SuSE version?"
    date: 2006-02-04
    body: "> Yes, it will ship with SuSE 10.1 but will it be maintained?\n\nOf course will SUSE support its products for the promised length of time.\n\n> when KDE 3.5.2 comes out, will they have RPM packages ready for it?\n\nThere will be unsupported 10.1 rpms in \"supplementary\" YaST source as usual.\nMaybe not as \"0-day warez\" but when developers have free time for it."
    author: "binner"
  - subject: "Re: What? No SuSE version?"
    date: 2006-02-04
    body: "really poor, better shouldn't merge with novell!"
    author: "anonymous suse user"
  - subject: "Re: What? No SuSE version?"
    date: 2006-02-06
    body: "There are now SuSE packages available on ftp.kde.org!"
    author: "Anonymous"
  - subject: "Re: What? No SuSE version?"
    date: 2006-02-07
    body: "Awesome DuDE!!! Thanx for the Tip..."
    author: "Just User"
  - subject: "Thanks guys"
    date: 2006-02-01
    body: "Ah, the perfect birthday present. Thanks guys! :-P"
    author: "Marcos Dione"
  - subject: "commit-digest"
    date: 2006-02-01
    body: "Commit-digest, I miss you :,-("
    author: "Shift"
  - subject: "Re: commit-digest"
    date: 2006-02-01
    body: "So do I. It was my best newspaper :) on the net. Where are you Derek? Do you hear us? :(("
    author: "thelightning"
  - subject: "Visibility patch for qt and gcc?"
    date: 2006-02-02
    body: "ftp://ftp.frugalware.org/pub/frugalware/frugalware-current/source/kde/qt/\n\nwill qt perform better with visibility patch as most gentoo users boast of :)"
    author: "aqueel ahmed"
  - subject: "Re: Visibility patch for qt and gcc?"
    date: 2006-02-02
    body: "Gentoo's Qt doesn't have the visibility patch.  There is an overlay with a tweaked Qt (qt-copy with some USE options to enable/disable different patches including visibility support), from what I've read it provides a nice speed boost, though you have to recompile all of KDE if you want full visibility=hidden enabled."
    author: "Corbin"
  - subject: "Thank you :)"
    date: 2006-02-02
    body: "I just wanted to say thank you to the developers :). I really, really appreciate your work. Your thankless toiling on KDE makes my computer-use that much more pleasant and productive.\n\nThank you, and I really mean it :)."
    author: "Janne"
  - subject: "MSN file transfer *still* not working"
    date: 2006-02-05
    body: "It's a shame that file transfers is totally broken in kopete....."
    author: "j.n."
  - subject: "Re: MSN file transfer *still* not working"
    date: 2006-02-05
    body: "perhaps its no FUN to reengineer some ever changing Protocol ?\n\nPlease use something where the Specs are open, and its documented how it works.\n\nYou cant blame Kopete on this, please blame MS"
    author: "chris"
  - subject: "Great!"
    date: 2006-02-08
    body: "Thanks to all the developers!"
    author: "Michael G. Richard"
  - subject: "Note to kubuntu users"
    date: 2006-02-19
    body: "Most of the 3.5.1 mirrors listed on kubuntu.org seems to only have 3.5.0 packages. What worked for me was:\n\ndeb http://kubuntu.org/packages/kde351/ breezy main\n"
    author: "Haakon Nilsen"
---
KDE 3.5.1 was released today, featuring fixes to over 150 reported bugs and many other small improvements making this the most stable and feature-rich Unix desktop ever.  <a href="http://www.konqueror.org/">Konqueror</a>, <a href="http://kmail.kde.org/">KMail</a>, <a href="http://kpdf.kde.org/">KPDF</a>, <a href="http://developer.kde.org/~wheeler/juk.html">Juk</a>, <a href="http://kopete.kde.org/">Kopete</a>, <a href="http://edu.kde.org/kalzium/">Kalzium</a> and <a href="http://quanta.kdewebdev.org">Quanta</a> in particular saw a large number of improvements in stability and performance. Users are encouraged to upgrade when their distributor releases packages.  There are updated packages available for <a href="ftp://ftp.archlinux.org/extra/os/i686">Arch Linux</a>, <a href="http://packages.gentoo.org/packages/?category=kde-base">Gentoo</a>, <a href="http://kubuntu.org/announcements/kde-351.php">Kubuntu</a>, <a href="http://apt.kde-redhat.org/apt/kde-redhat/kde-org.txt">Red Hat</a> and <a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.1/contrib/Slackware/10.2/README">Slackware</a> or you can compile it yourself with <a href="http://developer.kde.org/build/konstruct/">Konstruct</a>. See the <a href="http://www.kde.org/announcements/announce-3.5.1.php">3.5.1 Release Announcement</a> for full details and the <a href="http://www.kde.org/announcements/changelogs/changelog3_5to3_5_1.php">Changelog</a> for a full list of updates.










<!--break-->

