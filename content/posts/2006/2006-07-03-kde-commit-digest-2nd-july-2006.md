---
title: "KDE Commit-Digest for 2nd July 2006"
date:    2006-07-03
authors:
  - "dallen"
slug:    kde-commit-digest-2nd-july-2006
comments:
  - subject: "PDF Editing"
    date: 2006-07-03
    body: "Quote\nThis functionality that was constantly asked for by many of our users, and we regqard this as a big step forward. \n/EndQuote\n\nI can only agree. This is a *huge* step forward for all of us who have to work with PDF files in publishing under Linux every day. This is one of the very few things I still run Windows on QEMU for. There just is no replacement yet for Acrobat Pro under Linux. And even for professional users it would quite often not be necessary to replicate all functions of Acrobat Pro. Most of them are used rarely or not all. Great that inserting or deleting pages now works. What I'm still missing to completely get rid of Acrobat Pro:\n\n- Measure Tool to measure the distance between two points on the paper\n- Color Picker tool to fetch the CMYK/Spot value of any point\n- Modify/Remove crop box\n\nAnyway, KPDF is a great tool! I can still amaze my Windows colleagues when I'm searching in a PDF. This is extremely fast and looks really good.\n"
    author: "Martin"
  - subject: "Re: PDF Editing"
    date: 2006-07-03
    body: "Thank you for the encouragement. The measure tool is already implemented in kvieswhell, we will think about implementing the other tools you mention.\n\nOnly ... we're discussing the KviewShell application here and not kpdf or its successor, oKular. KviewShell is a similar application that is based on kdvi ---in the KDE 3.5 release, KviewShell is used to view DVI and DJVU files. The current version has seen great improvements over the KDE 3.5 version which makes it my favourite document viewing tool (but then ... I am one of the authors :-) )\n\nThe latest KViewShell is currently found in the HEAD branch of the KDE SVN server. Since we find PDF editing imporant, we will come up with a full release for KDE 3 as soon as possbile.\n\nStefan."
    author: "Stefan Kebekus"
  - subject: "Re: PDF Editing"
    date: 2006-07-03
    body: "Those features are great, it means for 95% of users not needing anything else to work with PDF files. Big plus for KDE. But...\n\nDon't get me wrong, but the project has a problem. It wouldn't be bad some clarifying of the project goals and intentions . I'm not blaming KViewShell more than I blame Okular's developers. It is just that the vast majority of users right now only know KPDF. And for KDE4 it will have to be decided which document viewer will be default. So you'll have to sort this out.\n\nOf course, if we end with two different GUI's for the same backends, it doesn't have to be bad. Two options for KDE users.\n\nBtw., KViewShell needs a homepage and a name."
    author: "Miq"
  - subject: "Re: PDF Editing"
    date: 2006-07-03
    body: "two gui's for one backend would be cool, but now it's more like two backends and one gui (exept the gui's don't share their code, just rebuild each other's look)."
    author: "superstoned"
  - subject: "Re: PDF Editing"
    date: 2006-07-03
    body: "> It wouldn't be bad some clarifying of the project goals and intentions .\n\nThe goal of KViewShell is to write a simple but powerful viewing and editing\napplication for page oriented fileformats. One of the core design decitions\nis that all supported fileformats are first class citizens. Not like for\nexample in Evince where most of the functionality is only available for PDF,\nand it cannot even print a DVI file.\n\nWe also still support the KDE3 platform, and maintain two branches for KDE4\nand KDE3 that have exactly the same functionality.\n\n> So you'll have to sort this out.\n\nIf you are interested you can read an old thread, were we voiced our\nconcerns at the time the Summer of Code project that lead to okular was\nannounced:\nhttp://lists.kde.org/?l=kde-devel&m=111789901318841&w=2\n\nI don't know how I could possibly sort this out, if people want to write\na competing program, they are free to do so.\n\n> Btw., KViewShell needs a homepage and a name.\n\nWe are open for suggestions.\n\nGreetings,\nWilfried Huss (kviewshell developer)"
    author: "Wilfried Huss"
  - subject: "Okular builds on FreeDesktop.org Poppler framework"
    date: 2006-07-03
    body: "You might have been right a year ago, but right now Okular seems to be the future, especially since it builds upon the FreeDesktop.org Poppler framework which is receiving support and improvements from both KDE and GNOME developers:\n\nhttp://Poppler.FreeDesktop.org/\n\nI would encourage you to try to incorporate the superior parts of KViewShell into Poppler and Okular."
    author: "AC"
  - subject: "Re: Okular builds on FreeDesktop.org Poppler framework"
    date: 2006-07-03
    body: "Well, my uninformed friend, kviewshell also uses poppler for its PDF plugin,\nand has so for longer then okular.\n\nAlso me and Stefan help with the Qt3 and Qt4 bindings of poppler, so you\ncan probably count us to the KDE developers that support poppler."
    author: "Wilfried Huss"
  - subject: "If you had at least read the article..."
    date: 2006-07-03
    body: "Thank you for this comprehensive evaluation of the situation, but next time please make sure that you actually know what you're talking about.\n\n\"This version requires the latest developement version of the poppler library. We will come up with a full release for KDE 3 in the near future.\""
    author: "Anonymous"
  - subject: "Re: Okular builds on FreeDesktop.org Poppler framework"
    date: 2006-07-03
    body: "As Wilfried says, KViewShell already uses Poppler. Personally, I wouldn't mind a merger between the two applications. But then, although I realize that people talk about oKular more often than about KViewShell, it seems that most people haven't seen both programs next to each other. Perhaps you could compile KViewShell from \n\nSVN:branches/work/kviewshell-0.7/kviewshell \n\nand have a look. I really think KViewShell does a great job.\n\n- Stefan."
    author: "Stefan Kebekus"
  - subject: "Re: Okular builds on FreeDesktop.org Poppler framework"
    date: 2006-07-03
    body: "I just checked out kvieshell from branches/work/kviewshell-0.7/kviewshell:\n\nsvn co svn://anonsvn.kde.org/home/kde/branches/work/kviewshell-0.7/\n\nI compiled it with make -f Makefile.cvs && ./configure --prefix=/usr/local --enable-debug=full && make && make install.\n\nWhen I try to show a PS file, I get the following error message:\n\nThe specified library psviewpart could not be loaded. The error message returned was:\n/usr/local/lib/kde3/psviewpart.so: undefined symbol: _ZN10KMultiPage20createDocumentWidgetEP8PageViewP17DocumentPageCache\n\nThe same for .dvi:\n\nThe specified library kdvipart could not be loaded. The error message returned was:\n/usr/local/lib/kde3/kdvipart.so: undefined symbol: _ZNK16DocumentRenderer10totalPagesEv\n\nWhen I try to open a PDF file, I get:\n\nThe file has mime type application/pdf which is not supported by any of the installed KViewShell plugins.\n\nI set KDEDIRS correctly:\n\necho $KDEDIRS \n/usr/local:/usr\n\nKOffice runs just fine when I install it to /usr/local\n\nGreetings,\nMichael"
    author: "Michael Thaler"
  - subject: "Re: Okular builds on FreeDesktop.org Poppler framework"
    date: 2006-07-03
    body: "Do you have KDVI or kviewshell from KDE 3.5 installed? It looks like the\nlinker tries to load an old versions of certain libraries.\n\n> The file has mime type application/pdf which is not supported by any of the installed KViewShell plugins.\n\nYou need the CVS version of libpoppler, or the PDF plugin will not be build.\nIf you have compiled the pdf plugin, you just need to recreate ksycoca.\nRestarting KDE should do this.\n\n> KOffice runs just fine when I install it to /usr/local\n\nI also have kviewshell installed into its own prefix, so it does work.\n\nMaybe we should continue the troubleshooting per private mail, as it is\nprobably not so interesting for the rest of the dot readers.\n\nGreetings,\nWilfried"
    author: "Wilfried Huss"
  - subject: "Re: PDF Editing"
    date: 2006-07-03
    body: "I just hope that, whatever will be chosen to be part of KDE4, there will be one and one only. No more kate/kedit/kwrite crap, please."
    author: "anonymous"
  - subject: "Re: PDF Editing"
    date: 2006-07-03
    body: "That never bothered me much. KWrite was the easy editor and Kate was for more hardcore editing. I don't know what KEdit is. I'm hoping this will be clearer in the future though. IMHO, the real confusion lies in the media apps. Kaboodle, Kscd, Noatun, KRec, Kmidi, Juk, Amarok, Kaffeine, KMplayer . . . The last few are the most well-known and established KDE players, but there's definitely deadwood that can be cleaned."
    author: "anony"
  - subject: "Re: PDF Editing"
    date: 2006-07-04
    body: "> Kaboodle, Kscd, Noatun, KRec, Kmidi, Juk, Amarok, Kaffeine, KMplayer\n\nc'mon do you know what these apps are doing? which ones compete with eachother?\n\nthere is a logic behind the creation of all of them.\n\n"
    author: "cies breijs"
  - subject: "Re: PDF Editing"
    date: 2006-07-04
    body: "There was a logic to creating all of them and some are still needed. I'm not accusing anyone of NIH Syndrome. But KDE4 promised a cleaning up of this kind of confusion. I love KDE and it's my primary DE. But if the nuances of some of these media apps is fuzzy to me, imagine what it would be like for your average user."
    author: "anony"
  - subject: "Re: PDF Editing"
    date: 2006-07-05
    body: "i agree,\n\nyet i also like freedom of choise!\n\npeople apperently like to make, let's say, amarok and juk. they share lib, maybe some code, but they are separate apps, with separate philosofies, etc.\n\ni like that. first i liked juk more (lean'n'mean intergration into the look of my simple destop), now i like amarok better (streams, lyrics, etc., teased my to their side)\n\nwho cares?\n\nthe basic modules of kde should not contain apps like this i guess. (i think the  module system of kde may have to be revised).\n\n\nas we're used to, KDE4 will work on better frameworks, KDE always works on the frame works. this is very important i think. this combined with the 'choise' that i meantioned earlier, make KDE the best desktop in the end. </opinion>\n\n_c.\n\n\n\n\n\n"
    author: "cies breijs"
  - subject: "Re: PDF Editing"
    date: 2006-07-04
    body: "Visio"
    author: "Miq"
  - subject: "Re: PDF Editing"
    date: 2006-07-03
    body: "\"KviewShell is used to view DVI and DJVU files\"\n\nEhmmm, shouldn't this be the job of okular for KDE 4"
    author: "Fuman"
  - subject: "Re: PDF Editing"
    date: 2006-07-03
    body: "Well, that depends on whom you ask. As I wrote above, I would very much like to ask you to compile both applications and have a look at a recent version of the KViewShell.\n\nI will also be able to provide screenshots.\n"
    author: "Stefan Kebekus"
  - subject: "Re: PDF Editing"
    date: 2006-07-03
    body: "> I will also be able to provide screenshots.\n\nYes, please, it would be interesting.\n\nBTW, why don't you just name it \"View\" instead of KViewShell? Ok, it doesn't have a k, but these days the k isn't fashionable anymore.."
    author: "anonymous"
  - subject: "Re: PDF Editing"
    date: 2006-07-03
    body: "Here are some screenshots of the version released with KDE 3.5\n\nhttp://finanz.math.tu-graz.ac.at/~huss/kviewshell\n\nBut the GUI has not changed that much since then. At least not in a way\nthat is visible on screenshots.\n\nGreetings,\nWilfried"
    author: "Wilfried Huss"
  - subject: "Re: PDF Editing"
    date: 2006-07-06
    body: "Sorry for the long silence ---it's too bad I have to work to make a living. You'll find up-to-date screenshots here:\n\nhttp://www.mi.uni-koeln.de/~kebekus/KVS\n\nBest,\n\nStefan."
    author: "Stefan Kebekus"
  - subject: "Re: PDF Editing"
    date: 2006-07-03
    body: "At Win plattforms Acrobat reader always kills my system and is soo slow. I would like to see a port to Win plattforms. One reason why I use Linux is the faster and better pdf viewer. Acrobat Reader is a pain in the ass. And when you install a new version it gets 20% slower and takes 30% more memory. \n\n\n\"Color Picker tool to fetch the CMYK/Spot value of any point\"\n\nA job for an external colorpicker app which is existing afaik."
    author: "Matze"
  - subject: "Re: PDF Editing"
    date: 2006-07-03
    body: "There most likely will be a Windows version of Okular or KViewShell with KDE4's release. (Acrobat Reader is an even bigger pain in the ass on Linux though)\n\n>>Color Picker tool to fetch the CMYK/Spot value of any point\n>\n>A job for an external colorpicker app which is existing afaik.\n\nExternal colorpickers would probably only be able to do RGB I would believe."
    author: "Corbin"
  - subject: "Re: PDF Editing"
    date: 2006-07-04
    body: "I think that the idea is for the color picker to extract the actual data in the PDF file rather than just to indicate what is being displayed on the screen.\n\nThis might be usefull in some cases, but AFAIK, CMYK is a myth (it should only be used internally by printer drivers) unless you are doing professional prepress work."
    author: "James Richard Tyrer"
  - subject: "Re: PDF Editing"
    date: 2006-07-05
    body: "Foxit Reader: http://www.foxitsoftware.com/pdf/rd_intro.php\n\nProblem solved."
    author: "Isaac"
  - subject: "Re: PDF Editing"
    date: 2006-07-04
    body: "Ignore.This is just a trial posting from khtml browser running on nokia e70 :)"
    author: "Anonymous"
  - subject: "kdevelop-teamwork"
    date: 2006-07-03
    body: "This teamwork-thing enables multiple users to hack on the same text in realtime, seeing what the others are doing, since they're somehow connected on the network, right?\n\nIf I understood this correctly, I have a few questions:\n\n - Why is this limited to kdevelop? Wouldn't it be smart to create a teamwork-lib, which could then be used by kword, kate, kvim etc? Right now, it seems that only kdevelop will benefit?\n\n - Will zeroconf be used to find each other on the network?"
    author: "me"
  - subject: "Re: kdevelop-teamwork"
    date: 2006-07-03
    body: "I remember that the abiword developers presented a similar framework for edits at a conference."
    author: "Matze"
  - subject: "Re: kdevelop-teamwork"
    date: 2006-07-03
    body: "That framework has already been brought up when the topic of collaborative editing for KOffice was discussed a while ago but it is completely abiword-specific.\n\n"
    author: "cm"
  - subject: "Re: kdevelop-teamwork"
    date: 2006-07-03
    body: "\"- Will zeroconf be used to find each other on the network?\"\n\nI think Jabber would be a better choise"
    author: "David"
  - subject: "Re: kdevelop-teamwork"
    date: 2006-07-03
    body: "Why is jabber a better choice, explain"
    author: "chris"
  - subject: "Re: kdevelop-teamwork"
    date: 2006-07-03
    body: "AFAIK zeroconf only works on LANs and not on the Internet. Jabber works also on the Internet"
    author: "Florian"
  - subject: "Re: kdevelop-teamwork"
    date: 2006-07-04
    body: "and if KDE gets a chat framework, they should use that ;-)\n\n(decibel?)"
    author: "superstoned"
  - subject: "Re: kdevelop-teamwork"
    date: 2006-07-04
    body: "\"Congratulations for attempting to post to KDE Dot News!\"\n\nHuh, thanks, I know, it was quite challenging for me!\n\nBack to the topic: Noone told me yet why this isn't a teamwork-lib, to be used by koffice, kate etc. Doesn't anyone know?"
    author: "me"
  - subject: "Re: kdevelop-teamwork"
    date: 2006-07-04
    body: "> Noone told me yet why this isn't a teamwork-lib, to be used by koffice,\n> kate etc. Doesn't anyone know?\n\nI think this is a question for David Nolden (aka zwabel, the author of the teamwork mode) himself. Obviously he doesn't read the dot comments, so you can try hitting him in #kdevelop (he's sometimes there, but not that often) or on the kdevelop-devel mailing list. I think the only knowledgeable people are David Nolden and Matt Rogers (aka mattr), so good luck on the dot :P"
    author: "Jakob Petsovits"
  - subject: "Re: kdevelop-teamwork"
    date: 2006-07-04
    body: "> - Why is this limited to kdevelop?\n\nWell, it was listed in KDevelop's Summer of Code project suggestions, and obviously there was little thought of generalizing it. I also don't know how many parts could actually be reused for generic collaboration features, and probably it won't be too hard to get those general-purpose features out of the KDevelop-specific lib and make a more generic one.\n\nEspecially for a Summer of Code project, it's important not to do everything at once, or the student may get lost easily. Just do it like KOffice: add interesting stuff to the KOffice-libs, and if they are mature enough, adapt them for kdelibs. Time will tell if the teamwork mode will be used all over KDE, till then, let's do one thing really good (TM) instead of a little bit of everything."
    author: "Jakob Petsovits"
  - subject: "Web SIte Down..."
    date: 2006-07-04
    body: "FYI, the website is down with an \"Account Disabled\" message.  Guess he got Dot-slashed :-)\n\nJohn."
    author: "Odysseus"
  - subject: "Site down?"
    date: 2006-07-04
    body: "I can't get to the digest. Instead I get an \"Account Disabled\" message."
    author: "Sheldon Lee-Wen"
  - subject: "freedb"
    date: 2006-07-04
    body: "How will the freedb closedown affect Amarok?\n\nhttp://freedb.org/"
    author: "Muk"
  - subject: "Re: freedb"
    date: 2006-07-04
    body: "You can set your own CDDB-server in amaroK, so I don't expect that the current situation at freedb would affect amaroK much."
    author: "Rinse"
---
In <a href="http://commit-digest.org/issues/2006-07-02/">this week's KDE Commit-Digest</a>: PDF hyperlink and file editing support in KViewShell. DVI format support in <a href="http://developer.kde.org/summerofcode/okular.html">Okular</a>. Continued progress in "WorKflow", "GMail-style conversation view for KMail" and "KDevelop-teamwork" <a href="http://developer.kde.org/summerofcode/soc2006.html">Summer Of Code</a> projects. <a href="http://bsfilter.org/index-e.html">BsFilter</a> and <a href="http://dspam.nuclearelephant.com/">DSpam</a> tools are now supported in the <a href="http://kmail.kde.org/">KMail</a> anti-spam wizard. <a href="http://last.fm/">LastFM</a> stream support becomes more robust and polished, alongside other notable development work in <a href="http://amarok.kde.org">Amarok</a>. Aesthetic modifications made in <a href="http://edu.kde.org/kmplot/">Kmplot</a> and <a href="http://edu.kde.org/kalzium/">Kalzium</a>. KDE 4 changes: Work begins on the "Cokoon" widget style, and KSpell2 is renamed "Sonnet" in preparation for some interesting development work.

<!--break-->
