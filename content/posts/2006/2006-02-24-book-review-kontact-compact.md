---
title: "Book Review: Kontact Compact"
date:    2006-02-24
authors:
  - "hkwint"
slug:    book-review-kontact-compact
comments:
  - subject: "I love and use Kontact, but..."
    date: 2006-02-24
    body: ".. it still has some serious issues, some of which it has had for years and have hundreds upon hundreds of votes.\n\n- You still can't set an HTML signature. In many companies such templates are considered standard and mandatory. (Bug 81989)\n\n- You still can't reply to an HTML email without losing all formatting and having it appear in a fixed-width font to Outlook users. (Bug 86423)\n\n- It still does not properly handle meeting requests sent from Exchange (Bug 121055), and it also doesn't send the responses properly when you accept / deny those requests.\n\nMost of it's issues revolve around using it in a heterogenous workspace, where most people are using Windows with Outlook and Exchange. It is really hard to defend these issues on a day-by-day basis, especially when Evolution* and Thunderbird handle most of them with ease. \n\n* I would use Evolution, but I really can't stand it. It is very slow and has horrible filtering and threading compared to KMail.\n\n"
    author: "Jason Keirstead"
  - subject: "Re: I love and use Kontact, but..."
    date: 2006-02-28
    body: "\"- You still can't set an HTML signature. In many companies such templates are considered standard and mandatory. (Bug 81989)\"\n\nAre they? I just have a signature with the text I need in it, and nobody gives a toss what it looks like.\n\n\"- You still can't reply to an HTML email without losing all formatting and having it appear in a fixed-width font to Outlook users. (Bug 86423)\"\n\nThe way to handle mail is primarily in a plain text format, and handle any formatting within the mail client. That's Kontact and KMail's central message, and not a bad one. Outlook is treated as a second class citizen because it is a third class e-mail client, and with enough drilling people will get that message.\n\nI think one or two parts of KMail are just plain illogical, regardless of what Outlook does, but I generally agree with the direction they're going in there.\n\n\"especially when Evolution* and Thunderbird handle most of them with ease.\"\n\nEvolution and Thunderbird handle Exchange? Wow.\n\nThe best way of handling this is to get you off Exchange."
    author: "Segedunum"
  - subject: "Re: I love and use Kontact, but..."
    date: 2006-03-09
    body: "\"I just have a signature with the text I need in it, and nobody gives a toss what it looks like.\"\n\n*You* have a given signature and nobody gives a toss about it.  Don't expect others to have your same needs.\n\n\"The way to handle mail...\"\n\n*Your* way to handle mail...  Others, no matter how wrongly, manage mail in quite different ways.\n\n\"The best way of handling this is to get you off Exchange.\"\n\nAnd then, your proposed strategy to get off of Exchange is by trying to challenge the current champion on your rules?\n\nThat *never* works."
    author: "The Critical eye"
---
Some people still think KDE can not do groupware as well as proprietry systems.  I found on the <a href="http://www.kde.org/stuff/books.php">KDE site</a>, the German publisher <a href="http://www.bomots.de/bestellen.htm">Bomots</a> offers a German book about <a href="http://pim.kde.org">Kontact</a>, KDE's Personal Information Manager. I decided it was time to see if this book, "Kontact Kompact" by Andre Schreiber, is useful for people looking for a replacement groupware client, and people willing to learn Kontact. Read <a href="http://lxer.com/module/newswire/view/54485/index.html">the book review at LXer</a>.


<!--break-->
