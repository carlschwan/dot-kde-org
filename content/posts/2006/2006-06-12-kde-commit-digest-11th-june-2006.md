---
title: "KDE Commit-Digest for 11th June 2006"
date:    2006-06-12
authors:
  - "dallen"
slug:    kde-commit-digest-11th-june-2006
comments:
  - subject: "amaroK"
    date: 2006-06-11
    body: "Why was it renamed? I thought they cleaned out all legal problems."
    author: "AJ"
  - subject: "Re: amaroK"
    date: 2006-06-11
    body: "??? Its one of the first things in the digest. Guess you didn't read it, did you :D"
    author: "superstoned"
  - subject: "Re: amaroK"
    date: 2006-06-11
    body: "\"amaroK\" was renamed to \"Amarok\".  The only change was the capitalization."
    author: "Corbin"
  - subject: "brown?"
    date: 2006-06-11
    body: "> /trunk/playground/artwork/Oxygen/theme/svg/filesystems:\n> changing the defualt folder color to brown\n\nWTF?"
    author: "anonymous"
  - subject: "Re: brown?"
    date: 2006-06-11
    body: "As a matter of fact it looks rather good (not brown as such, but the icons)"
    author: "james brown"
  - subject: "Re: brown?"
    date: 2006-06-11
    body: "yes the icons do look good\nit's the changes in the colors who leave me a bit disappointed\n\nhttp://commit-digest.org/issues/2006-06-11/moreinfo/549548/#visual\nhttp://commit-digest.org/issues/2006-06-11/moreinfo/549822/#visual\n\ni find the originals much better looking than the modified versions\n..and the curl on the document icon looks very bad IMHO\nbut i understand that it's a working in progress"
    author: "anonymous"
  - subject: "Re: brown?"
    date: 2006-06-11
    body: "How many blue folders do you have?"
    author: "me"
  - subject: "Re: brown?"
    date: 2006-06-11
    body: "I have many blue folders, and no brown one"
    author: "Sergio"
  - subject: "Re: brown?"
    date: 2006-06-12
    body: "I think most of my (real life) folders are pink or blue. I don't think I have any brown ones."
    author: "James"
  - subject: "Re: brown?"
    date: 2006-06-12
    body: "\"How many blue folders do you have?\"\n\nWell, I have enough of brown folders not to want them any more. \n\nSeriously, no amount of brown-ification will make KDE completely palatable for Gnomies. Running after another group of customers can be very very bad for the current ones."
    author: "Daniel \"Suslik\" D."
  - subject: "Re: brown?"
    date: 2006-06-11
    body: "Is it a fake? An april fool? ;-)\n\nI like much more the blue folder than the depressing brown!\n\nAnd by the way, the folder presented at http://www.oxygen-icons.org/?cat=3 was also more enjoying!\n\nI understand you want more real-looking icons, but it's too much.\nPlease make a better compromise between vibran colors and dark colors."
    author: "Sebien"
  - subject: "Re: brown?"
    date: 2006-06-12
    body: "Must be bad Ubuntu influence."
    author: "Anonymous"
  - subject: "Re: brown?"
    date: 2006-06-12
    body: "Bleah! Please give us blu back! :'((((\nPLEASE..."
    author: "Giovanni"
  - subject: "Re: brown?"
    date: 2006-06-12
    body: "i was chatting with ken about this today and it was just an experiment to see how it looks and to see if anyone was actually paying attention. the latter is obviously a \"yes\"... =)\n\nnote that the other folder icons with the emblems on them didn't become brown.\n\nanyways ... one of the issues with bright blue icons is that it makes for a pretty bright and loud desktop. but brown is a questionable alternative. we talked about going manilla with blue highlights (e.g. on the folder's tab).\n\nbut yeah, don't worry about kde4 coming out with depressing brown folders. =)"
    author: "Aaron J. Seigo"
  - subject: "Re: brown?"
    date: 2006-06-12
    body: "What about eye-searing Orange!  It was very popular in early builds of Dapper ;)"
    author: "Robert Knight"
  - subject: "Re: brown?"
    date: 2006-06-12
    body: "It would probably make the release very popular with the Dutch ;-)"
    author: "Andr\u00e9 Somers"
  - subject: "Re: brown?"
    date: 2006-06-12
    body: "I am affraid the WK will be over before the kde4 release :p"
    author: "Mark Hannessen"
  - subject: "Re: brown?"
    date: 2006-06-12
    body: "WK as in World _K_up?  Ouch. "
    author: "cm"
  - subject: "Re: brown?"
    date: 2006-06-12
    body: "I would try to use as many glass-like/gray colors as possible, and add small accents of bright colors. The colorful accents will really stick out that way while the overall look of the desktop is still quite subtle.\n\nJust an idea.."
    author: "Pingwing"
  - subject: "Re: brown?"
    date: 2006-06-12
    body: "I agree - brown's not my cup of tea. I prefere more alive colours - if I wanted to have damper colours I'd use a different hue of blue or green or even gray.\n\nBut if all comes to worse - I'll just change the default iconset for another one.\n\nHere's an idea: what about if you add two or more default iconthemes? KDE already comes with several styles that you can choose from when you first run it - why not do the same with icons? Then the brown-addicts and the blue-addicts can be both happy and probably some pink-lovers, gray-chillers and others as well..."
    author: "Matija \"hook\" &#352;uklje"
  - subject: "Re: brown?"
    date: 2006-06-12
    body: "well, we do ship with a few icon themes though the selection is generally between \"default beauty\", \"lo-fi\" and \"for accessibility purposes\". an icon theme is a stupidly huge amount of work, which is why you see so many themes and so few complete icon sets."
    author: "Aaron J. Seigo"
  - subject: "Re: brown?"
    date: 2006-06-12
    body: "I know. But what I'm saying is - if you're so far ahead with the iconset, I think it shouldn't take as much effort to just release two versions of the same set only in a different colour (it's SVG, right?) instead of making two completely different sets.\n\nIt was just a thought though... I'd probably prefere anything over brown (it just seems ...well, muddy, to avoid a more vivid description) - have you thought about pastel hues? Pastel green or violet/lavender, IMHO, would make it softer and easier on the eyes - look at the <a href=http://www.kde-look.org/content/show.php?content=28684>Ricebowl icon theme</a> for example where gray and pastel colours are dominant.\n\nBut, please, no brown ...it reminds me of mud and toilet sessions. o_O"
    author: "Matija \"hook\" Suklje"
  - subject: "Re: brown?"
    date: 2006-06-12
    body: "Ok, I've changed one of the folder SVG's colour fills and changed it to a more lavender-pink-ish tone (should be more lavender-like, but I suck)\n\nhere's the little bugger:\nhttp://matija.suklje.name/files/violet-ish_oxygen_folder.svg\n\nPlease ...take take this mock-up with reserve - it could have been done sooooo much better, but\n1) I'm a lawyer and suck at CG (it should have a more lavender-like hue)\n2) I did it in 2 minutes, because I'm very VERY short on time because of the exams I'm having this friday.\n\nAlso, I'm not trying to say anything bad about the artists - they probably put a huuuuuuge amount of effort into shaping these SVG's and make the colour transitions. I just made a quick \"hack\" to them, to help back up my arguments."
    author: "Matija \"hook\" Suklje"
  - subject: "Re: brown?"
    date: 2006-06-13
    body: "Please please please! Do NOT do this! Blue might not be thje best, but brown is way much worse!\n\nJo"
    author: "Jo \u00d8iongen"
  - subject: "Re: brown?"
    date: 2006-06-14
    body: "I think that blaming for the color of a folder of a work in progess theme in artwork/playgound while there is a full clear \"readme\" file is just a waste of time. Like the time I've just wasted to reply to this ... too late :) "
    author: "David"
  - subject: "Re: brown?"
    date: 2006-08-21
    body: "People, I reported https://bugs.kde.org/show_bug.cgi?id=132749 so that the developers do not forget to return the colour to blue. As of today, this misdeed has not been corrected. Please add your full 20 votes each and bring back our blue icons!"
    author: "Shriramana"
  - subject: "Decibel and universally avaiable communication"
    date: 2006-06-11
    body: "So Decibel (http://decibel.kde.org) gets a first (unofficial) announcement.\nThe reasoning behind it is not quite obvious yet at first sight, but it could mean that for chatting or voice you don't need Kopete or rely on your chat app, but could do it from anywhere on the desktop with the help of this backend framework. The use cases will still have to clear up. Decibel gives a reasoning why they came to the conclusion to favour Tapioca (http://tapioca-voip.sourceforge.net/wiki/index.php/Tapioca) over Telepathy (http://telepathy.freedesktop.org/wiki/).\n\nBut on the other hand a Kopete developer writes how they could change Kopete to use Telepathy \n(http://blog.bepointbe.be/index.php/2006/06/09/14-instant-messaging-integration-into-the-desktop)\n\nWithout any more knowledge, it looks like they should come to a common ground to avoid drifting apart which would be hard to overcome later... This seems to me as if Juk or Amarok would be defaulting to and using Gstreamer instead of Phonon (or even worse, if Telepathy is no backend for Decibel which looks like the abstraction layer to only Tapioca)."
    author: "Anonymous"
  - subject: "Re: Decibel and universally avaiable communication"
    date: 2006-06-11
    body: "Would be interesting to see support for Skype, Woize, SIP etc in KDE. Kopete?"
    author: "ale"
  - subject: "Re: Decibel and universally avaiable communication"
    date: 2006-06-11
    body: "Perhaps they should take an approach with communication service like phonon does. See also this blog here:\nhttp://frinring.wordpress.com/2006/06/11/its-about-contacts/"
    author: "Anonymous"
  - subject: "Re: Decibel and universally avaiable communication"
    date: 2006-06-11
    body: "Decibel is like Phonon. It's KDE's layer to provide the desktop and apps with the functionality of the backend. So imho it would be wise if Kopete used Decibel. The backend for Decibel, Tapioca, then provides the plugins for protocols like ICQ or VOIP. So the functionality is bundled in the backend, where the work would be done to properly support the different protocols.\nThe app itself could then concentrate work on being usable and easy to work with. Also there would be the possibility to add communication ability to other parts of the desktop (light-weight chat instead of full-blown app)."
    author: "Anonymous"
  - subject: "Open Wengo has a good VoIP library"
    date: 2006-06-12
    body: "Another good Voice over IP (VoIP) client is Open Wango:\n\nhttp://OpenWengo.org\n\nIt has several advantages:\n\n* Open Wengo has strong developer and commercial support from France's leading VoIP provider\n* Open Wengo is 100% Free & Open Source software (GPL-licensed)\n* The backend library is clearly separate from the GUI frontend, making it easy to incorporate in applications.\n* The existing GUI frontend is Qt-based and crossplatform (Linux, Windows, and Mac OS X)"
    author: "Vlad"
  - subject: "Re: Open Wengo has a good VoIP library"
    date: 2007-09-12
    body: "Openwengo is not ready for wide use, to many crashes, luck of essential functions (like info retrieval for a contact, not capable for multiple SIP profiles). However, It does have videocall feature. \nIMO Skype still lead VoIP market (no SIP, which is bad ).\nI'm still looking for a good SIP Qt4 client."
    author: "Dmitri"
  - subject: "Tenor?"
    date: 2006-06-12
    body: "What ever happened to the Tenor project?  The Commit-Digest listed every important KDE4 project but Tenor.  Does anyone know the status of Tenor or if it is being developed at all?\n\nProject Interview:\nhttp://www.linuxplanet.com/linuxplanet/reviews/5816/4/\n\nAppeal projects (including Tenor):\nhttp://appeal.kde.org/wiki/Projects\n"
    author: "Joe"
  - subject: "Re: Tenor?"
    date: 2006-06-12
    body: "http://mail.kde.org/pipermail/klink/2006-April/000133.html\n\nSummary: No hype for now, please."
    author: "Anonymous"
  - subject: "Re: Tenor?"
    date: 2006-06-13
    body: "Tenor has been replaced by kitten, real activity and real code in svn."
    author: "WTAC"
  - subject: "Curious myself."
    date: 2006-06-14
    body: "What is the current state of native search for KDE? I know we currently have Kerry but that's based on Beagle. Kat has stagnated and Tenor is quiet. No word on clucene. Kitten looks interesting. Is it intended to replace Kat? How is all this going to be integrated? (I currently use Recoll/Xapian myself.)"
    author: "sundher"
  - subject: "Font size"
    date: 2006-06-12
    body: "I'm probably not the first to ask it, but I'm quite curious as to why the font of the digest is so large?"
    author: "St. Kevin"
  - subject: "Re: Font size"
    date: 2006-06-12
    body: "I think the font size and type is perfect. Makes it stand apart from other technical documents, and it's easier to read."
    author: "Isaac"
  - subject: "Re: Font size"
    date: 2006-06-12
    body: "yes, a SANE font size would be very welcomed\nat least make it an option.."
    author: "anonymous"
  - subject: "Re: Font size"
    date: 2006-06-12
    body: "Font size looks fine here.\n\nctrl + -"
    author: "Jonathan Dietrich"
  - subject: "I've just added the font size option!"
    date: 2006-06-14
    body: "Quite a few people have asked why the font size is so large (of course, it really depends on your font settings), so i've just added the setting to the options panel of the digest:\n\nhttp://commit-digest.org/options/\n\nEnjoy :)\nDanny"
    author: "Danny Allen"
  - subject: "Of ZIP icon"
    date: 2006-06-12
    body: "Am I correct thinking this: http://commit-digest.org/issues/2006-06-11/files/549822/now/zip.png\nis the current proposal for ZIP icon? If yes - please, change it. \nZIP only in English means what is shown on that picture, in (most) other languages it is just meaningless three letters. It might be a clever pun for English KDE users, but for the rest of us it is just going to cause an AWFUL lot of confusion. Please replace it with a standard package icon or something that doesn't depend on ZIP meaning well, a zip."
    author: "ac"
  - subject: "Re: Of ZIP icon"
    date: 2006-06-12
    body: "It would be great if all the compressed archives would follow the same theme icon-wise. At the end of the day, in KDE where we have Ark it is more importaint to see from the first glance that it's an archive then a .zip or .gz Which type of archive it is, would be better to find out on the second glance.\n\nWhat you want is:\n*glance*\nOh, in this folder I have an archive - I'll just open/uncompress it.\n*look*\nWho sent me a .rar? I usually use tarballs. Oh, well, nevermind.\n\nWhat is less desirable:\n*glance*\nWhere's that archive he sent me? Ok, I see archives ...but where's that one?\n*look*\nOh, here's the bugger - damn, .zip's always looking different from the rest of the archives.\n\nOr even worse:\n*glance*\nOh, dear god! So many different icons!!\n*concentrate*\nOK, so in what format did he send it to me? Was it a .zip or a tarball?\n*look for tarballs*\nNope, it wasn't a tarball ..geeez...\n*look for .zip*\nNope, there's no .zip icon with that name as well... Man, I'll probably find it faster if I just use the console...\n\np.s. I did *not* look at other archive icons - couldn't find them. I only persume they're different then a .zip, because a tarball (ball of tar, haha) would look pretty silly with a zip on it - as would an .arj"
    author: "Matija \"hook\" Suklje"
  - subject: "Re: Of ZIP icon"
    date: 2006-06-12
    body: "I like it very much. I think it's very funny please keep it don't change it! By the way I'm not English and I still enjoy it :) "
    author: "Patcito"
  - subject: "Re: Of ZIP icon"
    date: 2006-06-12
    body: "It is funny.\n\nBut it looks aweful as a mimetype IMHO - especially in the .tgz example\n\nhttp://websvn.kde.org/*checkout*/trunk/playground/artwork/Oxygen/theme/svg/mimetypes/tgz.svg?rev=549511"
    author: "Matija \"hook\" Suklje"
  - subject: "Re: Of ZIP icon"
    date: 2006-06-12
    body: "well IMVHO it looks great."
    author: "Patcito"
  - subject: "Re: Of ZIP icon"
    date: 2006-06-12
    body: "But you KNOW English. Not everyone does, and for them this icon is going to be a huge \"WTF?!\". "
    author: "ac"
  - subject: "Re: Of ZIP icon"
    date: 2006-06-13
    body: "in my country most people use the same word for ZIP as in English. And in every country people that know fashion a bit call it zip too. Ask your gf :)\nCan't KDE be a bit cool and sexy? as long as we don't use the same icon as Windows, people will get lost anyway so let's get them lost in a cool and fashion and sexy way damnit (-:"
    author: "Patcito"
  - subject: "Re: Of ZIP icon"
    date: 2006-06-13
    body: "I still think the humoungous zip is annoying.\n\nI've gone through the webSVN of Oxygen and looked at pretty much two thirds of all the icons that are submited there already ...and I don't like it.\n\nIt's just me, don't worry - I just prefere icons that don't kick me in the face with what they're all about ...you know, I'm more of the timid icon guy - I like the icons that are pretty, but don't scream at you, so you can ignore them and concentrate on the more importaint jobs. :]\n\nMaybe, the Oxygen people are right! - Oxygen iconset is there as default -> default is there to help complete newbies to find their way around -> so, default has to scream at them what's what\n...meaning, a lot of oldbies don't like default (because they know what's what and they don't like the screaming) -> that's why things are themable and why we have kde-look.org :]\n\n...now if only I could get the guys behind Sparkling, Ricebowl and Zenith to update those iconsets ;)\n\nOh, even if they are - the zip is still bloody annoying!! Sweet merciful heavens - just look at the icon for .tgz! It's a box with a humoungous zip over it!! O_o"
    author: "Matija \"hook\" Suklje"
  - subject: "Re: Of ZIP icon"
    date: 2006-06-13
    body: "> in my country most people use the same word for ZIP as in English.\nWell, here they don't\n\n> And in every country people that know fashion a bit call it zip too. \nHere they don't, at least I have never heard that. And I would be suprised if it was used here because the word zip would have a really weird declension.\n\n> Ask your gf :) \nWell, something is wrong if our hypothetical user would have to ask his girlfriend to know what is the icon used for archives. Especially since he first would have to KNOW that he should ask his gf. \n\n> Can't KDE be a bit cool and sexy?\nYes it can be, but not at a cost of usability. There are many ways which don't carry the same price tag.\n\n> as long as we don't use the same icon as Windows, people will get lost anyway so let's get them lost in a cool and fashion and sexy way damnit (-:\nWe don't have to use the same icon as Windows, even if we don't want to confuse users. I've never seen anyone confused by KDE's folder icon, despite the fact that it is not identical to Windows. \n\nAnd would you be happy if someone replaced the icon for OGG files with a sphere, because ogg means sphere in some language you don't know? What if for the same reason HTML files became dogs, JPEG  turned into little houses, MPEG  changed into a dish of spaghetti? What if icon for folder became a helmet, because in KDE3 they were blue, and blue is Lancelot's favourite colour, and the real Lancelot probably wore a helmet?\n\nThe main function of a desktop environment is to help user do his work. If we can keep the desktop pretty (for example by adding more blue into colour schemes, background, and everywhere else), then it's good, but to put an obstacle in that workflow just to make the desktop a bit cooler is not a good idea IMHO."
    author: "ac"
  - subject: "Re: Of ZIP icon"
    date: 2006-06-13
    body: "> in my country most people use the same word for ZIP as in English.\n >>Well, here they don't\nare you sure? did you try \"zip <pant in your language>\" on google? I'm sure you'll get tones of accurate results. Most jeans model have ZIP in there name :-)\n\n> Ask your gf :) \n>> Well, something is wrong if our hypothetical user would have to ask his gf\nusually when people don't get the icon, they look at the file extention. KDE displays them on default so if he/she knows what a zip file is he'll figure it out. If he/she don't know then I'm afraid no icon will help. In the worse case he/she could just click on it and see what happen. \n\n> I've never seen anyone confused by KDE's folder icon\nwell believe me cause I did\n\n>for example by adding more blue into colour schemes, background, and everywhere else\nenough of blue, this is so 90, can't we get away from this color? how about some nice orange? not brown but bright orange, it really looks great\n"
    author: "Patcito"
  - subject: "Re: Of ZIP icon"
    date: 2006-06-13
    body: "> in my country most people use the same word for ZIP as in English.\n>> Well, here they don't\n> are you sure? did you try \"zip <pant in your language>\" on google? I'm sure you'll get tones of accurate results. Most jeans model have ZIP in there name :-)\n0 results. Not much for a language with some 40 mln speakers...\n \n\n> usually when people don't get the icon, they look at the file extention. KDE displays them on default so if he/she knows what a zip file is he'll figure it out. If he/she don't know then I'm afraid no icon will help. In the worse case he/she could just click on it and see what happen. \nIn the worst case a user wil spend a minute or more looking for a file which IS there, only with a totally unexpected icon. I for example know what a ZIP file is, and considering  that Windows uses package, KDE/Crystal uses a package, basically everything I used uses package for archives, I would spend some time looking for a package, and disregarding that zip picture (had I not found about it earlier). Probably even despite the fact that I know what the word zip means in English.\n\n\n>>for example by adding more blue into colour schemes, background, and everywhere else\n> enough of blue, this is so 90, can't we get away from this color? how about some nice orange? not brown but bright orange, it really looks great\nDEATH BEFORE ORANGE (or anything that isn't blue)!!! "
    author: "ac"
  - subject: "Re: Of ZIP icon"
    date: 2006-06-13
    body: "Patcito:\nin my country most people use the same word for ZIP as in English. And in every country people that know fashion a bit call it zip too. Ask your gf :)\n\nac:\nHere they don't, at least I have never heard that. And I would be suprised if it was used here because the word zip would have a really weird declension.\n\nPatcito:\nare you sure? did you try \"zip <pant in your language>\" on google? I'm sure you'll get tones of accurate results. Most jeans model have ZIP in there name :-)\n\nac:\n0 results.\n\nThis is a really weird conversation. I realize that there are people on this planet that believe in strange things, including a bizarre theory such as \"zip means the same in every language on earth.\" \n\nBut what on earth would make someone insist on this being true even in the face of a native speaker asserting the opposite?"
    author: "Martin"
  - subject: "Re: Of ZIP icon"
    date: 2006-06-13
    body: ">0 results. Not much for a language with some 40 mln speakers...\nok, type \"zip jeans\" in your local google, if there is still no results then I guess you won :) by the way what is your country?"
    author: "Patcito"
  - subject: "Re: Of ZIP icon"
    date: 2006-06-13
    body: "> 40 mln speakers\nI guess you might be from Poland:\nhttp://www.google.pl/search?hl=pl&inlang=pl&ie=ISO-8859-2&q=zip+jeans&btnG=Szukaj&lr=lang_pl\n\nor Spain:\nhttp://www.google.es/search?hl=es&ie=ISO-8859-1&q=zip+jeans&btnG=B%FAsqueda&meta=lr%3Dlang_es\n\nsee, that's thousands of results :)"
    author: "Patcito"
  - subject: "Re: Of ZIP icon"
    date: 2006-06-13
    body: "Are you for real?\n\nLook, here is what my all-knowing computer told me just now:\n\ndelphi> zip-means-the-same-in-every-human-language\nfalse\ndelphi> _\n"
    author: "Martin"
  - subject: "Re: Of ZIP icon"
    date: 2006-06-13
    body: "I'm beginning to suspect you are just prolonging this discussion for some twisted kind of fun, but in case you are serious:\nActually Spanish has ~300 mln native speakers, that's more than English (Hint: Spanish is the native language not only in Spain). Just a note, so you don't think English is the most important of the languages KDE supports, and \"if English know what ZIP means everyone else is insignificant\".\n\nFirst one is actually written half in Polish, half in English, and ZIP is used in English part.\nSecond, in rough translation: \nJeans, blah blah, are cool, blah, blah, our shop is the best, blah blah blah, you can download our offert here: our_prices.ZIP\nAnother random one: \"Sexy front-ZIP wetlook pants with pockets flared legs\" - even you must suspect it's not Polish.\nAnd so on.\nZIP. Does. NOT. Mean. ANYTHING. To. Average. Non-English-speaking. Person. Using. KDE!"
    author: "ac"
  - subject: "Re: Of ZIP icon"
    date: 2006-06-13
    body: "and how about that one then? :)\nhttp://skateshop.pl/search.php?text=zip"
    author: "Patcito"
  - subject: "Re: Of ZIP icon"
    date: 2006-06-14
    body: "They are SKATES for goodness sake! In other words, a subculture widely known for using a weird mix of Polish, English and \"yo!\"s thrown in between. \n\nSigh. Must we really go through the entire Internet, or will you believe me that an average Pole does not know that zip and zamek b&#322;yskawiczny are the same thing?"
    author: "ac"
  - subject: "Re: Of ZIP icon"
    date: 2006-06-14
    body: "Okay, prehaps this will convince you:\n\nSearch for \"zip\" in Polish Language Dictionary by PWN yelds only \"zipa&#263;\", which means \"to breathe heavily, to be winded\":\nhttp://sjp.pwn.pl/slowo.php?co=zip\n\nSearch for \"zip\" in Dictionary of words borrowed into Polish from other languages by PWN: 0 results:\nhttp://swo.pwn.pl/slowo.php?co=zip\n\nIn Dictionary of borrowed words by Kopali&#324;ski: http://www.slownik-online.pl/cgibin/search?charset=utf-8&words=zip&Submit=Szukaj\nwe get: \npoptop\nziptop\nziptop\nzip code\npoptop\nMultislownik\nDiscarding the obviously wrong, we are left with ziptop twice defined as a type of can from which lid can be removed by pulling at a metal strip around the edge of the lid, and zip code which means the area code system from US.\n"
    author: "ac"
  - subject: "Re: Of ZIP icon"
    date: 2006-06-14
    body: "ok but then how about that?\nhttp://pl.wiktionary.org/wiki/zip"
    author: "Patcito"
  - subject: "Re: Of ZIP icon"
    date: 2006-06-14
    body: "So the guy who invented ZIP compression was wrong in call it ZIP."
    author: "David"
  - subject: "Re: Of ZIP icon"
    date: 2006-06-14
    body: "Note the \"Kategorie stron: angielski (indeks)\" (\"Page cathegory: English (index)\") annotation at the bottom. Wikidtionary.pl contains many English words, such as http://pl.wiktionary.org/wiki/always or anything else in the English cathegory here: http://pl.wiktionary.org/wiki/Kategoria:angielski_(indeks)\n\nSo zip is listed as an English word. There is no such word in Polish language.\n\n"
    author: "ac"
  - subject: "Re: Of ZIP icon"
    date: 2006-06-13
    body: "Basing a visual cues on words or word associations is a big no-no. It should be based on the concept only. An archive, package, etc. Just basic industrial design common-sense.\n\nAnd in my language, ZIP means nothing. I showed that \"zip\" icon to friends who use computers everyday, and I needed to tell them what it stands for. Don't project assumptions based in YOUR culture on the whole world. That's a slippery slope."
    author: "Isaac"
  - subject: "Re: Of ZIP icon"
    date: 2006-06-13
    body: "> in my country most people use the same word for ZIP as in English. And in every country people that know fashion a bit call it zip too. Ask your gf :)\n\nThe funny thing is that in Turkey if I say zip the same way as it is pronounced in English my gf would think I talk about a/my penis :-)"
    author: "WTAC"
  - subject: "Re: Of ZIP icon"
    date: 2006-06-14
    body: "ok, but even if the icon represent a vagina you will have to call the file a ZIP file :) isn't fun?\n\nAll of this is pointless. Mac OS X which is the most usable OS around uses the same metaphor."
    author: "Anonymous"
  - subject: "Re: Of ZIP icon"
    date: 2006-06-13
    body: "You defnitely have a point. The classic package together with the known .akro.nym of the archive is the better choice, as it's more universal than a zipper."
    author: "Carlo"
  - subject: "Re: Of ZIP icon"
    date: 2006-06-17
    body: "ZIP files were called so because it is similar to a \"zip\". The most important thing is what is a zip not how it is called. But personally, I think that the ZIP icon is simply bad. You have to keep the same concept for all archives (GZIP, RAR, ACE, TAR, ...) . I suggest a yellow 3D box with a colored band, each color for each archive type and small letters written on the band."
    author: "Youssef"
  - subject: "commit in other languages"
    date: 2006-06-12
    body: "Nice, the commit is also available in other languages:\n\nhttp://commit-digest.org/issues/2006-06-11/?translation=nl\nhttp://commit-digest.org/issues/2006-06-11/?translation=it\nhttp://commit-digest.org/issues/2006-06-11/?translation=de\nhttp://commit-digest.org/issues/2006-06-11/?translation=se\nhttp://commit-digest.org/issues/2006-06-11/?translation=pt\n\netc. etc.\n\nJust change the language setting at http://commit-digest.org/options/\n"
    author: "AC"
  - subject: "Re: commit in other languages"
    date: 2006-06-12
    body: "yes, danny and the translators are doing an excellent job with the digest\nthanks denny!"
    author: "anonymous"
  - subject: "Kolourpaint troubles"
    date: 2006-06-13
    body: "Hmm, is it just me or is it a bit dissapointing to see Clarence's comments on the paint engine in Qt4?  \"This will eventually allow us to drop Arthur/Qt4 and use a proper paint engine\"\n\nI don't have much experience with it, but I thought that the Arthur paint system was supposed to be much improved over the old Qt3 system.  It would seem to me that an app like kolourpaint is exactly the sort of thing Arthur was designed for. "
    author: "Leo S"
  - subject: "Re: Kolourpaint troubles"
    date: 2006-06-13
    body: "Indeed, I wondered about the same thing. Sounds really bad... Can't the Trolls fix it for 4.2, or maybe Clarence can send em a fix..."
    author: "superstoned"
  - subject: "Wine Module"
    date: 2006-06-13
    body: "What does it mean? A wine database?"
    author: "Fred"
  - subject: "Re: Wine Module"
    date: 2006-06-13
    body: "No, it a module to configure WINE, as in the <a href=\"http://www.winehq.com\">Windows (Non-)Emulator</a>. "
    author: "B"
  - subject: "It's AmaroK, though, isn't it?"
    date: 2006-06-13
    body: "Doesn't the press release say that they renamed amaroK to AmaroK, not Amarok?  Shouldn't the K be capitalized then?  \"Misspellings ruin our brand\"\n\nUh oh =P\n\nThought I might point that out =P"
    author: "Nick Grinyer"
  - subject: "Re: It's AmaroK, though, isn't it?"
    date: 2006-06-13
    body: "What press release do you mean?\nIf you mean the commit digest, well read again :o)\nIt's Amarok now, not amaroK or AmaroK (nor AmarOk etc..)\n"
    author: "AC"
  - subject: "Re: It's AmaroK, though, isn't it?"
    date: 2006-06-20
    body: "Goodness me, I MUST be seeing things....I'm sure there was a misspelling somewhere.  The press release I was reffering to was the one that the dev team at AmaroK released about the name change, and I thought I saw a typo on this commit digest about that...\n\nNevermind =P I tend to see lots of things at 3 in the morning."
    author: "Nick Grinyer"
---
In <a href="http://commit-digest.org/issues/2006-06-11/">this week's KDE Commit-Digest</a>: <a href="http://developer.kde.org/summerofcode/okular.html">oKular</a> gets a backend for the <a href="http://en.wikipedia.org/wiki/Djvu">DjVu</a> document format. <a href="http://amarok.kde.org/">amaroK</a> is renamed Amarok. <a href="http://kde-apps.org/content/show.php?content=18703">Guidance</a>, a modular configuration GUI, gets a <a href="http://en.wikipedia.org/wiki/Wine_(software)">WINE</a> module. Developments in the Kopete "OSCAR (AIM) File Transfer", "WorKflow" and "KDevelop C# Parser" <a href="http://developer.kde.org/summerofcode/soc2006.html">Summer Of Code</a> projects. <a href="http://www.koffice.org/kformula/">KFormula</a>, the <a href="http://www.koffice.org/">KOffice</a> formula component, defaults to the <a href="http://en.wikipedia.org/wiki/Opendocument">OpenDocument</a> format.

<!--break-->
