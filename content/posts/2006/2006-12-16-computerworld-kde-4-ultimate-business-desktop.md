---
title: "Computerworld: KDE 4, the Ultimate Business Desktop"
date:    2006-12-16
authors:
  - "jriddell"
slug:    computerworld-kde-4-ultimate-business-desktop
comments:
  - subject: "Business Desktop???"
    date: 2006-12-16
    body: "See: http://ianmurdock.com/?p=388"
    author: "art"
  - subject: "Re: Business Desktop???"
    date: 2006-12-16
    body: "You have not read the article, do you?"
    author: "Patrick"
  - subject: "Re: Business Desktop???"
    date: 2006-12-17
    body: "Which article?\nThe KDE4 article about upcoming software visions or the link?\n\nIan describes why Linux fails. Linux remains third choice because of this problem. KDE3 or KDE4 does not matter, both are ready and superiour - or will be ready. KDE4 means designer clothes for a wheelchair-bound operating system. \n\nNo, the business desktop must wait. "
    author: "art"
  - subject: "Re: Business Desktop???"
    date: 2006-12-17
    body: "100% agreed.\n\nKDE4, business? Naw...\n\nThe best KDE won't help as long as there are such issues mentioned above in Ian's article."
    author: "fish"
  - subject: "Re: Business Desktop???"
    date: 2006-12-18
    body: "Ian should blame SUN for delivering a crappy installer, not the OS he is installing on.\nThere are enough goog installers available for Linux.\nHeck, even Install Shield is available for linux!!\n\nNo idea why SUN comes with a non-executing installation script..\n"
    author: "otherAC"
  - subject: "Re: Business Desktop???"
    date: 2006-12-18
    body: "Maybe because there's no way the downloaded file can tell the system that it is executable, huh? Think of it as a security measure -- the solution isn't to make the file executable, but the UI (KDE for us) should ask the user if he likes to make it executable (with appropriate security warning).\n\nAdding some sort of signature checking that vendors could use non-intrusively (ie, noop on unsupported platforms, like a gpg signature embedded in a shell comment) for a bonus. Apparently there is demand for completely out-of-band software distribution and FHS and friends even have provisions for those (/opt, installation under /home), so i don't see why the OS would be responsible for those. Desktop and ISVs can fix all this mess rather easily (contrary to what is claimed in this thread somewhere)... Also, there's LSB which is generally not very useful, but i think it may mandate some compatibility libraries to be present on system, so apt-get install lsb-base (and that should probably be part of default install, too)."
    author: "mornfall"
  - subject: "Re: Business Desktop???"
    date: 2006-12-18
    body: "\nI put in a wishlist \"to make the file executable...with appropriate security warning)\" like 2-3 years ago and *again* this year.  Go vote for it please!!!  Nautilus did this (last time I used it...years ago) and it was great.  I really wish KDE would listen.  It's one shining example of where KDE refuses to increase usability."
    author: "Henry"
  - subject: "I'm sorry but this is kind of ..."
    date: 2006-12-20
    body: "I don't know. Seriously. This is the biggest hole in Windows, and while some (few) Windows people that come to my house sometimes complain about it, I just explain to them that it is a BIG security hole.\nTwo ways of installing something that comes from outside, and both of them HARD to do accidentally (harder than hitting OK in a dialog): run it klik-style, but in a more strict sandbox than klik does today, or install a package from a previously-authorized source.\nIn the \"business desktop\" scenario, the second one is the Only Right Thing To Do (TM).\nThe network admins set the .deb/.rpm server inside the network, customize the distro to use that server, and put there the commercial software they authorize -- after auditing its network connections/ scanning for viruses, etc.\nWant to install some software? It must be in the synaptic/adept list or else, go complain to the BOFH -- if he is not really from Hell, he will study the software you want to install and in a couple of hours/days/weeks it will be in your list and in everyone (authorized to use the software) else's list on the company... so you can install your software with a couple of clicks.\nBeen there, done that. HTH."
    author: "Humberto Massa"
  - subject: "Re: Business Desktop???"
    date: 2006-12-17
    body: "Well, you can avoid having users changing permissions to executable for your installer by putting it on a tar package. It will keep permissions, and users can easily unpack it easier than changing permissions. With a graphical interface for it (static compile libraries), users won't even need \n\nGetting dependencies right won't be as easy though. However, most distributions already ship a decent amount of common libraries, as well as an intuitive package installer with repository support, so this shouldn't be as much as of a hassle.\n\nWhat really bothers me -- I don't know if this bother the casual user, but it sure bothers me -- is the directories naming that pretty much every distribution use. There is too much clutter at root and /usr, /var, /mnt are far from intuitive names. Yeah, I don't think this will scary grand ma who should just run her session like if it was a locked virtual machine, but I find that people, even if they are not too computer inclined, like to peak at the system.\n\nBut I guess KDE can't do a thing for these stuff."
    author: "blacksheep"
  - subject: "Wrogn title..."
    date: 2006-12-16
    body: "I think the title should be:\n\nKDE 4: the ultimate vaporware desktop?\n"
    author: "ok"
  - subject: "Re: Wrogn title..."
    date: 2006-12-16
    body: "The technology that is being talked about is already in SVN, but given you're busy trolling, I can understand that you did not have the time to check that yourself.\n\nPlease check your facts."
    author: "Sebastian K\u00fcgler"
  - subject: "Re: Wrogn title..."
    date: 2006-12-16
    body: "Till now much of everithing in KDE is to much blah blah blah and we will do this and that and take a look to SVN, KDE has been on developep for a year and a half and till now there is just so litle to show and that makes me think in 2 options:\n\n1.- Everything is hiden like a secret weapon to be show in the last moment and give us a surprise, that would be nice but at the same time hypocrital since being a open source project would be lame to keep the code just for a few while is developed.\n\n2.- Is like the MS Windows Vista campaing, where there were to much presententions and blah and blah and blah and to litle to show.\n\n\nI hope its the option 1.\n\n\nSince the mayority of KDE users and people specting KDE4 are not developers and have no clue about SVN I recoment to start giving something visible and not a bunch of lines of abstract code or SVN links.\n\n"
    author: "ok"
  - subject: "Re: Wrogn title..."
    date: 2006-12-16
    body: "I agree that there is little too see (as in 'for the end-user to try out') while things all over the place are being developed. Major frameworks have been introduced to KDE4, such as Oxygen, akonadi, Phonon, Qt4, Solid, threadweaver. Not all of them are completely finished already, but it really is the time where application developers are picking up this new stuff and use it to improve their applications and build new ones. The problem with releasing the software is simply that the end user applications need to be adapted and further developed -- not at least to stabilise this basis, and that has not happened at large yet.\n\nThat new stuff is not hidden, though (in that respect, sorry ;-)), but it's not exactly user-visible either -- that's a natural characteristic of the development process. Little to see while new frameworks are being created. \n\nIf you want to follow that, however, and get to know more about what's happening, have a look an Dannya's commit-digest and the different blogs syndicated via planetkde.org.\n"
    author: "Sebastian K\u00fcgler"
  - subject: "Re: Wrogn title..."
    date: 2006-12-16
    body: "> Since the mayority of KDE users and people specting KDE4 are not developers\n> and have no clue about SVN I recoment to start giving something visible and\n> not a bunch of lines of abstract code or SVN links.\n \nthis is the most amazing piece of advice i've read in a long time. i mean, really, what *are* we thinking writing code in a repository!\n\noh wait, we're a software project, that's what we do.\n\nour aim is not to create something for you to look at this moment, our goal is to put together a rather complex set of software for eventual release and hopefully enjoy the process.\n\nas for visuals, take a look at the kde games, the work happening in the directory view parts (saw some really stuff that frederikh was working on the other day for listview delegates), in dolphin, etc .."
    author: "Aaron J. Seigo"
  - subject: "Re: Wrogn title..."
    date: 2006-12-16
    body: "Don't feed the trolls."
    author: "Mark Kretschmann"
  - subject: "Re: Wrogn title..."
    date: 2006-12-17
    body: "Funny how these types of posts are filled with typos. Just in case there are any rational readers who don't understand the software process for a major release...\n\n1) work on your specifications for your new libraries\n2) begin writing code for those libraries, test it and interact with other developers using it\n3) once library code is stable begin application rewrites on it\n4) develop until the end of that cycle and begin releasing alpha, beta and release candidates\n\nIn other words there isn't anything new to give/show users other than a description and maybe a few screenshots/mockups unless they want to compile and run development code. Claiming this is vapor in the case of KDE is pretty much ignoring three major releases that have delivered exactly what the developers promised. Claiming this has anything to do with Microsoft ignores the realities of the different development models. To see what I mean read this...\nhttp://moishelettvin.blogspot.com/2006/11/windows-shutdown-crapfest.html\n\nAlways remember comparisons need rational data, not wild speculations and and unsubstantiated claims. If KDE4 was not stacking up to be what developers said other developers would be making loud noises about it."
    author: "Eric Laffoon"
  - subject: "Re: Wrogn title..."
    date: 2006-12-17
    body: "LOL! \n\nYou are joking right? How long did it take for Microsoft to create Vista? How many of the \"killer\" features have they pulled out to get it done? How long did it take them before they had anything to show?\n\nFor how long have they been developing KDE4? Not that long really, especially not considering how relatively few they are and how much they are trying to do. KDE4 is a huge change and it will live for many years after it is released.\n\nKeep up the great work KDE developers! Really looking forward to KDE4, but I'm waiting patiently ;)"
    author: "Joergen Ramskov"
  - subject: "Re: Wrogn title..."
    date: 2006-12-18
    body: "I second this sentiment.  Don't worry about a-holes mouthing off.  Those of us who are serious users of KDE are waiting (somewhat :-) patiently for KDE4.  The progress is obvious to those who follow closely, and it seems that the \"tipping point\" is getting near."
    author: "Louis"
  - subject: "Re: Wrogn title..."
    date: 2006-12-20
    body: "I thonk it's so great the KDE team is trying to innovate. Nowdays it seems everyone is just following Apple. It's nice that someone is thinking out of the box.\n\nKeep up the good work guys  :)"
    author: "daniel b"
  - subject: "Re: Wrogn title..."
    date: 2006-12-22
    body: "Dear KDE developers,\n\nyou are doing a great job. KDE 3.5.5 is a great piece of software and if 4.0 is better than it, then it is going to be awesome. If you look how long did it take to large organisations to implement applications of a similar size, then you will understand the long timeline.\n\nBest,\n\n-- \nTechnical Support\n\neeos uk ltd (http://www.eeos.biz)\n\nCompany No. 05765416\n\n\n\n\n\n"
    author: "Technical Support @ eeos uk ltd"
  - subject: " KDE 4, the Ultimate Business Desktop?"
    date: 2006-12-16
    body: "The original headline had a question mark."
    author: "ac"
  - subject: "Sorry guys..."
    date: 2006-12-16
    body: "KDE is not a \"professional\" desktop, and is not worth developing for.  In fact, it's \"shoddy\" and isn't as good as \"window managers written by the computer club in college\".\n\nSo says the Photoshop programmers:  http://www.adobeforums.com/cgi-bin/webx?14@@.3bc1d4af/0"
    author: "Dark Phoenix"
  - subject: "Re: Sorry guys..."
    date: 2006-12-17
    body: "Interesting that they don't know what they are talking about:\n\nwell the point is that adobe would provide these things bundled in the application. look at google earth for an interface\n\nAnd that was from *proponent* of porting PS to Linux..."
    author: "m."
  - subject: "Re: Sorry guys..."
    date: 2006-12-17
    body: "Interessting thread. But let's correct the statement \"Dark Phoenix\" just changed. What the adobe-devel (and the users) sayed exatcly was;\n\n\"Yes, I have looked at Gnome and KDE lately. That's why I largely write them off as pointless. [...] What's holding Adobe off from Linux: Linux, and it's users. The \"OS\" is nowhere near ready for large desktop applications\"\n\nFunny, that Acroread and flash do exist at all. Anyway, that's another topic even if an interessting one :)"
    author: "Sebastian Sauer"
  - subject: "Re: Sorry guys..."
    date: 2006-12-17
    body: ">>What's holding Adobe off from Linux: Linux, and it's users. The \"OS\" is nowhere near ready for large desktop applications\"\n\nI think it's more that their customers are holding adobe off from linux.\nporting their large desktop applications to linux will be a large effort wich should be payed back from revenues coming from their linux sales. And apperently there is not a big enough Linux market for adobe to justify such an operation.\n\nThat linux is not ready for large desktop applications is bogus: there are enough other players in the market that have created/ported their large desktop applications for/to linux.\n"
    author: "otherAC"
  - subject: "Re: Sorry guys..."
    date: 2006-12-17
    body: "A) See: http://ianmurdock.com/?p=388\n\nB) Linux has a chance as a professional cluster rendering station for graphic specialists. In fact clustering means Linux today. That could help to roll off the graphic market, starting with professionals. \n\nC) The weakness of Linux is not KDE or Gnome or ICEWM. It is to be found exactly where these Desktop Environments do not set standards."
    author: "art"
  - subject: "Re: Sorry guys..."
    date: 2006-12-18
    body: "A: Ian should blame SUN for delivering a crappy installation program.\nSurely Adobe would be capable in delivering an installation program that would work normally on Linux.\n\nB: Linux is at the moment the key platform for a lot of animation films on the market: shrek, finding nemo...\nfor whatever reason, the complex grafical rendering applications they use have no problem running on kde (mostley on kde 2 by the way..0\nSo why should running adobes applications be a problem?\n\nC: Large applications don't care about standards. They mostley come with their own specialised toolkit/framework/etc.\nThe parts of the desktop that they need to use are already standarised.\n"
    author: "otherAC"
  - subject: "Re: Sorry guys..."
    date: 2006-12-17
    body: "Funny thing is, Photoshop isn't the type of application that needs a whole lot of desktop integration. The state of the desktop is irrelevant to the application. The user could be running a broken FVWM for all the application should care. "
    author: "Brandybuck"
  - subject: "Re: Sorry guys..."
    date: 2006-12-18
    body: "actually not. E.g. cut n' paste bewteen a number of objects works much better under windows between various applications. \n\nExamples:\n\nMatlab graph -> Outlook.\nOrigin graph -> Word -> Origin\n\n"
    author: "jd"
  - subject: "Re: Sorry guys..."
    date: 2006-12-17
    body: "Actually, I did not change anything.  Stuff in quotes was copied directly from the posts.\n\nOh, and in case it missed some people, I'm being sarcastic.  I'm a KDE user, and I prefer it to the old Windows desktop."
    author: "Dark Phoenix"
  - subject: "Re: Sorry guys..."
    date: 2006-12-17
    body: "\"KDE is not a \"professional\" desktop, and is not worth developing for. \"\n\nForgive us, poor ignorant users and coders, who won't take your word for it. Go troll around elsewhere."
    author: "l3v1"
  - subject: "Re: Sorry guys..."
    date: 2006-12-18
    body: "Guessed someone missed the sarcasm in that post."
    author: "Dark Phoenix"
  - subject: "Idiot Trolls, was Re: Sorry guys..."
    date: 2007-01-01
    body: "You trolls sure seem to be 'interested' in Linux and KDE for you to be spending so much time trolling this blog! \n\nMore and more, corporations such as Adobe are becoming irrelevent. Their overpriced bloatware is like printing money (for them) but notice that the only way they can continue to grow is my eating other companies (Macromedia) and forming a monopoly? That indicates a declining industry.\n\n"
    author: "Allen of California"
  - subject: "Re: Idiot Trolls, was Re: Sorry guys..."
    date: 2007-01-16
    body: "Wow, more missed sarcasm...\n\nI am 'interested' in Linux and KDE because I'm RUNNING Linux and KDE.  I was angered and outraged by some of these comments (especially the computer club one; that about sums up how corporate programmers feel about open source programmers in general)..."
    author: "Dark Phoenix"
  - subject: "OT: does not appear to contain CMakeLists.txt"
    date: 2006-12-16
    body: "Sorry for posting this here, but I'm sort of desperate ;) I'm trying to compile trunk but get stuck now and then. My aim is to start using KDE4 already. (Yes I know what kind of minefield and battlezone I'm moving in to;) ), My sole purpose of trying to move to KDE4 this early is to live on the bleeding edge. I'm just that kind of guy ;) But! There are some trouble on the way. So far I've been to lazy to check out every single application by it self. I'm going for the whole kdegraphics, kdeedeu, kdesdk etc. But many of these fails compiling due to \"kdeXXX/build does not appear to contain CMakeLists.txt\" Is this the way it is supposed to be? Does this mean I've have to check out every single little KDE app by its' self? Or is this a bug? (Where do we report KDE4 bugs anyway? If we do/bugreporting is wanted at this early stage). \n\nLet me also use this opportunity to than all KDE developers for their excellent work, their sacrifice of valuable free time and their general commitment for a better (computer) world."
    author: "Jo \u00d8iongen"
  - subject: "Re: OT: does not appear to contain CMakeLists.txt"
    date: 2006-12-17
    body: "> Is this the way it is supposed to be?\nFor a full step-by-step introduction see http://developer.kde.org/build/trunk.html and http://developernew.kde.org/Getting_Started/Build/Unstable_Version\n\n> Where do we report KDE4 bugs anyway?\nIMHO it depends on what you mean with KDE4-bugs. Since that term includes pretty everything, without any specialization like e.g. \"where to report that KWrite from trunk crashes each time I open a file named a.txt since revision xyz till head compiled 10 minutes ago?\" I would say;\nJust try to fix them, offer more details or recompile again since it's very likly that the problem got fixed already cause things are faster then Lucky Luke this days what imho makes it very difficult to handle bugreports against single checkout-revisions cause just nobody has the time to check reports to just note that they got already fixed 5 minutes after your last checkout or to just note that the only single person who was able to reproduce the bug jumped to another revision where she/he can't reproduce it any longer too.\nI guess things like realtime-communication e.g. within the IRC-channels (hint: try Kopete or maybe even Konversation, that's damn great software) is a good helper here to keep in synch with a such fast moving development-process :)"
    author: "Sebastian Sauer"
  - subject: "Re: OT: does not appear to contain CMakeLists.txt"
    date: 2006-12-17
    body: "Yes, this is off topic. Don't post \"how do I build this?\" questions here. First of all your passion to be on the bleeding edge is great, but you also need to know what you're getting yourself into. While KDE is usually very stable throughout the development process a major release is subastantially different. You WILL have things broken and days you can't build some programs as well as conflicts requiring substantial parts to be rebuilt or rolled back until development is synced. I would not recommend your course of action prior to alpha or beta releases, which are not happening any time soon.\n\nIf you are determined to run KDE4 then get on the kde-dev mailing list. You will need to know when things are broken, how to fix things and where to feed back to developers. Bug reporting? At this point you may find something helpful, but most problems developers know about and are working on. That's not to say finding a bug isn't helpful. It is to say an active bug process here would really bog development down. Enjoy!"
    author: "Eric Laffoon"
  - subject: "Re: OT: does not appear to contain CMakeLists.txt"
    date: 2006-12-17
    body: "How are \"tests\" developed right now? \n\nAre there any janitorial tasks?"
    author: "gerd"
  - subject: "Re: OT: does not appear to contain CMakeLists.txt"
    date: 2006-12-20
    body: "Hi gerd,\n\nafaik unittests are a great way to start and e.g. kdelibs comes with a lot of them. Beside that, we also try to improve the new wiki located at http://developernew.kde.org and for sure any help there is very welcome. Beside kdelibs there are also a few others projects within KDE where help is very welcome. But here it depends on which part you like to work on. Well, or just join the #kde or #kde-devel IRC-rooms and ask who needs help :)"
    author: "Sebastian Sauer"
  - subject: "Sacrifice of valuable free time"
    date: 2006-12-18
    body: "Let me also use this opportunity to than all KDE developers for their excellent work, their sacrifice of valuable free time and their general commitment for a better (computer) world.\n\nWell, might I kindly ask you to sacrifice just a bit more of your own valuable free time to produce a vmware image (or better, periodic vmware images) of your favorite linux distribution running kde4 so the rest of us can benefit from all the KDE developers' work and from your time and obviously hard effort in solving these build issues?"
    author: "Krishna Sethuraman"
  - subject: "Re: Sacrifice of valuable free time"
    date: 2006-12-18
    body: "A bit confused wheter you ar being sarcastic or not?!? But OK, giving the benefit of doubt :) I have no clue about WMWARE :( But I solved the trivial task of getting things to compile. When ever I have the time I suash a bug or two in Bugzilla. (Nope, no coding. Only removing duplicates and bugs no longer valid). So yes, my efforts are really humble and bearly visible even if you look for them. But I belive in that many people doing litle things, will together achive great things :) \n\nSorry for disturbing. :)"
    author: "Jo \u00d8iongen"
  - subject: "Re: OT: does not appear to contain CMakeLists.txt"
    date: 2006-12-18
    body: "\"kdeXXX/build does not appear to contain CMakeLists.txt\"\nYes, the build/ dirs don't contain cmake files. \nDid you run cmake manually or use some script ?\nYou have to enter the builddir and then run cmake there, pointing to the source dir, e.g. \n$ cd ~/src/kdegraphics/build\n$ cmake ~/src/kdegraphics/\n...\n$ make\n\nThis works. If it doesn't, please \n-ask on kde-devel@kde.org\n-or report it to the kde4-buildsystem@kde.org list\n-report it as a bug for the product \"buildsystem\" at bugs.kde.org\n\nAlex\n\n\n"
    author: "Alex"
  - subject: "Re: OT: does not appear to contain CMakeLists.txt"
    date: 2006-12-18
    body: "I found my errors... And yes, I'm blushing thinking of how shallow and easy they were to fix...\n\nThanks for pointing to appropriate channels to get help :)"
    author: "Jo \u00d8iongen"
  - subject: "syncing PIM"
    date: 2006-12-16
    body: "Sorry, as a non-developer I do not want to critisize the development process of the PIM-suite Kontact but I simply want to know if there is progress in these applications for KDE4 beside Akonadi especially in connection of syncing with mobile phones or PDAs.\n\nBest wishes\nMM"
    author: "MM"
  - subject: "Re: syncing PIM"
    date: 2006-12-17
    body: "OpenSync is supposed to give better sybcing -> http://www.opensync.org/\n\nIf I'm correct openSUSE 10.2 ships with a beta of OpenSync.\n\n\n\nI certainly do not share ok's pessimism. I frequently read the commit digest and Planet KDE, and I have to say that I'm impressed with the effort for KDE 4 and that the developers are doing great. I have the greatest respect for the KDE 4 dev's.\n\nSure, maybe I had expected more by now, especially news about Plasma. But the effort required for KDE 4 seems much, so it will take some time. Just accept that the dev's need some time, and they have a job and a life as well. I can wait for another year or even longer, I'm sure KDE 4 will be worth it. Personally I'm playing computer games most of the day and I can't do much more than \"hello world\" in C++ and I didn't bother to learn it, I'm not productive at all. The KDE dev's are incredibly productive, I think they're awesome. "
    author: "Alexander van Loon"
  - subject: "Re: syncing PIM"
    date: 2006-12-17
    body: "KDE 4 will make full use of OpenSync for synchronizing mobile phones, PDAs and more. We already have replaced most of the old KDE-specific syncing solutions by OpenSync and its KDE frontend KitchenSync. There is a mostly stable version existing for the KDE 3.5 line which is being picked up by distributions like openSUSE 10.2 right now.\n\nFor KDE 4 KitchenSync and OpenSync will be the standard way of syncing your data with mobile devices. As OpenSync is designed to be a standard cross-desktop and cross-platform tool, this means that there will be plenty of plugins for syncing with a very broad variety of devices and problems can be addressed at a central place.\n\nSo the answer is: Yes, there is tremendous progress in the area of syncing and KDE 4 will make use of, support and integrate the best free software solutions for that which are available.\n"
    author: "Cornelius Schumacher"
  - subject: "A positive comment"
    date: 2006-12-17
    body: "I'm disappointed at all the negative comments on this page!  KDE is a GREAT desktop environment. It looks slick, it runs fast, and it's loaded with features.\n\nI have read about many of the new underlying frameworks - DBUS, Solid, Plasma, OpenSync, etc. - and it's clear to me that the dev teams have excellent ideas and are working together to make a really cool product. Will KDE4 be so revolutionary that if will change the very way we think about computers? Probably not. Nothing that revolutionary has happened since Xerox developed the WIMP (window, icon, menu, pointing device) interface. But will it be vaporware? No way. Just take a look on kde-look.org, and you'll see that smart people have all sorts of cool ideas for the new desktop interface, how Konqueror should look, and so on."
    author: "kwilliam"
  - subject: "Re: A positive comment"
    date: 2006-12-17
    body: "While I agree with your comment, I find KDE ugly when it comes to fonts. Why should one have to have Microsoft fonts in order to have a decent looking desktop?  KDE, with its power and configuration capabilities should have fonts that look clear, crisp and \"web-ready\" by default. This has never been the case in my opinion."
    author: "cb"
  - subject: "Re: A positive comment"
    date: 2006-12-17
    body: "What distribution are you using and which version of KDE are you using?\n\n1) In most distributions the fontserver is deliberately altered (e.g. SUSE) so that algorithms that are claimed to be patented by Apple / Microsoft don't get used to improve font quality. This results in poorer quality for the fonts displayed. Solution: Either look for optimized rpm's or deb's offered in unofficial repositories or compile the package yourself.\n\n2) Another thing that might decrease font quality is this: fonts usually get manually optimized by the font designer for certain absolute font sizes. Usually for GUI text those optimized font sizes got picked by the KDE team. However those optimizations only refer to 96 dpi. If the monitor resolution differs a lot from that value non-optimized font sizes might get used which result in poorer quality.\n\nSolution: In KDE 3.5.5 enable \"Force DPI for Fonts: 96 DPI\" in the font dialog of KDE's Control Center.\n\n\n"
    author: "MaXeL"
  - subject: "Re: A positive comment"
    date: 2006-12-17
    body: "On my vanilla Kubuntu 6.10 install the fonts look *awesome*. Crisp, well readable, and great anti-aliasing."
    author: "Mark Kretschmann"
  - subject: "Re: A positive comment"
    date: 2006-12-17
    body: "Yup, on my openSUSE 10.2 box as well :)\n\nAnd I think font-issues should be handled bij X or the distribution, not by the desktop environment (since not all applications use the desktop environment)"
    author: "otherAC"
  - subject: "Re: A positive comment"
    date: 2006-12-17
    body: "> I think font-issues should be handled bij X or the distribution, not by the desktop environment\n\nbut KDE developers have fixed quite some issues with the freetype lib (that handles fonts)... i even read some KDE dev are know as the \"performance guys\" to the freetype devs.\n\n"
    author: "cies breijs"
  - subject: "Re: A positive comment"
    date: 2006-12-17
    body: "yes, and they have fixed it in the original freetype lib, which is used by all graphical stuff, so all the fixes are not just KDE-specific."
    author: "Jakob Petsovits"
  - subject: "Re: A positive comment"
    date: 2006-12-17
    body: "Care to post a screen-shot? Thanks."
    author: "cb"
  - subject: "Can't reach the site"
    date: 2006-12-17
    body: "I tried to read the article a couple of times, but it looks like the site is down. Anyone knows what is going on?"
    author: "Tomas"
  - subject: "Re: Can't reach the site"
    date: 2006-12-17
    body: "probably to much traffic from the dot to the server :o)"
    author: "otherAC"
  - subject: "a good point"
    date: 2006-12-17
    body: "i think the article makes a good point - management. i think this is an area in which linux really is behind windows. i work in a company mixing linux with windows, and if we on linux have to make an account (NIS) we have to edit passwd, shadow, etc by hand. giving a person permissions on a folder? a lot of work.\n\nthe windows guys just point'n'click, and can even set a time the user has to be removed automatically. they're 10 times faster on the basics.\n\nok, we could write scripts. but the company (as any company in the current fast moving world) changes a lot, so the scripts have to be rewritten every - what, week? business units change to divisions, they want their projects indexed by year, but a year later it should be the newly (\"we're back\") business units, etc.\n\nok, linux is more powerfull. complex things can sometimes be done much more easily on linux. great. but in the basics, windows RULES. and what constitutes of 'the basics' is always expanding, thanks to the hard work microsoft and many other companies delivering system management tools are putting into it.\n\nlinux needs easy to use administration gui's. yeah, the commandline is powerfull, more efficient compared to a gui. well, forget it, that was years ago. it might be the case for linux gui's, as those are mostly a very one-to-one mapping of the commandline stuff. not for windows, tough.\n\ni hate to see those windows guys do much more administration much faster than we linux guys, and i think that's something which needs fixing...\n\nwe have the basics worked out here, in KDE. i think a current KDE desktop kicks windows XP's ass, and i hope around KDE 4.1, it'll kick Vista's ass. but a company needs a bit more than a flashy gui, it needs to deploy systems, manage users, set availability of software, etc - and not by writing (and rewriting) scripts every week..."
    author: "superstoned"
  - subject: "Re: a good point"
    date: 2006-12-17
    body: "I think you should have a closer look at the current state of linux configuration :o)\nlots of stuff you mention is also point and click in linux.\n\n"
    author: "otherAC"
  - subject: "Re: a good point"
    date: 2006-12-17
    body: "Hmm, since you are in a mixed workstation setup, wouldn't it be easier to use pam_smb instead if NIS to administrate the user accounts?\n"
    author: "Kevin Krammer"
  - subject: "Re: a good point"
    date: 2006-12-17
    body: "I read in my Linux Administrator book that NIS is really still only in Linux for backwards compatibility, and is especially not good enough for mixed Windows/Linux environments (since NIS is UNIX-only).  Yes, you should probably look into pam_smb or pam_ldap..."
    author: "Dark Phoenix"
  - subject: "Re: a good point"
    date: 2006-12-26
    body: "You really need to redesign your authentication infrastructure if you're editing passwd files by hand. Jeez... that's archaic. Use an LDAP server. If your network has an Active Directory server, you already have one (although somewhat bastardized by ms). Then, use plugins for PAM and NSS (usually packaged as libpam-ldap and libnss-ldap. Also look for a pam plugin that automatically creates users home directories on their first successful login.\nWhen an account is created on the server, the accounts just work on all the workstations. If you want to have users home directories available from both windows and linux systems, and you're using a Samba server, make the home directories available to the local network via NFS. Since the user database is shared among all the workstations, user numbers will match everywhere and, again, it will just work. This is far from the only, and probably not even the best, solution. However, it does show that you can do far, far better then editing files by hand to add users."
    author: "Benjamin Long"
  - subject: "Screenshots anyone?"
    date: 2006-12-17
    body: "Hopefully, KDE4 will be smoother in Desktop redrawing then KDE3 and runs on a 500Mhz as good as a window manager and/or Gnome.\n\nMaybe KDE4 will be better from the technical point of view then *every* other Desktop. But users don't want to see sloppy Desktop redrawing because this isn't ergonomic."
    author: "anonymous"
  - subject: "Re: Screenshots anyone?"
    date: 2006-12-17
    body: "\nWhat do desktop redrawings have to do with ergonomics?\n\nAnyway, kde4 is a lot faster then kde3, so chances are that redrawings wil go smoothly on your system.\n\nChoosing a lighter linux distribution might als help (dunno, which one you use now)"
    author: "otherAC"
  - subject: "Re: Screenshots anyone?"
    date: 2006-12-17
    body: "KDE is not a window manager, it's a Desktop Environment. If you compare it to Gnome on your machine, what figures can you give us about KDE 3 not being up to Gnome in 'Desktop redrawing'?\n"
    author: "anonymous"
  - subject: "Re: Screenshots anyone?"
    date: 2006-12-17
    body: "Everyone who wants to see KDE4 screenshots, here you are:\nhttp://canllaith.org/wp-content/images/svn-features/kde4-04.png"
    author: "Lans"
  - subject: "Re: Screenshots anyone?"
    date: 2006-12-17
    body: "The observant reader will notice that it looks much like KDE 3, so before you give yourself indigestion over that, remember that software is like an iceberg - 90% is below the surface.  Anyone remember the same deal looking at the KDE 1.8x versions?\n\nThe past 18 months have been spent in porting and rewriting the APIs that make those apps possible: agreeing a style standard, tidying up code, documenting, making interfaces cleaner, throwing out wholesale bad API, duplicate functionality and functions that turned out to be only used by one app.  We know what worked and what didn't after five years of being bound to the design decisions of KDE 3.  As an aid to ourselves, and as a gift to the new contributors who will join the project in the next five years, having this extended basic maintenance period for the platform is essential.  \n\nThis refit is now mostly done, and developers' attention can move to the user visible parts, like applying usability experts' advice (and 5 years of dot comments), writing nifty new widget themes, and writing cool new apps with greater ease than before - and major distros (openSUSE, kubuntu) are providing preview packages of the libraries to speed up porting.\n"
    author: "Bille"
  - subject: "Re: Screenshots anyone?"
    date: 2006-12-17
    body: "Yes, the widget drawing will be smoother, thanks to Qt4's Arthur. And you won't even need an expensive proprietary driver to get the benefits of it."
    author: "Brandybuck"
  - subject: "Re: Screenshots anyone?"
    date: 2006-12-17
    body: "Thanks! One great answer.\n\nBye!\n"
    author: "anonymous"
  - subject: "Re: Screenshots anyone?"
    date: 2006-12-18
    body: "proprietaty... ok...\nbut what is the last time you paid for the driver :S"
    author: "Mark Hannessen"
  - subject: "rethinking the window manager"
    date: 2006-12-19
    body: "There is an interesting comment on the LWN entry for this article\n( http://lwn.net/Articles/214528/ ).  For convenience:\n\nThe way window managers work (user interface side, not code) hasn't really changed since the start of X windows, many eons ago. Amongst the plethora of choices, they differ only superficially: there is still a titlebar as well as the close/minimise/maximise buttons and the ability to resize windows.\n\nHow about something along the lines of a window manager which enforces a particular structure and uses the available space considerably more efficiently? e.g. a \"tabbed\" window manager, ala Firefox (this is more than just having a taskbar).\n\nAn example: windows would not be allowed to be moved and resized arbitrarily and the annoying pop-ups (e.g.. file-requesters, error messages) would not simply (and rudely!) cover other windows. If one has a browser/editor that takes up the entire screen, and then launches a terminal (or some other window wants to open up), the size of browser/edtior would be reduced by half and the terminal \"window\" would take the bottom half (or the right half) of the screen. There would be no titlebars - on a laptop, where screen space is premium, having multiple title bars amounts to wasting space. Actions for closing the windows would be done on the tabs.\n\n"
    author: "Mark Tall"
  - subject: "Re: rethinking the window manager"
    date: 2006-12-20
    body: "I think it's a good idea that you would check out the many ways you can manipulate the way windows behave in KDE.\n\nMost of what you suggest has been available for ages in Linux desktops / window managers.\n"
    author: "otherAC"
  - subject: "Re: rethinking the window manager"
    date: 2006-12-22
    body: "The window manager in KDE does not enforce the originally described constraints  in a systematic fashion. It can have specific settings for specific windows which go partially towards a window manager with  \"constraint policies\".\n\nKwin is still an old-style window manager that just happens to have been written relatively recently. I'm not saying it's a bad old-style window manager, I'm saying that it would be handy to move beyond the current windowing paradigm.\n\nKwin doesn't, for example, automatically re-size the dominant window (e.g. a browser) when a new one opens up (e.g. a terminal).  It doesn't have an option to enforce a policy of \"disallowing any kind of window overlapping\".  \n\nThere are perhaps only two new window managers (ion3 and wmii, which stands for window manager improved 2), that (mostly) implement the described contraints.  However, they do not fit in too nicely with the rest of KDE (e.g. panels).\n\n"
    author: "Mark Tall"
  - subject: "Re: rethinking the window manager"
    date: 2006-12-21
    body: "Yeah, and that's particular important now that screens are getting bigger. In fact, reading a webpage in a fullscreen browser is not pleasent in modern wide screens, and resizing windows manually dragging your mouse is not pleasent either. \nI'm not sure if the solution is revamp automatation though. I think that when you press the maximize button, it should go to a sensitive size, and you'd press it again to maximize more or fully. Its funny that people use more than one screen to be more productive with the desktop management, because then they can just press maximize and have the window fit nicely.\n\nUnfortunately, I think proprietary systems, like Macs, will get this first and free software will just play catchup like in most user interface elements areas."
    author: "blacksheep"
  - subject: "Some thoughts and ideas"
    date: 2006-12-22
    body: "Although I'm not a developer I can imagine that it is a lot of work to release a new desktop environment. I think the developers are doing a good job when it comes to programming.\n\nBut I think there are some other problems in the KDE-project, especially in KDE4.\n\n1) Project management\n\nA lot of people complain about Microsoft and there release-delays. But on the other side, a lot of open source project do exactly the same thing (Debian, Mozilla, KDE and others). If you are in a position like Microsoft it might not be a big problem, because a lot of people have to use their OS, and a lot of people don't even know about other operating systems.\n\nBut if you are in a position like KDE where you are trying to gain market share, you sould not repeat their faults, you should try to do better. There is not even a timeline at the moment, at least as far as I know. And once again, if you are not in a position of having about more than 90% of market share the \"it will be released when it is finished\" is not very helpfull.\n\nAnd this might be one reason distributions like RedHat and Novell seem to prefer Gnome at the moment, just because they know what the will get in the next release and when they will get it. This is NO \"Gnome is better than KDE\" or \"KDE is better that Gnome\", I use both of them - some things I like in Gnome, some things I like in KDE. Maybe Gnome will fall far behind KDE4, but who really knows?\n\nNo timeline, no milestones, almost no information about the progress, no information about remaing tasks and their needed time. Don't get me wrong, but from outside of the project, this does not look very professional.\n\n2) Information management\n\nThere are a lot of websites, spreat all over the Web:\n\nKDE4 goals (http://wiki.kde.org/KDE%2B4%2BGoals)\nRelease Plan (http://developer.kde.org/development-versions/kde-4.0-release-plan.html)\nAppeal (http://appeal.kde.org/wiki/Appeal)\nPlasma (http://plasma.kde.org/)\nSolid (http://solid.kde.org/)\nPhonon (http://phonon.kde.org/)\nKDE-radio (http://radio.kde.org/)\nOxygen icons (http://www.oxygen-icons.org/)\nDecibel (http://decibel.kde.org/)\nWikipedia (http://en.wikipedia.org/wiki/KDE4)\n\nThe problem is, most of them are not very usefull to users, because there are only very vew information on this sites and even less news. Most of this websites are completly out of date.\n\nWhy not one Website related to KDE4, let's call it www.kde4.org for example. This site should/could contain all the related stuff as subsites, like\nwww.kde4.org/main (main infos about the project KDE4, what it is, etc.)\nwww.kde4.org/news (regular updated (!) site with all the news)\nwww.kde4.org/goals (detailed goals for KDE4)\nwww.kde4.org/timeline (or releaseplan, whatever you wanna call it)\nwww.kde4.org/appeal (infos to this subproject)\nwww.kde4.org/solid (infos to this subproject)\nwww.kde4.org/phonon (infos to this subproject)\nwww.kde4.org/oxygen (infos to this subproject)\nwww.kde4.org/decibel (infos to this subproject)\nwww.kde4.org/documentation (all documentation stuff)\nwww.kde4.org/screenshots (when available)\nwww.kde4.org/download (snapshots including needed infos how to build, compile, install...)\n\nThis website could be one site (including subsites) for both - users and developers. I think this could satisfy users in being informed about the status and the development of KDE4 and it could help developers who want to start developing KDE4 apps. One important thing is update, as mentioned above the complete site should (almost) be up to date all the time.\n\n\nI really hope you guys don't get me wrong, just a few points I have noticed in some critics about KDE4 and some ideas.\n\nRalf"
    author: "Ralf Weyer"
  - subject: "Re: Some thoughts and ideas"
    date: 2006-12-22
    body: "> And this might be one reason distributions like RedHat and Novell seem to prefer Gnome at the moment, just because they know what the will get in the next release and when they will get it.\n\nSince the KDE 3.5 release plan shows a expected date for the release of 3.5.6, I assume you are referring to KDE4.\n\nHowever, I am not sure if GNOME has even started with GNOME3.0 development, so I would be surprised if they had already a release date for it.\n\nOr it could be a misunderstanding. Do you mean that KDE's 3.x released were not as well scheduled as GNOME's 2.x releases?\n\n"
    author: "Kevin Krammer"
  - subject: "Re: Some thoughts and ideas"
    date: 2006-12-23
    body: "It is not about any number or special version. Numbers do not count that much I think. It is just about the latest version of the desktop environments, so Gnome still the 2.x-series and KDE it is version 4.\n\nWhen talking about Gnome, at this point there is not planing of version 3 (only so very undistinct thoughts about it).\n\nBut I don't think a major version should be handled different as a minor version. You allways have to plan it, you allways have to have a good project management, you allways have to have milestones, ... It is just about how to handle such a project, and the bigger the project, the better the project management has to be.\n\nAnd information to the world outside of this project is very important, especially in open source projects. This could be one of the biggest advantages of open sources software comparing to propriatary software.\n\nRalf"
    author: "Ralf Weyer"
  - subject: "Re: Some thoughts and ideas"
    date: 2006-12-30
    body: "This is a major misconception from a non-developer. Consider the fact that over the years, KDE has built up an extensive amount of code. Additionally, consider that changes to the API were disallowed during the KDE series of releases to allow commercial ISVs to have a stable API to work with. Now look at the preceding with these nuggets of thought. KDE 4 will be using Qt4, which has a wildly different API than that of Qt3 meaning lots of changes that MUST occur. Additionally, the kdelibs developers are finally getting the opportunity to clean up the code that couldn't be changed during the 3.x series due to the API guarantee. We all just have to be patient with this, think of it this way: when KDE 2 was in development, it took a sizable amount of time to get the code changes done too. GNOME had the same issues when they were moving from GNOME 1.x to GNOME 2. Additionally the Linux Kernel has had its share of growing pains when moving from the 2.4.x series to 2.6.x. This is hardly a KDE project mis-management issue, but rather a normal occurrence when you have a sizable code base such as KDE."
    author: "Gary Greene"
---
Australian computer news site Computerworld asks if <a href="http://www.computerworld.com.au/index.php/id;855780098">KDE 4 will be the ultimate business desktop</a>.  Speaking to developer Hamish Rodda they look at the changes being made to the KDE libraries including the <a href="http://pim.kde.org/akonadi/">Akonadi</a> storage manager for PIM data.  He also explains why KDE 4 will be important for ISVs to support.
<!--break-->
