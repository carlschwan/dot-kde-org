---
title: "Marcel's Linux App of the Month: KDissert"
date:    2006-03-27
authors:
  - "mgagn\u00e9"
slug:    marcels-linux-app-month-kdissert
comments:
  - subject: "\"OpenOffice Writer document\"??"
    date: 2006-03-27
    body: "As a KOffice hacker, I am somewhat affronted when I see things like that. OpenDocument is a file format that is intended to ensure total vendor interoperability - it's NOT only the OpenOffice file format.  I couldn't find anywhere to comment on the article, so please help me stamp out misconceptions like this, everybody.\n"
    author: "Inge Wallin"
  - subject: "Re: \"OpenOffice Writer document\"??"
    date: 2006-03-27
    body: "You have already told Marcel this. I am sure that he will either adjust for the future or will perhaps write an adendum for you."
    author: "a.c."
  - subject: "Re: \"OpenOffice Writer document\"??"
    date: 2006-03-27
    body: "Well Marcel was using the same terminology as KDissert."
    author: "Ian Monroe"
  - subject: "Re: \"OpenOffice Writer document\"??"
    date: 2006-03-27
    body: "You forgot \"GNU/Linux App of the Month\". Nitpicker :)"
    author: "anonymous"
  - subject: "klik link"
    date: 2006-03-27
    body: "Great article!\n\nOne more link for the install:\nhttp://kdissert.klik.atekon.de/\n\n"
    author: "anonymous"
  - subject: "integrate it in Koffice"
    date: 2006-03-27
    body: "It would be great, if kdissert would be integrated in Koffice"
    author: "Mark"
  - subject: "Missing collapse"
    date: 2006-03-28
    body: "I like KDissert, but what I really miss is the possibility to collapse your diagrams like Freemind [http://freemind.sourceforge.net/] can (btw: project of the month 02/2006 on sf.net)."
    author: "Dani"
  - subject: "Mind map"
    date: 2006-03-28
    body: "I wish there was a mind map mode in KDissert. Isn't this what it already does, you might ask, but no. KDissert is designed to allow only strict tree structure in the graph, allowing it to eventually output the text as a linear document.\n\nI wish there was, in addition to the tree branches, the possibility to add \"soft\" edges, connecting nodes in any combination. There could be edges in different colours; directed and undirected edges and edges with labels. This would not break the previous functionality, but allow KDissert to be used also for other purposes than document production.\n\nI have looked at other tools that do this, but found none to be satisfactory."
    author: "Martin"
  - subject: "Re: Mind map"
    date: 2006-03-28
    body: "Hold the control key pressed while linking objects (in link mode). This will add references."
    author: "ita"
  - subject: "Re: Mind map"
    date: 2006-03-29
    body: "holy hidden features batman!\n\nthis really needs to be exposed much better. keep the control key shortcut, but for the love of god[dess], add another button to the toolbar! i've also discovered that it's a little obtuse to new kdissert users how to begin adding items ... \n\nnow all we need is to make the drawing of the connectoins prettier (arthur's improved support for things like AA and alpha will go a long way here) and make customizing connections nicer (e.g. allowing one to give them custom colors, tag them, etc).\n\nkdissert is a very promising app (i use it regularly myself these days). a good ui review could do it handsomely at this point."
    author: "Aaron J. Seigo"
  - subject: "Re: Mind map"
    date: 2006-03-30
    body: "Can you expand a bit? What exactly are references? And how are they reflected if I output the document to html or to opendocument format?"
    author: "Gonzalo"
  - subject: "Re: Mind map"
    date: 2006-03-30
    body: "The references are converted in footnotes, for latex book and article at least. They are ignored by other document generators.\n\nThe chapter about references is missing from the documentation."
    author: "ita"
  - subject: "BasKet"
    date: 2006-03-28
    body: "Personally I like BasKet ( http://basket.kde.org/ ).  Not quite the same, but in the same general realm of application.  The betas of 0.6.x are stable and way way more impressive than 0.5.x.  If you haven't used it, play with it a bit... it features unobtrusive fancy effects that really seem integrated.  If all apps worked like it in KDE 4, I'd be happy."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: BasKet"
    date: 2006-03-28
    body: "I typed \"really seem integrated\" when I meant to say \"really seem intuitive\".  Fancy but not flashy effect that give subtle cues as to how you can manipulate the material."
    author: "Evan \"JabberWokky\" E."
---
Unix Review <a href="http://www.unixreview.com/documents/s=10060/ur0603d/">takes a look at KDissert</a>.  "<em>Somewhere in my head, there's a jumble of fleeting thoughts, ideas, and concepts, running every which way with no map for me to follow other than some casual mental digging here and there. If you find yourself in the same kind of cerebral jungle, what you and I really need is a mind map. Thomas Nagy's <a href="http://www.freehackers.org/~tnagy/kdissert/">KDissert</a> is an application referred to as a mind mapping tool. Its purpose is to help you create complex documents such as a thesis, or a dissertation, or a presentation.</em>"



<!--break-->
