---
title: "aKademy 2006 Call for Participation"
date:    2006-06-03
authors:
  - "mwelwarsky"
slug:    akademy-2006-call-participation
comments:
  - subject: "Slightly OT: Website Layout"
    date: 2006-06-03
    body: "If it's not only me who thinks that kde.org needs an overhaul in terms of layout and style: the style of the conference website seems like a really good candidate.\nNice-looking and eye-pleasing. Well done!"
    author: "Anon"
  - subject: "Re: Slightly OT: Website Layout"
    date: 2006-06-03
    body: "Oh, I think KDE deserves more diversity in terms of websites. Most projects are so integrated in the plattform that they do not have a real project or website of their own and if so, this website will be outdated."
    author: "hup"
  - subject: "Re: Slightly OT: Website Layout"
    date: 2006-06-03
    body: "I didn't think of one of the subprojects but kde.org, the site representing the project as a whole, the portal site guiding you deeper into different parts of the projects. Well the news part of kde.org looks like it gets regular updates, but the visual appearence is stuck somewhere in the past. It becomes obvious if you look at the conference's website layout. Iirc someone was already doing some tests for a css update at least. But then again there are quite often made remarks that much information on the site is outdated, hard to find or cluttered among different subpages. So it looks like some work needs to be done from the ground up. I'm sure this wouldn't be a small task, but it would be great if the problems got more widely recognized and taken care of."
    author: "Anon"
  - subject: "Re: Slightly OT: Website Layout"
    date: 2006-06-03
    body: "Heh, I could point you to http://dot.kde.org/1149336504/1149352077/1149360461/ on a similar note. Problems are easy to recognize: \"yup, that's a problem.\" Fixing problems (some of 'em anyway) takes a serious investment in time and effort and takes dedication.\n\nThe website(s) in particular need a particular mindset -- an *editor* -- to organize and maintain, and that needs to be a long-term thing. It's a problem we're trying to address on (for instance) the kde-www mailing list."
    author: "Adriaan de Groot"
  - subject: "Re: Slightly OT: Website Layout"
    date: 2006-06-03
    body: "I'll thank you for your kind words on behalf of the Oxygen team who also designed the website and Marcos who did most of the implementation.\n\nA website redesign is more than just a look (and we *are* considering this one for the KDE website as a whole) though, it's also a matter of (re)organizing the material we have so that it is easier to navigate, easier to find what you need and easier to maintain (for us). Look to see various website experiments in the coming months."
    author: "Adriaan de Groot"
  - subject: "Re: Slightly OT: Website Layout"
    date: 2006-06-04
    body: "I really, really like the aKademy website. Thank you Marcos F. ( and web development team), Oxygen team and aKademy team for presenting KDE in such a professional and appealing fashion."
    author: "Claire"
  - subject: "Help KDE Marketing team fundraise and publicize"
    date: 2006-06-03
    body: "Looks like GNOME racked in at least $110,000 USD for GUADEC. Can we do better? Sure we can!  \n \nHelp the KDE Marketing team make the upcoming Akademy 2006 a huge success by joining the KDE-promo mailinglist and sharing your ideas for fundraising and publicity: \n \nhttps://mail.kde.org/mailman/listinfo/kde-promo"
    author: "AC"
  - subject: "Re: Help KDE Marketing team fundraise and publicize"
    date: 2006-06-03
    body: "Not just your ideas. To quote something by Pratchett: \"you can't help people with magic, but you can help them with skin.\" A great idea remains a great idea until there are people to execute them, turn them into reality, make the idea fly. So put in time in promo -- it's a great way to support KDE. DTP work, design, organizational and administrative stuff all has to happen as well."
    author: "Adriaan de Groot"
  - subject: "Broken links"
    date: 2006-06-03
    body: "on many (most?) pages the links to sitemap and contact are broken"
    author: "ac"
  - subject: "Re: Broken links"
    date: 2006-06-03
    body: "Good point. I'll fix them later once the kids are in bed (they take precedence even over conference websites)."
    author: "Adriaan de Groot"
  - subject: "akademy.kde.org?"
    date: 2006-06-04
    body: "I always wondered why there is no akademy.kde.org subdomain that points to the current conference page. googling for \"akademy kde\" spit out the outdated link for 2004, just googling for \"akademy\" currently brings up conference2005.kde.org as the first hit. Are we all attending Conference2006? No! Of course we're going to Akademy :D"
    author: "John Noone"
---
This year's <a href="http://conference2006.kde.org/">KDE World Summit, aKademy 2006</a>, is approaching fast.  The website is now live.  On behalf of the programme committee I have the pleasure to announce that we will accept presentation abstracts as of now. Read on to find more information in the formal <a href="http://conference2006.kde.org/conference/call.php">Call For Participation</a>.






<!--break-->
<h2>KDE Contributors Conference 2006, Call for Participation</h2>

<p>You're working on innovative and exciting projects related to the
Free Desktop?  You've got something to say to the KDE community? Then this is for you!</p>

<p>The aKademy 2006 conference team is calling for contributors to
present their work and vision to the KDE community. This years' conference takes place at Trinity College, Dublin, Republic of Ireland, from September 23rd to September 30th. All presentations will be held during the "KDE Contributors Conference" event on September 23rd and 24th.</p> 

<p>Topics of interest include, but are not limited to:</p>

<ul>
<li>KDE 4 architecture and vision</li>
<li>Desktop related hardware and software technologies</li>
<li>Innovative human-machine interface design</li>
<li>Cool programming tools, patterns and techniques</li>
<li>Applications written for the K Desktop Environment</li>
<li>Advancements in l10n and i18n</li>
<li>Quality Assurance in Open Source projects</li>
<li>Legal, social, philosophical or promotional matters related to KDE</li>
<li>Desktop software standards, usability and accessibility</li>
<li>Performance analysis and improvements</li>
</ul>

<p>When preparing a proposal to present your work, please take note of these
guidelines:</p>

<p>Please submit a 300 word abstract of your presentation to
<a href="&#109;&#97;&#105;&#108;&#116;&#111;:&#x61;&#x6B;&#x61;&#100;&#101;&#x6D;&#121;&#x2D;&#x74;&#x61;&#108;&#107;&#x73;&#x2D;&#50;&#x30;&#x30;&#54;&#x40;&#107;&#x64;&#x65;&#46;&#x6F;&#114;&#103;">&#x61;&#x6B;&#x61;&#100;&#101;&#x6D;&#121;&#x2D;&#x74;&#x61;&#108;&#107;&#x73;&#x2D;&#50;&#x30;&#x30;&#54;&#x40;&#107;&#x64;&#x65;&#46;&#x6F;&#114;&#103;</a>. We accept documents in Open Document Format or plain 
ASCII text. Please send in your abstract before Friday June 30th at the latest, together with some background information about yourself and your presentation topic.</p>

<p>Your abstract will be reviewed by the programme committee based on content,
presentation and suitability for the event. Acceptance notices will be sent out on Saturday July 15th at the latest. If your presentation is accepted, be 
prepared to make adjustments as per comments of the programme committee. The 
programme committee cannot comment on rejected submissions.</p>

<p>By submitting an abstract you give permission to publish it on the conference website and in a printed conference booklet. Copyright is retained by you, the author.</p>

<p>The conference language is English.</p>

<p>A total of 30 minutes will be allocated for your presentation including a
Q&#038;A session, more time may be granted on request.</p>

<p>The program committee:</p>
<ul>
<li>John Cherry (OSDL)</li>
<li>Adriaan de Groot (KDE)</li>
<li>Kevin Ottens (KDE)</li>
<li>Keith Packard (Intel)</li>
<li>Daniel Stone (freedesktop.org)</li>
<li>Matthias Welwarsky (KDE)</li>
</ul>

<p>Point of contact: <a href="&#109;&#97;&#105;&#108;&#116;&#111;:&#x61;&#x6B;&#x61;&#100;&#101;&#x6D;&#121;&#x2D;&#x74;&#x61;&#108;&#107;&#x73;&#x2D;&#50;&#x30;&#x30;&#54;&#x40;&#107;&#x64;&#x65;&#46;&#x6F;&#114;&#103;">&#x61;&#x6B;&#x61;&#100;&#101;&#x6D;&#121;&#x2D;&#x74;&#x61;&#108;&#107;&#x73;&#x2D;&#50;&#x30;&#x30;&#54;&#x40;&#107;&#x64;&#x65;&#46;&#x6F;&#114;&#103;</a></p>

<p>Deadline for abstracts: <b>June 30th, 2006</b>.</p>





