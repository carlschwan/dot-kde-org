---
title: "KDE Commit-Digest for 18th June 2006"
date:    2006-06-19
authors:
  - "dallen"
slug:    kde-commit-digest-18th-june-2006
comments:
  - subject: "Osnews article"
    date: 2006-06-18
    body: "KDE criticism\nhttp://www.osnews.com/story.php?news_id=14927\n\nMost points mentioned have nothing to do with KDE"
    author: "gerd"
  - subject: "Re: Osnews article"
    date: 2006-06-18
    body: "Where is this mentioned, or was it just offtopic? ;-)"
    author: "pascal"
  - subject: "Re: Osnews article"
    date: 2006-06-19
    body: "Talking about Kopete, what I really miss in kopete is a \"Cybercafe\" mode, where the app just got the contact list from the server, deleting it after logout. No sync with adressbook, nor any other fancy integration things, just connect, chat, gone.\n\nIn its current state, Kopete is a great IM client for personal use, but it's useless for deploying at public places or just for someone trying to ocassionally connect from someone else's machine.\n\n"
    author: "Gabriel Gazz\u00e1n"
  - subject: "Re: Osnews article"
    date: 2006-06-19
    body: "Agree on this.\n\nThe workaround in older version is to set contactlist.xml as read-only.\nThis doesn't work with latest version as it seems that Kopete forcely\nremove and create a new contactlist.xml.\n"
    author: "Me"
  - subject: "Re: Osnews article"
    date: 2006-06-19
    body: "this seems a serious issue\ndo the developers know about that? (i.e. have you filed a bug report?)"
    author: "anonymous"
  - subject: "Re: Osnews article"
    date: 2006-06-26
    body: "Well, I've just filed a feature request; it's here:\nhttp://bugs.kde.org/show_bug.cgi?id=129829\n\nNow you can vote for it.  ;)\n"
    author: "Gabriel Gazz\u00e1n"
  - subject: "Re: Osnews article"
    date: 2006-06-21
    body: "Seriously, I think this is a lot of duplicated effort.\n\nInternet caf\u00e9 operators use KDE's kiosk mode with a locked-down profile anyway, and upon logout, this profile will be deleted (most of the time, along with the complete home directory contents). Including, of course, all Kopete settings.\n\nIf that is not enough, you are always free to use the diverse online Flash/Java based messenger app(let)s that exist ...\n\nI would rather the Kopete developers invest this time to get file transfer done correctly. I know this is a terrible amount of work with all the issues and different protocols it has :-) but IMHO that's a lot more important.\n\nJens"
    author: "Jens"
  - subject: "Re: Osnews article"
    date: 2006-06-26
    body: "Well, seriously, I think it is not.\nI have my personal machine, and when someone comes in and asks if he can connect to MSN for chatting a little while I have just two options:\n\n  a) Saying it can't be done. A lie, but a practical one. (Once I let a friend connect using Kopete and when he left the machine *I* was left with something like 50 new MSN contacts! He didn't realised of that, but I bet if he did he would be very uncomfortable with leaving all his personal contacts in another machine, inadvertedly! Besides that, I was also very annoyed, because I had to go through all the contact list checking to see what contact was mine and deleting all the others. A blast!)\n\nb) The other alternative is rebooting my machine with Windows just to let him connect to MSN. Well... enough said. Do you want to be forced out of KDE just because you don't have the right application in Linux? Me not. :-|\n\nSo, I think it is something to think about, for future versions of Kopete.\nI didn't file this as a feature request yet, and I don't know where it can be done. If any of you can help me with this it would be great!\n"
    author: "Gabriel Gazz\u00e1n"
  - subject: "Re: Osnews article"
    date: 2006-06-26
    body: "Er... How about quickly creating a new user for your friend?"
    author: "Boudewijn Rempt"
  - subject: "Re: Osnews article"
    date: 2006-06-26
    body: "A new user? You mean creating a whole Operating System user for every friend that comes to my house? You got to be kidding!\nThat's very impractical, not to mention that the guy will still be leaving behind all of its personal contacts revealed to me. Isn't Linux supposed to be a \"secure\" O.S. above all?\n\nCome on! Kopete is really great, but it lacks that function. Period.\n"
    author: "Gabriel Gazz\u00e1n"
  - subject: "Re: Osnews article"
    date: 2006-06-26
    body: ">>A new user? You mean creating a whole Operating System user \n>>for every friend that comes to my house? \n\nYes and no, you are not thinking it through. \n\nYou simply create one guest user, only one for all your friends to use, and simply delete all account data when they log out. The brute force approach would be removing the .kde dir, but a smarter solution would be only to remove things like Kopete information and browser cache/history. In any case it would only require a simple script running at logout. "
    author: "Morty"
  - subject: "Re: Osnews article"
    date: 2006-06-27
    body: "I am not kidding. A user account isn't something heavy-weight or important. It's something you can create in half a minute and throw away even faster. And it would mean your friends don't have access to all the other stuff in your home directory, too. Or you can just do what someone else suggested: create a single guest account (with a good password, of course, if your machine is reachable from the internet or through wifi) and use a .logout that\ncleans the .kde dir on logout.\n\nI do the first: it works for me, and has worked fine for me since I started using Linux in 1994."
    author: "Boudewijn Rempt"
  - subject: "Re: Osnews article"
    date: 2006-06-29
    body: "Excuse me, I know you're trying to give me the best workarounds you can think of to solve the problem. That's fine and I really appreciate your kindness.\nBut thinking in a more general way, do you both honestly think that KDE will popularize among normal (i.e. non technical) IM users if they have to learn to create a user or a guest user or whatever and then learn to delete don't-know-what config file just to do the same thing they always do automatically in another IM client in another O.S.?\nWell... it won't! These frustrated users will spread the word saying that KDE is not user friendly, that it lacks features, etc.\nAnd, you know what, they sure are right! It's easier to log in ICQ or MSN clients than it is with Kopete. Less steps, less knowledge involved.\n\nYeah... Kopete is great for all its integration with the rest of KDE, but people need more than that; they need to be able to move to another place and still log in and chat easily, effortlessly. Today Kopete don't offer this to these users.\n\nI'd wish the Kopete team would put some effort in this direction, it's really needed and will have a great impact on KDE's future.\n\nThanks.\n"
    author: "Gabriel Gazz\u00e1n"
  - subject: "Re: Osnews article"
    date: 2006-06-29
    body: "Well... I'm fairly sure that, no matter the operating system, it's a bad idea to give even the best of friends access to your user account. They could sent mail that appears to be signed by _you_, for one thing. Of course, it's not perfect, but well, nothing made by humans is, but this is a practical work-around. And since there's a real-life analogy to different operating system users (viz. different bodies for different souls), it shouldn't be too hard to teach non-technical people to make different users for different purposes.\n\nTo instance an anecdote: my father, who is non-technical to the extreme (for years, his bosses when confronted with sales-critters trying to sell some unbreakable piece of tech sent the said critters to my father, who usually succeeded in breaking the piece of tech by just picking it up)... Er... My father, he's created two users on his windows computer: one for his images & general browsing, and another, more private user for his taxes and book-keeping. He has grasped the idea of separating users according to tasks. \n\nAnd that gives me the confidence to say \"people who say ordinary non-technical computer users cannot grasp the concept of users to differentiate tasks and promote privacy are people who are steeped into computer usage themselves and have lost touch with the ordinary people they like to instance\"."
    author: "Boudewijn Rempt"
  - subject: "Re: Osnews article"
    date: 2006-06-30
    body: "ok, I see you really don't get it.\nI for example teach people how to use Adobe Premiere, After Effects, 3ds Max and other animation video and effects software. I have some computers at class... people usually value they can check mail and chat at breaks of before/after classes.\n\nOf course these machines have a user specifically created for student access only. But since there's no real work to protect there and because of other practical reasons (users change not always sit at the same machine every day), there's not one user per student but only one \"student\" user.\nIn Windows I have no problem with IM clients, because people just connect, chat, and disconnects without worrying for anything else.\nBesides the fact that Adobe products are not (yet) ported to Linux, if I wished to replicate this kind of setup using KDE I would have a serious problem with Kopete.\nIn this kind of setup, people just have to be able to access to IM with minimum effort and without any supervision (read cost), and still be able to keep his contact list secure.\nI think Kopete developers should take into account these very common situations.\n\n\nWith regard to the example you give of your father, may I remind you you are still talking about what I call a \"sedentary\" user (one that is on *his* machine, the one that uses everyday), but I am talking about needs of \"nomad\" users (users not using his machine, but any machine they get temporary access to). This kind of user just need to be able to satisfy their need, quickly and easily and is not interested nor inclined at that time to learn anything new or complicated to be able to do so. If they can't do it, they will just complain and quit.\nI have much experience seeing this kind of people in action and let me tell you this is how they act.\n\nCheers.\n"
    author: "Gabriel Gazz\u00e1n"
  - subject: "Re: Osnews article"
    date: 2006-06-30
    body: "You were, originally, talking about your personal machine, not about an institutional setting. For those cases, where there's competent system administration, kiosk is an option, as are remote home dirs."
    author: "Boudewijn Rempt"
  - subject: "Re: Osnews article"
    date: 2006-07-01
    body: "You didn't read right, I ended my original message this way:\n\n\"In its current state, Kopete is a great IM client for personal use, but it's useless for deploying at public places or just for someone trying to ocassionally connect from someone else's machine.\"\n\nI talked about my personal machine only as a concrete example, but not by any means as an example covering all possible cases of use.\n\nI really don't get why is so difficult to assume that there are people with different needs. All solutions kindly provided by you and others here are simply not what some users expect, no matter how logical or suited to your way of thinking this solutions could seem to you at this moment.\n\nCheers,\nGabriel\n"
    author: "Gabriel Gazz\u00e1n"
  - subject: "Re: Osnews article"
    date: 2006-06-19
    body: "That was the worst written criticism article I have ever read. He opens with complaining about some 'coherency' issue he can't even explain, and closes with a point that he doesn't want coherency by saying he wants icons here to behave differently then they do over there.\n\nJust in case the author of it sees this, people mailing you asking 'what in gods name are you talking about, what do you mean,\" is not a sign of success, it's a sign you failed to present your points."
    author: "Sean"
  - subject: "Re: Osnews article"
    date: 2006-06-21
    body: "Please re-read what he wrote. He says he wants more coherency. The point you mention about icon size is about consistency, not coherency. He explained the difference in the article."
    author: "oliv"
  - subject: "Re: Osnews article"
    date: 2006-06-19
    body: "That is just a rant of a 22 year old geek who thinks he has usability knowledge...\n\nI think he should get a life first before becoming a GUI expert, then he would be able to write constructive critisism about GUI's, in stead of rants like this..\n\nBut lets start commenting on his rant:\n\nthe coherency part: it's a large rant about kde not being coherent, without coming with real examples.\nWith kopete he failes to explain why kopete does not integrate with kde as a whole. I think kopete integrates nicelly, it integrates very good with kdepim.\n\nWith Amarok he completely misses the fact that Amarok is not part of KDE, it's a KDE application (like Winamp is not part of Windows, it's a Windows-application)\nAnd what's wrong with having a live-cd?\n\nThen the KDE root password dialog.\nNever seen that one before, the usual kdesu-dialog shows a line of text explaining that you need root-priviliges to continue, and that you can hit 'ignore' if you don't want to do that, but want to continue with the current privileges.\nSo must be a Kubuntu-thing, not a KDE-thing..\n\nThen klaptop: no explanation what's wrong with it.\n\nAdept: well, like Amarok, that is not a kde application\n\nSetting the icons size in konqueror seperate from the desktop: you actually can do that, by defining it in the .directory file of e.g. ~/\n\nAccidently dragging toolbars: never happened to me :)\nand is probably lockable using kdekiosk\n\nNaming scheme: ok, we probably should have called KDE iDE, and use names like iMail, iWord, iMusic, etc :)\nAnyway, the renaming is in progress, so this issue is being addressed.\n\n\n"
    author: "AC"
  - subject: "Re: Osnews article"
    date: 2006-06-19
    body: "Actually being able to lock the toolbars is the only point I agree with. Not because I move them by accident, but because the application looks tighter without the toolbar handles."
    author: "Carewolf"
  - subject: "Re: Osnews article"
    date: 2006-06-19
    body: "most styles offer an option to not show these handles... but yeah, the ability to hide them would be cool."
    author: "superstoned"
  - subject: "Re: Osnews article"
    date: 2006-06-19
    body: "To be honest, I believe KDE is perfect (yeah, kinternet is a mess but...). It is better than windows, at least.\n\nWhat is the problem then?\n\nIt is not KDE which needs to get improved. The problem lies on the distributor level but I am sure KDE can help the integration.\n\nWhat do I mean?\n\ni installed Suse 10.1 but\n- don't get internet connection, played around with the settings. Magically it works or not\n- don't get sound, before I used alsaconf, no really good solution\n- applications sometimes die when started (memory issue?)\n- Mouse wheel does not work, so I cannot use the scrollbar\n- when using the bar in konqueror I am informed about non-working protocols; drives etc.\nUsually no real indication how to solve the problem. \n\nAll these issues have nothing todo with KDE. The joke is, something works with Kubuntu, something works with SuSe. Murphy II: An upgrade will reverse the problem. Sound will not work anymore. Your mouse is not detected anymore etc.\n\nWhether KDE feels usable ot not depends on these issues. Usability engineers tend to compare a working installation a with a working installation b but the problem is: non-working installation.\n\nI wonder how many Linux installations work 100% out of the box as they were intended.\n\nThe real problem for a user is then how to find out what goes wrong and why it goes wrong and find a way to fix the problem. KDE's error screens are often not helpful."
    author: "Jim Der"
  - subject: "Re: Osnews article"
    date: 2006-06-19
    body: "looks like your installation corrupted somehow...\n"
    author: "AC"
  - subject: "Re: Osnews article"
    date: 2006-06-22
    body: ">> - don't get internet connection, played around with the settings. Magically it works or not\n\nThat's not related to KDE\nKinternet is also not part of kde, it's a suse application (kde ships with kppp)\n\n>> - don't get sound, before I used alsaconf, no really good solution\nAlso not related to KDE, SUSE uses YaST for hardware configuration.\n\n>> - applications sometimes die when started (memory issue?)\nno, probably means that your installation is corrupted.\nYou could try to switch of beagle (with the sysconfig editor of yast, or by removing the application including kio_beagle and kerry), and see if that helps. Without beagle, suse 10.1 should run like a breeze with only 256 mb ram\n\n>> - Mouse wheel does not work, so I cannot use the scrollbar\n\ntry selecting a different protocol in SaX2 or YaST.\n\n>> - when using the bar in konqueror I am informed about non-working protocols; drives etc.\nCould also indicate that your installation is corrupted.\ndid you download/burn de cd's/dvd yourself?\n"
    author: "AC"
  - subject: "Re: Osnews article"
    date: 2006-06-21
    body: "\"With Amarok he completely misses the fact that Amarok is not part of KDE, it's a KDE application (like Winamp is not part of Windows, it's a Windows-application)\"\n\nWrong comparison.\n\nAmarok web site is amarok.kde.org\nAmarok, is in KDE SVN (and thus discussed in KDE commit digest, e.g. in this week's one ( http://commit-digest.org/issues/2006-06-18/ ) )\n\nSo I do not think the relation between amarok and KDE is comparable to the relation between Windows and Winamp (IOW, winamp website is not at microsoft.com, and the CVS/SVN of winamp is not hosted by MS either)."
    author: "oliv"
  - subject: "Re: Osnews article"
    date: 2006-06-22
    body: "The fact that something is developed in kde's SVN does not mean that it is actually part of KDE itself. \n\nAmarok does not ship with KDE, it is a seperate project, that could have been hosted on sourceforge etc..\nJust like many other applications, like kaffeine, kplayer, digikam, ktorrent, karchiver, kile, etc etc..\n\nSo Amarok is a kde-application, but not a kde-application, just like winamp is a windows-application, but not a microsoft-application.\n"
    author: "AC"
  - subject: "Re: Osnews article"
    date: 2006-06-22
    body: "No, not \"just like\". Microsoft does not host winamp website in their domain, Microsoft does not host winamp SVN/CVS. There is absolutely _NO_ link between MS and winamp.\n"
    author: "oliv"
  - subject: "Re: Osnews article"
    date: 2006-06-22
    body: "Your missing the point :o)\n\nBesides, the fact that kde provides hosting for other applications does not imply that the developers of those applications are in any way involved in KDE itself. They follow their own plan, and not KDE's plan.\n"
    author: "AC"
  - subject: "Re: Osnews article"
    date: 2006-06-25
    body: "Agree.\nEveryone should try Slackware's KDE. This is the real KDE. Most distributions \n modify KDE and Gnome. So it's unfair to compare them."
    author: "Sotux"
  - subject: "Re: Osnews article"
    date: 2006-06-19
    body: "That guy is a complete moron. Like his predecessor, Eugenia Lopez Queru, he has no formal qualifications as either a journalist, software developer or usability professional.\n\nHe spends the day reposting articles at other sites and then \"delights\" everyone on Sunday's (they must be his day off from kindergarten) with a poorly written editorial that lacks any sense of cogency.\n\nI really cannot stand people like this guy. In previous times, this guy would have shouted in his hometown about how the end of the world was near or some other nonsense. Today, the web makes his steeple a lot bigger.\n\nThere is no point in even attempting to reply to something as poorly written or thought out as what he wrote. If he wanted to effect any change on KDE, filing a reproduceable bug would have been the way to go. "
    author: "Fuck OSNEWS!"
  - subject: "Just wow."
    date: 2006-06-19
    body: "KDE has many weaknesses but I think he ended up addressing Kubuntu's weaknesses instead. People already pointed out the kdesu is Kubuntu's design flaw, not KDE's. Adept is buggy and ugly, but it's not a KDE app (that would be Kpackage, which is rather sticky in its own way is not as bad as Adept).\n\nHe doesn't even bother critiquing Klaptop properly. I don't understand his gripes about Kopete and I don't use it. It integrates pretty well with KDEPim when I used it. To be fair, my main gripe against Kopete is probably minor. I want to save my logs in plaintext or have the ability to export them as plaintext. Silly and nitpicky? Yes, but it's a quirk I have so GAIM until then.\n\nProbably the oddest thing he went on about was coherence. KDE is probably one of the most coherent DEs I ever used. It and OSX are the only ones that implement spellcheck properly: system-wide in the browser, email app, etc. I don't remember passwords for anything anymore because of KWallet. Klipper and Kontact, while not perfect, are hardly incoherent."
    author: "sundher"
  - subject: "Re: Just wow."
    date: 2006-06-20
    body: "Personally I like adept.  Sure I wish it would remember that I hid the tags because I never use them, and I wish it was a bit faster sometimes, but in general it is excellent for what it was designed for: finding and installing applications.\nI tried Synaptic and Kynaptic and KPackage and none came close to moving me off Aptitude.  But with Adept I'm happy giving up some of the advanced features of Aptitude for ease of use."
    author: "Leo S"
  - subject: "Re: Osnews article"
    date: 2006-06-19
    body: "The problem is not really the article itself, but the way that it was presented.\nAll of his usability points are valid, but maybe not expressed clearly and more important constructively. I do not thing that non-constructive critism is inherently useful for KDE or for any other project open source or not.\n\nSo, let me explain what his trying to say in a constructive way.\n\nFirst of all, this article talks about deficiencies found in the KDE 3.x series,\nthat most KDE users have in a way or another experienced.\n\nWhat is not told in this article is that many of those critism are being addressed in the future KDE 4.x series. More importantly the work being made by Aaron Seigo, who seemed to have introduce the idea in the first place, and the rest of the KDE4 elite on projects such as: Phonon, Solid, Plasma, Oxygen, Dashboard and Akonadi. Also, notice the lacks of K in the new names.\n\nThis is what this guy is talking about when he's mentionning \"coherence\".\nSolid will make sure that every app behaves \"similarly\" and \"coherently\" for the hardware layer. Similary, Akonadi and kdepimlibs will ensure that every \"personal\" apps behaves \"similarly\" and \"coherently\" for the user information layer. Samething, for the new \"search interface layer\", the oxygen layer, the multimedia layer, etc.\n\nThis is one of the stated goal in KDE4 and while it was not really \"put together\" in KDE3, the issue is being addressed and personnally, I do not think that bashing the nail on KDE3 defects will help motivate those hard working developpers, artists, translators, designers and other specialists on solving these issues for KDE4. \n\nOne of the other problem mentionned is the comparision with MacOS and Windows,\nthe thing is until KDE has a complete abstraction layer for the OS, it cannot be compared on these grounds, since the backend will stay with some of the Linux problem at some point. So, it's like comparing orange and apple.\nA solution should be found, I think we are in a good track for KDE4 and more work still has to be done.\n\nFinally, the amaroK live cd seems to be just a sarcastic way of showing that the KDE3 emphasis is more application-oriented instead of being abstraction-layer-oriented like it seems to be with KDE4.\n\nPoint 2. kdesu dialog needs love. I think everyone agree, the main problem is that it is too technical and not really non-geek friendly. While the technical information is useful, the main information should be \"extremely user friendly\".\n\nThe command being executed is \"kdesu adept\".\nThe problem is, there is no explanation on what is adept and why I need root.\nSomething like \"Please enter your administrator password to install some new application using adept, the Kubuntu installer\" would be of great help!\nAssuming we trust the explanation...\n\nBasically, it misses a [?] icon.\nIt should be written  \"root password\" or \"administrator password\".\nObviously, those description should be \"PGP signed\".\nSince you could have \"kdesu trojan\" with the same help sentence:\n\"Please enter your administrator password to install some new application.\"\n\nPersonnally, I didn't know what adept was, prior to this article and I still don't even know:\nWhat is the difference between \"Cancel\" and \"Ignore\" ?\nWhat does Ignore do?\nWhat does Cancel do?\n\n\"Run as root\" gives a major clue, but it's still geek speak, for the average person.\n\nPoint 3. Never used it, so I cannot comment.\n\nPoint 4. adept is horrible and I think that \"Klik\" is personnally the way to go,\nsince it's much easier. The other favoritism I got on installer are click-click-click-finish wizard based installer.\n\nPoint 5. I found personnally that configuring hard stuff is easy,\nand easy stuff very difficult due to the configuration noise.\nBasically, the problem is finding the 90% task that everyone configures\nis hard to find in the zillion configuration settings.\n\nI think that KControlPanel is some advance mix of RegEdit and Power Tweak,\nwhich is useful, while a real \"control panel\" needs to be designed\nfrom scratch for neophyte and classical task:\n\n- Monitor size, angle, colors\n- Color scheme, background, Screen Saver\n- Date/Time\n- Keyboard, language, regional settings\n- Sound, Mixer settings\n- Printer, Scanner, Fax settings\n\nSome other stuff that are classic:\n- KMenu settings\n- Laptop/Battery stuff\n- Hot keys\n- Simple network settings\n- Fonts settings\n- Users accounts\n\nAny other options is rarely changed by the majority of people.\nBasically, any \"dying configuring needs\" on first install or common task,\nlike changing resolution, color, sound should be EXTREMELY easy, obvious and no-brainer.\n\nIn conclusion, I think that if people start explaining in a constructive way,\ncommon problems and start giving a real constructive answer instead of a rant\nthat says \"it's wrong, this sucks\", maybe more people would be willing to help out.\n\nAs a last word, be constructive, be positive and keep up the good work!\n"
    author: "fp26"
  - subject: "Re: Osnews article"
    date: 2006-06-19
    body: ">>The command being executed is \"kdesu adept\".\n>> The problem is, there is no explanation on what is adept and why I need root.\n\nTha is an error in kubuntu, not in KDE.\nKDE's kdesu tells you why you need to be root\n\n>> Something like \"Please enter your administrator password to install some new application using adept, the Kubuntu installer\" would be of great help!\n\nThe default kdesu would do that.\nThe kdesu of kubuntu not, since there is no administrator password present. In the dialog you need to type your own password, not the root password (there is no root password in kubuntu)\n\n \n>> It should be written \"root password\" or \"administrator password\".\n\nit does in the default kdesu\n\n>> Obviously, those description should be \"PGP signed\".\n\nno idea why\n\n>> Since you could have \"kdesu trojan\" with the same help sentence:\n \"Please enter your administrator password to install some new application.\"\n\nkdesu shows the command it wants to execute, surelly you can trust that. \n\n\n \n>> What is the difference between \"Cancel\" and \"Ignore\" ?\n\nThe default kdesu would have told you.\nI'm starting to wonder: you never used kde before?\n\n>> What does Ignore do?\nAccording to kdesu: you can continue to execute the command using your own privileges. So you ignore the request of becoming root.\n\n>> What does Cancel do?\nyou cancel the whole operation\n"
    author: "AC"
  - subject: "Re: Osnews article"
    date: 2006-06-20
    body: ">> kdesu shows the command it wants to execute, surelly you can trust that.\nAsking to trust something most newbies (and even some experienced users) don't understand? It's not much better than executing something with su priviledges without asking..."
    author: "Wrawrat"
  - subject: "Re: Osnews article"
    date: 2006-06-21
    body: "I personnally never used kdesu, ever.\nIf I need root I open a shell and do it from there, but the usability point is valid.\n\nIf the error was made by Kubuntu then someone should contact them to fix the problem, in the first place."
    author: "fp26"
  - subject: "Re: Osnews article"
    date: 2006-06-22
    body: "indeed,\nBut the autor of the article wants to rant, not to help :)\n\nFor example kubuntu's kdesu: there is only 1 password that can be filled in, and that is of the user (like the dialog states: 'Enter your password'). So it's not the password of root, administrator or god-mode: it is the password of the user that installed kubuntu in the first place...\n"
    author: "AC"
  - subject: "Re: Control Panel"
    date: 2006-06-19
    body: "(please excuse me for by bad english)\n\nThe \"Configuration Panel\" criticism against KDE is very common (almost every troll i had with GNOME users ended up speaking about \"the dirty all-in-one panel\").\n\nI personnaly have an idea of how this could be made easier _for my personal use_ (i don't pretend to know what other people like), but i never told that because it seemed me it someone likely already had this idea :\nWhat about \"configuration levels\" ?\nFor example, you may have four levels, \"Beginner, User, Geek, KDE Guru\", and everytime you increase the level, you access to more options.\n\nIf that was done \"coherently\" :p , I think this could make the configuration thing a lot easier."
    author: "bluestorm"
  - subject: "Re: Control Panel"
    date: 2006-06-19
    body: "Just compare it  to the windows control panel....\n\nThe fact is: control panels are obsolete.\n\n1. hide functionality\n2. minimize sources of errors\n\nControl panel criticism is often KDE control panel as provided and translated by distributor XY."
    author: "Jim Der"
  - subject: "Re: Control Panel"
    date: 2006-06-20
    body: "Configuration levels have been brought in the past, and the idea always gets shot down because:\n\n1a) You're asking the user to self-evaluate their level of expertise and\n1b) assuming that said evaluation will be made on the same criteria the developers would use when deciding where to put the options\n\n2) Options don't necessarily map easily to experience levels. You may be able to rank configuration options in order of how often they are used, but \"the fewest number of users fiddle with option X\" doesn't necessarily translate to \"only 'experts' will want to fiddle with option X.\"\n\n3) It doesn't really solve any clutter problems. Even experts don't necessarily want all possible options to be presented to them at once. There might be some truth to the idea that a beginning computer user might be flustered when presented with too many options, but that doesn't imply that advanced users *want* to be presented with many, many options. However, user levels, as they are usually presented, correlate \"experience with a computer\" with \"willing to put up with cluttered interfaces.\"\n\nI've been playing around with gnome for a few days, and I have to admit, I don't find the decision to throw seldom used options into gconf-editor *that* bad. The main problem I have with it is that when you put them there, it's difficult to find them, and to be sure that you're changing the options for the correct app. Perhaps there's a way to rectify that, or perhaps there's an altogether better solution, but user configuration levels seems to be generally agreed upon by people allegedly in-the-know as not the way to go."
    author: "Dolio"
  - subject: "Re: Control Panel"
    date: 2006-06-21
    body: "I'm an expert and personnally, user-level sucks.\n\nLike I said before, the problem is that we have a PowerTweak called Control Panel and no \"real control panel\".\n\nLet me say this straight, I don't want or wish to modify control panel \nit's fine, for advance stuff.\n\nThe problem is we need something else to fit \"another need\", which is \"simple common task\".\n\nIn other words, it's like tools. If I want a Nuke to kill some advance current setting needs, I want the current solution. But if I just want a hammer, to change the resolution, sounds or rotate the screen 20x a day, since I don't want to fiddle with zillion other choices... \n\nSo like other people says it's not really \"user level\", it's more a question of having the right tool for the right task with the right GUI.\n\nMaybe the solution is just to create some applet:\n- a \"monitor\" applet, with all possible resolution, color and rotation.\n- a mixer applet for sound settings.\n- a regional setting applet for keyboard and formats.\n- a date/time applet\n- a printer/fax applet\n- a network applet\n- a battery applet\n- a font settings applet!?\n- a user account applet!?\n\nTo right click on background to get the background image, screen savers, settings.\n\nThese are very common task.\n\nIt's not like changing the accelerator of X application\nor changing the gamma settings, which you gonna do once \nor twice in your life time.\n\nThe other option is to put the remaining stuff in an \"advanced >\" tab.\n"
    author: "fp26"
  - subject: "Re: Osnews article"
    date: 2006-06-19
    body: "Man, I like your opinions. KDE team needs people like you."
    author: "jose"
  - subject: "Some usablility issues (personal prefs)"
    date: 2006-06-20
    body: "I am a rather novice user of KDE but please do allow me to say a few words on the things that I feel are quite undesirable during my use of KDE so far. (Disclaimer: I am using KDE in FC5, installing whatever updates of KDE that are in the repos)\n\nFirstly, I have an issue with the windows displaying in a 800X600 screen. The configuration windows usually are too big to fit into the screen size and the OK and Cancel buttons go out of screen. Initially I thought there might be a scroller bar at the side but I found none. It took me quite a while to know I can move the window by moving the mouse while holding down the Alt key and left mouse button. It will be great to have the ability to resize the config windows and have a scroll bar.\n\nSecondly, I always get the sense of an information overload whenever I open the system config apps. I am presented with too many options yet those options I play around with (those which a normal user will use to customize the look-and-feel of the desktop) aren't visibly noticeable at a glance, often buried by the rest.\n\nThirdly, a point which closely follows the second point, is that I find the default desktop layout too cluttered. I am not working for Google but I kinda like the Google concept whereby everything on the screen is presented 'cleanly' with the icons, buttons and texts unobtrusive (as oppose to Yahoo!).\n\nMy preference is of a plain desktop that upon first-time login, introduce the user to the various aspects which he/she can customize the desktop.\n\nJust my two-cents worth..."
    author: "James"
  - subject: "Re: Some usablility issues (personal prefs)"
    date: 2006-06-20
    body: ">>Firstly, I have an issue with the windows displaying in a 800X600 screen.\n\n\ni don't have that problem over here.\nPerhaps you need to choose a smaller font, so the dialog windows will appear smaller\n\n>>Secondly, I always get the sense of an information overload whenever I open the system config apps. \n\nWell, that's the problem with configuration dialogs, you can't present them all without allowing a lot of options.\nOne way to solve this is by using 2 configuration dialogs, like in windows (control panel vs regedit), firefox (preferences vs about:config) or gnome (control panel vs gconfigeditor), but problem with that is that users will always have the need to configure something that is not find in the first dialog, so they need to dig into the more complicated second one...\n\n\n>> Thirdly, a point which closely follows the second point, is that I find the default desktop layout too cluttered. \n\nThat's up to you distribution: they decide what's on the desktop and what not, KDE has no influence on that.\nHaving said that, my experience is that the default desktop of most linux distrutions is rather empty, not cluttered or so..\n\n>> My preference is of a plain desktop that upon first-time login, introduce the user to the various aspects which he/she can customize the desktop.\n\nKDE has a first run wizard, which helps you to configure KDE without the need to dig into kcontrol. This first run wizard should start the first time you start KDE, but again, that depends on your distribution.\n"
    author: "AC"
  - subject: "Re: Some usablility issues (personal prefs)"
    date: 2006-06-21
    body: ">> Perhaps you need to choose a smaller font, so the dialog windows will appear smaller\n\nThe reason I am displaying in 800x600 is to have the fonts easy on the eyes so making the fonts smaller defeats the purpose. I will rather have the vertical scroll but again, that is only my preference.\n\n:)\n"
    author: "James"
  - subject: "Re: Some usablility issues (personal prefs)"
    date: 2006-06-21
    body: "Hi,\n\nYou should run x-windows with standard size for your monitor and card and rather change the fonts to a larger size if they are to small for your eyes.\n\n "
    author: "Dragen"
  - subject: "Re: Some usablility issues (personal prefs)"
    date: 2006-06-21
    body: "Sory but this is a *typical* Windows user failure due to severe usability problems of the incredible dumb Windows preferences dialogs.\n\nOn Windows you are not able changing the fontsize for *all* GUI elements at once (and after you have done it for all GUI elements the result looks still rather strange) while on KDE this is one of the easiest things you can imagine (and globally choosing larger icons is also a very easy thing in KDE).\n\nPlease if you really want to do your eyes something good choose the highest physical resolution provided by your screen mask (but by any circumstance no higher, even if your computer setting suggests it). Thus you will have on Linux (KDE) a sharp interface and big GUI elements at the same time. Look into your monitor data sheet for the physical monitor resolution. It is most times given in milimeter per pixel (called dotsize) or the other way round in DPI = dots per inch (be aware of the differences between inch and milimeter). With these values and the width and hight of your monitor you can compute what resolution your monitor is really able to display.\n\nAs a rule of thumb a 17 inch cathode ray tube can display 1152x864 pixel but not more (if you choose more you fake a resolution that does not exist and will lead to bluring).\n \nI really hope that Bill Gates himself will one time fix Windows settings so that I don't need to explain anylonger to the people that 800x600 is the worst thing they can do to their eyes.\n"
    author: "Arnomane"
  - subject: "Re: Some usablility issues (personal prefs)"
    date: 2006-06-21
    body: ">>Secondly, I always get the sense of an information overload whenever I open the system config apps. \n\n>Well, that's the problem with configuration dialogs, \n>you can't present them all without allowing a lot of options.\n>One way to solve this is by using 2 configuration dialogs, like in \n>windows (control panel vs regedit), \n>firefox (preferences vs about:config) or \n>gnome (control panel vs gconfigeditor), \n>but problem with that is that users will \n>always have the need to configure something that is not find in the first \n>dialog, so they need to dig into the more complicated second one...\n\nBut that's fine, like Larry Wall at one point said.\nIf you do something simple, then it should be simple.\nIf you try to do something complicated, then it should be \"feasible\".\nIt also means that \"complicated\" things are not for neophyte,\nbut not \"impossible to do\" or \"impossible to learn\".\nIt also means that \"some extra energy\" can be put to \"look for it\",\nsuch that common task are still kept as simple and as easy as possible.\n\nLike in any design, it's a trade-off.\n\nAlso, I don't know if any of you folks have look into commercial solutions\nsuch as Plesk and cPanel, but having an equivalent in KDE would be welcomed!\nThey are also a good starting point as a solution \"that works out of the box\",\n\"neophyte can learn and use easily\", \"advanced settings are present and easy\".\n\nIt contains everything someone needs to configure Linux and perform\nadministration without ever using SSH for 99.9% of the task, simple and intuitive with gorgeous graphics. Might be a good inspiration point.\n\nhttp://www.dotsynergy.com/pix/cpanel.jpg\nhttp://www.featurehosting.net/images/controlpanel/cpanel.jpg\nhttp://www.netlynx.org/images/plesk.gif\nhttp://www.internet-webhosting.com/images/plesk-big.jpg\nhttp://www.abcinternet.com/gif/plesk-file.gif\n\nNotice the logical grouping and that the number of icons per grouping\nis manageable. The icons are big, gorgeous, descriptive and hard to mismatch.\nFurthermore, \"advance stuff\" is there, \"easy stuff\" is there, it's all well organized.\n\n\n\n\n\n\n\n\n"
    author: "fp26"
  - subject: "Re: Osnews article"
    date: 2006-06-20
    body: "He did say that the rant is about Kubuntu's KDE. So yes, criticisms about kdesu or Adept are valid. What I don't understand is the \"coherency\" thing. By the comments to his rant, it seems he didn't explain that part well at all."
    author: "Isaac"
  - subject: "Re: Osnews article"
    date: 2006-06-22
    body: ">>He did say that the rant is about Kubuntu's KDE\n\nHe does now :)\nBut still, his points on kdesu and adept are not kde related, they are solely kubuntu related.\nSo he should have concentrated his rant to kubuntu, in stead of kde..\n\n"
    author: "AC"
  - subject: "The distribution"
    date: 2006-06-23
    body: "KDE is a very flexible environment. Kubuntu has the possibility to make the desktop coherent, just with a little tweaking here and there.\n\nExamples: Why install Amarok per default? Why not use JuK and call it \"Jukebox\". Why not call Kaffeine \"Mediaplayer\". \n\nMany of these small things that makes the desktop coherrent to the user, have been ignored.\n\nI think the key is to make the default settings simple and coherrent. And make it easy to restore those settings."
    author: "ale"
  - subject: "Re: The distribution"
    date: 2006-06-24
    body: "> Examples: Why install Amarok per default? Why not use JuK and\n> call it \"Jukebox\". Why not call Kaffeine \"Mediaplayer\".\n\nOk, do the same with Windows apps. Why not call Winamp \"Mediaplayer\"? Why not call Nero \"CD & DVD Burner\"? Why not call Excel \"Spreadsheet Calculation\"?\n\nThis has been done for GNOME, and I hate it. Aren't there many media players and burning applications? How is the other one called if I choose to install an alternative one? Is it necessary to rob an application of its name? How can you ever promote a cool app if it hasn't even got a name?\nLike, you know, it sounds like\n\"Hey, media player has awesome new features, right?\" - \"Media player? Which one?\"\n\nReally, it's totally sufficient to have the description besides the application name in the start menu, like it is now in KDE, with entries like \"Amarok (Audio Player\" or \"K3b (CD & DVD Burning)\"."
    author: "Jakob Petsovits"
  - subject: "Thanks"
    date: 2006-06-18
    body: "Thanks for making smaller text an option!"
    author: "anonymous"
  - subject: "CTRL + \"-\""
    date: 2006-06-19
    body: "CTRL + \"-\""
    author: "max"
  - subject: "all links to diffs are broken"
    date: 2006-06-18
    body: "there is one additional space in all links\n"
    author: "joi"
  - subject: "Re: all links to diffs are broken"
    date: 2006-06-18
    body: "Fixed, thanks!\n\nDanny"
    author: "Danny Allen"
  - subject: "Re: all links to diffs are broken"
    date: 2006-06-18
    body: "look at 9. an 10. digest - the same happened before and nobody noticed it :)"
    author: "joi"
  - subject: "Amarok is spelled wrong"
    date: 2006-06-18
    body: "In the last digest you stated that amaroK had changed name to Amarok, but it still says amaroK in the \"program buzz\" part of the digest\n\ncheers"
    author: "pascal"
  - subject: "Re: Amarok is spelled wrong"
    date: 2006-06-19
    body: "heh - thanks for the heads up, it would probably have been months before I noticed ;)\n\nThanks,\nDanny"
    author: "Danny Allen"
  - subject: "Re: Okular too?"
    date: 2006-06-19
    body: ">oKular is now known as okular.\n\nand I wonder if that shouldn't be Okular.\n(Else somebody probably just wanted to please the shift-key-challenged)."
    author: "Anonymous"
  - subject: "Not that icon thinggy again... "
    date: 2006-06-19
    body: "\"An enhanced version of the custom iconset developed during the 1.4 phase is re-enabled as the default option in Amarok.\"\n\nEveryone and his dog recognized that this is bad in terms of consistency. Someone has to reopen the bug http://bugs.kde.org/show_bug.cgi?id=125295"
    author: "Anonymous Coward"
  - subject: "Re: Not that icon thinggy again... "
    date: 2006-06-19
    body: "not the same discussion again - very boring. what is your problem with amarok/KDE consistency? Amarok's icons are consistent with my cd-player, tape-deck, walkman, mp3-player; the symbols speak the very same \"language\". and all other media apps use the same symbol language. what's your problem that amarok uses it's own icons and not the stock ones? the icons in amarok's custom iconset are easy to recognize; nothing compared to some very special function with an hard-to-remember icon in an office application - that's the place where consistency is important and reduces training. Amarok isn't even a core KDE app and you think that you can force them to provide an appearance which you prefer instead of the one the amarok developers like their application to look. and the custom icon theme looks good - that could not be the problem, or is it? perhaps the amarok developers think that their app should look consistent within itself - and that would be a very good reason to provide an own icon theme. stock icon themes don't provide all of the special icons needed for amarok, so they have to be provided by amarok. and now tell me by which magic the other icons from your favorite icon theme can be 100% consistent with these? You are not even forced to use the new custom icon theme. don't waste everybody's time with such a stupid discussion."
    author: "AC"
  - subject: "Re: Not that icon thinggy again... "
    date: 2006-06-19
    body: "Any good icon set uses symbols that a user can understand instantly. The problem people have is that it should use the default icons of the user. Not everyone has a blue/white theme on their desktop. If they want to fill in the gaps of the icon set I'm using that's fine. But my play, rewind buttons, etc. are adequate. Just because it isn't a core KDE app doesn't mean it's immune to criticism. And judging by the number of people voting for the bug it's not a \"stupid discussion\" to many people."
    author: "anon"
  - subject: "Re: Not that icon thinggy again... "
    date: 2006-06-19
    body: "O.K., my comment was a little bit too harsh. But my points remain: If someone prefers the KDE theme, it's very easy to reenable this in the settings menu. AFAIK, the new theme is just the new default, but you are not forced to use it and it can be changed in an easy way. that should please everyone."
    author: "AC"
  - subject: "Re: Not that icon thinggy again... "
    date: 2006-06-19
    body: "Point is, with this default, amarok neglects users who took the time to install and choose their own icon theme. Make amarok ONLY use its custom icons when a user uses the default KDE icon theme, or change the default, but don't ignore users who choose another icon theme for KDE..."
    author: "superstoned"
  - subject: "Re: Not that icon thinggy again... "
    date: 2006-06-19
    body: "Well, you had the chance to convince them otherwise while you were hosting them in Achtmaal :o)\n"
    author: "AC"
  - subject: "Re: Not that icon thinggy again... "
    date: 2006-06-20
    body: "of course i tried ;-)"
    author: "superstoned"
  - subject: "Re: Not that icon thinggy again... "
    date: 2006-06-20
    body: "Although I like the new icons, I agree that they shouldn't be the default.  Just like the crazy colours that Amarok used to have.  First time I tried Amarok I saw those crazy colours and immediately quit the app.  I don't have the patience to figure out apps that try to do their own \"cool\" thing by default.\n\nMake it an option or something.  The icons look really nice, and I'll probably turn them on, but I think a lot of people will be annoyed by the non-standardness.  Especially disabled people."
    author: "Leo S"
  - subject: "Re: Not that icon thinggy again... "
    date: 2006-06-19
    body: "Just think about accessibility. If users have a high contrast icon set by default because they have low visual capabilities, then imagine that user seeing Amarok with lots of icons he can't distinguish.\n\nSure, he could go through the config settings and disable it. But what if he doesn't know about the option to turn the Amarok icon theme off at all? He would just say: I can't use this, and never look back at Amarok.\n\nThese users already have a hard time configuring everything to suit their needs, so I think it's inappropriate to have them go through more config options, when that user already did the appropriate thing to have icons that suit him: in the KDE control center, where he chose the high contrast icon set."
    author: "Selene"
  - subject: "Re: Not that icon thinggy again... "
    date: 2006-06-19
    body: "Have you really looked at the new icon theme? and can you imagine what people with low visual capabilities see? the essential icons in fact have a quite high contrast (dark blue or red vs. light gray); the contrast for a b/w version would not be that much higher. the colours chosen are easy to distinguish with red/green and even blue colour vision deficiencies; all colors are used against a bright background and not in direct combination with other colours. if you don't believe this, install imagej (requires java) with the vischeck plugin http://www.vischeck.com/downloads/ and try this yourself. I don't see any problem there. people with common visual disabilities should be able to recognize the icons; a much bigger problem is the fact that icons could not be resized."
    author: "AC"
  - subject: "Re: Not that icon thinggy again... "
    date: 2006-06-19
    body: "The link you gave was to test colorblindness, not near legal blindness. It's silly to say a person with strong visual defects \"should recognize\" the icons. If they could, I doubt they'd be visually impaired and need the high-contrast icon set. Even if they could, why should we make it their burden to change the icon set? We change icons system-wide to make their lives easier."
    author: "anon"
  - subject: "Please STOP your Trolling !!!!!!!!!!!!!!!!!!!!!!!!"
    date: 2006-06-21
    body: "[ ] You are an Amarok developer\n[ ] It is impossible to switch back to the KDE standard icon set\n[x] Everybody knows your whining about those icons\n[x] Either conribute or stop complaining!"
    author: "Max"
  - subject: "Re: Please STOP your Trolling !!!!!!!!!!!!!!!!!!!!!!!!"
    date: 2006-06-23
    body: "He doesn't have to be an Amarok developer to critisize a very stupid decision made by people who created a wonderfull player. There really are a lot of people annoyed by the fact that they have to alter Amarok to fit in, instead of altering Amarok to have its special look. Really why does the music player have to have other icons than the movie player or the tv-player? I don't like my stereo set to have a different play button on the cd-player than the one on the tape player. \n\nPeople aren't complaining about the new iconset, they are complaining about the decision to make it the default icons. If they want to include the icons, fine, but don't make me do some extra work to make Amarok fit with the rest of my desktop again."
    author: "Terracotta"
  - subject: "Game controller icon"
    date: 2006-06-19
    body: "Naah, quite PS2 like and too black.\nhttp://commit-digest.org/issues/2006-06-18/moreinfo/551216/#visual\n\nExcept it is intended to do every gamecontroller in existence - Gamecube, XBox, Dreamcast, N64, well everything back to the NES... and don't forget the Wii remote ;)\nI like the current SNES like controller, it's really good and general, and it has the classical grey plus the red-yellow-green-blue button combo everyone digs ;)"
    author: "Anonymous"
  - subject: "Re: Game controller icon"
    date: 2006-06-19
    body: "Have to agree with you.\nPlus the old snes-style controller works better as 16*16 icon."
    author: "Iuri Fiedoruk"
  - subject: "State of searching."
    date: 2006-06-19
    body: "So strigi is the new name for kitten which is the replacement for Kat. It apparently can use any backend such as xapian or cluecene. What I'm really curious about is the part where he says he wants to implement Tenor concepts. So has this project absorbed the Tenor one? And what's kfile?"
    author: "anon"
  - subject: "Re: State of searching."
    date: 2006-06-19
    body: "well, Scott wheeler was the person behind Tenor, and as long as he doesn't chime in on the Strigi development, vandenoever can't really say Tenor and Strigi are merged. tough he can include ideas from Tenor, and try to let Strigi provide the neccesairy architecture for Tenor."
    author: "superstoned"
  - subject: "Re: State of searching."
    date: 2006-06-19
    body: "This is great news! Tenor was/is/will be the WinFS/Duke Nukem Forever of KDE, i.o.w. vaporware. Strigi on the other hand is real code that gets the job done. I hope this will be incorporated in KDE4.\n\nNow all say; hail Strigi!"
    author: "WTAC"
  - subject: "Opensource Wonders"
    date: 2006-06-19
    body: "from strigi website:\n\"I've named the thing Strigi, because I hope it grows into a Kat.\"\n\nwell, as long as strigi means owl , I think with some patience and special care it might be :-)"
    author: "Mohasr"
  - subject: "Re: Opensource Wonders"
    date: 2006-06-19
    body: "If you read the sourcecode, you will find the line\n#include <magic.h>\n\n"
    author: "jos"
  - subject: "kalzium"
    date: 2006-06-19
    body: "Why apps like Kalzium are included in KDE? It is too specific application and I don't see the need for it to be included in Desktop Environment. Many people use computer only for browsing, mail and music."
    author: "pierre"
  - subject: "Re: kalzium"
    date: 2006-06-19
    body: "I think it's good that it's included, because for the user it is verry usefull that all the apps are released on the same date because it's for distributions easier to make KDE stable when all have the same release date.\n\nAnd that it's in KDE doesn't mean it has to be installed by default, look at kubuntu for instance, it does only have the most basic programs by default. "
    author: "David"
  - subject: "Re: kalzium"
    date: 2006-06-19
    body: "I is in kdeedu. Kalzium fits perfectly in kdeedu. It is not in kde_base_ for the reason you stated."
    author: "Carsten Niehaus"
  - subject: "Re: kalzium"
    date: 2006-06-24
    body: "There was a time when email and browsing was way to specific for average users. This time wasn't the stone age - it was just 10 years ago the masses started using email and the web.\n\nSo this argumenation leads to nothing exept the hidden reason \"it is unlike Windows\".\n\nIn the gold rush era of the internet (1996 - 2000) all people talked beside dot.com about new buzzwords: \"multimedia\" and \"edutainment\". Especially Microsoft promised to bring new digital educational tools to the students in class.\n\nBut did they keep their promise? No absolutely not. They miserably failed. The best educational software so far has been written by private persons that often are teachers themselves that simply know what it missing and that are passionate about something. And this is especially true for Kalzium.\n\nSo KDE didn't just spread buzzwords. KDE just started the KDE-edu module and out of small beginnings this module evolved into something cool that is even usefull by (semi-) professionals like for example the KStars application.\n\nAnd as Carsten already told you: If you don't wan't edu-software just don't use/install KDE-edu, KDE-edu is not KDE-base which gets installed automatically if you install any KDE app.\n"
    author: "Arnomane"
  - subject: "I love KDE3.5"
    date: 2006-06-20
    body: "Thanks you a lot \n\n\n   I would like to thank all the people who make KDE what it is today, it's a great job men !\n I am a fanatic Linux fan and especially KDE, I read all the time, web content related to KDE development, specially planetkde, actually I did not understand all the blog entry( it is a geeky planet) but it's always a fun to hear about ASEIGO neighbor problems ;) and why rich man like to build high wall !!! \t\n\n  Recently I installed kubuntu 6.06, I was so happy to see how much KDE is becoming an enterprise class GUI, even my 12 years  brother begin to use it naturally, (  well, I have changed  the mouse setup to double click before) of course not all thinks are so perfect, sometimes application crash without obvious reason,  and worst I can't figure out how to change the default view of Konqueror ( I like to view folders in detail list) but guess what is my killer feature  in KDE 3.5.2 !!\nIt's the ability to lock the panel, so thanks you aseigo !! I can change the configuration of kde to make it look as I like ( actually is a copy of AQUA) and then lock the panel ( a small feature can have a great impact on newbie like me;) \n\n  so please take all your time to made the transition to KDE4, because IMHO KDE3.5 is a great product right now, all we need is more polishing, bug fixes and code optimization. I hope to see some day KDE3.5.9 \n\n  I think what we really miss in Linux desktop today is more applications, it's all about APPLICATIONS, perhaps Google earth is a good example to show to ISV, I wish it is the beginning !!\n\n  GOD bless you men, you are heroes !!!     \n"
    author: "mimoune"
  - subject: "Re: I love KDE3.5"
    date: 2006-06-23
    body: "> more applications\n\nReally? I doubt so."
    author: "ale"
---
In <a href="http://commit-digest.org/issues/2006-06-18/">this week's KDE Commit-Digest</a>: work begins on 3D molecule visualisation features for <a href="http://edu.kde.org/kalzium/">Kalzium</a>. More progress in the Kopete "OSCAR (AIM) File Transfer" and "KDevelop C# Parser" <a href="http://developer.kde.org/summerofcode/soc2006.html">Summer Of Code</a> projects. An enhanced version of the custom iconset developed during the 1.4 phase is re-enabled as the default option in <a href="http://amarok.kde.org/">Amarok</a>. Following the brand clarifications of last week, <a href="http://developer.kde.org/summerofcode/okular.html">oKular</a> is now known as okular. Kitten is renamed <a href="http://www.vandenoever.info/software/strigi/">Strigi</a>. Two security issues are <a href="http://dot.kde.org/1150310128/">addressed</a>.

<!--break-->
