---
title: "Nokia Releases KHTML Based Code to S60 Browser"
date:    2006-05-28
authors:
  - "trahn"
slug:    nokia-releases-khtml-based-code-s60-browser
comments:
  - subject: "KHTML?"
    date: 2006-05-28
    body: "So, how close are WebKit and KHTML these days? Is this browser really \"based on\" KHTML, or is it more based on WebKit, a descendant of KHTML? Their press release only mentions Apple, not KDE, sadly.\n\nS60WebKit and WebKit look close, since they share a repository, and a name. Is KHTML getting left behind?"
    author: "AC"
  - subject: "Re: KHTML?"
    date: 2006-05-28
    body: "since Apple and KDE are cooperationg more closely through kthml.org, webkit and KHTML aren't that different anymore, and when KHTML2 and the other enhancements to javascript etc are finished in KDE 4, Apple will start to use those as well."
    author: "superstoned"
  - subject: "Re: KHTML?"
    date: 2006-05-28
    body: "Nope.. WebKit and KHTML are more different that ever. We do however share KJS implementation now.\n\nApple is not going to adapt anything major from KHTML, and KHTML2 is not even likely to be adapted by KHTML ;)\n\nTo clarify the relationship, Apple is also moving away from using the name KHTML in WebCore."
    author: "Carewolf"
  - subject: "Re: KHTML?"
    date: 2006-05-28
    body: "Two completely different answers, can someone clear this out?"
    author: "Peppelorum"
  - subject: "Re: KHTML?"
    date: 2006-05-28
    body: "Could you elaborate Allan ? \n\nI clearly don't understand KDE strategy with KHTML (if we have any strategy). Why is it not possible for KDE4 to have Konqueror use either Webkit, either KHTML ?"
    author: "Charles de Miramon"
  - subject: "Re: KHTML?"
    date: 2006-05-28
    body: "WebKit doesn't provide most of the APIs any more. Nor would it build with any Qt or KDE version.\n"
    author: "SadEagle"
  - subject: "Re: KHTML?"
    date: 2006-05-30
    body: "Of course, WebKit is GUI independent. But how hard would it be to make a KDE frontend?"
    author: "blacksheep"
  - subject: "Re: KHTML?"
    date: 2006-05-31
    body: "Quite hard and for hardly any benefit.\nThe only compelling advantage of WebKit at this point is the editing functionalities. Porting those to KHTML is a much easier and desirable path.\n\nAs for rendering and ecma, it's pretty much a tie. There are a *lot* of bugfixes and some functionalities that have not made it into WebCore at all, since they are not making any backporting effort, whereas we do.\n\nWebKit enhancements have been very limited in the past 6 monthes, as they needed to go cross platform and basically turned their finests into windows programmers (poor guys!).\n"
    author: "germain"
  - subject: "Re: KHTML?"
    date: 2006-05-31
    body: "Gnustep?"
    author: "jimmie"
  - subject: "Re: KHTML?"
    date: 2006-05-28
    body: "Should KDE standardize on Webkit, then?"
    author: "AC"
  - subject: "Re: KHTML?"
    date: 2006-05-28
    body: "If I understand correctly the situation. What would be needed, would be to split KHTML between a glue interface between the HTML engine and Qt and the HTML engine. Then we could choose either Webkit or the revamped KHTML or KHMTL2 for the engine.\n\n "
    author: "Charles de Miramon"
  - subject: "Re: KHTML?"
    date: 2006-05-28
    body: "Are there any plans to do that? Does anybody know what the KHTML roadmap is?"
    author: "AC"
  - subject: "Re: KHTML?"
    date: 2006-05-29
    body: "They do mention KDE, just not directly on that article. But if you navigate to http://www.s60.com/business/productinfo/applicationsandtechnologies/webrowser, as they suggest in the end, KDE and Konqueror are mentioned on the first Paragraph."
    author: "Fede"
  - subject: "Nokia Releases KHTML Based Code to S60 Browser"
    date: 2006-05-28
    body: "The title makes it sound like they're doing something incredibly generous . Aren't they forced to do so given the LPGL license of KHTML and so Webkit? and by the way, how can Nokia relicense something LGPL under BSD? it's a little confusing there\n\n"
    author: "Patcito"
  - subject: "Re: Nokia Releases KHTML Based Code to S60 Browser"
    date: 2006-05-28
    body: "They are forced to release their version of Webkit (which is, of course, their version of KHTML as well).\n\nThey are not forced to release the actual browser. For example, Apple is not releasing Safari as open source."
    author: "Roberto Alsina"
  - subject: "Re: Nokia Releases KHTML Based Code to S60 Browser"
    date: 2006-05-28
    body: "this is a non-news, please move along\n\nhttp://opensource.nokia.com/projects/S60browser/architecture-0506.jpg\n"
    author: "Anonymous Coward"
  - subject: "Re: Nokia Releases KHTML Based Code to S60 Browser"
    date: 2006-05-28
    body: "it looks like the nokia browser is not floss,  but they do release a floss api and a floss implementation of the browser that is ReferenceUI Reindeer. is that it?"
    author: "Patcito"
  - subject: "KHTML should have been licensed GPL"
    date: 2006-05-28
    body: "It seems that KHTML had nothing to win by being licensed under the Lesser GPL (LGPL). In other words, KHTML and KDE developers got the short end of the stick, while big business opportunists like Apple took advantage of KHTML without lifting a finger to help it and give back. Apple did the _very least_ they could get away with under the LGPL, which was obviously not enough for KDE developers to improve KHTML for the benefit of FOSS users:\n\nwww.kdedevelopers.org/node/1001\n\nSo why continue to SUCK UP to DRM-loving big business by releasing KHTML and KDE-libs code under the LGPL, when you can fight for Freedom and for the users' rights by licensing _all_ your code under the GPL, the license with the strongest copyleft protections?"
    author: "AC"
  - subject: "Re: KHTML should have been licensed GPL"
    date: 2006-05-29
    body: "Nonsense. Nokia have done what the LGPL asks them to do. What else do you want? Do you want them to come round to your house and cook your dinner for you too? If that's what the LGPL was meant to achieve, then it would say so.\nKDE has benefited a lot from Webkit. Stop being an FSF troll!"
    author: "AC"
  - subject: "Re: KHTML should have been licensed GPL"
    date: 2006-05-29
    body: "You are a troll, too.\nWho do you think created the LGPL? \nStallman's FSF, of course."
    author: "Cobarde an\u00f3nimo"
  - subject: "Re: KHTML should have been licensed GPL"
    date: 2006-05-29
    body: "Agreed 100%. Apple are worse than Microsoft; instead of just competing with open source, they rip us off.\n\nThe LGPL might be arguably too tame, but at least KDE didn't make the mistake of licensing very important technologies under an artistic license (alarmingly much of KDE is under these licenses)."
    author: "Alistair John Strachan"
---
Nokia has <a href="http://www.nokia.com/A4136001?newsid=1052589">announced the release</a> of the code to the web browser for their S60 phones.  The browser is based on <a href="http://www.webkit.org">WebKit</a> and <a href="http://www.khtml.info">KHTML</a>.  At <a href="http://dot.kde.org/1129483687/">Akademy last year</a> the developers told us how they plan to be active Open Source contributors.  It will be used on ESeries, NSeries devices and 3250 phones soon.



<!--break-->
