---
title: "SQO-OSS Project Launched with KDE"
date:    2006-10-26
authors:
  - "agroot"
slug:    sqo-oss-project-launched-kde
comments:
  - subject: "seriously good news!"
    date: 2006-10-27
    body: "I really like this project, and I'm looking forward to use the tools they'll make for improving the overall quality. Thanks!!"
    author: "epo"
---
<a href="http://www.kde.org/">KDE</a> together with the <a href="http://www.aueb.gr/">Athens University of Economics and Business</a> and the <a href="http://www.auth.gr/home/index_en.html">Aristotle University of Thessaloniki</a> and other partners has launched <a href="http://www.sqo-oss.eu/">SQO-OSS</a>. This is a two-year multi-million euro project that aims to develop new tools and techniques for measuring Open Source quality.




<!--break-->
<p>SQO-OSS aims to assist European software developers in improving the quality of their code, and to remove one of the key barriers to entry for Open Source software by providing scientific proof of its quality to businesses. This two-fold approach which benefits both developers and users is complemented by a broad definition of the notion of quality. Quality encompasses not only the source code of the project (KDE's SVN repository is one of the largest in the world) but also the project's "soft" data like the community around it. By analysing both the code and the communication (like bug reports and mailing list discussions) that leads to changes in the code we can get a better picture of the quality of KDE's software and its processes.</p>

<p>For developers the results based on code analysis are immediately useful. Within KDE we have experience with the  <a href="http://www.englishbreakfastnetwork.org">EBN</a> and the fixes that come out of there (some more useful than others, granted). Similar checks will be done within the SQO-OSS framework, although the scope for analytical tools including static analysis is much greater. Having a clear indication of what is wrong in the code and what could be better is a great way to steer effort, for those developers that want a little steering or would like to apply their efforts towards immediately visible targets.</p>

<p>On the process side of things, the analysis of mailing lists and bug reports and the correlation of those with code changes will help the KDE project understand its development processes and identify where the processes can be improved.</p>

<p>The applicability of SQO-OSS for businesses becomes clear when the quality measurements for projects are aggregated and summarised. Research into novel and correct ways of analysing quality metrics into an overall quality report drives half of the SQO-OSS project. With these overall quality reports, businesses can compare and contrast different Open Source projects in order to obtain a better idea of which projects are usable for them.</p>

<p>Chief researchers for the KDE project within SQO-OSS are Sebastian K&uuml;gler and Adriaan de Groot. The KDE take on SQO-OSS progress can be found on <a href="http://people.fruitsalad.org/adridg/bobulate/index.php?/categories/8-SQO-OSS">Adriaan's SQO-OSS blog</a> and <a href="http://news.zdnet.co.uk/software/linuxunix/0,39020390,39284268,00.htm">ZDNet also has a report</a>.</p>




