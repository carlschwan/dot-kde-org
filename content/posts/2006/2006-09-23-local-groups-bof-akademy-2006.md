---
title: "Local Groups BoF at aKademy 2006"
date:    2006-09-23
authors:
  - "gventuri"
slug:    local-groups-bof-akademy-2006
---
This year at aKademy 2006 there will be a BoF section to discuss <a href="http://kde.org/international/">KDE local groups</a>. The issue will be how to spread KDE in your own country, helping each other and finding more to help in future.  The starting point of the BoF will be the <a href="http://www.kde-it.org/">KDE Italia</a> experience and then the experiences of all the groups will be put together to make sure we spread KDE all around the world.  We will look at the available resources including the use of KDE regional websites. See the <a href="http://wiki.kde.org/tiki-index.php?page=regional+groups+%40+aKademy+2006">local BoF wiki page</a> for more information.









<!--break-->
