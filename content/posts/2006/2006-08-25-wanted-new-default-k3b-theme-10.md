---
title: "Wanted: New Default K3b Theme for 1.0"
date:    2006-08-25
authors:
  - "strueg"
slug:    wanted-new-default-k3b-theme-10
comments:
  - subject: "Color themes? (little off-topic)"
    date: 2006-08-25
    body: "Great!\n\nThough I wonder why it is not possible to match the current color scheme with these pixmap themes. Theme designers may create a pixmap skin which fits a specific color theme, mostly KDE default, or their favourite KStyle. Changing the KDE color settings may yield a distracting result. \n\nThere are quasi-pixmap style engines like Baghira/Keramik which blend their pixmaps with the individual color settings. Why not adopting this colorizing algorithm for K3B themes or themes in general (like Amarok themes, Konqueror's context bar, help system stylesheets). Currently, we have a mixture of themes being totally different and mutually visually excluding. \n\nBeing consequent, KDE4 should rethink its theming concept. On the one hand we may adjust the shape of widgets (KStyle) and the corresponding colors individually, on the other hand pixmaps are used which fit only one specific color scheme. In this regard, Gnome seems to be more consistent where the color settings are defined by the used theme, such that everything fits together. Which way to go?"
    author: "sebastian"
  - subject: "Re: Color themes? (little off-topic)"
    date: 2006-08-25
    body: "I stumbled yesterday on this 3-years old article about magically changing the colours of KDE icons. I think this should be implemented in an automatical way, so one only stores the basic icon theme, and the new hue (the new ones can be written on disk too of course).\n\nhttp://scanline.ca/hue/\n\nAlso, using indexed colours would help to have icon themes magically follow the colour scheme."
    author: "oliv"
  - subject: "Re: Color themes? (little off-topic)"
    date: 2006-08-25
    body: "If you run under the assumption that icons and whatever other visual images on the desktop will continue to trend toward SVG, then with QT4 and Zack's work, the sky's the limit.\n\nSVG animations and CSS-color assignments.  If you want icons to follow a color scheme or have effects (brightness, animation, whatever) upon a mouse hover, it's all possible.  If you want you mount point/hard drive icons to show a current little capacity pie chart on it, or a decoration/notifier on icons that are already running, or an integer on your IM icon for how many session you have open,  or, or.....what an icon is and what an applet is will be blurred.\n\nPossible before?  Yes, but harder.\n\nOf course:\n\n1) It all depends on HCI/usability and people actually doing the work.  Auto-color changes might help desktop consistency, but it can also impact accessibility aspects if not watched.  And it dilutes icon/image recognition and \nmakes people lean more on the pattern for recognition.  Would someone your computer spot a FF/Konqueror/whatever icon as fast if it was purple?  Marketing-wise, we can also talk about brand equity being lowered.  But what the hell do marketing people know.\n\n2) This all has little to do with a request to pretty-up with cool application.\n\n"
    author: "Wade Olson"
  - subject: "Re: Color themes? (little off-topic)"
    date: 2006-08-25
    body: "\"HCI/usability and people actually doing the work\"\n\nWe have already discussed this in the HCI workgroup, taking accessibility, usability and artwork into account. What we urgently need at the moment are developers implementing it - and later the willingness of developers and artists to listen to our advise.\n\nOlaf\n"
    author: "Olaf Jan Schmidt"
  - subject: "Re: Color themes? (little off-topic)"
    date: 2006-08-26
    body: "> But what the hell do marketing people know.\n\n*g*"
    author: "Jakob Petsovits"
  - subject: "Re: Color themes? (little off-topic)"
    date: 2006-08-25
    body: "Similar algorithms for changing the hue, saturation or brightness of a colour are described here:\n\nhttp://accessibility.kde.org/hsl-adjusted.php\n\nOlaf\n"
    author: "Olaf Jan Schmidt"
  - subject: "Re: Color themes? (little off-topic)"
    date: 2006-08-25
    body: "I compared both pages. I do not want to sound too hard with you, but I think the difference is that the page I linked to is written by someone who is a professional of colour, so it is accurate. He uses CIELAB space to perform the rotation. Your formulas are probably much faster, but they are less accurate (for example, you use a gamma of 2.2 for sRGB space, when sRGB cannot be described by a simple gamma curve, and the part that can be has a gamma of 2.4). And accuracy is quite important with colours.\n\nYou also rely on the sRGB standard, which fortunately, is already phasing out of the HDTV world (replaced by the xvYCC space with a much wider colour gamut). Most new devices will use better light sources (LED, lasers, new CCFL or HCFL) which will make the sRGB space too limited. It has already started last year with LED LCD TVs (Sony, Samsung, etc.)."
    author: "oliv"
  - subject: "Re: Color themes? (little off-topic)"
    date: 2006-08-25
    body: "The two pages have totally different aims. The page linked by you does not address how to compute the luminosity contrast or other topics important for accessibility.\n\nThe sRGB standard defines a gamma value of 2.2, but it also says that the real gamma value depends on the brightness of the room where the monitor is placed. This implies that total accuracy is impossible when dealing with monitors or HDTV devices, unless we know the exact luminosity characteristics of the room (i.e. all lamps, the colour of walls, the time of the day, etc).\n\nAn additional problem is that some CIELAB colours cannot be expressed in sRBG. This means that a conversion to CIELAB and back to sRGB doesn't make sense if hue accuracy is your sole aim, as the page linked by you explains.\n\nCIELAB and derived colour spaces can be used in professional image editing applications, but they are never used for the user interface. All colours used in the KDE user interface are expressed as sRGB (colour schemes, CSS, SVG images). sRGB is also used in 99% of all raster images, and none of the image editing applications on Unix/Linux bar one (Krita) can handle anything different from sRGB internally."
    author: "Olaf Jan Schmidt"
  - subject: "Re: Color themes? (little off-topic)"
    date: 2006-08-28
    body: "Olaf,\n\nUnless you allow in your sRGB algorithms the use of values inferior to 0 and superior to 1 (or 255, depending how you normalise), then they will stop working as soon as the new displays (such as the ones introduced 1.5 years ago for HDTV) will be available. If you use CIELAB (or allow the negative RGB signal in your algorithms), then whatever the display technology, your formulas will work. DO you really wish to rewrite your library in 2 years from now ?\n\nAllowing the negative RGB values (and the ones superior to 1) allows to acess all of the colours. See the xvYCC standard."
    author: "oliv"
  - subject: "Re: Color themes? (little off-topic)"
    date: 2006-08-25
    body: "It's pretty much impossible to do well adjusting colors in general.\n"
    author: "SadEagle"
  - subject: "Re: Color themes? (little off-topic)"
    date: 2006-08-25
    body: "\"Though I wonder why it is not possible to match the current color scheme with these pixmap themes.\"\n\nThis is indeed very important, for example for partially sighted users who can only read light text on a dark background.\n\nThe plan for KDE4 is to have image colourisation algorithms in the KDE libraries, as well as a global option for switching off all background images:\nhttp://www.amen-online.de/~olafschmidt/colors/\n\nOlaf Schmidt\n\nKDE Accessibility team"
    author: "Olaf Jan Schmidt"
  - subject: "Re: Color themes? (little off-topic)"
    date: 2006-08-26
    body: "Actually the current svn trunk supports transparent pixmaps and defaults to the current KDE color scheme. This way one can create K3b themes that fit the KDE color scheme."
    author: "Sebastian Tr\u00fcg"
  - subject: "Good Looks aren't everything"
    date: 2006-08-25
    body: "Just as long as they replace that default reveille (or whatever that bugle riff is) sound for completed burns...I *hate* that."
    author: "Rube"
  - subject: "Re: Good Looks aren't everything"
    date: 2006-08-25
    body: "Why do people always bug devs about their preferred DEFAULTS? You want to personalize sounds - Control Center > Sound > Sound and Notofocations\n\nPersonalizing KDE is SO SIMPLE, leave the defaults alone."
    author: "Daniel \"Suslik\" D."
  - subject: "Re: Good Looks aren't everything"
    date: 2006-08-25
    body: "> Personalizing KDE is SO SIMPLE, leave the defaults alone.\n\nThe problem is, that this is also time expensive, so the defaults should fit for as many people as possible. Sometimes people do not even find the place to perform the changes. KDE is not shining in this regard. Too many places where you can change options. Not to speak about missed kconf_update files you stumble about in the one or the other case, later; So it's actually beneficial, when developers do not chose to set their preferred options as defaults, so they stumble about such issues."
    author: "Carlo"
  - subject: "Re: Good Looks aren't everything"
    date: 2006-08-25
    body: "The problem really is: Why does everyone assume that just because he dislikes a particular default setting the majority does?"
    author: "Martin"
  - subject: "Re: Good Looks aren't everything"
    date: 2006-08-25
    body: "Agreed. I happen to love the sound of a finished burn. Hurray for trumpets!"
    author: "Paul van Erk"
  - subject: "Re: Good Looks aren't everything"
    date: 2006-08-26
    body: "No. While your point seems to be a good one, it's just that the other way around the point is as good. Most dev's are not usability experts, nor do they usabilty tests with a decent number of testers - which is of course asked (maybe too) much for FOSS projects, without commercial backing.\n\nMaybe I should add that I do not argue about this particular sound - having sound notifications disabled, I couldn't care less - but usablity issues are a general problem of the Linux desktop and especially KDE 3.x."
    author: "Carlo"
  - subject: "Re: Good Looks aren't everything"
    date: 2006-08-25
    body: "Actually, I don't consider this bugging the devs.  It was a light-hearted comment to begin with, posted on a call at the Dot for input on K3b's default theme. \"Leaving the defaults alone\" is pretty much the opposite of what they're trying to do here, isn't it?"
    author: "Rube"
  - subject: "Re: Good Looks aren't everything"
    date: 2006-08-25
    body: "Perhaps a more taste neutral default would be desireable. My preferred default is no sound. If I were to leave the disc burning and be concerned about not missing the end of the burn then an option to enable a sound on completion that is visible during burning would be great. Just a checkbox for play a sound on completion that saves between burns and file picker for the sound with preview."
    author: "Matt"
  - subject: "Re: Good Looks aren't everything"
    date: 2006-08-26
    body: "I like the bugle sound. I even re-used it for a contact in kopete.\n\nstill, knotify config stuff could use some work. or, well, all of kde's config stuff. it can be kinda.. daunting."
    author: "Chani"
  - subject: "Colorizable theme"
    date: 2006-08-25
    body: "I agree with sebastian about the colorizable theme feature. It would allow to users to adapt the theme to their personal environement whatever color it is.\nHowever it wouldn't be that easy for k3b developpers to include that feature to the product for only the background should be colorized (take the current theme and colorize it with the gimp, you'll see how horrible it'll look like).\nSince it would have the developpers to rewrite the whole skinning source code, I don't think that feature will be include in the 1.0 release."
    author: "Francois Chazal"
  - subject: "Glitter"
    date: 2006-08-26
    body: "A general problem with our usability and visual standardisation issue is that applications like K3B look like other applications. Just imagine everything is themed the same.\n\nIt is important to keep contrast. Wow! applications need Wow! theming, with the default as neutral, invisible as possible.\n\n"
    author: "furangu"
  - subject: "Re: Glitter"
    date: 2006-08-26
    body: "A general problem with our usability and visual standardisation issue is that applications like K3B look like other applications\n\nI would call this a usability feature actually...\nI love the consistency that kde provides."
    author: "Mark Hannessen"
  - subject: "Re: Glitter"
    date: 2006-08-29
    body: "Lovely. A desktop with all applications constantly trying to out-glitz each other. Sounds like the recipe for a consistant desktop."
    author: "Robert"
  - subject: "Re: Glitter"
    date: 2006-09-02
    body: "MSN IM, EI7, WMP, and MS Office all have their ownwidget themes, and all other built in Windows apps are split between XP style and Classic... It's been tried, and the result is a confusing mess of styles.\n\nInterface consistency, IMHO is an important feature that sets KDE apart from that sort of system.\n\nNo one is ever going to mistake K3B for Konqueror or Kopete, because obviously it has different buttons because it performs different functions, so why make things inconsistent?"
    author: "Ben Morris"
---
K3b 1.0 is in sight and with it I would like to introduce a new default theme. Not because I don't like the current ones, but I just want K3b 1.0 to look different so that people will see the difference, not only feel it.  If you are interested in creating a theme for K3b see <a href="http://www.k3b.org/themeshowto">the themes howto</a> which describes the pictures that are needed in a complete K3b theme set.  Send your entries to <a href="https://lists.sourceforge.net/lists/listinfo/k3b-devel">k3b-devel</a>.  In other artwork news one of our best community sites <a href="http://www.kde-look.org/news/index.php?id=239">kde-look.org passed its fifth birthday</a>.



<!--break-->
