---
title: "Immanuel: KDE Introduces New Server"
date:    2006-08-22
authors:
  - "wmolkentin"
slug:    immanuel-kde-introduces-new-server
comments:
  - subject: "Permanent Donate button on the KDE home page"
    date: 2006-08-22
    body: "How about putting a permanent Donate button on the KDE home page (KDE.org)? A lot of people visit only the home page to find out what's new, and I'm sure some will be willing to donate especially if they are please with a particular release or application.\n\nAnother idea is to have a \"Friends of KDE\" page, similar to what gnome has. Perhaps put a button on the KDE's home page saying \"Friends of KDE: Donate now!\".\n\nhttp://www.gnome.org/friends/"
    author: "AC"
  - subject: "Re: Permanent Donate button on the KDE home page"
    date: 2006-08-22
    body: "> How about putting a permanent Donate button\n\nHave a look at http://download.kde.org/"
    author: "Anonymous"
  - subject: "Re: Permanent Donate button on the KDE home page"
    date: 2006-08-22
    body: "Not many users get KDE from KDE download page but through their favorite distribution. Having it on main page would be better."
    author: "Lure"
  - subject: "Re: Permanent Donate button on the KDE home page"
    date: 2006-08-22
    body: "And how many users having KDE coming with their distribution visit the KDE home page?"
    author: "Anonymous"
  - subject: "Re: Permanent Donate button on the KDE home page"
    date: 2006-08-22
    body: "How many users visit the KDE homepage at all?  I love and support KDE, and it hurts me to say this, but the KDE Homepage is terrible.  That's just my opinion, of course.  I go straight to the DOT and Planet to get news.  The home page seems to have been designed to bombard the visitor with information.  I personally think it should have a more 'marketing' feel to it.  Introduce visitors with some sexy, must-have stuff, and let them navigate to details on their own.  Some not-so-plain graphics and design would be nice.  I have no web design talent at all, but I know a well designed page when I see one."
    author: "Louis"
  - subject: "Re: Permanent Donate button on the KDE home page"
    date: 2006-08-22
    body: "There's a web-team (available via kde-www@) who *is* working on it. Design is just one part; getting to organize the content of the KDE site is a far bigget matter. What we really need is people who have the perseverance to sit through such an entire redesign."
    author: "Adriaan de Groot"
  - subject: "Re: Permanent Donate button on the KDE home page"
    date: 2006-08-22
    body: "I understand.  I know that it is a huge job.  It just brings me down a bit that KDE and the KDE apps don't have sweet websites that show off their great features.  I know this forum isn't the place for this discussion, but I've been thinking about it for a while, and the previous comments finally brought me to putting it down.\n\nCongrats on the new server hardware.  Has anyone considered some type of automatically recurring donation program?  I do this for other org's."
    author: "Louis"
  - subject: "Re: Permanent Donate button on the KDE home page"
    date: 2006-08-23
    body: "www.spreadkde.org"
    author: "hill"
  - subject: "Re: Permanent Donate button on the KDE home page"
    date: 2006-08-22
    body: "Well, i could imagine many people want to donate money, if it helps the project in a more direct way (like hardware instead of general donating). What about a monthly donation membership of which developers can be paid?\n\n"
    author: "mi"
  - subject: "Re: Permanent Donate button on the KDE home page"
    date: 2006-08-24
    body: "KDE user groups so to speak. \n\nOr a KDE industry association."
    author: "hill"
  - subject: "pron on the dot?"
    date: 2006-08-22
    body: "The server she has no clothes!!"
    author: "ac"
  - subject: "Halo effect on donations"
    date: 2006-08-22
    body: "Blogger.com wanted to sabotage my blog and at the same time butcher planetkde.org, but I wanted to mention a blog entry from a couple of days ago:\n\nhttp://wadejolson.blogspot.com/2006/08/fundraiser-halo-effect.html\n\nThe donations have just kept rolling in."
    author: "Wade Olson"
  - subject: "250GB RAID array"
    date: 2006-08-22
    body: "How do you fit an array in that little case?"
    author: "AC"
  - subject: "Re: 250GB RAID array"
    date: 2006-08-22
    body: "It could be an external array (SCSI and SATA disks make that possible)."
    author: "Gleb Litvjak"
  - subject: "Re: 250GB RAID array"
    date: 2006-08-22
    body: "I guess 2*250GB SATA as RAID1. BTW: Some 1U cases has 4 drive bays..."
    author: "dm"
  - subject: "Re: 250GB RAID array"
    date: 2006-08-24
    body: "A Dell PowerEdge 1750 has 3 hot-swap SCSI bays that can accept low-profile UltraSCSI drives.\n\nI've configured several for RAID-5 and while it is noisy, it works nicely."
    author: "chill"
  - subject: "Awesome"
    date: 2006-08-22
    body: "This is great news, we've been looking forward to a more responsive bugs.kde.org for a long time. Thanks to everyone who donated :)\n"
    author: "Mark Kretschmann"
  - subject: "It's us that's saying thank you!"
    date: 2006-08-22
    body: "It's is very nice to see that our donations are being put to good use! The donations is a small price to pay for all very cool desktop software you guys are producing :)\n\nThanks a lot! I hope to be able to help out someday too."
    author: "Joergen Ramskov"
  - subject: "Installation on the floor"
    date: 2006-08-22
    body: "Hmmm, looks like the box is sitting on the floor with carpet?!?\n\nSurely, if you're carefull enough not to touch any component (MB, CPU, RAM, etc.) that's OK, however ESD is a bitch...\n\nAll the best!\nTom"
    author: "Tom"
  - subject: "Re: Installation on the floor"
    date: 2006-08-23
    body: "You can rest assured our administrators only choose special carpet, designed for server installation. :) \n\n\n"
    author: "Rainer Endres"
  - subject: "Dell upstages us"
    date: 2006-08-22
    body: "We donated 7000 EUR and Dell disposes an outdated machine. With such many money it is posible to buy a much faster computer...\n\nI hope the money will be used for something usefull."
    author: "a donator"
  - subject: "Re: Dell upstages us"
    date: 2006-08-22
    body: "I mostly donated my work but I think fast server for KDE wiki could be highly usable (as well as moving e.g. to nicely cached media wiki engine).\n\nThat said, it's no problem to use a load-balanced system in the future if needed, so the money are not wasted even if not everything is spent right now.\n"
    author: "Jaroslaw Staniek"
  - subject: "Re: Dell upstages us"
    date: 2006-08-23
    body: "youre probably right, but i think it went the following:\n\nWe need a new bugtracker, lets ask the community and manufactures (dell,hp..)\n\nDell responded: here we donate this machine.... (4gbb ram, 2*3ghz)....\n\nok now we have 7000 euro what do we do... ??? ok 250gb raid array (scsi) 700 euro, so we have left ~6300 euro for something else.\n\nNOW, what i want to know, where is the rest money spent?"
    author: "ch"
  - subject: "Re: Dell upstages us"
    date: 2006-08-23
    body: "i want to add: the dell machine is not really bad!\nits rocking !!! \n\nThanks DELL, i will purchase a lcd (24'' or 30'') from you in the near feature.\n\nBut its not the *besT* machine , like www.kernel.org has it from HP."
    author: "ch"
  - subject: "Re: Dell upstages us"
    date: 2006-08-23
    body: "> NOW, what i want to know, where is the rest money spent?\n\nMore hardware. Most of the KDE server infrastructure has been around unchanged for many, many years (never touch a running system, after all), and a number of them face performance problems at this point. The Bugzilla server was certainly the most overloaded, but we can project fairly well when the other boxes are going to become critical, and will invest in new gear accordingly. Which is possible thanks to your donations."
    author: "Eike Hein"
  - subject: "ktown - remember?"
    date: 2006-08-23
    body: "http://dot.kde.org/1029669199/\nfrom Sunday 18/Aug/2002 :-)\n\nThat's when the old ktown machine went online...\n"
    author: "Thomas"
  - subject: "Totally off-topic but..."
    date: 2006-08-23
    body: "Can anyone comment on the status of the www.kde.org redesign?  The existing site is an eyesore IMHO (no offense to the original designers) with its \"designed by committee look\"."
    author: "ac"
  - subject: "Re: Totally off-topic but..."
    date: 2006-08-23
    body: "Working on it, but due to lack of manpower, it may take some time. Wanna help? :-)"
    author: "Sebastian K\u00fcgler"
  - subject: "Re: Totally off-topic but..."
    date: 2006-08-23
    body: "Unfortunately I lack the skills for web design.  I wish I had something to offer more then the few bucks I donated.  :(\n\nBest of luck."
    author: "ac"
  - subject: "Re: Totally off-topic but..."
    date: 2006-08-23
    body: "... help?  \n\nIndeed I do!  Where should I/we be looking for more info?\n\nThanks,\n\nPet"
    author: "Pet Tau"
  - subject: "Re: Totally off-topic but..."
    date: 2006-08-24
    body: "I've replied in private email."
    author: "Sebastian K\u00fcgler"
  - subject: "Don't forget the admins :)"
    date: 2006-08-23
    body: "Thanks to the donators! And thanks to those who are spending their time and efforts to set the new machine finally up! Any names we could put on our banners? ;)"
    author: "Anonymous"
  - subject: "Yay, IPv6"
    date: 2006-08-23
    body: "Finally an open-source project server with decent IPv6 connectivity\n\n$ host immanuel.kde.org\nimmanuel.kde.org has address 138.246.255.177\nimmanuel.kde.org has IPv6 address 2001:4ca0:100::10:177\n\nYay! :-)\n"
    author: "Anonymous"
  - subject: "Names Listed?"
    date: 2006-08-24
    body: "Nice to see al those names listed on the server, but it seems incomplete?  Is this only the donations you used for this server?  Will the other donors get recognised on the other hardware?\n\nAnother donor :-)"
    author: "Anonymouse"
  - subject: "New bugzilla"
    date: 2006-08-24
    body: "Will the new bugzilla be a 2.0 web thing?\n\nTo be in last and great new side of tech, people should place his locaton in google map, put screenshots from flickr, get new app bugs via RSS and tag everything. Don't forget the round corners.\n\nPD: Ironic\nPD2: the RSS and tag ideas are not bad at all :)"
    author: "mcosta"
  - subject: "Dell and positive PR"
    date: 2006-08-25
    body: "Nice that Dell gave a server. Thank you for\nthat, it really came to need.\n\nConsidering how big company Dell is and especially\na computer manufacturer, I would have thought that if\nyou really want to improve your PR with OSS community,\nyou would have addressed those issues what really bother\nus all.\n\nIn my work, I constantly see and hear how much \nproblems there are with Linux and Dell. And I \nthink that those problems would already have been \nsolved if there would have been intrest to do so.\n\nHow about dedicating few engineers to look at those\nproblems and managers to communicate found issues\nwith chip manufacturers?\n\nBy supporting the Linux projects you acknowledge that\nLinux exists, but for me now it looks that you're just\ntaking an advantage from it, not taking part to the \ncommunity.\n\n\nOwner of two 2045FPW\n\n\n(I know that you've different departments and opinions\nin those may vary, but in customer point of view, how \nthe company behaves outside is the only thing what matters.)"
    author: "Tuju"
---
With a donation of a new Poweredge server from <a href="http://www.dell.com">Dell Inc.</a>, and a successful <a href="http://dot.kde.org/1153321332/">hardware fundraiser</a> for disks last month, the long-suffering ktown.kde.org site gets a welcome upgrade, and a new hosting package from the <a href="http://www.lrz-muenchen.de/">Leibniz-Rechenzentrum</a>. Read on for the details on the new server, <a href="http://immanuel.kde.org">Immanuel</a>.



<!--break-->
<p>
For several years, ktown.kde.org has carried the burden of hosting the KDE bug tracker. Furthermore, it is the FTP master that synchronizes new releases and hosts developer content.  With the growth of the KDE project and surrounding community, it isn't surprising that over time, this server has become increasingly overloaded.
</p>
<h2>The server</h2>
<p>
Since this server plays such an important function for KDE, we had to make sure that a new server would be powerful enough for current loads and would anticipate future growth.  Thanks to a generous donation from Dell, the KDE project can now move the bug tracker to an new host. The new server is a <a href="http://www.dell.com/content/products/productdetails.aspx/pedge_1750?c=us&cs=28&l=en&s=dfb
">Dell Poweredge 1750</a> with 4GB RAM. It is powered by two 3GHz Xeon processors with hyperthreading. This chassis will handle its tasks more responsively than the previous Pentium 3 server with 1.5 GB of RAM.

<div style="text-align: center; float: right; border: thin solid grey; margin: 1ex; padding: 1ex">
<a href="http://static.kdenews.org/danimo/immanuel.jpg">
<img src="http://static.kdenews.org/danimo/immanuel_small.jpg" width="300" heigth="225" /><br /></a>
Immanuel during its setup.
</div>

<h2  clear="all">The hosting</h2>
<p>
With the new Dell Poweredge server, the next step for an improved bug tracker was the hosting, and associated bandwidth.  Enter the <a href="http://www.lrz-muenchen.de/">Leibniz Computing Centre</a> (Leibniz-Rechenzentrum) in Munich.  The LRZ has kindly offered to host our new server, giving us secure, stable and high performance hosting.
</p>

<h2  clear="all">The community</h2>
<p>Finally, to make sure that the new server has the storage capacity to meet KDE's needs for the foreseeable future, we looked to upgrade the hard disks.  On July 19, a Dot story <a href="http://www.lrz-muenchen.de/">announced a hardware fundraiser</a>.
</p><p>
The response from the community was immediate and overwhelming.  What we intended as a short fundraiser turned into an <a href="http://www.kde.org/support/donations.php">flood of donations</a> that continues weeks later.  To date, 7,000 Euro have been donated by both individuals and businesses.  Parts of the fundraiser have been used to purchase and install a new 250GB RAID array in the Poweredge server, the rest will be spent on further hardware purchases.
</p><p>
What can you say about our users and community?  Sincerely, we are amazed by your generosity.
</p>
<h2>The results</h2>
<p>
So without any further ado, please welcome <a href="http://immanuel.kde.org/">Immanuel</a>.  Immanuel has already become KDE's new FTP master site, an upgrade that will make the roll-out of new releases faster than ever.  Further, work has begun on porting over the current KDE bug tracker.  A new version of Bugzilla will be installed, and our web team is preparing an up-to-date user interface to accompany it.
</p><p>
As KDE continues to flourish and expand, our ability to release timely bug-free software is critical.  The previous bug tracking and ftp solution was showing the strain of supporting a popular Open Source Desktop, and many responded.  The KDE team would like to thank Dell Inc. for the hardware donation and the Leibniz Computing Centre for the hosting opportunity.  And finally, a massive Thank-you goes to the community for the donations.
</p><p>
KDE has now secured a server solution that will result in tangible benefits for all of our users.  The KDE infrastructure team is really happy that when KDE asks for help, the response from businesses and individuals is deafening.  We at KDE are grateful and hope to repay your support with a continuing stream of quality Open Source desktop software.
</p>


