---
title: "KDE Commit-Digest for 29th October 2006"
date:    2006-10-30
authors:
  - "dallen"
slug:    kde-commit-digest-29th-october-2006
comments:
  - subject: "Status of khtml"
    date: 2006-10-30
    body: "Is there anyone that can comment about the removal of the unity engine? Does this mean that the kde khml codebase has been resynced with webkit?"
    author: "AC"
  - subject: "Re: Status of khtml"
    date: 2006-10-30
    body: "No. It means that people who like the idea of Unity are continuing to work on it in Apple's repository, not KDE's.\n"
    author: "SadEagle"
  - subject: "Re: Status of khtml"
    date: 2006-10-30
    body: "Which is another way of saying there is no unity (with small 'u') ;)"
    author: "Carewolf"
  - subject: "Re: Status of khtml"
    date: 2006-10-30
    body: "So how will it effect our beloved KHTML and Konqueror? Does it mean that the KHTML team splits into KHTML and Unity + Webkit? I would really cry if we lost KHTML and Konqueror, especially for KDE 4 which IMO really needs a decent browser..."
    author: "fred"
  - subject: "Re: Status of khtml"
    date: 2006-10-30
    body: "Thus far, this project doesn't affect KHTML at all, since it doesn't resolve the issues relevant to KHTML development. Basically, the Unity answer is \"throw out 3+ years for work, and do unpaid work for Apple under their rules\".\nOne or both parts of this have to be changed for this stuff to be anything more than a new target of a fork. May be they'll be changed, may be they won't, but don't consider anything definite.\n\n\n"
    author: "SadEagle"
  - subject: "Re: Status of khtml"
    date: 2006-10-30
    body: "Ok, after reading your comments at least I know that the status is still uncertain now. I wish all the best for the developers and thanks for all your great work on KHTML and Konqueror!"
    author: "fred"
  - subject: "Re: Status of khtml"
    date: 2006-10-30
    body: "May I ask out of what position you make this statement. Are you a KHTML dev yourself (or close to that group) so you have inside knowledge of this, or are you just conjecturing? I find your comment interesting, but it would be good to know your position when interpretting it."
    author: "Andre Somers"
  - subject: "Re: Status of khtml"
    date: 2006-10-30
    body: "SadEagle is a well known KDE/KHTML hacker, if you don't know him you don't read SVN commits ;)"
    author: "cartman"
  - subject: "Re: Status of khtml"
    date: 2006-10-30
    body: "That's right, I don't. That's why I asked here. :-) Thanks for clarifying!"
    author: "Andre Somers"
  - subject: "Re: Status of khtml"
    date: 2006-10-30
    body: "Interesting this is...\n\n> do unpaid work for Apple under their rules\n\nI thought the appeal of unity is the other way round. KDE profits from further developments of the Safari-team inside Apple. It's supposed to be a win-win situation, as Apple does pay some developers to work on an OSS project...\n\nI think the biggest argument speaking against unity is that khtml is elegant and does little compromises in terms of code cleanness and quality, whereas apples fork of khtml got some unattractive modifications (probably in quick'n'dirty style). It's the way software has to be done in the big business, but just not the usual style of an OSS project and especially not of KDE..."
    author: "Thomas"
  - subject: "Re: Status of khtml"
    date: 2006-10-30
    body: "That argument is a little dead now. Actually Apple are doing a lot to clean up the code and there are many ugly parts in KDE KHTML as well.\n\nThe problem is more that WebKit is still predominately developed for Safari, which means we either are forced to fork or follow Apple releases. Secondly that all patches has to be beneficial to Apple, because they can approve and disapprove patches, but not the other way around. \n\nSo the problem that needs to be resolved is some way to avoid being forcefed Apple dogfood. "
    author: "Carewolf"
  - subject: "Re: Status of khtml"
    date: 2006-11-01
    body: "> Actually Apple are doing a lot to clean up the code \n\nAs Nietzsche puts it \"Who wears cleaned rags will be cleanly dressed without doubt, but dressed with rags nevertheless\".\n"
    author: "germain"
  - subject: "Re: Status of khtml"
    date: 2006-11-01
    body: "That's pathetically elitistic."
    author: "st. germain"
  - subject: "Re: Status of khtml"
    date: 2006-11-01
    body: "ouch! I forgot about the Nietzsche bot.."
    author: "germain"
  - subject: "Re: Status of khtml"
    date: 2006-11-01
    body: "That's pathetically elitistic."
    author: "st. germain"
  - subject: "Re: Status of khtml"
    date: 2006-11-12
    body: "kool site"
    author: "Tuco"
  - subject: "Re: Status of khtml"
    date: 2006-11-01
    body: "Give yourself a chance..."
    author: "romain"
  - subject: "Re: Status of khtml"
    date: 2006-10-30
    body: "What it could actually mean is that KHTML developers are splitting. That would be a big mistake, IMHO, specially for the ones that remain at the old KHTML project. Even if just a third of KHTML start developing Webkit, there is no way KHTML can compete with the combined might of Webkit and a those new KHTML developers.\nI guess KDE4's Konqueror webbrowser should be able to switch between both engines. But in the long run KHTML would fall behind.\n\nJust my 2c."
    author: "Mikun"
  - subject: "Re: Status of khtml"
    date: 2006-10-30
    body: "I think its only a new home:\nhttp://lists.kde.org/?l=kde-commits&m=116162004313833&w=2\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: Status of khtml"
    date: 2006-10-30
    body: "I would also like to now what \"due to a change in the circumstances that prompted its creation\" means..\n\nThanks,\n\nHenning"
    author: "Henning"
  - subject: "Re: Status of khtml"
    date: 2006-10-30
    body: "From the commit digest:\n\n\"Simon Hausmann committed a change to /branches/work/unity:  \n- this is obsolete, thanks to Nikolas's excellent work of feeding everything\nback into Apple's repository (and more!)\"\n \n \n"
    author: "Victor T."
  - subject: "Re: Status of khtml"
    date: 2006-10-31
    body: "Yesterday bumped against a very good feature the Unity should have brought - \"word-wrap: break-word\" tag support.\n\nIt is outside of CSS/HTML guidelines, but without it, putting a link in Kopete's chat window completely brakes the layout. It seems only products from parties that care about users (Microsoft and Apple :) ) that have that tag recognized.\n\nJust a little noise from the other side of the coin... :)"
    author: "Daniel \"Suslik\" D."
  - subject: "Re: Status of khtml"
    date: 2006-11-02
    body: "It's CSS3"
    author: "Sergio Cambra"
  - subject: "Over-zealous bug triage."
    date: 2006-10-30
    body: "> The past weekend saw the second bug triage day, concentrating on the\n> kde-pim  module.\n\nGreat.  They closed my bug.  I wonder if they'll actually fix it next time."
    author: "mart"
  - subject: "Re: Over-zealous bug triage."
    date: 2006-10-30
    body: "Link?  Maybe there was a mistake, or maybe it was a poorly defined bug that no-one could reproduce and had no more detailed info."
    author: "Leo S"
  - subject: "Re: Over-zealous bug triage."
    date: 2006-10-30
    body: "I've left a question to try to track it down, but once they closed the bug, they lost interest.\n\nhttp://bugs.kde.org/show_bug.cgi?id=114704"
    author: "mart"
  - subject: "Re: Over-zealous bug triage."
    date: 2006-10-30
    body: "> Great. They closed my bug. I wonder if they'll actually fix it next time.\n\nIf the bug still occurs for you then re-open it."
    author: "Robert Knight"
  - subject: "KViewShell?"
    date: 2006-10-30
    body: "Shouldn't it be replaced be oKular? Why is there still work on it?"
    author: "Jan"
  - subject: "Re: KViewShell?"
    date: 2006-10-30
    body: "Why does it need to be replaced by okular?\n"
    author: "AC"
  - subject: "Re: KViewShell?"
    date: 2006-10-30
    body: "indeed. we do have now two applications which look the same and (try to) do the same. this might not be the best situation, but only time can tell which one is/will be the best. after all, this is how free software works."
    author: "superstoned"
  - subject: "Re: KViewShell?"
    date: 2006-10-30
    body: "oKular may have a great name, but name alone doesn't determine which application (oKular vs KViewShell) is the better universal viewer."
    author: "Corbin"
  - subject: "Re: KViewShell?"
    date: 2006-10-30
    body: "Just for the record, the optimizations they added to kviewshell are in okular code for a year or more already."
    author: "Albert Astals Cid"
  - subject: "Re: KViewShell?"
    date: 2006-10-30
    body: "relax, albert! You're a great coder. Oklular is a nice program, so is KViewShell."
    author: "ac"
  - subject: "Re: KViewShell?"
    date: 2006-10-30
    body: "You still have to see me unrelaxed ;-)"
    author: "Albert Astals Cid"
  - subject: "Re: KViewShell?"
    date: 2006-10-31
    body: "Just for the record, I wasn't commenting on the quality of either of them (except that I like the name)."
    author: "Corbin"
  - subject: "Re: KViewShell?"
    date: 2006-10-31
    body: "Dublicate effort? That's not good."
    author: "Andre"
  - subject: "Mailody"
    date: 2006-10-30
    body: "Why this new software? Is there anything wrong with KMail?! There is no explanation on Mailody website, except maybe the sentence \"Supports Tabbed email reading\" at the bottom.\n\nWill it support IMAP only? I think this would be a mistake to support only one protocol.\n\nGood luck anyway!"
    author: "Hobbes"
  - subject: "Re: Mailody"
    date: 2006-10-30
    body: "You can read this: http://www.omat.nl/drupal/?q=node/98 (the author's blog). And also some interesting articles in his blog regarding this new email client ;)"
    author: "fred"
  - subject: "Re: Mailody"
    date: 2006-10-30
    body: ">>Is there anything wrong with KMail?!\n\nWhy should the creation of Mailody mean that there is anything wrong with kmail?\n"
    author: "AC"
  - subject: "Re: Mailody"
    date: 2006-10-30
    body: "> Why should the creation of Mailody mean that there is anything wrong with kmail?\n\nIt seems obvious to me: they are both mail clients! Whether KMail has nothing wrong and a new mail client is purposeless, whether KMail lacks features and a new client may be useful. Pure logic.\n\nI have read his posts on his blog (thanks Fred). Mailody was started because its author missed features in KMail and he was not able to add them in KMail. It seems that KMail code was hard to understand."
    author: "Hobbes"
  - subject: "Re: Mailody"
    date: 2006-10-30
    body: "> It seems that KMail code was hard to understand.\nSadly I'd agree if that were the case.\nI tried to look into fixing something a few days ago. It is horribly unorganized under the top level directory they have 414 files!\nI hope for kde4 they plan on cleaning it up a bit."
    author: "Stephen"
  - subject: "Re: Mailody"
    date: 2006-10-30
    body: "\"Why this new software?\"\n\nBecause the developer had an itch he wanted to scratch. Why should the developer need to justify his activities to you?\n\n\"Is there anything wrong with KMail?!\"\n\nMaybe, maybe not. How this is relevant to Mailody is beyond me. Or are you sying that we should forbid developers from working on another apps, if there already exists a similar app? Related to this: have you already told Amarok-devels to stop working on Amarok, since we already have Juk? How about Kate/Kwrite/Kedit? Konsole/Yakuake? Kaffeine/Codeine/Kmplayer/etc.?\n\nBTW. Shouting questions is quite rude.\n\n\"Will it support IMAP only? I think this would be a mistake to support only one protocol.\"\n\nIf it doesn't work the way you'd like, then don't use it. Problem solved."
    author: "Janne Ojaniemi"
  - subject: "Re: Mailody"
    date: 2006-10-31
    body: "I hope that KDE developers are not all that rude and irrational! I planned on working for KDE, but now I ask myself questions.\n\nActually, I should not ask questions, apparently. Do KDE developers think that starting a new application without any goal is a good option?!\n\nI think that reasonable people start a new application because there is something wrong with other similar applications (or because there is simply no similar application!). They have new ideas to improve things (may it be in features, code organization, license, etc.). This (of course) applies to Mailody (see the link given by Fred). I wanted to know what was wrong with KMail (I was surprised), and Mailody author explains on his blog: essentially bad code to add the features he wants.\n\nI hope KDE developers have enough in mind, too, in order to explain their goals! I feel afraid now!\n\n\"If it doesn't work the way you'd like, then don't use it. Problem solved.\"\n\nNice. I appreciate the advice. :)\n\nSeriously my point is: nowadays applications tend to do a lot. Toolkits, programming languages and techniques are good enough to build complete and perennial applications. If one wants Mailody to have some future, other developers, etc. it should aim at having at least POP in addition to IMAP. Otherwise, Mailody will not spread too much and will not gain a lot of developers. And in the end, it will be abandoned when the original author stops it. So this is just an advice...\n\nCrazy world, anyway..."
    author: "Hobbes"
  - subject: "Re: Mailody"
    date: 2006-10-31
    body: "\"I hope that KDE developers are not all that rude and irrational! I planned on working for KDE, but now I ask myself questions.\"\n\nI'm not a KDE-developer. Nor do I consider myself to be \"irrational\". All my opinions are mine and do not represent KDE in any shape or form.\n\n\"Actually, I should not ask questions, apparently.\"\n\nSure you can ask questions. But asking them in a demanding manner (\"why this piece of software? Is there something wrong with kmail!?\") is quite rude. Instead of shouting questions and demanding answers, why not simply ask (for example) \"why not work on Kmail instead?\". The tone is much nicer. Using exclamation-points (\"!\") in questions make it seem like you are shouting and demandind answers.\n\nI'm not even sure what prompted this discussion. So we have a developer who started writing a new mail-client. More power to him! Sometimes (most of the time) people do what they want to do, and maybe he just wanted to write a new mailclient. He did not want to work on Kmail, he wanted to start from scratch. And there's nothing wrong with that. Something the best course of action is to start from the beginning."
    author: "Janne"
  - subject: "Re: Mailody"
    date: 2006-10-30
    body: "\"Why this new software?\"\n\nBecause the developer had an itch he wanted to scratch. Why should the developer need to justify his activities to you?\n\n\"Is there anything wrong with KMail?!\"\n\nMaybe, maybe not. How this is relevant to Mailody is beyond me. Or are you sying that we should forbid developers from working on another apps, if there already exists a similar app? Related to this: have you already told Amarok-devels to stop working on Amarok, since we already have Juk? How about Kate/Kwrite/Kedit? Konsole/Yakuake? Kaffeine/Codeine/Kmplayer/etc.?\n\nBTW. Shouting questions is quite rude.\n\n\"Will it support IMAP only? I think this would be a mistake to support only one protocol.\"\n\nIf it doesn't work the way you'd like, then don't use it. Problem solved."
    author: "Janne"
  - subject: "Unity? And fix KHTML.info"
    date: 2006-10-30
    body: "I have to admit that I'm a bit troubled about the current status of khtml vs Unity as well. What is the situation? And what will be the benefits and shortcomings?\n\nAlso, the khtml.info side is broken, can someone, please, fix that?"
    author: "liquidat"
  - subject: "Pattern"
    date: 2006-10-30
    body: "KHTML - Unity, KMail - Mailody, Okular - KViewShell, there seems to be a very strong pattern here. Some users would like all developers to work on the same project, some developers don't agree upon the path to take and write different apps. This is why there is Krita and not just the Gimp, this is why there is Amarok and not just XMMS ( for heaven's sake ;-) ). This is probably a good thing. The people who prefer to work on old KHTML don't want to work on Unity, I presume. The people who still develop KViewShell have some reason (it doesn't matter whether it's a good one) to not work on Okular. So they wouldn't work on Okular anyway.\n\nSure it's bad to split efforts when there's no serious reason, but on the other hand it's good (very good) to have two or three different apps with different philosophies for (approximately) the same job. KDE is just one development platform, but KDE apps are plenty, which is just a proof of the success of the platform."
    author: "Gato"
  - subject: "Re: Pattern"
    date: 2006-10-30
    body: "For KViewShell/Okular see here:\n\nhttp://dot.kde.org/1148838047/1148852615/1148890165/"
    author: "Rick"
  - subject: "Re: Pattern"
    date: 2006-10-30
    body: "\"This is probably a good thing.\"\n\nNot necessarily. It's both good and bad. Look at it as a solution to a problem. The problem is the developers don't agree about the proper way forward. This is not good; it would be better if there were consensus about the correct path to take, and everyone's efforts could be unified towards a single goal, which would then be attained faster. But they don't agree, so the solution is that each of them tries his or her preferred method separately. This is good: eventually, we will find out which of them was right. (Could be both).\n"
    author: "illissius"
  - subject: "Re: Pattern"
    date: 2006-10-30
    body: "> it would be better if there were consensus about the correct path to take\n\nRight, as long as the path is really the correct one. But some paths have subtle flaws that will only be revealed by experience. And sometimes none of the paths is the correct one, because the competing solutions are not really solutions to the same problem, they are solutions to similar problems.\n\nSometimes the developers are not compatible themselves, and they can't form a single team, so they do the next best thing and form two teams.\n\nIt's not really a reason to get alarmed. However if none of the alternatives ends up really good, then you do have a real problem - and then it might be time to sacrifice diversity and emphasize sameness. \n\nWhat's important is letting things evolve instead of getting dogmatic. \n\nOh and it's also important to ship just one of the alternatives with stock KDE, and perhaps make it easy for distributions to replace it with their choice. And icecream. And an end to bugs ;-) \n "
    author: "Gato"
  - subject: "Re: Pattern"
    date: 2006-10-30
    body: "KDE vs GNOME is another example!"
    author: "Corbin"
  - subject: "Re: Pattern"
    date: 2006-10-31
    body: "I'm sure there's also some pain when a competing app becomes dominant, thereby sending a developers years of hard work and nurturing to the second-class-citizen realm.  I can definitely sympathize with a developer not wanting to \"give up\" and \"work for the other guys.\"  Competition is a good thing; it keeps the level of innovation high.  Look at some of the great ideas that have become mainstream because a developer had the balls to start a new project."
    author: "Louis"
  - subject: "Nepomuk"
    date: 2006-10-30
    body: "Never heard about Nepomuk. Does this mean that work on Tenor is definitely stopped?"
    author: "MM"
  - subject: "Re: Nepomuk"
    date: 2006-10-30
    body: "tenor has never seen any real work, except for some code which never even made it to the KDE repository (afaik). tenor was a concept, and nepomuk & strigi are implementing it."
    author: "superstoned"
  - subject: "NEOPUNK"
    date: 2006-10-30
    body: "OK, I know that NEPOMUK is a Networked Environment for Personalized, Ontology-based Management of Unified Knowledge, however every time I see \"NEPOMUK\" my inner voice says \"NEO-PUNK\"."
    author: "Jonathan Dietrich"
  - subject: "Re: NEOPUNK"
    date: 2006-10-30
    body: "wow, man, you have a weird problem ;-)"
    author: "superstoned"
  - subject: "Re: NEOPUNK"
    date: 2006-10-30
    body: "Same for me...  I think they _want_ us to think we're crazy!"
    author: "Corbin"
  - subject: "Re: NEOPUNK"
    date: 2006-10-30
    body: "NEPOMUK is also city in Czech Republic ( http://old.nepomuk.cz/ ) and birthplace of Saint John of Nepomuk ( http://www.sjn.cz/ )."
    author: "Poborskiii"
  - subject: "Re: NEOPUNK"
    date: 2006-10-30
    body: "And I thought it was just me.   It freaks me out every time I see it because NEO PUNK just makes more sense.\n\nAfter all, Neo WAS a punk! :)"
    author: "Tom Corbin"
  - subject: "Technical Preview"
    date: 2006-10-30
    body: "Maybe it is a little to optimistic, but the first technical preview should be out soon (from the \"release plan\"). Is there already a new official date ?"
    author: "Felix"
  - subject: "Re: Technical Preview"
    date: 2006-10-31
    body: "I think a second Krash snapshot is in the planning."
    author: "Gato"
  - subject: "huuh? diplomatic layer alert"
    date: 2006-10-31
    body: "\"The Unity web rendering engine experiment is removed from KDE SVN, due to a change in the circumstances that prompted its creation.\"\n\nCan someone please decode that message to me?"
    author: "Andre"
  - subject: "Re: huuh? diplomatic layer alert"
    date: 2006-10-31
    body: "Read the previous comments, it was answered near the top (merged with Apple's server)."
    author: "Corbin"
---
In <a href="http://commit-digest.org/issues/2006-10-29/">this week's KDE Commit-Digest</a>: Work on <a href="http://decibel.kde.org/">Decibel</a> and the KDE-based <a href="http://nepomuk.semanticdesktop.org/">NEPOMUK</a> components accelerates. The <a href="http://dot.kde.org/1152645965/">Unity</a> web rendering engine experiment is removed from KDE SVN, due to a change in the circumstances that prompted its creation. <a href="http://ktabedit.sourceforge.net/">KTabEdit</a>, a guitar tablature editor is imported into the KDE SVN playground. A branch of kde-pim for improvements in future 3.5 releases shows promise with the introduction of several new features. QMA, an experimental email client, continues to mature and is renamed <a href="http://www.mailody.net/">Mailody</a>. Usability and file format support refinements in <a href="http://amarok.kde.org/">Amarok</a>. Speed optimisations in KViewShell and <a href="http://kftpgrabber.sourceforge.net/">KFTPGrabber</a>. More improvements in the state of games in KDE 4.


<!--break-->
