---
title: "K3b Fundraiser Started"
date:    2006-03-09
authors:
  - "str\u00fcg"
slug:    k3b-fundraiser-started
comments:
  - subject: "Bank transfer"
    date: 2006-03-09
    body: "Within the EU you can do bank transfers easily and for free if you just know some information about the recipient and the account. I think this is something that EU law requires, and I assume Sebastian lives in Europe.\n\nFor a bank transfer to work you need the IBAN (international bank acount number?) and BIC (bank ? ?) codes, a full name and probably an address. That way anyone with a decent bank that has a decent web interface can easily contribute. At least all the Finnish ones I've seen have this functionality easily accessible."
    author: "Chakie"
  - subject: "Re: Bank transfer"
    date: 2006-03-09
    body: "Ahem, I wouldn't give my bank account number to my best friend, let alone publish it on the web :D\nPaypal should be fine."
    author: "Giacomo"
  - subject: "Re: Bank transfer"
    date: 2006-03-09
    body: "...because you are afraid that someone transfers money to your account?"
    author: "max"
  - subject: "Re: Bank transfer"
    date: 2006-03-09
    body: "This is Europe, not USA. Knowing account number is useless to you when you want to withdraw money here, you actually have to prove your right to do it. What a concept, eh?"
    author: "Vz"
  - subject: "Re: Bank transfer"
    date: 2006-03-09
    body: "That's not true, at least in Spain. Having the account number is more than enough to bill someone's account. If it's a small quantity (say 30 EUR) most people won't ever realize."
    author: "Pau Garcia i Quiles"
  - subject: "Re: Bank transfer"
    date: 2006-03-09
    body: "Hmm, spanish citizens must be rich :o)"
    author: "ac"
  - subject: "Re: Bank transfer"
    date: 2006-04-16
    body: "\"Having the account number is more than enough to bill someone's account.\"\n\nNo it's not."
    author: "truth commission"
  - subject: "Re: Bank transfer"
    date: 2006-04-18
    body: "Yes, it is. In Spain it is."
    author: "NabLa"
  - subject: "Re: Bank transfer"
    date: 2006-04-16
    body: "\"This is Europe, not USA. Knowing account number is useless to you when you want to withdraw money here, you actually have to prove your right to do it. What a concept, eh?\"\n\nThe same is true in the USA.  However, there are always ways to \"prove\" that you're someone you're not.  No security measure is perfect --what a concept.  And not being a chauvinist dick -- what a concept."
    author: "truth commission"
  - subject: "Re: Bank transfer"
    date: 2006-03-09
    body: "Heh, why advertise a method of payment that can't be used? \n\n\"Well, we have this nice method that you could use for free, but we won't tell you any details, it's a secret.\" :)\n\n"
    author: "Chakie"
  - subject: "Re: Bank transfer"
    date: 2006-03-09
    body: "If you browse to the k3b site you'll see that you can get Bank Transfer details by sending a mail to Sebastian.\n\nSebastian probably missed that part when submitting the article for the dot."
    author: "Kevin Krammer"
  - subject: "Re: Bank transfer"
    date: 2006-03-09
    body: "Thanks for the info!\n"
    author: "Chakie"
  - subject: "bank transfer"
    date: 2006-03-09
    body: "> you can do so using PayPal or a bank transfer.\n\nWhere can I find the necessary data for the bank transfer?"
    author: "max"
  - subject: "Re: bank transfer"
    date: 2006-04-16
    body: "The necessary data for a paypal transfer is the recipient's email address."
    author: "truth commission"
  - subject: "wow"
    date: 2006-03-09
    body: "K3B is one of the best tools on my KDE desktop.\n\nI think that GPL and the ASP shareware principle are not incompatible. But IBAN is easier..."
    author: "gerd"
  - subject: "Creditcard payments "
    date: 2006-03-09
    body: "Just wondering if KDE Ev couldn't set up a creditcard payment site and act as mediator for fundraising within KDE.  I don't use paypal and for undisclosed reasons I cant make international money transfers (which is also both slow and complex to do), but a credit card 'payment' (if done correctly) is fast, secure and convenient.\n\nPretty sure it would make it easier for a lot of people to contribute with such a system.\n\nCheers."
    author: "Einar"
  - subject: "Re: Creditcard payments "
    date: 2006-03-09
    body: "Yes, I also think this might be a nice idea. Currently, if you want to contrivute some money, you have to send some money to your paypal account first and then start there a transaction. Paypal might be nice for somebody who is using it regulary, but I would prefere a \"normal\" money transfer to somewhere."
    author: "Phobeus"
  - subject: "Re: Creditcard payments "
    date: 2006-03-09
    body: "There is a place for KDE e.V. donations: http://www.kde.org/support/support.php\n\nHowever, in my opinion this could get problematic quite quickly.  Handing out money in a volunteer community always puts some strain on the members of the community.  I think it would be difficult for e.V. to determine who and under what circumstances cash could be handed out.  Right now, it's generally limited to very tangible expenses -- traveling on behalf of KDE, promotional costs, conference registration and insurance, etc.  When you start handing out money (not, say, old hardware or whatever) people often think, \"Why not me too?\""
    author: "Scott Wheeler"
  - subject: "Re: Creditcard payments "
    date: 2006-03-12
    body: ">There is a place for KDE e.V. donations: http://www.kde.org/support/support.php\n\nSorry Scott, that one uses PayPal. And if someone cannot (or don't want) to use PayPal, bank transfer is expensive (outside of the EU), they don't really have a solution to donate."
    author: "Andras Mantia"
  - subject: "Re: Creditcard payments "
    date: 2006-03-14
    body: "Well, that kind of misses the point. First it's paypal, and secondly I was thinking of KDE ev. acting as a mediator for fundraising projects with each project having it's own 'account' number or something... So if e.g. K3B wants to have a fundraising initiative, KDE ev. would have the infrastructure to make a special 'account' for just that.\n\ncheers"
    author: "Einar"
  - subject: "I'll donate"
    date: 2006-03-09
    body: "I have some cash in my paypal collecting dust (its too much to write it off as lost change, but not enough to make it worth the pain to transfer it to my bank account), and i use k3b all the time...\n"
    author: "Mathias"
  - subject: "Re: I'll donate"
    date: 2008-04-13
    body: "i am behind $4,500 in bill being kicked out of my place and I have children in school please if you can call me 856-629-1805"
    author: "joe batts"
  - subject: "KDE Fundraiser"
    date: 2006-03-09
    body: "** Mozilla makes millions of Dollars with Google search box. **\n\nhttp://www.crn.com/sections/breakingnews/dailyarchives.jhtml?articleId=181501810\n\nKonqueror or KDE should do the same.\n\nSomething for KDE e.v.?\n."
    author: "max"
  - subject: "Re: KDE Fundraiser"
    date: 2006-03-09
    body: "> Konqueror or KDE should do the same.\n\nNot without win32 version.\n"
    author: "user"
  - subject: "Re: KDE Fundraiser"
    date: 2006-03-10
    body: "Why?\n\nKonqueror has a market share between 1 and 2%. Let's take a conservative approach and choose 1%. If 1billion of people have internet connection this makes 10million of konqueror users. More than enough."
    author: "max"
  - subject: "Actual state"
    date: 2006-03-09
    body: "K3b Fund-raiser state: 479,35/1000 Euro\n\nBTW: How is it with the reward - K3b 1.0?"
    author: "radfoj"
  - subject: "Re: Actual state"
    date: 2006-03-10
    body: "K3b 1.0 is currently under development. What I mean is that there will not be a K3b 0.13.\n\nI am currently trying to fix and implement those things I want to have in 1.0 and there will be a beta in the next few weeks, I hope."
    author: "Sebastian Trueg"
  - subject: "Re: Actual state"
    date: 2006-03-12
    body: "I think, atleast 100 to 150 million cds/dvds must have been burned using your s/w. You have been extremely important to us as far as archiving digital information on optical media is concerned. May 1.0 happen smoothly for you. God bless you Sebastian."
    author: "nits"
  - subject: "Re: Actual state"
    date: 2006-03-20
    body: "Wow 0.12 to 1.0 ... the change must be dramatic ;) :o :| :)\n\nI think k3b is the best application for KDE, hands down. I don't burn CDs often, but whenever I do I use k3b - and it just works! This is in contrast to winblows where you have to download some proprietary crippleware that typically can't even burn an ISO image.\n\nAre there any plans to include K3B into KDE 4? Would it change to K4B, or maybe a more user-friendly name?\n\nI have a couple of suggestions that would make the integration a little more slick:\n\nA right-click service for device icons, so that if I have a CD-ROM under media:/ I can choose to Copy CD..., etc.\n\nAlso, for actions that perform a simple task, such as copying a CD, burning an ISO, etc., there is no need to pop up the entire K3b application window in the background. It would be much cleaner to just show the dialog pertaining to the action. The only time the application window is needed is when you have an open-ended request (such as \"burn these files to cd\")."
    author: "LuckySandal"
  - subject: "Re: Actual state"
    date: 2006-03-20
    body: "+1 for Renaming and Suggestions !"
    author: "Fred"
  - subject: "Re: Actual state"
    date: 2006-03-21
    body: "Vulkano. \nPlease note the subtle use of the letter k.\n"
    author: "reihal"
  - subject: "Re: Actual state"
    date: 2006-03-21
    body: "Or Kash. \n\"It burns, burns, burns, the ring of fire, the ring of fire\", you know.\n\nhttp://www.lyricsdepot.com/johnny-cash/ring-of-fire.html"
    author: "reihal"
  - subject: "Re: Actual state"
    date: 2006-03-22
    body: "how about \"Kero\" for the name ;d\n\nProzilla and K3B are my favourite application to download and burn for my Linux iso/cd collections :)"
    author: "KeroppiX"
---
Sebastian Trüg has been working on <a href="http://www.k3b.org/">K3b</a> for a long time now and his computer has always served well for development. However it is now old and showing the first signs of senility. In order to keep up his work on K3b he needs a new machine. That is why the K3b fundraiser has been launched. The goal is to collect &euro;1000 Euro by 31st March 2006. If you are willing to donate you can do so using <a href="http://k3b.plainblack.com/donations">PayPal</a> or a bank transfer.  There will be one major reward and that is K3b 1.0.






<!--break-->
