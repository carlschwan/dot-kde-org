---
title: "KDE Commit-Digest for 24th September 2006"
date:    2006-09-25
authors:
  - "dallen"
slug:    kde-commit-digest-24th-september-2006
comments:
  - subject: "Cool!"
    date: 2006-09-24
    body: "Until now I completely ignored KLettres. I didnt think it could be useful for me. I thought it is for children. But just now I realize that you can learn strange alphabets from this. Fun and interesting! I'd really like to learn Hebrew letters."
    author: "Martin"
  - subject: "Re: Cool!"
    date: 2006-09-24
    body: "PS: MiniCLI-Calculator is great. What I'm still missing though is the option to drag an application from the minicli to the desktop. That would be a very intuitive and quick way to add new programs IMO.\n"
    author: "Martin"
  - subject: "Re: Cool!"
    date: 2006-09-24
    body: "you dont need this cause alt+f2 + typing first two or 3 letters is alot faster\n(now you regret that most of kde apps begin with \"k\":)"
    author: "anonymous-from-linux.org.ru"
  - subject: "Re: Cool!"
    date: 2006-09-25
    body: "The corresponding wish is here:\nhttps://bugs.kde.org/show_bug.cgi?id=73954"
    author: "a.c."
  - subject: "Re: Cool!"
    date: 2006-09-24
    body: "i was hoping, kde could be without mossad.."
    author: "i_hate_moses"
  - subject: "Re: Cool!"
    date: 2006-09-25
    body: "ok, Does anybody notice that the parent is really antisemitic? by the way shana tova..."
    author: "Patcito"
  - subject: "Re: Cool!"
    date: 2006-09-25
    body: "Why would you say that? Being critical of the *state* of Israel isn't the same as being antisemitic. Still, I think the parent you're replying to is widely off the mark as well. What has adding support for Hebrew in KLettres to do with the mossad?\n\nHaving said that: either way it's off topic here. I would like to see KDE as being politics-neutral; at least where it does not concern topics that directly affect KDE itself or it's supporting ecology (like software patents). "
    author: "Andre Somers"
  - subject: "Re: Cool!"
    date: 2006-09-25
    body: "Can't agree more. The fun part it that most jews can't accept any criticism before yelling \"anti-semith!\", if it was FBI or KGB instead of Mossad, I doubt anyone would really care or complain about the joke (I saw that as a joke).\nAnyway, this is not a KDE problem, there are better places ofr this kind of discussion :)"
    author: "Iuri Fiedoruk"
  - subject: "Re: Cool!"
    date: 2006-09-25
    body: "wow, you guyz are either ignorant or totally antisemitic. This is a blatant case of antisemitism. The guy name is \"i_hate_moses\" (moses is the most important figure of that religion) and then equates all jews with mossad. Are you guys all blind or something?"
    author: "Patcito"
  - subject: "Re: Cool!"
    date: 2006-09-25
    body: "Isn't Moses a prophet from the Christian bible as well?  How does that make it anti-semitic?"
    author: "ac"
  - subject: "Re: Cool!"
    date: 2006-09-25
    body: "Well, Moses is THE prophet, not just a prophet, in judaism...\nBut (as a jew I must say) I don't think his comment was antisemitic.. Just not in place. :P"
    author: "Hagai"
  - subject: "Re: Cool!"
    date: 2006-09-26
    body: "From my point of view, jews are a people, no a religion.\nI hope that when someone says \"I hate Maome\" or \"Moises is dead\" people could separate religious view from hating a entire country or people, even that sometimes they seem to be deeply mixed.\n\nAnyway, I don't hate anyone, just don't like the state (country) of Israel actions and don't like the fact they hide most of the time using that anti-semitic story.\n\nAnd, to stop (form my part) this discussion: WE ALL LOVE KDE :)"
    author: "Iuri Fiedoruk"
  - subject: "Re: Cool!"
    date: 2006-09-26
    body: "And of course we love our Israeli KDE hackers and the great hebrew support!"
    author: "fuman"
  - subject: "Re: Cool!"
    date: 2006-09-27
    body: "IIUC, Moses is also considered a prophet by Islam.\n\nI don't think that he is mentioned in the Bhagavad Gita so I guess that Hindus need not be offended."
    author: "James Richard Tyrer"
  - subject: "dangerous."
    date: 2006-09-24
    body: "> khtml is going to be killer post-unforking. between safari, nokia, omni,\n> konqueror it looks possible to hit 10% market share for khtml.\n\nthat is the idiotic point of view of people bowing to corporate interests and blinded by hype and featurism.\n\nWhile this stuff is controled by KDE's most fierce competitor, the next Microsoft, there is no way it can be a good deal.\n\nWill people ever learn?\nNever. Trade. Your. Freedom."
    author: "Kay"
  - subject: "Re: dangerous."
    date: 2006-09-25
    body: "This won't make KDE or Konqueror at all controlled by Apple or Nokia.  If at any point the KHTML devs disagree with the direction Apple and/or Nokia wish to take it, they can just separate their branch from Apple/Nokia.  It may be dangerous if it was under a non-OSS style license, or one that prevented forking, but khtml isn't at all under any threat from this move."
    author: "Corbin"
  - subject: "Re: dangerous."
    date: 2006-09-25
    body: "Que???  In case you missed it, Khtml is LGPL, so apple has no control over it.  By having one shared, unified code base we benefit from their spending money on developement and free up some of our resources, but if they start playing silly buggers then we just re-fork.\n\nFreedom intact.\n\nWhen will you trolls ever learn???\n\nJohn.\n\nP.S. Do I take it you are volunteering to maintain a separate fork???  No?  Didn't think so, you trolls are all talk and no action..."
    author: "Odysseus"
  - subject: "Re: dangerous."
    date: 2006-09-25
    body: "As to the quote you extracted: frankly, I am not sure why it's even there, and why Aaron is speaking on a project he is not involved in. There are no definite plans to change how khtml will be developed at this point.\n"
    author: "SadEagle"
  - subject: "Re: dangerous."
    date: 2006-09-25
    body: "let me show you how it works:\n\nit's raining outside today and the traffic is pretty light for a monday morning in a city of this size.\n\ni neither make the rain nor do i live in dublin but i'm fully capable of observing what's going on.\n\nthis particular set of observations came from attending george staikos' presentation where he made those exact statements. what's your problem, exactly?"
    author: "Aaron J. Seigo"
  - subject: "Re: dangerous."
    date: 2006-09-25
    body: "The fact that it's highly likely that that will not happen, and \nI don't like our users being misled.\n"
    author: "SadEagle"
  - subject: "Re: dangerous."
    date: 2006-09-25
    body: "then there's a disconnect amongst the khtml developers since the talk yesterday spoke about this effort in length, covering both the positives and negatives, with a definite bent towards it happening."
    author: "Aaron J. Seigo"
  - subject: "Re: dangerous."
    date: 2006-09-25
    body: "Well, George is a big proponent of the idea, and you've certainly covered a lot of the positives. Unfortunately, as far as I am aware, the negatives haven't yet been addressed. And as such, I think it'd be best to avoid saying that things may go one way or another. \n"
    author: "SadEagle"
  - subject: "Re: dangerous."
    date: 2006-09-25
    body: "it's not hype and featurism. it's being able to command enough market share to get people testing against khtml so that websites work reliably with it.\n\nas for working with the likes of apple, i have many issues with them as a company. but when it comes to working with them on an open source code base our relationship is defined by the licenses involved and the lgpl does a rather good job of managing that, imho.\n\nwe lose none of our freedom and gain the hands and feet of many."
    author: "Aaron J. Seigo"
  - subject: "Compiz??"
    date: 2006-09-25
    body: "I had read that someone was making a Compiz clone to be included in KWin for KDE4... Is there any relationship between that and this?"
    author: "Rafael Rodr\u00edguez"
  - subject: "Re: Compiz??"
    date: 2006-09-25
    body: "There is ongoing work in a branch to extend kwin into a composition manager with similar capabilities to compiz, yes. Keep in mind that kwin is a proven, tested codebase with a rather solid track record at managing windows of large numbers of real-world desktops, while compiz is none of these things - makes it very attractive to keep it around."
    author: "Eike Hein"
  - subject: "Re: Compiz??"
    date: 2006-09-25
    body: "Oops, somehow I failed to answer your actual question: No, there is no direct connection between those commits aiming at better compatibility with compiz and the work being done in that kwin branch. "
    author: "Eike Hein"
  - subject: "Re: Compiz??"
    date: 2006-09-25
    body: "In general I'm getting the idea that Gnome + Compiz is somewhat in lead of KDE. Or am I wrong? I'm seeing a lot of cool screenshots of Gnome + Compiz, but in the KDE department I don't.\n\nI'm not trolling here, I'm a hugh KDE fan. But it's just a (maybe wrong;-) observation..."
    author: "Harry"
  - subject: "Re: Compiz??"
    date: 2006-09-25
    body: "in the cool xgl screenshot category, yes, compiz is ahead.\n\nnot sure how much that'll matter in 6 months when the rotating cube effect becomes old hat. i continue to be impressed with how much focus this is getting when it's a nice feature set but neither critical or addressing the real issues we contend with on the desktop."
    author: "Aaron J. Seigo"
  - subject: "Re: Compiz??"
    date: 2006-09-25
    body: "Well, we all just want cool effects in our desktops to show our friends. If you say the rotating cube will be old in 6 months, we'll wait for Plasma :D\n\n*In Seigo we trust*"
    author: "Rafael Rodr\u00edguez"
  - subject: "Re: Compiz??"
    date: 2006-09-25
    body: "When it comes to that there is no real difference on Gnome + Compiz and KDE + Compiz. \n\nCompiz is nothing more than a new windowmanager with the ability to do som fancy stuff with GL. It's still new, and are lacking both in maturity and the functianlity usually found in windowmangers like kwin oand metacity."
    author: "Morty"
  - subject: "alt-f2 calculator"
    date: 2006-09-25
    body: "Thanx! That was the feature I was waiting for!\nAlt-F2 + is one of my favorite feature in the whole KDE.\nProbably one of the most common combination of keystrokes being\n\"ALT-F2 gg:\" \n:-)"
    author: "piters"
  - subject: "XPS, XML Paper Specification"
    date: 2006-09-25
    body: "What are the chances of the support for XPS being generalised so that it can be reused by the rest of KDE?  Particularly tools to convert out of XPS into other formats?  \n"
    author: "Alan Horkan"
  - subject: "Compiz"
    date: 2006-09-26
    body: "What Compiz version?"
    author: "fuman"
---
In <a href="http://commit-digest.org/issues/2006-09-24/">this week's KDE Commit-Digest</a>: The KDE World Conference, <a href="http://conference2006.kde.org/">Akademy 2006</a>, kicks off in Dublin. A rewritten version of <a href="http://edu.kde.org/kturtle/">KTurtle</a>, an educational programming tool, is imported into KDE SVN. ThreadWeaver is moved into kdelibs. Hebrew sounds are added to <a href="http://edu.kde.org/klettres/">KLettres</a> to add learning support for the language. Improvements in the <a href="http://en.wikipedia.org/wiki/Opendocument">OpenDocument</a> format and <a href="http://en.wikipedia.org/wiki/XML_Paper_Specification">XML Paper Specification</a> format support in <a href="http://kpdf.kde.org/okular/">okular</a>. Support for GPS metadata synchronisation in kipiplugins, on which <a href="http://www.digikam.org/">Digikam</a> and <a href="http://kphotoalbum.org/">KPhotoAlbum</a> depend. Support for calculations containing non-integer numbers (ie. numbers with decimal points) in the minicli (Alt-F2). Modifications made to support using <a href="http://en.wikipedia.org/wiki/Compiz">Compiz</a> as a window manager. More work in Memory Monitoring and Network Management in <a href="http://solid.kde.org/">Solid</a>.
<!--break-->
