---
title: "Zack Rusin on Linux Link Tech Show"
date:    2006-03-02
authors:
  - "jriddell"
slug:    zack-rusin-linux-link-tech-show
comments:
  - subject: "nicely done =)"
    date: 2006-03-02
    body: "just got finished... t. and the p-man and i sat around and listened in. family around the radio, so 1930. except now the radio is a laptop and the airwaves are tcp/ip networks ;)\n\nzack did an awesome job. very entertaining (lots of laughs) and lots of great points about X.org, open source, the open source desktop, kde and more ... props to da man, and thanks for taking the time out of your schedule to do the show! =)"
    author: "Aaron J. Seigo"
  - subject: "Re: nicely done =)"
    date: 2006-03-02
    body: "t likes to listen to discussion about latest developements in X and KDE? Where do you find gals like that :)?"
    author: "Janne"
  - subject: "Re: nicely done =)"
    date: 2006-03-02
    body: "well, i was even more amazed about the fact the \"P-man\" buyed into it... :D"
    author: "superstoned"
  - subject: "Re: nicely done =)"
    date: 2006-03-02
    body: "Oh man I was on the floor at times dang that show has it's moments! :P\nAnd by the way Aaron, it was nice of you to hop onto the IRC channel! ;)"
    author: "Troels Just"
  - subject: "Re: nicely done =)"
    date: 2006-03-02
    body: "The p-man is interested in x.org/exa/xgl/xegl/KDE development? When will his first patches come in? ;-)"
    author: "lippel"
  - subject: "got a working download link"
    date: 2006-03-02
    body: "^^ can't find a link to the show that works, anyone got one?\n\ncheers Peter"
    author: "peter vdm"
  - subject: "Re: got a working download link"
    date: 2006-03-02
    body: "http://tllts.info/archives/tllts_125-03-01-06.ogg"
    author: "cb400f"
  - subject: "cutting to the chase"
    date: 2006-03-02
    body: "ok..\nzack starts at 27:30 or so.\n(believe me.. you will not miss anything)"
    author: "Robert Guthrie"
  - subject: "Listening to it..."
    date: 2006-03-02
    body: "Listening to the podcast and am nearing the end, but its been really good. Zack has been a really good and interesting interviewee. Nice one :) Very informative."
    author: "Hans-Christian Andersen"
  - subject: "wow"
    date: 2006-03-02
    body: "It seems that Zack has a *lot* of exiting stuff on his harddrive, I hope we're\ngonna see some of it soon!"
    author: "ac"
  - subject: "Re: wow"
    date: 2006-03-04
    body: "I agree, why keep it closed? He should release it somewhere!"
    author: "ac as well"
  - subject: "Flash on PPC"
    date: 2006-03-02
    body: "He wrote his own power pc Flash implementation?\nNeato!"
    author: "Ryan"
  - subject: "Indirect VS direct rendering"
    date: 2006-03-03
    body: "I'm far from being an expert on the subject, but it looks like to me that certain accessibility graphical effects would be only possible, or at least more elegantly implemented, on direct render, like a desktop magnifying glass. Is that so?\n\nAlso, doesn't direct rendered use the GPU better?"
    author: "blacksheep"
  - subject: "Re: Indirect VS direct rendering"
    date: 2006-03-03
    body: "If you use direct rendering (right now at least), compositing effects and a desktop magnifying glass wouldn't work because everything is being rendered directly on the graphics card.\n\nBoth Direct Rendering and Indirect Rendering can use the GPU, and the main difference would be that indirect rendering goes through the X server instead of directly to the graphics card.\n\nStuff like a magnifying glass would be a whole lot easier with indirect rendering (to do it with direct rendering you would need to rewrite the drivers)."
    author: "Corbin"
  - subject: "Inte 945G; GMA950"
    date: 2006-03-06
    body: "Not exactly sure what Zach was talking about.\n\nDid he mean to say that graphs cards based on Intel chips are going to be available?\n\nDid he mean to say that Keith Packard had written a (3D) driver for the Intel Graphics Media Accelerator 950 (which is what is in the 82945G north bridge chip?\n\nA 'Yes' to both of these would make great improvements in the Linux graphics card problem!\n\nNote to Intel: you could probably offer a 945 series graphics chip using 945 dice that you are now throwing away (ones with a broken Pentium bus) in a smaller package. "
    author: "James Richard Tyrer"
  - subject: "Re: Inte 945G; GMA950"
    date: 2006-03-06
    body: "> ones with a broken Pentium bus.\n\nUnfortunately it appears not, but ..."
    author: "James Richard Tyrer"
  - subject: "Glucose?"
    date: 2006-03-09
    body: "Does anyone know anyting more about Glucose than its going to be a new acceleration architecture for Xorg, and its meant to not require any driver changes?  This is the first I've heard anything about Glucose, and trying to google for information on it failed miserably."
    author: "Corbin"
  - subject: "Re: Glucose?"
    date: 2006-08-16
    body: "It's hardly surprising that very little information is available: news of it just became public just twelve hours ago. Your best source of information should be http://zrusin.blogspot.com/2006/08/glucose-and-graphics.html ."
    author: "Parker Coates"
---
<a href="http://people.kde.nl/zack.html">Zack Rusin</a>, KDE graphics and X developer, will be on the audio streamed <a href="http://tllts.org/">Linux Link Tech Show</a> this US-time evening.  Zack will be talking about the latest developments in X and how we can make best use of the up-coming new technologies in KDE.  The show starts at 0200 Thursday UTC (2100 Wednesday evening Eastern Standard Time) and will last for about 45 minutes, streaming in MP3 format.  <strong>Update:</strong> the 03-01-06 show is <a href="http://tllts.org/dl.php">now available for download</a>, Zack starts 27 minutes in.  Slides are also available from <a href="http://ktown.kde.org/~zrusin/fosdem.pdf">Zack's talk at FOSDEM</a>.




<!--break-->
