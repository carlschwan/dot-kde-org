---
title: "Interview: Elizabeth Krumbach of LinuxChix"
date:    2006-09-22
authors:
  - "wolson"
slug:    interview-elizabeth-krumbach-linuxchix
comments:
  - subject: "Premature ending"
    date: 2006-09-22
    body: "I think Elizabeth had more to say towards the end. ;)"
    author: "Col. Habbablab"
  - subject: "Re: Premature ending"
    date: 2006-09-22
    body: "Right.  The last sentence of the interview should read:\n\n\"Although I don't believe people should contribute to F/OSS to be recognized, it is important to make it clear that work done in all areas of a project is important and valuable. After all, a big hurdle F/OSS has to overcome is becoming more user-friendly, and things like Artwork, Translations, and Documentation are important.\"\n\nThanks for pointing the truncation out."
    author: "Wade Olson"
  - subject: "Re: Premature ending; valuing all efforts"
    date: 2006-09-22
    body: "Thank you for printing the final point. I feel that it is an important one. All efforts are valuable, and acknowledging that will improve the quality of ALL contributions. "
    author: "Valorie Zimmerman"
  - subject: "The article is incomplete!"
    date: 2006-09-22
    body: "The last sentence is truncated, even.\n\nAnd for one whose stated goal is to reduce sexism, and who praises people who don't care what her own gender is, she seems to care about people's genders a lot herself: \"Reach out to women specifically\"."
    author: "em"
  - subject: "Re: The article is incomplete!"
    date: 2006-09-22
    body: "It's called \"one way to address and help fix a problem.\" We would not care about gender if it were not constantly thrown back in our faces.\n\nNice interview, thank you!\n\nCarla Schroder"
    author: "\nCarla Schroder"
  - subject: "Re: The article is incomplete!"
    date: 2006-09-22
    body: "I'm kind of torn between both sides of this argument.  There's some deeper truth in the idea that we shouldn't have to be biased in one direction in order to fix a bias in the other direction.  However, I'm not sure that applies to marketing so much.  For one thing, every subculture has its own advocates, who will market to their subculture in a way that their subculture hears well.\n\nThe grandparent poster is correct, in that we shouldn't encourage bias in favour of women.  And it's probably wrong to think that marketing towards men should be gender-neutral.  However, we DO have to remove any bias in favour of men when marketing in general.\n\nWe need to get to a stage where people don't assume that the market IS men.  And the best way to do that is to actively encourage women, until the take-up by both genders is equal.\n\nOn gender being thrown back in peoples' faces, and on the issue raised by EK of women having to prove themselves in LUGs... women might be interested to know that guys often feel like they have to prove themselves at LUGs too.  The LUG I attend largely consists of highly experienced geeks, who know each other well, and get on well.  It's a clique, and it's natural to feel like an outsider, even as a guy, when you first attend.  I've attended a few of times now, and participate a lot on mailing lists etc., but it still feels like I have to prove myself.  As a guy, I know that's not a gender issue.  I also know that the group welcomes women.  However, I can easily imagine a woman coming to a meeting, feeling isolated, and assuming it's a gender thing.  Basically, we need women to be more vocal, and more present in the community, so that other women don't feel like they're alone.  And, in the interests of fairness, I've gotta say it... just trusting and accepting the guys more--allowing them to not be evil or annoying despite some inexperience--would probably help too ;)"
    author: "Lee"
  - subject: "Re: The article is incomplete!"
    date: 2006-09-22
    body: "Thank you for a well thought and insightful post.  I agree with most of your points, except this:\n\n\"And the best way to do that is to actively encourage women, until the take-up by both genders is equal.\"\n\nI don't think take-up by both genders will ever be equal.  Statistically, males are simply more likely to be interested in technology than females.  This is worldwide and going back quite a bit of time, so I doubt it's purely cultural, but it wouldn't matter if it was.\n\nNow, this doesn't mean anything as far as *individual* females are concerned.  But it does mean that if we concentrate our efforts on attracting women, again statistically we're going to waste most of those efforts.  And the individual males who haven't been approached because of that don't deserve our lack of interest any more than individual females do.\n\nDon't get me wrong, I think it would be great if there were more women in the community, but not at any price or through any means.  And introducing sexism with the pretext of fighting it does make me a bit angry.  The OSS ideal is a meritocracy, and individuals should be judged on the quality of their contributions, nothing else -- and certainly not their sex."
    author: "em"
  - subject: "Re: The article is incomplete!"
    date: 2006-09-23
    body: "You're making all kinds of unsupported assumptions. Aiming for 50/50 representation is not realistic, and is not a stated goal of anyone with sense. We'll settle for not having barriers placed in the way of women who do want to contribute to FOSS projects. We are subjected to all kinds of idiotic treatment and behaviors just because we are women. We're not the ones making gender an issue. We're like anyone else who wants to be a part of the FOSS world- we want to do good work and be a part of the community, and not have to waste all kinds of time and energy getting hassled by idiots.\n\nBTW, I'm not afraid to use my real name.\n\n\n"
    author: "Carla Schroder"
  - subject: "Re: The article is incomplete!"
    date: 2006-09-24
    body: "> You're making all kinds of unsupported assumptions.\n\nNo, I'm not.  You're probably reading into my comments things I haven't said.\n\n> We are subjected to all kinds of idiotic treatment and behaviors just because\n> we are women.\n\nSo far I haven't seen any proof of that.  The specific examples of supposed discrimination that have been described so far, such as \"having to prove oneself at LUGs\" or somebody posting a link to \"linux babes\" below, have left me distinctly unimpressed.  They don't seem to warrant these stern demands for the male majority to change their ways.\n\n> We're not the ones making gender an issue.\n\nYou *are* the ones I have seen making gender an issue, my friend.  Again, I have not seen any instances of sexism in OSS mailing lists, but I have read this interview.  Perhaps you should be more careful as to the way you make your first impression on people without any previous contact with the issue?\n\n> BTW, I'm not afraid to use my real name.\n\nGood for you.  I happen to choose not to do so, like the majority of users of Internet forums.  If that makes you feel braver, then I guess it goes to show that \"macho\" feelings aren't exclusively male."
    author: "em"
  - subject: "Re: The article is incomplete!"
    date: 2006-09-29
    body: "\"Aiming for 50/50 representation is not realistic, and is not a stated goal of anyone with sense.\"\n\nExcellent point. The right goal to reach for is to make sure the community is welcoming, and as barrier-free as possible, to anyone - regardless of gender, ethnicity, orientation, etc... \n\nBTW, are you the \"Carla Schroder\" who wrote the O'Reilly _Linux Cookbook_ ? If so, great book! My copy is well thumbed-through. If not, then you really should go buy a copy of your namesake's book. It's *extremely* useful... "
    author: "David Huff"
  - subject: "Re: The article is incomplete!"
    date: 2006-09-27
    body: "Good points.  I agree that there seems to be a technological divide -- purely in terms of interest -- between women and men.  However, it strikes me that women (again, as a general trend) tend to value things which provide social empowerment.  As computers joined the internet and became global communication devices, as well as devices for managing photos and videos, women's interest and participation in technology seems to have increased.\n\nI think mobile phones are probably at 50/50 split right now, after all.  Many women use them for gaming too, and know how to use the technology just fine -- many better than I do, in fact, because they've been interested in their phones as a social tool.  I, on the other hand, despite liking technology and having an affinity with it, fell behind, since I considered them to be a hindrance to the peace and quiet of nature, and worried about the whole radio-waves-beside-the-brain thing, etc.  So, I think \"ability\" is largely dependent on interest in the topic, and perseverance.\n\nNow, I don't know if this is just my own stereotyping.  Maybe it's just that, as the internet happened, more people took up computers in general, and the common image of computer users began to change as mainstream press noticed that computer users weren't just teenage boys playing games.\n\nBUT, if I'm right, then interesting in technology as much as men are interested, is just a matter of providing the right tools, and the right community atmosphere.  If Linux allows people to communicate better, to share ideas better -- to enjoy the social side of life in a more global or simply better way -- then perhaps we'll really see that 50/50 split some day :)\n"
    author: "Lee"
  - subject: "Re:"
    date: 2006-09-22
    body: "Well, maybe she meant genderneutral attitude to be the goal, but the way leading there might be to care about the weaker party."
    author: "me"
  - subject: "Re:"
    date: 2006-09-22
    body: "\"weaker\" is perhaps not the ideal word choice there ;)\n\nSincerely,\nYour Neighbourhood Polical Correctness Officer"
    author: "Leo S"
  - subject: "Re:"
    date: 2006-09-22
    body: "What's \"Polical Correctness\"?"
    author: "Patrick Starr"
  - subject: "Re: The article is incomplete!"
    date: 2006-09-22
    body: "Hmmm, and how else would you suggest to attract women into the community, ignore them or tell them to go away???\n\nJohn."
    author: "Odysseus"
  - subject: "Elizabeth Rocks"
    date: 2006-09-22
    body: "I was brought into the Linux world thanks to Elizabeth. She answered all my ignorant questions in the beginning, and is still there to support me if I ever need her help again.\n\nI've heard about the hurdles women have to go through to become involved in OSS, but I never knew it was this bad! While programming and debugging are the meat and potatoes of OSS development, artwork is the cream on the cake. There's also an incredible need for documentation on numerous projects, since their documentation tends to be a few versions too old. Without documentation, how will Linux newbies help themselves?\n\nWomen are truly an invaluable resource to OSS. I think with the help of women, usability in Linux programs and window managers can reach a whole new level of simplicity and ease-of-use."
    author: "Nintendud"
  - subject: "Re: Elizabeth Rocks"
    date: 2006-09-22
    body: "I would think people in general are an invaluable resource to OSS.  It's breaking up the genders in any discussion like this that is part of the issue.  Women are of course part of the community, and should be treated no differently.\n\nThis soap box is uncomfortable."
    author: "Shaman"
  - subject: "Re: Elizabeth Rocks"
    date: 2006-09-22
    body: "I wasn't aware that women in FOSS had a particular interest in stuff like artwork.  (Is this really true, or is it just another stereotype?)  If it is true, then it's yet another reason to encourage more women in FOSS communities.  A few times now, I've tried to convince artists and writers to contribute to FOSS, and seen them not interested, despite it being a great way to get their work known, to do something will all the art and music work they have gathering dust, etc.  More writers, artists, musicians, vocalists etc. would be very welcome."
    author: "Lee"
  - subject: "Re: Elizabeth Rocks"
    date: 2006-09-22
    body: "It's difficult to approach this topic without bumping into stereotypes. Through my involvement with LinuxChix I know a lot of really brilliant women who are into Networking, Programming, all things technical. Certainly the first women getting into contributing to F/OSS were heavily technical because it took someone heavily technical to get into Linux at all.\n\nHowever, my involvement with Ubuntu and Ubuntu-Women has made me encounter many more women who are doing other things in the community - a majority of which takes the form of artwork, documentation writing, etc. Through further outreach I have confirmed that many women getting involved in all F/OSS are getting involved through these avenues.\n\nAnd just as a side note, I have to say that Ubuntu is a great example of a project that puts a high value on all contributions, and I believe this could be part of their reason for success."
    author: "EK"
  - subject: "indeed..."
    date: 2006-09-22
    body: "...girls are really needed and don't forget the linux babes :)\nhttp://images.google.com/images?q=linux%20babe"
    author: "Patcito"
  - subject: "Re: indeed..."
    date: 2006-09-22
    body: "Oh.. and the comments were going so well.  We were all tiptoeing very carefully and you had to ruin it ;)"
    author: "Leo S"
  - subject: "Interview truncate"
    date: 2006-09-22
    body: "Sorry, but i feel as if the interview were truncate.\nit seems incomplete. =(\n"
    author: "martin ponce"
  - subject: "Gender-blind fiance"
    date: 2006-09-22
    body: "> I've met plenty of fantastic people through F/OSS who don't care at all what my gender is. My fiance is one of these people, he's been fantastic.\n\nI have a slight suspicion that he at least does care what gender you are :-)"
    author: "David Fraser"
  - subject: "Re: Gender-blind fiance"
    date: 2006-09-22
    body: "no, think logically:\n\nthe fiance is just bisexual"
    author: "anonymous-from-linux.org.ru"
  - subject: "Re: Gender-blind fiance"
    date: 2006-09-22
    body: "I am not bi-sexual."
    author: "Michael Bevilacqua"
  - subject: "Re: Gender-blind fiance"
    date: 2006-09-22
    body: "David-\n\nGood job on taking that out of context and creating flame bait. That's two mistakes in one post. It takes someone special to fail that well.\n\nLet me clarify for you where you failed to understand. When she speaks of 'my caring which gender' she speaks of gender-equality and not which gender she is sexually.\n\n-Michael"
    author: "Michael Bevilacqua"
  - subject: "Re: Gender-blind fiance"
    date: 2006-09-22
    body: "It's not flame bait.  It's a revolutionary internet concept known as \"joke\". \nHe even used the proper markup \":-)\" as specified here: http://en.wikipedia.org/wiki/Emoticon\n\nA bit too obvious for my taste, though.\n\n\n> When she speaks of 'my caring which gender' she \n> speaks of gender-equality and not which gender she is sexually. \n\nI don't think a single person has *really* misunderstood that. \n\n"
    author: "cm"
  - subject: "Re: Gender-blind fiance"
    date: 2006-09-22
    body: "Right, because Wikipedia is THE repository for information and is totally correct in every fashion and with every article.  The joke was made in poor taste, regardless of emoticon use or not."
    author: "DarkSol"
  - subject: "Re: Gender-blind fiance"
    date: 2006-09-23
    body: "> Right, because Wikipedia is THE repository for information and is totally \n> correct in every fashion and with every article.\n\nWow. You've really outdone yourself with that comment.  \n\nFirst I wanted to make up a non-existing RFC number but \nthen I decided to go for a real link. I'm so sorry I didn't use the \ncorrect markup to tell even you that that part was meant\ntongue-in-cheek.\n\nOh, sometimes the dot just sucks. \n\n"
    author: "cm"
  - subject: "Re: Gender-blind fiance"
    date: 2006-09-25
    body: "well, I for one thought it was funny.\nand (going a little OT) I'm getting kinda tired of the attention KDE women are getting lately - it just seems to be provoking excessive political-correctness and making everyone feel awkward. I haven't found many problems related to my gender, and for the problems that do exist.. well, this seems to be making them worse, not better. I'm not really sure what *would* make them better, though. but the one thing I hate the most is when guys are afraid to make any jokes because there's a woman in the room. I've had to yell at friends in RL about that enough already."
    author: "Chani"
  - subject: "Re: Gender-blind fiance"
    date: 2006-09-23
    body: "Good point!"
    author: "martin ponce"
  - subject: "Woman make great contributions!"
    date: 2006-09-22
    body: "I just like to emphasize that woman make great contributions to the OSS community. \n\nI see a the biggest difference in documentation or articles. Where most men get stuck in technical brabble womans manage to make articles pleasant to read, and more directed to the actual reader. Jes Halls' articles about \"this week in SVN\" or \"what's new in KDE\" are a prime example for this. They are sparkled with excitement, things that interest the end-user, and give you a warm and fuzzy fealing. So keep up the good work!"
    author: "Diederik"
  - subject: "Documentation and Artwork for women?"
    date: 2006-09-22
    body: "What's that? More stupid prejudice?\nI know a hell of a lot of women who are great coders and analysts. \n\nSo I really think it's counterproductive to request the ecological niche for women to be _restricted_ to painting and documentation.\nThese are typically so-called \"softer\" skills than coding and thus saying these are more approbriate for women is to equal women to be soft.\n\nWhich is not a fact but a prejudice.\n\nWhile it is true that there needs to be fought for more room for women in F/OSS (and technique in general!), it would be even harder to get out of the artwork corner once they established it: The \"soft\"-prejudices that were exagerated by the choice of restricting to artwork, would have to be pulled down again.\n\nSo perhaps it's better to go straight for the *real* thing than taking an intermediate step via \"artwork\": fighting prejudice is hard enough, there's no need to help exagerating them before fighting them.\n\n"
    author: "TunaTom"
  - subject: "Re: Documentation and Artwork for women?"
    date: 2006-09-22
    body: "You're expressing something I've seen floating in the air for a long time. I'll be happy the day there'll be 50% girls into core programming. Not only in KDE, but in general. Among the linux kernel hackers, how many are girls? Same question about the kde-core-devel hackers? That should be 50% of the total, period. There's no big difference (if any at all) between a girl's brain and a boy's brain.\n\nI wouldn't go as far as you, though, on artwork: this is a highly creative domain to work in. But it's true that documentation, if very useful, isn't too creative, and girls deserve more that doing the tedious work that the male coders didn't want to do.\n\nNow I hope I didn't offend anyone :/ I mean among the documentation people. You're doing a very useful job."
    author: "Benoit Jacob"
  - subject: "Re: Documentation and Artwork for women?"
    date: 2006-09-22
    body: "esp docs _for_ hackers"
    author: "anonymous-from-linux.org.ru"
  - subject: "Re: Documentation and Artwork for women?"
    date: 2006-09-22
    body: "Hi Benoit.\n\n> I wouldn't go as far as you, though, on artwork: this is a highly creative\n> domain to work in. But it's true that documentation, if very useful, isn't\n> too creative, and girls deserve more that doing the tedious work that the\n> male coders didn't want to do.\n \nI didn't mean to belittle the work or creative act of creating artwork AND documentation (everyone having written a proper manual knows it _is_ creative).\nIt's just that these are generally considered to be soft skills. Nothing wrong with that yet. \nBut jumping to the conclusion that _since_ these are soft skills, they are better suited for women is a prejudice.\n\nImagine being a comp sci major with mad programming skillz (and rather poor painting skills). Now just because your nick is \"Jane\" people start to point you to the painter's chat.\nIt's not that painter's chats are boring or so, but perhaps you'd like to put your mad programming skillz to work instead of the poor painting.\n\nThe logic implications:\nwoman => artist\nman => coder\n\nare just plain wrong!\n\nI don't think we really disagree on that point, it's just that I didn't make myself clear in the first post.\nBeside that: FULL ACK ;)"
    author: "TunaTom"
  - subject: "Re: Documentation and Artwork for women?"
    date: 2006-09-22
    body: "Yeah, I agree with you. Btw, good to hear that documentation can be creative.\n"
    author: "Benoit Jacob"
  - subject: "Re: No brain differences."
    date: 2006-09-22
    body: "A lot of scientists will disagree with that.  There are plenty of differences that can be measured.  That's not to say women can't do everything men can.  There are a lot of studies about women in the sciences, as they are under-represented in more than just Computer Sciences.  Not all the facts are known; they may never be.  We just need to refrain from negative and harmful generalizations and attitudes.\n\nBTW, I think one of these generalizations is that Art is a 'soft' skill.  I have great respect for artists.  I'm a decent programmer, but I have zero artistic talent.  Thanks to all the artists who've made my PC more than a command prompt!"
    author: "Louis"
  - subject: "Re: Documentation and Artwork for women?"
    date: 2006-09-23
    body: "If you are looking for a representation of women within in any sector that mirrors the 52:48 f/m split in the human race then you really need to say 52% of all kernel coders should be women and that split should run the whole way through almost everything we humans do whether it's coding, changine nappies or whatever.\n\nHowever in reality we all know that isn't happening.\n\nI'm not surprised that Elizabeth suggests women dominate in art and documentation.  The more 'macho' or 'hardcore' an area is, the fewer females there are that have the skills to be able to do the job.  This social engineering turning women away from technical subjects at school is not the only problem and we now have far more opportunities to skill ourselves up for the work.\n\nMy experience of being 'high up' in an engineering trade is the sheer amount of sexist behaviour that can be used as a competitive tool to undermine and degrade a female and any work done by her.  While we only have one dork who's posted a sad link here today, any woman working in a 'non-traditional trade or profession' can expect that and much more like it every working day of her life.  Not just from one person but from many and it may even be institutionalised and supported by those that should be preventing it.  \n   \nWhile there are attempts to prevent this, we are fooling ourselves if we think it's been sorted.\n\nYou have to be very resilient and determined to do what you do when you are harassed every working day of your life for doing what you do.  Not many people can keep that up for extended periods of time and you have to have a very good support team in your life outside your work coupled to a great deal of self-confidence and self-belief to cope with it.\n"
    author: "LesleyB"
  - subject: "Untitled"
    date: 2006-09-22
    body: "> Perhaps my first negative experience I was when I was shopping for the first new computer I was paying for on my own. The salesmen consistently spoke down to me, but not to some male I happened to be shopping with, one even tried to persuade me to buy a less powerful computer because I \"really don't need that much RAM\"! I lost track of how many times similar scenarios have happened since then, I still prefer shopping online almost exclusively for this reason.\n\nIf you buy online almost exclusively just because the salesmen spoke down to you, wouldn't it be a little as if they had won? Also, how are they going to get used to women buying computers if you buy them online? How are we going to get used to women having opinions and expressing them if you hide under gender-neutral nicks?"
    author: "josemanuel"
  - subject: "Re: Untitled"
    date: 2006-09-22
    body: "Come on, I see your point, but honestly, if I were a girl I might also use a gender-neutral nick... that's just easier.\n\n/me just got an idea >) ... I'm going to join #kde-devel as QtGirl ... that could be fun :D. Join to see what happens !\n\nBenoit"
    author: "Benoit Jacob"
  - subject: "Re: Untitled"
    date: 2006-09-22
    body: "OK, joined #kde and #kde-devel as QtGirl_ "
    author: "Benoit Jacob"
  - subject: "Re: Untitled"
    date: 2006-09-22
    body: "My point was only to make clear that the presence of women in forums and computer stores should be something perfectly normal. But if women are not present (and that means visible) in forums and computer stores, your joining #kde-devel as QtGirl will yield a number of marriage proposals as a result.\n"
    author: "josemanuel"
  - subject: "Re: Untitled"
    date: 2006-09-22
    body: "Yeah, right, that makes sense. Though, I'm rather disappointed: I've not yet gotten any marriage proposal :( I should stay a bit longer I guess. These big boys are just so shy !"
    author: "Benoit Jacob"
  - subject: "A interesting idea I read about once..."
    date: 2006-09-22
    body: "I remember reading somewhere that a professor in a gender issues class (or something like that) got his male students to go participate in an internet chatroom and then let it slip after a bit that they were female.\n\nI understand what followed was quite an enlightening experience for a lot of folks and left them less than impressed with some members of their gender.  Nothing like walking a mile in someone's shoes...  : )"
    author: "Tyson"
  - subject: "Re: A interesting idea I read about once..."
    date: 2006-09-22
    body: "Isn't what that professor did called \"preaching to the converted\"? If those students had acted like assholes themselves prior to the experiment, it wouldn't have been such an enlightening experience, right?\n\nAlso, I've received insults, sex proposals, marriage proposals, etc., too, and I am a man. (And they were, supposedly, women.) So, it's not a matter of gender, but of how some people act in certain situations. Not all men act the same, neither do all women, and, most certainly, neither do all salesmen."
    author: "josemanuel"
  - subject: "Come on!"
    date: 2006-09-22
    body: "The only diffrence between men and females is the fact that, while most men can easily work without being constantly supported, some women only think about the social issue. AKA \"Why learn this and that if I am not admired and worshipped for it?\" COME ON! \nYes, I am a girl, and I find this interview offending to women in general. From my experience with computers so far, I have not been once discriminated for my gender. Why? Because when I make a mistake, I want to learn. I don't start an hysterical cry if I don't know something and someone points that out in my face, but in stead, I try to learn it myself. Why would I need a community to constantly push me from behind, when I can walk myself? And all women can do so.The thing anyone should teach women is not to think like a woman!! When one learns something, it should be because of pure curiosity and desire to know, not social reasons!\nConclusion: if you are interested only in the social aspect of programming, regardless if you are a male or female, just quit it! Being a geek/programmer/hacker is a lonely world, and not everyone can do it. If you are interested only in the technical aspect, and you are good at it, your gender won't matter. And don't take advice from someone who does mostly just howto's and tech support."
    author: "Valk"
  - subject: "Re: Come on!"
    date: 2006-09-22
    body: "For each, it's a different experience.\n\n>I have not been once discriminated for my gender. Why? Because when I make a mistake, I want to learn.\n\nThe two do not necessarily follow. Your 'why' is erronous. Sure, you may stave off some discrimination by working extra hard, but that point's already been made.\nI say you must be lucky, to have avoided running into discrimination before having the chance to prove yourself.\nCertainly, being a geek/programer/hacker can be lonely, but it need not be so. Simply because you accept such a position for yourself does not mean it is the same for others.\nThere are people who cooperate, work together and share ideas in this world. Each person does what they can, in the method they prefer of those they find available.\n\nyes, Elizabeth writes how-tos and does tech support. This does not make her contributions any less valid. She takes the time to push through, figure out how things work. The how-tos are written from her personal experience in making things work. From what I can tell, she does this because she likes to, because she WANTS to.\n\nTo conlude: Don't put her down simply because her experiences do not match your own. She has worked hard to get to where she is in the community. Each person has their own way of getting things done.\n\nOh: As for your \"walk myself\" line above, I prefer this variation: \"Why walk alone if I can have company?\"\n"
    author: "Peacimowen"
  - subject: "Re: Come on!"
    date: 2006-09-23
    body: "Because sometimes there is nobody near to you.\nOr perhaps nobody can hears (understands) you.\nWhile you search for answers at the middle of the night.\n"
    author: "martin ponce"
  - subject: "Re: Come on!"
    date: 2006-09-23
    body: "As a female who's worked as a professional software and loudspeaker engineer I am happy to let you in on something ....\n\nIt's all about teamwork. You don't work alone - you work in a team.\n\nNo one person does all the analysis and coding in a project - unless it's HelloWorld.c\n\nAnd I object very strongly to your allegation that other women go around being hysterical because their code don't compile first time round or because they don't know something.  Women are human beings and yes there are times I get totally pissed off that something isn't working.  I may display that anger and I may even swear.  At that point in time, if you were to call me hysterical I'd probably feel very very angry toward you.  I will not allow anyone to denigrate any emotion I might experience or display.  The bug may be a flaw in comprehension but there are flaws in compilers too and until your work comes up against one of those and you bang your head against it, trying to understand where you've got it wrong, you haven't experienced anything to piss you off.\n\nAs far as your claim that you have been treated completely asexually well .... I don't know about that.  My experience and the experience of a lot of women is that there are people out there who use your sexuality to harass you and attack you, both physically and verbally.  That's in the real world, not some irc channel.  That you haven't experienced it probably means you aren't even in the food chain yet.\n\nThere are plenty of women with PhD's out there, in traditional and 'non-traditional' roles.  To get a PhD you have to make a contribution to human knowledge and you don't do that by screaming hysterically cos you don't know something.\n\nLastly, the job isn't finished til the paperwork is done.  Coding is useless without documentation."
    author: "LesleyB"
  - subject: "for women only ?"
    date: 2006-09-23
    body: "I really sorry, i found this interview very sexist.\n\n> There are more women working with Artwork, Translations and Documentation writing than Programming and Software Packaging, but often these skills are taken to not be \"worth\" as much.\n\nBy who these skills are not taken be \"worth\" ?\n\nTake it simple, people search for what they perceive interesting,\nand nobody says you, what is \"worth and interesting\" for you.\n"
    author: "martin ponce"
  - subject: "gender bias all over the place"
    date: 2006-09-23
    body: "Most of the opinions I've ever read on the whole gender bias thing in ANY field seem to get bogged down in the question of whether or not the minority has to be specially treated in some way.\n\nI'm a heterosexual man who was a nurse for 15 years.  I can't tell you how many cross- shift reports I've sat though where the female nurses went on and on about their menstrual difficulties until one of them remembered I was there.  She would say, \"Oh, we shouldn't be talking about that, should we?\"  Then change topics to their fantastic new bra. \n\nGender bias happens on both sides of the fence and on both sides it's purely a display of personal disrespect to the minority person.  Every example EK cited in her interview was somebody who didn't respect EK enough to step outside their own bubble of self importance to meet her in at least neutral territory, as it were.\n\nMy own fantasy about how to right this sort of wrong, be it involving women in the computer field, men in nursing or gays in the military, is that simple respect for another human being becomes universally accepted as the default mode of social operation.\n\nJust think- \"if everybody [involved with computers] would just\" adopt personal respect as their modus operandi, we geeks could evolve the reputation of being, not only the smartest subculture on the planet, but also the most enlightened!  From the sounds of some of the responses to this interview, we're well on the way already.\n"
    author: "arawn"
  - subject: "Re: gender bias all over the place"
    date: 2006-09-24
    body: "Came on !!! WAKE UP !! And of course I DISAGREE.\n\nI read every day Linuxtoday and after +2 years i never have\nread NOTHING about mencal !!\nhttp://kyberdigi.cz/projects/mencal/english.html#download\n\nbtw, i have a daughter that love music, khangman and ktouch \n( thanks again Anmarie ) and who sometimes ( also loves ) crack my PC.\n\nAnd until today sometimes i am afraid of talk about some themes !!\n\nI think that the women have a GREAT CHANCE to talk about some themes (items)\nthat REALLY MATTERS to women ( hacking and coding included ).\n\nAnd i am disappointed. I Really want to hear about some interesting \nhacking from all that beauty women that are proud members of this comunity.\n\nPlease, don't get me wrong. I love you all.\n=)"
    author: "martin ponce"
  - subject: "Will you marry me?"
    date: 2006-09-26
    body: "Comn I don't want to be a virgin forever. :("
    author: "Nerd"
---
As women become more involved with open source communities, it's important that their voices be heard. The dot is beginning a new series of interviews with women who contribute to F/OSS. Our first interviewee is Elizabeth Krumbach, who is the coordinator for the Philadelphia area <a href="http://www.linuxchix.org">LinuxChix</a> chapter. Read on to find out how she became involved with computers, why she likes to buy equipment online, and her advice for women contemplating involved in open source communities.



<!--break-->
<br />
<strong>Please introduce yourself.</strong>
<p>
<strong>EK:</strong> My name is Elizabeth Krumbach, I've been using Linux since 2002. Currently I
use Debian and Ubuntu on my systems, but have a great deal of experience with
Red Hat and Gentoo. I have worked with <a href="http://women.debian.org">Debian-Women</a>
and <a href="http://ubuntu-women.org">Ubuntu-Women</a> in
an effort to recruit more women into the F/OSS community. I coordinate the
<a href="http://princessleia.com/phillychix/">Philadelphia area</a> Chapter of LinuxChix,
the <a href="http://montcolug.org">Montgomery County Linux Users Group</a> and
am a regular attendee at the <a href="http://phillylinux.org">Philadelphia Area Linux Users
Group</a>. I also write for <a href="http://www.oreillynet.com/linux/blog/">O'Reilly's Linux Blog</a>.
</p>
<strong>When did you first become interested in technology or computers?</strong>
<p>
<strong>EK:</strong> I've been interested in computers since the first PC came into our house in
1991; I was 10 years old. This PC was an old IBM 8086 that my uncle took out
of his basement to give us. It was sort of a hobby of mine while I went
through middle school and into high school, I'd save up money to buy computers
to tinker with. But I never really got serious about it (or even considered
computers as a career) until I got online when I was 17.
</p>

<strong>Did you have any friends or family as role models or mentors at that time?</strong>
<p>
<strong>EK:</strong> No. The combination of being a quiet kid and not taking my playing with
computers seriously caused me never to encounter other people who might share
my interests. It didn't matter to me at the time anyway.
</p>

<strong>Have you ever felt discouraged by others or frustrated by technical
hobbies or interests?</strong>
<p>
<strong>EK:</strong> Not directly, but there has been plenty of less direct discouragement along
the way, which I think can be more harmful.
</p>
<p>
Perhaps my first negative experience I was when I was shopping for the first
new computer I was paying for on my own. The salesmen consistently spoke down
to me, but not to some male I happened to be shopping with, one even tried to
persuade me to buy a less powerful computer because I "really don't need that
much RAM"! I lost track of how many times similar scenarios have happened
since then, I still prefer shopping online almost exclusively for this reason.
</p>
<p>
The F/OSS community online has been the primary source of discouragement
though. Many of the F/OSS forums out there are male-dominated, and women who
get involved in this world are often not treated equally. It might not be
direct insults or insults at all, but I know many women who have taken to
using gender-neutral pseudonyms in order to dodge snide comments and marriage
proposals that inevitably get directed their way when it's discovered they're
a woman doing something technical.
</p>
<p>
To be fair, LinuxChix and Women groups within the F/OSS community online have
been the primary source of encouragement. And it really is a small, vocal
minority in most F/OSS communities that make things uncomfortable, I've met
plenty of fantastic people through F/OSS who don't care at all what my gender
is. My fiance is one of these people, he's been fantastic.
</p>
<p>
"Real Life" F/OSS gatherings can be tough at times too. For example, I don't
like going down to the city alone, so I call up a male friend of mine who will
drive me down to LUG meetings. When I first started going I got the impression
that if I don't speak up during discussion with something smart and techie to
say, or don't actively detach myself from this male friend, it's often assumed
that I'm "just there as a girlfriend." Imagine having to prove yourself each
time you go out to such an event just to get an equal standing with your
fellow geeks: it's tiring.
</p>
<p>
These examples might seem trivial, but it adds up. I've heard the whole "just
get over it and grow a thicker skin" speech a thousand times, but why would I
spend my free time volunteering in a community where I don't feel comfortable?
I don't feel we should have to change ourselves by "growing a thicker skin" so
we can offer help.
</p>
<p>
In my case I did end up growing that thicker skin and toughing it out, but I
know several women who gave up on working with computers and F/OSS entirely
because they weren't willing to do this, I can sympathize.
</p>
<p>
<strong>How did you become more involved in F/OSS?  How have you contributed
to projects or communities?</strong>
<p>
<strong>EK:</strong> It came pretty naturally to me. I started using Linux, I started writing
How-To articles on my own website when I figured new things out, and then one
day I joined the IRC channel for some F/OSS software I was using. In that
channel they were talking about needing some documentation re-written, I
volunteered and made my first official contribution to a F/OSS project.
</p>
<p>
Since then, I've written documentation for other projects, done some Debian
packaging, done some web development and wiki-based work for projects, and
helped out extensively with IRC-based support for several F/OSS projects.
</p>
<strong>What is the goal of LinuxChix?  What role do you play in the
LinuxChix organization?</strong>
<p>
<strong>EK:</strong> The founder of LinuxChix, Deb Richardson, said she founded it for two reasons:
</p>
<ul>
<li>She "thought it would be fun."</li>
<li>"<a href="http://www.linuxchix.org/content/docs/faqs/#id2851109">
To give women</a> who use Linux a comfortable environment in which to discuss
the OS they love; to create a community that encourages and helps new users;
to make others realize that the vocal minority does not necessarily represent
the Linux community in general."</li>
</ul>
<p>
As for the goals, I believe LinuxChix has accomplished both these founding
reasons and now LinuxChix is really what you make it to be. For me, finding
the LinuxChix network of friendly, intelligent, and supportive women who
shared my interests was transformational. I felt quite alone as a woman in
F/OSS before I learned of LinuxChix, and suddenly that wasn't the case
anymore. In addition to friends, I found mentors and people who I could look
up to, and people who had shared experiences of discouragement and frustration
working and volunteering in tech.
</p>
<p>
Currently I'm the coordinator for the Philadelphia area LinuxChix
chapter.
</p>
<strong>What are your goals going forward with LinuxChix and F/OSS?</strong>
<p>

<strong>EK:</strong> In my chapter coordinator role for LinuxChix I've found my goals to be
generally more social. It's just fun to get together with women who share my
interests and to encourage other women who are interested in getting into
Linux and/or furthering their knowledge.
</p>
<p>
As for F/OSS, I've been working with Ubuntu-Women these past few months to get
more women interested in contributing to Ubuntu. I really hope that someday
there won't be any sexism or "boys club" feel to get over in order to get
involved with F/OSS, and that anyone who can offer help with anything will
feel comfortable doing so and feel that their contribution is valued.
</p>

<strong>Do the
computer hardware and software industries have issues due to
programmers/developers being mostly male while consumers are more gender
balanced?</strong>
<p>
<strong>EK:</strong> I'm not sure I'd go this far. There is a tendency among less developed
software products to have a feel that's more geared toward developers than
users, but I wouldn't say this was a gender problem.
</p>
<p>
I think some hardware manufacturers target certain things toward young men
though, I remember the first 3D accelerated graphics card I got had a picture
of a hot cartoony/digital woman with green hair on the box, but this is more
of a marketing issue.
</p>

<strong>What advice do you have for girls interested in technology, computers,
 F/oSS?</strong>
<p>
<strong>EK:</strong> Hang in there, you're not alone.
</p>
<p>
And always remember that you're doing this because you enjoy it. If you
encounter a community that is too hostile for you to contribute to either
speak up to the proper project authorities or walk away. As much as I want to
see more women contributing, I've found that it's better to admit defeat in
one project than to get burnt out and walk away from all of it. There are
projects out there that will value your contributions.
</p>
<p>
You might also want to join LinuxChix, we're a great bunch and have always
been very supportive of each other.
</p>
<p>

<strong>What advice do you have for communities such as KDE about female
 participation and gender balance?</strong>
<p>
<strong>EK:</strong> Reach out to women specifically. I believe that by recruiting more women now
we open ourselves to a future where women coming into these communities will
notice and feel comfortable with becoming involved.
</p>
<p>
And work to value all contributions to F/OSS equally. There are more women
working with Artwork, Translations and Documentation writing than Programming
and Software Packaging, but often these skills are taken to not be "worth" as
much. Although I don't believe people should contribute to F/OSS to be recognized, it is important to make it clear that work done in all areas of a project is important and valuable. After all, a big hurdle F/OSS has to overcome is becoming more user-friendly, and things like Artwork, Translations, and Documentation are important.</p>

