---
title: "KOffice 1.6 RC 1 Released"
date:    2006-09-28
authors:
  - "iwallin"
slug:    koffice-16-rc-1-released
comments:
  - subject: "Krita"
    date: 2006-09-28
    body: "The only Koffice apps I'm curently using is Krita. In 1.5 it crashed often, but now, even in the beta, it's rock stable and it's starting to get the features I need to use in the place of Gimp.\nBut it will probally roick on KDE4, because current qt miss a lot of features needed, and will bring others that will make it even simple to use draw functions. So, the future is bright for Krita, I hope the other Koffice apps get the same attention level soon."
    author: "Iuri Fiedoruk"
  - subject: "OT: switch user?"
    date: 2006-09-28
    body: "Hi, I google for switch user in KDE.  I find http://docs.kde.org/userguide/switching-sessions.html\n\nDoc tells me I can do it.  OK.  I see Switch User in menu but when I go there only option is grayed out.  How do I enable it please?  I look at document above and don't see.\n\nThank You."
    author: "KDE User"
  - subject: "Re: OT: switch user?"
    date: 2006-09-28
    body: "This is hardly the pace for such questions: I invite you to use the appropriate place: http://www.kde-forum.org/. Thank you :)\n"
    author: "Cyrille Berger"
  - subject: "Re: OT: switch user?"
    date: 2006-09-29
    body: "I have the same problem on Mandrake. :("
    author: "ac"
  - subject: "Re: OT: switch user?"
    date: 2006-10-01
    body: "KDE is Open Source.  If you don't like it, please submit a patch.  Don't whine like a luser."
    author: "Larter"
  - subject: "Re: OT: switch user?"
    date: 2006-10-01
    body: "Go away, troll.\n\n"
    author: "cm"
  - subject: "Klik"
    date: 2006-09-28
    body: "Would it be possible to provide Klik packages of the release candidate? Klik was made for this kind of software testing, and it's just awesome!"
    author: "Klik fan"
  - subject: "Re: Klik"
    date: 2006-09-28
    body: "It should be easy enough to do -- the recipe should still be available from 1.5, and if Isaac Clerencia makes Debian binaries, kliks would almost automatically follow. However, Isaac is pretty busy right now, so I think there are no debs, and someone must update the klik recipe and tell the KOffice release dude. \n\nIt just needs a little more manpower than we've got right now..."
    author: "Boudewijn Rempt"
  - subject: "Re: Klik"
    date: 2006-09-29
    body: "Boudewijn, thank you for your reply. Can you say a little more about what it takes to create a Klik package? With all the great Debian users around, maybe someone will get involved (keeping fingers crossed...)"
    author: "Klik fan"
  - subject: "Re: Klik"
    date: 2006-09-29
    body: "Probono is the official recipe maintainer -- if you contact him http://koffice.klik.atekon.de/recipe/ and offer help with creating a recipe for koffice 1.6 beta's, I've no doubt that business will result."
    author: "Boudewijn Rempt"
  - subject: "And OpenDocument compatibility?"
    date: 2006-09-29
    body: "I've read the changelog, and I haven't seen anything about OD compatibility. I have tried koffice 1.5.2 a bit, and there are errors opening documents created with OO.org. I'd love to see koffice doing what I need to do with OO.org, and yes, I'll check what is needed to submit bugs about this."
    author: "Alberto"
  - subject: "Re: And OpenDocument compatibility?"
    date: 2006-09-29
    body: "And vice-versa. Neither OO nor KOffice implements ODF completely correctly, or even completely. In 1.6, there are a few ODF-related bug-fixes, but the main effort for 1.6 has been expended on features for Krita and Kexi, while the maintainers of the other applications have been busy porting KOffice to KDE4 and Qt4.\n\nThe porting is now done, more or less, and a lot of effort is now going into making the flake vision described in http://dot.kde.org/1149518002/. It looks like we're going to have an ODF hackathon early 2007 -- we'll probably do a sponsorship drive for that some time soon."
    author: "Boudewijn Rempt"
  - subject: "Re: And OpenDocument compatibility?"
    date: 2006-09-29
    body: "This changelog contains only changes from 1.6beta, so if you want to look at new features you should take a look at 1.6beta changelog: http://www.koffice.org/announcements/changelog-1.6beta1.php\n\nYou can see improved OpenDocument support in KSpread and KFormula."
    author: "Alfredo Beaumont"
  - subject: "Re: And OpenDocument compatibility?"
    date: 2006-09-29
    body: "And KChart...  Working on that as we speak in the hacker labs at aKademy."
    author: "Inge Wallin"
  - subject: "Thanks"
    date: 2006-10-06
    body: "I just wanted to thank every KDE dev, and especialy Krita ones who make a exelent work.\n\nFor the other apps, I'm looking forward to them having full support of ODF.\nI agree to say that Koffice is becoming realy nice:\nI'm now using the 1.6 beta, and it is more sable, bug free, and featured than the 1.5 realise, sounds good for the final 1.6 !\n\nThanks again"
    author: "Kollum"
  - subject: "Re: Thanks"
    date: 2006-10-06
    body: "And, thank you! for taking the effort to tell us that you like our work -- that really helps with the motivation and all that."
    author: "Boudewijn Rempt"
---
In the middle of the yearly KDE conference <a href="http://conference2006.kde.org/">aKademy</a>, the <a href="http://www.koffice.org">KOffice</a> team
has released the first release candidate of version 1.6. This release
follows the <a href="http://dot.kde.org/1157906170/">earlier beta</a> according to the schedule.




<!--break-->
<p>
This version does not contain any new features, but comprises of a number of bug fixes that were the result of user comments made about the beta 1 version. The team hopes to continue its great dialogue with the users, and is looking forward to the final release on October 15th.
</p>

<p>As usual, you are invited to test it in depth and to report any bugs
via the <a href="http://bugs.kde.org/">KDE bug website</a>.</p>

<p>For more
information, see the <a
href="http://www.koffice.org/releases/1.6-rc1-release.php">download and release notes page</a>, the <a
href="http://www.koffice.org/announcements/announce-1.6-rc1.php">announcement
</a> and the complete <a
href="http://www.koffice.org/announcements/changelog-1.6rc1.php">list
of changes</a>.</p>



