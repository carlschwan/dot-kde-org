---
title: "People Behind KDE: Alexander Neundorf"
date:    2006-03-14
authors:
  - "jriddell"
slug:    people-behind-kde-alexander-neundorf
comments:
  - subject: "Networking"
    date: 2006-03-14
    body: "Is Lisa still up-to-date? Superseded by zeroconf? What's that for anyway?\n\nBack when I tried to access Windows PCs on the LAN (not so long ago.. KDE 3.3?) I always thought I'd need that (because nothing initially worked). Besides, smb:// sometimes didn't work for non-obvious reasons.\nSo lan:/ told me to start LISa first to get \"LAN\" working, and after finally managing that lan:/ redirected me to an empty view of lan://localhost/. Never really understood that completely..\nAlso, configuring Samba for smb:// wasn't that easy to do for a half-clueless noob, so there is was stuck. Some Linux attempt later LAN / smb:// worked (most of the time). So I never touched it again.\n\nI don't know if a fresh installation of Distro/KDE already delivers that for the most part. I wish I've had at least graphical tools to manage protocols, services etc. back then.. and without command line. I think that's a hard requirement for \"normal people\", who don't breathe Linux and know their processes by name.\nFor the future I wish it would just work without configuring most stuff, also covering wireless and dynamic changing networks.  I really hope that's what Solid and KDE 4 can deliver."
    author: "Anon"
  - subject: "Re: Networking"
    date: 2006-03-14
    body: "I agree. I have been using LISA since it first came out and absolutely love it. So I am a bit confused. LISA is about spying on the network and tracking what is going on.  zeroconf is more about listening to broadcasts. Big difference. If LISA is dead, why? It will be years before all the systems are into zeroconf."
    author: "a.c."
  - subject: "Re: Networking"
    date: 2006-03-20
    body: "I have to agree. It really, really frustrating to have all of this potential in a kioslave like smb:// and then find out that juk/amarok/konqueror can't play audio over them for mystrious reasons.\n\nAnyone have a clue how to fix this?\n\nThanks,\nBen"
    author: "Ben"
  - subject: "What's the status of FUSE-KIO?"
    date: 2006-03-14
    body: "KIO-FUSE (http://wiki.kde.org/tiki-index.php?page=KIO+Fuse+Gateway) is a very promising project because it allows non-KDE apps (including legacy and shell utilities) to access remote files set up under KDE as if they were local files. It combines Linux's Filesystem in Userspace (FUSE):\n\nhttp://fuse.sourceforge.net/\n\nand KDE's KIO slaves. Alexander had started the project awhile back, but I'm not sure how it's been progressing..."
    author: "ac"
  - subject: "Re: What's the status of FUSE-KIO?"
    date: 2006-03-14
    body: "For this to be of any use IO Slaves would need seeking support though, at least at  the framework level. There's plenty of IO Slaves that can potentially be made seekable, but the framework support is simply not there.\n\nAny chance of it happening in 4.0?"
    author: "yaac"
  - subject: "Re: What's the status of FUSE-KIO?"
    date: 2006-03-14
    body: "kio_fuse is in kdenonbeta and basically working, also the seek() command is implemented (not very efficently since ioslaves don't have seek, but it works), e.g. gimp, joe, OOo work with it.\n\nThe problem is, I don't do a lot of networking stuff since some time now already, so I don't actually use it.\n\nSo, IOW it needs a new maintainer.\n\nBeing able to seek in ioslaves would be nice, but it would require some work. KDE 4 libs are not finished yet, so if you want to help, you're welcome :-)\n\nAlex\n"
    author: "aleXXX"
  - subject: "Re: What's the status of FUSE-KIO?"
    date: 2006-03-15
    body: "Thanks for your effort, Alex. Can't help unfortunately, too much other stuff to do.\n\nBTW, the KIO seeking bug/wish is http://bugs.kde.org/show_bug.cgi?id=116091, currently with 0 votes. ;)"
    author: "yaac"
  - subject: "cmake, scons"
    date: 2006-03-14
    body: "Is cmake likely to be the build system for KDE4 then?\n\nI have to admit just glancing at the cmake docs that its syntax doesn't appear to be much of an improvement over Makefile.am/configure.in.in. Which isn't a requirement for KDE4's build system, it would just be nice. :)"
    author: "Ian Monroe"
  - subject: "Re: cmake, scons"
    date: 2006-03-14
    body: "A recent post on kde-devel reveals that apparently it has been chosen as the build system for KDE 4."
    author: "Marten Klencke"
  - subject: "Re: cmake, scons"
    date: 2006-03-14
    body: "That would suck. I thought scons/bksys was the chosen one!?"
    author: "Carlo"
  - subject: "Re: cmake, scons"
    date: 2006-03-14
    body: "yes, it was\ni really hope it still is\n[scons is the best tool for the job IMHO]\n"
    author: "ste"
  - subject: "Re: cmake, scons"
    date: 2006-03-14
    body: "SCons is terribly slow and has some portability issues, at least on win32 (maybe that changed lately though, I haven't tried it for some time)\n\nCMake is in itself not a build system but a Makefile generator, so CMake alone still won't compile KDE4. However, because it abstracts over the makefile-generators, it is (in theory) possible to use almost any make tool for building KDE 4. For example make, unsermake, qmake, nmake,...\n\nAdditionally, the Makefile syntax of CMake is much more readable than that of scons (the SConscript files are actually Python scripts)"
    author: "Michael"
  - subject: "Re: cmake, scons"
    date: 2006-03-14
    body: "> SCons is terribly slow and has some portability issues, at least on win32 (maybe that changed lately though, I haven't tried it for some time)\n\nIt is? I found it quite acceptable - even on slower boxes. Regarding portability issues: I don't know (or particularly care about Windows), but that should be fixable.\n\n\n> Additionally, the Makefile syntax of CMake is much more readable than that of scons (the SConscript files are actually Python scripts)\n\nCan't do anything else but disagree."
    author: "Carlo"
  - subject: "Re: cmake, scons"
    date: 2006-03-17
    body: "\u00abI found it quite acceptable - even on slower boxes.\u00bb\n\nBut KDE code is huge! An insignificant hit of performance for some program of yours, even if big, might be huge when you want to compile something of the size of the all KDE.\n\nAnyway, if you really want scons to be used, just jump forward and help making it suitable. Less talk, more code. ;)"
    author: "blacksheep"
  - subject: "Re: cmake, scons"
    date: 2006-03-20
    body: "I have a bit of experience with scons and it's obvious that gcc takes 95-99% of the time in large compiles. The benefits of scons are that its much easier to work with, parallel builds are possible in separate dirs (imagine having N cpus running scons -j 2*N, and it scales without major bottlenecks!), and it has caching of intermediate files for faster rebuilds. \n\n"
    author: "Ben"
  - subject: "Re: cmake, scons"
    date: 2006-03-14
    body: "Do you actually know cmake ? \n\nscons was chosen at akademy, and people started to work on it.\nBut after months of work we ended up basically with a fork of scons.\n\nSo apparently scons wasn't the best tool for the job.\n\nAbout \"it was chosen\": yes, scons was chosen, but KDE still follows the \"who codes decides\" or \"The power of the doer\" rule: I just did after I got the \"Go\" from David on the buildsystem mailing list.\nSo it just happened that right now it seems cmake will be the buildtool for KDE 4.\n\nCMake needed some enhancements mainly for the windows stuff (we are probably the first project which uses mingw heavily). The cmake developers have been very supportive for KDE, they are on the buildsystem mailinglist and fixed the issues we found within days. They want cmake to become the buildsystem for KDE 4.\n\nAlex\n"
    author: "aleXXX"
  - subject: "Re: cmake, scons"
    date: 2006-03-14
    body: "> About \"it was chosen\": yes, scons was chosen, but KDE still follows the \"who codes decides\" or \"The power of the doer\" rule: I just did after I got the \"Go\" from David on the buildsystem mailing list.\n\nThat's true of course.\n\nAs I looked at cmake (a while ago) it had lots of paths hardcoded in its modules files instead relying on some prefix. If you have more than a single (virtual) installation on some box and you want it to use /.../<foo>, but /.../<bar> is found, things get wrong. I also hope that the KDE 4 build system will be better regarding optional dependencies. Autodetection sucks, deterministic en/disabling is needed."
    author: "Carlo"
  - subject: "Re: cmake, scons"
    date: 2006-03-15
    body: "> The cmake developers have been very supportive for KDE, they are on the \n> buildsystem mailinglist and fixed the issues we found within days. They want\n> cmake to become the buildsystem for KDE 4.\n\nWell, this alone is enough reason to choose cmake. Its really nice to have an active \"upstream\"."
    author: "Ian Monroe"
  - subject: "Re: cmake, scons"
    date: 2006-03-15
    body: "> But after months of work we ended up basically with a fork of scons.\n\nI'm curious: why a fork?\nI can understand that some modifications were needed, but did scons's maintainer refused to integrate them?\n\n"
    author: "renox"
  - subject: "Re: cmake, scons"
    date: 2006-03-15
    body: "Not a fork but an experimental branch."
    author: "ita"
  - subject: "Re: cmake, scons"
    date: 2006-03-16
    body: "We had basically no relationship with the scons maintainers. None of our changes were applied upstream.\n\nOne of the reasons, at least, was to maintain compatibility with an old Python version. By dropping that compatibility, we made some progress, but created a fork."
    author: "Thiago Macieira"
---
<a href="http://people.kde.nl/">People Behind KDE</a> is our fortnightly expos&eacute; of the celebrities of KDE.  Tonight we bring you the man who made the first Samba io-slave, an NFS io-slave and from those pre-zeroconf days the Lisa lan-browsing io-slave.  This same man is single handedly porting KDE to a whole new build system, but get back in your seats ladies, he's not up for adoption.  Find out all the gossip in our <a href="http://people.kde.nl/neundorf.html">interview with Alexander Neundorf</a>.



<!--break-->
