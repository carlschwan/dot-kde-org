---
title: "KDE Commit-Digest for 3rd December 2006"
date:    2006-12-04
authors:
  - "dallen"
slug:    kde-commit-digest-3rd-december-2006
comments:
  - subject: "Kde4 Core"
    date: 2006-12-03
    body: "Reading the last Commit-Digests, i have seen nearly no work on kde4-core system.\nPerhaps you can say Akonadi/Strigi ist a kde4 core technology, but it looks like most of the work goes to kdegames/kdeedu which is not my prefered workplace.\n\nIf Konsole/Kate/Konqueror/KHtml/Kio is already done kde4 is already usable.\n\n"
    author: "chri"
  - subject: "Re: Kde4 Core"
    date: 2006-12-03
    body: "no commit doesn't mean no work, (beside no entry in commits digests doesn't mean no commit). It might mean developers are busy designing, or busy on kdelibs, or coding but that they prefer to make big commit rather than a lot of small one. And there is still a lot to do before kde4 is usable."
    author: "Cyrille Berger"
  - subject: "Re: Kde4 Core"
    date: 2006-12-04
    body: "Kate has seen a lot of improvements since its 3.5 days. It might not be done yet, but it's far away from \"nearly no work is done\".\n\nKHTML will likely be superseded by WebCore, which is developed outside KDE's Subversion repository.\n\nKIO worked well in KDE 3.x, any improvements are nice, but there are no obstacles for it to work just as well in KDE 4. Also, Allen Sandfeld's seeking ability for KIO is being done and will add the \"final polish\" to it.\n\nKonqueror is still a TODO item, as far as I know. However, work is done on the Dolphin file manager, which will definitely affect KDE4's Konqueror in terms of better usability.\n\nPhonon is getting a gstreamer-engine sponsored, and is already in place, as well as Solid is.\n\nAlso, the import of Kross into kdelibs is even featured on the digest summary, which is another building block of KDE 4 core technology being now waiting to be used.\n\nOther stuff like Decibel, the KMenu replacement (Raptor) or whatever temporary work branches are not very visible, but being worked on with more or less persistence.\n\nDon't worry, kdelibs has a brighter future than ever before :-D"
    author: "Jakob Petsovits"
  - subject: "Re: Kde4 Core"
    date: 2006-12-04
    body: "> KHTML will likely be superseded by WebCore, which is developed outside KDE's > Subversion repository.\n\nFalse. Possible? Yes. Likely? Heck no.\n\n"
    author: "SadEagle"
  - subject: "Re: Kde4 Core"
    date: 2006-12-04
    body: "Yes, Webkit will be used in Konqueror. See the Unity project to resynchronize KHTML and WebKit: http://dot.kde.org/1152645965/"
    author: "myself"
  - subject: "Re: Kde4 Core"
    date: 2006-12-04
    body: "Hmmm I don't think he needs to read that... do you know who SadEagle is?"
    author: "someone"
  - subject: "Re: Kde4 Core"
    date: 2006-12-04
    body: "Hm, guess I've been misguided then. My last information was that virtually all KHTML developers are working towards Unity now, except one (whom you might actually know ;)\n\nIt would be nice to have a short wrapup on how the html engine issue is doing at the moment. Maybe on the dot, or in the commit digest's editorial section. btw, khtml.info currently spits out a database error."
    author: "Jakob Petsovits"
  - subject: "Re: Kde4 Core"
    date: 2006-12-04
    body: "Your last information is quite clearly false. And may be because the actual khtml developers are more focused on development than making high-profile PR posts.\n"
    author: "SadEagle"
  - subject: "Re: Kde4 Core"
    date: 2006-12-06
    body: "... And then get annoyed when people aren't in the know?\n\n\nI'm not bashing you guys, don't get me wrong. I love you dearly because you make Konqueror work :-) But you can't be annoyed when people don't know something you're not communicating to them."
    author: "Anonymous"
  - subject: "Re: Kde4 Core"
    date: 2006-12-04
    body: "Tons and tons and tons of work is going on with kde4-core!\n\nThe problem is that svn-digest tends to highlight bugs that are fixed.  But no users are submitting bugs for kde4 yet!  And most commits aren't sexy enough by themselves to be highlighted and shown anyway.\n\nsvn-digest shows only a tiny fraction of the work that goes on.\n\n"
    author: "John Tapsell"
  - subject: "Re: Kde4 Core"
    date: 2006-12-04
    body: "When I compiled KDE4 from svn a couple of weeks ago I was really surprised how stable konqueror already was. I surfed the web for half an hour and it didn't crash. I used it to view the contents of my home directory (as thumbnails of course) and it worked fine. Also kate works o.k. for me (I only used it to do some small changes to some text files and it worked for that). There are not many visible changes yet but if you check out KDE4 from svn yourself and if you do a svn up every couple of days you will see that there is lots of work done. "
    author: "Michael Thaler"
  - subject: "Lower the barriers!"
    date: 2006-12-03
    body: "Jos van den Oever wrote about Strigi:\n\"Even though using D-Bus makes it easy to call Strigi from any program, so far, uptake in KDE applications is not what I had hoped it would be. So I have written convenience classes for integrating Strigi into external applications. Developers that want to make calls to Strigi from their application can simply link to a small library that takes care of communication and provides a nice queue which makes querying fast and ensures it does not block the GUI.\"\n\nNow, that's what I call lowering the barriers to usage!  It's exactly the same technique that Phonon is doing to ease the use of   I'd like to applaud Jos for this and ask other developers to think if they should do something similar for their own technology.  For instance, how easy is it to use KDE's spell checking nowadays?  GHNS?"
    author: "Inge Wallin"
  - subject: "Re: Lower the barriers!"
    date: 2006-12-04
    body: "How would app-developers know about that?\nI don't recall a dot story, nor a blog pointing to  HOWTO..."
    author: "AC"
  - subject: "Re: Lower the barriers!"
    date: 2006-12-04
    body: "> how easy is it to use KDE's spell checking nowadays\n\nsee kdelibs/sonnet. from the README:\n\n#include <kspell_loader.h>\n#include <kspell_dictionary.h>\nusing namespace KSpell;\n\n\nLoader::Ptr loader = Loader::openLoader( someKSettingsObject );\nDictionary *enDict = loader->dictionary( \"en_US\" );\nDictionary *deDict = loader->dictionary( \"de_DE\" );\n\nvoid someFunc( const QString& word )\n{\n    if ( enDict->check( word ) ) {\n        kDebug()<<\"Word \\\"\"<<word<<\"\\\" is misspelled.\" <<endl;\n        QStringList suggestion = enDict->suggest( word );*\n        kDebug()<<\"Suggestions: \"<< suggestions <<endl;\n    }\n\n    QStringList suggestions;\n    if ( deDict->checkAndSuggest( word, suggestions ) ) {\n       kDebug()<<\"Wort \\\"\"<<word<<\"\\\" ist fehlbuchstabiert.\" <<endl;\n       kDebug()<<\"Vorschlage: \"<< suggestions <<endl;\n    }\n}\n\ndelete enDict;\ndelete deDict;\n\n\nthere's more info in the README there."
    author: "Aaron J. Seigo"
  - subject: "Kross"
    date: 2006-12-04
    body: "\"Kross, the multi-language application scripting framework, loses its dependency on KOffice and moves into kdelibs as the cornerstone of scripting in KDE 4\"\n\nSeems the question about the \"default\" scripting language in KDE 4 is answered. :) Very cool."
    author: "Daniel \"Suslik\" D."
  - subject: "Re: Kross"
    date: 2006-12-04
    body: "for in-application scripting (aka \"application automation\") perhaps, yes. we still need to work out the non-compiled application dev question."
    author: "Aaron J. Seigo"
  - subject: "Any news on Plasma?"
    date: 2006-12-04
    body: "This is one part of the whole KDE 4 development that seems to be very silent, in terms of \"announcements\" (commits, blogs, etc.). It's also one part that had me really excited in the beginning. I'm really wondering what's happening to it behind the scenes.\n\nJust a few months months (maybe a whole year?) to go. Good luck guys! :-)"
    author: "Jucato"
  - subject: "Re: Any news on Plasma?"
    date: 2006-12-05
    body: "For the little I know (please anybody correct me if I'm wrong), by now plasma is just a concept, a vision on how things should be on the user interface.\nThere isn't even a master plan or sketches, so I belive it won't be a unified effort, but each group of developers (kwin, kicker, konqueror) will do a part of it.\n\nI'm still beting Plasma won't be on KDE 4.0. I've read Aaron Seigo calling for apps developers to start some KDE4 technologies as Phonon, stating that most programs won't use it on KDE4.0, and you see, phonon already have some code on SVN! If developers can't make use of phonon until 4.0, what rests for plasma, that today is still a dream?\n\nNote that I don't see anything wrong if plasma or other technologies make into 4.0, KDE have a great story of great improvements during .X versions. I just wished plasma developers where more talkative about it after so much was told to us about how great plasma would be. (you know, that smell of vaporware is still on the air, I don't like it). :)"
    author: "Iuri Fiedoruk"
  - subject: "Slight link error (KGehography)"
    date: 2006-12-04
    body: "KGeography link points to the KVocTrain page\n\n(sorry for posting twice in a row)"
    author: "Jucato"
  - subject: "Another link error:"
    date: 2006-12-04
    body: "The link http://kross.dipe.org/readme.html\ndoesn't exist"
    author: "Anonymous"
  - subject: "Re: Slight link error (KGehography)"
    date: 2006-12-04
    body: "Fixed both errors, thanks.\n\nDanny"
    author: "Danny Allen"
  - subject: "nitpick"
    date: 2006-12-04
    body: "inotify is not a daemon\n\nif you want a daemon look at fam or garmin (which in turn may use inotify)\n\nfrom the referenced link:\ninotify is a Linux kernel subsystem that..."
    author: "anonymous"
  - subject: "caught my eye"
    date: 2006-12-04
    body: "you know what commit caught my eye? the one by Craig Drummond, when he committed his work on the kde font tool in kcontrol. i just checked it out, and it sure is an improvement. ok, it might not be a 'crucial' part off the desktop, but it's so important to be good on the details as well... thanx, Craig!"
    author: "superstoned"
  - subject: "Re: caught my eye"
    date: 2006-12-04
    body: "This is also the thing I found most important this week. I agree - it's not a crucial part of the desktop but IMHO it's not any crucial part of the desktop that makes my KDE stand out against the evil W.... desktops of my colleagues. It's the many small things. Is it just me or is the gap between those desktops in terms of features widening more and more every day? Font grouping will be one of those features. \n"
    author: "Michael"
  - subject: "thank you"
    date: 2006-12-05
    body: "Thank you Danny for the wonderful digests. They are always a source of great conversation.\n\nAlso, thank you to all the developers consistently working to improve KDE on a daily basis.\n\nLastly, thanks to all the KDE users for creating the demand that pushes the software."
    author: "Vlad"
  - subject: "ogg???"
    date: 2006-12-07
    body: "why not use ogm as an extension for movies...\n\nthen it would at least start my kaffeine player instead of messing up my amarok playlist... ( yeah I know I can right click and select another app but I always forget that the first time...)\n"
    author: "Mark Hannessen"
  - subject: "Re: ogg???"
    date: 2006-12-07
    body: "oops, wrong topic"
    author: "Mark Hannessen"
---
In <a href="http://commit-digest.org/issues/2006-12-03/">this week's KDE Commit-Digest</a>: Substantial work and improvement in the font installation KControl module. Support for <a href="http://en.wikipedia.org/wiki/Opendocument">OpenDocument</a> annotations in <a href="http://okular.org/">Okular</a>. New Interface ideas and consistency work in <a href="http://amarok.kde.org/">Amarok</a>. <a href="http://ktabedit.sourceforge.net/">KTabEdit</a> gets better support for the 'Guitar Pro' file format. Iceland map added to <a href="http://edu.kde.org/kgeography/">KGeography</a>. Work starts on a new keyboard rendering engine in <a href="http://edu.kde.org/ktouch/">KTouch</a>, and on a model/view interface implementation for <a href="http://edu.kde.org/kvoctrain/">KVocTrain</a>. Early work on a <a href="http://phonon.kde.org/">Phonon</a> backend for KsCD. Speed optimisations in <a href="http://www.vandenoever.info/software/strigi/">Strigi</a>, with experimental probing for the feasibility of leveraging the <a href="http://en.wikipedia.org/wiki/Inotify">inotify</a> daemon. Experimental code sees <a href="http://pim.kde.org/akonadi/">Akonadi</a> become searchable through Strigi. <a href="http://kross.dipe.org/">Kross</a>, the multi-language application scripting framework, loses its dependency on <a href="http://koffice.org/">KOffice</a> and moves into kdelibs as the cornerstone of scripting in KDE 4.

<!--break-->
