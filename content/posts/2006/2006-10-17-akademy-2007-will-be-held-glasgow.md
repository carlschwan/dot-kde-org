---
title: "aKademy 2007 Will be Held in Glasgow"
date:    2006-10-17
authors:
  - "jduffus"
slug:    akademy-2007-will-be-held-glasgow
comments:
  - subject: "So..."
    date: 2006-10-17
    body: "Great! I suppose registration will be open until the end of October? :-P\n\nNo, really, Glasgow is cool from my point of view. June 28th is the last day of my summer semester, but let's just assume there's no exam on that date. Oh right, and hopefully Glasgow is less expensive than Dublin ;)"
    author: "Jakob Petsovits"
  - subject: "our laptop"
    date: 2006-10-17
    body: "England place... hum... they recently restricted policy about laptops in plane, and lots of them are stolen during travels. :(\n\nBut it seems to be a very nice place."
    author: "Johann Ollivier Lapeyre"
  - subject: "Re: our laptop"
    date: 2006-10-17
    body: "I forgot to say good work and luck to Kenny Duffus. And to thanks again Marcus for his fantastic job. "
    author: "Johann Ollivier Lapeyre"
  - subject: "Re: our laptop"
    date: 2006-10-17
    body: "Calling Glasgow an \"England place\"???\nDo not make the same mistake while you are there :)\n\nThere is no problems with Laptops, you just have to take it from the case so it can be x-rayed.\n\nJC\n"
    author: "Jose"
  - subject: "Re: our laptop"
    date: 2006-10-17
    body: "Simply go by ship&train :) Oh, than taking english trains is dangerous, less because of all the terrorists, but because of short term ROI oriented investors and foolish politicans... :/"
    author: "Lessy"
  - subject: "Re: our laptop"
    date: 2006-10-17
    body: "You can take electrical items including laptops on planes just fine.\n\nAs someone else has alluded to, don't make the mistake of referring to Scotland as England while you are there. You may not escape with your life ;)\n\nIt's like referring to Belgium as France. They are not the same country.\n\nThey are however part of Great Britain, which Wales is also a member of."
    author: "Chris Howells"
  - subject: "Re: our laptop"
    date: 2006-10-17
    body: "\ns/England/Great Britain\n\nSorry, i know this is not the same culture, separate parlement, the conflict history against england (and separate Rudby or football team...). But i fear the airport policy will be the same (laptop in the compartment of the plane). But maybe i'm wrong"
    author: "Johann Ollivier Lapeyre"
  - subject: "Re: our laptop"
    date: 2006-10-17
    body: "Johann,\n\nI have been flying from Edinburgh for the last 2 weeks ( Copenhagen, Amsterdam and Budapest), and can assure you there is no problems with Laptops, the restrictions were lifted like a month ago...\n\nJC\n"
    author: "Jose"
  - subject: "Re: our laptop"
    date: 2006-10-17
    body: "This is what i wanted to read :D Thanks\n\nWhat about the food now ? And the beer? ;) "
    author: "Johann Ollivier Lapeyre"
  - subject: "Re: our laptop"
    date: 2006-10-17
    body: "Unless you're a real ale drinker the beer offerings aren't very impressive, the UK has a culture of cheap, imported lagers.\n\nThat said, there's a lot of special pubs you can get bizarre things on tap."
    author: "Alistair John Strachan"
  - subject: "Re: our laptop"
    date: 2006-10-19
    body: "I was very impressed with beer in Scotland when I was there. I live in Sweden, though, which has a culture of *very* cheap lagers. ;) \n\nGuinness is especially good in Scotland.. perhaps because it's close to the brewery in Ireland."
    author: "Apollo Creed"
  - subject: "Scotland is a country?!"
    date: 2006-10-17
    body: "From abroad, one sees United Kingdom as a single country led by Tony Blair. Scotland, Wales, England and Northern Ireland are just regions of it.\n\nWell, on Wikipedia, I can read that Scotland, England, etc. are really countries... They are clearly not seen this way from abroad! For instance, the European Community includes 25 countries, not 28!"
    author: "Hobbes"
  - subject: "Re: Scotland is a country?!"
    date: 2006-10-17
    body: "Technically they're nations, unified as a single country. I'm Scottish and I consider myself British foremost, but a lot of Scots have a strong sense of cultural identity and would prefer to be Scottish.\n\nHope this helps clarify things."
    author: "Alistair John Strachan"
  - subject: "Re: Scotland is a country?!"
    date: 2006-10-17
    body: "Identity is strange.\nI'm English but would identify myself as western, or European foremost. I suppose British second.  "
    author: "John Tapsell"
  - subject: "Re: our laptop"
    date: 2006-10-17
    body: "I live in Glasgow and I (and others) get very angry when people refer to Scotland as England.  No wonder over half of Scottish people want independence!"
    author: "Bruce Cowan"
  - subject: "Re: our laptop"
    date: 2006-10-17
    body: "Heh, the scottish are crazy.  "
    author: "Fun fun"
  - subject: "Not at all"
    date: 2006-10-20
    body: "Its about the same as calling you Canadian if you are from the US (or vice versa)."
    author: "The Grum"
  - subject: "Re: our laptop"
    date: 2006-10-17
    body: "Strange. I live in NRW but if people thought this is in the Netherlands I would perhaps correct them but \"very angry\"??? Huh? You should watch your\nblood pressure if such things upset you that much. In fact it _is_ a wonder why all regions want independence. This is an anachronism in an ever more interdependent world IMHO. \n"
    author: "Michael"
  - subject: "Re: our laptop"
    date: 2006-10-17
    body: "Alright, I get a wee bit pissed off then.\n\nScotland, Northern Ireland, England and Wales are all part of the United Kingdom.  Saying that Glasgow is in England is wrong, but it is in the United Kingdom.\n\nIt's kind of like saying a place in Virginia is in West Virginia.\n\nNRW isn't in the Netherlands anyway, and the Netherlands and Germany are not regions of the same country.\n"
    author: "Bruce Cowan"
  - subject: "Re: our laptop"
    date: 2006-10-18
    body: "Well, you would make me angry if you think that I live in Holland.\nI don't live in Holland, I live in The Netherlands. Holland is just a region in The Netherlands, like England is just one of the regions in the UK.\n"
    author: "AC"
  - subject: "Re: our laptop"
    date: 2006-10-18
    body: "But why get angry? This nation thingie is so... oldfashioned at most. It's for leaders to hold their pack together, for making concepts of enemies...\nYou live a culture, not live within a nation."
    author: "Slubs"
  - subject: "Re: our laptop"
    date: 2006-10-18
    body: "Indeed, and i don't live the culture of Holland.\nI'm Frisian :o)\n"
    author: "AC"
  - subject: "Re: our laptop"
    date: 2006-10-19
    body: "aaah, frisian, I will save you the jokes from groningen then :p (I am from there)"
    author: "Mark Hannessen"
  - subject: "Re: our laptop"
    date: 2006-10-18
    body: "Well in some languages we have made this easy by translating Holland and Netherlands to the exactly the same word: Holland ;)"
    author: "Carewolf"
  - subject: "Re: our laptop"
    date: 2006-10-18
    body: "Well, that jus makes things more complicated :o)\n\nFor example, Amsterdam is the kapital of The Netherlands, but not of Holland.\n"
    author: "AC"
  - subject: "Re: our laptop"
    date: 2006-10-18
    body: "Interestingly, most people I've talked to from Holland consider Holland another name for the Netherlands.   You are in the first person I've talked to who doesn't consider them the same.   (Also the first from the Netherlands but not also from Holland)\n\nNote that my sample size is far too small to draw any conclusions.  Also, this topic doesn't come up too often when I meet someone from your country."
    author: "bluGill"
  - subject: "Re: our laptop"
    date: 2006-10-19
    body: "Originally, The Netherlands is a combination of 7 states, including Holland. (that's wy it's called The NetherlandS, in stead of Netherland).\nLike England, Holland is the dominant factor in The Netherlands.\n\nToday, The Netherlands consists of 12 provinces, including North and South Holland.\n\nMost Dutch people are pragmatic, so the say that they are from Holland, in stead of The Netherlands.\nProbably because every foreigner knows about Holland, but not about The Netherlands..\n\n"
    author: "AC"
  - subject: "Re: our laptop"
    date: 2006-10-19
    body: "I am from the netherlands as well, when I am in another country I usually refer to the netherlands, but if they don't know about holland is a pretty good second candidate, amsterdam being the third best ;)"
    author: "Mark Hannessen"
  - subject: "Re: our laptop"
    date: 2006-10-20
    body: "Wel, as long as they don't think that Amsterdam is a city in Germany, it's fine with me :)\n"
    author: "AC"
  - subject: "To help readers"
    date: 2006-10-18
    body: "NRW = North Rhine-Westphalia\nhttp://en.wikipedia.org/wiki/NRW\n\nThis is the first time I see this abbreviation. Even if I visited a family there for two weeks. If anyone is coming with the abbreviation of his region...\n\nOf course it is in Germany."
    author: "Hobbes"
  - subject: "Re: To help readers"
    date: 2006-10-20
    body: "Oh! Sorry. Was writing this in a bit of a hurry. Didnt think it could be unknown to the internet audience here who is living outside (Central) Europe. Over here this is not any regional abbreviation but very well known because\na) it is Germany's most populous federal state\nb) home to one of the largest metropolitan areas in Europe next to London and Paris\nc) it is nearly always referred to as NRW and not the long form, because we are all are a bit lazy, arent we?\n\nSo, sorry for making you to look this up but now you know ;)\nAnd by the way: The wikipedia is always in interesting read! I could dive into this for ages going from page to page once I start.\n\nHave a nice day!\n"
    author: "Michael"
  - subject: "trains"
    date: 2006-10-17
    body: "The area around Glasgow has a fantastic public transport system and a lot of rail links. The easiest way to get to Glasgow would be to fly to Prestwick Airport then get the train to Glasgow Central."
    author: "das_cheesecat"
  - subject: "2007 DebConf in Edinburgh"
    date: 2006-10-17
    body: "<A href=\"http://debconf7.debconf.org\">Debian's 2007 meeting</A> will be in Edinburgh, From 9th to 24th june.\n\nWhat about both events get together and work towards a common schedule,\nor even better to run both events at the same place and time!\n\nOk, ok, i'm already way beyond Earth's orbit :) ,\nbut it (at least at first sight) seems possible, doable and awesome!!\nSo i beg to the Scottish KDE team,\npleeeeeease try as hard as possible to work it out.\nIf not possible, well, i'm sure that Edimburgh has plenty of <A href=\"http://wiki.debian.org/DebConf7/Fun\"> things to get me busy for 4 days. </A> :D"
    author: "Fabricio \"aybabtu\" Cannini"
  - subject: "Re: 2007 DebConf in Edinburgh"
    date: 2006-10-17
    body: "Sorry but i could not get the url thingie to work.\nAnyway, you get the idea. :)\n"
    author: "Fabricio \"aybabtu\" Cannini"
  - subject: "Thanks from US students"
    date: 2006-10-17
    body: "Thank you for considering the students in the US.  I've never been able to go because it always falls during the fall semester.  Assuming I can get the cash, I'll finally be able to make it to one.  Thanks again!\n\nP.S. I'm probably missing something obvious, but where can I get information on how much everything will cost?"
    author: "carl"
  - subject: "Re: Thanks from US students"
    date: 2006-10-20
    body: "If you are actively contributing to KDE you can get assistance. Look at the link at the head of the article for more info on the conference and costs. I found http://airfareplanet.com to have the best deals.\n\nBefore you go getting too grateful for accommodating students realize that travel costs are substantially higher during the summer than in fall. Then get used to having a second class economy. I found out Dublin was something like the third most expensive city in the world. It cost $10 for a warm meal in a fast food place that was larger than what I consider a children's meal. Seriously, I walked out of Supermacs after eating a 3.40 Euro burger to Burger King next door to address my hunger. The 3.40 Euro burger was about the size of a 99 cent Wendys burger. I'm told that Scotland is not far behind in cost and of course they still use pound sterling there. In Heathrow I bought a bagel with cream cheese and bacon and a diet coke for $11. Granted that's an airport but in Chicago O'Hare I got the same with smoked salmon instead for $8.\n\nAlso remember that in the US we have diverse cultural cuisine and in Scotland they have haggis. In Dublin I got a Hawaiian pizza that replaced the Canadian bacon with corn. It really is fun going to aKademy and I love every one of them. There was a lot to like about Dublin, but I advise talking with someone who attends these to get prepared. Don't expect to eat cheap. Don't expect a hostel experience to be like a hotel experience. Do expect to spend time with fun people, soak in KDE technology and see fun places."
    author: "Eric Laffoon"
  - subject: "Re: Thanks from US students"
    date: 2006-10-21
    body: "> Also remember that in the US we have diverse cultural\n> cuisine and in Scotland they have haggis.\n\nHeh.  A man who goes on super-size-seeking door-to-door burger-crawls speaking of 'cultural cuisine'....\n\nIf you go, be sure to ask someone to explain the concept of irony."
    author: "Anon"
  - subject: "EuroPython compatible!"
    date: 2006-10-18
    body: "It's even compatible with EuroPython 2007: Monday 9th July to Wednesday 11th July in Vilnius, Lithuania."
    author: "The Badger"
  - subject: "videos akademy 2006"
    date: 2006-10-18
    body: "I was just wondering whether the videos of Akademy 2006 are already online??"
    author: "Me"
  - subject: "Re: videos akademy 2006"
    date: 2006-10-19
    body: "A few: http://youtube.com/results?search_query=akademy"
    author: "AC"
---
The annual KDE World Summit, <a href="http://akademy2007.kde.org/">aKademy</a>, has found a home for 2007 in the vibrant city of Glasgow. The week-long event for contributors to the leading Free Desktop project will be held from 30th June to 7th July 2007 in Scotland's largest city. Our hosts will be the <a href="http://www.cis.strath.ac.uk/">Department of Computer &amp; Information Sciences</a> at the University of Strathclyde. There are three sub-events: a contributors conference, the KDE e.V. annual general assembly and a week long hacking session that offers the opportunity to discuss all sorts of things face-to-face. We also look forward to the chance to mingle with local KDE enthusiasts.
<!--break-->
<p>A local team led by <a href="http://www.duffus.org/">Kenny Duffus</a> will organise the conference, where KDE enthusiasts from around the world will converge to share experiences, code and socialise in "Scotland's friendly city". "<em>As a long time user of KDE, on both student and staff desktops, we are proud to support KDE by hosting this conference</em>" said Richard Connor, Head of the Department of Computer &amp; Information Sciences at the University of Strathclyde. </p>

<p>The preliminary schedule for aKademy 2007 is as follows: </p>

<table style="border: thin solid grey; margin: 0px; padding: 0px" cellspacing="0">
<tr><td nowrap="nowrap" style="font-weight: bold; border: thin solid grey; margin: 0px;">Friday 29th June</td><td style="border: thin solid grey; margin: 0px; ">Arrival</td></tr>
<tr><td nowrap="nowrap" style="font-weight: bold; border: thin solid grey; margin: 0px;">Saturday 30th June &amp; Sunday 1st July</td><td style="border: thin solid grey; margin: 0px; ">KDE Contributors Conference</td></tr>
<tr><td nowrap="nowrap" style="font-weight: bold; border: thin solid grey; margin: 0px;">Monday 2nd July</td><td style="border: thin solid grey; margin: 0px;">KDE e.V. Membership Meeting (members only, see the <a href="http://ev.kde.org/members.php">e.V. members page</a> for how to join)</td></tr>
<tr><td nowrap="nowrap" style="font-weight: bold; border: thin solid grey; margin: 0px;">Tuesday 3rd to Saturday 7th July</td><td style="border: thin solid grey; margin: 0px;">Hacking sessions &amp; workshops</td></tr>
<tr><td nowrap="nowrap" style="font-weight: bold; border: thin solid grey; margin: 0px;">Sunday 8th</td><td style="border: thin solid grey; margin: 0px;">Departure</td></tr>
</table>









