---
title: "Roktoberfest: The Amarok Project Fundraiser"
date:    2006-10-03
authors:
  - "GMeyer"
slug:    roktoberfest-amarok-project-fundraiser
comments:
  - subject: "Donated!"
    date: 2006-10-03
    body: "Using Amarok more or less daily - thanks for creating a great music player!"
    author: "Joergen Ramskov"
  - subject: "Amarok"
    date: 2006-10-03
    body: "I would like to see options which make it easy to take my ogg files and create a torrent for upload and exchange of data.\n\nI know that iTunes had an easy exchange option but went under pressure from the music industry. However, Amarok is not involved with big music biz.\n"
    author: "Hannes"
  - subject: "Re: Amarok"
    date: 2006-10-03
    body: "This could be implemented as a script. Scripts can add options to the right-mouse-button menu for playlist tracks."
    author: "Ian Monroe"
  - subject: "Prevent Amarok from rewriting playlists?"
    date: 2006-10-03
    body: "The only think I don't like about Amarok is that when it finds an M3U in my library, it rewrites the M3U file!\n\nWhy is this a problem?  Most of my playlists use relative file paths--Amarok rewrites them using absolute file paths.  The playlists still work great on my machine, but they are suddenly broken when I sync them to my portable.\n\nWhat I have to ask myself is--why does it bother to do this in the first place?"
    author: "ac"
  - subject: "Re: Prevent Amarok from rewriting playlists?"
    date: 2006-10-04
    body: "I agree, this is really annoying.\nThere is an option:\nGeneral -> Playlist options -> Manually saved playlists use relative path\n\nBut of course, by the time I found out it had modified many of my playlists.\n\"Fixed\" it using bash and sed."
    author: "John"
  - subject: "donation method alternatives?"
    date: 2006-10-03
    body: "Hi,\nare there any other possibilities than paypal to donate?\n\nr."
    author: "raphha"
  - subject: "Re: donation method alternatives?"
    date: 2006-10-03
    body: "Send an e-mail to amarokproject@gkmweb.com and we'll give you a mailing address to send a check if you want."
    author: "Greg"
  - subject: "Re: donation method alternatives?"
    date: 2006-10-03
    body: "How about a german bank account? That would make the donation a lot easier for me!"
    author: "ben"
  - subject: "Re: donation method alternatives?"
    date: 2006-10-04
    body: "Email Greg (he's our treasurer) and I'm sure you two can figure something out."
    author: "Ian Monroe"
  - subject: "Mystery"
    date: 2006-10-03
    body: "It's a mystery to me why the Amarok people have to do everything themselves (widgets, server, live-cd, wiki, fundraiser, ...) and don't want to use KDE e.V.'s resources."
    author: "Anonymous"
  - subject: "Re: Mystery"
    date: 2006-10-03
    body: "I second that."
    author: "fish"
  - subject: "Re: Mystery"
    date: 2006-10-04
    body: "When did they offer?"
    author: "Greg Meyer"
  - subject: "Re: Mystery"
    date: 2006-10-04
    body: "well for the server they could use kde's svn like k3b and amarok used to do, for the wiki they could use http://wikia.com/ or another free wiki service, for the livecd they could use torrents to distribute it, who cares about the livecd anyway. In the end it looks like the only thing amarok needs is more developper but the money of this fundraiser won't go to the devs, it will go to useless fancy stuff IMO."
    author: "Patcito"
  - subject: "Re: Mystery"
    date: 2006-10-04
    body: "Okay, I have time now for a more serious post.  Amarok is not part of the KDE project.  It is part of extragear.  Amarok devs are very big KDE supporters and Amarok relies heavily on KDE, but it is not part of KDE proper.  The consensus of the devs is they like it this way because it gives them more freedom, not being tied to KDE release schedules and such.\n\n1) Amarok does use the kde svn system and is part of the extragear repository.\n\n2) Amarok has a popular website and uses a lot of bandwidth.  Most offers for donated web space are usually insufficient in terms of holding up to the server loads we often get and to the monthly bandwidth requirements.  In the past year, many people have inquired about donating server space only to back down after they learn of the bandwidth requirements.  Therefore, up until now, the project chooses to pay for an appropriate server solution.\n\n3) As for a livecd, the pupose is to demonstrate amarok, so we need music on it.  I am not sure how much room there would be, or whether it would be appropriate to include large music files on the kde livecd.  The KDE livecd is free to included Amarok, and the Amarok Livecd ships with KDE, stripped down a bit to save space.  I don't see why this is a problem.\n\n4) There are many widget and icon requirements that are not currently met by the current widget/icon sets.  Also, there are certain circumstances where the devs feel that breaking off is appropriate within the context of the app.  We aren't trying to reinvent the wheel, or suffer from \"not invented here\" syndrome.  Sometimes things just don't work for us, like gstreamer atm.\n\nIn the end, I don't really see why it is a problem that a free software project can be successful on its own.  We love KDE, we use KDE, we particiapte in KDE, but at the end of the day, we aren't KDE.\n\nAs for how the money is used, much of it will go to the developers in the form of hardware to work on and travel costs to get together and meet for planning and hacking together, which has proven itself to be quite productive in the past.  Are you proposing to pay the devs or give them a stipend from the funds being raised?  Amarok has many very active devs, many more than most other free software projects.  Last count there were over ten active committers with author level status.\n\nUltimately, the people involved are trying to build a financially sound sustainable project.  I think a worthy goal.  If you truly disagree and want to argue these issues substantively, lose your anon tag and come talk about it.  We don't bite."
    author: "Greg Meyer"
  - subject: "Re: Mystery"
    date: 2006-10-04
    body: "Amarok's independence declaration.\n\nUp next: Why Amarok will get fully migrated Mono"
    author: "jum"
  - subject: "Re: Mystery"
    date: 2006-10-04
    body: "I don't see e.V. jumping through hoops to give acceptable server space, I don't see e.V. jumping through hoops to convince the project to fold into KDE.  What you guys don't seem to get is that this is a two-way street we're on.\n\nIf you don't like the situation, why don't you substantively address the issues and try to convince us of a better way instead of just trolling.\n\n<i>\"Up next: Why Amarok will get fully migrated Mono\"</i>\n\nI don't get why separation makes us not KDE supporters "
    author: "Greg"
  - subject: "Re: Mystery"
    date: 2006-10-06
    body: "Hmm, yet another push for money from the community. No problem there...however, do people realise that the Amarok developers are committed to releasing a Windows version? Donating money to this project is funding the development of that Windows version.\nNo thank you."
    author: "Bhud"
  - subject: "Re: Mystery"
    date: 2006-10-06
    body: "there seems to be a trend at the moment to design kde (kdelibs and kdebase to only use things like qt so that it will be more portable. having a windows version (as well as a mac version) of amarok around is a pretty valid way to test the quality of kdelibs and kdebase on other platforms, how it works with other compilers, what problems get triggered, etc. might mean more users as well, not to mention more developers.\n\nI understand that people get a little nervious when good oss projects are ALL availible under windows as well, that it will make linux look like it doesn't have any advantage anymore over any other OS. but most of the time we seem to actually gain from it. think about open/star office where half the target audience that wanted to pay (for support) appeared to be on the windows site, and we got all the resulting code as well.\n\na windows version is on the todo list anyway, they will do it because they want/like to do it. it's not like they are not going to develop it when you don't support them. and they did give you a very nice (though pretty heavy :p) music player for free ;)\n\nI trust these guys to do what they do, by looking at what they already did."
    author: "Mark Hannessen"
  - subject: "Re: Mystery"
    date: 2006-10-11
    body: "Your misconception is common in the open source world.  OpenOffice.org's availability on Windows has done HUGE amounts to advance OpenOffice.org's adoption; but almost nothing to help out Linux adoption.  \n\nLet me repeat this again to people who continue to not listen...  there is NO correlation between OpenOffice.org's adoption, Firefox's adoption, or Scribus' adoption, or the Gimp's adoption, and Linux's adoption!  There is tons of statistical and historical data to demonstrate it.  Don't agree with me?  Too bad! The facts about product adoption in IT are consistent and fairly well known already.\n\nFirefox has grown from < 2% browser usage to over 15% in some markets (and 9% overall) in the last 2 years;  there has been absolutely ZERO change (not some, not almost none... zero) in the Linux adoption rate in that time.  In fact, if you run both systems, you already know that the Windows version of Firefox is now more feature rich than the Linux version.  Did you just get that?  If you want to run the most functional version of Firefox now days... your better off running WINDOWS!\n\nThe reason people switch OSes is because that new Operating System provides them something they could NOT get on their old system... be it the ability to play a game they bought, connect up to a device they have, or interact with someone on another system.  People don't switch because the applications they ALREADY RUN work on a different system.\n\nKDE getting ported to Windows will ABSOLUTELY improve the adoption rate of KDE.  We will get more users, more developers, and more attention... we will not get more people moving to Linux.  This may be the way that people want KDE to progress (remember, there are a number of people working with KDE that don't like Linux or at the very least are indifferent); but it will happen.  And when it does, the single biggest draw we have for Linux will be gone.\n\nThe day that happens, KDE (a project STARTED to provide a desktop environment for linux) will no longer be the flagship Linux desktop; but will become a desktop environment that happens to also run on Linux."
    author: "Bobby"
  - subject: "Re: Mystery"
    date: 2006-10-04
    body: "Money spent this last year was spent on a much needed computer for one of the devs as well as train tickets, lodging and food for the pre-K3M meeting in Amsterdam. Not just the server costs."
    author: "Ian Monroe"
  - subject: "Re: Mystery"
    date: 2006-10-05
    body: "hey, just let this guy troll away. i think amarok is a valuable part of the KDE community. maybe there are things you guys could do to get more involved (some code might be pushed down further the library stack, into kdelibs) but it's not a huge issue."
    author: "superstoned"
  - subject: "Re: Mystery"
    date: 2006-10-05
    body: "I'm not trolling, just stating my opinion mind you. There are a lot of things in this world that deserve donations and free software is not the most urgent thing IMO. So when I send money to a free software project I want to be sure this money is really needed. \nIn that case, I'm sorry but they are spending money on things they shouldn't have too. As I said, they could use kde svn, get a free wiki, a free website from free.fr or else that offers infinite bandwidth and 10gigs of space, for the livecd I don't understand what money they need, if it's for the download why don't they just use bittorrent? in the end they could easily cut their expense by a great factor cause I guess hosting is very expensive and they could get rid of that. just my 2cent' :)"
    author: "Patcito"
  - subject: "Re: Mystery"
    date: 2006-10-06
    body: "Please read the explanations others have posted before. As for the SVN server - amarok is using kdeextragear repository (together with k3b and others). You may want to check the reality first."
    author: "Ladislav Strojil"
  - subject: "Re: Mystery"
    date: 2006-10-04
    body: "On the other hand it is fantastic to see Amarok grow into such a huge project that it can do all this independent of the e.V.  We should have many many more KDE projects that are this successful."
    author: "AC"
  - subject: "Re: Mystery"
    date: 2006-10-04
    body: "+1"
    author: "Greg Meyer"
  - subject: "Re: Mystery"
    date: 2006-10-04
    body: "Thanks :) Let me just add that we are not entirely independent of the KDE e.V. In fact I (an Amarok core developer) am a member of the e.V., and I'm actively promoting KDE.\n\nFor instance I have recently represented KDE at the Come2Linux expo (check my report: http://amarok.kde.org/blog/archives/229-Come2Linux-Expo-My-Trip-Report.html ).\n"
    author: "Mark Kretschmann"
  - subject: "Re: Mystery"
    date: 2006-10-04
    body: "+1 too."
    author: "Gabriel Gazz\u00e1n"
  - subject: "Sod it - I love amarok"
    date: 2006-10-04
    body: "I was nearly swayed by the post complaining about how the money was spent, but then I realised that I use Amarok every day, and love it.  How the team spend the money isn't my domain - I use the software, and am happy to contribute.\n\n\nPS I started off encoding to ogg, then (I got givin an iPod mini) to mp3, and now so I can convert to either format to flac.  Can I prevent amarok from duplicating or triplicating entries in the playlist, when I select a whole album - the directory can contain a .ogg a .mp3 and a .flac - ideally I'd like a precedence rule: flac, then ogg, then if theres an mp3, I suppose we'll settle for that!\n\nOff to donate via Paypal"
    author: "Graham Nicholls"
  - subject: "Re: Sod it - I love amarok"
    date: 2006-10-05
    body: "Thanks! When we first put up the donate link it was for the \"Amarok Beer Fund\". Our goals have expanded somewhat since then, but its the same idea. :)"
    author: "Ian Monroe"
  - subject: "Re: Sod it - I love amarok"
    date: 2006-10-05
    body: "Regarding your issue... its a rather unique one. My only suggestion would be a script that would go through and turn off the 'read' file permission. :/"
    author: "Ian Monroe"
  - subject: "Re: Sod it - I love amarok"
    date: 2006-10-05
    body: "well, if you let amarok transcode the files, and preserve them, isn't this needed? or is it already there? (can't test it now...)"
    author: "superstoned"
  - subject: "Re: Sod it - I love amarok"
    date: 2006-10-06
    body: "Usually people transcode them to be transfered on to an audio device. I've never used it myself, so I'm not sure how it works. But perhaps at this point some improvements could be made in how its handled?"
    author: "Ian Monroe"
  - subject: "Windows version"
    date: 2006-10-06
    body: "Thanks all!\nAnd on the windows version, I occasionally am forced to boot windows on my laptop.  It drives me nuts having to use such a poor UI. (Linux has been desktop ready for years - when will windows be?) Having a windows version of Amarok would make it a little more bearable, as I can access my ext2/3 music partition from windows. iTunes is probably the worst software I've ever used (in 25 years using computers); Amarok is amongst the best."
    author: "Graham Nicholls"
---
The Amarok Project is giving away an iPod Nano during its current fundraiser to celebrate the month of Roktober.  Anyone that gives the equivalent of $10 or more is automatically entered in a random drawing to win a 2GB iPod Nano.  Amazingly, a year has passed since the last fundraiser.  Having the ability to spend some money on project resources and hardware made a big difference to the project's productivity.  The past year has seen much progress on the Amarok front with a 1.4 release that saw a whopping 64 new features, a very successful developer meeting in May and basic groundwork laid for the port to KDE 4.  Plans for 2007 include at least one more meeting to hack on Amarok 2.0. There is a PayPal link and more information on <a href="http://amarok.kde.org">the Amarok website</a>. 

<!--break-->
