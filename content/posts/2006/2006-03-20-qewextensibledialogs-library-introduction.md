---
title: "QewExtensibleDialogs Library Introduction"
date:    2006-03-20
authors:
  - "jcuadra"
slug:    qewextensibledialogs-library-introduction
comments:
  - subject: "nested dialogs?"
    date: 2006-03-20
    body: "Nested dialogs sounds like a horrible idea usability-wise."
    author: "Erik"
  - subject: "Re: nested dialogs?"
    date: 2006-03-20
    body: "Relax, the article explains their use. It seems it is intended more of a way to add conditional elements to dialogs. Instead of adding extra widgets individually to your dialog when needed, you slap in another complete dialog within the original dialog... \n\nIt's also described as an alternative to tabs, in cases where adding a condition (like a feature being enabled) adds an extra tab just means more clicking and searching for options... which could be nested in one location.\n\nI doubt you'll see this in common use. But it may have cases where its useful.\n\nSomething like this anyhow.... i skimmed the article pretty quickly... "
    author: "T. Middleton"
  - subject: "Re: nested dialogs?"
    date: 2006-03-20
    body: "\"i skimmed the article pretty quickly...\"\n\nWell, that's more than I did :)\n"
    author: "Erik"
  - subject: "QT 3 -> QT4 Porting"
    date: 2006-03-20
    body: "I have just ported an application from QT3 to 4 in order to take advantage of the new features of QThread. I have to say that if you are porting using the qt3to4 program, no problem. Doing it properly however, takes a bit of time.\n\nYou will find that there are large number of methods that have been removed and a whole load of new ones added. And some things just dont work the way they used to. Take something as simle as icons and text on buttons. Add text to buttons on QT 3 and 4 and the text is centered on the buttons. Add icons to the buttons and the QT4 text is no longer centered, it is left aligned up against the icon. Talking of icons, the whole way they are handled has also changed. So even the simple things about applications, before you even get to the functionality, takes time to port.\n\nI would like to personally thank those KDE developers who are currently porting KDE to QT4, it is a big job (assuming they doint take the easy route).\n\nFizz\n"
    author: "Fizz"
  - subject: "Re: QT 3 -> QT4 Porting"
    date: 2006-03-21
    body: "qt3to4 and the Q3Support classes are only stopgap measures. I believe all the 3Support classes and functions are deprecated, so the sooner you abandon them the better. Port it first with qt3to4 quickly, then spend time porting it the right way. The advantage is that your software isn't broken while you're doing it.\n\nI'm hoping KDE 4 will be released in a state where libQ3Support isn't needed. But I doubt it."
    author: "Brandybuck"
---
<a href="http://webs.ono.com/usr036/jmcuadrajer/qew/qew.html">QewExtensibleDialogs</a> is a plugin library for Qt Designer. It provides dialogs that can be nested with no limits and provides centralised control for accepting or rejecting the whole stack. Jose Cuadra <a href="http://webs.ono.com/usr036/jmcuadrajer/qew/qewarticle.html">introduces his library</a> in an article which describes the use cases, the general design pattern and his Qt implementation.




<!--break-->
