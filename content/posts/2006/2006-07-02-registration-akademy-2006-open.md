---
title: "Registration for aKademy 2006 Open"
date:    2006-07-02
authors:
  - "chowells"
slug:    registration-akademy-2006-open
comments:
  - subject: "I am the first"
    date: 2006-07-02
    body: "My main desire was be the first, but just today I don't have anything to write.\n\n:-(\n\n\n--x--"
    author: "Hell Lammer"
  - subject: "\u00cc\u00ec\u00cc\u00ec\u00c3\u00e2\u00b7\u00d1\u00b5\u00e7\u00d3\u00b0"
    date: 2006-07-12
    body: "[http://my.opera.com/uni165/homes/files/lt000.htm \u00c1\u00aa\u00cd\u00a8\u00c1\u00e5\u00c9\u00f9]"
    author: "\u00cc\u00ec\u00cc\u00ec\u00c3\u00e2\u00b7\u00d1\u00b5\u00e7\u00d3\u00b0"
---
The <a href="http://conference2006.kde.org">aKademy 2006</a> organisation team is please to announce that <a href="http://conference2006.kde.org/registration/">registration for this year's conference</a> in Dublin, Ireland, has now opened.  KDE welcomes registration from anybody interested in the future development of KDE, including developers, translators, other free software projects, representatives of the software industry and ISVs interested in using free desktops and the KDE application framework.
<!--break-->
