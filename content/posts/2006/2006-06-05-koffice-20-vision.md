---
title: "KOffice 2.0, The Vision"
date:    2006-06-05
authors:
  - "tzander"
slug:    koffice-20-vision
comments:
  - subject: "Pigment?"
    date: 2006-06-05
    body: "You mean Pikment surely"
    author: "bob"
  - subject: "Oh noes"
    date: 2006-06-05
    body: "Teh unfunny joke gave me teh cancer."
    author: "em"
  - subject: "Re: Pigment?"
    date: 2006-06-06
    body: "do you mean bob or blob ??"
    author: "blob"
  - subject: "Re: Pigment?"
    date: 2006-06-08
    body: "I've wondered for a while whether there's a K ego, perhaps even a conspiracy to eradicate all things G. But alas, Ockham's razor requires we consider the possibility that it is cute branding, a trademark if you will. :)\n\nThat and I'm too lazy to worry about inter-GNU project struggles (just install 'em all, they're free)!"
    author: "ash"
  - subject: "Re: Pigment?"
    date: 2006-07-11
    body: "Bad idea. Pik means dick in Danish."
    author: "Anders"
  - subject: "Development will delay a lot"
    date: 2006-06-06
    body: "With the FIFA world cup here, we will stop our work to give a look at those thousand pretty brazilian womens with their usual small skirts. \nSorry folks, KDE 4.0 will be launched next year, probably."
    author: "Nooopss"
  - subject: "Re: Development will delay a lot"
    date: 2006-06-06
    body: "There are some movements here in Brazil to prevent selling this nice-almost-naked-women image. This unhappilly is causing a lot of sexual tourism, mostly in north-east.\n\nBesides, we don't stop because of those women in world cup (fot that carnival exists, hehehe), football is enought to stop the entire country :)\nCount that brazilian portuguese translations will be out of pace for a while ;)"
    author: "Iuri Fiedoruk"
  - subject: "Brasileiras are my cup of tea"
    date: 2006-06-06
    body: "\nBrazilian woman are incredible. They know all the write commands in the right order: top, mount, fsck, flush, sleep;\n\nThey are often run threaded with the ability to engage their friends for a little distributed processing and a whole lot of fun. Never visit Brazil or thou shall not leave it.\n\nAhmmmmmmmm, what were we talking about? Oh, yeah, KDE 4 will win the world cup, I mean, KDE 4 is hot, and Brazililians write good software or something like that."
    author: "Me or somebody like me"
  - subject: "Re: Development will delay a lot"
    date: 2006-06-08
    body: "My dear nerd, if you don\u00b4t know, world cup will be in GERMANY, so.. how you can give a look at pretty brazillians womens?? Why don\u00b4t you go to Rio de Janeiro\u00b4s Carnival? It\u00b4s pretty good!\n"
    author: "Juliana"
  - subject: "Re: Development will delay a lot"
    date: 2006-06-09
    body: "I'm not the original poster, but just one word:  Fans!  :)\n\nI've even seen a few of them yesterday in K\u00f6nigstein where the Brazilian team is housed. \n\n"
    author: "cm"
  - subject: "RE: Yes, brazilians chiks will be at Germany"
    date: 2006-06-09
    body: "Yes, brazilian women will be at Germany, there is a top model team there, sponsored by a TV chanel. Rio de Janeiro will lose it\u00b4s postcard, the bikini tanned round *#@% of pretty brazilians.\n\nAll this *#@% will be at Germany, but, unfurtunately, well covered, due the cold wheather.\n\n"
    author: "noooppsss"
  - subject: "RE: Yes, brazilians chiks will be at Germany"
    date: 2006-06-13
    body: "What\u00b4s \"*#@%\"??\nI\u00b4m a brazillian women and I would like to know... "
    author: "Juliana"
  - subject: "Huunnnn"
    date: 2006-06-14
    body: "This may mean \"bumbum\", I think...."
    author: "bb"
  - subject: "RE: Yes, brazilians chiks will be at Germany"
    date: 2006-06-14
    body: ">All this *#@% will be at Germany, but, unfurtunately, well covered, due the cold wheather.<<\nNo - it's not cold here - it's hot. Beautiful sunshine and temperatures in the 30ies. And KDE/Koffice *will* be ready in time because we are used to see gorgeous chicks - actually we have beautiful ladies from all over the world here (and many from Latin America too :-) as well as some of our own tribe - all the year long.)\n\nSo have fun - keep on developping great software :-)\nConrad\n(Berlin/Hamburg Germany)"
    author: "beccon"
  - subject: "And why not fixing the bugs first?"
    date: 2006-06-06
    body: "Instead of reinventing the wheel, why don't they just try to fix the compatibility issues with ODF? You can't write ODF formulas (they're lost when you save them) and so you can't have any formulas at all in KWord's defauolt format.\nThe spreadsheet is a joke, Kword is fast, but lacks so many things... Only Krita seems to get proper attention.\nSo, why not make it usable after all and then start implementing innovations?"
    author: "me"
  - subject: "Re: And why not fixing the bugs first?"
    date: 2006-06-06
    body: "Good idea. I suggest we fork the project and implement what is missing. Contact me by email to do this, if you are really interested.\n\nWhy developers dont do this ? Because they do not want, and thats the point of open-source development: do it for fun, in the first place. i just hope all this work will bring a better koffice for all.\n\nAnd i dont think kword is so bad, in the first place. i use it sometimes, and if any problem occurs and i dont like it anymore i can switch to openoffice anytime i want, thanks to the work of koffice developers who changed the file format."
    author: "Henrique Marks"
  - subject: "Re: And why not fixing the bugs first?"
    date: 2006-06-06
    body: "I just don't have the skills :-)\nThe Koffice crew seem to be quite talented, it's just that it just isn't as good as OpenOffice yet, in my opinion. I can't really use it for real work (I've tried before), but I'd like to because it's promising and cool."
    author: "me"
  - subject: "Re: And why not fixing the bugs first?"
    date: 2006-06-06
    body: "Well, before I started on Krita I hadn't got the skills either. I had never written more than a dozen lines of C++, but it's an easy thing to learn on the job. It's just a matter of getting started and after that some perseverance."
    author: "Boudewijn Rempt"
  - subject: "Re: And why not fixing the bugs first?"
    date: 2006-06-11
    body: "lets make things straight. OpenOffice is slow as hell and unsable. their pretending compatibility with microsoft product is a joke. and only Microsoft Office really works with those documents. also OpenOffice only copy what Microsoft did and really does not improove anywhere.\n\nKOffice however is lighting fast and has great stuff that no other suite has. it does miss few important features and still have bugs. but its a charm to use. I do use it for production use as well as many other KDE user I'm sure. ideas in this suit are way ahead and koffice is the only Office suite that actually considered what computer life will be from 2007\n\nalso remember there is not many koffice developers. and what they did since 1.0 is amazing. clap clap clap\n\nkeep going guys. you're doing an awesome job. dont listen to flamers and gives us the best office suite for KDE 4\n\ngambatte kudasai\n"
    author: "somekool"
  - subject: "Re: And why not fixing the bugs first?"
    date: 2006-06-06
    body: "I support you implementing whats missing, very good idea!  You don't even need to fork you can do it right into the KOffice svn repository, all you need to do is send in the patches."
    author: "Thomas Zander"
  - subject: "Re: And why not fixing the bugs first?"
    date: 2006-06-07
    body: "Great. Someone raises valid concern/criticism and you just tell him to code it himself. Regardless it its developed for fun maybe the developers would like to have and use the critique positively.\nHe's just wront about krita."
    author: "josel"
  - subject: "Re: And why not fixing the bugs first?"
    date: 2006-06-07
    body: "What kind of concern/criticism? Let me quote: \"The spreadsheet is a joke\" does not tell the developers anything.\n\nNot everyone has a crystal ball. At least, I don't.\n\n"
    author: "Ariya"
  - subject: "Spreadsheet"
    date: 2006-06-08
    body: "Even OOo Calc lacks some important features in the graphical representation of numbers. The scaling of x/y isn't up to snuff. Then the embedded programming language isn't really usable either for serious work. I use OOo Writer and sometimes KOffice (rarely) but Excel unfortunatelty is still way ahead and the only winDOS program of use (but big one).\n\nJuergen"
    author: "Juergen"
  - subject: "Re: Spreadsheet"
    date: 2006-09-10
    body: "OOCalc is a very good spreadsheet as I find it much more stable than MS Excel when working on the same large data sets. I tried to open up the same ODS sheets in KSpread but it crashed (perhaps 25 columns of 10,000-12,500 data points is too much?) but I do have a few things that OOCalc should have:\n\n1. The ability to put the equation of the best-fit line ON the graph.\n2. The ability to plot multiple data sets on an XY graph and have them be independent data sets as in MS Excel.\n3. To not have to completely recalculate the graph if I move it on a page but do not resize.\n4. To be able to scale the X-axis in a regular line graph instead of putting a marker for each X value down there."
    author: "PGK1"
  - subject: "Re: And why not fixing the bugs first?"
    date: 2006-06-09
    body: "The spreadsheet _is_ a joke (or at least was last time I checked).\n\ngnumeric is the only good spreadsheet I've found for any platform.\n\nConstructivly:\n\nThe charting-functionality seems to be pretty bad.  When I use spreadsheets what I mostly do is dump in some numbers from somewhere, then graph them.  With gnumeric it's a breeze .. with kSpread .. I don't find the graph in the image that is generated!\n\nHmf.\n\n"
    author: "Anonymous Coward"
  - subject: "They are not bugs, they are missing features"
    date: 2006-06-12
    body: "I am not sure these are bugs, they may be a lack of features/immaturity of the concept. I do think that the metaphors used by KOffice make more sense than the ones of MS Office - cloned by OpenOffice.org - and I would very much like to learn it and use it for work and play.\n\nUnfortunately, I translate doc and pps documents written by people who would not learn MS Office properly, let alone learn how to use another suite on a different OS altogether; or worse, I sometimes work on documents developed by several people - often embedding more different source data - with less experienced people ignoring and mangling the careful settings of the more experienced ones because of the dumb, opaque interface.\n\nWith KOffice I resent the lack of adecquate language tools and more importantly the lack of good compatibility with the MS Office file formats. Those are the reasons I am not learning how to use KOffice and why I use it only to edit/change the format of PDFs I occasionally have to translate."
    author: "Bilbophile"
  - subject: "Re: They are not bugs, they are missing features"
    date: 2008-05-26
    body: "<i>on a different OS altogether;</i>\n\nErmem, KDE 4 ==> Windows port. Can't wait to use a fully functional Koffice 2.0 in Windows."
    author: "Madman"
  - subject: "Re: And why not fixing the bugs first?"
    date: 2006-06-06
    body: "> you can't have any formulas at all in KWord's defauolt format\n\nFixed. Thanks for the support."
    author: "bug buster"
  - subject: "Re: And why not fixing the bugs first?"
    date: 2006-06-06
    body: "Maybe you don't know this, but KOffice is being developed in three branches at the moment.\n\n- A 1.5.x branch for bug fixes\n- A 1.6 branch\n- A 2.0 branch (which is trunk)\n\nSo, there still get many bugs fixed in the 1.5 and 1.6 branches, while all the innovations get added in trunk.\n\n\nYou're always more than welcome to help with any aspect of KOffice, there's more to do than only coding. "
    author: "Tim Beaulen"
  - subject: "Re: And why not fixing the bugs first?"
    date: 2006-06-06
    body: "Because some problems (like font kerning) can't be fixed without those innovation. And flake will bring some innovations, but it is first about code sharing between apps, which means that in the future instead of fixing a bug in kword, and in kpresenter, and in karbon and in kivio and in etc, you will fix in flake and fix for all. That's the innovative part of code sharing ;)"
    author: "Cyrille Berger"
  - subject: "Re: And why not fixing the bugs first?"
    date: 2006-06-06
    body: "Because some problems (like font kerning) can't be fixed without those innovation. And flake will bring some innovations, but it is first about code sharing between apps, which means that in the future instead of fixing a bug in kword, and in kpresenter, and in karbon and in kivio and in etc, you will fix in flake and fix for all. That's the innovative part of code sharing ;)"
    author: "Cyrille Berger"
  - subject: "Re: And why not fixing the bugs first?"
    date: 2006-06-06
    body: ">The spreadsheet is a joke\n\nWhat is wrong with KSpread? It is actually quite good and fast. .ods-import works well for me, only the diagrams keep me from using it (I am using the sloooow OOCalc2)."
    author: "Carsten Niehaus"
  - subject: "Re: And why not fixing the bugs first?"
    date: 2006-06-08
    body: "While it is my general position that more effort should be put into fixing bugs and I would always error on the side of caution when implementing new technologies, we have to consider that with version 2.0 KOffice will be based on Qt-4.  \n\nTherefore, this is the time to break binary compatibility and some wheels are going to have be reinvented for some of the bugs (that can only be fixed by going to Qt-4) to be fixed.  The main example that we are all familiar with is the text formating issues which cannot be fixed with Qt-3's PostScript driver.\n\nIn some cases, it might be better to wait till after the 2.0 release to implement new technologies.  But, there is always a trade off since it would not be possible to, then, replace existing APIs (that would break binary compatibility) but rather to only add additional APIs (all APIs in 2.0 would have to remain till 3.0).  In this case, since KOffice is not really yet a mature application, I see nothing wrong with doing some things over if the current implementions are not working very well.\n\nBug fixes can continue with the KOffice version 1.x branches and can be front ported to the version 2 trunk."
    author: "James Richard Tyrer"
  - subject: "Live interaction beween apps?"
    date: 2006-06-06
    body: "So is going to be possible to have multiple koffice apps running side-by-side and edit shared items between docs live, so that I could edit a kspread table that would be updated on kword doc, kivio diagram on kpresenter, or whatever combination sharing objects _live_ so I could instantly how the end result would look like?\n\nAs it kinda sucks to need to save/reload whatever object when doing docs."
    author: "Nobody"
  - subject: "Re: Live interaction beween apps?"
    date: 2006-06-06
    body: "That would be hot.\n\nI spend so much time going back and forth between photoshop and powerpoint getting things just right."
    author: "theorz"
  - subject: "Re: Live interaction beween apps?"
    date: 2006-06-06
    body: "Already supported.  Thats actually what the screenshot at the end of the article is showing."
    author: "Corbin"
  - subject: "Re: Live interaction beween apps?"
    date: 2006-06-06
    body: "That's already possible: Insert a kspread object into kword. Select it. View->New View. Voila. All changes you'll make in one of the tables are immediately shown in the other representation."
    author: "Stefan Nikolaus"
  - subject: "DDE, OLE and OpenDoc?"
    date: 2006-06-06
    body: "What is the difference to Dynamic Data Exchange and Object Linking and Embedding in the Windows world? Or OpenDoc on the Mac?"
    author: "testerus"
  - subject: "Re: DDE, OLE and OpenDoc?"
    date: 2006-06-06
    body: "DDE is an interprocess communication control. Like DCop or D-Bus\nIt's not the same as Flake.\n\nFlake lets you edit objects (created with another program) from inside a single program. So you only run one process.\n\nI guess you can compare it a little with OLE, but it still is not the same.\nEditing a spreadsheet from within KWord for example doesn't load the whole KSpread program. Only those tools or shapes that are needed."
    author: "Tim Beaulen"
  - subject: "Re: DDE, OLE and OpenDoc?"
    date: 2006-06-07
    body: "DDE was a very cool thing just as DCOP is today. I'm not as familiar with OpenDoc but I believe it is an embedding framework. OLE stood for Object Linking and Embedding, but it ended up meaning \"One Long Expletive\" when it crashed trying to do anything useful. When I left Windows in the 90s focus had shifted from DDE to OLE because MS preferred bloated embedding protocols to scripting. KDE's KParts and the Koffice libraries are more efficient, but the libraries you mentioned are more general purpose. For instance in Quanta we use KParts to load an application and DCOP to communicate with it. Compared to what Koffice is doing it's very rudimentary. Our interface is fairly rigidly defined to a full panel and we may find the plugin needs additional interfaces. With KOffice there is a lot more intelligent and defined interaction in the libraries inside."
    author: "Eric Laffoon"
  - subject: "Re: DDE, OLE and OpenDoc?"
    date: 2008-05-26
    body: "On the subject of Quanta, is there going to be a KDE 4 port?\n\n... I found that tool infinitely useful."
    author: "Madman"
  - subject: "Why apps?  But anywho."
    date: 2006-06-06
    body: "I continue to wonder why it is necessary to have separate  Word, Spread, & Chart applications rather than having these integrated into one app that uses parts.\n\nIAC, what I would like to see is to have it possible to link a number in a KWord document to a cell in a KSpread document and have it look just exactly like I had typed the number in the KWord document, yet if I change the spreadsheet, the number in the KWord document would change."
    author: "James Richard Tyrer"
  - subject: "Re: Why apps?  But anywho."
    date: 2006-06-06
    body: "Because for a certain task, a specialized interface is preferable. We discussed this a lot and came to the conclusion that it is much more userfriendly to have different interfaces for different main tasks. Especially the example of Gobe Productive argued in favour of our decision.\n\nAbout sharing styles between different flakes -- that's something to consider. I don't know whether OpenDocument supports it, and if it doesn't, it will be hard to do, and getting inline data in a flake from another component will be a nice challenge, but as long as we're talking objects embedded in a text flake, doable. As long as OpenDocument supports it, of course."
    author: "Boudewijn Rempt"
  - subject: "Re: Why apps?  But anywho."
    date: 2006-06-06
    body: "Why apps?\nbecause the user interface for a text processor still looks and acts more specialized for that type of data.  Karbon will continue to have dockers and menus that are specialized for vector data while Krita will have menus with filters for bitmap data.\nIf all would be in one application we would either have 25 menus or alter our menu structure every time we make a selection.  I somehow think that that approach will not be good for usability :)\n\nThe feature request for KWord <-> KSpread interaction is a good one, I'll push that out the the developers for consideration.\n\nThanks James!"
    author: "Thomas Zander"
  - subject: "Re: Why apps?  But anywho."
    date: 2006-06-06
    body: "> we would either have 25 menus or alter our menu structure every time we make a selection.\nLike MS Office does now? Funny how MS shoots it's customers in the knees over and over again..."
    author: "Thomas"
  - subject: "Re: Why apps?  But anywho."
    date: 2006-06-06
    body: "Have a look at Officd 12 - they did some great work on that app. It might look unfamiliar, but it works - seriously. The first thing from MS I would call innovation... It might not be totally new, but still its a great thing for Office."
    author: "superstoned"
  - subject: "Re: Why apps?  But anywho."
    date: 2006-06-06
    body: "Actually, I am only suggesting (at least to start) integration of the three major office apps: Word, Spread, & Chart.  Embedding is sufficient for adding images.\n\nYes, the menu issue is one issue that would need to be solved to do this.  And, I know that the KDE guidelines state that all operations that appear in toolbars must appear in the menu-bar menu.\n\nI could probably list a handful of other issues, but I would look at these as challenges to be overcome rather than reasons not to try.  \n\nThe first idea that comes to mind is to have a main menu bar that is always there no matter which part is active and an part menu bar for each part that only appears when that part is in use.  I do not offer this as the ultimate solution, I am sure that with thought that we could come up with better ideas and refinements."
    author: "James Richard Tyrer"
  - subject: "Re: Why apps?  But anywho."
    date: 2006-06-07
    body: "see http://en.wikipedia.org/wiki/Archy for info about an app-less vision of Jeff Raskin. Very different way of looking at things."
    author: "Jonathan Dietrich"
  - subject: "Re: Why apps?  But anywho."
    date: 2006-06-07
    body: "This has been done several years ago with StarOffice. That's why it takes so long to load, it is one app in different shapes. And as we know everybody complains about it being bloated.\n"
    author: "Jay Pee"
  - subject: "Why 2.0"
    date: 2006-06-06
    body: "Why not name it something more glorious?\n\nLike Koffice 2000, or Koffice 4 (KDE 4). Koffice XP. :-))"
    author: "KDE User"
  - subject: "Re: Why 2.0"
    date: 2006-06-06
    body: "Yeah, why not call it KDE Power Office 3000 Professional Plus, Xtreme Edition?\n\nMaybe because in the free software world we don't care so much about cool names (except for the K-thing) and marketing blah-blah. We do care about good software. "
    author: "Pingwing"
  - subject: "Re: Why 2.0"
    date: 2006-06-06
    body: "> we don't care so much about cool names ...\n\n> ... (except for the K-thing)\n\nHehe :-)"
    author: "KDE User"
  - subject: "Re: Why 2.0"
    date: 2006-06-07
    body: "> Maybe because in the free software world we don't care so much about cool\n> names\n\nHm. plasma? solid? phonon? akonadi? pigment? flake? Those things used to be called \"KDE Multimedia\" or \"PIM Storage Layer\". Parts of KDE definately have started caring about cool names. I think that's good."
    author: "PC Squad"
  - subject: "Re: Why 2.0"
    date: 2006-06-13
    body: "KDE PO000PP XtremeE?\n\nSounds good"
    author: "Allan"
  - subject: "Re: Why 2.0"
    date: 2006-06-06
    body: "All those strange versions dont work and are bad marketing. People will not know if this is a new version or not. There are only a few worldwide brands (like Microsoft) that can afford to do this because a new release is announced everywhere in the media. And then the gap between versions must be large enough. \nLook up Dreamweaver in Wikipedia. You will find the version history at the bottom. My boss recently refused to buy Dreamweaver 8 at first because he thought we have already Dreamweaver MX 2004 which is newer. It's not. Did you expect that? I didnt until I looked it up on Wikipedia. Meaningless to say you will find nothing about this on their Homepage. If you already need an encyclopedia to look up which product is newer your marketing strategy is definitely flawed.\n"
    author: "Martin"
  - subject: "Re: Why 2.0"
    date: 2006-06-08
    body: "Why not call it \"KO Round 2\" ;)"
    author: "Super-Root"
  - subject: "Re: Why 2.0"
    date: 2006-06-14
    body: "K O is a bad portuguese name. This is a brazilian slang that means \"fake\" or \"lie\" and also we have a genital lubrificant for maleXmale with this name at the drugstores."
    author: "bb"
  - subject: "Re: Why 2.0"
    date: 2006-06-09
    body: "D'ont push it or someone will have the wonderfull idea to name it Koffice2 version 1,0. If i recall correctly this was schema was to be used on KDE2 until users protested."
    author: "josel"
  - subject: "Balancing feature race and stability"
    date: 2006-06-06
    body: "Look,\n\nI commend the effort to add advanced functionality to Koffice.\nBut right now, Koffice is at a point where I -want- to use it but -cannot-.\nWhy?\n\nSimply because it is too unstable and unreliable. You won't see me using Koffice before that is fixed, regardless of advanced new features.  I suspect a lot of other people are of the same opinion...\n"
    author: "Roger F"
  - subject: "Re: Balancing feature race and stability"
    date: 2006-06-06
    body: "Well, there are some very basic problems in Koffice right now, and hopefully these can be fixed by the Koffice 2 release. If they are, its very likely Koffice 2.0 (or 2.0.x) will be what you are looking for.\n\nMany seem to think the Koffice guys just go for 2.0 because they want new new new lovely features. But it is also very much about a fundamental redesign to SOLVE some deep problems."
    author: "Superstoned"
  - subject: "Re: Balancing feature race and stability"
    date: 2006-06-06
    body: "I hoped that it goes without saying that the stability and lack of features like tables and kerning are at the top of the list of things to do.\nBut I guess that the years of false promises by the software creators at large have skewed that perspective.\nMy fault for not expecting it. Sorry about that.\n\nThe creation of these two libraries move the development from several separate islands into one community with the positive effects that has with regards to peer reviews and code reuse (which means less code and thus less bugs).\n\nCompletely separate from this article, KOffice 2.0 will bring a new text engine for KWord to get rid of a lot of problems we had in previous versions. Including kerning and printing (wysiwyg).\n\nThis vision is not a list of features, its, well, a vision of the way we think it will look.  We did not steal it, we are innovating.  That does not mean that we don't care about stability and basic features.\n\nThanks for your feedback!"
    author: "Thomas Zander"
  - subject: "Re: Balancing feature race and stability"
    date: 2006-06-07
    body: "Yes, the same with me. Every time KOffice team releases bugfix release I install it and try to use, only to discover that with just casual use (editing some tables in KWord, embedding a formula etc. - usually not more than after 10 minutes :-) ) one of the apps (usually KWord - but not always) crashes. Ehhh... I can only hope that they would really do some QA on KOffice2 :-) "
    author: "Piotr Gawrysiak"
  - subject: "Re: Balancing feature race and stability"
    date: 2006-06-08
    body: "Exactly like the subject states; tables are a balance where I honestly wanted to remove tables completely in the 1.5 series of releases since they were not up to snuff.\nBut removing stuff (even if it crashes for some people) is a big no-no, so we let it stay.\n\nNeedless to say we (and by that I mean I) really wanted to get it right, but for that we need some of the features that 2.0 will bring.\nI expect tables to be so much improved in KOffice2 that you won't be able to stop using KWord even after a hour of playing ;)"
    author: "Thomas Zander"
  - subject: "Flake and printing"
    date: 2006-06-06
    body: "Is flake going to solve the printing diabilities of KOffice ? I would love to use KSpread but I have always been seriously annoyed by the complexity to go from the screen to a nice A4 printed sheet.\n\nCreating a well balanced set of printed sheets of paper is certainly not easy\nbut KOffice seems to have structural problems on this matter.\n"
    author: "Charles de Miramon"
  - subject: "Re: Flake and printing"
    date: 2006-06-06
    body: "> Is flake going to solve the printing diabilities of KOffice\n\nYes, it already makes printing a lot better for a lot of different issues.  Flake will generate PDFs directly and solve lots of issues with regards to printing in one go.\n\nI currently do not know how kspread will use flake, thats still in the future, but the printing issue will certainly be kept in mind when developers start to take advantage of flake."
    author: "Thomas Zander"
  - subject: "Re: Flake and printing"
    date: 2006-06-06
    body: "I'm sure Koffice 2 (based on KDE 4/Qt4) will be better in this area. But maybe you would like to elaborate about what is wrong, exactly?"
    author: "superstoned"
  - subject: "Re: Flake and printing"
    date: 2006-06-06
    body: "Look the TeX algorithms. \n\nWhen you print a text on a sheet of paper, you often cheat with geometry (because your eyes will not see the difference). You squeeze a line a little more, expand there a little, zoom a little bit this graphic. The TeX algorithms use the concept of penalties. It is a different logic that representing something on a screen which is just an infinite space. \n\nThe danger is thinking that going from the screen to the sheet of paper is just applying a mathematical transformation.\n\nI would argue to make it possible in KOffice 2.0 to plug in a page creation algorithm. Maybe somebody will step in at some point and write one taking inspiration from the published and tested TeX mechanisms. \n\n "
    author: "Charles de Miramon"
  - subject: "Re: Flake and printing"
    date: 2006-06-06
    body: "I know the algorithms, sure.  But I'm afraid I need a little more explanation on where you think Tex is usefull to get a kspread sheet on paper.\nI only see the use for text areas, not for loads and loads of cells."
    author: "Thomas Zander"
  - subject: "Re: Flake and printing"
    date: 2006-06-06
    body: "Well, TeX works with horizontal and vertical lists of boxes with spaces between boxes that are called glue or coils (I do not remember) and then cut this list in pages. \n\nI guess that similar algorithms could apply for printing tables in KSpread.  "
    author: "Charles de Miramon"
  - subject: "OpenOffice Compatibility"
    date: 2006-06-06
    body: "what does this mean for cmpatability with OOo?\nis this just an accessibility improvement, or does it result in special files?\n\ni bet all office suits will use ODF, but each in its own \"interpretation\", which will result in same old incompatability...."
    author: "hannes hauswedell"
  - subject: "Re: OpenOffice Compatibility"
    date: 2006-06-06
    body: "We will make sure that ODF support will be maintained. If OOo does not implement some part of the spec then it won't work and you have a right to tell them to fix their application since they claim to have good support.\n\nWe are doing the same, one of the things that we need to do is to provide various features for ODF; we are doing exactly that."
    author: "Thomas Zander"
  - subject: "Re: OpenOffice Compatibility"
    date: 2006-06-08
    body: "Read Linus' post about standards.\nStandards are not replacement for developer communication, you shouldn't treat them religiousely. OOo people are fighting the same war as you, so it will be better to talk with the before implmenting some inconsistent changes in the format's support code.\nWhen 1st 2 serious suites implementing format that is claimed to facilitate interopeability are incompatible with each other, the reputation is lost and every body looses in effect."
    author: "DS"
  - subject: "Re: OpenOffice Compatibility"
    date: 2006-06-11
    body: "I like Kpresent better than OpenOffice.org Impress because I like Koffice graphics better than OpenOffice.org's , but for text I like OpenOffice.org Writer because of the table of contents and bookmarks that can be done with the exported PDF. \n\nI like having backup software in case I have problems with my main software and I also like to try out new software, so I would be really interested in how well Kword will open a lightly formatted ODT formatted document. I don't need advanced layout or specialized features, but I would like the simple stuff to be compatible. Is Koffice 1.5 at that level yet?"
    author: "Benjamin Huot"
  - subject: "Tabs"
    date: 2006-06-06
    body: "What I would really like to see* in KOffice is tabs, like in Konqueror. I never saw an office suite that has such a simple but cool feature.\n\n\n_______________________________\n\n* Plus proper right-to-left support, at least like OpenOffice.org"
    author: "Youssef"
  - subject: "Re: Tabs"
    date: 2006-06-06
    body: "Well, good thing for you since KOffice has been supporting both those features since 1.4 (at least). :-)"
    author: "Thomas Zander"
  - subject: "Re: Tabs"
    date: 2006-06-06
    body: "Well, the problem is that tabbing is only supported in the KOffice Workspace instead of providing tabbed documents in Kword itself. Mostly, I find myself working with different documents or different spreadsheets or different images and therefore do not like everything in a big overloaded application like the old Staroffice used to be.\n\nPersonally, I find the KOffice Workspace not an adequate solution as 1) switching between two Kword documents results in annoying flickering of the toolbars 2) switching between KOffice Workspace tabs is slow.\n\nFurthermore, switching between two different applications such a Kword and e.g. a Krita is even more annoying as then the toolbars' positions and icons jump around even more."
    author: "Tuxa"
  - subject: "Re: Tabs"
    date: 2006-06-06
    body: "IIRC Lotus Word Pro had tabbed documents many years ago. Long before it became cool."
    author: "Matt Williams"
  - subject: "my hope"
    date: 2006-06-06
    body: "well, then, let me add my 'hope' for Koffice 2: nice versioning support, change management and all this cooperation-ready. Easy sharing of documents, being able to see what someone else changed, annotations to changes, being able to change a doc both at the same time (preferably real-time) etcetera. These things would writing documents in a company or for university/school much easier...\n\nI know there is a SOC project, but will it make these things possible?"
    author: "Superstoned"
  - subject: "Re: my hope"
    date: 2006-06-06
    body: "The SOC project is just about making this stuff available for KWord, minus the real-time editing, but we're already researching that topic, too."
    author: "Boudewijn Rempt"
  - subject: "Why not OOo"
    date: 2006-06-06
    body: "I bet KOffice users hear this all the time, but why not just use OpenOffice? Sure, it's big and all, and some hippies may complain about it using Java, but at least it's fully-featured and supported. I tried KOffice a couple of times and it left me at the most unimpressed."
    author: "Josh Taylor"
  - subject: "Re: Why not OOo"
    date: 2006-06-06
    body: "Josh, you're perfectly welcome to use OpenOffice. I use OpenOffice myself, from time to time. Even though you would probably call me a \"hippy\" in the sense that I care about free software and don't give a fig about open source. \n\nBut, well, OpenOffice is not an interesting topic for developers and users of KOffice. KOffice can go places; we can innovate. It is impossible to innovate with OpenOffice because it's committed to be a carbon copy of Microsoft Office. And innovation is fun, and may well give users something worthwile. "
    author: "Boudewijn"
  - subject: "Re: Why not OOo"
    date: 2006-06-09
    body: "True, Boudewijn, that's what Freedom is all about--choice.  Like you, I too care more about Free Software than Open Source.  I hope that your work continues.  It's a shame that Microsoft's proprietary, closed file formats are so damned ubiquitous.  When ODF becomes a major player, I would have no problem using KOffice regularly, and I sincerely look forward to that day.  Then, the choice of office productivity suite won't matter except strictly on its merits.\n\nOpenOffice.org is very important for exactly what you \"accuse\" it of here.  Its job, at this point in time, is to be as \"MS Office-like\" as possible.  A really big part of that is its excellent MS Office file format compatibility, which, BTW, took some serious engineering.  We thus have a major weapon in our arsenal to get MS Office users to consider migrating to something that supports ODF, and thus perhaps KOffice in the future.  OpenOffice.org is the bridge that is enabling this to happen.  *That* is its chief \"innovation\", and boy, am I thankful for it."
    author: "Sum Yung Gai"
  - subject: "Re: Why not OOo"
    date: 2006-06-07
    body: "OpenOffice has more features currently than KOffice, but KOffice integrates much more nicely with my desktop than OpenOffice.\n\nI believe KOffice will be able to fix all of its shortcomings before OpenOffice can fix even half of its shortcomings.  I'd love to be proven wrong about this, but I seriously doubt it."
    author: "ac"
  - subject: "Why not MS Office?"
    date: 2006-06-07
    body: "Well, why use OpenOffice, why not use MS Office instead?\n\nSeriously: KOffice integrates better with KDE. KOffice is relatively lightweight and fast, whereas OpenOffice is not. And maybe some people simply prefer using KOffice instead of OpenOffice? Obviously there are lots of people using KOffice all the time. And before you say \"Yes but OpenOffice has a lot more users than KOffice!\". Well, MS Office has a lot more users than OpenOffice, so what's your point? And it's even MORE \"fully-featured\" and \"supported\" than OpenOffice is! Clearly we should all drop OpenOffice and move to MS Office instead!\n\nI use MS Office (at work). I have also used Koffice and OpenOffice. And I found both of them to be quite enjoyable. So why shouldn't I use KOffice?\n\nI don't understand this \"We should all just use this one particular piece of software!\"-mentality that seems to be creeping in to the Free Software community."
    author: "Janne"
  - subject: "Re: Why not MS Office?"
    date: 2006-06-09
    body: "First, MS Office is not, and probably never will be, Free Software.  OpenOffice.org and KOffice pass this test; MS Office fails.\n\nSecond, I refuse to use an office productivity suite whose native file formats are not truly open.  MS Office fails that test; both OpenOffice.org and KOffice pass it.\n\nThird, MS Office does not run natively on my platform (GNU/Linux).  Yes, I know about WINE and CrossOver Office.  I said *natively*, though, meaning \"the developer supports Free Software platforms.\"  Both OpenOffice.org and KOffice pass this test, too.\n\nI use OpenOffice.org at work, in a dyed-in-the-wool Microsoft shop, instead of KOffice or AbiWord/Gnumeric, because I need really good MS Office proprietary file format compatibility (I am one of two exclusively Free Software users).  KOffice looks like it has some potential, but it just doesn't cut the mustard for handling MS Office files.  I look forward to the day that ODF becomes a sufficiently major player that corporate offices will *have* to use applications that can correctly handle them; then, perhaps, I might be able to consider KOffice.  But not until.\n\nIn the meantime, OpenOffice.org does the trick very well for me.  For those for whom it's feasible, or even preferred, to use KOffice, by all means do so; it, too, is Free Software.  That's what matters to me most for any tool that I choose to use; it must be a Free-As-In-Freedom tool that does the job that I need."
    author: "Sum Yung Gai"
  - subject: "Re: Why not MS Office?"
    date: 2006-06-10
    body: "\"First, MS Office is not, and probably never will be, Free Software. OpenOffice.org and KOffice pass this test; MS Office fails.\"\n\nThe original poster was talking about functionality.\n\n\"Second, I refuse to use an office productivity suite whose native file formats are not truly open. MS Office fails that test; both OpenOffice.org and KOffice pass it.\"\n\nIt's fileformat is also the de facto standard, and it's support for .doc-files is overwhelmingly superior when compared to OO. And that was something very important to the original poster.\n\n\"Third, MS Office does not run natively on my platform (GNU/Linux). Yes, I know about WINE and CrossOver Office. I said *natively*, though, meaning \"the developer supports Free Software platforms.\" Both OpenOffice.org and KOffice pass this test, too.\"\n\nIt runs on about 98% of desktops out there. And we WERE talking about functionality and fileformat-support. Both are areas in which MS Office excels.\n\nFYI: I'm not REALLY advocating the use of MS Office, and my reasons mirror your reasons. I'm just arguing the \"OO does this better than Koffice, use OOS instead!\". MS Office does just about everything better than OO, so why not use MS Office then ;)?\n\nKoffice does do some things better than OO. And it \"feels\" different to OO. So some people simply prefer it to OO."
    author: "Janne"
  - subject: "Why is everyone so negative ?"
    date: 2006-06-06
    body: "Sure, Koffice is still at early development stage, but at least it is considering office work from a new point of view and stating, at this early stage, the basis of a good design to support all innovative stuff it will bring. AFAIK, this gives better results on the long term than jumping in the buffer and typing lines and lines of code, then hacking them. OOo actualy has a long history of code then think that leaded to the actual situation : Less than 10 people in the world are casual devs, the remaning being professional employees of companies doing it at full time. The reason ? It is a BIG stack of hacks not understandable for the non-fulltime dev.\nIn the meantime, at the time being, OOo is the most powerfull tool available for office tasks - I consider it sincerly better than MSOffice - , then I use it happily and everyone probably do the same. But they are struggling to maintain it and paying for the lack of good early stage design.\nBy and large, I am very confident about Koffice on the long term, and I don't really see an emergency need for the short term, so thank you KOffice people, and take your time to make good choices. :)"
    author: "paul"
  - subject: "Re: Why is everyone so negative ?"
    date: 2006-06-07
    body: "The problem with K-Office is that it isn't ported to windows. Even if the K-Office developers will make an amazing KOffice 4,  it doesn't help. Organizations will not like to train their staff on two different office suits. This would of course  be a problem for only have KDE throughout their organizations. Unfortunately such organizations are quite rare.\n\nOf course now that QT is GPLed for windows as well, a windows port would be possible. That would probably boost the interest in KDE and help make ODF a must to implement for every vender that sells an office suit."
    author: "Uno Engborg"
  - subject: "Re: Why is everyone so negative ?"
    date: 2006-06-07
    body: "KOffice 2 will have native (as in no need for X11 or other 'extra' layers) ports for both Windows and OS X (OOo still doesn't have a native port to OS X last I tried)."
    author: "Corbin"
  - subject: "True Type fonts are crappy..."
    date: 2006-06-06
    body: "\"Times New Roman\" and other MS TTFs are not printed nicely with KWord :( But other fonts like \"Century\" fonts look awesome. Please fix Printing (font issues). I still have to use MS Word for better printouts :(\n\nalso the one of the nicest fonts \"Bookman Old Style\" is rendered bold always :("
    author: "fast_rizwaan"
  - subject: "Re: True Type fonts are crappy..."
    date: 2006-06-08
    body: "Fonts are pretty much broken because they are broken in Qt3 (so we couldn't fix it in KOffice 1.x even if we wanted to).\n\nThe font issue is one of the things that will be fixed with KOffice 2.0 simply because it is already fixed in Qt4. Another one of these bugs that will be fixed because of the overall redesign."
    author: "Raphael"
  - subject: "KDE and Koffice True Type fonts are crappy..."
    date: 2006-06-10
    body: "This Bug report shows KDE fonts printing problem and some solutions.\nhttp://bugs.kde.org/show_bug.cgi?id=128912"
    author: "Fast_Rizwaan"
  - subject: "Love what you guys are doing!"
    date: 2006-06-06
    body: "Please keep going ahead, full speed. KOffice is fantastic -- lightweight and innovative. I look forward to further compatibility, stability and functionality."
    author: "Chase Venters"
  - subject: "Embedded documents not original to KOffice"
    date: 2006-06-07
    body: "\"When I first saw a paper on KOffice back in 1999 it showed the concept of embedded documents which allows me to have a formula or chart in my paper and allow me to update my charts by just altering some cells in my spreadsheet. The idea that an open source and good looking office suite does that brought me into KOffice.\"\n\nIn these incoherent sentences, are you claiming that KOffice is responsible for bringing this concept to office automation software? I'm sorry... we had some fabulous DOS applications which did this, and frequently did it better.\n\n"
    author: "Malcolm Dean"
  - subject: "Re: Embedded documents not original to KOffice"
    date: 2006-06-07
    body: "the key phrase i think you're skipping over is \"open source\"."
    author: "Aaron J. Seigo"
  - subject: "headers and footers"
    date: 2006-06-07
    body: "every application (kword, kspread, kpresenter) has a different way to define and / or to present the header/fotter setup.\nIMHO this should be unifyed "
    author: "ferdinand"
  - subject: "kspread formulas not updated correctly"
    date: 2006-06-07
    body: "As long as kspread does not update the formulas as other spread sheets do, it has a very limited user base.\n\nsee http://bugs.kde.org/show_bug.cgi?id=58652"
    author: "ferdinand"
  - subject: "Re: kspread formulas not updated correctly"
    date: 2006-06-07
    body: "Oops, yes that's a serious limitation.\n\nAt least it should be possible to choose."
    author: "KDE User"
  - subject: "Naming standard"
    date: 2006-06-07
    body: "Why all those strange names? Kexi? Karbon14?\n\nWhy not continue using the easy and catchy K-standard. Kbase, Kpaint, Kdraw, kplan etc. Much easier, sounds more professional"
    author: "ale"
  - subject: "Re: Naming standard"
    date: 2006-06-08
    body: "People have asked, myself included, and the short answer is that the people who make those apps actually like the weird and inconsistent names, and so there's really very little chance that they'll ever be changed.\n\nIn my opinion, that's a shame.  But that's where it stands.  My personal preference is to drop the K-conventions altogether, but my second choice is your suggestion--to pick one convention and use it everywhere.\n\nAlso: saying things don't look \"professional\" seems to drive people batty, even though I certainly know what you mean.  Stick to less value-loaded terms like \"consistent\" and you'll at least get a polite rejection."
    author: "ac"
  - subject: "Re: Naming standard"
    date: 2006-06-08
    body: "Excel is called Excel and not Microsoft Calc."
    author: "Martin"
  - subject: "Re: Naming standard"
    date: 2006-06-08
    body: "Yes, and MS is still having headaches because of the \"start\" button which is used to \"shut down\".\n\nOnly now in Vista does it seem like they can finally get rid of that mistake."
    author: "ale"
  - subject: "Re: Naming standard"
    date: 2006-06-08
    body: "Legal reasons, in many cases. Clashes in other cases. KImageShop and KIllustrator attracted the unwholesome attention of reichsanwalt von Gravenreuth. I'm afraid the same would happen to KBase. KPaint already existed, although it has now been superceded by Kolourpaint.\n\nIn general, I find it advisable to rename applications as little as posible, which is why all requests for better names will be met by my deaf adder impersonation."
    author: "Boudewijn Rempt"
  - subject: "Re: Naming standard"
    date: 2006-06-08
    body: "> I find it advisable to rename applications as little as posible\n\nI agree.\n\nBut, facing a major release such as KOffice 2 coming up, this might be the only chance to change the names, for years to come.\n\nIf the KOffice 2 series turns out to be as good as it promises, and with the possible MS Win userbase, now seems to be the time to do it right.\n\n"
    author: "ale"
  - subject: "Karbon14"
    date: 2006-06-08
    body: "where does this name come from, \nwhat is the purpose of \"14\""
    author: "Jim Der"
  - subject: "Re: Karbon14 due to carbon-dating."
    date: 2006-06-08
    body: "Carbon is known most by people for the concept of carbon dating; where the 14 comes from.\n\ngoogle for 'carbon dating' to find out more."
    author: "Thomas Zander"
  - subject: "Re: Naming standard"
    date: 2006-06-08
    body: "Kexi is a proper name, after a cake some people love. Just like Excel is a proper name. That said, look that \"Access\" is so weird name. Type \"access\" in Google and you won't find too db-related information. Type in \"Kexi\" and you will get lots of entries...\n\nMoreover Kexi, Krita are proper words. OTOH, KWord is not... that's why we could think about naming it \"KDE Word\" instead... Same for KSpread.\n"
    author: "anon"
  - subject: "Koffice is a killer app"
    date: 2006-06-07
    body: "Please keep up the good work. KDE needs its own Office Package for diversity and integration. \n\nKOffice is fast and nice!"
    author: "Thomas"
  - subject: "SCIM support?"
    date: 2006-06-08
    body: "Does KOffice support other languages? I've been having a hard time getting Japanese text into Kword via SCIM-anthy. Is there a way to do this at all?"
    author: "Neo-Rio"
  - subject: "Re: SCIM support?"
    date: 2006-06-08
    body: "Should work without a hitch -- at least for 1.5.1, Bart Coppens fixed scim input. I don't know Japanese and my Chinese is a long way in the past, but I've used KWord succesfully for Russian and Greek."
    author: "Boudewijn Rempt"
  - subject: "Re: SCIM support?"
    date: 2006-06-08
    body: "I successfully configured CJK input in any Linux application. Is really easy with kubuntu dapper, just install skim and anthy or any plugin you like and follow this guide to setup your enviroment. I can now write japanese in konsole also :D \n\nhttps://wiki.ubuntu.com/InputMethods/SCIM/CJK_Chinese_Japanese_Korean_Input_Method_configuration_using_SCIM_in_Ubuntu_6.06_Dapper_Drake\n\nI missed really much write furigana or ruby text in linux, I also tried with OpenOffice but the support it's really bad. Any intentions on implement this feature in Kword? Anyone knows any \"good method\" to write furigana in Linux?\n\nThanks in advance :)"
    author: "biquillo"
  - subject: "sounds great!"
    date: 2006-06-08
    body: "This sounds like a huge step forward, really building on the new rendering features in Qt4.  I'll be excited to see KOffice 2 :)\n\nI do have a concern or two though.  One is memory use.  If this core library does everything, and editing SVG in KWord doesn't load a new app/kpart, but instead works as just another tool, then won't that mean loading the entire KOffice suite for every app?  I assume you've thought of this, so I'm mostly just asking what cool technology I'm missing that allows you to do this :)\n\nSecondly, have their been any more thoughts towards document processing in KWord as opposed to just word processing?  I meant to get a proposal together for this, but I've been really pushed for time, and it's a pretty complex thing to cover in detail."
    author: "Lee"
  - subject: "Re: sounds great!"
    date: 2006-06-08
    body: "On memory usage;\nKParts work because they dynamically load (and unload) a library to do all the work.\n\nI've just incorporated loading of tools from dynamically loaded libraries via the same concepts. Shapes are next.\nThe cool technology behind this is actually the same; kparts from kdelibs. Well, the funny thing is that they were invented by koffice a loong time ago and moved into kdelibs later. But thats beside the point ;)\n\nWith a dozen separate libraries startup time might suffer, but there are many ways to make the user experience no less (and I don't mean splash screens!).\nYou have my word that it is our highest priority to make sure we are and will stay really fast and lean :-)\n\nI'm not sure what you mean with document processing."
    author: "Thomas Zander"
  - subject: "Re: sounds great!"
    date: 2006-06-08
    body: "After all, just mmapping the libs won't use much mem, now, would it? As long as the most important libs get loaded soon to avoid a slowdown with startup?\n\nI still don't fully understand this linker stuff, but I wonder how it could help here..."
    author: "superstoned"
  - subject: "not impressed"
    date: 2006-06-08
    body: "thats great, already look forward to the future... but kspread still doesnt compare to gnumeric or openoffice's calc... samething could be said about kword vs abiword and openoffice's writer. How long do we have to wait, how often do you need to re-write and re-design? people are after more features, and less bugs, the current koffice suite unfortantly offers the opposite."
    author: "Rex"
  - subject: "Re: not impressed"
    date: 2006-06-08
    body: "That's nonsense. KOffice 1.5 has more features and fewer bugs than 1.4; and the same was true for 1.4 version 1.3 and 1.3 version 1.2 and so on. Every version has more features and fewer bugs.\n\nAnd in any case, you don't have to wait. You're not being asked to wait. I for one don't care whether you wait or not. You're welcome to use other software, you're welcome to use ours, you're welcome to work on other open source office software, you're welcome to help us with ours. Take your pick -- but if you need to rant, get your facts right, or don't rant."
    author: "Boudewijn Rempt"
  - subject: "Re: not impressed"
    date: 2006-06-08
    body: "Indeed. I've been using Kword for all my work since what, 1.2 or something. Ok, it didn't always work smoothly, but what does? I love your work, Boudewijn & friends..."
    author: "superstoned"
  - subject: "Re: not impressed"
    date: 2006-06-09
    body: "Both Gnumeric and OpenOffice Calc have had a lot more input from experienced developers and it shows.  KOffice 1.5 is at least a start in the direction of fixing the basics but there is a long way to go.\n\nI think the route of the problem is that KOffice has an identity crisis.  As far as I can tell, nobody knows exactly where it is supposed to be going.  Krita and Kexi seem to have well understood roadmaps, but not the core apps.  \n\nOpenOffice.org on the hand has a clear purpose - a free alternative office suite which is easy for MS Office users to migrate from and which provides the same or similar features.\n\nGnumeric has very clearly stated goals on the front page of its site:  http://www.gnome.org/projects/gnumeric/\n\nKOffice doesn't.  Although the words \"comprehensive\" and \"integrated\" appear on its site, and they do turn up in the mailing list a lot, but they don't really mean anything.  The 'integration' is something which only really applies at the source level and aside from the use of standard KDE dialogs and ioslaves etc, doesn't mean anything else particularly from the user's point of view over and above that offered by OO.org or Microsoft Office."
    author: "Robert Knight"
  - subject: "Re: not impressed - beware of KDE bashers"
    date: 2006-06-09
    body: "I see a lot of organized KDE bashing in these forums lately. I don't know if they are paid for or they are merely pushing their rabid (gnome-only) POV.\n\nEither way, the bashing is relatively clever and 'sophisticated' - notice how the conclusion always draws attention to abiword, gnumeric and gnome 'office'. Although references are also made to OO.\n\nHaving tried using abiword/gnumeric many times over the years, I know that they are CRAP. In fact, in my memory abiword never loaded because some archaic fonts were missing ( it didn't start using GTK 2 until recently). Even after it started using GTK2, it had font issues. Gnumeric was better but always crashed when I tried to do anything at all with it.\n\nNow, I agree openoffice is a mature product and its probably the only such OSS office suite.\n\nI look forward to Koffice reaching its 2.0 milestone, brimming with elation,  and features. Meanwhile, I also urge gnome lackeys to give up their missionary zeal and actually be useful to the OSS."
    author: "v m"
  - subject: "Re: not impressed - beware of KDE bashers"
    date: 2006-06-11
    body: "point is, there IS no Gnome-Office. ok, a few totally not connected apps which happen to use GTK2 are sometimes refered to as Gnome-Office, but it takes more to be a office suite..."
    author: "superstoned"
  - subject: "Re: not impressed - beware of KDE bashers"
    date: 2006-06-11
    body: "Someone call the whaaambulance its an emergency!"
    author: "Stuart"
  - subject: "Re: not impressed - beware of KDE bashers"
    date: 2006-06-11
    body: "Seems like this article got linked from Desktop Linux, and that's where the 'Abiword and Gnumeric' crowd came in from."
    author: "gng"
  - subject: "Congratulations for KOffice"
    date: 2006-06-09
    body: "Seriously, congratulations for the work you're doing, keep it up!\n\nI have to admit I'm not a user of office programs on a regular basis, although I have to use them from time to time, and KOffice is actually the first office program that I didn't hate after 5 minutes of using it. I actually quite like it!\n\nI do not like MS Office, nor OpenOffice (because it's an exact copy), because of the way they push you to write documents that just seem \"hackish\" to me. KWord seems much more natural to use, and this even without the \"help\" from clippy (which is just an obnoxious hack/patch for bad interface design).\n\nAnd to all the people that complain: if you don't like it, don't use it. KOffice developers don't owe you anything, it's rather the opposite. I am thankful to them for doing what they do, and I can't wait for KOffice 2 to be out (with KDE4 and amaroK 2... ahhh...)\n\nAlso, I think they're doing the right thing, because fixing a bad design is _much more_ important than fixing little glitches here and there, accumulating patches over patches until you get a mess out of it (think linux vs windows). Being a programmer, I can't recall the number of times that rewriting something from scratch eliminated all the bugs I had that would have taken weeks of slow and painful and boring bug-fixing, when there was even no possibility for them to appear in the new design.\n\nAnyway, I just wanted to thank the KOffice team and encourage them to continue innovating so as to produce the *only* office suite that is pleasant to use (which is its major feature to me)\n"
    author: "Wackou"
  - subject: "More Linux Idiots"
    date: 2006-06-10
    body: "This is another example of Linux idiots at work:\n\n1) Why do Linux idiots always add layers of features onto non-working or broken code?\n2) Why do Linux idiots always hype their projects, while ignoring the serious problems and bugs?\n3) Does anyone really need, or want, or plan to use KOffice?\n4) Why not cancel KOffice, and have those developers clean up the mess that is the KDE desktop?"
    author: "Helms"
  - subject: "Re: More Linux Idiots"
    date: 2006-06-10
    body: "Tell me, did writing this little bit of tripe make you feel real good? Made you feel a superior man? Do you think it shows yourself as a fine, upstanding, cooperative, honourable human being? Did you genuinely think you were able to influence any of the people who spend more of their free hours per week on this stuff than is either healthy or advisable? Who, in all probability, know more about writing software than you? Are you proud of yourself, satisfied with yourself?"
    author: "Boudewijn Rempt"
  - subject: "Re: More Linux Idiots"
    date: 2006-06-11
    body: "i think someone should be able to remove posts like this..."
    author: "superstoned"
  - subject: "Re: More Linux Idiots"
    date: 2006-06-10
    body: "> the mess that is the KDE desktop\n\nStill, there are thousands and thousands of KDE desktops used in organisations everywhere worldwide."
    author: "ale"
  - subject: "Re: More Linux Idiots"
    date: 2006-06-11
    body: "> 3) Does anyone really need, or want, or plan to use KOffice?\n\nYes, I do. I want my office suite to be integrated well into my desktop environment - and as I run KDE, the best choice for that is KOffice. Sure, it may have a few issues, but it's improving. I look forward to KOffice 2.0.\n\nKeep up the good work guys, and ignore these worthless trolls."
    author: "Paul Eggleton"
  - subject: "Re: More Linux Idiots"
    date: 2006-06-11
    body: "well, you don't really have a choice. OO.o is a pile of crap, big, bloathed, unstable and missing many integrative things like proper KIO support. Koffice might not have as many features as MS office and OO.o, but at least it WORKS..."
    author: "superstoned"
  - subject: "Re: More Linux Idiots"
    date: 2006-07-03
    body: "The idiocy is not limited to Linux, but I agree that it is stupid and irritating the way more slow buggy features are layered on top of the existing slow buggy features. Far better to have a stable responsive app that can actually be relied upon to work. At present this is a strength of KOffice (over OpenOffice.org) so I hope these things take priority over feature-bloat.\n\nHaving said that, some features are important because they're widely used, and it's equally annoying to see them ignored for the sake of gimmicks that may look impressive but aren't really useful. KWord is a word processor. It's nice that it has desktop publishing features, but most people use word processors for business documents, letters, academic papers, etc. I don't give a stuff if I can rotate my text frame 45 degrees to the left. I want to be able to sort tables, work collaboratively on long documents with users who may be running MS Word, etc. etc. Until KOffice does its core business well, I couldn't care less about its bells and whistles."
    author: "s"
  - subject: "Re: More Linux Idiots"
    date: 2006-08-22
    body: "In case you hadn't noticed what they are going to do is not so much bells and wistles, but creating a framework where improved code on one program immediately affects another program, this way resources are spent better, KDE's main feature are these frameworks that allow easy development afterwards. It's why amarok has become so good in such a short time for example. Creating something similar for Koffice will help in improving koffice faster in the future."
    author: "terracotta"
  - subject: "KChart better plotting?"
    date: 2006-07-02
    body: "Was wondering if there would be better plotting capabilities in KChart and flexibility in data plotting with KSpread (plotting multiple datasets in same graph...). Such a feature would definately be of use to scientists and engineers, and perhaps others in related fields..."
    author: "Prashanth Kumar"
  - subject: "The real challenge for an office suite"
    date: 2006-07-07
    body: "I really like KOffice, i wish, i wish i could use it at my job. So this is why i want to share what i have been thinking about while working in my office.\nI use 'that' office suite at work. Have no choice. It's no fun, but on the other hand, the differences with KO and OOo are actually quite small. There is no fundamental difference between the three. KOffice should make a fundamental difference.\nI think i might have an idea for such a difference. Many people who work in an office have a modern desk top computer or a laptop, but still they have a pile of paper on their desk and a virtual pile on their hard disk. This is because most knowledge workers make their product out of a great amount of very non-isotrope information. A report is almost always based on a collection of emails, .docs, articles on paper, pages from books, webpages, .pdfs etc. \n\nNowadays, there is only one way to order all this information: printing it all, and using lots of Post-It memo's. No joke. I'm considered a nerd, but i don't see any other solution. Printing & Post-It.\n\nIf KOffice wants to make a difference, tackle this. Please, drive Post-It out of business. One possibility would be to make an integration between Konqueror, KWord and some mindmapping-tool. On your screen, it should be possible to order your sources (i.e. the many totally different documents which form the base of your product), the essential parts of those sources (i.e. quotations with clear links to the sources), and the text you are writing.\n\nThe easy way to implement this is a three-pane window: one for your text, one showing the contents of your 'sources' folder on the HDD, and in between an ordered listing of the relevant quotations from the source.\n\nProbably there are better solutions. I am not a hacker, i cannot make this myself, that is why i try to share my experiences as a knowledge worker. I hope someone from the KOffice team gets inspired by my story. I really think the challenge for KOffice is getting beyond Printing & Post-It. "
    author: "walter hoogerbeets"
  - subject: "Re: The real challenge for an office suite"
    date: 2006-08-04
    body: "Sounds like you're describing BasKet Note Pads:\n\nhttp://basket.kde.org"
    author: "Conrad"
---
<a href="http://www.koffice.org">KOffice</a> is working on its future, one based on KDE4. KOffice is starting new initiatives with libraries like <a href="http://wiki.kde.org/tiki-index.php?page=Flake">Flake</a> and <a href="http://wiki.kde.org/tiki-index.php?page=Pigment">Pigment</a> that are going to be used for all KOffice applications.  For the users of KOffice those changes are invisible until the 2.0 previews actually start to appear some months from now.  Therefore the KOffice crew wants to show you their goals of what KOffice 2 is going to look like.  Read more for the whole story.







<!--break-->
<p>When I first saw a paper on KOffice back in 1999 it showed the concept of embedded documents which allows me to have a formula or chart in my paper and allow me to update my charts by just altering some cells in my spreadsheet.
The idea that an open source and good looking office suite does that brought me into KOffice.</p>

<p>Now, many years later, we are moving to the next level.  KOffice will define so called 'shapes' which can be anything from a simple triangle to a multi-layered image and allow those to be used in all KOffice applications as the building blocks of a document just like they are shapes that the application provides itself.  So no more embedded documents which are always square and always start from the top-left corner. This allows for simple things like KWord finally providing simple lines to be drawn, but much more exciting are new features like being able to rotate or skew a KWord text frame. And not only being able to do that in a KWord document, but also in a Krita or a Karbon document.

<p>In KOffice each application will have a specific media that it will specialise in. KWord will obviously specialise in text frames while Karbon will specialise in all sorts of vector graphics.  The difference is that the applications will put all that in a Shape and a Tool.

<p>Consider a user who wants to write a paper and have a nice vector graphic in the header of his document.  He would be able to load an svg graphic and place it in his KWord document. For tweaking the loaded graphic he can just click on it and at that point KWord will see that the shape is a vector one. KWord will then supply a tool in the toolbox (which is the normal 2-columns toolbar type thing Krita already has) that allows the user to alter the internal vector graphics right inside his KWord document. But without the annoying flickering and replacement of menu and toolbars that happens if the shape was really an external document being edited.

<p>KOffice 2 will still have the most applications there are combined in any single office suite, and on top of that those applications will show an integration that's unparallelled in the industry.  Each application really does make the whole more complete by literally making the other applications more powerful.

<p>Every application that uses the Flake library will provide a shape-type that the other applications can use.  Want to use that 1 cool Kivio-shape in your Krita painting, go ahead.  But the most exciting part is that the shapes come with so called 'tools'.  Where each application can use the interaction model for that shape type. And since its determined per shape-type, its the same across all the KOffice applications.</p>

<p>We already see the advantage with a basic interaction-tool that allows moving, rotation etc. of all shape-types.  It has features like scaling with the control button down, reserving the aspect ratio of the shape. Unlike in KOffice 1.x that single interaction-tool will be used in all KOffice applications stopping the application from reinventing the wheel, they are literally all running the same code and thus all applications will work consistently towards the user.  Definitely a good thing for usability.</p>

<img src="http://static.kdenews.org/jr/kword-flake.preview.jpg" width="400" height="199" /><br />
<strong>Flake in KWord 2</strong>





