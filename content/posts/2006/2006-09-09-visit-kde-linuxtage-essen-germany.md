---
title: "Visit KDE at the LinuxTage in Essen, Germany"
date:    2006-09-09
authors:
  - "cniehaus"
slug:    visit-kde-linuxtage-essen-germany
---
KDE will be present on the <a href="http://www.come2linux.org">LinuxTage Essen</a> in Germany from September 9th to 10th. A number of KDE developers will be present, including some of the famous <a href="http://amarok.kde.org">Amarok</a> hackers. Visit us to see the latest in KDE and tell us what you love and what needs improved.



<!--break-->
