---
title: "Technical Working Group Elected"
date:    2006-02-11
authors:
  - "sk\u00fcgler"
slug:    technical-working-group-elected
comments:
  - subject: "Employers "
    date: 2006-02-11
    body: "It has been said that Novell was not supporting KDE, but suprising three of these guys are employed by them. This is exciting to know.\n\nOn the other hand, I believe David Faure used to work for Mandrake... It's disapointing to see that they don't employ no-one in this group, since they are a pretty big distro I think."
    author: "blacksheep"
  - subject: "Re: Employers "
    date: 2006-02-11
    body: "It doesn't really matter where someone works, but what he has done and what we think he can do for KDE. The members were not appointed or elected by companies, but by the members of the KDE e.V., read KDE contributors and developers of all kind. "
    author: "Andras Mantia"
  - subject: "Re: Employers "
    date: 2006-02-11
    body: "Laurent Montel, employee of Mandriva, is working part time for KDE ans is the top current contributer to porting KDE to Qt4 according to Adriann de Groot's statistics.\n\nDavid Barth, CTO of Mandriva, asked me at Solutions Linux how he could collaborate with KDE to integrate the Mandriva hardware database into Solid.\n\nSo, indee, Mandriva is very helpful to KDE with their limited means."
    author: "Charles de Miramon"
  - subject: "Re: Employers "
    date: 2006-02-11
    body: "\"It has been said that Novell was not supporting KDE, but suprising three of these guys are employed by them. This is exciting to know.\"\n\nJust goes to show, you shouldn't believe everything you hear."
    author: "Segedunum"
  - subject: "Re: Employers "
    date: 2006-02-11
    body: "> It has been said that Novell was not supporting KDE, but suprising three of these guys are employed by them. This is exciting to know.\n\nSUSE has been a strong KDE contributor. Let's hope they remain that after next round of layoffs in Novell :/. Looks like Novell is on the journey of building its own middleware with full control over it (mono). For a billion dollar company something like this HAS to be the goal.\n\nAnd they have a valid point for doing this Gnome wise as well. Investing to Gnome with its current 'application framework' is like contributing your money to poorest areas of Africa. Sure, it does some good, but generally not much. Give man a bread and he'll live for a day. Teach him how to do it and he'll live forever..\n"
    author: "Anonymous Coward"
  - subject: "\"those who do the work decide\""
    date: 2006-02-11
    body: "But what do we do when those who do the work make the wrong decision?\n\nThe point here is that the KDE project has become quite large and it is used commercially.  Therefore, we can no longer afford to make releases that contain design errors.\n\nSee bugs: \n\nBug 106542: Code is *directly* dependent on \"crystalsvg\"\nBug 107087: The profiles \"filemanagement\" & \"webbrowsing\" are in the code.\nBug 116562: REGRESSION: The icons don't ignore autohide panels\n\nThe first two are simple design errors that shouldn't have happened.  The third appears to have been a \"wrong decision\".  I beleive that it was caused by commit 444012.  This regression make no difference with the default DeskTop setup, but if you have a complicated setup -- specifically if you use the External Taskbar -- it completely screws up the DeskTop.\n\nAnd then there is the general issue of the Icon Themes which doesen't appear to be well thought out.  There are also bugs in the icon search code.  The result is that if you don't use CrystalSVG, you have a lot of problems -- and we do not properly support HiColor since the HiColor icons were renamed KDEClassic.\n\nSo, I beleive that one of the tasks for the TWG is to see that such design errors do not find their way into released code, and that design decisions are made before they are coded -- design is an important part of software engineeing that is often neglected in OSS.  \n\nPerhaps this will require a change in the way we structure releases.  For example: The GIMP has two release series, a stable release and a development release.  Changes are first added to the development release and if they work out they are then added to the stable release branch.  \n\nPerhaps there are other methods that would be suitable.  The point is that the KDE method of not having a seperate development branch appers to the the cause of our problems with code whch should be considered experimental being included in what are supposed to be stable releases.\n"
    author: "James Richard Tyrer"
  - subject: "Re: \"those who do the work decide\""
    date: 2006-02-11
    body: "\"Perhaps this will require a change in the way we structure releases. For example: The GIMP has two release series, a stable release and a development release. Changes are first added to the development release and if they work out they are then added to the stable release branch.\"\n\nDon't you realise that that is exactly what happens now?"
    author: "ac"
  - subject: "Re: \"those who do the work decide\""
    date: 2006-02-12
    body: "> Don't you realise that that is exactly what happens now?\n\nNo it isn't.  Bug fixes are made on the current release branch.  However when the 3.x.0 releases were made, the current HEAD branch was tagged and after Beta and RC releases it was released.\n\nIf I have missunderstood anything, it is the way which The GIMP and other projects with two parallel branches works.  If I have done so, my point remains; we need some way to experiment with new features in a way that they are not automatically included in the next release without further review."
    author: "James Richard Tyrer"
  - subject: "Re: \"those who do the work decide\""
    date: 2006-02-12
    body: "And how could anyone except those already doing it 'experiment with new features' if they are not released?\n\nI (actually my wife) found a few bugs in the 3.5 release and they were in features that advanced users would rarely use. They became obvious after a release and were fixed in the point releases.\n\nSure it would be nice if there were no regressions, but that's the way it comes. Users are part of the process. This is a feature.\n\nI am reassured by the comments from the candidates when they say they will work to 'keep KDE very free of bureaucracy'.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: \"those who do the work decide\""
    date: 2006-02-12
    body: "> And how could anyone except those already doing it 'experiment with new \n> features' if they are not released?\n\nPerhaps this is why other projects have a series of development releases -- so that users (that choose to be testers) can test such new features without  putting them in what is supposed to be a stable release.\n\nI am not talking about minor bugs but rather new features which seem to find their way into releases before they are ready for \"prime time\".  I agree that minor bugs are inevitable (that is why there are x.y.1 releases) -- it is major problems that we need to keep out of stable releases.  And, if you review the history of the KDE-3 releases you will see that there were major problems in many of the releases.  We need to address this problem as it is a major impediment to the commercial success of KDE."
    author: "James Richard Tyrer"
  - subject: "Re: \"those who do the work decide\""
    date: 2006-02-13
    body: "> so that users (that choose to be testers) can test such new features \n\nThat's the problem. There aren't enough users who do that. Otherwise our beta and rc releases would be much more successful in finding these bugs _before_ the release."
    author: "ac"
  - subject: "Re: \"those who do the work decide\""
    date: 2006-02-13
    body: "Actually, these problems need to be found _before_ the Beta release."
    author: "James Richard Tyrer"
  - subject: "Re: \"those who do the work decide\""
    date: 2006-02-14
    body: "Nope, sorry you're wrong. We do beta releases to find the bugs. It's a \"it is stable here please test\" release. I have many bugs on bugs.kde.org that aren't reproducable here. How am I supposed to find these problems before the beta release????"
    author: "Christian Loose"
  - subject: "Re: \"those who do the work decide\""
    date: 2006-02-14
    body: "> How am I supposed to find these problems before the beta release????\n\nQA testing.  Testing by the developer adding the new code.  This is what I do.  Other developers commit code without even checking to see that it will compile.\n\nBut please note that I was talking about major bugs (specifically: regressions) and new features that aren't really ready for release yet.  We shouldn't have to make a Beta release to find these."
    author: "James Richard Tyrer"
  - subject: "Re: \"those who do the work decide\""
    date: 2006-02-15
    body: "> QA testing. Testing by the developer adding the new code. This is what I do.\n\nYou still don't understand. We all test our code but some bugs just don't happen on our computers because of different setups on the user's computer or even different OSs (BSD, Solaris, etc.).\n\nSimple example:\nHow should the K3B developer find the bug 79701 (http://bugs.kde.org/show_bug.cgi?id=79701) *before* a release when he doesn't own a Pioneer DVR-104?\n\n> Other developers commit code without even checking to see that it will compile.\n\nThis definitely only happens because of a mistake during compile or check-in.\n"
    author: "Christian Loose"
  - subject: "Re: \"those who do the work decide\""
    date: 2006-02-16
    body: "> You still don't understand. We all test our code\n\nConsidering some of the bugs which I have reported and commented on, I find it hard to believe that all code is actually tested before release.\n\n"
    author: "James Richard Tyrer"
  - subject: "Re: \"those who do the work decide\""
    date: 2006-02-11
    body: "Are you serious?\n\n>Bug 106542: Code is *directly* dependent on \"crystalsvg\"\nI would not call that a design error, more like a ordinary bug. And not a important one at that either, since it's a corner case and the effect of it is more annoying than serious. It should obviously get fixed, but it's understandable if it has low priority among the developers.\n\n>Bug 107087: The profiles \"filemanagement\" & \"webbrowsing\" are in the code.\nPerhaps it's ok to label this one as design error. But I think it's more a symptom of the whole profile handling code being somewhat broken and ugly. Basically a minor issue of a bigger problem, as it's well known that the whole profile management needs some fixing.\n\n>Bug 116562: REGRESSION: The icons don't ignore autohide panels\nWhich is neither a bug or a \"wrong decision\", but correct behavior even if you fail to see it. It removes corner cases and problems. The default and most used configuration are permanent panels. The most important thing for desktop icons are that they NEVER should be placed behind a fixed panel. This gives you only 3 options for correct behavior in scenarios with autohide panels.\n- Recalculate and move the icons every time you switch between fixed and autohide of panels, which is bad and annoying.\n- Remove the ability to change a autohide panel to fixed, and by that removing the need to rearrange the desktop(also bad). \n- Or the current behavior not allowing icons behind panels at all, removing the spurious rearranging of the desktop. \n\nOf course you can argue for a configuration option to brake the correct behavior, but that's definitely a case where those who do the work decide."
    author: "Morty"
  - subject: "Re: \"those who do the work decide\""
    date: 2006-02-12
    body: "Yes, I am serious.  I reported these bugs because they were causing problems.  And, yes these are design errors because someone made a design decision and the decision caused problems.  Unfortunately, your response is indicative of the corporate culture of KDE where the first reaction is to try to explain away bugs rather than fix them.\n\nRegarding the bugs:\n\n106542:  You might argue that this isn't a serious bug, but it illustrates the lack of design coordination.  Someone appears to have correctly designed this with the link: \"default.kde\" to point to the default icon theme but then someone else wrote the code and didn't use it -- instead they put the name of the current default theme in the code.  So, we can't change the default theme without changing the code.  The result of this design error is that if the CrystalSVG icon theme is not installed, KDE will not run.\n\n 107087: IIUC, the logic of you position is that there were a lot of other design errors in this code so this one doesn't really matter.  WHAT!  But, I have to agree with you, it is a kludge.  This makes my point that it was not sufficiently designed before it was implemented.  But, the issue here is that the names of the default Konqueror profile and default profile when called with \"kfmclient\" should be in a configuration file, not in the code.\n\n116562:  This is simple.  The developer that made the wrong decision appears to have used your reasoning but it is wrong.\n\n> - Recalculate and move the icons every time you switch between fixed and autohide of panels, \n\nThis is the best default behavior, but it could be made configurable.\n\n> which is bad and annoying.\n\nWhy?  Your position is based on the presumption that users often switch between fixed and autohide  panels.  I NEVER change this.  Do other users actually change this option often?  Is it better to have LARGE areas of the DeskTop where you can't place icons?\n\nBut, did you read my bug report and look at the screen shots.  This isn't just about autohide panels.  I have a small panel in the upper right.  This is not autohide and the DeskTop is reserving space for it that it NEVER uses.  \n\nYou, and the developer that did this, may have thought that this was a good idea, but look at my screen shots to see the result.  My DeskTop configuration was totally screwed up by this change.  Large areas of my DeskTop that are never covered by panels are reserved places where I can no longer place icons.\n\nI offered these as examples of design errors.  The first two are small errors, but I believe that if someone had given it a little thought that they wouldn't have done it this way.  These are just two examples which I found.  I have no idea how many other similar small design errors exist.\n\nThe third example indicates why \"those who do the work decide\" doesn't work -- at least not the way some construe it.  Someone decided to make a large change to the DeskTop without considering any possible adverse consequences.  This (considering adverse consequences) is an important part of the engineering process.  I am simply suggesting that the TWG needs to address these issues.\n\n\n"
    author: "James Richard Tyrer"
  - subject: "Re: \"those who do the work decide\""
    date: 2006-02-12
    body: ">Unfortunately, your response is indicative of the corporate culture of KDE\n>where the first reaction is to try to explain away bugs rather than fix them.\n\nNot so, don't try to invent something that does not exist. I just points out that those \"serious\" issues and \"design errors\" of yours is no more than minor annoyances, and with hundreds of more important bugs it's not surprising they get low priority among the limited pool of developers.\n\n\n>106542: [..]Someone appears to have correctly designed this with the link:\n>\"default.kde\" to point to the default icon theme but then someone else wrote\n>the code and didn't use it -- instead they put the name of the current default\n>theme in the code.\n\nYou are confusing design error with a buggy implementation.\n\n\n>The result of this [bug] is that if the CrystalSVG icon theme is not\n>installed, KDE will not run.\n\nWhich explains why this bug does not have a high priority, since you really have to go out of your way to manage to install KDE without the default icon theme.\n\n\n>107087: IIUC, the logic of you position is that there were a lot of other\n>design errors in this code so this one doesn't really matter.\n\nDon't try to apply meaning to my words which was not there. What I said was that you try to make a major issue out of what essentially are only a small part of a well known kludge. And it's rather apparent that even the well known kludge are not a big issue, since it more or less does the job it was intended to do. Sometimes the initial design and implementation of a feature are not as good as it should and need redesign, it's just the way things are. Given that the majority of the code being what? 5-6 years old? With well known problems, it SHOULD give you a indication how important the issues are compared to others. So the logic of my position says that it's only a small part of a bigger well known problem, which by the way are not very important. Thus I prefer that the developers spend their limited resources on the more important issues.\n\n\n>116562: Your position is based on the presumption that users often switch\n>between fixed and autohide panels.I NEVER change this. Do other users\n>actually change this option often?\n\nThe whole reworking of that part of the code was as far as I can tell related to several reported issues and complaints about spurious reordering of icons. Describing why the users find it both bad and annoying. I have known users who switch between those modes depending of the tasks preformed, so you can't really assume your working pattern are the most common.\n\n>did you read my bug report and look at the screen shots.\n\nYes I did, and it's rather obvious from the screenshots that your setup is far from what the majority of users would even consider to do. Based on that most users will not have such a panel overloaded setup and the actual KDE defaults, combined with users obvious dislike of automatic icon reordering the current behavior is the best. Having a option to change this for the small minority of users like you, are of course a possibility. But perhaps those who do the work decide it's not worth the extra effort, or they have more important things to do.\n\n\n>I offered these as examples of design errors.\n\nTwo of which is not design errors at all. And one that are only a small part  of a small well known problem, which the developers will fix when they get around to it. That's why I asked if you was serious, trying to make some of your pet issues into design errors. \n\nNot that genuine design errors don't exist in KDE, even as a non developer I  know of several. Some may be avoided when developing new code, but the problem are that such errors sometimes only become apparent over time and then requires lot of work to fix. Ask the KMail/Kontact developers about it sometime.     "
    author: "Morty"
  - subject: "Re: \"those who do the work decide\""
    date: 2006-02-13
    body: "> Not so, don't try to invent something that does not exist.\n\nUnfortunately, it does exist, and it most certainly is not unique to KDE.  Your replies are clearly indicative of the problem -- you are trying to find ways to say that I am wrong rather than discuss my points.  This accomplishes nothing.\n\n> You are confusing design error with a buggy implementation.\n\nYou must have missed reading the first sentence of the paragraph which you replaced by \"[..]\".  Actually, you are applying a definition of 'design error' which is different than mine.  This is simply quibbling over semantics; it has nothing to do with what I am saying.  I am saying that when a bug is introduced by a design decision that it is a design error.  Other sources of bugs are usually considered to be coding or logic errors.\n\n> Don't try to apply meaning to my words which was not there. What I said was \n> that you try to make a major issue out of what essentially are only a small  \n> part of a well known kludge\n\nAnd you do exactly what you falsely accuse me of.  I stated that I was using this as an example of a design error.  I did not say that it was a \"major issue\" only that it was an issue for me because it caused me a problem (which is why I reported the bug).  I stated that I agreed with you that the whole thing was a kludge but this does not mean that the example is not valid.\n\n> I have known users who switch between those modes depending of the tasks \n> preformed, so you can't really assume your working pattern are the most  \n> common.\n\nI make no such assumption only that the pattern of configuring you desktop and leaving it that way for weeks or months at a time is probably a common modality.  If there are a significant number of users that change the configuration often this is something which should be considered but it does not mean that what appears to be the normal way to do it should be neglected to accommodate what you have described as a \"corner case\".  It does not appear likely that this modality would be common, but I have no data (and neither do you).\n\n> ... from the screenshots that your setup is far from what the majority of \n> users would even consider to do. Based on that most users will not have such \n> a panel overloaded setup and the actual KDE defaults, combined with users \n> obvious dislike of automatic icon reordering the current behavior is the \n> best. \n\nAs I said above, your first response is to tell me why I am wrong rather than to discuss the issue.  This is the KDE culture which needs to be changed.  I can only suggest that you look at the bug report again:\n\n1.  As you would see if you had looked carefully, most of the problem is caused by the autohide External TaskBar.  That problem would still be the same even if I removed all of the other panels.\n\n2.  I still have the problem with \"automatic icon reordering\" (read the bug report!).  Actually to say that I \"still\" have it is inaccurate because I don't have it on my KDE 3.4.3 desktop but everytime I start up the test desktop for 3.5.1, the icons change position -- the problem is not fixed and in my case it has regressed.\n\n> Two of which is not design errors at all.\n\nAnd what is your defintion of \"design error\".  It should be obvious that they are 'design errors' according to how I defined it."
    author: "James Richard Tyrer"
  - subject: "Re: \"those who do the work decide\""
    date: 2006-02-14
    body: ">Unfortunately, it does exist, and it most certainly is not unique to KDE.\n>Your replies are clearly indicative of the problem -- you are trying to find\n>ways to say that I am wrong rather than discuss my points. \n\nSadly you missunderstands some very basic facts, and attribute your wrongly reasoned understanding to some kind of mythical corporate culture.  When it in fact all boils down to common sense and basic resource management. Since KDE has very finite resources, the developers rightly prioritize the most important tasks. Since your pet issues are not, they rightly get less developer attention. So it's not about finding ways to say you are wrong, I'm just explaining that fact to you. But you seems incapable of comprehending that, and instead thinks there is some KDE cultural thing working against you when your issues are rated as less important. \n\nIf you want to accomplish something why don't you find some real design issues and try to make a point with them. Rather than keep nagging on those minor issues, find something that deserves the TWG attention. "
    author: "Morty"
  - subject: "Re: \"those who do the work decide\""
    date: 2006-02-14
    body: "> some kind of mythical corporate culture\n\nAll groups develop a corporate culture.  This may be good or bad, but it does exist -- it is not a myth.\n\nWhy do you describe as minor issues two important design errors?  Is it the normal KDE way to address such an issue?  If so, then it is the corporate culture.\n\nWhy are these two small design errors important?\n\nThese small errors are important because they are at the root of a lot of code.  When code is written based on root design errors ... As time goes on. more and more code is written based on these root design errors.  The result is that these very small errors become very hard to fix.\n\n\"a stitch in time, saves nine\"\n\nAnd if they cause bugs the problem is worse.  Bug fixes for bugs caused by small design errors can also make maintainability worse if the bug fix doesn't go back to the actual problem and remedy it."
    author: "James Richard Tyrer"
  - subject: "Re: \"those who do the work decide\""
    date: 2006-02-14
    body: ">Why do you describe as minor issues two important design errors?\n\nSince they are not, only one of the issues you describe can is an actual design issue at all. And since the current implementation work in most cases and problems created by the profile handling code are minor, making the whole issue minor.\n\n\n>Is it the normal KDE way to address such an issue? \n\nIt's common sense to address the bigger issues with higher priority, with limited resources that makes minor issues like these less important. Nothing directly KDE related in that, but it prove that most KDE developers have common sense.\n\nWhy are these two small pet issues of yours less important?\n\nThe current implementation works, in most scenarios and most importantly in the default configuration. With the icon issue you have to deliberately remove part of the default install to make it break. And the only thing depending on the profile handling are the profile handling, making the functionality it provides less than optimal(but the default still work).\n"
    author: "Morty"
  - subject: "Re: \"those who do the work decide\""
    date: 2006-02-16
    body: "> only one of the issues you describe can is an actual design issue at all ...\n\nSince we don't have your definition of \"design issue\" and you appear to reject my definition, your statement has no meaning.  If not a design issue, is the other one (which I understand to be the icon issue) a coding error, a logic error, or some other type of error?  It doesn't matter.  You are saying that because I have characterized it in a way which you say is wrong that it isn't important.  Whatever type of error you wish to call them, I have explained why they are important.  You have simply ignored the explanation and responded with useless rhetoric.\n\n> It's common sense to address the bigger issues with higher priority ..\n\nYes, but what do we have to establish that these are minor issues except for your say so?  The KDE icon search method does not conform to the FD.o spec.  Are you saying that this is a minor issue?\n\nSo the question:\n\n>> Is it the normal KDE way to address such an issue? \n\nremains unanswered (and apparently not understood).  The question is whether it is the KDE way to talk down bugs rather than fixing them?  I, and many other users, do not appreciate such presumptuousness (substitute for the 'A' word).\n\n> The current implementation works, in most scenarios\n\nThe icon search only works in one scenario.  If you use any other icon theme besides CrystalSVG, you have problems -- the icon search is broken.  A possible work-around to this would be to remove the CrystalSVG icon them if you didn't want to use it, but you can't do that because KDE then refuses to run (and that is how I found this error).  So, if we want to try to fix the bug (the icon search is broken) what would we do first.  I suggest that we should first remove what appears to be a basic design (or some other type of) error.  It is still a basic (or root) error no matter what how you characterize it that the code should not be dependent on \"CrystalSVG\" (The string \"crystalsvg\" should not be anywhere in the code).\n\nAnd getting back to limited resources.  It would be a much better use of these limited resources to make sure that design errors don't find their way into the code base as it takes a lot more work to fix them later."
    author: "James Richard Tyrer"
  - subject: "Re: \"those who do the work decide\""
    date: 2006-02-16
    body: ">I have explained why they are important. You have simply ignored the\n>explanation and responded with useless rhetoric.\n\nNothing in your explanation do anything to disapprove what I have explained to you in very simple terms, based on the consequences of those bugs they are not important. But you are to wrapped up in your own issues and fails to see simple reason. \n\n\n>> It's common sense to address the bigger issues with higher priority ..\n>Yes, but what do we have to establish that these\n>are minor issues except for your say so?\n\nAny competent engineer should be able to grade the importance of issues based on their actual impact. And the fact that the developers have not bothered to fix them yet, would suggest they don't rate them as important either. \n\n\n>The KDE icon search method does not conform to the FD.o spec.\n>Are you saying that this is a minor issue?\n\nYes, since the nature of the bug apparently makes KDE fallback to CrystalSVG, the KDE default, rather than Highcolor for icons not present in the selected iconstyle. Not folowing the design or FD.o spec are of course not good and should get fixed(I have never said anything else), but it's rather far from important compared to other issues. You know real issues like crashes, hangs, dataloss or things that affect all users. \n\n\n>So the question:\n>>> Is it the normal KDE way to address such an issue? \n>remains unanswered (and apparently not understood). \n>The question is whether it is the KDE way to talk\n>down bugs rather than fixing them? \n\nI think the lack of understanding are on your part, I plainly told you that common sense would dictate the current practice of giving higher priority the more important tasks. It's not about talking down bugs, it's just explaining the fact that the developers have judged your bugs less important than the bugs they are currently working on fixing. And it's not only annoying, but insulting to all KDE contributors that you accuse the whole comunity everytime someone explain simple facts you don't appreciate. \n\n\n>If you use any other icon theme besides CrystalSVG, \n>you have problems -- the icon search is broken.\n\nYou don't get hicolor icons as replacement for those your iconstyle don't include, but CrystalSVG. Except for people with severe crystal allergy, it does not have a big impact.\n\nSo instead of \"The current implementation works, in most scenarios\" I should have said: It works for the majority of users running the default, and for user who don't care if the fallback icons are CrystalSVG rather than Hicolor. I should think that would cover most scenarios.\n\n\n>The string \"crystalsvg\" should not be anywhere in the code \n\nHardcode something that's supposed to be interchangeable are a simple bug.\n\n\n>It would be a much better use of these limited\n>resources to make sure that design errors don't\n>find their way into the code base as it takes a\n>lot more work to fix them later.\n\nAs you said, useless rhetoric. "
    author: "Morty"
  - subject: "Re: \"those who do the work decide\""
    date: 2006-02-20
    body: "I have reached the conclusion that you actually do not understand my position:\n\n1.  Small design errors that remain in the code base for a while become enmeshed in the code and become more and more difficult to fix -- these small errors can become pernicious as code is maintained and kludges are added to compensate for the root design errors.  If you doubt this, try to fix the icon example -- simply replace \"crystalsvg\" with a references to the link \"default.kde\".\n\n2.  You presume that these small design errors have no serious consequences.  But, you have no way of knowing this.  You correctly state PART of the problem with the broken icon search.  CrystalSVG icons show up when they shouldn't AND, the string \"crystalsvg\" appears in the code.  By induction, a compentent engineer would conclude that there is a probability that these might be related.  A competent engineer would not accept your rationalizations.\n\nI look at the broken icon search code in a different light.  I wonder why code which doesn't do what it is supposed to do was ever added to the code base.  Obviously, there was no (or at least not sufficient) QA testing.  I don't know how much of the problem is due to coding errors and how much is due to insufficient design and/or to logic errors -- what I do know is that it doesn't work.  To be clear, I am not saying this to insult anyone.  I am trying to make the point that a methodology which produces code which doesn't work correctly and then rationalizes away the need to fix the broken code needs some rethinking.  Your way of thinking is often criticized in the literature of software engineering -- it results in a code base which, over time, becomes an unmaintainable mess.\n\nSo, I hope that you can see that I understand your position.  It is just that I totally disagree with it (And, I believe that your position is part of the KDE corporate culture).  You are simply wrong when you state the position that code that works correctly most of the time only has minor problems; you have no way of knowing that -- what we know is that such code is not \"correct\".  You minimize \"corner cases\" while failing to understand why software crashes.  Technically the common cause is that an unanticipated condition occurs (a \"corner case\") and this causes a transition to an illegal state from which there is no return.  A classic example of this is the ASM for a 4 bit BCD counter."
    author: "James Richard Tyrer"
  - subject: "Re: \"those who do the work decide\""
    date: 2006-02-12
    body: "Speaking of regressions, currently what annoys me most with SUSE packaged 3.5.x series is that kio_ldap is completely dead. Was hoping that 3.5.1 would fix that, but no. Still dead :/\n"
    author: "Anonymous Coward"
  - subject: "Re: \"those who do the work decide\""
    date: 2006-02-12
    body: "I'm sure that you filed a bug report?"
    author: "Anonymous"
  - subject: "Re: \"those who do the work decide\""
    date: 2006-02-12
    body: "> I'm sure that you filed a bug report?\n\nI don't think that he needs to file a bugreport for such a major bug. Don't do automated tests catch bugs like this one?\n"
    author: "max"
  - subject: "Re: \"those who do the work decide\""
    date: 2006-02-12
    body: ">I don't think that he needs to file a bugreport for such a major bug. \n\nUh! I don't think you can call any bug in kio_ldap major.   \n"
    author: "Morty"
  - subject: "Re: \"those who do the work decide\""
    date: 2006-02-13
    body: "Even if tests would catch such a bug (I do not know if it is the case here or not), it would be better to create a bug report, of course checking that there is not already a bug report on such a problem.\n\nIt is better to have two reports than not any.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: \"those who do the work decide\""
    date: 2006-02-13
    body: "Don't think, just report it. :-)"
    author: "James Richard Tyrer"
  - subject: "Re: \"those who do the work decide\""
    date: 2006-02-12
    body: "\"those who do the work decide\"\nThis is the most important feature of the KDE project, and hope it will continue forever. The quality and design of KDE is a testament to it. \n\n"
    author: "jukabazooka"
  - subject: "Re: \"those who do the work decide\""
    date: 2006-02-12
    body: "And that principle doesn't work always, that's why the TWG was introduced. What if they are two competing solutions being worked on (eg buildsystem: scons<->cmake). And of course an application doesn't start to exist until someone works on it - still not every application can be included in the KDE modules."
    author: "Anonymous"
  - subject: "Re: \"those who do the work decide\""
    date: 2006-02-14
    body: "The quality and design of KDE is certainly a testament to how far an OSS project can go.  However, part of the KDE project can be used as an example of the fact that \"those who do the work decide\" does not work if you construe it to mean that one person makes the decision and then writes the code.  OTOH, if you construe it to mean a peer review process which IIUC is how much of the basic design of KDE was done then it is a good thing.  Peer review is the engineering method and (surprise) I think that it is the best way to do design.  No bureaucracy is needed, just a group of engineers discussing (actually engineers have a tendency to argue -- but in a positive way) how to design something and then how to improve it.\n\nPersonally, I do not like making decisions by myself.  I expect to discuss them with my peers before I commit something.  And I have found that despite the fact that people don't seem interested in discussing how to do something at the design stage, they are very willing to complain about it after you commit it."
    author: "James Richard Tyrer"
  - subject: "Re: \"those who do the work decide\""
    date: 2006-02-12
    body: "Nothing to see, go on. Just Mr. Perfect bashing again."
    author: "Anonymous"
  - subject: "Re: \"those who do the work decide\""
    date: 2006-02-12
    body: "We should always strive for perfection.\n\nIf we are willing to accept mediocrity, that is what we will get.\n\nSuggestion: learn the difference between criticism and \"bashing\".\n\n     http://www.m-w.com/dictionary/criticize\n\nCriticism is a necessary part of the process to improve things.  \n\nThis is the way the engineering method works.  You find the fault and you fix it.  You keep iterating this process and the product keeps improving."
    author: "James Richard Tyrer"
  - subject: "Re: \"those who do the work decide\""
    date: 2006-02-13
    body: "> You find the fault and you fix it.\n\nHow about starting to fix your own bugs: http://tinyurl.com/ayng5. Man, some of those are as old as Sept. 2003."
    author: "cl"
  - subject: "Re: \"those who do the work decide\""
    date: 2006-02-13
    body: "You forget that JRT wants to become KDE chief designer but he will not do any work because that's below his superior qualification."
    author: "Anonymous"
  - subject: "Re: \"those who do the work decide\""
    date: 2006-02-13
    body: "Actually, I would like to be of assistance as a design and engineering consultant.\n\nSpurious insults do not change the fact that KDE code seems to get written before adequate design work."
    author: "James Richard Tyrer"
  - subject: "Re: \"those who do the work decide\""
    date: 2006-02-13
    body: "> Spurious insults do not change the fact \n\nAnd your continuous by many people as such felt do?"
    author: "Anonymous"
  - subject: "Re: \"those who do the work decide\""
    date: 2006-02-14
    body: "Unfortunately you exude a certain persona which means that no one I have spoken to has the slightest bit of interest in working with you. I hope you will consider this critiscism and not an insult, for that is what it is."
    author: "ac"
  - subject: "Re: \"those who do the work decide\""
    date: 2006-02-14
    body: "I believe that I have previously admitted to being a perfectionist.\n\nYes, I realize that this has both positive and negative points.\n\n> [N]o one I have spoken to has the slightest bit of interest in working with  \n> you.\n\nThis is their loss, not mine.  When I started using computers you had to punch holes on those cards.  Perhaps I just might have learned a few things with all these years of experience.\n\nOTOH, some people have described me as being very reasonable.  Perhaps those people discussed issues with me rather than trying to simply think of ways to tell me that I was wrong about whatever I said."
    author: "James Richard Tyrer"
  - subject: "Re: \"those who do the work decide\""
    date: 2006-02-14
    body: "\"I believe that I have previously admitted to being a perfectionist.\"\n\nNo, actually it's one of extreme arrogance."
    author: "ac"
  - subject: "Re: \"those who do the work decide\""
    date: 2006-02-14
    body: "I suggest that before you use the word 'arrogance' that you check the dictionary.\n\nhttp://www.m-w.com/dictionary/arrogance\n\nI have made a decision not to use that word anymore because most people take it to mean just the first part of the definition:\n\n\"a feeling or an impression of superiority\"\n\nbut true arrogance also requires that this be:\n\n\"manifested in an overbearing manner or presumptuous claims\"\n\nSo, I have decided to use the word 'presumptuousness' because that is my real issue with some of what some developers say when they talk about users.\n\nIf I am actually being arrogant, it is only the persona that responds to flame wars, it is not the real me. :-)  The object of a flame war is to win, isn't it? :-D"
    author: "James Richard Tyrer"
  - subject: "Re: \"those who do the work decide\""
    date: 2006-02-14
    body: "A chef can make a very delicious and healthy food for me. But if it is served in a toilet bowl (assumed it's brand new and clean), I do not like to touch it. Even if it is from Jamie Oliver himself.\n\nSomeone can come to me and criticize my program. What s/he says is maybe correct. But if s/he does it in a way that does not please me, I might just object to listen carefully and simply ignore everything.\n\nNow of course s/he has the right to do that (and I have the right to remain defiance), and maybe it is even a big loss for me when I do this. But no matter how s/he insists that by rejecting the criticism my program would not improve, that would not make me listen to her/him. The proof and portofolio are important, yet these can not always convince me.\n\nContrary to popular belief, developers are not machines. They have heart, they have feelings."
    author: "Ariya"
  - subject: "Re: \"those who do the work decide\""
    date: 2006-02-14
    body: "Nod. _Exactly right_. I am highly greatful if people come along and suggest better ways to do things if they do it in a constructive and friendly mannger. However coming along and attempting to avail oneself to the development development by suggesting that there are gratuitous design flaws _is_ bashing, makes it sound like you are suggesting the developer is incompetent, is generally not useful, and does not set a useful tone for any continuing cooperation."
    author: "ac"
  - subject: "Re: \"those who do the work decide\""
    date: 2006-02-14
    body: "> if they do it in a constructive and friendly manner\n\nI have to tell you that most engineers are somewhat deficient in this regard.  We tend to be very direct to the point of being blunt.  I realize that this is a culture clash between the engineering culture and the hacker culture.\n\nNone the less, if someone tells you exactly what is wrong with your design and how to fix it, why would anyone take this as an insult rather than an offer of assistance."
    author: "James Richard Tyrer"
  - subject: "Re: \"those who do the work decide\""
    date: 2006-02-15
    body: "I'm an engineer myself and realize what you said is true. However, do not forget that engineering project is more like a job (one could argue that engineers love their jobs). You earn money by executing the tasks. Hence, discussion in a team is highly regarded with a hope that it should improve the quality of the project.\n\nCompletely different is how these hackers work. Most of the time, they create something because of \"the itch\", not to get some money. You just use the wrong words when throwing your critism, and it does very much make sense when they get offended. You of course have the right to criticize (or simply to give feedback), but take this also into account.\n\nWhen this happens (both side are not happy), whose fault it is does not matter anymore. The situation already does not improve anything. In fact, it makes it worse.\n\nNow I don't ask everybody to stop critisizing and giving feedback. On the contrary, you should do it more. But please also keep in mind about non-technical issues, e.g. the one mentioned above.\n\n(Personally, if I would like to help somebody but s/he seems does not want it, then I'll forget it. There are still other persons whom I can offer my help)."
    author: "Ariya"
  - subject: "Re: \"those who do the work decide\""
    date: 2006-02-16
    body: "My only interest is in improving the quality of our product.\n\nI am willing to have a discussion of how best to do this.\n\nWhile I can try to understand why people might take blunt critical remarks as somehow personal (they are not), I can not understand those that seem to think that they should respond with rhetorical attacks.  Or, those that seem to somehow think that these are in any way equivalent.\n\nNote: We do have an excellent product, but this does not mean that that it doesn't need improvement.  Some developers don't seem to get this.\n"
    author: "James Richard Tyrer"
  - subject: "Re: \"those who do the work decide\""
    date: 2006-02-16
    body: "Practically you can't have 1000 dB of signal-to-noise ratio. So, IMHO you just need to ignore those who attack you (I know it easier to say that). How on earth can you please everybody anyway?\n\nLooking at the extreme \"don't attack my baby or I will ignore you!\" principle, I'd be also cautious when I claim 'some developers....'. The sensitive one might just include her/himself in this category and thus refuse to listen further on.\n\nAlso, note that not everybody is a native speaker. Language sometimes plays an important role.\n\nI remember what a friend has concluded: bombarding yourself with all the wonderful motivation and plans to go to the gym does not magically make you doing the physical exercises. Let's not convince every single developer first that her/his program needs improvement, we may as well focus on those who want to listen.\n"
    author: "Ariya"
  - subject: "Re: \"those who do the work decide\""
    date: 2006-02-14
    body: "Most parts of the KDE framework have been designed by a group of developers, usually drafts being discussed on mailinglists and concrete design being done at conferences such as aKademy or the annual KDE-PIM meeting.\n\nEven after such meetings the concepts evaluated there are usually still open for discussion, since not all involved developers have the opportunity to attend those meetings.\n\nAt the moment examples for this kind of work in progress would be the KDe-PIM Akonadi Framework or the Phonon Multimedia API and I guess other areas like the hardware abstraction Solid or evaluating requirements for KDE4's KIO system would would welcome real design/review contributions as well.\n\nI am afrid just posting \"KDE should do more design\" does not in any way increase the amount of designers."
    author: "Kevin Krammer"
  - subject: "Re: \"those who do the work decide\""
    date: 2006-02-14
    body: "> I am afraid just posting \"KDE should do more design\" does not in any way \n> increase the amount of designers.\n\nTrue, but ...\n\nI believe that what I posted was my hope that the TWG would result in more design work being done."
    author: "James Richard Tyrer"
  - subject: "Re: \"those who do the work decide\""
    date: 2006-02-14
    body: "Actually, I measure success by the number of reported bugs that have been fixed or resolved:\n\nhttp://tinyurl.com/a7xgm\n\nrather than 5 that have not yet been totally resolved.  \n\nFinding the fault and fixing the fault are both important functions.\n\nAnd yes, I did fix some of these myself.\n\nTo say that nothing has been done on the remaining 5 bugs is missleading but it would take a while to enumerate what has been done and why these remain unresolved."
    author: "James Richard Tyrer"
  - subject: "Thanks JRT"
    date: 2006-02-14
    body: "I, for one, greatly appreciate your hard work, diligence, and valuable contributions."
    author: "Rex Dieter"
  - subject: "The members of the TWG"
    date: 2006-02-12
    body: "Hi,\n\nIMO the really nice thing about the election results is, that it basically changes nothing: these are the people which we trusted already before, and this was now simply formally confirmed by the election.\n\nNice that Lubos and Gunnar made it into the TWG :-)\nAnd of course David, but who would have expected anything else ?\n\nBye\nAlex\n"
    author: "aleXXX"
  - subject: "What about..."
    date: 2006-02-12
    body: "Aaron and Waldo?\n\nWaldo seems to be less active but has been very visible in the past...\n\nAaron, aka M. Plasma certainly would be a good member"
    author: "ac"
  - subject: "Re: What about..."
    date: 2006-02-12
    body: "Both didn't candidate."
    author: "Anonymous"
  - subject: "Re: What about..."
    date: 2006-02-12
    body: "Aaron is already on the KDE e.V. board, things get tricky if you're on too many boards.\n\nWaldo now work for Intel as their open source dude.\n"
    author: "Jonathan Riddell"
  - subject: "Re: What about..."
    date: 2006-02-12
    body: "yeah. and i already have too little time anyways =) we have lots of terrific and brilliant contributors and they are more than capable of handling these things very, very well =)\n\ni'm actually really happy to see the group that is making up this new body. they'll do a terrific job."
    author: "Aaron J. Seigo"
  - subject: "Re: What about..."
    date: 2006-02-13
    body: "I have been working for Intel for about the last year on Linux Desktop issues in a broader sense. Unfortunately that means that I am not as involved with KDE's day to day development as I used to be. So I decided not to run for the TWG because I think it deserves people who are more in touch with the daily KDE development. I think the election resulted in a very strong TWG that will be able to properly represent and balance the different viewpoints within the KDE community whenever it will need to make decisions. I think that by establishing the TWG, KDE will be better positioned to handle the many challenges that a large project like KDE faces. \n\nCheers,\nWaldo\nhttp://www.intel.com/go/linux   "
    author: "Waldo Bastian"
  - subject: "Re: What about..."
    date: 2006-02-13
    body: "Hey Waldo, say thank you to your employer for cooperating with Skype and showing that way why closed software sucks. ;)\nhttp://slashdot.org/articles/06/02/13/2015236.shtml"
    author: "ac"
---
The first Technical Working Group for KDE has been elected. It will consist of seven long-time contributors to KDE and become operational in the few next days. The group has been elected by the members of <a href="http://ev.kde.org/">KDE e.V.</a>. This initial Working Group is elected for a period of six months. After this period an evaluation of the Working Group will take place. If it proves successful, elections will take place once every year.  The group will help the hundreds of KDE developers in reaching technical decisions.  Read on to learn about the members of the first Technical Working Group.

<!--break-->
<p>Dirk M&uuml;ller rounds up the purpose of the Technical Working Group "<em>I think it is important to establish an infrastructure of people that are generally informed about what is going on in KDE and can provide a unique and well-functioning contact point whenever a developer is actively searching for help in some area.</em>"</p>

<p>The formation of the Working Groups was decided on at the <a href="http://ev.kde.org/meetings/2005.php">KDE e.V. membership council</a> held at aKademy 2005 last August in Malaga. The details of the Working Group formation were worked out during an <a href="http://ev.kde.org/meetings/2005-working-groups-discussion.php">open meeting</a>. The Technical Working Group will be the second Working Group to be announced, after the <a href="http://dot.kde.org/1131467649/">Marketing Working Group was formed</a> in November 2005.
</p>

<p><img src="http://static.kdenews.org/danimo/david.jpg" align="left" />
<br /><strong><a href="http://people.kde.org/david.html">David Faure</a></strong>, one of the authors of Konqueror, currently works for Klarälvdalens Datakonsult AB and is being sponsored by Trolltech to spend 50% of his work time to work directly on KDE. David has been one of KDE's release coordinators in the past. Important for David as a member of the Technical Working Group is that the Working Group does not step on people's toes and makes all decisions. However, he does want the TWG to ease decision-making processes in the areas of application inclusion, release schedules and conflict resolution where KDE currently lacks a decision-making process.
</p><br clear="all" />

<p><img src="http://static.kdenews.org/danimo/dirk.jpg" align="left" />
<br /><strong><a href="http://people.kde.org/dirk.html">Dirk M&uuml;ller</a></strong> has been kept busy with system administration and KDE infrastructure tasks. He feels that it's important to move KDE 4 forward because it is once again a big chance to change the way KDE works internally and the way it interacts with the user. Dirk is also doing research in various areas of quality assessment, such as automated code verification.  Dirk is working for SUSE/Novell.
</p><br clear="all" />

<p><img src="http://static.kdenews.org/danimo/george.jpg" align="left" />
<br /><strong><a href="http://people.kde.org/george.html">George Staikos</a></strong>, who acts as the Canadian and general North-American press contact, wants to ensure that the Technical Working Groups doesn't overstep its boundaries or make questionable decisions. He states that he wants to keep KDE very free of bureaucracy and make sure it continues to follow the bazaar model that made KDE so successful. George is the President of Staikos Computing Services, Inc.
</p><br clear="all" />

<p><img src="http://static.kdenews.org/danimo/gunnar.jpg" align="left" />
<br /><strong><a href="http://people.kde.nl/gunnar.html">Gunnar Schmidt</a></strong>, one of the founders of KDE Accessibility (which is also his main working area within KDE) sees himself as the accessibility and human interface coordinator in the Technical Working Group. Gunnar is currently working on his diploma thesis which includes writing a decent screen magnifier with a KDE user interface.
</p><br clear="all" />

<p><img src="http://static.kdenews.org/danimo/lubos.jpg" align="left" />
<br /><strong><a href="http://people.kde.org/lubos.html">Lubos Lunak</a></strong>, the maintainer of KWin, KSMServer, and some X11-related parts in kdelibs has also done quite a lot of work on performance. Lubos wants to do whatever it takes to shape KDE 4 and push it towards becoming reality. Lubos is employed by SUSE/Novell.
</p><br clear="all" />

<p><img src="http://static.kdenews.org/danimo/coolo.jpg" align="left" />
<br /><strong><a href="http://people.kde.org/stephan.html">Stephan Kulow</a></strong>, the current release coordinator of KDE has been pushing KDE into new directions more than once, notably the modular way of distribution, adoption of gettext and the bug tracking system. Coolo works for SUSE/Novell where his tasks involve working on KDE.
</p><br clear="all" />

<p><img src="http://static.kdenews.org/danimo/thiago.jpg" align="left" />
<br /><strong><a href="http://people.kde.nl/thiago.html">Thiago Macieira</a></strong> is a 24 year-old Brazilian who recently moved to Oslo to work for Trolltech. He thinks that the Technical Working Group has to be pro-active, especially in elaborating a long-term vision for KDE.
</p><br clear="all" />

<p>The purpose of the Technical Working Group is to define and execute the official software releases of KDE, to provide guidance on technical decisions and to make sure that the Open Source development process is being maintained. The responsibilities of the Technical Working Group include release management, guiding the decision making process about external and inter-module dependencies, KDE-wide technical infrastructure and technical guidance. All decisions will be made in the spirit of the Open Source development process and will be well-documented. Furthermore, the TWG will coordinate actions amongst people acting as KDE representatives, and function as a representative itself. The Technical Working Group will involve the KDE community in the decision making process at all times. Discussions will be held on the relevent mailing list, usually <a href="http://lists.kde.org/?l=kde-core-devel&r=1&w=2">kde-core-devel</a>. Further details on the purpose, responsibilities and procedures of the Technical Working Group can be found in <a href="http://ev.kde.org/corporate/twg_charter.php">its charter</a>.
</p>

<p>If you are a long-term contributor to KDE please consider <a href="http://ev.kde.org/members.php">joining KDE e.V.</a> which will allow you to take part in future votes for the TWG.</p>








