---
title: "People Behind KDE: Kenneth Wesley Wimer II"
date:    2006-04-10
authors:
  - "tbaarda"
slug:    people-behind-kde-kenneth-wesley-wimer-ii
comments:
  - subject: "kwwii is the goods"
    date: 2006-04-10
    body: "Thanks to Ken for past, present and future work.\n\nOxygen and HCI?  Oh yes, his fingerprints will be all over KDE4.\n\n\"Pretty Orange Pictures\" - dear God...are we about to go below 99% Blue in the next release? I'm crossing my fingers.\n"
    author: "Wade"
  - subject: "Re: kwwii is the goods"
    date: 2006-04-10
    body: "When Ken and me started to work at S.u.S.E. in the good old days we practiced to paint green pictures in all kinds of shades all day long. Up to the point where Ken's retina started to filter out all other colors, so he wasn't able to paint anything apart from green anymore. Then Everaldo showed us the light by sharing the secret of KDE-blue with us. What a totally new, fresh experience! After the blue era Ken had a period of time where he tried to experiment with strange combinations of red and green. But apparently this has become boring as well already. So on this Rainbow Exploration Tour it's really the next logical step for our artist hero to go for orange now ;-) May the goddess of bootsplashes and humanity give him enough power to deliver a KDE 4 that will rock totally and that will appeal to everyone! ;-)\n\nTorsten\n   \n\n\n  "
    author: "Tackat"
  - subject: "We need to know...."
    date: 2006-04-10
    body: "Go on what was the joke?"
    author: "ivor"
---
Tonight, the <a href="http://people.kde.nl/">People Behind KDE</a> interview series brings you an interview with <a href="http://people.kde.nl/kenneth.html">Kenneth Wesley Wimer II</a>.
As an KDE artist, he is known for his work on KDE's artwork and the <a href="http://www.oxygen-icons.org">Oxygen Icons</a> for KDE 4. An American living in Germany, Kenneth tells you what he wants us to know about himself in this interview.


<!--break-->
