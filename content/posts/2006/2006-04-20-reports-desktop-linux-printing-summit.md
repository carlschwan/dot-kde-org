---
title: "Reports from the Desktop Linux Printing Summit"
date:    2006-04-20
authors:
  - "jriddell"
slug:    reports-desktop-linux-printing-summit
comments:
  - subject: "Apple?"
    date: 2006-04-20
    body: "Why exactly was Apple involved at a summit for _Linux_ printing?  Were they there simply because they use CUPS for printing, or are they actually more involved than that?"
    author: "Corbin"
  - subject: "Re: Apple?"
    date: 2006-04-20
    body: "It was very kind from Apple to send Paul. Yes, they were perhaps interested to see what happens new in CUPS (although they must have very good contacts with the developers of CUPS already) but in the process they were very helpful with information and accounts of experience on their printing-related work. Very instructive. And Apple is commendable and deserving our deep respect and thanks for this.\n\nNot to forget the Paul Danbold (Apple) gave me and Andreas Vox (from Scribus) a lift to the airport at the end of the summit, which was an extremely nice and helpful thing to do ;-)\n\nAnd in general the level of collaboration and common work from natural competitiors at this event was simply impressive."
    author: "Inorog"
  - subject: "Congratulations!"
    date: 2006-04-20
    body: "Hi everybody,\n\nI heartily congratulate all maintainers and contributors of CUPS, KDEprint and all related projects. Great work! I've been using KDE since 1998, and it has really come a long way.\n\nI have a small request/problem/bug/config issue, that I couldn't find in the meeting (b)logs. I don't know whether KDEprint, Ghostscript, font management or KDE is at fault but printing from KDE applications always results in \"shaky\" text on the page. Even printing this very web page into a PDF will cause noticeable non-alignment between characters.\n\nScreenshot: http://www.jensbenecke.de/temp/kde/printing-text.png\n\nYou can see that the spaces between characters are not equal and some characters are not correctly vertically aligned. Look at \"developers\" in the second line - the space between \"de\" and \"pe\" is almost zero, and the letters \"v\" and \"p\" are set a bit higher than the others.\n\nIs this a configuration issue? I've had it with all Linux distributions I've worked with and all fonts. I just now tried Bitstream Vera and Arial as the browser font.\n\nPerhaps this can be solved ... perhaps I can even do it myself? Firefox and Openoffice.org have much better font alignment when printing.\n\nThanks!\n\nJens"
    author: "Jens"
  - subject: "Re: Congratulations!"
    date: 2006-04-20
    body: "Hi, Jens,\n\nthis is a known issue. The technical measure that covers this (or not) is called \"font hinting\". (And it may be average or bad or most bad with different fonts, but it never is very good or excellent right now.) But it is not only a technical issue, it is also a political one: the reason is, font hinting is patent encumbered (at least for TrueType fonts).\n\nKDE4 will hopefully be a *lot* better with font display and font printing, given that Qt4 has re-vamped all the underlying technologies.\n\nSee here for more technical and political background:\n\n  http://www.daube.ch/docu/glossary/font_hinting.html\n  http://www.microsoft.com/typography/tools/trtalr.htm\n  http://freetype.sourceforge.net/patents.html \n  http://www.freetype.org/david/unix-font-rendering.html\n\nCheers,\nKurt"
    author: "Kurt Pfeifle"
  - subject: "OT"
    date: 2006-04-20
    body: "Isn't the kerning wrong in between the T and the y in the header on the freetype web site itself?\n\nhttp://freetype.sourceforge.net/patents.html "
    author: "Martin"
  - subject: "Re: OT"
    date: 2006-04-20
    body: "Duh! \n\nI had meant to also hint at \"font kerning\" as one area which may be causing badly looking fonts (on screen and on paper), but then somehow forgot about it..\n\n   http://en.wikipedia.org/wiki/Kerning\n\nCheers,\nKurt"
    author: "Kurt Pfeifle"
  - subject: "Re: Congratulations!"
    date: 2006-04-26
    body: "Hi Kurt,\n\nthank you for your explanation! However, I wonder why apps like Openoffice and Mozilla do this a _lot_ better, although they too are open-source apps. Printout from OOo looks fine, the same document printed from e.g. KWord doesn't. Can't some of their ways of \"doing it\" be transferred so KDE as a whole can profit from it?\n\nJens"
    author: "Jens"
  - subject: "Re: Congratulations!"
    date: 2006-04-21
    body: "Perhaps this screen shot better illustrates the problem:\n\nhttp://home.earthlink.net/~tyrerj/kde/Verdana.png\n\nThe top is rendered with Type 42 scalable fonts in a PS made by Open Office Writer (OO always embeds the fonts :-().  The bottom is TrueType rendered by GhostScript from a PS made by KWord (fonts were NOT embedded in the PS file).  Note that neither of these is really correct but that OO is better.\n\nNotice that the lines in the bottom example are longer.  IIUC, the reason for this is that Qt-3 uses hinting for the screen display and then prints the unhinted fonts using the same spacing as the hinted screen display.  Why the Trolls ever did this is a mystery.  So far, Qt-4 does not offer a _simple_ solution to this problem."
    author: "James Richard Tyrer"
  - subject: "Re: Congratulations!"
    date: 2007-02-15
    body: "Hello,\n\nI experienced the same horizontal misalignment, but I found an easy way to solve it.\n\nOpen the Style Manager, select the Standard style, click on the \"Indent & Spacing\" and set \"Left Indent\" to a very small amount, such as 0.01. Click OK.\nNow what you type in Standard style should appear correctly.\n\nDon't know why, but it works for me. I am currently using Kword 1.5.2 (KDE 3.5.4) on FreeBSD 6.2.\n\nBye,\nMassimo\n\n\n"
    author: "Massimo"
  - subject: "Lexmark"
    date: 2006-04-20
    body: "Lexmark could offer GPL drivers for X2250, that would be a good start.\n\nAt this time I don't think that Lexmark really wants to support printing on Linux.\n\nEleknader\n"
    author: "Eleknader"
  - subject: "Moving to PDF as a future print spooling format"
    date: 2006-04-21
    body: "I remain unconvinced.\n\n> Font problems which are present in PostScript are less frequent with PDF, and \n> more easy to solve if they occur.\n\nFirst the one thing which PDF does not fix is that TrueType fonts can not be directly embedded -- they must be converted to Type 42 fonts.  This is a source of some of KDE's problems with printing and PDFs.\n\nI find that for me a solution to this problem, and some others, is to NOT embed the fonts in my PostScript files.  I have had considerable problems controlling whether or not fonts are embedded when I convert PS to PDF.  Is anything being done to fix this problem?\n\nIt would be nice if there was a GUI to control this -- even if it used the Athena Widgets like GV. :-)\n\nIAC, where there is an option to save directly to PDF, I would very much like to have complete control over the embedding of fonts and the resolution of raster images."
    author: "James Richard Tyrer"
  - subject: "Re: Moving to PDF as a future print spooling format"
    date: 2006-04-21
    body: "TrueType fonts can be embedded directly. In fact it does even work to copy the complete ttf file into the stream of a PDF font object. So this does work.\n\nregards,\n  Dom"
    author: "Dominik Seichter"
  - subject: "Re: Moving to PDF as a future print spooling format"
    date: 2006-04-23
    body: "I presume that this requires a tool that converts directly since you can't embed TrueType fonts in PostScript.\n\nCan you recommend a free tool that does this?"
    author: "James Richard Tyrer"
---
<a href="http://www.desktoplinux.com/">DesktopLinux.com</a> reports on the recent OSDL sponsored <a href="http://groups.osdl.org/workgroups/dtl/desktop_architects/desktop_printing/">Desktop Linux Printing Summit</a> in the USA.  "<em>The meeting was attended by about 40 developers from printer vendors, such as Hewlett-Packard, Lanier, and Lexmark; to operating system distributors like Apple Computer, Debian, and Novell; to those two Linux desktop powers, GNOME and KDE; and more. Their job? To nail down exactly what's wrong with printing and Linux, and to work out ways to resolve these problems once and for all.</em>"  They point to blogs from the KDE attenders, <a href="http://www.kdedevelopers.org/node/1936">Cristian Tibirna</a>, <a href="http://www.kdedevelopers.org/node/1934">Kurt Pfeifle</a> and <a href="http://www.kdedevelopers.org/node/1930">Waldo Bastian</a>.  <a href="http://lwn.net">LWN.net</a> has an <a href="http://lwn.net/Articles/179511/">extended report from Kurt</a>.  <a href="http://www.linux.com">Linux.com</a> reports on the plan to <a href="http://applications.linux.com/applications/06/04/18/2114252.shtml?tid=13">move to PDF for print spooling</a>.  Finally Waldo has the <a href="http://www.kdedevelopers.org/node/1932">group photo</a>.




<!--break-->
