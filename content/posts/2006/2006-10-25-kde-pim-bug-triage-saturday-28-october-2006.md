---
title: "KDE PIM Bug Triage on Saturday 28 October 2006"
date:    2006-10-25
authors:
  - "bschoenmakers"
slug:    kde-pim-bug-triage-saturday-28-october-2006
comments:
  - subject: "now thats an easy way out..."
    date: 2006-10-25
    body: "\"Close bugs which insufficient information and which are open for quite a long time\"\n\nand what about bugs that are open for a long time because no one gives a rats ass about it? https://bugs.kde.org/show_bug.cgi?id=17513 comes to mind.\n\nopened back in 12-2000 through the import of old bugs from the old kde bugtracker.\n3552 votes. 72 comments from users who want that fixed (or rather, that feature implemented), and no sensible comments from kmail developers, other than some pseudo-philiosophical debate about the ethics of \"tampering\" with emails after they have been delivered.\n \n"
    author: "Mathias"
  - subject: "Re: now thats an easy way out..."
    date: 2006-10-25
    body: "Just take some breath and read again:\n\nClose bugs with insufficient information _AND_ which are open for quite a long time.\n\nBug 17513 does not apply for both conditions so it won't be closed. It's about bugs with a oneliner \"it crashes\" and there's no further information available (despite the fact we asked for it)."
    author: "Bram Schoenmakers"
  - subject: "Re: now thats an easy way out..."
    date: 2006-10-25
    body: "https://bugs.kde.org/show_bug.cgi?id=17513 is a feature wish not a bug report. So what does it have to do with the article???"
    author: "cl"
  - subject: "Re: now thats an easy way out..."
    date: 2006-10-25
    body: "I want search folders with date options and mail tagging is interesting too.\n\nAnd no crashes and speed improvements in Imap.\n"
    author: "anonymous"
  - subject: "Re: now thats an easy way out..."
    date: 2006-10-25
    body: "Did you file bug reports/feature requests for that? Or did you vote for existing reports that ask/report the same? Did you provide additional information as well? (\"It crashes\" is NOT usefull for debugging!)\n\nIf so: good. After this bug squatting you'll stand a better chance of being heard. If not, there is no way you can be helped. Reporting issues on the dot is not productive."
    author: "Andr\u00e9"
  - subject: "Re: now thats an easy way out..."
    date: 2006-10-25
    body: "I want world peace, end to poverty and icecream too...\n\nHow about not demanding things from volunteers???"
    author: "cl"
  - subject: "Re: now thats an easy way out..."
    date: 2006-10-25
    body: "There's really nothing wrong with asking things of volunteer open source developers. It only annoys me when folks are like \"stop what your doing and do this instead.\"\n\nGranted dot.kde comments are hardly the time or place for it.."
    author: "Ian Monroe"
  - subject: "Re: now thats an easy way out..."
    date: 2006-10-26
    body: "Requests (\"I would like...\") are acceptable.  Demands (\"I want\") are not.  "
    author: "Robert Knight"
  - subject: "Re: now thats an easy way out..."
    date: 2006-10-26
    body: "One should, however, keep in mind that language barriers and cultural differences can make a request look like a demand.\n"
    author: "Kevin Krammer"
  - subject: "UNCONFIRMED status should get the NEW status"
    date: 2006-10-25
    body: "Hi!\n\nboth these bugs should get a new flag, I can reproduce them both.\n\nhttp://bugs.kde.org/show_bug.cgi?id=128357\nhttps://bugs.kde.org/show_bug.cgi?id=135948"
    author: "Robert Penz"
  - subject: "Re: UNCONFIRMED status should get the NEW status"
    date: 2006-10-26
    body: "thx for changing the status."
    author: "Robert Penz"
  - subject: "Thanks"
    date: 2006-10-28
    body: "Bram,\nI wanted to take the time to thank you and the team for this work. I am a user of KMail with insufficient programming knowledge to fix the bugs but I know I will benefit from the output.\n\nThanks again."
    author: "Taliesyn"
  - subject: "Good turnout"
    date: 2006-10-28
    body: "There are about 40 people in the irc channel working on bug fixing at the moment - that's amazing!"
    author: "Bille"
---
This Saturday, 28 October 2006, a bug triage day will be held. This time we are going to clean up the bugs from <a href="http://bugs.kde.org">Bugzilla</a> for the KDE PIM module, after <a href="http://dot.kde.org/1157239921/">a successful session for Konqueror</a> several weeks ago. We will meet on the IRC channel #kde-bugs on Freenode to coordinate the whole effort.





<!--break-->
<p>At the time of writing the two largest PIM applications, KMail and KOrganizer, have about 3400 open bugs and wishes in Buzilla. For developers it is almost impossible to get an overview. That is why a bug triage day is being organised in order to reduce the amount of bugs in the Bugzilla system. On this particular day, you can help us with one or more of these tasks:</p>

<ul>
<li>Confirming bugs. Bugs with the UNCONFIRMED status should get the NEW status once someone else is able to reproduce the bug reliably.</li>
<li>Finding bug duplicates. Many bugs entered into Bugzilla are duplicated of other bugs. Sometimes it's hard to recognise these duplicates by a single person.</li>
<li>Close bugs which insufficient information and which are open for quite a long time (i.e. reporter does not respond on a need-more-info request).</li>
<li>Categorise bugs into the right components. Many bug reports for Kontact really belong to KMail and KOrganizer, for example.</li>
<li>Finally, fix bugs when they seem to be trivial, but that is certainly not the focus of that day.</li>
</ul>

<p>More information on the bug days can be found on our <a href="http://developernew.kde.org/Contribute/Bugsquad">Bugsquad page</a>.</p>

<p>You are welcome to help us by joining the #kde-bugs channel on Freenode. Hope to see you there this Saturday.</p>



