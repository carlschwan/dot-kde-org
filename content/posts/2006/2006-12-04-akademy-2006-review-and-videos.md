---
title: "aKademy 2006 Review and Videos"
date:    2006-12-04
authors:
  - "jriddell"
slug:    akademy-2006-review-and-videos
comments:
  - subject: "Videos?"
    date: 2006-12-04
    body: "Finally! Thanks."
    author: "Lans"
  - subject: "torrent?"
    date: 2006-12-04
    body: "what about a torrent? thanks anyway (I was able to dl at least one video :)"
    author: "MK"
  - subject: "Re: torrent?"
    date: 2006-12-04
    body: "Download link restored.\n"
    author: "Jonathan Riddell"
  - subject: "Re: torrent?"
    date: 2006-12-05
    body: "Thanks a lot! BTW here is the link to the slides\n\nhttp://conference2006.kde.org/conference/presentations.php\n\n"
    author: "MK"
  - subject: "audio?"
    date: 2006-12-05
    body: "It's a shame the poor audio quality and noise through the mic makes it very hard to hear. Is it possible to fix this?\n\nI only saw the start of 'KDE4 Development Setup' and 'Competition and Cooperation' if it matters."
    author: "Schalken"
  - subject: "Transcripts"
    date: 2006-12-05
    body: "Thanks for the videos! I've been waiting for this.\n\nHere's a link to some of the transcripts of the talks:\n\nhttp://wiki.kde.org/tiki-index.php?page=Talks+%40+aKademy+2005\n\nVideos + Slides + Transcripts. Isn't that great? :-D"
    author: "Jucato"
  - subject: "Re: Transcripts"
    date: 2006-12-05
    body: "That's the 2005 page.\n\nLink for 2006:\n\nhttp://wiki.kde.org/tiki-index.php?page=Talks+%40+aKademy+2006"
    author: "ac"
  - subject: "Multi-Head RandR - Keith Packard"
    date: 2006-12-05
    body: "This talk was about something wonderful: output hotplugging for X. I hereby nominate Keith for the Penguin of the Year Award (which I just made up). \n\n\"When will this be released? It's released but not part of an x.org release, probably 7.3 next Spring.\"\n\nDoes anyone have some instructions how to plug this into the current x.org to try it out? X is supposed to be modular now, after all."
    author: "Martin"
  - subject: "Re: Multi-Head RandR - Keith Packard"
    date: 2006-12-06
    body: "Yes, better hotplug for X is sooo much needed. I do not really need any of these fancy 3D gimmick. I want to plug a Logitech wireless mouse reveiver into my notebook and this device should simply work. Today I still have to restart X. (For me) only non wireless mouse devices works hotpluged.\n\nDynamic screen changing works with the ATI config tool on my notebook. I simply bind some commands to acpi key events. Works like charm :-)\n\nBut here its time for a X standard, also.\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Input hotplugging"
    date: 2006-12-07
    body: "You may be in luck. Input hotplugging, which is what you want, is supposed to be improved already in x.org 7.2. This is due in just a few days! The output hotplugging, which Keith talked about, and which is even sexier IMO, will appear only in 7.3. Thus my question."
    author: "Martin"
  - subject: "Re: Input hotplugging"
    date: 2006-12-07
    body: "No such luck after all. Input hotplugging was deferred to the 7.3 release at the very last moment. (So, extrapolating this trend, maybe that will be in 7.3 and output hotplugging only in 7.4. We'll see.)"
    author: "Martin"
  - subject: "Re: Multi-Head RandR - Keith Packard"
    date: 2006-12-07
    body: "If the receiver is usb, then it should just work now, no?"
    author: "John Tapsell"
  - subject: "Re: Multi-Head RandR - Keith Packard"
    date: 2006-12-08
    body: "Generally, you just want to use the standard driver for mice. But if you want to change the driver (to enable the mouse you plug some extra features), you must restart X."
    author: "blacksheep"
  - subject: "Re: Multi-Head RandR - Keith Packard"
    date: 2006-12-07
    body: "I don't have instructions for x.org, but there is a work around:\n\nInstall FreeBSD, and enable moused.    Setup /etc/usbd.conf to start moused for each mouse hot pluged.   Set x.org to use /dev/sysmouse as the only mouse.\n\nThere are downsides to the above (you can't have two input devices, plus all the freeBSD vs Linux differences that may or may not matter to you).   I can confirm that it has worked for several years in though.\n\nI'm looking forward to output hot plugging - I have an external monitor port on my laptop that I'd like to be able to use at times, but selecting the correct xorg configuration manually is more than I've felt like configuring so far. "
    author: "Hank Miller"
  - subject: "Re: Multi-Head RandR - Keith Packard"
    date: 2006-12-08
    body: "On Linux, the device node to use would probably be \"/dev/input/mice\"."
    author: "rqosa"
  - subject: "ogg???"
    date: 2006-12-07
    body: "why not use ogm as an extension for movies...\n \nthen it would at least start my kaffeine player instead of messing up my amarok playlist... ( yeah I know I can right click and select another app but I always forget that the first time...)"
    author: "Mark Hannessen"
  - subject: "Re: ogg???"
    date: 2006-12-08
    body: "It extension is ogg not ogm.\n\nTake a look at : www.theora.org"
    author: "Henry"
  - subject: "Re: ogg???"
    date: 2006-12-08
    body: "Konqueror should check the start of the file to see its real format. It should be possible to don't use extensions and still have an usable system. Otherwise, it just feels like Windows."
    author: "blacksheep"
  - subject: "Re: ogg???"
    date: 2006-12-08
    body: "don't we all love the look and feel of windows :o)"
    author: "AC"
  - subject: "video sizes"
    date: 2006-12-11
    body: "videos are between 150-200 mb\n\nthis is crazy. and i dont really want to download them. i just want to view them once.\n\nwhy not making them webviewable? like youtube or quicktime ?\n\n"
    author: "slow bandwidth, lazy, small harddrive, thin clients"
---
Linux Magazine have put their overview of <a href="http://conference2006.kde.org/">aKademy 2006</a> -- the KDE World Conference -- online from their <a href="http://www.linux-magazine.com/issue/73/">December 2006 issue</a>. They describe how aKademy helped plan the road to KDE 4, and also report on the widely-successful OpenDocument day.  There is also a review of KAlarm available from the same issue.  In other aKademy 2006 news, the <a href="http://home.kde.org/~akademy06/videos/">videos of the presentations and talks</a> are now being uploaded.






<!--break-->
