---
title: "Second Beta of KOffice 1.5 Released"
date:    2006-03-11
authors:
  - "brempt"
slug:    second-beta-koffice-15-released
comments:
  - subject: "Why are there no kliks available?"
    date: 2006-03-11
    body: "Congratulations to the second beta. What you have added and fixed since the first one is really impressive. Krita starts to shake up the graphic tools market, and I hope Karbon will be in the same league very soon.\n\nI very much enjoyed being able to test the klik packages made from the first beta after it was out. It looks to me, that all the initial klik problems (some koffice programs didnt work for some distros) were shaken out -- the kliks now work very well even on some more obscure distros (yes, you need kdelibs3 and a few other base utilities installed prior to run the kliks) as can be seen from visiting the comments section and the wiki on the klik website [ http://klik.atekon.de/wiki/index.php/KOffice-1.5.0_DistroTable ].\n\nSo it seems all the klik teething pains are gone now... Where we initially had problems and crashes due to klik, now that the klik recipe for KOffice is stable most crashes and problems are more likely to be indicative a KOffice bug. \n\nAnd this is A Good Thing (TM) for the development process. It makes it very easy for a non-programmer like me (and a person that just doesnt have enough time for compile and build marathons) to donate at least *some* time to test and report bugs to bugzilla.\n\nHowever, the base packages (\"ingredients\" which go into the klik bundle) haven't been updated at all since 6 weeks. If I klik krita today, I still get Krita bugs from 6 weeks ago. This is not good.\n\nWhy is this? Was the KOffice team not too much pleased with the results? \n\n----\n\nI, as a frequent klik user, would very much appreciate if you would not only supply kliks for the beta 2, but also more regular builds (even nightlies) in the future. \n\nI think it would benefit the KOffice development too. \n"
    author: "klik-er"
  - subject: "Re: Why are there no kliks available?"
    date: 2006-03-11
    body: "It's simple, and has nothing to do with the KOffice developers: there are no kliks because the person preparing the Debian packages was too busy in this timeframe. It's all a matter of resources. We would like to have nightly kliks, but we don't have the resources to prepare Debian sid .debs nightly. If someone could help us with that, we'd shower that person with gratitude. \n\nWe love quick user feedback on new features, new bugs and regressions!"
    author: "Boudewijn Rempt"
  - subject: "Re: Why are there no kliks available?"
    date: 2006-03-11
    body: "What kind of resources are needed for a nightly build?  Is it just machine power?  I'm kind of busy myself but am willing to look into how much time it would take and maybe contribute based on how much time I have.\n---------------------\ncarl.homelinux.com"
    author: "sleepkreep"
  - subject: "Re: Why are there no kliks available?"
    date: 2006-03-11
    body: "Machine power and a little knowledge of how debian packages are built."
    author: "Boudewijn Rempt"
  - subject: "Krita..."
    date: 2006-03-11
    body: "has really come a long way. I never really liked GIMP but got used to\nit over the time. I gave it an extra virtual desktop because of all those\nstupid windows in the taskbar and learned to live with the unpleasant file dialog and the fact that it didnt support editing images on remote servers directly (KIO-FISH) and that there is no CMYK support. Nevertheless I'm still hoping for a way to dump it for good because of all those disadvantages which will probably never be solved. Enter Krita. It sure looks quite promising\nand because I only need some relatively basic editing (but a bit more\nthan KolourPaint could provide) I'm just that close to do a dpkg --purge\non GIMP. I'm still using Krita 1.4 that came with Kubuntu Breezy and I'm trying to use it for everyday tasks. Sometimes it works (during the last version\nmore and more often), sometimes not and I still have to resort to the GIMP.\nSome quite simple things I'm missing that would make working with Krita so much\nbetter (I'm talking 1.4 here - didnt test the new one) and\nwhich shouldnt be too difficult to implement (I'm no programmer though so \nI cannot say for sure)\n\n1) When you resize an image (not scale) you cannot center the image.\n2) The size of an image should always be shown in the status bar\n3) There should be an option to show an \"ant-line\" instead of cropping\nthe image when you do a rectangular selection and an option to set the color\nfor transparent areas. Sometimes you cannot discern the areas where there\nis transparency at all or what exactly you have selected.\n4) One zoom level more than 32:1 for editing small icons, I would like 64:1\n\n\n\n\n"
    author: "Martin"
  - subject: "Re: Krita..."
    date: 2006-03-11
    body: "Then you should really try this beta. It works very well here already and is _much_ more stable. Great work.\n\nnow,\n1) this works here\n2) Why do you need this? Image->Properties\n3) don't know if this has changed, but an \"ant-line\" is shown here\n4) you can zoom up to 1600%"
    author: "anon"
  - subject: "Re: Krita..."
    date: 2006-03-11
    body: "\"you can zoom up to 1600%\"\n----\nYes. But he asked to increase the maximum to 6400% instead of 3200% (which he says he has)."
    author: "klik-er"
  - subject: "Re: Krita..."
    date: 2006-03-11
    body: "1) Perhaps you use 1.5, here (1.4) there is no option to automatically\ncenter an image in the resize image dialog.\n2) Because in web design the exact size of an image is important to\nfit it in a web-page. Calling image-properties for just this basic\ninformation is getting old if you need to call it on hundres of images\nevery day. When you are clipping images you often lose track of what\nthe current size is.\n3) Perhaps this is in 1.5 too (great if it is).\n4) 1:64 is 6400%\n\nStability is already sufficient here for my basic needs.\n"
    author: "Martin"
  - subject: "Re: Krita..."
    date: 2006-03-11
    body: "> I'm just that close to do a dpkg --purge on GIMP.\n\nWhy do people always want to purge non-default-toolkit-apps?\nGimp is not the best of the best of the best but at least it works. Even copy&paste between gimp and KDE applications works. Gimps strength are it's features not the fact that it happens to use GTK+.\n\nAnd yes, Gimp (and all other non-kde-applications) run very nice with KDE.\n\nAnd if you don't like the gtk file dialog, see\nhttp://www.kde-apps.org/content/show.php?content=36077\n\nThis is not against Krita. I only aske myself why recently the look of applications  has became so important that (Gnome *and* KDE) people are proud if they can purge full featured programs. It's a strange habbit in a free software world."
    author: "Martin"
  - subject: "Re: Krita..."
    date: 2006-03-12
    body: "\"Why do people always want to purge non-default-toolkit-apps?\"\n\nMaybe he doesn't like the Gimp, and if he had a replacement (possibly Krita 1.5) he wouldn't have a need to keep the Gimp around.  With Krita 1.5 I may also be able to remove the Gimp from my system (I can't stand the user interface, it has to be one of the worst ever)."
    author: "Corbin"
  - subject: "Re: Krita..."
    date: 2006-03-12
    body: "Don't be silly, Krita at least needs one or two years to reach the feature-completeness and maturity of Gimp. And I'm sure that Krita *never* will contain *each end every* feature of gimp - do you really want to reinstall gimp if you nedd one of those features?\n\nThe only rational reason I can think of is to uninstall gimp to free some MByte of harddisk space...\n\nBtw. the user interface of Gimp 2.3 has improved quite a bit..."
    author: "max"
  - subject: "Re: Krita..."
    date: 2006-03-12
    body: ">Btw. the user interface of Gimp 2.3 has improved quite a bit...\n\nIs it still using many windows? Then it isn't enough for me. Since that's the only major problem, I've with the GIMP."
    author: "Morten"
  - subject: "Re: Krita..."
    date: 2006-03-12
    body: "1. Kwin has better support for multi-window applications like Gimp.\n2. In Gimp 2.3 everythin is dockable. So you only need one gimp window and one window per image.\nNo Unix-Desktop should have problems with that.\n\nhttp://www.softpedia.com/screenshots/GIMP_3.png"
    author: "Martin"
  - subject: "Re: Krita..."
    date: 2006-03-12
    body: "This discussion is going on for years. Talk all you want but accept\nthat there is a substantial amount of people that just dont want those\nstupid multiple windows. If others like it that's ok - I don't.\n"
    author: "Martin"
  - subject: "Re: Krita..."
    date: 2006-03-13
    body: "according to <A href=\"http://bugzilla.gnome.org/show_bug.cgi?id=7379\">this bugreport</A> there is some progress in that issue, slow but umm, well not so steady\n"
    author: "Martin Zbo&#345;il"
  - subject: "KWords Tables"
    date: 2006-03-11
    body: "I've tried to use KWord many many times, but until it's very rudimentary table support improves to be more on par with open office or ms office it's going to continue to be unusable for me."
    author: "Tables4All"
  - subject: "size"
    date: 2006-03-11
    body: "notice the size decrease (41mb->36mb on src) - its mostly because of reduction of screenshots to 8bit depth (except for colour select dialogues and krita images)"
    author: "Nick"
  - subject: "Re: size"
    date: 2006-03-11
    body: "Nice tool to transform 32bit PNGs to (smaller) 8bit PNGs with optimized colour palette:\npngnq  ( http://www.cybertherial.com/pngnq/pngnq.html )\npngnq -s 1 file.png"
    author: "Martin"
  - subject: "Re: size"
    date: 2006-03-11
    body: "yes, i used it, then optipng :)\n\nanyways, its a bit disappointing that light green becomes just black after pngnq  (on OK button giph) :("
    author: "Nick"
  - subject: "Re: size"
    date: 2006-03-11
    body: "Did you use option \"-s 1\"? The default is more or less unusuable..."
    author: "Martin"
  - subject: "Re: size"
    date: 2006-03-12
    body: "thanks, i'll use it in future"
    author: "Nick"
  - subject: "Handling of embedded objects and the KO-shell"
    date: 2006-03-11
    body: "I'd like to see the handling of the embedded KOffice objects improved.\n\nFor example, when I've got a KWord document with an embedded Kivo flowchart or KCalc spreadsheet which also contains some text. When I try to search for that text from the main document, it isn't found because it's not in the main document (the KWord text file). To find these, I would have to enter every frame and search there again (and Kivio itself can't even search). Unpleasant for working, and could be hindering for scripts. Just checked with the first beta.\n\nAlso, calling the programs from the KO-shell or opening an embedded frame really clutters the main window with the programs and their toolbars.\nThe KO-shell bar is an quarter of the screen size, the toolbars (most of them empty or just badly arranged) take half of the screen and the actual document is the size of an thumbnail (still, I can enter text boxes ;)). I could post screenshots if this is not on your radar yet."
    author: "7sec"
  - subject: "Re: Handling of embedded objects and the KO-shell"
    date: 2006-03-11
    body: "The first problem is not going to be solved before 2.0. It really needs a bit of redesign. Currenly, my house is filled with KOffice hackers trying to arrive at a common solution for graphical objects. Perhaps I'll organize another one for finding a solution for the text problem. Because you're right, of course, we really want to do that.\n\nThe second problem I don't see. The KOShell bar is about 70 pixels: you cannot be working on a screen with 280 pixels horizontal, right? And if you don't like the way koshell works, start the applications separately. A bigger problem is trying to fix the flicker when switching apps, but David Faure is working on that for KDE4."
    author: "Boudewijn Rempt"
  - subject: "Re: Handling of embedded objects and the KO-shell"
    date: 2006-03-11
    body: "Well my screen is a bit bigger but the German translation for Kivio is quite long (Flussdiagramme & Diagrammbearbeitung), though again, the font-size set in the Klik-file is 1 or 2 points smaller than usual.\nPerhaps the ko-bar could be minimized like the view-bars on the left side of a typical Konqueror window.\nThe handling of text into a single size also seems to depend on the size of the frame.\nAs can be seen from the screenshots, the bars, views tabs, etc. are currently rather hard to tame when embedded. I don't know if my config just has gone wild, but I'm sure this can be improved.\n(First one is KWord from the shell, second is in an embedded Kivio frame and third is an embedded Krita frame opened from the KWord document).\n\nhttp://img99.imageshack.us/img99/6574/koview015ov.png\nhttp://img99.imageshack.us/img99/5558/koview029xa.png\nhttp://img99.imageshack.us/img99/4173/koview038il.png"
    author: "7sec"
  - subject: "Re: Handling of embedded objects and the KO-shell"
    date: 2006-03-11
    body: "Ugh, that looks just broken. In the last two images, the ko-shell frame ends up between the a toolbar and the rest of the app. It should at least stay to the far left, and not mix itself up with the application.\n"
    author: "Morty"
  - subject: "Re: Handling of embedded objects and the KO-shell"
    date: 2006-03-11
    body: "Who's the brain behind the translation of application names?\n\nKivio is Kivio and not \"some program to create flow charts and diagrams\"! \n\nNobody ever tried to translate Powerpoint into the different languages MS Office is available in.\n\nA tooltip showing that Krita is a program for raster images should be sufficient. :-)"
    author: "Martin"
  - subject: "Re: Handling of embedded objects and the KO-shell"
    date: 2006-03-12
    body: "> Who's the brain behind the translation of application names?\n\nYou can't only blame the translators. It is also the fault of the developers for tagging those strings as translatable."
    author: "blacksheep"
  - subject: "Re: Handling of embedded objects and the KO-shell"
    date: 2006-03-12
    body: "I do not blame the translators. I criticise the fact that application names are translated. Krita, Kivio, Kword, ... are *brand* names, not some random strings which have to be translated."
    author: "max"
  - subject: "Re: Handling of embedded objects and the KO-shell"
    date: 2006-03-12
    body: "If you take a closer look, it's not application names at all. KO-shell uses descriptions, in the untranslated version it says Image Object, Flowchart & Diagram, Text Documents,... There are no brand names in KO-shell, only descriptive strings which have to be translated."
    author: "Morty"
  - subject: "Re: Handling of embedded objects and the KO-shell"
    date: 2006-03-12
    body: "Right, and I remember that being in response to user demand... The best action would be for people who want to use koshell to set it to icon-only (right-click on the bar)."
    author: "Boudewijn Rempt"
  - subject: "Re: Handling of embedded objects and the KO-shell"
    date: 2006-03-12
    body: "> Right, and I remember that being in response to user demand... \n\nNot every user demand has to be obeyed :-)\n\n\n> The best action would be for people who want to use koshell to set\n> it to icon-only (right-click on the bar).\n\nThe best??\n\nThere is an application comparable to koffice-shell: Kontact\nKontact's sidebar works because there is one word of text below each icon.\n\nTry to hide the text and it becomes much more complex to choose the right icon.\n\nSo please don't disable text below icons! Text should be on by default, but only one word per icon.\n\nIf you call them \"Image, Text, Spreadsheet,...\", or \"Krita, Kword, Kcalc,..\" is your decision. I would opt for the latter one because it's 100% exact (and I dont like Koffice but I like the individual Koffice applications :-P ).\n"
    author: "Martin"
  - subject: "Re: Handling of embedded objects and the KO-shell"
    date: 2006-03-12
    body: "I was thinking what users can do right now. Maybe you could add a bug report for koshell to use the name of the application instead of the explanation, Sven could think about it."
    author: "Boudewijn Rempt"
  - subject: "Re: Handling of embedded objects and the KO-shell"
    date: 2006-03-12
    body: "> If you take a closer look, it's not application names at all. KO-shell uses \n> descriptions, in the untranslated version it says Image Object,\n\nYou are right, I was on the wrong track because 7second wrote:\n\" but the German translation for Kivio is quite long (Flussdiagramme & Diagrammbearbeitung)\".\n\nBut my point still stands: Everyone knows Gnumeric, Abiword, K3b,...\nDo you really want that people only know that they are using some program for \"Text Documents\" or \"Flowcharts & Diagrams\"? It's bad enough that Suse sometimes puts only the category name instead of the application name into the K-Menu (when one category contains only one application). \nBut at least let's try to make the Koffice applications known. \"Image Object, Flowchart & Diagram, Text Documents\" are typical strings for tooltips!"
    author: "Martin"
  - subject: "Re: Handling of embedded objects and the KO-shell"
    date: 2006-03-13
    body: "> But my point still stands: Everyone knows Gnumeric, Abiword, K3b,...\n\nThis is an interesting point: those in positions such as yours often forget that not everybody knows the names of these applications.  There are plenty of people who don't care, and don't want to know.  They use the computer as a tool, and think in terms of tasks they want to get done, such as burning data to CD, writing a letter, making a diagram and so on.\n\nSome names are sufficiently self-descriptive for those users (KWord, KSpread), but others aren't (k3b, Kivio, Krita).  For these people, having application descriptions is an absolute necessity - the machine becomes practically unusable without them.\n\nWhether this lack of association between names and functions is bad or not is secondary.  It exists, and needs to be dealt with.\n\nI like Boudewijn's suggestion that the text be configurable (between name and function).  The K-menu already is, and can even have both.  That's very nice.  My suggestion would be to default to showing the function (as it now is) and adding the name as a tooltip; if application names are show, their function should be the tooltip.  For no text, the tooltip could contain some combination of both (\"name - function\" or somesuch).\n\nThe problem that affects the German translation is twofold:\n- English translated into German generally gets longer.\n- I doubt the translators had any information about where/how the translations are used (ie: whether they should be brief or more descriptive).  In this case, \"Diagramme\" would probably be sufficient, even though it's not strictly identical.\n\n-- Steve"
    author: "Steve"
  - subject: "Re: Handling of embedded objects and the KO-shell"
    date: 2006-03-11
    body: "ask german l10n team to insert a \\n in that label:\nFlussdiagramme &&\\nDiagrammbearbeitung"
    author: "Nick"
  - subject: "Re: Handling of embedded objects and the KO-shell"
    date: 2006-03-12
    body: "\"Skalierbare Grafiken\"\n--> Vektorgrafik\n\n\"Flussdiagramme & diagramm\n--> Modellierung, Verfahren, Diagramme\n\n\"diagramme\"\n-> \"Darstellung\""
    author: "Gert"
  - subject: "Re: Handling of embedded objects and the KO-shell"
    date: 2006-03-12
    body: "It seems to be a problem in general that translators don't put much priority on being succinct. Obviously some languages are just more verbose, but still..."
    author: "Ian Monroe"
  - subject: "kspread looses information"
    date: 2006-03-11
    body: "Just uses spread to read my spreadsheets that I made with older kspread,\nbut the new one looses information.  On first sight text is lost and some\ncells report division by zero...   In the old one this was just good...\nI'll need to do some more investigation to understand better what is wrong.\nHowever, I won't be around in the coming days.  I just wanted to make this\nknow.  Perhaps someone else can report and investigate the same problem...\n\n"
    author: "Richard"
  - subject: "Videos in KPresenter"
    date: 2006-03-12
    body: "It would be great to be able to embed a video in a KPresenter slideshow. Will we see that any time soon?"
    author: "An Onymous"
  - subject: "Re: Videos in KPresenter"
    date: 2006-03-12
    body: "We started yesterday working on a library that would give that capability for KPresenters, Kivio, Karbon and maybe even KWord and Krita. But it's not going to be soon: it's the stuff KOffice 2.0 will be made of."
    author: "Boudewijn Rempt"
  - subject: "Re: Videos in KPresenter"
    date: 2006-03-16
    body: "Thank you thank you thank you!"
    author: "anon"
  - subject: "where do you want example files?"
    date: 2006-03-12
    body: "Where's the best place to post complex files that work in oo.o and fail in koffice?  Bugs.kde.org?  Koffice mailing list?  Here?  Or are they not all that useful?\n\nI would love to switch over but my most important spreadsheet (a retail store employee schedule) is impossible to use or even reimpliment in kspread.  I consider myself somewhat of a beginner when it comes to spreadsheets so the fact that this one doesn't work is kind of disappointing. :-/\n\nHell, I'll post it here anyway.  Names and sales numbers have been anonymized.  Note that the layout is not negotiable-- it is required to exactly match the company's paper form.  This file has not been checked in beta2--I don't have the time to compile it myself, so I'll wait until debian unstable has it.  In beta1 it was such a complete disaster that I didn't know where to start with bug reporting--everywhere I looked there were more and more things that didn't work right, so I gave up.  Here's hoping beta2 will be an improvement!\n"
    author: "brian"
  - subject: "Re: where do you want example files?"
    date: 2006-03-12
    body: "The koffice developers mailing list is really the right place, that and/or bugzilla.All developers read the mailing list and bugzilla, but not all developers read the dot."
    author: "Boudewijn Rempt"
  - subject: "Re: where do you want example files?"
    date: 2006-03-12
    body: "bugs.kde.org would be the best place since it helps us keep things more organised.\n\nYour example spreadsheet is largely messed up in Beta 2 as well.  We desperately need more real-world examples of spreadsheets like this, so if you have any other sheets which you could anonymise and make available that would be very helpful.\n\n"
    author: "Robert Knight"
  - subject: "sharpen dialog is missing"
    date: 2006-03-13
    body: "IMO very important filter sharpen is at the moment not usable. I have no option\nto set the level to sharpen. "
    author: "Stephan"
  - subject: "Re: sharpen dialog is missing"
    date: 2006-03-13
    body: "Can't do anything about that for 1.5, I'm afraid. We're in UI and string freeze and I just cannot add a whole new dialog. It'll have to wait for the next release. You could use the custom convolution filter in the meantime (although that doesn't offer a possibility to save frequently used kernels).\n\nWe're also working on an extension plugin pack for Krita that should be released shortly and that already contains quite good blur and unsharp mask filters."
    author: "Boudewijn Rempt"
  - subject: "kde.org showing 1.5 beta1"
    date: 2006-03-23
    body: "kde.org is still showing 1.5 beta 1 released.  Anyone here that can change this?\n\nBen"
    author: "Ben"
---
With more than 1500 improvements since the first beta release of the 1.5 series, the KOffice developers invite the user community for the final round of testing of KOffice 1.5 before the first release candidate. Read the full <a href="http://www.koffice.org/announcements/announce-1.5-beta2.php">announcement</a>, the <a href="http://www.koffice.org/announcements/changelog-1.5beta2.php">changelog</a>, <a href="http://download.kde.org/download.php?url=unstable/koffice-1.5-beta2">download the release</a>, and give it some real world testing for us!








<!--break-->
<div style="border: thin solid grey; margin: 1ex; padding: 1ex; width: 250px; float: right;">
<img src="http://static.kdenews.org/jr/kchart-wee.png" border="0" width="250" height="213" />
The new KChart engine
</div>

<p>In this release, <a href="http://www.koffice.org/kchart">KChart</a> received a new charting engine, donated by <a href="http://www.klaralvdalens-datakonsult.se/">Klarälvdalens Datakonsult AB</a> and an initial implementation of the charting OASIS OpenDocument file format. We managed to improve <a href="http://www.koffice.org/krita">Krita</a>'s performance in loading large images, rendering complex images, creating gradients and showing filter previews. There are also a lot of bug fixes. Dag Andersen has reworked the <a href="http://www.koffice.org/kplato">KPlato</a> calculation engine and improved the user interface; and the documentation team has written a complete KPlato manual. And that's just the highlights: across the board, we have improved OpenDocument support in <a href="http://www.koffice.org/kword">KWord</a>, <a href="http://www.koffice.org/kpresenter">KPresenter</a>, <a href="http://www.koffice.org/kspread">KSpread</a> and <a href="http://www.koffice.org/karbon">Karbon14</a>. All applications have also received user interface polish.</p>







