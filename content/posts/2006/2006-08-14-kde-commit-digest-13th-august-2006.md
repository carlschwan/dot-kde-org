---
title: "KDE Commit-Digest for 13th August 2006"
date:    2006-08-14
authors:
  - "dallen"
slug:    kde-commit-digest-13th-august-2006
comments:
  - subject: "KTU"
    date: 2006-08-13
    body: "What's KTU?"
    author: "knut"
  - subject: "Re: KTU"
    date: 2006-08-13
    body: "In the digest it  says KTU (KDE Translation Updater), it's something introduced  into SVN 2 weeks ago it seems. For more info and screenshots look in last weeks digest, here is a snip: \n\"The idea is to create an interface to fetch translations straight from KDE SVN and install them for the user. Translations can be downloaded and installed on a per-application basis.\" \n\n"
    author: "Morty"
  - subject: "Re: KTU"
    date: 2006-08-13
    body: "like Firefox autoupdates on windows?\n\nWill this work for Klik packaged applications as well? Will it break your package management software?\n\nAre translations to be stored in home?"
    author: "knut"
  - subject: "Re: KTU"
    date: 2006-08-14
    body: "This is just for translations, I thought the Firefox updater did the whole application.\n\nThe way I read it, the translations get instaled on a per user base. That means install in home, and not interfer with your packagemangemnet software. As for working with Klik, I havn't the faintest idea. I guess it depends on how klik packages handles translations."
    author: "Morty"
  - subject: "Re: KTU"
    date: 2006-08-14
    body: "KTU downloads po-files from KDE's SVN, compiles them to mo-files and puts them into ~/.kde/share/locale/your_language/LC_MESSAGES\nIf  you start a kde application, it will first look in $KDEDIR/share/locale/your_language/LC_MESSAGES for a translation, then in ~/.kde/share/locale/your_language/LC_MESSAGES and will use the latter if it exists.\n\nKTU is handy for updating translations if an application was only partially translated during the release and is now fully translated available in SVN, for updating a translation after fixing errors, and for easy integration of your own translating work in exisiting installations (after your coordinator has proofread them and put them into SVN ;)."
    author: "Rinse"
  - subject: "Strigi"
    date: 2006-08-13
    body: "Will strigi be renamed tenor?"
    author: "pon dou"
  - subject: "When is the Release of the kde Techology preview"
    date: 2006-08-13
    body: "When is the Release of the kde Techology preview?\n\nIs there somthing planned ?"
    author: "chris"
  - subject: "Re: When is the Release of the kde Techology preview"
    date: 2006-08-14
    body: "oktober, on the 10th 'birthday' of KDE. a snapshot might come soon, but it's really really useless for non-developers... it won't show you even the smallest exciting thing. well, a few small things, like a smooth Kstars, and some random very small things, but not much to show off i'm afraid ;-)"
    author: "superstoned"
  - subject: "Re: When is the Release of the kde Techology previ"
    date: 2006-08-16
    body: "So when will KDE 4 and all the programs assoicated with it be ready. 2007, 2008 ?\nWill most apps be a true port or will the be using some kind of KDE3 library.  Will KDE 4 boot even faster or will it boot slower.  The reason I ask is QT4 is still slower then then QT3.\n\n "
    author: "2percent"
  - subject: "Re: When is the Release of the kde Techology previ"
    date: 2006-08-16
    body: "> So when will KDE 4 and all the programs assoicated with it be ready. 2007, 2008 ?\n\nNot 2006 :p I guess Q2 2007.\n\n> Will most apps be a true port or will the be using some kind of KDE3 library. \n\nIt might be that the one or other apps use some Q3Foo-classes but that doesn't mean they aren't true Qt4 apps. They are using 99.9% Qt4-classes and the one left-over from Qt3 for example.\nBut many apps are pure Qt4 *today* (in svn trunk, of course).\n\n> Will KDE 4 boot even faster or will it boot slower. The reason I ask is QT4 is still slower then then QT3.\n\nQt4 is way faster in most things than Qt3. But startup time is not only based on Qt3/Qt4 but also which parts of KDE are loaded, in which order, how much they are optimized, ..."
    author: "Carsten Niehaus"
  - subject: "Guidance"
    date: 2006-08-14
    body: "Guidance looks promising.\n\nWill kwine also be included?"
    author: "max"
  - subject: "Re: Guidance"
    date: 2006-08-14
    body: "I think guidance is a set of configuration modules. Kwine is a set of tools to integrate KDE with wine, among them a cnfiguration module. So it can't be included in Guidance, but it can be included with the default KDE distribution."
    author: "aa"
  - subject: "Re: Guidance"
    date: 2006-08-14
    body: "Besides that, Guidance contains a module for wine configuration."
    author: "Rinse"
  - subject: "Re: Guidance"
    date: 2006-08-18
    body: "Guidance includes a module for wine configuration (\"wineconfig\"), which is nearing completion and includes everything from winecfg and a few extra settings.\n\nkwine is a different project that includes various pieces to improve wine/kde integration.  Its features won't (at least yet) be included in guidance."
    author: "Yuriy Kozlov"
  - subject: "Unity"
    date: 2006-08-14
    body: "So are you gonna replace khtml with Unity? that would be great"
    author: "Bob"
  - subject: "Re: Unity"
    date: 2006-08-14
    body: "and why would that be great? it has not yet been determined that apples tree is better than khtml.."
    author: "anonymous"
  - subject: "Re: Unity"
    date: 2006-08-14
    body: "More developers working on the same codebase is a good idea IMHO. In particular as the both engines are derived from the same parent and thus current development appears to be a duplication of efforts (and thus a waste of resources). This is different from KHTML vs. Gecko where the code base has \"nothing\" in common and the design philosophy seems to be different, too."
    author: "MK"
  - subject: "Re: Unity"
    date: 2006-08-15
    body: "More chefs only spoil the dinner unless they can can cooperate."
    author: "Carewolf"
  - subject: "Re: Unity"
    date: 2006-08-15
    body: "it is my understanding that KHTML offers more than enough places, where work is needed. I never saw any comment of any KHTML developer complaining about too much men power. Thus I assume the kitchen is big enough to keep all chefs happy ;-)"
    author: "MK"
  - subject: "Re: Unity"
    date: 2006-08-15
    body: "The above comment was from a KHTML developer, FYI.\n"
    author: "SadEagle"
  - subject: "Re: Unity"
    date: 2006-08-14
    body: "There are no such plans at the time.\n"
    author: "SadEagle"
  - subject: "Re: Unity"
    date: 2006-08-15
    body: "I have to say this is very sadEagle "
    author: "Bob"
  - subject: "Re: Unity"
    date: 2006-08-15
    body: "*giggle*"
    author: "Anony Moose"
  - subject: "sudo"
    date: 2006-08-14
    body: "Hehe, I myself wasn't really on the edge of my seat aching and waiting for the sudo patch or anything but I had stumbled accross the bug report at one point after switching to Kubuntu then trying to build KDE from source and wondering why my passwords stopped working :). Good to see Jonathan Riddell's year old patch is finally merged... I imagine he's been busy elsewhere helping make Kubuntu and KDE rock."
    author: "Anonymous"
  - subject: "great"
    date: 2006-08-14
    body: "Great digest, thank you. BTW, who is Andreas Kling?"
    author: "aa"
  - subject: "Statistic creaton"
    date: 2006-08-14
    body: "[Offtopic]\n Commits:  \t 2331 by 197 developers, 5139 lines modified, 1787 new files.\n\nHow is this statistics created? With the help from SVN or some other thing? I have a SVN server and would like to know how this can be done.\n\n[/Offtopic]"
    author: "MaBu"
  - subject: "Re: Statistic creaton"
    date: 2006-08-14
    body: "its done probably with a small script issuing a few svn commands.. "
    author: "redeeman"
  - subject: "Re: Statistic creaton"
    date: 2006-08-14
    body: "Thanks for info. Anyone knows which commands, the only one that seems to me to be the right one is svn diff, with some scripts that parse output."
    author: "MaBu"
  - subject: "Re: Statistic creaton"
    date: 2006-08-15
    body: "Ask the author of the digest."
    author: "Rinse"
  - subject: "Re: Statistic creaton"
    date: 2006-08-18
    body: "Look at the kde svn repository for areas/cvs-digest. Find a perl script called stat_svn.pl.\n\nYou pass two numbers, starting and ending commits. It iterates through each commit.\n\nDanny does something similar.\n\nDerek"
    author: "Derek"
  - subject: "other SoC projects"
    date: 2006-08-14
    body: "Is there any place where we can read on progress regarding the various SoC projects? Specifically, I'm interested in progress on avKode, the ffmpeg backend for Phonon."
    author: "Ronald"
  - subject: "Re: other SoC projects"
    date: 2006-08-14
    body: "Cia is the page with progress report on various OSS projects. http://cia.navi.cx/"
    author: "MaBu"
  - subject: "Re: other SoC projects"
    date: 2006-08-15
    body: "To be more helpful, http://cia.navi.cx/stats/author/carewolf :)"
    author: "Ian Monroe"
  - subject: "question about search?"
    date: 2006-08-14
    body: "Is it possible for distro packagers to pre-package the indexes of known docs like those in th the /usr/share/doc tree so that when initial installs or system updates are done the indexes for the new/updated doco could be added directly to the system's index file without having to traverse a bunch of known ahead of time content?\n"
    author: "borker"
  - subject: "Re: question about search?"
    date: 2006-08-15
    body: "yes they can"
    author: "Rinse"
  - subject: "Whats the Buzz?"
    date: 2006-08-14
    body: "I can understand most of the measures but what the heck is 'Buzz'? \n\nWhat's it supposed to be and how is it measured?"
    author: "Peter"
  - subject: "Re: Whats the Buzz?"
    date: 2006-08-17
    body: "Hi,\n\"Buzz\" is the name I give to a rating that I generate from several components, including commit activity, mentions on webpages, and short-term news and discussion (eg. blogs). These are added together to produce the score. Of course, the scores are not completely scientific, and are meant as a way to measure \"popularity\" relative to the other calculated scores.\n \nThanks,\nDanny"
    author: "Danny Allen"
  - subject: "Oskar?.... Another half-baked multimedia app"
    date: 2006-08-15
    body: "We already have too many half-baked multimedia pps (NoAtun, Kaffeine, Kodeine, Amarok, Juk, KMPlayer, KMIDI, KSCD, etc). Developers should _work together_ to improve the best of these apps and not reinvent the wheel poorly everytime.\n\nI know there is learning curve to reading someone else's code, but in the end you'll get a better, more professional, more finished, and more usable product if you build on other people's work."
    author: "AC"
  - subject: "Re: Oskar?.... Another half-baked multimedia app"
    date: 2006-08-15
    body: "I certainly wouldn't put Amarok in a list of \"half-baked\" multimedia applications.  In fact it is better than commercial rivals such as Windows Media Player and iTunes in many ways.\n\nI agree that it would be nice to see a KDE video player of the same quality as amarok, but there is no point telling developers that they should or should not work on a project.  Most are non-paid volunteers after all.  \n\nBut the good news is that if you see an area of KDE or a program that needs improvement, you have the freedom to get involved and make it happen :) "
    author: "Robert Knight"
  - subject: "Re: Oskar?.... Another half-baked multimedia app"
    date: 2006-08-15
    body: ">>I certainly wouldn't put Amarok in a list of \"half-baked\" multimedia applications.\n\nSame goes for juk, kaffeine, kmplayer, ....\nThose applications are quite complete, very usable and certainly not half baked.\nTrue, not all of them are loaded with features, but not every user wants a complex application that is stuffed with functionality that he/she will never use...."
    author: "Rinse"
  - subject: "Re: Oskar?.... Another half-baked multimedia app"
    date: 2006-08-16
    body: "Kaffeine and KMplayer certainly are half baked, though Kaffeine in particular is getting there. The simple truth is that neither is stable enough to use, and I think the fact that neither ships with KDE is a reflection on this. Noatun and KSCD are finished in that they do what they're designed to and do it well, but we really don't have a good video player. I don't understand why, given their position on the bloat/features tradeoff when it comes to everything else, the amarok developers don't add video playback, but they haven't, and as it is a good video player is something we're lacking, especially in contrast to Totem. And whilst I certainly agree with letting anyone work on any player they want, I think the interests of KDE as a whole would be better served by picking one best candidate (Kaffeine would be my preference, but I'd prefer KMplayer being chosen to the situation we currently have), calling it the official KDE video player, and focussing attention and marketing on it."
    author: "mikeyd"
  - subject: "Re: Oskar?.... Another half-baked multimedia app"
    date: 2006-08-16
    body: "IMHO, Codeine is the most complete and well-designed apps. Maybe because it's goals are so simple ('KISS'), but it's very well accomplished.\nhttp://www.methylblue.com/codeine/\nBeen using it for some time, and it's very stable."
    author: "PhinnFort"
  - subject: "Re: Oskar?.... Another half-baked multimedia app"
    date: 2006-08-22
    body: "how to change volume in it?\ni press \"1\" it shows kinda slider but i cant change anything\n\nand btw it just crashed after closing it\nii  codeine                          1.0.1-3.dfsg-1                   Simple KDE video player\n"
    author: "Nick"
  - subject: "Re: Oskar?.... Another half-baked multimedia app"
    date: 2006-08-16
    body: "> neither is stable enough to use\n\nI use Debian Sarge (with Kaffeine 0.6) and Ubuntu Dapper (with Kaffeine 0.7.1), and both Kaffeine versions hardly ever crash when I'm using them. (The Kaffeine in Ubuntu Hoary, however, did crash often.)"
    author: "rqosa"
  - subject: "Re: Oskar?.... Another half-baked multimedia app"
    date: 2006-08-16
    body: "The latest Kaffeine (0.8) just rocks. I wonder what your problem is. Particularly Totem shouldn't be a competitor to be mentioned.\n\nIf there are any problems playing videos it is sibject to the video backend (xine). For playing wmv's and avi's the right Windows codecs must be installed. The GUI is nice, stable and very usable."
    author: "sebastian"
  - subject: "Re: Oskar?.... Another half-baked multimedia app"
    date: 2006-08-17
    body: "kaffeine, kmplayer and kplayer all run stable on my machine. If they don't run stable on your computer, then it must be a problem with your setup i guess.\n\nDid you file bugreports about your problems?\nTotem is a rather simple video player, probably comparable with kmplayer, but not with kaffeine.\n\nAmarok is a jukebox, not a versatile mediaplayer that should also play video.\nPerhaps that changes in the future, when portable videoplayers become more common..."
    author: "Rinse"
  - subject: "Re: Oskar?.... Another half-baked multimedia app"
    date: 2006-08-16
    body: "> But the good news is that if you see an area of KDE or a program that needs \n> improvement, you have the freedom to get involved and make it happen :) \n\nBut the bad news is that if you are lucky that this effort will be met with simple indifference; if you are unlucky it will be met with open hostility.\n\nThis is probably why people start a new project rather than working on improving an existing one.\n\nDevelopers need to decide if this is about them, or if it is about the product.\n  "
    author: "James Richard Tyrer"
  - subject: "Re: Oskar?.... Another half-baked multimedia app"
    date: 2006-08-17
    body: "Not again :-(\n\nSee http://dot.kde.org/1139614608/1139690807/1139762954/1139768731/1139810025/1139816710/1139857536/1139871749/1139876984/1139913110/\n\n"
    author: "ariya"
  - subject: "Re: Oskar?.... Another half-baked multimedia app"
    date: 2006-08-18
    body: "> Not again \n\nDid you some how expect that the problem would just go away?\n\n> Contrary to popular belief, developers are not machines. They have heart, \n> they have feelings.\n\nThe same can probably be said of engineers, but we have learned to get over it. :-)\n\nWhat have we learned to get over?  We avoid investing too much emotional capitol in our way of doing something.  Instead, we have learned to derive our emotional satisfaction from how good the final product is.  \n\nAnd as I have said before, engineers are direct to the point of being blunt.\n\nSo, to be blunt: Developers have too much ego and this gets in the way of making the best product that we could.  They need to get over it; this really *will* result in a better product.  Otherwise, people will write HIG (CHI) guidelines and write usability report and neither will do any good because developers think that \"those who do the work decide\" means 'those who write the code decide' -- that writing the code is what 'the work' is.\n\n\n\n"
    author: "James Richard Tyrer"
  - subject: "Re: Oskar?.... Another half-baked multimedia app"
    date: 2006-08-18
    body: "No, I don't expect that the problem would just go away. \n\nAnd do you somehow expect that the assertion that you post here again and again would make the problem go away?\n\nAs for your engineers comparison, I already have my theory in http://dot.kde.org/1139614608/1139690807/1139762954/1139768731/1139810025/1139816710/1139857536/1139871749/1139876984/1139913110/\nBasically you did not compare the same thing.\n\nImagine I'm an engineer working on a gigabit optical transmitter. A big customer complaints to the company, \"Look, your transmitters are crap. The laser frequency keeps changing every millisecond\" and the marketing departement consults me about this problem. Surely I must take this into account for further product development, or else my manager won't be happy.\n\nAnd if at the same I spend endless hours building couple of BEAM-based insect robots as a hobby, and then a coworker comes over to my office and says (bluntly), \"Look, your insects are crap. They're ugly and useless. How about a talking dog?\" and I'll be sure to ban this dude from my place. Not that my insects can fly. Not that my insects can talk. Not that my insects are beautiful. \n\nIt's my baby. If you attack my baby, you'd better be nice! Not that my baby can fly. Not that my baby can talk. Not that my baby is good looking. Not that my baby is genius. Not that I don't you to attack my baby. You can e-mail me bazillion times that this *will* result in a better baby (and technically I agree with you, no little objection at all), but it's *my baby* you're criticizing. You should consider the non-technical things as well, like the way you're talking about it.\n\nAnd for the \"too much ego\" things, I can't recall any of KDE developers (that I know) have that. You should back your statement with facts. Otherwise, those included in the \"developers\" part might claim that you are the one who actually starts that \"open hostility\".\n\n\n\n\n\n\n"
    author: "ariya"
  - subject: "Re: Oskar?.... Another half-baked multimedia app"
    date: 2006-08-19
    body: "Actually, your reply is crap.  Actually, only part of it is crap since part of it is indicative of the problem.\n\nIn rhetoric, it (the part which is crap) is called a straw man.  We aren't talking about developers taking offense at people telling them that their application is crap.  If people do that, they have every right to take offense.  \n\nWhat we are talking about is people either telling developers exactly what they think is wrong with the app, how the app could be improved, that there are design flaws (such a suggestion is not \"gratuitous\" if it is specific), or exactly why it doesn't work.  Such comments should be taken as helpful.  The problem is that developers with too much ego take such comments as attacks -- even as personal insults.\n\nYou said it; they consider their application to be their \"baby\".  This is the problem; they have too much emotional capitol invested in their app to be objective about it.  They need to get over this because it is detrimental to making a better product.  This isn't somebody's baby, it is our product.\n\nI do know of KDE developers that have too much ego (when it comes to their work), but it wouldn't be fair to them to single them out here.\n\nExample: \n\nSuggestion: It would probably be be better if the menus in you application conformed to the KDE UI guidelines.\n\nResponse: I read them once and never looked at them again.\n\nYes, there is a problem.  It would be better if you offered suggestions on how to address it rather than rhetoric designed to deny the existence of the problem or to make excuses.  The problem here is simple; it is the 'go start your own project' syndrome.  And, surprise! that is what has happened."
    author: "James Richard Tyrer"
  - subject: "Re: Oskar?.... Another half-baked multimedia app"
    date: 2006-08-19
    body: "Maybe my reply is crap.\n\nWhen you write that \"people either telling developers exactly what they think is wrong with the app\", that can means stepping the developer's toes as well. And who knows how big are his toes? I know some people, who no matter how polite you talk to them, it will be taken as hard critics.\n\nAnd I believe you misunderstood some of points I wrote before. I don't deny the existence of the problem. I don't make excuses about it as well. I merely suggested the theory (I'm not surprised should you think my theory is also \"crap\" and \"rhetoric\") which may explain the cause of the problem. You can stamp it as an excuse if you want, although I thought engineers believe that finding the cause is the first step towards the solution.\n\nAnd so you do know KDE developers that have too much ego? Fine. Then our experiences varied, cause I never got such response before. You can always claim that \"developers have too much ego\", it's your opinion after all. But I will then always also write \"no, that's not true\".\n\nLet me play the sensitive developers side again. I spent sleepless nights writing this wonderful FantasticFoo program and thought others might find it useful as well, and I then released on my site so people can grab it. And suddenly one chap that I don't know before tell me how the design flaw must be fixed because it's important to make a better product. And it isn't my baby, it is our product. Oh, since when it's suddenly become OUR product? And YOU are supposed to tell ME to fix the flaw? And how are YOU going to do that? With e-mails saying that I have my ego and must get over it? Ask me to write hundred times that I have to get over my ego? Or perhaps just flip a switch? Convincing a heavy smoker to stop smoking with hundreds of research papers has better chance than this.\n\nIt would be better if you offered suggestions on how to address the problem rather the same \"the developers need to get over this\" statement as it has been posted here again and again. Or perhaps I understood it wrongly. Is this statement perhaps one of your suggestions?"
    author: "Ariya"
  - subject: "Re: Oskar?.... Another half-baked multimedia app"
    date: 2006-08-20
    body: "I believe that I have stated the problem.  Developers have too much emotional capitol invested in their apps.  For the good of the project they need to get over this.  Or perhaps it is better stated that the project needs them to get over it.  How to do this?  I don't know.  I can only tell you that engineers somehow learn to do so.\n\nI don't see treating developers like prima donnas as a reasonable way to do things.  Developers shouldn't behave like prima donnas.  How are such prima donnas going to react when they receive a usability report written by the usability people or a UI guideline compliance report written by the HIG people.  This is a large project, lonewolves aren't going to fit in too well.\n\nBut to get back to the point: users \"telling developers exactly what they think is wrong with the app\".  This is called feedback.  Developers should welcome such feedback -- it should be taken as what it is: one person's opinion.  Now > 90% of it will probably be crap, but that other 10% might have some good ideas.  And occasionally someone might tell you exactly why your app doesn't work -- certainly that shouldn't be considered as an attack (but it is).\n"
    author: "James Richard Tyrer"
  - subject: "Re: Oskar?.... Another half-baked multimedia app"
    date: 2006-08-20
    body: "Money. Engineers do something they are told to do and that they don't want to do because they get money for it. With the money, they can buy food and leisure time to work on something they want to do, like working on an application for KDE."
    author: "Boudewijn Rempt"
  - subject: "Re: Oskar?.... Another half-baked multimedia app"
    date: 2006-08-21
    body: "Obviously you are not an engineer.  And from what I have seen, you never will be one. \n\nYour suggestion is absurd.  You suggest that engineers use efficient methods only because they are being paid.  I am still an engineer -- still use engineering methods -- despite the fact that I am retired.  I have not become an undisciplined hacker just because I am not being paid.\n\nI suggested that developers should take personal responsibility for their own product.  Your reaction was to post this:\n\nhttp://www.valdyas.org/fading/index.cgi/hacking/tqm.html\n\nwhere you demonstrate your total ignorance by showing everyone that you have no idea what so ever what TQM is.  Under the standard management model only the quality control people are responsible for quality and they tell the other workers what to do to *try* to achieve it.  However, under Total Quality Management, _everyone_ is responsible for quality.  This is (to use Tom Peters' favorite word) empowerment; it is not, as you wrongly suggest, some kind of subjugation.  \n\nWorkers prefer TQM to the old and inefficient quality management methods, and it improves productivity -- this is why I said:\n\n\"Someone posted that TQM had to be forced on people. Possibly, but the question is whether after it is forced on them that they ever want to go back to working without it.\"\n\nI think that what you, and others such as you, really object to is that, with TQM, you are responsible for the quality of your own work.\n\n> I'll be damned before I'll allow myself to be managed. Especially by someone\n> who hasn't earned my respect at all. And there's no way, short of taking  \n> away my CVS account, you can force me to comply.\n\nI presume that by that you mean that you will never be willing to take responsibility for the quality of your own work.\n\n> If someone whose work I admire tells me to fix this, do that, or pull up my \n> socks and start producing better code, I'll probably heed him or her. But  \n> that's the only kind of management I'll accept.\n\nSo, you also admit that you do not judge suggestions based on their merit -- no meritocracy for you!  This is your loss, not mine.\n\nClearly, you are illustrative of the problem I have been describing.\n\nYou also appear to be one of those people that equate knowing a computer language with knowing how to engineer programs.  The corollary to this is that some people appear to equate doing the GUI for a program with writing the program -- there needs to be a working program under that GUI.  \n\nYou are probably even good at writing code -- perhaps even as good in C++ as I was with FORTRAN (numerical methods programs are not usually written in C or C++).  But if you don't learn how to design programs you wind up with code that has a slick GUI and runs OK, but doesn't do the correct things.  Such software does not impress me at all.  \n\nPerhaps it is my background.  There is no question in my mind that a numerical analysis program that has a wonderful GUI but gets the wrong answers is worthless."
    author: "James Richard Tyrer"
  - subject: "Re: Oskar?.... Another half-baked multimedia app"
    date: 2006-08-21
    body: "\nYour logical argument is absurd. WHEN an engineer is paid to do a crappy task, THEN he must do that (and overcome his ego, yada yada). But nothing stops him from doing that crappy task (although he's not obligated to) even he is NOT paid. To said otherwise is a fallacy. \n\nOpen source development, like Boud asserted, is a voluntary work. And as many others who do voluntary works, mostly it's about having some fun. You can give some developers a very nice feedback but if they aren't convinced that it's going to be fun, they can just ignore it (no matter how amazing, constructive, brilliant and wonderful your feedback is).\n\nIs this an egoistic point-of-view? Perhaps yes. Is it a problem? For some people, it is. But is it natural? Of course. Tell me how we do feel if our (spare time) hobbies are not fun anymore. Many will choose to spend time with their kids, families and/or friends instead.\n\nPerhaps I can have some fun applying engineering methods to my unpaid hacking sessions. But some others do not feel like that. They won't reduce their sleeping time just to experience another nightmares. And they also can tell me to do something that I really hate as my hobby.\n"
    author: "ariya"
  - subject: "Re: Oskar?.... Another half-baked multimedia app"
    date: 2006-08-22
    body: "I think that you missed what BR meant.  He refuses to use TQM methods.  He refuses to do so despite the fact that they would increase his productivity.  He wrongly presumes that engineers use such methods only because they are paid.\n\nOr, he could have meant that engineers take responsibility for the quality of their work only because they are paid for it.  This is not the case and it is why I said that he would never be an engineer.\n\nYes, people will do crappy tasks because they are paid to do them.  But, that does not mean the people won't do them unless they are paid to do them.  Socrates' cat is not a dog, after all.\n\nWe presume then that part of the work needed to have a software project 100% finished would be classified as \"crappy tasks\" by many developers, and that developers won't do crappy tasks.  This idea means that all Open Software projects are doomed.  Perhaps it does explain why many projects never get past the 90% completed point.\n\nOTOH, it does not explain why some developers react so negatively to feedback and offers of help.  And, that was the original question which has nothing to do with whether engineers are responsible people only because they are paid or not."
    author: "James Richard Tyrer"
  - subject: "Re: Oskar?.... Another half-baked multimedia app"
    date: 2006-08-22
    body: "\nLook, I did not and never did comment on Boud's opinion on TQM nor your analysis of Boud's opinion on TQM. What I have written above is about my disagreement in understanding Boud's statement posted before (not anything about his blog entry). \n\nBoud wrote: \"Engineers do something they are told to do and that they don't want to do because they get money for it\". When I read this, I understand that money is definitely the cause that makes the engineers do this and that. But I do not conclude that the engineers are forbidden to do this and that if they are not paid, i.e. no money is involved. \n\nOn the other hand, you replied \"Your suggestion is absurd. You suggest that engineers use efficient methods only because they are being paid\". Furthermore, you wrote \"He wrongly presumes that engineers use such methods only because they are paid\" and again \"that engineers take responsibility for the quality of their work only because they are paid for it\". Compared to my understanding, you have made a gross extrapolation here by putting the word \"only\".\n\nThat what I meant in the first paragraph of my previous reply. You even seem to agree because further down you wrote \"But, that does not mean the people won't do them unless they are paid to do them.\" And you think I missed what Boud meant? Care to elaborate?\n\nAs for the original question why some developers react so negatively to feedback and offers of help, you have your own theory already: \"developers have too much emotional capitol invested in their apps\". I have offered my explanations as well: \"you step their toes and their toes are big\" and \"they think you spoiled their fun\". Boud has his own reason: he won't allow himself to be managed, especially by someone who hasn\u0092t earned his respect at all. Do these all answer that question or are you not yet satified yet with the answers?"
    author: "ariya"
  - subject: "Re: Oskar?.... Another half-baked multimedia app"
    date: 2006-08-23
    body: "The word 'only' is a major part of the point but perhaps what I meant was not understood.\n\nBR suggests that engineers do things because they are being paid to do them that they would not do otherwise.  If being paid is the cause of them doing these things, then they do them 'only' because they are being paid to do them.\n\nI have asserted that this is not true:\n\n1.  That people will use efficient methods because they make the task easier.\n\n2.  That people will take personal responsibility for the quality of their work because that is the right thing to do.\n\n3.  That people will do \"crappy things\" either out of a sense of personal responsibility or for the delayed positive reinforcement of a job well done.\n\nand that people will do these things even if they are not paid for what they are doing -- at least good engineers will do these things.\n\nI do not understand why people reject #1 because using efficient methods should be intrinsically reinforcing.  And, I do not know how to instill the values of #2 & #3 in developers -- obviously it would improve the project if we had more developers with these values.\n\nIIUC, we are in basic agreement on why some developers take offense at feedback and offers of help but we have no solution for the problem.  \n\nIt is an unfortunate situation because I am willing to do \"crappy things\" to improve the quality of the product and those that reject such help are probably not willing to do them, yet my offers of help are rejected because ... .  Well, I guess because I act like engineers act -- I am direct to the point of being blunt."
    author: "James Richard Tyrer"
  - subject: "Re: Oskar?.... Another half-baked multimedia app"
    date: 2006-08-23
    body: "I'm also going to be blunt. Look, you can present your problem descriptions again and again. Yet, they won't solve the problem. Worse, you further alienate yourself and the developers that you criticize.\n\nHuman relation is not an exact science. You may come to me and give a very genius feedback, but if I think you're a jerk, I may refuse to listen to you, ignore you and attack you back. You can argue until the end of the world that you're right and you have lots of experience even with punch cards, and I should judge the comment based on the merit, and all this method should make my life easier, and positive feedback should not be taken as an insult, and it's my own loss not to hear you, and I have to get over my ego, and I have to learn how to do things like engineers, and so on. But the fact is, every time you say so, I will be less motivated to listen to you. You can only use a stick to frighten kids, not me.\n\nWhy? It's simple. You place yourself as if you were not in the same boat. People do no listen to strangers (Social Skills 101). A beggar is not likely to take advices from a millionaire (not that I ask you to be a beggar or treat the beggar as a prima donna). It does not matter if the millionaire is 100% right and the beggar is 100% foolish, it just won't happen.\n\nPeople have (fortunately) different tastes, thoughts and opinions. What you think as an efficient method might be torturing for me and thus makes me reject it. You can argue again (and post the arguments here) how this method beautifully solve the design flaw, but by doing so I may become more and more suspicious. Do you really want to help? Or do you just want to show off your skills? Even salesmen know when to stop bragging.\n\nPersonally if I help someone but he does not seem to accept it (or even when he curses me back), I just ignore it and move on. Personally I don't bother too much to show him how my advices can improve his life, unless of course I just really want to impress him and others. Personally I don't have time to analyze his mental behavior in depth, as I'm sure I still can be of some help for other people. In any day, I won't aim for 1000 db SNR.\n\nYou can't please every single soul in this planet. Don't even think about it. Just take care of those who are pleased.\n"
    author: "ariya"
  - subject: "Re: Oskar?.... Another half-baked multimedia app"
    date: 2006-08-24
    body: "You aren't being blunt.  You are talking past me.\n\nBR is so intransigent that he won't take responsibility for the quality of his code.  And, he won't try my suggestion of using TQM even if it would allow him to write better quality code with less effort.  Why?  The reason is ego.\n\nYou can't argue with people such as that -- people that know that what they think is right all the time.  And don't say that applies to me because I am only repeating what people more knowledgable than I am have said (about TQM and software engineering).\n\nYou do not alienate such people by confronting them.  They are already alienated.  That is, you can't have a working relationship with them -- at least not a real one.  Sometimes confronting them works and they will have a breakthrough (an epiphany).  Otherwise you can only treat them like a prima dona and have a one sided and phony relationship.\n\nI am not in the same boat because some of the KDE developers don't welcome people into the boat.  There is a difference of cultures.  I am a professional engineer and they are self taught hackers.  The self taught hackers are much more proficient at C++ coding than I am and they know how to write GUIs.  However, what some don't seem to understand is that underneath the GUI is a program and a program is a program no matter what language it is written in and what it does -- programs are all based on logic, arithmetic, and string manipulation.\n\nI have many years programing and design experience.  The fact that it is in a different language for other types of programs makes little difference.  I thought that I could contribute some of this experience to the KDE project, but I realize now that I am wrong.  There are some nice people in the KDE project that have appreciated my work.  But, I never know when I will be on the receiving end of an ego attack.  It is unnerving.  I have an ego too when people start personally insulting because they don't like my technical advice -- doesn't really make much sense does it.\n\nSo, this is why I am going to be moving on and will be doing most of my work on another Open project."
    author: "James Richard Tyrer"
  - subject: "Re: Oskar?.... Another half-baked multimedia app"
    date: 2006-08-16
    body: "I would not call Amarok half-baked at all. \\\n\nI would call Kaffeine under-baked, but it's quite usable. I am running it as *the* application on my HTPC at the moment because it's *more* usable than *every* other multimedia application (specifically DVB play/record, DVD and movie file playing) I've found."
    author: "Richard Jones"
  - subject: "Re: Oskar?.... Another half-baked multimedia app"
    date: 2006-08-16
    body: "Outside of lack of DVB support, Codeine really is more usable.\n\nI concur regarding Amarok. ;)"
    author: "Ian Monroe"
  - subject: "Re: Oskar?.... Another half-baked multimedia app"
    date: 2006-08-18
    body: "I don't think that calling apps half-baked helps to correctly states the issue.  There are apps in development that probably are only half done, but the problem is really that we have too many 80% to 90% baked apps.\n\nThe problem with the 90% baked apps is that they never seem to get the last 10% of the time in the oven.  Possibly this is because that last 10% takes considerably more than 10% of the work and that work isn't much fun."
    author: "James Richard Tyrer"
---
In <a href="http://commit-digest.org/issues/2006-08-13/">this week's KDE Commit-Digest</a>: kdesu, the KDE application privileges manager, gets long-awaited support for the <a href="http://en.wikipedia.org/wiki/Sudo">sudo</a> method. <a href="http://www.vandenoever.info/software/strigi/">Strigi</a> gets <a href="http://en.wikipedia.org/wiki/RPM_Package_Manager">.rpm</a> and <a href="http://en.wikipedia.org/wiki/Deb_(file_format)">.deb</a> package contents indexing capabilities, and can now index <a href="http://en.wikipedia.org/wiki/Utf-8">UTF-8</a> encoded text. <a href="http://www.simonzone.com/software/guidance/">Guidance</a> gets a new power manager applet. Code import for the <a href="http://code.google.com/soc/kde/appinfo.html?csaid=9694A349D1C04E0C">Physiks</a> educational <a href="http://developer.kde.org/summerofcode/soc2006.html">Summer Of Code</a> project. <a href="http://amarok.kde.org/">Amarok</a> gets support for <a href="http://en.wikipedia.org/wiki/Media_Transfer_Protocol">MTP media devices</a>. Work starts on porting KGoldRunner to KDE 4. Rewrites begin in the KReversi game and <a href="http://websvn.kde.org/trunk/playground/multimedia/oskar/">Oskar</a> media player. GUI optimisations in <a href="http://ktorrent.org/">KTorrent</a> and KTU (KDE Translation Updater). Experiments using <a href="http://www.kexi-project.org/">Kexi</a> as a database backend in <a href="http://kphotoalbum.org/">KPhotoAlbum</a>, and rendering <a href="http://en.wikipedia.org/wiki/Scalable_Vector_Graphics">SVG</a> in <a href="http://dot.kde.org/1152645965/">Unity</a>.

<!--break-->
