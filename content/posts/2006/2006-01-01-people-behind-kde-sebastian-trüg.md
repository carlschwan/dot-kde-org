---
title: "People Behind KDE: Sebastian Tr\u00fcg"
date:    2006-01-01
authors:
  - "jriddell"
slug:    people-behind-kde-sebastian-trüg
comments:
  - subject: "pic gallery"
    date: 2006-01-02
    body: "a major still missing feature is the ability to create vcds directly from pics, there are several solutions, but i always wanted an integrated one. so i started coding myself and now i know why noone has done this yet - it's really difficult. well, since a have spare time atm, i guess it will be done by jan 10 or so. only png and svg for now, but other formats can be added with a few lines, when it's done.\n"
    author: "Kugelfisch"
  - subject: "Re: pic gallery"
    date: 2006-01-02
    body: "I use digikam for this\n\nWorks fine :)\n\nI think creating vcd's from photo's is a job for photo management applications like kimdaba or digikam, not for burning applications like k3b.\n\n(b.t.w., the actual burning is done by k3b)"
    author: "rinse"
  - subject: "k3b interface themes"
    date: 2006-01-02
    body: "Thanks Sebastian for k3b. It is quite useful (esp. for the people who've used nero version 5.x)\n\nBut the nero version 5's interface is not user-friendly enough...\n\nnero's version 6/7's has 2 interfaces, those are \"Application\" and \"Wizard\" interfaces (click next->next->finish). And the Wizard interface/mode is quite helpful for newbies...\n\nIMHO, A Wizard like interface of k3b will definitely help the \"new to cd/dvd burning world\".\n\n\n\n"
    author: "fast_rizwaan"
  - subject: "graveman"
    date: 2006-01-02
    body: "The gnome's cd/dvd burning app has wizard like interface, but Adding files and folders require right-clicking on the project area, which is confusing.\n\nperhaps k3b could use graveman interface (as an optional wizard/theme).\n\nhttp://graveman.tuxfamily.org/index.php\n\nscreenshots:\nhttp://graveman.tuxfamily.org/screenshot.php?l=e"
    author: "fast_rizwaan"
  - subject: "Re: graveman"
    date: 2006-01-02
    body: "i don't get this, i tried nero some time ago on windows, and found it extremely confusing. i just wanted to burn an iso, but it took me ages to do it... k3b is extremely simple as it is: start it, you'll see buttons for what you want to do (and its telling you how to add buttons, too). click one, it tells you what to do... drop the files or look for them, click 'burn', you get an easy dialogue, and just click burn to get it burning. a wizard just makes things harder, imho - i'm sure you have to go through more steps to burn a cd with a wizard than with k3b (k3b: start it, click 'copy cd', click start. a wizard might be as fast as this, but not faster... so what's wrong?)"
    author: "superstoned"
  - subject: "Re: graveman"
    date: 2006-01-02
    body: "Some days ago a friend of mine, who's still a windoof addict (even after losing most of his data twice due to a virus scanner running amok), tried to burn a dvd. So I discovered a possible design flaw I've never thought of: In the burning dialog You can choose the drive, but how do you know which is which? I have three multi-format lg drives, two internal ide and one usb and they don't carry their type id on the front.\nIt would help a lot if K3b opens the selected drives door if there's not already a disk in it, so it's easy to spot the right one. I already sent this suggestion to Sebastian, but without any success or reply yet.\n"
    author: "Andy"
  - subject: "multiple drives"
    date: 2006-01-02
    body: "People usually don't have multiple CD/DVD drives. The most common situation is the one where someone has only one drive (everytime (s)he buys another one, it's installed in the place of the old one), or when the person has more than one drive, (s)he is usually savvy enough to know which is each one."
    author: "Humberto Massa"
  - subject: "Re: multiple drives"
    date: 2006-01-03
    body: "But there are people having more than one drive and for them the current (from svn at least, as I always use that one) version does not display the drive name as the old one, so if you see two \"Empty CD-R\" entries, it is confusing. Not talking about the fact when you want to erase a CD-RW and you have CD-RWs in both drives. I discovered the needed information in the tooltip only after several days, so at least I know now what I want, but I think the old way of showing the drive name in the selection combo box was better and more usable."
    author: "Andras Mantia"
  - subject: "Re: graveman"
    date: 2006-01-03
    body: ">i don't get this,\n\nI was referring to graveman's confusing interface to add a file/folder! k3b is way too good.\n\na wizard like interface would help newbies (novice computer users) IMHO."
    author: "fast_rizwaan"
  - subject: "Re: graveman"
    date: 2006-01-04
    body: "The best interface for noobs to burn CD that I've ever seen is no wizard or anything fancy, just the MacOS one. You copy files to the CD using the filemanager as if it was in your own disk, and when the user ejects the CD, you burn it with a nice progess dialog."
    author: "blacksheep"
  - subject: "Thanks!"
    date: 2006-01-02
    body: "K3b is an amazing application!"
    author: "Joergen Ramskov"
  - subject: "Re: Thanks!"
    date: 2006-01-02
    body: "Thanks Sebastian for such a great application! Your answers are pretty cool too."
    author: "Flavio"
  - subject: "Re: Thanks!"
    date: 2006-01-03
    body: "I think k3b is great but we STILL can't burn CDs with files over the network.  I was able to do this in '97 with Win95, why can't I do it in 2006?\n\nWill k3b ever get KIO support?"
    author: "Anonymoud"
  - subject: "Re: an't burn CDs with files over the network"
    date: 2006-01-05
    body: "> but we STILL can't burn CDs with files over the network\n\nReally?  I recall doing exactly that(*) on many occasions.\n\n(*) Burning files from a network/NFS-mount"
    author: "Rex Dieter"
  - subject: "Re: Thanks!"
    date: 2006-01-05
    body: "I think he means FISH:// like he said KIO ! Networkprotocols from the kernel are supported my the virtualisation of the kernel.\n\nthe problem is , k3b relays on cdrecord (?) and its not an kde app, so NO Kio."
    author: "chri"
  - subject: "Re: Thanks!"
    date: 2006-01-06
    body: "Well, they could work around that by feeding cdrecord with a temporarly saved file pushed by KIO.\nKonqueror does that with network files for alien applications like Gimp."
    author: "blacksheep"
---
Welcoming in the new year is <a href="http://people.kde.nl">People Behind KDE</a> bringing us one of the little known stars of KDE development.  <a href="http://people.kde.nl/trueg.html">Sebastian Trüg</a> is the man behind one of KDE's most successful applications, <a href="http://www.k3b.org">K3b</a>.  Read the interview to find out how K3b started, what KDE needs to conquer the world and what keeps Sebastian motivated to work on the premier CD burning application.

<!--break-->
