---
title: "People Behind KDE: John Tapsell"
date:    2006-08-16
authors:
  - "talbers"
slug:    people-behind-kde-john-tapsell
comments:
  - subject: "hehe"
    date: 2006-08-16
    body: "Actually I'm just the current maintainer - other people have done most of the work. :)"
    author: "John Tapsell"
  - subject: "Can't wait..."
    date: 2006-08-16
    body: "Whoa, that KDE 4 splash looks awesome! Can't wait for KDE4..."
    author: "Bram Schoenmakers"
  - subject: "Re: Can't wait..."
    date: 2006-08-16
    body: "You have got to be kidding.\n\nThe splash screen looks awfull. Kindergarden stuff.  I actually think it was meant as a joke. Well, worked for me. Heh heh...there, I laughed. :P"
    author: "Alkis"
  - subject: "Re: Can't wait..."
    date: 2006-08-17
    body: "I second this"
    author: "yeap"
  - subject: "Re: Can't wait..."
    date: 2006-08-17
    body: "Somebody on the IRC channel said they wanted the kde4 splash screen to be like the kde3 one, but more colourful.  So I did just that :-)"
    author: "John Tapsell"
  - subject: "Re: Can't wait..."
    date: 2006-08-17
    body: "I think its great and that we should keep it :)"
    author: "Rinse"
  - subject: "flux"
    date: 2006-08-16
    body: "Best. Interview. Ever."
    author: "Ian Monroe"
  - subject: "Re: flux"
    date: 2006-08-16
    body: "Second that."
    author: "Jocke \"Firetech\" Andersson"
  - subject: "My gosh..."
    date: 2006-08-16
    body: "... I think I never laughed so much while reading a \"People behind KDE\". Especially the \"What is your most embarrassing KDE moment?\" section is brilliant.\n\nNice to see that even KDE developers are just humans, too :D"
    author: "liquidat"
  - subject: "Re: My gosh..."
    date: 2006-08-17
    body: "yep\n\nSooooo funny!\n\nThanks for making me laugh and for that KSysGuard patch!!!\n\nCU Dom"
    author: "Dominik Seichter"
  - subject: "Travel"
    date: 2006-08-16
    body: "\"What is your favourite place in the world?\nI don't know. I've seen so little. I want to travel and see more of it :)\"\n\nGo to Japan. Then stay there. It kicks Europe's *ss.\n\nYou know, they let you stay if you marry a Japanese person, and have a job. One down, one to go. ;)\n"
    author: "Apollo Creed"
  - subject: "Re: Travel"
    date: 2006-08-16
    body: "I'm not sure I would survive the weather :-)  From what I hear, it's more severe than the UK - hotter summers, colder winters and wetter rainy seasons.  I'd be a wet, burnt, frozen baka-gaijin."
    author: "John Tapsell"
  - subject: "Re: Travel"
    date: 2006-08-16
    body: "Hotter summers is probably true, wetter rainy seasons too - colder winters... well, that's what *they* think.  (Did I mention I'm from Sweden? ;))\n\n\n\n\n"
    author: "Apollo Creed"
  - subject: "Re: Travel"
    date: 2006-08-20
    body: "I read (in an atlas) that Japan is the most dangerous place in the world (natural disater-wise). They have earthquakes, hurricans, tunamis, and other nasty things.\n\nHaha, (im from California so maybe I shouldn't laugh so much)\n"
    author: "Ben"
  - subject: "Hand-held Wikipedia reader"
    date: 2006-08-16
    body: "Q: You're stuck on a train for 6 hours and are bored out of your skull. What do you do to amuse yourself?\n\nA: Try to invent something. A hand-held Wikipedia reader. [...]\n\nThat one must definitely carry the words \"Don't Panic\" on it. :)"
    author: "Arend jr."
  - subject: "Re: Hand-held Wikipedia reader"
    date: 2006-08-16
    body: "Have a look yourself: http://bedic.sourceforge.net/  ;)\n"
    author: "Apollo Creed"
  - subject: "Highlights"
    date: 2006-08-17
    body: "Highlights:\n\nQ:\nWhat makes you develop for KDE instead of the competition?\nA:\nWe have competition?\n\nThat is all..."
    author: "Danil"
  - subject: "Nice Interview"
    date: 2006-08-17
    body: "It's really nice to see the more personal side of the inhuman machines that have created KDE. I look forward to the next one."
    author: "ZennouRyuu"
  - subject: "Re: Nice Interview"
    date: 2006-08-17
    body: "Well, there have been a lot of of personal, funny and interesting interviews before in the series, but this sure was one of <I>the</I> funniest :-)"
    author: "G\u00f6ran Jartin"
  - subject: "KDE 4"
    date: 2006-08-18
    body: "However we are curious about KDE 4..."
    author: "Itchy "
---
Curious about the man responsible for the application behind CTRL-Esc?  This postgrad student will make sure his application in KDE4 will 'flash up in a ball of fire and lightning, flames of fire torching random apps on the screen. It will settle down into a mind blowing beautiful interface, with only flickers of flames around the edges.' Want to see a Sneak Preview of KDE4? Or read all about FIFO and LIFO, Saki err Sake, heart attacks and the man responsible for breaking Konversation for five months? People Behind KDE presents <a href="http://people.kde.nl/john.html">John Tapsell</a>.




<!--break-->
