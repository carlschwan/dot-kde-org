---
title: "KDE Commit-Digest for 15th October 2006"
date:    2006-10-16
authors:
  - "dallen"
slug:    kde-commit-digest-15th-october-2006
comments:
  - subject: "Broken link"
    date: 2006-10-16
    body: "The link to Karbon is broken. It should be changed to http://koffice.kde.org/karbon14/"
    author: "Patrick Starr"
  - subject: "Re: Broken link"
    date: 2006-10-16
    body: "Link changed to http://koffice.kde.org/karbon/\n\nThanks,\nDanny"
    author: "Danny Allen"
  - subject: "Re: Broken link"
    date: 2006-10-16
    body: "Damn. Posted the wrong link. ;)"
    author: "Patrick Starr"
  - subject: "Developer statistics"
    date: 2006-10-16
    body: "Hi Danny,\n  I like the new statistics on the commit digest :-)   How are you getting the information about the age etc of the developers?  I don't mind putting my information on that somewhere, but I don't recall anywhere having that info.\n\n  Seems better to just let us tell you than you trying to guess from random google searches or however you are doing it.\n\nJohn"
    author: "JohnFlux"
  - subject: "Re: Developer statistics"
    date: 2006-10-16
    body: "http://worldwide.kde.org is my guess"
    author: "Greg M"
  - subject: "Re: Developer statistics"
    date: 2006-10-16
    body: "To collect the information, I have so far relied on worldwide.kde.org data as a base, which I have been extending with the extra information I would like by asking personally asking people, or using google, etc.\n\nOf course, this is a painfully slow method - your message has prompted me to implement my automated idea, so...\n\nIf all KDE contributors (with SVN accounts) who want to be part of this statistics project can follow the instructions at http://commit-digest.org/data/ I would really appreciate it.\n\nThanks,\nDanny"
    author: "Danny Allen"
  - subject: "Re: Developer statistics"
    date: 2006-10-17
    body: "actually it would great if kde.org had a page with a list of all the contributors with some personal details and a link to their resume (the contributor could select what data to make public) and  with there personal svn stats if this is feasible . That would be a great way to find a job :)"
    author: "Patcito"
  - subject: "GraphicsMagick?"
    date: 2006-10-16
    body: "Why the switch from ImageMagick?\nHaving looked at the websites, it seems GraphicsMagick development is stagnating and outdated...\nWas it a political decision?\n\nBTW: there's a missing [/a] in the article just after \"Amarok\" which makes posting from Opera impossible."
    author: "AC"
  - subject: "Re: GraphicsMagick?"
    date: 2006-10-16
    body: "GraphicsMagick has a much more stable API and ABI -- trying to keep up with the ImageMagick du jour became tiresome a long time ago. GraphicsMagick is also used in only two places: KWord's latex filter and Krita's magick import/export filter -- the filter that handles all the file formats we haven't written code for ourselves. Nowdays it handles xcf, psd, bmp and gif for us, not much more."
    author: "Boudewijn Rempt"
  - subject: "Re: GraphicsMagick?"
    date: 2006-10-17
    body: "Oh i see now. Thanks for the reply!"
    author: "AC"
  - subject: "Re: GraphicsMagick?"
    date: 2006-10-17
    body: "Fixed, thanks."
    author: "Navindra Umanee"
---
In <a href="http://commit-digest.org/issues/2006-10-15/">this week's KDE Commit-Digest</a>: The KDE project <a href="http://dot.kde.org/1160834616/">celebrates its 10th anniversary</a>. System tray items can now be reordered by the user. Support for action sounds in <a href="http://okular.org/">okular</a>. Work begins on Dynamic Brush architecture and canvas improvements in <a href="http://www.koffice.org/krita/">Krita</a>, with layer handling improvements in <a href="http://koffice.kde.org/karbon/">Karbon</a>. Krita switches library dependencies from <a href="http://www.imagemagick.org/">ImageMagick</a> to <a href="http://www.graphicsmagick.org/">GraphicsMagick</a>. Memory usage optimisations in the <a href="http://www.khtml.info/">KHTML</a> web rendering engine and <a href="http://amarok.kde.org/">Amarok</a>.



<!--break-->
