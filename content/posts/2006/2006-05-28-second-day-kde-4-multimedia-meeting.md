---
title: "Second Day KDE 4 Multimedia Meeting"
date:    2006-05-28
authors:
  - "j(superstoned)"
slug:    second-day-kde-4-multimedia-meeting
comments:
  - subject: "Reviewing the custom icons issue"
    date: 2006-05-28
    body: "I would be interested in what Florian's opinion is, in regard of enabling the amaroK custom icons by default.\n\nFor those amongs you readers who would like to make clear that you prefer standard KDE icons for amaroK by default, remember that you can vote for the appropriate bug at http://bugs.kde.org/show_bug.cgi?id=125295"
    author: "Jakob Petsovits"
  - subject: "Yay, Flamewar"
    date: 2006-05-28
    body: "Why'd you have to start a flamewar as frist psot?"
    author: "Max Howell"
  - subject: "Re: Reviewing the custom icons issue"
    date: 2006-05-28
    body: "is that really so important?\n\nwhat does it got to do w/ hackers (not artists)?"
    author: "Nick"
  - subject: "Re: Reviewing the custom icons issue"
    date: 2006-05-28
    body: "It's important because apps should speak the same language."
    author: "yep"
  - subject: "Re: Reviewing the custom icons issue"
    date: 2006-05-28
    body: "It's important because much beer must be spent arguing it."
    author: "Christie"
  - subject: "Re: Reviewing the custom icons issue"
    date: 2006-05-28
    body: "Well, all apps speak english, so what is your problem ;)\n"
    author: "AC"
  - subject: "Re: Reviewing the custom icons issue"
    date: 2006-05-29
    body: "I have used the custom icon set of Amarok for several months now:\nYes, they look very cool\nNo, they don't reduce usability.\n\nThink about it:\nThe developers are the ones doing the decisions. If they like their own icons more, then they should enable them. If some non contributing users think otherwise they can always switch back to the KDE icons.\n\nSo please stop whining about icons. There are much more important things."
    author: "Min"
  - subject: "Re: Reviewing the custom icons issue"
    date: 2006-05-29
    body: "> So please stop whining about icons. There are much more important things.\n\nI know there are. But I trust the amaroK developers to take care of all those more important things, as they continue making amaroK the best music player out there. I don't trust them in respect with the icon thingie, so I try to take care of that by myself. I believe it's important enough to spend a little time on it."
    author: "Jakob Petsovits"
  - subject: "Re: Reviewing the custom icons issue"
    date: 2006-05-29
    body: "> I would be interested in what Florian's opinion is, in regard of enabling\n> the amaroK custom icons by default.\n\nIn Amarok 1.4.0 they are unfortunately not enabled by default which makes amarok look worse than it could. Hope that the icon set will be completed (and enabled by default) for Amarok 1.4.1.\n\nTo the self claimed usability expert: if you prefere standard KDE icons you can always switch back. It's so simple.\n"
    author: "martin"
  - subject: "Re: Reviewing the custom icons issue"
    date: 2006-05-29
    body: "I don't claim being a usability expert.\nI'd rather like to hear what the real ones have to say about this issue.\nI thought if the amaroK devs have come together with an \"external\" usability guy (Florian) for 2 days they may have exchanged their thoughts here. Maybe they haven't.\n\nAll I wanted is a clear answer, like:\n- We haven't touched this issue. or maybe:\n- We talked about it, and the outcome was ...\nWhatever. But of course it's easier to accuse someone of trolling instead of trying to find common ground."
    author: "Jakob Petsovits"
  - subject: "Re: Reviewing the custom icons issue"
    date: 2006-05-29
    body: "He is just asking whether Florian has an opinion about the issue. IMHO this is a valid question and it could easily be answered.\n\nI am irritated though that several replies are somewhat hostile ... "
    author: "ML"
  - subject: "Re: Reviewing the custom icons issue"
    date: 2006-05-29
    body: "> I am irritated though that several replies are somewhat hostile ... \n\nThat's maybe because every time amaroK is part of a news story, someone comes here and complains about the custom icons."
    author: "cl"
  - subject: "exactly (NT)"
    date: 2006-05-30
    body: "> That's maybe because every time amaroK is part of a news story, someone comes \n> here and complains about the custom icons.\n\nExactly!\n"
    author: "martin"
  - subject: "Re: Reviewing the custom icons issue"
    date: 2006-05-29
    body: "Good news, then. The amaroK people might have found an solution for the problem, and newer versions will possibly have the default icons, and added amaroK icons to fill in the the missing ones.\n\nRemember, there where good, technical reasons for the custom icon theme: it was hard to complete the KDE icontheme, and they wanted amaroK to be icon-complete ;-)\n"
    author: "superstoned"
  - subject: "Funny Quotes Thread"
    date: 2006-05-28
    body: "http://vizzzion.org/?id=viewpic&gcat=KDE4MultimediaMeeting&gpic=IMG_7727.JPG#images\n\nmuesli: Sebr, will you marry me?\nsebr: Sorry muesli, my friend, I can love only Leinir."
    author: "Max Howell"
  - subject: "Re: Funny Quotes Thread"
    date: 2006-05-28
    body: "ade: Hey Allan, you added seeking to KIO, right? So you can just fseek() your way to the hotel.\nallan: No, it's still buggy, I will deadlock and stay right here.\nade: No problem, we can put you on the D-BUS.\n\n[[ Note: since I was driving, there was no beer involved on my side. ]]"
    author: "Adriaan de Groot"
  - subject: "Re: Funny Quotes Thread"
    date: 2006-05-28
    body: "LOL, the most funny part is that he really tried to show off seeking in KIO, but had to admit it would deadlock immediately :D"
    author: "superstoned"
  - subject: "amaroK performance patches"
    date: 2006-05-28
    body: "Are these patches amaroK 1.4.1 or 2.0 material?\n\nSteBo"
    author: "SteBo"
  - subject: "Re: amaroK performance patches"
    date: 2006-05-28
    body: "1.4.1"
    author: "Ian Monroe"
  - subject: "Re: amaroK performance patches"
    date: 2006-05-28
    body: "1.4.1"
    author: "Mark Kretschmann"
  - subject: "Re: amaroK performance patches"
    date: 2006-05-28
    body: "1.4.1\n\nNow we said it three times does that make it a promise?"
    author: "Max Howell"
  - subject: "Re: amaroK performance patches"
    date: 2006-05-28
    body: "\"Just the place for a Snark!\" the Bellman cried,\n As he landed his crew with care;\n Supporting each man on the top of the tide\n By a finger entwined in his hair. \n\"Just the place for a Snark! I have said it twice:\n That alone should encourage the crew.\n Just the place for a Snark! I have said it thrice:\n What i tell you three times is true.\""
    author: "Adriaan de Groot"
  - subject: "I thought that sounded like Lewis Carroll."
    date: 2006-05-29
    body: "I just read Alice's Adventures in Wonderland and Through the Looking Glass on the backtrip, but didn't encounter that bit. According to Google, it's from The Hunting of the Snark."
    author: "illissius"
  - subject: "Re: amaroK performance patches"
    date: 2006-05-28
    body: "1.4.1"
    author: "Seb Ruiz"
  - subject: "Re: amaroK performance patches"
    date: 2006-05-28
    body: "1.4.1"
    author: "Anonymous amaroK User"
  - subject: "Re: amaroK performance patches"
    date: 2006-05-28
    body: "1.4.1, but given the amount of work that has been put into amaroK, one might as well consider bumping version to 1.5 without a 1.4.1.\n\nAnd thanks [ade] for outweirding everyone again."
    author: "sebas"
  - subject: "Re: amaroK performance patches"
    date: 2006-05-29
    body: "1.4.1"
    author: "KOffice fan"
  - subject: "Re: amaroK performance patches"
    date: 2006-05-29
    body: "1.4.1"
    author: "illissius"
  - subject: "superstoned?"
    date: 2006-05-28
    body: "what was jos poortvliet part in all this? when he says We it makes it sounds as if he was  hacking too. Is Jos a kde hacker too? "
    author: "Patcito"
  - subject: "Re: superstoned?"
    date: 2006-05-28
    body: "Jos is our \"press officer\"; he's not particularly a Multimedia hacker, no. However, he was there as part of the organization."
    author: "Adriaan de Groot"
  - subject: "Re: superstoned?"
    date: 2006-05-28
    body: "nope, not really my game... i was just bothering them asking what they where doing ;-)"
    author: "superstoned"
  - subject: "KIO file seeking"
    date: 2006-05-28
    body: "does the KIO file seeking mean that we can finally strean mp3 over smb:// using amaroK. If so Yeah!!!. Will it work for fish:// too?\n\nCheers,\nBen"
    author: "Ben"
  - subject: "Re: KIO file seeking"
    date: 2006-05-28
    body: "Does it mean I can navigate through LUG radio podcasts?\n\n"
    author: "lug"
  - subject: "Re: KIO file seeking"
    date: 2006-05-28
    body: "well, yes that's what it means. but you'll have to wait for KDE 4..."
    author: "superstoned"
  - subject: "Re: KIO file seeking"
    date: 2006-05-29
    body: "Well, it's time to start checking out the latest and greatest from the KDE SVN tree then...\n\nCheers,\nBen"
    author: "BenAtPlay"
  - subject: "Re: KIO file seeking"
    date: 2006-05-29
    body: "don't bother. as i wrote, this is experimental - and only in JUNE they will discuss if it should be merged. it lives in a seperate tree, for now."
    author: "superstoned"
  - subject: "Re: KIO file seeking"
    date: 2006-06-02
    body: "It surely would be an awesome thing to have."
    author: "Mark Hannessen"
  - subject: "Kaffeine"
    date: 2006-05-28
    body: "Why isn't anything said about Kaffeine? Isn't that part of KDE?\nIt could use a little polish from the amarok artists :) (no not the icon part).\nAnd perhaps a script to install libdvdcss like amarok can do for mp3 support (with a notification for it being quite illegal in the US, but legal in 95% of the other countries on this planet)"
    author: "Terracotta"
  - subject: "Re: Kaffeine"
    date: 2006-05-28
    body: ">>Why isn't anything said about Kaffeine? Isn't that part of KDE?\n\nNope"
    author: "AC"
  - subject: "Re: Kaffeine"
    date: 2006-05-28
    body: "well, ask them ;-)\n\nanyway, there was simply no amarok developer who wanted and had time to show up."
    author: "superstoned"
  - subject: "Re: Kaffeine"
    date: 2006-05-29
    body: "You mean Kaffeine developer :o)"
    author: "AC"
  - subject: "Re: Kaffeine"
    date: 2006-05-29
    body: "ehm... yes... amarok dev's where more than willing to show up :D"
    author: "superstoned"
  - subject: "Re: Kaffeine"
    date: 2006-05-28
    body: "No one from Kaffeine was there, so nothing can be said of it."
    author: "Max Howell"
  - subject: "Re: Kaffeine"
    date: 2006-05-28
    body: "NO, Please, NO! Let the amarok people continue building their own Frankenstein. I would rather ask Totem artists to look at Kaffeine. All I need in Kaffeine is another set of 300 buttons and tabs."
    author: "Daniel \"Suslik\" D."
  - subject: "Re: Kaffeine"
    date: 2006-05-29
    body: "Remember also that Max Howell, a well known amaroK hacker, also wrote Codeine (http://www.kde-apps.org/content/show.php?content=17161), a xine frontend which is probably at just about the opposite end of the spectrum from what you describe."
    author: "Dhraakellian"
  - subject: "Re: Kaffeine"
    date: 2006-05-29
    body: "Good news, there is already a Totem-like player for KDE:\nhttp://dot.kde.org/1102685664/\nhttp://ronald.bitfreak.net/kiss.php"
    author: "curious"
  - subject: "Re: Kaffeine"
    date: 2006-05-29
    body: "AFAIK Ronald stopped working on GStreamer, so he probably also stopped developing Kiss (wich was a proof of concept anyway)"
    author: "AC"
  - subject: "Re: Kaffeine"
    date: 2006-05-29
    body: "Sorry to hear.  Can you give more details on why Ronald quit GStreamer?  Do you have a link?"
    author: "curious"
  - subject: "Re: Kaffeine"
    date: 2006-05-29
    body: "because he's in gradschool studying medicine.\nthey work you quite hard there apparently"
    author: "bob"
  - subject: "Re: Kaffeine"
    date: 2006-06-02
    body: "\"All I need in Kaffeine is another set of 300 buttons and tabs\"\n\nFunny how you say \"another\".  It already received a ton of useless tabs and buttons and toolbar configurability with Kaffeine 0.5.  The 0.4.3 layout was quite elegant IMHO and very usable.  I still use that version, recompiling once in a while to keep it running with newer libs.  "
    author: "MamiyaOtaru"
  - subject: "Re: Kaffeine"
    date: 2006-06-29
    body: "Hey ... I don't see what you are getting at. Kaffeine current (0.8.x) is very useable, has a very clean GUI and even receives (and can save) DVB-T streams from any supported device.\n\nSee http://www.kde-apps.org/content/show.php?content=17161 ... I love it. The only thing that doesn't work too well ATM is jumping in MPEG files - there, MPlayer is a little better (doesn't get stuck).\n\nJens"
    author: "Jens"
  - subject: "Amarok new lay-out from SVN is terrible"
    date: 2006-05-29
    body: "I'm using recent SVN check-out, and noticed the new layout in the Amarok browser. Now the information about the playing track (wikipedia artist information, related artists, lyrics,...) are put in a seperate pane above the playlist. Personally I think it's extremely ugly and unpractical.\n\nThe contents of the pane (certainly the lyrics and music tab) are clearly more destined to be shown in \"portrait\" mode than in the new \"landscape mode\". With the new lay-out, a lot of horizontal space is wasted, while there's not enough vertical space. Now I always have to scroll to see most of the information in the music tab, while otherwise all could fit in the side pane and was always visible. Because of the extra space this pane takes above the playlist, there's much less space remaining for the playlist itself. And when I'm not interested in the information in the pane , it's more work to hide (and later make appear again) the pane. While in the previous lay-out, a click on the tab sufficed, I now have to use my mouse to drag & drop the splitter completely to the top of the window.\n\nPlease reconsider this lay-out!"
    author: "Frederik"
  - subject: "Re: Amarok new lay-out from SVN is terrible"
    date: 2006-05-29
    body: "please reconsider looking at it and judging it, once it is finished.\n\n..."
    author: "muesli"
  - subject: "Re: Amarok new lay-out from SVN is terrible"
    date: 2006-05-30
    body: "Give us a chance to polish it and you'll prolly be happy. Eg, the context-pane/browser/thing will be redesigned to fit the new width and not look ugly.\n\nAnd I will be spending time on the aesthetics like usual. Should be as pretty as ever, prolly much more so by next release.\n\nWe at amaroK thank you for your faith!"
    author: "Max Howell"
  - subject: "Re: Amarok new lay-out from SVN is terrible"
    date: 2006-06-19
    body: "I agree..i prefer old layout...however a button to choose layout could resolve the problem..\n\nJohn"
    author: "John Slave"
  - subject: "Re: Amarok new lay-out from SVN is terrible"
    date: 2006-06-28
    body: "\n  I prefer the old layout. There should be the possibility to change between both. Some people may be prefered the newest way, I really like the older one. It was great.\n\n   So, if want to change the layout, ok, but with the old layout option. Switch between them easily.\n\n  Cesar"
    author: "C\u00e9sar Delgado"
  - subject: "Re: Amarok new lay-out from SVN is terrible"
    date: 2006-06-22
    body: "At least it's better than having to twist my neck 90 degrees every time I want to read the text on the panes!"
    author: "Darkelve"
  - subject: "Sonic Visualiser: Interesting project"
    date: 2006-06-02
    body: "Hi,\n\nI was just browsing the web and found this project: http://sv1.sourceforge.net\nIt is  \"an application for viewing and analysing the contents of music audio files\". I just thought some people might be interested in it, as it incorporates a nice plug-in system to extract descriptive information from audio data.\n\nBy the way, its build upon Qt 4 and is GPL ;-)"
    author: "Camel"
---
Though it was still rainy here at the Annahoeve in the Netherlands, the <a href="http://dot.kde.org/1148004529/">KDE 4 multimedia meeting</a> was definitely up to speed. This article will report on the progress the hackers made yesterday, including the "why" and "what" of redesigning and speeding up amaroK, work on the KIO slaves and <a href="http://phonon.kde.org">Phonon</a>.




<!--break-->
<div style="float: right; border: thin solid grey; padding: 1ex; margin: 2ex"><a href="http://static.kdenews.org/danimo/rejoicing_peacocks.jpg"><img src="http://static.kdenews.org/danimo/rejoicing_peacocks_small.jpg" width="200" height="267" border="0" /></a><br /><a href="http://static.kdenews.org/danimo/rejoicing_peacocks.jpg">Happy peacocks:<br /> A few hours without rain</a></div>

<p>We had a quick start in the morning, several people where already busy hacking at 9. The morning was filled with hacking sessions and small get-togethers, and a few hours after lunch, we held a round table with everybody, asking what they had done so far.</p>

<p>Christian Mülhäuser and Seb Ruiz have been working on optimization of <a href="http://amarok.kde.org">amaroK</a> startup time. They gave the context browser a speedup of aproximately 50% and the playlist browser now loads 40-60% faster. Meanwhile, Bart Cerneels was working on the Podcasts. They sometimes have very long names, with a lot of redundancy. He tried to build some code to shorten them, without having to throw away information. And Martin Aumüller made it possible to drag'n'drop files from any KDE application into amaroK. amaroK then adds them to the database, by putting them in a sensible location based on the tags in the file.</p>

<p>Matthias Kretz redesigned large parts of Phonon completely today, after a talk with Thomas Zander (of KOffice fame). He decided to start experimenting with a few internals, which had to lead to a more flexible system. The currently almost stable backend API won't be affected, tough. Talks with the amaroK hackers also prompted him to enhance the communication between applications and Phonon. Christian Esken, who came visit today, has been working with us on Kmix. In cooperation with <a href="http://www.openusability.org">usability expert</a> Florian Grässle and Matthias, they decided on the implementation of several features. <a href="http://solid.kde.org">Solid</a> integration went in Kmix, so hotplugging will work in the future version of KMix. Florian also teamed up with Mark Kretschmann to enhance the usability of the script control in amaroK, while our other usability expert, Dan Leinir Jensen Turthra, worked with several other amaroK developers to create the next-generation amaroK interface.</p>
<div style="float: left; border: thin solid grey; padding: 1ex; margin: 2ex"><a href="http://static.kdenews.org/danimo/matthias_hacking.jpg"><img src="http://static.kdenews.org/danimo/matthias_hacking_small.jpg" width="300" height="255" border="0" /></a><br /><a href="http://static.kdenews.org/danimo/matthias_hacking.jpg">Matthias improving Phonon</a></div>


<p>Allan Sandfield Jensen has been working on KIO file seeking (as part of his <a href="http://developer.kde.org/summerofcode/soc2006.html">Google Summer of Code</a> project), and his announcement of a more-or-less working implementation was recieved by a big applause from the other attending hackers. His work will bring the network transparancy of KDE to a whole new level, allowing for easy playing of music over a network or directly editing of movies and pictures on a remote machine.</p>

<p>Ian Monroe and Max Howell have been working on better error messages for the users if there is no mp3 support in the distribution they use. The distribution has to provide a script for amaroK so it can offer the user to automatically install the nessecary codecs. Cooperation has already been promised by Jonathan Riddell from <a href="http://www.kubuntu.org">Kubuntu</a>.</p>

<p>A few hours after lunch, the amaroK developers spent considerable time whiteboarding and discussing the basic design of our favorite audio player. This time they went really in-depth, and there was a heavy discussion going on. Thomas has been working with the amaroK developers today on several design and usability issues, and was also involved in the whiteboarding of amaroK 2. An important goal was to design amaroK to become smaller, and thus faster and easier to maintain. By improving amaroK's design, they hope to make it easier to develop things like extensive plugin support. But design-wise this is hard to do and there are serious security-related issues. Aside from the basic design, things like quality control, usability and the &#8220;target user&#8221; for amaroK have been discussed.</p>

<div style="float: right; border: thin solid grey; padding: 1ex; margin: 2ex"><a href="http://static.kdenews.org/danimo/amarok_design.jpg"><img src="http://static.kdenews.org/danimo/amarok_design_small.jpg" width="300" height="255" border="0" /></a><br /><a href="http://static.kdenews.org/danimo/amarok_design.jpg">amaroK developers on the whiteboard</a></div>

<p>Meanwhile, the developers not involved with the discussion generally used earphones to avoid the discussion and continued to hack away, or talked in small groups. Gabor and Alexandre have been busy porting amaroK to Qt 4, and they got close to getting it to compile.</p>

<p>Slowly but steadily, the discussion dis-integrated, and more beer started to flow in. So, now the whiteboard had been filled enough, dinner was served. After the excellent diner everybody went upstairs again to continue their work or implement some of the stuff that has been discussed.</p>

<p>To wrap things up, this has been an extremely productive day, both in terms of code, design and face-to-face interaction. So, it is past midnight now, so time to get everybody to our hotel in Zundert - we have to be at breakfeast at 8:15...</p>

<p>Since we all love pictures. Sebastian Kügler has put up a <a href="http://vizzzion.org/?id=gallery&gcat=KDE4MultimediaMeeting">gallary of the meeting</a>. Thanks Sebas!</p>







