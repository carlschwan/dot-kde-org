---
title: "C++ GUI Programming with Qt 4 Published"
date:    2006-07-22
authors:
  - "hfox"
slug:    c-gui-programming-qt-4-published
comments:
  - subject: "Ordered and waiting for it to arrive"
    date: 2006-07-22
    body: "I've used the old edition of the book (the free PDF that is avialable for download) to learn Qt 3. I liked the book a lot so I have already ordered the new edition. Can't wait for it to arrive. I hope that authors added a bit more material for new users of Qt this time. I had a feeling with the old edition that while it started nicely with simple Hello Qt example it later jumped too fast to more complicated stuff. If they improved this then the new edition could be almost perfect. Oh and it would be nice to have some intro chapter dedicated to Qtopia."
    author: "Jure Repinc"
  - subject: "Re: Ordered and waiting for it to arrive"
    date: 2006-07-22
    body: "From the book's homepage:\n\nIncludes new chapters on Qt 4's model/view architecture and Qt's new plugin support, *along with a brief introduction to Qtopia embedded programming*"
    author: "Quique"
  - subject: "Re: Ordered and waiting for it to arrive"
    date: 2006-07-22
    body: "Very nice, I completely missed this when ordering the book. I only hope it is not too short."
    author: "Jure Repinc"
  - subject: "Re: Ordered and waiting for it to arrive"
    date: 2006-07-22
    body: "Just ordered it from amazon. I was also using the pdf of the Qt3 edition.\n\nGreat work, looking forward to upgrading to Qt4."
    author: "[Knuckles]"
  - subject: "Re: Ordered and waiting for it to arrive"
    date: 2006-07-22
    body: "It is already delivered here :-)  After browsing through the book it seemed to me that it is just an updated version of the previous book. That book was already good, so I guess this one will be even better. However, I don't think that there is much more introductionary material than in the previous version. (All right, I should have read it first before commenting on it.)"
    author: "HT de Beer"
  - subject: "Re: Ordered and waiting for it to arrive"
    date: 2006-07-23
    body: "Please let us know more when you have looked at it more thoroughly. I hate it when companies just sprinkle a little new material and call it a new edition. If there aren't any real improvements than I'll stick to the PDF, but it has really improved then I'd happily buy another copy."
    author: "Anymoose"
  - subject: "Re: Ordered and waiting for it to arrive"
    date: 2006-07-23
    body: "The differences between Qt3 and Qt4 are big enough to warrant a new edition. Besides, a dead tree version reads a lot easier than a pdf."
    author: "Johan Veenstra"
  - subject: "Re: Ordered and waiting for it to arrive"
    date: 2006-07-24
    body: "I have looked through the whole book now: the examples used are the same but they are updated; the text is often the same but sometimes it is rewritten somewhat; some new elements are added reflecting the new possibilities of Qt4, of course. In other words, it really is an updated version of the previous book, not a new book on Qt.\n\nWell, compare the table of contents, and see for yourself: all the chapters and sections which more or less have the same title as in the previous book do more or less have the same content."
    author: "HT de Beer"
  - subject: "Price"
    date: 2006-07-23
    body: "Price on phptr is high, better buy on amazon. I already did ;)"
    author: "cookiem"
  - subject: "Cool chapter, but what's with the tree example"
    date: 2006-07-23
    body: "Just when i though this is cool and easy to use, reading the tree example was a rather disappointment:\n<i>\"and with a Node * for the requested child. For hierarchical models, knowing\nthe row and column of an item relative to its parent is not enough to uniquely\nidentify it; we must also know who the parent is. To solve this, we can store a\npointer to the internal node in the QModelIndex. QModelIndex gives us the option\nof storing a void * or an int in addition to the row and column numbers.\"</i>\nIMO, model/view is not only a memory saver when having one's own data structures, or like in these examples, having dependent cells, but also a stability improvement for when eiter having own data structures or with dynamic content.\nSeperation of internal data and view is lost as soon as one uses QModelIndex to store a pointer. Having a path to parent would be better IMHO. And I don't see how one can easily 'fix' that, since allocating a path string and passing that pointer makes a memory leak (of course one could track all those strings, with some work for eg. seeing if a path is already created, that could actually work) and one can't overload QModelIndex AFAICS. \n"
    author: "koos"
  - subject: "Why not just use apidoc?"
    date: 2006-07-23
    body: "I never liked the books about programming libraries. Go read the bundled examples to get the basic idea how Qt works and afterwards just browse the apidoc when you need something. That is my approach and it usually works just fine. For example, yesterday I knew nothing about Qt's OpenGL widgets and just a few hints about OpenGL programming. Today I have a finished Qt OpenGL widget that visualises the object that I am controlling. It has textures, real-time display of what is going on and everything else I need. That is just a small part of the Control Systems project I am working on right now. Take a look at the screenshot http://i73.photobucket.com/albums/i213/brcha/nsu-opengl.png and you'll probably see that it looks all right (at least that's what I think). It is in Serbian, cyrillic, so you probably won't understand what the hell is written there, but that's not so important :)"
    author: "Filip Brcic"
  - subject: "Re: Why not just use apidoc?"
    date: 2006-07-24
    body: "I've only used the PDF a couple of times to help resolve actual programming questions (the API doesn't always provide a good overall view or define terms well), but I believe the book did give some useful insight in how Qt works.\n\nHave you read any of the Qt 3 book? Perhaps I'm a big dork, but I just found it exciting to read as well."
    author: "Ian Monroe"
  - subject: "Re: Why not just use apidoc?"
    date: 2006-07-24
    body: "Different people learn differently. The Qt documentation in assistant is mostly reference material, and is thus \"what\". The book is more expanatory and tutorial style, and is thus \"how\". Just because you don't want the book does not mean other people won't get any benefit from it."
    author: "Brandybuck"
  - subject: "Sharepoint for Linux?"
    date: 2006-07-23
    body: "Hi is someone working on something like Sharepoint for KDE ?\n\nIntegrating KOffice, Quanta, Digikam and all Apps with Webbased Parts !??\n\n"
    author: "chris"
  - subject: "Re: Sharepoint for Linux?"
    date: 2006-07-23
    body: "forgot to mention this is OFFTOPIC , but its recent"
    author: "chris"
  - subject: "Re: Sharepoint for Linux?"
    date: 2006-07-23
    body: "would be cool to have something like that, yes. it could integrate with KDE technology, like sharepoint does with ms office/explorer/etc."
    author: "superstoned"
  - subject: "C++ GUI Programming Qt 4"
    date: 2006-08-08
    body: "This book is not recommended for beginners. This book hides the true level of complexity of using the Qt 4 compiler and putting everything together when you quickly look through the book in the store. The Qt4 GUI C++ programming environment that is on the CD is not an integrated development programming environment and does not have an editor with linker and complier that is activated with push of a button, it is a command line complier (you need to know some DOS) and you have to use a text editor such as Note Pad with it. The book gave the impression that it was a \u0093RAD\u0094 type development type for GUI interface development that would automatically take care of the repetitive, non-trivial, and very difficult menial coding typical of purist C++ when using GUI objects. "
    author: "Digital Bob"
  - subject: "C++ GUI Programming Qt 4"
    date: 2006-08-08
    body: "This book is not recommended for beginners. This book hides the true level of complexity of using the Qt 4 compiler and putting everything together when you quickly look through the book in the store. The Qt4 GUI C++ programming environment that is on the CD is not an integrated development programming environment and does not have an editor with linker and complier that is activated with push of a button, it is a command line complier (you need to know some DOS) and you have to use a text editor such as Note Pad with it. The book gave the impression that it was a \u0093RAD\u0094 type development type for GUI interface development that would automatically take care of the repetitive, non-trivial, and very difficult menial coding typical of purist C++ when using GUI objects. "
    author: "Digital Bob"
  - subject: "Not For Beginners"
    date: 2006-08-08
    body: "This book is not recommended for beginners. This book hides the true level of complexity of using the Qt4 C++ GUI objects and putting everything together when you quickly look through the book in the store. The Qt4 GUI C++ programming environment that is on the CD is not an integrated development programming environment and does not have an editor with linker and complier that is activated with the push of a button, it is a command line complier (you need to know some DOS) and you have to use a text editor such as Note Pad with it. The book gave me the impression that it was a complete \u0093RAD\u0094 type development system for GUI interface development that would automatically take care of the repetitive, non-trivial, and very difficult menial coding typical of purist C++ when using GUI objects. As a first time user, I was very disappointed with the C++ Qt4 GUI development environment that came with the book. There are no useful instructions on using the command line complier in the book."
    author: "Digital Bob"
  - subject: "C++ GUI Programming with QT 4"
    date: 2006-08-25
    body: "Does any one has his book in pdf?"
    author: "Heather Fex"
  - subject: "Re: C++ GUI Programming with QT 4"
    date: 2007-12-19
    body: "i want book your c++ GUI Programing with QT4"
    author: "anhviet1010"
---
Prentice Hall has published a <a href="http://www.trolltech.com/company/newsroom/announcements/press.2006-07-16.6690497650/">new book, C++ GUI Programming with Qt 4</a>, authored by Jasmin Blanchette and Mark Summerfield of Trolltech. Billed as "<em>The Only Official Best-Practice Guide to Qt 4.1 Programming</em>", readers will discover the most effective Qt 4 programming patterns and techniques as one masters key technologies ranging from Qt's model/view architecture to Qt's powerful new 2D paint engine. The authors provide readers with unparalleled insight into Qt's event model and layout system. Then, using realistic examples, they introduce superior techniques for everything from basic GUI development to advanced database and XML integration.  You can read a sample chapter on <a href="http://www.prenhallprofessional.com/title/0131872494">the book's webpage</a>.





<!--break-->
