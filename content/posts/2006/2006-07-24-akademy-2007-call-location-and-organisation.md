---
title: "aKademy 2007 Call for Location and Organisation"
date:    2006-07-24
authors:
  - "Tink"
slug:    akademy-2007-call-location-and-organisation
comments:
  - subject: "Europe again?"
    date: 2006-07-24
    body: "For many contributors an aKademy 2007 in Canada would be very good, but most KDE devs live in Europe... Still, after four times Europe a non-european convention would be good I think..."
    author: "AS"
  - subject: "Re: Europe again?"
    date: 2006-07-24
    body: "As long as I get the trip paid, I can have it on any continent maybe except Antarctica.  I'd love Australia, actually. :-)"
    author: "Inge Wallin"
  - subject: "Re: Europe again?"
    date: 2006-07-24
    body: "Why wouldn't you want to visit the penguins? :-)"
    author: "Thiago Macieira"
  - subject: "Re: Europe again?"
    date: 2006-07-24
    body: "I have enough winter as it is, thank you. :-)"
    author: "Inge Wallin"
  - subject: "Re: Europe again?"
    date: 2006-07-24
    body: "I agree - I would have loved to go to dublin, but I was in ireland *last* year; I can't afford to go again for a while. I'm way over in BC... no matter where it is it'd probably be far away from me, but it'd be easier if it was at least on the same continent."
    author: "Chani"
  - subject: "Re: Europe again?"
    date: 2006-07-24
    body: "http://worldwide.kde.org/\n\nThis page makes it obvious why Akademy meetings traditionally happen in Europe.  Variety would be nice, but I think it's important to make the meeting convenient for the largest number of developers.  \n"
    author: "LMCBoy"
  - subject: "Re: Europe again?"
    date: 2006-07-24
    body: "How about we screw all the devels and hold it in Alaska? :)\n\nBut seriously, I vote for somewhere like .nl."
    author: "George Wright"
  - subject: "Re: Europe again?"
    date: 2006-07-24
    body: "well, we talked about that. not sure what the current status is, but we're trying to get some experience by doing some K4 meetings ;-)"
    author: "superstoned"
  - subject: "Re: Europe again?"
    date: 2006-07-25
    body: "I think most devels are up for Alaska if people want to pay for flights there, feel free to make a proposal.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: Europe again?"
    date: 2006-07-25
    body: "In fact it could well be Ludwigsburg all the time. There is no need to organise a 'world tour' for Akademy and no one discourages anyone from organising KDE events in Timbuktu, Canada etc. First the community has to be present, second travel should not be too expensive.\n\nAll that is needed is a close Airport with European cheap flight lines.\n\nRoma?\n\nBrussels (after Fosdem)?\n\nOsnabr\u00fcck?\n\n "
    author: "Itchy "
  - subject: "Re: Europe again?"
    date: 2006-07-25
    body: "Osnabr\u00fcck? That would be me organizing it :) For me, the netherlands would be perfect, especially Groningen as I know somebody there (free sleeping) :)"
    author: "Carsten Niehaus"
  - subject: "Only one place"
    date: 2006-07-25
    body: "Birmingham!!!"
    author: "bob"
  - subject: "Go where one did't go before..."
    date: 2006-07-25
    body: "Thanks, Tink, for working on this!\n\nTravelling costs are for sure an important factor for all unpaid contributors. So a place in Europe makes sense, still. But there could be Mini-Akademies in other continents as well, cmp. the mini akademy in spain, held after the real Akademy. There could be ones in South-/North-America and Asia. Well, Antarctica doesn't have any KDE developer?\n\nAkademy: Perhaps France, Poland, Hungary, or some other state with good infrastructure, where KDE was not before?"
    author: "Anonymous"
  - subject: "Sweden!"
    date: 2006-07-25
    body: "I would love it if akademy was held in sweden."
    author: "anonymous"
  - subject: "Re: Sweden!"
    date: 2006-07-25
    body: "But that would be too close.  Where's the fun in that?  :-)"
    author: "Inge Wallin"
  - subject: "Could be possible a little delay?"
    date: 2006-07-27
    body: "Could be possible to delay the deadline to 22nd September? I forgot about the context to propose aKademy in Italy :) . So if could be possible to delay I can try to prepare a draft to take part to this proposal. Other 3 weeks could change everything or nothing... who knows? :D . August is not a good month to write a draft and contact sponsor and people to organize it. I don't remember exactly, but was the deadline last year for September?\nLet me know :D ."
    author: "Giovanni Venturi"
  - subject: "Re: Could be possible a little delay?"
    date: 2006-07-29
    body: "We hope to discuss the results of the Call for Location & Organization at the KDE e.V meeting planned for September 25 in Dublin.\n\nJust send in whatever you can. I thought August would be a perfect month for the South Europeans as they have plenty of time during their vacation month :-)\n\nRoberto Cappuccio send in an excellent proposal last year for Bolzano. I've contacted him again and maybe you can work together with him on giving Bolzano another chance this year? A lot of work has already been done for that. Let me know if you need his email.\n\n--\nTink"
    author: "Tink"
  - subject: "Re: Could be possible a little delay?"
    date: 2006-07-31
    body: "Well if you can send it to me could be fine. In any case I hope KDE e.V. never accept Napoli (Neaples) as site for a future aKademy because it's not a real and possible candidate city for an aKademy. I really hope. Can be interesting to write a proposal, but, at the end, I think it's funny don't organize it. The organizer I don't think will have so funny as someone that simply take part in it :) . In any case in Dublin if I can help let me know :) I want to stay between you people.\nFor the location there is a lot of wonderful cities as Paris, Amsterdam, also some Scotland city. London, but not for sure Napoli in Italy. I live there and I can assure it would be not a good choice at all :) . Rome for example could be the fine choice, but also Pisa, Catania, Gaeta..."
    author: "Giovanni Venturi"
  - subject: "aKademy 2007 Call for Location"
    date: 2006-07-30
    body: "Belarus? It would be quite good:)"
    author: "SkySy"
  - subject: "What about Macedonia?"
    date: 2006-07-31
    body: "What do you think in Macedonia? It's pretty cheap for everything and we could use an event like aKademy to wake up a lot of people here from their long \u0093hibernation\u0094."
    author: "Zoran Dimovski"
  - subject: "Re: What about Macedonia?"
    date: 2006-08-01
    body: "Macedonia sounds exciting and interesting and so does Belarus.\n\nWhy don't you both write up a proposal?\n\nIf you need an a example of how a proposal would/should look like let me know and I'll be happy to send you one.\n\n--\nTink"
    author: "Tink"
  - subject: "No Belarus please"
    date: 2006-08-23
    body: "Make it some warm EU country! :)\n"
    author: "Linas"
---
While preparations for aKademy 2006 are in full swing, next year's annual meeting of the KDE community, aKademy 2007, is sending out a Call for Location and Organisation. aKademy is made possible mainly because of financial contributions by corporations. These corporations prepare their budget for 2007 in autumn of 2006, in order to be able to secure funding for aKademy 2007 we need to start organising aKademy 2007 early to be able to apply for sponsorship soon.







<!--break-->
<p>The event consists of the general assembly of the KDE e.V., a KDE developer conference and a multi-day hacking session.</p>

<p>If you are interested in hosting this large and exciting free software event, please consider to submit a proposal to the board of the KDE e.V. which will act as co-host.  Make sure you read the <a href="http://ev.kde.org/akademy/requirements.php">requirements for the aKademy location</a> which describes what we need from organisers.</p>

<p>If you want to apply as host for aKademy 2007 please submit a concrete proposal to kde-ev-board<span>@</span>kde.org. This proposal should include information about how the requirements for the location will be addressed, who will be the local organising team and a responsible person acting as contact to the KDE e.V. and head of the local organising team.</p>

<p>The deadline for submitting an aKademy 2007 proposal is August 31 2006.</p>

<p>If there are any questions please feel free to send them to Tink Bastian, she will answer any questions you might have about organising an aKademy at tink<span>@</span>kde.org or the board of the KDE e.V. at kde-ev-board<span>@</span>kde.org.</p>

<p>Previous aKademy events took place in <a href="http://events.kde.org/info/kastle/">Nove Hrady</a>, <a href="http://conference2004.kde.org/">Ludwigsburg</a> and <a href="http://conference2005.kde.org/">Malaga</a> and this year in <a href="http://conference2006.kde.org/">Dublin</a>. Where will we be next year?</p>

