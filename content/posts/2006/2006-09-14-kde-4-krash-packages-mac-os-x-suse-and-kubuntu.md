---
title: "KDE 4 Krash Packages on Mac OS X, SuSE and Kubuntu"
date:    2006-09-14
authors:
  - "jriddell"
slug:    kde-4-krash-packages-mac-os-x-suse-and-kubuntu
comments:
  - subject: "Gentoo?"
    date: 2006-09-14
    body: "Gentoo?\n\nProbably there are Fluid Ebuilds , but i want some easy to use ones.\n\nspecially crafted for Krash, so there are no compile probs"
    author: "chris"
  - subject: "Re: Gentoo?"
    date: 2006-09-16
    body: "\"but i want some easy to use ones\"\n\nYou want it easy? Then why are you using Gentoo? :-D"
    author: "blragh"
  - subject: "Re: Gentoo?"
    date: 2006-09-17
    body: "gentoo is easy , at least for me :-)\n\nits always a matter of what you have to do by hand in a distro and what is done automatically, and most stuff on gentoo is working out of the box so i think that is called an \"easy distro\""
    author: "c"
  - subject: "KOffice applications fail to start on Mac OS X"
    date: 2006-09-14
    body: "Can't start any KOffice app. Here is the error message:\n\ndyld: Library not loaded: /opt/kde4/lib/libkabc.4.dylib\n\nInstalled kdesupport, qt, kdelibs, kdebase and koffice. Is something missing ?\n"
    author: "Ga\u00ebl"
  - subject: "Re: KOffice applications fail to start on Mac OS X"
    date: 2006-09-14
    body: "Ok... Seems to be in the kdepimlibs :)"
    author: "Ga\u00ebl"
  - subject: "Re: KOffice applications fail to start on Mac OS X"
    date: 2006-09-15
    body: "Yes, kdepimlibs is now necessary for KOffice."
    author: "Boudewijn Rempt"
  - subject: "Re: KOffice applications fail to start on Mac OS X"
    date: 2006-09-14
    body: "Just a warning, KOffice is basically unusable at this point; they're still doing a lot of infrastructure work for the kde4 version.  :)"
    author: "Ranger Rick"
  - subject: "Torrent?"
    date: 2006-09-14
    body: "Are there torrents for the OSX files?  I'm grabbing the \"everything\" dmg, which is close to 2 GB!  BT would definitely be gentler on the fink servers...\n"
    author: "LMCBoy"
  - subject: "Re: Torrent?"
    date: 2006-09-14
    body: "come on, hdtv movies got 9gb now days."
    author: "chris"
  - subject: "Re: Torrent?"
    date: 2006-09-14
    body: "The 2GB download limmit is a well known problem on windows. Does OSX has the same restriction?\n"
    author: "Stephan Boeni"
  - subject: "Re: Torrent?"
    date: 2006-09-14
    body: "is well known? you mean WAS well known"
    author: "c"
  - subject: "Re: Torrent?"
    date: 2006-09-17
    body: "It's not a download limit, it is (was) a file system limit on the largest possible file in FAT32.\n\nIt doesn't apply to NTFS file systems, which are far more common now (although until recently, many dual-booters found it necessary to keep FAT32 as has been easier to write to from Linux, normal users of Windows XP have basically all been on FAT32 for ages, as it is the default FS in Windows XP).\n\nCome on, there are better things to poke fun at Windows for ;)"
    author: "Ben Morris"
  - subject: "Re: Torrent?"
    date: 2006-09-17
    body: "\"NTFS file systems, which are far more common now (although until recently, many dual-booters found it necessary to keep FAT32 as has been easier to write to from Linux,NTFS file systems, which are far more common now (although until recently, many dual-booters found it necessary to keep FAT32 as has been easier to write to from Linux,\"\n\nI realise this is off topic but what's changed recently which would allow linux to boot on NTFS?"
    author: "Sean"
  - subject: "Re: Torrent?"
    date: 2006-09-19
    body: "That would be ntfs-3g. AFAIK, it doesn't allow one to boot from NTFS, but ne can now have their Windows partition be NTFS and write to it from Linux."
    author: "mmebane"
  - subject: "FAT32 and NTFS in WinXP"
    date: 2006-09-22
    body: "Quote:\n\"Windows XP have basically all been on FAT32 for ages, as it is the default FS in Windows XP.\"\n\nWrong. NTFS is the default filesystem in Windows XP/2000. FAT32 was the default for Windows 9x/Me.\n\nTry to get your facts right before posting things you know very little about in public forums."
    author: "DJGM"
  - subject: "Re: FAT32 and NTFS in WinXP"
    date: 2006-09-22
    body: "Oh, come on, shut down your flamethrower.  \n\nHis whole paragraph doesn't make any sense unless you assume that he has made a simple slip and has written FAT32 instead of NTFS the second time, by accident (\"many dual-booters\" use FAT32 as opposed to \"normal users\" who use FAT32; does that make sense to you?!). \n\nHe's perfectly aware of what the default file system type for WinXP is.\n\n"
    author: "cm"
  - subject: "Re: FAT32 and NTFS in WinXP"
    date: 2006-11-09
    body: "I know this reply is late. Initial versions of Windows XP Home Edition (Prior to the Service Pack 1 and 2 slipstreams) did not seem to even -offer- NTFS as far as I remember, using FAT32 by default whenever possible, and then making NTFS the immediate default for partitions that were too large for FAT32. (This is only something like, 30GB maximum for these partitions!) They just offered the option to \"format\" your entire hard disk, assuming that the average home user had no real grasp of how that works. Microsoft has rescinded since then though, making NTFS the default and allowing some sort of partition selection. So, if you're speaking of initial machines featuring Windows XP (Like the later AMD TBird-based OEM boxes, and early P4 and very early Athlon XP OEM boxes) - and the user has the Windows XP home edition, then chances are they do indeed use FAT32. Modern boxes don't use it at all though."
    author: "Jordan"
  - subject: "Re: Torrent?"
    date: 2006-09-15
    body: "I've switched everything to torrents; should work now.  :)"
    author: "Ranger Rick"
  - subject: "Re: Torrent?"
    date: 2006-09-16
    body: "Are the OSX builds native toolkit or X11 ? "
    author: "Scot"
  - subject: "Re: Torrent?"
    date: 2006-09-16
    body: "Native Qt/Mac (well, as native as Qt/Mac is ;)"
    author: "Ranger Rick"
  - subject: "Icon texts"
    date: 2006-09-14
    body: "I think I like the new text under icons policy, but wouldn't text to the right of icons (with some suitable borders around icon + text) be even better? It would preserve vertical space and make more efficient use of the horizontal one. On the current screenshots there is lots of empty horizontal space on toolbars, with vertical space taken up instead of it."
    author: "aaa"
  - subject: "Re: Icon texts"
    date: 2006-09-15
    body: "I've been using \"Text Under Icons\" for a while now.  What I've found to be quite effective is to find a good, readable font, and use a smaller point size.  This cuts down the amount of dead space.  Maybe changing the default font and size would help.  Of course, that doesn't help the vision impaired :-(  but you may want to try it."
    author: "Louis"
  - subject: "Re: Icon texts"
    date: 2006-09-15
    body: "I agree with that, the new text under icons policy hurts those with 1024x768 or  less (yes i still have 2 monitors only capable of 800x600). One reason I use KDE instead of GNOME is that under GNOME every widget seems sooo big so that my 1024x768 screen looks like a 800x600. (I don't intend to start classic flame war again - just express what i feel). IMHO M$ Windows has a pretty good usability where the widgets and text fit nicely even in 800x600 screen. Btw - KDE rocks!"
    author: "fred"
  - subject: "Are the Kunubtu builds Debian compatible?"
    date: 2006-09-15
    body: "I am using Debian Sid (unstable) and am wondering if the Kubuntu packages are Debian compatible.\n\nThanks."
    author: "David R. Litwin"
  - subject: "Re: Are the Kunubtu builds Debian compatible?"
    date: 2006-09-15
    body: "No, since Ubuntu has no binary compatibility guarantees with Debian and it uses packages of qt-copy that are only available in the Ubuntu archive.\n"
    author: "Jonathan Riddell"
  - subject: "Re: Are the Kunubtu builds Debian compatible?"
    date: 2006-09-15
    body: "So Ubuntu took over Debian. Who will overtake Ubuntu? Kubuntu? \n\nSuse presented this \"build server\" stuff. Is it used by KDE? Why are no Fedora or Mandriva packages provided? Does anyone know who to get it run there?"
    author: "furangu"
  - subject: "Re: Are the Kunubtu builds Debian compatible?"
    date: 2006-09-15
    body: "Ubuntu did not \"take over debian\".  Whatever that means, I assume your saying that Ubuntu doesn't give anything back to debian. Which is false. I had read that Ubuntu sends the largest amount of upstream patches to debian than any other debian derivative. Also, taking over would actually mean that Ubuntu's administration has taken over debian's administration, which is entirely false.\n\nVlad"
    author: "Vlad Blanton"
  - subject: "Re: Are the Kunubtu builds Debian compatible?"
    date: 2006-09-15
    body: "Calm down, I think he/she just meant 'overtook', as implied by the rest of the sentence :-)"
    author: "Luke Plant"
  - subject: "Re: Are the Kunubtu builds Debian compatible?"
    date: 2006-09-16
    body: ">>Why are no Fedora or Mandriva packages provided? \n\nYou should ask Fedora and Mandriva that question.\n\nKDE only releases source code, the distributions or users of distributions create the binary packages for the different flavors of Linux.\n\nSo, if there are nog packages for fedora or mandriva, then no one from those distributions took the time and effort creating them.\n\n>>Does anyone know who to get it run there?\n\nYou can compile it from source ;)\n"
    author: "AC"
  - subject: "Re: Are the Kunubtu builds Debian compatible?"
    date: 2006-09-18
    body: "I much prefere the fedora and mandriva way to do. This release is for developers, they know how to get and compile the source."
    author: "Tmor"
  - subject: "So long"
    date: 2006-09-16
    body: "Why does KDE4 take so long?\n\nIs there a clear plan available, or is it just drifting away indefinitely?\n\nWhat needs to be implemented and finished in order for someone to say that it's ready to ship?\n\nWhat is KDE4 actually? What does it focus on? Does it focus on something?"
    author: "waiting"
  - subject: "Re: So long"
    date: 2006-09-16
    body: "An exiting new thing about KDE4 is that it's based on Qt 4.\n\nThis means that all KDE apps will be available for alla platforms supported by Qt 4, like Windows."
    author: "ale"
  - subject: "Re: So long"
    date: 2006-09-16
    body: "Well most not all, it depends on whether they use X11 specific things or not. But that's beside the point. Higher portability is obviously not by far the only target for KDE4. I wasn't asking what new things are planned, I was rather wondering whether there's not too much planned without clear priorities and an efficient release criterion. "
    author: "waiting"
  - subject: "Re: So long"
    date: 2006-09-18
    body: "Is even has the best possible release criterion to users. \"It's released when it's ready. Sooner if you help.\"\n\nThe KDE 3.x series is so good that we can allow for a longer time frame to release KDE 4. No need to panic if it takes longer. There are indeed plans for times to release KDE 4, but these are based on the experience and knowledge of the job from the past. They need not hold true.\n\nThe goals are increased plug/play support in applications, e.g. in networking, like my mail program suddenly knowns if it's ok to attempt to send email now.\n\nA new sound framework to accomplish more in this domain and make it easier to accomplish the easiest things.\n\nA integration of desktop and taskbar.\n\nSome more usability and better internal structures. \n\nCleaning up things that have only been kept for compatability and where better things are now available.\n\nToo much? Probably, but we will never know how things would turn out in the long run, if the plan had been otherwise. My impression is that everybody is having fun, so the most important objective has been reached already.\n"
    author: "Debian User"
  - subject: "Re: So long"
    date: 2006-09-16
    body: "It's taking long because there is more work than people available to do it. We're trying to clean up the API and do the reworks that we want to do because this is our only chance in 5 years."
    author: "Thiago Macieira"
  - subject: "Re: So long"
    date: 2006-09-16
    body: "> there is more work than people available to do it.\n\nThen why not convert more work to less work through focusing on what's important? (And deciding that first of all) It's the only chance in five years, but if the reworking takes two years itself, then maybe some things are already good enough (although not perfect) and don't need to be reworked. I admire the perfectionism of KDE, but I don't know whether it's really practical.\n\nWhat I mean is that a plan like , for instance\n\n- port to Qt4\n- port to windows and mac os\n- clean up APIs but only where experience has proven them dirty (i.e. don't fix what's not really-really broken)\n- something instead of arts (arts is really-really broken)\n- new HIG\n- new workspace\n- desktop search\n- and this is all, when we have it do the release.\n\nAnd this is probably already too much. HIG and desktop search can be added at 4.1 without breaking compatibility (let's just assume, I don't really know), so drop them for now and focus on the rest.\n\n(What I mean is not prevent someone from working on them now - people work on what they want to anyway, but realeasing independently of dropped or postponed targets.)\n\nWithout a strategy like this I fear it might take even longer than spring 2007. Remember Sarge? It can happen. I'm not being absurd. \n\n\n"
    author: "waiting"
  - subject: "Re: So long"
    date: 2006-09-16
    body: "Now, the real answer is of course:\n\n- recruit more developers.\n\nWhen hacking KDE4 becomes easy and is explained to many we will improve speed."
    author: "fur"
  - subject: "Re: So long"
    date: 2006-09-17
    body: "and the mentioned tutorial makes hacking KDE4 easier :-)\nit refers to a doc on develop.kde.org which explains how to build KDE4 as a new user. it has been used by newbies with success. so it is easy to start looking into kde4, even for new comers! please join the fun and participate to this exciting adventure!"
    author: "Anne-Marie Mahfouf"
  - subject: "Re: So long"
    date: 2006-11-09
    body: "KDE 4 appears to be a rather complex redefinition of the \"Desktop Paradigm\" - so it's probably going to take for-bloody-ever. Not to speak for the KDE developers, as I am not one of them... but this is what it appears. I'm willing to wait. KDE 3.5.5 is a lovely desktop environment, i'm running Beryl/Emerald on top of it. Alt-tab features and a feature like OS X's \"expose\" add yet more to it. Plus, it looks pretty. KDE 4 will be out whenever it's out, and it'll change the way you think about your desktop. I used to be a big fan of GNOME/GTK, but it looks like the development is somewhat stagnant. GNOME 2.16 is not different from when I used 2.4 some years ago, except for a slight performance improvement, being less ugly, and having a decent tree-viewing file manager. Oh, and they keep adding little insignificant desktop-accessory apps."
    author: "Jordan"
  - subject: "how to run KDE 4 in SuSE 10.1"
    date: 2006-09-19
    body: "I had installed all packages from\nhttp://software.opensuse.org/download/repositories/KDE:/KDE4/SUSE_Linux_10.1/\n\nHow can I run KDE 4 in my SuSE 10.1? Thx."
    author: "Cavendish Qi"
  - subject: "Re: how to run KDE 4 in SuSE 10.1"
    date: 2006-09-20
    body: "You're not supposed to _run_ KDE 4. These packages are there so you can _develop_ programs for kdelibs 4. But unless you really like bugs, KDE 4 isn't your everyday desktop right now. (If it was, the 4.0.0 release would be out already!)\n\nI'm considering trying to package the KDE 4 snapshots for Fedora to make it easy to develop against them, but I must say the prospect of getting lots of questions like yours discourages me. :-(\n\nThat said, the SuSE packager provided instructions for the use of his packages on his blog, I'm sure you'll find them if you look well enough."
    author: "Kevin Kofler"
  - subject: "Re: how to run KDE 4 in SuSE 10.1"
    date: 2006-09-20
    body: "Ths. It's a \"developers snapshot\". I will wait for new release of KDE 4. ^_^."
    author: "Cavendish Qi"
  - subject: "everything-20061113.dmg.torrent stuck at 99.9%"
    date: 2006-12-13
    body: "Can somebody help me here?  I was downloading the torrent for the everything-20061112.dmg file and it's been stuck at 99.9% for the last few days.  Thanks.\n"
    author: "Trevor"
---
Packages for the first KDE 4 developers snapshot "Krash" have started appearing.  Most exciting is packages <a href="http://ranger.users.finkproject.org/kde/">for a whole new platform, Mac OS X</a>.  More details are on <a href="http://www.racoonfink.com/archives/000700.html">Benjamin Reed's blog</a>.  For the traditionalists packages are available from <a href="http://software.opensuse.org/download/repositories/KDE:/KDE4/">openSUSE</a> and <a href="http://kubuntu.org/announcements/kde4-3.80.1.php">Kubuntu</a>.  If you are a KDE application developer, this is the easiest way to start porting your application to KDE 4.  Meanwhile work is continuing on <a href="http://www.kdelibs.com">KDE on Windows</a> where developers have successfully got <a href="http://www.kdelibs.com/wiki/index.php/Status">all of kdelibs compiling</a>.  Finally the KDE Women project has a new <a href="http://women.kde.org/articles/tutorials/kde4_template/">tutorial to get you started in KDE4 development</a>.



<!--break-->
