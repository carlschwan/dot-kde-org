---
title: "Trolltech Releases Second Preview of Qt for Java"
date:    2006-09-01
authors:
  - "eblomfeldt"
slug:    trolltech-releases-second-preview-qt-java
comments:
  - subject: "classpath"
    date: 2006-09-01
    body: "Is it gnu classpath compatible?"
    author: "furangu"
  - subject: "Re: classpath"
    date: 2006-09-01
    body: "Well if the classpath people do their work right (which they will I'm sure) there shouldn't be any compatibility problems because classpath is an implementation of a known specification. It should just work.\n\nThe problem is not compatibility but the fact that classpath is just not ready yet . If I recall correctly Jambi uses a lot of Java 5 specific features like generics and classpath doesn't implement that yet.\n\nOf course, things move along quickly so I might be wrong already."
    author: "Quintesse"
  - subject: "Re: classpath"
    date: 2006-09-01
    body: "Things move along quickly. You can find the \"generics\" branch of Classpath in their download directory (ftp://ftp.gnu.org/gnu/classpath/ and mirrors). It's quite up to speed, but not yet merged into the main branch as I understand it.\n\nHopefully Sun will pick an acceptable license for their open sourcening of Java, we'll have to wait until October to see."
    author: "Haakon Nilsen"
  - subject: "What about gcj?"
    date: 2006-09-01
    body: "Can you compile this with gcj? :)\n"
    author: "Pedro Alves"
  - subject: "Jython with Qt for Java/Jambi"
    date: 2006-09-02
    body: "Can I use Python/Jython with Qt for Java/Jambi?\n\nThanks!\n"
    author: "anonymous"
  - subject: "Re: Jython with Qt for Java/Jambi"
    date: 2006-09-02
    body: "Why not directly PyQt?"
    author: "ZeD"
  - subject: "Re: Jython with Qt for Java/Jambi"
    date: 2006-09-02
    body: "Yes, you're right. It's only a question if it's possible.\n"
    author: "anonymous"
  - subject: "Re: Jython with Qt for Java/Jambi"
    date: 2006-09-03
    body: "Jython works with any Java accessible library AFAIK.\n\nThen you've always got good old Python with PyQt / PyKDE.\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Legacy toolkits"
    date: 2006-09-03
    body: "I did not see anything about it in the whitepaper, but hopfully Trolltech will include tools in the final version to help port application made with legacy gui toolkits like AWT and Swing. Making it easier for those having such applications to migrate everything to Qt Jambi. Does anyone know if this is planned?"
    author: "Morty"
---
<a href="http://www.trolltech.com">Trolltech</a> has released a <a
href="http://www.trolltech.com/company/newsroom/announcements/press.2006-08-29.2990280700">second preview of Qt Jambi</a> - a prototype version of Qt that allows Java programmers to use the popular cross-platform development framework. This release incorporates the feedback of over 1700 beta testers, and features new
additions like Web Start functionality, improved integration with
Eclipse and single JAR file deployment for Qt Jambi-based
applications. Technical details are available in the <a
href="http://www.trolltech.com/developer/downloads/qt/resolveuid/23a8f998b27c2640e5d343cbe6e119b0"/>Qt
Jambi Whitepaper</a>.  To try it out, sign up for the preview license
and <a href="http://www.trolltech.com/developer/downloads/qt/qtjambi-techpreview">download</a>.



<!--break-->
