---
title: "KDE Commit-Digest for 19th November 2006"
date:    2006-11-20
authors:
  - "dallen"
slug:    kde-commit-digest-19th-november-2006
comments:
  - subject: "Kopete"
    date: 2006-11-19
    body: "I see a lot of development in kopete, and just get sad. :(\nBecause none of this is coming to the current KDE3 version, and KDE4 is probally still a year away.\n\nThis makes me just want that google port talk to Linux ASAP :-P"
    author: "Iuri Fiedoruk"
  - subject: "Re: Kopete"
    date: 2006-11-20
    body: "Just a me too. Got used svn up'ing ot every day, and lately all developement it's just on kde 4 so i can get the new stuff ... "
    author: "doom"
  - subject: "Re: Kopete"
    date: 2006-11-20
    body: "I think exactly the opposite...\n\nIt's a shame that still so much work goes into 3.5. If all commits went into 4.0, we would get kde 4.0 much faster."
    author: "Martin Stubenschrott"
  - subject: "Re: Kopete"
    date: 2006-11-20
    body: "There are much nicer Jabber clients out there other than Kopete. If you like Qt you can try Psi or Jabbin (fork from Psi's dev branch supporting Jingle). If Qt is not requirement you should definitely try Gajim."
    author: "Petteri"
  - subject: "Oxygen Icons Rock"
    date: 2006-11-19
    body: "Wow, is it just me or does the Oxygen icon set look really professional?  The visual diffs for the icons have become the first thing that I look at each week.  Honestly, I'm consistently blown away.\n\nAs a request would it be possible to do a step by step walk though on how one of the new icons was created.  If such a document already exists could someone post a link.  Thanks.\n\nKeep up the great work, the team should be proud."
    author: "beck"
  - subject: "Re: Oxygen Icons Rock"
    date: 2006-11-19
    body: "They really look good big, but I'm still have to see how they will work on the desktop with 22x22, 16x16 and 32x32 toolbars, you know, not everbody have a giant monitor with a high powred video board to get 3000x2000 resolution :)\n\nFor now, I'm just using Human (from Ubuntu) themes on my KDE because they work well on small sizes."
    author: "Iuri Fiedoruk"
  - subject: "Re: Oxygen Icons Rock"
    date: 2006-11-20
    body: "<i>but I'm still have to see how they will work on the desktop with 22x22, 16x16 and 32x32 toolbars,</i>\n\nDitto.\n\nThey'll probably have to redo most of the icons to fit them into smaller sizes."
    author: "Daniel"
  - subject: "Re: Oxygen Icons Rock"
    date: 2006-11-20
    body: "I'm using the oxygen icons right now and they work fine."
    author: "Boudewijn Rempt"
  - subject: "Re: Oxygen Icons Rock"
    date: 2006-11-20
    body: "No doubt that they work, but do they look good? Or will KDE hunt it's users with them ;-)"
    author: "MM"
  - subject: "Re: Oxygen Icons Rock"
    date: 2006-11-20
    body: "Theses are working fine because we are testing every size when we are making them. I've a little script that make that for me (16x16 to 512x512). And if an idea doesn't work well on small size, it's rejected. And if it need adjustement, a specific SVG is done for 16x16 and 22x22.\n\nSecond point, we are already using the Oxygen style on our KDE 3.5 to test it in \"real life\", to see what we need to change."
    author: "Johann Ollivier Lapeyre"
  - subject: "Re: Oxygen Icons Rock"
    date: 2006-11-20
    body: "Thanks for this reply, your work, and no offence..."
    author: "MM"
  - subject: "Re: Oxygen Icons Rock"
    date: 2006-11-20
    body: "No offence, this is a legitime (and usually) ask. I'm just trying to explain how we are working with my poor english (its explain the missing humour and diplomacy) ;)  "
    author: "Johann Ollivier Lapeyre"
  - subject: "Re: Oxygen Icons Rock"
    date: 2006-11-20
    body: "I am no native speaker, too. Again, thank you. Oxygen definitely rocks!"
    author: "MM"
  - subject: "Re: Oxygen Icons Rock"
    date: 2006-11-21
    body: "Good to know that you are working for small icons too. I'm very happy with it. Keep the good work and thank you :)"
    author: "Iuri Fiedoruk"
  - subject: "Ubuntu Tangerine and Tango themes"
    date: 2006-11-20
    body: "The Ubuntu Tangerine theme and the Tango theme have some good icons, too. I suggest adapting as many of those icons as possible (ie. make them shinier) but focus on creating new icons that don't already exist in Tangerine and Tango."
    author: "AC"
  - subject: "Re: Oxygen Icons Rock"
    date: 2006-11-20
    body: "> They really look good big, but I'm still have to see how \n> they will work on the desktop with 22x22, 16x16 and 32x32 \n> toolbars\n\nYeah, I hope that, too. That's the main reason I don't use Crystal because the icons on quickstart bars and in the taskbar are mainly a blurry blue something and are hard to distinguish. Look at Quanta, Kontact, KJobviewer and Konqueror e.g.. \n\nI like the trick of Gnome/Human/Tango/Firefox icons: a dark outline with a bright line next to it. These icons look very sharp at low resolutions."
    author: "Stefan"
  - subject: "KDE4"
    date: 2006-11-19
    body: "Plasma seems still to exist only in /dev/imagination\n\nWhen will we see KDE4? Is there a roadmap? How can we help?"
    author: "ac"
  - subject: "Re: KDE4"
    date: 2006-11-20
    body: "But from the plasma roadmap the development is finished. The next phase is  integrating into KDE.\n\n"
    author: "JC"
  - subject: "Re: KDE4"
    date: 2006-11-20
    body: "The roadmap is just that, a roadmap. Nothing is available from Plasma yet"
    author: "Carewolf"
  - subject: "Duplication..."
    date: 2006-11-20
    body: "Please, pretty please with sugar on top, make a new clean start with KDE4 and remove all duplicate apps from the default modules."
    author: "LB"
  - subject: "Re: Duplication..."
    date: 2006-11-20
    body: "According to my reading of the Okular / ligature discussion there is already a policy of not having two applications which provide the same functionality in the same module.  Which applications were you thinking of?\n\nI'm not quite sure what you mean by 'default modules'.  The only required parts for a KDE desktop are kdelibs,kdepimlibs (KDE 4 only) and kdebase.  After that it is up to the distributions to decide what to ship.  "
    author: "Robert Knight"
  - subject: "Re: Duplication..."
    date: 2006-11-20
    body: "I'm hazy on the timelines here.  I know Okular was a SoC Project (which year?) forked off kpdf and developed in Playground until it was considered mature enough.  So where did Ligature spring from and when?  If it too was a fork of kpdf or kghostview, was it was developed in trunk instead of playground (and if so how was that ever allowed?), or did it just get imported to trunk before okular requested?  \n\nThis one has been brewing for a while, it's a shame that 2 such excellent developers can't find the common ground they need to at least build a common back-end.  Document viewing is a core task for kdegraphics, like file dialogs and print and scanning services, that shouldn't be duplicated, but there might be room for different front-ends in keg designed to perform different tasks (view only versus edit/annote).  \n\nJohn.\n\nP.S. Please tell me that kedit/kwrite/kate will finally get merged into 1 basic and 1 advanced editor using the same text widget????\n"
    author: "Odysseus"
  - subject: "Re: Duplication..."
    date: 2006-11-20
    body: "Ligature is the new name for the well known and loved KViewShell. It is NOT a fork of KPDF or something like that. The discussion on them merging/cooperating/whatever seems to popping up each time one of them is mentioned. I really don't see how discussing this from the outside again and again is going to help. The effort might better be spend doing something more productive, like translating stuff, triaging bug reports, testing, writing code, donating, etc. \n\nKWrite and Kate already *ARE* exactly what you ask for. Just KEdit is a left over that has been kept around in KDE3 for its RTL writing capabilities. I understand that that has already been implemented in KDE4, so yes, KEdit will disapear."
    author: "Andr\u00e9"
  - subject: "Re: Duplication..."
    date: 2006-11-20
    body: "> Ligature is the new name for the well known and loved KViewShell.\n\nLOL. Loved?  Many distro's don't even install it by default (like kubuntu)\nIn its 3.5.5 incarnation it can only show formats that are so rarely used that you can be sure 99% of the KDE using public never used the app."
    author: "AC"
  - subject: "Re: Duplication..."
    date: 2006-11-20
    body: "I use it quite frequently for DVI files. It's also much faster than KPFD/Okular, so I prefer it. \n\nBTW: you do know that 86,7% of all statistics is made up, don't you? I feel your 99% falls in the same category..."
    author: "Andr\u00e9"
  - subject: "Re: Duplication..."
    date: 2006-11-20
    body: "Kedit will die ?\nIt would make me sad, because Kedit is so much faster than Kate and Kwrite !\nIf I have a .txt to read, it's in Kedit.\nIf I have to work on a text file, that's whith Kate.\nFor me Kwrite is useless, because too slow, whith to much feature in order to just read a text, and just not as good as Kate for editing. But some people like it.\n\nIf Kedit die, make Kwrite much more faster to start, or even Kate."
    author: "Kollum"
  - subject: "Re: Duplication..."
    date: 2006-11-20
    body: "There will always be fast text viewers around.  Here's one for example:\n\nhttp://kde-apps.org/content/show.php?content=41031&PHPSESSID=ba143c2e5c5547c50ea795d46197acf9\n\n"
    author: "Leo S"
  - subject: "Re: Duplication..."
    date: 2006-11-21
    body: "don't worry, kwrite will be faster in KDE 4, as will all KDE 4 apps ;-)"
    author: "superstoned"
  - subject: "Re: Duplication..."
    date: 2006-11-20
    body: "agreed."
    author: "ponder"
  - subject: "Re: Duplication..."
    date: 2006-11-20
    body: "Why does it bother you so much? Do you think those developers would otherwise automaticly be working on something else in KDE? Think again.\nA bit of competition isn't bad. It can trigger nice developments and new ideas. These sometimes need a bit of time to mature, so you need to give developers some leaway to play around and try things their way before having to negotiate each change with a bunch of other developers. This duplication might seem a waste of effort, but I tend to disagree.\n\nWould you claim linux is a duplication of windows? Or GNOME of KDE? Neither would I."
    author: "Andr\u00e9"
  - subject: "Re: Duplication..."
    date: 2006-11-20
    body: "> Why does it bother you so much? \n\nIn contrary to what you imply in your reply, the poster was pointing out he was worried about duplication in the kde-release. Not the generic duplication of effort.\nAs you should know, KDE aims to be a usable and complete set of software. And having 2 apps that have the same target usergroup in its release is just a no-go."
    author: "Thomas Zander"
  - subject: "Re: Duplication..."
    date: 2006-11-20
    body: "Since when is choice a no-go in KDE land? \nHow does including some duplicated pieces of software make KDE less complete or usable?\n\nAs another in this thread pointed out, distributions make their own choices on what to include anyway. It is my understanding that KDE does not distribute packages for distributions but leaves the packaging to them. Why not /let/ them make the choice if do they that already? Why does KDE have to choose?"
    author: "Andr\u00e9"
  - subject: "Re: Duplication..."
    date: 2006-11-20
    body: "Because there's a difference between giving distributors a choice, and forcing them to make that choice entirely.\n\nHaving alternative apps for a similar purpose is a good thing.\n\nShipping BOTH apps by default is a bad thing. Distributors should have a choice to customize KDE, not be forced to: KDE should be sane by default even if the distributor doesn't have the ressources or the will to do that job for us (think Slackware). And shipping two similar apps for the same purpose by default is simply not sane.\n\nAt this point, it looks like Ligature is going to go, seeing as it seems focused on the most esoteric formats that aren't in common mainstream use (yes, I do know about DVI; yes, I have been known to use it personally; no, I don't think that justifies shipping a DVI viewer by default, sorry). And that's a freaking pity, because Ligature is a good app. If only developers could let go of their freaking egos... :(\n\nIdeally, the battling of egos would stop right now, and one of the two apps moved as a backend to the other. (Wilfried, dear: Ligature would probably have to be the one going, for the reasons Aaron brought up. I wish I could find a way to put it more nicely -- it's not that Ligature is a bad app; it's more a matter of scope and focus, really.)\n\nSecond best solution is to move both to KEG and let the users clamor for what they need. And here's a hint: rightly or wrongly, they're already clamoring for oKular, be it only because the formats it supports are in MUCH broader use.\n\nMind, I know that merging an app into another is hard and not just any kind of developer has the talent to do it right. However, I didn't think it was above you, Wilfried. Am I wrong in this?"
    author: "A.C."
  - subject: "Re: Duplication..."
    date: 2006-11-21
    body: "now i'm no developer, but from a users point of view, and having seen and used both okular and ligature in their current state (kde4) - i'd rather use ligature as backend for okular's UI... okular has a better (more complete and usable) UI, but its 10 times slower than ligature. text selection drags behind the mouse on a 2 ghz processor, and once selected, there is no way to copy the text so it's useless anyway. at least this works nicely in ligature...\n\ni think we can talk about this for ages, and it won't help. the authors won't work together, and most likely the Technical Work Group will have to choose one app to be included in KDE. And as aaron prefers Okular, i think that's what it's going to be...\n\ni do remember a ligature developer who said something like he thought it was rather stupid to allow a summer of code project to add to KPDF all those features already mostly available in kviewshell, but hey, it did happen, and now we have two apps duplicating each other. let's hope the competition makes em both better, not worse."
    author: "superstoned"
  - subject: "Re: Duplication..."
    date: 2006-11-21
    body: "> text selection drags behind the mouse on a 2 ghz processor\nI improved a bit its performances last week.\n\n> and once selected, there is no way to copy the text so it's useless anyway. at \n> least this works nicely in ligature...\nAnd you think noone is already working on that? Come on...\n\n> i do remember a ligature developer who said something like he thought it was\n> rather stupid to allow a summer of code project to add to KPDF all those \n> features already mostly available in kviewshell,\nWell, when Piotr started to develop okular, kviewshell as the ligature you know was started too.\n\nsupestoned: I know you love ligature and you would like to see okular burning in the hell flames, but hey, if you have _constructive_ comments about okular, they are welcome.\nBut your current way of discussing is more flaming."
    author: "Pino Toscano"
  - subject: "Re: Duplication..."
    date: 2006-11-21
    body: "well, actually i like 'em both. and i really like the work on okular's annotations, i've been trying it for some time now. and of course somebody is working on the text selection. but i oppose the things Aaron said, when stating Ligature is more like kpdf, while okular is ahead. i think okular and ligature are both more or less equal. that's a bad thing if you have to choose one, and maybe a good thing in the sense of competition.\n\nbtw, what's nice about all this is that the KDE desktop environment has two PDF (etc) viewers which are both way ahead of the competition ;-)\n\nabout constructive comments about okular, well, thanx for speeding up the text selection... i just tried it, and it doesn't lag behind the cursor, so you did great.\n\nok, lets try to be constructive. what about the following: would you like to write a joint statement about this whole situation with the ligature guys?\n\ntell everybody what you both think about:\n- duplication of apps in KDE: a good or a bad thing?\n(so: not about having several apps for one thing, that's not bad, that's darwinism in free software. but what about shipping both with KDE-graphics by default? maybe: why would we need both?)\n- technically, how do okular and ligature compare? you guys are the developers. you must be able to find out which one is ahead where. maybe, more can be shared. maybe you can help the TWG making a decision here ;-)\n- what are the future plans? not just being better as the other one, right? how see okular/ligature the competition, and how are you guys going to be better?\n\ntry to make it a JOINT statement. see you both agree on it, not just answer the questions seperately...\n\njust an idea. it'll bring you guys together to create something, could be a start of something beautiful. or you'll guys see how to differentiate okular and ligature, so users can use the both for differend purpose... ???"
    author: "superstoned"
  - subject: "Re: Duplication..."
    date: 2006-11-23
    body: "> when stating Ligature is more like kpdf, \n\ncompare kpdf and kviewshell in 3.5. then compare ligature to that same kpdf. which has adopted the features of the other? it seems a bit obvious to me, really =)\n\n> while okular is ahead.\n\ngiven that okular has added additional features since 3.5's kpdf version, has gained supported for multiple file formats and has a devel team with positive energy i stand by my statement here.\n\nthat said, okular is missing adding/removing pages from files and the nicer smooth scrolling of okular. somewhat minor features, but useful ones.\n\ni think both could be further along if everyone worked together, however, as they aren't really working on unique apps but working on virtual carbon copies of the same one."
    author: "Aaron J. Seigo"
  - subject: "Re: Duplication..."
    date: 2006-11-23
    body: "> > i do remember a ligature developer who said something like he thought it was\n> > rather stupid to allow a summer of code project to add to KPDF all those\n> > features already mostly available in kviewshell,\n> Well, when Piotr started to develop okular, kviewshell as the ligature you know was started too.\n\nActually kviewshell started as a fork of KGhostview on May 12, 2000. The initial\nauthor was Matthias Hoelzer-Kluepfel. You can check this by consulting the SVN logs.\n\n> supestoned: I know you love ligature and you would like to see okular burning in \n> the hell flames, but hey, if you have _constructive_ comments about okular, they \n> are welcome.\n> But your current way of discussing is more flaming.\n\nMentioning concrete problems of an application _is_ constructive.\n\nGreetings,\nWilfried Huss "
    author: "Wilfried Huss"
  - subject: "Re: Duplication..."
    date: 2006-11-23
    body: "> Mentioning concrete problems of an application _is_ constructive.\nEvery statement like \"foobar does not work, please fix ASAP\" can never be costructive.\nInstead, statements like \"i tried using foobar this and that way, but it does not work. the problem(s) i noticed are bla and blabla.\" are _way more_ constructive. \nWhat do you do if someone files a bug report for any of your maintained application, writing stuff like \"foobar does not work, please fix ASAP\"?"
    author: "Pino Toscano"
  - subject: "Re: Duplication..."
    date: 2006-11-22
    body: "Ok, duplication is the wrong word, but to reply to question. Although I'm a KDE fan, I'm happy that Gnome exists as well. I wouldn't like if a distro installs KDE and Gnome at the same time. It should be only one. Life is about choice, it's time to choose unique and default applications that are shipped with KDE4. (e.g. default universal document viewer)"
    author: "LB"
  - subject: "scroll bug"
    date: 2006-11-20
    body: "I hope that at least in version KDE4 the file dialogue key scroll bug will be solved. I mean the jumping selection you can easily reproduce when you open the dialogue and browse real world files."
    author: "Bertram"
  - subject: "Re: scroll bug"
    date: 2006-11-20
    body: "What you can do to is to report it to http://bugs.kde.org, if not already reported."
    author: "Pino Toscano"
  - subject: "Re: scroll bug"
    date: 2006-11-20
    body: "It is already submitted. I have too noticed it alot."
    author: "Marius"
  - subject: "Re: scroll bug"
    date: 2006-11-21
    body: "Same here. I'm surprised this bug has lasted for so long already."
    author: "Shahar"
  - subject: "Digikam ???"
    date: 2006-11-20
    body: "I look into KDE Commit-Digest since a long time and i can't see fresh news about Digikam project. No commit, no activity, no report... sound like a dead project? What the Digikam 0.8 future ?"
    author: "Hercule"
  - subject: "Re: Digikam ???"
    date: 2006-11-20
    body: "Digikam is one of the most active projects :)\n\nDigest covers only small part of KDE development activity. If you want a good coverage of particular project you should subscribe to related mailing lists.\n\nDigikam 0.9 is planned as Christmas gift and it will be one of the best photo management apps in the world - including commercial programs."
    author: "m."
  - subject: "Re: Digikam ???"
    date: 2006-11-20
    body: "I'm really looking forward to the new release, it looks amazingly cool! \n\nI just hope my new Nikon D80 will be supported. I can see that DCRAW supports it, but it isn't mentioned on the gphoto2 site."
    author: "Joergen Ramskov"
  - subject: "Re: Digikam ???"
    date: 2006-11-20
    body: "With 0.9.0 release digiKam include dcraw 8.41 source code in core. There is no external dcraw depency. It's the same for RAW converter kipi-plugin.\n\nFor a lot of technical reasons, this is mandatory. We have a lots of bug reprot depending of different dcraw version used by users witch are completly uncompatible (problem with dcraw command line options in fact)\n\nOf course, dcraw will be updated in digiKam core in the future. I have also planed to do a shared dcraw interface between digiKam core and kipi-plugins witch will reduce duplicate code and simplify developement.\n\nGilles Caulier"
    author: "Caulier Gilles"
  - subject: "Re: Digikam ???"
    date: 2006-11-21
    body: "Can the devels PLEASE speed up showfoto?\nDigikam 0.8.x was so much faster...now it's so SLOW...\n\nI need gqview speed going from 3 mb JPEG to 3 mb JPEG. Speed baby!"
    author: "JoeSchmoe"
  - subject: "Re: Digikam ???"
    date: 2006-12-24
    body: "same problem here, Digikam 0.9 is so slow when showing photos with showFoto that I organize my albums with digikam, but use the feh image viewer for doing slideshows.\nDigikam 0.8 was much faster."
    author: "tr"
  - subject: "Re: Digikam ???"
    date: 2006-11-21
    body: "But does that mean that the next release will support my D80 which the current release doesn't. I'm talking about loading the pictures from the camera and to the computer, not support for Nikons RAW format, which I guess it will support through dcraw."
    author: "Joergen Ramskov"
  - subject: "Re: Digikam ???"
    date: 2006-11-21
    body: "If you use PTP protocol, digiKam use gphoto2 libary for that. If you use USB mass Storage, no problem, digiKam support it without gphoto2 library.\n\nLook at this url for more recent camera supported under linux :\n\nhttp://www.teaser.fr/~hfiguiere/linux/digicam.html\n\nGilles Caulier"
    author: "Caulier Gilles"
  - subject: "Re: Digikam ???"
    date: 2006-11-22
    body: "Yeah, I have looked there and the Nikon D80 isn't mentioned. Maybe I'm just doing something wrong though, I'll try connecting again through digicam later today.\n\nPs. Anyone else not getting notifications of new replies?"
    author: "Joergen Ramskov"
  - subject: "Re: Digikam ???"
    date: 2006-11-20
    body: "Since one year, we working hard to develop next generation of digiKam : 0.9.0 release.\n\nWe have recently updated the web project page with a lots of screnshots and new feature decriptions.\n\nEspecially, i recommend to take a look at these url:\n\n-Next 0.9.0 features: \n\nhttp://www.digikam.org/?q=about/features09x\n\n-Team member blogs with a lots a development progress:\n\nhttp://www.digikam.org/?q=blog \n\nActually, we are in post release Candidate. We plan to release final 0.9.0 version at Christmast.\n\nGilles Caulier"
    author: "Gilles Caulier"
  - subject: "Re: Digikam ???"
    date: 2006-11-20
    body: "Is there a way to have the database not on the own computer?\n\nIn my setup, all photos are on the server and last time I checked digikam, digikam wanted to have everything locally.\n\nAs I don't want to have the photos locally (as then they are not any longer available to others) I cannot use it.\n\nThis is not a must have for digikam, as I have simply a different setup than most users, I wouldn't mind if there is no possibility to use it in a network environment as well.\n"
    author: "Philipp"
  - subject: "Re: Digikam ???"
    date: 2006-11-20
    body: "Sorry,\n\nI wasn't precise enough.\nFor sure if I would add the server via linux fstab to the local filesystem, it would work.\nBut I tried it with KIO slaves (SMB) and here it failed."
    author: "Philipp"
  - subject: "Re: Digikam ???"
    date: 2006-11-20
    body: "I have the same complaint here.\n\nBut the more I was thinking of it, the more complicated it seemed to me to manage many databases not located on the local computer."
    author: "Kollum"
  - subject: "Re: Digikam ???"
    date: 2006-11-20
    body: "There is no change about database management with 0.9.0 against 0.8.x.\n\nThere area a lot of changes in others parts between these release. This si why we have delayed later 0.9.0 release all works on database.\n\nOf course we will working on later to provide a new database interface not limited to SQlite backend and not limited to a local place (using to an NFS server for example)\n\nThere are a lots of reports from users on B.K.O about to improve database interface.\n\nGilles Caulier"
    author: "caulier gilles"
  - subject: "Re: Digikam ???"
    date: 2006-11-20
    body: "One thing I whould realy like in digikam in future would be a external hard drive capability ( whith network / ftp ).\n\nI mean, I have my photo directory on my local drive. I have a usb drive on whitch I whant to do Mass storage, to keep only the most interesting photos on my local litle ( laptop ) hard drive.\n\nI whant to upload photos on a personal ftp server to share whith all my family. They just have to start digikam to have every piece of information manadged by Digikam that I added to the shared files, such as collection, tags, photo information, dates ....\n\nMaybe a realy huge work to do, and I will TRY to copy to a whishlist."
    author: "Kollum"
  - subject: "Re: Digikam ???"
    date: 2006-11-20
    body: "This is can be done into Kipi-plugins. Some threads are started in kde-imaging at kde.org ML about to build a common plugins GUI interface for remote access tools (flickr, gallery, etc).\n\nFTP shared connection tool can be added in this list. Please report you wish in B.K.O (if there is no current entry about this subject of course) :\n\nhttp://bugs.kde.org/buglist.cgi?product=kipiplugins&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED\n\nGilles Caulier"
    author: "Caulier Gilles"
  - subject: "Re: Digikam ???"
    date: 2006-11-21
    body: "i'd say use strigi in KDE 4 for this... you'll automatically support several database types ;-)\nKalzium is going to be the first KDE app to use it, beat amarok to it and make digikam the next one :D"
    author: "superstoned"
  - subject: "Re: Digikam ???"
    date: 2006-11-21
    body: "Interresing. I wil take a look.\n\nBut this require than digikam and DigikamImagePlugins and libkipi and Kipi-plugins are ported to Qt4 KDE4. Off course it's planed but it will take a while...\n\nGilles Caulier"
    author: "Caulier Gilles"
  - subject: "Re: Digikam ???"
    date: 2006-11-21
    body: "don't forget to ask questions on #strigi on freenode! ask vandenoever, the lead developer, or others whatever you need. this way, they can focus on those areas, to help make digikam 1.0 the best ever (if that's the release for KDE 4...).\n\nbtw, digikam is great - i love your work!"
    author: "superstoned"
  - subject: "Re: Digikam ???"
    date: 2006-11-20
    body: "The current development versions of digikam are indeed pretty stunning, the UI is nicer (for adjusting dates and such) compared to 0.8 and the image editing capabilities mean that I rarely fire up the Gimp for messing with my photos nowadays."
    author: "Simon"
  - subject: "Re: Digikam ???"
    date: 2006-11-20
    body: "...and it's not finish. In the future, i would to add in new features for photographers :\n\n- a CLens interface to fix Lens distorsion using PTLens camera profiles.\n- a new clone tool to fix dust spot or unwanted object on photograph.\n- Versionning of pictures. Original are untouched.\n\nGilles Caulier"
    author: "Caulier Gilles"
  - subject: "Renaming"
    date: 2006-11-20
    body: "\"KDissert is renamed to Semantic\", last week \"KViewShell is renamed Ligature\"\n\nIs it just me who feels that this is going in the wrong direction? Neither Semantic nor Ligature nor Ocular tells me anything what the application is all about. These names might sound cool but they do not help me finding the applications I am looking for. KDissert, KViewShell, and KPDF did..."
    author: "Dominic"
  - subject: "Re: Renaming"
    date: 2006-11-20
    body: "Does Firefox, Powerpoint, Java, K3B, Nautilus... tell you anything about the usage of the application? - No. Does it matter? - no."
    author: "Max"
  - subject: "Re: Renaming"
    date: 2006-11-20
    body: "Usually it's pretty handy that name tells you something. Earlier there were company named Tele and I guess it's quite easy even to foreigner to guess what it did. Nowadays there is Sonera. What that's name can tell you? Nothing."
    author: "Mikael Kujanp\u00e4\u00e4"
  - subject: "Re: Renaming"
    date: 2006-11-20
    body: "> Earlier there were company named Tele and I guess it's quite easy even to foreigner to guess what it did.\n\nThey made guitars?"
    author: "pete"
  - subject: "Re: Renaming"
    date: 2006-11-21
    body: "television? telephony? telepathy?"
    author: "superstoned"
  - subject: "Re: Renaming"
    date: 2006-11-21
    body: "telekinesis?"
    author: "superstoned"
  - subject: "Re: Renaming"
    date: 2006-11-22
    body: "Telephony is quite accurate. Tele used to be telecommunication company, today it's mostly mobilephone operator (but uses name Sonera, or TeliaSonera). I think the old name was good and two of three of you guessed it has something to do with communication.\n\nIn some cases the name can make difference."
    author: "Mikael Kujanp\u00e4\u00e4"
  - subject: "Re: Renaming"
    date: 2006-11-20
    body: "I like the new names, they are much nicer and it is acutally possible to pronounce them. Everybody was complaining about the K in front of every KDE application name, and rightful so.\n\nAnd I don't think it is a problem that the app names don't reflect what they do. That's what the description/summary is for. Both the K-Menu and the package managers (at least for Kubuntu and SuSE) have a description for every application, so it should not be a problem to find the right one for a given task."
    author: "Thomas McGuire"
  - subject: "Re: Renaming"
    date: 2006-11-20
    body: "On a usability note it will make it easier to actually find the applications in menus.\n\nI don't have benchmarks for this, but I find it difficult to quickly locate program names in the K-Menu when they all start with the same letter, because it is quite easy to scan for a unique first letter (eg. \"O\" for Okular, \"A\" for Amarok or \"L\" for Ligature\") , and more time consuming to scan for a particular second letter (eg. \"P\" in KPDF, \"M\" in KMPlayer or \"V\" in KViewShell).\n\n"
    author: "Robert Knight"
  - subject: "Re: Renaming"
    date: 2006-11-21
    body: "Sounds like lazyness to me..."
    author: "illogic-al"
  - subject: "Re: Renaming"
    date: 2006-11-21
    body: "maybe to you, but if millions of users daily save a few seconds looking for their app in the KDE menu, they can do more work in the same time, the world economy will grow a bit, poor people get less poor and everybody is happy ;-)\n\nso let's all use and improve KDE and make the world a better place :D"
    author: "superstoned"
  - subject: "Re: Renaming"
    date: 2006-11-21
    body: "yeahyeah, exactly what I've been saying for years now. screw inefficiency. coders unite! _regards_"
    author: "Marcel Partap"
  - subject: "Re: Renaming"
    date: 2006-11-20
    body: "I also like the K-Names, and I think they give a great feeling of \"this is a KDE App\". \n\nBut it seems, the new names are easier to market to corporates, and they sound stronger for standalone apps. \n\nSo I can't really take a position here. At the end my K-Menu will show me \"Dissertation Program (Ligature)\" :)"
    author: "Arne Babenhauserheide"
  - subject: "Re: Renaming"
    date: 2006-11-21
    body: "I totaly agree. Also, what ever one thinks of the use of K in KDE applications, it is (was?) a nice trademark. A shame to see the use of K declining. KViewShell is actually the most sensible app name I've seen in years..."
    author: "Jo \u00d8iongen"
  - subject: "khelpcenter"
    date: 2006-11-23
    body: "khelpcenter is in such a mess (in Kde 3x also). Developers, please give it some love.\n"
    author: "Sunny Sachanandani"
---
In <a href="http://commit-digest.org/issues/2006-11-19/">this week's KDE Commit-Digest</a>: <a href="http://ktorrent.org/">KTorrent</a> supports the creation of trackerless torrents, with work beginning on a web-based management GUI. Support for browsing the SHOUTcast webradio listings in <a href="http://amarok.kde.org/">Amarok</a>. Work starts on a new Planner Summary plugin for <a href="http://www.kontact.org/">Kontact</a>. <a href="http://www.freehackers.org/~tnagy/kdissert/">KDissert</a> is renamed Semantik. Maps of more countries added to <a href="http://edu.kde.org/kgeography/">KGeography</a>. Version 2 of <a href="http://kallery.kdewebdev.org/">Kallery</a>, a web image gallery creator, is imported into KDE SVN. Qt3 and KDE 3 Java bindings are removed from KDE SVN, superceded by the developments of <a href="http://www.trolltech.com/developer/downloads/qt/qtjambi-techpreview">Qt Jambi</a>.

<!--break-->
