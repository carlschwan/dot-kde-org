---
title: "UbuntuOS Podcast with KDE Developer"
date:    2006-06-27
authors:
  - "imbrandon"
slug:    ubuntuos-podcast-kde-developer
comments:
  - subject: "My poor ears"
    date: 2006-06-27
    body: "Unfortunately this sounds much more like some EVP experiment featuring spirit voices on audio recordings :-/ Therefore it's a real challenge to keep listening more than 5 minutes.\n\nHowever I'd like to say \"Thank You\" to Jonathan and all the Kubuntu developers for all the hard work on this fine piece of software."
    author: "KDE"
  - subject: "Re: My poor ears"
    date: 2006-06-27
    body: "Hey, we were running on a backup recording system since the main one went down 5mins before Riddel was due to meet us online and speek to us, hence mega poor quality. \n\nThanks to Riddel for taking the time to speak to us, and I can asure next Kubuntu cast (in 2 weeks) will have the problems sorted. :)\n\nJames "
    author: "James"
  - subject: "Re: My poor ears"
    date: 2006-06-29
    body: "Yeah right!\nWhy would a backup system have mega poor quality?\n"
    author: "reihal"
  - subject: "Re: My poor ears"
    date: 2006-06-29
    body: "because it was not a recording computer, just a laptop. The main one went down so I quickly installed stock packages of the audio apps that we use on their and got on with recording.\n\nWe have said sorry for the techincal difficultes we had, they def wont be there next show we do it was a freak occurance. \n\nCalling us liers is very imature."
    author: "James"
  - subject: "Mastering..."
    date: 2006-06-30
    body: "A good mastering job may have helped (ignore this comment if you guys have been doing that... :). I haven't listened, so I can't really say, but it would be worth a shot should something like that ever happen again."
    author: "fpoole"
  - subject: "Re: My poor ears"
    date: 2006-07-01
    body: "I didn\u00b4t call you a liar, you said that,and I can spell \"immature\".\nThers is no reason at all that a laptop should have \"mega poor quality\" audio.\nIt\u00b4s all up to the user.\n"
    author: "reihal"
  - subject: "anyone care to summarise?"
    date: 2006-06-28
    body: "Im on dialup for a week or two yet, and was wondering if anyone felt like summarising the Riddell content into a comment here?\n\n"
    author: "Robert Guthrie"
  - subject: "Re: anyone care to summarise?"
    date: 2006-06-28
    body: "Yeah, I'd second this point, too. This new habit to make podcasts out of everything instead of putting the good old transcript only is a bit annoying ;). \n\nMaybe it's only me, but I (as a non-native English speaker) am more comfortable with reading an interview. And, to be sure, it's easier to read an interview at work than to listen to it in the offce :-))))."
    author: "AC"
  - subject: "Re: anyone care to summarise?"
    date: 2006-06-28
    body: "I am working on a transcript and will post a link to it on the ubuntuos.com homepage. Im not sure of when this will be avaliable."
    author: "Joshua Henderson"
  - subject: "Ubuntu project?"
    date: 2006-06-28
    body: "I wonder if this is an Ubuntu project: http://beatnik.infogami.com/Gimmie\n\nImho it's very cool, and incorporates quite a few of the ideas which have been floating around for KDE 4. OK, its for Gnome, but again - it looks cool, and it already seems quite usable. Why don't we try to convince this guy to work on KDE 4 instead of waste his time on Gnome (it's not very likely the Gnomes will accept his work, it's not paid for by Sun, Novell or Red Hat so they can push it, and I've read a lot of negative reactions already... It doesn't look like the gnomes are looking for innovation.)?"
    author: "superstoned"
  - subject: "Re: Ubuntu project?"
    date: 2006-06-28
    body: "Sorry, I didn't really \"get\" this concept, let alone what's happening in those videos. Of course I get what he intends to tackle, but I fear that needs a different approach. Apart from that those colorized items look really strange."
    author: "KDE"
  - subject: "Re: Ubuntu project?"
    date: 2006-06-28
    body: "get rid of a all-in-one manager, but make a few: contacts/persons, documents and applications. each with a similair-looking, easy, searchbased interface.\nand second, the grouping on the taskbar. these two ideas are what he is implementing, and they are great. much easier to find stuff, and its all looking consistend and easy.\n\nOk, maybe this can be done even better, but i think the times of the filemanager are over. contacts can't be managed with konqi, and if they could - it would suck. let each app do what it does best - amarok for music (atm, i mostly use konqi to get files into the music folder... silly, sure!), digikam for foto's, and a document manager for documents, maybe a moviemanager too. and a filemanager for everything which doesn't fall in these categories."
    author: "superstoned"
---
<a href="http://www.ubuntuos.com/">UbuntuOS</a> have launched their new <a href="http://ubuntuos.com/2006/06/podcast-6">extended Podcast</a> with an interview of <a href="http://www.kubuntu.org">Kubuntu</a> developer Jonathan Riddell discussing the plans for the next release, Kubuntu Edgy. 100MB download, get it directly from <a href="http://kubuntu.org/~jriddell/ep6-062606-ubuntuos.ogg">Mirror 1</a>, <a href="http://geeksoc.org/~jr/ep6-062606-ubuntuos.ogg">Mirror 2</a>. The interview starts 1 hour 10 minutes in.






<!--break-->
