---
title: "KDE e.V. Quarterly Report Q1 2006 Published"
date:    2006-05-22
authors:
  - "sk\u00fcgler"
slug:    kde-ev-quarterly-report-q1-2006-published
comments:
  - subject: "KDE e.v. and Mellon Awards"
    date: 2006-05-22
    body: "Thanks for the report. Lot have been done last quarter !!\n\nIs somebody of the board taking care of applying KDE e.v. for the Mellon Awards ? http://rit.mellon.org/awards/\n\nThe first prize is 100 000 $, enough to fund our champaign swimming pool for 2007 Akademy."
    author: "Charles de Miramon"
  - subject: "Re: KDE e.v. and Mellon Awards"
    date: 2006-05-22
    body: "While the Mellon awards are an attractive prize, I haven't been able to think of a way to honestly apply for the award. The criteria *to me* are unmet by KDE as a whole or any part that I can find. Other input is welcome, of course, but it needs to be in the form of a concrete application (for the award).\n\nPS. Not everything should devolve to the board -- there are other KDE people who can take on tasks. Like .. you! Contribute!"
    author: "Adriaan de Groot"
  - subject: "Re: KDE e.v. and Mellon Awards"
    date: 2006-05-22
    body: "As a mere user, the top post looked right on target, so perhaps I must be missing something.  It does look like it has to be KDE eV that gets the nomination: \n\n \"the importance of the organization\u0092s support to the success of the project\"\n\nbut, e.g., perhaps an example relationship would be KDE eV to KOffice to ODT  \n\n\"Successful efforts to increase the benefits of the project by integrating or collaborating with other open source development projects, including the adoption or encouragement of open standards\"\n\nor perhaps to Gnome and freedesktop.org?\n\nAnyway the point is I'm happy to draft something, because that's my day job, but I'd obviously like it checked for fact and suitability.  Any idea where I might publish a draft for contribution comment? \n\n "
    author: "gerryg"
  - subject: "Re: KDE e.v. and Mellon Awards"
    date: 2006-05-22
    body: "As a total pessimist, I looked at other things, like the requirements listed under \"Eligibility and Award Criteria\". I can't see how we fulfill item (1), even though (2) and (3) are obvious. Again, this is _my_ take on it, please don't let that stop you from explaining how we do qualify and producing a good document.\n\nAs for where to publish a draft -- try kde-promo@kde.org, that's a good and lively mailing list for the probmotion of KDE."
    author: "Adriaan de Groot"
  - subject: "Re: KDE e.v. and Mellon Awards"
    date: 2006-05-22
    body: "Just before I start my 1000 words: eleigibilty criterion 1 seems to be met directly and indirectly: on \"higher education\" - google summer of code, largest number of sponsored students last year IIRC?  less directly: \"scolarly\" enabling collaboration - all projects generally; promoting standards etc, and providing a platform that supports support e.g., including but not limited to Kdissert? \"arts\" all the music stuff? krita? thinking Zen and the art of CSS (or whatever that book is called) Quanta? \n\n"
    author: "gerryg"
  - subject: "Re: KDE e.v. and Mellon Awards"
    date: 2006-05-22
    body: "I'm drafting an e-mail to the person in charge of the award to understand if they are looking for software *for* Universities and Universities Libraries or project that can be useful for their constituencies"
    author: "Charles de Miramon"
  - subject: "Re: KDE e.v. and Mellon Awards"
    date: 2006-05-22
    body: "I've got an answer for the Mellon Fundation. We do not qualify. No champaign pool ;-)"
    author: "Charles de Miramon"
  - subject: "Re: KDE e.v. and Mellon Awards"
    date: 2006-05-23
    body: "\"We\" as in KDE as a whole? What about subprojects? [[ oh, and surely a Frenchman would write Champagne? ]] It struck me while riding my bicycle that Krita does target the arts (with its oildpaint simulations and the like) so that might be a useful subproject to reconsider.\n\nAt the same time, I'm a big fan of *critically* looking at the requirements for applications before sending something off -- I'm sure the foundation has better things to do than sift through applications that don't make sense."
    author: "Adriaan de Groot"
  - subject: "Re: KDE e.v. and Mellon Awards"
    date: 2006-05-22
    body: "\"to increase the benefits of the project by integrating or collaborating with other open source development projects, including the adoption or encouragement of open standards\"\n\n!?\n\nPerhaps an organisation like SPI or FFII??\n\nOr projects like Mailman, SVN, bugzilla?"
    author: "Johan S. Bergen"
---
The <a href="http://ev.kde.org/reports/ev-quarterly-2006Q1.pdf">KDE e.V.'s first Quarterly Report for 2006</a> has been published. Those reports are used by the KDE e.V. to report its activities to the public. The highlights in this report include updates from the Working Groups, a report from the last KDE e.V. board meeting and a couple of other updates. The Technical Working Group for example announced a <a href="http://developer.kde.org/development-versions/kde-4.0-release-plan.html">planned technical preview of KDE 4</a>, the Human-Computer Interaction Working Group informs about progress in developing plans for usability, accessibility and artwork for KDE 4. The Marketing Working Group gives updates about the status of the promotional community around KDE 4 and different other aspects regarding marketing-related efforts.




<!--break-->
