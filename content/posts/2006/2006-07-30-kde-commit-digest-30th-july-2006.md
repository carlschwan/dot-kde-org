---
title: "KDE Commit-Digest for 30th July 2006"
date:    2006-07-30
authors:
  - "dallen"
slug:    kde-commit-digest-30th-july-2006
comments:
  - subject: "Great progress!"
    date: 2006-07-30
    body: "\"Fixes playback of youtube videos embedded in other web sites.\"  This bug is a major reason I was sticking with Firefox.  Will this fix be in 3.5.4?\n\nAlso I'm *really* looking forward to okular.  It's going to rock.\n\n"
    author: "DeKay"
  - subject: "Re: Great progress!"
    date: 2006-07-30
    body: "\"Fixes playback of youtube videos embedded in other web sites.\" \n\nYes, please, please include this in the next minor release. I only keep switching to Firefox sometimes for this single reason.\n"
    author: "Martin"
  - subject: "Re: Great progress!"
    date: 2006-07-30
    body: "Unfortunately, no - it slipped by 3.5.4."
    author: "Eike Hein"
  - subject: "Re: Great progress!"
    date: 2006-07-31
    body: "Not to worry: where is the patch? I think some of us will simply integrate it manually into the sources we'll use to upgrade to 3.5.4. :)"
    author: "A.C."
  - subject: "Re: Great progress!"
    date: 2006-07-31
    body: "Patch 1: http://websvn.kde.org/branches/KDE/3.5/kdebase/nsplugins/viewer/nsplugin.cpp?rev=565998&r1=565997&r2=565998&makepatch=1&diff_format=h\nPatch 2: http://websvn.kde.org/branches/KDE/3.5/kdebase/nsplugins/viewer/nsplugin.h?rev=565998&r1=565997&r2=565998&makepatch=1&diff_format=h\n\nCheers."
    author: "NabLa"
  - subject: "Re: Great progress!"
    date: 2006-07-31
    body: "Note: Gentoo already patched their 3.5.3 package in their development tree."
    author: "Eike Hein"
  - subject: "Re: Great progress!"
    date: 2006-08-01
    body: "Hmm.  I can't get konqueror to play youtube stuff on youtube's own site.  Is there a trick to it?  Do I need a konqi version more recent than 3.5.2?"
    author: "Lee"
  - subject: "Re: Great progress!"
    date: 2006-08-01
    body: "Does it also fix the problem with embedded video in the BBC's website? That's the only significant annoyance I have with konq at the moment."
    author: "Adrian Baugh"
  - subject: "Re: Great progress!"
    date: 2006-08-02
    body: "Apparently that doesn't work because konqueror doesn't support scripted plugins, or something.\n\nThe only way I've found to play BBC videos, is using Real Player (ugh) in firefox. And then I can't find a way to maximise the window. At least it is pretty good quality and the sound is in sync though, unlike that piece of crap flash player."
    author: "Tim"
  - subject: "knm vs. knemo"
    date: 2006-07-30
    body: "How does KNetworkmanager compare to the KNeMo referenced in the digest? They look like they intersect a lot?"
    author: "me"
  - subject: "Re: knm vs. knemo"
    date: 2006-07-31
    body: "I have KNemo running as a data plotter and statistics tool regarding up-/download and information regarding the own IP. (Daily/weekly/monthly total reports of up-/download are still missing I think). You can't change your IP settings with it.\n\nDon't know about KNetworkManager, but an ideal tool for managing networks in KDE should probably have these statistics and plotting functions included."
    author: "Anonymous"
  - subject: "Thanks!"
    date: 2006-07-31
    body: "Danny, thanks for the commit-digest. Your work is greatly appreciated!!!"
    author: "MK"
  - subject: "Stuhl"
    date: 2006-07-31
    body: "Benjamin K. Stuhl - what was wrong about the name?"
    author: "hill"
  - subject: "dreaming about kde "
    date: 2006-07-31
    body: "it rather a surrealistic situation; I am here nowhere in SAHARA; reading commit digest hoping to hear something about kde3.5.5; \n\ndefinitifely I love the internet"
    author: "morphado"
---
In <a href="http://commit-digest.org/issues/2006-07-30/">this week's KDE Commit-Digest</a>: Work begins on integrating <a href="http://en.wikipedia.org/wiki/C_Sharp">C#</a> support in <a href="http://www.kdevelop.org/">KDevelop</a>, as the second phase of the "C# parser for KDevelop" <a href="http://developer.kde.org/summerofcode/soc2006.html">Summer Of Code</a> project, whilst a companion effort concurrently starts to support <a href="http://en.wikipedia.org/wiki/Java_programming_language">Java</a>. Eigen, a matrix and vector mathematics library is begun. <a href="http://okular.org/">okular</a> is ported to QGraphicsView. Infrastructure improvements in <a href="http://solid.kde.org/">Solid</a> and <a href="http://edu.kde.org/kalzium/">Kalzium</a>. "Siox" tool ported to <a href="http://www.koffice.org/krita/">Krita</a>.

<!--break-->
