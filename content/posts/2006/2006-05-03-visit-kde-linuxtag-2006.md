---
title: "Visit KDE at LinuxTag 2006"
date:    2006-05-03
authors:
  - "jriddell"
slug:    visit-kde-linuxtag-2006
comments:
  - subject: "curiosity"
    date: 2006-05-03
    body: "I am curious; I am currently in neu-isenburg (BTW, nice area), but leaving friday morning. Where is the actual linuxtag at?  and how long does it run for?\n"
    author: "a.c."
  - subject: "Re: curiosity"
    date: 2006-05-03
    body: "It's in Wiesbaden and it runs until Saturday. Wiesbaden is about 20 mi from Neu-Isenburg west of Frankfurt."
    author: "d.c."
  - subject: "Re: curiosity"
    date: 2006-05-03
    body: "day only, or at night as well?\n"
    author: "a.c."
  - subject: "Re: curiosity"
    date: 2006-05-04
    body: "From what I see at http://www.linuxtag.org/2006/de/besucher/termine-tickets.html, there are daily talkings from 10:00 to 18:00.\n"
    author: "adac"
  - subject: "Re: curiosity"
    date: 2006-11-29
    body: "Hey I was there it was great exebition and I knew something new about linux :)"
    author: "zloy"
  - subject: "Re: curiosity"
    date: 2006-05-03
    body: "BTW, thank you for the info."
    author: "a.c."
  - subject: "Slides of the amaroK talk"
    date: 2006-05-04
    body: "I was mainly showing amaroK in action, but there were also some slides, located at http://www.kde.org/kdeslides/ (they are in German). Check out the last few ones... ;-)"
    author: "Sven Krohlas"
  - subject: "KDE features in SUSE 10.1"
    date: 2006-05-04
    body: "Stephan Binner and myself will be presenting the new features in SUSE 10.1 during the 10.1 presentation at 1330 in Room 6.2 on Thursday.  Expect to see Kerry, the KDE Beagle client; KNetworkManager, allowing easy configuration of wireless networks for laptops; KPowerSave, the no-fuss way to suspend to disk and configure your CPU throttling; and all the rest of the KDE goodness found on the OpenSUSE desktop."
    author: "Bille"
  - subject: "Re: KDE features in SUSE 10.1"
    date: 2006-05-04
    body: "Suse has been shipping with KPowersave for more than a year. Is this really a new feature?\n(But I just learned that Fedora Core 5 still does not include it - are they living behind the moon?)"
    author: "Hans"
  - subject: "build"
    date: 2006-05-04
    body: "What is the purpose of the build service and how does it work?"
    author: "Gerd"
  - subject: "Re: build"
    date: 2006-05-06
    body: "Just from reading the site, its for making binary packages for different distros and platforms easy.\n\nNot sure how it will work."
    author: "Ian Monroe"
---
Germany's largest Free Software exhibition <a href="http://www.linuxtag.org/2006/">LinuxTag</a> has opened today and runs until Saturday at <a href="http://en.wikipedia.org/wiki/Wiesbaden">Wiesbaden</a> near Frankfurt.  KDE is exhibiting on booth number 937a and will be showing off our plans for KDE 4 plus what's new in KDE 3.5 and KOffice 1.5.  KDE related talks include Ariya Hidayat on KOffice, Sven Krohlas on amaroK and a workshop by Torsten Rahn and Daniel Molkentin on Qt 4.  Elsewhere at the exhibition <a href="http://openusability.org/forum/forum.php?forum_id=855">OpenUsability</a> will talk about how the work with KDE, openSUSE will show off SUSE Linux 10.1 and their new open <a href="http://en.opensuse.org/Build_Service">build service</a>, The Federal Office for Security in Information Technology (BSI) will demonstrate its KDE based desktop ERPOSS 4 and <a href="https://lists.ubuntu.com/archives/ubuntu-announce/2006-April/000071.html">Mark Shuttleworth</a> will meet with KDE developers to talk about the future of Kubuntu.



<!--break-->
