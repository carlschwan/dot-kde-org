---
title: "Help KDE at LinuxWorld London"
date:    2006-08-07
authors:
  - "gwright"
slug:    help-kde-linuxworld-london
---
Today we received confirmation that we have been successful in obtaining a booth at LinuxWorld Conference and Expo. The two-day event is taking place on the 25th and 26th of October in London's Olympia 2 conference hall.  Following last year's success we are hoping to have a bigger presence this year to demonstrate our efforts at Akademy 2006 as well as the progress that will have been made with KDE 4. Anyone interested in helping out at the KDE booth should please contact <a href="http://lists.quaker.eu.org/mailman/listinfo/kde-gb">the kde-gb list</a> for further details.



<!--break-->

