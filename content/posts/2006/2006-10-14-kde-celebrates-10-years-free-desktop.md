---
title: "KDE Celebrates 10 Years of the Free Desktop"
date:    2006-10-14
authors:
  - "jriddell"
slug:    kde-celebrates-10-years-free-desktop
comments:
  - subject: "Congrats Konqi!"
    date: 2006-10-14
    body: "Nice party, I hope more countries had parties for themselves.\n\nI'm not sure if its a trend, but the stuffed konqies I've seen over the years tend to get more fluffy and basically younger looking. This one on the cake is just cute and looks like a dragon of 10 years! (assuming they get to live at least to a hundred)\n\nNew marketing slogan?  \"We just keep on looking younger!\""
    author: "Thomas Zander"
  - subject: "10 years of nice desktop!"
    date: 2006-10-14
    body: "This reminds me when first time I tried Linux... KDE 1.x was the only desktop  used, and since that I always use KDE as my desktop, KDE 2.x, and all of KDE 3 series plus now KDE 4 from svn. In 10 years we have already seen this project growing in every aspect. Wish all the best for the project and also for all great developers, contributors, artists and all people involved. Thanks for all your work!\n\nFrom forever fan of KDE."
    author: "fredy"
  - subject: "Congratulations!"
    date: 2006-10-14
    body: "I just want to say Happy Anniversary for all involved in KDE, and a big 'Thank You' to all who have made KDE desktop possible. You guys rock!\n\nA proud KDE user, of course :)"
    author: "Derek R."
  - subject: "Happy 10 years KDE and Konqui"
    date: 2006-10-14
    body: "It's also my personal 10 years of linux this year. \nA few months ago in fact."
    author: "JC"
  - subject: "Ten years already?"
    date: 2006-10-14
    body: "Oh, dear... Do I feel old? Yes, I do. I read the original announcement and wondered what the fuss was about... I mean, nobody would ever need more than fvwm1 and a couple of xterms, right? And then -- the system requirements were insane! No way the first verison of KDE would run on my 8MB 486 workstation!\n\nLittle did I know..."
    author: "Boudewijn Rempt"
  - subject: "My photos"
    date: 2006-10-14
    body: "If you are interested, here are some of my photos from celebration:\nhttp://www.unix-ag.uni-kl.de/~fischer/10yearsKDE/"
    author: "Thomas Fischer"
  - subject: "Re: My photos"
    date: 2006-10-14
    body: "Scary: http://www.unix-ag.uni-kl.de/~fischer/10yearsKDE/10yearsKDE_204.jpg"
    author: "Anonymous"
  - subject: "Re: My photos"
    date: 2006-10-14
    body: "Disclaimer: No, although I look really devilish / devourish on that photo, there was absolutely no harm being done to any small green marzipan dragons at the party. Actually in that picture I successfully saved his life by moving Konqi carefully with the fork from the cake to the plate. \nNobody was actually gutsy enough to eat him in the end (not even Matthias -- imagine that picture: some even more scary ritual where the founder has got to eat his mascot! A really frightening thought!). So no, the marzipan dragon was not eaten in the end. \n"
    author: "Tackat"
  - subject: "Re: My photos"
    date: 2006-10-15
    body: "Thanks. It was nice to see friends, people I've met and those I've yet to meet there. It wasn't exactly convenient to hop another trans-Atlantic flight to be there but I would have loved to. As it happens it falls on my wedding anniversary so I told my wife \"happy KDE day\" this morning. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: My photos"
    date: 2006-10-17
    body: "Hey, look at these faces:\n\nMosfet has the biggest picture, where is he now?\nBefore the dot, his news site was the only KDE news site.\n"
    author: "reihal"
  - subject: "KDE -- you are the best!"
    date: 2006-10-14
    body: "Happy Birthday KDE! I love you!\n\nWhere I would like to see you improve -- Please, Please Please implement support for OSX style application bundles -- it would make it so much easier for me to make distributable packages for you if you would support them.\n\nDon't pay attention to the clueless dorks who are suggesting that you should switch everything from C++ to something else, use GTK libs or reimplement everything to be a GUI front end to CLI commands -- those are incredibly bad ideas. You are pretty much perfect the way you are (especially on openSUSE). Next steps should be to make yourself more accessible for non-core Qt/KDE developers. Adding support for a flexible packaging scheme like the bundles used on OSX would go a long way towards that.\n\nPaul."
    author: "Paul Koshevoy"
  - subject: "Re: KDE -- you are the best!"
    date: 2006-10-14
    body: "Do you mean klik?: http://klik.atekon.de/"
    author: "Philip Rodrigues"
  - subject: "Re: KDE -- you are the best!"
    date: 2006-10-15
    body: "Klik seems about right -- but until the support for klik packages is integrated at the core of KDE it's still useless to me, because I can't count on every possible user having klik installed.\n\nHonestly, klik seems like an overkill -- a simple agreed upon .app directory layout with support for Info.plist and resources (icons, etc..) would have been enough. I mean, what's the server for? Why is mounting required?\n\nIs it possible to specify supported file/document types that a particular application package can handle? Is it possible include icons, templates, localization files and other resources into a klik package, and will they be automatically recognized by KDE? Can I add a klik package to the KDE applications menu?\n\nThat's the sort of integration that is necessary.\n\n    Paul.\n"
    author: "Paul Koshevoy"
  - subject: "Re: KDE -- you are the best!"
    date: 2006-10-15
    body: "what's the server for? \n\nis not really neccesary, just nice to have a lot of software available in one click.\n\n\nWhy is mounting required?\n\nthat's how MacOS X does it.\n\nKlik is one package in one file.\nTo run the software, the file is mounted just like an iso-image, making it's content available.\n\n\n  Is it possible to specify supported file/document types that a particular application package can handle? \n\nyes\n\nIs it possible include icons, templates, localization files and other resources into a klik package, and will they be automatically recognized by KDE? \n\nyes\n\nCan I add a klik package to the KDE applications menu?\n\nyes\nin my experience, the klik packages were automagicly added to the kde menu, no additional configuration required..\n"
    author: "AC"
  - subject: "Re: KDE -- you are the best!"
    date: 2006-10-15
    body: "Cool, although I think you are mistaken about OSX mounting the .app bundles. It can mount .dmg images that may contain bundles inside of them, but the .app directories are not mounted. All it does is look inside the .app directory in predefined places for the info file, the executable, the frameworks and resources.\n\nBTW, if KDE4 will be ported to OSX natively then support for .app bundles will be necessary anyway in order to be able to launch native OSX applications from konqueror.\n\nPaul."
    author: "Paul Koshevoy"
  - subject: "Re: KDE -- you are the best!"
    date: 2006-10-15
    body: "OSX does mount .app bundles, you just don't notice it. it's the way they work. you do know osx generally doesn't 'install' apps like linux tools like rpm normally do, isn't it? well, the only other doable way is mounting it like klik does..."
    author: "superstoned"
  - subject: "Re: KDE -- you are the best!"
    date: 2006-10-15
    body: ">>OSX does mount .app bundles, you just don't notice it.\n\nThats a huge difference between linux and macos: linux shows everything it does, while macos hides it all."
    author: "AC"
  - subject: "Re: KDE -- you are the best!"
    date: 2006-10-15
    body: "No it doesn't mount them.\n\nLook, I am running a Terminal.app on my iMac right now, building Qt 4.2.0:\napple:~ paul$ ps auxwww | grep Termin\npaul       230   0.3  2.3   128912  12260  ??  S     9:23AM   1:48.03 /Applications/Utilities/Terminal.app/Contents/MacOS/Terminal -psn_0_3407873\n\nAnd here is the output of the mount command:\napple:~ paul$ mount\n/dev/disk0s5 on / (local, journaled)\ndevfs on /dev (local)\nfdesc on /dev (union)\n<volfs> on /.vol\nautomount -nsl [118] on /Network (automounted)\nautomount -fstab [129] on /automount/Servers (automounted)\nautomount -static [129] on /automount/static (automounted)\nhomestead:/home on /private/var/automount/home (automounted)\nhomestead:/VCR on /private/var/automount/nfs/VCR (automounted)\nhomestead:/usr/local/unsafe on /private/var/automount/nfs/unsafe (automounted)\n\nAs you can see the Terminal.app is not mounted\n\nPaul"
    author: "Paul Koshevoy"
  - subject: "Re: KDE -- you are the best!"
    date: 2006-10-16
    body: "Are your sure?\n\nRead this:\nhttp://www.stepwise.com/Articles/Technical/2001-03-29.01.html\n\n\"2. Next, you want to mount the image. However you do not want the system to be notified of the mount, so you must used the -nomount command.\"\n\nAs said earlier, MacOS hides the mounting of the image.\n"
    author: "AC"
  - subject: "Re: KDE -- you are the best!"
    date: 2006-10-16
    body: "You are confusing a .dmg file with a .app bundle. A .dmg file is a disk image (like a .iso), it can contain anything, not necesssarily a .app bundle. You can mount a .dmg image, but the applications that ship with OSX are not installed as .dmg images, but as straight .app bundles.\n\nPaul.\n"
    author: "Paul Koshevoy"
  - subject: "Re: KDE -- you are the best!"
    date: 2006-10-16
    body: "A single image file (the klik image) is generally far easier to move around than a directory with bunches of files in it.  With klik you would only have to move a single file around, but with a .app folder there isn't a practical way to distribute it over http or other file transfer protocols without putting it into an image file (say a .dmg).  Since the mounting/unmounting of klik images is done automagically by the klik association it really is totally transparent to the user, though hopefully in the future FUSE will remove the need to even edit the fstab file."
    author: "Corbin"
  - subject: "Re: KDE -- you are the best!"
    date: 2006-10-16
    body: "Also, OSX does show mounted .dmg files which you can see for yourself using the mount command any time you have double clicked on a .dmg file.\n\nPaul."
    author: "Paul Koshevoy"
  - subject: "klik, .dmg and .app"
    date: 2006-10-16
    body: "Paul,\n\nthe difference between .app and .dmg is minimal. Basically, .app (\"application directory\") is an extracted .dmg (or .dmg is a compressed archive of an .app directory structure). \n\nAs such, .app and its subdir structure does not need mounting (in this point you are absolutely right), it just needs to be there. A .dmg needs mounting precisely, because mounting lets it look like it is part of the complete file directory system (instead of a single file, which it is if un-mounted.) \n\nA klik bundle can easily be extracted, and then it simply becomes an .app-lik sub-directory structure, from where you can run the application without mounting.\n\nMore info about klik here:  http://klik.atekon.de/wiki/index.php/User's_FAQ\n\nA question to the KDE community: what happened to previous promises to integrate a klik-friendly client structure into KDE4's core? Ya know, things like support for automatic integration of klik app images into the K menu (and their removal if a .cmg is deleted), display of app-specific icons that are glue-ed to the klik .cmg file, and more goodies?"
    author: "AC"
  - subject: "Re: klik, .dmg and .app"
    date: 2006-10-17
    body: "In the time honoured fashion of FOSS - what's stopping ya? You want it done - go to it!"
    author: "anon y mouse"
  - subject: "CFBundles and NSBundles are folders"
    date: 2007-01-04
    body: ".app bundles are just folders in OS X\nI have made them by hand before, so I know.  just like any other CFBundle object (carbon) or NSBundle object (cocoa) they are just a series of folders, and I think linux could learn from apple on mach-o bundles.  if kde used them, no more need for that huge /usr/share/apps folder of the /usr/share/applnk folder either.  here is the format of a CFBundle:\n\nKonqueror.App\n  Contents\n    Info.plist\n    MacOS\n      konqueror\n    Resources\n      browser-window.nib\n      toolbar-customizer-window.nib\n      ...\n\nmy point, something like that does not even need to be mounted, it can still be installed the linux way, and anywhere you want it, usually in the /Applications folder though.  If anything got mounted other than the .dmg, then you could never unmount the .dmg without unmounting what the .dmg is using, same rule applies to Mac OS classic (9.2.2 and below).  another thing, anything that is supermounted, appears on the desktop, btw, darwin tends to supermount all volumes in the /Volumes folder, which isn't that different than HAL on linux 2.6 is doing.  I plan on doing research on the apple interface builder format (.nib files) and write knib which can make, modify, and view nib files, also if you havent known yet, the code for CFBundle is in CoreFoundation Lite, thus we can use bundles on linux, I plan to learn objective c and make a cocoa style wrapper for kde sometime that wraps around CF-Lite, btw, I am ready to make a slackware 11 packageand an rpm and a deb, I have the compatibility headers too, the ones that are needed to build cf-lite, and I have a successful build of cf-299.33, even better than on apple's site, I not only have libCoreFoundation.a, but also libCoreFoundation_debug.a and libCoreFoundation_profile.a too, and the headers are all set up, even including AssertMacros.h, AvailabilityMacros.h, and TargetConditionals.h, from darwin 8.0, intel.  yes, CF built nicely.  I have even tested it on gentoo, in order to link with it, you need to add these -L/usr/include/CoreFoundation -L/usr/include -I/usr/include/CoreFoundation -I/usr/include -lpthread -lm -lCoreFoundation to gcc while building a program or library linked against libCoreFoundation, note: these instructions are for working with my build only, the apple build stores the stuff in /tmp/CoreFoundation.dst/usr/local instead of /usr, so make adjustments accordingly if you choose to build it yourself."
    author: "kram32768"
  - subject: "Congraturation! KDE 10th anniversary!"
    date: 2006-10-15
    body: "Korea KDE Team, we also had the party to celebrate KDE 10th anniversary. :)\n\nI wish KDE that would be present to the world's people.\n\nOur photo is in http://socmaster.homelinux.org/~jachin/57, my personal blog.\n\n(Our team site also will be renewal.)"
    author: "Cho Sung-Jae"
  - subject: "Re: Congraturation! KDE 10th anniversary!"
    date: 2006-10-15
    body: "That is soooo way cool :-) Could you please blog about it (if possible in english as my korean knowledge is rather non-existing ;-) on planetkde.org ?\n\nBTW: What happened to the kde.es party? "
    author: "Tackat"
  - subject: "Congrats ! and well done !"
    date: 2006-10-15
    body: "I am proud of you and KDE, a successful non-anglo-american FOSS project.\nKeep the good work up !\n\nMy wish for the next 10 years ? \nFirstly, that I can make it my preferred desktop when KDE offers non-saturated colours and quality icons.\nSecondly, that it offers non-cluttered menus (eventually levels of desire to tweak the system). I should use Gnome, then ? I could, but KDE is technically the better alternative.\nThirdly, drop Konqueror and integrate Firefox/IceWeasel into KDE.\n\nThanks again, and looking forward to the next 10 years,\n\n"
    author: "Uwe"
  - subject: "Re: Congrats ! and well done !"
    date: 2006-10-15
    body: "why do people want KDE to drop the best free software rendering engine there is, and adopt a far slower, more bloated, harder to mantain and less complete rendering engine??? firefox and OO.o should be burned to the ground, and be forgotten as soon as possible. both share the same problems, created by their history. they where proprietary apps, developed by a few and just for a few people. not modular, clean, transparant and wel-documented like most (no, of course not all) free software. enormous amounts of money have already been spend on cleaning them up, and still they lag behind free-software-from-the-beginning like khtml and Koffice.\n\nfrom every 10 bucks you spend on OO.o, 9 go into 'shit how and where does it do this' instead of usefull development.\n\nin free software, darwinism plays a big role. and darwinism mosly favors the BEST. sorry OO.o, sorry firefox/gecko."
    author: "superstoned"
  - subject: "Re: Congrats ! and well done !"
    date: 2006-10-15
    body: "Drop Konqueror? Bill, is that you?"
    author: "Erik"
  - subject: "Re: Congrats ! and well done !"
    date: 2006-10-16
    body: "> I am proud of you and KDE, a successful non-anglo-american FOSS project.\n\nKDE is an international project, as is evidenced by the comments here. It's about bringing people together. I live in the US and my software is part of the KDE release packages. I would not want to think I wasn't part of KDE or that it wasn't successful here. There are key people in KDE living in North America and I'm proud that we are part of the unifying thinking of KDE of providing great software to all people. When we are celebrating KDE we should not be divisive. KDE is about bringing all people together for a common goal and not about excluding people for petty bias."
    author: "Eric Laffoon"
  - subject: "Congratulations!"
    date: 2006-10-15
    body: "Congratulations to the KDE team! \n\nKDE 3.5.x is the best! B)"
    author: "Darkelve"
  - subject: "Nice laptop"
    date: 2006-10-15
    body: "no words need to be added."
    author: "Marc Driftmeyer"
  - subject: "Simply... amazing..."
    date: 2006-10-16
    body: "Hey team, \n\nHappy 10th birthday. Thank you,no really, THANK YOU, for such a great desktop enviroment. I can't wait for the day when I can make my contribution to this stable, beautiful, ever-evolving desktop without which i (literally) gag. \n\nCongratulations again."
    author: "George Ssali"
  - subject: "Party photos"
    date: 2006-10-16
    body: "I took a few more photos at the party in the evening:\n\nhttp://www.flickr.com/photos/wstephenson/sets/72157594331305112/"
    author: "Bille"
  - subject: ":D congratulations!"
    date: 2006-11-02
    body: "long live KDE!  woohooo!  keep up the great work on the best Linux desktop environment!  you rawk dudes! :D"
    author: "zero1"
  - subject: "Happy Birthday KDE!"
    date: 2006-11-09
    body: "Thanks to all the developers out there working on KDE and the Linux community for their work and unifying people and machines worldwide.  "
    author: "Tony"
  - subject: "Very good job! !You are the best!!"
    date: 2006-12-11
    body: "Hi! Team,\n\nHappy 10th birthday!!Very good job!!You are the best!!\nThanks\nRoberto"
    author: "Roberto"
  - subject: "Felicitaciones KDE"
    date: 2007-02-05
    body: "Os envio las primeras felicitaciones en espa\u00f1ol que les hestaba faltando. Los felicito por estos 10 a\u00f1oz y estamos para ayudarles no se olviden. Suerte!!!!"
    author: "Tomas"
  - subject: "Cool KDE just never stops getting better!"
    date: 2007-02-22
    body: "I say that \"KDE Rocks\" and the 10th anniversary is a wonderful thing.  I'd love to contribute when I sharpen my programming skills!"
    author: "Will Castro"
  - subject: "a Happy birthday to you from egypt"
    date: 2007-03-03
    body: "if KDE wasn't born i wont be using Linux right now, i would be paying $$$ for buggy feature-less os and another $$$$ for its feature-less applications\ni knew Linux through KDE and that's how i am sticking with it\nhappy birthday and \"3okbal me2at sana\"=\"wish you 100 years\" as we say it in Arabic  :)\n10000 roses to the KDE community, i wish i can contribute with more than words :("
    author: "Hossam"
  - subject: "Sto Lat! (Polish birthday song)"
    date: 2007-08-17
    body: "Sto lat, sto lat, Niech &#380;yje, &#380;yje nam.\nSto lat, sto lat, Niech &#380;yje, &#380;yje nam,\nJeszcze raz, jeszcze raz, niech &#380;yje, &#380;yje nam,\nNiech &#380;yje nam!\n\n:-D\n\n--\nBMO"
    author: "Boyle M. Owl"
---
Yesterday at 10:00 AM the president of the KDE e.V. Eva Brucherseifer welcomed the audience of the presentation track at the KDE anniversary event at the <a href="http://www.tae.de">Technische Akademie Esslingen</a> (TAE) in Ostfildern near Stuttgart, Germany. Keynote speakers were <a href="http://en.wikipedia.org/wiki/Matthias_Ettrich">Matthias Ettrich</a>, founder of the KDE project, as well as <a href="http://en.wikipedia.org/wiki/Klaus_Knopper">Klaus Knopper</a> of Knoppix fame.  During their presentations they looked back at KDE's successful past 10 years and they offered their thoughts about the future of KDE and <a href=http://www.fsf-europe.org>Free Software</a>.






<!--break-->
<img style="float:right" src="http://static.kdenews.org/jr/10yearskde.jpg" width="273" height="184" />
<p>Jono Bacon, Canonical's community manager of Ubuntu / <a href="http://www.kubuntu.org/">Kubuntu</a>, congratulated KDE with his own presentation about Kubuntu and KDE. Jan Mühlig from <a href="http://www.relevantive.de/">Relevantive</a> and Daniel Molkentin, KDE e.V. talked about Usability and KDE 4. In the afternoon the audience of the presentation track met for a group photo and celebrated the event with sparkling wine and a big birthday cake. Even our Konqi mascot of the project was present - this time made of marzipan, right on the top of the birthday cake.</p>

<a href="http://developer.kde.org/~tackat/kde10party/kde10years1.jpg"><img style="float:right" src="http://developer.kde.org/~tackat/kde10party/kde10years1-thumb.jpg" class="showonplanet" /></a>

<p>Right after the afternoon break the presentation track continued with a speech from Heinz-M. Gräsing from the city of Treuchtlingen who gave some insights about the successful migration of <a href=http://www.treuchtlingen.de/>Treuchtlingen</a> to KDE.</p>

<p>The presentation part of the anniversary was concluded by Knut Yrvin, community manager at Trolltech, who surveyed the KDE 10 Years raffle together with Eva Brucherseifer. As prizes Trolltech offered a <a href=http://www.qtopiagreenphone.com/>Qtopia Greenphone</a> and <a href=https://www.opensourcepress.de/>Open Source Press</a> offered Daniel Molkentin's <a href="http://www.qt4-buch.de">new book about Qt4 programming</a>.</p>

<a href="http://developer.kde.org/~tackat/kde10party/kde10years2.jpg"><img style="float:right" src="http://developer.kde.org/~tackat/kde10party/kde10years2-thumb.jpg" class="showonplanet" /></a>
<p>Further KDE people arrived at the TAE in the evening when people met in the cottage for a delicious big "italian" dinner. We clinked glasses later at midnight when the date turned into the actual anniversary.</p>

<p>The KDE project would like to thank the speakers of the presentation track, the Technische Akademie Esslingen, <a href=http://www.trolltech.com>Trolltech</a> and Open Source Press for their support of the whole event. A special "thank you" goes to the confectioner who created that awesome marzipan dragon for the cake.  And of course we would like to thank all the people who were not able to attend the event and who sent their nice wishes to the KDE Project.</p>
<center><a href="http://developer.kde.org/~tackat/kde10party/kde10years3.jpg"><img  src="http://developer.kde.org/~tackat/kde10party/kde10years3-thumb.jpg" class="showonplanet" /></a></center>

