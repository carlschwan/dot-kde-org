---
title: "Q & A About The KDE Usability Project"
date:    2006-03-01
authors:
  - "cpaul"
slug:    q-about-kde-usability-project
comments:
  - subject: "OT: 'usability' hype"
    date: 2006-02-28
    body: "Looks like Gnome 'usability' flamewar worked fine. Already 9 out of 10 feeds posted to planetsuse.org are G related :/. Sigh. So much for Linux on the desktop. Vista is coming and G is not an answer ( not that KDE would be either, but it would have a shot with smaller investment )."
    author: "Anonymous Coward"
  - subject: "Re: OT: 'usability' hype"
    date: 2006-02-28
    body: "well, not sure about this. \n\ngnome might very well be better than Vista in a 2 year timeframe, but it would take a lot of money. i'm quite sure if the companies supporting gnome would put half that money in KDE, the next big release (in a little over a year i guess) will kick vista's ass anytime. weird thing is, they seem to know it - except those that are responsible for the decision... "
    author: "superstoned"
  - subject: "Re: OT: 'usability' hype"
    date: 2006-02-28
    body: "> weird thing is, they seem to know it - except those that are responsible for the decision...\n\nThey all know it, but corporations are not interested and not expcted to be interested in technology, they are interested in control. Novell particularly is interested in Mono, and this is why:\n\n\"If you want to contribute source code to Mono you have to sign a copyright assignment giving Novell the right to relicense the code under other licensing terms, thus preserving Novell's ability under the dual license to commerically license Mono.\"\n\n(according to Wikipedia)\n\nThis is similar to Qt's dual licensing, but not the same. Qt Free Edition is guaranteed to always be the real thing, but Mono can always be replaced/surpassed by a more powerful proprietary offer from Novell. \n\nNow if Novell invested in KDE, it could not\n\n_sell a proprietary version of kdelibs_,\n\nwhich means it could not\n\n_control a platform_.\n\nWhich is why Novell will probably never be persuaded to (heavily) invest in KDE. Sure, some of their employees might dislike/hate KDE, but corporate politics doesn't always depend on some employees - they depend on the 'need' to control a platform (or more of them).\n\nI hope this makes things a bit clearer (although I also hope that I'm wrong).\n\n\n"
    author: "mihnea"
  - subject: "Re: OT: 'usability' hype"
    date: 2006-03-02
    body: "sorry, but this is just bullshit.\n\n>This is similar to Qt's dual licensing, but not the same. Qt Free Edition is guaranteed to always be the real thing, but Mono can always be replaced/surpassed by a more powerful proprietary offer from Novell. \n\nQt and Mono facing exactly the same situation.\n\nQt: Trolltech can theoretically decide to drop the Free Qt. What will happen? The last free version will have to be released under a BSD-style license so Qt will be available for free and non-free software.\n\nMono: Novell can theoretically decide to drop the free Mono. But the community can still keep the last free version and go on with it. Mono doesn't need a contract to release a last free version under another license like Qt because Mono is already licensed for the use for free and non-free software.\n\nSo whether Trolltech or Novell become \"evil\" in both cases the community will keep a framework which can be used for free and non-free software."
    author: "pinky"
  - subject: "Re: OT: 'usability' hype"
    date: 2006-03-02
    body: "And the last Qt version would still be GPL too"
    author: "Nicolas Goutte"
  - subject: "Re: OT: 'usability' hype"
    date: 2006-03-02
    body: "\n> So whether Trolltech or Novell become \"evil\" in both cases the community will keep a framework which can be used for free and non-free software.\n\nAgreed (with some details I won't go into). But the whole point was that corporations are not interested in technology, so don't expect them to support KDE just because it has great technology. This is not \"just bullshit\", it's a matter of lucidity, and of not wasting efforts (and peace of mind).\n"
    author: "mihnea"
  - subject: "Re: OT: 'usability' hype"
    date: 2006-02-28
    body: "a) the usability jihad had a much different/lesser effect on these sorts of decisions than many people seem to think\n\nb) people in the gnome project blog more than people in kde (visit the respective planets) and this goes trebly (or more) so for the kde people who work at novell. so comparing numbers of blog posts really just points to something we all already knew: gnome participants blog more.\n\nbut what really astounds me to no end is how a product that isn't even released yet (MS vista) has people declaring the \"war\" over. when we see increased adoption of the open source desktop, this boggles me. when kde continues to improve at a rapid clip, this boggles me. when microsoft *still* won't have key attributes we have (you know, like vendor neutrality, open formats, open source, works well with others) and we're catching up on the attributes that have won them accolades (works with commodity hardware, easy enough for average joes, reads proprietary formats, works with proprietary protocols) and are these days working on issues (better eyecandy) around the same time they are (instead of being N years behind the lead edge), this REALLY boggles me.\n\nthere are two issues that have led us to this point in my mind: explicitly aiming for the enterprise and expecting a \"year of the desktop\".\n\ni used to think of things in terms of winning the enterprise as well. but you know what? linus is right: take care of the open source project, the enterprise will take care of itself. right now we're having the most impact in schools, government and the SMB/SME spaces (particularly the latter). we'll grow \"upwards\", just as the server did, into the enterprise space. we have a harder row to hoe than on the server side, but we're making gains and winning the minds and hearts of more and more users every single day. some of those users come in packs of 5,000 or 10,000+ even, but the vast majority don't. if we paid better attention to those people we service best today, we could service their needs perfectly. best yet: most of us working on the desktop have much more in common with those people and know many more of them (so we have a better frame of reference from which to work).\n\nas for the year of the desktop, that's bullocks. when the year of the desktop happens, it'll simply mean that we succeeded several years prior. it's not a year of the desktop, it's a decade and a half (or more) of the desktop. it's a long, hard haul from the first open source desktop systems 9 years ago to our millions of users today to having double digit market share in the years to come. in both the business and music industry world they often say that all overnight successes are years of effort in the making. we're no different."
    author: "Aaron J. Seigo"
  - subject: "Re: OT: 'usability' hype"
    date: 2006-03-01
    body: "Aaron,\n\n> but what really astounds me to no end is how a product that isn't even released yet (MS vista) has people declaring the \"war\"\n\nWar is not over, but we need *money* to make the last push. Writing software is fun and all (I love it), but maintaining and squeezing out the last bugs isn't. It needs money no matter if we're talking about kernel, KDE or GNOME. It's kinda funny that KDE would need least of it, yet it's hardly getting any :/"
    author: "Anonymous Coward"
  - subject: "Re: OT: 'usability' hype"
    date: 2006-03-01
    body: "We have a couple of betas of Vista. It looks more of a slow shell. All the razzle and dazzle is superficial and probably much work-underneth is being finished currently.\nIt is well known that Win has a lot of crap under the polished shell and they would need much time to clean that up.\nKDE also accumulated a lot of bloat and that need to be cleared also. However in the current state it is quite fast and efficient than Vista. Many usability considerations need be introduced but what is important is to get the whole infrastructure a plug-n-play system (Not only Hardware)."
    author: "Manik Chand"
  - subject: "Re: OT: 'usability' hype"
    date: 2006-03-01
    body: "> but we need *money* to make the last push\n\nyes, allowing people to concentrate on things full time can certainly help a lot. \n\nof course, it's not a magic bullet, however. there are projects out there that received 7 figures and never produced much to show for that. it needs planning, execution, yadda yadda. all the obvious stuff =) </disclaimer>\n\nthere isn't a lot of money being made on open source desktop stuff right now. there is *some* but not a lot. and so funding is pretty scarce as well, no matter where you look. it is better than it was, but we're certainly not on the gravy train yet ;) i think there is room for some innovative business models that target non-enterprise markets that could help bring cash flow into the desktop."
    author: "Aaron J. Seigo"
  - subject: "Re: OT: 'usability' hype"
    date: 2006-03-01
    body: "\"i think there is room for some innovative business models that target non-enterprise markets that could help bring cash flow into the desktop.\"\n\nHmmm. Like what?"
    author: "Segedunum"
  - subject: "Re: OT: 'usability' hype"
    date: 2006-03-02
    body: "Schools, universities, administrations?"
    author: "reihal"
  - subject: "Re: OT: 'usability' hype"
    date: 2006-03-02
    body: "\"Schools, universities, administrations?\"\n\nThat's not a businbess model - just possible, potential customers. However, customers and revenue bring up interesting issues.\n\nWith what will revenue be brought in and who is going to use and keep that revenue? The distributors, or KDE or other open source projects?"
    author: "Segedunum"
  - subject: "Re: OT: 'usability' hype"
    date: 2006-03-01
    body: "Aaron,\n\nYou've got a lot of good points, and that was a super good read, and it made me feel better about things as a whole, so thank you. :)\n\nI still sort of fear that KDE is likely to become the underdog to a mostly GNOMEified world in the coming years, because a sufficient amount of marketing and industry ties trump even the greatest tech, but even in such a worst-case scenario, thanks to people like you, KDE would be the freaking BEST underdog the world has ever known.\n\nSo thanks, Aaron. :)"
    author: "A.C."
  - subject: "Re: OT: 'usability' hype"
    date: 2006-03-01
    body: "1. Novell hasn't got a cat in hell's chance of achieving anything with a lot of the software they are using. It's about control, as someone above has pointed out. Many of Novell's people felt a bit cheated when they bought Ximian because they thought they'd bought a nice shiny version of .Net. They hadn't. It was just an open source project they could have used anyway, but as the principal contributor they are in a position to have you sign away copyright to them for the code you contribute.\n\n2. Novell's Gnome people blog more, as Aaron has pointed out. In layman's speak that means they like to mouth off about nothing more. On the other hand, Suse's people just don't blog enough to the point where people assume they're doing nothing.\n\n3. Vista is a Windows XP shell with 3D graphics. I work for a company that has various MSDN licenses, and I get the Vista releases. There is no WinFS, nothing terribly different. If you don't believe me, read the press releases for Windows 98, Windows 2000, Windows XP and then Windows Vista and tell me the difference. There isn't any."
    author: "Segedunum"
  - subject: "What to do?"
    date: 2006-03-03
    body: "I think it will be important to link usability and quality discussions. There are lot of quality issues which lead to inconsistencies or bad naming/labels.\n\nFrom a usability perspective I would suggest to keep the number of applications low. E.g. I see no reason to keep the control center as a seperate application in \"KDE standard\". We can access the control center plugins just via Konqueror. And we could provide html like navigation which can be changed much easier. Usability problems of the web are usually a non-issue.\n\nSome dublication of shipped applications like KDE editors is unnessary and clutters KDE.\n\nPopup screens are bad in general. The Firefox search function shows how it is done more user convinient.\n\nError messages can and shall be improved, e.g for devices not plugged in."
    author: "Gerd"
---
The KDE Usability Project <a href="http://usability.kde.org/activity/reports/2005reports.php">reports</a> I posted last week received a lot of feedback.  There were several motivations for posting the reports, and I think it was a success.  They are a useful reference for developers and other usability specialists as well as provides a public appearance for what the project does. Some of the feedback included questions about the project and reports. So, as webmaster of <a href="http://usability.kde.org/">usability.kde.org</a> and a project representative, let me answer some of these questions for you.

<!--break-->
<p><b>Is this all the (or kind of) work the project has produced over the past year?</b></p>

<p>No, and this is a two part answer.  </p>
<ol>
<li>This just happens to be a collection of reports which were sent to me to be linked on the website.  I have not excluded any reports sent to me, however I do expect any reports sent to me to be professionally executed, written and complete.  There is no formal submission process to who and what gets reviewed or tested, and the list of reports is not a complete archive of what we have produced.</li>

<li>Reports of usability tests are not the only type of work the project does.  A lot of our work involves talking and working with developers during the development lifecycle and not just testing/reviewing a project after it has been released.  Often, this is where the majority of our participation lies; working <i>with</i> the developers and not reporting to them <i>after</i> the software is built.  We also discuss solutions to current Qt implementation problems which could effect usability (and accessibility), are working on human interface guidelines (HIG) for KDE4, and a whole slew of other things.</li>
</ol>
<p><b>How does an application get submitted to the project for review?</b></p>

<p>There isn't really a 'submission' process.  In our neverending quest for better KDE usability, we often discuss projects, submit bugs, and contact developers when we find something which poses a problem to users.  Sometimes a project or developer will request a feature or menu be reviewed with recommendations on how to fix it and an interested usability specialist will look in to it.  Overall, some projects (and developers) are just more interested and involved in usability and those are the projects we typically work with the most.</p>

<p>OSS usability is very different than industry which is what makes it so special.  The relationship between the project/developers and the usability specialists is very important, and often make or break key usability improvements in a software.</p>

<p><b>How can I get a report posted on the KDE Usability website?</b></p>

<p><a href="mailto: celeste aat kde doot org">Send it to me!</a>  You do not have to be involved in the KDE Usability Project to submit a report (although it helps to know the project).  I ask that it is indeed a KDE application (and not other software which happens to be installed with a shortcut in the KMenu), is professionally executed and written, and completed within the past year (2005+).</p>

<p>Also, if you do have a report it is always a good idea to contact the project and provide them with the report than let them accidently find it on our website first.</p>

<p><b>What is the separation (or difference) between the KDE Usability Project and OpenUsability?</b></p>

<p>The KDE Usability Project is part of the <a href="http://ev.kde.org/">KDE e.V.</a> which is concerned with only KDE usability.  <a href="http://www.openusability.org/">OpenUsability.org</a> e.V. is a completely separate entity which is involved in any project who is interested in usability.  There just happens to be people who are very involved and active in both projects.</p>





