---
title: "KDE Commit-Digest for 30th April 2006"
date:    2006-04-30
authors:
  - "dallen"
slug:    kde-commit-digest-30th-april-2006
comments:
  - subject: "nice"
    date: 2006-04-30
    body: "Great work guys, looks like things are progressing nicely :D\n\nI'm looking forward to compiling the upcoming KDE 4 for the first time. I know it's not really sexy now, but still!"
    author: "superstoned"
  - subject: "This is the style we like"
    date: 2006-04-30
    body: "http://clarencedang.blogspot.com/2006/04/so-whats-up-with-kolourpaint-in-kde4.html\n\nThis is the style we like: developers talk about their difficulties."
    author: "humhum"
  - subject: "KODOMETER NO..!!!! Please, why you removed it..?"
    date: 2006-04-30
    body: "I can't believe this. Why this funny app was removed."
    author: "Dostoiski"
  - subject: "Re: KODOMETER NO..!!!! Please, why you removed it..?"
    date: 2006-04-30
    body: "You can still install it.  It just won't come with KDE by default anymore."
    author: "Michael Pyne"
  - subject: "Re: KODOMETER NO..!!!! Please, why you removed it."
    date: 2006-06-09
    body: "I wrote KODOMETER...\n\nI'm hurt.  I'm going with GNOME.\n"
    author: "Armen Nakashian"
  - subject: "Removal of Sync software"
    date: 2006-05-01
    body: "Does the removal of all the sync software from the main KDE mean that this is a lost cause? The state of synchronisation between KDE and mobile tools has been pretty bad for the last years (at least, I never *once* managed to decently sync any of my Zauruses or mobile phones, and I really did try...)\n"
    author: "Andr\u00e9 Somers"
  - subject: "Re: Removal of Sync software"
    date: 2006-05-01
    body: "I'm pretty sure that the sync software has been removed in favour of a <a href=\"http://www.opensync.org/\">OpenSync</a>-based solution for KDE.\n\nI'll try and find out more information for next week.\n\nDanny"
    author: "Danny Allen"
  - subject: "Re: Removal of Sync software"
    date: 2006-05-01
    body: "yeah thanks , thats because nearly 100% of our users own a mobile phone to sync with.\n\ni once managed to sync my t68 sony - ericsson with irmc connector over bluetooth with kde - that was really cool - i like to see something with SOLID and BLUETOOH. whats the status there ?\n\ncurrently SOLID ist only made with NETWORK(plug and play) and USB stuff .. ?\nWhats is the Status with Solid and Bluetooth ? (kde-bluetooth)"
    author: "ch"
  - subject: "Re: Removal of Sync software"
    date: 2006-05-01
    body: "Why not list this as an idea for the Google Summer of Code?\n\nhttp://wiki.kde.org/tiki-index.php?page=KDE%20Google%20SoC%202006%20ideas"
    author: "ac"
  - subject: "Re: Removal of Sync software"
    date: 2006-05-03
    body: "For KDE4, OpenSync is supposed to Solve All Your Problems, so you can complain there when it comes out. Remember, all this is in trunk for KDE4, so won't affect users for another six to ten months. KPilot is developed in a different SVN repository now (for KDE3); basically under the guise of \"there is so little manpower available to work on syncing that there's no way to do anything useful within KDE's release cycle.\""
    author: "Adriaan de Groot"
  - subject: "KDE Commit-Digest for 30th April 2006"
    date: 2006-09-17
    body: "Yes, there also seems to be a bit of an abandonment of bluetooth, too.\n\nWhich is a shame, since practically EVERY SINGLE regular KDE user has a mobile phone - even in countries where they don't have basic amenities. Let's hope OpenSync delivers on this, which it looks to do.\n\nAlso, KDE's transparent bluetooth support in conjunction with mobile phones and PDAs is absolutely a killer feature for our end users, and combined with transparent support of fish/ssh etc. which is a similar issue represents almost 100% of the reasons given for our customers using KDE instead of/as well as gnome.\n\nSyncing and Bluetooth are vital people draws that shouldn't be ignored."
    author: "Ed Black"
---
In <a href="http://commit-digest.org/issues/2006-04-30/">this week's KDE Commit-Digest</a>: optimisations in KDE startup services. <a href="http://amarok.kde.org">amaroK</a> gains CD ripping functionality through the audiocd:/ kioslave. In KDE 4 <a href="http://phonon.kde.org">Phonon</a>, the KDE 4 multimedia service sprouts the beginnings of a Xine backend. Reorganisation of the <a href="http://pim.kde.org/">KDE-PIM</a> module (<a href="http://pim.kde.org/components/kpilot.php">KPilot</a> and KMobile leave the module, respective destinations extragear/pim and playground/pim; <a href="http://kandy.kde.org/">Kandy</a>, <a href="http://handhelds.org/~zecke/kitchensync.html">KitchenSync</a> and MultiSynk also removed). <a href="http://www.kde.gr.jp/help/doc/kdetoys/doc/mouse/HTML/">KOdometer</a> removed.


<!--break-->
