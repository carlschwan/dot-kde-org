---
title: "Kubuntu Developer Jonathan Riddell Interviewed"
date:    2006-04-13
authors:
  - "canllaith"
slug:    kubuntu-developer-jonathan-riddell-interviewed
comments:
  - subject: "Get a raise!"
    date: 2006-04-13
    body: "You poor laddie, not even a chair to sit on."
    author: "reihal"
  - subject: "deep friend"
    date: 2006-04-13
    body: "\"Deep friend pizza supper\"\nas opposed to shallow friends?"
    author: "ivor"
  - subject: "Re: deep friend"
    date: 2006-04-13
    body: "thanks fixed :)"
    author: "seaLne"
  - subject: "Kubuntu protest"
    date: 2006-04-13
    body: "As far as I understand the German Kubuntu team is currently fed up with Kubuntu, they even launched a protest site on www.kubuntu.de\n\nWhat goes wrong?"
    author: "Gerd"
  - subject: "Re: Kubuntu protest"
    date: 2006-04-13
    body: "See my responses and my blog.\n\nhttps://lists.ubuntu.com/archives/kubuntu-users/2006-April/004871.html\nhttp://www.kdedevelopers.org/node/1917\n"
    author: "Jonathan Riddell"
  - subject: "Re: Kubuntu protest"
    date: 2006-04-14
    body: "How about a little courtesy, Jonathan?  Your comments on the kubuntu-users list are a bit unkind.  Not to mention your \"Move Along, Nothing to See\" post.  Why is Canonical allowing you to be the public face of Kubuntu?\n\n\nSome examples:\n\nPoor Jane is a busy person, she probably doesn't much wish to ansewr\nrandom accusationary e-mails.\n...\nI don't know the details here, it sounds like personalities are\ngetting in the way.\n...\nAs I see it you have no valid complains in this list so please reopen\nkubuntu.de.  (From talking to other German Kubuntu users they seem to\nsuggest that they'll just start using ubuntuusers.de if this nonsense\ncontinues.)\n\nJonathan"
    author: "dusty"
  - subject: "Re: Kubuntu protest"
    date: 2006-04-15
    body: "All of those comments are factually correct (from my point of view anyway) and not rude.  What's unkind?\n"
    author: "Jonathan Riddell"
  - subject: "Re: Kubuntu protest"
    date: 2006-04-16
    body: "Don't even give them the time of day, Jonathan. They aren't following the Code of Conduct, and didn't go through the proper Ubuntu dispute channels."
    author: "Philip"
  - subject: "Re: Kubuntu protest"
    date: 2006-04-18
    body: "I think Germans are angry about the attempts of ximian to cripple SuSe as a KDE distro. It is like people felt invaded by Gnome imperialism."
    author: "Bert"
  - subject: "Re: Kubuntu protest"
    date: 2006-04-14
    body: "Don't think you noticed (atleast I missed at first glance) that after the german manifesto there is also english one. Good explanations about the situation there."
    author: "petteri"
  - subject: "Kubuntu Rocks"
    date: 2006-04-15
    body: "I use Kubuntu and I'd like to say thanks to Jonathan Riddell for working so hard to make Kubuntu the best KDE distro that I've tried."
    author: "Elijah Lofgren"
  - subject: "Re: Kubuntu Rocks"
    date: 2007-04-18
    body: "Kubuntu is a very nice distro and I use it as my second OS. I much prefer Mandriva's 2007 versions though. Kubuntu will need to evolve (especially graphically - The little things) to compete with MDV 2007.1 in my opinion."
    author: "Per"
  - subject: "Dennis the Menace"
    date: 2006-04-17
    body: ">> Favourite comic?\n> The Beano with Dennis the Mennis.\n\nThat should be \"Dennis the Menace\"\nhttp://www.waplingtons.freeserve.co.uk/dennis.jpg\n\n"
    author: "Anon"
  - subject: "My Compliments"
    date: 2006-04-17
    body: "As far as i can tel and experience K-UBUNTU works great\nI think it makes Debian usefull for those who like me \nare less experienced whit linux.\nThe worst anemy of LINUX getting tho peoples desktop is\nthat everybody has his own opinion about how to deal with\na problem.\nShould i work with GNOME or KDE i think, who cares it is my desktop\nbe happy with the posibility of being able tho choose\nThe British stil drive on the left side of the road, \nand i am not complaining.\nLeave everybody in its own value i can only say thanks to those\nwho develop al those beautifull things for all of us to use for free.\n\n \n"
    author: "Tim"
  - subject: "Re: My Compliments"
    date: 2006-04-20
    body: "Hello Tim,\nYou are so right my friend. Let's be thankfull to those people who have been working so hard to give us Linux (any distro),also giving us the freedom to choose between Gnome and Kde. Both desktops are really tops!\nI've been switching back and forth and I can't make up my mind which is better.\n So I have installed Kubuntu adding to it Ubuntu, and I have the pleasure of enjoying both since I can switch back and forth.\n Variety is the spice of life, they say.\n\nThanks again to all those people who generously spend their time and money to give \neverybody the opportunity to enter the 'Wonderful World of Linux'\n  A newbie"
    author: "Don"
---
Inspired by the People Behind KDE series, <a href="http://behindubuntu.org/">Behind Ubuntu</a> gives us a peek into the lives of contributers to Ubuntu, Kubuntu, Edubuntu and Xubuntu. The first episode honours our very own <a href="http://behindubuntu.org/interviews/JonathanRiddell/">Jonathan Riddell</a>, KDE contributer and lead <a href="http://www.kubuntu.org/">Kubuntu</a> developer. Jonathan shares with us details of his work and reveals elements of his personal life, passions and plans for the future.







<!--break-->
