---
title: "aKademy Awards 2006"
date:    2006-09-15
authors:
  - "acid"
slug:    akademy-awards-2006
comments:
  - subject: "Grinch stole akademy website..."
    date: 2006-09-16
    body: "...all mention removed from www.kde.org"
    author: "Waldo Bastian"
  - subject: "Re: Grinch stole akademy website..."
    date: 2006-09-16
    body: "http://wiki.kde.org/tiki-index.php?page=aKademy%202006\n\nhttp://akademy2006.kde.org/"
    author: "Daniel \"Suslik\" D."
  - subject: "Re: Grinch stole akademy website..."
    date: 2006-09-16
    body: "But nevertheless the akademy site should be linked from the kde.org mainpage."
    author: "J\u00f6rg Hoh"
  - subject: "Speculative guesses"
    date: 2006-09-16
    body: "Hm, I suppose the \"best application\" prize has to go to Krita, because the improvements since last year are just too overwhelming to let anything else get the prize. Well, Amarok maybe. Let's see.\n\nThe \"best contribution\" and \"special jury's choice award\" prizes are not so clear.  For the last one, what about Laurent Montel as Obsessive Qt4 Porter? Might be a good alternative for naming the technical preview after him - after all, \"Laurent\" doesn't include one single K ;)"
    author: "Jakob Petsovits"
  - subject: "Re: Speculative guesses"
    date: 2006-09-16
    body: "Don't forget Digikam :)"
    author: "AC"
  - subject: "Re: Speculative guesses"
    date: 2006-09-18
    body: "KLaurent    :p   Or maybe Laurkent"
    author: "Nicholas Robbins"
  - subject: "Re: Speculative guesses"
    date: 2006-09-19
    body: "What about Kalzium ?"
    author: "polz"
---
This year <a href="http://conference2006.kde.org/">aKademy</a> will continue with <a href="http://dot.kde.org/1125411946/">tradition created at aKademy 2005</a> of awarding the people that made an outstanding contribution to KDE in the last year. The <a href="http://conference2006.kde.org/conference/talks/awards.php">award ceremony</a> will be on Sunday, September 24th at 17:50-18:00. Read on for more details.

<!--break-->
<p>
Aaron Seigo and Albert Astals Cid will be the Masters of Ceremonies, giving the prizes of the three categories:
<ul>
<li>Best application</li>
<li>Best contribution to KDE</li>
<li>Special Jury's Choice award</li>
</ul>
</p>
<p>
This year's jury has been formed by Albert Astals Cid, Enrico Ros, <a href="http://people.kde.nl/lauri.html">Lauri Watts</a>, <a href="http://people.kde.nl/stephan.html">Stephan Kulow</a> and Oswald Buddenhagen, last year's Akademy Award winners.
</p>
<p>
Many thanks go to <a href="http://conference2006.kde.org/sponsors/index.php#staikos">Staikos Computing Services</a> and <a href="http://conference2006.kde.org/sponsors/index.php#ricoh">Ricoh</a> who are sponsoring aKademy 2006 award prizes.</p>
<p>
Do not forget to come to the ceremony. You could be one of the winners!
</p>

