---
title: "KDE Commit-Digest for 16th July 2006"
date:    2006-07-17
authors:
  - "dallen"
slug:    kde-commit-digest-16th-july-2006
comments:
  - subject: "kde4"
    date: 2006-07-16
    body: "i would love to see the kde4 techology preview !"
    author: "ch"
  - subject: "Re: kde4"
    date: 2006-07-16
    body: "I'll let you in on a secret, you can. It's called svn trunk. And if you keep on updating sooner or later you will see both betas and the final release:-)"
    author: "Morty"
  - subject: "Kdevelop C# parser"
    date: 2006-07-17
    body: "It's great to see the C# parser coming along so nicely.  KDevelop plus good C# support is a powerful combination.  Let's hope that the KDE and Qt bindings are coming along too.  If you want to look beyond C#, see what the Nemerle (http://www.nemerle.org) folks are doing.  It's basically a C# 4.0 now."
    author: "ac"
  - subject: "Re: Kdevelop C# parser"
    date: 2006-07-17
    body: "Eh wait, dannya got me wrong.\nThe parser is feature complete, right, but I still have to do the KDevelop integration which is yet to be started. So, only partial completeness for now :P"
    author: "Jakob Petsovits"
  - subject: "Re: Kdevelop C# parser"
    date: 2006-07-17
    body: "It's no big deal that it's not done yet:)  It's good to see someone working on it.  What tools are you using to write it?"
    author: "ac"
  - subject: "Re: Kdevelop C# parser"
    date: 2006-07-17
    body: "I'm using Flex for lexical analysis, and Roberto Raggi's parser generator kdevelop-pg for the parser. (Currently attempting to improve the latter one, in order to meet the needs for maintainable integration.) And, of course, KDevelop! :)\n\nThe rest of the project won't be using code generation tools, now it comes down to plain old C++ code..."
    author: "Jakob Petsovits"
  - subject: "Re: Kdevelop C# parser"
    date: 2006-07-19
    body: "i see a lot happen in free/open IDEs lately.\n\nEclipse has come a long way.\n\nand this project...\n  http://community.sharpdevelop.net/blogs/mattward/articles/FeatureTour.aspx\n...named SharpDevelop (there is a mono port called MonoDevelop) also looks very promissing (i like the textmate-like code generation features).\n\nKDevelop has a major advantage: speed.\n\n\n"
    author: "cies breijs"
  - subject: "Re: Kdevelop C# parser"
    date: 2006-10-20
    body: "When using C# it seems like a good idea to revamp the old QtC and Qt# toolkits as Gui design parts for the languages like C#, VB.net, J# , Asp.net, Ado.net which could use these kits as their base Gui api for the Systems.windows part\nMono uses Gtk# and this would be an exelent way to show Qt# and Qtc have the same if not better qualities tied to them.\n"
    author: "fabian pijpers"
  - subject: "KmPlot progress is amazing!"
    date: 2006-07-17
    body: "It isn't mentioned in the first commit-digest paragraph that is seen at the dot, but read it at the end of the Digest. The KmPlot developers have taken the program to a level well beyond what we have now. All of you math/physics fans/teachers/students do yourself a favor and go to the page: http://edu.kde.org/kmplot/development.php\n\nSeeing is believing!\n- The new Qt painter capabilities (mainly anti-aliasing) make the graphs look so much nicer (and professional)\n- The new function capabilities are great (implicit functions, diff. equations...)\n- The new interface looks very usable and extremely powerfull too!\n- Parameter animator is very interesting (got to test that one, though to decide how useful it is. Looking great as it is)\n- The new dialog for editing expressions is simple and efficient\n- And many more things\n\nAll in all, the new KmPlot looks nearly feature complete and professional. Miles, nay, parsecs better than the version in KDE 3.5 (1.2). It just hurts to think we will have to do without all that for another half a year at the least!. Once the developers complete the To-Do items, what are they going to work on all that time? Improve KmPlot until it has more features than Mathlab or Mathematica with even 3D plots and the like?\n\nDavid Saxton, Klaus-Dieter et al: many, many thanks. Your baby will rock big time!!!\n\nPD: Ah!, maybe a new name wouldn't be bad. What about Mathcurve? (or Graph Plotter, Powermath, Plotpower, or even Butterfly Plotter or Graphdigestor :-) You don't need my copywrite for that ;-)"
    author: "Miquel Torres"
  - subject: "Re: KmPlot progress is amazing!"
    date: 2006-07-17
    body: "s/Mathcurve/MathKurve/ :)\n\n> Improve KmPlot until it has more features than Mathlab or Mathematica with even 3D plots and the like?\ni guess first thing is rather to implement gnuplot functionality?"
    author: "Nick"
  - subject: "Re: KmPlot progress is amazing!"
    date: 2006-07-18
    body: "KmPlot is unlikely to ever get data plotting ability as GnuPlot has - doing so would burden its interface unnecessarily and duplicate the functionality of lots of existing high-quality data plotters, such as QtiPlot. But otherwise, GnuPlot has lots of good ideas :)\n\nIf you have suggestions for features, then please tell me about them via bugs.kde.org.\n"
    author: "David Saxton"
  - subject: "Re: KmPlot progress is amazing!"
    date: 2006-07-17
    body: "The new KmPlot is looking excellent.\n\nAt my secondary school we made heavy use of a commercial package called Omnigraph.  Although it looks like a very early Windows 95 program, it was a very powerful package, and was also very usable.\n\nIt is nice to finally see an OSS solution that could be used as a replacement ( earlier KmPlot builds had the right functionality but were very difficult to use ).\n\nI think it is still worth taking a look at Omnigraph for ideas about making entry of formulae as easy and clear as possible.  The new KmPlot still has too many widgets visible for my liking.\n\nhttp://www.mathsnet.net/omnigraph.html"
    author: "Robert Knight"
  - subject: "Re: KmPlot progress is amazing!"
    date: 2006-07-18
    body: "Don't forget good old \"derive.exe\" which supported symbolic transformations. Was hard to get an successor in the same category, had a multiplan style graphic user interface.\n\nI think also use of \"R\" might be useful in schools. Although it is designed for statistical stuff it is quite nice for educational purposes. \n\nIt is a pity that we don't have a GUI for it.\n\nhttp://www.r-project.org/\n\n\n"
    author: "funbar"
  - subject: "Re: KmPlot progress is amazing!"
    date: 2006-07-18
    body: "There is actually a GUI for R, called rkward (http://rkward.sourceforge.net)."
    author: "Luca Beltrame"
  - subject: "Re: KmPlot progress is amazing!"
    date: 2006-07-18
    body: "Kool!"
    author: "funbar"
  - subject: "Re: KmPlot progress is amazing!"
    date: 2006-07-18
    body: "I've used Omnigraph before and it's a nice program.\n\nHowever, Omnigraph and KmPlot have a key fundamental difference in how equations are handled. In Omnigraph, you enter e.g. \"y = x^2\" to draw a cartesian plot, or e.g. \"r = [theta];\" to draw a polar plot. But in KmPlot, you first specify the type of plot, and then enter the function, e.g. \"f(x) = x^2\".\n\nThis means that Omnigraph's method of entering formulae is entirely inappropriate for KmPlot.\n\nIMHO, KmPlot's method of doing it is better ;). By specifying the function type, the widgets for controlling that *specific* type of function can be made permanently available for quick access, as opposed to Omnigraph's hiding of the widgets in right-click menus, etc.\n\nBut of course I'd love to hear any specific suggestions you have for the UI.\n"
    author: "David Saxton"
  - subject: "Re: KmPlot progress is amazing!"
    date: 2006-07-18
    body: "It looks great but ... {TM}\n\nPlease don't use Runge-Kutta.\n\nWe now have computers (not desktop calculators).  Therefore, we don't have to use such primitive methods.\n\nRichardson's book describes the method which I have used although he never (AFAIK) ever actually provided the formulas for it.\n\nIf the developers like, I will explain it to them.\n\nThe problem with Runge-Kutta is that it will not solve the discharge of a capacitor problem -- or any DE with the analytical solution: x = a*exp(-k*t)"
    author: "James Richard Tyrer"
  - subject: "Re: KmPlot progress is amazing!"
    date: 2006-07-18
    body: "All integration methods will fail for some differential equations. But if you email me the method (or give its name), then I'll be happy to investigate its feasibility, etc, for KmPlot.\n"
    author: "David Saxton"
  - subject: "Re: KmPlot progress is amazing!"
    date: 2006-07-18
    body: "I have sent you a brief e-mail.\n\nAll such methods that attempt to extrapolate as \"h\" goes to 0 are called Richardson Extrapolation because he first proposed the idea.\n\nThe method which I use [IIRC] is based on what I read in the book \"Numerical Methods That Work\"  \n\nhttp://www.amazon.com/gp/product/0883854503/ref=nosim/103-8532950-2506261?n=283155\n\nUnfortunately, many of these methods (the ones with names) have evolved into complicated formulas when this isn't really necessary."
    author: "James Richard Tyrer"
  - subject: "about the old comment"
    date: 2008-07-12
    body: "I had no idea what was going on.\nAt this moment i understand that, the old bindings were to big, bloat and ugly.\nThe new bindings were done much more efficient.\nThis i learn'd after watching Richard Dales presentation.\n"
    author: "fabian pijpers"
---
In <a href="http://commit-digest.org/issues/2006-07-16/">this week's KDE Commit-Digest</a>: <a href="http://dot.kde.org/1152645965/">Unity</a>, a project to re-synchronise <a href="http://www.khtml.info/">KHTML</a> with <a href="http://webkit.opendarwin.org/">WebKit</a>, has begun, with work continuing throughout the week. Support for suspend and resume on KIO jobs. KSpread gets support for scripting with <a href="http://www.python.org/">Python</a> and <a href="http://www.ruby-lang.org/en/">Ruby</a>. One <a href="http://developer.kde.org/summerofcode/soc2006.html">Summer Of Code</a> project, "C# parser for KDevelop", reaches the feature-complete stage, with progress in the "Advanced Session Management", "GMail-style conversations for KMail" and "WorKflow" projects. Fixes made to support the German language in <a href="http://edu.kde.org/klettres/">KLettres</a>, with large-scale refactoring work in <a href="http://edu.kde.org/kiten/">Kiten</a>.
<!--break-->
