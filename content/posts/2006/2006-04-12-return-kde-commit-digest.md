---
title: "The Return of KDE Commit Digest"
date:    2006-04-12
authors:
  - "jriddell"
slug:    return-kde-commit-digest
comments:
  - subject: "Danny you are the man."
    date: 2006-04-11
    body: "Producing the digest is a huge undertaking. Thanks for the effort!\nI like the new looks. \n-Sam"
    author: "Sam Weber"
  - subject: "Some bugs"
    date: 2006-04-11
    body: "1. There's an encoding problem, e.g.:\n\"\u00c4\u00b0smail D\u00c3\u00b6nmez committed a change to /branches/KDE//kdelibs/kio/kfile/:\"\n\n2. Something's wrong with the layout of the pages. What works fine with Firefox, does not work fine with Konqueror. There's an ugly white gap at the top of the report pages.\n\nEckhart"
    author: "Eckhart W\u00f6rner"
  - subject: "Re: Some bugs"
    date: 2006-04-12
    body: "Encoding problem is fixed :)"
    author: "cartman"
  - subject: "Re: Some bugs"
    date: 2006-04-13
    body: "1) The encoding problem was caused by some server maintanence - as cartman says, it is now fixed :)\n\n2) This is a more difficult bug to fix - but, when I move to a fully css based layout (rather than tables), I think it will disappear.\n\nThanks,\nDanny"
    author: "Danny Allen"
  - subject: "Re: Some bugs"
    date: 2006-04-13
    body: "Danny,\n\nDo the following before worrying about restructuring the tables with thead,tfoot and tbody intermingled with divs.\n\nFix: colspan=\"2\" on your top table second td cell with get rid of the gap.\n\n<td colspan=\"2\" ...background: url('./images/standard/topbar.png')...>"
    author: "Marc Driftmeyer"
  - subject: "Re: Some bugs"
    date: 2006-04-16
    body: "Eh, that does actually fix the gap problem :)\nHowever, it messes the layout of the actual page content in random ways, so i'm going to have to investigate a bit before I add this fix.\n\nMany thanks though :)\nDanny"
    author: "Danny Allen"
  - subject: "SO HAPPY"
    date: 2006-04-12
    body: "I am so happy!!!  So happy!!  It has been so empty without the digest.  At last once again once a week I can wake up, sip my tea in bed, get the gf to make me toast, and read the commit digest. \n\nThank you!\n"
    author: "John Tapsell"
  - subject: "Re: SO HAPPY"
    date: 2006-04-12
    body: "You beat me to it!  The comment digests are really great and informative (and entertaining!), and since they stopped its been harder to know about whats happening, and it looks like now is a good time for them to start back up (it sounds like a lot of the interesting KDE 4 work is getting ready to come in)"
    author: "Corbin"
  - subject: "Welcome Back!"
    date: 2006-04-12
    body: "Missed these reports.  Glad to see that they are coming back!"
    author: "Ryan"
  - subject: "Forwardports?"
    date: 2006-04-12
    body: "I see many commits in the 3.5 branch which AFAIK are not forwardported to trunk. How is this handled?\nI hope all commits get ported somehow, otherwise a lot of effort would be wasted. And the bug database would be a total mess, because bugs marked as FIXED suddenly reappear in KDE 4.\nDoes anybody know more?"
    author: "Thomas.McGuire@gmx.net"
  - subject: "Re: Forwardports?"
    date: 2006-04-12
    body: "Don't worry, we are forwardporting as needed."
    author: "cartman"
  - subject: "Wow, welcome back my lovely digest!"
    date: 2006-04-12
    body: "It was really hard to know about KDE developement when you're away. It will be a real joy."
    author: "thelightning"
  - subject: "At last."
    date: 2006-04-12
    body: "KFileDialog becomes aware of media:/ and system:/.\n\nAt last. I don't know why it has taken so long. That bug has been there since KDE 3.4 and was making media:/ and system:/ mostly useless when treating with large files."
    author: "Jose "
  - subject: "kerry frontend to beagle..."
    date: 2006-04-12
    body: "http://www.thebreedsofdogs.com/KERRY_BEAGLE.htm\n\nnames really get overworked in linux don't they :p\na well.. at least it's not called keagle or kbeagle\n\nthis url has some screenies of kerry.\nhttp://www.kde-apps.org/content/show.php?content=36832\n"
    author: "Mark Hannessen"
  - subject: "Re: kerry frontend to beagle..."
    date: 2006-04-12
    body: "Sorry, I think such an application just clutters my desktop. I wish destop search to be integrated in my browser. Google Desktop Search for win is soo great.\n\nThese Standalone applications follow a paradigme that is obsolete. Today we have to make sure that we get as few applications as possible. Most functionality moves today moved web applications which are easiely to get adapted anc changed interface wise.\n\n"
    author: "Meister"
  - subject: "Re: kerry frontend to beagle..."
    date: 2006-04-12
    body: "> I wish destop search to be integrated in my browser.\n\nThen use kio-beagle."
    author: "Anonymous"
  - subject: "Re: kerry frontend to beagle..."
    date: 2006-04-12
    body: "indeed. even works in a file-open dialog..."
    author: "superstoned"
  - subject: "Re: kerry frontend to beagle..."
    date: 2006-04-13
    body: "kio-beagle? Okay, tell me more about it."
    author: "Meister"
  - subject: "Re: kerry frontend to beagle..."
    date: 2006-04-13
    body: "Try:\ngg:kio-beagle or apps-kde.org"
    author: "Morty"
  - subject: "Thanks"
    date: 2006-04-12
    body: "Many thanks :)"
    author: "m."
  - subject: "Cool but..."
    date: 2006-04-12
    body: "\\o/ Yeeeees ! I was waiting for that moment for days !\n\nUnfortunatelly, I find that the new look of the \"KDE Commit Digest\" is not very good regarding the old one.\nFirst : using HTML tables when semantic (X)HTML + CSS can be used is unfortunate.\nSecond : there is an over-use of italic text and my eyes hate that :) \nThird : it is difficult to see the categories of the commit. The titles have the same style (sans-serif black...).\n\nBut the most important is the content and the return of the KDE commit-digest \\o/\n\nThanks a lot."
    author: "Shift"
  - subject: "Re: Cool but..."
    date: 2006-04-13
    body: "You are definitely right that a css-based layout would be much better, but my HTML skills are mainly limited to working with tables... but, I do plan to move to a css layout shortly (when I find help ;)\n\nRegarding the second (and soon, the third) point, i've just added the options page that I have been working on. Take a look at http://commit-digest.org/options - you can now choose, amongst other options, the style of the quotes. Other style options will appear here soon, so stay on the lookout :)\n\nThanks,\nDanny"
    author: "Danny Allen"
  - subject: "Re: Cool but..."
    date: 2006-04-13
    body: "Danny, you are brilliant \\o/\n\nFrench readers will understand the private joke in this sentence ;)"
    author: "Shift"
  - subject: "Buzz ?"
    date: 2006-04-12
    body: "What does that mean in this context and how do you measure it ?\n\nAlex\n"
    author: "aleXXX"
  - subject: "Re: Buzz ?"
    date: 2006-04-13
    body: "The buzz ratings are something that I have been calculating for a while at http://myscreen.org/buzz\n\nThey are basically an indication (not a totally foolproof scientific method ;) of the popularity/vitality/activity of a program or person. They are calculated from mentions on the internet (web pages) of the subject, recent discussion of the subject (blogs), and, something which I need to reintegrate back into the ratings, commit activity (I stopped that component of the score when the digest stopped... of course, now I am making the digest, so... ;)\n\nThese scores are regenerated every 2 days - up to date scores and charts to see change over time can be found at http://myscreen.org/buzz\n\nThanks,\nDanny"
    author: "Danny Allen"
  - subject: "Fantastic!!!"
    date: 2006-04-15
    body: "Its great to see it back."
    author: "Fizz"
---
The weekly summary of happenings in KDE development, <a href="http://commit-digest.org/">The KDE Commit Digest</a>, has returned with a new author, Danny Allen.  Highlights in the <a href="http://commit-digest.org/issues/2006-04-09/">current issue</a>: "<em>KFileDialog becomes aware of media:/ and system:/. New icons and other fixes in amaroK. New privacy features and multiple webcam connection support for the MSN protocol in Kopete. kcmwifi removed in /trunk (to be replaced by Solid in KDE 4). Kerry, the KDE Beagle frontend, is imported into KDE SVN.</em>"  It also shows the week's most important postings to the KDE mailing lists and the top ten committers of the week (congratulations Gilles Caulier).




<!--break-->
