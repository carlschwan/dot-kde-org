---
title: "Konqcast at KDE://radio"
date:    2006-07-15
authors:
  - "jriddell"
slug:    konqcast-kderadio
comments:
  - subject: "Love them"
    date: 2006-07-14
    body: "I really enjoyed listening to the various interviews. They were very informative and /very/ interesting.  Hopefully more will be made and they will be released pretty regularly (Aaron could do one on Plasma and interview himself!)"
    author: "Corbin"
  - subject: "Re: Love them"
    date: 2006-07-14
    body: "> Aaron could do one on Plasma and interview himself!\n\noh man. like i don't have a hard enough time convincing people i'm not crazy as it is without recording conversations i have with myself ;)\n\nglad you enjoyed the interviews; they were equally enjoyable to make.\n\nthanks to rainer endres and danny allen for stepping up out of the blue to take on the website stuff =)\n\n"
    author: "Aaron J. Seigo"
  - subject: "Re: Love them"
    date: 2006-07-19
    body: "> oh man. like i don't have a hard enough time convincing people i'm not\n> crazy as it is without recording conversations i have with myself ;)\n\nOh yeah, it becomes much harder this way. Esp. if you just serve people, that picked you up, in order to get them at their balls. :|\n\nBut who would consider that...\n"
    author: "Stefan Nikolaus"
  - subject: "fixed rss"
    date: 2006-07-15
    body: "Here's a fixed RSS feed:\nhttp://www2.truman.edu/~iam504/konqcast.rss\nThere were two extra </channel>s which QDom doesn't apperciate very much. And I guess radio.kde.org hasn't reached my DNS server yet or something, the URLs were all broken.\nDon't subscribe to that permanetly, I'm not updating it."
    author: "Ian Monroe"
  - subject: "Re: fixed rss"
    date: 2006-07-15
    body: "did you perhaps grab the feed from my blog entry earlier in the week? because i fixed the \"2 </channel>s\" thing mid-week... \n\naaah.. osuosl.org hasn't resync'd yet and you're probably grabbing it from there (due to a dns server issue that we're still working out) ... but  osuosl.org not syncing is odd.. it should be happening each day at 13:30 ... =/ hm.\n\nyet more things to look into. sorry for the bumpy ride. didn't see the dns or the sync issues coming (and it all works from here) =/"
    author: "Aaron J. Seigo"
  - subject: "Podcast"
    date: 2006-07-15
    body: "Podcasting rocks. Open Source here lacks behind but I am sure this will change.\n\nWhat is urgently needed in the Podcast world is a kind of torrent protocol, bandwidth is a problem.\n\n\nMy favourite podcast station btw:\nhttp://chaosradio.ccc.de/\n\nI really enjoy your oggs! And it is very nice that you support the ogg format."
    author: "knut"
  - subject: "Re: Podcast"
    date: 2006-07-15
    body: "Heh, a torrent protocol like BitTorrent? :) Azuerus already supports RSS feeds via a plugin."
    author: "Ian Monroe"
  - subject: "Re: Podcast"
    date: 2006-07-15
    body: "The podcast could easily point to a .torrent file fpr a ogg-vorbis or mp3 or any other file.  AFAIK podcasts are really just RSS feeds that point to a media file.  It shouldn't be hard to have a podcast/rss client like Akregator that when it sees a feed that uses .torrent it could pass the torrent to kget (which I believe has a built in torrent client now), and when that download finishes it would then treat the file that was downloaded from the torrent the same as if it was downloaded via http and do what it normally would of done."
    author: "Corbin"
  - subject: "Re: Podcast"
    date: 2006-07-15
    body: "Right, but pocasting is \"easy\".\n\nI mean, we had Rss for ages an then Apple came with iTunes. Let's forget the iPod,  but we had no podcasting phenonemon without iTunes.\n\nThat you can download references in rss.files is one thing. With the new podcast experience it is all backgrounded. You do not have to care about it.\n\nbut the major disadvantage of podcasting is that success kills your channel. What I mean is that you pay for the download traffic. A 4MB mp3 that is downloaded by 10 000 users is 40 GB. Now, when we move to videoblogging we even need more traffic. peer2peer distribution is certainly a good method to reduce server load. And as it is about small files it should be possible to get fast data echange, your podcast files as an alternative target for others. Even further: It does not need to be provided by the person who runs the podcast channel. Users just have to exchange their podcast subscription lists and then get the files from other users who already downloaded the files.\n\n"
    author: "knut"
  - subject: "sleek interface?"
    date: 2006-07-15
    body: "Very interesting podcast... thankyou, i hope to hear more in the future.\n\nIn the Akonadi segment Aaron and the PIM crew are talking about a sleek new interface and Aaron says he even has it running on his laptop... It peeked my interest, but they didn't give any details.\n\nWhat program are they talking about? In what way is it slick? And are there any screenshots?"
    author: "yakhan"
  - subject: "Re: sleek interface?"
    date: 2006-07-15
    body: "This is the programmer Aaron talking:-) Akonadi are the new backend/storage server for KDE PIM, and it does not really have a UI. So the sleek interface he is talking about are the programmers interface, not material for exciting screenshots really."
    author: "Morty"
  - subject: "Re: sleek interface?"
    date: 2006-07-15
    body: "actually, i was talking about a user interface. though i could've been talking about an API as that wasn't particularly clear indeed.\n\nthe UI is part of a summer of code project being done by sheldonc that introduces a graphical representation of a timeline which can be populated with time information ... the timeline ticks by in real time (mouse interaction is also planned so you can zoom and move the timeline) and entries show up as cute little flags that you can interact with ...\n\ndata will come from akonadi among other sources and it will be integrated with plasma. neat.\n\n"
    author: "Aaron J. Seigo"
  - subject: "Re: sleek interface?"
    date: 2006-07-19
    body: "Ooops, my mistake. Was not my intention to put words in your mouth. I had never seen anything about any UI in connection with Akonadi, and it looks like I'm not very up to date on SOC either. But I'll blame the developers, running around and doing greate amounts of coding and developing new stuff all the time. And all that without notifying me about it, I think thats really bad form:-)"
    author: "Morty"
  - subject: "Hmm..."
    date: 2006-07-15
    body: "Which RFC defines the KDE protocol?"
    author: "AC"
  - subject: "Re: Hmm..."
    date: 2006-07-15
    body: "The \"kde:\" web shortcut works in KDE applications to access the KDE developer documentation (e.g. type \"kde:KApplication\" into the Konqueror address bar). As of 1 minute ago, kde://radio works as well.\n\nTo make it also work in capital letters, you need to change the web shortcut in the Konqueror configuration dialog from \"kde\" to \"KDE\".\n\nOlaf"
    author: "Olaf Jan Schmidt"
  - subject: "Re: Hmm..."
    date: 2006-07-15
    body: "kde://radio .. literally (at least in konq). holy crap. now -that- is cool =) i was wondering what your commit to the media/ framework did, and now i know... thanks olaf"
    author: "Aaron J. Seigo"
  - subject: "no more MP3"
    date: 2006-07-15
    body: "i think that the kde community should no longer provide media in the mp3 format.\nogg is supported on all free platforms and even on many non-free players.\nall those, not using a compatible player, should know, that we are all about freedom and that we oppose non-free and/or patent-encumberd formats or standards."
    author: "hannes hauswedell"
  - subject: "Re: no more MP3"
    date: 2006-07-15
    body: "Hello,\n\nplease also assume that KDE wants to equally well inform those that don't already have a free desktop.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: no more MP3"
    date: 2006-07-15
    body: "well, the default feed is in Ogg format. and the idea is to reach out to as many people so that they might be enticed to join us in our free software world, and that means offering it in mp3 ... note that most portable media players only support mp3 (the device i recorded with only does mp3 as well), most people on non-free platforms don't have ogg support installed ... etc.\n\nhopefully, though, the default feed format will encourage people to discover what ogg is ..."
    author: "Aaron J. Seigo"
  - subject: "Re: no more MP3"
    date: 2006-07-16
    body: "That would be very user unfriendly. I for example am using only Linux on my computers (even on my Mac mini), I know and love Ogg Vorbis, I know the licence and patents problems of MP3.\n\nBUT: my Hifi-System, my iPod, my Palm PDA, my cell phone, my DVD player, my MP3 USB stick - they all support MP3. But not one of them supports Vorbis. So I encode all of my music only in MP3 because it's the most usable solution for me.\n\nEven if I install Rockbox (rockbox.org) on my iPod for example (which provides Vorbis support but is rather unfinished yet), I still can't use my sound files on my other devices where I don't have the possibility to add Ogg Vorbis support.\n\nIn the end I would transcode Vorbis-only podcasts to MP3 and just be annoyed about the podcaster."
    author: "Stefan"
  - subject: "konqcast.rss broken !!!"
    date: 2006-07-18
    body: "Am I the only one to notice this ? Konqcast.rss file is broken i.e. it is an invalid XML file. Line 69, 70 both have </contain>, only one should be present. Its weird nobody (read: no player) noticed that. Kudos to amarok for refusing to load the podcast :)\n\nSomeone (Aaron?) please fix the rss file. Thanks."
    author: "D Bera"
  - subject: "Re: konqcast.rss broken !!!"
    date: 2006-07-18
    body: "Oops. </channel> I meant."
    author: "D Bera"
  - subject: "Re: konqcast.rss broken !!!"
    date: 2006-07-18
    body: "Oops again :(\nJust read the posts above. How did I not see that! Sorry for dupe."
    author: "D Bera"
  - subject: "Audio Quality Really Bad"
    date: 2006-07-18
    body: "Hi\n\nI was excited to hear about kde-centric podcasts! Unfortunately the audio quality is really poor and I could not hear what was being said most of the time :-( It sounds like you just put a recorder on the table and started talking - you really need to hold a mike to the mouth of the person speaking...\n\nThanks for taking the time to do these...hopefully the quality will improve as you get more practice!\n\nRegards\n\nTim\n"
    author: "Tim Sutton"
---
At the recent <a href="http://dot.kde.org/1151271635/">KDE Four Core</a> meeting Aaron Seigo interviewed a number of the developers.  You can hear them now on the new <a href="http://radio.kde.org/">KDE://radio</a> (<a href="http://radio.kde.org/pub/konqcast/">listing</a>) site.  Subscribe to the podcast feed in <a href="http://radio.kde.org/pub/konqcast/konqcast.rss">Ogg</a> or <a href="http://radio.kde.org/pub/konqcast/konqcast_mp3.rss">MP3</a>.  The interviews cover the new <a href="http://wiki.kde.org/KDE+4+GUI+framework">liveui</a> framework, <a href="http://pim.kde.org/akonadi/">Akonadi</a> PIM Storage Service, the <a href="http://usability.kde.org/hig/">Human Interface Guidelines</a> and many more.  Thanks to <a href="http://osuosl.org/">OSU Open Source Lab</a> for sponsoring the KDE://radio bandwidth.  In related stories Newsforge gives us a <a href="http://trends.newsforge.com/trends/06/07/10/2047210.shtml">wrap up of the blogs from Trysil</a> and here's the <a href="http://static.kdenews.org/jr/kde-four-core-group.html">group photo</a> you've all been waiting for.











<!--break-->
<p><a href="http://radio.kde.org/pub/konqcast/konqcast.rss"><img src="http://static.kdenews.org/jr/kde-radio.png" width="245" height="145" border="0" /></a></p>





