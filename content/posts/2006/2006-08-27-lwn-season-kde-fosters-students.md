---
title: "LWN: Season of KDE Fosters Students"
date:    2006-08-27
authors:
  - "jriddell"
slug:    lwn-season-kde-fosters-students
comments:
  - subject: "nice articles"
    date: 2006-08-27
    body: "sorry, jonathan, i can't email you now, so i'll say it here: the LWN link leads to lwn/net instead of lwn.net!\n\ni read the articles, the're cool ;-)\n\ni love the kmail idea. see if it works..."
    author: "superstoned"
  - subject: "E-Mail"
    date: 2006-08-27
    body: "I'm glad to hear about the E-Mail improvements. I always thought it was a waste to have an e-mail client built into several applications, such as knode and kmail."
    author: "Ian"
  - subject: "Wine integration?"
    date: 2006-08-28
    body: "Since the developer behind the KDE<->Wine integration is also the developer of gtk-qt, can we expect a KDE=>Wine widget style adaptation layer someday? :) Or maybe just a way for Windows apps to use the current KDE's color scheme. Is this planned?\n\nKeep up this interesting work!\nPat\n"
    author: "Patrice Tremblay"
---
<a href="http://lwn.net">LWN</a> has reported on KDE's <a href="http://developer.kde.org/seasonofkde/">Season of KDE</a> projects.  <a href="http://lwn.net/Articles/193723/">Article one</a> introduces the season and looks at projects for KWin, KOffice, user migration and accessibility.  <a href="http://lwn.net/Articles/193724/">Article two</a> covers projects on Poppler, Umbrello, KMail and more.







<!--break-->
