---
title: "amaroK 1.4: Rediscover More of Your Music"
date:    2006-05-18
authors:
  - "tteam"
slug:    amarok-14-rediscover-more-your-music
comments:
  - subject: "Warning - may destroy your collections database"
    date: 2006-05-18
    body: "Upgrading to amaroK 1.4 from 1.3 (probably anything prior to beta3, actually) will destroy your collections database unless you install the patch: http://svntalk.pwsp.net/project/amaroK/revision/541986 (Hopefully, there'll be a new release fixing it sometime soonish...)"
    author: "makomk"
  - subject: "Re: Warning - may destroy your collections database"
    date: 2006-05-18
    body: "Too late... Damn it. That's what I get for trying programs a few days before the official release.\n\n"
    author: "Dima"
  - subject: "Re: Warning - may destroy your collections database"
    date: 2006-05-19
    body: "I've solved the problem by doing it:\n  http://bugs.kde.org/show_bug.cgi?id=98482\n(change \"USE amarokdb;\" with \"USE amarok;\")."
    author: "An\u00f3nimo"
  - subject: "website down..."
    date: 2006-05-18
    body: "seems like the amarok website is down. It would be fun to find out how many hits the website recieved before it couldn't handle it anymore!"
    author: "Vlad"
  - subject: "Re: website down..."
    date: 2006-05-18
    body: "Now we're back, with a replacement static website. We ended up on digg.com front page, with a story linking directly to our wiki. That was just a bit too much for the server :)\n"
    author: "Mark Kretschmann"
  - subject: "Why do I have to install half of gnome to use it"
    date: 2006-05-18
    body: "That is what happens when I try to upgrade in debian unstable."
    author: "ac"
  - subject: "Re: Why do I have to install half of gnome to use it"
    date: 2006-05-18
    body: "I see no reason for this whatever.\n\nMaybe gtkpod has has large dependencies? I think that's used for iPods, and it should require at least glib."
    author: "ac"
  - subject: "Re: Why do I have to install half of gnome to use it"
    date: 2006-05-18
    body: "Some visualizations depend on libvisual which uses gtk2\n\ngnome is not needed, but if you want to have some visualizations you need gtk2\n"
    author: "Thomas"
  - subject: "Stars"
    date: 2006-05-18
    body: "Although amaroK's rating has only 5 stars, I'd give this player 6 or 7 ;-)\namaroK 1.4 is really great. Even iTunes-fans admint, that amarok is really cool. Especially the wikipedia and last.fm support. amaroK is truly a shiny star."
    author: "birdy"
  - subject: "Re: Stars"
    date: 2006-05-18
    body: "Podcasting is not so shiny with Amarok. \nAnd iTunes also supports video and radio.\n\n"
    author: "Hup"
  - subject: "Re: Stars"
    date: 2006-05-18
    body: ">Podcasting is not so shiny with Amarok.\n\nIt is with 1.4\n\n>And iTunes also supports video and radio.\n\nVideo is simply not amarok's scope, neither iTunes. Kaffeine does a way better job at showing videos. The real problem is to dispatch podcasts to the respective applications depending on the enclosure type. You can't judge that from the mime-type :("
    author: "Daniel Molkentin"
  - subject: "Re: Stars"
    date: 2006-05-18
    body: "Supoorting video is not necessarilty good for an audio player. I probably wouldn't like amarok if it got messed up with that.\n\nBTW, kaffeine would probably become a better app if it stopped trying to be an audio player.\n\nAs for radio, amarok supports radio too. (although pity that it doesn't have a way of browsing radio stations)"
    author: "ac"
  - subject: "Re: Stars"
    date: 2006-05-18
    body: "think it is a simple quicktime plugin in iTunes.\n\nVideo-Podacasts are hot and some podacasters also feed video and pdf."
    author: "Hup"
  - subject: "Re: Stars"
    date: 2006-05-18
    body: "> think it is a simple quicktime plugin in iTunes.\n\nThat's not the point, embedding video in amaroK would be easy enough technically. But a video playes and audio player ar just too different to mix them together. It would be like mixing a web broser with a file manager :-)\n\n> Video-Podacasts are hot and some podacasters also feed video and pdf.\n\nCome to think of it, this sounds like a job for a dedicated app to manage the podcasts, and a KIO slave to expose them."
    author: "ac"
  - subject: "Re: Stars"
    date: 2006-05-18
    body: "Have a look how its done with iTunes and take for instance great German CCC Chaosradio podcasting channel. See http://chaosradio.ccc.de/ for the feeds or find it as \"chaosradio\" in the iTunes podcast directory. They use a feed with audio, video and documents.\n\n"
    author: "Hup"
  - subject: "Video"
    date: 2006-05-18
    body: "My problem with this is: it's semi-ok if amaroK does not want to /play/ my videos in my multimedia collection, but it is _not_ ok that it does not put the videos correctly in my iPod (just like it only plays the audio on the desktop, it puts the videos in some way in my iPod that I can't see the video... so, I have to start gtkpod to put my videos in, at least on 1.4beta ...) AND it would be nice if it just forwarded videos to the correct app to play (in my case, KMplayer)"
    author: "Humberto Massa"
  - subject: "Re: Video"
    date: 2006-05-18
    body: "did you ever reported that misbehaviour?"
    author: "NabLa"
  - subject: "Re: Video"
    date: 2006-05-18
    body: "Not yet... and it seems that the wiki is offline."
    author: "Humberto Massa"
  - subject: "Re: Video"
    date: 2006-05-18
    body: "Not the wiki, try http://bugs.kde.org\n"
    author: "ac"
  - subject: "Re: Stars"
    date: 2006-05-18
    body: "> It would be like mixing a web broser with a file manager :-)\n\nActually, I do like Konqueror ;-)\nBut I think it can be better when it's split up, so I agree..."
    author: "Jakob Petsovits"
  - subject: "Re: Stars"
    date: 2006-05-18
    body: "I also like it. But the point is that one shouldn't provide a common abstraction every time things have something in common, they need to have much in common.\n\nAlso not all abstractions that are good for developers make sense for users, whereas some abstractions that are good for users are no good for developers. For instance amaroK can burn music on cds, because this fits the user's 'handling music with a computer' abstraction. But amaroK actually uses K3B for that, because to the developer this is not a good abstraction.\n\nOn the other hand Konqueror is a file manager that can handle networked files, and since web pages are sort of files on the network, it handles them too. That's OK for the developers (KIO slaves) but not for the user, who doesn't think of websites as files.\n\n\n"
    author: "ac"
  - subject: "Re: Stars"
    date: 2006-05-18
    body: "yeah, i think kaffeine would be better if it would drop the audio player thing. tried codeine? its really an amazing piece of work - fast, simple, intuitive, does exactly what it has to: play video's."
    author: "superstoned"
  - subject: "Re: Stars"
    date: 2006-05-19
    body: "well yeah but when you have a huge collection of divx, you need something like amarok to take care of your collection. So people that don't want to run two big consuming apps like amarok and kaffeine it would be good if amarok could handle videos too. Did you already try to manage thousands of videos all over your harddrive? it ain't easy. Something like amarok for videos would be great so why not do just do both at the same time?\nThis reminds me of the VoIP vs IM thing. Gaim and kopete devs thought it would be stupid to mix both and now everyboy's doing it cause it's just more convenient to manage two similar technologies with the same software video and text messaging, video and audio players... see what I mean?\n\n"
    author: "Patcito"
  - subject: "Re: Stars"
    date: 2006-05-19
    body: "Voice chat and text chat have similar patterns of usage.\n\nAudio and video players are different. An audio palyer is a background app that lives in the system tray most of the time. A video player is a foreground app, even one of the few fullscreen apps. So they need to be optimized differently."
    author: "wim"
  - subject: "Re: Stars"
    date: 2006-05-19
    body: "Yes;) Collections are great! I have many music videos and I have to run Kaffeine to watch them:( And without collection it takes too long... Last thing - new Kaffeine looks like amaroK, great :D"
    author: "Rezza"
  - subject: "Re: Stars"
    date: 2006-05-18
    body: "Mandriva announced with its multi-million Dollar Nepomuk EU research project that it would support sematic-desktop development in KDE. Is this wikipedia integration related to Nepomuk and what were the contributitions of Nepomuk so far?"
    author: "Mule Land"
  - subject: "Re: Stars"
    date: 2006-05-18
    body: "The wikipedia integration is for showing some artists' info in a tab, nothing to do with nepomuk or tenor (dunno if the last one is still alive)."
    author: "Morreale Jean Roc"
  - subject: "Re: Stars"
    date: 2006-05-18
    body: "Right. Possibly the best app available for linux, and I assume it can be used as an example for how to make a clear a clear and usable user interface for "
    author: "ac"
  - subject: "Re: Stars"
    date: 2006-05-18
    body: "Right. Possibly the best app available for linux, and I assume it can be used as an example for how to make a clear and usable user interface for a whole host of powerful features.\n\nAnd since I already have beta 3 and can tell you that 1.4 is great, I already can't wait to find out what's planned for 2.0 :-)"
    author: "ac"
  - subject: "amarok's really cool, except for one thing..."
    date: 2006-05-18
    body: "I just wish it didn't require xine or gstreamer for playback. When all you have is a bunch of mp3s, the xine engine is overkill, and apparently not too reliable. When starting 1.4, I get an error telling me the xine engine couldn't be initialized and that the void engine was installed instead, with no further info on how to fix the problem :-(. Pretty frustrating.\n"
    author: "Guillaume Laurent"
  - subject: "Re: amarok's really cool, except for one thing..."
    date: 2006-05-18
    body: "Sounds like your distro installed a neutered version of xine-lib (i.e. no mp3 support).  This isn't the fault of Xine.  Also Amarok doesn't force you to install all the engines (like on SuSE you can install amarok-xine for the xine engine, and on Gentoo you just use the USE flags to determine which is built), though again your distro may package it in a way that doesn't allow you to do that.\n\nSo blame your distro (and the mp3 patent), not Amarok."
    author: "Corbin"
  - subject: "Re: amarok's really cool, except for one thing..."
    date: 2006-05-18
    body: "I don't understand why people are not using OGG."
    author: "Sebasti\u00e1n Ben\u00edtez"
  - subject: "Re: amarok's really cool, except for one thing..."
    date: 2006-05-18
    body: "Device support maybe?\n\niRiver had some cool devices that play .ogg, but their newer 'U' models don't support USB Mass Storage, which makes them as good as useless for many people who want .ogg support.  At least, the Europe and US version of those models don't.\n\nPity, I would have bought one otherwise."
    author: "mart"
  - subject: "Re: amarok's really cool, except for one thing..."
    date: 2006-05-18
    body: "I believe most of them can be switched to UMS mode actually (though I have one of the H320, gotta love the OGG/video support).  Though unfortunately there aren't many players that support ogg."
    author: "Corbin"
  - subject: "Re: amarok's really cool, except for one thing..."
    date: 2006-05-18
    body: "I compiled Amarok from sources. I run mandriva, but all KDE stuff is home-built."
    author: "Guillaume Laurent"
  - subject: "Re: amarok's really cool, except for one thing..."
    date: 2006-05-19
    body: "Then your compiling was faulty :o)\ncheck the readme file for the right arguments for ./configure"
    author: "AC"
  - subject: "Re: amarok's really cool, except for one thing..."
    date: 2006-05-19
    body: "Dude, if you don't understand what you're talking about, just don't say anything :-)"
    author: "Guillaume Laurent"
  - subject: "Re: amarok's really cool, except for one thing..."
    date: 2006-05-19
    body: "Well, you don't need xine or gstreamer for playback, Goodold aRts will do just fine.\nAnd there are several other engines available for amaroK, like aKode, NMM, helix...\n"
    author: "AC"
  - subject: "Re: amarok's really cool, except for one thing..."
    date: 2006-05-19
    body: "You got Xine from PLF, right?\n\nJohn."
    author: "odysseus"
  - subject: "Re: amarok's really cool, except for one thing..."
    date: 2006-05-19
    body: "No, from mandriva, but strangely enough the libs are from PLF (I have both repositories configured as urpmi media). Apparently I only had the libs installed but not xine itself last time I tried (yet amarok's configure didn't see that). I'll try again now that xine is installed.\n\nIt's still a useability bug IMHO : amarok should at least give a hint on how to solve the problem of the xine engine failing. Not that I can blame them too much about it, we have a similar problem with Rosegarden when the sequencer process fails."
    author: "Guillaume Laurent"
  - subject: "Re: amarok's really cool, except for one thing..."
    date: 2006-05-19
    body: "What about helix, SuSe is shipped with Helix plugin on the add-on CD "
    author: "elek"
  - subject: "Re: amarok's really cool, except for one thing..."
    date: 2006-05-19
    body: "I was wondering the same. I always used Xine as playback engine.\nNow with Gstreamer maturing and Helis being supplied with SUSE, which one is a better option with aMarok, and why?"
    author: "E@zyVG"
  - subject: "Re: amarok's really cool, except for one thing..."
    date: 2006-05-19
    body: "xine, because it's both mature and supplied with suse (well at least with packman)"
    author: "wi"
  - subject: "more more always more"
    date: 2006-05-18
    body: "This is the sound of the new fuzz/buzz/<cool name goes here>.\n\nRediscover More and Everything From Your Music version 1.5.\n\nThe development state in amaroK has taken a place where we fit \nit with lots of code regardless of simplicity. We could have a nice \nplayer which doesn't split functions over dozen of entry points. \nBut with amaroK we have. After all this coding who will clean this \nup?\n\nAnd my 2 cents ;)"
    author: "Mike"
  - subject: "Re: more more always more"
    date: 2006-05-18
    body: "well, if you just want an mp3 player, why use amarok!?\n\nit clearly isn't aimed at people who don't want all the stuff..."
    author: "ac"
  - subject: "Re: more more always more"
    date: 2006-05-19
    body: "yep, use kaboodle or something similar :o)"
    author: "AC"
  - subject: "Re: more more always more"
    date: 2006-05-19
    body: "Ah right ;)\nYesterday I found moc."
    author: "Mike"
  - subject: "lost playlist"
    date: 2006-05-18
    body: "that wasnt so sweet, i dont care about collection :D"
    author: "kaulishs"
  - subject: "video support !!!"
    date: 2006-05-19
    body: "Still no video support ! such a pity"
    author: "none"
  - subject: "Re: video support !!!"
    date: 2006-05-19
    body: "Now why exactly would you want video support in a music player?"
    author: "wim"
  - subject: "Re: video support !!!"
    date: 2006-05-21
    body: "Well, for many reasons. One, it would be cool to have a central location where all of your media files can be played. Also, amaroK has such a good UI that it would only be a compliment to have video support. Video iPods, anyone?"
    author: "CplusGod"
  - subject: "Re: video support !!!"
    date: 2006-05-26
    body: "Many music clips are videos, right now. On Xmms, I listen to the music an eventually, a small window with a video pops up and if I want to, I view it on fullscreen, and If I don't, just stays there on top, while I work.\n"
    author: "Tristan Grimaux"
---
The <a href="http://amarok.kde.org">amaroK</a> team have <a href="http://amarok.kde.org/amarokwiki/index.php/Announce_1.4">announced</a> the official release of amaroK 1.4, and the launch of the <i>Fast Forward</i> series,  the cheeky successor to the well-received <i>Airborne</i> series. <i>Fast Forward</i> comes with improved media device support, featuring enhanced iPod support that handles the latest iPod devices, support for IFP/IRiver devices, a new plugin for generic media devices, and the ability to handle as many of these devices as you'd like.

<!--break-->
<img src="http://static.kdenews.org/danimo/amarok-1.4.png" align="right" />
<p>Changes to the way song lyrics are fetched mean you're now able to write scripts for your favorite lyrics site. The collection scanner has moved to its own process, keeping amaroK more responsive and stable.</p>

<p>One of the most requested features, user-chosen song ratings, has made it into amaroK 1.4 <i>Fast Forward</i> as a complement to the automated score system, allowing you to set a song's quality from 1 to 5 points, as well as the ability to use these ratings when creating smart playlists.
</p>

<p>Rediscover more of your music! amaroK now has tag support for Ogg, MP3, MP4/AAC, FLAC and Real Media, allowing all these music formats to be added to your collection.
</p>

<p>Check out all the new features in the <a href="http://amarok.kde.org/amarokwiki/index.php/What's_New_in_1.4">What's new in Fast Forward</a> guide and get futher information about this new amaroK release at <a href="http://amarok.kde.org/content/view/74/66/">amarok.kde.org</a>. A list of current available packages is online in the <a href="http://amarok.kde.org/amarokwiki/index.php/Download">amaroK wiki</a>.
</p>
