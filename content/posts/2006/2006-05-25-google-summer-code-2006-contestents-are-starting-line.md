---
title: "Google Summer of Code 2006: The Contestents Are At The Starting Line!"
date:    2006-05-25
authors:
  - "tmacieira"
slug:    google-summer-code-2006-contestents-are-starting-line
comments:
  - subject: "Looks good"
    date: 2006-05-25
    body: "There's quite a nice spread of projects for KDE this year. I look forward to reading descriptions of each one when they're available and hopefully seeing the results too."
    author: "Paul Eggleton"
  - subject: "Not so interesting"
    date: 2006-05-25
    body: "Neither of those projects makes a real difference for users... Weren't there more interesting ones?"
    author: "gooth"
  - subject: "Re: Not so interesting"
    date: 2006-05-25
    body: "\"Neither\"?  Theres 24 projects (just click the link!).  The QQ plugin for Kopete will be quite popular with people that live in/communicate with people in China.  The 'Rapid linux desktop startup through pre-caching' will make my laptop using experience much nicer, 'KDEPIM Google Calendar synchronization using OpenSync' will be useful for people that use Google's Calendar and have a PDA or other similar device, 'Oscar Filetransfer for Kopete' will be useful for everyone that uses Kopete and the Oscar protocol (AIM/ICQ) that ever wants to transfer files to a friend without having to upload to some random server.  \n\nHopefully longer descriptions will be added soon so it will be easier to figure out what some other the other projects are about (such as 'Advanced Session Management')."
    author: "Corbin"
  - subject: "Re: Not so interesting"
    date: 2006-05-25
    body: "Exactly. I especially like NX integration, OpenSync, Oscar Filetranfer (yay, ICQ images :-) )."
    author: "Michael Jahn"
  - subject: "Re: Not so interesting"
    date: 2006-05-29
    body: "Don't really see any point in writing an OS NX client? What is the official NX client not good enough? It's free to download, don't know why anyone would waste its time on it given FreeNX is a load of ..., well let's say it's subpar ;)"
    author: "anonymous"
  - subject: "Re: Not so interesting"
    date: 2006-05-27
    body: "Well mine is basically to add support for the iTunes shared music to amaroK. Ask anyone in a college dorm, its useful."
    author: "Ian Monroe"
  - subject: "KPhysics ? "
    date: 2006-05-25
    body: "There was another project for physics in soc 2006, called physika. physika would be a better choice because:\n\n1) the name is better\n2) it is an acronym(and a good one): PHYsics SImulation and eduKAtion\n\nSeriously, i think the two softwares can coexist, because they are different. physica (the qt version) is under development in gna.org . It is just the beggining, but i think we will have the first part ready in august. It would be better to have google's money for this, for one of the students, but, if it wasnt possible, we are going to do it anyway\n\nP.S. When summer of code ends, change the name: KPhysics is terrible"
    author: "Henrique Marks"
  - subject: "Re: KPhysics ? "
    date: 2006-05-25
    body: "Are you being serious?\n\nCan't we just get rid of people like this? You honestly listed two reasons why your project was better and they both were \"it has a better name.\" Honest to god, how do you think you are making a difference by leaving a post like this? Get control of your jealously and think before you speak next time... "
    author: "Matt"
  - subject: "Re: KPhysics ? "
    date: 2006-05-25
    body: "I think it was the naming, not the project. And he said they would do their project anyway regardless SoC or not.\n\nGet control and think before you speak next time..."
    author: "Faber"
  - subject: "Re: KPhysics ? "
    date: 2006-05-25
    body: "Take it easy Matt. Cant you read correctly my post? i cited two reasons, and then i said:\n\n\"Seriously, bla bla bla\", meaning that i wasnt being serious about the two reasons.\n\nWhy i need to explain this ? Irony should be self-explained.\n\nBut i saw the specs of the kphysics program, earlier than soc, because i was discussing the project with a KDE developer earlier. And i must say the projects will be different, and this is nice and good. Many educational programs are a good thing, we all agree on this. They must have a good quality, of course, but with more people working, better chances we have :-)\n\ni was not running for the dollars, because i am a professor, so i could participate. But i think that the choice for an educational program, regardeless which one, is GOOD.\n\nP.S. Change the name after SOC. Dont forget it.\n"
    author: "Henrique Marks"
  - subject: "Re: KPhysics ? "
    date: 2006-05-25
    body: "     Sweet, another educational program. They are my favorite part of KDE. Kalzium is stunningly full-featured and there's lots of good stuff at KDE-Apps (I love solseek). \n     I look forward to seeing how both these projects progress. I agree Kphysics tends to confirm the stereotype of K-random word naming of KDE, but you don't make it far in linux without learning to live with some rather odd naming choices. ;-) Good luck on your project!"
    author: "sundher"
  - subject: "I too love Kalzium"
    date: 2006-05-25
    body: "I agree about Kalzium, one of my favorite applications by a large margin. It has some many little thoughtful touches and the interface is so easy to learn.\n\nThe timescale slider and the \"state\" slider that shows how different elements change state under different temperatures is beautiful.\n\nKeep on rocking. You guys are making the world a better place one line of code at a time.\n\nHeartfelt thanks,\n\nGonzalo"
    author: "Gonzalo"
  - subject: "Re: KPhysics ? "
    date: 2006-05-25
    body: "> 2) it is an acronym(and a good one): PHYsics SImulation and eduKAtion\n\nSeriously?! That's worst and most contrived acronym I've ever heard. And you didn't even spell 'education' right!"
    author: "Tim"
  - subject: "Re: KPhysics ? "
    date: 2006-05-25
    body: "> 2) it is an acronym(and a good one): PHYsics SImulation and eduKAtion\n \n>> Seriously?! That's worst and most contrived acronym I've ever heard. And you didn't even spell 'education' right!\n\nSeriously seriously ?! Thank you. You never saw another one better than this ? bash, for instance, is my favorite. GNU is old fashion, but still nice. And education with a K is important for us KDE users, to recognize the application. This discussion has been made so many times that we all know the arguments.\n\nphysica, in gna.org, is spelled correctly. As it is qt based, we thought about physiqa, but this is way to far. physiqa, a program to teach ducks. Not good :-)."
    author: "Henrique Marks"
  - subject: "Re: KPhysics ? "
    date: 2006-05-25
    body: "I like the fact that Physika becomes pronounced in english how Physics is pronounced in other languages... makes the name more universal. And its not tongue-twisting for english speakers either."
    author: "A Polish Canadian"
  - subject: "GMail style conversations for KMail"
    date: 2006-05-25
    body: "Dream come true!"
    author: "Paul F"
  - subject: "Re: GMail style conversations for KMail"
    date: 2006-05-25
    body: "Um, can anyone enlighten me as to what a \"GMail style conversation\" is? Thanks!"
    author: "DaveC"
  - subject: "Re: GMail style conversations for KMail"
    date: 2006-05-25
    body: "Something like this: http://mail.google.com/mail/help/images/inbox2_lg.gif"
    author: "Arzie"
  - subject: "Re: GMail style conversations for KMail"
    date: 2006-05-25
    body: "It means that all e-mails of a thread are displayed together, like the combined view of akregator, but with the possibility to collapse allready read messages. The message list doesn't expand threads but considers a thread to be a \"conversation\". The conversations are sorted by their most recent messages, rather than their first messages as with standard KMail threads.\n\nhttp://www.forumscaniae.net/soc/images/9/98/View.png\nhttp://www.forumscaniae.net/soc/images/9/96/List1.png\n\n//the student"
    author: "Aron Bostr\u00f6m"
  - subject: "Still working on KPDF?"
    date: 2006-05-25
    body: "I thought KPDF had to be dropped in favour of the format-agnostic viewer ..."
    author: "Oblomov"
  - subject: "Re: Still working on KPDF?"
    date: 2006-05-25
    body: "yes, kpdf has morphed into okular for kde4. i imagine the kpdf project will happen in okular or, worst case, against kde3 kpdf and then forward ported."
    author: "Aaron J. Seigo"
  - subject: "Re: Still working on KPDF?"
    date: 2006-05-25
    body: "Commenting tools for KPDF\n\nWow, that means annotations in KPDF... This is a *very* good news (at least for me). Who knows, perhaps this can be integrated in KDE 3.5.5? :)"
    author: "Carsten Niehaus"
  - subject: "Re: Still working on KPDF?"
    date: 2006-05-25
    body: "I too would love to be able to include comments in KPDF. It would make academic work so much easier. I can just produce my files in lyx or kile, create a PDF and annotate any comments on it. That would rock!\n\nThe 3.5.x series has been wonderful and I hope that cumulative updates and releases continue happen until KDE 4 is the stellar release that we all want it to be, which, at the current pace, will be towards the early fall of 2007."
    author: "Gonzalo"
  - subject: "Early Fall??"
    date: 2006-05-26
    body: "Early Fall? So end of 2007?\n\nAnd no release in between?"
    author: "And"
  - subject: "KDE4"
    date: 2006-05-25
    body: "This list of accepted projects is quite confusing, so I was just wondering whether any of these projects would speed up the KDE4 development."
    author: "Jan"
  - subject: "Re: KDE4"
    date: 2006-05-25
    body: "I assume these might be:\n\nAdvanced Session Management\navKode - A Phonon Backend using FFMPEG\nPhonon Backend using NMM\nRapid linux desktop startup through pre-caching\nWriting a widget (View/Delegate) for KOffice 2.0 based on Qt4's Model/View architecture \n\nmaybe WorKflow (what does this do?)\n\nBut keep in mind that the SoC is not meant to speed up development or even provide useful code, it's meant primarily to introduce students to free software development (if I got that right).\n"
    author: "assume"
  - subject: "Re: KDE4"
    date: 2006-05-25
    body: "WorKflow is going to be the logical next step after OS X Automator (http://www.apple.com/macosx/features/automator/), bringing DCOP and the command line to the end user. It will automate boring tasks in a very easy way."
    author: "Thomas Kadauke"
  - subject: "Re: KDE4"
    date: 2006-05-25
    body: "So will it be a GUI for writing bash scripts? In that case, it won't bring the command line to the end user.\n\nOr will it allow assembling actions graphically? Something like simple visual scripting?\n"
    author: "assume"
  - subject: "Re: KDE4"
    date: 2006-05-26
    body: "the latter one :)"
    author: "Thomas Kadauke"
  - subject: "Isn't FFMPEG already included in Xine?"
    date: 2006-05-25
    body: "What's the point of a Phonon backend using FFMPEG? I thought Xine already used FFMPEG and that Xine can do everything FFMPEG can, and more. See the Xine website:\n\nhttp://xinehq.de/index.php/news"
    author: "AC"
  - subject: "Re: Isn't FFMPEG already included in Xine?"
    date: 2006-05-25
    body: "ffmpeg is a framework in itself so it's probably much more efficient to use it directly then through xine, also if you would only use ffmpeg directly you wouldn't have a xine dependancy on your install.\n\nI suppose the reasoning will be somewhere along those lines."
    author: "Mark Hannessen"
  - subject: "Re: Isn't FFMPEG already included in Xine?"
    date: 2006-05-25
    body: "But isn't having a FFMPEG backend wasteful. I mean, why wouldn't one want to have Xine installed if one needs multimedia? Also having too many choices (for backend) is a problem.\n\nInstead the time and energy could have been employed to do something that is more useful."
    author: "nilesh"
  - subject: "Re: Isn't FFMPEG already included in Xine?"
    date: 2006-05-26
    body: "> Instead the time and energy \n\nAnd why do you care about the time and energy of another person?"
    author: "cl"
  - subject: "Re: Isn't FFMPEG already included in Xine?"
    date: 2006-05-26
    body: "> And why do you care about the time and energy of another person?\n\nbecause he will paid (if the project completes) by Google. A more useful project could have been selected instead."
    author: "nilesh"
  - subject: "Re: Isn't FFMPEG already included in Xine?"
    date: 2006-05-26
    body: "Because I looked very hard at the xinelib interface and realized that using FFMPEG directly was easier, faster and cleaner. \n\nBasically\n* FFMPEG interface is ugly\n* Xinelib interface is horribly ugly\n\nI prefered FFMPEG."
    author: "Carewolf"
  - subject: "Re: Isn't FFMPEG already included in Xine?"
    date: 2006-05-26
    body: "and what (in your opinion) engine has the best api?\ngst10, other?"
    author: "Nick"
  - subject: "Re: Isn't FFMPEG already included in Xine?"
    date: 2006-05-31
    body: "Perhaps you could judge yourself. Take a peek at the code for amoroK or Juk, as they both has support for multiple backends. After doing the same my opinion would be NMM, the code being at a size of 1/3-1/2 that of gst. "
    author: "Morty"
  - subject: "Re: Isn't FFMPEG already included in Xine?"
    date: 2006-05-27
    body: "But Xine-lib also brings together other libraries that can play other codecs, so I expect that's where the new complexity comes from. If you don't like its API or you think it has performance problems, then you should fix up Xine-lib rather than starting from scratch and reinventing the wheel. If everybody did that, we wouldn't get anywhere."
    author: "Rich"
  - subject: "Re: KDE4"
    date: 2006-05-25
    body: "the \"linear scheduler\" is another one that is aimed squarely at kde4."
    author: "Aaron J. Seigo"
  - subject: "Re: KDE4"
    date: 2006-05-26
    body: "It would be useful to provide more detailed explanation of the projects on this list of projects.\nMaybe each project should have a page in KDE wiki?"
    author: "Krzysztof Lichota"
  - subject: "Re: KDE4"
    date: 2006-05-25
    body: "All 3 KDevelop projects are the part of KDevelop4 development efforts."
    author: "adymo"
  - subject: "Re: KDE4"
    date: 2006-05-26
    body: "yeah, i hope in the next course i'll be lucky to get my teachers (kpi, kiev) on FLOSS side w/ beauty of kdevelop :)"
    author: "Nick"
  - subject: "A few very nice choices"
    date: 2006-05-25
    body: "Some very interesting projects. Of particular interest to me are daap support for amaroK (the one thing banshee still has over amaroK), and gmail-style conversation for KMail. Those would be really handy. Screen recording application for KDE and KPhysics sound like they could shape up to be really great, too. Good luck to all those partaking. :)"
    author: "apokryphos"
  - subject: "Re: A few very nice choices"
    date: 2006-05-26
    body: "Nice to hear of the interest in gmail-style conversations for KMail. If your interested in following the progress, there is a blog at http://summerOfKode.blogspot.com/"
    author: "Aron Bostr\u00f6m"
  - subject: "Make KOffice a project of its own"
    date: 2006-05-25
    body: "Wouldn't it be smart to make KOffice a project of its own -- I'm sure that would attract more applicant interest, since open-source office applications are all the rage these days. Having KOffice on Google's list of projects will catch the eyes of students looking to help an office suite.\n\nThis seems to have worked well for Abiword and OpenOffice, who each got 5-6 projects accepted."
    author: "AC"
  - subject: "Re: Make KOffice a project of its own"
    date: 2006-05-25
    body: "Yes, I wanted to do that, but it got shot down. I think we need to lobby for it earlier next year."
    author: "Inge Wallin"
  - subject: "Re: Make KOffice a project of its own"
    date: 2006-05-25
    body: "You mean shot or shut?"
    author: "assume"
  - subject: "Re: Make KOffice a project of its own"
    date: 2006-05-25
    body: "Why was it shot down?  Who shot it down?\n\nBobby"
    author: "Bobby"
  - subject: "Re: Make KOffice a project of its own"
    date: 2006-05-25
    body: "Google said no, we have to be within the main KDE project.  This is not strange in itself, but I think that in that case, Abiword (for instance) should be a part of the Gnome project.  And gaim.  And so on...\n\nKDE applications, or rather kdelibs based applications, need to make it known that they are not totally tied to the KDE desktop."
    author: "Inge Wallin"
  - subject: "Re: Make KOffice a project of its own"
    date: 2006-05-25
    body: "It's also partially how many people use the project; Google seems to accept projects that are smaller if they are cross-platform and are used widely. Gaim and Abiword are both ported to Windows and therefore have more exposure. So does GIMP. If/When KOffice is ported to Windows and gains more presence, it'll be easier to argue that KOffice deserves its own category."
    author: "anon"
  - subject: "Re: Make KOffice a project of its own"
    date: 2006-05-26
    body: "it's a sad day when porting to windows makes a -free software- project deserving. if the project wishes to port to windows, great, but if approval is withheld from those who don't want to work on non-free systems doesn't that sort of put the encouragement in an odd direction when it comes to supporting open source projects? hm. *shrugs*"
    author: "Aaron J. Seigo"
  - subject: "Re: Make KOffice a project of its own"
    date: 2006-05-26
    body: "They are open source projects.  It's a sad day when certain people don't want open source projects to be accessible to the most people possible."
    author: "ac"
  - subject: "Re: Make KOffice a project of its own"
    date: 2006-05-26
    body: "If you add the GNOME, GIMP, Gaim and Abisource projects together, then the GNOME folks got 40 projects, while KDE only got 24 :("
    author: "Thomas Kadauke"
  - subject: "Mmmm"
    date: 2006-05-25
    body: "I dont find those projects too exciting either. Not that they are completely unnecessary - far from it. But I certainly could think of more interesting projects. But I dont have to program them, so I know that I'm not really fair ;-) Sorry for that. Anyway, perhaps for Summer of Code 2007,2008 & 2009:\n\n* Proper table support in KWord\n* Better PNG/transparency support (Checkerboard, Color to alpha) in Krita\n* Better image resize dialog in Krita\n* KPDF editing capabilities like in Acrobat (not Acrobat Reader)\n* Completely integrated out-of-the-box Bittorrent-support for KGet/Konq\n* Google cover search for Amarok\n* Mouse-over to read article for KNewsTicker\n* Skype support for Kopete\n* Service-menu configuration panel for KControl\n* Color-coding for KMail\n* Seamlessly integrated Spam-Filter for KMail (like in Thunderbird)\n\nNow, come on, get mad at me for posting this. I can stand this ;-)\n"
    author: "Martin"
  - subject: "Re: Mmmm"
    date: 2006-05-25
    body: "How often do we have to say that BitTorrent support makes no sense in Konqueror? Maybe it does in KGet, but KTorrent is doing the job just fine. Torrents aren't normal downloads. You need to have a special application for it because you want to set your upload rate, number of downloads, share ratio, etc.\n\nSkype support for Kopete will be much easier to implement next year, when we have moved to D-Bus.\n\nColor-coding for KMail? What do you mean by this? KMail already colours the multiple indentation levels in different colours.\n\nAnd it has spam-filter (spamassassin) integration."
    author: "Thiago Macieira"
  - subject: "Re: Mmmm"
    date: 2006-05-26
    body: "KTorrent: I know all the arguments against this but I'm 100% sure this will get implemented one day. At least when all the other browsers (except perhaps IE) have implemented this. Opera is planning to do so with Opera 9 AFAIK so strangely they dont seem to have the same reservations.\n\nSkype support will only really make sense standalone without calling a running Skype.\n\nWith Color-coding I mean the ability to right-click and mark mails with different colors like in Thunderbird.\n\nI know about the Spam-Filter wizard but it isnt as nicely integrated and easy to setup as the Bayesian Spam-Filter in Thunderbird which worked for me right out-of-the-box and was the reason why I finally switched to Thunderbird (I used KMail a long time before). Maybe I'll switch back one day. Fortunately because of IMAP switching clients really isnt a big issue for me.\n"
    author: "Jan"
  - subject: "Re: Mmmm"
    date: 2006-05-26
    body: "I don't think you get it. BitTorrent is already integrated in Konqueror by KTorrent. The KDE framework is just much more flexible, so we don't need to put things together in the same module for them to integrate nicely.\n\nYou could argue that KTorrent should use the UI-server to make it look like any other download, but that's a detail in KTorrent and won't require any changes anywhere else."
    author: "Carewolf"
  - subject: "Re: Mmmm"
    date: 2006-05-26
    body: "It looks like KGet will get Bittorrent support. http://bugs.kde.org/show_bug.cgi?id=57591\n\nI must agree that Konqueror getting Bittorrent support doesn't make much sense. Opera may have torrent support but integration seems to be their MO since they also have integrated mail and IRC, but which aren't as good as standalone versions of these apps. Out of curiosity, what's wrong with the spam filter? It's a three window wizard."
    author: "sundher"
  - subject: "Proposals posted on Google's website"
    date: 2006-05-26
    body: "For the full text of the proposals, they are listed on Google's website.\n\nhttp://code.google.com/soc/kde/about.html\n\nHopefully this answers a lot of people's questions."
    author: "anon"
  - subject: "Re: Proposals posted on Google's website"
    date: 2006-05-29
    body: "Well the page you gave only contain the tittle.."
    author: "renox"
  - subject: "Full featured open source NX client for KDE"
    date: 2006-05-26
    body: "KNX/NXc\n\nWhy not further work on Krdc to reach the desired functionality. We'd all love to see NX functionality where it belongs, with the other remote terminal viewers."
    author: "James Smith"
  - subject: "Next year"
    date: 2006-05-26
    body: "Next year it will be important to present more projects which contribute to KDE in order to get more slots.\n\nI think e.g. OpenUsability would qualify as an independend project and this also applies to other projects within the KDE community such as Valgrind."
    author: "hup"
  - subject: "See the past before looking at the future !"
    date: 2006-05-31
    body: "Just see what happened to last year's SOC? Possibly there were far more exciting projects like the xul and USMT and the fully integrated NX client.\n\nOne good piece of software produced by a SOC for desktop users was K3B but probably we should keep our expectations at halt and assign smaller set of works for a SOC project which could be dealt moderately in a vacation."
    author: "Manik Chand"
---
KDE is happy to announce the selection of <a href="http://developer.kde.org/summerofcode/soc2006.html">24 student applications</a> for the <a href="http://code.google.com/soc/">Google Summer of Code 2006</a>. This year, Google received a total of 6400 applications worldwide spread across 102 different Open Source organisations. <i>"It looks like we've got some very interesting projects for KDE as a whole, and a good number of projects for KOffice"</i>, said Boudewijn Rempt, the maintainer for Krita, celebrating the selection of 4 KOffice student proposals. <i>"We spent over 3 hours debating the final list of projects on Sunday evening, but we're confident our selection is solid"</i>, commented Cornelius Schumacher, the KDE e.V. vice-president who will act as a mentor for one of the 3 KDE PIM projects. The list is completed by 3 KDevelop projects, 2 Kopete ones, one for KHTML/KJS and 11 others in various areas of KDE.


<!--break-->
<p>
The full list of selected applications is available in the <a href="http://developer.kde.org/summerofcode/soc2006.html">KDE Summer of Code</a> website. Some of the students in that list have reported they have already started working on their projects -- and some who did not get selected have also said they will work on their ideas regardless of the outcome.





