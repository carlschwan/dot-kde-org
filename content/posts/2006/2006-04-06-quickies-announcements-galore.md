---
title: "Quickies: Announcements Galore"
date:    2006-04-06
authors:
  - "jriddell"
slug:    quickies-announcements-galore
comments:
  - subject: "Open bugs for qt"
    date: 2006-04-06
    body: "A collection of arabic-related bugs in KDE and QT: http://www.arabic-fedora.org/munzir/OpenBugs.html"
    author: "Osbeilis"
  - subject: "Re: Open bugs for qt"
    date: 2006-04-07
    body: "sigh. looks like a long list. i dont envy you; \nisnt it just horrible that the international community has to fight again and again for proper support of their language? \n\nwe (i.e. everything speaking a non-english language) all know it and have come to hate it. keyboards dont work, accents cannot be entered, fonts are not printed properly. And only the optimistic can see really a decline here.\n\nBad enough that non-english languages were completely forgotten when inventing email, www, etc. But inexcusable that still more and more of such problems come up today in _new_ technology. My colleague still cannot write turkish texts properly on his mobile phone, I'm fighting with lack of utf-8 support in Kompare every day (it's destroying files without warning if you use it - so dont), in my car I cannot set the clock to 15:20 but only to 3:20.\nThe list goes on and on... \n\nIMO the absolute worst #1 on this hall of shame: Mozilla/Firefox still does not support shy/hyphen character after 6 years of development - destroying layouts completely in all languages with long words. \n\nKudos to KDE though for improving a lot on many internationalization issues\nand still being the best option there is if you need support for many different languages across the board.\n\n"
    author: "Martin"
  - subject: "Re: Open bugs for qt"
    date: 2006-04-08
    body: "\"isnt it just horrible that the international community has to fight again and again for proper support of their language?\"\n\nLet me throw a wet blanket on your rant: the \"international community\" needs to get involved and stop expecting the \"English community\" to do their work for them. You cannot sit back and expect stuff to happen magically. A tiny development team cannot possibly manage every language and locale. This is why many Open Source projects are far better in this regard than closed source: there is a greater chance of people from around the world participating."
    author: "Brandybuck"
  - subject: "Re: Open bugs for qt"
    date: 2006-04-09
    body: "> Let me throw a wet blanket on your rant: the \"international community\" needs \n> to get involved and stop expecting the \"English community\" to do their work\n> for them.\n\nIt might be good idea in theory, but in practice these tiny development teams just reject patches they don't understand and they don't understand most of internationalisation issues. Even simple issues which are common for many European languages like more than one plural form are not easy to understand for developers. But with issues specific to some languages ...\n\nIn Estonian there is a \"problem\" - although using iso-8859-15, \"z\" isn't the last one in the alphabetical order, ascii letters \"t\", \"u\", \"v\" etc. are after \"z\". Obviously regexes like [a-z] don't work as expected with Estonian locale. And now grep through your binary directories in your OS to see how many scripts are using this. Many programs don't compile because of build scripts etc. using [a-z]. Guess how many development teams accept patches regarding this issue? \"Use C locale to compile/execute software.\" is the answer I mostly get. Lovely?"
    author: "Hasso Tepper"
  - subject: "Trolltech @ Adlershof, Berlin"
    date: 2006-04-06
    body: "I'm working at Fraunhofer FIRST at exactly this location. So Trolls, when you arrive, give me a ring!\n\nCheers,\nCarsten"
    author: "Carsten Pfeiffer"
  - subject: "Re: Trolltech @ Adlershof, Berlin"
    date: 2006-05-02
    body: "mit fraunhofer deppen wollen was nix zu tun haben...."
    author: "LEPSAI"
  - subject: "Thanks Trolltech"
    date: 2006-04-07
    body: "you've kept your word. hindi bugs are fixed, which crashed all KDE applications in qt 3.3.4 and fixed hindi text garbage while typing in qt 3.3.5.\n\nI appreciate the bug fix release QT 3.3.6! thanks, you Trolls rock!"
    author: "fast_rizwaan"
  - subject: "Congrats Anma"
    date: 2006-04-09
    body: "I think I certainly speak for everyone when I say warmest congratulations, Anma. =)"
    author: "John"
---
A quick roundup of a few important announcements from Trolltech and others.  Last month Qt developers Trolltech released bugfix releases <a href="http://www.trolltech.com/newsroom/announcements/00000246.html">Qt 3.3.6</a> and <a href="http://www.trolltech.com/newsroom/announcements/00000250.html">4.1.2</a>. *** Trolltech have also announced that they will be <a href="http://www.trolltech.com/newsroom/announcements/00000253.html">opening offices in Germany</a>, the requirement to work there is <a href="https://www.webcruiter.no/wcmain/AdvertViewPublic.aspx?company_id=216048">incredible coding skills</a>. *** Thiago announced he is <a href="http://lists.kde.org/?l=kde-core-devel&amp;m=114218369631259&amp;w=2">collecting requests from KDE developers for Qt</a>. *** On April 1st the amaroK developers surprised the world by announcing <a href="http://paleo.pwsp.net/amarokwin32.jpg">amaroK 2.0</a>. *** <a href="http://news.zdnet.com/2100-3513_22-6057373.html">ZDNet took a look</a> at the <a href="http://portland.freedesktop.org/wiki/">Portland Project</a> which announced a technology preview of their platform for ISVs needing cross desktop compatability.  *** <a href="http://www.kde-forum.org/" rel="nofollow">KDE Forum</a> is back online with a new look and new moderators, thanks Bram and Ruurd.  *** Finally congratulations to Annma who <a href="http://annma.blogspot.com/2006/03/no-k-in-clarisse.html">announced the newest KDE Edu tester</a>.











<!--break-->
