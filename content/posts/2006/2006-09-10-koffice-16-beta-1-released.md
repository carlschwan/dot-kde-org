---
title: "KOffice 1.6 Beta 1 Released"
date:    2006-09-10
authors:
  - "iwallin"
slug:    koffice-16-beta-1-released
comments:
  - subject: "Bug reports"
    date: 2006-09-10
    body: "Of course testers are encouraged to report issues to bugs.kde.org. "
    author: "Jaroslaw Staniek"
  - subject: "Hey,"
    date: 2006-09-10
    body: "Koffice is really getting interesting now."
    author: "ale"
  - subject: "packages"
    date: 2006-09-11
    body: "Does anybody know when (if ever) are kubuntu packages going to ship?"
    author: "shiny"
  - subject: "Re: packages"
    date: 2006-09-11
    body: "Already up since yesterday I think.\n\nhttp://kubuntu.org/announcements/koffice-16beta1.php"
    author: "anononymour"
  - subject: "Re: packages"
    date: 2006-09-11
    body: ">:(\nI'm obviously blind. Thx!"
    author: "shiny"
  - subject: "help"
    date: 2006-09-11
    body: "I don't even know where to start to install this on Mandriva.  Please help. :("
    author: "ac"
  - subject: "Re: help"
    date: 2006-09-11
    body: "If Mandriva doesn't have pre-packaged binaries (rpm's), then you'll have to build it straight from the source code.  Google might help, and asking around on the mandriva irc/forums may also help.  goodluck."
    author: "Vlad"
  - subject: "Re: help"
    date: 2006-09-11
    body: "If you are running Cooker, type into a root console:\n\n]# urpmi koffice --auto --no-verify-rpm\n\nIf you are running an older release you should compile yourself or keep running updates until its released. Its best to wait for Mandriva rpm releases anyhow so you dont go accidentally breaking other things at the same time."
    author: "Peter"
  - subject: "Kword tables?"
    date: 2006-09-11
    body: "Is there going to be some major work on Kword's table support? That is one think that I really think should be worked on for the next release..."
    author: "Scott"
  - subject: "Re: Kword tables?"
    date: 2006-09-11
    body: "Yes, version 2.0 will contain a totally new implementation of tables.  1.6 does not have any new stuff in KWord, only some bugfixes."
    author: "Inge Wallin"
  - subject: "Re: Kword tables?"
    date: 2006-09-15
    body: "Phew... That's one think I find really annoying. I'm really glad it's goign to be majorly worked on/fixed in the 2.0 release..."
    author: "Scott"
  - subject: "Re: Kword tables?"
    date: 2006-09-13
    body: "That and linenumbers and KBibTex-integration :)"
    author: "Carsten Niehaus"
  - subject: "Klil"
    date: 2006-09-11
    body: "Are there going to be \"Klik\" packages of this release? I think Klik is the fastest and easiest way to temporarily install software without messing up my installation."
    author: "ac"
  - subject: "Re: Klil"
    date: 2006-09-14
    body: "PLEASE PLEASE PLEASE someone get a klik package for Koffice 1.6b... I would love to help test the software and do bug fixes, and klik is BY FAR the easiest way to do that.\n\nBobby"
    author: "Bobby"
  - subject: "From the changelgo"
    date: 2006-09-12
    body: "I was hoping there might be some Karbon changes list in the changelog but sadly not but I did notice something I hope can be added to all of Koffice: \n\nKPresenter\n    * kpresenter can be built stand-alone\n\n\nWhat are the chances of this being done for all the Koffice applications?  \n(Alternatively what level of difficulty would it be to do the same for Karbon, maybe I'd try doing it as a last resort?)\n"
    author: "Alan"
  - subject: "Re: From the changelog"
    date: 2006-09-12
    body: "Hmm, generally it's not that difficult to build the applications standalone.  You need the libs, so after configure, do:\n\ncd lib\nmake install\n\nthen if you just want one application, go into its directory and build it, like: \n\ncd ../kspread (say)\nmake install\n\nIf you also want the import/export filters, then you have to build them too:\n\ncd ../filters/kspread\nmake install\n\nAnd that's it.  Note, that for kchart, you also need to make install in interfaces/, and you might need to go into servicetypes/ and make install to get mime types installed."
    author: "Inge Wallin"
  - subject: "Re: From the changelog"
    date: 2006-09-12
    body: "You might also need to make install in servicetypes/ and mimetypes/ other than libs/ as a general dependency."
    author: "Bart Coppens"
  - subject: "Re: From the changelgo"
    date: 2006-09-12
    body: "Most koffice applications are independent, you can use the environnement variable DO_NOT_COMPILE=\"kword kspread k...\" before calling the configure script to select applications you don't want to build. And there is nothing new in karbon in 1.6, because the authors are doing a massive rewrite of the application for 2.0."
    author: "Cyrille Berger"
  - subject: "Krita~"
    date: 2006-09-14
    body: "Krita is really looking very promising.\nThis perspective grid is very interesting and I can think of several uses for it quickly.\nNow what Krita needs to make it a real killer app is implementing better \"natural drawing\" tools (something like openCanvas, mostly proper watercolors and a decent pencil simulation) and it'll be my drawing tool of choice without a second thought.\nGo for it, you sure are improving!\n"
    author: "Random dude"
  - subject: "Re: Krita~"
    date: 2006-09-14
    body: "I don't know OpenCanvas, but natural drawing tools are exactly what I want to work on. Unfortunately, you need a whole infrastructure setup before you can do that sort of thing. We're getting there, though."
    author: "Boudewijn Rempt"
  - subject: "Re: Krita~"
    date: 2006-09-14
    body: "OpenCanvas is a very nice painting application (mostly with a japanes community from what I have gather), with a quiet unique feature, you can record the strokes and drawing operation, and then replay them on a different computer. But, unfortunately there is no demo of it :'("
    author: "Cyrille Berger"
  - subject: "Re: Krita~"
    date: 2006-09-14
    body: "Er, I just found a trial version of it. Doesn't run under wine, though."
    author: "Boudewijn Rempt"
  - subject: "Re: Krita~"
    date: 2006-09-16
    body: "Uh, got late to reply to this...ahem.\nWell, I was able to make it run in wine, sort of using the defaults in the lastest version. It wouldn't recognize pressure pressure from tablets(for that I use vmware player) but it's enough to check out or watch \"replays\".\nAn vmware setting is recommended, it runs perfectly and can use the tablet's pressure. \nBest regards Mr.Rempt."
    author: "(same) Random dude"
  - subject: "Kword sheet spaces"
    date: 2006-09-15
    body: "I was hoping that Kword could now save Document Structure list size right, after closing Kword. And Kword paper sheets could have space between them, like OpenOffice has.\n\nWell... Still very nice beta :-)"
    author: "Fri13"
---
<a href="http://koffice.kde.org">KOffice</a>, the KDE office suite, today released version 1.6 beta1. This release incorporates a number of new features, mainly from the Google Summer of Code projects, as well as a great number of bug fixes.  It also signals the start of the feature freeze that always preceeds a release of a major new version, thus giving the developers exactly a month to fix outstanding bugs. We urge everybody that is interested in KOffice to install and test this version to make sure that the final
1.6 has a high quality.  To see more details, you can look in the 
<a href="http://www.koffice.org/announcements/announce-1.6-beta1.php">announcement</a>
or <a href="http://www.koffice.org/announcements/changelog-1.6beta1.php">the
full changelog</a>.


<!--break-->
<p>KOffice 1.6 is mainly a feature release for the fastest developing
components <a href="http://www.koffice.org/kexi">Kexi</a> and <a
href="http://www.koffice.org/krita">Krita</a>, but also incorporates
some some new features and bugfixes in all the other components.  You
can look at <a href="http://dot.kde.org/1154424570/">the article about
the release of KOffice 1.6 Alpha</a> to see some of them.  The new
features in the beta are mainly that some of 
<a href="http://dot.kde.org/1157452184/">the projects from the Google
Summer of Code</a> were integrated.
</p><p>

The projects that were integrated into KOffice 1.6 were:
<ul>
  <li>Emanuele Tamponi: A bezier curve tool with stroking using
  brushes and a new outline selection tool for Krita.

  <li>Alfredo Beaumont: Major improvements to OpenDocument support for
  KFormula. 
</ul>
<p>

The other SoC projects were integrated into KOffice 2.0, which is
being developed in parallel with 1.6 and will be released later in
2007.
<p>

Other noteworthy but smaller things in 1.6 Beta1 are tools for
perspective drawing in Krita, new filters in Krita, great speed
improvements in KSpread, Kugar Designer works again and countless
improvements in Kexi. To see some of them, look at 
<a href="http://www.kdedevelopers.org/node/2163">Jaroslaw Stanieks
blog</a>



