---
title: "KDE Commit-Digest for 9th July 2006"
date:    2006-07-10
authors:
  - "dallen"
slug:    kde-commit-digest-9th-july-2006
comments:
  - subject: "kde 4 finally coming"
    date: 2006-07-10
    body: "Good to see real development on KDE4 side. I mean, just not porting to qt4 and improving/cleaning kdelibs but on application side.\nIt's nice glx and compiz support are coming for KDE4, probally when it's launched xgl and aixgl (I hope this is the right name) are already stable enought for day to day use.\nWay to go KDE team!"
    author: "Iuri Fiedoruk"
  - subject: "Re: kde 4 finally coming"
    date: 2006-07-10
    body: "actually it is aiglx ( accelerated indirect glx )\n;-)"
    author: "Mohasr"
  - subject: "Re: kde 4 finally coming"
    date: 2006-07-10
    body: "Thanks!\nI didn't get the name yet because for now it's just vaporware (even that I think it will get there) :)"
    author: "Iuri Fiedoruk"
  - subject: "Re: kde 4 finally coming"
    date: 2006-07-10
    body: "who said it is vaporware ?! no it is not.\nIt is even officially included in the new X.org 7.1 and you can also run compiz with it.\n\nhttp://en.wikipedia.org/wiki/AIGLX\n\"The AIGLX project has been merged into X.org and is available as of X.org 7.1\""
    author: "Mohasr"
  - subject: "Re: kde 4 finally coming"
    date: 2006-07-10
    body: "It's not vapourware. AIGLX has been fully merged with xorg's HEAD."
    author: "Robert"
  - subject: "Re: kde 4 finally coming"
    date: 2006-07-11
    body: "It's not vapourware, but driver support for it does not exist in the nvidia or fglrx closed source drivers.\n\nIf you use an intel card however it is reported to be easier to set up and better performing than XGL.\n\nL."
    author: "ltmon"
  - subject: "Re: kde 4 finally coming"
    date: 2006-09-26
    body: "Well, nvidia drivers are now supporting this, at least the beta drivers on\nnzone have texture_from_pixmap enabled and my desktop is running over aiglx."
    author: "mattepiu"
  - subject: "Re: kde 4 finally coming"
    date: 2006-07-10
    body: ">It's nice glx and compiz support are coming for KDE4\n\nI think most of the recent work by Lubos Lunak with compositing in KDE is based on 2D acceleration that runs on more graphics cards thanks to EXA:\n\nhttp://wiki.x.org/wiki/ExaStatus\nhttp://lists.kde.org/?l=kde-commits&w=2&r=1&s=kwin_composite&q=b"
    author: "AC"
  - subject: "Re: kde 4 finally coming"
    date: 2006-07-10
    body: "Very nice! Eventually kwin will rule them all!\n\nWhat we need now is a compiz-plugin compatible plugin-interface!"
    author: "ac"
  - subject: "Re: kde 4 finally coming"
    date: 2006-07-10
    body: "Well, yes, except that there's nothing like compiz plugin interface. Documentation on Compiz seems to be rather lacking, but it looks like the plugins simply get full access to all Compiz internals. Which would be bloody difficult to map for KWin, that is if I actually wanted to do that (decoration plugins worked that way before KDE3.2 and it sucked). That said, there's chance that a reasonable subset of the compiz \"interface\" could be mapped using a wrapper plugin.\n\n"
    author: "Lubos Lunak"
  - subject: "Re: kde 4 finally coming"
    date: 2006-07-10
    body: "What kind of stuff is Zack Rusin working on these days? He appears to be very talented in that area."
    author: "apokryphos"
  - subject: "KameFu Renamed to GameFu"
    date: 2006-07-10
    body: "Nice to see this KameFu switch and Amarok's too.  I hope other apps do the same for KDE 4.  Some suggestions for annoyingly named apps:\n\nkstars -> Night Sky\nKuickshow -> Image Show, Image View, Picture Show\nKSnapshot -> Screen Shooter, ScreenCap\nOkular -> Ocular\nKolourPaint -> Doodle, Scribble, Paint\ndigiKam -> Digicam\n\nMore annoying is how utilities just stick a K in front.  Why not:\nKFontView -> Font Viewer\nKJobViewer -> Print Job Status\nKonsole -> Console\nKInfoCenter -> PC Info\n\nThe naming of these utilities is just painful.  People are bound to say Apple does iStuff and Windows does WinStuff for their apps.  But you don't see WinDosPrompt, iTerminal, or the like."
    author: "DeKay"
  - subject: "Re: KameFu Renamed to GameFu"
    date: 2006-07-10
    body: "There is one huge benefit to these unusual names.  If you ever run into trouble with any of them, you can easily do a web search on the name get get exactly what you're looking for.  Who else uses KolourPaint [1]?  No one.  But plenty of people use Doodle [2], Scribble [3], and Paint [4].\n\n[1] http://www.google.com/search?q=KolourPaint\n[2] http://www.google.com/search?q=Doodle\n[3] http://www.google.com/search?q=Scribble\n[4] http://www.google.com/search?q=Paint"
    author: "Zippy"
  - subject: "Re: KameFu Renamed to GameFu"
    date: 2006-07-10
    body: "I have to agree. \n\nI think its a bad idea to move away from the K naming scheme because it does emphasize that the apps are kde apps and you need the kde framework to run them (as well getting all of the benefits of the KDE framework) as well as giving them a unique name. \n\nMarketing people are paid a lot of money to come up with outstanding names for their products but since kde is more focused on code/functional excellence (hopefully!!), people shouldn't feel forced to come up with a spiffy generic name.\n\nI guess that when there starts to be significant naming conflicts with other applications, people might start changing the names back.\n\nCheers,\nBen\n"
    author: "Ben Schleimer"
  - subject: "Re: KameFu Renamed to GameFu"
    date: 2006-07-11
    body: "Oh man, so true.  I've been playing with MESS lately, and it is an unholy pain to google for stuff related to it.  It would blow to search for info on how to use 'console.'  Please let that idea die."
    author: "MamiyaOtaru"
  - subject: "Re: KameFu Renamed to GameFu"
    date: 2006-07-10
    body: "Right-click K-menu icon, \"Panel menu\" -> \"Configure Panel\" -> \"Menus\". Change \"Menu item format\" to \"Description only\".\n\nVoila.  No more K names.  What's the big deal?"
    author: "mart"
  - subject: "Re: KameFu Renamed to GameFu"
    date: 2006-07-10
    body: ">>What's the big deal?\n\nWell, people somehow need to have something to complain about. And because of the quality of KDE, it's pretty hard to complain on anything :o)\nSo people start complaining about small things, like non-default icons and application names"
    author: "AC"
  - subject: "Re: KameFu Renamed to GameFu"
    date: 2006-07-11
    body: "What is wrong about K-names? \n\nwhat is horrible are descriptions like\n\nKfontview - a font view program\n\nPrograms need nice generic names like amarok, okular etc.\n\n"
    author: "finnes"
  - subject: "Re: KameFu Renamed to GameFu"
    date: 2006-07-10
    body: "these names are inacceptable as they are not international."
    author: "pentamax"
  - subject: "Re: KameFu Renamed to GameFu"
    date: 2006-07-10
    body: "Ohhhhhhhhhhh, no.\n\nYou forgot about international naming...\n\nAlso, KFontView explains it pretty well...so does digikam and Okular and Konsole is such an abstract concept anyway. The rest, you may be right."
    author: "Joe"
  - subject: "Re: KameFu Renamed to GameFu"
    date: 2006-07-11
    body: "FontView means English language imperialism. The dominance of English terminology is so strong that it needs to be broKen."
    author: "jum"
  - subject: "Re: KameFu Renamed to GameFu"
    date: 2006-07-10
    body: "Generic names like these are a trademark minefield."
    author: "Robert"
  - subject: "Re: KameFu Renamed to GameFu"
    date: 2006-07-11
    body: "> kstars -> Night Sky\n\nI for one won't be changing the name of my app.  People are already familiar with our current name, and as someone else pointed out, a search for \"kstars\" takes you right to our page, whereas searching for \"Night sky\" would do no such thing. \n\nBesides, KStars already has an alternative generic name, as another person pointed out...it's 'i18n(\"Desktop Planetarium\")', and KDE can easily be configured to show that name in the application menu (although you'll still see KStars in the program itself). \n\n[[Fun trivia: there is a spectral class of stars called \"K stars\", so the name is not as apropos-of-nothing as you might think]]\n"
    author: "LMCBoy"
  - subject: "Re: KameFu Renamed to GameFu"
    date: 2006-07-11
    body: "Night Sky is not international, it is an English name and we can bet the name alredy exists."
    author: "gerd"
  - subject: "Re: KameFu Renamed to GameFu"
    date: 2006-07-11
    body: "While it may be clearer to use generic names.  It is absolutely necessary that the official name of an application be unique.  If they don't have unique names you will have all kinds of trouble if you install two apps with the same name.\n\nThe easiest way to do that for KDE is to start them all with 'K'."
    author: "James Richard Tyrer"
  - subject: "Re: KameFu Renamed to GameFu"
    date: 2006-07-11
    body: "Completely agree. Good suggestion.\n\nFortunately KDE naming seems to be going that way.\n\nI'm sure no one would like this, but what about renaming KDE to something cooler sounding?"
    author: "Tim"
  - subject: "Re: KameFu Renamed to GameFu"
    date: 2006-07-12
    body: "I'm sure no one would like this, but what about renaming KDE to something cooler sounding?\n\nHow about \"Kool Desktop Environment\"?!?"
    author: "Gibly"
  - subject: "Compositing"
    date: 2006-07-10
    body: "Is there any chance of a video of some of the new compositing effects that we might be able to look forward to, for us poor souls who are too lazy to compile the experimental stuff for ourselves? :)\n\nIt'd be nice to add something KDE-related to my lickable window manager video collection."
    author: "dolio"
  - subject: "Re: Compositing"
    date: 2006-07-10
    body: "No, there's not any chance, not now. Right now it can do even less than kompmgr. What is needed are people who are capable at graphics stuff (which rules me out) who'd actually write more than just the core that I was able to do. So if there are people able and willing to do it -> https://mail.kde.org/mailman/listinfo/kwin .\n"
    author: "Lubos Lunak"
  - subject: "Komment"
    date: 2006-07-10
    body: "Well, I liked the \"K\" names, but that's a good move if it's going to help promote KDE more"
    author: "ahmed saad"
  - subject: "100% of women?"
    date: 2006-07-10
    body: "Can anyone explain the cryptic Kulow comment \"in my usability tests, 100% of the women like the effect\"?"
    author: "AC"
  - subject: "Re: 100% of women?"
    date: 2006-07-10
    body: "Kpat is being ported to QGraphicsView IIRC"
    author: "Ian Monroe"
  - subject: "Re: 100% of women?"
    date: 2006-07-10
    body: "Coolo usually mentions \"best tester\" or something similar when speaking about KPat, IIRC that \"best tester\" was his mother, maybe now \"100% of women\" is his mother and his wife?\n\nNothing you should feel like a anti-women commentary, just the typical \"i feel inspired\" commit log."
    author: "Albert Astals Cid"
  - subject: "Re: 100% of women?"
    date: 2006-07-10
    body: "AFAIK his wife likes to play KPat. So he probably asked her."
    author: "cl"
  - subject: "Re: 100% of women?"
    date: 2006-07-11
    body: "probably a reference to celeste, the only female attendee at kde four core (which is, as i understand it, one more than in past such core library meetings) who was also our resident usability guru for the week."
    author: "Aaron J. Seigo"
  - subject: "kopete in multimedia"
    date: 2006-07-10
    body: "I noticed that in this and several past releases kopete (kdenetwork) is in Multimedia, is there any reason for that or is it just an overlook?\n"
    author: "Michal"
---
In <a href="http://commit-digest.org/issues/2006-07-09/">this week's KDE Commit-Digest</a>: Kamefu (a multi-machine emulator frontend) has been renamed <a href="http://sourceforge.net/projects/gamefu/">Gamefu</a>. Physiks, a physics educational project, and a project for advanced session management, both a result of the <a href="http://developer.kde.org/summerofcode/soc2006.html">Summer Of Code</a>, are imported into KDE SVN. Work progresses in the "GMail-style conversation view for KMail" and "WorKflow" projects. KDE 4 changes: KPat, a card game application, gets <a href="http://en.wikipedia.org/wiki/OpenGL">OpenGL</a> bling, while <a href="http://en.wikipedia.org/wiki/Kwin">kwin</a> gets experimental compositing support and <a href="http://en.wikipedia.org/wiki/Compiz">compiz</a>-like effects. <a href="http://kpdf.kde.org/okular/">Okular</a> gets support for the <a href="http://en.wikipedia.org/wiki/Tiff">TIFF</a> file format. <a href="http://pim.kde.org/akonadi/">Akonadi</a> advances towards its goals with the import of a command-line and GUI client.


<!--break-->
