---
title: "People Behind KDE: Tom Chance"
date:    2006-01-30
authors:
  - "tbaarda"
slug:    people-behind-kde-tom-chance
comments:
  - subject: "Tom Tom"
    date: 2006-01-30
    body: "I know Tom from his swpat action. If KDE ever needed a lobbying project I would join him."
    author: "aranea"
  - subject: "Re: Tom Tom"
    date: 2006-01-30
    body: "Yes, with Tom being so busy with KDE and other projects, I'm pretty certain he's just too involved to take the time to kick my \"arse.\"\n\nCan someone clarify?  Tom wasn't shipwrecked onto Great Britain was he?  I'd hate to get unwittingly tackled in the Tube while he minds my gap with his boot.\n\nThanks to Tom for his invaluable work with KDE marketing and promotions."
    author: "Wade Olson"
  - subject: "NetworkManager"
    date: 2006-01-30
    body: "OpenSuse 10.1 will include a KDE frontend for NetworkManager I think..."
    author: "Sam Weber"
  - subject: "But SUSEwatcher will be replaced by zen-updater "
    date: 2006-01-31
    body: "SUSEwatcher (based on qt/KDE) will be replaced by zen-updater (GNOME-based, courtesy of our primate friends at Ximian) in SUSE 10.1:\nhttp://lists.opensuse.org/archive/opensuse-factory/2006-Jan/0128.html\n\nThis is just one more step toward Novell's standadrization on GNOME:\nhttp://linux.slashdot.org/article.pl?sid=05/11/05/1620206\n\nI suspect they plan to do a proprietary version of ZenWorks in their Novell Linux Desktop line. \n\nI'd quit SUSE cold turkey and start using Kubuntu -- the only problem is that Kubuntu doesn't have a good tool like YAST to do hardware management (sound, printer, scanner, bluetooth, video card, infrared, TV Card) and administrative tasks (http://shots.osdir.com/slideshows/slideshow.php?release=565&slide=22). Basically YAST does everything I want in a spiffy GUI without making me edit a single config file by hand. If other distros don't want to bother porting YAST, perhaps they can come up with something easier to maintain. \n\nBut then again, hardware management is a complex task, and I doubt any tool that excels at it will stay \"easy to maintain\"."
    author: "Anonymous"
  - subject: "Re: But SUSEwatcher will be replaced by zen-updater "
    date: 2006-01-31
    body: "\"SUSEwatcher (based on qt/KDE) will be replaced by zen-updater (GNOME-based, courtesy of our primate friends at Ximian) in SUSE 10.1\"\n\nWell, the back-end is based on Zen but that doesn't necessarily mean we won't get a KDE front-end. I'm disliking Suse, and especially \"open\"Suse more and more, simply because there are parts of it that are closed and you can't see. For example, the bug report in there I'm apparently not 'authorised' to view.\n\nFrom where I sit KDE is still 'the' desktop to use in Suse and the integration with stuff like other apps through GTKQt, YOU, KInternet and YaST is first class. YaST is still better than anything on any Linux distribution I've seen for what it does - and it requires a lot more work. I just don't see that with Novell's Gnome, and it is going to take them years to get anywhere near it. I just can't see them having the resources to do what they need to do, and at best, if they do indeed head in that direction they are going to become an even poorer image of Red Hat with even more woeful graphical tools holding things back even more........."
    author: "Segedunum"
  - subject: "Whatever happened to YAST for Debian?"
    date: 2006-01-31
    body: "There was a promising start to porting YAST to Debian:\nhttp://yast4debian.alioth.debian.org/\n\nPerhaps the hardware functinality in YAST and SAX2 can be included in Solid.KDE.org, escpecially since they are both GPL and written in qt. Then all distros using KDE will have good hardware setup tools and we wouldn't need to rely on SUSE anymore."
    author: "ac"
  - subject: "Re: But SUSEwatcher will be replaced by zen-updater "
    date: 2006-01-31
    body: "> zen-updater (GNOME-based, courtesy of our primate friends at Ximian)\n\nIt's Mono based, not GNOME based. It has been, like Beagle, splitted up so that you don't have to install a GNOME bit to use it on a KDE desktop."
    author: "Anonymous"
  - subject: "Re: But SUSEwatcher will be replaced by zen-update"
    date: 2006-01-31
    body: "Mono? Ewww..."
    author: "ac"
  - subject: "Re: But SUSEwatcher will be replaced by zen-update"
    date: 2006-01-31
    body: "Mono? Yuck. Its sounds like they're continually trying to find a problem for that thing to solve."
    author: "Segedunum"
  - subject: "Re: But SUSEwatcher will be replaced by zen-updater "
    date: 2006-02-01
    body: "Mono?? That's actually worst than being Gnome-based.."
    author: "ac"
  - subject: "Re: But SUSEwatcher will be replaced by zen-update"
    date: 2006-02-15
    body: "Considering I'm fucking off wankers you might as well have a copy..\n\nAs anybody made any amusing alternatives to the regional and language icon.\nFor people who don't understand SuSE kisses arse so much it is embarrassing\nthe icon for regional and language is an American flag Hahaha laying over\nthe top of the German flag. German passive and the Americans dominant\nGermany takes it up the arse careful boys careful me need K-Y jelly\n\nKDE by default as some amusing icons but this one is taking fucking liberties \nwhy the fuck would I want an American flag on my computer or the German flag.\nI think those silly fuckers at SuSE should fucking grow up and stop sticking flags \neverywhere  nobody else  does it.. Its not appreciated it is fucking stupid. Flags \nhave no place in any desktop computer that is obvious isn't it.\n\nI deleted the flag and changed the icon into an amusing sendup but then the idea that\nthey should even try such a thing annoyed me and I removed SuSE  pathetic stupid \nflags it's a fucking KDE desktop computer you fucking wankers..\n \nSuSE if you keep on sucking you are going to end up with bruised knees and a bad back.\n\n ____________________________\nID signature\nFreedom, if you don't use it you lose it.\nPhilip Davidson,\n10 Ronald Avenue\nWest Ham\nE15 3AH\nEast London\nMobile phone 07906821566"
    author: "Philip Davidson"
  - subject: "Re: But SUSEwatcher will be replaced by zen-update"
    date: 2006-08-13
    body: "Wow, you are one pissed off individual."
    author: "Doug"
  - subject: "i just have to agree"
    date: 2006-01-30
    body: "--- start quote ---\nQ: Richard Stallman or Linus Torvalds?\n\nTom Chance: Stallman, no question. Anyone who can quit a safe job, write a manifesto that brilliantly articulates the vision of a then-dying subculture (hackers), kick off a hack-fest that produces the basis for a totally free operating system, write a serious of interesting and visionary articles on ethics in computing, and then devote 20 years and counting of his life to spreading freedom to all corners of the world has to get my vote.\n\nTorvalds is obviously a great hacker, but I think a lot of geeks overestimate his importance and appeal to the rest of the world. I think that most people - that is, people other than geeks and technology managers - find the freedom stuff far more compelling than talk of coding gods and nifty development methodologies.\n--- end quote ---\n\ni can just agree!\nCouldn't say it better!"
    author: "pinky"
  - subject: "Re: i just have to agree"
    date: 2006-01-30
    body: "completion: Great Homepage, Tom, and great writings + blog-entries!"
    author: "pinky"
  - subject: "Linus"
    date: 2006-01-30
    body: "Although I am not a Linux user (switched so something more UNIX-like long time ago), when talking about Linus vs RMS (why you don't include Perens, Bill Joy, ESR, McKusick, etc?), I can't just watch and not disagree with such overestimation of RMS.\n\nI can agree that RMS was one of the first revolutionaries, but he has grown this into political career and turned himeself into something like the Great Dictator of the FSF empire. Openly discrediting others' work, devotion and role in the open source movement, and not only that but also openly \"brainwashing\" and underestimating the open source movement with his \"free software\" ideas (remember: \"free software\" is only a one point of view view of the open source (do not troll about this: just take a look what Apple, Sun, Apache, Mozilla, X and BSD and many others do)). Who still takes this guy seriously?\n\nLinus on the other hand is (still) a real revolutionary. He is the one of the few guys who has the balls to strongly oppose RMS. Especially I like his position on GPL3, and how pissed RMS feels about Linux vs \"GNU/Linux\". He is not a politican (unlike RMS), but the guy really acts and does something instead of spreading FUD and political manifestos. Hacking himself most of the time he created what some of you are still hacking on. Without Linux, GNU is nothing but a toolkit and politics.\n\nThe real heroes in the open source game are not the ideologists but the contributors!\n\nYou don't need \"the big brother\" to watch after you and tell you what to do or not to do, you don't need a dictator/autocrat, neither you need politics.\n\nWhat you really need is contributions, corp support, market appretiation, openness, standards, collaboration, and less FUD, 'X vs Y' and flamewars."
    author: "Anton Velev"
  - subject: "Re: Linus"
    date: 2006-01-31
    body: "> he has grown this into political career and turned himeself into something\n> like the Great Dictator of the FSF empire\n\nWhat makes you say that?\n\n> Openly discrediting others' work, devotion and role in the open source \n> movement\n\nAny examples? He makes public his disagreements with people but that's hardly discrediting them.\n\n> not only that but also openly \"brainwashing\" and underestimating the open \n> source movement with his \"free software\" ideas (remember: \"free software\" is \n> only a one point of view view of the open source (do not troll about this: \n> just take a look what Apple, Sun, Apache, Mozilla, X and BSD and many others \n> do)).\n\nI didn't realise he brainwashes people :-) To be absolutely correct, Free Software came over a decade before the term 'Open Source' was coined, and the two simply represent very different approaches to software. One is concerned with ethics and the other with development methodologies. It's a bit bizarre to attack Stallman for speaking his mind on an issue he's passionate about, to claim he brainwashes people, and to then suggest that everybody should just talk about Open Source!\n\n> Who still takes this guy seriously?\n\nThe presidents of a whole host of countries who have invited him to speak to them, including India? All the other political figures who consult him? The tens of thousands of people who would describe themselves as \"free software hackers\"?\n\n> The real heroes in the open source game are not the ideologists but the \n> contributors!\n\nNo, the true heroes are those who can contribute to something in any way they can. Stallman is truly amazing because he contributed a massive amount of code, pretty much all of his time and also a political/philosophical will that drove the whole thing forward. If nobody had written the GPL and setup GNU and the FSF then Linus' little kernel would never have taken off in the way it did.\n\nThis is exactly why I chose Stallman over Linus. It's very typical of an engineer to hate \"politics\" when you completely misunderstand how it _enables_ you to do so much more. Linus is a great hacker, undeniably. But Stallman showed leadership in political, philosophical and engineering matters; he kick-started and then led a movement that got us where we are today. Forget that and you might as well write shareware for Windows!\n\nBut for all your worries about FUD and flamewars, this is one of the worst kinds, up there with KDE vs GNOME. Debate the worth of the free vs open source positions, certainly, but what use is there in throwing mud at personalities?"
    author: "Tom Chance"
  - subject: "Re: Linus"
    date: 2006-02-01
    body: "\"but what use is there in throwing mud at personalities?\"\n\nTom, who was behind the decision to kill KDE by starting Gnome?"
    author: "reihal"
  - subject: "Stop trolling!"
    date: 2006-02-02
    body: "What Richard Stallman did was to start the development of a library that cloned Qt (called Harmony). As soon as Qt was released under a free software license (GPL), Stallman was perfectly fine with it.\n\nGnome was started by Miguel Icaza."
    author: "blacksheep"
  - subject: "First Post!"
    date: 2006-01-31
    body: "First Post!\n\n-------\n\npriggio2@uiuc.edu\nEmail: priggio2@uiuc.edu"
    author: "Patrick Riggio"
  - subject: "Re: First Post!"
    date: 2006-01-31
    body: "\nRight. This one too.\n\n\n          Frans\n"
    author: "Frans Englich"
---
We know him as one of the KDE promotion heroes. He has a very long description of himself, he has a t-shirt with an interesting history and he has an explicit view on the Torvalds vs. Stallman question. Tonight the <a href="http://people.kde.nl">People Behind KDE</a> series offers an interview with <a href="http://people.kde.nl/tomchance.html">Tom Chance</a>.


<!--break-->
