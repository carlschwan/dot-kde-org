---
title: "Looking Back on Three Years of OpenUsability with Jan M\u00fchlig"
date:    2006-12-01
authors:
  - "swheeler"
slug:    looking-back-three-years-openusability-jan-mühlig
comments:
  - subject: "Low hanging fruit"
    date: 2006-12-01
    body: "There are some really low hanging fruit that could/should be fixed in the aspect of usability. E.g having the scrollbar in konq go all the way to the rightmost pixel on the screen when maximized, instead of like now where there are 3 border-pixels at the edge (plastik)."
    author: "Marius"
  - subject: "Re: Low hanging fruit"
    date: 2006-12-02
    body: "> E.g having the scrollbar in konq go all the way to the rightmost pixel on the screen when maximized, instead of like now where there are 3 border-pixels at the edge (plastik).\n\nIn this case, I think you can solve this. Somewhere where you configure the window, there is a option that says something like \"Hide the border when maximized\"."
    author: "Hans"
  - subject: "Re: Low hanging fruit"
    date: 2006-12-02
    body: "Are you thinking of \"Allow moving and resizing of maximized windows\"?\nNo that doesnt help, i have already turned that of (if you have it on there is like 12pixels on the edge instead of 3pixels)"
    author: "Marius"
  - subject: "Re: Low hanging fruit"
    date: 2006-12-02
    body: "I don't see what that has to do with usability.\nThose three pixels don't make konuqeror less usable when in full screen mode..."
    author: "AC"
  - subject: "Re: Low hanging fruit"
    date: 2006-12-02
    body: "> I don't see what that has to do with usability.\n> Those three pixels don't make konuqeror less usable\n\nYes they do, they make it more difficult to hit the scroll bar."
    author: "gato"
  - subject: "thank you"
    date: 2006-12-01
    body: "Thanks! That was a great read. I'm looking forward to hearing more about Season of Usability.\n\nvlad"
    author: "Vlad Blanton"
  - subject: "Substantial rethinking"
    date: 2006-12-01
    body: "I can only agree: The most difficult thing about Open Source software that many GUIs are never invented again from the ground up once they are somewhat established. KDE4 and Plasma are a promising exception though.\nOne thing I never ever understood is the extreme un-userfriendliness of IRC applications. Lots of people I know chat in communities on the internet. But nearly everyone on webpages offering the chat function via their proprietary page. This has several disadvantages:\n1) If you want to join many distinct communities you have to relearn every interface\n2) Those interfaces often are suboptimal. Some are sluggish, some dont work cross-platform and so on. \n3) You are dependent on one provider. More often than not they want money for their services or require you to give away lots of personal information. Some are plastered with pop-ups and ads.\n4) Often you cannot easily exchange files and so on.\n5) Many people may not care but you depend on a centralised commercal provider which opens the door to lots of privacy and other issues, so I'm mentioning this here as a final point\n\nThe solution could be so easy: IRC clients do exist for years. But they never gained traction on the mainstream. Why? Because they are a gazillion times more difficult to use than those web pages. But all IRC clients look more or less the same. Do you know KVirc or mIRC? Those clients are absolutely impossible to use for many of my family members. If you run them they ask for a server to connect to and offer a list with thousands of servers. You just cannot know which are recommended and contain many users and which not. There is no assistant whatsoever. Once you are connected there is no help in finding a channel. The channel listing function doesnt work properly with many IRC servers. Or Konversation: When you run it you are connect to some obscure Kubuntu server and that's it. There is no channel list. No graphics. Nothing. It looks like Konsole. IRC clients really belong to the user interface hall of shame!\n"
    author: "Michael"
  - subject: "Re: Substantial rethinking"
    date: 2006-12-01
    body: "> I can only agree: The most difficult thing about Open Source software that many GUIs are never invented again from the ground up once they are somewhat established.\n\nFrom http://www.sparkynet.com/spag/backissues/SPAG44\n\n  SPAG: Many Internet programs, especially those intended to be used by a   \n  community, are written in public, with open source and frequent published \n  drafts. Why is Inform 7 only now making its first public appearance?\n\nGN: Partly my nature, no doubt. TADS 3 has had a much more open process - and \nperhaps a more confident one. I will certainly plead guilty to being a control \nfreak. The hard decisions are what to take out, not what to put in, and that \nbecomes more difficult when there is a nascent user base. (Also, you need to be \ncareful which wish-lists to read: the wish-list you need to pay most attention \nto is the one written by a designer of actual IF, not a well-motivated \nbystander.)\n\nHad Inform 7 been developed in open source, I am fairly sure it would now be an \nelaborated version of the superficial prototype, and that it would be much the \npoorer. And it ought to be remembered that for at least the first year of the \nproject, I wasn't at all sure it would ever work - \"work\" in the sense of being \ncapable enough to be useful.\n"
    author: "Krishna Sethuraman"
  - subject: "Re: Substantial rethinking"
    date: 2006-12-02
    body: ">>>Why is Inform 7 only now making its first public appearance?\n\nSpeakinf of Inform 7, they are looking voor kde-developers to create a Linux gui :)\n\nhttp://www.inform-fiction.org/I7/Download.html"
    author: "AC"
  - subject: "Re: Substantial rethinking"
    date: 2006-12-02
    body: "lol, if your family members cant use stuff like irc clients, im surprised that you are able to read/write, i guess not all things are genetic.. :)"
    author: "redeeman"
  - subject: "Name Typo"
    date: 2006-12-02
    body: "Brigitte Zypris in the milestones box should be Brigitte Zypries."
    author: "Anonymous"
  - subject: "Re: Name Typo"
    date: 2006-12-02
    body: "Fixed, thanks!\n\nDanny"
    author: "Danny Allen"
  - subject: "Usability?"
    date: 2006-12-02
    body: "Cmon, KDE and Usability in the same sentence? must be a joke."
    author: "cgi-bin"
  - subject: "Re: Usability?"
    date: 2006-12-02
    body: "Wow, somebody is jealous :o)"
    author: "AC"
  - subject: "Re: Usability?"
    date: 2006-12-03
    body: "Funny how Birmingham rejected GNOME for KDE because of usability issues."
    author: "KDE User"
  - subject: "Re: Usability?"
    date: 2006-12-04
    body: "You guys don't have to try to bite a troll whenever he comes, otherwise you encourage him to try and bite back."
    author: "strider"
  - subject: "Hi !!!"
    date: 2008-08-03
    body: "Hello, my name is Carla Muhlig and how you see we have the same surname, really I want to contact you in the order to know more about you, I know that you live in Berlin and you was born in 1971, I was born in 1978, my grand grand father arrive to my country around 1857 but I`m not sure so I`m in an investigation about that for find more details about him.\nI will appreciate if you answer me, thanks in advance, Carla"
    author: "Carla Muhlig"
---
Just following the recent <a href="http://www.worldusabilityday.org/">World Usability Day</a> and a few months past the third birthday of <a href="http://www.openusability.org/">OpenUsability</a> I took some time to talk to Jan M&uuml;hlig, one of the OpenUsability founders and to get an inside look at some of the history of the project, how it works from the inside and some of the current direction.



<!--break-->
   <table bgcolor="white" align="right" cellspacing="10">
      <tr>
        <td>
          <table bgcolor="lightgrey" cellspacing="1">
            <tr>
              <td>
                <table bgcolor="white" border="0" width="250" cellspacing="10">
                  <tr>
                    <td align="center"><img border="1" src="http://developer.kde.org/~wheeler/images/jan-muehlig.jpg"></td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td>
          <table bgcolor="lightgrey" cellspacing="1">
            <tr>
              <td>
                <table bgcolor="white" border="0" width="250" cellspacing="10">
                  <tr>
                    <th>OpenUsability Milestones</th>
                  </tr>
                  <tr>
                    <td>
                      <ul>
                        <li><p><b>July 2003</b></p><p>Relevantive KDE / Linux usability study published</p></li>
                        <li><p><b>August 2003</b></p><p>OpenUsability founders at KDE World Conference</p></li>
                        <li><p><b>October 2003</b></p><p>OpenUsability project started</p></li>
                        <li><p><b>May 2004</b></p><p>Slashdot and similar news sites first pick up on OpenUsability</p></li>
                        <li><p><b>June 2005</b></p><p>First OpenUsability booth at LinuxTag, visit by German Minister of Justice Brigitte Zypries</p></li>
                        <li><p><b>November 2005</b></p><p>OpenUsability e.V. founded</p></li>
                        <li><p><b>December 2006</b></p><p>OpenUsability begins cooperation with the Open Society Institute to sponsor and mentor student usability projects</p></li>
                      </ul>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>


    <p><b>Hi Jan &mdash; first I'd like to get a little background &mdash; could you tell us where you're from, who you work for, where you live?</b></p>

    <p>I'm Jan M&uuml;hlig.  I was born 1971 in Reutlingen, in southern Germany.  I studied sociology, ethnology and philosophy in Regensburg (Germany), Mainz (Germany), Chicago (US) and Lausanne (Switzerland).  I've been living in Berlin since 2000 and am currently the CEO of the usability consulting firm <a href="http://www.relevantive.com/">relevantive</a>, which I founded.</p>

    <p><b>How did you get into usability &mdash; was that something that came while you were studying?</b></p>

    <p>Not really.  I did user research during my studies (media sociology), mostly dealing with TV and I did ethnology, which has a lot to do with understanding others' behavior.  After my degree (MA) I started working in an advertising agency where I projected TV audiences for commercials.  Then, in 1999, I took a position as a marketing manager at a big multimedia agency.  The problem there was that we made a lot of interactive things, but many of them were crap because we never took the users' perspective but just assumed that we knew what was best.  A year later, after I quit that position (in the summer of 2000), I decided to do exactly what was missing in my previous work.</p>

    <p>With the background I had from my studies it was not very difficult.  I met a school friend who worked as an IT consultant and we decided to found the company.  We still had a lot to learn.  In the end, most of my usability knowledge I got from books and practical experience.</p>

    <h3>Open Source Meets Usability</h3>

    <p><b>So, that's the beginning of Relevantive.  How did you move from there to getting into the Open Source world?</b></p>

    <p>I played around a little with Mandrake around 2001, but I had no special interest in OSS.  Then, in 2003, I had some arguments with Jutta Horstmann, who claimed that Windows is not more usable than Linux.  In order to settle that, we started a <a href="http://web.archive.org/web/20060208135328/http://www.linux-usability.de/download/linux_usability_report_en.pdf">large usability study</a> during which we &mdash; especially Jutta &mdash; got in contact with Eva Brucherseifer who helped us to set up the testing machines with Linux / KDE.  We published the study under a free license and when we presented first results at <a href="http://www.linuxtag.org/">LinuxTag</a> in 2003 we got a lot of different responses.  On the one hand we said Linux on the desktop can be as usable as Windows, but you very much have to configure it and we mentioned a lot of weaknesses.  We were invited to present our study at the KDE world summit in <a href="http://dot.kde.org/1061562947/">Nove Hrady</a>.</p>

    <p>The reaction there and the interest in usability was very mixed.  Some, like Daniel Molkentin, Cornelius Schumacher, Eva Brucherseifer and a few others were very interested and wanted to know how to improve usability in KDE.  At that time I understood very little about how thinks were done in Open Source, and I saw that the infrastructure OSS projects worked with weren't very suitable to usability work (mailing lists, bug tracking, CVS).</p>

    <p>I also had the impression that it would be very difficult to get usability folks to join OSS projects.  That's why we said we needed a platform that actually interfaces usability and OSS development.  So jutta started to set up GForge.  I knew from reading usability mailing lists (KDE) and from experiences during Akademy that it didn't make sense to approach a developers and &ldquo;convince&rdquo; them of the benefits of usability; I came to the conclusion that usability can only work if the developers are ready to go for it, which is incidentally the same in the commercial world.</p>

    <p>So we had three challenges:</p>
    <ul>
      <li>Get developers interested in usability</li>
      <li>Get usability people inside the projects</li>
      <li>Get workflows, interfaces and resources between the two groups</li>
    </ul>
    <p>The interesting thing is that OSS developers identify much more with the software they write, so many actually have a strong interest in their software being usable because they like  happy users.  But I don't actually believe that a majority of developers can become really usability savvy &mdash; it takes time to learn it and actually you need some distance.</li>
    </ul>

      <h3>A Project is Born</h3>

      <p><b>So now we've got some of the background &mdash; the platform, the motivation, some of the process &mdash; how and when did that all come together to form OpenUsability?</b></p>

      <p>After Nove Hrady (Akademy 2003), the platform we developed was financed by Relevantive and all efforts came from there.  But we didn't want people asking, &ldquo;What is the <i>real</i> reason they're doing this?&rdquo;  We wanted something with more credibility.  So even though the OpenUsability project existed, we thought we'd be more successful by formally being separate from Relevantive and on November 3 of last year, we founded OpenUsability e.V.  Since it was not just a project, but a real organization we had more options &mdash; the ability to take donations, add new members, etc.</p>

      <p><b>Who's a part of OpenUsability &mdash; both in the early days as a project, the founding, now?</b></p>

      <p><a href="http://weltraumsofa.de/">Jutta</a> and I were the first, then when <a href="http://www.kdedevelopers.org/blog/931">Ellen</a> joined Relevantive, she did a lot both in her worktime as well as in her free time.  I organized a periodic usability round table in Berlin and through that I had the chance to convince more usability specialists to do things for OSS like <a href="http://www.lazs.de/">Bj&ouml;rn Balazs</a>, or <a href="http://www.mmiworks.net/eng/publications/blog.html">Peter Sikking</a>.  Ellen had contacts to the <a href="http://www.ccc.de/">CCC</a> people, like <a href="http://svenfoo.geekheim.de/">Sven Neumann</a>, for instance.  Since they were interested in Open Source, <a href="http://tina-t.blogspot.com/">Tina Trillitzsch</a> and <a href="http://holehan.blogspot.com/">Florian Gr&auml;sle</a> also did some work for Relevantive.</p>

      <p><b>So are all of those guys from usability backgrounds?</b></p>

      <p>Except for Sven, the <a href="http://www.gimp.org/">GIMP</a> maintainer, all of the others had some kind of professional or academic usability background.</p>

      <h3>Working with OSS Projects</h3>

      <p><b>What are some of the other projects you have worked with at OpenUsability?</b></p>

      <p>KDE PIM, though they're short on developers now.  Bj&ouml;rn and Florian worked on <a href="http://kuroo.org/">Kuroo</a>, the Gentoo package manager.  Bj&ouml;rn also worked with TV-Browser; Ellen worked with Guidance (Kubuntu).  Celeste and Ellen are very much involved with KDE Human Interface Guidelines.  Katrin is helping out with Gallery, a web image gallery presenter.</p>

      <p><b>How did those projects get in contact with OpenUsability?  Did the developers approach OpenUsability?</b></p>

      <p>Yes, that's one of the main principles of OpenUsability: that developers approach OpenUsability, mainly by registering their project on openusability.org.  We also meet a lot of people at conferences and try to get folks that are interested in usability to sign up too.  That's what recently happened after a EuroOSCON presentation with a radio server project <a href="http://campware.org/">Campware.org</a>.</p>

      <p>What is also important is that we prefer a &ldquo;small&rdquo; or &ldquo;local&rdquo; approach; usability does not come necessarily from top down, but through developers or projects working directly with usability specialists.  That's why a project like OpenOffice somehow don't fit our approach as most decisions there are done top-down.</p>

      <p><b>And what are the first steps?</b></p>

      <p>Ideally we'd start with a thorough analysis of the goals and try to work out the best solution, but in practice, many developers come with a question and a certain feeling that things could be made better.  Unfortunately, a lot of this is in the form of &ldquo;What's the right way to do...&rdquo; and on deeper inspection it points to a deeper problem rather than a quick fix.</p>

      <p>In other cases, developers take themselves as the primary (or ideal) user. If the usability specialist from OpenUsability sees this and has no hard-data to base a recommendation on, a usability test (i.e. confronting &ldquo;real&rdquo; users with the piece of software in a structured manner) often helps, especially if no one has figured out what the user actually needs, or how they use the program.</p>
      <p>Unfortunately usability tests are rather expensive; you need to find people, usually pay them for coming, analyse the results, and so on.</p>

      <p><b>So, that usually doesn't happen for OSS projects?  Or only in special cases?</b></p>

      <p>Just like Open Source developers, our usability specialists also contribute in their spare time or &ldquo;for fun&rdquo;, so we try to maximize their efficiency.  Analytical methods often make more sense and in many cases they're a prerequisite to doing tests anyway.  A lot of real user testing would ideally be covered by universities, for instance as student projects, since they often have resources for such.</p>

      <p><b>What other challenges do you tend to hit along the way?</b></p>

      <p>In the case of GIMP, for instance, one main challenge was to identify what GIMP wants to be for the user, that is: what is the vision behind the software?  That's difficult in OSS because a lot of software was started because of a developer &ldquo;scratching an itch&rdquo;.  Evolution of OSS projects mostly works through additions (of features, etc.), but rarely are projects changed at the core after it's reached a certain size or level of recognition.</p>

      <p><b>So, roughly speaking &mdash; how does that compare with non-community projects that you've seen.  Is &ldquo;open&rdquo; usability a totally different world or just a set of variations on the usual theme?</b></p>

      <p>It's a completely different world.  In most commercial projects you're there (as the usability specialist) because marketing or product management wants you there.  Commercial developers often hate you because they think you make trouble and slow things down.  Also, in commercial projects (like, let's say a corporate intranet), there are a lot of politics, and usability only can change 5%.  In Open Source, you are working directly with the developer and he is the one who decides.  There is no management above, at least not in the community projects.</p>

      <p>So, if as a usability specialist you are trusted by the developer you might have a big influence on the outcome &mdash; which is something extremely gratifying but also a lot of responsibility.</p>

      <p>Also, in OSS you can see the effects almost immediately because of the short release cycles, whereas in commercial projects the feedback loop is very long.  This is pretty close to an ideal usability engineering process where you have lots of feedback loops between software and user and thus iteratively get better in small steps.  That's just wishful thinking in commercial software development.</p>

      <p><b>What about the methods?</b></p>

      <p>The methods do not differ very much, but the communication channels do because you rarely work locally together but rather instead communicate by electronic means &mdash; IRC, mailing lists (usually poorly), wikis and other collaboration platforms.  However, if you happen to sit together with the developers you're working with you may be able to do a lot work in short amount of time, like at Akademy, LinuxTag or similar events.</p>

      <p>Trust also has a lot to do with it.  Usability input is not comparable to code.  It is not as much objectively reproducible or verifiable.  A project maintainer can't evaluate usability input in the same way as he can evaluate code.  Instead, to some extend they must trust the competency of the usability specialist.  There, meeting each other face-to-face and really talking can change a lot.</p>

      <p><b>I think that's true in a lot of OSS.  It's easier to work with people with faces.</b></p>

      <p>That's why I would love to see more usability specialists present &mdash; let's say at Akademy.  You often have to meet the people before you are not ignored in mailing lists anymore.</p>

      <h3>The Future</h3>

      <p><b>Any other big things on the horizon?</b></p>

      <p>The Season of Usability, which Ellen initiated, which thusfar has been very successful is a clear sign that we are on the right track.  The other important thing coming up will be to launching the new OpenUsability platform which should very much ease collaboration.  From there we want to spread the word worldwide and get usability people in every country to support Open Source projects.  We also want to get universities to support Open Source projects in an educational setting and on the other side to get companies to share their usability knowledge and to open up the process &mdash; to make usability open.</p>




