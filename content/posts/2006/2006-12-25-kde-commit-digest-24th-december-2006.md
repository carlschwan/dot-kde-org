---
title: "KDE Commit-Digest for 24th December 2006"
date:    2006-12-25
authors:
  - "dallen"
slug:    kde-commit-digest-24th-december-2006
comments:
  - subject: "Oxygen-themed widget style"
    date: 2006-12-24
    body: "\"Work begins on the Oxygen-themed widget style and window decoration for KDE 4\"\nthis is the most interesting info for me in this week's commit-digest :-)\n\nI just hope it has some innovative ideas , and good implementation.\nas you know , icon theme is not enough to give a fresh look for KDE4."
    author: "Mohasr"
  - subject: "Re: Oxygen-themed widget style"
    date: 2006-12-24
    body: "Please, please, please... if there's a new window theme, can we have the buttons reaching the borders of the windows, at least in maximised mode?  That way, we can quickly close a window or restore it to normal size by \"throwing\" the mouse to the screen edge, and clicking, rather than having to slowly aim for the widget box."
    author: "Lee"
  - subject: "Re: Oxygen-themed widget style"
    date: 2006-12-24
    body: "If you go to Desktop -> Window Behavior in the KDE Control Center, you can change it so that maximized windows no longer have a frame, just the title bar and the window content."
    author: "Me"
  - subject: "Re: Oxygen-themed widget style"
    date: 2006-12-25
    body: "It works for the close button, but not for the scrollbar, which still has a frame right of it."
    author: "Anonymous"
  - subject: "Re: Oxygen-themed widget style"
    date: 2006-12-25
    body: "you don't have a scroll wheel mouse??"
    author: "otherAC"
  - subject: "Re: Oxygen-themed widget style"
    date: 2006-12-26
    body: "It's already written in another thread; even if you have a scroll wheel mouse, the scrollbar is much faster for quick navigation."
    author: "Anonymous"
  - subject: "Re: Oxygen-themed widget style"
    date: 2006-12-26
    body: ".. and this is the default behaviour.\n\nif the window deco you are using doesn't support this feature, file a bug with the maintainer of that deco."
    author: "Aaron J. Seigo"
  - subject: "Re: Oxygen-themed widget style"
    date: 2006-12-25
    body: "That feature is the #1 reason why I only use the Plastik window theme, I know of none other that does it (plus Plastik is just cute). I only wish screen borders were given more importance when some widget will potentially be stuck to it. For instance, it always bothered me not being able to grab the scroll bar in maximized Konqueror by throwing the mouse against the right border of the screen, there's one or two pixels in between making me miss it each time and having to go back.\n\nThanks Danny!"
    author: "Pilaf"
  - subject: "Re: Oxygen-themed widget style"
    date: 2006-12-25
    body: "You're absolutely right Pilaf! It seems most KDE apps, when maximised, don't have any widgets touching the screen border (always 1 or 2 pixels off). Take an app's scrollbar, for example. They really should make use of Fitts' law, which states that clickable regions touching the border of the screen can be considered infinitely wide and hence easy to click on quickly. GNOME uses this rule (mostly), and at least Kicker does too.\n\nThe same goes for the windeco. I use Plastik as well for that reason (plus it stylishly matches the rest of my QtCurve theme). Normal-looking windecos really should have their titlebar buttons touching the edge of the screen when maximised.\n\nThanks,\nSchalken"
    author: "Schalken"
  - subject: "Re: Oxygen-themed widget style"
    date: 2006-12-25
    body: "Though I never personally maximize my windows and thus find no need for it, I'll be sure to handle this (in maximized mode only).\n\nShould I forget then please remind me again when the alpha or beta vesions starts to come out. Right now there is so much else to do."
    author: "Casper Boemann"
  - subject: "Re: Oxygen-themed widget style"
    date: 2006-12-25
    body: "Rather diverse pool of usage habbits... I highly value an \"easily grabbable\" window edge, because I drag-n-drop a lot of things around - have to resize maximized windows often. \n\nAs a result not having access to the window frame next to the scroll bar in maximized Konquerror would be a rather horrid situation.\n\nLuckly, there is a switch in KDE 3.x WM control panel to \"allow resizing of maximized windows\", which draws the window frame always. Hope the functionality stays.\n\nI am rather puzzled by the need to grab the scroll bar though... what's wrong with your scroll wheel?"
    author: "Daniel \"Suslik\" D."
  - subject: "Re: Oxygen-themed widget style"
    date: 2006-12-25
    body: "There's nothing wrong with the scroll wheel, but it just isn't as fast when I need to jump quickly to the other end of the page I'm viewing."
    author: "Pilaf"
  - subject: "Re: Oxygen-themed widget style"
    date: 2006-12-25
    body: "Most laptops don't have a scroll wheel ;)\n\nDanny"
    author: "Danny Allen"
  - subject: "Re: Oxygen-themed widget style"
    date: 2006-12-25
    body: "On a laptop you can use the right hand side of the touchpad as a scroll wheel.  Not sure what that's called"
    author: "John Tapsell"
  - subject: "Re: Oxygen-themed widget style"
    date: 2006-12-27
    body: "For those people in love with the mouse nipple right in the middle of the keyboard (as seen in Thinkpads) the touchpad is too far away. For us it is easier to rush the mouse cursor to the right than to reposition the right hand to try to scroll."
    author: "Slubs"
  - subject: "Re: Oxygen-themed widget style"
    date: 2006-12-28
    body: "I think anyone who's handy with the mouse/touchpad/etc has no problem locating the scroll bar with it.\n\nBut if you do, you can always set the acceleration of the mouse and the treshold in kcontrol to get a better grip on your mouse pointer"
    author: "otherAC"
  - subject: "Re: Oxygen-themed widget style"
    date: 2006-12-25
    body: "Do you \"quickly close\" a window with your mouse?! I suggest \"ctrl+q\" or \"alt+f4\" for that job! And I suggest you define a shortcut to maximize/minimize your window (e.g. \"alt+f1\"). Life will be less painful!"
    author: "Hobbes"
  - subject: "Re: Oxygen-themed widget style"
    date: 2006-12-25
    body: "Only if you mainly use the keyboard.\n\nMost people use the mouse for maximising, minimising and closing windows.\n\nThe plastik windeco seems to get everything right, except for the scrollbar.\n\nAnd to the person above: many laptops don't have a scroll thing on the touchpad, and what if you are using a plugged in mouse?\n\nBesides, the existence of alternatives is no reason not to make the default method better. The extra 2 pixels to the right of the scrollbar would be missed by no-one if they were removed."
    author: "Tim"
  - subject: "Oxygen widget style"
    date: 2006-12-24
    body: "still wonder about the 'work begins on oxygen widget style and window decoration'... i've exchanged mails with Thomas L\u00fcbking, who was at that time working on the oxygen style (we know Thomas from his Baghira style [1], btw). now the work begins?!? is he not involved anymore?\n\n\n\n[1] http://baghira.sourceforge.net/"
    author: "superstoned"
  - subject: "Re: Oxygen widget style"
    date: 2006-12-25
    body: "No he is not. He wasn't responsive to the artist and didn't show up on irc, so in the end we couldn't wait anymore."
    author: "Casper Boemann"
  - subject: "What a great Christmas present !"
    date: 2006-12-24
    body: "Thanks again Danny, thanks for doing this !"
    author: "Marc Cramdal"
  - subject: "Re: What a great Christmas present !"
    date: 2006-12-25
    body: "Word for word what I was going to write.  Thanks Danny!"
    author: "Wade Olson"
  - subject: "Re: What a great Christmas present !"
    date: 2006-12-25
    body: "I second that.  Thanks for keeping us up to date Danny :)\n\nHave a very Merry Christmas."
    author: "Robert Knight"
  - subject: "Re: What a great Christmas present !"
    date: 2006-12-25
    body: "Can you post a screenshot please, so I can be happy as you? I have no deep knowledge and a slow (500Mhz) PC to compile KDE4-svn.\n\nThanks a lot and merry x-mas!\n"
    author: "anonymous"
  - subject: "Re: What a great Christmas present !"
    date: 2006-12-25
    body: "screenshot of what? the theme isn't there (does compile but contains no code), the icons can be seen from the digest, and aside from some games there aren't many changes to be seen. i'd wait for the first alpha or beta."
    author: "superstoned"
  - subject: "Re: What a great Christmas present !"
    date: 2006-12-26
    body: "... does compile but contains no code ...\n\nSo... what is compiling then?? =)\n\nMerry Christmas!\n\n//Oskar"
    author: "Oskar"
  - subject: "Re: What a great Christmas present !"
    date: 2006-12-26
    body: "> the icons can be seen from the digest\n\nSorry, I can't see any beautiful icons?"
    author: "anonymous"
  - subject: "Re: What a great Christmas present !"
    date: 2006-12-27
    body: "So you find them ugly?\n\n:o)"
    author: "otherAC"
  - subject: "yauap?"
    date: 2006-12-24
    body: "Not that I mind, but it seams to be a new tradition on the Dot (see rant about kaffeine in last comments thread on the commit digest), so I ask the following question:\n\nAmarok gets support for gstreamer through yauap?\nWhy not spend that time in porting Amarok to Phonon?\n"
    author: "otherAC"
  - subject: "Re: yauap?"
    date: 2006-12-24
    body: "Because Amarok is still a KDE 3 application, and Phonon only works in KDE 4."
    author: "Narishma"
  - subject: "Re: yauap?"
    date: 2006-12-26
    body: "yep, i know, and i think it's a good idea that amarok continues to develop in the 3.x series while kde4 is not yet on the horizon.\n\nBut i'm still suprised that kaffeine and other applications got the blame for doing the same thing...."
    author: "otherAC"
  - subject: "KDevelop"
    date: 2006-12-25
    body: "Is that new python parser supposed to bring real python code completion? Can I finally drop SPE?!"
    author: "CptnObvious999"
  - subject: "Re: KDevelop"
    date: 2006-12-25
    body: "This is one of the goals, although the python parser is just in its beginnings and we don't know yet how far we'll come with it. The problem with parsing Python from C++ is that it's a highly dynamic language, and the information we'll get from the parser will most probably not be enough to get satisfactory code completion results.\n\nSo regard this as a first attempt - we'll work it out eventually, but you may have to wait a good while for it."
    author: "Jakob Petsovits"
  - subject: "Re: KDevelop"
    date: 2006-12-25
    body: "The C++ code completion is still years behind MSVC 6 (which is now 8 years old).\n\nThat is, it doesn't work. I downloaded KDevelop 3.4RC2 and typed this:\n\nstruct foo\n{\nint a;\nint b;\nint c;\n};\n\nint main()\n{\nfoo s;\ns.\n\n\nExpecting it to give me a nice list of a, b, and c, but alas. Nothing. However after typing the s it does give me a list of every function containing an s! Eg strdup(s). And also 'foo s (Local Variable)'. Err great... thanks for telling me what I just wrote! How.. err.. useful!\n\nI'm sure it's extremely hard to do (you more or less have to compile the code) but still, 8 years!\n\nAnd the 'Security Checker' does absolutely nothing as far as I can tell."
    author: "Tim"
  - subject: "Re: KDevelop"
    date: 2006-12-25
    body: "Of course we can do code completion like this one. It would be ridiculous to support smart pointer completions (like we do in 3.4) and to not support simple struct completion. Any time you see things like this, please report us that as a bug!\n\nNot having enough information, I still have tried to reproduce the bug and found one case when the code completion doesn't work. This happens for automatically opened file immediatelly after project creation. Once you reopen file (or project) - completion will work. I've fixed now this bug in svn so please update and check.\n\nSo instead of remembering msvc, do tell us (bugreport, irc, ml) what doesn't work for you ;)\n"
    author: "Alexander Dymo"
  - subject: "Re: KDevelop"
    date: 2006-12-26
    body: "Ah yes so it does. That's good. I didn't think it was that clever because it has never worked before.\n\nI added my Qt4 path, and /usr/include/c++/4.1 to the search database, which makes it pretty slow.\n\nAlso things like this don't seem to work:\n\n//header\n\nclass W : QWidget\n{\nQ_OBJECT\npublic:\nW();\nQComboBox* b;\n};\n\n//implementation\n\nW::W()\n{\nb->\n\nHard to report bugs when you don't know what it should be capable of!"
    author: "Tim"
  - subject: "Re: KDevelop"
    date: 2006-12-26
    body: "Oh actually it does work.\n\nI had imported the Qt4 headers using 'custom directory' instead of the Qt4 importer.\n\nSurely it should work for both? If not maybe you should auto-detect qt4 and qt3 (shouldn't be that hard)."
    author: "Tim"
  - subject: "Re: KDevelop"
    date: 2006-12-27
    body: "Custom directory importer is not smart enough to understand qt4-style includes, so we have dedicated importer for that. As fo autodetection, not everybody use Qt/KDE so we left completion databases creation for users. I'm not sure whether that was a good solution though."
    author: "Alexander Dymo"
  - subject: "Re: KDevelop"
    date: 2006-12-28
    body: "imho, it would be great to ship databases for the frameworks we'd like people to be using so things Just Work as much as possible. note that other IDEs have installation footprints that measure in the 100s or even 1000s of MB because they do this, so the added footprint wouldn't be a surprise (kdevelop's footprint is downright tiny =), and for packagers that split out the different template packs by language/env this added footprint won't even be an issue. "
    author: "Aaron Seigo"
  - subject: "Re: KDevelop"
    date: 2006-12-26
    body: "as i worked with ANTLR 3.0 , there you can easyly parse pyhton , even broken pyhton --..\n\nwhitespace is sent to the parser on a different channel so you can get this \"tab\" and \"notab\" events in your grammar..."
    author: "wingsofdeath"
  - subject: "mailody"
    date: 2006-12-25
    body: "as i know, that you cant say what people should do for helping kde, - i dislike the mailody and dolfin efforts :/\n\nsry. just my pof. as i have seen many oss projects, it looks like there is one person with muchas time. Thats why the project goes this fast right now. When this person has other things in life, (Girlfriend, Job, ..) the project will slow down or is gone :(..\n\nwhen someone picks it up again, it will first be refactored, and so on ..\n\ni just build kde4 today, and there is so much work to do everywhere. You cant even work with it..\n\nI appreciate all the work which is going in kde4 and want to thank you, keep going with mailody and so on, that are just my thoughts - and the purpose is to have fun coding :>\n!!!\n\n\n\nsry, for my poor english"
    author: "wingsofdeath"
  - subject: "Re: mailody"
    date: 2006-12-25
    body: "It's mostly a question of purpose.\n\nMailody sorta does look like a skunk-works-of-the-day project with no particular grandious purpose but to write a mailer from scratch. Since this apps doesn't try to solve any particular problem, luckly not many people are distructed. When main dev gets tired of it, everyone can rummage the code for usefull pieces.\n\nDolfin, on the other hand, does try to solve a real problem - make resource/file management easier. Some \"new\" (coppied from other platform to the point of using the same separator chars) features are tested in Dolfin. May end up as a good alternative to Konqueror fm, if features are not just parroted over blindly."
    author: "Daniel \"Suslik\" D."
  - subject: "Re: mailody"
    date: 2006-12-25
    body: ">>i dislike the mailody and dolfin efforts :/\n\nI wonder: do you dislike their efforts, or the fact that they happen in KDE's SVN?\n\nIf you checkout kde-apps.org, you will find many kde applications that are developing fast and can be considered as alternatives for main kde applications.\n\nNo-one complains about them, simply because they don't notice their development since it happens outside SVN and outside the scope of the commit digest.\n\nand besides that, i don't think that kde4 will progress faster just by throwing a lot of developers on the project.\n\n\n"
    author: "otherAC"
  - subject: "Re: mailody"
    date: 2006-12-25
    body: "I agree that often it's not good to do duplicated work, but if you look at for instance dolphin. It really has a lot of improvemends over konqueror. The developper is really trying lots of new stuff which I think sometimes is only possible when you start from scratch."
    author: "AC"
  - subject: "Re: mailody - dolphin"
    date: 2006-12-31
    body: "what is the difference between dolphin way and using Krusader ?\n\nKrusader already does things differently than konqueror, that's why I wonder what Dolphin is trying to accomplish...."
    author: "boemer"
  - subject: "Re: mailody"
    date: 2006-12-25
    body: "Speaking with my former-Dolphin-dev hat on, I see Dolphin only as a way to showcase how well a radical departure from previous status quo concepts could work. I therefore hope a freshened incarnation of Konqueror remains the KDE file manager.\n\nMerry christmas, btw =)"
    author: "logixoul"
  - subject: "dolphin"
    date: 2006-12-25
    body: "dolphin does look like \"Thunar\" the filemanager for xfce desktop! clean and fast..\n\nwhereas konqi filemanager seems cluttered, though kubuntu has made it to look good :)"
    author: "fast_rizwaan"
  - subject: "Re: mailody"
    date: 2006-12-28
    body: "so Dolphin is not meant for everyday usage?"
    author: "otherAC"
  - subject: "Re: mailody"
    date: 2007-01-03
    body: "Correct, in the long term I don't consider Dolphin's purpose to be usage.\nThe maintainer, however, does. ;)"
    author: "logixoul"
  - subject: "Re: mailody"
    date: 2006-12-25
    body: "About Dolphin...I hope that someday in 2007 we'll see the (flaming) discussion about replacing Konqueror as filemanager with Dolphin by default.\nMaybe I'm a dreamer bu Dolphin is IMO the way to go. Light, quick, it does ONE thing and it does it well. And with KIO support there's nothing I'm missing from Konqui."
    author: "Vide"
  - subject: "Re: mailody"
    date: 2006-12-25
    body: "Yeah, I look forward to that (flaming) discussion too. As long as we focus on what Dolphin can solve and how Konqueror could solve long outstanding issues for itself, we can keep some rational going in that discussion. Because IMO Konqueror has to prove, very clearly, that it can and will solve some longstanding issues in the 4th KDE round. Dolphin is here to help but Konqueror can always clearly demonstrate when it believes it needs none. Either way, the (added) benefits should be made obvious for either choice."
    author: "ac"
  - subject: "Re: mailody"
    date: 2006-12-25
    body: "I think a dolphin view mode for konqueror would be a better idea.\nI think KDE would shoot itself in the foot if they move away from an advanced file manager like konqueror by replacing it with a simple manager like dolphin."
    author: "otherAC"
  - subject: "Merry Christmas"
    date: 2006-12-25
    body: "to everyone @ KDE!\n\nAnd a happy New Year as well - may it become yet another great year for KDE!"
    author: "Joergen Ramskov"
  - subject: "I think that kde4 may be happening"
    date: 2006-12-26
    body: "<I>Siraj Razick committed changes in /trunk/playground/base/kbfx_plasma</I>\n\nI was so happy when I saw that\nGreat work! i hope that kbfx actually gets a decent implementation of the plasmiod idea before the end of 2007! Then I guess it'll take over from kicker as the default bottom bar....\n\nHappy holidays!\nBen"
    author: "BenAtPlay"
---
In <a href="http://commit-digest.org/issues/2006-12-24/">this week's KDE Commit-Digest</a>: A new game, KSquares, is imported into KDE SVN, with KLines starting on the (now familiar) path towards scalable graphics and general improvement. Usability and other improvements in <a href="http://okular.org/">Okular</a>. Support for multiple "identies", alongside a festive basket of other enhancements in <a href="http://www.mailody.net/">Mailody</a>. Search support and plugin handling improvements in <a href="http://kget.sourceforge.net/">KGet</a>. In <a href="http://amarok.kde.org/">Amarok</a>, the "yauap" engine (a redeveloped <a href="http://gstreamer.freedesktop.org/">GStreamer</a> interface, using D-Bus interaction) progresses, with support for audio CD's. Improved OpenFormula specification compliance in <a href="http://www.koffice.org/kspread/">KSpread</a>. A much-enhanced implementation of "run-around text" comes to <a href="http://koffice.kde.org/kword/">KWord</a>. A work-in-progress python parser for <a href="http://www.kdevelop.org/">KDevelop</a> is imported into KDE SVN. Work begins on the <a href="http://oxygen-icons.org/">Oxygen</a>-themed widget style and window decoration for KDE 4.
<!--break-->
