---
title: "Security: KJS and KPDF Security Advisories"
date:    2006-01-20
authors:
  - "jriddell"
slug:    security-kjs-and-kpdf-security-advisories
comments:
  - subject: "Auto_Patch_Install"
    date: 2006-01-20
    body: "what about a k_patch_gethotnewstuff, gets all the files it needs , compiles, and replaces only the files it has to. \n\nBecause it does not create new files, it will not conflict with the package management of the various distributions.\n\njust an idea, i have not the time to implement it."
    author: "chris"
  - subject: "Re: Auto_Patch_Install"
    date: 2006-01-20
    body: "\"k_patch_gethotnewstuff\" would need to be run as root, and would change the checksums of the apps installed by the distro tools, which might cause them to freak out.  Using the distro's own updating tool would be far simpler (especially since a lot of distros patch KDE with their own custom patches)."
    author: "Corbin"
  - subject: "Re: Auto_Patch_Install"
    date: 2006-01-20
    body: "Doesn't prelinking already do that?"
    author: "Greg"
  - subject: "Re: Auto_Patch_Install"
    date: 2006-01-20
    body: "The CRCs are only for *Complete* Packages , and only used @ install. If you would change a file while its installed , the package managers will never know.\n\nThe advantage is clear : Fast Security Fixes."
    author: "chris"
  - subject: "Re: Auto_Patch_Install"
    date: 2006-01-20
    body: "No. RPM based systems has a db of hashes of all the files belonging to installed packages.\nSo 'rpm -qf /usr/bin/dvd+rw-mediainfo' gives the answer \"dvd+rw-tools-6.0-2\".\nYou can't change anything on the file without altering its hash value, and thereby rendering it \"unknown\" to the RPM db.\n\nrpm -qfi <filename> and rpm -V -a are very handy. The latter checks for any changes to filesizes permissions etc etc, to all installed packages.\n\nI would rather have large updates, than losing my ability to do basic security checks of installed software on my box.\n"
    author: "Peter H.S."
  - subject: "Re: Auto_Patch_Install"
    date: 2006-01-20
    body: "ah ok didnt know that, but the DB is not checked upon removal , only for security checks .\n\nIf an attacker changes a file, he will most probably change the checksum too, so i dont think you should trust thats silly check.\n\nPatch the System in the first place, and the chance an attacker replaces a file because he broke through that hole ist gone.\n\nAs linux marketshare rises, we musst have the ability to fix problems instant, and a gentoo like system where the system compiles the needed stuff and replaces it would be nice.\n\nIf it work well, then the packagemanagers can implement something like : \"register changed file in package X\"\n\n\n\n"
    author: "chris"
  - subject: "Re: Auto_Patch_Install"
    date: 2006-01-20
    body: "Even if the attacker alters the DB (generally that isn't done), you could still check against the RPM files.  Gentoo also does a similar thing (at least for the config files, not sure about everything else).  Gentoo also doesn't instantly get the new versions, someone has to update the ebuilds (with either a new tar.{gz,bz}, or add the patches to the list of patches).\n\nIt also probably would be faster in most cases to wait for the distro to release new packages rather than compiling it on the local system (think ccache and distcc).  KDE really shouldn't encroach on the package manager's territory (most distros would probably patch KDE so it wouldn't update then)."
    author: "Corbin"
  - subject: "Re: Auto_Patch_Install"
    date: 2006-01-21
    body: "I am fully aware, that one can never check the integrety of a system from it self, though one can actually perform a \"tripwire\" like check of the installed rpms from a read-only media with similar rpms on. \n\nIt is not trivial to change a hash value in the RPM db, and certainly not in a automatic way. Most attacks are automatic-scripts, either by script-kiddies, or spammers, and therefore \"rpm -V -a\" gives a reasonably defence against root-kits that overwrite systemfiles.\n\nThe present system with signed rpm packages, containing a full version of the fixed program is lightyears better than anything the competition has. The only downside is bandwith usage, which sucks for modem-users. Then again, it always sucks to be a modem user.\n\nSystem integrety is more than intrusion detection, and your proposal simply break any existing pagkage system, without any gain what so ever.\nYou talk about some \"ability to fix problems instant\", and in the next sentence about patching source code and recompiling? Applying a large rpm package is so much faster, than recompiling even a medium-sized program.\n\nI vastly prefer \"new_fixed_package.rpm\" instead of \"old_package.rpm + hotfix<largenumber>+hotfix<largenumber>+hotfix<largenumber>. It is so much easier, faster and simpler to deal with.\n\n-- \nRegards\nPeter H.S.\n\n\n"
    author: "Peter H.S."
  - subject: "Re: Auto_Patch_Install"
    date: 2006-01-21
    body: "Dumb idea."
    author: "Anonymous"
  - subject: "Re: Auto_Patch_Install"
    date: 2006-01-21
    body: "i dont think so , you can also use this for development - it monitors #kde-commits , and if you choose update , it compiles only a few files , and your ready."
    author: "chri"
  - subject: "Re: Auto_Patch_Install"
    date: 2006-01-22
    body: "kdesvn exists - for building from source but not for updating a binary distribution."
    author: "Anonymous"
  - subject: "kde-files.org"
    date: 2006-01-22
    body: "completly OT:\n\ni hope you all have seen http://kde-files.org/\n\nnice !"
    author: "chris"
  - subject: "Variable names"
    date: 2006-01-22
    body: "@@ -257,6 +262,12 @@ UString decodeURI(ExecState *exec, UStri\n        assert(n == 4);\n        unsigned long uuuuu = ((octets[0] & 0x07) << 2) | ((octets[1] >> 4) & 0x03);\n        unsigned long vvvv = uuuuu-1;\n\n\nVariables named \"uuuuu\" and \"vvvv\"?!\nReally impressive to have code of this quality in kdelibs."
    author: "EHa"
  - subject: "Re: Variable names"
    date: 2006-01-22
    body: "did you see the semantics behind those variables ? i think its ok \nthe number of chars mean something."
    author: "chris"
  - subject: "Re: Variable names"
    date: 2006-01-22
    body: "That question was answered in a comment over Slashdot (http://it.slashdot.org/comments.pl?sid=174643&cid=14527629):\n\n\u00abCheck section 15.1.3 of the ECMA standard (http://www.ecma-international.org/publications/files/ECMA-ST/Ecma-262.pdf), which the source refers to. The algorithm is explained there, and the variable names are taken from the standard for readability.\n\nSheesh, do a little homework first.\u00bb"
    author: "blacksheep"
  - subject: "firefox-Qt"
    date: 2006-01-22
    body: "No news about the Firefox port to Qt?\n\nAre there still issues with kde devs not getting cvs access to the Mozilla codebase?"
    author: "ac"
  - subject: "Re: firefox-Qt"
    date: 2006-01-22
    body: "Firefox is being ported to Qt by KDE developers?"
    author: "blacksheep"
  - subject: "Re: firefox-Qt"
    date: 2006-01-22
    body: "> Firefox is being ported to Qt by KDE developers?\n\nYes :-)\n\nhttp://dot.kde.org/1094924433/"
    author: "ac"
  - subject: "Re: firefox-Qt"
    date: 2006-02-18
    body: "Unfortunately that message was from September 2004....\n\nWould be nice to see something by now.... Would make my use linux a lot more..."
    author: "boemer"
  - subject: "Re: firefox-Qt"
    date: 2006-01-22
    body: "Everyone has access to the Mozilla CVS...\n"
    author: "Reply"
  - subject: "Re: firefox-Qt"
    date: 2006-01-22
    body: "https://bugzilla.mozilla.org/show_bug.cgi?id=297788"
    author: "firefox"
  - subject: "Re: firefox-Qt"
    date: 2006-01-23
    body: "I repeat, everyone has access to the Mozilla CVS.\n\nAs for commit rights, the link you posted answers the question. Nothing stops anyone from porting the code. It's very simple, you start contributing patches and when enough of them are accepted you'll probably get a CVS account. And if you don't, you can rely on other people commiting the patches or making them available as a branch somewhere else.\n\n"
    author: "Reply"
---
The KDE Project released a security advisory today for a <a href="http://kde.org/info/security/advisory-20060119-1.txt">heap overflow vulnerability in KJS</a>.  Earlier this month, a number of <a href="http://kde.org/info/security/advisory-20051207-2.txt">integer overflows affecting KPDF</a>, and consequentially KOffice were found and fixed.  Patches have been made available and your distributor should have updated binary packages.  The <a href="http://www.kde.org/info/security/">KDE security advisory page</a> has an overview of all KDE advisories.  Links to source patches are contained in the advisories.




<!--break-->
