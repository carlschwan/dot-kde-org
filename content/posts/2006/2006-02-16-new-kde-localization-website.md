---
title: "New KDE Localization Website"
date:    2006-02-16
authors:
  - "nternisien"
slug:    new-kde-localization-website
comments:
  - subject: "Hurray!"
    date: 2006-02-16
    body: "Hurray! The last few weeks seem to have been pretty active on the translation front.\n\nJust a quick note, isn't the new l10n website also the new home for documentation in general, not just translation related stuff?\n\nI'm looking forward to the coming year.\n\nVlad Blanton"
    author: "Vlad Blanton"
  - subject: "Re: Hurray!"
    date: 2006-02-17
    body: "Yes, you're right, there are some very good documentation on how to write a docbook or stuff like that. \n\nThe l10n name has been chosen because docs.kde.org has already been taken ;-)\n"
    author: "Nicolas Ternisien"
  - subject: "Localisation"
    date: 2006-02-16
    body: "I like the irony of the link to a site titled Localization being spelled Localisation. :)"
    author: "Ian Monroe"
  - subject: "Re: Localisation"
    date: 2006-02-17
    body: "Yes, I was very tired the day I wrote this news ...\n\nAnd I'm french of course ;-)"
    author: "Nicolas Ternisien"
  - subject: "from the tres-uber-&#922;&#945;&#955;&#974;&#962; dept.?"
    date: 2006-02-16
    body: "I've got no idea if the word \"&#922;&#945;&#955;&#974;&#962;\" got a meaning. If not, wouldn't it be nice to spell other umlauts correctly? \"tr\u00e8s\" and \"\u00fcber\" are not too difficult to spell :)\n\nIf you just wanted to use chars that cause surprise to all americans which heavn't seen umlauts before I'd recommend something like: \"The d\u00ecv\u00ebr\u00df\u00eet\u00fd \u00f5f l\u00e5\u00f1g\u00fc\u00e2g\u00e9s dept.\" -> \"The diversity of languages dept.\" ;)"
    author: "Patrick Trettenbrein"
  - subject: "Re: from the tres-uber-&#922;&#945;&#955;&#974;&#962; dept.?"
    date: 2006-02-17
    body: "\"&#922;&#945;&#955;&#974;&#962;\" is Greek for beautiful.\n\nBut I agree that spelling \"tr\u00e8s\" and \"\u00fcber\" correctly would be nice.\n\nOlaf\n"
    author: "Olaf Jan Schmidt"
  - subject: "Re: from the tres-uber-&#922;&#945;&#955;&#974;&#962; dept.?"
    date: 2006-02-17
    body: "Hm, there seems to be a bug in the web software that encodes Greek letters twice, leading to this strange \"&#922;&#945;&#955;&#974;&#9\" line."
    author: "Olaf Jan Schmidt"
  - subject: "Lol!"
    date: 2006-02-17
    body: "> Congratulations for attempting to post to KDE Dot News!\n\nWhen I saw \"It uses the default KDE layout\", I thought: \"Cool!\", but alas it's something totally different... it's the KDE _website_ layout.\n\nWhat I thought could be implemented thusly: you are running a program, you see a badly translated menu text, you right-click on it and bingo... you can choose to email the app mantainer (or translator) what the right expression would be.\n\nE.g.:\n\n\"piece of cake!\" --> \"\u00e9 sopa!\" (it's soup!)\n\n\"outline\" (text view mode) --> \"lista de t\u00f3picos\" (topic list)\n\"outline\" (hollow font) --> \"contorno\" (border) (and not topic list!)\n\nJust an idea... Sorry if it already works this way somehow and I didn't see it..."
    author: "Gr8teful"
  - subject: "&#26085;&#26412;&#35486;&#35379;&#12418;&#12354;&#12427;"
    date: 2006-02-20
    body: "&#32032;&#26228;&#12425;&#12375;&#12356;!\n\n"
    author: "Koizumi"
  - subject: "kde php framework?"
    date: 2006-02-25
    body: "\"Subsites of each teams now use the KDE PHP framework, and can easily respect KDE.org look and feel.\" \n\nWhere can I find more info about this framework? Would need it to restructure Lithuanian team site."
    author: "Donatas G."
---
After 6 months of development, the <a href="http://l10n.kde.org">KDE Localization (l10n) website</a> has been launched replacing the old i18n.kde.org.  It uses the default KDE layout, and its admins hope this site will help the KDE translation process work better than ever.  Read on for the details.






<!--break-->
<p>
This refactoring, for the moment, mostly modifies scripts, pages, and styles on the site. As you can see there are no main differences between features of this site and the old one. In fact, we hope this site will be the beginning of new ideas that people could have to improve the translation and internationalization processes in the KDE project.
</p>
<p>
This big updating was also the opportunity to rewrite the scripts which generate <a href="http://l10n.kde.org/stats/gui/trunk/index.php">translation status pages</a> of each teams and packages.  They are now easier to use, and can be integrated quickly in other open source projects.
</p>
<p>
Subsites of each teams now use the KDE PHP framework, and can easily respect KDE.org look and feel. Some sites are already doing the conversion like the <a href="http://fr.l10n.kde.org">French site</a> or the <a href="http://el.l10n.kde.org">Greek site</a>.
</p>
<p>
The next big step will probably be the integration of a PO file management interface, to allow KDE translation teams defining specifical translator per file, to avoid duplicated work.
</p>
<p>
Of course, the rewrite of the previous website will have probably added some bugs, so do not hesitate to contact admins if you find any problem (broken links, strange behavior, etc)
</p>




