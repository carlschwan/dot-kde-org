---
title: "KDE and Business: AEI Interview"
date:    2006-05-15
authors:
  - "wolson"
slug:    kde-and-business-aei-interview
comments:
  - subject: "So true - Qt is the best software devel framework"
    date: 2006-05-15
    body: "I absolutely agree with Caleb Tennis and have to say that I share his experience with using Qt for development of commercial software products on Linux. At first I looked into GTK because of all the hype around it, but dropped it after a few days struggling to get the simplest app to work. Qt, on the other hand, is a pleasure to code in.\n\nGreat job, Trolltech!"
    author: "Nick"
  - subject: "Thanks for the interview"
    date: 2006-05-15
    body: "It's nice to see a quite lengthy interview with somebody, who uses Qt/KDE for the work.\n\nI also like working with Qt, although not professionally yet - but that's why I like companies to use Qt.\n\nHow many people are actually employed at your company?"
    author: "Martin Stubenschrott"
  - subject: "small glitches"
    date: 2006-05-15
    body: "What annoys at KDE are almost these small glitches in the default configuration and some oddness of the programs. \n\nExample Kmail, you set focus on your mails but are unable to scroll through bykeystrokes. I always did it like that, don't know what other key is used by KDE.\n\n\n\n"
    author: "elek"
  - subject: "Re: small glitches"
    date: 2006-05-15
    body: "KMail's keys are set that up and down control one thing, and left and right control another (one set controls scrolling through the list of mail, the other scrolls through one of the individual messages).  Not the most intuitive, but it does work better than the other way once you know how it works."
    author: "Corbin"
  - subject: "Re: small glitches"
    date: 2006-05-16
    body: "Yeah, KMail's keyboard navigation is broken. This has already been reported as a usablity bug. The dev's ignore it, but you can reconfigure it yourself to some extent to mak eit behave normally."
    author: "ZACK"
  - subject: "Re: small glitches"
    date: 2006-05-17
    body: "It's NOT broken. It's the only way which makes sense. I hate to grab the mouse just to change focus into the mail overview window if I want to got to the next/previous message.\n\nAfter you get used to it you don't want to miss it anymore.\n\n@Kmail devs: please igonre these usability trolls which claim that Kmail's keyboard navigation is broken. It's one of the main reasons I use Kmail.\n"
    author: "ac"
  - subject: "Re: small glitches"
    date: 2006-05-17
    body: "seconded."
    author: "me"
  - subject: "Re: small glitches"
    date: 2006-05-17
    body: "Thirded, nothing even vaguely compares to KMail in terms of usability. It's the power of a shell client, with the power of a GUI."
    author: "Alistair John Strachan"
  - subject: "Re: small glitches"
    date: 2006-05-17
    body: "Heh.  I was used to kmail and then tried to use outlook express at work.  I could not work out how to scroll through a message and switch between messages.  I worked out it was context sensitive, and I can use tab to switch the focus, but the must be a better way.\n\nKmail is by far much better."
    author: "JohnFlux"
  - subject: "Qt license"
    date: 2006-05-15
    body: "  \"Our test cell code is used in house, which means \n   that we didn't need to buy a Qt license.\"\n\nReally?  It was my understanding that if Qt is used for commercial purposes, the Trolls want it licensed commercially, regardless of distribution.  It was never clear to me if that was just a suggestion, or a hard requirement (and if intended to be the latter, whether that's inconsistent with the GPL...)\n\nAnyway, a moot point in this case, since they did buy a license.\n"
    author: "LMCBoy"
  - subject: "Re: Qt license"
    date: 2006-05-15
    body: ">> \"Our test cell code is used in house, which means \n>> that we didn't need to buy a Qt license.\"\n>\n> Really? It was my understanding that if Qt is used for commercial \n> purposes, the Trolls want it licensed commercially, regardless of\n> distribution.\n\nBut Caleb's company doesn't sell the applications, they just use them\nin-house, which makes them non-commercial.\n\nBye,\nTassilo"
    author: "Tassilo"
  - subject: "Re: Qt license"
    date: 2006-05-15
    body: "If they only use it inhouse they can even use the GPL version legally. Because the GPL only forces you to make the source available along with the binary if you give it to someone else (outhouse). "
    author: "David"
  - subject: "Re: Qt license"
    date: 2006-05-16
    body: "Yes, but acording to Trolltech, you cannot use the open-source version of Qt unless you are pulically distributing the code:\n\nhttp://www.trolltech.com/developer/faqs/190/\n\nHence my question of how they can require this and not violate the GPL.  Anyway, the reading of the above FAQ answer is a bit vague.  Maybe they only \"recommend\" the commercial license for internal applications.\n\n"
    author: "LMCBoy"
  - subject: "Re: Qt license"
    date: 2006-05-17
    body: "\"Using the Qt Open Source Edition, can I make _non-opensource software_ for internal use in my company/organization?\"\n\nKeyword:  'non-opensource software'\n\n\"Although it is possible to write open source software for internal use, it is difficult to ensure that such software is used and distributed legally. For example, if your open source software requires any modules that impose conditions on you that contradict the conditions of the GNU GPL, including, but not limited to, software patents, commercial license agreements, copyrighted interface definitions or any sort of non-disclosure agreement, then you cannot distribute it at all; hence it cannot be given to consultants, employees for their personal computers, subsidiaries, other divisions, or even to new owners.\"\n\nRight here it says that you can write open source software for internal use, just that if you're using any modules their license may contradict the GPL.\n\nNext time please read the link before posting, ok?"
    author: "Corbin"
  - subject: "Re: Qt license"
    date: 2006-05-15
    body: "indeed. so, as most software development is done in-house - most development doesn't care at all about GPL or LGPL... and some even still pay for the support!"
    author: "superstoned"
  - subject: "Re: Qt license"
    date: 2006-05-16
    body: "I'm not talking about GPL vs. LGPL, I'm talking about Trolltech's position that internal development can *only* use the commercial license; you are not supposed to use the open-source edition for internal-use applications:\n\nhttp://www.trolltech.com/developer/faqs/190/\n\n"
    author: "LMCBoy"
  - subject: "Re: Qt license"
    date: 2006-05-16
    body: "They don't say that. Read again."
    author: "Carlo"
  - subject: "Re: Qt license"
    date: 2006-12-01
    body: "GOSH, please READ the article, LMCBoy!\n\nYou are making wrong claims."
    author: "shev"
  - subject: "Re: Qt license"
    date: 2006-05-16
    body: "They could even sell the apps using the GPL version, as long as they provided the source to their customers upon request, and otherwise complied with the GPL.  Trolltech should really call their \"commercial\" license their \"proprietary\" license, since there's no problem distributing commercial apps under the GPL."
    author: "ac"
  - subject: "Re: Qt license"
    date: 2006-05-16
    body: "That's the experience I had. I've been asked if Qt/GPL prevented the author from being paid for the work done. Most people combine \"commercial\" with \"to be paid for\", which is a big issue.\n\nlg\nErik"
    author: "Erik"
  - subject: "Re: Qt license"
    date: 2006-05-16
    body: "Haha, you can sell the GPL version to a custommer but the custommer can freely post it on the Internet and everyone else can dowmload it for free.\n\nGPL really is not a commerce-friendly license. It protects rights of the user, not author's rights."
    author: "ZACK"
  - subject: "Re: Qt license"
    date: 2006-05-17
    body: "Long as your take that into account with your business model thats not a problem, RedHat is surviving rather well (and is making a profit) despite all of their code being GPL/LGPL/other OSI approved OSS license.\n\nAlso for pretty niche products the version on the Internet probably wouldn't spread very fast..."
    author: "Corbin"
  - subject: "Re: Qt license"
    date: 2006-05-16
    body: "> But Caleb's company doesn't sell the applications, \n> they just use them in-house, which makes them \n> non-commercial.\n\nWrong, according to Trolltech:\n\nhttp://www.trolltech.com/developer/faqs/190/\n"
    author: "LMCBoy"
  - subject: "Re: Qt license"
    date: 2006-05-17
    body: ">> But Caleb's company doesn't sell the applications, \n>> they just use them in-house, which makes them \n>> non-commercial.\n>\n>Wrong, according to Trolltech:\n>\n>http://www.trolltech.com/developer/faqs/190/\n\nHow about actually reading that link?  Please stop trolling."
    author: "Corbin"
  - subject: "Thanks to AEI for sponsoring QtRuby development"
    date: 2006-05-16
    body: "The interview doesn't mention QtRuby, but as well as writing a book about it Caleb and AEI have been sponsoring my work on the Qt4 version of QtRuby recently. So a big thankyou from me for that. Working on it fulltime has allowed me to get the Windows port done, along with a whole pile of bug fixes, enhancements and new example programs."
    author: "Richard Dale"
  - subject: "Re: Thanks to AEI for sponsoring QtRuby developmen"
    date: 2006-05-16
    body: "What is the title of that book?"
    author: "reihal"
  - subject: "Re: Thanks to AEI for sponsoring QtRuby developmen"
    date: 2006-05-16
    body: "\"Rapid GUI Development with QtRuby\"\n\nhttp://pragmaticprogrammer.com/titles/ctrubyqt/index.html"
    author: "Richard Dale"
  - subject: "The importance of eye candy"
    date: 2006-05-16
    body: "\"eye candy is actually quite important from a business perspective. Potential clients on plant tours tend to remember catchy things they've seen\"\n\n\nKDE eye candy is profitable for your business. I guess you didn\u00b4t see that coming."
    author: "reihal"
---
Continuing in a series of interviews with businesses that benefit KDE
and benefit from KDE, we investigate <a href="http://www.aei-tech.com/">AEI</a>
(Analytical Engineering, Inc), a Midwestern engineering firm founded in 1994.
In an interview originally conducted by <a href="http://aseigo.blogspot.com/">Aaron
Seigo</a>, AEI's design engineer and <a href="http://pragmaticprogrammer.com/titles/ctrubyqt/index.html">
author</a> Caleb Tennis discusses AEI's IT needs and KDE's involvement.




<!--break-->
<p><strong>What sort of products or services does AEI offer, and what is your role at AEI?</strong></p>
<p>
<strong>CT:</strong> We're a research and development firm.  Right now we are heavily focused on diesel
engine technology, particularly emissions.  We offer in house durability testing
and performance development.  We also make a number of products which measure
various engine characteristics, such as lubricant oil soot particles.  My role is
facilities design engineer, but I'm also head of the IT department.
</p>

<p><strong>What are AEI's IT requirements?</strong></p>
<p>
<strong>CT:</strong> We have a number of rooms (called test cells) which house the engine that
is being tested.  The engine is coupled to a large amount of instrumentation
that monitors various parameters, such as temperatures and pressures.  All of
this instrumentation is connected back to a computer, which maintains the engine
running at various conditions.  Most importantly, it has to be certain that
everything is running correctly.  If any problems happen, it has to get the
engine shut off and notify someone to fix the problem.
</p>
<p>
We also need a computer to watch over the facility globally.  To support each
of the cells, we have fans that must provide air for combustion.  There are
pumps to bring fuel into the building.  There is a process water that must
be cycled to provide cooling capacity.  All of these things must be monitored
and maintained.
</p>
<p>
Furthermore, it's very important from a cost standpoint to minimize how
hard the facility is running - we only need to provide air, fuel, and cooling
for what the current needs are at any snapshot in time. Providing extra is
wasted electricity.
</p>

<p><strong>How is KDE helping AEI meet its IT needs, and how long has AEI been using KDE?</strong></p>
<p>
<strong>CT:</strong> Having a very easy to use GUI for the test cells is very important to us.  Our
test cell computers operate in what I call "pseudo-kiosk" mode.  That is, most of
the desktop features of KDE aren't used much, but they are available. Instead, all
of the operation is done via a few custom written applications. The widgets that
are available, and the ease of customizing new widgets, is a huge plus.
</p>
<p>A polished
look helps a lot too.  Not only does it make life very simple for the operator, but
eye candy is actually quite important from a business perspective.  Potential
clients on plant tours tend to remember catchy things they've seen, and almost every
computer in our test facility that controls something uses KDE.
</p>

<p><strong>What were the key factors that led you to choose to deploy KDE at AEI?</strong></p>
<p>
<strong>CT:</strong> We chose Linux because we felt the need to have something that would be rock
solid, customizeable, and affordable.  We chose KDE because the API and documentation
for it and Qt are unbeatable.  In the end it really did come down to a Gnome/KDE
decision.  Some people still seem shocked that we didn't choose Gnome, because
it's "free".  That may be so, but from my perspective after having tried both,
Qt's design fits our needs a lot better.  There will always be an argument for
which side is better; after having evaluated both, Qt/KDE worked best for us.
</p>
<p>
When it comes down to it, we cannot afford to have a mistake happen because
the program crashed.  Many desktop users are now completely accustomed to
having to reboot every few days.  They're also used to programs crashing
haphazardly.  That may be liveable on your girlfriend's computer, but for
one that's monitoring a $1,000,000 development engine, it becomes pretty
important to not have problems because "Windows crashed".
</p>
<p>
From our experience, Linux/KDE uptime and reliability are second to none.
</p>

<p><strong>How smoothly did the initial deployment of KDE go, and does KDE integrate
with any other systems at AEI?</strong></p>

<p>
<strong>CT:</strong> Not that bad.  We initially started by using Redhat.  I fell in love with
KDevelop for my development and started getting disgrunted when trying to
compile snapshots of it.  This was also around the time that the KDE 3.0
series was being released and I wanted to try it out.  We fell into the
"rpm dependency hell" that gets talked about very frequently.  Later we
switched to a Linux From Scratch setup, and now we're using Gentoo.  While
our production systems all run stable versions, we are constantly trying
to keep up with the development versions so that we can take advantage of
what's new. Some people believe you should upgrade every few years.  I think
it's much easier if you're constantly upgrading, every few months.  You don't
get behind the game this way.
</p>
<p>
We started using KDE in one of the units we sell to customers.  This
is as a replacement to software that was running on Windows 95.  So far the
feedback has been good.  Once we're all comfortable with it, we plan to
offer more embedded KDE type products in the future.
</p>

<p><strong>How many and what sort of machines are you using KDE on?</strong></p>
<p>
<strong>CT:</strong> We have 12 test cells, 2 database servers, development servers, production
machines, and various emissions racks.  We probably have 15 computers running
KDE and another 15 running just Linux.
</p>

<p><strong>Has AEI realized any special business or technology advantages
from using KDE? (Alternatively: How has KDE been useful/instrumental in
solving problems or addressing special needs that AEI has?)</strong></p>
<p>
<strong>CT:</strong> The cost savings has been phenomenal.  Our test cell code is used in
house, which means that we didn't need to buy a Qt license.  We did
anyway - because we believe that the value was there.  Our code probably
doesn't have much value to the greater open source community, anyway.
</p>
<p>
Linux and KDE are allowing us to make use of older hardware.  When someone
needs a new desktop computer for their office, we can take their old desktop,
put KDE on it, and give it another 5 years of useful life.  This means that
instead of having to upgrade every computer in the building every 3 years, we
can "hand down" computers and get more life out of them.  Obviously, this
makes management very happy.
</p>
<p>
Most obviously, since it's all open source, we can customize anything we
need. We can also get in and see how things work.  We can get ideas from
existing code.
</p>
<p>
And it's not all about us.  I've attempted to give back by contributing
to projects that I've used.  I've worked on KDE, both in bug fixes and
documentation.  I helped as the maintainer of KDevelop through a large
portion of the 3.0 release.  Currently I'm helping maintain KDE within Gentoo.
</p>
<p>
I think if more companies realized that this method works, and that
it's substantially less expensive than what they're used to, they would be
begging for more information.  We've been working more and more
with outside companies who are experienced in Qt and KDE development for
building up some of our products.
</p>

<p><strong>What are your favourite and least favourite aspects of KDE?</strong></p>
<p>
<strong>CT:</strong> My favorite part of KDE is the strength of the community.  There are people
who thoroughly enjoy their work and their contribution.  They want to make
the product better.  If you have a problem, there's someone who wants to help you.
</p>
<p>
My least favorite is the strength of binary compatibility.  This is also
one place where the library shines brightest.  I would personally like to
see binary compatibility broken more often, particularly when it adds tons
of functionality.  But I move a lot faster than the rest of the world - and
I'm sure 95% would disagree with me.  I want the latest and greatest.  I don't
mind recompiling.
</p>

<p><strong>Where would you like to see the future of KDE go, and what would you
like to see in future releases?</strong></p>
<p>
<strong>CT:</strong> It's in the right direction now.  Continuous improvement - listen for feedback
and make things better.  The people who care about KDE are doing this anyway.
</p>
<p>
Better interoperability with Gnome is important, and I think that's heading
the right direction.  <a href="http://www.freedesktop.org/wiki/">freedesktop.org</a>
is a wonderful idea.
</p>
<p>
Currently I fail to understand why so many people are scared of Qt's
licensing scheme.  My prediction is that over the next few years, a lot
of companies getting interested in Linux will jump to Gnome to avoid
paying for Qt.  Right now the Gnome folks are dangling this carrot
by saying "We're LGPL.  You can use us for free!".  This will translate
to higher costs in the end as the key Gnome players charge more and more
for their services. It's like buying razors.  The razor costs almost
next to nothing; it's the refills that are really expensive.
</p>




