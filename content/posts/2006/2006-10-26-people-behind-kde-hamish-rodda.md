---
title: "People Behind KDE: Hamish Rodda"
date:    2006-10-26
authors:
  - "tbaarda"
slug:    people-behind-kde-hamish-rodda
comments:
  - subject: "Thanks!"
    date: 2006-10-26
    body: "Good read! \n\nI'd like to thank Hamish for his work on KDevelop, Kate and KDE in general!\n\nThere's just one thing I don't understand: how does a medical doctor have enough time to hack this much on KDE, much less learn how to write such fantastic code?"
    author: "Anon"
  - subject: "Re: Thanks!"
    date: 2006-10-26
    body: "27 years, doctor, and all that hack on KDE.\n\nI must admit, programmers are not \"normal\" people, and me moaning in work about lack of time.\n\ncheers\nmimoune"
    author: "djouallah mimoune"
  - subject: "Re: Thanks!"
    date: 2006-10-26
    body: "Agree, he must be a superior doctor and developer! I can't imagine how he could divide his time as a doctor and as a programmer...\n\nthanks Hamish for your great works!"
    author: "fred"
  - subject: "Re: Thanks!"
    date: 2006-10-26
    body: "Well still up for adoption, saves a lot of time....."
    author: "Boemer..."
  - subject: "First things first. Married, partner or up for ado"
    date: 2006-10-26
    body: "don't flame me ! I really don't understand it \n\nwhat do you mean by \"up for adoption\", of course married has an universal meaning; partner is a kind of a mariage without a formal commitement; but for up ado I don't get it\n\nregards\nmimoune"
    author: "djouallah mimoune"
  - subject: "Re: First things first. Married, partner or up for ado"
    date: 2006-10-26
    body: "'Up for adoption' means that someone does not have a partner at all (be it formal or not). In that case, he/she can be 'adopted' by someone in the same situation :)."
    author: "Jonathan Brugge"
  - subject: "Re: First things first. Married, partner or up for ado"
    date: 2006-10-26
    body: "ah ok thanks\n\nnow I understand why he is so active;)anyway we really need people like him, as I read it not sure the next kubuntu will include kde4 - after 6 months-\n\nmimoune"
    author: "djouallah mimoune"
  - subject: "Re: First things first. Married, partner or up for ado"
    date: 2006-10-26
    body: "> now I understand why he is so active ;)\n\nSo we need to make sure that he's not adopted, right? After all, KDevelop 4 ought to be released in time... :-P\n\nCheerio, Hamish! Glad you're there, your code is nothing but exceptional. i hope you can make it to next year's aKademy :-?"
    author: "Jakob Petsovits"
  - subject: "Re: First things first. Married, partner or up for ado"
    date: 2006-10-27
    body: "Thanks for everyone's kind words... although the interview is a bit out of date, I have a partner now, but the hacking should continue... :)"
    author: "Hamish Rodda"
  - subject: "Re: First things first. Married, partner or up for ado"
    date: 2006-10-27
    body: "Now that's a head line: \"do a people behind KDE interview, get adopted!\" :)"
    author: "Kevin Krammer"
  - subject: "Origin of Name?"
    date: 2006-10-27
    body: "I'm curious about your name \"Hamish Rodda\". Doesn't sound like a typical Aussie name (I could be wrong), so what's the story behind the name? :-)\n"
    author: "AC"
  - subject: "Re: Origin of Name?"
    date: 2006-11-03
    body: "The name is fairly unique but not too unusual... Hamish is Scottish (parents just liked it) and Rodda is an English surname originally."
    author: "Hamish Rodda"
  - subject: "Well deserved!"
    date: 2006-10-28
    body: "Hamish,\n\nNice to see you're getting the recognition your deserve mate! I've been out of the loop for quite some time and something tells me I ought to fix that... reading your interview showed me I've got my priorities all wrong. ;)\n\nMaybe, just maybe I'll commit a few gems for this Christmas...\n\n- K\n\n\n"
    author: "gallium"
---
Tonight in the <a href="http://people.kde.nl/">People Behind KDE</a> series of interviews we feature an Australian core hacker. He is very motivated in programming but his social life is as important. He focuses mainly on programming tools but works for core parts like kdeui too. We are talking about KDE star <a href="http://people.kde.nl/hamish.html">Hamish Rodda</a>.

<!--break-->
