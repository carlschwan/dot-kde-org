---
title: "K3b Fundraiser 2006: A Complete Success"
date:    2006-04-17
authors:
  - "strueg"
slug:    k3b-fundraiser-2006-complete-success
comments:
  - subject: "K[4]B?"
    date: 2006-04-17
    body: "Will the current K3B be renamed K4B when the release of Qt4 is realized and KDE is based on it? Or let me put it this way...Why the `3' in K3B? Thanx."
    author: "cb"
  - subject: "Re: K[4]B?"
    date: 2006-04-17
    body: "The \"3B\" is short for \"Burn, Baby, burn\". So it will probably not be changed."
    author: "AJ"
  - subject: "Re: K[4]B?"
    date: 2006-04-17
    body: "K4B -> Burn, Baby Burn Better.... ?"
    author: "boemer"
  - subject: "Burn, Baby Burn Better"
    date: 2008-02-02
    body: "+1"
    author: "anonymous"
  - subject: "Re: K[4]B?"
    date: 2008-02-03
    body: "lol. I like that\n\nEither way, as long as the program is great and the Penguin with the blowtorch stays and gets updated visuals I'm happy.\n\nI love using it just because of that cute logo.\n\nMaybe include Konqi the dragon as a helper? Just brainstorming. :-)"
    author: "Max"
  - subject: "Re: K[4]B?"
    date: 2008-04-16
    body: "-1\nGood idea, but I think that changing the name of this wonderful application can confuse people. Long life to Burn, Baby Burn!"
    author: "zzack3"
  - subject: "Re: K[4]B?"
    date: 2008-04-16
    body: "i think that the name of k3b should remain the same...we can't change the name at every version...maybe we can give it a \"surname\" for every version...(see ubuntu dapper/gutsy/etc)\n"
    author: "Luca "
  - subject: "Re: K[4]B?"
    date: 2008-10-27
    body: "always thought it meant k free burn"
    author: "jorge"
  - subject: "3893.17 Euro"
    date: 2006-04-17
    body: "I'm missing what happened to this money which you didn't intend to collect."
    author: "Anonymous"
  - subject: "Re: 3893.17 Euro"
    date: 2006-04-17
    body: "he bought a better pc.\nI wonder what a machine you can get for 3.8k &#8364; - 30'' display , 4800+ opterons dual,... 8gb ram\n\ni am fine with that, if he got a monster machine."
    author: "ch"
  - subject: "Re: 3893.17 Euro"
    date: 2006-04-17
    body: "He deserves it for providing us with such a great product."
    author: "Ian Whiting"
  - subject: "Re: 3893.17 Euro"
    date: 2006-04-17
    body: "Maybe he bought lots of different dvd writers to test them."
    author: "Kugelblitz"
  - subject: "So what?"
    date: 2006-04-17
    body: "The author very much deserves this and much more. Sebastian has consistently cranked out good code and an excellent application. To have given us all this wonderful free (beer and speech) app without asking anything in return speaks volumes of his devotion to free software and of his own generosity.\n\nSo he got himself a fantastic system and had a bit of money to spare to go on vacation for a weekend: well deserved I say!\n\nWe need to pamper our KDE developers. They are the best thing to have happened to the free software world and they keep cranking excellent code every single release.\n\nBut hey, be a miser and piss on other people's parade if that's your thing.\n"
    author: "Gonzalo"
  - subject: "Re: 3893.17 Euro"
    date: 2006-04-17
    body: "I couldn't care less. He deserves it and much more than that for his great work. I don't have a doubt all donors would agree with this. If you were trying to flame... that's not gonna work."
    author: "J.B."
  - subject: "Loose the frames or fix your links please!"
    date: 2006-04-17
    body: "When clicking an external link on your homepage, the location bar still claims 'www.k3b.org', because you use a extra frameset. Please do not! I'm sure we can bear viewing the actual URL of the homepage! Or at least fix your links!\n\nI think k3b is a great program, and i use it for my burning needs, now also for DVD media.\n\n"
    author: "Anders"
  - subject: "WELL DONE!"
    date: 2006-04-18
    body: "I'm personally glad to see you used the money for the purpose you stated it was for.  I hope you got a really awesome system that will give you pride and enjoyment.  \n\nIt's good that the community does come together and reward those coders out there that do this as a hobby.  They do so much yet ask so little."
    author: "tannhaus"
  - subject: "Blu-ray support"
    date: 2006-04-18
    body: "Congratulations for the fund raising efforts, and thanks for everybody who supported this worthy cause.\n\nIt seems Blu-ray drives (http://www.blu-ray.com/drives/) may hit market any time soon if not already available now. I'm using K3b on a Tomahawk Desktop box and working fine for my DVD burning requirements.\n\nI wonder, does the current version of K3b (0.12.15) support Blu-ray disc burning? If not, I kindly propose the K3b project, please use part of the excess funds to buy a Blu-ray disc burner. Thank you."
    author: "Paul"
  - subject: "Homepage"
    date: 2006-04-18
    body: "Maybe you can use 10 Euro of that money for a k3b home page without advertizement popup?"
    author: "Anonymous"
  - subject: "Re: Homepage"
    date: 2006-04-18
    body: "don't underestimate the costs of bandwith..."
    author: "superstoned"
  - subject: "Re: Homepage"
    date: 2006-04-18
    body: "many developers now pay this themselves!"
    author: "superstoned"
  - subject: "Summer of code"
    date: 2006-04-18
    body: "Will we get summer of code opportunities to work on K3B?\n\nhttp://code.google.com/soc/mentorfaq.html"
    author: "Hen Tai"
  - subject: "Love it!"
    date: 2006-04-18
    body: "I love it when people who create great product that others use to get a lot of money.  To me, K3B is the best burning software there is in the industry today.  Beating out all the other burning software in Windows.\n\nWay to go Sebastian!!!"
    author: "zero"
  - subject: "Hooray!!!"
    date: 2006-04-19
    body: "I'm very happy for you Sebastian. Every time I use k3b it just works. Plain and simple. I was happy to be a small part of the fund drive. It feels good to know that the developer of something I find indispensible is being directly rewarded. Congratulations and thank you again for all your hard work."
    author: "0x29a"
  - subject: "I forgot to mention..."
    date: 2006-04-19
    body: "Thanks and hooray for Chris, too!!"
    author: "0x29a"
  - subject: "screenshots"
    date: 2006-04-19
    body: "errr.... boxshots?"
    author: "bangert"
  - subject: "Lightscribe support"
    date: 2006-04-29
    body: "I hope that Sebastian will buy a lightscribe-enabled writer too :-D\n\nThat's the only feature I miss from K3B!"
    author: "Alessandro Pentori"
  - subject: "Y'know, Blu-Ray recorders and"
    date: 2009-03-20
    body: "Y'know, Blu-Ray recorders and media are quite expensive these days and developing a burning software for them requires a few :)"
    author: "Sten"
---
At the beginning of March 2006, I <a href="http://dot.kde.org/1141847740/">started a fundraising campaign</a> with the goal of collecting 1000 Euro by
the end of the month in order to buy a new computer system.  I soon  discovered how very unrealistic this goal
was!  You -- the K3b users -- taught me a lesson: by the end of the second day I had already received more than 1000 Euro and in the end the goal was surpassed by far.

Read the full story at <a href="http://www.k3b.org/">the K3b news page</a>.






<!--break-->
