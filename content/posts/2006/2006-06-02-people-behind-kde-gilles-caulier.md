---
title: "People Behind KDE: Gilles Caulier"
date:    2006-06-02
authors:
  - "jriddell"
slug:    people-behind-kde-gilles-caulier
comments:
  - subject: "praise 'em!"
    date: 2006-06-02
    body: "I want to thank the digikam developers for such a great app! I'm not much in photographing myself, but my girlfriend is and she uses digikam exclusively. \nShe likes digikam. There are not a lot of apps which have this pleasure :-) \nI will talk to her about the mentioned money flow...\n"
    author: "infopipe"
  - subject: "Thank you"
    date: 2006-06-02
    body: "I set up my ex-wife on digikam about a year ago. and after a bit, she fell in love with it. Fortunately for me, that means that it is easy to support from afar."
    author: "a.c."
  - subject: "digiKam"
    date: 2006-06-02
    body: "What a fantastic application."
    author: "Segedunum"
  - subject: "simply the best :-)"
    date: 2006-06-04
    body: "i take this opportunity to thank, again! and again!, to Gilles and all the other developers of this beautiful and amazingly useful application !\nluck me, i was born to use it !\n\nkindly,\nnadav :-)"
    author: "nadav kavalerchik"
  - subject: "Thanks to all to support digiKam"
    date: 2006-06-04
    body: "I would to thanks all users who supports digiKam project.\n\nAbout next release, we working to finalize 0.9.0-beta1. I hope to provide this version in a near future. We would polish implementation before to publish beta1 tarball over the world. There are some problems identified and we would kill all to prevent a lot of reports in B.K.O.\n\nTODO and NEW files from svn are updated in live. Take a look for more informations:\n\nhttp://websvn.kde.org/trunk/extragear/graphics/digikam/TODO?rev=547883&view=auto\nhttp://websvn.kde.org/trunk/extragear/graphics/digikam/NEWS?rev=547883&view=auto\n\nRegards\n\nGilles Caulier"
    author: "Gilles Caulier"
---
Tonight's <a href="http://people.kde.nl">People Behind KDE</a> interviews the developer of KDE's premiere photo management application <a href="http://www.digikam.org/">Digikam</a>.  <a href="http://people.kde.nl/gilles.html">Gilles Caulier</a> started out as a French translator for KDE but is now busy programming for hours each day.  Find out his development tools of choice and his most influential photographer in our interview.

<!--break-->
