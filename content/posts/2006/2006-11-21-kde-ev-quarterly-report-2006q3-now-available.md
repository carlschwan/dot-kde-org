---
title: "KDE e.V. Quarterly Report 2006Q3 Now Available"
date:    2006-11-21
authors:
  - "jriddell"
slug:    kde-ev-quarterly-report-2006q3-now-available
comments:
  - subject: "What about Novell/Suse?"
    date: 2006-11-21
    body: "Suse used to sponsor some KDE developers but recently they have switched their default desktop to Gnome (not to mention their dubious deal with Micro$oft). Does that mean we (KDE community) are in a danger of losing this regular source of financial support?"
    author: "Artem S. Tashkinov"
  - subject: "Re: What about Novell/Suse?"
    date: 2006-11-21
    body: "> Suse used to sponsor some KDE developers \n\nSUSE did and does not only sponsor KDE development but they also do employ several KDE developers. The number of those developers more or less actively working on KDE has basically remained unchanged. Concerning the default: That is mostly true for the enterprise products and if you look at openSUSE I'd rather argue that the developers behind openSUSE still show a major focus on KDE. \n"
    author: "Kandalf"
  - subject: "Re: What about Novell/Suse?"
    date: 2006-11-21
    body: "What about it? I'm not sure why you're bringing this up here -- the report is about July-September, which is before any of the events around Novell and Microsoft happened. Neither do I see any mention of corporate support in the quarterly; Novell *was* a Bronze sponsor of aKademy, though.\n\nPlease, keep comments on-topic for the news item you are responding to. For vague worries or serious questions about unrelated topics, I think the KDE forum (www.kde-forum.org) is a better place to start."
    author: "Adriaan de Groot"
  - subject: "About the e.V."
    date: 2006-11-21
    body: "I think I need to be glad of serendipitously posting a blog entry \"what is the e.V.\" just yesterday: http://people.fruitsalad.org/adridg/bobulate/index.php?/archives/312-What-is-the-e.V..html . That entry tries to explain what the e.V. is and what it does and why in a relatively lighthearted way. That might help those considering becoming members."
    author: "Adriaan de Groot"
  - subject: "The Pdf file"
    date: 2006-11-22
    body: "In case the links are supposed to be clickable, they're not. Assuming the stuff in blue are meant as links. If you wanted to follow some information in the document, it must be found the hard way -- googling or clicking through the kde.org pages.\nSo either my Kpdf is too old (0.4.3, KDE 3.5.1), you could create clickable urls, or give an appendix with the urls."
    author: "Anonymous"
  - subject: "Re: The Pdf file"
    date: 2006-11-23
    body: "It's caused by the way the PDFs are generated from KWord. In KWord the links are clickable, then it goes to PS with KWord's printing engine (losing the clickability but keeping the formatting that makes it look like a link) and then is converted to PDF."
    author: "Adriaan de Groot"
---
The <a href="http://ev.kde.org/reports/ev-quarterly-2006Q3.pdf">KDE e.V. Quarterly Report</a> is now available for July to September 2006.  Topics covered include the outcomes from the <a href="http://ev.kde.org/reports/2006.php">2006 membership meeting</a>, the status of the Technical Working Group's <a href="http://ev.kde.org/rules/twg_charter.php">improved charter</a>, the new press channel from the Marketing Working Group and for the first time a report from the Sysadmin Team.  All long term KDE contributors are welcome to <a href="http://ev.kde.org/getinvolved/members.php">join KDE e.V</a>.






<!--break-->
