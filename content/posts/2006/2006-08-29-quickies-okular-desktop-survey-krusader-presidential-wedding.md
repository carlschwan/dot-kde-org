---
title: "Quickies: Okular, Desktop Survey, Krusader, Presidential Wedding"
date:    2006-08-29
authors:
  - "jriddell"
slug:    quickies-okular-desktop-survey-krusader-presidential-wedding
comments:
  - subject: "Surveys"
    date: 2006-08-29
    body: "The trouble with those KDE/Gnome surveys is that when some people hear about them they turn it into a contest, and tell everyone in #gnome or #kde to go and fill it in, and write about it on their blog.\n\nThis makes the sample decidedly non-random."
    author: "Tim"
  - subject: "Re: Surveys"
    date: 2006-08-30
    body: "So consider it a survey of which desktop environment has the most enthusiastic user base.  :)"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Surveys"
    date: 2006-08-30
    body: "Enthusiastic or desperate?"
    author: "Anonymous"
  - subject: "KDE is starting to loose momentum"
    date: 2006-08-29
    body: "Gnome is really increasing in usage, and let's face it: distros are helping out.\n\nI was one that didn't belived the default option for distros would make people choose gnome after KDE or vice-versa, but now I see every day some friends trying out Ubuntu or Fedora, and they don't even know what KDE is.\n\nThere isn't something wrong with that, the distros are free to do anything they want, they can even exclude KDE. There was a lot of complain of distros not giving gnome a chance, but now it seems the game is turning, so the question is:\nIsn't there anything we can do to make distros give KDE more importance?\nDo the distros have the GPL in KDE so much and love the LGPL in gnome?\n\nMaybe we could contact distros to see what we could do to make them like KDE more."
    author: "Iuri Fiedoruk"
  - subject: "Re: KDE is starting to loose momentum"
    date: 2006-08-29
    body: "ou know what: In Germany magazines are sold with Ubuntu CDs but when you look at Ubuntu a bit closer you will find out that they packed Kubuntu on the CD.\n\nKDE is still the leading distribution. But it would be great if SuSe would recover from the hostile takeover.\n\nGPL or LGPL is a minor topic. \n\nNot companies have to like it. Users. And users do."
    author: "gerd"
  - subject: "Re: KDE is starting to loose momentum"
    date: 2006-08-29
    body: "But new users stick with what they got to know. See how many changed the wallpaper and the colors."
    author: "frinring"
  - subject: "Re: KDE is starting to loose momentum"
    date: 2006-08-29
    body: ">But it would be great if SuSe would recover from the hostile takeover.\n\n1st, Suse was not a hostile takeover. It was properly sold. \n2nd, Novell allowed the 2 groups to battle it out and the GNOME group appears to be winning at this time.\n\nBut I agree with the original poster. It would appear that distro's pushing a default of GNOME will come to haunt. Basically, it is the same principle as Windows. You get the most out there and ppl get locked in."
    author: "a.c."
  - subject: "Re: KDE is starting to loose momentum"
    date: 2006-08-29
    body: "> Novell allowed the 2 groups to battle it out and the GNOME group appears to be winning at this time.\n\nAssuming there was \"a battle\" to push KDE from the enterprise distribution then it seems that \"the GNOME group\" lost. "
    author: "Anonymous"
  - subject: "Re: KDE is starting to loose momentum"
    date: 2006-08-29
    body: "The battle is, who is in control. Gnome is in control. For instance, they have moved yast to GNOME. In addition, they have moved from KDE being the default to GNOME. This is a battle of attrition. Slowly but surely, they count on gaining a bit at a time. That is why when the SuSE CEo left, he was wicked to the gnome guys."
    author: "a.c."
  - subject: "Re: KDE is starting to loose momentum"
    date: 2006-08-29
    body: "> they have moved yast to GNOME\n\nYou don't know what you're talking about.\n\n> That is why when the SuSE CEo left\n\nAgain, you don't know what you're fudding about."
    author: "Anonymous"
  - subject: "Re: KDE is starting to loose momentum"
    date: 2006-08-29
    body: "Why don't you enlighten us instead of trolling? a.c. is making some good points."
    author: "Other AC"
  - subject: "Re: KDE is starting to loose momentum"
    date: 2006-08-29
    body: "I already tried to enlighten you that they are not good points because they are not true. But it seems it doesn't help..."
    author: "Anonymous"
  - subject: "Re: KDE is starting to loose momentum"
    date: 2006-08-29
    body: "\"You don't know what you're talking about.\"\n\nThey're pointlessly writing a GTK front-end for YaST. A good case of NIH syndrome.\n\n\"Again, you don't know what you're fudding about.\"\n\nI think we can all see who's been doing the fudding. Unfortunately, neither Gnome or KDE or open source is going to win anything because of it."
    author: "Segedunum"
  - subject: "Re: KDE is starting to loose momentum"
    date: 2006-08-29
    body: "GTK!=GNOME, moving to!=adding additional frontend, they!=single SoC sponsored student - 'nough said."
    author: "Anonymous"
  - subject: "Re: KDE is starting to lose momentum"
    date: 2006-08-30
    body: "In the eyes of all Gnome users I've met, a GTK program is a Gnome program. It's not true of course, but it's just the way it is. \n\nAnd it's just a matter of time before KDE gets dropped (or abandoned, even if they keep including it) from Suse. Do you think they're going to pour resources forever into two different environments and duplicate all their work? Of course not. SLED shows this is the direction Novell is going to take. \n\nIf you think I'm spreading FUD, just wait. I personally think KDE is the one that should succeed since is far, far superior to Gnome, but we all know in the real world things are often not as they should."
    author: "Derek R."
  - subject: "Re: KDE is starting to lose momentum"
    date: 2006-08-30
    body: "> And it's just a matter of time before KDE gets dropped (or abandoned, even if they keep including it) from Suse.\n\nThen I'll drop SUSE.\n\n"
    author: "Yogesh M"
  - subject: "Re: KDE is starting to lose momentum"
    date: 2006-08-30
    body: "Hey, me too. Maybe even Linux. Don't know if something like FreeBSD will suit my needs, but I would give a try if this situation really happens, as I fear it will. The only thing I know is I want to use KDE, I don't really care much about what's running behind the curtains."
    author: "Derek R."
  - subject: "Re: KDE is starting to lose momentum"
    date: 2006-08-30
    body: "Me too. But, If the OS doesn't matter for us, just the desktop, then, why don't use our own, KDE specific, application packaging and distribution method?, that's the only thing that kde lacks of ... but maybe that is getting too near to gnustep, isn't it?"
    author: "marce"
  - subject: "Re: KDE is starting to lose momentum"
    date: 2006-08-30
    body: "\u00abIn the eyes of all Gnome users I've met, a GTK program is a Gnome program. It's not true of course, but it's just the way it is.\u00bb\n\nActually, I find it to be true. Gnome base library is pretty much just an extension of GTK+, while the KDE base library actually enhances Qt. So, if it follows the Gnome HIG and uses Gnome libraries where it makes sense (eg. an audio player should use GStreamer rather than say Alsa), it is a Gnome application."
    author: "blacksheep"
  - subject: "Re: KDE is starting to lose momentum"
    date: 2006-08-30
    body: "> And it's just a matter of time before KDE gets dropped (or abandoned\n\nFUD\n\n> Do you think they're going to pour resources forever into two different environments and duplicate all their work? Of course not.\n\nThe irony is that Novell is really putting resources into duplicated work - on the GNOME side. They developed F-Spot and Banshee while already the best photo manager and music player existed with Digikam and Amarok available for free from the community for KDE. So when they want to remove \"duplicate work\" then there is nothing to cut on KDE side.\n\n> SLED shows this is the direction Novell is going to take.\n\nSLED shows that Novell has committed to provide support for its KDE for the next seven years."
    author: "Anonymous"
  - subject: "Re: KDE is starting to lose momentum"
    date: 2006-08-30
    body: "> FUD\n\nI sincerely hope you're right and it's just that. \n\n> The irony is that Novell is really putting resources into duplicated work - on the GNOME side\n\nOf course, because they've chosen GTK for their enterprise distribution and it had a long way to catch up with KDE. Nothing makes me think once they get there and actually start innovating in the GTK side they will bother to duplicate that work in KDE. Why do I think so? Because spending your resources into two different platforms is just a waste, it doesn't make sense. \n\n> SLED shows this is the direction Novell is going to take.SLED shows that Novell has committed to provide support for its KDE for the next seven years.\n\nNo, if anything, it shows that with the product they intend to make money with they're going fully GTK, and they're sending a very clear message about it."
    author: "Derek R."
  - subject: "Re: KDE is starting to lose momentum"
    date: 2006-08-31
    body: "novell recently created kde-applications like kerry and knetworkmanager.\nThey also are busy creating a totally new k-menu (similar to the gnome one in SLED). And there is probably more KDE related stuff in the hood of Novell that we might see in the future.\n\nThose aren't actions I would expect from a distribution that wants to abandon KDE"
    author: "AC"
  - subject: "Re: KDE is starting to lose momentum"
    date: 2006-08-31
    body: "\"The irony is that Novell is really putting resources into duplicated work - on the GNOME side. They developed F-Spot and Banshee while already the best photo manager and music player existed with Digikam and Amarok available for free from the community for KDE.\"\n\nThey did that because they looked at Digikam and Amarok and thought \"Shit, we'd better do something\". The fact that Digikam and Amarok exist without Novell having to start projects to initiate them is testament to which desktop environment has the better development framework and where they should really be heading ;-)."
    author: "Segedunum"
  - subject: "Re: KDE is starting to loose momentum"
    date: 2006-08-30
    body: "Surely Novell is investing in Gnome and related tech big time, but I still feel there is a good commiteement towards KDE (not sure if as good as before though). The OpenSuse community and the distribution they build is pretty much KDE oriented btw.\n\nAbout Yast-GTK, I don't find it as pointless. Integrating setup tools make a lot of sense; the user should expect to find Yast tools together with the desktop control center and well integrated, not think about them as an application. There are also other reasons why you would want to make a new fronted. Yast-Qt has some unnecessary complex interfaces and the code is not any prettier. For reference, Yast-Qt and -GTK package selectors:\nhttp://rpmcruz.planetaclix.pt/trash/package-selector-qt.png\nhttp://rpmcruz.planetaclix.pt/trash/package-selector-gtk.png\nOf course I am biased because I am the SoCee guy doing it. :) (and yep, it's not a Novell effort at all. And all other Summer of Code projects were given to KDE stuff btw.)"
    author: "blacksheep"
  - subject: "Re: KDE is starting to loose momentum"
    date: 2006-08-31
    body: "\"Of course I am biased because I am the SoCee guy doing it. :) (and yep, it's not a Novell effort at all.\"\n\nWell, if Novell are 'going Gnome' then they're even dafter than I thought. You would have thought 'they' would have at least put in an effort to port Qt YaST to looking right under Gnome, or something similar."
    author: "Segedunum"
  - subject: "Re: KDE is starting to loose momentum"
    date: 2006-08-29
    body: "> Gnome is really increasing in usage\n\nWell, I'm not so sure. How can you be so sure?"
    author: "Ale"
  - subject: "Re: KDE is starting to loose momentum"
    date: 2006-08-29
    body: "I see every day more and more people using gnome. \nThe pool results show a LOT of ubuntu users out there, the advancege on pools is decreasing.\n\nNot a scientific measure, I assume, but a fairy one.\nMaybe it's just me?"
    author: "Iuri Fiedoruk"
  - subject: "Re: KDE is starting to loose momentum"
    date: 2006-08-29
    body: "> The pool results show a LOT of ubuntu users out there\n\nThe poll was advertized on Ubuntu Forums (gg:ubuntuforums survey) despite being asked to \"refrain from promoting or advertising the survey to mailing lists, or encouraging friends or co-workers to vote for specific software\" so don't be surprised about high Ubuntu and GNOME numbers."
    author: "Anonymous"
  - subject: "Re: KDE is starting to loose momentum"
    date: 2006-08-30
    body: "According to Desktop Linux Polls: \nUbuntu = Ubuntu + Kubuntu + Edubuntu"
    author: "Yogesh M"
  - subject: "Re: KDE is starting to loose momentum"
    date: 2006-08-30
    body: "Me too, specially experienced user going from slackware to ubuntu and new ones beggining with ubuntu. The former know there are other things out there (read: KDE), latters don't, and they are the future (that is, the mass).\nAnyway, this year's poll at linuxquestions, show different results:\nhttp://www.linuxquestions.org/questions/forumdisplay.php?f=69\nNevertheless, there is a common point, gnome is gaining moment."
    author: "marce"
  - subject: "Re: KDE is starting to loose momentum"
    date: 2006-08-29
    body: "i think we'll see the rise after kde4"
    author: "Nick"
  - subject: "Re: KDE is starting to loose momentum"
    date: 2006-08-29
    body: "My impression is, that KDE has an increasing momentum. Amarok, K3b, Kontact, Digikam, KOffice since 1.4, ... just to name what comes to my mind. All those applications are great and even many GNOME-users use them.\nYou are right that GNOME finally takes greater steps forwards (thanks for Novell pushing very hard). But that doesn't mean  KDE is \"loosing\".\nKDE is more alive than ever, and it surely has a very bright future"
    author: "birdy"
  - subject: "Re: KDE is starting to loose momentum"
    date: 2006-08-29
    body: "\"I was one that didn't belived the default option for distros would make people choose gnome after KDE or vice-versa, but now I see every day some friends trying out Ubuntu or Fedora, and they don't even know what KDE is.\"\n\nRelax! It's not like KDE is like an endangered species. \nIn my opinion, the transition to KDE4/QT4 is the reason for the current lesser attention. KDE 3 doesn't get the love it was used to, while KDE4 is far from release. So what do you expect?\n\nProbably it's more like the silence before the storm..."
    author: "Joe"
  - subject: "Re: KDE is starting to loose momentum"
    date: 2006-08-29
    body: "*** warning! playing armchair strategist can lead to panic attacks ***\n\n.. that and too many people try and measure things by the buzz dejour of the community (which usually boils down to cults of personality and the volume of one's voice) versus the reality of usage in the real world.\n\nhere's kde's quick reality tally:\n\n- our developer community is growing\n- our user community is growing\n- our public exposure is growing\n\nif that's so, in what ways is KDE having problems again? ;)\n\nthe idea that \"gnome is the measuring stick of kde and vice versa\" is really not useful. what -is- useful is to compare kde against itself:\n\n - are we growing?\n - if so, faster or slower than global the desktop market is growing?\n - what can we do to increase our growth by increasing our exposure and usage?\n\nand no, we don't have to (nor should we, imho) try and mimic (or discount out of hand) what gnome does to answer that last question. we must chart a map that works for who we are and what we do. the two projects have historically had different methods of creating, sustaining and supporting growth which reflects differences in personalities and goals. currently both strategies are working (huzzah for free software!) and switching to something that doesn't resonate with our unique core values (\"that which is KDE\") would not be progressive for the project.\n\nadded to that, the answer to increasing exposure and usage often includes an element of working with other free software projects including gnome and gnome-friendly people.\n\nin summary:\n\ngnome is not our competition. microsoft and apple are.\nwe do not need to best gnome, we need to best ourselves."
    author: "Aaron J. Seigo"
  - subject: "Re: KDE is starting to loose momentum"
    date: 2006-08-29
    body: "I agree, thats why I said \"relax!\" :)\n\nGnome is no enemy, you have to keep that in mind if you compare usage. However, it's quite natural that a project gets sometimes more or less popularity. So what?\n\nRegards, Joe"
    author: "Joe"
  - subject: "Re: KDE is starting to loose momentum"
    date: 2006-08-29
    body: "Yes, I do agree, the problem isn't gnome, I belive.\nThe problema is that, for some reasons, distros are placing gnome as defult option. I wanted to know the reasons for this, so maybe we could push for KDE being at least a option in the same level as gnome.\nYou see, for years gnomers complained about distros using too much KDE, maybe it's your turn to look for more usage on distros?\n\nAnyway, my intention wasn't create a trollwar, maybe the KDE EV group is already doing something about it and I just don't know. Or maybe simply we should not care about distros default, and go ahead."
    author: "Iuri Fiedoruk"
  - subject: "Re: KDE is starting to loose momentum"
    date: 2006-08-29
    body: ">>The problema is that, for some reasons, distros are placing gnome as defult option.\n\n\nSo what's new?\nredhat/fedora have always placed gnome as default option(with kde as alternative), and ubuntu is the first popular distro that uses gnome as only environment (comparing with kde: linspire, lycorish, xandros, pardus, etc. all ship with only kde as desktop). \nAnd even ubuntu could not stop kde: within short time, kde was available as alternative on gnome, and has now its own playground with kubuntu.\n"
    author: "AC"
  - subject: "Re: KDE is starting to loose momentum"
    date: 2006-08-29
    body: "Agree with most, but would correct:\nProprietary software with closed specifications is our competition.\n\nMicrosoft and Apple only so far, as they still tend to believe in that crap. That day they are getting it there will be no more hard competition, but happy coevolution for the best solutions. Apple here even already might have a chance to get the twist why we want more control of our systems (ignoring that DRM business here)."
    author: "frinring"
  - subject: "Re: KDE is starting to loose momentum"
    date: 2006-08-29
    body: "Perfectly right.\n\nHowever, there is another battle that, in my opinion, needs to be faced : gaining momentum on the professional side.\nIt is clear that KDE is very strong in the desktop-linux arena, and as you pointed out its aim is to grow in the desktop-computer arena. But this battle can only be won if KDE manages to get enterprise support. Unfortunately nowadays, as I see the situation, managing to get enterprise support means fighting against gnome - and not (yet) apple nor windows. \nWhy is Gnome being so attractive for linux businesses ? Why are all the biggest linux players mainly focusing on gnome and putting their bucks there ?\nI am not a linux insider and my opinion is probably biased by unconsistent communication, but as far as I see and fear, desktop linux is slowly getting de-kde-ified.\n\nRegards,\nAlvise"
    author: "Alvise"
  - subject: "Re: KDE is starting to loose momentum"
    date: 2006-08-30
    body: "I totally agree, KDE will keep going as long as it stays KDE. If it tried to be 'gnomish', it would simply fail and Gnome would overtake its users (\"why use a copycat when we have the original?\"). KDE people use KDE because it is what it is. Simple as that."
    author: "b"
  - subject: "Re: KDE is starting to loose momentum"
    date: 2006-08-30
    body: "Agree completely! The gnome people are our friends!\n\nBesides, as long as KDE exists and is getting better all the time (and I'm sure KDE4 is going to seriously ROCK!), what the problem? I don't care the slightest what everyone else uses as long as I can use what I prefer."
    author: "Joergen Ramskov"
  - subject: "Re: KDE is starting to loose momentum"
    date: 2006-08-29
    body: "Isn't it Fedora who is losing momentum for the welware of Ubuntu? So it's just GNOME users shifting from one distro to the other. And many lately rising distros use and ship only KDE: PCLinuxOS, Mepis, Freespire."
    author: "Anonymous"
  - subject: "It's 'lose momentum'"
    date: 2006-08-29
    body: "Anyhow I love the image of kde moving so fast the we are suffering from 'loose momentum' - as we head towards moving at warp 9 our very moments of inertia are beginning to rattle.. :-)"
    author: "Richard Dale"
  - subject: "Re: It's 'lose momentum'"
    date: 2006-08-30
    body: "No matter what Internet forum I read, I find that the word \"lose\" is misspelled more often than not. Can someone please explain why? How hard can it be?"
    author: "Martin"
  - subject: "Re: It's 'lose momentum'"
    date: 2006-08-30
    body: "Obvosly verry hart."
    author: "cm"
  - subject: "Re: It's 'lose momentum'"
    date: 2006-08-30
    body: "This mistake has a very simple (and equally obvious) basis. It's the pronunciation. 'o' in lose is pronounced exaclty like 'oo' in loose, choose and many other words. So people spell it 'loose', because they feel there's something missing in 'lose' (oh sweet irony), or they wrongly see it as a past tense form. That's how undereducation works. Oh well."
    author: "bojster"
  - subject: "Re: It's 'lose momentum'"
    date: 2006-08-30
    body: "Actually that's how etymological spelling works. I always spell lose 'loose', because that way it makes most sense no matter whether it's also supposed to be right. It's not undereducation, it's just plain healthy reason.\n "
    author: "phonos"
  - subject: "Re: It's 'lose momentum'"
    date: 2006-08-30
    body: "English spelling is great that way: e.g. \"bow\" (part of a ship) and \"bow\" (used to fire an arrow) are prounounced differenty, but \"bow\" (ship again) and \"bough\" (part of a tree) are pronounced the same. \"Through\" does not rhyme with \"trough\" however, and neither of them rhymes with \"bough\"\n\nBasically English is an awful mish-mash of a language, with spelling rules from French, Saxon and Latin, which were not fully codified till the release of the first dictionary in the 1700s.\n\nThat first dictionary caused a lot of fuss too. For example until its publication no-one pronounced the \"h\" at the start of words such as \"hotel\", \"historic\", \"hour\", \"herb\" and so on, as this was the way things worked in French. An awful muddle ensued when people released those h's were there. \"Hour\" stayed as it was, but the 'h' in \"herb\" got pronouced (except in America, where the old pronounciation remained). \"Hotel\" and \"historic\" were even more muddled. To this day, half the presenters on TV pronounce the 'h' in \"historic\" and the other half don't. It can be \"a historic event\" or \"an historic occasion\" depending on how they feel on the day.\n\nIncidentally \"lose\" and \"loose\", for those non-English speakers, are pronounced differently, the first ends with a Z sound, the other ends in a hard S sound. \n\nIt's a wonderful language: I think I read somewhere that English speakers have one of the worst records for accidental misspellings in the world because of all this confusion :-/"
    author: "Bryan"
  - subject: "Re: It's 'lose momentum'"
    date: 2006-08-30
    body: "I read somewhere that english spelling was invented by non-native speaking Belgians around the time of Caxton, which might explain how horribly wrong it went."
    author: "Re: It's 'lose momentum'"
  - subject: "Re: It's 'lose momentum'"
    date: 2006-08-30
    body: "Not sure where you learned English.\n\nhttp://dictionary.reference.com/search?q=loose"
    author: "Ryan James"
  - subject: "Re: It's 'lose momentum'"
    date: 2006-08-30
    body: "Yea, once we get to warp 16 or so we'll go back in time. And Spock can talk to some whales at the Monterrey Bay Aquarium. It'll be awesome."
    author: "Ian Monroe"
  - subject: "Re: KDE is starting to loose momentum"
    date: 2006-08-29
    body: ">>Gnome is really increasing in usage, and let's face it: distros are helping out.\n \n >>I was one that didn't belived the default option for distros would make people choose gnome after KDE or vice-versa, but now I see every day some friends trying out Ubuntu or Fedora, and they don't even know what KDE is.\n\nWell, Fedora (RedHat) has always been a gnome-distribution. So no change there. Still, they also ship KDE. For some reason they can't afford not including KDE (there is also a strong kde community within the fedora community)\n\nLooking at ubuntu, you actually see the same thing happening. Ubuntu started as a pure Gnome-distribution, but in no time there was a KDE variant available (Kubuntu). Now kubuntu is so strong that canonical gave it the same status as ubuntu!!\n\nAlso in a lot of countries in Europe there was a Linux Starter Guide available. That magazine used Kubuntu as reference, not Ubuntu.\n\nSo, how is KDE loosing momentum?\n\nSpeaking of SUSE; SUSE does not ship with a default desktop, you can choose between kde and gnome during installation."
    author: "AC"
  - subject: "GNOME trolls or false friends of KDE?"
    date: 2006-08-29
    body: "I dunno. Really sad to see regularly such postings hurting KDE."
    author: "someone"
  - subject: "Re: GNOME trolls or false friends of KDE?"
    date: 2006-08-29
    body: "That wasn't my intention, I didn't plan anytime attack gnome, I belive our problem isn't them.\nIt's jsut that for some reason distros seems those days to like gnome more, work more on it, letting KDE aside. (Hapilly KDE community is showing to be strong and working well without major distro support).\n\nI don't have answers, I came here looking for them :)"
    author: "Iuri Fiedoruk"
  - subject: "Re: GNOME trolls or false friends of KDE?"
    date: 2006-08-29
    body: "> Hapilly KDE community is showing to be strong and working well without major distro support\n\nYou're wrong with that. KDE *has* major distro support: Ubuntu, SUSE, Mandriva, Xandros, Linspire...\n\nI don't understand what some people want: do you want the KDE community to become weaker so that companies have to increase their efforts (to a similar level some companies must give to GNOME)?"
    author: "Anonymous"
  - subject: "Re: GNOME trolls or false friends of KDE?"
    date: 2006-08-30
    body: "Maybe because distros hire a lot of developers for creating work as beagle, themes, f-spot, etc, so they can work full-time on the desktop?\nSure there are KDE developers hired too, sure a strong community can overdoo hired people.\n\nI DIDN'T SAID this was the end of the world!! People alwyas tend to start flaming even when it's not your intention to start a war, I just wanted to know if there was anything I could do to help out... :-("
    author: "Iuri Fiedoruk"
  - subject: "Re: KDE is starting to loose momentum"
    date: 2006-08-29
    body: "I'm a GNOME and Windows XP/Classic user and I, for one, welcome the move of a native QT4/GPL port for Windows. This allows QT and KDE applications as well as KDE itself to be more popular on Windows. I'm eager to replace OO.o with KOffice on Windows to name one example and Kalzium or whatever its called would also be very useful for me, on Windows.\n\nThe GPL vs LGPL issue is milked and IMO the arguments you raise are not wrong at all. If you want a good discussion on that I suggest you try better and certainly not include it in a main argument about popularity. Popularity is just that, popularity. It says nothing about quality and its the question wether you want to be popular, the implications, etcetera ad infinitum."
    author: "AC"
  - subject: "Re: KDE is starting to loose momentum"
    date: 2006-08-30
    body: "Adobe is using GTK for Flash: http://blogs.adobe.com/penguin.swf/2006/07/api_review.html"
    author: "Other AC"
  - subject: "Re: KDE is starting to loose momentum"
    date: 2006-08-30
    body: "which works fine in konqueror, so what's your point?"
    author: "AC"
  - subject: "Re: KDE is starting to loose momentum"
    date: 2006-08-30
    body: "\"Do the distros have the GPL in KDE so much and love the LGPL in gnome?\"\n\nThe licensing of Qt was always going to be problematic, no matter how many people wanted to wish it wasn't a problem."
    author: "ac"
  - subject: "Re: KDE is starting to loose momentum"
    date: 2006-08-30
    body: "trolltech still makes money with Qt, so apparently, the license is not that much of a problem.\nSpeaking of distros: all major distros ship with kde (either as default or as alternative), so no problem over there..."
    author: "AC"
  - subject: "Re: KDE is starting to loose momentum"
    date: 2006-08-30
    body: "\"trolltech still makes money with Qt, so apparently, the license is not that much of a problem\"\n\nA liberal redistribution license costs money.  That is the problem.  That is why Novell has settled on Gnome for their recommended desktop.\n\n\"Speaking of distros: all major distros ship with kde (either as default or as alternative), so no problem over there...\"\n\nAnd all major distros have E16 packages too.  So you might not like that Ubuntu, Fedora, and Novell's desktops are Gnome default, but even you have to deal with reality.\n\nThe funny thing is that KDE's future might rest on its ability to run great on Windows."
    author: "ac"
  - subject: "Re: KDE is starting to loose momentum"
    date: 2006-08-30
    body: "This is what informed users expects, and win32 developers would _love_.\n\nFor GNOME I'd say: \"The funny thing is that GNOME's future might rest on its ability to run great on .NET (being written in C#)\" ;)\n"
    author: "Jaroslaw Staniek"
  - subject: "Re: KDE is starting to loose momentum"
    date: 2006-08-30
    body: "It looks like all paths lead back to windows, and not surprising considering 95%+ of all desktops run it.\n\nOf course that begs the question, is your allegiance to KDE or Linux?"
    author: "ac"
  - subject: "Re: KDE is starting to loose momentum"
    date: 2006-08-30
    body: "Considering that the vast majority of development on windows will be done via .NET, the days of windows developers having a vastly superior programming environment in Qt/KDE are over."
    author: "ac"
  - subject: "Re: KDE is starting to loose momentum"
    date: 2006-08-30
    body: "And why do you think that?\n\nAbout Gnome \"vs\" KDE issue. Gnome got some popularity because Novell and Xgl. But now think about KDE4! Native port in Windows, Xgl, and many many other nice stuff (plasma, solid, phonon)..."
    author: "MasterMind"
  - subject: "Re: KDE is starting to loose momentum"
    date: 2006-08-31
    body: ">>That is why Novell has settled on Gnome for their recommended desktop.\n\nNo, they settled on Gnome because they bought a gnome company. It doesn't make sense to buy a company that uses somekind of technology and then flush it completely down the drain."
    author: "AC"
  - subject: "Re: KDE is starting to loose momentum"
    date: 2006-08-31
    body: ">>No, they settled on Gnome because they bought a gnome company. It doesn't make sense to buy a company that uses somekind of technology and then flush it completely down the drain.\n\nSo why did they buy Ximian in the first place?  Clearly KDE is(was) superior development technology.  Oh yeah, we already know the reason.  Novell would be insanse to have to rely on a toolkit in which they and their customers have to pay for non-GPL compatible work.\n\nSo the whole \"don't talk about it and it doesn't exist\" is ridiculous.  For years you could wish that Trolltech would get bought out and the license would be liberalized, but we're at the point now that it probably won't matter if that happens.  "
    author: "ac"
  - subject: "Re: KDE is starting to loose momentum"
    date: 2006-09-01
    body: ">>So why did they buy Ximian in the first place? \n\nWell, Novell wanted to gain ground on the linux market. They wanted to buy SUSE, but could not raise enough money. So they bought Ximian in stead.\nLater on, they could raise the money to buy SUSE ass well (thanks to ibm and others), so they went ahead.\n\n\n>>Clearly KDE is(was) superior development technology. \nYou can't buy kde, so what's your point?\n\n\n>>Novell would be insanse to have to rely on a toolkit in which they and their customers have to pay for non-GPL compatible work.\n\nYup, we all know it's a bad thing to charge your customers for license fees. That's why Microsoft Windows never got any momentum on the desktop market (oh wait, it actually does!!)\n\n>> For years you could wish that Trolltech would get bought out and the license would be liberalized, but we're at the point now that it probably won't matter if that happens. \n\nindeed.\nShame on trolltech for wanting to earn money on their product\nShame on trolltech for releasing Qt under the gpl, which is completely against what opensource stands for\nshame on trolltech for making it impossible to use gtk/wxwidgets/name-your-opensource-toolkit/etc under KDE\n\nHow could they be so silly, not realising that software development under linux should be completely free of charge.\n"
    author: "AC"
  - subject: "Re: KDE is starting to loose momentum"
    date: 2006-09-01
    body: ">>And all major distros have E16 packages too. So you might not like that Ubuntu, Fedora, and Novell's desktops are Gnome default, but even you have to deal with reality.\n\ndifference is that E16 is shipped for convenience (not on novell by the way), while kde is well integrated as an alternative.\nAs with novell's desktop, there is no difference between kde and gnome, and many companies choose kde over gnome on novell. Novell gives 7 years of support on kde-installations as well on gnome installations. When you install Novell, you can choose between kde and gnome. Both are equal.\n\nAs for ubuntu, kubuntu is wel integrated and gets more popular every day.\nMost ubuntu users come from redhat and debian, both are desktops that default to gnome since their beginning. So no new users over there, just a shift between gnome oriented distributions.\n\nLooking at your replies, i guess you are a jealous gnome user that can't stand that kde wins serveys everytime...\n\n"
    author: "AC"
  - subject: "Re: KDE is starting to lose momentum"
    date: 2006-08-30
    body: "Just two words: Internet Explorer\n\nAs long as the most used and hyped distributions of the moment choose Gnome by default, KDE will lose many users to Gnome. I'd bet some are using Gnome just because XGL works far better with Gnome at this time, also.\n\n\n\n"
    author: "Derek R."
  - subject: "Re: KDE is starting to lose momentum"
    date: 2006-08-30
    body: "I'm very much impressed by Xgl, but compiz was crashing very frequently when used with KDE. I DUMPED Xgl!"
    author: "Yogesh M"
  - subject: "It's a Matter of What You Count"
    date: 2006-08-30
    body: "Compare the DesktopLinux poll with the last LinuxQuestions poll.\n\nLQ says KDE 65%, Gnome 26%, XFCE 9%, GNUStep and XImian negligible.\n\nDesktopLinux says KDE 38%, Gnome 35%, XFCE 10%, other choices togeher 17%.\n\nThose other choices (17%) are not accounted for by the LQ survey. So maybe those 17% prefer KDE to Gnome if deprived of their favorite choice. Maybe they use KDE the desktop environment with another window manager instead of KWin - after all the DesktopLinux poll says \"windowing environments\", while LQ says \"desktop environments\", which explains why all those 17%-choices weren't available on LQ.\n\nSo while Gnome does indeed score higher in on poll than in the other, I don't see any reason to panic. \n\nOn the other hand perhaps this just yields the significant/interesting discovery that many people combine KDE with alternate window managers.\n\n"
    author: "mihnea"
  - subject: "Thunderbird ?"
    date: 2006-08-29
    body: "Why are so many people using Thunderbird ?\nWhile there are some webpages where konqueror has problems, so that there is some reason to prefer Firefox, kmail works perfectly for me since 1998 or something like this.\nSo, what did the Thunderbird team do to get so much attention ?\n\nAlex\n\nP.S. (k|X)ubuntu 30 percent, more than twice as much as the second place, that's more than impressive"
    author: "aleXXX"
  - subject: "Re: Thunderbird ?"
    date: 2006-08-29
    body: "Even though I'm a KDE fan and prefer using KDE apps whenever possible, I had to switch to Thunderbird due to poor IMAP support in Kmail. "
    author: "Elad"
  - subject: "Re: Thunderbird ?"
    date: 2006-08-29
    body: "Well, my move was in the opposite way. I got angered by thunderbird's temporal freezes and wiped it from my system. Kmail better suits my needs in the means of features and integration."
    author: "Non_E"
  - subject: "Re: Thunderbird ?"
    date: 2006-08-30
    body: "...and I was annoyed with all the KMail disabilities and so I dumped it in favour of Sylpheed. It may be less featured and I actualle miss some of the stuff I had in KMail, but it's far more stable, quicker and confortable to use.\n\nAs for the Thunderbird, I tried it once, but I was appaled with its Windows-like approach (1. users are morons, 2. they want the screen to be as cluttered as possible, it makes them feel professional, 3. everybody loves emoticons and other graphical intrusions all over the screen), so I never actually started using it."
    author: "b"
  - subject: "Re: Thunderbird ?"
    date: 2006-08-30
    body: "Same here, I just installed Kubuntu Dapper on my new laptop, upgraded to KDE 3.5.4 and thought I would try out Kontact/Kmail, however it sadly it seem to still have problems with imap :(\n\nI hope it will get fixed someday as I would like to use Kontact/Kmail."
    author: "Joergen Ramskov"
  - subject: "Re: Thunderbird ?"
    date: 2006-08-30
    body: "one strong point for thunderbird and ff: they're available on windows. so, using them on linux is less scary for newbies.\nyet anoither reason why I want kde4 to be ready yesterday ;)\nFF really gets on my nerves, but on windows I use it - anything's better than IE."
    author: "Chani"
  - subject: "Re: Thunderbird ?"
    date: 2006-09-03
    body: "> Why are so many people using Thunderbird ?\n\nBecause Thunderbird can filter junkmail without freezing:\nhttp://bugs.kde.org/show_bug.cgi?id=41514\n\nThe bug has 3909 votes right not, so it is probably not just me."
    author: "Erik"
  - subject: "ignore the gnome/ubuntu FUD"
    date: 2006-08-29
    body: "People who spam distrowatch are not representative of ALL linux users.\n\nFor e.g. - look at Ubuntu and fedora. Do you really think that ubuntu has that much of mindshare ? And did you believe that about fedora a couple of years back, when most people voted fedora ? I have been using linux for > 5 years and I have been to LUG meetings etc and I haven't yet met anyone using ubuntu personally. I think that ubuntu is out to market itself and so is gnome. \n\nMost distros still prefer KDE. Few ( if any) linux users use ubuntu. They just might not visit distrowatch. "
    author: "vm"
  - subject: "Re: ignore the gnome/ubuntu FUD"
    date: 2006-08-30
    body: "I haven't met anybody who doesn't use Ubuntu yet (in real life I mean not just on the forums). Using Kubuntu and promoting that version instead of Ubuntu but the ones that used ubuntu before I knew them, all use gnome. Ubuntu is having a big mindshare anyway, perhaps not with the oldies, but certainly with the newbies and they learn fast enough."
    author: "terracotta"
  - subject: "Re: ignore the gnome/ubuntu FUD"
    date: 2006-08-30
    body: "I know many Ubuntu users.  Both personally and over the \u0092net.  It\u0092s kinda like Debian on acid \u0097 APT/DPKG + ridiculously fresh packages; two reasons why people could like it :)\n\nFor one (at least a while ago when I still used it), Debian seems to have a Gnome preference.  And so did Ubuntu until the officialisation of Kubuntu.  I doubt you could say that Debian too isn\u0092t used by more than a few linux users."
    author: "Ralesk"
  - subject: "KDE presens strong in Debian"
    date: 2006-08-29
    body: "Debian is big distro and has strong KDE side. KDE packages are recent (at least in sid), there are lots of KDE/Qt apps in the repository, debian-kde mailing list is much more active than debian-gtk-gnome etc., so KDE is doing well at least here in the free world ;)"
    author: "petteri"
  - subject: "I don't know..."
    date: 2006-08-30
    body: "I'm still seeing the people who prefer KDE, running KDE.  I'm seeing the people who prefer Gnome running gnome.  The ones who don't know, run whatever the default is.\n\nDoesn't really matter which desktop you run-- Chances are, there's a software package you Really Need the Other Desktop for.  So the best bet for being able to install random application is to have both, and have both kept up to date, which will lead to people trying both.\n\nTruth is, every now and then, I fire up Gnome.  Then I try to extend my panel across both monitors, turn on window snap, and active borders between screens... \n\nAnd log out, and go back to KDE.\n\nAs for SuSE, I've got some 7.x CD's somewhere around here, and I've used it steadily since.  Gnome might be the default, but KDE is still well supported by the openSuSE team.  The only developments for Gnome that haven't made it to KDE (yet) are the \"Control Center\" and the new \"Slab\" menu from SLED.\n"
    author: "Anonymous"
  - subject: "Re: I don't know..."
    date: 2006-08-30
    body: "Well, openSUSE has no default - you get the option of Gnome, KDE or 'other' with none preselected, but SLED/SLES defaults to Gnome.\n\nThe 'Slab' type menu is being brought to KDE for openSUSE 10.2 - one of the SUSE guys (binneri perhaps? my spelling is probably way out, sorry) blogged that it will be in openSUSE 10.2 alpha 4 - don't know whether it is actually good, but I know a lot of gnome people who like slab."
    author: "Simon"
  - subject: "Re: I don't know..."
    date: 2006-08-30
    body: "Yeah, pretty much what I do too.  I always try to give a chance to the other desktop environments, but eventually I\u0092ll end up going back to KDE anyway.  Gnome might be relatively small and fast, but there are things I don\u0092t like in it, want to change them and there\u0092s just no way to \u0097 take \u0093open everything in a new file manager window\u0094; that\u0092s so Windows 95, even Microsoft have given up on it in 1998!\n\nBut at the same time, there are a handful of programs that just really kick the butts of their KDE counterparts.  KCharSelect sucks \u0097 I can\u0092t live without gucharmap, for example.  Buggy too, but much less unusable :)\n\nre Control Centre: We do have kcontrol for ages, and we do have a new \u0093System Settings\u0094 thing (at least here in Kubuntu Edgy) whose program name I never bothered to look up \u0097 I think they do a nice job at being a control centre."
    author: "Ralesk"
  - subject: "konqueror is the best"
    date: 2006-08-30
    body: "For me \"The\" Best thing about kde desktop is Konqueror. I don't know what i will do without it. I also like kate as i start using it more.\nI have not used its multimedia capability.\nThe Worst appl i ever used is kdevlop i hate it others love it.\nI use konqueror for my all browsing needs i find it very neat and clean.\nlong live konqueror ...\n\n\n"
    author: "konqueror"
  - subject: "Re: konqueror is the best"
    date: 2006-08-30
    body: "Funny, because for me it was my use of KDevelop that convinced me to start using KDE full time at home. I can't wait to see what improvements are made in KDevelop for KDE 4 (heck, even the changes in KDevelop 3.4 sound pretty exciting)."
    author: "Paul Eggleton"
  - subject: "Re: konqueror is the best"
    date: 2006-08-30
    body: "i agree! three cheers for konqueror!\n\n"
    author: "superstoned"
---
Ten days ago we got the <a href="http://dot.kde.org/1155935483/">first snapshot of KDE4</a>. If you already played a bit with it, now you can continue discovering more interesting things playing with the <a href="ftp://ftp.kde.org/pub/kde/unstable/apps/KDE4.x/graphics/okular-0.5.tar.bz2">unstable package</a> of <a href="http://www.okular.org/">Okular</a>, a universal document viewer for KDE4 based on the <a href="http://kpdf.kde.org">KPDF</a> code. *** <a href="http://www.desktoplinux.com/">Desktop Linux</a> has finished its annual <a href="http://www.desktoplinux.com/cgi-bin/survey/survey.cgi?view=archive&amp;id=0821200617613">desktop survey</a> with KDE as the most popular desktop once again.  *** Krusader released <a href="http://krusader.sourceforge.net/phpBB/viewtopic.php?t=1665">1.70.1</a> last month fixing a security issue. *** Finally congratulations to KDE e.V. president Eva Brucherseifer who <a href="http://www.kdedevelopers.org/node/2293">married the charming Matthias Welwarsky</a>.



<!--break-->
