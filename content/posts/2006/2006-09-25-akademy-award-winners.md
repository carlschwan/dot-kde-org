---
title: "aKademy Award Winners"
date:    2006-09-25
authors:
  - "ahorkan"
slug:    akademy-award-winners
comments:
  - subject: "amarok"
    date: 2006-09-25
    body: "It must have been hard to decide between amarok and krita. Although I really like krita, I think amarok should have been it. It should have made it last year in Malaga, too!\n\nAmarok is pretty much the only app that windows-users envy me for, it has already converted one of my friends. Unfortunately, it seems there will soon be a windows-version."
    author: "ben"
  - subject: "Re: amarok"
    date: 2006-09-25
    body: "<em>Amarok is pretty much the only app that windows-users envy me for, it has already converted one of my friends. Unfortunately, it seems there will soon be a windows-version.</em>\n\nThere are 2 schools of thought about this:\n1. If all the open source gems are also available on Windows, Windows users will never switch.\n\n2. If all the open source gems are also available on Windows, once Windows users are used to using them, it will be easier to convince them to make the last step.\n\nTime will tell."
    author: "Daniel \"Suslik\" D."
  - subject: "Re: amarok"
    date: 2006-09-25
    body: "Guess which school I am from :)\n\nSwitching operating systems is work. You have to invest a lot of time, for most people time better spent otherwise. This implies that you need a motivation sufficiently big to switch, which shrinks when both OSes have your apps.\n\nI do agree with your last sentence though."
    author: "ben"
  - subject: "Re: amarok"
    date: 2006-09-25
    body: "This is always tough to tell, and I think it changes for the person.  I know I watched my sister slowly switch to free stuff, and learn the things that were linux only on my computer, and then switch the nth time her Windows box needed to be reformatted and/or crash on her.\n\nOn the other side, my step-father is trying to decide whether he can switch, because he's tired of upgrading his home box every few years for minimal benefits, but cannot switch due to his music stuff online.  ( And Microsoft gains another monopoly somewhere that hurts every one )  Anyway, in the mean time, he's been trying to switch in the areas he can to open source to make his inevitable move easier."
    author: "Nicholas Robbins"
  - subject: "Re: amarok"
    date: 2008-01-23
    body: "I use both linux and windows.\nI like both. I think it's great to be able to have the same set of apps on both.\nPeople complain about IE only sites. The reason is not just the IE has problem, but also that it can restrict people to windows. I think people should be able to choose the operating system they like best, not the one which will give them all the good apps. So make windows, mac, and linux software available on all or at least most platforms. Isn't that what open source is about, using software how you want, not how makers dictate it to you? "
    author: "Johny"
  - subject: "Re: amarok"
    date: 2006-09-26
    body: "I think porting Amarok to Windows would also gain more developers for the project."
    author: "stripe4"
  - subject: "Re: amarok"
    date: 2006-09-27
    body: "Also think of freedb..."
    author: "gerd"
  - subject: "Re: amarok"
    date: 2006-10-01
    body: "> porting Amarok to Windows\n\nunfortunately porting Amarok to windows is not likely to help promote important standards like OGG.  \n\nAs with many of the suggested ports to windows there is some concern about if it will be possible for windows ports to do all the things the KDE versions can do.  It would be a terrible shame if people got the wrong idea about KDE.  \n\n> gain more developers for the project.\n\nit will definately gain many more users in need of support and maybe some of those could be turned into contributors but it often takes a skilled maintainer to manage and encourage/mentor people in the right way to become active contributors.  dont forget code is not the only form of contributions, documentation, marketing, usability, accessibility and translation can all be opportunities to put new users to work.  \n\n\n"
    author: "Alan Horkan"
  - subject: "Re: amarok"
    date: 2006-09-25
    body: "Krita's award is well-deserved in my opinion. It has the potentional of becoming the killer image editor."
    author: "Ian Monroe"
  - subject: "Re: amarok"
    date: 2006-09-26
    body: "I think Krita has the potential for sure, I've seen some screenshots of what it can do and I must say, I'm very impressed.  Unfortunately, when I tried it, I just couldn't figure out how to use it. Maybe it is because I'm too use to using Photoshop or maybe the usability could improve a bit.  Since I was able to figure out how to use photoshop, I think it could be a usability thing... Having said that, I bet once I end up figuring out how to use it, it will be the solution for photoshop on KDE."
    author: "Version4"
  - subject: "Re: amarok"
    date: 2006-09-26
    body: "What did you want to do that you couldn't figure out?"
    author: "Boudewijn Rempt"
  - subject: "Re: amarok"
    date: 2006-09-26
    body: "(Provided it's not something that we haven't implemented yet :-)"
    author: "Boudewijn Rempt"
  - subject: "Re: amarok"
    date: 2006-09-27
    body: "Boudewijn, I was recently trying to insert a few lines of text into a picture.  I wanted to set the text to be center aligned.  I couldn't figure out how to do it.  Is this a feature that's not there yet?  I couldn't find a \"text frame\" tool.  Maybe I'm just not finding it?  BTW, Congratulations!"
    author: "Louis"
  - subject: "Re: amarok"
    date: 2006-09-27
    body: "There's no text frame tool yet indeed :-). It is possible (in 1.5/1.6) by embedding a KWord text document with a single text frame in your layer stack -- but that doesn't work entirely smoothly and you need to know what you're doing. I'm right now working on making it possible to use a single text frame in 2.0 (and embedded svg and so on)."
    author: "Boudewijn Rempt"
  - subject: "Re: amarok"
    date: 2006-09-27
    body: "Awesome!  Thanks.  I know this isn't the place for such questions, but you opened the door and I couldn't resist :-).  I've already switched to using krita for my (not too intensive) image editing.  It keeps on getting better."
    author: "Louis"
  - subject: "Re: amarok"
    date: 2006-09-27
    body: "Hi Boudewijn.  The text frame tool will be handy when you get that implemented.  Another thing I'm trying to do is create a cirle and fill it with a gradient.  I haven't quite figured out ho to do that.  What I'm really trying to do is create a CD label.  I have a friend who took an existing CD label in jpeg format, broke it down in Photoshop, and gave me a quick tutorial on how to do it.  Of course I'd like to do it in Krita.\n\nEric....."
    author: "cirehawk"
  - subject: "Re: amarok"
    date: 2006-09-27
    body: "We haven't implemented fill-with-gradient as an option to the shape tools yet, but there's an easy work-around. Draw your circle. With the select-contguous tool click inside the circle -- the inside is now selected (you may need to play with the threshold setting to get rid of nasty anti-aliasing issues). Then simply use the gradient tool."
    author: "Boudewijn"
  - subject: "Re: amarok"
    date: 2006-09-27
    body: "Cool, I'll give that a try later when I get home."
    author: "cirehawk"
  - subject: "Re: amarok"
    date: 2006-09-26
    body: "I have to agree.\nI recently upgradaded to koffice 1.6 and krita is getting really good, now it's stable and works faster and better, but I still miss a lot of tools (see 1) and think there are some parts confusing or difficult, as adjusting brightness/contrast. It indeed have a bright future and I belive in two years or less, it will steal the throne from the gimp.\n\nIn the other hand amarok is one of the most mature open source programs ever! It's simple but full of features, I simply love it.\n\n1: being able to see selection size during selectioning (no only after releasing the mouse button), I also miss spray painting tool"
    author: "Iuri Fiedoruk"
  - subject: "Re: amarok"
    date: 2006-09-26
    body: "There _is_ a spray painting tool, but it isn't very good. The combobox in the top toolbar has airbrush, eraser, pencil and brush options. I really should revisit that bit of code, it's probably still 2003 in those files :-)."
    author: "Boudewijn Rempt"
  - subject: "Laurent Montel, mistery man"
    date: 2006-09-25
    body: "Someone from people behind kde should track down Laurent for an interview.  He constantly makes an insane number of commits and works on just about every part of KDE, but there is just about no information out there on who he is.  I for one, am curious."
    author: "Leo S"
  - subject: "Re: Laurent Montel, mistery man"
    date: 2006-09-26
    body: "> Someone [...] should track down Laurent for an interview.\n\nI agree.  He has no blog and it was very difficult to get any relevant link for him.  \n"
    author: "Alan Horkan"
  - subject: "Re: Laurent Montel, mistery man"
    date: 2006-09-26
    body: "AFAIK he has been asked for a \"People Behind KDE\" interview but declined the offer. I think he just wants to get work done, not being under the lights. I understand and respect this."
    author: "ervin"
  - subject: "Re: Laurent Montel, mistery man"
    date: 2006-09-26
    body: "There is a kspread-related interview:\n\nhttp://www.kde.org.uk/apps/kspread/interview.html\n\nIt also contains a few lines about him, but not much."
    author: "lippel"
  - subject: "Re: Laurent Montel, mistery man"
    date: 2006-09-27
    body: "> I think he just wants to get work done, not being under the lights. I understand and respect this.\n\nGiving him a big award probably doesn't help if he is determined to be private.  \nKDE and Mandriva should be very disappointed at a missed opportunity to promote the project.  I assume he does want KDE to succeed and he as an opportunity to do that and it would be a shame to miss it.  It should be possible to an article that promotes his work - without getting into his personal life - and maybe encourage contributors to help in areas he would most like to see more work being done.  Hopefully developers can explain other ways how he could see an interview as an opportunity and use it to his advantage.  \n\nI really hope the marketing team will approach him again and strongly encourage him to give an interview.  \n"
    author: "Alan Horkan"
  - subject: "Re: Laurent Montel, mistery man"
    date: 2006-09-27
    body: "Hi, \nI am very happy for this award.\nI want to thank guy which offert it.\n\nThanks to Staikos Computing and Linux New Media.\n\nI don't think that I am a mistery man :)\n\nI like KDE and I want just that it works very well\nthat's all.\n\nIf marketing team wants to make an interview, I will\nbe very happy.\n\nRegards."
    author: "Montel Laurent"
  - subject: "Re: Laurent Montel, mistery man"
    date: 2006-09-28
    body: "Great! I often wonder why I read so few about you and your ideas about Mandriva and KDE. Thanks a lot for your work and I have to say that KDE under Mandriva 2007 is brilliant!"
    author: "lezard"
  - subject: "OT: KDE.News Feed"
    date: 2006-09-25
    body: "Why is it that he feed for this site (http://www.kde.org/dotkdeorg.rdf) contains the titles of the artices and not the body as well?  I find it rather annyoning that I'm not able to read an article in my feed reader without browsing to the site."
    author: "ac"
  - subject: "Re: OT: KDE.News Feed"
    date: 2006-09-25
    body: "If your reader is Akregator, there is a workaround: right-click on \"KDE Dot News\", select \"Edit Feed...\" / \"Advanced\" / \"Load the full website when reading articles\".\n\nI agree though that having the post body (at least the first paragraph) in the RSS would be better."
    author: "Maarten ter Huurne"
  - subject: "Re: OT: KDE.News Feed"
    date: 2006-09-26
    body: "Oooh, excellent, this! Thanks for the tips, Maarten!"
    author: "A.C."
  - subject: "Re: OT: KDE.News Feed"
    date: 2006-09-26
    body: "<i>Why is it that he feed for this site contains the titles of the artices and not the body as well?</i>\n\nEvil \"them\" want you to read the clever comments (like this one) as well.That's why."
    author: "Daniel \"Suslik\" D."
  - subject: "Akademy Awards?"
    date: 2006-09-25
    body: "Were the awards presented by the Akademy of Desktop Environment Arts and Sciences?"
    author: "AC"
  - subject: "Yay, Boudewijn"
    date: 2006-09-25
    body: "One of the nicest people in the community and someone always willing to provide advice and information. He deserves an award just for being Boudewijn."
    author: "Jim Bublitz"
  - subject: "Re: Yay, Boudewijn"
    date: 2006-09-26
    body: "> He deserves an award just for being Boudewijn.\n\nI think being Boudewijn is its own reward ;)\nHe is a humble genius.  \n\n"
    author: "Alan Horkan"
  - subject: "Phones"
    date: 2006-09-26
    body: "Those were not 'razr' phones - they were ROKR E2 Linux+Qt+music phones!"
    author: "George Staikos"
  - subject: "Re: Phones"
    date: 2006-09-26
    body: "Qt, too! You should have heard my daughters when I told them about the phone. They're so jealous of their old dad :-).\n\nThanks George!"
    author: "Boudewijn Rempt"
---
Day two of <a href="http://akademy.kde.org/">aKademy</a> 2006 and the speakers conference was brought to a close with the <a href="http://akademy.kde.org/conference/talks/awards.php">aKademy Awards Ceremony</a>.  And the winners are: <a href="http://www.valdyas.org/fading/index.cgi" title="Boudewijn Rempt, Krita and KOffice developer">Boudewijn Rempt</a>, <a href="http://www.kdedevelopers.org/blog/531" title="Alexander Neundorf, KDE developer and CMake wizard">Alexander Neundorf</a>, and <a href="http://www.kde.org.uk/apps/kspread/interview.html" title="Laurent Montel KDE Commit champion">Laurent Montel</a>. Read on for more details.















<!--break-->
<div style="border: thin solid grey; float: right; margin: 1ex; padding: 1ex; width: 300px">
<a href="http://static.kdenews.org/danimo/akademyawards06.jpg"><img src="http://static.kdenews.org/danimo/akademyawards06_small.jpg" width="300" height="200" alt="aKademy award ceremony impressions" /></a><br />

</div>
<br />
<b>2006 aKademy Award for Best Application:</b> <a href="http://www.valdyas.org/fading/index.cgi" title="Boudewijn Rempt, Krita and KOffice developer">Boudewijn Rempt</a> for <a href="http://koffice.org/krita/" title="Krita Paint">Krita</a>.   
<p>
<b>2006 aKademy Award for Best for Best Non-Application:</b> <a href="http://www.kdedevelopers.org/blog/531" title="Alexander Neundorf, KDE developer and CMake wizard">Alexander Neundorf</a> for his work on <a href="http://www.cmake.org/" title="Cross Platform Make">CMake</a> in KDE4.  
<p>
<b>2006 aKademy Awards Jury's Award:</b> <a href="http://www.kde.org.uk/apps/kspread/interview.html">Laurent Montel</a> for KDE4 Commit Champion.  
<p>
Prizes included an iPod, sponsored by Ricoh, two Motorola ROKR E2 phones with Linux and Qt, sponsored by Staikos Computing, a years subscription to <a href="http://www.linux-magazine.com/" title="Linux Magazine">Linux Magzine</a> as well as the right to help decide next years winners.
<p>
Thanks were handed out to pretty much everyone for the part they all played in making aKademy a success.  aKademy local organiser Marcus Furlong received a standing ovation.










