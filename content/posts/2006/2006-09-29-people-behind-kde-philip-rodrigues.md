---
title: "People Behind KDE: Philip Rodrigues"
date:    2006-09-29
authors:
  - "tbaarda"
slug:    people-behind-kde-philip-rodrigues
comments:
  - subject: "Thanks !!!"
    date: 2006-09-29
    body: "I really wanted to thank Phil the work he does in KDE. He demonstrates yet again that you can be a first level citizen in the KDE world without having to be an elite coder."
    author: "Albert Astals Cid"
  - subject: "w000000000t!"
    date: 2006-09-30
    body: "for Phil.\nHad a chance to meet him in jolly ol' England and he's every bit as genuine and kind as he seems on IRC. \nI want to be just like him when I grow up! :-)"
    author: "illogic-al"
---
This week the <a href="http://people.kde.nl/">People Behind KDE</a> series is featuring <a href="http://people.kde.nl/philip.html">Philip Rodrigues</a>. He mostly is active as a documentation contributor, but he also does user support and some coding. Enjoy the interview with tonight's star.

<!--break-->
