---
title: "Distro Quickies: Tomahawk, QiLinux, Arabian and Kubuntu"
date:    2006-04-21
authors:
  - "jriddell"
slug:    distro-quickies-tomahawk-qilinux-arabian-and-kubuntu
comments:
  - subject: "Bah"
    date: 2006-04-22
    body: "Arabian Linux?   Why!?\n\nWhy not just have ubuntu and when you burn the dvd's simply set the default language to whichever country it is in.  If ubuntu doesn't have all the packages needed then that's a bug and fix ubuntu.\n\nIs there any need at all to make a new distro instead?\n"
    author: "John Tapsell"
  - subject: "Re: Bah"
    date: 2006-04-22
    body: "Ubuntu? Why!?\n\nWhy not just have Debian and when you burn the dvd's simply set the default language to whichever country it is in. If Debian doesn't have all the packages needed then that's a bug and fix Debian.\n\nIs there any need at all to make a new distro instead?\n\n\n... and yes I don't really want to start a distro war, just pointing out the obvious ;)"
    author: "petteri"
  - subject: "Re: Bah"
    date: 2006-04-23
    body: "Well, you have a very good point :)\n"
    author: "AC"
  - subject: "Re: Bah"
    date: 2006-04-22
    body: "I think you are right, more efforts should be made on integration of Arabic in the main distros rather than creating a new one."
    author: "Youssef"
  - subject: "Re: Bah"
    date: 2006-04-22
    body: "Why should not Arabs make business with arab distributions?\n\nIt is probably also of strategic importance for the arab hemisphere to run an own distribution."
    author: "And"
  - subject: "Re: Bah"
    date: 2006-04-22
    body: "Why?  Here's one reason.  Sometimes it's easier to work out all the issues within a focused dedicated team of people sharing the same goal than it is to be just a small insignificant part of a much larger project where it's much harder to make the changes you need to make.  Then once the problems are solved the larger projects can pick up on the solutions and mainstream them.\n\nJohn."
    author: "Odysseus"
  - subject: "Re: Bah"
    date: 2006-04-22
    body: "And the best thing about it is that with open source all their changes can be put into the larger distros!"
    author: "Jacob"
  - subject: "Re: Bah"
    date: 2006-04-23
    body: "\"Is there any need at all to make a new distro instead?\"\n\nYou should ask that question to Mark Shuttleworth"
    author: "AC"
  - subject: "Re: Bah"
    date: 2006-04-24
    body: "John Tapsell? Why!?\n\nWhy labour an old point when you could have just linked to one of the many other vocal commentators who've made this point again and again throughout open source history?\n\nBut to be more serious, you can't summarise the worth of the project from a kde-dot summary. Some people don't like joining huge projects, some people have a tight group that makes much faster progress than a larger effort. How do you know their work won't filter into Ubuntu? How do you know their efforts actually are equivalent to a localisation effort on Ubuntu?\n\nAnd anyway, you can't stop people doing their own thing. I thought you were a kde-devel so I'm a little surprised you don't understand how open source works. I wanted to get involved with KDE, so I've gradually worked myself into a niche. But with hindsight I know it would have been a lot easier and perhaps more satisfying to have started my own similar project. Even though I wouldn't have been able to help or amuse nearly as many people with my work.\n\nPerhaps this says something about large open source projects, but I don't think this can be fixed."
    author: "Max Howell"
  - subject: "Re: Bah"
    date: 2006-04-27
    body: "Well, I use Debian unstable installed from Kanotix. I don't know whether this is my (mis)configuration but I cannot open my arabic file correctly. Even ini konqueror the filename is rendered as ?????.doc. Opening with kword make some paragraphs that use pronounciation hints (that show the reader whether a word should be read \"kataba\" (means \"wrote\") or \"kutiba\" (means \"was written\")) rendered broken. Funnily, when I close kword, the save-or-discard confirmation dialog rendered the filename correctly.\n\nRemember that Arabian Linux devs recompile KDE from source. Maybe because it should be patched."
    author: "Reza"
  - subject: "Tested Kubuntu 6.06 LTS Beta"
    date: 2006-04-22
    body: "I downloaded 6.06beta and tested on a pretty high-end notebook (a Samsung). Almost Everything worked. DHCP didn't find an IP but a manual call in konsole did... no clue why it didn't work on boot.\nThere is a big bug in Espresso (the installer for the live-cd) which crashes while partitioning: https://launchpad.net/distros/ubuntu/+source/espresso/+bug/40555/ I hope this will be fixed soon...\n\nI really don't like the KWin-Theme... For flight6 it was a Vista-lookalike; while I think Vista is ugly (und so was flight6) this new one is even worse... Well, it doesn't look like Vista and that is *good* (I think KDE shouldn't look like Vista/Windows because it is not Vista/Windows). Well, with 3 mouseclicks Plastic (KDE 3.5-default) is running :-)\n\nI really think that Dapper will rock. I will test on my other machines on Monday, I hope the bugs I reported against Flight6 are fixed."
    author: "Carsten Niehaus"
  - subject: "Re: Tested Kubuntu 6.06 LTS Beta"
    date: 2006-04-22
    body: "dapper will most likely have another one in the final release, actually work is going on on this. do an apt-get dist-upgrade and choose the 'kubuntu icons' in the windowdecoration settings..."
    author: "superstoned"
  - subject: "Many questions regarding Kubuntu"
    date: 2006-04-22
    body: "Why is KDE Network manager not part of Kubuntu's default installation, when it is for Ubuntu? Are they not supposed to have feature parity?\n\nIf it is because of its newness, well, Kubuntu is shipping a pre-release version of CUPS, something far more critical that Network Manager.\n\nI currently have a non-profit with 20 KDE desktops running Suse 9.3. While Suse 9.3 may still have a bit of life left, we are beginning to look for alternatives and Kubuntu could be the ticket, assuming they fix the printing issues that many have reported (look at planetkde.org).\n\nHow serious is the promise of long-term support and what does exactly does it mean?\n\nDoes it mean only security issues will be fixed or will we also see bug-releases in KDE's stable series be backported?\n\nHow big is Kubuntu's QA/security/packaging team? How well established is their QA methodology for releasing security updates? Do they have sufficient hardware to test their updates on? How about automated testing?\n\nThe reason I ask is that when I first tried Kubuntu, I fell in love with it, but there were some broken packages that took forever and a day to fix. This, or any form of breakage on production systems, is not something that I can expect any of my clients to be able to accept.\n\nI have just moved and haven't had a chance to test the Kubuntu Dapper betas. Can someone who has comment on how thorough the graphical configuration tools are and how well they work?\n\nI have found Yast to have a lot of depth and to be extremely reliable, although I do prefer apt for package installation. So how well do Dapper's tools compare to Yast? Considering that Yast was Gpled a while ago, I wonder if anyone has given any thought to integrating into Kubuntu.\n\nI could potentially get some customers to provide financial support for a port of Yast if Kubuntu committed itself to it.\n\nSorry for the long past and thanks for all the work."
    author: "Marc"
  - subject: "Re: Many questions regarding Kubuntu"
    date: 2006-04-22
    body: "Hope this may answer many of your questions: http://img.osnews.com/story.php?news_id=14283"
    author: "AC"
  - subject: "Re: Many questions regarding Kubuntu"
    date: 2006-04-22
    body: "As far as I know, Yast is currently being ported to Debian, but far from being finished. ( see also http://yast4debian.alioth.debian.org/ )\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Many questions regarding Kubuntu"
    date: 2006-04-22
    body: "Yast is what I am missing on Debian."
    author: "And"
  - subject: "Re: Many questions regarding Kubuntu"
    date: 2006-04-23
    body: "KNetworkManager isn't in main because it's a new package and not yet been reviewed, it will be soon before final release.  network-manager isn't installed by default for Ubuntu.\n\nCUPS 1.2 is a pain but we have someone working on patches, see my recent post to kubuntu-devel mailing list if you want to help testing them.\n\nLong term support promise is very serious and means you can get a contract with Canonical for thee years of support rather than the normal 18 months.  Security updates will also be available for free for three years (five for server packages).\n\n\"Kubuntu's QA/security/packaging team\".  We're a community project and Dapper has been really exciting in the number of people we have had helping.  Security is handled by me and the Ubuntu security dude, I have all three supported architectures at my house to test on.\n\nThe live CD installer is still a bit rough, but should be good in time for final release.\n\nWhat broken packages did you find?\n\n\n"
    author: "Jonathan Riddell"
  - subject: "Re: Many questions regarding Kubuntu"
    date: 2006-04-23
    body: "KDE network manager will be in the final release.\n\nand about Yast, please contact Jonathan Riddell (jriddell a t ubuntu d o t com) or aks on #kubuntu, they might be interested."
    author: "superstoned"
  - subject: "SuSE 10.1 RC2 also released"
    date: 2006-04-22
    body: "SuSE has also released their 10.1 RC2: http://lists.opensuse.org/archive/opensuse-announce/2006-Apr/0006.html"
    author: "apokryphos"
  - subject: "Re: SuSE 10.1 RC2 also released"
    date: 2006-04-22
    body: "Even a half year old Suse 10 works better that Kubuntu!\nPrinting is total broken, I have to use Turboprint for my Canon Pixma4000.\nThis works only in Suse. Why? Dont know! Ask the Kubuntu people...\nI converted all my 2 installations back to Suse and all is working again.\n\nGreetz\nWolf\n\n"
    author: "wolf"
  - subject: "Re: SuSE 10.1 RC2 also released"
    date: 2006-04-23
    body: "My Pixma4000 works fine on (k)ubuntu. It was not easy but i did it lookin around at linuxprinting.org\n"
    author: "josel"
  - subject: "Re: SuSE 10.1 RC2 also released"
    date: 2006-04-23
    body: "workin' on the printer problem. probably already fixed (apt-get dist-upgrade) :D"
    author: "superstoned"
  - subject: "Kanotix & PCLinuxOS"
    date: 2006-04-23
    body: "There is a lot of talk about K/Ubuntu and I am not sure if it is worth it. Currently, I seriously don't see anything that makes it better than other distros. Suse 10.x is more reliable and robust, so is Kanotix and PCLinuxOS. There are many features that they have or work better but missing in K/Ubuntu or don't work as good. Every time there was a new release of K/Ubuntu, I burn a cd and end up disappointed and feel I wasted a cd for nothing. K/ubuntu has potential because of the financial backing, but that doesn't make it better. I applaud Kanotix & PCLinuxOS developers for a first class distro even though they only have limited resource.\n\n  Note: I am not a troll. I never used Windows at home. I have been using Suse for 5 years and Kanotix for a year. Occationally, I look into PCLinuxOS and other distros. By the way, the three above are mainly commited to KDE and Ubuntu is not. I think it is a mistake and may be it has to do with commercialization of Ubuntu in the future."
    author: "Abe"
  - subject: "Re: Kanotix & PCLinuxOS"
    date: 2006-04-24
    body: ">>Note: I am not a troll. I never used Windows at home.\n\nMy Dad always claimed he was not an alcoholic. \"I'm not an alcoholic, son\" he would say, as he smacked me back and forth and downed another 12 pack of Hamms. He died of cirrhosis at the age of 45, all the while claiming it was the pesticides on the trees where he worked (he was a mechanic and worked indoors.)\n\nMy advice to you is quite simple: If you feel like you have to post \"I am not a troll\", then you are trolling. There are a number of clinics where you can get together with other close-minded zealots, and you can talk openly with one another. Tuesdays are \"My desktop environment it better than yours\", followed by Friday night's \"My distro is better than yours\"..\n\nI use KDE on Ubuntu. Each to his own, eh? One size does not fit all. Also, Suse is dropping KDE as default.... You *did* know this, right?"
    author: "De-obfuscator"
  - subject: "Re: Kanotix & PCLinuxOS"
    date: 2006-04-25
    body: "Yes to each his own and what I was presenting is my opinion of Ubuntu and my discust with the big hupla being created about nothing.\n\n  No, I am not a troll and I don't care one way or the other because I am happy with what I have. Keep in mind though, that people who lie think everyone else are liers. So may be your are the troll after all.\n\n  May be Suse is dropping KDE as the default, but they still support it. Ubuntu hasn't been as supportive."
    author: "Abe"
  - subject: "Re: Kanotix & PCLinuxOS"
    date: 2006-04-27
    body: "I gotta agree. Ubuntu gets lots of \"free\" press, but it's not that great of a distro. Kanotix does far more out of the box. The last time I tried Ubuntu, it had all kinds of silly errors and missing packages that it took forever to get into a working, usable, system.\n\nUsable to me means that all movie formats work, for example, that two accounts can be logged into the GUI at the same time on the same machine without silly errors, etc.\n\nKubuntu probably isn't worth using until Ubuntu makes it standard. When one installs a GUI app from a Ubuntu repository, both menu systems (KDE and GNOME) should be updated, etc. It was silly for Ubuntu not to include both from the start.\n\nUbuntu still needs Debian repostories to make it in the real world, which, IMO, defeats the whole purpose. Kanotix uses real Debian repos, and I'm rather up to date, recent KDE, xorg, etc. What was the point of Ubuntu getting their own repos agan??\n\nKanotix has a lush KDE setup, the perfect combination of speed and features. The right-click menus are stocked full of handy items - extract here for archives, etc., and it's FAST, even on older systems - the fastest KDE distro I've ever used. They must have cut something, but I can't find anything missing...\n\nI so often hear, in the outside, non-linux world, ubuntu mentioned as a major ditro. For example they will mention Mac, Windows and Ubuntu...Why, lol? No offense to anyone, but nothing makes Ubuntu special. It's not the best Desktop distro, it's not the best newbie distro, it's not the best live distro, it's not the best business distro, it's not the best hacker/do it yourself distro (though sometimes I think it tries). It doesn't even make 4th or 5th place in any of those categories...\n\nIf you want a robust business distro, use SUSE. If you want a damn-good \"it's in there\" live distro, use Kanotix. Kanotix also makes a damn-fine liveCD, with unionfs and klik, you can add many packages to your live session. You can get flash and other non-free stuff NEEDED for a modern browsing experience without BS which is why I found it amusing that there is a vmware saved state machine for ubuntu, of all distros, available for download! I haven't tried it yet, but it's supposed to be for use as a browsing system - but I wonder if it comes with flash, java, the ability to view quicktime movies, etc. If not, who would use it to browse?\n\nfred"
    author: "fred"
  - subject: "Re: Kanotix & PCLinuxOS"
    date: 2006-04-27
    body: "+1 to Kanotix.\n\nIt's just it sometimes break when dist-upgraded to Debian. Oh, well."
    author: "Reza"
  - subject: "Re: Kanotix & PCLinuxOS"
    date: 2006-04-29
    body: ">Currently, I seriously don't see anything that makes it better than other distros.\n\nThe one thing that makes K/Ubuntu better than SUSE above all else is the 17.000+ packages that are easily installable with apt-get.  There's no hassle with trying to find additional repositories, or hunting for an rpm for your distro.\n\n"
    author: "Yeah Right"
  - subject: "Re: Kanotix & PCLinuxOS"
    date: 2006-05-02
    body: "As if Suse doesn't have all those same 17,000 apps! Suse has YaST which is as user friendly as APT. Kanotix & PCLinuxOS also use Debian repositories and use Kpackage which is even simpler and more user friendly than apt-get. Your point has no merits. In my opinion, the only thing K/Ubutnu is good for is the finnacial resources \"Shuttleworth\" has available. Other than that, it still has ways to get to the level of Kanotix, PCLinuxOS & Suse. I guess this is enough debating. To each his own. When K/Ubuntu gets to par with others, then I might think about using it, but for now, it is not good enough, plain and simple."
    author: "Abe"
  - subject: "um hello"
    date: 2006-06-02
    body: "couple of points:\n\n1) If you think Yast is as user friendly as synaptic with apt, you're smoking crack\n\nand\n\n2) PCLinuxOS does NOT use Debian repositories. It's an RPM BASED DISTRO (which started out as a fork of Mandrake to be exact)which uses SYNAPTIC.\n\nFact checking before venting is always a plus :)"
    author: "Only_the_Truth"
  - subject: "Re: Kanotix & PCLinuxOS"
    date: 2006-05-23
    body: "I have played with a ton of distros.  Kubuntu would not install,Simply Mepis is way too simple, PC Linux had a wonderful sharpness to it but I really fell for Kanotix.  I have it running on several pc's and a laptop perfectly.  Now and then I stray and play with a few other debian distros but nothing has even come close to the speed and ease of use.  I will say however that it is rather ugly out of the box. "
    author: "brian"
---
A few of the KDE distributions have been sending us their annoucements.  <a href="http://www.tomahawkcomputers.com/">Tomahawk Desktop</a> 1.1 has been released.  "<em>Tomahawk Desktop is an advanced multimedia centric KDE desktop</em>". *** <a href="http://www.qilinux.org">QiLinux</a> 2.0rc1 free edition was released with KDE 3.5.2, "<em>QiLinux is a KDE-centric distribution for desktop and server made completely from scratch</em>". *** <a href="http://www.arabian.arabicos.com/html/">Arabian Linux</a> has released <a href="http://en.arabian-linux.org/wiki/index.php/Arabian-Linux-v0.6">version 0.6</a>, "<em>It's the first Arabic live distribution using KDE as the default GUI and the first to have the Arabic language enabled in consoles</em>". *** Finally <a href="http://kubuntu.org/announcements/dapper-beta.php">Kubuntu 6.06 LTS Beta</a> was announced with the promise of Long Term Support.

<!--break-->
