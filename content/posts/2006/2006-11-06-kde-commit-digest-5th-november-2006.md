---
title: "KDE Commit-Digest for 5th November 2006"
date:    2006-11-06
authors:
  - "dallen"
slug:    kde-commit-digest-5th-november-2006
comments:
  - subject: "Finally !"
    date: 2006-11-06
    body: "Finally, it's 1:39 am here and I can read the commit digest and then go to sleep\n\nGreat job, Danny ! Thanks a lot !"
    author: "Marc Cramdal"
  - subject: "Re: Finally !"
    date: 2006-11-06
    body: "I would like to second that. Thanks a lot Danny for the weekly joy! Your work is very much appreciated!"
    author: "MK"
  - subject: "New KDE Games"
    date: 2006-11-06
    body: "I'd like to congratulate the artists for a great job on the new games.  even unfinished, KATomic looks beautiful, and KReversi is nice and shiny.  Great job, guys!"
    author: "bkudria"
  - subject: "Re: New KDE Games"
    date: 2006-11-06
    body: "Thanks :). Dev that make that possible (porting to svg) are also to congratulate.\n\nMore infos, on Katomic, black wall are finished now (the screenshoot is outdated), and atoms are still to be finished. And Kreversi need a background ;)\n\nI'll post some stuffs in the futur on http://johann.pwsp.net/  and our TODO list is here:\nhttp://wiki.kde.org/tiki-index.php?page=KDE+Games+SVG+status\n\n"
    author: "Johann Ollivier Lapeyre"
  - subject: "Re: New KDE Games"
    date: 2006-11-06
    body: "Not exactly a game, but I'd love to see someone carry on development of KQF.  Its an online FPS server browser, and a KDE based alternative to XQF.  With Warsow, and a few other REALLY good free FPS games being developed, a KDE server browser would be very appreciated.\n\nI think the code lives under playground of the kde svn."
    author: "Greg Loscombe"
  - subject: "Re: New KDE Games"
    date: 2006-11-07
    body: "I also like the new graphics, but I think the shadows under those green arrows in katomic should be removed.\n\nThanks for your great work!"
    author: "ben"
  - subject: "Re: New KDE Games"
    date: 2006-11-07
    body: "the shawows will be removed, i always thought that. but i didn't had the time, because i had to finish the wall, atoms... now look at:\nhttp://johann.pwsp.net\n"
    author: "Johann Ollivier Lapeyre"
  - subject: "Star tool"
    date: 2006-11-06
    body: "Well, actually it's Karbon, or to be really precise, flake, that gets the star tool. I'm still working on porting Krita's tools to flake. It's taking a bit of time, but it's also quite a big job."
    author: "Boudewijn Rempt"
  - subject: "Re: Star tool"
    date: 2006-11-06
    body: "> Well, actually it's Karbon, or to be really precise, flake, that gets the star\n> tool. I'm still working on porting Krita's tools to flake. It's taking a bit of \n> time, but it's also quite a big job.\n\nI am relieved to hear this:-) The star tool was the first thing I ever contributed to Krita (or KDE in general) and I would be mournful if it goes away. But from you comment, I guess someday it'll go away..."
    author: "Michael"
  - subject: "Re: Star tool"
    date: 2006-11-06
    body: "Nah, my kids love it way too much. (And so do I...) I'm just working really hard to make all Krita tools use the KOffice standard tool code."
    author: "Boudewijn Rempt"
  - subject: "Amazing"
    date: 2006-11-06
    body: "I just want to let you know how amazing good all the artwork looks to me! Oxygen quality is impressive. KDE is allready the best engineered desktop, and on top of that, KDE4 will hopefully be the best looking. Looks schouldn't matter, but with desktops it does."
    author: "Cossidhon"
  - subject: "Mailody"
    date: 2006-11-06
    body: "I think Mailody is pretty cool, there really is a need for a mail client focusing more on usuability. Your way of handling the adresses in the mail composer is imho very origional and I think good usuability. \n\nBut wouldn't it be a good idee to create a ui on top of akonadi to avoid duplication of work, or are you already planning that?"
    author: "au"
  - subject: "Re: Mailody"
    date: 2006-11-06
    body: "IMHU akonadi is the data storage layer, it doesn't contain an email client."
    author: "Gato"
  - subject: "Re: Mailody"
    date: 2006-11-06
    body: "Who is talking about akonadi?"
    author: "NabLa"
  - subject: "Re: Mailody"
    date: 2006-11-06
    body: "The post he's replying to."
    author: "Narishma"
  - subject: "Re: Mailody"
    date: 2006-11-06
    body: "Still, wouldn't that be a good idea? Using akonadi for storage, that is."
    author: "Martin"
  - subject: "Re: Mailody"
    date: 2006-11-06
    body: "(This may not be needed as long as it is IMAP only, of course.)"
    author: "Martin"
  - subject: "Re: Mailody"
    date: 2006-11-06
    body: "On the other hand it uses KAdressbook, which will be exposed as an akonadi data source. Also IMAP may be cached (disconnected). So there's plenty of room for akonadi, but on the other hand KDE 4 is still one year ahead."
    author: "Gato"
  - subject: "Re: Mailody"
    date: 2006-11-06
    body: "What I am generally wondering about is the motivation for Mailody. I searched its homepage but couldn't find it. I guess there must be one as Kontact/KMail exists for such a long time and is a mature app.\n\nI don't want to say \"don't do that because KMail [insert your reason here]\" but it would be still interesting to know what the difference in their goals is.\n"
    author: "Arnomane"
  - subject: "Re: Mailody"
    date: 2006-11-06
    body: "Maybe for the same reason I wrote Ascension (my email client) - just because it was fun. Well, there were other reasons for me, including not really liking any other mail client out there, but there's no way I would have done it if it wasn't going to be fun. Mine's languished somewhat, as I've had little time to work on it in the last couple of years, but I'm planning on installing the KDE4 preview and trying to port it."
    author: "Psiren"
  - subject: "Re: Mailody"
    date: 2006-11-06
    body: "Link? (To Ascension.)"
    author: "Martin"
  - subject: "Re: Mailody"
    date: 2006-11-06
    body: "http://www.hibernaculum.net/ascension\n\nThat version is the only one released. My current copy in CVS is considerably more usable (I've been running it for months with no problems), but of course it lacks some release polish. If anyone is actually interested I'll update the site with the latest CVS snapshot when I get home."
    author: "Psiren"
  - subject: "Re: Mailody"
    date: 2006-11-07
    body: "I'd be interested."
    author: "Eike Hein"
  - subject: "Re: Mailody"
    date: 2006-11-07
    body: "http://www.hibernaculum.net/download/ascension-snapshot.tar.gz\n\nI've not updated the web page yet, but you can get it from the above link. It's changed considerably since the 0.3 release, so you may have to wipe your configuration and start again. All the usual warnings apply of course :)"
    author: "Psiren"
  - subject: "Re: Mailody"
    date: 2006-11-06
    body: "This has been discussed in the previous \"Commit-Digest\".\nExplanations are on the author's blog:\nhttp://www.omat.nl/drupal/?q=node/98\n\nIn short, it says KMail code is so badly written that it was very hard to add the features he wanted."
    author: "Hobbes"
  - subject: "Re: Mailody"
    date: 2006-11-06
    body: "That is not exactly true. Extracted from the blog entry:\n\n\"Probably my lack of thorough c++ knowledge. But it prevents me to start implementing the things I need in KMail.\"\n\nCheers"
    author: "Damnshock"
  - subject: "Kontact?"
    date: 2006-11-06
    body: "Could Mailody be integrated with Kontact, so the user has a choice of which email component to use within Kontact?"
    author: "Martin"
  - subject: "css3 opacity in khtml"
    date: 2006-11-06
    body: "Sweet :) That one is really usefull for webdev."
    author: "Moltonel"
  - subject: "Re: css3 opacity in khtml"
    date: 2006-11-06
    body: "What do you think the KDE4's Kopete style will use? ;)"
    author: "Johann Ollivier Lapeyre"
  - subject: "Opacity backport"
    date: 2006-11-06
    body: "Will be opacity backported to 3.5? Would be nice."
    author: "m."
  - subject: "Re: Opacity backport"
    date: 2006-11-07
    body: "No, it would be difficult to do with Qt 3, and probably too slow anyway.\n"
    author: "germain"
---
In <a href="http://commit-digest.org/issues/2006-11-05/">this week's KDE Commit-Digest</a>: Work on porting kdegames applications to SVG and other general improvements continues at a fast pace. Work continues on video support in <a href="http://kphotoalbum.org/">KPhotoAlbum</a>. <a href="http://www.koffice.org/krita/">Krita</a> gets a new star shape tool. <a href="http://okular.org/">Okular</a> gets support for freehand ink overlays in presentation mode. <a href="http://kate-editor.org/">Kate</a> gets syntax highlighting support for <a href="http://en.wikipedia.org/wiki/Actionscript">ActionScript</a> and <a href="http://en.wikipedia.org/wiki/RapidQ">RapidQ</a> code. <a href="http://www.mailody.net/">Mailody</a> continues to mature as an alternative email client. <a href="http://www.vandenoever.info/software/strigi/">Strigi</a> refactors to allow searching within multiple simultaneous indexes, with preliminary interoperability with <a href="http://pim.kde.org/akonadi/">Akonadi</a> on the horizon.
<!--break-->
