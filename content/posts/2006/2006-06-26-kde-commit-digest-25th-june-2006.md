---
title: "KDE Commit-Digest for 25th June 2006"
date:    2006-06-26
authors:
  - "dallen"
slug:    kde-commit-digest-25th-june-2006
comments:
  - subject: "Good work!"
    date: 2006-06-26
    body: "Having a \"GMail-style conversation view for KMail\" sounds really nice. Once it's finished, I look forward to trying it out someday.\n\nAlso, I'm glad that the \"align-to-grid option\" regresssion for desktop icons has been fixed. I had noticed that.\n\nThe \"new fast image preview mode\" in Gwenview sounds really nice. If Gwenview adds an easy way to increase brightness it will probably make a great replacement for Kuickshow.\n\nThanks and congrats to all the hard working KDE developers.\n"
    author: "Elijah Lofgren"
  - subject: "Re: Good work!"
    date: 2006-06-26
    body: "> The \"new fast image preview mode\" in Gwenview sounds really nice.\n\nWell actually it was digikam which added this feature :-)"
    author: "infopipe"
  - subject: "Re: Good work!"
    date: 2006-06-28
    body: "Well, what do you know? :)\nThanks for point that out. I guess I hadn't searched the menus hard enough.\n\nIncreasing brightness in Gwenview can be done with either \"View -> Colors -> Increase Brightness\" or with \"Control + B\".\n\nIn Kuickshow it could be done with \"Right Click -> Brightness -> More Brightness\" or with \"B\"\n\nGwenview rocks, and I'm happy. :)"
    author: "Elijah Lofgren"
  - subject: "Re: Good work!"
    date: 2006-06-26
    body: "> If Gwenview adds an easy way to increase brightness\n\nAnd it actually has a way to increase brightness :).\n"
    author: "Lubos Lunak"
  - subject: "David Faure == hero"
    date: 2006-06-26
    body: "\"Rename ksycoca to ksycoca4, and rename kdeinit's socket to kdeinit4_<display>.\nThis should make it possible to run kde4 and kde3 apps side by side for the same user (ignoring each other, of course).\"\n\nWoohoo!  Thanks, David :)"
    author: "LMCBoy"
  - subject: "last.fm stats already there"
    date: 2006-06-26
    body: "you mean:\nThe beginnings of LastFM _radio_ support for Amarok\n"
    author: "Nick"
  - subject: "Re: last.fm stats already there"
    date: 2006-06-26
    body: "Indeed."
    author: "Mark Kretschmann"
  - subject: "Krecipes!"
    date: 2006-06-27
    body: "I use krecipes rather often. I just wish the bug reporting tool was in KDE bugzilla so it'd be easier to file wishes and bugs. On a side note, I'm glad amarok reverted to the old style. The top bar didn't appeal to me."
    author: "sundher"
---
In <a href="http://commit-digest.org/issues/2006-06-25/">this week's KDE Commit-Digest</a>: Support for the <a href="http://en.wikipedia.org/wiki/Encapsulated_postscript">Encapsulated PostScript</a> format in KViewShell. Important progress in <a href="http://www.vandenoever.info/software/strigi/">Strigi</a> desktop search. The beginnings of <a href="http://last.fm/">LastFM</a> support for <a href="http://amarok.kde.org/">Amarok</a>, whilst the experimental interface layout is reverted - for the time being, at least.
Import into KDE SVN of the "GMail-style conversation view for KMail" project, with breakthrough progress in the "OSCAR (AIM) File Transfer" project, both for the <a href="http://developer.kde.org/summerofcode/soc2006.html">Summer Of Code</a>. Ingredient substitution functionality in <a href="http://krecipes.sourceforge.net/">KRecipes</a>.

<!--break-->
