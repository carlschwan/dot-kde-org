---
title: "People Behind KDE: Marco Gulino"
date:    2006-03-27
authors:
  - "jriddell"
slug:    people-behind-kde-marco-gulino
comments:
  - subject: "after 3.43 seconds....."
    date: 2006-03-26
    body: "at little too accurate, or has it any special meaning, like the current version of SimpleMEPIS? (Shouldn't be if you're more a Gentoo or Slackware user...)"
    author: "boemer"
  - subject: "Re: after 3.43 seconds....."
    date: 2006-03-27
    body: "i think he meant 3 minutes and 43 seconds, not 3 point 43 seconds."
    author: "jamie"
  - subject: "Thanks Marco"
    date: 2006-03-27
    body: "for KMobiletools, and amarok sidebar. I appreciate your work!"
    author: "fast_rizwaan"
  - subject: "Long live Rockman!"
    date: 2006-03-27
    body: "It's nice to finally see you here! :-) I'm looking forward to use the suite after changing my half-decade old green phosphorous cell phone!\n(Btw, it looks like University of Padua is really a KDE hacker incubator, since your first contact happened there, as long as mine and Kget2's main hacker's).\nBreak your leg! (In bocca al lupo!)"
    author: "eros"
  - subject: "kopete file transfer"
    date: 2006-03-27
    body: "While kopete:/ filesystem sounds interesting, much more useful would be to make the file transfer actually work. For instance the oscar/icq file transfer doesn't work at all ATM :-(\n"
    author: "Michal"
  - subject: "Re: kopete file transfer"
    date: 2006-03-27
    body: "Did you post a bug report? If this is your itch, go scratch it! I'm sure the kopete developers would welcome your help in implementing file transfer for oscar/ICQ."
    author: "Philip Rodrigues"
  - subject: "Re: kopete file transfer"
    date: 2006-03-27
    body: "That's \"normal\", OSCAR(thus ICQ and AIM) file transfer are not implemented yet. And for that, help is always welcome."
    author: "Micha\u00ebl Larouche"
  - subject: "Hey Marco ... be aware "
    date: 2006-03-28
    body: "that the KDE eV can help developers with travelling/ accomodation costs they should have.  "
    author: "fab"
  - subject: "Hey Marco, be aware "
    date: 2006-03-28
    body: "that the KDE eV can help developers with travelling/ accomodation costs they should have.  \n\nI am reffering to this part of the interview\n\nMG: Are you coming to akademy 2006 this year?\nPBK: Unfortunately I am a no-money student.\n\nAnyway ... thanks for the cool KMobiletools!!\n\nTake care'\n\nFab"
    author: "fab"
  - subject: "Re: Hey Marco, be aware "
    date: 2006-03-28
    body: "Thanks!\nProbably i'll try submitting a talk, as suggested by Jonathan and Daniel in chat.. hoping that my english will be better in september :P\n\np.s.: 3.43 seconds was only a semi-random number :P"
    author: "RockMan"
---
Today on <a href="http://people.kde.nl">People Behind KDE</a> we introduce you to <a href="http://people.kde.nl/marco.html">Marco Gulino</a>.  This man is the author of <a href="http://kmobiletools.berlios.de/">KMobileTools</a> and the all important Konqueror sidebar for amaroK.  We also meet his intelligent dog Ricky and learn about the beauty of Sicily.  Enjoy the interview.


<!--break-->
