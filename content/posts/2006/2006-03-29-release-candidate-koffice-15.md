---
title: "Release Candidate of KOffice 1.5"
date:    2006-03-29
authors:
  - "iwallin"
slug:    release-candidate-koffice-15
comments:
  - subject: "Debian packages already available"
    date: 2006-03-29
    body: "I've just uploaded Debian packages to unstable.\n\nThey should be available tonight, in the meantime you can get them from:\nhttp://people.debian.org/~isaac/koffice/\n\nI'll try to get packages for Sarge ASAP to have up-to-date kliks.\n\nBest regards :)"
    author: "Isaac Clerencia"
  - subject: "Re: Debian packages already available"
    date: 2006-03-29
    body: "Great, great, great!\n\nI'll do my best to have kliks available half an hour after you announced your Sarge packages then....  :-)\n\nCheers, Kurt \n [  and thanks for all your hard packaging work -- making the kliks\n    on that bases is really easy now, and much less of a job ]"
    author: "Kurt Pfeifle"
  - subject: "Re: Debian packages already available"
    date: 2006-03-29
    body: "Oh, and I forgot to ask one thing: what about the \"MS Access Import Driver\", described here:\n\nhttp://www.kexi-project.org/wiki/wikiview/index.php?MDBDriver#2._Download\n\nWould it be possible to provide *.deb packages for this too? (I must be blind if they are already out there, somewhere; I didn't find any)."
    author: "Kurt Pfeifle"
  - subject: "Re: Debian packages already available"
    date: 2006-03-30
    body: "I'll give them a try after I finish with Sarge packages."
    author: "Isaac Clerencia"
  - subject: "Re: Debian packages already available"
    date: 2006-03-30
    body: "Yes! Please do... the mdb drivers would be great to have!"
    author: "ealm"
  - subject: "1.5's use of fonts"
    date: 2006-03-29
    body: "Hats off to all of the Koffice developers for moving us closer to what I'm sure will be a great release.  There is just one thing keeping me from using Koffice (mostly Kword) on a daily basis instead of OpenOffice.  For some reason or another whenever I use Kword it always seems to leave an irregular amount of space between letters.  Sometimes letters are mashed together without enough space between them and other times there is a noticeable gap between letters.  I'm sure I'm not using the proper nomenclature to explain this, but surely someone knows what I am talking about.  Are we to expect any changes with respect to this issue in this release?  If not, are there plans to address this in the future.  I really would like to use Kword instead of OpenOffice Writer on a daily basis, but I do a lot of printing and this issue comes up a lot for me.  In any event, thanks again for all the hard work!\n\nAdam York"
    author: "Adam York"
  - subject: "Re: 1.5's use of fonts"
    date: 2006-03-29
    body: "Hmmm.  Wasn't that planned to be fixed?  I can't check right now, but I thought it would was being worked on.  Ohh, I think it might be something that requires Qt4, so you'll see it fixed in KDE4."
    author: "Lee"
  - subject: "Re: 1.5's use of fonts"
    date: 2006-03-29
    body: "I read a very very long time ago, that there was some flag in XFree86 which if set at compile time would include a good font-kerning in it, but because of some licensing problems it was off by default. Then I read something about Trolltech telling they would include real font-kerning in Qt4. \n\nI Hope it will get better, because that is something I think Linux still lacks, it has become a little better with those anti-aliasing, but still I have the idea that under Windows or MacOSX the fonts look generally better...\n\nDon't come with well those fonts are better, because I'm just importing the windows fonts, and it still looks better under Windows..."
    author: "boemer"
  - subject: "Re: 1.5's use of fonts"
    date: 2006-03-29
    body: "Hmmm.  Well I guess KDE4 with Qt4 won't be that far away.  I'm really looking forward to fonts looking better in Koffice."
    author: "Adam York"
  - subject: "Re: 1.5's use of fonts"
    date: 2006-03-29
    body: "To make your post a bit more precise ;-)\n\nProfessional TrueType fonts (like those from Microsoft or Adobe) usually contain additional information on how to optimize the look of the\nletters at various font-sizes (this is called 'hinting'). This additional information comes inside the font files in form of a small application (bytecode). Some but not all commands of this bytecode are patented. Thats why FreeType (the free font renderer usually used on Linux) is usually compiled with support for these commands turned off in many distributions to avoid\nlegal repercussions.\n\nNot all fonts use these commands but most professional ones do. So if FreeType encounters such a command it can skip this command (but this usually yields unusable, ugly results) or totally ignore the complete hinting information including the known commands. This is what usually happens. To prevent the fonts from looking extremely awful, FreeType can \"invent\" its own hinting code automatically (called auto-hinting) which is not as good as the real deal but much better than no hinting at all.\n\nSo, if you have problems with fonts you might want to try recompiling\nFreeType with patented bytecode support turned on. This means removing\nthe \"/*\" and \"*/\" from this line in the file ftoption.h\n\n/* #define TT_CONFIG_OPTION_BYTECODE_INTERPRETER */\n\nfrom the source-code of FreeType and recompiling the whole thing.\nDescribed in detail here (just googled it for Mandrake):\nhttp://convexhull.com/mandrake_fonts.html\n\nSearch for TT_CONFIG_OPTION_BYTECODE_INTERPRETER and your distro on\nthe web if you want to know more.\n\nCaveat emptor: This will of course only improve TrueType fonts and only\nthose which use these commands (like Arial, Tahoma, Verdana). Type-1 fonts do not contain such information at all so there's nothing to improve there.\nUsually this makes only sense for screen fonts. On the printer resolution\nis high enough for hinting to be mostly irrelevant. Decide for yourself if you want to go through all those hassles. Modern open-source fonts (like DejaVu) yield results which are at least equivalent if not better than for exampel Microsoft Tahoma which has been specially designed for screen legibility.\n\n\n\n\n\n\n\n\n\n\n"
    author: "Martin"
  - subject: "Re: 1.5's use of fonts"
    date: 2006-03-30
    body: "Thanks Martin.  I'll certainly try this out.\n\nAdam"
    author: "Adam York"
  - subject: "Re: 1.5's use of fonts"
    date: 2006-03-30
    body: "Mandrake/iva doesn't support this hinting thing? I thought they did, since it is a French distribution (they don't have to fear patents, and, in fact, include support for MP3/DVD and all out of box) and fonts in it look much better than in Suse. So, fonts could be rendered even better?"
    author: "blacksheep"
  - subject: "Re: 1.5's use of fonts"
    date: 2006-04-01
    body: "> I thought they did, since it is a French distribution (they don't have to fear patents,...\n\nHow do come to this idea? Patents exist in Europe as well and the patent holders in case of mp3 are french and german. Mandriva will pay for the mp3 license."
    author: "Carlo"
  - subject: "Re: 1.5's use of fonts"
    date: 2006-04-01
    body: "I was obviously referring to software patents. Their holders might be french and german, but they are only filled on countries that have them (USA, Australia, etc)."
    author: "blacksheep"
  - subject: "Re: 1.5's use of fonts"
    date: 2006-04-05
    body: "When you refer to software patents, you shouldn't have mentioned mp3. And no: In Europe there are already thousands of software licenses filed and (questionably if legally) granted by the european patent office. It's just nearly impossible to enforce them, since there's no common european patent law."
    author: "Carlo"
  - subject: "Re: 1.5's use of fonts"
    date: 2006-03-31
    body: "This is all quite true but it isn't relevant for printing.  Hinting is for the low resolution screen display.\n\nFor printing the fonts should be spaced without hinting and Qt 3.x is not able to do this.\n\n"
    author: "James Richard Tyrer"
  - subject: "Re: 1.5's use of fonts"
    date: 2006-03-31
    body: "Yeah.  The I've been trying to use KWord to, but have been unable to because of the font spacing issue.  I did some reading and also figured it must be the freetype glyph hinting thing.  So I downloaded the freetype source deb (I'm using the unstable Debian release) planning on enabling it, and discovered it already was.\n\nAs the freetype changelog says (or as you can deduce from debian/rules and debian/patches/030-bytecode-interpreter.diff in the source package):\n\nfreetype (2.1.2-10) unstable; urgency=low\n* Turning back on the bytecode interpreter.  Too tired to care now.  May turn it off again when Xft2 and fontconfig are in Debian.\n\nIt seems from the source package that the only other Debian specific things are a backwards compatibility patch (adding back in some dropped API), some consistency checks (underflow and overflow checks for BDF glyphs), and an X related rpath linking option.  So, unless I've missed something, I'm thinking it is a QT/KWord thing.\n\nIn fact, from James post, it almost sounds like I should just disable hinting (I have a high resolution display) and everything will be fine.  Maybe I'll give that a go."
    author: "Tyson Whitehead"
  - subject: "Re: 1.5's use of fonts"
    date: 2006-03-31
    body: "Okay tried recompiling without the bytecode interpreter.  It changes the output (both on the screen and on the printer) for sure.  Some parts are better and some parts are worse.  For example, if you use the Times New Roman font at 12pt, a printout of the word \"away\" will have excess space between the 'w' and the 'a' with the Debian stock freetype.  If you recompile it freetype without the bytecode engine this goes away, however, the word \"serious\" acquires a space between the 'o' and the 'u'.\n\nAfter digging into it a bit, I was left unclear over whether the the full bytecode interpreter is enabled by default (due to the inclusion of the autohinter).  For completeness, I tried a version compiled with the bytecode enabled (i.e., TT_CONFIG_OPTION_BYTECODE_INTERPRETER defined) and the  autohinter disabled (i.e., TT_CONFIG_OPTION_UNPATENTED_HINTING undefined).  I couldn't tell any difference over the stock version (i.e., both defined).\n"
    author: "Tyson Whitehead"
  - subject: "Re: 1.5's use of fonts"
    date: 2006-03-29
    body: "Hmmm... I have a good set of fonts (Microsoft's \"web core\" fonts - Arial, Times, Courier New etc) and I tunned up autohinting in my freetype. The combination of both gives me an almost perfect rendering and spacing of letters on the screen, and on (rare) printouts)\n\nHave you tried using good fonts?"
    author: "Daniel \"Suslik\" D."
  - subject: "Re: 1.5's use of fonts"
    date: 2006-03-29
    body: "Yes I've tried a variety of fonts and it's still never looked as good as in OO.  I'll try some others though.\n\nAdam"
    author: "Adam York"
  - subject: "Re: 1.5's use of fonts"
    date: 2006-03-29
    body: "How do you mean by tunning up autohinting? Were or how can you do that?"
    author: "boemer"
  - subject: "Re: 1.5's use of fonts"
    date: 2006-03-29
    body: "The font problems in KWord and KPresenter will indeed be fixed when we move to Qt4. And today I spotted the first relevant commit in svn trunk :-). In any case, you can mitigate the problem by using good quality fonts (Monotype or Adobe) and a high resolution (that means, lots of dpi, not just\nlots of pixels) screen.\n\nI've noticed the problem with OO2 and Abiword, too, by the way. Only Scribus seems immune at the moment."
    author: "Boudewijn"
  - subject: "Re: 1.5's use of fonts"
    date: 2006-03-31
    body: "Font problems in Abiword are most often due to using Zoom to page width rather than Zoom to an even number like 100%.  There may be other issues too but like I said most font complaints about Abiword go away when users change their zoom level.  \n"
    author: "Alan Horkan"
  - subject: "Re: 1.5's use of fonts"
    date: 2006-03-31
    body: "The same is true to a certain extent for KWord. In any case -- I wouldn't work with anything but zoom to page width in any word processor. I'm so happy that Thomas added that zoom level to KWord :-)."
    author: "Boudewijn Rempt"
  - subject: "Re: 1.5's use of fonts"
    date: 2006-03-30
    body: "I have only seen this problem with MS fonts. Other fonts like Nimbus work just fine both on screen and in print."
    author: "yaac"
  - subject: "kpresenter and OOo2 impress problem"
    date: 2006-03-29
    body: "1) I created an odp file with OOo impress. When I tried to open it with kpresenter, it's a mess. The title with a font 36 in OOo does have a font 11 and are not anymore center for example. The strange thing is that the effect is not systematic but depend on the slide. \n\n2) I tried to do the opposite and save a file create with kpresenter in odp and open it with OOo2. The first thing I saw is that the bullet are missing. I didn't push to much the test after it. It's a shame but opendocument between the two most pro-eminent open-source office suite gave to very different result. In the state it's not possible (in my opinion) to tell that the ODT format is an exchange format.\n\n3) I tried to do a bug report but I don't know where...\n \nYour softwares are very cool, the critisim above are only to help to have the best office suite :). Thanks again.\n\nNicolas\n\nps: I'm using package from kubuntu"
    author: "Nicolas"
  - subject: "Re: kpresenter and OOo2 impress problem"
    date: 2006-03-30
    body: "For reporting bugs please go here:\nhttp://bugs.kde.org\n\nBye\nAlex"
    author: "aleXXX"
  - subject: "Re: kpresenter and OOo2 impress problem"
    date: 2006-03-30
    body: "> It's a shame but opendocument between the two most pro-eminent \n> open-source office suite gave to very different result. \n> In the state it's not possible (in my opinion) to tell that \n> the ODT format is an exchange format.\n\nI'd like to emphasise here that problems exchanging OpenDocument documents between KOffice and OO.org usually indicates a bug in software at the moment.  The OpenDocument specification itself is a very good thing.\n\nOpenDocument documents may fail to load correctly for a variety of reasons:\n\n- Architectural issues.  For example the way KSpread stores formatting information currently scales very poorly.  When OpenOffice.org produces spreadsheets, it assumes that assigning a particular format to a large area of otherwise empty cells is 'cheap' in terms of memory and time when loading - not true in KSpread.  This should be fixed in the 2.0 release.  \n- Bugs in KOffice or OpenOffice.  This is probably the biggest cause.  KOffice is probably the worst offender since this is the first release with serious OpenDocument support.  Other developers certainly ran into OpenOffice bugs as well on the road to 1.5 (When developing OpenOffice, there weren't really any other packages to test its output against, so OO.org may load and save a document correctly even if the saved content is wrong)\n- Missing features in KOffice.  It is a younger office suite and doesn't have all the features that OO.org has (eg. KSpread doesn't support multiple text formats in a single cell)\n- Ambiguities in the specification.  This is the most problematic when it happens, but I don't think there have been many cases of this so far.\n\nThe problems concerning OpenDocument support are in some ways similar to achieving compatibility between web browsers when dealing with HTML/CSS.  Although both formats are well defined, open and stable, there is a lot of work required to support all aspects of them and even small flaws can produce big visual changes.\n\nThe good news is that a large set of test documents are being compiled at the moment so in the future we should have a good set of reference documents to test against.  "
    author: "Robert Knight"
  - subject: "new/open"
    date: 2006-03-31
    body: "Do file->new and file->open still take you to the same place in the same widget?  It's a small thing, but that redundancy really annoys me, especially since its smallness makes it likely to be ignored.\n\nhttp://osnews.com/permalink.php?news_id=13975&comment_id=104819 <- how to reproduce my gripe."
    author: "MamiyaOtaru"
  - subject: "Re: new/open"
    date: 2006-04-03
    body: "This is a bug indeed; I'll take a look.\n\nI must have missed the report when you reported it to http://bugs.kde.org :)"
    author: "Thomas Zander"
  - subject: "Tables in KWord - extremely inconvenient"
    date: 2006-04-01
    body: "You have to do a good lot of clicks just to append a row to a table.  MS Office / Open Office allow you to do this with single \"Tab\" press. \nYou have to do yet bigger lot of clicks to insert / append several rows.\nHas it been really so hard to apply a little bit of thinking to details so simple and trivial?\n"
    author: "Alex"
  - subject: "Re: Tables in KWord - extremely inconvenient"
    date: 2006-04-01
    body: "Is it really so hard to dive into the code and come up with a patch? We know the current table implementation is very limited in reliability, in usability, and in many other respects. Doing a new table implementation is hard work, much harder than you seem to appreciate. Suggestions, help with testing, that's all welcome. Sneering is not."
    author: "Boudewijn Rempt"
  - subject: "Re: Tables in KWord - extremely inconvenient"
    date: 2006-04-03
    body: "Unfortunately, I've got no spare time to dive into the code. \n\"Doing a new table implementation is hard work...\" - sorry, I've got only one answer for that, no sneering intended - it should have been done with usability and convenience in mind from the very beginning. As for now, KWord is confined to \"quick and simple\" documents. For them, it's excellent choice - lightweight, simple, well-integrated with KDE. For more advanced missions - my personal choice is OpenOffice.\n"
    author: "Alex"
  - subject: "Re: Tables in KWord - extremely inconvenient"
    date: 2006-04-01
    body: "A classic example of the importance of design.\n\n'Design twice, code once'"
    author: "James Richard Tyrer"
---
Things are getting closer.  The KOffice team is proud to announce the release candidate of KOffice 1.5.  With lots of good feedback from the users of beta 1 and beta 2, the release candidate is better than ever. We now invite the user community for the final round of testing of KOffice 1.5 before before the suite is finally released. Read <a href="http://www.koffice.org/announcements/announce-1.5-rc1.php">the full announcement</a>, <a href="http://www.koffice.org/announcements/changelog-1.5rc1.php">the changelog</a>, <a href="http://download.kde.org/download.php?url=unstable/koffice-1.5-rc1">download the release</a>, and give it some real world testing for us! 




<!--break-->
<p>In this release, the OASIS OpenDocument support is improved even more, especially in KChart. It's still not perfect, but we have improved interoperability with OpenOffice.org a lot. Krita has gotten faster and more stable and KFormula has a new maintainer that has really come up to speed quickly. And that's just the highlights: across the board, we have improved all applications, making them better and more polished.</p>





