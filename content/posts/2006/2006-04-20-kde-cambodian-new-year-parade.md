---
title: "KDE at the Cambodian New Year Parade"
date:    2006-04-20
authors:
  - "ajohnson"
slug:    kde-cambodian-new-year-parade
comments:
  - subject: "Khmer"
    date: 2006-04-21
    body: "Khmer language looks so beautiful."
    author: "gerd"
  - subject: "Re: Khmer"
    date: 2006-04-21
    body: "Is there a video or other tutorial which explains an average joe how to translate and contribute?\n\nTranslations are supposed to be an easy task but the translation backlog clearly shows that translation needs an usability boost and entrance barriers to get removed.\n\nWebbased systems were advocated by ssome but all that I had seen so far was garbage.\n\nI mean, when people show up and say: I want to help your Khmer translation effort. What happens next?  \n\nWhat I would like to see is a kind of simple way of contributing translations.\n\nAn entry in the help menu of each KDE application \"translate\" which shows up if no native language support for the app exists, which then opens a translation tool, fetches the associated pot file, and lets me translate the app Kmyapp into lg language and try it out. When its fine for me I can submit the po file through some automatic background transmission to the national team which then performs q&a checks and puts the file into the repository or forwards it to the external project.\n\nThis would make it easy to even use a simple live-CD of the newest version of KDE for translators and try translations out on the fly without getting involved with translation teams, SVN etc."
    author: "Karsten S."
  - subject: "Khmer Software?"
    date: 2006-04-21
    body: "What kind of Khmer Software you have there, Aaron? I can only see the KhmerOS logo at the back slat, the blue one above Debian."
    author: "soben"
  - subject: "Re: Khmer Software?"
    date: 2006-04-21
    body: "Actually, \u00fee software \u00fe\u00e6t is \u00fe\u00e6re \u00e6re just \u00fee regular Kubuntu \u00e6nd Knoppix CDs in \u00c6nglish. \u00de\u00e6re w\u00e6s not enough time to create CDs \u00fe\u00e6t h\u00e6d \u00fee Khmer language on it. \u00dee flyer did list \u00fee software \u00fe\u00e6t w\u00e6s available in Khmer, \u00feough. Ich did h\u00e6ve Khmer running on \u00fee computers, but couldn't demonstrate \u00fee software wi\u00f0out power."
    author: "Aaron Johnson"
---
April 16, 2006 was not just Easter Sunday, but also Cambodian New Year Day. Long Beach, California, which has one of the largest Cambodian communities outside of the country of Cambodia, celebrated the day with a <a href="http://www.cambodiannewyearparade.com/">parade</a> and displays at a nearby park. KDE was there presented by a couple of local KDE users representing <a href="http://www.khmeros.info">KhmerOS</a>. KhmerOS is tasked with translating Free Software such as KDE into the Khmer language.



<!--break-->
<div style="border: solid grey thin; padding: 1ex; margin: 1ex; float: right">
<img src="http://static.kdenews.org/jr/khmeros-stand.jpg" width="300" height="225" border="0" />
</div>

<p>The day started out with Aaron Johnson bringing the equipment and Daniel Dotsenko setting up the booth. This being the first time that a Free Software booth was setup in Long Beach, there was bound to be glitches. The main problem was that the organisers were not able to provide the power that was promised in order to run the computers. That left the fallback of providing software and flyers. On hand were some Kubuntu install and Knoppix live CDs. However, there were not enough and within two hours, all the CDs were gone, as were the flyers. No power meant that they could not burn any other CDs or print more flyers. Other than these problems, things went well.
</p>

<p>
For the longest time Cambodia had been in neglected in the area of technology due to the fact that the country had been in the midst of civil war for many years. Now that things have stabilised the country has found itself behind other countries when it comes to computers. Many users have found themselves needing to use proprietary software in foreign languages such as English and French. Cambodia is too poor to pay proprietary software companies to translate their software to Khmer, and not a large enough market for those same companies want to cater their products to the language. That is where Free Software comes in. The open source licenses allow developers working with KhmerOS, also known as the Khmer Software Initiative, access to those parts that need translating, without needing to pay a proprietary software company fees for such access. Other than creating drivers for the Khmer Unicode keyboard, most of the work has concentrated on translating Free Software such as KDE.</p>

<p>
There was quite a lot of interest in software available in the Khmer language. Many, of course, were surprised that the software could be available at no cost, especially after growing accustomed to paying for proprietary solutions to typing Khmer. Some wondered if we were trying to infect their computer with viruses, or trying to pass off pirated material. Many however were excited about the prospects of using their computers in their own language. Some took the CDs or flyers, and then shortly returned with members of their family or friends for more.
</p>

<p>
The display did have several objectives. One of the goals of the display was to encourage bilingual Khmer and English speaker to help KhmerOS in translating and proofreading the translations. The other goal was to create awareness of Free and open source software under initiatives such as KDE Everywhere. The display on Sunday has started the process of accomplishing those objectives. This has been the first attempt to market KDE and FOSS in the greater Long Beach area, and hopefully not the last.</p>


