---
title: "KDE Wins Best Desktop Environment Award"
date:    2006-03-10
authors:
  - "asitter"
slug:    kde-wins-best-desktop-environment-award
comments:
  - subject: "Thanks to everyone!"
    date: 2006-03-10
    body: "A heartfelt thanks to all developers, testers and users of Quanta Plus for garnering our third consecutive win as best web development tool from linuxquestions.org. This is truly a well conducted user poll and it's not at all easy to pull off a win there. This has also been good to promote Quanta and KDE to a lot of users who for whatever reason may not be familiar with us. Finally huge thanks to all KDE developers who have made the worlds best development platform. Without the advantages of KDE we would still be in the starting blocks pulling our hair out. Andras and I have a lot of hair, so let's not think about that. ;-) Oh, and a special note of appreciation to Andras for being the most awesome guy I know."
    author: "Eric Laffoon"
  - subject: "Re: Thanks to everyone!"
    date: 2006-03-10
    body: "I read from previous Linux Questions Members Choice Awards, Quanta also won the best web development category for year 2003, 2004, 2005. So now is the fourth consecutive win? :D\n\nAnd KDE always wins the best Development Environment since 2001, or 6 times! Kongratz to all KDE developers!"
    author: "fyanardi"
  - subject: "Re: Thanks to everyone!"
    date: 2006-03-10
    body: "The current award is for 2005. ;-)"
    author: "Andras Mantia"
  - subject: "Re: Thanks to everyone!"
    date: 2006-03-10
    body: "Yeah, thanks for putting us in the bag for 2006, but we have to wait a year to win that. 2007 will be sweeter because that will the one that reflects a substantial KDE 4 useage with new tools and rewritten VPL."
    author: "Eric Laffoon"
  - subject: "Re: Thanks to everyone!"
    date: 2006-03-11
    body: "Ooopss :D but of course next year I'll vote for Quanta again ;)"
    author: "fyanardi"
  - subject: "Konqueror+metabar is great!"
    date: 2006-03-10
    body: "konqeuror looks a lot better with metabar and without sidebar tabs/buttons visible."
    author: "fast_rizwaan"
  - subject: "my metabar theme"
    date: 2006-03-10
    body: "i've edited the layout file of metabar, and moved the \"links\" section to the top which makes dynamic changing of metabar entries won't change the position of \"links\"."
    author: "fast_rizwaan"
  - subject: "hide sidebar tabs buttons"
    date: 2006-03-10
    body: "copy the konqsidebartng.rc to ~/.kde/share/config/ and restart konqueror"
    author: "fast_rizwaan"
  - subject: "Re: Konqueror+metabar is great!"
    date: 2006-03-10
    body: "I have to disagree, I love Konqueror vertical side tabbar and doesn't like Metabar. We all have different taste..."
    author: "Mikos"
  - subject: "Re: Konqueror+metabar is great!"
    date: 2006-03-10
    body: "Looks really nice. Kudos."
    author: "blacksheep"
  - subject: "Award page desperately needs an update"
    date: 2006-03-10
    body: "http://www.kde.org/awards/"
    author: "ac"
  - subject: "makes me wonder"
    date: 2006-03-10
    body: "> The distance between KDE and the other desktop environments increased over last year\n\nSo, Red Hat and (to some extent) SUSE - how do you guys expect to build desktop business by not listening to your customers a single bit? Incredible."
    author: "Anonymous"
  - subject: "Re: makes me wonder"
    date: 2006-03-10
    body: "It is not Suse, SuSe was always KDe based. It is ximians hostile takeover at Novell."
    author: "gerd"
  - subject: "Re: makes me wonder"
    date: 2006-03-10
    body: "RedHat has long ago abandoned the desktop in favor of the enterprise, where the money is."
    author: "blacksheep"
  - subject: "Love Konqueror but the awful Name"
    date: 2006-03-10
    body: "Hi,\nFirst Of all thank KDE for this great Desktop. The Only reason I could encourage people to use Linux/Open source was because of all the free killer Apps like Amarok and others.\nI also like Konqueror but the Name is too much.\nI am German and in German the pronunciation is... (can\u0092t realy describe it\u0085 horrible I guess). I also believe that in English it isn\u0092t fun to say \"Konqueror\" either. Are here any French people who can tell me if the Name sounds good being a native French speaker? I know of the story behind the meaning of the Name but it is too geekish nowadays. Wouldn\u0092t it be nice to get a flasher, snappyer Name for 4 which is nicer to write and pronounce in more Languages? At least the main Languages English, German, and French.\nAny Ideas? \n\nHermann\n"
    author: "Hermann Thomas"
  - subject: "Re: Love Konqueror but the awful Name"
    date: 2006-03-10
    body: "People, why don't we allow KDE to rename a few apps for KDE4? That would be really cool!"
    author: "ac"
  - subject: "Re: Love Konqueror but the awful Name"
    date: 2006-03-10
    body: "Yes, let's confuse everybody by changing all the application names!\n\nSorry, you're not making sense. For an application _re-write_ this could make sense. But one of the names that is NOT going away is \"Konqueror\", since it's already too much ingrained and there are already some websites detecting Konqueror's user-agent (instead of KHTML...), so we want to keep compatibility.\n\nFinally, \"he who does the work decides\". If you write an application, you get to chose the name. Just as long as it is not offensive (which is quite subjective and always a hot topic)."
    author: "Thiago Macieira"
  - subject: "Re: Love Konqueror but the awful Name"
    date: 2006-03-10
    body: "Of course that is probably a reason. And of course it makes sense. \nThe Question I was askin is if you all like the Name?\nDoes Konqueror sounds good to you all?\nAm I the only one that tae Name gives me a headake When I say or write it?\nI was not ment to Flame. I am probably the Only one. "
    author: "Hermann Thomas"
  - subject: "Re: Love Konqueror but the awful Name"
    date: 2006-03-11
    body: "The Question I was askin is if you all like the Name?\n\nYes, it's much better then KFM :)\n\n Does Konqueror sounds good to you all?\n\nSure, why not?\n\n\n Am I the only one that tae Name gives me a headake When I say or write it?\n\nWell, why would you say/write it? Just use it :o)"
    author: "ac"
  - subject: "Re: Love Konqueror but the awful Name"
    date: 2006-03-10
    body: "\"But one of the names that is NOT going away is \"Konqueror\", since it's already too much ingrained and there are already some websites detecting Konqueror's user-agent (instead of KHTML...), so we want to keep compatibility.\"\n\nMy comment aboutrenaming several apps was too much, I understand, sometimes I type before I think. Still, what if many many people would want a rename, would you defy that? In the end it's up to the users I think, it's not like some people would like a rename just for the heck of it, there would have to be strong cases for it, I think there are. A rename might be an opportunity for KDE4. Just as with all the cool Plasma and Solid names. Changing Konqueror's name after KDE4 seems harder, but the confusion about it's name would likely only grow with a larger user base. It's a turn off name and ask anyone to write it down for you! Yes there are allot of problems related to a rename of Konqueror, but allot of opportunity as well I think. I keep rechecking how I spell Konqueror even after several years, the name just sucks IMO."
    author: "ac"
  - subject: "Re: Love Konqueror but the awful Name"
    date: 2006-03-10
    body: "I, for one user, have no interest in seeing Konqueror renamed.  To me, it's a good name that works well in the tradition of Navigator and Explorer.  When you think about it, Opera, Firefox, etc., are equally strange, IF you sit and think about them that much.  But mostly, it works.  It's a good name, that has built up a good brand reputation, and it would be silly to give that up.  All sorts of programs have weird names; it's not a problem."
    author: "Lee"
  - subject: "Re: Love Konqueror but the awful Name"
    date: 2006-03-12
    body: "Let me repeat: Konqueror will not be renamed. It is already too late for that."
    author: "Thiago Macieira"
  - subject: "Re: Love Konqueror but the awful Name"
    date: 2006-03-13
    body: "Let me repeat: Konqueror will stay a crappy name. It is not too late. How many People on earth use it?"
    author: "Hermann Thomas"
  - subject: "Re: Love Konqueror but the awful Name"
    date: 2006-03-14
    body: "why would a new name make users use it?\nkonqueror is the most popular filemanager on linux, how much more popular would another name make it?\n"
    author: "ac"
  - subject: "Re: Love Konqueror but the awful Name"
    date: 2006-03-10
    body: "No not a few. Just the one with the crappy name."
    author: "Hermann Thomas"
  - subject: "Re: Love Konqueror but the awful Name"
    date: 2006-03-17
    body: "About French pronounciation, it just sounds great. It reminds the word \"conqu\u00e9rant\" (conquering) thus it sounds like a winner name which can only get more users ;)"
    author: "Futal"
  - subject: "Re: Love Konqueror but the awful Name"
    date: 2006-03-10
    body: "Konqueror is actually very easy to pronounce in English (as far as such things go).  It is pronounced just like the \"real\" English word \"conqueror\". See, they replaced the \"C\" with a \"K\", starting a naming tradition that we have yet to break.\n\nNowadays application developers get smarter about where to put the \"K\" though, like amaroK."
    author: "Michael Pyne"
  - subject: "Pronunciation is easy"
    date: 2006-03-11
    body: "It goes like \"Kon-ke-ra\" (yes, also in German). Really doesn't twist your tongue. And the name is similar to (Windows) Explorer or (Netscape) Navigator."
    author: "7sec"
  - subject: "Re: Pronunciation is easy"
    date: 2006-03-13
    body: "I know how to pronounce it. That wasn\u0092t my Point. And it is possible to say it. It Doesn\u0092t change the fact that it's not snappy and nice.\nIt Sounds too geek.\n\nHermann\n"
    author: "Hermann Thomas"
  - subject: "Re: Love Konqueror but the awful Name"
    date: 2006-03-12
    body: "Konqueror is English as well as Firefox and kmail and I wouldn't attempt to pronounce these words German because they aren't. As for English: There was this bloke called William the Conqueror and he invaded Britain in 1066 and you can pronounce konqueror exactly like this name and it sounds really nice. "
    author: "gromit"
  - subject: "Re: Love Konqueror but the awful Name"
    date: 2006-03-13
    body: "I know how to pronounce it. And yes I can speak English quite well.\nAnd Firefox and kmail are nicer to say. It is not a matter of English or not English. Konqueror just stays a Crappy Name.\nLook at al the Names Apple uses for there Products. Those are nice. I actually disgust this in a non geek group of people, and the majority found the Name awful as well.\n\nHermann"
    author: "Hermann Thomas"
  - subject: "konqueror market share"
    date: 2006-03-10
    body: "Actually, Konqueror as a web browser is not very popular when looking at distrowatch statistics:\nhttp://distrowatch.com/awstats/awstats.DistroWatch.com.html\n3% of all users, with of 29.5% using linux, means around 10% of linux users use konqueror (very similar to the 11% of the votes). Adding the few BSD does not change a lot the stats.\n\nHow many distros do propose Konqueror as web browser to the users. Mandriva, Suse are quite big distros, so it means when given a choice, most users do prefer Firefox to browse."
    author: "oliv"
  - subject: "Re: konqueror market share"
    date: 2006-03-10
    body: "Maybe they are browsing on side, while managing files?"
    author: "reihal"
  - subject: "Re: konqueror market share"
    date: 2006-03-10
    body: "I still believe that we should promote Konqueror as the best true unified browser rather than trying to artificially split it into two applications.\n\nBut, to the point.  The unfortunate fact is that Firefox is probably more popular due to the many seriously broken web sites our there.  For whatever reason, Firefox appears to be better able to display such broken web sites.\n\nFor now, it appears that Kecko is the answer to this situation.  Are we making any progress on this.\n\nNote that I prefer Konqueror and only open Firefox when Konqueror fails to work on a broken web site.  If Kecko was available, I could just switch from KHTML to Kecko when needed.\n\nAnother problem which could probably be fixed is that Konqueror sometimes fails to open Flash -- probably also due to broken HTML code."
    author: "James Richard Tyrer"
  - subject: "Re: konqueror market share"
    date: 2006-03-10
    body: "> rather than trying to artificially split it into two applications\n\n:-D I assume your 'artificially' is a joke.\n\n> it appears that Kecko is the answer to this situation. Are we making any progress on this.\n\nSure, most of 'we' are working hard on kecko. Come join 'we'."
    author: "asd"
  - subject: "Re: konqueror market share"
    date: 2006-03-10
    body: ">Another problem which could probably be fixed is that Konqueror sometimes fails to open Flash\n\nTo me, this looks like a big feature ;)\n\nI use flashblock in Firefox, it is one of my preferred extensions :)"
    author: "oliv"
  - subject: "Re: konqueror market share"
    date: 2006-03-10
    body: "> But, to the point. The unfortunate fact is that Firefox is \n> probably more popular due to the many seriously broken web \n> sites our there. For whatever reason, Firefox appears to be \n> better able to display such broken web sites.\n\nI use FF instead of Konqueror because of some excellent extensions and because I can use it on Windows and MacOSX too. I love crossplatform programs. \n\n> For now, it appears that Kecko is the answer to this \n> situation. Are we making any progress on this.\n\nSafari on OSX has similar problems with broken websites. Even so it's the most popular browser on that OS."
    author: "Ascay"
  - subject: "Re: konqueror market share"
    date: 2006-03-10
    body: "I used to be a big fan of Firefox until I switched to a Linux desktop at work and home. I kept Firefox for roughly a month, but gave Konqueror a shot from time to time since it was integrated into KDE. I noticed that it was quite fast, where Firefox was not.\n\nI've been a devoted user ever since. And in that context, I hope the rumours of a Konqueror for Windows come true, because I think Firefox could use some real free software competition.\n"
    author: "Chase Venters"
  - subject: "Re: konqueror market share"
    date: 2006-03-19
    body: "> I use FF instead of Konqueror because of some excellent extensions and because I can use it on Windows and MacOSX too. I love crossplatform programs.\n\nKDE has Konqueror\nMac has Safari\n\nDoes Windows have any version of KHTML at all?  Put another way if a web designer is using windows is there an easy way to test against KHTML?  Just compling with standards would be the ideal solution I know, but a windows version of KHTML would make it a whole lot easy do run a few quick tests to at least make sure the basics work.  "
    author: "Alan Horkan"
  - subject: "Re: konqueror market share"
    date: 2006-03-14
    body: "\"I still believe that we should promote Konqueror as the best true unified browser rather than trying to artificially split it into two applications.\"\n\nThat tac has failed. A web browser and a file manager are simply two different types of applications, albeit, with shared components (and that's where this flawed unified idea generally comes from). That is a much simpler and better solution to have - applications with shared components."
    author: "Segedunum"
  - subject: "Re: konqueror market share"
    date: 2006-03-17
    body: "<I>\"A web browser and a file manager are simply two different types of applications\"</I>\n\nTell this to my mom who doesn't even know what a web browser or a file manager is. Actually, this is two different types of applications only for geek people who have been educated to use two different applications.\n\nMy mom does not even make a difference between what is on her computer and what is on the web. And I don't see the point to explain her because Konqueror is what she needs:\n<ul>\n <li>she clicks on a icon, she sees the content whatever the file type is;\n <li>she clicks on a link, she sees the next page whatever it is;\n <li>she put words in the URL bar, it launches a search on Google.\n</ul>\nI don't want to explain her what a URL is. I don't want to tell her that she has to have a different window to display each document because it is quite hard for her to manage with different windows."
    author: "Futal"
  - subject: "Re: konqueror market share"
    date: 2006-03-20
    body: "Exactly!\n\nThe true greatness of konqueror (and yes, I also think that it's a lot cooler/better/supercalifragilistiouser name than firefox) is that it unifies access to resources, local and remote. I've never understood what makes local and remote files so different that they need to be accessed with two different applications.\n\nIf konqueror would be split up to a web browser and a file manager, each part would end up duplicating functionality in the other, creating unnecessary confusion. For example, to which application does the functionality for ftp site browsing belong? Put it in either one and it is in the wrong place."
    author: "Anonymous Joe"
  - subject: "Re: konqueror market share"
    date: 2006-03-10
    body: "Both Mandriva and Suse have a browser icon on the panel that fires FireFox. Only if not installed, it will use Konqueror."
    author: "blacksheep"
  - subject: "Re: konqueror market share"
    date: 2006-03-21
    body: "It would be interesting if it were possible to break those linux/bsd stats down between dedicated users and dual-booters. I suspect more dual-booters use Firefox due to cross platform convenience, and probably more dedicated KDE desktops use Konqueror.\n\nThis is not to say that apparently a sizable number of dedicated KDE users do not favour Firefox, but just that I suspect the ratios would be different. \n\nIt seems to me the main killer feature of Firefox, from people's comments,  is easy to install extensions, more than its renderer/js implementation. For me the only extension that almost (but not quite) made me consider Firefox was adblock. Now that (finally!) Konqueror has a similar feature, Firefox (with it's hideous, hideous GTK based dialogs) is used even less. \n\nThough as a developer I do use it a fair bit. Especially for the extremely convenient liveheaders extension, and the \"web developer\" extension. Also the Firefox/Seamonkey \"Page Info\" dialog is quite wonderfully detailed.\n\nStill for 90% of my browsing... Konqueror, like KDE as a whole, just feels a whole lot better than the \"competition\" while being functionally equivalent. (And the tabs---without the need of extension enhancement---work better. KGet integration is handy at times.)\n\n"
    author: "T. Middleton"
  - subject: "KOffice increases its market share significally"
    date: 2006-03-10
    body: "For the first time in 4 years, KOffice is starting to climb again.  This is the result of the influx of serveral new developers in the last year and the increased development speed that brings.  And this is DESPITE that OO.o released it's great new 2.0 this year with all the hype that generated.\n\nNow, let's just finally get 1.5 out the door and start on the next new KOffice generation.  Expect more to come here!\n"
    author: "Inge Wallin"
  - subject: "KDE wins awards and Gnome wins the distros"
    date: 2006-03-10
    body: "It looks like the superior desktop KDE is going to lose in the end against the inferior Gnome (I am not saying that gnome is bad). Red Hat and Ubuntu prefer Gnome and Suse/Novell seem to make the shift from KDE to Gnome as well. What a pity for KDE! does anyone know why KDE is losing acceptance by the big distros? Let's hope that Linux (with whichever desktop) is going to win the future ;-) My dream would be that Linux will beat Windows in the long term and both KDE and Gnome will coexist with 50% share each."
    author: "Oliver"
  - subject: "Re: KDE wins awards and Gnome wins the distros"
    date: 2006-03-11
    body: "most redhat installs on desktops that i've seen use kde. Besides, redhat does not score that good in the award poll :o)\n\nubuntu can be running kde or gnome, you can't tell that from the figures on linuxquestions.org. But if you compare the 'best distribution' with 'best desktop', one can conclude that ubuntu-users mostly run KDE :)\n\nSUSE switching to gnome? What gave you that idea?\n\nKDE losing acceptance by the big distro's? \nRedhat never accepted KDE, so nothing to loose over there.\nCanonical made Kubuntu equal to Ubuntu, so that's a win for KDE\nSUSE is still running KDE, en probably will continue to do so.\nSUSE 10.1 will contain some interesting new/improved tools (kpowersave, knetworkmanager,kerry), all of them custom made for KDE.\n\n"
    author: "ac"
  - subject: "Re: KDE wins awards and Gnome wins the distros"
    date: 2006-03-11
    body: "Yes, it's a shame that KDE doesn't have any major distros anymore.  VHS won out over Beta, Gnome over KDE..."
    author: "Tony"
  - subject: "Re: KDE wins awards and Gnome wins the distros"
    date: 2006-03-11
    body: "no major distro's?\nkde has kubuntu, suse, mandriva, linspire, etc. etc.\nand kde has the users, 2 out of 3 prefers kde.\n"
    author: "ac"
  - subject: "Re: KDE wins awards and Gnome wins the distros"
    date: 2006-03-11
    body: "the only major distribtion which is left is mandrake. kubuntu is second class compared to ubuntu. do you really believe that mark shuttleworth, a long time gnome user and contributor, suddently promotes kde? the reason behind it was (maybe still is) that novell (suse) stupidily decided to drop kde. as you know, they now promised to support kde as well (while gnome being the major desktop on the enterprise poducts).\n\nlinspire really isn't a major distribution, the distribution is just being sold by a few people (some who buy new computers).\n\non the long run, gnome seems to win (which isn't gnome's fault,rather being kde's which always wanted to be independent)."
    author: "acacac"
  - subject: "Re: KDE wins awards and Gnome wins the distros"
    date: 2006-03-11
    body: "http://linux.slashdot.org/article.pl?sid=05/11/06/1413237\n\n\"In his opening remarks at the start of the conference Ubuntu founder Mark Shuttleworth announced that he was now using Kubuntu on his desktop machine and said he wanted Kubuntu to move to a first class distribution within the Ubuntu community\"\n\nSo yes is the answer."
    author: "Chris Howells"
  - subject: "Re: KDE wins awards and Gnome wins the distros"
    date: 2006-03-12
    body: "Well, that's been by the time Novell \"tried to abandon\" KDE. Shuttleworth just jumped in. Guess why? Right! It's all about popularity and money :-)\nSo, no, (k)Ubuntu is not really a major distribution (just compare how well organized/arranged GNOME is compared to KDE)."
    author: "acacac"
  - subject: "Re: KDE wins awards and Gnome wins the distros"
    date: 2006-03-14
    body: "\"(just compare how well organized/arranged GNOME is compared to KDE).\"\n\nusers think otherwise :o)\n"
    author: "acdc"
  - subject: "Re: KDE wins awards and Gnome wins the distros"
    date: 2006-03-13
    body: "\"the only major distribtion which is left is mandrake. \"\n\nout of how many major distributions?\ncan you tell me which major distributions switched from kde to gnome?\nI can't name anyone..\n\nFor example Novell, do you know how many kde-developers Novell has? 3 out of 7 members of kde's technical working group are employees from Novell.\n\nLooking at SUSE 10.1, most promising additions to the desktop are kde-applications developed bij novell (kpowersave, knetworkmanager, kerry).\nNow, that's not something a distro that moved to gnome would do, don't you think?\nNovell also pays the guy that makes openoffice.org integrate in kde\n\n\"kubuntu is second class compared to ubuntu.\"\nubuntu never abandoned kde, ubuntu started as a gnome distribution, and made a move towards kde by giving kubuntu the same status as ubuntu.\nThat's not a move away from kde, that is a move towards kde, no matter how insignificant you find kubuntu compared to ubuntu.\n\n\"Do you really believe that mark shuttleworth, a long time gnome user and contributor, suddently promotes kde? \"\nWell, apparently he does, canonical now also ships kubuntu cd's.\n\n\"the reason behind it was\"\nhmm, so he did promote kde?\nYour now contradicting yourself: did or didn't mark promote kde?\n\n \"(maybe still is) that novell (suse) stupidily decided to drop kde. as you know, they now promised to support kde as well (while gnome being the major desktop on the enterprise poducts).\"\non novell linux desktop, gnome was already the desktop of choise for novell.\nSo no loss over there.\nBesides, not every novell customer chooses gnome, see some articles about the novell desktop and kde on the dot.\n \n \"linspire really isn't a major distribution, the distribution is just being sold by a few people \"\nlinspire can't make a living from just a few people, so i guess it should be a lot more then just a few :o)\n\n\"(some who buy new computers).\" Ah yeah, linspire comes pre-installed on several computersystems.\nEven on HP laptops in Africa, which should be the domain of ubuntu..\nNow, who is major, and who is not?\n\n \n\" on the long run, gnome seems to win (which isn't gnome's fault,rather being kde's which always wanted to be independent).\"\nHmm, gnome isn't independent?\nIt was the last time i looked, what has changed since then?\nWho controlls gnome according to you?\nAnd is it a good or bad thing for linux is a major component like the desktop is controlled by one entity?\nI thought the whole purpose of Gnome was to provide a free and independent desktop, as answer to KDE, which depended on TrollTech.\n"
    author: "ac"
  - subject: "Re: KDE wins awards and Gnome wins the distros"
    date: 2006-03-13
    body: "Let's not be pessimistic.Yes, it seems that for some reason most distros prefer Gnome at the moment, but things change over time - sometimes rapidly. And it's not like Gnome is attracting lots of commercial apps either.. Nothing is carved in stone."
    author: "Apollo Creed"
  - subject: "Re: KDE wins awards and Gnome wins the distros"
    date: 2006-03-14
    body: "Yes, it seems that for some reason most distros prefer Gnome at the moment\n\n-> name 1 distribution that prefers gnome at the moment :)\nIt is just plain nonsense, most distributions (even redhat) include both desktop, some distributions (like linspire) have only 1 desktop.\n\nApperently 2 out of 3 linux users prefers KDE, so distributions would be real stupid if they dropped kde.\nAnd therefore no distribution ever dropped kde.\n(some do drop gnome, like slackware last year...)"
    author: "acdc"
  - subject: "Re: KDE wins awards and Gnome wins the distros"
    date: 2006-03-14
    body: "Slackware always had Dropline to begin with, so dropping Gnome didn't make much difference.\n\nStop living in denial about KDE and distros."
    author: "Tony"
  - subject: "Re: KDE wins awards and Gnome wins the distros"
    date: 2006-03-14
    body: "\"Slackware always had Dropline to begin with, so dropping Gnome didn't make much difference.\"\n\nJust like KDE has it's own line in Fedora\n \n \"Stop living in denial about KDE and distros.\"\n\nWell, give some evidence about distro's moving away from kde towards gnome.\nSo far, none seen in this thread.\n\nIt's easy to comment like \"you guys have all the users, but we own the distro's\"\nWithout giving any proof..\n\n"
    author: "acdc"
  - subject: "Re: KDE wins awards and Gnome wins the distros"
    date: 2006-03-14
    body: "It doesn't really matter if Gnome is winning the distros.  It's not like the desktop Linux is becoming mainstream.  If anything, it gives KDE more creative room."
    author: "Tony"
  - subject: "Re: KDE wins awards and Gnome wins the distros"
    date: 2006-03-14
    body: "\"What a pity for KDE! does anyone know why KDE is losing acceptance by the big distros?\"\n\nIt's not, and even if it was, it doesn't matter because the distributions don't matter."
    author: "Segedunum"
  - subject: "Re: KDE wins awards and Gnome wins the distros"
    date: 2006-03-14
    body: "indeed, it's the users that count :)\n\nAnd the users will find their way to KDE, no matter what desktop their distibution prefers..."
    author: "AC"
  - subject: "K3B would have deserved an award"
    date: 2006-03-17
    body: "Unfortunately, there was no award for CD-burning application because K3B should definitively have deserved an award. It is far more useable than any other burning application, even Nero which has been released for Linux is not as good.\n"
    author: "Futal"
---
The <a href="http://www.linuxquestions.org/questions/showthread.php?t=422222">results of the  LinuxQuestions.org 2005 awards</a> were published earlier this week and KDE once again won the Desktop Environment of the Year award.  The distance between KDE and the other desktop environments increased over last year while no less than 3 KDE applications won in their own categories. The LinuxQuestions.org visitors thought that <a href="http://www.kdewebdev.org">Quanta</a>, the homemade feature rich web development application, is the best Free Software application available for web design. The extragear music player <a href="http://amarok.kde.org">amaroK</a>, which helps you rediscover your music, finally took over reign within the multimedia category. Last but not least KDE's most used application, the pumping heart of the desktop environment, <a href="http://www.konqueror.org">Konqueror</a>, received an award as best file manager. 







<!--break-->
<p>But that's not all, various applications also received mostly close second places. The code development application <a href="http://www.kdevelop.org">KDevelop</a>, which is used by most KDE authors, got a supremely close 0.61% less votes than the winner Eclipse. <a href="http://kate.kde.org">Kate</a>, the text editor of choice for KDE users, also got a solid 2nd place in the editors category, and is probably the only one within the top 3 also usable by non-geeks. Coming close behind the cross-platform competition was <a href="http://www.koffice.org">KOffice</a> in the Office Suite category, <a href="http://www.konqueror.org">Konqueror</a> for web browsers, <a href="http://kmail.kde.org">KMail</a> for e-mail clients and <a href="http://kopete.kde.org">Kopete</a>, the Instant Messenger of KDE, narrowed its position compared to last year. Finally the application that everyone is using, but noone knows it exists, KWin, our mighty window manager, received a second place as well. </p>

<p>KDE and its applications are still improving so expect increased ratings next year, when LinuxQuestions.org again asks "Who rules the Linux world?".</p>





