---
title: "Qt Included in New LSB Desktop Standard"
date:    2006-04-27
authors:
  - "mklingens"
slug:    qt-included-new-lsb-desktop-standard
comments:
  - subject: "GNOME to be included in LSB 3.2, but not KDE ?!"
    date: 2006-04-27
    body: "According to the following link, GNOME libraries will be added to LSB, but there is no talk about including KDE libraries:\n\nhttp://www.freestandards.org/en/GnomePlan3\n\nThis seems just wrong, considering that KDE has over 50% of marketshare. Are they going to get away with stuffing GNOME down our throats simply by calling it 'the standard' ?!"
    author: "ac"
  - subject: "Re: GNOME to be included in LSB 3.2, but not KDE ?!"
    date: 2006-04-27
    body: "Read the full article above, troll."
    author: "KDE User"
  - subject: "Re: GNOME to be included in LSB 3.2, but not KDE ?"
    date: 2006-04-27
    body: "Yes."
    author: "reihal"
  - subject: "Re: GNOME to be included in LSB 3.2, but not KDE ?!"
    date: 2006-04-27
    body: "A lot of the pages on freestandards.org are really outdated, including the one you mention.\n\nThe \"GNOME libraries\" listed on that page are really only Gtk+ (which was standardized in LSB 3.1 alongside Qt), plus a few minor GNOME libraries that have been left out.\n\nThe current plan is to focus on freedesktop.org and other shared libraries in LSB 3.2, and to deal with the KDE and GNOME libraries much later.\n\nOlaf\n"
    author: "Olaf Jan Schmidt"
  - subject: "A step in the right direction."
    date: 2006-04-27
    body: "Without KDE I wouldn't be using Linux"
    author: "J.B."
  - subject: "Re: A step in the right direction."
    date: 2006-04-27
    body: "Yeah, I don't like Gnome at all. Long live for KDE!"
    author: "Fermentito"
  - subject: "Re: A step in the right direction."
    date: 2006-04-27
    body: "Yes, without KDE linux looks outdated! eagerly waiting for KDE 4 :)"
    author: "fast_rizwaan"
  - subject: "An additional standard includes Qt 4"
    date: 2006-04-27
    body: "which *additional*standard*?"
    author: "fast_rizwaan"
  - subject: "Re: An additional standard includes Qt 4"
    date: 2006-04-27
    body: "See http://webservices.freestandards.org/en/Download#Optional_modules for the Qt4 module."
    author: "Martijn Klingens"
  - subject: "Roadmap"
    date: 2006-04-27
    body: "The roadmap shows one year between releases of the LSB. Considering the inter-corporate bureaucracy inevitable for standards organizations like this, I'm imagining we will have to wait two years for Qt 4 to be included."
    author: "Brandybuck"
  - subject: "Too bad"
    date: 2006-05-05
    body: "The LSB desktop group used to have some integrity with their stance of \"no strings attached\" which Qt does not conform to.  But, alas, they caved.  Oh well, that was pretty much the deathnail for any chance of desktop linux ever breaking out of hobbyist status."
    author: "Rick"
  - subject: "Re: Too bad"
    date: 2006-05-05
    body: "I second to that. The LSB compliance should have been used to put some pressure on Trolltech to make Qt licensed as LGPL.\n\nQt, unfortunatelly for the KDE project and Linux as a whole,  is not a completely free software, you have to pay a lot for non-free developmnet. Gtk is not limitted by the GPL requirements and thus will be chosen more and more by companies (interested in Linux) despite its low quality.\n\nIn other words, making QT LGPL would blow Gtk away. Not doing so will lead to prevalent choice of Gtk, leading to Linux desktop inferiority and obsolescence.\n\n"
    author: "ZACK"
  - subject: "Qt is GPL and thus Free & Open Source Software"
    date: 2006-05-05
    body: "Qt is GPL'ed on all major platforms (Linux, Windows, and Mac) and is Free and Open Source Software according to the Free Software Foundation (http://FSF.org) and the Open Source Initiative (http://www.OpenSource.org/).\n\nQt is superior to GTK both technically and license-wise. Being licensed under the GNU General Public License (GPL), Qt encourages developers to open up their software and share it with the world. GTK is licensed under the WEAK LGPL and fails in this regard.\n\nSecondly, anyone can make proprietary, closed source software using Qt if they buy a license from Trolltech. If a company doesn't want to give back to the community, they should pay up -- this is absolutely fair.\n\nThe GPL helps spread FREEDOM and is good for Trolltech, who works t to improve Qt for the benefits of users like you and me. Go Trolltech, and stick to the GPL!"
    author: "ac"
  - subject: "Re: Qt is GPL and thus Free & Open Source Software"
    date: 2006-05-05
    body: "LGPL spreads FREEDOM, not GPL.  The GPL is viral and why Gnome is winning the corporate desktop.  It's too bad that KDE had to choose a toolkit with a bad license."
    author: "Rick"
  - subject: "Re: Qt is GPL and thus Free & Open Source Software"
    date: 2006-05-06
    body: "I'd rather have a free-as-in-speech GPL'ed KDE desktop than a proprietary-infested, DRM-crippled GNOME \"corporate desktop\" that's no better than Vista."
    author: "ac"
  - subject: "Re: Qt is GPL and thus Free & Open Source Software"
    date: 2006-05-06
    body: "\"I'd rather have a free-as-in-speech GPL'ed KDE desktop than a proprietary-infested, DRM-crippled GNOME \"corporate desktop\" that's no better than Vista.\"\n\nThat's a pretty selfish attitude that can only hurt the wider adoption of KDE.  The ramifications of Qt being GPL can already be seen.  Novell, Sun, RedHat, Ubuntu..."
    author: "Rick"
  - subject: "Re: Qt is GPL and thus Free & Open Source Software"
    date: 2006-05-06
    body: "It's a pretty selfish attitude to expect Trolltech to release Qt under the LGPL just so that unscrupulous companies can write non-free, proprietary software that locks in users and shackles them with draconian DRM.\n\nTrolltech cares about the USERS' FREEDOM, and by releasing Qt under the GPL they are encouraging other companies to write open-source applications as well. If a company doesn't want to open-source their application, they have to pay Trolltech, who in turn uses that money to improve the open-source Qt framework. Qt under the GPL is a WIN-WIN situation for users of free software like you and me.\n"
    author: "Erik"
  - subject: "Re: Qt is GPL and thus Free & Open Source Software"
    date: 2006-05-07
    body: "\"It's a pretty selfish attitude to expect Trolltech to release Qt under the LGPL just so that unscrupulous companies can write non-free, proprietary software that locks in users and shackles them with draconian DRM.\"\n\nI know Trolltech has fooled a lot of people into thinking it's one big happy KDE/Qt family, but what's in Qt's interests is not necessarily what is in KDE's interests.\n\n\"Trolltech cares about the USERS' FREEDOM,\"\n\nTrolltech cares about making money.\n\n\"If a company doesn't want to open-source their application, they have to pay Trolltech, who in turn uses that money to improve the open-source Qt framework. Qt under the GPL is a WIN-WIN situation for users of free software like you and me.\"\n\nOnce again, you should re-examine your position that it's a win-win for KDE in general.  You do get a kickass toolkit, but you also get the licensing baggage.\n\nLet's put it this way.  KDE will have a hard time growing with Trolltech controlling the toolkit.  RedHat, Novell, Sun and others don't want to say to their customers, \"Hey, we've got this kickass KDE desktop (way superior to Gnome), but you've got this company in Norway that you'll have to pay in order to distribute a KDE/Qt app that is not GPL compatible\".  \n\nRemember, Apple gives away their tools for free and now Microsoft is giving away their Express editions away for free...indefinitely...with no distribution restrictions, no royalties, no licensing fees..\n\nListen, I love KDE, but I have to accept reality too.  Too many people have their head in the sand regarding this issue."
    author: "Rick"
  - subject: "Re: Qt is GPL and thus Free & Open Source Software"
    date: 2006-05-07
    body: "\"Remember, Apple gives away their tools for free and now Microsoft is giving away their Express editions away for free...indefinitely...with no distribution restrictions, no royalties, no licensing fees..\"\n\nReally? Where can I get and run their development tools for free without having to buy their respective operating system first to run them? Or since when do Apple and Microsoft offer cross platform development tools which one could run under a free software OS?"
    author: "ac"
  - subject: "Re: Qt is GPL and thus Free & Open Source Software"
    date: 2006-05-07
    body: "\"Really? Where can I get and run their development tools for free without having to buy their respective operating system first to run them?\"\n\nI didn't say that the operating systems were free, but way to duck the issue.  But , yeah, you know....you buy things in life.\n\n\"Or since when do Apple and Microsoft offer cross platform development tools which one could run under a free software OS?\"\n\nOnce again, you're just bailing out of the conversation.  I guess you have no answer."
    author: "Rick"
  - subject: "Re: Qt is GPL and thus Free & Open Source Software"
    date: 2006-05-07
    body: "\"I didn't say that the operating systems were free.\"\n\nHe's not asking about operating systems. He's asking about your claim that M$ and Apple are giving away developer tools at no cost, a claim for which you have provided absolutely no proof.\n\nAnyway, nothing Apple or M$ gives is free, because you have to buy their DRM-crippled hardware and software. Users are better off buying computers pre-installed with free software such as Linux and KDE:\n\nhttp://UseFree.org/buying"
    author: "ac"
  - subject: "Re: Qt is GPL and thus Free & Open Source Software"
    date: 2006-05-07
    body: "\"Trolltech cares about making money.\" \n \nMaking money by selling Free & Open Source Software under the GPL is absolutely moral. In fact, the Free Software Foundation encourages developers to do because they get money to eat and at the same time do a service to the community: \n \nhttp://www.fsf.org/licensing/essays/selling.html \n \nTrolltech is doing a great service to the community of FOSS users by fostering Qt and making sure it meets the need of KDE. But they don't want their work to be used by unscrupulous companies that leech off of community without giving back. I admire this stance, and I encourage Trolltech to continue sticking with the GPL. \n \n\"RedHat, Novell, Sun and others don't want to say to their customers... you'll have to pay in order to distribute a KDE/Qt app that is not GPL compatible.\" \n \nKDE's customers are its *users*, and users are best off with copyleft free software that they can change to suite their needs. KDE should NOT bend to the coersion by the RIAA, MPAA, and other cartels that are pushing DRM and proprietary software onto the Free Desktop. It is up to developers to protect users from proprietary software by releasing their work under the strongest copyleft license available, the GPL. \n \n\"Apple gives away their tools for free and now Microsoft is giving away their Express editions away for free\" \n \n1) Apple and M$ are not in the business of promoting Free Software; KDE is. \n \n2) M$ will never give away valuable developer tools like Visual Studio, IIS Server, SQL Database etc. What they give out for free is child's play. They make big bucks (ie. thousands of dollars per license) selling those proprietary products."
    author: "ac"
  - subject: "Re: Qt is GPL and thus Free & Open Source Software"
    date: 2006-05-17
    body: "http://msdn.microsoft.com/vstudio/express/visualc/download/\n\nnever say never\n\n"
    author: "Jon"
  - subject: "Re: Too bad"
    date: 2006-05-05
    body: "It's too bad that more people just don't seem to grok that issue.  KDE is technically superior to Gnome, but now we see everybody going with Gnome because they don't want to have their hands tied with a GPL toolkit.\n\nIt's amazing that people don't seem to understand the ramifications of this for KDE.  It's right before their eyes, and they still continue to put their head in the sand.\n\nAnd you're right, Gnome is so far below what's coming up in Vista that it's basically a lost cause for the current linux desktops."
    author: "Rick"
  - subject: "The GPL protects users from draconian DRM"
    date: 2006-05-06
    body: "\"\"\"\nKDE is technically superior to Gnome, but now we see *everybody* going with Gnome because they don't want to have their hands tied with a GPL toolkit.\n\"\"\"\n\nYou mean everybody who doesn't care about bringing freedom to computer users. Many developers are motivated by a desire to empower users to control their own computer, and the GPL is an effective tool in achieving this goal.\n\nThe GPL only ties your hands if you're trying to lock in users, subjugate them with draconian DRM, and avoid giving back to the community. As a developer of Free & Open Source Software, I simply DON'T WANT my work to be used for these unscrupulous purposes. That's why I always license my code under the GPL and encourage everyone to do the same."
    author: "Erik"
  - subject: "Re: The GPL protects users from draconian DRM"
    date: 2006-05-06
    body: "\"You mean everybody who doesn't care about bringing freedom to computer users.\"\n\nNo, I mean the real world where people laugh at made-up definitions of freedom.  Where people want good software and could care less about source code.\n\n\"Many developers are motivated by a desire to empower users to control their own computer, and the GPL is an effective tool in achieving this goal.\"\n\nDevelopers code for themselves because they love programming and want recognition for their programming efforts.  It has nothing to do with handy-wavy \"empowerment\".  99.9999% of the people in the world could care less about source code and never will.  It's an appliance to them.  One of these days you'll be able to sit back and have a little perspective.\n\n\"The GPL only ties your hands if you're trying to lock in users, subjugate them with draconian DRM, and avoid giving back to the community.\"\n\nWeird FSF/Stallman-speak that means nothing to the real world.\n\n\"As a developer of Free & Open Source Software, I simply DON'T WANT my work to be used for these unscrupulous purposes. That's why I always license my code under the GPL and encourage everyone to do the same.\"\n\nAlso, as a developer, I might be tempted to license my software under the GPL, but then I have to consider the ramifications of my license choice.  If it's an application, then the ramifications are not as great if I choose GPL.  If it's a library, then the ramifications are much bigger because If my desire is also have people use it, then I'm limiting my audience by choosing straight GPL."
    author: "Rick"
  - subject: "Re: The GPL protects users from draconian DRM"
    date: 2006-05-06
    body: "\"Where people want good software and could care less about source code.\"\n\nProprietary, non-free, DRM-crippled software that forces you to upgrade and view pop-up ads is not my idea of good software. Only open-source software gives users control over their own computers -- everything else can't be trusted. The GPL ensures that all derivative works will be open-source, and that's why it is the best license from a user's perspective.\n\n\"Developers code for themselves because they love programming and want recognition for their programming efforts.\"\n\nWrong, FOSS developers care about the USERS' freedom because they themselves are users and don't want to be shackled by proprietary software and Digital Restrictions Management. You're trying to coax developers by appealing to their pride -- this is a known tactic also adopted by the XFree Consortium, and a trap that free software developers need to be wary of:\n\nhttp://www.gnu.org/philosophy/x.html\n\n\"If it's a library, then the ramifications are much bigger\"\n\nThe Free Software Foundation, authors of the LGPL, warn developers NOT to use it, but rather to use the GPL for all software including libraries:\n\nhttp://www.gnu.org/licenses/why-not-lgpl.html"
    author: "Erik"
  - subject: "Re: The GPL protects users from draconian DRM"
    date: 2006-05-07
    body: "\"Proprietary, non-free, DRM-crippled software that forces you to upgrade and view pop-up ads is not my idea of good software. Only open-source software gives users control over their own computers -- everything else can't be trusted. The GPL ensures that all derivative works will be open-source, and that's why it is the best license from a user's perspective.\"\n\nDRM is a problem, but Linux is not immune - not in the least bit.  Have you been following the comments of Torvalds and GPL v3?  Linux is going to get DRM too.  Remember DRM is not just for megalo-corp that wants to screw you.  It's also for individuals protecting their property rights.\n\n\"Wrong, FOSS developers care about the USERS' freedom because they themselves are users and don't want to be shackled by proprietary software and Digital Restrictions Management. You're trying to coax developers by appealing to their pride -- this is a known tactic also adopted by the XFree Consortium, and a trap that free software developers need to be wary of:\"\n\nSure, they want something that they can use too, but if it was something that wasn't fun, then they wouldn't be doing it because typically they're not getting paid.  I have no idea what XFree Consortium tactic you're talking about.  Is it the advertising clause or something?\n\n\"The Free Software Foundation, authors of the LGPL, warn developers NOT to use it, but rather to use the GPL for all software including libraries:\n\nhttp://www.gnu.org/licenses/why-not-lgpl.html\"\n\nThe FSF is irrelevant to my licensing decisions.  In fact, I'll tend to look towards the exact opposite advice they give."
    author: "Rick"
  - subject: "Re: The GPL protects users from draconian DRM"
    date: 2006-05-07
    body: "\"Have you been following the comments of Torvalds and GPL v3?\"\n\nHave you been following Torvalds' support for KDE? He doesn't seem to be bothered by the fact that KDE is Free & Open Source and licensed under the GPL:\n\nhttp://linux.slashdot.org/article.pl?sid=05/12/13/1340215\n\n\"DRM ... is also for individuals protecting their property rights.\"\n\nThis is FUD straight from entertainment cartels. Artists are themselves users and audiences for other people's works, and many of them are just as appalled by DRM as anybody else:\n\nhttp://www.downhillbattle.org/\n\n\"I have no idea what XFree Consortium tactic you're talking about.\"\n\nThe XFree Consortium defined success by the number of computer companies that used the X Window System. This definition put the computer companies in the driver's seat. Whatever they wanted, the X Consortium had to help them get it.\n\nComputer companies normally distribute proprietary software. They wanted free software developers to donate their work for such use. If they had asked for this directly, people would have laughed. But the X Consortium, fronting for them, could present this request as an unselfish one. ``Join us in donating our work to proprietary software developers,'' they said, suggesting that this is a noble form of self-sacrifice. ``Join us in achieving popularity,'' they said, suggesting that it was not even a sacrifice.\n\nBut self-sacrifice is not the issue: tossing away the defense that copyleft provides, which protects the freedom of the whole community, is sacrificing more than yourself.\n\nhttp://www.fsf.org/licensing/essays/x.html"
    author: "Larry"
  - subject: "The GPL is the best license for Qt and KDE"
    date: 2006-05-05
    body: "The GPL is a Free and Open Source license, and it is the best (optimal) choice for Qt and KDE because it encourages developers to give back to the community:\n\nhttp://blogs.qtdeveloper.net/archives/2006/04/30/on-the-lessor-gpl/\n\nIf you want to make proprietary, closed-source applications, Trolltech has a program that provides discounts for up to three proprietary Qt licenses:\n\nhttp://www.trolltech.com/products/qt/smallbusiness.html"
    author: "Erik"
  - subject: "Gnome LGPL zealots are sell-outs and weaklings!"
    date: 2006-05-05
    body: "The gnome zealots that are pushing for the lgpl are SELL-OUTS or plain WEAKLINGS in the face of big-business interests that want to lock in users and rip them off with DRM-infested proprietary software.\n\nAs far as I\u0092m concerned, the Linux desktop should remain free and insulated from proprietary, DRM-crippled apps that work against the user. Go GPL!"
    author: "Larry"
  - subject: "Re: Gnome LGPL zealots are sell-outs and weaklings"
    date: 2006-05-05
    body: "\"The gnome zealots that are pushing for the lgpl are SELL-OUTS or plain WEAKLINGS in the face of big-business interests that want to lock in users and rip them off with DRM-infested proprietary software.\n\nAs far as I\u0092m concerned, the Linux desktop should remain free and insulated from proprietary, DRM-crippled apps that work against the user. Go GPL!\"\n\nNice, with your suggestion, the linux desktop will always remain in hobbyist-land."
    author: "Rick"
  - subject: "Re: Gnome LGPL zealots are sell-outs and weaklings"
    date: 2006-05-06
    body: "\"Nice, with your suggestion, the linux desktop will always remain in hobbyist-land.\"\n\nThe Linux desktop has already overcome the hobbyist-only stage. There are plenty of professional KDE apps that are licensed GPL and that are used by millions of people throughout the world. Just look how many computer manufacturers sell Linux desktops and laptops pre-installed:\n\nhttp://usefree.org/buying/\n\nWe didn't need to compromise out ideals to get this far, and we're certainly not going to start compromising them now."
    author: "Larry"
  - subject: "Re: Gnome LGPL zealots are sell-outs and weaklings"
    date: 2006-05-06
    body: "\"The Linux desktop has already overcome the hobbyist-only stage.\"\n\nNo it hasn't.  There's a little bit of desktop linux for call-center type workstations, where the browser is the major application hosting environment, but not as much when you get into higher-level analyst type positions.  I sell linux software and hardware to lots of people from a wide variety of industries and I ask them what they are running on their desktops, and invariably its still windows.  And these are sys/admin, IT types too.\n\n\"There are plenty of professional KDE apps that are licensed GPL and that are used by millions of people throughout the world.\"\n\nProfessional?  See the problem is that as you get higher-up the software stack, the harder it is for free software to compete.  It might get their one day, but in the meantime having some GPL-holier-than-thou attitude only stymies the wider adoption of free systems.\n\n\"ust look how many computer manufacturers sell Linux desktops and laptops pre-installed:\"\n\nCompared to windows, it's completely insignficant.\n\n\"We didn't need to compromise out ideals to get this far, and we're certainly not going to start compromising them now.\"\n\nThe farther you go towards Stallman's philosophy of things, the further you alienate users.\n\nIt's ok to be an optimist, but let's be realistic here.  Desktop Linux has a hard road ahead of it.  It's still stuck in low single digits in market penetration, the fragmentation between Gnome and KDE has hurt desktop linux big time, and whether you like Microsoft or not, Vista will most likely be a fairly decent operating system, and at the very least have a very, very powerful graphics system with WPF and the new graphics driver model.\n\n"
    author: "Rick"
  - subject: "Re: Gnome LGPL zealots are sell-outs and weaklings"
    date: 2006-05-06
    body: "\"I sell linux software and hardware to lots of people from a wide variety of industries and I ask them what they are running on their desktops, and invariably its still windows.\"\n\nI agree that the Free Software community has done a poor job advertising the importance and technical superiority of Free Software. Not to mention the fact that Micro$oft has billions of dollars at their disposal to create artificial hype about their sucky products while at the same time smearing Linux and KDE.\n\n\"Compared to windows, it's completely insignficant.\"\n\nAnybody in the world can buy performant, fully functional desktop computers that come preinstalled with Linux and KDE. Users can do everything with their Linux/KDE desktop that they can do with Windows, and more! This is a realization of the struggle for Free Software and a testament to the importance of the GPL."
    author: "Larry"
  - subject: "Re: Gnome LGPL zealots are sell-outs and weaklings"
    date: 2006-05-07
    body: "\"I agree that the Free Software community has done a poor job advertising the importance and technical superiority of Free Software. Not to mention the fact that Micro$oft has billions of dollars at their disposal to create artificial hype about their sucky products while at the same time smearing Linux and KDE.\"\n\nDon't be so sure.  XP is stable with the NT kernel, but has userspace (mostly browser) issues.  Vista will have a new graphics system that absolutely blows away X.  The Xorg developers know this, read the mailing list.  Dismissing Vista is a recipe for disaster.\n\n\"Users can do everything with their Linux/KDE desktop that they can do with Windows, and more!\"\n\nAnd anybody can download an ISO and install it, but that doesn't mean much.  And you can have a local PC shop install some distro on your PC, but that doesn't mean much either.  Get back to me when Dell is pushing pre-installed linux on their PCs.\n\n\"This is a realization of the struggle for Free Software and a testament to the importance of the GPL.\"\n\nI have no idea where you get the idea that the GPL had anything to do with the technical excellence of KDE.  It was developers writing code.  The license didn't write the code."
    author: "Rick"
  - subject: "Re: Gnome LGPL zealots are sell-outs and weaklings"
    date: 2006-05-07
    body: "\"Get back to me when Dell is pushing pre-installed linux on their PCs.\"\n\nDell is selling open source software that gives you the choice of what operating system to install:\n\nhttp://www1.us.dell.com/content/topics/segtopic.aspx/e510_nseries\n\nThere are a least 20 manufacturers that sell desktops and laptops pre-installed with Linux and KDE:\n\nhttp://usefree.org/buying\n\n\"The license didn't write the code.\"\n\nThe GPL ensures that open source software licensed under it will always remain free, an assurance that has motivated many developers:\n\nhttp://www.gnu.org/philosophy/why-copyleft.html"
    author: "ac"
---
Yesterday the <a href="http://www.linuxbase.org">LSB Workgroup</a> released the first version of the LSB-Desktop specification. Qt and Gtk+ are part of this new specification which is another step towards standardizing the Linux Desktop. The <a href="http://www.kde.org">KDE project</a> and <a href="http://www.trolltech.com">Trolltech</a> are working closely with the LSB Workgroup to have KDE itself accepted in future LSB-Desktop releases when the standard further evolves. Read on for the full press release.



<!--break-->
<p><i>KDE's core library - Qt - included in new LSB desktop standard
"ISO standard for Linux extended with desktop specifications"</i></p>

<p>DATELINE April 25, 2006</p>

<p><b>FOR IMMEDIATE RELEASE</b></p>

<p><b>April 25, 2006 (The Internet)</b> - <a href="http://www.kde.org">KDE</a>®, the open source project that creates user-friendly desktop software, announced today that its core library Qt® from <a href="http://www.trolltech.com">Trolltech</a>® is part of the Linux Standard Base (LSB) Final Release 3.1 standards specification document. The new LSB desktop standard includes the Qt 3.3 libraries and the tools required to create Qt-based applications, both of which are the foundation of KDE. An additional standard includes Qt 4, the next generation technology which will be used in the upcoming KDE 4 release.</p>

<p>The LSB, a workgroup of the Free Standards Group organization, works to develop through consensus a standard operating environment for the Linux platform. This allows third-party application developers to ship a single version of their application that works on all LSB-compliant systems, regardless of the Linux distribution. With the inclusion of Qt in the LSB desktop specifications many desktop applications will now benefit.</p>

<p><i>"KDE wants to prevent fragmentation of the Linux Desktop market,"</i> stated Olaf Schmidt, a KDE representative in the LSB Workgroup. <i>"Collaboration is currently one of the hot topics in the KDE community, and this is why our participation in the LSB makes a lot of sense."</i> Other efforts supported by KDE include the OSDL Portland project, which aims to make it easier for applications to integrate well into all Linux distributions and desktops, and freedesktop.org, where KDE cooperates with other open source projects on common, open interfaces.</p>

<p><i>"KDE has always been a strong supporter of open standards"</i>, Schmidt added. <i>"Our KOffice developers have made key contributions to the OpenDocument standard. Code from Konqueror, our standards compliant web browser, has been adopted by Apple and Nokia. We are also active in the Accessibility Workgroup of the Free Standards Group, where we make Linux more accessible to users both with and without disabilities."</i></p>

<p>The LSB Desktop standard ensures that KDE is supported through the inclusion of its foundation library Qt in the specification. Trolltech and KDE are both represented in the LSB Workgroup and will continue to work with the Workgroup to ensure that future products including, but not limited to, KDE itself will meet the requirements for inclusion in subsequent LSB specifications.</p>

<p><i>"The LSB workgroup is pleased to see the participation in and support for LSB 3.1 from the KDE project"</i>, said Ian Murdock, chief technology officer of the Free Standards Group. <i>"Since our new standard now includes support for desktop libraries, it's crucial that KDE and other desktop projects participate in the process. We are all working toward the same goal: expansion of applications on the Linux desktop and continued success for Linux. This release is testament that we are well on our way of achieving that goal."</i></p>

<p>Ends

<p>For more information see press contacts listed at the bottom of this release.</p>


<p><b>Supporting KDE</b></p>

<p>KDE is an open source project that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether its help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the Supporting KDE page for further information.</p>

<p><b>About The KDE Project</b></p>

<p>The KDE® project consists of hundreds of developers, translators, artists and other contributors worldwide collaborating over the Internet. The community creates and freely distributes a stable, integrated and free desktop and office environment. KDE provides a flexible, component-based, network-transparent architecture and powerful development tools, offering an outstanding development platform. Reflecting its international team and focus, KDE 3.5 is currently available in over 80 different languages.</p>

<p>KDE, which is based on Qt technology from Trolltech, is working proof that the Open Source "Bazaar-style" software development model can yield first-rate technologies on par with and superior to even the most complex commercial software.</p>

<p><b>Trademark Notices.</b></p>
 KDE, K Desktop Environment and the KDE Logo are trademarks or registered trademarks of KDE e.V. in the European Union, the United States and other countries. Trolltech and Qt are registered trademarks of Trolltech AS.  Linux is a registered trademark of Linus Torvalds. All other trademarks are the property of their respective owners.

<p><b>Press Contacts</b></p>

<pre>
Africa
 Uwe Thiem
 P.P.Box 30955
 Windhoek
 Namibia
 Phone: +264 - 61 - 24 92 49
 info-africa kde.org

Asia
 Sirtaj S. Kang 
 C-324 Defence Colony 
 New Delhi 
 India 110024 
 Phone: +91-981807-8372 
 info-asia kde.org 

Europe
 Matthias Kalle Dalheimer
 Rysktorp
 S-683 92 Hagfors
 Sweden
 Phone: +46-563-540023
 Fax: +46-563-540028
 info-europe kde.org

North America
 George Staikos 
 889 Bay St. #205 
 Toronto, ON, M5S 3K5 
 Canada
 Phone: (416)-925-4030 
 info-northamerica kde.org
 
Oceania
 Hamish Rodda
 11 Eucalyptus Road
 Eltham VIC 3095
 Australia
 Phone: (+61)402 346684
 info-oceania kde.org

South America
 Helio Chissini de Castro
 R. José de Alencar 120, apto 1906
 Curitiba, PR 80050-240
 Brazil
 Phone: +55(41)262-0782 / +55(41)360-2670
 info-southamerica kde.org
</pre>











