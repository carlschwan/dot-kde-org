---
title: "KOffice 2 Design Competition Deadline Approaches"
date:    2006-02-04
authors:
  - "wolson"
slug:    koffice-2-design-competition-deadline-approaches
comments:
  - subject: "Approch : Document centric or Format Centric ???"
    date: 2006-02-03
    body: "lets see how many clones we will see..\n\n\ni am for a document centric approch (like iWorks , or Latex) - not an Approch where you are formatting all things all the time like Kword/OOO/Ms-Office\n\nWhats you opinion on this?\n\nchris\n\n\n\n\n(ps.: i like latex)"
    author: "chris"
  - subject: "Re: Approch : Document centric or Format Centric ?"
    date: 2006-02-03
    body: "\"lets see how many clones we will see\"\n\nLet's see how many wierd and unworkable interfaces we'll see as well...\n\nWeaning people off of formatting will be very difficult. The use of styles and templates needs to be encouraged. I cut my document teeth on FrameMaker, and while it was difficult to set up style or template, once you had them actually writing the document was a snap. Our productivity plummeted at work when we switched to MS Word. I've also used Lotus SmartSuite extensively. Its still page/formatting oriented like OOO/MSOffice, but does a much better job with styles.\n\nA good approach would be focus on styles in the interface and provide good default document templates. Discourage direct formatting of the document, and encourage the use of paragraph and character styles. I would even warn the user on problematic style usage, such as out of sequence heading styles or overlapping character styles.\n\nJust my thoughts."
    author: "Brandybuck"
  - subject: "Re: Approch : Document centric or Format Centric ?"
    date: 2006-02-03
    body: "\"A good approach would be focus on styles in the interface and provide good default document templates. Discourage direct formatting of the document, and encourage the use of paragraph and character styles. I would even warn the user on problematic style usage, such as out of sequence heading styles or overlapping character styles.\"\n\nBingo. I think this is the right approach. Proper use of styles and style inheritance in a standard word processor can be quite effective, if not quite as programmable and flexible as TeX.\n\nI don't know how it would be done. Novice (read: 95% of) word processor users are impossible to wean off reaching for their beloved 'Italic' and 'Bold' Buttons."
    author: "Robert"
  - subject: "Re: Approch : Document centric or Format Centric ?"
    date: 2006-02-03
    body: "Since most novice-user documents use bold, italics and underlining for almost everthing, especially where it breaks all rules of typography I would want to make use of these 3 buttons as impossible as possible.\n\nThe semantic approach of LaTeX should be much more emphesized. One approach could be to replace 'italics' by 'emphasize', 'bold' with 'strong emphasize' and 'underline' with 'make link' (and really nothing else!). There could still be a Text Format Option wich enables everthing else. But one could think of allowing text-format changes only in terms of styles - like it is done in HTML with CSS tmeplates.\n\nMy personal opinion (and the reason why I prefer semantic script languages)\nMatthias"
    author: "Pospiech"
  - subject: "Re: Approch : Document centric or Format Centric ?"
    date: 2006-02-03
    body: "> Since most novice-user documents use bold, italics and underlining for almost everthing, especially where it breaks all rules of typography I would want to make use of these 3 buttons as impossible as possible.\n\nWhy do you want to make life for those users harder than it needs to be? Because you know what's good for them? I love LaTeX as much as the next guy, and I use it for all scientific work, but if I just want to write a simple letter I'll use KWord, and I even dare to use those three buttons..."
    author: "ac"
  - subject: "Re: Approch : Document centric or Format Centric ?"
    date: 2006-02-03
    body: "The word 'styles' should not be used when talking about word processing. If I'm writing a letter, article or email I want to get a message across in words. Some parts of the text serve to grasp the contents off the subsequent larger part. These parts are called headings. Headings are not a style: they are way of structuring a text.\n\nSo instead of talking about 'the style heading' one should talk about 'the meaning heading'. LyX is promoted as WYSIWYM (what you see is what you mean).\n\nAnd about bold and italic: they are not just styles, they add meaning to a text.\nThis in contrast to changes in font. The font selector in word processors is the most annoying and counterproductive GUI element by far.\n\nI think KOffice should make a strict separation between content and style by having a clear switch of GUI between the two concepts. So one LyX similar interface and one interface where the current document is shown along with buttons, lists, whatever that let you change the style of the indivudual element s uniformly.\n\n\n\n\n\n"
    author: "Jos"
  - subject: "Re: Approch : Document centric or Format Centric ?"
    date: 2006-02-03
    body: "Great idea! The toolbar of the LyX-like editor should include just the style elements included in the current document type definition. So when writing a scientific article I could have a drop-down list of references from my bibliography database right in the toolbar. There would also be an \"insert figure\" button that inserts a figure already formatted according to the style of the article.\n\nThis LyX-like editor part is WYSIWYM.\n\nIn addition, there should be a document style designer that lets you adjust things like the spacing after bullets on the third level of itemization as well as to define entirely new document types. When the style designer is opened, the editor window shrinks so that the style designer can dock next to it. You can click on items in the editor part and the corresponding definitions are opened in the style designer. Style changes in the latter immediately update the appearance of the document.\n\nThis style designer part is WYSIWYG."
    author: "Martin"
  - subject: "Re: Approch : D or C -> iWork"
    date: 2006-02-03
    body: "I just want to point out the Apple Guys have done a great job with their iWork (i think that was their word-processor)\n\nyou can make a style centric word processor still WYSIWYG !\n\nif you want to format your document , the UI changes into a style editor.\n\nback to the text editor you have fullscreen-text view... very nice\n\n(i think thats how iWork works)\n\n:-)\n\n"
    author: "chris"
  - subject: "Re: Approch : D or C -> iWork"
    date: 2006-02-04
    body: "Isn't this quite close to the idea on M$ Office \"12\" that has that changing toolbar (changes on data under the cursor to options available for that particular data)?"
    author: "Nobody"
  - subject: "Re: Approch : D or C -> iWork"
    date: 2006-02-04
    body: "no i am voting for an extra Style Editor and Text Editor , and you witch between the 2\n\n"
    author: "chris"
  - subject: "Re: Approch : Document centric or Format Centric ?"
    date: 2006-02-05
    body: "> In addition, there should be a document style designer that lets you adjust\n> things like the spacing after bullets on the third level of itemization as\n> well as to define entirely new document types.\n\nThat already exists; its the style manager (format menu) :)\n\n> When the style designer is opened, the editor window shrinks so that the \n> style designer can dock next to it. You can click on items in the editor part\n> and the corresponding definitions are opened in the style designer.\n\nGood idea, but requires a lot of optimalisations in the current workflow since it would simply be too slow with the current engine.\n\n> Style changes in the latter immediately update the appearance of the\n> document.\n\nDito!\n\n"
    author: "Thomas Zander"
  - subject: "Two different use cases"
    date: 2006-02-06
    body: "Many documents do not need styles.   The bold/italic (underline is still evil) buttons are all they need.  \n\nWhile easy to abuse, it is handy to put something in bold in my letter to mom once in a while.  Would you use a complex style for a 1 page letter?  I wouldn't.  I don't think anyone else would either.\n\nBy contrast my business memo should be in the corporate style, and so the bold/italic buttons should be banned.   When I need bold I should switch to the pre-defined style \"hazardous chemical warning\" style, so that a script can parse the memo before I print it, sending the memo by leagal to ensure it meets the legal requirements for the warning. \n\nThe uses are different, so we can allow two different modes: one for simple 'styleless' letters, and one for complex styled documents."
    author: "bluGill"
  - subject: "Re: Two different use cases"
    date: 2006-02-06
    body: "One could introduce classes of documents to achieve this. For different documentclasses the toolbar could even change.\n"
    author: "Pospiech"
  - subject: "Re: Approch : Document centric or Format Centric ???"
    date: 2006-02-03
    body: "My opinion as member of the jury is: show me your entry. Please! We're not fishing for something, we genuinely want to receive cool, weird, simple, logical, absurd, inventive, practical ideas. Pick two of the previous, explain them well, and you make a good chance of getting the prize.\n\nGet your entries in before it's too late! "
    author: "Boudewijn Rempt"
  - subject: "Re: Approch : Document centric or Format Centric ???"
    date: 2006-02-06
    body: "Keep kword for doing the approach it does - believe it or not, some people prefer that kind of model, and they should be catered for. By all means suggest the addition of a latex-type editor to koffice, but it shouldn't replace the current kword model."
    author: "mikeyd"
  - subject: "Re: Approch : Document centric or Format Centric ?"
    date: 2006-02-08
    body: "I agree, we need to try to get people off the Bold and Italics buttons.  I suggest a pop-up assistant which, erm, pops-up when the user tries to format a document themselves and politely suggests that the user should use one of the predefined styles instead.  This \"assistant\" could come in any number of amusing forms, like a talking paper-clip for example or a mad professor!  For instance, if the user starts to write a letter, the assistant could pop up and say \"I think you're writing a letter.  Let me format it for you!\"  Ta da! Or another example, if the user starts with \"Dear Bill...\" the assistant could pop up and say, \"I have detected that you're writing an Anti-Microsoft satirical posting to slashdot.  Let me delete that for you and instead post some random Anti-Linux FUD!\".\n"
    author: "Timothy Collins"
  - subject: "Latex vs. KOffice"
    date: 2006-02-03
    body: "What you forgot to mention is that Latex is horrible broken. It consist of many hacks and is a universe you have to approach from inside. It is very difficult to enter from outside into the Latex universe. Latex has its bugs and you can resolve them by adding an additional package which users have to be aware of. \n\nJust one example: Ever tried to refer to newspaper articles or web ressources in BibTex? I wanted to create a bibliography with Bibtex, ran into known problems and doing it with pure Latex was no good idea either and then switched over to OO.org and did it in half an hour. \n\nbold, italic, underline are just for emphasis. I do not care if others misused these styles. I do not want to change the world but produce my own documents in good quality.\n\nIntrestingly those who promote freedom of choice on the one hand, e.g prefer C++ which enables you to write broken and buggy code requiring artistic discipline and reject reduced languages like Java or even Visual Basic, prefer restriction when it comes to document design on the other hand.\n\nI don't need freedom provided I get better results.\nI need freedom provided I want to get better results.\n\nLaTex has some advantages when it comes to scientific document production and this is an area where Word has huge problems. Latex also has very good standard templates. So where are good KWord templates?"
    author: "Andre"
  - subject: "Re: Latex vs. KOffice"
    date: 2006-02-04
    body: "btw. there are some @ www.kde-files.org "
    author: "chris"
  - subject: "Re: Latex vs. KOffice"
    date: 2006-02-04
    body: "BTW, you can test the current Beta very easily now via klik:\n\n  klik://kword-1.5-beta\n\nis the secret key to KWord beta testing happiness.\n\nThere you'll see that there are already some *very* nice templates shipping with KWord. They are even displayed in the \"File --> New\" dialog.\n\nWhich does not mean that the quota is full, and that the KOffice developer would veto any additional templates to ship. There could well be another 5 dozen nicely done templates there IMHO. I'm working on one right now...\n\nSo where is  _your_   _own_  template that could be included in KWord? Just one? Would you be so kind to put some of your valuable time into where your mouth is? Otherwise, \"talk is cheap\".\n"
    author: "klik-er"
  - subject: "Re: Latex vs. KOffice"
    date: 2006-02-04
    body: "The fact that LaTeX is broken is exactly the reason why some of us are arguing for WYSIWYM in KWord. LaTeX has both saved us and wasted us huge amounts of time. It has saved us time when we have been able to type in an entire *book* full of figures, tables, equations, cross-references, bibliographic references, indices, chapter title pages etc. without worrying about formatting one bit. It has wasted us time for the reasons that you state and others.\n\nWhile LaTeX is broken, WYSIWYM is not. In fact, while LyX tries to be WYSIWYM, the underlying LaTeX is pretty low-level stuff (and the underlying TeX even more so). This is one reason why it is broken.\n\nBoudewijn is right though: Someone should put together a document-centric entry for the competition. (Even though it seems more like material for a PhD thesis to me, but don't be discouraged.)"
    author: "Martin"
  - subject: "Re: Latex vs. KOffice"
    date: 2006-02-05
    body: "Kile?\nPlease also mind that there are plans for a new Latex version.\n\nNote: this competition is apparently about interface dsign for KOffice, not about changing Koffice as a whole.\n\nA modern approach would be an easy XML-style representation.\n\nWYSIWYM - your document in a kindof XML\n<type>article</type>\n<author>Jack Hacker</author>\n<author>Lord British</author> \n<title>My Scientific Article</title>\n<tableofcontents>\n<abstract>The future of LaTex is XML.</abstract>\n<h1>Introduction<h1>\nbla \n...\n<h1>History</h1>\n<h2>LaTex</h2>\nToday LaTex is the <emph>defacto</emph> standard for scientific papers.\n...\n<h1>conclusion</h1>\nbla\n<bibliography src=\"mybib.bib\">\n--> then converted by a tool using a style sheet to ODF, PS or PDF.\n"
    author: "aranea"
  - subject: "Re: Latex vs. KOffice"
    date: 2006-02-05
    body: ">Please also mind that there are plans for a new Latex version.\nIF you mean LaTeX 3, then I have to say that it is not even sure that it is going to be released this century. It could depend on the release of the TeX replacement ExTeX.\n\n>A modern approach would be an easy XML-style representation.\nWhich is in prinziple not much different from LaTeX, just that LaTeX is more readable than XML, wheras XML is better parsable for the computer. But no matter which language is better we do not want people to use code in kword anyway, do we ?\n"
    author: "Pospiech"
  - subject: "Re: Latex vs. KOffice"
    date: 2006-02-07
    body: "I have been using KWord for a long time now. It's good for short texts with little formatting. But as soon as you start to use more complex things such as paragraph headline markup etc, you run into problems. \n\nI really don't want to be insultive: But concerning stability, KWord is currently no better than Winword 2.0 back in 1993... or even worse! Nevertheless I love the approach because we need an office application in the KDE framework. This is why I will continue to use it and I will also continue to report bugs. \n\nThough, free software profits from code quality and users will be attracted through scalability and stability. And a nice design is no so important in my eyes. We'll have enough good press if one single scientist decides to write his dissertation in KWord. Up to now I won't risk this, since longer texts with headline markup and 2-3 rewind and redo operations can kill your day's work. \n\nFor this reason I would like to ask all our beloved developers to please concentrate on the bug database and please not waste their time in surface polishing!! I need good, working code. I don't need eyecandy!"
    author: "Markus Heller"
  - subject: "Re: Latex vs. KOffice"
    date: 2006-02-07
    body: "Thomas and David have really worked hard on stability for this release of KWord. We're not there, but the improvement is marked: when I started using KWord for some professional work myself I would often report three crashes in a single day, but most of those are gone."
    author: "Boudewijn Rempt"
---
Late December the <a href="http://www.koffice.org/">KOffice</a> team announced a <a href="http://dot.kde.org/1135283071/">design competition for KOffice 2</a>.  A prize of $1000 USD will be given to the best entry as determined by a panel of judges.   The deadline for submissions is now only 2 weeks away.  Several quality proposals have been submitted, but there is always room for more.  So if you have been considering an entry until now, please read the <a href="http://www.koffice.org/competition/guiKOffice2.php">guidelines</a> for objectives, submission formats and examples.  For those already working on entries, please review entry requirements and make sure to meet the deadline.  Questions on the competition can be directed to the <a href="http://www.koffice.org/contact/">KOffice mailing lists</a> or on the #koffice IRC channel.




<!--break-->
