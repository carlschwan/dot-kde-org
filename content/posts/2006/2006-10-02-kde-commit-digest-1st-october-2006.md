---
title: "KDE Commit-Digest for 1st October 2006"
date:    2006-10-02
authors:
  - "dallen"
slug:    kde-commit-digest-1st-october-2006
comments:
  - subject: "why"
    date: 2006-10-02
    body: "I couldn't find out why KPersonaliser has been removed.  It's always the first thing I run to do undo the custom themes that my distribution does to KDE.  I like a pure KDE look n feel and customisation and KPersonalizer always fixes that for me.\n\nIf you remove it, then what's the replacement?"
    author: "KDE User"
  - subject: "Re: why"
    date: 2006-10-02
    body: "kiosktool\nhttp://www.kde-apps.org/content/show.php?content=12028"
    author: "name"
  - subject: "Re: why"
    date: 2006-10-02
    body: "The replacement is the 'defaults' buttons in kcontrol."
    author: "button"
  - subject: "Re: why"
    date: 2006-10-02
    body: "'default' button does not provide choice.\nactually KPersonaliser offered an easy way to setup mouse click settings the way I am most used to - Microsoft Windows style (double click on title bar maximizes the window etc.) I just hope they include some kind of quick settings in KControl for switchers - one click and your mouse behaves similarily to whatever you used before - Windows, Mac OS, GNOME etc."
    author: "stripe4"
  - subject: "Re: why"
    date: 2006-10-02
    body: "if you remove something you will *ALWAYS* find someone who needs and uses the feature removed :-)))))"
    author: "c"
  - subject: "Re: why"
    date: 2006-10-02
    body: "What was the rationale for removing it?  I can't find any mention of it on kde-devel...\n"
    author: "Leo S"
  - subject: "Re: why"
    date: 2006-10-03
    body: "I did see it somewhere... Rationale went like this:\nDistros always bypass it anyway, and roll their own tunes. Distros know better than users. Since it's never used, why bother?\n\nOr, something like that."
    author: "Suslik"
  - subject: "Re: why"
    date: 2006-10-03
    body: "well, i used it as well - every time i did a fresh install. i think it really made sense to ask a user how he/she wanted KDE to behave. Maybe this can be on the desktop the first time a user starts KDE, if kpersonalizer was to intrusive (blocking the first login)."
    author: "superstoned"
  - subject: "Re: why"
    date: 2006-10-04
    body: "Yeah, I always used it on a new install.  Quick and dirty way to make most of the basic changes I like.  I'm assuming the choices themselves aren't gone, their existence is one of the main reasons I use KDE over Gnome.  \n\nI know it's cultishly popular right now to hate on choice, esp on the G side, but why is that attitude creeping into KDE?  KDE's done just fine up until now.\n\nAnyway, even if people are dead set on hiding choices, this seems wrong headed.  Kpersonalizer changes some very fundamental behaviors, behaviors people expect from years of habit, and it changes them in an easy way.  Now, if people want to change them, they get to wade though the mess of kcontrol all the choice haters seem so especially ashamed of.  I don't get it."
    author: "MamiyaOtaru"
  - subject: "New web site"
    date: 2006-10-02
    body: "I have a note about the new KDE.ORG web site:\nthere is no more the \"visited link\" colour!\nI relied on it for years to know what news I had already read and which ones, instead, were new.\nIs this a \"feature\" or a bug?"
    author: "Alessandro"
  - subject: "Re: New web site"
    date: 2006-10-02
    body: "Very true!\nI suppose, it is supposed to be a feature: For some web designers, different colors for hyperlinks may look -well- inconvenient. I also recommend to use a different stylesheet for the apps-feeds and the dot news where the 'visited link' color is different from the default text color. "
    author: "sebastian"
  - subject: "Workflow!"
    date: 2006-10-02
    body: "Workflow sounds lige a really nice addition to the KDE desktop! Very powerful and at the same time, pretty easy to use. "
    author: "Joergen Ramskov"
  - subject: "Re: Workflow!"
    date: 2006-10-02
    body: "I agree with you!\nVery nice application!"
    author: "Alessandro"
  - subject: "Tribute to Apple..."
    date: 2006-10-02
    body: "...where it's due: Apple did exactly this already and called it 'Automator'.\n\nHave a look at\n\nhttp://developer.apple.com/macosx/images/automator.jpg\n\nand compare that with\n\nhttp://www.kde-apps.org/content/preview.php?preview=2&id=43624&file1=43624-1.png&file2=43624-2.png&file3=43624-3.png&name=WorKflow\n\nI dare to claim this was at least inspired by Automator (I actually think it's a 1:1 clone) and this should be mentioned somewhere. I did not see any such tribute to Apple on the kde-look.org site.\n"
    author: "Frerich"
  - subject: "Re: Tribute to Apple..."
    date: 2006-10-02
    body: "Well, congratulations for figuring this out! A thing I want to add though. The pages at kde-apps.org are not intended to bore people with irrelevant things, which is why I kept the description as short as possible. Furthermore, the text in the commit digest refers to my original SoC proposal at\n\nhttp://commit-digest.org/issues/2006-10-01/files/workflow.txt\n\nin which Automator is tributed all over the place.\n\nAnd, no, it's not a 1:1 clone. Since it's built on top of KDE, it includes certain features out of the box (like network transparency and IPC). It also contains some flow control features which automator lacks. That's why I didn't mention Automator."
    author: "Thomas Kadauke"
  - subject: "Re: Tribute to Apple..."
    date: 2006-10-03
    body: "> The pages at kde-apps.org are not intended to\n> bore people with irrelevant things\n\nI didn't know about the Automator from Apple (or even this kde version) and I personally don't find boring at all talking about desktop environment related stuff on a desktop environment forum."
    author: "blacksheep"
  - subject: "Re: Tribute to Apple..."
    date: 2006-10-02
    body: "but ok , lets admit most apps are copied from other OSses to Linux. For Example Winamp -> Amarok , Office 2003 -> Koffice, and many many more. Its just nobody knows Apple Automator. \n\nSo please make this Clone better than the orginal like Amarok and Koffice !! with their Windows Counterparts."
    author: "c"
  - subject: "Re: Tribute to Apple..."
    date: 2006-10-02
    body: "OMG how can you call Amarok a Winamp clone??\n\nAnd KOffice is much older than Office 2003 so it can't be an Office 2003 clone. Besides, I don't think it's an MS Office clone at all, although I'm not sure of this.\n"
    author: "oig"
  - subject: "Re: Tribute to Apple..."
    date: 2006-10-02
    body: "Well if kword is a clone, it is a clone of Abobe Framemaker, not Microsoct Word.    However there are large differences (and not only because kword isn't as complete).  "
    author: "bluGill"
  - subject: "Re: Tribute to Apple..."
    date: 2006-10-02
    body: "No KOffice is not a clone of Office 2003, and amarok look closer to itunes than winamp for me, but both are original software that aim at the same goal as Office 2003 (write document spreadsheets...) or Winamp (listening to music) but doing differentely. My personnal opinion is that the open source world won't win by proposing clone that are just free of charge (non geek users don't care about code :) ), they have to offer something better."
    author: "Cyrille Berger"
  - subject: "Re: Tribute to Apple..."
    date: 2006-10-02
    body: "That's true. But the first step is to offer something at all."
    author: "Thomas Kadauke"
  - subject: "Re: Tribute to Apple..."
    date: 2006-10-02
    body: "Well yes, but if I cannot do the basics with Open Source, offering something better isn't of much use.    Nearly everyone browses the web, and nearly everyone uses a word processor.    You cannot skip those two.\n\nNow you might come up with a killer app, but even if everyone who needs your app uses it, it is unlikely to have as many uses are word processors are web browsers have.  (But you are welcome to try - however applications that a computer can do that currently it does not are rare)   Further, those who do use and need your killer app will use it, but they will find a word processor and web browser to install along side it.   (In some cases this may mean a second computer)\n\nSo we create word processors and web browsers - people will need them.   \n\nOnce those are working well, then open source can innovate.    I'm posting this in IE, which is why there are (I think - I don't see any but I know my spelling skills) spelling mistakes.   Konqueror has had a spell checker installed in the base version for years - innovation that the comptition has not caught up with.  \n\nOTOH, ODF is open source's message that there is little/no innovation left in word processor file formats (likely a correct statement, and if not, the innovation is unlikely to be worth the pain of someone not able to open a document that uses that innovation)"
    author: "bluGill"
  - subject: "Re: Tribute to Apple..."
    date: 2006-10-02
    body: "most applications are created as closedsource/commercial applications with the ideas taken from open source, contrary to the common belief that opensource is copying everyone else."
    author: "redeeman"
  - subject: "Re: Tribute to Apple..."
    date: 2006-10-09
    body: "Complete and utter bullshit.\n"
    author: "grnch"
  - subject: "Re: Tribute to Apple..."
    date: 2006-10-02
    body: "Microsoft's Office suite is by far not the first office suite out there, so claiming one is a clone of another is pretty hard to say since most all are quite similar in basic look and have been for a long time.  Also how on earth can you claim Amarok is a Winamp clone?  XMMS I could understand, but Amarok doesn't work or look anything like Winamp, if you're gonna claim its a clone of something claim iTunes, at least it looks vaguely similar (though the playlist-in-main-dialog approach iTunes uses is far from original as well)."
    author: "Corbin"
  - subject: "Re: Workflow!"
    date: 2006-10-02
    body: "Holy crap! I have been using OS X for some months now (my Linux-box broke down, so I have been stuck with my Mac Mini). And during that time I have been very impressed with Automator. I wished for similar app for Linux as well. We have had pipes and such in the CLI, but we have had nothing like that in the GUI. And now, it seems like KDE-devels have peered right in to my mind, and they are giving us Workflow! Great work guys!\n\nNow, compared to Automator, Workflow could use some eye-candy. But what's important is that the functionality is there. Once the groundwork has been done, eye-candy is quite simple to do (knocks on wood).\n\nNow, what we need is every KDE-app to support workflows. Will Workflow be part of KDE4?"
    author: "Janne"
  - subject: "Re: Workflow!"
    date: 2006-10-02
    body: "Glad you like it :)\n\nI hope it'll be part of KDE 4.0. At least, I plan to introduce it in the KDE 4 lifecycle. It depends on other applications' support for WorKflow though."
    author: "Thomas Kadauke"
  - subject: "Re: Workflow!"
    date: 2006-10-02
    body: "i like it too .-) go for it , make it better than the apple counterpart."
    author: "c"
  - subject: "Re: Workflow!"
    date: 2006-10-02
    body: "I have a question.. Will we be able to add commands or we'll have to use the ones that come with wWrkflow?"
    author: "Hagai"
  - subject: "Re: Workflow!"
    date: 2006-10-02
    body: "Commands come bundled in so called libraries, which are basically plugins for the WorKflow executable. Right now, there's the executable at\n\nhttp://www.kde-apps.org/content/show.php?content=43624\n\nas well as an add-on package for automating Kate at\n\nhttp://www.kde-apps.org/content/show.php?content=43625\n\nSo, yes, you will be able to add commands. I also plan to provide ways to use something other than C++ for commands, but I guess that'll have to wait some more time."
    author: "Thomas Kadauke"
  - subject: "Diffs"
    date: 2006-10-02
    body: "Hi, looks like the diffs and visual changes pages don't work. Cheers."
    author: "pnawrocki"
  - subject: "Re: Diffs"
    date: 2006-10-02
    body: "Fixed, thanks!\n\nDanny"
    author: "Danny Allen"
  - subject: "konqueror !"
    date: 2006-10-02
    body: "hi, i know it is an offtopic question , but i searched everywhere but don't found the answer\n\nwhat is the future for konqueror, do u will split it on two parts, aka file manager and web browser or it will remain the same as the 3.5 series\n\nthanks"
    author: "morphado"
  - subject: "Re: konqueror !"
    date: 2006-10-02
    body: "I doubt it will ever be split, since file browsing and web browsing basically is the same. This way for example FTP-support feels more natural than the Internet Explorer way (a dedicated web browser that suddenly becomes an ftp-client that looks like a file browser...)."
    author: "Terracotta"
  - subject: "Re: konqueror !"
    date: 2006-10-02
    body: "> since file browsing and web browsing basically is the same\n\nFile browsing and web browsing are not the same. A web page doesn't have an icon size (and neither does a web browser have widgets to control icon size).  A web page is itself the contents you're looking for, while a file manager view is just information _about_ contents (the contents are in the files). A file manager always works, a web browser only works with a network connection. Etc etc etc.\n\nWhen you say 'basically the same' you assume that there is a certain abstraction that successfully covers both. This assumption is most likely wrong - it's not enough to have some similarities between concepts (such as a 'Back' action) in order to declare them the same.\n\nI thought it had been decided at last year's academy to split Konqueror into a web browser, a file manager and a universal viewer (Okular?) ?\n\nOh and by the way, you have a web _browser_ and a file _manager_. You can manipulate (manage) your files with a file manager, but you cannot manage the web with a browser. Browsers are read-only, managers are RW. So we really do not have the same concept here.\n\n\n\n\n"
    author: "aii"
  - subject: "Re: konqueror !"
    date: 2006-10-02
    body: "It makes no sense to me that web browsers and file managers/viewers should be separate programs.  Are not HTML documents JUST files?  There is nothing magical in nature about HTML files.  Konqueror already browses and views files, in which is part of managing them.  Why should Konqueror act any differently when it runs across an HTML file?  If being able to view and 'browse' html documents should be split off from Konqueror, then so should all other file views."
    author: "Corbin"
  - subject: "Re: konqueror !"
    date: 2006-10-02
    body: "> Are not HTML documents JUST files?\n\nFor of all no they are really not just files.\n\nSome of them are not files at all, they are GUIs. Many web sites are actually web apps.\nSome of them are generated on the fly, they don't exist anywhere on the web server.\nIf you save the \"html file\" and try to load it from your file systems, it won't work. Because of relative image urls, because of relative links, because of javascript that relies on a cookie to know that something has happened etc etc etc.\n\n> Konqueror already browses and views files,\n\nYes. But the 'views' part is highly controversial and has for many/most people proven to be confusing => a dedicated universal viewer was born.\n\n> Why should Konqueror act any differently when it runs across an HTML file?\n\nBecause Konqueror doesn't run across an HTML file, it runs across a web page.\nIt can only run across an HTML file if you keep one on your hard disk and browse to it. When you load a web URL, all you get is a web page. You do NOT get a file. You only get a file from the web when you download one.\n\nBesides, people have proven to understand the web extremely easily and to understand file systems hardly or not at all. Which means that claiming that the web is a file system is tantamount to shooting yourself in the foot.\n\n(Maybe do the opposite and claim that the file system is a web...).\n\nReiterating the grandparent's question: is the KDE 4 \"smart content manager\" idea dead?\n\n\n"
    author: "aii"
  - subject: "Re: konqueror !"
    date: 2006-10-03
    body: "you ARE right, browsing the web and managing files and viewing files are three different things. but i find myself mixing them all the time. in one konqueror window, i have opened ftp and local locations, websites, and for example pdf or embedded word files. as they are mostly related to each other, i PREFER to have them in one window, it's more logical. so tough you are right, i still disagree and think konqi shouldn't be split. or maybe by default, but allow users to use it the 'old' way, as it is much easier and keeps things tidy. i would have to open several windows to allow me to do what i can do now in one.\n\nalso, seperate apps would not allow me to split-screen on a webpage, drag'n'drop the links to a local location. not that i do that every day, but i do use it, and i think it's very usefull...\n\none one had, i support a re-thinking of konqueror, but i'm really afraid it'll lose most of its power and won't be as usefull as it was. i read the kuicklinks for copying and moving files have already been removed in trunk - which really sucks. i used them very often, mostly the quicklocations. we won't have something like this, most likely, i guess. well, that's the first thing the new konqi will lack. not a good feeling, i tell you."
    author: "superstoned"
  - subject: "Re: konqueror !"
    date: 2006-10-03
    body: "> as they are mostly related to each other, i PREFER to have them in one window\n\nRight. But wasn't plasma assumed to take care of that kind of grouping? Why abuse an abstraction that is already abusive itself, when better ways can be found?"
    author: "aii"
  - subject: "Re: konqueror !"
    date: 2006-10-03
    body: "> Yes. But the 'views' part is highly controversial and has for many/most people \n> proven to be confusing => a dedicated universal viewer was born.\n\nPlease avoid using the phrase \"most users want X\" or variants thereof, when that statement is not a product of research.\n\nLooking at the report on Konqueror which was done by the usability folks (available on usability.kde.org) , the integration of views in Konqueror proved a popular feature, although there were problems with specific viewers such as the music player.\n\nAs far as I know, plans for Konqueror for KDE 4.x are still at a very early stage, so if you have ideas or suggestions, now is an ideal time to put them forwards.  The current Konqueror/KDE4 code is, I believe, essentially a port to the Qt4/KDE 4 libraries.\n\n\n"
    author: "Robert Knight"
  - subject: "Re: konqueror !"
    date: 2006-10-03
    body: "> Please avoid using the phrase \"most users want X\" or variants thereof, when that statement \n> is not  a product of research.\n\nRight. I was trying to refrain from it by saying many, but finally I just couldn't and said most ;-)\n\n"
    author: "aii"
  - subject: "Re: konqueror !"
    date: 2006-10-04
    body: "\"Some of them are not files at all, they are GUIs. Many web sites are actually web apps.\n Some of them are generated on the fly, they don't exist anywhere on the web server.\"\nOnly if people are being silly. Your browser issues \"get /path/to/file.html\", and in most cases it will just get that file. And I have used sites which do on-the-fly generation of pdfs and images.\n\nIf you save the \"html file\" and try to load it from your file systems, it won't work. Because of relative image urls, because of relative links, because of javascript that relies on a cookie to know that something has happened etc etc etc.\n\nSo you can embed external documents in it - you can do the same with most office programs. And any javascript that works like that is the plain stupid.\nI want to view html documents the same way as any others, and if the other end is messing around to generate them that's their business."
    author: "mikeyd"
  - subject: "The new website is beautiful and ergonomic..."
    date: 2006-10-02
    body: "Congratulations to everyone involved and thanks for making it easier to communicate, get to know and be involved in and with the KDE project.\n\n"
    author: "Gonzalo Porcel"
  - subject: "New kopete feature"
    date: 2006-10-02
    body: "\"Work begins on supporting Telepathy in Kopete.\" It's very tempting to take that at face value :-)"
    author: "Wilfred"
  - subject: "Re: New kopete feature"
    date: 2006-10-03
    body: "Telepathy is very important as cross-desktop integration effort (which has to come from KDE anyway), the gnomes and esp some distributions strongly supporting gnome are working hard on it."
    author: "superstoned"
  - subject: "No links from kde.org to kde-look, apps, edu, etc."
    date: 2006-10-04
    body: "Well, new oxygen look is welcomed and the page looks stylish. However, section listing links to important kde projects/parts is gone.\n\nIn the previous version, there was a list with links to kde-look, kde-apps, edu, art, ... I miss it and I think some new people could welcome if this part comes back."
    author: "Stromek"
  - subject: "Re: No links from kde.org to kde-look, apps, edu, "
    date: 2006-10-08
    body: "Yes, I have the same complain!\nI always used kde.org to access that sites because I don't want to remember all that addresses!\nPlease restore them."
    author: "Alessandro"
  - subject: "Re: No links from kde.org to kde-look, apps, edu, "
    date: 2006-10-09
    body: "We're trying to move away from the concept of a portal that's leading to all KDE related websites.\n\nkde-apps, kde-look, the dot and many other subsites have excellent RSS feeds, please use those.\n\nWhat we want with the new website in the end is deliver a strong message of what KDE is, what it's about and address the most important questions for a user. It's *not* an aggregator for all kinds of news out there, we had this, and 60+ links on a frontpage leaves an extremely messed up impression."
    author: "Sebastian K\u00fcgler"
---
In <a href="http://commit-digest.org/issues/2006-10-01/">this week's KDE Commit-Digest</a>: KPersonaliser, the new installation greetings wizard, has been removed from KDE 4. <a href="http://solid.kde.org/">Solid</a> is imported into kdelibs for KDE 4. <a href="http://www.kdedevelopers.org/node/2412">Marble</a>, a generic geographical widget with wide-ranging possibilities, is imported into KDE SVN. Work begins on supporting <a href="http://telepathy.freedesktop.org/">Telepathy</a> in <a href="http://kopete.kde.org/">Kopete</a>. <a href="http://www.jowenn.at/permalink/1159438883.html">Experimental eyecandy</a> in the Kate editor, with a new, <a href="http://impul.se/~kling/blog/?p=10">non-obtrusive search bar implementation</a>. User interface experiments in Krita. Development of Krossrunner in KOffice, a command-line <a href="http://en.wikipedia.org/wiki/OpenDocument">OpenDocument</a> format manipulator. KArm has been renamed to KTimeTracker, to better represent its functionality. The <a href="http://kde.org/">kde.org website</a>, along with many related sub-sites, has changed over to the <a href="http://oxygen-icons.org/">Oxygen</a> style. <a href="http://conference2006.kde.org/">aKademy 2006</a> draws to a close.


<!--break-->
