---
title: "KDE Commit-Digest for 27th August 2006"
date:    2006-08-28
authors:
  - "dallen"
slug:    kde-commit-digest-27th-august-2006
comments:
  - subject: "Big plans! Or dreams?"
    date: 2006-08-28
    body: "Sleek looks so cool! A clever idea with a lot of possibilities, if done right. Great. :)\n\nSomething like Decibel would be nice to have, too. But I have some questions: What is the relationship to Kopete? The website looks rather outdated. And the plans are really ambitious, given there is no code yet (public available). Especially, as the (long term) plans are rather, well, unspecific. The given example with realtime communication out of KMail is already there, nothing new. What kind of collaboration features do they envision, next to whiteboards? What about integrating e.g. with KDevelop's teammode? Perhaps plans should be withhold from the public until first prototypes are there? See what happened to Tenor. And where is Plasma right now? Or is the project name Decibel somehow related to \"Much noise, but...\". Yes, sounds indeed like a lot of buzz...\n\nInformed answers welcome!"
    author: "Anonymous"
  - subject: "Re: Big plans! Or dreams?"
    date: 2006-08-29
    body: "AFAIK you can compare decibel with phonon: it's a intermediate between kde appliations and several different backends.\n\nWhile phonon is an intermediate between multi media applications and different sound engines like nmm, gstreamer and xine, decibel is an intermediate between e.g. pim-applications like kopete and backends for VOIP.\n"
    author: "AC"
  - subject: "Sleek looks cool"
    date: 2006-08-28
    body: "Sleek looks like are really neat idea.  Just a couple questions about it, is it a new type of panel or an applet?  Will there be a widget, I assume a slider, to control the scale of the time line?  Can I slide the now line to the left or right to see what has happened or see further into the future.\n\nLooks really good, keep up the great ideas."
    author: "____"
  - subject: "Re: Sleek looks cool"
    date: 2006-08-28
    body: "Sleek does look interesting byt it seems that it should be built on top of Plasma and not as a stand alone app."
    author: "ac"
  - subject: "Re: Sleek looks cool"
    date: 2006-08-28
    body: "____: Right now it exists as its own app. You can already zoom in and out to scale the timeline using Ctrl + Mousewheel. Mousewheel alone moves ahead or reverse in time. \n\nSo basically the features you're asking about are already working. It is just hard to tell from the screenshots how the bar operates. :)\n\nac: Patience."
    author: "Sheldon Cumming"
  - subject: "Re: Sleek looks cool"
    date: 2006-08-29
    body: "Great work.  :)"
    author: "ac"
  - subject: "What took so long?"
    date: 2006-08-28
    body: "The idea of \"time ribbon\" has been floating around for a long time already. (Idea became wide-spread in Adobe's Photoshop Album I have even seen prototypes with 2-layer time ribbons with week/month events in the back, and inter-day activities on top all centered on \"now\" and with scroll-wheel zooming (in center) and scrolling (pointing to the sides).)\nC'mon, this should have been done for 3.5.x already. It's sad to be accused of copying again."
    author: "Danil Suslik Dotsenko"
  - subject: "Re: What took so long?"
    date: 2006-08-28
    body: "What took so long? Well, nobody did it. It is as simple as that. "
    author: "Carsten Niehaus"
  - subject: "Re: What took so long?"
    date: 2006-08-28
    body: "the idea of a time line has been floating around for centuries. i remember making them on paper in elementary school. i suppose adobe sadly copied grade 1 students. ;)\n\nthe new thing here is to make such things a central part of the desktop (think of having one sitting at the top/bottom of your screen or on the desktop itself) and available to all apps as a reusable widget that ties into common datasources (e.g. your appointments in akonadi).\n\nshow me where that's been done on a commodity desktop out of the box.\n\nthanks for you enthusiasm though ;) btw... saying \"this should have been done for...\" as a way of bashing a current effort is a little odd, don't you think? if it should've been done before isn't it nice that it's at least getting done now as opposed to, say, a year from now?"
    author: "Aaron J. Seigo"
  - subject: "Cheer, not sneer"
    date: 2006-08-29
    body: "thanks for you enthusiasm though ;) btw... saying \"this should have been done for...\" as a way of bashing a current effort is a little odd\n\nNo bashing, just some wishfull thinking that there would be more money / resources floating around to make tangible changes on FOSS come sooner. One example is Novell's \"K Menu\" - it is available (almost) now, as opposed to some time in the next major release.\n\nAs for \"what took you soo long\" - it's translated as \"this is so mind-blowingly simple, but incredibly usefull. I want it now!!!\" kind of cheering thing :)\n"
    author: "Daniel \"Suslik\" D."
  - subject: "Re: What took so long?"
    date: 2006-08-28
    body: "Okay.\n\nYes, the idea of a time ribbon has been floating around for a long time. So where is the implementation? _Prototypes_ within Adobe Photoshop products? \n\nIf you want to run it on 3.5.x, go right ahead.\n\nOtherwise thanks for coming out."
    author: "Sheldon Cumming"
  - subject: "Re: Sleek looks cool"
    date: 2006-08-28
    body: "Sleek looks really awesome. I'm only not so sure about the part of keeping the information on the screen all the time, since it takes a lot of space if it's the size like in the screenshots. It would be cool if you can magically turn the kicker bar into such a time bar when you ask for it (I already see the animation of kicker rotating into this :-) maybe Plasma can allow such fantasies...) and have a smaller bar always open, like an applet on the desktop, or the kicker bar."
    author: "Selene"
  - subject: "Re: Sleek looks cool"
    date: 2006-08-28
    body: "Mmm, I think the great idea is to have it always on screen. Otherwise I could just as well click an icon in my systray to call up KOrganizer. IMO this is a great idea. So simple but I have never thought of it before. This is probably the reason why I never used applications like KOrganizer. They show alert boxes either too early or too late and you always have to set how much time you want to be warned in advance. With Sleek the \"doomsday\" will slowly approach, rolling more and more into your view. Just like in real life, isn't it? ;-(\n"
    author: "Martin"
  - subject: "Re: Sleek looks cool"
    date: 2006-08-28
    body: "Selene:\n\nThe current version is only at the greater size when you click on it. Normally it remains in a minimal state until you want to do something nifty, like scroll ahead, or zoom out, etc.\n\nMartin: \n\nYep, pretty much :) It needs to be always visible, otherwise its useless and easily trumped by a paper calendar."
    author: "Sheldon Cumming"
  - subject: "Re: Sleek looks cool"
    date: 2006-08-29
    body: "Always visible, or a mouse move (or at most one click) away. Cmp. the kicker panel hiding options. I would like that very much, please proceed :)"
    author: "frinring"
  - subject: "Re: Sleek looks cool"
    date: 2006-08-29
    body: "the plan is to integrate this with the \"panels\" in kde4 as an option (perhaps even as part of the standard set up if we can get it both useful and cool enough?) so that'll come =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Sleek looks cool"
    date: 2006-08-29
    body: "Wonderful :) Go for it!"
    author: "frinring"
  - subject: "Re: Sleek looks cool"
    date: 2006-08-29
    body: "The auto-hide function for the taskbar is one of the *worst* ideas for GUIs ever made IMO. Some colleagues of me (rather novice users though) find it so cool under Windows to first move the cursor down, wait for the bar to appear, then look around where the application is, then go there, click and wait until the bar disappears. They do this everytime they want to switch between apps. Just for 20 pixels saved in screen-height. Just crazy."
    author: "Martin"
  - subject: "hiding things can be usefull"
    date: 2006-08-29
    body: "20 pixels in height, but e.g. 1400 pixels in width... You know you can have more than one panel? I have four (yeah), two visible, two hidden. The visible with the often used, the other with less often used items. And only one is of 20 pixel height, the others are 64 pixels and more.\n\nThere are different working habits, and different solutions for this. There is no one-size-fits-all. If you are switching apps using a mouse every two minutes, a non hidden task bar might be better for you. But if you change only once in an hour, or do it using the keyboard (Alt-Tab), that screen estate is better used for the working window. IMHO. And less visual clutter."
    author: "frinring"
  - subject: "Re: hiding things can be useful"
    date: 2006-08-29
    body: "My experience with auto-hide has been much like Martin's. Windows users that try to save space by forcing themselves to go through horribly unproductive taskbar scrubbing every time they want to switch apps. \n\nWhat things could be in a hidden panel that would be even remotely important? Bits of information? UI elements? If these things were important, I'd be mousing to focus the panels every few seconds - huge waste of time. If they weren't important, I could probably have this stuff elsewhere.\n\nI'm not saying this functionality should be scrapped, I'm just one that would disable it on my own computer :)\n\nI'd be interested to hear what you have in your two hidden panels :)"
    author: "Sheldon Cumming"
  - subject: "Re: hiding things can be useful"
    date: 2006-08-29
    body: "<i>... I could probably have this stuff elsewhere.</i>\n\nLike in hidden panels, so it is not too far away.\n\nI have there all things that I need and which do not show some status info:\nProgram shortcuts, trash, (quick) folders, bookmarks and contacts."
    author: "frinring"
  - subject: "Re: Sleek looks cool"
    date: 2006-08-30
    body: "I started using the auto hide function as soon as KDE introduced the \"hide immediately when the mouse leaves the bar\" option (earlier versions wouldn't let you set a delay shorter than 1 second).I always work with maximized apps and use alt-tab to switch applications. \n\nThis is an example of why it's good to have many options. Different people work in different ways.\n"
    author: "Apollo Creed"
  - subject: "What the summary missed..."
    date: 2006-08-28
    body: "Siraj Razick committed changes in \n/trunk/playground/base/kbfx_plasma: \n\"A new Chapter of KBFX starts here\"\n\nAnd by that he probably means more KDE4 applications will he an exit button right in the middle of the window.\n\nMatthias Kretz committed changes in\n/trunk/KDE/kdemultimedia: \n\"I think the Phonon xine backend is ready for trunk. Playback of URLs supported natively by xine should all work\"\n\nSure is more usefull than than building a backend to play essentially just one codec"
    author: "Danil Suslik Dotsenko"
  - subject: "Re: What the summary missed..."
    date: 2006-08-29
    body: "Reason for building a backend to \"play essentially just one codec\" is testing - it might as well have been a tone generator, but that gets very old very quickly ;)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Sleek"
    date: 2006-08-28
    body: "That sleek thing is indeed the coolest thing I've seen in times!"
    author: "Me"
---
In <a href="http://commit-digest.org/issues/2006-08-27/">this week's KDE Commit-Digest</a>: as the <a href="http://developer.kde.org/summerofcode/soc2006.html">Summer Of Code</a> draws to a close, a mass code import in the Physiks project, and other notable commits for several of the other affiliated projects. Work begins on a <a href="http://www.kexi-project.org/">Kexi</a> importer for <a href="http://www.koffice.org/kspread/">KSpread</a>. Numerous improvements for displaying data in forms and table view in Kexi, including support for default values and tooltips for large content. Lots of work on the <a href="http://kross.dipe.org/readme.html">Kross</a> scripting framework. Improved functionality in <a href="http://konversation.kde.org/">Konversation</a> and <a href="http://kftpgrabber.sourceforge.net/">KFTPGrabber</a>. Speed and memory optimisations in <a href="http://www.kdevelop.org/">KDevelop</a> and <a href="http://www.methylblue.com/filelight/">Filelight</a>. An experimental project begins to integrate the <a href="http://live.gnome.org/Orca">Orca</a> Screen Reader into KDE 4 using <a href="http://www.freedesktop.org/wiki/Software/dbus/">D-Bus</a>.
<!--break-->
