---
title: "KDE 3: All About the Apps"
date:    2006-04-10
authors:
  - "cniehaus"
slug:    kde-3-all-about-apps
comments:
  - subject: "amarok versus consistency and usability"
    date: 2006-04-10
    body: "It's sad to see my loved amarok doing it so totally wrong to include an iconset own its own. That is so contraproductive in order to get a good desktop experience. \n\nWelcome to the Windows world, where every application delivers its own icons. \n\nI don't want that, listen!"
    author: "Anonymous Coward"
  - subject: "Re: amarok versus consistency and usability"
    date: 2006-04-10
    body: "> I don't want that, listen!\n\nStop your Gnomish FUD and read the amaroK changelog:\n\n\"amaroK now has a custom icon theme, and an option to switch back to the\n system icons, if preferred (in the General settings section).\""
    author: "max"
  - subject: "Re: amarok versus consistency and usability"
    date: 2006-04-10
    body: "You really don't get it, do you?\n\nIt doesn't have anything to do with FUD...\n\nPlease consider reading: \nhttp://amarok.kde.org/blog/archives/80-Arrrr...-Artwork!.html#c1685"
    author: "Anonymous Coward"
  - subject: "Re: amarok versus consistency and usability"
    date: 2006-04-10
    body: "You really don't get it, do you?\nAmroK has a cool design consisting of custom colors, custom widgets, custom layout and custom icons. If someone wants to switch back to the KDE-standrad-icons he can do so. Same as with amaroK's color theme.\n"
    author: "max"
  - subject: "Re: amarok versus consistency and usability"
    date: 2006-04-10
    body: "In short: It doesn't match the desktop. Exactly the custom stuff (e.g. the vertical image on the popup menu, the mouse-over effect on the sidebar tabs,... is, what makes Amarok a playground, than a nicely (graphically) integrated application.\n\nTo some, this is cool design, to others it simply sucks."
    author: "Carlo"
  - subject: "Re: amarok versus consistency and usability"
    date: 2006-04-10
    body: "well, personally i am torn between these two reactions. on one hand, i think it's cool. on the other hand, i don't like it if amarok looks totally different. i don't use the custom colors, nor custom icons. that's too much for me."
    author: "superstoned"
  - subject: "Re: amarok versus consistency and usability"
    date: 2006-04-10
    body: "I like it both ways. On the one hand, amaroK is a great way to try out new graphical schemes before you build stuff for the whole of KDE. On the other hand, it can be a bit of a pain having those different icons.\n\nI think that amaroK should have the default KDE icons, and then let you change the icon theme to whatever you want."
    author: "Alex"
  - subject: "Re: amarok versus consistency and usability"
    date: 2006-04-10
    body: "Why not make the amaroK's eye-candy available to all KDE apps by integrating them into KDE-artwork?\n\nIn fact, Sadro Giessl, author of KDE's Plastik theme and of the new SVG-based coKoon theme is thinking about doing just that. Read what he had to say in a message to the Compiz mailinglist:\n\"\"\"\nCurrently, the library [coKoon] is based on Qt4 and I aim to integrate it into KDE 4 (window decorations, widget styles, maybe replace \"hardcoded\" stuff like the Amarok eyecandy). A theme editor application is planned as well.\n\"\"\"\n\nhttp://lists.freedesktop.org/archives/compiz/2006-April/000019.html"
    author: "ac"
  - subject: "Re: amarok versus consistency and usability"
    date: 2006-04-10
    body: ">  Why not make the amaroK's eye-candy available to all KDE apps by integrating them into KDE-artwork?\n\nBecause it's looking ugly. Matter of taste. :p\n\n\nOr to phrase it more politcal correct: As long as it remains optional..."
    author: "Carlo"
  - subject: "Re: amarok versus consistency and usability"
    date: 2006-04-10
    body: "Although I do like the new icons for amarok. I find it annoying that they've included their own icon set. One of the reasons why they don't want skin support for amarok is because they want consistency with kde, or at least they did. Although a custom icon set isn't as drastic as skin support, it still goes against one of their so called goals and reasons to not include other certain features."
    author: "mr double standard"
  - subject: "Re: amarok versus consistency and usability"
    date: 2006-04-10
    body: "  Stop calling usability on things that have nothing to do with it! Go read a HCI book, please!\n\n\n  And now, to be more on topic - If you have ever used an icon theme that is not Crystal SVG, Noia or one such shiny icon set, you will have surely noticed that the icons included are utterly and totally out of place. This way it is ensured that the application is consistent WITHIN ITSELF!\n\n  There is an option to turn on system icons, which will cause the icons to be wrapped and existing icons to be used. Trust me on this - we are taking every care in the world to make sure that these icons make sense; as an example we spent a couple of hours yesterday trying to find two icons, for podcast and new podcast, that would make sense. We gave up then, but we shall try again later today.\n\n\nP.S.: If you named yourself, we might consider your points. Anonymous Cowards are useless - we have no way of considering who you are and why you make these statements. Context, my good man/woman/person/entity/nothingness! (see? even that bit is impossible to do when you don't know who you're talking with)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: amarok versus consistency and usability"
    date: 2006-04-10
    body: "you do not really want to say that you weigh arguments by who gives them? this can get ridiculious. sources are a hint, but no proof."
    author: "Human in HCI"
  - subject: "Re: amarok versus consistency and usability"
    date: 2006-04-10
    body: "Pseudonymity is just annoying."
    author: "Ian Monroe"
  - subject: "Re: amarok versus consistency and usability"
    date: 2006-04-10
    body: "And who the heck is \"Dan Leinir Turthra Jensen\" anyway? And how do I even know you are it/he/she?\n\nNames are useless without digital signatures."
    author: "As you like it"
  - subject: "Re: amarok versus consistency and usability"
    date: 2006-04-10
    body: "I find it shocking that so many amaroK core developers agree with having those custom icons by default. I already made my points on the blog, so I won't repeat them here.\n\nJust a question: As you can clearly see, there is quite a bit controversy about this topic. So, as I would do it if I wasn't sure if this is a good idea (given the opposing statements you must not be sure about it): have you asked the KDE Usability Team for input?\n\nIf you haven't, please do so. I'll be quiet when I read a statement from Ellen, Celeste or someone else there that actually agrees with this move."
    author: "Jakob Petsovits"
  - subject: "Re: amarok versus consistency and usability"
    date: 2006-04-10
    body: "Please see (and vote for) bug #121715 regarding the icons-topic.\nAfter you've read my original text, please read the first comment as well, as it is important to the whole thing.\n\nAlthough I'm not fine with those custom icons, I'd like to thank the amaroK-developers for this fine piece of software.\nKeep rocKing!"
    author: "Christian Nitschkowski"
  - subject: "Re: amarok versus consistency and usability"
    date: 2006-04-10
    body: "That bug is about the moodbar."
    author: "Ian Monroe"
  - subject: "Re: amarok versus consistency and usability"
    date: 2006-04-11
    body: "Sorry, I had the wrong number in the clipboard..."
    author: "Christian Nitschkowski"
  - subject: "Re: amarok versus consistency and usability"
    date: 2006-04-10
    body: "The bug number is wrong, it is http://bugs.kde.org/show_bug.cgi?id=125295\nLet's vote for it and show the amarokkers that we do not want this!"
    author: "Jakob Petsovits"
  - subject: "Re: amarok versus consistency and usability"
    date: 2006-04-11
    body: "Agreed.\n\nIt's very sad that one of the up-and-coming best known *KDE* applications is going against a few vital KDE principles (this is totally regardless of whether it has the \"option\" to go to KDE's normal icons). Having it on by default (and this is the key point) is palpably directly detrimental to the internal consistency of KDE (which ships Crystal icons by default). \n\nOne thing KDE primes itself on is integration; it's very sad and unfortunate to see that one of the popular examples of KDE software breaks such a principle.\n\nAs aseigo put it: http://amarok.kde.org/blog/archives/80-Arrrr...-Artwork!.html#c1685"
    author: "Anon"
  - subject: "Re: amarok versus consistency and usability"
    date: 2006-05-07
    body: "KDE has so much potential if it can clean up its UI more.  I think the best route here is first-run wizards that set important options like these, but really, the defaults should be a simple, consistent, uncluttered default."
    author: "D Rollings"
  - subject: "Re: amarok versus consistency and usability"
    date: 2006-08-21
    body: "I don't think it's so bad. I liked my music application looking different and that's why I used XMMS for so long, it's like saying Karamba themes are bad, because they are inconsistent, well eye candy won't be, because nobody can produce whole desktop in Linux environment so easily. On the other hand those icons just suck, please don't take it personally, because they are monochrome and they handicap usability, that's why I turned them off."
    author: "dhill"
  - subject: "Good"
    date: 2006-04-10
    body: "That is a fine initiative, and it's well done.\n\nFor the next editions, I suggest to let KDE users discover good but not very well known applications. I know three of them :\n* Basket (wow, innovative and useful http://basket.kde.org . Don't cover the stable version 0.5, but the much better 0.6a1) \n* KPhotoAlbum (see http://ktown.kde.org/kphotoalbum/videos/ )\n* KIO from a user point of view (why you don't need a clumsy FTP/SSH client in KDE)"
    author: "jmfayard"
  - subject: "Re: Good"
    date: 2006-04-10
    body: "Basket: Yes, that is a good tool. http://basket.kde.org/development.php looks like 0.6 will take quite a while, though. Not sure if it fits here... Perhaps for the last issue.\n\nKPhotoAlbum: Not to much activity in the last few month (beside its renaming from Kimbdaba)\n\nKIO: Not sure if this fits into this series of articles. Perhpas this is more for something like \"poweruser tipps for KDE 3.5\"?"
    author: "Carsten Niehaus"
  - subject: "Re: Good"
    date: 2006-04-10
    body: "Great idea, and so true 3.5 is all about the apps:-) And looking at the commits to svn 3.5.3 are going to be one solid release.\n\nAnyway, for future edditions what about:\nKtorrent\nDigiKam\nKMyMoney\nkdissert\nKTechlab"
    author: "Morty"
  - subject: "Re: Good"
    date: 2006-04-10
    body: "Digikam (0.9) is already written. Thanks for the other suggestions."
    author: "Carsten Niehaus"
  - subject: "Re: Good"
    date: 2006-04-10
    body: "Is it ? Then, can its plugins (libkexif, kipi-plugins) be compiled with the visibility flags now ?\nBecause the \"stable\" versions are very old, and they will just not provides all the API if compiled with gcc-visibility flags, which I have to disable by hand (as there is no configure flag to disable the visibility flags).\nLearnt this the hard way when my wife said she lacked lots of functions in Digikam.\nDon't get me wrong, Digikam is a wonderful app, the dependancies are just lagging behind.\nAh, also, some french guy said he had done the fench translation of the doc, but I never found it in 0.8. Is it there in the 0.9 ?\nThanks"
    author: "Ookaze"
  - subject: "Re: Good"
    date: 2006-04-10
    body: "0.9 is the next stable release, it is already working very well and should be released soon (not next week or so, but soon, read http://www.digikam.org/?q=node/116). Here is a gallery showing new things in 0.9\n\nhttp://www.digikam.org/?q=image"
    author: "Carsten Niehaus"
  - subject: "Re: Good"
    date: 2006-04-10
    body: "And even more:\nKAT\nkBeagleBar\nKerry\nKMPlayer\nKrusader\nScribus\nKile"
    author: "birdy"
  - subject: "Re: Good"
    date: 2006-04-11
    body: "Ktorrent\n\nI'd rather see it disappear and be replaced by a torrent:// KIOslave, so you can just use kget for torrents. Or at least merge Ktorrent into kget, as for the users perspective there's no difference."
    author: "AJ"
  - subject: "Re: Good"
    date: 2006-04-11
    body: "I beg to differ, it's a rather big differnce both from a users perspective and from implementation and thecnical point. You don't make KGet handle your ftp uploads either.\n\nThere are no particulary good way to solve the upload/seed functionality, in a simple userfriendly way fitting with the userpattern of KGet. Either keep it seeding for some time, and have the users wonder and complain why it don't shut down like with all other forms of download it handles. Or have it shut down after the dowwnload is complete, and end up being baned by most torrent tracers.\n\nPersonaly I don't get peoples obsession with merging everything remotly similar into the same application. Some tasks are better left to specalized tools or separate tools due to different usagepatterns."
    author: "Morty"
  - subject: "Re: Good"
    date: 2006-04-11
    body: "Well, the users klicks on a torrent and expects the file (xgl demo cd in this case) to be downloaded. Kget could do that via ftp or torrent. When it's done just keep seeding the file until the user moves it away or unlinks it, inotity should be able to handle that, or deletes it from the download list of kget.\nAnd you're right, ftp or fish uploads are smth else, they should still be shown by the normal kfile transfer status, just like local actions.\nIt's just I don't like to have two apps with nearly similar functions and most users don't care about the upload part of torrents so this should be handled automatically (but of course still be configurable). You could even include jigdo downloads into kget, so it just acts as frontend to jigdo."
    author: "AJ"
  - subject: "Re: Good"
    date: 2006-04-11
    body: "No, the user should knowingly decide how much he wants to upload in return. Maybe he's short on bandwidth or he pays for the amount of data transferred: in this case, he could choose to upload very little.\n\nMaybe, OTOH, he's got a nice bandwidth and wouldn't mind helping during the night, when he's asleep anyways.\n\nFinally, maybe the torrent he got came from a community that enforces good upload:download ratios. In this case, he'd better stay close to 1:1.\n\nNone of those options are available in KGet/KIO nor will ever be. They are really torrent-specific and should stay in a torrent-specific client."
    author: "Thiago Macieira"
  - subject: "Re: Good"
    date: 2006-04-10
    body: "Yes, Basket is certainly not yet ready for regular user consumption.  When it is ready though, it will be quite the app.\nAs for KIO, I've always felt that the way KDE implements it is somewhat troubling.  There is all sorts of functionality in there that most people would never know existed.  Fish is of course the killer ioslave, but would ever know to type fish://user@localhost into their location bar, or a save dialog for that matter?  This stuff is great, but only if you can discover it.  I can't tell you how many times I would've loved to have a list of the ioslaves that I could use.  You can find a list in the protocols screen of konqueror, but not all of those are user accessible.  Anyway, there is so much great functionality in KDE, I just wish it was more obvious."
    author: "Tim"
  - subject: "Re: Good"
    date: 2006-04-10
    body: "Just out of curiosity, why use fish rather than sftp?"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Good"
    date: 2006-04-11
    body: "No reason, other than I know about fish and didn't know about sftp.  Also, sftp doesn't seem to be installed on my system.\nThe point was, how would somebody find out about this?\nyou could google it, but that would require that you know it exists in the first place.  KDE is great on functionality, it can just be kinda hard to find out about it."
    author: "Tim"
  - subject: "Re: Good"
    date: 2006-04-11
    body: "fish requires ssh + a shell on the remote side. sftp requires the sftp subsystem to be enabled on the remote side's /etc/ssh/sshd_config."
    author: "Thiago Macieira"
  - subject: "No thanx for fish"
    date: 2006-04-10
    body: "I have seen issues with it from time to time. It is now last resort. Instead the sftp always works if ssh is on the system (rarely have I seen an install include ssh, but not sftp)."
    author: "a.c."
  - subject: "Re: Good"
    date: 2006-04-11
    body: "That's why, if you go to system:/ and click \"Remote Places\", you'll see an \"Add a Network Folder\" wizard that lets you create that kind of URL.\n\nBut it's intended for the novice user: not many features are possible in the wizard. If you're advanced enough to notice what's missing, you already know how to type them in URLs."
    author: "Thiago Macieira"
  - subject: "Media KIOSlave"
    date: 2006-04-10
    body: "Unfortunately, I don't see any improvement for the media KIOSlave.\nThis is very important : will the next KDE support the latest hal version, which does not provide mount points (through modifying the fstab) ? Or is pmount/gnome-mount the only way to go ?"
    author: "Ookaze"
  - subject: "Re: Media KIOSlave"
    date: 2006-04-10
    body: "Kde media:/ kio support pmount/hal since months..."
    author: "gnumdk"
  - subject: "Re: Media KIOSlave"
    date: 2006-04-10
    body: "yes, but nothing else supports the media:/ protocol correctly, all the stuff downloads media:/ content to temp, but that's not smartiest way thru imho..\n(or perhaps one? very rencent kaffeine?)"
    author: "Martin Zbo&#345;il"
  - subject: "Re: Media KIOSlave"
    date: 2006-04-11
    body: "if kio downloads stuff to /tmp, in stead of opening it directly in the appropiate kde-application, you should check the mime type or kmenu entry of that kde application: make sure that the command that launches the kde application contains the argument '%U' to tell kio that it can handle URL's by itself.\n(e.g. make sure that the kmenu entry of kaffeine has 'kaffeine %U' as startcommand, not just plain 'kaffeine')"
    author: "AC"
  - subject: "Re: Media KIOSlave"
    date: 2006-04-10
    body: "The KIOslave does not support the last hal : when you click the icon, it can't mount it itself, it complains that the device is not in fstab nor in mtab.\nWhich is a big problem to see the content of data CD and DVD.\npmount/gnome-mount are external programs."
    author: "Ookaze"
  - subject: "Re: Media KIOSlave"
    date: 2006-04-10
    body: "yeah, hal breaks compatibility every now and then. so you'll have to wait for a new KDE minor release (3.5.x) to fix it."
    author: "superstoned"
  - subject: "Re: Media KIOSlave"
    date: 2006-04-10
    body: ">>Unfortunately, I don't see any improvement for the media KIOSlave.\n\nTry KIO-FUSE:\nhttp://wiki.kde.org/tiki-index.php?page=KIO+Fuse+Gateway"
    author: "ac"
  - subject: "Amarokian icons"
    date: 2006-04-10
    body: "Pretty on the website but don't work in amarok because they are not defined when small. The pause button resembles the stop button too much for example."
    author: "hachaboob"
  - subject: "Re: Amarokian icons"
    date: 2006-04-10
    body: "Yeah, we're aware of these issues. The icons will be reworked to provide higher contrast.\n"
    author: "Mark Kretschmann"
  - subject: "Re: Amarokian icons"
    date: 2006-04-11
    body: "The colors in the moodbar look horrible. \n\nI think that you really need to choose better colors for it. And of course it would make sense to \"filter\" the view. Right now it just looks noisy due to its appearance. As defining the mood by exact values is a rather \"ambigious\" attempt and as the whole graph can only be a good well-founded guess it wouldn't harm to noise-filter the graph to make it look more beautiful. \n\nOf course eventually it should be the aim to have some icon or label instead that translates the graph into something a common user understands instantly."
    author: "Torsten Rahn"
  - subject: "Re: Amarokian icons"
    date: 2006-04-11
    body: "There is an option to change \"moodbar theme\" in the configuration dialog. At least there was one when I last checked. The reason it's unthemed by default is because an unthemed moodbar makes more sense (I.E. it's easier to get something out of it, if anything...)."
    author: "Firetech"
  - subject: "KDE 4.0"
    date: 2006-04-11
    body: "When will there be alpha release of KDE 4.0 ?\n\n"
    author: "Anonymous Person"
  - subject: "Re: KDE 4.0"
    date: 2006-04-11
    body: "Please forget KDE 4.0 for now and focus on KDE 3.5 The better we get it the better forthcoming KDE major releases will be."
    author: "vito gonzaga"
  - subject: "Re: KDE 4.0"
    date: 2006-04-11
    body: "That recommendation is valid for users only.\n\nIf you're a developer and want to help us, you're welcome to try KDE 4.0 right now. But be aware that you have to be willing to get your hands dirty with coding."
    author: "Thiago Macieira"
  - subject: "Re: KDE 4.0"
    date: 2006-04-11
    body: "http://developer.kde.org/development-versions/kde-4.0-release-plan.html"
    author: "Nicolas Goutte"
  - subject: "What about Wireless"
    date: 2006-04-11
    body: "One thing I really miss from windows its the ability to change and connect to a wireless network on an easy fashion.\n\nIve tried a lot of tools for WiFi connection (KDE and Gnome based) and at the end I end up with iwconfig as the only reliable tool.\n\n\n\n"
    author: "Mario"
  - subject: "Re: What about Wireless"
    date: 2006-04-11
    body: "Try looking at OpenSuSE 10.1, They have a nice client to handle this."
    author: "Falcon"
  - subject: "Re: What about Wireless"
    date: 2006-04-12
    body: "There is no openSUSE distribution, it's called SUSE Linux."
    author: "Anonymous"
  - subject: "Re: What about Wireless"
    date: 2006-04-11
    body: "I use knetworkmanager. It's simple to switch between different nets and you can add new wireless connections easily.\n\n"
    author: "hiasll"
  - subject: "Re: What about Wireless"
    date: 2006-04-11
    body: "Try the Wireless assistant (wlassistant) that comes with PCLinuxOS, it works well and I use it wherever I go."
    author: "PCLinuxOS"
  - subject: "Re: What about Wireless"
    date: 2006-04-26
    body: "http://www.gnome.org/projects/NetworkManager/\n\nIt's simple and works very well, there's not much more that could be asked from it!\n\nI've found it to better than the inlcuded Windows apps and many 3rd party applications.  WPA also works perfectly, something that I was really shocked at :)"
    author: "Daemon"
  - subject: "Kaffeine good, Codeine better"
    date: 2006-04-11
    body: "Maybe Kaffeine has improved recently, but I always found the interface to be kind of confusing.  I really enjoy Codeine (http://kde-apps.org/content/show.php?content=17161) because it just works, and the interface is dead simple.  I don't care about playlists or anything, I just want to double click on a video and have it play."
    author: "Leo S"
  - subject: "what k3b is missing to be a killerapp"
    date: 2006-04-11
    body: "k3b already can do all the common burning jobs. The only thing i'm missing is an easy way to rip, shrink and burn a Video-DVD. If this would be integrated in k3b, k3b would the the absolute killer app."
    author: "pinky"
  - subject: "Re: what k3b is missing to be a killerapp"
    date: 2006-04-11
    body: "I don't think that k3b should do this. k3b is a burning app and no video editing program.\n\nTry lxdvdrip! \n"
    author: "hiasll"
  - subject: "Re: what k3b is missing to be a killerapp"
    date: 2006-04-12
    body: "Or one of the DVD ripping tools at apps-kde.org."
    author: "Morty"
  - subject: "Re: what k3b is missing to be a killerapp"
    date: 2006-04-12
    body: "there are a lot of apps, can you recommend one? Thanks!"
    author: "pinky"
  - subject: "Re: what k3b is missing to be a killerapp"
    date: 2006-04-12
    body: "Since I don't have a  DVD  burner and hence no need for such tools I don't know how much my recommendation is worth:-) For rip, shrink and burn I think I'd go for k9copy or DVD BacKup Express. If you are out to recode to XviD, perhaps DVD Rip-O-Matic would be better.  Anyway I think they all use K3b for their burning. \n\nhttp://www.kde-apps.org/content/show.php?content=23885\nhttp://www.kde-apps.org/content/show.php?content=35247\nhttp://www.kde-apps.org/content/show.php?content=21455"
    author: "Morty"
  - subject: "Re: what k3b is missing to be a killerapp"
    date: 2006-04-12
    body: "i don't want k3b to be a video editing program.\nBut if i can use k3b to copy data cds, audio cds, video-cds and data dvds than i think it's a logical step to offer video-dvd copying too."
    author: "pinky"
  - subject: "Re: what k3b is missing to be a killerapp"
    date: 2006-04-16
    body: "Copying? Yes. Shrinking, reencoding and otherwise editing? No."
    author: "Thiago Macieira"
  - subject: "Re: what k3b is missing to be a killerapp"
    date: 2006-04-17
    body: "<i>>Copying? Yes. Shrinking, reencoding and otherwise editing? No.</i>\n\nI think you should see this more as a use case with the eyes of an user and not from the technical side.\nUser want to copy CD/DVD and don't care what's the content of this media. And i think a \"killer-bruning-app\" should do this.\nI don't talking about a sophisticated video-DVD app where i can select tracks etc. Just a simple function which allows me to copy Video-DVD's and do what's necessary to get the data from one DVD to another (including ripping and shrinking if necessary as intermediate steps).\n\nFor more powerfull things like selecting special tracks etc. there can be a extra video-editing/ripping tool. But a simple copy should be possible with a \"killer-burning-app\"."
    author: "pinky"
---
Last November, <a href="http://www.kde.org/announcements/announce-3.5.php">KDE 3.5.0</a> was released. Since then, many users have been waiting for the next big steps. While most of the core developers are working on the first iterations of KDE 4, the KDE 3 developer platform is more vital than ever, resulting in new and exciting applications. "All About the Apps" puts the spotlight on the classics of KDE's applications as well as new and promising applications from the KDE community that can make your KDE desktop more productive. We will also keep you informed about development in current KDE 3.5 series.












<!--break-->
<h2>New rules in the KDE 3.5 release cycle</h2>
<p>Until KDE 3.5, when a KDE release was frozen in preparation to be released, new features, and new user-visible text (strings) were not allowed in the later releases in the series, in order to make sure that no new bugs were introduced and no translations were broken. For KDE 3.5 this has been changed slightly.</p>

<p>Since the KDE 4.0 development cycle will take longer than usual due to the 
scope of the changes and improvements being undertaken, the developers want 
KDE 3.5.x to be the best possible KDE for users in the meantime. Therefore, KDE 3.5.1 and 3.5.2 not only fixed a lot of bugs (see <a href="http://www.kde.org/announcements/changelogs/changelog3_5to3_5_1.php">3.5.1-fixes</a> and <a href="http://www.kde.org/announcements/changelogs/changelog3_5_1to3_5_2.php">3.5.2-fixes</a>), but the developers were also allowed to change some strings in KDE so that many usability-improvements and updates in documentation found their way into KDE 3.5.x.</p>

<p>KDE 3.5.3 will even include some new features. When a new feature is already in the upcoming KDE 4 code, well tested and known to work in KDE 3.5, the author of the code can ask for inclusion in KDE 3.5.3. If no other developer objects, the new feature can be added. Therefore, you will see some new stuff in <a href="http://www.kde.org/announcements/changelogs/changelog3_5_2to3_5_3.php">KDE 3.5.3</a> (this list is constantly being updated and not yet complete).</p>

<h2>Artwork</h2>
<p><a href="http://www.kde-look.org">KDE-Look.org</a> features many new themes, colour schemes, KDM themes, new icons, improvements to <a href="http://kopete.kde.org">Kopete</a> styles and many other artworks. If you want to pimp your desktop, kde-look it is the place to go!</p>

<h2>Fast Forward with amaroK</h2>
<div style="width: 300px; border: solid grey thin; margin: 1ex; padding: 1ex; float: right">
<a href="http://static.kdenews.org/danimo/amarok.png">
<img src="http://static.kdenews.org/danimo/amarok_small.png" width="300" rheight="208" /></a><br />
<a href="http://static.kdenews.org/danimo/amarok.png">
amaroK 1.4 analyzes the mood of your songs.</a>
</div>
<p>Many people already know that <a href="http://amarok.kde.org/">amaroK</a> is the best audio player available. Version 1.4, of which Beta 3 <a href"http://amarok.kde.org/">was just released</a>, takes this even further. With the help of a new artwork team, amaroK has a great new look, including new <a href="http://amarok.kde.org/blog/uploads/Amarokbuttons2.png">icons</a>, and introduces <a href="http://static.kdenews.org/danimo/amarok.png">moodbar</a>: the first audioplayer to support this new way of representing your music!
The improved media device system allows many more devices to be supported, as well as vastly improving the handling of iPods and iRivers.  There have also been major improvements in all supported audio engines, including support for extra codecs such as WMA, MP4/AAC and RealMedia (RA,RV,RM).</p>

<h2>Tellico: Keep your collections in Order</h2>
<p>In February 2006, <a href="http://www.periapsis.org/tellico/">Tellico 1.1</a> was released. Tellico is a KDE application for organizing your collections, be they CDs, films, files or books. Version 1.1 brings you many <a href="http://www.periapsis.org/tellico/changelog.php">improvements</a> over 1.0 which was the latest version when KDE 3.5 was published. 
Version 1.1.4 is quite an improvement compared to version 1.0.x which was the 
stable one when KDE 3.5.0 was released.</p>

<h2>RSI-Break: Stay fit</h2>
<p><b>R</b>epetitive <b>S</b>train <b>I</b>njury is an illness which can occur as a result of working with a mouse and keyboard. With the new tool <a href="http://www.rsibreak.org/index.php/Main_Page">RSI-Break</a> your are reminded to take a break now and then. In the last month, development has really picked up speed, so now you can enjoy version 0.6 with many new features.</p>

<div style="width: 300px; border: solid grey thin; margin: 1ex; padding: 1ex; float: right">
<a href="http://static.kdenews.org/danimo/kaffeine.png">
<img src="http://static.kdenews.org/danimo/kaffeine_small.png" width="300" height="208" /></a><br />
<a href="http://static.kdenews.org/danimo/kaffeine_small.png">
Kaffeine's new user interface in DVB mode.</a>
</div>
<h2>Kaffeine: Multimedia all around</h2>
<p>Kaffeine is the <a href="http://www.xinehq.de">Xine</a>-based media player. The authors just released <a href="http://kaffeine.sourceforge.net/index.php?page=news&details=13">Kaffeine 0.8</a>. Compared with the previous version (0.7), many new features have been added and the user interface has been revised. Of course, Xine itself is constantly improving, so that you can expect an even better multimedia experience!</p>

<h2>K3B: Hot stuff</h2>
What is the tool for burning DVDs and CDs in Linux? Most will probably agree that this is the famous <a href="http://k3b.plainblack.com/about">K3B</a>! In the past few months the author has published several new versions of K3B to provide you with the most stable burning tool ever. The changes included are far too many to be listed here, so just have a look at the extensive <a href="http://websvn.kde.org/trunk/extragear/multimedia/k3b/ChangeLog?rev=518009&view=markup">ChangeLog</a>

<p>That's it for this week. If you have a favorite application, please propose them in the comments section. If you want to stay up to date with new cool KDE applications, look at the list of <a href="http://www.kde-apps.org/">latest KDE applications</a>.</p>




