---
title: "Quickies: Kopete, ArkLinux, GetMyDistro, Portland, Usability"
date:    2006-06-04
authors:
  - "jriddell"
slug:    quickies-kopete-arklinux-getmydistro-portland-usability
comments:
  - subject: "getMyDistro"
    date: 2006-06-03
    body: "thanx  Jonathan for posting :-)\nby the way I need an icon for getMyDistro so if anyone could make one for me that would be cool (think of a mix of cd burning, bittorrent, windows, gnu and linux :D). Mail me at patcito at gmail dot com.\n\nhttp://getmydistro.blogspot.com/"
    author: "Patrick Aljord"
  - subject: "Re: getMyDistro"
    date: 2006-06-03
    body: "by the way it's a pity that the kollaboration forum is gone on kde-artists.org, it was really active . I like the concept of Open-collab.org but it's really confusing I couldn't even figure out where to post for asking icons for my app and the site seems pretty empty compared to kde-artists.org. \n\n"
    author: "Patrick Aljord"
  - subject: "Re: getMyDistro"
    date: 2006-06-04
    body: "you're not the only one who thinks open collab and the new kde artists are a completely mess...."
    author: "perri"
  - subject: "Re: getMyDistro"
    date: 2006-06-04
    body: "Open-Collab.org is still Beta and should be seen as a work in progress. We would love to receive any input you may have that you think would be helpful. Here is a link to the \"site issues\" forum. http://open-collab.org/mod/forum/view.php?id=241 We welcome any discussion about \"issues,\" ideas or thoughts that you may have that you feel would be beneficial. You could say that we are open to collaboration.;)"
    author: "Janet Theobroma"
  - subject: "Re: getMyDistro"
    date: 2006-06-04
    body: "Well this is the problem. I could never have found this forum if it wasnt for your post. The problem is that its a hard to find what youre looking for on opencollab but i promess i will post there  when i have time."
    author: "Patrick Aljord"
  - subject: "Re: getMyDistro"
    date: 2006-06-04
    body: "Looks great. Good Work. A humble suggestion though, for the Spanish version. \"Quemacion\" deberia ser \"Grabacion\". Y \"Quemar\" deberia ser \"Grabar\". \n\n:-)"
    author: "KubuntuUserExMandrake"
  - subject: "Re: getMyDistro"
    date: 2006-06-04
    body: "gracias ya hice la actualizaci\u00f3n. \n\n"
    author: "Patrick Aljord"
  - subject: "Bookmarklets"
    date: 2006-06-03
    body: "Did anyone get this suite to work in Konqueror?  \nhttp://slayeroffice.com/?c=/content/tools/suite.html \nLooks good in Firefox, but doesn't seem to do anything in Konqi. \n\nAre there any known limitations w.r.t. Konqi?  \nHow do you debug this stuff?  \nIs there a collection of bookmarklets known to work in Konqi?\n\n"
    author: "cm"
  - subject: "The link to bookmarklets article is obsolete"
    date: 2006-06-03
    body: "Use this one -- http://liquidat.wordpress.com/2006/04/19/konqueror-with-bookmarklets/\n"
    author: "Matej Cepl"
  - subject: "usability "
    date: 2006-06-03
    body: "Isn't that overengineering? Consultations on how to write a HIG guide? Sure, for funded projects it would not surprise me, that you need to write baseless reports for third parties. But that isn't hackers style. \n\nIt is part of our pragmatic culture that we keep the documentation level low and just do the stuff. I don't see why a HIG guide shouldn't be developed like this. Set up Media wiki, create content, let others review and comment and apply changes and care about the text. HIGs are developed evolutionary. "
    author: "pentamax"
  - subject: "Re: usability "
    date: 2006-06-04
    body: "There is nothing wrong with having real usability experts work jointly with KDE developers, just like it is wonderful the early involvement and input that artists are having in the development of KDE 4.0.\n\nKDE is not allergic to research and we only stand to benefit from having different voices in our community heard."
    author: "Gonzalo"
  - subject: "Re: usability "
    date: 2006-06-04
    body: "Agreed 100% Gonzalo. KDE is IMHO already more useable than WinXP (that I am forced to use at work), but the more usable the better. I love to see input from artists and usability experts. I don't want KDE for hackers. I want KDE for human beings :-)\n\n"
    author: "KubuntuUserExMandrake"
  - subject: "Re: usability "
    date: 2006-06-04
    body: "There's nothing preventing anyone (besides licensing) from taking the test and dumping it into a wiki.\n\nThat should probably be done anyway.\n\nCheers,\nBen"
    author: "BenAtPlay"
  - subject: "Kopete"
    date: 2006-06-04
    body: "And nobody talks about Kopete???\nI've compiled it, in my gentoo box and it runs smoothly, and the new styles are great.\nMuch fast!!! Congratulations to all devs!!!\n\nNow I would like to see more artists making some more eye-candy styles, and Emoticons.. "
    author: "Kanniball"
  - subject: "Re: Kopete"
    date: 2006-06-04
    body: "There are no details on the release though. Has there been any progress on the video front ? \n\nThanks !"
    author: "KubuntuUserExMandrake"
  - subject: "Re: Kopete"
    date: 2006-06-04
    body: "http://kopete.kde.org/roadmap.php"
    author: "Anonymous"
  - subject: "Re: Kopete"
    date: 2006-06-05
    body: "To my understanding, Webcam support for MSN and Yahoo works for both send and receive, but it appears audio is still unsupported?  Anyone care to confirm?  It would help to have a proper detailed release announcement.\n\nCheers!\n\nJohn."
    author: "Odysseus"
  - subject: "Re: Kopete"
    date: 2006-06-05
    body: "\"but it appears audio is still unsupported?\"\n\nIt looks that way. Just got a message to upgrade my MSN:-), because my version does not support voiceclips. So I'd gather something related to audio is missing at least. "
    author: "Morty"
  - subject: "Re: Kopete"
    date: 2006-06-04
    body: "Screenies?"
    author: "Carsten Niehaus"
  - subject: "Re: Kopete"
    date: 2006-06-04
    body: "Where you would expect them: http://kopete.kde.org/screenshots.php"
    author: "Anonymous"
  - subject: "Re: Kopete"
    date: 2006-06-04
    body: "Kopete style is compatible with adium styles so you can get them all here http://www.adiumxtras.com/index.php?a=cats&cat_id=5&sort=date_reviewed"
    author: "Patrick Aljord"
  - subject: "Re: Kopete"
    date: 2006-06-05
    body: "There very good improvements in Kopete, I have however some problems, anyone else have these problems?\n\n- IRC is broken, I'm unable to connect to any existing channels, I always open a new channel with the same name\n- I'm unable to make voice/jingle work, I can initiate a voice-session and it gets accepted, however we hear nothing, anyone have some succes with it and can share some tricks and tips? Does it matter what jabber server I use?\n- I really hope that they release another 0.12.x release with more complete translation.\n\nAnyway Kopete looks very promissing. Thanks a lot!!\n"
    author: "LB"
  - subject: "Re: Kopete"
    date: 2006-06-05
    body: "meh, gaim 2.0betas seems work well enough"
    author: "Bruce"
  - subject: "Re: Kopete"
    date: 2006-06-05
    body: "Uh oh, gaim is even uglier than kopete."
    author: "prey"
  - subject: "Re: Kopete"
    date: 2006-06-05
    body: "but it's working much better and is always top compared to kopete :P"
    author: "blubb"
  - subject: "Re: Kopete"
    date: 2007-09-20
    body: "Hahahahaha\noh man that was pretty good...\n\noh, you're serious?\n\nI guess by working much better, you mean \"crashes often with little or no explanation\" and \"doesn't have webcam support\".  Or maybe you mean \"looks like a baby seal that was beaten to death with a rake\".\n\nI could see any of those making more sense than kopete coming on top.  Its actually embarrassing that the GNOME native multi-messenger client is less stable than kopete is in GNOME."
    author: "doctor"
---
Kopete branched from KDE to release <a href="http://kopete.kde.org/news.php#itemKopete0120Released">Kopete 0.12.0</a> *** ArkLinux released <a href="http://www.arklinux.org/index.php?option=com_content&amp;task=view&amp;id=15&amp;Itemid=1">2006.1-rc2</a> with a focus on KDE VoIP programmes. *** We were sent a link to <a href="http://getmydistro.blogspot.com/">GetMyDistro</a>, a nifty Qt 4 Windows application to download and burn your favourite KDE distro with bittorrent. *** Waldo pointed out <a href="http://www.gnomejournal.org/article/43/the-portland-project">an interview he did in Gnome Journal</a> about the Portland Projet and the news that <a href="http://www.desktoplinux.com/news/NS7478724750.html">LSB 3.1 was released</a>. *** We were sent this <a href="http://liquidat.blogspot.com/2006/04/konqueror-with-bookmarklets.html">article on bookmarklets in Konqueror</a>. *** Usability Celeste published the <a href="http://obso1337.org/hci/kde_hig/KDE4_HIG_Developers_Survey.pdf">results of her HIG survey</a>.




<!--break-->
