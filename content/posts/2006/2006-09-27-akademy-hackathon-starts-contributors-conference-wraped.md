---
title: "aKademy Hackathon Starts as Contributors Conference Wraped-up"
date:    2006-09-27
authors:
  - "agroot"
slug:    akademy-hackathon-starts-contributors-conference-wraped
comments:
  - subject: "Group photo with names"
    date: 2006-09-27
    body: "Would someone be able to create a version of that group photo with names? (like the WineConf group photo: http://www.winehq.org/images/wineconf06_grp.jpg). Thanks!"
    author: "Paul Eggleton"
  - subject: "Re: Group photo with names"
    date: 2006-09-27
    body: "It does have names.  Mouse hover or search.\n"
    author: "Jonathan Riddell"
  - subject: "Re: Group photo with names"
    date: 2006-09-28
    body: "Wow thats simply awesome (I was wondering what the Strigi box was doing there).  Though there seems to be a bug in it, if you type something then delete whatever you typed it will highlight everyone there."
    author: "Corbin"
  - subject: "Re: Group photo with names"
    date: 2006-09-28
    body: "Well, just consider it a feature: an empty string matches everybody. You can remove the boxes by reloading.\n\n"
    author: "Jos"
  - subject: "Re: Group photo with names"
    date: 2006-09-28
    body: "Ah right, cool. I didn't notice because it doesn't seem to work very well in Firefox (1.5.0.7, Win32 - I'm browsing from work, please don't hit me :) ). If you enter something in the search the captions appear but mouseover doesn't seem to function. Great idea though."
    author: "Paul Eggleton"
  - subject: "Re: Group photo with names"
    date: 2006-09-28
    body: "Actually I was wrong, it is working :)"
    author: "Paul Eggleton"
  - subject: "Re: Group photo with names"
    date: 2006-09-29
    body: "The photo has names and you can even search on names thanks to Strigi."
    author: "Anonymous admirer"
  - subject: "Re: Group photo with names"
    date: 2006-09-29
    body: "> thanks to Strigi\n\nThe reference to Strigi is a joke only."
    author: "Anonymous"
  - subject: "Video of Plasma-Talk"
    date: 2006-09-28
    body: "In case anybody is interested: I have made a video of Aaron's talk about plasma available online.\nGrab it at: http://shyru.blogspot.com/2006/09/akademy-2006-plasma-video.html\n\nGreetings Shyru"
    author: "Shyru"
  - subject: "Re: Video of Plasma-Talk"
    date: 2006-09-29
    body: "Thanks a lot, was nice to watch!"
    author: "Martin Stubenschrott"
  - subject: "Re: Video of Plasma-Talk"
    date: 2006-09-29
    body: "Thanks for sharing!"
    author: "Lans"
  - subject: "Slides from Timetable?"
    date: 2006-09-28
    body: "It links to bio of the selected devs."
    author: "Ronaldst"
  - subject: "Re: Slides from Timetable?"
    date: 2006-09-28
    body: "nvm.  Some have PDFs of the slides."
    author: "Ronaldst"
---
The <a href="http://conference2006.kde.org/conference/program.php">KDE contributors conference part</a> of <a href="http://conferene2006.kde.org">aKademy 2006 </a> in Dublin
kicked off Saturday morning bright and early, much to the dismay
of those who had been out late the evening before at the registration
desk, conveniently located in a pub. Read on for a full report.














<!--break-->
<div style="border: thin solid grey; margin: 1ex; padding: 1ex; width: 500px">
<a href="http://static.kdenews.org/jr/akademy-2006-group-photo.html"><img src="http://static.kdenews.org/jr/akademy-2006-group-photo-wee.jpg" width="500" height="221" border="0" /></a><br />
<a href="http://static.kdenews.org/jr/akademy-2006-group-photo.html">aKademy 2006 Group Photo</a>
</div>

<p>
The inspirational Aaron Seigo started the conference
off in a confrontational manner by looking the KDE project community
in the eye and asking "who are we?". Punctuated by some awful music
and a rapid-fire slide-show of contributors, Aaron outlined what he
thought is the most important aspect of the KDE community:</p>

<blockquote>
	Building communities around Free Software and 
	bringing Free Software into communities.
</blockquote>

<p>This shows how we create code and exciting groups of people around
those code products but <i>also</i> bring those products into communities
where they are useful.
</p>
<p>After Aaron's keynote, the 180 attendees spread out over two parallel
tracks; the cross-desktop track contained talks on issues that affect
all of the Free desktops out there. The cross-desktop technologies
of DBus and Portland were explained briefly. We looked at standardisation
efforts such as the Linux Standards Base and security issues in web
browsers in general. Gnome's John Palmieri told us a nice allegory about inline
skates manufacturers and how competition (on the desktop) and cooperation
(in underlying technology) are not mutually exclusive. Embedded systems
from open platform printer manufacturer Ricoh and the future of X with Keith Packard rounded out the cross-desktop lineup.</p>

<p>In the room opposite cross-desktop, the KDE4 track focused on new
technologies for KDE4 and our user-centric workflow approach.
Ellen Reitmayr kicked off the track by Keeping Users in Mind.
After sketching some basic usability notions such as personas
and the trade-off between features and happiness she examined
the challenges faced by both application developers and library
developers in incorporating usability advice into their designs.</p>

<p>John Cherry from the OSDL spoke about the state of the Linux desktop,
in particular analysing multiple market studies and how that reflects
on the uptake of Free Software in various (national) markets.</p>

<p>The community entered the fray at the end of the day with
presentations by Claire, Martijn, Kevin and Inge. Organising
KDE events was the topic Claire and Martijn addressed. This was
a practical session with tips and checklists and a script for
the organization of trade-show visits and developer sprints.
Local groups events such as a BBQ can also gain benefits
from such a structured approach. Kevin Krammer brought the
message that providing *support* to individual users is an
important aspect of being a developer - you can't hide from it.
Selecting appropriate channels of communication is essential
to making your users more comfortable when dealing with their
support requests. Persistent channels such as forums can be
preferable to ephemeral channels like instant messaging because
they foster the creation of shared knowledge. Inge Wallin
covered some basic marketing principles and gave suggestions
about how to apply these to Free Software applications. He
emphasised that communicating about your program and showing
new developments, ideas and plans attracts new developers <i>and</i> users.</p>

<div style="border: thin solid grey; margin: 1ex; padding: 1ex; width: 240px; float: right">
<img src="http://static.kdenews.org/jr/akademy-2006-hacker-labs.jpg" width="240" height="180" border="0" /><br />
The hacker labs
</div>

<p>On Sunday morning Anne &Oslash;sterg&acirc;rd spoke about Women in Free Software and
the efforts undertaken to increase the participation of women in
all aspects of Free Software. This led to some lively discussion
about what the KDE project in particular can do to help.</p>

<p>Eric Laffoon spoke about the question of "how do you eat" (what buys
your cookies, for instance) and that a Free Software project
needs to have some way of keeping its developers alive. Here,
passionate developers (eager to learn and contribute)
are even more important than skilled ones (who have the
technical skills but not the passion). The messages from Inge's
talk about communications returned, and the talk wrapped up
with some fund-raising tips and techniques.</p>

<p>Coffee (with cookies, because the aKademy organisation team is really
cool) followed, and then the Programming and Applications tracks started.
Applications included the <a href="http://www.englishbreakfastnetwork.org/">English Breakfast Network</a>, the KDE code quality
checking site, the best practices of the Debian team responsible
for packaging KDE "extras", applications of KDE in schools 
and for craftsmen (a good example of how the KDE platform can
really support the development of niche applications) and the
state of KDevelop (tremendous technological innovations occur
there).</p> 

<p>Julian Seward showed remote virtual KDE execution by running
a complete KDE session in Valgrind. This helps discover errors
in running KDE code through Valgrind's suite of detection tools.
Valgrind has grown into a complete simulation tool framework,
now powerful enough to tackle software as complex as KDE is.</p>

<p>spectaKle is a new QtRuby/Korundum based application for doing digital
signs and video on demand. We could have used this at aKademy itself
to point participants to the right lecture hall during the day.
As it stands, paper and pen had to do the job. It is a cool illustration
of classical frustration with proprietary software development and
the itch-to-scratch inspiration for Free Software.</p>

<div style="border: thin solid grey; margin: 1ex; padding: 1ex; width: 240px; float: left">
<img src="http://static.kdenews.org/jr/akademy-2006-googleplex.jpg" width="240" height="180" border="0" /><br />
Outside the Googleplex
</div>

<p>Stephan Kulow showed the technical implementation of usability 
testing at Novell, where they videotaped users trying to perform
a range of tasks with KDE. The puzzled look on the guys face when
the k-menu doesn't do what he expected speaks <i>volumes</i>. It
made us laugh, but also realise all the more that what we think
is usable may not be so for people with different expectations
or background. A new implementation of the K-menu was
demonstrated, which was very impressive in how it integrated
usability improvements with new and old features (like the
hidden calculator in the miniCLI).</p>

<p><a href="http://conference2006.kde.org/conference/program.php">Slides are available</a> by following the links from the timetable, <a href="http://wiki.kde.org/tiki-index.php?page=Talks+%40+aKademy+2006">write-ups from some of the talks</a> are available and videos should be online soon.  The <a href="http://wiki.kde.org/tiki-index.php?page=Pictures+%40+aKademy+2006">photos wiki page</a> lets you see the fun and for views and opinions remember to read <a href="http://planetkde.org/">Planet KDE</a>.</p>

<p>On the Monday we enjoyed the hospitality of sponsors Google at their European headquarters for some very welcome food, beer and a fridge full of ice cream.</p>









