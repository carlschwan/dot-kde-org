---
title: "KDE and Distributions: SabayonLinux"
date:    2006-07-27
authors:
  - "hnordin"
slug:    kde-and-distributions-sabayonlinux
comments:
  - subject: "Distributions"
    date: 2006-07-27
    body: "The main task is to ensure that distributions come closer to each other. KDE has to integrate as much system functionality as possible to ensure that no madness happens on the level of the distributors. Why not integrate Yast2 into KDE with backends for each distro family?"
    author: "funbar"
  - subject: "stupid KIOSLAVES!"
    date: 2006-07-27
    body: ">> Fabio: Better networking and kioslaves management. Well... what's the difference between /media and media:/ ? Or what's the difference between /home/username and system:/home? For someone, only confusion, but that's my personal point of view.\n\nTHANKS FOR SAYING THAT!!!!\n\ni really hate these absolute senseless kioslaves, too. they just make everything more complicated! \nget rid of them, better now than later!\n\ndont forget: theres not only system:/home theres also home:/ :@"
    author: "hannes hauswedell"
  - subject: "Re: stupid KIOSLAVES!"
    date: 2006-07-27
    body: "Totally agreed! That particular sets of ioslaves was supposedly to increase usability, but I don't think it's any well considered thoughts behind the design. More like some random ideas that something like this would surely increase usability. Since it mask some of the underlying filesystem, and \"everyone\" knows that is not userfriendly. And as an improvement we get a inconsistent mess, and the concept creates lots of bugs in applications (Like k3b and lots of other apps, treating home:/ as a non local disk).\n\nHopfully the whole mess gets removed in KDE 4."
    author: "Morty"
  - subject: "Re: stupid KIOSLAVES!"
    date: 2006-07-28
    body: "I like it very much. It is great. Please don't remove it.\n\nI would like to know whether the distributor uses testing software such as\ntestlink.sf.net."
    author: "funbar"
  - subject: "Re: stupid KIOSLAVES!"
    date: 2006-07-27
    body: "Agreed, it's a PITA."
    author: "Mark Kretschmann"
  - subject: "Re: stupid KIOSLAVES!"
    date: 2006-07-28
    body: "I also agree - it is confusing rather than user-friendly"
    author: "claes"
  - subject: "Re: stupid KIOSLAVES!"
    date: 2006-07-28
    body: "home:/ = system:/users \n\nwhile\n\nsystem:/home = home:/username = /home/username = system:/users/username\n\nomfg.\n"
    author: "MamiyaOtaru"
  - subject: "Good name"
    date: 2006-07-28
    body: "http://www.gnome.org/projects/sabayon/"
    author: "pete"
  - subject: "Re: Good name"
    date: 2006-07-28
    body: "Try to compare the strings \"sabayon\" and \"sabayonlinux\". The result is !!False!!"
    author: "andrew"
  - subject: "Re: Good name"
    date: 2006-07-28
    body: "...and sabayon is not a registered trademark: http://en.wikipedia.org/wiki/Sabayon\n"
    author: "andrew"
  - subject: "Re: Good name"
    date: 2006-07-28
    body: "Good comebacks...well done"
    author: "pete"
  - subject: "Wh00t?"
    date: 2006-08-04
    body: "\"I want to start by saying that I am proud to be Italian\"\n\nProud to be a tattletale?"
    author: "Wrath of Frings"
  - subject: "Re: Wh00t?"
    date: 2006-08-11
    body: "huh?  \n\nSabayon's a great showcase for KDE.  Hopefully it'll attract more people and distro's to use it as their default DE.\n\nPS: What is this Wrath of Frings on about?  Great interview, pity about the dumb comment above!"
    author: "sevinster"
  - subject: "One-man distros = I'm a bit worried"
    date: 2007-07-20
    body: "I am always a bit worried with one-man distros...give me the jitters that the developer might have a tiny detail which could ruin everything. no offense to Fabio here, i tried the sabayon live cd and i was impressed by its performance. But i hope plenty of heads would be able to supplement the great mind of the developer...and im sure, this would become one of the best distros ever."
    author: "Jack G"
---
<a href="http://www.lxnaydesign.net/">Sabayon Linux</a> is quite a new addition to the family of KDE distributions. It first came into existance on the Gentoo Forums as RR4/RR64 and was designed to provide a fast and easy way to get a Gentoo system with extras. After the inital success, founder and developer Fabio Erculani decided to turn this project into a fully fledged distribution. It was also decided that a new name was needed and thus  SabayonLinux was born. Fabio Erculani talks about the past, current status and the future of SabayonLinux.










<!--break-->
<h2>Past</h2>

<h3>Can you tell us about the history of your distribution?</h3>

<p><strong>Fabio:</strong> I want to start by saying that I am proud to be Italian, even if, unfortunately, I don't speak much Italian with other developers. Obviously, I am proud to use the best desktop environment of the *nix world, too!</p>

<p>By the way, as with most of my projects, this project was started to satisfy my personal needs, it was the end of 2004, I was running some Gentoo servers and I wanted to have a rescue live CD that supported a lot more features than the official one. And that's it, called "Gentoo Rescue Reiser4 LiveCD", sporting Reiser4 filesystem support, X.Org, GNOME, KDE, NX (introduced later) and Project Utopia. Gentoo Forums, for a lot of time were my home, until I started to work on a well designed website, in mid-2005. I was nearly the first to build a graphical live CD based on Gentoo and I was the #1 to build a live DVD too. Well, most people currently know my work with the name of RR4 and RR64 Linux, an installable live DVD with the most exciting applications and technologies built into it. Along with them, there are its miniEdition releases, featuring the same multimedia characteristics of their fathers but fitting on a single CD and much more faster on memory constrained systems. This was made possible, only thanks to the Gentoo GNU/Linux Community and their efforts. Since some weeks, we have started to migrate the website, the forums and even the distro artwork and name to a new brand: SabayonLinux. So, if someone gets confused, this interview should help ;)</p>


<h3>Why did you choose KDE and which version of KDE did you first implement?</h3>

<p><strong>Fabio:</strong> The first editions of Rescue Reiser4 LiveCD (abbreviated RR4 :P), were running GNOME. Not because KDE wasn't good, but because I had never used KDE intensively even if it was always available on the CD. Bored from too many critical bugs from GNOME, the "hard to manage without gconf-editor" XML configuration files and the amazing work that has been done for KDE 3.4, I started to work on KDE integration, and that's the result: a nice and good looking desktop, nice killer applications such as Amarok, Kaffeine, K3b, KNetworkManager.</p>

<p>I am trying to bug as much as possible (read 24h/day :P) every KDE developer to try to get some annoying bugs fixed, like the system:/ kioslave related ones.</p>

<h3>How did you find initial support for a new distro?</h3>

<p><strong>Fabio:</strong> I counted only on myself. I grab and even steal every good idea from the users and try to implement that. I do not follow a timed release schedule, because it's something stupid in my opinion, I prefer to follow a feature release schedule. Working alone is quite hard, but if you believe on what you are doing, you can do everything you want. An example? Try to compare the latest RR4 release with any other KDE-based distribution and keep in mind that I've always worked alone. The results? Just let me know...</p>

<h3>What could KDE have done better to help new distros use KDE?</h3>

<p><strong>Fabio:</strong> Create a better comunication channel between KDE project and us.</p>

<h3>What were your first impressions about KDE's documentation and community?</h3>

<p><strong>Fabio:</strong> From the development side, KDE documentation is quite good, from the user side, it can be improved without any large effort.</p>

<h2>Present</h2>

<h3>How closely do your releases depend on KDE releases?</h3>

<p><strong>Fabio:</strong> I tend to wait a little bit more if I know that a new bugfix release is coming.</p>

<h3>Do you have a clear target audience for your distro?</h3>

<p><strong>Fabio:</strong> Yes, the "just works" philosophy is a must for tired and lazy Gentoo users :) And often, KDE just works.</p>

<h3>Do you have any user feedback mechanism?  If so, what feedback do they
have about KDE?</h3>

<p><strong>Fabio:</strong> We use forums and mailing list for the development and I've always seen that KDE feedback is not bad at all.</p>

<h3>In what ways do you customise the version of KDE that ships with your distro?</h3>

<p><strong>Fabio:</strong> I tend to use a standard Portage (aka the Gentoo package manager) release but if I find something that needs to be fixed, I try to fix it using websvn.kde.org and the diff utility :P</p>

<h3>What are the biggest strengths of KDE for your distro?</h3>

<p><strong>Fabio:</strong> Nice look and feel, nice localisation management, nice integration and nice killer applications.</p>

<h3>What are the biggest weaknesses?</h3>

<p><strong>Fabio:</strong> Not completely perfect Konqi's Javascript management (see the Ebay image upload bug), broken system:/ or media:/ kioslave layout for non-KDE apps and missing KDirWatch support (that I hope will be fixed within two next minor releases), broken standard floppy management using media:/ and some very old bugs that are still open, like the "KMail and disk full bug". But I think that they will be completely addressed for the KDE4 release.</p>

<h3>What KDE applications are the most popular among your users?</h3>

<p><strong>Fabio:</strong> Amarok, we love it. And we hope that someday someone will add iPod Video and even video functionality support. Merge Kaffeine (or Codeine) with Amarok? Why not!</p>

<h3>Do you feel that you have a good relationship with the KDE community?</h3>

<p><strong>Fabio:</strong> Yes, they're smart people! I love Aaron Seigo (and not only) point of view. It's the users' point of view.</p>

<h2>Future</h2>

<h3>What feature would you as a distro maintainer like to see in KDE?</h3>

<p><strong>Fabio:</strong> NetworkManager integration. XGL/AIGLX support for Kwin and remove the K-this - K-that names. I prefer latin names. KDVI, KFaxView, KGhostView are bad. Digikam, Amarok, Plasma, Solid, etc... ARE GOOD!.</p>

<h3>Is the extended 4.0 release cycle an issue for your distro?</h3>

<p><strong>Fabio:</strong> Issue? Maybe... but if we'll have some more bugfix releases, that's not a problem. You just need to fix the most annoying bugs.<p>

<h3>What are you most looking forward to about the 4.0 release?</h3>

<p><strong>Fabio:</strong> Better networking and kioslaves management. Well... what's the difference between /media and media:/ ? Or what's the difference between /home/username and system:/home? For someone, only confusion, but that's my personal point of view.</p>

<h3>Do you plan any involvement in the beta/RC releases of the 4.0 release?</h3>

<p><strong>Fabio:</strong> Yes, I want to bug and spam you if something does not work properly :P</p>

<h3>Any other plans for your distro in the future?</h3>

<p><strong>Fabio:</strong> Complete the "Sabayon" name migration, add Anaconda installer thanks to the VLOS team, get some donations to buy and support Mactel computers, work hard to make things just work (everything). Yes, here in the Sabayon world, we need hens too, because without hens and their eggs, we cannot prepare our favorite italian dessert: sabayon :P. And yes, I am the farmer...</p>










