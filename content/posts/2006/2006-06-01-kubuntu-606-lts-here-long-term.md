---
title: "Kubuntu 6.06 LTS Here for the Long Term"
date:    2006-06-01
authors:
  - "jriddell"
slug:    kubuntu-606-lts-here-long-term
comments:
  - subject: "hooray for Kubuntu"
    date: 2006-06-01
    body: "great distro! and thank you for the 3.5.3 packages"
    author: "Tom"
  - subject: "Kubuntu is a publicity failure for KDE"
    date: 2006-06-01
    body: "Kubuntu is a publicity failure for KDE \u0097 whenever someone mentions Kubuntu, they are also publicizing Ubuntu, a GNOME desktop. The result is that it is impossible to promote KDE without promoting GNOME. The reverse (promoting GNOME and not KDE) happens all the time (after all, Kubuntu is not a substring of Ubuntu). Even KDE devs often leave out the K, thereby promoting GNOME, when in fact they are referring to the KDE desktop. The current situation is a publicity disaster for KDE. \n \nThere's a very simple solution to solving this unfair naming: call the the KDE desktop Ubuntu KDE and the GNOME desktop Ubuntu GNOME. If that happens, KDE will *immediately* become the most popular desktop on the Ubuntu platform. Mr. Shuttleworth, all I'm asking is that you give the KDE desktop a fighting chance by naming it fairly!"
    author: "AC"
  - subject: "Re: Kubuntu is a publicity failure for KDE"
    date: 2006-06-01
    body: "> Kubuntu is a publicity failure for KDE \u0097 whenever someone \n> mentions Kubuntu, they are also publicizing Ubuntu, a GNOME \n> desktop. The result is that it is impossible to promote KDE \n> without promoting GNOME.\n\nGet a life."
    author: "BD"
  - subject: "Re: Kubuntu is a publicity failure for KDE"
    date: 2006-06-02
    body: "Get a life."
    author: "illissius"
  - subject: "Re: Kubuntu is a publicity failure for KDE"
    date: 2006-06-02
    body: "is this a vote of some kind?  :)\nin which case let me support GP's view... it's bluntly expressed but nevertheless a valid concern.\nUbuntu/Kubuntu is pretty much like \"man\" and \"woman\", where the generic word is \"man\"/\"mankind\"... if history is any indication, we'll have a tough time reaching sexual equality ;)\n"
    author: "FE"
  - subject: "Re: Kubuntu is a publicity failure for KDE"
    date: 2006-06-02
    body: "AC, get a job and then you can start your own distro and name it whatever you want."
    author: "Rick"
  - subject: "Re: Kubuntu is a publicity failure for KDE"
    date: 2006-06-02
    body: "You are a moron.  If you really think that tacking \"KDE\" at the end of \"Ubuntu\" is going to suddenly make people aware of and prefer the KDE desktop, then you're problems go well beyond mere ignorant fanboy allegiance.  If people were confused about which version (Ubuntu, Kubuntu, Edubuntu, etc) used KDE, they could easily find out through google.  I would suggest that the majority of users coming to the distribution are intelligent enough to determine which DE they would like to use in order to choose appropriately.  As an aside, if you think the acronym \"DE\" advertises GNOME in particular, then you're doubly moronic.  GNOME is a desktop environment just as KDE is.  The only difference is that KDE includes the \"desktop environment\" in their acronym (K Desktop Environment) as opposed to GNOME (Gnu Network Object Model Environment).  So, if anything, when a developer says \"DE\" he's closer to advertising KDE than GNOME."
    author: "cius"
  - subject: "Re: Kubuntu is a publicity failure for KDE"
    date: 2006-06-03
    body: "In a fight or in war, you never ask from your enemy a fair fighting chance! Its considered an ultimate shame. In such a situation, committing suicide is honourable. \n\nIf you want to promote KDE without promoting GNOME, promote a KDE ONLY distro such as Tomahawk Desktop.\n\nTomahawk Computers\nhttp://www.tomahawkcomputers.com/"
    author: "Tomahawk Computers"
  - subject: "Re: Kubuntu is a publicity failure for KDE"
    date: 2006-06-15
    body: "I tell people to install KDE, XFCE, and GNOME and try them all. I would MUCH rather see people pick their favorite than to pick my favorite. Sure, I tell them why I prefer KDE over the others (esp. GNOME), but I tell them to pick their favorite rather than just picking my favorite. Since KDE, XFCE, and GNOME are all free software, I don't care which one people use."
    author: "gnulinuxman"
  - subject: "Re: Kubuntu is a publicity failure for KDE"
    date: 2006-06-03
    body: "Kubuntu is not synonomous with gnome. Everyone knows this. If you want kde AND gnome in kubuntu or ANY of the other distros, you just install it!\n\nI don't think anyone's trying to start another Microsoft here...  Being the most popular distro in linux is something Kubuntu is already on top of even though it is not important at all..  It is popular already because it works well, and installs easily.\n\nPublicity failure for KDE? No. Go to www.kde-look.org and tell me what you see. Are people using KDE? Yes they are. Albeit it might be difficult for one that has never used linux to know the difference between the two. \n\nLet me throw you for a loop if you don't mind; I'm using Kororaa linux and it is KDE based but also includes gnome by default. There however is also Gororaa, wich is Gnome based. So, Kubuntu is not the only one with name variations.\n\n"
    author: "Joe"
  - subject: "Re: Kubuntu is a publicity failure for KDE"
    date: 2006-06-15
    body: "Not really. They're not calling it Gubuntu or anything like that.\n\nI really think you're blowing this out of proportion. The distro itself is called Ubuntu. The reason they supported GNOME only at first is because they didn't have anyone to maintain KDE, even though they still had it available in the Universe for anyone who wanted it. The distro grew so fast that they had good KDE support by the second official release, which I think is nothing short of impressive.\n\nStill, when I tried out Kubuntu the first time (Hoary), it had some bugs they needed to work out, but I liked it anyway, and by Breezy, it was marvelous. Dapper is even better! It is also very nice to be able to always run the latest KDE release, too. I just can't stand GNOME anymore since it hit 2.0, but KDE is getting better and better!"
    author: "gnulinuxman"
  - subject: "Re: hooray for Kubuntu"
    date: 2006-07-14
    body: "Even though this is a KDE site, Ubuntu works equally well with Gnome and KDE on the same machine.  I have run both and they each have their advantages and disadvantages.   I think it is silly at this point to argue in favour of one or the other.   The strong point about linux is that you can use whatever interface feels right at the present moment.  The strength of KDE is not the desktop itself but rather the many K applications such as ktorrent or koffice.   These run fine under Gnome as well so even someone running Ubuntu will still appreciate the KDE project for its amazing applications.\n\nI would recommend that the open source people such as ourselves give up the \"Them vs Us\" viewpoint.   We are not trying to sell a product.  Personally I enjoy the variety and run KDE, Gnome and Xfce4 according to my mood.   \n\nThis is a non-issue."
    author: "Ian Soutar"
  - subject: "Thinko in the article?"
    date: 2006-06-01
    body: "\"also includes some of the best KDE software available for Microsoft Windows\"\n\nWhat does that mean?"
    author: "Andy Parkins"
  - subject: "Re: Thinko in the article?"
    date: 2006-06-01
    body: "That there are win32 binaries of those softwares"
    author: "Davide Ferrari"
  - subject: "Re: Thinko in the article?"
    date: 2006-06-01
    body: "You have to download them as part of the ISO even if you don't use/want them."
    author: "Anonymous"
  - subject: "Re: Thinko in the article?"
    date: 2006-06-01
    body: "All software on the CD you don't want you have to download with the ISO."
    author: "panzi"
  - subject: "Re: Thinko in the article?"
    date: 2006-06-15
    body: "Dang! My girlfriend and I would rather use them on Kubuntu than put them on Windows."
    author: "gnulinuxman"
  - subject: "Why didn't Dapper fix its CUPS printing problems??"
    date: 2006-06-01
    body: "I just installed Kubuntu Dapper (albeit it was from the RC1 iso image), and I dist-upgraded to the newest packages.\n\nBut I can't print to our company's CUPS 1.1 print server!\n\nFirst of all, kprinter does not even see the shared printers offered from this CUPS box. Some very paranoid Canonical packages seemed to have disabled everything that makes CUPS user friendly (CUPS browsing, CUPS administration via the web interface); they seem to have gone as far as even disabling printing to a 1.1.x CUPS server altogether! Boy, that suxxxx...\n\nSecond, even if I force my kprinter to connect to the remote CUPS server, it does not print, but it gives me the error message \"(400) -- Bad Request\". (Note, I know quite a bit about CUPS, and it works fine with a Kubuntu Breezy client!).\n\nThird, they seem to be clueless as far as the benefits of the new cool SNMP backend goes: they have disabled it altogether. Which means: no auto-discovery of network printers in one's own neighbourhood when installing a new queue, no (semi-)automatic driver selection for users! Are we going to see another case of that?\n\nTo me it looks like none of the problems which Kurt Pfeifle outlined in his blog 2 months ago have been fixed:\n\n######## . . .   http://www.kdedevelopers.org/node/1899   . . . ########\n\nHow should Linux ever get ready for the desktop if supposedly user friendly distros do unexplainable things like that?\n\nWe all remember the rant of ESR, which he (wrongly) directed towards CUPS developers, when he was unable to configure his home printer because the RedHat printer config tool did mess up what CUPS itself would have done just fine... Is this another opportunity to flame Mike Sweet and the CUPS.org folks, just because their marvellous work gets spoiled by overshooting distro \"experts\" who make Linux unusably secure and safe??\n\n"
    author: "ex-suse"
  - subject: "Re: Why didn't Dapper fix its CUPS printing problems??"
    date: 2006-06-01
    body: "If it makes you feel better, Suse has a pretty strange way of understanding security vis-a-vis network printing via CUPS.\n\nAs you point out, lots of the most useful CUPS features are either removed, disabled in such a way that it takes a good chuck of time to re-enable them or just broken, although I believe it is slightly better than Kubuntu in this respect, which I remain unable to make it see my CUPS enabled network printer.\n\nIn Suse, although the CUPS autodiscovery didn't work, the printer prints after I give it the full URL of the CUPS printer."
    author: "Gonzalo"
  - subject: "Re: Why didn't Dapper fix its CUPS printing proble"
    date: 2006-06-01
    body: "I found this in the KDE 3.5.3 changelog:\nhttp://www.kde.org/announcements/changelogs/changelog3_5_2to3_5_3.php\n\"KDEPrint: Fix usage of private methods in CUPS which broke KDEPrint with CUPS 1.2 (bug #124157)\"\n\nIf you haven't, I would try upgrading to KDE 3.5.3 (Kubuntu 6.04 comes with 3.5.2).\nMore info here: http://kubuntu.org/announcements/kde-353.php"
    author: "Elijah Lofgren"
  - subject: "Re: Why didn't Dapper fix its CUPS printing proble"
    date: 2006-06-01
    body: "Thanks for trying to console me with your kind words, Elijah!\n\nBut alas!, KDEPrint's problems are of a different nature. (Also, Kurt deals with these in a different blog entry:  http://www.kdedevelopers.org/node/1901 )\n\nWhat my griping here is about, is that...\n\na) ...commandline printing does not work from Dapper to a CUPS-1.1.x server, it gives \"Bad request\" errors\n\nb) ...the default CUPS web interface features are deliberately crippled on Dapper, supposedly for the sake of better \"security\" \n\nc) ...the changes are not sufficiently documented, users are left without info how to fix it\n\nd) ...if users turn to the \"official\" CUPS documentation, they are screwed, because that documentation does not match with what Canonical/Ubuntu/Dapper ships\n\ne) ...the new snmp backend of CUPS that implements one of the OSDL Desktop Printing Summit decisions for easier autoconfiguration of printers is simply ignored\n\nAll of these are not fixed by any KDE upgrade.\n\nWhat I did not yet say is that I really take issues with that type of super-smart *packagers* who try to decide these things on behalf of me, and in contrast to what the CUPS *developers* recommend as defaults, but who on the other hand seem to lack the smarts of shipping a CUPS 1.2.1 version (as was released 10 days ago) with more than 30 new bug fixes over 1.2.0 (shipping today in Dapper)...\n"
    author: "ex-suse"
  - subject: "Re: Why didn't Dapper fix its CUPS printing proble"
    date: 2006-06-04
    body: "Well file a bug report then (after of course updating all packages). They would probably be delighted to fix it in the next release (Edgy Eft : 6.10).\n"
    author: "Sunny Sachanandani"
  - subject: "Re: Why didn't Dapper fix its CUPS printing proble"
    date: 2006-06-02
    body: "Agreed, this is something that could have been done better. As a non-cups expert it took me about an hour to figure out how to turn the browsing on and to print on my old ubuntu box (with self compiled kde) from my new kubuntu dapper box. Reminded me of all the fun of the old days.\n\nThe details on how to turn the browsing on was available in the ubuntu forums and I think it is a minor point for what is a very good distibution."
    author: "hbc"
  - subject: "Re: Why didn't Dapper fix its CUPS printing proble"
    date: 2006-06-05
    body: "Browsing in CLI/KDE/GNOME/whatever can be enabled with one command:\n\nsudo /usr/share/cups/enable_browsing 1\n\nSharing you printers in CLI/KDE/GNOME/whatever can also be enabled with one command:\n\nsudo /usr/share/cups/enable_sharing 1"
    author: "ivoks"
  - subject: "Re: Why didn't Dapper fix its CUPS printing proble"
    date: 2006-08-13
    body: "Thank you su much for posting this here!\nThank you thank you thank you.\nNow I can get some sleep."
    author: "Thore"
  - subject: "Re: Why didn't Dapper fix its CUPS printing proble"
    date: 2006-06-05
    body: "But the thing with CUPS1.2 is that browsing/printing with older 1.1 CUPSes really *is* broken in CUPS, not by Ubuntu. Take a look at CUPS1.2.1 changelog:\n\nhttp://www.cups.org/relnotes.php"
    author: "ivoks"
  - subject: "Re: Why didn't Dapper fix its CUPS printing proble"
    date: 2006-06-05
    body: "BTW, no one is flaming Mike. Bugs do happen and bugs get fixed. This is how (opensource) software works."
    author: "ivoks"
  - subject: "Re: Why didn't Dapper fix its CUPS printing proble"
    date: 2006-08-08
    body: "You can switch on CUPS browsing from the GUI in Ubuntu (although not sure about Kubuntu as always is second class)\n\nI found Ubuntus support in printers to be far better than any other distro, although a little confusing because all my FC4 friends had IPP Browsing by default."
    author: "Martin Owens"
  - subject: "How is this a kde-news?"
    date: 2006-06-01
    body: "There are so many distros using KDE, I don't see the point, why this has to be mentioned here."
    author: "Anonymous Coward"
  - subject: "Re: How is this a kde-news?"
    date: 2006-06-01
    body: "They are all mentioned here"
    author: "yaac"
  - subject: "Re: How is this a kde-news?"
    date: 2006-06-01
    body: "Yep, and if you notice a release that is not reported, please contribute an article about it :)"
    author: "rinse"
  - subject: "KDE"
    date: 2006-06-01
    body: "I like KDE so much. It is a fantastic desktop. I feel that KDE 4 could be released too late and KDE 3.5.3 will be outdated then. This gives Gnome an opportunity to take over."
    author: "Faber"
  - subject: "Re: KDE"
    date: 2006-06-01
    body: "not likely, gnome is still releasing stuff that has been in KDE for years as 'new features', they'll have a hard time catching up to 3.5.x - and as some features go into the 3.5.x series, the next release of Kubuntu will be even better."
    author: "superstoned"
  - subject: "Re: KDE"
    date: 2006-06-01
    body: "... and in other news the sky is falling"
    author: "Aaron J. Seigo"
  - subject: "Re: KDE"
    date: 2006-06-01
    body: "I'm actually reading a book about that... so far it seems careful application of the right amount of Magic has managed to revert the impeding disaster though.\n\nYou can read about it yourself: http://www.amazon.com/gp/product/0061020702/qid=1149191790/sr=1-1/ref=sr_1_1/102-0806347-9830558?s=books&v=glance&n=283155"
    author: "Waldo Bastian"
  - subject: "Re: KDE"
    date: 2006-06-01
    body: ">> feel that KDE 4 could be released too late and KDE 3.5.3 will be outdated then.\n\ndon't worry, there probably will be more kde-versions in between (e.g. kde 3.5.4 etc) and kde applications continue to evolve and grow.."
    author: "Rinse"
  - subject: "Re: KDE"
    date: 2006-06-02
    body: "Exactly, having the solid foundation of KDE 3.5 the interesting part is really in the area of the application developers. \n\nThe real power of KDE is shown in the continuous releases of high quality applications from the extragear modules and other 3rd party venues. Applications like DigiKam, KTorrent, amaroK, BasKet, Kdissert and Codeine to mention some. In addition there will be new releases of KOffice and separate releases of some of the applications from the regular KDE packages, like Kopete and KDevelop.\n\n"
    author: "Morty"
  - subject: "Re: KDE"
    date: 2006-06-02
    body: "lmms\n"
    author: "Menge"
  - subject: "Re: KDE"
    date: 2006-06-03
    body: "I have just tried GNOME 2.14 on Debian/sid and I have to admit that it is probably the coolest looking desktop I've ever seen (commercial or otherwise). However, this absolutely positively feeling got broken in the moment I actually tried to do something. I was not able to play even OGG streams. And so on. Fortunately I found klearlook style on kde-look.org (http://www.kde-look.org/content/show.php?content=31717, together with Clearlooks window decoration http://www.kde-look.org/content/show.php?content=37009 and dekorator; Debian packages are here http://matej.ceplovi.cz/progs/debian/klearlook) and I have same sleek look and functional applications ;-)."
    author: "Matej Cepl"
  - subject: "All the hoo-ha about Ubuntu."
    date: 2006-06-01
    body: "Why would you bother, when Kanotix works so well, has not abandoned Debian and defaults to KDE in all its glory? I'm tolerant, so I suppose that Gnome and MS Windows are OK too, if you like that sort of thing!"
    author: "Bob Buick"
  - subject: "Re: All the hoo-ha about Ubuntu."
    date: 2006-06-02
    body: "that's the beauty of free(dom) software: people bother because they care to. you are doing the same by raising awareness of kanotix. various people have various expectations and requirements. we can cover these only with effort and understanding"
    author: "Aaron J. Seigo"
  - subject: "PIM"
    date: 2006-06-01
    body: "http://kubuntu.org/images/winfoss.png\n\nMentions KDE-PIM for Windows. Is this still the old stoneage release or a new version?"
    author: "Menge"
  - subject: "lots of bugs this time around"
    date: 2006-06-02
    body: "http://gpiancastelli.wordpress.com/2006/06/01/worst-kubuntu-upgrade-ever/ : Be careful with the Kubuntu upgrade process.\n\nI never had good experiences with Kubuntu, it felt like a hack to cross-breed KDE on a GNOME-based infrastructure... but I respect the great work many kubunters are doing for KDE. \nKanotix is much better if you want a no-nonsense Linux/KDE install, but it gets less publicity because of its german roots.\n\nGiacomo\n(who did the big switch from Windows to Linux with Kanotix, ad it's now a happy Gentoo user)\n\n"
    author: "Giacomo"
  - subject: "Re: lots of bugs this time around"
    date: 2006-06-02
    body: "well, kanotix brought me to linux, too, but it is far less polished compared to kubuntu. it uses mostly stock debian stuff, after all - which is what sets kubuntu apart from the other debian deriviates."
    author: "superstoned"
  - subject: "Re: lots of bugs this time around"
    date: 2006-06-02
    body: "At least CUPS and kprinter and printing work in Kanotix; unlike (K)Ubuntu Live CD or a (K)Ubuntu harddisk installation!"
    author: "ex-suse"
  - subject: "Re: lots of bugs this time around"
    date: 2006-06-03
    body: "So why can I print on different laser printers with Kubuntu 6.06? Am I dreaming?"
    author: "Davide Ferrari"
  - subject: "Yikes"
    date: 2006-06-04
    body: "Both KDE and GNOME are free software, so at least people don't use Windows or MacOS.  Although I use GNOME, I respect the personal preferences of others.  I mean this whole \"We shouldn't promote GNOME, but KDE, because I use it\" thing is rubbish.  You should promote free software, not your personal opinions."
    author: "Bruce"
  - subject: "Ubuntu=>GNOME vs Kubuntu=>KDE"
    date: 2006-06-04
    body: "Is this offical yet?\nI mean it seems like thats the directions the distros are moving in.\n\nBen\nPS. How the heck did gnome manage to fake smb: ioslaves in the latest version? have gnome devs finally realized that kio slave functionality is sweet?\n\n"
    author: "BenAtPlay"
  - subject: "kde 3.5.3"
    date: 2006-06-07
    body: "I have a suggestion for Jonathan Riddell. Instead of providing unsupported kde 3.5.3 packages, why don't you test kde 3.5.3 for a while then package it for ubuntu-updates?\n"
    author: "ht"
---
KDE based distribution <a href="http://kubuntu.org/announcements/6.06-lts-release.php">Kubuntu 6.06 LTS has been released</a>. It is available for <a href="http://kubuntu.org/download.php">download</a> now or for the first time you can order free Kubuntu CDs through <a href="https://shipit.kubuntu.org/">Shipit</a>. This release comes with KDE 3.5.2 (packages for 3.5.3 are available) and includes a new installer which you can use direct from the live desktop CD.  The Desktop CD also includes some of the best KDE software available for Microsoft Windows including KOrganizer, Kexi and Scribus, use it to convert your friends to Freedom.


<!--break-->
