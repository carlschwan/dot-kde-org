---
title: "A Galician Government Representative Meets KDE Translators"
date:    2006-07-21
authors:
  - "marce"
slug:    galician-government-representative-meets-kde-translators
comments:
  - subject: "Moola!"
    date: 2006-07-21
    body: "EUR120,000 to translate OpenOffice.org, wow! I know it's always good to pay people properly, but that figure surprised me. I wonder whether it would be better for local governments to set-up web sites like Pootle and invite their constituents to contribute, then perhaps hire some people to finish the job if it goes slowly or there are gaps?\n\nVery disappointed to see that nobody has started to translate ktuberling yet by the way ;-) (http://www.trasno.net/kde/)"
    author: "Tom"
  - subject: "Re: Moola!"
    date: 2006-07-21
    body: "not much - seams cheap"
    author: "chris"
  - subject: "Re: Moola!"
    date: 2006-07-23
    body: "Possibly just before chirstmass ;-)"
    author: "marce"
  - subject: "EU"
    date: 2006-07-21
    body: "Should be considered to ask the EU for support. Responsible for language policy is Commissioner Jan Figel."
    author: "hennes"
  - subject: "Re: EU"
    date: 2006-07-23
    body: "Could you give more details?"
    author: "marce"
  - subject: "Funny"
    date: 2006-07-21
    body: "Trasno's site was written in the galician language. It's funny to see that there's an english traslation, but no spanish one, even though as a part of Spain, they use spanish all time. :)\n\n"
    author: "Xenu"
  - subject: "Re: Funny"
    date: 2006-07-21
    body: "For some people im Galiza, \"Galiza is NOT Spain\"... you know... ;-)\n\nPS: And, off course, some people don't speak spanish."
    author: "Alexandre"
  - subject: "Re: Funny"
    date: 2006-07-21
    body: "\"don't speak spanish\" but can speak & understand one."
    author: "xama"
  - subject: "Re: Funny"
    date: 2006-07-21
    body: "That's because they care about us... they want us to learn English :-)"
    author: "JB"
  - subject: "Re: Funny"
    date: 2006-07-21
    body: "its like ukrainian and russian: native ukr-speakers love to pretend that they dont understand russian"
    author: "Nick"
  - subject: "Re: Funny"
    date: 2006-07-22
    body: "That's because you are supposed to visit Trasno's only if you are interested on the job, and you know, to translate to galician language all you nedd to know is en_US and gl."
    author: "marce"
  - subject: "120.000\u0080"
    date: 2006-07-21
    body: "just to know... who got that money?\nIt seems to me a lot for just a localisation, doesn't it?"
    author: "Pepe Corvina"
  - subject: "Re: 120.000\u0080"
    date: 2006-07-21
    body: "IMAXIN SOFTWARE\n\nhttp://www.imaxin.com/\n\nhttp://www.cesga.es/concursos/concurso4_05/DO_CSL_valoracion-concurso4_05.pdf\n\nWrong choice as AFAIK they were not the \"original upstream\" translators.\n\nShows how much goverments bypass the Open Source \"rules\" when doing that kind of things."
    author: "Albert Astals Cid"
  - subject: "Re: 120.000\u0080"
    date: 2006-07-21
    body: "on the other hand, the origional translators will probably keep translating as well, which means that this adds an additional party to the group of translators. but I would agree that it would have been nice if the origional translators had seen something of the money.\n\nbut I guess governments don't work that way. they probably just want this done, and this is the easiest way for them to get there.\n\nbut he! look at the bright side, at least we'll be getting a good translation.\njust say thank you to the nice people :p"
    author: "Mark Hannessen"
  - subject: "Re: 120.000\u0080"
    date: 2006-07-23
    body: "I smell corruption."
    author: "Luis"
  - subject: "Re: 120.000&#8364;"
    date: 2006-07-21
    body: "magic word (esp loved by former-USSR countries) - KICKBACK"
    author: "Nick"
  - subject: "about time too"
    date: 2006-07-21
    body: "It is difficult to understand that the Basque and Catalan regional governments in Spain are not much more involved in OpenSource. I remember the huge amount the Basque Government paid for Microsofts' Office localisation. If they used a fraction of that in open source we'd be much better off.\n\nAs to only Galician in a web page, well, if you speak Spanish well and you make an effort you can understand most of what's written in Galician. After all, we all had to study the Cantigas in school...\n"
    author: "ile"
  - subject: "Re: about time too"
    date: 2006-07-21
    body: ">After all, we all had to study the Cantigas in school...\n\nI didn't... but I agree it's easy to understand anyways."
    author: "Pepe Corvina"
  - subject: "Sure there is source code,"
    date: 2009-06-13
    body: "Sure there is source code, but on the svn server of KDE :-)\r\nWe are sending all our translations to the KDE server and you can get it from there.\r\nBTW. KDE 3.5 was released with the Khmer language pack included. So if we are lucky the distributions will pick it up.\r\n\r\n__________________\r\n\r\nDevis from @<a href=\"http://how-to-hide-ip.info\">change ip</a>\r\n"
    author: "deviswilliam"
---
Last Saturday, a representative from the <a href="http://www.xunta.es">Galician Government</a> in Spain met members of the <a href="http://www.trasno.net">Trasno project</a>.
This project includes Free Software volunteer translators for the Galician language, from a wide range of Free software projects including KDE.  The government representative was Mr. Antonio Pérez Casas, Adviser for the Information Society of the Industry and Innovation Councillor.



<!--break-->
<p>Six people came to the meeting from the Trasno team including Xavier García Feal, coordinator of the KDE galician language team, two other KDE translators, a couple of translators from Gnome and the author of the Galician ispell dictionaries.</p>

<p>The Adviser informed us that they are about to launch a forge site to host Galician free-software projects. The craze for on-line translation tools also arose when he noted that they were considering to set up a Rosetta like site for translations. This project will be closely related to <a href="http://mancomun.org">Mancomun</a>, another site recently launched motivated by the release of the first fully localised OpenOffice.org release.</p>

<p>The meeting had a friendly atmosphere, despite the recent frictions related to the OpenOffice translation.</p>

<p>We told the representative of the future plans and needs of the Trasno project. At the present time we are switching our web page to a wiki. To fullfill the desire of having a full desktop in the official Galician language, we urged the Councillor's adviser to improve the Galician Hunspell dictionary and to integrate this into KSpell. Additionally the attending translators noted the need for more volunteers. We expressed that we would prefer the government to evaluate <a href="http://pootle.wordforge.org/">Pootle</a> as an online translation tool.</p>

<p>The Councillor's Adviser noted that the regional ministry was not considering creating a new Linux distribution nor to massively deploy any other distribution.</p>

<p>After the meeting KDE's team chatted for a time, exchanging thoughts. This was our first face to face meeting.</p>

<p>We shared a lot of similar ideas.  Galicia has two official languages. Any deployment would demand both localisations. The Galician government has paid &euro;120 000 for the localisation of OpenOffice.org. But they are now finding that there is still a lot of work to do and it will be very expensive.  It would be cool, and a good sign, if the Trasno project could get a server with revision control and Pootle.  With a "project liaison" we could get in sync with upstream repositories, while still being able to work off-line.  We were also aware that we need more help.</p>

