---
title: "KDE Commit-Digest for 28th May 2006"
date:    2006-05-29
authors:
  - "dallen"
slug:    kde-commit-digest-28th-may-2006
comments:
  - subject: "Why ?"
    date: 2006-05-28
    body: "> KDELibs is now fully ported to D-BUS.\n\nSo - yet another GNOME dependency for KDE. Sorry to say this but this really sucks."
    author: "AC"
  - subject: "Re: Why ?"
    date: 2006-05-28
    body: "> So - yet another GNOME dependency for KDE. Sorry to say this but this really sucks.\n\nWRONG WRONG WRONG.\n\nD-Bus is a very welcomed step. It is not related to Gnome, and will help\nmaking the Linux Desktop a better experience!\n"
    author: "AC2"
  - subject: "Re: Why ?"
    date: 2006-05-28
    body: "D-BUS is desktop-agnostic."
    author: "Eike Hein"
  - subject: "Re: Why ?"
    date: 2006-05-28
    body: "...and the trolls get the first post. Come back when you have an argument. I for one am looking forward to more collaboration and unification where it makes sense!"
    author: "Tom"
  - subject: "Re: Why ?"
    date: 2006-05-28
    body: "There are many reasons for switching to DBUS, though of cause there are also reasons not to switch. But \"it sucks because it is gnome\" is a very bad reason.\n\nReasons for the switch: \n- All apps using dbus will be able to communicate with KDE4-apps nativly (they use the same IPC)\n- The dbus-stuff is maintained outside KDE (on freedesktop), this means a workload is taken from KDE, the KDE-guys can concentrate on KDE\n- KDE-apps will integrate easier into a non-KDE environment. For example, many Gnome-users use amarok and k3b.\n- DBUS is very much like DCOP in some ways because the guys who created DCOP had a very close look how DCOP works. This means it is not like everything is changing.\n- Every line of code shared between two project means: More testing, better documentation, more features and so on (because more people are use the API, more applications use it and so on)\n\nThe current situation with KDE and Gnome (and xfce, fluxbox...) is that some things are already shared but still there are many things for which similar solutions exist in both \"worlds\". It is good for OSS in general to cooperate instead of fighting each other just because \"the other one sucks\"."
    author: "Carsten Niehaus"
  - subject: "Re: Why ?"
    date: 2006-05-28
    body: "> the guys who created DCOP had a very close look how DCOP works\n\nthat was of course supposed to be\n\n\"the guys who created DBUS had a very close look how DCOP works\""
    author: "Carsten Niehaus"
  - subject: "Re: Why ?"
    date: 2006-05-29
    body: "> \"the guys who created DBUS had a very close look how DCOP works\"\n\nBut they still managed to write butt-ugly code. It's C, depends on glib and in general it looks messy and documentation is scarce (if not less).\n\nAnd I don't buy the workload argument. How much workload was DCOP really for KDE?\n"
    author: "ac"
  - subject: "Re: Why ?"
    date: 2006-05-29
    body: "This is why there is QDBUS to wrap the C DBUS. To hide all of the \"messy\" code. I expect a rewrite will eventually be done to remove the dependency on glib since dbus is a protocol, not just a library.\n\nI do have a small problem with D-Bus though. Why is there 3 address types needed to find a client in dbus? Shouldn't one to the trick?\nOtherwise, it looks like a very solid interface.\n\nCheers,\nBen"
    author: "Ben"
  - subject: "Re: Why ?"
    date: 2006-05-30
    body: "There is more than one type of bus, usually two: the session bus and the system bus.\n\nThe session bus can be compared with what DCOP does, i.e. allowing communication of applications within the same user session.\n\nThe system bus is, again compared with DCOP, a new possibility, a way to communicate with applications not running within the user's session, usually not even as the same user.\n\nI don't think the third type, activation bus, is used directly though.\n"
    author: "Kevin Krammer"
  - subject: "Re: Why ?"
    date: 2006-05-31
    body: "That shows you've never tried.\n\nFirst of all, beauty (or ugliness) is on the eye of the beholder. Obviously who wrote the code finds it beautiful. And as long as there are skillful people maintaining it, why would you care?\n\nSecond, it does not depend on glib.\n\nThird, it does not look messy.\n\nFourth, documentation is not scarce, though it may be outdated in some areas. There's a whole description of the protocol and specifications, something that DCOP never had. We just relied on everyone using Qt's QDataStream, so we never bothered with writing a description of the wire format."
    author: "Thiago Macieira"
  - subject: "Re: Why ?"
    date: 2006-05-31
    body: "To me, refering to other documentation is also a form of documentation. The way Qt datatypes were written to a QDataStream is nicely documented (as we have grown accustomed with Qt), so it's not like the wire format was undocumented. \nWhy anyone would care about \"beautifull\" code, is that there is no guarantee that the one who wrote is and/or is maintaining it now will still be doing so in the future. If KDE depends on it, it must be maintainable. To me, beautifull code is maintainable code. \n\nFor the record: I don't oppose the move to DBUS at all. I am looking forward to a tighter integration of my system with my desktop environment, and I understand that DBUS will make this goal easier to reach. "
    author: "Andr\u00e9 Somers"
  - subject: "Re: Why ?"
    date: 2006-05-31
    body: "Also, DBus does *not* depend on glib. Only the dbus glib bindings depend on glib.\nAt the moment there is an issue that one of the headers installed as part of libdus should actually only be in the dbus glib bindings. This is on our (the DBus maintainers) plan to be fixed ASAP, and definitely before a dbus 1.0 release. All the language bindings will be split out from the main dbus tree - if anyone wants to help out doing this, we could do with the help ;)"
    author: "Rob"
  - subject: "Re: Why ?"
    date: 2006-06-01
    body: "\"KDE-apps will integrate easier into a non-KDE environment. For example, many Gnome-users use amarok and k3b.\"\n\nI wouldn't hold out too much hope of that. One end still needs to understand what the other is sending, and DBUS specifies nothing in that way. Integration is far more than sharing an IPC mechanism."
    author: "Segedunum"
  - subject: "Re: Why ?"
    date: 2006-06-01
    body: "True. But sharing the same IPC mechanism is at least a good start."
    author: "Paul Eggleton"
  - subject: "Re: Why ?"
    date: 2006-05-28
    body: ">>So - yet another GNOME dependency for KDE\n\nWhat other gnome dependencies does kde have?\nAnd if kde is already depending on gnome, why would another dependency hurt?\n\nAs already stated, D-BUS is not a gnome technology, it comes from freedesktop.org and is derived from DCOP"
    author: "AC"
  - subject: "Re: Why ?"
    date: 2006-05-28
    body: "I guess he means HAL or perhaps gstreamer as one of the \"favorite\" backends in phonon and amarok?\n\nNo clue otherwise."
    author: "Carsten Niehaus"
  - subject: "Re: Why ?"
    date: 2006-05-29
    body: "libxml2 for documentation and glib in arts"
    author: "Carewolf"
  - subject: "Re: Why ?"
    date: 2006-05-29
    body: "> libxml2 for documentation\nlibxml2 depends on libc and zlib. So what's the real problem? The name \"GNOME XML library\" or any real objections?\n\n\nlg\nErik"
    author: "Erik"
  - subject: "Re: Why ?"
    date: 2006-05-29
    body: "> What other gnome dependencies does kde have?\n\nThere are dependencies on LibXML2, LibXSLT, LibArt_LGPL, GLib and (indirect) on GTK+.  It also appears that AudioFile is now part of GNOME.\n\nThis is a problem since GLib 2.10.x appears to break KDE stuff.\n\nOTOH, libraries which have moved from GNOME to FreeDesktop.org are not really GNOME dependencies.\n\n-- \nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Why ?"
    date: 2006-05-31
    body: "libxml2, libxslt don't bring GNOME dependencies. Libart_lgpl is not being used anymore. Glib was used only by aRts and that is gone.\n\nWhy does anyone care if any of the libraries that we use are used by GNOME as well?\n\nNext people are going to complain that we shouldn't use libc because GNOME uses it too."
    author: "Thiago Macieira"
  - subject: "Re: Why ?"
    date: 2006-05-29
    body: "> So - yet another GNOME dependency for KDE. Sorry to say this but this really sucks.\n\nHaters really suck."
    author: "ac"
  - subject: "Re: Why ?"
    date: 2006-05-29
    body: "Everybody sucks ;-)"
    author: "ac"
  - subject: "Re: Why ?"
    date: 2006-05-29
    body: "No, I don't! ;)"
    author: "Max"
  - subject: "Re: Why ?"
    date: 2006-05-29
    body: "apt-cache show libdbus-1-2\nPackage: libdbus-1-2\nPriority: optional\nSection: devel\nInstalled-Size: 360\nMaintainer: Utopia Maintenance Team <pkg-utopia-maintainers@lists.alioth.debian.org>\nArchitecture: i386\nSource: dbus\nVersion: 0.61-5\nDepends: libc6 (>= 2.3.5-1)\nConflicts: dbus (<< 0.60)\nFilename: pool/main/d/dbus/libdbus-1-2_0.61-5_i386.deb\nSize: 233722\nMD5sum: 3d9eed32ba7d7072f47b85d66bc36019\nDescription: simple interprocess messaging system\n D-BUS is a message bus, used for sending messages between\n applications.  Conceptually, it fits somewhere in between raw sockets\n and CORBA in terms of complexity.\n .\n D-BUS supports broadcast messages, asynchronous messages (thus\n decreasing latency), authentication, and more.  It is designed to be\n low-overhead; messages are sent using a binary protocol, not using\n XML.  D-BUS also supports a method call mapping for its messages, but\n it is not required; this makes using the system quite simple.\n .\n D-BUS is still under heavy development, but is expected to be widely\n used.  It comes with several interfaces, including GLib.  See the\n description of libdbus-glib-1-2 for more information about those.\n .\n The daemon can be found in the dbus package.\n\napt-cache show dbus\nPackage: dbus\nPriority: optional\nSection: devel\nInstalled-Size: 532\nMaintainer: Utopia Maintenance Team <pkg-utopia-maintainers@lists.alioth.debian.org>\nArchitecture: i386\nVersion: 0.61-5\nReplaces: libdbus0, dbus-1\nDepends: libc6 (>= 2.3.5-1), libdbus-1-2 (>= 0.61), libexpat1 (>= 1.95.8), libice6, libsm6, libx11-6, adduser, debianutils (>= 1.22.0), lsb-base (>= 3.0)\nConflicts: libdbus0, dbus-1, dbus-1-utils (<< 0.50-2), libdbus-1-1\nFilename: pool/main/d/dbus/dbus_0.61-5_i386.deb\nSize: 301352\nMD5sum: 6243a07cdaab71c2ed77c1b5e500173d\nDescription: simple interprocess messaging system\n D-BUS is a message bus, used for sending messages between\n applications.  Conceptually, it fits somewhere in between raw sockets\n and CORBA in terms of complexity.\n .\n D-BUS supports broadcast messages, asynchronous messages (thus\n decreasing latency), authentication, and more.  It is designed to be\n low-overhead; messages are sent using a binary protocol, not using\n XML.  D-BUS also supports a method call mapping for its messages, but\n it is not required; this makes using the system quite simple.\n .\n D-BUS is still under heavy development, but is expected to be widely\n used.  It comes with several interfaces, including GLib.  See the\n description of dbus-glib-1 for more information about those.\n .\n This package contains the D-BUS daemon; the client-side library can\n be found in the libdbus-1-2 package, as it is no longer contained in\n this package.\n\ndbus does not depend on gblib. There are wrappers for glib and qt4, however."
    author: "Michael Thaler"
  - subject: "Re: Why ?"
    date: 2006-05-30
    body: "Yes, it does, but they statically link to glib to please KDE."
    author: "Carewolf"
  - subject: "Digest layout"
    date: 2006-05-28
    body: "Sorry to be a pain, but what happened to the plans of moving to use the KDE.org stylesheets for the digest? The typography is very unfriendly to the eyes."
    author: "Eike Hein"
  - subject: "Re: Digest layout"
    date: 2006-05-30
    body: "Personal preference and all but I like the current layout very much. The font size is just large enough to be easily readable by a lazy mind, yet not overly large to waste space. The colour is slightly lighter than black but not too light - it retains contrast but still makes things feel smoother. The page generally has a nice, consistent feel.\nMy only cripe would be that the links are a little tricky to distinguish (as they're black and the text is some form of dark grey).\n"
    author: "Cerulean"
  - subject: "Re: Digest layout"
    date: 2006-05-30
    body: "I think when you looking at this under windows with firefox, you can press Ctrl+[-] to make it one position smaller, afterwards it looks very good... Don't know about it when under linux...."
    author: "boemer"
  - subject: "why KViewShell is still developed in SVN???"
    date: 2006-05-28
    body: "I thought everything has moved to Okular, why KViewShell is still being developed?"
    author: "Sergey"
  - subject: "Re: why KViewShell is still developed in SVN???"
    date: 2006-05-29
    body: "Because it fully supports DVI while oKular not and probably never will be?\nKDE/Linux is still geeky thing and formats like DVI are important."
    author: "m."
  - subject: "Re: why KViewShell is still developed in SVN???"
    date: 2006-05-29
    body: "Hmm, my impression was that all of this stuff will be in Okular, including kCHM and kFAX? Otherwise what's the point to create \"universal viewer app\" that really not that universal? "
    author: "Sergey"
  - subject: "Re: why KViewShell is still developed in SVN???"
    date: 2006-05-29
    body: "> Because it fully supports DVI while oKular not and probably never will be?\n\nThanks for that great showing of support ;-)"
    author: "Albert Astals Cid"
  - subject: "Re: why KViewShell is still developed in SVN???"
    date: 2006-05-29
    body: "> Thanks for that great showing of support ;-)\n\nJust think how encouraging this topic is for us ;-)\n\nGreetings,\nWilfried Huss (Maintainer of KViewShell)"
    author: "Wilfried Huss"
  - subject: "Re: why KViewShell is still developed in SVN???"
    date: 2006-05-29
    body: "well, i wouldn't want to downplay your work, on the contrary, but wouldn't it be nice if DVI support would get into oKular, instead of in kview? adding everything oKular does to Kview would cost more time than adding kview's capabilities to oKular, i guess. as KDE 4 is most likely gonna get a cleanup so it'll only ship one viewer (most likey oKular) - only those that install kview by hand will be able to use its features. and i guess you want everybody to be able to use what you write (and, after all, it IS cool to have DVI support), right? :D\n\nbut sure, you've heard ppl asking to help a bit on oKular before, i guess, and you'll have you'r reasons for not doing it (or maybe you even DO)..."
    author: "superstoned"
  - subject: "Re: why KViewShell is still developed in SVN???"
    date: 2006-05-29
    body: "> but wouldn't it be nice if DVI support would get into oKular.\n\nThis is not as easy as it sounds, because oKular uses normalized\ncoordinates to store things like the position letters or of hyperlinks.\nThis does not work for DVI-files, because DVI-files don't need to specify\na papersize. How do you know long 0.5 times the full page width is, when you\ndon't know the size of the page?\n\n> adding everything oKular does to Kview would cost more time than adding\n> kview's capabilities to oKular, i guess.\n\nYou guess incorrectly. It would need a rather big rewrite of oKular.\nAnd KViewShell already supports: DVI, DjVu, Fax/g3, PDF and now PostScript.\n "
    author: "Wilfried Huss"
  - subject: "Re: why KViewShell is still developed in SVN???"
    date: 2006-05-30
    body: "so it looks more like the oKular ppl should join kviewshell, right? hmmm, come up with a better name and they might ;-)"
    author: "superstoned"
  - subject: "Re: why KViewShell is still developed in SVN???"
    date: 2006-05-29
    body: "> KDE/Linux is still geeky thing and formats like DVI are important.\n\nDVI files are the format of choice when working on scientific publications\nwith TeX/LaTeX. And it will still take some time until PDFs will be a better\nchoice for this work.\n\nAnd the scientific desktop is a very important niche for desktop linux, and\ngood DVI support is important for that.\n\nKViewShell is today probably the most featureful DVI-Viewer in existence\non any platform."
    author: "Wilfried Huss"
  - subject: "Re: why KViewShell is still developed in SVN???"
    date: 2006-05-29
    body: "> Because it fully supports DVI while oKular not and probably never will be?\nThen fix oKular instead!"
    author: "mETz"
  - subject: "Re: why KViewShell is still developed in SVN???"
    date: 2006-05-29
    body: "Well, go ahead :o)"
    author: "AC"
  - subject: "Re: why KViewShell is still developed in SVN???"
    date: 2006-05-29
    body: "Because we never came to agree if it was better to develop on kpdf's source code or in kdvi's (kviewshell) source code."
    author: "Albert Astals Cid"
  - subject: "Re: why KViewShell is still developed in SVN???"
    date: 2006-05-29
    body: "> I thought everything has moved to Okular, why KViewShell is still being developed?\n\nThe KViewShell developers never moved to Okular. When that project started,\nKViewShell already was a fully developed universal viewer. Also the SOC-Student\nthat started Okular, clearly never was interested in a merge of our projects.\n\nAnd I get really tired of hearing that we should throw away our program, everytime\nit is mentioned somewere on a website."
    author: "Wilfried Huss"
  - subject: "Re: why KViewShell is still developed in SVN???"
    date: 2006-05-29
    body: "> The KViewShell developers never moved to Okular. When that project started,\n> KViewShell already was a fully developed universal viewer. Also the\n> SOC-Student that started Okular, clearly never was interested in a merge\n> of our projects.\n\nMaybe he wasn't, but I think Albert should be? Perhaps you can find a common direction with him, no matter if oKular or KViewShell would be the outcome. (You could also trick the world and get all the oKular features into KViewShell, and afterwards nuke the original one and rename KViewShell to oKular. ...erm... just speculating.)"
    author: "Jakob Petsovits"
  - subject: "Re: why KViewShell is still developed in SVN???"
    date: 2006-05-29
    body: "> Maybe he wasn't, but I think Albert should be?\n\nWe approached Albert about collaboration way before the oKular project\nstarted, obviously nothing came out of it.\n\n> Perhaps you can find a common direction with him, no matter if oKular or\n> KViewShell would be the outcome. \n> (You could also trick the world and get all the oKular features into\n> KViewShell, and afterwards nuke the original one and rename KViewShell to\n> oKular. ...erm... just speculating.)\n\nWe already merged features from KPDF, where it made sense (for example the\npresentation mode, and the accessibility viewmodes). KViewShell already has\nmost of the features that oKular has, and quite a few features that oKular\ndoesn't have.\n\nMerging the two programs would have made sense one and a half year ago, but\ntoday KViewShell would gain nothing out of it.\n"
    author: "Wilfried Huss"
  - subject: "Re: why KViewShell is still developed in SVN???"
    date: 2006-05-29
    body: "> Merging the two programs would have made sense\n> one and a half year ago, but today KViewShell \n> would gain nothing out of it.\n\nOh yes, it would: It would gain a _NAME_ which doesn't \nsound like rocket science. Sorry, but while KViewShell \nmight be descriptive to a developer it sounds simply \nawful to a common user and has exactly no potential \nwhen it comes to marketing. (Not that I really like oKular,\nbut while it's still quite technical it gives at least \nsomething for the user to imagine).\n\nTorsten "
    author: "Torsten Rahn"
  - subject: "Re: why KViewShell is still developed in SVN???"
    date: 2006-05-29
    body: "I know the name is not good. It was chosen by the original\nauthor, and probably never meant to be visible outside the\nAPI. Even on KDE 3.5 we still install kdvi as a binary for\ncompability, although it is really the same program as\nkviewshell, and it supports more fileformats than only DVI.\n\nWe do plan to rename KViewShell for KDE4. So, if there are\nsuggestions for a better name, I would really be glad to\nhere them."
    author: "Wilfried Huss"
  - subject: "Re: why KViewShell is still developed in SVN???"
    date: 2006-05-29
    body: ">>probably never meant to be visible outside the API.\n\nindeed, people don't mind application names, they just click on a file and use whatever programm is associated with it. \nIf that is kviewshell, and it's sufficient for the user, he/she will continue using it.\n\nRenaming kviewshell may be a good idea, especcially if it comes with a good promotion strategie.\nApparently kviewshell can do whatever okular wants to do (or even more), but nobody knows about it, while okular gets all the momentum and kviewshell not..\n"
    author: "AC"
  - subject: "Re: why KViewShell is still developed in SVN???"
    date: 2006-05-30
    body: "<I>We do plan to rename KViewShell for KDE4. So, if there are suggestions for a better name, I would really be glad to here them.</I>\n\nHow do you like oKular?  ;-)"
    author: "Cobarde an\u00f3nimo"
  - subject: "Re: why KViewShell is still developed in SVN???"
    date: 2006-05-31
    body: "Please don't use that insane capital K in the middle of the word. Looks just horrible and normal users have no clue what does it mean.\n"
    author: "Michal"
  - subject: "Re: why KViewShell is still developed in SVN???"
    date: 2006-05-31
    body: "Maybe krystal?\nThe idea behind the name is of course that one can see (view) all in a crystal ball, including the future (new formats) and the past (\"old\" formats \"nobody\" uses anymore). Also, crystal clear displaying are of course the target for any viewer. Last, a natural crystal has may facets, wich could be seen as a reference to the many formats that come together in a single viewer.\nThe K is just thrown in for KDE-ishness, which I happen to like :-)\n\n"
    author: "Andr\u00e9 Somers"
  - subject: "Re: why KViewShell is still developed in SVN???"
    date: 2006-05-31
    body: "By the way: it fits nicely with the current \"phyisics\" naming theme in KDE too: Solid, Phonon, Plasma..."
    author: "Andr\u00e9 Somers"
  - subject: "Re: why KViewShell is still developed in SVN???"
    date: 2006-05-29
    body: "Why are we reinventing the wheel yet again?\n\nKViewShell is a mature application -- something that takes more than just a Summer to accomplish.\n\nI can see possible advantages to adding PS and PDF viewing to KViewShell, but why start over?"
    author: "James Richard Tyrer"
  - subject: "so, does it mean"
    date: 2006-05-29
    body: "we will have TWO \"not so universal\" viewers in KDE4, or 4 if you count kfax and kchm? I'm not being negative, just trying to understand when \"application cleanup\" idea went down the toilet."
    author: "Sergey"
  - subject: "Re: so, does it mean"
    date: 2006-05-29
    body: "No, it just means that nothing is decided yet...."
    author: "cl"
  - subject: "Phonon"
    date: 2006-05-29
    body: "Do the Phonon refactorings mean that it will be able to manipulate multiple backends at once? "
    author: "mime"
  - subject: "Re: Phonon"
    date: 2006-05-29
    body: "Why would you want that?"
    author: "AC"
  - subject: "Re: Phonon"
    date: 2006-05-29
    body: "To allow crossfading or other kinds of simultaneous playback of different media (which can be handled by different backends)"
    author: "mime"
  - subject: "Re: Phonon"
    date: 2006-05-29
    body: "I still don't see the point in using multiple backends?\nand simultaneous playback is already possible if you are using a decent sound card, or with alsa's dmix.\n"
    author: "AC"
  - subject: "Re: Phonon"
    date: 2006-05-30
    body: "I mean this: http://bugs.kde.org/show_bug.cgi?id=127308"
    author: "mime"
  - subject: "Re: Phonon"
    date: 2006-05-30
    body: "Ah that one :)\n\nBut there are some problems with that: \n\nThe outpunt from the backends won't be mixed, so if phonon directs different streams to different backends simultaniously, they will collide with each other (unless you have a sound card that does the mixing for you), and backends will complain that the audio device is not ready.\n\nAnd something else: lets say you have a playlist in amaroK containing mp3, ogg and wmv files, and for each file format you have configured phonon to use a different backend.\nPlaying that list would mean that phonon should switch backends every time another audio format is loaded in amaroK.\nThat would create a lot of overhead on the system, causing large gaps between the songs..\n"
    author: "AC"
  - subject: "Re: Phonon"
    date: 2006-05-30
    body: "> they will collide with each other (unless you have a sound card that does the mixing for you)\n\nEven if they all output to ALSA? Doesn't dmix solve that for everybody? (except the OSS apps)\n\n> That would create a lot of overhead on the system, causing large gaps between the songs..\n\nThis is why I was asking. If Phonon is now meant to allow apps to queue tracks for playback, then a specific engine can be preloaded when it's track is nearing, so that it's already available when needed, and no gap is caused. (I assume)"
    author: "mime"
  - subject: "Re: Phonon"
    date: 2006-05-30
    body: "Right now I use aRts for general stuff in /dev/dsp, and I configured Xine to ouput /dev/dsp1 to get sound in my usb headphones (which I use to play music and movies without anoying my flatmates). "
    author: "me"
  - subject: "KDe not a D-BUS user"
    date: 2006-05-29
    body: "Strange, but KDE is not on the D-BUS users list."
    author: "KDe User"
  - subject: "Re: KDe not a D-BUS user"
    date: 2006-05-29
    body: "So?  That page is a Wiki, you can fix it."
    author: "James Richard Tyrer"
  - subject: "Re: KDe not a D-BUS user"
    date: 2006-05-31
    body: "Technically, we weren't a D-BUS user until a few hours ago."
    author: "Thiago Macieira"
  - subject: "Re: KDe not a D-BUS user"
    date: 2006-05-31
    body: "Are you sure that's technically correct? I thought KDE was a (optional) D-BUS user since KDE 3.4 (or was it 3.3?). Using D-BUS for it's HAL support, in handling mounting of media and such. More or less equaling Gnomes usage of D-BUS. \n\n"
    author: "Morty"
---
In <a href="http://commit-digest.org/issues/2006-05-28/">this week's KDE Commit-Digest</a>: KViewShell gets support for PostScript files. Work begins on <a href="http://pim.kde.org/akonadi/">Akonadi</a> (the new KDE PIM data storage backend) and <a href="http://amarok.kde.org/">amaroK</a> 2.0, with further optimisations to the stable amaroK version. <a href="http://accessibility.kde.org/developer/kttsd/">kttsd</a> (the kde-accessibility text-to-speech system) is ported to <a href="http://phonon.kde.org/">Phonon</a>. KDELibs is now fully ported to <a href="http://www.freedesktop.org/wiki/Software_2fdbus">D-BUS</a>. Aesthetic improvements to <a href="http://docs.kde.org/development/en/kdebase/ksysguard/">KSysGuard</a>.



<!--break-->
