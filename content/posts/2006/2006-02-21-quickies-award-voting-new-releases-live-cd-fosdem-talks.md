---
title: "Quickies: Award Voting, New Releases, Live CD, FOSDEM Talks"
date:    2006-02-21
authors:
  - "jriddell"
slug:    quickies-award-voting-new-releases-live-cd-fosdem-talks
comments:
  - subject: "Best file manager"
    date: 2006-02-21
    body: "I wonder why the poll for \"Best File manager\" has both \"Konqueror\" and \"KFM\" entries...maybe for people using KDE-1.x?\n"
    author: "LMCBoy"
  - subject: "Re: Best file manager"
    date: 2006-02-25
    body: "Because Konqueror is a browser and KFM is a file manager which runs in Konqueror."
    author: "James Richard Tyrer"
  - subject: "Poor choices of linuxquestions.org"
    date: 2006-02-22
    body: "I had left some comments in the site about poor choices. Konqueror and KFM, for instance. K3b and amarok. Xine in the multimedia but not kaffeine, etc. Unfortunately, this award is, despite being well conducted, is not very fair with the participants."
    author: "Henrique Marks"
  - subject: "What is a \"File Manager\", a \"Browser\""
    date: 2006-02-22
    body: "I hope that it wasn't just an accident that LinuxQuestions said: \"Browser\" and not \"Web Browser\" since (with the possible exception of deliberately crippled software from MS) there is no such thing as a Web Browser.  All Browsers will browse your local files as well a network or the Web.\n\nSo, my favorite Browser is Konqueror -- I have Firefox installed but only use it when KHTML won't display a web page.\n\nThen we have the question of what is a \"File Manager\"?  Some people have answered \"Konqueror\" but Konqueror is NOT a \"File Manager\" it is a shell that runs the various plugins which are necessary to browse.\n\nFrom a programing view point, KDE has a file manager (K File Manager [KFM]) which can only run in Konqueror.  This tends to confuse the issue since it (KFM) is also used for browsing functions since Konqueror does not have a way to display lists of files for protocols other than HTTP.\n\nSo, for File Manager, should we vote for KFM since Konqueror isn't really a File Manager?"
    author: "James Richard Tyrer"
  - subject: "Re: What is a \"File Manager\", a \"Browser\""
    date: 2006-02-23
    body: "Yeah, and FireFox isn't really browser, Gecko is."
    author: "blacksheep"
  - subject: "Re: What is a \"File Manager\", a \"Browser\""
    date: 2006-02-25
    body: "Perhaps you didn't read what I wrote.\n\nFirefox IS a browser.\n\nGecko is an HTML rendering engine.\n\n"
    author: "James Richard Tyrer"
  - subject: "Re: What is a \"File Manager\", a \"Browser\""
    date: 2006-02-25
    body: "> So, for File Manager, should we vote for KFM since Konqueror isn't really a File Manager?\n\nNo we, should vote for Konqueror, since that's how the file manager identifies itself to the user. Otherwise we'll just split up votes. Only vote 'KFM' if you're still using KDE 1."
    author: "a"
  - subject: "Re: What is a \"File Manager\", a \"Browser\""
    date: 2006-03-01
    body: "IIRC, the term KFM was still in use after KDE-1, but that isn't relevant because there is a file manager (in function if not in name).   The name appears to have been changed to \"kfmclient\"  But, that command can also start the Konqueror as web browser (an artificial distinction). \n\nThe file manager seems to have been divided up into several parts.  There are the two front ends: \"iconview\" & \"listview\" and various \"kio\" stuff including \"job\" which does much of the actual management of the files."
    author: "James Richard Tyrer"
  - subject: "Re: What is a \"File Manager\", a \"Browser\""
    date: 2006-02-26
    body: "\"This tends to confuse the issue since it (KFM) is also used for browsing functions since Konqueror does not have a way to display lists of files for protocols other than HTTP.\"\n\nI'm sorry but this sentence only shows that you don't really know what you are talking about.  Konqueror loads kparts depending on the mimetype of the url that is being loaded.  The url can be *ANY* url/protocol the KURL will parse and that there is a kioslave to access.  If konqueror has a kpart for that mimetype(and depending on mimetype settings), konqueror will load the kpart to show the url, otherwise it will load the url externally, or ask the user.\n\nKparts happen to have extensions, such as the browser extension so that konqueror can control them in specific ways that aren't available in the kpart api.\n\nKfm, if it still exists since 1.x, is simply a name for konqueror with one of the directory browsing kparts loaded, but i don't think very many people refer to it as such.\n\nSo, to recap, the poll is either mistaken(kfm==konqueror) or unclear(kfm 1.x vs konqueror).\n\nI think we need a quality control team to control the quality of your comments :)\n\nMatt"
    author: "Matt"
  - subject: "Re: What is a \"File Manager\", a \"Browser\""
    date: 2006-03-01
    body: "> I think we need a quality control team to control the quality of your  \n> comments :-)\n\nYes, my proof reader really screwed up on that sentence.  But, it isn't quite as fractured if you take it in the context of contrasting Konqueror with other browsers.\n\nYes, I know that Konqueror loads a K-Part for everything it does but that doesn't mean that we can't speak of them together.\n\nWhen you run the FTP protocol in Firefox, you get a file list.  Konqueror doesn't have a K-Part specifically to do this, it just uses the KFM part to do it.  OTOH, when you are using KHTML, that K-Part does display a file list just like Firefox does.  So, Konqueror is using the file manager K-Part when it is doing what most people regard as web browsing.  And, that further confuses the issue."
    author: "James Richard Tyrer"
  - subject: "Re: What is a \"File Manager\", a \"Browser\""
    date: 2006-03-01
    body: "I am an expert of neither HTTP nor FTP, but, as far as I know, HTTP does not support any file listing like FTP does. So what you get by HTTP is an HTML page instead. Therefore it is displayed in KHTML. \n\nAs for FTP (without proxy), you indeed get back a text listing. (The ftp KIO supports getting listings similar to the output of ls(1) and has known problems with other listing formats.)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: What is a \"File Manager\", a \"Browser\""
    date: 2006-03-02
    body: "To be exact: the FTP KIO slave expect to get an output  like the one of ls -l"
    author: "Nicolas Goutte"
  - subject: "Re: What is a \"File Manager\", a \"Browser\""
    date: 2006-02-27
    body: "I'm afraid I don't understand your point.\n\nFinder and Nautilus are file managers that can also be refered to as file browsers, but they're not generic browsers.\n\nSafari and Epiphany are web browsers, not generic browsers.\n"
    author: "Reply"
  - subject: "Re: What is a \"File Manager\", a \"Browser\""
    date: 2006-03-01
    body: "And Konqueror is a generic browser which works equally well browsing anywhere.\n\nGreat design, but then to try to conform, they screwed it up.\n"
    author: "James Richard Tyrer"
  - subject: "Re: What is a \"File Manager\", a \"Browser\""
    date: 2006-02-27
    body: "\"From a programing view point, KDE has a file manager (K File Manager [KFM]) which can only run in Konqueror. This tends to confuse the issue since it (KFM) is also used for browsing functions since Konqueror does not have a way to display lists of files for protocols other than HTTP.\"\n\nYou have just shown that you don't have the slightest clue what you are talking about. Matt, just above, is correct."
    author: "ac"
  - subject: "Article Format"
    date: 2006-02-23
    body: "Is it really hard to format an article for KDE.news?\n\nHey, look!\n\nIt isn't!"
    author: "ac"
---
LinuxQuestions.org launched their <a href="http://www.linuxquestions.org/questions/forumdisplay.php?f=69">Members Choice Awards Polls</a>.  Plenty of KDE applications and KDE itself up for your votes. *** <a href="http://krusader.sourceforge.net/phpBB/viewtopic.php?t=1465">Krusader released 1.70.0</a> of their file manager.  "<em>Bringing a feature rich filemanager with new technologies and standards to the Linux desktop.</em>" *** Premiere IRC client <a href="http://konversation.kde.org/">Konversation released 0.19</a> with an impressively long changelog. *** <a href="http://kubuntu.org/special-cds.php">Kubuntu released a live CD</a> with the latest KDE, KOffice and Amarok.  *** The <a href="http://www.fosdem.org/2006/index/dev_room_kde/schedule">schedule</a> for the <a href="http://www.fosdem.org/2006/index/dev_room_kde">KDE Developers room at FOSDEM</a> is up with talks on Kubuntu, Free Software marketing, KOffice and Krita.  See you at the weekend.







<!--break-->
