---
title: "The Scribus Team Announces 1.3.2 - Egalit\u00e9"
date:    2006-01-23
authors:
  - "mrdocs"
slug:    scribus-team-announces-132-egalité
comments:
  - subject: "\u00e9galit\u00e9"
    date: 2006-01-23
    body: "1.3.1 was called \"Libert\u00e9\", 1.3.2 is \"\u00c9galit\u00e9\", I wonder if the 1.3.3 version will be called \"Fraternit\u00e9\" or \"Choucroute\"..."
    author: "oliv"
  - subject: "Re: \u00e9galit\u00e9"
    date: 2006-01-30
    body: "I vote for choucroute !"
    author: "slainer68 aka Nicolas Blanco"
  - subject: "klik://scribus-latest"
    date: 2006-01-23
    body: "Do I need to say that you can have Scribus (for Linux) from CVS all the time via klik? It is rebuild several times a week from the development tree. Try \"klik://scribus-latest\" if you have the klik client installed.\n\nTonight I'll create a klik recipe for the Scribus-1.3.2 release specifically. Tomorrow morning, by the latest, \"klik://scribus-1.3.2\" should work for this.\n\nCheers,\nKurt"
    author: "Kurt Pfeifle"
  - subject: "Re: klik://scribus-latest"
    date: 2006-01-24
    body: "Great work Scribus team.\n\nAnd great work Klik Team, thanks more making things accessible for normal users, early on.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Request to the Scribus team"
    date: 2006-01-23
    body: "I trust and respect the Scribus team for the good job done. My request to them is to do something about the KDE print dialog. I find there are just too many options. Imagine: We have \"Add\", \"Printer\" \"Print Manager\" \"Print Server\" \"View\" and \"Documentation\". I believe the Scribus Team can clean up that manager or provide clean sensible defaults.\n\n\nAsk yourself: For the \"Add\" button, what are we adding? Why do I have to click to know what is to be added?"
    author: "charles"
  - subject: "Re: Request to the Scribus team"
    date: 2006-01-23
    body: "I am a lover of the KDE print system/dialog, having worked for years at a service bureau under the thumb of Macs and Windows."
    author: "Allen of California"
  - subject: "Re: Request to the Scribus team"
    date: 2006-01-23
    body: "Erm, Scribus only inherits the KDE printer dialog, if you select it in alternative print commands.\n\nThat said, you will pry kprinter out of my cold dead hands.. I use kprinter everywhere - even in Gnome apps :)"
    author: "mrdocs"
  - subject: "One small build problem"
    date: 2006-01-26
    body: "I get this message when I run \"configure\":\n\nchecking If we should build Boost::Python-based Python scripting... no\n\nI have Boost and Boost::Python installed.\n\n"
    author: "James Richard Tyrer"
  - subject: "Re: One small build problem"
    date: 2006-01-29
    body: "We have in 1.3.x some experiemental python::boost stuff, which is not built by default and not even complete.\n\nPlease ignore that message, we are hopefully migrating to something besides the autotools soon."
    author: "mrdocs"
  - subject: "Re: One small build problem"
    date: 2006-01-30
    body: "you need to enable some super-super extra supersecret option for configure. I don't recomend it to anybody as it's really hot (=untested and unsupported at all) stuff. Anyway I let to get this option on you, you magnificent spy... ;)"
    author: "petr vanek"
  - subject: "Does Scribus support KDE..."
    date: 2006-01-31
    body: "Does Scribus now support KDE icons and open/save dialogs? Scribus is _great_ OSS software (best in kind) but i dont like if it is Qt software and it dont support KDE dialogs :-/"
    author: "Fri13"
  - subject: "Re: Does Scribus support KDE..."
    date: 2006-02-03
    body: "If you run Scribus on Suse 10.0, it will have KDE dialogs owning to a special patch added to Suse's build of Qt. \n\nWe have looked at optional KDE integration, but it is difficult for us to do this with Scribus as is. Perhaps with Qt4.\n\n"
    author: "mrdocs"
  - subject: "try it out"
    date: 2006-01-31
    body: "I've try out the Scribus win32 application:\n\n#1. It look nice.\n#2. It's obviously not a word processor, but a layout engine for newspapers/books/flyers,etc.\n#3. The windows icon set is okay.\n#4. More icons within the menu should be present.\n#5. The keyboard shortcut make sense.\n#6. How do we import pre-typed Word/OOWriter/HTML formatted text?!\n#7. Editing text in place require to go through the right-click menu, maybe on purposes.\n#8. It's obviously a dedicated app. \n#9. It takes a minute to load the app, but there's many plugin in it, such as SVG import/export...\n#10. I would go a bit further with keyboard shortcut... if possible.\n#11. A wizard/tutorial/sample/templates would be a great add-on.\n#12. There doesn't seem to be a way to configure the app menu/toolbar/shortcuts.\n\nI'm sure a publisher knows very well what he got to do,\nso I'm not sure right now how beginner can find it useful =P\ncompared to word for normal task.\n\nHowever, the very picky layout/font/color should be nice to work with\nfor high resolution printing task, which is the desired user base.\n\nAnyway you should try it out, if you are in the field.\n\nBTW, the latest build doesn't work (broken), use the previous one with the following md5sum:\nc1c8736f5f7e29cd485525782f1053e3 *scribus-132-win32-install.exe\n\nYou need ghostscript/ghostview also... =P\n\nGood job guys!"
    author: "fp26"
  - subject: "Re: try it out"
    date: 2006-02-03
    body: ">#9. It takes a minute to load the app, but there's many plugin in it, such as SVG import/export...\n\nI just counted 15 s on my 2.5 years old computer (cold start)... strange that it takes so long on yours.\nThe Linux version starts faster on my 6 years old computer, so there might be possible optimisations yet to do for win32 port."
    author: "oliv"
---
The Scribus Team is pleased to announce the release of <a href="http://www.scribus.org.uk/modules.php?op=modload&amp;name=News&amp;file=article&amp;sid=112">Scribus 1.3.2</a>, <em>Egalité</em>, the third development version working towards a new stable 1.4.  With this release we are excited to announce the first beta of <a href="http://windows.scribus.net/">Scribus on the Windows platform</a>. With the gracious support of Trolltech AS, developer of the Qt C++ application framework, we are able to release Scribus on Windows with Qt 3.  It also includes fixes for over 290 requests and bugs.
<!--break-->
<p>Within this release period:</p>
<ul>
<li>We welcomed another member to the team, Jean Ghali, who performed the
Windows migration and developed the printing system for Windows.</li>
<li>We resolved over 290 requests and bugs.</li>
<li>Made significant progress in cleaning and restructuring the codebase which has lead to speed improvements, clearer code and opened the way forward for future 1.3.x development.</li>
 <li>Preparation for the new modular file load and save system.</li>
 <li>EXIF support.</li>
 <li>Enhancements to TIFF and PSD file format support.</li>
 <li>Image Effects - Tinting, Sharpening and more can be applied directly within Scribus.</li>
 <li>Better cursor placement in text frames.</li>
 <li>Section based page numbering.</li>
 <li>Enhancements to the colour wheel and shortwords plugins.</li>
 <li>Enhancements to guide management.</li>
 <li>PDF export with viewer export options.</li>
 <li>Updates for both Windows and Mac OS X compatibility.</li>
 <li>Significant updates to documentation, including the beginning of 1.3.x documentation.</li>
 <li>Translation updates.</li>
</ul>

<p>Scribus is an open source page layout program with the aim of producing conformant commercial grade output in PDF and Postscript, along with extensive support for graphical PDF form and PDF presentations.</p>

<p>While the goals of the program are ease of use and simple to understand tools, Scribus offers support for professional publishing features such as CMYK colors, easy PDF creation and Encapsulated Postscript import/export. Other professional features include full ICC colour management, spot colours and creation of colour separations.</p>

<p>Scribus is commercially supported and is available for GNU/Linux and other Unixes, along with MacOSX and Windows. Scribus is used around the world for newspaper production, book publishing and a wide variety of page layout tasks. </p>

<p>Get Scribus 1.3.2 from our <a href="http://sourceforge.net/project/showfiles.php?group_id=125235">download page</a>, <a href="http://debian.scribus.net ">Debian/Kubuntu repository</a>, <a href="http://aqua.scribus.net">MacOS X page</a> or <a href="http://windows.scribus.net">Windows installation page</a>.








