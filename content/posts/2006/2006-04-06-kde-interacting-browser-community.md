---
title: "KDE Interacting With the Browser Community"
date:    2006-04-06
authors:
  - "gstaikos"
slug:    kde-interacting-browser-community
comments:
  - subject: "Extensions"
    date: 2006-04-05
    body: "I think Konqueror should get Firefox extension support.  There are many useful Firefox extensions but barely (if none) for Konqueror.  This would make Konqueror gain much more market share.\n\nAn alternative is to rewrite an extension system for Konqueror.  Also Konqueror should have a choice of layout engines that are hot swappable (like IE-tab and Netscape 8)."
    author: "Thomas"
  - subject: "Re: Extensions"
    date: 2006-04-05
    body: "Firefox extensions don't eaven worked on SeaMonkey or on new versions of Firefox without breaks so I think Konqueror support for them is a dream :)"
    author: "Shift"
  - subject: "Re: Extensions"
    date: 2006-04-05
    body: "I would say, that making firefox extensions work in a mozilla-based browser would be an effort, but not a titanic one. Most incompatibilities of extensions between firefox versions are due to DOM differences. But making it work in konqueror which is not mozilla-based really is a dream. Too much in the extensions is based on mozilla specific technologies such as XUL and mozilla specific APIs."
    author: "Dmitriy"
  - subject: "Re: Extensions"
    date: 2006-04-05
    body: "I agree Firefox extension support would be a truely great thing to have. But even more so because that would mean KDE would have its own XUL implementation. Unfortunately that's not an easy task, and unlikely to happen soon. I known an effort was once started to bring XUL to KDE, but I think it has been left untouched for a very long time now. Add to that that XUL is somewhat of a moving target and I don't think you will see Firefox extension support happen, ever.\n\nNow \"hot-swappable layout engines\" should be already possible. I remember having used Gecko inside Konqueror a long time ago, but it wasn't too catchy and I don't think the Gecko KPart is maintained anymore.\n\nSo in practice, we will just keep using either Firefox in its entirety or Konqueror with KHTML (which really is quite good nowadays, surely better than Gecko in some respects) and its (few) extensions. Btw, I'm really looking forward to the \"KDE 4 browser\", as KHTML2 is promising some huge new opportunities and would even provide the framework to make a XUL plugin at least somewhat more practical (not for Firefox extensions though).\n\nCheers,\nArend jr."
    author: "Arend jr."
  - subject: "Re: Extensions"
    date: 2006-04-06
    body: "Konqueror has really made great steps ahead in the last time. I can remember not much more than a year ago I already like the speed of Konq and use it\noften but frequently had to switch to Firefox for pages that just did not work. This has improved MUCH for the better and as a web developer I can\nreally appreciate that Konqueror in the meantime has amazing built-in support\nfor DOM, JavaScript, CSS. Amazing because I used to think that Firefox' lead ahead was unreachable for little Konq.\nNow, I dont see Firefox extension support happen soon because a KDE XUL implementation is really unrealistic in the near future but anyway I dont really agree I *want* this at all.\nOriginally I thought this whole extension thing of Firefox would be a really,really great idea. I was totally sold on it. Lean browser - put all in extensions. In the meantime I've somewhat changed my mind. IMHO the big problem in reality is now that the extensions have led to an increased \"outsourcing\" of browser functions. This means that a large group of individuals care for their own extensions. Their is no common beta phase, no common quality assurance, localization, standard etc. This results in a far from optimal integration of those extensions and not much of the polish\nwe all have come to like in KDE. Some of my extensions in Firefox are only in English, some in German. Some dialog boxes look like a mess and when I update my browser some extensions cease to work and I have to wait until the extension developer updates his version to get my functionality back. Not that I'm totally against extensibility - I'm all for it. But not as a main route to go when developing software. It's still a good idea IMO to have a core set of developers responsible for one application and who take finally decisions. And it's a good idea to properly discuss what should be part of a browser and what not. Another really ugly example of extension horror is Kate which is otherwise (and I want to really stress this here despite all my criticism) a really great editor. There are actually functions to perform find-as-you-type or tabbed-browsing. I've overlooked them for ages cause they are neither easily discoverable nor are you usually looking for the extensions tab to configure them nor can the user understand why some quite common functions like searching or tabs are  in an extension. Integration is just awful - for instance the find-as-you-type-bar is hacked together as a toolbar and because not many seem to find and use this it doesnt really work properly.\n\n\n\n\n"
    author: "Martin"
  - subject: "Re: Extensions"
    date: 2006-04-06
    body: "Konqueror doesn't need compatibility with FF extensions - what Konqueror needs is a method to make writing plugins more enjoyable than it is. As it stands it's far too much of a daunting, time consuming task - you need to use a compiled language, need a bunch of metadata files, a bunch of boilerplate code, etc. It really is a big pain at the moment.\nI sat down one evening with the intention of writing a plugin to let you use greasemonkey userscripts with Konqueror (a fairly trivial task as userscripts are simple things in essence) but had real difficulty finding a template to begin with or a set of instructions/instructions for setting things up. It all felt far too clunky.\n"
    author: "Cerulean"
  - subject: "Re: Extensions"
    date: 2006-04-06
    body: "Yes man, I agree with you. We don't need to support the same firefox plugins in konqueror, but would be so much better to make a plugins extension like Amarok has. Using any script language should be the better solution. In KDE we have dcop, we have many KDE bindings to script languages, we have also superkaramba to make great things with pages. Let's imagine a Greasemonkey alike plugin using superkaramba to put widgets over the pages.... could be interesting....\n\nSometimes I ask my self about why not most of kde apps has IMHO best feature in amarok -> a plugin architecture. I feel good making true good scripts in a easy way using python or bash, I would like to do the same in the rest of KDE applications.... digikam, kget, kopete, kaffeine or codeine.... I always liked to write an script to fetch subtitles for my favorites films or TV Series.... yes, I have DCOP and any script language to do this but I would like to make a plugin that could be integrated in the main app....\n\nThanks for reading and sorry because of my bad english.... :P"
    author: "biquillo"
  - subject: "Re: Extensions"
    date: 2006-04-06
    body: "Yes, we urgently need a simplified way of configuring media players and java applets with Konqueror. Till date, I have to resort to Firefox just to get to my netbanking site or play streaming audio/video. Konqueror either crashes (mplayer, for instance) or just refused to work (Java applets).\n\n"
    author: "Kanwar"
  - subject: "Re: Extensions"
    date: 2006-04-08
    body: "You seem to confuse khtml-plugins, that are in the 'Tools' menu and KParts that are pulled in a html page w/ object/applet/embed tags base on their mimetype. The latter are in principle usable in every kde application, eg. you can have a java applet inside an application that is written in javascript (using KJSEmbed).\nSecondly, mplayer can't crash konqueror because they are both applications. I know that the KMPlayer plugin can use mplayer for video viewing and if you find a crashing case, then please file a bug report or contact the author.\n "
    author: "koos"
  - subject: "Re: Extensions"
    date: 2006-04-06
    body: "I couldn't agree more. I would love to write little Python script to extend some application. Is there any chance that Kross will be used KDE-wide, or is there any other language-independent scripting framework in sight?"
    author: "ac"
  - subject: "Re: Extensions"
    date: 2006-04-06
    body: "wel, yes, afaik this is one of the plans for KDE 4 - system-wide extension support. javascript at least, and python and ruby, optionally."
    author: "superstoned"
  - subject: "Re: Extensions"
    date: 2006-12-28
    body: "I totally support for system-wide extension capability in KDE 4 (and I vote for python scripting)."
    author: "trung"
  - subject: "Re: Extensions"
    date: 2006-04-06
    body: "What extensions are needed for Konqueror?  Aren't many of the Firefox extensions are for features which Konqueror already has?\n\nIAC, I would suggest that Firefox needs to be redesigned so that it conforms to FD.o standards.  The same can be said of Konqueror (KDE).  Currently Firefox uses the MIME standard (although I have problems getting it to work :-() as does GNOME, and KDE doesn't.  However, Firefox does not use the icon theme standard although both KDE (but where are the *real* HiColor icons) and GNOME do -- still lacking is a standard way to select the icon theme.  IIUC, there isn't yet a widget theme standard although we clearly need one.\n\nPerhaps after these more important issues are addressed, a standard for web and browser extensions can be addressed.  Note that we should not put this as 'Konqueror needs to be able to use Firefox extensions', we need to say that we need a standard for extensions that various browsers can use.  Perhaps these extensions should be written in Java so that they would work on all platforms."
    author: "James Richard Tyrer"
  - subject: "Re: Extensions"
    date: 2006-04-10
    body: "You got it - That will be the best thing! The other way around it will be the same nightmare as with esd and arts. Therefor I think that the decision to make gstreamer to the defacto standard is really the best way.\n\nThe same should be pointed out for browser extensions and other stuff like widgets, themes, plugins, etc..."
    author: "Matthias Fenner"
  - subject: "Re: Extensions"
    date: 2006-04-06
    body: "I think that extension system is not good solution. It's useful for geeks, but ordinary users were confused on extension search, install and upgrade. Distributed sources of extensions lead to incongruous usability, bugs and poor l10n and documentation. \n\nBest way - to rewrite several brilliant FF extensions (such as ScrapBook) as kdeaddons parts. It will be not hard job - for ScrapBook we can use BasKet and only code dragging formatted text from Konqueror. Mostly FF extensions can be replaced by KDE applications: Akregator, KGet and so on.\n\nOn the other hand we will have KDE4 plasmoids that bring Konqueror \"extension\" development on quite new level (sure, if plasmoid can be embedded on Konqueror window).\n\nMy conclusion: FF extensions should be reviewed only as useful ideas and concepts. :)"
    author: "Andrey Cherepanov"
  - subject: "Re: Extensions"
    date: 2006-04-06
    body: "Most of KDE users are power users, so why not to do an extension system? if someone wants to make an easy distribution oriented, the extension system could be deactivated through Kiosk.... With easily created plugins most of applications can be developped realy fast, anyone can contribute to the application without any knowledge of c++ or qt libraries.\n\nJust look Mac OS X, its an easy Operating System and Desktop but very powerful for advanced users with features like Spotlight, Automator, Smart Folders, etc \n\nAn advanced Desktop can be used by anyone, the big problem is how to make the Desktop seems great for any kind of user..."
    author: "biquillo"
  - subject: "Re: Extensions"
    date: 2006-04-07
    body: "> Most of KDE users are power users, so why not to do an extension system?\n\nWhat about kdeaddons? :)\n\n> With easily created plugins most of applications can be developed realy fast\n\nPlasma lets write application on script languages. :)"
    author: "Andrey Cherepanov"
  - subject: "Copy usability refinements from Firefox"
    date: 2006-04-06
    body: "Firefox may not have the fastest, best thought-out internals (it still carries baggage from its Netscape origin), but a lot of work has been put into making it usable. This includes the organization of menus, privacy options, etc.\n\nGiven Firefox's great adoption, I daresay it has worked well for them. I would suggest Konqueror get inspired from (if possible, even duplicate) Firefox's menu layout. This way, current Firefox users (who make up over a quarter of all Internet users), will find it easy to switch to Konqueror.\n\nThis might be a challange since there are so few developers working on Konqueror: the kfm-devel mailing list (https://mail.kde.org/mailman/listinfo/kfm-devel) has been going downhill since peaking in year 2000. Another problem is that Konqueror bundles a file browser with a web browser. This approach has proven to be a huge security hole for Windows Explorer, and the same would be true for Konqueror in its current for if it ever became mainstream. Why not follow Matthias Ettrich's suggestion (http://dot.kde.org/1123150234/) and split the web browser from the file browser?"
    author: "ac"
  - subject: "Re: Copy usability refinements from Firefox"
    date: 2006-04-06
    body: "Again the same thing I've never really understood. What makes local resources so much different than external resources that they need two separate applications to access? It's just like you should have one application for listening CDs, one for listening mp3s stored on hard disk and yet another for listening streaming audio despite the fact that all three do essentially the same thing.\n\nThe IE/explorer security holes aren't exploitable because file and web browsers are integrated but because huge parts of the operating system are being exposed to the browser engine (to assist the user, I'm being told). And Microsoft has the whole thing backwards anyway; they have two different applications with the same engine for web and file browsing."
    author: "atte"
  - subject: "Re: Copy usability refinements from Firefox"
    date: 2006-04-06
    body: "\"Again the same thing I've never really understood. What makes local resources so much different than external resources that they need two separate applications to access?\"\n\nBecause there is several years of evidence within the KDE project, regarding Konqueror, that says that file browsing and web browsing are not the same things. You need stuff in a web browser that is not necessary in a file manager - cookies, different arrangement of UI elements etc. It simply doesn't work.\n\nHowever, this doesn't mean that you can't have separate application which use common components between them. This is what you're getting confused about."
    author: "Segedunum"
  - subject: "Re: Copy usability refinements from Firefox"
    date: 2006-04-07
    body: "I beg to differ about my confusedness. You see, I'm not talking about file and web browsing from an application's point of view but instead from a user's. I know that, from a technical perspective, there is a huge difference between browsing web pages and files. But, again, so is between playing music from a CD and streaming it from a server.\n\nThe important point is that from a user's perspective they're very similar operations. And thus it makes a lot of sense to have one application implementing them.\n\n<humor>\nBy the way, anyone whose preferred editor is emacs automatically loses his/her argument about splitting konqueror into two different applications. Split your own damn editor first and then come back complaining about konqueror.\n</humor>"
    author: "atte"
  - subject: "Re: Copy usability refinements from Firefox"
    date: 2006-04-08
    body: "\"The important point is that from a user's perspective they're very similar operations. And thus it makes a lot of sense to have one application implementing them.\"\n\nNo they're not. Years of experience of users using web browsers, file managers and Konqueror have taught people using KDE that. They are just too different from a use case point of view. Even though some of those use cases appear to be similar, they are to disimilar to have one application performing them. The one application argument has been lost.\n\n\"Split your own damn editor first and then come back complaining about konqueror.\"\n\nDaftest bit of humour I've heard. I suggest people look at a text editor and Konqueror from a functional, use case point of view."
    author: "Segedunum"
  - subject: "Re: Copy usability refinements from Firefox"
    date: 2006-04-11
    body: "Bah, repeating a point doesn't make it true or false.\n\nAny evidence beside 'years of experience...'?\n\nMyself I am frustrated when the application used depend on the medium: those application are alway different/inconsistent such as the way you magnify, bookmark, etc.. for no good reason."
    author: "renox"
  - subject: "KGecko"
    date: 2006-04-06
    body: "What has become of the integration plans regarding gecko and konqueror?\nI think embedding gecko in the konqueror interface is a much better solution than bloating khtml/konqi by adding firefox related features, though it might get more difficult than it seems on the first glance.\nI'd like to have the (rendering)speed and reliability of the gecko engine combined with the amazing konqueror interface!\n\nWhat is your opinion?"
    author: "katakombi"
  - subject: "Re: KGecko"
    date: 2006-04-06
    body: "I don't mean to be rude, but the rendering speed and reliability of Gecko is crap. KHTML passes ACID2 and beats the crap out of Gecko in CSS performance (there's a CSS benchmark floating out there somewhere... while KHTML gives me 200-300ms times, Gecko gives me 1700ms times).\n\nPersonally, I think KHTML is the most advanced HTML rendering engine there is. Gecko is horrible bloatware. Why do you want it in KDE?"
    author: "Chase Venters"
  - subject: "Re: KGecko"
    date: 2006-04-07
    body: "There is a simple reason.  There are a lot of broken web sites out there and most of them that don't work in KHTML do work with Gecko.  Perhaps this is why it is bloated -- the need to deal with broken web page code."
    author: "James Richard Tyrer"
  - subject: "Re: KGecko"
    date: 2006-04-07
    body: "Get Konqueror for Win and the problem will be gone."
    author: "Gerd"
  - subject: "Re: KGecko"
    date: 2006-04-08
    body: "This is a common misconception. When a broken site works in Gecko, it chiefly means the site supports Gecko, not the other way'round."
    author: "ac"
  - subject: "Re: KGecko"
    date: 2006-04-09
    body: "Actually, what you said is a misconception.\n\nSites that are written to \"support\" browsers rather than being written to the published standards are often broken.  Broken meaning that they do not conform to the published standards and therefore do not work in all browsers.  \n\nWhat sites should support is the W3C Validator.\n\n"
    author: "James Richard Tyrer"
  - subject: "Re: KGecko"
    date: 2006-04-09
    body: "Well, yeah, we should live in a perfect world - thank you for stating the obvious.\n\nWhat I debunk is your false assumption that Gecko has more logic to handle broken cases, thus accounting for the bloat. \n\nThis is just wrong. Khtml is far superior and thorough in this respect. In fact, it has to be, because site owners typically don't test their sites with KHTML. When it renders a site it owes it only to itself.\nIn comparison, Gecko doesn't need to be as fool proof because it is more widely know and thus specifically *tested* and *worked around* by site owners.\n\nAnd yes, UA detection is still in very wide use - not much anymore for HTML/CSS, but for AJAX. To the point that some site that used to work in Mozilla, now don't in SeaMonkey because site owners switched the UA detection to supporting FireFox only.\nYou would have the exact same problem embedding Gecko: the broken/stupid sites don't support an engine, they support a browser.\n"
    author: "ac"
  - subject: "Suggestions"
    date: 2006-04-06
    body: "There are a lot of suggestions here to be more like Firefox. Please don't. I *love* Konqueror just the way it is. The performance is fantastic, and the lack of clutter is fantastic as well.\n\nI found something highly amusing - in response to people griping about Firefox 1.5's excessive memory usage, one of their developers told users that the memory consumption was necessary for good history performance. Amusingly enough, a quick glance at a Browser Speed Comparison page showed that Konqueror had better history performance than Firefox. Sans taking hundreds of megs of RAM.\n\nKonqueror has a lower incidence of security problems, better performance, lower resource consumption, and far better adherence to standards. Please keep doing exactly what you're doing now, because it's great."
    author: "Chase Venters"
  - subject: "Gmail and Other annoying problems"
    date: 2006-04-06
    body: "I know it's hardly konqueror's fault that gmail doesn't work (Google apparently does not heed very well to standards). However I would have to say using the less featured gmail with konqueror is probably the most annoying issue I run into. In general my only complaints with konqueror is its inability to render pages correctly that I'd like to view (plentyoffish.com is another example where bizarre problems occur). \n\nI'd imagine that most of these problems are bad web page design but konqueror needs extensions that handle these bad pages, unfortunate as this may be.\n\nCarmelo"
    author: "Carmelo"
  - subject: "Re: Gmail and Other annoying problems"
    date: 2006-04-07
    body: "You can use Gmail with konqueror just changing the browser ID to Firefox 1.0 under Tool menu in konqueror. It's works great for me in KDE 3.5.2. You can also set some rules to identificate your browser as you want in specific webpages, so you can use Konqueror ID in most of webs and set a rule only for gmail Tools -> Change Browser ID -> Configure -> Add gmail.com with Firefox 1.0"
    author: "biquillo"
  - subject: "Re: Gmail and Other annoying problems"
    date: 2006-08-15
    body: "Wouldn't that imply that Google is deliberately breaking their site in Konqueror.  Or is it that Firefox 1.0 has special needs which Google supplies when it's sent that user-agent header?"
    author: "Anonymous"
  - subject: "Re: Gmail and Other annoying problems"
    date: 2007-10-13
    body: "Thanks for this comment. It helped me a lot, I tryied to identify it like safari and it didn't work, but now it is working perfectly. Just identify it like Firefox 1.0.7. :-)"
    author: "Smajchl"
  - subject: "Re: Gmail and Other annoying problems"
    date: 2006-04-09
    body: "Gmail works fine for me with Konqueror.  Of course, there's the mildly annoying \"use a fully supported browser\" banner, but I can do what I need to do.  Admittedly, that's not much, because I use POP to download messages received to my gmail account for use in a real email application (Kmail) with a real email-specific UI (webmail sucks -- the web is inadequate for doing the sorts of things one wishes to do with email, and gmail's implementation has a number of problems (e.g. screwing up the message header)).\n\nThere remain a number of long-standing problems with Konqueror, however.  One is the abysmal script processing performance.  Far too often, Konqueror pops up a dialog box saying that \"a script is causing Konqueror to freeze\".  Well, it's not so much frozen as just horribly, horribly slow.\n\nAnother problem is the broken interaction between Konqueror (the browser), its cache, and external applications (image and other format handlers). Konqueror puts the content in a temporary file, then invokes a handler, passing the file name.  If the handler uses fork/exec, the parent exits, whereupon Konqueror assumes that the handler has finished with the content, and Konqueror removes the temporary file.  Meanwhile, the child tries to open the file (which Konqueror has removed when the parent exited), resulting in an error message.\n\nThe security capabilities could stand some improvement.  For example, I'd like to be able to reject certain content (e.g. Shlockwave Flash) on a site-by-site basis -- I can apparently do that currently only for Java and ECMAscript. Also, the security-related configuration should probably go under a security configuration setting rather than being spread throughout the settings.\n\nFor that matter, I'd prefer separating Konqueror the file manager from Konqueror the browser.  That might simplify improvements on both browsing and file management sides, and would certainly make configuration of each application less daunting.  It might pave the way for a better browsing experience, e.g. by making it easier to develop a browser with Mozilla-like scripting performance but with a kDE-friendly UI."
    author: "Bruce Lilly"
  - subject: "Regarding the scripting performance."
    date: 2006-04-09
    body: "At this point (3.5.2) it should be pretty comparable to mozilla's --- faster in some cases, slower in others. If you see any cases where it is still freezing please, please, file a bug report (and if you are using 3.4, and are not happy with speed of executing scripts, please upgrade to 3.5)\n\n"
    author: "SadEagle"
  - subject: "Re: Regarding the scripting performance."
    date: 2006-04-09
    body: "OK -- a quick check with a couple of known problem sites (http://maps.google.com and http://www.novell.com/linux/download/linuks/i386/update_for_9_2 and sub-pages)\nwith 3.5.1/3.5.2 shows promise; once browser identification settings which were necessary for usability have been deleted.  It would be nice if the upgrade process checked for known sites for which Konqueror improvements have eliminated the need to fudge browser identification and offer to remove such hacks from the configuration."
    author: "Bruce Lilly"
  - subject: "Re: Gmail and Other annoying problems"
    date: 2006-04-09
    body: "\"Of course, there's the mildly annoying \"use a fully supported browser\" banner,\"\n\nDon't use Gmail myself, but that banner sounds like a nice target for the addblock filter:-) I kind of use it more like a annoyance remover, than strictly for adds myself. \n\n\n\"I'd like to be able to reject certain content (e.g. Shlockwave Flash) on a site-by-site basis\"\n\nIt require a bit of manual work, and probably not as elegant, but you can set up the adblock filter to handle it. Someting like: reject.from.url/*.swf \n"
    author: "Morty"
  - subject: "Re: Gmail and Other annoying problems"
    date: 2006-05-14
    body: "It is not only the banner which is annoying ( in fact, and for me at least, well, it's quite easy to ignore ).\nThe main problem is that important stuff like adress autocompletion and ( if i recall correctly ) text formatting does not work ...\n\nIts also true that after having set mail.google.com to get the 'Firefox 1.0' id, everything started working correctly ...\n\n"
    author: "Xavier B"
  - subject: "Re: Gmail and Other annoying problems"
    date: 2006-05-24
    body: "I thought the problem with Gmail was just konqueror's browser identification, which is \"not known\" to gmail. In past KDE versions there were essential problems like lack (or bad implementation) of XmlHttpRequest object.\n\nNow, with KDE 3.5, I changed the ID to firefox and it seemed to work, but not completely. The Quick Contacts list is not shown, and the new Google Calendar is malfunctioning. It seems like incomplete or wrong DOM manipulation functionality in konqueror.\n\nGoogle may be using non-standard extensions, but it is setting new de-facto standards, IMHO. Now browsers not fully supporting Gmail are considered incomplete, and several browsers have been modified quite specifically to support these new Google services (gmail, calendar, chat). I think konqueror should do the same, to not stay behind.\n"
    author: "Alvaro Segura"
  - subject: "Re: Gmail and Other annoying problems"
    date: 2006-06-04
    body: "I stopped using konqueror as my main browser when I started using gmail as my regular email account. I'd love to go back, so I just fired it up. With user-agent konqueror I get that warning message and html mode, with user-agent safari I can't read emails (everything just appears totally messed up), with user-agent firefox I can read emails but hotkeys don't work and contacts are missing. And that's just from a two minute checkup."
    author: "Chris"
  - subject: "Re: Gmail and Other annoying problems"
    date: 2008-09-04
    body: "Can anyone tell me what the heck is up with plentyoffish.com. Is this site a joke or what? I have tried to sign in several times and have even resorted to making new id's, did this 4 times. Then about 5 minutes it signs you out and you can never sign back in. Damn frustrating"
    author: "Todd  Sterling"
  - subject: "editor box"
    date: 2006-04-07
    body: "I have been using konqueror for some time and for me it works quite well, the only time I find myself using firefox is when I have a site that uses a html box for making text bold/cursief changing, and for changing fonts. this is mostly used to write html mail and in WYSIWYG cms like products. this for some reason never worked in konqueror. is this a known bug, or am I just the only one who is experiencing this?\n"
    author: "Mark Hannessen"
  - subject: "Re: editor box"
    date: 2006-04-07
    body: "It's not a bug. It's a missing feature. \nSee http://bugs.kde.org/show_bug.cgi?id=48302 \n\nYou may want to vote for this bug to express your interest in that feature. \n\n"
    author: "cm"
  - subject: "Konqueror Interface"
    date: 2006-04-09
    body: "I think that Konqueror's view profiles shoould remember toolbar and button placement.  Also, there should be a feature where Konqueror can automatically switch view profiles when you use a certain kio-slave.  This would make usability much better.  An example is that for web browsing you dont need certaing buttons that you need for file browsing."
    author: "Thomas"
  - subject: "We don't need firefox extensions"
    date: 2006-04-10
    body: "We don't need firefox extensions, most users don't even use extensions. But if it was possible to make a standart for adding search engines to the browser it would be nice.... Maybe support the script that firefox uses to add a search engine... or even better create a standart for writing search engines... i cooperation with mozilla, perhaps even w3c...\n\nthat wy we could have a cross browser search engine library like: http://mycroft.mozdev.org/"
    author: "Jonas F Jensen"
---
Recently I've been on a mission to raise awareness of KDE and Konqueror in the wider Internet community as well as prepare to make the KDE 4 web browser a top-tier secure application.  As part of those ongoing efforts, I have been attending and participating in various workshops and conferences.  Recently I had the opportunity to participate in the <a href="http://www.w3.org/2005/Security/usability-ws/">W3C Workshop on Transparency and Usability of Web Authentication</a> in New York City.  Read on for the report.



<!--break-->
<p>It was an excellent chance to interact with usability and security experts from all over the world and discuss the challenges we face as well as candidate solutions to some of those problems.  The final day concluded with presentations from <a href="http://www.w3.org/2005/Security/usability-ws/presentations/33-staikos-improving-trust">KDE (me)</a>, <a href="http://www.w3.org/2005/Security/usability-ws/presentations/36-opera">Opera</a> and <a href="http://www.w3.org/2005/Security/usability-ws/presentations/30-mozilla">Mozilla</a> about how we see the challenges to the web from a software developer point of view, and how we view the potential candidates.  We left the meeting with a mandate to focus on specific areas that appear to be solvable and possibly form working groups in various organizations (IETF, W3C, etc) to attempt to solve those problems.  In addition to the slides, I also <a href="http://www.w3.org/2005/Security/usability-ws/papers/33-staikos-improving-trust">submitted a paper</a>, <a href="http://www.w3.org/2005/Security/usability-ws/papers/">as did others</a>.</p>

<p>In addition, today (April 5, 2006) I will be participating in a <a href="http://middleware.internet2.edu/pki06/agenda.pdf">panel</a> at <a href="http://middleware.internet2.edu/pki06/">PKI'06</a> at <a href="http://www.nist.gov/">NIST</a> where we will discuss and debate what we can do about easing the requirements to make decisions about security on the web.</p>

<p>If you're a software developer and serious about contributing to Konqueror and KDE 4, now is your chance to <a href="http://www.kde.org/jobs/">join us</a> and make a real difference!</p>



