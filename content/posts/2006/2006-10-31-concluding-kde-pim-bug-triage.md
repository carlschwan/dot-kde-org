---
title: "Concluding the KDE PIM Bug Triage"
date:    2006-10-31
authors:
  - "bschoenmakers"
slug:    concluding-kde-pim-bug-triage
comments:
  - subject: "Persistence"
    date: 2006-11-01
    body: "Whats even more impressive is that the PIM section on BKO is more alive than ever this week."
    author: "Sylvester"
---
Last weekend, a second bout of KDE bug triage took place in the <tt>#kde-bugs</tt> IRC channel on <a href="http://www.freenode.net">Freenode</a>. This round was dedicated to the KDE PIM module, with key applications <a href="http://www.kontact.org">Kontact</a>, KMail and KOrganizer.
All these applications have seen a drop in bug count, thanks to many people who joined the bug squad. During the weekend more than 180 bugs were confirmed, closed, some even fixed right away. That's a huge amount of bugs less to worry about for the KDE PIM developers.
But there's still plenty to be done! Read on to learn how.


<!--break-->
<p>Everyone can <a href="http://developernew.kde.org/Contribute/Bugsquad">help with bug triage</a>. Bug triage is a great way to contribute to the KDE project. You don't need programming skills to participate; the only thing you need is a recent KDE PIM installation which enables you to confirm bugs and provide additional information.</p>

<p>Again, thanks go to all who helped last weekend, it was a really productive two days.</p>



