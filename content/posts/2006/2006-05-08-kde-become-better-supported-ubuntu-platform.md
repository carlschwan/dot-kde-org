---
title: "KDE to Become Better Supported on the Ubuntu Platform"
date:    2006-05-08
authors:
  - "sk\u00fcgler"
slug:    kde-become-better-supported-ubuntu-platform
comments:
  - subject: "Vague"
    date: 2006-05-08
    body: "A bit vague, as a matter of fact. Either things are missing from the news, or they didn't happen."
    author: "cw"
  - subject: "Re: Vague"
    date: 2006-05-08
    body: "They didn't happen yet."
    author: "Anonymous"
  - subject: "Re: Vague"
    date: 2006-05-08
    body: "Read again, especially that parts about inviting / Paris and hiring."
    author: "sebas"
  - subject: "Re: Vague"
    date: 2006-05-08
    body: "I didn't skip anything, but when the big talks in Germany result in scheduling other talks in France this is what most people call 'vague'. And the part about hiring contains as much precise information as 'will hire some developers some time in the future', which is at best inconclusive and at worst meaningless."
    author: "cw"
  - subject: "Re: Vague"
    date: 2006-05-09
    body: "Be patient, it will become more concrete in the next weeks."
    author: "Anonymous"
  - subject: "Re: Vague"
    date: 2006-05-08
    body: "it seems that this was in essence a relationship building event. it builds upon the efforts and successes of the kubuntu team and the growth of canonical's linux play. it's a milestone, nothing more or less. aside from relationship building, the key events that took place imho include:\n\nannouncing that kubuntu will be on shipit\nmark publicly expressed that his company's support extends beyond just gnome (wearing the kde shirt on stage, talking about it with the media, etc)\n\nnot everything is about code all the time. sometimes it's about more \"ephemeral\" things like mechanisms of distribution ...."
    author: "Aaron J. Seigo"
  - subject: "Re: Vague"
    date: 2006-05-08
    body: "- announcing that kubuntu will be on shipit\n\nthis is the most important thing announced, I belive.\nThere is a LOT of people I know that have windows on their machines, but use sometimes a ubuntu live-cd to play with linux, but today this means they play with gnome, and I belive some of them could be happier with linux if using KDE.\n\nNever forgetting, here in Brazil, the number one live-cd distro is Kurumin, that used KDE and Knopix as base, conectiva/mandriva uses KDE, if there where kubuntu live-cds floating around, probally more people would use linux here."
    author: "Iuri Fiedoruk"
  - subject: "Is Kubuntu actually feasible?"
    date: 2006-05-08
    body: "I wonder whether and how the Kubuntu idea can actually work out. Just take this issue for example:\n\n- the next Kubuntu release comes out, ships (let's assume) amaroK 1.4\n- since Ubuntu's sound is based on Gstreamer, Kubuntu defaults amaroK to the Gstreamer engine\n- which is the Gstreamer 0.10 engine, the only one left (old gstreamer engine discontinued)\n- however the Gstreamer 0.10 engine is very young and very immature, lots of features missing, leaks, crashes etc. etc.\n\n- ergo bad amaroK implementation, unless one switches engine to xine or perhaps helix.\n\nAnd since people are not expected to know/care/find out about this engines mess, Kubuntu users disappointed by amaroK and go back to xmms/something else.\n\nWhat I mean to illustrate by this is example is the following problem: \n\nhow does Kubuntu plan to ship a good KDE on top of an OS which is developed in more or less full indifference to KDE's needs? \n\nNow I don't mean to speak evil of Ubuntu :-), but it's obvious that it's built with Gnome in mind, and that Kubuntu is just constantly trying to catch up.\nWhy not focus on an OS built either with a focus on KDE, or at least dedicated to all desktops' needs? \n\nWhat is Ubuntu's stance on desktop neutrality? How is this meant to be implemented, and is it meant to be? I was curious to hear about this, but it seems that nothing is really news on this topic.\n\n\n"
    author: "cw"
  - subject: "Re: Is Kubuntu actually feasible?"
    date: 2006-05-08
    body: "Actually, Kubuntu uses xine in dapper. There is a package called libxine-main1 that is used. Of course, it doesn't include support for much more than ogg with theora and vorbis. However, if you install another package (libxine-extracodecs) you get all this stuff back.\n\nSo yeah, Kubuntu is doing ok there, despite the distro being mostly focused on Gnome right now."
    author: "Amaranth"
  - subject: "Re: Is Kubuntu actually feasible?"
    date: 2006-05-08
    body: "> Actually, Kubuntu uses xine in dapper. \n\nGlad to hear. As a matter of fact, I've suggested it on the kubuntu wiki some while ago. This means that the example happened to be off the spot. The problem as such still remains, though.\n\nDon't get this wrong, I have nothing against a distro being focused on Gnome. My question is how a KDE implementation is expected to work well and especially realize the full potential of KDE on a system that doesn't help with this.\n\n"
    author: "cw"
  - subject: "Re: Is Kubuntu actually feasible?"
    date: 2006-05-08
    body: "I think you exaggerate the difference in dependencies between KDE and Gnome. Regardless, the whole point of this Dot news post is that KDE is apparently becoming a first-class citizen to Canonical."
    author: "Ian Monroe"
  - subject: "Re: Is Kubuntu actually feasible?"
    date: 2006-05-08
    body: "> I think you exaggerate the difference in dependencies between KDE and Gnome\n\nThat may be, since I'm certainly not an expert on the matter. What I can't be exaggerating is that:\n\n1. Ubuntu tools are written with Gnome in mind and Kubuntu spends its time trying to reimplement them for KDE\n\n2. Kubuntu can only get those Ubuntu users that are really willing to try something else, since KDE is neither the default desktop nor offered as a choice by the Ubuntu installer. And since all the hype says 'Ubuntu', and even Kubuntu says 'ubuntu', any 'spread kubuntu' battle is designed to be lost.\n\n> the whole point of this Dot news post is that KDE is apparently \n> becoming a first-class citizen to Canonical\n\nI got that, but since 'first class citizen' is just a way of speaking and means nothing in itself, I was wondering what civil rights this citizenship brings with it. And except for Shipit (which is not really news), the story says nothing about the newly acquired rights.\n\nSo I'll sum up my two questions:\n\n1. What does 'first-class citizen' mean, and how is being 'less default than others' compatible with first-class citizenship?\n\n2. In what way is it reasonable to try to provide the best KDE implementation on a system that is not optimized for KDE? Is this just for the pure challenge of it?\n\n\n\n\n\n"
    author: "cw"
  - subject: "Re: Is Kubuntu actually feasible?"
    date: 2006-05-08
    body: "> 1. What does 'first-class citizen' mean, and how is being 'less default \n> than others' compatible with first-class citizenship?\n\nThe default on Kubuntun will be KDE, the default on Ubuntu will be GNOME, the default on Xubuntu will be XFCE, dead simple :-) \n\n> 2. In what way is it reasonable to try to provide the best KDE \n> implementation on a system that is not optimized for KDE? Is this just for \n> the pure challenge of it?\n\nKubuntu will be optimized for KDE. The parts that are shared with GNOME will be optimized for both desktop, it's not all that hard to have both, KDE and GNOME running really fine on the same base system. Canonical will embed more KDE developers in their development process to assure that enough expertise for both desktops goes in."
    author: "sebas"
  - subject: "Re: Is Kubuntu actually feasible?"
    date: 2006-05-08
    body: "> The default on Kubuntun will be KDE, the default on Ubuntu will be GNOME, \n> the default on Xubuntu will be XFCE, dead simple :-) \n\n:-)\n\nYes, and also dead newspeak, as long as the very name 'Kubuntu' says 'I am just a modification of something called Ubuntu'. Being the default on a non-default version is really the same as being 'less default than others', and this is precisely what I was saying. How can a first-class citizen not be equal to the other first-class citizens? Or do we count citizen classes from zero?"
    author: "cw"
  - subject: "Re: Is Kubuntu actually feasible?"
    date: 2006-05-08
    body: "the relevant term here is \"looking a gift horse in the mouth\"\n\nrelationships are built; they don't appear magically in a day. they can be strengthened through cooperation and mutual discussion and subsequent efforts, and conversely they can be destroyed by preventing the same.\n\nthis is a step forward for the relationship between canonical and kde which only helps improve our stable of partners. given where we were with ubuntu when it first launched, a ton of progress has been made and the kubuntu team deserves all the credit for this particular set of developments imho. is it perfect right now? perhaps not, but perfection isn't the destination only the goal.\n\npersonally, i'm pretty confident that the user base will speak with its feet and that canonical will continue to improve their support of all the projects that are in demand and producing quality software.\n\nmeanwhile, Novell, Mandriva, Linspire, Xandros, Arch and on and on and on and on (i saw probably half a dozen kde-centric distros in brazil last month that i'd never even heard of) will continue doing their bit to grow the reach of kde and free software desktops."
    author: "Aaron J. Seigo"
  - subject: "Re: Is Kubuntu actually feasible?"
    date: 2006-05-08
    body: "Aaron,\n\nI very much appreciate your friendly and cooperative attitude, and really I neither want to nor am able ( :-) ) to spoil KDE's collaboration with anybody.\n\n> the kubuntu team deserves all the credit\n\nIndeed. I don't blame the kubuntu team for anything.\n\n> a ton of progress has been made\n\nThis is what I really feel compelled to challenge. The only progress I can see is that:\n\nA. now I can bring in KDE on Ubuntu through the back door\n\nand\n\nB. now I can order Kubuntu per mail\n\nAnd B. only happens to be the case because the LiveCD installer has been finished on time. Had the LiveCD installer not been finished, Canonical would have further shipped _two_ Ubuntu CD-ROMs and _zero_ Kubuntu CD-ROMs.\n\nWhich proves my point: Kubuntu and Ubuntu are not equal.\n\nNow I am not mad at anybody for this, after all it's not my money (unfortunately :-) ). \n\nBut what I really, really cannot understand is why the community is so excited about Kubuntu. I can understand XFCE being excited about Xubuntu. XFCE is relatively small, and it gets exposure this way. KDE, on the other hand, seems to be twice as popular as all other DEs combined. And the KDE community is possibly the most numerous of all Free Software communities. Under these circumstances, it's not surprising at all that Canonical wants to talk. What's surprising is that KDE makes news out of something that is not even strictly speaking promises. Now I don't suggest arrogance, but why not focus the news on KDE-friendly distros (whichever they be)? \n\nThis at least until Kubuntu becomes independent (and gets a name, a release schedule and more than one employee of its own).\n\n\n\n"
    author: "cw"
  - subject: "Re: Is Kubuntu actually feasible?"
    date: 2006-05-08
    body: "I think you should download dapper drake when it gets released, try it out.\nIt has really done great things for kde. It simplifies and ties kde closer together. A great desktop.\n\nThe only time where I saw that I was using ubuntu packages was when I installed firefox and it had by default some ubuntu bookmarks and no kubuntu bookmarks. Other than that, I've had none of the problems that you suggest should be there.\n"
    author: "anonymous coward"
  - subject: "Re: Is Kubuntu actually feasible?"
    date: 2006-05-09
    body: "I can understand your scepticism and partly share your view - KDE really deserves a distribution that is build completely around KDE.\n\nA better example is the CUPS trouble that plagued dapper during development. The problem is not that I would expect everything to work flawlessly in an unrealeased distribution, but the mere fact that an (at that time) incompatible unreleased CVS version of CUPS was introduced in Ubuntu because it goes well with GNOME and their tools - it improves the functionality. Unfortunaltey it breaks KDE functionality - a known issue, that under normal circumstances would have to be fixed within KDE before the next stable release of CUPS anyway. The matter is that Ubuntu's choice of CVS CUPS was based on what's good for Ubuntu/GNOME and not Kubuntu/KDE. So that's where I agree.\n\nOn the other hand you really should have a look at Kubuntu if you haven't already. The excitement in my oppinion is simply based on the great mix of usability and features that Kubuntu provides. In my book it really is the best KDE distro there is at this time - no matter if it builds upon a GNOME-centric core or not."
    author: "wiesen"
  - subject: "Re: Is Kubuntu actually feasible?"
    date: 2006-05-09
    body: "Why don't you just use Gentoo, SuSE or Mandrake then. Gentoo will give you complete control over what gets built.\n\nI am sure the Knoppix team currently enjoy the exact opposite of what you are trying to say here.\n\nI just think that this arguement is a little pointless. What I am more concerned about is what the Gnome team are doing to try to share technologies. We often hear about KDE making use of some technology that Gnome has been using but not the other way around.\n\n"
    author: "Ian Whiting"
  - subject: "Re: Is Kubuntu actually feasible?"
    date: 2006-05-09
    body: "Linking C++ libs from C is harder than linking C libs from C++...\n"
    author: "Eero Tamminen"
  - subject: "Re: Is Kubuntu actually feasible?"
    date: 2006-05-13
    body: "As someone who actually uses Kubuntu (Breezy), I can say that it has a lot of promise, however...\n\nThe two distros have separate websites, which do link to each other (and edubuntu, but not Xubuntu... not yet anyway...) but Ubuntu is the more famous of the two, the Ubuntu forums see more action than the Kubuntu forums at http://www.kubuntuforums.net and htp://www.ubuntuforums.org has a Kubuntu section.  \n\nIn the Ubuntu forums, they discuss solutions that mostly refer to the terminal, but if they refer to the GUI, they invariably refer to the GNOME GUI, with no explanation of the KDE equivalents, or even that solutions in the Ubuntu forums apply to the other *ubuntus in any extent (I've also searched for \"equivalent commands\" in the forum and found nothing, tbh I think there ought to be GNOME <==> KDE <==> XFCE commands sticky thread over there, detailing things that Synaptic == Adept, gedit == kwrite, gksudo == kdesu etc)\n\nI was attracted to the Ubuntu name as I heard it was an easy distro to get into and good for Linux noobs like me and I sought out Kubuntu because all the Linux experiences I had with live CDs (Knoppix et al) were with KDE.\n\nI do think that Kubuntu is an excellent distro and if the (K)Ubuntu team are going to reach out to KDE, then it can only benefit everyone for the KDE team to work with them and make Kubuntu a even better distro."
    author: "GeoNorth"
  - subject: "Re: Is Kubuntu actually feasible?"
    date: 2006-05-08
    body: "amaroK 1.4 is not going to ship with an immature engine. We've been there, got the t-shirt, learned our lesson. Likely amaroK 1.4 will only have Xine, Helix and NMM backends, at least in the first stable releases."
    author: "Ian Monroe"
  - subject: "The KDE and LinuxTag colors practically the same"
    date: 2006-05-08
    body: "Heh, it's funny to look at the picture with Marks Shuttleworth above and see that the KDE gear on his chest have almost exactly the same colors as the LinuxTag logo at the table.  Is this a coincidence? :-)"
    author: "Inge Wallin"
  - subject: "Re: The KDE and LinuxTag colors practically the sa"
    date: 2006-05-08
    body: "No, it's your monitor, what lies you :)"
    author: "hehe"
  - subject: "Re: The KDE and LinuxTag colors practically the sa"
    date: 2006-05-17
    body: "<sarcasm tag>\nUmm, this must be a conspiracy by NSA or equivalent! Colors... text... same... hmm... subliminal messages! Conspiracy!\n</sarcasm tag>"
    author: "jamal"
  - subject: "Encourage KDE developers to join Kubuntu!"
    date: 2006-05-08
    body: "****Canonical will make sure that some of the people Canonical will hire in the future for working on (K)Ubuntu come from the KDE world.****\n\nThis is wonderful news! Let's make sure all KDE developers know about this job opportunity!"
    author: "ac"
  - subject: "Re: Encourage KDE developers to join Kubuntu!"
    date: 2006-05-08
    body: "It's not news. It doesn't say when, doesn't say how many and doesn't say what for. It's just a diplomatic way of saying 'no, or at least not unless we change our minds'."
    author: "cw"
  - subject: "Re: Encourage KDE developers to join Kubuntu!"
    date: 2006-05-08
    body: "cw, are you just here to downplay every positive kde post or news? it really seems that you're not very pro kde for some reason especially on the *buntu side of things.\n"
    author: "fire............."
  - subject: "Re: Encourage KDE developers to join Kubuntu!"
    date: 2006-05-08
    body: "I think he's just being sceptical. And I agree with him, we need some real facts and not marketing speeches."
    author: "J.B."
  - subject: "Re: Encourage KDE developers to join Kubuntu!"
    date: 2006-05-08
    body: "No, as a matter of fact I'm here looking for positive news. Really. And I'm glad when I find them, and usually don't complain when I don't. \n\nHowever I did get a little carried away over this Ubuntu demagogy. This is not because I have something against Ubuntu, but it came on a bad background: I've been planning to do a system upgrade and researching for a good KDE distro, and this is what I found:\n\n- SUSE's focus on KDE is vanishing (although it still employs some 12 developers and it wouldn't be fair not to notice this). Prefers Mono for business reasons and this hurts both KDE (which I want) and Java (which I still need)\n\n- Mandriva no longer Mandrake, although it seems to employ a very dedicated developer. Tends to get obsolete and I unfortunately tend to be a bit of a freak when it comes to new software releases, especially KDE\n\n- Linspire replaces perfectly good apps with alternatives of its own, just because it can. And for some (manipulative) reasons Click'n'Run doesn't call apps their names (at least it calls K3B 'CD Burner')\n\n- a while ago I dropped Mepis for obsolescence and lack of configuration tools. I'll give the Kubuntu-based one a try, and actually this is my best hope for now :-(\n\n- Yoper? I don't understand what's happening there. Does anyone?\n\n- PC BSD didn't work when I tried it out. Might try the stable release, but since I need it on a laptop it's highly unlikely to work.\n\n- I'll also give PCLinuxOS a try again, last time it was horribly unstable\n\n- Kubuntu broken, neglected by its wider community, really a second-rate citizen apparently only meant to promote its bigger brother, pretending to be 'the KDE desktop' yet having to worry about whether it will be able to ship the new KDE release (because the Kubuntu release schedule actually depends upon a desktop environment that it doesn't even ship!)\n\nWhich means there's no good KDE distro left. And under these circumstances, it's considered big news that several months after saying 'first rate citizen', a distribution that wasn't even meant to ship KDE says 'first rate citizen' again! Where is dignity in this?\n\nDISCLAIMER: don't read this as 'against ubuntu' or 'against gnome' - it's not. Instead, read it as 'please stop making a fuss out of obscure demagogical speeches that don't even contain clear promises, let alone facts'.\n"
    author: "cw"
  - subject: "Re: Encourage KDE developers to join Kubuntu!"
    date: 2006-05-08
    body: "Your ought to try Kanotix."
    author: "Abe"
  - subject: "Re: Encourage KDE developers to join Kubuntu!"
    date: 2006-05-08
    body: "Well although I'm someone quite optimistic, I must agree with cw conclusion. I was waiting a lot from this famous linuxtag meeting and what I see right now really looks like marketing fuss. Apart from the Paris invitation I don't see anything concrete coming out of this meeting.\n\nKubuntu is (was ?) a big hope for all KDE people, a distribution really oriented towards KDE. But for the moment I'm really asking myself where it's heading ...\n\nOne more thing : from what I read in the last months news on the Dot, nearly each time a KDE booth was installed, Kubuntu CDs were given, which means Kubuntu is seen by most people (in the KDE sphere) as the KDE-oriented distribution of reference and is used to show real KDE possibilities. So shouldn't KDE developpers help more Kubuntu ones (well, one in fact  :p ) polishing the distro (bugs, integration, ...) ?  (open question)\n\nPS : I'm also considering installing a new KDE-oriented distribution. I'm particularly considering OpenSuse and Kubuntu. Any other suggestion ?"
    author: "Anonymous"
  - subject: "Re: Encourage KDE developers to join Kubuntu!"
    date: 2006-05-08
    body: "Look into Ark Linux ( http://ArkLinux.org ) -- it is 100% KDE-centric and always up to date. \n \n\"SUSE's focus on KDE is vanishing. Prefers Mono for business reasons\" \n \nThe Mono implementation of .NET sucks big time, and Novell is setting itself up to be crushed by a lawsuit from Micro$oft. Novell would be much better off taking Google's example and betting on Python and KDE. PyKDE is already supported commercially, is used to build real-world tools (ie. Guidance), and is used in universities to teach object-oriented programming. \n \nPart of the reason why the Ubuntu developer base is so biased toward gnome is that gnomers like Jeff Waugh post to planet.gnome.org whenever there is a job opening at Ubuntu (http://perkypants.org/blog/2006/03/06/jobs-at-canonical/). KDE devs should also inform each other about employment opportunities by blogging and sending emails to KDE developer lists ( https://mail.kde.org/mailman/listinfo/kde-devel ). \n \nKDE is technically years ahead of gnome, but KDE devs don't publicize their work as much. KDE devs: BLOG about *every* improvement you make, and feed your blog to http://Planet.KDE.org . \n \nFollow this rule: No svn commit without a blog! (ok, I'm exaggerating, but you get the point :-)"
    author: "ac"
  - subject: "Re: Encourage KDE developers to join Kubuntu!"
    date: 2006-05-09
    body: "> I was waiting a lot from this famous linuxtag meeting and what I see right now really looks like marketing fuss. Apart from the Paris invitation I don't see anything concrete coming out of this meeting.\n\nYou only hear now what was said in the public meeting. Stay patient to see the actions of what was agreed on in the second non-public meeting."
    author: "Anonymous"
  - subject: "Have you USED kubuntu dapper lately?"
    date: 2006-05-16
    body: "I have it installed in a normally-non-supported laptop (PCChips A530), in my home desktop, and in my work desktop. The KDE thing is first-class -- I agree that breezy wasn't, but dapper is promising to be a really good release."
    author: "Humberto Massa"
  - subject: "Re: Encourage KDE developers to join Kubuntu!"
    date: 2006-05-08
    body: "> Kubuntu broken, neglected by its wider community\n\ni personally run both kubuntu and suse. i have run other distros in the past on my desktop (including mandrake and red hat). with that background my personal take on kubuntu is this: the first release of kubuntu wasn't stellar, but it was promising. breezy was good enough to use, though it certainly had room to grow and lots of polish yet remained to do. when you have limited resources, that's to be expected. the real test is what happens on the next release.\n\ni'm running dapper right now on my laptop and it's a -huge- improvement on the already passable breezy. the time and effort being put into it is pretty obvious release-over-release. the kubuntu channel on irc grows, the number of people i know using it and even working on kubuntu itself grows ......\n\nso i'm not sure if your characterization is accurate. perhaps you got this impression from the start of the kubuntu project, but i think the kubuntu team really proved this possibility false with their consistent and continued efforts (kudos to them)\n\nbtw, i too was healthily skeptical of ubuntu when it started out. breaking into the distro game is really hard. i was hopeful but quietly skeptical of kubuntu when it started, too. but it's really hard to make final judgements on such projects that aren't obviously flawed in some way until they've had a couple years under their belt. perhaps you, like me, fall into the more conservative crowd when it comes to \"new and improved\" distros ;)"
    author: "Aaron J. Seigo"
  - subject: "Re: Encourage KDE developers to join Kubuntu!"
    date: 2006-05-08
    body: "<p>I am running kubuntu 5.10 and have to say that it is working really well. And KDE is going really well right here. That makes me skeptical about the conclusion that ubuntu is not KDE friendly.</p>\n\n<p>It just got gnome as default desktop. If you download Kubuntu's package from Ubuntu you can install fully working KDE and then upgrade it</p>\n\n<p>It was not hard for me a new user in the linux world to get into Kubuntu</p>\n\n<p>And there are Stand alone Kubuntu CDs that have ubuntu's installer but that defaults to KDE instead of gnome. When Dapper gets into stable I am gonna download Kubuntu directly</p>\n\n<p>Only issues I had with Kubuntu are the default appearance it got. I find kubuntu's splash screen hideous and had to replace it with KDE's I also had to modiffy a lot of things about the colors</p>\n\n<p><i>. Starting with Dapper Drake, the next release of Kubuntu to be released at the beginning of June, Canonical will ship CD sets of Kubuntu in the same manner as it did with Ubuntu in the past. Artwork of the CD sets was shown to the attendants of the meeting.</i></p>\n\nI personally think this is a great step forward."
    author: "vexorian"
  - subject: "Re: Encourage KDE developers to join Kubuntu!"
    date: 2006-05-08
    body: "> I am running kubuntu 5.10 and have to say that it is working really well. \n> And KDE is going really well right here. That makes me skeptical about \n> the conclusion that ubuntu is not KDE friendly.\n\nThe conclusion is that KDE is not currently a priority for Ubuntu, that Kubuntu is treated as 'nice to have' which is not 'first-class citizenship', and that nothing specifical has been said regarding how this problem will be fixed and whether it will be fixed.\n\nIf you're in doubt, take a look at this: \n\nhttps://launchpad.net/distros/ubuntu/+specs?show=all\n\nTen specifications are tagged 'essential', almost all Gnome specific, neither KDE specific. Even DapperReleaseProcess is Gnome specific:\n\n\"This still allows plenty of time (three weeks) from the GNOME string freeze to ours.\"\n\n\"Links and cross-references: GNOME 2.13.x/2.14 release schedule\"\n\nNeedless to say, Kubuntu happens to be following the same release schedule. So what exactly is the point in being enthusiastic about Kubuntu as a KDE distro, if it doesn't even adjust it's releases to KDE? Is it that it started to work rather well lately? Well, Suse for instance has already worked well for ages.\n\nOr is it just an irrational fear of being left aside? In that case just stop hyping Ubuntu until it doesn't leave KDE aside. I don't say 'attack', I just say stop giving it gratuitous praise.\n\n"
    author: "cw"
  - subject: "Re: Encourage KDE developers to join Kubuntu!"
    date: 2006-05-09
    body: "Ubuntu choose gnome (probably, but not only) because they have a fixed release schedule. For them it was easy to adapt they own release schedule around gnome's. That's one huge point gnome has over kde: fixed release schedules. Please note that I prefere KDE over Gnome. Since KDE depends heavily on QT (which is a good thing) it's more difficult to them to make a fixed release schedule. KDE is like debian, they release when it's ready. my 2\u0080\n\nIt's too late for dapper to make huge changes in kubuntu. We'll have to wait another release to see if cannonical and mark take KDE/Kubuntu more into account in their releases. Time will tell. AFAIK I'm using dapper on a daily basis. It's very stable/polished and with shipit support it will get kde/kubuntu closer to more people, which is a great thing."
    author: "javier"
  - subject: "Re: Encourage KDE developers to join Kubuntu!"
    date: 2006-05-09
    body: "> KDE is like debian, they release when it's ready.\n\nActually they release when it's scheduled, and far more often than debian :-)\n\n> It's too late for dapper to make huge changes in kubuntu. \n> We'll have to wait another release to see if cannonical and mark take \n> KDE/Kubuntu more into account in their releases.\n\nSeveral months ago the same 'first-class citizen' story has been told, and it wasn't too late for dapper at that time.\n\nMy suspicion is that the only reason why kubuntu exists is not having to admit that kde is not supported. From a marketing point of view it's understandable, of course."
    author: "cw"
  - subject: "Re: Encourage KDE developers to join Kubuntu!"
    date: 2006-05-24
    body: "\"That's one huge point gnome has over kde: fixed release schedules.\"\n\nHuge point to you, friend, not me."
    author: "Joe"
  - subject: "Re: Encourage KDE developers to join Kubuntu!"
    date: 2006-05-09
    body: "Ubuntu started out as a Gnome distribution. Kubuntu started out as an unofficial \"add-on\" to ubuntu not too long ago. It can hardly be a surprise that Ubuntu is still more focused on Gnome. \n\nSo far, KDE has been getting more and more attention for each release and that seems to continue. These things takes time.\n\nI don't quite understand that you're so skeptical. I think Mark has done an extraordinary job so far. He's done nothing that makes me suspicious of his intentions in regards to KDE.\n\nPs. I personally use Kubuntu and have done so for quite some time and while it is certainly far from perfect, I still like it and I'll most likely continue to use it."
    author: "Joergen Ramskov"
  - subject: "Re: Encourage KDE developers to join Kubuntu!"
    date: 2006-05-09
    body: "Kubuntu might start to sync to KDE releases. not sure how they will do that, but they intend to have a look at making it possible..."
    author: "superstoned"
  - subject: "Re: Encourage KDE developers to join Kubuntu!"
    date: 2006-05-08
    body: "> so i'm not sure if your characterization is accurate. \n> perhaps you got this impression from the start of the kubuntu project\n\nAs a matter of fact yes. I was quite enthusiastic about the first release, until I got fed with everything crashing around me and dumped it. Then I looked at the second release especially in order to see Adept, and although it wasn't as bad as the first try I suddenly noticed that I'm not impressed at all, especially by Adept (at that stage, however I hope it got better in the meanwhile).\n\nIf you say so, then I'll have a look at Dapper (and also at Mepis, especially for girlfriend). I don't question the fact that it might actually be good in spite of being treated as a stepchild. But I do think it's irrational for KDE to make waves around a distro that turns it into the 'other DE available on the other CD-ROM that no one has really heard about'.\n\n\n"
    author: "cw"
  - subject: "Re: Encourage KDE developers to join Kubuntu!"
    date: 2006-05-09
    body: "Aaron J. Seigo wrote:\n\"when you have limited resources, that's to be expected.\"\n\nMark Shuttleworth has limited resources? Yeah right.\nHe should put his money where his mouth is."
    author: "reihal"
  - subject: "Re: Encourage KDE developers to join Kubuntu!"
    date: 2006-05-09
    body: "> He should put his money where his mouth is.\n\nYou seem to have missed TFA. It was reporting on a recent meeting of KDE and Kubuntu developers at LinuxTag, where Mark Shuttleworth pledged to do precisely that.\n"
    author: "illissius"
  - subject: "Re: Encourage KDE developers to join Kubuntu!"
    date: 2006-05-08
    body: "You might consider SimplyMEPIS. Okay, it's now based on Kubuntu, but since KDE is MEPIS's bread and butter, it'll definitely be treated as a first class citizen."
    author: "Robert"
  - subject: "Re: Encourage KDE developers to join Kubuntu!"
    date: 2006-05-09
    body: "Why not try arklinux then? They now have a livecd aswell."
    author: "David"
  - subject: "Video of Mark's keynote?"
    date: 2006-05-08
    body: "Hi,\n\ndoes anyone have a recording of Mark Shuttleworth's Keynote at the LinuxTag in Wiesbaden?\n\nDavid"
    author: "David"
  - subject: "Re: Video of Mark's keynote?"
    date: 2006-05-17
    body: "according to the linuxtag website they'll post keynote's videos soon."
    author: "javier"
  - subject: "Re: Video of Mark's keynote?"
    date: 2006-05-17
    body: "I've found this: http://www.netzpolitik.org/2006/podcast-interview-mark-shuttleworth/\n\nIt's not the keynote, but it's a nice podcast talk about kubuntu/ubuntu and other topics. Probably similar to the keynote."
    author: "javier"
  - subject: "I can see the problem"
    date: 2006-05-08
    body: "I have used *buntu for about a year. I want to use only KDE, however, documentation is lacking greatly everywhere. I use *buntu because of the community, I can find the answer I need very easily. This is not true with Kubuntu or anything KDE I have seen. When it came to setting up my wifi in the Ubuntu it took 2 minutes the first time with a good howto. Until dapper I had not been able to get it configure properly, in the end it was just a need to better know how KDE works as far as configuration. So in short the way I see it is if you want Kubuntu to be first class you need the community involvement of a first class distro and you don't have that. If the community and documentation is their the users will come, especially if you this can happen before the release of KDE 4. I think that then the stage would be set for widespread adoption."
    author: "OKnewbie"
  - subject: "Re: I can see the problem"
    date: 2006-05-09
    body: "> If the community and documentation is their the users will come\n\nUnfortunately this is only true of relatively advanced users, the vast majority of uses will only come if there's great marketing to make them feel safe. (It's silly, but that's how people act). And all of the Ubuntu marketing says Ubuntu, none of it says Kubuntu. So the users won't come.\n\nMepis at least doesn't say Ubuntu, it has a real name of its own. From a PR point of view I'd rather understand an article on Mepis on the Dot, although again that woudn't be fair to Debian and Ubuntu.\n\nI'm losing my coherency and probably need to go to sleep :-)"
    author: "cw"
  - subject: "Re: I can see the problem"
    date: 2006-05-09
    body: "The current naming scheme of Ubuntu is indeed biased toward gnome and will invariably channel popularity toward gnome and away from KDE. Here are two ways of fixing it:\n\n1) Call the KDE version Ubuntu KDE and the gnome version Ubuntu GNOME. When users go to download Ubuntu, they should be given a choice of desktops.\n\n2) Call the gnome version Gubuntu, and let Ubuntu refer only to the base distro.\n\nI bet that if the naming was made fair, Ubuntu KDE downloads would *immediately* outnumber Ubuntu GNOME downloads."
    author: "ac"
  - subject: "Re: I can see the problem"
    date: 2006-05-09
    body: "i think dropping the name \"kubuntu\" is already a good step. Maybe just call it \"ubuntu/KDE\", which already sounds nicer..\n"
    author: "anon"
  - subject: "Re: I can see the problem"
    date: 2006-05-09
    body: "Yeah, let's make sure we have the names Gnome and KDE visibly present to get everyday people to switch from Wdoze. Folks love vague references that mean nothing, right? \n\nThink you'll ever see this exchange? \"What does KDE stand for?\" \"Uh, Kool Desktop...\" \"Alright, I'm gonna change operating systems to something with Kool in the name - sign up my whole corporation.\" "
    author: "Me"
  - subject: "Re: I can see the problem"
    date: 2006-05-09
    body: "i agree - but the gnomes don't want that. they are standing in the way of more linux use, imho - as Microsoft has said a few times, its developers developers developers that matters - and KDE is a much more mature and efficient environment to develop software in. you will even get commercial support (trolltech).\n\nwell, its clear anyway - KDE dev's are better at coding, Gnome ppl can make more noise. sadly the big company's (Novell, Red Hat) seem to prefer the latter."
    author: "superstoned"
  - subject: "Re: I can see the problem"
    date: 2006-05-09
    body: "there have been articles about mepis (and other kde OSes) on the dot in the past (example: http://dot.kde.org/1077133869/). when there is news in the distro world regarding kde (and this thing about kubuntu qualifies) it ends up here.\n\nwhile i'm certainly not proclaiming kubuntu to be the killer distro to end all kde distro shuffling, i'm also happy to see it progressing and gaining more mindshare within the canonical group that it had. that's all =)"
    author: "Aaron J. Seigo"
  - subject: "Try Archlinux"
    date: 2006-05-09
    body: "As mentioned before, if you want a distribution that gives that best kde performance and always the latest kde version, try archlinux.\n\nDid anybody notice that kde 3.5.3 will ship shortly after dapper ships? If kubuntu  users want kde 3.5.3, they will have to use unofficial packages or semi-official but unsupported ones. \nOn the otherhand, archlinux will always have the latest kde in the main repository.\n\nStill, if you don't 'need' the latest kde, suse first and next kubuntu will give the most polished kde installations."
    author: "ht"
  - subject: "Zelotism"
    date: 2006-05-09
    body: "what speaks against this KDE committment is the announcement of the edubuntu developer which followed his talks that the edubuntu would want to remove all educational apps from edubuntu which are KDE based and replace them by gtk ones. And: He would like to get a K-edubuntu with KDE. What madness, what toolkit zelotism.\n\nI think we should get away from toolkit centric thinking. \n\nIt is not motif etc. anymore. GTK and QT applications are quite interoperable and *more could be done*. We should get away from the Gnome = GTK, KDE = QT formula.\n\nPortland offers great ways of improvement. Then GTK often only means that applications uses toolkits which just render with GTK. X-theming, standardisation on the backend level and of course a common unix bookmark repository which could be used by firefox, Konqueror, Galeon etc.\n\nKDE users want GTK apps and want them to integrate better into their desktop environment of choice, same applies for KDE or QT apps under Gnome."
    author: "Andy"
  - subject: "Re: Zelotism"
    date: 2006-05-09
    body: "I think the reason for separating them is performance, mainly memory usage.\n\nA toolkit like Gtk/Gnome or Qt/KDE is big and this doesn't mean\njust disk space consumption.  When you run the applications, the libraries\nneed to be loaded into memory and they might require additional service\nprocesses (gconf, kio-slaves etc).\n\nIf the applications are using same libraries and support services,\nthey are shared between the applications and user can run more\napplications at the same time and the applications start faster.\n\nSo, if user is running Gtk/XFCE/Gnome desktop (Xubuntu/Ubuntu), by default\nGtk applications are installed, if user is running KDE desktop (Kubuntu),\nby default KDE applications are installed.  HOWEVER, this is just default,\nnothing is preventing user from installing the meta package bringing in\nset(s) of applications using the other toolkit, if she has enough memory\non the computer to use both, or otherwise just wants to have both.  The\npoint is that default set of apps is sensible (also from performance point\nof view) AND that it's easy to add/install other (supported) software.\n\nHaving Kubuntu and Ubuntu in close collaboration makes more well\nworking & integrated software available to users of both desktops.\n"
    author: "Eero Tamminen"
  - subject: "common bookmarks and bug reporting"
    date: 2006-05-09
    body: "I agree about the common bookmarks used for all browsers.  Opera, Konquerer, Firefox, Galeon, Links, Lynx, they should all save a user's bookmarks in /home/username/bookmarks or /home/username/settings/bookmarks or /home/username/browser/bookmarks.  Or someplace that all browsers can get to.  This should be a part of the LSB.\n\nFor Ubuntu and Kubuntu, there should be a desktop icon when running the liveCD, or the installed version, for test versions for making bug reports.  The link needs to bring up a webpage or a window for creating a simple email bug report and if email, then tied to an ubuntu smtp server. Lock it to a single send address to prevent outbound spamming.  Also have this icon launch a script to grab dmesg or whatever other logs are commonly collected or asked for. I've tested many LiveCDs and several Dapper testing CDs.  I've had various problems, mostly with install and i386 and had no idea how or where to send a bug report while running it.  I just hunted around and found a blurb:\n\n     Your comments, bug reports, patches and suggestions will help turn \n     this Beta into the best release of Ubuntu ever.  Please report \n     bugs through the Launchpad bug tracker:\n\n     http://launchpad.net/distros/ubuntu/+bugs\n\n     If you have a question, or if you think you may have found a bug but \n     aren't sure, first try asking on the #ubuntu IRC channel on FreeNode, \n     on the Ubuntu Users mailing list, or on the Ubuntu forums:\n\n     http://lists.ubuntu.com/mailman/listinfo/ubuntu-users\n     http://www.ubuntuforums.org/ \"\n\nBut that was not obvious from the kubuntu.org page and not found by chasing the \"bugs\" link.  The \"bugs\" link, takes you to https://launchpad.net/distros/ubuntu/+bugs and a whole rigamarol to report a bug or crash.   \n\nJust my opinion."
    author: "Brian"
  - subject: "Complaining."
    date: 2006-05-09
    body: "I see a lot of complaints about KDE/Kubuntu so I will try and show the direction I think Mark is taking based on past actions and news articles rather then just feelings of skepticism.\n\n1.  Ubuntu started as a gnome distribution.  (Probably for its predictable release cycles of every 6 months)\n\n\n2.  Some people wanted a kde based ubuntu distribution.  One of the cheif people being Jonathan Riddell\n\nSee this link for an interview with Jonathan Ridell\n(http://behindubuntu.org/interviews/JonathanRiddell/)\n\n3.  Mark Hired Jonathan to start working on Kubuntu.  At that time Kubuntu was not supported with Shippit.\n\n4.  At some point Mark Shuttleworth announced that Kubuntu would be a first class distribution along with Kubuntu.  Some time after Kubuntu was supported with shippit (xubuntu is not yet as far as I know).  This is a significant since shippit doesn't cost us anything but it costs Mark Shuttleworht a lot.  Mark I guess was impressed enough with Kubuntu to start shipping it with Shippit.\n\n5.  Some people complained on the http://www.kubuntu.de/ that Mark Shuttle worth was not giving access to some of the programmers, wanted more control and wanted ubuntu to hire more kde developers and some other general complaints.  They also wanted to know what first class distribution meant. (I can't find the orgiginal article)\n\n6.  Mark sent out this announcment about the meeting at linuxtag for kubuntu and kde \nhttps://lists.ubuntu.com/archives/ubuntu-announce/2006-April/000071.html\nBasically if there is interest in the KDE community to work with Kubuntu Mark is ready to give a lot more control to KDE /Kubuntu developers even to the point of eventually letting Kubuntu have it's own release cycle (if there's enough support for it).  (see the story)\n\n7.  We have the this recent announcment where Mark met with KDE and Kubuntu developers about where things can go.  It seems there is some interest in the KDE community to work with Kubuntu.  \n\nA lot has been accomplished and there is a lot more to go before Kubuntu can be a real \"first class\" distribution but this depends not only on Mark Shuttleworth but on the KDE community as well.\n\nBasically it seems to me that Mark is doing more and more for KDE starting from nothing when Ubuntu was first realeased.  Mark seems to start small and adding support for stuff rather than starting big and taking away like Redhat did and SUSE did with KDE.  \n\nSome people in the KDE community want more support for KDE/Kubuntu.  Mark seems to be doing that.  The question is whether the KDE community will support those efforts or whether Mark Shuttleworth is wasting his time.  It seems there are enough people interested in Kubuntu/KDE that it's probably not a wast of time.\n\nPS.  I use Ubuntu and now prefer GNOME.  Before Ubuntu I actually liked KDE better.  Maybe Kubuntu can make people give KDE another try."
    author: "gardion"
  - subject: "Re: Complaining."
    date: 2006-05-09
    body: "> PS. I use Ubuntu and now prefer GNOME. Before Ubuntu I actually liked KDE \n> better. Maybe Kubuntu can make people give KDE another try.\n\nThat's a strange and a bot twisted way of looking at things, since according to all apparencies the sole purpose of Kubuntu is making KDE users give Ubuntu a try. And since there are lots of these KDE users out there, it certainly looks like a trap. My question is whether Kubuntu is still a promotional trap (take this word lightly), or certain things have changed. Up to this point nothing has changed, strictly speaking not even Shipit, which will only be available for Kubuntu in the (admittedly near) future."
    author: "cw"
  - subject: "Re: Complaining."
    date: 2006-05-09
    body: "> That's a strange and a bot twisted way of looking at things, since according\n> to all apparencies the sole purpose of Kubuntu is making KDE users give\n> Ubuntu a try. \n\nWhat a load of crap. Give it a rest cw. The \"kubuntu is a gateway drug for Ubuntu usage\" theory doesn't hold any water at all. The Gnome desktop isn't even offered as an option when installing Kubuntu.\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Re: Complaining."
    date: 2006-05-09
    body: "I've never said 'gateway drug', I just got carried away with this because I don't understand the Kubuntu project's role within Ubuntu, neither technically nor politically, and it seems that no one is able or willing to explain it. Of course I don't suspect a conspiracy, but I do suspect a serious lack of overall direction, and I was hoping that maybe I'm wrong. \n\nHowever the most encouraging answers that I've got have been along the lines of 'be patient, the real news is secret for now'. And since I can afford being patient, I will. Sorry for the inconvenience."
    author: "cw"
  - subject: "Re: Complaining."
    date: 2006-05-29
    body: "> 'be patient, the real news is secret for now'\n\nI'm not sure, if secrecy is any good for _Open_ Source projects."
    author: "Anonymous"
  - subject: "Re: Complaining."
    date: 2006-05-09
    body: "Actually one of the main reason I switched to Gnome was that Ubuntu made GNOME very clean on the desktop and cleaned up a lot of the menus.  KDE had seemed a lot more cluttered in terms of menu items.  My hope is that Kubuntu & KDE clean up a lot of the less useful menu entries and gives a much cleaner desktop to start with.\n\nSecondly if Mark is trying to get KDE users to swtich to GNOME, then he is a fool.  He is much better targeting windows users (90-95% of the destkop market) rather than kde users (<1% of the desktop market).  If you look at the number 1 bug on gnome you will see that Mark is targeting windows users.  He is trying to get them to swtich to Linux and using ubuntu / kubuntu as a platform for other linux distributions.  \n\nFinally you can always use Mepis a KDE/kubuntu based distribution, if you are concerned about being pushed into gnome.  Mepis is definatly a KDE based distribution and it is number 4 on distro watch in the last month, scoring higher then Kubuntu."
    author: "gardion"
  - subject: "Owes?"
    date: 2006-05-09
    body: "This \"community\" acts like Mark owes you something just for existing. The guy doesn't have to even recognize your existence if he doens't want - his his distro/his dollars."
    author: "Me"
  - subject: "Kubuntu's biggest problem: Bugs"
    date: 2006-05-09
    body: "  Hi!\n\nI've been wanting to use Kubuntu for about a year now, but the apps keep crashing on me... and I can't really find any pattern. This goes for both Hoary and Breezy, no matter which of the three available platforms I've used.\n\nAt last I gave up and went with Ubuntu (GNOME) modifying it's panel to look like KDE's, but that doesn't give me everything that I was looking for. :-)\n\nNow however I've started testing SimplyMEPIS (which now is moving over to a Ubuntu-base), despite it's ugly default theme (which I changed) it looks very promising. I also don't seem to be alone switching from Kubuntu to SimplyMEPIS, because of the bugs (according to forums&mailinglists). I actually would like Kubuntu better... but the bugs are really a big problem... Why doesn't people involved with the project talk more about?\n\n(Check how Kubuntu has been going down at distowatch.com lately, I think this might be the answer... people give up on it because of the bugs.)\n\nWell anyways, it's nice to see that Mark is showing interest for KDE while others (such as SUSE&Redhat) go in the other direction. I think his, along with the community's efforts (mentioned in the article) will help clearing out the bugs in the long run. Hope SimplyMEPIS's move to a Ubuntu-base helps in some way too.\n\n  cheers, Simon"
    author: "Simon R\u00f6nnqvist"
  - subject: "Re: Kubuntu's biggest problem: Bugs"
    date: 2006-05-10
    body: "Try Dapper and I think you will be pleasantly surprised. A lot of effort has gone into the quality of Dapper and making sure that it is much more stable than the previous versions. As Aaron mention earlier, the difference is -huge-.\n\nThere is also a live CD too which you can easily try out.\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "yast"
    date: 2006-05-10
    body: "Would be nice to get Ubuntu's support for YaSt for Debian. Yast could lead to simplification and unification."
    author: "Hup"
  - subject: "Re: yast"
    date: 2006-05-10
    body: "Kubuntu dapper brings many easy-to-use-yet-powerfull-tools to configure the OS written in python (guidance). Probably yast is richer right now, but they are getting closer."
    author: "javier"
  - subject: "Well.. "
    date: 2006-05-11
    body: "I've tried Both versions (K)Ubuntu etc...\n\nI've found the gnome version to be far more stable & easier to use (hardly any app crashes).. on the other side KDE seems much nicer (easier on the eyes, yet you can change the look ^^).. I prefer KDE simply because of the way it's set out... But I miss the stable gnome desktop.. (that's what I think)\n\non KDE there is also some major bugs in the GUI when setting up Wireless access, I had to edit the network file to get a connection to my wireless router!\n\npraise network-admin in gnome!\n\n(btw I'm very new to linux...)\n\nKeep it coming!"
    author: "Mark"
  - subject: "Re: Well.. "
    date: 2006-05-11
    body: "KDE itself is normally very stable. Kubuntu, on the other hand, has been normally very unstable. However the new version (Dapper) is reported to be vastly better, so do try it out (I certailnly will).\n\nAlso check the new SimplyMEPIS (which is based on Kubuntu) and the new SUSE (SUSE has always had a great and very stable KDE).\n\nBTW everybody, sorry for my temporary madness, although I still am eager to hear some meaningful news :-)\n"
    author: "cw"
  - subject: "Gnubuntu?"
    date: 2006-05-12
    body: "I guess the titles Kubuntu, Xubuntu, etc. really declare the obvious: that they are a derivative project.\n\nUbuntu's philosophy seems to be ease of use, and the question here one of choices. This is quite a cliche argument among linux users, but there is a reason it is addressed again and again. Every choice provided by distro architects is another choice that must be made, and another factor of complexity introduced to the system.\n\nMaybe my question reflects a puritan idealism which if carried to its logical end would result in a more Gentoo-like distro, WHICH IS NOT MY INTENTION,\n\nbut if ubuntu is going to be developed with KDE as much in mind as GNOME, shouldn't the gnome version be renamed GNubuntu?"
    author: "stairwayoflight"
  - subject: "Re: Gnubuntu?"
    date: 2006-05-13
    body: "Except that that would imply the distribution is made by/directly affiliated with the FSF.\nPlus \"Ubuntu\" and \"Kubuntu\" mean something, whereas, \"Gnubuntu\" doesn't."
    author: "Mark"
  - subject: "Re: Gnubuntu?"
    date: 2006-05-17
    body: "Gubuntu perhaps? why the N?\n\nGubuntu vs Kubuntu all Ubuntu's... ?\n\nBut seriously, i don't see the use in renaming anything now, it would only confuse people.\n\nUbuntu is a good step in the right direction, but it's still not as easy as Mac/Win.\n"
    author: "Jocke"
---
At <a href="http://www.linuxtag.org/2006">LinuxTag</a> on Saturday, a meeting of Kubuntu and KDE contributors was held in order to improve the collaboration of both projects.  The aim was to to talk about the common future of both projects. Jonathan Riddell and Mark Shuttleworth from Canonical attended the meeting. Later in his keynote speech to the conference, Mark publicly committed to Kubuntu as an essential product for Canonical and showed his commitment by wearing a KDE t-shirt.




<!--break-->
<div style="width: 319px; border: solid grey thin; margin: 1ex; padding: 1ex; float: right">
	<img src="http://static.kdenews.org/jr/mark-shuttleworth-linuxtag.jpg" width="319" height="390" /><br />
	Mark Shuttleworth during his keynote at Linuxtag 2006.
</div>

<h3>Introductions</h3>

<p>At the beginning of the meeting, Mark outlined Canonical's vision of the future of Ubuntu Linux and the role of Kubuntu and KDE therein. Canonical wants to create a free, professional economic eco-system and help to develop and transport KDE's vision of the future of the free desktop. Starting with Dapper Drake, the next release of Kubuntu to be released at the beginning of June, Canonical will ship CD sets of Kubuntu in the same manner as it did with Ubuntu in the past. Artwork of the CD sets was shown to the attendants of the meeting. </p>

<p>Eva Brucherseifer held a short introduction to KDE, explaining different aspects of how the community works together, challenges in the community life cycle such as finding enough new developers and improving the sustainability of KDE as a Free Software project. She also explains that KDE is working actively on extending the KDE community further to non-developers. Eva described the decision making process within KDE as being very much bazaar-like; people exchanging ideas, seeking mindshare and creating it.</p>

<h3>The Future of Kubuntu</h3>

<p>Mark said that Canonical has created some tools to make Free Software developer communities more scalable such as <a href="http://launchpad.net/rosetta">Rosetta</a> and <a href="http://launchpad.net/malone">Malone</a>. It is also important to help newcomers to get into the project. Ubuntu's vision includes offering multiple desktops because it is a healthy way for a sustainable future that those desktop environments should work great together. Tighter integration of Canonical's collaboration tools with e.g. KDE's bugzilla is another keypoint of the collaboration in the future.
</p>
<p>Mark went on acknowledging that native office programs - such as KOffice - are the preferred way to go, but that the process of adopting those is not easy because of exchange of for example files with the Windows platform and because they do not work on Windows as OpenOffice.org does. Canonical is committed to making sure to connect the source code vision of a project such as KDE to a user and market centric vision of Ubuntu. They will invite a number of contributors from different parts of the KDE project to the next developers meeting in June in Paris, where the next Kubuntu release - Edgy Eft - will be sketched.</p>

<h3>Split up Sessions</h3>

<p>After the more general part of the discussion, the group split up to discuss issues such as human computer interaction, artwork, marketing and naturally technological issues.</p>

<p>Collaborations in the improvement of the technical side of things cover topics such as communication on developer level, for example discussing distribution-specific problems directly between the Kubuntu developers and packagers, but also very specific issues such as extending the Ubuntu laptop testing and any other future derivatives to ensure a high quality level of future releases of Kubuntu.</p>

<p>From the human computer interaction point of view, improvements in the acceptance of usability and accessibility are important to be made in the future. Creating more mindshare among artists is another challenge for the future. KDE needs more artists and must help them enter the Free Software community. Kubuntu and KDE will collaborate in trying to attract more non-coding contributors to the projects, not only for work on the human-computer interaction, but also on promotion and marketing.</p>

<p>There are also quite a lot of technological issues that can be solved by having more KDE people in the middle of the Ubuntu community, currently most of the Ubuntu platform developers are mostly using GNOME when developing the underlying operating system basis.  In order to achieve that improved interoperation between the different desktop environments on the Ubuntu platform, Canonical will make sure that some of the people Canonical will hire in the future for working on (K)Ubuntu come from the KDE world.</p>





