---
title: "aKademy 2006 Sponsors"
date:    2006-08-21
authors:
  - "jriddell"
slug:    akademy-2006-sponsors
comments:
  - subject: "Novell on the way down"
    date: 2006-08-21
    body: "Who would have though that (K)ubuntu would be a bigger KDE sponsor than Novell?  I guess this means the SuSE department can no longer sponsor us on their own.\n\nAlso it almost seems like the logos in the bottom of the pyramid are larger than those on the top. "
    author: "Carewolf"
  - subject: "Re: Novell on the way down"
    date: 2006-08-21
    body: "Assuming the gold/silver/bronze status equates to money/stuff in kind donated it's good to see Kubuntu up there at the top as it suggests that Canonical are serious about investing in Kubuntu and KDE.\n\nIt's also nice for me, as a SUSE user, to see Novell taking an interest, even in the third tier. I don't care what they do with SLED/SLES, but KDE is an essential part of me sticking with openSUSE.\n\nI think Trolltech need a bigger logo - I almost missed them :-)"
    author: "Simon"
  - subject: "Re: Novell on the way down"
    date: 2006-08-21
    body: "> that Canonical are serious about investing in Kubuntu and KDE\n\nBecause of 15000 Euro!? Maybe if Canonical starts to employ more than one full-time KDE developer - like Novell does."
    author: "Anonymous"
  - subject: "Re: Novell on the way down"
    date: 2006-08-22
    body: "Touch\u00e9. \n\nSponsoring events is cheap, and while important it is much less important than sponsoring developers. "
    author: "Carewolf"
  - subject: "Re: Novell on the way down"
    date: 2006-08-22
    body: "Well, they added kubuntu to shipit too ;-)\n\nSeriously though, it is a statement - deciding to get your name in at the top level of sponsorship of the major KDE event. Sure it's cheaper than hiring developers which probably helped, although if they're not serious about supporting kubuntu - I perhaps should have said 'kubuntu' rather than 'kubuntu and kde' - then 15000 euro strikes me as a pretty expensive joke.\n\nAs you say, Novell/SUSE have for a long time and continue to do a lot of great work in KDE - as in creating stuff that's useful across kde rather than just for suse. The concern I have is that when money is tight the obvious motivation is to put money into projects that have a higher relevance to the products that they hope will make them money (that is Gnome for SLED) unless they have strong (paying) customer demand for decent kde support (which I don't know) and I can't honestly criticise them if they do that. So it's good to see the Novell name up there in addition to the other stuff they continue to do."
    author: "Simon"
  - subject: "Re: Novell on the way down"
    date: 2006-08-22
    body: "And just to clarify, I have no problem with Novell resources going into Gnome. Sharing a bigger cake (Novell's resources) is just as good as having most of a smaller cake (the old SuSE). My concern has been and to some extent still is that if the Novell free software cake gets smaller then KDE's slice might shrink quite rapidly, which is pure selfish self-interest, but there you go."
    author: "Simon"
  - subject: "Re: Novell on the way down"
    date: 2006-08-23
    body: "Well, Novell took SuSe and transformed it. I am not against Gnome. But I am against those Gnome crackpots who broke SuSE just to push Gnome."
    author: "hill"
  - subject: "Re: Novell on the way down"
    date: 2006-08-21
    body: "Well, Novell sponsored (together with trolltech) KDE Four Core (even mentioned in the article). I think they are still a big and powerful contributor to KDE. Even though KDE is no longer SuSE's standard desktop they still support it and provide good solutions for both big desktop environments."
    author: "rittmey"
  - subject: "Re: Novell on the way down"
    date: 2006-08-21
    body: "opensuse don't have a standard desktop\n\nanyway, majority opensuse user use kde...\n\nopensuse continue to be a very kde centric distribution"
    author: "Marc Collin"
  - subject: "Re: Novell on the way down"
    date: 2006-08-24
    body: "Novell is the new SCO. Cladera was fucked up, Novell did the same with Suse. So the transition ot a community distribution will be crucial."
    author: "furangu"
  - subject: "Re: Novell on the way down"
    date: 2006-08-21
    body: "> I guess this means the SuSE department can no longer sponsor us on their own.\n\nMaybe funds were shortened? Remember, Novell was only Silver sponsor for GUADEC."
    author: "Anonymous"
  - subject: "Re: Novell on the way down"
    date: 2006-08-22
    body: "\"Who would have though that (K)ubuntu would be a bigger KDE sponsor than Novell?\"\n\nMaybe you've been out of the loop, but Gnome is now the recommended desktop by Novell."
    author: "ac"
  - subject: "Novell only bronze"
    date: 2006-08-21
    body: "What a shame."
    author: "AC"
  - subject: "Re: Novell only bronze"
    date: 2006-08-21
    body: "What a troll."
    author: "Anonymous"
  - subject: "Re: Novell only bronze"
    date: 2006-08-24
    body: "Novell/SUSE sponsor more KDE developers than ANYONE else. Now tell me how valid your point is."
    author: "Francis"
  - subject: "Great results on short notice"
    date: 2006-08-21
    body: "We really need to thank not only our sponsors, but the aKademy team that has secured this sponsorship on such short notice.\n\nIt's an impressive list, and a heck of a lot of work was done in a compressed time frame to get everything in order."
    author: "Wade Olson"
  - subject: "Re: Great results on short notice"
    date: 2006-08-21
    body: "Second that.\n\nIt's impressive to see Intel, Google, Enterprise Ireland and BitBuzz on there, companies that don't (ok, I know little about bitbuzz) have such a clear reason to be involved as the distros and free software media/companies (although granted Intel take open source hardware support seriously and Google support free software through soc etc, but it's good to see them supporting KDE). I guess what I'm trying to say is that while you can approach Trolltech, Kubuntu, Novell etc and perhaps expect to be successful, getting the others involved suggests that the akademy team has put forward a professional and persuasive sponsorship case to these guys.\n\nSo, well done!"
    author: "Simon"
  - subject: "hurrah!"
    date: 2006-08-21
    body: "hurrah!"
    author: "Vlad Blanton"
---
aKademy 2006 has announced the <a href="http://conference2006.kde.org/sponsors/">sponsor's list</a> for KDE's World Summit.  This is one of the our most impressive list of sponsors to date.  Our Gold sponsors are the home of Linus Torvalds <a href="http://www.osdl.org">OSDL</a> and the KDE based distribution <a href="http://www.kubuntu.org">Kubuntu</a>.  Housing the conference as our host institution is The School of Computer Science at Trinity College Dublin.  Read on for the full list.










<!--break-->
<p>Silver sponsors of aKademy 2006 are computer and graphic chip makers <a href="http://www.intel.com">Intel</a>, Qt developers <a href="http://www.kdab.net">Klarälvdalens Datakonsult</a> also known as KDAB, <a href="http://www.nlnet.nl">NLNet</a> the Dutch open source foundation, <a href="http://www.trolltech.com">Trolltech</a> makers of the worlds best application framework (and sponsor of the <a href="http://dot.kde.org/1151271635/">KDE Four Core</a> meeting) and user friendly distribution <a href="http://www.xandros.com">Xandros</a>.</p>

<p>Our bronze sponsors are <a href="http://www.enterprise-ireland.com">Enterprise Ireland</a> the Irish state development agency, <a href="http://www.google.com">Google</a> the leading internet search engine, KDE and Qt consultants <a href="http://www.staikos.net/">Staikos Computing Services</a> and <a href="http://www.novell.com/">Novell</a> creators of openSUSE and the SUSE Enterprise line (another recent sponsor of <a href="http://dot.kde.org/1151271635/">KDE Four Core</a>).</p>

<p>We will also have free Wifi provided by <a href="http://www.bitbuzz.com">Bitbuzz</a>, <a href="http://www.linux-magazine.com">Linux Magazine</a> provide invaluable support as our Premier Media Partner while our other Media Partners are tech book company extraordinaire <a href="http://www.oreilly.com">O'Reilly</a> and <a href="http://www.realnetworks.com">RealNetworks</a> creators of The Helix Community.</p>

<p>Many thanks to all our sponsors and to Tink who has done amazing work in coordinating with these companies.  We still expect a few more names to be added to the list and it is not too late if you want to <a href="http://conference2006.kde.org/sponsors/akademy-sponsor-call.pdf">sponsor aKademy 2006</a>.</p>

<img src="http://static.kdenews.org/jr/akademy-2006-sponsors.png" width="800" height="498" />
