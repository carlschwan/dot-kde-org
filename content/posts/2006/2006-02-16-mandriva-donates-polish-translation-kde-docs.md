---
title: "Mandriva Donates Polish Translation of KDE Docs"
date:    2006-02-16
authors:
  - "klichota"
slug:    mandriva-donates-polish-translation-kde-docs
comments:
  - subject: "ha"
    date: 2006-02-16
    body: "palmsource invests in linux, great huh?"
    author: "curtain-calls"
  - subject: "Fajnie."
    date: 2006-02-16
    body: "Wielkie dzi&#281;ki ludziska. ;-)"
    author: "dpc"
  - subject: "Mandriva donates"
    date: 2006-02-20
    body: "I would like to see Mandriva to supply kde 3.5.1 for there mandriva users!\n\nIn the past it was available as a download at the kde site, but for several years Mandrake/Mandrive stopped to deliver new kde versions\n\nFor kde 2006 users the only thing is get the Cristmas KDE version and you get 2006 with kde 3.5.1.\n\nR."
    author: "R.Koendering"
  - subject: "Re: Mandriva donates"
    date: 2006-02-21
    body: "The Club Xmas edition (if you refer to this: http://club.mandriva.com/xwiki/bin/Main/launchdec2005specreleaseclub) comes with old KDE AFAIK. I use KDE 3.5.1 on Mandriva from http://www.seerofsouls.org."
    author: "Ferenc Veres"
---
The <a href="http://www.mandriva.pl/">Polish department of Mandriva</a> has <a href="http://sourceforge.net/mailarchive/message.php?msg_id=13654485">contributed over 100 files</a> of documentation translations to the <a href="http://kdei18n-pl.sourceforge.net/">Polish localisation team</a>.  The commits (<a href="http://websvn.kde.org/branches/stable/l10n/pl/docmessages/?rev=494024&amp;view=rev">1</a>, <a href="http://websvn.kde.org/branches/stable/l10n/pl/docmessages/?rev=494026&amp;view=rev">2</a>) are made up of over 8000 messages.  This allows Polish people to get an even better experience when using KDE in their native language.  The KDE Polish team would like to thank Mandriva for their contribution and encourage GNU/Linux companies in other countries to support local versions of KDE.







<!--break-->
