---
title: "KDE Commit-Digest for 4th June 2006"
date:    2006-06-05
authors:
  - "dallen"
slug:    kde-commit-digest-4th-june-2006
comments:
  - subject: "I cannot read it!!"
    date: 2006-06-04
    body: "I think the server is now down..."
    author: "biquillo"
  - subject: "Re: I cannot read it!!"
    date: 2006-06-04
    body: "I'm also having issues.\n\nEvery once in a while, the server will actually respond and open a connection, but I never get a response to the HTTP request.  Weird."
    author: "Des"
  - subject: "Style?"
    date: 2006-06-04
    body: "\nWhat is the \"Oxygen style and window decoration for KDE\"?\n\nWill these be the defaults for KDE4?\n\nHow will they look like?\n\n"
    author: "mingus"
  - subject: "Re: Style?"
    date: 2006-06-04
    body: "You might want to follow the Oxygen link... that might answer your question."
    author: "Des"
  - subject: "Re: Style?"
    date: 2006-06-04
    body: "I have followed it, that's where I got the question from, but there's no answer."
    author: "mingus"
  - subject: "Re: Style?"
    date: 2006-06-04
    body: "The question \"how will it look like?\" isn't really easy to answer as stuff like this is work in progress (and probably will be so until KDE4 will be released).\nSo just be patient :)"
    author: "Easy to guess"
  - subject: "Re: Style?"
    date: 2006-06-04
    body: "Oxygen is an icon theme.  I haven't heard anyone say it will be a style or window decoration.\n\nYes, it will be the KDE4 default.\n\nThere are previews of what it looks like at the link you followed."
    author: "Des"
  - subject: "Re: Style?"
    date: 2006-06-04
    body: "Thomas is author of the popular and impressive Baghira style for QT/KDE. He is working on Oxygen style and window decoration for KDE.\n\n(http://www.oxygen-icons.org/?cat=2)"
    author: "mingus"
  - subject: "Re: Style?"
    date: 2006-06-05
    body: "Well ... unfortunately Baghira is a rip-off of an existing style (Apple's Aqua) and suffers from a multitude of graphical glitches (things not matching up, extraneous bevels, badly aliased edges - in many cases limitations of Qt3s painting engine, I'm sure, but not in all), so those aren't particularly impressive credentials."
    author: "Anonymous"
  - subject: "Style? forget style, some icons are worse off"
    date: 2006-06-05
    body: "If the credentials are not up to snuff, look as the icons themselves in the svn\n\nhttp://websvn.kde.org/trunk/playground/artwork/Oxygen/theme/svg/\n\nI think we all will be UNpleasantly surprised. The new Kmail icon is a black stick crossing tiny tiny envelop. The most distinguishing features of the new Kaddressbook icon are holes in the \"rolodex\" card (anyone still remember those?)\n\nI just ran a small \"lets see how distinguishable are you at 16 pixels test\" - most of the major application icons sucked at small sizes. Sometimes closed-doors development can make a major product launch a dud."
    author: "Daniel \"Suslik\" D."
  - subject: "Re: Style? forget style, some icons are worse off"
    date: 2006-06-05
    body: "> I just ran a small \"lets see how distinguishable \n> are you at 16 pixels test\" - most of the major\n\nThat test makes currently zero sense: \n\n- as the script which created the icons in SVN from vector graphics had some bugs.\n- as small icon sizes (16x16, 22x22) always need to be fine-tuned manually. That step is always the hardest and the most critical one and that final finishing hasn't happened yet.  \n- as the development of Oxygen is still very much work in progress and the icon theme pretty much incomplete.\n\nI know that the previous icon themes had the same problems at that stage of development (quickly scaled down icons were considered colorful hard to distinguish blobs back then). So just be patient and give it some time. As someone who was involved in creating the HiColor icon theme and with the initial development of CrystalSVG I actually think this looks very promising already :-) And don't forget that the creators of Oxygen (David, Ken and Nuno) have very much experience with creating icon themes even behind \"closed-doors\" already. So they know what they are doing.\n\n"
    author: "Torsten Rahn"
  - subject: "Re: Style? forget style, some icons are worse off"
    date: 2006-06-05
    body: "As someone who worked on an icon set before, you should probably already know how to approach the creation correctly. 1st you think of a concept that is potentially good and distinguishable at small sizes, then you artistically blow it up.\n\nThe way I see Kontact and Kmail icons created (the most commonly used apps after Konqueror) - \"Lets how how well I can draw a pen or an assorted stack of paper. We'll optimize whatever I come up with to small sizes as an afterthought.\"\n\nI admire pinheiro's ability to recreate real objects with vectors, but I think in some rare but important things (like the kmail icon) he put the horse before the carriage. Internally he even exports the Kmail icon as ../pics/new oxygen/svg/pen.png - talking about people's priorities and understanding of the purpose.\n\nAll artsy ppl have an affliction of doing things because they \"can\", not because they \"should\". I wish they would be more open to input from \"dim whit\" public, at least in important things like icons for Kmail.\n\nNot to come empty handed - over impose an @ sign over something like this\nhttp://www.handmadeinteractive.com/lufthansa/images/seal.jpg\nCrude but instantly detectable at small sizes and rather unique."
    author: "Daniel \"Suslik\" D."
  - subject: "Re: Style? forget style, some icons are worse off"
    date: 2006-06-05
    body: "> 1st you think of a concept that is potentially \n> good and distinguishable at small sizes, then \n> you artistically blow it up.\n\nIf you really really want to have something in the end that adheres to usability so much that you can essentially forget about the look then this might be a possible approach. Oxygen on the other hand is not an icontheme which is purely meant to focus on usability only though.\n\nCreating icons is a highly iterative process. If you start off while putting too many limits onto yourself the result will be quite limited (in terms of artwork) itself.\nIt's much more creative, innovative and challenging to start by creating something \"impossible\" just following the artistic vision. Once your vision has become reality it's time to strongly look at the \"rough edges\" in terms of usability, accessibility and implementation without loosing the focus for the original vision. At worst you have to rethink and redo pieces at that stage after having reached for the sky.\n\nThere's no question that earlier icon themes like HiColor, Windows 3.0, Mac OS 9 etc. were modelled sticking without compromises to the usability vision (and sticking to the workflow you suggested). However these days the compromise between usability and artwork has shifted a lot towards the artwork part (resulting in iconthemes that focus less on usability like Vista, Mac OS X, etc.). And people choose their desktops based on the first visual impression and tend to stick to it. So making it look really good is mandatory. Pushing the limits is mandatory, because we can.\n\n"
    author: "Torsten Rahn"
  - subject: "Re: Style? forget style, some icons are worse off"
    date: 2006-06-05
    body: "\"he put the horse before the carriage. Internally he even exports the Kmail icon as ../pics/new oxygen/svg/pen.png\"\n\nsory i dont folow you there.\nThe apps icons should funtion in small sizes thats true, but they should also be appeling in large sizes, done in svg, coerent in all szes, work has marketing material.\nThat what we try to do in Oxygen ofcourse we wont please every one, and ofcourse we still have lots of work to do, and lots of imput to get.\nEvery work is made of several stages we now think we can enter a new stage, hope you can help us to make the best icon set ever.\n\n"
    author: "pinheiro"
  - subject: "Re: Style? forget style, some icons are worse off"
    date: 2006-06-06
    body: "\"hope you can help us to make the best icon set ever.\"\n\nHow open is the team towards reopenning the discussion of what goes onto icons of main KDE apps? My primary interests: Kmail, Kaddressbook. \n\nAgain, my issue is not with the quality of drawing, it is with the quality of composition (chosen pieces, size-significance of pieces, intent / conveyance of each item)\n\nIf someone would like to REopen a discussion on a particular IMPORTANT icon. Is there a mailing list / forum where proposals may be posted and openly discussed?\n\nEvery time you say \"lets see you do better\" you should also qualify if \"doing better is still allowed for submission / consideration\" Otherwise, it looks like vailed paw swipes from under the bed \"no-caps\" Aaron likes to present (see bellow. :) )"
    author: "Daniel \"Suslik\" D."
  - subject: "Re: Style? forget style, some icons are worse off"
    date: 2006-06-06
    body: "Just feel free to post your suggestions on kde-artists@kde.org or visit #kde-artists on freenode or mail your opinion to nuno, ken or david directly. I guess they are open to any constructive ideas for improvement. And I know that if you come up with a good suggestion they won't be able to resist from even repainting the icon from scratch if necessary;-)"
    author: "Torsten Rahn"
  - subject: "Re: Style? forget style, some icons are worse off"
    date: 2006-06-05
    body: "I do crystal icons and I know it is a overtly designed set. Just keeping with the icons done already is a lot killing to infuse details. Finally at 16x16 most detailing go poof!\nNo No, bitmap handywork at 16x16 can't bring the details gone. I work on OOo set and just open the svgs in inkscape and you will know for sure.\nGood about Oxygen is that it looks better than crystal. But much handywork is needed for smaller sizes. May be somewhat more than the real icon designing."
    author: "Manik Chand"
  - subject: "Re: Style? forget style, some icons are worse off"
    date: 2006-06-05
    body: "> But much handywork is needed for smaller sizes. \n> May be somewhat more than the real icon designing.\n\nRight. I remember that I wanted to have a teddy bear for the HiColor iconset packages_toys icon. At that time it was mandatory not to use the alpha channel (because X wasn't able to use it) and a fully transparent background and therefore not having antialiasing available.\nI started to paint the 32x32 version first:\nhttp://websvn.kde.org/tags/KDE/2.2.2/kdebase/pics/hicolor/hi32-app-package_toys.png?rev=411963&view=markup\n\nOnce I was done with it somebody laughed at me: \"How do you want to accomplish that in 16x16 pixels?\" It took longer than creating the 32x32 version but without having created the 32x32 as a \"target vision\" first in the end I probably wouldn't have created an icon half that good let alone deeming it possible ;-)\n\nhttp://websvn.kde.org/tags/KDE/2.2.2/kdebase/pics/hicolor/hi16-app-package_toys.png?rev=411963&view=markup\n\n\n\n\n\n\n"
    author: "Torsten Rahn"
  - subject: "Re: Style? forget style, some icons are worse off"
    date: 2006-06-06
    body: "I would not fault Oxygen for what is a general problem.  It was decided that new icons must be SVG and must also include 16x16 and 22x22 PNGs.  The reason for the small icons being required is that simply rendering the SVG to those sizes tends to produce a fuzzy colored blob rather than an icon. :-)  The more detailed the SVG icon, the worse the problem.\n\nThe intent is that these small icons _must_ be hand optimized.  Unfortunately, what many have done is simply render the SVG to 16x16 and 22x22 PNG with either this script, InkScape, or The GIMP thereby totally defeating the purpose of this and simply wasting space.\n\nTo be clear: 16x16 and 22x22 icons must be hand optimized PNGs and many icons are improved in 32x32 (and possibly even 48x48) when hand optimized PNGs are made as well."
    author: "James Richard Tyrer"
  - subject: "Re: Style? forget style, some icons are worse off"
    date: 2006-06-05
    body: "\"Sometimes closed-doors development can make a major product launch a dud.\" \nThis is very true. And in this case doubly so. I say this because what the Oxygen project promises and what they are delivering are inconsistent. \nWhat ever happened to the standard answer that used to be given that the community  decides which is the default for KDE4. The answer is it never was! The Appeal team controls all of the decisions behind closed doors. The default will be Oxygen because they know best. Who gives a crap what anybody else says. This message will continue to be echoed all though out KDE now. Welcome to the new Kworld order. \nThe Oxygen stinks in here, where is the unparalleled consistency and unmatched beauty that we were promised? If Oxygen was an open project there would have been a lot of people fixing patching contributing improving. It would have been healthy for KDE. The Tango team does it right in this area. You guys did not just miss the boat you missed the whole dock! "
    author: "Holding nose"
  - subject: "Re: Style? forget style, some icons are worse off"
    date: 2006-06-05
    body: "> What ever happened to the standard answer that\n> used to be given that the community decides \n> which is the default for KDE4.\n\nWho says that KDE is a total democracy? It never was. \nActually no Open Source project even is. I guess you got certainly something wrong there.\nWhile the KDE team values the input of its users and certainly takes it serious KDE never was designed by community vote.\n\n\n> The Tango team does it right in this area. \n> You guys did not just miss the boat you missed the whole dock!\n\nBla! You are aware of the fact that Tango had been developed at Novell under NDAs for months before it was finally released to the public? \n\nSo there's no difference compared to Tango concerning that.\n\nHaving a closed intial period is not uncommon for OpenSource projects.\n\n\n"
    author: "Torsten Rahn"
  - subject: "Re: Style? forget style, some icons are worse off"
    date: 2006-06-06
    body: "Hmmm you are an Appeal member if I am not mistaken.\n\n>Who says that KDE is a total democracy? It never was.\n>Actually no Open Source project even is. I guess you got certainly something >wrong there.\"\n\n>\"While the KDE team values the input of its users and certainly takes it serious KDE never was >designed by community vote.\" \n\nBefore Oxygen was publicly named as the default there was some discussion on IRC about whether it would be the default. The answer always seemed to be along the lines of the defaults are not for us to decide that if the community excepts it as the default that would be great.(The humble artists approach.) I witnessed this type of response several times before it was named default that is what I was referring to. \nBefore Appeal was created decisions as to the defaults were discussed openly on a mailing list giving all interested people the ability to weigh in on the matter. Sure someone eventually made a final decision but at least people could be involved even in even a small way. That is no longer the case. \n\n>\"Bla! You are aware of the fact that Tango had been developed at Novell under NDAs for months >before it was finally released to the public?\n\n>So there's no difference compared to Tango concerning that.\n\n>Having a closed intial period is not uncommon for OpenSource projects.\"\n\nI was referring to the success of Tango's community involvement currently. Not it's beginnings. As I am questioning Oxygens current state. There is a huge difference in the current way things are developed between the two projects. IMO Tango has the right formula and it is wildly successful. They have a few top tier artists tightly managing a very active community of contributors. I believe that for the Oxygen team to not follow Tango's example at this point would cause them to miss not just the boat but the whole dock. IMO.  \n\n>Having a closed intial period is not uncommon for OpenSource projects.\"\n\nBla! Blah! Are you saying that Appeal and Oxygen are still in a closed initial period?"
    author: "Holding nose"
  - subject: "Re: Style? forget style, some icons are worse off"
    date: 2006-06-06
    body: "Like Tango we now planed from day one to open the theme to the cummunity help, this is the stage we are now entering.\nHope we can get your help has well.\nAbout the defoult thing, no one till this day said it would be kde4 defoult theme, i halyays belived that it has god chances of being couse we are working so hard at it and adresingin so many old questions wen making it.\nBut im kite sure that if we simply (do not deliver) there wont be any oxygen in kde.\nSuming it help us make the best theme ever the theme is now in svn donwload it see it make your improvments send them to us or svn you feedback is also apreciated."
    author: "pinheiro"
  - subject: "Re: Style? forget style, some icons are worse off"
    date: 2006-06-06
    body: "Wow that is news!\nWhere will the Oxygen team be accepting submissions? kde-artists mailing list or...? IRC #kde-artists or...? Will it be similar to Tango in this regards or is there another plan underway?"
    author: "Holding nose"
  - subject: "Re: Style? forget style, some icons are worse off"
    date: 2006-06-07
    body: "stay tuned we hope to update the site in the coming days on the subject, rmember its alwys esay to email me Ken or David we will surely read it and try to respond, we dont think we are the howners of the truth in fact we do beleve that 2 heads think beter than 1 we just had to frist design somthing that was coerent, for starters betwen all of us. "
    author: "pinheiro"
  - subject: "Re: Style? forget style, some icons are worse off"
    date: 2006-06-07
    body: "Like I said before this is breaking news. As nothing like this has ever been said about Oxygen before. It will be interesting to see what happens in this area. "
    author: "Not holding nose so tightly..."
  - subject: "Re: Style? forget style, some icons are worse off"
    date: 2006-06-06
    body: "> Hmmm you are an Appeal member if I am not mistaken.\n\nYes, and of course a member of the Illuminati. I suppose you are an anonymous coward trying to spill shit from the safe _closed_ dark onto the work of others. Really, I love your obvious believe in transparency & openness.\n\n> Sure someone eventually made a final decision\n\nYeah and that someone usually is the KDE artist team who maintains the respective current theme in KDE svn. And of course the KDE artist team always took feedback from the public into account for the development. That's after all why Oxygen exists: It tries to take the good aspects from Crystal and adds a lot of elements that were requested by the community (\"less cartoonish\", \"better colors\", \"less colorful\").\n\nJust as an exercise you might want to tell me how choosing Crystal (which initially was developed behind \"closed doors\" at Connectiva) as a default icon theme over HiColor was the result of a public community discussion voting on mailing lists ( when in fact it was _not_ ).\n\n> Not it's beginnings.\n\nBut Oxygen _is_ still in its beginnings. And with Oxygen being released to SVN the artist team has completed another milestone successfully. Still there is enough open work ahead that will probably take many months until Oxygen is at 1.0 stage.\n\nIf you are really interested in KDE's decision making process I'd urge you to go and read a book (while having a cup of good hot chocolate) which deals with the social aspect of open source development. KDE isn't very much different from other projects with regard to this. You'll find out that KDE's development as a social phenomen is much more complex than you'd like it to be. So complex actually that there are scientific papers which focus on that aspect within KDE. \n\n"
    author: "Torsten Rahn"
  - subject: "Re: Style? forget style, some icons are worse off"
    date: 2006-06-06
    body: ">Yes, and of course a member of the Illuminati. I suppose you are an anonymous >coward trying to spill shit from the safe _closed_ dark onto the work of others. \n>Really, I love your obvious believe in transparency & openness\n\nLOL! If I want to make statements and ask questions in an anonymous way that is my perogitive and my right. If it was not, the option would not be open to me here at the dot. This does not deminish my point or my questions. \nYou do not need to defend your position and I am surprised that you did. I am not spilling shit onto the work of others. I was merely pointing out that you are an Appleal member nothing more nothing less.\n\nIn all honesty Penhiro answered my questions and made available information that was never mentioned anywhere that I know of before. \n\n>Yeah and that someone usually is the KDE artist team who maintains the >respective current theme in KDE svn. And of course the KDE artist team always >took feedback from the public into account for the development. That's after >all why Oxygen exists: It tries to take the good aspects from Crystal and adds >a lot of elements that were requested by the community (\"less cartoonish\", >\"better colors\", \"less colorful\").\n\nAfter a quick search of the kde-artists mailing list on gmane.org (for the word decisions) I came across a post from the primary maintainer of KDE artwork in response to a curious potential contributor. It was stated that all of the decisions concerning default artwork is discussed on the mailing list. Discussing the defaults openly is not something I invented nor have I once until now written the word Vote. I was hard pressed to find any discussion about the Oxygen theme between the creators and others as you imply above. Other than an announcement about the launch of the website.\nHowever the results for \"less cartoonish\", resulted in \"No documents match your query\". A subsequent search for \"cartoonish\" returned 2 pages but alas nothing in relation to Oxygen. A search for \"better colors\" returned 6 pages but once again no results related to Oxygen. A search for \"less colorful\" resulted in one page with a post made by you in 2002-04-30 in regards to crystal. \n\nHere is my point before Appeal decisions were made openly on the mailing list now they are not. On one hand Penhiro says he never expected Oxygen to be the default on the other hand in the announcement made after Akademy on this very site it is stated very clearly that it was always intended for the default. The decision for Oxygen to be pushed as the default was decided through channels other than what were previously defined by the primary maintainer with his blessings. The reality is decisions have been made off of the list this is a break from the ways things have been done in the past. If decisions will continue to be made off of the list then there should be an announcement on the list that decisions will no longer be made on the list. At least let people know how the decisions will be made from now on rather that just making moves in SVN and then announcing it after the fact. \n\n>If you are really interested in KDE's decision making process I'd urge you to >go and read a book >(while having a cup of good hot chocolate) which deals with >the social aspect of open source >development.\n\nWith Appeal members holding various key positions (The KDE.eV board, HIG and CIG and Marketing work group) I think I will wait until a book has been written that discusses the effect that Appeal has had on the decision making process within KDE. It could even be positive but there is no way of really knowing is there.(I am a glass of straight good Scotch man myself I find it a bit more warming with a longer lasting side effect. :) Never really acquired a taste for chocolate myself.) But thanks for the tip. ;) "
    author: "Holding nose"
  - subject: "Re: Style? forget style, some icons are worse off"
    date: 2006-06-06
    body: "s/perogitive/prerogative"
    author: "Holding nose"
  - subject: "Re: Style? forget style, some icons are worse off"
    date: 2006-06-07
    body: "Very honestly if we dont suport enough information about what we do is becouse we simple dont have time for that, i'm totaly sure this is a bad thing but we are so busy making so much work we cant find time to deliver that output, i hope we can in the near future.\nThe problmem is that in the end brain commands me to make art not documentation, (be sure i know this is wrong) sory."
    author: "pinheiro"
  - subject: "Re: Style? forget style, some icons are worse off"
    date: 2006-06-05
    body: "> the community decides which is the default for KDE4.\n\nerm, that was -never- intimated or promised. what was said, and what we have been doing (so our actions match the words), is that we were going to working more with users and the community at large when designing and creating kde4. that has certainly been done, but no project of any sort of size and coherence does \"design by popular vote\". there has been an unprecedented amount of community and end-user feedback for kde4.\n\n> The Appeal team controls all of the decisions behind closed doors.\n\ncare to back that up with some facts? no, you can't because it isn't true.\n\n> Who gives a crap what anybody else says\n\ni get the feeling you have a particular event in mind. care to share what it is?\n\n> If Oxygen was an open project there would have been a lot of people fixing\n> patching contributing improving. It would have been healthy for KDE.\n\nerm ... you want to contribute to the icon set? break out your graphics app and rev up the pixels! somehow i really doubt that if you start contributing quality oxygen styled icons they won't end up svn ;)\n\nbut consider: there needs to be a unifying look and feel, a vision behind the visual concepts. that isn't something that \"a lot of people fixing patching contributing\" is good at producing. that part is largely done at this point now thanks to a small team of hard working individuals. \n\n> The Tango team does it right in this area.\n\nfirst off, the tango team worked for quite a while in complete secrecy (~1 year iirc?). oxygen was announced pretty much from day 0. as results that demonstrate the concepts have become available they have also been shared immediately. so if you want to talk openness, Tango isn't the winner here.\n\nas for quality of final product, i think the results speak for themselves. go look at the tango icons, look at the oxygen icons ... tell me which look better."
    author: "Aaron J. Seigo"
  - subject: "Re: Style? forget style, some icons are worse off"
    date: 2006-06-06
    body: "To answer most of your statements above I refer you to the last post I made to Mr. Rahn. \n\nI only have this to add.\n\n>as for quality of final product, i think the results speak for themselves. go >look at the tango icons, look at the oxygen icons ... tell me which look better.\n\nBased on an overall assessment of the current state Tango would harbor more votes than Oxygen. Oxygen has a few very stunning icons mixed with a bunch questionable ones at this point. But as Pinhero pointed out they will be working in a similar manner as Tango and including anybody interested in improving the current set in SVN. This could give the set the boost it needs. And I could see it overtaking Tango on overall look and feel eventually. And yes everyone is a critic. :)\n\n "
    author: "Holding nose"
  - subject: "Re: Style? forget style, some icons are worse off"
    date: 2006-06-07
    body: "tango's had longer to mature, yes. but (and be sure you're looking at the oxygen icons in svn, not the website) look at the style concepts and final artwork of what exists right now. it's rapidly becoming more consistent and the ideas of clear visual icon class separation, colour usage and composition is several steps ahead."
    author: "Aaron J. Seigo"
  - subject: "Re: Style? forget style, some icons are worse off"
    date: 2006-06-05
    body: "16px icons? are you serious? i suppose you use an 8 bit display as well. and a 16 bit processor. with 4MB of ram. and no hard disk.\n\nit's not the 1980s anymore. it's time we moved on to new breathtaking heights like 32px or even 64px icons, particularly when it comes to application icons and filemanager views.\n\ni'll take amazing 32px icons with crap (or even no 16px) icons over good 16px icons (which are never good, btw; there's not enough information in them to be anything better than \"ok\" imho) with good 32px icons."
    author: "Aaron J. Seigo"
  - subject: "Re: Style? forget style, some icons are worse off"
    date: 2006-06-06
    body: "I'm sorry Aaron, but most people use 16px icons in their system tray, I'm also pretty sure you use it. Mostly because I don't know that it's possible to change the size of the system tray icons. \n\n-Pascal"
    author: "pascal"
  - subject: "Re: Style? forget style, some icons are worse off"
    date: 2006-06-06
    body: "Nah, system try icons are 22 pixels.\n\nThe only time you use 16 pixel icons in KDE are when you specifically set your toolbars to that size, or when you have launcher icons on a \"tiny\" sized panel (in that case, the 22 pixel system tray icons are just cut off)."
    author: "Dolio"
  - subject: "Re: Style? forget style, some icons are worse off"
    date: 2006-06-06
    body: "as Dolio said, systray icons in kde are 22px and have been for some time. fact checking is good ;)\n\nbut let's pretend they -were- 16px icons down there in the systray. that would be a really poor idea (16px is -tiny-, rather hard to hit with the mouse and very low amount of information) and in kde4 we'd -fix- that by making them larger. in kde4 we can change what sucks and make good decisions for today as well as tomorrow. we're not beholden to how things are (though it admitedly does require a bit of imagination to get past that)\n\n"
    author: "Aaron J. Seigo"
  - subject: "Re: Style? forget style, some icons are worse off"
    date: 2006-06-06
    body: "\"it's not the 1980s anymore. it's time we moved on to new breathtaking heights like 32px or even 64px icons\" \n\nThis is utter  B.S. aaron and you know it. Title bar and task button icons are 16 pixels. For majority of people, so that the system tray would not look like a task-bar, icons there are also 16 pixels."
    author: "Daniel \"Suslik\" D."
  - subject: "Re: Style? forget style, some icons are worse off"
    date: 2006-06-07
    body: "the task bar is only 16 pixels because nobody has bothered to fix the taskbar. did you know that there is already support in kwin for larger icons? or where do you think the 32px icons in the alt-tab window switcher come from?\n\nas for the \"taskbar not looking like systray\" that's also rubbish =)\n\nexpect changes here in kde4. 16px icons are usability hell."
    author: "Aaron J. Seigo"
  - subject: "Re: Style? forget style, some icons are worse off"
    date: 2006-06-07
    body: "Actually, the usability of 16x16 icons depends on the format of your screen and its size.  If you have 1024x768 or less, they are usable unless you have a *really* small screen.  They probably *are* useless if you have a screen resolution of much more than 100 DPI.\n\nPerhaps it would be possible to have the TaskBar icon size automatically set based on the size and format of the screen in the X configuration file.\n\nAlso, IIRC the system tray icons are 24x24 not 22x22.  And, you can't just change that because the larger icons probably don't exist.\n\n"
    author: "James Richard Tyrer"
  - subject: "Re: Style?"
    date: 2006-06-05
    body: "However Baghira is vastly popular. And concerning your comments: I don't think that Thomas is meant to be responsible for the artwork part but for creating the actual code after specifications made by the artists. And in that department he has obviously been very successful already.   "
    author: "Torsten Rahn"
  - subject: "Re: Style?"
    date: 2006-06-09
    body: "I like it, but please don't use brown for the folders...i find it ugly, i really like the blue one :)"
    author: "Giovanni"
  - subject: "php broken ? "
    date: 2006-06-05
    body: "Warning: main(http://commit-digest.org/index.inc): failed to open stream: HTTP request failed! in /home/kde-digest/public_html/issues/2006-06-04/index.php on line 1\n\nFatal error: main(): Failed opening required 'http://commit-digest.org/index.inc' (include_path='.:/usr/share/php4:/usr/share/php') in /home/kde-digest/public_html/issues/2006-06-04/index.php on line 1\n\n:("
    author: "anon"
  - subject: "DCOP gone, what to do?"
    date: 2006-06-05
    body: "I use DCOP quite often, I'm interested how DBUS can be used, what is for example the DBUS-variant of;\ndcop kontact kontact-mainwindow#1 hide\n\nWhat is the DBUS-equivalent of kdcop? I think kdcop is extremely nice to browse through the different options.\n\nThanks!"
    author: "LB"
  - subject: "Re: DCOP gone, what to do?"
    date: 2006-06-05
    body: "DCOP has already been completely replaced by DBus in KDELibs and by the time KDE 4 is released 'kdbus' should be working replacement for kdcop.  Considering that D-Bus's design was based on/inspired by dcop it shouldn't be any problem in KDE 4 to do things like that (hopefully more functionality will be exposed in the KDE 4 version of programs).\n\n"
    author: "Corbin"
  - subject: "Re: DCOP gone, what to do?"
    date: 2006-06-05
    body: "I asked Thiago, trolltech employee responsible for the Qt d-bus bindings, about how extensively d-bus was going to be supported.  He said that each Qt object has a property and if that propery is enabled then that slot/signal will be accessible via d-bus.  (I've forgotten the name of the property at the moment though.)   The compiler will do the work so adding d-bus support at that level will be trivial."
    author: "Anon Cow."
  - subject: "Re: DCOP gone, what to do?"
    date: 2006-06-05
    body: "\"What is the DBUS-equivalent of kdcop?\"\n\nMy initial guess would be kdbus, and some search reveals:\nhttp://www.kde-apps.org/content/show.php?content=34692\n\nWhy not give it a try? If you report your impression as a kdcop user to the developers, it may help them make it even better than kdcop. "
    author: "Morty"
  - subject: "Re: DCOP gone, what to do?"
    date: 2006-06-05
    body: "As far as I understood, the \"external\" dcop interface you are using manipulate\nkontact will remain unchanged, the same is true for the dcop tool.\n\nThe changes are that the dcop tool will communicate with the applications via\nthe dbus daemon, and no longer via the dcopserver.\n"
    author: "ac"
  - subject: "Oxygen icon"
    date: 2006-06-05
    body: "So if the Oxygen team themeselves say:\n\n\"When possible action icons should be compound of a single element. More than two elements will make the icon to look too crowded and not usable at all. \"\n\nThen why does the configuration-icon have two elements? A screwdriver and a wrench, why not just the wrench???\n\n"
    author: "KDE User"
  - subject: "Re: Oxygen icon"
    date: 2006-06-05
    body: "Well, it says \"When possible\"..."
    author: "Luca Beltrame"
  - subject: "Re: Oxygen icon"
    date: 2006-06-05
    body: "Yes, and jsut the wrench seems just as intuitive. And definitely less crowded."
    author: "KDE User"
  - subject: "Re: Oxygen icon"
    date: 2006-06-05
    body: "Two elements are not more than two elements..."
    author: "mingus"
  - subject: "Re: Oxygen icon"
    date: 2006-06-05
    body: "LOL. Logic is not for everyone ;-)"
    author: "Martin"
  - subject: "kviewshell and okular"
    date: 2006-06-05
    body: "what is the difference between kviewshell and okular and why do we need two applications?"
    author: "Kurt"
  - subject: "Re: kviewshell and okular"
    date: 2006-06-05
    body: "You can read the responses to a similar question on last weeks Commit-Digest.\n\nhttp://dot.kde.org/1148838047/1148852615/\n\n"
    author: "Wilfried Huss"
  - subject: "Re: kviewshell and okular"
    date: 2006-06-05
    body: "KViewShell is much older than Okular and is based on KDVI, while Okular was started as a SoC project last year and was based on KPDF.  From what I can tell the pros of each of the apps seem to be that KViewShell is much more mature, and Okular has a much cooler name (while still somewhat saying what it does)."
    author: "Corbin"
  - subject: "Re: kviewshell and okular"
    date: 2006-06-05
    body: "Cool, just rename KViewShell to Okular and have this issue solved :-)\n"
    author: "Michal"
  - subject: "Re: kviewshell and okular"
    date: 2006-06-05
    body: "Thanks for your answers. Seems like kviewshell needs a new name, a nice homepage, regular releases and some promotion. ;-) Should not be that much of a problem, compared to the programming work that's already been done. "
    author: "Kurt"
  - subject: "Re: kviewshell and okular"
    date: 2006-06-07
    body: "Absolutely, but also persuade Albert to combine efforts.\n"
    author: "Michal"
  - subject: "ktorrent \"expands DCOP interface\" ?"
    date: 2006-06-08
    body: "If KDE4 removes DCOP in favor of DBUS, what's up with Joris Guisson commit of  /trunk/extragear/network/ktorrent which \"Added Adam Forsyth's patch which expands the DCOP interface significantly\" ?\n\nJust armchair curiosity.  The Flake and DBUS and QT4-ification and continuous Amarok improvement all sounds great!"
    author: "S Page"
  - subject: "Re: ktorrent \"expands DCOP interface\" ?"
    date: 2006-06-08
    body: "well, ktorrent is not part of any kde package.\n\nAnd since kde4 is not due until somewhere 2007, ktorrent can use the benefits of DCOP for more then a year, before having to port it to KDE4 and DBUS"
    author: "Rinse"
  - subject: "Re: ktorrent \"expands DCOP interface\" ?"
    date: 2006-06-11
    body: "Thanks.  I thought a check-in in /trunk implied KDE4, but I guess it's only certain packages."
    author: "S Page"
  - subject: "Re: ktorrent \"expands DCOP interface\" ?"
    date: 2006-06-11
    body: "Correct, besides the default kde modules there are the modules extragear-*, playground-*, kdereview, kdenonbeta, koffice kdekiosk and others.\n\nSoftware in these modules have their own relase cycle and decide for them self on which kde-version they will depend. \n\nSo for ktorrent, the version in branch/stable is the current release, and the version in /trunk is the development for the next release of the application (which will probably get released in a few days/weeks; after that the code moves to branches/stable and the next version will be developed in trunk)"
    author: "Rinse"
  - subject: "The Os KDE4 has to compete with is already beta"
    date: 2006-06-08
    body: "http://fileforum.betanews.com/detail/Microsoft_Windows_Vista_English_32bit/1149728719/1"
    author: "Jim Der"
  - subject: "Re: The Os KDE4 has to compete with is already beta"
    date: 2006-06-08
    body: "Why does kde4 have to compete with Vista?\n\nBesides, Vista is more then 3 years over due, so it probably should have competed with kde3, not kde4\n"
    author: "Rinse"
  - subject: "Re: The Os KDE4 has to compete with is already beta"
    date: 2006-06-09
    body: "Currently KDE 4's estimated release date is pretty close to Vista's current 'release date' (assuming its not moved back for the 100th time)."
    author: "Corbin"
  - subject: "The 5 Horse Training"
    date: 2009-05-10
    body: "The 5 <a href=\"http://www.horseclicks.com/\">Horse Training</a> Secrets..it's good corner...By practicing flexing on the ground. At-Liberty Round-Pen Training. Lead your horse properly."
    author: "michalraise"
---
In <a href="http://commit-digest.org/issues/2006-06-04/">this week's KDE Commit-Digest</a>: <a href="http://kopete.org/">Kopete</a> 0.12 is released after 10 months of development. Usability fixes in <a href="http://www.rsibreak.org/">RSIBreak</a> and experiments in <a href="http://amarok.kde.org/">amaroK</a>. Common <a href="http://koffice.org/">KOffice</a> color management initiative - "pigment" - started. User interface optimisations in <a href="http://web.mornfall.net/adept.html">Adept</a> package manager. KDE 4 changes: DCOP is finally removed from trunk/. The KDE 4 icon theme, <a href="http://oxygen-icons.org/">Oxygen</a>, is imported into KDE SVN.
<!--break-->
