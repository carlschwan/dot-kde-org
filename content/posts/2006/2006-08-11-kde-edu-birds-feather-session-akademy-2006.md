---
title: "KDE-Edu Birds of a Feather Session at aKademy 2006"
date:    2006-08-11
authors:
  - "ptoscano"
slug:    kde-edu-birds-feather-session-akademy-2006
comments:
  - subject: "pupils"
    date: 2006-08-12
    body: "What do pupils think about skolelinux and KDE? Do they like it?"
    author: "knut"
  - subject: "Re: pupils"
    date: 2006-08-12
    body: "Pupil don't know about Skole ;-) Skole is just like any other Linuxsystem for them. The have a KDE with some apps.\nSkole is just extremly cool for admins in school because of LTSP."
    author: "Carsten Niehaus"
  - subject: "typo in month"
    date: 2006-08-14
    body: "Semptember.."
    author: "jose"
---
This year in Dublin will host the <a href="http://conference2006.kde.org/">annual meeting of the KDE community</a>, and it will be a great occasion for developers to meet, code, hold bug-fixing sessions, discussions and much more.  During the week of the conference, some KDE-Edu developers will meet to discuss themes including their future strategies of the module for the upcoming KDE 4, current applications, ideas for new ones and collaboration with other education-related projects, like <a href="http://www.skolelinux.org/">SkoleLinux</a>. A SkoleLinux representive will give a <a href="http://conference2006.kde.org/conference/talks/28.php">talk</a> during the conference at aKademy.




<!--break-->
<p>If you are interested and able to attend, we will be waiting for you on Semptember 28th, at 10am (Dublin local time). If you can not, you can always let us know about your ideas by writing them down in the <a href="http://wiki.kde.org/tiki-index.php?page=KDE-Edu+%40+aKademy+2006">BoF wiki page</a>. We will be glad to receive and discuss any thoughts.  Of course you can always join KDE-Edu by subscribing to the <a href="https://mail.kde.org/mailman/listinfo/kde-edu">KDE-Edu mailing list</a> or contacting us on our IRC channel <a href="irc://irc.freenode.net/#kde-edu">irc.freenode.net, #kde-edu</a>.</p>

