---
title: "Phil Thompson Talks About PyQt"
date:    2006-08-09
authors:
  - "jriddell"
slug:    phil-thompson-talks-about-pyqt
comments:
  - subject: "<Blush>"
    date: 2006-08-09
    body: "Phil is, of course, being overly modest. PyKDE is much larger than PyQt, but basically much simpler to implement. That, of course, is because of a) the excellent tool Phil has developed and continued to enhance (sip) and b) Phil's willingness to provide assistance. So nearly all of the credit for PyKDE goes to Phil (and the criticisms to me, unfortunately).\n\nSimon Edwards also deserves the credit for getting PyKDE on the KDE CVS and into the kdebindings package.\n\nAnd trust me, I'm not being modest (although Phil is right about regularly breaking sip - but all for a good cause)."
    author: "Jim Bublitz"
  - subject: "Re: <Blush>"
    date: 2006-08-10
    body: "And we shouldn't forget Roberto Alsina and Giovanni Bajo, who are always ready to give advice and teach people about PyQt. There are also many other helpful and friendly people who have made positive contributions to the community around PyQt and PyKDE."
    author: "David Boddie"
  - subject: "My favourite application written in PyQt: TreeLine"
    date: 2006-08-09
    body: "Thanks to Phil for the great work!\n\nI'd like to point everybody who is interested in how PyQt applications can look like to TreeLine (http://www.bellz.org/treeline/), which is my favourite PyQt app.\n\nTreeLine is still using PyQt 3 (the PyQt 4 version is in development), so the code is not always using the latest and greatest Qt can offer, but other than that it is a good start for anyone who wants to get into PyQt programming. At least it was for me.\n\nApart from being an interesting codebase to learn from it's also a great application to use."
    author: "Joachim Werner"
  - subject: "Re: My favourite application written in PyQt: Tree"
    date: 2006-08-09
    body: "Thanks! That's what i'm looking for. TreeLine is a very nice Tool on Linux and Windows too."
    author: "It's me"
  - subject: "Thanks"
    date: 2006-08-09
    body: "Thanks for the article and the great bindings. Very nice stuff to work with :)"
    author: "Sebastian Sauer"
  - subject: "Phil is the man"
    date: 2006-08-09
    body: "The PyQT / PyKDE bindings are great and IMHO they should be more publicised by the core KDE organisation."
    author: "Giacomo"
  - subject: "Re: Phil is the man"
    date: 2006-08-10
    body: "I agree. What would really be sweet would be if releases of KDE guaranteed the presence of the appropriate PyQt and PyKDE (and hence a run-time dependancy on Python). \nThe use of PyQt just has so many advantages over your regular C++ and Qt: No hours wasted compiling; no worrying about getting a package created for all architectures; no memory management hassles; interactive testing. You can concentrate on the functionality of your app and describe it in Python eloquently.\nAlmost all distros ship Python anyways, so it wouldn't be a major problem. It would encourage people who would like to write applications for KDE (but want to cut out the C++ cruft) to do so, and, if the applications were worthy, they could be included in a relevant module in the KDE SVN repository."
    author: "Cerulean"
  - subject: "Re: Phil is the man"
    date: 2006-08-16
    body: "Well, kdebindings3 contains a full set of PyQt, PyKDE, and even QScintilla already. And at least on SUSE Linux the dependency of HP's printing tools on PyQt make sure that the kdebindings3-python RPM is installed by default. ;-)"
    author: "Joachim Werner"
  - subject: "KParts"
    date: 2006-08-09
    body: "Have anyone ever used kparts in a pykde-app? I tried it once some months ago, but failed with some strange failures. Also, the pykde-ml couldn't help me. So i'm courious whether pykde is really complete?"
    author: "Nabe"
  - subject: "Re: KParts"
    date: 2006-08-09
    body: "I don't remember the exact problem you had with KParts, and these things are difficult to debug when you simply get some KParts that work, but others behave in a way that you could only describe as \"incomplete\".\n\nWhile writing the above sentence, I thought I'd try to recreate the problem so I could describe it better, but it seems that many of the standard KParts work just fine; for example:\n\np = createReadOnlyPart(\"libkpdfpart\")\np.widget().show()\np.openURL(KURL(\"http://www.trolltech.com/trolltech/products/qt/learnmore/whitepapers/qt41whitepaper-a4\"))\n\nThis shows the Qt 4 whitepaper in a KPDF KPart.\n\nSome of the other KParts aren't as well behaved, and I'd be interested to know why."
    author: "David Boddie"
  - subject: "Re: KParts"
    date: 2006-08-09
    body: "The problem was: i can load kparts, but couldn't use them. They dosn't load the url (kghostviewerpart for example) or crashed by the first chance (koffice-parts). Also, the kontext-menu was missing and the parts doesn't appear localized.\n\nI tried most of my installed parts, and kpdf's and kate's part was the unproblemeticst parts so far. At least, they load the files ;)"
    author: "Nabe"
  - subject: "Re: KParts"
    date: 2006-08-10
    body: "That should bring us to the point that we're halfway through 2006, and no matter how great the C++ api of KDE and Qt is, we're entering an era where most non-systems app development is going to be done in a high-level language.\n\nIt's pretty crucial that KDE get good support for something like Python, Ruby, or Mono in order to compete.  "
    author: "ac"
  - subject: "Re: KParts"
    date: 2006-08-10
    body: "What disturbs me is that Python seems to be getting left behind as some KDE developers focus on ruby.  This doesn't make sense to me, as python is a more popular, better supported language.  Don't get me wrong; I'm all for Kross and generalised solutions that work no matter what language you choose, and I'm interested in Ruby too, so I've no problem with ruby being supported.  But, python is hugely important too."
    author: "Lee"
  - subject: "Links to Resources"
    date: 2006-08-10
    body: "I couldn't see any links in the article, but for PyQt (for both Qt 3 and Qt 4) and PyKDE, start here:\n\nhttp://www.riverbankcomputing.co.uk/\n\nIt would be great to see KDE recognise the position that languages other than C++ now occupy in the programming world in general. The GNOME people may be openly agonizing about runtimes and environments, but their general attitude is arguably more progressive."
    author: "The Badger"
  - subject: "Thanks for the great work"
    date: 2006-08-11
    body: "PyQt and PyKDE both rock, pleasure of python, easiness of Qt."
    author: "cartman"
  - subject: "Thank you!"
    date: 2006-08-15
    body: "I just wanted to say that two years ago, I needed to write a small graphical application to help my wife making her short animation film (hop, a small advertisement : http://svdboom.free.fr/en/animations/novecento.htm). I had no knowledge of any programming language except Fortran and some shell and HTML. I got into Python because it was said to be easy and since I use exclusively KDE as desktop, I checked PyQT. In one month, I managed to learn the principles of object programming (Python way, so it's easier), most of the Python language (thanks Boudewijn) and the way to make a QT interface in Python using PyQT. The GUI was perfectly responsive and fast, quite a bit of shock to me who was expecting something slow.\nI have been extremely impressed by how easy it had been to get into all this in such a short amount of time and I would like to thank here all the people who work on these projects for their efforts. It would have been quite a lot more difficult for me without these tools.\nThanks again!"
    author: "Richard Van Den Boom"
  - subject: "PyQt is great, thanks so much"
    date: 2006-08-18
    body: "Hi,\n\nPhil and eveybody else working on and contributing to PyQt: Thanks for these great bindings.\n\nPyQt makes writing small helper tool \"dead easy\", I've put a GUI onto xpath evaluation from 3 different python xml modules within 2 weeks, including a tree view of the xml and highlighting of result nodes.\n\nThe community is very friendly and doesn't get \"pissed\" when newbies ask real newbie questions, or make \"stupid\" mistakes in their code. Phil isn't as unfriendly as he thinks he is (at least I didn't get that impression from the list)\n\nAndreas"
    author: "Andreas Pakulat"
  - subject: "Ports of the standard Qt examples"
    date: 2006-09-12
    body: "Phil mentions ports of the standard Qt examples to PyQt.\nAre these available for download anywhere (I've not had much luck googling for them).\n"
    author: "Alan"
  - subject: "Re: Ports of the standard Qt examples"
    date: 2006-09-12
    body: "Take a look in the PyQt source package for your platform:\n\nhttp://www.riverbankcomputing.co.uk/pyqt/download.php"
    author: "David Boddie"
---
High level languages are increasingly being used in preference to C and C++ in new desktop software.  One of these languages best supported in KDE and Qt is Python.  To find out about the history and current state of <a href=" http://www.riverbankcomputing.co.uk/">PyQt</a>, KDE Dot News talked to Phil Thompson, author and maintainer of the bindings.




<!--break-->
<div style="border: thin solid grey; margin: 1ex; padding: 1ex; float: right; width: 250px">
<a href="http://static.kdenews.org/jr/pyqt.png"><img border="0" src="http://static.kdenews.org/jr/pyqt-wee.png" width="250" height="234" /></a><br />
The HP Linux Printing system uses PyQt for its tools
</div>
 
<p><strong>Please introduce yourself and your role in Free Software</strong></p>
 
<p>Through my company, Riverbank Computing, I develop and maintain a set of related packages. These are SIP, PyQt and QScintilla.</p>
 
<p>SIP is a Python bindings generator for C and C++ libraries. It started out as a small SWIG (hence the name) and was first released in 1998. At the time I felt that SWIG, as a more general purpose tool, wasn't great at producing Python bindings for C++. So SIP was intended to do a more specialised job much better.</p>
 
<p>PyQt are the Python bindings for Qt. PyQt v3 supports Qt v1 to v3. PyQt v4 supports Qt v4. The first release was also in 1998, although named PyKDE since originally the two were not split. PyQt is written using SIP. PyQt follows Trolltech's licensing model in that there are GPL and commercial versions.</p>
 
<p>QScintilla is a port to Qt of the Scintilla code editor widget. It supports Qt v3 and v4 and (of course) includes a set of Python bindings. I initially did the port so that the Eric IDE (written by Detlev Offenbach) would have a decent programmer's editor. Again, it is available under the GPL and a commercial license.</p>
 
<p><strong>What is PyQt and why should I use it?</strong></p>
 
<p>PyQt gives the Python programmer access to the full Qt API. You can do almost all the things you can do from Python that you can do from C++.</p>
 
<p>As the API is the same from C++ as from Python then "why should I use it" comes down to the choice of programming language and isn't specific to PyQt. For me, the advantage of Python over C++, as an application development language, is simply programmer productivity. You can see this with the ports to PyQt of the standard Qt examples. They have the same function and use the same API but the Python versions have 50-60% of the lines of code, and are much easier to read.</p>
 
<p>Related to productivity is how easy Python is to learn, but still has enough power to satisfy more experienced programmers. Trolltech are discovering this themselves as PyQt allows them to sell Qt into highly technical organisations where the users are industry specialists (chemists, aeronautical engineers) rather than experienced C++ programmers.</p>
 
<p>And of course PyQt is mature, stable, and has a large user base. The two most common pieces of feedback I get from users is "it just works" and "it's fun".</p>
 
<p><strong>Why did you start the PyQt bindings?</strong></p>
 
<p>The usual reason - I had an itch to scratch. I'd been using Tcl and Tk for a few years for developing personal productivity tools and got frustrated with how visually ugly Tk applications were becoming (because everything else was getting prettier). I'd started using KDE v1 and switched to Python because there were some initial KDE bindings built using SWIG. I soon realised that I could do better than that and started again.</p>
 
<p>The v0.1 was released on November 1st 1998 and used Qt v1.41 and KDE v1.0. Releases have been made about every 3 months ever since.</p>
 
<p><strong>How are the bindings implemented?</strong></p>
 
<p>SIP takes a set of specification (.sip) files describing the API and generates the required C++ code. This is then compiled to produce the Python extension modules. A .sip file is basically the class header file with some things removed (because SIP doesn't include a full C++ parser) and some things added (because C++ doesn't always provide enough information about how the API works).</p>
 
<p>One of the keys to PyQt's success is the attention to detail in the .sip files. I do not believe you can automate the generation of bindings and end up with something that is industrial strength. You can use automation to do 95% of the work, but you must still go through the API adding extra information needed by a particular target language. With Python, for example, it's important that a Python object knows whether or not it should call the corresponding C++ instance's destructor when it is itself garbage collected. Expecting the programmer to manage this themselves is a recipe for core dumps.</p>
 
<p>In PyQt v3 the .sip files are hand-rolled - they have evolved incrementally from the originals created for Qt v1.41. SIP contains a versioning system and so the v3 .sip files contains a complete history of the Qt API from v1.41 to v3.3.6. Although I don't test it, PyQt v3 should still build against Qt v1.</p>
 
<p>For PyQt v4 I use an internal tool (written using PyQt of course) called metasip. This is sort of an IDE for SIP. It uses GCC-XML to parse the latest header files and saves the relevant data, as XML, in a metasip project. metasip then does the equivalent of a diff against the previous version of the API and flags up any changes that need to be looked at. Those changes are then made through the GUI and ticked off the TODO list. Generating the .sip files is just a button click. In my subversion repository, PyQt v4 is basically just a 20M XML file. Updating PyQt v4 for a minor release of Qt v4 is about half an hours work.</p>
 
<p>In terms of how the generated code works then I don't think it's very different from how any other bindings generator works. Python has a very good C API for writing extension modules - it's one of the reasons why so many 3rd party tools have Python bindings. For every C++ class, the SIP generated code creates a corresponding Python class implemented in C.</p>
 
<p>The Python class implements methods corresponding to the C++ equivalents. As Python doesn't support function overloading the correct signature is determined at run time by checking the types of the arguments passed in.</p>
 
<p>If a C++ class contains virtual functions then SIP generates a C++ sub-class with code that re-implements those virtuals and checks if there is a corresponding Python re-implementation. If so it is called, otherwise the base implementation is called. The result of the check is cached so it's not a significant overhead. Access to protected functions is also implemented using this generated sub-class.</p>
 
<p><strong>How does this compare to bindings implemented with SMOKE?</strong></p>
 
<p>I don't know. I've never used SMOKE or used any bindings based on it.</p>
 
<p><strong>How complete are the PyQt 3 bindings?</strong></p>
 
<p>They have been complete for a number of years. New releases of PyQt v3 tend to contain small updates driven by new releases of Qt and infrastructure changes driven by new releases of SIP.</p>
 
<p>I concluded very early on that I wasn't in a position to decide what parts of the Qt API should be exposed to the Python programmer, so I exposed all of it.</p>
 
<p><strong>What is the state of bindings for Qt 4?</strong></p>
 
<p>Complete and stable. PyQt v4.0 was released June 10th after 6 months of development snapshots. PyQt v4.0.1 was released July 15th and is just a minor maintenance release - not a single bug has been reported in the bindings themselves.</p>
 
<p><strong>What is your opinion of the PyKDE bindings?</strong></p>
 
<p>Speaking as the original author, I think they are wonderful. Jim Bublitz, the current maintainer, has a much more difficult job than I do with PyQt. KDE is much bigger than Qt, the API is less consistent, distros seem more inclined to mess around with KDE than with Qt, and he has to deal with me improving SIP for me while breaking it for him. Jim is also a much nicer person than I am.</p>
 
<p>I suspect that he could do with more help than he currently receives when it comes to PyKDE v4. Perhaps those distros that are making more use of PyKDE could step up to the plate (cough, cough).</p>
 
<p><strong>What contact have you had with Trolltech about your work?</strong></p>
 
<p>We have had an ongoing, informal relationship for 5 or 6 years. They were kind enough to invite me over to Oslo a few years ago. It's been interesting to observe their development. Originally they were a very technically focused (they still are of course), but without a strong sales/marketing effort. As a company they didn't "get" PyQt because they didn't understand why anybody would want to use anything other than C++ - I suspect that the majority of their programmers still feel that way.</p>
 
<p>These days however the sales/marketing influence is much greater. They know that people want to use other languages because that's what their customers are telling them. They know that there are large corporates who are buying Qt only because they want to use PyQt.</p>
 
<p>They are also putting effort into developing the Qt ecosystem: partnership programs with companies like Riverbank that develop complementary technologies.</p>
 
<p><strong>How is this work funded?</strong></p>
 
<p>PyQt makes a profit - it is self funding. It's not just sales of licenses, it is also the spin-off work that sometimes arises from a sale. For example I'm spending the rest of the year working with a customer on making greater use of PyQt within their organisation.</p>
 
<p><strong>How many commercial users do you have?</strong></p>
 
<p>Over 200 at the moment.</p>
 
<p><strong>Can you name some exciting specific users of PyQt?</strong></p>
 
<p>One of the disadvantages of being a one-man-band selling software over the web is that you don't get to know your customers very well. One thing I'm pleased about is that I'm a great film fan and Disney, Dreamworks, Pixar, Industrial Light and Magic, and Sony Pictures are all commercial PyQt users.</p>
 
<p><strong>How many people have worked to create PyQt?</strong></p>
 
<p>I'm the only person who works on SIP and the core bindings - apart from patches from others from time to time. For PyQt v4 Torsten Marek wrote pyuic4. Ulli Berning is a huge help in making sure PyQt builds and runs on HP-UX, AIX and Solaris.</p>
 
<p>Of course a successful project is about much more than people who write code, it's about the people who contribute to the community: Detlev and Jim I've already mentioned, Boudewijn for his original book, the group who ported the PyQt v4 examples, GerardV for PyQwt, DavidB and AndreasP for their quality contributions to the mailing list.</p>
 
<p><strong>What else does Riverbank Computing offer?</strong></p>
 
<p>The other area of expertise is embedded Linux systems - porting, device drivers etc. My first job after graduating was as a hardware engineer and I've always liked projects where you can kick the product.</p>
 
<p><strong>Would you like to see more programmers use higher level languages?</strong></p>
 
<p>Yes - and I think it is inevitable. In my youth there were debates about whether you should do your CP/M programming in Z80 assembly language or C. C programs took up too many resources (so I was told), but all my C programs had more functionality, fewer bugs and were quicker to write than Z80 programs. I see any debate between the use of C++ and languages like Python (and Ruby etc.) as being exactly the same. At the moment C++ is fine for writing frameworks, but I would prefer not to use if for writing applications.</p>
 
<p><strong>Are you going to aKademy this year?</strong></p>
 
<p>No, too busy I'm afraid.</p>
 


