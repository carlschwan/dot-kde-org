---
title: "KDE Commit-Digest for 16th April 2006"
date:    2006-04-16
authors:
  - "dallen"
slug:    kde-commit-digest-16th-april-2006
comments:
  - subject: "Layout"
    date: 2006-04-16
    body: "The typography is rather unpleasant to my eyes, I have to say. Would it be possible to use KDE.org stylesheets for the digest? That would have the additional advantage of design consistency."
    author: "Eike Hein"
  - subject: "Re: Layout"
    date: 2006-04-16
    body: "Have you got a screenshot of how it looks to you? (I am aware that the digest looks different on different peoples' computers, and may not look how I intended it to...)\n\nThanks,\nDanny"
    author: "Danny Allen"
  - subject: "Re: Layout"
    date: 2006-04-16
    body: "i'v got one"
    author: "AC"
  - subject: "Re: Layout"
    date: 2006-04-16
    body: "I am having the same bug."
    author: "Mark Hannessen"
  - subject: "Re: Layout"
    date: 2006-04-16
    body: "Here's a screenshot."
    author: "Eike Hein"
  - subject: "Re: Layout"
    date: 2006-04-16
    body: "I think your problem is related to font size. I notices when you increase the size with some fonts they become bold."
    author: "Narishma"
  - subject: "Re: Layout"
    date: 2006-04-16
    body: "Probably screwed up X DPI."
    author: "Lee"
  - subject: "Re: Layout"
    date: 2006-04-17
    body: "The same bug with mine Konqueror. Firefox displays it properly.\nIt's not DPI, 'case i'm using recommended 96.\nI guess it's better to find the reason rather than saying \"it's your problem\".\n"
    author: "Anton"
  - subject: "Re: Layout"
    date: 2006-04-16
    body: "Well, it would seem that the KDE.org stylesheets compensate adequately for any problem that may exist on my end. My suggestion to use them stands."
    author: "Eike Hein"
  - subject: "Re: Layout"
    date: 2006-04-16
    body: "yes, fonts are way too big for my tastes as well."
    author: "AC"
  - subject: "Re: Layout"
    date: 2006-04-25
    body: "Mines is totally screw up too.\n\nOlder digest were fine (few months ago), but this one is screwed up\nthe menu items are also \"white on white\",\nbut appears only when \"highlighted\"."
    author: "fp26"
  - subject: "Re: Layout"
    date: 2006-04-16
    body: "I like the idea of recycling the kde.org stylesheets. This would make for a much cleaner layout scheme IMO."
    author: "James Smith"
  - subject: "Thanks!"
    date: 2006-04-16
    body: "Thanks for doing this Danny! The KDE Commit Digest probably one of the most important products to attract new KDE developers :-)"
    author: "MK"
  - subject: "Buzz"
    date: 2006-04-16
    body: "what does the 'buzz' figure mean?"
    author: "Bert"
  - subject: "Re: Buzz"
    date: 2006-04-17
    body: "The buzz ratings are something that I have been calculating for a while at http://myscreen.org/buzz\n \nThey are basically an indication (not a totally foolproof scientific method ;) of the popularity/vitality/activity of a program or person. They are calculated from mentions on the internet (web pages) of the subject, recent discussion of the subject (blogs), and, something which I need to reintegrate back into the ratings, commit activity (I stopped that component of the score when the digest stopped... of course, now I am making the digest, so... ;)\n \nThese scores are regenerated every 2 days - up to date scores and charts to see change over time can be found at http://myscreen.org/buzz\n \nThanks,\nDanny"
    author: "Danny Allen"
  - subject: "Feature Request"
    date: 2006-04-17
    body: "I'm really glad that the commit digest is back. Would it be possible to have links to the diffs, or links to the kde-commits mailing list post?\n\nSome of the commits, like Kulow's change to the audiocd ioslave, don't make much sense without a diff."
    author: "AC"
  - subject: "Re: Feature Request"
    date: 2006-04-17
    body: "You are definitely right about having the diffs for each commit - luckily, it was at the top of my priority list for the digest... :) Look out for it in (hopefully) next week's digest.\n\nDanny"
    author: "Danny Allen"
  - subject: "Re: Feature Request"
    date: 2006-04-18
    body: "Great! Thank you. :-)"
    author: "AC"
  - subject: "yay"
    date: 2006-04-17
    body: "thank you for your time.  KDE commit digest is a great read."
    author: "DeadS0ul"
  - subject: "Is it dead?"
    date: 2006-04-17
    body: "I can't seem to reach commit-digest.org"
    author: "Allan S."
  - subject: "Re: Is it dead?"
    date: 2006-04-17
    body: "It has worked a few hours ago, but I cannot reach it now either, so probably it is overloaded.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Is it dead?"
    date: 2006-04-17
    body: "Yep, the server seems to be down - this is probably due to the fact that amaroK's website is hosted on the same server, and two sites from a single server on the front page of dot.kde.org at the same time is probably a bad idea... :)\n\nThe site should be back shortly.\n\nDanny"
    author: "Danny Allen"
---
In <a href="http://commit-digest.org/issues/2006-04-16/">this week's KDE Commit-Digest</a>: Furious activity in <a href="http://www.digikam.org/">digiKam</a>, <a href="http://edu.kde.org/kmplot/">KmPlot</a> and <a href="http://amarok.kde.org/">amaroK</a>, compile and linking fixes for applications in <a href="http://websvn.kde.org/trunk/KDE">trunk</a> with <a href="http://www.cmake.org/">CMake</a> and multi-platform porting fixes. Furthermore, KSmileTris was removed from the <a href="http://websvn.kde.org/trunk/KDE/kdegames">kdegames</a> module in trunk.

<!--break-->
