---
title: "Security Updates: Artswrapper and KDM"
date:    2006-06-15
authors:
  - "jriddell"
slug:    security-updates-artswrapper-and-kdm
---
KDE made two <a href="http://www.kde.org/info/security/">security announcements</a> today, the <a href="http://www.kde.org/info/security/advisory-20060614-1.txt">KDM Symlink Vulnerability</a> is a potential local exploit on systems using KDM as their login manager.  <a href="http://www.kde.org/info/security/advisory-20060614-2.txt">Artswrapper return value checking vulnerability</a> affects Linux 2.6 systems that have artswrapper installed SUID root.  A separate <a href="http://sourceforge.net/project/showfiles.php?group_id=10501&amp;package_id=68781">update was made for the wv2 library</a> used in KWord to import MS Word files to fix a boundary check error.  Your distribution should have updates for these issues.




<!--break-->
