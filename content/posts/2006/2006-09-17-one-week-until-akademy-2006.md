---
title: "One Week Until aKademy 2006"
date:    2006-09-17
authors:
  - "jriddell"
slug:    one-week-until-akademy-2006
comments:
  - subject: "videos"
    date: 2006-09-17
    body: "judging by the talks, this will be the best akademy so far...\nhope there will be videos this year too..!"
    author: "AC"
  - subject: "Re: videos"
    date: 2006-09-17
    body: "agreed!"
    author: "Vlad"
  - subject: "and transcripts, too"
    date: 2006-09-17
    body: "I hope there will be transcripts of the talks made available for the public. The topics are not only very interesting but very informative as well.\n\nGood luck and have fun!"
    author: "Jucato"
  - subject: "Re: videos"
    date: 2006-09-17
    body: "I agree. Something like the media coverage of OpenOffice.org Conference 2006 <http://ooocon-arnes.kiberpipa.org/> would be nice. Since I didn't have time to watch video streams live, the video archive was also of great help. Oh and one thing I miss in those videos is that you often can't hear what people from audience ask. So it would be nice to have a microphone ready for them too."
    author: "Jure Repinc"
  - subject: "Re: videos"
    date: 2006-10-19
    body: "come on"
    author: "helen"
  - subject: "Re: videos"
    date: 2006-09-17
    body: "I think cameras are ready already :) So don't panic! Recording questions from auditory is a great idea and we have this done already (wireless microphone)."
    author: "cookiem"
  - subject: "Universal Power Adaptor!"
    date: 2006-09-17
    body: "Yay! Now there's a thing less i should care about :-)"
    author: "Albert Astals Cid"
  - subject: "Re: Universal Power Adaptor!"
    date: 2006-09-17
    body: "I already have a Universal Power Adaptor, another one will be nice to plug more stuff than my laptop :)"
    author: "Micha\u00ebl Larouche"
  - subject: "aKademy map"
    date: 2006-09-18
    body: "I've just created a map for my own use (Google maps + some Krita magic), feel free to use it if you like it :)\n\nhttp://people.warp.es/~isaac/trinity-map.png\n\n"
    author: "Isaac Clerencia"
---
There is now less than one week to go until KDE developers meet with our users and industry supporters at Trinity College Dublin for our annual KDE World Summit, aKademy 2006.  We are pleased to announce a further two sponsors to our <a href="http://dot.kde.org/1156144808/">long list</a>.  Office automation equipment manufacturer <a href="http://conference2006.kde.org/sponsors/#ricoh">Ricoh</a> and mobile phone company <a href="http://conference2006.kde.org/sponsors/#nokia">Nokia</a> are now both silver supporters.  Read on for the keynote speakers and some more useful information.

<!--break-->
<div style="border: thin solid grey; margin: 1ex; padding: 1ex; float: right">
<img src="http://static.kdenews.org/jr/akademy-2006-da.png" width="400" height="400" />
</div>

<p>Our <a href="http://conference2006.kde.org/conference/program.php">conference programme</a> is one of the strongest we have had, full of fascinating talks and demonstrations.  aKademy will be opened with an introduction from the Head of the Irish Office of Science, Technology and Innovation, Ned Costello. The <a href="http://conference2006.kde.org/conference/talks/keynote1.php">opening keynote</a> is from Aaron Seigo with a reflective talk called Looking Ourselves In The Eye.  After lunch we welcome <a href="http://conference2006.kde.org/conference/talks/keynote2.php">John Cherry from OSDL</a> with a keynote talk on The State of the Linux Desktop.  <a href="http://conference2006.kde.org/conference/talks/keynote4.php">Sunday's keynote</a> is from Ciaran O'Riordan of FSF Europe talking about the problems of software patents.</p>

<p>As well as many of KDE's greatest developers and contributors, the programme also hosts talks from those in other Free Software communities including Keith Packard from X.org, Waldo Bastian from freedesktop.org and Anne Østergaard &amp; John Palmieri from Gnome.</p>

<p>The <a href="http://conference2006.kde.org/codingmarathon/bof.php">Birds of a Feather sessions</a> are filling up fast, including sessions on Strigi the exciting new desktop search framework, Kubuntu with Mark Shuttleworth, GPL v3 with the Free Software Foundation Europe, a beginners Qt Tutorial from Mirko Böhm, the HCI day on Wednesday and many more.</p>

<p>For those of you coming to the British Isles for the first time we have some good news: you will be supplied with a universal power adaptor courtesy of sponsor Trolltech, so you can plug in to the mains power without any re-wiring.</p>

<p>Check the <a href="http://wiki.kde.org/tiki-index.php?page=Arrival%20%40%20aKademy%202006">Arrival at aKademy wiki page</a> to find out if others will be at the airport at the same time as you.  If you are arriving on the Friday come to The Lloyd Institute in Trinity before 18:00 or Kennedy's pub after 18:00, both marked on <a href="http://conference2006.kde.org/organization/map.php">the conference map page</a>.</p>






