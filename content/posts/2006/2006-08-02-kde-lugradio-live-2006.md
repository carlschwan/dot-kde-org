---
title: "KDE at LugRadio Live 2006"
date:    2006-08-02
authors:
  - "blamb"
slug:    kde-lugradio-live-2006
comments:
  - subject: "Great Work"
    date: 2006-08-02
    body: "And you guys are certainly brave to talk to those Gnome loving guys.  :)  (j/k)"
    author: "am"
---
"Wow, that's impressive!" was one of many comments made by visitors to the KDE stand at <a href="http://www.lugradio.org/live/2006/">LUG Radio Live 06</a>. Five of the KDE-GB crew attended the two day event in Wolverhampton demonstrating the latest Kubuntu distribution, Dapper Drake, and the finest KDE applications, including Digikam, to over 400 attendees.



<!--break-->
<div style="border: thin solid grey; float: right; margin: 1ex; padding: 1ex">
<img src="http://static.kdenews.org/jr/lugradio-live-2006.jpg" width="180" height="240" /><br />
Ben Demonstrates KDE
</div>

<p>Jonathan Riddell gave <a href="http://kubuntu.org/~jriddell/lugradio2006/">a talk</a> covering Kubuntu and the current development of KDE 4.  The talk included some of the changes and additions made to KDE in Kubuntu and the exciting technologies being created for KDE 4 such as Plasma, Solid, Phonon and Live UI.  He also mentioned <a href="http://bazaar-vcs.org/">Bazaar</a>, the new distributed revision control system from Canonical.</p>

<p>"Many users hadn't used KDE for several years and were unaware of the quality of the latest apps." said Ben Lamb who was demonstrating <a href="http://www.digikam.org">Digikam</a> to visitors. "These are tools that would cost hundreds of pounds to buy for proprietry platforms."</p>

<p>We also showed off <a href="http://wiki.thekatapult.org.uk/Home">Katapult</a>, a program launcher utility that ships with Kubuntu. Simply hit Alt-Space and type the first few letters of the program you want, once Katapult has identified the program a large version of its icon and name are shown, hit return to launch the program.</p>

<p>One of Katapult's developers, Martin Meredith, explained how the forthcoming version offers quick spell-checking functionality and will even start playing your favourite music in amaroK if you enter the name of a track or artist. Users can also write their own plugins for the utility.</p>

<p>The stand had a definate Kubuntu theme to it thanks to the artwork of Ken Wimer on the banner, the stylish t-shirts from Kenny Duffus we had on sale and of course the hundreds of Kubuntu CDs we gave away.</p>

<p>It became apparent from talking to users that even those who don't use KDE as their desktop depend on the rich library of KDE applications to perform their work. K3b, Amarok and KPDF were frequently cited as being superior to any alternatives.</p>

<p>We also enjoyed the fine cuisine of Wolverhampton (curry), sold a t-shirt to Mark Shuttleworth, met with our <a href="http://www.flickr.com/photos/jriddell/197547977/">Friends from Gnome</a> and enjoyed the Saturday night party.  See the <a href="http://www.flickr.com/photos/jriddell/tags/lugradiolive2006">photos</a>.  Many thanks to the Lugradio crew for hosting another fun event.</p>



