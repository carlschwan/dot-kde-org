---
title: "\"C++ GUI Programming with Qt 3\" Book Download"
date:    2004-10-28
authors:
  - "binner"
slug:    c-gui-programming-qt-3-book-download
comments:
  - subject: "Great"
    date: 2004-10-28
    body: "The last and final thorn abt the book has also gone"
    author: "Iam who iam"
  - subject: "I can't believe it"
    date: 2004-10-28
    body: "Hey!\n\nI was searching yesterday evening exactly for a C++ and KDE developers book. Now this is like someone has read my mind. Sure QT is not directly KDE but it gets me close there. Thanks a lot."
    author: "Ali Akcaagac"
  - subject: "Re: I can't believe it"
    date: 2004-10-29
    body: "Haha, finally we've got rid of him...\nKDE.org, meet Ali \"oGALAXYo\" Akcaagac.\nEnjoy!\n\nIts time for a party over here!"
    author: "www.gnome.org"
  - subject: "Re: I can't believe it"
    date: 2004-10-30
    body: "I'm again and again astonished how hostile the GNOME community is against other desktops and in part their own users and developers."
    author: "Anonymous"
  - subject: "Re: I can't believe it"
    date: 2004-11-01
    body: "ATTENTION: This is not to start a flamewar.. it is just MY personal experience!\n\n After a few visits to #gnome (and #gnu) it stopped surprising me :-(\n\n To me those places apear more like brainwash-facilities then they are places to socialize and get voulenteer support :-/ Every time I said \"Linux\" I was flamed with remarks like \"It GNU/Linux lameass!!!\"... When I [accedently] mentiond KDE I was told: \"If you want to use that POS then please go jerk off there, and stop bothering us with your commercials!\". Apart from that i think I saw \"RTFM n00b!\" more than anything else 8-|\n\n Now... when I listen to RMS's speaches about \"free speach as opposed to free beer\" and \"the right to form a society\" I really think that a lot of the GNU ppl missed that point compleetly :-( I was effectively shut out of that society because my views didnt match that of the rest 100%.. so I feel that I have been deprived from just those two wonderfull things by the very group who is preaching it.\n\n Or is it just all the ill-mannerd youngsters who hang out in the main chans to look \"l33t\"? I dont know.. but I do for sure think that the entire KDE society is much more freindly and open to n00bs and people who by preference happen to like say Gaim over Kopete ;-)\n\n Peace, Love & OpenSource! (oops-sorry, Free Software! ;-)\n\n-Macavity"
    author: "Macavity"
  - subject: "Re: I can't believe it"
    date: 2004-11-01
    body: "A lot of people are so high and mighty thinking their way is the only way. I like KDE, and I like GNOME and frankly although most of my GUI development has been geared towars QT & KDE I also want to develop under GNOME and other desktop platforms as well. Why embrace one thing only and shut out and condemn everything else.\n"
    author: "Mylar"
  - subject: "Re: I can't believe it"
    date: 2004-11-02
    body: "> Why embrace one thing only \n\nLearning effort?  For a company: Cost of the time spent on getting familiar with all there is to know about the different frameworks. I think these are very strong points for specialising in only one of them.\n\n\n> and shut out and condemn everything else.\n\nThat does not necessarily follow from developing in only one DE.  When using standards as defined on http://www.freedesktop.org/ you may still find yourself developing *for* other DEs (by writing KDE apps that play nicely with other DEs).\n\n"
    author: "cm"
  - subject: "The book is great"
    date: 2004-10-28
    body: "I bought the book more or less when it was available for online purchase, I must admit that is great, and I'm very happy with it.\n\nIt's a pity that the only book about KDE programming is outdated. There was a project intended to solve this, kdeev-books, but it's mailinglist, and it's CVS module are completely empty.\n\nIf I had more knowledge, I will try to help with this, but when basic things like XML-GUI etc. appear, I become lost, and all I can do is copy and paste code from others. :-/"
    author: "suy"
  - subject: "This is a really great book"
    date: 2004-10-28
    body: "Bought it myself a few months ago, got half-way through before I got sidetracked (*cough* Doom *cough*) but am getting back into it know. It's not just the API, there's lots of little coding tips in their, and the book is clearly written with a view to Qt 4 (e.g. they only show menu creation using actions).\n\n"
    author: "Bryan Feeney"
  - subject: "More downloadable books!"
    date: 2004-10-28
    body: "The rest of \"Bruce Perens' Open Source Series\" is here:\n\nhttp://phptr.com/promotion/1484?redir=1\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Source code downloadable?"
    date: 2004-10-28
    body: "Did anyone have any look with downloading the accompanying source code? I tried with both Konqueror and Mozilla several times since the book was available to download some weeks ago. But all I ever got was a 404 page."
    author: "Some KDE user"
  - subject: "Re: Source code downloadable?"
    date: 2004-10-28
    body: "I could download the 11 MB sit file without a problem. It's a stuffit archive, right? Is there a open source tool to unpack those, or do I have to use the stuffit trial version? "
    author: "Flitcraft"
  - subject: "Re: Source code downloadable?"
    date: 2004-10-28
    body: "where is this .sit file?"
    author: "Osho"
  - subject: "Re: Source code downloadable?"
    date: 2004-10-29
    body: "Under downloads, download source document\n\nhttp://phptr.com/content/images/0131240722/downloads/blanchette_sourcecode.sit"
    author: "Flitcraft"
  - subject: "Re: Source code downloadable?"
    date: 2005-01-28
    body: "It is of a propritary format... so no use downloading the sit file... Anybody please show me a gzip archive of the same?\n\nraj"
    author: "Rajesh"
  - subject: "Re: Source code downloadable?"
    date: 2005-08-12
    body: "You can uncompress .sit file by downloading stuffit expander trial software(the one I used is for Microsoft Windows platform) and uncompress this .sit file. After that you can see all of the sourecodes of this book."
    author: "Gunslicker"
  - subject: "Don't I feel dumb now... :-P"
    date: 2004-10-28
    body: "I bought this several months ago. But I don't actually feel dumb - it was worth the purchase price. Read it cover to cover (a bit scary); extremely well-written and very good."
    author: "Antonio Salazar"
  - subject: "Great, Take II"
    date: 2004-10-28
    body: "As soon as I read the initial posting about the availability of this book, I bought it from Amazon!  \"Fate, she is a cruel mistress...\""
    author: "Robert Tilley"
  - subject: "D'oh! This is a joke!!!"
    date: 2004-10-28
    body: "What a joke! I brought it 2 weeks ago from Amazon. If I only waited a bit...\n:-D\n"
    author: "Samuele"
  - subject: "Re: D'oh! This is a joke!!!"
    date: 2004-10-29
    body: "If I didn't have the book already, I would still buy it. A dead tree version is so much more conveniant than a pdf file. Printing the pdf is not really an option, since it cost about the same as the book itself."
    author: "Johan Veenstra"
  - subject: "Konqueror problems with that page"
    date: 2004-10-29
    body: "Anybody else having problems in konqueror when they click on the downloads link? I am running 3.3.1 and the page just won't display. Works fine in firefox but I've got so used to just using konqi."
    author: "Jon Scobie"
  - subject: "Re: Konqueror problems with that page"
    date: 2004-10-29
    body: "I confirm with KDE 3.3.0 on gentoo here. Konqueror won't display the page correctly."
    author: "Med"
  - subject: "Re: Konqueror problems with that page"
    date: 2004-10-29
    body: "It seems funny that a site which is giving away a book about Qt development won't work with the very application that is written using the toolkit. I know that is looking at the problem VERY broadly, but annoying all the same."
    author: "Jon Scobie"
  - subject: "Re: Konqueror problems with that page"
    date: 2004-10-29
    body: "Don't know how it's supposed to look like, but it look perfectly ok in Konqueror 3.2.2-4 (RedHat)"
    author: "Johan Veenstra"
  - subject: "Great book"
    date: 2004-10-29
    body: "I got this book about a month before troll tech said it would be released (still not sure how... I wasn't expecting it to come for a few months), I skimmed around in it, and read parts.  This is the only C++ refernce I've ever had and the first time reading it I was a bit confused (mostly because I skipped over a few IMPORTANT chapters).  Now I've decided to go back and read it from cover to cover (started about a week ago, don't get much time to read it though), and with what I've learned in AP Comp Science (java not c++) has made it so I've understood everything in this book so far (and I've had about 4 weeks of classes).\n\nIf you want to get into KDE or QT programming, this book is the greatest thing I've found so far.  Maybe someone can write a book / long tutorial about KDE programming for someone with some experience in QT.  This is all I need to really be able to start helping some KDE projects."
    author: "Corbin"
  - subject: "Yes!!"
    date: 2004-11-13
    body: "Finally, a Qt 3 book. Just in time for the release of Qt 4!!"
    author: "LuckySandal"
  - subject: "Re: Yes!!"
    date: 2004-11-13
    body: "Is this meant sarcastic? The book was published one year before the planned release date of Qt 4."
    author: "Anonymous"
  - subject: "thank"
    date: 2005-03-17
    body: "thank"
    author: "hhh"
  - subject: "help me"
    date: 2005-04-24
    body: "I bought the book a long time ago, and I feel really dumb since I still don't understand how to acutally run my project. I've compiled the Makefile and .pro file, but nothing happens when I run make. "
    author: "dave"
  - subject: "Re: help me"
    date: 2005-06-05
    body: "Makefile created with qmake? ok. try \"make -fmakefile\"\n\nex.\n  //creating the makefile\nqmake -o makefile myproj.pro\n  //creating the compiled file (executable)\nmake -fmakefile"
    author: "Helper"
  - subject: "\"C++ GUI Programming with Qt 3\" +download"
    date: 2005-07-14
    body: "I am very upset its not available for download. Can anyone tell me where i could get this book for download.\n"
    author: "sailaja"
  - subject: "Re: \"C++ GUI Programming with Qt 3\" +download"
    date: 2005-07-14
    body: "It is still available on the page which the story mentions."
    author: "Anonymous"
  - subject: "Re: \"C++ GUI Programming with Qt 3\" +download"
    date: 2006-02-24
    body: "i am student of engg.college i required for project purpose"
    author: "sudrik sanjay"
  - subject: "Re: \"C++ GUI Programming with Qt 3\" +download"
    date: 2006-01-06
    body: "thank God i found this tutorials."
    author: "ustaz"
---
The "<a href="http://www.amazon.com/exec/obidos/tg/detail/-/0131240722/ref=ase_thekdesktopenvir/103-3775731-8087868?v=glance&s=books">C++ GUI Programming with Qt 3</a>" book written by Trolltech software engineer Jasmin Blanchette and Trolltech's documentation manager Mark Summerfield, already <a href="http://dot.kde.org/1076068778/">featured by the dot</a>, can now be downloaded in PDF format from the <a href="http://phptr.com/title/0131240722">publisher's homepage</a> in the "Downloads" section.


<!--break-->
