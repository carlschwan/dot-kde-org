---
title: "Application of the Month: Konsole"
date:    2004-03-22
authors:
  - "fmous"
slug:    application-month-konsole
comments:
  - subject: "Ctrl+T to open a new tab"
    date: 2004-03-22
    body: "Konsole is a great app and by far the best terminal emulator I have ever seen, but I never understood why I can't use the CTRL+T shortcut to open a new tab. I just get a visual bell signal, although I have assigned it in the options.\n\nI thought it might be blocked because ctrl+t would be a bash shortcut (like ctrl+d is for instance), but it works in gnome-terminal"
    author: "fl"
  - subject: "Re: Ctrl+T to open a new tab"
    date: 2004-03-22
    body: "Ctrl+T is the readline (emacs mode) binding for transpose character.\n "
    author: "Will Newton"
  - subject: "Re: Ctrl+T to open a new tab"
    date: 2004-03-22
    body: "You can use Ctrl-N or Ctrl-Alt-N to open a new tab.  It's one or the other, just can't remember which.  This shortcut has been available for quite some time.\n\nAs an added bonus, you can hold down the Shift key and press the right and left arrow keys to navigate the tabs, similar to holding Alt and pressing left and right to navigate virtual consoles."
    author: "Jared"
  - subject: "Re: Ctrl+T to open a new tab"
    date: 2004-03-22
    body: "Ctrl-Shift-N or similar. This is why I still prefer multi-gnome-terminal as terminal emulator: key-chains. Press Ctrl+L and any letter to perform action, no chords, no problems."
    author: "m."
  - subject: "Re: Ctrl+T to open a new tab"
    date: 2004-03-23
    body: "KDE in general supports key-chains (labelled multi-key mode) too, it's just not working in Konsole with Ctrl (#61667)."
    author: "Anonymous"
  - subject: "Re: Ctrl+T to open a new tab"
    date: 2004-03-23
    body: "Voted."
    author: "m."
  - subject: "Re: Ctrl+T to open a new tab"
    date: 2004-03-22
    body: "CTRL+ALT+N\n\ndoes the trick."
    author: "cascadefx"
  - subject: "Re: Ctrl+T to open a new tab"
    date: 2004-03-22
    body: "Hold down CTRL+SHIFT and press the left/right arrow keys to rearrange the order of the tabs."
    author: "Mike"
  - subject: "Re: Ctrl+T to open a new tab"
    date: 2004-03-23
    body: "Or use middle mouse button."
    author: "Anonymous"
  - subject: "Re: Ctrl+T to open a new tab"
    date: 2004-03-22
    body: "The cvs digest said this was added this week."
    author: "John Tapsell"
  - subject: "Re: Ctrl+T to open a new tab"
    date: 2004-03-24
    body: "To konqueror. Not to konsole."
    author: "Leo Savernik"
  - subject: "ES.kde.org?"
    date: 2004-03-22
    body: "I know it's off topic, but I don't know where to look for information. The Spanish KDE website (KDE Hispano, es.kde.org, www.kdehispano.org) is down (ans has been down for about a week now). Does anyone have any news on this?\nThanks!"
    author: "j00z"
  - subject: "Re: ES.kde.org?"
    date: 2004-04-14
    body: "It's back online now."
    author: "Anonymous"
  - subject: "Thank you Lars !"
    date: 2004-03-22
    body: "I can't even imagine my life without using konsole ;-). It brought so much sanity to my everyday work. I always keep several shells open: one for root, two or three in different projects I am working on, one on a remote machine, etc. I switch back and forth, change fonts if needed, etc. Lovely app.\n\nCheers !"
    author: "MandrakeUser"
  - subject: "Re: Thank you Lars !"
    date: 2004-03-22
    body: "I you like Konsole, you should give \"screen\" a try someday... it has too many features to list here, but it's enough to say that it is the most underrated unix utility there ever was.\n"
    author: "Pedro"
  - subject: "Re: Thank you Lars !"
    date: 2004-03-22
    body: "imo screen lacks a feature for easy scrolling. Shift+PgUp/PgDn is much more convenient."
    author: "Dominic"
  - subject: "Re: Thank you Lars !"
    date: 2004-03-22
    body: "screen keeps a configurable number of lines in each virtual terminal buffer.  You can see previous screensful by typing \"^Z[esc]\" (thus entering copy-mode), and using your page-up, page-down, up ,and down keys.  Press any non-copy-mode key (\"Z\" works) to go back to normal mode.\n\nI'll note that I use screen *with* konsole.  One ssh session in konsole per host, each with its own screen session.\n\nChris"
    author: "Chris Goldman"
  - subject: "Re: Thank you Lars !"
    date: 2004-03-23
    body: "Same here ... recursive screen's gets a little hard to use ..."
    author: "Pedro"
  - subject: "Re: Thank you Lars !"
    date: 2004-03-23
    body: "Yes, but Shift + Page Up/Down is much, much easier :) The following line in ~/.screenrc does the trick for me:\n\ntermcapinfo xterm ti@:te@\n"
    author: "Gaute Hvoslef Kvalnes"
  - subject: "Re: Thank you Lars !"
    date: 2004-03-23
    body: "Very nice, thank you."
    author: "Dominic"
  - subject: "bruce.edge@gmail.com"
    date: 2006-03-23
    body: "Super cool.\nThat even makes the mouse scroll work in screen. \n\n-Bruce"
    author: "Bruce Edge"
  - subject: "Re: Thank you Lars !"
    date: 2004-03-23
    body: "Is it possible that Lars contributed to Konsole last time in 9/2000 (http://tinyurl.com/2czwu)?"
    author: "Anonymous"
  - subject: "some questions"
    date: 2004-03-22
    body: "I use Konsole most of time. It is great! But I found that I don't see many features in my computer:\n\nI don't have Bookmarks Menu in my Konsole. I don't have Settings->Save Sessions Profile...\n\nI use RedHat 7.3. KDE 3.?\n\nThank you very much for your help"
    author: "xs"
  - subject: "Re: some questions"
    date: 2004-03-22
    body: "RedHat 7.3 shipped with KDE 3.0.0 which has its second anniversary next week.\nUse a current KDE version and you will have the current feature set."
    author: "Anonymous"
  - subject: "Re: some questions"
    date: 2004-03-22
    body: "you may type the following in a konsole to see what version you are running:\n\nrpm -qa |grep kde\n\nOr even simpler, go to \" Help -> about \" in any kde app (assuming RedHat didn't modified these help entries)\n\nIn the end, yes, you'll need to update both your OS and KDE version to have the latest features ..."
    author: "MandrakeUser"
  - subject: "Re: some questions"
    date: 2004-03-22
    body: "> In the end, yes, you'll need to update both your OS and KDE version to have the latest features ...\n\nWhy should he update his whole desktop environmen? He just needs one app updated.\n\n-> Just download konsole and do \"./configure; make; checkinstall\"\n\nOh, wait, it's KDE! MandrakeUser is right, you have to update your whole desktop environment.  That's one of the reasons, KDE has not jet reached 50% (unix) market share."
    author: "Hans"
  - subject: "Re: some questions"
    date: 2004-03-22
    body: "> Oh, wait, it's KDE! MandrakeUser is right, you have to update your whole \n> desktop environment. That's one of the reasons, KDE has not jet reached 50% \n> (unix) market share.\n\nUh Uh, I didn't mean any of these assertions. KDE is one of the two big Linux desktops, I understand it is the most popular but I couldn't care less. It just works great for me.\n\nNow, you might be able to compile konsole by itself if your kdelibs' version is greater than 3. I am not sure though. However, I recommended upgrading the whole desktop environment simply because the original poster is certainly several features everywhere. And it is much faster and safer, at least in mandrake, to upgrade the whole kde than compiling stuff by yourself."
    author: "MandrakeUser"
  - subject: "Re: some questions"
    date: 2004-03-23
    body: "This is not about KDE. It's a general fact that programs require the minimum version of libraries which offer the functionality they depend on. Troll elsewhere."
    author: "Anonymous"
  - subject: "Copy/Paste behavior in Konsole"
    date: 2004-03-22
    body: "Konsole is a great application, and I use it quite extensively every day.  However, one thing bugs me about it that I cannot seem to resolve.  Prior to KDE 3.1, selecting (highlighting) text in Konsole would automatically copy it to the clipboard.  From there, I could go to any other application, and use the Paste function to paste it (in addition to the standard X11 method of middle-clicking).\n\nAs of 3.1, however, this functionality was changed so that text is not automatically copied to the clipboard; you have to manually click Edit, Copy to be able to then use the Paste function in another application to copy/paste text.  Yes, you can still middle-click, but this is often limiting (middle-clicking to paste text in a browser's URL bar, for example, is an awful experience - you highlight the text you want to copy/paste, click on the URL bar (which automatically highlights the current URL to delete it) and then you paste...  the same URL that you already had loaded.)\n\nI presume this change was made as a convenience, which I can understand, but I can't figure out any way to change it back.  Basically, I'd like to do one of two things - either revert the behavior back to it's pre-3.1 style, or just be able to manually copy the text I have highlighted (eg., a hotkey sequence like Ctrl-C).  I've tried the hotkey method before, but that didn't want to work for me either.\n\nAny suggestions on this?  Thanks."
    author: "Jared"
  - subject: "Re: Copy/Paste behavior in Konsole"
    date: 2004-03-22
    body: "You can configure your very own shortcuts via the settings menu. Just don't use Ctrl-C, that's pretty much reserved in a console.\n\nAlso, you don't have to paste an URL in the address bar, just middle click somewhere in the browser window. Konqueror will then open the URL. Beats Ctrl-V hands down, in my opinion.\n\nCheers, Fogger"
    author: "Jan Felix Reuter"
  - subject: "Re: Copy/Paste behavior in Konsole"
    date: 2004-09-13
    body: "I found this link while trying to figure out why pasting something like this;\n\nfoo:bar\n\ninto konsole results in only\n\nbar\n\nbeing pasted in. \n\nThis behavior sucks and I am at a loss to configure it away or even explain it.\n\nOffers?\n\nThanks!\n"
    author: "Steve Wray"
  - subject: "Re: Copy/Paste behavior in Konsole"
    date: 2005-12-10
    body: "I like Konsole too. But I still do not know how to config the setting to make the copy/paste working. \n\nI use WindowsXP, VNC, and Linux (VNC). I would like be able to copy/paste text between windows and Linux Konsole, also between Konsole in one VNC to another Konsole in another VNC.\n\nMy environment could copy/paste texts between windows and xterms. The text could also be copy/paste between xterm and Konsole in one VNC. \n\nI would be appreciate that someone could give me a solution. \n\nBest regards\n\nJonathan \n"
    author: "Jonathan Su "
  - subject: "Re: Copy/Paste behavior in Konsole"
    date: 2005-12-16
    body: "The copy / paste works by adding two lines to xstartup:\n\n  /home/gnu/vnc-4.0/bin/vncconfig -nowin &\n  /home/utils/autocutsel-0.8.0/bin/autocutsel -selection PRIORITY &\n\nJonathan "
    author: "Jonathan Su "
  - subject: "Re: Copy/Paste behavior in Konsole"
    date: 2004-03-22
    body: "Just to point out that you can select an URL and then middle-click it anywhere into a Konqueror window and it will load that page.\nThe same is truth to Mozilla."
    author: "Ricardo Cruz"
  - subject: "Re: Copy/Paste behavior in Konsole"
    date: 2004-03-22
    body: "I think this is a setting in Klipper (assuming you're running it).  Look in \"configure Klipper...\", and choose \"Synchronise contents of the clipboard and the selection\".  This reverts to pre-3.1 behaviour (for me).\n\n--MC"
    author: "martinc"
  - subject: "Re: Copy/Paste behavior in Konsole"
    date: 2004-03-30
    body: "\"Synchronise contents of the clipboard and the selection\" is an\nadditional feature that some people might want, but is not the\nsame as the pre-3.1 behavior.  The klipper documentation explains\nthat X keeps it's own clipboard which is separate from klipper's\nclipboard.  The problem is that the new version of kde seems to\nhave disabled X's clipboard.  I also am having this problem and\nselecting \"Synchronise contents of the clipboard and the selection\"\ndoesn't fix the problem (even if it did, I still would prefer the\nold behavior of having independent clipboards).  Any help would\nbe greatly appreciated."
    author: "Jon Gabrielson"
  - subject: "Re: Copy/Paste behavior in Konsole"
    date: 2004-03-30
    body: "Ok, I figured it out.  I disabled klipper and then restarted X\nand now it works like pre-3.1"
    author: "Jon Gabrielson"
  - subject: "Re: Copy/Paste behavior in Konsole"
    date: 2007-08-16
    body: "There's now an option in Klipper to synchronize copy and select. I was having the same problem, so I found this thread and just enabled it instead of quitting Klipper altogether."
    author: "Archon810"
  - subject: "Konsole??"
    date: 2004-03-22
    body: "Wow, great app, a black screen with text. Really cool guys.\n\nI wonder why Konsole shows up as app of the month all the time,\nare there only hackers voting?"
    author: "OI"
  - subject: "Re: Konsole??"
    date: 2004-03-22
    body: "I haven't noticed it being app of the month before.\n\nI think you're ranting :-)"
    author: "Jeremy"
  - subject: "Re: Konsole??"
    date: 2004-03-22
    body: "If you don't like white text on black background, change it! There are several predefined color schemes, including several transparent themes."
    author: "David Johnson"
  - subject: "Re: Konsole??"
    date: 2004-08-05
    body: "How Proprietary! Predefined Schemes!\nIn the font menu, we can choose 'custom', but I'm sure glad we don't get the option in the Schema menu! Yup, I just love it when some programmer decides what we're allowed to customize and what we're not. We should all switch to Microsoft and vote CMD as the \"Cool new application of the month\"!\n\nWhat if we don't like the four preconfigured transparent schemes we get? What if we want to make it fit in with our desktop? I guess we have to suffer, eh? Or vote for some OTHER console! Anybody ever tried ATerm?"
    author: "Chibix"
  - subject: "Re: Konsole??"
    date: 2004-08-05
    body: "> What if we don't like the four preconfigured transparent schemes we get? \n\nCreate new ones with the scheme editor."
    author: "Anonymous"
  - subject: "Re: Konsole??"
    date: 2004-03-22
    body: "There are a huge number of indispensable utilities available in the command line. Nothing beats a shell for admin work, programming, etc. Nothing. Even Microsoft is enhancing their command line stuff. These users may be over represented right now among linux users. I don't hear them denigrating people for using and prefering graphical tools to draw pictures. Why have a heavy weight application when a simple lightweight shell utility works best?\n\nFor those who need it, Konsole is very nice.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Konsole??"
    date: 2004-03-22
    body: "A little further and maybe a rant.\n\nIn my work, among other things I maintain digital control systems for buildings and refrigeration plants. Invariably they come with some pretty graphical displays. Note I said maintain, not install. There is a reason for that. They are sold on the premise that pretty pictures will help the unskilled maintain their systems. I can't lie very well, so I can't sell them. But I digress. The graphical screens are inevitably useless for the technicians that maintain the systems. They only show things, maybe accurately, maybe not. To fix problems require white text on black, or the inverse. Changing configuration, reprogramming, etc. And what happens is the graphical screens, which are an expensive useless addon, get out of sync with what is happening in the system. We don't maintain them since the software to do so is hugely expensive and proprietary, and the customers can't see any payback anyways. So they end up being an expensive boat anchor.\n\nGraphical programming is very expensive in resources, manpower and engineering. In some situations the costs are worth it. Most times they are not, especially with embedded controls with limited hardware. What that means is the graphical effort takes resources away from the basic and necessary stuff, like control algorithms, hardware design, etc. What we end up with is a pretty useless piece of junk.\n\nSo don't go ragging on command line tools. They work. That alone puts them above 95% of everything else. And when money is on the line, I'll go for what works. That way I may get paid for my effort.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Konsole??"
    date: 2004-03-23
    body: "I certainly don't think anyone was ragging on console-based tools, Derek.  It is no doubt a valuable tool for many people. "
    author: "anon"
  - subject: "Re: Konsole??"
    date: 2004-08-05
    body: "Expensive and proprietary to keep the graphical environment up to date? Jesus, sounds like Linux needs a lot of work. If they're such boat-anchors, maybe it's time to change the operating systems. You could go with a version that comes without X11, maybe? Maybe if all you GOT was white text on a black screen, They'd be less boat-anchory?\n\nThen again, You never have problems with Microsoft Operating Systems getting \"Out of Sync\" with themselves, since the graphical environment Only Reflects what is true in the command-line.\n\nHey, Go with BeOS if you want something impressive, though. Then you don't have to worry about tools for graphical maintenance because the company's dead! They won't be MAKING any more tools! =D FUN!"
    author: "Chibix"
  - subject: "Re: Konsole??"
    date: 2004-03-23
    body: "Heh, I love it, anybody who uses a command line is a 'hacker'.  Perhaps you'd be more comfortable using Microsoft Bob.\n\nOr Just wait until they come out with computers with only one button, that comes pre-pressed at the factory?\n\nOr mabye you'd like to scrap the written word altogether and go back to crude cave paintings?"
    author: "Tom"
  - subject: "konsole title"
    date: 2004-03-22
    body: "Does anyone know how to get konsole to set the last executed command to konsole's window title. For example issuing the command 'find /' would set the title as \"larry@localhost: find /\" or for telnet sessions to other hosts like \"larry@otherhost\".\n"
    author: "larry"
  - subject: "Re: konsole title"
    date: 2004-03-22
    body: "It appears this can't be done (from bash, at least). The shell command that can be used to get the last command - fc -l - could potentially work, *except* that it appears the last command isn't entered into the history at the time the prompt is prepared. (Try setting PROMPT_COMMAND to \"fc -ln|tail -1\" - you'll see not the last command, but the one before that.)\n\nThe closest one can get is to put $_ in PS1; unfortunately this is only the last argument to the last command, so it only works if the command had no arguments. (i.e. for 'find / -name foo', you'd get just 'foo').\n\nPity."
    author: "Grant McDorman"
  - subject: "Re: konsole title"
    date: 2004-03-22
    body: "You can do this with tcsh, though I admittedly haven't actually tried it, but I'm sure you can.  See tcsh's special variables and a special alias (all of which I forget the name of since I haven't used them recently) to execute a command before each prompt.  I set the title of my konsole screens like this, which you could take a mess with.\n\nsetenv XTITLE \"!*\";cwdcmd; dcop `echo $KONSOLE_DCOP_SESSION | sed \"s/DCOPRef.//;s/,.*//\"` `echo $KONSOLE_DCOP_SESSION | sed \"s/.*,//;s/)//\"` renameSession \"$XTITLE\"\n\nI realize I haven't been a great help, but I can only offer a starting suggestion since I don't have any more time. "
    author: "Wes Hardaker"
  - subject: "Re: konsole title"
    date: 2004-03-22
    body: "This is not really what you're looking for, but you might find it useful:\n\nhttp://kde-look.org/content/show.php?content=6976"
    author: "Aur\u00e9lien G\u00e2teau"
  - subject: "Re: konsole title"
    date: 2004-03-23
    body: "This depends on which shell you use: you can do it in tcsh or zsh using the 'postcmd' or 'preexec' hooks [1, 2]. Unfortunately, as others have said, you can't do it in standard bash because it lacks such a hook [3]. (There is, however, a patch to add it [4, 5].)\n\n[1] http://cns.georgetown.edu/~ric/howto/Xterm-Title/Xterm-Title-5.html\n[2] http://www.macosxhints.com/article.php?story=20020207093427440\n[3] http://www.kuro5hin.org/comments/2003/4/22/0206/78523?pid=19\n[4] http://mail.gnu.org/archive/html/bug-bash/2003-10/msg00034.html\n[5] http://noxa.de/~rvb/pub/patches/"
    author: "hysterion"
  - subject: "Re: konsole title"
    date: 2004-03-23
    body: "I am wondering how make it stop! I does it all the time for me, which is embarresing when I am logging onto a SQL-server using a clear-text password after --password.\n"
    author: "Allan Sandfeld"
  - subject: "Re: konsole title"
    date: 2004-08-18
    body: "I added this to my .cshrc\n\nif (\"$TERM\" == \"xterm\") then\n      alias precmd 'echo -n \"\\033]0;`history | tail -n 1 | cut -f 3-`\\007\"'\nendif"
    author: "Stephen"
  - subject: "Kicker Default"
    date: 2004-03-22
    body: "So...  is anyone convinced that Konsole needs to put back in the Kicker default yet?\n\nI know I am!"
    author: "ac"
  - subject: "Re: Kicker Default"
    date: 2004-03-22
    body: "I second the motion. Konsole needs to remain on Kicker! If some particular distro can't stand this, then they can trivially change it for themselves. Konsole is there for a purpose. I am getting sick of this attitude that usability means dumbing down the interface."
    author: "David Johnson"
  - subject: "Re: Kicker Default"
    date: 2004-03-22
    body: "> I am getting sick of this attitude that usability \n> means dumbing down the interface.\n\ngood thing that wasn't why it was done, then =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Kicker Default"
    date: 2004-03-23
    body: "Perhaps \"dumbing down\" was the wrong phrase. Maybe I'm just an old fart out of touch with the times, but for me Konsole is one of the most frequently used applications in KDE. It's the primary starting point for most of my tasks.\n\nI realize that there is very limited room on Kicker, and that stuff needs to be prioritized. The question is whether we prioritize according to the needs of the ex-Windows user who has no idea what a terminal is, or prioritize for the current Unix user who sees the command line as a useful tool.\n\nSo why was it done? Set an old codger straight in his ways..."
    author: "David Johnson"
  - subject: "Re: Kicker Default"
    date: 2004-03-22
    body: "I'm curious. Why does console need to remain in the kicker? I mean, I use the konsole all the time (I've got half a dozen open at any given time) but I see no reason it needs to remain in the default kicker. Certainly, I'd wager most KDE users don't use it enough to warrent it."
    author: "Rayiner Hashem"
  - subject: "Re: Kicker Default"
    date: 2004-03-23
    body: "Are you kidding?  No matter what you do, almost all linux users need the command line from time to time.  Konsole and Firefox are the ONLY icons I need in kicker.  It's trivial to add them myself, of course, but I really can't see how anything else can warrant inclusion by default over konsole."
    author: "Nick Matteo"
  - subject: "Re: Kicker Default"
    date: 2004-03-23
    body: ">  I am getting sick of this attitude that usability means dumbing down the interface.\n\nGetting rid of konsole from the default kicker config was dumbing down the interface? Just found that comedic. \n\nThat is all. "
    author: "Sashmit Bhaduri"
  - subject: "Re: Kicker Default"
    date: 2004-03-23
    body: "My counterargument at the time was that people who use consoles have their hands at the keyboard, and for them doing an alt-f2; ko<TAB><ENTER> is way more comfortable than pulling mouse to icon. I don't think that has changed."
    author: "Cloaked Penguin"
  - subject: "Re: Kicker Default"
    date: 2004-03-25
    body: "True.. \n\nOr.. use KHotkeys to set up a hotkey shortcut, as I have for all my frequently used apps.\n\nI have one of those fancy keyboards with a bunch of \"multi-media\" and \"internet\" keys, and set up one of them, XF86WWW, as the \"user-menu\" key.  \"UMenu, C\" gives me a konsole, \"UMenu, Ctrl-C\" gives me the KDESU prompt and then a root konsole.  F/Ctrl-F work the same way as C/Ctrl-C but for file management (Konqueror).  E/Ctrl-E are the same for editing (KWrite), N for news (PAN, since KNode doesn't do yEnc, last I checked), M for Mail (KMail), G for one game, Ctrl-G for another, etc.  I use Alt, as in Alt-E, for alternatives, so UMenu, Alt-E, for instance, opens KHexEdit.\n\nI haven't set up any mouse gestures yet, as I just installed KDE 3.2 since Mandrake is a bit behind on AMD64 and just came out with 3.2 for AMD64  (the reason I'm working on switching to Gentoo, BTW).   I will eventually, most likely, but that isn't really appropriate for a konsole shortcut anyway, for the reasons you mentioned.\n\nDuncan (who followed the link from LWN).\n"
    author: "Duncan"
  - subject: "KNode - yEnc"
    date: 2004-03-31
    body: "I think KNode supports decoding of yEnc now. (As of KDE 3.2)"
    author: "jfb3"
  - subject: "Doele -> Doelle"
    date: 2004-03-23
    body: "There is a spelling error in the name of the interviewee."
    author: "Jens"
  - subject: "Re: Doele -> Doelle"
    date: 2004-03-23
    body: "Thanks .. fixed \n\nFab"
    author: "Fabrice Mous"
  - subject: "konsole with ssh and screen rocks"
    date: 2004-03-23
    body: "i setup a few sessions in konsole like this:\n\nssh -t user@host screen -r -d ScreenSessionName\n\n(menu, Settings, Configure Konsole, Session Tab -> Execute)\n\nthis way, i'm only one klick away from my remote hosts"
    author: "ac666"
  - subject: "Unicode Not supported In Konsole"
    date: 2004-03-23
    body: "I can't type hindi in Konsole, even after setting the font to mangal (ms hindi unicode font).\n\nAnd also, I can 't save a file with Hindi names (unicode characters). Though all the KDE environment looks hindi fine. But one can not have hindi file/folder names in KDE..."
    author: "_Fast_Rizwaan___"
  - subject: "Re: Unicode Not supported In Konsole"
    date: 2004-03-23
    body: "Indeed. My locale is \"en_US.utf8\" and I use gnome and gnome-terminal.\n\nI tried konsole and I can't even type quotes or accented chars('\"\u00e7\u00e9\u00f1&#341;&#291;\u00f9\u009f ...)"
    author: "A.N. Onimus"
  - subject: "Visual bell - feature request"
    date: 2004-03-23
    body: "If I type the following:\n$ tes<tab>\nand the available applications are test, testparm, and testprns, bash beeps and completes up to the \"t\":\n$ test\nThis interacts with the Konsole visual bell in a way that bugs me immensely. Instead of printing the \"t\" and flashing to indicate the bell, Konsole flashes to indicate the bell, then prints the t. During the delay it looks like it can't complete anything at all!\n\nMy configuration has bell set to \"System Bell\" in Konsole, and \"Use visible bell\" under KDE Control Centre/Accessability/Use visible bell. Hmm... it actually seems to behave better when set to visual bell in Konsole itself.\n\nMy feature request: Make the visual bell concurrent with subsequent updates for the visual bell accessabilty option.\n\nBenjamin.\n\n"
    author: "Benjamin."
  - subject: "The good and the bad"
    date: 2004-03-23
    body: "I like konsole - I like being able to have anti-aliased fonts, and have a widget set that fits with everything else, but there are a couple of features which I miss too much to give up using rxvt:\n\n- Scrolling lots of text is very much slower than rxvt (which can slow compiles down!)\n\n- I can't ask it that $DISPLAY is; rxvt can tell me.  This feature can be very useful; I don't want to \"ssh -X\" all the time as that's slower.\n\nThe memory footprint could also be considered a point, but these days that's becoming more and more insignificant.  I don't have a 486 w/ 12MB anymore. :-)\n\n-- Steve"
    author: "Steve"
  - subject: "Re: The good and the bad"
    date: 2004-03-23
    body: "> - I can't ask it that $DISPLAY is; rxvt can tell me.\n\nI don't understand what you mean. Asking within a program what environment variables are set?"
    author: "Anonymous"
  - subject: "Re: The good and the bad"
    date: 2004-03-24
    body: "It's not asking what environment variables are set directly.  You probably know that you can do things like set the terminal title by echoing a command sequence.  There are similar sequences for moving the cursor, setting colours, asking the terminal about itself and so on.  One such sequence (^[[7n) will cause rxvt to write the name of the display it's on back to stdin, which you can read in and use to set $DISPLAY.  You can configure rxvt (in the source) to give you either the hostname of the IP address.\n\nIt's a very nifty feature for those of us who use remote X apps.  I'd very much like to have this in konsole as an option (disabled by default though).\n\n-- Steve"
    author: "Steve"
  - subject: "Re: The good and the bad"
    date: 2004-03-24
    body: "If you really want to have a feature then please file a wishlist entry at http://bugs.kde.org."
    author: "Anonymous"
  - subject: "Re: The good and the bad"
    date: 2004-03-23
    body: "I really like konsole but have to agree on the fact is it quite slow. It's not a problem normally, but the testbed for my programming project takes about 20 seconds to run in xterm and 35 seconds to run in konsole. It spits out a lot of debug text on the way, so konsole appears to be much slower in displaying it. In xterm, it just whizzes by. I'd rather use konsole, but for this instance the time really adds up and I am forced to use the more primitive xterm instead."
    author: "Fred"
  - subject: "Re: The good and the bad"
    date: 2004-03-23
    body: "Sounds like something is wrong since obviously your choice of console shouldn't change what the environment variables are.\n\nAnd you can't have your cake and eat it too. Scrolling takes a long time because of the anti-aliasing. When I'm doing large compiles I usually minimize Konsole since all that text scrolling by can take a chunk of CPU."
    author: "Ian Monroe"
  - subject: "Re: The good and the bad"
    date: 2004-03-23
    body: "Try to use a non-antialiased bitmap font."
    author: "Anonymous"
  - subject: "Re: The good and the bad"
    date: 2004-03-23
    body: "But then it will look ugly like xterm. :)\n\nCouldn't the programmers implement something where it would be more conservative about how much text is displayed? My program spits out screens of data, so fast that you cannot read it. The only thing I need to know is that it is streaming out text (i.e. it hasn't stopped moving). Like a frames per second limiter or something."
    author: "Fred"
  - subject: "Re: The good and the bad"
    date: 2004-03-24
    body: "This is a good idea.\n\nWhat I could imagine is a jump-scroll mode, where instead of trying to scroll through everything, the konsole jumps to the bottom of what is visible, and just renders that.\n\nIf rendering the scrollable part (offscreen) means that this wouldn't help much, maybe limiting the scroll refresh to every 100ms or so and then jumping to the bottom would help.  I've not looked at the konsole source, but I would guess they use a QTextSomething, and that that would only render the visible part - so doing the jump thing could be quite effective.\n\n-- Steve"
    author: "Steve"
  - subject: "Konsole scroll speed"
    date: 2004-03-25
    body: "I've noticed it is slow on scrolling as well.  The effect becomes more noticable when Konsole is displaying more text, however, so when I'm doing something like compiling a kernel or something, I shrink the konsole window down fairly small, and it the text whizes by as it normally does when I'm using a virtual console directly.\n\nDuncan (who followed the link from LWN)\n"
    author: "Duncan"
  - subject: "Re: Konsole scroll speed"
    date: 2004-03-25
    body: "Can this not be fixed with programming though? In some of my circumstances, I found xterm was about 50% faster at displaying large quantities of scrolling text. I would rather use konsole here and minimizing the window is a hacky solution that would not be practical in general. "
    author: "Fred"
  - subject: "Re: Konsole scroll speed"
    date: 2004-04-14
    body: "Yep, I've noticed this too.. it's quite annoying.. rxvt is SO much faster.  \n\nIt appears as tho konsole doesn't copy the part of the screen that is the same, then render the new part when scrolling text... it just redraws the whole thing.  This is very expensive when antialias fonts are enabled.\n\n"
    author: "Davy Durham"
  - subject: "Great Interview!"
    date: 2004-03-30
    body: "great interview, and thanks to Mr. Diekmann and Mr. Doulle!\n\n-=WFA=-"
    author: "wfa"
---
 As part of the <A href="http://www.kde.de/appmonth/2004/konsole/index-script.php">March 2004 issue</A> 
 of the "Application of the month" series on  <A href="http://www.kde.de/">KDE.de</A>, Andreas C. Diekmann
 has interviewed Lars Doelle, author of 
 <A href="http://konsole.kde.org/">Konsole</A>. Konsole is what is known as an X terminal emulator and gives you the equivalent of an old-fashioned text screen on your desktop, but one which can 
 easily share the screen with your graphical applications. The <A href="http://www.kde.nl/">Dutch KDE website</A> is 
 offering an English translation of the <A href="http://www.kde.nl/apps/konsole/en/interview.html">interview</A> 
 and the <A href="http://www.kde.nl/apps/konsole/en/">overview</A> of this issue. Many thanks to Tom Verbreyt and Wilbert Berendsen for their help in translating this issue.

<!--break-->
