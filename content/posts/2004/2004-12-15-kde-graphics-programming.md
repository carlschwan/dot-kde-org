---
title: "KDE Graphics Programming"
date:    2004-12-15
authors:
  - "zrusin"
slug:    kde-graphics-programming
comments:
  - subject: "kde-imaging?"
    date: 2004-12-14
    body: "So what's on-topic on kde-imaging and what on kde-graphics-devel?"
    author: "Anonymous"
  - subject: "Re: kde-imaging?"
    date: 2004-12-14
    body: "kde-imaging is related to development of kipi (kde image plugin interface) and associated plugins (http://extragear.kde.org/apps/kipi.php). Zack has already mentioned what's on-topic for kde-graphics-devel"
    author: "pahli_bar"
  - subject: "hmm"
    date: 2004-12-15
    body: "Is there a VRML engine under KDE?\n\n"
    author: "gerd"
  - subject: "Re: hmm"
    date: 2004-12-15
    body: "Is still VRML alive? didn't it die with others late '90s hypes?"
    author: "Davide Ferrari"
  - subject: "Re: hmm"
    date: 2004-12-16
    body: "http://en.wikipedia.org/wiki/X3D"
    author: "Andre"
  - subject: "Two eye cand things I'd like to see"
    date: 2004-12-15
    body: "Three things...\n\n Log off effect\n----------------\nWhen loging out of my machine it probably takes 2-3 seconds before the thing that makes everything grey is done. It would be much more useful if the logout/end session dialog was shown right away and the effect was done in the background (would be cool if it was as smooth as when hoovering over the icons in the window title).\n\n Selection effect\n------------------\nTransparent selections with round corner - like its done when selecting icons on the gnome desktop.\n\n Less flicker in Konq\n-----------------------\nJust open konq in file browser mode and hold down F5 (or is it ctrl+r). I get flicker when hoovering over files as well. Pretty annoying.\n\n"
    author: "Mr Eye Candy"
  - subject: "Re: Two eye cand things I'd like to see"
    date: 2004-12-15
    body: "Odd.  On a 266Mhz system (Compaq Armada) and a 300Mhz system (Celeron), the grey pops in instantly.  No real flicker over files either (although on such slow systems, every once in awhile there's a grind and screen updates crawl, it is an occasional event, not the rule).\n\nObviously I'm not saying it doesn't happen to you... just that a whole 2 to 3 seconds (which is a *very* long time) seems odd compared to what I've seen.  Perhaps there are other issues at play?  Misconfigured X? "
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Two eye cand things I'd like to see"
    date: 2004-12-15
    body: "I have two machines,\n\nOne P4-3Ghz with NVidia FX (NV binary driver. This one goes most smooth)\nOne Athlon-2000 with Radeon 9200\n\nPerhaps its more like 1 - 2 seconds, but it feels that I have to wait for it... Then screen is setup @ 1600x1200\n\n"
    author: "Mr Eye Candy"
  - subject: "Re: Two eye cand things I'd like to see"
    date: 2004-12-15
    body: "Same thing here, on a P4-3Ghz with a Radeon 9000 something. The change came from KDE 3.2 to KDE 3.3. KDE 3.2 was instant gray, KDE 3.3 is slowly graying out. I always thought it was done on purpose, but I am very annoyed by it as well. Usually, when I want to log out, I don't want to wait."
    author: "Claus Wilke"
  - subject: "Re: Two eye cand things I'd like to see"
    date: 2004-12-15
    body: "Well, you don't need to wait. On my sxga+ idsplay it takes around 2-3 seconds to fade the screen to gray (kde3.3). But during this I can already use the pushbuttons to log out.\n\nregards"
    author: "Sebastian"
  - subject: "Re: Two eye cand things I'd like to see"
    date: 2004-12-16
    body: "Looks like it's really done on purpose. But this \"effect\" really looks more like the video driver lacks some optimization. That was actually the reaction of a friend who saw the KDE logout screen for the first time. \nHow about blurring (and graying out) the screen incrementally instead?"
    author: "uddw"
  - subject: "Re: Two eye cand things I'd like to see"
    date: 2004-12-16
    body: "> How about blurring (and graying out) the screen incrementally instead?\n\nThat's quite hard to do. To create a smooth animation over a few second you need, say, 25 frames, each of varying darkness, which requires a large amount of CPU."
    author: "Chris Howells"
  - subject: "Re: Two eye cand things I'd like to see"
    date: 2004-12-16
    body: ">which requires a large amount of CPU.\n\nThis is something that really should be handled by the graphics card. I s'ppose that with the composite extension for the x.org server, it's possible."
    author: "Anonymous"
  - subject: "Re: Two eye cand things I'd like to see"
    date: 2004-12-16
    body: "This does _not_ \"require a large amount of CPU\". Maybe you should ask the MPlayer people to do the programing."
    author: "hans"
  - subject: "Re: Two eye cand things I'd like to see"
    date: 2004-12-16
    body: "I think the difference between 3.2 and 3.3 is:\n\n- 3.2 would cover the screen in a 50% stipple pattern to grey it.\n- 3.3 takes a snapshot of the screen contents, tints it grey, and writes it back to the screen."
    author: "AC"
  - subject: "Re: Two eye cand things I'd like to see"
    date: 2004-12-16
    body: "The \"slow\" log off effect is intentional. That's what the fading algorithm is meant to do."
    author: "Chris Howells"
  - subject: "Re: Two eye cand things I'd like to see"
    date: 2004-12-16
    body: "I think me ment: you can fade... but please please.. when i am faster in clicking the logout button than kde is with fading.. I DO want it to quit and logout...\n\nI agree with this.\neye candy is cool, but waiting is anoying."
    author: "Mark Hannessen"
  - subject: "Re: Two eye cand things I'd like to see"
    date: 2007-04-16
    body: "This has annoyed me as well... If it is intentional, then is it possible to make it an option?\n\nThanks :-)\n\n- Bernt"
    author: "Bernt"
  - subject: "logout image"
    date: 2005-08-24
    body: "How can I change the image in the \"end session\" window? \n\nI am assuming that I will have to change the graphic file somewhere because this appears to be the one thing thatt cannot be easily customised in kde."
    author: "SA"
  - subject: "Re: logout image"
    date: 2005-08-24
    body: "Search for $KDEDIR/share/apps/ksmserver/pics/shutdownkonq.png\n\n"
    author: "cm"
---
With great pleasure I would like announce creation of the <a href="https://mail.kde.org/mailman/listinfo/kde-graphics-devel">kde-graphics-devel</a> mailing list. The list is developer oriented and will be the central place for all eye-candy development within KDE. Developers and researchers from the computer graphics field are welcomed and strongly encouraged to subscribe. Everything computer graphics related will be on topic - that includes developments within the X.org community, uses of OpenGL within a desktop environment or simply sharing your latest computer graphics research findings with others.
<!--break-->
