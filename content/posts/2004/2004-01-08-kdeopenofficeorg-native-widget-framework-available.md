---
title: "KDE.OpenOffice.org:  Native Widget Framework Available"
date:    2004-01-08
authors:
  - "jholesovsky"
slug:    kdeopenofficeorg-native-widget-framework-available
comments:
  - subject: "Native widget?"
    date: 2004-01-08
    body: "So by Native widget they mean OOo's native widgets, not KDE's like it reads?\nIs there really no other way than emulating the style?  Can the real widgets really not be used?\n\nWidgets that look like kde ones, but don't feel like it... great.\n\n"
    author: "JohnFlux"
  - subject: "Re: Native widget?"
    date: 2004-01-08
    body: "Problem is, that a different approach (to replace the OO widget set with real qt/kde widgets) would result in a near 80% rewrite of the complete OO.org office suite. It's the same with other things like \"hey, why not replace the widgets of gimp with native qt/kde ones...\"\nSo I welcome this \"emulated\" look as an important step into the direction of at least giving the visual impression of OO.org playing nicely together with kde. (though I prefer the plastik style atm, but thinkeramik is also very good)\nAnd I'm looking forward to using kdeprint directly with OO.org (and the lovely kde filedialogs)."
    author: "Thomas"
  - subject: "The post points to two different projects"
    date: 2004-01-08
    body: "One is the KDE Native Widget Framework, which is the one being released, this is as far as I can tell the real deal, bindings to _real_ KDE.\nThe other is OOo Native Widget Framework, this is only an emulation to make it look more like KDE, it doesn't really help in anyway."
    author: "spamatica"
  - subject: "Re: The post points to two different projects"
    date: 2004-01-08
    body: " Following the links, that doesn't seem to be right. My impression is that the \"KDE Native Widget Framework\"  is simply a specific implementation of an \"OOo Native Widget Framework\", i.e. it contains the code needed to make ONWF work with KDE.\n"
    author: "ac"
  - subject: "Re: Native widget?"
    date: 2004-01-08
    body: "When looking into the CVS source of the Gimp today for an answer to the pressing question 'Why can I draw a nice, precise, accurate, connected line with the Gimp, and not with my Krita brush tool', I couldn't help noticing that the Gimp's UI and core are now really neatly separated. \n\nReplacing the Gimp's interface with a Qt interface would be doable, and conceivably less work than creating a whole new image editor from scratch. Of course, it wouldn't be any fun, either, and you'd always be tagging behind the Gimp CVS to keep up, and you wouldn't have time to do something interesting and original, but it would be doable, quite doable. More doable than doing the same with OpenOffice, for sure.\n\nStill, in the end, I think it's more fun to work on KOffice, and more rewarding.\n\nBut I'm going to take a second look at that little bit of mouse handling in the gimp's displayshell. I am sure that that's the trick."
    author: "Boudewijn Rempt"
  - subject: "Re: Native widget?"
    date: 2004-01-08
    body: "Well what about the GTK-QT Theme Engine combined with those KDE filedialogs as found in the Sodipodi application  \n<p>\nAnyway there seems to be so much integrative work going on. I'm losing grip :)\n<p>\n\n*<a href=\"http://www.kde-look.org/content/show.php?content=9714\">GTK-QT Theme Engine</a>\n\n<p>\n*<a href=\"http://static.kdenews.org/mirrors/www.xs4all.nl/%257Eleintje/stuff/sodipodi-screenshots/sodipodi_savefiledialog2.png\n\">Screenshots filedialogs Sodipodi application</a>\n<p>\n\nFab\n\n"
    author: "Fab"
  - subject: "Re: Native widget?"
    date: 2004-01-08
    body: "Back a long time ago the \"Kimp\" people thought the same thing. It's logical. Just replace GTK+ with Qt. The problem was, IIRC, that the core of Gimp still assumes a GTK+ framework. For similar reasons, it was feasible to create a Gimp-plugin \"plugin\" for Krayon/Krita."
    author: "David Johnson"
  - subject: "Re: Native widget?"
    date: 2004-01-08
    body: "I think you mean 'wasn't', not 'was' :-). I've guss actually read every posting on the kimageshop mailing list by now.  And most of the Gimp source, both in its 0.99 and pre-2.0 incarnation, too. The Gimp has been cleaned up considerably by now (it has lots of GDK dependencies, of course, and that silly ersatz object-system in C, but not much direct GTK deps).\n\nNot that I'd want to just port the Gimp over to Qt. I want to make a really useful artist's app out of Krita. But you have to give GTK one thing, I rather fancy, and that's that it was created purposedly for a graphics app. GTK has very nice functions that blast a region of memory filled with RGBA bytes to the screen -- with Qt you have to muck with QImage and then convert it to QPixMap, and then bitBlt it. That hurts performance.\n\nAnyway, I'm still questing for a handy way to receive enough mouse events to draw an accurate line in Qt (i.e., not with a polyline!), and still have enough milliseconds to do some heavy computational work. But there's a chunk of code in the Gimp that's peculiarly licensed and that grabs, filters and mangles mouse events for the purpose of having enough of the buggers to draw an accurate line, and I guess that's where the trick is hidden..."
    author: "Boudewijn Rempt"
  - subject: "Re: Native widget?"
    date: 2004-01-08
    body: "Out of curiosity what do you mean by peculiarly licensed?  It's licensed so that you can't use it in KDE?"
    author: "anonymous"
  - subject: "Re: Native widget?"
    date: 2004-01-08
    body: "No, I mean that bang in the middle of a single source file there is just a small chunk of code that declares that it's X11 licensed. That's kind of weird..."
    author: "Boudewijn Rempt"
  - subject: "Re: Native widget?"
    date: 2004-01-09
    body: "That probably just means it was copied from some XFree86 library. X11 licensing is compatible with the GPL so it's really no problem. If you want to use it make sure to preserve the copyright headers + license that come with it though."
    author: "Waldo Bastian"
  - subject: "Re: Native widget?"
    date: 2004-01-08
    body: "You can use the kde printer utility by changing in the Generic Printer the command line from lpr to kprinter. The same as for gimp, Acrobat Reader and any other not too badly written program. Very useful when you want to print multiple times the same deocument with Acrobat Reader or Gimp.\n\nRichard\n"
    author: "Richard Van Den Boom"
  - subject: "Re: Native widget?"
    date: 2004-01-09
    body: ">> I prefer the plastik style atm <<\n\nHey, Thomas,\n\nsalvation is near you. -- See here:\n\nhttp://openoffice-et.sourceforge.net/pildid/OOo-1.1/ooo-plastik/ooo-plastik1.png\nhttp://openoffice-et.sourceforge.net/pildid/OOo-1.1/ooo-plastik/ooo-plastik2.png\nhttp://openoffice-et.sourceforge.net/pildid/OOo-1.1/ooo-plastik/ooo-plastik3.png"
    author: "Kurt Pfeifle"
  - subject: "Plastik OOo screenshots"
    date: 2004-01-08
    body: "Some screenshots of plastikized OOo: <A href=\"http://openoffice-et.sourceforge.net/pildid/OOo-1.1/ooo-plastik/\">http://openoffice-et.sourceforge.net/pildid/OOo-1.1/ooo-plastik/</a>\n<p>\nThanks, Jan:)"
    author: "pezz"
  - subject: "Re: Plastik OOo screenshots"
    date: 2004-01-08
    body: "Oh my ... that looks sweet ... \n\n\nFab"
    author: "Fab"
  - subject: "Re: Plastik OOo screenshots"
    date: 2004-01-08
    body: "Hmm made some screenies myself as well\n<p>\nscreenies<br>\n<a href=\"http://www.xs4all.nl/~leintje/stuff/OOo-KDE/images.html\">\nhttp://www.xs4all.nl/~leintje/stuff/OOo-KDE/images.html</a>\n<p>\nKDE-OOo with the QtCurve-V3 style (from Craig Drummond) combined with OpenOffice.org1.1 toolbar icon theme (from Rohit Kaul)\n<p>\nFab\n"
    author: "Fabrice Mous"
  - subject: "Re: Plastik OOo screenshots"
    date: 2004-01-10
    body: "> Oh my ... that looks sweet ... \n \nExcept for that strange, north scandinavian language ;-)\n"
    author: "tanghus"
  - subject: "Re: Plastik OOo screenshots"
    date: 2004-01-11
    body: "It's not scandinavian ... north european maybe, but not scandinavian.\nEstonia is not a scandinavian country."
    author: "flex"
  - subject: "Re: Plastik OOo screenshots"
    date: 2004-01-12
    body: "OK - sorry. I thought it was Finnish"
    author: "tanghus"
  - subject: "Re: Plastik OOo screenshots"
    date: 2004-01-08
    body: "I wonder if every KDE style has to be implemented seperately for OOo. \nAnyone knows how it works?"
    author: "Henning"
  - subject: "Re: Plastik OOo screenshots"
    date: 2004-01-08
    body: "All styles work out-of-box. These just happen to be done with Plastik style."
    author: "ah"
  - subject: "Re: Plastik OOo screenshots"
    date: 2004-01-08
    body: "Ah sounds good :)\nHopefully scrollbars and tabs will be using KDE styles as well."
    author: "Henning"
  - subject: "Emulated? not KDE native?"
    date: 2004-01-08
    body: "Well...  I do not like to use OOo because it is too heavy, it uses a lot of memory. I do not care if it looks like the rest of my KDE, because Itry not to use it a lot. It is too heavy for my PC.\n\nIf you are emulating kde, i guess it will consume more memory. I would prefer to have a native OOo with kde widgets...\n\nSo, for now, I use koffice, and if the .ppt is not understandable,... then i use OOo"
    author: "StR"
  - subject: "Re: Emulated? not KDE native?"
    date: 2004-01-08
    body: "The emulation works the following way: It uses QStyle to draw to a pixmap, which is then copied to the screen. Then OOo's toolkit draws the text over it. The difference between this approach and real Qt/KDE application is one bitblt operation (pixmap->screen).\n\nThe memory usage? One instance of KApplication, one instance of each widget to be \"emulated\" (QPushButton, QRadioButton, ...), and one temporary QPixmap. And of course shared Qt and KDE libraries, which anyone running KDE has in the memory anyway."
    author: "Jan Holesovsky"
  - subject: "Re: Emulated? not KDE native?"
    date: 2004-01-08
    body: "Mmm. That's quite interesting, and I'll admit to being ignorant of how this worked. Apart from running OO (which is memory intensive by iteslf) this doesn't seem to add much overhead. That's absolutely stellar work Jan."
    author: "David"
  - subject: "Re: Emulated? not KDE native?"
    date: 2004-01-08
    body: "It should also be worth to note that this is how the Qt gtk engine also works; it makes QStyle draw to a pixmap and then make either OOo or GTK blit the pixmap to screen. Very little overhead and as fast as Qt drawing the widget by itself. "
    author: "fault"
  - subject: "Re: Emulated? not KDE native?"
    date: 2004-01-08
    body: "I'm not sure whether to laugh or cry ;)\n\nEither way, that's a very intelligent hack, and hats off to all involved, if only for geek points ;)"
    author: "JohnFlux"
  - subject: "Re: Emulated? not KDE native?"
    date: 2004-01-08
    body: "ok, but must of the big vendors use OOo - and thats an important point.\n \n --- and i am using it on an daily basis.\n \n just my 2 cents ;-)"
    author: "gunnar"
  - subject: "GTK"
    date: 2004-01-08
    body: "Can we do the same with gtk?"
    author: "Gerd"
  - subject: "Re: GTK"
    date: 2004-01-08
    body: "Yes, see the Qt-GTK theme."
    author: "anonymous"
  - subject: "Re: GTK"
    date: 2004-01-08
    body: "As Fab above already pointed out:<br>\n<a href=\"http://www.kde-look.org/content/show.php?content=9714\">\nhttp://www.kde-look.org/content/show.php?content=9714</a>\n"
    author: "cloose"
  - subject: "Merge with crystal icons"
    date: 2004-01-09
    body: "This is excellent work.  Would it be possible to merge the work with crystal icons by default?\nhttp://www.kde-look.org/content/show.php?content=7131\n\nscreen-shot:\nhttp://www.kde-look.org/content/preview.php?file=7131-1.png\n\nThat would really complete the \"kde look\"!\n"
    author: "anon"
  - subject: "Re: Merge with crystal icons"
    date: 2004-01-11
    body: "Yeah something what Texstar has already done with pclinuxos ...\nCheck out:\n\nhttp://www.pclinuxonline.com/pclos"
    author: "Kanwar"
---
A development version of the 
<a href="http://kde.openoffice.org/nwf/index.html">OOo KDE Native Widget Framework</a> is now available for
download. So far, it can draw KDE-styled push buttons, radio buttons,
check boxes and list boxes (<a href="http://kde.openoffice.org/files/documents/159/1555/OOo-KDE-NWF-check_radio_list.jpg">screenshot1</a>, <A href="http://kde.openoffice.org/files/documents/159/1554/OOo-KDE-NWF-listboxes2.jpg">screenshot2</a>, <a href="http://openoffice-et.sourceforge.net/pildid/OOo-1.1/ooo-plastik/">Plastik</a>). The <a href="http://people.redhat.com/dcbw/">OOo Native Widget Framework</a> is a way to get the look of the host platform in 
<a href="http://www.OpenOffice.org/">OpenOffice.org</a>. 
It does not affect the feel, because real KDE widgets are not used; the framework simply uses the <a href="http://doc.trolltech.com/3.2/qstyle.html#details">QStyle API</a> to draw its widgets the same way KDE/Qt would. It is currently developed for OOo 1.1, but it will be used in 2.0 as well.





<!--break-->
<p>A <a href="ftp://ftp.kde.org/pub/kde/devel/OOo_1.1.0_kde_2004-01-06_LinuxIntel_Install.tar.gz">binary snapshot (i386 only)</a> [85 Mb] is available.
</p>
<p>
Please note that it is a development version and that it contains bugs;
for example editable list boxes do not render correctly at the moment and you may need to unset your SESSION_MANAGER environment variable before running "setup" or "soffice".
</p>




