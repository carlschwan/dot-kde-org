---
title: "KDE-CVS-Digest for April 16, 2004"
date:    2004-04-17
authors:
  - "dkite"
slug:    kde-cvs-digest-april-16-2004
comments:
  - subject: "The snapshot url link is missing"
    date: 2004-04-17
    body: "First of all, thanks to Derek for the wonderful reporting of KDE development activity. \n\nI have noticed that there is no url link to any Linux Desktop. Where is it ?"
    author: "Pablo Pita"
  - subject: "KPDF and a bit of self propaganda"
    date: 2004-04-17
    body: "KPDF had thumbnails added this week too. Here is an example."
    author: "Albert Astals Cid"
  - subject: "Re: KPDF and a bit of self propaganda"
    date: 2004-04-17
    body: "Thanks Albert! Good to see someone working on KPDF again. It still needs some work. \n\nAre you also working on the font problems KPDF has (BR#67950)? That would be awesome!\n\nChristian"
    author: "Christian Loose"
  - subject: "Re: KPDF and a bit of self propaganda"
    date: 2004-04-17
    body: "kooool.\nDose it now support smooth continuous view of the pages and bookmarks? If yes, who needs acroread any longer?"
    author: "panzi"
  - subject: "Re: KPDF and a bit of self propaganda"
    date: 2004-04-17
    body: "Anyone who needs ECMAscript and forms, unfortunately. But kpdf is great, yes ..."
    author: "Lurchi"
  - subject: "Re: KPDF and a bit of self propaganda"
    date: 2004-04-17
    body: "I haven't tried KDE 3.2 yet. Does KPDF support searches and hyperlinks?"
    author: "Helder Correia"
  - subject: "Re: KPDF and a bit of self propaganda"
    date: 2004-04-18
    body: "no...And it's one of its biggest problems..."
    author: "SF"
  - subject: "Re: KPDF and a bit of self propaganda"
    date: 2004-04-18
    body: "I was really disappointed by KPDF.  It doesn't have any features to make it better than KGhostView, and it often renders PDFs wrong or at least very ugly.  What's the point?"
    author: "Spy Hunter"
  - subject: "kresources"
    date: 2004-04-17
    body: "I have a few kdepim-questions:\n\nHave people thought about creating an sql-resource for kaddressbook, korganizer, notes and todos? Wouldn't that be a very cool way of sharing contact and calendar data? I always wonder why the kolab/kdepim developers decided to store this information in imap-folders. Isn't it a little weird to create emails for every contact or appointment?\n\nIs there a way to EDIT addresses with kaddressbook that are in an LDAP-database?"
    author: "me"
  - subject: "KSVG2 ?"
    date: 2004-04-17
    body: "KSVG2? What? What happened to KSVG?  Most users still probably believe that KDE doesn't have SVG support since most of the icons are still .png (even when svg ones exist -- and .pngs are added every once and a while still!) and konq doesn't like to view SVG's some times.\n\nWill KSVG2 be able to render more svgs, be faster, or just be easier to maintain? I'm just curious and surprised that's all :) I guess I didn't see the move to a new ksvg so soon ...\n\nAlso, will a general purpose svg drawing canvas be availiable in the future? Not a canvas for editing svg's (karbon?) but one for applications like Umbrello or KPresenter ;) to use to do its diagrams and presentations?"
    author: "Jim"
  - subject: "Re: KSVG2 ?"
    date: 2004-04-18
    body: "KSVG2 (AFAIK) is currently an incomplete port/rewrite of KSVG to the new DOM implementation ( KDOM). \n\nThis new DOM implementation is being done because the KHTML DOM has some problems... \n\nIs it possible KHTML would eventually switch to this new DOM too? "
    author: "rjw"
  - subject: "Congratulations to KDE..."
    date: 2004-04-19
    body: "... for being the first OS to actually make use of the <host> part of the file protocol."
    author: "LuckySandal"
  - subject: "Re: Congratulations to KDE..."
    date: 2004-04-19
    body: "1. KDE is not an OS\n2. Microsoft has used the host part of the file protocol forever, and this is only being done for compatibility\n3. The idea of accessing remote files with the file protocol is retarded, especially when it is irreversibly tied to Microsoft's awful filesharing protocols.  The file protocol should only be for accessing files on the filesystem.  If you want to use a filesharing protocol, you should put its name in the protocol field of the URL, since that's what it's there for.\n\nI agree that the compatibility benefits are good, so this change helps KDE as a whole.  But that's the only good reason to do this."
    author: "Spy Hunter"
  - subject: "Re: Congratulations to KDE..."
    date: 2004-04-19
    body: "Actually I don't think M$ does support the <host> either. You access SMB shares like \\\\host\\share, but I doubt file://host/share would even work."
    author: "LuckySandal"
  - subject: "Re: Congratulations to KDE..."
    date: 2004-04-19
    body: "Actually on mosaic this used to open an ftp connection, I can't remember what netscape 1.x used to do but it might well have done this too.\n\nRich.\n"
    author: "Richard Moore"
---
In <a href="http://members.shaw.ca/dkite/apr162004.html">this week's KDE CVS-Digest</a>:
<A href="http://xmelegance.org/kjsembed/">KJSEmbed</A> adds shell calls and now builds with Qt. 
<A href="http://www.kdevelop.org/">KDevelop</A> has a new documentation viewer, with bookmarks,
printing, plugins and full text search. 
KSVG2 ecma support added. 
<A href="http://kdepim.kde.org/components/knotes.php">KNotes</A> is now network enabled. 
<A href="http://www.konqueror.org/">Konqueror</A> gets an enhanced version of caret mode. 
<A href="http://kopete.kde.org/">Kopete</A> supports KIMproxy, the generic IM interface. 
Many bugfixes in <A href="http://developer.kde.org/~wheeler/juk.html">Juk</A>,  <A href="http://kate.kde.org/">Kate</A>, <A href="http://uml.sourceforge.net/">Umbrello</A> and others.


<!--break-->
