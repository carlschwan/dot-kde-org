---
title: "A new Wiki for PyQt and PyKDE"
date:    2004-05-06
authors:
  - "tmarek"
slug:    new-wiki-pyqt-and-pykde
comments:
  - subject: "Python or Ruby"
    date: 2004-05-06
    body: "I think it'd be interesting to get some comments from people using Python or Ruby to create KDE programs. I'm in the market for a higher-level language to use besides C++. As I don't have any preference towards Python or Ruby yet, having used neither, I'd be interested in comments on for example the KDE bindings for both languages."
    author: "Apollo Creed"
  - subject: "Re: Python or Ruby"
    date: 2004-05-06
    body: "I'm not sure about how many people have used the ruby bindings yet, as they only were first released as part of KDE 3.2. Whereas PyKDE has been around for longer, so you're more likely to get feedback/help from other people. I'm as interested as anyone to hear how they've got on with either the QtRuby or Korundum bindings.\n\nI think both PyKDE and Korundum are very complete, and you can use Qt Designer with both.\n\nThis week I've added a '-kde' option to the rbuic Qt Designer tool, so it can generate KDE Korundum ruby code with KDE widgets, and run it straight away:\n\n$ rbuic -kde -x  kspellui.ui -o kspellui.rb\n$ ruby kspellui.rb\n\nHere is recent ruby oriented blog here 'KApplication or KDE::Application?':\nhttp://www.kdedevelopers.org/node/view/457\n\nBut it really needs more publicity with things like online tutorials like the ones Roberto Alsina does or Ian and Rich do with the KJSEmbed stuff.\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: Python or Ruby"
    date: 2004-05-06
    body: "I have not released any KDE or QT app, I only use PyQT for small apps I need. I can only talk about that since I don't know Ruby at all.\nI have found PyQT bindings very well advanced and, combined with the huge number of optimized modules available for Python, the simplicity of the language itself and of the Object implementation, I could code all the applications I needed using this. I found the results almost as responsive as regular QT apps, with a memory footprint in the same range as them.\nI could not recommend enough this toolkit for people wanting to create very quickly graphical interfaces for specific applications. Especially since it is possible to convert automatically QT Designer ui file to Python scripts.\n\nBest regards,"
    author: "Richard Van Den Boom"
  - subject: "Re: Python or Ruby"
    date: 2004-05-07
    body: "I use PyQt to make quick and dirty little custom apps for Opie.  Opie is the Open Source handheld interface based on Qtopia (kind of like KDE is on Qt, although Opie is much more similar to the base Qtopia).\n\nIt's all very specific stuff like cast lists and timed check lists for stageshows, or character profiles and interrelations for RPGs I'm participating in.  It's very handy to be able to use Qt Designer on a computer, lay out the UI, add a bit of Python code, and then copy it over and finesse it on the actual handheld. "
    author: "Evan \"JabberWokky\" E."
  - subject: "Why so many wiki"
    date: 2004-05-06
    body: "That's great news, but why not use the wiki of wiki.kdenews.org ? I am concerned than scattering the informatino makes is more difficult to find."
    author: "Philippe Fremy"
  - subject: "Re: Why so many wiki"
    date: 2004-05-06
    body: "I agree with Philippe on this one. KDE Wiki these days look really sharp. \n\nFab"
    author: "Fabrice Mous"
  - subject: "Re: Why so many wiki"
    date: 2004-05-06
    body: "That also begs the question why isn't the wiki at wiki.kde.org?"
    author: "anon"
  - subject: "Re: Why so many wiki"
    date: 2004-05-06
    body: "Which leads to the question  \"why isn't pykde a part of the cvs kdebindings \nmodule\" ? \n\nI asked this question before and the quick answer is simple: it means some work\nand somebody has to do it. So far nobody volunteered IIUC, and I don't have\nthe time myself. The long answer has to do with how the bindings are generated,\netc, it isn't trivial to keep the bindings in sync, in part because they \ndepend on pyqt, which would also need to be maintained in sync. Maybe\na pyqt-copy or something like this. I hope someone with the time and talent\nto coordinate these efforts will step up. \n\n:-)"
    author: "MandrakeUser"
  - subject: "Re: Why so many wiki"
    date: 2004-05-06
    body: "You probably missed this recent proposal:\n\nhttp://dot.kde.org/1081064385/1081096290/1081097679/\n\nI don't see a problem in using a separate Wiki for PyQt/PyKDE. There are already pages on the PythonInfo Wiki at\n\nhttp://www.python.org/cgi-bin/moinmoin/PyQt\n\nbut it's no problem to link between the two. Wikis make this easy. :-)"
    author: "Anonymous Custard"
  - subject: "Re: Why so many wiki"
    date: 2004-05-06
    body: "> You probably missed this recent proposal:\n> http://dot.kde.org/1081064385/1081096290/1081097679/\n\nHey, this is GREAT news, THANKS for the good news !!!"
    author: "MandrakeUser"
  - subject: "OT: Haskell Bindings"
    date: 2004-05-07
    body: "GTK and WxWindows bindings for Haskell are available, but I'd really like my programs to look nice on my desktop ;) Anyone planning on taking a shot at this?"
    author: "John Haskell Freak "
---
In order to create a community platform, we have set up a <a href="http://www.diotavelli.net/PyQtWiki">wiki entirely devoted to Python GUI development</a> with PyQt and PyKDE. So if you don't know anything about it, the time might be great, because a new version of PyKDE supporting the KDE APIs up to 3.2.2 is currently in Beta stage - and a release is coming soon! In the wiki, you will find links to tutorials on how convenient Qt or KDE programming can be without C++, no matter if you use it for rapid prototyping or for the actual applications. And maybe you never read about <a href="http://www.die-offenbachs.de/detlev/eric3.html">eric3</a>, the Python IDE entirely written in and for PyQt?


<!--break-->
<p>Anyway, what we need <em>right now</em> for the wiki to get successful, is, surprisingly enough, more content. Thus, if you have links, tutorials, howtos, articles about GUI design and techniques of GUI development (be it Qt our in general), resource pointers, useful or peculiar code snippets or a program written in PyQt/PyKDE, please visit our site and share your knowledge (resp. the source)!</p>

