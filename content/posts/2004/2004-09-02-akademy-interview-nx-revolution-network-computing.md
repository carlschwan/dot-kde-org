---
title: "aKademy Interview: NX - Revolution of Network Computing?"
date:    2004-09-02
authors:
  - "pipitas"
slug:    akademy-interview-nx-revolution-network-computing
comments:
  - subject: "Nice technology"
    date: 2004-09-02
    body: "They should patent it... hehe\n\n(Yes I know http://www.ffii.org)"
    author: "Jan"
  - subject: "Question...!"
    date: 2004-09-02
    body: "Can the FreeNX and kNX initiatives be looked at as a replacement of LTSP? According to what I have read, FreeNX and kNX require that the client have a window manager like KDE or GNOME. That is, a client cannot be thin!. If that is the case can FreeNX and kNX be hacked somehow so that they can be functional on a disk-less workstation much like the situation one finds in the case of the LTSP? This would be very very useful. As things stand now, it's only fat clients that can make use of these tools whereby they would be accessing services of systems that are far more powerful. This does not solve one of the major problems I am facing like noise, power consumption and general maintenance related to computer systems with moving parts.\n\nI also wanted to know whether a configuration can be made where a single system that can boot GNOME or KDE can be made to provide either environments depending on a user specifications.\n\nCb.. "
    author: "charles"
  - subject: "Re: Question...!"
    date: 2004-09-02
    body: "You might want to take a look at the following article:\n\nNoMachine NX Client now available for Thinstation users\nhttp://www.nomachine.com/news_read.php?idnews=122"
    author: "Christian Loose"
  - subject: "Re: Question...!"
    date: 2004-09-02
    body: "Yes.  pxes.sourceforge.net\n\nYou have a PXE bootable ethernet adapter, and configure your server to push a PXES image over via tftp.  This image will include the NX client to connect to your NX server running KDE. \n\nCheers"
    author: "nobodyofinterest"
  - subject: "In KDE 3.4?"
    date: 2004-09-02
    body: "Will support for NX be built-in in KDE3.4? BTW: does it support drag and drop? I mean, suppose I'm running KDE and make a NX-connection to another KDE-machine. Could I copy files between the sessions by simply dragging a file from the \"native\" KDE-session and dropping it in to the desktop of the NX-session?\n\nI'm really excited about NX! At work, I have to endure Windows XP. With NX, I could use my beloved KDE-desktop at work as well. Just connect to my home-machine using NX and I would be all set! I'm just waiting for the FreeNX-packages to appear in Gentoo...."
    author: "janne"
  - subject: "Re: In KDE 3.4?"
    date: 2004-09-02
    body: ">>> Will support for NX be built-in in KDE3.4? <<<\n----\nI guess so.\n\n>>> BTW: does it support drag and drop? <<<\n----\nYes.\n\n>>> Could I copy files between the sessions by simply dragging a <<<\n>>> file from the \"native\" KDE-session and dropping it in to the <<<\n>>> desktop of the NX-session? <<<\n----\nYes.\n\n>>> I'm really excited about NX! <<<\n----\nCalm down. It wont go away.  ;-)"
    author: "pipitas"
  - subject: "It is available in Gentoo"
    date: 2004-09-02
    body: "According to \"http://packages.gentoo.org/search/?sstring=FreeNX\" Free NX server is available in gentoo."
    author: "FreqMod"
  - subject: "Re: It is available in Gentoo"
    date: 2004-09-02
    body: "Yep, there seems to be preliminiary packages available. But I'm using AMD64, and it's not available for it yet. Well I could use the provided package with some hacking, but I can wait for a while longer"
    author: "Janne"
  - subject: "Comments by anon user"
    date: 2004-09-03
    body: "Do the comments at the bottom of \nhttp://wiki.kde.org/tiki-index.php?page=Remastering%20Knoppix%20and%20Free%20NX%20Demonstrations&comzone=show#comments\nand \nhttp://wiki.kde.org/tiki-index.php?page=NX/FreeNX%20integration%20into%20KDE&comzone=show#comments\nabout the validity of the speed comparison have any merit? "
    author: "cm"
  - subject: "Re: Comments by anon user"
    date: 2004-09-03
    body: "He apparently didn't try it out himself, that's all."
    author: "Datschge"
  - subject: "Re: Comments by anon user"
    date: 2004-09-03
    body: "Well, I wouldn't want to have a nice new technology advertised with incorrect arguments, no matter how good the real acceleration is.  \n\nBut I cannot tell, and I haven't even tried it out yet. "
    author: "cm"
---
In their own series of <a href="http://conference2004.kde.org/">aKademy</a> interviews, the German IT news website <a href="http://www.golem.de/">Golem.de</a> has talked to KDE contributors Fabian Franz (also a member of the <a href="http://www.knoppix.org/">Knoppix</a> development team) and Kurt Pfeifle about FreeNX. Now <a href="http://www.osnews.com/">OSnews</a> <a href="http://osnews.com/story.php?news_id=8139">carries the English translation</a>. NX, developed by <a href="http://www.nomachine.com/">NoMachine.com</a> aims for nothing less than to revolutionize network computing. The software allows to connect and work on remote desktops even across low bandwidth links such as ISDN or modems. While at its core it speed-boosts X11 connections, as an addon it also can accelerate VNC/RFB and Windows Terminal Server/RDP sessions by a factor of 2 or more.


<!--break-->
<p>Fabian and Kurt had initially presented <a href="http://www.kalyxo.org/">FreeNX</a> as a preview <a href="http://dot.kde.org/1088363665/">during LinuxTag2004</a>. FreeNX is a Free Software NX Server based on the GPL'd libraries from NoMachine. During the aKademy week they released FreeNX Server for the first time as a public snapshot (which also ships with <a href="http://www.knopper.net/knoppix-mirrors/index-en.html"><i>Knoppix-3.6 aKademy Edition</i></a>). The license is GPL. Debian FreeNX packages are hosted at <a href="http://www.kalyxo.org/">Kalyxo.org</a>.</p>

<p>
Also, a first working 0.1 version of the FreeNX <i>kNX Client</i> is now checked in into kdenonbeta. Fabian, Aaron J. Seigo and Joseph Wenninger are working on a deep and seamless integration of the new NX Client into the KDE framework, utilizing DCOP, KParts, kio-slave and KWallet technologies and integrating it with the existing KDE Remote Desktop Connection utility.</p>

<p>
The FreeNX and kNX initiatives will bring efficient, lean and high speed Terminal Services to KDE which work well even over very distant and low bandwidth links. They will connect KDE workstations with multiple Operating System platforms and enable users to seamlessly operate their favorite or required applications, regardless on what platform they run. Overall they will make Linux desktop migration plans significantly easier to implement and give a big boost to KDE adoption as the desktop environment of choice for corporate and enterprise usage.</p>

<p>The <a href="http://osnews.com/story.php?news_id=8139">OSnews piece</a> has some nice examples of NX/FreeNX use cases outlined by Kurt and Fabian.</p>