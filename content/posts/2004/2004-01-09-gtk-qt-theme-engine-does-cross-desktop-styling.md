---
title: "GTK-Qt Theme Engine Does Cross-Desktop Styling"
date:    2004-01-09
authors:
  - "fmous"
slug:    gtk-qt-theme-engine-does-cross-desktop-styling
comments:
  - subject: "This indeed is a great idea!"
    date: 2004-01-09
    body: "One of the things that will yield the most results in terms of unification. There were some nice themes which had GTK ports, such as QtCurve, but they don't compare to this, this completely eliminates the need to even have a GTK port and is likely to be far more accurate in terms of appearance for both toolkits once it matures a little more.\n\nGreat job and Great idea!"
    author: "Alex"
  - subject: "Re: This indeed is a great idea!"
    date: 2004-01-09
    body: "Is it? I can't comment on the technical achievement, which is presumably pretty high. But from the design perspective it's helping to create a monster.\n\nIf you want to keep a future desktop understandable and maintainable, in short, if you do not want the kind of thing that made Windows so horrible, then it is neccessary to keep a clean, simple design. What's currently happening is the complete opposite. The free desktop is on its way to become a huge, bloated monster with components written in mostly unrelated languages and libraries. A system that no sane person can understand, because it requires knowledge in all the different technologies that are used. What happens when there is a difficult-to-find bug in a system that integrates Qt and GTK (and maybe even more)? There are few people who know one of them very well, but you will not find anone who knows both.\n\nThere is a short-term benefit, you have more applications from the 'other' toolkit (even if most of them just duplicate the applications of the other toolkit). But what is the long-term price?"
    author: "AC"
  - subject: "Re: This indeed is a great idea!"
    date: 2004-01-09
    body: "Stop crying...\n\nThe GTK/GNOME-app and OOo replacing projects (KOffice, Kontact, Karbon, etc.) are also on their way! If you dont like Davids solution then put your efford/money in the just mentioned projects.\n\nFor me:\nI love it... thanks David!\n\nP.S.: KOffice 1.3 and Kontact look very promissing, check it out!"
    author: "cies"
  - subject: "Design perspective?"
    date: 2004-01-09
    body: "Mr. design perspective, this is just a way to integrate legacy apps and gnome apps into the desktop. KDE itself is extremely well designed, arguably a lot better than Windows or GNOME.\n\nThe gtk-qt theme as well as the OOo theme are only there if you must use an old app and don't want it to look different from the rest of the apps. If you start to develop a new app, you would always use native KDE. (unless you prefer C)\n\nIt is also not hard to debug, as you can simply try out the old apps without the theme integration and see if the bug persists. Also of one person can write this theme integrator in 2 weeks, it can't be extremely complex.\n\nSo there is no longterm price to pay. This is optional and GREAT.\n"
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: This indeed is a great idea!"
    date: 2004-01-09
    body: "I disagree with you - the design here IS clean and simple.  If anything it reduces the complexity of the \"free desktop\".\n\nTake for example the Bluecurve (Redhat) and Galaxy (Mandrake) themes.  Each consists of three individual components - the KDE style, the GTK1 theme, and the GTK2 theme.  These are all seperate pieces of code, so each will behave slightly differently.  If you wanted to modify, or fix a bug in the style, you would need to change all three components.  Using the GTK-QT engine, there is only one piece of code doing the drawing (the QT style).  A bug would only need to be fixed in one place.\n\n\"The free desktop is on its way to become a huge, bloated monster with components written in mostly unrelated languages and libraries.\"\n\nThat's the situation now - themes are being written in C and C++, using GTK and QT.  I'm trying to resolve this by unifying the theme interface.\n\n\"There are few people who know one of them very well, but you will not find anone who knows both.\"\n\nExactly, so why not concentrate on wrappers that allow you to write themes in the language you know well (QT), and have them applied to the other (GTK) automatically."
    author: "David Sansome"
  - subject: "Re: This indeed is a great idea!"
    date: 2004-01-09
    body: ">That's the situation now - themes are being written in C and C++, using GTK\n>and QT. I'm trying to resolve this by unifying the theme interface.\n \nBut right now when you are developing Qt you do not need to care about GTK. At least not more than you care about Mac OS. It is a different world. \nBut combining both is like combining Mac OS and Windows.\n\n> Exactly, so why not concentrate on wrappers that allow you to write themes in\n> the language you know well (QT), and have them applied to the other (GTK)\n> automatically.\n\nWha do you do when you get an error that can only reproduced when both are combined? Like a GTK component modifying the file descriptors used by the Qt component."
    author: "AC"
  - subject: "Re: This indeed is a great idea!"
    date: 2004-01-09
    body: "Dude, this is not about a merger of KDE with GNOME, this is simply a GTK style which relies on the Qt style, nothing less and nothing more. If you didn't use GTK/GNOME or KDE/Qt apps anyway nothing changed for you at all. However if you did mix both already you now have a way to unify the look without having to care about only using styles which are available for both Qt and GTK. Get a life."
    author: "Datschge"
  - subject: "Dave's IO sleve wish will become reality soon..."
    date: 2004-01-09
    body: "Hi, David,\n\nfirst let me say that this is an awesome headstart! Your success fits very nicely with one of the new themes of KDE development: going from the *integrated* Desktop Environment towards an *integrative* one. \n\nThere is quite some work going on in that respect -- and your work and the quick results also came as a hefty surprise to some of us. It has inspired many KDE developers to hack on the other fronts that makes KDE embracing more of the best \"foreign\" applications (without giving up its own inner integration). I hope to see more from you going into KDE proper!\n\n> I'd also like to see other areas of the Linux desktop \n> unified - simple things..... as well as the more complex \n> technologies, like IO slaves ...\n\nStay tuned and wait a few more days -- you will not miss the announcement when it's here. It is not very long. The KDE X-Mas period is not yet over, and presents are still coming in....\n\n;-)"
    author: "Kurt Pfeifle"
  - subject: "Re: Dave's IO sleve wish will become reality soon..."
    date: 2004-01-09
    body: "Kurt, that sounds very interesting :)\n\nI can't wait."
    author: "David Sansome"
  - subject: "an idea(s)"
    date: 2004-01-09
    body: "gtk themes (their associated gtkrcs) can set the stock icons that gtk uses.. it'd be cool if this engine could use KDE's instead. This would perhaps rely on linking to kdelibs instead of just Qt; but it'd be cool. \n\nFor further intregation, it'd also be nice to set various properties for gtk widgets based on their Qt equivalents (qstyle's stylehints, pixelmetrics, etc..)"
    author: "fault"
  - subject: "Re: an idea(s)"
    date: 2004-01-09
    body: "Do you know of any way of setting these properties in the theme engine itself (ie. with C code, not in the rc file)?"
    author: "David Sansome"
  - subject: "Re: an idea(s)"
    date: 2004-01-12
    body: "Have a look at the source code ofg my GtkQt - which sets the Gtk style based upon the KDE style. In this I set things such as the icon-sizes, and toolbar settings. Using similar code you can set the icons as well. (Although in my GtkQt I just generate a gtkrc file - as this works for me!)\n\nhttp://www.kde-look.org/content/show.php?content=6954"
    author: "Craig"
  - subject: "Tiny correction"
    date: 2004-01-09
    body: "\"make is incredibly simple to integrate Qt event loop into a GTK+ application for the application authors\"\n\nshould be \"make it incredibly simple to integrate Qt event loop into a GTK+ application for the application authors\"\n\nThis is meant for this page:http://developer.kde.org/documentation/tutorials/qtgtk/main.html\n\nI just discovered it now because it was linked on OSNEWS.com\n"
    author: "Alex"
  - subject: "freedesktop.org"
    date: 2004-01-09
    body: "I think that all this efforts are perfect to take a place on freedesktop.org.\n\nFor further integration, integrativeness adopting \"independent\" standart, API, icons, themes on both KDE and Gnome (and XPDE and XFce of course) is the \"standart\" way of standartization.\n\nThis effort is a \"big step\" towards more tight integration accross all Free Desktops and Toolkits (thanks Dave), this will increases the Freedom...\n\nI envision very soon a chance to be possilbe to write a GTK app (or wxWindows one) that integrates nicely in KDE (from what I read already is possible GTK apps to use standart KDE file & print dialogs, and now we have themes) .. if the KDE libs are made avalialbe to other desktops, desktop frameworks and toolkits (like KParts, Kiolsaves etc) one can choose whatever api & kit & tools and be sure it will integrate.\n\nFor example if one GTK app uses KDE file & print dialogs, draws in KDE style & theme, uses \"standart\" kde (or shared freedesktop.org) icons, uses KIOSlaves and KParts i must say it will be in fact KDE APP!\n\nDave, tell me this (having in mind that your efforts and suggestions are in this direction): do you think it's possilbe (technologically) to produce in future a GTK KDE app?"
    author: "Anton Velev"
  - subject: "Re: freedesktop.org"
    date: 2004-01-09
    body: "i think a Linux price shall be a awarded to this effort!\nWonderful progress.\nFreedesktop shall also put it on their homepage."
    author: "Gerd"
  - subject: "The Screenshot Could Have Been More Impress"
    date: 2004-01-09
    body: "IMHO, the screenshot would have been a touch more impressive if style wasn't ThinKereamik.  Can't we already achieve these results simply by using ThinGeramik?\n\nBTW, GTK-Qt sounds great, I am sure it will be very successful."
    author: "nonamenobody"
  - subject: "A lillte question.."
    date: 2004-01-09
    body: "What happens if a GTK widget doesn't exists in QT Style? "
    author: "Rossend Bruch"
  - subject: "Hope someone does the other way, too"
    date: 2004-01-09
    body: "I'm primarily a GNOME user, although I also fully support the KDE project's existence and goals, and recognise that it's better for many users. I've been hoping for projects like this for some time, and one hope I have is that someone does something for the other direction: making KDE apps look right according to my currently configured GTK theme.\n\nI also hope that when this is done, the two projects cooperate to make sure that you don't get the inevitable infinite-loop crash when Qt is configured to use the \"GTK\" theme and GTK is configured to use the \"Qt\" one."
    author: "Stuart"
  - subject: "Re: Hope someone does the other way, too"
    date: 2004-01-09
    body: "> I also hope that when this is done, the two projects cooperate to make sure\n> that you don't get the inevitable infinite-loop crash when Qt is configured\n> to use the \"GTK\" theme and GTK is configured to use the \"Qt\" one.\nLOL :-)"
    author: "Thomas"
  - subject: "I second that!"
    date: 2004-01-11
    body: "...though I use XFce4 (my box is kinda underpowered, Cel266/128M). Would be wonderful to have an engine that does such things for Qt/KDE apps. On the other hand, Bluecurve/Galaxy are our friends :-)"
    author: "Temcat"
  - subject: "Forked in Spirit"
    date: 2004-01-09
    body: ">>I'd also like to see other areas of the Linux desktop unified  \n  \nI find this whole 'desktop unifying' very interesting generally. Let me dwell into the realm of philosophical here. This 'unification process' suggest there is no unity in both technical and philosophical aspects, where their 'conflict' stands somewhat in the way of 'mass' adaption for any of the two desktops (in relation to inconsistence between the two). Such it is that when the hacker was free to develop a desktop he/she prefers, two biggest would emerge with conflicting interests. Both recognize this and go on a journey of unification - to some extend.  \n  \nWhatever both 'spirits' will do, they _will_ create the best desktop build around their own philosophy and interests plus unifying code and features along the way. But the interesting question is, at what point does the implementing of same features on both desktops (double effort) become so inneffective that some kind of 'grand merge' is inevitable?  \n  \nThe difference between these two desktops is technical, but more importantly: philosophical. Both philosophies will remain and eventually it will be the spirits of the hackers that will either prevent or introduce the 'true unification' both users and coders vocally ask for.  \n  \nThe most bi-polar 'actors' playing behind this unification process are guided by a hacker spirit and a corporate spirit. Both are journeying to see their interest flourish on an operating system. The unifycation of these desktops, and only theirs, can mean true unifycation across the whole desktop. To align to the same interest, is to unify.  \n  \nThis 'struggle' for unification goes beyond the Desktop interestingly! There is for example the area of kernel modules.  \n  \nAt any point, to act out of shared interests, is key to progress in the right direction. And this is what you can see emerge in threads on KDE-news these days, progress in the right direction."
    author: "ac"
  - subject: "Re: Forked in Spirit"
    date: 2004-01-10
    body: "\"Whatever both 'spirits' will do, they _will_ create the best desktop build around their own philosophy and interests plus unifying code and features along the way. But the interesting question is, at what point does the implementing of same features on both desktops (double effort) become so inneffective that some kind of 'grand merge' is inevitable?\"\n\nA merge would be bad. Having both KDE and Gnome projects provides competition. As each project experiments with different but similar ideas and creates different perspective on what does and does not work that experience can be taken to freedestop.org and used to help create better standards. Accessibility, mime type management, window notification, file alteration, etc... \n\nAs these standards get created and used integration becomes easier, but the need for choice is not reduced any.\n"
    author: "Don"
  - subject: "Competition for the sake of competition is useless"
    date: 2004-11-16
    body: "We already have competition from Mac OS and Windows. There's no need for infighting in the free software world.\nI long for the day when the two merge."
    author: "Sunnan"
  - subject: "Re: Competition for the sake of competition is useless"
    date: 2004-11-16
    body: "You are perfectly right. GNOME developers (just like everyone else) are always welcome (and as such should be forced) to take part in KDE development. Sadly many of them just think that both throughout configurability and using a truly object oriented programming language are both bad ideas. </sarcasm>"
    author: "ac"
  - subject: "Re: Competition for the sake of competition is use"
    date: 2005-09-13
    body: "Throughout configurability? You mean that horrible Control Center thing kde has? No thanks, i don't like to spend half an hour looking through loads of options to change 1 setting. I hated that control panel ever since the first time i used it. Gnome is on the right way, dropping useless options from the GUI that 99% of its users dont need, and making them only configurable through gconf."
    author: "Mephisto"
  - subject: "wow"
    date: 2004-01-10
    body: "I just love how those guys keeps surprising me every day!\nThere are no problems without solution for people like them that make it happen!\n\nAnd there are some who say there's no innovation in open source... hehehe.\n\nThanks guys!"
    author: "Iuri Fiedoruk"
  - subject: "Does this make sense?"
    date: 2004-01-11
    body: "Why wasn't the project a translation layer forQT themes to GTK+?  I mean this is an open source project isn't it?  Hopefully the author is aware that QT is only GPL'd for linux?  If you wanted to port this software to another platform, it would suddenly mean the user using it on the other platform would require a license. Oh well, I suppose open source means different things to different people."
    author: "AC"
  - subject: "Re: Does this make sense?"
    date: 2004-01-12
    body: "What a troll:\nQT/X11 is GPL/QPL and it runs at least on AIX, FreeBSD, HP-UX, IRIX, Linux, Solaris, Tru64, Unixware 7, OpenUnix 8. Yes, you do need a license when running on Windows, but then again: you need a license when running Windows anyways..."
    author: "Johannes Wilm"
  - subject: "Re: Does this make sense?"
    date: 2004-01-12
    body: "Not to mention that the GPL'ed Qt also works on OS X and when running on OS X or Windows you want to use native widgets (or as close as you can fake it), not GTK or Qt themes."
    author: "Scott Wheeler"
---
The <A href="http://www.kde-look.org/content/show.php?content=9714">GTK-Qt theme engine</A> is a nifty hack for GTK+ applications that uses the currently selected KDE/Qt style to do its drawing  in a very similar fashion to the <a href="http://dot.kde.org/1073557624/">recently announced</a> KDE Native Widget Framework for OpenOffice.org.  Basically, what this means is that it will make your GTK apps look just like KDE/Qt ones and hence integrate better into your desktop (<a href="http://www.kde-look.org/content/pics/9714-1.png">screenshot</a>).  I contacted the author <a href="mailto:me at davidsansome.com">David Sansome</a>; it resulted in the following small interview.
<!--break-->
<p><strong>Please introduce yourself.</strong></p>

<p>I'm David Sansome, a college student living in London, UK, and I work on
open-source software in my free time.</p>

<p><strong>How did you start the project?</strong></p>
<p>I began it on Christmas Eve this year (when I had nothing better to do ;)), and released the first version a few days later. Version 0.1 was released on New Year's Eve.</p>


<p><strong>What is this engine? What makes it so different?</strong></p>

<p>This engine acts as a connectivity layer between GTK and Qt.  It is
essentially a normal GTK theme - it's loaded by applications to draw widgets
(buttons, scroll bars, etc.) to the screen.  However, whereas most theme
engines do the drawing themselves, this one calls functions from Qt.</p>

<p>What this means is that the Qt style does the drawing for the GTK application.
So no matter what Qt style you use, its look will be applied to GTK apps too.</p>

<p><strong>Does it work through KControl or does it have a GUI of its own?</strong></p>

<p>There is no need for any configuration module - the engine automatically picks
up your current Qt style, and calls on it to do its drawing.
The only thing that must be done is to set the "Qt" theme as your default in
GTK.  As far as I know, the only way to do this short of editing your config
files is with a utility called "switch2".  I eventually hope to write a
KControl module to help configure GTK themes.</p>

<p><strong>Who is working on it?</strong></p>

<p>Currently, it's only me - although several people, including Ashley Collins
and Ludovic Dupont (from the KDE/Debian project), have kindly offered to help :</p>


<p><strong>Is there a website? Is there a CVS repository somewhere?</strong></p>

<p>There is no website or CVS repository at the moment, but I'll probably set up
a SourceForge project when I need to. For now, <A href="http://www.kde-look.org/">KDE-Look</A>
is sufficient.</p>

<p><strong>Where do you see GTK-Qt Theme Engine going and what are your plans?</strong></p>

<p>My short term goals for the project are bug-fixes (especially for crashes in
applications such as gaim), and speed-improvements.  Currently the drawing is
quite slow, as several conversions have to be applied to the pixmaps to make
them usable by GTK.  A caching system is in place, but this is very
memory-intensive.  The perfect solution would be a new class derived from
QPaintDevice to translate the drawing commands directly from Qt to GDK.</p>

<p>I hope the theme will eventually act as a perfect replica of Qt on GTK, both
in terms of appearance and speed.</p>

<p>I'd also like to see other areas of the Linux desktop unified - simple things
like file selectors and choice of web-browser (I want Evolution to open links
in Konqueror), as well as the more complex technologies, like IO slaves and
KParts.  These are brilliant ideas and designs, but are rather limited by the
fact they can only be used in KDE.</p>

<p><strong>What does the project need the most now?</strong></p>

<p>Like any open-source project: time, money, and contributers :)</p>