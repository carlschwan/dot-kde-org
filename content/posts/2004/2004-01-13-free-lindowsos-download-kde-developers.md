---
title: "Free LindowsOS Download for KDE Developers"
date:    2004-01-13
authors:
  - "fmous"
slug:    free-lindowsos-download-kde-developers
comments:
  - subject: "Very Cool"
    date: 2004-01-12
    body: "I'm glad Lindows is developing good relations witht he KDE community. This will be even cooler when 5.0 is released with what looks to be KDE 3.2 this spring."
    author: "Kraig "
  - subject: "Re: Very Cool"
    date: 2004-01-13
    body: "You sure it will have 3.2? They still sound unsure.\n\nhttp://tinyurl.com/368bc"
    author: "Alex"
  - subject: "Re: Very Cool"
    date: 2004-01-13
    body: "I know they are shoting for it i think it depends on when 3.2 is released."
    author: "Kraig "
  - subject: "Interesting"
    date: 2004-01-12
    body: "This is quite a smart ploy on Lindows' behalf.\n\nFirst, they are rewarding the developers of KDE and such by giving them the equivalent of a 'staff package'.  Now most of the KDE developers will decline this offer for their development systems, but at the same time will appreciate the offer as a sign of goodwill.\n\nA few of them would download the iso's just to check it out, and by doing that they are becoming more aware of what kind of distro Lindows is, and mindshare is important for marketshare in the opensource world. (If not, then we'd all be running slackware *grins*)\n\nEither way, in my own case, I develop on freebsd, so this doesn't appeal to me.  Regardless, thanks for the offer.\n\nTroy\n(if you're bored or crazy, try reading my blog at http://tblog.ath.cx/troy)"
    author: "Troy Unrau"
  - subject: "Re: Interesting"
    date: 2004-01-12
    body: "I bet we'll see a KDE booth this spring at the San Diego Linux desktop Summit :) Now that i live in Sna Diego i'm looking forward to going. "
    author: "Kraig "
  - subject: "Re: Interesting"
    date: 2004-01-12
    body: "Kraig are you offering to go to San Diego Linux desktop Summit? If so please tell this at the kde-promo mailinglist or mail me if you like\n\nFab"
    author: "Fabrice Mous"
  - subject: "Re: Interesting"
    date: 2004-01-13
    body: "woah woah woah...\n\nI live near SD and would LOVE to come!\n\nwhere can I get information about this Summit?\n\nthanks \n\n//standsolid//"
    author: "standsolid"
  - subject: "Re: Interesting"
    date: 2004-01-13
    body: " http://www.desktoplinuxsummit.com Keep checking back.\n"
    author: "Kraig "
  - subject: "Re: Interesting"
    date: 2004-01-13
    body: "I suggest you subscribe yourself to the mailinglist kde-events-us and let it be known .... \n\nFab"
    author: "Fabrice Mous"
  - subject: "Re: Interesting"
    date: 2004-01-14
    body: "Done"
    author: "Kraig "
  - subject: "Re: Interesting"
    date: 2004-01-12
    body: "You could always upgrade your BSD box to linux. :-)\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: Interesting"
    date: 2004-01-12
    body: "Yeah! Just try installing a workable BSD on a Centrino system.  ;-)\n\nI'm curious as to how well Lindows Laptop would fare out of the box.  I've got practically everything working from wireless to winmodem and speedstep on Centrino with Mandrake 9.2 + various downloads.\n\nAs a side note, it would be fantastic if laptop manufacturers start standardising on Centrino...  then both Linux and BSD will have a fixed target and finally get the laptop issue licked."
    author: "Navindra Umanee"
  - subject: "Re: Interesting"
    date: 2004-01-12
    body: "Try it out its a free download for you now :) "
    author: "Kraig "
  - subject: "Re: Interesting"
    date: 2004-01-13
    body: "DON't GET LINDOWS LAPTOP EDITION!!!\n\nGet 4.5, Laptop Edition is just a incomplete 4.5. It was 4.5 at the stage which they believed al the laptop support was prett ymuch done for that release. 4.5 however is better and incldues better drivers for desktops too!\n\nIt's nice, but really I won't be using it until version 5 with Resier FS 4, Kernel 2.6 and hopefully KDE 3.2. 3.0 is a little too old for me."
    author: "Alex"
  - subject: "Re: Interesting"
    date: 2004-01-13
    body: "I have been running Gentoo for some time and don't have a lot of interest in this, but I think it's nice. When I was running Mandrake I remember them initiating their club and I don't recall any benefits from developers. I always felt like I should be getting some special consideration since I was making them money. I got a form to fill out in case they wanted to send me a donation but none of those arrived. I also didn't get much satisfaction when I complained about their menu system when they tried to homogenize things. In the end I was already building everything myself and liked KDE best as it arrived from the developers so Gentoo was my ticket off the RPM upgrade merry-go-round.\n\nHats off to Robertson and Lindows for doing the right thing. I may yet check out one of those releases on another of my systems."
    author: "Eric Laffoon"
  - subject: "Re: Interesting"
    date: 2004-01-13
    body: "> Now most of the KDE developers will decline this offer for their development systems\n\nTrue, this is where the LindowsOS LiveCD comes into action - looking for interesting modifications without installing."
    author: "Anonymous"
  - subject: "KDE fork"
    date: 2004-01-12
    body: "I've always found it interesting that Lindows must basically maintain a fork of at least the UI parts of KDE. The Lindows KDE is quite a bit more simplified than the default KDE install. In Xandros, this is even more true. It would be nice to see a lot of this UI work merged back into the official KDE. I don't want to hear \"you can configure things yourself...\" Users don't want to do that. They see a cluttered UI (and believe me, most \"mainstream\" reviews of KDE that I've seen point conclude that KDE's UI is cluttered) and just move on.\n\nIf KDE wants to compete in the mainstream (and I've got a feeling that some elements of the KDE community really don't want that) then it has to at least provide people the *option* of having a simple/elegant UI. If KDE's internals are so freakishly powerful (and from working with the API, I can tell you that they *are* that powerful) then there should be no problem in providing this option. \n\nTo those who have a different idea of the UI: It doesn't matter if the people who want simplicity are actually right. It doesn't matter if that's the best/most efficient model. What matters is that is what they want, or at least, that is what they think they want. It's all about perception, and its small things like this that have major effects on perception. Apple understands this, Microsoft understands this (perhaps more than anyone else), and even GNOME understands this. But it seems to me that the KDE project, and the KDE community as a whole, often seems to forget this.\n"
    author: "Rayiner Hashem"
  - subject: "Re: KDE fork"
    date: 2004-01-12
    body: "Whats important is that distros like Lindows and Xandros can simplify KDE to cater it to there user base."
    author: "Kraig "
  - subject: "Re: KDE fork"
    date: 2004-01-12
    body: "To be honest, Xandros and Lindows aren't exactly players in the enterprise desktop market. And corporations don't want \"Xandros KDE\" and \"Lindows KDE\". They want KDE, and want any modifications that they use to be officially sanctioned by KDE. They are already in uncomfortably waters by using Linux, and they don't want to add more uncertainty to that situation. \n\nAgain, the GNOME people get this. GNOME is GNOME (Ximian is an extension of GNOME, but follows in the same direction as the official GNOME policy). The KDE developers seem to just want to produce good, quality software.  History has shown continually that good, quality software means jack without *perception*. The KDE project can build the perception of KDE in the customers' eyes. Xandros and Lindows can only do that for their specific distribution. More importantly, the perception of Lindows as being polished does not reflect on SuSE. SuSE is sticking with KDE for the time being, but if KDE can't build the proper perception, then their customers are going to start demanding GNOME, and they are going to have to oblige. \n"
    author: "Rayiner Hashem"
  - subject: "Re: KDE fork"
    date: 2004-01-12
    body: "Try installing a vanilla version of GNOME and tell me that's the same as Ximian or Red Hat again."
    author: "ac"
  - subject: "Re: KDE fork"
    date: 2004-01-12
    body: "Its not identical, but its not very different. The look is different, but the same basic principles are at work: minimal number of visible icons and menu items, widespread accessibility support, minimal number of visible toolbars, etc. This kind of value-added stuff is quite normal and accepted in the corporate market.\n\nHowever, a simplified KDE would not be percieved to be the same as real KDE. I realize that its a simple matter of changing around some XML-GUI files. However, taking a GUI that openly embraces complexity, and turning it into a simple one *seems* like a major change, and will be *percieved* as such. Contrast this to GNOME: take a GUI that embraces simplicity, and polish a few components and add some extra tools. That *seems* totally different.\n\nPS> I use RedHat 9 on my web server, and I've played around with GNOME on my Debian desktop. Once you set both to the same theme, they behave pretty similarly. RedHat's GNOME has a slightly different panel arrangement (no panel-at-top) but the toolbars and widgets look and feel more or less the same.\n"
    author: "Rayiner Hashem"
  - subject: "Re: KDE fork"
    date: 2004-01-13
    body: "> The look is different, but the same basic principles are at work: minimal number of visible icons and menu items, widespread accessibility support, minimal number of visible toolbars, etc. This kind of value-added stuff is quite normal and accepted in the corporate market.\n\nI think that KDE is headed in the same direction. Perhaps we'll see that in KDE 3.3. There are definitely developers and users who would oppose such things, but I don't think the momentum is towards people who prefer a cluttered KDE. \n\nOf course, developer help to make a less cluttered KDE is always welcome; especially patches. There are plenty of people with ideas, but KDE is perhaps more of a meritocracy than other projects. These patches can either be from users or companies (hint.. hint.. Lindows/Xandros/Lycoris) "
    author: "anon"
  - subject: "Re: KDE fork"
    date: 2004-01-13
    body: "Of course, I'm willing to put up or shut up (actually, I'm coding away right now) but all of it is kind of pointless unless the KDE project makes some motions suggesting what direction they want the UI to head. Again, perception is more important than most anything else. "
    author: "Rayiner Hashem"
  - subject: "Re: KDE fork"
    date: 2004-01-13
    body: "They've already made a statement.\n\nThe KDE team has done a lot of work to clean up context menus, the toolbars in kmail etc. And nobdoy even opposed the cleaning up of Kmail's toolbars or anything. KDE developers want KDE to become more mainstream and for users to like it more, so of course they want to simplify it."
    author: "Alex"
  - subject: "Re: KDE fork"
    date: 2004-01-13
    body: "As I remember, some of the KHTML stuff was back-pedeled, and stuff like \"Up/Back/Forward\" never even came under contention. The menus are definately better, but I still count 13 items in the root context menu, and there are still too many buttons on the toolbar. \n\nAs for making a statement --- 3.2 has seen quite a good bit of simplification (I've been using the CVS releases since before the alphas) but I've seen little PR about it. Are people just supposed to try out 3.2 to see how much better it is? PR is critical! Compare the \"What is GNOME?\" page to the \"What is KDE?\" page.\n\nhttp://www.gnome.org/about/\nhttp://www.kde.org/whatiskde/\n\n- The GNOME page puts \"usability\" right as the second point, with links to the HIG and the GNOME Usability Project.\n- The GNOME page speaks in simple, high-level terms. The only specific technology mentioned is the Acessibility Framework, which is self-explanatory.\n- The KDE page starts by extolling the virtues of UNIX.\n- The KDE page makes many references to specific technologies (X11, KParts, MFC, COM, ActiveX)\n- The KDE page makes no mention of usability, and a link to the HIG or the KDE Usability Project is nowhere to be found.\n- In fact, the Acessibility Project is listed under \"community\" on the main KDE website, AFTER kde-apps, kde-forum, kde-look, the wiki, and the developers journals. This makes the project seem unimportant, and most importantly, not affiliated with the core KDE Project. \n\nIf you ask me, they don't seem to be making a very strong statement about usability at all.\n- "
    author: "Rayiner Hashem"
  - subject: "Re: KDE fork"
    date: 2004-01-14
    body: "Usability is in the eyes of beholders (change the KDE settings to what you think is the best usability wise and offer the resulting setting files for others to download, it's easy). Fact is that KDE is more concerned about contributors (that's why you see more technology related information on the about pages) while distributors making use of KDE can then rave about their own settings (doing so is no fork at all and fully supported by KDE, otherwise the settings weren't there).\n\nJust use GNOME if you prefer it so much but please stop forcing your own personal views on others."
    author: "Datschge"
  - subject: "Re: KDE fork"
    date: 2004-01-14
    body: "Usability is in the eyes of beholders\n----------\nThat's not really true. You can judge usability using empirical observations. It is this sort of usability testing that has revealed that the mainstream likes simpler UIs. And it doesn't matter what individuals think. Unfortunately, the computing market suffers from a *tremendous* amount of group-think. When you've got big companies like Apple and Sun saying that simpler is more usable, and when you've got the tech media saying that simpler is more usable, then people will believe that simpler is more usable. Perception is all that matters. And right now, the \"perception\" is that KDE is more cluttered and more complex. \n\n(change the KDE settings to what you think is the best usability wise and offer the resulting setting files for others to download, it's easy).\n-----------\nThe mainstream is not willing to do that. The simpler, more marketable UI should be the default, and people who want it otherwise should do the download.\n\nFact is that KDE is more concerned about contributors\n------------\nAnd, therein lies the problem. I ask you a question: do we care about KDE being a player in the mainstream? If Linux get a degree of popularity in the mainstream, do we want KDE to be a part of that success? Do we want cool commercial apps sporting KDE front-ends, and taking advantage of KIO and KParts? Do we want SuSE to continue to be able to keep KDE as the default desktop? If we don't want this, then the discussion is pointless. KDE is fine as it is. If we do want this, than we have to consider that we must put forth a mainstream face first, while offering options for those who want to go deeper. \n\nwhile distributors making use of KDE can then rave about their own settings\n-------------\nI don't want to minimize the work of the Lindows people, but lets face it, the KDE Project has a lot more ears listening to it than Lindows. kde.org raving about KDE's features means a whole lot more than lindows.com raving about KDE's features. \n\n(doing so is no fork at all and fully supported by KDE, otherwise the settings weren't there).\n---------------\nIf its not a fork, tell me how I can get my desktop to look like Xandros without doing an inordinate amount of work? I'm pretty sure there are code changes in there in addition to the XML-GUI stuff. Most importantly, the resulting product isn't KDE, but a derivative of it. It doesn't matter that its technically trivial to do the derivation, people don't understand that. \n\nJust use GNOME if you prefer it so much but please stop forcing your own personal views on others.\n---------------\nI can't stand GNOME. I've been using KDE for years, and there is no way I'm switching until GNOME stops being inferior. I do, however, recognize that the GNOME developers \"get it\" when it comes to understanding that marketing and perception is just as important as product quality. Many of their moves (co-opting OpenOffice under the GNOME umbrella) are brilliant. I don't want to see KDE become yet another incredible technology that nobody uses. The computing world is already littered with those, we don't need any more. \n\nAnyway, this discussion is getting pointless. Obviously, we have completely different ideas about where KDE should head, and how it should get there.\n\n"
    author: "Rayiner Hashem"
  - subject: "Re: KDE fork"
    date: 2004-01-14
    body: "Hey, can I quote you on this and write an article with your opinions mostly on the matter, since I have pretty much the same feelings. I'm thinking of submitting it to OSNEWS and I want to quote you in some areas."
    author: "Alex"
  - subject: "Re: KDE fork"
    date: 2004-01-14
    body: "Um, sure, be my guest.\n"
    author: "Rayiner Hashem"
  - subject: "Re: KDE fork"
    date: 2004-01-14
    body: "Sorry, but your talk about \"mainstream\" is bull. You keep talking as if KDE should court the mainstream with its offering, as if KDE is offering a distribution on its own. This is not at all the case, KDE is just offering the source code and numerous services related to documentation and contributing yourself. Even if you seriously think the mainstream which you worry so much about should start compiling KDE themselves you still better approach Gentoo for letting them include the limited down settings you prefer. KDE is a project living through its contributors, it's created by and for its contributors. It should stay far away of any change which might scare away some of its contributors. If you want replace something with something completely different you need to give the choice to the contributors and let the project decide as a whole this way. As soon as you start mind numbly saying that the project should focus on being a mainstream player instead on its contributors you are endangering the very foundation of the whole project.\n\nEverything else is just a matter of finding volunteers or doing the work yourself just to show \"it can be done, look how good it would be\". But your approach is wasting your time, putting yourself in a bad light for everyone who does actually contribute instead ramble here, and scaring away potential volunteers realizing what you are talking about. I hope you \"get it\" by now."
    author: "Datschge"
  - subject: "Re: KDE fork"
    date: 2004-01-14
    body: "Sorry, but your talk about \"mainstream\" is bull.\n--------\nYou dodged my question. Should KDE go after the mainstream? Let me make this more concrete. Do the KDE developers want KDE to be used by the mainstream market, or do they want to only target a certain type of user? If they don't want the mainstream market, then the rest of my points are baseless, and you can stop reading right here. \n\nYou keep talking as if KDE should court the mainstream with its offering, as if KDE is offering a distribution on its own. This is not at all the case, KDE is just offering the source code and numerous services related to documentation and contributing yourself.\n---------\nI am perfectly aware of KDE's current distribution model. However, you have to distinguish between two things: logistical structure and political structure. Consider the GNOME project as an example: Logistically, they are just like KDE: they offer source code and documentation for distributors to integrate. Politically, they are completely different. The KDE project mostly acts like it is just offering source code and documentation, while the GNOME project pushes GNOME as an independent, complete product. I'm saying that KDE should be more like GNOME politically. \n\nEven if you seriously think the mainstream which you worry so much about should start compiling KDE themselves you still better approach Gentoo for letting them include the limited down settings you prefer.\n--------\nLet's face it. Most distributions (with the exception of Xandros and Lindows) do not extensively customize KDE. They *brand* their version of KDE (just as, say Dell brands Logitech's mice) by changing themes, colors, icons, etc, but do not really change the UI markedly. Why? Because people percieve KDE as a complete product! People don't talk about SuSE's KDE or Mandrake's KDE, they talk about KDE! The defaults shipped in the raw source are pretty close to what end-users will see on their desktop. If those defaults are not suitable for a mainstream market, then demand for KDE will fall, and distributions will have to oblige their customers. Like it or not, KDE has an independent profile that goes beyond the bounds of any distribution. Its not a cog in the machinery of the distribution. And the last phrase is a nice little bit of misdirection. Simple settings are not what I prefer, but what the mainstream prefers. For myself, I don't care so much. I maintain not only customized settings and toolbar configurations, but minor patches to Qt and several KDE themes. I'm willing to go to that much trouble to use KDE. However, the mainstream is not, and judging by their current behavior, neither are distributers. Speak of distributers, I raise another point: why should distributors have to customize KDE to target their customer base? After all, they don't have to customize GNOME... Distributors are good at integration and packaging --- application development is not their primary purpose. That explains why most distributions don't bother to do the degree of customizations that Xandros and Lindows do. Andy why duplicate all that effort anyway? It just doesn't make sense. \n\nKDE is a project living through its contributors, it's created by and for its contributors. It should stay far away of any change which might scare away some of its contributors.\n--------\nI don't see why any contributers should be scared away. This is a matter of changing defaults, not fundementally changing the UI. I'm not suggesting that we pull a GNOME and get rid of features wholesale! And while KDE is a project for developers, it should present a different face to the mainstream. To go back to my \"About KDE\" page critique, what is wrong with putting that detailed information under developers.kde.org, and putting something more mainstream in the mainstream sections of the site?\n\nIf you want replace something with something completely different you need to give the choice to the contributors and let the project decide as a whole this way.\n---------\nI don't see the point of you constantly misrepresenting my viewpoint. I'm not saying that we \"replace\" anything with anything \"completely different.\" I'm saying that we change the defaults to make KDE more marketable to the mainstream. Why is that so scary?\n\nAs soon as you start mind numbly saying that the project should focus on being a mainstream player instead on its contributors you are endangering the very foundation of the whole project.\n-----------\nI don't see why there should be a conflict here. If the contributers want KDE to be mainstream, then they should work that way. If they do not, then, as I said, my points are baseless and you can feel free to ignore them. My basic assumption in this argument is that the KDE developers want KDE to enter the mainstream. That they want corporate users to pick KDE rather than GNOME, or even better, they want corporate users to be drawn to *NIX because of KDE. \n \nEverything else is just a matter of finding volunteers or doing the work yourself just to show \"it can be done, look how good it would be\". \n------------\nIts not a matter of \"build it and they will come.\" That's not how the mainstream software industry works! History has shown repeatedly that this is not how the mainstream works. Its a matter of \"hype it as much as possible, and they will come!\" I present Microsoft's Longhorn as a prime example. People treat Longhorn as an actual product that exists now! They compare it to Linux and Mac. But Longhorn as a product doesn't exist yet, and won't for several more years. It's all about *perception*. I present two example cases:\n\n1) Consider what the GNOME project did with OpenOffice. They incorporated it into GNOME Office long ago, when there was *zero* technical merit for the move. Today, GNOME/OpenOffice integration is not that much further along than KDE/OpenOffice integration. Yet, everyone thinks that OpenOffice is a GNOME app.\n\n2) Consider the KDE Enterprise project. They announced the project and released their paper before they had anything the users could download and play with. Yet, they were able to offer a prompt reaction to Perens's comments, and give themselves time to build the actual product without affecting public perception. \n\nI don't believe that this is a good thing. Where I work, I'd love to build the software first, an extoll its virtues later. Yet, I find myself pushing our design to our customer before a line of code is written! Just think: how long to hear about commercial products before they are released? Months, or even years!\n\nBut your approach is wasting your time\n-------\nI don't think so. Action is not speech, and speech is not action. Both are essential. \n\nscaring away potential volunteers realizing what you are talking about. \n--------\nI would hope potential volunteers would have more sense than to take the ramblings of a random poster on a message board that seriously...\n"
    author: "Rayiner Hashem"
  - subject: "Re: KDE fork"
    date: 2004-01-16
    body: "I didn't dodge your question, nor do I get the impression you understood that one important distinction needs to be made.\n\nThe KDE project as a whole is solely there for creating and maintaining a desktop environment and the community around it, this is an easy to find common ground. What you are asking for is the inclusion of politics, goals and maintenance foreign to KDE as a project and unlikely to have a common ground there. For example KDE is not a complete product, you still need an underlying system, and it's not the KDE project's goal to change this, there is no plan to import the Linux kernel or any other kernel into KDE's CVS and I doubt there ever will.\n\nNow, and here I think you fail to make a distinction, there are the contributors and third parties, who independently of KDE often do have specific goals, courting the mainstream being just one of many possibilities. And I count commercial companies like Lindows, SUSE but also the numerous smaller companies who have employees directly or indirectly working on KDE as contributors. Those obviously don't have an interest in the KDE project being limited to one political goal which might even clash with that of a contributing company unnecessarily starting a competition where should be cooperation.\n\nOver at GNOME its contributors from Ximian, as you are not getting tired to keep noting, are doing a good job combining minimal integration with maximal PR for both Ximian and GNOME. Furthermore the GNOME project seems to be very lax about what applications can be considered GNOME applications (GNOME Office as you mentioned), leading to unnecessary confusion about which \"GNOME\" applications can actually make use of GNOME's low level features. All the minimal integration (and more) is usually done by some KDE contributors as well eventually, but whether they are becoming an official endorsed part of the KDE project is a different question for above mentioned reasons.\n\nAlso I don't consider you a \"random poster\" since you're usually doing a fine job over in the comments sections over at osnews.com. =P\n\nBtw. there is no KDE Enterprise project as you described it, there's however a KDE Enterprise website which purpose is completely different, and the KDE-Debian mailing list (to which you are referring to) which does a fine job more or less coordinating several existing integrative subprojects of which numerous were mentioned here at dot as of late."
    author: "Datschge"
  - subject: "Re: KDE fork"
    date: 2004-01-14
    body: "> Usability is in the eyes of beholders... [snip] but please stop forcing your own personal views on others.\n\nThere are certain times where this is the case; but is not often true. \n\nKDE is already *good enough* to be used by most regular people. However, it can be made even easier to use, as Xandros/Lindows/Lycoris prove. There is, of course a growing movement to change various default KDE settings in the manner that those distros have, and despite people who oppose such changes, I think the momentum is there. "
    author: "fault"
  - subject: "Re: KDE fork"
    date: 2004-01-14
    body: "The changing of \"default\" settings by distributions in their own products is fully supported by KDE, otherwise it wouldn't offer all the settings and kpersonalizer which does something similar already for quite some time. Stop talking about \"default settings\" as if they are the end of the world, KDE offers plenty of room for easy changes and distributions make use of it, and you can too."
    author: "Datschge"
  - subject: "Re: KDE fork"
    date: 2004-01-14
    body: "Erm... you certainly have a curious interpretation of what KDE is, and what KDE releases are. I think most contributors would agree that KDE release have never been about simply making a base on which other distributions are based on. It's not meant to be a raw piece of software that distributors have to work hard in customizing; if it were, a LOT OF the current user interface designs that we have wouldn't be there; we might have all applets on kicker by default, or things like that. \n\nOf course, distributions are certainly free to customize KDE all they want, and the KDE project and it's contributors have been generally (rh8 is an exception, but even then, it was mostly certain flamebaiting people) supportive of customizations. "
    author: "fault"
  - subject: "Re: KDE fork"
    date: 2004-01-14
    body: "No, KDE offers source code mainly for contributors, and the most important and influencial contributors are the developers contributing code, and those were also who set up the defaults like they are now. Distributions, just like any other additional users, are free to change those defaults settings. What is the curious part about this \"interpretation\"?\n\n(The customizations in RH8 weren't that welcome since they went beyond simple setting changes by including custom patches for libraries for including features which where work in progress in the respective projects already, and some of them turned out to be incompatible, unnecessarily adding bugs for which the original projects weren't responsible and Red Hat didn't felt responsible to fix.)"
    author: "Datschge"
  - subject: "Re: KDE fork"
    date: 2004-01-14
    body: "<quote>\nAs I remember, some of the KHTML stuff was back-pedeled, and stuff like  \"Up/Back/Forward\" never even came under contention. The menus are definately better, but I still count 13 items in the root context menu, and there are still too many buttons on the toolbar. \n</quote>\n\nActually, this has recently been dealt with in kde-usability. See http://lists.kde.org/?l=kde-usability&m=107333625300627&w=2\nIt is much easier now that konq supports custom XMLGUI files :)"
    author: "Luke Sandell"
  - subject: "Re: KDE fork"
    date: 2004-01-13
    body: "Technically speaking it's not a fork if you rebase your work to the main development line from time to time but rather a branch."
    author: "Anonymous"
  - subject: "Re: KDE fork"
    date: 2004-01-13
    body: "True. But from the point of view of a corporate user, there isn't a huge amount of difference.\n\n"
    author: "Rayiner Hashem"
  - subject: "what about the larger dev community"
    date: 2004-01-13
    body: "I'm not good enough to be a KDE dev. But I participate as best as I can with bug reports and wishlist items. I also translated Kbarcode to french and created Picwiz which is an app for KDE. I also started to port yammi from QT to KDE but then I met Juk...\n\nI've recently decided to try most KDE centric distros and I would love to test out Lindows. Unfortunatly I don't fit in the above categories and being unemployed for the past 6 month I can't afford to buy a copy (but that will change soon as I've finaly got the papers for my new job...)\n\nI like mdk but I find there approach to be weird sometimes (how can your configure tools be in GTK when you're a KDE distro?) Mepis and Ark linux are cool but way too alpha to be recommended to anyone else. Suse has the same problem as Lindows (my wallet).\n\nI know my programming skills won't get me in the inner circles so maybe I'll hurry up and translate something in kde :-)."
    author: "ybouan"
  - subject: "Thanks"
    date: 2004-01-13
    body: "Thanks Lindows for this step towards developers. That would be cool to see others do the same, even with hardware support. Lindows also takes great care in packaging their applications and have a vast choice of Family & Educational programs. I find the Click-N-Run Warehouse very nice and I really hope that will bring new people to Linux, people who just want the software with one click. "
    author: "annma"
  - subject: "Re: Thanks"
    date: 2004-01-13
    body: "Someone will have to invent downloadable costless replicatable hardware first."
    author: "Anonymous"
  - subject: "Re: Thanks"
    date: 2004-01-13
    body: "costless? replicatable? hardware?\nHow does this go with my post? Applications that are available from Lindows are free as in free speech (i.e source is available). Costless, that cannot be, as Lindows aims to make life easier for users and Lindows people take time to package their software. Why would their time be costless? \nWhy would not people pay for having easy-to-install software versus painful compiling from sources? Not everyone is a geek.\n"
    author: "annma"
  - subject: "Re: Thanks"
    date: 2004-01-13
    body: "Not that I want to down play Lindows' program but it will not cost them much money. Hardly any KDE developer now downloading the ISO because of curiousity would have bought it otherwise. Hardware would cost distributors money, both in producation and shipping."
    author: "Anonymous"
  - subject: "Re: Thanks"
    date: 2004-01-13
    body: "Yeah, but the most important thing is the gesture of goodwill. I find myself most endeared to Lindows now and next time a member of my family asks me what Linux distro they should consider (it's happened once!) I'll be more inclined to suggest Lindows.\n\nI'm considering whether or not to apply for one of these \"coupons\"."
    author: "Max Howell"
  - subject: "Michael R."
    date: 2004-01-13
    body: "My problem is that you cannot get Lindows on the European market through ordinary sources. Why doesn't he open up a German branch?"
    author: "gerd"
  - subject: "Re: Michael R."
    date: 2004-01-14
    body: "Too much legal action being pushed against him in other European Countries.\n\nhttp://www.choicepc.com"
    author: "Adam Moniz"
  - subject: "And what can the artists do to get lindows?"
    date: 2004-01-13
    body: "And what can the artists do to get lindows?"
    author: "Ante Wessels"
  - subject: "Re: And what can the artists do to get lindows?"
    date: 2004-01-13
    body: "Yes, the artists also need to benefit from that. They do an amazing work and are seldom thanked. I for myself is deeply grateful for all the icons and other drawings they do. And we need more artists by the way..."
    author: "annma"
  - subject: "Re: And what can the artists do to get lindows?"
    date: 2004-01-13
    body: "Just contact the art guy, Tackat/Torsten.  Shouldn't be a problem, IMHO."
    author: "Navindra Umanee"
  - subject: "Lindows"
    date: 2004-02-29
    body: "website to download lindows for free"
    author: "Arun"
  - subject: "Re: Lindows"
    date: 2006-10-13
    body: "http://www.linspire.com/cnr_linspirelive.php"
    author: "me"
  - subject: "link"
    date: 2004-04-01
    body: "link plz"
    author: "mohamed"
---
As mentioned on KDE development mailing lists, <a href="http://Lindows.com/">Lindows.com</a> is offering all their major versions of <a href="http://lindows.com/lindows_sales_intro.php">LindowsOS</a> for free download to all KDE developers. This is the result of some talks between the Linux vendor and the KDE project. If you want to apply for the LindowsOS please read on. The KDE project would like to thank Lindows.com
for this program.







<!--break-->
<p>Lindows' CEO Michael Robertson announced it in
<a href="http://www.lindows.com/lindows_michaelsminutes_archives.php?id=98">his latest "Michael's Minutes"</a>:</p>

<pre>
	"So today we're kicking off a program with KDE to make
	LindowsOS software available to their developers at no
	cost. All major versions of LindowsOS, including Laptop
	Edition and the Developer Edition (which includes popular
	programming tools), will be free. We hope to quickly
	expand this to other developer groups. We see this as a
	first step in growing a symbiotic relationship between
	rockhoppers and Lindows.com."
</pre>

<p>Also Michael Robertson sent the following message to the developers
of the KDE project:</p>

<pre>
	KDE'ers,

	I wanted to personally thank all of the effort, skillful programming and
	good organization which you all provide. Your work is greatly
	appreciated and makes LindowsOS possible. Our goal is to see to it that
	your code and Linux more broadly impact the widest possible audience.

	To reach this goal we've been working very closely with hardware
	manufacturers to convince them that people want (read: will buy) linux
	desktops and laptops. This also entails helping create ways for them to
	make money so they will consider doing something different then MSWin
	and providing the hardware and quality assurance so that their machines
	will work well running Linux. Enough about us.

	The reason I'm writing is to let you know that we've talked with a few
	folks at KDE about coming up with a strategy to make LindowsOS available
	to all KDE developers at no cost. Since you help make LindowsOS possible
	it seems to make sense. Starting today you can download digitally any
	version of LindowsOS (LindowsOS, Laptop Edition, LindowsCD or Developer
	Edition) by simply visiting <a href="http://www.lindows.com/kde">http://www.lindows.com/kde</a> and following the
	links to purchase the software<sup>1</sup>. You will not be charged any money and
	gain access to download ISOs from our high speed servers.

	We hope you'll appreciate these features:

	- Working with common file types (we have a big list at
	  <a href="http://www.lindows.com/filetypes">http://www.lindows.com/filetypes</a> of the most popular including Windows
	  Media, Quicktime, Java, etc.)
	- Plug n play USB and firewire devices
	- Power management on laptops for a broad range of portable via our
	  Laptop Edition
	- One click software adding/updating via CNR (click-n-run)

	Thanks for all the great work you have done and continue to do! We hope
	to continue to find ways to directly help the KDE team. If you're
	curious what we've done to date, please visit:
	<a href="http://www.lindows.com/opensource">http://www.lindows.com/opensource</a>
</pre>

<p>How to obtain the LindowsOS through this program?</p>

<p>You will have to enter a coupon code to identify you as a
KDE developer. If you have contributed to KDE you are
qualified to request the code:</p>

<ul>
<li>If you have a CVS account then please contact <a href="mailto:sysadmin at kde.org">sysadmin@kde.org</a></li>
<li>If you are a documentation writer, please contact the documentation
  team coordinator <a href="mailto:lauri at kde.org">Lauri Watts</a></li>
<li>If you are a translator then please contact your i18n team leader</li>
</ul>

<p>More information can be found on the <a href="http://www.lindows.com/devmembership-kde.php">Lindows Website</a>.</p>

<p>1. You will need a coupon code to complete the registration for download</p>






