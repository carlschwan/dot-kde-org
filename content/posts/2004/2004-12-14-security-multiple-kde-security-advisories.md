---
title: "Security: Multiple KDE Security Advisories"
date:    2004-12-14
authors:
  - "wbastian"
slug:    security-multiple-kde-security-advisories
comments:
  - subject: "xdelta kde-update"
    date: 2004-12-14
    body: "i think no one patches his kde installation for bugs. We just wait and continue installing the updateded releases. The problem is the releases dont come this fast. The unpatched systems remain a long time on the net unpatched. Its the problem of your distribution if they update , you get the fixes , if not your doomed :-) perhaps we need a better update machanism. like with hashes and xdelta patches...\n"
    author: "chris"
  - subject: "Re: xdelta kde-update"
    date: 2004-12-14
    body: "> perhaps we need a better update machanism. like with hashes and xdelta patches...\n\nDistributor task, eg SUSE 9.2 uses delta.rpms for online updates."
    author: "Anonymous"
  - subject: "Re: xdelta kde-update"
    date: 2004-12-14
    body: "yes distributer task sucks. you get the updates not when you need them. recompiling kde takes looong , and patches is much work"
    author: "chris"
  - subject: "Re: xdelta kde-update"
    date: 2004-12-14
    body: "Since current KDE policy is not to provide binaries, only source, the binaries have to be provided by the distributions. Hence the responsibility to provide security updates in binary form are the distributions. If the distribution don't take security seriously and offer updates to fix vulnerabilities, you are better of switching to another distributor. Whether they use binary diffs or update whole packages does not matter much, what counts are that security are not optional."
    author: "Morty"
  - subject: "Re: xdelta kde-update"
    date: 2004-12-14
    body: "what about a distributed compile mechanism , which sends the required libaries to the other computers and only gets back the bytes that changed in the binaries ? something like bittorrent for compiling (distcc) which updates the binaries automatically.\n\nit should uses hashes for the libs and h files , and if someone has *exact* the same setup it just uses the precompiled things but if the hash differs it send the libs and .h in the net and gets back the patch."
    author: "chris"
  - subject: "Re: xdelta kde-update"
    date: 2004-12-14
    body: "Unfortunately I think using something like for executable code would introduce a pretty major security hole... ;-)\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: xdelta kde-update"
    date: 2004-12-14
    body: "not if its done right and covered with hashes and crypto..\n"
    author: "chris"
  - subject: "Re: xdelta kde-update"
    date: 2004-12-14
    body: "ok, and so crypto made with untrusted people would make things secure \"magically\" ?\n\n;)"
    author: "Michel Nolard"
  - subject: "Re: xdelta kde-update"
    date: 2004-12-15
    body: "Ok, there are 2 possibilities:\n1) you are trolling\n2) you don't know what you're talking about\n\nobviously 1 do not exclude 2"
    author: "Davide Ferrari"
  - subject: "Responsible Distribution Management"
    date: 2004-12-14
    body: "In fact, all the problems like this one come from a lack of Responsible Distribution Management, RDM for short. Let me explain myself a little, although this is only a big picture.\n\nA good distribution, being a complete GNU/Linux OS or a single application or game, or a complete Desktop Environment, needs to insert its own Distribution Management level to cope with its specific goals and problems.\n\nAn OS has to cope with the fact that nothing lies beyond it to help with packages and installations. A game has to be VERY VERY simple to install, update, remove and configure as it can be used by children. A desktop environment has to give an easy way to extend it with applications without neither complexifying the packager's work or the user's work.\n \nSystems like RPM, DEB,... are OS Distribution level specific. They provide what is needed in terms of stability, dedication and power for a complete OS to be managed.\n\nThe thing is that distribution systems really NEED to DELEGATE (it is the right important word) some Distribution Management tasks to other distribution systems. Just look at an example : a game has no vital needs to provide patches as quickly as an OS would ! So, the release, package and distribution cycles do not have to behave in the same way.\n\nThe things should be made upon a responsible point of view, using Responsible Distribution Management. Wonder why does Konstruct have so much success ? Because it is a (source level) kind of RDM for the KDE Distribution !\n\nOf course, I don't tell that there is no reason for the Debian Project to provide its own safely built packages by using a (very) long testing phase. There is some needs for that ... but not for EVERYTHING.\n\nI just tell you that there is a simple (but not easy) reason why people are reinventing the wheel by creating new package management systems (look at Gentoo, Sorcerer, ...) : this is because what they are waiting from their Distribution System is different... simply because everybody tend to provide a single solution which should fit for many different problems at the same time. The trap is in the fact that the problems seems to be similar... when they are not as much as we can think of at first glance.\n\nThis come from a problem in computer science : people are all seeking after the Holy Gr\u00e2\u00e2l which would be \"one application\" which makes everything. I can predict that this is not yet to happen : there should be one solution for each problem ... and obviously not always the same.\n\nThis is the key : Responsible Distribution Management ! Give power to others were it isn't wise that YOU make the choice at THEIR place !\n\n--\nMichel Nolard\nJust thinking...\n"
    author: "Michel Nolard"
  - subject: "Re: Responsible Distribution Management"
    date: 2004-12-15
    body: " I read your post end-to-end three times to make sure I got your views right..\n\n>A good distribution, being a complete GNU/Linux OS or a single application or game, or a complete Desktop Environment, needs to insert its own Distribution Management level to cope with its specific goals and problems.\n\n Ok-so... either they all have their seperate system on this one, which would make for a lot of duplicate work and general maintanance/bugfix nightmares... or a common system that is flexible and powerfull enough to fit all projects, to allow hackers to focus on the job of producing the application itself rather then spending time on getting the update functionality to work.\n\n>The thing is that distribution systems really NEED to DELEGATE (it is the right important word) some Distribution Management tasks to other distribution systems.\n\n Uh-huh... \nA) Distributors alrady does this by pumping binary security patches out pretty fast. \nB) What if a distribution has a tighter security policy then the project in question? \nC) What kind of nightmare do you think it would be to maintain interopreatability with say 50+ kinds of update mechanisms?\n\n>Just look at an example : a game has no vital needs to provide patches as quickly as an OS would ! So, the release, package and distribution cycles do not have to behave in the same way.\n\n Distributinion vendors are not bound by the release cycle of a given project. They are absolutely free to backport any and all patches, be it security or bugfix, to the version they are curently shipping. For OSS games it makes sense that they ship new versions when (big surprise) new versions are released. For comercial games it makes sense that the particular vendors make .rpms and/or .debs available at their web/ftp-site when (next big surprise) updates are ready for public release. What ever they choose to make an in-game [auto-update] button is irrelevant to a security discussion...\n\n>The things should be made upon a responsible point of view, using Responsible Distribution Management. Wonder why does Konstruct have so much success ? Because it is a (source level) kind of RDM for the KDE Distribution !\n\n I can of course only give my own opionioin on this one... but i use it because it makes dependencies a non-issue. It is, from a technical standpoint, nothing more then an ultra-advanced buildscript. If this truely was a RDM-thingie it would have its profiles updated in synchronizasion with the release of security-patches.\n\n>This is the key : Responsible Distribution Management ! Give power to others were it isn't wise that YOU make the choice at THEIR place !\n\n I'm sorry for my somewhat condecending tone.. but i dont even think you are barking up the wrong tree here... i think you are barking up a tree that isnt even there!\n Distribution vendors have their proffesional pride on the line here. They should not rely on other projects to make this kind of choice. Instead they should (and do) keep adding more manpower to monitor security-advisories/patches and pump updates out as fast as they can, and instruct users to have a cron-job running to check for updates every 6-24 hours... period!\n\n~Macavity"
    author: "Macavity"
  - subject: "Re: Responsible Distribution Management"
    date: 2004-12-21
    body: "\"which would make for a lot of duplicate work\"\n\nAh ! The ever faith-related thought present in free software communities ... which is a big non-sense !\n\nThis is something that LOTS of free software developers think and which is the complete opposite to freedom which they are seeking for ! Not to duplicate means standardization and thus intolerance. It is like the unique ring (see The lord of the rings)\n\nThe fact is that the real power of the free software communities is that they can communicate to make thinks interoperable.\n\nDon't be so fanatic or extremist like this, please ! They are several skin colors on earth and nobody dares to tell there should be only one without being stated xenophobic. Why isn't it the case in the free software world too ? This is intolerance that must be fought ! This is what comes from proprietary softwares : they do not tolerate competition.\n\nSo stop telling GNOME users they should go on with KDE or to KDE users to stop and go on with GNOME. Diversity is what brings innovation.\n\nThe danger is to recreate a Babel situation where the first's applications cannot run on the second and vice versa.\n\nDuplicating work is wise when it permits to achieve better results. Isn't it what Linus Torvalds did when he started to develop its Linux kernel ?"
    author: "Michel Nolard"
  - subject: "Re: Responsible Distribution Management"
    date: 2004-12-21
    body: "There is only one logical response to this:\n\nIf you are in favour of doing that, even though it implies extra work, then you do it. The rest is bla bla bla."
    author: "Roberto Alsina"
---
Three security advisories have been issued by the 
<a href="mailto:security@kde.org">KDE Security Team</a> over the last few days for three distinct vulnerabilities that have been found in KDE:
<a href="http://www.kde.org/info/security/advisory-20041209-1.txt">
Plain Text Password Exposure</a>, 
<a href="http://www.kde.org/info/security/advisory-20041209-2.txt">
KFax libtiff Vulnerabilities</a> and a 
<a href="http://www.kde.org/info/security/advisory-20041213-1.txt">
Konqueror Window Injection Vulnerability</a>.




<!--break-->
<p>The most serious one is the libtiff vulnerability in KFax. Until recently KFax used to include a private copy of libtiff. As a result KFax has not been able to take advantage of 
<a href="http://www.securityfocus.net/advisories/7364">
several recent security fixes in libtiff</a>. KFax in KDE 3.3.2 has been fixed so that it no longer requires a private copy of libtiff. Due to the complexity of this change there are no source patches for older versions available, we do expect vendors to provide fixed KFax packages for older KDE versions though.
</p>

<p>
The password exposure vulnerability primarily concerns passwords for SMB shares, as previously reported by
<a href="http://www.sec-consult.com/index.php?id=118">SEC Consult</a>. The Konqueror window injection vulnerability addresses a problem 
<a href="http://secunia.com/advisories/13254/">raised by Secunia</a> and last week <a href="http://it.slashdot.org/article.pl?sid=04/12/09/0053205">reported by Slashdot</a>.
</p>

<p>
An overview of all the KDE security advisories can be found on
<a href="http://www.kde.org/info/security/">http://www.kde.org/info/security/</a>.
</p>


