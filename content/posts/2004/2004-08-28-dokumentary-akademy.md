---
title: "DoKumentary at aKademy"
date:    2004-08-28
authors:
  - "ateam"
slug:    dokumentary-akademy
comments:
  - subject: "Amazing!"
    date: 2004-08-28
    body: "I love the idea. I will be waiting eagerly for the final result."
    author: "Pedro Fayolle"
  - subject: "Download?"
    date: 2004-08-28
    body: "Will there be any plans to have a downloadable version?  Maybe like a torrent of the file?\n\nOr will you only be able to get it from the DVD?"
    author: "Corbin"
  - subject: "Re: Download?"
    date: 2004-08-31
    body: "Entirely possible.  The goal is to get it seen by as many people as possible."
    author: "Thor"
---
If you are attending the <a href="http://conference2004.kde.org/sched-userconf.php">UserConf</a> over the coming weekend, then make sure you drop by the doKumentary room during your visit. Thorarinn Einarsson (Thor), a student of Documentary Studies at Duke University, is making a documentary about the KDE project. He is filming at <a href="http://conference2004.kde.org/">aKademy</a>, and is keen to hear from users and contributors.



<!--break-->
<p>Thor's documentary will cover the processes and culture of the project, and so he invites as many people as possible to contact him at the UserConf or <a href="mailto:teinarsson@gmail.com">by e-mail</a>. He said: "I even want to hear from people if they have any ideas or things that they feel should be included in the documentary. You can send me stories, photos or even footage as I'm open to anything."</p>
<p>Currently scheduled for completion late next year, the documentary will be burned to DVD and distributed as widely as possible. If you have any thoughts on the project or stories to tell, be sure to get in touch, to help Thor make an entertaining documentary that represents the entire KDE community. Thor will also be filming at other events like LinuxWorld and LinuxTag as well as smaller meetings to show the relationship between the KDE community and the wider free software communities.</p>



