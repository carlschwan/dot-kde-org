---
title: "KDE Configuration Tamers"
date:    2004-07-08
authors:
  - "zrusin"
slug:    kde-configuration-tamers
comments:
  - subject: "Kiosk Admin Tool Graphics"
    date: 2004-07-08
    body: "Maybe waldo should ask kde-artists for some new graphics in Kiosk Admin Tool before showing it off in akademy :-)"
    author: "anon"
  - subject: "Re: Kiosk Admin Tool Graphics"
    date: 2004-07-08
    body: "Kiosk is still a work-in-progress really. Functionally it is getting there, and then perhaps its look and feel can be talked about. Maybe it might become a KPart and be integrated into Control Centre in some way? Don't know."
    author: "David"
  - subject: "Re: Kiosk Admin Tool Graphics"
    date: 2004-07-09
    body: "> Kiosk is still a work-in-progress really. Functionally it is getting there\n\nWhat is missing?\n\n> Maybe it might become a KPart\n\nHu? Locking down code is wide-spread within different KDE components."
    author: "Anonymous"
  - subject: "Re: Kiosk Admin Tool Graphics"
    date: 2004-07-09
    body: "> What is missing?\n\nWell, as Waldo says it is in KDE Extragear and it still needs to be more widely used.\n\n> Hu? Locking down code is wide-spread within different KDE components.\n\nI meant the configuration tool. It may become part of the Control Centre, or as part of the admin tools."
    author: "David"
  - subject: "KDE's uniform look'n'feel"
    date: 2004-07-10
    body: "I see: a few apps on my KDE desktop that come with their own look'n'feel.\n\nI hope: this will not end up in the look'n'feel hell that windows is in right now, cauz all styles tend to mix there. To name a few: 98, XP (luna), XP-mediaplayer, XP-office (i.e. different toolbars), XP-software/update-dialogs and OSX (QuickTime, iTunes). And several apps ship there own style (Norton, RealPlayer, AdAware). This is bad looking'n'feeling in my eyes.\n\nI think: KDE will be smart enough to avoid this problem. IN the future this will become a big advantage that KDE ill have over other desktop experiences.\n\nI greet: all of you and wish you a pleasant day..."
    author: "cies"
  - subject: "Woah"
    date: 2004-07-08
    body: "Dear God the KCfgCreator looks so freakin' cool! I Know I have been messing with KConfig XT and have been struggling grasping it.  I hopw this will help.\n\nKConfigEditor looks promising, It would be cool if it can not only edit desktop preferences but things in /etc too, like /etc/fstab for example.\n\n'twold be nice -- but I doubt it would happen.  Stuff like that is usually left to the distro. \n\n//standsolid//"
    author: "standsolid"
  - subject: "KDE for Linux/Unix Admin :)"
    date: 2004-07-09
    body: "thank you developers for making kde administrable. KDE 3.3 must have to have these stuff in KDE Admin package."
    author: "Fast_Rizwaan"
  - subject: "naming conventions"
    date: 2004-07-09
    body: "KConfigXT\nKConfigEditor\nKCfgCreator -- What about KConfigCreator? :)"
    author: "anonymous poster"
  - subject: "Package Placement?"
    date: 2004-07-09
    body: "The placement of these packages seems way wrong. Why are they in KDE Extra Gear?\n\nKConfigEditor and Kiosk Admin Tool surely belong in kdeadmin. They would be extremely usefull to any administrator, and I fear too many people will miss them in kdeextragear ( many people do not install the KEG packages ).\n\nKCfgCreater surely belongs in kdesdk. Its not usefull to anyone who is not a developer."
    author: "Jason Keirstead"
  - subject: "Re: Package Placement?"
    date: 2004-07-09
    body: "They are immature?"
    author: "Anonymous"
  - subject: "Re: Package Placement?"
    date: 2004-07-09
    body: "Kiosk Admin Tool is in KDE Extra Gear because I want to be able to have fast release-cycles."
    author: "Waldo Bastian"
  - subject: "GNOME Support"
    date: 2004-07-09
    body: "Finally a KDE tool to turn off spatial Nautilus mode! ;-)\nSo who had search within a GConf editor first? KDE or GNOME?"
    author: "Anonymous"
  - subject: "Please no GConf"
    date: 2004-07-12
    body: "Please no GConf in KDE and no GConf variant or something hidden with a different name but being GConf in core."
    author: "Martin G\u00f6ring"
  - subject: "Re: Please no GConf"
    date: 2004-07-12
    body: "Please have more trust in the KDE developers. IMHO they did pretty well so far. :-)"
    author: "Christian Loose"
---
KDE is known to be remarkably configurable. Unfortunately up till now there was no GUI application exposing that. The problem got more evident with the coming of the wonderful <a href="http://developer.kde.org/documentation/tutorials/kconfigxt/kconfigxt.html"> KConfig XT</a> framework. Developers and <a href="http://www.kde.org/areas/sysadmin/">administrators</a> wanted to have nice graphical tools to edit and create configurations. KDE developers listened. We introduce three applications which let administrators and developers take full control over their desktops.







<!--break-->
<p><a href="http://extragear.kde.org/apps/kiosktool.php">Kiosk Admin Tool</a> - A Point & Click tool for system administrators to enable KDE's <a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdelibs/kdecore/README.kiosk?rev=KDE_3_2_BRANCH&content-type=text/x-cvsweb-markup">KIOSK features</a> or otherwise preconfigure KDE for groups of users. </p>

<p><a href="http://extragear.kde.org/apps/kconfigeditor.php">KConfigEditor</a> - First preview release of a desktop configurator. Note that this is a preview release and while the version released along KDE 3.3 will support editting of GNOME configuration and exporting changes as KJSEmbed scripts - this version doesn't.</p>

<p><a href="http://extragear.kde.org/apps/kcfgcreator.php">KCfgCreator</a> - Application which should be used by developers to create configurations for their applications. It can be used to migrate older applications to KConfig XT. It allows developers to use KConfig XT without knowing anything about it.</p>

<p>Please note that during <a href="http://conference2004.kde.org/">KDE Community World Summit 2004</a> prominent KDE hackers Waldo Bastian and David Faure will give a <a href="http://conference2004.kde.org/tut-kiosk.php">one day tutorial</a> about mass administration of KDE Desktop Workstations.</p>





