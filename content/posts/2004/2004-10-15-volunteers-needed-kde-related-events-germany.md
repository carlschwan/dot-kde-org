---
title: "Volunteers Needed for KDE Related Events in Germany"
date:    2004-10-15
authors:
  - "kpromo"
slug:    volunteers-needed-kde-related-events-germany
comments:
  - subject: "Further events in Germany..."
    date: 2004-10-16
    body: "You are also welcome to join the booth at the Linux-Info-Tag (30.10., http://www.linux-info-tag.de) in Dresden, and, supposedly, the same day at the Practical Linux (http://www.practical-linux.de) in Giessen.\n\nSee you there\nFriedrich"
    author: "Friedrich W. H. Kossebau"
  - subject: "I will be glad to help"
    date: 2004-11-20
    body: "I am looking foward to hear from you,\nMacouko"
    author: "shadrack ouko"
---
During October a lot of conferences and events in Germany will take place 
where KDE will be present. The KDE Project is still in urgent need for some 
volunteers to staff our booth at <A href="http://www.systems-world.de/">Systems Fairs</A> (18.-22.10.2004) in Munich, <a href="http://www.berlinux.de/">Berlinux</a> (22-23.10.2004) in Berlin and <A href="http://www.linuxworldexpo.de/">LinuxWorldExpo </A>(26.-28.10.) in Frankfurt. If you would like to help and present KDE please send an email to the <a href="mailto:kde-promo@kde.org">KDE promo mailinglist</a>.


<!--break-->
