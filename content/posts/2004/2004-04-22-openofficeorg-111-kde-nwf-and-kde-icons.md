---
title: "OpenOffice.org 1.1.1 with KDE NWF and KDE Icons"
date:    2004-04-22
authors:
  - "jholesovsky"
slug:    openofficeorg-111-kde-nwf-and-kde-icons
comments:
  - subject: "great, thanx!"
    date: 2004-04-22
    body: "but just d/l it... hopefully it's great too if I work with it :-)\n\nNow, I'm using StarOffice7update2. Can I implement the addon features from StarOffice7 into this ooo-kde release?\n\nHmm, now I'm think a bit more after this hard day. Is this version multilanguage? I need a german version...\n\nThanx for your answers!\n"
    author: "anonymous"
  - subject: "Re: great, thanx!"
    date: 2004-04-22
    body: "What features? For non-code parts I can say that I have archived samples/templates/gallery/fonts of the last Star Office Beta and decompress them everytime into the designated OOo target directory before installing OOo. Works fine."
    author: "Anonymous"
  - subject: "Nice"
    date: 2004-04-22
    body: "Tabs don't look alright, but otherwise good stuff. Looking forward to OpenOffice.org 2.0 with further integration (native feel, file dialog, printer dialog, ...)."
    author: "Anonymous"
  - subject: "Questions about OpenOffice.org version 2"
    date: 2004-04-22
    body: "Can anyone tell me if version 2 is going to be using GTK, QT or will it continue to use the existing toolkit?\n\nI've heard the currently when you open Writer - Calc, Draw, etc. is also running in the process in the background.  Will the apps be separated to run in their own process and only when called?\n\nIf anyone can answer these questions or give me a link to a resource it would be appreciated.\n\nThanks."
    author: "Darin"
  - subject: "Re: Questions about OpenOffice.org version 2"
    date: 2004-04-22
    body: "<a href=\"http://tools.openoffice.org/releases/q-concept.html\">\nhttp://tools.openoffice.org/releases/q-concept.html</a>\n"
    author: "Anonymous"
  - subject: "Re: Questions about OpenOffice.org version 2"
    date: 2004-04-22
    body: "They don't seem to mention KDE integration anywhere."
    author: "OI"
  - subject: "Re: Questions about OpenOffice.org version 2"
    date: 2004-04-22
    body: "Sun are probably preferring Gnome integration, but Open Office will never be a Gnome app. Why? Open Office has to run on Windows and Macs, and fit in. KDE integration? That's what this project is about."
    author: "David"
  - subject: "Re: Questions about OpenOffice.org version 2"
    date: 2004-04-23
    body: "Wouldn't it make that much more sense, then, to use QT since it can be compiled for several platforms easily?"
    author: "Mike Tangolics"
  - subject: "Re: Questions about OpenOffice.org version 2"
    date: 2004-04-23
    body: "No, as the OOo own widget set is LGPL and QT us either commercial, QPL or GPL."
    author: "anno"
  - subject: "Re: Questions about OpenOffice.org version 2"
    date: 2004-04-24
    body: "GTK is available on windows to, so it doesn't have to be a full gnome app (or KDE app, I don't mind) , as long as it uses GTK or QT."
    author: "Kukeltje"
  - subject: "Re: Questions about OpenOffice.org version 2"
    date: 2004-04-22
    body: "Yup, because /Sun/ doesn't have any plans to do much KDE integration. OpenOffice however, is a open project, and other people can do it, like Novell is with Jan. "
    author: "anon"
  - subject: "Re: Questions about OpenOffice.org version 2"
    date: 2004-04-22
    body: "It isn't going to focus on any toolkit - Open Office is multi-platform."
    author: "David"
  - subject: "Re: Questions about OpenOffice.org version 2"
    date: 2004-04-22
    body: "GTK and QT are multi-platform toolkits so that would not hinder OOo from using one of them.\n\nI've heard rumors of them moving to GTK for version 2 but I haven't seen it confirmed."
    author: "John"
  - subject: "Re: Questions about OpenOffice.org version 2"
    date: 2004-04-22
    body: "> I've heard rumors of them moving to GTK for version 2 but I haven't seen it confirmed.\n\nYeah, OpenOffice 2.0 will allow native frontend. Sun is planning to develop Windows, GNOME (gtk), OSX, Swing, and possibly other frontends. Novell/SUSE is sponsoring Jan to do the KDE NWF and future native KDE/Qt frontend. "
    author: "anon"
  - subject: "Screenshots"
    date: 2004-04-22
    body: "Erm - wouldn't it be much nicer if these screenshots were saved as PNG?\nThese JPGs look really ugly...\n\nBut anyway - nice job!\nI'm now waiting for a german version :-)"
    author: "SegFault II."
  - subject: "Re: Screenshots"
    date: 2004-04-22
    body: "...never use the native language in your applications! If you use another language (eg. english instead of german) then you can improve your skill there. I for myself have KDE running in spanish since a month so I practice words like a background-process ;-) Klappt echt ganz gut soweit - man kennt ja eh fast alle Kn\u00f6pfe und Dialoge auswendig *g*"
    author: "Andreas Pietzowski"
  - subject: "Not using your native lang"
    date: 2004-04-23
    body: ".. Great!\n\nalthough I see some sence in this (as meanwhile I am used to communicate in english), you will get a lot of new user, promoting this idea.\n\nEver thought of the idea to *work* with your desktop?"
    author: "Andre"
  - subject: "Re: Screenshots"
    date: 2004-04-23
    body: "Ya know, there are a lot of people still on modems...  \n\nI'm one of them.\n\nPlease realize that you are actually in the minority with your broadband connection."
    author: "Xanadu"
  - subject: "Re: Screenshots"
    date: 2004-04-23
    body: "Please realize that broadband connections are the future *everywhere*.  I like the bright and shiny broadband future and I'm curious what more it holds.  You might like it, too."
    author: "he-sk"
  - subject: "Re: Screenshots"
    date: 2004-05-06
    body: "These JPG's look completely normal! Why should anyone be made to download a huge PNG to see exactly the same thing but waiting far longer."
    author: "Piotrek"
  - subject: "Looks very nice, but installer doesnt work for me"
    date: 2004-04-22
    body: "/home/bni/Temp/sv001.tmp/setup.bin: error while loading shared libraries: libstartup-notification-1.so.0: cannot open shared object file: No such file or directory\n\nInstallation Completed\n\nUsing current Debian unstable"
    author: "bni"
  - subject: "Re: Looks very nice, but installer doesnt work for me"
    date: 2004-04-22
    body: "Try to install on the command line: ./install --prefix=PREFIX"
    author: "Anonymous"
  - subject: "Re: Looks very nice, but installer doesnt work for me"
    date: 2004-04-22
    body: "doesn't work here"
    author: "Mathieu Chouinard"
  - subject: "Re: Looks very nice, but installer doesnt work for me"
    date: 2004-04-22
    body: "I had to install startup notification to make this work ;-)\nhttp://www.freedesktop.org/software/startup-notification/\n\nbut still it crashes (after being installed) when I move the window o oo writer in the screen..."
    author: "Giovanni"
  - subject: "Re: Looks very nice, but installer doesnt work for me"
    date: 2004-04-23
    body: "I am sorry to hear that :( Probably it will be safest for you to build yourself an own version; the binary package was build on a very up-to-date system (SUSE 9.1)."
    author: "Jan Holesovsky"
  - subject: "Re: Looks very nice, but installer doesnt work for me"
    date: 2004-04-23
    body: "mmm...\nbut I HAVE a very up-to-date system... :(\ngentoo 2004.0\ngcc 3.3.2\nlinux 2.6.5\nXorg (X11R6.7.0, based on xfree 4.4rc2)\nkde 3.2.2\nqt3.3.1\n\nmmm...if it's not a known bug...it's a very strange problem :|"
    author: "Giovanni"
  - subject: "Re: Looks very nice, but installer doesnt work for me"
    date: 2004-04-23
    body: "So maybe your system is too up-to-date? :) Please, could you try to build your own version? You already lacked the startup-notification, maybe some other incompatibility (with my build system) is biting you here..."
    author: "Jan Holesovsky"
  - subject: "Re: Looks very nice, but installer doesnt work for me"
    date: 2004-04-25
    body: "apt-get install libstartup-notification0\n\n;-)"
    author: "Huebi"
  - subject: "Re: Looks very nice, but installer doesnt work for me"
    date: 2004-08-07
    body: "Don't know if anybody is still paying attention to this thread... I installed the KDE-ified version previously on a Gentoo 2004.2 system, and it worked like a charm. Just today I tried to install it on a very similar system, and ran into the same troubles you are describing. \n\nThe solution: Install the regular version of OOo first (e.g. in /usr), and then the KDE version parallel to it (e.g. in /usr/local). Works like a charm for me.\n\nKlaus"
    author: "Klaus Voelker"
  - subject: "Great stuff :)"
    date: 2004-04-22
    body: "I hope KDE file and printing dialogs come soon too!\nI'll probably wait for SuSE 9.1 to get it. Nice work :)\n\nJ.A.\n\nP.S.: \nI might have mentioned this before, but here it goes again.\nThis is a really *nasty* bug for KDE OpenOffice users, my sis nearly lost an entire document over it. Only 3 votes so far, some more wouldn't hurt.\nDnD pictures from Konqueror to OODraw problem:\nhttp://qa.openoffice.org/issues/show_bug.cgi?id=23526\n\nThanks"
    author: "Jadrian"
  - subject: "Re: Great stuff :)"
    date: 2004-04-22
    body: "Don't forget that you can already use the KDE Printer Dialog, although with a small workaround. \n\nSpecify 'kprinter' as the printing command. No arguments.\n"
    author: "MathieuK"
  - subject: "User-settings are not saved"
    date: 2004-04-22
    body: "Hi,\n\nI installed OOo as root in /opt/OOo1.1.1 but then when I start it as user my settings are not saved. It works with OOo1.1.0 but not within 1.1.1.\n\nCan anyone reproduce this or does anyone have a hint how to get it workin? I searched in the usenet but only found the same problem reported under Win98 and I guess this is maybe due to other problems in that specific OS ;-)\n\nThanx for help\nPietz"
    author: "Andreas Pietzowski"
  - subject: "Re: User-settings are not saved"
    date: 2004-04-22
    body: "With a regular OOo binary install, you set it up at the command line with\n\n./setup -net\n\nto run it with multiple users. I don't know whether ooo-build allows a similar option on the ./configure line - must be there somewhere."
    author: "Dominic Cleal"
  - subject: "Re: User-settings are not saved"
    date: 2004-04-22
    body: "The linked binary has the same (ok, other look :-) ./setup as a regular OOo binary."
    author: "Anonymous"
  - subject: "Good, but..."
    date: 2004-04-22
    body: "...why does it not mimic the toolbar and pusbutton behaviour? i.e. the icons opn the toolbars do not move when pressed - and neither does the text on the pushbuttons.\n\nAlso, shouldn't the UI font be set from the KDE general font?"
    author: "Craig"
  - subject: "Re: Good, but..."
    date: 2004-04-22
    body: "Maybe because you didn't contribute the code for that feature yet? Get your ass in front of your computer and start coding! I would also like to see these improvements in the future ;)\n\nSCNR\nPietz"
    author: "Andreas Pietzowski"
  - subject: "Re: Good, but..."
    date: 2004-04-22
    body: "> ...why does it not mimic the toolbar and pusbutton behaviour?\n\nBecause the Native Widget Framework doesn't support it? OpenOffice.org 2.0 will allow this too."
    author: "Anonymous"
  - subject: "Re: Good, but..."
    date: 2004-04-23
    body: "But the original OO.o does move the icons on toolbars when pressed - and the same for the text on the buttons. So why is this changed?"
    author: "Craig"
  - subject: "Re: Good, but..."
    date: 2004-04-23
    body: "Not all styles look good when the text on the button changes, so it is safer not to move the text---that is why it is the default behavior with NWF. And nobody implemented passing of QStyle::PM_ButtonShiftHorizontal and QStyle::PM_ButtonShiftVertical (http://doc.trolltech.com/3.2/qstyle.html) to NWF so far; I'm adding it to my TODO."
    author: "Jan Holesovsky"
  - subject: "Re: Good, but..."
    date: 2004-04-23
    body: "OK - fair enough! Its just one of my pet peaves - the reason I never used GNOME in the GTK1.x days was because the icons on toolbars, and text on buttons, did not move! (Hence on my QtCurve styles I hacked the Gtk1 draw_sting function to get it to move the damn text on being pressed... never figured out how to do it for icons, :-( )\n\nSome styles embolden the text of the default button - anyway this could also be mimiced?\n\nIs there a way to set the OO.o UI scren font size? I've had to adjust the Tools/Options/View/Scale to 110% so that the OO.o UI is the same pixel size as the rest of KDE.\n\nWell, enven in its present state it looks fantastic - so much nicer than the horrible semi-windows style default look..."
    author: "Craig"
  - subject: "Grrr"
    date: 2004-04-23
    body: "I don't want to download the whole package again to get better KDE integration... Isn't there a patch? OR will future OOo versions eventually get --enable-kde build flags to enable this stuff?"
    author: "etoibmys"
  - subject: "Re: Grrr"
    date: 2004-04-23
    body: "An xdelta patch against the original OOo 1.1.1 tar[.gz] is still over 50MB.\nThe source patches are of course much smaller. :-)"
    author: "Anonymous"
  - subject: "Re: Grrr"
    date: 2004-04-23
    body: "Yes the source patches are smaller, but I guess a bulid would\ntake a lot of disk space. If I remember corrrectly several gigabytes.\nAnd also many hours to build."
    author: "Uno Engborg"
  - subject: "Re: Grrr"
    date: 2004-04-23
    body: "Do you mean a patch against the binary package, or the sources? If you mean the sources, you do not have to download OOo 1.1.1 once more, just copy it to src/ subdir of your ooo-build and configure ooo-build to use your version. ooo-build is the set of patches you are probably calling for.\n\nFor --enable-kde see http://www.openoffice.org/issues/show_bug.cgi?id=26571 (target milestone OOo 2.0); but I have to finish the KDE vclplug first."
    author: "Jan Holesovsky"
  - subject: "No More Formulas in Math"
    date: 2004-04-23
    body: "After installing the KDE look and feel for OpenOffice I can no longer see my formulas. E.g. the Sum looks like a square. How can I fix this ?"
    author: "Onno Hommes"
  - subject: "Re: No More Formulas in Math"
    date: 2004-04-27
    body: "Read http://dot.kde.org/1082652256/1082725387/1082972297/"
    author: "Anonymous"
  - subject: "French version"
    date: 2004-04-23
    body: "\nHello,\n\nI'd like to make a french version of Ooo-Kde with ooo-build. Does anyone know what should I change in ooo-build for this ?\n\nThank you,"
    author: "Laurent Rathle"
  - subject: "Re: French version"
    date: 2004-04-23
    body: "With ooo-build, you should get all the language versions out of the box after make install. If not, just install from <ooo-build>/build/OOO_1_1_1/instsetoo/unxlngi4.pro/XX/normal/, where XX is code for your language; I am sorry, I am not sure what it is for French at the moment."
    author: "Jan Holesovsky"
  - subject: "Re: French version"
    date: 2004-04-23
    body: "I still can't understand why OpenOffice dosen't use module for translations. I know this is possible because Mandrake does this way, but if you go to openoffice.org you have , for example, a complete english installation and a complete brazilian portuguese installation... what a waste...\nNow I've installed this shiny new openoffice with Qt widgets and it dosen't speak portuguese..."
    author: "Iuri Fiedoruk"
  - subject: "font in formula are broken"
    date: 2004-04-23
    body: "beautiful work but spadmin option for install new font are deleted and font in formula not working anymore. You not use this version if you need mathematical formula."
    author: "stefano ferraro"
  - subject: "Re: font in formula are broken"
    date: 2004-04-23
    body: "Argh... this is really a drawback... any hope to have this fixed?\nIn fact I depend on the formula editor, so this version of OO is a no-go \nfor me..."
    author: "Andrea Aime"
  - subject: "Re: font in formula are broken"
    date: 2004-04-26
    body: "I am sorry, I'm unable to reproduce:( Please could you try to add <your_KDEized_OOo_installation>/share/fonts/truetype to FontPath in your /etc/X11/XF86Config and restart your X server? Does it get better?"
    author: "Jan Holesovsky"
  - subject: "Re: font in formula are broken"
    date: 2004-04-27
    body: "Yeh adding <your_KDEized_OOo_installation>/share/fonts/truetype to my /etc/X11/fs/config and restarting XFS fixed the problem"
    author: "theboywho"
  - subject: "Word"
    date: 2004-04-23
    body: "Is it possible to edit the toolbars so that OO resembleds more Ms Word? Abiword does it but the engine is crap. We have the OO engine that is  very good. Is it possible to create a word style GUI for OO?"
    author: "Bert"
  - subject: "Re: Word"
    date: 2004-04-23
    body: "View->Toolbars->Customize..."
    author: "Jan Holesovsky"
  - subject: "Re: Word"
    date: 2004-04-24
    body: "This did not work for me. When I change the toolbar in writer it applies also for calc ecc. And the editing is very much limited."
    author: "gerd"
  - subject: "The source of this package?"
    date: 2004-04-23
    body: "Where can I download the source of this package? I really mean the source of this package, based on ximian's OOo, and with kde integration, not the more general OOo source, and KDE patches. I would like to see if I could make RPM packages."
    author: "Anonymous Coward"
  - subject: "Re: The source of this package?"
    date: 2004-04-23
    body: "ooo-build is really all you want, see http://kde.openoffice.org/kde-icons/index.html#download\nIt contains even a .spec file; hopefully it could save you some time."
    author: "Jan Holesovsky"
  - subject: "Keramik"
    date: 2004-04-23
    body: "please, I want a see it with keramik, plastik does not give a good impression."
    author: "somekool"
  - subject: "Re: Keramik"
    date: 2004-04-25
    body: "erm, where in the screenshots is plastik. i can't see any plastik. maybe thats because it isn't plastik."
    author: "Anonymous Coward"
  - subject: "which icons are better ?"
    date: 2004-04-23
    body: "anyone else thinks gnome icons (or default icons) look better and more professional than the kde icons ?\n\nwhats your opinion ???"
    author: "chris"
  - subject: "Re: which icons are better ?"
    date: 2004-04-23
    body: "My opinion is that you have a great opportunity to make them better. Please read http://kde.openoffice.org/kde-icons/faq.html; there is a pointer to that file even in the announcement."
    author: "Jan Holesovsky"
  - subject: "Re: which icons are better ?"
    date: 2004-04-23
    body: "Uhmmmm...errr... not really ;)\n\nIf you mean the ximian thingie, I dislike the icons.. not so bright, colorful, meaningful and sharp as these. Maybe you mistakenly installed the ximian version instead of the kde one? ;P"
    author: "uga"
  - subject: "Problems with Equations"
    date: 2004-04-23
    body: "I install normally, but when I equation editor don't work, several symbols (=, (, ), etc) disapears or are change to others estraneous symbols. I think that something maybe wrong with fonts.\nThadeu"
    author: "Thadeu"
  - subject: "Re: Problems with Equations"
    date: 2004-04-23
    body: "Yes, I can confirm this one. All my equations show up screwed. Well, I can live with kile meanwhile =)"
    author: "uga"
  - subject: "Re: Problems with Equations"
    date: 2004-04-26
    body: "I am sorry, I'm unable to reproduce:( Please could you try to add <your_KDEized_OOo_installation>/share/fonts/truetype to FontPath in your /etc/X11/XF86Config and restart your X server? Does it get better?"
    author: "Jan Holesovsky"
  - subject: "Houaaawwww"
    date: 2004-04-23
    body: "Hello all,\n\nwhat a WONDERFUL Open Office !\n\nI have just tried it on simpress and scalc and it works great.\nOK, I lost my settings but this is really great and \nthe integration with kde gives a pretty interesting feeling to the user.\nIt's really a wonderful job. All works for me and I even got the feeling that it is faster than ever ... Can it be ?\n\nI am really impatient for the enable kde switch when building office or \neven beter : a gentoo package when kde is in you USE variable...\n;)\n\nYou make linux friendly !\nThanks !\nSteff\n"
    author: "Steff"
  - subject: "great..."
    date: 2004-04-23
    body: "...but the menus aren't qt/kde native, too bad :(\nMut this shows that the 2.0 version with complete KDE suppoer will shine even more than this nice version."
    author: "Iuri Fiedoruk"
  - subject: "OOo and KDE"
    date: 2004-04-24
    body: "Who needs KOffice if he can get such a good program as OpenOffice integrated in KDE. Looking forward to OOo 2.0 it might be a great success for Open Source Software."
    author: "Miesgram"
  - subject: "Re: OOo and KDE"
    date: 2004-04-24
    body: "Although I agree that it is better to have one great app  rather than two avarage ones, I cannot agree that KOffice is useless - I actually use it more than OOo in my every day work, esp. for small documents and printing tasks, because it so fast and damn KDE-ish; I think its very unfair to KOffice develppers to say - who needs this sh*t."
    author: "Petar"
  - subject: "Re: OOo and KDE"
    date: 2004-04-26
    body: "Printing tasks? No KDE applications prints well for me -- OO is much better in this respect. See attached for an example.\n\nMatej"
    author: "Matej Cepl"
  - subject: "Re: OOo and KDE"
    date: 2004-04-26
    body: "Sorry the attachement did not attach :-)."
    author: "Matej Cepl"
  - subject: "Re: OOo and KDE"
    date: 2004-04-27
    body: "Sorry, attachements just don't work. So the PDF is available on http://www.ceplovi.cz/matej/tmp/print.pdf.\n\nMatej"
    author: "Matej Cepl"
  - subject: "Re: OOo and KDE"
    date: 2004-04-27
    body: "I agree - for most things KWord is just plain quicker but I do keep OOo around for some things, such as being able to save to MS Word format, something that is lacking in KWord unfortunately.\n\nKWord's integration with the spellchecker and the print manager are definite pluses.\n\nThe one thing that both are lacking is WordPerfect's \"Reveal Codes\" feature. I'm not talking about \"Show tabs & returns\" either - it's closer in concept to looking at raw HTML or TeX. It's especially useful for cleaning up MS Word documents..."
    author: "David P James"
  - subject: "Re: OOo and KDE"
    date: 2004-04-24
    body: "OOo can not never be as integrated into KDE as a pure KDE program. Plus, I have a feeling that once KOffice is fully developed, it will still be faster and work nicer then OOo will at that time."
    author: "Ian Monroe"
  - subject: "Re: OOo and KDE"
    date: 2004-04-24
    body: "My KWord is very very quick... I didn't even bother with oo this time.  OpenOffice is too slow and it's .doc rendering is not very good.  I tried MSWord under Office Wine and not only did it render everything 100% (of course) it also started right up when I clicked the icon.  This new KDE look/feel is a big boost, but I will not even try it unless it gets alot faster.  The problem is they tried building a nice house on sand.  Their base code is what sucks... and it is too late to do anything about it.  KWord has impressed me with its .doc rendering.  I bet OO has gotten better... but I wouldn't know because, like I said a million times, it is too slow to take seriously."
    author: "Brandito"
  - subject: "Re: OOo and KDE"
    date: 2004-04-25
    body: "Yer, I think KOffice will get better. The underlying OO code is crap. But, I think KWord etc. will need to become a little bit more MS Word/OO Writer looking if it is to be more widely accepted."
    author: "David"
  - subject: "I would love to use this ..."
    date: 2004-04-24
    body: "... but it won't let me install because my system is stil glibc 2.2 based.\n\nWould it be possible to provide a version that doesn't require 2.3? I'm sure that there are a lot of people out there who are running older systems and who won't go to all the trouble of the non-trivial glibc update just for this nice but not essential package.\n\nThis is not a demand, just a suggestion."
    author: "Flitcraft"
  - subject: "Re: I would love to use this ..."
    date: 2004-04-30
    body: "Yes, I downloaded the whole thing.... and well it requieres glibc 2.3.\n\nIs there a chance to have a glibc 2.2 version ???\n\nOpenOffice.org 1.1.1 standard, works with glibc 2.2, so this one should have"
    author: "mcanedo"
  - subject: "nice work"
    date: 2004-04-24
    body: "Running SuSE 9 Pro with KDE 3.2.2 - works extremely well. Very fast! I plan on installing it at work on Monday.\n\n :)"
    author: "Robert MacEwan"
  - subject: "speed???"
    date: 2004-04-26
    body: "What we really want to know is... does it start faster??? Or does it still take over 30 seconds just to start one word processor????!!"
    author: "anonymoussy"
  - subject: "Couldn't you warn for size?"
    date: 2004-05-05
    body: "I already had Ooo1.1.1. running, and I started installing the icons because I do not like the look of the standard version. I was not amused when I found out that after downloading and installing a couple of extra needed progs (pkg-config and ImageMagick) to run ./configure, the ./download lets me download hundreds of MB, just for a couple of icons. I do not need to install the whole Ooo program again.\n\n1) Couldn't you warn about the file sizes to be downloaded before? Not everyone has an unlimited broadband connection.\n2) I suggest only to install a replacement icon set on top of the original Ooo. That should not be more than a few MB's"
    author: "Jack"
  - subject: "Re: Couldn't you warn for size?"
    date: 2004-05-05
    body: "Why didn't you use the precompiled install set?\n\n1) Done, warning added to http://kde.openoffice.org/kde-icons/index.html#download\n2) The icons are compiled into the OOo resources and they need changes in the sources to work properly. It is safer to distribute the complete install set."
    author: "Jan Holesovsky"
  - subject: "Cool.  Please add an option for small icons"
    date: 2004-05-06
    body: "Is there an option to use small icons on the toolbars?\nDefault icons are colorful but eat too much space IMHO"
    author: "npak"
  - subject: "Re: Cool.  Please add an option for small icons"
    date: 2004-05-07
    body: "Yes, Tools->Options...->OpenOffice.org->View->Icon size; set to \"Small\" and you'll get 16x16 icons."
    author: "Jan Holesovsky"
  - subject: "Re: Cool.  Please add an option for small icons"
    date: 2004-05-11
    body: "Thank you, Jan.  I found it.  Sorry for stupid question."
    author: "npak"
  - subject: "segmentation fault"
    date: 2004-05-08
    body: "When I run soffice it runs.\nWhen I try to open a document, or create a new one, soffice crashes/\n\nThis is the output from the terminal:\n[roie@localhost roie]$ soffice\nsoffice.bin: WARNING: KXMLGUIClient::setXMLFile: cannot find .rc file soffice.binui.rc\nsoffice.bin: WARNING: KXMLGUIClient::setXMLFile: cannot find .rc file soffice.binui.rc\nsoffice.bin: WARNING: KXMLGUIClient::setXMLFile: cannot find .rc file soffice.binui.rc\nSegmentation fault\n\nI use mandrake 10 Community and 2.4.25 kernel.\n\nWhat could be done against that?\n"
    author: "roie"
  - subject: "Install problem"
    date: 2004-05-15
    body: "'/home/opt/OOo_1.1.1_kde_LinuxIntel_Install/setup'\nglibc version: 2.3.3\n/home/ego/tmp/sv001.tmp/setup.bin: error while loading shared libraries: libstartup-notification-1.so.0: cannot open shared object file: No such file or directory"
    author: "alain2"
  - subject: "Re: Install problem"
    date: 2004-05-22
    body: "Install libstartup-notifcation."
    author: "Fred Emmott"
  - subject: "locale"
    date: 2004-05-27
    body: "can i make this version in french...?\nwhat do i need localize'it in french?\ncan i juste copie over my french openoffice.org already installed, or jsute need to drag over sum files?\n\nthanks to for helping me with this isue.\n"
    author: "brainstorm"
  - subject: "Re: locale"
    date: 2004-05-28
    body: "The icons as well as the localizations are compiled into the resource files, so when you use the French resource files, you will have the original icons. You have to recompile to get the KDE icons together with the French localization."
    author: "Jan Holesovsky"
  - subject: "Great !"
    date: 2004-09-21
    body: "This is fantastic! Really, the KDE team is awesome! \n\nOne of the things I didn't like about OO was its look and feel. I know this is trivial, but after working with MS Office, it looked a bit pale and poor. \n\nI really hope that major distributions (hello Mandrake) will adopt it in the next releases.\n\n"
    author: "Mistinthenight"
---
OpenOffice.org 1.1.1 with KDE Native Widget Framework and KDE Crystal icon set is now available for download. If you are interested in screenshots, you can have a look at pictures of <a href="http://static.kdenews.org/mirrors/www.openoffice.org/files/documents/159/1804/NWF_icons_writer.jpg">OOo Writer</a>, <a href="http://static.kdenews.org/mirrors/www.openoffice.org/files/documents/159/1805/NWF_icons_calc.jpg">OOo Calc</a> or at a <a href="http://static.kdenews.org/mirrors/www.openoffice.org/files/documents/159/1806/NWF_icons_screen.jpg">KDE desktop running OOo</a>.





<!--break-->
<p>Download: <a href="http://download.kde.org/download.php?url=packages/ooffice/OOo_1.1.1-1_kde_LinuxIntel_Install.tar.gz">Installation set for Linux i386</a> (~80MB).</p>

<p>Features:
<ul>
<li>The newest stable version of <a href="http://www.openoffice.org/">OpenOffice.org</a>.</li>
<li>The KDE support is built using Ximian's <a href="http://ooo.ximian.com/ooo-build.html">ooo-build</a>, a tool which simplifies the process of OOo building a lot. See the <a href="http://kde.openoffice.org/kde-icons/index.html#download">build instructions</a> if you want to build it yourself.</li>
<li><a href="http://kde.openoffice.org/nwf/index.html">KDE native widget framework</a>; see the <a href="http://dot.kde.org/1073557624/">older story</a> about it.</li>
<li><a href="http://kde.openoffice.org/kde-icons/index.html">KDE (Crystal) icons for OOo</a>.</li>
</ul> </p>

<p>If you want to help, there is a great opportunity for you to improve the icons. Please read the <a href="http://kde.openoffice.org/kde-icons/faq.html">icon FAQ</a> on where to start.</p>

