---
title: "KDE 3.4 SVG Wallpaper Challenge"
date:    2004-12-18
authors:
  - "fmous"
slug:    kde-34-svg-wallpaper-challenge
comments:
  - subject: "Advertising contests"
    date: 2004-12-18
    body: "Shouldn't this contests be advertised in non-kde related websites too? Good luck to everybody, it's great to see more and more quality artwork being contributed to KDE."
    author: "John Artwork  Freak"
  - subject: "Re: Advertising contests"
    date: 2004-12-18
    body: "I agree completely!"
    author: "Leverkusen"
  - subject: "Using system colors in SVG wallpaper?"
    date: 2004-12-18
    body: "Would it be possible to define some colors of a SVG wallpaper from the \"outside\"?  It would be nice if the artists could use the colors defined in the current color scheme by name, so that the background matches the window decoraton etc."
    author: "uddw"
  - subject: "Re: Using system colors in SVG wallpaper?"
    date: 2004-12-18
    body: "Dude - this is cool idea - can somebody post it to respondsible ppl?"
    author: "Abraxis"
  - subject: "Re: Using system colors in SVG wallpaper?"
    date: 2004-12-18
    body: "I think SVG uses CSS, so just use the system-color entities."
    author: "Carewolf"
  - subject: "Re: Using system colors in SVG wallpaper?"
    date: 2004-12-18
    body: "I didn't know that CSS(2) defines system color names. Thanks for pointing that out :)\nLooks like the icon engine doesn't support CSS2 system color names though. I've asked on kde-core-devel if it's still possible to add this for KDE 3.4. I wonder who decides if I can commit such a change in kdecore..."
    author: "uddw"
  - subject: "Awesome Idea, Can it be taken further?"
    date: 2004-12-30
    body: "I like it!!\n\nOn an a similar note, could this idea be taken even further (Expecialy for icons)\n\nSince svg is xml based, could we do variable substitution before rendering.\n\nSo we could include the name of the computer or user name rendered into the background, or the the name of a file rendered into its icon.  The preview of the file could be rendered into the icon also.  Easy for text files, I don't know what's involved for images though.  Would make for a really consitent look when previews are turned on.\n\nMany other things could be rendered into the background.  (rss feeds, weather, system log, todo-lists. etc,)"
    author: "Adam Ashenfelter"
  - subject: "Re: Awesome Idea, Another Notch!"
    date: 2004-12-30
    body: "OK, I'm brain storming in this spot.\n\nWhat about a svg based window manager theme.  So new themes for window managers are as easy as opening up Inkscape.  If the css can be set from the system, the theme will automaticly follow system color's.  Variable can be substituted to make titles or whatever.  \n\nJust brain storming here."
    author: "Adam Ashenfelter"
  - subject: "Silly question, but..."
    date: 2004-12-18
    body: "I know kde-3.4's svg-stuff is still using/needing libart, but could kde-4.0 be compiled to use cairo/glitz for ksvg/ksvgiconengine instead of the libart?"
    author: "Nobody"
  - subject: "Re: Silly question, but..."
    date: 2004-12-18
    body: "KDE 4 will likely use libagg.  Little known about but allegedly the best.\n\n"
    author: "Jonathan Riddell"
  - subject: "Re: Silly question, but..."
    date: 2004-12-18
    body: "If KCanvas/KSVG2 will be ready for KDE 4, it will be possible to use Libart, AGG2 or Cairo."
    author: "Sven Langkamp"
  - subject: "Thanks1"
    date: 2004-12-19
    body: "On behalf of everyone who owns a nonstandard (4x3) screen (a whole lot of laptops, widescreen monitors, multiple monitors, etc), I give a big thanks! "
    author: "Benjamin Meyer"
  - subject: "SVG Icons?"
    date: 2004-12-19
    body: "When will KDE support SVG-icons?"
    author: "Janne"
  - subject: "Re: SVG Icons?"
    date: 2004-12-19
    body: "In 2001. \n\nWe don't ship SVG icon sets because they are slower than bitmap sets, but that doesn't mean they don't work. The first 3rd party sets are even starting to appear on www.kde-look.org"
    author: "Carewolf"
  - subject: "The best wallpappers.."
    date: 2005-01-23
    body: ".. are not branded. I much prefer a nice photograph of a flower, an animal, a landscape, a face, over a KDE wheel. It will be interesting to see what images are submitted to this contest. SVG as format is suitable for logos, but at the same time logos are boring.\n\nI think KDE is so strong on its own that it does not need much branding where commercial companies would use that. I hope for some original artwork that is strong on its own!"
    author: "Claes"
  - subject: "Re: The best wallpappers.."
    date: 2005-01-27
    body: "Unfortunately kde-look has no sections for wallpapers..."
    author: "gerd"
  - subject: "Re: The best wallpappers.."
    date: 2005-04-18
    body: "Hu? Have a second look. :-)"
    author: "Anonymous"
---
As you <A href="http://www.kdedevelopers.org/node/view/739">might have heard</A>, KDE 3.4 will support SVG wallpapers. That's why <A href="http://www.kde-look.org/">KDE-Look.org</a> has announced the <A href="http://www.kde-look.org/news/index.php?id=145">SVG Wallpaper Challenge</A>, where 4 wallpapers will be selected to be bundled with the KDE 3.4 release. There are actual prizes to be won, and the first place winner will be able to chose between a nVIDIA GeForce FX5700LE video card or a 120GB hard drive, graciously provided by <A href="http://www.corefunction.com/">Core Function</A>. The second place winner will receive a t-shirt featuring their wallpaper, provided by <a href="http://www.cafepress.com/revelinux">Revelinux Gear</a>.  Be sure to check the KDE-Look.org website for the guidelines and rules. This contest closes on December 31st.






<!--break-->
<h3>amaroK Icon Contest</h3>

<p>Also note, the amaroK Icon Contest has been extended until December 31st. So this is your chance to make a killer icon for the next generation of <A href="http://amarok.kde.org/">amaroK</A>. Why, you say?  The current amaroK icon simply looks too much like the kmix icon, so we would like to have something more original for the next release. You might even win a prize: a new amaroK t-shirt featuring the winning icon! Be sure to <A href="http://www.kde-look.org/news/index.php?id=140">check the criteria and other useful information</A> which can be found at the KDE-Look.org website. </p> 






