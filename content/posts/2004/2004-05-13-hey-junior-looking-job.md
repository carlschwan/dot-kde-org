---
title: "Hey Junior... Looking for a Job?"
date:    2004-05-13
authors:
  - "agroot"
slug:    hey-junior-looking-job
comments:
  - subject: "Let's see if this will work"
    date: 2004-05-13
    body: "First of all, I didn't say I'd teach anybody anything, I said I'd help. Which means I don't intend to teach anybody how to fish, but I can give you the right fishing pole and tell you where to find fish. You'll need to handle the fishing yourself, sorry, I don't intend to do any \"C++ for newbies\" course.\n\nI added a comment to this specific bugreport (http://bugs.kde.org/show_bug.cgi?id=78950) which I believe should include enough information for somebody who'd be really interested in KDE development and should make it quite simple. I can help with real problems with it, and I'll review the final patch, but don't expect me to solve compile errors or help with setting up KDevelop.\n\nLet's see if this experiment will work.\n"
    author: "Lubos Lunak"
  - subject: "Re: Let's see if this will work"
    date: 2004-05-13
    body: "Second of all, nobody said that you said that you'd teach anybody anything!  ;-)\n\nHaving read your posting to the list, I don't think the news story is an unfair summary of your excellent idea...   ;-)"
    author: "anon"
  - subject: "Re: Let's see if this will work"
    date: 2004-05-13
    body: "Sorry, Lubos, if \"teaching them to implement it\" sounds like it's a promise by you to do a C++ course or whatever. I intended it to mean just some basic handholding, pointing folks to the right source and header files, etc. -- just like what you've done with your extra comment in the bug report.\n\nI have done similar things for other apps, and people really do show up who, ith a minimum of handholding, implement neat and useful features. Often just providing a roadmap in the jungle of source files and everything is all they need."
    author: "Ade"
  - subject: "Can't resist this..."
    date: 2004-05-13
    body: "Make a man a fire, and he'll be warm for a night.  Set him alight, and he'll be warm for the rest of his life!\n\n\nActually, I think that it's a very good idea for people to be able to ease themselves into contributing to open projects by doing such junior tasks.  People don't start with lots of experience, and making very visible contributions can be very daunting.\n\nIn some cases, newcomers who have tried to help have been effectively beaten up by more experienced people who have no patience.  This doesn't seem to be so much of a problem with KDE, but certain other projects are well known for this!\nI still don't think it wise to let people who know nothing about programming loose on the source, so I don't think that this should be a hand-holding excercise.\n\n-- Steve"
    author: "Steve"
  - subject: "Re: Can't resist this..."
    date: 2004-05-13
    body: "> In some cases, newcomers who have tried to help have been effectively beaten up \n> by more experienced people who have no patience. This doesn't seem to be so \n> much of a problem with KDE, but certain other projects are well known for this!\n\nIn fact, the potential for being beat up has led me personally to avoid any contact with KDE developers at all. Who wants to be belittled in public? I just want to help make software that works.\n\nIts also no fun writing a patch to have it languish in the bug tracker. Having it labelled \"wontfix\" is bad, but having it just sit there for a couple of years is worse. If a mentoring process is set up, it must be clear that there is some responsibility to the JJ not only from the mentor but from everyone.\n\n> I still don't think it wise to let people who know nothing about programming\n> loose on the source, so I don't think that this should be a hand-holding \n> excercise.\n\nI don't think we're talking about those new to programming, but rather people that are new to KDE programming. It just needs to be made clear the KDE 'mentor' isn't going to help you with C++ basics, just KDE processes and procedures. Some of the \"KDE way\" isn't at all obvious.\n"
    author: "would-be"
  - subject: "Re: Can't resist this..."
    date: 2004-05-13
    body: "So you haven't actually been belittled, you are just afraid that you will be?\n\nPeople need to have thicker skins.\n"
    author: "John Tapsell"
  - subject: "Re: Can't resist this..."
    date: 2004-05-13
    body: "or a greater sense of adventure. personally, i can't remember when i've ever seen someone publicly belittled by a KDE developer for code they've contributed in good faith ... \n\n\"Do you have any messages without FUD?\"\n\"Well... we have facts, figures, FUD, statistics, FUD and FUD. That's only got a bit of FUD in it.\"\n\"But I don't LIKE FUD!\""
    author: "Aaron J. Seigo"
  - subject: "Re: Can't resist this..."
    date: 2004-05-13
    body: "I found it very easy to get into. I looked through a programs todo list I was interested.\n\nSaid I'd like take one of the items on the list to the maintainer, he said go for it...\n\nAfter a few bugs and some helpful pointers later, It was committed. Now Several months later, I'm working on other parts, and doing quite a lot of stuff.\n\nI've never been belittled even though I made quite a lot of mistakes at first, ( okay I still make mistakes :) ). KDE developers where the nicest bunch I've been involved in."
    author: "Leon Pennington"
  - subject: "Wow!"
    date: 2004-05-13
    body: "This is very very cool! I've spent hours scouring Bugzilla in search of a job that we could now qualify as JJ. I want to have fun and learn while fixing it, and my knowledge of KDE is limited. It was very hard to find such a bug. Now they are just a click away!\n\nThanks, very nice idea!"
    author: "Simon Perreault"
  - subject: "Re: Wow!"
    date: 2007-05-22
    body: "uoi;ui;oi;i;"
    author: "gyhmkyg,"
  - subject: "Junior Job #1"
    date: 2004-05-13
    body: "Bugzilla is currently broken. Please try again later.\n\nLOL\n\n...but seriously why hasn't the open source world come up with a better bug tracking system then the fragile, unwieldy mess that is bugzilla?"
    author: "General Zod"
  - subject: "Re: Junior Job #1"
    date: 2004-05-14
    body: "There's a very good reason that mozilla went down.  Read some kde-core-devel archives if you like to actually know what you're talking about.\n\nBugzilla is the best BTS available, and it's normally pretty good, this is the first time I've known b.k.o to go down since the switch to bugzilla.  I don't know where you get the idea that bugzilla is bad or even \"unwieldy\"."
    author: "domi"
  - subject: "Sweet, very sweet"
    date: 2004-05-13
    body: "I've been looking for a way to get into KDE development for a while. I maintain Kaffeine for Debian but that's not the same. I'll start looking at the JJ's now, but does anyone have a suggestion on a book or some other source of information on Qt/KDE programming? Everything I've found so far seems to be for KDE 2."
    author: "Zack Cerza"
  - subject: "Re: Sweet, very sweet"
    date: 2004-05-13
    body: "there are a number of tutorials and bits of info on developer.kde.org and the Qt and KDE API docs are rather good. many of us are also on IRC in #kde-devel on irc.kde.org (part of the FreeNode network)"
    author: "Aaron J. Seigo"
  - subject: "ive heard..."
    date: 2004-05-13
    body: "that if you give a man a fish, he eats for a day... \nif you teach a man to fish, he complains at you for being so selfish and not sharing your fish ;)\n"
    author: "Ian Reinhart Geiser"
  - subject: "Give a man fire..."
    date: 2004-05-14
    body: "..and he be warm for a day.\n\nSET a man on fire, and he'll be warm for the rest of his life. "
    author: "Roey Katz"
  - subject: "Bugs?"
    date: 2004-05-13
    body: "Why only for wishlist items?"
    author: "Anonymous"
  - subject: "Re: Bugs?"
    date: 2004-05-13
    body: "a) implementing a wish is more rewarding for a newcomer developer\nb) if a bug is easy to fix then the maintainer should do it immediately and not wait for someone else to do it. Shouldn't he?"
    author: "Christian Loose"
  - subject: "a great idea..."
    date: 2004-05-13
    body: "This is really a great idea (marking up the reports)!  I have been looking at how to get started with KDE/QT programming, but I have been having difficulty knowing where to start.  I have been helping test features, report bugs, etc, but what I really want to do is get down and program some lines of code.  What a perfect was to start!\n\nThe reason I believe the JJ: prefix will be effective is because not only does it point out projects which may be more on the trivial side to implement, but also because they communicate that the author/maintainer would rather see someone else concentrate on that task.  In a sense, you won't be stepping on anyone's toes, at least not the maintainer of the report.  I know that personally I might report a bug that I know I will implement in the next release.  So it happens.\n\nI must object to the title of Junior, though.  I have been a programmer for many years and have worked in an assortment of programming environments.  Just because I want to learn KDE, doesn't mean I am a \"junior.\"  Perhaps a better title would be \"GYFW\" for \"Get Your Feet Wet.\""
    author: "Dan Allen"
  - subject: "Re: a great idea..."
    date: 2004-05-13
    body: "You can be a janitor instead of a junior.  :)\n\nBut a rose by any other name is still a rose ..."
    author: "mbucc"
  - subject: "things in progress"
    date: 2004-05-13
    body: "Is there something that can be checked to see if someone is already working on something?\n\nOr do we need to contact the owner all the time?\n\nMaybe it's best to leave a short message in the bug report that you're working on something so that someone else doesn't waste his or her time?"
    author: "tbscope"
  - subject: "Re: things in progress"
    date: 2004-05-14
    body: "> Is there something that can be checked to see if someone\n> is already working on something?\nNot really.\n\n> Or do we need to contact the owner all the time?\nYes.\n\n> Maybe it's best to leave a short message in the bug report\n> that you're working on something so that someone else doesn't\n> waste his or her time?\nYes, that would be the best solution."
    author: "Clarence Dang"
  - subject: "Great idea, and, are there any developers willing "
    date: 2004-05-16
    body: "Are there any developers willing to teach a class on... how to set up kdevelop?  Like a web course, or video download.\n\nI'm wanting to make a couple \"simple\" tweaks to the UI, but I'm finding that it takes a lot of head banging to learn how to use kdevelop.\n\nI'm hoping to get pykde up and running on my box, but it's always a version behind on gentoo, so it doesn't work at all for me :(\n\nThere is just so much stuff to be familiar with to even get started.\n\nI want to become a JJ grade developer (and more) , but that feels like a long ways off.\n"
    author: "Aaron Peterson"
  - subject: "Re: Great idea, and, are there any developers willing "
    date: 2004-05-16
    body: "The \"head banging\" usually takes place trying to wrestle with build system.\n\nI doubt a video will happen, but #kdevelop is usually responsive."
    author: "teatime"
---
Lubos Lunak <a href="http://lists.kde.org/?l=kde-core-devel&m=108438221522742&w=2">asked recently</a> on <a href="https://mail.kde.org/mailman/listinfo/kde-core-devel">kde-core-devel</a> what to do with <a href="http://bugs.kde.org/show_bug.cgi?id=78950">a little wishlist</a> item filed against KWin. 
It's a nice entry, well described, and
he even has some ideas about how to implement it --- but he also believes
that he could spend his time better by <i>teaching someone to implement
it</i> rather than do it himself. Give a man a fish, and he eats for a day.
Teach a man to fish, and he eats for a lifetime.



<!--break-->
<p>This is where <A href="http://bugs.kde.org/">KDE's Bugzilla</A> comes in. 
As a convention -- no more than that --
developers can mark wishlist items with "JJ:" (JJ stands for Junior Jobs) in the summary to indicate that
these jobs are suitable for people looking for some initial programming
challenge to get acquainted with KDE programming. A <a
href="http://bugs.kde.org/buglist.cgi?short_desc_type=allwordssubstr&short_desc=JJ:&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&bug_severity=wishlist">simple
search</a> in Bugzilla can show aspiring users what kind of jobs are
available for people just starting out.</p>

<p>With that said, I'd like to invite - or remind - developers to mark wishlist
items that are suitable beginning jobs with the JJ: "seal of approval" so
that everyone -- budding young programmers, busy developers, folks with
wishes -- profits. (Users that enter new wishlist items with the JJ: mark of
course intend to implement the feature themselves, and are just "staking a
claim" in Bugzilla).</p>



