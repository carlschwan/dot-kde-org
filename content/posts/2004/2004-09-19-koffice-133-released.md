---
title: "KOffice 1.3.3 Released"
date:    2004-09-19
authors:
  - "ngoutte"
slug:    koffice-133-released
comments:
  - subject: "Nice work."
    date: 2004-09-19
    body: "Koffice is a needed app set. While I find errors in it, I use it more than OO. It is fast and IMHO, easier to use."
    author: "a.c."
  - subject: "Great!"
    date: 2004-09-19
    body: "I can't wait, I'm downloading it wight now. I love KOffice!\n"
    author: "lover"
  - subject: "I do love Koffice, but..."
    date: 2004-09-19
    body: "I use koffice at home exclusively and use it at my church for our \"powerpoint\" setup (I do hate it when people call it that).  The only thing I wait for in kpresenter is a true projector mode where I can still work with the slides while it displays things to my second \n\n"
    author: "Nathan"
  - subject: "Re: I do love Koffice, but..."
    date: 2004-09-20
    body: "why do you go to church? don't you know there is no God?"
    author: "yoppy"
  - subject: "Re: I do love Koffice, but..."
    date: 2004-09-20
    body: "why do you come here?  don't you know that everything is nothingness and that this place doesn't exist?"
    author: "ac"
  - subject: "Re: I do love Koffice, but..."
    date: 2004-09-20
    body: "actually there is a God but it's only in your mind which indeed is nothingness"
    author: "yoppy"
  - subject: "Re: I do love Koffice, but..."
    date: 2004-09-20
    body: "I can certainly see your point about your mind being void. You don't seem to be shamed of that either. However, i'd suggest that as a generic rule of thumb in life, keep this information to yourself. It really helps."
    author: "anonymous"
  - subject: "Re: I do love Koffice, but..."
    date: 2004-09-20
    body: "Maybe he goes to church to learn some manners.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: I do love Koffice, but..."
    date: 2004-09-20
    body: "Why don't you just take care of your own damn business, and let others take care of their own?"
    author: "Janne"
  - subject: "Re: I do love Koffice, but..."
    date: 2004-09-20
    body: "LOL!\nIf there is no God, WHAT created all things? The Big Bang? Oh then... and WHAT created the Big Bang? Another universe? Oh then... and WHAT created this other universe?... Etc...\n\nSomething that is called \"God\" exist because \"something\" created the universe.\nYou don't go to church because God exist, but because 2000 years ago a person saw that he was God and resuscitated and is Present here, now. Stop. You go to church because you meet someone that has that Person inside.\n\nThen, why can't I go to the church? I go only because a person called me and make me happy. For ever."
    author: "Emanuele"
  - subject: "Re: I do love Koffice, but..."
    date: 2004-09-20
    body: "Please, stop this!\nThere are people who believes in God, even having no proof about it.\nOthers don't accept this God thing if there is no credible proof about it.\n\nOver the years this two groups had never agreed, and this is certainly NOT the place to talk about it.\n\nStop it.\nThanks\n"
    author: "gabriel"
  - subject: "Re: I do love Koffice, but..."
    date: 2004-09-20
    body: "Not really sure how this all pertains fo KOffice, btu that being said... I bleleive in God, and I do have proof enough for me. For those of you who like it in print so that you can hold the proof (I guess the Bible is not good enough), try reading \"The Case for Christ\" by Lee Stroebel. He is a well seasoned investigative journalist who tried to disprove Jesus and his deity. In the process he actually proves it. After reading that, get back with me."
    author: "Challenger"
  - subject: "Re: I do love Koffice, but..."
    date: 2004-09-20
    body: "Errr... so what created God? :)"
    author: "Confused Guy"
  - subject: "Re: I do love Koffice, but..."
    date: 2004-09-20
    body: "I think the real question is: who created God?"
    author: "Anonymous"
  - subject: "Re: I do love Koffice, but..."
    date: 2004-09-20
    body: "I think the real question is how did this go from a question about a feature needed in Kpresenter to a debate about the validity of belief in God?"
    author: "Nathan"
  - subject: "Re: I do love Koffice, but..."
    date: 2004-09-26
    body: "http://dot.kde.org/1095608979/1095625674/1095643548/1095681945/"
    author: "Confused Guy"
  - subject: "Re: I do love Koffice, but..."
    date: 2004-09-20
    body: "> I think the real question is: who created God?\n\nWell, men of course who else?\nSome even say that god looks like men, so it must be men who created it, no?\n\nA part from this, how do you know whether if it is a what or a who which created god(s)?\nYou have better information than most of us for being so sure about this topic..\n"
    author: "RenoX"
  - subject: "Re: I do love Koffice, but..."
    date: 2004-09-20
    body: "but who created God? how dit He come into existence? the presence of a God doesnt solve the \"but what created ...\" problem."
    author: "superstoned"
  - subject: "Re: I do love Koffice, but..."
    date: 2004-09-20
    body: "yoppy, you certainly aren't required to believe in God. You aren't required to believe in anything, that doesn't make you better or worse then anyone else. But common courtesy dictates that you at least respect others who do. That is if you want them to respect your desire not to believe. I don't know for sure that \"God\" as we understand that word to mean, truely exists I just have faith that God exists just as you don't know for sure that \"God\" doesn't exist you just have faith that he doesn't. So see there, you do have faith, you do believe in something, you believe in anarchy. Which is ok I guess, sad, but ok."
    author: "bartman"
  - subject: "Re: I do love Koffice, but..."
    date: 2004-10-02
    body: "what is really sad, is that if you says something has faith in not believing in god.\n\nI don't need to believe or not to believe in god. god is just something more for people on the earth to talk about. I can have faith that Schumacher will won F1 world champion again, or that I could pay less tributes. The difference is that god is not part of the society. I live in a laic state: Spain, who doesn't care about religion. If you are catholic is good, if you are not, is also good. And religion does not take part of the government or the important thinks in life.\n\nSo please don't tell me that is sad not believe in god or even worse... that not believing in god is just.. anarchy. I like order, society and rules, I don't like anarchy. But I believe in my politicians, friends, police, and organizations... I don't belive in \"something\" that is ethereal. I've never believed in ghosts, so I don't believe in any kind of god... \nbut well... Afrodita was a godess and she was really pretty :-P\n\nOh I like very much the films of greek gods, they were great!\n\n"
    author: "a true KDE fan"
  - subject: "Re: I do love Koffice, but..."
    date: 2004-10-02
    body: "Try read good books like Davinci's code, and not fantasy books like the Bible.\nIf you like fantasy, the best book is the Lord of the Rings, not the Bible.\n\nOf course I don't believe in god (don't know why people write god with G, it's just a common word in the middle of a sentence, so use lowercase please)\n\nAnd I think my life is extremely short to think about believing in something absolutetly impossible to proove.\n\nSo try having more sex with your girlfriend (with condom), going to the mountains, enjoying life and freedom, family and friends, and you will discover\nthat believing in a god is just useless and timewasting.\n\nand try Gentoo Linux, too :-)\n"
    author: "a true KDE fan"
  - subject: "Re: I do love Koffice, but..."
    date: 2004-10-12
    body: "Your own words judge you. So many humans fail to understand infinity. Most of you are so dimensionless. You can't see past time. Can you even see time? Or are you only aware of it's exsistence because you were taught it in school.\n\nPerhaps some choose to ignore the possibilities and thereby limit themselves."
    author: "witness"
  - subject: "Re: I do love Koffice, but..."
    date: 2004-09-23
    body: "You can vote for\nhttp://bugs.kde.org/show_bug.cgi?id=39690\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "I love Koffice"
    date: 2004-09-19
    body: "I love Koffice. It is small, fast and full featured. I only wish that some benefactor would donate enough money to hire a dedicate team of 25 developers for two years.\n\nPay them well and create the best office suite the world has ever seen. We are headed in that direction but it would be nice to speed up the process.\n\nKeep up the great work. If I hit the jackpot, KDE will be getting  a big check."
    author: "Gonzalo"
  - subject: "Re: I love Koffice"
    date: 2004-09-19
    body: "If you love KOffice why don't you work on it instead of waiting to hit the jackpot and donate KDE a big check? KOffice definitely has a shortage of developers and contributors. You could for example help with Krita. Krita is a painting and image editing application for KDE and is part of KOffice. Many of the basic things like loading and saving images, basic painting ops like freehand painting, drawing lines etc. are already working. But some of the basic functionallity is still missing and some of these things are not hard to implement. Basically everyone with some basic knowledge of programming can help. I think another application that definitely needs some help is kspread. As far as I know KSpread only has one or two active developers right now and there are some plans to rework KSpread, so they can definitely need someone to help them."
    author: "Michael Thaler"
  - subject: "Re: I love Koffice"
    date: 2004-09-20
    body: "Well, unfortunately, not only KSpread has so few developers.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: I love Koffice"
    date: 2004-09-20
    body: "Only problem with that is I can't program good enough to contribute code, I'm horrible at writing so I can't do docs, not the best graphics designer either...\n\nI really need to some time just try and stress test the apps a lot and the report any bugs I can find and reproduce, that will help them some."
    author: "Corbin"
  - subject: "Re: I love Koffice"
    date: 2004-09-20
    body: "Well, a task could be to test the MathML import filter of KFormula with the MathML test suite, see http://lists.kde.org/?l=koffice-devel&m=109553329117502&w=2\n\nThe only problem is that a very recent KFormula is needed, as a bug made the output of the MathML import filter corrupt (even in KOffice 1.3.3).\n\nHave a nice day!\n"
    author: "Nicolas Goutte"
  - subject: "Re: I love Koffice"
    date: 2004-09-20
    body: "Adding Doxygen comments could be a task too for people understanding C++, but not wishing to develop. (Doxygen: http://doxygen.org )\n\nSometimes the comments already exist, but not in Doxygen format, sometimes there are in the .cpp files but we need them in the .h files and sometimes the existing Doxygen comments could be improved, by a brief description for example or by replacing plain text by the corresponding Doxygen command.\n\nHave a nice day!\n"
    author: "Nicolas Goutte"
  - subject: "Re: I love Koffice"
    date: 2004-09-20
    body: "Well, Your prospects in job market aren't very bright indeed ;-)"
    author: "piggy"
  - subject: "Re: I love Koffice"
    date: 2004-09-20
    body: "I've noticed that there are some things that can be done for Krita in bite-size   pieces, but other things demand at least a week of concentrated hacking. I had taken two days off to work on selections, with the idea of adding Saturday, so I would have three whole days. But then an issue arose at work, and I had to work on some obscure FreeBSD/MySQL/Java problem that day.\n\nI guess what I want to say is, I'd love to get a check that would enable me to buy five holidays from my employers to get something big done :-).\n\nif no checks are forthcoming, Michael is absolutely right: there's this big TODO list which has lots of small, easy tasks that can be done with nothing more than the Qt documentation and a little perseverance..."
    author: "Boudewijn Rempt"
  - subject: "Re: I love Koffice"
    date: 2004-09-21
    body: "Because I already work on other projects. And if your intent is getting people to contribute, then it would help a great deal if you put the accusatory tone down.\n\nPeople contribute when they can whatever they can give. Stop playing leader and lead with your actions. Stop pretending you know me or how many of my waking hours I already contribute to Free Sotware. Is that clear?"
    author: "Gonzalo"
  - subject: "Re: I love Koffice"
    date: 2004-09-21
    body: "We are not forcing anybody.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Big Apps"
    date: 2004-09-19
    body: "The big apps are not the word processor, spreadsheet and presentation apps. The big ones are apps like Kexi, Kivio and Krita. If any distro has any ambitions of cracking the desktop then they should contribute to these apps and put them together with their Open Office setups and integrate them."
    author: "David"
  - subject: "i would like koffice too, but..."
    date: 2004-09-19
    body: "it is not stable yet and doing work with it made me not very happy.\n\nI really like kformula and the other apps are \"high potential\" too. But at the moment larger project are better done by ooo"
    author: "Ramin"
  - subject: "koffice comes first"
    date: 2004-09-20
    body: "koffice has made a *lot* of progress. Now I can use kword and kspread for almost all I need, except the mail merge function, and viewing of MS office files. \n\nI do everything in koffice first, and used to switch to OO only if something would not work, or it would crash to often. \"Used to\", because now koffice does the job :-)\n\nNo more crashes, but easy to use apps. Great job, guys, and keep going that way!"
    author: "Matt"
  - subject: "Binary Packages for Solaris Uploaded"
    date: 2004-09-20
    body: "Binary Packages for Solaris 8/9 SPARC have been uploaded. (The xdelta for Solaris 9 applies to Solaris 8 too.)\n\nhttp://download.kde.org/stable/koffice-1.3.3/contrib/Solaris\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Binary Packages for Solaris Uploaded"
    date: 2004-09-21
    body: "Also SUSE binary packages for 9.0 and 9.1 are now available."
    author: "Anonymous"
  - subject: "Re: Binary Packages for Solaris Uploaded"
    date: 2004-09-21
    body: "Add Mandrake 10 to the list."
    author: "Anonymous"
  - subject: "Re: Binary Packages for Solaris Uploaded"
    date: 2004-09-21
    body: "Why is the Solaris 9 patch an .xdelta.tar.bz2?\n\nShouldn't it be a .tar.xdelta? (also xdelta is already gzip compressed)"
    author: "Clarence Dang"
  - subject: "Re: Binary Packages for Solaris Uploaded"
    date: 2004-09-22
    body: "> Why is the Solaris 9 patch an .xdelta.tar.bz2?\n\nBecause it's archived with tar and then compressed with bzip2.\n\nIt's not a Solaris 9 patch. It's a collection of source code\npatches for KOffice 1.3.3, generated by xdelta. The archive\nhappens to be in the Solaris 9 tree because it was generated\noff the Solaris 9 build with SunOne Studio 9 (a.k.a. Forte 9).\nIt applies just as well to Solaris 8, and/or to SunOne Studio 8.\n\n> Shouldn't it be a .tar.xdelta?\n\nHave you run bunzip2 and then tar xvf on it ? That will answer all\nyour questions.\n\n\n--Stefan\n"
    author: "Stefan Teleman"
  - subject: "Re: Binary Packages for Solaris Uploaded"
    date: 2004-09-22
    body: "> > Why is the Solaris 9 patch an .xdelta.tar.bz2?\n>\n> Because it's archived with tar and then compressed with bzip2.\n>\n> > Shouldn't it be a .tar.xdelta?\n> \n> Have you run bunzip2 and then tar xvf on it ? That will answer all\n> your questions.\n\nThanks for the info.  I'm actually trying out xdelta at the moment so I was curious about the naming.\n\nI don't actually have Solaris :)\n"
    author: "Clarence Dang"
  - subject: "Re: Binary Packages for Solaris Uploaded"
    date: 2004-09-23
    body: "> I'm actually trying out xdelta at the moment so I was curious about the naming.\n\nIMHO xdelta is a very well done diff/patch tool.\n\n> I don't actually have Solaris :)\n\nThat's ok. :-) Some of the patches are Solaris specific, but many of them\nare generic.\n\n--Stefan"
    author: "Stefan Teleman"
  - subject: "Compiliation for dummies"
    date: 2004-09-20
    body: "I try to compile this package on Suse 9.2\nIt goes pretty far but craps out when it can't fing libgmodule-2.0.la\nIs there a configure flag to work around gnome dependencies. And can anybody tell me where I can download this gnome library from, (pre-built would be nice) \n\nAnd thanks for the good work here, KOffice is really starting to look good."
    author: "bando"
  - subject: "Re: Compiliation for dummies"
    date: 2004-09-20
    body: "It's part of glib2-devel."
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Compiliation for dummies"
    date: 2004-09-20
    body: "> Is there a configure flag to work around gnome dependencies.\n\nAnd I want to know the configure flag to disable dependency on libc - another gnome dependency."
    author: "Anonymous"
  - subject: "Re: Compiliation for dummies"
    date: 2004-09-20
    body: "libc? \n\nBut what C library would you use then? (Are you not mixing up (g)libc with glib?)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Compiliation for dummies"
    date: 2004-09-21
    body: "I mean libc. The OP doesn't want KDE to depend on anything that GNOME depends on, right?"
    author: "Anonymous"
  - subject: "Re: Compiliation for dummies"
    date: 2004-09-21
    body: "Sorry, I do not know what you mean by \"OP\".\n\nBut if you develop with C or C++, you need a C library (if you do not want to make everything very OS-dependent.) Also Qt needs a C library.\n\n(Not counting that KDE is using libxml2 and libxslt which are distributed by gnome.org.)\n\nHave a nice day!\n\n"
    author: "Nicolas Goutte"
  - subject: "Re: Compiliation for dummies"
    date: 2004-09-21
    body: "OP=Original Poster. And you still don't get it, or? Add a smiley in mind to my posting."
    author: "Anonymous"
  - subject: "Re: Compiliation for dummies"
    date: 2004-09-20
    body: "--without-arts \n\nBut you do not get any sound for KPresenter and perhaps you would need a kDE compiled --without-arts from kdelibs on (otherwise you might have indirectly the dependecy.)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Compiliation for dummies"
    date: 2004-09-21
    body: "> I try to compile this package on Suse 9.2\n\nWhere on earth did you get SuSE 9.2?  It hasn't even been released yet!\n\n-- Steve"
    author: "Steve"
  - subject: "KOffice wfm"
    date: 2004-09-20
    body: "I just wanted to say that I do exist, and I love KOffice. The developers get 1000 years deducted from purgatory.\n\nKeep it up."
    author: "God"
  - subject: "Re: KOffice wfm"
    date: 2004-09-21
    body: "If you were really God you would have said that you DON'T exist, which would have lead to thousands of years of factional fighting over exactly how we should interpret that.\n\n...and you of all people would have an ultra-hip Gmail account.\n"
    author: "ac"
  - subject: "Binary Packages for SuSE and Mandrake Uploaded"
    date: 2004-09-21
    body: "Binary packages for SuSE and MAndrake have been uploaded!\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Binary Packages for SuSE and Mandrake Uploaded"
    date: 2004-09-24
    body: "Thanks. They installed fine. I never understood the strange Mandrake-packages though, seems to exist multiple packs for the same app.\n\nWill there be more bugfix-releases before v2?"
    author: "Alex I"
  - subject: "Re: Binary Packages for SuSE and Mandrake Uploaded"
    date: 2004-09-24
    body: "The Mandrake packages does not seems too complex:\n- in noarch are the package for things that are common between multiple architectures (read: CPUs)\n- in RPMS are the package for a i586-compatible CPU\n- in SRPMS are the sources (which you do not need if you just want binaries).\n\nAs for KOfffice 2, it has been postponed (without date).\n\nA KOffice 1.4 is targeted, with less feature, to spring 2005.\n\nAlso the KOffice 1.3.x branch is not closed, we will try to release a new version early November 2004 (but it is not completely sure now).\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Now with source package descriptions"
    date: 2004-10-01
    body: "I have change the announcement page a little to give short descriptions of the source packages: http://kde.org/areas/koffice/releases/1.3.3-release.php\n\nIt is especially useful for users not knowing the language codes (but it is still sorted according to the language codes.)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "good stuff"
    date: 2004-10-12
    body: "KOffice is good stuff. I use it all the time. I don't know what people are talking about crashes for. Perhaps if they compiled from the source, they wouldn't have those problems. IMHO, OO is quite a bit too slow. Though the many platforms it runs on, does give it a good spot. So if your already using KDE, KOffice is a steal."
    author: "witness"
---
The <a href="http://kde.org/areas/koffice/">KOffice</a> team is happy to bring you the third bugfix package that builds upon the previous 1.3.x versions, with many fixes, mainly in the core libraries and in some filters. But there is also a fully new and complete translation for KOffice: Welsh. See the <a href="http://kde.org/areas/koffice/releases/1.3.3-release.php">release notes</a> and <a href="http://kde.org/areas/koffice/announcements/changelog-1.3.3.php">the complete list of changes</a>. 
 




<!--break-->
