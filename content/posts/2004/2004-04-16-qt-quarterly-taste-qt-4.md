---
title: "Qt Quarterly: A Taste of Qt 4"
date:    2004-04-16
authors:
  - "binner"
slug:    qt-quarterly-taste-qt-4
comments:
  - subject: "New features"
    date: 2004-04-16
    body: "I really like these:\n\n* Qt splitted in GUI and non-GUI part\n* Performance improvements\n* Better integration with KDevelop\n* Arthur (paint engine): perhaps a Cairo (FDO stuff, together with Keith's XServer) backend\n* Scribe (text render engine) and ATK accessibility bridge: now we're on par with GTK on this stuff :)"
    author: "Niek"
  - subject: "Re: New features"
    date: 2004-04-17
    body: "> Scribe (text render engine) and ATK accessibility bridge: now we're on par with GTK on this stuff :)\n\nErm, Qt is already on par with Pango.. The only thing missing for a while was proper Indic support which came out in Qt 3.2. Qt 3.0 had bidirectional-text support months before Gtk 2.0 came out (with Pango), and Qt 2.0 came out with full fledged Unicode support, many years before Gtk 2.0 and Pango had that. "
    author: "wow"
  - subject: "Re: New features"
    date: 2004-04-17
    body: "> Erm, Qt is already on par with Pango.. \n\nActually not quite. Pango deals with fonts better on linux, especially a lot of those in the cjk range. It also has a text layouting api that qt desparately needs, to handle complex line breaking and paragraphing situations.\n\nAlso, support for input methods in qt is lacking, as qt3 only really supports XIM.\n\nSo on the i18n front, gtk is still ahead overall, i would have to say"
    author: "Ken"
  - subject: "Re: New features"
    date: 2004-04-17
    body: "\"Pango deals with fonts better on linux, especially a lot of those in the cjk range. \"\n\nPerhaps, I don't have much experience with cjk. \n\n\"It also has a text layouting api that qt desparately needs, to handle complex line breaking and paragraphing situations.\"\n\nIt already has a complex text layouting API since Qt 2.0. It was mostly rewritten in 3.0 to support bidirectional text. \n\n\"Also, support for input methods in qt is lacking, as qt3 only really supports XIM\" \n\nNot true, depending on the language, there are other input methods available as Qt plugins. What thing that is lacking is IIIM support however. "
    author: "ac"
  - subject: "Re: New features"
    date: 2004-04-17
    body: "> It already has a complex text layouting API since Qt 2.0. It was mostly rewritten in 3.0 to support bidirectional text. \n \nIs it a public API? Last time I checked, things like QTextEdit could do it right, but when it came two writing your own widgets that needed layout, there wasn't much of a way to do it?\n\nAlso, which input method plugins were you referring to? THe only thing I know about is the 'immodule for qt' effort, but it hasn't been incorporated into qt and as far as i know there hasn't been much mention of it outside of the pages and mailing lists that are directly associated with it."
    author: "Ken"
  - subject: "Re: New features"
    date: 2004-04-17
    body: "> Erm, Qt is already on par with Pango.\n\nI can't say since I haven't used Pango lately.\n\nHowever, concern about these advanced features is misplaced because the current release of Qt does not handle fonts well -- basic font handling.  It fails to find a significant number of my installed fonts which even WordPerfect-8.1 finds correctly.\n\nTest case: I have several weights and widths of Helvetica (Type1) which are simply not listed.  What is worse is that this (to some extent) is a regression.  I am certain that I used to use Helvetica Narrow with no problem.\n\nYes, I did send them a bug report and the advised me that my analysis of the problem was correct (TrueType and Type1 handle this differently) and that they would work on it.\n\nWith other fonts, Qt does find the font, but it then uses the wrong font.  Considering that Qt is now in version 3.x.y this causes great concern -- in general problems like this are usually due to a naive design.\n\nI hope that at least some of this gets fixed with 3.3.2 -- if I had paid full price for these fonts, I would not be very pleased that I can't use them in KDE.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: New features"
    date: 2004-04-17
    body: ">Better integration with KDevelop\n\nIf this is well done we can have some kind of delphi/kylix, but open source and more native! WOW!"
    author: "Iuri Fiedoruk"
  - subject: "foreach!"
    date: 2004-04-16
    body: "I love the new foreach.  How is this done?  moc?"
    author: "ac"
  - subject: "Re: foreach!"
    date: 2004-04-17
    body: "foreach is part of STL. I think the new QT constructs inherit from the STL containers (or at least implement the container interfaces) so we can use STL algorithms for the QT containers."
    author: "Jonathan Tan"
  - subject: "Re: foreach!"
    date: 2004-04-17
    body: "> foreach is part of STL. I think the new QT constructs inherit from the STL\n> containers (or at least implement the container interfaces) so we can use STL\n> algorithms for the QT containers.\n\nNo, you can already use STL algorithms on Qt containers since Qt 3.0 made them STL compatible:\n\nQStringList list;\nstd::for_each( list.begin(), list.end(),\n\t\tSomeFunctor() );\n\nThe new foreach is most probably a macro (and it will probably expand to suboptimal code (say \"const vs. non-const iterators\").\nIf it was more than that, you wouldn't need the redundant \"QString s\" declaration in it (\"list\" already has that information), so you could write:\n\nforeach( s, list )\n\tlen += s.length();\n\nThe problem I see is that \"foreach\" - as an identifier - will surely clash with  a possible future C++ reserved keyword of the same name. But then, the C++ people will more likely standardize the Boost Lambda library than introducing syntactic sugar in the language...\n"
    author: "Marc Mutz"
  - subject: "Re: foreach!"
    date: 2004-04-17
    body: "Actually, with our version of foreach you can write both. foreach(s, list) works just as well as foreach(QString s, list). It is a combination of template magic and a macro. The macro is of course called Q_FOREACH, but you can set a flag that \"pollutes\" the namespace with the foreach keyword if you want to.\n\nThe code it expands to isn't exactly trivial, especially not on non-gcc compilers like MSVC6. The generated assembly, however, is pretty optimal. While it is slightly bigger than a handwritten loop, there is basically no speed difference.\n\nWould I recommend the construct to someone who knows and loves the STL? No. Would I recommend it to somebody who wants to write readable, fast-enough, and good-enough code and possibly has a Java background? Yes. Do I use it myself? In application code, yes."
    author: "Matthias Ettrich"
  - subject: "Re: foreach!"
    date: 2004-04-17
    body: "\"Would I recommend the construct to someone who knows and loves the STL? No. Would I recommend it to somebody who wants to write readable, fast-enough, and good-enough code and possibly has a Java background? Yes. Do I use it myself? In application code, yes.\"\n\nThat would be me then. Sounds great!"
    author: "David"
  - subject: "Re: foreach!"
    date: 2004-04-17
    body: "I am quite a bit curious: Did Trolltech implement this via the moc preprocessor? If not, I can hardly imagine a way how this can be done with pure C++ and general enough so that user defined container classes will work with this. I came up so far with the snippet below, but this has its limitations as can be seen.\n\n----------------------------------------------------------------\n\n#define foreach(decl, container) for(std::pair<int, deduce_from_tag<deduce_param1<void (decl)>::type, sizeof(deduce_helper(container))>\\\n        ::type::iterator> iter = std::make_pair(0, container.begin()); iter.second != container.end(); (iter.first = 0, ++iter.second) ) \\\n\tfor(decl = *iter.second; iter.first != 1; ++iter.first)\n\nstruct std_vector_tag { char d; };\nstruct std_list_tag { char d[2]; };\nstruct std_string_tag { char d[4]; };\nstruct std_map_tag { char d[8]; };\n\ntemplate<typename T>\nvoid deduce_helper(T) { }\n\ntemplate<typename T>\nstd_vector_tag deduce_helper(std::vector<T>);\n\ntemplate<typename T>\nstd_list_tag deduce_helper(std::list<T>);\n\nstd_string_tag deduce_helper(std::string);\n\ntemplate<typename T,typename T2>\nstd_map_tag deduce_helper(std::map<T,T2>);\n\ntemplate<typename T,int >\nstruct deduce_from_tag;\n\ntemplate<typename T>\nstruct deduce_from_tag<T,sizeof(std_vector_tag)>\n{\ntypedef std::vector<T> type;\n};\n\ntemplate<typename T>\nstruct deduce_from_tag<T,sizeof(std_list_tag)>\n{\ntypedef std::list<T> type;\n};\n\ntemplate<typename T>\nstruct deduce_from_tag<T,sizeof(std_string_tag)>\n{\ntypedef std::string type;\n};\n\ntemplate<typename Tpair>\nstruct deduce_from_tag<Tpair,sizeof(std_map_tag)>\n{\ntypedef std::map<typename Tpair::first_type,typename Tpair::second_type> type;\n};\n\ntemplate<typename T>\nstruct deduce_param1;\n\ntemplate<typename T>\nstruct deduce_param1<void (T)>\n{\ntypedef T type;\n};\n\n--------------------------------------------------------------------"
    author: "El Pseudonymo"
  - subject: "Re: foreach!"
    date: 2004-04-19
    body: "Eric Niebler submitted an implementation of a FOREACH macro to boost that was really clever. It has since been yanked. Here is the readme:\n\nBOOST_FOREACH\nby Eric Niebler\n\nThe BOOST_FOREACH macro is a simple, intuitive and typesafe way to iterate\nover a collection. It is inspired by similar looping constructs in other\nlanguages, particularly C#. It is intended for inexperienced programmers who\ndo not know much about iterators, predicates and half-open sequences. It is\nalso useful for anybody who wants to type fewer characters when writing loops\nover sequences. Its salient features are:\n\n- It works over STL containers, arrays and null-terminated strings.\n\n- Your loop variable can be a value or a reference. If it is a reference, then\n  any changes you make to it get written through to the underlying sequence.\n  \n- It generates near-optimal code. That is, it is almost equivalent to the\n  hand-coded equivalent.\n  \n- It behaves just like a for loop. That is, you can break, continue, goto or\n  return out of the loop body.\n\nThe syntax is as follows:\n\n  std::list<int> int_list;\n  ...\n  BOOST_FOREACH( int &i, int_list )\n  {\n      i += 10;\n  }\n  \nNote that the loop variable, i, is a reference to the ints within int_list. You\ncan also declare your loop variable before the BOOST_FOREACH loop:\n\n  int i;\n  BOOST_FOREACH( i, int_list )\n  {\n      std::cout << i << std::endl;\n      if ( -1 == i )\n          break;  // this breaks out of the BOOST_FOREACH loop\n  }\n\nAcknowledgements\n\nMany of the ideas for BOOST_FOREACH are described in the Nov 2003 C/C++ Users\nJournal article by myself and Anson Tsao. That article describes how to\nimplement a FOR_EACH macro that works with .NET collections. BOOST_FOREACH is\na reimplementation of the original FOR_EACH macro that works with native type.\nIt also corrects a number of shortcomings of that implementation.\n"
    author: "michael toksvig"
  - subject: "Re: foreach!"
    date: 2004-04-19
    body: "You can find the code to BOOST_FOREACH along with a PowerPoint presentation about it at http://www.nwcpp.org/Meetings/2004/01.html.\n\nEnjoy!\nEric Niebler\nBoost Consulting\nwww.boost-consulting.com\n"
    author: "Eric Niebler"
  - subject: "Re: foreach!"
    date: 2004-04-19
    body: "God, I love the Internet! You mention somebody's name in a thread, and he comes and replies to the thread with complete details. Thanks Eric."
    author: "Craig Buchek"
  - subject: "Consumer -style interfaces?"
    date: 2004-04-16
    body: "\"Driven by consumer electronic devices such as mobile phones and PDAs, desktop applications are moving away from standard widgets and styles towards more customized user interfaces\"\n\nWhile I'm glad Qt 4 will have a better style engine, and that it will better support miniscule low-bit displays on phones and devices, isn't this another word for \"skins\"? And aren't the current Qt style preferable to bitmapped skins? I don't want my KDE desktop looking like an LCD display. Nor do I want it looking like something Nullsoft drew."
    author: "David Johnson"
  - subject: "Re: Consumer -style interfaces?"
    date: 2004-04-17
    body: "> And aren't the current Qt style preferable to bitmapped skins? \n\nI'm not sure if the article actually meant that. Anyway even right now you can use bitmaps to render styles, and that's what actually some styles do right now. Discover which of those ;)\n\n> I don't want my KDE desktop looking like an LCD display\n\nheh, discover which of the current styles use images ;)) Does any look an LCD? :))\n\nI know what you mean, but I don't think you should worry, new Qt's drawing methods and styles will just improve. I wouldn't worry that much :))"
    author: "uga"
  - subject: "WOW, "
    date: 2004-04-16
    body: "this looks very cool!"
    author: "wilbert"
  - subject: "Re: WOW, "
    date: 2004-04-16
    body: "Couldn't agree more."
    author: "Pilaf"
  - subject: "Re: WOW, "
    date: 2004-04-16
    body: "That shiznit is off the hook!"
    author: "Rice"
  - subject: "Insanely cool"
    date: 2004-04-16
    body: "OK.  I love the look of the Qt iterators.  One big thing I dislike about C++ is the STL.  It is soooo ugly.  C++ is an inherently ugly language, but at least Trolltech are doing their best to make it useable.  Just compare:\n\nforeach (QString s, list)\nsum += s.length();\n\nto\n\nQList<QString>::const_iterator i;\n  for (i = list.begin(); i != list.end(); ++i)\n    sum += (*i).size();\n\n!!!  Neither is as nice, IMO, as:\n\nlist.each {|s| sum += s.length }\n\nof Ruby, but the Qt way makes things much more bearable in C++."
    author: "KDE User"
  - subject: "Re: Insanely cool"
    date: 2004-04-17
    body: "Once you remove the template (and casting) syntax, C++ isn't ugly. Quite the opposite. It makes a readble and easy-to-write language. But only if you stay away from that awful template (and casting) syntax."
    author: "David Johnson"
  - subject: "Re: Insanely cool"
    date: 2004-04-17
    body: "I must disagree.  I find C++ to be ugly.  It's a bolt-on to C, with a bunch of other bolt-on features.  It's too hacky for my tastes.  For example, why not make a sane syntax for abstract classes, such as\nabstract class Foo;\n\nInstead, you can choose to set a method equal to zero (hello, what?  does this make any sense at all?) or make the constructor private (makes more sense, but still not as clear, to me, as an \"abstract\" keyword).\n\nI'm not saying that C++ is the world's crappiest language, but it's far, far, far from elegant.  ESPECALLY, as you point out, when you add templates.  \n\nLanguages, such as Ruby, that were designed from day one with OO in mind are much cleaner to work with.  Now all we need is a Ruby compiler.. :("
    author: "KDE User"
  - subject: "Re: Insanely cool"
    date: 2004-04-17
    body: "But there are more pattern than OO. I like python more, but I believe its a question of taste.\n\nIMHO language should be designed to be easily readalbe. Reading is IMHO much more important than writing."
    author: "Marco Bubke"
  - subject: "Re: Insanely cool"
    date: 2004-04-18
    body: "The template mechanism is one of THE reasons why C++ is THE language especially for high-complex numeric problems since it provide the capability to write complex but very performant programs. There is no substitution for this in any other language. Beside this, I won't miss any of the other features C++ provides. See the Java-guys: they try to integrate all these C++ features the'd called \"depreached\" like enums and other. IMHO Java has more structural disadvantages as C++. One of the biggest disadvantage is thatyou have to call new every time you want to instance an object. "
    author: "Ruediger Knoerig"
  - subject: "Re: Insanely cool"
    date: 2004-04-18
    body: "Name one \"high-complex numeric\" program in KDE?\n\nI agree with the other poster. Templates in C++ are an ugly preprocessor hack. It is a bit like operator overloading, the arguments for why operator overloading in C++ is so good always involve either complex numbers or matrix math...\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Re: Insanely cool"
    date: 2004-04-17
    body: "and yet for more language examples:  Ah, perl...\n\nmap { $sum += $#$_ } @list;\n\nWho couldn't understand that ;-)"
    author: "Wes Hardaker"
  - subject: "Re: Insanely cool"
    date: 2004-04-17
    body: "Perl - $Just @when->$you ${thought} s/yn/tax/ &couldn\\'t %get $worse;"
    author: "cm"
  - subject: "Re: Insanely cool"
    date: 2004-04-17
    body: "Just to avoid language bigotry: this is a real Python line of code I wrote today (and no, I can\u00b4t find a simpler way to do it, and yes, it does something useful):\n\nreturn apply(ObjectInstances[method].__class__.__dict__[params[0\n]],(ObjectInstances[method],)+params[1:])\n\n\nNotice how it\u00b4s ugly in a completely different way than perl? ;-)\n\nOf course when I saw what I had to write, I was shocked. While a perl programmer seing this level of uglyness probably doesn\u00b4t even notice (just to bring back language bigotry ;-)"
    author: "Roberto Alsina"
  - subject: "Re: Insanely cool"
    date: 2004-04-17
    body: "It's a shame that everyone pontificating about perl seems to know so little about it:\n\n  map { $sum += $#$_ } @list;\n \nTry this instead, and show me how this is more obfuscated than any other language:\n\n  $sum += length for @list;\n\nOh of course - the $ to indicate a scalar, and @ to indicate an array - that's not how C/C++/python does it so it must be rubbish. Hey, <insert YOUR favourite language here> is so much cooler than perl.\n\nThere are plenty of things wrong with perl, as there are with all other languages - use each for its strengths (and learn how to code idiomatically).\n\nRant over!"
    author: "Rich"
  - subject: "Re: Insanely cool"
    date: 2004-04-17
    body: "...and even Ruby isn't as elegant as the equivalent Perl:\n\n$sum += length($_) foreach @list\n\nbut that's just me :)"
    author: "stunji"
  - subject: "Re: Insanely cool"
    date: 2004-04-17
    body: "Nothing beats python :-)\n\nsum=0\nfor string in li: sum=sum+len(string)"
    author: "wow"
  - subject: "Re: Insanely cool"
    date: 2004-04-17
    body: "I'm 15 years old and one of these Delphi/Kylix/FreePascal guys :-)\n\nsum:= 0;\nfor I:=1 to MyStringList.Count do\n  Sum:= Sum+Length(MyStringList[I]);\n\nBut now I'm also learning C#.\n\nAndy"
    author: "Andy"
  - subject: "Re: Insanely cool"
    date: 2004-04-17
    body: "But this is technically not the same as the other examples. It would be trivial to implement this in C++ via a macro. The reason why your example is different from the others is that it will only work with containers which have an subscript operator. It will not work with containers which can only be forward traversed."
    author: "El Pseudonymo"
  - subject: "Re: Insanely cool"
    date: 2004-04-17
    body: "With Python, you can also do it in a more functional programming way:\nreduce(lambda x,y:x+y,[len(s) for s in li])\n\nThe list comprehension '[len(s) for s in li]' returns the list of the lengths of the strings in li and the 'reduce' operator computes the sum:\n[1,2,3,4] -> (((1+2)+3)+4)\n"
    author: "Kigr"
  - subject: "Re: Insanely cool"
    date: 2004-04-17
    body: "ah, but the most Pythonic way would be:\n\nsum([len(l) for l in list])\n\nwhich is a) the nicest notation of everything I've seen here, and b) is the thing that pops right into your head when thinking about the problem.. right?"
    author: "mark dufour"
  - subject: "Re: Insanely cool"
    date: 2004-04-17
    body: "Yeah, but only if you know sum() accepts lists :-)"
    author: "wow"
  - subject: "Re: Insanely cool"
    date: 2004-04-17
    body: "Is:\n\n  sum([len(l) for l in list])\n\nReally easier to read than the *idiomatic* perl equivalent:\n\n  $sum += length for @list;\n\nThere are a lot of muppets posting crap perl code on this site ATM - perhaps they should stick with a language they actually know.\n\nBTW I've got no problem with Python, Ruby, Tcl, Perl, ... - folks should use whatever they're happy with!"
    author: "Rich"
  - subject: "Re: Insanely cool"
    date: 2004-04-18
    body: ">Is:\n>sum([len(l) for l in list])\n>Really easier to read than the *idiomatic* perl equivalent:\n>$sum += length for @list;\n\nnot if you are already a perl programmer. yeah, I do not really like the way it reads. what are these extra characters, '$', '@' and ';'? I think I would prefer 'sum += length for list', that really is easier to read. you know that working memory is limited, and having all sorts of redundant characters on a line (especially one longer than this) makes it harder to read. then, the '+=' turns me off a bit. it just seems awkward, but also for beginners and for e.g. presenting code to a math professor it's not very readable. in Python you tend to 'transform' datastructures, so you don't need awkward constructions like this. I admit the Python expression isn't insanely obvious at first, but at least once you get it it reads easily, and it's not awkward at all. it just says what you want to do, almost literally: 'I want the sum of the lengths of the things in this list.' what does the perl version say? 'I want to += the length for this list, and it's not clear what $sum is, initially.' and how flexible is this perl construct? can I add for instance the numbers of two lists? in Python this would be [a+b for (a,b) in zip(l1,l2)]..\n\n \n"
    author: "mark dufour"
  - subject: "Re: Insanely cool"
    date: 2004-04-18
    body: ">what are these extra characters, '$', '@', ';'\n\nBut you have to know a little of any language before using it properly don't you:\n\n$= scalar, @ = array, % = hash etc. Most of the other punctuation marks rarely have to be used or have English equivalents.\n \n>but at least once you get it it reads easily, and it's not awkward at all\n\nThat's the whole point isn't it - once you get most (though not all :) languages they read easily! It's sad that much of the code posted by people to show how bad perl is often isn't idiomatic perl - it wouldn't take me long to write some hideous python, ruby, tcl, Java, C# etc code because I'd only have a relatively superficial knowledge of them.\n\n> 'I want the sum of the lengths of the things in this list.' what does the perl version say?\n\nYou're being silly now - it says \"sum the length of each item in this list\". There's really not much difference here - they're both clearer than many other languages!\n\n> it's not clear what $sum is, initially\n\nEh? \n \n>in Python this would be [a+b for (a,b) in zip(l1,l2)]..\n \nYes, there's no doubt that's a nicer way than a perl equivalent - that's why zip is being introduced in p6. There's some great stuff in python, and p6 will certainly show some influence from python, but the fact remains I find it amazing that people are quick to judge other languages when they clearly have little knowledge of them.\n\nI'm not implying you, rather the original posters of the perl examples.\n"
    author: "Rich"
  - subject: "Re: Insanely cool"
    date: 2004-04-18
    body: "Double posting - ah well never mind!\n\nAs I didnt provide code:\n\n  [a+b for (a,b) in zip(l1,l2)].\n\n*Could* go to:\n\n $sum = $a[$_] + $b[$_] for 0 .. $#a;\n\nNot as nice - roll on p6!"
    author: "Rich"
  - subject: "Re: Insanely cool"
    date: 2004-04-18
    body: "I respectfully disagree with your statement, that most languages have the same readability, once you are used to using them. by readability I mean a lot of things, like: is a language orthogonal, so that different programmers can quickly understand each others code? is it compact yet still clear, so that you can see what is happening from a high level? does it do away with low-level details irrelevant to the essence of what you are doing? can you re-read your code again after having worked on something else for six months? is the code accessible to beginner (or new at the project) programmers, so they can easily add or change things? I think these, among other things of course, all contribute to the  inherent 'readability' of a language. not to make this into a python-perl war, looking at it this way, I think it is safe to say that Java is more readable than C. I also think Python is more readable than Java, because you can do a lot more in much less lines, while still being perfectly clear as to what is happening. I think the addition of all sorts of more or less redundant characters in Perl (I forgot ';', '{' and '}' :-)) makes it a little less inherently readable. \n\nbut I admit I was probably overreacting a little.. Perl and Python are both great languages, and they are a lot more alike than that they differ.."
    author: "mark dufour"
  - subject: "Re: Insanely cool"
    date: 2004-04-18
    body: "Code like that makes me glad I chose to migrate from Perl to Ruby, rather than to Python."
    author: "KDE User"
  - subject: "Re: Insanely cool"
    date: 2004-04-18
    body: "I don't know Ruby, but here is the corresponding code fragment someone gave earlier:\n\nlist.each {|s| sum += s.length }\n\ncan you tell me why you find this more readable than, say\n\nsum([len(s) for s in list])\n\nalso, what is the value of sum initially? and, can you use this list.each.. construction as an expression itself? \n\n"
    author: "mark dufour"
  - subject: "Re: Insanely cool"
    date: 2004-04-18
    body: "\"also, what is the value of sum initially? and, can you use this list.each.. construction as an expression itself?\"\n\nYou need to assign a 0 to sum. Array.each returns the array so you can nest calls - all ruby expressions return a value even 'if' statements\n\nirb(main):008:0> list = [\"a\", \"bb\", \"ccc\"]\n=> [\"a\", \"bb\", \"ccc\"]\nirb(main):009:0> sum = 0\n=> 0\nirb(main):010:0> list.each {|s| sum += s.length }\n=> [\"a\", \"bb\", \"ccc\"]\nirb(main):011:0> puts \"sum: #{sum}\"\nsum: 6\n=> nil\n\nAlthough doesn't look any more readable than the python to me - they both look just fine!\n\nBut ruby blocks and iterators are certainly very nice - I don't know if python works the same. Does it allow you to yield multiple values from a block? In ruby the little expression above is actually a sort of co-routine where the block and the Array yield control to each other for each iteration."
    author: "Richard Dale"
  - subject: "Re: Insanely cool"
    date: 2004-04-18
    body: "so printing out the sum takes three lines..? yes, Python also has multiple return values (I do not understand how a ruby block works?), a general iteration protocol, and yield statements that allow functions to temporarily return and later resume what they were doing.. I don't thnk there are big semantic differences between perl, ruby and python. it's mainly the syntax.. "
    author: "mark dufour"
  - subject: "Re: Insanely cool"
    date: 2004-04-18
    body: "I think yield is a bit easier conceptually to understand if it's viewed as an invocation of a closure and not a co-routine transfer, since IIRC, the block can just return, and not transfer back, no? (Although the co-routine definition certainly matches the history of the yield keyword), particularly considering the roughly interchangeability with Proc/Lambda"
    author: "Sad Eagle"
  - subject: "Re: Insanely cool"
    date: 2004-04-18
    body: "A block can either return or yield.\n\nSo here is the same thing in ruby 'functional style'. You use the collect method to map a function onto each element of the Array:\n\nirb(main):018:0> list = [\"a\", \"bb\", \"ccc\"]\n=> [\"a\", \"bb\", \"ccc\"]\nirb(main):019:0> sum = 0\n=> 0\nirb(main):020:0> list.collect! {|s| sum += s.length }\n=> [1, 3, 6]\nirb(main):021:0> puts sum\n6\n\nAll the other examples have been in this functional style. The iterator/yield approach is more powerful especially once you start to nest them."
    author: "Richard Dale"
  - subject: "Re: Insanely cool"
    date: 2004-04-18
    body: "It's compositional and perfectly orthogonal; it involves two concepts (blocks and methods) which are found in every single program all over the place. It's just passing a code fragment to the .each method of the list, to call on every element. It does not need a special syntax to construct a new list of values by iterating over an another list, which I am guessing the Python example is doing. Also, as a matter of my own personal preference, the Ruby example follows a hierarchical structure of control flow, where outer control statements are on the outside in the code (yes, I never uses the postfix forms of if or unless in Ruby code)\n\n"
    author: "Sad Eagle"
  - subject: "Re: Insanely cool"
    date: 2004-04-19
    body: "I found this discussion about ruby vs. python iteration styles:\n\nhttp://www.rubygarden.org/ruby?IterationStyles\n\nHe ends with:\n\n\"So which way is better? I find it fascinating that both Ruby and Python have almost equivalent power in its iteration, they have just taken different paths to get there.\""
    author: "Richard Dale"
  - subject: "Re: Insanely cool"
    date: 2004-04-19
    body: "I find OO to be clearer in cases like this.  You ask the list to give you each element in turn.  Since each element is an object, you can pass messages to them.\n\nIt's not that I find the Python *terrible*, but I think the Ruby just makes a lot more sense.  I also get sick of statements like \"is the thing that pops right into your head when thinking about the problem.. right?\""
    author: "KDE User"
  - subject: "Re: Insanely cool"
    date: 2004-04-18
    body: "How about?\n\nsum(map(len, list))"
    author: "Kosh"
  - subject: "Re: Insanely cool"
    date: 2004-04-19
    body: "Bing! we have a winner. The simple clarity combined with brevity of this solution is beautiful. I was looking at the list comprehension solutions, and fiddling around with my own... then saw this. And saw the light.\n\nThe only part that looks slightly cryptic to a non-Python person may be the map(). But that's okay. \n\nI also find it interesting that while python has the slight reptuation of being more rigidly structured that certain other languages, so many very different Python solutions were given here."
    author: "Tim Middleton"
  - subject: "Re: Insanely cool"
    date: 2004-04-19
    body: "map is slowly being deprecated though from the language.. because list comprehensions are easier to understand, and are more powerful. I like this construction though :))"
    author: "mark dufour"
  - subject: "Re: Insanely cool"
    date: 2004-04-17
    body: "You can also replace the list comprehension with a map\n\nreduce(lambda x,y:x+y, map (len, li))"
    author: "anon"
  - subject: "Re: Insanely cool"
    date: 2004-04-17
    body: "Haskell:\n\nsum [length s | s <- li]\n\n"
    author: "mill"
  - subject: "Re: Insanely cool"
    date: 2004-04-17
    body: "> foreach (QString s, list)\n> sum += s.length();\n \n> QList<QString>::const_iterator i;\n> for (i = list.begin(); i != list.end(); ++i)\n> sum += (*i).size();\n\nYou fell face-on into the marketing trap :)\n\nFor one, you could use it->length() instead of the (*it).length();\nFor two, if you allow Qt to use non-std-c++ tools, let me show you how the same code looks using lambda functions (from the Boost library, the playground for libraries that are proposed for inclusion in the next C++ std version):\n\nfor_each( list.begin(), list.end(),\n          sum += _1.length() ); // you probably need var() around \"sum\"\n\nThe best thing here is that this is void of redundancy (cf. \"QString s\" in the Qt foreach). You might wonder why I have to give the seemingly redundant list.begin(), list.end() here, after all \"list\" should just do, right?\n\nYou are free to write a small wrapper around std::for_each() that does exactly that. But the begin/end concept allows the algorithm to work on any range, e.g. this one:\n\ndouble foo[] = { 1.2, 3.4, 5.6, 7.8, 9.0 };\nfor_each( foo, foo + 5, sum += _1 );\n\nNot to speak about\n\nsum = accumulate( foo, foo + 5, 0.0 );\n\nwhich is also an STL algorithm :o\n\nSo if you say that C++ is an ugly language b/c of the STL, then let me respond by telling you what the STL is all about. It's about removing redundancy. As with desgin patterns, once you start thinking in algorithms instead of implementation, you are able to tackle more complex problems. In this case, note how we can change the value_type of the containers without changing a single line of the algorithm code. Can't be done with Qt4's foreach(), which is even worse than the full-length STL example they gave, since you can always use \"typedef QList<QString> Container\" to hide the value_type, whereas Qt's foreach _requires_ you to explicitly state the value_type for _every_ for loop. Note also how in the accumulate() example, we said _what_ we do instead of _how_ we do it :)\n\nIt's of course easy to make a special case look better. This reminds me of the wannabe physicists that regularly write papers that seemingly convincingly disproof the Einstein theory of relativity. But if you look closer, they pick a single prediction out of the vast array of predictions of the full theory and found another explanation for that since prediction. That their \"theory\" (in contrast to Einstein's) fail to explain all the other effects that man observes in the universe isn't relevant to them.\n\nSame here. The STL tries to relieve you from writing for loops. for_each() is just one of many STL algorithms. It doesn't matter that you have found a nicer syntax for a for loop if the real goal is to avoid writing them out explicitly in the first place :)\n"
    author: "Marc Mutz"
  - subject: "Re: Insanely cool"
    date: 2004-04-17
    body: "> For one, you could use it->length() instead of the (*it).length();\n\nYou can't. They overloaded the * operator to return the object the iterator points to. However, the -> operator isn't overloaded. I don't really know why, but anyway, it won't work ;)"
    author: "Arend jr."
  - subject: "Re: Insanely cool"
    date: 2004-04-17
    body: "Oh, yes. I used QValueVector, where it works due to the iterator being value_type*. Which is another problem with Qt containers. They use lists all over the place, where a vector would suffice, and probably yield better performance (less malloc()s, for one). Just look at the implementation of operator[] for QValueList - ugh. (hint: QValueListPrivate<T>::at())"
    author: "Marc Mutz"
  - subject: "Re: Insanely cool"
    date: 2004-04-17
    body: "So if you say that C++ is an ugly language b/c of the STL, then let me respond by telling you what the STL is all about. It's about removing redundancy. As with desgin patterns, once you start thinking in algorithms instead of implementation, you are able to tackle more complex problems. In this case, note how we can change the value_type of the containers without changing a single line of the algorithm code. Can't be done with Qt4's foreach(), which is even worse than the full-length STL example they gave, since you can always use \"typedef QList<QString> Container\" to hide the value_type, whereas Qt's foreach _requires_ you to explicitly state the value_type for _every_ for loop. Note also how in the accumulate() example, we said _what_ we do instead of _how_ we do it :)\n--\n\nThat's nice.  The STL and C++ are still ugly.  The claim wasn't that C++ is a useless language.  It's that C++, and the STL are ugly.  You'll never convince me otherwise, simply because in my opinion, they're not fun to look at."
    author: "KDE User"
  - subject: "Re: Insanely cool"
    date: 2004-04-17
    body: "Have you ever thought that\n\nint sum = 0;\nforeach (QString string, names)\n    sum += string.length();\n\nis simply more readable than\n\nint sum = 0;\nfor_each(names.begin(), names.end())\n    var(sum) += _1.length();\n\n?\n\nReadability is king, because reading code is much more time consuming than writing code! (even though trying to find way to l33t-STL-encrypt a simple loop can indeed cost more time ;)\n\nI guess that's also why foreach (just like C#'s foreach, btw) requires the type of the loop variable. Plus it's perfectly consistent with for and while.\n\nAnd talking about redundancy: The 'var' is redundant. Unlike the type in the foreach it adds no value when reading the code.\n\nTry teaching a freshman from university the magic of _1, ref(), var() and constant() versus the simple foreach in the example. It becomes much more fun, btw, if next to the _1 a 1 as constant appears - goodbye readability.\n\nSTL is not necessarily a good argument when trying to convince for example a C# or Java programmer to use C++ for his next project.\n"
    author: "Anonymous"
  - subject: "Re: Insanely cool"
    date: 2004-04-17
    body: "Hey, the OP stated that you can of course write wrappers.\nSo you can do something like this:\n\n-------------------------------------------\n\nstruct add_to\n{\nadd_to(int& sum);\n\nvoid operator()(int add);\n\nint& sum;\n};\n\nadd_to::add_to(int& sum) : sum(sum)\n{\n}\n\nvoid add_to::operator()(int add)\n{\nsum += add;\n}\n\ntemplate<typename Tcontainer, typename Top>\nvoid my_for_each(Tcontainer const& container, Top op)\n{\nfor(typename Tcontainer::const_iterator iter = container.begin(); iter != container.end(); ++iter)\n\t{\n\top(*iter);\n\t}\n}\n\n...\n\nstd::vector<int> v;\n\nint sum = 0;\nmy_for_each(v, add_to(sum) );\n\n-------------------------------------------\n\nThis is pretty readable, or?\n\n"
    author: "El Pseudonymo"
  - subject: "Re: Insanely cool"
    date: 2004-04-17
    body: "Not compared to just writing + in a functional language. The whole thing is really a hack around the lack of lambda forms, IMHO.\n"
    author: "Sad Eagle"
  - subject: "Re: Insanely cool"
    date: 2004-04-18
    body: "When I first read it I thought: Ok, this adds 'sum' to each element of the vector v.... Wait, that wouldn't make sense. Ok, so I need to look up what exactly 'add_to' does. *browses possibly external header file for definition, while breaking the flow of reading code*"
    author: "Anonymous"
  - subject: "Re: Insanely cool"
    date: 2004-04-17
    body: "The STL is not a great thing to use here. Heck, you ran into number of its problems just in your example:\n\n1) Boost.Lambda is tricky, because you have to remember corner cases where things like var() are necessary;\n\n1.1) You don't mention what the compiler error looks like when you make a mistake in Boost.Lambda (its *unreadable*);\n\n2) You have to write wrappers that should absolutely be in the base language;\n\n3) You have to litter your code with useless typedefs;\n\n4) As a result of (1), and the lack of a proper lambda in C++, many of the STL algorithms are much less useful in non-trivial cases."
    author: "Rayiner Hashem"
  - subject: "Re: Insanely cool"
    date: 2004-04-23
    body: "you can do foreach in standard c++ if you are really clever\n\ncheck out the bottom of http://dot.kde.org/1082132072/1082135961"
    author: "michael toksvig"
  - subject: "Re: Insanely cool"
    date: 2004-04-18
    body: "Dylan:\n\nreduce1(method(s, i) s + size(i) end, list);\n\nMine might be a bit shorter, because your's needs to initialize sum first."
    author: "Rayiner Hashem"
  - subject: "How nice will play QT4 with STL?"
    date: 2004-04-16
    body: "\nCan I do something like:\n copy(l.begin(),l.end(),QTListBoxBackInserter<string>(listBox));\n?\n"
    author: "dotTroll (c) "
  - subject: "Re: How nice will play QT4 with STL?"
    date: 2004-04-17
    body: "How said that you can't do it now?\n\nCompile the attached example with:\ng++ main.cpp -o insert_test -lqt-mt -I[include-path-qt] -L[lib-path-qt]\n\n(Qt must have been compiled with STL support)\n\nrun it...works for me pretty well.\n\nChristian\n\n"
    author: "Christian Loose"
  - subject: "Wow, pure awesome!"
    date: 2004-04-16
    body: "Trolltech is so great. Qt4 looks just awesome. I'm particularly happy about the vector drawing engine (Arthur), and the new Interview system. It looks like Qt will remain one of the most powerful toolkits out there. If Qt4 is as good as its shaping up to be, it'll also quiet down any nay-sayers who think Longhorn will leave *NIX GUIs behind the technology curve.\n"
    author: "Rayiner Hashem"
  - subject: "Re: Wow, pure awesome!"
    date: 2004-04-17
    body: "The nay-sayers won't shut up. It's their hobby to ridicule the NIX desktop. I suspect that in ten years, when KDE has 62% of the desktop and Windows with a paltry 3%, they'll still be saying \"but wait until the next release!\""
    author: "David Johnson"
  - subject: "Re: Wow, pure awesome!"
    date: 2004-04-17
    body: "\"It's their hobby to ridicule the NIX desktop. I suspect that in ten years, when KDE has 62% of the desktop and Windows with a paltry 3%, they'll still be saying \"but wait until the next release!\"\"\n\nWell let us not get ahead of ourselves :). However, I'm using .NET now commercially, and although it is certainly an improvement on Win32 programming (C# is a very nice language) there is a heck of a lot in the NIX/Qt world that completely nullifies any hype. Rest assured that there is nothing in .NET/WinFX that you can't do just as easily using Qt and NIX based technology - quite the opposite infact :)."
    author: "David"
  - subject: "\"proper font kerning on all platforms\""
    date: 2004-04-17
    body: "hooray, at last !\n"
    author: "ik"
  - subject: "WOW! I'm impressed!"
    date: 2004-04-17
    body: "Qt 4 sounds amazing, I just hope that they will be able to deliver, that's a hume amount of stuff their promising and none of it is trivial. I'm very happy with the direction Trolltech is taking with Qt 4 and I wish them the best of luck!\n\nI'm also looking forward to Mark Summerfield and Jasmin Blachette's book on Qt 4.0. I've already got their book on 3.2.x and I found it to be pretty good, though still a  bit too advanced for me (I don't know C++ well). "
    author: "Alex"
  - subject: ":-O"
    date: 2004-04-17
    body: "drooool..\ntransparency. foreach. double-buffering. vector and pixel-based drawing. /real/ WYSIWIG. uber multi-threading. accsessbility. gui/non-gui split. designer in kdevelop. smaller. faster. \n\nIs there anything they havn't done??"
    author: "je4d"
  - subject: "Re: :-O"
    date: 2004-04-17
    body: "I'm missing the function\nQshow_app_i_have_in_mind();"
    author: "anonymous"
  - subject: "Re: :-O"
    date: 2004-04-17
    body: "DWIM! DWIM!\n"
    author: "Rayiner Hashem"
  - subject: "Goodness this is Interesting"
    date: 2004-04-17
    body: "\"In Qt 3, Qt Designer and Qt Assistant are used independently of the programming environment. Starting with Qt 4, it will be possible to use these tools from within the programming environment. Initial plans are for integration with the Visual Studio .NET, KDevelop, and Xcode IDEs.\"\n\nYes, very important. I think at the moment people are a bit mystified with using two or three development environments. With VB/VC++ etc. Microsoft learned the wisdom of binding things together.\n\n\"A new paint engine...A new text-rendering engine\"\n\nThis sounds like a premier platform to do Office suite development on....\n\n\"The new model/view framework, codenamed Interview, provides tree views, list/icon views, and tables using a common data model abstraction to support data-driven GUIs that are fast and responsive even with large data sets.\"\n\nSpot on! People moving from a Windows environment and really developing commercially will certainly appreciate this.\n\n\"Qt 4 introduces a new set of template classes, codenamed Tulip, that are lighter, safer, and easier to use than the STL's containers. Developers who are unfamilar with the STL, or who prefer the \"Qt way\" of doing things, can use these new classes instead of the STL classes.\"\n\nThis sounds great, although I will say that if you are going to do this with C++ why not use a language like C#/Java etc. in the first place? They are languages that syntactically look much better when doing stuff like this.\n\n\"Qt 4 with STL-style iterators:\n\n      QList<QString>::const_iterator i;\n        for (i = list.begin(); i != list.end(); ++i)\n            sum += (*i).size();\"\n\nCommercial development with syntax like this? Yuck! It is also terribly unreadable.\n\n\"We are putting an enormous amount of effort into Qt 4, and we are very excited about the new capabilities and technologies that we are building into it.\"\n\nSo you should be. I'm using .NET now in a big way, and beyond the hype of stuff like XAML and the future WinFX stuff I'm not all that impressed to be honest."
    author: "David"
  - subject: "Qt on DirectFB"
    date: 2004-04-19
    body: "is \"Qt on DirectFB\" project dead? as on http://qt-directfb.sourceforge.net/ -- last updates was in middle of 2003..."
    author: "gray"
  - subject: "Garbage collection"
    date: 2004-04-19
    body: "Why don't the Trolls add optional garbage collection to QT?  That would really help it when competing with Java."
    author: "Spy Hunter"
  - subject: "Let the guessing begin :D"
    date: 2004-04-19
    body: "When will KDE 4.0 (which is my guess is the first KDE release to be based on Qt4) be released?\n\nMy wild guess would be sometime in the fall 2005, say October 2005 :)\n\nAnd yes, this is of course for fun only, but I think it'll be fun to look back to when KDE 4.0 sometime in the future is released :)"
    author: "Joergen Ramskov"
  - subject: "Re: Let the guessing begin :D"
    date: 2004-04-19
    body: "mid summer 05.. if kde 3.3 comes out late summer 04"
    author: "anon"
---
Qt Quarterly is a paper-based newsletter exclusively available to <a href="http://www.trolltech.com/">Trolltech</a>'s Qt customers. As a courtesy and convenience, a selection of articles are delayed also made <a href="http://doc.trolltech.com/qq/index.html">available online in HTML format</a>. One of the more interesting articles for KDE developers in this year's first quarter issue is an article by Jasmin Blanchette which gives "<a href="http://doc.trolltech.com/qq/qq09-qt4.html">A Taste of Qt 4</a>". A Qt 4 technology preview is expected for this summer requesting for feedback and a Qt 4 Beta is planned for the second half of 2004.

<!--break-->
