---
title: "Qt Quickies: Qt 3.3.1, Qt Developer Conference, QicsTable, Independent Qt Tutorial"
date:    2004-03-04
authors:
  - "binner"
slug:    qt-quickies-qt-331-qt-developer-conference-qicstable-independent-qt-tutorial
comments:
  - subject: "many bugfixes ... and revival of old bugs?"
    date: 2004-03-04
    body: "This version seems to re-introduce an old bug: some fixed width fonts can't be used in konsole (and qtconfig) any more. For example \"Courier 10 Pitch\" gets apparently replaced by the default system font (Bitstream Vera Sans) in an extreme wide spacing. Very ugly and quite annoying. :-("
    author: "Melchior FRANZ"
  - subject: "Re: many bugfixes ... and revival of old bugs?"
    date: 2004-03-04
    body: "Same here ... :-("
    author: "tholti"
  - subject: "Re: many bugfixes ... and revival of old bugs?"
    date: 2004-03-04
    body: "This but also was a killer for me ! It's so annoying not to be able to use fixed10 in a terminal ! aaaaargh ... this makes me somewhat happier to know this is an acknowledged bug."
    author: "metoo"
  - subject: "Re: many bugfixes ... and revival of old bugs?"
    date: 2004-03-04
    body: "Why do you assume it's an acknowledged bug?  It's just reported on a random forum so far, not reported to Trolltech."
    author: "anon"
  - subject: "Re: many bugfixes ... and revival of old bugs?"
    date: 2004-03-06
    body: "06 Mar 2004; Caleb Tennis <caleb@gentoo.org> qt-3.3.1-r1.ebuild,\n files/qfontdatabase_x11.diff:\n Add a patch which fixes many font problems for users.\n Patch is from Trolltech, and will be in 3.3.2"
    author: "Ernst Persson"
  - subject: "Re: many bugfixes ... and revival of old bugs?"
    date: 2004-03-04
    body: "I have also encountered this bug and another font bug. Some of my\ntruetype fonts default to some system font. They are not  missing in\nthe list but do not render the way they are supposed to look. I did\nfile a bug report with the Trolltech people. All these fonts work fine\nwith 3.3.0."
    author: "Sammy Umar"
  - subject: "Re: many bugfixes ... and revival of old bugs?"
    date: 2004-03-04
    body: "Yes, that's a regression that appeared in Qt 3.3.1. It has however nothing to do with fixed pitch vs. proportional fonts. \n\nThe problem originated from the fact that we started loading bitmapped fonts through Xft2 in Qt 3.3.0. In some cases this lead to Qt loading fonts without Euro character even though there is a font having the Euro available. We tried to fix this in 3.3.1 (as we got some reports about this), but apparently went a bit too far, so FontConfig didn't find a valid font anymore and fell back to the default font (forced to monospace). Why FontConfig is not smart enough to at least substitute a decent monospaced font is another issue....\n\nWe have a workaround for this scheduled for Qt 3.3.2 (see attached patch).\n\nCheers,\nLars\n"
    author: "Lars Knoll"
  - subject: "Re: many bugfixes ... and revival of old bugs?"
    date: 2004-03-04
    body: "If this is not GOOD SUPPORT, support doesn't exists...\nHave you ever seen a company that answers a bug report on a forum of a news site with a patch for the bug ?!? Sometimes, I really take out the hat for TrollTech.\nThanks."
    author: "Pupeno"
  - subject: "Re: many bugfixes ... and revival of old bugs?"
    date: 2004-03-05
    body: "I can't agree more. You guys at Trolltech have made programming fun again. It's just that simple. Trolltech really feels like a company that cares about it's customers and people in general, not to mention making a quality product. Bravo!"
    author: "Will Stokes"
  - subject: "Re: many bugfixes ... and revival of old bugs?"
    date: 2004-03-05
    body: "I was hoping Lindows would include Qt 3.3.1. I guess it will probably have 3.3.2 if it is released really fast or 3.2.x."
    author: "Alex"
  - subject: "Re: many bugfixes ... and revival of old bugs?"
    date: 2004-03-09
    body: "Or they will release Qt 3.3.1 with the patch. Its common for distros to package with such patches... on Gentoo for XFree86 there are dozens of patches it applies. There are usually a few for Qt."
    author: "Ian Monroe"
  - subject: "Re: many bugfixes ... and revival of old bugs?"
    date: 2004-03-04
    body: "I have rebuilt the qt-3.3.1 rpm with the above patch and\nrecovered all my fonts....problem solved....thanks."
    author: "Sammy Umar"
  - subject: "Re: many bugfixes ... and revival of old bugs?"
    date: 2004-03-10
    body: "I used qt-copy from the KDE CVS repository. And yes, it includes the patch, and a whole others. Thanks for providing it too."
    author: "Fr\u00e9d\u00e9ric L. W. Meunier"
  - subject: "attached patch doesn't build (on rh80 or rh90)"
    date: 2004-03-10
    body: "??\n\nIn file included from kernel/qfontdatabase.cpp:633:\nkernel/qfontdatabase_x11.cpp: In function `QFontEngine*\n   loadFontConfigFont(const QFontPrivate*, const QFontDef&, QFont::Script)':\nkernel/qfontdatabase_x11.cpp:1798: `family' undeclared (first use this\n   function)"
    author: "Rex Dieter"
  - subject: "Re: patch is ok, never mind."
    date: 2004-03-10
    body: "OK, ignore me.  I had misapplied the patch.\n\nMove on, nothing to see here..."
    author: "Rex Dieter"
  - subject: "OT but related to fonts"
    date: 2004-03-05
    body: "Sorry for OT, but this is related to fonts, and I don't have platforms different enough to test to see what is the source of the problem: KDE, Qt, ot Redhat.\n\nI am using KDE 3.1.4 on Red Hat 9, i used to use 3.0.x (default installation) on RH8... and I installed TT fonts from my XP.\n\nStrange problem is that when I choose fonts, font preview is not same as what I see in app. I have to choose 2pts larger fonts. I mean, preview is 2pts larger than what realy appears in apps. What could be the source of this strange problem?\n\nTomislav\n\nPS Qt and KDE teams, I admire for your good^H^H^H^H excellent work!"
    author: "Tomislav Sajdl"
  - subject: "KGET crashes"
    date: 2004-03-05
    body: "Any one know if the KGet problem (http://bugs.kde.org/show_bug.cgi?id=75429) introduced with QT3.3 is fixed in 3.3.1?"
    author: "theboywho"
---
Trolltech has <a href="http://www.trolltech.com/newsroom/announcements/00000158.html">released Qt 3.3.1</a> with <a href="http://www.trolltech.com/developer/changes/changes-3.3.1.html">many bugfixes</a>. A <a href="http://www.ics.com/qtmeetings/">Qt Developer Conference for Northeast USA</a> has been announced to be held in Boston on May 10th with Trolltech in attendance. <a href="http://www.ics.com/">ICS</a> announced <a href="http://www.ics.com/qt/">a new table widget QicsTable</a> with <a href="http://www.ics.com/qt/?cont=qicstable/qicstableVSqtable">several advantages over QTable</a> which is <a href="http://www.ics.com/qt/qicstable/?cont=getqicstable">dual-licensed under GPL and a commercial license</a> like Qt. The <a href="http://www.digitalfanatics.org/projects/qt_tutorial/index.html">Independent Qt Tutorial</a> has been enlarged and now features <a href="http://www.digitalfanatics.org/projects/qt_tutorial/chapter11.html">a new chapter</a> introducing <a href="http://qwt.sourceforge.net/">Qt Widgets for Technical Applications</a> which gives support for advanced plotting and offers a range of new technical widgets.


<!--break-->
