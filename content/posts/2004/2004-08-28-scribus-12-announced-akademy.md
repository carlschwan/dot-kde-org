---
title: "Scribus 1.2 Announced at aKademy"
date:    2004-08-28
authors:
  - "scribusdocs"
slug:    scribus-12-announced-akademy
comments:
  - subject: "more integration with KDE?"
    date: 2004-08-28
    body: "will there be more integration with KDE -- common dialogs, kio slaves, printing system, icons, etc? Right now it's a foreign body, more like LyX (with Qt interface)."
    author: "Sergey"
  - subject: "Re: more integration with KDE?"
    date: 2004-08-28
    body: "More than likely, but not for 1.2, which will be maintained strictly for bug fixes and some minor updated.\n\nThis has already been submitted as a feature request. No time frame, but we have given it some serious discussion. As for printing, Scribus directly supports CUPS, so any printers configured via kdeprint, are automatically picked up.\n\nThis will probably be a compile time option, as we wish to keep Scribus available for MacOSX and eventually Win32. \n\nCheers,\nPeter"
    author: "scribusdocs"
  - subject: "great!"
    date: 2004-08-29
    body: "btw 1.2 is so much more responsive than 1.1.7, and a new story editor is excellent.\n\nAs for the interface, yes, I understand that Scribus tries to be more than just another KDE app, and you probably don't want to get yourself into the LyX-style UI-independence mess. But support for ioslaves when compiled for KDE will add alot productivity-wise. As for printing -- stuff like pseudo-printers is a nice addition, IMO.\n\nThanks for the marvelous work you are doing.\nAnother happy occasional Scribus user."
    author: "Sergey"
  - subject: "Re: more integration with KDE?"
    date: 2004-08-29
    body: "Take a look at the kvirc3 code, there you'll see pretty clearly and example of how to implement the KDE support at compile time."
    author: "Juanjo"
  - subject: "Great app"
    date: 2004-08-28
    body: "Scribus is really a great app.\nIt's filling are sorely needed gap in Linux: DTP.\nAlso on my wishlist (who wonders?): KDE integration\nSo I'm waiting impatiently for the next version...\nAh - BTW: Does anyone know what the biggest missing\nfeatures are before it can compete to InDesign? Or is\nthis a bit far-fetched yet? Where do we approximately stand \nbetween 0 and 100% in terms of InDesign competition.\nPlease be honest and don't say: \"Its sufficient for most \nbasic tasks\" like most open source \"addicts\" usually like \nto say ;-)"
    author: "Martin"
  - subject: "Re: Great app"
    date: 2004-08-29
    body: "The slogan of Open-source is \u0093there is no warranty or fitness for any particular task, use it at your own risk\u0094.\n \nIf you want something work right out of the box, buy Apple based or Microsoft based. This is the people's perception about Open-source products whether you like it or not. No offense please. I'm a user of open-source products and I support to change the above slogan to \u0093Our project and products conforms to Mercedes quality\u0094 :)\n\nAll most all of the open-source projects do not have schedules, develop when they feels to do so, there is no testing or quality criteria to pass, there is no proper management or developers are responsible to any person or authority unlike in commercial software development.\n\nWhat most of the open-source projects lack is quality and standards. What we need is a standardization authority to standardize projects and products. Can the ISO be kind enough to help open-source? Is there any opportunity for open-source projects to proudly display a certification on their web sites that it conforms to this level of standards?\n\nThis is the right way, I think your's and other's questions should be answered no matter whether it today conforms to 1% compared to it's commercial counterpart, but we all know if the project adhere to same quality one day it surely going to surpass the commercial counterparts."
    author: "Sagara"
  - subject: "Re: Great app"
    date: 2004-08-29
    body: "What a nice troll !\n\n\"All most all of the open-source projects do not have schedules\"\n\nWTF ? ALL big open source projects have roadmaps, release schedules, and so forth. Look at the KDE developer info for one. But also OpenOffice, GNOME, Mozilla, Shessh!\n\n\"there is no testing or quality criteria to pass\"\n\ngo to bugs.kde.org, take a look, for God's sake, what the hell are you smoking ?\n\nOpen Source developmenmt is peer reviewed, strongly remminescent of the basic sciences. Whether you like it or not. Mozilla, OpenOffice, the BSD kernels, Linux kernel, KDE, GNOME, how many examples you need ? It works.\n\nAnd yes, it can work out of the box. Boot Knoppix from a CD, install Mandrake or Suse ...\n\nI know, I know, I shouldn't feed the trolls ... I'll stop here."
    author: "MandrakeUser"
  - subject: "Re: Great app"
    date: 2004-08-30
    body: "I'm sorry if you see it as a troll, but I never meant that way. \n\nThere is a saying, \u0093be sensitive to what he say, rather than how he say\u0094 :) \n\nWe both lovers of Open Source software but the difference is how we see it and where we stand. \n\nI may be wrong, but according to your reply you are at the 'peer' end. But I'm in the middle between the peer and the developer, like the MandrakeSoft. I know the pain of compiling software from source and make it ready for peers to enjoy. \n\nThe main point what I tried to say is the \u0093requirement of an International Standard Institution\u0094 to standardize the Open Source projects and products to the bring them to the next level. Let the peer to risk burn hand is not the way I see. Just turn your telephone and see the other side what standards the manufacture followed to make it a working product for you! Actually, safety matters more than functionality.\n\nThe MandrakeSoft, Suse, etc. are not open source projects, they are commercial companies and they bridge the gap between the peer and the open source developer. Without them if you don't like Microsoft, you got turn to Apple, no other choice.\n\nWhen I say most, I did not mean all. It's mostly the case, \u0093I have great product but no manual\u0094 :) It's like to me you wear a Giorgio Armani jacket, but no pant just underwear only :) \n\nStandardization make a product usable and safe in many ways."
    author: "Sagara"
  - subject: "Re: Great app"
    date: 2004-08-30
    body: "Ok, sorry for the rant ;-)\n\nI honestly thought you were astroturfing or something. I think I now understand better what you mean, but I still humbly disagree :-)\n\nSee, under the peer-reviewed, open-source method, one important element is the \"bazaar model\":\nhttp://www.catb.org/~esr/writings/cathedral-bazaar/\n\nDevelopment is rather caothic at some instances, but there is an overall evolution towards order.\n\nTake an example, any example. Office suites. Late 90's. It was a mess. Some worked a bit, some didn't, etc. OpenOffice emerged. \n\nDE's. After several dozen projects started, two strongest contenders survived: GNOME and KDE. \n\nAs for Standarization and all, yes, it has to happen and it will happen. There are several efforts on the way. A few: LSB, freedesktop.org, this Linux Registry thing posted recently here at the dot. \n\nOne more thing: putting together open software yourself is hard. But then what ? Try doing that with the MS sources. Oh, wait, they don't offer you the source, my bad ;-) But this is the whole idea of (commercial or non-for-profit) distributions. This is WHY they exist!\n\nAnd finally: yes, I agree that commercial (opensource) contributions are important. Mandrake and SuSE are there for profit and there is nothing wrong with that. It is to be noted that Knoppix is helping bring debian to the masses. \n\nIt's all good, I still fail to see where Open Source projects are inherently low quality. There are several huge, excellent projects around, and thousands of smallish projects that will either grow strong or die ;-)"
    author: "MandrakeUser"
  - subject: "Re: Great app"
    date: 2004-08-30
    body: "Although you \"love\" open-source software, I'm afraid you don't understand it. The best measure of the quality of software is how many people use it. Do you really think people would use it if it was not any good? I installed my first version of linux in 1997 because I needed a unix-like environment to develop code for my research. I have used nothing but open-source software ever since. The number of people with a similar story has grown to millions since the early days of linux. \nMost people don't use computers to play, but depend on them to accomplish vital tasks. If open-source software (or the open-source model for that matter) was not reliable we would not be having this discussion.\nIn addition, unless you've developed and released code under an open-source license(have you?), you cannot fathom the kind of quality assurance open-source software undergoes. In the case of a product as popular as scribus, literally hundreds of programmers have looked at the code; now you tell me how many \"commercial products\" can say the same. Bottom line: the open-source model works.\nAnd as far as the \"feeding the troll\" comment  made by MandrakeUser, this kind of post stems from ignorance and misunderstanding. They will only multiply as open-source becomes more high profile; should we dismiss them or do our best to inform?"
    author: "insanepenguin"
  - subject: "Re: Great app"
    date: 2004-08-30
    body: "I don't deny the strength of open Source model.\n\nTo make things simple, lets take an example. We don't have to go too far, lets download the latest Scribus 1.2 (http://www.ibiblio.org/pub/linux/apps/office/scribus-1.2.tar.bz2).\n\nNow try to compile and install it according to the instructions given in a Intel Pentium III or 4 based computer running Linux and honestly post your experience here :)"
    author: "Sagara"
  - subject: "Shoo! Shoo!"
    date: 2004-08-30
    body: "Just type:\n    emerge scribus\nor:\n    apt-get install scribus\nor:\n    yum install scribus\nor:\n    ..."
    author: "A Numinous Cow Herd"
  - subject: "Re: Great app"
    date: 2004-08-30
    body: "Okay. Done. you can read about my experience here \n\n  wget http://www.ibiblio.org/pub/linux/apps/office/scribus-1.2.tar.bz2\n  tar jxf scribus-1.2.tar.bz2\n  cd scribus-1.2\n  ./configure && make && su -c\"make install\"\n--> IT JUST WORKS!!!\n\nNow, you do something for me: find any piece of software you like, download the source, and compile it; do it all under any Microsoft or Apple OS then honestly post your experience here. :)"
    author: "insanepenguin"
  - subject: "Re: Great app"
    date: 2004-08-31
    body: "I use Linux 2.6.7, Xfree86 4.4.0, Freetype 2.1.9, Qt 3.3.2 and KDE 3.2.3 and all installed from source.\n\nHere is my experience:\ncd scribus-1.2\n\n./configure\n:\n:\nchecking for freetype-config... /usr/X11R6/bin/freetype-config\nchecking for FT_Get_First_Char in -lfreetype... no\nchecking for FT_Get_Next_Char in -lfreetype... no\nconfigure: error: You need at least freetype 2.1.0\n\nThe freetype-config is available in /usr/X11R6/bin and /usr/X11R6/bin is available in PATH variable.\n\nI managed to get Scribus 1.2 to compile only by patching the configure script.\n\nThis is just one example. I compile and install wide range of software for Linux desktop. I must say now there are very much less bad experiences than 2 years ago.  \n\nBtw, I have no experience in compiling software in either Microsoft or Apple.\n\nI was very much happy to see the start of KDE Quality Team\n(quality.kde.org). \n\nWhat I say is we need an institution like ISO to rate Open Source projects. Rate means not one aspect, wide range of aspects, like in a beauty contest marks are given on a wide rage of criteria, not just for beauty. Thereby, each and every project can see where they stand and what are their strengths and weaknesses. \n\nToday what I see projects get disciplined by them selves. It's better than nothing. But that's not the way whole world works. They comply to well established standards like ISO 9000, ISO 14000, etc. And those companies proudly display their grade in advertisements and in their web sites. Thereby, we all know what is the level of quality of their products. By third-party standardization, you have a target to achieve, and some thing to protect. \n\nThe number of people using a product is not an indication it is a superior product. Sometimes, people use a product because no choice. One example is, OpenOffice suit on Linux, if you compare OOo to Microsoft Office suit, where does OOo stands? \n\nAnyway, guys, lets conclude our argument here as we all are busy people. I sincerely thank all of you for your time. "
    author: "Sagara"
  - subject: "Re: Great app"
    date: 2004-08-30
    body: "    \"And as far as the \"feeding the troll\" comment made by MandrakeUser, this kind of post stems from ignorance and misunderstanding. They will only multiply as open-source becomes more high profile; should we dismiss them or do our best to inform?\"\n\nPoint well taken, cheers and thanks !"
    author: "MandrakeUser"
  - subject: "Re: Great app"
    date: 2004-08-29
    body: "We know of many personal and small organisation newsletters and magazines being made with Scribus. As the release states, we also have one commercial weekly newspaper in the USA now running off OSS and of course using Scribus. I know of 2 others newspapers thinking of moving, and I know of at least one commercial magazine wanting to switch.\n\nAs for comparison to InDesign, well, many people including myself have moved to InDesign to Scribus, many from Quark. We have many professional DTP people helping out and giving advice. If its not there yet, it will be there in the future."
    author: "Craig Bradney"
  - subject: "KDE integration"
    date: 2004-08-29
    body: "Better KDE integration is really important. It could improve productivity to large scale."
    author: "Jupp"
---
The <a href="http://www.scribus.org.uk/">Scribus</a> Team is pleased to <a href="http://www.scribus.org.uk/modules.php?op=modload&name=News&file=article&sid=67&mode=thread&order=0&thold=0">announce the release of Scribus 1.2</a> - Open
Source Desktop Publishing. The 1.2 release codenamed "aKademy" is the culmination of over 1 year
of hard work since last year's 1.0 release. The Scribus team urges all users
and distros to use this latest release.







<!--break-->
<p>The Scribus Team is also proud to announce the first commercial
implementation of Scribus for commercial newspaper production. Last month,
the premier issue of Twin Tier Times, a US weekly newspaper was launched
using a production workflow centered on Scribus and other Open Source
applications including GIMP 2.0, Inkscape and others.</p>

<p>Among the major changes and features in 1.2 since the release of Version 1.0:</p>

<ul>
<li>A new EPS/PS importer, which can import EPS and PS files as vector or
postscript outlines, which can then be edited as native Scribus objects.</li>
<li>A dedicated documentation site, <a href="http://docs.scribus.net/">http://docs.scribus.net</a> has been
implemented. There are French and German translations in progress.</li>
<li>Over 800 bug reports and user feature requests have been squashed.</li>
<li>Scribus now features a New from/ Save as Template plug-in, which enables
end users to quickly generate preformatted documents. This is useful for both
first time users, as well as experts.</li>
<li>The Scribus GUI is now translated into 27 languages.</li>
<li>Scribus 1.2 features new print previewer which has the ability to render
CMYK separations on screen, transparency, UCR (Under Color Removal) to give
users a true postscript generated preview of the page.</li>
<li>A new table feature to create basic tables within Scribus.</li>
<li>Many enhancements to text frames including multi-columned text, baseline
grids, new contour options, tabs and drop caps.</li>
<li>Several new plug-ins for exporting documents as images, as well as
special typography plug-ins for Czech, French and other languages.</li>
</ul>

<p>Scribus was chosen by Trolltech as one of their "Cool Applications". We on
the Scribus Team are honored to be recognized as such and appreciate the
recognition.</p>




