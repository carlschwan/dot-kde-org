---
title: "aKademy Hackers Port Mozilla to Qt/KDE"
date:    2004-09-12
authors:
  - "kteam"
slug:    akademy-hackers-port-mozilla-qtkde
comments:
  - subject: "this is a great example..."
    date: 2004-09-11
    body: "... of Qt's toolkit and KDE's component system in action.  It was very cool to watch Zack and Lars rip though the code like no tomorrow.  I think this is a great example that shows off the power of the KDE archatecture for enterprise applications.  Now all we need is Opera and Konqueror supports basicly all the big rendering engines ;)"
    author: "ian reinhart geiser"
  - subject: "wtf?"
    date: 2004-09-12
    body: "Were you in the middle of a spasm when you wrote that?  Mozilla is already portable across three GUI toolkits.  I don't think this qualifies as a \"great example of Qt's toolkit [...] in action.\"\n\nGood work on the part of the developers, but what was that garbage about \"enterprise applications\"?"
    author: "Kannan"
  - subject: "Re: wtf?"
    date: 2004-09-12
    body: "What he means is that integrating a foreign html renderer into KDE and porting a html renderer to QT were achieved quickly thank to the high quality of both technologies. The fact that gecko has been designed to be portable was fundamental too."
    author: "Philippe Fremy"
  - subject: "Re: wtf?"
    date: 2006-02-22
    body: "toca\n"
    author: "dgfh"
  - subject: "Re: wtf?"
    date: 2004-09-12
    body: "I think he meant it as a compliment to both Mozilla and Qt/KDE related techonologies. How many ports of Mozilla have been accompilished in less than a week? "
    author: "anon"
  - subject: "questions and musings"
    date: 2004-09-11
    body: "Congratulations on the great hack!\n\nI take it that for the immediate future KDE will provide KHTML as default and Gecko as an \"alternative\".  Are typical Konqueror users expected to know that they can switch from KHTML to Gecko?  Or are we expecting that distributors will evaluate the choices and choose one themselves?\n\nWhat has the Kecko team found so far, is Konqueror with Gecko competitive vs Konqueror with KHTML?  I suppose if it is, it only makes sense to defer to Gecko as the default since it has a massive and independent development effort behind it.\n\nHave any distributors expressed an interest in using Konqueror with Gecko instead of Firefox?  I suppose the next step would be to get KaXul in Konqueror.\n"
    author: "Navindra Umanee"
  - subject: "Re: questions and musings"
    date: 2004-09-11
    body: "> Have any distributors expressed an interest in using Konqueror with Gecko instead of Firefox?\n\nNovell/SUSE has (with Novell Linux Desktop in mind)."
    author: "Anonymous"
  - subject: "Re: questions and musings"
    date: 2004-09-11
    body: "Killer!  That's quite encouraging."
    author: "Navindra Umanee"
  - subject: "Re: questions and musings"
    date: 2004-09-11
    body: "I think many KDE-default distros will be interested too, as many of them default to Mozilla-browsers or present both a Moz browser and konqueror browser--- such as Mandrake, Knoppix, Linspire, Xandros, mepis, "
    author: "anon"
  - subject: "Re: questions and musings"
    date: 2004-09-12
    body: "With improvements from Apple, I don't see the point to be honest. Firefox is a cross-platform application, not one to be integrated into a desktop."
    author: "David"
  - subject: "Re: questions and musings"
    date: 2004-09-12
    body: "Getting geko to be the standard for open source layout engines helps everyone since you get more eyes looking at a code base instead of their own code.\n\nThis just looks like further movement in the goal to merge geko and khtml.\n\nMore developers on geko (whether they use gtk and the suite (mozilla) or firefox) is a very good thing(tm)."
    author: "Mike Fedyk"
  - subject: "Re: questions and musings"
    date: 2004-09-12
    body: "Unfortunately Gecko is not better. Why on Earth do you think Apple chose KHTML (and they worked on Netscape/Mozilla!)"
    author: "David"
  - subject: "Re: questions and musings"
    date: 2004-09-12
    body: "Because it is easier to maintain and faster (since it wasn't intended to draw itself, and because accuracy wasn't a top preference)."
    author: "Anonymous"
  - subject: "Re: questions and musings"
    date: 2004-09-13
    body: "owned."
    author: "illogic-al"
  - subject: "Re: questions and musings"
    date: 2004-09-13
    body: "In all the CSS support (and correctness) tests I have seen, Gecko is the best. No doubt KHTML is getting close, but I have no good reason to take your assertion as a truth.\n\nApple chose Gecko because of size and cleaner-coding of KHTML. This is not an issue for the end-user (Gecko browsers such as Firefox are under the 10 MiB). What the end-user wants is good rendering."
    author: "oliv"
  - subject: "Re: questions and musings"
    date: 2004-09-13
    body: "Which is why Apple chose it. Browse backwards and forward between a handful of pages to see the speed difference. Apple needed a rendering engine that gave them a selling point for Safari (rendering speed) that would get IE off Mac. They succeeded. If they didn't think it would render well, they would never ever have picked it over Gecko."
    author: "David"
  - subject: "Re: questions and musings"
    date: 2004-09-15
    body: "I am sure for many people Gecko works better, but I have had some aweful luck lately.  khtml seems to do a better job on many of the sites I visit.  Great hack guys, but for now I will stick to khtml.\n\nBobby"
    author: "brockers"
  - subject: "Re: questions and musings"
    date: 2005-03-03
    body: "The advantage of this integration, is that in the future you can use gecko rendering engine with native KDE components. Just like under MacOSX, when they use Safari, everything looks OSX native, just like firefox under windows. But when I firefox on linux or OSX, the buttons all look strange (or primitive).\n\nFor MacOSX the camino project is a good start, but for what I mostly need it. It is the designmode option in Gecko, so you can edit like RichText in your browser. That works with Mozilla & Firefox, but not yet with Camino. \n\nAnd it AT THE MOMENT certainly doesn't work with KHTML. I know there is someone who has been working on it for the last two years, but it still isn't finished, and even when it is finished, it will take some time, for someone to build a script that supports RICHTEXT editing in MSIE & GECKO & KHTML.\n\nI would like to see firefox with KDE look & feel, and also firefox with MacOSX look & feel.\n\nI think that would be a win for all plattforms. I do not really like Konqueror for webbrowsing, probably because I'm used to the shortcuts for firefox, they are mostly the same under windows and linux. \n\nAnd JavaScript handling for gecko and KHTML still isn't 100% the same, so I still have to test and improve the script under KHTML, after that it works for GECKO & MSIE. But I have to say, that correcting a script after it has been made to work for GECKO is much easier, than correcting it to work for MSIE!\n"
    author: "Boemer"
  - subject: "javascript"
    date: 2004-09-11
    body: "Also, what about JavaScript in Konqueror/Gecko?  Is this still handled by kjs or does Gecko take over?  Essentially, does using Gecko in Konqueror mean that rendering will be 100% comparable with Firefox or will there be something missing?"
    author: "Navindra Umanee"
  - subject: "Re: javascript"
    date: 2004-09-12
    body: "JavaScript is done by Gecko, read Zack's blog."
    author: "Anonymous"
  - subject: "KFirefox?"
    date: 2004-09-11
    body: "Well, as neat as this sounds, I would like to see a standalone web browser that supported the extensions of Firefox on the Qt side of things. I really don't want to see Gecko rendering in Konqueror as much as a Gecko browser for KDE that's not Konqueror. Hopefully we will see this.\n\nMy main complaints about Konqueror are twofold:\n\nNo mozilla extension capability, so no adblock.\n\nClicking on home takes you to your home directory."
    author: "Turd Ferguson"
  - subject: "Re: KFirefox?"
    date: 2004-09-11
    body: "Well, when this is done, won't you just be able to use Firefox with Qt/KDE instead of GTK?"
    author: "anon"
  - subject: "Probably"
    date: 2004-09-11
    body: "Right now, Firefox can be compiled on Linux against either GTK2 or GTK1. On other platforms (Mac or Windows, it can be compiled against their native toolkit. So there's no reason to not add a third toolkit option for Linux, as it's quite possible."
    author: "QV"
  - subject: "Re: KFirefox?"
    date: 2004-09-11
    body: "I agree. I spent ages trying to work out how to set my home page before realising that I couldn't have a filesystem home page and a separate web home page.\n\nI think they should separate the web browser from the file browser. Allow the web browser to display local files and the file browser to display web pages, but you do different things in a web browser to a file browser..."
    author: "Tim"
  - subject: "Re: KFirefox?"
    date: 2004-09-11
    body: "I agree. File manager and web browser should be two apps with a lot in common, rather than the same app except configured differently. Or even just giving seperate sets of preferences for the two would be enough..."
    author: "Illissius"
  - subject: "Re: KFirefox?"
    date: 2004-09-11
    body: "Uh... you can do that already.\n\nPlease lookup the docs for Konqueror profiles."
    author: "Roberto Alsina"
  - subject: "Re: KFirefox?"
    date: 2004-09-13
    body: "We've already done this.  Yes, starting Konq the web browser can be made to have a different starting page than Konqueror the file manager.  But when you hit the Home button in the web browser, suddenly you're browsing the filesystem.  The docs for Konqueror profiles are silent on this subject (which is part of why I use Mozilla instead)"
    author: "ac"
  - subject: "Re: KFirefox?"
    date: 2004-09-13
    body: "Ok, that's trickier, but not impossible.\n\n* Create a plugin that simply goes to your home folder (that's the hard step ;-)\n\n* That plugin should now be available to add as a button in the toolbars\n\n* On the web profile, use the \"Home\" action in the toolbar\n\n* On the file manager profile, use the new plugin\n\nTada!"
    author: "Roberto Alsina"
  - subject: "Re: KFirefox?"
    date: 2004-09-13
    body: "I appreciate that a solution can be created, but I prefer my one-step solution: 1) Don't use Konqueror for anything but file browsing until this is fixed."
    author: "ac"
  - subject: "Re: KFirefox?"
    date: 2004-09-14
    body: "Had you been any less of an asshole about it, I would have published the plugin, since I just wrote it. Your loss."
    author: "Roberto Alsina"
  - subject: "Re: KFirefox?"
    date: 2004-09-14
    body: "He isn't the only one who thinks that this behaviour is weird.\nIt would be great if it'll get fixed."
    author: "CE"
  - subject: "Re: KFirefox?"
    date: 2004-09-14
    body: "It's a solution, sure, but it's also a dirty hack. May publish it on friday if I have any free time."
    author: "Roberto Alsina"
  - subject: "Re: KFirefox?"
    date: 2004-09-14
    body: "Can you not fix some CSS bugs that have been bothering me too?  I just want to see how unhelpful I can make you."
    author: "ac"
  - subject: "Re: KFirefox?"
    date: 2004-09-14
    body: "No, since that would involve actual work, instead of editing the example konqueror plugin and changing ten lines of code."
    author: "Roberto Alsina"
  - subject: "Re: KFirefox?"
    date: 2004-09-11
    body: "I don't think that separating web from file browser would be a good thing. Integration is the key KDE is so much powerful and so much different from the other desktops. If the problem with the homepage button is the only problem you have, it is a good sign that things are working quite good :)"
    author: "fake"
  - subject: "Re: KFirefox?"
    date: 2004-09-11
    body: "Yeah, I am not saying Konqueror should get slimmed down. I like its universal file viewer capability among any i/o slave, including the web. I mean, I think that it should have HTML capability, but I think that overall Konqueror makes for a poor web browser as opposed to Firefox and makes an excellent file browser as opposed to Nautilus, or heaven forbid, Windows Explorer.\n\nBut I think that Mozilla's extension system is the best around and it's foolishness not to support it.\n\nKDE is full of duplicate applications and despite my constant complaining that a lot of these should get deprecated, like kedit, kpaint, and noatun (nothing against noatun, but I find the similarities with kaboodle and its uncapabilities compared to amarok reason for its removal) to name a few, what's bad about a new web browser for KDE?"
    author: "Turd Ferguson"
  - subject: "Re: KFirefox?"
    date: 2004-09-11
    body: ">  KDE is full of duplicate applications and despite my constant complaining that a lot of these should get deprecated, like kedit, kpaint, and noatun (nothing against noatun\n\n- kpaint is already gone\n- kedit will be gone whenever kate supports bidirectional-text (being localized is very important to kde)\n- noatun+arts will be gone in kde 4.0, unless someone manages to port it to kdemm, which is unlikely since it's tied into arts very closely"
    author: "anon"
  - subject: "They're dropping KEdit? Why?"
    date: 2004-09-12
    body: "Personally, I think if they're going to drop an editor, they should get rid of KWrite.\n\nKEdit has an advantage in that it's very light. Kate has an advantage in that it's very powerful. I use both regularly, depending on what I want to do. KWrite is neither as light as KEdit, nor as powerful as Kate. I never use it, ever--there's no reason to.\n\nNow, dumping Noatun, that I can agree with. JuK runs circles around it, IMO (I recently switched to JuK from XMMS, and like it quite a bit). Unfortunately, JuK doesn't support streaming audio, but AmaroK does. AmaroK also runs circles around Noatun, tho I prefer JuK to AmaroK in every area except streaming."
    author: "QV"
  - subject: "Re: They're dropping KEdit? Why?"
    date: 2004-09-12
    body: "To gain anything they have to drop KEdit, since KWrite and Kate are the same editor. KWrite is only a light version of Kate, without all the advanced stuff.  "
    author: "Morty"
  - subject: "Re: They're dropping KEdit? Why?"
    date: 2005-05-01
    body: "Personally, the only one I use is KWrite, because a lot of files that I look at would be a lot harder to use without syntax highlighting. If I need anything more powerful than KWrite I tend to jump straight into KDevelop.\n\nI guess removing KWrite would work though, as we have a really light editor (KEdit) and a slightly heavier editor (Kate), and then a full development IDE."
    author: "David House"
  - subject: "Re: KFirefox?"
    date: 2004-09-13
    body: "I for one don't want to see Noatun go for the very simple reason that Juk sound quality is not very good (I don't know why but the sound its garbled) as compared to Noatun which plays with very high fidelity.  If someone has had the same problem and found a way to fix Juk let me know so I can  fix it too..... otherwise I would hate to boot into Windows just to listen to MP3s"
    author: "Kostumer"
  - subject: "Re: KFirefox?"
    date: 2004-09-13
    body: "I had issues with Juk on certain files causing lots of static.  My guess is it had to do with the bit rate or frequency or some other setting.  I never did figure out what caused it, as it went away when I upgraded to KDE 3.3. :-)  If you've not yet upgraded I suggest you do so, as there was probably a lot of cleanup internally for that sort of problem.\n\nNoatun has always bugged me, as it takes a long time to load and then doesn't let me edit tags.  Why bother with it? :-)  (Or if it's supposed to, it malfunctions for me.)\n\nElsewhere in this thread someone mentioned that artsd is going away in KDE 3/4/4.0.  Why is that?  Has ALSA caught up to the point that artsd is redundant?"
    author: "Larry Garfield"
  - subject: "Re: KFirefox?"
    date: 2004-09-14
    body: "Regarding the first point (why bother with noatun), I must say that I've been a long-time noatun advocate.  I tried Juk for a week or so a little while ago, and loved the id3-editing portion.  However, it fell down when it came to how I use my mp3-mplayer.  I hate how it resets to whereever you starting playing in the playlist whenever you hit stop (rather than go to beginning of the the current song).  In fact, I think that's the only thing that was wrong with it, and that alone was enough to drive me back to noatun :-)  It's something that's very simple, but incredibally annoying if done wrong.\n\nSecondly, arts and alsa are not on the same level.  If KDE switched to alsa as it's multimedia layer, it would suddenly not be usable (for sound and video) on any *BSD, OSX, or Cygwin.  Alsa does stand for Advanced *Linux* Sound Architecture, you know :-)  The last I heard (and admittedly, it's been a while), kde-multimedia was looking seriously at gstreamer to use instead.  There may be other systems, I forget.  But basing it directly on alsa (or oss, or CoreAudio, etc) is simply not an option.  Oh, and it definetly won't go away for 3.4, which by definition has to be compatible with all 3.x apps.  "
    author: "David Bishop"
  - subject: "Re: KFirefox?"
    date: 2004-09-12
    body: "I agree too! KHTML is great, but having the web browser integrated with the file manager is a real PITA. Homepages is one example (and I must commend the KDE team on there not being many more), but also just the fact that Konquror the file manager had more than enough stuffed into it with out need a web browser on top of that. Also, I can't see why they were married in the first place, a web browser and a file manager are two completely seperate concepts. Also, to the person who says this is good integration, it isn't! It would be good integration if clicking an HTML opened it in the appros browser (which I'm sure it does), but making and application bloated in terms of features is bad. Usability?"
    author: "Anonnysauraus"
  - subject: "Re: KFirefox?"
    date: 2004-09-12
    body: "It isn't a matter of making Konqueror a bloated application. It's actually a relatively lightweight container. The parts which are loaded in order to accomplish a task don't need to be loaded at the same time unless you're using both. That goes for the kparts viewers that load directly in Konqueror  as well. This argument is somewhat akin to stating that Netscape is a bloated app because Adobe's PDF plugin loads Acrobat Reader in the same window. The main difference is that the apps are more closely knit in KDE, with more options.\n"
    author: "Anonymous"
  - subject: "Re: KFirefox?"
    date: 2004-09-12
    body: "Re-read my initial post. I said bloated in terms of features, not resources."
    author: "Anonnysauraus"
  - subject: "Re: KFirefox?"
    date: 2004-09-12
    body: "I did read it, but it seems to miss the point. Konqueror isn't a web browser or file manager. It's a container application, sort of a universal meta-browser. The file manager and HTML renderer are simply plugins. Features can be added or removed dynamically by adding or removing plugins. If you don't use it as a file manager, then it isn't. The same is true about the web browser portion. Calling it bloated in terms of features seems a bit silly since features can be added or removed dynamically. If you take that away, it's no longer Konqueror.\n\nAs an example, try right-clicking on a text file in the file manager. Choose \"open in new window\" from the menu that pops up. If you have the appropriate kpart, voila, Konqueror is now a text editor. Depending on what other plugins are present, other data types may have a part that can be loaded into konqueror. PDF files, filesystem trees and audio files are examples off the top of my head. You can use the location bar or even one of the plugins to access other plugins. IMO, it's a great model for data access that isn't constrained by a focus to be a single type of application.\n"
    author: "Anonymous"
  - subject: "Re: KFirefox?"
    date: 2004-09-12
    body: "Completely agreed with mr. Anonymous from previous post.\nIf you think konqueror supports to much mimetypes, then go to the 'File Associations' dialog and remove unwanted mimes from the 'Embedding' tab. Eg. to make konqueror not supporting HTML anymore, simply configure it at text/html to not embed with the khtml part, and put your favorite browser on top of the 'Application Preference Order'."
    author: "koos"
  - subject: "Re: KFirefox?"
    date: 2004-09-12
    body: "Actually it helps me get my work done faster that kde is network transparent all over the place and that konqueror is a kpart viewer that uses ioslaves to grab data. I do web stuff for a living and in my world a lot of html resources are on remote sites that I need to access with sftp, ftp, webdav, etc. With konqueror I can go to any url that the system can recognize and an appropriate kpart is launched. khtml will render pages just fine no matter what protocol is used to get them and that saves me many hours a week.\n\nYou may consider it bloated and horrible usability but for me it is the primary purpose I use kde. I have used gnome some and the integration was just too painful. In kde I set spellcheck ONCE. I set proxy settings ONCE. I set my default editing compenent ONCE. I set my address book settings ONCE. It just saves me so much time by having stuff network transparent in a world where resources really are network transparent it is just that most are not used to dealing with network transparency. I have also worked with windows and mac machines extensively over the years and for web app stuff I consider them unusable. Overall the ioslave system should be some vfs layer just about the os (ie still userspace but below almost all the rest of userspace) So that you have one http, webdav, sftp, imap, pop, gzip etc layer on the system and EVERY app that uses it can do so. Think of how much nicer many things would be if mozilla, opera, lynx, links, wget etc did not have any understand of http at all but used a very nice vfs layer instead. You could improve the protocol, add a new version, fix bugs etc and all transparent to the apps.\n\nThe kde way really is the right way to do this to stop so much duplication of effort and they really have not gone far enough but at least the developers seem to be pushing for more transparency over time. "
    author: "Kosh"
  - subject: "Re: KFirefox?"
    date: 2004-09-13
    body: "Completely agree with Kosh - KDE's network transparency is awesome once you get used to it.  It's good for new computer users as well - you shouldn't have to use different applications for each type of file resource.  Lots of things fit very well into the ioslave model, so you have two great usability bonuses:\n\n- Only learn one overall model for file access. (protocol:/[domain/]path, with graphical browsing to make the path part easy)\n- Only learn one interface for file transfers and access (Konqueror)\n\nCompare with the way GNOME are trying to do it - one model for the desktop (spatial), another for file dialogs (hierarchal/browsing), and then another for remote access (URL based). The internet is here to stay and KDE embraces it.\n\nI prefer to think of Konqueror as a GUI equivalent of a shell.  No one would say that bash was bloated because you can do a million and one things with it - it is the glue that allows you to use the other programs.  Konqueror is the same for file access and viewing, except you don't need to have programmer leanings to use it."
    author: "Luke"
  - subject: "Re: KFirefox?"
    date: 2004-09-13
    body: "\"KHTML is great, but having the web browser integrated with the file manager is a real PITA.\"\n\nThat's your opinion, but a lot of us love it. I started my GUI life with OS/2, and was consequently weaned on the document-centric model of the desktop. Windows is the opposite with the application-centric model. What Konqueror does is to provide more document-centricity to the desktop. It's not perfect (nothing is), but being able to browse anything without worrying what application you're using is a major feature.\n\n\"a web browser and a file manager are two completely seperate concepts.\"\n\nNot at all! Think of them as purpose-specific browsers. One browses the web and the other browses the file system. The actions for each are remarkably similar (back, home, follow, etc). Konqueror is a universal viewer with browsing capabilities. It's a generic browser. It's not just for the web or just for the file system. You can browse an ftp site as if it were a remote filesystem, or an audio CD as if it were a digital CD full of MP3 and OGG files. You can browse your photo gallery without having to open each photo in a separate viewer or creating a gallery html page. Dump xman and browse your man pages in Konqueror! Etc, etc, etc."
    author: "David"
  - subject: "Re: KFirefox?"
    date: 2004-09-13
    body: "Exactly. What a lot of people seem to miss is that much so-called file-management is, in fact, document browsing. Go to some directory. View six out of eight files. Dump the first. Browse somewhere else. Check the contents of a text file.\n\nI seldom use Konqueror for file management -- I more at home with bash -- but I use it a lot for document browsing. And that's where, for instance, the OS X finder falls short."
    author: "Boudewijn Rempt"
  - subject: "Re: KFirefox?"
    date: 2005-06-07
    body: "I completely agree. \n\nTake this. In Konqueror, split the content pane into left and right views. Then, click at the right view, click the <home> button to go to your home direcotry. Then, click at the left view, typing \"ftp.kernel.org\" in the URL bar to get a dirctory listing. Then click and click to find your favoriate version of kernel, point on it, drag it to the right view. After some minutes, you got the .tgz file in your home directory. \n\nNow just double click and double click on the .tgz file to get a virtual directory tree of the new kernel. Right click on the Readme file, select \"open in new tab\" to view the file in other tab. Or do the same with a .c file and view the source with syntax highlighting. With some setting, you can even view the file in the left view without opening other tab.\n\nI never saw such capability in any other desktop environment. I love and adore Mozilla, but I must dump it after migrating to Konqueror and KDE.\n\n"
    author: "Monte Lin"
  - subject: "Re: KFirefox?"
    date: 2004-09-11
    body: "\"Clicking on home takes you to your home directory.\"\n\nFor all those who don't like this behavior: Vote here: http://bugs.kde.org/show_bug.cgi?id=8333. But please, don't add comments before reading through all the comments already made. This bug is already a mess.\n\nBTW: I can only select Encoding: \"Plain Text\" and the html tags don't work when submitting a comment to the dot. Does anybody know why? I am using Konqueror 3.3"
    author: "Michael Jahn"
  - subject: "Re: KFirefox?"
    date: 2004-09-12
    body: "People used to abuse HTML comments, so it was disabled."
    author: "AC"
  - subject: "Re: KFirefox?"
    date: 2004-09-12
    body: "I'd like that very much. \nI prefere Firefox, but I use konqueror because:\n\n1. Little-to-none respect for (KDE's) file-associations in Firefox and Mozilla, when I click a link to a .pdf in my browser I want it to suggest opening it in the associated program. eg. kpdf.\n(I know this might change, if/when KDE/GNOME starts the share MIME-types, but still)\n2. Forms looks very bad (old) in Firefox.\n3. Different filedialog from the rest of my apps (I use mustly KDE-apps)\n4. Very bad printing dialog in Firefox.\n5. Konqueror seems to start much faster.\n\nThis is NOT a bashing of Firefox, as I already said I prefere Firefox.\n\nI think most of my complains would be resolved if Firefox were ported to Qt/KDE.\n\nI miss my plugins and the better IMO pop-up protector. And oh, I don't want to change browser to use my home-banking, only Firefox+Blackdown-java-1.4.1 works with mine."
    author: "Morten"
  - subject: "Re: KFirefox?"
    date: 2004-09-12
    body: "No adblock was the only think keeping me from switching to konq until I found http://www.privoxy.org. Give it a try. It runs circles around adblock, and because it runs as a proxy you can have all your browsers go through it, which is really great if you need to use different browsers for different things during the day.\n\nFor me the killer feature in KDE right now is spell checking in every text box. That's why I finally had to switch to konq."
    author: "Scott"
  - subject: "Re: KFirefox?"
    date: 2004-09-13
    body: "Yes!  I use/love KDE and Konqueror, but the tight browser/file integration drives me nuts.  \n\nTo the various people below who point out that Konqueror is just a chrome around IOSlave plugins and that there are profiles etc. etc., you're missing the point.  I know how Konqueror is built architecturally, but from a user perspective that is irrelevant.  When I am MANAGING files, I want the application to work in one specific way.  When I am viewing web pages (i.e., HTTP IOSlave to an apache server somewhere), I want it to behave VERY differently.  I want my \"browser application\" to always have tabs visible, and my \"file manager application\" to never have tabs visible, because that's how I like to work.  I want web URLs on my bookmark toolbar for my \"browser application\", and either none or common local URLs like my music directory or remote SFTP servers on my \"file manager application\".  Doing that with a single unified program/chrome/framing system is extremely difficult.  Perhaps profiles could help there, but the fact that I never figured them out is proof that they're not sufficient. :-)\n\nIf the solution is to have a KDEified Firefox as a standalone HTTP-viewing-only application and then just set Konqueror to direct HTTP to KFireFox or whatever, so be it.  That would suit me fine.  \n\nAs for the differences in the engines and their feature sets, welcome to the glory of Free Software.  I love the auto-spellcheck in Konqueror.  No reason that can't be ported into Gecko.  I find the popup blocking in Firefox far superior to that in Konqueror.  Port that over into KHTML/Konqueror.  Let both engines share and improve.  That's what makes FS/OSS superior to proprietary/closed software.  Both sides can improve without anyone being bought out.  \n\nAnd if in the end we end up with Gecko-with-KHTML-inspired-improvements as the de facto default browser/engine for all FS/OSS web browsing, so much the better.  This is the browser/engine that the entire security world and Homeland Security (in the US) is recommending over IE, so it's the one that clueful but pragmatic developers are going to support.  If the OSS world can standardize on a unified \"single target\" browser and rendering engine, that provides a united front to make everyone's lives easier and try to stamp out IE, and making the Internet a safer place in the process."
    author: "Larry Garfield"
  - subject: "Re: KFirefox?"
    date: 2004-10-01
    body: "I want to use Firefox, but I need that when I open a link to an image file,  the file opens in a program different from Firefox. I can do it with konqueror, but I dont know if it is possible whit firefox\nCan you help me?"
    author: "Leire"
  - subject: "Wow.."
    date: 2004-09-11
    body: "This a very surprising and exciting! And it raises an even more important question: if the Gecko port offers all the features of the current engine and can be embedded easily, should KDE get rid of khtml + kjs? \n\nWarning: uninformed user opinion below ;)\nI am all for it. Now don't get me wrong, I use konqueror almost exclusively. And I don't think this is very likely to happen but one may speculate. The reasoning:\n1. Gecko has much more exposure (even though there is Safari) and thus at least _some_ web developers pay attention to make their site compatible. \n2. Reduce duplication of efforts. The OSS world would have only one major rendering engine.\n3. IMHO Gecko is still a little more standards compliant.\n4. The most important part last: devs could spend more time improving KDE instead of working on khtml :-)\n\nWhat do you think?"
    author: "Michael Jahn"
  - subject: "Re: Wow.."
    date: 2004-09-11
    body: "Absolutely not.. \n\n1. there are only a few khtml developers anyways (e.g, less than 5 of them)\n2. only one major open-source engine is not good\n3. khtml has a more elegant design and probably has more expansion possiblities in the future. I bet khtml gets things like WebForms much quicker than Gecko does. "
    author: "anon"
  - subject: "Re: Wow.."
    date: 2004-09-11
    body: "agreed.. and, IMHO, the most important thing is: having our own html rendering engine can give us the ability to add features, and integrate them fully in KDE, without having to rely on an external (and cross-platform) project."
    author: "fake"
  - subject: "Re: Wow.."
    date: 2004-09-11
    body: "Yes, I agree with all of the arguments made. Well, at the very least it is an impressive feat and choices don't hurt anybody. Thanks, devs :)"
    author: "Michael Jahn"
  - subject: "Re: Wow.."
    date: 2004-09-12
    body: "well.. i don't agree.\n\nthe future is: cross-platform. and gecko has it"
    author: "FJ"
  - subject: "Re: Wow.."
    date: 2004-09-12
    body: "Thanks for allowing all of us a glance in that glass bowl of yours. "
    author: "Andre Somers"
  - subject: "Re: Wow.."
    date: 2004-09-12
    body: "What is wrong with the cross-platformness of khtml? It's already the defacto native browser of OSX, the native engine of the most popular Linux desktop, and will be arriving in Windows soon. "
    author: "anon"
  - subject: "Don't take me wrong but..."
    date: 2004-09-11
    body: "There are things Mozilla can't do. And Opera can't, too.\n\nOnly Konqueror deals correctly with certain sites. I'm not sure whether it's because of KHTML, it probably has to do with going beyond standards to support certain \"javascript\" proprietary extensions.\n\nWhile I'm not exactly enthusiastic about this, it is more or less like Openoffice.org excellent compatibility with the .doc format: it is essential to make proprietary software, erm, less relevant.\n\nSo, yes, I think this Gecko porting is great (and technically impressive, considering the short time they spent). Nonetheless, what I think is more important is that people are starting to exchange ideas and programs.\n\nIsn't it what open/free people are supposed to do best? :-) I think this is really good to the spirit.\n\nGood luck to y'all!"
    author: "Gr8teful"
  - subject: "So as to make my post more complete..."
    date: 2004-09-11
    body: "www.monark.com.br, click on \"Produtos\" and after a new page loads, click on words beside red arrows on the left. Expanded submenus only appear in Konqueror.\n\nI'm not affiliated with such company."
    author: "Gr8teful"
  - subject: "Re: So as to make my post more complete..."
    date: 2004-09-12
    body: "The website works great in Internet Explorer 6.0 SP1, which is what it was designed for, I am 100% sure.\nWho the fuck cares what it looks like on a web browser running on a dead os that *NOBODY* uses.\n"
    author: "timecop"
  - subject: "Re: So as to make my post more complete..."
    date: 2004-09-12
    body: "Never feed the trolls & all that, but still...\n\nFirst: the usage of a webbrowser for a given website is never 100% IE6.  So the $$$ question should come to mind: which of my customers am I p*ss*ng off?\n\nSecond: be careful if you target an academic audience or certain other large organisations.  Chances are that the people there are still forced to use Netscape 4.7 on an NT4 system or something.\n\nAs long as websites are still not build using 100% standard code, choice is good.\n\n'nough said.\n"
    author: "Matt"
  - subject: "Re: So as to make my post more complete..."
    date: 2004-09-13
    body: "Why post to this site?"
    author: "David"
  - subject: "Re: Don't take me wrong but..."
    date: 2004-09-13
    body: "You make a good point. I was with a group of Mac users last weekend. One worked for Apple, one for a major ISP, and one for a law firm. All used both Camino and Safari. All said that there were some sites Camino could render and Safari not, and others that Safari could render and Camino not.\n\nAnd all agreed that they preferred Safari."
    author: "David"
  - subject: "Re: Wow.."
    date: 2004-09-11
    body: "> should KDE get rid of khtml + kjs? \n\nNo! This is exactly the kind of senseless prattle that we should avoid as somebody is bound to think it's inside information. I was hacking Quanta only a couple meters from Lars and Zack, and KHTML is Lars' baby, along with a few other developers. I don't see them ditching it, thankfully.\n \n> Warning: uninformed user opinion below ;)\n\nYes indeed. And while everyone has a right to their opinions we should also recognize that it's generally better to direct things from informed opinions. ;-)\n\n> 1. Gecko has much more exposure (even though there is Safari) and thus at least _some_ web developers pay attention to make their site compatible. \n\nNeither has enough market share to make a difference, but together they approach significance. FYI it's W3C standards compliance that serious web developers (who are not just using FlunkPage) want. That's a compliance issue with CSS-2 mainly right now.\n\n> 2. Reduce duplication of efforts. The OSS world would have only one major rendering engine.\n\nRemember KHTML working great when Mozilla was at 0.8 and less and had egregious rendering errors and couldn't even show form buttons right among other things? Go look at www.w3c.org and read everything there (that should take about a year) and tell me that you want to trust everything to one rendering engine. If that logic worked why not use IE? One is what MS is about. Choice is what OSS is about.\n\n> 3. IMHO Gecko is still a little more standards compliant.\n\nOkay, a little, but not by much. Now look at the architecture. Why did Apple choose KHTML when their team members were involved with Mozilla even though it was slightly more compliant? The answer to that question is why dropping it is utterly nuts.\n\n> 4. The most important part last: devs could spend more time improving KDE instead of working on khtml :-)\n\nBZZT!!! News flash! KHTML is part of KDE. Flash number two... uninformed users _shouldn't_ decide what developers work on... In fact since they don't what's the point? It's interesting that people almost always offer opinions with no substantial basis but rarely ask questions to determine if the facts support their initial supposition.\n\nInformed software decisions by developers based on their assessments is why OSS is growing so fast and producing such respected software. What has been the problem with commercial software? Bean counters, investors, cliche marketing types and clueless journalists have had too much input in direction and scheduling. Think about it. If you're infected with interest (which is how developers get hooked) then seek information. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: Wow.."
    date: 2004-09-12
    body: "Lay off the coffee for a while, man."
    author: "yeahright"
  - subject: "Re: Wow.."
    date: 2004-09-13
    body: "indeed. He's more than allowed to give his opinion and If I read correctly, he actually changed his ming about some of the things he said after becoming informed.\nSee how much good can happen when you don't go off attacking those unfortunate enough to not know better and instead explaining in a calm manner?"
    author: "illogic-al"
  - subject: "Re: Wow.."
    date: 2004-09-14
    body: "Maybe Eric's post was a little harsh, but he is absolutely correct. Choice is a good thing. Competition is a good thing. We need to get away from the mentality that there can (or should) only be one product in any category. If you are a commercial software company it is in your best interest to try to kill off competing products until yours is the only one left. But, if you are a consumer/user it is in your best interest to encourage the existance of multiple competing products, even if you only use one of them. If only one product exists what will you do when you become unhappy with it? \n\nCompetition causes products to improve faster. It also keeps any one product from dictating standards or having too much control of a market. Do we really want to have one development team dictating the future of the web? That is what we have now with Microsoft and IE. Is anyone happy with the current situation we're in? I'm not. \n\nIt is good to have multiple competing browser engines. It helps keep the browser developers focused on supporting standards instead of trying to rule the world. It also keeps web designers from getting lazy and only supporting the dominant web browser. \n\nBy the way, Mozilla is my browser of choice. I only use Konqueror as a file manager and to test web pages. But I hope KHTML has a bright future. KHTML is important regardless of whether or not it is the number one browser engine or not."
    author: "wg"
  - subject: "Re: Wow.."
    date: 2004-09-13
    body: "Reducing the duplication of effort is a trap. Do not fall for it. This is the antithesis of Free and Open Source Software. Duplication of resources has a great many advantages.\n\nFirst of all, the different projects are not interchangable. A developer that is happy working on KHTML might not be happy working on Gecko. Or vice versa. Similar arguments about merging (or dumping one of) KDE and GNOME have been around for years, but they ignore that KDE development is a completely different thing than GNOME development. The communities are different, the cultures are different, the even the coding is different (C vs C++, GTK-- vs Qt, multiple disjoint APIs vs fewer complementary APIs, etc).\n\nSecond, different free projects allows cross-pollination of ideas and code. Tabbed browsing came about because *one* browser first decided to try it out. GCC only got decent C++ support because Cygnus forked off egcs. Think of all the stuff you dislike about GNOME (or KDE) and imagine that they were the standard with no alternative.\n\nThird, why dump KTHML? Why not dump Gecko instead? Perhaps the vote will go against you. While Gecko has more support for \"broken\" HTML, KHTML is smaller and faster. Would you stop using a Gecko browser if the \"vote\" for a single engine went against you?\n\nFourth, choice is king. There are lots of reasons why people use Free and Open Source Software, but choice ranks up near the top. This is not a dictatorship. You cannot tell a developer what she can or cannot code for. You cannot tell a user what he can or cannot use. Heck, you can't even tell them what kernel they must use! This is not a proprietary commercial software house where the CEO gets to choose what the employees will be working on. This is not their product where the customer gets a one-size-fits-all solution."
    author: "David"
  - subject: "Where is the code?"
    date: 2004-09-11
    body: "I have looking for some NEWS in Konqueror homepage and Mozilla, but I can't find anything related to this.\n\nWhere is the code for that? What's the actual status? Is it usable?\nAre there more links about it?\nThx"
    author: "KikoV"
  - subject: "Re: Where is the code?"
    date: 2004-09-12
    body: "The code could not be committed before the announcement, don't argue about that. And if you follow the next top-level comment to Zack's blog you will read that he has limited internet connectivity at the moment. So just wait a few (work) days."
    author: "Anonymous"
  - subject: "Zack's Blog"
    date: 2004-09-12
    body: "Be sure to don't miss <a href=\"http://www.kdedevelopers.org/node/view/615\">http://www.kdedevelopers.org/node/view/615</a>\n"
    author: "Anonymous"
  - subject: "SpiderMonkey; Standards compliance of KHTML"
    date: 2004-09-12
    body: "Just added this comment to specify that Mozilla's ECMAScript/JavaScript engine consists of SpiderMonkey (it wasn't clear in some replies, which seemed to assume that Gecko absolutely has to replace kjs)... So we have:\n\nKonqueror:\nKHTML\n KSJ\n\nMozilla:\nGecko\n SpiderMonkey\n\nThis also means that combinations of the two would also be possible if it was necessary provided there be a unified API between the ECMAScript engines...\n\n\nAlso, what I also consider a productive effort would be to make optional all non-standards behavior in KHTML and KJS. A user could decide to enable or disable proprietary extensions which are already implemented then, and if those extensions were documented (I.E. for each \"feature\" a checkbox with a comment, it would even be better. This would allow KHTML to be used even by standards pedantics, while not necessarily limiting it's functionality, and Web developers to easily migrate non-standard code to standard one, disabling a particular feature one by one and seeing where it breaks, to address each problem one by one..."
    author: "anonymous"
  - subject: "Re: SpiderMonkey; Standards compliance of KHTML"
    date: 2004-09-12
    body: "Very interesting. Thanks.\n\nBTW, a lot of middleware work depend on such JS quirks, which force me to use IE (albeit I have Firefox and Opera installed)."
    author: "Gr8teful"
  - subject: "Re: SpiderMonkey; Standards compliance of KHTML"
    date: 2004-09-12
    body: "I love your proposition ! I allready think about that but you explain it very clearly. \nYou must fill in a whishlist. I'll vote for it :)"
    author: "Shift"
  - subject: "Interesting Idea"
    date: 2004-09-12
    body: "Assuming this gets into konqueror at some point, Have a per-domain setting on which rendering engine to use: ala plugins, and browser id.\n\nThat would make sites that don't work with khtml (gmail) or gecko available to users of one or the other engine. \n\nAmazing work, as usual. Thanks to all the people who work on KDE."
    author: "James L"
  - subject: "Re: Interesting Idea"
    date: 2004-09-12
    body: "this is a very great idea, i use firebird most of the times nowadays but there are a couple of sites that doesnt act like they are intended to be with mozilla, so i switch to konqi. so i think this would be the most useful feature that would come with doubling the engines"
    author: "emre"
  - subject: "So..."
    date: 2004-09-12
    body: "...is Gecko finally cleanly separated out of all those different Mozilla products?  Or is this just the addition of choice to depend on a whole Mozilla software package even when just using a part of it which should instead be available as runtime library?\n\nFor years GNOME (as in Epiphany and Galeon) already uses Gecko as default web rendering engine. But that didn't get anyone to finally solve the above mentioned huge dependency, leading to the experimental import of Webkit (aka khtml + kjs) into the GNOME CVS (see http://cia.navi.cx/stats/project/gnome/gnome-webkit ). I'm afarid this announcement is a nice showcase of flexibility of the involved projects, but not really anything more."
    author: "ac"
  - subject: "Replacing Firefox with Konqueror + Gecko"
    date: 2004-09-12
    body: "Now i can think in remove Firefox from my machine and start to use Konqueror for access my gmail account..."
    author: "Rog\u00e9rio"
  - subject: "Re: Replacing Firefox with Konqueror + Gecko"
    date: 2004-09-12
    body: "http://www.kde-apps.org/content/show.php?content=14927"
    author: "fab"
  - subject: "Thank you konqueror developers"
    date: 2004-09-12
    body: "While I can see value in having Firefox/Mozilla ported to QT/KDE, I prefer konqi. It is fast. It is clean. It does what I want it to do. In addition, one problem that I see with this port is that in the past, these kind of ports fall to the way. "
    author: "a.c."
  - subject: "Well..."
    date: 2004-09-12
    body: "This is obviously great news for all the Firefox users, who know can have a KDE native version.\nThough, as a KDE user and lover, I would have preferred to see time spent hacking on KHTML/Konqueror instead of on Gecko. I mean, we all know that since 3.2 our beloved Konqi has made giant steps. But there are a few things still missing or to polish more, like cluttered menus, better CSS2 support, or an integrated adblock-like plugin. Well, why don't spend time hacking on those?\nI'm not saying that porting Firefox is bad, since it isn't. But shouldn't Konqi have more \"priority\" on the development?"
    author: "fake"
  - subject: "Re: Well..."
    date: 2004-09-12
    body: "> or an integrated adblock-like plugin.\nUse Privoxy."
    author: "Anonymous"
  - subject: "Re: Well..."
    date: 2004-09-13
    body: "Yeah, who doesn't like annoying checkboard patterns where annoying banners used to be?"
    author: "Zooplah"
  - subject: "Re: Well..."
    date: 2004-09-12
    body: "Isn't great that developers do what _they_ want? I love it. ;-)"
    author: "Christian Loose"
  - subject: "Great!"
    date: 2004-09-12
    body: "This is great news, I can't wait to try it out.\n\nKonqueror is my primary browser, keep up the good work guys!"
    author: "Mikhail Capone"
  - subject: "Configuration"
    date: 2004-09-12
    body: "One of my favorite aspects of Konqueror is the site-specific settings.  Will these settings work with Gecko?  Can I enable javascript for some sites and disable it for others?  Can I accept and reject cookies based on the site I am visiting?  Can I make the browser identify itself as Lynx on some sites and Internet Explorer on others?  Can I choose the rendering engine itself based on the site I'm visiting?\n\nHow about other config settings, like the default fonts, animation settings, or form completion?\n\nKHTML also allows KMail to not load external images and other resources.  Will this functionality apply to the Gecko KPart?\n\nWhile I'm on the topic of settings, could we have a setting to lower the priority of the javascript interpreter?  Some sites have scripts that seriously bog down the browser.\n"
    author: "Burrhus"
  - subject: "Re: Configuration"
    date: 2004-09-12
    body: "Oh, i didn't know you could do that with Konqueror :) mozilla/firefox can do that for years"
    author: "Joruus"
  - subject: "Re: Configuration"
    date: 2004-09-12
    body: "Sadly there is no UI for it (at least not for the complete version)"
    author: "Anonymous"
  - subject: "Gecko vs khtml"
    date: 2004-09-12
    body: "Look at screenshots of the article:\n\nhttp://www.solutions.lv/~dimss/gallery/tmp/khtml_vs_gecko/index.html\n\nI think they speak for themselves...\n"
    author: "Dmitry Ivanov"
  - subject: "Re: Gecko vs khtml"
    date: 2004-09-12
    body: "Congratulations, you proved that you don't use the current Konqueror version."
    author: "Anonymous"
  - subject: "Recent KHTML much better than Gecko - see Link!"
    date: 2004-09-12
    body: "Konqueror 3.3:\nhttp://home.knuut.de/MWieser_/Konqueror-3.3.png\n\nGecko:\nhttp://www.solutions.lv/~dimss/gallery/tmp/khtml_vs_gecko/20040912-103013.png\n\nGecko does not even show the links to the Gnome-, KDE-, Mac OS X-Reviews and the Digital Video Cleaning guide!\n"
    author: "Asdex"
  - subject: "Re: Recent KHTML much better than Gecko - see Link!"
    date: 2004-09-12
    body: "You're comparing different urls ;P"
    author: "too fast"
  - subject: "Re: Gecko vs khtml"
    date: 2004-09-12
    body: "Uh........ you aren't using Konqueror 3.3 are you?\n\nHere is a screenshot in Konqueror 3.3:\n\nhttp://128.61.66.26/snapshot4.png\n\n"
    author: "anon"
  - subject: "Re: Gecko vs khtml"
    date: 2004-09-12
    body: "Since they use Quirks mode, no browser is supposed to show this correctly"
    author: "Anonymous"
  - subject: "Re: Gecko vs khtml"
    date: 2004-09-13
    body: "Which theme for Firefox do you use?"
    author: "aim"
  - subject: "WYSIWYG Editing in Konqueror"
    date: 2004-09-12
    body: "I am very pleased to see this project. Konqueror does most of the things right already (I've been using it as my default browser for a couple of months now), but there is one thing only the Gecko engine is able to do on Linux right now: in-page WYSIWYG editing. If I get it right, a Konqueror with the Gecko engine should be able to run the existing Mozilla-based WYSIWYG-editors like HTMLAREA or Kupu. That would eliminate one of the last reasons for me to still use Mozilla/Firebird from time to time ..."
    author: "Joachim Werner"
  - subject: "Re: WYSIWYG Editing in Konqueror"
    date: 2004-09-12
    body: "Maybe you should check out Quanta. It's \"Bleeding Edge\" branch features just that, and yes, that's based on KHTML."
    author: "Andre Somers"
  - subject: "Re: WYSIWYG Editing in Konqueror"
    date: 2004-09-12
    body: "I think Joachim means in-page HTML editing, just like a textarea but with bold, italic, lists, tables, images, etc. that generates HTML that can be submitted with a form button to a content management system, not a full WYSIWYG editor for HTML-files."
    author: "wilbert"
  - subject: "Re: WYSIWYG Editing in Konqueror"
    date: 2004-09-12
    body: "No need for using the \"Bleeding Edge\". Quanta 3.2 and 3.3 has the WYSIWYG (VPL) support. Well, by mistake it's disabled in the 3.3.0 release, but it's there and mostly works. And it heavily depends on KHTML, so I hope that it's development won't be stopped. ;-)"
    author: "Andras Mantia"
  - subject: "Re: WYSIWYG Editing in Konqueror"
    date: 2004-09-13
    body: "Koos Vriezen today posted his first try at implementing Mozilla's MIDAS editing. See\n\nhttp://lists.kde.org/?l=kfm-devel&m=109501847920318&w=2 "
    author: "anon"
  - subject: "Mozilla Platform with QT will be unstoppable"
    date: 2004-09-12
    body: "I don't think many understand the ramifications of this news! \n\nNow we can have all the Mozilla apps, firebird, sunbird, etc.., as well as the mozilla platform internals XUL, XBL, XPCOM, spidermonkey, etc. work with QT and KDE. \n\nThis is the best news I have seen all year.. "
    author: "Anthony Tarlano"
  - subject: "Also available for kde 3.3?"
    date: 2004-09-12
    body: "This are really great news. \nDoes anybody know if this will also be available for kde 3.3 or just for 3.4(4.0)?\n\nTom"
    author: "0tom0"
  - subject: "KHTML deprecated?"
    date: 2004-09-12
    body: "Please please do *NOT* kick khtml.\nMake \"kfirefox\" a choice (optional), but khtml is just so cool.\nkhtml is THE choice for me, and as maybe some of you know: even the \"brand new firefox\" has its old contamination (hope this is the right word) whereas khtml s so clean and *small*.\n\n- khtml fan"
    author: "KDE developer"
  - subject: "Re: KHTML deprecated?"
    date: 2004-09-13
    body: "Are you retarded or brain damanged? Who said that khtml is depreciated? "
    author: "anon"
  - subject: "Re: KHTML deprecated?"
    date: 2004-09-17
    body: "I wouldn't worry if I were you.  I recall that when the Navigator 5.0 source release was announced, there were two camps: the people who got all excited that KDE could integrate Navigator components, and people who were upset that the KDE-native engine might die.  It's been a few years; KHTML is still around.\n\nStill, it's nice to have the option of using Gecko.  May the best engine win."
    author: "regeya"
  - subject: "Happy about this development"
    date: 2004-09-13
    body: "I gotta say i'm quite happy about this development \nfirst of all because firefox never looked right in kde due to its gtk themeing \nand second of all if they manage to seperate Gecko Engine from Mozilla properly as kpart then i can use Konqueror Exclusivly and for me it means less space being used "
    author: "Matthew"
  - subject: "whats the latest commit import from Apple WebCore?"
    date: 2004-09-15
    body: "well, thats good we now have a new standalone browser made with Qt/KDE libs.\nknow, we need konqueror ship as a standalone packages (with khtml)\n\nwhat about fix from apple, long time I have not heard about new commits from Apple. they are at version 1.25 in Safari 1.2. And gmail works fine in safari but don't in konqueror. please get the new fixes.\n\nhttp://developer.apple.com/darwin/projects/webcore/\n\n"
    author: "somekool"
  - subject: "Re: whats the latest commit import from Apple WebCore?"
    date: 2004-09-15
    body: "> please get the new fixes.\n\nGetting them is easy. But extracting them from there and merging into khtml is the work."
    author: "Anonymous"
  - subject: "Re: whats the latest commit import from Apple WebCore?"
    date: 2004-09-17
    body: "Getting \"magic fixes\" from WebFork is not easy, mind you.\nThere's no public CVS. All you get is a big fat tarball with very few hints about what changed or why.\nWas it something really wise, some one-time dirty hack to keep a common DHTML script happy, or some pointless rename-half-the-methods-so-PHBs-think-we-are-going-ahead (they do that at every friggin release)?\n\nYou take your pick painfully after hours of code scrutiny, and that is not exciting.\n \n"
    author: "."
  - subject: "Re: whats the latest commit import from Apple WebCore?"
    date: 2004-09-18
    body: "wow,... if only I knew....\n\nsharing a same repository would not have been an option ?\nI guess they don't want to... \nand I suppose they are not generous enough to send patches for important stuff .... its another marketing thing who does not help anybody.\n\nwhat about the first few ? does Apple gave more collaboration, or that just was as painfull ?\n\n"
    author: "somekool"
  - subject: "Credit where credit is due"
    date: 2004-09-15
    body: "The first time I skimmed this article, I had the sense it was more one-sided than it actually is.  The author /did/ acknowledge that the ease of porting was due to the design of both KDE and Mozilla.  My original reason for returning to the article to respond was to stress that Mozilla's design was probably a significant element in the success of the effort.  After reading it again, that's not necessary. Nonetheless, I do want to point out what appears to me to be a very noteworthy aspect of Mozilla's design approach:\n\nhttp://www.mozilla.org/scriptable/xpidl/idl-authors-guide/rules.html#interfaces\n\nI've never actually written code using the approach, but it does seem to provide a framework for very clearly defined interfaces.  I'm not saying KDE should or could use that exact approach.  I'm just pointing it out as food for thought."
    author: "Steven T. Hatton"
  - subject: "Mozilla gets my credit for the framework when..."
    date: 2004-09-15
    body: "...they finally ship Gecko separated from all their Mozilla products. That is something they promised to do for years but still haven't realized. Currently non-Mozilla project still have to rely on and be compatible to a particular version of a full whatever-Mozilla-product installation. Also within the Mozilla products available at one given time the discrepancy between the used Gecko version in Mozilla, Firefox, Thunderbird etc. is a whole mess and a whole lot of unnecessarily duplicated code on the HD, and every single update to either the UI or the Gecko core pushes the user to update the whole thing again.\n\nConsidering that the Mozilla foundation is concerned about market shares for their XUL technology (which I agree is great) I have to seriously question their priorities, a Gecko runtime should have existed from the very beginning instead keeping it bundled with software demoing the technology and breaking compatibility to third party software with each version."
    author: "ac"
  - subject: "Re: Mozilla gets my credit for the framework when..."
    date: 2004-09-15
    body: "For those interested in contributing to Mozilla (and supporting its use in other projects) and wondering what I'm talking about, look at http://www.mozilla.org/projects/xul/xre.html (stalling since nearly two years now)."
    author: "ac"
  - subject: "title"
    date: 2004-09-16
    body: "when will be binaries/source be released? Should one has to wait for KDE3.4 or KDE3.3.1?"
    author: "Mathi"
  - subject: "Hacker Programes"
    date: 2004-12-23
    body: "I need hacher programes"
    author: "salah"
  - subject: "Re: Hacker Programes"
    date: 2004-12-23
    body: "No, you need a spell-checker."
    author: "Anonymous"
  - subject: "Re: Hacker Programes"
    date: 2005-10-20
    body: "asl3amo alakom\ni want some hack programes \nand thanx u at all\nthanx my friend\nyours\nsalah"
    author: "salah salah alden ali abd alkader"
  - subject: "Alot of time has passed, what's the status?"
    date: 2005-04-17
    body: "It has been a long time since this announcement, i've thoroughly searched google, but haven't a clue what has happened. What is the status of this project?"
    author: "crobot"
  - subject: "Re: Alot of time has passed, what's the status?"
    date: 2005-04-17
    body: "http://www.kdedevelopers.org/node/view/976"
    author: "Anonymous"
  - subject: "Re: Alot of time has passed, what's the status?"
    date: 2005-04-17
    body: "Thanks for the link...i'm pleased to hear it hasn't died, and will wait excitedly for the first release."
    author: "crobot"
  - subject: "Re: Alot of time has passed, what's the status?"
    date: 2007-03-25
    body: "I would be happy to have a qt ported firefox since I have absolutely no gtk support whatsoever installed and I don't intend to install it anytime soon only for the sake of one application. On the other side to address any of my university issues (like registering for curses or exams) I have to use a site which doesn't work under Konqueror. So... what is the status? Any other browser using qt?"
    author: "iliaarpad"
---
Among the most exciting things to come out of <a href="http://conference2004.kde.org/">aKademy</a>, the recent KDE Community World Summit, is a Qt port of <a href="http://www.mozilla.org/newlayout/">Mozilla's Gecko</a> rendering engine. This will give Gecko the full native look and feel of KDE/Qt, and make it available as a KPart, where it can provide an alternative HTML renderer for <a href="http://www.konqueror.org/">Konqueror</a>.
<!--break-->
<p>
<i>"This is the best of both worlds for KDE"</i> said Lars Knoll of the KHTML
project.  <i>"Integrating Gecko side by side with our existing renderer
opens a lot of doors, without any compromise of the hard work and clean
design that make KHTML what it is."</i></p>
<p>
On the night before the start of the hacking marathon, a conversation
including, among others, Ian Geiser, Lars Knoll, Dirk Mueller, and Zack
Rusin happened onto the topic of integrating Gecko into KDE.  Lars and
Zack jumped into Mozilla's code, to see how feasible such a plan might
be.  Within four days (and before the end of the marathon) the two had
a working port: Gecko running on Qt.  They credited the speed of
implementation to the maturity of the respective technologies and KDE's
component architecture (though the caliber of the hackers certainly
didn't hamper the effort).  In their implementation, Qt is just another
platform for Mozilla, parallel to the drawing and widget layer for
Mozilla's other platforms like GTK, Win32, or MacOS X.  Though the work
is on-going, the team is close to integrating Gecko into Konqueror,
connecting Gecko to the higher level browser machinery in Konqueror
such as KWallet and KCookieJar.</p>
<p>
The Mozilla organization was supportive: <i>"We are delighted to work with
the KDE community, both to extend the reach of Gecko, and to be part of
an effort bringing even greater depth to the KDE desktop.  Making Qt
another platform for Mozilla, and Gecko another option for KDE is a win
for both users and developers"</i> commented Mitchell Baker, President of
the Mozilla Foundation.</p>
<p>
There have been previous starts at this idea in the past.  Trolltech's
QtScape, some initial Corel work, and a Mozilla-based XPart, foundered
from lack of maintenance and little advocacy within the Mozilla
community.  The current team will be full-fledged contributors to the
Mozilla code-base, with KDE people and mozilla.org behind the effort.
The Qt port will live and develop in Mozilla's CVS repository.  The
code to embed the corresponding QWidget will live, naturally, in KDE's
repository.</p>









