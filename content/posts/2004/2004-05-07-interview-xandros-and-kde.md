---
title: "Interview: Xandros and KDE"
date:    2004-05-07
authors:
  - "fmous"
slug:    interview-xandros-and-kde
comments:
  - subject: "Excellent interview"
    date: 2004-05-07
    body: "Really good, Fab :)\n\nI've been really impressed with the quality of work going on here on the dot recently!"
    author: "Tom Chance"
  - subject: "know what they're doing"
    date: 2004-05-07
    body: "I've never really thought much of Xandros, since I found the Corel Linux 1.1 distro to be crap, more or less. But now it looks like they have good direction. I totally agree about the need for an integrated media player. Personally, I prefer my MP3 player to not get muddled with a video player, but seeing my friends use Windows Media Player, its functionality thats kind of expected.\n\nMy question is what role thin clients have in the future of the corporate desktop. It doesn't take too much computer power to have 30 VNC clients, so the old client/server model starts making more sense. And its a niche that Windows can't/won't fill. "
    author: "Ian Monroe"
  - subject: "Re: know what they're doing"
    date: 2004-05-07
    body: "I agree with you about your media player comment.  I use Juk to listen to and organize all of my music (some 8GB of it, mostly CDs of mine I ripped) and Kaffeine to watch movies.  It's kinda funny that they would want to create an app to do this and not just lend a hand to the Kaffeine project."
    author: "Etriaph"
  - subject: "Xandros File Manager = Konqueror?"
    date: 2004-05-07
    body: "Viewing www.grc.com's shields up web site with the Xandros File Manager (XFM), the website clearly identifies the headers of my web browser (XFM) as being none other than Konqueror:\n\n\"Accept: text/html, image/jpeg, image/png, text/*, image/*, */*\nAccept-Language: en\nConnection: Keep-Alive\nHost: grc.com\nReferer: http://grc.com/x/ne.dll?rh1dkyd2\nUser-Agent: Mozilla/5.0 (compatible; Konqueror/3.1; Linux)\nPragma: no-cache\nContent-Length: 31\nContent-Type: application/x-www-form-urlencoded\nAccept-Encoding: x-gzip, x-deflate, gzip, deflate, identity\nCache-control: no-cache\nAccept-Charset: iso-8859-1, utf-8;q=0.5, *;q=0.5\"\n\nThe aforementioned headers demonstrate that XFM appears to be Konqueror or Konqueror-derived. Therefore, it is wholly apparent that XFM >IS< quite possibly a modified version of Konqueror. Hence, it is also possible that the XFM is not GPL compliant as XFM seems to be based on KDE source code and the XFM is not Open Source."
    author: "Rene Lavandier"
  - subject: "Re: Xandros File Manager = Konqueror?"
    date: 2004-05-07
    body: "XFM has not to be GPL compliant if it only builds on LGPL'ed kdelibs (and khtml is a part of it). Why shouldn't they use the default User-Agent string of the used rendering engine?"
    author: "Anonymous"
  - subject: "Not necessarily"
    date: 2004-05-07
    body: "Unless Xandros is paying for and shipping commercial Qt ( which they may bery well be doing, I don't know ), they still need to GPL their app, since QT is GPL. KDELibs being LGPL ( qith q QT exception ) is only usefull to you if you have the non-GPL license from Trolltech.\n"
    author: "Jason Keirstead"
  - subject: "Re: Not necessarily"
    date: 2004-05-07
    body: "They wouldn't be required to ship a commercial Qt along with it assuming they had an appropriate license for the commercial Qt.  See section 5 of the QPL:\n\n5. You may use the original or modified versions of the Software to\n   compile, link and run application programs legally developed by you\n   or by others."
    author: "Scott Wheeler"
  - subject: "Re: Not necessarily"
    date: 2004-05-07
    body: "They don't need to ship a non-GPL Qt, they only need to use non-GPL Qt on their dev boxes, which they said they do above (you obviously missed that bit)."
    author: "ranger"
  - subject: "Re: Not necessarily"
    date: 2004-05-08
    body: "They obviously have a commercial Qt license otherwise they would have to give up sources for XFM.\n\nkdelibs being LGPL does not shield them from the GPL'ness of Qt. They still need Qt to compile and therefore need a commercial Qt license if they want to keep closed source."
    author: "Maynard"
  - subject: "Re: Xandros File Manager = Konqueror?"
    date: 2004-05-07
    body: "IT IS KONQUEROR!\n\nTry this - goto the Settings -> Configure XFM... in the Xandros File Manager.  Now, open up konqui and goto the Settings -> Configure Konqueror... and compare.  I think you'll find this *quite* illuminating.\n\nEither the Xandros people are lying or they don't know what their engineers are up to."
    author: "anon"
  - subject: "Re: Xandros File Manager = Konqueror?"
    date: 2004-05-07
    body: "> Either the Xandros people are lying or they don't know what their engineers are\n> up to.\n\nor you don't have the slightest idea of what you're talking about."
    author: "michele"
  - subject: "Re: Xandros File Manager = Konqueror?"
    date: 2004-05-10
    body: "Well...\nanyone 'igrepped' for konqueror ????\n-Andy\n"
    author: "andy"
  - subject: "Re: Xandros File Manager = Konqueror?"
    date: 2004-05-07
    body: "As somebody who used to work for Xandros, I can assure you that XMF is quite separate from Konqueror. It is true however that it makes heavy use of various KDE components, including KHTML widget for web browsing. This also explains the similarity in the settings dialogue that you noticed: it pulls in the KDE settings plugins associated with KHTML.\n\nHave fun,\nmag"
    author: "mag"
  - subject: "Re: Xandros File Manager = Konqueror?"
    date: 2004-05-07
    body: "Considering that KDE is highly componentized, this makes perfect sense."
    author: "David"
  - subject: "Re: Xandros File Manager = Konqueror?"
    date: 2004-05-07
    body: "xfm uses kparts and is as fully integrated into the kde framework as konqueror is, and thus what you are seeing is stuff from kdebase or kdelibs, NOT konqueror. "
    author: "anon"
  - subject: "Re: Xandros File Manager = Konqueror?"
    date: 2004-05-07
    body: "Nope, it's not.  All you are demonstrating is that both Konqueror and XFM use the same components for web duties:  khtml and related kparts."
    author: "Anonymous"
  - subject: "Re: Xandros File Manager = Konqueror?"
    date: 2004-05-07
    body: "There is konqueror and Browser/View kparts like khtml. Your demostrating XFM supports kparts too."
    author: "koos"
  - subject: "Re: Xandros File Manager = Konqueror?"
    date: 2004-05-07
    body: "Wow, could you know any less about how modular, componentised software works?\n\nThere are several browser shells out there for Windows that make use of the MSHTML components (the heart of Internet Explorer).  All of those are unique, separate, original projects.  And the user-agent for all of those show that they are Internet Explorer.  Yet, none of those projects are Internet Explorer, or make use of anything but the HTML rendering engine of Explorer.\n\nSame thing on MacOS  with Safari.  It uses the KHTML rendering engine and other related web components.  Not sure what the user-agent for Safari is, though.  But, I bet it is very similar to the one for Konqueror, since they both use the same rendering engine.  Yet, Safari is not Konqueror, is it?\n\nSame with XFM.  It's a file manager first, written before Konqueror was around.  Later, they decided to add web browsing features.  What's easier, write your own HTML rendering engine, or use the excellent KHTML part?  So they integrate KHTML into XFM, same as Apple did with Safari.\n\nGeez, next your going to say that all those Gecko-based browsers are all actually Mozilla.  Get a clue."
    author: "Anonymous"
  - subject: "\"We also put relevant bug fixes back into KDE.\""
    date: 2004-05-07
    body: "> The relevant bug fixes always get in unless KDE is in code freeze mode which it was in our last release cycle.\n\nMaybe it's just not obvious, can someone name the Xandros' developers KDE CVS accounts?"
    author: "Anonymous"
  - subject: "Re: \"We also put relevant bug fixes back into KDE.\""
    date: 2004-05-07
    body: "Not sure about current Xandros accounts, but most of the Corel accounts were disabled in June 2003 due to bouncing emails.. http://lists.kde.org/?l=kde-devel&m=105472214422002&w=2"
    author: "anon"
  - subject: "Is Xandros ever planning to restart Qt Mozilla?"
    date: 2004-05-07
    body: "Qt Mozilla was quite active (and up to par with gtk version of Mozilla) when Corel hired John Griggs to work on it full time.. Unfortunatly, when Corel left the Linux world, Qt Mozilla mostly died with it.. Is Xandros ever planning to bring it back to life?"
    author: "anon"
  - subject: "Re: Is Xandros ever planning to restart Qt Mozilla"
    date: 2004-05-08
    body: "No they are not, Mr. John C. Griggs.  Why do you ask?  Are you available, again?"
    author: "anon"
  - subject: "Not all perfect in Xandros"
    date: 2004-05-07
    body: "I just bought the $89.00 version on Xandros, and I must say I'm less than satisfied. I got it for my Sony Vaio PCG-V505X laptop, and it simply won't install there. The installation always crashes when it attempts to start Xwindows. Xandros tech support has been less than helpful, replying after three days to my email, and asking for my display adapter model, then never getting back to me with any answers.\n\nMeanwhile I installed Xandros on the living room machine the kids use. Unlike the other modern Linux distributions I have installed (Debian and Mandrake) the boot manager installation failed to find the Windows partition, and now it looks like I won't be able to boot into Windows without some serious work. I already modified the LILO config file to point to the Windows partition, but this didn't work, probably because the hard disk uses a separate contoller on the PCI bus, not the motherboard IDE bus. \n\nSo my kids are upset because they can't play the Windows games they were using. They tried the games that came with Xandros, but it is not much fun becasue the machine crashes quite frequently when the games are being played. Only good point is that it found the printer attached to my mandrake box in the other room, so I didn't have to fuss with configuring printing, and at least my kids could print out their homework using OpenOffice.\n\nI've used Debian quite a bit, and I realize I could tweak many of these problems out of existance, but isn't not having to do that the whole reason I paid $89 for Xandros? I'm not so happy at this point. \n"
    author: "David Roberts"
  - subject: "Re: Not all perfect in Xandros"
    date: 2004-05-07
    body: "\"Meanwhile I installed Xandros on the living room machine the kids use. Unlike the other modern Linux distributions I have installed (Debian and Mandrake) the boot manager installation failed to find the Windows partition, and now it looks like I won't be able to boot into Windows without some serious work.\"\n\nNot all that familiar with Xandros setup - is there an option to boot inot Linux, or Windows and Linux?\n\n\"I already modified the LILO config file to point to the Windows partition, but this didn't work, probably because the hard disk uses a separate contoller on the PCI bus, not the motherboard IDE bus.\"\n\nThis should still work, but are you just using the PCI card and not the motherboard controller - or are you using both. Why do you need a PCI controller?\n\n\"They tried the games that came with Xandros, but it is not much fun becasue the machine crashes quite frequently when the games are being played.\"\n\nHow does it crash, does it give any messages and what does it crash with? 2D games shouldn't crash at all, and 3D games should be very stable with some good drivers these days."
    author: "David"
  - subject: "Re: Not all perfect in Xandros"
    date: 2004-05-08
    body: "\"Not all that familiar with Xandros setup - is there an option to boot inot Linux, or Windows and Linux?\"\n\nXandros offers almost no options during installation. Just a few different choices including VESA, 256 color, and several different APM and ACPI choices. You just choose the installation. It is supposed to detect Windows and includi it in the boot manager screen. Xandros doesn't let you change any settings. It is supposed to be an installation that only requires a few keystrokes. Actually Mandrake really is like that. The only reason I tried Xandros is because I prefer Debian, especially the .deb package system. I have installed Debian on several computers and it works fine, just there is a lot of downloading and compiling of driver modules needed if your hardware is at all new.  \n\n\"are you just using the PCI card and not the motherboard controller - or are you using both. Why do you need a PCI controller?\"\n\nI am using the IDE controller on the motherboard for two DVD drives and a CD-RW drive. The one on the PCI bus is a Maxtor Ultra ATA 133 controller that I installed because the motherboard doesn't support ATA 133, and also because I have too many drives for the just the two IDE buses on the motherboard. \n\n\"How does it crash, does it give any messages and what does it crash with? 2D games shouldn't crash at all, and 3D games should be very stable with some good drivers these days.\"\n\nWhen it crashes, either the game or the whole system just locks up with no meessages. The games that crash are both 3d and 2d. They include Tux Racer, Chronium, GLtron, and Pingus.\n\nI checked the config file for Xwindows, and the correct driver is installed for the video card, and everything else looks ok about it. "
    author: "David Roberts"
  - subject: "Re: Not all perfect in Xandros"
    date: 2004-05-10
    body: "\"I already modified the LILO config file to point to the Windows partition, but this didn't work, probably because the hard disk uses a separate contoller on the PCI bus, not the motherboard IDE bus. \"\n\nWhat does 'lilo' say when you run it?\n"
    author: "Johan Veenstra"
  - subject: "Re: Not all perfect in Xandros"
    date: 2004-08-04
    body: "I tried to install xandros too and the boot manager screwed me over too. I put in my windows disk and went into recovery and did a  fix MBR and now i can use it again i just deleted the xandros partition and now have sucessfully switched to red hat with no problems as yet. "
    author: "Mark"
  - subject: "Re: Not all perfect in Xandros"
    date: 2006-07-27
    body: "I have the open circulation of xandros (after trying to install linspire many times and gave up) and im so glad i did i found it easy to work with easy to learn on after moving from Micro**** what im most proud of is the fact its running on a pentium 2 266mhz with 128mb ram without a hitch the only real problem i have is installing stuff sort of without the use of XNetwork and i cant use my usb ADSL modem which is no real hassle as i still have my windows machine so my suggestion would be just go get a crappy pc like a P3 550mhz chuck some old SDram in it(if its not provided) an ok vid card etc etc and have your xandros machine running like that\n\n-Mystik"
    author: "Mystik"
  - subject: "Switch user"
    date: 2004-05-07
    body: "I saw on the Xandros web site a \"switch user\" program screenshot.\n\nIs there a KDE app today that makes it easy to switch between X sessions owned by different users?\n\nStephen"
    author: "Stephen"
  - subject: "Re: Switch user"
    date: 2004-05-07
    body: "If you edit the file (this is where it is in gentoo anyway) /usr/kde/3.2/share/config/kdm/Xserver and uncomment one of the lines that specifies additional screens that can be available, you will automagically get a \"start new session\" menu choice in the desktop context menu, the kmenu, and a button when the screen is locked.  When you select this option, it starts kdm on another screen that you can login to.  Then to switch back and forth, you just switch between graphical consoles with ctrl-alt-fx where fx is somewhere between f7 and f10, depending on how many sessions you uncommented in the Xserver file."
    author: "Joe Kowalski"
  - subject: "Re: Switch user"
    date: 2004-05-08
    body: "There is no single KDE app because it also requires patching kdm."
    author: "Anonymous"
  - subject: "XFM's heritage"
    date: 2004-05-07
    body: "As I recall, XFM is actually based on the very first KDE file manager, kfm, which was abandoned when Konqueror was created. I don't believe that kfm was GPL'ed so there's nothing wrong with Xandros basing its XFM on it."
    author: "Joe Luser"
  - subject: "Re: XFM's heritage"
    date: 2004-05-08
    body: "I doubt your recall, under what license other than GPL was kfm?"
    author: "Anonymous"
  - subject: "OT: www.kde-????.org sites are an endless mess"
    date: 2004-05-08
    body: "Cross-posting stuff on the www.kde.look.org and www.kde-apps-org sites is\ngetting more and more of an ugly annoyance.\nCan't folks just decide if their latest creation is a program or a theme/style?\n\nHow \"look\"-related is \"Set as background service\"?\nAnd why does the latest \"kwrite\" release get posted on kde-apps?\nKwrite is included it kde, so every kde-user has it.\nAnd why doesn't the latest \"kdontchangethehostname\" get posted?\n\nIMHO right now both sites cannot be taken seriously anymore because of extensive cross-posting and useless-posting.\n\nEither merge both into www.kde-stuff.org, or apply some policies that restrict\nposting apps on kde-look and optical stuff on kde-apps."
    author: "ac"
  - subject: "Re: OT: www.kde-????.org sites are an endless mess"
    date: 2004-05-08
    body: "They are both the same server."
    author: "Datschge"
  - subject: "Re: OT: www.kde-????.org sites are an endless mess"
    date: 2004-05-08
    body: "I really don't care about physical locations...\n\nFact is, there is much too much duplication between both sites.\n\nkde-look != kde-apps"
    author: "ac"
  - subject: "Re: OT: www.kde-????.org sites are an endless mess"
    date: 2004-05-08
    body: "Fact is that no \"cross-posting\" is happening. These entries are posted once to one of the two categories which appear on both sites. And your comments about someone posting or not posting his application are foolish, everyone is free to decide about it this himself."
    author: "Anonymous"
  - subject: "Re: OT: www.kde-????.org sites are an endless mess"
    date: 2004-05-08
    body: "IMHO you cannot be taken seriously because of posting anonymous and off-topic and on the wrong site and trolling."
    author: "Anonymous"
  - subject: "so sad"
    date: 2004-05-08
    body: "...that they are not present on the European market."
    author: "gerd"
  - subject: "Re: so sad"
    date: 2004-05-08
    body: "They are starting to get into it. "
    author: "CPH"
  - subject: "Re: so sad"
    date: 2004-05-09
    body: "i hope they do so. i'll be the first who buys a fully german translated xandros... :o)"
    author: "reznor"
  - subject: "Insightful interview"
    date: 2004-05-08
    body: "Thank you!"
    author: "Alex"
  - subject: "y-fu"
    date: 2006-01-31
    body: "from what i have read XANDROS is really nice. and because of that i bought it but the only problem is that there is no wi-fi,so i was wondering if in the next couple of months are so will XANDROS release an patch for this problem. but most of all xandros is good i enjoy using and running some of my favorite windows program oon it.\nbut like i said it would be even better if it had wi-fi, seen as though i use a laptop"
    author: "Damion"
---
The <A href="http://www.xandros.com/products/home/desktopdlx/dsk_dlx_screenshots.html">Xandros Desktop OS</A> is known for their intuitive graphical environment that works right out of the box. Their polished desktop product is based on <A href="http://www.kde.org/">KDE</A>. Your humble <A href="http://dot.kde.org/">Dot</A> editor had the privilege to talk to Rick Berenstein, Xandros Chairman and CTO and Ming Poon,Vice President for Software Development about Xandros and their products and the relationship between 
<A href="http://www.xandros.com/">Xandros</A> and the KDE project. Without further due ... enjoy the interview!

<!--break-->
    <style type="text/css">
  
.imgboxrt{
border:1px dotted #000;
float:right;
margin-left:5px;
margin-right:10px;
}

.imgboxlft{
border:1px dotted #000;
float:left;
margin-left:10px;
margin-right:5px;
}
  
  </style>
              
  


<p><IMG src="http://static.kdenews.org/binner/www.xs4all.nl/~leintje/xandros/xandros-konqi.png"  width="300" height="144" align="right" border="0"></p>

<p><strong>Please introduce yourself.</strong></p>
<p>Rick Berenstein, Xandros Chairman and CTO and Ming Poon,Vice 
President for Software Development</p>


<p><strong>How did Xandros start? What's its history?</strong></p>
<p><strong><em>Rick Berenstein:</em></strong> Linux Global Partners, a NYC investment group, originally planned to invest in Linux companies 
which were developing applications or technologies that we felt would be essential pieces of a viable 
alternative to Microsoft Windows. We planned eventually to do a joint venture with one of the existing 
Linux distros and, in fact, started just such talks with Corel. But along the way Corel decided to divest 
itself of its Linux Business Division and we took it over, along with most of the development staff, and 
named the new company Xandros.</p>


<p><strong>The Xandros Desktop OS is based on the Corel LINUX 2 release. This was a Debian
     based distribution. Which current version of the Debian release cycle are you
     using with Xandros Desktop Version 2?</strong></p>

<p><strong><em>Ming Poon:</em></strong> We are now based on what is commonly known as Debian Sarge, which will form the basis of the 
next official Debian release.</p>


<p><strong>Xandros acquired Corel's Linux OS in 2001.
     What part does the Corel heritage play in the creation of the Xandros Desktop? Are there any technologies
	 you retain by that acquisition? Will there be a comeback of <a href="http://www.wordperfect.com/">Corel WordPerfect</a>
	  or <a href="http://www.coreldraw.com/">CorelDraw</a> for Linux?</strong></p>
	  
<p><strong><em>Ming Poon:</em></strong> We have pretty well retained all the original key architects and software engineers from the Corel's 
Linux teams. All the technologies have been carried over and improved upon. New ones have also been created. 
We carried over what we think what are the best parts of the Corel culture and heritage where there is no 
politics in our R&D office and we always see things in the customer's viewpoint whether it is software design 
or any policies that relate to the customers. We have hired many new staff and applied the same cultural 
elements to them. We respect each other and we just have fun everyday in creating the best desktop product 
out there.</p>

<p><strong><em>Rick Berenstein:</em></strong> We hope that Corel will make a comeback for WordPerfect Office for 
Linux as well as CorelDraw. And if they do you can be sure that Xandros will offer them to our users.</p>

<p><strong>What role does KDE play for Xandros?</strong></p>

<p><strong><em>Ming Poon:</em></strong> KDE continues to play a big role in Xandros. Just as 
Corel LINUX, KDE forms the basis of our desktop environment. We enhance and fixes bugs in KDE and 
add in our own KDE/Qt applications to complement KDE to fit our targeted mass desktop market where 
all the users are somewhat familiar with Windows only. Some of the standard KDE configurations and apps 
just don't fit into that market very well so we have to either to replace them totally or make lots of 
changes to them. All of our changes are available from our FTP site in source code form. We also put 
relevant bug fixes back into KDE.</p>


<p><strong>What were your reasons for choosing KDE over other available desktops?</strong></p>
<p><strong><em>Ming Poon:</em></strong> When we first started our Linux desktop effort back in 1997, we actually implemented a 
100% pure Java solution called Cabot which was running on the StrongARM processor on a little NC 
(Network Computer) called the NetWinder. It had pretty well all the key functionalities of KDE or 
any other desktop environment including toys like an Internet news ticker in the task bar. It is 
probably something more close to a true Java desktop than what Sun's Java Desktop is today. It was 
really 100% Java.</p>
 
<p>At the time, the JVM in Linux was based on green threads and the performance was not as good as the 
JVM in Windows. Combined with running on a relatively slower 200 MHz StrongARM processor with no 
math co-processor, it was just not as slick as running our Java desktop under the JVM in Windows or 
as any native Linux desktop implementation. We had to make some tough choices in mid 1998 to drop 
the Java implementation and switch to something native. KDE was the pretty well the only good choice 
at the time since it had most of the desktop environment features we were looking for.</p>
 
<p>When we started the Corel LINUX project back in March of 1999, GNOME/GTK was there so we actually reviewed 
both GNOME and KDE to make sure we used the right desktop environment to start. We had a very short and 
aggressive cycle and the simplicity of KDE/Qt won again. Looking back, we never regretted about not 
supporting GNOME at all. Most of us came from OS/2 PM or Windows GUI development or freshly from a new 
object oriented technology called Java back then. MFC was a big life saver when it came out in Windows in 
developing GUI apps. Java was even better where everything was simple and made perfect sense. There was 
no way any of us would like to go back in time and program in something (GNOME/GTK) that was even more 
awkward than programming in pre-MFC days where we had to deal with the Win32 C API only. KDE/Qt was just 
like Java where everything (well most of the time anyway) made sense.</p>

<p>We have also seen a lot of poor arguments made on Qt where it cost money if you want to develop a commercial 
closed source application. Usually people argued that the $500 per developer license fee was just as much 
as a developer's salary in some third world countries. That may be true but they don't really take into 
account the months of headaches and development time they will save by using Qt every year. That alone 
is probably worth the $500. KDE/Qt is simple and is designed for the desktop. We like it and we have no 
regrets in supporting KDE at all.</p>


<p><strong>What do you like most about KDE?</strong></p>
<p><strong><em>Ming Poon:</em></strong> The thing I like most about KDE has always been its simplicity in design and the really 
passionate developers that have been supporting it for years.</p>

<p><strong><em>Rick Berenstein:</em></strong> I think that the KDE developers are not only dedicated 
but continue to work on terrific new features and new capabilites within the framework of a wide KDE 
community. We are looking forward to implementing many of these changes in the Xandros suite of 
products.</p>


<p><strong>What do you like least about KDE?</strong></p>

<p><strong><em>Ming Poon:</em></strong> I can not think of anything big really. Perhaps the Control Center could use an overhaul some day. 
It is a bit awkward to use from time to time because you have to save the settings before going to the next 
panel. It would really be great if the CD Player supported playback by digital ripping as the default. Most 
new systems, especially laptops, don't ship with the cable connecting the CD-ROM drive to the sound card any 
more so it is an obvious area to address in the desktop market.</p>


<p><strong>Could you tell us somewhat more about the work that Xandros has done to
     integrate KDE in their products? Has this been a difficult process?</strong></p>
     
     
<!--IMAGE BOX RIGHT -->
<div align="center" class="imgboxrt" style="width:340px;">
<p><a href="http://static.kdenews.org/binner/www.xs4all.nl/~leintje/xandros/shortcut_wizard1.jpg"><img src="http://static.kdenews.org/binner/www.xs4all.nl/~leintje/xandros/shortcut_wizard1-small.jpg" alt="Xandros Desktop and Shortcut Wizard" /></a><br />
Xandros desktop, creating a shortcut ...</p>
</div>
<!-- IMAGE BOX RIGHT-->     


     

<p><strong><em>Ming Poon:</em></strong> We actually do quite a big surgery on KDE in every release. We changed things from how the File 
Open dialog looks and work across network environments to replacing the standard KDE file manager 
(Konqueror) completely. Most people think that Xandros File Manager (XFM) is a highly tweaked version 
of Konqueror. The truth is that it is not Konqueror at all. XFM was started in the KDE 1.x days before 
Konqueror even existed. Konqueror is great and is well suited for people coming from a UNIX environment. 
For where our product is going, we need a simple, easy to use and yet powerful file manager that is catered 
more towards the people coming from a Windows environment. I believe we have achieved this objective 
quite well. In the new XFM that ships with Xandros Desktop V2, it even has an integrated CD Writer applet 
that is even simpler to use and yet more powerful than the one that comes with XP. Our users just love it.</p>
 
<p>There are many other things we modify that really touch on every component of KDE from the low level 
to the look and feel. We have done this kind of surgery for so many years now that we can do it while 
we are almost asleep.</p>


<p><strong>Does Xandros make any effort to get these merged upstream.  If not, why not?</strong></p>

<p><strong><em>Ming Poon:</em></strong> We try our best. The relevant bug fixes always get in unless KDE is in code freeze mode which 
it was in our last release cycle. For a lot of other stuff especially the ones that have to do with the 
UI, those are really tough calls. Some KDE people don't like our changes and we can't live with the 
existing look and feel in our product direction so we just leave the source code out there for people 
to use or to merge if they ever decide to. That's what a GPL open source project is all about. You can 
make whatever changes to it as long as you make the changes available in source code form.
</p>

<p><strong>How does Xandros support the KDE community?</strong></p>

<p><strong><em>Ming Poon:</em></strong> We do sponsor some of the developers traveling to conferences to talk and promote KDE. We 
always promote KDE in every tradeshow we are in and in any business meetings we have had with all 
the big software or hardware vendors out there. Some of the existing KDE features also came from 
our contributions in the past.</p>


<!--IMAGE BOX RIGHT -->
<div align="center" class="imgboxrt" style="width:340px;">
<p><a href="http://static.kdenews.org/binner/www.xs4all.nl/~leintje/xandros/shortcut_wizard2.jpg"><img src="http://static.kdenews.org/binner/www.xs4all.nl/~leintje/xandros/shortcut_wizard2-small.jpg" alt="Shortcut Wizard" /></a><br />
Shortcut Wizard</p>
</div>
<!-- IMAGE BOX RIGHT-->  

<p><strong>It seems that the KDE project is perhaps more of a meritocracy than other projects. How does
     Xandros perceive the KDE project and how does this reflect in accepting patches from Xandros?</strong></p>
<p><strong><em>Rick Berenstein:</em></strong> We also see the KDE project as more of a meritocracy and we feel that this shows in the 
level of programming that comes out of the KDE community.</p>


<p><strong>What do you think of <a href="http://dot.kde.org/1073599985/">the</a>
<a href="http://dot.kde.org/1073668213/">latest</a> <a href="http://dot.kde.org/1073557624/">efforts</a>
to integrate several technologies into the KDE desktop to make non-KDE applications feel more comfortable
on the KDE desktop?<br>
[<a href="http://ktown.kde.org/~danimo/bmp_kde_diropen_dlg.png">screenshot 1</a>]
[<a href="http://openoffice-et.sourceforge.net/pildid/OOo-1.1/ooo-plastik/ooo-plastik1.png">screenshot 2</a>]</strong></p>

<p><strong><em>Rick Berenstein:</em></strong> I think this is really one of the best things that has 
happened for the desktop end user.</p>



<p><strong>What kind of users is Xandros targetting?</strong></p>
<p><strong><em>Rick Berenstein:</em></strong> We originally started by addressing the needs of the most dependent user, the home user. 
The reception this got in the press ("It just works." "The King of the Linux Desktop") convinced us 
that our philosophy, based as Ming mentioned above on our belief that most of our users would be 
coming from a Windows environment,was correct. Our new products, the Business Desktop,the Xandros 
Desktop Management Server, and our upcoming server line will address the enterprise customer as 
well as the larger scale customer needs in government and education.</p>


<!--IMAGE BOX LEFT-->
<div align="center" class="imgboxlft" style="width:340px;">
<p><a href="http://static.kdenews.org/binner/www.xs4all.nl/~leintje/xandros/xfm.jpg"><img src="http://static.kdenews.org/binner/www.xs4all.nl/~leintje/xandros/xfm-small.jpg" alt="XFM filemanager" /></a><br />
Xandros File Manager</p>
</div>
<!--/IMAGE BOX LEFT-->



<p><strong>In the Xandros Desktop OS there is an application called "Xandros File Manager"[XFM].
     This application is regarded by many as a killer feature. Can you tell us a bit more about
     it and the technologies it support?</strong></p>
     
     
<p><strong><em>Ming Poon:</em></strong> For a detailed discussion you can view this article 
<a href="http://consultingtimes.com/articles/xandros/filemanager/filemanager.html">here</a>. 
In V2, there is also a built-in CD Writer plugin application where you can just drag and drop files 
to burn to CD-R or CD-RW media. Alternatively, you can also create data and music CD projects to back 
up your data, create your favorite music CDs or burn your MP3 CDs. It is just a few mouse clicks and 
everyone who uses it has been more than impressed by its simplicity and yet its full capabilities.</p>
 
<p>There is also support for on-the-fly data archiving and de-archiving using the popular zip 
or tar.gz formats.</p>





<p><strong>How much the underlying system is hidden to the end-user?</strong></p>
<p>Most of our users have not used Linux before so that tells you how much hidden it is.</p>


<p><strong>How do users that switch from Microsoft Windows to Xandros perceive that switch?</strong></p>
<p><strong><em>Ming Poon:</em></strong> Based on all the feedback we have had, every one of them is more than happy. They have a more stable 
OS, free of all the Windows viruses out there, and can continue to use their favorite Windows application on Xandros 
Desktop OS if they choose to. Some users' family members have not even noticed that it is not Windows until they are 
told. It is quite a phenomenon..</p>

<p><strong><em>Rick Berenstein:</em></strong> In one corporate roll-out (150 desktops) at the end of the first shift, two operators asked what had happened to 
their Windows wallpaper (we did not migrate it over). Other than that, none of the 150 operators ever knew that they no 
longer had Windows on their machines. That for me is the proof of the pudding, former Windows users can just sit down and 
go back to work.</p>


<p><strong><a href="http://www.xandros.com/products/business/products_business.html">On the Xandros website</a>
it mentions "Xandros Business Desktop" a premier alternative to Windows desktops
for small and large organizations. Can you tell us some more about this product?</strong></p>
<p><strong><em>Rick Berenstein:</em></strong> Well, the Xandros Business Desktop is the first in that line of solutions. 
It offers enterprises all the features 
of Xandros Deluxe and also adds automatic Windows domain authentication and total support for Active Directory servers. 
This way it can just be added in to a heterogeneous network environment and start working right away out of the box.</p>


<p><strong>Do you see a market for commercial closed-source desktop applications on
     Linux? Do you think the KDE API provides a viable application framework for
     such software?</strong></p>
<p><strong><em>Rick Berenstein:</em></strong> I see the potential for both in the future. 
The KDE API certainly provides a framework for such software. I think 
that over the next few years this will become a major market in the Linux world.</p>


<p><strong>What are the critical applications that enterprises need and does
     Xandros provide these applications?</strong></p>
<p><strong><em>Rick Berenstein:</em></strong> I think the Xandros Desktop (in all of it's versions) 
provides the basic applications that people need on a 
day-today basis. There is a full office suite (and several other good ones available for free download), a web browser, 
and a variety of best-of-breed applets that make working easier. So yes, I believe we provide the critical applications 
with out of the box.</p>


<p><strong>What are the typical cost savings when using Xandros in an enterprise environment?</strong></p>
<p><strong><em>Rick Berenstein:</em></strong> In an enterprise environment the cost savings go from huge to astronomical because many enterprises 
who are thinking of upgrading to XP also have to count in the cost of hardware upgrades, the XP license, 
the Palladium licensing scheme and so on. And Xandros not only provides a great OS but also the critical 
applications so there is no need to add third-party application costs either.</p>


<p><strong>What would motivate enterprises to move to Linux on the desktop?</strong></p>
<p><strong><em>Rick Berenstein:</em></strong> Many things. Cost of course, but also knowing that they have their vital information on a more 
stable and more secure operating system. And a licensing system that does not bind them to forced 
future software and hardware upgrades. There are many reasons for enterprises to move to Linux and 
as each day passes more and more enterprises are discovering this. At last November's Desktop 
Linux Consortium gathering, IBM gave a speech titled "Now Is The Time For Linux On The Desktop" 
and they talked about how many pilots are going on and how, after the first Fortune 500 company 
announces it is switching to Linux the rest of the enterprise community will follow in a deluge 
of conversions.</p>


<p><strong>What do you consider the biggest barrier when a company decides to use Linux on
     their desktop?</strong></p>
<p><strong><em>Rick Berenstein:</em></strong> In the past it might have been concerns about whether they would be able to run the applications they 
were accustomed to or whether they would have access to all of their data (Word documents and Excel spreadsheets, but 
that is no longer really an issue. Today it is mostly getting over the FUD coming out of Redmond and just giving Linux a whirl.</p>


<p><strong>Some IT managers claim that an enterprise desktop migration from Windows
     to Linux would be very difficult. What do you think on that matter?</strong></p>
<p><strong><em>Rick Berenstein:</em></strong> As I mentioned above, I don't think that this would be the case today. Two years ago, 
yes, but not today. And with the Xandros Business Desktop and the Xandros Desktop Management Server for 
wide area deployment and remote management, working in an enterprise environment will be easier with 
Xandros than it is today with Windows.</p>


<p><strong>Reality today is that more and more mixed computing environments are found in organizations.
     How will these organizations benefit from using Xandros in these mixed computing environments?</strong></p>
     
<p><strong><em>Rick Berenstein:</em></strong> In a mixed environment, Xandros will provide a level of administrative control and firewall protection 
which is not only powerful but a model of simplicity as well. Again, enterprises will save money and time 
(which also equals money). At the DLC gathering Nat Friedman said that the cost of viruses in the Windows world 
from January 2003, until November 2003 was on the order of 130 billion dollars in terms of lost time and data. 
They would have saved a bundle with Xandros on the desktop.</p>


<p><strong>What is your take on the desktop computing market for 2004 and where do
     you see Xandros going?</strong></p>
<p><strong><em>Rick Berenstein:</em></strong> 2004 will definitely be the year of the Linux desktop - at least in terms of its real acceptance and 
growth in the mainstream. The CAGR (<a href="http://www.investopedia.com/terms/c/cagr.asp">Compound Annual Growth Rate</a>) 
for Linux on the desktop is 44%, higher than 
on the server side (33%) so all the figures suggest that by the end of 2007, on a global basis, Linux will be 
on about 45% of the desktops, which, if you think about it, is really amazing. Between now and 2008 more than 
600 million computer will be built and the number running Linux will almost equal all the PCs currently in operation.</p>

<p>We believe that Xandros, by offering a complete end-to-end solution for home users, enterprises, and government 
and education as well as by innovation products which we will introduce in the future, will remain a major and favored 
company in the Linux world.</p>


<p><strong>Where would you like to see the future of KDE go,
     and what new features would you like to see in future releases?</strong></p>
<p><strong><em>Ming Poon:</em></strong> An all-in-one media player will be on the top of our list so that the end users won't be 
stumbling upon different media players that have very different looks and feel and may even have overlapping. 
I think KDE needs to focus on finishing off some of these apps in a more polished manner and making things more 
consistent across all the apps as opposed to inventing more new features (at least for the next release). Having 
a consistent way or policy on how each app handles File Open dialog errors would be a very good starting point.</p>

<p><strong><em>Rick Berenstein:</em></strong> I personally see a very bright future for KDE and the KDE desktop. Of course polishing always needs to be done, 
but I believe that the underlying structure as well as the Qt widget set and programming environment allow KDE to 
flexibility to add any new features that the future will demand. </p>


<p><hr align="center" width="400px"></p>







