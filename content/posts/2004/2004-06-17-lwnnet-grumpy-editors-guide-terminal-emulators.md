---
title: "LWN.net: The Grumpy Editor's Guide to Terminal Emulators"
date:    2004-06-17
authors:
  - "binner"
slug:    lwnnet-grumpy-editors-guide-terminal-emulators
comments:
  - subject: "Transparency ???"
    date: 2004-06-17
    body: "I'm sorry but I don't see any kind of transparency on the screenshot !\nWhere is it ?"
    author: "Pinaraf"
  - subject: "Re: Transparency ???"
    date: 2004-06-17
    body: "It's the black konsole window. You need to change your gamma to an insanely high value to see it:  $ xgamma -gamma 3  ... or something. Or make the picture brighter with kuickshow (b-key) or gimp. Gwenview doesn't support that. :-("
    author: "Melchior FRANZ"
  - subject: "Re: Transparency ???"
    date: 2004-06-17
    body: "Makes me wonder what CRTs/TFTs you have, it's visible on my TFT even on the lowest brightness setting."
    author: "Anonymous"
  - subject: "Re: Transparency ???"
    date: 2004-06-17
    body: "Thanks, you were true : gamma settings were wrong so I have to use kuickshow..."
    author: "Pinaraf"
  - subject: "Re: Transparency ???"
    date: 2004-06-17
    body: "http://www.werbefoto.at/d_base/calibration.htm\n\n;-)"
    author: "Thorsten Schnebeck"
  - subject: "X"
    date: 2004-06-17
    body: "I know this is slightly off-topic, but since freedesktop.org and the X Server where brought up in this article, I enjoy seeing the faster and more flexible progress the open source world will witness thanks the new fork of X. \n\nIs this transparency based on the current patch for transparency in some kind of new X version? ala: http://freedesktop.org/~keithp/screenshots ?\n\nX ( http://www.mediaapplicationserver.net ) seems to have ongoing projects, like for example \"Media Application Server - MAS\" ( http://www.mediaapplicationserver.net ). Anything known about KDE adopting MAS or any of the other projects going on with X that KDE might decide to adopt?"
    author: "ac"
  - subject: "Re: X"
    date: 2004-06-17
    body: "> Is this transparency based on the current patch for transparency in some kind of new X version? ala: http://freedesktop.org/~keithp/screenshots ?\n\nExactly, you will find your link on the page linked in above story."
    author: "Anonymous"
  - subject: "Looks great!"
    date: 2004-06-17
    body: "The screenshot looks amazing. I hope the freedesktop xserver is making good progress! Does anyoneknow where I can get the background image shown in the screenshot?"
    author: "Michael Thaler"
  - subject: "Re: Looks great!"
    date: 2004-06-17
    body: "Heck, I'd like a quick rundown of themes and settings used there.  It's not only beautiful, it would be great when I go back and forth between OSX and KDE."
    author: "Evan \"JabberWokky\" E."
  - subject: "*Which* X server?"
    date: 2004-06-17
    body: "freedesktop.org hosts two different X server projects, one called Kdrive (a small testbed X server that Keith used to develop Xdamage, Xfixes, etc. afaik) and the X.org X server tree, which is very similar to XFree86 4.4, but now has those extensions as well, I think.  Which one does this article refer to?  It would be nice if it were updated to prevent such confusion in the future.\n\nJesse"
    author: "Jesse Barnes"
  - subject: "Re: *Which* X server?"
    date: 2004-06-17
    body: "they mean the x server based on kdrive"
    author: "kdrive"
  - subject: "Re: *Which* X server?"
    date: 2004-06-17
    body: "yeah, that's what I figured, but given past confusion about what's hosted at freedesktop.org, and what sort of work people are doing on various X servers, I thought it was important to be specific.\n\nJesse"
    author: "Jesse Barnes"
  - subject: "Re: *Which* X server?"
    date: 2004-06-18
    body: "Well, the distinction may become less important soon.  There is an effort to port KDrive's new extensions (the ones which make possible transparent windows and stuff) to the X Server that is based on the old XFree86.  Once that happens it won't matter which server you're running."
    author: "Spy Hunter"
  - subject: "Re: *Which* X server?"
    date: 2004-06-17
    body: "Well, KDrive is the old name of what is now called X server \nhttp://xserver.freedesktop.org/\n\nThen you have the old X11 server of X.org\n"
    author: "oliv"
  - subject: "Re: *Which* X server?"
    date: 2004-06-17
    body: "> Well, KDrive is the old name of what is now called X server \n> http://xserver.freedesktop.org/\n>\n> Then you have the old X11 server of X.org\n\nIncorrect.  The CVS tree has both in them.  Also see the topic of #freedesktop at Freenode.net.\n\nJesse"
    author: "Jesse Barnes"
  - subject: "Re: *Which* X server?"
    date: 2004-06-17
    body: "> Which one does this article refer to?\n\nThe one which is called \"X Server\" and to which the article links to? :-)"
    author: "Anonymous"
  - subject: "can't make it work with iptraf "
    date: 2004-06-17
    body: "I don't use konsole cause when I try to use iptraf or \"make menuconfig\" I get that \"This program requires a screen size of at least 80 columns by 24 lines\nPlease resize your window\" so I open an xterm and it just works. is there a way to make konsole work with this kind of app?\n\nthanx in advance"
    author: "Pat"
  - subject: "Re: can't make it work with iptraf "
    date: 2004-06-17
    body: "resize it!\n\nSettings -> Size"
    author: "stripe4"
  - subject: "Re: can't make it work with iptraf "
    date: 2004-06-17
    body: "actually it was a font problem, it was on \"huge\" so i changed it to normal and it works now :)"
    author: "Pat"
  - subject: "monitor for string"
    date: 2004-06-17
    body: "Sigh. Terminal feature i've been waiting for a long time. "
    author: "foo"
  - subject: "Re: monitor for string"
    date: 2004-06-18
    body: "With only one person voting for this feature it will be never considered."
    author: "Anonymous"
  - subject: "Confusing"
    date: 2004-06-17
    body: "SO why do they call it all Xserver? dont they think thats confusing? \n\ncan someone tell us if its the server from x.org / the patched one has wich  tranparency??"
    author: "Willo"
  - subject: "Re: Confusing"
    date: 2004-06-17
    body: "> can someone tell us if its the server from x.org / the patched one has wich tranparency??\n\nIt's the goal to add the fdo extensions, allowing transparency and other stuff, to X.org within the next one, two major releases."
    author: "Anonymous"
  - subject: "new kde3.3 screenshot"
    date: 2004-06-17
    body: "> And as a teaser for upcoming KDE 3.3, look at this screenshot of Konsole showing\n> real transparency while running under freedesktop.org's X Server. \n\nThis screenshot of KDE3.3 looks very close to OS X Panther. It's nice!"
    author: "Anton Velev"
  - subject: "Please tell us where you got the theme components!"
    date: 2004-06-17
    body: "Hi\n\nWhich widget theme is that?\nHow do I get drop shadows behind window frames like that - IIRC there was support for that in kde 3.2 betas and then it disappeared by final release?\nIs that ksmoothdock at the bottom?\n\nThanks"
    author: "timlinux"
  - subject: "Re: Please tell us where you got the theme components!"
    date: 2004-06-17
    body: "The theme is plastik, the windec is baghir 0.4pre5, the icon theme is cristal real svg 0.5 (only for kde 3.2 and better) ---> http://themes.freshmeat.net/projects/crystalrealsvg/\ndropshadows come from fdo xserver...they are native, you can have a similar effect using a patch for kde 3.2 and cvs..you can find it in kde-look.org\n:)"
    author: "Giovanni"
  - subject: "Re: Please tell us where you got the theme components!"
    date: 2004-06-17
    body: "Oh I forgot to say that the one at the bottom is not ksmoothdock..I still prefer to use the old, tested, rick solid, useful, but also nice-looking kicker! ;)"
    author: "Giovanni"
  - subject: "Re: Please tell us where you got the theme compone"
    date: 2004-06-17
    body: "And the window decorations?"
    author: "timlinux"
  - subject: "Re: Please tell us where you got the theme compone"
    date: 2004-06-17
    body: "baghira 0.4pre5"
    author: "Giovanni"
  - subject: "Re: Please tell us where you got the theme compone"
    date: 2004-06-18
    body: "And which konsole? Does the current CVS version actually have support for render/whatever-extenstions or is this just a proof-of-concept hack?"
    author: "Fred Sch\u00e4ttgen"
  - subject: "Re: Please tell us where you got the theme compone"
    date: 2004-06-18
    body: "Konsole (and necessary support for this transparency in kdelibs) of KDE 3.3 Alpha. It's really in CVS and will be in KDE 3.3."
    author: "Anonymous"
  - subject: "Re: Please tell us where you got the theme components!"
    date: 2004-06-18
    body: "Thanks, you forgot to answer the wallpaper question some postings higher. :-)"
    author: "Anonymous"
  - subject: "Re: Please tell us where you got the theme components!"
    date: 2004-06-18
    body: "errr..I don't remember where I took it :)\nmaybe in kde-look?"
    author: "Giovanni"
  - subject: "Re: Please tell us where you got the theme compone"
    date: 2004-06-18
    body: "Any support for drop shadows before now was only a hack, and would behave badly when moving windows or when shadowed windows changed their contents underneath the shadow.  Drop shadows and the like require extensions to the X server to work correctly.  Those extensions are still in development and are not being shipped with any current distribution that I know of.  They will come eventually, though."
    author: "Spy Hunter"
  - subject: "good news for linux ... and KDE!"
    date: 2004-06-18
    body: "the city of Bergen in Norway decided to switch from windows to linux and according to that zdnet article (  http://zdnet.com.com/2100-1104_2-5238146.html?tag=zdfd.newsfeed ) , they choose Suse over Red Hat because:\n \"We consider SuSE being ahead of Red Hat technically,\" with earlier 64-bit versions, better support for multiple languages and a focus on the KDE graphical interface. \"\n\nwhich is good news for linux and KDE :)"
    author: "Pat"
  - subject: "multi-gnome-terminal is still my choice"
    date: 2004-06-18
    body: "It surpasses gnome-terminal and konsole on a feature no other ever tried to implements :\nyou can split tab vertically/horizontally, as much as you need, and still have multiple tab. It's damn usefull when you need to have multiple terminals visible.\nSure, you could just fire a few \"New window\" up, but :\n1) you have to resize/manage them to get the desired layout, with maximized visible part\n2) This is as many processes forked (multi-gnome-terminal only forks the shells)\n3) if you want to use 2-3 of these layouts, you have to waste as many workspace (whereas in mgt, all this goes to tab layout).\n\nSure, it's a kinda abandoned project (it's gtk-1, and there's no update since April 2003), and it doesn't have as much eye candy as Konsole, but that would be so nice if any of the current fashioned terminal emulator would pick up those features..."
    author: "Seb C."
  - subject: "Just not full of screen goodness..."
    date: 2004-06-18
    body: "My one wish for Konqueror, the one thing that could possibly make it more delicious....  integration with GNU Screen."
    author: "anon"
  - subject: "Missing the point?"
    date: 2004-06-19
    body: "The article may have praised konsole for some innovative features, but one of the strongest concerns voiced by the author was the drive for silly transparent backgrounds and images etc. which do nothing to help the user read the text. So you follow on from reporting on the praise with a screenshot of pointless eye candy..."
    author: "Jon"
  - subject: "Re: Missing the point?"
    date: 2005-04-27
    body: "helow nao entendo nada de ingles do que vcs estao falando ???\nsou do brasil  brasil   hahahhah"
    author: "junior.kraus"
---
As part of its <a href="http://lwn.net/Articles/grumpy-editor/">Grumpy Editor series</a>, <a href="http://lwn.net/">LWN.net</a> looks at <a href="http://lwn.net/Articles/88161/">current terminal emulators</a>. Not surprisingly they also mention KDE's <a href="http://konsole.kde.org/">Konsole</a> which seems to win in terms of features. And as a teaser for <a href="http://developer.kde.org/development-versions/kde-3.3-release-plan.html">upcoming KDE 3.3</a>, look at <a href="http://kde-look.org/content/preview.php?preview=1&id=12940&file1=12940-1.jpg&file2=&file3=&name=kde+3.3alpha+%2Bfd.o">this screenshot</a> of Konsole showing real transparency while running under <a href="http://freedesktop.org/">freedesktop.org</a>'s <a href="http://freedesktop.org/Software/xserver">X Server</a>.

<!--break-->
