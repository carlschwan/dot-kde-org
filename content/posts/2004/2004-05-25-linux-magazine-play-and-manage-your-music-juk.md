---
title: "Linux Magazine: Play and Manage your Music with JuK"
date:    2004-05-25
authors:
  - "dmolkentin"
slug:    linux-magazine-play-and-manage-your-music-juk
comments:
  - subject: "Automatic playlists"
    date: 2004-05-24
    body: "Is it just me or is there no way to edit \"automatic playlist\" (from the GUI)?\nWhen you create them, you can add several keywords to search for, but once you click okay, there's now way to change that?\n\nAnyway, I really like juk, too bad it's a little slow with my usb disc (slow usb hub, not juk's fault).\nDisabling startup scanning for certain folders would be a welcome feature.\n(The rhythmbox guys were planning to implement/have implemented this)\n"
    author: "dwt"
  - subject: "Re: Automatic playlists"
    date: 2004-05-25
    body: "You mean search playlists? Yes, in CVS HEAD it is possible to edit those and I think it'll be available in next KDE release..."
    author: "tpr"
  - subject: "Re: Automatic playlists"
    date: 2004-05-25
    body: "> Is it just me or is there no way to edit \"automatic playlist\" (from the GUI)?\n\nAs was mentioned earlier -- this is done now.\n\n> Disabling startup scanning for certain folders would be a welcome feature.\n\nWhy not just remove it from your list of music folders?  All that list is really used for is \"scan these folders on startup\"."
    author: "Scott Wheeler"
  - subject: "Re: Automatic playlists"
    date: 2004-05-25
    body: "Hi\n\n\nThe disable feature would be useful to toggling certain folders on and off. if you have a folder somewhere deep and have to add and remove them it can be a nuisance"
    author: "getit"
  - subject: "upcoming playlist?"
    date: 2004-05-25
    body: "I've loved Juk since I discovered it a few weeks ago. The only thing it's missing (apart from support for .mpc) is an upcoming playlist. I imagine this would be an optional interface extra, that would let people drag songs they want played next onto the upcoming list. This saves the hassle of creating a regular playlist which is, imo, unsuited for something that's only ever going to be played once!\n\nThis would be great at a party. You could set a \"double click\" to \"add to upcoming\" and then you just double click the song you want and it goes onto the list, a seperate window could show what's coming next with half the screen and dissapears when someone moves the mouse.... or soemthing like that. This combined with Juk's clean, clear interface, ANYONE would be to sit down and use it without any instructions.\n\nWhat do other people think?\n\nBTW, thanks for a great app, Scott!"
    author: "Hemfor"
  - subject: "Re: upcoming playlist?"
    date: 2004-05-25
    body: "this would be a nice improvement!\n\nWhat about using Xmms Plugins for fading and such stuff?\n\nfading + upcoming playlist and everybody thinks you have a dj in a box ;)"
    author: "dani"
  - subject: "fading (was:Re: upcoming playlist?)"
    date: 2004-05-25
    body: "Bug 68539: crossfade between played tracks\nhttp://bugs.kde.org/show_bug.cgi?id=68539"
    author: "testerus"
  - subject: "Re: upcoming playlist?"
    date: 2004-05-25
    body: "Bug 63260: \"upcoming\" playlist\nhttp://bugs.kde.org/show_bug.cgi?id=63260"
    author: "testerus"
  - subject: "Two words"
    date: 2004-05-25
    body: "Album shuffle. (There's a bug for this in bugs.kde.org... I also tried to look at the source and see if I could do something, but gave up fairly quickly. :-))"
    author: "AC"
  - subject: "juk"
    date: 2004-05-25
    body: "is it possible to add a folder of music without adding it's subfolders?"
    author: "me"
  - subject: "Playlist / directory connection?"
    date: 2004-05-25
    body: "JuK is really great, I have only one question:\n\nI added a directory like \"Rock\" as a playlist. But if I add a\nnew song to the Rock directory, the playlist is not updated\nautomatically. Is there a way to do this?\n\nThanks,\nOliver"
    author: "oli61"
  - subject: "Re: Playlist / directory connection?"
    date: 2004-05-25
    body: "Yes -- but it's a bit of a hack at the moment since \"Folder Playlists\" aren't really what one would assume they are; this will be fixed in KDE 3.3.\n\nJust create a Search Playlist (aka \"Advanced Search\") based on the path name and it will be automatically updated."
    author: "Scott Wheeler"
  - subject: "Re: Playlist / directory connection?"
    date: 2004-05-25
    body: "Thanks, I will try it."
    author: "oli61"
  - subject: "shoutcast/icecast?"
    date: 2004-05-25
    body: "Does JuK still lack support for shoutcast/icecast?"
    author: "Marcus Sundman"
  - subject: "nasty jukiness"
    date: 2004-05-26
    body: "I accidentally left my JuK running last night (the speakers were off) and when I just got in there were 22 Krash dialogs on my screen. artsd has a nasty habit of crashing on some mp3s and Juk got into a loop. It's a testament to the durability that JuK can just keep playing anyway. (The GStreamer plugin has even worse crashing problems and puts 3 second gaps between each mp3 to boot).\n\nStill, great program. Love the musicbrainz stuff. I just wish there was some way to give back to the server."
    author: "c"
  - subject: "wxMusik vs JuK, not."
    date: 2004-05-27
    body: "Hey there, JuK is a really nice software.\nBut it really lacks important features that the app wxMusik brings.\nIts really sad to see both of them having nice features not implemented in the other.\nWhy not merge the efforts to do the best open source mp3 collection manager?\n\nI don't get the point when many similar open source softwares exists and get maintained over the time.\nI understand the choice and the diversity is important.\nBut in a open source \"world\" (community), if the feature is not available or something is bugged, It is POSSIBLE to EASILY REPORT IT or WRITE A PATCH FOR IT.\nThen the software is better and still open for more improvement. No wasted energy in starting a whole new project because you CAN help for the existing one.\nWhen the software is closed, I understand its temptating to start on a new project, but it is not the case here.\nNo need for choice when the best of the these choices can be integrated in the same thing.\n\nIsnt that logic?\n\nI'd like to see my idea critized because I really wonder why there is many open projects that are similar and who still get developped apart one from each other.\n\n-Pierre"
    author: "Pierre Delagrave"
  - subject: "Re: wxMusik vs JuK, not."
    date: 2004-05-27
    body: "Ummm, it might have something to do with the fact that wxMusik uses the WX windows toolkit and JUk uses KDE.... you can't merge them, you scrap one."
    author: "Hemfor"
  - subject: "Re: wxMusik vs JuK, not."
    date: 2004-05-27
    body: "It has always been my assumption that wxMusic is primarily targetting Windows -- this is based on the fact that I've never even managed to get it built on Linux.  It doesn't use standard configure tools; the makefiles have the authors home directory path hard coded in them; it's missing required files, etc.\n\nIt's also using a proprietary, binary only sound backend, which I find to be unacceptable.\n\nBut you're going after the age old Open Source debate here -- why do people do it at all?  You assume that everyone has the same goals for software that you do, but this really isn't the case at all.  There is no single reason that people develop OSS software; the closest you really can get to an answer is \"because they want to\" (though even that isn't always true).  As disappointing as this may be OSS authors generally aren't working on the stuff primarily for your amusement.  ;-)  People work on projects because they like their way of doing things or think that there are things that they can do better or just differently an so on.  That's the essence of the \"bazaar\" in Open Source development.  Lots of things happen in parallel and they gradually evolve towards better software.\n\nAnd finally you're kind of asking in the wrong place as JuK has been around for a few years longer than wxMusic."
    author: "Scott Wheeler"
  - subject: "Re: wxMusik vs JuK, not."
    date: 2004-06-02
    body: "Thanks for your reply.\n\n<quote>As disappointing as this may be OSS authors generally aren't working on the stuff primarily for your amusement.</quote>\n\nMy main point was: When programmers decide to code an app and use an open source licence for it. The step of making the app open sourced is usefull to the society and science at large. But I think it is never as usefull as if the programmer took the little extra (important) energy needed to care about current existing choices. And then, maybe try to help out developping a project already on the way.\nLetting the sources open is a proof of non-egoism from the coder, which should be positive.\nBut how much better is it if the people work together!\n\nTo come back to the quote I pasted at the beginning, I know I'm not enough self-centered to think people code and licence app as open source primarily for my amusement.\n\nYou maybe misunderstood me (as I don't write english very well), but what I was saying is: open and respected standards, associated with coders working in cooperation, would enormously profit to everyone.\n\n\nI understand when you say people like to do things their way, and coders around the world like this create the open source bazaar as you named it. But big projects like KDE as you are yourself participing coulnt exist if there wasnt any cooperation around standards between coders.\n\nThe ways to structure an application when desinging/coding should not be left to the programmer's habits and tastes. Considering the computers and softwares as a SCIENCE. Even an really exact science. (think physics theories that are almost philosophical ideas). Everything being a 0 or a 1, there is aways the BEST way to acheive a goal on a computer. There is ALWAYS a best and only way to program something. Of course the perfect program for one user won't be the same for the other. This is the desktop world of softwares (rudely, compared with the very sharp apps needed for scientific-only computations). The goal here is to build the best customizable base for the end users. Having the choice of 5 jukebox softwares is probably less good than having one, very modular and extensible program using a standards who can be modified to fit the new needs. \n\nI dont say this way of developing software is easily realizable, but I really do think it should work, at least in theory. But this theorical view, was tought to please the end user and then, should be appliable.\n\nBy the way, I (and i'm of course not the only one) really appreciate your work for JuK and taglib, among your others contributions.\n\nPierre D."
    author: "Pierre Delagrave"
  - subject: "I still think Yammi is a better solution"
    date: 2004-05-27
    body: "Even with its nonstandards icons ;), Yammi (http://yammi.sourceforge.net) is a great music manager that uses XMMS (or noatun) as the player. So, all the XMMS plugins, file formats etc. are avaiable... Yammi also has a powerful fuzzy (tolerant) search function, wishlists and more."
    author: "Laerte Barbalho"
---
In its <a href="http://www.linux-magazine.com/issue/43">current issue</a> <a href="http://www.linux-magazine.com/">Linux Magazine</a> published <a href="http://www.linux-magazine.com/issue/43/Juk_20.pdf">an article</a> (PDF) about playlist management with <a href="http://ktown.kde.org/~wheeler/juk.html">JuK</a>. For starters: JuK is KDE's outstanding playlist-based jukebox application with a lot of unique and powerful features. The article talks about playlist management, advanced tag guessing with <a href="http://www.musicbrainz.org/">musicbrainz</a> and how to keep your music collection consistant easily.
Author <a href="http://ktown.kde.org/~wheeler/">Scott Wheeler</a> has also been <a href="http://www.kde.nl/apps/juk/en/interview.html">interviewed</a> earlier by Andreas C. Diekmann.


<!--break-->
