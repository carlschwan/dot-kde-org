---
title: "KDE-CVS-Digest for April 23, 2004"
date:    2004-04-25
authors:
  - "dkite"
slug:    kde-cvs-digest-april-23-2004
comments:
  - subject: "Kpresenter"
    date: 2004-04-25
    body: "Is very good to see that kpresenter is evolving like this.\nCongratulations!"
    author: "Oliveira"
  - subject: "KStars ExtDate"
    date: 2004-04-25
    body: "My thanks to Michel Guitel for contributing the original ExtDate code.  It's very nice to be free of QDate's narrow year limitations (1752-8000)!\n\nMy commit log said that I might submit this to be part of kdelibs; as it turns out, that  might not be possible.  The code is heavily derived from several GPL'd Qt classes, so there's no way to release them under the LGPL.  I may submit the changes to Trolltech for consideration, though.  Or maybe we'll just leave it in libkdeedu; I don't know of another app that would really benefit from having such remote dates.\n"
    author: "LMCBoy"
  - subject: "Re: KStars ExtDate"
    date: 2004-04-25
    body: "libkdeedu is not LGPL?"
    author: "Anonymous"
  - subject: "Re: KStars ExtDate"
    date: 2004-04-25
    body: "Why should they be?"
    author: "Albert Astals Cid"
  - subject: "Re: KStars ExtDate"
    date: 2004-04-25
    body: "To clarify, libkdeedu is not part of kdelibs, it's in kdeedu.  They are generally just APIs that may be of interest to EDU apps.\n\nThe files in libkdeedu have the same license policy as any other kde apps directory (i.e., any Free software license os Ok, as long as it doesn't violate other licenses).\n"
    author: "LMCBoy"
  - subject: "Re: KStars ExtDate"
    date: 2004-04-25
    body: "Yes!  Now I can schedule a meeting with Sir Isaac Newton!"
    author: "KDE User"
  - subject: "KDE is becoming THE Open Source/Free Software proj"
    date: 2004-04-25
    body: "As a Linux user and free software/open source advocate, seeing KDE develop really boggles the mind, I think KDE is becoming one, if not the best, software that exemplifies the power of what a dedicated, motivated community can do by itself.\n\nThe infrastructure, the communication beetwen developers/users, the migration beetwen these two groups (thanks also to kde-apps which are enticing lots of users to try a bit of coding), everything about this project is something never before seen in the  computer science world. \n\nIt's just really amazing, it's beyond definition the magnitude of all of this.. it's....phew, mere words can't express anything that capture the scope of all this... Great work, everyone, great work.\n\n"
    author: "raditzman"
  - subject: "Text Selection in KHTML"
    date: 2004-04-25
    body: "I saw the following change:\n\n- added modes of text selection as mandated by the KDE style guide[1]\n  allowing extending selection by character (single click),\n  by word (double click), and by line (triple click).\n- fixed automatic scrolling which was broken on double click and triple\n  click when the selection was to be extended beyond the viewport.\n- fixed selection of both left and right words when double clicking\n  on the space between them.\n\nHow can I set this back to the old behaviour? I really hate this. Especially since this happens in e-mails too!\nWhy does KDE decides to be different than all other things in this?\nThe style guide mentions that this should be used in text editors also. I don't think any sane person would like this. Copy & Paste of all kinds of things would work so differently from what everyone would be used to..."
    author: "Jan Jitse"
  - subject: "Re: Text Selection in KHTML"
    date: 2004-04-25
    body: "> Why does KDE decides to be different than all other things in this?\n\nThere is absolutely no standard among non-KDE apps in this regard. \n\n> Copy & Paste of all kinds of things would work so differently from what everyone would be used to...\n\nwhat does copy paste have to do?"
    author: "fault"
  - subject: "Re: Text Selection in KHTML"
    date: 2004-04-25
    body: "Actually I like very much the \"new\" behaviour. It has been extensively been discussed on the relevant mailing lists, without anybody really disagreeing, which is quite a feat. \nAnyway, it's simply the way text selection is supposed to work under X11, check with Xterm, for example.\nI find myself trying to cut and paste full words or rows much more often than partial words, so I think this behaviour to be better for the common cases.\n\nThe only problem so far with the new selection code is that, at least on this machine, the selection sticks (that is, once a selection is done, a new click does not start a new selection, it extends the current one. I think this is a bug, but maybe I did a khtml checkout at a bad time).\n"
    author: "Luciano"
  - subject: "Re: Text Selection in KHTML"
    date: 2004-04-25
    body: "Ah... that was the most irritating one. New selections don't work out well enough. So that is a bug? At least in XTerm I can easily \"free-hand\" select, which I can't do in Konqueror right now.\nI am glad by that. Sorry for sounding flamey, but I was getting rather irritated by not being able to easily select a sentence in mails. It took me at least 4 tries to select the right amount of words. Especially the first and last word in a sentence are rather difficult to get right.\nWhere did this discussion take place? I'd like to see the arguments for and against."
    author: "Jan Jitse"
  - subject: "Re: Text Selection in KHTML"
    date: 2004-04-25
    body: "The expected behaviour is described at the following url:\n\nhttp://developer.kde.org/documentation/standards/kde/style/mouse/selection.html\n\nThe discussion took place mostly in kde-usability.\nSearch for \"standardizing modes of text selection\". \n\nThe \"sticky\" selection is not in the recommended guideline, so it is a bug. \nI understand from your comments that I'm not the only one experiencing a quirky selection. If that is the case, I can understand your reaction, up to a point.\nYou should trust KDE developer better...\n "
    author: "Luciano"
  - subject: "Re: Text Selection in KHTML"
    date: 2004-04-26
    body: "Wondered why Konqueror was whacky in that regard, my cvs checkout ~23 April 2004, yours?\n\nI will checkout & rebuild it, and see if it goes away if not, I intend to file a bugreport.\n\n"
    author: "James L"
  - subject: "Re: Text Selection in KHTML"
    date: 2004-04-27
    body: "No! Please don't. I'm aware of the problem. Filing bug reports won't make the bugs go away any faster.\n\nBtw, if you want to have a working checkout, use one from *before* 22. April."
    author: "Leo Savernik"
  - subject: "Re: Text Selection in KHTML"
    date: 2004-04-25
    body: "Yeah, I'm not fine with this too. I think a \"three click operation\" is as useful as a one button mouse."
    author: "Carlo"
  - subject: "Re: Text Selection in KHTML"
    date: 2004-04-25
    body: "so, do you like it or not? :)"
    author: "me"
  - subject: "Re: Text Selection in KHTML"
    date: 2004-04-25
    body: "Text selection is fundamentally broken in KDE CVS Head. What you're seeing is *not* the effect of said commit, but a totally different problem.\n\nAs soon as I have sorted out the problem, you will feel the new experience of text selection as it was intended to be from its inception: easy, consistent, and powerful.\n"
    author: "Leo Savernik"
  - subject: "Re: Text Selection in KHTML"
    date: 2004-04-26
    body: "Thanks leo... I know I looked in that code once to figure out why my selection would go bonzo when i hit tables.  The fact you have you head around that code is monumental.\n\ncheers!"
    author: "Ian Reinhart Geiser"
  - subject: "A lot of code"
    date: 2004-04-27
    body: "54278 lines changed?  How is that possible in one week!?"
    author: "a"
  - subject: "Re: A lot of code"
    date: 2004-04-27
    body: "It definitely is alot of code.\n\nThere are 160+ developers working. Commits are work committed in one cvs commit, which can be one file, or many files. The files touched are usually 3 times the commit count.\n\nThe i18n committers are often a collection from all the translators, which makes for a very large number of lines changed.\n\nAnd think of how a developer may work. He sees a block of code needing work, he either comments it out, or deletes it. That is say 10 lines. He writes the new code, say 15 lines. It requires some other header, maybe some variable names changed, any other calls need changing. So the 10 line fix results in 40 or 50 lines in the end.\n\nOther times, it simply is a matter of lots of new code added. \n\nhttp://cvs-digest.org/?diff&file=kdevelop/parts/documentation/searchview.h,v&version=1.3\n\nThat is one commit. How many lines? One commit among maybe a dozen or so of work on the same feature. Some real valuable work.\n\nThe numbers are real. As some kind of formal analysis, lines of code is flawed. But as a way to represent a large amount of work, week in week out, it is truthful. Frankly, it boggles my mind.\n\nhttp://cvs-digest.org/?stat&period=apr232004 \n\nshows the complete statistics from wednesday to wednesday.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: A lot of code"
    date: 2004-04-27
    body: "I'm just wondering how it is _humanly_ possible that Stephan Kulow and Ahmad M. Zawawi can edit tens of thousands of lines in 1 week!?  Even if they are editing translations or creating generated files, that sounds very, very high."
    author: "b"
  - subject: "Re: A lot of code"
    date: 2004-04-27
    body: "Stephan is probably committing work generated by scripts. For example the files used by the translators are generated from the source code by script. Remember he also changed version numbers, added a tag to each file, when he produced 3.2.2 as release dude. Plus all the small final finishes that make up a release. The scripts may make the numbers high, but I would say that Stephan probably spent many long hours as release dude.\n\nMr. Zawawi probably committed a translation. He is also the committer for other translators. If you look at:\n \nhttp://cvs-digest.org/?list&file=kde-i18n/ar/messages/kdebase&user=zawawi\n\nyou see he is merging the work he has in another cvs repository. His work pattern is to save a whole bunch, and commit all at once.\n\nBut take a look at \n\nhttp://cvs-digest.org/?diff&file=kde-i18n/ar/messages/kdebase/ksplash.po,v&version=1.51.2.3\n\nand you see some real work represented.\n\nAs I said, the numbers don't mean anything in absolute terms. When the script was written, I didn't believe the numbers either, so I went through the files and manually counted. They are right.\n\nDerek"
    author: "Derek Kite"
---
In <a href="http://members.shaw.ca/dkite/apr232004.html">this week's KDE CVS-Digest</a>:
<A href="http://www.koffice.org/kpresenter/">KPresenter</A> can save to Sony memory chips for use with Sony projectors. 
<A href="http://korganizer.kde.org/">KOrganizer</A> implements recurring todos. 
<A href="http://www.slac.com/pilone/kpilot_home/">KPilot</A> implements auto-detection of devices.
KDE now supports relocation of installation directories.
Work continues on <A href="http://www.kdevelop.org/">KDevelop</A> documentation browser, allowing documentation search from cursor.
<A href="http://xmelegance.org/kjsembed/">KJSEmbed</A> adds more signals and examples.

<!--break-->
