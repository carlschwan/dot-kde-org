---
title: "KDE France: Highest Internet Access Center in Europe uses KDE"
date:    2004-03-08
authors:
  - "gdelafond"
slug:    kde-france-highest-internet-access-center-europe-uses-kde
comments:
  - subject: "Yay"
    date: 2004-03-08
    body: "Cool story. I wonder why the monitors are mounted in such a funny way. Privacy?"
    author: "AC"
  - subject: "Re: Yay"
    date: 2004-03-08
    body: "Maybe, because this is the best way to place a monitor for health/usability reasons as scientific research found out in the 70s and 80s, but was forgotten by the PC industry during the PC boom of the 80s/90s (because it is more expensive than palcing the monitor on the desk) !?\n"
    author: "Harald Henkel"
  - subject: "Re: Yay"
    date: 2004-03-09
    body: "Or maybe it's just for space reasons and to protect the monitor. Remember, this is in a skiing resort, and you don't really want people putting their wet gloves, goggles, hats on top of the monitor, for lack of a better place.\n\nWith this \"funny\" mounting, the monitor is nicely protected behind a watertight glass plate, so no problem there.\n\nThe downside is that it might be a tad difficult if two people wearing big clumsy ski boots sit next to each other with their legs spread (due to lack of space _under_ the table).\n"
    author: "Al"
  - subject: "Re: Yay"
    date: 2005-10-03
    body: "i hate y bisachy\n"
    author: "dygumdt7ui,"
  - subject: "Cool"
    date: 2004-03-08
    body: "I'm going there in two weeks. This gives me another good reason to (sk)email some friends ;)"
    author: "dizzl"
  - subject: "Re: Cool"
    date: 2004-03-08
    body: "Just came back from there after some fantastic skiing and, as an avid KDE fan of many years, wonder why anybody would want to surf the web when they could be surfing the slopes?"
    author: "J"
  - subject: "Re: Cool"
    date: 2004-03-09
    body: "Because people don't surf the slopes 24 hours a day? You need to do other stuff, one of which might include surfing the web!\n"
    author: "Slovin"
  - subject: "Re: Cool"
    date: 2004-03-08
    body: "I found one of these in Les Arcs 1800 too. We came across it in a bar just as a Windows loving friend was telling me how Linux was never going to catch on on the desktop. \"Imagine getting Linux support up here,\" he said...\n\n"
    author: "James"
  - subject: "Is kiosk wel known?"
    date: 2004-03-08
    body: "The guy said he discovered kiosk after they started development. \nShouldn't there be more tutorials/promotion of kiosk? Searching for KDE kiosk on google mainly returns old links (howto for KDE2). I didn't look much further as I don't plan to use it immediately, but it should be more easy to get to the current docs (the only online doc I found is in the README in the cvs).\n\nTake this as a constructive remark ;-)\n\nrb"
    author: "rb"
  - subject: "Re: Is kiosk wel known?"
    date: 2004-03-08
    body: "http://www.kde.org/areas/sysadmin/ which is linked from kde.org in the left menu under \"Deploy\" as \"Sysadmin Documentation\". Know a better place for this?"
    author: "Datschge"
  - subject: "Re: Is kiosk wel known?"
    date: 2005-09-26
    body: "hi, \n   i have not been able to see any detailed link for setting up kde kiosk.the only Readme.kiosk i saw could not give me a guide to setup my kiosk for kde.\n   can any one help me out in steps for kde kiosk setup.\n\nregards \nchinedu "
    author: "chinedu"
  - subject: "Re: Is kiosk wel known?"
    date: 2004-03-08
    body: "> Searching for KDE kiosk on google mainly returns old links (howto for KDE2).\n\n\"KDE Kiosk\" returns here two different possible results: One shows the kde-kiosk mailing as first hit, the other the Kiosk README as second hit."
    author: "Anonymous"
  - subject: "Re: Is kiosk wel known?"
    date: 2004-03-08
    body: "you know what? I did not have the link to the readme in second place when I posted my message, only the MM and older howtos. Now I indeed get the link to the readme in the cvs.\n\nI still think a good illustrated howto would be better than this readme. I can only suggest this as I don't have time to do it. If I ever use kiosk (and that should come sooner or later), I'll certainly put a webpage with my experience.\n\nrb"
    author: "rb"
  - subject: "Re: Is kiosk wel known?"
    date: 2004-03-08
    body: "Yeah, but it's not really usefull if people don't know they need to search for the \"kiosk\" keyword to find info on locking down a system, is it? You can't assume people know exactly what to look for."
    author: "Andr\u00e9 Somers"
  - subject: "Re: Is kiosk wel known?"
    date: 2004-03-08
    body: "First hit for \"kde lockdown\" is a good one."
    author: "Anonymous"
  - subject: "Re: Is kiosk wel known?"
    date: 2004-03-08
    body: "Well, really there needs to be a GUI interface for most of this. An administrator should be able to open KControl in some sort of special system-wide mode and do something like disable arts for everyone. Aaron Seigo mentioned that lack in his People behind KDE interview."
    author: "Ian Monroe"
  - subject: "Re: Is kiosk wel known?"
    date: 2004-03-08
    body: "> Well, really there needs to be a GUI interface for most of this.\n\nWhat coincidence. kdeextragear-3/kiosktool was just imported."
    author: "Anonymous"
  - subject: "sigh.... kiosk"
    date: 2004-03-08
    body: "\"The interface was carefully locked but for the finest man in Fleet Street, it is a piece of cake to sneak in private places\"\n\nIthought... uhm? no kiosk?\n\n\"Browsing KDE community websites, we found an article about KIOSK. We immediately adopted it. We use it to deactivate apps setting menus, limit access to files and forbid shell access. \"\n\nargh! so one can't really trust kiosk!"
    author: "konqi"
  - subject: "Re: sigh.... kiosk"
    date: 2004-03-08
    body: "What do you mean?\n"
    author: "Richard Moore"
  - subject: "Re: sigh.... kiosk"
    date: 2004-03-09
    body: "Well, I thought iyt was simple. Kiosk is designed to lock the system for the user. But the author could _easily_ access the system in any way he wanted.\n\nThis clearly means kiosk is far fropm safe, and I wouldn' encourage any serious business to use it. I think the author needs to provide a little feedback on this to fix kiosk"
    author: "konqi"
  - subject: "Re: sigh.... kiosk"
    date: 2004-03-08
    body: "hmmm... I don't understand what you're trying to say?\n\n/David\n"
    author: "David"
  - subject: "Re: sigh.... kiosk"
    date: 2004-03-08
    body: " I think he's pointing out that the system had been locked down using kiosk support, but\nthat the author of the article got into the system easily all the same, and that this is does \nnot make the lockdown sound particularly trustworthy.\n "
    author: "ac"
  - subject: "Re: sigh.... kiosk"
    date: 2004-03-08
    body: "I personally never tested the kiosk mode so I couldn't say anything about any security vulnerability. But at the same time it might be worth looking into to see IF there is any weakness in the kiosk lockdown methods?!\n\n/David\n"
    author: "David"
  - subject: "Re: sigh.... kiosk"
    date: 2004-03-09
    body: "exactly. Ithought one could trust kiosk to lockdown the system. After all, it's meant to be designed just for that"
    author: "konqi"
  - subject: "Re: sigh.... kiosk"
    date: 2004-03-08
    body: "personally i didn't see anything in there that indicated a violation of the kiosk sandbox. more details may provide further details, but none of the information he presented demonstrated a kiosk weakness."
    author: "Aaron J. Seigo"
  - subject: "Re: sigh.... kiosk"
    date: 2004-03-08
    body: "Didn't read the article, but if he was using 3.1, wasn't there  a few items fixed over the last year. Hmmm, (thinking out loud) wouldn't it be nice to search on 'Waldo Bastian' & 'kiosk' & '>february 2003' and get a list?\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: sigh.... kiosk"
    date: 2004-03-09
    body: "From the author's comments I understood that he could (with some tricks) access some system information (like the linux version, etc) that was meant to be locked. I never used kiosk, so I can't tell what it's meant to disallow.\n\nNow,... I've seen similar systems on W$ platforms, and most were far from safe. The weakness is not the system (KDE in our case) but the applications.\n\nFor example, no matter how much you lock KDE dialogs, you can't prevent mozilla from showing your system files. And any (kde) application that uses non-standard file access methods wouldn't be locked either.\n\n"
    author: "uga"
  - subject: "Difference between KDE and Windows"
    date: 2004-03-09
    body: "Hehe, that's the difference between KDE and Windows: if somebody is using KDE, somebody immediately writes an article about it, no matter how unimportant the site may be. But nobody would write an article about people who are usnig Windows - they only are writing articles about those who don't.\n"
    author: "AC"
  - subject: "Re: Difference between KDE and Windows"
    date: 2004-03-09
    body: "What can I say to your comment? \n\n\n\nBetter I say nothing at all!\n\n\n\nCalle"
    author: "Calle"
  - subject: "Re: Difference between KDE and Windows"
    date: 2004-03-09
    body: "Yes, I agree there is a difference. It shows there are a lot of people who care about the KDE project enough to enjoy writing and reading this kind of articles. That's a thing that you dont see in the windows world.\n\nPersonally, not only I find a unix-like system running KDE more pleasant to use than Windows, I also find that the existance of the community adds a lot to the KDE experience."
    author: "Sergio"
  - subject: "Re: Difference between KDE and Windows"
    date: 2004-03-09
    body: "As I wrote this article, don't completely agree with you.\nEverybody knows what Windows can do. Millions of PCs in the world use Windows.\nBut, when you talk about Linux/KDE/whatever, most people don't know what it can do or think ordinary people cannot use it.\nThis sort of installation simply shows that even not skilled people can use Linux/XFree/KDE without any help.\nThis is the first for KGX on the desktop for all sorts of uses and all sorts of users.\nBut I agree that an Internet access point is not the most important thing in the world."
    author: "Gerard"
  - subject: "Re: Difference between KDE and Windows"
    date: 2004-03-09
    body: "The reason is simple. Say right now 100 internet cafes use this system. If now 5 of those more turn up, that's 5% sales growth.\n\nDon't you think MS would shout on every news site if they had a 5% sales increase????"
    author: "uga"
---
<i>"An intrepid KDE fellow did not hesitate to venture in the bare landscape of ice, snow and rocks of the French Alps to report on the highest Public Internet Access Centre located in the Tourist House of Val Thorens, a ski resort spreading between 2300 and 3200 meters. In these extreme conditions, our reporter found in the hall of the Tourist House, five cybertables full of computers in front of which tourists of all nationalities were sitting surfing the web with intensity after having surfed down the snowy slopes. "</i> Read <a href="http://www.kde-france.org/article.php3?id_article=115">more of this fascinating story</a> and view pictures at <a href="http://www.kde-france.org/">KDE France</a>.


<!--break-->
