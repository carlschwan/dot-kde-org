---
title: "Application of the Month: KMPlayer"
date:    2004-05-17
authors:
  - "ateam"
slug:    application-month-kmplayer
comments:
  - subject: "Debian package.."
    date: 2004-05-17
    body: "Is this packaged in debian?\nI couldn't find it with a cursory look..\n"
    author: "John Tapsell"
  - subject: "Re: Debian package.."
    date: 2004-05-17
    body: " *** 0.4.0-1 0\n        500 http://marillat.free.fr unstable/main Packages\n        100 /var/lib/dpkg/status\n"
    author: "Joseph Reagle"
  - subject: "Re: Debian package.."
    date: 2004-05-17
    body: "no mplayer or mplayer frontends are packaged in debian-- they are in 3rd party apt sources"
    author: "fault"
  - subject: "Re: Debian package.."
    date: 2004-05-17
    body: "I'm not at my computer now, so I'm not sure, but I think kmplayer is a frontend for xine as well (a relatively recent enhancement). If not kmplayer, then kplayer. "
    author: "Ian"
  - subject: "Embedded KMPlayer"
    date: 2004-05-17
    body: "I still use plain old MPlayer for local video files.. But KMPlayer really shines as a Konq plugin.  It works with many sites and many video formats.  Now instead of viewing the page source and trying to find a link to download, I can usually just let KMPlayer take care of everything.  It's great!"
    author: "KDE User"
  - subject: "Re: Embedded KMPlayer"
    date: 2004-05-18
    body: "that's the one thing that eluded me.  it never occurred to me that it could be used as plug-in also.  thanks for the tip!"
    author: "mark"
  - subject: "Re: Embedded KMPlayer"
    date: 2004-05-18
    body: "And, as inforamtion for developers, it implements the KMediaPlayer::Player interface, which makes it available as a video player component to any KDE application that needs an embedded player widget."
    author: "Kevin Krammer"
  - subject: "Please get the seeking right"
    date: 2004-05-17
    body: "Both KMPlayer and Kaffeine are pretty good frontends, but they both have an annoying behaviour when using the slider to seek in movies.\n\nIt's very slow and you can see the slider jumping from the start to the new position when dragging. They don't even update the picture until you release the mouse as far as I can tell.\n\nI begin to wonder if there is a flaw in the Qt toolkit since it seems so hard to implement right.\n"
    author: "gambolputty"
  - subject: "Re: Please get the seeking right"
    date: 2004-05-17
    body: "You're right, this should be done much better. Positioning the movie only after releasing the slider was because mplayer crashed if it was flooded with seek commands.\nThe jumping comes because the backends decide where exactly to start playing and this will reposition the slider.\nI guess this can be done with Xine much better.\nThanks for mentioning it!"
    author: "koos"
  - subject: "Re: Please get the seeking right"
    date: 2004-05-18
    body: "I just fixed this same bug in KPlayer a couple of weeks ago. You can now drag the slider, and it will seek through the movie quite smoothly. No need to wait till the slider is released. Check it out: http://sourceforge.net/cvs/?group_id=71710"
    author: "kiriuja"
  - subject: "Re: Please get the seeking right"
    date: 2004-05-18
    body: "I took a quick look, but didn't find it. Did saw a license conflick, I want to keep libkmplayer_common LGPL.\nBtw, your CVS comments, like Multiple bugfixes/Various changes and fixes/Multiple fixes/Various fixes and improvements, are not very helpfull.\nAnyhow, I think I should make it work for Xine first..\n\nNevertheless, thanks for the invitation. For kmplayer the application, which is also GPL, I might take a look for a playlist (if no one beats me with that, hint hint :-). "
    author: "koos"
  - subject: "Re: Please get the seeking right"
    date: 2004-05-19
    body: "> I took a quick look, but didn't find it. Did saw a license conflick, I want to keep libkmplayer_common LGPL.\n\nIt's in kplayerprocess.cpp. Basically once you send a seek you wait till the seek actually happens, then send a new one if any. Also when the slider is being dragged kplayerengine ignores any progress change events so the slider does not jump like crazy. :-) Works quite nicely for me. By \"check it out\" I meant see if it actually works in KPlayer for you, or for anyone else for that matter.\n\nAlso the problem some people had coming back from full screen mode is supposed to have been fixed, so if anyone is still having it with the current KPlayer CVS they would do well by reporting it asap per instructions at http://sourceforge.net/docman/display_doc.php?docid=22190&group_id=71710\n\nAs for LGPL, I am no expert in licensing, but I am really not sure how a program that compiles against Qt can be LGPL. Pardon my ignorance.\n\n> Btw, your CVS comments, like Multiple bugfixes/Various changes and fixes/Multiple fixes/Various fixes and improvements, are not very helpfull.\n\nI am aware of that problem. :-) But the amount of changes has been such that at 10 PM I am simply unable to do a diff and describe every change before committing.\n\n> Nevertheless, thanks for the invitation. For kmplayer the application, which is also GPL, I might take a look for a playlist (if no one beats me with that, hint hint :-)\n\nWell, of course KPlayer had a playlist since 0.4, but after I release 0.5 and then relax for a month or two, I will start making major enhancements to it. Anyone who has any playlist suggestions should post them at http://sourceforge.net/forum/forum.php?forum_id=244388\n\nI may post a detailed plan for the enhancements sometime this summer. And of course the playlist can be made a KPart so other people can reuse it. I don't think there is an existing playlist anywhere that is a KPart and also is anywhere near satisfying the KPlayer requirements.\n"
    author: "kiriuja"
  - subject: "Re: Please get the seeking right"
    date: 2004-05-21
    body: "> Basically once you send a seek you wait till the seek actually happens, then send a new one if any\n\nYes sound like it should be implemented that way, thanks.\n\n> I may post a detailed plan for the enhancements sometime this summer. And of course the playlist can be made a KPart so other people can reuse it. I don't think there is an existing playlist anywhere that is a KPart and also is anywhere near satisfying the KPlayer requirements.\n\nThat was one of the reasons I refused to make yet another playlist implementation. Something like kbookmarks for mm files should be in kdemultimedia, that basically understands playlist formats, widget for the playlist files, editing posibilities etc.\nI don't think it fits the KPart model though."
    author: "koos"
  - subject: "Re: Please get the seeking right"
    date: 2004-05-22
    body: "> I don't think it fits the KPart model though.\n\nWhy not? The way it is now in KPlayer is that the playlist editor is a widget, and everything else pertaining to the playlist is implemented as actions. Even the combobox with the currently played list is KWidgetAction based, so it embeds into a toolbar very nicely.\n\nAnyway, I'll let you know when I post an RFC. I won't mind if you do it either. ;-)"
    author: "kiriuja"
  - subject: "KPlayer"
    date: 2004-05-18
    body: "I prefer KPlayer. I just can't get along with KMPlayer's Interface, specifically the overly small main control buttons. "
    author: "Eike Hein"
---
Again this month we offer you a new version of the "Application of the Month" series as
the <A href="http://www.kde.nl/">Dutch team</A> is trying to get in sync with the 
<A href="http://www.kde.de/">German team</A>. 
This issue covers an interview of Koos Vriezen, author of 
<A href="http://www.xs4all.nl/~jjvrieze/kmplayer.html">KMPlayer</A>, a 
multimedia player for the KDE Desktop. Enjoy Application of the Month in the languages <A href="http://www.kde.de/appmonth/2004/kmplayer/index-script.php">German</A>, 
<A href="http://www.kde.nl/apps/kmplayer/">Dutch</A> and 
<A href="http://www.kde.nl/apps/kmplayer/en/">English</A>. 




<!--break-->
