---
title: "Linux.com: GUI Administration with KSysGuard"
date:    2004-05-24
authors:
  - "fmous"
slug:    linuxcom-gui-administration-ksysguard
comments:
  - subject: "Criticism"
    date: 2004-05-24
    body: "His usability criticism is probably the best of the article\n\n\"While I've started using KSysguard on a regular basis, and I recommend it to others, there's room for improvement. For one thing, this being a KDE application, it would make sense to integrate it with KPopUp and allow for alerts to be issued at certain threshholds, since it's not possible to view every sensor cell for every machine in a single worksheet. Alternatively, maybe the developers could build a \"Summary\" worksheet so that these alerts could simply be listed there, in a logview-type format. Also, it would be useful to be able to configure color changes based on thresholds in the signal plotter views. For example, I'd like my System Load signal plotter sensors to turn orange if the load creeps beyond, say, 6, and red if it goes over 10.\n\nThere are minor annoyances in the application, but they're mostly annoying only when you're first getting started using it. For example, if you drag two sensors to a cell, then discover that was not a stellar move on your part, you can't simply drag one sensor away from the cell -- you have to remove the sensor from the cell completely and start over. I understand why this is the way it is, but it would be good if there were a friendly way of reconfiguring a cell without starting over. \""
    author: "gerd"
  - subject: "Re: Criticism"
    date: 2004-05-25
    body: "It looks like the author would like to transform KSysguard into KTivoli or KBigbrother. While such an application would be a good idea, my guess is that KSysguard, which is great in it's own right, is not a good platform to build it on.\n"
    author: "Anonymous Coward"
  - subject: "Indeed..."
    date: 2004-05-24
    body: "Ksysguard is indeed quite useful.. I wasn't even aware of its ability to monitor processes on remote machines.  Awesome.\nI have just 2 minor nitpicks.  Firstly it's a bit cumbersome to remove sensors and remove sheets.  This should all be done by context menus when clicking on a tab or an active sensor.\nMore importantly, as a \"task manager\" like application it is far too complex for a normal user to make heads or tails of it.  I know that CTRL-ESC brings up just the process viewer but it's still too complex to figure out.  The columns displayed for the process viewer should be configurable and by default it should only show the process name, percent cpu usage, and percent memory usage.   I've been using linux for quite a while now and I still have absolutely no idea what the difference between Vmsize and Vmrss is and what actually represents the memory being used by a process."
    author: "Leo S"
  - subject: "Re: Indeed..."
    date: 2004-05-24
    body: "Something like 'ps auxw' is a common command to show processes, and gives you size and rss too. Size is the total amount of memory the process would be using if the entire thing was in memory, and rss is the amount of real memory currently in use."
    author: "AC"
  - subject: "Re: Indeed..."
    date: 2005-04-01
    body: "thanks for explaining this!! :)\nnow I understand why my java process 'takes up' 512MB so to say ;)"
    author: "mattie"
  - subject: "Interval"
    date: 2004-05-25
    body: "And I KSysguard is still limited by the minimum of 2 second interval?\n\nBah! My \u00fcbermighty Windows XP Task Manager has 0.5 second update rate, beat that! ;P\n\nSame thing with this Kicker applet thingie IIRC."
    author: "Tar"
  - subject: "Re: Interval"
    date: 2004-05-26
    body: "Let's not bring TaskManager into the picture... They still have to show us all the processes and a little more than just processor/memory performance and network traffic."
    author: "me"
---
<A href="http://www.linux.com/">Linux.com</A> is offering an article from the author Brian Jones, who as a sysadmin came across  <A href="http://docs.kde.org/en/3.2/kdebase/ksysguard/">KSysGuard</A>, the
KDE task manager and performance monitor. KSysGuard uses so-called sensors to 
retrieve the information it displays. Please <A href="http://www.linux.com/article.pl?sid=04/05/17/1832239">read the findings</A> of a sysadmin on KSysGuard.
<!--break-->
