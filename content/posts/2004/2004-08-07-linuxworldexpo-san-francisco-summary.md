---
title: "LinuxWorldExpo San Francisco Summary"
date:    2004-08-07
authors:
  - "csamuels"
slug:    linuxworldexpo-san-francisco-summary
comments:
  - subject: "we-hate-george-wright.jpg"
    date: 2004-08-07
    body: "You horrible horrible horrible people. I feel really hurt inside now :P *runs away to cry*"
    author: "George Wright"
  - subject: "ac"
    date: 2004-08-07
    body: ">>>To the author of this summary, it's clear that the 3.3 release is a turning point for KDE, the codebase has become stabilized and KDE is becoming faster, even more bug free, and just a really polished, beautiful and pleasant environment to work in.\n\nAnd we are \"only\" at 3.3. How old is KDE? Imagine all this in context. KDE development will theoretically never end. Even if we would all at some point in time agree that KDE had reached absolute perfection, there would still come another release ..and after that another and another. Then again, another release and another and another. Just when you think everything is OK, there comes another release and another... and another..."
    author: "ac"
  - subject: "Re: ac"
    date: 2004-08-08
    body: "I think if we continue to develop KDE, it's clear that it won't be in a perfect state. I'm saying that KDE will never be perfect, but it will continue to get closer and closer to that state."
    author: "Charles Samuels"
  - subject: "Re: ac"
    date: 2004-08-08
    body: "If people will not start to think one day they should rewrite KDE in mono or java, I think KDE has very good chances to become the best choice desktop\nof the next decade (and more), and really leave GNOME behind.\n\nIt's amazing how much bugfixes, speedups and features have been done for 3.3.\nIt looks exponential, what a pace... :-)\n\nPolishing, bugfixing, polishing and speeding up should be the credo for 3.4.\n\nI love KDE, and you guys ROCK."
    author: "ac"
  - subject: "Re: ac"
    date: 2004-08-08
    body: "I wouldn't bet on a KDE 3.4 to happen."
    author: "Anonymous"
  - subject: "Re: ac"
    date: 2004-08-08
    body: "Well I do bet there will be a 3.4.\nLets wait and see who's right ;)"
    author: "John Minor Versions Freak"
  - subject: "Thanks"
    date: 2004-08-07
    body: "Thanks novell for shutdown kde :)\nROTFL"
    author: "Miriam"
  - subject: "Re: Thanks"
    date: 2004-08-08
    body: "Yup.. I guess I better start getting used to the \"other\" DE now :-("
    author: "Abhi"
  - subject: "Re: Thanks"
    date: 2004-08-08
    body: "What are you talking about? "
    author: "cachehit"
  - subject: "Re: Thanks"
    date: 2004-08-08
    body: "Sounds worrying. What's up?"
    author: "jmk"
  - subject: "Re: Thanks"
    date: 2004-08-08
    body: "Nothing to see. Only a troll. Please don't stop, go on."
    author: "Anonymous"
  - subject: "Re: Thanks"
    date: 2004-08-08
    body: "http://news.com.com/Novell+to+release+enhanced+Linux+in+fall/2100-7344_3-5300555.html?tag=nefd.top\n\nI'm from India. I have seen Novell's Bangalore development center advertise for GTK developers. Think about it, KDE seems to be losing corporote support. Redhat, Sun, Novell and UserLinux. With Suse gone, I fear the clouds are looming in the horizon. boo hoo :-("
    author: "Abhi"
  - subject: "Re: Thanks"
    date: 2004-08-08
    body: "SUSE/Novell is not gone. And what lost development support from the others for KDE are you talking about?"
    author: "Anonymous"
  - subject: "Re: Thanks"
    date: 2004-08-08
    body: "Problem is briefly this: corporations will only use what ever comes by default and is most supported. If i read this article correctly, KDE is losing this support - ximian guys have brainwashed management well enough. But on the other hand; how could they be this stupid, if KDE has most users? "
    author: "huu"
  - subject: "Re: Thanks"
    date: 2004-08-10
    body: "Who gives a damn. Seriously.\n\nIf corporate support would be the key factor towards a useful product, I'd be running Microsoft Windows. But it isn't: KDE was useful long before corporations got interested, and will be useful for a long time after they've moved to GNOME or something else. It was never money that made KDE great, and in fact, some of the biggest screw-ups in our releases of the past were directly influenced by corporate interests.\n\nIn a world of increasing interoperability, market share is really not that important."
    author: "Rob Kaper"
  - subject: "Re: Thanks"
    date: 2004-08-08
    body: "SUSE is NOT gone. The current SUSE Linux 9.2 will still default to KDE. And the same is true for all future versions of SUSE Linux:\n\n http://linuxtoday.com/mailprint.php3?action=pv&ltsn=2004-03-31-... \n \n However, SUSE supports both the KDE and GNOME environments in its professional and enterprise products only, Schlaeger pointed out. \"We will never support anything but KDE in Personal Edition,\" he predicted. "
    author: "Anonymous"
  - subject: "Re: Thanks"
    date: 2004-08-08
    body: "from http://news.com.com/Novell+to+release+enhanced+Linux+in+fall/2100-7344_3-5300555.html?tag=nefd.top\n\n\"Novell's desktop software employs the GNOME user interface and software, but it will also include that of rival KDE, McLellan said. However, Novell's integration work is happening only with the GNOME applications, she said. \n\nFor example, a calendar item entered in Evolution will appear in the GNOME calendar, and an instant-messenger nickname entered in Evolution will appear in the GAIM instant-messenger client. GNOME components Evolution and GAIM will dovetail with GroupWise server software, while the KDE equivalents--KMail and Kopete--will not. \""
    author: "Abhi"
  - subject: "Re: Thanks"
    date: 2004-08-08
    body: "This is really terrible. I've tried to use almost every version of Evolution up to date on Fedora and SUSE with no luck - it just refuses to do anything with our imap server ( MS Exchange 5.5 ). Before anyone asks - yes, it doesn't even show my email, let alone calendars etc. Kontact works fine. And this buggy piece of s.. software you're now starting to offer to us. Great."
    author: "huu"
  - subject: "Re: Thanks"
    date: 2004-08-08
    body: "McLellan is a former Ximian employee. It's no wonder that she touts the stuff that way. Actually this article is a much more accurate read:\n\nhttp://www.eweek.com/article2/0,1759,1631172,00.asp\n\n'At the time, Novell thought it would have such a desktop ready by year's end. Now, Stone won't publicly give a date. \"The project isn't even in alpha.\"'\n\nAnd currently those not-even-alpha-versions offer the user the choice between KDE and Gnome (with no default picked)."
    author: "Anonymous"
  - subject: "Re: Thanks"
    date: 2004-08-08
    body: "> And currently those not-even-alpha-versions offer the user the choice between KDE and Gnome (with no default picked).\n\nSo,\na) Did the Ximian guy lie in public (wouldn't be a first time)? If this is the case, please use some proper PR folks to do these 'announcements', not coders. This is getting ridiculous.\nb) Or is really all integration work done for Gnome only? If it is, i think the default is quite clear no matter what the installer option does at this stage."
    author: "huu"
  - subject: "Re: Thanks"
    date: 2004-08-08
    body: "> b) Or is really all integration work done for Gnome only?\n\nNo, isn't the SUSE employee working on further integrating OpenOffice.org with KDE proof enough?"
    author: "Anonymous"
  - subject: "Re: Thanks"
    date: 2004-08-08
    body: "you know what the most funny thing is... kopete/kontact/kadressbook can already do this..."
    author: "superstoned"
---
Another LinuxWorldExpo has ended in the lovely San Francisco, and the KDE project
was proud to have a booth and present the equally lovely desktop environment. Also
available is a small
<a href="http://ktown.kde.org/~charles/lwce2004/">photo gallery</a> of
the event.


<!--break-->
<p>We KDE developers had yet another great time demoing KDE 3.2 (and the 3.3 beta).
Our users wanted to know what kind of new <a href="http://www.kde-look.org">eye candy</a>
was in store for them for the 3.3 beta. We also demonstrated (and discovered!) the cool
new program <a href="http://www.kde-look.org/content/show.php?content=14865">Komposé</a>.
</p>

<p>
To the author of this summary, it's clear that the 3.3 release is a turning point for KDE, the
codebase has become stabilized and KDE is becoming faster,
even more bug free, and just a really polished, beautiful and pleasant environment to work in.
</p>

<p>
KDE received many donations for <a href="http://www.xandros.com/">Xandros</a> Desktop Office,
and <A href="http://www.libranet.com/">Libranet</a>.
We want to thank all our users who have made a donation as it will go to assist in bringing
developers to events like the upcoming <a href="http://conference2004.kde.org/">KDE World Summit</a>
in Ludwigsburg, Germany. Again, a big <i>thank-you</i> to our supporters!
</p>

<p>
Another big thanks also goes to <a href="http://www.novell.com/">Novell</a> which contributed
two computers which we demonstrated KDE with. We couldn't have done it without you!
</p>

<p>
And as a special bonus, we had contests on Thursday in which the sound of the words "I Love KDE"
permeated the entire show floor.
</p>

<p>
(Editor's note -- editor not available, buried in inside-jokes)
</p>

In attendance were the following:
<ul>
	<li>Alex "blarf" Zepeda (all days, starting at 3:00 when he woke up :)<br />
		<i>"We made lots of people shout, drank Gnome booze, and tried our best to not offend people."</i>
		</li>
	<li>Nathan Krause (all days)</li>
	<li>David "Canada" Mattatall, eh (Wednesday, Thursday)<br>
		<i>"Eh"</i>
		</li>
	<li>David Johnson (Wednesday)</li>
	<li>Roland Krause (Wednesday)</li>
	<li>Matthias Ettrich (on and off when the Trolls let him go)
		<i>"Girlie Men"</i>
		</li>
	<li>Charles "I hate you" Samuels (all days)</li>
</ul>

