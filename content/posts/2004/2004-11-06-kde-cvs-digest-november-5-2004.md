---
title: "KDE CVS-Digest for November 5, 2004"
date:    2004-11-06
authors:
  - "dkite"
slug:    kde-cvs-digest-november-5-2004
comments:
  - subject: "Kwallet"
    date: 2004-11-06
    body: "Great to see that Kwallet is now asynchronous, now I only have just one tiny wish : make Kwallet usable. Allowing a passwordless wallet could just do that.\n\nhttp://bugs.kde.org/show_bug.cgi?id=78505\n"
    author: "AC"
  - subject: "Re: Kwallet"
    date: 2004-11-06
    body: "1) Kwallet was always asynchronous.  Just KHTML didn't use the asynchronous interface.  That's what I added this week.\n2) Passwordless wallets was committed about 10 minutes ago.  It's still unbeleivably stupid to use such a thing though.  Maybe in a few weeks I'll release tools for automated downloading passwordless wallets in a stealth fashion (think: Linux spyware).  Should be fun.\n"
    author: "George Staikos"
  - subject: "Re: Kwallet"
    date: 2004-11-06
    body: "About password-less wallet, as it is told in the bug report, it's a security feature that do not have sense in some cases. What about the option to leave the wallet opened? If someone could access your desktop, he will get every password of yours.\nAnd if someone could get his hands over an unprotected wallet, well, he have already broken your ~ security walls."
    author: "Davide Ferrari"
  - subject: "Re: Kwallet"
    date: 2004-11-06
    body: "But what if somebody happens to be root or otherwise have read access to the wallet without having to break any security walls? In that case this guy can also use the stored data, if the wallet is not password-protected.\nAnd even if somebody breaks in from the internet, why should this mean that everything is lost anyway so just let him open the wallet?\nWhy not make pgp keys with no passphrases then? If somebody already has gained access to my hard disk/wherever i keep my key, why not also let him use it? :)"
    author: "yet_another_insecure_poster"
  - subject: "Re: Kwallet"
    date: 2004-11-06
    body: "But what if somebody happens to be root or otherwise have read access to the wallet without having to break any security walls? In that case this guy can also use the stored data, if the wallet is not password-protected.\nAnd even if somebody breaks in from the internet, why should this mean that everything is lost anyway so just let him open the wallet?\nWhy not make pgp keys with no passphrases then? If somebody already has gained access to my hard disk/wherever i keep my key, why not also let him use it? :)"
    author: "yet_another_insecure_poster"
  - subject: "Re: Kwallet"
    date: 2004-11-07
    body: "If someone is root on your system, they could just install a keylogger and get your password when you next login.\n"
    author: "JohnFlux"
  - subject: "Re: Kwallet"
    date: 2004-11-07
    body: "Do you know of any key [free] logger for Linux? I'd like to get my hands on one."
    author: "charles"
  - subject: "Re: Kwallet"
    date: 2004-11-06
    body: "Woohoo!!, thanks a lot! (and at the same time shame on you, because of you I now can't wait until kde 3.4 is released). KDE 3.4 is going to be awesome, new kpdf, improved khtml and passwordless kwallet, just to name a few.\n\nHow realistic is it that wallet can be downloaded from my computer? Only by using an exploit (somewhere else)? How secure are other passwordless wallets (e.g. Firefox, IE)?"
    author: "AC"
  - subject: "Re: Kwallet"
    date: 2004-11-06
    body: "> How secure are other passwordless wallets (e.g. Firefox, IE)?\n\nLet me refrase that question; how easy can other passwordless wallets be downloaded from my computer (e.g. Firefox, IE)?"
    author: "AC"
  - subject: "Re: Kwallet"
    date: 2004-11-06
    body: "> shame on you\n\nYour nice words are truly appreciated."
    author: "Anonymous"
  - subject: "Re: Kwallet"
    date: 2004-11-06
    body: "wow, you don't know what a joke (although a little bit sarcastic) is, right?\n\nPlease don't quote a half sentence and rip it out of the context.\n\nIf you read the entire sentence (before hitting reply) you see that I really appreciate his work."
    author: "AC"
  - subject: "Re: Kwallet"
    date: 2004-11-07
    body: "LOL, maybe he was being sarcastic too.\n\nI also can't wait for 3.4 btw. Though I am quite pleased with 3.3.1."
    author: "Alex"
  - subject: "Re: Kwallet"
    date: 2004-11-06
    body: ">  It's still unbeleivably stupid to use such a thing though.\n\nI second that. I don't even understand why this is added. You can already configure KWallet to ask for the password just once for the whole KDE session, already insecure and easy enough IMO.\n\nBest regards,"
    author: "Richard Van Den Boom"
  - subject: "Re: Kwallet"
    date: 2004-11-06
    body: "> I don't even understand why this is added.\n\nSome people care more about usability then security."
    author: "AC"
  - subject: "Re: Kwallet"
    date: 2004-11-06
    body: "Considering that providing a password once during your whole KDE session makes the whole thing unusable is the very kind of reasoning which made Windows the virus trashcan it is now.\nSimplifying things as Windows did made the users get a lot of bad habits. I don't think it's a good idea at all for Linux to make the same mistake.\n\nBest regards,"
    author: "Richard Van Den Boom"
  - subject: "Re: Kwallet"
    date: 2004-11-06
    body: "Hello,\n\nplease understand that at work many of us don't have any secret passwords at all. In many cases, the passwords are for not-exactly public downloads for and from subcontractors, shared support database accounts, identifications of myself for the intranet email, etc.\n\nNone of this is secret because I have to have no privacy at work.\n\nSo why protect it with a password?\n\nSee it this way, privacy is good and there I want to protect my passwords very well. I wouldn't save my private passwords at work though. There kwallet is only something to save me time. Even entering the password once or twice a day is wasted time in my use case.\n\nAt home I see your point.\n\nYours, Kay\n\n"
    author: "Debian User"
  - subject: "Re: Kwallet"
    date: 2004-11-06
    body: "Se my bug report [92826]: http://bugs.kde.org/show_bug.cgi?id=92826\n\n"
    author: "jadrian"
  - subject: "Re: Kwallet"
    date: 2004-11-07
    body: "Nevermind :-|"
    author: "jadrian"
  - subject: "Re: Kwallet"
    date: 2004-11-07
    body: "The reason KWallet has passwords isn't to annoy the user, it's to encrypt the password file on the hard disk.  So what happens if (God-forbid), your computer gets hacked, and some random cracker gets access to all of the passwords in your wallet file.  Now all the subcontractors that you have access to have been put into danger because it's too hard to type a password once per session. :(\n\nIf you need people at work to know your passwords, just give them your passwords.  But there's no reason nowadays to have any kind of sensitive information sitting on your hard disk in plaintext.\n\nSo although I'm not going to fault George for implementing this, I think it's a bad idea.\n\nRegards,\n - Michael Pyne"
    author: "Michael Pyne"
  - subject: "Re: Kwallet"
    date: 2004-11-07
    body: "If someone have access to your password file, he could easily bruteforce it in a few days, so if a cracker gain access to your home directory, you are alredy f*cked.\n\nThe weak point, in this case, is not the password-less wallet but the rest of the system. And anyway, noone is asking for an only-passwordless wallet a-la IE, we are only asking for an option (even with a big red warning reminder saying it's your OWN risk and NOT activated by default) to use it.\n"
    author: "Davide Ferrari"
  - subject: "Re: Kwallet"
    date: 2004-11-09
    body: "Hello,\n\nI repeat: These are not exactly high profile secrets being exchanged. It's more an issue of making things not anonymous ftp but rather login protected. Or giving people the chance to identify themselves in their postings on support database.\n\nNothing at risk there, often the password is designed to be easy to remember, sometimes even more or less obvious to guess.\n\nIf you are not working on calls for tender, or doing something like accouting, management and stuff, you don't have anything secret, but still a lot of passwords to deal with. I guess it's like 10 in my case.\n\nSo what. Why not just work well in this context and don't bother me with things at all. \n\nKay"
    author: "Debian User"
  - subject: "Re: Kwallet"
    date: 2004-11-06
    body: "Because KWallet is _a pain in the ass_. After KMail also started using it, it locks up the desktop on bootup, which mean entering two passwords every time I log in (how does that make sense?). If someone breaches my local-account I have a lot bigger problem than loosing the low-security passwords kept in KWallet."
    author: "Carewolf"
  - subject: "Re: Kwallet"
    date: 2004-11-06
    body: "So turn it off, like it gives you the option to do right when you first run it?  You sound like Homer Simpson at the batting cage.  Constantly being hit by baseballs but not knowing enough to just move out of the way."
    author: "George Staikos"
  - subject: "Re: Kwallet"
    date: 2004-11-06
    body: "George, you hit the nail right on the head! Keep it up.\n\nCb..\n"
    author: "charles"
  - subject: "Re: Kwallet"
    date: 2004-11-06
    body: "George, why not a pam module or something similar for kwallet instead of passwordless mode? That way the user can enter one password on login, instead of likely the same password twice. (They may be different, but usually not.)\n\nCheers,\nSheldon."
    author: "Sheldon Lee-Wen"
  - subject: "Re: Kwallet"
    date: 2004-11-09
    body: "I completely agree.  I would love to have this as an option.  Just a pam module that the sysadmin has to put in himself to take the first password and ATTEMPT to unlock the wallet with it on login.  Is this possible?"
    author: "Chris"
  - subject: "Re: Kwallet"
    date: 2004-11-06
    body: "You guys have to start to think about what provides security and what doesn't.\n\nHasseling the user doesn't increase security.\n\nIf you ask for a password once per KDE-session and the wallet is open for the whole session, there is ABSOLUTELY NO DIFFERENCE from a security POV compared to no password at all.\n\nIt's just a little bit more annoying which seems to give some people the delusion that when it's annoying, it must also be secure.\n\nWhich of course is pure nonsense.\n"
    author: "Roland"
  - subject: "Re: Kwallet"
    date: 2004-11-06
    body: "> If you ask for a password once per KDE-session and the wallet is open for the \n> whole session, there is ABSOLUTELY NO DIFFERENCE from a security POV compared \n> to no password at all.\n\nSorry, but this is UNTRUE.  It seems you did not consider all the use cases.\n\nIf I left my workstation with my screen lock on (or if it's just off), what would you try in order to get to my plaintext passwords?  If there's no password you could just boot the machine from a CD, for example, and get my unencrypted kwallet, if there is a password you would still need to crack it. \n\n"
    author: "cm"
  - subject: "Re: Kwallet"
    date: 2004-11-07
    body: "And what about the rest of your home files? Why aren't you using an encrypted filesystem if you are living in a such unsecure environment and your data has to be protected?"
    author: "Davide Ferrari"
  - subject: "Re: Kwallet"
    date: 2004-11-07
    body: "What does that have to do with the fact that the two situations \na) passwordless kwallet and \nb) encrypted kwallet with infinite timeout\nare not equal under all scenarios?  Roland said they were and I disagreed. \n\n"
    author: "cm"
  - subject: "Re: Kwallet"
    date: 2004-11-07
    body: "What I meant if that some untrusted one have phisical access to your box, you're not safe at all, KWallet with password or without"
    author: "Davide Ferrari"
  - subject: "Re: Kwallet"
    date: 2004-11-09
    body: "> You can already configure KWallet to ask for the password\n> just once for the whole KDE session\n\n\nConsidering our uptimes, isn't this just as \"stupid\"?  I don't consider it \"stupid\" either way, though.  I personally don't use kwallet, but that's not my point.  It's a great life-saver for people that it applies to (and, as has been pointed out, for general \"useability\").  So either save it \"forever\", or for days/weeks/etc.  What's the difference?\n\nM.\n\n"
    author: "Xanadu"
  - subject: "Re: Kwallet"
    date: 2004-11-06
    body: "> It's still unbeleivably stupid to use such a thing though.\n\nI for one think self proclaimed \"security experts\" are unbelievably stupid for thinking there are no other concerns than security.\n\nMaybe you are smart enough to realize that some people don't give much about having their slashdot account compromised.\nMaybe you are smart enough to realize that some people are the only ones who use their personal accounts and don't install unoffical software?\n\n"
    author: "Roland"
  - subject: "Re: Kwallet"
    date: 2004-11-07
    body: "> Maybe you are smart enough to realize that some people don't give much about having their slashdot account compromised.\n\n> Maybe you are smart enough to realize that some people are the only ones who use their personal accounts and don't install unoffical software?\n\nMaybe these people should be smart enough to disable KWallet if they don't need it?  Some people are smart enough to actually take advantage of the fact that KWallet encrypts passwords on disk, which makes KWallet useful for more that just storing Slashdot passwords."
    author: "Michael Pyne"
  - subject: "Re: Kwallet"
    date: 2004-11-07
    body: "Yes, but if I disable KWallet I have to remember all my passwords, and this is not definitely usable."
    author: "Davide Ferrari"
  - subject: "Re: Kwallet"
    date: 2004-11-06
    body: "Passwordless wallets maybe are a bit more insecure, they're a hell of a lot more usefull. I personaly hate it that everytime I fire up something that accesses KWallet I have to give my password. Yes, I do use the \"remember password this session\"-option, but it still is one time too many.\n\nApple's Keychain is great in that aspect: you don't see it, but it's just there in the background unlocked when I log in. That's what KWallet should be: invisible.\n\nIsn't it possible to unlock the wallet the same way Keychain does it: when I log in? That way it would stay secret for other users yet be convenient to use.\n\nI really enjoy your work btw, wish I could do what you can."
    author: "Anonymous"
  - subject: "USB Keychain unlocks kWallet?"
    date: 2004-11-08
    body: "Would it be possible for KWallet to look in a plugged in USB Flash keychain so people could plug in their key and then all of their KWallet information is released? That way when you pull out the keychain KWallet will check for the key and it won't be there and no access\n\nbut when you have the key plugged in, each time KWallet wants to use information stored in it, it will check the USB keychain for the PGP key and if its there it will allow the user to access the information..\n\ndoes this sound like a good idea? It would be the best feature ever IMO.... I know at least the people I work with would adopt something like this for better security and more convenience (and coolness factor of unlocking your computer with a USB Key)\n\n:)"
    author: "Forest"
  - subject: "Re: Kwallet"
    date: 2004-11-06
    body: "What if KWallet would use PAM (which would use pam_ssh)? With the right setup you can already just enter the passphrase of your private key in ~/.ssh/ to login to you computer. pam_ssh then launches ssh-agent so that you\u00b4ll never have to enter your passphrase again when it\u00b4s needed.\n\nNow if KWallet would support PAM you\u00b4d only have to login once at system startup! And ssh-agent should be considered to be save, shouldn\u00b4t it?"
    author: "Steffen"
  - subject: "Re: Kwallet"
    date: 2004-11-07
    body: "And yes, this would be even better than the password-less wallet, cause it would take the best from the 2 choices"
    author: "Davide Ferrari"
  - subject: "Re: Kwallet"
    date: 2004-11-07
    body: "I\u00b4ve created a wishlist item in the KDE bug databse: http://bugs.kde.org/show_bug.cgi?id=92845"
    author: "Steffen"
  - subject: "Re: Kwallet"
    date: 2005-01-23
    body: "what kde/kdm/X really needs is a login where you select from a user name tied to a private, password protected key at the login screen which is used to encrypt every small (under a meg, configurable?) file in your home directory, or the directory itself.  Including all stored passwords, config files, ect.  And if you then felt the need you could have other ssh keys passwordless in your file system, or use the same login key from X.  \n\nIf anyone knows how to do anything like this please post."
    author: "Madnessx"
  - subject: "Ints and Bools"
    date: 2004-11-06
    body: "\"Same thing for myint == 0, this is more readable than !myint\"\n\nActually I have mixed feelings about this. And I explain. When working with Logic everyone simplifies P <=> True to P, and P<=>False to ~P. It actually makes things much easier to read. Also in Pascal and other languages, students are taught to simplify P = True  to P and P = false to not P.\n\nSo why do I have mixed feelings about it? Because David Faure actually as a point in the context of C/C++/Matlab etc, which (unfortunately) mix booleans and integer datatypes. Lets look at booth versions of the code for P integer (k-n) and P boolean (k>n).\n\n1a) while (k-n){...}  // P is an integer\n2a) while (k>n){...}  // P is a boolean\n\n1b) while (k-n == 1){...}  // P is an integer\n2b) while (k>n == 1){...}  // P is a boolean\n\nI expect most people to agree that 2a) and 1b) are the easiest to read. Only this ones would typecheck in languages that do not mix booleans with integers. In C++ you can also have \n\n2b') while (k>n == true){...}\n\nwhich would typecheck too, but which is not more readable then 2a).\n"
    author: "John Programming Freak"
  - subject: "Re: Ints and Bools"
    date: 2004-11-06
    body: "\"1b) while (k-n == 1){...} // P is an integer\"\n\nOpss, make that \n1b) while (k-n != 0){...} // P is an integer\n"
    author: "John Programming Freak"
  - subject: "Re: Ints and Bools"
    date: 2004-11-06
    body: "I think readable may have much to do with the context. There may be a large number of conditions that are checked, and being more verbose can help the feeble mind keep it all straight.\n\nIt is a matter of style. Some prefer one way, others prefer another.\n\nReminds me of a story from my youth. My father had a garage full of tools that he used to feed us hungry hordes. It seemed disorganized and random until I took something to use. I would put it back on the nearest flat surface, and invariably my father would ask where it was. He had a system which made little sense to anyone else, but it worked for him.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Ints and Bools"
    date: 2004-11-06
    body: "Yes, of course I understand you ;) I don't think this is the case though. Your examples reflect my own reasoning:\n\nYou say \"isNull()\" is ok, for instance. Well, this is a boolean expression. It's also easy to read \"!isNull()\", right? \n\nThen you stated:\n\"Same thing for myint == 0, this is more readable than !myint, when the int\ncan take several values and we're only testing against 0 here, but maybe \n1 and 2 later on. There is nothing to gain by writing !myint here, only loss of readability.\"\n\n\"Several values\" reflects usage of \"actual\" integer values, not booleans. In this case you suggest not using them as booleans. \n\n\nYou also say:\n\"But changes like\n-  while ( ( wdg = it.current() ) != 0 ) {\n+  while ( ( wdg = it.current() ) ) {\ndon't make it faster, and do hurt readability (the changed line looks to me \nlike it might be a typo for wdg == it.current(), whereas the original line\ndidn't have that problem.\"\n\nWhy doesn't the original line doesn't have that problem? Couldn't it be?\nwhile ( ( wdg == it.current() ) != 0 )\n\nMaybe because, since it's a boolean expression you'd write it like:\nwhile (wdg == it.current())\n\n\n\nBools as Ints are just a very old and poor design decisions, which just add to the confusion with no benefits whatsoever. We can avoid them on our own, but it's a shame the compiler won't help."
    author: "John Programming Freak"
  - subject: "Re: Ints and Bools"
    date: 2004-11-07
    body: "> 1a) while (k-n){...} // P is an integer\n> 2a) while (k>n){...} // P is a boolean\n\nThese are not equivalent.  A while loop will iterate will the expression is != 0.  So, it should be:\n\n1a) while (k-n){...}  // P is an integer\n2a) while (k!=n){...} // P is a boolean\n\n> 1b) while (k-n == 1){...} // P is an integer\n> 2b) while (k>n == 1){...} // P is a boolean\n\nThese are nowhere near equivalent.  2b) is true for many (k,n) pairs for which 1b) is false.\n\nRegards,\n - Michael Pyne"
    author: "Michael Pyne"
  - subject: "Re: Ints and Bools"
    date: 2004-11-07
    body: "Yes, I did correct that ;)\nhttp://dot.kde.org/1099721820/1099742242/1099742691/"
    author: "jadrian"
  - subject: "Is aRts dead?"
    date: 2004-11-06
    body: "I see that JuK has added GStreamer support. However it is GStreamer 0.8. My distro (which was released ealy 2004) has only GStreamer-0.6. Why are not GStreamer version <0.8 supported? It is the same with amaroK (it's worse with amaroK CVS as aRts support is completely broken)\nIs aRts dead for KDE 3.4?"
    author: "annma"
  - subject: "Re: Is aRts dead?"
    date: 2004-11-06
    body: "JuK has had GStreamer support for years.  It was GStreamer 0.6 until this update; I just updated the bindings to use 0.8.  GStreamer 0.8 came out in March and it's relatively easy to install it side-by-side with 0.6."
    author: "Scott Wheeler"
  - subject: "Re: Is aRts dead?"
    date: 2004-11-08
    body: "For whatever reason, juk never picks up gstreamer in my /usr/local/lib"
    author: "Joe"
  - subject: "Re: Is aRts dead?"
    date: 2004-11-07
    body: "Given that gstreamer hasn't even reached 1.0 yet, it would be silly to support older versions."
    author: "Ian Monroe"
  - subject: "Re: Is aRts dead?"
    date: 2004-11-07
    body: "All the gstreamer stuff can still use arts.  Just run gstreamer-properties and set your default output sink to arts.  gstreamer does not replace the audio server.\n\nSpeaking of audio servers: It seems both kde and gnome are looking at replacing their audio servers in the near future.  I really hope they choose the same one."
    author: "theorz"
  - subject: "CVS digest"
    date: 2004-11-07
    body: "I don't know whether anybody has voiced the idea already: It would be cool if you could generate links to applications on kde-apps.org or their respective homepages  in the CVS-Digest.\n\nFor example, \"kst\" is an application name that tells nothing about the application itself. Therefore it, would be cool, if the entry \n\n\"George Staikos committed a change to kdeextragear-2/kst/kst/datasources/cdf/cdf.cpp\"\n\ncould be linked to http://www.kde-apps.org/content/show.php?content=10399 automatically based on the fact that the path of the commit begins with \"kdeextragear-2/kst\"."
    author: "Dominic"
  - subject: "Re: CVS digest"
    date: 2004-11-07
    body: "Someone could be of assistance here.\n\nwww/areas/cvs-digest/enzyme/enzyme_summary.ini\n\ncontains application = urls that are used for substitution. It is currently used for the summary, but can easily be extended to other places.\n\nAdd all the desired applications and urls, and either commit the changes or email me the file.\n\nThanks\n\nDerek"
    author: "Derek Kite"
  - subject: "Secuirity Problems with Passwordless Kwallet"
    date: 2004-11-07
    body: "I personally would not have added this *feature* at all because of the consequences it will have. Many users will likely use it because it is more convenient. Kwallet was after all created to improve convenience.\n\nAnyway, I just want to make sure that the user is warned. If he has a passwordless Kwallet, there should be some warning of the grave problems with such a thing. Yes, it is obvious that it is less secure, but users often miss supposedly obvious things."
    author: "Alex"
  - subject: "Re: Secuirity Problems with Passwordless Kwallet"
    date: 2004-11-07
    body: "Where do you get the idea that normal users actually expects, or even wants, kwallet to increase their security? I just want a convenient way to store passwords, and i find that the only thing i want it to ask me, is whatever i want to save a password or not. Can i do this by turning off kwallet? I haven't tried, but the obvious guess is no, since it is afterall kwallet that does the storing.\n\nI find it seriously disturbing that people speak against adding this feature just because they find the security aspect to be paramount. I haven't seen anyone question whatever or not it should let those who want the added security have it. Now if it could just stay in the background and out of the way and still be secure, that would be great, but i would much rather use a non-encrypted version that gets off my back, than an encrypted version that doesn't. I don't want to enter an extra password every time i enter my house either, and i would be a lot more upset if my house was ripped, than my computer."
    author: "Troels"
  - subject: "Re: Secuirity Problems with Passwordless Kwallet"
    date: 2004-11-07
    body: "Gee whiz. Now we must ask whether people want things secure? In case it causes some minor inconvenience?\n\nThis whole discussion is unbelievable.\n\nThe feature can be turned off. \n\nLets say you turn it off because you (or someone less experienced) thinks they don't want passwords saved securely.\n\nLet's see... internet banking, paypal, ebay, various purchases from amazon or many of the other vendors on the internet. Now every time you set up an account somewhere you (or someone less experienced than you) are going to remember that you don't have things secure?\n\nOver time most users are going to have some important things in kwallet. I don't think that it is unwise to assume that. So the only thing a responsable developer will do is default to the most secure setting.\n\nI don't lock my door at home either. No need for it. My front door isn't exposed to ? million people like my always on internet enabled computer.\n\nDerek (it isn't paranoia to think everyone is out to get you when they are)"
    author: "Derek Kite"
  - subject: "Re: Secuirity Problems with Passwordless Kwallet"
    date: 2004-11-07
    body: "Derek, the point is that there should be a \"compromise\" between security and convenience, for example a kwallet encryption based on the already entered user password."
    author: "Davide Ferrari"
  - subject: "Kontact and RSS - don't know where else to ask"
    date: 2004-11-07
    body: "Apologies to all about this one -  but I hope all of you will be tolerant and someone could indentify a source of Kontact compatible French language RSS feeds.  I have found loads with .XML extensions - but they don't seem to work.\n\nOr am I being irritating and stupid?\n"
    author: "gerry"
  - subject: "Re: Kontact and RSS - don't know where else to ask"
    date: 2004-11-08
    body: "Which entices to bang again my bug #7765 (building whole KDE to understand concept of online/offline) -- one reason why I am still using rss2email instead of RSS support in Kontact is that it is not possible to switch RSS reader into offline mode.\n\nMatej"
    author: "Matej Cepl"
  - subject: "Plastik updates of 3.3.1"
    date: 2004-11-08
    body: "Hi, I notice that 3.3.1 is quicker than 3.3.0, this might be related\nto the performance fixes of plastik, I like it!\n\nAm I the only to notice anything?"
    author: "ac"
  - subject: "Re: Plastik updates of 3.3.1"
    date: 2004-11-08
    body: "Here too. It seems more robust, rapid and eye-candy. Very nice work."
    author: "Davide Ferrari"
  - subject: "Re: Plastik updates of 3.3.1"
    date: 2004-11-09
    body: "The main reason why Plastik should not become the default theme is the button rollover. The colour thing does not work nicely with button of different colours."
    author: "blahism"
  - subject: "Re: Plastik updates of 3.3.1"
    date: 2004-11-10
    body: "IMHO the highlighting on mouseover of tabs sucks also... well but apart of that\nplastik is great!"
    author: "ac"
  - subject: "kdevelop extension"
    date: 2004-11-09
    body: "what are they ?\n\nmore details anyone ?\n\n"
    author: "anonymous writer"
  - subject: "Hippies"
    date: 2004-11-09
    body: "Simplify mannn."
    author: "blahism"
  - subject: "digikam 0.7 released"
    date: 2004-11-09
    body: "Hello? Anyone home? digikam-0.7 is there!\nIs is like k3b or konqueror a KDE killer application.\nTry it!\nhttp://digikam.sourceforge.net/Digikam-SPIP/article.php3?id_article=212\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
---
In this <a href="http://cvs-digest.org/index.php?issue=nov52004">week's KDE CVS-Digest</a> (<a href="http://cvs-digest.org/index.php?newissue=nov52004">experimental layout</a>): <a href="http://www.kdevelop.org/">KDevelop</a> adds extension support.
Kommander improves signal and slot editor.
Kwallet is now asynchronous.
<a href="http://developer.kde.org/~wheeler/juk.html">JuK</a> adds support for <a href="http://gstreamer.freedesktop.org/">GStreamer</a> 0.8.
KPasswordDialog adds password strength meter.

<!--break-->
