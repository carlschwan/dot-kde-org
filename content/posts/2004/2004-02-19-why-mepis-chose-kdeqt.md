---
title: "Why MEPIS Chose KDE/Qt"
date:    2004-02-19
authors:
  - "numanee"
slug:    why-mepis-chose-kdeqt
comments:
  - subject: "Disappointed"
    date: 2004-02-18
    body: "No one asked whether he wanted Konsole in the default kicker :)\n\nThere seems to be some very good distros coming along, most based on debian and KDE. Interesting how he is offering services along with the distribution.\n\nDerek\n\n"
    author: "Derek Kite"
  - subject: "Re: Disappointed"
    date: 2004-02-19
    body: "> No one asked whether he wanted Konsole in the default kicker :)\n \nOuch! It never dies. Come on. They've wasted 0.06% of that desktop to support a conclusive 5% of users and quite possibly avoid  eventually overcrowding developer channels as people ask \"what is this button?\" and discover Konsole. It's important to tomorrow's users that we expose them to programs we think we should have been exposed to, not what we actually were. We knew all about the console before we knew anything about the console, which is why we now so much about the console and why we promote it by hiding it in the menu... which is how it should be. Is any of this getting through to you? ;-)"
    author: "Eric Laffoon"
  - subject: "QT Licensing..."
    date: 2004-02-18
    body: "I found the way people point to the QT license and say it's ridiculous. How come it takes sooo long to notice, that for GPL software there are no issues. For developing commercial software, all you have to do is to buy a QT license that will certainly cost fraction of the development costs. If you have a decent bussiness plan it's never going to be an issue!\n\nOne thing that I'm not sure about is how will commercial applications integrate into the KDE desktop? I guess they'll still be able to use DCOP, but how about kparts and so all the embedding and stuff? \n\n\n"
    author: "David Siska"
  - subject: "Re: QT Licensing..."
    date: 2004-02-19
    body: "> I found the way people point to the QT license and say it's ridiculous. How come it takes sooo long to notice, that for GPL software there are no issues. For developing commercial software, all you have to do is to buy a QT license that will certainly cost fraction of the development costs. If you have a decent bussiness plan it's never going to be an issue!\n\nThere's a program called sloccount. Try running it to get source lines of code counts and estimated development costs. I would be surprised if licensing is 1% of development costs and if it makes you 2% faster it's a heck of a deal. More likely it's several percent which is why so many people buy the license. If there was any merit to the stupid argument Trolltech would be out of business instead of prospering. It's a red herring. Worse, because we should support GPL'd software it actually casts the GPL as too restrictive on business... Businesses have access to millions of dollars in investment capital and don't have the same need for protection as your every day user.\n \n> One thing that I'm not sure about is how will commercial applications integrate into the KDE desktop? I guess they'll still be able to use DCOP, but how about kparts and so all the embedding and stuff? \n \nYou can't link to software that's incompatibly licensed with GPL, but pretty much all of the KDE base libraries are either LGPL or another license that makes that not an issue."
    author: "Eric Laffoon"
  - subject: "Re: QT Licensing..."
    date: 2004-02-19
    body: "<intro>i surely don't know all the facts</intro>\n\n> Businesses have access to millions of dollars in investment capital and\n> don't have the same need for protection as your every day user.\n\nthis is true for software developed by lets say adobe or ximian or the like.\nbut on the windows box my dad runs there also is a ton of shareware (most of\nwhich he actually uses). A significant part of the shareware is characterized\nby beeing (a) not gpl compatible (no source - try free and give 10$ later) and\n(b) developed as a hobby in the spare time. thus, when these developers start\ndeveloping for KDE they have two choices: cough up a couple thousand bucks or\nchange their licensing.\n\nnow you tell me what they are going to do.\n\never since Bruce declared the Qt/GPL as non-free, they will probably head over\nfor gtk+/gnome..\n\ni know that these people should use the GPL for ethical/RMS reasons - but they\nare not going to. and suddenly you have blocked off a large number of\napplication developers...\n\nhow are you going to win them over?\nKylix could have been an answer..."
    author: "bangert"
  - subject: "Re: QT Licensing..."
    date: 2004-02-19
    body: "However as it is a frequently asked question I see no website about this issue I can point gtk fanatics to."
    author: "Holler"
  - subject: "Re: QT Licensing..."
    date: 2004-02-19
    body: "Oh, found:\n\nhttp://kdemyths.urbanlizard.com/mythTopic.php?topic=10"
    author: "Holler"
  - subject: "Shareware"
    date: 2004-02-19
    body: "Your point about shareware beeing developed as a hobby in the spare time, you are in most cases wrong. You are creating a non existing problem.  Quality shareware pepople use are not developed by single developers in their spare time. Take a look at what shareware are used (in your case the ton on your fathers computer) you will see programs like WinZip, Paint Shop Pro, CoffeCUP ftp, Ad-Aware and Spam Inspector etc. None of those are developed by hobby developers, but small professional companys. They have in most cases several produkts and probably sound buissness models. I would not say they are different from the Kompany, and I have never heard they have any problems with the cost of Qt. \n"
    author: "Morty"
  - subject: "Re: Shareware"
    date: 2004-02-19
    body: "I would like to see a KDE port of Paint shop!"
    author: "Gerd"
  - subject: "Re: Shareware"
    date: 2004-02-20
    body: "Not only is this a good answer but look at how much penetration you get with shareware. Look at an application like BitTorrent. The author did it for free but then he was hired by a company to do similar work and gets a nice pile of donations on his site. Shareware has to hamper the code to get reasonably high rates of money and lots of people don't want to have their arm twisted and avoid it.\n\nOver 80% of all software is developed under s contract or service model. Lots of people make good money this way. Most proprietary software is a very expensive and risky roll of the dice that you will have an idea you will sit back and collect royalties until you are rich. Getting away from the expensive game you don't see any money at for years and takes deep pockets and into the one you see money tomorrow is highly rational for those who don't have the scratch to play. However if you want to better your lifestyle your odds are better with GPL than shareware."
    author: "Eric Laffoon"
  - subject: "Re: QT Licensing..."
    date: 2004-02-19
    body: "The amount of shareware software available for Qt matches about the one of other toolkits under Linux: Essentially it's non-existant. Most shareware products are leftovers from the early 90s anyways as shareware itself is a rather outdated philosophy these days (it either developed into demo-versions or free software). So this again is a non-issue."
    author: "tackat"
  - subject: "Interviewer is quite ignorant"
    date: 2004-02-19
    body: "asking questions like \":As we know KDE was derived from QT widget which has limited freedom fordevelopers in term of licensing issue on re-distribution. In your opinion,will this limitation be an issue in the future development of KDE?\"\n\nLOL. It's not 1999. get over it. "
    author: "anon"
  - subject: "Re: Interviewer is quite ignorant"
    date: 2004-02-19
    body: "It is interesting that some bad licensing decisions made early on have continued to create image/PR problems for trolltech years after they went to the dual license...."
    author: "Anony Guy"
  - subject: "Re: Interviewer is quite ignorant"
    date: 2004-02-19
    body: "Nah... These days, it's all down to GTK users lobbying against QT, which forces this to come up every single time QT is ever mentioned. They don't like that it forces either commercial or GPL licenses on you. They know that Trolltech aren't going to LGPL it anytime soon, and they are just picking a fight they know they can win.\n\nHonestly, if you want to develop commercial apps, you pay the fee, but in that case, you've got ample money behind you to cover the fees, and it is inconsequential, especially compared to the development time that QT's quality saves you. If you want to develop free apps, then QT helps you do that too, but GPL only, in an effort to support the community.\n\nWhat you can't do is write applications as shareware and sell them for \u00a3100 a pop if not many people will buy them.\n\nUnfortunately, the conversation seems to want to target this market, as if it is somehow important. Having tried to find a simple VB6 FTP component a while back, it must be said that I'm deeply annoyed at having to pay \u00a350 for a simple control that implements the protocol, amid hundreds of other supposedly shareware components. Not a single free one amongst them! And I'm only wanting to upload an image to a server! This is the big tragedy of windows... All non-professional code, you now have to pay for. The free software community spirit really doesn't exist for windows anymore."
    author: "Dawnrider"
  - subject: "Re: Interviewer is quite ignorant"
    date: 2004-02-19
    body: "Exactly; the only arguing point the GNOME folks have left is that YOU CAN WRITE PROPRIETARY SOFTWARE FOR GNOME FREE OF CHARGE.  Isn't that what Free Software advocates are fighting for--free-beer software for proprietary software distributors?\n\nI thought not."
    author: "Shane Simmons"
  - subject: "Re: Interviewer is quite ignorant"
    date: 2004-02-20
    body: "> Nah... These days, it's all down to GTK users lobbying against QT, which forces this to come up every single time QT is ever mentioned. They don't like that it forces either commercial or GPL licenses on you. They know that Trolltech aren't going to LGPL it anytime soon, and they are just picking a fight they know they can win.\n \nBut they can only win this argument shouting loudly at people too clueless to get it. If you are going to devlop a commercial application you need money, pure and simple. Otherwise you will be using ever free hour for the next 3-5years in your bedroom. If you can find a couple other programmers you might be there in 2-3 years. Even at this it will lack the depth and breadth of a really professional application and it will be so late to market, assuming you finish it, that people will almost certainly have superior free software options. If you succeed with all of that you now need tens, if not hundreds, of thousands of dollars to promote it and hope you sell enough to start making a profit.\n\nUnder the above scenario if you saved 5% of 5 years that's 3 months and if you started with Qt and bought the license before you released that would come out to $26.32 a month. Remember you still need thousands more to advertise.\n\nThis argument could not be any more freaking stupid! It also says a lot about how utterly clueless these people feel they need the Qt hating GTK loving peanut gallery to be.\n\nNow answer me this... how many applications have you bought from small independent shops in the last 2 years? How many commercial software programs have you bought in the same time? How many open source programs do you use that are more than good enough?\n\nDo you really actually give a rat's ass about these imaginary business that you have virtually zero comprehension of their real challenges? I thought not. Go back to your corner and look for an \"intelligent\" complaint."
    author: "Eric Laffoon"
  - subject: "Re: Interviewer is quite ignorant"
    date: 2004-02-20
    body: "\" Nah... These days, it's all down to GTK users lobbying against QT, which forces this to come up every single time QT is ever mentioned. They don't like that it forces either commercial or GPL licenses on you. They know that Trolltech aren't going to LGPL it anytime soon, and they are just picking a fight they know they can win.\"\n\nThey can't win this. 'No one' in the world develops any meaningful commercial/proprietary software for free. People pay for this development. As a commercial developer I've never ever understood this. People actually thought that Michael Meeks' presentation on this was FUD and some thought that it made GTK/Mono the correct development environment for business - whatever that is. I just thought that it was bollocks. If I develop proprietary software then that is a business decision for me and I will pay for it.\n\nYes a lot of the shareware/software for Windows is bollocks, but that is why we have a mix of commercial development software where it is good, and good free software to fill the gaps and give us flexibility. It works. All of these little shareware apps are designed to make money for some individuals - nothing more. Many are total rubbish."
    author: "David"
  - subject: "Re: Interviewer is quite ignorant"
    date: 2004-02-19
    body: "You know, it's probably not even a PR problem at all. The more I look at it, the more I think that if Qt was LPGL'ed, then the GTK+ guys would complain about it being C++, or object oriented, or too modular, or whatever the f*ck they'll pull out of their asses this time.\n\nIt is NOT about making rational arguments for/against a tool. I wonder if it has ever been, to be honest.\n\nWhat you see here is kids of various ages growing an emotional attachment to a freaking -tool-, and then going more and more pissy and bitchy as it becomes increasingly obvious that their chosen tool is terminally lagging technologically (and, as it turns out, a significant waste of time).\n\nMoving on. Go back to coding great software and let them whine."
    author: "Anonymous Coward"
  - subject: "Re: Interviewer is quite ignorant"
    date: 2004-02-19
    body: "Where can I find the the GPL-Version of Qt/WINDOWS?\nIs there a GPL-Version of Qt/WINDOWS?"
    author: "tux"
  - subject: "GPL'ed Qt on Windows is available..."
    date: 2004-02-19
    body: "Here: http://kde-cygwin.sourceforge.net/qt3-win32/\n\nIf you'd rather use the Trolltech-provided non-commercial (not GPL per se) edition of Qt 3.2 for Windows, it comes on the CD-ROM that ships with the  \"C++ GUI Programming with Qt 3\" book."
    author: "Anonymous Coward"
  - subject: "Re: GPL'ed Qt on Windows is available..."
    date: 2004-02-19
    body: "Where can I find the OFFICIAL GPL-Version of Qt/WINDOWS?\nIs there an OFFICIAL GPL-Version of Qt/WINDOWS?\n\nAs far as I know the version which comes on the CD-ROM that ships with the \"C++ GUI Programming with Qt 3\" book isn't GPL'ed.\nAnd as far as I know there isn't an OFFICIAL GPL-Version of Qt/WINDOWS at all."
    author: "tux"
  - subject: "Re: GPL'ed Qt on Windows is available..."
    date: 2004-02-20
    body: "The reason there is no current official GPLed QT/Windows is that when it was released last time, QT sales dropped noticably.  People who should have been paying for the commercial version were violating the license.  Trolltech had no way of figuring out who was doing it, so they had to discontinue offering a free downloadable version of QT/Win.\n\nAt least that's what I remember hearing quite some time ago.  If someone has more information I'd be interested to hear it."
    author: "Spy Hunter"
  - subject: "Re: Interviewer is quite ignorant"
    date: 2004-02-19
    body: "GPL version of Qt/Windows is available with the book C++ GUI Programming with Qt 3 see http://www.amazon.com/exec/obidos/tg/detail/-/0131240722/ref=ase_trolltech/002-7637596-9548047?v=glance&s=books "
    author: "George Moody"
  - subject: "Re: Interviewer is quite ignorant"
    date: 2004-02-19
    body: "This is no GPL version."
    author: "Anonymous"
  - subject: "Re: Interviewer is quite ignorant"
    date: 2004-02-19
    body: "- There is actually no GPL version for Windows. So if you want to create cross-platform GPL-apps, you have to use GTK! :-(\n\n- Cygwin-version (which is mentioned above) is no Windows-version but the unix-version with sort of an emulator. (Of course, emulator may not be the right technical term but you get the point, it's NOT native-Windows)"
    author: "Michael"
  - subject: "Re: Interviewer is quite ignorant"
    date: 2004-02-19
    body: "You are misinformed about kde-cygwin. Or at least have old information.\n\nYou are thinking of this:\n\nhttp://kde-cygwin.sourceforge.net/qt3/\n\nBut there's also this:\n\nhttp://kde-cygwin.sourceforge.net/qt3-win32/"
    author: "Roberto Alsina"
  - subject: "Re: Interviewer is quite ignorant"
    date: 2004-02-19
    body: "Looks interesting! I will read more about it, when I have time, thank you for the hint!"
    author: "Michael"
  - subject: "crash?"
    date: 2004-02-20
    body: "Does the TechDOT interview page crash any one else's Konqueror?"
    author: "jb"
  - subject: "Re: crash?"
    date: 2004-02-20
    body: "Yes, see http://bugs.kde.org/simple_search.cgi?id=techdot."
    author: "Anonymous"
---
In a <A href="http://www.techdot.com/doc/0093.html">TechDOT.com interview</a>, <A href="http://www.mepis.org/">MEPIS Linux</a> founder Warren Woodford explains their choice of desktop and toolkit: <i>"The bottom line is that I believe KDE gives a better user experience and that Qt is a better application building framework."</i> No arguing with that.  Take a gander over to their <a href="http://www.mepis.org/image/tid/64">photo gallery</a> and <a href="http://www.mepis.org/project">project</a> sections to see some of the desktop tools they are working on.


<!--break-->
