---
title: "KDE CVS-Digest for December 10, 2004"
date:    2004-12-11
authors:
  - "dkite"
slug:    kde-cvs-digest-december-10-2004
comments:
  - subject: "k3b, Added an animated pixmap to the audio project"
    date: 2004-12-11
    body: "I love to see KinDergartEn 3.4... \n\nPlease, please, please keep this stuff optional."
    author: "Carlo"
  - subject: "Re: k3b, Added an animated pixmap to the audio project"
    date: 2004-12-11
    body: "k3b is not part of KDE"
    author: "Anonymous"
  - subject: "Re: k3b, Added an animated pixmap to the audio pro"
    date: 2004-12-11
    body: "Not yet."
    author: "Carlo"
  - subject: "Re: k3b, Added an animated pixmap to the audio pro"
    date: 2004-12-11
    body: "I'm not aware of any intention to make it that."
    author: "Anonymous"
  - subject: "Re: k3b, Added an animated pixmap to the audio pro"
    date: 2004-12-11
    body: "Even if it will not, blinking and animated stuff is still a pain."
    author: "Carlo"
  - subject: "Re: k3b, Added an animated pixmap to the audio pro"
    date: 2004-12-11
    body: "You don't care about correctness in flaming against KDE 3.4, right?"
    author: "Anonymous"
  - subject: "Re: k3b, Added an animated pixmap to the audio pro"
    date: 2004-12-11
    body: "As long as I have the last kword. ;)"
    author: "Carlo"
  - subject: "Re: k3b, Added an animated pixmap to the audio pro"
    date: 2004-12-12
    body: "Your not gonna get it..."
    author: "pieter"
  - subject: "Re: k3b, Added an animated pixmap to the audio pro"
    date: 2004-12-20
    body: "cause I'm gonna take it!"
    author: "thatguiser"
  - subject: "KEG"
    date: 2004-12-12
    body: "> k3b is not part of KDE\n\nYeah it is, it's in kdeextragear. Apps in keg have their own release schedule but get translated, get docs, and are in CVS.\n\nI'm not the only one who thinks KDE should get more this way. The regular KDE releases should be: kdelibs, kdebase, kdepim, a collection of KIO-slaves and misc. libraries. Everything else should be on a separate release schedule imo.\n\nkde-multimedia would turn into just the codecs, (thus under misc-libraries)\nkde-network can be just the useful kio-slaves (probably more, I'm not very familiar with this module)\n\nThings like kuickshow should go into kdebase, as it is small and very useful.\n\nThings like kpdf, kopete can have their own release schedules.\n\nThe main reason I think like this is because I want all the KIO slaves and I want all the libraries and I want Konq and Kicker. But I don't want everything from the rest. And the apps in keg are the best (K3B, konversation, gwen-view, amaroK), and this is because they have dedicated developers who can dictate their own release schedules.\n\nI've seen this sort of thing discussed on the lists before and usually 2 or 3 vocal people shout it down. Maybe for KDE 4?"
    author: "Max Howell"
  - subject: "Re: KEG"
    date: 2004-12-12
    body: "Jupp. Maybe KDE will be stripped down to KDE core competence.\n\nBut I thinks the way it is now is fine.\n\nKDE is no \"big leap\" development anymore. We see it mature and grow.\n\nWhat counts now is in the application space."
    author: "Bert"
  - subject: "Re: KEG"
    date: 2004-12-12
    body: "i don't claim any greater insight, but i feel this way too...\n\nthere are just too many apps which are not very well maintained. that is no problem in itself, but it lowers the overall quality of the packages (IMHO)\n\nlook at kppp, ksirc, kwikdisk and the like. not that these programs are not usefull, but they either \n - serve a too specific problem to be actively maintained\n - have better* alternatives (either in KDE cvs or outside)\n - or are simply in need of \"some love\"\n\nhaving (large) numbers of lesser supported programs in the main distribution makes the desktop as a whole feel cluttered.\n\nof course, there is always DO_NOT_COMPILE, but i'd prefer sane defaults - one can always add programs on top of a desktop base install.\n\ni know about the argument for the large packages and my experience from the gnome packages has been mixed - but just because that way of handling things has problems of its own doesn't mean that it couldn't be better...\n\nthe aim, as i see it, is to clean up the packages a bit. with the move of kmail and knode to kdepim the kdenetwork package is looking very different and perhaps it is time to rearrange things a bit\n\ni'd expect some selection to happen naturally for KDE4 as many apps will need to be ported over to new API's \n\nand then again i'm only standing here at the sideline...\n\n*YMMV"
    author: "bangert"
  - subject: "Re: KEG"
    date: 2004-12-13
    body: "> I'm not the only one who thinks KDE should get more this way. The regular KDE releases should be: kdelibs, kdebase, kdepim, a collection of KIO-slaves and misc. libraries. Everything else should be on a separate release schedule imo.\n \nI would probably be amicable to that in general. The release schedules have been a challenge for kdewebdev, but my impression is that translators seem to like it like this and distributions that consume KDE like this. Schedules are good for developing urgency but different programs and features develop on different timelines. Primarily I want everyone to be happy and I want to be a team player, but I used to set my own release schedule and I really loved it. I guess I just need to follow mailing lists more closely but finding time seems to be the challenge.\n\nThe chief problem with the collective schedule, along with the expected things that don't make it in, is that we always seem to have something we just get in before a freeze. The fine tuning of the UI ends up going into the next major point release and the cycle continues. Then there are the features that will never fit in the allotted time. However the arguments to the contrary with the potential confusion and dependence on core libraries is not without merit. In fact a feature utopia is a developer dystopia with short and long development branches releasing, which makes the current system look not so bad. If anyone finds a working utopia let me know. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: k3b, Added an animated pixmap to the audio project"
    date: 2004-12-11
    body: "EHi man, the shell is down there, if you want it"
    author: "Davide Ferrari"
  - subject: "mDNSResponder"
    date: 2004-12-11
    body: "> mDNSResponder libraries moved to kdelibs\n\nThat is wrong, they may be used by kdelibs but are not contained in them."
    author: "Anonymous"
  - subject: "Re: mDNSResponder"
    date: 2004-12-11
    body: "Let's rephrase that. Libraries that offer the DNS-SD are now part of kdelibs. They are based on mDNSResponder from apple.\n\nIndeed, the mDNSResponder libraries must be installed separately.\n\nDerek"
    author: "Derek Kite"
  - subject: "CSS2 'text-shadow' support in KHTML"
    date: 2004-12-11
    body: "I'd love to see a screenshot. Anyone? :-)"
    author: "Eike Hein"
  - subject: "Re: CSS2 'text-shadow' support in KHTML"
    date: 2004-12-12
    body: "Try http://www.kdedevelopers.org/node/view/763"
    author: "Carewolf"
  - subject: "Links are a bit optimistic..."
    date: 2004-12-12
    body: "Derek,\n\nNice work, as always. \n\nJust a suggestion: change the regex that does the links to only look for whole words. That prevents things like: cOPIEs, where OPIE is a link to something unrelated. Maybe also try to exclude anything that is inside an open quote, or adjacent to a / (path delimiter).\n\nBrad"
    author: "Brad Hards"
  - subject: "Re: Links are a bit optimistic..."
    date: 2004-12-12
    body: "Everything is fixed. Looks much better :)\n\nDerek"
    author: "Derek Kite"
  - subject: "KNewStuff and kde-look.org"
    date: 2004-12-12
    body: "now knewstuff can download wallpapers from k-l.o , what about iconsets,mouse cursers,splash screens,Color Schemes and Service Menus and anything don't need intallation? it will be a killer feature to integrate kde-look.org and kde together"
    author: "Mohasr"
  - subject: "Re: KNewStuff and kde-look.org"
    date: 2004-12-12
    body: "how about a standart way to integrate websites (webservices) with the desktop ?? anyone ? like kommander programms or kjsembeded apps ?"
    author: "chris"
  - subject: "Re: KNewStuff and kde-look.org"
    date: 2004-12-12
    body: "That standard way already exists. It's called KNewStuff..."
    author: "Henrique Pinto"
  - subject: "Re: KNewStuff and kde-look.org"
    date: 2004-12-12
    body: "yeah - take this to freedesktop.org and share it with the gnomes (and roxers and xfcers)...\n\neverybody likes wallpapers...\n\nat least the spec is something, the desktops could share..."
    author: "bangert"
  - subject: "Re: KNewStuff and kde-look.org"
    date: 2004-12-13
    body: "Themes shared between GNOME, KDE and other DEs would be THE killer feature.\nJust imaginge Crystal on GNOME and Industrial on KDE. Everybody could use his favorite theme on his favorite DE. Hosted on freedesktop-look.org\n\nThere should be done much more together."
    author: "Anonymous"
  - subject: "Re: KNewStuff and kde-look.org"
    date: 2004-12-13
    body: "i was only talking about the downloading of desktop independent theming stuff - like wallpapers and as you mention icons... (window)styles and other stuff involving code is tricky - also because of virus ...\n\nfor icons there exists the icon-theme-spec@freedesktop.org\nhttp://freedesktop.org/wiki/Standards_2ficon_2dtheme_2dspec\n\nAFAIK the gnomes are already following it (I'm not sure), but there still seems to be some confusion about it (see recent post on kdedevelopers.org when it gets back up)\n\nyes, it would be great if icons could be shared across the desktop. it would be totally cool if the icon-theme chosen in the 'home' desktop would be respected by the others\n\nand then a repository serving icons for all desktops (using the fdo-newstuff-spec) could be put up..."
    author: "bangert"
  - subject: "Re: KNewStuff and kde-look.org"
    date: 2004-12-13
    body: "How would it be the \"Killer\" feature?\n\nYeah, even better than a VPN connection or IMAP that works 100% of the time...yeah, keep dreaming. You're the kind of guy who \"can't live\" without ground-effects on his Honda Civic, aren't you?"
    author: "Joe"
  - subject: "Re: KNewStuff and kde-look.org"
    date: 2004-12-13
    body: "I'm trying to convince cartman to use knewstuff for icon themes for konversation.  Only a small thing I know, but it's one small step :)  We just need to figure how to get them on kde-look.org"
    author: "JohnFlux"
  - subject: "whaaaa!"
    date: 2004-12-12
    body: "just tried KDE 3.4 alpha 1, and - amazingly good work! its much faster, and the look of plastik is greatly enhanced. and some features are just sooo cool :D\n\nthanx, kde team!"
    author: "superstoned"
---
In <a href="http://cvs-digest.org/index.php?issue=dec102004">this week's KDE CVS-Digest</a> (<a href="http://cvs-digest.org/index.php?newissue=dec102004">experimental layout</a>):

mDNSResponder libraries moved to kdelibs.
Krdc and Krbc now use DNS-SD.
<a href="http://www.konqueror.org/features/browser.php">khtml</a> improves CSS compliance.
KNewStuff support for wallpapers.



<!--break-->
