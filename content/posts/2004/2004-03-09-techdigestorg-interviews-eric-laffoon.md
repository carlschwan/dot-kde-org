---
title: "TechDigest.org Interviews Eric Laffoon"
date:    2004-03-09
authors:
  - "tparty"
slug:    techdigestorg-interviews-eric-laffoon
comments:
  - subject: "Yes yes..."
    date: 2004-03-09
    body: "But the big question is of course, what does he look like? ;-)"
    author: "Gagnefs Tr\u00e4skodansarf\u00f6rening"
  - subject: "Re: Yes yes..."
    date: 2004-03-09
    body: "See http://quanta.sourceforge.net/main2.php?newsfile=wilwheaton"
    author: "wilbert"
  - subject: "Re: Yes yes..."
    date: 2004-03-09
    body: ">> But the big question is of course, what does he look like? ;-)\n\n> See http://quanta.sourceforge.net/main2.php?newsfile=wilwheaton\n\nI know I'm a funny guy, but looks aren't everything. Ironically I have an interview with Tink coming up and she has pictures of me included with it. So I won't steal her thunder. I'll just send this picture from behind which I hope clears up who has the longest ponytail in KDE. I thought I was the oldest but Kurt Pfeifle snatched that from my grasp by a year. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: Yes yes..."
    date: 2004-03-09
    body: "I am not opposed to coders above 47. Perhaps they could help us with cobol bindings... \n\n:-)\n\n47 does not sound like old guy. \n\nIn software development via internet nobody cares about your age, race, gender."
    author: "Jan"
  - subject: "Re: Yes yes..."
    date: 2004-03-09
    body: "> I am not opposed to coders above 47. Perhaps they could help us with cobol bindings... \n \n> :-)\n \nTry fortran, if I could remember back that far. ;-) \n\n> In software development via internet nobody cares about your age, race, gender.\n\n...except you when you see your picture and say \"I look like that?!? I'm going to the gym now!\" ;-)"
    author: "Eric Laffoon"
  - subject: "Re: Yes yes..."
    date: 2004-03-09
    body: "I am  not a coder, but I have translated for KDE to Danish since 98. \nAnd I am 58\n\nErik"
    author: "Erik Pedersen"
  - subject: "Re: Yes yes..."
    date: 2004-03-09
    body: "> I am not a coder, but I have translated for KDE to Danish since 98. And I am 58\n \nWhen Andras told me he was like an old man at age 27 among KDE developers I thought I might be the oldest. Now I'm just old for the KDE crowd. I think counting translators is only fair. Now I'm not as old on the bell curve thanks to Erik. I submit to your years and salute you for obviously thinking young.\n\nBTW I have confirmed users in their late 60s. One is a retired professor who loves Quanta (and Kitty Hooch).\n\nNow about the ponytail... ;-)"
    author: "Eric Laffoon"
  - subject: "Re: Yes yes..."
    date: 2004-03-09
    body: "Hey, I resemble that remark! My first formal language was Fortran IV. I still have my \"Fortran Coloring Book\"..."
    author: "David Johnson"
  - subject: "Re: Yes yes..."
    date: 2004-03-09
    body: "Ha! I did Fortran III+ on a PDP11.\n\nUsing a paper terminal (ok, one of those and two glass ttys)\n\nAnd that was my third language (basic and 6510 asm were the first two)\n\nSometimes living in a poor(ish) country gives you strange bragging rights :-)\n\nBTW: before someone gets a wrong idea, I am only semi-old, being 32"
    author: "Roberto Alsina"
  - subject: "Re: Yes yes..."
    date: 2004-03-10
    body: "\"I am not opposed to coders above 47. Perhaps they could help us with cobol bindings...\"\n\nThanks, no problem - that sounds someone just like me.. I wonder if I'm the only KDE Cobol expert? I used to work on tools to convert one dialect of COBOL into another. Coding sheets/punched cards/flowchart templates - it was all as crap as it sounds!\n\nThe best COBOL in my opinion was Burroughs '68 - it had some nifty message  passing features - great on an early sixties B5500. It well downhill with the '74 version, and got 'too standard'.\n\n-- Richard, aged 47 and a half. "
    author: "Richard Dale"
  - subject: "Re: Yes yes..."
    date: 2004-03-10
    body: "> -- Richard, aged 47 and a half. \n\nWow! I am so not alone now. You guys are making me feel less odd and more like one of the crowd. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: Yes yes..."
    date: 2004-03-09
    body: "Oops! Just woke up and pushed the wrong button... I thought preview was taking a long time. Anyway, the pictures with Wil were prior to a very serious several month regimen of excercise and diet where I went from (if the digital skin fold caliper is to be believed) 26.9% body fat to 8% body fat. In short I don't look quite the same, and I'm picking up where I left off with even better scientifically supported research in these areas. Over the next several months I will be approaching contest level body composition (at age 47). This is my act of defiance against aging which I will not go gently into.\n\nAnyway, as promised, me from behind... vying for \"longest ponytail\" and \"best lat spread\" both. I'll let Tink present pictures of me from the front. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: Yes yes..."
    date: 2004-03-09
    body: "\"Anyway, as promised, me from behind... vying for \"longest ponytail\" and \"best lat spread\" both. \"\n\nHm, we are close in the longest ponytail contest, but Eric may win soon. But I may win in the (very) long run. ;-)\n\nAndras\n\n"
    author: "Andras Mantia"
  - subject: "Re: Yes yes..."
    date: 2004-03-09
    body: "Mea culpa! I failed to mention Andras and I are tied at 28 inches (71 cm) for the longest strand of hair. However he beat me in 2002 by a lot so mine grows faster. He will beat me in the long run when I get tired of the work of caring for it. Not before we get to hang out together though.\n\nAnyway we've raised the bar for Quanta developers. We need to get a sign made... \"your hair must be at least this long to go on this ride...\" ;-)"
    author: "Eric Laffoon"
  - subject: "Re: Yes yes..."
    date: 2004-03-11
    body: "Well, you just have to wait as I have an interview with Eric as well this weekend on the People Behind KDE. Eric provided me with a good range of really fun photographs. Watch this space for the announcement this weekend. ;-0\n\n--\nTink"
    author: "Tink"
  - subject: "Eric Lafoon"
    date: 2004-03-09
    body: "is a true star of the scene. Quanta and Kdevelop are products where you don't find a proper replacement in the windows world. I mentioned it last week during a kdevelop presentation: It was presented to intrested viewers as a good framework to add more languages, as a kind of work in progress. To outsiders this sounds as if the software was not mature. Talk about your pearls, don't take everything for granted. There is nothing like it in the windows world."
    author: "Gerd"
  - subject: "Re: Eric Lafoon"
    date: 2004-03-09
    body: "eclipse.org? it's even platform independent.. \n\n(or better yet, vim)"
    author: "mark dufour"
  - subject: "Re: Eric Lafoon"
    date: 2004-03-09
    body: "Eclipse? Well, this is no real community project and a little bit limited. It's not quite the same market. "
    author: "Gerd"
  - subject: "Re: Eric Lafoon"
    date: 2004-03-09
    body: "> eclipse.org? it's even platform independent.. \n\nWhich means it's not classically a part of the Windows world doesn't it? There are also a number of people who prefer not to work in Java interfaces. IIRC Zend Studio also uses this and I've had people tell me they were thrilled to be up to speed again with Quanta. Eclipse is an impressive tool, but by being so open and diverse it may well not be near what you'd like for particular development use. Even Quanta suffers a little from this open vs focus syndrome, but by being much more tightly focused on the type of development predominant on the internet it remains a very good choice. It does not compare with Eclipse for Java development, but the key point Gerd was making is about being open. This year the foundation should be complete that would allow Quanta to be able to be modified for this level of support. \n\nSo the question if you're a developer might be if you prefer a fast integrated KDE native solution with it's advantages or an IBM sponsored Java project with it's backing. Note that I am an amoeba compared to an elephant looking at my resources and IBM and we are still having this conversation. Score one for the little guy, getting in the ball park.\n\n> (or better yet, vim)\n\nYou may find this heresy, but I've had many Vim users write me under cover of darkness that they have been converted. They probably can't say so in public for fear of being labled a heretic, but I consider it a substantial accomplishment nonetheless. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: Eric Lafoon"
    date: 2004-03-09
    body: "hm. I don't think they were real vim users.. ;) anyway, I've never used quanta.. but I will give you the opportunity to promote it some more.. :) are you planning on adding support for ktexteditor components? that would be great.. I just tried kdevelop in ideal mode, using the vim ktexteditor.. heh, it was almost like the real vim! :) \n\n\n\n"
    author: "mark dufour"
  - subject: "Re: Eric Lafoon"
    date: 2004-03-10
    body: "> hm. I don't think they were real vim users.. ;)\n\nI can only go by what I saw in the shadowy moonlight and what I heard in hushed whispers. I believe them though... They seemed to know the secret handshake, but it could have just been their fingers contorting to another key combination. ;-)\n\n> anyway, I've never used quanta.. but I will give you the opportunity to promote it some more.. :)\n\nHey, thanks, but remember I warned you... It has led to heresy.\n\n> are you planning on adding support for ktexteditor components? that would be great.. I just tried kdevelop in ideal mode, using the vim ktexteditor.. heh, it was almost like the real vim! :) \n \nActually we're working on implementing KMDI which will make ideal mode support a reality. We have three developers dancing around what they have to do trying to decide who does it, but we are working on foundational elements that will make it easier. We've been gradually moving toward a more editor neutral design so that Kvim would be possible to use, though it may give up a small amount of functionality. Also the Kate guys have been doing some compelling work.\n\nI can't give a date for Vim support in Quanta for sure. We have heard a lot less requests for it lately. I think, depending on developer input, the odds are fairly good it will be implemented this year. It would be very quick if a strong pro Vim developer were to get involved.\n\nOf course if we enable a Vim plug in in Quanta I have to think about how to calm Vim users troubled that they may be addicted to something like an IDE. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: Eric Lafoon"
    date: 2004-03-10
    body: "We are already using KTextEditor, but in some places our code depends on the katepart. There is no deadline nor a real priority for it, but we try to make it editor independent in the near feature. So yes, expect that KVim will be usable in Quanta (hopefully this year). But having the IDEAl (and other KMDI based) mode is a priority and should come in a month or so, but definitely it will be in Quanta 3.3.\n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: Eric Lafoon"
    date: 2005-07-07
    body: "Hi Andras,\n\na year ago you wrote that kvim will be usable as the Quanta editor. \n\nIs it still valid?\n\nMaybe I'm freak, but I edit (mainly PHP scripts) at least 5 times faster in kvim as in kate-like editors.\n\nThe editor concept is the only reason I do not use Quanta as my main development tool.\n\nAm I really alone or I only don't know how to setup kvim in Quanta?\n\nBest regards,\nJozef"
    author: "Ing. Jozef Sakalos"
  - subject: "Re: Eric Lafoon"
    date: 2005-07-07
    body: "KVim has been discontinued ages ago, you should look for its replacement Yzis."
    author: "ac"
  - subject: "Re: Eric Lafoon"
    date: 2005-07-07
    body: "Yes, I've looked at Yzis but it's still far from being usable for serious work.\n\nBut OK, if there is no other possibility I'll wait until the Yzis is usable.\n\nThanks for answer,\n\nJozef"
    author: "Ing. Jozef Sakalos"
  - subject: "Re: Eric Lafoon"
    date: 2004-03-09
    body: "to be perfectly fair, vim is a text editor (and a damn fine one at that, he says glancing at his vim-in-konsole sessions), which is quite different than what Quanta and KDevelop are.\n\nand Quanta and KDevelop are pretty \"platform independent\" as well as they run on any platform KDE runs on, which is quite a few. Eclipse gains it's platform flexibility by using Java, which is available pretty much everywhere these days, in a similar way to how Quanta can run on a number of systems due to KDE libs running just about everywhere these days. personally (note that i said _personally_) i could care less if it runs on MS Windows; if it does, hooray for those stuck in that world.\n\nOT: i have a feeling that Eclipse's popularity has a lot to do with IBM's pushing it hard PR-wise more than anything else =) i'm sure it's a fine product (i don't use IDEs myself), but that's not why it's suddenly become so popular =) it's an interesting experiment in Free Software because we'll see how far old-fashioned PR stunting can go in pushing a project into the mainstream."
    author: "Aaron J. Seigo"
  - subject: "Re: Eric Lafoon"
    date: 2004-03-10
    body: "I guess the initial point was: Wow, these tools, nothing on the market compares with and what KDE supporters on trade fairs talk sounds like the understatement of L.I. (Klaus Wennemann) in the classic movie Das Boot(1981) scene: A few things still need to be fixed. \n\nIntrested outsiders will get: Oh, it is not ready yet."
    author: "Gerd"
  - subject: "Thank you Eric"
    date: 2004-03-10
    body: "That was a good interview, I always like reading these things =)\n\nAlso, I think OSS needs to advrtise much better that they need donations, every popular OSS project should have at least 5% of its base contributing a little."
    author: "Alex"
  - subject: "Is Quanta for me?"
    date: 2004-03-10
    body: "I've been wanting to try out Quanta for a while, but never got to it. Me and my wife have a smallish personal site with mainly a calendar of events and a lot of digital images in various slideshows. It's all done in quite simple PHP with most of formatting done using stylesheets. The stuff is uploaded to a remote webserver using scp (or alternatively checked into CVS and checked out at the remote webserver).\n\nWould Quanta bring us any benefits over using Emacs? Could I import the existing project and just start creating new stuff? I'm not normally too much into IDE:s and stuff like that, but Quanta does sound nice, especially for one not too used to web development. \n"
    author: "Chakie"
  - subject: "Re: Is Quanta for me?"
    date: 2004-03-10
    body: "You work with PHP, CVS and mostly stylesheets. Quanta would bring you :\n\no) the ability to use Cervisia ( KDE's graphical CVS client) from your editor.\no) PHP support. Think PHP function autocompletion, highlighting and a structure overview\no) A powerfull CSS editor (which *does* require some time to get used to, if you ask me :) ). \no) Funky table editor\no) A project manager (so, yes, you could basically import it and move right along)\no) Templating capabilities.\no) Since Quanta builds on KDE, it has access to the KIOSlaves on your system. So by definition you can open files, or even maintain projects via FTP,FISH (that's like SCP), SMB and others.\n\nIt's probably not for everyone, but it's sure worth a try. I've moved from vim to quanta for my web work. I still use vim, when I have to edit something like a configuration file or just need an editor on the console. But for me, Quanta has added enough to use it for web work. I do miss vim keybindings though :).\n\n\n"
    author: "Mathieu Kooiman"
  - subject: "Re: Is Quanta for me?"
    date: 2004-03-10
    body: "Emacs doesn't quite nail that list down - but it gets the main ones.\n\n> the ability to use Cervisia (CVS) from your editor\n\nEmacs has built in vc (version control) mode which works very well with CVS (and I would be suprised if it worked with subversion).  The key bindings are very easy to use once they are known (discovering them is always the fun/hard part in emacs).  I do like Cervisia when I am working on the cvs tree in multiple directories - but I can also do this in vc-dired mode (version control directory edit).\n\n> PHP support.\n\nEmacs and vim would give you plenty of syntax highlighting, and if ctags supports it, you would get the autocompletion as well.  Haven't seen about the structure overview though - sounds very nice.\n\n> KIOSlaves\n\nHmmmm - this one is harder to match.  KIOSlaves are very cool.  Emacs does have built in ftp mode (edit files, manage directories on remote servers).  Samba in the past has been done on a fileside mount point.  SCP - I haven't seen that built into emacs yet.  Well - I guess this is a mute point all now because of the FuseIO framework which lets commandline programs access\n\n> Everything else\n\nYeah - Emacs doesn't have much template support, or a table editor, or a project manager or a css editor.  So these could make life nice.  And Emacs definately won't do WYSIWYG.\n\n\nSeems to be that it depends upon your needs.  There are definite advantages to Quanta.  But it may take awhile (if ever) for some old commandline people to make the switch.  I still know plenty of people who use WordPerfect 5.1 and are faster at it and more productive (in writting simple documents and books at least) than anybody else that I've met using another word processor.\n\nOf course - the commandline people equal about 1% of the enduser population (sorry for the obvious unsubstantiated statistic).\n\nPaul"
    author: "PaulSeamons"
  - subject: "Re: Is Quanta for me?"
    date: 2004-03-10
    body: "Of course - if you read at all about the huge, lengthy list of the amazing things that Quanta can do - then the reasons to switch from emacs to Quanta really start to make the decision easy."
    author: "PaulSeamons"
  - subject: "Re: Is Quanta for me?"
    date: 2004-03-11
    body: "> Seems to be that it depends upon your needs. There are definite advantages to Quanta. But it may take awhile (if ever) for some old commandline people to make the switch. \n\nWow, CLI? Guess what? There is an old saying, \"The pen is mightier than the sword\" but an old samauri saying paraphrased \"pen and sword in accord\". In the windows world GUI is king and every operation is a click away. As you no doubt know this is a recipe to drive you insane many times where repetitive tasks demand the \"sword\" of the CLI to cut quickly through those operations. This doesn't mean that the GUI is in any way bad. It means that GUI and CLI both have strengths and weaknesses. The ideal application \"thinks\" and acts like a hybrid, getting out of the way and offering up the CLI for the tasks that it makes sense for and pulling the strengths of the GUI where it makes sense. The key is exposing strengths and augmenting weaknesses accordingly. \n\nQuanta offers the user programmable user actions that can execute scripts using any language that runs in the shell. These can operate on selected text, editor content or no input and can output to replace the editor content, selected text or place it at the cursor position... even no output or make a new file. Add to this the fact that we're making sure everything inside is exposed via DCOP and you can script using your current file or project directory... we even have Kommander to build dialogs that add scripting. You could quite literraly open Quanta on one desktop, go to another and begin composing documents and performing various tasks in Quanta from konsole. ;-)\n\nQuanta is not at all about being a superior application because the GUI is king. It is about being a GUI application that is superior because it pays much more than a passing homage to CLI and offers the developer an easy integration of the best of these two technologies. \n\nWhy would anyone seriously choose to not be able to choose between them where their strengths serve their needs? ;-)"
    author: "Eric Laffoon"
  - subject: "Re: Is Quanta for me?"
    date: 2004-03-10
    body: "I thank Mathieu for his presentation here. It's a good picture. I think the thing to remember is the focus and background of each. Emacs is very mature and tremendously powerful software. At the same time it has expanded along various paths, not been really specific to the task of web work (actually started prior to public web interfaces) and can be somewhat maddening to learn. I decided to look elsewhere because I knew it would not deal with some aspects of what I wanted with specificity and that I would always be slapping my forehead over finding something else it would do better that was lost in the myriad of things it's strapped on over 20+ major revisions.\n\nQuanta was designed specifically for me to address what I needed to do to get my work done as a professional web developer and now what I need to do work on my personal sites. There has been a lot of good input from others for related tasks. In all cases the assumption is that I don't know everything and never will but Quanta will help me look as if I do. Software should naturally extend your efforts, enable you to achieve specific tasks with loose general knowledge and help you to better master your tasks by exposing you to usefull information that expands your essential knowledge, without creating a burden in the process. This is because we don't want to read the book before taking step one.\n\nSo to expound on Mathieu's points...\n> o) PHP support. Think PHP function autocompletion, highlighting and a structure overview\n \nThis includes a structure tree that is much like a DOM tree for HTML. However you can right click and show PHP groups which will show classes, objects, functions and variables. It will also show these within linked files and enable you to open them as well as use their functions and variables in auto completion. Frankly auto completion is worthless to me if I can't modularize my design and am forced to build hideous monolithic disaster zones copying functions all over and editing them. Auto completion also respects variable scope. Inside or outside a function you see what you're supposed to see. The groups in the tree enable an overview. More functionality is on the way.\n\n> o) A powerfull CSS editor (which *does* require some time to get used to, if you ask me :) ). \n \nReview the interview section on CSS now that it's edited. I know why Mathieu thinks it takes getting used to and on the same issue I grasped it instantly. It may not have been Vim like enough for him. ;-)\n\n> o) Funky table editor\n \nFunky because Mathieu is our \"ultimate tester\" and found that it chokes on bad HTML as did I. However it will allow you to recursively edit sub tables and add rows and columns anywhere in the table so it is a very cool improvement. It's currently being worked on to improve it and make it less sensitive to editing broken content.\n\n> o) A project manager (so, yes, you could basically import it and move right along)\n \nThe project manager will allow you to edit numerous files and hit upload and verify which files, but it will present only those changed since last upload. Quanta also has preview for HTML but using the project facilities it is possible to preview at an http address. I use this by placing my project in my server path and this means I can edit and live preview PHP/Mysql pages in Quanta. (I did just find a bug where the cached version continues to show and I'm not sure how long this has been an issue but it's fixed in CVS.)\n\n> o) Templating capabilities.\n \nFiles and code snippets. It does linked templates too but we need to modify the upload process to include linked templates.\n\n> o) Since Quanta builds on KDE, it has access to the KIOSlaves on your system.\n\nThe fish protocol is ssh/scp. You can also add \"top directories\" to the files tree which means that you can view and work with any of your sites from this tree. You can also open your project remote if you choose to create a remote project and specify the kio slave to use. You can open and save files via kio to anywhere as well as specify the protocol for upload. Quanta also allows you to store your password for convenience if you system is secure.\n\nWhat Mathieu didn't mention was some of our support applications like KFileReplace, KImageMapEditor (CVS version), and Kommander which enable you to visually create dialogs for custom functionality. There's a lot to like.\n\n> It's probably not for everyone, but it's sure worth a try.\n\nWhile we focus on serious developers we try to accommodate all experience levels by making docs available which provide context sensitive help. These include HTML, PHP, CSS, Javascript and MySql. We will continue to work on diverse user support."
    author: "Eric Laffoon"
  - subject: "INTERVIEW UPDATE - CSS"
    date: 2004-03-10
    body: "Hi all,\nif you were confused of frustrated reading the part of the interview about the CSS where dialogs were referenced but not linked... Travis was a bit confused when he was working on it at 3 AM. It's been corrected now. So if you want to see all nine images linked as intended they are accessable now.\n\nStay tuned to Quanta development info because there's more exciting development with our CSS tools coming soon!"
    author: "Eric Laffoon"
---
Recently <a href="http://www.techdigest.org/">TechDigest.org</a> put their creative thinking into the interview format by asking  people to submit questions which could be part of an interview. The results take the <a href="http://quanta.sf.net/">Quanta</a>/kdewebdev project leader from childhood tech memories to what's new in Quanta and points between. If you're curious about Quanta, where it's going or what's on Eric's mind this is a good read. If you would like to some hints about some exciting possibilities to see even greater growth in Quanta development to benefit users and attract web developers to KDE then <a href="http://www.techdigest.org/modules.php?op=modload&name=News&file=article&sid=384&mode=thread&order=0&thold=0">grab the interview</a>! 

<!--break-->
