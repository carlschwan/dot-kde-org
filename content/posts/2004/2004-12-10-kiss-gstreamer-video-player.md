---
title: "Kiss GStreamer Video Player"
date:    2004-12-10
authors:
  - "numanee"
slug:    kiss-gstreamer-video-player
comments:
  - subject: "hmm?"
    date: 2004-12-10
    body: "Why cut and past when it is just fine?\n\nWhat's wrong with Kdevelop?"
    author: "gerd"
  - subject: "Re: hmm?"
    date: 2004-12-10
    body: "1. Maybe because you want to reuse the code?\n2. Who said there was anything wrong with KDevelop?"
    author: "Navindra Umanee"
  - subject: "2.8MB overhead? "
    date: 2004-12-10
    body: "Wait a second, 23 K of code and 28 000 K of overhead? What the hell?"
    author: "KOffice fan"
  - subject: "Re: 2.8MB overhead? "
    date: 2004-12-10
    body: "I think u mean 2800 KB of overhead"
    author: "Zaheer"
  - subject: "Re: 2.8MB overhead? "
    date: 2004-12-10
    body: "The price of source code portability, I suppose?  Ever wonder how all that \"./configure; make; make install\" magic happens? *shrugs*\n<p>\nIt's been like this for a while, try looking at the source code for <a href=\"http://www.area.com/ntucker/ky/\">Ky</a>, for instance.\n\n"
    author: "Navindra Umanee"
  - subject: "Re: 2.8MB overhead? "
    date: 2004-12-10
    body: "The 2800k overhead are lots of configure and makefile magic implemented in a generic way to cover most, if not all and then some ways to get the build to work on different systems. You could write the configure and makefiles from scratch and the resulting overhead would become negligible, but the time you  end up using maintaining and debugging the buildsystem for different targets are time you could spend writing code for the application. Or you could remove unneeded parts from the generic configure/makefile's, but the cost are again time. You trade size of the source tree against development time, since this has no effect on the compiled app it's a good solution.  "
    author: "Morty"
  - subject: "Re: 2.8MB overhead? "
    date: 2004-12-10
    body: "Weird. A PyQt app works on just as many systems and has about 500 bytes of overhead (the setup.py script). C/C++ really is lame in this area."
    author: "Roberto Alsina"
  - subject: "Re: 2.8MB overhead? "
    date: 2004-12-10
    body: "Hey, this is not a fair comparison. And since you usually have to write 2-10 times the lines of code in C/C++ compared to Python to do the same thing, it's not fair at all:-)\n"
    author: "Morty"
  - subject: "Re: 2.8MB overhead? "
    date: 2004-12-10
    body: "But if PyQt binding uses autoconf/automake (I don't remember, I'm not a Python fan) then it have that large overhead itself, so indirectly you still have it.\nNot to mention the overhead in the build system of KDEBase/Libs/...\n\nProbably the only solution could be a \"runtime\" for configure, so most of its stuff is on its own package and not in the source tarballs. But there are a lot of people (not me) claiming HD/RAM/BW are cheap and wanting for self-contained packages, so I'm not sure how such idea could be taken."
    author: "Shulai"
  - subject: "Re: 2.8MB overhead? "
    date: 2004-12-10
    body: "No, sorry, you can't make both arguments.\n\nYes, PyQt (and Python) have some overhead like that. But a PyQt app doesn't.\n\nIf you want to make the argument that it does, then you can't make the argument about the \"runtime\" because what python gives you is exactly that (distutils) :-)"
    author: "Roberto Alsina"
  - subject: "Re: 2.8MB overhead? "
    date: 2004-12-10
    body: "You are going too far, the point was about applications not the whole framework. Folowing your reasoning you forgot to mention the buildsystem for X, the compilers and the kernel, and that's ridiculous. The requirements for installing an application either binary or from source, are that the needed infrastructure are in place, library's etc. Anything concerning install of those have no bearing on this case, as they are already installed. \n\nBased on the quality they now have I consider the Python, Ruby and Javascript packages from kdebindings as an integral part of KDE, and I rate it as a bug if they are not included in the distribution. "
    author: "Morty"
  - subject: "Re: 2.8MB overhead? "
    date: 2004-12-10
    body: "That's because PyQt needs python to be installed already.  Automake/Autoconf relies on only a few tools which are preinstalled on almost every Unix system (like HP/UX, Solaris, AIX).  However these tools are implemented differently on each Unix, so Automake/Autoconf has to bend over backwards to be compatible with everybody.  Plus it has the capability to check several hundred compatibility issues that are different between Unixes (plus your own checks that you define) so that your source code can be compatible.  Python is the same everywhere so PyQt does none of that.  It would be nice if C was the same everywhere too, but for historical reasons every Unix is different.  Luckily Automake/Autoconf are here and do the compatibility work for you, at the price of a couple of MB of automatically-generated files.  It is a good trade-off."
    author: "Spy Hunter"
  - subject: "Re: 2.8MB overhead? "
    date: 2004-12-10
    body: "The auto* files are hardly autogenerated. Every line of your configure file was written by someone as a macro. Also, pretty much noone understands the things, so if you need to test for, say, libmikmod, you just don't, and it seems to be portable, but isn't.\n\nAs for auto* requiring only a few tools... well, that's their problem. The way they are written sucks. \n\nMind you, I have nothing better to offer, except maybe publishing programs in ways that force platform vendors to wake up and offer a decent platform to work on, but that's just wishful thinking.\n\nPython being the same everywhere is a python *plus* as a platform. C and C++, as a platform, don't have it. There comes my point about C and C++ being lame in that area."
    author: "Roberto Alsina"
  - subject: "Re: 2.8MB overhead? "
    date: 2004-12-10
    body: "BTW: here's how you test for PyQt on a setup.py:\n\ntry:\n  import qt\nexcept ImportError:\n  print \"You need to install PyQt\""
    author: "Roberto Alsina"
  - subject: "Re: 2.8MB overhead? "
    date: 2004-12-12
    body: "Of course \"The auto* files are hardly autogenerated\", is python autogenerated? The executables of your c(++) code. They ARE autogenrated, after you manually give some command. So first define autogerated, but then still this is a silly discussion, because you are making a comparrsion between two different things that are made for different goals. When c++ was invented phyton was just a snake I guess. So many people know c++, so its a waste of time, for a lot of them (they can make good money with this knowledge) to learn python. So python is newer, so it SHOULD be better. Else it was a lame job too invent it at the first place. Too give all those good c++ programmers a more easy job by publishing and share there great ideas and work with everybody on a lot of different systems, designed for a lot of different tasks, auto* was made. Very handy, with unfortanatly a lot of overhead. Owh my .... Lets start a disgussion about it. because. WE python guys don't have this overhead. Whoopsy we are much better. Well in that area. okay you made your useless point. A ferrary IS faster but cannot transport the milk from hunderd cows. Whow the milk tank is slow, but hey it CAN transport a lot of milk. Thats why they are both there. So if you compare those two completlye different things, compare them fully, test them, write a report and publish it :) Or just code good programs :).\n\nWell anyway, it stays fun to make all this stupid replies :).\n\nAuke"
    author: "Air"
  - subject: "Any C++ bindings?"
    date: 2004-12-10
    body: "Will anyone be creating C++/Qt bindings to GStreamer for KDE 4, similar to what's happened with D-BUS? Judging by Gnome its a powerful backend, but it doesn't seem to mesh perfectly with KDE style code.\n\nAlso, does anyone know why is the author using greater than and less than instead of plain old equals when querying the GStreamer state? I'm not saying this is wrong, I've just never used GStreamer before and I'm curious to know what's going on there."
    author: "Bryan Feeney"
  - subject: "Re: Any C++ bindings?"
    date: 2004-12-10
    body: "The greater thans are there for no real reason. You could replace them with equals and it'd work just as well (state changes are guaranteed to be one-level at a time). I guess I use greater thans to make the purpose of the code (as in: checking a state shift) somewhat more clear."
    author: "Ronald S. Bultje"
  - subject: "Thanks"
    date: 2004-12-10
    body: "Wow, could be a nice starting point for kmplayer having a gstreamer backend too ... only get seg faults in KissWindow::open... ah I should use the File dialog (silly me, the mac menubar).. hmm no video only sound (mplayer says\n[mpeg4 @ 0x84857a8]looks like this file was encoded with (divx4/(old)xvid/opendivx) -> forcing low_delay flag).\n\nBtw. anyone know if debian's gstreamer-0.8 from testing is crippled or not (eg. can it play avi and such) This is what I have\n$ apt-show-versions |grepgstreamer\nlibgstreamer-plugins0.8-0/testing uptodate 0.8.5-1\ngstreamer0.8-a52dec/testing uptodate 0.8.5-1\ngstreamer0.8-oss/testing uptodate 0.8.5-1\ngstreamer0.8-hermes/testing uptodate 0.8.5-1\ngstreamer0.8-alsa/testing uptodate 0.8.5-1\ngstreamer0.8-vorbis/testing uptodate 0.8.5-1\ngstreamer0.8-sdl/testing uptodate 0.8.5-1\ngstreamer0.8-dv/testing uptodate 0.8.5-1\ngstreamer0.8-dvd/testing uptodate 0.8.5-1\ngstreamer0.8-esd/testing uptodate 0.8.5-1\ngstreamer0.8-mpeg2dec/testing uptodate 0.8.5-1\ngstreamer0.8-jpeg/testing uptodate 0.8.5-1\ngstreamer0.8-doc/testing uptodate 0.8.7-1\ngstreamer0.8-speex/testing uptodate 0.8.5-1\ngstreamer0.8-swfdec/testing uptodate 0.8.5-1\ngstreamer0.8-mikmod/testing uptodate 0.8.5-1\ngstreamer0.8-flac/testing uptodate 0.8.5-1\nlibgstreamer-gconf0.8-0/testing uptodate 0.8.5-1\nlibgstreamer0.8-dev/testing uptodate 0.8.7-1\ngstreamer0.8-jack/testing uptodate 0.8.5-1\ngstreamer0.8-cdparanoia/testing uptodate 0.8.5-1\ngstreamer0.8-x/testing uptodate 0.8.5-1\ngstreamer0.8-sid/testing uptodate 0.8.5-1\ngstreamer0.8-caca/testing uptodate 0.8.5-1\ngstreamer0.8-gsm/testing uptodate 0.8.5-1\ngstreamer0.8-plugin-apps/testing uptodate 0.8.5-1\ngstreamer0.8-plugins/testing uptodate 0.8.5-1\nlibgstreamer-gconf0.8-dev/testing uptodate 0.8.5-1\ngstreamer0.8-gnomevfs/testing uptodate 0.8.5-1\nlibgstreamer0.8-0/testing uptodate 0.8.7-1\nlibgstreamer-plugins0.8-dev/testing uptodate 0.8.5-1\ngstreamer0.8-misc/testing uptodate 0.8.5-1\ngstreamer0.8-artsd/testing uptodate 0.8.5-1\ngstreamer0.8-aa/testing uptodate 0.8.5-1\ngstreamer0.8-festival/testing uptodate 0.8.5-1\ngstreamer0.8-tools/testing uptodate 0.8.7-1\ngstreamer0.8-mad/testing uptodate 0.8.5-1\ngstreamer0.8-audiofile/testing uptodate 0.8.5-1\ngstreamer0.8-theora/testing uptodate 0.8.5-1"
    author: "koos"
  - subject: "Re: Thanks"
    date: 2004-12-10
    body: "Hi Koos, you want to use at least gst-plugins 0.8.6 (as the <a href=\"http://ronald.bitfreak.net/me/kiss.php\">website</a> also mentions). 0.8.5 will work for audio, and with a small hack it'll work for video too, but I don't want those hacks in example code. ;-). The hack is that you need a different property for getting the video size in 0.8.5 than in 0.8.6. With current code and 0.8.5, Kiss will always claim that the video is 0x0 pixels, and thus not show the video window (user experience: no video). For playback of divx, you will additionally want gst-ffmpeg. This package is not yet available in Debian's main archives, but it's available in several unofficial repositories. Ask the debian people for details. RPMs for both gst-plugins-0.8.6 and gst-ffmpeg are available all over the internet.\n<p>\nIf you want to add a backend to kmplayer, please do. I'll try to help if I can.\n"
    author: "Ronald S. Bultje"
  - subject: "Re: Thanks"
    date: 2004-12-10
    body: "Got it working now for a MPEG-PS stream w/ 0.8.6 debian/unstable. Does crash on exit but that's for kmplayer no problem :-) FFMpeg can wait .. Did need to resize the window a bit for the video actually stayed, but I had that with Xine too.\nGive me a few days and I'll have something to start with.\n "
    author: "koos"
  - subject: "Re: Thanks"
    date: 2004-12-10
    body: "It would be nice if kaffeine added this functionality as well. It already has multiple backends (xine/arts/netscape) so adding another plugin should be easier than with kmplayer."
    author: "mikeyd"
  - subject: "Re: Thanks"
    date: 2004-12-11
    body: "While I'm a fan of Kaffeine and not as much KMPlayer, this is totally inaccruate.  Just read the first line on their page \n\nhttp://www.xs4all.nl/~jjvrieze/kmplayer.html\n\nIt supports multiple backends (MPlayer/Xine/ffmpeg/ffserver/VDR) -- I think even more than Kaffeine.\n\n-//--- standsolid --->>"
    author: "standsolid"
  - subject: "Re: Thanks"
    date: 2004-12-11
    body: "Kaffeine support kmplayer kpart so ... :)"
    author: "gnumdk"
  - subject: "Re: Thanks"
    date: 2004-12-11
    body: "Ok, you're right. It didn't have them last time I looked - but then neither did kaffeine at that time."
    author: "mikeyd"
  - subject: "Re: Thanks"
    date: 2004-12-12
    body: "Easier is hard to measure, no? For me it's obviously easier to add it to kmplayer. Actually it already works, but only for a local file."
    author: "koos"
  - subject: "Time for a standard"
    date: 2004-12-10
    body: "More and more applications arise that use a the backend (GStreamer, NMM, ...) directly and in its own way.\nIt think its time for a KDE standard way, a abstraction layer, so that there are not too many KDE application which are fixed on one backend. Me, for example would prefer using NMM.\nThe abstraction layer should be agnostic of the multimedia backend used.\n\nJust my two \u0080-Cents...\nFlorian"
    author: "Florian"
  - subject: "Re: Time for a standard"
    date: 2004-12-10
    body: "If you would have read any off the kdemultimedia discussion or summaries of the last week you would know that such a layer for basic audio functionality is planned."
    author: "Anonymous"
  - subject: "Re: Time for a standard"
    date: 2004-12-10
    body: "I know that its planned.\nBut planning is just the first step. What I wanted to say it that they need to hurry with planning."
    author: "Florian"
  - subject: "Re: Time for a standard"
    date: 2004-12-10
    body: "But not for complicated audio funcionality. The layer that will exist will be suitable for things like the track playing preview thingy in K3b, and possibly also in juk. It won't be suitable for a dedicated media player. For that gstreamer looks (to me) to be the best option, but what would be nice is an option \"approved\" by kde. (of course gstreamer has the approval of the, ah, \"other people\")"
    author: "mikeyd"
  - subject: "Re: Time for a standard"
    date: 2004-12-11
    body: "I think the major disadvantage of GStreamer is the missing network transparency, which is included in NMM."
    author: "Florian"
  - subject: "Re: Time for a standard"
    date: 2004-12-11
    body: "True, but the time needed to add network transparency to GStreamer would be smaller (15 days of work to design and implement NMM style or better network transparency was our estimate when we discussed it last) than the time needed to create all the GStreamer plugins etc. for NMM."
    author: "Christian Schaller"
  - subject: "Re: Time for a standard"
    date: 2004-12-11
    body: "Seriously, does a video player need a network-transparent multimedia backend? Are you seriously going to play this video in a distributed network fashion?"
    author: "Ronald S. Bultje"
  - subject: "Re: Time for a standard"
    date: 2004-12-11
    body: "I am not talking about the backend for this specific player but the standard backend used for all KDE applications."
    author: "Florian"
  - subject: "Re: Time for a standard"
    date: 2004-12-11
    body: "KDE is often deployed on thin-clients.  The applications run on the server and display on the thin-client.   It wouldn't do to play Kiss on the server and have the sound play *on the server*.  The sound has to play on the client."
    author: "ac"
  - subject: "Re: Time for a standard"
    date: 2004-12-12
    body: "The A is before the D, retard, first, get gstreamer working well with the KDE enviroment and ask for network transparency later.\n\n"
    author: "..."
  - subject: "Re: Time for a standard"
    date: 2004-12-11
    body: "yeah, its called gstreamer."
    author: "Salsa King"
  - subject: "Re: Time for a standard"
    date: 2004-12-11
    body: "people make you think that but it's not really the case (yet)."
    author: "mETz"
  - subject: "configure script error"
    date: 2004-12-10
    body: "configure script doesn't insert the CXXFLAGS for gstreamer and kde include paths."
    author: "MrPeacock"
  - subject: "Re: configure script error"
    date: 2004-12-10
    body: "Correct, it sets CPPFLAGS instead. Requires automake >= 1.7, though, won't work with 1.6 (which you appear to be using). I've disted using 1.7, so you shouldn't see that unless you re-ran make -f Makefile.cvs."
    author: "Ronald S. Bultje"
  - subject: "gstreamer"
    date: 2004-12-10
    body: "Now if only GStreamer wouldn't crash and burn on startup, I could try some of these new KDE applications. But since the aRTs based apps work just fine, I don't feel any pressing need to debug GStreamer..."
    author: "Brandybuck"
  - subject: "Re: gstreamer"
    date: 2004-12-10
    body: "Exactly what I was thinking every time I tried gstreamer and lets wait until it's robust first. But this is a nice gift to at least start with it. Remember xine was also a crashing beast not long ago."
    author: "koos"
  - subject: "huh, not Totem-like"
    date: 2004-12-10
    body: "I mean, if Totem had as many feature as this example application, nobody, not even me, would use it."
    author: "Bastien Nocera"
  - subject: "Re: huh, not Totem-like"
    date: 2004-12-10
    body: "I think got that description from the original link submission to this site, and I guess Ronald himself described it as Totem-like at some point, although he did say \"simple version\". Sorry.  :)"
    author: "Navindra Umanee"
  - subject: "Re: huh, not Totem-like"
    date: 2004-12-11
    body: "Well, I don't think that the classic Gnome app has got so much more features then this code example app <grin>"
    author: "Davide Ferrari"
  - subject: "Re: huh, not Totem-like"
    date: 2004-12-13
    body: "Find me any movie player other than Totem that has a telestrator mode!"
    author: "Bastien Nocera"
  - subject: "Re: huh, not Totem-like"
    date: 2004-12-14
    body: "Is it your mission to troll other news sites and bash anything that can play video?"
    author: "Distro Whore"
  - subject: "Re: huh, not Totem-like"
    date: 2004-12-14
    body: "Where did you see bashing? Buy goggles."
    author: "Bastien Nocera"
  - subject: "Re: huh, not Totem-like"
    date: 2004-12-16
    body: "Your hostility is uncalled for and out of line, if I may say so."
    author: "ac"
  - subject: "another KISS video player"
    date: 2004-12-13
    body: "Another KISS video player is codeine  http://www.kde-apps.org/content/show.php?content=17161\n\nIt uses Xine as its backend. \n\nI still just use CLI mplayer. 9 times out of 10 any GUI just gets in the way for me, though I usually use something like kmplayer for DVDs."
    author: "Ian Monroe"
  - subject: "problem with the configuration script"
    date: 2008-01-03
    body: "i'm trying to compile the kiss project but the configure script keeps failing\n\nits says i dont have gstreamer version 0.8 or higher although i installed gstreamer 0.10.\n\nwhat could be the problem?"
    author: "daniel"
---
As part of the <a href="http://gstreamer.freedesktop.org/">GStreamer</a> <a href="http://www.advogato.org/person/rbultje/diary.html?start=63">invasion of KDE</a>, <a href="http://ronald.bitfreak.net/">Ronald Bultje</a> has implemented <a href="http://ronald.bitfreak.net/me/kiss.php">Kiss, a simple version of a Totem-like video player for KDE</a> with a GStreamer-backend (<a href="http://ronald.bitfreak.net/images/kiss.jpg">screenshot</a>).  I took a look at the <a href="http://ronald.bitfreak.net/priv/kiss-0.0.1.tar.gz">source code</a> and it looks fairly simple -- it's actually 23K worth of well-separated code in all, with the other 2.8M being makefile/configure overhead. Even if one doesn't understand the GStreamer parts, you could probably copy and paste them into your own KDE application.  (Incidentally, this application seems to have been made with <a href="http://www.kdevelop.org/">KDevelop</a>, for those of you who need inspiration.)














<!--break-->
