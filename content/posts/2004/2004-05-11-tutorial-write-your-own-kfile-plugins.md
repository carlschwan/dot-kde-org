---
title: "Tutorial: Write your own KFile Plugins"
date:    2004-05-11
authors:
  - "bhards"
slug:    tutorial-write-your-own-kfile-plugins
comments:
  - subject: "Some simple plugins need maintained"
    date: 2004-05-11
    body: "Plugins to display information on BibTex, Python, Java, and LyX files are available at <a href=\"http://dogma.freebsd-uk.eu.org/~grrussel/\">http://dogma.freebsd-uk.eu.org/~grrussel/</a> . The code is small, simple and in need of a maintainer.\n<br>\nIn general, textual formats are easy to support in konquerer plugins!\n\n"
    author: "George Russell"
  - subject: "Re: Some simple plugins need maintained"
    date: 2004-05-11
    body: "I've been using these for ages, they're very useful.  \n\nI'd assumed they'd be in kdeaddons by now, is there any reason they are not?"
    author: "Will Stephenson"
  - subject: "Re: Some simple plugins need maintained"
    date: 2004-05-11
    body: "I would be willing to do the work needed to bring these plugins into some KDE module (kdegraphics?). It seems a waste of time maintaining these modules outside the main source. If the code is not in the main distribution many users would not use the plugins (or even know they existed).\n\nGeorge if you want me to do this, contact me by email.\n"
    author: "Pieter"
  - subject: "Re: Some simple plugins need maintained"
    date: 2004-05-11
    body: "Two reasons; the Java / Python plugins are hacks that do not parse Java / Python syntax but scan for keywords.\n\nSecondly, the code has never been cleanly packaged and I no longer use KDE (its not up to scratch on OSX yet)."
    author: "George Russell"
  - subject: "Searching based on meta info?"
    date: 2004-05-11
    body: "Thanks for the tutorial, Brad :] Docs like that are more valuable\nfor KDE than the same amount of C++-code.\n\nWouldn't it be possible to use the meta info data for searching?\nKDE should really provide an index based search function. The meta\ndata plugins could be utilized to collect data from non-textual files,\nlike jpeg with exif data, pdf etc. Many they could also be used for \nfull-text indexing.\n\nIn the end it should be something like an extended version of 'locate',\nwhich is fast enough to use it just because one is too lazy to click\nthrough several folders and with a UI not much more complex as google.\n"
    author: "Fred Sch\u00e4ttgen"
  - subject: "Re: Searching based on meta info?"
    date: 2004-05-11
    body: "KFind already allows you to search the metainfo sections."
    author: "Anonymous"
  - subject: "KPlayer 0.5 comes with a KFile plugin"
    date: 2004-05-11
    body: "For the record, KPlayer 0.5, which is due out in just a few weeks, will come with a KFile plugin that shows metadata for any media file playable with MPlayer. One minor drawback is that it will only show metadata for file that have already been opened with KPlayer. http://kplayer.sf.net/"
    author: "kiriuja"
  - subject: "Great Tutorial"
    date: 2004-05-11
    body: "Hi,\n\nThis is a very good tutorial and I hope it brings lot's of new file plugins to KDE. \n\nMaybe you could add in the future a chapter how to use KFile plugins in your own application. For example my application, KRename (http://www.krename.net), uses KFile plugins to retrieve various meta informations from all kinds of files and remames them using this pieces of information. With little code I was able to add support for lot's of different file types.\n\nCU Dom"
    author: "Dominik Seichter"
  - subject: "Re: Great Tutorial"
    date: 2004-05-11
    body: "I'll look to add more usage details (on KFile and KFind, as they relate to the meta-data stuff) in a future version."
    author: "Brad Hards"
  - subject: "Metadata in Tooltips"
    date: 2004-05-11
    body: "Speaking of metadata, wouldn't it be nice if tooltips separated generic from metadata info somehow? Maybe a small horizontal line between both, or each inside a rectangle? Or at least some blank space?\n"
    author: "John Metadata Freak "
  - subject: "OT: KDE-Pim Outlook Connector?"
    date: 2004-05-11
    body: "Given the release of Ximian's Outlook Connector under the GPL (http://lwn.net/Articles/84494/), are there any plans to add Outlook/Exchange support to KMail and Kontact?\n\nJust wondering...\n"
    author: "Anonymous"
  - subject: "Re: OT: KDE-Pim Outlook Connector?"
    date: 2004-05-11
    body: "Use the Exchange-server-resource of KDEPIM:\n\nls kdepim/kresources/\nCVS  Makefile  Makefile.am  Makefile.in  egroupware  exchange  imap\n\nI don't know how well evo-exchange-plugin works, but I think kdepim could benefit/learn from there implementation.\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "OT: Novell To Release Ximian Connector Under GPL"
    date: 2004-05-11
    body: "Novell To Release Ximian Connector Under GPL\n\nThe code is here:\nhttp://ftp.ximian.com/pub/source/evolution/ximian-connector-1.4.7.tar.gz\n\nAm I the only one who thinks that Kontact needs to incorporate this?"
    author: "ac"
  - subject: "Re: OT: Novell To Release Ximian Connector Under GPL"
    date: 2004-05-11
    body: "> Am I the only one who thinks that Kontact needs to incorporate this?\n\nI think the kontact developers are already working on exchange support, and this will no doubt help them.,"
    author: "anon"
  - subject: "KDE PIM"
    date: 2004-05-12
    body: "I need to things to become a happy KDE-full-time user.\n\na) A descent client/server groupware application. I'm wondering, why KDE PIM has no connector for OpenGroupware or Exchange (even if we hate it, it works)\nb) A way to synchronize my Pocket PC running on Windows Mobile. I know there is synchronisation for Palm, but Pocket PC's are on the market and their market share is quite big.\n\nFrom the day on, when KDE PIM can do groupware I'm switching. On the day, I get my Pocket PC synchronizing, I'm free. (All my Oracle tools work already under Linux :))"
    author: "Art"
  - subject: "Registering multiple mime types"
    date: 2004-05-13
    body: "If I am writing a KFile plugin for say Java .class files, there are multiple mime types in use for them - application/java, application/java-byte-code, application/java-vm, application/x-java-class, application/x-java-byte-code - to name a few. \n\nHow do I register all of them as associated with the same plugin class? Is it by multiple calls to addMimeTypeInfo?\n\nHow about the .desktop file? Do I add multiple MimeType entries there?\n"
    author: "Arun"
  - subject: "Re: Registering multiple mime types"
    date: 2004-05-17
    body: "I figured out the answers to the two questions myself. \n\nIn the .desktop, all the mime types need to be in the same MimeType entry like this:\nMimeType=application/java;application/java-byte-code;application/java-vm;application/x-java;application/x-java-class;application/x-java-byte-code\nI believe regular expressions are also allowed.\n\nIn the plugin class, multiple calls should be made to addMimeTypeInfo for each type and all the groups, items need to be added to each KFileMimeTypeInfo.\n\nI have a small KFile plugin for .class files at http://arun.homeip.net/kdev/kfile_class_src.zip if anyone is interested."
    author: "Arun"
---
If you've ever wanted to get into KDE coding, but reading tens of thousands of lines of code to get familiar with one of the bigger programs was more time than you have, then you might want to check out the <a href="http://developer.kde.org/documentation/tutorials/kfile-plugin/t1.html">tutorial on KFile plugins</a> that I've written.

<!--break-->
<p>For those not familiar, a KFile plugin is the meta-data magic that powers the "MetaInfo" tab when you display the <a href="http://developer.kde.org/documentation/tutorials/kfile-plugin/pictures-kfile/mng-metadata4.png">properties of a file</a>, and the <a href="http://developer.kde.org/documentation/tutorials/kfile-plugin/pictures-kfile/konqi-infolistview.png">Info List View</a> in Konqueror. It's easy to write one, and there are a lot of file formats we don't have support for yet. This is a fun way to get into KDE development!</p>


