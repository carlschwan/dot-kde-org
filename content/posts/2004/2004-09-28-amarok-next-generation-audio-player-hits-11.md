---
title: "amaroK: Next Generation Audio Player Hits 1.1"
date:    2004-09-28
authors:
  - "mkretschmann"
slug:    amarok-next-generation-audio-player-hits-11
comments:
  - subject: "Nice features, bad UI"
    date: 2004-09-28
    body: "Yesterday I used amarok for the first time. It has some nice features like the crossfade or the album manager. On the other hand the UI is the worst I have ever seen in an KDE application (I'm sorry I have to say this).\n\nFor instance the menu items change depending on which of the sidebar menu (context, collection,...) one chooses. The stuff below the main entry looks chaotic to me, with a \"menu\" button at the lower right corner?! What really is brilliant is the \"current track\" context menu with the informations about the currently played track and related information - great! \n\nTag handling is much more complicated than in JuK which is the greatest application for adding meta data to mp3 IMHO. It would be great if amarok and Juk developers would work together on some parts: crossfade, album manager and musicbrainz integration could be part of a library shared by both applications.\n\nI am not sure if I like the amaroK announcement, it sounds too much like marketing bla bla, but perhaps this is just me?!\n\n\"Before amaroK was born, most KDE users were stuck with XMMS [..] Now those days are over.\"\n\nA bit modesty and at least a short link to JuK / noatun would have been more in place. Anyway, thanks for amaroK :-)\n"
    author: "MK"
  - subject: "Re: Nice features, bad UI"
    date: 2004-09-28
    body: "I haven't tried amaroK yet, but I agree with some of the things you mentioned in your post.\n\n\"most KDE users were stuck with XMMS\" is simply not true. Having that in the first paragraphs of the main page is a big turnoff. I have used JuK a lot and really like it. Maybe I'll try amaroK sometime too, maybe I'll like it better than JuK. But until then, amaroK hasn't left the best impression...\n\nAll else aside - keep up the good work and MAY THE BEST WIN.\n\nNathan"
    author: "NF"
  - subject: "Re: Nice features, bad UI"
    date: 2004-09-29
    body: "sorry but i must tell you i love Xmms  this is nice but you don't think you are monopolize a lot the desk live a little to others programmer and this stupid color white you are using, I hate it, the good you can change it. but  It's good enough, the good to the KDE is that you can change almost all good and go ahead to who like is  I nice stuff    "
    author: "jorge"
  - subject: "JuK and amaroK aren't competitors"
    date: 2004-09-29
    body: "First off, while I think JuK might be a little older then amaroK it, it isn't older by much. I've been using amaroK for a year, it started being usable about September of last year. So saying most KDE users were stuck with XMMS would be true, as that was certainly the case for me. Also it ignores that amaroK and XMMS  both follow the 'winamp' non-jukebox style of MP3 players, whereas JuK is more of the iTunes variety. \n\nI think the UI for amaroK is great. Better then any MP3 player out there. I do a good job of organizing my MP3 into directories myself thank you very much, so I don't like JuK which is organizes by ID3 tag (not that that's wrong or anything, just not my style). amaroK is for folks who either want a real simple MP3 player or (like myself) don't want to have to open seperate file dialogs boxs. File tree on the left, playlist on the right. Simple and less cumbersome. "
    author: "Ian Monroe"
  - subject: "Re: JuK and amaroK aren't competitors"
    date: 2004-10-06
    body: "I do a good job of organizing my MP3 into directories myself thank you very much, so I don't like JuK which is organizes by ID3 tag (not that that's wrong or anything, just not my style).\n\nActually, JuK won't touch your file organization unless you ask it to.  I keep meaning to use the feature myself but I like my files the way they are too. =D"
    author: "Michael Pyne"
  - subject: "Re: Nice features, bad UI"
    date: 2004-09-28
    body: "You are right saying the GUI sucks.\nIMHO, something like JuK with the amamkoK features could be the best."
    author: "Peter"
  - subject: "Re: Nice features, bad UI"
    date: 2004-09-28
    body: "If you use amarok for a longer time, you'll notice that the UI of amarok is very good for what it was designed for - it is one of those few media player which actually ease often-repeated tasks like playlist handling & creation. It is far better than xmms or (almost) all other media player I've used in this regard, which is why I got hooked :-).\nAnd AFAIK amarok is not supposed to be a juk-replacement concerning tag-handling."
    author: "ac"
  - subject: "Re: Nice features, bad UI"
    date: 2004-09-29
    body: "Tag-handling in amaroK is only meant to be something you do on the spur of the moment. Our philosophy is that you should be using a dedicated application like the excellent Kid3 or JuK, if you want to do mass-tagging or serious tag-management.\n\nIt is not true that the \"menubar\" changes with every side-tab. There is only one menubar! It is in the Collection Tab. And thanks to your reminder I will shortly make it into a toolbar. The menu button in the corner is a compromise that I have not yet thought of a better solution for.\n\nI take some offense that you consider the amaroK UI to be the worst you have ever used. I have spent hours considering the UI, fighting off bloated feature requests and refining the interface to be usable, powerful and \"amaroKy\". I personally feel that anyone who has used amaroK for a reasonable amount of time would find it to be an excellent interface. Having said this I acknowledge that it cannot be perfect and thus I welcome any suggestions you may have, reading your post above I found none.\n\nCollaboration with JuK actually happens more than people might think. Wheels is well aware that we nick loads of his code ;-) Not to mention we both use TagLib. JuK can use our engine system for sure if wheels/JuK-devel want to, the API is quite pleasant at the moment. They could probably take some stuff like the cover-manager, but then you have to ask where the distinction between the two applications would be?\n\nAnd finally, despite my serious tone here, generally us amaroK people like to joke about. Hence the tone of the dot article! Sorry you found it somehow offensive, please take it with a pinch of salt!"
    author: "Max Howell"
  - subject: "Re: Nice features, bad UI"
    date: 2004-09-29
    body: "///I take some offense that you consider the amaroK UI to be the worst you have ever used. I have spent hours considering the UI, fighting off bloated feature requests and refining the interface to be usable, powerful and \"amaroKy\"///\n\nThis is the problem. People used to KDE apps don't want a UI to be more \"amaroKy\", whatever that is. They want it to be more KDE-like. As such, it should at *a minimum* have a standard menubar and toolbar. I am what I would consider to be an \"expert\" KDE user and even I felt lost when I fired up amaroK. Where do I configure it? Where do I change keyboard  shortcuts? Nearly every GUI feature is totally different than all other KDE applications.\n\nThis is where Juk and Noatun win and amaroK loses. Now, if *ONLY* I could configure Juk or Noatun to output *DIRECT* to OSS/Alsa and not use any arts/esd/GStreamer/<Other BS sound server I don't need because I have a sound card made after 1998 that can do its own hardware mixing thank you>,\n*THEN* KDE would have a decent media player. Until then I am still stuck with XMMS / KMPlayer.\n\n"
    author: "Jason Keirstead"
  - subject: "Re: Nice features, bad UI"
    date: 2004-09-29
    body: "We use KDE, we rave about KDE, we stick to all KDE style-guidelines, but our goal is to produce the best media-player. Personally I think amaroK does feel and look like a KDE application, but I suppose my point is that if we had to do something really differently because amaroK would be better as a result, then we probably would. Naturally there is a fine line, too little KDE-ness and amaroK would be worse. I wouldn't personally add a feature unless I felt KDE users would know how to use it.\n\n\"Nearly every GUI feature is totally different than all other KDE applications\"\n\nI concede the rest of your points but this is complete nonsense, we don't have a menubar, what else can you proffer? We use KMultiTabBar, KToolBar, KPopupMenu, KListView, KStatusBar, KGlobalShortcuts, KActions, etc. etc. The Player-Window isn't very KDE, but it is also very optional, and frankly is there for people who, like me, enjoy something a little colourful and fun.\n\nFinally, GStreamer isn't a sound-server, it is a decoding framework, I suggest that if you don't like amaroK and you don't like aRts but you do like KDE stuff, that you use JuK with the GStreamer-bindings."
    author: "Max Howell"
  - subject: "Re: Nice features, bad UI"
    date: 2004-09-29
    body: "Actually don't ever listen to anyone on usability and you may keep afloat.\nFollow your taste, or whoever's taste it was, and since most users that tried amarok liked it, they will continue to like it.\nYou just can't satisfy everybody, but usuability experts will convince you that they know what is best for all. As a matter of facts usability experts are the best at convincing people that their usability expertise is needed, then they put a perfect app or desktop to the ground. \nThey are the new plague of open source."
    author: "hmmm"
  - subject: "Re: Nice features, bad UI"
    date: 2004-09-29
    body: "///I concede the rest of your points but this is complete nonsense, we don't have a menubar, what else can you proffer? We use KMultiTabBar, KToolBar, KPopupMenu, KListView, KStatusBar, KGlobalShortcuts, KActions, etc. etc.///\n\nEr.... just because you use widgets from KDElibs does not mean that your app behaves like a KDE application. As a user I would expect that at a *mimumum* this would mean that when I first start the app, it would have menu bar on the top, and a toolbar right under it. If I as a user like it at the bottom better, I can just drag it down there. Likewize, if I like a Menu button in te bottom right corner, I can customize my toolbar and put one there.\n\nThis is the whole reason you can customize these things. But it should never be the default config, since it makes new users feel totally lost. And when they already have a halfway-decent alternative, they tend to subsequenltly say \"no thanks\" (like I have) and not even both trying to \"learn to appreciate\" the UI you so painstakingly designed.\n\nAlso, note that I am by no means a UI guide stickler. In the past there have been numerous times where I questioned the guide and some points I whole-heartedly disagree on. However, there are just *some* things an app should have consistant - and a menu bar and tool bar are two of them.\n\n"
    author: "Jason keirstead"
  - subject: "Re: Nice features, bad UI"
    date: 2004-09-29
    body: "Maybe it's hard for you to believe this, but amaroK has about the best GUI possible - better than almost any other KDE app. Maybe KDE should be more 'amaroKy' (whatever) as a whole! That would be better then adding bullsh*t like menubars or toolbars for every application (just to make _you_ feel at home), even if the program has only five menu items. Empty menus are disturbing, a waste of space and introduce unnecessary clutter.\n\nBTW, there are applications that don't need to (and should not) follow standard interface guidelines, because they fulfill a special goal: music production apps, paint (that's why Gimp or Photoshop are bad), compositing (After Effects' GUI is really bad), 3D apps or - in this case - audio players...\n"
    author: "Willie Sippel"
  - subject: "Re: Nice features, bad UI"
    date: 2004-09-29
    body: "\"Maybe it's hard for you to believe this, but amaroK has about the best GUI possible - better than almost any other KDE app.\"\n\nNot quite, as you can read that several people dislike amaroks UI. It might be the best UI for _you_ but this doesn't mean that it is the \"best UI\", especially if a couple of people stated exactly the opposite.\n\nAs I already suggested the developers should ask Aaron for his opinion and I'm sure he can give them some hints why amaroK is so confusing to use.\n\nIMHO the general problem is that amaroK wants to be cool and exceptional and thus they try to make the UI non-standard (no menu bar, no toolbars, menu button bottom right and so on). It's o.k. for some audience, e.g. <18 years, but I am not sure this application should be a part of core KDE then. If you want to see a standard GUI, use JuK, which probably is too \"boring\" for the general amaroK audience.\n\n\"there are applications that don't need to (and should not) follow standard interface guidelines, because they fulfill a special goal:\"\n\nwhy should special applications not use a standard interface?\n\n\"[...] paint (that's why Gimp or Photoshop are bad)\"\n\n??? I really don't understand your line of reasoning here. \n "
    author: "MK"
  - subject: "Re: Nice features, bad UI"
    date: 2004-09-29
    body: "Well, and there are also quite a few people that love amaroK's UI...\n\nYou know, if the amaroK team wants to add a menu- and toolbar, I couldn't care less - as long as it's possible to remove them, as they are simply not needed. Why would anyone in their right mind want to add UI elements just to comply to some unwritten UI standard, even if they are completely useless?!?\n\nAnd that has nothing to do with the audience's age, as I'm neither <18, nor a gamer, modder or geek...\n\n\"why should special applications not use a standard interface?\"\n\nBecause a standard interface is just the smallest common denominator. It can't be perfect for every app, that's why all workflow-optimized professional applications use their own non-standard, workflow-centric interfaces. Take Softimage|XSI for example: the context menu is available by pressing ALT+RMB, because RMB is deselect (and MMB is toggle select). With Maya, the menubar is star-shaped (the context menu is also star-shaped) and accessible by holding spacebar. Houdini has the menu on TAB, with type-ahead-find to select the menu item. All those apps are professional 3D suites, all have a completely different interface and controls, and they all have a very fast workflow (with a steep learning curve) that you can't achieve following standard HIG's...\n\n\"??? I really don't understand your line of reasoning here. \"\n\nPhotoshop is a consumer/ prosumer/ low-end professional application, with Gimp being a clone. The interface is designed to be usable by casual users without much learning, but the workflow is nothing to write home about. There are also professional paint applications like Mirage, Liberty, Amazon, Paintbox, Cyborg Paint etc - you need a while to get the uncommon design, but once you're used to it, you are much faster than any Photoshop user.\n"
    author: "Willie Sippel"
  - subject: "Re: Nice features, bad UI"
    date: 2004-09-29
    body: ">why should special applications not use a standard interface?\n\nBecause they don't need to use it?\n\nDo you think kfind need to be removed because there is no toolbar/menubar?"
    author: "Bellegarde Cedric"
  - subject: "Re: Nice features, bad UI"
    date: 2004-09-30
    body: "KFind is a dialog based application. It shouldn't have a menu (and if you look at the current styleguide you will see a case study exactly about some kind of find dialog). Yet a standard menu/toolbar in a not-dialog based application (like amaroK) should be present. You can remove them, move them, whatever you like. KDE allows it. But right now with amaroK you cannot move the toolbar, you cannot add a menubar. Some would like to have those and you can't say that they are wrong. There is no reason to limit the possibilities. You can be different in other ways as well.\n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: Nice features, bad UI"
    date: 2004-09-29
    body: "I find amaroK's UI to be close to perfect; there isn't anything I can immediately think of that I'd change. The menu in a button in the bottom right corner is the only thing that sort of sucks, but I don't think a menubar would be a better solution. Perhaps something like what winamp has, where rightclicking on any of the \"empty space\" in the UI (ie, where a rightclick has no other useful function) brings up the menu?"
    author: "Illissius"
  - subject: "Re: Nice features, bad UI"
    date: 2004-09-29
    body: "\"It is not true that the \"menubar\" changes with every side-tab. There is only one menubar! It is in the Collection Tab.\"\n\nA menubar is either there all the time or not at all (smaller games). I don't know any KDE application where the menu bar behaves that strangely. I spend several minutes(!) to find the cover manager--I knew was there because I used it already but I was not able to find it again. What is the reason to not allow the user to access the cover manager all the time via a menu bar?\n\n\"I take some offense that you consider the amaroK UI to be the worst you have ever used. I have spent hours considering the UI, fighting off bloated feature requests and refining the interface to be usable, powerful and \"amaroKy\".\"\n\nI'm sorry, but I hoped you liked constructive feedback. Also I didn't wrote that it is the worst UI ever, but the worst KDE UI I have experienced so far--meaning it is not THAT worse, because KDE UIs in general are good ;-) \n\nAnother smaller usability issue: the track slider is in the status bar. This is confusing since I am not aware of any KDE application that has an active UI element in the status bar. Also the stuff below the main window looks like a toolbar, but is none.\n\nOther examples: I would not expect the \"recode ID3v1 tags\" in the \"font\" section. A \"what's this?\" help is not available. Using check boxes for the \"tag filter\" is confusing since they behave like radio buttons. I could surely find many more examples and I am not a usability expert.\n\n\" I personally feel that anyone who has used amaroK for a reasonable amount of time would find it to be an excellent interface. \"\n\nThis is what the xfig fans (one of the UI nightmares) keep telling me too, but this is not the idea of a good UI. If you can don't fear some serious critics and want to make the UI better, then ask some of the UI experts like Aaron Seigo. He surely can help you explaining far better than I can why the UI is so hard to use for thos who have not already used a \"reasonable amount of time\". Good luck with the project -- Marco\n"
    author: "MK"
  - subject: "Re: Nice features, bad UI"
    date: 2004-09-29
    body: "\"Another smaller usability issue: the track slider is in the status bar. This is confusing since I am not aware of any KDE application that has an active UI element in the status bar.\"\n\nFire up Konqui and look at the status bar: there's a checkbox at the right side - so I guess there's at least _one_ other KDE app with an active UI element in the status bar... :-)"
    author: "Willie Sippel"
  - subject: "Re: Nice features, bad UI"
    date: 2004-09-29
    body: "The track-slider is there because it is status information. We deliberated putting it in the toolbar, but decided not to because the statusbar is where you put status related widgets. The rest of KDE not doing it is an issue with KDE in my opinion."
    author: "Max Howell"
  - subject: "Re: Nice features, bad UI"
    date: 2004-09-29
    body: "I used amarok today for the first time. I really loved the features, and I am off of xmms for good. So thank you very much for the nice app.\n\nThat said, I wanted to report on my experience with the UI. I literally spent 5 minutes searching for where to set my multimedia keys to work with amarok. The main reason for that was because I did not see the \"Menu\" button. I did not expect to find it there.\n\nIt also took a while to figure out how to add a directory that contains all my music. I did not know where to go.\n\nI felt a little lost with the interface at the beginning. Once you get used to it, it is ok, but it seems to be a little difficult for newbies.\n\n"
    author: "Paul"
  - subject: "Actually, I like the UI!"
    date: 2004-09-29
    body: "The UI is a departure from any other media player I've used, but after several weeks of using it I'd have to say it's the best UI of any of them.\n\nPerhaps it doesn't look cool enough for some people, but obviously the authors felt the same way I did about XMMS, Freeamp, Winamp, Windows Media Player, etc and cam eup with something fresh.\n\nGood work guys, Amarok really did free me from XMMS!\n\nArawak"
    author: "Arawak"
  - subject: "Re: Nice features, bad UI"
    date: 2004-09-29
    body: "Yes I don't like the announcement, either. But amaroK. I like the UI a lot - easy to use and much better than e.g. JuK (I prefer the original \"Apple\" design more than this copy). The xmms style window is really nice if you want a control on top of everything else. While it is still KDE like, the colors put in a higher contrast which makes it perfekt for this.\n\nI also don't see tag handling as complicated (especially if you compare it to Winamp) but I will try JuK on this...\n\nAnyway I like amaroK."
    author: "Norbert"
  - subject: "Re: Nice features, bad UI"
    date: 2004-10-05
    body: "> I prefer the original \"Apple\" design more than this copy\n\nActually Apple didn't design that UI at all.  There were jukebox programs for many years before iTunes that had similar interfaces.\n\nThe ideas came more from Real Jukebox than anywhere else, but even then there was a several year gap between me using it and designing the JuK interface.  I hadn't ever used iTunes when I came up with the interface for JuK and even now I've probably used it for less than an hour total."
    author: "Scott Wheeler"
  - subject: "Re: Nice features, bad UI"
    date: 2004-09-29
    body: "While I appreciate amarok's functionality and its integration to KDE, I too take exception to the UI.  There are no fatal flaw in amarok, though, and most of my nits are easily fixed details.  What is disturbing, and is yet one more example why FLOSS hasn't made any serious inroads with the \"unwashed masses\", is the rumours I've read that the amarok developers don't want to listen to anyone who comments on the application's UI and are essentially saying \"use something else, don't bother us, we like it as it is and we won't change it\".\n\nSigh.  Some days, I wish someone would just simply port XMMS to Qt/KDE so that we could have a simple, fast and nimble audio player for KDE that doesn't stick out like a sore thumb...\n\nMakes me wish I was still an active coder."
    author: "J.D. Smith III"
  - subject: "Re: Nice features, bad UI"
    date: 2004-09-29
    body: "Thats cause amarok is great and I think media players UI are kinda like pizza toppings, you can't please everyone, though as indicated on the dot they are open to suggestions regardless. You want to port XMMS to KDE? When did XMMS have great UI. I mean, I used XMMS for years, but amaroK is clearly better IMHO. Having seperate file open dialog boxs is such a pain. "
    author: "Ian Monroe"
  - subject: "Re: Nice features, bad UI"
    date: 2004-09-30
    body: "\"the rumours I've read that the amarok developers don't want to listen to anyone who comments on the application's UI\"\nThat's not true at all. They may not agree with what you are saying but they're absolutely willing to at least listen to what you have to say. In the end it is up to them to cater to not just a few, loud, non-representative-of-the-majority type users, but to the most people they can. \n\nFrom hanging out in the IRC channel (#amarok on freenode) I get the feeling that most users _love_ the UI, and yes, I am one of those users.\n"
    author: "illogic-al"
  - subject: "Re: Nice features, bad UI"
    date: 2004-09-30
    body: "I second that. I tried it but was put off by the UI.\n\nCrap. It's too bad. Kaffeine rocks for playing movies, but I want something more adapted to playing music while still getting the KDE goodness."
    author: "Jonas"
  - subject: "If you don't like it's UI don't use it."
    date: 2004-09-30
    body: "The UI is the best I've ever seen on KDE.\nOn the one hand people say amaroK does not look like a KDE app.\nSo does XMMS ?\nThe main reason why I use amaroK is that it includes new ideas concerning UI.\nI like it very much because it does not look like every other stupid boring KDE app.\nThere seem to be many people who a addicted to there boring KDE UI scheme (Toolbar, Window, ugly buttons) that's it.\nSome people are too shortminded or maybe even too stupid to accept new ideas.\nSeems to be a problem in the whole linux community.\nI hope nothing will change in amaroK's UI because hate to always use the same shit standart KDE UI's.\n(Gray and White and everythink looking the same somehow)\nMaybe amaroK does not need to change but some people have to change their mind and become more flexible.\nAlways the same people who molest the comunity with there opinion about how a UI should look and that everything has to look the same !!!!\nAnd other shit.\nIf you want a boring KDE UI use noatun !!!!!!!"
    author: "Stephan"
  - subject: "Re: Nice features, bad UI"
    date: 2004-10-01
    body: "Ok, you don't like the \"Menu\" button in the bottom-right corner, but what else? There's not really a need for a \"menu bar\" since everything is doable wih drag-and-dropping or single clicks. *This* is usability, not designing a UI where you have to put thousands of menu items to do everything.\n\nSaid this, there's really the need to find a solution for the menu button :)"
    author: "Davide Ferrari"
  - subject: "Very good app, UI so-so"
    date: 2004-09-28
    body: "I disagree that this marketing \"bla-bla\" is bad.\nI think we (the Open Source world) has way too less\nof it. Just compare Firefox and previous Mozilla Suite\nand how marketing is important to really make impact.\nSometimes I'm under the impression that many geeks don't\nreally want a larger user base. Fortunately this isn't\nthe goal for KDE. On Windows you have big-scale annoucements\nand bad apps. We should have great apps and shy annoucements\nwith no self-esteem? No way - we can have great apps and\ngreat annoucement. Not a contradiction.\n\nWhat I'm still missing in Amarok is the ability to somehow dock\nthe main window and the playlist into one single window with\na common look. \n\nJuK is cool for organizing music. Amarok for playing. They are both\ngood partners and I think sharing some code (for Album, etc.) is\ncertainly a good-idea. Besides (slightly off-topic):\nWhat's totally confusing me is that the current JuK that came\nwith KDE 3.3 (SuSE 9.1) has no sign _whatsoever_ that there\never was an option to fetch meta data from the internet (TRM/MusicBrainz).\nBut I could swear there used to be an option some time ago...\nWeird!\n"
    author: "Martin"
  - subject: "Re: Very good app, UI so-so"
    date: 2004-09-29
    body: "\"Sometimes I'm under the impression that many geeks don't really want a larger user base.\"\n\nWhere did you get the impression most geeks DO want a larger user base?  I usually see two factions on forums and mailing lists.  One would be those who constantly bitch about how Linux will never get Joe Sixpack to use it if they don't CONFORM!  The other is a guy with a personal anecdote about how linux was easy for him.  Neither of these imply a desire by the community as a whole.  I woul dbe interested in the results of an accurate poll of a sufficient sample size on wether or not the majority actually DO want widespread adoption of the same users who carelessly allow their machines to be infected with any worm that wants in. Allow me to cast my vote now.  Screw the average user.  Linux was made by geeks for geeks and I wanna keep it that way.  The only widespread adoption I wanna see is in business and hardware vendors."
    author: "MrGrim"
  - subject: "Re: Very good app, UI so-so"
    date: 2004-09-29
    body: "Why, exactly, do you expect hardware vendors to support an unpopular system that's only for geeks?\n\nOh, wait, you want it to be popular *in business* as well. This may come as a surprise to you, but some businesses employ non-geeks for some of their computer operations! Guess what, if you want business adoption, you have to acommodate the \"average user\". And once they use it at work, they'll want it at home.\n\nPopularity is our friend. I think there's a point, maybe around 10% marketshare, where ignoring Linux will be harmful to both software and hadware vendors. Also, more users = more money to distros = better testing, more stuff working out of the box, more developers sponsored to work on apps, plus things like reduced FUD credibility, reduced chance of unhelpful laws, reduced clueless \"this website only works in IE on Windows\" websites, ...\n\nSo, where does this leave j03 51xp@X, average geek? Well, here's where the Free = Freedom comes in. Nobody forces you to use FooDistro EasyConfig, the text files are there for you to edit. Nobody forces you to use BarWM (optimised for the end users!), you can use some WM optimised for \"power users\".\n\nWhat you're really advocating is \"I think people should only write software that I want to use, I don't care about anyone else\". If somebody wants to write software that's easier to use, what right do you have to complain? Making suggestions and giving ideas is one thing, but that is not your tone.\n\nLet's face it - Microsoft has no clue about end users. They come out with \"innovations\" like the collapsing menus - I have seen users look in the right menu for a function, see it collapsed, and assume the function does not exist. And they've been trained by that \u00a3@$% paperclip to avoid Help like the plague.\n\nIn some areas, Windows is already being beaten. The typical Windows start menu is a jumble of disorganised programs. Sometimes, they are helpfully grouped by publisher (like most people would expect to find Print Artist next to Halflife). My KDE menu lists games together, utilities together, etc. I know the \"Start menu metaphor\" is hated. I've read several articles making the point Linux desktops should do \"something else\". They never say *what* though. I've heard WindowMaker praised for being different, but it looks to me they put \"Start\" as a right-click instead of a button. Should somebody come up with something, you'll have the option to use it, whether it's for an OS written for 100, 100,000 or a billion.\n\nUsability and users are not the enemy. The enemy is closed formats that lock your data in. The enemy is being told the bugfix is in the new version you have to pay for. The enemy is valuing shinyness over security. Everyone deserves freedom from software companies who muck around their customers, not a select few."
    author: "a mysterious anonymous poster"
  - subject: "Re: Very good app, UI so-so"
    date: 2004-09-29
    body: "you can disable the xmms-like playerwindow, and amarok will appear as a single-window application with \"a common look\".\n\nregards,\nmuesli"
    author: "muesli"
  - subject: "add a PlayPause"
    date: 2004-09-29
    body: "And then you can add a PlayPause button to the play list window and your good to go. I think this is what the parent was wanting."
    author: "Ian Monroe"
  - subject: "Re: add a PlayPause"
    date: 2004-09-30
    body: "it is already there :)\n\nRinse"
    author: "Rinse"
  - subject: "Re: Very good app, UI so-so"
    date: 2004-10-05
    body: "> What's totally confusing me is that the current JuK that came\n> with KDE 3.3 (SuSE 9.1) has no sign _whatsoever_ that there\n> ever was an option to fetch meta data from the internet \n\nThat probably means that SuSE didn't have the appropriate dependencies when creating the package.  Support for internet-based tag-guessing was actually improved for KDE 3.3."
    author: "Scott Wheeler"
  - subject: "Just what I was looking for"
    date: 2004-09-28
    body: "Finally a player that doesn't make it all complicated to play my music.  With xmms I really like that I can just play a directory of music without having to build a database of my music and sort on the artist, etc (assuming all the tags were correct).  Amarok is great in that I can just browse my collection right on the drive and drag the folder to the current playlist.  Nice and simple.\n\nThanks!"
    author: "Eric Weidner"
  - subject: "Re: Just what I was looking for"
    date: 2004-09-28
    body: "try juk... If juk had *some* of amarok's features, or if amarok had the great interface juk has, that whould be the perfect audio player. now I use juk because I prefer a good interface and ease of use (also for newbies, everyone nows how to use it immediately, try that with xmms or *cough* amarok) above features like albumpictures and OSD's."
    author: "superstoned"
  - subject: "Re: Just what I was looking for"
    date: 2004-09-28
    body: "I have tried juk and rhythmbox (I like rhytmbox a little more than juk, btw) and both didn't give me the dead-simple browse to my music and drag it into the playlist functionality that Amarok has.  Both of these want me to catalog all my music in order to get to it and that's not how I work.\n\nI don't care about album pictures and OSD.  The Amarok interface works pretty well for me.  The only issue I have is I keep forgetting where the properties option is located, but I really don't need it often anyway.\n\nDead simple browse and play is what I like about Amarok."
    author: "Eric Weidner"
  - subject: "Re: Just what I was looking for"
    date: 2004-09-28
    body: "I use Juk over Amarok as well for the same reasons, but it would be great if Juk would figure out how to reload Folders when they change."
    author: "Joe Schmoe"
  - subject: "Re: Just what I was looking for"
    date: 2004-09-29
    body: "well, it does, over here... maybe it takes some time? or the config isn't right?\n\nmy mayor annoyance is it is difficult to remove files from a playlist (without deleting them from disk!) - actually I never succeeded..."
    author: "superstoned"
  - subject: "Removing files from Juk playlists"
    date: 2004-09-29
    body: "Just \"cut\" the files from the playlist without pasting them anywhere.\n\nIt works for me, although I'd like the ability to be able to put an \"Empty Playlist\" button on the toolbar."
    author: "driptray"
  - subject: "Re: Just what I was looking for"
    date: 2004-09-29
    body: "I would have agreed with using juk until last month when my collection grew from a couple thousand to 23 thousand files. It might have something to do with being on a FAT32 formatted drive, but it takes over an hour for juk to index.\n\nI've been using amarok for a couple weeks now and I think it's pretty good. It looks like it's getting some more of the meta-data editing features that are the only thing that make juk really desirable for me since I have lots of fixing to do on those new files."
    author: "scragz"
  - subject: "Nice engine"
    date: 2004-09-28
    body: "The amarok engine with its support for gstreamer and xine is really fantastic. (And it works beautifully, amarok's the first music player which can do correct crossfading between tracks). On the other hand the UI is not - well - liked by everybody (especially not by me) because it feels so non-KDE-ish.\n\nWhat I'd like to see: put the amarok engine in kdemultimedia and let juk and the other applications also use it. Juk is such a great application but its output support covers only gstreamer-0.6 (not really good working) and arts (obsolete). \nHowever the amarok engine supports (or will support) all major media frameworks."
    author: "Bausi"
  - subject: "Re: Nice engine"
    date: 2004-09-28
    body: "It would be nice if all KDE multimedia apps would share the same metadata. This way reading metadata, downloading covers etc. would only be necessary once, users could use rate songs across applications (image starting juk for the first time and be able to play your favorite songs from amarok). Only playlists would be seperate.\n\nA global multimedia meta data store would also allow new and interestings stuff using metadata. Imagine you work on a document and want to know: Which song did I listen to the last time I opened this document. Now, this sounds lame, but I am sure there could be 1000's of useful applications for system wide metadata.\n\n(OT: One thing that comes to my mind as a use for metadata: Give me the document I was working on while talking to X on kopete/skype... I am confident the OSS community would find tons of nice applications for a good set of metadata.)\n\n"
    author: "Anonymous"
  - subject: "Re: Nice engine"
    date: 2004-09-29
    body: "Use Noatun with Hayes if you want standard meta data functionality. It relies solely on KMetaFileInfo. Proper implementation of scoring, playlists, grouping etcetera through metadate is not possible with most file systems though.\n\nI have a strong wish for Hayes to, instead of making different playlists, categorize songs or albums individually and then making a selection based on that meta info. That would be the proper way to get this unified.\n\n"
    author: "Rob Kaper"
  - subject: "Correction: aRts engine is available"
    date: 2004-09-28
    body: "The article states that the aRts-engine is not yet ported to the new engine architecture. This information is not up to date, as I forgot to update the text.\n\nIn fact aRts is fully usable in 1.1 (although we don't recommend using it).\n\n"
    author: "Mark Kretschmann"
  - subject: "Re: Correction: aRts engine is available"
    date: 2004-09-29
    body: "Does that mean bug 81436 is no longer an issue?\n\nhttp://bugs.kde.org/show_bug.cgi?id=81436\n\nThis is the only problem I have with amarok. I frequently kill artsd to use mplayer - (mplayer + artsd gives significant audio sync problems on my SuSE 9.1 install). Then, to use amarok, I have to quit a running copy - restart artsd and start amarok again.\n\nBut I love amarok so much that I am putting up with all this!! \n\nTo all who say amarok's UI is not good and doesn't confirm to KDE's style - try using it for a few days and see for urself why this UI makes so much sense. Why try using the same UI across applications when all applications don't do the same task!\n\n"
    author: "antrix angler"
  - subject: "Re: Correction: aRts engine is available"
    date: 2004-09-29
    body: "The path to a better way of life is to not use aRts at all! You should use amaroK with xine or GStreamer with direct alsa output instead, as we recommend. \n\nIf your soundcard does not provide hardware mixing, the natural solution is to use the dmix plugin for alsa. It allows you to run several audio applications at the same time, without the need for a clunky soundserver in between.\n\nInformation on dmix is available here:\nhttp://opensrc.org/alsa/index.php?page=DmixPlugin\n "
    author: "Mark Kretschmann"
  - subject: "Re: Correction: aRts engine is available"
    date: 2004-09-30
    body: "Your advice + a gentoo wiki on dmix+alsa and now I am free from sound server troubles!"
    author: "antrix angler"
  - subject: "Re: Correction: aRts engine is available"
    date: 2004-09-30
    body: "I'd switch to either using GStreamer (what I use) or the XINE engine.  Also, I'd recommend disabling the sound server, as it's not really all that useful these days.\n\nReally, if you get right down to it, I'm guessing that aRts was adopted and adapted mainly to compete with ESD (which sucks worse than aRts.)  GNOME moved away from ESD; I think it's time to ditch the notion of crummy software-based mixing and synthesis.  aRts was a neat idea, but we've not really seen any improvement for quite a while now."
    author: "regeya"
  - subject: "Re: Correction: aRts engine is available"
    date: 2004-09-30
    body: ">  Really, if you get right down to it, I'm guessing that aRts was adopted and adapted mainly to compete with ESD (which sucks worse than aRts.) \n\nNothing of the sort. In KDE1, we had KSoundServer which was quite akin to ESD. \n\n> aRts was a neat idea, but we've not really seen any improvement for quite a while now.\n\nYes, arts is already pretty much depreciated. kdemm is the next generation KDE infastructure. This time, we aren't going to make the mistake of implemeting our own backend stuff; we'll let people like gstreamer and nmm do that."
    author: "anon"
  - subject: "Why I use amarok"
    date: 2004-09-28
    body: "The reason amarok has been the only player/orginzer/jukebox that I have stuck with is that it combines all the goods in one place. Mind you I am not saying that those feutures have been implemented/represented(GUI wise) in the best possible way, but it still has them while others don't. I would love to use Juk but it cannot handle my mp3 collection (40,000+ mp3s) without getting stuck for sometimes up to a minute before it starts responding again. I use Juk to do my mp3 tags but now that almost everything in my collection has tags amarok is great. The album art manager needs work though:\na) it should be configurable what the name of the file should be (aka Folder.jpg, Artist.jpg, whatever)\nb) it should be able to place the jpeg in the album directory instead of its own ~./kde/share/apps/amarok/albumscovers....\nc) it should scan for files already located in the folders. \nI have images for almost half of my collection. The context tab displays the art beutifuly but the mng doesn't  even get phased...\nOverall though amarok is my choice of player so far. I have tried madman, yammi, and juk.\nBtw if you ever wanted to be able to access your music from home check out http://www.gnump3d.org I was using that before I get my hands on amarok. I still use it from my office but no longer while I am at home. There is only one thing I am missing in amarok: ratings. I love to rate my songs, got hooked on it with my ipod. Talking about ipod can we get an ipod mngr for amarok too... that would great.\nKeep on coding."
    author: "MANOWAR^"
  - subject: "Re: Why I use amarok"
    date: 2004-09-29
    body: "a) sorry, this is currently not possible - we might think about that once implementing the image database in sqlite.\n\nb) we're not allowed to do this because of amazon licensing issues - sorry.\n\nc) it does. we'll add mng support.\n\nratings? an intelligent rating system is already implemented in amarok, rating songs manually will follow.\n\nregards,\nmuesli\n"
    author: "muesli"
  - subject: "Re: Why I use amarok"
    date: 2004-09-29
    body: "I used xmms, then mp3kult (great app, which sadly was never updated to KDE3.) Then I discovered juk. But my MP3 collection kept on growing...\n\nI now switched from juk to amarok because juk is just inbearably slow with a large collection of music. Startup times of several minutes with just 8000 MP3 on an Athlon-600 are a joke (with an existing database!!). Also the app freezes frequently for several seconds. After shutting juk down it consumed 100% CPU for minutes. Just unusable.\n\nRhythmbox crashes too much on invalid files. Also I tend to avoid GTK apps. The complete lack of KDE integration turns me off.\n\nAmarok-1.1 is also still a little unstable for me, but I already like it better than juk. Great UI, much faster, I love the cover manager. This looks like a winner!"
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: Why I use amarok"
    date: 2004-10-02
    body: "I agree that players need to be faster when loading the library, but rhythmbox has improved much of late and mine doesn't crash anymore. In fact, mst \"rhythmbox crashes\" are actually gstreamer problems now, and the ones I have are probably the result of mixing different repositories (Fedora) to play some esoteric file formats.\n\nrhythmbox excersizes gstreamer a fair bit, and has removed all file format related code which is why it still doesn't have things like tag editing which should be implemented in gstreamer first. I really wish KDE and GNOME could work together (maybe through f.d.o) to make gstreamer the one for *nix at least for now. One interesting player out there is muine, but you seem to mind gtk based apps though. It is relly simple and it has most of what you want from a player, but is not big on configurability."
    author: "Maynard"
  - subject: "App name"
    date: 2004-09-28
    body: "Why is it named \"amaroK\"? Is there some significance there?"
    author: "AC"
  - subject: "Re: App name"
    date: 2004-09-28
    body: "The app is named after the album Amarok by Mike Oldfield. It is truly great music, and as a long time Oldfield fan I felt this is a nice name for a music program.\n\nIt is also worth noting that Amarok means \"Wolf\" in the Inuit language. In a different context it could also be interpreted as \"I am a rock\" ;)\n\n"
    author: "Mark Kretschmann"
  - subject: "Re: App name"
    date: 2004-09-29
    body: "Quoting a large Mike Oldfield fan site: http://tubular.net/discography/Amarok.shtml\n\n\"There is speculation on what the word Amarok might actually mean. Mike himself has said that it doesn't really mean anything, but it could sound a bit like 'am a rock' (as in saying I am a rock, perhaps a comment on his nature). Others say that, like Ommadawn, the title is a corruption of Irish gaelic words, which mean 'morning' and maybe most interestingly, 'happy'. The word 'happy' is spoken at various times throughout the album.\n Also similar is the Innuit word for wolf, amaroq, which some say Mike heard on a TV programme about wolves.\"\n\nThat's the full story.\n\n\n- Niels\n(proud owner of the Amarok Gold Edition, and original transcriber of the  Amarok Fast Riff)."
    author: "Niels"
  - subject: "Unstable"
    date: 2004-09-28
    body: "I for one, enjoy amaroK's UI, especially the cool album cover feature. I do have stability issues, though.\nI've compiled amaroK from source, under Debian with KDE 3.2.3, and GStreamer-0.8.4. Every once in a while, playback stops, without any other observable problem (i.e., amarok doesn't hang.) Clicking the play button in these cases does not resume playback, and I have to restart the application.\n\nAny suggestions on how to solve/debug this issue? I suspect it has something to do with the underlying engine, so I'm not sure simply running amaroK through a debugger would help."
    author: "Elad"
  - subject: "Re: Unstable"
    date: 2004-09-29
    body: "I have a similar, but worse, problem. Since last night upgrade from Debian/Kalyxo sources.list server, amarok just cannot play with gstreamer. It justs doesn't sound, and leaves the soundard/alsa/arts ina a very bad state, no other appilcation can play...\n"
    author: "charles dickens"
  - subject: "Nice UI"
    date: 2004-09-28
    body: "I have to disagree with other people here, I really like the original UI of amaroK. Perhaps they should try it a bit more. The context tab is awesome, the collection tab is simple and powerful, the browser tab and the \"Add media\" button allows you to not mess the database when you don't want (which is a weak point of iTunes or Rhythmbox in my view), and as the website says, amaroK \"doesn't get on your way\"\n\nOne tiny thing though : on the sidebar, you can quickly access each browsing mode by clicking on a tab. But for some reason, the \"Playlist\" tab and the \"Smart playlist\" are in the same tab. \nhttp://amarok.kde.org/images/zoom/XLZENU/transparent.png\nThis splitted window is not very usable with a small screen (like me) when you a non-trivial number of playlists. Fortunately, you can make the \"Playlist\" or the \"Smart Playlist\" take the full screen of the tab. But this means that you have now two logics and two ways to change from a tab to another.\n\nWhy not just make two separate tabs ? Simple is beautiful."
    author: "jmfayard"
  - subject: "Good job"
    date: 2004-09-29
    body: "I like Amarok's UI and it's feature set is dead on for what I need.\n\nI know the UI is not like most other programs, but I think the differentiation is for the best and it is also quite common for such an application. Now if this was a word processor or web browser I would insist on a uniform interface, but these programs are not so complex to use anyway and people do not want the screen real estate to be limited by a menu bar and like the cool and different look IMO.\n\nGreat job!"
    author: "Alex"
  - subject: "non-kde-ish"
    date: 2004-09-29
    body: "I don't understand the comments about amaroK not looking like a KDE app. This is what caught my attention the first time I got a look at it. I also think that features added like the player window follows KDE color scheme makes it look more KDE-like then ever.\n\nI don't understand the need for a menubar, could someone answer this for me. All of the functions (very few) that are needed to actually play music are right there in the toolbar. All of the more advanced features (read stuff not often used i.e. configuration) are in a single location, the menu button. Had the devs put a menu bar and every other \"KDE-like\" feature, the playlist would grow bloated with no playlist to look at.\n\n\nThis all really bothers me, of all the time I've spent in #amarok I have yet to hear anyone say it was non KDE-like, or that amaroK was hard to use. I can however speak of the many users who have traded in XMMS for amaroK.\n\nBut everyone is entitled to there opinion, I guess this explains the all of the recent \"usability experts\".\n:)\n\nCheers,\nMike"
    author: "Mike Diehl"
  - subject: "Simple Review"
    date: 2004-09-29
    body: "I wrote this review:\nhttp://grebowiec.net/archives/2004/09/amarok_11_revie.html\nof Amarok 1.1 just to day :)  I am rather enjoying it.  It is what I have been waiting for in the music/media manager area."
    author: "Nathan"
  - subject: "AmaroK rocks!"
    date: 2004-09-29
    body: "Best media player I have ever used. I find the ui great too, despite the many people saying the ui is bad. Then the ultrafast tagging is great, when I ran some hot mediaplayer in windows it crashed when scanning my collection. taglib sweeped it very fast!\n\nThe hardest part with amaroK is remebering the name ;=)\n\nAwesome guys!\n\n  P."
    author: "perraw"
  - subject: "My only problem with Amarok..."
    date: 2004-09-29
    body: "... is that it's not packaged for Debian so I can't try it. Currently I can't play my music at all under KDE.\n\nI just ripped a new CD with the audio-CD ioslave, and it only gave me the options to rip as WAV or OGG, so naturally I picked OGG (KAudioCreator just sat and spun the CD, had to be killed [this is no copy protected CD, cdparanoia could rip it just fine at home]). Ripping went fine, but Juk can't seem to play OGG. It loads the files, starts playing, knows the file length and everything seems ok, but there is just no audio output! Playing a few old MP3:s I happen here at work works just fine. \n\nSo as Juk can't handle OGG I'd like to try Amarok, but I don't want to destroy my machine with randomly installed software, I prefer it packaged as deb:s (yes, call me lazy, but compiling and installing KDE apps with a different --prefix never seems to work properly).\n"
    author: "Chakie"
  - subject: "OGG in Juk"
    date: 2004-09-29
    body: "Most of my music is in OGG, and it plays just fine in Juk.  Something must be wrong with your setup."
    author: "driptray"
  - subject: "Huh?"
    date: 2004-09-29
    body: "Amarok-1.1 is packaged in Debs by Kalyxo.org. Follow the Debian link on the amarok homepage. There are some problems with gstreamer playbackin this build, so I use the xine playback. I detest and hate artsd.\n\nJuK can certainly play MP3 and OGG files here (Debian unstable). Do you use the gstreamer or the artsd backend?"
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: My only problem with Amarok..."
    date: 2004-09-29
    body: "Debian packages are available on kalyxo.org. Just click the \"Debian\" link on our site."
    author: "Mark Kretschmann"
  - subject: "Re: My only problem with Amarok..."
    date: 2004-09-29
    body: "> My only problem with Amarok is that it's not packaged for Debian so I can't try it.\n\nYou can find packages for Debian here:\nhttp://www.kalyxo.org/twiki/bin/view/Main/KalyxoPackages"
    author: "Christian Loose"
  - subject: "Re: My only problem with Amarok..."
    date: 2004-09-29
    body: "The KAudioCreator thing is a bug, and it should be fixed in KDE 3.3.1 and 3.4\nhttp://bugs.kde.org/show_bug.cgi?id=87267"
    author: "Jan Jitse"
  - subject: "Re:I have _the_ explication (please answer me)"
    date: 2004-10-02
    body: "Hi,\n\nI was beginning to think I was the only one to have such a problem with kaudiocreator. I can tell you why you can't hear a thing :\n\nkaudiocreator just didn't succeed in ripping the cd tracks. I have the same problem here : it happens from times to times (espacially when I try to rip a cd all in 1 go) that kaudiocreator freezes. I then have to open a console an kill the kaudio_cd process related (you can also kill the kaudiocreator process). Then it _looks_ like everything is back to normal : you try re-ripping the cd and, let's say, this time everything goes ok. You open your <ogg-reader>, load the files : everything looks fine but you can't hear a thing. WTF ? I made some searches and I found that the wav file extracted by kaudiocreator is also \"speechless\". The only solution I got to be able to rip cds again is to reboot the entire system !!!\n\nI think there are 2 problems :\n1) The first one is how kaudiocreator deals with the CDrom drive (see the bug reported earlier http://bugs.kde.org/show_bug.cgi?id=87267 (which I just found out)\n2) I think that this kaudiocreator behavior pushes the CDROM drive driver to its limits and that the kaudiocreator freeze is due to the CDROM driver getting \"mad\". When you kill the application, the driver is left in a \"weird\" state and cannot rip cds anymore... \nMy CDrom drive driver is built-in my kernel so I cannot test if unloading/reloading the kernel would solve the problem (and validate my theory). Also it could be that, with <bug1> solved, <bug2> won't appear anymore.\n\nAs I found nobody else that had the same pb, I gave up and was waiting for the new 2.6.9 kernel to test my theory. But I'm happy (sorry for you) that someone encounters the same problem : could you please tell me your distribution / kernel version and cdrom drive model ? That would be very helpful !\n\nThanks for having the same problem as me :)\n\nthibs"
    author: "thibs"
  - subject: "Re:I have _the_ explication (please answer me)"
    date: 2004-10-02
    body: "For those who don't follow the bug link I have reworked KAudioCreator to not rely on the cdrom drive anymore and should solve the problem that a number of people's drives were having (too bad mine didn't or I could have caught it for3.)).  This fix is in 3.3.1 which will be out very soon. "
    author: "Benjamin Meyer"
  - subject: "Maybe just not for me"
    date: 2004-09-29
    body: "I've tried Amarok, just as I've tried Juk and Noatun, and in the end, I always\nhave to go back to either xmms or ogg123. I simply cannot figure out how to play music with Amarok. I fumble around as if it's my first day with a computer. Which is rather annoying, since I've been messing around with all kinds of computers for more than twenty years.\n\nMaybe I should make a nice mpeg video of what I'm trying to do. I found Bart's video of him using Krita very useful in assessing Krita's UI. Maybe showing in what way I am simply incapable of getting Amarok to function would help the developers. \n\n(The reason both Juk and Noatun fail to make the grade is that they skip when I'm compiling code, which is pretty much permanent with me)."
    author: "Boudewijn Rempt"
  - subject: "Re: Maybe just not for me"
    date: 2004-09-29
    body: "that is just what I felt when I started amarok: damn, how does this work??? I hate that feeling, and I dont have it very often... please, can you guys try to make the interface a bit more like itunes or juk? its much easier to have play/pause/next on top of the interface.\n\nhmmm, now I try it again - I think that's my mayor annoyance, altough the menu key is quite stupid, there - imho. I'd say make a normal menubar, put the tools now on bottom on top, and you'r interface is more KDE-like and more logical. or at least make it configurable (and use the KDE-like interface as standaard)."
    author: "superstoned"
  - subject: "Re: Maybe just not for me"
    date: 2004-09-29
    body: ">please, can you guys try to make the interface a bit more like itunes or juk?\n\nWhy not using juk instead? Why do you want amaroK to look like juK? I never use Juk, i hate Juk GUI but i'm not mailing juK devel to make his soft looking like window media player...\n\n>its much easier to have play/pause/next on top of the interface.\n\nok, good argument, so\n\nits much easier to have play/pause/next at bottom of the interface.\n\n"
    author: "Bellegarde Cedric"
  - subject: "Re: Maybe just not for me"
    date: 2004-09-30
    body: "check other interfaces. where are the buttons in kmail? kopete? konqueror? need I say more?\n\nand juk has a clean interface, its a good starting point. I like amarok, I use it, but I really dont like the way it handles playlists. I still have trouble figuring out how to do them in an easy way... in juk this was easy."
    author: "superstoned"
  - subject: "Re: Maybe just not for me"
    date: 2004-09-29
    body: "\"(The reason both Juk and Noatun fail to make the grade is that they skip when I'm compiling code, which is pretty much permanent with me).\"\n\nDo you have dma on?  (use 'hdparm /dev/hd?' )"
    author: "JohnFlux"
  - subject: "Re: Maybe just not for me"
    date: 2004-09-29
    body: "Yes... I must admit that I goofed when I specified my laptop. I should have gone for the bigger & faster hard disk, but on the other hand, xmms never skips."
    author: "Boudewijn Rempt"
  - subject: "Re: Maybe just not for me"
    date: 2004-09-29
    body: "with realtime-priority turned on and akode from kde 3.3 (instead of mpeglib) artsd can play mp3 and ogg pretty reliable. I always had dropouts on cvs up, even with xmms. akode finally has cured this here."
    author: "mETz"
  - subject: "Re: Maybe just not for me"
    date: 2004-09-29
    body: "Xmms/winamp must be the worst interface there is for a music collection.\n\nUsing only 5% of your screen-space when you have a collection of maybe over 1000 songs just does not make sense."
    author: "KDE User"
  - subject: "Re: Maybe just not for me"
    date: 2004-09-29
    body: "Well just resize the playlist window?"
    author: "XMMS user"
  - subject: "Re: Maybe just not for me"
    date: 2004-10-06
    body: "...and resize the file dialog open window. That's what I remember about playing MP3s back in the bad old days of XMMS, having to resize the open file or directory window all the time. And well, 'of course' you have to open a seperate window to select a directory in XMMS. And then the Open Directory window would take like 5 seconds to open (but I'm sure that was just a GTK1 thing).\n\nFor some folks I'm sure the XMMS UI does work for them. And amaroK can be setup to be pretty XMMS-like for those folks. "
    author: "Ian Monroe"
  - subject: "Re: Maybe just not for me"
    date: 2004-09-29
    body: "Hi Boudewijn,\n\nit distresses me to hear you find our player confusing. I'd like to address this. If you could take the time to describe what was confusing for you I'll take the time to fix it up and make amaroK perfect for you too! If you want to either reply here or write to our mailing-list, or visit us in #amarok.\n\nThanks!\n\nMax"
    author: "Max Howell"
  - subject: "Re: Maybe just not for me"
    date: 2004-09-30
    body: "I spend some time yesterday to mail Max a description of what I was doing, and true to the tradition of debugging-by-confessional discovered that I was using a backend that couldn't play oggs. Without attempting to get that backend to play oggs, I switched to another, and lo! and behold, I could play my music files. \n\nI'm still not sure that I'm among the intended audience of Amarok, because what I mostly do is drag a bunch of oggs from konqueror onto the systray icon, and and let them play, which hardly makes use of all that work on making playlists powerful"
    author: "Boudewijn Rempt"
  - subject: "Re: Maybe just not for me"
    date: 2004-10-02
    body: "Yes, I have _exactly_ the same experience. I tried a lot of those music managers, but at the end, my vast collection breaks them all.\n\neg amarok would scan on each change of a tag, and makes it use 100% then for very logn time. You can't sort after path+filename, and it rescans the tags everytime you drop them into the playlist (why then use sqllib inside?).\n\nAt the end for just playing music, I use xmms (either with arts or direct alsa) and for tagging easytag (old but works very good). The rest I do in the shell, like ripping with abcde (works just flawless) and moving around file."
    author: "Gullevek"
  - subject: "Re: Maybe just not for me"
    date: 2004-10-06
    body: "amaroK used to be really annoying about scanning files. I think its improved a lot in 1.1."
    author: "Ian Monroe"
  - subject: "Re: Maybe just not for me"
    date: 2004-10-06
    body: "I doubt, I used 1.1 in debian unstable and it was still unbearable :("
    author: "Gullevek"
  - subject: "Juk"
    date: 2004-09-29
    body: "Juk appears with cleaner interface & is such a pleasure to use. my 2 cents."
    author: "The Juker"
  - subject: "Re: Juk"
    date: 2004-09-29
    body: "Thanks for sharing this very interesting and helpful information."
    author: "anonymous"
  - subject: "Re: Juk"
    date: 2004-09-29
    body: "All this comments are just reaction of Juk fans thinking that amarok is going to replace it ...\n\nSo stupid, all I wan't to say is : \"JUK SUX, STUPID GNOME LIKE APPS WITH NO OPTION/FEATURE\"\nReally intelligent, no?\n\nBut i don't think this, i don't think amaroK should be like juK and vice versa. I don't like juk gui, i prefer amaroK ... Don't troll in this news, i you like juK use it, please..."
    author: "Bellegarde Cedric"
  - subject: "Please don't change the UI!"
    date: 2004-09-29
    body: "Don't listen to all the ppl here that says they prefer JuK, noatun, xmms or whatever. Let them use the things they prefer and keep amaroK the way it is for those of us that like it!"
    author: "Ingemar Eriksson"
  - subject: "Doesn't work - It just closes"
    date: 2004-09-29
    body: "I just installed this new version but I just can't get it to work. When I start it it says that I need to select output plugin gstreamer. And when I select it later and click Apply amarok just closes (no crash or something like that). I also tried to select arts and it also just silently closes. I'm runing this on AMD Athlon 64 in 64-bits if it has anything to do with this."
    author: "Jure Repinc"
  - subject: "Re: Doesn't work - It just closes"
    date: 2004-09-29
    body: "Yep, I would say I have the same problem here (and I use also amd64 :-) gentoo? )\n\nIt's a problem with the gstreamer method (gst_emelent_link many) as far as I can tell you (placing kdDebug code before and after that method in createPipeline()).\n\nThe funny thing is, if you start amarok for the first time, it wants you to select a gstreamer output plugin. So if one chooses for example arts or xine or whatever instead (we don't want to use gstreamer because it crashes/exits), it will call after applying the changes also the gst_element_link_many method and exits:) Although I exepcted it to switch the engine without calling gst_element_link_many again.\n\nOne possible solution:\nIf you disable gstreamer for amarok (-gstreamer) during compilation, it works here with arts..."
    author: "gogo"
  - subject: "Re: Doesn't work - It just closes"
    date: 2004-09-30
    body: "Actually, I pulled this off with switching to Xine, then switching back to GStreamer, then selecting the alsasink output plugin. Didn't close. I'm on an AMD TBird, so obviously that ain't it, but I am running gentoo."
    author: "Antonio"
  - subject: "Re: Doesn't work - It just closes"
    date: 2004-09-30
    body: "I tried to compile without gstreamer but now I get this error:\n\ng++ -DHAVE_CONFIG_H -I. -I. -I../.. -I../../amarok/src/amarokcore -I../../amarok/src/amarokcore -I../../amarok/src/analyzers -I../../amarok/src/engine -I../../amarok/src/plugin -I/usr/kde/3.3/include/arts -I/usr/include/taglib -I../../amarok/src/sqlite -I/usr/kde/3.3/include -I/usr/qt/3/include -I/usr/X11R6/include   -DQT_THREAD_SUPPORT  -D_REENTRANT  -Wnon-virtual-dtor -Wno-long-long -Wundef -ansi -D_XOPEN_SOURCE=500 -D_BSD_SOURCE -Wcast-align -Wconversion -Wchar-subscripts -Wall -W -Wpointer-arith -Wwrite-strings -DNDEBUG -DNO_DEBUG -O2 -march=athlon64 -pipe -O2 -fomit-frame-pointer -frename-registers -Wformat-security -Wmissing-format-attribute -fno-exceptions -fno-check-new -fno-common -DQT_CLEAN_NAMESPACE -DQT_NO_ASCII_CAST -DQT_NO_STL -DQT_NO_COMPAT -DQT_NO_TRANSLATION  -c -o collectionbrowser.o `test -f 'collectionbrowser.cpp' || echo './'`collectionbrowser.cpp\ng++ -DHAVE_CONFIG_H -I. -I. -I../.. -I../../amarok/src/amarokcore -I../../amarok/src/amarokcore -I../../amarok/src/analyzers -I../../amarok/src/engine -I../../amarok/src/plugin -I/usr/kde/3.3/include/arts -I/usr/include/taglib -I../../amarok/src/sqlite -I/usr/kde/3.3/include -I/usr/qt/3/include -I/usr/X11R6/include   -DQT_THREAD_SUPPORT  -D_REENTRANT  -Wnon-virtual-dtor -Wno-long-long -Wundef -ansi -D_XOPEN_SOURCE=500 -D_BSD_SOURCE -Wcast-align -Wconversion -Wchar-subscripts -Wall -W -Wpointer-arith -Wwrite-strings -DNDEBUG -DNO_DEBUG -O2 -march=athlon64 -pipe -O2 -fomit-frame-pointer -frename-registers -Wformat-security -Wmissing-format-attribute -fno-exceptions -fno-check-new -fno-common -DQT_CLEAN_NAMESPACE -DQT_NO_ASCII_CAST -DQT_NO_STL -DQT_NO_COMPAT -DQT_NO_TRANSLATION  -c -o collectiondb.o `test -f 'collectiondb.cpp' || echo './'`collectiondb.cpp\ng++ -DHAVE_CONFIG_H -I. -I. -I../.. -I../../amarok/src/amarokcore -I../../amarok/src/amarokcore -I../../amarok/src/analyzers -I../../amarok/src/engine -I../../amarok/src/plugin -I/usr/kde/3.3/include/arts -I/usr/include/taglib -I../../amarok/src/sqlite -I/usr/kde/3.3/include -I/usr/qt/3/include -I/usr/X11R6/include   -DQT_THREAD_SUPPORT  -D_REENTRANT  -Wnon-virtual-dtor -Wno-long-long -Wundef -ansi -D_XOPEN_SOURCE=500 -D_BSD_SOURCE -Wcast-align -Wconversion -Wchar-subscripts -Wall -W -Wpointer-arith -Wwrite-strings -DNDEBUG -DNO_DEBUG -O2 -march=athlon64 -pipe -O2 -fomit-frame-pointer -frename-registers -Wformat-security -Wmissing-format-attribute -fno-exceptions -fno-check-new -fno-common -DQT_CLEAN_NAMESPACE -DQT_NO_ASCII_CAST -DQT_NO_STL -DQT_NO_COMPAT -DQT_NO_TRANSLATION  -c -o configdialog.o `test -f 'configdialog.cpp' || echo './'`configdialog.cpp\ng++ -DHAVE_CONFIG_H -I. -I. -I../.. -I../../amarok/src/amarokcore -I../../amarok/src/amarokcore -I../../amarok/src/analyzers -I../../amarok/src/engine -I../../amarok/src/plugin -I/usr/kde/3.3/include/arts -I/usr/include/taglib -I../../amarok/src/sqlite -I/usr/kde/3.3/include -I/usr/qt/3/include -I/usr/X11R6/include   -DQT_THREAD_SUPPORT  -D_REENTRANT  -Wnon-virtual-dtor -Wno-long-long -Wundef -ansi -D_XOPEN_SOURCE=500 -D_BSD_SOURCE -Wcast-align -Wconversion -Wchar-subscripts -Wall -W -Wpointer-arith -Wwrite-strings -DNDEBUG -DNO_DEBUG -O2 -march=athlon64 -pipe -O2 -fomit-frame-pointer -frename-registers -Wformat-security -Wmissing-format-attribute -fno-exceptions -fno-check-new -fno-common -DQT_CLEAN_NAMESPACE -DQT_NO_ASCII_CAST -DQT_NO_STL -DQT_NO_COMPAT -DQT_NO_TRANSLATION  -c -o contextbrowser.o `test -f 'contextbrowser.cpp' || echo './'`contextbrowser.cpp\ncollectiondb.cpp:1342: warning: unused parameter 'parent'\ncollectiondb.cpp:1342: warning: unused parameter 'artist'\ncollectiondb.cpp:1342: warning: unused parameter 'album'\ncollectiondb.cpp:1342: warning: unused parameter 'noedit'\ng++ -DHAVE_CONFIG_H -I. -I. -I../.. -I../../amarok/src/amarokcore -I../../amarok/src/amarokcore -I../../amarok/src/analyzers -I../../amarok/src/engine -I../../amarok/src/plugin -I/usr/kde/3.3/include/arts -I/usr/include/taglib -I../../amarok/src/sqlite -I/usr/kde/3.3/include -I/usr/qt/3/include -I/usr/X11R6/include   -DQT_THREAD_SUPPORT  -D_REENTRANT  -Wnon-virtual-dtor -Wno-long-long -Wundef -ansi -D_XOPEN_SOURCE=500 -D_BSD_SOURCE -Wcast-align -Wconversion -Wchar-subscripts -Wall -W -Wpointer-arith -Wwrite-strings -DNDEBUG -DNO_DEBUG -O2 -march=athlon64 -pipe -O2 -fomit-frame-pointer -frename-registers -Wformat-security -Wmissing-format-attribute -fno-exceptions -fno-check-new -fno-common -DQT_CLEAN_NAMESPACE -DQT_NO_ASCII_CAST -DQT_NO_STL -DQT_NO_COMPAT -DQT_NO_TRANSLATION  -c -o coverfetcher.o `test -f 'coverfetcher.cpp' || echo './'`coverfetcher.cpp\ng++ -DHAVE_CONFIG_H -I. -I. -I../.. -I../../amarok/src/amarokcore -I../../amarok/src/amarokcore -I../../amarok/src/analyzers -I../../amarok/src/engine -I../../amarok/src/plugin -I/usr/kde/3.3/include/arts -I/usr/include/taglib -I../../amarok/src/sqlite -I/usr/kde/3.3/include -I/usr/qt/3/include -I/usr/X11R6/include   -DQT_THREAD_SUPPORT  -D_REENTRANT  -Wnon-virtual-dtor -Wno-long-long -Wundef -ansi -D_XOPEN_SOURCE=500 -D_BSD_SOURCE -Wcast-align -Wconversion -Wchar-subscripts -Wall -W -Wpointer-arith -Wwrite-strings -DNDEBUG -DNO_DEBUG -O2 -march=athlon64 -pipe -O2 -fomit-frame-pointer -frename-registers -Wformat-security -Wmissing-format-attribute -fno-exceptions -fno-check-new -fno-common -DQT_CLEAN_NAMESPACE -DQT_NO_ASCII_CAST -DQT_NO_STL -DQT_NO_COMPAT -DQT_NO_TRANSLATION  -c -o covermanager.o `test -f 'covermanager.cpp' || echo './'`covermanager.cpp\ncovermanager.cpp: In member function `void CoverManager::showCoverMenu(QIconViewItem*, const QPoint&)':\ncovermanager.cpp:379: error: `FETCH' undeclared (first use this function)\ncovermanager.cpp:379: error: (Each undeclared identifier is reported only once for each function it appears in.)\nmake[4]: *** [covermanager.o] Error 1\nmake[4]: *** Waiting for unfinished jobs....\nmake[4]: Leaving directory `/var/tmp/portage/amarok-1.1/work/amarok-1.1/amarok/src'\nmake[3]: *** [all-recursive] Error 1\nmake[3]: Leaving directory `/var/tmp/portage/amarok-1.1/work/amarok-1.1/amarok/src'\nmake[2]: *** [all-recursive] Error 1\nmake[2]: Leaving directory `/var/tmp/portage/amarok-1.1/work/amarok-1.1/amarok'\nmake[1]: *** [all-recursive] Error 1\nmake[1]: Leaving directory `/var/tmp/portage/amarok-1.1/work/amarok-1.1'\nmake: *** [all] Error 2"
    author: "Jure Repinc"
  - subject: "Re: Doesn't work - It just closes"
    date: 2004-10-02
    body: "That's a bug that is fixed in amarok cvs.\nBut it comes from not using \"amazon\" with amarok, \nbut the amarok ebuild is(was!?) broken anyway \nwhen it comes to the \"amazon\" use."
    author: "gogo"
  - subject: "Wish..."
    date: 2004-09-29
    body: "hi, i wish, \"amarok\" will get features like \"Helium\" has got ( http://www.helium2.com ).\namarok seems to become very nice, altough i have got a lot of crashes when using version 1.1 especially when i try opening streams while using arts-engine, but also xine-engine doesn't work with streams (for me).\n\nthanx, and keep on the nice work..\n"
    author: "mononoke"
  - subject: "I don't get it"
    date: 2004-09-29
    body: "I just don't get it - why is the big list on the right empty after clicking on 'Current playlist' in the 'Playlists' tab? Is that a bug or am I too confused?\n\nI thought it would work like that: browse through collection, drag a few files from here and there into the playlist, doubleclick on one to play it immediately, and the playlist would persist during at least the current session. It should be possible to shuffle stuff around in the playlist by dragging it up and down, remove it by hitting the delete button when selected and so on. All that works fine as expected, but the 'Current Playlist' does not. I thought when doubleclicking on other playlists in the 'Playlists' tab like 'Cool Streams' I would switch to that other playlist but I can switch back to what I previously assembled by clicking on 'Current Playlist', but that doesn't work - the other playlist is displayed in the big window on the right, but when going back the current playlist is always empty.\n\nAnyone care to explain?\n\n(amarok 1.1 from Kalyxo Debian package)"
    author: "til"
  - subject: "Re: I don't get it"
    date: 2004-09-29
    body: "I think this is a typical JuK users issues with amaroK. We don't work the same way as JuK at all. The playlist on the right is static, not dynamic.\n\nHmm, I need to think about how to address this."
    author: "Max Howell"
  - subject: "Re: I don't get it"
    date: 2004-09-29
    body: "I've given up on juk after a short try some months ago, so I'm not a typical juk user, rather a 'drag and drop from konqueror into xmms playlist' type. From this rudimentary interface I'm very used to having one dynamically during the session assembled playlist. Having the possibility to deal with more playlists looks like a cool feature to me, but then I would expect 'current playlist' to be the one I assembled before and the others saved ones. If it is not, what else is 'current playlist'? Why is it always empty?\n\nBtw, when clicking on 'current playlist' it displays the empty list but the current song continues to play and I have no idea how to make it appear in the big list on the right again.\n\nCould you elaborate what you mean with static? If it is meant to always stay there I would understand it, but then the 'current playlist' always being empty is clearly a bug, no?"
    author: "til"
  - subject: "Re: I don't get it"
    date: 2004-09-30
    body: "The current playlist item is a bit of a hack, and apparently a big usability issue. It's there so you can easily add the current playlist to the playlist-browser list. Clicking it doesn't go back to what you had before. It wouldn't be current then anyway. I think we need to remove that from the browser.\n\nIt appears empty because if you click it it just loads what you alredy have in the playlist.\n\nYeah it should be removed init. We can have a toolbar button instead."
    author: "Max Howell"
  - subject: "Re: I don't get it"
    date: 2004-09-30
    body: "Thanks for the answer! I'm still confused about the meaning of the button but at least there's hope that after the change I'll understand it.\n\nSo am I right in assuming that the list in the big box on the right should be considered very volatile and not something that can be assembled steadily during a session? How could I achieve the following in amarok: select a few songs from my collection, while it is playing add some more songs from the (very slick) recently played list, then add some more from the collection? Would that work with a saved playlist?"
    author: "til"
  - subject: "Re: I don't get it"
    date: 2004-09-30
    body: "The playlist on the right is winamp like. You build it steadily during the session.\n\nDrag and drop is the main method we recommend for using amaroK, also right click on stuff and you can \"append to the playlist\".\n\nThe playlist-browser should append by default really as everything else in amaroK works this way. I think it was changed to be more like JuK and this is a mistake destined to cause confusion."
    author: "Max Howell"
  - subject: "amarok is good"
    date: 2004-09-29
    body: "i really don't know how people can compare juk and amarok and say that juk's better. for me it's not. i tried it once and it was terrible. amarok interface is really clean and simple to use. maybe control buttons could be moved to the top, as most peaple are used to itunes, rythmbox and other. but collection viewer, file browser, search and last but not least context viewer are just awesome!\nbest regards\nyemu"
    author: "yemu"
  - subject: "AmaroK is great"
    date: 2004-09-29
    body: "AmaroK just saved me 50$... my Asus K8V SE Deluxe card isn't very well supported by aRtsd, so I where going to by me a new SB Live 5.1 just to listen to my music...\nBut then I discovered that Xine played DVD's just fine, so I just recompled AmaroK with xine-support, and now it works like a charm...\n\nMorten"
    author: "Morten Fangel"
  - subject: "Re: AmaroK is great"
    date: 2004-09-29
    body: "that's what you deserve for using crappy onboard hardware."
    author: "mETz"
  - subject: "Re: AmaroK is great"
    date: 2004-10-06
    body: "it ain't the 90s anymore, sound cards (or in this case, a sound chip) don't need to be spiffy to do everything you'd ever need them for."
    author: "Ian Monroe"
  - subject: "KHotKeys"
    date: 2004-09-29
    body: "\"Multimedia Keyboard Friendly: If you happen to have extra multimedia buttons on your keyboard, using amaroK is even more fun. Just use KHotkeys to map those special buttons to amaroK's dcop functions (like play/pause/volume up/volume down and many more)!\"\n\nWhy not use amaroK's global shortcuts?"
    author: "Qerub"
  - subject: "Re: KHotKeys"
    date: 2004-09-30
    body: "Every kde app can use these keys, not only amarok."
    author: "mETz"
  - subject: "amaroK review with screenshots at Linux Reviews"
    date: 2004-09-29
    body: "http://linuxreviews.org/software/media-sound/amarok/\n\n:-)\n"
    author: "xiando"
  - subject: "too much of a good thing"
    date: 2004-09-29
    body: "Like the majority here, I've been using amarok for a while. I always hated xmms... gtk... file dialogs... painful skin... ugh. Noatun is okay really; but why on earth does it load so slowly. kplayer and kaffiene are quite good too at playing; but their interfaces are more geared towards video. JuK was a giant leap forward, and is still uniquely good in a few ways, but I always found it very heavy. The scanning of my audio many large folders always would kill my system in the past. \n\nAmarok suddenly appeared. Nice new ideas. Interface took a little bit to get used to (especially the weirdness of not being able to save/organize playlists). But generally it was just a pleasure of simplicity, with loads of wow factor (like when i discovered it was keeping stats on what files i played, and ranking them). \n\nNow my complaints! I still find amarok pretty heavy in many ways. I actually use it's own colour scheme, it's pretty nice: but it's absolutely annoying the way the selection bar in the playlists insists on forever fading in and out. I wish i could turn off the infernal title scrolling in the player window. (Just like animated GIFs on web pages are usually annoying, this stuff annoys me.) And also while the level display embedded in the bottom of the playlist is extremely nifty, i'd like to turn it off too. I find that just having amarok running, not even playing anything, increases my system load more than any other player, which annoys me. Yes it only increases it slightly; a few tenths or so, but still... it's needless. I like my cpuload display to be as flat as possible when i'm not doing anything on the system.\n\nBut ... well, amarok is still the best for me for now.\n\n"
    author: "Tim Middleton"
  - subject: "Re: too much of a good thing"
    date: 2004-09-30
    body: "You can turn off the player-window and you can remove the analyzer from the toolbar. You've always been able to save playlists - I don't quite understand what you mean by \"especially the weirdness of not being able to save/organize playlists\".\n\nWe like the flashy stuff, but considering how many people seem intolerant to it, are starting to consider making it optional. Frankly I don't understand why people spend enough time looking at it get annoyed. I tend to minimise amaroK pretty quickly when I'm just using it (rather than developing it). \n\nI'd be surprised if just having amaroK running increases your CPU at all. I'm very sensitive to that kind of thing and spent quite some time hunting out stray timers and disabling them when amaroK is hidden.\n\nThanks for the feedback, any feature requests?"
    author: "Max Howell"
  - subject: "Re: too much of a good thing"
    date: 2004-09-30
    body: "I appreciate you response to me, and others I have seen above. It's clear that you love your project a lot, as well as respect the (even sometimes negative) perspective of others. Very admirable.\n\nI haven't checked what you say yet, but I if you say it it must be true. It may indicate that there is indeed some room for usability improvement/redesign in the interface to make things more intuitive. Many have said this above; though some of them I don't agree with on some points. But my comments above are based on my using amaroK for months and months now as my primary audio player. And I somehow missed the options to save play lists and turn off some of the animations? Hmmm.\n\nAs for feature requests: perhaps it already is one... but the reason i keep the player window open (and am afflicted by the scrolling titles <-:) is because I like having the player controls (and occassionally actually one or other of the  alternate analyzers available, though i usually turn it off). Need to move player controls onto the playlist window somewhere. Yeah, i know you'll probably say just use right click on the kicker icon to get controls... but that's too many clicks on an areas of the screen i'm not likely to be. <-; \n\nYou may also tell me to just use the global hot keys... fair enough. But i just prefer the player buttons for some bizarre reason. I guess i'm just used to them. Up to you whether you want to make your app more comfortable for people stuck in their ways, or make people conform more entirely to your way. I know by all the stuff you have thrown in you do try to do a bit of both.\n\nWow, you can save play lists! How bizarre; all this time, I never actually really considered those icons on bottom left of the playlist window. They don't seem \"attached\" to the playlist to me somehow, being over there underneath the pannel. Simple suggestion: place the option on the context menu when right clicking on the actual playlist. Or maybe move the buttons to the right side to the left of the \"menu\" button.\n\nI still can't figure out how to remove the analizer from the tool bar though.  I found the frame rate selector. But if i cared *that* much i'd probably have complained on a mailing list by now or something. <-;\n\nI also should apologize that all my comments are based on 1.0 ... maybe some little things have changed in 1.1 which this thread is attached to the announcement of. (FreeBSD is currently in ports freeze for the impending 5.3 release, so an easy amaroK build hasn't been updated for us yet.)\n\nAnyhow great work. My complaints are not signficant. Just keep improving everything for as long as you can, is all we can really ask. (-:\n\nP.S. you are also right in that amarok does pretty much not take any cpu when minimised (ie. existing only in kicker). I guess i just have to train myself to minimize it. I tend to just use ALT/CTL-TAB to switch between apps/desktops... don't usually minimize anything normally. \n"
    author: "Tim Middleton"
  - subject: "Re: too much of a good thing"
    date: 2004-09-30
    body: "I think we need to cater more to the non-flashy crowd. A single option to disable flashy stuff is something other developers have suggested before. I'll propose it on the mailing-list, probably to much surprise since it is usually me who says \"naaa..\" ;-)\n\nYou can already have the player-controls in the playlist, we cater for two main interface options. The first we call XMMS-mode, the second is COMPACT-mode. You can switch between them using the first-run wizard. Compact-mode is only the playlist-window with a statusbar and a more complete (ie playback buttons) toolbar. I think we need to make this switch behaviour available in the configure dialog. For 1.0 you can adjust this using the Welcome-tab (replaced by the wizard in 1.1).\n\nYou are right that the playlist toolbar should be under the playlist. I will shortly try this out to see how it feels. Hopefully it'll seem so right the other developers will like it too.\n\nThanks for the suggestion to have a hide-this-analyzer option in the playlist-analyzer's context-menu! I have no idea why it didn't occur to me to add this in the first place. Currently you have to configure the toolbar to remove it. Yes.. I know this is not intuitive, but you see, it's in the toolbar so it kind of makes sense ;-)\n\namaroK should stop using extra CPU when on different desktops too, since KWin sends a hideEvent to the application when you switch desktop. But I'll investigate.\n\nThanks for the suggestions! :-)"
    author: "Max Howell"
  - subject: "Skips with gstreamer/alsa"
    date: 2004-09-30
    body: "I'm using amarok 1.1 from the kalyxo debian package with gstreamer 0.8 outputting to alsa and experience short skips frequently in cpu usage peaks, e.g. when konqueror renders a complicated page. Any well known problems with this setup?\n\nThis is a recent pentium M machine that definitely should be capable of playing uninterrupted. xmms outputting directly to alsa works fine, even under higher load."
    author: "til"
  - subject: "Re: Skips with gstreamer/alsa"
    date: 2004-10-10
    body: "Thats a gstreamer bug that is fixed in the last version.\n\nregards\n Daniel"
    author: "Daniel Poelzleithner"
  - subject: "never been able to get it to work"
    date: 2004-09-30
    body: "I'd love to join in the praise/criticism of amaroK, but I've been completely unable to get it to play anything. I've been trying each new version for the last 2 or 3 months, and I always have the exact same problem....namely that everytime I try to use amaroK to play an mp3, it starts up, splash screen stays for ages, and then I get a message saying amaroK could not be started, try running it from the command line (this is while the blue amaroK speaker icon is in my systray btw)\n\nNow, I'm not necessarily saying that it's amaroK itself - I mean on my system I'm running LFS, with everything freshly compiled from source - so it could be that I've not compiled something correctly. Also, I run KDE cvs, updating every few days... so again, that could be the problem.\n\nAny advice, hints etc.. would be appreciated.\n\nPS\n\nxmms, juk, and other media players seem to work ok"
    author: "Gogs"
  - subject: "Re: never been able to get it to work"
    date: 2004-09-30
    body: "Try running the amarokapp binary instead. amaroK startup depends on some IPC that you may have disabled with your LFS (?)."
    author: "Max Howell"
  - subject: "Re: never been able to get it to work"
    date: 2004-10-01
    body: "Tried that....\n\nbash-3.00# amarokapp Blondie\\ -\\ Call\\ Me.mp3\nkdecore (KConfigSkeleton): Creating KConfigSkeleton (0x827c7a0)\nkdecore (KConfigSkeleton): KConfigSkeleton::readConfig()\namarok: BEGIN [static EngineBase* EngineController::loadEngine()]\namarok: [PluginManager] Plugin trader constraint: [X-KDE-amaroK-framework-version] == 3 and [X-KDE-amaroK-plugintype] == 'engine' and [X-KDE-amaroK-name] == 'void-engine' and [X-KDE-amaroK-rank] > 0\nkio (KSycoca): Trying to open ksycoca from /home/gogs/.kde/cache-insp8200/ksycoca\nkio (KTrader): query for amaroK/Plugin : returning 1 offers\namarok: [PluginManager] Trying to load: libamarok_void-engine_plugin\nkdecore (KLibLoader): Loading the next library global with flag 257.\namarok: [amaroK::Plugin::Plugin(bool)]\namarok:\namarok: PluginManager Service Info:\namarok: ---------------------------\namarok: name                          : <no engine>\namarok: library                       : libamarok_void-engine_plugin\namarok: desktopEntryPath              : amarok_void-engine_plugin.desktop\namarok: X-KDE-amaroK-plugintype       : engine\namarok: X-KDE-amaroK-name             : void-engine\namarok: X-KDE-amaroK-authors          : (Max Howell,Mark Kretschmann)\namarok: X-KDE-amaroK-rank             : 1\namarok: X-KDE-amaroK-version          : 1\namarok: X-KDE-amaroK-framework-version: 3\namarok:\namarok: END [static EngineBase* EngineController::loadEngine()]\namarok: [browserBar] Initialisation statistics:\namarok:   Init: ContextBrowser\namarok: [ContextBrowser::ContextBrowser(const char*)]\nkhtml (xml):  using compatibility parseMode\namarok: KMultiTabBarTab::updateState(): setting text to an empty QString***************\namarok:     Time: 0.04s\namarok:   Init: CollectionBrowser\namarok: [CollectionView::CollectionView(CollectionBrowser*)]\namarok: [void CollectionView::renderView()]\namarok: KMultiTabBarTab::updateState(): setting text to an empty QString***************\namarok:     Time: 0.02s\namarok:   Init: PlaylistBrowser\namarok: KMultiTabBarTab::updateState(): setting text to an empty QString***************\namarok:     Time: 0.03s\namarok:   Init: SearchBrowser\namarok: KMultiTabBarTab::updateState(): setting text to an empty QString***************\namarok:     Time: 0.01s\namarok:   Init: FileBrowser\nkio (KDirLister): +KDirLister\nkio (KDirListerCache): +KDirListerCache\nkio (KDirWatch): Available methods: Stat\nkio (KDirLister): +KDirLister\nkio (KDirLister): -KDirLister\nkio (KDirLister): [virtual void KDirLister::stop()]\nkio (KDirListerCache): [void KDirListerCache::stop(KDirLister*)] lister: 0x83a9b40\nkio (KDirListerCache): [void KDirListerCache::forgetDirs(KDirLister*)] 0x83a9b40\nkio (KDirLister): [virtual bool KDirLister::openURL(const KURL&, bool, bool)] file:/home/gogs/ keep=false reload=false\nkio (KDirListerCache): [void KDirListerCache::listDir(KDirLister*, const KURL&, bool, bool)] 0x83b77b8 url=file:/home/gogs keep=false reload=false\nkio (KDirListerCache): [void KDirListerCache::stop(KDirLister*)] lister: 0x83b77b8\nkio (KDirListerCache): [void KDirListerCache::forgetDirs(KDirLister*)] 0x83b77b8\nkio (KDirListerCache): listDir: Entry not in cache or reloaded: file:/home/gogs\nkio (KDirWatch): Added Dir /home/gogs [KDirWatch-1]\nkio (KDirWatch): Global Poll Freq is now 500 msec\nkio (KDirWatch):  Started Polling Timer, freq 500\nkio (KDirWatch):  Setup Stat (freq 500) for /home/gogs\nkdecore (KAction): WARNING: KAction::plug(): has no KAccel object; this = 0x83bb2b8 name = add_bookmark parentCollection = (nil)\namarok: KMultiTabBarTab::updateState(): setting text to an empty QString***************\namarok:     Time: 0.04s\namarok: BEGIN [void App::applySettings(bool)]\namarok: Discovered 170 widgets in PlaylistWindow\namarok: BEGIN [static EngineBase* EngineController::loadEngine()]\namarok: [PluginManager] Plugin trader constraint: [X-KDE-amaroK-framework-version] == 3 and [X-KDE-amaroK-plugintype] == 'engine' and [X-KDE-amaroK-name] == 'gst-engine' and [X-KDE-amaroK-rank] > 0\nkio (KTrader): query for amaroK/Plugin : returning 1 offers\namarok: [PluginManager] Trying to load: libamarok_gstengine_plugin\nkdecore (KLibLoader): Loading the next library global with flag 257.\namarok: [amaroK::Plugin::Plugin(bool)]\namarok: [GstEngine::GstEngine()]\namarok:\namarok: PluginManager Service Info:\namarok: ---------------------------\namarok: name                          : GStreamer Engine\namarok: library                       : libamarok_gstengine_plugin\namarok: desktopEntryPath              : amarok_gstengine_plugin.desktop\namarok: X-KDE-amaroK-plugintype       : engine\namarok: X-KDE-amaroK-name             : gst-engine\namarok: X-KDE-amaroK-authors          : (Mark Kretschmann)\namarok: X-KDE-amaroK-rank             : 255\namarok: X-KDE-amaroK-version          : 1\namarok: X-KDE-amaroK-framework-version: 3\namarok:\namarok: BEGIN [virtual bool GstEngine::init()]\nkdecore (KConfigSkeleton): Creating KConfigSkeleton (0x844a0e8)\nkdecore (KConfigSkeleton): KConfigSkeleton::readConfig()\namarok: Thread scheduling priority: 2\namarok: Sound output method: alsasink\namarok: CustomSoundDevice: false\namarok: Sound Device:\namarok: CustomOutputParams: false\namarok: Output Params:\namarok: END [virtual bool GstEngine::init()]\namarok: [static void PluginManager::unload(amaroK::Plugin*)]\namarok: [virtual amaroK::Plugin::~Plugin()]\namarok: [PluginManager] Unloading library: libamarok_void-engine_plugin\namarok: END [static EngineBase* EngineController::loadEngine()]\namarok: END [void App::applySettings(bool)]\namarok: [App] Pruned 0 of 1 amazon cover images.\namarok: file:/mnt/hdc2/home/gogs/normal mp3/Blondie/Blondie - Call Me.mp3\nQThread object destroyed while thread is still running.\nkio (KDirListerCache): [void KDirListerCache::slotEntries(KIO::Job*, const KIO::UDSEntryList&)] new entries for file:/home/gogs\nkio: KSambaShare: Could not found smb.conf!\nkio: KNFSShare: Could not found exports file!\nkio (KDirListerCache): [void KDirListerCache::slotResult(KIO::Job*)] finished listing file:/home/gogs\n\nNow I have the little blue amaroK speaker icon in my systray, but no sound. I know (or at least I'm assuming) that alsa is working ok, because I'm using xmms with the alsa output plugin, and that works fine\n"
    author: "Gogs"
  - subject: "Re: never been able to get it to work"
    date: 2004-10-02
    body: "So amaroK loads up OK? Click the little blue icon and configure amaroK. If you have no sound then the gst-engine is outputting to a sink plugin that doesn't actually give any PCM to your sound card. I suggest you read our faq, there is a section that explains how to test gstreamer with the various output plugins. Alternatively use the xine-engine, in our experience the xine-engine \"just works (tm)\"."
    author: "Max Howell"
  - subject: "Re: never been able to get it to work"
    date: 2004-10-03
    body: "/me hangs head in shame. I did as you suggested, and discovered that there was a part of gstreamer that wasn't installed - namely the ffmpeg bit. Once I installed that, amaroK played ok. The sound skipped a bit, and I still on occasion got the message that amaroK could not be started (while it was running), but given my kindergarten attempts at setting up gstreamer, I'll assume that that is my fault as well. :o)\n\nThanks for taking the time to answer my question, and for trying to help\n\nGogs"
    author: "Gogs"
  - subject: "Re: never been able to get it to work"
    date: 2004-10-24
    body: "the ffmpeg plugin ain't that good either. pick the libmad plugin and you'll be really happy :-)\n\nregards,\nmuesli"
    author: "muesli"
  - subject: "Stuck with XMMS"
    date: 2004-09-30
    body: "I'm still using xmms sometimes because arts dosen't have some plugins I like for playing videogames songs like spc (super nintendo) and psf (playstation)"
    author: "Iuri Fiedoruk"
  - subject: "Re: Stuck with XMMS"
    date: 2004-10-01
    body: "Try using amaroK with GStreamer, which has a ModPlug plug-in.  That should handle most of them (though not zipped ones, I believe).\n"
    author: "Astatine"
  - subject: "Re: Stuck with XMMS"
    date: 2005-06-04
    body: "PSF, SPC etc. aren't SoundTracker files (which is what modplug plays). They are direct rips of the sound from a video game (the players tend to have a stripped down emulator in them).\nThere is possibly a GStreamer port of HighlyExperimental (PSF/PSF2 player) in the works, according to it's homepage (http://www.brownjava.org/software/he-xmms). As for SPC, NSF, GYM etc., somebody has to write a gstreamer plugin for them (I'd try myself, except I have no experience of gstreamer ^_^;;)\n"
    author: "Paul Nolan"
  - subject: "Re: Stuck with XMMS"
    date: 2005-08-04
    body: "Yes, someone HAS to write a gstreamer plugin for NSF, SPC and PSF(2)!\nBecause I love amaroK and hat to use xmms for those formats.\n\nI have absolutly no experiance in codec programming, but maybe I try it on my own (and fail and cry...)."
    author: "panzi"
  - subject: "Re: Stuck with XMMS"
    date: 2005-12-08
    body: "Isn't there any news about this topic?\nI heared it WAS possible to use xmms-plugins with older versions of gstreamer, but there is a glib version conflict with newer versions (this is the same reason why those old xmms-plugins won't work with bmp).\n\nSo what we need is at least someone to port the xmms-plugins to a new glib version so the gstreamer-xmms plugin works again.\n\nbtw. I just found this:\nhttp://grammarian.homelinux.net/~kde-cvs/kfile_spc/\n\"Lately clee's SPC plugin for gstreamer has been integrated into their development version, so hopefully we will have SPC support in a major sound system in time for KDE 4. :-)\""
    author: "panzi"
  - subject: "Sounds great."
    date: 2004-10-01
    body: "I haven't tried it yet, but when I can reboot back to Linux, I'll give it a shot.  I'm completely fed up with Noatun, KPlayer works -ok- for video, so I've been using Juk.  There are a few things with Juk I would like to change, but I just don't have the time ATM.  From the little I've read and screenshots of amaroK, it looks like maybe I don't have too. :)\n\nJust from a users persepective, later today I'll give it a shot, and give my own little review -- someone MAY care :)\n"
    author: "Super Pet Troll"
  - subject: "The best player ever :)"
    date: 2004-10-01
    body: "I dunno about you guys but playlist and how it is organized is most important to me. AmaroK is simply superior here. I love the possibility to add album covers. All other features are intelligently implemented.\n\nExcellent job developers."
    author: "wygiwyg"
  - subject: "amarok is good"
    date: 2004-10-03
    body: "as the most people only write if they want to complain , and no-one writes if they like the app , i thought i will write that amarok is really nice and has a excellent interface ! \n\ndont change , i think most people like it - you can't count the comments here as the impression of *all* users , just the one that complain."
    author: "chris"
  - subject: "Replacing XMMS? I doupt that!"
    date: 2004-10-03
    body: "AmaroK is nowhere near replacing XMMS. It's probably more usable, but it can't handle exotic file formats, like tracker or C-64 sound files, and XMMS really rocks with its other plugins too. So stop that nonsense!"
    author: "Jouni H."
  - subject: "Re: Replacing XMMS? I doupt that!"
    date: 2004-10-03
    body: "By this I didn't mean to mock the program. It's excellent, and I use it every day to listen internet radio. But it doesn't compete with XMMS. They're on different fields."
    author: "Jouni H."
  - subject: "Re: Replacing XMMS? I doupt that!"
    date: 2004-10-06
    body: "Actually that's not quite correct. I haven't used AmaroK myself, but I understand it can use xine as a backend. Support for tracker formats was added to xine-lib about 6 months ago (through the use of libmodplug), so in theory AmaroK should be able to play tracker files if your xine-lib was compiled with libmodplug support enabled."
    author: "Paul Eggleton"
---
Before <a href="http://amarok.kde.org/">amaroK</a> was born, most KDE users were stuck with XMMS; others would even run console-based audio players. Now those days are over. amaroK, written for the KDE desktop environment with its slick GUI and plugable, engine-independent audio capabilities is coming to a computer near you!



<!--break-->
<p>
amaroK is the first KDE application to use the <a href="http://gstreamer.freedesktop.org/">GStreamer</a> Multimedia Framework without any dependency on external bindings. amaroK can also integrate with <a href="http://xinehq.de/">xine</a> so you have the freedom of choosing your own flavor. 
With version 1.1 there are many exciting changes that make using amaroK even more fun. Here are some of the features that you will simply love :
</p>

<ul>

<li>
Fully Integrated with KDE: Play from your Samba share with smb:/, or use the fish KIO slave to play from a remote host.
</li>

<li>
Simple and Elegant: Collection-based system where you can choose songs from a variety of criteria, including artist, most played, never played, etc.
</li>

<li>
Stream Support: Listening to online radio has never been easier. Just load the playlist file and it starts playing.
</li>

<li>
Engine Independence: Ability to use GStreamer or xine. (There is also aRts engine but it is not ported to the new engine architecture yet).
</li>

<li>
Improved Crossfade: Crossfading with customizable fade-in/fade-out durations now works for GStreamer engine too
</li>

<li>
Inline tag editing: Tag your music while you play it.
</li>

<li>
MusicBrainz support for tag editing: No voodoo needed for tagging your music, just click the MusicBrainz button and it will get the song information from the online database.
</li>

<li>
Album cover support: If you have album covers in your music directory amaroK will automatically load them - better yet, amaroK can automatically download album covers from the Internet!
</li>

<li>
Multimedia Keyboard Friendly: If you happen to have extra multimedia buttons on your keyboard, using amaroK is even more fun. Just use KHotkeys to map those special buttons to amaroK's dcop functions (like play/pause/volume up/volume down and many more)!
</li>

<li>
On-screen Display: amaroK can optionally announce the currently playing song using its smart and stylish on-screen display.
</li>

</ul>

<p>
So give amaroK a spin today! You won't be disappointed and that is guaranteed. Fire up amaroK, start playing your favorite songs, and visit the amaroK development squad @ irc.kde.org #amarok. Tell us why you just love amaroK, or your killer new idea for it. We are waiting for you!
</p>

