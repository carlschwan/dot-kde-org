---
title: "Preview of New Qt Book \"Practical Qt\" at KDE Conference 2004"
date:    2004-08-26
authors:
  - "ateam"
slug:    preview-new-qt-book-practical-qt-kde-conference-2004
comments:
  - subject: "More info here"
    date: 2004-08-26
    body: "There's some more info on the kind of thing you can expect from the book on the aKademy wiki:\n\nhttp://wiki.kde.org/tiki-index.php?page=Doing+Things+with+Qt+that+Qt+Wasn%27t+Made+For+Talk"
    author: "Richard Moore"
  - subject: "Re: More info here"
    date: 2004-08-26
    body: "Here is the toc and some sample chapters\nhttp://www.dpunkt.de/buch/3-89864-280-1.html"
    author: "J\u00f6rn Ahrens"
  - subject: "Re: More info here"
    date: 2004-08-27
    body: "Fantastic.  I love the wide coverage.  For newbie or even experience Qt programmers I would pick this up.  I wish I had this book 7 years ago when I started working on Qt.  It covers all sorts of things that I have had to figure out the hard way on my own or my seeing others implement cleanly rather then my crude hack.  Some highlights that even though it is in the Qt documention I have been asked on more then one occasion and am very happy to see:\n\nHiding header in a list\nTooltips in a list\nLinks in textedit\nContext-sensitive menus\nIcon for the main window\nHow to Quit\nMultiple buttons and or menu items to one slot\nMaking *cross platform* Libraries (with one build tool!)\n\nIn the last section there is one called: \"Shoot a bug\", no clue what that might mean though (anyone?).  There was very few items that I personally don't already know, but I will definetly pick up this when it comes out.\n\n-Benjamin Meyer\n\nP.S. I would be happy to give it a read and review it if someone can snag me a preview copy (or pdf).\n\nP.P.S. I would enjoy helping with \"Even more Practical Qt\" whenever that might come out. :-D"
    author: "Benjamin Meyer"
  - subject: "Re: More info here"
    date: 2004-08-27
    body: "> In the last section there is one called: \"Shoot a bug\", no clue what that might mean though (anyone?).\n\nShootABug was presented in the aKademy talk, see http://conference2004.kde.org/sched-devconf.php for transcript, slides and source."
    author: "Anonymous"
  - subject: "Re: More info here"
    date: 2004-08-29
    body: "We have tons of material for a \"Even More Practical Qt\". It's basically a question of the publisher letting us do that, which in turn is a question of \"Practical Qt\" selling well enough (whatever the publisher considers \"well enough\" - don't ask me). So, you know what to do :-) (Hint for slow readers and shameless plug: Tell everybody about this book and point them to http://www.kdab.net/practicalqt)"
    author: "Matthias Kalle Dalheimer"
  - subject: "Too bad"
    date: 2004-08-26
    body: "You can't order this from a KDE store so the 9\u0080 could go to KDE if you are not at the conference."
    author: "am"
  - subject: "Re: Too bad"
    date: 2004-08-29
    body: "We are not supposed to compete with bookstores, that's why. \"Trade show specials\" are OK, though."
    author: "Matthias Kalle Dalheimer"
---
The KDE Project is proud to announce an exclusive preview of the book 
 '<a href="http://www.kdab.net/practicalqt">Practical Qt</a>' written by Jesper K. Pedersen 
 and Matthias Kalle Dalheimer at the <a href="http://conference2004.kde.org/">KDE World Summit - aKademy</a>.

<!--break-->
<p><IMG src="http://static.kdenews.org/fab/events/aKademy/pics/kalle_practical.qt.jpg"  align="right" border="0"></p>

<p>Those attending aKademy can pre-order the book, with &euro;9 going to the KDE Project for 
 each copy sold. The book will be packaged and shipped to you as soon as the book hits the shelves. 
 All pre-ordered books will be shipped within the European Union. For orders outside the EU shipping costs of &euro;5 will be added.</p>

 <p>Alternatively you can order it at the <a href="http://www.klaralvdalens-datakonsult.se/?page=publications&sub=orderonline">Klar&#228;lvdalens 
 Datakonsult AB website</a>, though the KDE project will not receive the &euro;9 for these sales. So please visit the merchandise booth during aKademy and order this book.</p>