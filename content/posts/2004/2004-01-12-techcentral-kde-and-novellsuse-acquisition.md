---
title: "TechCentral on KDE and Novell/SUSE Acquisition"
date:    2004-01-12
authors:
  - "numanee"
slug:    techcentral-kde-and-novellsuse-acquisition
comments:
  - subject: "thats a great thing"
    date: 2004-01-12
    body: "I guess it whouldn't be a good thing if SUSE whouldn't support KDE anymore..."
    author: "Superstoned"
  - subject: "ok, then what"
    date: 2004-01-12
    body: "What happens when they are in a money crunch or their stock is down and something needs to be cut?"
    author: "frizzo"
  - subject: "Re: ok, then what"
    date: 2004-01-12
    body: "They cut the expensive Gnome support. Supporting KDE is cheap. The community does almost everithing. See how uch money Gnome companies spent over the years..."
    author: "AC"
  - subject: "Re: ok, then what"
    date: 2004-01-12
    body: "indeed.. Eazel burned through $50 million dollars making Nautilus, and went bankrupt soon after."
    author: "anon"
  - subject: "Re: ok, then what"
    date: 2004-01-12
    body: "Well, sorry to piss on your parade\nBut it was 13million\nAnd that was to support 90 employees\n(at a rather nice 100grand a year salary)\nmost of whom were working on things other than Nautilus\nEazel went bust because of other things\nnot because of making nautilus."
    author: "Yellowboy"
  - subject: "Re: ok, then what"
    date: 2004-01-12
    body: "Hey, hey, don't get mad. Notice that the comment was _not_ an attack on Gnome, but only a response to some smart guy trying to put KDE down.\n\nCool man!"
    author: "cwoelz"
  - subject: "Re: ok, then what"
    date: 2004-01-12
    body: "> Eazel went bust because of other things not because of making nautilus.\n\nAnd to think their press releases made it sound like they were finally bringing a GUI to Linux. IIRC they had something like 30 or so developers and 30 or 40 usability people. This was a big time proprietary model writing GPL'd code. Most interesting was their interview answers about how they were going to make money. They had several ideas. Where I come from we call that \"What do you mean you don't have a business plan?\" Because they were Apple guys who \"invented\" the GUI after a field trip to Xerox the press treated them like the coming of a messiah.\n\nAll I know is that I can't imagine anyone dumping millions in my lap without a business plan. Konqueror seemed to come out with similar features before Nautilus with less developers. I have actually spent money developing Quanta. We use users on CVS for usability testing. I can't begin to imagine what I'd do with $13 million. Given that Quanta has cost a tiny fraction of a percent of that to develop I would guess KDE would have the most formidable application suite in the world within a few years.\n\nI don't use Nautilus so I can't judge their knowledge of software. What Eazel clearly did lack was a business plan or a desire to use common open source methodologies. They did in fact burn money. However a friend of mine who is a financial planner has told me that creatively burning venture capital can leave you set for life, so it's possible I'm missing the real objective. It's difficult for me to imagine the Eazel team being a real hero to anyone who didn't think pro wrestling was real."
    author: "Eric Laffoon"
  - subject: "Re: ok, then what"
    date: 2004-01-12
    body: "Developing software is cheap and getting cheaper.\n\nNow, the guys that can manage U$S13M... those cost a fortune!"
    author: "Roberto Alsina"
  - subject: "Re: ok, then what"
    date: 2004-01-12
    body: "*ROTFLOL*"
    author: "anonymous"
  - subject: "Re: ok, then what"
    date: 2004-01-12
    body: "Very very funny. And probably not far from the truth.\n\nThe challenge of getting into the desktop market is how to survive while the software grows and matures (years). There is an established competitor who knows full well that it can kill any newcomer by cutting off the cash supply. Can Novell bankroll the whole thing? No way. They are depending on the goodwill and contributions of all the desktop developers. The developers decide where they put their energies.\n\nMaybe Perens has done some inadvertent consulting for Novell with the Userlinux desktop controversy. File under \"Mistakes to Avoid\".\n\nDerek\n\n"
    author: "Derek Kite"
  - subject: "Re: ok, then what"
    date: 2004-01-12
    body: "How the fuck is it \"probably not far from the truth\"?\nHow much money have gnome companies gone though in the years and how is that in any way directly related to the cost of supporting gnome?\n\n"
    author: "Yellowboy"
  - subject: "Re: ok, then what"
    date: 2004-01-12
    body: "Because it shows just how expensive it is to develop apps for Gnome. All that money for a file manager and a PIM application? Look at Konqueror, Kroupware and the Kontact suite? How much did that little lot cost in non-existent venture capital funding? Some people on the Gnome side really are living in a fantasy world."
    author: "David"
  - subject: "Re: ok, then what"
    date: 2004-01-12
    body: "But be honest, Konqueror get support from SuSE and Mandrake, Kroupware was manly payed for by the German government. I guess that it didn't cost those millions but it did have some corporate support."
    author: "Expensive"
  - subject: "Re: ok, then what"
    date: 2004-01-12
    body: "Yeah, I guess what they're trying to say is that there haven't been any big venture capital backed KDE companies.  Ximian, Easel and Redhat all rode that wave (for good or bad).\n\nHowever this basically has nothing to do with their toolkits or whatever.  Management quality is not a feature of the toolkit.  ;-)  So the Easel guys didn't know how to run a for profit company -- this is somehow connected to them using GTK and we're supposed take from this that GTK costs more to develop for?\n\nWhere'd I set my bat-o-logic?"
    author: "Scott Wheeler"
  - subject: "Re: ok, then what"
    date: 2004-01-12
    body: "Not so hard to grok, Scott.  The contention is that developing for GTK+ is *hard* compared to Qt, thus (at least on average) it requires more development hours spent for features produced.  Someone upthread said that Easel burned through capital primarily through developer salaries and all they had to show was Nautilus."
    author: "manyoso"
  - subject: "Re: ok, then what"
    date: 2004-01-13
    body: "No, what I showed was that they were paying their developers handsomely.\nThey had IIRC about 20 out of 30 developers working on Nautilus, the other ten were working on \"other things\". The rest of the team was managers, PR departments, artists, and other things that weren't overly essential to developing software, but are essential to running a company.\n\nIt has *nothing* to do with how hard or easy one toolkit is to use over another.\n\nIMO Eazel's main flaw to to assume that they were going to succeed with no problems at all and expanded too far too quickly. But as Scott said, management competency isn't a feature of either QT or GTK"
    author: "Yellowboy"
  - subject: "Re: ok, then what"
    date: 2004-01-12
    body: "Qt is a heck of a lot more than a graphical toolkit, like GTK is. There is no comparison."
    author: "David"
  - subject: "Re: ok, then what"
    date: 2004-01-12
    body: "Don't forget Corel and Caldera as well. They played crucial rules in early KDE 2.x development. Although they aren't around much anymore in terms of KDE (Corel->Xandros, Caldera->SCO)"
    author: "anon"
  - subject: "Re: ok, then what"
    date: 2004-01-12
    body: "Corel? I think they contributed  a fair chunk of Ark, and a code review.\n\nCan\u00b4t remember anything else coming from them."
    author: "Roberto Alsina"
  - subject: "Re: ok, then what"
    date: 2004-01-13
    body: "Well, using the above logic -- of course Corel didn't contribute anything when Qt is sooooo expensive and hard to use..\n\n:)\n"
    author: "mill"
  - subject: "Re: ok, then what"
    date: 2004-01-13
    body: "Yer, it's expensive to a kid on Slashdot. To a corporate - no. Hard to use? Yer. right."
    author: "David"
  - subject: "Re: ok, then what"
    date: 2004-01-13
    body: "*woosh*"
    author: "mill"
  - subject: "Re: ok, then what"
    date: 2004-01-12
    body: "Whatch your language. Are you trying to discuss something or are you here to call names?\n\nAnswering your quastion:\n\nIt seems easier to develop to KDE. The development tools are better, the base library is more documented (Qt), the translation tools are excelent (and even used by other desktops), kdevelop rocks, the components (kparts) are easy to implement and easy to use, the XMLUI makes it easy to adapt the existing applications to a specific function.\n\nBut I could be wrong."
    author: "cwoelz"
  - subject: "Re: ok, then what"
    date: 2004-01-13
    body: "I didn't call anyone names did I?\nNo, I just checked, I didn't.\n\nSometimes, use of strong language adds emphasis to a point.\nSometimes.\n\nAnd ummm, your answer didn't answer my question.\n"
    author: "Yellowboy"
  - subject: "Language Issue."
    date: 2004-01-12
    body: "Howdy Navindra.\n\tDo you do edits on postings? Or is it simple to do so? \nIt might nice to do some re substitues over the postings and subjects. \nPerhaps just replace after the first letter with the usual garbage.\n\nf!@#\ns!@#\nc!@#\n\n\n\n"
    author: "a.c."
  - subject: "Re: ok, then what"
    date: 2004-01-13
    body: "Well, lets see who actually lives off what they write.\n\nKDE now has a very good foundation of a groupware solution because someone went out and sold a solution, got money for it, stuck their neck out in promising something, then did it. They built on what was there before, and they and others are putting time and resources into finishing the whole thing. We all benefit. Novell can improve and sell it too.\n\nCall it reality. When I see FOSS people doing the industry fluff, vapor and scam stuff, I get disgusted.\n\nFunny, it is just fine and logical to suggest that KDE disappear, but the instance Gnome and their rich fanboys get criticised, the language gets bad real quick.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: ok, then what"
    date: 2004-01-13
    body: "Yeah, cos the KDE boys didn't get annoyed over the UserLinux thing at all.\n\nAnd the rest of your comment is completely unrelated to anything I posted and is really just a strawman you've set up.\n\nAnd for what its worth...\nI don't get paid to work on GNOME so I'm definelty not a \"rich fan boy\"...\nYou probably shouldn't jump to conclusions, it makes you look very silly.\n\nWhen I see FOSS people doing the \"lets talk shit about stuff we don't know\" stuff, I get disgusted."
    author: "Yellowboy"
  - subject: "Re: ok, then what"
    date: 2004-01-13
    body: "\"I don't get paid to work on GNOME so I'm definelty not a \"rich fan boy\"...\"\n\nHe probably wasn't talking about you. You can talk in the third person you know.\n\n\"When I see FOSS people doing the \"lets talk shit about stuff we don't know\" stuff, I get disgusted.\"\n\nYer it's called common sense. Hurts doesn't it?"
    author: "David"
  - subject: "Re: ok, then what"
    date: 2004-01-13
    body: "> He probably wasn't talking about you. You can talk in the third person you know.\n\nNo-one else was jumping in to defend GNOME, so... I guess he was talking about me.\nAnd no\nIts not called \"Common sense\"\nIts call bullshiting.\nAnd it doesn't hurt\nIts just annoying."
    author: "Yellowboy"
  - subject: "Re: ok, then what"
    date: 2004-01-15
    body: "\"No-one else was jumping in to defend GNOME, so... I guess he was talking about me.\nAnd no\nIts not called \"Common sense\"\nIts call bullshiting.\nAnd it doesn't hurt\nIts just annoying.\"\n\nGo away then."
    author: "David"
  - subject: "Re: ok, then what"
    date: 2004-01-13
    body: "Indeed, the KDE people (me included) got annoyed with the Userlinux thing. For very good reason, just like you were annoyed and moved to swear when someone had the temerity to suggest that Gnome could be the loser in some decision.\n\nSo let's put the shoe on that foot. Does Gnome pay it's way? Does it cover anyone's cash flow requirements? Maybe it does. I know, and I used one example, where it does cover someones cash flow, someone sold a solution based on KDE, were paid for development work that then was put back into the project. This isn't some flakey 'well, if we scam some investors with some grandiose promises...' thing, this is real money for real solutions.\n\nAnd no, I wasn't referring to you. I don't know who you are.\n\nIt isn't KDE people who are saying that there should be one desktop to rule us all. It isn't KDE people who are saying, and I quote \"it will take us two years to catch up to KDE in functionality\", and suggesting that it would be best if KDE was marginalized, or better, went away. It wasn't the KDE people who were saying that Novell will naturally use their desktop.\n\nKDE people are saying that there are two desktops, will be two desktops, lets make them both work together where it makes sense. And Gnome developers are saying the same things as far as I can tell. There are certain interests and prominent people in the FOSS community that somehow think they can take charge of something that they don't own. So far, it belongs to whoever writes the code. Both KDE and Gnome have active communities, churning out copious amounts of code. And both have growing, enthusiastic user communities. It's not a zero sum game.\n\nDerek"
    author: "Derek Kite"
  - subject: "openexchange future?"
    date: 2004-01-12
    body: "This story is very thin on details only suggesting that ultimately Novell will make changes.  Anybody have a clue what the thinking is with regard to OpenExchange server?  Is it going to get canned in favor of GroupWise? (I hope not!)"
    author: "jdell"
  - subject: "Re: openexchange future?"
    date: 2004-01-12
    body: "The sales of SLOX4.1 are going very well. It has a WebDAV interface which you can easily use from your own applications (see http://devel.slox.info/ ) to access ALL data on SLOX. Together with iSLOX and oSLOX (plugins for Outlook) it allows much over 90% of all Exchange users to switch from Exchange to SLOX without losing functionality while using Outlook as their groupware client. SLOX is really an Exchange killer and Novell would be dumb to throw this away. "
    author: "Anonymous Coward"
  - subject: "SuSE CEO Seibt's Statement to KDE Core Developers"
    date: 2004-01-12
    body: "Here is a full quote of what he wrote to kde-core-devel list, for all those \nwho are too lazy to click the link in the story above (or missed to see it).\n\n------------------- quote --------------------------------------------------\n\nList:     kde-core-devel\nSubject:  Statement\nFrom:     Richard Seibt <>\nDate:     2003-11-11 13:19:43\n\nDear KDE developers,\n\nsince I joined SUSE in January 2003, I learned that KDE is one of the most \nsuccessful and fastest evolving open source projects. Since the first day I \nuse it myself and I am thrilled with it. My personal thanks to all of you who \ncontributed in the last seven years to KDE.\n\nKDE is the defacto standard for Linux desktops in Europe. Without KDE, I do\nnot believe we would be seeing the interest in Linux on the corporate desktop\nthat we are seeing today and on the consumer desktop as well. \n\nPlease be clear: SUSE LINUX will continue to strongly support KDE.\n\nBoth the Ximian and SUSE LINUX acquisitions reaffirm Novell's strong belief in \nthe value of the open source development model and strengthen Novell's \ncommitment to supporting the open source community. SUSE LINUX is an \nintegrator with a wealth of experience in working with both the open source \ncommunity and the IT industry. SUSE LINUX brings extensive experience in KDE \nand has a track record working with GNOME. Ximian has core competence in \nGNOME and a great desktop focus, both in platform and product offerings.\n\nTogether with our Ximian colleagues at Novell, we will also enable our \ncustomers to use GNOME with the same convenience and comfort KDE offers to me \nand all SUSE employees today.\n\nThe integration capabilities SUSE LINUX brings will continue to help \nseamlessly integrate Linux-based offerings, inside and outside of Novell. \nTogether, we might even think about new opportunities to leverage the usage \nof Linux on the desktop. E.g. why do we not open Linux for Apple's Mac OS \ndesktop?\n\nToday and tomorrow, we must be strongly committed to deliver what our \ncustomers ask us for. They know best what tools will help to fit their needs. \nThat's why we recently choose our new slogan - Simply change! It is an \naffirmation to all of us to listen what our customers and users need\n-- and change our manners and goals, if necessary.\n\nSo I really love to stay with you in an ongoing and fruitful dialog.\n\nRichard\n-- \nRichard Seibt, Chief Executive Officer - Vorsitzender des Vorstandes\nSUSE LINUX AG, Deutschherrnstr. 15-19, D-90429 Nuernberg\n\n------------------- unquote ------------------------------------------------\n"
    author: "KDEfan"
  - subject: "Re: SuSE CEO Seibt's Statement to KDE Core Develop"
    date: 2004-01-13
    body: "That is all well and good, but it may not be his decision at the end of the day.\n\nLong term it doesn't make sense to have Suse supporting KDE and Ximian doing Gnome. Either Suse switches over to Gnome or Ximian switches to KDE (wouldn't that be cool!)\n\n"
    author: "Anony Guy"
  - subject: "Re: SuSE CEO Seibt's Statement to KDE Core Develop"
    date: 2004-01-13
    body: "Why doesn't it make sense? IBM for example, sells and supports AIX, AS400, the big iron, Windows (alot) and Linux. Why? because the have customers who want one or all of the above.\n\nRight now, Ximian and gnome have valuable elements. KDE has valuable elements. This won't change.\n\nThe reason this won't change is because developers that aren't working for Novell will work on, improve, extend, innovate on whatever they feel like. It isn't Novell's to control. Right now if you limit your clients to one desktop, they are missing the strengths of the other. That won't change. KDE will improve to match Gnome where it may lack, and visa-versa. At the same time each advances elsewhere. They will never be equal, or equally suited to all purposes.\n\nThe only way the linux desktop can be viable for Novell or SUSE or anyone is to use the work of hundreds of other developers not in your employ. Yes, improve, contribute, finish, maybe gently push in a direction or other. No-one could afford to do it all.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: SuSE CEO Seibt's Statement to KDE Core Develop"
    date: 2004-01-13
    body: "You are talking as if all the decisions on the linux desktop front were already made. Neither gnome nor kde have a clear advantage over each other. \n\nSo we may expect that Novell will decide what is best to its business. Companies may try to shape the market, but usually it's the market who shapes the companies! Novell will support either gnome and/or kde if that makes a profit for the company, that's all."
    author: "hgg"
  - subject: "\"Why not open Linux for Apple's desktop\""
    date: 2004-01-12
    body: "Richard Seibt:\n>>>> \"E.g. why do we not open Linux for Apple's Mac OS desktop?\" <<<<\n\n\n...or why not the other way round? Bring KDE applications to Apple's Mac OS Desktop?\n\nOh, wait.... they are doing this already! They are even nearly there!! (Ignoring the rough edges which may still be there -- but hey, our two Benjamins working on it say the port is still Beta). \n\nScreenshots here:\n\n -->  http://ranger.befunk.com/screenshots/  <--  # names starting with \"qt-mac-...\"\n -->  http://www.csh.rit.edu/~benjamin/about/pictures/KDE/  <--\n\nDownloads here:\n\n -->  http://kde.opendarwin.org/\n\nThe fact that they were able to do most of the work during X-Mas holiday season is a tremendous tribute to the Qt and KDE cross platform capabilities.\n\nA Win32 port is also going forward (\"http://kde-cygwin.sf.net/\"). More problems there to solve, but it is a living and kicking project, also with already working KDE programs....\n\n"
    author: "Kurt Pfeifle"
  - subject: "How many KDE people does SUSE employ?"
    date: 2004-01-12
    body: "I read somewhere that it was about 4. It does not sound so much, and I definitely think they could continue to pay for that, no matter what. For Novell, especially."
    author: "claes"
  - subject: "Re: How many KDE people does SUSE employ?"
    date: 2004-01-12
    body: "According to http://www.kde.org/support/thanks.php :\n\nSuSE pays KDE developers Waldo Bastian and Lubos Lunak to work on KDE development. SuSE also employs around 10 KDE developers which are free to work on KDE during their work time."
    author: "Erik Hensema"
  - subject: "Might be worse for Ximian"
    date: 2004-01-12
    body: "All the Gnomeistas are automatically assuming that its the end for KDE on SuSE, but the opposite is more logical.  Heres my thinking:\n\n1.  KDE is established on SuSE (counter:  Why acquire Ximian then?)\n2.  SuSE is the money maker, not Ximian. (counter:  ?)\n3.  Novell is interested in making money.  (counter: ?)\n4.  Most commercial linux desktop distributions use KDE as default: SuSE, Lindows, Xandros:  precedence issues (counter: differentiation)\n5.  KDE is not \"tied\" to specific corporate interests like Gnome (Sun, ex-redhat). (counter:  so this means that Gnome is already co-opted)\n\nOf course the biggest argument against this is that the Ximian honchos are in the Desktop side of Novell distribution so reason might be trumped by ideology...  Hasn't been the first time.\n\nregards,\nBruce.\n\n"
    author: "Bruce"
  - subject: "Re: Might be worse for Ximian"
    date: 2004-01-13
    body: "Novell bought ximian because it bets on mono becoming .net on linux. I agree they will dump ximian gnome."
    author: "-"
  - subject: "Re: Might be worse for Ximian"
    date: 2004-01-13
    body: "Looking at http://www.desktoplinux.com/news/NS3056338289.html doesn't sound like Novell is about to dump gnome..."
    author: "Birdy"
  - subject: "Re: Might be worse for Ximian"
    date: 2004-01-13
    body: "Only 7 of the 45 bounties were solved in the given time frame. Either they are missing enough developers or it's too difficult to develop for Gtk/GNOME."
    author: "Anonymous"
  - subject: "Re: Might be worse for Ximian"
    date: 2004-01-13
    body: "Wow.  That is some pretty bad PR."
    author: "anonymous"
  - subject: "Re: Might be worse for Ximian"
    date: 2004-01-13
    body: "+++ \"Only 7 of the 45 bounties were solved in the given +++\n+++  time frame. Either they are missing enough developers +++\n+++  or it's too difficult to develop for Gtk/GNOME.\" +++\n\n\nIf that's true, it is a really bad sign for GNOME. First tease the developers to\ndo the unpopular/difficult stuff -- and then fail to get what you want....\n\nBut, maybe, it is just a webmaster being on vacation not having updated the table?\n\nAnyone here who is lurking on GNOME mailing lists knowing what's going on?"
    author: "anon"
  - subject: "Re: Might be worse for Ximian"
    date: 2004-01-13
    body: "It's true.  Check gnomedesktop.com."
    author: "anonymous"
  - subject: "Re: Might be worse for Ximian"
    date: 2004-01-14
    body: "It could _still_ be the webmaster being on holiday..."
    author: "anon"
  - subject: "Re: Might be worse for Ximian"
    date: 2004-01-14
    body: "Wow -- I haven't visited the dot in a while, and I'm horried at the attitudes around here.  I thought Slashdot was bad ...\n\nAnyways, yes, only 7 bounties were claimed IN TIME for the deadline.  The whole contest was only about a month long, and involved a lot of non-GNOME people learning a new development environment.\n\nIf you actually look through the bugzilla entries, lurk on the mailing lists, or listen in on irc.gnome.org, you'll see that ALMOST ALL of the bounties are being worked on.  Only 7 were finished ahead of time.  Others are on the doorstep.  Others are still coming in the second round.\n\nWhile many people are working on the bounties, the people running the contest are holding the highest of standards for the entries.  No cheap hacks here."
    author: "anony"
  - subject: "Re: Might be worse for Ximian"
    date: 2004-01-13
    body: "Other possible reasons:\na) Gaim developers are not forthcoming with accepting patches (who'd have guessed...)\nb) Evolution mailer developers both went on 3 weeks vacation over the holidays and so many of the patches could not get committed or discussed\nc) Christmas is exam/finals time for students, so hacking is a secondary issue.\n"
    author: "Yellowboy"
  - subject: "Re: Might be worse for Ximian"
    date: 2004-01-13
    body: "I think we should post it to Slashdot and let them decide. :D"
    author: "anonymous"
  - subject: "Re: Might be worse for Ximian"
    date: 2004-01-13
    body: "\"b) Evolution mailer developers both went on 3 weeks vacation over the holidays and so many of the patches could not get committed or discussed\"\n\nHa. Having a holiday from hacking on a free software project that has no return on investment for Novell. Nice one."
    author: "David"
  - subject: "Re: Might be worse for Ximian"
    date: 2004-01-14
    body: "Are there only _two_ Evolution developers. Boy these are real geniuses!\nKudos to the two Evolution developers! May they have a nice holiday... Come back soon!"
    author: "anon"
  - subject: "Re: Might be worse for Ximian"
    date: 2004-01-14
    body: "--- Kudos to the two Evolution developers! ---\n\nAnd even more Kudos, lots of Kudos, shitloads of Kudos, to the two KDE developers named Benjamin who _didn't_ go for a X-Mas holiday, but who did a _complete_ port of KDE over to Mac OS X (which included \"Kontact\", the new Outlook-Killer-ish, KDE app) !!!! How's that? Isn't the KDE community a wonderful bunch of dedicated hackers? Creating miracles while others go for a holiday..."
    author: "ATK"
  - subject: "Re: Might be worse for Ximian"
    date: 2004-01-14
    body: "The maintainer of Evolution died in December.\n"
    author: "mill"
  - subject: "Re: Might be worse for Ximian"
    date: 2004-01-14
    body: "God. You mean there's more of them. Holy crap!"
    author: "David"
  - subject: "Re: Might be worse for Ximian"
    date: 2004-01-13
    body: "> c) Christmas is exam/finals time for students, so hacking is a secondary issue.\n \nwtf? Academic years start around september and finish around june.\nFinals are in may/june, so the exam/revision frenzy is from easter onwards, not christmas."
    author: "ac"
  - subject: "Re: Might be worse for Ximian"
    date: 2004-01-13
    body: "Exams in Canada are in December.\nExams in the UK are in January.\nThere are other exams in May/June as well, but December/January time is defintely exam time.\n"
    author: "Yellowboy"
  - subject: "Re: Might be worse for Ximian"
    date: 2004-01-13
    body: "> Exams in Canada are in December.\n> Exams in the UK are in January.\n> There are other exams in May/June as well, but December/January time is  defintely exam time.\n\nWell, im not canadian, so I wont make any assertions about their education system except to say that if you know as little about canada as you do about the UK, you're almost certainly wrong.\n\nI've been in the UK education system for the last 15 years, non-stop. I've had occasional exams at most times of year.\nBut _The_ big exam season happens around June. Kinda figures, since it's the end of the academic year.\n\nThere are always, of course, exceptions. Some freinds of mine are doing uni courses that have all-year-round exams, and yes there is a _small_ exam season around this time of year. mostly for gcse/a-level course modules.\n\nI'm getting the feeling there's a big YHBT coming.. <sigh> "
    author: "ac"
  - subject: "Re: Might be worse for Ximian"
    date: 2004-01-13
    body: "> But _The_ big exam season happens around June. Kinda figures, since it's the end of the academic year.\n\nIn the US, usually exam time is mid to late December AND late may/early June. They are equal since most of the time (semester system... academic year? WTF is that?)\n\n"
    author: "anon"
  - subject: "Re: Might be worse for Ximian"
    date: 2004-01-13
    body: "What might be great to see out of the Novell/Ximian/SuSE trio is to see some Mono/KDE integration. I don't really like C#, but its a popular language, and it would be good to see KDE accessible from it.\n"
    author: "Rayiner Hashem"
  - subject: "Re: Might be worse for Ximian"
    date: 2004-01-13
    body: "Sure.\n\nAlso, the Mono project is using the SWT toolkit (from the Eclipse Java IDE) for its GUIs, which draws on Gtk+ on Linux.  It'd be nice to see a Qt port (there's already a Motif one)."
    author: "David Walser"
  - subject: "Re: Might be worse for Ximian"
    date: 2004-01-13
    body: ">>Also, the Mono project is using the SWT toolkit (from the Eclipse Java IDE) for its GUIs, which draws on Gtk+ on Linux. It'd be nice to see a Qt port (there's already a Motif one)<<\n\nIt does NOT use the SWT toolkit. In fact, it has very little to do with Java at all.  It's all about c#.  \n\nWhat some of them are doing is porting the SWT toolkit to C#."
    author: "rizzo"
  - subject: "Re: Might be worse for Ximian"
    date: 2004-01-13
    body: "I didn't say it had anything to do with Java, smart guy.  SWT has two parts, a library (written in C I think) and the Java part of it.  Obviously they're not using the Java part of it, I know mono is C# (duh!).  They are using the library.  If that means they're not *really* using the SWT _toolkit_, then you're getting all worked up over semantics."
    author: "David Walser"
  - subject: "Re: Might be worse for Ximian"
    date: 2004-01-13
    body: "Now now. You are not thinking right. The only way to make money in the software biz is:\n\nHype great idea!!!\nScam investors, get millions in venture capital.\nBurn money.\nProduce 1/2 finished software.\nBe purchased by *.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Might be worse for Ximian"
    date: 2004-01-13
    body: "Yep, but to do that you have to think only about $. Easy when some people doesn`t care about computer stuff."
    author: "JC"
  - subject: "This can be all over if..."
    date: 2004-01-12
    body: "Trolltech released Qt as LGPL under Linux/Unix, let them keep it propietery under Windows..etc, but let it be LGPL under Linux!\n\nNo one would be interested in GNOME if KDE/Qt is LGPLed.\n\nC'mon Trolltech, you made a brave move earlier, just one more!\n\n"
    author: "RMS"
  - subject: "Re: This can be all over if..."
    date: 2004-01-12
    body: "If you think that Gnome will disappear if Qt is released as LGPL then you are mistaken.  Its gone way beyond that.  This whole licensing thing is a complete red herring.  The Gnome laddies and lassies can't have it both ways, either:\n\n1. GPL is more \"pure\" or...\n2. LGPL is more \"business friendly\".\n\nChoose your poison and stick with it.  Both of these arguments are mutually exclusive and if you take RMS at his word (ahem!!) then LGPL is the greater evil and therefore, because the core libraries of GNOME are licensed LGPL then GNOME is greater evil.  But no, the open source \"leaders\" (with the noticeable exception of Linus T) consider the GNOME project to be the more blessed.  \n\nBoth desktops need each other to spur on growth.  KDE has the better underpinnings whereas Gnome has seen HIG work (which I think sucks eggs, but thats me :).  Both desktops cross pollinate each other with ideas and the concept of two interoperable desktops is intriguing.\n\n"
    author: "Bruce"
  - subject: "Re: This can be all over if..."
    date: 2004-01-12
    body: "It's more than that. Since day one the GNU community has been against KDE, and has rolled out one weak argument after another. First it was the proprietary \"free-beer\" license of Qt. So a binding agreement was made with KDE to ensure that Qt would always remain \"free beer\". But that wasn't good enough. Trolltech then changed the license to the QPL, which even RMS approved as Free. But that wasn't good enough. Then Trolltech released Qt under a dual QPL/GPL license (even better than pure GPL). But again, that wasn't good enough. Along the way other arguments unrelated to licensing have been offered and countered."
    author: "David Johnson"
  - subject: "Re: This can be all over if..."
    date: 2004-01-12
    body: "RMS wants a desktop that he can influence and be more under his influence. Its all personnel pride and such."
    author: "Kraig "
  - subject: "Re: This can be all over if..."
    date: 2004-01-15
    body: "That's not true.\nIn fact RMS is quite disappointed with Miguel de Icaza and GNOME."
    author: "Debian User"
  - subject: "Re: This can be all over if..."
    date: 2004-01-15
    body: "Why because he can't control it like he planned?"
    author: "Kraig "
  - subject: "Re: This can be all over if..."
    date: 2004-01-15
    body: "Stop the FUD.\n \nWhen KDE started, Qt was proprietary (the GNU community doesn't care about free beer, but about freedom). That's the only reason the GNOME and Harmony projects were born.\n \nLater TrollTech used the QPL license. As you say, the FSF approved it as a free software license, but incompatible with the GPL. The KDE developers could add an exception to allow linking to it, but people behave too emotionally at the moment.\n \nWhen Qt became GPL, a lot of people jumped wagon, and keep doing it. The licensing terms of Qt and KDE are very nice for the GNU community.\n "
    author: "Debian User"
  - subject: "Re: This can be all over if..."
    date: 2004-01-16
    body: "I might just add this quote from an earlier interview with Eirik Eng of Trolltech (http://dot.kde.org/1001294012):\n\n\"We had the same rate of growth before the change in license. In the very early years, we were afraid that if we GPLed Qt, someone with more development muscle would create a hostile fork of Qt and, in a sense, take over our only product. You just don't take any chances with your only bread and butter. \n\n\"So, as soon as we felt that we could outrun anyone trying to make a hostile fork, we switched to using the GPL. The switch did not affect our customers, and it had very little practical impact on the Open Source community. But the symbolic effect was astronomical. My inbox was flooded with 'thank you' e-mails for quite some time.\""
    author: "Anonymous"
  - subject: "Re: This can be all over if..."
    date: 2004-01-12
    body: "I am against it.\n\nYou want to develop proprietary apps, pay Trolltech licence per developer. It is cheap, considering development costs. Even in here in Brazil.\n\nMake Qt for windows GPL: that would be a great change! Imagine all KDE apps available for windows! The transition to linux could be smooth.\n"
    author: "cwoelz"
  - subject: "Re: This can be all over if..."
    date: 2004-01-12
    body: ">> Make Qt for windows GPL: that would be a great change! Imagine all KDE <<\n>> apps available for windows! The transition to linux could be smooth. <<\n\nI can't help but to agree. (I am not sure if all KDE apps could be ported and would run as \"smoothly\" and easily as on Mac OS X, or if too many \"UNIX assumptions\" went into the KDE design... But probably a thing that *could* be solved!) The migration path to Linux would indeed become much more smooth, for everybody -- users, admins, ISVs, bosses, programmers...."
    author: "Kurt Pfeifle"
  - subject: "Re: This can be all over if..."
    date: 2004-01-12
    body: "\"Make Qt for windows GPL...\"\n\nHah! You know Windows people are bootleg everything."
    author: "Jilks"
  - subject: "Re: This can be all over if..."
    date: 2004-01-12
    body: "Qt is not only GPL:d.\n\nIt's GPL:d and QPL:d. You can choose whichever you want."
    author: "OI"
  - subject: "Re: This can be all over if..."
    date: 2004-01-12
    body: "Yes, but not on all platforms, and that was the point.\nNote that I'm not saying that Trolltech should do this. I think they are quite right *not* to actually. They get a lot back from the open source movement on linux (especially from KDE, I'd imagine), and it's far from certain the same would be true on windows, which has on average, IMHO, a different mindset on things like this."
    author: "Andr\u00e9 Somers"
  - subject: "Re: This can be all over if..."
    date: 2004-01-12
    body: "Actually I don't see this really happen, however...\nIf the GTK integration efforts reach the level where the user can't (or can hardly) tell which app is Qt and which one is GTK, we can say we got two native toolkits. This means businesses that want to write KDE apps are not tied to Qt and unless GNOME would do a similar thing for Qt integration this would give KDE a _huge_ advantage. And the most beautiful part is the GNOME crowd can't complain about licenses anymore ;)"
    author: "Arend jr."
  - subject: "Re: This can be all over if..."
    date: 2004-01-12
    body: "+++ GNOME crowd can't complain about licenses anymore ;) +++\n\nGNOME zealots (which not all GNOME supporters are) will always complain about KDE or Qt:\n\n+ KDE has more apps and features --> \"KDE is bloated!\"\n+ KDE has more configurability --> \"KDE is too complicated!\"\n+ Qt uses C++ --> \"Qt excludes C coders!\"\n+ KDE is a big grassroots movement --> \"KDE is not a useful desktop for  businesses!\"\n+ KDE extends its integrative desktop to GTK/GTK+ apps --> \"KDE hasn't got any good programs of its own!\"\n+ KDE develops programs of its own --> \"KDE is re-inventing the wheel every week!\"\n+ Qt uses the GPL --> \"Qt doesn't allow for gratis proprietary development by ISVs!\"\n+ KDE is ported to Mac OS X --> \"KDE is trying to betray Free Software by supporting a proprietary OS!\"\n+ Trolltech ploughs money back into GPL'ed Qt development --> \"Trolltech is demanding huge lock-in tax from poor ISV developers!\"\n+ Trolltech, KDE or Qt could be whatever you imagine --> \"Trolltech, KDE, Qt are not what *we* want them to be!\"\n\n## Who can list more examples? Go to Slashdot any day and you'll find lots and lots and lots.... Or just save your time.\n\n\nOh, well...\n\n"
    author: "anon"
  - subject: "Re: This can be all over if..."
    date: 2004-01-13
    body: " + KDE has more configurability --> \"KDE is too complicated!\"\n----------\nThis is a legitimate complaint. Configurability is not necessarily exclusive to complexity. Given the appropriate tools/interface, you can be very configurable without becoming overly complex.\n\nYour other arguments are spot-on.\n"
    author: "Rayiner Hashem"
  - subject: "Re: This can be all over if..."
    date: 2004-01-14
    body: "Exactly. I'm looking forward to downloading and testing your personal idea-KDE-world settings package for everyone. The configurability is there."
    author: "Datschge"
  - subject: "Re: This can be all over if..."
    date: 2004-01-13
    body: "Just out of interest, where are the commercial GTK applications?\n\nI see commercial Motif applications, and commercial QT applications, but I haven't actually seen any commercial GTK applications.\n\nDoes anyone have any examples?"
    author: "mabinogi"
  - subject: "Re: This can be all over if..."
    date: 2004-01-13
    body: "Evolution, RealPlayer, YahooMessenger... umm... there are a few more but i cannot rember as i have never used them.  Does eclipse count?  Or any java app that uses SWT or what ever its called?\n\nThere are a few... but none that really are big apps, and they are rewriting evolution now...\n\ncheers\n\t-ian (really really trying hard not to troll) reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: This can be all over if..."
    date: 2004-01-13
    body: "And GTK is not enough mature to be used for commercial apps.\nIt's bankruptcy...\nOr suicidal :)"
    author: "JC"
  - subject: "Re: This can be all over if..."
    date: 2004-01-13
    body: "Now that my friend is a direct troll...\n\nRemember people used MFC for many years and motif also. GTK really draws from those roots, and tries to emulate them.  Some people are just more comfortable in C with no objects to encumber them.  Some of us like objects and C++...\n\nI think time will tell what happens, but I will admit programming in GTK for a summer is what made me move to Qt/KDE.\n\nCheers\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: This can be all over if..."
    date: 2004-01-14
    body: "If anything, Qt is much more like than MFC than gtk is.. although wxWindows has even more close resemblance to MFC than Qt does. "
    author: "fault"
  - subject: "Re: This can be all over if..."
    date: 2004-01-13
    body: "ldd /usr/bin/realplay\n        libXp.so.6 => /usr/X11R6/lib/libXp.so.6 (0x4002e000)\n        libXmu.so.6 => /usr/X11R6/lib/libXmu.so.6 (0x40036000)\n        libXext.so.6 => /usr/X11R6/lib/libXext.so.6 (0x4004c000)\n        libXt.so.6 => /usr/X11R6/lib/libXt.so.6 (0x4005a000)\n        libX11.so.6 => /usr/X11R6/lib/libX11.so.6 (0x400ae000)\n        libm.so.6 => /lib/i686/libm.so.6 (0x401ab000)\n        libdl.so.2 => /lib/libdl.so.2 (0x401ce000)\n        libpthread.so.0 => /lib/i686/libpthread.so.0 (0x401d1000)\n        libc.so.6 => /lib/i686/libc.so.6 (0x40222000)\n        libSM.so.6 => /usr/X11R6/lib/libSM.so.6 (0x40355000)\n        libICE.so.6 => /usr/X11R6/lib/libICE.so.6 (0x4035e000)\n        /lib/ld-linux.so.2 => /lib/ld-linux.so.2 (0x40000000)\n\nWhich of these dependencies make Real Audio Player a GTK-application?\n\nRinse"
    author: "rinse"
  - subject: "Re: This can be all over if..."
    date: 2004-01-13
    body: "i was sure that i read somewhere it used gtk... maby staticly...\n\nnot sure as i dont use real player here, it may or may not, im not the expert, i was just trying to keep reality involved with the troll...\n\ncheers\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: This can be all over if..."
    date: 2004-01-13
    body: "It uses Motif.  At least, RealPlayer 8 does.  strings $(which realplay) | grep Xm will show all sorts of Motif stuff."
    author: "KDE User"
  - subject: "Re: This can be all over if..."
    date: 2004-01-14
    body: "The new HelixPlayer (https://player.helixcommunity.org/) is build on GTK2.\nOld (RP8 and RealOne) players use Motif."
    author: "Niek"
  - subject: "Re: This can be all over if..."
    date: 2004-01-14
    body: "But is HelixPlayer commercial and proprietary?\n\nIf it isn\u00b4t, then it\u00b4s still not an example of a commercial, proprietary GTK app ;-)"
    author: "Roberto Alsina"
  - subject: "Re: This can be all over if..."
    date: 2004-01-15
    body: "don't forget AIM and VMWare"
    author: "Steven Scott"
  - subject: "i agree"
    date: 2004-01-13
    body: "I support your statement.. LGPL is the better way for a lib\n\n(although BSD is the best way - see what beauty Apple did)"
    author: "Anton Velev"
  - subject: "Re: i agree"
    date: 2004-01-16
    body: "Some folks have noticed (http://www.deadly.org/article.php3?sid=20030927090008) that some executables shipped with Microsoft's SFU 3.0 (Services for Unix) kit contain OpenBSD RCS tags.\n\nInterix, the company contracted to develop the SFU for Microsoft, had apparently borrowed a great deal from the OpenBSD codebase. Providing that they kept the BSD copyright notice in the SFU sources intact, this is all completely legal.\n\nIt seems that having such an unrestrictive license does cut both ways.\n\n"
    author: "Anonymous"
  - subject: "Re: This can be all over if..."
    date: 2004-01-14
    body: "1. Trolltech has already said they will GPL Qt for Windows once it becomes a more \"open-source friendly\" OS.\n\n2. http://www.fsf.org/licenses/why-not-lgpl.html\n\n3. If Trolltech released Qt under LGPL, they would go bankrupt. No commercial developers would pay for QPL anymore, because they would just use the LGPL version in their commercial product! That would be an asinine decision, which would mean the end of Qt and KDE."
    author: "Luke Sandell"
  - subject: "Re: This can be all over if..."
    date: 2004-01-16
    body: "I hear Sleepycat makes most of its money through a similar dual-licensing trick with Berkeley DB.\n\nThe database is released under the GPL, but companies wanting to link it into their proprietary application have to buy a license. They sell to folks writing embedded applications (like set-top boxes, network appliances and the like), where the one-off cost of buying a license would be a tiny fraction of the development budget.\n\nTrolltech probably makes money off the same principle; that people developing for Windows are doubtless already paying for proprietary development tools and other libraries and should well be able to afford a Qt license, too."
    author: "Anonymous"
  - subject: "Re: This can be all over if..."
    date: 2004-01-19
    body: "Funny how for the longest time the GPL zealots screamed that QT sucks because it isn't free.  GNOME started as a GNU project partially due to the FSF and others dislike of proprietary QT.  Now that it is GPL, you have the GNOME people bitching that it isn't LGPL, although the FSF and GNU project now prefer GPL for libraries.  In fact, GNU has basically lost all influence in GNOME, and RMS seems to be warming to KDE.\n\nI think that the GNOME camp that was complaining in the pre-GPL days is very different than the GNOME camp complaining now."
    author: "anonymous"
  - subject: "medium term != long term"
    date: 2004-01-13
    body: "\"...for the medium term KDE will remain the default GUI on SuSE Linux,\u0094 assured John Phillips, Novell's corporate technology strategist for the Asia Pacific region.\n\nNovell is not committed to KDE, even if SuSe is. How long is \"medium term\"? I think they are planning something, or they would have said \"long term\". I switched to SuSe because of their KDE support, I would hate to think that would ever change."
    author: "dapawn"
  - subject: "Re: medium term != long term"
    date: 2004-01-13
    body: "\"We don't expect to make Ximian the default user interface\"\n\nNovell isn't committed to Ximian, even if Ximian is.  I think they are planning something, or they would have said they expected to make Ximan the default user interface.  Maybe they will make Ximian polish KDE or port Evolution to it."
    author: "anonymous"
  - subject: "Re: medium term != long term"
    date: 2004-01-13
    body: "> \"Maybe they will make Ximian polish KDE or port Evolution to it.\"\n\nI would love to have Ximian Connector ported as Kontact/Kolab plugin (or better yet, native webDAV support), so that I could access my company's global address book, and group calendar (MS Exchange based).\n\n> \"Novell isn't committed to Ximian, even if Ximian is.\"\n\nI get your overall point, but does that statement really make any sense? :-)"
    author: "dapawn"
  - subject: "Re: medium term != long term"
    date: 2004-01-13
    body: "Why should it be ported when we have our own? Thanks to Jan-Pascal van Best there will soon be an exchange-resource for kontact (kdepim 3.3?)"
    author: "Birdy"
  - subject: "Re: medium term != long term"
    date: 2004-01-13
    body: "Novell's interest and focus on the medium term is to remain in business. They have some cash, a good dealer network, some goodwill, some good products. Linux on the server was probably eating away at their already shrinking marketshare. So they join them, rather than get beat from both ends \n(MS -><-Linux).\n\nSUSE, from what I understand, is paying their own way. Novell would be foolish to mess around with something they really don't have any experience in.\n\nI suspect that they are just going to watch what happens in the market. They aren't stupid. Take OpenOffice and Mozilla out of the equation, and what does anybody have? What connection do they have with Gnome, or KDE for that matter? Is there going to be an amalgamation of the desktops? I don't think so, but time will tell. Will one or the other become ascendant? Will they both exist as they do now, both vigorous and growing? Once things become more clear, or more muddled (more likely) they will probably make a decision. Or maybe not.\n\nDerek (who notes that the PIM stuff is sponsored by someone other than SUSE)"
    author: "Derek Kite"
  - subject: "Re: medium term != long term"
    date: 2004-01-13
    body: "Just to clarify (as many people still seem to get it wrong): The PIM stuff is not _sponsored_ by any German governmental entities. The German BSI contracted a few companies (erfrakon, Intevation, Klar\u00e4lvdalens Datakonsult, who regularly won the public invitation to tender) because the BSI wanted to use the KDE PIM applications, but were missing a few groupware features. That's no sponsoring. Instead it's regular commercial software development. This also shows that commercial software development and Free Software development under the GPL is very well possible. No LGPL is needed for this.\n\nThe only thing that one could maybe confuse for sponsoring is that the additions were merged back into KDE (the GPL wouldn't have required them to give back the changes if the BSI would have just used the patched PIM apps internally). But that can't really be called sponsoring either. If they had not given back their changes then they would have had to continue paying the companies to port new features and bug fixes from the official PIM apps (i.e. from KDE CVS) to their patched versions. This would have cost a lot of money. Making sure that the changes are reintegrated into KDE saved them this money.\n\nAnd FWIW, all participants of the Osnabr\u00fcck Meeting (http://dot.kde.org/1073950883/) paid their travel and accommodation expenses themselves. So again no sponsoring.\n\nPlease don't confuse sponsoring with commercial Free Software development by companies like the aforementioned. Instead use this example to counter the argument that commercial software development and GPL contradict each other.\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: medium term != long term"
    date: 2004-01-14
    body: "Thanks for pointing that out. Nevertheless, you've got to give it some credit. This is commercial software that people obviously see a real need for and have funded. It shows the GPL business model works. Qt also shows that the proprietary business model works. What is Ximian's business model again?"
    author: "David"
  - subject: "Promises, promises..."
    date: 2004-01-13
    body: "I don''t believe that in a long run its viable business strategy  to have focus on 2 desktop enviroments. One of them will drop into the \"alternative\" maybe even \"unsuported\" category.\n\n"
    author: "magneto"
  - subject: "Re: Promises, promises..."
    date: 2004-01-13
    body: "What if there are active developers and an active support community for two desktops? As there is right now. What if there are consultants and service operations that sell solutions based on one or the other, and who contribute back to the respective projects? As there is right now. What if there are large corporations that fund developers, contribute code and back the respective projects, again, as it is right now? What if there are distributions that put focus and polish on one or the other, including both, but defaulting to one or the other, as it is right now?\n\nRight now there are viable businesses doing work in the desktop area, using one or the other desktops. Kinda like there are viable businesses that use or prefer php, others perl, others java. \n\nIt sounds like a healthy vibrant marketplace to me.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Promises, promises..."
    date: 2004-01-14
    body: "Don't get me wrong i don't except KDE to die quite the opposite. But, how many companies you can name which have focus (services, solutions & software) on both KDE and Gnome? \n\nWhat i am saying is, Novell will be (if all goes well) one company (no more suse or ximian) whit focus on one enviroment. \n \nOf course if freedesktop.org goes forward well we don't have to worry about this shit anymore. \n\n"
    author: "magneto"
---
With the KDE development and user communities flourishing more than ever, some people are anxious to drum up panic and drama surrounding corporate plays such as the acquisition of <a href="http://www.suse.com/">SUSE</a> by <a href="http://www.novell.com/">Novell</a>.  As you might know, SUSE has thus far been a huge KDE believer and by using KDE has benefited from a loyal and enthusiastic Linux desktop userbase of its own.  Last year, Richard Seibt CEO of SUSE <a href="http://lists.kde.org/?l=kde-core-devel&m=106855804831790&w=2">confirmed this sentiment</a> and pledged to maintain SUSE's strong support for KDE.  A <a href="http://star-techcentral.com/tech/story.asp?file=/2004/1/12/technology/7079523&sec=technology">recent investigation</a> by <a href="http://star-techcentral.com/">TechCentral</a> reveals the same: <i>"SuSE will continue (to operate) as a business unit of its own."</i>  said John Phillips, Novell's corporate technology strategist for the Asia Pacific region. <i>"We don't expect to make Ximian the default user interface, and for the medium term KDE will remain the default GUI on SuSE Linux."</i>  

<!--break-->
<p>
With the number of incredible KDE Enterprise features available now and planned for the near future, the robustness and vitality of the KDE community, we can remain confident KDE will be an incontrovertible choice for Novell in the long term as well.
