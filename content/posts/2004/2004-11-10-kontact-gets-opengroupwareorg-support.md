---
title: "Kontact gets OpenGroupware.org Support"
date:    2004-11-10
authors:
  - "cschumacher"
slug:    kontact-gets-opengroupwareorg-support
comments:
  - subject: "Small incongruity on the Kontact website"
    date: 2004-11-10
    body: "On http://kontact.org/groupwareservers.php, I don't think that Kollab has a\n'vital community' but more a 'lively community it is a confusuion between Lebenskraftig and Lebsensnodwendig.\n\nWhat is the status of versioning in the webdav Kio ? With a Webddav server, it looks like that a poor man Sharepoint portal could be easily created for KDE."
    author: "Charles de Miramon"
  - subject: "Re: Small incongruity on the Kontact website"
    date: 2004-11-10
    body: "Vital community sounds fine to me, as a speaker of American English...."
    author: "Tycho Brahe"
  - subject: "Re: Small incongruity on the Kontact website"
    date: 2004-11-10
    body: "In the context, lively would make more sense to me, another follower of en_US."
    author: "eean"
  - subject: "Re: Small incongruity on the Kontact website"
    date: 2004-11-11
    body: "Vital makes sense, but can be interpreted in two wildly different ways, both of which could make sense. Vital could mean the community is active or it could mean that the community is essential, either of which could make sense in one way or another. By contrast, lively only means the first, making it a more pointed and reliable term (assuming that the second meaning was not what was meant)."
    author: "jameth"
  - subject: "Re: Small incongruity on the Kontact website"
    date: 2004-11-11
    body: "You can read/write with the WebDAV KIO to the OGo document store. Versioning needs to be controlled in the web interface though. I'm not sure whether versioning can be properly done at such a low level - AFAIK in SharePoint the versioning is also done at application plugin level (using Office plugins) and is not supported in WebFolders.\n\nSomething which would be cool (and probably not very difficult) for the WebDAV slave is the ability to maintain a local cache of the server documents similiar to Apple iDisk - using the same (standard!) mechanism described in the RFC and implemented by the Kontact resource."
    author: "Helge Hess"
  - subject: "Re: Small incongruity on the Kontact website"
    date: 2004-11-11
    body: "I think it would be quite possible to integrate locking, setting attributes and versioning into Konqueror and the standard file dialig without having to use a special application plugin.\n\nThis could be done for WebDAV in general as well as for Subversion. And Cervisia is a good example how one could even integrate diffing and more advanced versioning commands directly into the file management view.\n"
    author: "Joachim Werner"
  - subject: "Re: Small incongruity on the Kontact website"
    date: 2004-11-11
    body: "Sounds cool! If someone wants to work on such a file management extension for OpenGroupware.org, let me know :-) I would be pleased to discuss that.\n\nBTW: while WebDAV as a V in its name, it does not support versioning itself. There is an extension for WebDAV which does that, but I don't think it is widely implemented by servers (even if the provide a versioned document store)."
    author: "Helge Hess"
  - subject: "Re: Small incongruity on the Kontact website"
    date: 2004-11-11
    body: "I'm aware that plain WebDAV only supports locking, no versioning. But your post implied that OpenGroupware.org supports some sort of versioning, so I mentioned it ;-).\n\nSubversion actually implements (a subset of) the DeltaV versioning extension for WebDAV.\n\n"
    author: "Joachim Werner"
  - subject: "Re: Small incongruity on the Kontact website"
    date: 2004-11-11
    body: "Yes, OGo has a \"document storage\" plugin system somewhat comparable to KIO, you can find some info on it over here:\n  http://www.opengroupware.org/en/devs/docs/snippets/DocumentAPI/DocStorage.html\n\nBesides a simplistic filesystem storage OGo currently provides a RDBMS based one which has fast meta data, ACLs and a simple versioning system.\nAs mentioned OGo currently does not support the WebDAV version extension mostly due to the lack of clients - if KDE would support that, it might be worth consideration.\n\nMaybe this is really getting off-topic, but does Svn really use a strict subset of RFC 3253? I had the impression that some concepts are shared but that Svn has its own extensions to WebDAV making it useless as a basis to start a standards implementation.\n"
    author: "Helge Hess"
  - subject: "Re: Small incongruity on the Kontact website"
    date: 2004-11-12
    body: "http://svnbook.red-bean.com/en/1.0/apcs02.html says it all:\n\n\"So how 'compatible' is Subversion with other DeltaV software? In two words: not very.\"\n\nBut there is some common ground. Basically you can use Subversion via pure WebDAV if you can live with automatically generated checkin messages. If one only uses WebDAV clients (which is an option for a repository that is only used for shared document storage) even locking can be made to work.\n\nI wouldn't say that Subversion can be a reference server for a fully DeltaV-compliant versioned document backend, but I think that it would be quite possible to write a configurable KDE extension as the basis for both a Subversion-aware and a fully DeltaV-compliant frontend.\n\nI know that I'm getting more and more off topic, but an interesting question would be how well the different backend approaches (Subversion with its two backend implementations, OpenGroupware's two implementations, etc.) perform. If used as a document storage for a department or SOHO network such a backend should have throughputs that come close to NFS or SMB, at least for retrieving the most current version of the stored documents ..."
    author: "Joachim Werner"
  - subject: "Re: Small incongruity on the Kontact website"
    date: 2004-11-11
    body: "I can't speak as to whether the WebDAV KIO is a compliant client, nor if OGo document store implements a compliant server, but the WebDAV protocol can handle locking and versioning. \n\nWebDAV stands for \"Web-based Distributed Authoring and Versioning\" http://webdav.org/ (see the RFC at http://www.ietf.org/rfc/rfc2518.txt )"
    author: "Jonathan C. Dietrich"
  - subject: "Re: Small incongruity on the Kontact website"
    date: 2004-11-11
    body: "While the name suggest that, WebDAV (RFC 2518) does *not* implement any operation related to versioning (point to the section inside the spec ;-).\nIt does support locking, both optimistic (etags as used in the Kontact resource) as well as pessimistic (LOCK HTTP method).\n\nVersioning is added by RFC 3253 (Versioning Extensions to WebDAV):\n  http://www.ietf.org/rfc/rfc3253.txt\n\nRFC 3253 is not implemented by OGo, and I don't know any client nor server which implements it (Xythos maybe?) Same goes for WebDAV access control (RFC 3744)."
    author: "Helge Hess"
  - subject: "Re: Small incongruity on the Kontact website"
    date: 2004-11-11
    body: "I think I saw on a mailing list that somebody had started implementing RFC 3253 for the KDE webdav KIO but I don't know if it is finished.\n\nAdding integrated versioning capacity to KDE file operation be it for a local Reiser4 filesystem (maybe RFC 3253 could be implemented as a Reiser4 plugin) or a Webdav server like OGo would be a \"killing\" feature alonside with a better support for metadata."
    author: "Charles de Miramon"
  - subject: "Re: Small incongruity on the Kontact website"
    date: 2004-11-11
    body: "You mean kdenonbeta/deltav? (http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdenonbeta/deltav/)\n\nUnfortunately it hasn't been touch since almost two years.\n\nChristian"
    author: "Christian Loose"
  - subject: "Re: Small incongruity on the Kontact website"
    date: 2004-11-11
    body: "Ah, it is sad but, in a way, quite normal. If most WebDaV servers don't support the versioning addition,it is hard to create a community to develop a client and vice-versa. The classical chicken and egg problem.\n\nWhat are your plan Christian for Cervisia ? Could it be possible to extract from it a light universal versioning client targeted to regular office workers ?"
    author: "Charles de Miramon"
  - subject: "Localization"
    date: 2004-11-10
    body: "BTW, Looking at the RFC, is there any intention to handle localization issues? AFAIK OGo's libFoundation is 8 bit (problem with Hebrew/Arabic etc.) Does it mean that with the new protocol there's no need to use libFoundation?\n\nRegards,\nUri"
    author: "Uri Sharf"
  - subject: "Re: Localization"
    date: 2004-11-11
    body: "The protocol has nothing to do with the Foundation library being used, actually its even independend of the groupware server being used and only relies on HTTP, some WebDAV, iCalendar and vCard.\nMaybe it should be made explicit in the RFC, the client and server are supposed to support UTF-8 as the encoding for the iCalendar and vCard entities (text/calendar;charset=utf-8 MIME type).\n\nPS: OGo already works fine with Cocoa Foundation and probably does with the GNUstep one. Also, libFoundation does support UTF-8 and the OGo API was always based on Unicode. The pending localization issues are mostly in code which connects to plain-C libraries. There are detailed reports on them in the OGo developer list, feel free to join! ;-)"
    author: "Helge Hess"
  - subject: "Good news..."
    date: 2004-11-11
    body: "This is good news. Does someone know when there will be a Kontact version with OGo support available? Do we have to wait until KDE 4?\n\nThanks,\nMartin"
    author: "Martin Brodbeck"
  - subject: "Re: Good news..."
    date: 2004-11-11
    body: "It will likely be in the next feature release, which I think will be KDE 3.4."
    author: "Till Adam"
  - subject: "OOg accounts"
    date: 2004-11-11
    body: "I know it's a bit of a long shot, but I'd like to know whether anyone is offering sample demo accounts for opengroupware. I'd rather do a quick test over the net than install the software on my computer or download a full ISO to test it!"
    author: "Me"
  - subject: "ferocious hacking ?!?!? ^_^"
    date: 2004-11-11
    body: "I can't keep from laughing.. The timeline and presentation is so good. And I think beer was too. :-)\n@all: see the 'ferocious hacking' link!! It's a must!"
    author: "koral"
---
Members of the <a href="http://pim.kde.org/">kdepim</a> and <a href="http://www.opengroupware.org/">OpenGroupware.org</a> teams met at Magdeburg on the last
weekend of October to implement OpenGroupware.org support in <a href="http://kontact.org/">Kontact</a>. The effort
turned out to be a full success. After two days of <a href="http://www.opengroupware.org/people/hh/kdepim/">ferocious hacking</a> the team
consisting of <a href="mailto:adam@kde.org">Till Adam</a> and <a href="mailto:schumacher@kde.org">Cornelius Schumacher</a> from KDE and <a href="mailto:helge.hess@opengroupware.org">Helge He&szlig;</a> and
<a href="mailto:znek@mulle-kybernetik.com">Marcus M&uuml;ller</a> from OpenGroupware.org had completed a first
working implementation for accessing the OpenGroupware.org server through Kontact.
This adds another Free Software system to the growing list of <a href="http://kontact.org/groupwareservers.php">groupware servers</a>
supported by KDE. A special aspect of this implementation is that it is
completely based on existing open standards like HTTP, WebDAV, iCalendar and
vCard and could serve as a model for a standard access to groupware servers. The
used protocol is documented as a <a href="http://www.opengroupware.org/people/hh/draft-hess-groupdav-01.txt">draft RFC</a>.

<!--break-->
