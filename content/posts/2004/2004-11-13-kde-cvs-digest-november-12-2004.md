---
title: "KDE CVS-Digest for November 12, 2004"
date:    2004-11-13
authors:
  - "dkite"
slug:    kde-cvs-digest-november-12-2004
comments:
  - subject: "Passwordless wallets"
    date: 2004-11-13
    body: "George Staikos committed a change to kdelibs in HEAD on November 06, 2004 at 11:47:46 AM\nAdd support for insecure wallets as requested.  Crackers around the world rejoice!\n\n^^ Great, I was waiting on this one :) I know a empty password is not secure at all, but typing your password everytime you login (Kopete autostart) is annoying as hell.\n"
    author: "Niek"
  - subject: "Re: Passwordless wallets"
    date: 2004-11-13
    body: "yeah best feature ever ! i hate giving passwords twice. can the wallet not be encrypted with the login key ? if i use an empty password , is the wallet saved in plaintext ??!?!? that would not be good. i would like it to be encrypted with the login password."
    author: "chris"
  - subject: "Re: Passwordless wallets"
    date: 2004-11-13
    body: "How long is your login password? 8 chars? Do you really think 8 chars provide enough security? If you store really valuable passwords/data (your credit card number, your online banking PIN, etc.) in the wallet then the wallet password has to be at least as long as the key that the wallet uses for encryption. If you for example just use lower and upper letters and numbers then you have about 6 bit per char. A good password would need to be at least 21 chars long (to provide about 126 bit which would be sufficient to secure a 128 bit key).\n\nThe whole point of KWallet is that you just have to remember one long (!!!) password instead of 20+ short passwords.\n\nSide note: On SuSE 9.1 it's not possible to encrypt your hard disk with a password that's less than 20 chars long. Unfortunately most users don't get it, that using a short password doesn't provide any security (this is open source so you can't hope for security by obscurity). Therefore it's a good thing to force the user to choose a long password for his own good.\n\nSide note 2: Now that KWallet supports an empty password I might even consider starting to use it. After all, my hard disk is already encrypted with a sufficiently long password, so using KWallet doesn't provide any extra security on my single user machine.\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Passwordless wallets"
    date: 2004-11-13
    body: "All security experts have to get it through their skulls that not all computers need that kind of security.\n\nFor those computers it's good enough when automated exploits (from worms, viruses, etc.) can't capture the system.\n\nAlso, if you have other security measures in place (like a firewall or like in your case file system encryption) you might not need the maximum security settings of kwallet or vice versa.\n\nThe great thing about open source is that the user can choose features to fit their needs, it should be the same when it comes to security. The defaults should be secure, but security shouldn't be forced onto the user.\n\n"
    author: "Roland"
  - subject: "Re: Passwordless wallets"
    date: 2004-11-13
    body: "Yeah, choice is good. But most users are not aware of the security impacts of choosing a stupidly short and ridiculously easy guessable password. The Handelsblatt (German business newspaper) has an interview with some highly paid managers each week. One of the questions always is \"What was your first password?\". I don't remember a single interview where the answer was not \"The name of my son/daughter/dog/cat/etc.\". This makes me sick. Ebay users are not better as a recent test with a simple dictionary attack showed. A cracked Ebay password can cost the user a lot of money (if only for the time he has to spend convincing everybody that his account was hacked). So I counter your first sentence by claiming \"All internet users have to get it through their skulls that using their cat's name as password is as stupid as leaving their car unlocked in a dangerous area.\"\n\nActually, I just realized how useful KWallet is. KWallet users don't have to choose a weak (easily rememberable) Ebay password because they don't have to remember the password. KWallet will remember it for them. Now the big problem is how do we communicate this to our users? How do we make them choose strong passwords for everything? Is there actually anything we, the KDE developers, can do about this? The password dialog has already a password strength meter. Should we probably add a password strength checker to Konqueror which shows a warning each time a weak/short password is entered in a web form and which would allow the user to create a strong random password?\n\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Passwordless wallets"
    date: 2004-11-13
    body: "If now users using kwallet don't have to remember a password, what about then to automatically create a secure password for them ?"
    author: "Martin Koller"
  - subject: "Re: Passwordless wallets"
    date: 2004-11-14
    body: "There are several good secure password creator tools at kde-apps.org already. One of those could probably implemented in KWallet, but I'm personally not sure how KWallet could detect the cases where a new password is being created to automatically offer such a tool to the user."
    author: "Datschge"
  - subject: "Re: Passwordless wallets"
    date: 2004-11-14
    body: "That's just it... KWallet shouldn't need to know about this because it has nothing to do with KWallet.  It just goes into KPasswordDialog."
    author: "George Staikos"
  - subject: "Re: Passwordless wallets"
    date: 2004-11-14
    body: "Right. I was apparently only thinking of the one common border case, creating new passwords on websites where KWallet often pops up thinking it's a usual password entry."
    author: "Datschge"
  - subject: "Re: Passwordless wallets"
    date: 2004-11-14
    body: "Yes Ingo, you definitely stumbled upon one of the great kwallet features.  All of my passwords are now gibberish and I don't care.  I don't have to remember any of them.  I don't remember any of them.  Now, coupled with a filed feature request to generate random passwords in KPasswordDialog, this could be very powerful.\n\nI wonder how the users who don't believe in having any security would feel if KDE installations left port 80, 21, 23, and SMB open for them without passwords and with buffer overflows available.  Maybe even install a vulnerable suid root application too.  They don't need security, right?  That's fine then.  I wonder how many slave Linux machines would be out there then!\n\nIf they don't need security, they should use the same password on every website they visit.  Then they don't need KWallet.  They can just disable it and they have nothing to complain about.  All complaints of KWallet being too secure to be \"usable\" beyond having unencrypted passwords now go to >/dev/null as far as I'm concerned.\n"
    author: "George Staikos"
  - subject: "Re: Passwordless wallets"
    date: 2004-11-14
    body: "Hello,\n\nlong time for insecure passwords I just used the URL of the website for boards and stuff that I didn't care about. That worked nice and you must admit, is even more insecure. \n\nKWallet works better in that it also enters them for me already, I just need to submit the form then. \n\nAnd my friend, if Konqueror can query the password, how secure can it be? How do apps authenticate themselves towards the wallet? Maybe not at all?! \n\nSo if the wallet remains open during login, why not immediately after login already?\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: Passwordless wallets"
    date: 2004-11-15
    body: "There are a variety of scenarios where someone has access to the 1 and 0's on your disk (such as someone stealing your hard drive) and not able to access an authenticated wallet. "
    author: "Ian Monroe"
  - subject: "Re: Passwordless wallets"
    date: 2004-11-13
    body: "Let's say it better: security shouldn't be forced onto the user when it has some usability side-effects, as in the KWallet case."
    author: "Davide Ferrari"
  - subject: "Re: Passwordless wallets"
    date: 2004-11-14
    body: "Passwords themselves are usability problems.  Let's stop using them and just trust our neighbours.  Actually keys are a problem too, so we shouldn't have to lock our door.  It's very hard to use the door when you forget or lose your key.  Also carrying a key in your pocket is annoying."
    author: "George Staikos"
  - subject: "Re: Passwordless wallets"
    date: 2004-11-14
    body: "Exactly. Passwords are the real problem. Real keys are a bit easier because you don't have to remember the shape of the key. You just have to remember where the key is. One solution for the password problem is a KWallet which is itself not protected by a password but by a biometric key (e.g. your fingerprint). Then the users don't have to remember any passwords anymore. Given the bad support for biometric devices on Linux that's currently not more than wishful thinking."
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Passwordless wallets"
    date: 2004-11-14
    body: "I agree.  I once started a project to do biometric or smartcard support in KDE too, but it proved to be too much work and I had too little motivation.  It was going to be a perfect match for KWallet.\n\n(I actually had most of the hardware part done, but the card software was the real issue.)"
    author: "George Staikos"
  - subject: "Re: Passwordless wallets"
    date: 2004-11-15
    body: "PwManager claims to have smartcard support and it has a KWallet emulation layer.\n\nSo if someone has the needed hardware...\nhttp://passwordmanager.sourceforge.net/#features"
    author: "anonymous"
  - subject: "Re: Passwordless wallets"
    date: 2004-11-14
    body: ">>Actually keys are a problem too, so we shouldn't have to lock our door. It's very hard to use the door when you forget or lose your key. Also carrying a key in your pocket is annoying.\n\nIs this the beginning of a revolution?"
    author: "ac"
  - subject: "Re: Passwordless wallets"
    date: 2004-11-15
    body: "In my opinion, the default level of security should be (the goal of) immunity to remote attacks. Whatever sacrifices necessary to achieve that should be made, and if additional security can be obtained at no cost, then there's not reason not to have it, but additional sacrifices shouldn't be made beyond that point. If someone gains physical access to the machine, well... if someone gains physical access to their TV, they can just walk off with it, and you don't see people chaining TVs to their walls to avoid the scenario. So really, it's a nonissue for 99%+ of the userbase -- and the rest can take further measures themselves (encrypting the entire drive, whatnot).\nSo what I'd like to see is that users have to enter a password when logging in, and never again after that, unless they specifically choose to. Autogenerating highly secure passwords seems like a good idea -- perhaps Konqueror could try and detect registration forms, and fill in the password field(s) in advance? Or either way, the method to do so should be in plain sight and require minimal effort.\nAnother idea, in order to get rid of the login password hassle entirely and increase security in the process: autogenerate a hugely secure password, and then let the user put it on a USB thumb/pen/whatever drive, flash card, floppy disk, heck, CD, or whatever media they have, and then use it in the same way as a car key. Press the power switch to turn on the computer, and when they insert the 'key', automatically login the user who's key it is. And when they remove the 'key', automatically log them out. That would be rather nice, don't you think? (There should also  be a way to recover if the key is lost -- probably just forcing or forcefully suggesting the user to make backups, but that's getting into details.)\n(And again, if someone manages to steal it, well, credit cards and car keys can be stolen as well. There's no need to be paranoid to such a degree.)"
    author: "Illissius"
  - subject: "Re: Passwordless wallets"
    date: 2005-10-12
    body: "All this about autogenerating obscure passwords is well and nice, but you forget one thing: it only works on that one computer. Myself, I have a home-computer running Linux, an iBook that I carry around, and I also frequently access GMail/my blog/bloglines/my university intranet etc from University computers, my friends computers etc. That's one of the reasons I think sites like GMail are so powerful - I can be in an internet cafe in Argentina, and still have access to my entire history of emails (plus all the other information and backups I've started dropping there). Of course, GMail raises its own issues of security, but the convenience cannot be beat. So, this autogenerating of insane passwords only seems viable to people who only have one device, that they always use, which is a major problem for most of us.\n\nStian"
    author: "Stian Haklev"
  - subject: "Re: Passwordless wallets"
    date: 2004-11-13
    body: "Sorry for replying twice, but AFAIK an attacker has no way to know in advance how long the passphrase is and therefore would have to try all passwords anyway.\n\nIf I'm not wrong (and I very well could be) a short password is as good as a long password with the sole exception that a short password would be tried earlier in a brute-force approach.\n\nCan anybody confirm that?\n\n"
    author: "Roland"
  - subject: "Re: Passwordless wallets"
    date: 2004-11-13
    body: "Since any attacker not knowing the length of the password would in a brute-force approach first try all possible passwords of length 1, then of length 2, etc. a short password is always much more insecure than a longer password. Of course, you could make a short password long by simply appending a load of '0's (or whatever) to it. As long as the attacker doesn't know this cracking this padded short password will be as hard as cracking an equally long non-padded password.\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Passwordless wallets"
    date: 2004-11-13
    body: "> Since any attacker not knowing the length of the password would in a brute-force approach first try all possible passwords of length 1, then of length 2, etc. a short password is always much more insecure than a longer password.\n\nYes, that's *EXACTLY* what I've already said.\n\nHowever:\n\n- It's irrelevant how long the key is, so the claim that the password has to be longer than the key is pure nonsense.\n\n- If you assume 60 possibilities per character, you get 60^8 = 167,961,600,000,000 combinations for an 8 character password.\n"
    author: "Roland"
  - subject: "Re: Passwordless wallets"
    date: 2004-11-14
    body: "A chain is as weak as its weakest link. Thus your first claim doesn't hold in generality in practice and it surely doesn't hold in theory. Sure, as long as you are not living in a police state or are not prosecuted because of file sharing or don't have powerful competitors who are very interested in the secret details of your products the length of the password isn't too important (if it has a certain minimum length).\n\n64^8 = 2^48. DES has 56 bit keys and is considered broken since several years. I can assure you that an 48 bit key can be broken in no time by a powerful attacker. Protecting your Ebay account with a random 48 bit password is okay, but protecting your 1024 bit OpenPGP key with an 48 bit password is frivolous.\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Passwordless wallets"
    date: 2004-11-15
    body: "Is there someway to get kwallet to use ssh-add functionality.  Whenever I start one of my computers I always have to enter my login password, kwallet password, and then my ssh-add passphase. \n\nBobby"
    author: "brockers"
  - subject: "Re: Passwordless wallets"
    date: 2004-11-15
    body: "There is something to be said for short passwords in that they are easier to remember and less-likely forgotten.  Forgotten passwords are a security hazard in and of themselves because, you either need to have a system for replacing a forgotten password (which is a potential security hazard) or cause data loss if changing the password cannot be done with the original lost.  "
    author: "Joe Kowalski"
  - subject: "dcopwidgets"
    date: 2004-11-13
    body: "What ever happened with dcopwidgets? :-)"
    author: "AC"
  - subject: "Re: dcopwidgets"
    date: 2004-11-13
    body: "it had some problems with how DCOP would interact with objects created outside of the process.  Things degenerated in to COM style braindamages of reference counting. there where also some issues with signals and slots that won't be fixed until Qt 4.\n\ndon't fear though, i aim to keep this going for KDE 4 and it will be worth the wait."
    author: "Ian Reinhart Geiser"
  - subject: "Re: dcopwidgets"
    date: 2004-11-13
    body: "That's too bad. I'm really excited by dcopwidgets. I look forward to seeing it in KDE 4. :-)"
    author: "AC"
  - subject: "Expired list / KDM / KWallet"
    date: 2004-11-13
    body: "What a nice Saturday morning:\nKDM finally gets theming support and KWallet can be used\nwithout a password. These wishlist items had lots of votes and\nhave finally been resolved. Especially that they implemented\npassword-less KWallet _against_ their conviction just because\nso many people voted for it is really great IMO. It shows that\nthe KDE team is interested in what the users think and\nI'm sure this attitude will make KDE a better product at the \nend of the day because it fulfills the needs of the user-base\nnot only of the developers. \n\nThe \"to be expired\" list was an interesting read by itself.\nI've found some wishes I always thought about but I never\ntook the time to submit a bug. Now at least I can vote\nfor some of them and possibly help them from going straight to\nCoolo's purgatory ;-)\n"
    author: "Martin"
  - subject: "Knetworkmanager"
    date: 2004-11-13
    body: "Does anyone know what this knetworkmanager that Chris Howells has started is going to be? Is it a KDE version of http://people.redhat.com/dcbw/NetworkManager/ or something different?"
    author: "Sean O'Dubhghaill"
  - subject: "Re: Knetworkmanager"
    date: 2004-11-13
    body: "afaik, no it's not, because Nework Manager requires HAL, which is Linux only, and since Chris Howells used some BSD it's pointless.\n\nSaying that, it woul be VERY nice if there was one, and only one network manager in kde based on Network Manager, rateh rthan kwifimanager, and all the other obscure network managers/monitors."
    author: "cmf"
  - subject: "Re: Knetworkmanager"
    date: 2004-11-13
    body: "That's not really true. I used FreeBSD on my desktop and server but Linux on my laptop since it has marginally more change of ACPI sleep/suspend ever working.\n\nAnyway, Network Manager is going to be based on a plugin architecture to support different OSs, so there will be nobody writing a HAL/DBUS/Network Manager plugin."
    author: "Chris Howells"
  - subject: "Re: Knetworkmanager"
    date: 2004-11-14
    body: "Erm. Obviously that should have read \"nothing stopping anybody writing a HAL/DBUS/Network manager plugin\"."
    author: "Chris Howells"
  - subject: "Re: Knetworkmanager"
    date: 2004-11-14
    body: "hal is not linux only.  Hal can have backends for whatever system you want.  That is the entire point of having an hardware abstraction layer.  Hal is an attempt to create a library for dealing with hardware that will allow desktop software to easily do all sorts of cool things with the hardware without having to deal with the underlying os.  If hal does not have a backend for your os of choice, don't blame hal and discard it.  Get to work writing one because hal (or something like it) is the only way we can have good hardware interaction and portability."
    author: "theorz"
  - subject: "Digikam..."
    date: 2004-11-13
    body: "ROCKS!!\n\nThanks guys!"
    author: "Anonymous"
  - subject: "Re: Digikam..."
    date: 2004-11-13
    body: "Yeah it rocks!!\nI converted my photo collection in a minute.. and I configured my USB pendrive as external camera :-).\nGo go Digikam!!\n"
    author: "Anonymous"
  - subject: "please derek"
    date: 2004-11-13
    body: "could you change the new experimental layout? It's a real nightmare for me I have to scroll back and forth because my monitor is too small. It woud be cool if u could adjust it to people that have tiny monitor like me :)\n\nthanx in advance\n\nPS: here's a screenshot"
    author: "Pat"
  - subject: "Re: please derek"
    date: 2004-11-13
    body: "What resolution is that? Can the default fonts be smaller?\n\nThe challenge is in formatting the commit logs. They are wrapped as a reasonable length then surrounded with the pre tag. If someone has a better idea please let me know.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: please derek"
    date: 2004-11-13
    body: "800x600 i tried with smaller fonts but to get it to fit into one page large it gets so small it's unreadable."
    author: "Pat"
  - subject: "Re: please derek"
    date: 2004-11-13
    body: "Append &noleftcolumn to the url.\n\nhttp://cvs-digest.org/index.php?newissue=nov122004&noleftcolumn\n\nIs that better? There is no navigation bar or table of contents, however the next page at the bottom will lead you through everything.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: please derek"
    date: 2004-11-13
    body: "yeah it's defenetly better this way. thanx"
    author: "Pat"
  - subject: "Re: please derek"
    date: 2004-11-13
    body: "actually it renders just fine with firefox because it breaks line before it gets too long for my screen. AS an exemple for the first line of the digest:\n\nFirefox breaks the line at this point:\nKJSEmbed QT events now work on Windows. Kdm adds themes. Media kioslave <here>\n\nKonqueror breaks it at this point:\nKJSEmbed QT events now work on Windows. Kdm adds themes. Media kioslave now can use HAL. Kate improves Java and Perl syntax<here>"
    author: "Pat"
  - subject: "A Small Suggestion"
    date: 2004-11-13
    body: "Derek, I notice that when a patch is committed to HEAD and BRANCH, you have two separate entries in the digest...e.g John Smith committed a change to blah/blah in HEAD\n\nDetails here.....\n\nand then also John Smith committed a change to blah/blah in BRANCH.\n\nSame details here....\n\nWouldn't it be simpler just to combine that into.. John Smith committed a change to blah/blah in HEAD and BRANCH?\n\nDetails here....\n\n\nWould also free up more space for you to put more stuff in as well! :o)\n"
    author: "Gogs"
  - subject: "Konqueror wish - useable image viewer"
    date: 2004-11-20
    body: "Hey. \n\nA new wish that I found very interesting:\n\nhttp://bugs.kde.org/show_bug.cgi?id=93611\n\nBug 93611: Make Konq a useable image viewer - add \"next/previous\" function\n\nRight now if you want to look at pictures in Konqueror, you go to a directory in the file-browsing mode, you click on the file, the image loads, you go back, you click on the next file and so on...\n\nWhy not make it that - like in Kuickshow or ACDsee for instance - you can move on to the next and previous images?\n\nI'd suggest that when you load an image, you have extra buttons in the main toolbar so that you can go \"next/previous\" and that some keyboard keys are binded to that function.\n\nThis might even be a Junior Job and I think it would be very useful."
    author: "Wise"
---
In <a href="http://cvs-digest.org/index.php?issue=nov122004">this week's KDE CVS-Digest</a> (<a href="http://cvs-digest.org/index.php?newissue=nov122004">experimental layout</a>):
<a href="http://xmelegance.org/kjsembed/">KJSEmbed</a> <a href="http://www.trolltech.com/products/qt/index.html">Qt</a> events now work on Windows.
kdm adds themes.
Media kio slave now can use HAL.
<a href="http://kate.kde.org/">Kate</a> improves Java and Perl syntax highlighting.
KWallet adds search, empty password support, and XML data import.


<!--break-->
