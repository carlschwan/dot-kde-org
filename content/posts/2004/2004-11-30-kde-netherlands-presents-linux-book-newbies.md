---
title: "KDE-Netherlands Presents Linux Book for Newbies"
date:    2004-11-30
authors:
  - "fmous"
slug:    kde-netherlands-presents-linux-book-newbies
comments:
  - subject: "Gefeliciteerd"
    date: 2004-11-30
    body: "Congratulations!"
    author: "Boudewijn"
  - subject: "Re: Gefeliciteerd"
    date: 2004-11-30
    body: "I'll join :D\n\ncongrats, guys, nice work!"
    author: "superstoned"
  - subject: "Re: Gefeliciteerd"
    date: 2004-12-01
    body: "me too. A big 'well done' to all involved. :)\n \n--\nSimon"
    author: "Simon Edwards"
  - subject: "Linux in 10 minutes"
    date: 2004-11-30
    body: "Sams has commisioned a Teach Yourself Linux in 10 Minutes book for English as well."
    author: "Ian Monroe"
  - subject: "Re: Linux in 10 minutes"
    date: 2004-12-01
    body: "Good stuff.\nCongrats"
    author: "MoosyBlog"
  - subject: "Supporting KDE"
    date: 2004-12-02
    body: "This could be a good way of providing extra funds for the KDE project. Publish books based on specific areas of KDE (Kontact, KDE Network Admin, KDE Development etc) and the cash goes into supporting KDE (maybe with full time developers).\n\nGreat."
    author: "Ian Whiting"
  - subject: "KDE for newbies"
    date: 2004-12-18
    body: "I am dutch, haven't read the book, a huge GNome fan and maybe I should write, Linux Gnome in 5 minutes"
    author: "Marcel Roos"
  - subject: "Re: KDE for newbies"
    date: 2004-12-18
    body: "You're either a huge GNOME troll or want to say that this time in comparison reflects GNOME's lesser usefulness."
    author: "Anonymous"
  - subject: "Re: KDE for newbies"
    date: 2004-12-18
    body: "Well *he* is not posting anonymously :) \n\nAnyway .. on a more serious (and honest) note I wish you the best on writing such a book. If you are really serious you can contact me and I provide you with some additional info of the publisher.\n\nAlso there is a very active Dutch GNOME group. Please join them and share zour thoughts. \n\nKind regards, \n\n\nFabrice"
    author: "Fabrice"
---
During this summer while <A href="http://conference2004.kde.org/">aKademy</A> was filling our minds and consuming our time other things were cooking in the Dutch KDE community. Some people from <A href="http://www.kde.nl/">KDE-NL</A>, the Dutch KDE community, were offered the opportunity to write a book about Linux and KDE. This month the book '<a href="http://www.nl.bol.com/is-bin/INTERSHOP.enfinity/eCS/Store/nl/-/EUR/BOL_DisplayProductInformation-Start;sid=c0KKoqn9dwOKgOhR6Pi0lUkl6UXhmQt73iU=?BOL_OWNER_ID=1001004002103338&Section=BOOK&lgl=1&plid=&lgl_BOL_OWNER_ID=1">Linux in 10 minuten</a>' was published and officially launched by <A href="http://www.pearsoneducation.nl/">Pearson Education Benelux</A> and KDE-NL.



<!--break-->
<p>The book is intended for newcomers who want to use Linux as their desktop. We show them in a step-by-step fashion and in a clear language how to manage their daily computer tasks easily. </p>

<p>Jan Verdiesen, Novell Netherlands says:</p> 

<p><em>"This book is a great step forward in the adoption of Linux by the masses. Equipped with 'Linux in 10 minuten' it will be even more easy for me to entice people to use Linux."</em></p>


<p>Before you head off to the book shop please remember this book is only available in Dutch (for now). The entire profits of the sales of this book will be donated to KDE-Netherlands.</p>


<p>We would like to thank the following people for their time and contributions: Bram Schoenmakers, Ruurd Pels, Wilbert Berendsen, Tom Verbreyt, Fabrice Mous, Rinse de Vries, Marijke Verkaik and Tom Albers. </p>

<p>For our Dutch readers there is also a <a href="http://www.kde.nl/archief/2004-11-18-litm/index.html">Dutch press release</a>.</p>




