---
title: "aKademy Interview: Lars Stetten About Unix Accessibility"
date:    2004-09-13
authors:
  - "ateam"
slug:    akademy-interview-lars-stetten-about-unix-accessibility
comments:
  - subject: "language"
    date: 2004-09-13
    body: "The biggest problem for me is the languagebarrier. I don't speak englisch well enough to follow most discussions and it is very complicated for me to particiaption. Is it difficult to write something like babelfish on a open source basis? manylingual CMS and automatic translation/translation assistence of englisch texts is not good enough now, it will take a few years.\n\nI would also like to filter my mails with the language used. How can I find automatically out what language used is in a mail?  "
    author: "Jan"
  - subject: "Re: language"
    date: 2004-09-13
    body: "Writing translation software is *very* complex. I mean, really, really complex. I think that would be a huge undertaking to tackle as an open source project. Natural languages are extremely complex to deal with. One issue, for instance, is that a word in a language may have different meanings based on it's context. How, without some sort of understanding of the semantics of the text, will you choose the corresponding word in the other language?\n\nDetecting what language is used in a text on the other hand is much simpler, and I think that systems exist that can do it. One - probably not sufficient - way would be to analyse frequencies of words in texts in a specific language and compare these to the frequencies of the words in the text you want to classify. \n\nBTW: I guess from the errors in your English text that your native tongue is Dutch. Right? "
    author: "Andre Somers"
  - subject: "Re: language"
    date: 2004-09-13
    body: "Not quite.  Writing _correct_ translation software is very complex.  However, writing software that gets things 80% right, 80% of the time is pretty easy.  \nActually, have a look at Stanford's toolsuite (nltk.sf.net) and see what is needed -- I really don't think there is much (I can't say because every time I look they've added heaps more features, it might be finished already).\n\nBasically you parse the input sentence, and then you generate a sentence that matches your internal representation.  You can even simply this more by applying transformations on the internal representation instead of searching for a sentence that matches it (this tends to generate long and ugly sentences).\n\nAs for quality, there is no way this would produce professional quality transforations -- for that you still need human intervention. However it would be quite easy to build a system that does a better job than altavista.  Specifically, altavista uses systran which was developed in the 70s.  Systran is ugly, but the thing about code written thirty years ago is that on a modern machine, it really screams along!\n\nOh, detecting the language of text is pretty much a solved problem too (unless it is a very short text).  It isn't something I've read any papers so I'm just guessing here, but I suspect your 'insufficient' idea is actually how it is done.\n\nCorrin"
    author: "Corrin"
  - subject: "Re: language"
    date: 2004-09-15
    body: "Post your comments in Esperanto!\n\nExperience has shown that you can learn Esperanto 4times (!!) faster\nthan English.\nAdditionally Esperanto is political neutral, something you can't tell about English!\n\n"
    author: "atrink"
  - subject: "Re: language"
    date: 2004-09-17
    body: "\"Experience has shown that you can learn Esperanto 4times (!!) faster\n than English\"\n\nFor a Vietnamese?\n\n\"Additionally Esperanto is political neutral\"\n\nNo, it isn't, since most political aspect of a language are about the basic sintactical level.  Even more, Esperanto is clearly \"latin-centric\": is there not any other languages than those derived from latin?"
    author: "devil advocate"
  - subject: "Re: language"
    date: 2005-05-13
    body: "I believe the statement still holds for any _source_ language. It would still be easier to learn Esperanto than English even if you start from Vietnamese.\n\nIt would not be easier (or that much easier) to learn Esperanto than a different language more close to Vietnamese (?), but that language would probably be much harder for a speaker of English to learn.\n\nSo, if you want the _combined_ minimum effort between all people, than Esperanto is still your bet.\n\nAlex. (The comment is very late, but better late than never)"
    author: "Alex"
  - subject: "Re: language"
    date: 2005-12-24
    body: "> No, it isn't, since most political aspect of a language are about the basic sintactical level.\n\nI disagree.\n\nWe're talking culture here. Someone who speaks English fluently is subject to much influence from reading English newssites, magazines, journals and the like. Even I -- as a non-English, non-German, non-First World -- even so I'm still tainted, because I get news firsthand from reading English sources, which will have e.g., American, English or Australian bias (to name a few).\n\nEsperanto is a way to escape this, because it is mostly multicultural.\n\n> Esperanto is clearly \"latin-centric\": is there not any other languages than those derived from latin?\n\nIt's not that much latin-centric -- though vocabulary has strong latin influence, syntax is much simpler (even less latin-oriented than German, for instance).\n\nOf course, if you speak an oriental language (chinese, vietnamese, korean, japanese etc.), there's not much for you to recognize in Esperanto. But then, do you have an asiatic Esperanto you can use, even if not worldwide? You can speak Chinese, but then it's the same problem, just with a different tongue.\n\nAnd what English provides that Esperanto doesn't? Or are we going to use a monopoly-like argument: \"it's bad but don't change, because improving is costly\"?\n\nAs a side note, you seem to speak English well... do you think your Vietnam compatriots will find it easy to learn English? Esperanto is much easier to learn! And easier to master, which is important when arguing; of course, you and I will lose our advantage of knowing English rather well, huh? ;-P\n\nHappy Holidays from Brazil!"
    author: "Gr8teful"
  - subject: "Re: language"
    date: 2006-11-23
    body: "I am also in favour of esperanto, but as an additional information: theres is one international language of slavic origin which is understandable for many people (up to 400 Million in)\nsee: http://www.slovio.com\nBest regards\nDjore\n(from Spain)\n"
    author: "Djore"
  - subject: "Re: language"
    date: 2006-11-23
    body: "I am also in favour of esperanto, but as an additional information: theres is one international language of slavic origin which is understandable for many people (up to 400 Million in)\nsee: http://www.slovio.com\nBest regards\nDjore\n(from Spain)"
    author: "Djore"
  - subject: "LOL"
    date: 2004-09-13
    body: "\"control the mousecurser\"\nWell I'm the one who is constantly cursing at the mouse but\nany attempts to control me have so far been futile ;-)\nHave a nice day...\n"
    author: "Martin"
  - subject: "Speech recognition"
    date: 2004-09-15
    body: "Maybe the opening by IBM of it's speech recognition software (see http://developers.slashdot.org/article.pl?sid=04/09/13/1058241&tid=136&tid=215&tid=117&tid=8&tid=218) can be an impulse to add this technology to KDE? It could be helpfull as an accessibility tool if one would be able to use vocal commands and/or dictate texts."
    author: "Andre Somers"
  - subject: "Re: Speech recognition"
    date: 2004-09-15
    body: "As I mentionned in another news website http://linuxfr.org/2004/09/13/17194.html,\nSun also offer his toolkit as opensource http://research.sun.com/speech/\n"
    author: "jmfayard"
---
During the <a href="http://conference2004.kde.org/">KDE Community World Summit 2004</a> "aKademy" the '<a href="http://accessibility.kde.org/forum/">Unix Accessibility Forum</a>' took place which gave possibility for handicapped persons to come in touch with Unix. We talked with Lars Stetten, a partially sighted computer science student from Giessen.





<!--break-->
<p><IMG src="http://static.kdenews.org/fab/events/aKademy/pics/lars_stetten2.JPG"  align="right" border="0"></p>

<p><strong>Dear Mr. Stetten, you study computer science in Giessen. How do you estimate 
the situation for handicapped working with computers?</strong></p>

  <p>The current situation with Linux is not so good. Sure, the SUSE installation kernel has had 
  support for the braille line for many years, but you can't operate a graphical user interface 
  with this feature alone.</p>

  
<p><strong>Is the support in Windows better?</strong></p>

  <p>There is only a tiny market for handicapped accessible software. There are some suppliers, but 
  their software is expensive and health insurance funds only pay for the cheapest software. 
  Unfortunately such software does not function properly in many cases and even 
  crashes from time to time.</p>

  
<p><strong>Can you go into some details please?</strong></p>

  <p>This technology lies in between the driver and the application functionality and lacks flexibility. Let me 
  give you some examples: There is some software, that reads directly from the keyboard to preprocess key strokes. But this software 
  was written for the German keyboard layout only; it is not possible to use a keyboard with another layout. 
  I can, of course, connect another keyboard and configure my Windows box to use the proper layout for 
  the desktop, but the support for the partially sighted will fail because of the different layout! Another example from the 
  Windows world: There is a software screen magnifier, but when it is installed you will run into problems compiling as soon 
  as you try to write OpenGL applications, which makes debugging difficult. Nobody is searching for errors while running the 
  compiler in a software for screen magnification? And last I want to mention the text-to-speech software that can read out 
  the menu points from your browser, but not the content of a webpage!</p>


<p><strong>So far we have just talked about visual impairments. What is about other handicaps?</strong></p>

  <p>Visual impairments are the most common handicaps. This is the reason why there are the most tools for this area. I know 
  that there are eyetrackers and headtrackers for those with a neuromuscular handicap. With these tools, which are known to
  work well, you can control the mouse cursor or type text.</p>


<p><strong>Can we say that the partially sighted won't need special hardware?</strong></p>

  <p>This is more or less the truth for visual impairments, but not those who are blind. In the GNOME project 
  there is the support for visual impairments as part of the applications; Gnopernicus does this well, and supports
  type-to-speech, the braille line and magnification for screen areas. Harald Fernengel from Trolltech built
  support at the base on the toolkit Qt 4.0. If these classes are used by the application maintainers, KDE applications
  based upon Qt 4 will be an enthralling prospect. Because the same API that GNOME uses will be used there will 
  be no difference between a KDE or a GTK application concerning accessibility. With KMouth, KMouseTool and 
  KMagnifier the KDE project has three tools for handicapped. With these tools the audio-visual output is 
  supported and mouse control for people with neuromuscular problems are improved. KMagnifier at last is a 
  screen magnifier.</p>

<p><strong>But with alpha blending or hardware accelerated applications a software magnifier won't work. The 
software will never know what the graphics adapter has rendered.</strong></p>

  <p>This is right. You can not magnify OpenGL graphics which such solutions. But particularly for using video 
  players like Xine or MPlayer there is a simple solution that doesn't require the installation of additional 
  software: it is easy to change the screen resolution to 320x240. So you can watch movies without 
  installation work.</p>

<p><strong>Mr Stetten, we thank you for this interview and hope you have an interesting conference.</strong></p>





