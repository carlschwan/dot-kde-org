---
title: "PCLinuxOnline: The Free Groupware Killer Solution for SMBs"
date:    2004-06-30
authors:
  - "STIBS"
slug:    pclinuxonline-free-groupware-killer-solution-smbs
comments:
  - subject: "Judging from manuals"
    date: 2004-06-30
    body: "While I appreciate the fact that Kontact is able to connect to many groupware servers including MS Exchange, egroupware and Kolab it is plain ridiculous to judge on a piece of SW after reading the manual.\n\nFunny enough is the fact that while Kolab has many hundred pages of in detail manuals these manuals don't cover the installation process at all ;-)\n\nCurrently the installation of the old Kolab 1.0 is done (after downloading all required packages from ftp.zfos.org) by calling the install script 'obmtool kolab' in order to do a full installation from source. After the installation there is a single line to bootstrap the server.\n\nIf you prefer precompiled binaries than have a look at ftp://ftp.zfos.org/brew/kolab/BIN where you can find prebuild binary tarballs for 11 platforms. \n\nFor the upcoming Kolab 2.0 the packages are available from ftp.kolab.org and the same procedure applies.\n\nLast but not least there is a total integration of Kolab 1.0 into Mandrake 10."
    author: "Martin"
  - subject: "Kolab 2.0"
    date: 2004-06-30
    body: "What is the release schedule for Kolab 2.0? And what new features does it have?"
    author: "Anonymous"
  - subject: "Re: Kolab 2.0"
    date: 2004-07-03
    body: "Among other things is will feature a much easier installation procedure based on obmtool :)"
    author: "Anonymous"
  - subject: "Re: Judging from manuals"
    date: 2004-06-30
    body: "I provided a patch to have the first kolab module build using\nautotools.  The module that was patched is kolab-webadmin.\nhttp://kolab.org/cgi-bin/viewcvs-kolab.cgi/server/kolab-webadmin/kolab-webadmin/\n\nI have another tarball available with proof of concept for the\nkolab module \"kolab\".  The tarball can be obtained from:\nhttp://linux01.gwdg.de/apt4rpm/kolab/\n\nThis needs testing, more support and developers to get it going and\nworking on different distrutions.\n\nHopefully the installation will be easier, once all modules can be build with configure."
    author: "Richard"
  - subject: "Re: Judging from manuals"
    date: 2004-07-02
    body: "Last but not least there is a total integration of Kolab 1.0 into Mandrake 10.\n\nNot on my .isos. And I am a silver member."
    author: "a.c."
  - subject: "Re: Judging from manuals"
    date: 2004-07-03
    body: "Check with http://www.mandrakelinux.com/en/10.0/features/#6\n\n\"The Kolab server adds a full groupware server solution to Mandrakelinux 10.0 Official professional packs.\""
    author: "Martin"
  - subject: "Re: Judging from manuals"
    date: 2004-07-03
    body: "I installed kolab as a test. \n\nWhat absolutely goes beyond me is why these programmers ignore the FHS and stick everything under /kolab. It's even worse, they hardcoded the path of many (all?) files like IIRC $prefix/etc/ldap/ldap.conf - come on!\n\nWhat also goes beyond me is why they do not co-operate with the respective cyrus, apache, proftpd etc. projects if they needed extra features. Taking a snapshot of those software packages and building a tower on top of it is not the OSS way of doing things.\n\nI also do not understand why they try to press that stupid openpkg package format. The OpenPKG environment needs to \"bootstrap\" itself and it is not cross-platform (it did not work on PPC). The posting about the \"autotoolization\" of Kolab shows the right way - but why so late?\n\nThe OpenGroupware.org people offer a custom Knoppix ISO - why isn't there any  such thing with Kolab? There are OpenGroupware debs that neatly fit into the system - there aren't any Kolab debs (see above).\n\nThe Kroupware project chose the server of the Bynary solution - now there is no supported client software for the Win32 platform. You cannot buy Bynary Insight 1.0 anymore and version 2.0 does not work with Kolab. (Aethera is not stable yet.)\n\nMany mistakes in my eyes that are typical of proprietary software development going OSS like OpenGroupware, Mozilla, OpenOffice.org, with the speciality that Kolab consists of modules that were  OSS beforehand.\n\nI hope the Kolab people read this and reply in public. (Basiskom are regular here, I think).\n\nMark"
    author: "kmsz"
  - subject: "Re: Judging from manuals"
    date: 2004-07-03
    body: "I don't think that Basiskom has anything to do with Kolab/Proko."
    author: "Anonymous"
  - subject: "Re: Judging from manuals"
    date: 2004-07-03
    body: "Please have a look at http://www.openpkg.org/faq.html\n\nand at \n\nhttp://kroupware.org/faq/faq.html#Kolab%20Server4\n\nSome more in depth information why Kolab uses OpenPKG can be read at  http://www.openpkg.org/security.html and http://www.openpkg.org/facts.html\n\nQ: Why no Knoppix? \nA: Simply because it is only usefull as a demo tool not for deployment.\n\nQ: Which Windows client can be used with Kolab?\nA: Currently the most stable and featureful Outlook Plugin is the Toltec Connector http://www.toltec.co.za/. In addition there are multiple other Kolab clients for the Windows platform including the Aethera client from theKompany."
    author: "Martin"
  - subject: "Re: Judging from manuals"
    date: 2004-07-03
    body: "Dear kmsz,\nyour posting clearly shows some more background information is required to understand Kolab and OpenPKG.\n\n> ignore the FHS\n> \nThe \"Filesystem Hierarchy Standard\" does not support \"parallel worlds\" where a user can install multiple instances of the same or similar software, say different minor versions of apache. OpenPKG breaks out of this limitation and allows that by adding a prefix to the filesystem layout were a Unix-like filesystem structure is repeated once per instance just beneath that prefix. This allows maximum independence and least interference with the underlying OS. It would be a nice but theoretical exercice to build OpenPKG for / which means it would rip and replace the OS ;-) The FHS is targeted for Linux and was created with the help of the BSD community. OpenPKG also runs on Solaris, AIX and HPUX which do care little about a document which claims itself being a standard and not being accredited by any international standards body.\n\n> /kolab\n> \nOpenPKG can be installed multiple times on a single machine. The original authors of Kolab, while running their Kroupware project, decided to provide binaries for /kolab and use that prefix in documentation, scripts, defaults etc. Nothing prevents you from choosing an arbitrary prefix presumed you use sources.\n\n> hardcoded path\n> \nThe path get's hardcoded when OpenPKG rpm creates a binary RPM from a source RPM. This is where the prefix comes into play. The SRPM is prefix neutral, the binary RPM is locked to a specific prefix. Pathes must be hardcoded, otherwise an application might accidentially read the config of another instance or the OS, writes to the wrong logs etc.\n \n> co-operate\n> \nInitially Kolab was created under a professional contract called \"Kroupware\". They had their things to get out of the door with a fixed amount of resources and time. It is my understanding that at the beginning they were just ignored by everyone else. It is not easy to be determined but with the release of OpenPKG 2.0 in 2004Q1 all their changes to publically available packages were included into OpenPKG and some of their changes just vanished because the pristine vendor sources support them. Now they can stick with developing the heart of Kolab and need none or little patching the OSS they consume. This eventually allows them to become independent of OpenPKG and, in fact, Mandrake was the first Linux distro which jumped in and integrated the Kolab engine into their OS replacing OpenPKG packaging with Mandrake packaging.\n\n> stupid openpkg package format\n> \n[initial answer was subject to censorship and discarded after review past coffee break]\n\n> OpenPKG environment needs to \"bootstrap\" itself\n> \nThat's a fundamental part of the OpenPKG philosophy and it will stop as soon as all supported platforms use the same package manager, which will never happen.\n \n> is not cross-platform (it did not work on PPC).\n> \nOpenPKG claims itself being *the* cross-Unix-platform packaging tool. Of course it is not possible to support every OS and every CPU. Demand for PPC is negligible. One of the OpenPKG developers uses the CURRENT bootstrap from June 2004 and some CORE essentials like gcc on his 604e running Debian 3.0. OpenPKG continously improves portability of itself, RPM and the some hundred applications they offer, i.e. in 2004Q2 a volunteer ported CORE to Tru64/alpha and in 2004Q3 a HPUX/hppa machine was added to the build farm after CORE packages were ported.\n\n> late \"autotoolization\"\n> \nDunno about the timing. But it needed to be done. It was done as a first step. That's what I was talking about regarding independence from OpenPKG.\n\n> Many mistakes\n> \nAt least the decision to use OpenPKG was clearly an advantage not a mistake. It immediately allowed deployment of Kolab for a large number of OSs at a time most of them were not up to date with the required OSS, some are still not up to date or do not offer required components at all. OpenPKG fills the gap. When the gap vanishes, it's time to go using native OS support. That's what the \"autotoolization\" started.\n\nI believe Kolab has a bright future and OpenPKG will accompany it even Kolab will become more independent as it's presence and it's community grows."
    author: "Thomas Lotterer"
  - subject: "Re: Judging from manuals"
    date: 2004-07-03
    body: "> I hope the Kolab people read this and reply in public.\n\nMark, \nwe (the Kolab people) have limited resources and get many questions,\nso we cannot monitor all forums. If you want to talk to us more directly, you can use the\nkolab-devel mailing list.\n\nI must say that your rant only shows that we need to publish more information\nabout our reasons and history on our webpages, because we debated all\nthose questions in public already. Help with putting this on the web is highly appreciated.\n\nLet me add that as software group we get beaten a lot for talking to the Free Software community from all sides. Each of our team has a long list of credible Free Software experience and we fought hard about our decisions. In the end without the contracts\nand our willingness to put in extra effort to work with the  community there would be no \nKolab at all. Getting beaten and accussed for this extra efforts is sad, but it seems to be common and we still will continue doing Free Software. We have a new Groupware concept\nthat has the potential for a killer application at hand!\nBernhard"
    author: "Bernhard Reiter"
  - subject: "Re: Judging from manuals"
    date: 2004-07-03
    body: "I believe it is very impotsnt to care about standardisation in the groupware community, or \"interoperability\",\n \n Up to now I see x 80% solutions that don't really please me. Usability of most of the tools is still weak.\n \n eGroupwa\u00b4re is a nice tool but is far from usable. Products like Notes were designed with the user in mind, not a technology push products."
    author: "gerd"
  - subject: "Re: Judging from manuals - Aethera 1.1 is out toda"
    date: 2004-07-03
    body: "you mention that Aethera isn't stable, and this is true for some people and we appreciate feedback that allows us to make it more stable, but today we have released version 1.1 of Aethera for Linux, Windows and Mac OS X with even more features for Kolab, you can check it out at www.thekompany.com/projects/aethera."
    author: "Shawn Gordon"
  - subject: "Re: Judging from manuals"
    date: 2004-07-03
    body: "Well thank for the overwhelming reaction, also via e-mail.\n\n\nFirst, I'd like to apologize for the negative tone in my comment. I appreciate the efforts the \"Kolab people\" put into the project even after the initial funding has run out. I bet FOSS maintainers have to put up with rants all the time... :-(\n\nSorry for the injustice and the \"stupid package format\".\n\nI stick to my arguments, however.\n\nMartin:\n\nOf course I had a closer look to OpenPKG. I also looked at the archives. \n\nThe only reason the FAQ (http://www.openpkg.org/faq.html) mentions why there needs to be yet another package format:\n\n\"But to be honest, these tools do not satisfy the requirements of an OpenPKG like system.\"\n\nWell that's not enough.\n\nThe other link you provided brings up:\n\n\"We think that OpenPKG is currently the best solution in order to be able to support many platforms simultaneously. With the help of the OpenPKG people (especially Thomas Lotterer) we now can offer binary packages for SUSE 9.0, SuSE 8.2, Redhat 9, Redhat Enterprise Linux 3, Gentoo 1.4.3, FreeBSD 5.2, FreeBSD 4.9, Fedora 1, Debian 3.1, Debian 3.0, and Debian 2.2. \"\n\nThousands of FOSS software projects managed to get into a multitude of operating system (FOSS _and_ proprietary) without that specific OpenPKG format. You will find cyrus in almost all Unix-like operating systems, many more than those you enumerated and on many more processor platforms. I mean thousands of FOSS projects many of which have significantly less programming (wo)man-power available than Kolab.\n\nI still have the strong feeling you press your pet project OpenPKG, to the detriment of the Kolab software. \n\n\nIf it were easier to integrate into any OS, many more distribution would have done that, given the great demand for such software and the age of Kolab 1.0.\n\n\"Q: Why no Knoppix?\nA: Simply because it is only usefull as a demo tool not for deployment.\"\n\nDemo tools fare quite well. Knoppix made significant inroads just because it is a demo tool.\n\nAd Windows clients: there is currently no Windows client ready to replace the Exchange/Outlook gang. Last time I checked, the Toltec was not ready for production environment. \n\n\nThomas:\nAs you reveal your clear preference for the OpenPKG format, it does not surprise me that you underestimate the relevance of the FHS. The lack of accreditation of an international standards body is usually not an issue within FOSS if you look at the rapidly growing numbers of installations which conform to it. BTW, all binary packages listed in the Kroupware FAQ are FHS conforming Linux distributions (see quote from kroupware.org above.)\n\n\"OpenPKG can be installed multiple times on a single machine.\" \n\nOK, perhaps this is of interest to some of you, I esteem this feature not worth the hassle it entails. AFAIK, I also can install .deb and .rpm packages into other places than the default directory (as is done by debootstrap) if I happen to need several&different cyrus installations on a single machine. (Testing is done better on a test machine apart from the production instance... :-)\n\n\n\"OpenPKG claims itself being *the* cross-Unix-platform packaging tool. Of course it is not possible to support every OS and every CPU. Demand for PPC is negligible.\"\n\nNow, this is a beautiful contradiction. Either it is \"the\" cross-Unix-platform or a specific platform is negligible. BTW, have a look at the statistics, Debian PPC is still alive and kicking :-) And I can have .deb packages for 11 (or 18?) different platforms.\n\nAs a conclusion, I find bundling the Kolab development to OpenPKG a bad idea and the project found its way into mainstream FOSS distributions unfortunately way too late.\n\nBernhard:\nMay I repeat, I appreciate your efforts and unpaid work very much - I am very eager to see the new features.\n\nShawn:\nThanks for pointing me to the new Aethera release, I will check it out soon.\n\nMark"
    author: "kmsz"
  - subject: "No killer app yet?"
    date: 2004-06-30
    body: "Hmm, I don't think you can compare Kontact and Outlook. Just compare Outlook 2003 with the current CVS version of Kontact: you still see that Outlook is years older and much ahead especially form the usability point of view.\nOutlook is also visually more attractive.\n\nThere are not many features missing, e.g. KMail seems to me much better than the mail part of outlook."
    author: "Norbert"
  - subject: "Re: No killer app yet?"
    date: 2004-07-01
    body: "I think things are improving quite a bit in the attractiveness standpoint.. look at korganizer 3.1 versus 3.2, for example. \n\nMy major request is to replace the sidebar with icons in kontact with the one that most KDE preferences dialogs as well as apollon uses. It's more standard. "
    author: "anon"
  - subject: "Re: No killer app yet?"
    date: 2004-07-03
    body: "I just used Outlook XP for the first time. Open a new mail window, click on the \"To:\" button so the address selection modal window pops up. Not resizeable! I could not make the address selection widget big enough so I could see the whole name of the entries. \n\nThis Outlook software is old, really old, that's true.\n\n\nTell us, what features do you exactly miss?"
    author: "kmsz"
  - subject: "Re: No killer app yet?"
    date: 2004-07-03
    body: "Actually Outlook 2003 (the current one, after OL XP) is a substantial rewrite.\n One can tell this because it locks up less often!"
    author: "Ulrich Kaufmann"
  - subject: "bring back more quality to dot stories"
    date: 2004-06-30
    body: ">>>> Kontact connects to free groupware servers like Kolab only.<<<<\n----\nThis guy \"STIBS\" doesnt make the impression of a well informed journalist to me. Why are stories like this one approved by the dot editors?\n\nKontact has also (at least some level of) support for Microsoft Exchange and SharePoint groupware servers.\n\n\n>>>> After reading the manuals I think Kolab is a PITA to setup.<<<<\n----\nThis \"PITA to setup\" statement may be right or wrong. But certainly it cant be deduced from \"reading the manuals\".\n\n\nPlease: bring back more quality to the dot stories. Return submissions to authors and ask them to overwork them if they are poorly done at first."
    author: "kde-friend"
  - subject: "Re: bring back more quality to dot stories"
    date: 2004-06-30
    body: "> Kontact has also (at least some level of) support for Microsoft Exchange and SharePoint groupware servers.\n \nAnd is this partial support enabled (in any release)?"
    author: "Anonymous"
  - subject: "Re: bring back more quality to dot stories"
    date: 2004-06-30
    body: "Why, it most certainly is. I've been using it at least the entire KDE 3.2.x line, possibly even longer, and I'm using binary packages from kde-redhat for Redhat 7.3. Writing to an Exchange server will bring up a dialog saying that it is experimental, it's scrambles national characters (UTF-8 problem?), it's  a manual synch operation etc etc but it's there."
    author: "Anonymous"
  - subject: "Re: bring back more quality to dot stories"
    date: 2004-06-30
    body: "Why I ask? Because http://kontact.org/groupwareservers.php says that those are \"disabled for the KDE 3.2 release\"."
    author: "Anonymous"
  - subject: "Re: bring back more quality to dot stories"
    date: 2004-07-01
    body: "Yes, by default it's disabled in the KDE 3.2 release. You enable it by changing a setting."
    author: "Anonymous"
  - subject: "Re: bring back more quality to dot stories"
    date: 2004-07-02
    body: "Within the GUI? Or by tweaking a *rc only nearly undocumented setting?"
    author: "Anonymous"
  - subject: "Re: bring back more quality to dot stories"
    date: 2004-06-30
    body: "Can you point me to some documentation on using Kontact w/ Exchange?  We've got Exchange 2003, and I can retrieve email via IMAP, but Calendering isn't working."
    author: "Steve"
  - subject: "Re: bring back more quality to dot stories"
    date: 2004-07-02
    body: "> Please: bring back more quality to the dot stories.\n\nThen please submit these quality stories, will you?"
    author: "Anonymous"
  - subject: "Have to admit"
    date: 2004-07-01
    body: "that I have tried several times to get kolab going (on fedora, redhat 9, and mandrake 10 official). No go. After really going through it, it was set-up to handle just about any scalable enterprise type senario. \n\nInstead, I think that a simplier set-up would work better for SMBs and Homes. \nUse pam and regular files instead of ldap, etc.\nThe calendaring is very useful."
    author: "a.c."
  - subject: "open"
    date: 2004-07-02
    body: "Don't forget openGroupware\n\nWhat I found horrible is tha lack of standards in this field. Most groupware solutions are real crap. Freedesktop shall sztandardize the components, so we don't get x different monsters unwilling to cooperate."
    author: "hein"
  - subject: "Lucane anyone ?"
    date: 2004-09-06
    body: "Did someone try Lucane ?\nhttp://lucane.org\n\nIt's also a complete groupware solution, written in Java"
    author: "vincent"
---
Under Windows there is the Outlook/Exchange duo. Under Linux there is ...? <a href="http://www.novell.com/products/evolution/">Evolution</a> is nice but, correct me, it's not an easy task to find a *free* groupware server on Linux where it connects to. On the other hand <a href="http://www.kontact.org/">Kontact</a> <a href="http://www.kontact.org/groupwareservers.php">connects to free groupware servers</a> like <a href="http://www.kolab.org/">Kolab</a> only. After reading the manuals I think Kolab is a PITA to setup. The recent Kontact version in CVS also connects to <a href="http://www.egroupware.org/">eGroupware</a>, a <a href="http://www.phpgroupware.org/">PHPGroupware</a> fork. <a href="http://pclinuxonline.com/">PCLinuxOnline</a> features a <a href="http://pclinuxonline.com/article.php?sid=9068&mode=&order=0">Howto about the setup</a> of <a href="http://stibs.cc/pics/kontact_egroupware.png">Kontact CVS and eGroupware</a>.


<!--break-->
