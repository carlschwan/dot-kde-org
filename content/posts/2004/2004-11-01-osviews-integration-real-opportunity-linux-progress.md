---
title: "osViews: Integration, a Real Opportunity for Linux Progress"
date:    2004-11-01
authors:
  - "binner"
slug:    osviews-integration-real-opportunity-linux-progress
comments:
  - subject: "My wish..."
    date: 2004-11-01
    body: "My wish is for Konqueror to be integrated into Kontact just like Lotus Notes 7.0 has a browser embeded. I find the interface of Lotus Notes very intuitive. I hope Kontact developers can learn from this.\n\nMy other wish is also borrowed from Lotus Notes...that is, implementing tabs. I mean one tab could be having a weather applet, and another having kmail while another is having Konqueror and so on.\n\nDisclaimer: I know nothing in coding for KDE, so it might be that, what I am suggesting is not a very good approach. I liked it though."
    author: "charles"
  - subject: "Re: My wish..."
    date: 2004-11-01
    body: "well, seen Lotes Notes being used by my father (who is a reall n00b) and I think it has a pretty good interface. so copying it whoulnt hurt... but the design should stay within the boundary's of the KDE look&feel."
    author: "superstoned"
  - subject: "Re: My wish..."
    date: 2004-11-01
    body: "Well, you mean you want khtml embedded, not Konqueror really. ;-)\n\nAs far as I understand that wouldn't be hard at all, besides figuring out how it would fit into everything.\n\nI haven't used Lotus Notes so I don't know how the browser fits in with the rest, I don't really see how it could work."
    author: "kundor"
  - subject: "Learn from Notes!"
    date: 2004-11-01
    body: "The most important lesson the Kontact developers can learn from Lotus Notes are how NOT to design an UI. I think Notes has one of the worst UI I have had the pleasure to use the last few years. There probably are a few gems of good UI-design hidden in that monster, but overall it's quite horrible. It has lots of quirks and inconsistencies, things like no drag'n drop from mail to todo list. Or my personal favorite, removing a reoccurring item from one day in the calendar wipes out all instances of said event.\n\nPersonally I don't think the notion of clicking on a link in the PIM transforming it into a browser, rather than opening a browser are particularly intuitive. This perhaps has more to to with personal preferences. But for Notes user I suspect it comes more from the fact that Notes cluttered UI forces people always to run it fullscreen.\n\nAs for the use of tabs in Notes, my experience is that this is the single most confusing factor in the UI for users with skills ranging from novice to intermediate.  "
    author: "Morty"
  - subject: "Re: My wish..."
    date: 2004-11-02
    body: "\"My other wish is also borrowed from Lotus Notes...that is, implementing tabs. I mean one tab could be having a weather applet, and another having kmail while another is having Konqueror and so on.\"\n\nThis would be done by the window manager.  And in fact there are window managers that can do this.  I can't remember the names, but I've tried a couple that do this.  Search around on google.\n\n\nBtw, why would you want a browser in kontact?  What is the use-case for it?"
    author: "JohnFlux"
  - subject: "Lotus Notes has a stupid interface"
    date: 2004-11-02
    body: "I Worked one year with this tool, I think that\nthere is nothing worse than notes for reading\nmails and that it is absolutely NOT intuitive.\nThe icons are spread everywhere in the window\nso that you never know where to search, even the\nmenus are complex and things that should be\ntogether are in separated menues. No, realy,\nin one year, i could not figure out how it works.\n\nSo this is a question of beeing used to a tool,\nI was so pleased to change my job, just to get\nrid of this silly tool !\nI wish that nothing from Notes will be integrated in KDE\nwhich is VERY intuitive (okay, too many\noptions in kcontrol, but the GUI is very easy).\n\n"
    author: "Louis"
  - subject: "Re: Lotus Notes has a stupid interface"
    date: 2004-11-26
    body: "I agree.\nLotus Notes mail is the biggest pile of crap I have ever had the misfortune of using.\n\n"
    author: "anon"
  - subject: "Re: Lotus Notes has a stupid interface"
    date: 2006-02-16
    body: "Please don't copy the Lotus Notes \"UI.\"  It has a mostly unusable user interface I've ever seen.  NOTHING is intuitive in Lotus Notes, except finding the Exit command on the File menu."
    author: "Gary"
  - subject: "Re: Lotus Notes has a stupid interface"
    date: 2007-05-17
    body: "Lotus Notes for email is one of the worst examples of UI design in the history of computing.  I would rather work entirely from a DOS prompt.\n\nThe Notes email experience a bit like changing light-bulbs with wet hands.  Sure, it lights up the room, but it's bloody painful.\n\nMay the paths of Notes and KDE never cross.\n\nBrendan"
    author: "Brendan Dodds"
  - subject: "HTML interface for KDE"
    date: 2004-11-01
    body: "It would be really cool if KDE would start using a XHTML interface. This interface could be rendered with KHTML.\n\nSay, for example, the control-center, file-dialogs, and maybe even menus and the K-menu?\n\nThen, we would really be talking about integration and standards. Also, it would make it easier for new contributions. Everybody (well, almost) knows html.\n\nIs this doable?"
    author: "Arnold P"
  - subject: "Re: HTML interface for KDE"
    date: 2004-11-01
    body: "Ohhhhhh ! Stop please :)\nXHTML is for prensenting specific data (texts, images, quotes, tables,...). XHTML has nothing to do in GUI construction.\n\nIf you think about XHTML, why not using LaTeX ? :)\nYou want to sue something made for an other purpose. (X)HTML is a semantic language not a design one. KDE already use .ui files to build application's GUI and this files are in XML.\n\nMoreover you say that \"Everybody (well, almost) knows html\". It is wrong. Try to validate some random web sites with W3C (X)HTML and CSS validators. Look at their code and tell us if it is semanticly correct, if it is accessible, if it respect WAI recommandations. I don't think so :)\n\n"
    author: "Shift"
  - subject: "Re: HTML interface for KDE"
    date: 2004-11-01
    body: "The real question is whether we need windowing-Desktop Envoronments at all. A only web-browser fullscreen interface with a few local webservices top control and configure your local maschine do meet users needs better. No startmenu. Programs run like webservices or java applets embedded in a tab."
    author: "gerd"
  - subject: "Re: HTML interface for KDE"
    date: 2004-11-01
    body: "This may be better for a few new users, but its far from efficient, you have less control of whats going on.  Say you want to look at something still while working on another thing?  Can't do that in a web based environment."
    author: "Corbin"
  - subject: "Re: HTML interface for KDE"
    date: 2004-11-01
    body: "Wouldn't you just open a second browser window to do that?"
    author: "Greg"
  - subject: "Re: HTML interface for KDE"
    date: 2004-11-02
    body: "Then you would just be using Webmin."
    author: "Corbin"
  - subject: "Re: HTML interface for KDE"
    date: 2004-11-01
    body: "Did you ever actually try to write a full-fledged application with a web-interface? I did. It sucks. You have a far less rich set of options to create a usable gui, which needs to be reloaded with everything you actually do with the application. Using such a thing for *everything* would be my definition of a UI hell. Lucky for me (and the rest of the people who actually use the system), I think there is no chance KDE will head in that direction."
    author: "Andre Somers"
  - subject: "computer newbies"
    date: 2004-11-02
    body: "Its hard to come up with something new and to get a valuable unbiased feedback.\nSome weeks ago my parents (64 and 66 years old) asked me, if I could setup their first-time-computer for some easy tasks - email, web, print some text.\n\nOf course I setup a kde system: strip it down, only a few icons with text, configure kiosk, install an automatic backup system.\n\nThey are really happy with their system. No problems, they know about email folder, they look at the photos previews from their new digicam in konqi.\n\nBUT the biggest problem for my parents is to understand the concept of files and folders. In a time of google where you _find_ your information it is not that easy to accept that you have to _arrange_ your work.\n\nWhen my mother wrote a new text she asked me, why the work must be saved: \"Its there on the screen!?\"\n\nIts very interessting to look at these absolute computer newbies. I learned a lot what I have already accepted as necessary.\n\nBye\n\n  Thorsten        "
    author: "Thorsten Schnebeck"
  - subject: "Re: computer newbies"
    date: 2004-11-03
    body: "i have something similar now :D my dad asked me if he could use my computer for some tasks :D (63) - but he found the system good and logical (5 minutes for basic usage like useing mouse, and looking at docks on the cd - yes - I WAS shocked and stuned for a while :D ) - so that is the proof that no a one system would be best for all of users :]\n\n// quite funny -he is a person whose first computer operating system is aqtualy considered as one of the thoughest :D - gentoo"
    author: "Psychollek"
  - subject: "Re: computer newbies"
    date: 2004-11-03
    body: "Hey, this concept of \"the text is already written, why should I save it\" it's something that should be investigated more deeply, IMO."
    author: "Davide Ferrari"
  - subject: "SO 4"
    date: 2004-11-01
    body: "SO 4 was integrated, mozilla was integrated. \n\nA failed business ideology."
    author: "gerd"
  - subject: "Re: SO 4"
    date: 2004-11-01
    body: "Agree.\n\nIf you start adding specific code to made app X talk to app Y, you are lost.\n\nThe real point is not Integration, but interoperability. Apps should be able to talk other apps, but in an open fashion, I mean, using standardised protocols, so the apps are interchangeable.\n\n"
    author: "Shulai"
  - subject: "Re: SO 4"
    date: 2004-11-01
    body: "Bingo! Spot on! :-D\nI Could Not Agree More(TM)\n\n-Macavity"
    author: "Macavity"
  - subject: "Re: SO 4"
    date: 2004-11-01
    body: ">>> SO 4 was integrated, mozilla was integrated. <<<\n=====\nNo. Those were not _integrated_. Those were _monolithic_. There's a difference.\n"
    author: "pipitas"
  - subject: "Re: SO 4"
    date: 2004-11-02
    body: "Exactly. \nAnd the result were monster applications that are very slow to load. \n\nCompare that to how KMail displays a user's instant messenger connection status. The ICQ, Jabber, ... functionality is not duplicated in KMail but the info is gotten from Kopete *if* it's running.  Much less additional code in KMail and no slowdown in KMail loading, no matter if that feature is used or not.  If it *is* used then the user has already \"paid\" when loading Kopete (memory, CPU cycles, IO), but he doesn't have to pay twice.\n\nThat's KDE's idea of integration.  \n\n"
    author: "cm"
  - subject: "Re: SO 4"
    date: 2004-11-02
    body: "It's also one step cooler than that.\nKMail, kaddressbook, etc actually have no idea about kopete.  It uses a generic interface that any app can implement.  Konversation also implements this interface, so you can also see if any irc contacts are online (and chat to them, send them a file, and so on.)  There's also work on adding support for this to atlantik (monopoly game) and a few other apps.\n\n"
    author: "JohnFlux"
  - subject: "Re: SO 4"
    date: 2004-11-02
    body: "I just want to add that this isn't limited to KMail or Kopete. Any (KDE) app that implements the IM interface can tell apps like KMail or KAddressBook who's currently online. IIRC then the IM interface is also being implemented in Konversation."
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: SO 4"
    date: 2004-11-02
    body: "It'd be great if there was a gaim plugin to integrate it into Kontact/Kmail.  It'd be nice to be able to pick and choose between applications from different OSS \"vendors\".\n\nFor example, more integration with Firefox or Thunderbird would be nice too."
    author: "anon"
  - subject: "Re: SO 4"
    date: 2004-11-02
    body: "I think the important word here is _interface_ (read the two posts above yours by JohnFlux and Ingo Kl\u00f6cker).  Now if those apps you're talking about implemented those interfaces (if gaim implemented KDE's instant messenger interface, for example) then this would already be possible.  \n\nI don't know if implementing these interfaces would be feasible for a non-KDE app, though, or how hard it would be. \n\nFor an outlook on the integration of Firefox into KDE read Zack Rusin's blog entry: http://www.kdedevelopers.org/node/view/615\n"
    author: "cm"
  - subject: "at the end of the article posted on OSNEWS..."
    date: 2004-11-02
    body: "...the author says \"the possibilities are vast. E.g. instead of going to KDE-look to fetch wallpapers, you could have a module in the KDE Control Center which automatically connects to a server and downloads new wallpapers as they become available.\"\n\nI remember that something like this was being developed a while ago, in form of a special button that would exist in aplications, which would connect to kde-look to download specific content for it.\n\nKorganizer would download calendars, hollydays, kcontrol center would download themes, wallpapers, icons, Koffice would download clipart, etc, etc, etc. I just don't remember what the project was called or who was developing it (maybe aseigo), neither do i know what's the status of it, since I haven't lurked the mailing lists for a long time.\nA great idea, in my opinion! There's so Great stuff on kde-look, it's a shame a percentage of users won't take advantage of it because they won't browse it. "
    author: "jobezone"
  - subject: "Re: at the end of the article posted on OSNEWS..."
    date: 2004-11-02
    body: "quanta+ and korganizer have this feature already build-in.\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: at the end of the article posted on OSNEWS..."
    date: 2004-11-02
    body: "Well we need to do some things to update our site and enhance the experience. The software is called KNewStuff and it is actually used in even more places. What we did was to write some code to enhance security and allow content to be signed, but we will continue working with it. It's very cool technology and it allows any application to set up online resources."
    author: "Eric Laffoon"
  - subject: "Re: at the end of the article posted on OSNEWS..."
    date: 2004-11-02
    body: "> What we did was to write some code to enhance security and allow content to be signed\n\nAh, that's very good to hear.  "
    author: "cm"
  - subject: "Re: at the end of the article posted on OSNEWS..."
    date: 2004-11-03
    body: "That's right, it appears as a \"Get Hot New Stuff\" in the Help menu.\n\nAt this link is a tutorial for programmers who want to add this infrastructure to their apps,\nand it shows kamikaze and korganizer as examples of apps using this feature:\n\nhttp://mindx.josefspillner.de/kde/hotstuff/\n\n"
    author: "jobezone"
---
In <a href="http://www.osopinion.com/modules.php?op=modload&name=News&file=article&sid=2489">this editorial</a> published on <a href="http://www.osviews.com/">osViews</a> the author preaches integration as real opportunity for Linux to become a "killer application". As example for his argumentation he mentions KDE 3.3 and its new integration features.

<!--break-->
