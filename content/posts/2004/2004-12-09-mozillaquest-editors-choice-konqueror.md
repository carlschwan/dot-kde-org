---
title: "MozillaQuest Editor's Choice is Konqueror"
date:    2004-12-09
authors:
  - "gstaikos"
slug:    mozillaquest-editors-choice-konqueror
comments:
  - subject: "So what?"
    date: 2004-12-09
    body: "MozillaQuest has been bashing mozilla for years with almost religious fervor.\n\nIt looks to me like Mr. Angelo installed Mandrake on his computer and found out there is more to life than IE and mozilla :-)\n\nI'm a passionate KDE user, I use both konqueror and firefox and I simply love KDE/Qt APIs but why this is good for KDE/konqueror is beyond me..."
    author: "Claudio Bantaloukas"
  - subject: "MQ"
    date: 2004-12-09
    body: "Hey George. Seriously, MozillaQuest is a one huge trolling joke. "
    author: "anonymous coward"
  - subject: "Re: MQ"
    date: 2004-12-09
    body: "I have to agree.  Positive press on Konqui is good, but please stick to reputable sources.  Not all endorsements help Konqueror's image..."
    author: "ac"
  - subject: "Endurance"
    date: 2004-12-09
    body: "I'd never have thought this MQ troll would last for so many years. I just clicked the link to ensure this is actually still the same site, but I'd drop dead if I read another \"article\" from this guy."
    author: "AC79"
  - subject: "Oh no!!!"
    date: 2004-12-09
    body: "Why did you post this on the DOT???\nI like Konqueror but is it really necessary to link\nto this major trolling site???\nAngelo has an amazing track record on bashing Mozilla.\nLet's face it: This guy is completely nuts.\nI do not write sth. like this easily - but otherwise\nI cannot imagine how he keeps up for years writing\nbad things about Mozilla. I haven't seen his site for\nyears and forgot about him. Now - he's still there.\nEven if I imagine that all developers of Mozilla at once\nsaid bad things about his wife and his daughter this wouldn't\nreally explain what is driving him.\nIn an interrsting way he manages to walk _that_ close to\nthe border of being an obvious troll. \nI dont bother to read what he's saying this time. For some\nyears he always compared the number of bugs in mozilla and\nkept saying they were rising because the Mozilla  team did\nnot fix any bugs.\nAnyone working on KDE should be fair and ask himself if\nhe would like Konqueror or KDE in general should be\nrated by the number of bugs filed on bugs.kde.org.\nNo matter what you think about mozilla and/or Konqueror:\nThis guy is just trolling and there should be no place\nhere for such serious disinformation.\nPlease do not give people like him any unnecessary attention.\nThanks!\n\n\n\n"
    author: "Martin"
  - subject: "Don't know about mozillazine; konqueror is better."
    date: 2004-12-09
    body: "The only big issue I have with konqueror now is adverts.  Adverts are far too tricky now to manually code them all out by editing middleman or junkbuster files.  If there was something like Firefox's ad filtering, I'd be extremely happy :)"
    author: "Jel"
  - subject: "Re: Don't know about mozillazine; konqueror is better."
    date: 2004-12-09
    body: "Please do not confuse mozillazine with M****Quest.\nmozillazine is a reputable news magazine about Mozilla issues\nthat seldom does any other browser bashing and _especially_\nnot other open source browsers. They give profound reasons\nfor what they are writing and I respect its editor AlexBishop\nas a very good journalist.\n\nM***Quest is just a bad trolling site. 'Nuff said.\n"
    author: "Martin"
  - subject: "Re: Don't know about mozillazine; konqueror is better."
    date: 2004-12-09
    body: "Ahh, I see; that explains a lot :)\n\nYea, sorry about that; not something I'd want to do intentionally :)\n\n"
    author: "Jel"
  - subject: "Re: Don't know about mozillazine; konqueror is better."
    date: 2004-12-10
    body: "You're not alone, it is one of the most voted wish on bugs.kde.org (vote here: http://bugs.kde.org/show_bug.cgi?id=15848 ), but noone seems working on it. In the meantime, try http://www.privoxy.org . Works for any browser, but it takes a while to configure it."
    author: "Anonymous"
  - subject: "Re: Don't know about mozillazine; konqueror is better."
    date: 2004-12-10
    body: "> http://bugs.kde.org/show_bug.cgi?id=15848 ), but noone seems working on it.\n\nReally not? http://lists.kde.org/?t=110260827900002&r=1&w=2"
    author: "Anonymous"
  - subject: "Re: Don't know about mozillazine; konqueror is better."
    date: 2004-12-10
    body: "I've been using the firefox ad-blocking css listed here: http://www.mozilla.org/support/firefox/adblock\n\nIt works pretty darn well."
    author: "Nope"
  - subject: "Re: Don't know about mozillazine; konqueror is better."
    date: 2004-12-10
    body: "Thanks; I forgot about that option.  Can't find a css file that blocks the stuff on slashdot yet, though :/"
    author: "Jel"
  - subject: "Re: Don't know about mozillazine; konqueror is bet"
    date: 2004-12-13
    body: "Use Privoxy at http://www.privoxy.org/, it's just like Junkbuster (a fork, actually), but it Just Works. I find Moz AdBlock much too cumbersome to set up. With Privoxy, I just install it and change the proxy setup in all web browsers I want to use it with."
    author: "Jonas"
  - subject: "KDE org should not spread disinformation"
    date: 2004-12-09
    body: "I don't know why and how this choice was elected but in my\nopinion Firefox is the best choice.\nIf one is honest and has used Konqueror he has to say that Konqueror is everything else but robust.\nI love it but it seems to me the buggiest app for Linux at the moment.\nI at least stopped surfing the internet with it because I fear to make a wrong\nclick and it crashes again.\nConcerning the bug database most bugs really exist and some of\nthem are really severe.\nKonqueror really crashes quiet often, too often and sometimes I wished\nthe team would not spend so much time on implementing new features but\nmaking existing features work stable.\n(This is true with KDE in general)\nMore then 1500 bugs in one app is too much.\nIn saying Konqueror is robust you help promoting lies and I think we should\nnot begin to do things which M$ can do better and has more practise in."
    author: "Mephisto"
  - subject: "Re: KDE org should not spread disinformation"
    date: 2004-12-10
    body: "> I don't know why and how this choice was elected but in my opinion Firefox is the best choice.\n\nDid you miss half of his article or how good is Firefox as file manager?\n\n> I love it but it seems to me the buggiest app for Linux at the moment.\n\nPlease go trolling elsewhere.\n\n"
    author: "Anonymous"
  - subject: "Re: KDE org should not spread disinformation"
    date: 2004-12-10
    body: "Criticism is not necessarily trolling. Please be more patient with accepting other people's comments, especially when they are not written in an inflammatory or derogatory way.\nI am a huge KDE fan, but I find Firefox more suitable for my day-to-day surfing. From my experience, it is compatible with more sites than Konquerer, and has better BiDi support (though I understand the latter has been improved in 3.3.2.)\n\nElad"
    author: "Elad"
  - subject: "Re: KDE org should not spread disinformation"
    date: 2004-12-10
    body: "Criticism is not necessarily trolling\n\n-> correct, but it appears that the criticast has a buggy kde-installation, nothing more, nothing less.\n\nIt's true that firefox has a better rendering engine, but Konqi is about to use that one besides khtml, so that advantage will be gone soon.\n\nAlso 1500 bugs for an application does not mean much. Bug numbers don't inform you about how stable/good/etc an application is, since there can be a lot of duplicate reports. Also, people tend to blame Konqueror for not rendering a site well, when actually the html-code of that site is buggy..\n\nAC"
    author: "ac"
  - subject: "Re: KDE org should not spread disinformation"
    date: 2004-12-10
    body: "Bug numbers don't inform you about how stable/good/etc an application is, since there can be a lot of duplicate reports. Also, people tend to blame Konqueror for not rendering a site well, when actually the html-code of that site is buggy.\n\n1. Why does nobody close duplicated reports ?\n2. Follow the bug reports some months, the number of bugs are ever growing.\nHow long shall this take until there are 20.000 bugs and more reported for Konqui ?\n3. Every browser has to handle bugs in html-code. But most of them don't crash.\nKonqui does.\n\n "
    author: "Mephisto"
  - subject: "Re: KDE org should not spread disinformation"
    date: 2004-12-10
    body: "1.  Closing duplicates is boring and unrewarding work.  People do it but duplicates are reported faster and faster as KDE gets more users.\n2.  More users = more bug reports.  This is a sign of Konqueror's success, not its failure!\n3.  You say other browsers don't crash?  I laugh in your face."
    author: "Spy Hunter"
  - subject: "Re: KDE org should not spread disinformation"
    date: 2004-12-10
    body: "Correction:\nBut most of them don't crash.\nI know reading is difficult sometimes.\nAt least I did not see Firefox crashing 1/10 as often as Konqui !\nTell me that this is not true and I laugh in your face."
    author: "Mephisto"
  - subject: "Re: KDE org should not spread disinformation"
    date: 2004-12-10
    body: "Trolling may not be the right choice of words, but it's catchier than \"erroneous objective evaluation\" which is what appears to be on display here. People tend to think their experience is universal when it's not. For instance someone wrote me the other day that Quanta was unstable and crashed often on Fedora. I run Gentoo and Andras runs SUSE, plus many users run various distributions and we do not have the same problems, but Fedora is an unstable distribution. We have encountered a number of problems created by various package alterations on Fedora. Many people got Mandrake's \"Community Edition\" not realizing it contained a KDE CVS snapshot that was not coordinated with developers. As a result Quanta was included from a major internal change period where we told our user mailing list not to download CVS until further notice. \n\nWhen it comes to KHTML there are a variety of other factors including...\n* system setup problems\n* unstable distribution releases\n* Javascript written specifically looking for IE or Netscape\n* actual real KHTML bugs\n\nThe point is that in some cases poor software packaging by distributions and/or badly designed web sites can appear to the user to be a problem with KHTML when in fact they are not. I use KHTML daily and almost never have to get out Firefox. I do online banking and shopping as well as browsing and of course I do web design. Firefox is a nice piece of work (after all these years) and has some advantages over Konqi but I prefer the UI of Konqueror and it serves me well. I also don't have stability problems and rarely have rendering issues. If I did I would file a bug report instead of looking like a troll."
    author: "Eric Laffoon"
  - subject: "Re: KDE org should not spread disinformation"
    date: 2004-12-10
    body: "Come on Eric, that is not fair either. :-)\n\nI don't think that Fedora is an unstable version, and I don't remember either seeing Quanta crashing there. Even if it crashes the user should report this to the appropriate forum and/or submit a bugzilla entry, because \"if it is not in the bugzilla is it is not a bug\". :-)\n\nJudging a distribution to be unstable just because Quanta crashes there seems to be a very biased criterion. ;-)"
    author: "Jos\u00e9 Matos"
  - subject: "Re: KDE org should not spread disinformation"
    date: 2004-12-10
    body: "This is not limited to Quanta. Red Hat and Fedora are unfortunately known for shipping with non-standard (patched and tweaked) KDE libs leading to all sorts of more or less obvious issues."
    author: "ac"
  - subject: "Re: KDE org should not spread disinformation"
    date: 2004-12-10
    body: "Trolling may not be the right choice of words....\nIf I did I would file a bug report instead of looking like a troll. ????????\nAnd who told you that I did not ???\n"
    author: "Mephisto"
  - subject: "Re: KDE org should not spread disinformation"
    date: 2004-12-10
    body: "Amyway to blame the distibutions for every bug is nothing less than trolling\nand besides it's arrogant !\n "
    author: "Mephisto"
  - subject: "Re: KDE org should not spread disinformation"
    date: 2004-12-10
    body: "Blaming distros for every bug is of course stupid, but if you are a developer sooner or later you will face the fact that many bugs appear to users either because the users get broken packages, or broken libraries that you depend on, or broken environment, all causes that depend on the packager (read distribution) and not of the developer. Sometimes even building from source is not a solution if the distribution ships a broken compiler or some standard library.\n And from our experience RH/Fedora has most of the problems (e.g they shipped pre-release versions as stable even if new, stable versions were available; many people had problem with recent KDE packages on Fedora), but I can remember problems related to almost every big distribution including MDK (again, they shipped a CVS version which had a bug that was present there for less than a day), Gentoo (broken ebuild causing problems later for those who upgraded their KDE), SuSE (missing packages and thus not providing the latest available to the users). The common problem is that they ship whatever is at that moment in CVS and don't bother to change the version number (SuSE seems to decide to add \"Level a\" this time). So what the user later reports to be a problem in version X is not present in version X, maybe not even in version X+1, but was present somewhere between. And problems can arrive from using heavy optimizations, compiler bugs, bugs in libraries that we depend on (eg. they ship an unstable version of library X that we depend on, while there is no problem with the stable version of that library, there is a problem with the shipped one) and the list could continue.\n Regarding the number of bug reports: a reason why the list grows might be also because some reporters do no answer to mails, thus the bug cannot be fixed (maybe not even reproduced without further information), but the developer doesn't close it as it may be valid. After some time the developers may even forget about it as it's not an easy job to walk through 100+ bugs and verify them. Yet you cannot blame them that they are not working on bugfixing as many times the number of bugs fixed in the past week is bigger than the number of opened bugs.\n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: KDE org should not spread disinformation"
    date: 2004-12-10
    body: "Yes, might be true but the bug database and the number of open bugs\nis not a real good promotion for KDE.\nMaybe you should think about a system that a reported bug stayes open for\na while and if it's not confirmed after a certain period of time by any\ndeveloper or user it's automatically closed by the bug system.\nOtherwise nobody will ever be able to get control about that anymore.\nThe Bugsystem might also close any new bugs related to old KDE versions\nbecause they won't be fixed anyway and maybe still don't even exist anymore.\n"
    author: "Mephisto"
  - subject: "Re: KDE org should not spread disinformation"
    date: 2004-12-10
    body: "I use Konqueror as my everyday browser, and it crashes very rarely as of KDE 3.2. Sure, it does have rendering issues with some sites, but these are being worked on and fixed."
    author: "Paul Eggleton"
  - subject: "Re: KDE org should not spread disinformation"
    date: 2004-12-10
    body: "By the way Konqui on KDE 3.2 also seemed more relieable on me than Konqui\non KDE 3.3 at least it seemed to me.\nBut stability should increase instead of decrease.\nHope KDE 3.4 will be better again."
    author: "Mephisto"
  - subject: "Re: KDE org should not spread disinformation"
    date: 2004-12-10
    body: "> especially when they are not written in an inflammatory or derogatory way.\n\n\"not\"? This one definetely is (read the quoted part?)."
    author: "Anonymous"
  - subject: "Re: KDE org should not spread disinformation"
    date: 2004-12-10
    body: "Which ones ???\nDon't see any stating lines.\n"
    author: "Mephisto"
  - subject: "Re: KDE org should not spread disinformation"
    date: 2004-12-10
    body: "\"it seems to me the buggiest app for Linux at the moment\""
    author: "Anonymous"
  - subject: "Re: KDE org should not spread disinformation"
    date: 2004-12-10
    body: "So you don't seem to be able to distinguish between a quote and a personal opinion ! ..... O.K.\n"
    author: "Mephisto"
  - subject: "Re: KDE org should not spread disinformation"
    date: 2004-12-10
    body: "I didn't see you marking it as quote, so I assign this trolling to you."
    author: "Anonymous"
  - subject: "Re: KDE org should not spread disinformation"
    date: 2004-12-10
    body: "If you think to have an own opinion is trolling and you condemn this as trolling.\n.... O.K.\nGood bye freedom of speech.\n"
    author: "Mephisto"
  - subject: "Re: KDE org should not spread disinformation"
    date: 2004-12-10
    body: "Personally, I do not see a Konqueror crash since ages, all I get sometimes is a web site poorly rendered, but in some cases is even not Konqui's fault.\n\nSo..what?"
    author: "Davide Ferrari"
  - subject: "Re: KDE org should not spread disinformation"
    date: 2004-12-10
    body: "I can reliable crash Konqueror by switching styles or colours while a konqueror window is open. Not by browsing, though."
    author: "Boudewijn"
  - subject: "Re: KDE org should not spread disinformation"
    date: 2004-12-10
    body: "How often a day are you doing this?"
    author: "Anonymous"
  - subject: "Re: KDE org should not spread disinformation"
    date: 2004-12-10
    body: "Not every day, but a few times a week... You see, when I've finished reading the web, and I'm still waiting for Krita to compile I tend to start tinkering :-). It beats playing Klines."
    author: "Boudewijn"
  - subject: "Re: KDE org should not spread disinformation"
    date: 2004-12-11
    body: "> Good bye freedom of speech.\n\nPlease don't confuse political freedoms and someone telling you that you're spouting nonsense.  He's not saying that you can't spout nonsense -- somewhere -- he's just saying that it's inappropriate here."
    author: "Anonymous"
  - subject: "Re: KDE org should not spread disinformation"
    date: 2004-12-10
    body: "\"well-built, feature-robust, and free\" web browser and file manager\n                                       ---------------"
    author: "Mephisto"
  - subject: "Re: KDE org should not spread disinformation"
    date: 2004-12-10
    body: "1500 bugs for Konqueror the shell, not the browser part of it.  Konqueror is a container for tons of different kparts, so if there's a bug in any of those, then it usually gets reported as a konqueror bug, even though it may have nothing or little to do with konqueror at all.\nCheck how many bugs there are filed against only the browser and it will be a lot less.\n\nYes Konqueror's rendering isn't quite as good as Gecko, but for me it works for the vast majority of sites and has several crucial advantages like:\n1.  Close to instant loading time\n2.  KDE integration \n       - nice folder display for ftp, ssh, sftp sites etc\n       - spell checking in text fields\n       - KWallet\n       - common look and feel\n3. Native\n       - less resources required\n       - more responsive UI (doesn't really matter on a fast machine, but try running firefox on something old, its completely unusable on server, a Celeron 333)\n4. Web shortcuts, being able to search tons of different sites right from the address bar.  PHP documentation, debian packages, google images.. etc etc etc..  There's tons of them.\n\nSo you see, I can easily tolerate the occasional broken page (very rare) for all the benefits that Konqueror gives me."
    author: "Leo S"
  - subject: "Re: KDE org should not spread disinformation"
    date: 2004-12-10
    body: "But if there is a bug in HTML code Konqueror should not crash at once.\nFirfox has to handle the same bugs.\nBesides there are some strange bugs that still exist for ages.\nOpen a big directory with bi icons scroll up und down and you will see that\nsome icons disppear and will only reappear with you go over them with the mouse arrow. .....\nI think there are still too much bugs in KDE that are to obvious.\nSure Windows also has many bugs but it seems M$ can hide them better.\nI think in general that what people need in KDE is more stability.\nWhat's the use of loads of features within a UI, if it is too unstable to use\nit in a proffesional environment ?\nAnd besides Konquror does not have to be a egglayingwoolmilkpig.\nKonqui does not need to be able to burn CD's (K3B).\nKonqui does not need to to access FTP (KBear ....)\nThe more features the more bugs.\nAnd every new feature can have consequences on other parts.\n\n"
    author: "Mephisto"
  - subject: "Re: KDE org should not spread disinformation"
    date: 2004-12-10
    body: "> Konqui does not need to be able to burn CD's (K3B).\n\nAnd it is not able to burn CDs. You don't know the facts, what's your point except trolling?"
    author: "Anonymous"
  - subject: "Re: KDE org should not spread disinformation"
    date: 2004-12-10
    body: "Have you read tolkien too often, are you just stupid or what's wrong with you.\nIt seems you can do nothing else but attack people mister Anonymous.\nThink about who is trolling and for whom only the own opinion counts.\nIf you don't know any other word than trolling and can't react on things different\nthan with insultions just shut up and molest your psychiatrist with that.\n"
    author: "Mephisto"
  - subject: "Re: KDE org should not spread disinformation"
    date: 2004-12-10
    body: "Ever since 3.3, I have had Konqueror crash on me only once.\nKDE itself hasn't crashed at all.\nI'm not saying it doesn't crash on your machine, but for me it just works great.\n\nI only remember one site which rendered correctly in FireFox but not in Konq. I'm sure there are more, I just haven't visited them.\n\n\nThere's one site that always crashes the Flash plugin. This causes FireFox to crash immediately as well. Taking all my other windows and tabs with it.\n\nIn Konqueror, I get a dialogue box telling me that the plugin crashed, and Konq simply carries on without any problems.\n\nFireFox is great. Konqueror is great.\nUse the one that works best for you.\nAs they stand Konqueror is a little better for me."
    author: "Kim"
  - subject: "Re: KDE org should not spread disinformation"
    date: 2004-12-10
    body: "> More then 1500 bugs in one app is too much.\n\nMozilla has >8000. That's too much."
    author: "hans"
  - subject: "Re: KDE org should not spread disinformation"
    date: 2004-12-10
    body: "So? Mozilla also includes an email application and more things, and has a larger user base than Konqueror.\nI think Konqueror is good, but for web browsing Firefox is still better. For example Konqueror still has some weird problems with tabbed browsing (e.g. it's impossible to open the Google Bar's result in a new tab), and can be slow sometimes. Another very stupid problem is that people can't use it anymore when Firefox is set to their default browser."
    author: "Mark"
  - subject: "Re: KDE org should not spread disinformation"
    date: 2004-12-10
    body: "And Konqueror is more than just a web browser. It's also a file manager, a device manager, a KIO master and some much more... This comparison is stupid anyway !"
    author: "Romain"
  - subject: "Re: KDE org should not spread disinformation"
    date: 2004-12-10
    body: ">> More then 1500 bugs in one app is too much.\n>Mozilla has >8000. That's too much.\n\nSince Mozilla is more than a browser it's not fair to compare it directly against Konqueror. If I'm remembering correctly Mozilla also have an E-mail part and a HTML-editor. So if we add the bugs from KMail and Quanta to those of Konqueror we get the correct result. Then we get slightly over 2300. But that's not right either, we should also remove all bugs in Konqueror not related to webbrowsing. \n\nActually why bother, since all this is nonsens anyway. The number of bugs in a bugdatabase gives very little indication about the quality of the application anyway. The only time you get meaningful information are when there are 0 bugs, and then it only tells you that the application has no known bugs."
    author: "Morty"
  - subject: "Re: KDE org should not spread disinformation"
    date: 2004-12-10
    body: "> Actually why bother, since all this is nonsens anyway.\n\nKDE, Mozilla, Gnome, Gentoo... Well everybody who uses Bugzilla for reporting Bugs  always have more opening than closing events. number of bugs really mean nothing. Bugzilla gives more burden on the developer side than on the user side. This is a critical flaw in Bugzilla. I think there should be one more state for all bugs : \"just opened by user\". Only developer should be able to put this bug from this pre-open state to an open state. Counted open bugs should be only those that are acknowledged as bug by the developer which means that there is an acknowledgment that it must be and will be corrected sometime.\n\n> The only time you get meaningful information are when there are 0 bugs, and then it only tells you that the application has no known bugs.\n\nNope. It just means the database is not used for bug tracking :)"
    author: "Romain"
  - subject: "Re: KDE org should not spread disinformation"
    date: 2004-12-10
    body: "> there should be one more state for all bugs : \"just opened by user\"\n\nUNCONFIRMED exists."
    author: "Anonymous"
  - subject: "Re: KDE org should not spread disinformation"
    date: 2004-12-10
    body: "Indeed.\nThat's exactly what I want. But a bug opened by a user should be in that state first and not be counted as open. Which is not the case currently."
    author: "Romain"
  - subject: "Re: KDE org should not spread disinformation"
    date: 2004-12-10
    body: "Ehrrmm. that is seriously on the low side. \n\nBugzilla reports more than 50000 open bugs currently where 20000 are for the browser part alone. Just remember that Mozilla/Firefox has more than 20 times as many users to report bugs than we do."
    author: "Carewolf"
  - subject: "Re: KDE org should not spread disinformation"
    date: 2004-12-10
    body: "What's even more I think is that Mozilla/Firefox bugs are bugs reported\non Windows + bugs reportrd on Linux.\nMany but not all of them are the same.\n\n"
    author: "Mephisto"
  - subject: "RE: MozillaQuest Editor's Choice is Konqueror"
    date: 2004-12-13
    body: "Way to go Konqueror, I love the progress that this browser has made over the years. The one and only feature request that I have is a way to load a web  only interface, and pop-up blocking...but other than that lovin it...Keep up the good work guys...\n"
    author: "flock(ed)"
---
The online computer magazine <a href="http://www.mozillaquest.com/">MozillaQuest</a> has awarded <a href="http://www.konqueror.org/">Konqueror</a> their <a href="http://www.mozillaquest.com/Linux04/KDE-Konqueror-Web-browser-01_Story01.html">Editor's Choice award</a> as a <i>"well-built, feature-robust, and free"</i> web browser and file manager.  The award was given to Konqueror over Mozilla Firefox, Internet Explorer, and AOL's Netscape.  Congratulations to the Konqueror and KHTML developers on a job well done!



<!--break-->
