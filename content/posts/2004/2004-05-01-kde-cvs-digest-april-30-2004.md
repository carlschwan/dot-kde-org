---
title: "KDE-CVS-Digest for April 30, 2004"
date:    2004-05-01
authors:
  - "dkite"
slug:    kde-cvs-digest-april-30-2004
comments:
  - subject: "great job"
    date: 2004-05-01
    body: "first :-) \n\ngreat job"
    author: "chilly"
  - subject: "kde bluetooth"
    date: 2004-05-01
    body: "KDE Bluetooth really rocks. i think it should be part of the standart kde\nrelease.\n\nWhat are your opinions ??\n\nchris."
    author: "chris"
  - subject: "Re: kde bluetooth"
    date: 2004-05-01
    body: "I think so too.. of course, the developers would have to ask core-devel. It seems like something that would go into kdebase. "
    author: "anon"
  - subject: "Re: kde bluetooth"
    date: 2004-05-01
    body: "It's interesting to read on its webpage that it is 'bemused on demand'. :-)\nhttp://docs.kde.org/en/HEAD/kdeextragear-3/kdebluetooth/components.html\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: Bemused-like project for a media + home automa"
    date: 2005-05-12
    body: "Pluto is a new open source project consisting of a \u0093Core\u0094: a central media server to distribute movies, music and tv shows throughout the house; a home automation controller for lights, climate, pool, etc.; and a phone system with video conferencing. It also exposes a network boot for \u0093Media Directors\u0094 so your PCs can boot off the net as a media player, PVR and video conferencing set-top box without installing software.\n\nThe remote control software, called \u0093Orbiter\u0094, runs on Symbian Bluetooth mobile phones as well as Linux, Windows and Windows CE devices like webpads and pdas. They all feature cover art, interactive maps and floorplans, and let you control any device in the house. The UI is skinnable and multi-language. The mobile phone has a \u0091follow-me\u0092 feature so your media and other settings follow you from room to room. When you leave the house, it switches to gprs/wap. So the instant something happens at the house (burglar, doorbell, etc.) you get a live video feed, can control the house, and speak to the person through the stereos. It also includes a plug-and-play back-end for Squeeze Boxes, IP phones and cameras.\n\nCheck it out at plutohome.com. It\u0092s already useable. If any of the Bemused team would like to work with us on a bridge so Bemused and Pluto can work together, or add support for other types of phones, we have forums or you can email chris.m (at) plutohome.com"
    author: "laurac"
  - subject: "Konsole menubar"
    date: 2004-05-01
    body: "That show/hide menubar change to Konsole sounds like a bad idea. Like the bug page says, all the other KDE apps worked the same way..."
    author: "AC"
  - subject: "Re: Konsole menubar"
    date: 2004-05-02
    body: "You say it \"workED\". Due to a change in kdelibs/HEAD all applications now use adapting Show/Hide Menubar/Toolbar standard actions."
    author: "Anonymous"
  - subject: "What is KIPI?"
    date: 2004-05-02
    body: "What is KIPI and what will it mean for KimDaBa?\n\nAny pointers would be cool."
    author: "Dan"
  - subject: "Re: What is KIPI?"
    date: 2004-05-02
    body: "KIPI stands for KDE Image Plugin Interface. This is a common effort started by Digikam, Kimdaba, Showimg and Gwenview developers to be able to share image plugins.\nIt works well right now but the API is not frozen. It's currently implemented in Kimdaba and Gwenview. We are now porting Digikam plugins to the KIPI api.\n\nThere are no release of KIPI for now. It's only available from the KDE CVS, in the kdeextragear-libs-1 module. You're welcome to join the kde-imaging@kde.org mailing list if you want to speak about it."
    author: "Aur\u00e9lien G\u00e2teau"
  - subject: "Re: What is KIPI?"
    date: 2004-05-02
    body: "Interesting, how does this interact with the KImageIO system?\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: What is KIPI?"
    date: 2004-05-02
    body: "It does not really interact with KImageIO: KIPI plugins can ask the host app what image collections (aka photo albums) are available and then do whatever they want with them. Have a look at the Digikam plugin page to see what I mean ( http://digikam.sourceforge.net/plugins.html )"
    author: "Aur\u00e9lien G\u00e2teau"
  - subject: "Latest FUD"
    date: 2004-05-02
    body: "\"KDE isn't free, unless you use it for free things. You can essentially develop for KDE for free, as long as your app is for free. If you want to charge for your app, you have to pay KDE.\"\n\nSource: http://www.drunkenbatman.com/drunkenblog-archives/000257.html"
    author: "Gerd Br\u00fcckner"
  - subject: "Re: Latest FUD"
    date: 2004-05-02
    body: "This is only FUD to a lot of narrow-minded people.\n\nI'd like to make perfectly clear to people potentially taking KDE into the enterprise that this is not FUD. Paying for good development tools is totally natural for enterprises (and they *will* want to pay), and they are not going to understand the LGPL, develop everything for free arguments. With QtGTK, and I'm sure other developments, it may be possible to have more LGPL and free proprietary development avenues than there are at present. It will also increase the flexibility of KDE development, but paying for Qt and development tools is not FUD in the real world. This is slowly beginning to dawn on some people - which is why they mention it even more :).\n\nMind you, referring to this blog article, it is quite clear that this guy hasn't done his homework and is scared sh*tless of Linux/free desktops overtaking the Mac in terms of 'share' - however that happens to be measured. The 'viral' and 'niche KDE' comments were quite funny. It seems as though KDE is rather upsetting a few people."
    author: "David"
  - subject: "Re: Latest FUD"
    date: 2004-05-02
    body: "No, that's pretty much an accurate statement, not FUD.  It's in the context of a 'history and future of MacOS' discussion and is a very brief overview of the situation on Linux.  And that part is very factual, even though it glosses over quite a bit, like the existance of Qt and Trolltech.  They are not mentioned because the whole state of the Linux desktop is given two bullet points.  \n\nHis conclusions in the next couple paragraphs are dubious, but they are his idea of how things will play out.  I just spent close to an hour going through his article (and all the linked references, plus some of their links), and I think it is an excellent work.  I admit that I got the hot flash of indignation when I read that bulletpoint as well (right under a Gnome is free as in beer bullet), but that is an accurate very very brief summary of the situation.  Kneejerk cries of \"FUD\" don't change that.\n\nIncidently, his article is about the developer community on OSX, and how it came to be, why people are developing for Macs, why others aren't, and what has happened historically to the developer relations and community.  Interesting read."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Latest FUD"
    date: 2004-05-02
    body: "You think this is excellent work?\n\n\"In other words, KDE is very much taking a play from the Openstep playbook here, and if Apple doesn't watch it their Yellowbox ace-up-their-sleeve is about to be rendered scarily redundant. Apple needs to wise up to this, as I certainly doubt the idea of a developer realizing they can just pay KDE and get Linux, Windows & Mac OSX for free, which is of course what KDE wants.\"\n\nFor starters this guy seems to think that KDE is a company. He also seems to think that there is some sort of KDE takeover strategy going on, with ports of KDE apps to the Mac platform. I'm sure the people who have ported KOffice and other things to Mac OS will be pleased to know they are part of a conspiracy to kill Apple.\n\n\"I admit that I got the hot flash of indignation when I read that bulletpoint as well...\"\n\nNo you didn't :).\n\n\"Kneejerk cries of \"FUD\" don't change that.\"\n\nNot that bullet point, no, but it does lead onto something else :). The one underneath is just FUD, however, and we've heard it all before:\n\n\"History tells us that when it comes to technology, the markets are like water and tend to go towards the path of least resistance. IE, if you were a betting man, you'd bet that Gnome is going to win the desktop, and KDE is going to end up moving into some sort of niche. Probably a high-end development environment for Gnome (and possibly other) desktops.\"\n\nMuch of it is simply a promotional piece for a non-existent development environment in Mono, and a bit of KDE FUD (see above). I'll say this much for Mono - the hype generated around it by many people is so disproportionate to its real-world usage that it must qualify for some kind of record.\n\n\"More than a year ago I was walking around seeing VB developers carrying C# books around to the coffee shop, not because they were having to use it, but because they believe they're going to and scarily enough... they like it.\"\n\nThen reality kicks in, and they realise that no one can be bothered to re-write all those VB5/VB6 apps that are already working very well, thank you. They're going to use it because they are developing on Windows, and they like it because the current Windows development infrastructure isn't all that great.\n\n\"Incidently, his article is about the developer community on OSX, and how it came to be, why people are developing for Macs, why others aren't, and what has happened historically to the developer relations and community. Interesting read.\"\n\nIt was absolute meaningless tosh. The guy who wrote it isn't a developer and doesn't know much at all about what he is writing about, and it shows.\n\nWe've also got Eugenia on OSNews linking to these articles, desperately trying to tell everyone that Mono will takeover and that Apple will be moving the Mac to it. Mono is certainly a decent looking development option, but the reality is going to be somewhat more mundane than the hype.\n\nIf Apple is slashing their wrists and people feel incredibly threatened by KDE, for whatever reason, then that's just tough. Mono isn't going to change any of that."
    author: "David"
  - subject: "Re: Latest FUD"
    date: 2004-05-02
    body: "You quote the next couple paragraphs... did you miss when I said: \"His conclusions in the next couple paragraphs are dubious\"?\n\n:: \"I admit that I got the hot flash of indignation when I read that bulletpoint as well...\"\n \n: No you didn't :).\n\nYes I did you shit eating asshole.  And this statement combined with the smirk makes me want to bash your fucking face in.\n\nOkay, did that register enough indignation to satisfy your perception of my emotions?  ;)  Seriously, though... it did sting, and I did feel a need to correct him until I realized that the article is really about Apple, not KDE.  Not quite sure why you can't accept my simple word that it did affect me.  Does the sailor language help?\n\n: Not that bullet point, no, but it does lead onto something else :). The one underneath is just FUD, however, and we've heard it all before:\n\nI don't think it's FUD, just dubious reasoning.  And yeah, I've heard it many times before as KDE wins award after award and all the KDE distros dominate the Linux desktop and Gnome appears in highly publicized but never materializing corporate announcements.  Do a search on \"JabberWokky\"... I've been a KDE advocate since before Gnome was even around, and I've not changed my mind since then.  I switched to it when I made the jump from BSDi to Linux for my desktop in 1998.  Before that I ran Coherent.\n\nAs for the whole C# thing, I think it will be like Java.  For awhile everybody was chanting \"Java... Java... Java...\".  If you took it all at face value, every application and system was going to be written in this new miracle language in a couple years.  It happens every several years... I've seen the exact same thing dating back to the early 80s.  CASE tools (sometimes under a different name) are also going to \"revolutionize programming\" every several years.  You get used to it.\n\nThe article is a good history.  That's 90% of what it is.  I've said his conclusions are dubious before you even replied.  If Eugenia is linking to it for political reasons, more power to her.  It doesn't change the fact that I'm running KDE, and will continue to for the forseeable future.\n\n:  If Apple is slashing their wrists and people feel incredibly threatened by KDE, for whatever reason, then that's just tough. Mono isn't going to change any of that.\n\nI hardly think there are suicides going on in Apple.  What precisely do you mean by this paragraph?  Mono is not a player in Apple lore, although this guy seems to think it should be.  But then, the chant of \"C#... C#... C#...\" is like the chants of \"Java\" above.  \"It's going to change everything!\" is an easy thing to say.  Sometimes it does, but never overnight.\n\nSwitch your perception of the article around.  This is about OSX and Apple's positioning, not about KDE.  If you can't see it from that perspective, you can't read the article.  The only reason he delves into the *nix desktops and developers is to see \"what are they doing right\" and \"how can Apple use some of that momentum\"?  Which, for an Apple centric article, is perfectly fine. Glossing over big chunks of information is okay, as he's only pulling things that are relevant and can assist Apple's position."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Latest FUD"
    date: 2004-05-03
    body: "\"You quote the next couple paragraphs... did you miss when I said: \"His conclusions in the next couple paragraphs are dubious\"?\"\n\nYer, but the whole article is rubbish. That's the material point. He complains about GCC compiled apps being slow, but I fail to see how something like Mono or Java is going to solve that problem. The rest of it is a pessimistic rant on the decline of Apple, and sales dropping through the floor. I don't think the situation is actually as bad as he is describing.\n\n\"Yes I did you shit eating asshole. And this statement combined with the smirk makes me want to bash your fucking face in.\"\n\nYou get many comments these days from people talking about liking KDE, using it, telling everyone it is the best and then coming up with some statement about it being finished and not having corporate support. Quite what corporate support means is anyone's guess. It is unfortunate that people like me choose not to believe what many say now.\n\n\"This is about OSX and Apple's positioning, not about KDE.\"\n\nYer I realise that, but even switching it around to look at the Apple perspective much of it is still stupid. I do agree though that Apple needs to look pragmatically for a decent development environment, envourage and others, and really use it, but not in the manner that this guy is talking about. That is what his whole article is revolving around.\n\n\"The only reason he delves into the *nix desktops and developers is to see \"what are they doing right\" and \"how can Apple use some of that momentum\"?\"\n\nThat is good, but he needs to get his facts and understanding straight on *nix desktops and developers to start off with if he is going to learn. No? It just seems as though he has bought a load of rubbish about KDE, Qt and Mono. Really moving to Mono would be absolutely disastrous performance-wise for Apple, and Java isn't good enough because it has serious speed issues. It is no use arguing to the contrary, because very few people out in organisations today are using Java for client-apps because it is just too slow. Given that corporate computers an networks are slower than anything you will have at home, Java and the JVM are non-starters. Apple really needs to help companies like Trolltech and anyone developing for the Mac, and become much more inclusive (picking today's fashion isn't going to help) - his comments on Apple's market share are valid here. Apple won't, however, as we all know."
    author: "David"
  - subject: "Re: Latest FUD"
    date: 2004-05-03
    body: ": You get many comments these days from people talking about liking KDE, using it, telling everyone it is the best and then coming up with some statement about it being finished and not having corporate support. \n\nI'm not quite sure how you can say it doesn't have corporate support.  I can walk into any store and buy a copy of SuSE off the shelf and call for support if I run into a problem.  I get better support than from a copy of Windows and Microsoft (circa 1997ish, the last time I called them)."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Latest FUD"
    date: 2004-05-03
    body: "Please don't swear on a public site like this. There's absolutely no need for it."
    author: "Tom Chance"
  - subject: "Re: Latest FUD"
    date: 2004-05-03
    body: ":: Please don't swear on a public site like this. There's absolutely no need for it.\n\nSome people do not accept a simple statement of emotion.  On the internet, the three ways to reinforce a statement emotionally are comparisons to Nazis (which invokes Godwin's law), leet speak (which I am not talented at) and profanity.  Lacking the ability to simply state \"I got a hot flash of indignation when I read that\" and have it accepted, reinforcement was called for.  I'd rather see profanity than comparisons to Nazis or a mixed alphanumeric pseudo language.  At least profanity is happily ensconced in the OED. \n\nThe next paragraph makes it clear that I actually have very mild feelings regarding the author, but wished to make it clear that I can actually feel indignation, something the author questioned."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Latest FUD"
    date: 2004-05-03
    body: "> Yes I did you shit eating asshole. And this statement combined with the\n> smirk makes me want to bash your fucking face in.\n\n<Keanu>Whoa.</Keanu>"
    author: "Tukla Ratte"
  - subject: "KMail, Kontact and Others - You Guys Rock!"
    date: 2004-05-02
    body: "KMail and Kontact as a whole just keep getting better and better. I am full of admiration for how these apps, and the container app Kontact, have been built up over the past few years. Would someone care to tell me what is or isn't enterprise material now? I really can't praise the KDE-PIM/Kontact/KMail people enough - their work is absolutely stellar. The same praise is heaped on the Kopete people for the same reasons, and it is good to see new avenues for KDE development being explored with the bindings. The bindings activity has been *really* interesting over the past few weeks and months."
    author: "David"
  - subject: "Oh and Big Thanks to Derek"
    date: 2004-05-02
    body: "Your new CVS Digest 'system' is awesome and much appreciated by everyone I'm sure. Fantastic work!"
    author: "David"
  - subject: "KDE 3.3 ?"
    date: 2004-05-03
    body: "I remember there were talks on kde-devel some time ago about releasing KDE 3.3\nthis summer (IIRC august)\n\nIs that still true?"
    author: "ac"
  - subject: "Re: KDE 3.3 ?"
    date: 2004-05-04
    body: "No release schedule have been setup up yet. My bet would be 2-3 month earliest after the KDE summit."
    author: "Anonymous"
---
In <a href="http://cvs-digest.org/?issue=apr302004">this week's KDE CVS-Digest</a>: 
<a href="http://kde-bluetooth.sourceforge.net/">KDE Bluetooth</a> improves Device Discovery Service. 
<a href="http://kopete.kde.org/">Kopete</a> has a new history browser. 
Optimizations in K-menu drawing and <a href="http://kmail.kde.org/">KMail</a> POP fetching.
<a href="http://developer.kde.org/language-bindings/">Kdebindings</a> adds a graphical tool with wizards for generating bindings.
KMail adds support for <a href="http://www.fourmilab.ch/annoyance-filter/">Annoyance-Filter</a> anti-spam tool.
<!--break-->
