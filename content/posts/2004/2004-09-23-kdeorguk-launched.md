---
title: "KDE.org.uk Launched"
date:    2004-09-23
authors:
  - "fmous"
slug:    kdeorguk-launched
comments:
  - subject: "Is anyone writing KDE Custom Apps in the UK?"
    date: 2004-09-23
    body: "I think KDE is the best development framework there is for writing Custom Applications. \n\nBut I never read any articles about anyone actually using it to write business applications. Only stuff about how people install it, whether it has a word processor that matches MS Word, does it look pretty, is it usable and so on. \n\nI don't read about training courses for learning about the KDE api. There aren't any books about programming KDE in bookshops anymore. I find this worrying, because I was hoping to make a career out of writing KDE apps. Maybe it will happen, but I just don't see a critical mass of infrastructure to create a development community yet. Is there something going on out there, especially in the UK where I live, but it just isn't visible?"
    author: "Richard Dale"
  - subject: "Re: Is anyone writing KDE Custom Apps in the UK?"
    date: 2004-09-23
    body: "The framework is actually QT (www.trolltech.com). sure there are numerous enhancements by KDE, such as open/save dialogs, KApplication <-> QApplication, KIO etc. Most apps written in qt only, get \"somehow\" the same visual style like kde apps, but cannot benefit from kio, the custom dialogs. I read on the kde-developer mailing list that there are plans to do something about it..."
    author: "miro"
  - subject: "Re: Is anyone writing KDE Custom Apps in the UK?"
    date: 2004-09-23
    body: "Qt has the infrastructure - there are books, training courses, a large user base and Trolltech is making a profit. So perhaps that gets us halfway there. \n\nI think you would write a large KDE business solution very differently for a Qt one though. \n\nThere is nothing like KParts components, and so a Qt app is likely to be monolithic whereas a KDE one would be a bunch of components inside a lightweight shell. That's how KDevelop or Konqueror are structured, and that couldn't be done with plain Qt. \n\nKDE has DCOP for ubiquitous cross process scripting which allows apps to easily be controlled by other apps. That helps you to create apps as lightweight components that communicate and coordinate to model a complex workflow based system. A lot more than a scripts which just start apps.\n\nYou mention KIO and I think that's the third killer feature. I'm not sure if custom applications would need custom KIO slaves, or whether it would be sufficient to use the ones that come with KDE. \n\nI went to aKademy and met loads of developers, but I didn't meet a single user all week. Unfortunately I missed the last Sunday, which was aimed at end users - that might have changed my impression that there wasn't anyone much doing custom apps.."
    author: "Richard Dale"
  - subject: "Re: Is anyone writing KDE Custom Apps in the UK?"
    date: 2004-09-23
    body: "What kind of apps do you have in mind Richard?\n\nI might be biased since this is where my bread and butter comes from, but I think that most IT money for 'custom' apps is being spent on websites, intranets and web-based apps. The bulk of the custom work; billing systems, customer DBs, accounting systems, stock tracking etc are for a large part being done or redone as web-based applications now.\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Re: Is anyone writing KDE Custom Apps in the UK?"
    date: 2004-09-23
    body: "\"What kind of apps do you have in mind Richard?\"\n\nI suppose it's got to be the sort of thing where a web based UI wouldn't be good enough. I worked on a trading system which had a very complex UI, and the replacement for that was using java and Swing. So I guess the answer is whatever it is that people still use Swing for. Although perhaps that is on the decline, and not just because Swing is such a poor toolkit but that everything is migrating to the web as you say. UI problems or not.\n\nData visualisation? Groupware where several users could share the electronic equivalent of a white board? Workflow? KDE apps on smartphones once they have big enough screens? I'm not sure.\n\nI did write a blog recently 'Is client side Custom Application development dead?'. I certainly think any app which isn't expected to be connected to the net all the time must be dead."
    author: "Richard Dale"
  - subject: "Re: Is anyone writing KDE Custom Apps in the UK?"
    date: 2004-09-23
    body: "I agree, everybody wants web applications. IMHO for good reasons, the counter reaction on fat MS Windows clients (which was a responds to vt100 terminals). Reasons like support, upgrade, accessability, ..\n\nI disagree on Swing being a bad toolkit. Might be steep learning curve, but is very well setup (Model/View/Controler everywhere).\nIt is quite slow and heavy though. Strangely, because it can perform quite well on 2d/3d operations. It's more java itself that sucks I think.\n\nIMO one of the reasons companies choose java over say KDE/Qt or GTK is the uncertainty which way linux desktop is going. And slowness of GUIs has very much to do how it's implemented. More then 10 years ago I had a 486 with 8Mb of RAM that ran various TCL/Tk apps quite nice (was a bit slow on startup). So unless one wants to develop a 3D shooting up game or so, this is basically a non-issue."
    author: "koos"
  - subject: "Re: Is anyone writing KDE Custom Apps in the UK?"
    date: 2004-09-23
    body: "KDE has no features of itself making it interesting to develop such apps.\nThe fact is, there is a serious lack of development tools for database based apps, not only on KDE but on whole Linux platform. Most of the work is in the web front.\nQt DB support is not enough, and KNoda/Kexi are not for serious use.\nMaybe Black Adder, but is not free, and there is little interest in a closed tool for a little platform as Linux is today on the desktop.\nAlso in the propietary side, Kylix is not really DB oriented neither, and Oracle, Sybase and all those are not very interested in providing Linux-specific tools.\n\nThe only FOSS project somewhat interesting si GNUe, but the development is very slow... If KDE were born at GNUe's pace, we all would be using FVWM now.\n"
    author: "Shulai"
---
A new KDE website has been launched! <a href="http://www.kde.org.uk/">KDE.org.uk</a> promotes the K Desktop Environment and showcases activities of KDE developers and contributors around the United Kingdom. If you want to help with creating more content on KDE.org.uk or get involved in KDE events in the UK, please <a href="http://www.kde.org.uk/contact.php">contact</a> the KDE.org.uk team.




<!--break-->
