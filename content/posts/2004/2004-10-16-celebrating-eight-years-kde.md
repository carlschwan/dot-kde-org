---
title: "Celebrating Eight Years of KDE"
date:    2004-10-16
authors:
  - "dmolkentin"
slug:    celebrating-eight-years-kde
comments:
  - subject: "First Post!!!!!"
    date: 2004-10-15
    body: "No comments ;)"
    author: "Mathieu Chouinard"
  - subject: "Happy Birthday, and thank you!"
    date: 2004-10-15
    body: "Happy birthday KDE, and big thanks to all who contributed on making it. I've been using KDE programs since last century and I really appreciate all the hard work that lies beneath it's hood (I don't use the KDE desktop or window manger, but that's my taste)."
    author: "xiando"
  - subject: "Congratulations ..."
    date: 2004-10-15
    body: "to all the people involved. I really enjoy KDE and its community! Looking forward to the years to come ... \n\n\nFab"
    author: "Fabrice Mous"
  - subject: "Happy birtday!"
    date: 2004-10-15
    body: "And a big thanks to all involved!\nKDE has made computing fun again and\nis an aid to me and a lot of others\nday after day.\nLong live you!"
    author: "pieter"
  - subject: "Happy birthday KDE!!!"
    date: 2004-10-15
    body: "Wow, my favorite Linux GUI is EIGHT YEARS OLD! I had no idea this project was started so long ago.\n\nWell, what can I say, but that KDE is hands down the best *NIX graphic user interface out there, and I know there are a lot. I was hooked on it when I started using Linux back in March of 2002, when it was the default desktop for SuSE 8.0. Now I'm using Gentoo and I'm up to 3.3.0, and loving all the new functionality and eye candy. Let's face it: the great looks are what grabs people, but the ease of use and all the terrific programs is what keeps them hooked. In fact, what we have in KDE is a brainy super model!\n\nSo bravo all you intrepid coders all over the world! Keep up the astounding work, and I'll keep doing my part to spread the word about KDE here in California.\n\nCheers!"
    author: "Chris Evans"
  - subject: "Re: Happy birthday KDE!!!"
    date: 2004-10-16
    body: "A brainy supermodel? That's funny. I was converted in 2000 when I tried a second time to switch from OS/2 to Red Hat Linux. I had tried it in 1999 but was turned off because the GNOME 1.0 desktop crashed constantly. I found KDE and the first thing I noticed was that it actually had the ability to search it's docs so I could learn it, and  it was more stable. It wasn't quite up to OS/2 PM with Stardock's add on, but it has long since passed that mark. Now I'm hooked and up till all hours of the morning using Quanta, Kommander and Kdevelop. Thanks to KDE I found a platform that was worth developing applications on and even learned to hack C++... badly. ;-)\n\nSo you're in California? I'm up here in Oregon about 10 minutes drive from where Linus Torvolds lives now. It's fun to say \"Happy Birthday KDE\" and realize that I am actually part of that great community."
    author: "Eric Laffoon"
  - subject: "Re: Happy birthday KDE!!!"
    date: 2004-10-16
    body: "Did u get a chance to have a beer with Linus?"
    author: "Old guy"
  - subject: "Re: Happy birthday KDE!!!"
    date: 2004-10-18
    body: "I started using kde in its pre 1.0 days.  I believe that was '98 or so.  I had been using linux since early '96 and was delighted to find kde with its consistent and clean look and feel.  I have been using and promoting kde since then.  I have to say I have an incredible respect for the kde development team.  You guys really are amazing."
    author: "James Pellow"
  - subject: "Congratulations"
    date: 2004-10-16
    body: "Congratulations and special thanks to all who created the project, and left us a great desktop environment, and quite a nice framework for programmers."
    author: "suy"
  - subject: "Reaching basic goals?"
    date: 2004-10-16
    body: "Hmm.  Reading the original motivation is interesting.  Don't get me wrong; I love KDE.  But the motivation states that it's not a graphic environment if you're still launching vi as your editor.  I still use xterm rather than konsole for speed, and vi on that xterm rather than kate for the same reasons, unless I'm doing real development, when kdevelop comes into its own. \n\nPerhaps a little more optimisation of the basics are required to make KDE meet its basic goals.  How about at least having an editor that preloads like Konqueror does?  And... is there any reason we can't have the most commonly used apps or libraries pre-loaded, somehow?  Could a chosen list of urgently needed apps be automatically preloaded?  Kate and KWrite are both just too slow to be useful for me -- half the time, I forget what I wanted to do, by the time they load."
    author: "Lee"
  - subject: "Re: Reaching basic goals?"
    date: 2004-10-16
    body: " Uh-oh.. In my very humple and troll-free opinion that would be a bad step.. as this is the approach that mswind~1 has been using for quite some time, with the ill sideefect that it now [pre]loads everything and its mother to suite everyone (but ends up suiting no-one).\n\n I think that doing it for the filemanager only is a good temporary solution.. that should be removed again when the core technologies are optimized enough to make it feel snappy without preloading.\n\n But btw.. have you tried prelinking?\n\n/Macavity"
    author: "Macavity"
  - subject: "Re: Reaching basic goals?"
    date: 2004-10-16
    body: "I tried pre-linking a while back.  Didn't seem to make a big difference.  What would be perfect would be something like the Amiga's \"pure resident\" executables, which preloaded everything, and didn't have any statically defined variables, much like a library.  So when you called it, it was like having an app in rom, and each instance only took the time to initialise its own instance variables.\n\nI'll try prelinking again.  Now that it's been available in debian for a while, trying to maintain the prelinks may be less error-prone.  But I suspect if konqueror needs preloading to come up quickly, kate and konsole will need the same."
    author: "Lee"
  - subject: "Re: Reaching basic goals?"
    date: 2004-10-18
    body: "I believe the environment variable KDE_IS_PRELINKED has to be set to 1 to let KDE make full use of prelinked binaries. Just a quick note."
    author: "Qerub"
  - subject: "Re: Reaching basic goals?"
    date: 2004-10-16
    body: "So why not the ability to selectively choose which apps are preloaded? This would kick ass IMO. Avoid the bloat you don't need but keep the speed you do; excellent compromise. :)"
    author: "James S."
  - subject: "Re: Reaching basic goals?"
    date: 2004-10-16
    body: "And/or... let KDE select that automaticly, by monitoring how the system is being used (what applications are often started and closed again, for instance)."
    author: "Andre Somers"
  - subject: "Re: Reaching basic goals?"
    date: 2004-10-16
    body: "Idea of preselecting what would be preloaded in RAM seems great to me too! Every KDE app should have an option to be preloaded. This should be solved somehow on library level, in order only one recompile to be enought to add this feature to all apps. \n\nIs this added as an official request? I think that this should be added as a feature to kde4 if not before."
    author: "Tomislav"
  - subject: "Re: Reaching basic goals?"
    date: 2004-10-18
    body: "I don't get why so many people complain about app startup time. Just start it once and leave it running, and let the session manager start it for you at next login. I have konsole running all the time, and it's always there for me after I log in. Why bother with xterm?\n\nBtw, Kate starts in 4 seconds on my gentoo laptop - yes, it's a bit strange that my favourite KDE text editor (Kate) on my 2.4 GHz machine takes longer to load than my favourite Amiga text editor (Cedpro) on a 14 MHz machine... still, it's not slow enough to be annoying.\n"
    author: "Apollo Creed"
  - subject: "Re: Reaching basic goals?"
    date: 2004-10-18
    body: "Because that makes no sense.   If I don't need an app, I should be able to close it, and open the ones I need.  Think of it as communication: I'm saying I don't need this right now, but I do need that.  Later, when I reload a program, I'm saying, \"Okay.  Now I need this again.\"  KDE should not only cope with what I say; it should listen carefully, and adapt to my needs.  Certainly, I shouldn't have to work around KDE's quirks by keeping open apps I don't need, and wasting resources I could better use for other things.  I hate to think how much swapping would happen if I kept every app open that I use occasionally, but need quickly."
    author: "Lee"
  - subject: "Re: Reaching basic goals?"
    date: 2004-10-18
    body: "Because that makes no sense.   If I don't need an app, I should be able to close it, and open the ones I need.  Think of it as communication: I'm saying I don't need this right now, but I do need that.  Later, when I reload a program, I'm saying, \"Okay.  Now I need this again.\"  KDE should not only cope with what I say; it should listen carefully, and adapt to my needs.  Certainly, I shouldn't have to work around KDE's quirks by keeping open apps I don't need, and wasting resources I could better use for other things.  I hate to think how much swapping would happen if I kept every app open that I use occasionally, but need quickly."
    author: "Lee"
  - subject: "Happy birthday!"
    date: 2004-10-16
    body: "Many happy years to come -\nFor developers and users :)"
    author: "m."
  - subject: "Metadata?"
    date: 2004-10-16
    body: "Happy birthday KDE.\nFor the sake of history, the linked posting should be edited to add the following metadata:\n1) A date.\n2) A URL reference to the relevant mailing list archive (if any)"
    author: "Paul Leopardi"
  - subject: "Re: Metadata?"
    date: 2004-10-16
    body: "1) Tue, 15 Oct 1996 09:58:20 +0100 (MET)\n2) http://lists.trolltech.com/qt-interest/1996-10/thread00134-0.html"
    author: "Don't let us combat about timezone and date"
  - subject: "Happy Birthday, to the bright and shinny KDE."
    date: 2004-10-16
    body: "Keep up the good work, thanks for all you already have done :-)\n\nGreetings from Denmark, a small country in northern Europe. \nKDE er for fedt, lykke til."
    author: "Morten Sj\u00f8gren"
  - subject: "Long live KDE!"
    date: 2004-10-16
    body: "I guess that one of the next big steps is to start using all the goodies that are surfacing in X.org's X11R6.8 & co.\n\nKeep up the good work, everybody!"
    author: "Mikhail Capone"
  - subject: "sniff"
    date: 2004-10-16
    body: "I go to cry... :)\n\ncongratulations Matthias Ettrich ! kde today is not a dream.\n\n\"Usually these postings get a lot of answers like \"Use a Mac if you want a GUI, CLI rules!\", \"I like thousands of different widgets-libraries on my desktop, if you are too stupid to learn them, you should use windoze\", \"RAM prices are so low, I only use static motif programs\", \"You will never succeed, so better stop before the beginning\", \"Why Qt? I prefer schnurz-purz-widgets with xyz-lisp-shell. GPL! Check it out!\", etc. Thanks for not sending these as followup to this posting :-) I know I'm a dreamer... \"\n\nUSER:\nkde-i18n-pt_BR\nBRAZIL"
    author: "alexandre"
  - subject: "congratulations"
    date: 2004-10-16
    body: "to the developers, the bug-report-writers, the artists, the translation-teams, the documentation-writers and all the other people involved in this great project.\n8 years ago, nobody would've ever thought that a loose bunch people could work together so tightly to build a brilliant product like KDE is today. There's no big KDE-company, no strict organization, no big leader. Still the result of this collaborative work is ready to surpass e.g. Microsofts Windows GUI in most aspects in present and future releases.\nThank you! I've been using KDE since its first betas for 1.0 an it's amazing to see the big steps Konqui has made from release to release. Keep up the good work!"
    author: "Thomas"
  - subject: "Basic desktop tasks"
    date: 2004-10-16
    body: "so 8 years later, i still can't copy text with formatting and images from the web browser to the word processor. i won't even talk about editing images (and no, gimp is not a kde app nor is it usable anyway). not to mention releasing a \"stable\" version where the web browser doesn't work on hotmail nor gmail. long way to go for the masses."
    author: "sadfdasfsd"
  - subject: "Re: Basic desktop tasks"
    date: 2004-10-16
    body: "hotmail and gmail are working with konqueror (to admit, gmail is working since a couple of weeks ago).\ncopy text with formatting and images from the web browser to the word processor? with tables and typical html-layout?\nIf I want to use a website as basis for word processing, I use \"archive website\" from konqueror, untar the .war-file to a new folder and open the html-file with OO.writer. OO.writer will fetch all images from this folder and will display formatting including tables and all this html-stuff you normally don't want to have in text document.\n"
    author: "Thomas"
  - subject: "Re: Basic desktop tasks"
    date: 2004-10-18
    body: "i also think rich-text clipboard content is needed. and i still remember the patch implementing it for khtml several years ago, that didn't get into the tree due to some missing functionality ..."
    author: "ik"
  - subject: "Re: Basic desktop tasks"
    date: 2004-10-17
    body: ">  i still can't copy text with formatting and images from the web browser to the word processor.\n\nI know there is a patch to do this (for khtml->OOo/kword) by John Tapsell (johnflux). I have no idea if he submitted the patch to khtml devs. You might want to ask him."
    author: "anon"
  - subject: "Re: Basic desktop tasks"
    date: 2004-10-17
    body: "yeah, I use hotmail every day. because it is free, offers so much space, easy to use, almost no adds... nice...\n\nand of course, my second mail adress is Gmail. google makes alot fuzz about it, they want everyone to join it, now its not beta anymore. and its nice you can sign up for as many mailadresses you want, its easy, you dont need invitations anymore, do you?\n\nAnd the most nice thing is: this all works so great because google and microsoft spend a few minutes of their expensive time to test their code for bugs, and whether it works in all browsers or not.\n\nand its a shame you have to open quanta to be able to edit an online webpage. damn, I do this (of course) every day, like every normal user does, and its so.. stupid... copy-paste from the webbrowser to kword, why does this not work..."
    author: "superstoned"
  - subject: "Celebrating Eight Years of KDE"
    date: 2004-10-17
    body: "Greetings to the best Desktop Envirovement :)"
    author: "shal3r"
  - subject: "awesome work..."
    date: 2004-10-19
    body: "as a relative newcomer (only 18 months of linux / kde) i have to say its great ... i hardly every use windoze these days unless i have to code for it\n\ncant wait to see where kde goes in the future\n\n:)"
    author: "lauren"
  - subject: "What a great desktop environment"
    date: 2004-10-21
    body: "Fantastic. Been using KDE since it first came out and it was better than anything out there on *nix even then. Nowadays, I've converted sooooo many people from windoze because of the look and functionality that is KDE. It's just so easy to do things. \nEven at work, I use it full time on my desktop in amongst a load of Windows users and they are amazed what you get for free (have to admit that running Linux behind it is what really makes it cook).\nMy only regret being a software developer was never getting in on the application writing game. I've done a few custom apps in PyQT/KDE but really feel like I should contribute back more than just converting people over to Linux/KDE.\nBTW, can I just say that I couldn't work (as many people I know would also say) without Konsole."
    author: "Jon Scobie"
  - subject: "Amazing!!!!!"
    date: 2004-10-26
    body: "KDE has become the key which will push the Linux revolution into the real world (businesses and homes alike). You have NO IDEA how edgy M$ has been in South-east Asia, India and Pakistan as they see doom hovering :) \n\nGood to go! Though I'm a great Linux proponent I would not use Linux on my work PC if it wasnt for KDE. Great Work Team!\n\nHailing from Karachi, Pakistan (I'm sure you guys have NO IDEA what kind of penetration KDE has :) I saw a govt office running kde 3.2 the other day in a remote village in Pakistan)\n \nDid I mention I love KDE? "
    author: "FRK2"
  - subject: "Re: Amazing!!!!!"
    date: 2004-10-26
    body: "Cool, how about making some kind of photo story about that and submit it to dot (this site)? =)"
    author: "ac"
---
Almost unnoticed, our <a href="http://www.kde.org/">favorite Desktop Environment</a> has turned eight. <a href="http://www.kde.org/announcements/announcement.php">This posting</a> by a young and daring student called <a href="http://people.kde.org/matthias.html">Matthias Ettrich</a> started it all. Since then, KDE has come a long way. It did not only evolve technically, but also has one of the greatest  <a  href="http://www.kde.org/people/credits/">communities</a>, where <a  href="http://people.kde.org/">people</a> are not only terribly productive but friends. Thanks to all contributors and happy birthday KDE!
<!--break-->
