---
title: "Using KGpg with KMail"
date:    2004-04-18
authors:
  - "mgagn\u00e9"
slug:    using-kgpg-kmail
comments:
  - subject: "One thing I would like in Kmail"
    date: 2004-04-18
    body: "I have my inbox set to forward things from comcast to a larger mailbox as an archive. I want my sent messages to be forwarded as well and sent to my inbox. In Mozilla this is easy to setup, but in Kmail I can't find a way to do this in a simple way. \n\nThis is something I use foten in Mozilla Mail and I haven't found a way to do this in Kmail. Any tips?\n\nAnyway, Kmail is great and I still switched to it because I like it's integration and speed. Though secret messages are an added benefit! ;)\n\nThank you!"
    author: "Alex"
  - subject: "Re: One thing I would like in Kmail"
    date: 2004-04-18
    body: "I guess you could do that by setting a bcc: to your own email address \nfor each of the identities you use.\n\nThat way your account will receive a copy of each mail you send.\n\nIt's under Settings -> Configure KMail -> Identities"
    author: "cm"
  - subject: "Re: One thing I would like in Kmail"
    date: 2004-04-18
    body: "Hi Alex,\n\ni'm using kmail with my local imap server. As i want to keep track of my sent emails, i store them in my imap mailbox. I've created a folder 'SENT' in my imap account, where i want to store them. In kmail's preferences -> identities on the extended tab (second tab on my kde 3.2.1 installation) you can set the folder for sent messages. If you have more than one outgoing email server, you only have to create an identity for each server and set the right outgoing mail server (needs to be configured before) on the same tab as above. This way, you can store mails to different mail servers in different folders.\n\nHope, this helps,\n\nMarkus"
    author: "Markus Rachbauer"
  - subject: "Re: One thing I would like in Kmail"
    date: 2004-04-18
    body: "Personally, I'd use a filter for this. Something like this:\n\nConditions:\n<From>    equals       myaddress@mydomain.com\nActions:\nForward to   myaddress@mydomain.com\nApply to: Sent Messages\nStop further processing.\n\nSince I use the german version, I can't guarantee that all the options have the exact same names, but it should be possible to figure it out."
    author: "Andreas Steffen"
  - subject: "Thanks everyone"
    date: 2004-04-18
    body: "All these solutions work great. =)\n\n"
    author: "Alex"
  - subject: "PKCS#11 tokens support"
    date: 2004-04-18
    body: "What I would really like to see in KMail is support for PKCS#11 security tokens. Mozilla has it (Privacy & Security -> Certificates -> Manage security devices...) so I'm able to use my keypair residing in employer's security smartcard with Mozilla and don't have to mess around with generating new keypair and trustability of it.\n"
    author: "anon"
  - subject: "Re: PKCS#11 tokens support"
    date: 2004-04-18
    body: "Heck yeah!  This is my #1 complaint about KMail.  I've tried following the HOWTO for using openssl/certificate with signed/encrypted emails, but it is (1) overly complicated and (2) very flaky, when it works at all."
    author: "repugnant"
  - subject: "Re: PKCS#11 tokens support"
    date: 2004-04-22
    body: "I second this motion.  It gets real old fast having to resubmit my password to my certificate periodically when I don't have it set to be so.\n\nNot to mention complaint number TWO:\n\nFilters on IMAPD folders.  Right now I move everything by hand but hell if Konqueror can SFTP via drag n' drop you'd think KMail could filter outside of the local mail folders."
    author: "Marc J. Driftmeyer"
  - subject: "Re: PKCS#11 tokens support"
    date: 2005-05-18
    body: "Hi, currently i am doing a sch project on PKCS #11. I've read the documents on PKCS #11 found in RSA sites. But i still could not understand what is PKCS #11 about. Can anybody guide me?\n\nThanks."
    author: "Joan"
  - subject: "Re: PKCS#11 tokens support"
    date: 2005-05-18
    body: "Sorry for the error, it should be PKCS #11 v2.11\n"
    author: "Joan"
  - subject: "Kmail"
    date: 2004-04-18
    body: "Is it possible to detect the language of an email?\n\nHow can GnuPG be integrated in the spamfilter system. I think a web of trust is a good idea to avoid junk, my spam filtering relies on my whitelist. "
    author: "Bert"
  - subject: "PGP/Mime less useful than pgp encryption"
    date: 2004-04-18
    body: "While working with business partners and exchanging encrypted mail with kmail, I have discovered that actually very few pgp programs support pgp/mime (the way to send encrypted attachment directly with your mail).\n\nSo, I had to encrypt manually the document to all the person I want to send it to, and then attach the right encrypted document to the mail and send it. Very tedious. ThunderBird allows me to send encrypted documents using either pgp/mime or by encrypting them separately for each recipient. Because of this, I now use thunderbird to send encrypted emails!\n\nThe errors reported by kmail are also quite poor. One of my partner uses IDEA which is not in gpg. All kmail could tell me is that I entered a wrong passphrase although I just opened the mail.\n\nI think there is still a lot of work to make kmail work seemlessly with encrypted stuff."
    author: "Philippe Fremy"
  - subject: "How about S/Mime?"
    date: 2004-04-21
    body: "Well, GPG seems to work, but how about S/MIME? The support has been announced\na while ago. However, I don't think it's possible to configure. I wish somebody figured it out and wrote an article in English..."
    author: "Nicholas Sushkin"
  - subject: "PKCS#11 Support in GnuPG"
    date: 2006-10-13
    body: "Please try the following implementation:\nhttp://gnupg-pkcs11.sourceforge.net\n\nIt enables PKCS#11 support in GnuPG."
    author: "Alon Bar-Lev"
---
Finding you need to send top-secret email communication with <a href="http://kmail.kde.org/">KMail</a>, but you don't know where to start? An <a href="http://www.linuxjournal.com/article.php?sid=7354">article in Linux Journal</a> (part of Marcel Gagné's "Cooking with Linux" series) shows you how to use the Gnu Privacy Guard (<a href="http://www.gnupg.org">GnuPG</a>) in conjunction with <a href="http://devel-home.kde.org/~kgpg/">KGpg</a> to secure your email. Learn how to generate and import keys, sign and encrypt email, and otherwise keep your private communication away from prying eyes. 



<!--break-->
