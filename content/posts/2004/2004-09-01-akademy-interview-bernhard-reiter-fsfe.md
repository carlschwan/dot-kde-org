---
title: "aKademy Interview: Bernhard Reiter, FSFE"
date:    2004-09-01
authors:
  - "tchance"
slug:    akademy-interview-bernhard-reiter-fsfe
comments:
  - subject: "Non-issue"
    date: 2004-09-01
    body: "I consider the lack of GPL'ed Qt for Windows an annoying steady exaggereated non-issue. Qt already is free as in freedom for both the X Window and the Mac OS X version, and nobody hinders anyone to port one of those to Windows as well if the demand is really that big like all those requests to Trolltech to offer a GPL'ed Qt/Win want us to believe. The reality is that Trolltech already considered diverse approaches of offering free Qt version for Windows which all were more abused than the two other more free version, and there's even an effort to port Qt/X11 to Windows (which the FSF could support instead) which is rather slow since the Windows audience seems to be more about taking and not giving back. How the FSF can honestly back the latter audience and demand from Trolltech to unnecessarily put its business at risk (consider the audience, the closeness of the system and the existing warez culture there) which might well fire back at the existing free Qt versions is beyond me."
    author: "Datschge"
  - subject: "Re: Non-issue"
    date: 2004-09-01
    body: "I fully agree with you here. Why support a platform that is non-free in nature? I would like to point to http://www.fefe.de/nowindows/ for an appeal NOT to port to windows, but instead use our energy to make really free platforms as good as we possibly can. Personally, I will never support porting the software I worked on to windows. I know the official standpoint differs here, stimulating the addition of the Qt exception to the licence. "
    author: "Andre Somers"
  - subject: "Re: Non-issue"
    date: 2004-09-01
    body: "I whole heartedly disagree. What point is there in not having a GPL'ed QT for Windows from a User's point of view other than that one probably doesn't like windows?\n\nIf QT would have been GPL an all available platforms some years ago many multiplatform projects would have considered it as a real possibility - think of OOo, Mozilla, Firefox, smaller software such as eMule/aMule and the like. Theres quite some good multiplatform commercial QT stuff like Opera, but I really think the non availability of a GPL version did stop QT adoption more than it did anything else.\n\nGiven that many companys/projects that see Windows as an important platform might want to code in C++ GTK could be 'dead' by now when it comes to such multiplatform project if Trolltech did the right thing before.\n\nMy 0.02\u0080"
    author: "Christoph Wiesen"
  - subject: "Re: Non-issue"
    date: 2004-09-01
    body: "this over-and-over again demand on a GPL'd windows version of qt is just ridiculous and hypocritical.\nproviding free software to those who have decided to use M$ is in fact nothing else than supporting Bill G. for free."
    author: "hoernerfranz"
  - subject: "Re: Non-issue"
    date: 2004-09-01
    body: "Is it really so far fetched that a toolkit available for all major platforms could lead to more applications using it for _all_ of the supported platforms?\n\nI don't think QT have to GPL QT at all cost or don't want somebody to force them or something, it is just my honest oppinion that something like that would give us more applications that are based on QT since many important projects decided and decide to be multi-platform.\n\nThe free software *is* and will be there for \"M$ and it's supporters\" - it's just that QT doesn't play a role in this game, and to me thats sad since GTK doesn't 'feel' any good to me when I'm forced to use it or something else like apps based on wx in Linux (something QT would have done better, but it just wasn't an option for the projects)."
    author: "Christoph Wiesen"
  - subject: "Re: Non-issue"
    date: 2004-09-01
    body: "OpenOffice and Mozilla are dual-license. So I doubt that a GPL Qt would be enough.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Non-issue"
    date: 2004-09-01
    body: ">more abused than the two other more free version, and there's even an effort to p>ort Qt/X11 to Windows (which the FSF could support instead) which is rather slow>since the Windows audience seems to be more about taking and not giving back. \n\nI think what needs to be taken into account here is the tendency of a particular culture to develop around different software.  Windows' culture is based on greed...it always has been.  The concept of free software is one which has an infinitely stronger basis in UNIX based environments, whereas Windows is traditionally a closed, proprietary system.\n\nWindows is in the process of dying, and my own perspective is that we should allow this to happen.  I feel that development work is far better spent on new projects for Linux rather than porting Linux-based software to an OS which is already on the endangered list.  If Windows users want to use KDE, let them download a Linux distribution and use it in its native environment.  They will benefit immeasurably in other ways from doing that anyway.  I believe we should be encouraging migration from Windows, not attempting to graft parts of Linux onto Windows' already broken architecture.  It's a sinking ship...let it go the rest of the way down."
    author: "Petrus"
  - subject: "Re: Non-issue"
    date: 2004-09-02
    body: ">    Windows' culture is based on greed...it always has been.\n\nPlease put down the broad brush, and slowly step away. It may have taken a while to catch on, but there is now a thriving open source community targeting the Windows platform. Trolltech may like to say otherwise*, but I think that's because they don't know how to search SourceForge**.\n\n>    I believe we should be encouraging migration from Windows, not attempting to graft parts of Linux onto Windows' already broken architecture.\n\nFirst of all, the effort to port Qt itself to Windows has already been made. It simply is not available under a license that allows it to be a first-class member of the open source community.\n\nIf you want to encourage migration from Windows to Linux, you need to make the operating system irrelevant. The best way to do that is to provide as many cross-platform open source applications as possible.\n\nThere's a major psychological barrier to switching operating systems: Losing functionality. That fear of a Linux version of an app either not existing or being unsuitable to task is a major concern. It will become *more* important as Linux works out its remaining usability issues. Progress is being made: OpenOffice.org is replacing MS Office, Mozilla apps are replacing Internet Explorer and Outlook Express, and KDE *could* replace Explorer. Someday, with enough cross-platform open source apps, users will realize that they can make the jump to Linux, and not lose any capabilities they had in Windows.\n\nMaybe it will be a slow migration, maybe it will be a mass exodus from the next Code Red. However it happens, sooner or later, Windows USERS will start heading down the road to Linux. And we, as developers/IT professionals/geeks, can either ease their journey, or put roadblocks in their path.\n\n*: http://dot.kde.org/1081772638/\n**: http://sourceforge.net/softwaremap/trove_list.php?form_cat=216&discrim=15"
    author: "Keith Russell"
  - subject: "Re: Non-issue"
    date: 2004-09-02
    body: "What is the goal of the free (as in freedom) software community? To grant more freedom, provided that the same freedoms are passed on upon distribution. Licenses are about freedoms, not price. Proprietary vendors have 90%+ of the marked. How are you going to convince the marked if you start saying \"freedom for some of us\"?\n\nGeneral software that just about everyone needs some of the time includes email, browser, file manager, word processor, calculator, calendar, archive manager and a game now and then. You should encourage people you meet to try free (as in freedom) software available for Windows. If they become regular users, it is easier for them to switch to a free OS where the regular tools they use are available.\n\nIn light of the above argument, do you agree that a GPLed QT for Windows would be a good idea? Then developers using QT could say to their customers: \"Incidentally, we provide the same products for GNU/HURD, GNU/Darwin and GNU/Linux. If you switch to one of them, your license management will be back on track (no unsolicited copying and law suites to worry about), at present you will save money when obtaining a license, and all your regular tools are available to you. We can even offer you a discount, because if you switch, then we can switch completely as well. At present, we too must renew our Microsoft product licenses every two years. To migrate is a win-win situation for both of us.\""
    author: "Unleash freedom"
  - subject: "Re: Non-issue"
    date: 2004-09-02
    body: "Seems lots of you ignore the fact that such a GPL port of Qt to Windows already exists although it is not yet finished.\n\nhttp://kde-cygwin.sourceforge.net/qt3-win32/\n\nPeschm\u00e4"
    author: "peschmae"
  - subject: "Reiter, nicht Breiter"
    date: 2004-09-01
    body: "FSFE is great and FSF may adopt a lot of FSFE's diplomacy, but: they did very very little about Software patents and DRM in particular. In fact it was Stallman, not Greve, who prayed against software patent legislation throughout Europe, in Vienna, in Prague. And I think in Austria he was invited by Austrians, not by the FSFE, with no involvement whatsoever. Further more the FSFE lacks democracy and transparency (yes, I know the funny lawsuit argument). An organisation with such a small member base represents no one. FSFE's second class members, called the \"volunteers\" even have to pay for their their contributive work such as trade fair representation, in Germany we call it the Fischerch\u00f6re-Gesch\u00e4ftsprinzip.\n\n\"In terms of Free Software organizations, apart from the FSFE in Europe and the FSF in North America there aren't any well established organizations.\"\n\nThis is in fact a lie. FSFE is no real \"organisation\". It does very little and makes a lot of noise about its own action. There are a lot of established organisations that are more democratic and more transparent than FSFE, with a real member base. FSFE holds the holy grail of the 4 freedoms, not more not less and helds a monopoly of \"Free software\" interpretation. \"organisation\" is not the problem when it comes to OSS-style action, it is an autoritarian model that does not apply to our work that is more efficient than \"organised action\". Note how of how little importance KDE e.V. as a formal organisation is for \"commanding\" the community :-) The very same model can be applied for lobbying and politics. FSFE has to open up and liberalise before they pray freedom to others.\n\nDon't forget, there is real work for you:\n\nhttp://europa.eu.int/information_society/eeurope/2005/all_about/digital_rights_man/text_en.htm\nhttp://europa.eu.int/comm/internal_market/copyright/review/consultation_en.htm\nhttp://www.bmj.de/enid/5fdbcdbabc4fc06e513fd3affae795f6,0/ku.html\n\nYes, FSFE is great but it needs reform. And don't say you care about issues when you play a minor role whatever the reasons. You get credit for your work, not for your talks about your plans."
    author: "Erik Janssen"
  - subject: "Re: Reiter, nicht Breiter"
    date: 2004-09-01
    body: "The FSFE has exactly one employee, Georg Greve, who works full-time for the FSFE. Everybody else works for the FSFE in his spare time or is allowed to do FSFE stuff during his working hours. How much paid employees doesn't the FSF have? I can't tell because I wasn't able to find this information on fsf.org or gnu.org. What did you say about transparency?\n\nAlso you might want to take a look at http://www.germany.fsfeurope.org/about/funds/2003.en.html (I wasn't able to find the corresponding income/expense sheet of the FSF; what did you say about transparency?). A yearly budget of ~50,000 EUR is near nothing. You can't expect them to make high jumps/wide leaps with so little money to spend. The FSFE indeed makes a lot of noise about its actions. It has to. Otherwise it would be completely ignored by the media, the politics and the big players.\n\nI guess you've already written several letters to your representatives (in your country's government and in the European government) telling them how you feel about software patents. If not, then don't blame it on the FSFE if software patents are legalized in Europe."
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Reiter, nicht Breiter"
    date: 2004-09-01
    body: "Transparency: Mailing list, decision process, election\n\n\"A yearly budget of ~50,000 EUR is near nothing. You can't expect them to make high jumps/wide leaps with so little money to spend. [..] I guess you've already written several letters to your representatives (in your country's government and in the European government) telling them how you feel about software patents. If not, then don't blame it on the FSFE if software patents are legalized in Europe.\"\n\nLetters don't count, you have to talk to them, personally. What persons feel is irrelevant, Decision makers have to be triggered with good prepared arguments and facts, what is needed is training for lobbying. FFII's approach is too broad as they try to get involved in everything. And Greve missed many chances to do something substancial on software patents and DRM. Instead he makes himself rare and arrives in time for the press photo. I don't see a substancial contribution of FSFE in the software patents or DRM debate or even a operational approach. What was the FSF directive proposal for Sept 23? What did FSF do against the IPR Enforcement directive?\n\n\"don't blame it on the FSFE if software patents are legalized in Europe\"\n\nIt is the lack of organisation of our interests in the political field. I am not responsible for the FSFE's failure in fundraising. Perhaps it only gets 50 000 Euro because of work which most people think is irrelevant for them. Who cares whether a UN-talkshop invites Greve when we face problems with DRM devices and patents. Perhaps it is the kind of hermetic organisation that does not let you particiapte on the efforts. Perhaps it is the result of the \"Greve takes it all\" principle."
    author: "Erik Janssen"
  - subject: "Re: Reiter, nicht Breiter"
    date: 2004-09-01
    body: "On the issue of software patents, you couldn't be more wrong. The FSFE, both in its own name and through helping its associated national members, has been doing *a lot* on the issue.\n\nGeorge Greve, Bernhard Reiter and others have made regular trips to Brussels and Strasbourg. They've held many meetings with high-ranking officials, and have even been in the group of German representatives to events like World Summit on Information Society.\n\nThey may not have saved free software - money is very much the issue here... with more they could hire people like Bernhard full time and massively improve their influence - but to paint the FSFE as a good-for-nothing waste of time, and as an unjustified media hog, is completely inaccurate."
    author: "Tom"
  - subject: "Re: Reiter, nicht Breiter"
    date: 2004-09-01
    body: "Eastern Europe is an example. The FSFE could do a lot in Eastern Europe for swpat lobbying. I just see what RMS does and what the FSFE does."
    author: "Erik Janssen"
  - subject: "Re: Reiter, nicht Breiter"
    date: 2004-09-01
    body: "Well again, how do you propose that a group of volunteers do more in Eastern Europe without additional funding? They don't even have the time to properly lobby in the Western European nations (though they do a very good job with the time they have) so how are they supposed to do lots in Eastern Europe as well?\n\nI assume you are referring to RMS' recent tour of Europe, which included some speeches in Eastern Europe. He has the time to do that because the FSF can cover the work needed in North America, and because it is but a few visits. I'm sure that if George Greve were invited there he would go; in fact I'm fairly sure he already has.\n\nReally, unless you personally have committed a substantial amount more of your time than the FSFE volunteers to Eastern Europe, or unless you have donated a substantial amount of money that you thought was misused, you can't go criticising the FSFE for inaction.\n\nRather than attacking them here, your time would be better spent helping them!"
    author: "Tom"
  - subject: "Re: Reiter, nicht Breiter"
    date: 2004-09-02
    body: "Hope somebody from fsfe will be in Markelo,NL at the Vrijschrift Patent meeting\n17-20 Sept, can RMs be brought to the event, this would be great\n\nhttp://wiki.vrijschrift.nl/MarkeloEvent0409"
    author: "Jan"
  - subject: "Re: Reiter, nicht Breiter"
    date: 2004-09-02
    body: "Did you get in touch with them to ask? Did you invite them?\n\nI think email might be a much more suitable medium than posting in fora. :)"
    author: "anocomm"
  - subject: "Re: Reiter, nicht Breiter"
    date: 2004-09-02
    body: "> Perhaps it is the result of the \"Greve takes it all\" principle.\n\nYou don't seem to know much about the principles of the FSFE.\n\nAnyone who cares to find out about its history will easily discover that FSFE started by Greve initiating it, taking a personal credit from the bank to start working full-time and afterwards working insane hours for dirt payment. Maybe \"Greve gives it all\" would be a more adequate description.\n\nBut in fact the FSFE is not revolving around Greve and if you cared to do something else than attacking those who do get active in unfair ways, you could get involved with them and find out for yourself."
    author: "anonymous"
  - subject: "Qt's success"
    date: 2004-09-01
    body: "I was under the impression that *Windows* development was Trolltech's bread-and-butter-- most of their paid, commercial licenses come from Windows developers: http://www.trolltech.com/success/index.html\n\nTo be honest, I think that Trolltech has been exceeding generous to open their toolkit up for free development on Linux and OS/X, and I'm not sure why people feel the need to criticize.  Trolltech actually pays some of its employees to work on KDE, so that commerical profit is literally feeding back into OSS.\n\nPerhaps the issue simply is that some people dislike all commercial software?"
    author: "jaykayess"
  - subject: "Re: Qt's success"
    date: 2004-09-01
    body: "What do you expect to happen if Trolltech releases Qt for Windows under the GPL? Will suddenly all licensees start using the GPL version? Do you really expect them all to make their software free in order to be able to use the GPL version? Or do you think they will secretly use the GPL version for their commercial products?\nOnly for inhouse products a GPL'd Qt would be a possible alternative to the commercial version, but for commercial products it's not.\n\nWhere would Trolltech be today if KDE wouldn't exist?\nOr the other way around: Where would KDE be today if it would run on Windows since several years? Where would people want to go today if KDE was available for Windows?"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Qt's success"
    date: 2004-09-01
    body: ">Only for inhouse products a GPL'd Qt would be a possible alternative to the commercial version, but for commercial products it's not.\n\nThat's still quite a risk given that something like 90% of all commercial development is said to be for inhouse use."
    author: "teatime"
  - subject: "Re: Qt's success"
    date: 2004-09-02
    body: "I don't know the exact statistics of Qt in house development, but I know they have had a great deal of success on Windows and that roughly 80% of software development is said to be in house. So this is a huge number no matter how you look at it. Personally I trust that the people at Trolltech are making decisions that are good for free software as well as their business, and I can see how both benefit from our relationship. If I thought it was advantageous to argue for free software on Windows I would, but I don't.\n\nThe FSFE statement seems ludicrous to me, because it could just as easily have been \"we have a problem with supporting free software on Windows because it props up an operating system that is hostile to free software in general and since we believe that free software is the future we want to encourage people to move to free platforms instead of supporting companies that would like to find a way to destroy freedoms.\" That would be a much better statement.\n\nIn my experience the Windows world is much more concerned with free as in beer than free as in freedom, and this argument really comes down to that. Who is going to tell me that having access to source code is of any importance to Windows users? I don't see where free Qt on Windows doesn't do more harm than good in a number of ways."
    author: "Eric Laffoon"
  - subject: "Re: Qt's success"
    date: 2004-09-02
    body: "> Who is going to tell me that having access to source code is of any importance to Windows users?\n\nSo you are trying to tell us that we shouldn't even try to change the attitude of the Windows users?\n\nI find your generalization very inappropriate. Get real. The more users Linux gains the less important will the availibility of the source code be to the majority of Linux users. Why? Simply because the majority of computer users doesn't care for having access to the source code regardless of the operation system they are using."
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Qt's success"
    date: 2004-09-02
    body: "As I've stated on the kdepim list I would like to see KDE running on Windows. Given that Qt is a cross platform toolkit it does make a lot of sense from a technical standpoint.\n\nUnfortunately GPLing Qt on Windows would have a significant unfavourable financial repercussion on TT. This is not mere speculation but based on the post GPLing X11/OSX sales data.\n\nBut I'm not too worried. Given the progress of the cygwin and related projects I think it's only a matter of time before a native MSVC/MinGW KDE port is available on Windows.\n\nJust speaking for myself based on knowledge I believe to be non-confidential."
    author: "Don"
  - subject: "Berhard Reiter"
    date: 2004-09-01
    body: "Bernhard, what is your Position about the marxist \u00d6konux-Projekt?\n\nIn a Frankfurter Rundschau article they presented their view.\nhttp://www.fr-aktuell.de/uebersicht/alle_dossiers/politik_inland/wie_viel_staat_braucht_der_mensch/der_sozialstaat/die_alternative/die_alternative_themenausgabe/?cnt=352260\n\nhttp://www.fr-aktuell.de/uebersicht/alle_dossiers/politik_inland/wie_viel_staat_braucht_der_mensch/der_sozialstaat/die_alternative/die_alternative_themenausgabe/?cnt=352259\n\nYou once joined their conference. Is it dangerous to deal with communists?"
    author: "martin"
  - subject: "Re: Berhard Reiter"
    date: 2004-09-01
    body: "You might want to direct this question at Bernhard, since there's no guarantee that he'll read your comment. Try chancellor_AT_germany.fsfeurope.org and maybe post his reply here if you ask for permission and he agrees."
    author: "Tom"
---
In his <a href="http://conference2004.kde.org/cfp-userconf/bernhard.reiter-social.political.aspects.free.software.php">speech</a> at <a href="http://conference2004.kde.org/">aKademy</a>, Bernhard Reiter of the <a href="http://www.fsfe.org/">Free Software Foundation Europe</a> (FSFE) both celebrated Software Freedom Day and reminded the KDE community of what freedom in software means. The FSFE was founded in 2001 to promote and defend Free Software, and to coordinate national Free Software organizations, throughout Europe. Bernhard is the Chancellor of the German Chapter and has been involved since the FSFE's inception. Interested in what he had to say to the KDE community, I caught up with him after his talk to ask him a few questions. Read on for his responses.



<!--break-->
<p><b>You mentioned in your presentation the threats of software patents and DRM 
(Damn Restriction Management, as you described it). What other areas do you 
think the Free Software movement will need to address in the next few 
years?</b>
</p>

<p>
Well to begin with, the issues of software patents and DRM will be huge and so 
will be keeping us all busy for many years yet. Other issues do exist but 
they are generally ongoing as opposed to short-term projects. We expect that 
there will be more legislative and political attacks on Free Software, but we cannot 
predict what they will be.</p>

<p>
The most important point I want to emphasize is that we must continue to
educate people, and especially politicians, about Free Software. We must 
convey what it means to the whole world, in terms of who controls technology 
and how it affects our lives. If we can do that, we won't have to spend so 
much time defending against attacks because more people will help us.</p>

<p><br>
<b>In Europe in particular, Governments are now driving a lot of the Free 
Software adoption. What part can projects like KDE and organizations like the 
FSFE play in this process?</b></p>

<p>
Not everybody takes a huge interest in politics and I believe that it is normal that some people stay of the technical side of things and just develop software. On the other hand we need more people to engage themself politically for Free Software. Others should at least be responsible and keep their eyes open for events and developments where they can do their share. So they should, for example, be aware of demonstrations and lobbying efforts on issues like software patents.</p>

<p>
In terms of development, members of the Free Software community should also 
bear in mind that code can shape laws. If the code doesn't allow a person to 
perform a particular action, then the code is controlling that person. In 
projects like KDE, which are reaching a fairly mature stage, developers 
should be focussing on the users and trying to make Free Software more usable 
for them. Even development can be political issue.</p>

<p><br>
<b>An important aspect of freedom in software is the ability to use it. Yet 
although KDE is currently translated into over 80 languages, there are 
hundreds more that are commonly used and that aren't yet covered. How can a 
community of volunteers, both in KDE and the FSFE, help this?</b>
</p>

<p>
KDE developers already do help, in that they are part of a process of gradual 
improvements in internationalization and localization. In terms of Free 
Software organizations, apart from the FSFE in Europe and the FSF in North 
America there aren't any well established organizations. We are hoping to 
change that, but we don't want to take money and help from Governments and 
companies, even if it would help with internationalization, if that were to 
harm our independence and our ability to promote Free Software. We need to be 
careful about where we use our money, and what the specific focus of our 
investments are.
</p>

<p>
In general the direction that Free Software takes ought to be driven by competition and user demand, not interference from government.</p>

<p><br>
<b>But do you not think that there is a role for government in areas where the 
market cannot yet supply full internationalization, especially where a large 
proportion of the population cannot speak English?</b></p>

<p>
Well yes, of course government should be interested in preserving the cultural 
and linguistic heritage of their country. Where the market cannot provide, it 
is their duty to do so.</p>


<p><br>
<b>Moving back to software, are there any areas in particular on the Free 
Software desktop that you think developers need to address most urgently?</b>
</p>

<p>
It is hard to say what is missing before we try deploying our software in the 
target markets, and so we will know a lot more when GNU/Linux is deployed in more corporations. I am worried that many desktops are adding proprietary 
software to their systems where Free Software isn't yet available or good 
enough, usually because it is more convenient. Proprietary PDF readers and 
the MP3 format are an example where Free alternatives exist (KGhostView and 
Ogg Vorbis, for example). Any many proprietary tools are being used to 
develop Free desktops; proprietary vector editing applications, for example, are used by many of the icon artists. We ought to be working to stop this being necessary or even desirable.</p>

<p>
Personally I would like to see more attention to businesses, with a good 
business model for private desktop users. The FSFE has an 
initiative called the <a href="http://www.fsfeurope.org/projects/gbn/">GNU 
Business Network</a> which could help in this area if we received more 
funding. Society needs businesses, but it shouldn't be driven by business 
interests. Rather, society should be laying down the ethics by which 
businesses operate. Free Software desktops can be part of the development of 
such an ethical business model for Free Software.</p>

<p><br>
<b>Should projects like KDE be more proactive in promoting Free Software then, including more protective licenses like the GNU GPL?</b>
</p>

<p>
It is a matter of strategy. Yes, the KDE Project should promote freedom both 
by choosing free licenses (e.g. the GNU GPL) and by talking to partners and 
the public about it, but there are also times when it is more strategic to 
use a license with less protection (using the GNU LGPL, for example, to spread 
adoption).</p>

<p>
Qt on Windows remains a problem for us, since it is not available under a Free 
license. KDE goes to some lengths to allow proprietary KDE software to 
be developed by releasing its libraries under the LGPL. This may be a good 
strategy - I don't want to condemn it in general - but it poses practical problems for KDE development as they cannot accept GNU GPL code for the core libraries and the modules that require a Qt licensing exception when using GNU GPL. Which code goes into which module within KDE is governed by this requirement that only benefits proprietary KDE applications.</p>

<p>
The KDE Project ought to recognize how much it has given Trolltech, whose 
success is based to a large extent on the success of KDE. Maybe the KDE 
Project could encourage Trolltech to GPL the Windows version of its toolkit? 
The current arrangement is not unreasonable, but the KDE Project should be 
aware that it can talk to Trolltech on an equal level.</p>

<p><br>
<b>Bernhard, thank you for this conversation.</b>
</p>

<p><br>
It's no problem, it is always good to educate more users about Freedom. I 
would like to note that it is a good sign that the FSFE were invited to 
aKademy, and that the KDE Project and the FSFE have a good relationship. We 
welcome all future cooperation.
</p>



