---
title: "KDE CVS-Digest for October 15, 2004"
date:    2004-10-16
authors:
  - "dkite"
slug:    kde-cvs-digest-october-15-2004
comments:
  - subject: "OT: KDE PIM API"
    date: 2004-10-16
    body: "Although I can't find it anymore, I remember an article on the dot about KDE creating a competitor to WinFS/Gnome Storage. My question is, what about PIM data? Will it be possible to search for eMails, contacts, ... as well? If so, how is it done?\n\nI have not done much research yet, however my question is: Is it possible to access KDE PIM data from outside the applications? E.g., is there an API for accessing the data? I recall there's an API to access the addressbook, but I've never heard about an equal API for eMails.\n\nThanks to Derek for CVS digest.\n"
    author: "MM"
  - subject: "OT too: Secure File Shredder for Konq"
    date: 2004-10-16
    body: "If you are interested, vote for this wish!\n\nhttp://bugs.kde.org/show_bug.cgi?id=91474\n\nI think it would be a nice security feature if Konqueror had a secure file shredder. \n \nAs I see it, you could use it on a file by file basis by right-clicking and selecting the option (shred file), or you could set some kind of global setting that would shred all the files that you delete (with a setting for the number of passes that you want). \n \nI figure some people want added security and want to make sure that nobody can undelete their files, and this would be very helpful for them. "
    author: "Mikhail Capone"
  - subject: "Re: OT too: Secure File Shredder for Konq"
    date: 2004-10-16
    body: "Voting for a rejected and closed request is senseless."
    author: "Anonymous"
  - subject: "Re: OT too: Secure File Shredder for Konq"
    date: 2004-10-16
    body: "Well, I didn't know that it had been tried in the past."
    author: "Mikhail Capone"
  - subject: "Re: OT too: Secure File Shredder for Konq"
    date: 2005-11-23
    body: "O do follow that wonderful mans request and vote against file shredders in linux.  I so do like folks that can make a girl a some money and buy me diamonds.  Diamons are a girls best friend.  \n  I went to this auction where a computer service company went out of business.  There was a whole bunch of stuff there.  Computer companies are always going out in this area;  seems they got too numerous to make a good livin I suppose.  Anyway there was this big box of old hard drives mixed in with old keyboards and some old modems and other items.  Weighed a ton.  I bought it literally for a song....well the next bidder in line gave into my bid when I told him I would 'personally' make it worth his while to quit bettin his bosses money while he was ahead.  Anyway after ten minutes of 'hard work' I had this box to take home and the slob even helped me load it into my SUV.\n  Back home, I went over the old hard drives and found some were not so old.  In fact, one local company had all its corporate data on one of them.  You know, its customer lists, its inventory and cost figures, its suppliers, and even a lot of cad drawing files.  It was one of those three hundred gigabyte models from LaCie that evidently had been used as a mirrored backup on a RAID system.  The company had just evidently pulled the drive out of the system during one of those 'upgrades' that major software companies periodically force on its corporate customers in the interest of its 'security' hehehehe.  Whatever!  Hell I don't know.  But here was this drive with all of its data in plain view.  Well almost.  I took the time to run one of those file undeleter utes on it and guess what?  All kind of more files showed up.  The company actually DID delete some stuff.  This was mostly dry stuff.  Employees get fired all the time, and the old pink slip data collects...why they were fired....suspicions of theft that the company was embarrased about.  Other stuff was interesting, like grafik copies of cancelled checks to some local politicians for 'misc services'.  Still other was of porno kept by some mid level execs.  Old directories belonging to one user had his family, kids wife dogs and all, in some subdirectories and in other directories were naked pix of his secretary and some unlucky office staff.  The shocker was seeing my sister naked in one of them.  Even I had never seen my prissy sister naked! All of this on some old company computer.  \n  Well this girl soon realized that this was a GOLD mine and I have always been\nknown as a 'gold digger'.  Hey, I LIKE drivin my Lexus with the gold fittings\nand bumpers.  I just checked around and found out who the competitors of this company were.  You see, if you offer to not publicize some information in return for some money from some company or whatever, the fuzz buttcan ya for blackmail or extorshun or something.  But if you just go ahead and sell the stuff to the competition, well then thats a hoss of a diffrent color.  And I now owned that info.  Hell! I worked my ass off for it!  And it was on a bunch of junk at an auction.  I owned that stuff and I done sold it for all I could get.  And I\ngot a lot.  I paid my mortgage off.  I bought some nice jewelry...did I say that \ndiamonds are a girl's best friend....\n   No you just go forth and multiply, cuz one day I'll find another box of coconuts...I mean hard drives in another auction.  I go to more auctions now.\nGot my car fixed.  One of these auctions will have some linux hard drives.  \n\nMaybe yours.  \n\nHey, the businesses who formerly owned the drives that I bought\nhad no idea that when they sent their stuff to be fixed that their company would be fixed instead.  I LIKE it that there are no shredders for linux like there are for windows.  I also like it that some guys have the idea that they can't get rid of the stuff on it so the dumbassed men just give up.  That just makes my work easier.  I also like it when some talk about their 'encryption programs'.  Seen those programs and most are a chore to learn and a bigger pain in the butt to use....meanin they won't get used often....or enough to stop me.  Besides most of the 'cryptors' are dumb guys.  I've dated a lot of them.  They like to brag about how smart they are. Some have even told me, in verrry unguarded moments, their company passwords\nand shit.  Not that I'd ever get to use 'em.  I did'nt work for their companies.  \nMake better money on my own.  Computer consulting for businesses makes good money!  Maybe you'll see me or one of my new girls in or around your company sometime just a chekin your system over!"
    author: "Oksana Datkova"
  - subject: "Re: OT too: Secure File Shredder for Konq"
    date: 2004-10-16
    body: "kgpg provides a file shredder"
    author: "AC"
  - subject: "Re: OT too: Secure File Shredder for Konq"
    date: 2004-10-16
    body: "AFAIK the problem with file shredding is that it does not really increase data security much, as applications create temporary files which are 'deleted' and not 'shreddered' later. Another problem is the swap partition. Sensitive data may be paged out while in RAM. Same for Notebook 'hibernation' partitions.\n\nThis problem could be adressed by the Linux kernel and libraries by encrypting the swap/hibernation partitions, and changing the unlink function in a way that deleting a file results in not only removing the file reference but overwriting the content as well. However, this would work only on 'patched' systems. And second, while encryption should not have much impact on performance, ALWAYS shredding instead of deleting files may decrease system performance significantly.\n"
    author: "MM"
  - subject: "Re: OT too: Secure File Shredder for Konq"
    date: 2006-08-07
    body: "With all due respect.  The above contains half truths to forward a KDE policy to disallow shredding, possibly to comply with some secret request from some security agency that wants unrestricted access to everybody's business records, etc.  Computers can be operated without a disk cache or paging file.  There is no such thing as an unbreakable code, and powerful agencies public and private have very good codebreakers.  It has become hard to find linux shredding programs.  Some wonder if microsoft has also a hand in this.  However, that said, a very good Konqueror implementation of the shredder does exist.  Just keep around an old linux with kernel 2.4 and KDE 9.0.  THAT wipes out files very handily.  All the new crap about 'insecurity of mangetic media' that gets handed out by pseudointellectuals is just that, worthless crap and halftruths.  Also if caches are an issue still, there is always Warty WArthog UBUNTU.  That is kernel2.4 built on ext2 FS which has none of the claimed insecurities.  UBUNTU does not kernel panic after power failures like SCO and other ext2 distros do under ext2.  UBUNTU just 'keeps coming back!"
    author: "Pteradactyl"
  - subject: "Database filesystem"
    date: 2004-10-17
    body: "I can't say I remember reading of a WinFS/Gnome Storage competitor on the dot, but I saw a small note in the latest linuxformat. A fellow named Onne Gorter has created a database filesystem for KDE, DBFS as a part of his master thesis. Don't know if this has been on the dot befor but heres the link\nhttp://ozy.student.utwente.nl/projects/dbfs/\n"
    author: "Morty"
  - subject: "Re: Database filesystem"
    date: 2004-10-17
    body: "I've read about DBFS earlier. However, it's not the article I am talking about. Maybe I am totally wrong, but I really beliefe there WAS an article about an 'official' KDE competitor to WinFS and Gnome Storage; I think development started some month ago at some KDE or Open Source/Free Software conference (can't remember the name for now). \n\nBTW, it's a pitty that the author of DBFS seems to prefer Gnome over KDE now. I really liked some of his ideas! But as far as I understand the concept, DBFS is somewhat bound to filesystems. So, for eMails being accessible using DBFS, they must be accessible via a filesystem, or a KIO slave (however, I don't now whether DBFS works with non-local files or another KIO slave as file:/).\n"
    author: "MM"
  - subject: "Re: Database filesystem"
    date: 2004-10-17
    body: "I got the impression from his website that the KDE implementation was in a working state, and he probably don't mind someone continuing his work on it for KDE while he consentrate on the Gnome part. Why don't you download and give it a testrun and post your impressions here(Sounds like you have done some thinking on the subject). Perhaps someone gets interested and want to continue development :-) If the code and ideas are good, there are no need for others to re-implement them. "
    author: "Morty"
  - subject: "Re: Database filesystem"
    date: 2004-10-17
    body: "I've downloaded it now, but as I am using SUSE and compiling KDE on SUSE has ever been a nightmare, I must try this anytime later.\n\nBTW, I finally found the dot article at\n\nhttp://dot.kde.org/1093616333/\n"
    author: "MM"
  - subject: "Re: Database filesystem"
    date: 2004-10-20
    body: "Yes, that's based on some of my work and that article emerged based on my talk a aKademy.\n\nThere's still work ongoing on this, but the targeted inclusion date isn't until KDE 4.  The system is in many ways fundamentally different from things like Spotlight, WinFS, DBFS, Beagle, Google for the Desktop, Storage, etc. but will make it possible to search the same types of data that they search.\n\nAnd yes -- email indexing will be a part of things.  It's actually not that hard to do based on the way that the indexer is designed to work to reuse KDE components.\n\nAt some point once it's there's a bit more work on it done I'm sure that I'll do a bit to create some publicity for it."
    author: "Scott Wheeler"
  - subject: "Re: OT: KDE PIM API"
    date: 2004-10-17
    body: "There is something like this in Gnome: Beagle (http://www.gnome.org/projects/beagle/)\nIt's an indexing and search tool written in C#. Currently it depends on Mono and has a GTK frontend, I think with the new C# bindings it could also be used in KDE."
    author: "Sven Langkamp"
  - subject: "Re: OT: KDE PIM API"
    date: 2004-10-17
    body: "Thanks for your hint, first time I heard about Beagle."
    author: "MM"
  - subject: "more and more reported bugs :-("
    date: 2004-10-16
    body: "I am always impressed so much job is done in KDE.\nKDE gets more and more functions, more and more polish...\n\nThe sad side is the growing number of declared bugs : nearly 8,000 now.\n\nHope some day, the so useful Derek's digest will show a more reasonable number of bugs."
    author: "Gerard"
  - subject: "stop obsessing over bug numbers"
    date: 2004-10-16
    body: "Why don't people understand that the number of bugs in KDE's bugzilla is not closely related to the actual number of bugs in KDE?  There are tons of bad reports; duplicates, just plain mistakes, and bugs that were fixed months or years ago but didn't get marked fixed in Bugzilla.  Secondly, any piece of software as large as KDE is going to have lots of bugs, no matter what.  Windows, in case you hadn't noticed, has many bugs.  So does OS X.  Really, the only conclusion you can draw from the 8000 bug reports is that KDE is used a lot, and thousands of people care that it is made better!  Lots of bugs in Bugzilla is a sign of KDE's SUCCESS, not its failure!"
    author: "Spy Hunter"
  - subject: "Stop ignoring bugs..."
    date: 2004-10-16
    body: "Well, unfortunately, bugs are sometimes completely ignored. For me, KDEPIM 3.3.1 on KDELIBS 3.2 is still very unstable, and I have reported several crashes. There have been no comments on these bugs for several weeks now, and they are still not solved :-(\n\nEven if there are a lot of \"bad\" bugs, at least the maintainers should immediately react and close them if necessary."
    author: "Frederik"
  - subject: "Re: Stop ignoring bugs..."
    date: 2004-10-16
    body: "It's difficult for maintainers to immediately close bad bugs. They do a phenomenal job as it is. I am quite impressed with how far KDE has come since I first started using it. A number of times in the past I have switched from KDE to GNOME (and once, blackbox). KDE is so far ahead of the competitors now that I feel confident that I'll not switch again. And the community that has grown up around KDE is so supportive that I wouldn't want to anyway.\n\nregards,\nDaniel"
    author: "Daniel Roe"
  - subject: "Re: Stop ignoring bugs..."
    date: 2004-10-16
    body: ">at least the maintainers should immediately react and close them if necessary.\n\nThanks for volunteering. You will find the time spent frustrating and boring with very little reward except some number not increasing. Much of it would be administrative busy work.\n\nThe bug database should be telling the developers what 'they' need. Like most administrative systems it has almost become demanding.\n\nThere are real issues here with no easy answer. But demanding that developers 'should' give more time, work longer hours, take more time away from their families so I can have something better for free really doesn't cut it. \n\nDerek (who thinks it may be time to dust off his entitlement rant)"
    author: "Derek Kite"
  - subject: "Re: Stop ignoring bugs..."
    date: 2004-10-16
    body: "From my experience, KDE has never supported mixing different versions of their packages. Recently, I upgraded from 3.3.0 to 3.3.1 and mid-upgrade (post-libs; pre-base), KHTML was broken. I wouldn't *expect* kdepim 3.3.1 to work on kdelibs 3.2.\nUnfortunately, KDE 3.3.1 introduced a number of new bugs significantly more annoying than those it fixed. Luckily, recompiling kdebase 3.3.0 seems to work somewhat with the rest of KDE 3.3.1 as a somewhat temporary workaround to the most annoying bugs."
    author: "Luke-Jr"
  - subject: "Re: Stop ignoring bugs..."
    date: 2004-10-16
    body: "> KDE 3.3.1 introduced a number of new bugs significantly more annoying than those it fixed.\n\nExamples?"
    author: "Anonymous"
  - subject: "Re: Stop ignoring bugs..."
    date: 2004-10-16
    body: "http://bugs.kde.org/show_bug.cgi?id=91278"
    author: "cbcbcb"
  - subject: "Re: Stop ignoring bugs..."
    date: 2004-10-17
    body: "I don't consider that a bug. You are opening a new Window...of course it should respect the size YOU set."
    author: "Joe"
  - subject: "Re: Stop ignoring bugs..."
    date: 2004-10-17
    body: "\nDid you read the report? This is a clear bug and a regression.\n\n1. It is opening a new tab within an existing window - this should not have a side effect of resizing the existing window. (If you don't believe me try adding those lines to your konquerorrc and see if you like it)\n\n2. I did not set any the fixed size.\n\nI don't know whether the bug is the fact that setting got into my configuration file by mistake, or that the setting causes the window-resizing effect. That is a decision to be made by the maintainers who know the KDE architecture.\n\nIf weird settings are being added to KDE configuration files this is a very serious bug. "
    author: "cbcbcb"
  - subject: "Examples?"
    date: 2004-10-16
    body: "kdevelop crashes on closing with kde 3.3.0 (I guess due to kmdi changes)"
    author: "Carlo"
  - subject: "Re: Examples?"
    date: 2004-10-17
    body: "Bugreport? Backtrace? Guesses are entertaining but not much else.."
    author: "teatime"
  - subject: "Re: Examples?"
    date: 2004-10-18
    body: "no\u00b2(t yet), I wanted to test on 3.3.1 first"
    author: "Carlo"
  - subject: "Re: Stop ignoring bugs..."
    date: 2004-10-16
    body: "kdepim 3.3.1 works with kdelibs 3.2. But of course you have to compile kdepim 3.3.1 against kdelibs 3.2. You can't simply install a precompiled binary of kdepim 3.3.1 because this binary has most likely been linked against kdelibs 3.3.1 and thus won't work with kdelibs 3.2.\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Stop ignoring bugs..."
    date: 2004-10-16
    body: "Which bug reports are you referring to?\n"
    author: "Cornelius Schumacher"
  - subject: "Some kontact bugs"
    date: 2004-10-18
    body: "http://bugs.kde.org/show_bug.cgi?id=90000\n\nhttp://bugs.kde.org/show_bug.cgi?id=89549\n(Especially this one is extremely annoying, as I see it more than half of the time when switching IMAP folders)\n\nhttp://bugs.kde.org/show_bug.cgi?id=89419\n\nMy excuses if you think I was ranting and starting a flamewar, that was certainly not my intention. Especially the topic of my post was a bit over the top..."
    author: "Frederik"
  - subject: "Re: Stop ignoring bugs..."
    date: 2004-10-18
    body: "It is often not easy to close such bugs immediately.\n\nFor examples:\n- the bug may be already reported but from another point of view (so you need time to find that these bugs are indeed the same).\n- if a bug is reported again, you need to find out if it is because the reporter has an old KDE or if his particular system still triggers the bug.\n- if the bug seems not to make sence to the developer, the developer has mostly to ask back the reporter.\n- the developer simply has not any time to look at the bug reports (or perhaps he takes the time to read them but has not the time to do anything useful with them, like testing.)\n\nAlso I know of many bugs in KOffice that remain open, because the reporter does not answer to the questions of the developers. As in KOffice, such bug reports are typically kept open for a year for somebody else to help (or for the reporter to finally react), that makes also a few bugs open.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: stop obsessing over bug numbers"
    date: 2004-10-16
    body: "I agree with you to a degree.  But Gerard is also correct.\n\nI see a number of problems:\n\n1. The growing bug list makes using bugzilla more difficult for the developers. The real bugs get obscured by the number of old, false and duplicate bug reports.  So a well maintained bug database is important.\n\n2. There is, in fact, a tendency to ignore bugs for new features. My pet peeve is letting a wonderful, useful program stay in a state with lots of small bugs that makes it painful to use.  I think there is a tendency for this in, for example, KOffice and kdegames, the domains where I do most of my work.\n\nOk, I understand that a lack of developers makes it difficult to do it all, but that doesn't detract from the fact that new features are often higher prioritized than bug fixes.\n\nSince I like to put my money (or in this case time) where my mouth is, I have started a private crusade against all small, unnecessary bugs in KDE.  I think you can see that in this weeks CVS digest. And it's great for the ego to be able to close lots of bugs and then get high in the list of bug resolvers. :-)\n\nOf course, once in a while a bigger feature gets implemented as well, but that is purely coincidental. :-)"
    author: "Inge Wallin"
  - subject: "Re: stop obsessing over bug numbers"
    date: 2004-10-16
    body: "Especially KWord is an example for this: A great app, but the rendering and table bugs make it unusable for me."
    author: "Anonymous"
  - subject: "Re: stop obsessing over bug numbers"
    date: 2004-10-18
    body: "The problem is that fixing the table code is not a small thing. (\"Re-implementing\" would be more the right word.)\n\nSo somewhere priorities have to be made. For KOffice, currently it is clearly adding OASIS-support, even if there it does not go forward as fast as it would be preferable. :-(\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: stop obsessing over bug numbers"
    date: 2004-10-17
    body: "> Ok, I understand that a lack of developers makes it difficult to do it all, \n> but that doesn't detract from the fact that new features are often higher \n> prioritized than bug fixes.\n \nPretty natural, when you doing stuff for fun in your free time, right?"
    author: "anonymous"
  - subject: "Re: stop obsessing over bug numbers"
    date: 2004-10-17
    body: "No, I don't think so. \n\nI do this stuff in my free time as well, and I hate bugs.  Besides, many of the bugs that I have fixed so far have been real easy.  I don't see why a program should be made difficult to use just because there are easily fixed bugs.\n\nIn fact, I think that I will start a movement against Small Unnecessary Bugs, SUBs.  These things make programs SUB-standard.  Why not let one day per month be a anti-SUB day?  The more I think about it, the better it sounds. I think we should have a competition to squash as many bugs as possible in one day."
    author: "Inge Wallin"
  - subject: "Re: stop obsessing over bug numbers"
    date: 2004-10-18
    body: "I do not really understand point 2 for KOffice. \n\nSure many bugs that sounds small remain open (if they are really small is another problem) but many wishes remain open too.\n\n(If you take the example of KWord, the first wishes are older than the first bugs.)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: stop obsessing over bug numbers"
    date: 2004-10-19
    body: "Ok, I'll give you an example:\n\nhttp://bugs.kde.org/show_bug.cgi?id=87700"
    author: "Inge Wallin"
  - subject: "Re: stop obsessing over bug numbers"
    date: 2004-10-17
    body: "I don't obsess. I think the 8,000 are the reflect of my 20 declared bugs, of which 2 should have a very high priority (have you ever tried to send a fax from Kooka or to print KSpread sheets ?).\nMost of my declared bugs are documentation related, and some of them can be fixed by simply deleting a  paragraph.\nThe question you could have asked is \"why don't you fix them yourself ?\"\nFor many reason\n-I am very poor at programming and I don't know how the system is internally designed\n-I have no CVS access (and I don't ask for one because of my poor knowledge of the system)\n-In the case of trivial bugs, the hard job is to find them, not to fix them\n\nI have no critics against the core KDE developers, except perhaps about the order of the priorities : I think making things work properly should be a priority, before adding new features. When we are in feature freeze period, the number of closed bugs reaches 500 a week, and it goes down to about 200 in development periods, so the KDE team has the power to fix more bugs than users find (about 300 a week)\n\nIf I would have some critics, it would be against corporate users, who could pay some devs to polish KDE. It would remain much less expensive for them than paying for hundreds of licenses of proprietary software.\n\nPerhaps the system of bug reports should allow to suggest priorities ? That way, we could really know how many important bugs remain ?\n\nThanks for reading"
    author: "gerard"
  - subject: "Re: stop obsessing over bug numbers"
    date: 2004-10-17
    body: "And one more thing people don't understand or bother to relate having to do with the number of bugs, are KDEs increase in pure size. The constant addition of features and new applications witch Bugzilla contains bugreports for (eg. the extragears). To get the real picture you have to compare the number of bugs against lines of code, and this will give some indication of the quality of the code. \n\nI guess the result may even say, an decrease in bugs over the last 2yrs. And it is a nice exercise in statistics for the next person wanting to complain about rise in number of bugs, prove me wrong before you start complaining.  Show that you know what you are talking about, not just whining over a worthless number."
    author: "Morty"
  - subject: "Re: more and more reported bugs :-("
    date: 2004-10-17
    body: "http://www.icefox.net/kde/tests/report.html\nIs probably the most useful link"
    author: "gerd"
  - subject: "Re: more and more reported bugs :-("
    date: 2004-10-18
    body: "Simple question: KDE 3.3 has more open bugs that (for example) KDE 2.2 does. But is 3.3 worse than 2.2? is it less stable? Does it have less functionality? No? Then what are you complaining about? Because some number in some website is now bigger than it was before? Just because that number is larger does not mean that quality of KDE is going down. Did you know that Mozilla (a single app!) has more open bugs than entire KDE does? And (AFAIK) GNOME has more open bugs than KDE does as well.\n\nI for one am happy that there are more reported bugs. Seriously, how are the developers supposed to find and fix those bugs if they are not reported?\n\nAnd, consider this: If I find a bug in KDE and report it, does that somehow magically make KDE buggier? Would it be better if I didn't report it? The bug would still be there, but since it wasn't reported, the number of bugs in bugzilla would be less. Would KDE then be magically \"less buggy\"?"
    author: "Janne"
  - subject: "Pixel Plus"
    date: 2004-10-16
    body: "Where!?"
    author: "fdsa"
  - subject: "Re: Pixel Plus"
    date: 2004-10-16
    body: "kdenonbeta/pixieplus"
    author: "Anonymous"
  - subject: "Sound servers are stupid"
    date: 2004-10-16
    body: "We don't need a sound server, what we need is for the Linux kernel to do its job as an operating system and virtualize the sound hardware so we don't have to care how many applications try to use it at once.  It is ridiculous that such a modern operating system has such a backwards sound system that it can't even be used by two programs simultaneously.  Imagine if your hard drive could only be used by one program at a time!"
    author: "Spy Hunter"
  - subject: "Re: Sound servers are stupid"
    date: 2004-10-16
    body: "If I am not mistaken, this is already true for Linux (ALSA + dmix plugin)\n\nBut you have to remember that KDE is not restricted to Linux, other operating systems might still have those limitations, also for example older Linux systems or sound devices without ALSA drivers.\n"
    author: "Kevin Krammer"
  - subject: "Re: Sound servers are stupid"
    date: 2004-10-16
    body: "Except that dmix needs to be configured and setup properly. There's no reason I can think of why ALSA can't do this internally automatically.\nCouldn't Arts be modified to simply act as an ALSA wrapper on Linux and a sound server on other platforms?\nAnother issue that really should be addressed is virtual terminals and transparent network sound... If I'm on vt11, I don't want to hear sounds from vt07 (well, I usually do, but that should be via Krdc or such)\nLikewise, if I logon to my computer remotely (via Krdc), I'd want sound to go to the remote system, not play locally. If the display is active both locally and remotely (currently required for remote usage, unfortunately), I'd want it to play in both places."
    author: "Luke-Jr"
  - subject: "Re: Sound servers are stupid"
    date: 2004-10-16
    body: "This idea has been discussed at length by the ALSA developers, who feel that mixing is not ALSA's job."
    author: "Robert"
  - subject: "Re: Sound servers are stupid"
    date: 2004-10-17
    body: "I tried to set up dmix myself, since no distributions seem to be interested in including it by default.  It didn't work with any program I tried.  Perhaps it has improved since then but as long as it is not the default, the problem will remain."
    author: "Spy Hunter"
  - subject: "mosfet"
    date: 2004-10-16
    body: "Apropos Pixie Plus, what happened with mosfet?"
    author: "MaX"
  - subject: "Re: mosfet"
    date: 2004-10-17
    body: "not seen for a long time. quit because of ??? some fights ??? dunno. anyway, I guess mosfet wont return."
    author: "superstoned"
---
This <a href="http://cvs-digest.org/index.php?issue=oct152004">week's KDE CVS-Digest</a> (<a href="http://cvs-digest.org/index.php?newissue=oct152004">experimental layout</a>): 
Pixie Plus returns with new maintainer.
<a href="http://www.koffice.org/krita/">Krita</a> now shears and rotates images.
<a href="http://www.koffice.org/kpresenter/">KPresenter</a> adds master page support.
<a href="http://amarok.kde.org/">amaroK</a> now supports <a href="http://www.networkmultimedia.org/">NMM</a>.
Plus coverage of the <a href="http://gstreamer.freedesktop.org/">GStreamer</a> presentation from the aKademy conference.

<!--break-->
