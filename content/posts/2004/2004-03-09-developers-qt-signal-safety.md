---
title: "Developers: Qt Signal-Safety"
date:    2004-03-09
authors:
  - "jkarneges"
slug:    developers-qt-signal-safety
comments:
  - subject: "Good stuff"
    date: 2004-03-09
    body: "Very good article. One remark though, I think that this line:\n<code>#define SAFESIGNAL(a) { QGuardedPtr<QObject> self = this; a; if(!self) return; }</code>\nshould be \n<code>#define SAFESIGNAL(a) { QGuardedPtr<QObject> self = this; emit a; if(!self) return; }</code>\n"
    author: "Bojan"
  - subject: "Re: Good stuff"
    date: 2004-03-09
    body: "I simply do not understand how QGuardedPtr will help in this case. QGuardedPtr will be set to zero when deleted, but if the owner does not use QGuardedPtr, it will still delete the object without setting the QGuardedPtr to zero.\n\nI think most of this article is wrong, and should be reduced to the starting comment: DO NOT DELETE OBJECTS IN FUNCTIONS CALLED FROM THE DELETED OBJECT. \n\nBe creative do something else, anything else.."
    author: "Allan Sandfeld"
  - subject: "Re: Good stuff"
    date: 2004-03-09
    body: "<< QGuardedPtr will be set to zero when deleted, but if the owner does not use QGuardedPtr, it will still delete the object without setting the QGuardedPtr to zero >>\n\nNo, when the QObject inherited class is deleted, it notifies all the QGuardedPtr and they all get set to 0.\n\n"
    author: "Philippe Fremy"
  - subject: "Re: Good stuff"
    date: 2004-03-09
    body: "I think this macro is very dangerous and should be avoided, since it does more than just emitting a signal (as the name suggests - and it's not even good at that): it\na) assumes that the function has the return type 'void'\nb) fails to do any cleanup work (freeing resources which have previously allocated by the function) if necessary.\n\nIn general, it's interesting to discuss about this problem but I think it's foolish to rely on guarantees like this, since it means you suddenly have to keep in mind which classes promise it and which don't. The Qt library does not give that promise (and as the article correctly states, explicitely warns about it) and I think you should not do so either. By assuming that deleting sender() in a slot is unsafe, you're always on the safe side.\n"
    author: "Frerich Raabe"
  - subject: "Re: Good stuff"
    date: 2004-03-09
    body: "I agree with the above posters. It's not a good idea to delete an object from a function called by that object. It goes against the whole component idea lying behind the signal/slot paradigm. \nIf you do want to delete an object from a slot, I think I would give the signal an additional by reference boolean argument 'delete'. Set it to true in your slot, and let the object take care of deleting itself when it's ready to do so. That way, you're sure that no other objects are still to be called (a signal can be connected to more than one slot!) Of course, you still need to make sure you're not using any pointers to your deleted object anymore, but that is a matter of good software design, I'd say.\n\nSo: stick to the warning in the docs not to delete an object from a slot triggered by that object."
    author: "Andr\u00e9 Somers"
  - subject: "Re: Good stuff"
    date: 2004-03-09
    body: "\"It's not a good idea to delete an object from a function called by that object. It goes against the whole component idea lying behind the signal/slot paradigm.\"\n\nDoes it really go against anything?  If you can't delete the sender in the slot, then you have to delay it, and delaying the delete is tricky business.  Even QTimer is not necessarily enough (if sub-eventloops are invoked), which means that there is no guaranteed safe time to delete anything.  You either need a safe-owner policy or a safe-child policy, and I've found that the latter is easier.\n\n\"a signal can be connected to more than one slot!\"\n\nIndeed, though I don't think it breaks any rules if one of the listeners misses out on receiving a signal."
    author: "Justin Karneges"
  - subject: "Re: Good stuff"
    date: 2004-03-10
    body: "i would have to agree with previous poster about not deleting an object from a function called by that function. what are qt signal/slots - just wrappers around plain functions. its not a fire and forget kind of scenario - the function will get called and then only the processing will continue in the caller function. its something like this:\nMyClass::mainFunc() {\n   ....\n   dontDeleteMe(); // equivalent to signal emitted and function called\n\n   .... // any processing beyond this is not safe if SomeClass doesn't \n        //heed the warning of not deleting this class object\n\n}\n\n// the slot function - but treat as a part of the chain from the previous \n// function\nSomeClass::dontDeleteMe() {\n   delete MyClassObject;\n}\n\nso rule of thumb - DON'T delete caller class objects in the slot. One can always use a timer singleshot (as you indicated) to delete the caller if one really wants to do so.\n\nSomeClass::dontDeleteMe() {\n   QTimer::singleShot(0, this, SLOT(deleteMyClass()));\n}\n\nSomeClass::deleteMyClass() {\n  delete MyClassObject;\n}\n\nhere MyClassObject will be deleted only after all the signal processing is complete and the application returns to it event processing.\n\nadditionally, i don't agree about the part of not using a qdialog::exec(). very frequently one needs to do a yes/no type of query and do further processing based on the result of this query. without using an exec, one would to override the functions in the dialog class to find out the results. \n\npahli_bar\n\n\n\n"
    author: "pahli_bar"
  - subject: "Re: Good stuff"
    date: 2004-03-10
    body: "You really should read the article and my comments here.  QTimer doesn't solve everything.  Your example above is essentially the same as deleteLater(), which is error-prone if you mix it with QDialog::exec().  I cover this case in my article.  Of course, your example would work for your specific purpose, but my goal here was to find a generic solution.  Instead of having to guess or deal with slots safely depending on what is going on, I wanted to come up with something that would work everywhere, all the time.\n\nThe fully generic user-side solution is a lot of code, certainly not something you'd want to write for every slot, and this is why I made the 'SafeDelete' class.  You don't need to prove to me that it is possible to secure things on the user-side, as I've already demonstrated this with SafeDelete.\n\nHowever, you have to admit that solving the problem in the object is a heck of a lot simpler and saner.  Best of all, writing a signal-safe class does not require the user to change anything.  He simply benefits from increased crash-resistance.  What is there to lose?"
    author: "Justin Karneges"
  - subject: "Re: Good stuff"
    date: 2004-03-09
    body: "Personally I don't use the macro in my own code, it was just a way to illustrate how straightforward the concept is, as opposed to the QTimer method which requires a lot more thought and planning.  I definitely agree with your points (a) and (b), and you'll notice that in the 'legacy' section I don't use the macro, as I need to free resources before returning.\n\nI have encountered so many instances where programs explode in cleanup cases, due to a signal being on the call stack somewhere.  If we rule out the use of delete, deleteLater(), and QTimer as the response of a signal, then cleanup is impossible.  If you're careful about invoking sub-eventloops, then you can at least use the latter two.\n\nIn my experience, I've found that it is easier for the child object to behave itself than for the owner to make workarounds."
    author: "Justin Karneges"
  - subject: "He's the author of Psi"
    date: 2004-03-09
    body: "I think it's good to note he's also the (primary) author of the wonderful Jabber client called Psi (http://psi.affinix.com).\n\nCheck it out !"
    author: "Jelmer Feenstra"
  - subject: "Exceptions?"
    date: 2004-03-09
    body: "After reading this article I thought: Isn't it a pitty that Qt doesn't use exceptions?"
    author: "Nick"
  - subject: "Re: Exceptions?"
    date: 2004-03-09
    body: "I agree, using exceptions would be technically superior.\n\nHowever, given the range of platforms Trolltech wants to support with QT, that they do not require exception support is probably a good (business) decision for them.\n\nBut a more grave problem I think is, that building QT with exception support is not encouraged by Trolltech and KDE. This way integration of code which uses exceptions is not naturally possible."
    author: "Some KDE user"
  - subject: "Re: Exceptions?"
    date: 2004-03-11
    body: "I agree entirely. Exceptions would make things much, much easier to code. In one of my projects I've converted a return-code based function call hierarchy to exceptions, and saved about 1/4th of the lines of code (and many indentation levels).\n\nIMHO it was a bad design decision that Qt doesn't work with exceptions. It is one of the major reasons why I would not consider it for my next business class project.\n\n-- Hannes\n"
    author: "J6t"
  - subject: "more suggestion :)"
    date: 2004-03-11
    body: "wrote up more solution to the problem.\n\nspent way too much time not to post :)\nhttp://tanoshi.net/signalsafety-ext.html\n\n"
    author: "Akito Nozaki"
  - subject: "Alternate solution"
    date: 2004-03-18
    body: "How about the following alternate solution:\n\nDeleter.h:\n\nclass Deleter {\npublic:\n  void deleteRegistered();\n  void registerForDeletion(QObject *);\n...\n};\n\nextern Deleter deleter;\n\nAnd in main.cpp:\n\nwhile(..) {\n  // Let the Main loop run for a few ticks\n  deleter->deleteRegistered();\n}\n\nAnd delete stuff via registerForDeletion(..);\n\nThis guarantees what you wanted: That there's no signal on the call stack when the actual deletion takes place."
    author: "Peaker"
---
After many years of writing Qt code, most of which has been non-GUI-based, I've gained a great deal of experience with signals and slots.  One tricky issue about signals is that they are generally emitted when the QObject is not in a safe state to be deleted.  This can often bite the user of such an object unexpectedly, especially when performing resets or invoking QMessageBox.  The simple solution is to write all QObject classes in such a way that they are deletable as the result of any signal they emit.  I've written an <a href="http://affinix.com/~justin/signalsafety.html">article on Signal-safety</a>, where I outline the problem and solution, as well as provide workarounds for safely handling misbehaving objects (those that don't follow such guidelines).  Every Qt developer should read and understand this.  Happy coding!




<!--break-->
