---
title: "KDE CVS-Digest for September 24, 2004"
date:    2004-09-25
authors:
  - "dkite"
slug:    kde-cvs-digest-september-24-2004
comments:
  - subject: "Great stuff again :)"
    date: 2004-09-25
    body: "Nice, after all Plastik is now the default style. The new nVidia control center module sounds great :) Gonna install and test it soon.\nI'm not so sure about the MM backend stuff though, wouldn't it be much better when the KDE and Gnome guys sit down and choose one backend? That backend could be development further (FD.O?) so it meets all the requirements. I'm afraid this 'multiple-backend'-stuff will become a mess. Can someone enlighten me?"
    author: "Niek"
  - subject: "Re: Great stuff again :)"
    date: 2004-09-28
    body: "Plastik ?\n\nWhy not choosing something new for KDE 4 ? Everybody already know keramik and plastik. every kde version has his new default theme. why not giving kde 4 a new fresh look, something people will be impress and anxious about. ???\n\nin the mean time, Niek, if you read all the MM stuff, I think they propose the best solution. Creating a wrapper which communicate with most common MM framework. This does not require Gnome collaboration and makes all kde application works everywhere.\n\nplus, no API change, since KDE still controls it, its like aRts becoming a wrapper but without the MM conflict with other software.\n\n"
    author: "somekool"
  - subject: "Re: Great stuff again :)"
    date: 2004-09-28
    body: "The next version will be KDE 3.4 and not 4.0."
    author: "Anonymous"
  - subject: "It's about API stability"
    date: 2004-09-25
    body: "While I agree that trying to keep up with multiple backends seems like an uphill battle that can't possibly be won, having a simple abstraction layer so KDE apps can be on the save side API-wise sounds like a good idea (or even a necessity, considering the 'Desktop Environment' moniker). I think a decision should be made on which backend to focus most proimentlyto ensure a fully-featured KDE 4 release, but having some basic wrapper functionality in place that can be made to work with additional backends doesn't hurt."
    author: "Eike Hein"
  - subject: "kdemm : thinking forward"
    date: 2004-09-25
    body: "Just some notes about the incoming multimedia framework.\nSince we're going to have in into KDE4 and rely on it for the next 2-3 years I think it must be planned good. Ideally it must be fast, clean, feature complete and face technologies we'll find in upcoming OSs next years.\n\nSo here are some MORE contraints I'd like to be kept into consideration when planning the KDEMM:\n\n- video / audio channels monitorable, that means applying effects, getting the current stream for processing, applying FFTs on an audio channel and be notified when getting samples, etc.\n\n- multiple paths for video/audio/analyzed data streams. for example amarok/noatun/sidebar player/etc _must_ have a way to connect to an analyzer embedded into the desktop window.\n\n- capture as source. video/audio capture must be supported as a source like others, but it must have a driver that capture if somebody that need that type of stream is listening. for example I'd like to implement Video/Audio authentication as another method to autenticate.\nBut while KWallet watches my face or my eye for letting me in, the same stream could be requested by video conference in kopete or anything other.\n\n\nPlanning it good and clean as other successful kde technologies will bring more fun into kde (and more useful applications, see the 'auth' example above).\n\nI'd like to join discussion and development and I'm sure there are a lot of people outta here with cooler (and hopefully implementable :-) ideas.\n"
    author: "Enrico Ros"
  - subject: "Re: kdemm : thinking forward"
    date: 2004-09-25
    body: "And it has to be ready to go in about a year.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: kdemm : thinking forward"
    date: 2004-09-25
    body: "The API for sure. But that will be more pencil/mouth work than keyboard one.\n\nThe API needs to be good so we can plug in it more and more features (the camera capturers, stream sources, filters, cool filters, etc.), but that comes next.\n\n"
    author: "Enrico Ros"
  - subject: "KDE in African languages (O/T)"
    date: 2004-09-25
    body: "Hello all.\n\nOn LWN.net (http://lwn.net/) there is an article \"Africans get tools to cross the digital divide (Globe & Mail)\" that states (quoting http://tinyurl.com/4djqe\">TheGlobeAndMail)\n\"\"Last week, Mr. Bailey's group, Translate.org.za, launched versions of the software Open Office [...] in Zulu, Afrikaans and Northern Sotho, the predominant languages in the three main language groups in South Africa -- the first software to exist in any of those languages.\"\"\n\nI already posted a comment stating that KDE is already translated to those languages between 30 and 56%).\n\nMaybe, it would be a good thing for the KDE AND the OOo translation, if the translators of both projects would talk to each other, i.e. know of each other in the first place.\n\nJust my 0.02$.\n\n"
    author: "Harald Henkel"
  - subject: "Re: KDE in African languages (O/T)"
    date: 2004-09-25
    body: "Translate.org.za are the people behind the KDE translations into those languages too, so hard for them not to know about each other ;-).\n\nNewspapers getting facts wrong, whatever next...  I'm so surprised.  I mean I believe everything that's in print!  Err, ok, maybe not."
    author: "Spanner"
  - subject: "Re: KDE in African languages (O/T)"
    date: 2004-09-25
    body: "Indeed, Dwanye Bailey is even the team coordinator of the KDE Zulu translation team. So, it seems to be a newpaper error.\n"
    author: "Harald Henkel"
  - subject: "Re: KDE in African languages (O/T)"
    date: 2004-09-27
    body: "And both Dwanye and David Fraser were both at OOoConf last week.\nNice guys."
    author: "CPH"
  - subject: "Thanks for excellent coverage"
    date: 2004-09-25
    body: "Great, Derek, that the CVS digest is so very readable and informative, esp. with the added coverage about the mm discussions. I weekly read the digest quite thoroughly. Many thanks again!"
    author: "wilbert"
  - subject: "Plastik"
    date: 2004-09-25
    body: "nice...\n\n-------------------------------\nFrank Karlitschek committed a change to kdelibs/kdefx/kstyle.cpp in HEAD on September 17, 2004 at 08:27:01 PM\n\nPlastik is the new default style.\n\nView Source Code (1 File)\n-------------------------------"
    author: "standsolid"
  - subject: "Re: Plastik"
    date: 2004-09-25
    body: "IMHO this decision took just too long and should already have been done for 3.3. ;-( \nWell better now than never or later I guess. ;)"
    author: "Anonymous"
  - subject: "Re: Plastik"
    date: 2004-09-26
    body: "Yeah, Plastik is already old and outdated now.  Why not choose something more modern like Keramik?  I've been using Keramik for ages and it seems stable despite all the graphic effects."
    author: "ac"
  - subject: "Re: Plastik"
    date: 2004-09-26
    body: "Because most people seem to prefer Plastik. Keramik looks amateurish and simply takes up too much screen real estate."
    author: "Stephen Douglas"
  - subject: "Re: Plastik"
    date: 2004-09-26
    body: "Keramik is older and more outdated than Plastik. What's your point?"
    author: "Anonymous"
  - subject: "Re: Plastik"
    date: 2004-09-26
    body: "Somehow keramik looks more modern than plastik indeed. The thing I like about plastik are the window decorations, which looks like semi transparent plastic with lights beneath it, and the small GUI elements. However, and that's what I like about keramik, these GUI elements from plastik are really boring. I also don't see what's plastic about that, more clay I think. Oh well, fashion .. wonder when we get hi-color retro :-)"
    author: "koos"
  - subject: "Re: Plastik"
    date: 2004-09-27
    body: "Boring is good. The GUI should not impose on what we are doing. It should be there to help us perform our tasks but it shall not steal the attention from the contents of our applications. So, a change to Plastic is a step forward. Keramik also had some problems with readability. I'm specifically thinking of the text in window decorations. The border around the text was catching the eye too much and made the text hard to read.\n"
    author: "Uno Engborg"
  - subject: "Re: Plastik"
    date: 2004-09-26
    body: "The main reason it wasn't default in kde 3.3 was the fact that documentation screenshots for all the languages KDE supports had to be remade with the new default style. Now we have the capablitity to do that automatically with a tool, so the doc team dropped their objections to changing the default.\n\nOf course, the fact that a lot of distros switched to plastik or other themes ANYWAYS doesn't help that fact, but nothing we could do about that."
    author: "anon"
  - subject: "Re: Plastik"
    date: 2004-09-26
    body: "I was all for keeping plastik, mostly because IMO changing default style all the time is not a good option. I don't understand the documentation argument though! After all most application interfaces in 3.3 are different from 3.1 ones! If that is not enough motivation for new screenshots I don't think changing style should be much of an issue."
    author: "John Styles Freak"
  - subject: "Re: Plastik"
    date: 2004-09-26
    body: "Some users are disoriented if the style in the documenation does not match the one on the screen. (Don't ask me why, I do not know. But it seems to be a reality and to be a real problem.)\n\nChanging the default style means to change all screenshots from all applications, even for the screenshots that would have not otherwise changed. \n\nAdditionally this has to be done not only in the original documentation but also in all translated documentation.\n\nHave a nice day!\n\n\n\n\n"
    author: "Nicolas Goutte"
---
In <a href="http://cvs-digest.org/index.php?issue=sep242004">this week's KDE CVS-Digest</a> (<a href="http://cvs-digest.org/index.php?newissue=sep242004">experimental layout</a>): KPDF supports table of contents. <a href=" http://www.koffice.org/krita/">Krita</a> adds scaling. Plastik is now the default style. The <a href="http://conference2004.kde.org/">aKademy</a> section introduces the requirements of the KDE 4 multimedia architecture, reports about kdemultimedia developers' plans and summarizes the first talk "MAS in KDE" of the multimedia track.


<!--break-->
