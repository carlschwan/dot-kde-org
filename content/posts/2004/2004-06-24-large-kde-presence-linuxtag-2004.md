---
title: "Large KDE Presence at LinuxTag 2004"
date:    2004-06-24
authors:
  - "trahn"
slug:    large-kde-presence-linuxtag-2004
comments:
  - subject: "Yay"
    date: 2004-06-24
    body: "My beloved dot is back again. Where did it go this time? :-)"
    author: "AC"
  - subject: "Re: Yay"
    date: 2004-06-24
    body: "Probably to the LinuxTag :)"
    author: "JC"
  - subject: "kdebluetooth"
    date: 2004-06-25
    body: "you can see kdebluetooth in action there :-))))"
    author: "chris"
  - subject: "Picture of the Booth"
    date: 2004-06-25
    body: "Just found this: http://www.sebastian-bergmann.de/gallery/linuxtag2004/aba"
    author: "Anonymous"
  - subject: "another photo gallery"
    date: 2004-06-25
    body: "http://ariya.pandu.org/gallery/linuxtag2004/ (will be update with time)"
    author: "ariya"
---
<a href="http://www.linuxtag.org/">LinuxTag 2004</a> is the largest Linux and Open Source exhibition in Europe. Last year over 19,500 visitors and 159 exhibitors attended the event. This year's event will be hosted in Karlsruhe, Germany from June 23 through June 26. The KDE Team will have a large presence there, including several presentations and workshops, a large number of KDE developers, and of course the KDE mascot Konqi.
<!--break-->
<p>The KDE Project's primary focus this year will cover such topics as "KDE --
The Integrative Desktop", "Kiosk/NoMachine: KDE on Enterprise Desktops and
Thin Clients ", "Accessibility", "KDE PIM / Kontact -- Groupware for Linux",
"KDevelop and Qt-Forum", "KDE EDU -- School, Science &amp; Edutainment" and
"KOffice -- Linux in the Office".</p>

<p>Besides staffing a KDE booth where users can see the latest KDE, ask questions
and mingle with KDE developers, KDE developers will make numerous
presentations targeted at users and developers. The conference program
features more than a dozen KDE-related talks about exciting topics like KDE
Usage/(Enterprise) Deployment, KDE &amp; Qt Development, KOffice, Thinclients,
NoMachine, Knoppix, usability and successful examples for migrations to the
KDE desktop in the government.</p>

<p>Many of these talks will be held by notable KDE developers, e.g. David Faure,
Fabrice Mous, Ariya Hidayat, Lucijan Busch, Ralf Nolden, Kurt Pfeifle, Daniel
Molkentin, Klas Kalass, Eva Brucherseifer and Torsten Rahn. </p>

<p>See the <a href="http://www.linuxtag.org/cc/schedule.pl">LinuxTag Talk Schedule 2004</a>
for more information.</p>

<p>At the Office Productivity Booth Franz Schmid will present the latest
developments concerning the DTP application <a href="http://www.scribus.org.uk/">Scribus</a>.</p>

<p>We invite everybody to meet more than 40 KDE developers, KDE enthusiasts, the
members of the <a href="http://opie.handhelds.org/">Opie</a> and
<a href="http://www.qt-forum.org/">Qt-Forum</a> team  and of course our mascot
Konqi at the KDE booth. </p>

<p>You find the KDE project on booth D124 and the Office Productivity Booth at
A102.</p>

