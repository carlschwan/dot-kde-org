---
title: "OSNews.com: KDE 3.3 Beta 1 Experiences"
date:    2004-07-20
authors:
  - "Mambo-Jumbo"
slug:    osnewscom-kde-33-beta-1-experiences
comments:
  - subject: "Plastik Style"
    date: 2004-07-19
    body: "This is the 10000000st review wishing that Plastik were the default style. It's really sad that there is a small but powerful group of core developers that is preventing this from happening. I wish there would be a public vote on this."
    author: "Anonymous Coward"
  - subject: "Re: Plastik Style"
    date: 2004-07-19
    body: "Core developers? More likely the documentation writers/translators."
    author: "Anonymous"
  - subject: "Re: Plastik Style"
    date: 2004-07-19
    body: "There is a saying: There is no fighting about taste (or so - sorry, I am not a native English speaker). The original version goes like that: DEGVSTIBVSNONESTDISPVTANDVM ;-)\n\nThis is why I would like to thank the responsible people for staying with Keramik which I do appreciate very much. I like its colours and its liveliness. I like the frames that grow around the active window.\n\nBy the way, if I remember well, wasn't there quite a hype when the first versions of Keramik were released?\n\nMarkus"
    author: "Markus B\u00fcchele"
  - subject: "Re: Plastik Style"
    date: 2004-07-19
    body: "> By the way, if I remember well, wasn't there quite a hype when the first versions of Keramik were released?\n\nYeah, but then we realized it sucked for daily usage. It's the bulkyness factor. It's one of those styles that you either love or hate. That's not a good trait of a default style. "
    author: "anon"
  - subject: "Re: Plastik Style"
    date: 2004-07-19
    body: "I guess I don't understand the problem with Keramik.  I don't know what is meant by \"bulky\".  To me Keramik is simple, unobtrusive, inoffensive, and boring.  Plastik is a little more distracting/flashy for my tastes, which is why I like Keramik better.\n\nTo me, the job of a desktop environment is to step back and let the user run the apps in a productive manner.  Keramik seems to do the job better than any other theme I've tried so far.  IMO boring and simple SHOULD be the default--it's business-friendly.  If people like a little visual clutter they can change it.\n\nEveryone seems to say Keramik is ugly as if it doesn't need explaining.  I for one would like a tactful discussion of its shortcomings, because I can't think of any myself."
    author: "ac"
  - subject: "Re: Plastik Style"
    date: 2004-07-19
    body: "I see Keramik as being obtrustive, distracting, and too exciting (in a bad way). Just look at scroll bars and menus. They're all huge and 3D-ish. I never saw it as being a very nice first impression. \n\nPlastik, however, is very subtle and sleek, in my opinion."
    author: "banjo"
  - subject: "Re: Plastik Style"
    date: 2004-07-20
    body: "> To me Keramik is simple, unobtrusive, inoffensive, and boring. Plastik is a little more distracting/flashy for my tastes, which is why I like Keramik better.\n\nYou seem to be opposite from 90% of people. We find Plastik to be simple and inoffensive, while Keramik is flashy and \"heavy\". \n\nI for one like thinkeramik. Between plastik and keramik, plastik is better by a great deal."
    author: "AC"
  - subject: "Re: Plastik Style"
    date: 2004-07-20
    body: "Maybe I need to look at Plastik again.  I recall it being pretty much a Luna rip-off, and Luna is pretty much the worst theme I've ever seen.  Maybe I'm remembering the wrong one, or maybe it's improved.  I'll give it another go, but frankly I don't see Keramik's pseudo-3d effects as any worse than the overused gradients elsewhere.\n\nThen again, I still can't get used to the close button not being in the upper left corner, a decade or so after it migrated to the upper right corner.  Sure KDE allows me to change this (for which I am extremely thankful) but I can't understand the advantage of moving it in the first place."
    author: "ac"
  - subject: "Re: Plastik Style"
    date: 2004-07-20
    body: "Maybe you need to check style names before continuing to post. Keramik is the Luna rip-off."
    author: "Anonymous"
  - subject: "Re: Plastik Style"
    date: 2004-07-20
    body: "Wasn't it meant to be an \"Aqua\" ripoff?\nWhatever - I prefer it to the dull Plastik. At least widgets are clearly visible. With Plastik, I everything seems surrounded by fog. "
    author: "Luciano"
  - subject: "Bad point of reference?"
    date: 2004-07-20
    body: "Sounds like your monitor is set too bright or something... ;)"
    author: "Lee"
  - subject: "Re: Plastik Style"
    date: 2004-07-20
    body: "Just checked and Plastik is indeed the Luna rip-off.  Keramik is the Aqua/\"Windows Classic\" fusion rip-off.  I'm glad you were so polite about this, but I do wish you had been less wrong.\n\nAnyway, just checked out ThinKeramik and it is indeed very nice and low-profile.  Perhaps a new favorite, I'll have to try it out for a while.  But no, upon second glance, Plastik is just as bad as I remember."
    author: "ac"
  - subject: "Re: Plastik Style"
    date: 2004-07-20
    body: "I couldn't agree more with the close button on the upper left. The important bit is that the close button shouldn't be grouped with minimize and restore/maximize to avoid accidently closing a window. "
    author: "John Upper Left Close Button Freak"
  - subject: "Re: Plastik Style"
    date: 2004-07-22
    body: "I do the exact opposite. I put the minimize on the upper left, so my most used functions are the top corners."
    author: "Joe Schmoe"
  - subject: "Re: Plastik Style"
    date: 2004-07-22
    body: "Are you talking about the widgets or the Win-decos?  Personally I hate the Plastik win-decos, I use ThinKeramik for that and use Plastik for widgets.  (ThinKeramik with the title bubble on small)  *THAT*, IMHO, is the best combination.  I just thought I'd ask / voice my opinion.\n\nM."
    author: "Xanadu"
  - subject: "Re: Plastik Style"
    date: 2004-07-19
    body: "Most of the core developers *want* a move to plastik. However, it's simply *impractical* to do in KDE 3.3, because of documentation. Distros that change the default away from keramik on their own (which uh at least, Mandrake, SUSE, Xandros have done within the last year), have the resources to regenerate hundreds of screenshots in their own documentation. We don't until KDE 4.0. "
    author: "anon"
  - subject: "Re: Plastik Style"
    date: 2004-07-19
    body: "We -users- could help with the screenshots :)\n\nI mean, ask 4/5 users of any language to take screenshots at 800x600 (or 1024x768) in a given color-scheme, submit them to webmasters and voila! Problem solved.\n\nWhat are we waiting for?"
    author: "Norberto"
  - subject: "Re: Plastik Style"
    date: 2004-07-19
    body: "We are waiting for kde 4.\nIt is not a good idea to keep shifting the look and feel of a desktop within minor release version.\nMinor versions releases should  be cosistent.\nAs said; it is quite easy to change the default style to something else, and so,e distro`s have done that already.\n\nSo, what is the problem?"
    author: "ac"
  - subject: "Re: Plastik Style"
    date: 2004-07-20
    body: "> Minor versions releases should be cosistent.\n\nOnly small things like speeling errors can be fixed. \n\nSCNR ;-)"
    author: "cm"
  - subject: "What about DCOP?"
    date: 2004-07-20
    body: "Can't you use dcop to script all the screen shots? It would make a lot of sense, and make sure all screen shorts were upto date."
    author: "Oliver Stieber"
  - subject: "Re: Plastik Style"
    date: 2004-07-20
    body: "Contact your I18N team:\nhttp://i18n.kde.org/teams\nI am sure that many would not mind getting some help.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Plastik Style"
    date: 2004-07-20
    body: "I would suggest development of some script to generate the required shots in whatever theme you want.  Makes no sense to manually create screenshots for something that changes programmatically."
    author: "Lee"
  - subject: "Re: Plastik Style"
    date: 2004-07-20
    body: "You would still have 1000s of screenshots to do, to *verify* and to put in the corresponding documentation. (And the same for each translation.)\n\nHave a nice day!\n"
    author: "Nicolas Goutte"
  - subject: "Re: Plastik Style"
    date: 2004-07-20
    body: "If having the default style in the documentation screenshots is so important, isn't it even more important that applications are also up to date? That said I agree that changing theme all the time is not that good of a good policy. "
    author: "John Consistency Freak"
  - subject: "Re: Plastik Style"
    date: 2004-07-21
    body: "If most of the major distros don't use keramik anyways, does it really matter what style the docs are in? "
    author: "AC"
  - subject: "Re: Plastik Style"
    date: 2004-07-21
    body: "Well, I guess it's about putting your best foot forward. Especially for potential new users."
    author: "Mikhail Capone"
  - subject: "Re: Plastik Style"
    date: 2004-07-19
    body: "Plastik needs improvement, and a lot of TLC. KDE 4.0 and onwards will be a big thing, with new screenshots needing to be done, and this will be the correct time. KDE can't change its default theme every other minute."
    author: "David"
  - subject: "Re: Plastik Style"
    date: 2004-07-20
    body: "> Plastik needs improvement, and a lot of TLC\n\nI agree. I'd like to see Plastik fix the ages old \"little dots in the corner of kicker\" bug, as well as tone down the gradients *slightly*. Other than that, even it's current state, it's better than keramik. I however, agree, that KDE 4.0 is the best time to make the change from keramik to whatever should replace it, plastik or not. "
    author: "AC"
  - subject: "Re: Plastik Style"
    date: 2004-07-20
    body: "Keramic was my love on the first glance. But Plastic is now my true love (although it could have tinier window bar IMHO). I don't see a real problem. Changing the theme is only couple of clicks... I don't understand the fuss about that..."
    author: "tomislav"
  - subject: "Re: Plastik Style"
    date: 2004-07-20
    body: "I agree. Plastik is very nice, but the windec is slightly too \"fat\" for my liking."
    author: "Slartibartfast"
  - subject: "Re: Plastik Style"
    date: 2004-07-20
    body: "\"Keramic was my love on the first glance. But Plastic is now my true love.\"\n\nMen! All pigs!"
    author: "Bad Mood Bitch"
  - subject: "lol"
    date: 2004-07-20
    body: "exactly what I was thinking. better not mention that I am on the other side, too. *fg\n(you are not hetero, are you? scnr..)"
    author: "that guiser"
  - subject: "Re: Plastik Style"
    date: 2004-07-20
    body: "The window bar scales automatically with the font you use. Simply choose a smaller font, and the title bar will become smaller.\n\n(note: I don't have a clue what the default font for the title bar is, I have upgraded my KDE from 3.0 to 3.3b1 (in small steps ;-)))."
    author: "Erik Hensema"
  - subject: "Plastik *DOES* Look like Windows XP's Luna Theme"
    date: 2004-07-20
    body: "Plastik is like Luna theme with Gradient effect removed. Why don't you have gradient effect for plastik, that would of course look good. Indeed that looks.\n\nC'mon, be brave, accept that \"Plastik = Luna - Gradients\". Because you don't like Windows XP, that doesn't mean you deny everything..."
    author: "Fast_Rizwaan"
  - subject: "Re: Plastik *DOES* Look like Windows XP's Luna Theme"
    date: 2004-07-21
    body: "Plastik looks bugger all like Luna. I can use Plastik - I can't use Luna."
    author: "David"
  - subject: "Re: Plastik *DOES* Look like Windows XP's Luna Theme"
    date: 2004-07-21
    body: "Agreed. It's a similar design, but the implementation is better and more usable. I can't wait until Plastik is the default theme and they drop the ugly metallic gradient in the kicker."
    author: "Mikhail Capone"
  - subject: "Re: Plastik Style"
    date: 2004-07-20
    body: "The only problems with Plastik is there is no AA for the curves (or it's just the curves) and the button mouseover makes me want to puke green vomit."
    author: "uniqueuser"
  - subject: "Glow Window Decoration"
    date: 2004-07-20
    body: "I don't like Keramik's window decoration, but I do rather like its widgets. The Glow window decoration is great, I certainly prefer it over Plastik. Don't know if it should be the default though, as it does in fact glow."
    author: "Ian Monroe"
  - subject: "Re: Glow Window Decoration"
    date: 2004-07-20
    body: "Glow needs to support the menu button."
    author: "Anonymous"
  - subject: "Re: Glow Window Decoration"
    date: 2004-07-20
    body: "Yes, that is also true, the plastik win decor sucks :-/"
    author: "John WinDecor Appreciator Freak"
  - subject: "Quanta Plus"
    date: 2004-07-20
    body: "\"I said a few years ago that we would be rivalling the top commercial applications in a few years and here we are. Now I'm saying we will be blowing them away next year, and we will introduce technologies that \nwill revolutionize web development.\"\n\n-- Eric Laffoon, Quanta Plus project manager\n\nHi,\n\nEveryone knows of \"competitive advantage\", but \"collaborative advantage\" is more applicable when it comes to making great software.  The Quanta Plus core development team needs to be able to make it to aKademy.  Now is our opportunity to collaborate to help make this happen.\n\nAs far as I know the core team have never had the funds to meet in person and cooperate on improving Quanta Plus.  This is their time to finally do that.  They could spend a week and a half together and brainstorm how to improve Quanta.  \n\nThe project manager, Eric Laffoon, is struggling to get the money together (the entire project is financed largely out of his own pocket) and if he can't do it they will not be able to make it.  So now is truly an important time to make a donation.\n\nIt's easy to donate, and even small donations will make a big difference:\n\nhttp://quanta.sourceforge.net/main1.php?actfile=donate\n\nI've made a donation today... I hope others will too.\n\nDamon"
    author: "Damon Lynch"
  - subject: "Re: Quanta Plus"
    date: 2004-07-20
    body: "Hi\n\nI don't mean to be a party pooper for you quanta guys as I realise that a) you write great software and b) you need funds to continue doing so. \n\nAs an OSS developer myself I also appreciate that marketing your beloved package is important.\n\nBUT...(there had to be a but at the end of that)...its beginning to feel like no matter what is posted to the dot, quanta is always 'on topic', and that reading the dot is always accompanied with petitions for money for quanta development.\n\nI hope this doesn't come across as hyper critical because it isn't intended to be. But I believe in the end you will do yourselves a disservice by posting to each and every thread whether it relates to Quanta or not.\n\nBest regards\n\nTim"
    author: "TimLinux"
  - subject: "Re: Quanta Plus"
    date: 2004-07-20
    body: "Yes.  Also, if you feel it's hard to develop remotely, then make up the right technology to fix that, and add it to quanta.  That will be your first feature to blow away the competition.  Or wait 'till I make one ;)"
    author: "Lee"
  - subject: "Re: Quanta Plus"
    date: 2004-07-20
    body: "Damon is supporting us since a long time and I was surprised by his mail on the user list (it has the same text, I belive) and I'm also surprised by this posting as well. This just shows that he likes our software and maybe us as well. ;-) And he cares about it. I agree that sometimes it can be just disturbing hearing the same thing over and over (even if it is true/valid). But I don't think Quanta is/was \"on topic\" in every dot posting, although it was in many of them, but in those case it mostly made sense (was mentioned in the news, by a comment or related comments were made). In this case it's true that it's offtopic and this may upset some, but I we cannot do much about it (cannot control who posts and what), nor can I be angry as I like Damon and what he wrote is true. I hope that his post won't scare anyone. ;-)\n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: Quanta Plus"
    date: 2004-07-21
    body: "Hey, I'll admit I was a little surprised to find this post here, but I sure appreciate Damon's heart for helping out. What can I say? I was personally astonished how much of this article talkback is consumed by the discussion of styles, which I think is pretty superficial, but I'm not going to be critical of that. ;-) \n\nTechnically Quanta did not show up in this article about the 3.3 release, even though it's a part of it. Off topic? Maybe a little. Relevent? I hope so. Detrimental? The fact of human nature remains that our donations trickle down to a couple a month unless we have a great release or a someone like Damon points out that we have a genuine need. Personally I hate asking, but since becoming part of the KDE packages our release donation bump is diluted. While I would like to imagine that the fraction of a percent of our user base that makes this possible on this level just knew that now was a good time to make it count, it isn't so.\n\nI don't enjoy asking for help, but I think the benefits to the community will be worth while. Damon has been very effective in helping us to grow Quanta and I'd really like to publicly thank him for taking up the cause of helping us to hit important objectives. As for how often we appear, we have a lot going on and a lot of people are interested, though some are not. I think I can also say with confidence that no other KDE application has a better shot at besting commercial applications. Quanta is competitive with Dreamweaver for many uses now and we have many converts. If you want to see a wide spread adoption of KDE on the desktop then even if you don't do web development (and there's always Kommander too) Quanta should be of some interest for it's potential to be an asset to KDE. Besides, I still get email telling me people just found Quanta and asking for information. :-)"
    author: "Eric Laffoon"
  - subject: "Re: Quanta Plus"
    date: 2004-07-20
    body: "I dont really know everyone's personal situation, but I do know that most of us have $1 US sitting in our pockets/our desk drawer/under our bed/in our sock/under an ash tray/wherever else you can stick a dollar. And if every person who has ever used Quanta (and actually made some use out of it) were to give that dollar to Eric, the team could go first class, no doubt. Here is my dollar and $9 others as well (check PayPal).\n\nCheers Guys -- Quanta Rules!"
    author: "ZennouRyuu"
  - subject: "Pagers like in XFCE would be nice"
    date: 2004-07-20
    body: "What I'd really appreciate are pagers like in XFCE; you can drag windows from one pager desktop to the other.\n\nIt's much more efficient than right click -> to desktop -> desktop #\n\nIt'd add polish.\n\nUnless someone can tell me that they are working on it, I suppose that I'll go look to add a wish on bugs.kde.org"
    author: "Mikhail Capone"
  - subject: "Re: Pagers like in XFCE would be nice"
    date: 2004-07-20
    body: "kpager can do this"
    author: "Alex"
  - subject: "Re: Pagers like in XFCE would be nice"
    date: 2004-07-20
    body: "So I just discovered, but it's still not what I'm looking for since it can't be integrated with the kicker (I don't use that function quite often enough to leave kpager laying around, taking real estate).\n\nAnybody else interested in the issue should check out (and vote):\n\nhttp://bugs.kde.org/show_bug.cgi?id=33343\n\nhttp://bugs.kde.org/show_bug.cgi?id=58677\n"
    author: "Mikhail Capone"
  - subject: "Re: Pagers like in XFCE would be nice"
    date: 2004-07-20
    body: "You might wanna take a look at \n \n http://www.kde-look.org/content/show.php?content=13771 "
    author: "Anonymous"
  - subject: "Re: Pagers like in XFCE would be nice"
    date: 2004-07-20
    body: "Yes, KDE has this already.  If you change the panel size, it will go horizontal with all virtual desktops in a row, rather than a grid layout.  Also, it has an option to allow switching desktops by scrolling a mousewheel over the desktop background, and there are keyboard shortcuts, too.\n\nVirtual desktops have been around almost FOREVER on Unix.  Most window systems have them."
    author: "Lee"
  - subject: "Re: Pagers like in XFCE would be nice"
    date: 2004-07-20
    body: "That's not at all what I was asking for, sorry."
    author: "Mikhail Capone"
  - subject: "Re: Pagers like in XFCE would be nice"
    date: 2004-07-20
    body: "OOPs.  Sorry, guess I skimmed your post.  Yea, I think enlightenment did that thing you're looking for.  I thought it was very cool, but I wanted the idea extended a bit, so that the windows in the pager were the same windows as on the desktop.  Not graphically -- just in terms of having the full range of options that something like clicking on a window title bar gives.\n\nAlthough, now that I think about it, maybe it makes sense to ask windows to render their own thumbnails rather than the pager doing screengrabs like it does in Enlightenment :)"
    author: "Lee"
  - subject: "Re: Pagers like in XFCE would be nice"
    date: 2004-07-26
    body: "     \"It's much more efficient than right click -> to desktop -> desktop #\"\n\nApropos efficiency: I've found that 2 desktops is the right thing for me. So when doing \"right click -> to desktop -> desktop #\" I'd like the system to autodetect that I've got exactly two, and offer me \"Move to other desktop\" or \"Move to current desktop\" (respectively) and also \"Move to both desktops\". It should be in the root menu. That wouldn't take more place in the menu, and I'd save a click.\n\nI don't really care about window-moving pagers, so y'all gotta take that discussion by yourself."
    author: "Anonymous Coward"
  - subject: "Re: Pagers like in XFCE would be nice"
    date: 2004-07-27
    body: "A shortcut you may find a little quicker is:\n\nGo to the \"destination\" desktop.  Right-click on the desired app's entry in the taskbar, then select the first entry there: \"To Current Desktop\".  Et voila, it gets moved to the desktop you're currently viewing.\n\nIt's not quite what you're asking for, but I find it pretty efficient."
    author: "Anonymous Coward"
  - subject: "Theme Manager of 3.3b1"
    date: 2004-07-20
    body: "lacks:\n\n1. Splash Screens\n2. Fonts\n3. Mouse Cursor Themes (and mouse setting like double-click to open)\n4. Panel Settings (Kmenu)\n5. Window Decoration\n6. Konqueror profiles"
    author: "Fast_Rizwaan"
  - subject: "Re: Theme Manager of 3.3b1"
    date: 2004-07-20
    body: "A few remarks from the theme manager author:\n\nad 1: on my TODO\nad 2: I might consider that\nad 3: works (although it doesn't actually \"bundle\" the cursor theme, it's in the TODO)\nad 4: WONTFIX (it is a behavior, not visual thing)\nad 5: ditto as 3\nad 6: ditto as 4"
    author: "Lukas Tinkl"
  - subject: "Re: Theme Manager of 3.3b1"
    date: 2004-07-20
    body: "Don't want to be nagging, but 'Window Decoration' not a visual thing??"
    author: "ac"
  - subject: "Re: Theme Manager of 3.3b1"
    date: 2004-07-20
    body: "4 was panel settings.\n\nI think small parts of the panel settings could be moved, in fact.\nThe \"menu sidebar\" setting, maybe with an option to customize it.\nThe background tiles, transparency, etc. too. "
    author: "Luciano"
  - subject: "Re: Theme Manager of 3.3b1"
    date: 2004-07-22
    body: "The transparency and the bg image of panel are supported"
    author: "Lukas Tinkl"
  - subject: "Re: Theme Manager of 3.3b1"
    date: 2004-07-20
    body: "I once wrote a patch to add \"font themes\" to the font control center module, but it didn't seem to be well received when I tried to submit it to the developers. The patch is still in the Improvement section of kde-look.\nI'd have to port it to current KDE fonts panel, however.\n\nAbout the Theme manager: Am I the only one that have the text labels on the \"Customize your theme\" section truncated? And I also don't understand why the buttons can't look like normal buttons.\n\nThat said, the theme manager is going to improve the current \"look and feel\" situation, so I'm quite happy it's been done. \n\nFor example, it took me quite a bit of browsing to discover that the Cursor theme was under \"Peripherals/Mouse\" instead of \"Look and feel\". Maybe I'm peculiar, but I expected that the look of the cursors has little to do with the peripherals, and it's only slightly connected with the mouse. \n\nAn option I can't find anymore, which I miss, is the ability to have \"Big cursors\". On large displays, the standard cursors are a bit too hard to find."
    author: "Luciano"
  - subject: "Sound theming"
    date: 2004-07-20
    body: "-> perhaps you could insert sound themeing too !"
    author: "chr"
  - subject: "Re: Sound theming"
    date: 2004-07-22
    body: "That works as well"
    author: "Lukas Tinkl"
  - subject: "Choosing stuff"
    date: 2004-07-20
    body: "In KDE 3.1 chosing style and win decor was much easier than in KDE 3.2.\nYou could click one style or win decor, and check them out by moving the arrows. Now using the keyboard is a pain, you have to put the focus of you keyboard in that widget by using ALT, most people won't figure this out... I know it saves space, but seems like a pretty bad option to me. Probably can't be changed in time for 3.3 though...\n\nAnd yes maybe KDE-Usability is a more apropriate place for this :P"
    author: "Bad Mood Dog"
  - subject: "Konqueror "
    date: 2004-07-20
    body: "I find his comments about Konqueror to be just right. The user interface is way to cluttered. I once had a student job as an computer admin, in a facility running about 50 suse boxes with kde as default login, one of the questions ask at most had to do with configuring konqueror - perople could often not:\n\n- set the default startup page.\n- understand the idea with the diffrent profiles.\n- find the way though the menues.\n\nThese users where not geeks, but users, normaly running windows. I am 100% sure that someone will come and say that konqueror is the best file/web browser around, and it may be for geeks, but it is not great for normale perople. \n\nI think that the two apps in KDE that needs the most cleaning is Kcontrol and Konqueror - please do not misunderstand me, I am not voting for removing the advanced functionalyty in these apps. I am voting for sane defaults and a simple default look - advanced users can always go into some advanced menu and get there nifty functionalyty added to the default one."
    author: "Lars Roland"
  - subject: "Re: Konqueror "
    date: 2004-07-20
    body: "I've just recently discovered the power of Konq's profiles, and I'm loving it!  *holds gun*  \"step away from the profiles code!!\" :D"
    author: "Lee"
  - subject: "Re: Konqueror "
    date: 2004-07-20
    body: "> I am voting for sane defaults and a simple default look - advanced users can always go into some advanced menu and get there nifty functionalyty added to the default one.\n\nCompletely agreed. My family uses KDE. They all used to use phoenix/firebird/firefox back about a year ago, but when KDE 3.2 came out, I switched them to Konqueror, as it was MUCH faster on their hardware. However, my little sister continued to use firefox, because she thought it was simplier to use.  "
    author: "anon"
  - subject: "Re: Konqueror "
    date: 2004-07-21
    body: "Hey!\nWell I'm a long time Windows user just Installed my PC with Gentoo and KDE 3.2.3. Well Guess what! Konqueror the web browser has some serious usability issues. ;-) I mean I had to do a google search to actually find out how to disable pop-up windows. I use MyIE2 on Windows, and boy that is sooo much geared towards usability. Well as soon as I'm through with QT tutorial 1, I guess I can finally wet my feet with KDE Development!\nAbhi"
    author: "Abhijeet"
  - subject: "Re: Konqueror "
    date: 2004-07-22
    body: "agreed.\n\nWhile I have mine configured to only display about 5 buttons on the toolbar, its definitely way too cluttered by default.  And to be honest, the way to configure the toolbar was confusing as hell.\nIf I right click on the toolbar I have \"Show main toolbar\" and \"Show location bar\" checked.  Then I go into configure toolbars and half of the buttons that I have visible on the toolbar don't show up in the list of current actions!\n\nIt's completely unintuitive that it takes the actions from all the main toolbars and concatenates them onto the main toolbar.  Weird.  \n\nAnd theres wacky behaviour too..  Can't add a location bar to the main toolbar, can't remove the kget icon..  I guess I'll check b.k.o for those issues."
    author: "Leo S"
  - subject: "Re: Konqueror "
    date: 2004-07-23
    body: "You can get a location bar on the main toolbar, the trick is that you can only have *one* location bar, so if you remove it from the location toolbar, then it will show up in the main toolbar.\n\nIntuitive? No, not at all, but one of the smaller problems i have with konqueror. :-( One worse thing is that if you remove the \"weird\" entries (the merge ones) then you can't get them back, i have no idea how anyone could even think of making it like this, they even went to great lengths to add a warning text, instead of simply fixing the problem.\n\nI am beginning to think it would be better to start a new browser only program, since konqueror is so complex. Others have mentioned it, but i can't believe that something as simple as setting your homepage is practically impossible. (yes, i know how to do it, but i wouldn't imagine my sister could figure it out, while she had no problem making both ie and mozilla start with a blank page)"
    author: "Troels"
  - subject: "Re: Konqueror "
    date: 2004-07-22
    body: "A lot of the problem with KDE usability is the default settings. It seems as if the developers intentionally set the defaults to be really unusable, and the distro makers just seem to be keeping the defaults as the standard settings.\nFor example, why is the Up arrow, the least used button in web browsing (because it rarely does anything beyond bring up an error page) on the far left where every other browser has Back, the most used button.\nI love the kde desktop, but it is a little annoying that it takes so long fiddling with settings to make it usable. Many users just won't bother."
    author: "Ben Roe"
  - subject: "Re: Konqueror "
    date: 2004-07-22
    body: "> It seems as if the developers intentionally set the defaults to be really unusable\n\nPlease troll somewhere else. Thank you."
    author: "Anonymous"
  - subject: "Re: Konqueror "
    date: 2004-07-23
    body: "I'm not trolling. Why is a constructive criticism trolling? I get plenty of it to my project (http://benroe.com/sied) and consider it very useful. The default settings have remained reasonably constant through plenty of releases, and many people have a problem with them.\n\nI use KDE 10-12 hours a day. But it's only usable, at least IMHO, after a lot of fiddling. The default toolbar icon sizes, the default icon theme (try figuring out quickly which is reply, check mail and forward in the default Crystal/tiny icon settings - I'm not the only one who thinks this is a bit silly: http://lwn.net/Articles/91308/). It's not very welcoming to new users, who may never figure out the true power of KDE."
    author: "Ben Roe"
  - subject: "Re: Konqueror "
    date: 2004-07-23
    body: "> . The default toolbar icon sizes,\n\nhttp://lists.kde.org/?t=108877140700002&r=1&w=2"
    author: "anon"
  - subject: "Re: Konqueror "
    date: 2004-07-23
    body: "Yay!"
    author: "Ben Roe"
  - subject: "Re: Konqueror "
    date: 2004-07-22
    body: "I agree Konqueror has a few usability problems.\n\nTo the developers: Pretty please, with sugar on top, consider renaming the Location menu to File!\n\nOther problems:\n\n1. Right click a web page, the context menu has an Actions sub-menu. Most of these involve adding the webpage to archives (gzip, zip, bzip2). All fail, in various ways, as we're not in the local filesystem here. None should be there. The menu also includes Print, which is likely useful, though not necessarily needed here. Also the name \"Actions\" is meaningless, as all menu options are actions.\n\nAlso, \"Copy Text\" should just be \"Copy\", in order to match the hundreds of other places a user is likely to have seen. The \"Text\" bit may seem helpful, but it's not.\n\nWell there I go again on the Konqueror context menu... I haven't filed enough bugs on this yet! ;-) Maybe fixed in 3.3 anyway...\n\nDon't want to be too negative about Konqueror, but I have never really enjoyed using it. The tabbed file browsing is cool though. :)"
    author: "Lars Janssen"
  - subject: "Stability"
    date: 2004-07-20
    body: "I have to agree that 3.3 beta1 is the most stable bete KDE team ever produced, I'm using it daily and I've naver had any serious crash, actually the only programs that crashes are quanta (when you fastly closes all the files open) and kget (this one is really buggy). But those are minor things for a beta.\nI belive this shows that KDE is already a very mature techonology and probally the jump to KDE4 won't be problematic at all."
    author: "Iuri Fiedoruk"
  - subject: "Re: Stability"
    date: 2004-07-20
    body: "> I have to agree that 3.3 beta1 is the most stable bete KDE team ever produced\n\nI think this is at least partly due to the shorter release cycle. Let's hope that Beta 2 with all the last-day features is stable too."
    author: "Anonymous"
  - subject: "Re: Stability"
    date: 2004-07-20
    body: "It's probably stable because it's the end of line 3.x release. KDE 2.2 was very stable as well because of this reason (and had feb 2000->aug 2000 6 month release cycle as well)\n\nNote that kde 2.1 was pretty crash prone (not as bad as kde 2.0), and has a small release cycle (4 months). kde 1.1 was pretty stable."
    author: "anon"
  - subject: "Re: Stability"
    date: 2004-07-20
    body: "KDE 3.2 was also believed to be the end of 3.x line at time of release. And were their betas especially stable because of that?"
    author: "Anonymous"
  - subject: "Re: Stability"
    date: 2004-07-22
    body: "I would guess it being the other way around: 3.2 was not the end of the 3.x line because it didn\u00b4t stabilize too well"
    author: "AC"
  - subject: "Re: Stability"
    date: 2004-07-22
    body: "You do recognize that 3.3 has new features, yes?"
    author: "Anonymous"
  - subject: "Re: Stability"
    date: 2004-07-22
    body: "The problem was more that, as there was no Qt4, KDE4 could not be started.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Stability"
    date: 2004-07-20
    body: "In my case, I have more crashes using Konqueror in Beta 1 than in Alpha 1.\n\nI keep getting this one on several web sites (no problem with 3.2.x & 3.3a1) :\n\nUsing host libthread_db library \"/lib/libthread_db.so.1\".\n[KCrash handler]\n#5  0x41d5a78a in KJS::Value::operator=(KJS::Value const&) ()\n   from /usr/kde/3.3/lib/libkjs.so.1\n#6  0x41c18b20 in EmbedLiveConnect::toPrimitive(KJS::ExecState*, KJS::Type) const () from /usr/kde/3.3/lib/libkhtml.so.4\n#7  0x41c2b01b in TestFunctionImp::call(KJS::ExecState*, KJS::Object&, KJS::List const&) () from /usr/kde/3.3/lib/libkhtml.so.4\n#8  0x41acdf44 in KHTMLPart::clear() () from /usr/kde/3.3/lib/libkhtml.so.4\n#9  0x41ae26ab in KHTMLPart::restoreState(QDataStream&) ()\n   from /usr/kde/3.3/lib/libkhtml.so.4\n#10 0x41af66d1 in KHTMLPartBrowserExtension::restoreState(QDataStream&) ()\n   from /usr/kde/3.3/lib/libkhtml.so.4\n#11 0x415702cc in KonqView::go(int) ()\n   from /usr/kde/3.3/lib/libkdeinit_konqueror.so\n#12 0x415ad929 in KonqMainWindow::slotGoHistoryDelayed() ()\n   from /usr/kde/3.3/lib/libkdeinit_konqueror.so\n#13 0x415c0516 in KonqMainWindow::qt_invoke(int, QUObject*) ()\n   from /usr/kde/3.3/lib/libkdeinit_konqueror.so\n#14 0x40b7dacc in QObject::activate_signal(QConnectionList*, QUObject*) ()\n   from /usr/qt/3/lib/libqt-mt.so.3\n#15 0x40ead99a in QSignal::signal(QVariant const&) ()\n   from /usr/qt/3/lib/libqt-mt.so.3\n#16 0x40b9782d in QSignal::activate() () from /usr/qt/3/lib/libqt-mt.so.3\n#17 0x40b9ef63 in QSingleShotTimer::event(QEvent*) ()\n   from /usr/qt/3/lib/libqt-mt.so.3\n#18 0x40b225bf in QApplication::internalNotify(QObject*, QEvent*) ()\n   from /usr/qt/3/lib/libqt-mt.so.3\n#19 0x40b2198b in QApplication::notify(QObject*, QEvent*) ()\n   from /usr/qt/3/lib/libqt-mt.so.3\n#20 0x406f1946 in KApplication::notify(QObject*, QEvent*) ()\n   from /usr/kde/3.3/lib/libkdecore.so.4\n#21 0x40b11e95 in QEventLoop::activateTimers() ()\n   from /usr/qt/3/lib/libqt-mt.so.3\n#22 0x40acdc96 in QEventLoop::processEvents(unsigned) ()\n   from /usr/qt/3/lib/libqt-mt.so.3\n#23 0x40b345d8 in QEventLoop::enterLoop() () from /usr/qt/3/lib/libqt-mt.so.3\n#24 0x40b34488 in QEventLoop::exec() () from /usr/qt/3/lib/libqt-mt.so.3\n#25 0x40b22811 in QApplication::exec() () from /usr/qt/3/lib/libqt-mt.so.3\n#26 0x415601fe in kdemain () from /usr/kde/3.3/lib/libkdeinit_konqueror.so\n#27 0x4089e966 in kdeinitmain () from /usr/kde/3.3/lib/kde3/konqueror.so\n#28 0x0804ca03 in ?? ()\n#29 0x00000002 in ?? ()\n#30 0x0805ef70 in ?? ()\n#31 0x00000001 in ?? ()\n#32 0x0805ef13 in ?? ()\n\nMoreover, a Konqueror plugin is very buggy in B1 and makes Konqui crash each time a program launches it on a specific URL."
    author: "Nicolas Blanco"
  - subject: "Re: Stability"
    date: 2004-07-20
    body: "dot.kde.org is not bugs.kde.org!"
    author: "Anonymous"
---
<a href="http://osnews.com/">OSnews.com</a> features a <a href="http://www.osnews.com/story.php?news_id=7736">"non-typical" review of KDE 3.3 Beta 1</a>. It contains the observations of what the user Osho GG saw when he updated from KDE 3.2.3 to KDE 3.3 Beta 1. The story examines the central parts of KDE and some major applications.










<!--break-->
