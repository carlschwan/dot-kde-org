---
title: "KDE-CVS-Digest for July 30, 2004"
date:    2004-08-02
authors:
  - "dkite"
slug:    kde-cvs-digest-july-30-2004
comments:
  - subject: "Huh?"
    date: 2004-08-02
    body: "\"Kommander is also being developed to be the ultimate tool for in house vertical market software.\"\n\nApologies - but what exactly is 'in house vertical market software'?\n\nI've never heard of vertical market software...  What's vertical market?\n\n\nJust curious.\n\nThanks!"
    author: "name"
  - subject: "Re: Huh?"
    date: 2004-08-02
    body: "http://en.wikipedia.org/wiki/Vertical_application\nhttp://en.wikipedia.org/wiki/Vertical_Market"
    author: "Jaroslaw Staniek"
  - subject: "Re: Huh?"
    date: 2004-08-02
    body: " 'in house vertical market software'\n\nSounds like a contradiction in terms to me. Software which is 'in house' doesn't get marketed by definition. \n\nA vertical market is a business area such as financial services or GIS or whatever. A horizontal market is something generic like word processing or spreadsheets which all sorts of types of business will use."
    author: "Richard Dale"
  - subject: "Re: Huh?"
    date: 2004-08-02
    body: "\"'in house vertical market software'\n\nSounds like a contradiction in terms to me.\"\n\nNot to me. \n\nIn-house = written by the local IT\nVertical market = Software that fills a certain specific need\n\nIn-house vertical market software = Software written in house by the local IT to fill a need for certain specific need inside the company.\n\nAnd that software DOES get marketed: To the users of the software inside the company! Just because they don't sell it for revenue, does not mean it's not marketed."
    author: "janne"
  - subject: "Re: Huh?"
    date: 2004-08-02
    body: "\"And that software DOES get marketed: To the users of the software inside the company!\"\n\nI'm sorry that isn't marketing, that's selling. There is only one buyer and one seller involved.\nFor a market you need one or more buyers, and two or more sellers who are *competing*. "
    author: "Richard Dale"
  - subject: "Re: Huh?"
    date: 2004-08-02
    body: "Semantics. Marketing might as well mean in this case \"hey, your workstation has this new whiz-bang app that you can use to accomplish xxxxxxxx\". Of course the users could use something else to do the same thing. So even if we follow your definition, there would still be marketing involved.\n\nAnd, like I said: it's just semantics. You know perfectly well what was meant, you are just arguing whether some word means some specific thing or not. in short: arguing for the sake of arguing."
    author: "janne"
  - subject: "Re: Huh?"
    date: 2004-08-02
    body: "No, we're not just discussing 'semantics', someone originally asked for clarification of what Eric Laffoon was talking about, otherwise I wouldn't have said anything."
    author: "Richard Dale"
  - subject: "Re: Huh?"
    date: 2004-08-02
    body: "Janne was right. Maybe it was a poor choice of terms or maybe it was just what made sense in my mind combining the phrases. To my mind vertical market software does not have to be developed externally and the idea that it can be done very quickly for specific needs would be very appealing for a company considering using KDE.\n\nIt's fun to get in after a long weekend and see something I said is the first thing disputed here. ;-) I met a guy on Saturday at my booth who lives in New York City and works for a company that has something like 1000 KDE desktops. I thought he said he was going to be converting another 1000, and I really wanted to talk with him more on it but we were too busy. I hope he emails me. Anyway he was excited about Quanta and I mentioned using Kommander to quickly develop in house small applications and integration. He was familiar with Kommander and was quite excited it's development and using it. I'm hoping he can give us some good testimonials of KDE and Kommander use in the future."
    author: "Eric Laffoon"
  - subject: "Re: Huh?"
    date: 2004-08-02
    body: "\"Janne was right. Maybe it was a poor choice of terms or maybe it was just what made sense in my mind combining the phrases. To my mind vertical market software does not have to be developed externally and the idea that it can be done very quickly for specific needs would be very appealing for a company considering using KDE.\"\n\nWell, if it was a poor choice of terms, and all I was doing was pointing that out, I fail to see how I was wrong..\n\nHow about calling Kommander 'a killer tool for scripting in house custom applications'?\n\nIMO 'custom application' is a much better name than 'in house vertical market application'"
    author: "Richard Dale"
  - subject: "Re: Huh?"
    date: 2004-08-02
    body: "or better:\n\n'a killer tool for visually scripting in house custom applications'"
    author: "Richard Dale"
  - subject: "Re: Huh?"
    date: 2004-08-05
    body: "'a killer tool for visually scripting in house custom applications'\n\nWell this illustrates why it is so hard to come up with a descriptive phrase... Maybe \"visually building\" is better because while you can do scripting you can also rely on internal DCOP functions, which could be called scripting, but is not in the classical sense. Besides, scripting usually implies a script language builds the GUI. To paint the right picture means saying too little or too much.\n\nTo me it is an attempt to make small to medium size application building comparable to working with a spreadsheet for ease of use. There are a number of improvements needed to make it really friendly, but it's making good progress. It will also change shortly when we switch to K(exi) Form Designer which means we could remove licensing issues if someone wants to resell Kommander programs. Because they can be compiled to binary it adds an interesting twist. In a few months we will probably have a version with a new editor that natively does KPart creation and loading, MainWindow and wizards (without having to use Designer), menu and action support and more. Then we can discard even more constraints from the description. ;-)"
    author: "Eric Laffoon"
  - subject: "BTW"
    date: 2004-08-02
    body: "\"Kexi adds two run modes; final mode and design mode.\"\n\nA Small contest: could somebody invent more appropriate term for \"final mode\"? , i.e. the application mode provided for end-user (without design tools)."
    author: "Jaroslaw Staniek"
  - subject: "Re: BTW"
    date: 2004-08-02
    body: "Design mode and production mode.\n\nRich."
    author: "Richard Moore"
  - subject: "Re: BTW"
    date: 2004-08-02
    body: "We call that sort of thing 'Target Mode' in our area of work, but Production is good, too, and prolly better for the lay person :)"
    author: "Dawnrider"
  - subject: "Re: BTW"
    date: 2004-08-02
    body: "I prefer \"user mode\", that's the mode the users see!\n\n\"production mode\"? That's the mode used for production, maybe almost the same like \"design mode\"."
    author: "atrink"
  - subject: "Re: BTW"
    date: 2004-08-03
    body: "The businesses that I've worked at have all used the term \"production environment\" to mean \"computers the end-users are actually using\".  I've never really thought about why that is called \"production\", though."
    author: "Tukla Ratte"
  - subject: "Re: BTW"
    date: 2004-08-02
    body: "kexi runtime? runtime mode might sound silly, dunno."
    author: "Futt"
  - subject: "Re: BTW"
    date: 2004-08-02
    body: "I like it, actually...\n\n(however, \"runtime\" would be difficult to translate to Polish, nie, Jarku?)"
    author: "MacBerry"
  - subject: "Re: BTW"
    date: 2004-08-02
    body: "We've one problem with \"runtime\" term: it's  already reserved for Runtime Mode (i.e. the one that allows to use a database app created with Kexi without Kexi environment, i.e. a binary).\n\nAnd yes, most of these terms are damn hard 1-to-1 translatable to Polish (and other langiages too, I suppose)."
    author: "Jaroslaw Staniek (Kexi Team)"
  - subject: "Re: BTW"
    date: 2004-08-02
    body: "Hmm... I thought that \"binary\" mode was the one you were talking about.\n\nSounds like what you have is a development/design mode, a binary/runtime/standalone app mode, and what you want a name for would be a \"preview\" mode?"
    author: "Lee"
  - subject: "Re: BTW"
    date: 2004-08-02
    body: "Oh, I thought that was what was referred to in the original request?"
    author: "Futt"
  - subject: "Re: BTW"
    date: 2004-08-02
    body: "How about design and release mode."
    author: "manyoso"
  - subject: "Re: BTW"
    date: 2004-08-03
    body: "or design and work mode?"
    author: "-"
  - subject: "What happened with preview mode ? =)"
    date: 2004-08-03
    body: "Uhm, what about \"Preview mode\" for example ? I think everyone would get its meaning, fast ;-)."
    author: "Edulix"
  - subject: "Re: BTW"
    date: 2004-08-04
    body: "Design and use(r) mode. It is clear and short. "
    author: "Jord"
  - subject: "No CVS Digest last week ?"
    date: 2004-08-02
    body: "Very hard to live 1 week without KDE-CVS Digest !\n\nGerard"
    author: "gerard"
  - subject: "Re: No CVS Digest last week ?"
    date: 2004-08-02
    body: "Agreed.  This great for the KDE userbase to keep track of what is going on inside KDE.  Keep up the great work and thanks from this KDE user.  :)"
    author: "am"
  - subject: "Re: No CVS Digest last week ?"
    date: 2004-08-02
    body: "I enjoyed it :)\n\nA thought occurred to me when reading the lwn report on X.org. I would enjoy reading a commit digest for that project. Is anyone interested in producing one?\n\nDerek"
    author: "Derek Kite"
  - subject: "KDE CVS Digest is too long to read"
    date: 2004-08-05
    body: "subject"
    author: "a"
  - subject: "Re: KDE CVS Digest is too long to read"
    date: 2004-08-05
    body: "Nope, not at all. It's categorised, so all you have to do (if you find it too long) is scroll down to the relevant section. I'm not interested in EVERY KDE application etc, but it's still good to see the amount of work being done, and interest that is out there.(and seems to be still growing)\n\nMy congratulations and thanks to Derek, but I think he should ask next time before he takes a week off!! :o)"
    author: "Gogs"
  - subject: "Re: No CVS Digest last week ?"
    date: 2004-08-03
    body: "Agreed. Now, does anyone know where KDE Traffic has gone?"
    author: "Anon"
  - subject: "Taiki Komoda been busy"
    date: 2004-08-02
    body: "With more that 128.000 lines written in the last 2 weeks this guy must be exhausted :) I'd recommend sleep;)"
    author: "B\u00f6rkur"
  - subject: "Re: Taiki Komoda been busy"
    date: 2004-08-04
    body: "Those are translations; they always pick up around this time in the release cycle."
    author: "Scott Wheeler"
  - subject: "How is the top-10-list ordered?"
    date: 2004-08-02
    body: "Everytime I look at this list (http://i18n.kde.org/stats/gui/HEAD/toplist.php)\nI ask myself what's the criterion for ordering this list?\n\"translated\", \"fuzzy\" or sum of both?\nI do't get it."
    author: "atrink"
  - subject: "Re: How is the top-10-list ordered?"
    date: 2004-08-02
    body: "It is clearly sorted by \"translated\".\n1. Swedish 99880 translated\n2. Portuguese 99859 translated\nand so on.\n\n(\"Fuzzy\" is not much different that untranslated.)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: How is the top-10-list ordered?"
    date: 2004-08-02
    body: "OK, but what are the green bars good for, which reflect the translated messages?\nObviously the length is computed wrong."
    author: "atrink"
  - subject: "Re: How is the top-10-list ordered?"
    date: 2004-08-03
    body: "Take a look at the bottom of the page. There is a legend for the graph.\n\nGreen = percentage of translated strings\nBlue = percentage of fuzzy strings\nRed = percentage of untranslated strings"
    author: "Christian Loose"
  - subject: "Re: How is the top-10-list ordered?"
    date: 2004-08-03
    body: "The bars are in percent, not in absolute numbers.\n\nAnd as a team can have more or less messages than it should have, you can have a total message number that is different. In that case, the green bar does not look sorted anymore (for example currently Hindi at position 27.)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: How is the top-10-list ordered?"
    date: 2004-08-03
    body: "German on PLACE 17 ? i thought may developers of KDE are in Germany , so why is the german translation this bad ?"
    author: "chris"
  - subject: "Re: How is the top-10-list ordered?"
    date: 2004-08-03
    body: "developers!=translators && motivation(coding)!=motivation(translating)"
    author: "Anonymous"
  - subject: "Re: How is the top-10-list ordered?"
    date: 2004-08-03
    body: "As everywhere in KDE, you need volunteers...\n\n(See http://i18n.kde.org/teams if you want to become a translator.)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: How is the top-10-list ordered?"
    date: 2004-08-03
    body: "What is the reason, that column 9 (\"total\") has different values for\nthe various languages?\nMaybe missing po-files?\n\nShouldn't be the value in column 9 the same for all languages?\n\nShouldn't be the percentage related to the maximum amount of messages available in the default language?\nIn this case the length of the bars would reflect the sorted list."
    author: "atrink"
  - subject: "Re: How is the top-10-list ordered?"
    date: 2004-08-03
    body: "No, the total should be what it really is, not what it is supposed to be.\n\nGood, for the bars, it would perhaps look better the other way round, but as far as I know, the statistics are mainly for the teams and were not meant for a \"hall of fame\", like this page.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Umbrello and Ruby support?"
    date: 2004-08-02
    body: "Anyone know if Umbrello CVS supports Ruby yet?\n\nI know someone was working on it, but couldn't contact the Umbrello team/author:\n\n  http://www.rubygarden.org/ruby?action=browse&id=RubyAndUML\n\nHas that changed?"
    author: "Lee"
  - subject: "Rosegarden"
    date: 2004-08-03
    body: "I would like to thank the Rosegarden team for their really astonishing piece of software. I tried out version 0.9.7 and found it excellent. I just wanted to communicate this as an encouragement to you. Thanks! \nThe only two limitations I have found are the midi set up on my notebook and when printing out, I was unable to change the size of the margins. The first one I'll try to solve in the next few weeks, the latter one you might have resolved already in 0.9.9. \nJust once again: thank you very much for your great programme!\nBTW: great idea with the sample files!\n\nMarkus"
    author: "Markus B\u00fcchele"
  - subject: "Re: Rosegarden"
    date: 2004-08-04
    body: "i second this, creating an application like rosegarden requires the most advanced GUI programming skills (widgets and painting), realtime programming skills, and deep knowledge of one (or no, two, err 3) media layers. \n\nthanks, and keep up the good work !"
    author: "ik"
  - subject: "KDEVibes"
    date: 2004-08-04
    body: "with those nifty desktop switching sounds, I might be trying to make cool melodies instead of getting work done.\n\nthe new sounds are excellent. "
    author: "standsolid"
---
In <a href="http://cvs-digest.org/index.php?issue=jul302004">this week's KDE CVS-Digest</a>: New KDE system sounds.
<a href="http://digikam.sourceforge.net/">Digikam</a> has a histogram viewer, new camera kioslave, image editor, HSL balance correction plugin.
<a href="http://www.koffice.org/kexi/">Kexi</a> adds two run modes; final mode and design mode.
New icons for <a href="http://developer.kde.org/~wheeler/juk.html">Juk</a>, Kommander, new splash screen for KDE.
Plus many bug fixes in preparation for the release.


<!--break-->
