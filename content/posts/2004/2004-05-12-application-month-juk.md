---
title: "Application of the Month: JuK"
date:    2004-05-12
authors:
  - "fmous"
slug:    application-month-juk
comments:
  - subject: "Juk is nice but is limited by its backend :("
    date: 2004-05-12
    body: "I wanted to switch to juk, but since it depends on artsd and artsd doesn't seem to honor the .alsarc file properly there was no way in using juk. \n\nBesides i don't see a point in this artsd stuff. What are the advantages of artsd compared to use libalsa directly? And don't tell me it is simultaneous playing of multiple sounds! Besides it is really a pain to have all these backends playing nicely together. Jack, artsd, (the gnome thingy which i forgot the name, etc.). So if somebody could enlighten me why artsd is there and how it can be configured to honour the default card or other alsarc stuff?\n\nCheers\nTim"
    author: "Tim"
  - subject: "Re: Juk is nice but is limited by its backend :("
    date: 2004-05-12
    body: "JuK also has an optional GStreamer backend."
    author: "Anonymous"
  - subject: "Re: Juk is nice but is limited by its backend :("
    date: 2004-05-13
    body: "which crashes far too much. :-(. sorry."
    author: "c"
  - subject: "Re: Juk is nice but is limited by its backend :("
    date: 2004-05-12
    body: "Because some of us are still using OSS, or some other way of playing sound. Myself, I use OSS because I can compile it into a 2.4 kernel (And I'm stuck on 2.4 because of my winmodem). Arts is used so juk etc. only needs one sound output method programming. Of course the downside is that for cross-wm apps like xmms it means an extra output method needs coding."
    author: "Michael Donaghy"
  - subject: "Re: Juk is nice but is limited by its backend :("
    date: 2004-05-12
    body: "aRts (and GStreamer -- which JuK also provides a backend for) handles decoding, none of the other sound daemons that you mention do."
    author: "Scott Wheeler"
  - subject: "Re: Juk is nice but is limited by its backend :("
    date: 2004-05-12
    body: "\"What are the advantages of artsd compared to use libalsa directly?\"\n\nThe answer is there in the title. Advanced _Linux_ Sound Architecture.\n\nYou make things plug straight into alsa and you rule out all platforms other than linux. Goodbye freebsd, openbsd, netbsd, solaris, hpux, irix..."
    author: "Robert"
  - subject: "Re: Juk is nice but is limited by its backend :("
    date: 2004-05-12
    body: "\"What are the advantages of artsd compared to use libalsa directly?\"\n\nOne huge advantage is that it doesn't limit KDE to just ALSA-fied Linux systems. KDE is a cross platform desktop. It works on Linux, Solaris, HPUX, FreeBSD, NetBSD, IRIX, etc. Only one of those uses ALSA..."
    author: "David Johnson"
  - subject: "Re: Juk is nice but is limited by its backend :("
    date: 2004-05-12
    body: "Ok, an abstraction layer, thats ok. But i think that this artsd settings dialog and probably backend needs some big improvements concerning alsa. I suppose a majority of users uses Linux as base for KDE, and a lot of people use alsa as sound driver. For example these settings work with xmms and alsaplayer but not with artsd:\n\npcm.!default {\n        type plug\n        slave.pcm \"ch40dup\"\n}\n\npcm.ch40dup {\n        type route\n        slave.pcm surround40\n        slave.channels 4\n        ttable.0.0 1\n        ttable.1.1 1\n        ttable.0.2 1\n        ttable.1.3 1\n}\n\nWhen i enable artsd i get the following error mesg: Unable to set interleaved.\n\nIt is also not clear how to select output devices defined in the .asoundrc.\nI tried the -D flag from alsaplayer in \"Weitere Benutzereinstellungen\" or under \"Eigene Hardwaredatei verwenden\" the name. Nothing worked :(.\n\nSome really great wishlist item for me would be a party mode juk with a play queue of the next songs.\n\nTim\n"
    author: "Tim"
  - subject: "Re: Juk is nice but is limited by its backend :("
    date: 2004-05-13
    body: "What's wrong with a playlist?"
    author: "Jason"
  - subject: "Re: Juk is nice but is limited by its backend :("
    date: 2004-05-13
    body: "Take a look at yammi (http://yammi.sf.net) for the difference between a play list and a play queue. I can tell you from personal experience that a play queue beats anything else (well, apart from a real DJ :)) for parties."
    author: "Mr. Fancypants"
  - subject: "Re: Juk is nice but is limited by its backend :("
    date: 2004-05-13
    body: "Well, it just doesn't do the same thing, and thus I tried to implement it some time ago. Anyways I found some problems I couldn't solve with my programming skills so I decided to leave it behind.. And yes, here is the URL for wish on bko: http://bugs.kde.org/show_bug.cgi?id=63260 - please vote for it if you like :)"
    author: "tpr"
  - subject: "I'd change to JuK"
    date: 2004-05-12
    body: "I'm using SuSE 8.2 and I'd love to change to JuK now that I upgraded to KDE 3.2, still sound skips with every KDE media program (I assume it is a aRts issue)... anyone knows a workaround?"
    author: "John JuK Wannabe Freak "
  - subject: "Re: I'd change to JuK"
    date: 2004-05-12
    body: "3.2.0 had many bugs. Most of multimedia applications hang. My own workaround was KDE 3.2.1."
    author: "buddel"
  - subject: "Re: I'd change to JuK"
    date: 2004-05-12
    body: "Thanks but I have 3.2.2 :-/"
    author: "John JuK Wannabe Freak "
  - subject: "Re: I'd change to JuK"
    date: 2004-05-12
    body: "I think it is to do with the buffering.  On my wireless network, I have an SMB mount and the mp3's skip with JuK.  On XMMS I just set a higher buffer (and not have it use aRts.  I suspend aRts for XMMS because it seems aRts is the reason for skipping?"
    author: "Geoff Rivell"
  - subject: "Re: I'd change to JuK"
    date: 2004-05-12
    body: "Xmms works fine here, I do think it doesn't use aRts by default.\nTried to change the size of the aRts buffer in KControl but it didn't help.\n"
    author: "John JuK Wannabe Freak "
  - subject: "Re: I'd change to JuK"
    date: 2004-05-12
    body: "Same here, XMMS works fine, but everything related to aRts is a disaster... tried 2.6.x and that made a BIG difference but not stable enough for day to day work... But it's a shame that we have to use XMMS as I find juK to be much nicer!\n\n/David\n"
    author: "David"
  - subject: "Re: I'd change to JuK"
    date: 2004-05-13
    body: "I use kde 3.2.2 and mdk-kernel 2.6.3-8.\n\nIt works fine most of the time, but skips when the harddrive is used heavily or I crunch the CPU. I used to use mdk-kernel 2.6.3-3 and updating really helped.\n\nStill, artsd is a piece of crap, IMO, and one of kde's weakest points.\n\nDo your bit to let the developers know how poor artsd is!\n\nvote for http://bugs.kde.org/show_bug.cgi?id=61737 and http://bugs.kde.org/show_bug.cgi?id=74744\n\n(it only takes a few moments to register!)"
    author: "Jake"
  - subject: "Re: I'd change to JuK"
    date: 2004-05-13
    body: "\nIt seems the general consensous of KDE develors is that artsd sucks--maybe not as an API, but for end-users.  I have no problem with having an abstraction layer, but artsd is very weak in lots of respects.  I pretty much don't use KDE for sound at all *because* of artsd.  I was using Noatun about a year ago, but I've switched to XMMS.\n\nFor compatibilities sake, would it be possible to use GStreamer as the abstraction layer?  I guess the API probably wouldn't be very friendly to us C++ folks."
    author: "Henry"
  - subject: "Re: I'd change to JuK"
    date: 2004-05-13
    body: "Just wrap the GStreamer C api into KDE-style C++ apis.  (I believe this has already been done, too.)  Then you have \"developer friendly\" to C++ folks.\n\nCheers!"
    author: "coulamac"
  - subject: "Re: I'd change to JuK"
    date: 2004-05-13
    body: "And then you have a wrapper library that has to be changed/fixed everytime the API changes underneath (Gtk-- anyone). Yeah!"
    author: "anonymous"
  - subject: "Re: I'd change to JuK"
    date: 2004-05-13
    body: "I forgot to mention, those KDE bug reports don't seem related.  They seem to deal with artsd CPU utilization.  And we know off-topic bug comments aren't taken seriously."
    author: "Henry"
  - subject: "Re: I'd change to JuK"
    date: 2004-05-13
    body: "Try the new 2.6.6 with the CFQ scheduler (append \"elevator=cfq\" to the kernel's boot parameters). Without it arts was useless for me as soon as the hard drive kicked in. Now it's working fine.\n\nCFQ is also in the -mm tree for the \"2.6.x, x<6\" series.\n\nHTH\nR."
    author: "Rom1"
  - subject: "Re: I'd change to JuK"
    date: 2004-05-13
    body: "> Still, artsd is a piece of crap, IMO, and one of kde's weakest points.\n\nWell, on the topic of JuK -- it's been noted in this thread and elsewhere that JuK has provided a GStreamer backend since its first release.  To be honest, almost everyone is too lazy to do it since it's a lot easier to just complain about aRts.  ;-)\n\n> Do your bit to let the developers know how poor artsd is!\n\n\"The developers\" are familiar with the problems with aRts (and randomly spamming bug reports won't help).  But we're stuck with it until at least KDE 4.  At that point we'll almost certainly be switching to something else, though there is still some debate on what exactly.  See the kde-multimedia development list archives for more."
    author: "Scott Wheeler"
  - subject: "Re: I'd change to JuK"
    date: 2004-05-13
    body: "turning on realtime-priority for artsd and making sure that the artwrapper binary is setuid root does wonders."
    author: "mETz"
  - subject: "Re: I'd change to JuK"
    date: 2004-05-14
    body: "Yeap, did all that and all was fine... but thank you all for your help and sorry about the (OT?) question."
    author: "John JuK Wannabe Freak "
  - subject: "not enough codecs supported"
    date: 2004-05-13
    body: "I love Juk and use it as much as possible.\n\nSadly, the backend, artsd is the weakest link. It skips and doesn't support .mpc, .ape or .wma despite plugins for all of these being available for xmms and mplayer. These are popular formats nowadays and I hate having to use a konqueror/xmms combo to play a lot of my music..."
    author: "Lilly"
  - subject: "Re: not enough codecs supported"
    date: 2004-05-13
    body: "thats my main problem with arts too... alot of my music is in musepack format, and I cant play it. and arts crashes alot, too - while it doesnt skip as much as xmms does for me. And I love Juk's interface. Nice it supports Gstreamer, but Gstreamer can't play mpc either..."
    author: "superstoned"
  - subject: "Re: not enough codecs supported"
    date: 2004-05-13
    body: "Probably because nobody knows about mpc :) wma could be played through xine_artsplugin but that probably needs a little tweak (the wma mimetype needs to be linked to that plugin)."
    author: "mETz"
  - subject: "Re: not enough codecs supported"
    date: 2004-05-13
    body: "TagLib would have to be extended to support reading tags from .wma as well. I think Scott's previously said that's not going to happen."
    author: "Stephen Douglas"
  - subject: "Re: not enough codecs supported"
    date: 2004-05-13
    body: "aye. lack of mpc support is the biggest kick in the teeth for me too. I love juk (especially the tag editting stuff) but xmms is just way more flexible. \n\nthrow in the artsd mess (3.2.2 keeps crashing arts) and buggy gstreamer plugin and it's amazing I use Juk at all. says a lot about how good the tag editor is."
    author: "c"
  - subject: "Re: not enough codecs supported"
    date: 2004-05-13
    body: "mpc was just very recently made OSS.  They haven't even done a release based on the OSS version yet and the version available from their source repository doesn't actually build on Linux.  I'll consider adding support for it once this stuff comes around."
    author: "Scott Wheeler"
  - subject: "Re: not enough codecs supported"
    date: 2004-05-13
    body: "their decoder (which is, I guess, actually needed for playback) has been OSS since the beginning, afaik... anyway, thanks for the work!"
    author: "superstoned"
  - subject: "Re: not enough codecs supported"
    date: 2004-05-13
    body: "I have a policy in JuK / TagLib of not implementing formats for which a F/OSS encoder does not exist (as I'm not particularly interested in encouraging the use of proprietary formats in an area where more open formats are dominant.).  Until quite recently the encoder was available in binary format only."
    author: "Scott Wheeler"
  - subject: "Re: not enough codecs supported"
    date: 2004-05-14
    body: "There is an mplayer and an xmms plugin available for .mpc files.\n\nWhat about .ape? It's an increasinlgy popular lossless codec. There license is a bit weird (you have to ask permission) but after that your good to go. There is active linux development and I believe that an xmms plugin exists."
    author: "Lilly"
  - subject: "Re: not enough codecs supported"
    date: 2004-05-14
    body: "...except that having to ask for permission is GPL incompatible.  Please see the above comment.  I don't see any reason to encourage the use of proprietary codecs when they're less popular than open formats.  FLAC is already more popular than APE and I don't know of any significant benefits that APE offers that would convince one to use it over something less restrictive.\n\nAnd in the case of lossless codecs, transcoding is an easy options since, well, it's lossless."
    author: "Scott Wheeler"
  - subject: "Re: not enough codecs supported"
    date: 2004-09-13
    body: "yep, lack of mpc support is the reason i still use xmms as well."
    author: "axion"
  - subject: "Needs Plugin Support."
    date: 2004-05-13
    body: "There's one reason why I'm still using XMMS instead of Juk.  XMMS has some awesome plugins - everything from visualizations to alarm clocks, as well as input and output plugins so you can input zillions of different file formats, and you can output to ALSA, ARTS, OSS, to a file, etc.\n\nMostly, I use the IMMS plugin (http://www.luminal.org/wiki/index.php/IMMS/IMMS) to automagically rate my music, play good music more often, etc.\n\nSo far, there is no plugin interface for JuK.  It needs one.\n"
    author: "Meldroc"
---
As part of the <A href="http://www.kde.de/appmonth/2004/juk/index-script.php">April 2004 issue</A> 
of the "Application of the month" series on <A href="http://www.kde.de/">KDE.de</A>, 
Andreas C. Diekmann has interviewed Scott Wheeler, author 
of <A href="http://kde-apps.org/content/show.php?content=10575">Juk</A>, an audio jukebox for KDE. 
Despite the downtime of the <A href="http://www.kde.nl/">Dutch KDE website</A> they are now 
offering an English translation of the <A href="http://www.kde.nl/apps/juk/en/interview.html">interview</A> 
as well as the <A href="http://www.kde.nl/apps/juk/en/">overview</A> of this issue for your reading pleasure.

<!--break-->
