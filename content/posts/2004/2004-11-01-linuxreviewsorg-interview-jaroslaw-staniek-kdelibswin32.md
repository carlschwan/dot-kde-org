---
title: "LinuxReviews.org: Interview with Jaroslaw Staniek on KDElibs/win32"
date:    2004-11-01
authors:
  - "binner"
slug:    linuxreviewsorg-interview-jaroslaw-staniek-kdelibswin32
comments:
  - subject: "Mark Hannessen"
    date: 2004-10-31
    body: "Being able to work without x11/cygwin really makes this project cool.\nI however must say that the programs look to kdeish for windows.\nNot that I dislike the kde look. it's just that it is out of place.\n"
    author: "Mark Hannessen"
  - subject: "Re: Mark Hannessen"
    date: 2004-10-31
    body: "I took a look again at kexi and just started to understand the coolness of having it on windows. This might be the first truely cross platform \"MS Access\" replacement."
    author: "Mark Hannessen"
  - subject: "Re: Mark Hannessen"
    date: 2004-10-31
    body: "Having Kexi on windows are probably cool on its own merits, but it's not the first truly cross platform \"MS Access\" replacement this title I would without doubt give to Rekall. An it has already been available for several years. It's proprietary on windows but the Linux version is GPL (not sure what the Mac version are).Looks like it starts to get included by distros now. It's in Contrib for MDK10.1."
    author: "Morty"
  - subject: "Rekall is not that great..."
    date: 2004-10-31
    body: "I tried Rekall once, and found it to be a terrible application. I had really expected much more from a program which was sold commercially for some time already. It was awkward to use: I remember that moving or resizing a form component was completely uninituitive and different from other similar applications. Also creating subforms was difficult, while I think this should be one of the most important features which should be simple to use, even by newbies. I also had the impression that the development of kexi was a lot more active than that of Rekall. So I have much more high hopes for Kexi which will be part of KOffice 1.4."
    author: "Anonymous Coward"
  - subject: "Re: Rekall is not that great..."
    date: 2004-11-01
    body: "<i>I remember that moving or resizing a form component was completely uninituitive and different from other similar applications.</i>\n\nIn what way. It does point, click'n'drag?\n"
    author: "mikeTA"
  - subject: "Re: Mark Hannessen"
    date: 2004-10-31
    body: "How ironic considering that KDE is still often called a Windows clone."
    author: "irony"
  - subject: "kdegames on Windows"
    date: 2004-11-01
    body: "Hope we can have kdegames on Windows soon then :)"
    author: "Stentapp"
  - subject: "Kontact"
    date: 2004-11-01
    body: "I'm currently testing Kolab, and when I read this I help thinking about Kontact. This would really be a fine application for windows. You could use the same outlook replacement throughout the organisation."
    author: "Debian"
  - subject: "great news :-D"
    date: 2004-11-01
    body: " This was really great reading! The more different ways we make OSS visible to the end users the better :-D\n\n It is especially nice to see that a country like Poland, which has traditionally \"lagged a little behind\" on the technical front, attacks the \"Micros~1 Entrapment Syndrome\" from the bottom-up.. If Polish companies continues this trend in general, they will face a lot less problems when the time is right for \"the full flip-over\". My guess is that in time it will give Poland what it really deserves: A good headstart on both the technical and financial level, which could become a much needed case-study for many \"old west european\" companies.\n\n Keep Hacking Guys(TM)... (Then i'll keep beta-testing)\n\n/me goes grab kexi4win32\n\nta-ta\n\n-Macavity"
    author: "Macavity"
  - subject: "What about kde-cygwin.sf.net porting Qt libs?"
    date: 2004-11-02
    body: "I thought it was a shame that the article didn't mention the work done in the http://kde-cygwin.sf.net/ project on porting GPL Qt libs to native Win32.\n\nJaroslaw simply said that he didn't think it was a good idea without mentioning that there is already work in progress towards this end.\n\nSee the qt3-win32 page at http://kde-cygwin.sourceforge.net/qt3-win32/ for the details. Significant progress has been made... This compiles on cygwin but also MinGW and MSVC++"
    author: "David Fraser"
  - subject: "Re: What about kde-cygwin.sf.net porting Qt libs?"
    date: 2004-11-02
    body: "The article is not a \"KDE-on-windows Survey\". Btw, I'm in good relations with kde-cygwin hackers, since their work is related to qt3-win32."
    author: "Jaroslaw Staniek (Kexi Team)"
  - subject: "Krusader port to Windows"
    date: 2004-11-10
    body: "I'm wondering if Krusader can run native on Windows without X11 and CygWin,\ni don't know if Krusader even runs with X11 and CygWin on Windows.\nTo be clear i prefer Linux, but on my job there is only Windows (:\nAnd some Krusader users has asked for a Windows port in the past.\nRecently we ported Krusader 1.40 to MAC OS X with kdelibs (3.1.4) / fink.sourceforge.net\nA MAX OS X port of Krusader 1.50 with kdelibs 3.2 is planned.\n\nbtw. i'm writing the documenatation for Krusader\n\nRegards,\n\nFrank Schoolmeesters\nhttp://krusader.sourceforge.net "
    author: "Frank Schoolmeesters"
  - subject: "Re: Krusader port to Windows"
    date: 2007-01-14
    body: "There is no Krusader for Windows, or at least not for now.  Total Commander, however, is very similar to Krusader.  Its homepage is here: http://www.ghisler.com."
    author: "Mark"
---
<a href="http://linuxreviews.org/">LinuxReviews.org</a> had an <a href="http://linuxreviews.org/features/2004-09-14_Jaroslaw_Staniek/">interview with Jaroslaw Staniek</a>. Jaroslaw works on <a href="http://wiki.kde.org//tiki-index.php?page=KDElibs+for+win32">making KDE applications running on MS Windows</a> without X11 and CygWin and also contributes to <a href="http://www.koffice.org/kexi/">Kexi</a>.

<!--break-->
