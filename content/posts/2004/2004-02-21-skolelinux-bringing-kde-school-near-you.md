---
title: "Skolelinux: Bringing KDE to a School Near You"
date:    2004-02-21
authors:
  - "wbastian"
slug:    skolelinux-bringing-kde-school-near-you
comments:
  - subject: "\"Kurt\""
    date: 2004-02-20
    body: "Could you tell me: do spelling variations of \"Kurt\" also qualify?\n\nTIA,\nCurd"
    author: "Curd"
  - subject: "Re: \"Kurt\""
    date: 2004-02-23
    body: "I guess Kurd would qualify, also Qurt and Qurd. Then C++urd and C++uurt will certainly qualify as well. So if you are a object oriented, then yes, surely."
    author: "ac"
  - subject: "good stuff"
    date: 2004-02-20
    body: "<a href=\"http://www.skolelinux.no/testskoler/map/skolelinux-norway.png\">\nhttp://www.skolelinux.no/testskoler/map/skolelinux-norway.png</a><br>\nis a very nice map, good work skole guys :)\n"
    author: "anon"
  - subject: "Security Updates"
    date: 2004-02-20
    body: "<i>Now that KDE 3.2 is backported to Debian-stable, it would be nice if the security patches could be maintained for it just as it is done in Debian-stable itself. </i>\n\nI could be mistaken, but I think Ralf Nolden (actually his employer) needs updated packages and so he is rebuilding the backports when security fixes become available.\n\nCheers,\nKevin"
    author: "Kevin Krammer"
  - subject: "Wonderful project"
    date: 2004-02-21
    body: "I just installed this debian-based distro and it's delightfully complete and well-geared towards larger organizations. Great showcase for KDE!"
    author: "Simon Skrede"
  - subject: "heh"
    date: 2004-02-21
    body: "\"KDE is the desktop, and it gives us the translation tools (kbabel). We still use KDE 2.2.2 as that's in Debian Woody 3.0r2 - with some kde-edu applications, kmail, KOffice and more. \" \n\n\"Personally I use Debian-unstable with KDE 3.2 - which is wanted by many schools in Norway, but we cannot release in Skolelinux because of various security and maintenance issues.\"\n\nHehehe, fucking retards.. Living on the edge with KDE 2.2 eh :p Crazy, why not use KDE 1.0! even less bugs in that i guess :p"
    author: "anoni"
  - subject: "Re: heh"
    date: 2004-02-22
    body: "Not \"living on the edge\", but using a stable, secure and well-maintained desktop. We'll switch as soon as a newer KDE is in Debian stable, with a guaranteed painless upgrade. And believe it or not: KDE 2.2.2 is a really nice and capable desktop. As long as we don't rely on the new and advanced features of KMail, Konqueror etc., we might as well stick with it. Our manpower is limited, so anything that enables us to spend more time on the important issues is welcome.\n\nBy the way, the Skolelinux team is responsible for the KDE translations into Norwegian Nynorsk, Norwegian Bokm\u00e5l and Northern Sami, so we do our part for the KDE project as well.\n\nGaute Hvoslef Kvalnes, Skolelinux"
    author: "Gaute Hvoslef Kvalnes"
  - subject: "Re: heh"
    date: 2004-02-22
    body: "Well like he says, KDE 2.2 has been around for a while and this is important when you are targetting a particular group of users. There are issues with later versions of KDE, which are important, and those have to be ironed out."
    author: "David"
  - subject: "Skolelinux in WHV"
    date: 2004-02-21
    body: "I like Skole, this is by far one of the best Debian distributions! It provides so many features other userlinux people just talk about.\n\nPlease don't forget to use your Fosdem stay in Brussels to contact your MEPs because of the new IPR enforcement draft and the recorrection of the swpat directive. See latest news: http://www.ffii.org.uk. I would also like to remind you of the April Free Information Infrastructure 2004 conference in Brussels (see: http://plone.ffii.org)\n\nParallel to the Event in Chemnitz a similar event will take place in Wilhelmshaven, Germany (close to dutch border): Gnu/Linux Informationstage 2003. 6/7 march. Also speech streaming with Chemnitz is planned (See http://www.lug-whv.de).\n\nPersons involved in the Skolelinux project will be there, too. \nhttp://www.lug-whv.de/?menu=202004000103\n\nTimetable draft: Gro\u00dfer H\u00f6rsaal, Sonntag, March 7\n[..]\n\t\n10:00-11:00\tKDE 3.2 \tTill Adam \tKDE\n11:00-12:00\tGNOME - Ger\u00fcstet f\u00fcr die Zukunft \tSven Herzberg \tGNOME\t\n12:00-13:00\tContent Management und eBusiness mit OpenSource \tThorsten Harms \tArche AG\n13:00-14:00\tStarOffice / OpenOffice.org \tThorsten Keller \tSUN Microsystems\n14:00-15:00\tSkolelinux Schul-Server \tBart Cornelis/Bastian Veltin \tSkolelinux\n[..]\n\nI will speak on the same day for FFII about the state of the EU swpat directive (I was born and raised in WHV, and I'm registered there as one of 81 000 (??) citizens).\n\nAnd don't forget the Friesathlon workshop, an intresting effort to translate KDE into Plattd\u00fc\u00fctsch that will take place on both days."
    author: "Andr\u00e9"
  - subject: "Re: Skolelinux in WHV"
    date: 2004-02-21
    body: "sorry: 2004!!\n\n"
    author: "Andr\u00e9"
---
<a href="http://www.skolelinux.no/index.php.en">Skolelinux</a>
is an ambitious project that aims to provide schools
with a flexible and low-cost IT solution based on Linux and KDE.
I <a href="http://edu.kde.org/interviews/skolelinux.php">interviewed
Bart Cornelis, Kurt Gramlich, Conrad Newton and Knut Yrvin</a> to
learn more about Skolelinux and the role that KDE plays in it.
You can meet developers from Skolelinux at
<a href="http://www.fosdem.org/">FOSDEM</a> and
the <a href="http://www.freeedem.org">FreeEDEM-conference</a>
in Brussels, Belgium this weekend (February 21-22, 2004)
and later this year at the
<a href="http://www.tu-chemnitz.de/linux/tag/2004/allgemeines/">
Chemnitzer Linux-Tag</a> in Germany (March 6-7, 2004).

<!--break-->
