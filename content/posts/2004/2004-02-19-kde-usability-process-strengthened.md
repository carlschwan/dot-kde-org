---
title: "KDE Usability Process Strengthened"
date:    2004-02-19
authors:
  - "aseigo"
slug:    kde-usability-process-strengthened
comments:
  - subject: "Leading Open Source Desktop???"
    date: 2004-02-19
    body: "\"As the leading Open Source desktop\"\n\nCan we really be making statements like this? I don't recall seeing proof of this anywhere, and in direct contradiction to this statement, it seems like we're getting trounced by Gnome on the business desktop. I wish the authors of articles with opinions such as these would think a little before making such sweeping statements."
    author: "Gil"
  - subject: "Re: Leading Open Source Desktop???"
    date: 2004-02-19
    body: "Do you have any proof that it's not the case? GNOME doesn't trounce KDE. And the business desktop is only a specialized task."
    author: "Anonymous"
  - subject: "Re: Leading Open Source Desktop???"
    date: 2004-02-19
    body: "While Gnome has been getting more press than KDE in the business desktop as of late; the numbers I see from Linux usage polls strongly indicate the KDE is preferred by almost 2-to-1 over Gnome.  Sun, Ximian, and Redhat may be pushing Gnome but that does not mean users are choosing that desktop.  I work for a state government in the US and even though every agency I know of in my state (that has Linux desktops) chooses Redhat, but every one of them without exception make KDE their default desktop."
    author: "brockers"
  - subject: "Re: Leading Open Source Desktop???"
    date: 2004-02-20
    body: "Indeed. In Paris Linux Solution, many red hat computers were running KDE. Kind of ironic..."
    author: "Philippe Fremy"
  - subject: "Re: Leading Open Source Desktop???"
    date: 2004-02-20
    body: "It's a natural adaptation to customers' requests."
    author: "Anonymous"
  - subject: "Re: Leading Open Source Desktop???"
    date: 2004-02-20
    body: "Really?  I guess not the computers at the Red Hat booth?\n\nThe ones that were running KDE, were they using the BlueCurve defaults?  I find it very surprising that people would go out of their way to run KDE instead of GNOME on Red Hat...  It would be interesting to find out why."
    author: "Navindra Umanee"
  - subject: "Re: Leading Open Source Desktop???"
    date: 2004-02-20
    body: "You don't really have to go out of your way.  Just download the RPMs from:\n\nhttp://kde-redhat.sourceforge.net/\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Leading Open Source Desktop???"
    date: 2004-02-19
    body: "Actually, there are a large number of reports showing that KDE is used on more desktops than Gnome. Ratios vary depending on what you read and the methodology used, but around 2/3 are KDE.\n\nAnd the PR about the business desktop is somewhat unfair, because while Gnome is doing well with Redhat and Sun, it needs to be noted that the majority of these features are US-only, where multiple corporations have a tendency to just pick one large supplier (hurd theory... Why they want RH to be the next Microsoft, I have no idea.). Outside of the US, KDe has far more traction. Unfortunately, the tech press and the online world, to a large extent, are still US-centric."
    author: "Luke Chatburn"
  - subject: "Re: Leading Open Source Desktop???"
    date: 2004-02-19
    body: "Well Spoken(TM)!\n\n Attention needs to be payed to the fact that the non-hacker type of user actually *like* KDE whereas most of them have more of a \"i *could* get used to this\" kind of attetude towards GNOME.\n\n And before someone starts shouting about proof, i better mention that i have converted quite a number (30+) of non-gamers to OSS. The server-admins/webdevelopers mostly went with GNOME because it screams \"Im not windows\", but the regular users found KDE a bliss because it isnt afraid of doing what works (and this in some cases means \"lending\" thoughts and ideas from windows).\n\n It would be fun to see a \"US vs. Europe and Freinds\" desktop poll. Ill bet my old Dual PII-233 that KDE winns ;-)\n\n/kidcat"
    author: "kidcat"
  - subject: "Re: Leading Open Source Desktop???"
    date: 2004-02-19
    body: "US vs Europe and Friends? Huh?"
    author: "ML"
  - subject: "Re: Leading Open Source Desktop???"
    date: 2004-02-19
    body: "Probably there is more bias towards KDE support in Europe. That may be the reason why they lack behind. \n\nNo EU vs. US bashing, please. The enemy are the capitalist commis in China. "
    author: "Gerd"
  - subject: "Re: Leading Open Source Desktop???"
    date: 2004-02-19
    body: "\" Probably there is more bias towards KDE support in Europe. That may be the reason why they lack behind. \"\n\nObviously, they don't <b>lag</b> behind in spelling."
    author: "Joe"
  - subject: "Re: Leading Open Source Desktop???"
    date: 2004-02-19
    body: "Yeah, make fun of the people who don't speak Engrish natively :-)"
    author: "anon"
  - subject: "Re: Leading Open Source Desktop???"
    date: 2004-02-19
    body: "Maybe he means gnome users have small buttocks? :-)"
    author: "Roberto Alsina"
  - subject: "Re: Leading Open Source Desktop???"
    date: 2004-02-19
    body: "LMAO!!"
    author: "Bruce"
  - subject: "Re: Leading Open Source Desktop???"
    date: 2004-02-19
    body: "Perhaps, what you really meant to say was that they were leading due to no real corporate bias?"
    author: "a.c."
  - subject: "Re: Leading Open Source Desktop???"
    date: 2004-02-19
    body: "I think saying \"leading\" might be over the top, but in actual deployments, both City of Largo, Florida and Munich have chosen KDE. GNOME might have support of more vendors, but for the time being, it's the corporations using Linux to have the final say and KDE is not doing badly at all in that respect."
    author: "David Siska"
  - subject: "Re: Leading Open Source Desktop???"
    date: 2004-02-19
    body: "How many distros have gnome as the default and how many have KDE?  Redhat was the biggest gnome default and they dropped desktop support for it.  I know others push gnome but the bigs still push KDE.  Lindows, Suse, etc...  I would say that makes KDE the current leader."
    author: "AM"
  - subject: "Re: Leading Open Source Desktop???"
    date: 2004-02-19
    body: "Heh... That's Distrowatch data mining territory, really.\n\nRedhat and Sun's Java Desktop are the main proponents of Gnome-only desktop systems. The Gnome variants of Knoppix also.\n\nFor KDE, Lindows, Xandros, Knoppix, Mepis, Arc Linux are KDE-only.\n\nMandrake, SuSE are KDE-first distros which still ship Gnome too, and a good implementation at that. Debian, Slackware and Gentoo are fairly neutral.\n\nI must say that I ardently disagree with distros which ship only one DE, and it is nice to see, after RH leaving the desktop market with their Gnome-only offering, Fedora working hard to produce good implementations of both KDE and Gnome.\n\nOn balance, for desktop distros it's mostly KDE+Gnome and KDE-only."
    author: "Dawnrider"
  - subject: "Re: Leading Open Source Desktop???"
    date: 2004-02-19
    body: "Actually, RH is back with this:\n\nhttp://www.newsforge.com/os/04/02/11/1549214.shtml"
    author: "Gagnefs Tr\u00e4skodansarf\u00f6rening"
  - subject: "Re: Leading Open Source Desktop???"
    date: 2004-02-19
    body: "actually, Red Hat started shipping officially supported KDE packages starting with Red Hat 8. in contrast, previous to that the KDE packages in Red Hat were basically unsupported contrib packages (thanks Bero! we still remember and luv ya for it!). so Red Hat, though GNOME-friendly, have increased their support for KDE in recent years. =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Leading Open Source Desktop???"
    date: 2004-02-20
    body: "RH8 KDE was an abomination :) Bero's packages were much better and the RH9 version was hardly much better.\n\nFor the users, their experience of KDE was reduced for 8 and 9, so I don't count it as progress over Bero's hard work :)"
    author: "Dawnrider"
  - subject: "Re: Leading Open Source Desktop???"
    date: 2004-02-19
    body: "> it seems like we're getting trounced by Gnome on the business desktop.\n\nNo such trouncing has happened.  It's true that GNOME has had better PR on the topic in the North American market and could possibly even be argued that GNOME is poised to be stronger in that area, but the fact is that the actual switchovers haven't happened and in fact overwhelmingly are still on the drawing board.\n\nIn the parts of the world where transitions on the business desktop are actually starting to happen (though it's still just the very beginning) -- notably Europe -- KDE is dominant for both business and home usage and mind share.\n\nIt's unclear how things will play out in North America, but so far it's been mostly a PR circus and a lot of buzzword bingo; what will be really interesting is to see what's going onto these systems once any sort of large scale migration starts.\n\nKDE *is* currently the leading Open Source desktop.  With the phenomenal growth of OSS on the desktop there's always the possibility that anything could happen, but right now KDE is on top and continuing to innovate."
    author: "Scott Wheeler"
  - subject: "Re: Leading Open Source Desktop???"
    date: 2004-02-20
    body: "Remember North America has Lindows and Xandros and Gnome isn't even an option on those 2 major North American desktop Distros. Gnome's desktop domination in reality exists only in the Minds of the FSF fanatics and the PR spin doctors at Novell/Ximian"
    author: "Kraig"
  - subject: "Re: Leading Open Source Desktop???"
    date: 2004-02-20
    body: "Only in the minds of FSF fanatics who are not up-to-date. Point them to Stallmann's latest KDE utterances and the \"Why not LGPL\" document."
    author: "Anonymous"
  - subject: "Re: Leading Open Source Desktop???"
    date: 2004-02-20
    body: "I share the ethical approach of the FSF && I use (and contribute to) KDE. In fact I believe KDE is closer to the FSF philosophy than GNOME.\n \nBTW, isn't it a bit fanatic calling `fanatic' those you don't agree with?"
    author: "Peter Plys"
  - subject: "Re: Leading Open Source Desktop???"
    date: 2004-02-20
    body: "Generalizing is wrong everytime, but calling RMS and some of his FSF pals fanatics is realistic."
    author: "Roberto Alsina"
  - subject: "Re: Leading Open Source Desktop???"
    date: 2004-02-20
    body: "And considering how careless the rest of the world is we better hope they stay fanatic. =P"
    author: "Datschge"
  - subject: "Re: Leading Open Source Desktop???"
    date: 2004-02-20
    body: "Opinions, opinions... what's that clich\u00e9 about opinions.... ;-)\n\nNow, everyone is free to be as fanatical as they want about whatever, and since the FSF has basically left the software world behind years ago[1], they are a non-issue for me anyway.\n\n[1] They develop no software whatsoever, AFAIK"
    author: "Roberto Alsina"
  - subject: "Re: Leading Open Source Desktop???"
    date: 2004-02-19
    body: "it all depends on whether you decide to listen to the PR efforts of people with vested interests in GNOME (the recent Novell / Ximian tempest-in-a-teapot comes to mind) or whether you wish to concentrate on current realities. if the latter, then KDE is indeed the leading Open Source desktop not only in usage but also in features, third party development and industry recognition (aka awards).\n\npersonally, i'm more content to concentrate on KDE's usability for the moment since that is what this announcement is about. i'm not sure when it became politically incorrect to be openly proud and accurate about the success of KDE, but i don't really care either. viva la KDE! viva la usability! =)\n\nbtw, have you sent me your web browser toolbar usage stats yet? ;-)"
    author: "Aaron J. Seigo"
  - subject: "Re: Leading Open Source Desktop???"
    date: 2004-02-20
    body: "In correct Castilian (Spanish) it would be:\n\u00a1Viva KDE! \u00a1Viva la usabilidad! :-)"
    author: "Quique"
  - subject: "Re: Leading Open Source Desktop???"
    date: 2004-02-20
    body: "Oh yeah! :-)\n\nhttp://www.pycs.net/lateral/weblog/2004/02/19.html#P159"
    author: "Roberto Alsina"
  - subject: "Re: Leading Open Source Desktop???"
    date: 2004-02-20
    body: "\"it all depends on whether you decide to listen to the PR efforts of people with vested interests in GNOME (the recent Novell / Ximian tempest-in-a-teapot comes to mind) or whether you wish to concentrate on current realities. if the latter, then KDE is indeed the leading Open Source desktop not only in usage but also in features, third party development and industry recognition (aka awards).\"\n\nYer, exactly. I've not seen others hesitating over the opportunity to predict KDE's demise and hype Gnome for their own ends."
    author: "David"
  - subject: "Re: Leading Open Source Desktop???"
    date: 2004-02-19
    body: ">it seems like we're getting trounced by Gnome on the business desktop.\n\nhummmm. Other than the PR (FUD?) coming from Sun and Ximian, I see KDE everywhere. In fact, if I were starting a new distro, I would probably go with KDE so that I would not have to take on these 2 companies directly. "
    author: "a.c."
  - subject: "Re: Leading Open Source Desktop???"
    date: 2004-02-20
    body: "Thats a joke right?"
    author: "Kraig"
  - subject: "1st impression counts"
    date: 2004-02-19
    body: "It is more \"look and feel\".\n\nUsually the usability problem is that text is cut or the widget are not fixed. You could see it in the desktop settings dialog, all tabbed setting panels had a different size, so the main window resizes.\n\nAnother issue is the lack of example files. When you first start a program you want to play around a little bit. So good example files are needed, a birthday card printout.\n\nAnd of course a mature \"theme manager\" is needed.\n\nIt is not always easy to find the features, it was very difficult for me to remove a top Macstyle bar. when this feature is enabled often applications pop up so you cannot get them with the mouse anymore.\n\nProgram descriptions are sometimes unusable for newbies. What's a po file editor good for. The description does not help you.\n\nI would suggest a talkback option for KDE test installations with an event logging. So you can analyse what was done by a newbie. Which options were used and how often. Put in in a datawarehosue and then analyse the user behaviour. \n\nOr start certain difficult cases and look how it is done by the user. \n\nMore complicated things like \"format a floppy disc\" or write a letter and save it to a memory stick."
    author: "Holler"
  - subject: "Re: 1st impression counts"
    date: 2004-02-19
    body: "there are various issues with \"talkback options\" in a project such as KDE, but you hit on many of the issues that we will be addressing. hopefully when KDE4 arrives you'll won't be able to write such a long list of issues so easily =)"
    author: "Aaron J. Seigo"
  - subject: "Re: 1st impression counts"
    date: 2004-02-19
    body: "> What's a po file editor good for. The description does not help you.\n\nAssuming you're talking about KBabel, it's description is \"Translation Tool\". And newbies install the development tools package?"
    author: "Anonymous"
  - subject: "Re: 1st impression counts"
    date: 2004-02-19
    body: "\"And newbies install the development tools package?\"\n\nOfcourse your absolutly right, but from the experience\nwith newbies to linux, I have seen that roughly the half\nof them install everything on there first install.\n\n(The other half of them are girls, and they let me do it for them ;-)"
    author: "pieter"
  - subject: "Great news!"
    date: 2004-02-19
    body: "As someone who has been on the kde-usability mailing list for some time, this sounds like excellent news. The usability process is in desparate need of some formalisation, sicne at the moment the list is a barrage of opinions and not very much research.\n\nIt will still, no doubt, be useful to have many of the discussions already going on on the kde-usability list; a lot of good ideas have been dreamt up and worked on there. But it will also be good to get a sense of direction and policy from a more professional body of opinion."
    author: "Tom Chance"
  - subject: "Mailing list"
    date: 2004-02-19
    body: "Mailman support of Kmail has to improve. KMail shall be able to analyse automatic response mails by the list and be able to automatic forward your subscription password to KWallet. Automatic folder creation, automatic subsription, I would like to see a \"one-click\" mailing list subscription."
    author: "Holler"
  - subject: "Re: Mailing list"
    date: 2004-02-19
    body: "Please use http://bugs.kde.org and don't post off-topic."
    author: "Anonymous"
  - subject: "Application/UI Design Review Site"
    date: 2004-02-19
    body: "This is most interesting news. I have recently been mulling proposing starting a new kde/qt site which offers reviews of application user interfaces at a very early stage in development (aka before milestones are reached). I think we are finding the design flaws in applications when it is too late, and as a result considerable extra effort is involved fixing UI's up compared to the work corporate competitors must go through if they do pay attention to such detail. All the same, my idea is not orthogonal to this news and I'll outline it here and see what feedback it gets.\n\nI propose we construct a web site which provides reviews of KDE and Qt applications as early stages of their development cycles. Teams of reviewers would review submitted applications and provide a single review which covers a set of points providing constructive critisim. This process is similar to that which takes place when papers are submitted to journals for publication. By having multiple people review an application, then get together and combine their reactions into a single review, noise in the data is averaged out. You don't get those \"I love KDE, it's so great, everything in this new version is an improvement\" or \"KDE sucks. Gnome rules! Troll on!!\" reviews this way. Furthermore, all criticism should come with constructive suggestions. For example, I dislike the clutter in Konqueror's menu system, but if I were writing a review of Konq I would sketch out and scan in, or whip up in Qt Designer an alternative organization of menu entries. As a developer, critism of my work is nice, but suggestions of method for improvment is so much more helpful. The obvious question is will this work? As a developer of an open source app (Album Shaper) I would LOVE to get constructive critisim from a larger audience. Currently all user interface feedback I get from asking people I live with for their ideas. Furthermore, I would be intersted in review other software and giving authors ideas for better layout and interaction with their applications. What is more interesting is that reviewers ideally are nonprogrammers IMHO. Thus we can pull in a large resource that currently is unable to provide constructive feedback without getting too involved.\n\nWhat do people think? If I get some feedback on this idea I'll see if I can make it happen by sending it to a few of the higher ups in the kde community. I strongly believe this resource should be provided to all KDE and QT applications. Qt applications are cross platform and are the first glimpse many computer users will first have of open source development at its best, all the more reason to impress them, then lure then to KDE. :) \n  \nPlease respond!\nWill Stokes, Album Shaper maintainer (http://albumshaper.sf.net)"
    author: "Will Stokes"
  - subject: "Re: Application/UI Design Review Site"
    date: 2004-02-19
    body: "I think this is an excellent idea. Go for it, I would say. I will be happy to provide some reviews."
    author: "Raf Willems"
  - subject: "Re: Application/UI Design Review Site"
    date: 2004-02-19
    body: "suggestions for improvements are welcome, however... those suggestions should be rooted in a deep understanding of how the application works, the goal of the application and backed with actual testing. otherwise we end up with lots of not-all-that-useful opinion slinging, with most opinions being retrograde despite their author's best intentions =)\n\nif you have a serious interest in usability work and are willing to put effort into it, i would sincerely invite you to join the efforts on kde-usability-devel rather than create a separate project since that will likely mean more work getting done with less conflicting goals in the end. i think our goals are quite similar, with a usability portal website being seen as a key component."
    author: "Aaron J. Seigo"
  - subject: "Re: Application/UI Design Review Site"
    date: 2004-02-20
    body: "Hi\n\nI have been trying to contact you for quite sometime now. email to you just bounces back so I hope you will answer me here. what happened to the kde hig project which you were talking about earlier. I am writing a kde dictionary with a list of common terms used in kde. would anybody else join me?\n\nregards\nrahul"
    author: "Rahul Sundaram"
  - subject: "Re: Application/UI Design Review Site"
    date: 2004-02-20
    body: "which address are you using? aseigo at kde.org and aseigo at olympusproject.org both work just great ... i did have problems from Dec 29-Jan 1 with email delivery, though .. hrm... make sure you're spelling it right ;)\n\nin any case.. HIG.. yes, the KDE UI guidelines are going to be updated as part of the new kde-usability-devel project pretty thoroughly. the current guidelines are good, but a lot has happened between now and then and there are a lot of things not covered in the current guidelines that need to be...\n\nas for the \"KDE dictionary\", is this the one of terms commonly used in KDE's UI? things like \"Folder\", \"DCOP\", etc? if so... that may work quite nicely with the new project as well since we know we have to start codifying some of the wording and terms so that they are consistent. please... subscribe to kde-usability-devel =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Application/UI Design Review Site"
    date: 2004-02-19
    body: "These are grand ideas, but as Aaron said it depends a lot on knowing the target audience of the application and what the application must do. I've had good ideas from people knowledgeable in usability as well as some that weren't really workable due their lack of familiarity with the target.\n\nEverything that happens here has to happen with the developers and the development process. It also ought not overload the developers. I tend not to put a lot of focus into usability as a big project. I let Aaron do that. ;-) What I do focus on is our targets. Here is how I achieve usability...\n\n1) I identify key principle factors as a series of tests for anything we change or add to the interface. These include first of all efficiency, relative value and adherence to our philosophies of respecting the individual... This is actually a round table process on the development list.\n2) We have a user list which has a lot of user running CVS. These users provide preliminary feedback and help us to refine and prioritize.\n3) I have a feature request page. The truth is many requested features are in newer versions or the user just missed them. Many requests are not all that practical and some are... confused. On occasion, like sifting ore for diamonds, a really good one comes in and we work on it.\n4) Most of our key developers are heavy users.\n\nMost usability comes down to having a good grasp of what you want to accomplish and how people work. A good tool is an extention of yourself and in using it you forget your tool and extend yourself into your task. Usability is about the people using the software so establishing a feedback link with your users is very important. Of course you conform to design standards. That leaves a few small issues that another set of eyes trained to look for the ugly think you got used to seeing on top of the refrigerator and pointing it out.\n\nThere is a point at which you end up with less results to reach perfection in the process. To help a program improve it's usability you need to develop an interaction with the developers, but they need to set it up. Specific application improvement needs to filter to the applications the developer is working on and the user is stress testing. Casual use can miss problems and too many things to think about can hamper development."
    author: "Eric Laffoon"
  - subject: "KDE style should have attention as well"
    date: 2004-02-19
    body: "It is good that KDE tries to take usability seriously, but I am slightly skeptical. Out of experience from what I have seen here and on the mailinglists the developers\n\na) Are in general opposed to do the needed changes\nb) Lack the needed understanding and vision pursue such goals\n\n\nChanges do happen, but reluctantly, and only with a lot of kicking and screaming. Gnome do indeed have the right vision and seems to be genuinly committed do needed changes. So my guess is that KDE will never be able to catch up, and as Gnome gets technically better, it will be the winner of the desktop wars. With hindsight history will probably say that it has already won at this point.\n\nBut that was actually the semi-trolling part (is it trolling if it is sincerly meant?).  What I really had to say is that style is a different element that needs attention. The KDE desktop has a visual style communicates with geeks and engineers. The style should communicate to the mainstream user and her values. That would mean friendlier, more elegant, and more informative application names, style-elements that speaks to the office workers; it should convey an impression of friedliness, empowerment and ease of use. Another part of style is general cleanliness and harmonization of fonts, proportions and styles. \n\nI'm sure this is all sounds very abstract, but an idea would be to mail a few  agencies for advertising or graphic design, or perhaps professors on the relevant academies, and ask for volunteer advice on how the desktop should look..."
    author: "will"
  - subject: "Re: KDE style should have attention as well"
    date: 2004-02-19
    body: "> So my guess is that KDE will never be able to catch up, and as Gnome gets\n> technically better, it will be the winner of the desktop wars.\n\n:-)\n\nGood joke\n\n"
    author: "Tim Gollnik"
  - subject: "Re: KDE style should have attention as well"
    date: 2004-02-20
    body: "Nope, not a 'good joke' at all.\n\nGNOME's technology may be nearing abysmal right now, but they ARE working hard (well, partly because there's no other way to get any kind of result with the GNOME API, granted), and now that they've got D-BUS and other KDE-inspired technologies to build on, they may very well begin to catch up. It's not a as much a matter of time, as one of man-hours, and given that Miguel de Icaza certainly makes up a certain lack of engineering competence with lots of excellent connections in the industry, accumulating man-hours is much less of a problem for GNOME than KDE.\n\nSee Evolution for an excellent example of what I mean. Totally HUGE piece of software, does loads and loads of stuff manually, that would be automatic with the KDE API, etc. Yet they (literally) poured millions of investor dollars into it, hired a bunch of developpers to work on it full time, and there you have Evolution: poorly built application that works pretty darn well.\n\nIn short, if we (KDE developpers and other contributors) remain sitted on our asses, other alternatives WILL eventually catch up.\n\nSo I wouldn't call it a 'good joke' at all, no. Because they're not playing in the same league as KDE right down doesn't mean KDE should believe itself the winner forever."
    author: "Anonymous Coward"
  - subject: "Re: KDE style should have attention as well"
    date: 2004-02-20
    body: "And how exactly is Evolution poorly built. Can you clarify that please? It seems you are stating an opinion on something you have no knowledge of, and immediately assuming that opinion to be true, and then coming up with a conclusion. That, my 'friend', is called \"begging the question\"."
    author: "Maynard"
  - subject: "Re: KDE style should have attention as well"
    date: 2004-02-20
    body: "> And how exactly is Evolution poorly built.\n\nJust guessing, it's not built from components? AFAIK its developers plan to break it up so you can start mail-only or organizer-only applications."
    author: "Anonymous"
  - subject: "Re: KDE style should have attention as well"
    date: 2004-02-20
    body: "And GNOME/GTK is doing rather well for an environment that is really poor. I can now count at least 3 IDEs that use GTK or GNOME. Anjuta, Monodevelop, Scaffold, plus Eclipse which can be built using GTK.\n\nGNOME technologies work, and that is why people use them, duh. And KDE ones work well too. This whole thing gets really ridiculous when people start making unsubstantiated statements like the one I am replying to. Do not fan flame wars by saying stuff that is unnecessary or outright false."
    author: "Maynard"
  - subject: "Re: KDE style should have attention as well"
    date: 2004-02-20
    body: "It's an irony of life that \"integration\" is the harder to do right the more benefits from integration there actually are."
    author: "Datschge"
  - subject: "Re: KDE style should have attention as well"
    date: 2004-02-19
    body: "thanks for giving me one more reason to work hard on KDE. who said ignorance had no value? ;-)\n\nseriously, i appreciate your skepticism, but your broad-stroke painting of the KDE project is dead wrong. there are those who don't grok usability or even care much about it. that's true in every project, including GNOME.\n\nthere are many KDE developers interested in and working towards a more usable KDE. how do you think this project started, exactly? this emerged from discussions between several people at the Kastle conference in August; this isn't my brain child, but the desire of others currently involved in KDE to join in an make KDE better when it comes to usability.\n\nthe goal is to improve the processes, and we have more support to do just that than ever before."
    author: "Aaron J. Seigo"
  - subject: "Great News "
    date: 2004-02-19
    body: "It is great news. The Usability list sometimes look like a big mess with never-ending discussion rehearsing the same points every couple of months. But I've found that there was a sort of auto-organization process with people starting to do more work than talk, eg the work of Beno\u00eet Walter on KCM modules, the propositions of Frans Englisch and the toolbar effort of Aaron. \n\nThe new devel list will push forward this process of auto-organization. We need more people that take care of finding all inconsistencies in toolbar, menus. Critical eyes on help messages can also be very valuable to push clarity and consistency. If we have volunteers (don't need to be a programmer) who specialize in even tiny janitorial works on usability and believe in step-by-step improvements, we will really be more productive than endless talks about one-click vs double-click, MDI vs SDI. We will then succeed in open-sourcing usability."
    author: "Charles de Miramon"
  - subject: "Re: KDE style should have attention as well"
    date: 2004-02-19
    body: "> Changes do happen, but reluctantly, and only with a lot of kicking and\n> screaming. Gnome do indeed have the right vision and seems to be genuinly\n> committed do needed changes. So my guess is that KDE will never be able to\n> catch up, and as Gnome gets technically better, it will be the winner of the\n> desktop wars.\n\nSo Gnome has the right vision? Why? I think Gnome got blinded by Suns usability study and now think it's the Holy Grail, thereby alienating itself from both their developers and users. Of course, usability is important and KDE developers will need to realize you can't just throw every possible feature in a GUI. But still, though some changes may be painful for developers they are definitely welcomed by our users (seen the positive reactions to usability improvements in KDE 3.2?). As long as we've got good developers focusing on good features and others who will fit those features nicely into the GUI we will have a very balanced desktop. So no, KDE won't be able to catch up, we'll just keep our lead ;)"
    author: "Arend jr."
  - subject: "Re: KDE style should have attention as well"
    date: 2004-02-19
    body: "> the developers > a) Are in general opposed to do the needed changes\n\nAnd the GNOME developers are different how? AFAIK most of the accessibility and usability patches are contributed by Sun and not written by the applications' maintainers.\n\n> The style should communicate to the mainstream user and her values.\n> style-elements that speaks to the office workers\n\nFor you the office workers are the mainstream!?"
    author: "Anonymous"
  - subject: "Re: KDE style should have attention as well"
    date: 2004-02-20
    body: "> For you the office workers are the mainstream!?\n\nIndeed.. office workers are != mainstream.\n\nHome users are far more important. "
    author: "anon"
  - subject: "Re: KDE style should have attention as well"
    date: 2004-02-20
    body: "why, exactly, are they far more important?"
    author: "Aaron J. Seigo"
  - subject: "Re: KDE style should have attention as well"
    date: 2004-02-20
    body: "Well, there\u00b4s more of them, I suppose."
    author: "Roberto Alsina"
  - subject: "Revisionist history?"
    date: 2004-02-20
    body: "> It is good that KDE tries to take usability seriously, but I am slightly skeptical. Out of experience from what I have seen here and on the mailinglists the developers\n\nDid you participate in GNOME development pre-GNOME 2.0 (and post GNOME 1.4)?\n\nThere was a MUCH larger attitude against usability changes. Major flamewars were the order of the day.\n\nThis was at a time where KDE was against any changes that would hinder usability (for example, the 'hicolor' iconset was always default instead of prettier icon sets because it was easier to use). Sadly, I saw some of these decisions rolled back during KDE 3.0 and 3.1. 3.2 is a good step forward, and I think 3.3 will be even better.\n\nRock on KDE (and GNOME!) \n\n"
    author: "ac"
  - subject: "Re: KDE style should have attention as well"
    date: 2004-02-21
    body: "> Out of experience from what I have seen here and on the mailinglists the \n> developers:\n \n> a) Are in general opposed to do the needed changes\n> b) Lack the needed understanding and vision pursue such goals\n\nUnfortunately, I have to agree that 'a' is true in *some* cases.  I have often -- when all I expected was to start a discussion -- found unrelenting and irrational opposition. \n\nHowever, I believe that 'b' is totally false.  It is not a question of understanding and vision, it is one of methodology.  The methodology needed is, in most cases, a discussion of the question.  So, the only problem is 'a'.\n\nAn example of this is the discussion (or lack there of) of bug 73851 that has occurred on KDE-BugZilla and various KDE lists (and in private mail).  When I started discussing this, I was not clear on the exact problem or the solution.  It was only through discussion with others that I was able to clarify my thinking on the matter (my final proposal has now been posted on KDE-BugZilla).  Such discussions need to be critical and pointed for good engineering to occur, but this would have gone much better without the 'you don't know what your are talking about' remarks.  What is needed is rather 'just exactly what do you mean' -- by having to explain your ideas to others it forces you to refine them.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "USABILITY STUDY!!!"
    date: 2004-02-19
    body: "Doesn't anyone get the irony inherent in the phrase \n\"Suns usability study\"?\n\nFrom the folks that gave us CDE (jointly), \nbrowser applets, the horrible Metal L&F, etc...\n\nGee! Good thing Gnome has those folks to point\nto the right direction!"
    author: "buggy"
  - subject: "Re: USABILITY STUDY!!!"
    date: 2004-02-19
    body: ">CDE (jointly), \n\nwow there. Sun was not behind CDE. That was HP, IBM, and Dec. Sun only came on board when it was apparent that they had lost with their own desktop (openview).\n\nBTW, I was part of those wars back then. In those days, Sun was doing PR that constantly said that they won the desktop war. I would argue that nothing has changed.\n"
    author: "a.c."
  - subject: "Users and usability"
    date: 2004-02-19
    body: "\"A message consistently received from those users has been that KDE's usability could be better.\"\n\nI don't want to sound elitist here, but what the heck do users know about usability? Think about it. They are not usability experts. They may have a sense that the overall interface is somehow less than ideal, but they do not have the expertise to identify the real problems or submit correct solutions.\n\nUsers are going to request interface changes not on the basis of usability, but on the basis of their personal familiarity. If they're familiar with Windows, then their requests are going to be based on the Windows interface, which is a poor example of usability."
    author: "David Johnson"
  - subject: "Re: Users and usability"
    date: 2004-02-19
    body: "This shows the necessity of looking at the requests and complaints to figure out what the problem really is.\n\nSomeone may complain about something not working as expected. So the questions are what is expected? What actually happened? What were you doing?\n\nIt's amazing what you can learn if you listen to people. They may not know what is going on, may not be able to identify the problem, but they usually communicate that something isn't right. In my real life work, I do this all the time with comfort issues. The clients don't know what is wrong, but they know something is wrong. I ask them questions to ferret out the details that help me troubleshoot the problem. Then I fix it, and send them a big invoice for my expertise :)\n\nIt is too easy for 'experts' to essentially couch their own preferences and prejudices in complicated language. \n\nI like what I see happening here. Aaron is trying to get experienced people to contribute. He is collecting data from users. And throwing in his own experience and knowledge. Then he takes all this, mixes it, and comes out with incremental improvements.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Users and usability"
    date: 2004-02-19
    body: "they know when it's hard to use or when things are inconsistent or when it confuses them. they may not know WHY, but they usually know WHEN it happens. =)\n\nnobody said that the masses were suddenly going to shape KDE by democratically voting on UI issues or anything, simply that the need has been identified and we're doing something about it.\n\nhuzzah!"
    author: "Aaron J. Seigo"
  - subject: "Re: Users and usability"
    date: 2004-02-20
    body: "\"they know when it's hard to use or when things are inconsistent or when it confuses them\"\n\nWhile usability is concerned with consistancy, among several dozen other factors, it is wholly unconcerned with difficulty or confusion by untrained users. Yes, seriously! They only reason its a factor with KDE is because somehow the notion has cropped up that it needs to be dumbed down to the level of the greenhorn newbie, and screw the intermediate and advanced users.\n\nThis may initially seem unintuitive to most people, so let me explain a bit more. Confusion among newbies occurs not because the UI is unusable, but merely because they are unfamiliar with it. And they think the UI is hard simply because they haven't learned how to use it. Just because a newbie doesn't know what something is for in KDE, does not mean that that something must be eliminated. These new users come primarily from Windows. If KDE does not exactly duplicate the Windows UI, then they will think that KDE is unusable. This is not true.\n\nThere are genuine usability concerns in KDE, but they should not be confused with newbie confusion. Heck, they were confused the first time someone stuck a mouse in their hand and pointed them at Windows, but they got over it. It may mean slower adoption of KDE by the unwashed masses, but it's worth the price to get it right.\n\np.s. I am not disparaging this usability effort. I am supremely heartened to see that it is involving real industry usability experts."
    author: "David Johnson"
  - subject: "Re: Users and usability"
    date: 2004-02-20
    body: "\"Users are going to request interface changes not on the basis of usability, but on the basis of their personal familiarity. If they're familiar with Windows, then their requests are going to be based on the Windows interface, which is a poor example of usability.\"\n\nnot true, none of the games I know have windows interfaces, yet i never heard of someone having trouble with their user interface"
    author: "bibi"
  - subject: "Now THAT's WHAT I WANT TO HEAR!"
    date: 2004-02-20
    body: "The weakest spot by far is usability, though I have to say I'm pleased that it was significantly improved in KDE 3.2 and that it's on the road to more rapid improvement.\n\nI just hope I won't have many usability nightmares, like those with Khotkeys 2."
    author: "Alex"
  - subject: "Good Work!"
    date: 2004-02-20
    body: "Well done Aaron, this sounds really promising."
    author: "Dominic Chambers"
  - subject: "More refinement and testing needed"
    date: 2004-02-20
    body: "KDE usability actually isn't *that* bad, and in my opinion Gnome, Windows and Mac all have their problems too.\n\nMS Word is a good example of bad usability - despite widespread use and familiarity, few users can get it to behave correctly. MacOS 7 was a good example of usability - very simple and yet techie users had some really neat keyboard shortcuts.\n\nOh, and I like the usability of Firefox/Thunderbird vs the original Mozilla.\n\nThis is all relevant; look around at what's out there, it's easy to get used to KDE, especially I suspect for developers. Windows XP is good for many users, and yet sometimes they achieve this by sacrificing security. Don't knock it, just try to better it - aim for the same usability without the security compromise, and see how close you get.\n\nKDE needs simplification. I wouldn't mind if 3.3 had, if not a feature freeze, then a very high usability focus.\n\nIn Konqueror, if you select some text in a webpage and right-click, the menu has 14 items in it. That's a lot to look through if you just want to Copy. The Copy item is actually 'Copy Text', which is non-standard and less recognised than 'Copy', but the longer name is 'needed' because further down the menu is an option to copy the whole web page. Surely this latter one is out of context if you've highlighted some text?\n\nIf you select some text in a Kmail message and right-click, there are 19 items. None of them allow you to copy the text to the clipboard.\n\nIf most people have used Windows, and most other people have uses Mac apps, Mozilla, Open Office, etc. etc, then meeting their expections as much as possible *is* improving usability.\n\nAlso, the KDE Control Centre is horribly scary, and even as a seasoned Linux user I look at it and wonder if I can be bothered to go through so many pages. The Windows approach of having an 'Advanced' button is actually a good one here - but if you don't like it, maybe it would be best to come up with some other way to make it easy for the user to differentiate uncommon options from advanced.\n\nPersonally, I'd want to play with about a third of the options, and only dig deeper if I want something specific. For less advanced users, this would be more of a *need*.\n\nThere are other things too, but a lot of it would come down to cleaning up the look and feel further, lots of small refinements and efforts to make things more bug-free, fault-tolerant etc.\n\nPlease, let's have a seriously cleaner and more usable KDE 3.3... and any KDE developers who haven't used Windows XP extensively, I urge you to try and do so!"
    author: "Lars"
  - subject: "Re: More refinement and testing needed"
    date: 2004-02-21
    body: "I suppose you will donate all the computers running Windows XP so we can, uhm, waste our time with it extensively?"
    author: "Datschge"
  - subject: "Re: More refinement and testing needed"
    date: 2004-02-22
    body: "> In Konqueror, if you select some text in a webpage and right-click, the menu has 14 items in it. That's a lot to look through if you just want to Copy. The Copy item is actually 'Copy Text', which is non-standard and less recognised than 'Copy', but the longer name is 'needed' because further down the menu is an option to copy the whole web page. \n\nI'm planning to disable the kuick plugin that provides the \"Copy To\" for khtml though. The only reason that this wasn't done in KDE 3.2 was that I forgot to add support for popup plugins to check certain new context-related flags before it was too late (I didn't have kdeaddons installed) \n\n> Surely this latter one is out of context if you've highlighted some text?\n\nI was planning to implement that originally, but there are usability problems with that. Some people forget they've forgetten to highlight text. This is the same reason why you'll get the items for a page when you click on a image.\n \n> If you select some text in a Kmail message and right-click, there are 19 items. None of them allow you to copy the text to the clipboard.\n\nyes, kmail's context menus are a mess"
    author: "fault"
  - subject: "Just to say something"
    date: 2004-02-22
    body: "If you are afraid of pain, you can not learn, and not progress. The guys behind GNOME were not afraid of pain when they started enforcing the HIG rules. They rubbed quite a few people the wrong way, but they are successful in that they have managed to make people change the way they do things. Now everyone who is developing for GNOME wants to follow their guidelines, because it makes things easier for them too.\n\nAlso, the rule enforcing one app per use is especially important. Take the Kedit, Kate situation. These are both very good apps, and there is a case to be made for including both of these in the DE, for some people at least. But if you are not a developer, you do not need these. So a choice needs to be made, and there will be pain."
    author: "Maynard"
  - subject: "Re: Just to say something"
    date: 2004-02-22
    body: "> Also, the rule enforcing one app per use is especially important. Take the Kedit, Kate situation. These are both very good apps, and there is a case to be made for including both of these in the DE, for some people at least. But if you are not a developer, you do not need these. So a choice needs to be made, and there will be pain.\n\nThe /only/ reason that kedit still exists is that kate doesn't support BiDi. It will in the future (KATE_GOES_BIDI branch), and when that's done, kedit will be removed. \n\nIt has -nothing- to do with developers. "
    author: "anon"
  - subject: "Re: Just to say something"
    date: 2004-02-22
    body: "KDE is working, learning, progressing and changing just fine the way things are handled so far, no need for unnecessary additional forced \"changes\" and \"pain\". And no way there is someone being able to force something like that anyway. Your example of the for uninformed outsiders problematic looking Kate/KEdit situation shows that you really aren't in a position of being able to judge about how things are handled within KDE."
    author: "Datschge"
  - subject: "General Comments"
    date: 2004-02-22
    body: "Just my two cents worth:\nFirst, on this forum setup: In the 'Important Stuff' below, one of the items is:\n>>Try to reply to other people comments instead of starting new threads\nYet when I click on Reply, all the other comments are gone unless I'm folowing a specific thread. Makes it kinda difficult to quote other comments. Couldn't the forum be set up using frames or something? As it is, I have to have reply screen open in a new tab....good thing konqueror & mozilla have tabs.\n\n>>On KDE & useability. IMHO KDE doesn't really have a 'useablity problem', it is the currently most useable Linux GUI available. The GIMP's unusual ui has a useability problem, KDE is pretty easy to get on to no matter what OS you're coming from. This is not to say it's perfect, there is room for improvement but generally it's pretty good. Holler's post ('1st impression counts, 19/Feb/2004) lists some good examples of where improvements are wanted. \nThe 'problem' is getting improvements happening. \nIt would appear 'will' (post 19/Feb/2004) has a point:\n>>... developers\n>>a) Are in general opposed to do the needed changes\n>>b) Lack the needed understanding and vision pursue such goals\n\nLars (Friday 20/Feb/2004) gives the example of revising the drop-down Konqueror menus. A quick search of the bug list reveals that this has already been reported as Bug 53772 (and several others, with over 300 votes). Bug list reports it as a wishlist item and it is marked 'resolved'. How is it resolved?, I have just installed the latest KDE 3.2 & still it has the same problem? \n \nFor KDE (and Linux generally) to progress, issues like this are going to have to be taken seriously and dealt with. This is one of the few disadvantages of open source and particularly volunteer labor open source. In a closed source environment, 'Sales' or somebody with authority can identify a problem (big or small) and enforce a solution but with open source there is no authority structure. This forum may (probably will) help identify problem issues but who's going to see that they get fixed?\n\n\n>>by Gil: \"As the leading Open Source desktop\" Can we really be making statements like this?\nYes. To be the best you must believe you are the best. \n\nand finally\n\n>>by David Johnson on Thursday 19/Feb/2004\n\n>>I don't want to sound elitist here, but what the heck do users know about >>usability? \nYou can't be serious. A 'user' is the ultimate test. If the user can't use the product, what good is it?\n\n"
    author: "CrashedAgain"
  - subject: "Re: General Comments"
    date: 2004-02-22
    body: "> Lars (Friday 20/Feb/2004) gives the example of revising the drop-down Konqueror menus. A quick search of the bug list reveals that this has already been reported as Bug 53772 (and several others, with over 300 votes). How is it resolved?, I have just installed the latest KDE 3.2 & still it has the same problem? \n\nPlease compare KDE 3.1's context menus with KDE 3.2's. \n \n\n"
    author: "fault"
  - subject: "Re: General Comments"
    date: 2004-02-22
    body: "The Bug report starts out:\"The context menu in konqueror-the-web-browser should *only* contain entries relevant to the currently selected element. \"\nSelect text, right click in 3.1.3 gives:\n\n\nBack\nReload\nOpen in New Window\nOpen in Backbround Tab\nOpen in New Tab\nOpen with\nAdd to Bookmarks\nActions\nSelect All\nStop Animations\nView Document Source\nView Document Information\nSecurity\nSet Encoding\n\nIn 3.2:\n\nback (but not fwd)\nreload\nCopy Text\nSelect All\nBookmark this Page\nOpen With\nPreview in\nActions\nCopy To\nStop Animations\nView Document Source\nSecurity\nSet Encoding\n\n(It is of course different again in this editor box)\n\nAdmittedly the ability to Copy/Paste on right click is a welcome and very much overdue improvement but what are all those other things? How is 'Stop Animations','View Document Source' or 'Preview In' relevant to selected text? \nDoes this really solve the Bug? \n\nAs an aside, I would have expected Select text->rt click->preview in embedded text editor would have presented me with the selected text displayed in the embedded text editor but instead it gives the html code for the whole page. This is probably going to confuse a nOOb who will then maybe click the up arrow  trying to back out of it and will soon become thoughly lost. \n"
    author: "CrashedAgain"
  - subject: "Re: General Comments"
    date: 2004-02-22
    body: "Yeah, I guess that hasn't been optimized yet. I'll take a look at.  Compare KDE 3.1 and 3.2's context menus for links however. "
    author: "fault"
  - subject: "Re: General Comments"
    date: 2004-02-22
    body: "I should also mention that reopening that bug report probably won't do any good; it has far too generic stuff in comments. "
    author: "fault"
  - subject: "Re: General Comments"
    date: 2004-02-22
    body: "// Yet when I click on Reply, all the other comments are gone [...]Makes it\n// kinda difficult to quote other comments. Couldn't the forum be set up using\n// frames or something? As it is, I have to have reply screen open in a new\n// tab....good thing konqueror & mozilla have tabs.\n\nKonqueror also let you split the screen up into two halves, which would get you the forum \"set up using frames\".\n\n// In a closed source environment, 'Sales' or somebody with authority can\n// identify a problem (big or small) and enforce a solution but with open source\n// there is no authority structure.\n\nI doubt it's a problem of (authority structure free) open source versus (dictatorial) closed source environment. It's more a problem of many people agreeing that something should be done while there is no single clear vision but multiple competing solutions by those people how the end result should look like, leading to confusion what is actually wanted. As fault already noted, the report 53772 you are referring to is actually an excellent example of that. Solution to that issue would be encouraging commenters to stop watering down reports and instead commonly agreeing on a single solution, but seeing how controverse these specific cleaning up issues are this will be hard to achieve."
    author: "Datschge"
  - subject: "Re: General Comments"
    date: 2004-02-22
    body: ">> split the screen up into two halves...\nSo it does! I had completely forgotten about this feature. Leads me to wonder how it could be made more visible - perhaps an icons on the main toolbar??\nThis is one of the things I like about KDE...almost every app is chock full of really good features. Way ahead of Gnome.\n\tBTW it is Windows, not Gnome which is the rival in the 'desktop wars'. The primary choice faced by any potential system installer is Windows system or Linux system. The choice of which desktop then defaults to the choice of which distro which will be largely influenced by other factors such as geography. Red Hat probably dominates in North America, SuSE in Europe. Each of these has their own reasons for their choice desktop, I doubt if useablity comparisons are very big as both are generally considered pretty much equal in this area.\n\tIt does appear that KDE could implement some improvements in the methodology of implementing improvements, however. At present this is pretty much limited to the bug reporting system, possibly something more like an open forum might generate more ideas and may be able to focus the ideas to get a consensus of what is really needed in the way of a solution. One problem is visibilty of the forum, even this one is not easy to find from the KDE homepage. I happened on it quite by accident. I knew about the forums at kde forums.org but I never would have gotten here from there. Maybe 'wish list' forum at KDE-forums with possibly a separate link on the KDE home page. Whatever it is, I think it would have to be directly and obviously visible on the KDE home page, if I'm a nOOb, that's where I'm going to start. \n"
    author: "CrashedAgain"
  - subject: "Re: General Comments"
    date: 2004-02-22
    body: "Well, how could we see \"improvements in the methodology of implementing improvements\"? I agree that for \"n00bs\" it might be preferable to hang around at kde-forum.org instead at bugs.kde.org, but for the rest of the KDE using/contributing/developing community to notice suggestions on kde-forum.org results there better be reported to bugs.kde.org for the suggestions not to get lost. So I have to disagree that limiting ourselves to bug.kde.org regarding suggestions is bad, actually I see it as the only way ensuring that all kind of reports and suggestions are tracked and searchable anytime without having to keep track of multiple sites, message boards, irc channels and mailing lists and whatever other place could be used for just that. The best solution would probably be that each forum, mailing list etc. has at least one person who regularly checks whether specific suggestions and problems mentioned at his place are already reported on bugs.kde.org, and when that's not the case he reports them there himself."
    author: "Datschge"
  - subject: "Re: General Comments"
    date: 2004-02-27
    body: ">>So I have to disagree that limiting ourselves to bug.kde.org regarding suggestions is bad, actually I see it as the only way ensuring....\n\nYes, probably you are right. It would be too much to expect developers to keep track of numerous forums, etc. Still, the present setup (maybe) has some problems in that \n1. it is not easy for a nOOb to 'effectively' use the bug system. I speak from experience, I'm still pretty much a nOOb having used Linux for only about a year and I'm only just now beginning to feel like I can 'use' it as opposed to feeling my way around to see what this or that will do. The main problem in using the bug system is trying to figure out if your bug really is a bug, is a repeat bug, is maybe just a wish list item or is just a feature that you haven't figured out yet. Is is rather off-putting to a nOOb to be told to RTFM, especially when it is difficult read the FM. \n2. the bug system is not very 'open' in the sense that the only people who will see your problem are those who maybe have a similar problem ie there are no 'browsers'. An open forum  might trigger some discussion which could develop a more appropriate resolution to a problem, for instance the items list on the rt click menu example mentioned above.\nAs a total nOOb, I am/was/would be hesitant about using a 'bug reporting system' whereas I would be quite comfortable using an open forum, especially for something minor. Also, you would never get any 'first impressions' comments in the bug system whereas you might in s forum.\nAnyway, that's about the best case I can present for a 'Bugs & Wish List' forum as an additional way of getting user feedback. \nAnticipating a 'Do we really need an additional forum?' response, the answer is 'I don't know'. If the present forums are producing feedback which is actually reaching the developers and initiating action, then there is no need for another one but if this is not the case, then maybe there is a need for an 'official' forum.\n\n"
    author: "CrashedAgain"
  - subject: "Re: General Comments"
    date: 2004-02-27
    body: "You know what, I fully agree with you. ;) The problem is that reporting bugs and wishes which should be resolved by programmers are technical issues, so the tracking system will never be able to lose the obstacle that at least some basic technical knowledge is there for reporting such stuff.\n\nAs I wrote before the best solution would probably be that every forum has at least one person who has the technical basic knowledge and regularly checks whether specific suggestions and problems mentioned at his place are already reported on bugs.kde.org, and when that's not the case he reports them there himself.\n\nIt looks like kde-forum.org has a special forum called \"Feedback - Post here for Ideas & Feedback for KDE and the KDE Developers\" for feedback purpose already, containing suggestions etc which should go to bugs.kde.org. I will look if anyone is taking care of the job I mentioned before, and if not I might do it."
    author: "Datschge"
  - subject: "Re: General Comments"
    date: 2004-02-25
    body: "Actually, if you go \"Settings->Toolbars->Show Extra Toolbar\" you will have a an icon on the toolbar for splitting the screen and joining it up again.  Also, you can edit your toolbars so you could put the icon on the main one if you preferred.\n \n"
    author: "Mark Grant"
---
As the leading Open Source desktop, many from the private and public sectors have expressed interest in KDE and are currently using it on a daily basis. A message consistently received from those users has been that KDE's usability could be better. While the need for greater usability is not unique to KDE, what is unique is our ability to directly and positively affect the usability processes in KDE via the same Open Source methods that have been at the core of its success.



<!--break-->
<p>KDE is delighted to have received invitations from corporate users to leverage the expertise of their own usability teams to help improve our software. In order to take advantage of these generous offers, the current usability processes in KDE were reviewed so as to discover how best to marry these new opportunities with the KDE project in general.</p>
 
<p>To facilitate these efforts a new moderated email list has been set up: <a href="https://mail.kde.org/mailman/listinfo/kde-usability-devel">kde-usability-devel@kde.org</a>. Subscription to the list is open and the list will be publicly archived. The list will be moderated to keep the signal-to-noise ratio as high as possible.</p>
 
<p>The primary goal of this new email list will be to find, promote and implement methods to integrate ongoing usability efforts with existing KDE design methodologies. KDE developers will be encouraged and given the tools to include usability as a primary concern at the earliest phases of application development.</p>
 
<p>To provide additional support for these efforts, a new website is being developed that will become the incubator for a set of revamped and extended KDE UI Guidelines and house industry research papers, studies and findings in order to allow greater communication and cooperation between academic, commercial and Open Source usability efforts.</p>


