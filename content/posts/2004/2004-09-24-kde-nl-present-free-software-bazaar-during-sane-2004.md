---
title: "KDE-NL present at Free Software Bazaar during SANE 2004"
date:    2004-09-24
authors:
  - "fmous"
slug:    kde-nl-present-free-software-bazaar-during-sane-2004
---
Coming week the <A href="http://www.nluug.nl/events/sane2004/">4th edition of SANE</A> will start, an international 5-day event on System Administration and Network Engineering. Wednesday evening 29th September, the Free Software Bazaar will take place which is part of SANE 2004. Bazaar is forming a bridge between the tutorials and the conference and is a perfect opportunity to meet software developers and get involved in these projects. This event takes place in the evening and it can be visited for free. Richard M. Stallman will start the Bazaar with a presentation on Free Software followed by some BOF sessions of different Free and Open Source Software projects. The <A href="http://www.kde.org/">KDE project</A> is represented with two sessions arranged by <A href="http://www.kde.nl/">KDE-NL</A>. It looks like it is going to be a nice evening so if you have the chance please visit us at this event!

<!--break-->
