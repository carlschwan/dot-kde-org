---
title: "Guest Series: Interview with Inkscape Founder Bryce Harrington"
date:    2004-11-05
authors:
  - "fenglich"
slug:    guest-series-interview-inkscape-founder-bryce-harrington
comments:
  - subject: "KDE?"
    date: 2004-11-05
    body: "No flame intended, but why is this posted on a KDE/QT newssite? This has nothing to do with KDE and/or QT. What's next? Articles about the elections?"
    author: "AC"
  - subject: "Re: KDE?"
    date: 2004-11-05
    body: "The article says it all:\n\n\"This is the official start of the Dot's Guest Series, which will house articles and interviews relevant and interesting to the KDE community, but not necessarily about KDE.\"\n\n"
    author: "Anonymous"
  - subject: "More openmindedness!"
    date: 2004-11-05
    body: "In fact, I like the idea of this \"Guest Series\"...\n\nFor the likes of you, your eyes just seeing the keywords \"Guest Series\" should be enough to let your brain trigger the decision \"Skip this -- evil foreigners!\"\n\n:-P"
    author: "open mind"
  - subject: "Re: More openmindedness!"
    date: 2004-11-05
    body: "Not really. The article is interesting, but it would be better if it was published at another site. It really makes no sense to have an article like this in a site whose intent is to bring news about KDE.\n\nWell, perhaps the dot just wants to be a \"general\" IT site. That would be a loss, since we'd be loosing the best source for news about KDE ever. I really hope not to see more nonsense like this on the dot again..."
    author: "Henrique Pinto"
  - subject: "Re: More openmindedness!"
    date: 2004-11-06
    body: "\nNo, this is no attempt from the Dot editors to become a general IT site. I neither see how that would be achieved by once in a while, posting an interview with content related to KDE but not about KDE. We don't remove KDE news, we just invite \"guest speakers\" now and then.\n\nThe reason why we found the Guest Series a good idea, was it is a possibility of introducing new thoughts to the KDE community. Now and then, a quick slip of different air than \"KDE gets feature X! We rock!\". However, if many expresses that they don't want to hear about anything else than purely about-KDE, or that it overshadows the other news, we'll of course skip it. \n\n\nCheers,\nFrans\n\n"
    author: "Frans Englich"
  - subject: "Re: More openmindedness!"
    date: 2004-11-06
    body: "No, I think its a great addition.. Keep up the good work."
    author: "mrdocs"
  - subject: "Re: More openmindedness!"
    date: 2004-11-06
    body: "Hi Frans!\n\nObviously there are doubts about whether an interview with Gnome developers to promote Gnome apps fits in the dot. The concerns are not only mine. This interview is currently being discussed at the kde-promo mailinglist, where many people have expressed their dissatisfaction with it (including Aaron Seigo, Scott Wheeler, Rob Kaper and Jason Keirstead). If you haven't yet, you should really take a look at the thread (you'll find it easily if you search for \"What is the goal of dot.kde.org?\").\n\nPlease not that I have no problem with the interview itself, it was actually a nice reading, but I still think it should have been posted elsewhere. Next time you have such an interview, please consider posting it to FootNotes. It is surely much better suited there.\n\n"
    author: "Henrique Pinto"
  - subject: "Re: More openmindedness!"
    date: 2004-11-06
    body: "Hi Henrique,\n\nWhen you create a svg icon (which is KDE requirement for maintained apps in KDE modules), what tool do you use please?\n\nAnne-Marie"
    author: "annma"
  - subject: "Re: More openmindedness!"
    date: 2004-11-06
    body: "Bigotry. I think it's admirable to be able to look around and see the rest of the world, determine what's good and what you can learn from. The interview was more a prepared questionnaire than a real interview, and I'm heartily sick of that type of interview, but despite that, it was an interesting read. Maybe the next installment can be a _real_ interview with questions provoked by answers and some deep digging.\n\nThe fact that some people discuss it on some mailing list is utterly irrelevant. The dot editors were fortunately independent-minded enough to accept it.\n\nTo suggest that it should stay in a ghetto of its own colour, but that you don't have 'a problem' with it it, is... Polite words fail me. It's bigotry. Narrow-mindedness. Parochialism. "
    author: "Boudewijn"
  - subject: "Re: More openmindedness!"
    date: 2004-11-07
    body: "Not really.  I don't have any problems with this sort of interview.  I'll even read them when they happen to pique my interest.  It's just a question of when and if they belong on a KDE news site.\n\nThe question isn't \"Do you like this application?\"  \"Do you find this application useful?\" or \"Do you find this interview interesting?\" -- rather they're \"What is the dot here for?\" and \"Does this sort of interview fit in with that?\"\n\nI mean -- surely we'd think hardware reviews were off topic here?  What about a review of Microsoft office?  There's a limit somewhere, some of us simply believe that on KDE's news site the information should have some fairly direct relevance to KDE."
    author: "Scott Wheeler"
  - subject: "the answers"
    date: 2004-11-07
    body: "\"What is the dot here for?\"\n\nOn this web site you can post or read about all things KDE including new developments, applications and breakthroughs, announcements, feature articles, interviews, events and more.\n\n\"Does this sort of interview fit in with that?\"\n\nThis is the official start of the Dot's Guest Series, which will house articles and interviews relevant and interesting to the KDE community, but not necessarily about KDE. The previous article, despite its sneaky start, was Pete Gordon on Portable Usability Laboratories."
    author: "ac"
  - subject: "Re: KDE?"
    date: 2004-11-05
    body: "Ermmm, you did read the bits about Project Management and Usability?  The guy makes some good points.  Just because people come from the G side of the world doesn't mean we can't learn things from them..."
    author: "Odysseus"
  - subject: "Inkscape..."
    date: 2004-11-05
    body: "Inkscape is a beautiful piece of software that is a prime example of just why open source is so amazing.  You take another wonderful title like Sodipodi, then the ability to  fork off of it if you have new ideas that aren't being expressed in the code.  \n\nThank you to all the Inkscape developers.  You guys seriously *rock* and are paving the way to make illustrating with some other proprietary tools a thing of the past!\n\n//Kenny//"
    author: "standsolid"
  - subject: "What a pity...."
    date: 2004-11-05
    body: "...that they decided to fork Sodipodi and for that they choose gtk+ instead of join into the Karbon14 Project and push a Killer App on Qt basis."
    author: "jambo"
  - subject: "Re: What a pity...."
    date: 2004-11-05
    body: "that would be annoying, as I use this program on windows, and since theres no free opensource solution of QT, then its kinda pointless. Not everything needs to be QT-ised."
    author: "Salsa King"
  - subject: "Re: What a pity...."
    date: 2004-11-05
    body: "We chose to fork Sodipodi because whem we made that decision we had already been Sodipodi developers for a long time and had invested a great deal of work in the codebase.\n\nI'm sure if we had been Karbon14 developers we would have forked that instead. :)  Karbon14 seems aimed in a different direction than Inkscape.  For example, Inkscape emphasizes making it possible to work directly with the XML of our SVG documents when needed, and we are fairly careful to enable round-tripping of \"foreign\" XML fragments in our documents.  I admittedly haven't looked at Karbon14 much, but it doesn't seem to be oriented towards that in the same way.  That's not a failing of Karbon14, necessarily, but it is different from what we want to do.\n\nWe did consider switching to Qt early on, but it is not an option for us since we do not wish to give up our Windows port, and there is no Open Source Edition of Qt/Windows available.  GTK on Windows is not great, but we've been able to get much farther with it than our Qt-based sister project Scribus has with Qt/X11 on Windows.\n\nThere's some truth to both sides of the \"Should we port Linux applications to Windows?\" debate, as represented in <a href=\"http://nashi.altmuehlnet.de/pipermail/scribus/2003-November/003199.html\">these</a> <a href=\"http://nashi.altmuehlnet.de/pipermail/scribus/2003-November/003209.html\">posts</a> on the Scribus ML.  IMO, one \"killer app\" isn't enough for people to switch operating systems.  On the other hand, if most or all of the apps they use on a daily basis are also available on Linux, it suddenly becomes much easiser to switch because they don't have to relearn everything.  So I think the more apps available on Windows the better, as long as you have Windows-based developers to keep the ports \"alive\" without unduly burdening the Linux-based ones.\n"
    author: "MenTaLguY"
  - subject: "Re: What a pity...."
    date: 2004-11-05
    body: ">>> GTK on Windows is not great, but we've been able to get much farther \n>>> with it than our Qt-based sister project Scribus has with Qt/X11 on \n>>> Windows\n====\nHuh?\n\nHow is it that Scribus is a \"sister\" project to inkscape? From what I read off the inkscape website, inkscape strifes to be a great and standards-implementing SVG authoring tool. From what I read off the Scribus website, Scribus strifes to be a great and ass-kicking DeskTop Publishing tool (which also can export pre-press quality PDF/X-3, complying to all conceivable \"standards\" here).\n\nThis may make the too projects even siblings -- but it doesnt make them comparable in terms of \"we got farther then them\" for each one.... "
    author: "open mind"
  - subject: "Re: What a pity...."
    date: 2004-11-05
    body: "Both projects work closely together \n\n\nFab"
    author: "fab"
  - subject: "Re: What a pity...."
    date: 2004-11-06
    body: "Do they share code? Or just friendship among developers...\n\nThese projects rock!\n_cies."
    author: "cies"
  - subject: "Inkscape and Scribus"
    date: 2004-11-06
    body: "The Inkscape and Scribus teams do things like periodically have IRC meetings together to exchange ideas, and we work closely together on interoperability between the two apps (the obvious example being improving Scribus' SVG support).\n\nWe also share developers a little bit, though mostly folks do work on their respective sides.  Most notably Peter (mrdocs) from Scribus contributes to Inkscape.  There's also a standing invitation for bulia and me to spend some time working on the Scribus UI, but so far we've had our hands too full with Inkscape work.\n\nI think we'd share code if we could.  At the moment though our codebases are too different; that may change a little now that Inkscape is becoming more of a proper C++ app.\n"
    author: "MenTaLguY"
  - subject: "Re: What a pity...."
    date: 2004-11-06
    body: "The only thing I was comparing here is the completeness of our respective Windows ports.  Inkscape's is almost as mature as the Linux \"port\", whereas Scribus on Windows is (as far as I know) not really tested or used so much.\n\nRequiring an X server (as Windows apps using Qt/X11 do) is a non-starter for many Windows users.\n"
    author: "MenTaLguY"
  - subject: "Re: What a pity...."
    date: 2004-11-06
    body: "Odd, I seem to recall that the Scribus developers got free Qt/Win licenses from Trolltech so they didn't need to use X11"
    author: "Carewolf"
  - subject: "Re: What a pity...."
    date: 2004-11-06
    body: "No, we have not.. Though we would welcome that :-)"
    author: "mrdocs"
  - subject: "Get in touch"
    date: 2004-11-10
    body: "\nHave someone on the team contact TT and we'll work something out."
    author: "Anonymous"
  - subject: "Re: What a pity...."
    date: 2004-11-08
    body: "I think the Psi developers got that.. Trolltech periodically does that. Perhaps the Scribus developers should contact TT. "
    author: "anon"
  - subject: "Re: What a pity...."
    date: 2004-11-06
    body: "Scribus also tries to adhere as strictly to standards as possible. SVG is a big part of that e.g. Scribus uses SVG as one of its primary means of importing vector artwork, as well as exporting SVG as compliant as possible. Small example: witness the support for dublin core metadata in both applications.\n\nAs for siblings, Its fair to say both projects are trying very very hard to become best of breed, despite the different toolkits used. Actually, choice of tool kits, except for some good natured banter and joking is rarely an issue or a problem.\n\nWe struggle with the same sorts of issues in attempting to create standards compliant output. SVG is a big and complex spec and so far no app open or proprietary has 100% perfect compliancy. \n\nThis kind of cooperation is no doubt benefital to each project *and* to end users. \n\nWe have an enormous amount of respect for the Inkscape devels and how they have managed their project. The proof is in the results.. In one short year, their progress has been remarkable.\n\n"
    author: "mrdocs"
  - subject: "Re: What a pity...."
    date: 2004-11-07
    body: "You, mrdocs and Mentalguy, and all the rest of your teams REALLY ROCK. \nDifferents but equals. This is the spirit.\n\nAnd obviously thanks for these great apps, I don't use Inkscape cause I do not draw graphic but from time to time I use Scribus to print something, and it's really useful."
    author: "Davide Ferrari"
  - subject: "licenses"
    date: 2004-11-11
    body: "The PSI (psi.affinix.com) developers got free Qt licenses because their product was open source. Shouldn't the Scribus developers get the same treat?\n\n(I guess it also requires an addition to the GPL license on the software that explicitly allows liking against Qt, just like OpenSSL requires.)"
    author: "Jonas B."
  - subject: "Related: What about editable SVG canvas for KDE?"
    date: 2004-11-06
    body: "I'm trying to write an application that has to work a lot with vector-based drawings. And I'd like to do it in KDE, mainly because I like all the facilities it gives to coders.\n\nI ask: is there some kind of canvas or application that enables me to start with something that's at least half-working? \nI've seen QtCanvas is nearly unusable for serious work, SVGCanvas/KCanvas are in an unknown state, trying to become a supertool, but still in a state of flux about what to do (also, there's been very few commits, lately).\nKarbon is nice, but documentation on the program and how it works is nonexistant (most class don't even have doxygen comments...) and it's tied to KOffice in which I'm uninterested (mine should be a cartoon program, not a Office application).\n\nAny idea, except the obvious \"use GTK and learn from Inkscape\"? ;-)\n\nCiao,\n  Rob!\n"
    author: "RobM"
  - subject: "Re: Related: What about editable SVG canvas for KD"
    date: 2004-11-06
    body: "Have a look at \"Karbon 14\"...."
    author: "kdefriend2"
  - subject: "Re: Related: What about editable SVG canvas for KD"
    date: 2004-11-06
    body: "I already have, but as I said, documentation is really scarce and even the source code doesn't help much: I've tried using kdevelop and doxygen to see if I could make some sense on how to reuse part of the code/classes, but it doesn't help much: few comments, and no doxygen comments on many classes...\n\nIt also doesn't make much sense for me to learn and work _on_ Karbon, since the things I have to do are quite different from those a good vector drawing program have to be able to do.\n\nFinally, it's (obviously) heavily dependent on KOffice classes, and I don't know how useful those could be to my future program. I suspect they'd be more hindrance than gain.\n\nCiao,\n  Rob!"
    author: "RobM"
  - subject: "Re: Related: What about editable SVG canvas for KD"
    date: 2004-11-06
    body: "I don't know what it is about canvas widgets, but things are no better on the Gtk side of things.  There, too, the standard canvas widget is nearly unusuable for serious work, and the \"next generation\" efforts seem to have bloated and flamed out.\n\nInkscape will eventually offer a canvas widget of some kind, but it's probably still pretty far in the future.  As you're doing it in KDE, I'll second the other posters' suggestion to have a look at what you can take from Karbon14."
    author: "MenTaLguY"
  - subject: "Re: Related: What about editable SVG canvas for KDE?"
    date: 2004-11-06
    body: "Well, you would be most welcome to have a look at Scribus, we are very serious about adding to the drawing capabilities of Scribus, though Inkscape is really the best choice for Linux IMO.\n\nThe reason I mention Scribus is already it has probably the best PDF export/PS export in Linux land and replicating that is going to be *very* difficult.\n\nThe canvas code in Scribus is based on libart_lgpl a Gnome! library.\n\nCheers,"
    author: "mrdocs"
  - subject: "Re: Related: What about editable SVG canvas for KDE?"
    date: 2004-11-06
    body: "Thanks to all! :-)\n\nI'll try to contact KCanvas people, and to have a look at what Scribus can do.\n\nCiao,\n  Rob!\n"
    author: "RobM"
  - subject: "Re: Related: What about editable SVG canvas for KDE?"
    date: 2004-11-07
    body: "I think KOffice libraries also use libart_lgpl :)\nwilbert@obelix:~$ ldd /opt/kde3/bin/kword | grep libart\n        libart_lgpl_2.so.2 => /usr/lib/./libart_lgpl_2.so.2 (0x40c2a000)\n"
    author: "wilbert"
  - subject: "Re: Related: What about editable SVG canvas for KDE?"
    date: 2004-11-07
    body: "> The canvas code in Scribus is based on libart_lgpl a Gnome! library.\n\nIt is *not* a gnome library as it doesn't depend on glib.\n"
    author: "Leo Savernik"
  - subject: "that looks cool"
    date: 2004-11-06
    body: "\"16. Animation\n0.50\nImplement full animation support\"\n\nhttp://www.inkscape.org/roadmap.php\n\n:)"
    author: "Pat"
  - subject: "Inkscape Founder to Speak at SCALE 3x"
    date: 2004-12-29
    body: "<a href=\"http://www.socallinuxexpo.org/speakers/speakers_gould.php\">Ted Gould</a>, one of the founding developers of the <a href=\"http://www.inkscape.org\">Inkscape</a> project, will be speaking at <a href=\"http://www.socallinuxexpo.org\">SCALE 3x</a> at the Los Angeles Convention Center. Ted's presentation will cover the basics of the SVG standard, use of SVG in the Linux desktop, and an introduction to creating SVG graphics with Inkscape. SCALE 3x will take place on February 12th and 13th, 2005, in Los Angeles, California."
    author: "I"
---
In a very short time, the <a href="http://www.inkscape.org/">Inkscape</a> project has developed into a flexible, solid, and user friendly vector drawing program. To what does the project owe its success? <a href="http://www.bryceharrington.com/">Bryce Harrington</a>, one of the main developers behind Inkscape, reveals in this interview his thoughts on project management and keeping the focus on the big issues.






<!--break-->
<p><b>Frans Englich: Bryce, feel free to introduce yourself.</b></p>

<p><b>Bryce Harrington:</b> I'm an engineer at the Open Source Development Labs, but work on Inkscape in my freetime. I've been involved with the project since the start, doing a mix of release wrangling, development, and other organizational type things. I've dabbled in a lot of open source projects, but Inkscape's my favorite.</p>

<p><b>From even a glance at Inkscape's <a href="http://www.inkscape.org">website</a>, one quickly realizes that this project has sharply-defined goals with a clear focus.  What did you have in mind when formulating this plan?</b></p>

<p>When we formed Inkscape it was important to us to communicate where the project intends to go, and what steps are needed to get there. There were several reasons for this.</p>

<p>First, having an established plan makes it easy to figure out where to "fit" your work in. When working on projects where the overall vision is not communicated, you find your patches getting rejected for unpredictable reasons. By specifying a clear vision, it helps new developers in figuring out how to make their contributions tie into the project's goals.</p>

<p>Second, we get a *lot* of feature requests. Quite often we hear about some "must have" feature that isn't implemented yet, and that we simply haven't got the time to work on. By putting those features into a roadmap we can say, "Yes, that's a good idea, and we plan to work on it for the XYZ release." This is received better than an ambiguous, "I dunno, maybe I'll work on it later."</p>

<p>Third, it really helps us organize our development efforts. New contributors can identify where the major development activity is, and track the work. Also, users can see what cool new things have been implemented that they can play with; this is an invaluable way for the development team to get early testing and feedback on their work.</p>

<p><b>What role does the website play?</b></p>

<p>The website really is just a communications tool, and while it's the most important "outward facing" communications tool, internally it's simply one of many tools. The bug list, roadmap, release notes, jabber, and development mailing list are all key tools; we couldn't do without any one of them.</p>

<p>Of course, success in open source projects requires a lot more than just good tools - it requires skill, cooperation, dedication, focus, and most importantly a desire to be successful. If you're missing any one of those, the best tools in the world won't help.</p>

<p><b>Some think a common problem in open source is the "pet project effect", that developers start new projects from scratch, instead of reusing existing ones. Inkscape focuses strongly on following established standards, and contributing back to and re-using existing libraries. How do you achieve that productive mentality? Why not write a program from scratch when perhaps it would be more fun?</b></p>

<p>Taking the second question first:</p>

<p>Yes, creating a piece of software from scratch has a very strong lure -- one I've succumbed to more times than I could count.</p>

<p>I think part of the reason is that when you start from an existing codebase, you have to invest a lot of time coming up to speed in learning it before you can start making meaningful changes. Often it *seems* like it'd be faster to start anew.</p>

<p>However, after doing a few projects from scratch, you learn a few things.</p>

<p>First of all, it's a heck of a lot of work to create a full fledged software product that's worth releasing. Starting from something that already exists means your time to first release is as short as you like; for Inkscape our first release came 4 days after the founding of the project, and was immediately usable.</p>

<p>Second, despite what it seems like, it's actually quite hard to get very many people to contribute to Open Source projects. It's easy to make the assumption that if you do the first 80%, other developers will magically appear and take care of the rest for you. In reality, you pretty much have to be prepared to take it 100% of the way, and then, *maybe* if you're really lucky, you'll get a smattering of contributions. But if you start from an existing codebase, that's sort of like getting the Mother of All Patches; plus, it's already received some amount of testing and benefitted from critique and real-world usage.</p>

<p>Third, and probably most importantly, you realize that there's a lot of drudgery work in completing a full fledged software product. You probably only care about some portion of it, like the rendering algorithms or the internal data structures, and the rest holds minimal interest for you. Starting from an existing codebase means you can put off having to work on those other areas and focus on the part you are most passionate about. Heck, maybe you'll get lucky and someone will
join in that enjoys doing the part you consider drudgery and save you the work entirely. :-)</p>


<p>Back to the question regarding libraries and standards. When developing a piece of software, it's not unusual to get frustrated with the library or standard; there are integration hassles, bugs, or missing features you have to spend time working around, and sometimes you just plain disagree with how it's implemented.</p>

<p>However, there are other benefits to reusing existing libraries that should be kept in mind. Think about libraries as a way to modularize code responsibility among multiple people; you only have a limited amount of energy to devote to creating and maintaining code, and you should focus it into the areas you want to be in, and let others take care of the other parts. It is also important to consider the larger perspective: if you use (and improve) a library, then the benefits are
gained by the whole community, making it better even for people who don't use your program.</p>

<p>Also, from a sociological perspective libraries are a great way of organizing effort. I believe that communities have optimal sizes, and if you get too many people involved, dissent and acrimony can make the community unsuccessful. With published libraries, you don't even have to know the developer in order to use them with your code. So you can build a much more sophisticated application by integrating the work of several communities. With standards and libraries, you also avoid a lot of arguments in your project - if folks disagree with the approach, you can just forward them to the appropriate standards body or development community.</p>

<p>Standards are also extremely valuable because they represent a huge amount of arguing that you won't have to do. Because we have the SVG standard, inside the project we don't have to waste a lot of energy and hot air hashing out how to store the data or what syntax to use; it's already defined for us. And it helps even more outside the project; if we find another program isn't interoperating with Inkscape very well, having the standard means we simply check each program's adherance, and the one that is not in compliance has to change.</p>

<p><b>On Inkscape's website there's a <a href="http://www.inkscape.org/roadmap.php">roadmap</a>, where you carefully plan what is to be implemented in each release. Why are the benefits of having a roadmap? Can't one simply implement features at one's convenience?</b></p>

<p>As I mentioned above, a roadmap is a helpful communication tool both for users and developers. We use it as a way to communicate what we intend to do, not as a mechanism to "control" development, as one might be tempted to think. It is extremely common for changes to get implemented out of order due either to necessity or personal interest, and that's fine -- the goal is to get the work done, the plan is just a means to that end.</p>

<p>It's not unusual for us to periodically review and revise the roadmap. For example, for this upcoming 0.40 release we realized we'd bitten off more than we could chew, and split it into two releases so that we could get our recently implemented features out to users more quickly. We wanted to do a release, and so we simply adjusted the roadmap to make it fit what we intended to do.</p>

<p>We also find the roadmap to be an effective way to get closure even on drudgery tasks. Being able to get stuff "checked off the list" can be enough motivation to do some work that would otherwise not have any particular lure to do. It's also helped us coordinate large scale changes to the codebase, although there's no substitute for having a smart, gung ho developer with time on his hands to do the massive change work. :-)</p>

<p><b>The Hacker's Jargon Dictionary writes about <a href="http://www.catb.org/~esr/jargon/html/C/creeping-featurism.html">featurism</a>, when a software project strays from its main course, beginning to neglect the larger audience in favor of smaller features for a smaller set of users such as developers or power users. How do you stay on track? How do you ensure Inkscape does not wander off in a short term direction, as the feature requests rain in?</b></p>

<p>Given that Inkscape is only one year old, this is still a problem we have to be attentive to. However, I think there's a number of reasons why we've been able to stay on track so far, and that I think will help us keep on track in the future.</p>

<p>The roadmap and mission statement provide a compass we can use to detect whether a given new idea fits to where we want to go or not. The feature tracker gives us a place for new ideas to be proposed without disrupting the overall development effort. We use the feature list as a place to mine for ideas and to judge the demand for particular features; we have a saying, "Patch first, discuss later", which means that we expect discussions about where to go to begin after we see working code, not with a distracting debate on the mailing list over how it "should"
be done.</p>

<p>But beyond that, there's little that would actually prevent the project from wandering. We hardly ever tell people, "No, you can't do that", and it's rare that we would actually refuse to integrate a patch (although we may tweak it beyond recognizability later). So I think it is largely due to the dedication and experience of each of the developers on the team that we stay on track. The team *wants* to stay on track, and so they stay on track fairly naturally. It probably helps that we know the track can be changed if and when necessary, and that we have the freedom to choose whether or not to follow it.</p>

<p><b>Some usability engineers say that first the interface should be designed, and then it should be implemented, such that the interface doesn't become a "symptom" of the coding, or a secondary aspect which needs to adapt to the code. One of the early <a href="http://www.inkscape.org/roadmap.php">project goals</a> was to establish a usability design, and rearrange Inkscape's code to it.  What was the difficult part of that "establishing an interface design" development phase?</b></p>

<p>Actually, as I recall the only hard part was convincing users that the change was a good idea. The user interface change was pretty radical from the original Gimp-style layout, so all the users had to re-train their fingers. Technically the change was straightforward.</p>

<p>Bulia deserves the lion's share of the credit for the good usability that Inkscape has today. I think one of the secrets to our success was simply recognizing that he had a strong vision and good ideas, and encouraging him to run with them. Bulia is a professional artist and uses Inkscape in his daily work, so has a built-in motivation to make its usability perfect. He's also an extremely active developer and puts an amazing amount of time into changing the codebase.</p>

<p>Of course, Bulia doesn't do this work in a vacuum; Inkscape benefits from ample feedback from users, who test out each new feature and comment on whether the given key binding makes sense to them, or if the location of the feature's command is inconvenient. The team also listens seriously to ideas for changing how the drawing tools work, and adopt ideas that make it quicker or more painless to get work done. The project provides nightly snapshots to make it easy for users to download and test the latest version without having to compile it themselves. This close feedback loop between the developers and the users testing the usability means we can optimize the usability of a new feature within a matter of days.</p>

<p><b>What can we expect from Inkscape in the next few months?</b></p>

<p>The major new feature coming up in the next month is Mentals' layers work, which extends SVG's grouping feature to allow artists to put drawing objects on different layers.</p>

<p>We're also on the short path to the 0.40 release, so you can expect a new Inkscape release in November. Right now, a lot of bugs are getting fixed as developers prepare for this release. Particular attention is being placed on Windows bugs this time, so maybe Windows users will gain the same level of stability that the Linux Inkscape users have been enjoying.</p>

<p>In the months to come after this release, I suspect a lot of attention will focus on some planned changes to the user interface, including conversion from GTK+ to Gtkmm, overhauling several of the dialogs including Object Properties and Gradient Fill/Stroke, and updating the look of the icons and cursors. Of course, what actually happens also depends on the passions of new developers that join in with their own itches to scratch!</p>

<hr>

<p><em>This is the official start of the Dot's Guest Series, which will house articles and interviews relevant and interesting to the KDE community, but not necessarily about KDE. The previous article, despite its sneaky start, was <a href="http://dot.kde.org/1097763942/">Pete Gordon on Portable Usability Laboratories</a>.</em></p>





