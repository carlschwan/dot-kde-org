---
title: "OSDir.com: Kompos\u00e9 - Expos\u00e9-like, Full Screen Task Manager"
date:    2004-09-09
authors:
  - "smallett"
slug:    osdircom-komposé-exposé-full-screen-task-manager
comments:
  - subject: "note"
    date: 2004-09-09
    body: "Hans Oischinger appears to be the primary author of Kompos&eacute; and not \nLuis Mayoral as implied by the review.  Apparently Luis does the Debian packages.  It actually isn't too obvious from the project page who the author is, but you can read <a href=\"http://waidlafragger.de/~blog/oisch/\">the author's blog here</a> (currently he fears Apple might own a patent on Expos&eacute;).\n\n"
    author: "Navindra Umanee"
  - subject: "Re: note"
    date: 2004-09-09
    body: "About patents, how do what enlightenment has been doing for years (5-6 years?) do compare with expose? Things like the pager with mini screenshots of the windows content and the iconbox with mini-window shots: If I resize a pager or an iconbox to full screen, I have more or less the same..."
    author: "oliv"
  - subject: "Patents"
    date: 2004-09-09
    body: "Not a patent on expose, but transcluent windows.. interesting considering release of xorg 6.8.0.\n\nhttp://tinyurl.com/2hyct\n"
    author: "smt"
  - subject: "Re: Patents"
    date: 2004-09-09
    body: "Luckily that patent isn't granted yet, and it only appears to cover the specific case of windows that are originally opaque, then fade slowly to being translucent while they are inactive, and then become opaque again when they change.  If a window manager does that, it might violate the patent (if it is granted).  Otherwise, translucent windows are just fine.  In fact, I know of specific prior art for this patent:  Bluecave Winamp Slider has been doing this exact thing since 2001, and the patent was filed in 2003.  So take that, stupid patent!  There's nothing to worry about here."
    author: "Spy Hunter"
  - subject: "xorg"
    date: 2004-09-09
    body: "Does Kompos\u00e9 work without xorg's new Composite extension?  Can it take advantage of it when it's available?"
    author: "Spy Hunter"
  - subject: "Re: xorg"
    date: 2004-09-09
    body: "Yes. The next version (look into CVS!) will take advantage of it."
    author: "Anonymous"
  - subject: "Re: xorg"
    date: 2004-09-09
    body: "Saweet."
    author: "Spy Hunter"
  - subject: "Re: xorg"
    date: 2004-09-09
    body: "In particular, the next version won't support people without composite.\n(As far as I remember)"
    author: "JohnFlux"
  - subject: "Re: xorg"
    date: 2004-09-10
    body: "Although Hans gave that impression, I now think (looking at the blog) that he's currently got it detecting and using XComposite when available, otherwise working old-style.\n\nThe old-style screenshotting is needed in the code anyhow, due to unforeseen limitations of XComposite (see his blog).\n\nOn an unrelated note, I'm planning to do a BSD port (i.e. an addition to the ports collection - it runs fine on BSD if you have KDE 3.2) of this once I've got on more on top of my work :-)"
    author: "Mark"
  - subject: "Mini Review"
    date: 2004-09-09
    body: "I've used skippy before, and just installed the SUSE 9.1 rpms from the sf.net site.\n\nKompose \"feels\" much more like a KDE app than skippy.  Skippy is much more minimalist and seems faster, but kompose sits in the system tray and has a KDE-esque configure dialogue.  Skippy feels like an X app... it works well, but kompose has the UI advantage.  Plus kompose has some nifty features above Expose like minimizing windows in \"global view\" and viewing all windows grouped by desktop.  Stuff that makes sense on a X server.\n\nBoth have the fatal flaw of having to move each window to the foreground, wait for it to redraw and then take a snapshot before it can display the \"global view\".  Maybe the new extensions in x.org can help limit that?"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Mini Review"
    date: 2004-09-09
    body: "> Both have the fatal flaw of having to move each window to the foreground, wait for it to redraw and then take a snapshot before it can display the \"global view\". Maybe the new extensions in x.org can help limit that?\n\nThe Xcomposite extension can help to limit that (but only for non-minimized windows on your current desktop). I read that the \"Skippy-XD\" version of Skippy already supports it.\n"
    author: "Anonymous"
  - subject: "Re: Mini Review"
    date: 2004-09-14
    body: "except that Skippy-XD, while making use of the Composite extensions (previously found on the freedesktop.org X server), seems to have some problems with KDE, with the same screenshot being repeated over different windows and window-switching not actually working :("
    author: "gryphonlord2001"
---
<a href="http://kompose.berlios.de/">Kompos&eacute;</a>
is an Expos&eacute;-like (OS X) full screen task manager for KDE that has just
gone to release 0.4.1 in two months. You really have to see it to
understand, but imagine that tiny little box in your taskbar that
indicates all our running windows blown up and on the entire desktop.
Then add a tiny screenshot for each. This kind of task manager is new to
Linux desktop users, but has been a staple of OS X users for nigh on a
year and a half or so. If you have seen Expos&eacute; in action on a OS X
machine then you have a good idea of what Kompos&eacute; is like. Check out <a href="http://osdir.com/Article1581.phtml">our screenshot review</a> to see what it's all about.

<!--break-->
