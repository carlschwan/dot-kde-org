---
title: "KDE-CVS-Digest for March 12, 2004"
date:    2004-03-13
authors:
  - "dkite"
slug:    kde-cvs-digest-march-12-2004
comments:
  - subject: "Textpad"
    date: 2004-03-13
    body: "Textpad is still the best editor for me over Kate when it comes to usability and options. But unfortunately the shareware is not available for Linux. It runs in Wine but poor.\n\nhttp://www.textpad.com/\n\nScreenshots: http://www.textpad.com/about/screenshots/index.html\n\nI think Kate shall adopt more of textpads ideas. However I saw a lot of progress in the past with Kate. It also needs a integration of a KHexedit mode.\n\nWhat I wonder the most is that I don't find an editor that 'pretty prints' the source, this makes it hard to edit html files.\n\n\nAnyway, I don't think it is fair to compare KDE programs always with the state-of-the-art alternative under windows. \n\nKMail integrates mailing lists? Then it may be the superior tool. Mailman uses standard reply mails but you have to handle them manually, quite annoying. A usenet subscription is still easier, but the usenet an nntp is actually not very different from a mailing list (despite the rough tone),  but the usenet died away because users prefer a non-decentralized server environment that makes it easy to create your own lists and list policy."
    author: "Bert "
  - subject: "Re: Textpad"
    date: 2004-03-13
    body: "\"Anyway, I don't think it is fair to compare KDE programs always with the state-of-the-art alternative under windows.\"\n\nLOL!!\n"
    author: "David"
  - subject: "Re: Textpad"
    date: 2004-03-13
    body: "COTFLAHAHA!\n\n*wipes tear off*"
    author: "Pilaf"
  - subject: "Re: Textpad"
    date: 2004-03-13
    body: "that is rather amusing"
    author: "macewan"
  - subject: "Re: Textpad"
    date: 2004-03-13
    body: "also a bit ignorant as well to react that way. I agree that he uses strong words. But maybe, just maybe he has some good points. \n\nFab"
    author: "fab"
  - subject: "Re: Textpad"
    date: 2004-03-13
    body: "Well yer, I'll give hime that. There are some good programs on Windows, and the best are not developed by Microsoft obviously. Textpad is pretty good, but I haven't had the chance to check it out fully simply because I haven't needed it.\n\nThe point is that it is nit-picking most of the time. Kate does a pretty fabulous job and negates the need for shareware."
    author: "David"
  - subject: "Re: Textpad"
    date: 2004-03-13
    body: "As much as I like KDE, sometimes it is true...\n"
    author: "Ariel Arjona"
  - subject: "Re: Textpad"
    date: 2004-03-13
    body: "It's totally true; see dopus versus konqui for example :)\n\nor trillian _pro_ versus kopete.\n\n"
    author: "anon"
  - subject: "Re: Textpad"
    date: 2004-03-13
    body: "I agree. Textpad is a great editor. I used it for a while. The other one is jEdit. The \"j\" is deceiving. It's not only for Java. It's search facility is just remarkable. Some other features are the AStyle plugin - to tidy up code, the JDiff plugin - for comparisons (=kompare), block selection mode (Ctrl-Shift), split screens, colorizations of different language elements. If Kate should adopt the best features from both then KDE has a great editor.\nBut that's what the wishlist items category on http://bugs.kde.org/ is for."
    author: "Claus"
  - subject: "Re: Textpad"
    date: 2004-03-13
    body: "\"I think Kate shall adopt more of textpads ideas.\" \n\nWhich features do you mean ? I don't know Textpad, so I don't know which features it has.\nCan you please explain a bit more ?\nOf course the kate developers want kate to become the best text editor.\nYou can also reply via private mail.\n\nBye\nAlex\n"
    author: "aleXXX"
  - subject: "Re: Textpad"
    date: 2004-03-13
    body: "Hi,\n\nit may surprise you, but \"be more like textpad\" is not a wish well perceived in our community. We want concrete suggestions with details. Preferably not in comments here, but as formal bug reports, but that is not well adhered to around here anyway. This way things can turn out good. You must understand that about everybody reading here almost exclusively uses KDE nowadays.\n\nThen, Usenet never died, at least not because we stoped using it exclusively, it lives on. \n\nRegarding comparing KDE and Windows Shareware programs, well, it's just too expensive and nagging (in the real sense) to compare it. Kate just works nicely. \n\nI want to mention, how I hated it that Kate warned of unsaved changes after a cvs commit to that file, just because of the date touch it gave. Obviously this will be a thing of the past with KDE 3.2.2 to be released. Thanks a lot for that.\n\nI also wait for real scripting support in Kate, so it will become more of an Emacs. Scripting is not yet as onmipresent as it could in KDE. Undestandably, people want to make it secure, and I hope they shall succeed.\n\nYours, Kay\n\n\n"
    author: "Debian User"
  - subject: "Re: Textpad"
    date: 2004-03-14
    body: "Kate is wonderful. More progress can be expected, textpad is ten years old and mature software. But it is far from perfect.\n\nEmacs and VI are unix style, Kate is user style. Real unix hackers will not switch to Kdevelop or Kate. "
    author: "Gerd Br\u00fcckner"
  - subject: "Re: Textpad"
    date: 2004-03-14
    body: "\"Emacs and VI are unix style, Kate is user style. Real unix hackers will not switch to Kdevelop or Kate.\"\n\nDefine Unix and User styles. And why won't they change to KDevelop and Kate?\n\nSource\n"
    author: "Source"
  - subject: "Re: Textpad"
    date: 2004-03-13
    body: "Just viewed the Screenshots.\n\nConclusion:  What a butt-ugly UI.  And don't get me wrong I'm not a KDE Zealot and prefer NeXTSTEP/Openstep's approach when people knew how to develop to NeXT's UI Design Standards.\n\nKate has me writing all of my XHTML/CSS code and I have yet to touch Quanta-Plus.\n\nWhat I would like Kate to extend, or perhaps it already does this and I haven't explored it enough, is to allow me to pre-define Code Formatting to a standard.\n\nFor instance when I worked at NeXT we had an internal structure all development of Objective-C had to go by and let's just say it's not completely known nor adhered to still in the OS X World--I blame Apple for not fully publicizing such a benign document.  It promoted self-documenting to an Art.\n\nApplying the same sort of design to writing Java or XHTML or XML or CSS or XSLT, etc., is quite simple.\n\nKate to me is a joy to use.  Designing templates is visually clean and minimalistic.\n\nWishlist:\nWhat I would like Kate to allow me to do is to select a range of files I have opened, leaving others out of the range and still open, and close just between the first and last one I have selected, something common in applications.\n\n"
    author: "Marc J. Driftmeyer"
  - subject: "Re: Textpad"
    date: 2004-03-14
    body: "You are 100% right. Textpad looks bad under Luna, but the persons that use textpad disable the teletubbie style of XP."
    author: "Gerd Br\u00fcckner"
  - subject: "Re: Textpad"
    date: 2004-03-14
    body: "Besides Luna looks 100 times better than keramik.\nI think 9 out of 10 people will agree.\nJust think of keramik's terrible window decoration.\n"
    author: "Notheydont"
  - subject: "Re: Textpad"
    date: 2004-03-14
    body: "Unfortunately, yes most people seem to agree with you on the Keramik question. Unfortunately, the consensus is that we're stuck with it until 4.0, as the core people are mostly in agreement that it only confuses the user if we change the default look every major release. I would probably tend to agree with them, too.\n\nHowever, when this is said, 4.0 will see a new window decoration, and unless something better comes up before then, it seems it will be Plastik. And here as well, I would probably tend to agree ;)\n\nThis, of course, doesn't stop distributions from selecting another default style (Mandrake 10 has Galaxy2 for example, SUSE I suppose has it's own style as well? Others simply select another style as default)."
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: Textpad"
    date: 2004-03-14
    body: "That's a matter of taste. I like Luna and I find 'traditional' Windows style really ugly, however the majority of people I know (especially the pseudo-technical users) turn off the themeing service as the first thing they do after a fresh Windows setup.\nI personally like Keramik a lot, more than Luna and more than Plastik.\nAnd I find the window decoration fantastic.\nI like all the bluish stuff like Aqua and Crystal, they relax my eyes and make me work in a more pleasant environment\n"
    author: "gigi"
  - subject: "Re: Textpad"
    date: 2004-03-15
    body: "Well, I for one think the Keramik windec looks great. The widgets aren't that hot, though, so I prefer to use Keramik as windec and Plastik as widget theme. :-)"
    author: "Skoterf\u00f6rbundets v\u00e4nf\u00f6rening"
  - subject: "Re: Textpad"
    date: 2004-03-15
    body: "I think 11 out of 14 people would agree that you just made that statistic up."
    author: "Scott Wheeler"
  - subject: "Re: Textpad"
    date: 2004-03-16
    body: "Yeah, they prefer that flat, dull, lifeless Luna ripoff, Plastik."
    author: "Tukla Ratte"
  - subject: "Re: Textpad"
    date: 2004-03-14
    body: "Looking at the feature list for Textedit, the only thing it has that Kate perhaps should is macro. The other features (like file diffs) are part of the other KDE programs, and as far as I'm concerned, it can just stay that way. There is still some of the Unix small-tool philosophy around.\n\nI really like KMail's mailing list handling, its a fairly unique feature. KMail also makes using various email aliases easier (though not as easy as pine, where you can just type in your From address like any other field). Though I don't like Kmail's imap support as much as Thunderbird's, so I use the latter."
    author: "Ian Monroe"
  - subject: "Re: Textpad"
    date: 2004-03-14
    body: "You can type in the 'From' address in KMail if you want to do that. Simply enable it in the composer. 'View->From'."
    author: "Dawit A."
  - subject: "Re: Textpad"
    date: 2004-03-14
    body: "Monolithic Apps that try to solve a multitude of many smaller applications, in one are cumbersome, slow, and most of all ineffective due to it losing a minimalist viewpoint.\n\nOne of the strengths of Apple's Cocoa is Services allowing many small apps to work harmoniously without having many small apps running continuously."
    author: "Marc J. Driftmeyer"
  - subject: "Re: Textpad"
    date: 2004-03-17
    body: "Some kind of scripting inside Kate would be really nice, but I think that is something that is even being worked on? I like to be able to create simple scripts as coding aids, and Emacs allows me to use it (although using the more or less horrid Elisp). Ecmascript (whatever it really is), Javascript, Qt-Script or (my favourite) Python would be extremely nice to have inside Kate and with some easy facility to bind the macros to keys. If the macros could even open simple dialogs for asking stuff from the user it would be really nice. :)\n"
    author: "Chakie"
  - subject: "Re: Textpad"
    date: 2004-03-14
    body: "The last time I used Windows (around 1998), I used Textpad (it was called TextPad32 back then) as my windows editor of choice.  It is (was) really quite excellent, and I remember when Kate came out, I was impressed by how similar they were.  Kate was what pulled me from Konsole and vi.\n\nLooking at the screenshot, I do not like the way that the UI has gone, and I appreciate the way Kate has progressed.  \n\nThe most exciting thing that I've read in years is the adaptive logical parser that lets Kate determine blocks of code or text and apply transformations to them... there was an article on the developer blogs about it.  I really hope that it comes to fruition.  It would make for seriously powerful scripting capabilities and codefolding and navigation beyond anything out there.  Plus realtime selfdocumenting and selfabstracting puretext documents."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Textpad"
    date: 2004-03-14
    body: "I agree that Kate has progressed very well, especially now that the flickering is gone.\n\nOne thing I like is that it applies syntax highlighting in blocks for javascript or css style sections in web pages."
    author: "Fredrik"
  - subject: "Re: Textpad"
    date: 2004-03-14
    body: "If you have specific feature requests, don't hesitate to file wishlist issues (over at http://bugs.kde.org), write the kwrite-devel list or even me personally. Detailled descriptions and clear explanations helps a lot :)"
    author: "Anders"
  - subject: "Re: Textpad"
    date: 2007-10-02
    body: "LOL, Usenet will never die. Its been around forever and wont go anywhere soon.\n\nJT\nwww.Ultimate-Anonymity.com\n\"Anonymous Email & Web surfing Tools\""
    author: "John Hopkins"
  - subject: "multimedia"
    date: 2004-03-13
    body: "the part about \"amaroK now supports NMM architecture\" has made me wonder: where is kde going with things like the soundserver and multimedia architecture?\n\nI think I heard that gnome is looking to replace their sound server with something new as well.  Could the kde developers please choose something that the gnome developers also agree on.  Having both esd and arts running for different apps really sucks, especially since my integrated soundcard does not do mixing."
    author: "theorz"
  - subject: "Re: multimedia"
    date: 2004-03-13
    body: "Don't confuse the sound server and the multimedia architecture. They're interconnected, but different things.\n\nTechnically, nothing prevents you from having an NMM output plug-in for artsd and another for esd.\n\nHopefully, if we're lucky, in two years from now everyone will have settled on NMM for the multimedia architecture (although I wouldn't trust the GNOME devs to make a sound technical choice, esp. now that they have GStreamer...) and JACK for the sound server. This would be really, really nice..."
    author: "Anonymous Coward"
  - subject: "Re: multimedia"
    date: 2004-03-14
    body: "NMM is a candidate for freedesktop"
    author: "ac"
  - subject: "Amarok"
    date: 2004-03-14
    body: "Amarok should support drag 'n drop from konqueror to the amarok main window as every other media player. "
    author: "Tester"
  - subject: "Re: Amarok"
    date: 2004-03-14
    body: "Please consider joining the KDE-Multimedia quality team and help us improving amaroK with work like testing, extending the documentation, webmastering, etc.  There's a big chance that the feature you are requesting will be implemented soon, when you file it properly on http://bugs.kde.org.\n\nWe have assembled a list of open tasks for the project, you can find it on our KDE-Wiki page:\n\n        http://wiki.kdenews.org/tiki-index.php?page=amaroK\n\n\nThanks, Mark.\n"
    author: "Mark Kretschmann"
  - subject: "Re: Amarok"
    date: 2004-03-14
    body: "As mentioned in this week's cvs digest. I fixed this bug last week."
    author: "Max Howell"
  - subject: "Re: Amarok"
    date: 2004-03-15
    body: "Great.\n\n"
    author: "Tester"
  - subject: "Evolution?"
    date: 2004-03-14
    body: "Why not just take Evolution and put our Macalike interface over the top of it and call it Kvolution? It's light years ahead of KMail IMHO and gives the impression of being a much better respected heavyweight office application rather than the DE curiosity that is KMail.."
    author: "Gil"
  - subject: "Re: Evolution?"
    date: 2004-03-14
    body: "Sorry ?\nSo heavyweight apps are preferred ?\nWhy is kmail a DE curiosity ?\nWhat is actually a \"DE curiosity\" ?\nWhat is \"our Macalike interface\" ?\nEver tried kontact ?\nWho respects evolution better than kmail/kontact ?\n\nNevertheless, feel free to port evolution to KDE...\nAlex\n"
    author: "aleXXX"
  - subject: "Re: Evolution?"
    date: 2004-03-14
    body: "Don't feed the trolls."
    author: "Anonymous"
  - subject: "Re: Evolution?"
    date: 2004-03-14
    body: "> Nevertheless, feel free to port evolution to KDE..\n\nHow could he?  He's a troll, not a developer.\n\n"
    author: "Kil"
  - subject: "Re: Evolution?"
    date: 2004-03-14
    body: "LOL!!\n\nCompare apples with apples.\n\nEvolution: Has taken four/five years or more to get into it's current pretty usable state (and has had to have had paid, employed developers working on it to boot!), but it is still slow and uses crap programming tools (that's my opinion as a sometime Windows programmer, not a KDE-user). Wait until they re-write it in Mono!\n\nKontact (that is KDE's equivalent - KMail is a component): Fast, integrated and developed pretty fast considering it hasn't yet been in existence for two years, and it isn't even version 1 yet! If I want an advert for Qt/KDE programming - there it is.\n\nIf I were someone moving to Linux/free desktops in general (who didn't know anything about them) who was given a choice, guess what I'd pick?\n\nComments like this only make it worse I'm afraid."
    author: "David"
  - subject: "Re: Evolution?"
    date: 2004-03-15
    body: "Well, you are very dishonest when you say Kontact has been in existence for less than two years. Most of it has been there for more than two years. I consider the clendar and kmail to have been there and those are probably the main parts of Kontact.\n\nIf you were moving to an operating system, would you choose something that works better than another, or just something that might one day become better than the other.\n\nThere is much more to Evolution than just mail yes, but it does what it does extremely well. In fact, it has innovated in many ways that are now being copied albeit unsuccessfully by even Microsoft themselves. (Search folders anybody)\n\nI think Evolution is actually one of the examples of OSS software actualy innovating, regardless of what some people may claim.\n\nAnd where do you get the idea that Evolution is developed slowly?"
    author: "Maynard"
  - subject: "Re: Evolution?"
    date: 2004-03-15
    body: "\"Well, you are very dishonest when you say Kontact has been in existence for less than two years. Most of it has been there for more than two years. I consider the clendar and kmail to have been there and those are probably the main parts of Kontact.\"\n\nPutting that all together in a modularised way that works together well is hard.\n\n\"If you were moving to an operating system, would you choose something that works better than another, or just something that might one day become better than the other.\"\n\nNot one day - now. Kontact already works very well - sub version 1.\n\n\"And where do you get the idea that Evolution is developed slowly?\"\n\nIt's not developed slowly. That's the point."
    author: "David"
  - subject: "Re: Evolution?"
    date: 2004-03-14
    body: "> IMHO\n\nIMHO = In my humble opinion ?\n\nYou don't seem very humble to me..."
    author: "OI"
  - subject: "Re: Evolution?"
    date: 2004-03-14
    body: "Why not just crawl back under the GNOME rock you came from? \n\nHere's another troll posting by Gil: \nhttp://dot.kde.org/1066553018/1066578010/1066578960/1066582910/1066584382/1066597926/\n\n"
    author: "Kil"
  - subject: "Re: Evolution?"
    date: 2004-03-14
    body: "Yer well, you're going to get them. Just dismiss them, if they need dismissing, and move on. There are some people out there you can have some fun with though :)."
    author: "David"
  - subject: "Sorry, to go Off Topic"
    date: 2004-03-14
    body: "However, I really need some clarification on this.\n\nWhat is Gstreamer really? I read on their website that it is not a sound server (http://www.freedesktop.org/~gstreamer/data/doc/gstreamer/head/faq/html/chapter-general.html#general-sound-server) if it isn't a sound server what doe sit do, I do not really understand. Is there anything like Gstreamer at the moment?\n\nAlso will KDE adopt JACK as the sound server for KDE 4? It seems like a great sound server and tightly integrated with ALSA and LADSPA\n\nhttp://www.agnula.org/documentation/dp_tutorials/alsa_jack_ladspa/\n\nPlease clear this up for me someone.\n"
    author: "Alex"
  - subject: "Re: Sorry, to go Off Topic"
    date: 2004-03-14
    body: "> Also will KDE adopt JACK as the sound server for KDE 4? It seems like a\n> great sound server and tightly integrated with ALSA and LADSPA\n\nI guess, as usual, everyone perfers to make his own stuff.\n\nAnd you know, all the stuff on freedesktop is evil old-school-c-gnome stuff\nanyway.\n\nAs mentioned above, NMM is really a freedesktop candidate. But I guess it's\nbeing programmed by modern-c++-boneheads and they are not able to lower\nthemselves to freedesktop level...\n\nThings could be so nice. Imagine kde and gnome having the same multimedia\ninfrastructure.\n\nbtw: arts sucks big time"
    author: "ac"
  - subject: "Re: Sorry, to go Off Topic"
    date: 2004-03-15
    body: "Gstreamer is a multimedia framework which enables you to write applications that take advantage of certain capabilities of the framework. Think of it like a toolkit, except that instead of  dealing with how apps look and behave, it deals with media items instead. So it provides ways for multimedia apps to decode, encode and play around with media files. Best to go to their website to learn about it.\n\nAn example of what it can do is the following. Think of the following as a 3 stage pipeline. you need input, the source (src), which is filesrc in this case. You are taking a file here. Then you need to decode the file (in this example), so since you have an mp3, you need an mp3 decoder, in this case, mad. Then you need output. We want to hear the mp3, so we output the result to, in this case, artsdsink.\n\ngst-launch filesrc location=music.mp3 ! mad ! artsdsink.\n\nIf we wanted to transcode an mp3 to ogg, for example, we would replace artsdsink with vorbissink, and we would get an ogg vorbis file.\n\nIf we prefer a different decoder, e.g, lame, we could replace mad with lame ad be good to go.\n\nYou can also add effects to the pipeline, so you could process the sound before you hear it.\n\nSo you can create any manner of apps, which basically play around with pipelines to do stuff with media. Rhythmbox and Juk basically use gstreamer (as an option)\n\nTo support extra formats for example, they only need to get a new plugin to put in that pipeline. And also, gstreamer can find the correct decoder to use for you if you use spider instead of mad in the example above.\n\nIts really powerful, and there is absolutely no need to have two competing frameworks like that."
    author: "Maynard"
  - subject: "Elitest behaviour?"
    date: 2004-03-14
    body: "Will some of you please read over your posts and consider your attitudes? You can also happily say things like \"KDE application A is better than Windows/Gnome applcation B\", but whenever the comparison goes the other way the person is labelled a troll. Instead of attacking these people, you should critically look at the application and the person's views and try to improve KDE. Saying things like TextPad looks bad in screenshots is unbelievably childish."
    author: "Jack"
  - subject: "Re: Elitest behaviour?"
    date: 2004-03-14
    body: "Hi\n\nIts not childish. Its a fact. The screenshot shows a bad UI. Any longterm user of that program will tell you exactly that. Its UI has actually degraded than it was before. \n\nRegards\nRahul"
    author: "Rahul Sundaram"
  - subject: "Re: Elitest behaviour?"
    date: 2004-03-14
    body: "I hardly think that looking at a screenshot of the much aclaimed Textpad and saying it looks terrible is a good critical review of an application. Its fair enough to make comments on the UI, but the post was written in the sense that the screenshot is poor so there is no point looking any further. Emacs and vi are widely used text editors and their screenshots are not what sells them either.\n\nI am aware screenshots are useful, but many comments on this topic just reek of elitism. It's nice to be proud of KDE but don't let it cloud your judgement or attitude when somebody prefers something else."
    author: "Jack"
  - subject: "Post-xfree86 X11 and GPL'ed security/tunneling?"
    date: 2004-03-15
    body: "arts->mas\nlibart->cairo\nxfree86->xlibs/xserver\nopenssh->lsh\nopenssl->gnutls\n\nFor kde-3.3 (compile time config) or kde-4.0 (native default)?"
    author: "Nobody"
  - subject: "Re: Post-xfree86 X11 and GPL'ed security/tunneling?"
    date: 2004-03-15
    body: "What\u00b4s wrong with openssh?\nAnd isn\u00b4t gnutls immature?"
    author: "Roberto Alsina"
  - subject: "Re: Post-xfree86 X11 and GPL'ed security/tunneling?"
    date: 2004-03-16
    body: "Just remmebered few discussions in the past about moving more and more programs (and binded programs) to GPL, and both openssh/openssl are using the BSD license.\n\nSure gnutls is immature, but so is kde-4.0 ;-).\n"
    author: "Nobody"
  - subject: "Re: Post-xfree86 X11 and GPL'ed security/tunneling?"
    date: 2004-04-06
    body: "openssh has had more few security holes and lsh is more gnu-like and generally better thought out, imho. (lsh lacks mainly an agent and agent-forwarding.)\n\ngnutls isn't immature anymore, they're past 1.0 and works on a devel branch. 1.0.x works great for me."
    author: "Jonas"
---
In <a href="http://members.shaw.ca/dkite/mar122004.html">this week's KDE CVS-Digest</a>: 
A new <A href="http://www.kde.org/areas/sysadmin/">Kiosk</A> configuration front-end. 
<a href="http://amarok.sourceforge.net/">amaroK</a> now supports <A href="http://www.networkmultimedia.org">NMM</A>  architecture. 
<A href="http://kate.kde.org/">Kate</A> adds an autobookmark editor. 
KGeography, a geography teaching tool is in kdenonbeta. 
<A href="http://kmail.kde.org/">KMail</A> adds automatic mailing list handling.
And work continues on <A href="http://www.koffice.org/kexi/">Kexi</A> with a property editor and form framework. 
<!--break-->
