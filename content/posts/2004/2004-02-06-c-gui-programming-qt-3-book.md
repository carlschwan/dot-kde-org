---
title: "\"C++ GUI Programming with Qt 3\" Book"
date:    2004-02-06
authors:
  - "binner"
slug:    c-gui-programming-qt-3-book
comments:
  - subject: "free qt software for windows!"
    date: 2004-02-06
    body: "So if I understand this correctly, I can buy the book and compile my C++ + STL + Qt apps for the people that are still using windows, without needing anything but e.g. Windows XP and the accompanying CD?\n\nThat would be great! Finally a use for the XP parition on my laptop. (although putting music on it would a good idea too)"
    author: "Jos"
  - subject: "Re: free qt software for windows!"
    date: 2004-02-06
    body: "If your apps are non-commercial, freely redistributable and the source code is available, yes."
    author: "Anonymous"
  - subject: "Re: free qt software for windows!"
    date: 2004-02-07
    body: "Lets say I would sell the CD which includes the QT-Non Commercial App for 500 Starbucks. Would this be legal then?"
    author: "anonymous"
  - subject: "Re: free qt software for windows!"
    date: 2004-02-07
    body: "If you make the same application for the same customers availabe at no cost perhaps. Your behavior is the exact reason why there was no current non-commercial Qt Windows edition available for such a long time."
    author: "Anonymous"
  - subject: "Re: free qt software for windows!"
    date: 2004-02-09
    body: "What do you mean for such a long time! It's still not available!\n\nTrolltech is stupid not to release one, if they had a GPL edition available than that would automatically popularize all their platforms bringing more life to Mac and Linux too due to the crossplatform nature of Qt and the availability of source, the best GPL applications for Windows would be availabe in Linux too.\n\nNot to mention that they would be able to get free press from 95% of the computer using world and therefore interest many more people.\n\nReally, I am very dissapointed in that aspect and I think the trolls are acting foolishly."
    author: "Alex"
  - subject: "Re: free qt software for windows!"
    date: 2004-02-14
    body: "I have the QT Non-Commercial edition and its compiled so it can only be linked to dynamically, not statically, it also requires the non-commercial edition installed on the computer to run.  Plus it puts [Non-Commercial] at the front of the main windows title."
    author: "Corbin"
  - subject: "Re: free qt software for windows!"
    date: 2004-02-07
    body: "It doesn't matter if your app is commercial or not.  What matters is if it's proprietary.  You CAN use the GPL/free Qt for commercial GPL software.  You CAN'T use the GPL/free Qt for non-commercial proprietary software.  There's no relationship between whether your app is commercial and whether you can use the GPL'd Qt.\n\nIf you want to develop proprietary apps (commercial or otherwise), you need the \"commercial\" Qt.  Trolltech's choice of language really muddies this whole issue.  If in doubt, just read the GPL and see what it allows you to do."
    author: "ac"
  - subject: "Re: free qt software for windows!"
    date: 2004-02-09
    body: "\"It doesn't matter if your app is commercial or not.\"\n\nThe question concerned the non-commercial Qt that comes with the book, not the GPL/QPL edition for X11 or MacOS. In reference to the book edition it actually *does* matter if the application is commercial.\n\n\"10. Distribution of the Applications are subject to the following requirements: \n(i) The Application(s) must be distributed for free and/or be freely downloadable; \n(ii)The Application(s) must be licensed under a license which is compliant with the Open Source Definition version 1.9 as published by the Open Source Initiative [...]\"\n(Qt Book Edition Non-Commercial License Agreement North and South America Agreement version 1.1)\n\n\nSo the application that links to the non-commercial Qt must be open source and available at no cost."
    author: "/dev/null"
  - subject: "Re: free qt software for windows!"
    date: 2004-02-06
    body: "Indeed, I bought the book for this very reason, as I am in a Computer Graphics class where I must turn in my projects on Windows, but my testing/development box runs Linux.  This makes my task incredibly easier (I had been using FLTK+OpenGL earlier).\n\nBe aware, however, that the Qt Windows Non-Commercial edition is distributed as a shared library (DLL), not a static library.  You can't redistribute the Qt Non-Commercial from the book, but I'm not sure if that applies to the Qt DLL itself, as it will obviously be required for the program to run.\n\nActually, I just checked the license:\n\n11. Copies of Redistributables may only be distributed with\nand for the sole purpose of executing Applications\npermitted under this Agreement that Licensee has created\nusing the Licensed Software. Under no circumstances may any\ncopies of Redistributables be distributed separately.\n\nAppendix 1:\nParts of the Licensed Software that are permitted for \ndistribution (\"Redistributables\"):\n- The Licensed Software's main and plug-in libraries in \ndynamically loaded library / shared object format.\n\nIt also says you can install Qt-Win on more than one computer, as long as you're the only one using any of the copies.  Also, the software you develop MUST be under an OSI approved Open Source software license, and you have to give it away.  I know, I know, that's not a problem for most of us, but just be aware. ;-)"
    author: "Michael Pyne"
  - subject: "Re: free qt software for windows!"
    date: 2004-02-06
    body: "Here is the complete North/South American license, attached."
    author: "Michael Pyne"
  - subject: "Re: free qt software for windows!"
    date: 2004-02-06
    body: "Section 11 says :\n11. Copies of Redistributables may only be distributed with \nand for the sole purpose of executing Applications \npermitted under this Agreement that Licensee has created \nusing the Licensed Software. Under no circumstances may any \ncopies of Redistributables be distributed separately.\n\nTo make the thing clear, If I have a GPL application.\nI have the right to distribute it for the windows plateform \nalong with the QT.DLL. But I have not the right to distribute\nQT.DLL alone. \n\nSounds perfects for me. "
    author: "QT/GPL apps 4WIN"
  - subject: "Re: free qt software for windows!"
    date: 2005-07-27
    body: "i want to your \"free qt software\", please send  to my e-mail"
    author: "achunk"
  - subject: "Re: free qt software for windows!"
    date: 2005-07-27
    body: "Go to www.trolltech.com, that's the maker of Qt.  You can download it from there. \n"
    author: "cm"
  - subject: "I'm getting it for sure"
    date: 2004-02-06
    body: "I'm think I'm gonna buy this book only for that non-commercial Qt/Windows that comes with it.\n\nHah, I feel like a kid again, buying a crappy cereal just for the toy inside. :) (ok bad analogy, I'm sure the book itself isn't crappy, but it's still funny)"
    author: "Simon Roby"
  - subject: "Re: I'm getting it for sure"
    date: 2004-02-07
    body: "Luckily this time you only need to buy one pack of cereal box, and not 20 to get the prize ;)"
    author: "uga"
  - subject: "A very good move"
    date: 2004-02-06
    body: "I think its a very good move by trolltech to bring developers like me to develop in QT.and the contents of the book also look great and specially the content of the CD makes me to buy this book.\n                                     regards"
    author: "Bilal Anwer"
  - subject: "Another reason to get the book"
    date: 2004-02-06
    body: "I'm going to get the book, but not so I can use it on windows, since I don't have to use windows. Though it may come in handy some time. I'm going to get it because...\n\n* I like having reference material in book form. I can read it when I'm not at my desk.\n* I like to show support for the community.\n* I'm sure I'll find something good in the book. I seem to get so little time to code that taking this in the reading room is sure to inspire new code. ;-)\n\nIt's really great to have resources like this available. Hats off to Jasmin and Mark, Trolltech, Matthias Ettrich, the publishers and everyone who made this possible."
    author: "Eric Laffoon"
  - subject: "A Good Book for Reasons Beyond the CD"
    date: 2004-02-06
    body: "\nI've purchased the book and gave it a quick first pass, now I am reading it\nin more detail.\n\nI am not a professional programmer, nor have I ever programmed\nGUIs before. But I have found myself wanting to a program a GUI\nin C++ and QT seemed like a good choice. I bought it for Linux\npurposes, not windows purposes so I can't comment on the Windows\nversion that is distributed on CD. Nor can I compare this with\nany other book on the QT subject, I haven't read any. I have\nlooked at books outside the QT realm that have touched upon\nC++ and GUI programming and this book fares well. \n\nI like the book because:\n\n(a) It does not try to teach you C++, so it wastes little\n    of the readers' time before getting to the subject at hand.\n    It does get one to understand the generic aspects of GUI \n    programming within the QT context, something I needed since\n    I am new to the subject. I think some of this might be unavoidable,\n    if only for the sake of establishing the vocabulary used in QT. \n\n(b) It embraces a clean object oriented style and a philosophy that would\n    have taken me a long time to figure out on my own without\n    reading the book. There seems to be a lot of references out there\n    on the QT library. But learning how to use the library in its intended\n    way is what a book like this captures. A book such as this will make you \n    a better user much quicker. Since the authors work for Trolltech, they\n    probably have information access that advantages its readers.\n\n(c) The language in the book is simple, so are many of the examples. \n    Instead of spending too much time on specific subject, a clear\n    overview of many the library capabilities is covered.\n    I think the uniformity in the QT methodology makes it simple for one \n    to go beyond the book in terms of QT complexity. \n\nBut I think much of what makes the book good is the fact that QT is\nan excellent library that is consistent with itself. So that makes\nit a hard book to screw up to start with. \n\nI'm a chip designer by trade. So perhaps that clouds my judgement on\nhow easy QT really is to use. Dealing with ports (slots),\nsignals, and connections with events in a rigid modular (and\nsomewaht parallel) way has become second nature to me.\n\nIn that sense, don't expect any scientific revelations or engineering\ninnovations in reading a book such as this. The concepts used in QT are\ntried and tested ones. Afterall, the subject of GUIs and C++ is not a new\none. But the book is a good technical reference on QT.\n\nA COMMENT ON QT\n\nA comment falling outside of the book but within the realm of QT:\nI am disappointed QT requires preprocessing of the C++. I faill to see \nthe reason why this is necessary. I sure hope it isn't for reasons of \nsyntactic sugar. Nor do I think it is for reasons of performance. If\nfor either of these two reasons, the Trolltech should have studied\nwhat others have done in the area of signalling and C++ in \nmuch more demanding application than GUIs... The only justification \nI can think of is that the QT targets embedded systems that often\nhave C++ compilers that are watered down versions of the language.\nBut, if that is the case, I would have  preferred something that\npre-processes standard C++ into a weaker version of the language \nfor the purposes of the embedded platform. \n\nI am not a C++ purist (on most days I use a typeless language!), \nbut I have seen the dire consequences of pre-processing simpler \nlanguages than C++ when the pre-processor does not have the full-fledged properties of a solid compiler parser. Pre-processing problems I've \nencountered in the past were very rare, but when they happened, they were \nhard to find and serious. Here's hoping I'll never see one with QT. \nIn the case of QT, I think it also allows for an ambiguity: \n\nIf one releases a GPLed QT appplication, can the code released be the\noutput of the preprocessor? I hope not for legibility reasons that would\ndefy the GPL, but I don't know that you can't...\n\nAs for the complaints about using QString instead of std::string, I can see\nwhy Trolltech wants absolute control on such an important class\nto its library given the set of plaforms/uses it must cover. Again, especially\nin the embedded area of things. In this case, it's not like a new keyword\nis added to the language, it's still C++ and QString looks like a solid \nclass.\n"
    author: "BookReader"
  - subject: "Re: A Good Book for Reasons Beyond the CD"
    date: 2004-02-06
    body: "Well, the preprocessor is still from the Qt 1.x days, at this time the compilers weren't as advanced as today (gcc 2.7.1). The trolls held compatibility for very important, so they didn't change this until now. Maybe it will change for 4.0.\n\nBtw. the preprocessor moc actually works really good, I actually never experience problems. The makefiles get a bit more complicated, but qmake helps a lot with this.\n\nBye\nAlex"
    author: "aleXXX"
  - subject: "Re: A Good Book for Reasons Beyond the CD"
    date: 2004-02-06
    body: "> I am disappointed QT requires preprocessing of the C++. I faill to see the reason why this is necessary. \n\nRead http://doc.trolltech.com/3.3/templates.html"
    author: "Anonymous"
  - subject: "Re: A Good Book for Reasons Beyond the CD"
    date: 2004-02-07
    body: "Thanks, it was worth reading this.  \n\nMuch of what I earlier called a weakness is presented as a strength.\nSome of the 5 reasons given go in the opposite direction of what I have\nargued (1. syntax matters, 2. precompilers are good, \n4. calling performance is not everything), so I won't bore everyone with \nrehashing my views.  As for point 5 (no limit), I think it is the type\nof argument one makes afer the fact of deciding upon preprocessing.\nI must admit some of the language in point 5 is beyond my comprehension,\nis this some of internal lingo to Trolltech's framework?\n\nI have seen elegant and syntactically clean ways of doing C++ signalling\nin other projects. However, I must concede that those signals do take up\nmore memory. But in those application, execution speed rules the day \neventhough the signal count is still very large. For some reason, I don't \nthink signal memory consumption from a GUI is that big an issue.\nBut the argument against that comes down to embedded systems again.\nThe other fact that I must concede is that the user base I am considering\nin my arguments stops at one user. Trolltech probably thinks things out in a \nway compatible with the majority of its users, a majority in which I \nprobably don't belong on the issue of pre-processing. \n\nI can also attest that there are still some development environments that run \non gcc 2.7.1 and will be for quite some time. Such an environment can be\nfrustrating to the modern C++  programmer. But to address those compilers,\nI would have preferred a down translation from standard C++ to their limited\nsubset.\n\nAs previously stated by a few, the moc is very robust. From a pragmatic\nstandpoint I am willing to accept that as an answer. Afterall C++ itself\nwas once a very robust preprocessor to C and little more. \n\nQT is the best thing (by far) that I could find when I went through\nthe list of GUI options I knew  about. While I don't agree with some\nof the points made, I can see their merits based on the arguments expressed.\nAnd I am not about a pre-processing issue change my mind on my tool of choice. \n\nBut the mere fact that such a justification exists on Trolltech web site\nindicates that the reservations about pre-processing must be heard often\nenough not to be left unanswered. I think it'll be hard to completetly \nsilence this out, in the end some of it comes to a matter of preferences that\nare not always black and white. "
    author: "BookReader"
  - subject: "Re: A Good Book for Reasons Beyond the CD"
    date: 2004-02-07
    body: "\"I have seen elegant and syntactically clean ways of doing C++ signalling in other projects. However, I must concede that those signals do take up more memory.\"\n\nI once did an experiment as part of a STL class where I wrote the same \"fake\" CD player app in Qt, Boost::signals (a pure template method), and a home grown signal/slot mechanism using templates. All were statically linked. IIRC the numbers correctly, my implementation came out to 16k, Qt's to 28k, and Boost's to 70k. Huge difference! That was with three signal/slot connections of the same signature. Both mine and Boosts would have dramatically risen with additional signatures."
    author: "David Johnson"
  - subject: "Re: A Good Book for Reasons Beyond the CD"
    date: 2004-02-07
    body: "That's interesting about the size difference.  What compiler were you using?\nDon't some(all?) versions of gcc do a poor job of elimating template specializations\nthat are not actually used?"
    author: "steve"
  - subject: "Re: A Good Book for Reasons Beyond the CD"
    date: 2004-02-07
    body: "\"But the mere fact that such a justification exists on Trolltech web site indicates that the reservations about pre-processing must be heard often enough not to be left unanswered.\"\n\nWhen discussing this all on a theoretical base it's easy to forget that languages are actually in use. Most of those who actively use Qt often experienced the advantages of Trolltech's approach first hand already and don't need an explanation anymore, the \"justification\" is only for those who tend to completly dismiss Qt due to its moc ignoring why it's actually used.\n\nSince Trolltech promotes its toolkit as portable completely without changes in the source code while keeping the choice of compilers, making down translations from standard C++ to their limited subset depending on the the platform availability of certain compiler features, certain additional libraries like std and boost etc. would completely crush the advantage of being portable; and I can't imagine any software project, be it open like KDE or commercial, to actually be interested in the work overhead introduced by having to maintain different platform forks of their code due to different feature sets available on each of them.\n\nBtw. it's \"Qt\", \"QT\" is usually considered an acronym for QuickTime."
    author: "Datschge"
  - subject: "Re: A Good Book for Reasons Beyond the CD"
    date: 2004-02-07
    body: "Well, one only need to chose a subset that is low level enough so that\none need not cover a whole spectrum of special cases. Something close\nto C (or perhaps gcc 2.7.1). In any case, the amount of work to\ncover a new platform seems more substantial outside of this from the Aqua/Mac\ncomments I encountered when making a choice of Qt. Which tells me the number\nof platforms covered by Qt is rather finite and manageable.  \n\nAs for the memory usage reported in the earlier post, those number are\nquite large from what previous experience would dictate but it is interesting to see one reported difference. \n\nIn any case, the pre-processor is here to stay even in 4.0 it seems. \nSo its up to me to warm up to that notion, or find an alternative which\ncan't see. I'm sure I'll forget I'm using the pre-processor\nafter a few weeks of Qt usage. \n\nAs for my question, does anyone know:\n\nWhen releasing code depending on Qt under the GPL, is it possible to \nrelease the preprocessor output instead of the pre-moc code? I am hoping\nthere are measures against doing that in the license, is this correct? \n"
    author: "BookReader"
  - subject: "Re: A Good Book for Reasons Beyond the CD"
    date: 2004-02-08
    body: "IMHO the reason are non-standard-compliant compilers like VC (especially for template support). So moc is required to provide the signal-slot-mechanism.\nAs the sigc++ project shows there is no need for such a precompiler if you use\na template-proof c++ compiler - and you would get a faster and more type-safe signal-slot-mechanism."
    author: "Ruediger Knoerig"
  - subject: "Re: A Good Book for Reasons Beyond the CD"
    date: 2004-02-09
    body: "  \"IMHO the reason are non-standard-compliant compilers like VC (especially for template support).\"\n\nI'm confused at your reference of VC as being a non-standard-compliant compiler.  As far as I know, which I've been reiterated by many published sources, the current VC++ is the MOST standard-compliant compiler out of any available, ESPECIALLY for template support.  Are we talking about the same \"VC\"?"
    author: "Roger"
  - subject: "Re: A Good Book for Reasons Beyond the CD"
    date: 2004-02-09
    body: "\"I'm confused at your reference of VC as being a non-standard-compliant compiler.\"\n\nI'm not, but then I'm not using the absolute latest version of Microsoft Bullsh!t Studio. Perhaps the latest output from Microsoft on this front demonstrates their commitment to following standards, just as with all their other products.\n\n(Be sure to read that last sentence with the use of sarcasm in mind.)"
    author: "The Badger"
  - subject: "Re: A Good Book for Reasons Beyond the CD"
    date: 2004-02-10
    body: "Visual Studio.NET 2003 actually seems to have got the templates right. Use ANY other version of Visual Studio (and I mean ANY other) with standard C++ and you're entering a world of pain. Some people don't use the latest and greatest."
    author: "dimby"
  - subject: "Thoughts on the MOC"
    date: 2004-02-11
    body: "I don't really have a problem with preprocessing. But why change the C++ language to add your directives? You could just as easily use C++ macros. Currently a declaration for a Qt class uses extra-C++ member scopes \"public slots\", \"private slots\", \"protected slots\", and \"signals\". This adds four  new categories to the existing \"public\", \"private\", and \"protected\". Why not instead preface the member names with macros like QSIGNAL or QSLOT instead of inventing new keywords and adding new scopes for class members?"
    author: "LuckySandal"
  - subject: "What about KDE programming"
    date: 2004-02-06
    body: "How useful is this book for someone who wants to get started coding KDE apps?"
    author: "AC"
  - subject: "Re: What about KDE programming"
    date: 2004-02-06
    body: "Since Qt is the base of KDE you need to know most parts of Qt for KDE application programming. Of course you don't have to read the book, you can also follow http://doc.trolltech.com/3.3/how-to-learn-qt.html\n\nAlso see http://developer.kde.org/documentation/other/developer-faq.html#q1.4"
    author: "Anonymous"
  - subject: "How long..."
    date: 2004-02-07
    body: "I ordered the book as soon as I heard about it (early Jan 2004). I'm still waiting for the distributor to get it in. Now expected mid-Feb (from an original 13 Jan 2004 or thereabouts).\n\nI also don't care about Win32 compat, but I do want to grok Qt.\n\nBrad"
    author: "Brad Hards"
  - subject: "Re: How long..."
    date: 2004-02-07
    body: "Think about changing your distributor. Many people got their book once it was available."
    author: "Anonymous"
  - subject: "Re: How long..."
    date: 2004-02-07
    body: "I bought it at Amazon.com on 25/01 and i got it last wednesday. Quite fast since it had to cross the Atlantic Ocean. Moreover with a 30% rebate and the euro/$ rate it is quite cheap.\nAlthough i don't have very good knowledge in C++ i think this book is really nice. There is explanation for every line of code they write. Is there any KDE developper to tell us what he thinks about this book from a KDE point of view?"
    author: "Med"
  - subject: "Also"
    date: 2004-02-07
    body: "Wasn't there a new book on KDE development in the works? I seem to remember hearing about it.\n\nAnyway the book comes with an older version of Qt, 3.2.1 which won't even let your applications run on 98 or ME if you are using a Borland compiler and on Windows didn't even install correctly even after I followed all the instructions."
    author: "Alex"
  - subject: "Re: Also"
    date: 2004-02-07
    body: "It was in the plannings but real work never started."
    author: "Anonymous"
  - subject: "This is sweeter than my mother's apple pie!"
    date: 2004-02-07
    body: "Not so much the book, cause I'm already pretty familiar with Qt, but the release of a non-commercial verison of Qt 3 for Windows. It's now possible to port my any apps I write to Windblows! Is this available elsewhere, or only with the book? \n\nI wonder if the majority of this stuff will remain valid,  or if it signifies that Qt is planning a radical change in Qt4, as they did with Qt3."
    author: "LuckySandal"
  - subject: "Re: This is sweeter than my mother's apple pie!"
    date: 2004-02-07
    body: "> Is this available elsewhere, or only with the book? \n\nIt's only available with the book at the moment.\n\n> a radical change in Qt4, as they did with Qt3.\n\nRadical change with Qt3? Compared to Qt2? Can't remember that there was one."
    author: "Anonymous"
  - subject: "Re: This is sweeter than my mother's apple pie!"
    date: 2004-02-07
    body: "OK, maybe that was a bit of an exaggeration. But when Qt 3 first came out all there was on the Windows Non-commercial side was Qt 2.3, which nobody wanted to use because it was so antiquated. Besides 2.3 not using native widgets, 3.0 introduced an entire new design model that involved ui.h files as opposed to subclasses. That was the radical change I was referring to."
    author: "LuckySandal"
  - subject: "Re: This is sweeter than my mother's apple pie!"
    date: 2004-02-08
    body: "So I've decided to experiment with the Windows version for a short while.\nTook me a while to get things installed right, I've not\nused Windows much in the past 12 years... Getting the PATH right \nrequired some tinkering and all I could do was reboot on XP. \n\nIn any case, when I compile application with non-commercial Qt for Windows, \nI get all windows with the title \"[Non-Commercial] Application Name\"\ninstead of \"Application Name\" as in the book. It is a bit annoying... \nIs there a way around this or is it a built in feature to the \nnon-commercial Qt library for Windows? \n\nThose of you debating which Borland compiler to install: when I tried\nversion 6 it stated the trial would only last 60 days. Could not find\nthe duration until I went down the path of installing it... User beware.\nI was hoping for a feature restricted version instead of a timer...\nSo I reverted to the 5.5 version, rather slow though.\n\nBut hey, it works and it was fun to see all of this on Windows. Now back\nto Linux! \n\nAlso, will this book be freely availble as a download in a short while?\n(in the spirit of open source docs)"
    author: "BookReader"
  - subject: "Re: This is sweeter than my mother's apple pie!"
    date: 2004-02-08
    body: "> Is there a way around this or is it a built in feature to the non-commercial Qt library for Windows? \n\nIt's a built-in feature if you want to call it so.\n\n> will this book be freely availble as a download in a short while?\n\nRather not."
    author: "Anonymous"
  - subject: "Re: This is sweeter than my mother's apple pie!"
    date: 2004-02-09
    body: "> Rather not.\n\nWhat do you mean rather not?  The author releases all of his stuff on the internet after the book has been circulated enough.  Why are you saying rather not?"
    author: "Roger"
  - subject: "Re: This is sweeter than my mother's apple pie!"
    date: 2004-02-09
    body: "> The author releases all of his stuff on the internet after the book has been circulated enough.\n\nWhere did you read that?"
    author: "Anonymous"
  - subject: "Re: This is sweeter than my mother's apple pie!"
    date: 2004-02-09
    body: "There was a review at slashdot of the book and one of the comments said that the book will be released for free in a couple of months. "
    author: "Michael Thaler"
  - subject: "Re: This is sweeter than my mother's apple pie!"
    date: 2004-02-09
    body: "Ok, here is the comment: http://slashdot.org/comments.pl?sid=94616&cid=8114816 - but it only talks about the book and not accompanying stuff like the non-commercial Windows Qt version."
    author: "Anonymous"
  - subject: "If you're about to buy this book at Amazon"
    date: 2004-02-08
    body: "If you're about to buy this book at Amazon then please use the referral link given in the story and on http://www.kde.org/stuff/books.php so that KDE receives a small referral fee."
    author: "Anonymous"
---
Perhaps the recent KDE and Qt releases made you want to contribute to KDE or to start your own Qt/KDE application? Then you may be interested in the new <a href="http://www.amazon.com/exec/obidos/tg/detail/-/0131240722/ref=ase_thekdesktopenvir/103-3775731-8087868?v=glance&s=books">&quot;C++ GUI Programming with Qt 3&quot; book</a>, the first official Trolltech guide to Qt 3.2 programming. It's written by Trolltech software engineer Jasmin Blanchette and Trolltech's documentation manager Mark Summerfield, with a foreword by Matthias Ettrich, Trolltech lead developer and founder of the KDE project.


<!--break-->
<p>The <a href="http://vig.prenhall.com:8081/catalog/academic/product/0,4096,0131240722-TOC,00.html">table of contents</a> shows that book teaches every facet of Qt 3 programming, ranging from basic user interfaces and layout managers to 2D/3D graphics, drag-and-drop, signaling, networking, XML, database integration, even internationalization and multithreading. The "Layout Management" chapter is <a href="http://vig.prenhall.com/samplechapter/0131240722.pdf">available as sample</a> in PDF format. The accompanying CD contains the book's code examples, the Qt 3.2 Free Editions for Unix/Linux/Mac OS X and a <a href="http://developers.slashdot.org/comments.pl?sid=95499&cid=8181832">special Qt 3.2 Non-Commercial Windows Edition</a> for Open Source developers together with a compiler (Borland C++ 5.5 Non-Commercial Edition).</p>
<p>Alex Moskalyuk posted a <a href="http://slashdot.org/article.pl?sid=04/01/28/1749200">review of the book</a> to Slashdot some days ago.</p>

