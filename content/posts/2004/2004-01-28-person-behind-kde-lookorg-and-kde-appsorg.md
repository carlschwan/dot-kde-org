---
title: "The Person Behind KDE-Look.org and KDE-Apps.org"
date:    2004-01-28
authors:
  - "fmous"
slug:    person-behind-kde-lookorg-and-kde-appsorg
comments:
  - subject: "WOW"
    date: 2004-01-28
    body: "65 MILLION PAGE VIEWS PER MONTH FOR KDE-APPS and KDE-LOOK TOGETHER!\n\nThat's astronomical, I wouldn't of imagined it was more than 1 million/month. That's 780 million in a year, close to 1 billion, that is more than Linux's marketshare isn't it?"
    author: "Alex"
  - subject: "Re: WOW"
    date: 2004-01-28
    body: "He said \"13,000,000 page views a month\", not 65 million."
    author: "Anonymous"
  - subject: "Re: WOW"
    date: 2004-01-29
    body: "He also said that there were 65 million hits, which is what the parent was referring to."
    author: "Ed Moyse"
  - subject: "Re: WOW"
    date: 2004-02-02
    body: "Not to be pedantic but a hit is different from a page view.  If I have a .html page which includes 9 graphic files then there would be 10 hits (9 graphics and 1 .html file) and 1 page view (the actual .html file).  Therefore it is not surprising to see 65 million hits on a sites such as kde-look and kde-apps but \"only\" 13 million page views.  "
    author: "Philo"
  - subject: "Re: WOW"
    date: 2004-01-29
    body: "1 billion is 1,000,000,000,000 (10E12) and not 1,000,000,000(10E9)"
    author: "Anonymous"
  - subject: "Re: WOW"
    date: 2004-01-29
    body: "> 1 billion is 1,000,000,000,000 (10E12) and not 1,000,000,000(10E9)\n\nErr - depends on where you live. My dictionary says it's the former in British English (and other European languages), in American English it's the latter. Just another source of fun quite like imperial/metric measures."
    author: "Thomas2"
  - subject: "Re: WOW"
    date: 2004-01-29
    body: "out of curiosity, what is/was the word for 10E9 in british English?"
    author: "..."
  - subject: "Re: WOW"
    date: 2004-01-30
    body: "Thousand Million\n\nBut nearly everyone I know calls 1,000,000,000 one billion. IMO It makes more sense to keep everything in magnitudes of three, so the American way is best. However I will defend colour and favourite until my dying day!"
    author: "Max Howell"
  - subject: "Re: WOW"
    date: 2004-01-31
    body: "Try to defend your ignorance"
    author: "mike"
  - subject: "Re: WOW"
    date: 2004-01-29
    body: "10E12 is The old british def'n of one billion - i'm in the UK and pretty much no-one uses it here anymore, the american 1 billion=10E9 is accepted instead.\n\nThat said, there may still be some countries that use 10E12, but I dont know of any."
    author: "ac"
  - subject: "Re: WOW"
    date: 2004-01-29
    body: "Germany, Austria. For example."
    author: "Eike Hein"
  - subject: "Re: WOW"
    date: 2004-01-29
    body: "Any spanish speaking country: a billon is a million millions, a thousand millions is a millardo, artificial word noone uses."
    author: "Roberto Alsina"
  - subject: "Re: WOW"
    date: 2004-01-29
    body: "ehrm... in dutch, this is the same.\nmiljoen\t\t \t= 1.000.000\nmiljard \t\t= 1.000.000.000\nbiljoen \t\t= 1.000.000.000.000\nbiljard\t\t\t= 1.000.000.000.000.000\n\n;-)"
    author: "superstoned"
  - subject: "Re: WOW"
    date: 2004-01-29
    body: "also in France\n\n1 milliard = 1.000.000.000\n\nI think it's used pretty much everywhere except in English\n\n"
    author: "AC"
  - subject: "Re: WOW"
    date: 2004-01-29
    body: "Also in Polish:\n\n1 milion = 1e6\n1 miliard = 1e9\n1 bilion = 1e12\n1 biliard = 1e15\n1 trylion = 1e18\n1 tryliard = 1e21\n...\nI'm not sure what's next ;-). Kwadrylion (read \"quadrilion\") perhaps?"
    author: "Rafal Rzepecki"
  - subject: "Re: WOW"
    date: 2004-01-29
    body: "Billion, Trillion, Quadrillion, Quintillion, Sixtillion, Septillion, Octillion, ...\n\nbut you have to write 10eX, because 1eX is still 1... ;-)\n"
    author: "Juergen"
  - subject: "Re: WOW"
    date: 2004-01-30
    body: "AFAIK that's not correct as XeY stands for X * 10^Y. Therefore 1e9 is 1*10^9 which is 1.000.000.000"
    author: "Jan Drugowitsch"
  - subject: "Re: WOW"
    date: 2004-01-29
    body: "And in Poland is the same, too.\n\n"
    author: "Marcin"
  - subject: "Re: WOW"
    date: 2004-01-31
    body: "Portugal too.\n"
    author: "Source"
  - subject: "Re: WOW"
    date: 2004-02-01
    body: "Italy too:\n\nmilione: 1.000.000\nmiliardo: 1.000.000.000\nbilione: 1.000.000.000.000"
    author: "Maurizio Colucci"
  - subject: "Re: WOW"
    date: 2004-01-30
    body: "Denmark\n"
    author: "Niels L"
  - subject: "Re: WOW"
    date: 2004-01-31
    body: "All the spanish country (that is, Spain and all Latin America) use the world \"bill\u00f3n\" which is close to \"billon\" and it's 1e12, not 1e9 which makes sense.\n1e9 is Mil Millones (or Thousand Millon).\nWhy do I think it makes sense, because billon is just the second step in the string of millon(1e6), billon(1e12), trillon(1e18) -spanish-cuatrillon(1e24), -spanish-quintillon(1e30)... do you see the patron ? so you have thosand millon, thousand billon, thousand trillon for 1e9, 1e15, 1e21... it makes sense and it is scalable, it is clean, I would never use the world 'billon' in english for what is thousand millon.\nLatter on, due to the \"billon\"(en_US)->\"Mil Millones\" translation problems (most translator just traslanted \"billon\"(en_US) to \"bill\u00f3n\"(es)) \"La Real Academia\" (the insitution that regulates the evolution of the spanish language) invented the word Millardo for \"billon\"(en_US) or \"thousand billon\"(en_UK) or \"Mill Millones\"(es). Which kinda breaks the scalability of spanish counting, but nobody uses millardo lukely.\nThanks."
    author: "Pupeno"
  - subject: "Von Billionen und Milliarden"
    date: 2004-01-29
    body: "There is an interesting article in the german magazin Telepolis\nhttp://www.heise.de//tp/deutsch/inhalt/glosse/13184/1.html\n\nThe proposal is to use\nGillion = 10^9\nTetrillion = 10^12\nPentillion = 10^15\nHexillion = 10^18\ninstead."
    author: "testerus"
  - subject: "Re: Von Billionen und Milliarden"
    date: 2004-01-30
    body: "yay, greek roots are cuter :-)\nbut Gillion is weird in that scheme. \nThe greek serie reads: eis, duo, tres, tetares, pente, hexa, octo, enea, deca."
    author: "..."
  - subject: "Re: Von Billionen und Milliarden"
    date: 2004-01-30
    body: "s/xa, oc/xa, hepta, oc/ "
    author: "..."
  - subject: "Re: Von Billionen und Milliarden"
    date: 2004-01-30
    body: "The idea is to be compatible with SI (System International) Prefixes, that are well known worldwide.\n\nfactor -> prefix -> symbol\n10^24 -> yotta- -> Y\n10^21 -> zetta- -> Z\n10^18 -> exa-   -> E\n10^15 -> peta-  -> P\n10^12 -> tera-  -> T\n10^9  -> giga-  -> G\n10^6  -> mega-  -> M\n10^3  -> kilo-  -> k\n\nsource: http://scienceworld.wolfram.com/physics/SI.html"
    author: "testerus"
  - subject: "Thanks"
    date: 2004-01-28
    body: "Thanks, Frank!  KDE-Look has given a great big boost to the KDE art community.  Without KDE-Look I'm sure the number of KDE icon/widget themes would be much smaller today.  I hope that KDE-Apps will help give a much-needed similar boost to the KDE application development community.\n\nAnd thanks to Lindows too for their generous support."
    author: "Spy Hunter"
  - subject: "Thanks Frank"
    date: 2004-01-28
    body: "Thanks for your good work Frank, and the help you give to the development of KDE.  We're very happy to be able to support you in any way possible.\n\nKeep up the great work!\n\nKevin Carmony\nPresident, Lindows.com, Inc.\n"
    author: "Kevin Carmony"
  - subject: "Re: Thanks Frank"
    date: 2004-01-28
    body: "Thank you lindows for hosting those two sites! they mean quite a lot for the KDE community!"
    author: "anon"
  - subject: "Re: Thanks Frank"
    date: 2004-01-28
    body: "Lindows has gained a lot of credibility by this.\n\nThey are sponsoring my favorite desktop environment (KDE) and my favorite file system (reiser). I have no real need for a commercial distribution since I am familiar with debian, but I will buy Lindows 5.0 just to see how user friendly it is, and if I like it I will recommend it to other people who need/want a turn-key solution."
    author: "Androgynous Howard"
  - subject: "Re: Thanks Frank"
    date: 2004-01-29
    body: "I'm seriously thinking that I'll give it a shot.  I'm on SuSE 8.2 and happy, but I'm thinking I'll reinstall to SuSE 9... which also gives me the chance to try out a couple different distros.  (My /home is a seperate drive, which has everything I use).\n\nLindows does quite a bit of open source support and sponsorship, plus Eugenia at OSNews gives it pretty good reviews (which is a hard thing to get, as she's rather picky and a hard reviewer). \n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Thanks Frank"
    date: 2004-01-29
    body: "I was a Mandrake and Lycoris user before Lindows came along. I wanted a KDE only distro that had a future and was willing to take on Microsoft. I so glad to see Lindows working with KDE more. I'm looking forward to using KDE 3.2 on Lindows 5.0. I'm one of there insiders so i'll get to play with the betas when they release them to the insider community."
    author: "Kraig"
  - subject: "Re: Thanks Frank"
    date: 2004-01-29
    body: "I too am eagerly awaiting 5.0 which will feature KDE 3.2, ReiserFS 5 and kernel 2.6, that's why I signed up at ChoicePC.com. Now I have unlimited acess to any future Lindows releases and unlimited Click-N-Run acess too :)\n\n"
    author: "Alex"
  - subject: "Just nitpicking"
    date: 2004-01-29
    body: "But its ReiserFS 4, not 5. 5 is maybe at conception stage, or still in someone's mind yet."
    author: "Maynard"
  - subject: "Re: Thanks Frank"
    date: 2004-03-01
    body: "\nHello\n\nI us now lindows 4.5, when came lindows 5,0 out?\n\ni am a full lindows.com member."
    author: "dennis"
  - subject: "Thanks, Frank, Lindows"
    date: 2004-01-28
    body: "With kde-look, I think that the kde artists community would have a LOT less icon and theme development! kudos to frank for starting up kde-look and lindows for supporting him!"
    author: "rider"
  - subject: "Frank U RULE!"
    date: 2004-01-28
    body: "Subject says it all. :) It's amazing Frank can keep all this going on only 4-8 hours a week. I regularly visit both sites and the design is great, and it's very interactive, which is also pretty cool."
    author: "misfit-x"
  - subject: "Lindows and Frank... we owe a debt"
    date: 2004-01-28
    body: "kde-look.org specifically has done more to invigorate the non-developer KDE community than almost any single contribution to KDE I know of.  It was what artist.kde.org should have been.  We owe you Frank, thank you!  Lindows became a respected member of the Linux community to me (and many others) the day they started supporting those two sites.  Thank you Lindows, your support does not go unnoticed; or unappreciated.\n\nbrockers"
    author: "brockers"
  - subject: "stuttgart"
    date: 2004-01-28
    body: "oh, you live in stuttgart, the home of Mercedes-Benz. Have you ever seen that factory??"
    author: "Daniel"
  - subject: "Re: stuttgart"
    date: 2004-01-28
    body: "I am also from Stuttgart. I am sure he has seen the factory but the Porsche factory is muche more interesting. Porsche has a viadukt where all cars cross the street to the other side into the next building. "
    author: "Calle"
  - subject: "Re: stuttgart"
    date: 2004-01-28
    body: "The closest I may ever get to that Viadukt from here in Vermont USA is in NFS:Porsche Unleashed. After hundreds of hours playing that game I'm afraid now that if I actually travel to Stuttgart the temptation to catch air at 300KMPH will be too great :)\n\nSean."
    author: "Sean Pecor"
  - subject: "Thank you Frank...."
    date: 2004-01-30
    body: "Your contribution to KDE and opensource is just worth a millon thanks. I really appreciate your work. thank you, thank you, thank you, thank you....\n\nKDE Look and KDE Apps are just amazing... :)"
    author: "Fast_Rizwaan"
  - subject: "thanx"
    date: 2004-02-01
    body: "i cute my teeth on suse 8.2, i now run 9.0 and your site has been a god send to me PLEASE keep up the great work.I hope to contribute someday soon!"
    author: "james werner"
---
Lately I have been seeing the name of Frank Karlitschek popping up, so I thought I would contact him and ask him about his role in the KDE project. The following is an interview conducted by email.
<!--break-->
<p><strong>Please introduce yourself.</strong></p>
<p>My name is Frank Karlitschek. I'm 30 years old and live in Stuttgart, Germany.</p>

<p><strong>What's your position within the KDE project?</strong></p>
<p>I build and run <a href="http://www.kde-look.org/">KDE-Look.org</a>
and <a href="http://www.kde-apps.org/">KDE-Apps.org</a>. I also work on the
<a href="http://www.kde-look.org/content/show.php?content=8341">Crystal icons</a>, help
to polish KDE for the 3.2 release and do a few smaller things like writing bug reports
and trying to help organizing the 2004 KDE conference.</p>

<p><strong>How long has KDE-Look.org existed?</strong></p>
<p>The idea and the first version are from the summer of 2001, but I try to improve the
system constantly.</p>

<p><strong>Do you work alone on those websites or is there some kind of team involved?</strong></p>
<p>At the beginning it was only me, but now I get a lot of help from the
community. People send me bug reports and suggestions to improve the system. I
also have a few users with editors' rights who help me to write news and polls.
If you want to help drop me a line. :-)<br/>
And of course there are the thousands of contributors who develop themes, icons and
applications -- they do the main work. :-)</p>

<p><strong>How do you pay for these services you provide to the KDE community?</strong></p>
<p>I have to finance the servers/hosting on my own, so the web sites rely on
sponsorship and advertising for support.</p>

<p><strong> We read in <a href="http://www.osnews.com/story.php?news_id=5758">an interview</a>
that <a href="http://lindows.com/">Lindows</a> is sponsoring KDE-Apps and KDE-Look. Can you tell us a bit more
about that? How did this happen and are there any terms?</strong></p>
<p>KDE-Look was in trouble at the end of 2002. The ISP who housed my server for
one year had quit the sponsorship because of the huge amount of traffic that KDE-Look 
generates. Lindows approached me after an announcement that KDE-Look.org was
looking for a sponsor. There aren't any special terms; they just want
a small banner on the site. Lindows has no influence on the development of
KDE-Look.org. One month ago Lindows agreed to sponsor the new project
KDE-Apps.org as well. I'm very grateful for the great support from Lindows.</p>

<p><strong>When did you first hear about KDE? When did you start using it?</strong></p>
<p>I think it was in 1998. A friend showed me an early beta of KDE 1.0 -- it was
amazing. It was the first time I realized that one day Open Source Software
could conquer the Desktop. I've been using KDE as my main desktop at work since
1999.</p>

<p><strong>Can you tell us a bit about how many visitors and submissions KDE.Look.org
and kde-apps.org has?</strong></p>
<p>At the moment the two sites have about 65,000,000 hits and 13,000,000 page
views a month, 7,500 active submissions and 13,500 registered users. The server
creates over one terabyte of traffic a month.</p>

<p><strong>Is there a difference between KDE-Apps.org and <a href="http://apps.kde.com">apps.kde.com</a>?</strong></p>
<p>I'm not sure because I don't know the internals of apps.kde.com, but I think
the main difference is that apps.kde.com has a more closed, editor centric
concept. KDE-Apps.org/KDE-Look.org are more open and community driven systems. The idea
is to create the rules for the base system with the user registration,
content voting, content filtering, comments system, et cetera and in that way
a community can evolve which is self-adjusting. If it works the
right way you don't need an editor in chief or and administrator controlling
the websites all the time.</p>

<p><strong>Is there something the comunity can do to support kde-apps.org and
kde-look.org?</strong></p>
<p>Upload your work or write constructive comments. You can also donate a little
money for the hosting of course. :-)</p>

<p><strong>How much time does KDE consume in your day?</strong></p>
<p>It depends. Normally 4-8 hours a week.</p>



<p><strong>I understand you work also on the icons in KDE. Does there exist some kind of icon-design team?</strong></p>
<p>There is no official team at the moment, but there are of course a few
people who work on the icons.</p>

<p><strong>Who are these people?</strong></p>
<p>I think in the last few months the most active ones are Ken Wimer from SUSE,
Everaldo from Lindows and myself, but there are a lot more artists contributing of course -- for example the
people on the revived <a href="https://mail.kde.org/mailman/listinfo/kde-artists">kde-artists mailinglist</a>
and the KDE-Look community.</p>

<p><strong>What should users do when they find an application has missing icons?</strong></p>
<p>Write a bug report on bugs.kde.org.</p>

<p><strong>What do you like most about KDE?</strong></p>
<p>It is a very active and very open world-wide family of talented and motivated
developers improving this great desktop. It is an honor to be part of this
community.</p>

<p><strong>What do you like least about KDE?</strong></p>
<p>I think developers should pay a bit more attention to usability and the visual
appearance of the applications. There are really great
applications that an unskilled user can't understand.</p>

<p><strong>What other stuff do you do beside this KDE stuff?</strong></p>
<p>I work as the head of engineering in a multimedia company in Germany. This
consumes most of my time. On vacation I like to travel and do scuba diving.</p>




