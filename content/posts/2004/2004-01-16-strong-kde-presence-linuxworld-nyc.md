---
title: "Strong KDE Presence at LinuxWorld NYC"
date:    2004-01-16
authors:
  - "mbucciarelli"
slug:    strong-kde-presence-linuxworld-nyc
comments:
  - subject: "Great !"
    date: 2004-01-15
    body: "Congratulations and thanks to everyone involved !"
    author: "domi"
  - subject: "Wow!"
    date: 2004-01-15
    body: "In the Linux World Product Excellence Awards KDE has a pretty strong presence. In the Front Office Award Ximian is no where to be seen. I have to say, Xandros is an truly excellent distribution, and it should get a heck of a lot more press - as should all KDE distributions."
    author: "David"
  - subject: "Re: Wow!"
    date: 2004-01-16
    body: "Everytime a new Xandros release is reviewed everyone gets all gooey about how nice and easy it is to use and that it is based on KDE. Who cares ? What has Xandros done to improve KDE ? And I do not mean \"improve\" to benefit only themselves. I have yet to see a single stuff Xandros has contributed. Can someone please enlighten me why I as a developer and user of KDE should care about this distro ? At least Lindows tries to sponsor somethings here and there. Where and what has Xandros done ???? Oh well...."
    author: "DA"
  - subject: "Re: Wow!"
    date: 2004-01-16
    body: "quite a *lot* if you consider when Xandros was essentially Corel's Linux division. "
    author: "anon"
  - subject: "Re: Wow!"
    date: 2004-01-16
    body: "So, what did Corel do then?\n\nRik\n"
    author: "Rik Hemsley"
  - subject: "Re: Wow!"
    date: 2004-01-16
    body: "Complained a lot about us getting pissed at them for never feeding patches back upstream ;)  \nNothing beats two pissy sales chicks coming over to the KDE booth and attacking us in front of the crowd about how we where ungrateful for us not being happy they chose to use (ie brand KDE).\n\nah well im sure they are happy selling shoes or what ever they do now ;)\n\ncheers\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Wow!"
    date: 2004-01-16
    body: "Well we should start asking them then."
    author: "David"
  - subject: "Re: Wow!"
    date: 2004-01-16
    body: "Xandros does sponsor KDE as well.  They have funded KDE presence at conferences for instance."
    author: "George Staikos"
  - subject: "Re: Wow!"
    date: 2004-01-16
    body: "Well, the last time that Kurt Granroth was seen in KDE circles he was working for Xandros...  (Kurt used to be really active in KDE but in the 3.x era has mostly disappeared.)"
    author: "ac"
  - subject: "Re: Wow!"
    date: 2004-01-16
    body: "seems Kurt works at Ticketmaster now.. http://www.granroth.org/resume.html"
    author: "anon"
  - subject: "Best Of Show"
    date: 2004-01-15
    body: "We'll be up for Best Of Show too!  Hey... nothing wrong with thinking big ;)\n\n\"Best of Show\" will be awarded to the product deemed by the judges to be an important advancement in the state of the art, and a major step forward for Linux in the market place.\n\nIt looks like it's up to KDE and Gentoo for the Best Open Source Project.  I just can't see Sun's JXTA or Real's Helix Player (bllleeeccckkkkk) competing with all of KDE.  Hell, I can't see much competing with all of KDE ;)\n\nOh and KDevelop should win hands down over the other tools.  Now, if they had put up Quanta then maybe we'd have some competition..."
    author: "manyoso"
  - subject: "Re: Best Of Show"
    date: 2004-01-16
    body: "Actually, although I agree that Real's Helix Player has nothing on KDE, I think it's still surprisingly good (I've tested a late December snapshot).  Unlike prior Real (and Adobe, and Corel, and...) products for Linux, this one actually seems to work as advertised, with few if any ill effects.  So regarding big-name proprietary end-user software companies, it's actually quite a milestone in the Linux world.\n\nAnd if they used Qt or switched around the button order, I'd even use it! ;)"
    author: "ac"
  - subject: "Re: Best Of Show"
    date: 2004-01-28
    body: "I would love to see a QT widget as part of the player project. Personally, it would be great to use it on my gentoo/kde box. The project is architected to make it easy to build a QT widget as well. Why not come help us do it? :) I would love to see someone be the QT player maintainer. We have a lot of people dropping by the site, and there shouldn't be many problems with a QT widget development taking off.\nHere is the main page (sorry about the links not being linked - the site's not giving me an option for HTML encoding:\nhttps://player.helixcommunity.org/\nHere is the platforms page:\nhttps://player.helixcommunity.org/platforms/\nHere is a link to give you a good idea of the Helix Player Architecture:\nhttps://player.helixcommunity.org/2003/draft/arch.html\nThere are a lot of people already using the Helix Player on KDE. I look forward to seeing more KDE/QT folks on our site.\nIf you have specific suggestions on the button order and to make the player fit in better with the KDE environment, please feel free to email me.\n-V\n\nVikram Dendi\nProgram Manager\nHelix Player Project\n"
    author: "Vikram Dendi"
  - subject: "Kolab"
    date: 2004-01-15
    body: "Presenting Kolab in a mixed Unix-Windows client setup is great stuff.\nAny chance to enter the \"Best Productivity Application\" contest also? :-))\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: Kolab"
    date: 2004-01-15
    body: "Windows? uhmmm... can't Wine do the job? Just try avoiding it, Put a blue screen as background... nobody will notice anyway! :)"
    author: "uga"
  - subject: "Re: Kolab"
    date: 2004-01-16
    body: "no it cannot.  wine still dosent implement enough mapi for us to show off our service provider.  then again figure a demo of outlook working with kolab might be more impressive if done via windows... \n\ncheers\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Kolab"
    date: 2004-01-16
    body: "Whats the status of kolab now. Where can I get binaries for redhat/fedora? I checked the site and... its been the same for the past 6  months. no rpms. do we have to wait for the kde-debian distro?"
    author: "muleli"
  - subject: "Re: Kolab"
    date: 2004-01-16
    body: "Do you mean binaries for the client? Those will be on cAos (www.caosity.org), and you might just ask the kde-redhat team (http://kde-redhat.sf.net) to make an extra kdenetwork and pim rpm for kolab, as its rather easy.."
    author: "datadevil"
  - subject: "Re: Kolab"
    date: 2004-01-16
    body: ">might be more impressive if done via windows... \n\nYes indeed. I was joking (I thought obviously :P ). It's all about demonstrating it's stable and works with Windows. Saying it's compatible with Wine would be too suspicious :)"
    author: "uga"
  - subject: "Re: Kolab"
    date: 2004-01-16
    body: "Geiseri, is there any info on your connector on the web, I've not seen anything and i'm really looking forward to it..."
    author: "datadevil"
  - subject: "Re: Kolab"
    date: 2004-01-16
    body: "come check out the KDE booth on Thursday afternoon!\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Kolab"
    date: 2004-01-16
    body: "While you are in New Amsterdam, I'm in the old one, so its a bit hard..."
    author: "datadevil"
  - subject: "SOunds GREAT!"
    date: 2004-01-15
    body: "PR really is very important, especially with all the FUD that has been spread about KDE and the great PR GNOME is doing too.\n\nCan't wait to hear how the conference will go~"
    author: "Alex"
  - subject: "aweww"
    date: 2004-01-15
    body: "I'm torn between Gentoo and KDE in the best product award. Oh well, I'll be happy if either win. :)"
    author: "anon"
  - subject: "Re: aweww"
    date: 2004-01-16
    body: "I think DE is more important than Gentoo to desktop linux. What computer newbie uses Gentoo anyway! I want this show to award projects that bring linux closer to the desktop."
    author: "Alex"
  - subject: "Re: aweww"
    date: 2004-01-16
    body: "Why is it important if newbies use Gentoo? Is the newbie the standard to which all is measured nowadays?\nRemember: newbies don't stay newbies forever..."
    author: "Andr\u00e9 Somers"
  - subject: "Re: aweww"
    date: 2004-01-16
    body: "Oh trust me they do. They may become more productive with their tools, but they will still expect everything with a nice easy to sue GUI. \n\nHave you people any idea how computer illiterate most users are. I often get asked how to make an e-mail account in AOL or how to play a DVD."
    author: "Alex"
  - subject: "Re: aweww"
    date: 2004-01-16
    body: "Well, I am about as experienced a user as you gonna find, and I have not heard a single reason for using Gentoo that makes sense.\n\nOr at least, not one that makes sense *and* offsets taking a day to install KDE."
    author: "Roberto Alsina"
  - subject: "Re: aweww"
    date: 2004-01-17
    body: "I'll give you one. And if anyone knows of an alternative, please say so.\n\nGentoo allows a person to have a stable (say kde3.1.x) and cvs versions accessible and working and selectable from the kdm menus. And the cvs ebuilds make it very easy to keep an up to date cvs snapshot on my machine.\n\nI've been running like that for most of the year. CVS versions changed from stable and nice to flakey and dangerous probably 5 or 6 times last year. My wife and daughter weren't subjected to the instability because they were running 3.1. I reverted to the stable version from time to time because I needed to get some work done. Not complaining, that's to be expected using bleeding edge developer versions.\n\nI agree otherwise with your point. I actually bought a faster machine so I could compile in the background and still have reasonable speed. I need to be using current CVS so I can at least fake it that I know what I am talking about.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: aweww"
    date: 2004-01-17
    body: "Well, if you had to buy a faster machine, youcould probably just have bought ANOTHER machine instead for about the same money, and have CVS in it?"
    author: "Roberto Alsina"
  - subject: "Re: aweww"
    date: 2004-01-17
    body: "Indeed, that would be an option. Or just use Gentoo, enjoy it's features, put up with the long builds.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: aweww"
    date: 2004-01-17
    body: ">> I have not heard a single reason for using Gentoo that makes sense.\n\nI have one : Portage.  There's no such thing like Portage hell.\n\n>> ...taking a day to install KDE.\n\nFor me it takes only a night.  I can start a compilation before going to bed and wake up with a shiny new KDE desktop optimized for my computer :-)\n"
    author: "Dominic"
  - subject: "Re: aweww"
    date: 2004-01-17
    body: ">I have one : Portage. There's no such thing like Portage hell.\n\nHahahha. Have you heard that somebody is trying to replace it? ;)"
    author: "uga"
  - subject: "Re: aweww"
    date: 2004-01-17
    body: "\"There\u00b4s no such thing as portage hell\".. Oh yeah? \n\nhttp://home.primus.ca/~dooley/travel/baroncanyon/portage/portage.html\n\nSeriously, portage is not magic. If the guy writing the build script makes a mistake, his stuff is broken, just as if a rpm maker does.\n\nBTW: last time I tried to build KDE at my office desktop, it took 4 (four!) days. I find any distro that demands a 1Ghz box so that it \"only\" takes a night to install a package... of limited appeal."
    author: "Roberto Alsina"
  - subject: "Re: aweww"
    date: 2004-01-18
    body: "\"BTW: last time I tried to build KDE at my office desktop, it took 4 (four!) days. I find any distro that demands a 1Ghz box so that it \"only\" takes a night to install a package... of limited appeal.\"\n\nHow much time would you lose during that night, since you would be sleeping regardless? I run Gentoo on a 300Mhz laptop. I installed it from stage 1 (meaning: EVERYTHING is compiled) with ZERO problems. How did I manage that? Simple: I left it compiling GCC, Glibc etc. when I went to bed. It was finished in the morning. Then I started compiling X, Fluxbox etc. and went to work. When I came back, it had finished. I had a usable system with ZERO time lost. And of course, I can use the laptop while it compiles (you know, Linux IS a multitasking OS). It's not like I have to stare at the screen and watch while it compiles.\n\nOf course, if you need to have the app installed NOW, you can always install a binary instead of compiling from source. I fail to see the problem here"
    author: "Janne"
  - subject: "Re: aweww"
    date: 2004-01-19
    body: "\nMy computers work. They are mail servers, web servers, app servers, and most of them do their work 24/7. They are not hobby toys. They are tools.\n\nI simply can\u00b4t say \"the upgrade will take place sometime in the next 24 hours\", or assume that performance will hold.\n\nI need deterministic downtime for software upgrades, and gentoo\u00b4s portage doesn\u00b4t give that. Yes, I can install binaries. But then gentoo loses the only thing that is different from other distros!\n\nIf I install binaries, what\u00b4s the point in using gentoo? It\u00b4s like using Debian and install everything through alien.\n\nYou know, I will stop replying now, because this is boring."
    author: "Roberto"
  - subject: "Re: aweww"
    date: 2004-01-19
    body: "Point of Gentoo is not \"yay, we compile everything!\". You are extremely superficial if you think that Gentoo is just about compiling. Gentoo-users who use it because \"everything is compiled and optimized for my system\" are in the minority. Most use it for other reasons (cutting-edge apps, Portage (there's more to Portage than just compiling you know), the community, startup-scripts etc. etc.). The fact that you can compile and optimize the apps is just a nice addition to everything else."
    author: "Janne"
  - subject: "Re: aweww"
    date: 2004-01-17
    body: "If you think how often you actually install software, compiling time really isn't that big a problem.\n\nI mean, it takes me a couple of days to get a new version of KDE with all of the packages installed, because I can't just leave it running overnight, but then considering that I could download and start compiling KDE the day it is released, and have it churning away in the background while I work, it's probably installed faster than RedHat, SuSE, Mandrake, Debian et al come out with their precompiled patched packages."
    author: "Tom"
  - subject: "Re: aweww"
    date: 2004-01-17
    body: "So you say it\u00b4s not worse? That\u00b4s not precisely a compelling reason to switch ;-)\n\na) I don\u00b4t know what box you have, but here compiling KDE takes a fair chunk of this computer\u00b4s wits (Duron 1Ghz). So, while you build it, everything becomes much slower.\n\nb) I manage some 70 boxes. They range from PI/75 to P4/2Ghz. The larger ones are busy. The smaller ones are small. Compiling huge stuff is out of the question in both.\n\nc) I install lots of software. I like trying stuff. When I find RPMs, it\u00b4s faster than gentoo. When I don\u00b4t, I just use checkinstall and it\u00b4s about the same as gentoo.\n\nReally, how long woulkd it take to reproduce my 3.2beta2 with X 4.3 installation? The box is a PII/233, 128MB of RAM. Oh, and it has a 20GB HD, with about 1GB free ;-)\n\nI had that puppy working as I wanted in 2 hours with apt.\n\nI already did all that compiling-everything-from-source stuff back in 1997, and it was no fun then, either.\n"
    author: "Roberto Alsina"
  - subject: "Re: aweww"
    date: 2004-01-18
    body: "You're very correct, but minor point of detail I'd like to point out is that you can create a binary package after compiling KDE to Gentoo and then distribute that to your remote machines.  Granted, you have to compile it to work on the architecture it will be distributed to, but for someone like me who maintains a large bank of very similiar machines, this feature works very well.\n\nWhen compiling, Gentoo makes good use of distcc, so if you have 70 machines available I would think a compilation of KDE would be rather quick. :)"
    author: "Caleb Tennis"
  - subject: "Re: aweww"
    date: 2004-01-18
    body: "They are not mine, I just keep them running :-)"
    author: "Roberto Alsina"
  - subject: "Re: aweww"
    date: 2004-01-18
    body: "\"a) I don\u00b4t know what box you have, but here compiling KDE takes a fair chunk of this computer\u00b4s wits (Duron 1Ghz). So, while you build it, everything becomes much slower.\"\n\nUh, you did know that you can renice portage so it doesn't get in the way of doing other stuff??? If you didn't, then I just have to assume that you don't know what you are talking about.\n\n\"b) I manage some 70 boxes. They range from PI/75 to P4/2Ghz. The larger ones are busy. The smaller ones are small. Compiling huge stuff is out of the question in both.\"\n\nUse tools like distcc where the compiling is spread to the machines on the network.\n\n\"c) I install lots of software. I like trying stuff. When I find RPMs, it\u00b4s faster than gentoo.\"\n\nYou did know that you can install binaries on Gentoo as well, did you?"
    author: "Janne"
  - subject: "Re: aweww"
    date: 2004-01-19
    body: "* If I renice it, it takes even longer.\n\n* If I use distcc over the computers (which I can\u00b4t because they are not in the same place), and build for each one, I gain no time (simple math :-). OTOH, if I use distcc but only once and then install binaries, how is it better than installing a RPM?\n\nAfter all, one of the alleged advantages of portage is that the build is optimized for each box?"
    author: "Roberto"
  - subject: "Re: aweww"
    date: 2004-01-19
    body: "\"If I renice it, it takes even longer.\"\n\nMaybe, but it's not that bad. You you could use the sytem just fine while it compiles. I fail to see why you are in a such a hurry.\n\nYou can use distcc and optimize for each box (ever heard of cross-compiling?). But that is NOT the sole reason to use Gentoo. You seem to think that Gentoo is only about \"compiling and optimizing\" when nothing could be farther from the truth! Yes, optimizations are ONE of the advantages, but it's NOT the biggest advantage!"
    author: "Janne"
  - subject: "LinuxWorld show"
    date: 2004-01-16
    body: "I want to go :)))\nWell, I have to find some time next week to go to nyc see the KDE booth. It seems pretty nice this year."
    author: "JC"
  - subject: "KDE on OSX"
    date: 2004-01-16
    body: "I will be walking around on Thursday & Friday with my Apple Laptop for those who wish to see KDE on it :)\n\n-Benjamin Meyer"
    author: "Benjamin Meyer"
  - subject: "Re: KDE on OSX"
    date: 2004-01-16
    body: "Cool!"
    author: "David"
  - subject: "Re: KDE on OSX"
    date: 2004-01-22
    body: "Walk around?  Will you walk all the way over here to Sherman Oaks CA and up to Kalispell MT to show my old friends too? :-)  Or how about a link to where I can find out how to install it on my own :-)  Thanks in advance.\n\nWilliam"
    author: "William Janoch"
  - subject: "Re: KDE on OSX"
    date: 2005-07-21
    body: "How did you install KDE on your Apple laptop? I've ben looking for a way to do this for a while.\n\nThanks"
    author: "Richard Bonomo"
  - subject: "USA?"
    date: 2004-01-16
    body: "I don't know whether it is good to have development based in the USA. DMCA, Software patents, export regulations .... So I guess it is better to remain a Europe-centered DE before the US make trouble with their crude law. "
    author: "Gerd Br\u00fcckner"
  - subject: "Re: USA?"
    date: 2004-01-16
    body: "he trolls and scores!\n\nsorry man, the EU is not immune to silly laws.  in fact the up and coming EU dcma style laws and software patents will be just as bad, if not worse.\n\nno where in the world is safe as long as corporations control the governments. just because you are anit-american doesnt make Europe any safer... it just makes you more ignorant to the world around you.\n\n-ian reinhart geiser\n\n"
    author: "Ian Reinhart Geiser"
  - subject: "Re: USA?"
    date: 2004-01-16
    body: "> no where in the world is safe as long as corporations control the governments. just because you are anit-american doesnt make Europe any safer... it just makes you more ignorant to the world around you.\n\nBravo! Well said. I often wonder how many people would be conflicted meeting me to realize that I am an American and love my home. Perhaps when you are very young you can image such a thing as a perfect place because you are unaware of reality. Then again it's easy to become aware of a few things, and perhaps the things that your centers of influence choose to expose you to, and develop a bias based on a fraction of the available facts.\n\nCountries, like people, are flawed and have failures and problems, but with countries often diverse influences can make some very good and very bad things side by side. What we in FOSS believe is important is not just free as in beer but free as in speech. Look back in your history for where and when this originated. It was the late 1700s here. The US also contributed the microporcessor, the ISA architecture, Microsoft, DMCA and the FSF/GPL.\n\nClearly on both fronts, freedom and oppression, America is at the front lines of the battle and there is no reason to withdraw from the front lines. The idea it's bad to do something here suggests that Europeans should not attend and guys like Linus Torvolds and myself should either find something else to do or move. That's good for FOSS?\n\nIf you want to make positive change in the world then you move across borders to like minds and establish bonds. You build strength and organization where people live... and you never verbally attack their homeland or make arrogant statements. Growing power to make change cannot be done creating division."
    author: "Eric Laffoon"
  - subject: "Re: USA?"
    date: 2004-01-16
    body: "Just to correct some of your FUD...\n\n<i>sorry man, the EU is not immune to silly laws. in fact the up and coming EU dcma style laws and software patents will be just as bad, if not worse.</i>\n\nThe current situation in the EU remains that software patents are not going to be implemented. The only thing that will change this is if Europeans who know about the issues stop pestering their MPs and MEPs, particularly when the new EU Parliament gets elected later this year.\n\nBut yes, there's no point in getting nationalistic (or should that be continentalistic?) about these things, except for when it makes sense to remain in one nation or legal entity because another is a bad place to be."
    author: "Tom"
  - subject: "Award results"
    date: 2004-01-21
    body: "http://www.linuxworldexpo.com/linuxworldny/V40/index.cvn?ID=1024"
    author: "Thorsten Schnebeck"
---
The preparations for the KDE presence at the upcoming <a href="http://www.linuxworldexpo.com/">LinuxWorld NYC</a> are now complete.  It should be a blast!  We have ten KDE volunteers staffing the booth, some great demo hardware (generously provided by <a href="http://www.suse.com/">SUSE</a>), a seven-foot banner, <a href="http://www.kolab.org/">Kolab Server</a>/KDE Groupware demos on a 21" LCD (provided by Ian Reinhart Geiser), and a laptop we will give away to one lucky booth visitor.  It is also worth a mention that <a href="http://www.linuxworldexpo.com/linuxworldny/V40/index.cvn?ID=10240">KDE has been named finalist in two categories</a> of the LinuxWorld Product Excellence Awards: Best Open Source Project and Best Development Tool (<a href="http://www.kdevelop.org/">KDevelop</a>).

The North American PR momentum continues to grow!
<!--break-->
<p>
On the technical side, in addition to the two SUSE machines running KDE 3.2, Ian Reinhart Geiser will have a minicube with Debian unstable and XFree 4.3 and have demos for:

<ul>
<li>KDE Groupware with Kolab and <a href="http://www.sourcextreme.com/">SourceXtreme</a>'s Kolab connector</li>
<li>KDevelop 3.0 and <a href="http://quanta.sourceforge.net/">Quanta</a></li>
<li><a href="http://developer.kde.org/documentation/tutorials/kconfigxt/kconfigxt.html">KConfigXT</a>, <a href="http://edu.kde.org/">KDE EDU</a>, KDE Desktop Services, and <A href="http://xmelegance.org/kjsembed/">KJSEmbed</a></li>
</ul>

All this in addition to general Q&A on KDE deployment and development in companies.

<p>


Sticking with the KJSEmbed theme, on Thursday George Staikos will present a talk on desktop scripting (KJSEmbed, DCOP, and other fun stuff) that is only available to people with conference passes.
<p>

In addition to the technical demos, we also have a bit of press relations set up.  George will be doing a 15 minute SysCon radio show on "What's New in 3.2" and Brian Proffitt (managing editor at Linux Today) will stop by on Wednesday to get an update on progress on the <a href="http://dot.kde.org/1071510647/">Conquering the Enterprise Desktop</a> initiative.

<p>

A final note of thanks to Nathan Krause at <a href="http://www.nalekra.com">NALeKRA</a>.  He approached the KDE e.V. with an offer of thanks for all the great work done on KDE, and as a token of his appreciation he donated a laptop to KDE that we will give away at the booth.  <strong>Thanks Nathan!</strong>

<p>

If you are going to the show, be sure to stop by and say hello!