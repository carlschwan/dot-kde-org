---
title: "Quanta+ Sponsor Asks for Assistance "
date:    2004-10-17
authors:
  - "djoham"
slug:    quanta-sponsor-asks-assistance
comments:
  - subject: "Thanks David"
    date: 2004-10-17
    body: "Thanks David! You are a truly great guy and I'm thankful to know you and honored by your association. Amazingly much less than 1% of our users contribute anything, and yet that fraction of a percent has made a huge difference and by the standards of most projects we are a great success. We also received a lot of help from users when we wanted to get all of us to aKademy to meet for the first time, for which I can't thank everyone enough. Exciting things are in the works from that. \n\nIt gets tiring asking for support and I'm sure some tire of being asked, but in September we received payments from 8 sponsors (several sponsors weren't included because they pay larger amounts quarterly or annually) with David's being the largest. We did not receive any donations in September outside of sponsorship payments for the first time since July of 2003. If that continues I have to come up with maybe another $1,000 by the end of the year. This is also the busiest time of year for me. So I need to focus on income because I hardly make any in the first quarter of the year. So the last thing I want to do now is fund raising, because ironically it will cost me too much money. I have no time for it. \n\nWhat people probably don't fully realize is the amount of stress and time consumed dealing with trying to come up with funds when we are short. The negatively affect my business and development on Quanta. These fluctuations also mean sponsorships are more comforting than donations. Certain critical times of the year a few hundred dollars invested rather than spent can make a difference of thousands of dollars of income. \n\nDavid Joham is one of our three most generous contributors. Those three people contribute the majority of our financial support and they are all private individuals personally moved to make a difference. I don't count myself even though I've put more money into the project than I want to count.  I hope others will join me in expressing appreciation and encouragement to David. \n\nThanks again David, and as I said before I hope all goes well with your adoption efforts, not nearly so much for Quanta as because I think you and Kim would make the best possible parents and I greatly admire your love and devotion in this pursuit."
    author: "Eric Laffoon"
  - subject: "Re: Thanks David"
    date: 2004-10-17
    body: "> Amazingly much less than 1% of our users contribute anything\n\nsequiter, you do realise that this is free software (in both senses), don't you?"
    author: "bh"
  - subject: "Re: Thanks David"
    date: 2004-10-17
    body: "Sure it's free software but that doesn't me you shouldn't give someting back.\n\nQuanta is a professional tool that's comparable with software that costs hundred of dollars. If you earned money with it, then what's wrong giving a very small percentage of this money to the people that created this tool?\n\nEspecially when this money is used to even more improve the tool?"
    author: "Christian Loose"
  - subject: "Re: Thanks David"
    date: 2004-10-17
    body: "> sequiter, you do realise that this is free software (in both senses), don't you?\n\nLet me see... I'm the author and I am the one who made the decision to use the GPL for the license. So it is free as in freedom, but just because we provide it free to use doesn't mean it doesn't cost real money to produce. As Christian points out here, if you're making money with it why not give back a little? Why not indeed? If 1% of users gave 10% of what they spent on commercial software the kdewebdev module would be substantially more impressive because we would be able dramatically change the demands on our developers as well as expand the reach of our tool set. Our goal is to produce a killer app, not just a decent tool.\n\nGPL software gives you advantages in not being locked in and being assured enhancements are possible. KDE provides a great architecture. Commercial programs require money and expect to make a profit. Shareware asks you for money in creative ways. Free software allows you to pay if and when you feel that you would like to and in whatever amount works for you. \n\nPaying for some software may make little difference to the actual development of it, but in this case it makes a big difference. \n\n1) Had I not invested finances in 2000 the project might have died. The original authors did not have a home with a phone line and were about to lose access to university computers.\n\n2) After the original authors left in 2001 had I not invested in Quanta again in 2002 bringing Andras in I would have been toiling trying to keep the old 2.0 code base compiling with KDE 3. Many people thought the project was dead.\n\n3) When I was critically ill in 2003 I was months behind paying Andras. If people had not stepped up and helped he might have had to find some other work, which would have made it very hard for him to contribute. Where he lives companies typically work you as long as they can but don't pay by the hour.\n\nPersonally I think the selfish reasons to contribute to this project are huge. While we do have some volunteer developers and I contrubute small amounts of code Andras continues to produce most of the code in Quanta. Michal has helped Quanta and transformed Kommander. As to what this effort has produced, here is the sloccount summary for kdewebdev CVS...\n\nTotal Physical Source Lines of Code (SLOC)                = 198,879\nDevelopment Effort Estimate, Person-Years (Person-Months) = 51.83 (621.91)\n (Basic COCOMO model, Person-Months = 2.4 * (KSLOC**1.05))\nSchedule Estimate, Years (Months)                         = 2.40 (28.81)\n (Basic COCOMO model, Months = 2.5 * (person-months**0.38))\nEstimated Average Number of Developers (Effort/Schedule)  = 21.59\nTotal Estimated Cost to Develop                           = $ 7,000,999\n (average salary = $56,286/year, overhead = 2.40).\nSLOCCount is Open Source Software/Free Software, licensed under the FSF GPL.\nPlease credit this data as \"generated using David A. Wheeler's 'SLOCCount'.\"\n\nThis is why software is expensive and why it is in a web developers best interest to invest in our project. If we quit and you had to spend $7 million to replace it you still wouldn't have the Kate part or KHTML. Yet we've only spent as much as it would cost for a really cheap car you would not want the valet to see at a nice restaraunt. ;-) \n\nWhen I was a kid I saw a monument that said \"Freedom is not free\". It made no sense to me and I asked my dad how that could be. Now I understand that all freedom is purchased at some cost. To be free to use something is of no value without there being something to exercise that freedom on. Freedom is power and it is always under threat from those who want power over others. Join me in spreading freedom."
    author: "Eric Laffoon"
  - subject: "Re: Thanks David"
    date: 2004-10-17
    body: "Indeed it is free. Both ways.\n\nBut the social contract of free software demands much from both developers and users.\n\nDevelopers give their time and expertise without any guaranteed (as much as there is anything certain) return on their investment. They give away control of their code, and allow others to use it within certain broad limits. In doing so they build a community of users and hopefully contributors.\n\nUsers get the fruits of the developer's labor, usually at no monetary cost. Now what? Can users expect to get this stuff for nothing? No. Users of Quanta get a good piece of software to use, but also get the opportunity to make it work for them by contributing to the project. Either contributing fixes, documentation, code or money. Really, what right does anyone have to expect someone to do something for them at no cost to themselves?\n\nWhen the giving is going only one way, the whole system grinds to a halt.\n\nDerek"
    author: "Derek Kite"
  - subject: "Distro support"
    date: 2004-10-17
    body: "Perhaps some distro could hire Andras fulltime to work on Quanta+. (He Novell, wanna do something useful?) Quanta+ has the potential to be a good selling argument :-)"
    author: "Bausi"
  - subject: "Re: Distro support"
    date: 2004-10-17
    body: "Linspire sponsored NVU and they used it as a selling argument.\n\nI believe what is really needed is a donations infrastructure or a bug bounty system, i.e. I pay for a feature and it will be donated when it is released ecc."
    author: "juppel"
  - subject: "Re: Distro support"
    date: 2004-10-17
    body: "this might indeed help. a lot corporate users approach OSS the same as they do proprietary software - they don't understand the value of having/being able to modify the source.\n\nthey test the OSS application, and would say - yes, its almost as good as the proprietary one, but not good enough. I'll wait. but if they should realize they could for one time put the money they pay for the proprietary piece of software every year into the OSS application, and it would be good enough (and free for ever) to use! which, in the long run, would be much cheaper for them.\n\nthis should be made more clear, and such a bug/feature bounty system might work.\n\nlets say, you want a feature. you go to a site, and say you want the feature, and are willing to pay lets say 150 bucks. then others come, and say \"we are willing to pay some too\". until a developer steps up and says, hey, I am willing to build this feature, for this amount of money. so everyone donates, the website owners tell the coder to get going, and until he's (almost) ready he wont recieve funds. when the final product passed the tests, and those that did a donation agree on paying, the money is paid, and the product gpl'ed.\n\n"
    author: "superstoned"
  - subject: "Re: Distro support"
    date: 2004-10-17
    body: "Idealistically this sounds good. In practice it has real potential for problems. I've hashed this over with several people a lot.\n\n1) All project contributions have to go through a maintainer since they have authority over their project. You don't want something somebody else is working on or something that will be rejected because it will have to be rewritten when something else changes. So any system like this has to involve the maintainer. As a side note this can become an administrative black hole on a project our size. :-/\n\n2) Addressing things on a per feature basis may be motivational for users but it is not cost efficient. Take a look at the Quanta codebase. It takes a few months to really get efficient at coding in there. I worked on a patch to the parser for hours and couldn't find one thing. Andras went ahead and modified my patch and committed. It was only a few lines of code.\n\n3) Sometimes the most expensive things are not features you want until you try them out. Not only that, some of the most critical work on software is not really seen as a feature. A lot of our early work on the 3x series was architectural foundations to make features easier to add. For some time we did not produce many new features because of this work. A totally feature driven sponsorship could put good design factors at risk.\n\nOutside of all that this is interesting, but there are other ideas I'm working on that I think have more potential to generate revenue in a way that is more organically beneficial to the process and gets users a better value."
    author: "Eric Laffoon"
  - subject: "Re: Distro support"
    date: 2004-10-17
    body: "well, of course, a lot thought has to be put in this ;-) and obviously, you do so. I hope you can find a viable solutin, I think a good solution to this can lead to an enormous gain for the OSS community."
    author: "superstoned"
  - subject: "Re: Distro support"
    date: 2004-10-18
    body: "All I want to say until I have something to present is that I'm working on several ideas that I hope will prove successful. It wouldn't be prudent to discuss them until they are closer to a tangible state, but I have been pondering and planning this for the last four years. When I have something to present I will post it here first, don't worry. So don't bother asking for specifics yet. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: Distro support"
    date: 2004-10-17
    body: "Andras already does work full time on Quanta! I started paying him in 2002. It has been my project and now I consider Andras very much a partner. I would be happy to take money from a corporate entity as long as they agreed to allowing us to function as we do currently, making our decisions about our development direction. Neither Andras or I would be particularly excited about a new arrangement that might compromise our vision on the project. I'm sure Andras would be the first to credit my direction on the project and of course I'd love to get a check as well. ;-)\n\nFWIW I know distros do support some developers, but I didn't feel like waiting or trying to make the case to a corporate board. distrowatch did send us money... I'm still waiting on all the distros that are making money on my efforts... but I'm not holding my breath. ;-)"
    author: "Eric Laffoon"
  - subject: "donations"
    date: 2004-10-17
    body: "There is a serious lack of a donation infrastructure. I would recommend to integrate such a tool into KDE. Please also note that ASP Shareware and GPL are compatible."
    author: "gerd"
  - subject: "Re: donations"
    date: 2004-10-18
    body: "This is a very good idea. Plus, it would be great if donations could be made only by typing a Mastercard (or Visa or any other) number, without having to mess with Paypal and/or bank wire transfers...\n"
    author: "Bojan"
  - subject: "Re: donations"
    date: 2004-10-18
    body: "While this is a good idea, and one I've seen kicked around for quite a few years now, the argument always comes up: that's not very professional.  It was once thought to have a \"donations\" link prominently (or even not) right on the KDE home page (and/or as you suggest somewhere in KDE itself like Help --> About --> Donate), but PHB's tend to think a lot less of software, \"companies\", web sites, etc. that appear to be \"begging\" for money.\n\nI agree that making it more obvious that KDE programmers need to eat too, but, I also agree with the impression side of it.  If there was a happy middle ground...\n\nI know and understand the value of OSS (and KDE!! :-) ), but to convince my current (and past) PHB's of that value is hard enough as it is, and to throw \"Donate to [insert OSS project]\" right on a home page or something isn't that great of an idea, in this respect.\n\nM."
    author: "Xanadu"
  - subject: "Re: donations"
    date: 2004-10-18
    body: "Your \"non professional\" argument is only valid if you are talking about projects with no marketing and business savvy... good for humor maybe but not for rational consideration. See my post above...\n\nWhen it becomes more trendy to use FLOSS then it is already more businesses will be going to sites of projects. Asking for a donation from a company on a web site is a difficult presentation to make. Having your company presented as a sponsor of a project on a web site other companies go to, while at the same time getting a tax write off for helping to improve critical business software is so no brainer it's not even funny.\n\nIn this case it's not what you say so much as how you say it."
    author: "Eric Laffoon"
  - subject: "Re: donations"
    date: 2004-10-19
    body: "Mr. Laffoon,\n\n> Your \"non professional\" argument is only valid if you are talking about\n> projects with no marketing and business savvy... \n> good for humor maybe but not for rational consideration\n\nBut doesn't KDE have \"marketing value\"?  I believe it does.  My PERSONAL OPINION is that it is the \"superior Desktop for *NIX\", this is why I use it.  We, KDE users, prefer this \"desktop\" over others.  *Please* correct me if I'm wrong in that assumption. And it is us, that are in \"control\" of corporate networks, that (help) decide what gets used. (yes, I'm stretching a bit, but you see my point)\n\nAside, I choose \"Linux\" / KDE / Gentoo for my personal (main) machine.  Cool.  OK, whatever.  How do I convince PHB's to *POTENTIALLY* drop a few millions on some \"OS\" (please understand I know the difference between an OS and desktop...) when they see web sites that are asking for cash?  It gets rather interseting at that point.\n\nThat was my only point (althogh I made a few in there, but... :-) )\n\n> When it becomes more trendy to use FLOSS then it is already more businesses\n> will be going to sites of projects. Asking for a donation from a company on >a web site is a difficult presentation to \n> make.\n\nYep.  You, sir, are 100% right.  When it becomes more obvious that OSS (in general) is the way to go, yup, asking to feed the codist's (programmer's) kids isn't a big deal at all (and rather welcome, IMHO) ).\n\n\nAs far as the rest:\n\n> Having your company presented as a sponsor of a project on a web site \n> other companies go to, while at the same time getting a tax write\n> off for helping to improve critical business software is so no brainer\n> it's not even funny.\n\nI agree, 100%.  One just has to get to that point first, which is my point...\n\nM.\n\n\n"
    author: "Xanadu"
  - subject: "Re: donations"
    date: 2004-10-21
    body: "> Mr. Laffoon,\n\nWhat's next, \"sir\"? I have a ponytail to my belt. ;-)\n\n> But doesn't KDE have \"marketing value\"? I believe it does. My PERSONAL OPINION is that it is the \"superior Desktop for *NIX\", this is why I use it. We, KDE users, prefer this \"desktop\" over others. \n\nI don't know where you're getting this. I said projects that didn't have marketing savvy, as in developers not knowing how to promote their projects. This is no reflection on the project. I've been using KDE since I discovered it in 2000 and I develop on it as my desktop of choice. I have only run windows a few times in the last 10+ years and I never liked it. BTW I'm running Gentoo also.\n\n> I agree, 100%. One just has to get to that point first, which is my point...\n \nMaybe it's just that I don't see any as points but all as a stream, and it's not really an external thing that has to happen for me, but having the resources. If I was independently wealthy I would be promoting at maximum level now. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: donations"
    date: 2004-10-19
    body: "How about creating a KDE club (something Mandrake has done with its Mandrakeclub), where one could join for a small annual fee? I use Mandrake and I was very happy when I found out about the possibility to join Mandrakeclub for something like 60$ which can be paid with a credit card - for me that is very small amount of money, considering the quality of software I get with Mandrake Linux. I guess that this could be done with KDE too. Many users that are happy with KDE and can afford it, would join the KDE club for a small annual fee, IMHO."
    author: "Bojan"
  - subject: "Re: donations"
    date: 2004-10-21
    body: "There's nothing inherently wrong with it but I don't think it's optimal either. There are questions about the value offered to both users and developers. What would the club provide? How would the developers delegate the funds? As a former Mandrake users I think you could say I am one who supports their efforts and this club was an idea to address financial shortcomes for a company. In the pursuit of this they released development packages under the label of \"community\". One of these contained a CVS snapshot of Quanta where we transitioned the parser and it was a disaster. We had advised CVS testers on  our list not to get it. As a result of this decision we saw a pile of reports to our user list and requests for help. In debugging them we finally found that the problem was a non released version and advised everyone to get a new copy. So this effort created additional work for us and tarnished our image with who knows how many users... because they needed to offer something for their side of the contract. \n\nBTW I haven't seen any funding from this, but still happily provide the software. The big problem here was the corporate decision to use the \"community\" edition. It's a feel good term that is warm and fuzzy, but it should have been called development snapshot, which it was. In fact real community software would imply that the distribution was actually in contact with the community. Unfortunately as a developer I've seen distributions interact with us on releases once from Mandrake from a guy they had to let go in cutbacks and from Debian and Gentoo... community projects."
    author: "Eric Laffoon"
  - subject: "Re: donations"
    date: 2004-10-21
    body: "I agree.  It would be nice if KDE had an application for donating digital cash to open source (and other) projects.  I should be able to buy the cash from any number of vendors and store it on my machine.  If I feel like making a donation on the spur of the moment,  I should be able to easily open the app and donate any small amount I choose."
    author: "Burrhus"
  - subject: "flash"
    date: 2004-10-17
    body: "I remember a person who wrote a Flash like application for KDE, but I think he or she abandonned the project as nobody was there to help him earn his linving. That's really sad. What was the project called? I didn't find it with google.\n"
    author: "Hein"
  - subject: "Re: flash"
    date: 2004-10-17
    body: "Flash-4-Linux (f4l)?\nhttp://dot.kde.org/1071590847/"
    author: "anon"
  - subject: "Re: flash"
    date: 2004-10-17
    body: "The program was called F4L (Flash for Linux) and was written by a Turkish student. There were problems. First it was an alpha and I could not get it to compile and second many criticized that he had copied a bad interface and there were better design approaches. As I haven't done much with Flash I was not in the best position to evaluate. I approached the author and he was going to get it to compile and work with us. He as very excited about getting it into the kdewebdev module. Unfortunately he wasn't too excited about actually coding. I found very little done in his CVS durnig our trial period and he did not access our developers for help. We still have the source from what he did, but I'm not sure it's value. I do know a developer who is quite knowledgable in Flash but he hates it. I won't name names but he lives in the UK. It would be pointless to approach him unless he was looking for work and I could pay him to do something he didn't particularly like. Note that the UK is too expensive for me to look to sponsor developers there and I already know he won't work on this for free.\n\nAnyway the project you're referring to is a rare case where I broke my rule and asked for help before we had something good to show, because of the urgency the developer presented me. There didn't seem to be a lot of interest in the community. I did try to make something happen there but there is only so much one can do."
    author: "Eric Laffoon"
  - subject: "Re: flash"
    date: 2004-10-17
    body: "Yesterday I was trying the new Kompos\u00e9 software, it's pretty cool.\nAnyway as I was checking the homepage I landed on the developer blog here http://waidlafragger.de/~blog/oisch/ where he says he want to develop a free software project for his thesis and he would like to do something like:\n\"Creating storyboard-driven SVG Animation and Interaction Authoring Tool\".\n\nI don't think he's looking for a sponsor, maybe u could try to convince him to do it and include it in kdewebdev.\n\nCheers and thanx for Quanta\n\nPat"
    author: "Pat"
  - subject: "Re: flash"
    date: 2004-10-19
    body: "well that idea has been in my mind for ages and i would be happy to help sponser sutch a project. \nSuperkaramba is a very popular aplication, think what one could do if animated svg were possible?"
    author: "nunopinheiro "
  - subject: "Re: flash"
    date: 2004-10-19
    body: "Send me an email so I can put it in a ticker. At KDE dot org I'm sequitur."
    author: "Eric Laffoon"
  - subject: "Re: flash"
    date: 2004-10-17
    body: "Why is that of any importance?  I'm sure he has a name, like you do, whatever your nationality may be, probably German, but I didn't check. As for the \"Turkish Developer\", who won a contest IBM arranged, like many OSS projects, lack of time, money and resources kill many of them. Also, in respect for the interface, it's a bit pretentious claiming it's bad because it clones something, if he did, comming from somone developing for KDE. \n\nIMHO."
    author: "Turkish"
  - subject: "Re: flash"
    date: 2004-10-17
    body: "Nationality is important in terms of exchange rates and cost of living, as he mentioned in regard to the UK developer."
    author: "Ian Monroe"
  - subject: "Re: flash"
    date: 2004-10-17
    body: "> I'm sure he has a name, like you do, whatever your nationality may be, probably German, but I didn't check.\n\nHis name is \u00d6zkan Pakdil. Give me a break. I was typing that at 4 or 5 in the morning and the thing is I'm used to Western names and don't even have the non English characters on my keyboard. I would rather not mess up someone's name. Getting people's names right is important to me. BTW I'm not German, I'm from the US. I know a lot of KDE developers are from Germany, but it is mildly entertaining you assumed that and humbling to realize that my vital stats are still unknown. I like good software, cats and walks on the beach. ;-)\n\n> Also, in respect for the interface, it's a bit pretentious claiming it's bad because it clones something, if he did, comming from somone developing for KDE. \n\nWell with that command of the language maybe you should try re-reading what I said. I said that others who had used flash tools claimed that of two interfaces it could have modeled, he chose the bad one. I did not criticize what he did because, as I stated, I did not feel knowledgable enough on the tools because I don't use them.\n\nHowever I really have to take offense to the slap on the end. If you're implying that I copy other tools here is my response. My previous web development tool was the OS/2 EPM editor. I have never owned any commercial web development tools nor have I seriously used any other free tools for web development. I did evaluate other tools before starting Quanta.\n\nAnyone who has ever written and asked if I could make Quanta \"like windows program X\" knows that it is the wrong thing to say to me. I don't have and won't buy the program, I strongly resent the idea of cloning apps and in most cases the features people ask for can already be done in Quanta. I didn't build an application on Linux so I could copy Dreamweaver. I've spent thousands of dollars of my own money on Quanta. I could have bought Windows and Dreamweaver for less. I would have if I didn't have my own vision. I want to produce the best software anywhere. You can only be second best as a clone. What have we copied? Who have we copied? Where are my copies of what we've copied?\n\nIs there any relationship to fact in your humble opinion? I find facts useful when developing opinions. ;-) Many former Dreamweaver users Have told me Quanta is by far a better tool for their work, not that we made a decent copy. Those are opinions based on experience and in my opinion that experience gives them value."
    author: "Eric Laffoon"
  - subject: "Re: flash"
    date: 2004-10-17
    body: "http://f4l.sourceforge.net/SS/\n\nWhat's wrong with it? It looks like Flash?"
    author: "Hein"
  - subject: "Re: flash"
    date: 2004-10-17
    body: "I don't know what's wrong with it, other than it never compiled for me. I believe the other interface people have said was better is Fireworks. I would most like to have someone knowledgable with developing Flash look at the task, and not just an interface, but I guess anything is better than nothing. In any case we have a policy that we only do what we can do well. So to develop this application for kdewebdev we would need two things...\n\n1) A developer with the time to code it\n2) A Flash user who could help with specifications and testing.\n\nNote that we don't have #1 and #2 requires some rare talent and passion as well. Both combined is exceptional. \n\nIf there was community support to develop Flash/SVG web tools on Linux I have developers who have written me and told me they would like to be sponsored. (Possibly other SVG tools may become applicable as a plugin too.) This would take no more than a few hundred dollars a month to provide a sponsored backbone developer. I'm just not going to spread things too thin with current resources."
    author: "Eric Laffoon"
  - subject: "Re: flash"
    date: 2004-10-18
    body: "Eric, if you could provide \"support contracts\" for Quanta I could possibly make my employee to sign such a contract, we have a software/IT budget. Bt donations are only worth if they are tax exempted. \n\nAnd I am afraid for me as an European I can donate private money but I cannot donate business money."
    author: "gerd"
  - subject: "Re: flash"
    date: 2004-10-19
    body: "At the moment I'm incredibly busy with work. When I get a chance to breath I will be working on the new web site with our site team. I also have registered a domain for consulting on Quanta and will be registering a company and setting up and account. This will enable professional developers and corporate entities to access true consulting arrangements. Our mailing lists and email support will continue as it is, but the idea is two fold.\n\n1) Provide a higher level of structured information on a subscription basis - this will enable advanced users to team up to \"contribute\" their services as well as some other interesting ideas like offer professional level content - this would be an inexpensive subscription that would go to supporting development and also content for the service.\n\n2) Provide real consulting - This would enable companies to purchase contractural support agreements which would range from X amount of time, other arrangements all the way to flying out developers to set up and install custom packages on a networked system and train users.\n\nI don't expect the more advanced consulting to do much for a while but I would like to offer it. When we are farther along with Team Projects, VPL, XSLT/VPL and some other enhancements I think we will start to see returns on this.\n\nIn the mean time feel free to contact me regarding this. My email is sequitur at kde dot org. I'm sure you would expect I am very serious about delivering value with any contracting. I am able to contract now and will have the business umbrella shortly."
    author: "Eric Laffoon"
  - subject: "Re: flash"
    date: 2005-08-18
    body: "There is a little program for flash animation: FAT (300kb). Its new and you can download it here: http://fat.hut1.ru"
    author: "Flash for Linux"
  - subject: "triggered"
    date: 2004-10-17
    body: "Although your \"community appeal\" was not the reason, it triggered an item on my todo list, which was to donate for this wonderfull application. ;-)"
    author: "Chris van de Wouw"
  - subject: "Re: triggered"
    date: 2004-10-17
    body: "Hi Chris,\nyou've donated before so you know I send personal thankyou when we get a donation. This time I can send a public one as well. A big thanks!!! ;-)\n\nEric"
    author: "Eric Laffoon"
  - subject: "Re: triggered"
    date: 2004-10-18
    body: "Your welcome!\n\nThis was allready on my todo list, so this shouldn't be counted for David's goal, eh? ;-)"
    author: "Chris van de Wouw"
  - subject: "contributions"
    date: 2004-10-18
    body: "Is the amount donated so far public? If it is, then there is high chance that my \"Third World\" government can contribute at least 30 dollars for the cause. We use lots of GPL software."
    author: "vila"
  - subject: "Re: contributions"
    date: 2004-10-19
    body: "I have not been publicly posting this as of yet for several reasons. It would be additional effort to be complete and thorough and I would not want to mention specifics on any persons finances. That would include Andras and Michal, because I could also say \"I could pay them more if only...\" but that would mean I'd have to shoot myself for acting like a jerk. ;-)\n\nAlso I'm not sure I understand your question. Do you need specific disclosure or just to know if we still have a need. Another reason I am hesitant is that I really don't like crisis mentality... \"The Quanta team is in crisis and needs your help...\" because, well I just hate it. It sounds pathetic and it means that we wait until we have wasted time looking for solutions, lost sleep and developed ulcers. All of this is no good for either our development efforts or our personal lives. Operating from one specific need to the next becomes living in crisis, which I've had plenty of for one lifetime. Having contributors deciding whether to contribute base on whether we were in crisis would be stressful.\n\nWhen we can set up a tax exempt non profit we will be required to disclose everything, but that's much different because we will be able to go after corporate entities who can get a tax break supporting software they use. Currently we have several team members receiving bank transfers as well as PayPal and e-gold. PayPal seems to have done something weird that makes it so I cannot accurately derive my fees by formula so this would be inaccurate without some work too. Disclosure is more fun when you have enough to disclose that you can be paid for the accounting work. ;-) \n\nSee my post below for some public information."
    author: "Eric Laffoon"
  - subject: "International contributions?"
    date: 2004-10-18
    body: "I'm not a Quanta+ -user (not a web-developer), but I can recognize a quality piece of software when I see it. And even though I'm not in Quanta-teams target-market, I can see how important apps like Quanta+ can be. So, what if I wanted to chip in with a modest* monthly contribution, how could I do it in most convenient way? Of course I can arrage with my bank to deposit a monthly fixed sum to some other bank-account, but how would that work internationally (I'm from Finland)?\n\n* = The sum would be modest (20 euros/Month?), since I just bought a house and I'm up to my ears in debt. But if just few people gave something like 20 bucks a month, it would end up being quite a sizable sum."
    author: "Janne"
  - subject: "Re: International contributions?"
    date: 2004-10-18
    body: "Thanks for your proposal, I will send a private mail to you. And thanks for all the other contributors and David for supporting us!\n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: International contributions?"
    date: 2004-10-18
    body: "It would be helpfull if there was an european account with IBAN & BIC numbers!\n\nThank you for Quanta+! I love it.\n\ntom\n"
    author: "tom"
  - subject: "Re: International contributions?"
    date: 2004-10-18
    body: "There is as I have an account inside the EU in euro! Just contact me for details!\n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: International contributions?"
    date: 2004-10-18
    body: "The problem of sending money internationally are the bank fees\n(assuming that the receiver's country has not the Euro as currency).\n\nHave a nice day!\n\n"
    author: "Nicolas Goutte"
  - subject: "Re: International contributions?"
    date: 2004-10-18
    body: "I think we can also donate to KDE e.V. right???"
    author: "Martin"
  - subject: "Re: International contributions?"
    date: 2004-10-18
    body: "That would not exactly be the same. The money would not be sure to be used by/for Quanta.\n\nBut for those who are interested to donate this way, see:\nhttp://kde.org/support/support.php\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Sell Support"
    date: 2004-10-18
    body: "Eric, I suggest you sell support and/or stand behind the product in some way.  That will net you 1) money 2) more corporate/pro user adoption.  If <a href=\"http://www.flexiety.com/\">Flexiety</a> can sell OpenOffice.org on CD and back it with support, you can certainly do the same.  Businesses get a warm fuzzy when they have someone to go to for help.  Quanta+ is amazing, and having a formal support structure that pros can pay for is something it deserves at this point.\n\nPeace"
    author: "Jason Spisak"
  - subject: "Re: Sell Support"
    date: 2004-10-19
    body: "This is a good idea and is already in the works. See the post above in RE: Flash.\nhttp://dot.kde.org/1097981481/<nobreak>\n1098013857/1098015524/1098021641/1098040399/1098093075/1098142675"
    author: "Eric Laffoon"
  - subject: "Response report"
    date: 2004-10-19
    body: "Hello everyone! I want to publicly thank the 28 people (so far) who stepped up and responded, as well as Chris who insists this was already on his do list. ;-) we have easily exceeded replacing David's lost sponsorship money. What that means is that our annual average income from donations is back over $300 a month. Combined with sponsorships that means we are close to the amount it costs to cover Andras and Michal every month when you average it out. This is really good news and I'm enjoying a much lower stress level. THANK YOU! This does not mean if you're reading this late that we have no further use for assistance, but it does mean you should definitely not send me half your grocery money in fear the project might collapse. ;-) \n\nHere are a few reasons why it is good to be a little bit ahead...\n\n* Our largest sponsor is Carlos Woelz in Brazil. He contributes quarterly and is a banker in the volatile area of investments. Should he encounter difficulty any quarter and not be able to contribute... We're back to talking large dollar deficits. \n\n* Given that I pay Andras and Michal basically what it costs to pay their bills I really like to be able to give out a nice bonus over the holidays so that they can have some fun and do something nice or get something they need.\n\n* Our current level of community support, while really excellent, has little for equipment upgrades.\n\n* My income falls to a fraction of my expenses in January and February and I live on my savings from the holiday season... in 2004 we received a total of $150 in donations not from sponsorships, so I should have funds in reserve there too.\n\n* In directing this project I easily spend 20-30 hours a week on oversight, development and public interface. Some weeks I spend even more time and work suffers. I have also spent thousands of dollars of my own money on the project. Andras assures me it's okay for me to accept something in return without feeling guilty. ;-)\n\n* There has been some interest expressed in developing a Flash/SVG plugin. We are not really limited to our current levels. We can choose as a community to do more. This is just one example of where we could expand our efforts.\n\n* I _REALLY_ hate asking for money. That's why I like sponsorships. Donations always fluctuated with releases and now that we release with KDE we don't get the same kick. It would be so cool if I had a buffer from here through the holidays and the first of the year without having to ask again. ;-)\n\nSo if it is stressful for you to contribute then go ahead and feel good that you can do it some other time. (Don't forget!) If you just found out and wanted to really make a difference then it's not too late to get in on the fun. If I find that we've done \"too well\" I will open a discussion with the community on what direction to take extending our development.\n\nThanks again to all those who support our project! I'm getting ready to leave on a road trip to work a holiday show for five long days, mostly 11 hours. I'm leaving in less than 24 hours so I have to get busy. I will have wifi and cat5 in my hotel so I can check email and see how things are going. I just want to say that I really appreciate the great comments from contributors and I'm really happy to be able to head up this project which has been one of the best experiences of my life. Our users ROCK!"
    author: "Eric Laffoon"
  - subject: "Re: Response report"
    date: 2004-10-19
    body: "I just threw $200 in your direction. Enjoy the road trip!"
    author: "Sean Pecor"
  - subject: "Re: Response report"
    date: 2004-10-20
    body: "Whoa, nice to know KDE has friends like you. :)"
    author: "KDE User"
  - subject: "Re: Response report"
    date: 2004-10-21
    body: "Thanks Sean! It's good to have support from the \"Mad Scientists\" out there. BTW the road trip is fun so far, but too tiring. I may have to take up sleeping. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: Response report"
    date: 2004-10-21
    body: "Cool that it worked out whith \"sponsors\" :P \nAny news on when the new website should be up and when a new version of quanta is out? ... or whar are you guys doing in quanta project right now?"
    author: "SYNERGiST"
  - subject: "Re: Response report"
    date: 2004-10-21
    body: "I really except that Eric will put the new site in the CVS, so it can be improved and made publicly available soon.\nThe new version of Quanta will be out when the new version of KDE. Check the KDE release plan (both for 3.3.x and 3.4.x). If the KDE release is delayed  and we have enough new goodies we may have an interim BE release.\nWe are working on Quanta. ;-) Right now I'm trying to improve the script language support. \n\nAndras"
    author: "Andras Mantia"
  - subject: "Donations and bounty for bug and feature hunting."
    date: 2004-10-20
    body: "This is probably not the right place to put forward this idea but it has to start somewhere :-)\n\nMy itch that i want to scratch is more or less as follows. I would like to donate to the KDE project but I would like my money to be used on solving specific problems.\n\nOne of these is support for webcam in Kopete. \"See here http://bugs.kde.org/show_bug.cgi?id=70538 \"\n\nMy idea is to add the option to donate to a bounty for this (and other) feature(s). It would be great to have a \"PayPal\" (or similar) option in the voting dialog. The bounty would be claimed by he/she/those who implement the feature (or fixes a bug).\n\nAdministrative problems that needs to be solved is among other \"who will be responsible for releasing the bounty\", \"who will say this solution is good enough to claim the bounty\" etc...\n\nFeedback? Questions? Flames?\n\nCheers Jo"
    author: "Jo \u00d8iongen"
  - subject: "Re: Donations and bounty for bug and feature hunting."
    date: 2004-10-20
    body: "Some projects (like KMail if I'm not mistaken) have such kind of offer. For others, I'd suggest you to contact the developers on the project mailing list and raise up your offer. I think you have high chances to find a developer who will want to fix a bug or implement a feature for some money.\n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: Donations and bounty for bug and feature hunting."
    date: 2004-10-21
    body: "Did not know that Kmail offered this opportunity. But I would really like to be able to browse bugzilla and see what is the bounty for this feature to day? Then developers could choose a task where they feel that the effort/pay ratio is good enough. And for me as a contributer I could make an evaluation on where my money would be best spent. (If there are to features I would like to see implemented and I only can afford to sponsor one I could evaluate which one was closest to fulfillment).\n\nCheers Jo"
    author: "Jo \u00d8iongen"
  - subject: "Re: Donations and bounty for bug and feature hunting."
    date: 2004-10-21
    body: "Look at http://www.kontact.org/shopping. I don't say that it would be not nice to be possible to add an entry to bugzilla where developers could say \"I'd do this for XXX money\", and those wanted to support KDE development could pick up one of those entries, but the other approach is also straightforward and logical: you want a feature (and you *know* what you need) or you are annoyed by a bug, so you approach the developers and offer something in order to see the bug/feature implemented. This way you really sponsor the one who does the job that you really want."
    author: "Andras Mantia"
  - subject: "another way to donate is ideacradle or dropcash"
    date: 2004-10-22
    body: "You can check out http://www.ideacradle.com and http://www.dropcash.com for simple bounty creation tools."
    author: "Brad Sears"
---
For the past year and a half, I've been a co-sponsor of the <a href="http://quanta.sf.net/">KDE Quanta+ project</a> helping Eric, Andras and the entire Quanta+ team deliver one of the best web development applications in the world -- Free or otherwise. It has been my pleasure and an honor to be part of bringing Quanta+ to you.



<!--break-->
<p>
My wife and I are currently in the process of adopting two beautiful children from Haiti. Sadly, one of the drawbacks to international adoption is that it is very expensive. These expenses have now come to the point where I will be unable to continue my sponsorship of Quanta+ after this month. If the adoptions do come through this year, I am hopeful I'll be able to resume my sponsorship in April after taking advantage of the generous tax credits available to successful adoptive parents.
</p>

<p>
Previously, I was contributing 100 US dollars a month to the Quanta+ project. My goal in this community appeal is to cover my contributions for the months of November through March -- a total of $500. Of course, it would also be nice to blow this goal out of the water and raise considerably more. :) 
</p>

<p>
Please help me continue my assistance to the Quanta+ project "in proxy" for the next few months. Remember, the Quanta+ sponsors help pay the salary of two programmers who in turn bring you some of the best software around -- all under Free software licenses. If you can help, please visit <a href="http://quanta.sourceforge.net/main1.php?actfile=donate">the Quanta+ donation page</a>. Thanks in advance to everyone for their support!
</p>



