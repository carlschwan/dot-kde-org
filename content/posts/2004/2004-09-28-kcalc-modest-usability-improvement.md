---
title: "KCalc: A Modest Usability Improvement"
date:    2004-09-28
authors:
  - "ralsina"
slug:    kcalc-modest-usability-improvement
comments:
  - subject: "well first off..."
    date: 2004-09-27
    body: "if it could start in less than 10 seconds on my 400mhz powerbook, that would be a huge usability improvement."
    author: "Ian Reinhart Geiser "
  - subject: "Re: well first off..."
    date: 2004-09-27
    body: "Well, it does it in about 2 seconds in a Duron 900.\n\nI suppose I could preload it ;-)"
    author: "Roberto Alsina"
  - subject: "What about using Kspread?"
    date: 2004-09-27
    body: "I agree what just about everything you are saying.\nHowever, I think that the improved calculator already exists: KSpread.\n"
    author: "ScottZ"
  - subject: "Re: What about using Kspread?"
    date: 2004-09-27
    body: "Talk about overkill ;-)\n\nBut yeah, I'd rather use KSpread than kcal.\n\nIn fact, I have been using \n\nAlt+F2 gg:2+3"
    author: "Roberto Alsina"
  - subject: "Re: What about using Kspread?"
    date: 2004-09-27
    body: "Why not use \"Alt+F2 2+3\"? Much faster.\n\n"
    author: "Waldo Bastian"
  - subject: "Re: What about using Kspread?"
    date: 2004-09-27
    body: "Wow! Cool! KDE never stops to amaze me with supprising little gems like this."
    author: "Andre Somers"
  - subject: "Re: What about using Kspread?"
    date: 2004-09-27
    body: "OK, I am officially shocked :-)"
    author: "Roberto Alsina"
  - subject: "Re: What about using Kspread?"
    date: 2004-09-27
    body: "What the...\n\nThis is KDE in a nutshell. :-)"
    author: "KDE User"
  - subject: "Re: What about using Kspread?"
    date: 2004-09-28
    body: "Hunh.  Nifty."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: What about using Kspread?"
    date: 2004-09-28
    body: "WTF???? where did that come from. Nice."
    author: "a.c."
  - subject: "Re: What about using Kspread?"
    date: 2004-09-28
    body: "That is SOOO cool. Thanks!\n"
    author: "Anonymous"
  - subject: "Re: What about using Kspread?"
    date: 2004-09-28
    body: "Lol! Great :D\n\nBut wtf is the ^ operator??? I expected power...\nHmmm ** works as power. Oh and % for mod, nice.\n\nAre there plans for floating point arithmetic support?\n"
    author: "John Calc Freak"
  - subject: "Re: What about using Kspread?"
    date: 2004-09-29
    body: "I am guessing ^ is xor like it is in C."
    author: "Carewolf"
  - subject: "Re: What about using Kspread?"
    date: 2004-09-28
    body: "\"Alt + F2 2x2\"  Reply: 'Sorry KDesktop: could not run the specified command' ;)"
    author: "wygiwyg"
  - subject: "Re: What about using Kspread?"
    date: 2004-09-28
    body: "use \"*\".\nAlt + F2 \"2*2\" gives 4.\n\n"
    author: "Rich Jones"
  - subject: "Re: What about using Kspread?"
    date: 2004-09-28
    body: "wow man... this is soooo cool ;-)\n\nLOL"
    author: "superstoned"
  - subject: "Re: What about using Kspread?"
    date: 2004-09-28
    body: "I LMAO. :-D\n\nIt's definitely a nice feature to impress your friends, but since \"Alt+F2 2/3\" outputs 0 and it lacks a history (which is the only serious problem of kcalc) it's not a useful replacement for bc -l.\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: What about using Kspread?"
    date: 2004-09-28
    body: "Where is floating point?\n\n0.5*2 do nothing\n0,5*2 = 10  (it seems it ignore everything before the comma)\nwe also miss sin,cos,log,ln,ecc..\nCan we improve it?\n\nCiao\nAndrea"
    author: "Andrea"
  - subject: "Re: What about using Kspread?"
    date: 2004-09-29
    body: "Why not count to five? Much faster. ;-)\n"
    author: "Rob Kaper"
  - subject: "Re: What about using Kspread?"
    date: 2004-09-28
    body: "My favorite calculator: calc - the C style calculator:\nhttp://www.isthe.com/chongo/tech/comp/calc/\n\nBeats any graphical calculator IMHO - and maybe is a bit like the one you have written..."
    author: "anon"
  - subject: "Re: What about using Kspread?"
    date: 2004-09-28
    body: "KSpread isn't that great as a straight calculator application.  What KDE really needs is a nice native interface for XCas:\nhttp://www-fourier.ujf-grenoble.fr/~parisse/giac.html\n\nIn its TI-89 compatibility mode, it makes an excellent everyday-use calculator with bonus super-advanced capabilities (symbolic integration, for example), but the user interface is not very nice.  With a little work on a KDE frontend it could be awesome.  Students would love it."
    author: "Spy Hunter"
  - subject: "I just created a better calculator..."
    date: 2004-09-27
    body: "and its already installed.\n\nOpen your favorite terminal and type \npython\n\nNo, I'm half serious, when I need a calculator I just use the interactive python interpreter.  (I guess I should rename it kython ;)\n(anyone who has learned to program in any language should find it very easy to use AS A CALCULATOR).\n\nAs far as usability goes, I think this (UseCalc) is great.  Good job Robert.  People have distorted ideas about what usability is, and just because it looks like a calculator, doesn't mean it is usable. \n\n"
    author: "foobie"
  - subject: "Re: I just created a better calculator..."
    date: 2004-09-27
    body: "And since I am simply doing an eval()  of whatever you input, it even gives the same result ;-)"
    author: "Roberto Alsina"
  - subject: "Re: I just created a better calculator..."
    date: 2004-09-27
    body: "I guess that my point was why do I even need a calculator program?  (I'm trying to take what you are saying a step further).  There is already something installed on my computer that does it just fine.\n\nOT RANT WARNING....\n\nTo me a kalculator is just bloat (in fact 80-90% of kde/gnome are bloat to me).  Don't take this the wrong way, I love kde and have been using it on the desktop since 1.x days.  But I'd much rather lose all of the bloat.  To me more important is to have integration between best of breed apps.\n\nI mostly use audacity, firefox, cinelerra, dvdstyler, gimp, inkscape, eclipse, kde (mostly for k3b and konqueror file browsing (fish://, etc), want to try digikam at some point), emacs.  (I'm still looking for the best of breed multimedia app, using xine for dvds and mplayer for things that xine chokes on, but xmms, and noatun just don't feel good).  It would be nice if these apps played together nicely (sound, printing, drag and drop, etc).  (And that I don't have to install all of the other apps for kde when I mostly just want k3b and konqueror).\n\n"
    author: "foobie"
  - subject: "Re: I just created a better calculator..."
    date: 2004-09-28
    body: "> I'm still looking for the best of breed multimedia app\n\nCheck amaroK. It rocks."
    author: "ulri"
  - subject: "Re: I just created a better calculator..."
    date: 2004-09-29
    body: "You might want to try Kaffeine. It doesn't fallback to MPlayer but since Xine reached 1.0 it has been perfectly useful. Embeds in Konqueror nicely, too.\n"
    author: "Rob Kaper"
  - subject: "Re: I just created a better calculator..."
    date: 2004-09-28
    body: "\"and its already installed.\nOpen your favorite terminal and type \npython\"\n\nI believe you, I do the same with haskell. I open a terminal and type ghci :)"
    author: "John Calc Freak"
  - subject: "Ruby version"
    date: 2004-09-27
    body: "I blame it on Alex. After I saw this I had to write my version in Ruby using Korundum. And thanks to Aaron I can even show it: <a href=\"http://www.bddf.ca/~zrusin/kalc.tar.bz2\">http://www.bddf.ca/~zrusin/kalc.tar.bz2</a> . It uses KComboBox with a history instead of a lineedit. I simply preferred it that way. Also uses text browser instead of a listview to display the results. I thought it seperates it nicely. The main gui is done in designer so people who don't know ruby can play with it. You can also define your own variables in it which is kinda neat.\n"
    author: "Zack Rusin"
  - subject: "Re: Ruby version"
    date: 2004-09-27
    body: "Smooth.\n\nScreenshot? ;-)"
    author: "Roberto Alsina"
  - subject: "Re: Ruby version"
    date: 2004-09-28
    body: "Bring on the scripting languages! I remember when people used to write KDE apps in C++ - how quaint :)"
    author: "Richard Dale"
  - subject: "To the editor"
    date: 2004-09-27
    body: "Ahem. While I identify with the comicbook guy as much as the next hacker, and share some physical features, I am noticeably prettier (photo attached)."
    author: "Roberto Alsina"
  - subject: "Like Psion palmtops"
    date: 2004-09-27
    body: "Wow - it works (almost) the same way as calculator on old Psion palmtops did. Cool!!! :-)"
    author: "Gawron"
  - subject: "I like Roberto's calculator.."
    date: 2004-09-27
    body: "...but I'm quite sure that my mom doesn't.\n\nThere are so many ways to improve kcalc. There is no history. You can't easily review and correct what you have typed. There.. mmmm... I really can't complain much more about it, because there are just no features to complain about. \n\nI'm not sure if an improved calculator (but remember, we're talking about the default, basic, standard KDE calculator here) should duplicate the physical keys of the keyboard, but somehow it should *look like* a calculator and not like the prompt of bc. Otherwise the target audience will never touch kcalc and use their plain old solar calculator (or your HP48G, whatever) instead.\n\nMaybe the number buttons could be replaced by a \"Casio\"-Logo in the top right corner, so it will still look somewhat familiar."
    author: "uddw"
  - subject: "Re: I like Roberto's calculator.."
    date: 2004-09-27
    body: "Their solar calculator is better than kcalc (or wincalc or gnome-calc).\n\nSo, why exactly shouldn't they use it?"
    author: "Roberto Alsina"
  - subject: "Re: I like Roberto's calculator.."
    date: 2004-09-27
    body: "At the moment there is really no reason not to use a physical calculator, since kcalc is just a clone of it.\n\nI wonder why you are asking this question, since your calculator makes much better use of the capabilities of a computer. The only problem I see with it is that it won't work for people with a weak mathematical background. The goal should be to provide a better calculator for those who use nothing more than basic calculators now.\n\nOf course I wouldn't mind any advanced features, but the highest priority must be to make it instantly usable for every kde user.\n\n"
    author: "uddw"
  - subject: "Re: I like Roberto's calculator.."
    date: 2004-09-27
    body: "Well, here's how I see it.\n\nIf the alternative is kcalc/real calculator:\n\nI wouldn't use kcalc.\n\nIf the alternative was usecalc/calculator:\n\nI would probably use usecalc\n\nI am not sure making it instantly usable is the main goal for a calculator. I think it should be able to calculate comfortably, too.\n\nAnd come on! Mathematical background? If you went to school you know what 2+2 and the like means :-)\n\nSure, maybe allow x as a replacement for * would be nice."
    author: "Roberto Alsina"
  - subject: "Re: I like Roberto's calculator.."
    date: 2004-10-08
    body: "> At the moment there is really no reason not to use a physical calculator, since kcalc is just a clone of it.\n\nSo how do you copy & paste between an application on your computer and a physical calculator?\n\n(From the article):\n\n> The supported operators are the usual, plus parenthesis, and ** which is power. If you want a square root, just use **0.5. If you want, I can add something more obvious.\n\nThis is a regression from KCalc.  If somebody wants a square root in KCalc, they see the square root button and hit it.  If somebody wants power in KCalc, they see the button and hit it.  Learning new syntax for such a simple application that *already works* is insane.  It would work well if normal keyboards had square root and power keys on them, but they don't, so your whole principle of \"the keypad already does what we want\" is false.\n"
    author: "Jim"
  - subject: "Re: I like Roberto's calculator.."
    date: 2004-10-08
    body: "Please, *please* PLEASE read the whole freaking article & thread.\n\nI know that\u00b4s not good. I also know, and said, that this is just a prototype application. In fact, in another post, I mentioned that some buttons could come handy. The square root one is a good such example.\n\nAnd yes, there *must* be something that\u00b4s easier using kcalc.\n\nSure, only 1% of the users give a damn, but it\u00b4s easier to do square roots.\n\nAnd you are obviously being argumentative, but the keyboard does have a \"1\" button. That it doesn\u00b4t have a sqrt button doesn\u00b4t invalidate the argument I made about number keys, it invalidates the argument I *didn\u00b4t* make, about square roots.\n\nThat, sir, is called a strawman."
    author: "Roberto Alsina"
  - subject: "Re: I like Roberto's calculator.."
    date: 2004-10-11
    body: "> Please, *please* PLEASE read the whole freaking article & thread.\n\nI already did.\n\n> And you are obviously being argumentative\n\nActually, it's you that appears to be argumentative.  Simply disagreeing with one of your fundamental premises doesn't make me argumentative.\n\n> but the keyboard does have a \"1\" button. That it doesn\u00b4t have a sqrt button doesn\u00b4t invalidate the argument I made about number keys\n\nYes it does, unless you split the interface between the mouse and keyboard.  If, as you now suggest, add a graphical sqrt button, it means people making use of it are constantly switching between the keyboard and mouse.  It's a basic ergonomics mistake.  If you expect people to use the mouse for that task, don't force them to switch back to the keyboard for related tasks unnecessarily.  If half the interface is the keyboard and half is the mouse, the application == calculator metaphor is completely broken."
    author: "Jim"
  - subject: "Re: I like Roberto's calculator.."
    date: 2004-10-11
    body: "Of course *I* was being argumentative. That{ s the whole point of the article.\n\nBut anyway, regarding your comment, I call bullshit. Every application has the interface split between keyboard and mouse: the common tasks are easier to access with the keyboard, the harder ones have both a harder keyboard shortcut, and a mouse access.\n\nConsider a word processor. The text input is done via the keyboard, and there isn\u00b4t any way to perform it via the mouse. But switching to bold can be done via the keyboard (not too obvious), and a easily seen mouse interface.\n\nThe obvious analogous situation is that number and basic operation in a calculator should tend to be keyboard-based, while not-often used functionality should be mouse-accessible.\n\nSo, what you say about usability seems pretty much something you just made up right now. And yes, the point that the application == calculator metaphore is broken is absolutely right. That\u00b4s why my interface proposal dumps it."
    author: "Roberto Alsina"
  - subject: "Re: I like Roberto's calculator.."
    date: 2004-09-28
    body: ">...and use their plain old solar calculator (or your HP48G, whatever)\n\nI guess people able to use HP's are not a good example.\n\nI know a lot of people almost running away of mine because they are completely lost with the RPN mode. But OTOH people using HP's will be in science/enginering fields and have no trouble using algebraic expressions as input.\n\nThat said, I also do not prefer a different Calc. Maybe a dumber one, as you do not want to do anything really serious with it, just a few sums when you are in the computer, not your 50 items math homework. The only trouble about dumbing it down is what features should be left to keep people happy. I rarely do DEC-HEX conversions, I guess other people want to keep almost every else feature available.\n\n"
    author: "Shulai"
  - subject: "Re: I like Roberto's calculator.."
    date: 2004-09-28
    body: "I prefer the way HP calc series (such as my HP-48G) do work, using inverse polish notation. Specially liked the possibilities that the calc offers to this way of operating, making possible to operate things on the stack..."
    author: "c0p0n"
  - subject: "ACKs, NAKs, and Suggestions"
    date: 2004-09-27
    body: "I fully agree with you that Kcalc needs a replacement or overhaul. Especially the \"paper log\" is something I often miss.\nBut about the buttons: I actually use the number buttons from time to time. This is during phone calls, when I have one free hand only, so I don't like shifting back from the mouse to the keyboard. Suggestion: Groups of keys should be enabled separately.\nAnother suggestion: It should be possible to go back on the paper say 10 rows, update a number there and have everything below recalculated accordingly.\nFinal nagging: My old TI59 has a display mode \"engineering\", where the exponent is normalized to multiples of 3. This would be cool; even more so by replacing the exponent with the SI-Unit prefixes (3=k, 6=M, 9=G, ...)"
    author: "Jens Schmidt"
  - subject: "Re: ACKs, NAKs, and Suggestions"
    date: 2004-09-27
    body: "If you want to do it, here's how:\n\n* Add a way to toggle the modes in the UI.\n\n* Look at window.evaluate\n\n* Format the number anyway you want before you display it.\n\nIt should be about 15 lines of code (hint hint wink wink ;-)"
    author: "Roberto Alsina"
  - subject: "Re: ACKs, NAKs, and Suggestions"
    date: 2004-09-28
    body: "We did a business calculator in Kommander. It does a paper tape type log and allows you to enter basic math formulas which are entered into the log along side the total values and it provides a summary value at the bottom. It's okay as it is but we wanted to add the ability to output printed results too. Anyway because it's Kommander it's small, fast, easy to enhance and only requires that you have kdewebdev 3.3 installed. Enjoy.\n\nBTW as a security measure it will complain if you try to run it from a non local source."
    author: "Eric Laffoon"
  - subject: "Classic difference"
    date: 2004-09-28
    body: "This is just another example of a big difference between different types of people when it comes to doing calculations.  Some people have to write things out, think slower, and prefer using graphing calculators where they have to type a whole problem in some horrible mess of parentheses, not thinking linearly through the problem as to the order of calcuations as they type, but of the order they have to type to get the calculator to do the right thing.\n\nThen there's people who think faster, can keep track of some things in their head, and prefer scientific calcuators with memories (yes plural, mine has 7 instead of one), and can fly through complicated calculations (in a much more linear and logical fashion I might add) much faster than others.\n\nAnd it's not to say there's anything wrong with you whether you fall into one group or the other, you can't change who you are.  Some people will just prefer the style of interface you have shown, and for some KCalc just is more usable.  Not that there isn't room for improvement."
    author: "David Walser"
  - subject: "Re: Classic difference"
    date: 2004-09-28
    body: "I am sure you can calculate 20% of 75 plus 15% of 64 faster using your calculator, right?\n\nI mean, .2*75+.15*64(enter) is so much slower than .2*75(push into mem).15*64+(pop from mem)= :-)\n\nFor each approach used to interface the calculator, there is some set of problems that are slower. My own guess (and nothing more than that) is that this approach is faster for a significant set of problems.\n"
    author: "Roberto Alsina"
  - subject: "RPN"
    date: 2004-09-28
    body: ".. And this is where an rpn interface would shine.\n.2 [enter] 75 [*] .15 [enter] 64 [*] [+]\nYou get to see intermediate results, and get to enter in the order written, evaluating as you go. The only practical problem with the RPN approach is stack height, but that's not a problem with a calculator based on a computer, with kaboodles of memory.\n\nPGCalc2 lets you enter number and operators directly from the numpad, and use the mouse for things like SIN."
    author: "Rich Jones"
  - subject: "Re: RPN"
    date: 2004-10-04
    body: "You don't need to use the mouse for things like SIN, just press it's letter. S for SIN, T for COS, U for TAN, etc. Also CTRL, ESC to quit or SHIFT, ESC to minimize to the tray."
    author: "Chanan Oren"
  - subject: "Re: Classic difference"
    date: 2004-09-28
    body: "The example you gave should take the exact same keystrokes actually, so no advantage either way there.  With other problems, *nothing* will be faster with a usecalc/graphing calculator interface if you know what you are doing."
    author: "David Walser"
  - subject: "Re: Classic difference"
    date: 2004-09-28
    body: "Sorry, but I can't parse your post. Are you saying that with other problems a scientific calculator will be faster, or that a graphig calculator is never faster?"
    author: "Roberto Alsina"
  - subject: "Re: Classic difference"
    date: 2004-09-28
    body: "Sorry if I wasn't clear.  I am saying both."
    author: "David Walser"
  - subject: "Re: Classic difference"
    date: 2004-09-28
    body: "Then one is redundant, as far as I can see ;-)"
    author: "Roberto Alsina"
  - subject: "Re: Classic difference"
    date: 2004-09-28
    body: "This reminds me of a feature that I would like to see on all history/tape calculators. I wish that they would include the calculation next to the answer.\nWhen all you see is the answer it can be hard to remember which calculation created the answer."
    author: "ScottZ"
  - subject: "Some good points, but most don't make sense..."
    date: 2004-09-28
    body: "I think Kcalc's Simple mode is definitely overloaded with too many buttons and that it could benefit from a ncie history window.\n\nHowever, I do not think you should get rid of the buttons for numerous reasons. With the buttons you can easily type numbers you see on the screen into the calculato. Sometimes I do not feel like changing to the keyboard and would prefer to use the mouse. The interface resembles a real calculator and it seems more intuitive to me. It's also like this in every OS and so aids in familiarity.\n\nFINALLY, THERE IS NO GOD DAMN POINT! Those who prefer to use the keyboard use that and those who like graphical will use that and for the strange ones, use both at the same time. (I often use my mosue to click on functions and keybaord to type in numbers). There is no need to destroy one or the other.\n\nVALID POINTS:\n\nI defnitely think your right on the history and that the \"Simple\" view is way too overloaded. And yes, Python rules.\n"
    author: "Alex"
  - subject: "Re: Some good points, but most don't make sense..."
    date: 2004-09-28
    body: "Sorry, but are you telling me that you would rather hunt and peck using your mouse than type? In such case, use a on-screen keyboard. Sure.\n\nBut please don\u00b4t tell me that\u00b4s in any way efficient or better from a usability standpoint, because I don\u00b4t quite believe it.\n\nAnd sure, people are used to this. It doesn\u00b4t make it any less braindead."
    author: "Roberto Alsina"
  - subject: "Re: Some good points, but most don't make sense..."
    date: 2004-09-28
    body: "Well, I know people who will indeed use their mouse rather than typing, especially since that make them do basically the same thing as they are used to outside a computer. It's exactly the same thing as the \"bin\" to suppress files : many people still drag the file to the bin. You'd better get used to it since this will remain.\nAnd you'll find that many people understand the square root button on Kcalc, but don't know that square root is the same as **0.5. \n\nYour proposal is nice for a scientist. It's not as nice for the next guy.\nWith all its defaults, Kcalc can be used by anyone without even reading a manual. For your proposal, don't count on it. \n\nBest regards,"
    author: "Richard Van Den Boom"
  - subject: "Re: Some good points, but most don't make sense..."
    date: 2004-09-29
    body: "I'm not telling you if it's more efficent or not, I'm only saying it's sometimes easier. Sometimes I don't feel like taking out the keyboard or I'm reading a website and plugging in numbers from it and do not want to look down to my keyboard. An on screen keyboard would defy the purpose as it is an external application and too much trouble to bother with. In addition, the calculator has amny function buttons like for squaring and it seems more integrated.\n\nWhy not just allow both, it definitely doesn't hurt usability (except for people that are advanced) and it is more natural. I agree with adding a history and having a simpler simple mode, but I don't think you're in touch with the general population on this issue."
    author: "Alex"
  - subject: "KCalc is good as it is"
    date: 2004-09-28
    body: "I don't particularly like what the usabilistists propose about KDE, and this proposal about KCalc makes me sick. I use KDE because it's full of buttons and options."
    author: "NSK"
  - subject: "I like KCalc."
    date: 2004-09-28
    body: "I don't really understand the problem. The screenshot he shows looks nothing like my default kcalc in kde 3.3\n\nMy KCalc (defaults) has 33 buttons total. The screenshot he shows has twice that. Plus, if you are not able to use a Keyboard (think accessability people!) then you probably WANT to use your mouse... and if you want to use the keyboard, certainly buttons don't get in your way. They don't get in mine.\n\nIf you don't like KCalc, do what other's do, use Python or Alt+F2. There's no reason to tank KCalc... it's a very familiar and useful application."
    author: "Dan Ostrowski"
  - subject: "Re: I like KCalc."
    date: 2004-09-28
    body: "Obviously you haven\u00b4t tried all 33 buttons yet ;-)\n\nIf you can\u00b4t use the keyboard, you already should be using a on-screen keyboard. Why should kcalc bring its own beer to the party?"
    author: "Roberto Alsina"
  - subject: "Re: I like KCalc."
    date: 2004-09-28
    body: "KCalc takes TOO LONG to load. It's a CALCULATOR for crying out loud, and it takes almost 2 seconds to load AFTER it's already been loaded once! On my Athlon XP-M 2800+ w/ 512 meg RAM! Seriously, that thing is a pig and when I want to do a calculation, I want it to pop up immediately, not make me hold on til it's ready."
    author: "Trevor Smith"
  - subject: "Re: I like KCalc."
    date: 2004-10-05
    body: "loads in less than a second here, athlon 64 3000+ (gentoo, so its compiled 64 bit, but still for an application of this size i dont think it makes too much of a difference)"
    author: "Ben Klopfenstein"
  - subject: "realy funny..."
    date: 2004-09-28
    body: "that there're people out there that \n- complain about kcalc\n- start hacking\n- have to corroborate they need only 20 min\nand don't use 'dc'"
    author: "llx"
  - subject: "Re: realy funny..."
    date: 2004-09-28
    body: "Glad to be helpful.\n\nHowever, you totally missed the point. But don\u00b4t worry, that was funny for me, so everything has a good side ;-)"
    author: "Roberto Alsina"
  - subject: "Re: realy funny..."
    date: 2004-09-28
    body: "He didn't miss the point at all.\n\nThere IS still a UNIX operating system under that big shiny KDE thing you know. Yes, I'm being faceitious, but cmon.. the tools are THERE already. Not EVERYTHING has to be gui-ized."
    author: "clayne"
  - subject: "Re: realy funny..."
    date: 2004-09-28
    body: "And perhaps that applied to kcalc more in general.. but the aspect of a gui window for the equivalent of one or more specific basic unix commands seems absurd to me.."
    author: "clayne"
  - subject: "Re: realy funny..."
    date: 2004-09-28
    body: "Well, you seem to have missed it too. I know, I thought the point myself ;-)\n\nBut here it is in explicit form:\n\n* I am not saying kill kcalc\n\n* I am not saying ship usecalc\n\n* I am not saying usecalc is even a good application as it stands\n\n* I am not saying I will make it good (in fact, I say I wont)\n\nWhat I **am** saying is: what's good about kcalc, really? What's bad about kcalc, really? \n\nDoes it make sense to have a app kcalc-like at all?\n\nYou may say, you are not saying, just asking. Well, yeah. That's more fun ;-)"
    author: "Roberto Alsina"
  - subject: "Tempest in a Teapot"
    date: 2004-09-28
    body: "I think this is much ado about nothing. Roberto marks down kcalc for two reasons. The first reason is erroneous. The default kcalc configuration does not have nearly as many keys as his screenshot suggests. His second reason is that buttons are superflous because there are keys on the keyboard. Nonsense! While they may not be necessary for him, they might be desirable to many people. My brief survey reveals to me that EVERY GUI calculator ever uses buttons. Even that sample text UI calculator form Borland's TurboVision!\n\nAnd his solution is to use Python's eval(). Why not use bc (or dc) instead? If you're going to dump the GUI, at least use the command line's standard fare!"
    author: "David Johnson"
  - subject: "Re: Tempest in a Teapot"
    date: 2004-09-28
    body: "Agreed, I'm not sure what the problem is with Kalc.  It's purpose is to server as a simple GUI calculator.  My only complaint is the lack of an option for RPN operation.  There are plenty of powerful command line math tools as noted such as bc, dc, octave, kalc (an unrelated to Kalc), pari/gp, tons more..."
    author: "Byron"
  - subject: "Re: Tempest in a Teapot"
    date: 2004-09-28
    body: "The default has exactly as many buttons as the article says, though.\n\nAnd as the article says, those buttons are already too many. And, IMHO, absolutely useless and redundant.\n\nJust because everyone has always done it that way, it doesn't mean that way is right. Only that everyone does it that way.\n\nWhy not use bc or dc? Because I didn't feel like parsing a freaking pipe to catch the errors and give meaningful reports. If bc (or dc) were a library, though, I may have done it."
    author: "Roberto Alsina"
  - subject: "Re: Tempest in a Teapot"
    date: 2004-10-01
    body: "KCalc has it's place. There are other tools in addition to bc, and dc....lots of other tools. If I did not have KCalc I would (I know this may seem foolish to you) go and use my desk top calculator and not use the computer at all. KCalc is a tool for a reason, and it has a place, BUT the proposed tool ALSO  has a place. I like the new idea! BUT I don't want to see KCalc go away - IT SERVES A PURPOSE FOR ME, AND HAS A PLACE!"
    author: "gmontie"
  - subject: "Re: Tempest in a Teapot"
    date: 2004-09-28
    body: "In fact, this article made me rediscover KCalc ;-)\n\nReally, my only wishlist item for KCalc would be a log of last operations. You know, a text widget or similar, where you can go back in time so you don't need to retype a whole operation if you misstyped a number, things like that. \n\nThe idea of having a graphical calculator is to give a GUI abstraction that looks and feels like a real calculator. KCalc does that. Roberto's app doesn't.\n\nNow, don't get me wrong. I am a scientist, I never ever use KCalc, 'cause for a technically inclined person like me it is much faster to type \"python\" and enter my operations. But for many, many people, this doesn't cut it, and an app like KCalc does. \n\nIn short, Roberto's article and app prototype aim at different users than KCalc does. Typing math in python, bc, dc and such is for geeks, and using a calculator is for the 95% rest of the population. Are we saying that we want to ignore this 95% ? Oh, they have windows, I see ;-)\n\nCheers!"
    author: "MandrakeUser"
  - subject: "I like kcalc"
    date: 2004-09-28
    body: "I like kcalc more than Roberto's proposal. Actually I like kcalc a lot.\n\nAn optional tape mode would be a nice improvement to kcalc. \n\nJust my 2 cents,\n\nGareth\n\nPS: I am a big fan of KDE but there are too many \"k\" names. A modest usability improvement would to rename the \"kcalc\" to \"Calculator\"\n\n"
    author: "Gareth Sargeant"
  - subject: "Seconded."
    date: 2004-09-28
    body: "For a Roberto-style calculator, there is bc -l. It's fast, easily opened, and reasonably intuitive. No need to duplicate that in KDE.\n\nFor Kcalc, there's a very different use - it allows people who know how to use a pocket calculator (most everyone) to use a calculator. Remember, outside the programmers' community, there are pretty few people who actually know about text-based interfaces.\n\nYes, It should be renamed Calculator. It took me two minutes to find it in my new and shiny KDE 3.3 menus, and I've used every KDE version since 1.0beta3 ...\n"
    author: "vblum"
  - subject: "Re: I like kcalc"
    date: 2004-09-28
    body: "While I'd prefer \"Calculator\", KCalc or KCalculator are the lesser evils of K-names.\n\nI think a much more irritating thing than the K fixation is that KDE apps can't settle on a single way to incorporate K into their names.\n\nDo you stick a K in front of a normal word, like KSpread?  Do you find a word that starts with a K-sound and misspell it with a K, like Konqueror?  Do you find a word that describes what the application does, and capitalize ANY K-sound in the word, like JuK?  Or do you just make up nonsense words and throw a K into the mix, like AmaroK?\n\nFrankly, if KDE apps could pick any ONE of the above, the names would annoy far fewer people.  As KDE-naming-weirdness supporters would tell you, the first (K in front of a normal word) has analogues in the Windows and Mac worlds.  The others, however, really REALLY need to go.  In a hurry.  And frankly I'd prefer if the K went away altogether (although apps like JuK could rename themselves Juke and I'd be fine with that).  It's not endearing, it's aggravating.\n"
    author: "ac"
  - subject: "Re: I like kcalc"
    date: 2004-09-29
    body: "AmaroK is not a nonsense word: http://dot.kde.org/1096390465/1096406674/1096407703/\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: I like kcalc"
    date: 2004-09-29
    body: "Thanks for the info.  So it appears the four ways to use K in a KDE app are:\n\n- Insert K in front of a normal word\n- Find a word that starts with a K-sound and misspell it with a K\n- Find a word that describes the program's function and turn any K sound into a K\n- Transliterate an Inuit word and make sure to spell it with a K\n\nSounds like the rules are even more divergent than I originally thought.\n"
    author: "ac"
  - subject: "Re: I like kcalc"
    date: 2004-09-29
    body: "What's in a name? that which we call a rose\nBy any other name would smell as sweet;\n\nRomeo & Juliet, 1595"
    author: "anonymous"
  - subject: "Re: I like kcalc"
    date: 2004-09-30
    body: "I cannot tell what the dickens his name is.\n   - Merry Wives of Windsor"
    author: "Ed Moyse"
  - subject: "Re: I like kcalc"
    date: 2004-09-28
    body: "<bad joke>\nWhy not kalculator then?\n</bad joke>"
    author: "standsolid"
  - subject: "Even faster!"
    date: 2004-09-28
    body: "It took me 3 seconds to implement such a calculator with bash!!! :\n\n\"konsole -e lisp\"\n\n(note: You can use your favorite language with a top-level)\n\n:P ;)"
    author: "Sergio"
  - subject: "Re: Even faster!"
    date: 2004-09-28
    body: "Oh, I had not read the python comment before :). But seriously, what does the proposed new GUI application provide over a simple terminal?\n\n\n"
    author: "Sergio"
  - subject: "Re: Even faster!"
    date: 2004-09-28
    body: "Easier recovery of previous results (double click instead of click-drag then middle click)."
    author: "Roberto Alsina"
  - subject: "Re: Even faster!"
    date: 2004-09-29
    body: "\"Easier recovery of previous results..\"  \nyep... all I've got is a button on the panel....\n\n[Desktop Entry]\nComment[en_US]=I kept misspelling dc\nEncoding=UTF-8\nExec=dc\nIcon=kcalc\nName[en_US]=The Calculator\nSwallowTitle=MyCalc\nTerminal=true\nTerminalOptions=--notoolbar --nomenubar --title 'dc' --vt_sz 50x40\nType=Application\nX-KDE-SubstituteUID=false"
    author: "Paul Ahlquist"
  - subject: "Re: Even faster!"
    date: 2004-09-28
    body: "konsole -e lisp\nUh oh.. can't write data..\nUh oh.. can't write data..\nUh oh.. can't write data..\nUh oh.. can't write data..\nUh oh.. can't write data..\nUh oh.. can't write data..\nUh oh.. can't write data..\nUh oh.. can't write data..\nUh oh.. can't write data..\nUh oh.. can't write data..\nUh oh.. can't write data..\nUh oh.. can't write data..\nUh oh.. can't write data..\nUh oh.. can't write data.\n\n-> very userfriendly :)"
    author: "Rinse"
  - subject: "Re: Even faster!"
    date: 2004-10-01
    body: "and even better: use bc or dc, two calculating programs included on every single distribution ;)"
    author: "oliv"
  - subject: "I agree"
    date: 2004-09-28
    body: "I think he's right.\nWe only have this calculator because it looks like the M$ one.\nAnd as always whenever there are radical approaches everyone\nis whining like\n\"Hey, I actually use those buttons when I'm on the phone\".\nThis is completely ridiculous.\nWhy not then integrate a button with every letter in a word-processor?\nPerhaps you need an on-screen-keyboard to get along with your \"disability\" ;-)\nIt doesn't matter how many buttons there are. They are completely useless.\nHis complaint about memory is right, too. \nAnd why use M+ ?\nWhy not type 60+20+30+40?\nAnd why not using the command line  or (insert other app)?\nThis is like saying why improve KDE when you can use Windows.\nThe discussion right now is about whether KCalc is good usability-wise\nor not. Just proposing a different app is no argument whatsoever.\nI'm quite sure such an app can look a bit fancier. It would need\nan ordinary toolbar with buttons for print and perhaps the options\nto open/save whole calculations. Then it would like the other KDE\napps which also a good idea IMO. I'm absolutely sure if Microsoft\nintroduces such a calclator everyone here will follow suite.\nThe problem here is that all innovation is stifled right on the\nspot with unfounded arguments. The question here remains:\nDo we really need those buttons or isn't an on-screen-keyboard\nthe right solution for one-handed/half-eyed whatever people? Yes/No?\nIf not - get rid of them. It's as simple as that IMO.\nBesides: I absolutely hate it when I want to convert multiple numbers\nfrom hex to dec and I have to switch back to hex everytime for the\nnext number. A calculator as in the proposal would make it possible\nto define your own shortcuts, functions etc, etc. It's a much more \nextensible concept and I don't think that even a novice would really \nneed long to understand that you can enter 3*4 and it shows the result.\n"
    author: "Martin"
  - subject: "Compromise of KCalc and UseCalc"
    date: 2004-09-28
    body: "Whilst I agree that the KCalc user interface is ugly, and that UseCalc is far prettier, KCalc is still easier to use from a beginners persepective.\n\nI really like the idea of a calculator which can show the whole equation in one line, as opposed to the current KCalc / Windows Calculator type system.\n\nHowever, I think it is important to have the scientific function buttons available, as well as the number buttons because some people (not myself) do use them - but in a way that is easy to hide when people don't want them.\n\nSo how about:\n\n- UseCalc-style expression entering with a scrollable history list.  If you did the equation in several parts, then clicking and editing a previous equation would optionally recalculate all subsequent equations which depended on that answer.\n- Allow a more friendly expression entering system.  Eg to enter 2**2, you would press [2] [Alt] [2] and the power would appear as a subscript.  This system works well on a graph-plotting package I use at college.   The calculator would need to be able to infer the * sign, eg: 9(6) -> 9*6\n- Number pad which can be revealed or hidden using a tab button (ala. Konqueror or KDevelop, but with a text label so I don't have to mouse-over to find out what it does).\n- Scientific function buttons either placed into a menu, or put on a hideable tab (as with the Number pad above).  I would only use the most important functions - most of the ones on the KCalc pad people will never use.\n"
    author: "Robert Knight"
  - subject: "Usability & Kcalc"
    date: 2004-09-28
    body: "From the usability point of view the buttons in Kcalc are better then the buttons in on-screen keyboard.\n\nOn-screen keyboard emulates a real laptop keyboard where buttons which represent digits are presented in a row. Kcalc digits buttons are grouped. It is much easier and faster to move mouse cursor from one to another grouped button then to skip from left to right and vice versa. Because of this the separate keyboard is much better then laptop keyboard for doing calculations.\n\nA friend of mine lost an arm in the war. He uses KDE (on a laptop) and I asked him what does he prefer: buttons in Kcalc or buttons on a real laptop keyboard/on-screen keyboard? The answer: buttons in Kcalc.\n\nAS many other posters already mentioned a default Kcalc setup has not too many buttons, but, but...\n\nKcalc default setup buttons are grouped wrongly. Digits and basic calculation functions should be better separated from the other for most users unknown functions.\nI am going to create a little mockup and upload it here.\n\nI agree that Kcalc should have history in its window. Even my mobile phone calculator has one.\n"
    author: "wygiwyg"
  - subject: "Re: Usability & Kcalc"
    date: 2004-09-28
    body: "Why not ask the on-screen keyboard guy to add a num keypad?\n\nThat way it works everywhere you need numerical input (like, say, KSpread or IP addresses, or phone numbers)"
    author: "Roberto Alsina"
  - subject: "RPL Calc would be great"
    date: 2004-09-28
    body: "PG Calc is a great calculator (see link below) but it has the same problem as KCalc: useless buttons.\nhttp://www.kde-apps.org/content/show.php?content=11896\n\nA Great calc would be a PG calc with the GUI proposed in this article.\n\nand having that as KDE bar applet that would fold/unfold with a shortcut or mousclick would be awesome :-)\n\n"
    author: "olahaye74"
  - subject: "KMaxima ?"
    date: 2004-09-28
    body: "KCalc is good for what is done for : a simple calculator for \"newbies\".\n\"Geeks\" will use bc, python, perl,....\n\nWhat KDE is missing is a \"computer algebra system\".\nThis already exists and it is GPL : maxima http://maxima.sourceforge.net/ with an existing front-end xmaxima http://maxima.sourceforge.net/screenshots.shtml\n\nxmaxima is good but in tk (ugly) and doesn't interact well with other KDE applications. A KDE front-end (KMaxima ?)will be a very very good point for KDE. Perhaps with MathML import/export ? \n\nPlease keep kcalc as it is but work on KMaxima :)\n\nThanks,\n\nFranck"
    author: "Shift"
  - subject: "Re: KMaxima ?"
    date: 2004-10-01
    body: "OK - I've started on a prototype, you can see a screenshot here:\n\nhttp://kprayertime.sourceforge.net/shots/kmaxima.png\n\nWhat I'm looking for is suggestions on a good user interface - any ideas anyone?\n\nabdulhaq"
    author: "abdulhaq"
  - subject: "Re: KMaxima ?"
    date: 2004-10-02
    body: "Great !\n\nDo you plan to use Qt MathML support ?\n\nFor the interface something similar to Mapple, Mathematica, TI 92 will be good I suppose : All appear as a big sheet with the question align left and results align right.\nGraphics could be integrated in the sheet or open in another sheet.\nToolbars will be add for common operation (sin, integration, sum,...) and propably other optional toolbars specialised (complex, geometry,...).\n\nIt will be a very large project."
    author: "Shift"
  - subject: "Re: KMaxima ?"
    date: 2004-10-02
    body: "We're thinking along the same lines. That's a good sign (isn't it?:)\n\nThe project appears large but already I've got something useful, everything you can see in that screenshot is working as it appears. I need to update my expression parser some more, to handle all the different functions etc, but the back of the job has been broken (I think, anyway). Graphing has yet to be added and I have to to decide what tool to use - gnuplot +/- kdvi, kplot, etc.\n\nThe app has been written in python, the Qt bindings are now a part of the standard KDE distribution and 99% of PCs have python. Once the project has settled down it could be rewritten in C++ but... is it worth it?\n\nIt's not as hard as it seems because maxima does all the hard work. I'm just parsing the in and out expressions so that I can display them nicely.\n Anyway, I'm enjoying it so I don't notice the hard work.\n\nAny other thoughts? What graphics engine to use?\n\nabdulhaq\n"
    author: "abdulhaq"
  - subject: "Re: KMaxima ?"
    date: 2004-10-25
    body: "Is there no library of maxima to use? The source is open I think, so it should not be too difficult to compile one. This would be much easier than parsing the maxima output...\n\nJust my thoughts...\n\nThyMYthOS"
    author: "Manuel Stahl"
  - subject: "Re: KMaxima ?"
    date: 2004-10-25
    body: "And something else...\n\nhttp://kayali.sourceforge.net/ is really a good start, but it needs the input of kformula I think. This would be great."
    author: "Manuel Stahl"
  - subject: "Calculators in Windows"
    date: 2004-09-28
    body: "When people talk about the Windows calculator, they mean the original one that has been there since at least Windows 3.0.  There are others, and they have interesting features.\n\nThe one for Windows CE is like the normal Windows one, but has two extra features.  One is a history, and the other is a compact mode, where it only shows the result field (and a button to go back to full mode).  Anything typed in in the compact mode still goes into the history.\n\nThe other Windows calculator is the powertoy one for XP (free download from MS).  I think the thing I like most about that is that the entry is done on a command line, so parentheses and all that work;  typing 1+1 <enter> +1 <enter> also works - it automatically inserts a variable called \"answer\".  This calculator also has other functionality (graphs etc), and it has no number buttons!  I much prefer this calculator to the normal Windows one.\n\nBack to KDE: having different styles of working is important.  I like the command-line style entry (I was a dc/bc user!), but somebody like my mother likes having the normal calculator entry mode, complete with number buttons.  Others like RPN (although the Polish guy here doesn't!).\n\nA single app which can be configured to do all of these combinations would be nice, but I wonder if that wouldn't kill startup time.  I want to try the ALT+F2 thingy as soon as I get home.  Judging by the response, nobody knew or expected it to work, but that'd be an excellent compact mode calculator, and it should be publicised more!\n\n-- Steve"
    author: "Steve"
  - subject: "reminds me of printKalc"
    date: 2004-09-28
    body: "Hi!\n\nRoberto's suggestion reminds me somehow of printKalc:\n\nhttp://www.hoenig.cc/linux/printkalc.shtml\n\nThis one is a QT/KDE C++ program which does not \"eval\" Python code but has other fancy features. If you like this type of calculators it will be worth a try. :-)\n\nGreetings,\n\n  Gunter Ohrner"
    author: "Gunter Ohrner"
  - subject: "bc"
    date: 2004-09-28
    body: "Didn't you just reinvent bc(1)?  Anyway, what you wrote is an alternative, not a replacement.  Replacement requires a functional equivalent, and to many people, a CLI style interface is not functionally equivalent.  (I, on the other hand, use bc(1) all the time.)\n"
    author: "George Staikos"
  - subject: "derive"
    date: 2004-09-28
    body: "Remember derive.exe?\n\nAn algebraic calculator that made you derive functions, plot functions and so on. algebraic transformations"
    author: "gerd"
  - subject: "Re: derive"
    date: 2004-09-28
    body: "Heard of it, never used it, though."
    author: "Roberto Alsina"
  - subject: "Units"
    date: 2004-09-28
    body: "I generally agree that KCalc is inefficient to use for a an experienced user. I would probably prefer bc instead. Does anyone know of a calculater with a command-line interface that can handle units? Something like Mathematica's 'Units' package would be very nice.\n"
    author: "AC"
  - subject: "World's best calculator in 324 bytes"
    date: 2004-09-29
    body: "Handles unit conversion and anything you can throw at it.\n\nperl -e 'use CGI::Util qw(escape);print \"Eval: \";while(<>){chomp;open(G,\"lynx -source \\\"http://www.google.com/search?q=\".escape($_).\"\\\"|\");while(<G>){if(/<tr><td><img src=\\/images\\/calc_img.gif><\\/td><td>&nbsp;<\\/td><td nowrap><font size=\\+1><b>(.*)<\\/b><\\/td><\\/tr>/){$r=$1;$r=~s/<(.*)>//g;print \"$r\\nEval: \";}}close(G);}'\n\n\n\n"
    author: "Jason Keirstead"
  - subject: "Re: World's best calculator in 324 bytes"
    date: 2004-09-29
    body: "Nice one!"
    author: "Navindra Umanee"
  - subject: "usability improvements"
    date: 2004-09-28
    body: "hmm.. As far as I can see no one has actually proposed the rather simple usability  improvements that needs to be done. \n\n* All functions but addition, division, multiplication and substraction must be removed. Most people don't know how to use the other functions, so they will get in the way for 95% of the instances the calculator is used. Add a button for \"scientific functions\" to unfold those. Memory functions can stay as well.\n\n* rename the program to Calculator. KCalc is not very elegant, and is also somewhat disctracting, since it isn't clear what purpose it has to name it in this fashion.\n\n"
    author: "will"
  - subject: "Frink is the best calculator out there"
    date: 2004-09-29
    body: "You want a real calculator?\n\nThis is a REAL calculator:\nhttp://futureboy.homeip.net/frinkdocs/\n\nAdd it to your Sidebar/navigation panel and voila a sensational calculator integrated into KDE.\nRight click on empty sidebar and click Add New then Web Sidebar Module. Use this URL:\nhttp://futureboy.homeip.net/fsp/sidebar.fsp"
    author: "Roger"
  - subject: "Nonsense?"
    date: 2004-09-29
    body: "Hey, why don't you do the same think with the KDE CD player? People can type in \"start 4\" to listen to the 4th track, \"random\" for shuffle mode, \"stop\" and \"eject\". Why using buttons at all?\nWhy do we use a _G_UI at all? The shell is enough.\n\nIf you really want to be serious, you have to think a little bit more to provide real improvements."
    author: "Andreas"
  - subject: "Re: Nonsense?"
    date: 2004-09-29
    body: "Ok, go read the article carefully, then the rest of this post.\n\nDone yet?\n\nHere\u00b4s why play/ffwd/etc buttons on the CD player make sense:\n\nYour keyboard usually doesn\u00b4t have a play button."
    author: "Roberto Alsina"
  - subject: "Re: Nonsense?"
    date: 2004-09-29
    body: "Come on. My keyboard doesn't have a Pi button, nor Sinus/Cosinus buttons.\n\nAs many posters pointed out, extensions like a payroll are nice. Removing the buttons would confuse the majority of users."
    author: "Andreas"
  - subject: "Re: Nonsense?"
    date: 2004-09-29
    body: "The app you see there is just a quick hack. The article says that.\n\nYour keyboard lacks a pi button. However, your keyboard has a P and a I buttons. You can click them in sucession, and you end with pi. Guess what, that\u00b4s pi ;-)\n\nBut I kinda agree that maybe **some** buttons would be a good idea. I am not a goddamn taliban of buttonlessness. However, those buttons should only be enabled in a trig mode. On the usual 6-operations use (and come on, almost nobody uses calculators for anything else), those buttons are only clutter (and kcalc doesn\u00b4t show them by default either, or does it? (not on Linux right now)\n"
    author: "Roberto Alsina"
  - subject: "NONSENSE!"
    date: 2004-09-29
    body: "When I first read your article I thought you were kidding. Well there are numerous reasons why you are wrong for most people. I give some of them:\n\na) People have been trained to use physical calculators with kcalc's current layout since years. They are used to a calculator with such a layout/look and feel and that makes them feel comfortable when using kcalc -- they feel right at home. Actually that is the best thing that could ever happen when it comes to usability.\n\nb) already mentioned: accessibility\n\nc) While the user might use the keyboard in your approach he's completely lost when it comes to the available _valid_ set of keys. Which keys that I see on my keyboard are valid numbers and operations in the respective mode?\nAm I allowed to type #66, &, **, ^, ~, ++, a, b, c, A, B, C, $, <, \u00b0, ?, !, ... or any other possbible key that I find on my localized physical keyboard? Am I allowed to use the A-Key (Hex) while being in decimal or binary mode? With the valid keys still appearing on the screen in their respective disabled/enabled state the user can _SEE_ what input might be valid and which isn't. That way he has full control over the possible actions he might take.\n\nOn the other hand it's true that kcalc needs improvments: \n\n- The Trigonometrie, Statistics, Logical, Exp/log and Constant -buttons menuentries should go into a settings-dialog. \nInstead kcalc should default to a simple mode and have menu-items called \"Simple calculator\", \"Scientific Calculator\", \"Ticket-Tape\" and \"Custom\" available (with custom being the tweaked layout in the settings-dialog).\n \nIn addition kcalc should have a history and should be renamed to kcalculator to avoid confusion.\n\nIn addition developers should look at the layout of current physical calculators and match their average common layout as close as possible. \nIf there are common well-accepted layout-standards like this available it's complete nonsense to reinvent a new \"standard\" that nobody but KDE is using.\n\nGreetings,\nTorsten Rahn\n"
    author: "Torsten Rahn"
  - subject: "Re: NONSENSE!"
    date: 2004-09-29
    body: "I will try to reply to your points:\n\nYes, I am joking, partly. More on what I proposed as a solution than on what I see as a problem, but on both sides I am joking somewhat.\n\nHere's some more serious reply:\n\na) No, sorry, you are confusing usable with used to. Something can be more usable and yet not be practical because of habit. That doesn' t mean that it is any less usable. It only means that achieving adoption is harder.\n\nOtherwise, we should all use windows, and KDE should clone it. Everyone has years of training on windows, after all.\n\nb) The accesibility argument is wrong. What is so special about a calculator? I enter numbers on my phonebook. Why doesn't my phonebook have a on-screen keypad?\n\nIf on-screen input is so important and the current accessibility solutions don' t fix it, then then the right thing to do is work on the accessibility solutions, not on the calculator.\n\nc) Every key is valid. It doesn't have modes. Modes are usually considered bad usability anyway. And you can trivially do wrong input in kcalc. 2/0. Error.\n\nI don' t see any actual usability rationale in your post except \"people are used to physical calculators\", and I don't think much of that one.\n\nd)"
    author: "Roberto Alsina"
  - subject: "Re: NONSENSE!"
    date: 2004-09-30
    body: "> a) No, sorry, you are confusing usable with used to.\n\nUsability is not only about doing things the easiest way. It's _also_ about ways that people are used to. Otherwise KDE would have explored a much more radical approach to offering a GUI right from the start. But as we knew that acceptance is also about delivering solutions that the user is used to and where he feels right at home we went for the \"clone a Windows/OS2/Mac-Interface with improvements\"-way.\n \n> Something can be more usable and yet not be practical \n> because of habit. \n\nIf that habit has become a widely used standard that is considered to be easy to use by the general public then it makes absolutely no sense to reinvent the wheel. I wouldn't even be surprised if there existed ISO-Standards around calculators already.\nDo you really believe that the manufacturers of physical calculators will adopt your layout? No? Then you are in trouble, because it will mean that people who are used to the usual layout will have to relearn the way they treat their calculator every time they use KDE.\n\n> Otherwise, we should all use windows, and KDE should clone it. \n> Everyone has years of training on windows, after all.\n\nJust in case you missed it: We do already clone Windows most of the time for the reasons stated above. And most of the customers of the company I work for chose KDE just for that particular reason.\n\n> It only means that achieving adoption is harder.\n \ns/harder/close to impossible/\n\n> b) The accesibility argument is wrong. \n> What is so special about a calculator?\n\nIf you don't show the number keys then it will mean that people with disabilities which make it impossible for them to use a keyboard won't have a way to make their input. Of course they could use an on screen-keyboard but then they have to worry about switching between the calculator and the onscreen-keyboard which is rather an unnecessary pain. Also on an onscreen-keyboard you have about 104 keys they will have to choose from and the additional keys that exist on your calculator - makes about 124 keys to choose from. That's actually twice more than choosing from the about 50 keys on kcalc in scientific mode! \nAlso people with cognitive or learning disabilities find it difficult when the presentation, interaction style or task flow varies. If there is no consistency between different calculators, users will have to repeatedly relearn the procedure. \nOn the other hand blind users will definately be happy to use the keyboard, but they can do this with the current calculator already\n\n> I enter numbers on my phonebook. \n> Why doesn't my phonebook have a on-screen keypad?\n\nIt should. That's why even PDA's which optionally can be used as mobiles emulate the well-known phone-layout on the screen (even if they offer the numbers on the physical qwerty-keyboard). And that's also the reason why ATM's  don't use the button-order of a qwerty-keyboard but that of a phone/calculator instead.\n\n> c) Every key is valid. It doesn't have modes. \n> Modes are usually considered bad usability anyway. \n\nNo. While this sentence might be true in some situations it is false for many others.\nTake konqueror for example: currently it doesn't offer any real modes.\nTherefore while in \"webbrowsing-mode\" it still offers lots of operations which only make sense in \"file-browsing-mode\". That's why many users feel that the interface becomes cluttered with unnecessary options and feel lost because of the overwhelming number of menu-entries.\nOnce konqueror will offer true seperation between webbrowsing and filebrowsing mode it will offer less menu-entries which are better focused on the task.\n\nSame for the calculator: They are using modes because the prevent errors for the very same reasons why programmers like a language to be type-safety.\n\n> And you can trivially do wrong input in kcalc. 2/0. Error.\n\nJust because you can already do wrong input it doesn't mean that you shouldn't try to guide a user to prevent wrong input. \n\nActually I think that your proposal would harm usability of kcalc because\n\n- people would have to choose from 104 keys to find their needed keys. People who work on notebooks don't even have a easily accessible number-pad so they are f**cked.\n- people don't see which inputs are valid and feel overwhelmed by the number of possible or not so possible keys they can or can not use as an input. They don't feel guided at all.\n\n "
    author: "Torsten Rahn"
  - subject: "Re: NONSENSE!"
    date: 2004-09-30
    body: "I seem to have missed where I said KCalc should be replaced. I only said it sucks, and Usecalc sucks less :-)"
    author: "Roberto Alsina"
  - subject: "Qalculate!"
    date: 2004-09-29
    body: "why not have a look at Qalculate! (http://qalculate.sourceforge.net/). it uses gtk+ for the gui, but it wouldn't be hard to make a kde version. "
    author: "nique"
  - subject: "You all missed the point"
    date: 2004-09-29
    body: "Roberto was *obviously* just making fun of the KDE usability \"experts\". He applied literally a few rules found in some book somewhere, to an app that is very special. The result was, ofcourse, that it became not only useless but also redundant (you can get the same thing with Alt+F2 or Google or bc or dc or python or lisp or.......)\n\nThis was just a joke on some recent events here on the dot, like when venerable Mattias Etrich proposed that KMail keybindings should strictly follow focus, or when a bunch of loosers cried that amaroK interface sucks because it doesn't have a proper menubar or toolbar.\n\nWell as I always say, when you do something you do it right! So Roberto, for start, your Usecalc doesn't seem to have a toolbar? You absolutely need a toolbar or it's not a proper KDE app! The toolbar can have buttons for Copy, Paste, Print and other such stuff but also a Go button, just like the one seen in Konqueror besides the URL bar - that button would Perform The Calculation.\nAlso please make sure that the arrow keys scroll the history only when it has focus! When the entry textbox has the focus, you can do something else e.g. you can do like bash, just bring whatever was typed earlier or something.\n\n:)))))))))"
    author: "Hahahahah"
  - subject: "Re: You all missed the point"
    date: 2004-09-29
    body: "The history has NoFocus policy, it has no toolbar because I don' t want to put \"1\" \"2\" \"3\" and so on in it, and I posted it before the amarok thing, but you are **partly** right.\n\nWhy partly?\n\na) It is actually a quite nice calculator. I *do* like it more than kcalc.\n\nb) No, Alt+F2's calculator (which I didn't know) is too limited (no floating point?)\n\nc) It is nicer to use than bc/dc/lisp/python because you can fetch the results for reuse in a much nicer manner.\n\nd) I am kinda partial to providing a trig toolbar and a statistics toolbar (which would start disabled). I may release usecalc 0.2 someday (Now with kicking power actions!)"
    author: "Roberto Alsina"
  - subject: "Another suggestion for Kcalc"
    date: 2005-10-16
    body: "I really like KCalc as it is, except the fact that when you do things like: 2.0 + 2.0 you get 4.000000000000001 to the screen.  It may not seem that annoying, but it can become very annoying when just trying to balance the checkbook.  I do some programming and I know it wouldn't be that hard to add a persistant mode, so that KCalc can watch the precision coming in and make sure that the number going out is matched to the largest precision when it is displayed to the screen.  This means that if you enter 1.2 + 1.24 you should get 1.44 or 2.0 + 2.0 will be 4.0, just my take."
    author: "jammer"
---
Ok, so I don't like <a href="http://docs.kde.org/en/3.3/kdeutils/kcalc/">KCalc</a> much. I think that usability-wise, it's the worst KDE application ever. So here's <a href="http://www.pycs.net/lateral/stories/33.html">a proposal for a replacement</a>. And since code speaks louder than words, here's an <A href="http://lateral.pycs.net/static/usecalc-0.1.tar.bz2">implementation</a> too (<a href="http://www.riverbankcomputing.co.uk/pyqt/">PyQt</a>). 








<!--break-->
<p>
<i><b>[<a href="mailto:navindra@kde.org">Ed</a></a>:</b> <a href="http://www.collectors-network.com/news/worstEpisodeEver.jpg">Screenshot</a> of <a href="http://ree.typepad.com/lackwit/2004/05/worst_episode_e.html">Roberto</a>?]</i>








