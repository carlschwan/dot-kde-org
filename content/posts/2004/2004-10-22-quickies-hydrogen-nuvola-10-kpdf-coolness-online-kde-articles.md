---
title: "Quickies: Hydrogen, Nuvola 1.0, KPDF Coolness, Online KDE Articles"
date:    2004-10-22
authors:
  - "fmous"
slug:    quickies-hydrogen-nuvola-10-kpdf-coolness-online-kde-articles
comments:
  - subject: "Acrobat 6?"
    date: 2004-10-21
    body: "How does kpdf is on opening Acrobat 6 files?\nI've tryied it and some other linux pdf viwers and none opened pdf files created with it :("
    author: "Iuri Fiedoruk"
  - subject: "kpdf uses xpdf"
    date: 2004-10-21
    body: "Probably the same as latest xpdf, because that is what it uses for rendering the PDF file."
    author: "Anonymous Coward"
  - subject: "PDF advances"
    date: 2004-10-21
    body: "It's good to see that KPDF is finally putting in the features that we've missed for so long. Proper handling of thumbnails, tables of contents and bookmarks will be much appreciated.\n\nI can't tell from the screenshots, so maybe somebody can tell me -- can it now select text within a PDF to copy & paste?\n\nOh, and Hydrogen is great... except that it will have the same destructive influence on my work that Fruity Loops did all those years ago :)"
    author: "Tom"
  - subject: "Re: PDF advances"
    date: 2004-10-22
    body: ">I can't tell from the screenshots, so maybe somebody can tell me -- can it now select text within a PDF to copy & paste?\n\nNo. The bigger issue is, that it is hell of a lot slower than xpdf3 - especially with large pdf's, building all the thumbnails - while xpdf3 itself is only the second choice compared to acroread regarding speed/usability."
    author: "Carlo"
  - subject: "Re: PDF advances"
    date: 2004-10-22
    body: "Text selection in selection mode is possible in the cvs version. As for being slower than xpdf3... considering kpdf uses xpdf to render the pages, it shouldn't be too much slower."
    author: "James Horey"
  - subject: "Re: PDF advances"
    date: 2004-10-22
    body: "speed, i believe. but acroread and usability in the same sentence? gah. any app that closes the but not the window when you click the window close button is just broken. and it generally goes downhill from there."
    author: "Aaron J. Seigo"
  - subject: "Re: PDF advances"
    date: 2004-10-22
    body: ">but acroread and usability in the same sentence? gah. any app that closes the but not the window when you click the window close button is just broken.\n\nThis behaviour is indeed stupid and the gui (on *nix) is a mess. But! It's fast, the scrolling is smooth and I get a bookmark tree, which is far more important, than the thumbnails, which are pretty useless, since they look (more or less) all the same."
    author: "Carlo"
  - subject: "Re: PDF advances"
    date: 2004-10-22
    body: "Don't the first and third screenshots show KPDF's ability to use bookmarks and a table of contents?"
    author: "Tom"
  - subject: "Re: PDF advances"
    date: 2004-10-23
    body: "I can't open my pdf's with a screenshot. Comparing released/(supposed to be)stable software with cvs stuff is not serious."
    author: "Carlo"
  - subject: "Re: PDF advances"
    date: 2004-10-23
    body: "> Comparing released/(supposed to be)stable software with cvs stuff is not serious.\n\nWell, CVS is where kpdf is being developed and the cool stuff is on a branch where kpdf has been reengineered.. You're free to compare adobe reader with xpdf 1.0 if you want, but the article was for announcing features in active development. If the screenshot isn't good for you, go check out the branch and try that.\n"
    author: "koral"
  - subject: "Re: PDF advances"
    date: 2004-10-23
    body: "Since the new features in kpdf makes me jump up and down with joy :-) Could you please tell me the magic secuence to get thath branch from CVS? "
    author: "Morty"
  - subject: "Re: PDF advances"
    date: 2004-10-23
    body: "in kdegraphics folder:\ncvs up -r kpdf_experiments kpdf"
    author: "tpr"
  - subject: "Re: PDF advances"
    date: 2004-10-23
    body: ">Well, CVS is where kpdf is being developed and the cool stuff is on a branch where kpdf has been reengineered.. You're free to compare adobe reader with xpdf 1.0 if you want, but the article was for announcing features in active development. If the screenshot isn't good for you, go check out the branch and try that.\n\nSomehow I knew I would get such a reply. Please accept it, that I (mostly) try to argue from the viewpoint of an average user. The stuff you have in cvs looks fine, but the majority of people won't/can't check out, compile and install. It's really only fair to compare the latest, easily available (released) software. Btw., I suppose you're a kde developer: I always wondered why zooming in KDE looks like (+) 100% (-). Usually - not only in other software - it's the other way around."
    author: "Carlo"
  - subject: "Re: PDF advances"
    date: 2004-10-23
    body: "With all due respect, the entire point of this news item is to draw attention to the new features and development. You are coming to a discussion of progress with the assertion that all development is irrelevant because the current stable release version ought to be synonymous with the development version.\n\nBut the point of development is the understanding that the present version is lacking, or can be improved. This is true of all research and the creation of new products in any field of endeavour.\n\nComparisons against present market competition and previous iterations of the product in question is an essential part of research and development, otherwise the programmer is producing something in isolation that may be useless or regressive. You have to assess failures and areas of improvement before it is reasonable to move forward. Similarly, you have to perform regular analysis of your development against market progress to continue in a healthy manner.\n\nThis is the point of these posts, and highlighting new features and accurate comparisons with products such as xpdf and Adobe's Acrobat Reader are valuable. Of course the features in CVS should be presented as a complete product as soon as possible, but that does not mean that the development version is useless and foolish until that time."
    author: "Luke Chatburn"
  - subject: "Re: PDF advances"
    date: 2004-10-24
    body: "Luke, Aaron asked why I think, that Acroread is more usable. Comparing with the cvs version doesn't make sense in this context, if you want to use it now. This has nothing to do with the announcement or if the code in cvs compiles and runs perfectly or not. I neither said the development version is useless or foolish.\n\n>You are coming to a discussion of progress with the assertion that all development is irrelevant because the current stable release version ought to be synonymous with the development version.\n\nI did not say \"development is irrelevant\". Sorry, but your impression of that what I said is completely wrong."
    author: "Carlo"
  - subject: "Re: PDF advances"
    date: 2004-10-22
    body: "Obviously you have not tried to use acroread 6 on MS \"Windows\".   If you had you would know that any gains in runtime speed it has are more than made up for by losses in load time.  Someone else already mentioned just how unusable anything pdf is."
    author: "bluGill"
  - subject: "Re: PDF advances"
    date: 2004-10-27
    body: "You can drastically reduce Adobe Reader 6 load times by removing or disabling most of the useless plugins it loads at startup."
    author: "Kamil Kisiel"
  - subject: "Re: PDF advances"
    date: 2004-10-22
    body: ">>I can't tell from the screenshots, so maybe somebody can tell me -- can it now select text within a PDF to copy & paste?\n>No.\n\nYes. On branch it copies text. I've not tested the program, but the code is there. :-)\n\n>The bigger issue is, that it is hell of a lot slower than xpdf3\n\nIn branch you have a little overhead over imported xpdf3 sources. about 5-10% slower. But since it caches contents and is meant to perform forward-caching on next pages, it can be very faster than xpdf. Search using the search lineedit is *many* times faster than acrobat reader (after the first loop).\n\nAn issue may be xpdf rendering backgrund graphics.. that takes too long and such pdfs are very slow rendered compared to acroread counterpart.\n"
    author: "koral"
  - subject: "Re: PDF advances"
    date: 2004-10-22
    body: "In branch you have a little overhead over imported xpdf3 sources. about 5-10% slower.\n\nSorry, my formulation was a bit unclear. The thumbnail rendering is the culprit. Opening a 500 page PDF 1.3 document, it takes >5 seconds to print the first 10 visible thumbnails. I know it is a bit unfair, but acroread as the competitor needs ~one second for the same amount of thumbnails on the same box. When you now directly jump to e.g. page 300, it takes more visibly more time to show the page with kpdf, than with xpdf, than with acroread. While the thumbnails are right there with acroread, it now takes the \"hell of a lot of\" time to draw them with kpdf."
    author: "Carlo"
  - subject: "error compiling kpdf"
    date: 2004-10-22
    body: "I wanted to use latest kpdf but I got this during make:\n\nmake[1]: Entering directory `/root/kdecvs/kdegraphics/kpdf/kpdf'\nif /bin/sh ../../libtool --silent --mode=compile --tag=CXX g++ -DHAVE_CONFIG_H -I. -I. -I../.. -I./.. -I./../splash -I./../goo -I./../xpdf -I/usr/include/kde -I/usr/include/qt3 -I/usr/X11R6/include  -I/usr/include/freetype2  -DQT_THREAD_SUPPORT  -D_REENTRANT  -Wnon-virtual-dtor -Wno-long-long -Wundef -ansi -D_XOPEN_SOURCE=500 -D_BSD_SOURCE -Wcast-align -Wconversion -Wchar-subscripts -Wall -W -Wpointer-arith -Wwrite-strings -O2 -Wformat-security -Wmissing-format-attribute -fno-exceptions -fno-check-new -fno-common -DQT_CLEAN_NAMESPACE -DQT_NO_ASCII_CAST -DQT_NO_STL -DQT_NO_COMPAT -DQT_NO_TRANSLATION  -MT kpdf_part.lo -MD -MP -MF \".deps/kpdf_part.Tpo\" \\\n  -c -o kpdf_part.lo `test -f 'kpdf_part.cpp' || echo './'`kpdf_part.cpp; \\\nthen mv -f \".deps/kpdf_part.Tpo\" \".deps/kpdf_part.Plo\"; \\\nelse rm -f \".deps/kpdf_part.Tpo\"; exit 1; \\\nfi\nkpdf_part.cpp: In member function `void KPDF::Part::find()':\nkpdf_part.cpp:941: error: `setSupportsBackwardsFind' undeclared (first use this\n   function)\nkpdf_part.cpp:941: error: (Each undeclared identifier is reported only once for\n   each function it appears in.)\nkpdf_part.cpp:942: error: `setSupportsCaseSensitiveFind' undeclared (first use\n   this function)\nkpdf_part.cpp:943: error: `setSupportsWholeWordsFind' undeclared (first use\n   this function)\nkpdf_part.cpp:944: error: `setSupportsRegularExpressionFind' undeclared (first\n   use this function)\nmake[1]: *** [kpdf_part.lo] Error 1\nmake[1]: Leaving directory `/root/kdecvs/kdegraphics/kpdf/kpdf'\nmake: *** [all-recursive] Error 1\n\nI use gcc version 3.3.5 (Debian 1:3.3.5-1)"
    author: "Pat"
  - subject: "Re: error compiling kpdf"
    date: 2004-10-22
    body: "yeah post errors on the dot :>> its fun reading them , really :> blub"
    author: "chris"
  - subject: "Re: error compiling kpdf"
    date: 2004-10-22
    body: "That's simply because your kdelibs is too old"
    author: "Albert Astals Cid"
  - subject: "Italy rocks!"
    date: 2004-10-22
    body: "David Vignoni and Enrico Ros! Italian OSS development rocks! :)"
    author: "Davide Ferrari"
  - subject: "Security Alert"
    date: 2004-10-22
    body: "Please note that there is a security alert about KPDF (due to xpdf)\nhttp://kde.org/info/security/advisory-20041021-1.txt\n\nAnd the same vulnerability exist in the PDF import filter of KWord in KOffice 1.3.x (see KOffice News: http://kde.org/areas/koffice/news.php )\n\nHave a nice day!\n\n"
    author: "Nicolas Goutte"
  - subject: "OMG WOW"
    date: 2004-10-23
    body: "OMG WOW I have been awaiting this KPDF news for longer than I can remember. After having tested some 300-500 page PDFs that used to choke KPDF but now load quickly and smoothly, and having seen the excellent new interface, features, and todo list, I am ever grateful of this news. Finally, the day I have eagerly awaited has come: removing Acrobat for good. Thanks! Keep up the great work!"
    author: "Jose Hernandez"
  - subject: "Re: OMG WOW"
    date: 2004-10-23
    body: "yeah, aren't these guys great ;-)"
    author: "superstoned"
  - subject: "non-standard widgets"
    date: 2004-10-23
    body: "What kind of tab widgets are displayed on the left? \n\nIs that going into kdelibs?\n\nMark"
    author: "kmsz"
  - subject: "Re: non-standard widgets"
    date: 2004-10-23
    body: "QToolBox. Very simple api (watch it in assistant). It's the one you can see in designer, or in k3b cvs or in other projects such as konqueror's sidebar mods that appeared on kde-look.\n"
    author: "dev"
  - subject: "offtopic: is it me or KDE?"
    date: 2004-10-23
    body: "sorry, it's offtopic, but didn't find anything at google:\n\n- is there any option in KMail to display smiley graphics (like in th-bird/opera-m2) instead of display ':-)'? (should be in text mails too)\n\n- is there any option in KMail to have only in KMail Firefox the default program for http/-s and ftp links? it's not nice to open konqueror and after typing in 'www.google.com' konqui opens Firefox ;-) (there are moments when I want to surf with konqueror)\n\n- is there any option in KDE to have the desktop icons placed at that place where there are before I logout/rebooting? I tried different options in context menu (snap to grid?) but doesn't help. Or if I move one 'non-kde' Icon (eg firefox icon) all icons on my desktop moves some pixels down (if I have snap to grid turned on)\n\nThanks for you help!!!!\n"
    author: "anonymous"
  - subject: "Re: offtopic: is it me or KDE?"
    date: 2004-10-23
    body: ">>sorry, it's offtopic, but didn't find anything at google\n\nStrange, I just googled and got 45.700.000 results for 'anything'.\n\n>>Thanks for you help!!!!\n\nNo problem."
    author: "ac"
  - subject: "Re: offtopic: is it me or KDE?"
    date: 2004-10-24
    body: "> Strange, I just googled and got 45.700.000 results for 'anything'.\n\nOh, you're a great gay, not like me!!!1!\n"
    author: "anonymous"
  - subject: "Re: offtopic: is it me or KDE?"
    date: 2004-10-24
    body: "Please skip insults."
    author: "Anonymous"
  - subject: "Re: offtopic: is it me or KDE?"
    date: 2005-06-29
    body: "he meant guy not gay. so ac is maybe not gay but he should shut up his mouth anyway"
    author: "anonymous"
  - subject: "Re: offtopic: is it me or KDE?"
    date: 2004-10-24
    body: "\"- is there any option in KMail to display smiley graphics (like in th-bird/opera-m2) instead of display ':-)'? (should be in text mails too)\"\n\nNope.\n\n\"- is there any option in KMail to have only in KMail Firefox the default program for http/-s and ftp links? it's not nice to open konqueror and after typing in 'www.google.com' konqui opens Firefox ;-) (there are moments when I want to surf with konqueror)\"\n\nYou can set this globally at KDE Control Center->KDE Components-> Component Chooser -> Web Browser. Requires KDE 3.3.x.\n\n\"- is there any option in KDE to have the desktop icons placed at that place where there are before I logout/rebooting? I tried different options in context menu (snap to grid?) but doesn't help. Or if I move one 'non-kde' Icon (eg firefox icon) all icons on my desktop moves some pixels down (if I have snap to grid turned on)\"\n\nIt works like that by default in here... so I don't know what to say...\n"
    author: "John Features Freak"
  - subject: "Re: offtopic: is it me or KDE?"
    date: 2004-10-24
    body: ">> ...smiley...\n> Nope.\n\nsad, it's not important, but I like them, cause email shows me more feelings from the other person...\n\n> You can set this globally at...\n\nglobally means that _every_ program use for eg. firefox as the default webbrowser, konqueror too. But if I open konqueror I want to browse with konqueror... that's what I want. It's ok, that every other program opens firefox (it's my default browser), but second is konqueror that I open manually and want to browse with them.\n\n> It works like that by default in here... so I don't know what to say...\n\nMy icons geting sometimes at the wrong position after logout/reboot. It should really stays at this position like in Rox-Filer or Nautilus after logout/reboot.\n\n"
    author: "anonymous"
  - subject: "Re: offtopic: is it me or KDE?"
    date: 2005-01-19
    body: "Hi,\n\na workaround for your firefox issue might be:\n\n- Enable the Klipper actions (the clipboard popups).\n- Add Firefox to the URL actions in Klipper.\n- If you want to open a URL with Firefox, right-click on it, select \"Copy \", and wait for Klipper to pop up, then select \"Open with Firefox\" (or whatever you called your Firefox shortcut in Klipper).\n\nHTH, HAND\n\nJens"
    author: "Jens"
  - subject: "Nuvola"
    date: 2004-10-24
    body: "Thanks, David!"
    author: "stripe4"
  - subject: "Re: Nuvola"
    date: 2004-10-24
    body: "G-R-E-A-T icon theme! No, it's the BEST one! :-)))"
    author: "abec5"
---
<A href="http://www.linuxjournal.com/">Linux Journal</A> published the article: '<A href="http://www.linuxjournal.com/article.php?sid=7846">An Introduction to Hydrogen</A>', <A href="http://hydrogen.sourceforge.net/">an advanced drum machine</A> programmed in <A href="http://www.trolltech.com/products/qt/">Qt</A> and highly rated at <A href="http://www.kde-apps.org/content/show.php?content=14152">KDE-apps.org</A>. *** 

Last week <A href="http://dot.kde.org/1089270630/">famous graphic artist</A> David Vignoni, <A href="http://www.icon-king.com/index.php?p=23">released version 1.0</A> of the <A href="http://www.kde-look.org/content/show.php?content=5358">Nuvola icon theme</A>. Now with over 600 icons Nuvola will surely make your desktop an eyecandy and colorful experience. This icon theme can be <A href="http://www.icon-king.com/files/nuvola-1.0.tar.gz">downloaded</A> from the homepage of David Vignoni. ***

On the comments section of <A href="http://dot.kde.org/1097509333/">this Dot article</A> KPDF hacker Enrico Ros posted a <A href="http://dot.kde.org/1097509333/1097513291/">comment</A> which 
 <A href="http://static.kdenews.org/fab/screenies/kpdf/kpdf_singlepage_contents.png">showed</A> <A href="http://static.kdenews.org/fab/screenies/kpdf/kpdf_continous_search.png">some</A> <A href="http://static.kdenews.org/fab/screenies/kpdf/kpdf_2p_continous_popup.png">cool</A> <A href="http://static.kdenews.org/fab/screenies/kpdf/kpdf_2pages_fitPage.png">screenshots</A> of KPDF in the kpdf_experiments branch. ***

Even more KDE coverage in the <A href="http://www.linux-magazine.com/issue/48">latest Linux Magazine</A> (issue 48). Articles about <A href="http://www.linux-magazine.com/issue/48/KDE_Kontact.pdf">Kontact</A> and <A href="http://www.linux-magazine.com/issue/48/KXML_Editor.pdf">KXML</A> can all be downloaded from their <A href="http://www.linux-magazine.com/Magazine/Archive">online archive</A>. Be sure to <A href="http://www.linux-magazine.com/issue/48/KDE_World_Summit_2004_aKademy.pdf">read the article</A> covering aKademy hint: use latest KDPF for viewing these articles).
<!--break-->
