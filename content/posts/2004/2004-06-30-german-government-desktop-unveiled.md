---
title: "German Government Desktop Unveiled"
date:    2004-06-30
authors:
  - "amueller"
slug:    german-government-desktop-unveiled
comments:
  - subject: "From the mysterious future..."
    date: 2004-06-30
    body: "Wow, posted Wednesday 2004/6/30. News from the future. Can you please include Euro 2004 news on the dot, I'd like to start a few bets..."
    author: "AC"
  - subject: "Re: From the mysterious future..."
    date: 2004-07-01
    body: "LOL! Thanks for pointing this out.\nI would have missed this great opportunity to have a good\nlaugh otherwise ;-)"
    author: "Martin"
  - subject: "Re: From the mysterious future..."
    date: 2004-07-01
    body: "You people seem to forget that there are many time zones in the world and half the world may be a day ahead of you in terms of date."
    author: "Anonymous"
  - subject: "Re: From the mysterious future..."
    date: 2004-07-01
    body: "probably americans :)"
    author: "anonymous"
  - subject: "Re: From the mysterious future..."
    date: 2004-07-02
    body: "There are time zones two days ahead?\n\nWhereabouts?"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: From the mysterious future..."
    date: 2004-07-03
    body: "Probably the French.  I think it's metric time.  ;-)"
    author: "Scott Wheeler"
  - subject: "Re: From the mysterious future..."
    date: 2004-07-03
    body: "Do you really thing that it is appropriate to post such a comment with a kde.org address?"
    author: "Nicolas Goutte"
  - subject: "Re: From the mysterious future..."
    date: 2004-07-03
    body: "Why of course yes, because the French, like everybody else, can take a joke :)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: From the mysterious future..."
    date: 2004-07-02
    body: "The moment I post this, it's the 30th June almost everywhere except some islands where it's still the 29th. But it's nowhere the 2nd July. The server date *is* wrong."
    author: "Anonymous"
  - subject: "Great"
    date: 2004-06-30
    body: "OSS is virgin territory to most and much more approachable by CD than by pulling pieces from the web.\nAre there any takers yet (my German is rusty)? I guess this a competitor to SuSE (which also is Debian based AFAIK). But what about support?\nAnd why Mozilla - what is it that it can do that Konqueror cannot? Does it integrate with OO? I have it but use only Konqueror."
    author: "Claus"
  - subject: "Re: Great"
    date: 2004-06-30
    body: "SUSE is not Debian based."
    author: "Anonymous"
  - subject: "Re: Great"
    date: 2004-06-30
    body: "slack-> debian\nslack-> suse  \n\nso debian and suse are brother and sister cause both have the same mom ? ? ?"
    author: "Thomas"
  - subject: "Re: Great"
    date: 2004-07-01
    body: "As far as I know, Debian, Slackware and SUSE are three completely different products, none of them based on another."
    author: "Stew"
  - subject: "Re: Great"
    date: 2004-07-01
    body: "suse is originally based on slackware"
    author: "anonymous"
  - subject: "Re: Great"
    date: 2004-07-02
    body: "/suse is originally based on slackware/\n\nYeah, a long, long time ago...."
    author: "Anonymous"
  - subject: "Re: Great"
    date: 2004-07-02
    body: "... and at some point they adopted RedHats package management system (aka RPM) and even tryed to be a bit compatible with RedHat's RPM's.\n\nSecretly i hope SuSE will someday adopt Debians 'apt' (.deb) for their packages; for the sake of online updating, for the eaz adding of 'sources' and for freedom in general [Oeps no secret anymore]\n\nRight know i use apt-for-suse to install the needed multimedia packages. This takes alltogether an extra 1/2 - 1 hour on the install time."
    author: "cies"
  - subject: "Re: Great"
    date: 2004-07-02
    body: "> for the sake of online updating, for the eaz adding of 'sources' and for freedom in general\n\nWhy for freedom? And for the other two things you don't have to switch the package format.\n\n> Right know i use apt-for-suse to install\n\nYou say it. Other solutions for RPMs are yum and urmpi. No need to switch to .deb packages."
    author: "Anonymous"
  - subject: "Re: Great"
    date: 2004-06-30
    body: "http://derstandard.at for example."
    author: "hiasl"
  - subject: "Re: Great"
    date: 2004-07-01
    body: "Konqueror seems ot have trouble with complex scripts and complex stylesheets, in my experience."
    author: "Anonymous"
  - subject: "Re: Great"
    date: 2004-07-01
    body: "Not now, and especially not with KHTML being used in Safari. I've experienced surprisingly few problems, if any, with my web browsing. Mozilla is put down here simply because it is more well known."
    author: "David"
  - subject: "Re: Great"
    date: 2004-07-01
    body: "> And why Mozilla - what is it that it can do that Konqueror cannot?\n\nThe unfortunate truth is that Mozilla is better at displaying broken websites than Konqueror is.  I don't think that is it possible that Konqueror will ever get there by just fixing bugs.  \n\nThe technology in Mozilla is open source, but is there anything other than the code which is available -- do they have a test suite of broken HTML to use for development?\n\nThe most efficient method is to use their code.\n\nMy suggested solution is to either totally integrate FoxFire/Gecko into KHTML or to make a FoxFire/Gecko based KPart so that users can choose to use it in place of KHTML.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Great"
    date: 2004-07-02
    body: "> > And why Mozilla - what is it that it can do that Konqueror cannot?\n\nmail, irc, webpage composer, and all that on the windows platfom too...\n\n> The unfortunate truth is that Mozilla is better at displaying broken websites than Konqueror is. I don't think that is it possible that Konqueror will ever get there by just fixing bugs. \n\nTrue.\n\n> The most efficient method is to use their code.\n\nKHTML is (IMO) more efficient than gecko... If there would be a gecko-kpart this will result in loss of speed. Not having to use the bloated gecko was the main reason to create KHTML in the first place.\n\n> My suggested solution is to either totally integrate FoxFire/Gecko into KHTML or to make a FoxFire/Gecko based KPart so that users can choose to use it in place of KHTML.\n\nIIRC this is done some time ago, but was not maintained... \nhttp://www.google.com/search?q=gecko+kpart\n\n\nMY CURRENT SOLUTION is to start Mozilla/Firefox when Konq cannot view a particular website. Which is like once every 2 months for me. When there would be a site that i regularly have to use and that i cant open with Konq I'll let the site maintainer and the konq-team know, till that day I work like this.\n\nmy .02 riel (cambodian money)\ncies."
    author: "cies"
  - subject: "Re: Great"
    date: 2004-07-02
    body: "> > And why Mozilla - what is it that it can do that Konqueror cannot?\n> mail, irc, webpage composer\n\nAnd why are they breaking it into single applications now then?\n\n> Not having to use the bloated gecko was the main reason to create KHTML in the first place.\n\nDidn't khtml exist already before Gecko?"
    author: "Anonymous"
  - subject: "Re: Great"
    date: 2004-07-02
    body: "\"KHTML is (IMO) more efficient than gecko...\"\nNot just yours... I believe Apple choose KHTML over gecko for a number of reasons...not just efficiency either."
    author: "am"
  - subject: "Regression in KDE on RH"
    date: 2004-07-02
    body: "> MY CURRENT SOLUTION is to start Mozilla/Firefox when Konq cannot view a\n> particular website. Which is like once every 2 months for me. When there would\n> be a site that i regularly have to use and that i cant open with Konq I'll\n> let the site maintainer and the konq-team know, till that day I work like this.\n\nThis is just my experience, don't take it as real survay.\n\nI was happily using Konqi from RH8 default KDE (3.0.4 IMHO). Quite happy about that. As I worked for Linux based company, it was my desktop 99% of the time. Only few pages did not want to render properly. But they were readable anyway.\n\nOnce, I screwed up my HDD, so I installed RH9, and its 3.1.? KDE. I was not quite happy, so I upgraded to then latest 3.1.4. Suddenly, I had to go to Mozzila very, very often. I was quite dissapointed. \n\nOne month later, I moved to win-based job (I am writing this in Mozilla 1.6 on WinXP), so I cannot say about KDE 3.2.x, but this regression left a bad taste in my mouth.\n\nAnd I cannot forget my true love: KMail! Nothing comparable to it in Win world!\n"
    author: "tomislav"
  - subject: "Re: Regression in KDE on RH"
    date: 2004-07-03
    body: "Kmail IS great, but Eudora was greater still. WAY more configurable with more features...but Kmail is growing on me."
    author: "Joe"
  - subject: "Re: Great"
    date: 2004-07-02
    body: ">The unfortunate truth is that Mozilla is better at displaying broken websites...\n\nMakes me think of bad 3rd party Windows drivers making Windows look bad.\n\nAnyway - also with the browser id set to Windows? Apple's trademark to me is perfectionism if anything and it's puzzling then that Safari would adopt khtml - but maybe that's what Apple fixed (some of) and gave back."
    author: "Claus"
  - subject: "Re: Great"
    date: 2004-07-03
    body: "I still use firefox once in a while though, and it has nothing to do with bad html coding. For MathML suppor for instance. Also cannot wait for \"find as you type\" support in KDE 3.3 :) That said, Konqueror is already my favourite browser. \n\nP.S.: \nThe new possibility to \"list all links\" is *great* :D"
    author: "John WebSurfing Freak"
  - subject: "Re: Great"
    date: 2004-07-03
    body: "Mozilla and Konqueror are both easily available. \n\nOf course Konqueror is the better choice ;-)\n\nTackat"
    author: "tackat"
  - subject: "Re: Great"
    date: 2004-07-04
    body: "how do you get it to load in english?"
    author: "mike"
  - subject: "Can I download this distro/ISO?"
    date: 2004-06-30
    body: "Is this version free for download? And if it's free where can I find it and download it?"
    author: "slaff"
  - subject: "Re: Can I download this distro/ISO?"
    date: 2004-06-30
    body: "I found an iso here: http://source.rfc822.org/pub/local/erposs/"
    author: "Peter Rockai"
  - subject: "Re: Can I download this distro/ISO?"
    date: 2004-06-30
    body: "Only German version available?"
    author: "jmk"
  - subject: "Re: Can I download this distro/ISO?"
    date: 2004-07-01
    body: "You do understand the basis of the article?\n\"German Government Desktop Unveiled\""
    author: "Paul"
  - subject: "Re: Can I download this distro/ISO?"
    date: 2004-07-03
    body: "> You do understand the basis of the article?\n\nVery well thank you.\n\n\n> \"German Government Desktop Unveiled\"\n\nIt may come as a suprise to many Germans, but Internet & computers mostly\nspeak English to you (from one corner or another). Thus, some may prefer to \nuse their desktop with one language only - no mixed language settings to\nme, thank you. I hate partial translations; which, they more or less always\nare."
    author: "jmk"
  - subject: "Re: Can I download this distro/ISO?"
    date: 2004-07-02
    body: ">>Only German version available?\n\ngee.. now that's really a surprise don't you think? Who would would have figured..\"German Government Desktop\".\n\nAccording to my superstring theory calculations, it should be in German.\n\nAnyone agree? Then we can start a movement unifying the \"German Government Desktop in German\" theory."
    author: "ac"
  - subject: "Re: Can I download this distro/ISO?"
    date: 2004-07-02
    body: "Those damn jerries should learn to speak the Queens English!\n\nWell, i guess we will have wait for the official EU version."
    author: "reihal"
  - subject: "Re: Can I download this distro/ISO?"
    date: 2004-07-03
    body: "Links to both installation and live cd are listed on http://www.heise.de/security/artikel/48807"
    author: "binner"
  - subject: "Re: Can I download this distro/ISO?"
    date: 2004-07-03
    body: "Just to make this clear:\n\nThe distro was designed for native german speakers, so there? s no need for an english version.\n\nOr do you think, the guys/girls from your gouverment are able to use a german OS?? Well, I don?t think so.\n\nResume:\nThe distro does exactly what it should do: TALK GERMAN!"
    author: "harry"
  - subject: "KOffice?"
    date: 2004-07-01
    body: "Why not KOffice?  In my opinion, it has many advantages over OpenOffice."
    author: "Shaman"
  - subject: "Re: KOffice?"
    date: 2004-07-01
    body: "Apart of being faster, I see none... unfortunately :-("
    author: "ac"
  - subject: "Re: KOffice?"
    date: 2004-07-02
    body: "Several times I tried to use KOffice to work on more complex .doc files of WinWord2000. It was hell how KOffice misformatted the files. I understand why they don't want to use it 'cause beside .pdf it's quasi-standard for file exchange in the real world... sorry for bringing you down."
    author: "the Mysterious KDE user"
  - subject: "Re: KOffice?"
    date: 2004-07-02
    body: "I've used Kword since January for writing reports. I really wanted to be able to use Kword but I found that it just isn't ready for prime time use. It needs a lot of polish and bug squishing. In general, I found that it took longer to complete my work using Kword then when I used OpenOffice.org.\n\nJarefri"
    author: "Jarefri"
  - subject: "nothing new, but indeed cool"
    date: 2004-07-01
    body: "On the 5th FISL in Porto Alegre, brazil, brazilian government throught it's ITI arm (institute for technologies) was giving away nice customized kurumim (fork of knopix) live cds with propaganda to show people and companies (there where two flavors for you to choose) about linux and open source.\n\nGovernments are taking linux very seriusly almost everywhere. The biggest resistence seems to come from US, a logic thing because the country gets a lot of money from software export."
    author: "Iuri Fiedoruk"
  - subject: "Re: nothing new, but indeed cool"
    date: 2004-07-01
    body: "Yes, we here in the U.S. are bastards.  OSS does need to be taken seriously, not only within governments, but also in companies and personal use.  Unfortunatly, many people here are still stuck on Windows.  I have been trying to convince my friend to use Linux, but he is so stuck in the Windows rut that I can't get anything across to him.\nHim: \"I like the Windows interface.\"\nMe: \"Linux looks almost exactly the same.\" \nHim: \"I like the Windows programs.\"\nMe: \"Linux has almost identical programs.\"\nHim: \"I like games.\"\nMe: \"I can run Windows games on Linux.\" (http://www.transgaming.com)\nHim: \"...No wanna try Linux...\"\n\nI don't get it.  Why are people so opposed to it over here?"
    author: "Eric Kincl"
  - subject: "Re: nothing new, but indeed cool"
    date: 2004-07-01
    body: "Oh, I have a theory: they simply are LASY!\nWhy change?\n\nActually this could even herlp linux, give a person a computer with linux pre-installed and ... it will use and like it!\nMost people simply don't have brains for computers nor want to care of what it is running."
    author: "Iuri Fiedoruk"
  - subject: "Re: nothing new, but indeed cool"
    date: 2004-07-03
    body: "Heck, I'd guess half of the world's computer users can't even tell the two apart.  Linux won't ever take off with \"normal\" users until the OEM's are shipping it.\n\nMost people don't really care what OS they're using so long as stuff works.  In the shortterm this is bad for us; in the longterm it's great.\n\nMost of the time this is kind of like those door to door cutlery salesmen -- I mean sure, they've got nice knives and whatnot, but I don't really care beyond \"it cuts stuff; that's all I need them for -- I don't really care if you can saw through a log with it.\"  ;-)"
    author: "Scott Wheeler"
  - subject: "Re: nothing new, but indeed cool"
    date: 2004-07-01
    body: "I agree. This is even AFTER people complain to me about Windows .. \"too much spyware\" \"too many viruses\" \"trojans\" who knows what else.\n\nI popped in a Knoppix CD (from mid-May) today for a Windows guy -- within a minute or so, it detected EVERYTHING on his brand new 3.2Ghz P4 machine. Internet access was self configured, I did a quick config of the printer and he was going. Heck, I even plugged in some USB devices (scanner, etc..) and Knoppix detected I plugged them in and let me use them WITHOUT installing drivers or other non-sense.\n\nWe are not just talking a stripped down WinXP install (or rescue boot disk) .. we are talking a fully configured system with scanning, OCR, fully featured office suite, fully featured CD/DVD writing capabilities, games, multimedia apps, full internet suite (gaim, mozilla, mail, newsgroups, irc, you name it..), desktop publishing  (scribus) and who knows what else is jammed on that CD. Nothing in the Windows world even comes close to that amount of functionality accessible in less than 2 minutes. Not only that, but its FREE. Free to use, free to distribute, free to hack, free to do basically whatever with.\n\nHe still doesn't get it. Its funny, after I demo'd Knoppix and he was leaving, he started in again about Windows related problems -- issue with his iPod locking up the system, random reboots, spyware, etc..etc..etc.. *shrug*"
    author: "John Alamo"
  - subject: "Re: nothing new, but indeed cool"
    date: 2004-07-02
    body: "What John just points out is -- to me -- the reason why Linux will succeed on the desktop..."
    author: "cies"
  - subject: "Re: nothing new, but indeed cool"
    date: 2004-07-02
    body: "Getting way off topic now, but still...\n\nTo be fair, Wine or Cedega (formerly WineX) aren't really that practical as a general alternative to Windows for running games - not all games work perfectly (or at all), games tend to run slower, and it costs extra. Couple that with ATI's fairly poor 3D driver performance in Linux and it is not a good recipe for running Windows games. Personally I've subscribed to WineX/Cedega (for now), but I still reboot into Windows to play a lot of games because it's just easier. Other than that I run my KDE-based system as a full-time home desktop.\n\nI agree though, some people seem to have a strong resistance to Linux even if they see the problems with Windows and don't run games."
    author: "Paul Eggleton"
  - subject: "agree"
    date: 2004-07-03
    body: "Games are the best reason (and only for me at least) to keep Windows around.\n\nI think a lot of the resistance to Linux is because you can't buy it pre-installed at retail, and its different. And most of the non-Linux using people I know who would have the expertise to install it also have a pirated version of Windows, so there really isn't any cost benefit for Linux. Pirated Windows remains one of Linux's biggest competitors. "
    author: "Ian Monroe"
  - subject: "Re: nothing new, but indeed cool"
    date: 2004-07-02
    body: "its not only the us. the dutch government is really opposing OSS, too. the ict-guy of the liberal party stated a few days ago that the netherlands wont use OSS cuz its immature and they dont want to \"experiment\" with the tax-payers money. hmmm. And I just discovered his own website runs Red Hat Linux & apache...\n\nI did sent him a letter, I voted liberal... ;-)"
    author: "superstoned"
  - subject: "Re: nothing new, but indeed cool"
    date: 2004-07-02
    body: "Unfortunately the 'liberal' party you are referring to is not a liberal party in the sense of the word liberal. The VVD party is just plain conservative, you can hardly call them liberal. \n\nWhich is a big pity. I am a social-democrat myself but I do like a lot of liberal ideas. Unfortunately the VVD is *not*, I repeat, *not* a liberal party.\n\nIf you want to stick to a true liberal party, go to D'66. Problem there is they have an absolut asshole as a minister for Economics in The Netherlands.. This lamehead doesn't understand anything about this whole EU Software Patent Law... but he voted in favour of it despite his own party being against it... *and* the whole parliament against it... Not a very democratic minister in my opinion...\n\nAnyway, this is ehm *slightly* of topic ;). On topic: I am very happy about progress being made on OSS in Germany. Since The Netherlands always looks at Germany because we are neighbours I still have good hopes that if the whole thing succeeds in Germany, the Politicians in our country might wake up sooner or later..."
    author: "Pim"
  - subject: "Re: nothing new, but indeed cool"
    date: 2004-07-03
    body: "Actually, to continue the offtopic rant, the classical meaning of the word \"liberal\" applied to politics means a hands-off government that lets private industry pretty much do whatever it wants.  In other words, the word liberal originally meant conservative ;)\n\nOf course in those days conservative meant monarchist, so that meaning has changed as well.\n\nThere is no true \"liberal\" or \"conservative\".  These are just handy labels that have been so woefully misapplied for so long that they mean whatever people want them to mean at the time.  If you want to talk about politics in English at any real level, you need to use words that have not yet lost their usefulness."
    author: "ac"
  - subject: "Re: nothing new, but indeed cool"
    date: 2004-07-03
    body: "\"left\" and \"right\" sides of the political spectrum are usually pretty clear in the western world.  \"Liberal\" on the other hand in American politics tends to note \"left of center\" and in Europe it's more often \"rather far right\"."
    author: "Scott Wheeler"
  - subject: "Re: nothing new, but indeed cool"
    date: 2004-07-03
    body: "To me, dutch government sticks its noise in USA's ash all the time. Take for instance the decisions on the fighter planes. Also the more or less support for the iraq invasion. I can only think of short term money earning/saving reasons, but who knows what the real reasons are."
    author: "koos"
  - subject: "fantasy"
    date: 2004-07-02
    body: "yeah yeah yeah"
    author: "aNDY"
  - subject: "konqueror"
    date: 2004-07-03
    body: "OOo has better MSword-compatibility than KOffice, but there should be no good reason to use Mozilla instead of Konqueror. Konq starts faster, integrates with the desktop, and can render almost any page (and is pretty future safe since Apple helps development)."
    author: "anon"
  - subject: "Re: konqueror"
    date: 2004-07-03
    body: "I'm sorry, but Konq quite simply doesn't work with a huge lot of websites. Especially submitting data has caused a lot of grief for me. I've not had time to hunt down the bugs and report them, though.\n \n I really like the amount of work the konq team has done, but there is still a pretty long way to go for me to be able to use konq on a daily basis.\n \n Some more things:\n - No flashblocker (flashblocker exists for mozilla/firefox)\n - No adblocker (adblocker exists for mozilla/firefox)\n - Scrolling down a page with flash often 'stops' when arriving at a flash ad\n - I want a decent popup blocker.\n - Slow rendering/sluggish scrolling on flash-ridden pages.\n \n Konqueror is quite simply unusable for my daily \"newspaper browsing\" due to them being overloaded with flash/ads that swamp my computer. I need a browser that may block'em, or the pages are unbearable slow."
    author: "arcade"
  - subject: "Re: konqueror"
    date: 2004-07-03
    body: "emerge privoxy\n rc-update add privoxy default\n /etc/init.d/privoxy start\n \n change konq's proxy settings to localhost:8118 and you'll have far better ad/popup blocker than can be provided by moz."
    author: "chris"
  - subject: "Re: konqueror"
    date: 2004-07-03
    body: "What about just installing mozilla? It happens to be a better browser anyway."
    author: "Maynard"
  - subject: "Re: konqueror"
    date: 2004-07-05
    body: "> - I want a decent popup blocker.\n\nI'm not sure what your complaint is here.  Using KDE with the \"Smart\" popup blocking option is better than anything Mozilla used to have.  KDE had smart popup blocking well before Mozilla did, although FireFox probably has an extension that is just as good by now."
    author: "anon"
  - subject: "Re: konqueror"
    date: 2004-09-29
    body: "where is the \"smart\" option?\n\nthanks\nlogiq"
    author: "logiq"
  - subject: "Re: konqueror"
    date: 2004-10-22
    body: "Under Java and JavaScript"
    author: "jlar"
  - subject: "Re: konqueror"
    date: 2005-03-21
    body: "Thank you. I was also unknowledgeable of this feature by name though I really doubted if Konqueror did not have this feature somewhere."
    author: "Cristian"
  - subject: "Re: konqueror"
    date: 2004-07-05
    body: "\"there should be no good reason to use Mozilla instead of Konqueror\"\n\nOther than Mozilla being a much better browser, I guess not."
    author: "Me"
  - subject: "Re: konqueror"
    date: 2004-09-12
    body: "hey, i'm trying to login to my gmail with konq, but not having any luck, what do i need to do to login to it with konqueror? thanks"
    author: "new with suse"
  - subject: "Re: konqueror"
    date: 2004-10-21
    body: "It simply doesn't work with gmail.  Sure, there are some\ntricks with \"Change Browser Identitifcation\", but this\ndoes not correct the numerous failed javascript features.\n\nSuggestion: use firefox.  Konq is great for simple,\nHTML3.2-style pages.  But for anything complicated,\nit dies.  :(  "
    author: "criteek"
  - subject: "Re: konqueror"
    date: 2004-10-21
    body: "Konqueror's issues with GMail are fixed now."
    author: "Datschge"
  - subject: "Re: konqueror"
    date: 2005-01-02
    body: "Actually, they are not."
    author: "Apexified"
  - subject: "Re: konqueror"
    date: 2005-01-02
    body: "They <i>were</i> fixed, but gmail has managed to make new wierder ones."
    author: "Carewolf"
  - subject: "Re: konqueror"
    date: 2005-05-30
    body: "Konqueror issues with Gmail are again fixed."
    author: "Laviniu"
  - subject: "Re: konqueror"
    date: 2006-04-25
    body: "Nope, it is not...\nbut its google fault..."
    author: "a tester"
  - subject: "Re: konqueror"
    date: 2006-10-25
    body: "Who cares who's fault it is?\nFor me it is a reason *not* to use konqueror..."
    author: "Marten"
  - subject: "Re: konqueror"
    date: 2006-10-25
    body: "Well, if you care enough to complain about it then you should care enough to complain to the right people, ie. Google."
    author: "Paul Eggleton"
  - subject: "BE CAREFUL!!!!"
    date: 2004-07-05
    body: "If you want to try this distro, be extremely careful: after you partition with cfdisk, it will format your first linux partition without any further warning!"
    author: "Sergio"
  - subject: "Languages Support"
    date: 2004-07-05
    body: "Does this distro support installation/use in English too? \n\nI do not want to download and find it does only German, which sadly, I cannot read or write."
    author: "Kanwar"
  - subject: "Re: Languages Support"
    date: 2004-07-05
    body: "No, it does not. German only."
    author: "Sergio"
  - subject: "Re: BE CAREFUL read the instructions "
    date: 2004-07-05
    body: "Read carefully the GERMAN instructions, it starts at the first linux partition.  If you dont want to use it click CANCEL, it then selects the next partition in acending order. It also can encrypt swap space (very nice). I tried both the live Iso and installed the woody hard drive install!"
    author: "You did not bother to read the page before ypu clicked!!"
  - subject: "Re: BE CAREFUL read the instructions "
    date: 2004-07-05
    body: "My understanding of German is rather limited.\nAnd besides a german user told me that those instructions weren't clear at all.\nHe is a long time linux user.\nAnd anyway, what about a warning while you actually install?"
    author: "Sergio"
  - subject: "Languages Support"
    date: 2004-07-05
    body: "Does this distro support installation/use in English too?\n\nI do not want to download and find it does only German, which sadly, I cannot read or write."
    author: "Kanwar"
  - subject: "Re: Languages Support"
    date: 2004-07-05
    body: "Knoppix is easier to change you have to run locales on the hard drive install.\n The iso changes only on a reboot which is impossible on a live cd.\nThe desktop is mostly in english if you change the language settings in KDE, Mozilla, etc "
    author: "IT is in english too!!"
  - subject: "Re: Languages Support"
    date: 2004-07-05
    body: "Thanks for the info but sorry I am a novice as far as changing locales go. So how do I change the locale after booting this German Govt. Desktop? "
    author: "Kanwar"
---
During <a href="http://www.linuxtag.org/">LinuxTag 2004</a> the <a href="http://www.bsi.bund.de/">German Federal Office for Information Security</a> (BSI) and the company <a href="http://www.credativ.de/">credativ</a> unveiled <a href="http://www.bsi.bund.de/presse/pressinf/linuxtag220604.htm">the Linux Government Desktop</a>. The Linux Goverment Desktop has been developed within the scope of the project ERPOSS which evaluates Open Source Software in government environments.




<!--break-->
<p>Composed entirely of free software the distribution is available as a Live CD as well as an Install CD. While it's based on <a href="http://www.debian.org/">Debian Stable</a> (Woody) the CD contains KDE 3.2.2, <a href="http://www.mozilla.org/">Mozilla</a> and a <a href="http://kde.openoffice.org/">special themed version of OpenOffice 1.1.1</a> which integrates seamlessly with KDE.</p>

<p>One of the highlights brought by the Government Desktop is the fact that it saves the whole data on encrypted filesystems. Furthermore <a href="http://kmail.kde.org/">KMail</a> is preconfigured to send and receive encrypted e-mail (GnuPG and S/MIME) and to make use of all kinds of authority certificates. The package is completed by integrated spam and virus protection and a preconfigured personal firewall.</p>

