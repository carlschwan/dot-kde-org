---
title: "Application of the Month: KPilot"
date:    2004-12-19
authors:
  - "ateam"
slug:    application-month-kpilot
comments:
  - subject: "The AotM process"
    date: 2004-12-19
    body: "It's good to see the Application of the Month series see a continuation -- and I'm looking forward to January and a new installment. However, there's a few things that bug me about the AotM process.\n\nKPilot has been around for a long time. I'd be the last guy to insist that it is new and cool (or Hot New Stuff, for that matter). There's newer neat stuff (aKregator comes to mind, but also less-visible things like the work on Holidays), and it leaves me wondering: what are the criteria for the application-of-the-month? What does it buy me? I wish, naturally, that the app of the month gets extra attention from all the talented hackers that otherwise would work on the other PIM apps -- but it's probably not going to work out like that, eh.\n\nThe lead time on AotM is pretty long. The interview shows this in particular -- KDE 3.3 came out ages ago, in my personal experience, and lots of things have changed since then. The number of my children has doubled, for one thing, which means I'm going to putting less time into KPilot than before. Will Stephenson deserves mention in the \"advantages of the PIM community\" now, since he fixed a nasty KPilot bug.\n\nAnother thing is the whole notion of \"interview\" as implemented in AotM (and actually in the vast majority of \"interviews\" you see on the 'net): It's not an interview, it's a checklist sent out by email and filled in. There's no interaction (that'd be the \"inter\" part of the word), except for one set of questions fired off and answered. No additional questioning. No reaction along the lines of \"gosh, why the heck are you using a Dying OS?\" Now, I've heard that it's hard enough to get answers out of any developer (for instance with Tink's series of People behind KDE), but that's no excuse to do a less-than-stellar job in the cases that you do get an answer.\n\nOr maybe my answers are just so boring they don't inspire any more questions. Darn. I will have to be more controversial then, and suggest that KDE apps on OS/2 are a nail in OSS' coffin."
    author: "Adriaan de Groot"
  - subject: "Re: The AotM process"
    date: 2004-12-19
    body: "please, please. join us. write a AotM... whould be great. I'll help, if you want.\n\nand you can have your intervieuw with skype, gnomemeeting or chat... :D"
    author: "superstoned"
  - subject: "Re: The AotM process"
    date: 2004-12-19
    body: "While I appreciate the knee-jerkness of your reaction, don't you think I can better do the things I do do? There's a limited amount of time in a day, and between kpilot, ktu, kde-nl, kde-freebsd and pilot-link, which should I drop to do AotM?\n\nMy mantra is \"I never use software I'm not prepared to patch (and release the patches for)\"; the same does not apply to processes, interview schemes, etc."
    author: "Adriaan de Groot"
  - subject: "Re: The AotM process"
    date: 2004-12-19
    body: "Why the hostility?  All he is doing is asking for help and you seem to be a person with ideas who could help."
    author: "ac"
  - subject: "Re: The AotM process"
    date: 2004-12-19
    body: "Why the hostility? Well, it wasn't intended in a hostile fashion. If you're referring to \"knee-jerk\", that's a common English term. The knee-jerk reaction here is that when someone says \"it'd be better if...\" you reply \"well then help!\" Which would be OK, except that I'm already helping and haven't got even more time to help AotM as well. Jos (parent poster) would have known this if he'd read the article and connected the name of the guy being interviewed with the guy writing the comment."
    author: "Adriaan de Groot"
  - subject: "Re: The AotM process"
    date: 2004-12-20
    body: "you are right. I am at fault, I saw the name and connected it, after I posted my reaction... sorry."
    author: "superstoned"
  - subject: "Re: The AotM process"
    date: 2004-12-19
    body: "The whole confusion has to do with both people don't know each other and I know them both :) \n\nJos is one of the active contributors in KDE-NL and Adriaan de Groot is the current maintainer/hacker of KPilot and also a very very active contributor within the Dutch KDE Team.\n\n\nCiao'\n\nFab"
    author: "Fabrice"
  - subject: "Re: The AotM process"
    date: 2004-12-19
    body: "Hi Adriaan, \n\nI will reply to your questions:\n\n\"What are the criteria for the application-of-the-month? What does it buy me?\"\n\nWell the only criteria I can think of are:\n\n\na) It is a KDE-application\nb) You have never been interviewed before by AotM team [1]\nc) Your application has a certain amount of mindshare within the community\nd) Somebody is willing to write about your application\ne) You offer the AotM-editors a large sum of money [2]\n\n\nBeing mentioned in the \"Application of the Month\" series is good for your application as it will be exposed to a large group of Dot readers. As these series are much more targeted at non-developers I suspect that chances are slim that new developers will be attracted. The benefit is that you get a nicely written article targeted at new users of your application which you can use on your project page. For example the Dutch KDE team converts these articles to howtos with almost no extra effort and keeps the content up-to-date. \n\n\"The lead time on AotM is pretty long\"\n\nThis is true indeed but don't forget that your application review and the interview needs some time to get translated. This is a team of people working on it and not just one person. Timeframes can not be too short because of this. A good solution would be that we start with an application review and do the interview in the end. So a sort of 2-phase approach. \n\n\"It's not an interview, it's a checklist sent out by email and filled in.\"\n\nYou are right about that and this could be done better. Maybe we can have a live session on IRC or send out a second email. Be aware that this would take a bit more time and most of us have somewhat limited time. But yes, it would be nice to add some more interaction. So I promise you we will work on this for the next interview.\n\n\"Or maybe my answers are just so boring..\"\n\nWell not more then the usual :) \n\n\nCiao'\n\nFab\n\n\n[1] especially http//www.kde.de/appmonth has been doing these series for quite some time now\n[2] although it is meant as a joke you are always welcome to donate money to the KDE project @ http://www.kde.org/support/#Money\n"
    author: "Fabrice"
  - subject: "Only Palm devices?"
    date: 2004-12-19
    body: "Does KPilot work with other \"pilots\" or only with products from Palm?"
    author: "KDE User"
  - subject: "Re: Only Palm devices?"
    date: 2004-12-19
    body: "It only works with PalmOS products. It is so, because its backend is \"pilot-link\". More info here: http://wiki.pilot-link.org/ \n\n\"pilot-link is a suite of tools used to connect your Palm or PalmOS\u00ae compatible handheld with Unix, Linux, and any other POSIX-compatible machine. pilot-link works with all PalmOS\u00ae handhelds, including those made by Handspring, Sony, and Palm, as well as others.\" \n\nCheers!"
    author: "MandrakeUser"
  - subject: "Re: Only Palm devices?"
    date: 2004-12-19
    body: "It works with PalmOS (tm) devices, which are commonly called \"Pilot\". Vendors include PalmOne, Handspring (taken over by Palm, but they made the Visor line of products), Garmin (not officially, but I'm told their GPS thingy works just fine), TapWave and Samsung.\n\nIf you are asking about \"other handhelds\", then the answer is \"no\". KPilot does not sync with PocketPC, OPIE, Blackberry, or whatever else is out there. There are tools for that, perhaps MultiSync or KitchenSync, but I'm not into them at all."
    author: "Adriaan de Groot"
  - subject: "Include date"
    date: 2004-12-19
    body: "Could you include a date in both the interview and the article ? This is useful when linking to the artible one or two years later, and see what was the context at that time, or see when you find it if it might still be relevant or not."
    author: "Philippe Fremy"
  - subject: "Re: Include date"
    date: 2004-12-19
    body: "Good point, Philippe -- it would make clear that the comments about \"KDE 3.3.0 just released\" are because the interview is from august, and not because either the interviewer or the interviewee are clueless about the release schedule. (The interviewee <i>is</i> clueless about the 3.4 feature plan, and hasn't filled in <i>anything</i> there, so that means bugfixes only for 3.4.)\n"
    author: "Adriaan de Groot"
  - subject: "KDE App of Month - I hope not !"
    date: 2004-12-19
    body: "Sorry to rain on the parade, and don't like to criticize any volunteer effort, but of all the KDE apps we use, KPilot is the only one to totally corrupt data  and cause major headaches.  I read this article again hoping that a new version would be announced that finally works right.  I have reported my findings before but either they are ignored or new critical problems keep arising.  I keep thinking version after version that basic synching problems would go away but they only get worse.   The last attempt made was with 3.3.1 - it screwed up the field labels on my palm (they are just binary characters now) and somehow phone fields have gotten swapped.   So I've reached the break point, I cannot afford to have our contacts constantly getting screwed up so we are migrating to Thunderbird/Sunbird/etc.   It's too bad. Of the whole KDE PIM series, KOrganizer is really nice now, KAddressbook has really improved, KMail has always been solid and dependable, but all our contacts are now so messed up that we're now forced into putting all the contact data into a spreadsheet to clean up - and then will be using the CVS import and export with Thunderbird and hoping it won't be long until Thunderbird has the Linux port of it's palm synch extension working (it's already working okay on the windows side) and then getting it to work with Sunbird - I just have more confidence with that team now. I only wished we had kept a running backup on JPilot and have never figured out why JPilot works so well and KPilot works so bad and why the working JPilot code couldn't be integrated with KDEPIM etc.  Maybe others have better luck, I hope so since I don't wish on anyone else the problems we have gone thru with KPilot."
    author: "John"
  - subject: "Re: KDE App of Month - I hope not !"
    date: 2004-12-19
    body: "Hi John,\n\nSince you've not left any identifying marks on your comment post, I can't tell what problems you've reported. Searching for John in the bugs database is not very productive. Sorry it didn't work out for you.\n\nI've never been coy about the fact that KPilot is in dire need of more developers -- keeping up with the changing landscape that is PIM, as well as dealing with new devices and all that, is a big job that I just don't have the time to do very well. As a consequence, KPilot tends to work for my particular usage pattern and not necessarily for anything else. In recent months we've had vanRijn and Joern and Bille fix some bugs, but it remains in need of a careful, concerted effort at resolving its issues."
    author: "Adriaan de Groot"
  - subject: "Re: KDE App of Month - I hope not !"
    date: 2004-12-20
    body: "My experience with kpilot/pilot-link has been terrific. It is fantastic to see this sort of \"hassle-free\" software/hardware integration in Linux. I bought a Sonie Clie (two, one for me, and my wife got hers too). No major problems. Now, the pilot link is not perfect. \n\nI had some minor issues, but never a major database corruption. Something I noted is: (1) translation of categories/fields from KDE PIM to Palm is not always transparent, some times a field is not copied, sometimes I have to create a new contacts group in one side or the other to have everything go smooth. No big deal, and I really don't have time to check whether this is a bug or what, these days. (2) when trying to keep two computers' KDE PIM in sync to each other via the handheld, it is not all that good. The simplest scenario is one computer and one handheld. But then again, I think this is going to be covered by the KitchenSync or whatever the name. \n\nOverall, I consider it a huge desktop help. If/whenever pilot-link gets to a point of 100% effective sync with PalmOS, KPilot will be a killer app. So far it is a really, really good app IMHO. "
    author: "MandrakeUser"
---
A new issue of the series "Application of the Month" has been released. It covers an application from the <a href="http://pim.kde.org/">KDE PIM suite</a> called <a href="http://www.kpilot.org">KPilot</a>. Besides the application review we interviewed its current maintainer Adriaan de Groot. KPilot is a replacement for the Palm Desktop software from Palm Inc, which makes your Palm/Palm Pilot/Visor handheld capable of exchanging information with your KDE powered computer. If you want to help us creating this monthly series or you want to translate it to your own language please <a href="mailto:kde-appmonth@kde.org">contact us</a>. Enjoy App of the Month in <a href="http://www.kde.nl/apps/kpilot/">Dutch</a>, <a href="http://www.kde.org.uk/apps/kpilot/">English</a> and <a href="http://www.kde.de/appmonth/2004/kpilot/">German</a>. 



<!--break-->
