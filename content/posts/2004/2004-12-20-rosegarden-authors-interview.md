---
title: "Rosegarden Authors Interview"
date:    2004-12-20
authors:
  - "glaurent"
slug:    rosegarden-authors-interview
comments:
  - subject: "*BSD"
    date: 2004-12-19
    body: "After a quick look on the homepage... does rosegarden run under BSD, for examply FreeBSD?\n\nBesides that: looks great (not tested yet), as I'm a musician I will certainly look at it, hopefully enough time during Christmas holidays.\n\nBest wishes!\nRaphael"
    author: "raphael"
  - subject: "Re: *BSD"
    date: 2004-12-21
    body: "http://www.freshports.org/audio/rosegarden/"
    author: "Tim Middleton"
  - subject: "Re: *BSD"
    date: 2004-12-21
    body: "This is old rosegarden, not the new KDE based one. New one requires alsa, which is Linux only. In theory it can be done with arts, but support for arts is broken and unmaintained. It's not \"we don't care about other platforms\" issue, it's \"other platforms don't offer good enough audio/midi support\" issue. Hope that it will change though."
    author: "Hasso Tepper"
  - subject: "protux-like?"
    date: 2004-12-20
    body: "Sounds a lot like protux, how rosegarden compares to it (pros/cons)?"
    author: "Nobody"
  - subject: "Re: protux-like?"
    date: 2004-12-20
    body: "I had never heard of protux until today, but apparently it's mostly an audio editor. Rosegarden is a sequencer (with some audio support). "
    author: "Guillaume Laurent"
  - subject: "Re: protux-like?"
    date: 2004-12-20
    body: "co-op/cross-usage/merge/fork features? ;-)"
    author: "Nobody"
  - subject: "Re: protux-like?"
    date: 2004-12-20
    body: "We probably could collaborate... We'll try to contact them and see if they are interested."
    author: "Guillaume Laurent"
  - subject: "Re: protux-like?"
    date: 2004-12-24
    body: "That would be a great idea! A lot of musicians are looking forward to that."
    author: "MuD"
---
<a href="http://rosegardenmusic.com/">Rosegarden</a> 1.0pre1 has been released.  Musicians and tuxedoed noise-makers, please give it a try and help iron out the last few nasty bugs so we can release 1.0 and have a party.
To coincide with this is <a href="http://www.linuxdevcenter.com/pub/a/linux/2004/12/16/rosegarden.html">an interview of its authors on oreilly.com</a> by Howard Wen. We discuss the problems of sound on Linux and the choice to use KDE.  "<em>KDE provided us with a development framework that let us concentrate on Rosegarden issues, not KDE ones.</em>"




<!--break-->
