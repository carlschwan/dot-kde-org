---
title: "Planet KDE Discovered"
date:    2004-06-16
authors:
  - "clee"
slug:    planet-kde-discovered
comments:
  - subject: "Wow"
    date: 2004-06-16
    body: "Saruman tracks KDE weblogs? :-)"
    author: "AC"
  - subject: "Re: Wow"
    date: 2004-06-16
    body: "No silly, Count Dracula does. Although this time he isn't sucking blood but RSS feed. :)"
    author: "138"
---
<a href="http://planetkde.org/">Planet KDE</a> is an aggregation of public weblogs written by contributors to the K Desktop Environment. The opinions expressed in these weblogs and hence this aggregation are those of the original authors. Planet KDE is not a product or publication of KDE e.V.; as such, it does not necessarily represent the views of the KDE project as a whole or the views of KDE e.V. Planet KDE is powered by <a href="http://planetplanet.org/">Planet</a> and is run by me. Mail me with the full address of your RSS feed and a short description of what you hack on if you want your blog added to the subscription list. 
<!--break-->
