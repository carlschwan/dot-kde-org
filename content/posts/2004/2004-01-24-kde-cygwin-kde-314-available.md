---
title: "KDE on Cygwin: KDE 3.1.4 Available"
date:    2004-01-24
authors:
  - "cylab"
slug:    kde-cygwin-kde-314-available
comments:
  - subject: "Unbelievable"
    date: 2004-01-24
    body: "Subject says it all."
    author: "David"
  - subject: "Re: Unbelievable"
    date: 2004-01-24
    body: "Agreed.  Cygwin alone rocks for those of us who have to use Windows at work, and getting a desktop and familiar apps that we can use really helps.  Kudos to the dev team."
    author: "Steve"
  - subject: "Tried it, it's really easy to set up"
    date: 2004-01-24
    body: "It runs slower than in Linux (obviously), but it's really amazing to see it in action. Plus, the instalation is automatic, so every newbie out threre can set it up."
    author: "raditzman"
  - subject: "Re: Tried it, it's really easy to set up"
    date: 2004-01-24
    body: "who cares about speed.  it's already so slow on Linux anyways.  i just wish i didn't need an x-server to run it now :)"
    author: "reoeken"
  - subject: "QT version?"
    date: 2004-01-24
    body: "Hello,\n\nwhich version of QT is it using?\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: QT version?"
    date: 2004-01-24
    body: "3.2.3 using gcc3.3\n"
    author: "Ralf Habacker"
  - subject: "Re: QT version?"
    date: 2004-01-24
    body: "Probably the GPLed X11-Version and not the non-commercial QT2"
    author: "Hans-Joachim Daniels"
  - subject: "Re: QT version?"
    date: 2004-01-25
    body: "Yes, because KDE 3.x requires qt 3.x and currently there is no (l)gpled native qt3 available.\n\nThere is some work in progress to port a native qt 3. See http://kde-cygwin.sourceforge.net/qt3-win32/ for further informations. \n\nRalf\n"
    author: "Ralf Habacker"
  - subject: "Installer?"
    date: 2004-01-24
    body: "\nI fail to find the installer to set it up... Or do I have to get the\nzillions of bz2 files?\nTIA for a link"
    author: "ac"
  - subject: "Re: Installer?"
    date: 2004-01-24
    body: "see http://kde-cygwin.sourceforge.net/kde3/installation.php\n\n"
    author: "Ralf Habacker"
  - subject: "Re: Installer?"
    date: 2005-07-13
    body: "I know it's been over a year, but I've been to \"http://kde-cygwin.sourceforge.net/kde3/installation.php\", and it directs to another server, which just has the tar.bz2 files and rather non-luser-friendly instructions -- has the install-executable come and gone?"
    author: "luser?"
  - subject: "Re: Installer?"
    date: 2005-08-02
    body: "you can find install instructions here:\n\nhttp://webdev.cegit.de/snapshots/kde-cygwin/kde/kde3.4/\n\neasy to follow, though not as user-friendly as a windows install file."
    author: "david gunnells"
  - subject: "When do we get a non-Beta ?"
    date: 2004-01-25
    body: "I like this project very much, and I don't want to sound like a whiner, but all they have ever released was a Beta version. Sure, a 3.1.4 beta is cool, but I'd rather have a 2.2.2 stable ...\n"
    author: "Vlad Petric"
  - subject: "Re: When do we get a non-Beta ?"
    date: 2004-01-26
    body: "The beta-ness comes not from the KDE version, but from the porting effort to Cygwin ... it wouldn't have been any quicker to get KDE 2.2.2 working stably than KDE 3.1.4, so it makes sense to rather get the latest version working..."
    author: "David Fraser"
  - subject: "DE replacement"
    date: 2004-01-30
    body: "Is there a HOWTO describing how to replace Windows-GUI with an entire KDE?"
    author: "Micha"
  - subject: "Re: DE replacement"
    date: 2004-01-31
    body: "In http://lists.kde.org/?l=kde-cygwin&m=103072530327420&w=2 David Fraser reported how to replace explorer with kde. \n\nRalf "
    author: "Ralf Habacker"
  - subject: "Help installing"
    date: 2007-02-20
    body: "Hi! I tried the link in the previous posts and there is't any hotwo on installing KDE on cygwin, there is just the ftp link. Could anyone tell me where I can find a howto or smth to help me install KDE? Thanks."
    author: "Priest"
---
One step closer to all-platform-support. The <a href="http://kde-cygwin.sourceforge.net/">KDE on Cygwin</a> project <a href="http://sourceforge.net/forum/forum.php?forum_id=347175">announced its KDE 3.1.4 release</a> for <a href="http://xfree.cygwin.com/">Cygwin/XFree</a>. New is native sound support, windows executables are usable in Konqueror, prelimary printing support using Ghostscript and much more. Together with recent <a href="http://www.cygwin.com/">Cygwin</a> and Cygwin/XFree releases  you will enjoy font and pixmap caching speedups together with a multi-windowed display mode. Beside the basic packages this release contains additional packages like kdevelop 3.0, kdeedu, quanta, kdenetwork 
(alpha version) and kde-i18n for 45 languages. Packages for kdegames and kdesdk (umbrello) are planned next. See <a href="http://kde-cygwin.sf.net/kde3/">the KDE 3 on Cygwin</a> page for further information.



<!--break-->
