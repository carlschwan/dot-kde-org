---
title: "CeBIT: KDE Impresses at World's Largest Computer Expo"
date:    2004-04-03
authors:
  - "dmolkentin"
slug:    cebit-kde-impresses-worlds-largest-computer-expo
comments:
  - subject: "Siemens"
    date: 2004-04-02
    body: "I usually run in linux support trouble with my Siemens notebooks... "
    author: "gerd"
  - subject: "Scribus.org?"
    date: 2004-04-03
    body: "The url for scribus should be http://web2.altmuehlnet.de/fschmid/ ."
    author: "CE"
  - subject: "Re: Scribus.org?"
    date: 2004-04-03
    body: "Or http://www.scribus.org.uk/ ."
    author: "CE"
  - subject: "Re: Scribus.org?"
    date: 2004-04-03
    body: "Should be Franz Schmid, not Fanz.\n\nOur official address is www.scribus.net, which points to the .org.uk address. The old address (altmuehlnet) is still in place as a backup in case of network or server issues."
    author: "Craig Bradney"
  - subject: "64bits!"
    date: 2004-04-03
    body: "Mh, I have kde 3.2 on athlon64 too (in 64bits mode i mean, of\ncourse), but i am experiencing a lot of bugs that i do not find\nrunning kde 3.2 in 32bits mode. I am happy to know that kde people\nis using athlon64 too, so those bugs will likely disappear soon :-)\nDid anyone who is using athlong64 experienced those bugs too?"
    author: "maurizio"
  - subject: "Re: 64bits!"
    date: 2004-04-03
    body: "> Did anyone who is using athlong64 experienced those bugs too?\n\nWould be nice if you could tell what \"those bugs\" are... :-)"
    author: "ac"
  - subject: "Re: 64bits!"
    date: 2004-04-03
    body: "There's at least one problem:\nChanging ARTS' volume or adding an effect mutes the soundcard 'till you restart ARTS. It's on bugs.kde.org...\nBut I don't know of any other problems. On the other hand, a nspluginviewer that supports 84bit _and_ 32bit plugins at the same time would be great... ;-)"
    author: "Willie Sippel"
  - subject: "Re: 64bits!"
    date: 2004-04-03
    body: "I would also like to know which bugs. I run KDE 3.2.1 on a FreeBSD-amd64 and have not found any difference to the 32bit version."
    author: "arved"
  - subject: "Did the KDE developpers visit the NMM booth ?"
    date: 2004-04-03
    body: "Did the KDE developpers visit the NMM booth ?"
    author: "Phil Ozen"
  - subject: "Re: Did the KDE developpers visit the NMM booth ?"
    date: 2004-04-03
    body: "Who the hell is NMM?\n\n\nYou don't seem to have any idea just *how* large CeBIT is. There are 27 halls for the exhibition, and I mean they are all *large* halls.  You don't have *any* chance to visit all halls and still see something at the booths..."
    author: "CeBIT Visitor"
  - subject: "Re: Did the KDE developpers visit the NMM booth ?"
    date: 2004-04-03
    body: "> Who the hell is NMM?\n\nAlt-F2 fm:nmm"
    author: "Anonymous"
  - subject: "Re: Did the KDE developpers visit the NMM booth ?"
    date: 2004-04-03
    body: "NMM stands for NETWORK-INTEGRATED MULTIMEDIA MIDDLEWARE FOR LINUX.\n\nKDE4 will use a new multimedia architecture (to replace aRts) and NMM is one of the candidate.\n\nThey were at hall 11, booth E 30.\n\nSee http://graphics.cs.uni-sb.de/NMM/\n\n"
    author: "Phil Ozen"
  - subject: "scribus"
    date: 2004-04-03
    body: "Scribus is very cool. It is. I just wish there where a project to kdeify its interface -- which sort of stands out out in the very coherent KDE desktop.\n\n\nBut aside from that, nothing to say :stabler and stabler and decidedly very powerful."
    author: "A. nonym"
  - subject: "Re: scribus"
    date: 2004-04-03
    body: "I don't think it looks that odd - it is a Qt program. You should try running Office 2000 (or even Office XP/2003) or VS.NET under Windows XP. VS.NET is weird, as you expect to develop with XP widgets but you don't. There are a tonne of applications that just do not fit in look-and-feel wise at all. Different buttons, scroll-bars, you name it.\n\nI think people overplay the 'integrated' desktop thing sometimes."
    author: "David"
  - subject: "Re: scribus"
    date: 2004-04-05
    body: "If Scribus is to be \"kdeified\", the current Qt-only interface should still remain as an option. An application that doesn't really need kdelibs shouldn't make it a dependency for people who don't use KDE. Think of AbiWord which can be built with or without Gnome, as an example. A \"--with-kde\" option during configure would be nice, and even used by default if KDE was detected. But I get the impression that Scribus is meant to be a standalone application regardless of your desktop.\n\nI have an application of my own that is Qt-only, and I've thought on this issue for several years. Since it always annoys me to have to \"suck in\" several megabytes of Gnome libraries to run a single Gnome application like Dia, why should I annoy my Gnome users by making them suck in several megabytes of KDE libraries? I also have many Windows and Mac users, and making them suck in Cygwin, Fink, and/or XFree86 is going to scare a lot of them away.\n\nI understand both sides of the issue, and I am not advocating non-integration with KDE. But I would like people to know that there's another viewpoint."
    author: "David Johnson"
  - subject: "Scribus == Wow! and Other Stuff"
    date: 2004-04-03
    body: "Goodness. Shirts, ties and suits all round!\n\nScribus is without a doubt a very, very fine piece of software, and somewhat of a missing link for desktop applications. It was nice to see it there.\n\n\"In her talk, Eva Brucherseifer discussed the possibilities of porting MFC applications to Linux, particularly using Qt.\"\n\nExcellent stuff. I absolutely guarantee you that any MS Visual C++ developer will be extremely attracted to Qt, and considering the state of MFC programming that isn't all that hard (forget .NET and Mono). The cross-platform capabilities are also very compelling. Absolutely no one involved with KDE or Qt programming should be shy about this at all. Ram it down peoples' throats!\n\n\"When she saw the stack of stickers on software patents in Europe stating the fact that even the progress bar is patented, asked for details.\"\n\nPatents are certainly a bad thing when used for the wrong purpose (and that means all the time these days). However, I'm totally convinced that many patents are so ridiculous (and not just in computing) that if companies were to really leverage them on a wide scale we would see a catastrophic collapse in patents systems everywhere. That in itself, may not be a bad thing. I don't think that big companies can see that, but it's coming.\n\nI found some of the images on Kontact at the Suse/Novell booth rather bizarre (yes it's good - we know that). What I was surprised at were the large screen presentation pictures. The Suse and Novell logos are there, and it gives a very strong impression that Kontact is the PIM suite of choice on Suse/Novell desktops. Afterall, it is the only Novell PIM suite that is fully pre-installed by default, despite much hype. Can we expect things like Groupwise support etc. for Kontact?\n\nMind you, I don't envy Suse. They're going on with their business strategy as normal (and I think they quite sensibly sought guarantees on that from Novell when they were bought) and they've walked bang into the middle of a warzone, so I think we're going to see some mixed messages for a while yet.\n\nI also see Konqui was his usual charming self :)."
    author: "David"
  - subject: "Re: Scribus == Wow! and Other Stuff"
    date: 2004-04-04
    body: "> Excellent stuff. I absolutely guarantee you that any MS Visual C++ developer will be extremely attracted to Qt\n\nRecently I gave a talk about Rapid Application Development with Linux explaining how to use KDevelop, understanding the basic Application Framework and how to integrate with Designer.\nAnd one Visual Basic developer told: \"I envy your programming environment.\" And he went on \"Because it is as easy as Visual Basic, but it doesn't hide you what is going on below like Visual Basic does.\". And I totally agree... sometimes, you need to go deeper to achieve something, with Qt/KDE libs and tools it is possible to go deeper, but you don't need to go deeper that is a huge advantage when programming.\n"
    author: "Pupeno"
  - subject: "Re: Scribus == Wow! and Other Stuff"
    date: 2004-04-05
    body: "I totally agree. You can do 95% of your \"GUI work\" for Qt without writing a single line of code. But that remaining 5% isn't lost as it is in most other \"visual\" environments. A few lines of code and you have it.\n\nWith Qt's excellent layout widgets, you get the choice of using a complete visual GUI editor, writing the GUI by hand, or a combination of the two. Under Windows only masochists write GUI callbacks and resources by hand :-)"
    author: "David Johnson"
---
The <a href="http://www.kde.de/">KDE Germany Team</a> was exhibiting the KDE team's achievements at the world's largest computer
trade show <a href="http://www.cebit.de/homepage_e">CeBIT</a> in Hannover, Germany from 18.03 - 24.03.
Hosted at the booth of <a href="http://www.linup-front.de/">Linup-Front</a>, the team presented KDE 3.2 as well as a recent developer snapshot of <a href="http://pim.kde.org/">KDE PIM</a> on an Athlon64 system. Most people asked to see the <a href="http://kde.openoffice.org/">KDE/Qt integration for OpenOffice.org</a> and GTK as well as
<a href="http://www.kontact.org/">Kontact</a>. They were impressed to learn how
fast KDE is implementing the promise to become the "Integrative Desktop Environment" that lets you use a wide variety of popular
applications while still maintaining consistency in look and -to some extent- even feel.
Another focus of interest was KIOSK, which is now even easier to handle thanks
to <a href="http://extragear.kde.org/apps/kiosktool.php">Kiosk Tool</a>. Read on for the full story about KDE at CeBIT 2004.

<!--break-->
<p>
Some developers <a href="http://ktown.kde.org/~danimo/fairs/cebit2004/rainer/.tmp/img_0313.jpg.html">
used the opportunity</a> to fix bugs as they were reported or at least assisted
and motivated people to file bugs reports or to search for already existing ones and comment and vote
for them. Fundraising was also done. Gifts for donations included KDE Pins, Stickers, T-Shirts and
"KDE Development" books. A lot of people asked for stuffed Konqis which were unfortunately not available.
</p>
<p>
Among the visitors, even politicians stepped by to see KDE. J&uuml;rgen Chrobog, a State Secretary
for the German Department for Foreign Affairs was fascinated by KDE's language support. Minister
of Justice Brigitte Zypris got
<a href="http://ktown.kde.org/~danimo/fairs/cebit2004/rainer/.tmp/img_0301.jpg.html">an introduction
</a> to KDE 3.2. When she saw the stack
of stickers on software patents in Europe stating the fact that even the progress bar is patented,
asked for details. Thus, the KDE Team got the unique chance to explain the danger for FOSS
Software as well as small and average companies that emanates from software patents.
</p>
<p>
<a href="http://www.linux-events.de/">LinuxPark</a> was an excellent forum to get in touch with Linux related companies and other projects
such as <a href="http://ktown.kde.org/~binner/CeBIT2004/dscf0016.jpg">Debian</a>,
the <a href="http://ktown.kde.org/~danimo/fairs/cebit2004/rainer/.tmp/img_0308.jpg.html">LPI</a> and
of course <a href="http://ktown.kde.org/~binner/CeBIT2004/dscf0014.jpg">GNOME</a>.
<a href="http://www.linux-ag.de/">
Linux Information Systems</a> was kind enough to host <a href="http://www.scribus.org.uk/">Scribus</a>
Author <a href="http://ktown.kde.org/~binner/CeBIT2004/dscf0011.jpg">Franz Schmid</a> with his incredible Qt-based desktop publishing
software. Even DTP experts agreed that Scribus is already usable for professional printing and gave valuable feedback that Franz started to implement in his rare spare time during the fair.
</p>
<p>
Klas Kalaß gave an
<a href="http://ktown.kde.org/~danimo/fairs/cebit2004/michael/.tmp/Klas.JPG.html">impressive</a>
<a href="http://ktown.kde.org/~binner/CeBIT2004/dscf0019.jpg">talk</a>
that introduced KDE with almost all its different aspects. In her talk,
Eva Brucherseifer <a href="http://www.linux-events.de/LinuxPark/cebit04/LinuxPark/cebitbilder/18/72.jpg">discussed</a> the possibilities of porting MFC applications to Linux,
particularly using Qt.
Daniel Molkentin <a href="http://www.linux-events.de/LinuxPark/cebit04/LinuxPark/cebitbilder/23/067.jpg">
showed Kontact</a> interacting with the Kolab Server and
explained the current development status of the integration with eGroupware and OpenGroupware.org.
All speeches were very well attended. Even though CeBIT is a fairly big expo with a fairground of
about 360.000 square meters, most people took the time to attend the talks that averaged 30-40
minutes each, including a Q&amp;A session.
</p>
<p>
Looking off-site the LinuxPark area, visitors found that more and more exhibitors now plan to
offically support Linux on the desktop. Over in Hall 1 at the Novell booth,
<a href="http://www.suse.com/">SUSE</a> showed Beta Versions of their upcoming
version 9.1 featuring KDE 3.2 including
<a href="http://ktown.kde.org/~binner/CeBIT2004/dscf0002.jpg">Kontact</a> and the KDE OpenOffice
Integration.
</p>
<p>
CeBIT 2004 was a successful event for KDE. Thanks go to
<a href="http://www.linup-front.de/">Linup-Front</a> for the booth, to
<a href="http://www.linuxnewmedia.de/">Linux New Media</a> for
providing exhibitor tickets, to <a href="http://www.nomachine.com/">nomachine</a> and
<a href="http://www.basyskom.de/">basysKom</a> for paying the expenses of the apartment and
<a href="http://www.fujitsu-siemens.de/">Fujitsu-Siemens Computers</a> for the demo system. See you all next year at CeBIT 2005!
<p>
</p>
Photo galleries:
<a href="http://ktown.kde.org/~danimo/fairs/cebit2004/rainer/">Rainer Endres</a> |
<a href="http://ktown.kde.org/~danimo/fairs/cebit2004/michael/">Michael Brade</a> |
<a href="http://ktown.kde.org/~binner/CeBIT2004/">Stephan Binner</a>
</p>




