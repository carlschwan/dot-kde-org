---
title: "KDE CVS-Digest for December 17, 2004"
date:    2004-12-18
authors:
  - "dkite"
slug:    kde-cvs-digest-december-17-2004
comments:
  - subject: "ooo svg icons"
    date: 2004-12-18
    body: "I've just checked out and installed kdeplayground-artwork/oooicons, how I make openoffice use them now? :)\n\nthx in advance"
    author: "Pat"
  - subject: "one thing that is missing..."
    date: 2004-12-18
    body: "KDE's future is going to \"change\" next tuesday:\nhttp://www.ffii.org/"
    author: "ac"
  - subject: "The game isn't over yet"
    date: 2004-12-18
    body: "It's dangerous, yes, but there are still hopes. The European Parliament can restart the process from the beggining if they want, or if they make a second review they can reject the proposal. Many national parliaments have spoken out against this directive, so there are chances that the EP may reject the directive or make ammendments to it. Some people also say the common position taken by the EU Council would be illegal due to an unqualified majority and the European Court of Justice might annule it. I don't know if that is factible, but at least we still have the EP to fight against the directive."
    author: "V\u00edctor Fern\u00e1ndez"
  - subject: "Re: one thing that is missing..."
    date: 2004-12-18
    body: "Not really. \n\nI was depressed on thursday, I was excited on friday and there are still a few days to come. Yesterday the fishery vote was moved and the Dutch parliament will have its say on monday. It is likely that it will be adopted in Council but as we learnt there is always a possible solution to a problem in code, and law is code as well.\n\nWe can still put pressure on the national governments and it happens to a large degree.\n\nFor latest news you can make use of the rss:\nhttp://www.ffii.org/news/rss/\n\nSee also\nhttp://kwiki.ffii.org/Cons041217aEn"
    author: "Andre"
  - subject: "Firefox on KDE"
    date: 2004-12-18
    body: "KMozilla reminds me .. is it by any chance possible to switch to Windows HIG\n(ok/cancel button order) once running Firefox in KDE? Difference is really\nannoying. It has to be some setting as it's just fine once running in Win32.\n"
    author: "jmk"
  - subject: "Re: Firefox on KDE"
    date: 2004-12-18
    body: "Yeap, some themes allow you to do that. The plastik themes by V\u00edctor Fern\u00e1ndez (djworld) have that option. \n\nPlastik for Firefox: http://www.kde-look.org/content/show.php?content=11442\nPlastikfox Nuvola: http://www.kde-look.org/content/show.php?content=15195"
    author: "John Firefox Freak"
  - subject: "Re: Firefox on KDE"
    date: 2004-12-19
    body: "\nThis is already possible in stock mozilla and firefox. google it. you dont need kde or kde themes for this"
    author: "anon"
---
In this <a href="http://cvs-digest.org/index.php?issue=dec172004">week's KDE CVS-Digest</a> (<a href="http://cvs-digest.org/index.php?newissue=dec172004">experimental layout</a>):

<a href="http://www.kdevelop.org/">KDevelop</a> implements profile and language plugins.
<a href="http://digikam.sourceforge.net/">Digikam</a> adds a ratio-crop tool.
KPDF now does full screen mode.
New media KIO slave backend for CD insertion detection.
KMozilla part committed to kdenonbeta.
<a href="http://www.konversation.org/">Konversation</a> adds dynamic theme switching.
<!--break-->
