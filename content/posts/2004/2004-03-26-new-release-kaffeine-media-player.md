---
title: "New Release of Kaffeine Media Player"
date:    2004-03-26
authors:
  - "ggousios"
slug:    new-release-kaffeine-media-player
comments:
  - subject: "I love Kaffeine!"
    date: 2004-03-25
    body: "I've used Kaffeine for nearly six months now. It is a wonderful app, and I think it is probably the first real competition to Windows Media Player that we have on Linux now. It's not that other players don't have similiar features, but Kaffeine has got the interface right. Similar situation with k3b being the first real serious contender to Nero. \n\nGreat stuff!"
    author: "fault"
  - subject: "Re: I love Kaffeine!"
    date: 2004-03-26
    body: "Nero makes worse usability faults... You have a close look on it."
    author: "Bert"
  - subject: "Re: I love Kaffeine!"
    date: 2004-03-27
    body: "Which usability faults are in Nero. Maybe I am used to it, but no other cd burning app manages to come close."
    author: "Maynard"
  - subject: "Re: I love Kaffeine!"
    date: 2004-03-27
    body: "Nero's usability faults are NOTHING compared to KDE's usability faults. You don't even need a close look on it."
    author: "Nero-Friend"
  - subject: "why not move it to kde's cvs"
    date: 2004-03-25
    body: "To kdeextragear? It's a mature app but could probably benefit from added translations and contributors that being in kde-cvs would help in. As with all extra-gear apps, there would be independent releases of course. It has helped many other larger kde apps, like kmplayer, k3b, etc.. "
    author: "anon"
  - subject: "Way to go!"
    date: 2004-03-26
    body: "Kaffeine is definitely my favourite media player, it's also the most versatile one. The only major complaint I have is that it doesn't have cool visual plugins like TOtem, but anyway Totem is too weak in other areas so I use Kaffeine."
    author: "Alex"
  - subject: "Re: Way to go!"
    date: 2004-03-26
    body: "I don't see much differences between Totem and Kaffeine.\nThe only thing I know is Totem tells the user what codec is missing if it tries to play a file with unsupported codec. That's sometimes good to know.\nAnother thing is Totem doesn't crash when it is finished. But that may be a Kaffeine installation failure on my system.\n"
    author: "Why"
  - subject: "Re: Way to go!"
    date: 2004-03-26
    body: "I have the same problem here. Kaffeine always crashes when it's finished."
    author: "ViRgiLiO"
  - subject: "Re: Way to go!"
    date: 2004-05-03
    body: "The new 0.4.3 version doesn't have this problem."
    author: "Jose M\u00aa Adrover"
  - subject: "Re: Way to go!"
    date: 2004-03-26
    body: "Personally I like totem  a little better.  It provides me with a simple interface with a good playlist control.  In kaffeine I find the interface to be a little too cluttered, and the playlist could use a random play option.\n\nFor me between kaffeine and juk all of my media needs are met.  In the future I would like to to see these apps take over the default role for media on the kde desktop.  Noatun does too much and isn't very usable as a result, and kaboodle would be redundent if kaffeine is included.  I think kaffeine for video and lightweight audio, and juk for serious audio would provide new kde users with a much better multimedia experience.\n\n"
    author: "theorz"
  - subject: "Re: Way to go!"
    date: 2004-03-26
    body: "Just a thought, a random play option is a must, if I am to use any media player..."
    author: "Bojan"
  - subject: "Re: Way to go!"
    date: 2004-03-26
    body: "And of course a random play option: Play -> Random Play"
    author: "J\u00fcrgen"
  - subject: "Re: Way to go!"
    date: 2004-03-26
    body: "Kaffeine uses the same visual plugins than Totem: View -> Audio Visualization"
    author: "J\u00fcrgen"
  - subject: "need more kaffeine!"
    date: 2004-03-26
    body: "I like kaffeine a lot, kudos to the team!\n\nI just gave a presentation using KPresenter, and I had use an external player to play some animations.  What would it take to get kaffeine to embed its KPart into a KPresenter slide?  That would be really cool.\n\nAnyways, thanks a lot!\n"
    author: "LMCBoy"
  - subject: "Re: need more kaffeine!"
    date: 2004-03-26
    body: "I'd really like to see this question answered!\n\nAnd not just that, but this damn site clears the comment box when you forget to enter an author. Bad bad bad."
    author: "Remenic"
  - subject: "Re: need more kaffeine!"
    date: 2004-03-26
    body: "I did an attempt once for kmplayer, making a koffice part for it. Turned out that not active parts in koffice documents don't have a own window, but draw in the parent document on repaint events (not very usable for video).\nI talked about this with Lucas for kpresenter at N7Y and he said there where plans to support the KMediaPlayer::Player interface for video (like the open-file dialog does). That means that all players supporting this interface (I know of kaboodle and kmplayer) will work in kpresenter.\nDon't know the current status though..."
    author: "koos"
  - subject: "KMPlayer"
    date: 2004-03-26
    body: "This sounds extremely promising, excellent.\n\nI thought I'd mention that KMPlayer ( http://www.xs4all.nl/~jjvrieze/kmplayer.html ) is also an excellent frontend to mplayer and xine, with file/dvd/tvcard interface, does recording with ffmpeg, it's capable of being embedded into konqi too, and it's been around for absolutely ages and is therefore solid."
    author: "Dan"
  - subject: "Re: KMPlayer"
    date: 2004-03-26
    body: "I have tried both Kaffeine and Kmplayer.\nAnd I find Kmplayer/mplayer much better than\nKaffeine/xine both in performance and stability.\nIn fact, mplayer even works brilliantly without \na gui(the way I use it most of the time); all \ncontrolls can be done using keyboard much faster.\nWhen it comes to 'click and play or preview'\nkmplayer works fine, better than kaffeine(not \ntried the latest). mplayer is my companion \nin multimedia under linux"
    author: "Asokan K"
  - subject: "Re: KMPlayer"
    date: 2004-03-26
    body: "I only tried KMPlayer, and I'm loving it. Lemme ask you, does kaffeine also give you the option to use mplayer OR xine as he backend ? This is a great (the killer IMHO) feature in KMPlayer ..."
    author: "MandrakeUser"
  - subject: "Re: KMPlayer"
    date: 2004-03-27
    body: "No. Kaffeine is only a xine front-end. No mplayer support."
    author: "Asokan "
  - subject: "Kaffine interface ..."
    date: 2004-03-26
    body: "I like Kaffine but it's starting to look like the GIMP interface.  I dislike the fact that when running GIMP there are a bunch of GIMP windows on my task bar.  I get confuse and clutter my taskbak.  \n\nIf you look closely, Kaffine launch in one windows and another windows is the playlist.  The taskbar showed two windows.  I prefer that it only shows one window.  "
    author: "Old guy"
  - subject: "Re: Kaffine interface ..."
    date: 2004-03-26
    body: "that would acctually be pretty sweet.  \nThink of how apollon does it with an iconview.\n\nI'm anti-gimp, but it'd be a pretty cool competitor to wmp.\n\n//standsolid//"
    author: "standsolid"
  - subject: "mplayer vs xine"
    date: 2004-03-26
    body: "mplayer is far superior to xine.. that's why kaffeine will never replace any mediaplayer. If you want an KDE interface for your mediaplayer, try KMPlayer. It needs still some more development, but so does kaffeine.\nTry mplayer with --enable-gui. And see what ye like best."
    author: "beware_of_the_donbot"
  - subject: "Re: mplayer vs xine"
    date: 2004-03-26
    body: "Well thanks for that constructive and oh so helpful criticism, it will do wonders in helping making xine better."
    author: "James Stembridge"
  - subject: "Re: mplayer vs xine"
    date: 2004-03-26
    body: "That would be very nice of you :) Go ahead. I ll give xine a chance every now and then and see where it is getting. I'd like to see it grow."
    author: "beware_of_the_donbot"
  - subject: "Re: mplayer vs xine"
    date: 2004-03-26
    body: ">That would be very nice of you :) Go ahead.\n\nWell I'm happy to say I already do."
    author: "James Stembridge"
  - subject: "Re: mplayer vs xine"
    date: 2004-03-26
    body: ">mplayer is far superior to xine\n\nBased on what? Awaiting your arguments...."
    author: "Giorgos Gousios"
  - subject: "Re: mplayer vs xine"
    date: 2004-03-26
    body: "argument 1: stability.\nThere is one situation that produces something like a of mplayer: the file is corrupted and mplayer just says the sorry-dude-that-wont-work phrase. There are numoures crashes of xine.\nargument 2: file format support.\nGet me on that, but mplayer reads almost everything that's out there, maybe it does even more. \nlook & feel: if you like, and honestly, sometimes I do, mplayer runs without gui. Just the quick & slick player that does what it does best. Play.\nCheck mplayer's homepage.\nIt does it best. Award winning. Since ages. However, don't get me wrong.\nI appreciate the work of xine and of kaffeine-developers. There's no bad thing about competition.\nAnd if you read my first comment closely, it says: try and see what you like best. There might be ppl around that prefer kaffeine over everything else. Why not. If it fits their needs, that's fine. [ And if you read my first comment even more closely.. I left out an 'here'. guess where it fits :) ]"
    author: "beware_of_the_donbot"
  - subject: "Re: mplayer vs xine"
    date: 2004-03-26
    body: ">argument 1: stability\n>There is one situation that produces something like a of mplayer: the file is\n>corrupted and mplayer just says the sorry-dude-that-wont-work phrase. There \n>are numoures crashes of xine.\n\nCare to provide any examples of such files?\n\n>argument 2: file format support.\n>Get me on that, but mplayer reads almost everything that's out there, maybe it\n>does even more.\n\nAgain, examples?\n\n>look & feel: if you like, and honestly, sometimes I do, mplayer runs without\n>gui. Just the quick & slick player that does what it does best.\n\nThere is at least one command line front-end to xine-lib.\n\n>Check mplayer's homepage.\n\nWell they're hardly going to say \"use xine, it's better\" now are they ;)\n\n\n\n"
    author: "James Stembridge"
  - subject: "Re: mplayer vs xine"
    date: 2004-03-26
    body: "mplayer and xine all steal (borrow) each others code\nthere isn't really that much difference.\n\nthe pro about xine is that the libs and the player are seperated.\nbut this is also planned for mplayer G2.\ncurrently, mplayer is mostly nice if you use the console.\nbut since I also do so, it is also a nice player for me.\nxine however is far ahead on the gui front."
    author: "Mark Hannessen"
  - subject: "Re: mplayer vs xine"
    date: 2004-03-26
    body: ">Care to provide any examples of such files?\n\nsorry for being so short bound, mplayer crashes only on corrupted files, xine even crashes on normal files. The time xine does it again, i ll provide you with a bugreport. \n \n \n >>Check mplayer's homepage.\n \n>Well they're hardly going to say \"use xine, it's better\" now are they ;)\n\nthey probably wouldnt. the thingie here is not 'let you convince mplayer rocks because mplayer-devel-crew says so\", it is \"check-out-the-news-section-and-compare-it-to-xine-news\"\nthe short summary: mplayer wins awards, xine keeps on dropping releases.\n\nWould you mind telling what makes xine your player of choice?"
    author: "beware_of_the_donbot"
  - subject: "Re: mplayer vs xine"
    date: 2004-03-26
    body: ">Would you mind telling what makes xine your player of choice?\n\nFor my day to day playback needs either would do the job fine, xine's support for dvd menus is a big plus though I think. It is also very stable these days, and I can't remember the last time I came across a file myself that didn't play perfectly. I've never noticed any great performance advantage either way, though I'm sure people can come up with benchmarks favouring either.\n\nFrom a developers point of view xine has a much cleaner, more thought out and logical structure. The other developers are friendly and patient and take the time to help users with problems without telling them to RTFM all the time, which doesn't always seem to be the case with mplayer."
    author: "James Stembridge"
  - subject: "Re: mplayer vs xine"
    date: 2004-03-27
    body: "> I can't remember the last time I came across a file myself that didn't play perfectly\n\nI can. I've been an avid MPlayer user for a while, but I do check up on Xine regularly. I installed the newest version (non-CVS, latest release canidate) and fired it up.\n\n1) On a file with a broken index, the fixing by Xine resulted in severe artifacting. The fixing by MPlayer played perfectly. However, Xine was capable of rebuilding the index on the fly while I started playback. I prefer MPlayer's method, but Xine has something going for it there.\n\n2) On a file with a bad header, Xine failed to properly check the file type and said it wasn't supported while MPlayer started playback properly (it was mpeg-4, supported by both)\n\n3) On a file which was partially downloaded and had massive missing sections (coming over torrent) Xine started playback and then died at the first hole while MPlayer could play straight-through, just skipping any missing portions. I know that's not too important, but I like that it's possible.\n\nThat being said, Xine is more dynamic, allowing me to change framerate after start-up and build a new index while playing. Also, Xine properly supports DVD menus. Both have advantages, and I prefer MPlayer by a long-shot."
    author: "jameth"
  - subject: "Re: mplayer vs xine"
    date: 2004-03-26
    body: "i have a lot of experience with both players, and xine is much more stable.\nmplayer performs better, both in cpu speed and format jungle, but playing is\nmore smooth with xine.\ni cannot use mplayer since it gets my X down very often, kmplayer and kplayer with mplayer is the same...\nAnd yes, i do compile it from source...\n\nsince kaffeine has a great interface and is embedded in KDE it's my favourite choice as media player."
    author: "katakombi"
  - subject: "Re: mplayer vs xine"
    date: 2004-03-26
    body: "Agreed. Unfortunately people use builds provided by distributions that sometimes introduce bugs and performance problems. I can document problems people are having for instance with Quanta on Fedora core 1 that don't exist in our source, fedora core 2 or elsewhere. There is also a Mandrake 10 package that, by the luck of the draw, pulled from CVS during a particularly bad day. (I think they're distributing bleeding edge packages from CVS but some users are unclear.) Prior to our becoming part of the official KDE packages several distributions included a horrid prerelease (the final release was out before the distribution releases) which I'm sure was murder on our reputation.\n\nIt is entirely unfair to rate software stability and performance on subjective experience, as strange as that seems, unless you can insure that it is the release as intende and built as intended by the developers. Unfortunately this is not always the case with packaged binaries. BTW KDE has a huge advantage by releasing so much software together distributions have to take notice and get the right build.\n\nOn my system Kaffeine and Xine produce the smoothest playback with the best interface for the most codecs. It also doesn't not crash at the end of video clips like other players seem prone to. Good job guys!"
    author: "Eric Laffoon"
  - subject: "Re: mplayer vs xine"
    date: 2004-03-26
    body: "pro mplayer: it's just much faster, and is able to use my hardware acceleration, I can use it to display video on my TV out.\n\nPro Xine: it can play DVD's (including the menu's) in a decent fasion. mplayer sucks in this respect I fear.\n\nAll in all, I think the competition is a Good Thing (TM). It makes both projects strive to be better than the other, so we'll end up with a better player in the end."
    author: "Andr\u00e9 Somers"
  - subject: "Re: mplayer vs xine"
    date: 2004-03-26
    body: ">argument 1: stability.\n>argument 2: file format support.\nhttp://ert.ntua.gr/tv.ram. Ok, try this URL with mplayer and then with xine and please write us your experience.\n\n>look&feel\nXine is a library, not a player. That's why there are so many frontends to it. If you like a console frontend, try toxine.\n\n>Check mplayer's homepage. It does it best. Award winning. Since ages.\nI 've checked. And I did n't find a single point in the page claiming the oposite...\n\n>Why not. If it fits their needs, that's fine\nYes, that's fine, but claiming without having a serious supporting argument that mplayer is better than xine is not fine."
    author: "Giorgos Gousios"
  - subject: "Re: mplayer vs xine"
    date: 2004-03-27
    body: "> Xine is a library, not a player. That's why there are so many frontends to it. \n> If you like a console frontend, try toxine.\n\nI use the command line version of mplayer to do a lot of conversion, wma to mp3, mpg2 to divx, stuff like that.  Can toxine, or any of the xine tools be used to accomplish the same tasks?"
    author: "Matthew Feinberg"
  - subject: "Re: mplayer vs xine"
    date: 2004-03-27
    body: "> http://ert.ntua.gr/tv.ram. Ok, try this URL with mplayer and then with xine and please write us your experience.\nI don't if this is 100% correct for every country, but Rob Lanphier from RealNetworks stated on bugs.gentoo.org that mplayer uses realplayer libraries, which violates their license."
    author: "Carlo"
  - subject: "Re: mplayer vs xine"
    date: 2004-03-27
    body: "MPlayer CAN use their libraries, and it is debatable whether their liscense to prevent that is legal. It's up there with most other EULAs on the asks-way-too-much scale.\n\nMPlayer plays up through RealMedia 10 fine on its own with reverse-engineered codecs."
    author: "jameth"
  - subject: "Re: mplayer vs xine"
    date: 2004-03-26
    body: "Well, one of the mplayer developers admitted once that he thinks that xine is a lot better than mplayer and that they can't keep up with xine. (I can't remember who and when, but I'm not lying about this, you can google it). Anyway, this may or may not be true, but you definately are not in the place to make such claims that are justified. I'd rather believe one of the mplayer developers than you. Anyway, in this case I think that maybe the developer was a bit modest. Mplayer and xine are both great players. (and personally I do like the mplayer \"interface\" (CLI) best)\n"
    author: "diver1302"
  - subject: "Re: mplayer vs xine"
    date: 2004-03-28
    body: "I find that mplayer plays more formats, yeah, but xine plays the formats it does *better* than mplayer does."
    author: "regeya"
  - subject: "Re: mplayer vs xine"
    date: 2005-01-21
    body: "Im just some regular linux user who has Kaffeine and Mplayer installed on my mandrake box.  My observations\n\nmplayer plays more files, better (e.g. i have several files that play poorly in kaffeine but play well in mplayer)\n\nmplayers gui (its package mplayer-gui in mandrake) is way better then kaff or xine\n\nmplayer is way faster\n\nSo, I like mplayer."
    author: "Gino"
  - subject: "no info on kde-apps.org"
    date: 2004-03-26
    body: "No update Kaffeine on kde-apps.org."
    author: "anonymous"
  - subject: "hmm"
    date: 2004-03-26
    body: "Waht about software patents etc. Do I have to compile manually and enable illegal compiler option or can I just install a rpm."
    author: "Bert"
  - subject: "Re: hmm"
    date: 2004-03-26
    body: "What are you talking about? There is nothing \"illegal\" in xine or Kaffeine. All used libs are LGPL/GPL licenced. And software patents... well, thats not the place to start another discussion about that."
    author: "J\u00fcrgen"
  - subject: "Re: hmm"
    date: 2004-03-26
    body: "But weren't there legal problems of DVD playing for instance? I guess Lindows had the only licensed dvd player. "
    author: "Bert"
  - subject: "Re: hmm"
    date: 2004-03-26
    body: "Playing DVDs is only illegal in the US AFAIK, and even that's arguable.  To be safe, US citizens can still go to Canada to watch their DVDs."
    author: "ac"
  - subject: "Re: hmm"
    date: 2004-03-26
    body: "That applies to css decrypted DVDs. xine can use libdvdcss (runtime detection)to play such DVDs, but its not included. And yes, the use of libdvdcss or similar reverse engineering methods is illegal in some countries."
    author: "J\u00fcrgen"
  - subject: "Re: hmm"
    date: 2004-03-26
    body: "Well, they have a screenshot of a pirated version of\nMatrix Revolutions. Is that illegal enough ?\n\nMartin."
    author: "Martin"
  - subject: "Re: hmm - Pirated \"Matrix Revolution\" avi"
    date: 2004-03-26
    body: "http://kaffeine.sourceforge.net/pics/subtitles2.png\nGood job kaffeine developers!\n\n\"Hi, let me show you our leet free software and and by the way, we also openly pirate movies that have not yet been released on DVD.\""
    author: "MooGooFooBar"
  - subject: "Re: hmm"
    date: 2004-03-27
    body: "Ok, you convinced me! Kaffeine is an illegal, evil program. Just for information: The screenshot was'nt made on my machine and it was'nt aimed to glorify file sharing. Anyway, i removed it, i hope you can sleep better now..."
    author: "J\u00c3\u00bcrgen"
  - subject: "Nice work"
    date: 2004-03-26
    body: "Upgraded kaff* this morning. Works like a charm with embedded quicktime in konqueror.\n\n:)\n\nthis is SuSE 9 Pro with KDE 3.2.1"
    author: "macewan"
  - subject: "?? ???? ?? ?????? ?????? ??_??_? ?? ??? ??? ??..."
    date: 2004-03-27
    body: "???? ????? ?? ??? ??, KDE ?? ??? ?? ????? ?? ?????? ?????? ??? ??? ??????, ??????, ??? ????? ?? ?? ?? ????? ?????? ?? ???? ?? ???? (???) ???? ????\n\nKDE ?? ?? universal media player ?? ???????? ??? ???? ??? ????? ???"
    author: "?????"
  - subject: "Hindi is not visible :("
    date: 2004-03-27
    body: "Just above i posted a comment in Hindi, but it is not visible in konqueror. I was able to type the text in hindi (using mangal (hindi unicode) font). and Devnagari keyboard layout."
    author: "rizwaan"
  - subject: "KDE Stinks with Media Players"
    date: 2004-03-27
    body: "It is really frustrating to see that there is not media player in KDE which can be called a media player at all! Because all those so called KDE media player can't play which they are supposed to play.\n\nNoatun is crap:\ninterface is really ugly and insanely not-useful. Only good thing is that it allows docking in tray. it can't play all media types.\n\nKaboodle is more crap (though faster than noatun)\n\nJuk is only good at playlist. no video is supported hence crap again!\n\nAktion was good but for videos only.\n\nKMplayer -  it is just a frontend to the great mplayer program, but it has a crappy interface.\n\nCan't you all not-so-good programmers (if you were all good then, a good media player would be in existence) join to create ONE GOOD media player for KDE which can play all media types as supported by mplayer and xine.\n\nCan't you just make a good looking media player like \"Windows Media Player 9\", yes Windows media player look better than all the KDE media players combined.\n\nPoor KDE - it can't have a great media player!!! :( \n\n\n\n\n"
    author: "rizwaan"
  - subject: "Re: KDE Stinks with Media Players"
    date: 2004-03-27
    body: "Just downloaded Kaffeine (slackware packages) from \nhttp://www3.linuxpackages.net/packages/Slackware-9.1/pcxz/\n\nAnd I hope Kaffeine will be the media player of KDE 3.3 :)"
    author: "rizwaan"
  - subject: "Re: KDE Stinks with Media Players"
    date: 2004-03-27
    body: "Hmmm \"Windows Media Player 9\". Just try to install it by default (no other plugins-codecs). What types of files can you play? MP3, AVI (only the MS-oriented codecs), MPEG and possibly some others not worth mentioning at all (eg WMA-WMV). No AAC, no FLAC, no DVD's, no OGG, no Real, no Quicktime, no Divx, no... A default installation of kaffeine+xine+w32codecs packages can not only play all of the above but also has features like subitle support, A/V streaming, filters and possibly more. The installation is hardly more difficult with RPMs or apt-get. Playing is at least as easy as in windows (DnD, playlists, click and play). \n\nDid you ever care to try this combination or you just read a multimedia-in-linux-is-bad column somewhere and wanted say something about it? Com' on mate, you didn't write a single word about kaffeine on a posting list supposed to comment on its release!\n\n>Can't you all not-so-good programmers (if you were all good then, a good >media player would be in existence) \nPerhaps you, oh mighty programmer, could help us mere mortals write one!"
    author: "Giorgos Gousios"
  - subject: "Re: KDE Stinks with Media Players"
    date: 2004-03-29
    body: "> KMplayer - it is just a frontend to the great mplayer program, but it has a crappy interface.\n\nDid you try KPlayer?"
    author: "kplayer-user"
  - subject: "Re: KDE Stinks with Media Players"
    date: 2005-07-11
    body: "Yes! There are no good media players to Linux, yet!\nThis is one of my frustrations, as a new Linux user.\n\nI am searching to a good player. The great problem is that people in the developers comunity are more interested in create beautiful skins, full of unuseful options, and a lot of unuseful things. They think that it is the most important, when as really, the only thing really important is that things works!\n\nI have never seen multimedia things that works good in Linux. Maybe people in this comunities have not evolued yet.\n\nIn the other hands, network, security, process, I know the Linux is good. But for now, its all I believe.\n\nAll the multimedia tecnologi for Linux is an obscure world. Maybe, by this, there are not so good games like in Windows! In the day that it occurs, Computing world will have a great revolution. Perhaps Microsoft will be making money by services, or another thing like this. I hope!\n\nThanks.\n\n"
    author: "Nephestofolis"
  - subject: "Re: KDE Stinks with Media Players"
    date: 2005-07-11
    body: "Actually mplayer is such a fine program that I know several people that use it on Windows instead of the usual player there.\n\n"
    author: "Kevin Krammer"
  - subject: "Re: KDE Stinks with Media Players"
    date: 2005-07-11
    body: "The usual player there is ironically called \"mplayer.exe\"."
    author: "KDE User"
  - subject: "Re: KDE Stinks with Media Players"
    date: 2006-04-22
    body: "I thought it's \"wmplayer.exe\"......."
    author: "duger"
  - subject: "Automatic resize!"
    date: 2004-03-27
    body: "Whoo!  I've been waiting for this one!  The file dialog improvements are nice as well.  Thank you."
    author: "Hooded One"
  - subject: "Cache size"
    date: 2004-03-28
    body: "Is is possible to enlarge the cache or buffer size of Kaffeine, so that I can watch trailers on www.apple.com/trailers without waiting for them to load every 3 seconds?"
    author: "Arjan van Leeuwen"
  - subject: "Re: Cache size"
    date: 2004-03-30
    body: "Increase video buffer size: xine Engine Parameters|video|num_buffers - set it to 1000 or higher. "
    author: "Juergen Kofler"
  - subject: "Re: Cache size"
    date: 2004-03-30
    body: "I second that request"
    author: "macewan"
  - subject: "Kaffeine Rocks"
    date: 2004-03-29
    body: "Well, I don't know what other people have been smoking but Kaffeine is very, very good. KMPlayer is good, but Kaffeine has a much better interface and I have been able to play every video file known to man. As far as I have been able to tell, and I've been using them a while, Xine and MPlayer are fairly equal. Perhaps Xine was behind on playing certain video types, but it has caught up well and it handles DVD menus much better. As a video player, Kaffeine really is a swiss army knife.\n\nNice one Giorgos!\n\nI suspect that people have been compiling it wrong, or with bad optimisations, not installing the Xine libraries properly or there are some bad things going on with their distribution."
    author: "David"
---
The <a href="http://kaffeine.sourceforge.net/">Kaffeine</a> development team, lead by Juergen Kofler, is proud to present a new release (0.4.2) of Kaffeine. Kaffeine is a <a href="http://xinehq.de/">xine</a>-based media player. What sets it apart from other similar players is the fact that it tries to be an all-around solution by exposing xine's capabilities on an easy to use interface. Kaffeine supports all types of video and audio files and also VCDs, DVDs and media URLs. Almost all supported multimedia files can be played by point-and-click. In KDE 3.2, Kaffeine can also be controlled by a remote control. It is lightweight, starts fast and features a nice playlist.









<!--break-->
<p>
For the latest release the emphasis has been put on stability rather than on the implementation of new features. Some very annoying bugs where fixed, including some problems with control panel, various crashes and some interoperability bugs with KDE 3.2. Despite the emphasis on stability there are also some new features such as a new setup dialog, better embedding in Konqueror and support for multiple external subtitle files that can be changed on the fly while playing. The new Konqueror embedding can be seen in action when visiting
<a href="http://www.apple.com/trailers">http://www.apple.com/trailers</a>,
the "coffee bean" button starts playback in Kaffeine externally.
</p>
<p>
Other new features include:<br>
<ul>
<li>all meta/length information and CDDB entries are read on loading</li>
<li>navigation in DVD menus with arrow keys and enter</li>
<li>automatic resize feature: main window will be adapted to the video frame size</li>
<li>playlist statusbar shows total entries and total play time</li>
<li>file dialogs: remembers last directory; filter for all supported media formats</li>
<li>the --device switch sets DVD device too, path will no longer be saved in the xine configuration</li>
<li>parser for ram (real-time) playlists </li>
<li>possibility to turn equalizer on/off</li>
<li>support for audiocd:// URLs (audiocd kio-slave)</li>
<li>Konqueror servicemenus</li>
<li>translations available in 16 languages</li>
</ul>
</p>
<p>
The development of Kaffeine started about a year ago, but Kaffeine was mainly left into the unknown until major distributions (mainly SUSE but also others) chose it as their default media player. Since then, progress and addoption was very fast (
<a href="http://www.kde-apps.org/index.php?xsortmode=high&page=0">
1st place in kde-apps.org multimedia apps</a>,
<a href="http://sourceforge.net/project/stats/index.php?report=months&group_id=86937">
activity monitor on sourceforge</a>). The current core development team is still <a href="http://kaffeine.sourceforge.net/contact.html">very small</a>, but patches and bug reports are coming from various users. If you want to help, please report bugs to our <a href="https://sourceforge.net/tracker/?atid=581406&group_id=86937&func=browse">
bug-tracker</a> or use our <a href="http://kaffeine.sourceforge.net/contact.html">
mailing lists</a>. If you want to compile yourself you will be interested in the <a href="https://sourceforge.net/project/showfiles.php?group_id=86937&package_id=90404&release_id=226057">
Kaffeine source code</a>.
Binary packages and ports will soon be available for all Linux distributions as well as FreeBSD. Please check <a href="http://kaffeine.sourceforge.net/download.html">our download page</a> regularly.
</p>




