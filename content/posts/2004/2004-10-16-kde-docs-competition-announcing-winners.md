---
title: "KDE Docs Competition: Announcing the Winners"
date:    2004-10-16
authors:
  - "qteam"
slug:    kde-docs-competition-announcing-winners
comments:
  - subject: "Minor problem"
    date: 2004-10-16
    body: "The examples seem to have disappeared from the online version of my text. Anyone wanting to see them can find a copy at:\nhttp://xmelegance.org/customising-window-behaviour/html/customising-window-behaviour.html\n\n"
    author: "Richard Moore"
  - subject: "\"all of which can be found here\" ?"
    date: 2004-10-16
    body: "It seems that I'm not able to find my submission...\n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: \"all of which can be found here\" ?"
    date: 2004-10-16
    body: "Hi Andras,\nSorry your entry didn't get added to those. I've just added it to the docbook source for that page, and it'll be visible as soon as the HTML is regenerated."
    author: "Philip Rodrigues"
  - subject: "Re: \"all of which can be found here\" ?"
    date: 2004-10-16
    body: "Thanks."
    author: "Andras Mantia"
  - subject: "docbook is so lame"
    date: 2004-10-16
    body: "Am I the only one who thinks having to click on *endless* links and navigate *endless* structure to read documentation is completely unnecessary?\n\nLooking at the UserGuide TNG, I see tons of links.  I click on one and what do I get?  Many more links...  Then I click a link...  More links...  A tiny piece of text and to get the rest I have to click Next, Next, Next, Next,... \n\nThis is really hard work to read."
    author: "KDE User"
  - subject: "Re: docbook is so lame"
    date: 2004-10-16
    body: "Just because the source is docbook, has nothing to do with how much text is on the page, that is simple thing to change.  Not everyone agrees with you that pouring everything into one page is a good idea, but it's easily done.\n\nIn any case, there wouldn't be a tiny amount of text to read per page, if there were more contents, which was partly the point of the competition, and one which i think succeeded admirably.  Each chapter in the competition sample doc, will in fact be either a page, or part of a larger page, when it is merged into the larger document."
    author: "Lauri"
  - subject: "Flat toolbars"
    date: 2004-10-16
    body: "While reading one of winning entries, I wanted to try to flat toolbar mode, but it seems really broken (always appears as the top toolbar, even if it has been set oriented to left, and it doesnt return to flat mode if double clicked again). \nWhy document it, if doesn't work reliably or even usefully?"
    author: "Carewolf"
  - subject: "Superb Documentation Guys!!!"
    date: 2004-10-17
    body: "Thanks guys you've all done a great job, it is very easy to understand. Thank you..."
    author: "Rizwaan"
  - subject: "Needs some work.."
    date: 2004-10-17
    body: "\"The normal way to send instant messages in KDE is to use the IM client, Kopete.\"\n\nHopefully that's going to be rephrased, I don't like the idea of a KDE manual stating that using third party applications is abnormal. :-(\n"
    author: "Rob Kaper"
  - subject: "Re: Needs some work.."
    date: 2004-10-17
    body: "Two points:\n\nFirst: Obviously KDE documentation can not cover third party applications, since we have no way to know if they are available.  That's always been the case.\n\nSecondly: Obviously, these are the unmodified, as submitted entries, since judging them in any other form would have been really dumb.  Of course there is proofreading and some copy-editing to be done."
    author: "Lauri"
  - subject: "Re: Needs some work.."
    date: 2004-10-18
    body: "I wrote that, so I suppose I should comment.\n\nI suggested the normal way of IM'ing was Kopete because:\n\nI had to say something. suggesting the KDE IM client seemed a better idea than suggesting gaim, for example. \n\nI use kopete, and so felt I could write about it. how many reviews have you seen that were written by people how had used the software for a couple of days? It isn't the same but it is similar. \n\nif you think these reasons suck, then send in a patch.\n\nPete\n\n"
    author: "Peter Nuttall"
  - subject: "Re: Needs some work.."
    date: 2004-10-18
    body: "Mentioning Kopete is just fine with me, I just think it could be rewritten to say something like: \"KDE includes a multi-protocol instant messaging application, Kopete\". Or something along those lines. It's merely the word 'normal' I don't like. :-)\n"
    author: "Rob Kaper"
  - subject: "Re: Needs some work.."
    date: 2004-10-18
    body: "It could do with a little massage, that's all.  KDE has to remain fairly app-agnostic, because the distributions do customize which applications are available, but tend not to customize the documentation.\n\n\"Normal\" has implications, as Rob pointed out, that not using Kopete is abnormal, so yes, that sentence needs a rework, but not by much.  Pointing out it is the default conveys the message you were trying to get across, without making a value judgement on people who choose not to use it, so everyone is happy.\n\n"
    author: "Lauri"
  - subject: "Re: Needs some work.."
    date: 2004-10-19
    body: "how about replacing normal with default? then it would read:\n\nThe default way to send instant messages in KDE is to use the IM client, Kopete.\n\nPete"
    author: "Peter Nuttall"
  - subject: "Switch session"
    date: 2004-10-17
    body: "I read in this documents that there is a submenu to switch session, but I do not have this (and I just updated to KDE 3.3.1 yesterday!).\nWhat the hell ?\n\nThanks."
    author: "Marc"
  - subject: "Re: Switch session"
    date: 2004-10-17
    body: "You must reserve X servers in file \"Xservers\"."
    author: "Anonymous"
  - subject: "Re: Switch session"
    date: 2004-10-17
    body: "OK, I did it.\nBut now, I have only one console session left :-( and the default session is no more Ctrl+Alt+F7 but Ctrl+Alt+F2. Where can I change this ?"
    author: "Marc"
  - subject: "Launching Applications ... with kicker"
    date: 2004-10-17
    body: "I think the \"Launching Applications\" entry should mention kicker [buttons].\n"
    author: "Anders"
---
The <a href="http://quality.kde.org/">Quality Team</a> and <a href="http://i18n.kde.org/">Docs Team</a> are pleased to announce the results of the recent <a href="http://dot.kde.org/1095589644/">Docs Competition</a>. The judges were impressed with a wide range of "very high quality" submissions, all of which can be found <a href="http://people.fruitsalad.org/lauri/competition/index.html">here</a>. In time they will all be included in the <a href="http://users.ox.ac.uk/~chri1802/kde/userguide-tng/">new User Guide</a>, and each entrant will receive full credit. Read on to find out who won!


<!--break-->
<p>The winners were as follows:

<ul>
  <li>Adriaan de Groot with an entry on configuring toolbars</li>
  <li>Nicolas Goutte who wrote about Hand Editing Configuration Files</li>
  <li>Robert Stoffers whose entry was on Launching Programs</li>
</ul></p>

<p>
The prizes, kindly donated by <a href="http://www.oreilly.com/">O'Reilly</a>, will be in the post soon. Other entrants can expect fame and the gratitude of the KDE community.</p>

<p>
The judges also highly commended Richard Moore's entry on Customizing Window Behavior as well as Deepak Sarda's entry on Audio CD Ripping in KDE. The Docs Team would like to extend their thanks to the winners, and to every other entrant, for their excellent work.
</p>

