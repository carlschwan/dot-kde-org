---
title: "Tom Chance: C.T. Leung on LTSP and KDE at Sisler High School"
date:    2004-03-24
authors:
  - "numanee"
slug:    tom-chance-ct-leung-ltsp-and-kde-sisler-high-school
comments:
  - subject: "Winterpeg"
    date: 2004-03-23
    body: "Oh wow -- there's another KDE person in Winterpeg other than myself that is involved in promotion of KDE and Unix?!\n\n*does a little dance*\n\nYou hiring by any chance? *grins maniacally*\n\nAnyway, back to coding."
    author: "Troy Unrau"
  - subject: "Re: Winterpeg"
    date: 2004-03-24
    body: "I'm here too, eh. Didn't go to Sisler though."
    author: "John Hughes"
  - subject: "Re: Winterpeg"
    date: 2004-03-24
    body: "hmmmm .. KDE in winterpeg .. who would have thought.\nI've been a-usin kde since v1 .. love it .. always have always will. \nIf you need any help Troy in promoting it, let me know.\n\n\n\n"
    author: "Michael Young"
  - subject: "Re: Winterpeg"
    date: 2004-07-01
    body: "Winterpeg sucks you geeks..Decode this 8===============>"
    author: "Steve"
  - subject: "Re: Winterpeg"
    date: 2005-03-16
    body: "hahahahaha"
    author: "sarah butskey"
  - subject: "Re: Winterpeg"
    date: 2005-03-16
    body: "hahahahaha"
    author: "sarah butskey"
  - subject: "...software to see in KDE..."
    date: 2004-03-23
    body: "> TC: Is there any software you'd like to see in KDE related to the LTSP? \n\n\n> CTL: If there were something similar to MS Access, I think it would be a \n> killer. mysql is great for undergraduates but might be too difficult for \n> beginners. I remember that Corel Linux has a network neighbourhood browser \n> pretty much the same as the one in Windows. It would be great if we could \n> have something similar in KDE. \n\n1) Kexi, when it will come. But even before there were little useful apps. Using one of the web frontends to mysql/psql would do for now, I believe.\n\n2) lisa daemon activated for netneigh browsing. Although smb:/ does the work right out of the box for connecting to the windows posts.\n\nExcellent report. Thanks to Tom Chance. And thanks to M. Leung for giving Linux and KDE visibility in Canada.\n"
    author: "Inorog"
  - subject: "Re: ...software to see in KDE..."
    date: 2004-03-24
    body: "Kexi will indeed be great, but right now Knoda is none too shabby. Have a look."
    author: "Eric Laffoon"
  - subject: "Re: ...software to see in KDE..."
    date: 2004-03-24
    body: "Or perhaps take a look at theKompany's Rekall as it's dual licensed under the GPL. http://www.rekallrevealed.org/\n\n"
    author: "Morty"
  - subject: "Re: ...software to see in KDE..."
    date: 2004-03-24
    body: "What's the situation with Kexi? I've tried it out, and it is certainly great so far, but I have been hearing a lot of things about non-standard widgets etc."
    author: "David"
  - subject: "ltsp+kde is great"
    date: 2004-03-24
    body: "I've set-up a lts3+kde3.1 on my old job, worked like a charm serving up to 3 pcs (without conting my pc that was the server, just a athlon xp, nothing big).\nI just need some money to get a network card for my old 233 and I'll place ltsp4 here serving my kde 3.2 for people who visit me :)"
    author: "Iuri Fiedoruk"
  - subject: "end()"
    date: 2004-03-25
    body: "\"Cache the return of the end() method call before doing iteration over large containers.\"\n\nSince a reasonable -O would do turn this into an inline function anyway, it seems redundant. Plus it detracts from readability."
    author: "LuckySandal"
---
In <a href="http://www.linuxjournal.com/article.php?sid=7418">a recent</a> <a href="http://www.linuxjournal.com/article.php?sid=7419">two part article</a>, <A href="http://www.linuxjournal.com/">Linux Journal</a> documented the transition of <a href="http://www.sislerhigh.org/">Sisler High School</a> in Manitoba, Canada, to an LTSP-based desktop.  In <A href="http://www.tomchance.org.uk/writing/leunginterview">this informative interview</a>, <a href="http://www.tomchance.org.uk/">Tom Chance</a>  follows-up with Mr C.T. Leung on the ups and downs of the deployment of LTSP and KDE. <i>"The cost saving is huge, in my case. [...] In my lab, I save $1000 for a copy of Windows NT/2000 server, $200 x 30 copies of Windows XP or equivalent, $1000 x 30 1GHz pc, $100 x 30 copies of Office, $100 x 30 C++. The total saving is about $45,000.  However, the greatest saving is confidence!  We are proud to be the first computer lab in our city doing all kinds of computer teaching without Windows!"</i> Should be inspiring for other institutions considering the switch to a Linux-based desktop.


<!--break-->
