---
title: "Qt 4.0 Beta 1 Released"
date:    2004-12-22
authors:
  - "binner"
slug:    qt-40-beta-1-released
comments:
  - subject: "Dublication"
    date: 2004-12-22
    body: "Why duplicates Qt so much of what is available in the STL (eg. Tulip containers such as QList, QVector, QStack, QQueue, etc.)? I don't see any reason to do this (that is not to say that there could be no reason, but I don't see the additional benefit)."
    author: "Anonymous"
  - subject: "Re: Dublication"
    date: 2004-12-22
    body: "Ever tried cross-platform development with STL? There are dozens of implementations all with their own bugs. On AIX for example, if you switch on large file support, the STL will suddenly live in std::LFS namespace instead of std. If you try to link an application that doesn't use LFS with a library that uses it, you'll get very interesting results.\nBesides, Qt containers are more modern, use implicit sharing, feature java and stl iterators and are in most cases faster and more memory optimized. The other nice thing is that Qt doesn't force you to use its containers. If you insist using STL, all Qt containers have convenience constructors so you can pass a std::vector instead of a QVector."
    author: "Harald Fernengel"
  - subject: "Re: Duplication"
    date: 2004-12-22
    body: "I've done some cross platform development with STLport, with good results.\n\nThat said, I'm not really opposed to QT providing its own containers... after all it is a complete development platform, and it makes sense that it would provide all of the basic building blocks."
    author: "Arawak"
  - subject: "Re: Dublication"
    date: 2004-12-22
    body: "\"\nEver tried cross-platform development with STL? There are dozens of implementations all with their own bugs.\n\"\nA technically superior approach would be to fix broken STL implementations instead of inventing an own set of containers. That is one point of standardization: To have a common set of components, so people do not need to invent their own. To this end, the decision of Trolltech is really a bad one. Of course I realize that just having a set of containers on every platform supported by Qt is not the only reason for \"Tulip\". I guess features like implicit sharing or other additional features over the STL are just as well a reason. That Trolltech is able to deliver performance tuned components is nice to have too. \n\nIt is sad however to have to read statements like this in Trolltechs documentation: \"The containers provide new iterators with a nicer, less error-prone syntax than STL, inspired by Java's iterators.\"\nSuch bogus FUD from them just hurts.\n"
    author: "El Pseudonymo"
  - subject: "Re: Dublication"
    date: 2004-12-22
    body: "The unix vendors created Motif to have a common set of graphical components, so people do not need to invent their own. By this logic a technically superior approach to Qt would be to fix broken Motif implementations instead of inventing another set of widgets.\n\nTulip's containers provide new iterators with a nicer, less error-prone syntax than STL, and they are inspired by Java's iterators. This is neither bogus nor FUD."
    author: "Anonymous"
  - subject: "Re: Dublication"
    date: 2004-12-22
    body: "\"\nThe unix vendors created Motif to have a common set of graphical components, so people do not need to invent their own. By this logic a technically superior approach to Qt would be to fix broken Motif implementations instead of inventing another set of widgets.\n\"\nI explicitly said that I realize that Qt needs additional features over those provided by the STL. And on such grounds I do not criticize Trolltech. I just said that the solution to non-compliant STL implementations are compliant STL implementations and not another set of containers. \n\nBy the way (this is not really on topic), the example with Motif shows the value of a *real* standard (as C++ is) versus a loose agreement of vendors (in this case to provide some common software component).\n\n\"\nTulip's containers provide new iterators with a nicer, less error-prone syntax than STL, and they are inspired by Java's iterators. This is neither bogus nor FUD.\n\"\nThe claim that these iterators are less error-prone is entirely unsubstantiated by Trolltech. So Trolltech scares people that not sticking with STL-style iterators will make their code worse. The notion that you have to use the software of one specific vendor, or you will suffer, without any evidence shown is FUD by my understanding. What do you think?\n\nDo not get me wrong though, in general I think Qt is a very good toolkit. In general, Qt \"just works\". It is just that I prefer the C++ equivalents where functionality of C++ and Qt overlaps, and I would recommend this practice to others as well, for the sake of interoperability. \n"
    author: "El Pseudonymo"
  - subject: "Re: Dublication"
    date: 2004-12-22
    body: "\"The claim that these iterators are less error-prone is entirely unsubstantiated by Trolltech. So Trolltech scares people that not sticking with STL-style iterators will make their code worse. The notion that you have to use the software of one specific vendor, or you will suffer, without any evidence shown is FUD by my understanding. What do you think?\"\n\nJava-style iterators are less error-prone in different ways. With STL-syntax it is easy to accidentally dereference the end element, and it's difficult to filter out certain elements (you need to increment manually, but only if you didn't remove an element). That kind of code simply doesn't look nice, Java solved it by having iterators between the elements rather than having pointers to elements. But what's worse is that STL iterators don't work well with implicitely shared classes, and all of Qt's container classes are implicitely shared. If you start iterating with begin() and while you are in the loop some function makes an implicitely shared copy of the container, you first might end up modifying both copies, and then your program crashes because the next call to end() detaches container, so your iterator never reaches end().\n\n"
    author: "Anonymous"
  - subject: "Re: Dublication"
    date: 2004-12-23
    body: "If you make a copy of the container, and changing the copy also does something to the original (other than decrease a refcount) then there is something very very wrong with your container.\n\nYou can quickly get in to trouble if you modify an STL container while you are iterating though it, but fortunately it's easy enough to iterate through a copy of the container (and normally this is good enough)."
    author: "Anonymouse"
  - subject: "Re: Dublication"
    date: 2004-12-23
    body: "> If you make a copy of the container, and changing the copy also does something to the original (other than decrease a refcount) then there is something very very wrong with your container\n\nSorry, you didn't understand. In the scenario describe changing the *original* changed the copy, not the other way round. There's nothing wrong with the container, but with the concept of STL iterators. \n\nLet me try to explain it differently:\n\nFirst understand implicit sharing, and then look again at the STL iterator pattern. An STL iterator is essentially a pointer into the container's data structure. They are used under the assumption that they are as lightweight as pointers and can be assigned and copied without any overhead. That's the beauty of the STL: an iterator behaves like a pointer to a pointer, semantically and performance wise. If you changed this, code using STL iterators would become horribly slow.\n\nFixing the mentioned problem is only possible if you make STL iterators aware of the container they are iterating over, and make the container keep track of the number of instances of iterators on it.  Qt could do this, but then you would want to use the STL iterators even less with it (because it would be slow).\n\nThe other alternative is to make any call that returns a STL iterator (like begin() and end()) turn off sharing for good. That's even worse.\n\nFace it, the STL is not about implicit sharing. That's why Java-style iterators are safer.\n"
    author: "Anonymous"
  - subject: "Re: Dublication"
    date: 2004-12-23
    body: "\"\nSorry, you didn't understand. In the scenario describe changing the *original* changed the copy, not the other way round. There's nothing wrong with the container, but with the concept of STL iterators. \n\"\nEither you provide containers with correct copy semantics or you do not and disallow copying, or at least document that copy is destructive. But then your iterators are not really STL compatible (you can still use some STL algorithms however). The GNU libstdc++ std::string implementation is reference counted and it does not suffer from strange copy semantics (*). \n\n\"\nFixing the mentioned problem is only possible if you make STL iterators aware of the container they are iterating over, and make the container keep track of the number of instances of iterators on it. Qt could do this, but then you would want to use the STL iterators even less with it (because it would be slow).\n\"\nThat is one of multiple possible ways. But this affects only the implementation of the iterators, not their style of usage. \n\n\"\nFace it, the STL is not about implicit sharing. ... \n\"\nVery true.\n\n\"\n... That's why Java-style iterators are safer.\n\"\nIf you make STL-style iterators sufficiently \"dumb\", of course you can easily show that Java-style ones are safer. Turn on library debug mode with newer GCC versions and you get quite safe STL iterators.\n\n\n(*) This reference counted implementation is not without problems too. For example some methods might throw which can give the guarantee to not throw in other implementations. And there are issues with multi-threading of course.\n"
    author: "El Pseudonymo"
  - subject: "Re: Dublication"
    date: 2004-12-23
    body: "\"The GNU libstdc++ std::string implementation is reference counted and it does not suffer from strange copy semantics\"\n\nYes, they use the approach that turns reference counting off the first time you call begin() or end()."
    author: "Anonymous"
  - subject: "Re: Dublication"
    date: 2004-12-23
    body: "\"\nWith STL-syntax it is easy to accidentally dereference the end element, and it's difficult to filter out certain elements (you need to increment manually, but only if you didn't remove an element). \n\"\n\nBut that can be solved with wrapper functions, you do not need another iterator style for it.\n\nAnd Java-style iterators have weak points too. For example iterators which belong only to the InputIterator category: The \"hasNext\" operation is difficult if not impossible to implement for them. Because by the time of calling hasNext, you do not even really know if extracting one more element from the input will succeed or fail. \nAnd I imagine that as soon as you have multipass algorithms, the notation that at no point in time a Java-style iterator points at a valid element, and that value retrieval and incrementation are one tied together operation, can bring confusion and thereby problems too. \n\nThat the considerably lower flexibility of these Java-style iterators can not fully support building frameworks like the STL is also detrimental to software quality in tendency.\n\nSo while Java-style iterators allowe for a nicer coding style for some purposes, I still fail to see a reason why the should be unanimously called less error prone.\n\nBut all in all, as they are optional, I guess STL- versus Java-style will not be such a big deal anyways. Lets see how things develop.\n"
    author: "El Pseudonymo"
  - subject: "Re: Dublication"
    date: 2004-12-22
    body: ">It is just that I prefer the C++ equivalents where functionality of C++ and Qt overlaps, and I would recommend\n>this practice to others as well, for the sake of interoperability. \n\nI would go the other way and recommend using the Qt version of the functionality, for the sake of portability. Since you will use the same implementation on all targets, and not having to deal with vendor and implementation specific  differences."
    author: "Morty"
  - subject: "Re: Dublication"
    date: 2004-12-23
    body: "\"\nI would go the other way and recommend using the Qt version of the functionality, for the sake of portability. Since you will use the same implementation on all targets, and not having to deal with vendor and implementation specific differences.\n\"\nJust in case you did not mean to make a joke, I can detail you why that suggestion would be nonsensical."
    author: "El Pseudonymo"
  - subject: "Re: Duplication"
    date: 2004-12-23
    body: "This was not meant as a joke, so please enlighten me why it is nonsense to use the Qt versions of functionality? Take the case of using the Qt versions instead of STL functionality, I could happily use the same code on Windows, Mac, Linux'es, Solaris, AIX and HP-UX without having to deal with different implementations of STL on the different platforms. In my book that's a big win in portability and the biggest reason one uses cross platform toolkits.  "
    author: "Morty"
  - subject: "Re: Duplication"
    date: 2004-12-23
    body: "Think what would happen if it is indeed recommended to use Qt and not the STL, and people would actually use it in masses. Then the market for Qt-API compatible libraries would be so great that competitors would step up. These competing libraries would of course have the same subtle differences from the origin as today there are with different STL implementations. So you have gained nothing, you would still have to care for different implementations. \n\n[ Besides, to only have to cope with one implementation one could also recommend to just use Stlport for example. ]"
    author: "El Pseudonymo"
  - subject: "Re: Duplication"
    date: 2004-12-23
    body: "That is irrelevant, I recommend using the Qt versions when you already uses the Qt libs not replacing STL with the Qt version when no other part of Qt are needed. Then your competing library's have to compete with the whole Qt not some small part. And this would not destroy any standards, only make Qt one. \n\nOne could do as you say and use Stlport, but you don't gain anything just create an somewhat unnecessary dependency on a 3rd party lib. There are a few reasons not to use the Qt versions of functionality, but they are mostly technical reasons. Things like performance and resource usage, or if the other solution are easier and less eror prone to write. Or if you, or the team are very experienced using the non Qt functionality. Like being able to write STL code blindfolded and knowing of all the pitfalls of the different implementations. Then it makes sense to not use Qt, all other resons are of category education, philosophy, religion or masturbation."
    author: "Morty"
  - subject: "Re: Duplication"
    date: 2004-12-23
    body: "\"\nThat is irrelevant, I recommend using the Qt versions when you already uses the Qt libs not replacing STL with the Qt version when no other part of Qt are needed.\n\"\nReally?\n\n\nThis was your original Posting (the quote is part of mine):\n-------------------------------\n>It is just that I prefer the C++ equivalents where functionality of C++ and >Qt overlaps, and I would recommend\n>this practice to others as well, for the sake of interoperability. \n \nI would go the other way and recommend using the Qt version of the functionality, for the sake of portability. Since you will use the same implementation on all targets, and not having to deal with vendor and implementation specific differences.\n------------------------------------\n\nSo, you were indeed talking about replacing STL usage by Qt usage in overlapping functionality.\n\n\"\nOne could do as you say and use Stlport, but you don't gain anything just create an somewhat unnecessary dependency on a 3rd party lib.\n\"\nYou gain exactly what you were talking about: \"... you will use the same implementation on all all targets, and not having to deal with vendor and implementation specific differences.\"\n"
    author: "El Pseudonymo"
  - subject: "Re: Duplication"
    date: 2004-12-23
    body: ">Really?\n\nYes, that's rather implicit since the whole thread are about Qt in the first place. If you are not using Qt, any functionality it provides is a totally unrelated matter. And there are no overlaps since it's not a part of the equation. It would be rather silly to recommend usage of a GUI bound library when none is needed or another one already is in use. "
    author: "Morty"
  - subject: "Re: Duplication"
    date: 2004-12-23
    body: "As much as I hate to dive into this rather heated dicussion, it has long been planned that Qt4 will be shipped as a couple of complimentary libraries, one of which \"Qt Non-Visual\" provides containers, file access primitives etc. without any GUI code or dependencies.\n\nThis means it could become an alternative to the STL for programming in C++. Personally I like that idea. I find the Qt syntax easier to learn and read, but as I've done most of my programming in Java and Delphi, I have an obvious bias."
    author: "Bryan Feeney"
  - subject: "Re: Duplication"
    date: 2004-12-24
    body: "Same here, but this would become even simpler if the QtCore would be put under LGPL.\nPlease, Trolls ! :-)\nWould this really take any paying customers away ?\n\nAlex\n"
    author: "aleXXX"
  - subject: "Re: Duplication"
    date: 2004-12-24
    body: "\"\n>Really?\n \n Yes, that's rather implicit since the whole thread are about Qt in the first place. If you are not using Qt, any functionality it provides is a totally unrelated matter.\n\"\nThen we were not talking about the same matter; I do not see myself at fault here, since I clearly stated that I was talking about overlapping functionality. You replied to that statement."
    author: "El Pseudonymo"
  - subject: "Re: Dublication"
    date: 2004-12-22
    body: "\"A technically superior approach would be to fix broken STL implementations instead of inventing an own set of containers. That is one point of standardization: To have a common set of components, so people do not need to invent their own.\"\n\nAlthough true, it's not as if Trolltech has access to the source code of the STL on every supported platform.  I have found Qt's container classes to be invaluable in the development of kfile_torrent, as many people had problems linking my STL code due to weird differences in the std namespace.\n\n\"It is sad however to have to read statements like this in Trolltechs documentation: 'The containers provide new iterators with a nicer, less error-prone syntax than STL, inspired by Java's iterators.' Such bogus FUD from them just hurts.\"\n\nIt's not FUD, and it's not bogus.  Although the STL iterator mechanism is certainly more generic, it has more opportunities for typos and other annoying programmer errors.\n\nExample:\n\nstd<vector>::iterator it;\nfor(it = vec.begin(); it != vec.end(); ++it) {\n  // use *it\n}\n\nvs.\n\nListIterator it = vec.iterator();\nwhile(it.hasNext()) {\n  // use it.next()\n}\n\nThere are more references to \"it\" in the STL method, and you have to make sure you increment the iterator yourself (in this case done in the for loop, but lots of people forget this when doing while loops), which is done automatically with Java-style iterators.  One thing that is really annoying about STL-style iterators is removing elements from the container you're iterating over.\n\nNow, this is coming from someone who is as big a believer in C++'s strengths over Java as anyone, but it's clear to me that although Java iterators are less generic (and thus harder to use when doing weird things), they're more appropriate than STL-style for 95% of the things a programmer needs to do."
    author: "Michael Pyne"
  - subject: "Re: Dublication"
    date: 2004-12-22
    body: "\"\nExample:\n \n std<vector>::iterator it;\n for(it = vec.begin(); it != vec.end(); ++it) {\n // use *it\n }\n \n vs.\n \n ListIterator it = vec.iterator();\n while(it.hasNext()) {\n // use it.next()\n }\n \n There are more references to \"it\" in the STL method, and you have to make sure you increment the iterator yourself\n\"\n\nBut this is just the special case where you are iterating over a whole container. What do you do with arbitrary ranges? These are not even supported by such Java-style iterators. \n"
    author: "El Pseudonymo"
  - subject: "Re: Dublication"
    date: 2004-12-22
    body: "> But this is just the special case where you are iterating over a whole\n> container. \n\niterating over the whole container or just a part of it, the same loops generally occur. there are some cool STL algos that do cover certain cases better than iterating in a for or while loop, but the for or while loop iteration is the common case, not a \"special case\" at all.\n\n> What do you do with arbitrary ranges? These are not even supported by such\n> Java-style iterators.\n\ni believe that was the 5% Michael was alluding to. use the right tool for the right job. we now have both sorts of iterators to utilize. hooray!\n\nbut look through KDE's code base sometime and see how often the Java-style iterators would be an improvement versus when they wouldn't. personally, i think a 95% success rate is a bit on the conservative side =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Dublication"
    date: 2004-12-22
    body: "\"\ni believe that was the 5% Michael was alluding to. use the right tool for the right job. we now have both sorts of iterators to utilize. hooray!\n \n but look through KDE's code base sometime and see how often the Java-style iterators would be an improvement versus when they wouldn't. personally, i think a 95% success rate is a bit on the conservative side =)\n\"\nAhh, okay, I guess I have to admit, having two styles as an option is better than having only one.\n\nIt is just that the claim I cited (and the availability of Qts containers) should not make people believe that abandoning the STL in general would be good thing. Just think what would happen if every John Schmoe project would invent its own style of containers and algorithms. A nightmare in my opinion."
    author: "El Pseudonymo"
  - subject: "Re: Dublication"
    date: 2004-12-22
    body: "\"\nBut this is just the special case where you are iterating over a whole container.\n\"\n\nHuh?! This is the _common_ case which is what you want 95% of the time. It's not a \"special\" case. Besides for the other 5% of the time you can use an index for example instead of an iterator.\n\nPersonally I prefer Python's way of iterating over a collection:\n\nfor item in vec: doStuff(item)\n\nIt can't get any simpler than that.\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Re: Dublication"
    date: 2004-12-22
    body: "Here comes Ruby:\nvec.each{ |item| doSomething(item) }\n\nAlex\n"
    author: "aleXXX"
  - subject: "Re: Dublication"
    date: 2004-12-22
    body: "Why this redundant declaration of \"item\"?  In a proper functional language, all you should have to do is pass doSomething to the higher-order function.  Like this:\n\nvec.each(doSomething)\n"
    author: "GentooUser"
  - subject: "Re: Dublication"
    date: 2004-12-22
    body: "It's there to facilitate aliasing, which makes for better code.\n\nToo many times I see the abstract word item, this leads away from self-documenting code."
    author: "Saem"
  - subject: "Re: Dublication"
    date: 2004-12-23
    body: "Sorry.  I don't understand your meaning at all.  Can you clarify?"
    author: "GentooUser"
  - subject: "Re: Dublication"
    date: 2004-12-23
    body: "First the statement...\n\ncollection.each{|alias_for_each_element_in_the_collection| code to be executed per element }\n\nBasically, the \"|alias_for_each_element_in_the_collection|\" is used instead of a key word because that way you could say\n\nmyMonetCollection.each{|painting| puts painting.toString + \" is in my collection.\"}\n\nThe fact that you're not using a generic default keyword and instead aliasing each item with the word painting leads to self documenting code.  Instead of getting off on \"computer science\"-ese and bothering with iterators, this is more natural, at the very least to those familiar with English or languages with remotely similar grammar."
    author: "Saem"
  - subject: "Re: Dublication"
    date: 2004-12-23
    body: "Forgive me, but this still doesn't make any sense to me.  To take your own example and to formulate in a functional way, what you would do is:\n\nmyMonetCollection.each(lambda(painting){ puts painting.toString + \" is in my collection.\"}\n\nRight?  lambda is basically declaring an anonymous function.  If it confuses you: s/lambda/function/\n\nSo, again, I don't see what the advantage of Ruby's bastardized syntax is at all.  It seems to me that Ruby proponents don't really know what the syntax is derived from (you talk about iterators and self-documenting code and all that), whereas functional language people look at Ruby and see a bastardized syntax with no explanation as to why.  \n\nFunctional languages are as much \"computer science\"-ese as anything else.  So I don't know about that high horse of yours.  :-)"
    author: "GentooUser"
  - subject: "Re: Dublication"
    date: 2004-12-23
    body: "I don't harbour the hubris that you speak of.\n\nLambda doesn't do much.  It does not tell me the purpose of the SOLUTION, it's superfluous -- it does however tell me, in gory if not \"Captain Obvious\" style detail, the means of the solution, but that's uninteresting in every case that Ruby attempts to address.  One might think it to be necessary, but languages are a tool for expression of solutions for a domain of problems, their study and methods of construction are an another matter, lambda belongs in the latter.  It does nothing when one says that it's an anonymous function.  I suggest some study of pragmatics, it should be very obvious why \"|alias|\" was used.  It's not bastardized syntax.\n\nThe world is moving towards targeted languages, in particular domain specifics are highlighted while the rest is kept to a minimum or not at all via inference.  Such generic concepts (lambda) are best kept in their domain.  Rightly so, I might add.  It makes little sense to spew \"computer science\"-ese when we're talking about an e-store or accounting software or what have you.  It makes sense to speak in the terminology of the domain.  Not to mention the fact that it is elementary to the language and thusly and user would know what it means by spending a few minutes with the tutorial, that's quite a bit less start-up cost than learning computing science from the ground up as one would require in your model.  Now I concede Ruby isn't addressing the space of meta-programming, but it does outline a rough domain which it attempts to address and does so surprisingly well.\n\nUpon consideration of your remarks about functional languages, it seems your comments betray a lack of experience with Lisp.  I thought you would know that, typically, the approach is to build DSLs or syntactic sugar and other abstractions to move away from the base language."
    author: "Saem"
  - subject: "Re: Dublication"
    date: 2004-12-23
    body: ">I don't harbour the hubris that you speak of.\n\nOh man, I hope I one day get a chance to use this line. Too cool! :)"
    author: "teatime"
  - subject: "Re: Dublication"
    date: 2004-12-23
    body: "Wow man.  Your lecture does nothing for me, but wow.  I applaud your simultaneous display and denial of hubris.\n\nI can't believe you really wrote all that.  The differences between the lambda syntax and the almost-uglier {||} syntax are in reality *so* trivial, I honestly fail to see how you can write so many words in defense of the latter. Replace \"lambda\" with \"bind\" and don't you get the same thing in appearance?\n\nAny programmer would look at that {||} and they would want to know how it worked, what it was doing, and how to do the same thing in their own code.  Eventually they would find out about \"yield\" and \"co-functions\" and then they would realize how much uglier and inelegant the whole thing is compared to the powerful concept it's based on.\n\nAnd wow, attacking me for lack of knowledge in Lisp.  Nice one.\n\nI guess you Ruby people have something in common with us Gentoo people.  We're freaks who're unable to see that we're freaks.  :-)"
    author: "GentooUser"
  - subject: "Re: Dublication"
    date: 2005-01-05
    body: "\"Ruby people\" are usually fairly congenial (if the mailing list is any indication). However, when you come up and lambast someone's pet language for having \"bastardized syntax\" and so on, what exactly do you expect to happen?\n\nDo you even know Ruby beyond the one-line snippet that was posted above?"
    author: "Dolio"
  - subject: "Re: Dublication"
    date: 2004-12-22
    body: "vec.each{ } means do something with every \"entity\" which makes up vec, the \n|item| gives a name to each of these entities, so that you can use this name to specify what to do with it\n\nAlex\n"
    author: "aleXXX"
  - subject: "Re: Dublication"
    date: 2004-12-23
    body: "My code means the exact same thing.  It's called higher-order functions. :-)\n\ndoSomething already knows what its parameter is from the declaration, and vec.each simply calls it with the required argument."
    author: "GentooUser"
  - subject: "Re: Dublication"
    date: 2004-12-22
    body: "Qt4:\n\nforeach( variable, container )\n\tstatement;"
    author: "Johan Veenstra"
  - subject: "Re: Dublication"
    date: 2004-12-23
    body: "This looks even better than Ruby, frankly."
    author: "GentooUser"
  - subject: "Re: Dublication"
    date: 2004-12-23
    body: "That's old school. List comprehensions re newschool (when adequate):\n\n[do_stuff(x) for x in vec]\n\nIt's usually faster, too. Specially if you are trying to apply an operation to a vector and want to keep the results in another."
    author: "Roberto Alsina"
  - subject: "Re: Dublication"
    date: 2004-12-23
    body: "Deleting from a STL container while iterating is not annoying at all. Just iterate from the back to the front and all is well."
    author: "-"
  - subject: "Re: Dublication"
    date: 2004-12-22
    body: "\"A technically superior approach would be to fix broken STL implementations instead of inventing an own set of containers.\"\n\nLike that is possible."
    author: "Diego"
  - subject: "Re: Dublication"
    date: 2004-12-24
    body: "Well, it is, that's what this does:\n\nhttp://www.stlport.org/\n\nSomeone mentioned it already in an earlier post.\n\nMatt"
    author: "Matt Benjamin"
  - subject: "Re: Dublication"
    date: 2004-12-23
    body: "Because the STL is shite from a commercial developer's point of view - plain and simple."
    author: "David"
  - subject: "Very cool siuff"
    date: 2004-12-22
    body: "I'm especially happy about Arthur and the new Qt Designer, Trolltech is doing great work."
    author: "Alex"
  - subject: "Font metrics"
    date: 2004-12-22
    body: "There are three possibilities for font metrics:\n\n  Screen display\n  Printer resolution (Windows emulation)\n  PostScript (the unhinted metrics in the font files)\n\nThe page for \"Scribe\" doesn't say anything about this except that WYSIWYG is Future Work.\n\nIs there any progress with this?\n\nNote that AFAIK, Qt 3.x supports only 'Screen display' font metrics which severely limits a wordprocessor based on it.  OTOH, 'Screen display' *is* needed for computer presentations.  The 'Printer resolution' font metrics are needed so that imported MS Word documents will not be reformatted.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Font metrics"
    date: 2004-12-22
    body: "I've been waiting for Qt4's Scribe because KOffice development has slowed while waiting for it (Kword works great, I use it, but it hasn't been updated in awhile and picture layout still has some bugs)."
    author: "Evan \"JabberWokky\" E."
  - subject: "Arthur"
    date: 2004-12-22
    body: "Once you build it try out the arthur example... Stunning.\nDouble buffering built in to all QWidgets, AA edges, alpha built into QColor.. It rocks.\n\nDesigner shocked me with the new GIMP-like interface. You can get used to it, but i'm not too impressed. What did impress me in designer is the visual connecting of signals and slots (screenie attatched)."
    author: "Paul"
  - subject: "Re: Arthur"
    date: 2004-12-22
    body: "Sweet...\n\nI remember seeing something like this in VisualAge from IBM years ago, and always wondered why I didn't see it in other toolkits..\n\nCan you do things like like a button to another dialog, and have it generate the code to pop open that dialog when you press the button? That would rock for prototyping..\n\n--garion"
    author: "garion"
  - subject: "Re: Arthur"
    date: 2004-12-22
    body: "I don't understand their rationale for this:\n\n>These components can either be used together in the Qt Designer application, or integrated into other systems. As a result, certain features such as the project editor and code editor have been removed from the version that is included in this release. This allows the components to be independent of each other.<\n\nWhy doesn't their own integrated version provide the functionality it did before?  They can still componentize the rest, but by their own admission they are now missing a project editor and code editor.  Anyway, I haven't used it."
    author: "GentooUser"
  - subject: "Re: Arthur"
    date: 2004-12-22
    body: "> Why doesn't their own integrated version provide the functionality it did before?\n\nPerhaps there will be one. Trolltech writes that it's only an incomplete preview of Qt Designer 4."
    author: "Anonymous"
  - subject: "Re: Designer"
    date: 2004-12-22
    body: "Dang.  I am finally getting used to using Designer to edit my code and was just starting to become productive with it :o(  There were two approaches used by Designer to code applications:\n1. Subclassing\n2. The ui.h approach (which I just recently mastered!)\n\nIt seems to me that Trolltech is eliminating #2.  Is that correct?\n\nIt was a bit of a pain to learn, and makes some things tricky.  I won't miss the code editor on Linux (where I use Kate), but I will miss it for Windows development (I don't use MS-Studio).  Also, the project editor is quite nice, I will miss it too...\n\nCraig"
    author: "Craig"
  - subject: "Re: Arthur"
    date: 2004-12-23
    body: "For KDE development KFormDesigner is coming: http://www.kde-apps.org/content/show.php?content=14796\n\n(more KDE-compatible)\n\nCurrently it's even a base for the new ReportDesigner :)\n\nhttp://iidea.pl/~js/kexi/shots/beta6/reports.jpg"
    author: "Jaroslaw Staniek (Kexi Team)"
  - subject: "Question regarding Qt4"
    date: 2004-12-22
    body: "Will Qt4 be able to use the cool stuff in the upcoming versions of x.org? Such as transparency or using 3d graphic cards to accelerate 2d performance?"
    author: "Magnus"
  - subject: "Re: Question regarding Qt4"
    date: 2004-12-22
    body: "Well, if I am interpreting this Arthur stuff correctly, eg being able to render everything to your choice of X11, PostScript, and notably OpenGL, as well as various propertiary Windows/Mac things, I'd say yes for the latter, probably the former also."
    author: "Illissius"
  - subject: "Re: Question regarding Qt4"
    date: 2004-12-22
    body: "Nice, so we can expect this in KDE 4.0 :) "
    author: "Magnus"
  - subject: "Re: Question regarding Qt4"
    date: 2004-12-22
    body: "Well, maybe KDE 4.1.\nAFAIK Trolltech said they will implement support for the new X Server things like transparence in the future, but not in Qt 4.1.\nBut that was last year or so..."
    author: "panzi"
  - subject: "Re: Question regarding Qt4"
    date: 2004-12-22
    body: "Qt (3) already supports this for font aliasing. They do there rendering stuff by using the Xrender extension. If you have a card (and driver) that supports this (only nVidia at the moment) you get your hardware acceleration.\n\nDid some tests with it, and it is impressive. I guess that Trolltech is using the Xrender extension for anti-aliasing of lines. So that should be hardware rendered also. Same goes for transparancy. \n\nMain problem at the moment lies with the X server. The Xrender extension has some problems, and these needs to be fixed first. "
    author: "Harry"
  - subject: "Re: Question regarding Qt4"
    date: 2004-12-23
    body: "Actually, the open source Radeon drivers have a RenderAccel option which will accelerate XRENDER for doing things like drawing fonts (but they don't accelerate large blends or composites [especially not from incompatible pixmap formats])."
    author: "Anonymouse"
  - subject: "warez the Windows version?"
    date: 2004-12-22
    body: "I dont get it. I have GTK2+ running on Windows right now."
    author: "Anonymous"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-22
    body: "The commercial edition is only available to paying customers."
    author: "Anonymous"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-22
    body: "Don't feed the troll =)"
    author: "Anonymous"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-22
    body: "No problem, cutie."
    author: "Anonymous"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-23
    body: "You cannot compare GTK vs QT"
    author: "JC"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-23
    body: "Could you elaborate as to why one cannot compare GTK to QT?\n\nI see the point of view that, at least personally, GTK is overly low level and is best used to build upon through bindings while QT is more of a finished product in this regard.  Are there any other reason, however?"
    author: "Saem"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-24
    body: "Qt provides a rich API for doing many things that are commonplace in anything you do.  GTK is for graphics.  QT is a whole crapton more then that."
    author: "Thomas Charron"
  - subject: "Only if you ignore that glib "
    date: 2004-12-24
    body: "GTK is only graphics, but glib provides many of the same things that QT does outside of graphics. The GTK authors just decided to move the non GUI bits outside of the GTK library itself so that authors of non graphical programs can benefit without depending on X libraries. Strangely enough Trolltech has decided to do much the same thing in QT4, though they are leaving it all in one QT \"package\" while splitting into multiple libraries. "
    author: "Greg"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-23
    body: "first, you shouldn't use proprietary software, so you shouldn't use windows. \n\nsecond, very bad GTK2+ supports windows. Trolltech promotes the use of free software, making QT available for Linux and other free environments for free (GPL, so even more free than the Lesser GPL, as GTK2+ is), and not for windows.\n\nits good to let these people, who where willing to fund Bill gates with another car, pay to make QT (also for linux) a better product. really good. if they want to use windows and proprietary software, fine, but let them pay for it. they seem to be more than willing to pay, considered they pay for windows itself, while its such crap."
    author: "superstoned"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-23
    body: "<blockquote>first, you shouldn't use proprietary software, so you shouldn't use windows.</blockquote>\nPerhaps its beyond my control. Perhaps i'm merely running Windows to port my QT application over to Windows. Perhaps i'm not a free software zealot.\n\n<blockquote>Trolltech promotes the use of free software, making QT available for Linux and other free environments for free (GPL, so even more free than the Lesser GPL, as GTK2+ is), and not for windows.</blockquote>\nTrolltech doesn't promote compatibility with that, nor do they promote the use of free software to Windows users with their non-free Windows port. Not against that? Then you're not a true free software zealot.\n\n<blockquote>second, very bad GTK2+ supports windows.</blockquote>\nIts LGPL. Anyone can port it to Windows if they wish to. That's exactly what happens. Against that? Then you're not a true free software zealot.\n\n</blockqt>"
    author: "Anonymous"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-24
    body: "you're moaning about having to pay for QT, while GTK is free - but you (or your boss) are willing to pay for windows, why not for QT? And if you dont want to pay, you are free to use free software. I don't pretend being a true free software zealot, but you are a hypocrite.\n\n"
    author: "superstoned"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-24
    body: "So what you say is that free software developers who would like to see their QT applications running on Windows native, have to PAY Trolltech? Ridiculous."
    author: "Anonymous"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-24
    body: "\"Perhaps its beyond my control.\"\n\nHow can it be beyond your control?\n\n\"Perhaps i'm merely running Windows to port my QT application over to Windows.\"\n\nSo you are willing to pay microsoft in order to port your Qt-application to Windows, but you won't pay TrollTech for their work on the Windows-version of Qt to make that port possible?\nNow, that's a hypocrit\n\n\" Perhaps i'm not a free software zealot.\"\n\nThen you won't have any problem with buying a Qt-license, and asking your Windows-audience to pay for the Windows-version of your software.."
    author: "ac"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-24
    body: "\"How can it be beyond your control?\"\n\nBecause i'm merely porting an application for users? Because my boss decides what i have to do?\n\n\"Then you won't have any problem with buying a Qt-license, and asking your Windows-audience to pay for the Windows-version of your software..\"\n\nI have. I decided to program in GTK2 for cross-platform compatibility. If i were developing my program using QT, Windows users would have to pay for the software and the source wouldn't be fully open. Hence my software wouldn't be free then, because of QT. Not because its cross-platform...\n\nBut you people are a bunch of dildos who don't care for cross-platform compliance. You only care for Linsux. Hitler also hated the Jews, and only cared for the Germans >:("
    author: "Anonymous"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-24
    body: "> If i were developing my program using QT, Windows\n> users would have to pay for the software and the\n> source wouldn't be fully open.\n\nBoth of your statements are false:\n1. Windows users don't *have* to pay for your Qt software if you don't want to charge them. You can distribute a Windows binary application for free. The programmers are the ones that needs to pay for Qt, in order to get the necessary includes and etc to compile their software. Each programmer needs a license. Users don't need to pay anything at all.\n\n2. I believe you can open source your Qt software, as long as it is not a viral license (ie. GPL)!! But if someone wants to work on it, he/she will need a Qt license. So, yes, it wouldn't make much sense to have an open source project like that, but I believe it would be possible.\n\nI am not that familiar with Trolltech's licenses, but I believe these two are valid points. Anyone more familiar, please say so."
    author: "blacksheep"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-24
    body: "I think both your points are right, except that you don't get what i thought on when i read point 1! So my reponse to that.\n\n1) If i compile static, then i think you're right. But if i compile dynamic the user still needs the QT library. Hence i have to compile static to reduce costs.\n\n2) If developers of free software have to pay to develop a Windows-compatible port then who's gonna pay for *their* costs?\n\nThe QT proponents answer to point #2: \"but its Windows, we dont want to encourage software to that\" or \"you are already paying for Windows, why not pay for QT as well\" but i wouldn't pay for it for my own benefit, it would be for my users. Besides, marketing on Windows is a nice side effect. Firefox works fine on Windows and its nice marketing for FOSS. Moreover, who thinks about QT applications on other proprietary systems? Last time i checked, proprietary Unices were significantly more expensive than Windows yet QT runs free on these. And finally, who thinks about MY APPLICATION?"
    author: "Anonymous"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-24
    body: "Re your point 1.  It always surprises me about how misinformed people are about the Qt license, bitch and moan about their ignorance everywhere, no matter how wrong they are.  I guess instead of informing yourselves of the facts you would rather \"think this\" and \"think that\" or worse let others do the \"thinking\" for you by buying into their lies and propaganda.  Sigh."
    author: "ac"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-24
    body: "->\"How can it be beyond your control?\"\n \n \"Because i'm merely porting an application for users?\"\n\nThat is something you control.\nYou decide wether you want to port something to windows or not. You can also port Qt to Windows if you want to have a GPL-version of the toolkit ..\n\n \"Because my boss decides what i have to do?\"\n\nIf you boss wants you to port or develop something for Windows, he is also paying you to do so :)\nAlso he is the one that should buy the Qt-license (and the Windows-license, etc..), not you.\n\n  \n \"I have. I decided to program in GTK2 for cross-platform compatibility.\"\nThat is nice. \nAs developer you are free to decide wich toolkits fits your needs, and if GTK does so, then use that :o)\nBut don't start bashing another toolkit if it does not fit al your requirements.\nOr would you switch to Qt if there was a GPL-version available for Windows?\n\n\n\n\" If i were developing my program using QT, Windows users would have to pay for the software and the source wouldn't be fully open. Hence my software wouldn't be free then, because of QT. Not because its cross-platform...\"\nYeps, but I thought you were not a Free Software Zealot :o)\nAlso, your Linux-program would still be free, only the Windows-port would not be.\n\n \n \"But you people are a bunch of dildos who don't care for cross-platform compliance. You only care for Linsux.\"\n\nIf Linux sux, why develop for it in the first place?\nAlso, Qt is available as GPL for a lot of platforms, including linux, several Unices, MacOS...\nIs your program already available on MacOS?\n\nAnd finally, there is nothing wrong with cross-platform compliance. Qt offers you that, perhaps even more then GTK2 does. \nBut you are not asking for cross-platform compliance, you are asking for a free (as in gratis/freedom) product on a propietary platform. \nAnd that is something TrollTech does not deliver..\n\nAnd please, leave Hitler out this discussion..\n"
    author: "ac"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-24
    body: "\"Or would you switch to Qt if there was a GPL-version available for Windows?\"\n\nProbably, yes. Because its technical superior to GTK :o)\n\n\"Also, Qt is available as GPL for a lot of platforms, including linux, several Unices, MacOS...\nIs your program already available on MacOS?\"\n\nNo, and i don't have a PPC computer to port it over either. But MacOS is also a proprietary system why is QT available for MacOS free and Windows non-free? Say i ported a QT application over to MacOS. Theoretically, not many people would run it, in relation to the number of people who'd run it on Windows. So if i aim for as much users as possible and supporting major platforms first where x86 is the easiest (because i don't have PPC or POWER or SPARC) then Windows would be #1 or #2.\n\nTrollTech seems to want to discourage free software on Windows. Funny thing is, i heard a KDE developer also saying that he didn't want free software applications running on Windows. Makes me wonder ???"
    author: "Anonymous"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-25
    body: "\"Probably, yes. Because its technical superior to GTK :o)\"\n\nAh, then you indeed have a problem ;)\n \n \"But MacOS is also a proprietary system why is QT available for MacOS free and Windows non-free?\"\nWell, MacOS is not 100% proprietary, afaik only the grafical interface is. So, there is a difference.\nDunno why TrollTech won't create a GPL-version for Windows, but is is probably because that is where most of their revenue comes from.\n \n\"TrollTech seems to want to discourage free software on Windows. Funny thing is, i heard a KDE developer also saying that he didn't want free software applications running on Windows. Makes me wonder ???\"\n\nWell, dunno about the real reason for TrollTech (see above), but I also think it is not a good idea that free software can run on Windows. Why not? To get people to use free operation systems in stead of Windows, it needs killer applications. Now, if all current or future killer applications are available for Linux/BSD/name your OSS-os are available for Windows as wel, why would anyone want to switch?"
    author: "ac"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-25
    body: "\"Well, MacOS is not 100% proprietary, afaik only the grafical interface is. So, there is a difference.\nDunno why TrollTech won't create a GPL-version for Windows, but is is probably because that is where most of their revenue comes from.\"\n\nProbably, yes. I think i understand their revenue scheme.\n\nI partly support their overal reasoning regarding non-GPL compliant licensing schemes. That is to say, in theory it works fine. If one developed a proprietary program they have to pay TrollTech for a QT license. The problem is that when one develops a program which is roughly as free / restrictive as the GPL, but it has a different name, then you still end up with a problem. Even though you morally do ~ the same as a GPL developer, you use a different license and you get a burden because of that. Thats not fair.\n\n\"To get people to use free operation systems in stead of Windows, it needs killer applications. Now, if all current or future killer applications are available for Linux/BSD/name your OSS-os are available for Windows as wel, why would anyone want to switch?\"\n\nIts not good for compatibility though and in the Real World, some people have to run Windows or run Windows and want a compatible-with-*NIX system. What if *NIX developers said: \"Well we're not gonna implement SMB/CIFS, Windows must use NFS instead\" or something similar.\n\nPS: My Windows is not 100% proprietary and the parts of MacOSX which are proprietary are the most exciting. They do the very same as Windows regarding GUI, but they don't have this restricting licensing scheme!"
    author: "Anonymous"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-28
    body: "\"The problem is that when one develops a program which is roughly as free / restrictive as the GPL, but it has a different name, then you still end up with a problem.\"\n\nYou don't if its GPL compatible - whatever the name is.\n\n\"Its not good for compatibility though and in the Real World, some people have to run Windows or run Windows and want a compatible-with-*NIX system. What if *NIX developers said: \"Well we're not gonna implement SMB/CIFS, Windows must use NFS instead\" or something similar.\"\n\nThe NFS and the SMB networking stacks are implementations of network protocols that are common to different platforms. Desktop software is just not like that, so the comparison is non-existant.\n\n\"PS: My Windows is not 100% proprietary and the parts of MacOSX which are proprietary are the most exciting. They do the very same as Windows regarding GUI, but they don't have this restricting licensing scheme!\"\n\nOnly you know what you mean by that crap, but I think you'll find that Microsoft and even Apple have some pretty restrictive licenses. You also have to pay for their software stacks, which you don't have to do (or pay very little) if you're using Linux and KDE. You simply pay for a development license if you need it, which for real-world programmers makes perfect sense."
    author: "David"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-28
    body: "\"Probably, yes. Because its technical superior to GTK :o)\"\n\nAnd why is it technically superior, oh anonymous arsehole? Because Trolltech has a business model to fund its continued development and still respect free software.\n\nLook, Qt will never be LGPLd so piss poor programmers and incredibly stupid people like yourself can develop with piss poor development tools for absolutely nothing - OK? The reason why Qt is so good is because it isn't LGPLd, and that doesn't matter to good programmers, Windows programmers or anyone other than you that it isn't - OK?\n\n\"Funny thing is, i heard a KDE developer also saying that he didn't want free software applications running on Windows. Makes me wonder ???\"\n\nAnd therein is the piss poor logic you have floating around in your head. You'll pay for Windows, Office and other Windows software you'll need but you don't want to pay for good development tools to develop good software?!"
    author: "David"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-28
    body: "\"The reason why Qt is so good is because it isn't LGPLd\"\n\nHahaha. Poor zealot, out of arguments. :D"
    author: "Anonymous"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-28
    body: "So you actually tell us Qt would have been more successful if it were LGPL'ed instead GPL'ed? Care to elaborate exactly how?"
    author: "ac"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-29
    body: "So i tell what? You're making that up. Nowhere did i state that hence i don't see the need to back that up. It would be an interesting hypothesis for me, although you haven't defined what 'success' exactly means to you."
    author: "Anonymous"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-29
    body: "Well, you apparently \"questioned\" (more like ridiculed) the argument that Qt not being LGPL'ed made it as good as it is today. Either you are taking part in the whole GPL vs. LGPL discussion just for the sake of it, or you tell us why you consider the above statement a \"zealot's argument lacking better ones\". I was only asking questions in case you noticed."
    author: "ac"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-29
    body: "<i>Either you are taking part in the whole GPL vs. LGPL discussion just for the sake of it, or you tell us why you consider the above statement a \"zealot's argument lacking better ones\"</i>\n\nThe person arguing this is the same person as the one who didn't even know the LGPL is considered Free software by the FSF. Neither did you, for the record.\n\nHis post contains several flat statements which are simply not argumented. Besides the several ad hominems, for which i don't care that much, he stated:\n\n<i>And why is it technically superior, oh anonymous arsehole? Because Trolltech has a business model to fund its continued development and still respect free software.</i>\n\nThat's not invalid reasoning. He makes 2 statements. The first one we agree on, the second one is a questionable statement but the connection between the 2 is not made. Its like saying: \"Oh my, the world is round\" and \"Einstein was a smart mathematician\" with a because between those 2. It makes no sense at all because the alleged corelation is not argumented.\n\n<i>Look, Qt will never be LGPLd so piss poor programmers and incredibly stupid people like yourself can develop with piss poor development tools for absolutely nothing - OK? The reason why Qt is so good is because it isn't LGPLd, and that doesn't matter to good programmers, Windows programmers or anyone other than you that it isn't - OK?</i>\n\nIn these statements lie various personal attacks, which show personal attachement to various morals.\n\nThe first statement shows arrogance which is unfounded because David cannot know what exactly happens in the future or the future for QT. He also hasn't shown he has somehow more power over that future than anyone else which would have been a way to gain authority over that statement.\n\nThe second statement is what you quote and its simply not proven or argumented. Its similar to what the Catholic church said about the Earth: that its flat and that the Earth evolves around the Sun while not proving it. Hence it were never scientific statements. The Catholic church, in that situation, pulled an authority argument. That is to say, they used their authority to make people believe something which they were not able to prove. It also seems to argue good programmers (whatever the definition of that may exactly be) don't care for licensing issues. The LGPL statement is similar to my Einstein example here above.\n\n<i>And therein is the piss poor logic you have floating around in your head. You'll pay for Windows, Office and other Windows software you'll need but you don't want to pay for good development tools to develop good software?!</i>\n\nAside from the first statement, the second one somehow seems to know that i've paid for or will pay for Microsoft Office. No merit and untrue. Besides that it doesn't fit in my situation at all. I've more or less explained why i develop for Windows already. Thats also the very reason why i run it.\n\nAs for the last statement, there's no definitive corelation between good development tools and good software. That statement ignores qualities such as the programmer. It also ignores the possibility that there might be good development tools available for free, which i argue is true. You see, the funny thing is, thats more or less what Linux with GCC et al exactly are considered to be and which KDevelop and Eclipse are. With GNU, Linux et al you don't have to pay loads and loads of money for a license for an OS and a compiler which you had to do with UNIX systems such as OpenServer, Solaris and IRIX. Unfortunately, i also have the opinion that `Linux' is not ready for overal desktop usage -- yet.\n\nThat's where i'm coming from and that's why i made that statement. Clear? Typing and arguing all this costs a great deal of time and doesn't raise awareness of my QT political problem at all. It only deals with someone who's drawing the discussion away from my very problem. This someone is who i define as a typical GPL zealot; which people are according to my experience too stubborn to deal with. I just hope some at least someone won't fall into his pittraps of reasoning because they lack any substance."
    author: "Anonymous"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-29
    body: "\"The person arguing this is the same person as the one who didn't even know the LGPL is considered Free software by the FSF. Neither did you, for the record.\"\n\nI thought it didn't matter what the FSF thought? GPLd software is considered free software - the LGPL is for instances where you need to link GPLd and other software together somehow. It was never intended for mass LGPLing of software like Qt so you can develop piss poor software that no one will use.\n\n\"His post contains several flat statements which are simply not argumented. Besides the several ad hominems, for which i don't care that much, he stated:\"\n\nYou've obviously never argued anything in your life.\n\n\"That's not invalid reasoning. He makes 2 statements. The first one we agree on, the second one is a questionable statement but the connection between the 2 is not made. Its like saying: \"Oh my, the world is round\" and \"Einstein was a smart mathematician\" with a because between those 2. It makes no sense at all because the alleged corelation is not argumented.\"\n\nYou can't talk bollocks, expect people to get confused and say \"Oh, alright then\". I've met several people who've done this to me - they don't know what they're talking about, and cannot stay on the discussion when they have nothing left to say.\n\n\"In these statements lie various personal attacks, which show personal attachement to various morals.\"\n\nPersonal attacks are usually brought on by the people who post this kind of stupidity. Look at the first post and all the way down this thread as to who brought what on whom. This is also a strategy employed by many people - when you have nothing left to say claim you've been personally victimised, even though you're anonymous.\n\n\"The first statement shows arrogance which is unfounded because David cannot know what exactly happens in the future or the future for QT. He also hasn't shown he has somehow more power over that future than anyone else which would have been a way to gain authority over that statement.\"\n\nAnother classic strategy - claim that no one actually knows anything. You look into the future by reasoning your arguments. I also never claimed I knew anything about the future of Qt. I explained why Qt was technically superior by the way it is funded, and the way it continues to be funded. Nothing is ever 'free', I'm afraid.\n\n\"The second statement is what you quote and its simply not proven or argumented. Its similar to what the Catholic church said about the Earth: that its flat and that the Earth evolves around the Sun while not proving it. Hence it were never scientific statements. The Catholic church, in that situation, pulled an authority argument. That is to say, they used their authority to make people believe something which they were not able to prove. It also seems to argue good programmers (whatever the definition of that may exactly be) don't care for licensing issues. The LGPL statement is similar to my Einstein example here above.\"\n\nBlah, blah, blah. You should do this for charity - you're wasting your talents.\n\n\"Aside from the first statement, the second one somehow seems to know that i've paid for or will pay for Microsoft Office. No merit and untrue.\"\n\nIf you use Windows then if you want to stay legal you need to pay for Windows and Office. After that, because everything in Windows development is geared to Microsoft software, Office inevitably becomes part of the equation if you actually develop for a living which you almost certainly don't.\n\nIf you've warezed all of your software then please don't have the cheek to ask around here that Qt be given away for nothing.\n\n\"As for the last statement, there's no definitive corelation between good development tools and good software.\"\n\nTotally wrong, both practically and logically. If the house that you're building was built with poor materials and tools, it isn't going to stand up very well, is it?\n\n\"It also ignores the possibility that there might be good development tools available for free, which i argue is true. You see, the funny thing is, thats more or less what Linux with GCC et al exactly are considered to be and which KDevelop and Eclipse are.\"\n\nThere are, but when you get to the desktop end development tools become a whole lot more complex, which lends itself better to commercial development tools. You may also want to ask yourself what commercial companies fund the development of 'free' software like GCC and others. Mono certainly isn't free for Novell to develop, and yet they realy need more people to push it forwards. Nothing is ever 'free' I'm afraid, but it is something many people on these forums have deluded themselves inot thinking.\n\n\"Typing and arguing all this costs a great deal of time and doesn't raise awareness of my QT political problem at all.\"\n\nBlah, blah, blah, look at me I've been attacked. I think you'll find your comments starting this all further down this thread.\n\nQt isn't your software, so if you have a problem with it then that's just tough basically.\n\n\"It only deals with someone who's drawing the discussion away from my very problem.\"\n\nNo, I've only been dealing with what's actaully relevant - talking about Qts license, why it is the way that it is, and why it is actually technically superior. The bollocks above is not relevant to any of this, but then again, that's what you want. Roughly translated it means \"I'll talk a lot of crap, and hope that I can tie so many knots that he thinks I know what I'm talking about.\"\n\n\"This someone is who i define as a typical GPL zealot; which people are according to my experience too stubborn to deal with.\"\n\nLook out, everyone's a zealot now!\n\nRegardless of whether I'm a GPL zealot or not, the GPL actually works as a license. It ensures free software development, and also allows companies to fund their software further.\n\n\"I just hope some at least someone won't fall into his pittraps of reasoning because they lack any substance.\"\n\nI'm very glad that you think that, because with nothing left to say that statement shows you know I'm right."
    author: "David"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-29
    body: "I'm not gonna waste any more time to a dwarf like you, you can have the last word. Except for the following statement:\n\n\"I thought it didn't matter what the FSF thought?\"\n\nIt does not matter what Stallman and his friends think. The only thing from the FSF which does matter these days, are the legal matters. Wether LGPL is free software, with what it is compatible are all legal matters. Hence important, although they're humans who can made mistakes."
    author: "Anonymous"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-30
    body: "\"I'm not gonna waste any more time to a dwarf like you, you can have the last word. Except for the following statement:\"\n\nBlah, blah. You shouldn't have started this pointless thread, should you?\n\n\"It does not matter what Stallman and his friends think. The only thing from the FSF which does matter these days, are the legal matters. Wether LGPL is free software, with what it is compatible are all legal matters. Hence important, although they're humans who can made mistakes.\"\n\nWow, thanks for that snippet and thanks for pointing out the bleedin' obvious. It has no relevance to the past 'discussion' though.\n\n"
    author: "David"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-30
    body: "<i>Wow, thanks for that snippet and thanks for pointing out the bleedin' obvious. It has no relevance to the past 'discussion' though.</i>\n\nWhoever looks back in this discussion notes that is you who made incorrect statement on the LGPL and who somehow forces another to accept the FSFs standpoint on LGPL uses while that is in no way 1) dictated 2) practice. Your colors (FSF, Free Software zealot) are seeing the light right now."
    author: "Anonymous"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-30
    body: "\"Whoever looks back in this discussion notes that is you who made incorrect statement on the LGPL and who somehow forces another to accept the FSFs standpoint on LGPL uses while that is in no way 1) dictated 2) practice.\"\n\nThe FSF makes the license, therefore they are entitled to recommend what is 'standard practice' - not you. I have in no way forced anyone - feel free to get lost and do what you like.\n\nFor free software's protection they effectively recommend that you keep development within the cycle of the GPL (used to very, very good effect with Linux) and use the LGPL on a case-by-case basis where you need to link and use other software. That is a pretty logical argument, as once you start arguing the case for the LGPL you are worrying more about what license your software is under, and what it will link against, rather than the actual development of the software itself.\n\nThat's the reasoning behind why I think the FSF has this right in this case, and why the rest of your comments are just bollocks in effectively demanding that Qt be LGPLd. Again, it's not your software.\n\n\"Your colors (FSF, Free Software zealot) are seeing the light right now.\"\n\nWish all you like. As I've pointed out many times, the GPL is a license that works, spectacularly well in the case of Linux and has allowed Qt to be open sourced whilst giving the community the benefit of a fully commercially supporting programing tool.\n\nNo amount of zealot mud-slinging will change that I'm afraid, and I find that amusing, since I have never been an FSF zealot in any way. The FSF have the stance on the GPL and the LGPL exactly right though, and I can certainly accept it when they are right."
    author: "David"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-24
    body: "they don't care, unless they use windows themselves - and if so, they are paying microsoft. then why not pay trolltech?\n\nand that goes for the users - they are willing to pay microsoft, explain me whats wrong with paying for trollteck's product...\n\nif you want to develop a free software product, develop it for a free software platform. non-free software platform-users seem to be willing (stupid enough?) to pay, well, I don't care if they pay. If they don't want to pay, they are FREE to use free software. If they are stupid enough to continue paying Microsoft, well, its oke - but don't complain."
    author: "superstoned"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-24
    body: "\"and that goes for the users - they are willing to pay microsoft, explain me whats wrong with paying for trollteck's product...\"\n\nWell, it is simple, users don't have to pay TrollTech, but developers do.\nAnd they rather spent more time using a less featured product, or charge their boss more ours for porting the software to Windows then they are willing to pay Trolltech for a license.\n"
    author: "ac"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-24
    body: "Free software is GPL'd software. LGPL'd stuff isn't.\n\nYou also conveniently paint over the fact that free software developers who want to run their software on Windows have to pay Microsoft - yes, Microsoft. If I want to develop for Windows I expect to pay.\n\nPlease, keep your brainless wonder ideas to yourself please."
    author: "David"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-24
    body: "\"Free software is GPL'd software. LGPL'd stuff isn't.\"\n\nHA HA HA. IDIOT.\n\nGPL is free software\nLGPL is free software\n\nMOOOROOOON!!!\n\nAnd no we don't have to pay, except to Microsoft. Python, Perl, Tk, GTK they're all free. Free software, you know -- and more free than QT."
    author: "Anonymous"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-24
    body: "I believe what David meant is that GPL is more friendly of free software. The reason for this is that everybody that modifies and/or uses/links GPL code will have to license their software under a free software license. In this sense, it is more friendly of the free software community.\nAnd yes, LGPL (used by GTK, Python, etc) are more free in the sense that they allow you to use them for property software. But, at the same time, it hurts free software because it allows this to happen.\n\nI don't necessarly agree with this opinion, just trying to help. Please don't flame me. ;)"
    author: "blacksheep"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-25
    body: "I suggest you get yourself along to the FSF web site then. LGPL'd software is not considered to be free software, but merely a step on the road to getting people to use free software - i.e. the GPL.\n\nFree does not mean \"I don't have to pay for anything.\" Please, do educate yourself, quickly."
    author: "David"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-25
    body: "You're a fucking idiot. Same David as the idiot from OSnews? That would explain something.\n\nHere have a look at http://www.gnu.org/licenses/license-list.html\nEspecially \"GPL-Compatible, Free Software Licenses\"\n\nQUOTE\nGNU Lesser General Public License, or GNU LGPL for short.\n    This is a free software license, but not a strong copyleft license, because it permits linking with non-free modules. It is compatible with the GNU GPL. We recommend it for special circumstances only.\nUNQUOTE\n\nStop spreading bullshit. The LGPL is free software."
    author: "Anonymous"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-26
    body: "Hi there anonymous. I suggest you read what you've quoted. The LGPL is not considered 'free software' in the same sense for exactly that reason - 'we recommend it for special circumstances only'.\n\nThat means that it is not recommended for LGPLing everything and telling everyone that you can develop everything for free, which is what you want. I'm afraid that just isn't realistic, and isn't recommended by the FSF either.\n\nThe LGPL is a license for special cases where you need to link with other kinds of software. It is not a license for free everything, because nothing is completely free in a monetary sense. That's the point I've tried to get across many times, but it just doesn't seem to get through many thick skulls.\n\nI know I've frustrated a lot of people because they know I'm right, but quite frankly, I'm not responsible for the arseholes who post crap to these places. If you want to comment, I suggest you think first. If not - go away."
    author: "David"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-27
    body: "\"Hi there anonymous. I suggest you read what you've quoted. The LGPL is not considered 'free software' in the same sense for exactly that reason - 'we recommend it for special circumstances only'.\n\nThat means that it is not recommended for LGPLing everything and telling everyone that you can develop everything for free, which is what you want. I'm afraid that just isn't realistic, and isn't recommended by the FSF either.\"\n\nBullshit. Who cares what the FSF says in regards of what 'they recommend'? What if the only reason i see the FSF as authority is because they define what free software is and that i only wonder wether they qualify a certain a certain license as free software or not? I don't see OSI whining about 'recommendations' and they include the LGPL as open source. You may argue your moral values, but don't whine when i don't agree with them.\n\nThe following 2 statements are the ones i care for:\n* LGPL is free software according to the FSF.\n* LGPL is open source according to the OSI.\nBoth are true. I prefer the LGPL above the GPL in cases such as libraries, for non-GPL compatibility (theres more than the GPL) and for proprietary compatibility.\n\n\"The LGPL is a license for special cases where you need to link with other kinds of software. It is not a license for free everything, because nothing is completely free in a monetary sense. That's the point I've tried to get across many times, but it just doesn't seem to get through many thick skulls.\"\n\nActually its not for libraries alone. Thats why the FSF uses 'Lesser' instead of 'Library' for the 'L' these days. Besides that, this is merely what the FSF argues, and they're not some higher power, although to some its a church. Hence it can be used to license whatever you like.\n\n\"I know I've frustrated a lot of people because they know I'm right, but quite frankly, I'm not responsible for the arseholes who post crap to these places. If you want to comment, I suggest you think first. If not - go away.\"\n\nStraw man. FWIW, don't force your moral values and arguments of authority upon me. Have a nice day!"
    author: "Anonymous"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-28
    body: "\"Bullshit. Who cares what the FSF says in regards of what 'they recommend'?\"\n\nBecause it's their license and they created it, perhaps?\n\n\"Actually its not for libraries alone. Thats why the FSF uses 'Lesser' instead of 'Library' for the 'L' these days.\"\n\nYou use the GPL wherever you possibly can. The LGPL is for instances where you need to link against and use other software, but it isn't for everything. You have to have the partition that the GPL gives you otherwise you just don't get the payback in terms of development that software like the Linux kernel enjoys.\n\n\"Straw man. FWIW, don't force your moral values and arguments of authority upon me.\"\n\nI'd hardly call them moral values or arguments of authority. I'm just plain right."
    author: "David"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-28
    body: "\"You use the GPL wherever you possibly can.\"\n\nEhm, no? Its my software. I decide what license i use where, and i decide wether thats GPL, LGPL or something else. What the FSF recommends is fine (if they have arguments) but its no rule. It doesn't matter they created the license. That doesn't mean they have full authority and know what i want to allow or disallow with my software.\n\nI noticed you're stubborn and want to have the last reply. You can have it."
    author: "Anonymous"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-29
    body: "\"Ehm, no? Its my software.\"\n\nThen don't post to sites like this and basically demand that Qt is LGPLd. It's not your software - OK?\n\n\"I noticed you're stubborn and want to have the last reply. You can have it.\"\n\nPlease don't try and pretend you have the moral high-ground here - you're not good at it. You're wrong and know it, OK?"
    author: "David"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-25
    body: "\"\"\"Free software is GPL'd software. LGPL'd stuff isn't.\"\"\"\n\nEr, I've never heard someone argue that, could you elaborate?"
    author: "Anonymous"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-25
    body: "Directly from the people who defined the term \"Free Software\": http://www.gnu.org/philosophy/why-not-lgpl.html"
    author: "ac"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-25
    body: "Here have a look at http://www.gnu.org/licenses/license-list.html\nEspecially \"GPL-Compatible, Free Software Licenses\"\n\nQUOTE\nGNU Lesser General Public License, or GNU LGPL for short.\n    This is a free software license, but not a strong copyleft license, because it permits linking with non-free modules. It is compatible with the GNU GPL. We recommend it for special circumstances only.\nUNQUOTE\n\nStop spreading bullshit. The LGPL is free software."
    author: "Anonymous"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-25
    body: ">Directly from the people who defined the term \"Free Software\": http://www.gnu.org/philosophy/why-not-lgpl.html\n\nThe link tells you, that you should think about it if you really want to use the LGPL. But same could be even said about weaker Free Software licenses like the BSD license.\n\nThe real link would be this:\nhttp://www.gnu.org/philosophy/free-sw.html\n\nIf you comply the 4 points of the Free Software definition the program is Free Software and LGPL porgrams comply this 4 points.\n\nAt the end you can look at the (incomplete) list of Free Software licenses, and you will see that the LGPL (beside many other licenses) is listen as a \"Free Software License\": http://www.gnu.org/licenses/license-list.html\n\nSo stop talking about things you don't know.\n"
    author: "pinky"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-24
    body: "Well, it is even worse, those users should pay money to Microsoft in order to run your Windows-version of the software.\nShame on Microsoft, Shame on TrollTech, for charging people for using their software.\n\nHek, even bread is available for free at the local bakery.\nSo why should somebody spent money on software??\n\n"
    author: "ac"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-24
    body: "You have GTK2 running on Windows, but you won't have it working :). Thanks for the troll.\n\nAround here we talk about free software desktops and operating systems, and that's what we want to run our free software on."
    author: "David"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-24
    body: "Actually GTK2 works perfectly fine on Windows. Stop spreading misinformation.\n\n<blockquote>Around here we talk about free software desktops and operating systems, and that's what we want to run our free software on.</blockquote>\n\nThats why your philosophy sucks. Bill Gates only cares for Windows; you only care for Linsux. The end result is more or less the same, although QT runs on more than Linsux alone -- and FREE!"
    author: "Anonymous"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-25
    body: "\"Actually GTK2 works perfectly fine on Windows. Stop spreading misinformation.\"\n\nThen, what is your problem?\n\n \n \"Thats why your philosophy sucks. Bill Gates only cares for Windows; you only care for Linsux.\"\nAgain, there is more in the world then windows and linux.\nand Qt GPL supports quite a lot of them.\n\n\n\" The end result is more or less the same, although QT runs on more than Linsux alone -- and FREE!\"\nYeps, gratis :o)\nSo, what is your problem?\n"
    author: "ac"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-25
    body: "\"Again, there is more in the world then windows and linux.\nand Qt GPL supports quite a lot of them.\"\n\n..but almost nobody runs these other OSes so it makes little sense to port MY application to these OSes. Although i would accept a patch. It means there's almost no new users; with a Windows port the chances are higher.\n\n\"So, what is your problem?\"\n\nThis dorkish licensing scheme.\n\nThink about it. What if i were a software developer on Windows and i were to develop using a cross-platform toolkit? QT falls from the list right away."
    author: "Anonymous"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-26
    body: "\"Think about it. What if i were a software developer on Windows and i were to develop using a cross-platform toolkit? QT falls from the list right away.\"\n\nIf you were a Windows-developer, then you would already be used to the fact that you have to pay for your development tools and work with restricted software licenses.\nSo you would not have any problem using Qt because of its license."
    author: "ac"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-26
    body: "\"Think about it. What if i were a software developer on Windows and i were to develop using a cross-platform toolkit? QT falls from the list right away.\"\n\nYou don't, and have never, written software for a living. All Windows developers who develop commercially, without exception in many cases, pay large sums of money for development tools. This is the way things work out in the real world. I'm getting rather sick of of this \"Oh, developers want totally free and crap development tools\" or \"small development shops can't afford it\". Small development shops all over the world pay vast sums for development tools, as well as all the other software they need. This is total rubbish.\n\nPlease don't comment on things you obviously know nothing about. Comments like this only confirm it, and show just how disconnected from reality you and everyone who talks like this are."
    author: "David"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-27
    body: "Hello David,\ni agree to 100% to your writting.\nBut one point, who nobody can explain, is the future. With future i mean the children wo learn programming and will in the feature maybe make the decision which tool they use in bussiness.\nFor this group and for Trolltech, it would be create if there were also a GPL windows version. Trolltech would lose non of there costumers who develop proprietary software but maybe will gain costumers in the future who have learned and loved the Qt Toolkit in there childhood."
    author: "pinky"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-30
    body: "TrollTech can of course give free or cheaper versions to schools in stead.\nNo need to release it under GPL.\nAlso Schools don't have much choise when it comes toe free software on Windows, they usually need to pay through their nose for Windows-software (they would be better of using a true open source os, like linux..)"
    author: "ac"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-27
    body: "\"All Windows developers who develop commercially, without exception in many cases, pay large sums of money for development tools.\"\n\nUnrelated to what i'm saying. In my case, i'm developing free software / open source for Windows. Have a nice day David. Nice try though."
    author: "Anonymous"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-27
    body: "For the unenlightened:\n\n\"What if i were a software developer on Windows and i were to develop using a cross-platform toolkit?\"\n\nMakes no mention of developing free software on Windows - which is pointless anway. If you're a software developer on Windows (of any description) you are paying for development software. If you're developing free software you'll probably have a copy of Visual Studio anyway for any other work you'll be doing. You'll also have had to pay for a copy of Windows and Office and all the usual software you need on that platform.\n\nSo tell me - how much does free software on Windows cost again? Given that, I have to question your commitment to free software, and your sanity. Windows isn't free - OK?"
    author: "David"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-27
    body: "\"If you're a software developer on Windows (of any description) you are paying for development software. If you're developing free software you'll probably have a copy of Visual Studio anyway\"\n\nEver heard of Cygwin? Ever heard of cross-platform applications?\n\n\"So tell me - how much does free software on Windows cost again?\"\n\nIf you really insist, i'm able to get Windows licenses up to Windows 2000 for free. As in, it doesn't cost me anything. But that's not my point. My point is that i'm merely porting free software for non-*NIX users; Windows users.\n\nStop acting like a dork. I said that for like 3 times already throughout this thread. Thank you."
    author: "Anonymous"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-27
    body: "Every heard of Qt-cygwin GPL?  Dork."
    author: "ac"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-28
    body: "Doesn't work very well though. IIRC it requires target = x11 (hence an X server!) its compiled with GCC, which means its slow."
    author: "Anonymous"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-29
    body: ">If you really insist, i'm able to get Windows licenses up to Windows 2000 for free.\nOk, so you can get somebody else to pay for your developmentpalform, so what.\n\n>Ever heard of Cygwin? Ever heard of cross-platform applications?\nSince the GPL Qt for X11 has run on Cygwin for years, your whole argument against Qt based on price/non GPL on windows now dissapears. \n\nBut what I really want to know are, which open source Cygwin based projects have you done? And where can I download them?"
    author: "Morty"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-29
    body: ">If you really insist, i'm able to get Windows licenses up to Windows 2000 for free.\nOk, so you can get sombody else to pay for your developmnetpalform, so what.\n\n>Ever heard of Cygwin? Ever heard of cross-platform applications?\nSince the GPL Qt for X11 has run on Cygwin for years, your whole argument against Qt based on price/non GPL on windows now dissapears. \n\nBut what I realy want to know are, which open source Cygwin based projects have you done? And where can I download them?\n\n"
    author: "Morty"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-29
    body: "> Ok, so you can get sombody else to pay for your developmnetpalform, so what.\n\ns/pay/paid\n\n> Since the GPL Qt for X11 has run on Cygwin for years, your whole argument against Qt based on price/non GPL on windows now dissapears.\n\nIt actually doesn't. As you say, the Cygwin one is \"GPL Qt for X11\" not \"GPL Qt for Windows\". I don't want to deal with this politics crap, i only want to use QT to develop a true cross-platform application.\n\n\"But what I realy want to know are, which open source Cygwin based projects have you done? And where can I download them?\"\n\nIt hasn't got much to do with X11 or Cygwin. Where did i wrote that? I'm writing a cross-platform, opensource Usenet client like BNR2 but then open source (LGPL) and more modular. Its still in development."
    author: "Anonymous"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-30
    body: "\"It actually doesn't. As you say, the Cygwin one is \"GPL Qt for X11\" not \"GPL Qt for Windows\". I don't want to deal with this politics crap, i only want to use QT to develop a true cross-platform application\"\n\nWell, then stop with the politics, pay for your Qt-license and start porting stuff to Windows..\n"
    author: "ac"
  - subject: "Clueless or trolling?"
    date: 2004-12-30
    body: ">\"GPL Qt for X11\" not \"GPL Qt for Windows\".\nAnd this matter how? There is no politics no noting only the GPL, and the GPl gievs you the rigt to build it with Cygwin or any other place you please. "
    author: "Morty"
  - subject: "Re: Clueless or trolling?"
    date: 2005-01-01
    body: "Who cares? I'm really wondering who's clueless here.\nDo i have to spell it out? X11 != Windows. I'm not gonna run some X server."
    author: "Anonymous"
  - subject: "Re: Clueless or trolling?"
    date: 2005-01-02
    body: "Then don't and wander off back to Windows, tail between your legs.\n\nIf you have a look at this site and those around it, KDE is a desktop for Unix/Linux and free/open source software systems where you can see the infrastructure that an OS is built on. Qt fits in with that purpose.\n\nIf you want to run Qt natively on Windows then pay for it, as you do for Windows and as you do for a great deal of other software on that platform. If you don't want to run quality development software then there's plenty of shite you can use.\n\nIf you still don't like it then you can just get lost basically - no one can help you. I suggest you put some time and effort into developing the LGPL development tools you need.\n\n\"I'm really wondering who's clueless here.\"\n\nHere's a hint. It's you......."
    author: "David"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-28
    body: "\"So tell me - how much does free software on Windows cost again?\"\n\nLess than you think. The C++ compiler and Platform SDK are available for free download from MSDN. And Office is a non sequitur. If you need to exchange documents on a cross-platform Open Source project, you'd be foolish to choose MS Office over OpenOffice.org, anyway.\n\nSo what you're left with is the cost of a Windows license. We all want to get rid of the Dane, but we can't stop paying Dane-gold just yet.\n\n\"I have to question your commitment to free software, and your sanity.\"\n\nSo you think anyone working on Mozilla, or OpenOffice.org, or Apache is insane? You think they lack commitment to the Open Source movement because they're displacing Microsoft's browser, office suite, and web server? Or would you gladly sacrifice everything those projects, and others like them, have gained, just for one operating system kernel?\n\nHating Microsoft just wastes energy better spent supporting Open Source software."
    author: "Keith Russell"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-29
    body: "\"Less than you think. The C++ compiler and Platform SDK are available for free download from MSDN.\"\n\nWhich keeps you programming on Windows. Why do you think Microsoft gives this stuff away?!\n\n\"Hating Microsoft just wastes energy better spent supporting Open Source software.\"\n\nIt's not a question hating Microsoft. To truly have an open source platform you need open source software on a totally visible and open source operating system. You also need application that will attract people to it, not get them to stay where they are.\n\nFirefox and Mozilla, strategically speaking (making developers aware of other browsers), have been reasonably good for open source software. However, you have to ask yourself what it will take to get people off Windows because that is ultimately what is required to ensure the survival of open source software."
    author: "David"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-30
    body: "\"Unrelated to what i'm saying. In my case, i'm developing free software / open source for Windows. Have a nice day David. Nice try though.\"\n\nThat is in correct, you are porting free software to Windows, not developing free software for Windows.\nIf you were developing something for Windows in the first place (and want to port it to linux for example), you would already have paid for you development tools, as that is what you do when you work with Windows, you pay for your licenses..\n\n"
    author: "ac"
  - subject: "Re: warez the Windows version?"
    date: 2004-12-26
    body: "My understanding was that at some stage there was an opensource version of QT\nfor windows but Trolltech pulled because companies were abusing it by using it to create commercial apps.\n\nQt have granted Opensource licenses to some known Opensource projects so their product can run on windows. I guess that Trolltech want to be very careful controlling who gets this access (can't blame them since this is their revenue stream)"
    author: "Danni Coy"
  - subject: "Funny how history repeats itself, indirectly"
    date: 2004-12-23
    body: "I remember us at NeXT with out MVC view of computing was considered outdated.\n\nNow if only Konqueror 3.3.2 could stop hanging on forms during editing and I just might enjoy it.  Debian Sid, KDE 3.3.2.  The damn thing has slowed down with each .x.x revision.\n\nAny idea folks?  Or does one have to wait until Qt4 for this crap to go away?"
    author: "Marc Driftmeyer"
  - subject: "Re: Funny how history repeats itself, indirectly"
    date: 2004-12-23
    body: "Turn off spell-checking?"
    author: "Anonymous"
  - subject: "Re: Funny how history repeats itself, indirectly"
    date: 2004-12-26
    body: "How?"
    author: "ac"
  - subject: "Re: Funny how history repeats itself, indirectly"
    date: 2005-01-05
    body: "Klipper is also reported to cause problems with form editing, I believe. Try killing klipper and seeing if konqueror responds better."
    author: "Dolio"
  - subject: "Don't Upgrade youe SUSE Kernel"
    date: 2004-12-24
    body: "Sorry for posting here, but  a) I  know some suse people hang out here and I so want to tell suse  b) its more active than suse.com and c) some of you might be grateful\n\nI klicked on suse watcher this morning to discover a kernel upgrade - and broke my kernel - it stiffs reiserfs and rendered my computer unusable.  The upside is that I am back up and running having used repair to downgrade the kernel   - the downside is that I am now running 3.3.0 rather than 3.3.2 and have got a lot of updating to do.\n\n\n"
    author: "gerry"
  - subject: "Re: Don't Upgrade youe SUSE Kernel"
    date: 2004-12-25
    body: "Thanks for your warning :o)\nEarlier, suse YOU broke mplayer with a XFRee-update (which I have reverted..), so now I should also stay away from kernelupgrades from suse :(\n\nBut why are you back at KDE 3.3.0?\nDid you re-install SuSE?\nYou could have started SuSE from install disk, use chroot to go back to your hard-disc installation (still using the kernel from the cd/dvd), start YOU and downgrade the kernel.."
    author: "ac"
  - subject: "Re: Don't Upgrade youe SUSE Kernel"
    date: 2004-12-25
    body: "I used \"upgrade\" and by accident upgraded everything in the wrong direction.  I don't really understand enough about under the bonnet Linux but now have a copy the Linux Cookbook  :)  Now understand chroot\n\nThere's an even cleverer solution over at suse-linux-e which I still don't understand :( "
    author: "gerry"
  - subject: "Only Viable Option"
    date: 2004-12-24
    body: "Qt 4 is a quality development tool, and quite frankly, when you are talking about getting people outside of the usual 'open source community' to develop for something other than Windows (Windows developers etc.) it's the only viable option in town. GTK, Mono, Java and other alternative methods of development are fine as plain open source tools, but realistically speaking, in this context they just don't cut it.\n\nThe most intersting part of Qt 4 for me is probably Interview. As someone who develops a lot of applications that have all sorts of data using different views this looks really, really good, and having native support to attach models and views (rather than all that stupid automatically generated Visual Studio .Net code) is very, very nice. Scribe looks nice, but support for things like the Open Office XML file format would be much nicer. The rest of it is just all-round better - no comments there.\n\nQt Designer looks a lot better these days, but I haven't tried it used it enough yet. C++ has never looked so good, but I think Trolltech need to have a serious look at higher level languages like C#, Java and above that Python and VB. If you could use Qt fully with a higher level language (yes, I know there are bindings) you could get so much done it wouldn't be believable.\n\n"
    author: "David"
  - subject: "Re: Only Viable Option"
    date: 2004-12-24
    body: "at the akademy i heard trolltech was considering to support either C# (Mono-compatible) or Java. At that point no decision was made since both options had much outstanding pro and cons (i.e. copyright/Microshaft issues with Mono's free-C# and Java's well know issues).\n\nI there anyone here who has some news?\n\nFor me trolltech maintaining bindings to some managed-code programming language will make me learn that language; since i think managed-code programming languages need Qt to become (1) truly crossplatform, and to have a (2) real x-platform widget lib that has a (3) nice API.\n\n_cies.\n"
    author: "cies"
  - subject: "Re: Only Viable Option"
    date: 2004-12-24
    body: "This is the first I've heard of it, but it would be WAY COOL if they did.\n\nC++ is nice and all, but really, in this day and age there is no excuse to do application development in a non-memory-managed language.  MM Languages (Java, C#, Python, hell even Perl) are just so much more robust from a reliability standpoint.  A good language with runtime garbage collection and bounds checking automatically eliminates dozens of the most common kinds of bugs and security holes.  Yes, fundamentally C# and Java are MORE SECURE than C++, no matter how good a programmer you are.  The time and energy required to write bulletproof code in Python or Java or C# is far lower than it is in C, C++, or the rest of that family.  Period.  (Yes, that means C#/.NET on Windows is a HUGE threat to the \"we're actually secure\" argument for GNU/Linux / Free Software, one that needs to be addressed.)\n\nQt, from everthing I've seen, is an amazing and mouth-watering API.  But having to remember to manually free your memory makes a HUGE dent in productivity.  If they could make it easy/trivial to use C# or, hell, even mutate Qt into a memory managed language (it's already not pure C++), that would rock serious ass on all platforms.  Particularly if it meant that you could then do KDE4 development using C# or some similarly managed language.  That would be The Killer App, IMHO."
    author: "Larry Garfield"
  - subject: "Re: Only Viable Option"
    date: 2004-12-25
    body: "> But having to remember to manually free your memory makes a HUGE dent in productivity.\n\nHm, and there was me thinking that QObject subclasses automagically delete objects they are parent to. "
    author: "Paul"
  - subject: "Re: Only Viable Option"
    date: 2004-12-30
    body: "\"But having to remember to manually free your memory makes a HUGE dent in productivity.\"\n\nI agree with a lot of arguments for memory management, and can't wait for it myself, by saying it puts a huge dent in productivity is .. overdoing it a lot.\nEspecially with what I've seen of qt 4 (elimination of explicitly shared classes, getting rid of qptrlist, etc)."
    author: "JohnFlux"
  - subject: "Try the Python \"bindings\""
    date: 2004-12-24
    body: "PyQt is really very, very good. You may say they're just bindings, but the generation tool (sip) was written specifically for use with qt and python. Signals and slots are actually easier to do in python (no messing with moc), and there's a working uic for python. PyQt is actually my preferred way of doing a gui."
    author: "mikeyd"
  - subject: "Re: Try the Python \"bindings\""
    date: 2004-12-24
    body: "It's too bad that PyKDE is always a version or two behind."
    author: "ac"
  - subject: "Re: Try the Python \"bindings\""
    date: 2004-12-25
    body: "can I use PyQt with Qt4beta now?\n\nThank you for your answer and merry xmas!\n"
    author: "anonymous"
  - subject: "Multi-mouse-button support"
    date: 2004-12-24
    body: "Do anyone know if this version implements support for mice with more than the two+scroll mouse buttons? A wishlist item on bugs.kde.org got the response it was a limitation of QT, not KDE. I'm really looking forward to using back/forward/application switcher, etc. "
    author: "mikal"
  - subject: "Re: Multi-mouse-button support"
    date: 2004-12-25
    body: "Can't you map them with xmodmap?"
    author: "ac"
---
<a href="http://www.trolltech.com/">Trolltech</a> has released the first Beta version of the upcoming Qt 4.0. You can download it from <a href="ftp://ftp.trolltech.com/qt/source/">ftp.trolltech.com</a> or from one of <a href="http://www.trolltech.com/download/betas.html">its mirrors</a>. An updated online <a href="http://doc.trolltech.com/4.0/">Qt Reference Documentation</a> is also available. The final Qt 4.0 is expected to be released in late first quarter of 2005.













<!--break-->
<p>There are five new technologies that are new to Qt, written specifically for Qt 4:</p>

<ul>
<li><a href="http://doc.trolltech.com/4.0/qt4-tulip.html">Tulip</a>, a new set of template container classes.</li>
<li><a href="http://doc.trolltech.com/4.0/qt4-interview.html">Interview</a>, a model/view architecture for item views.</li>
<li><a href="http://doc.trolltech.com/4.0/qt4-arthur.html">Arthur</a>, the Qt 4 painting framework.</li>
<li><a href="http://doc.trolltech.com/4.0/qt4-scribe.html">Scribe</a>, the Unicode text renderer with a public API for performing low-level text layout.</li>
<li><a href="http://doc.trolltech.com/4.0/qt4-mainwindow.html">Mainwindow</a>, a modern action-based mainwindow, toolbar, menu, and docking architecture.</li>
</ul>
<p>This beta release also previews the new <a href="http://doc.trolltech.com/4.0/qt4-designer.html">Qt Designer</a> user interface design tool which is still heavily under development.
<p>In addition, the following modules have been significantly improved since Qt 3:</p>
<ul>
<li>A fully cross-platform <a href="http://doc.trolltech.com/4.0/qt4-accessibility.html">accessibility</a> module, with support for the emerging SP-API Unix standard in addition to Microsoft and Mac Accessibility.</li>
<li>The <a href="http://doc.trolltech.com/4.0/qt4-sql.html">SQL module</a>, which is now based on the Interview model/view framework.</li>
<li>The <a href="http://doc.trolltech.com/4.0/qt4-network.html">network module</a>, with better support for UDP and synchronous sockets.</li>
<li>The <a href="http://doc.trolltech.com/4.0/qt4-styles.html">style API</a>, which is now decoupled from the widgets, meaning that you can draw any user interface element on any device (widget, pixmap, etc.).</li>
<li>Enhanced <a href="http://doc.trolltech.com/4.0/qt4-threads.html">thread support</a>, with signal-slot connections across threads and per-thread event loops.</li>
</ul>
<p>Trolltech has set up a special mailing list, <a href="http://lists.trolltech.com/qt4-preview-feedback/">qt4-preview-feedback</a>, for discussion of issues relating to the Qt 4 beta releases.</p>






