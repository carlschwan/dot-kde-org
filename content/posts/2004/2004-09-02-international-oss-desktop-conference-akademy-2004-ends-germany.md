---
title: "International OSS Desktop Conference aKademy 2004 Ends in Germany"
date:    2004-09-02
authors:
  - "pipitas"
slug:    international-oss-desktop-conference-akademy-2004-ends-germany
comments:
  - subject: "Great News"
    date: 2004-09-02
    body: "I'm glad so much got done, I've been keeping up with news through the streams and newsforge, Wish I could of made it... :(\n\nMaybe next time."
    author: "Leon Pennington"
  - subject: "Re: Great News"
    date: 2004-09-02
    body: "It was fun, but with everything going on I am trying to read up on everything I missed while I was there. Not that I would trade having been there for anything, but we were reading web pages while we were there to try to find out the other things we missed while we were hacking or in the other presentation area. However I did manage to snag a couple cool T-Shirts. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: Great News"
    date: 2004-09-03
    body: "Any relation to Havoc?"
    author: "AC"
  - subject: "Re: Great News"
    date: 2004-09-03
    body: "No, That would be scary :)"
    author: "Leon Pennington"
  - subject: "THE single most impressive new thing from aKademy?"
    date: 2004-09-03
    body: "Just being curious|\n\nWhat do participants dub THE single most impressive new thingie they took back from aKademy?\n\nTo outsiders they event seems confusing. There was a lot going on and all....yadda-yadda-yadda.... But reading all the different weblogs and reports, it looks like KDE has entirely lost focus. Are their developers in any way still motivated to hunt behind GNOMEs advances? Do they still have vision? Do they still follow a plan? Do they have a strategy?"
    author: "Curious"
  - subject: "Re: THE single most impressive new thing from aKad"
    date: 2004-09-03
    body: "> What do participants dub THE single most impressive new thingie they took back from aKademy?\n\nI'd have to say the CIG/HIG/AG. If this is done successfully, I think it will be one of the most expansive set of documents relating to Usability, Accessibility, and Identity in not only Open Source but in any single platform. If successful, it will have the capacity and capability of large changes in many KDE applications, and hopefully applications (OpenOffice, Mozilla, GNOME), will follow suit. "
    author: "anon"
  - subject: "Re: THE single most impressive new thing from aKad"
    date: 2004-09-03
    body: "... and we've got a tremendous start on the work. this isn't just talk; significant portions of the documents have been written and are under review."
    author: "Aaron J. Seigo"
  - subject: "Re: THE single most impressive new thing from aKad"
    date: 2004-09-03
    body: ">Just being curious|\n\n>What do participants dub THE single most impressive new thingie they took back >from aKademy?\n\nI wasn't there, but I think it will be etheir the new multimedia stuff or metadata  and searching though same. Both of these seem to be quite big and have had a lot of talking about them, both online and off. \n\n>To outsiders they event seems confusing. There was a lot going on and >all....yadda-yadda-yadda....\n\nI think it was confusing for those who were there too. From reading the blogs and the comments above it seems to me that because it is the only time all the devs can get together, there is a need to get lots of talking done really quickly. I think about a month or two from now, we will see large amounts of the vaquorus ideas floating around in the blogs, websites, mailing lists and dev's heads take solid form in code. \n\n>But reading all the different weblogs and reports, it looks like KDE has >entirely lost focus. Are their developers in any way still motivated to hunt >behind GNOMEs advances? Do they still have vision? Do they still follow a plan? >Do they have a strategy?\n\nFirst of all, I have never seen KDE with a plan, a strategy or even focus. Such a thing is against the way kde works. \n\nOn the \"can kde keep up with gnome\" question, I would say that in some areas kde is ahead and in other areas gnome is ahead. I also like to remember what both were like when I started using linux, about 2 years ago:\n\nBack then, kde had just released 3.0 and gnome had just released 2.0. I started off playing with 2.2 and 1.4 but soon upgraded to the new versions. I settled on kde for a bunch of reasons that I wil try and explain:\n\nBack then, kde 2.2 was slow, but the speed improvement from 2.2 to 3.0 was amazing, at least twice as fast. nothing gave me more confidence in a project than it's ability to fix its biggest problem so quickly. gnome, on the other hand, was faster to begin with, but didn't show such a massive improvement in speed between 1.4 and 2.0 (maybe there was a speed improvement, it just wasn't as big as the kde improvement)\n\nBack then, kde apps sucked. there wasn't anything like kopete, appollon, juk or amarok, quanta was a shadow of it's present self, kofffice was crash happy, and so on. There was no kde-apps.org, instead there was apps.kde.org, a money making site that could have given lessons in bad interface design to anyone you care to name. It was common pratice to criticise the kde devs for writing their own html rendering engine and window manager. I mean, gecko was very good back then, and there seemed to be no need for a second open source html renderer.\n\nAt the same time however gnome used the best-of-breed open source apps. Mozilla and Galon were wonderful web browsers, kmail couldn't (back then) hold a candle to Evolution, noatan sucked when compared to xmms, the gnome office apps like abiword and gnumeric kicked koffice's butt, and even things like the games were much better. \n\nBut the difference between kde apps and gnome apps was that kde apps looked and felt the same. They used the same dialog boxes and icons, they all had the same menu options in the same places. gnome apps, on the other hand, seemed to me if the gnome devs had gone out onto the internet, found the best apps, and bundled them with gnome. I didn't like that much. I wanted all my apps to look and feel the same.\n\nBecause the desktop showed so much promise, and the apps all felt the same, I whent with kde. not long after I made this choice, I ended up talking to a couple of gnome users, both with years of linux useage under their belts. They stated that they used gnome because:\n\n1) it is faster than kde and uses less resources (remember, two years ago) \n\n2) the best apps are bundled with gnome\n\n3) it is written in C, a standard language with good compliers for linux. kde, on the other hand, is written in C++, which was, at the time, non-standard and less well supported under linux\n\n4) gnome was very confgurable, for example you could use one of three window managers with it\n\nLets think about the sitution now. \n\n1) kde apps rock now, quanta is the best web dev environment for linux, juk is the best itunes style music player, kdevelop is a great heavy weight IDE, kate is a wonderful text editor, kmail is very good, there have been some great dev tools such as valgrind invented, and so on.\n\n2) kde is faster than gnome now. \n\n3) kde has way more config options now.\n\n4) kde seems to be getting along fine with C++, while gnome is looking for a new language to use (C#, python)\n\non the other hand, gnome is beating kde on the look and feel issue and the intergration issue, also, gnome is using khtml.\n\nso in the two years I have been using linux, the two desktop environments have changed their positions completely. If I was a newbie again, I would pick gnome.  wierd. \n\nI think that these things go in cycles, I think that kde will sit down and tame the mass of options in the control centre, write it's own HIG, and get involved  in HAL and DBUS. And I think that gnome will start bringing in more options, start using some new language and speed things up again. After all it is a win-win for the users and it lets the devs say things like, if you want (kde/gnome) you know where to find it\n\nPete"
    author: "Peter Nuttall"
  - subject: "Re: THE single most impressive new thing from aKad"
    date: 2004-09-03
    body: "> I think that these things go in cycles.\n\nInteresting post. Yeah, I think GNOME is headed towards KDE. and KDE is headed towards GNOME. Perhaps KDE 5/GNOME 4 will see that reversed. "
    author: "anon"
  - subject: "Re: THE single most impressive new thing from aKad"
    date: 2004-09-03
    body: "> instead there was apps.kde.org, a money making site\n\nThere was never a apps.kde.org, you mean apps.kde.com."
    author: "Anonymous"
  - subject: "Re: THE single most impressive new thing from aKad"
    date: 2004-09-03
    body: ">> instead there was apps.kde.org, a money making site\n\n> There was never a apps.kde.org, you mean apps.kde.com.\n\nThat would be it, thanks\n\npete"
    author: "Peter Nuttall"
  - subject: "Re: THE single most impressive new thing from aKad"
    date: 2004-09-03
    body: "> gnome apps, on the other hand, seemed to me if the gnome devs had gone \n> out onto the internet, found the best apps, and bundled them with gnome. \n\nIt did not only seem to be like that, that's exactly what had happened.  XMMS, OpenOffice.org and Mozilla are often seen as being under the umbrella of the GNOME project, but the only real connection to GNOME was the common use of the gtk toolkit, where the \"g\" does *not* stand for GNOME but for gimp.\n\nBundling is the correct term. "
    author: "cm"
  - subject: "Re: THE single most impressive new thing from aKad"
    date: 2004-09-04
    body: "the G actually stands for GNU, not?"
    author: "Debian User"
  - subject: "Re: THE single most impressive new thing from aKad"
    date: 2004-09-04
    body: "The \"G\" in GIMP yes, but not the \"G\" in Gtk+. :-)"
    author: "Anonymous"
  - subject: "Re: THE single most impressive new thing from aKad"
    date: 2004-09-04
    body: "I thought that openoffice has its own toolkit and thats why its slow. Mozilla prior to 1.5 also did that, didn't it ?\n\nMost gtk apps load very quickly - why then do OO & mozilla load so slowly even today ?"
    author: "vm"
  - subject: "Re: THE single most impressive new thing from aKad"
    date: 2004-09-04
    body: ">on the other hand, gnome is beating kde on the look and feel issue and the >intergration issue, also, gnome is using khtml.\n\nInteresting that you should see it that way.  I had to use gnome for 3 weeks in August, and I believe that for me KDE has better looks, better integration and is easier to use.   The *only* thing I see gnome doing better at is vision: dbus, hal, etc.   For *many* things I see in gnome 2.8, it seems like kde has had for a while.\n\nGnome's use of mozilla/firefox/open office I see as a HUGE negative, since they are so poorly integrated.  Ximian apparently has made an effort to better integrate open office, which is appreciated.\n\nBut I have to say that those 3 weeks were pure torture to me."
    author: "TomL"
  - subject: "Re: THE single most impressive new thing from aKad"
    date: 2004-09-04
    body: "IMO KDE looks much more polished than GNOME.\nGNOME looks like a kiddies desktop - too primary & flat.\nKDE is very easy on the eye.\nJust my opinion - but also that of my few Linux aware colleagues."
    author: "KDE user"
  - subject: "Re: THE single most impressive new thing from aKad"
    date: 2004-09-05
    body: "<em>on the other hand, gnome is beating kde on the look and feel issue and the intergration issue, also, gnome is using khtml.\n\nso in the two years I have been using linux, the two desktop environments have changed their positions completely. If I was a newbie again, I would pick gnome. wierd.</em>\n\n?\n\nIf anything, KDE remains more consistent in the look and feel game IMHO, at least as far as reusability (KParts) and integration (DCOP).  OpenOffice is still too different from Mozilla which is too different from XMMS.  Each GNOME app can be themed differently, while with KDE you pick one great theme (Baghira in my case) and everything renders all the widgets and chrome for you instead of trying to find all the disparate themes in all the disparate apps.  Plus, thanks to GTK-QT, my GNOME2 apps that behave properly look pretty good too, particularly Firefox and GIMP2.  However, you still have laggards like OOo that are _ugly_, though I haven't source-built it in awhile.\n\nKopete, Apollon, Juk are individually damn fine apps, but with integration into KDE they're synergistically superior.  Also, KDE is stealing some really great ideas from OS X (KWallet, KAddressBook), so when the Xorg lower-level stuff improves to the OS X display quality level, KDE is closest IMHO to approaching OS X in usability.\n\nOTOH, KOffice still freezes in 100% cpu when I try to read in a Word2k file.\n\nI continue to stick with KDE, even with some of the political annoyances."
    author: "Otis Wildflower"
  - subject: "[OT] linuxformat awards"
    date: 2004-09-03
    body: "http://www.linuxformat.co.uk/awards/\n\nvote!"
    author: "anon"
  - subject: "Re: [OT] linuxformat awards"
    date: 2004-09-03
    body: "The choices are a joke, right? GNOME but no KDE for best desktop software? Evolution but no Kontact for Office? GAIM but no Kopete for Internet?"
    author: "Anonymous"
  - subject: "Re: [OT] linuxformat awards"
    date: 2004-09-03
    body: "*shrug*.. there are still plenty of KDE-related choice, but in weird places. Vote away!"
    author: "anon"
  - subject: "Re: [OT] linuxformat awards"
    date: 2004-09-03
    body: "Best Desktop Software:\nThe Gimp 2\nGnome\nK3B\nKonqueror\nMplayer\nSlackware\nSUSE\n\nLol! WTF??"
    author: "John Options Freak"
  - subject: "Re: [OT] linuxformat awards"
    date: 2004-09-03
    body: "I prefer suse to mplayer. The icons are just nicer."
    author: "Fred Sch\u00e4ttgen"
  - subject: "Re: [OT] linuxformat awards"
    date: 2004-09-03
    body: "LOL"
    author: "Andre Somers"
  - subject: "Re: [OT] linuxformat awards"
    date: 2004-09-06
    body: "> The choices are a joke, right?\n\nI think this may be the one I just laughed at as a joke last year. For instance here is what I have to choose from for best development tool...\nAnjuta - no\nEclipse - great software but not my style\nEmacs - same as above\ngcc - I could not do without gcc, but I don't see it competing with Kdevelop\nKdevelop - best for application development\nQuanta - best for web development, but then I'm biased ;-)\n\nSo if I vote for Quanta, my software, I vote against Kdevelop, what my software is developed in, and ignore the fact that I would have neither without gcc, which Eclipse would get around only because there is no Java toolkit here. Anjuta lacks maturity and Emacs also requires gcc so it's down to a Java based development tool and a compiler by the criteria set in the survey.\n\nAlso one could make anti GNOME assertions too, such as there are no GNOME web development tools listed, even if Quanta may be preferred they are older software. If this isn't a joke then some people don't know when they are being funny. It would be more fun to guess the category that holds SUSE, GNOME and K3B. Can you guess? Most confused poll... ;-)\n"
    author: "Eric Laffoon"
  - subject: "3.4 !!!!!"
    date: 2004-09-03
    body: "I am so glad to see that the devs opted to go for a 3.4 release ! I guess they will split the development in two parallel branches: 3.4 will have new features that don't need Qt4, and the rest of the new features, will go to a different branch where comatibility is broken.\n\nIs that right ? Or is the 4.0 development going to take off AFTER kde 3.4 is released ?"
    author: "MandrakeUser"
  - subject: "Re: 3.4 !!!!!"
    date: 2004-09-03
    body: "As long as there is not any complete Qt4 beta, nothing can be done in a direction of a KDE 4.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Thanks!"
    date: 2004-09-03
    body: "I was given the oppertunity to visit the User Conference part, and I have to admit that i was very impressed. Not only by the talks and presentation, but also by the developers attitude.\nDuring the weekend I didn't hear one of the useless discussions \"Our desktop/OS is better than the other desktop/OS\". It seems the the developers are clear aware of the pro's and con's of KDE, and that they are focusing on improving KDE rather then run something else down.\n\nIt was also interesting that there seem to be a lot a people around who really care about the architecture of KDE, and are not only keen on implementing new features. There also seem to be some very smart guys in the KDE project.\nThe weekend was very encouraging to start contributing.\n\nI would like to thank the KDE Team for this great Event, it was really worth the visit!\n\nPJ"
    author: "PJ"
  - subject: "Re: Thanks!"
    date: 2004-09-06
    body: "> I was given the oppertunity to visit the User Conference part, and I have to admit that i was very impressed. Not only by the talks and presentation, but also by the developers attitude.\n> During the weekend I didn't hear one of the useless discussions \"Our desktop/OS is better than the other desktop/OS\". It seems the the developers are clear aware of the pro's and con's of KDE, and that they are focusing on improving KDE rather then run something else down.\n\nIt's nice you would say this, but I guess I'm having difficulty with why anyone would think this is all that surprising. Did you hear this type of talk from other developers? I think most of the trash talk is from people who aren't actually doing anything but trash talk. If you can actually do something to improve your software then you do. Logically KDE developers are internally motivated because they are most familiar with KDE. For instance I'm not familiar enough with the counterparts to my software to run it down and I'm also not running short on ideas. Besides Quanta is winning awards... so why would I want to play schoolyard games? ;-)\n\n> It was also interesting that there seem to be a lot a people around who really care about the architecture of KDE, and are not only keen on implementing new features. There also seem to be some very smart guys in the KDE project.\n\nI've always been attracted to the \"smart guys\" in the project, even if most are half my age. Most of the discussions here and elsewhere about whether the developers care about obviously important aspects are based upon the wild imagination of users who have not yet learned that so much interest and effort could be directed into positive contributions. Developers understand better than anyone what enables the functionality in KDE.\n\n> The weekend was very encouraging to start contributing.\n\nGreat!\n \n> I would like to thank the KDE Team for this great Event, it was really worth the visit!\n\nI'm glad you enjoyed it. It was a lot of fun to be there as a developer too. ;-)"
    author: "Eric Laffoon"
---
September 1, 2004 (The Internet) - The KDE Project is pleased to announce the successful completion
of the KDE Community World Summit ("<a href="http://conference2004.kde.org/">aKademy 2004</a>") in
Ludwigsburg (Germany) taking place from August 20th to 29th.



<!--break-->
<p>With more than 230 KDE core developers, usability and accessibility
experts, translators, editors and artists participating, the event is expected to have a huge and lasting
impact on the next major releases of the leading Linux and Unix desktop environment.
In addition, 270 visitors from the KDE user base and from other Free Software projects brought
the total number of attendees to 500. The international participants, coming from 5 continents, took
part in 65 <a
href="http://wiki.kde.org/tiki-index.php?page=Talks+@+aKademy">talks</a>, 10 full-day <a
href="http://conference2004.kde.org/tutorials.php">tutorials</a> and numerous <a
href="http://conference2004.kde.org/sched-marathon.php">BoF-meetings</a> over the course of 10 days.
Thanks to this huge turnout and the numerous activities, the event evolved into the largest conference
ever held that focused on a single open source desktop environment.</p>
<p>
The schedule consisted of six parts: <a
href="http://wiki.kde.org/tiki-index.php?page=KDE+ev+meeting+2004">KDE e.V. meeting</a>, <a
href="http://conference2004.kde.org/sched-devconf.php">Developer Conference</a>, <a
href="http://accessibility.kde.org/forum/program.php">Unix Accessibility Forum</a>, <a
href="http://conference2004.kde.org/usabilityforum/uakademy_schedule.html">Usability Forum</a>, <a
href="http://conference2004.kde.org/sched-marathon.php">Coding Marathon</a> and <a
href="http://conference2004.kde.org/sched-userconf.php">User Conference</a>.

<p>
<a href="http://ktown.kde.org/akademy/">Video and audio streams</a> from the
<em>Developer Conference</em> (August 21st to 22nd) are available for public consumption, thanks to
the magnificent support of <a href="http://www.fluendo.com/">Fluendo</a>, who provided the
technology and manpower to record all of the talks. A large number of detailed write-ups were contributed
by event attendees and have been published on the <A
href="http://wiki.kde.org/tiki-index.php?page=Talks+@+aKademy">KDE Wiki</a> website.

<p>
During the <em>First Unix Accessibility Forum</em> (August 22nd to 23rd) developers and disabled
people met to improve and advance assistive technologies for Unix. The following projects and
companies contributed with talks and presentations to the outstanding success of the Accessibility
Forum: <a href="http://accessibility.kde.org/">KDE Accessibility Project</a> (organizer), <a
href="http://www.trolltech.com/">Trolltech</a> (sponsor), <a href="http://www.ibm.com/">IBM</a>, <a
href="http://www.sun.com/">Sun Microsystems</a>, <a href="http://www.baum.de/">BAUM Retec AG</a>, <a
href="http://developer.gnome.org/projects/gap/">GNOME Accessibility Project</a>, <a
href="http://accessibility.freestandards.org/">FSG Accessibility Workgroup</a>, <a
href="http://www.suse.com/">SUSE</a> / <a href="http://www.novell.com/">Novell</a> and <a
href="http://see-by-touch.sourceforge.net/index_.html">SeebyTouch Project</a>.

<p>
With usability becoming increasingly important for the broad success of Linux and other Open Source
Software on the desktop, the <em>Usability Forum</em> (August 24-26) discussed, demonstrated and
refined the means by which KDE can effectively and seamlessly integrate usability into its everyday
development process. The Usability Lab set up and run by experts from <a
href="http://www.relevantive.de/">relevantive AG</a> and their professional support resulted in many
instant changes to the KDE source code that will significantly advance the user experience in KDE
3.4.

<p>The <em>Coding Marathon</em> saw the evolution of a great many ideas which will make it into upcoming
3.4 and 4.0 releases of KDE. Several completely new projects were also started during the event.
One of the most remarkable ones
is the <A href="http://www.kalyxo.org/twiki/bin/view/Main/FreeNX/">FreeNX/kNX</A> initiative, which
will bring high speed Terminal Services to KDE, that will connect multiple Operating System
platforms and make Linux desktop migration plans significantly easiert to implement.</p>

<p>The <em>User Conference</em> featured the first public announcement of another big public
sector organization preparing its <a href="http://www.heise.de/newsticker/meldung/50491">move to the
Linux OS platform</a>: all 69 local tax offices in Lower Saxony and their central office in Hannover
will be migrating the 12.500 desktop workstations of their clerks and employees to the KDE Desktop
Environment, utilizing the <A href="
http://extragear.kde.org/apps/kiosktool.php">Kiosk Mode</A> enterprise desktop features of KDE.</p>

<p>
In addition to general KDE users and developer community, representatives from the <A
href="http://www.skolelinux.org/">SkoleLinux</a> and <A href="http://www.gnome.org/">Gnome</a>
projects were invited and took part. They helped to further improve close cooperation and
mutual interoperability between these projects.

<p>
Photos of the event are available <a href="http://wiki.kde.org/tiki-index.php?page=Pictures+%40+aKademy">here</a>.

<p>
The KDE team would like to thank the <A href="http://www.ludwigsburg.de/">City of Ludwigsburg</A>
and the <a href="http://opensource.region-stuttgart.de/">Wirtschaftsf&ouml;rderung Region Stuttgart
(WRS)</a> for hosting the event. Thanks to the City of Ludwigsburg our first social event featured a
delicious buffet. We especially appreciated the hospitality of the <a
href="http://www.filmakademie.de/">Filmakademie</a> -- they were extremely supportive and hosted our
coding marathon. This meant keeping the doors open for the computer labs for 24 hours a day, on 10 consecutive
days, so that our programmers and contributors could work freely and unrestricted some of whom
wouldn't even attempt to adapt to their jet-lags.

<p>
The <A href="https://www.cccs.de/">CCC
Stuttgart</A>, <A href="http://www.space.net/">Space.NET</A>, <a href="http://www.BelWue.de/">BelWue</a> and <A
href="http://LF.net/">LF.net</A> have been an invaluable help to construct a reliable high
bandwidth network and internet connection, providing hardware and connectivity.
Their high level of volunteer organization and work was
crucial to the overall success of the event: you girls and guys rock! <A
href="http://www.linuxnewmedia.com/">Linux New Media</A> and <a
href="http://www.basyskom.de/">basysKom</a> helped us in an unbureaucratic and extensive way in
all matters. KDE developers, users and outside visitors showed a lot of interest in
the available KDE merchandise. We would like to thank <A href="http://www.opensourcefactory.com/">Open
Source Factory</A> and <A href="http://www.kernelconcepts.de/">Kernel Concepts</A> for satisfying
those needs.

<p>
Furthermore we would like to thank the <A
href="http://www.stuttgarter-zeitung.de/page/detail.php/16780">Blue Angel/Blauer Engel</A> for food
price reduction. <a href="http://www.ibm.com/linux/">IBM</a> sponsored <a
href="http://developer.kde.org/~binner/aKademy2004/day1/dscf0039.jpg"><i>"Free Lunch for Free
Software Developers"</i></a> during the first weekend -- that was a very popular benefit for the hungry
crowd! Thank you, <a href="http://www.hp.com/">HP</a>, for making available notebooks to our Free
Software and KDE developers for a reduced price! Plus, your notebooks received an excellent rating by
our expert Linux users who in the course of the week installed various Linux distributions onto the
harddisks.

<p>
Special thanks go to the volunteer people running the all-night shuttle-service from the Filmakademie to
the Ludwigsburg Youth Hostel. Their service was really extremely well appreciated by all those who
avoided 45 minute walks while the German rain was pouring down, as well as those who were simply too
tired after coding for 16 hours, at 4 o'clock in the morning.

<p>
And of course a big thank you to <a href="http://www.kde.org/areas/kde-ev/">KDE e.V.</a>, all the
KDE people and other volunteers for making this year's KDE Community World Summit such a great,
ground-breaking and successful event!

<p>
Many thanks go to our sponsors! Platinum Sponsor: <a
href="http://www.hp.com/linux/">Hewlett-Packard</a>, who provided us with more than 50 excellent,
Linux-optimized notebooks for the event! Gold Sponsors: <a href="http://www.ibm.com/linux/">IBM</a>,
<a href="http://www.suse.com/">SUSE</a> / <a href="http://www.novell.com/">Novell</a> and <a
href="http://www.trolltech.com/">Trolltech</a>. Bronze Sponsors: <a
href="http://www.erfrakon.de/">erfrakon</a>, <a href="http://www.nomachine.com/">NoMachine</a>, <a
href="http://www.parity-soft.de">Parity Software</a>, <a href="http://www.xandros.com/">xandros</a>,
<a href="http://www.intevation.de/">Intevation</a>, <a
href="http://www.science-computing.de/">science+computing</a>, <a
href="http://www.danka.de/">Danka</a>, <a href="http://www.linux-ag.de/">Linux Information Systems
AG</a> and <a href="http://www.linuxland.de/">LinuxLand</a>.
<p>
Printer Sponsor: <a href="http://www.danka.de/">Danka</a>. Book Sponsor: <a
href="http://www.oreilly.com">O'Reilly</a>. Compile Kluster Provider: <a
href="http://www.transtec.de/">transtec</a>. Usability Lab Provider: <a
href="http://www.relevantive.de/">relevantive</a>. Media Partner: <a
href="http://www.linuxnewmedia.com/">Linux Magazine</a>. Streaming Partner: <a
href="http://www.fluendo.com/">fluendo</a>. Organizing Parties and Main Contributors: <a
href="http://www.kde.org/areas/kde-ev/">KDE e.V.</a>, <a
href="http://opensource.region-stuttgart.de/">Wirtschaftsf&ouml;rderung Region Stuttgart</a>, <a
href="http://www.linuxnewmedia.com/">LinuxNewMedia AG</a>, <a
href="http://www.basyskom.de/">basysKom</a>, <a href="http://www.erfrakon.de/">erfrakon</a>, <a
href="http://LF.net">LF.net</a>
Further Contributors: <a href="http://www.ludwigsburg.de/">City of Ludwigsburg</a>, <a
href="http://www.credativ.com/">credativ GmbH</a>, <a
href="http://www.klaralvdalens-datakonsult.se/">Klar&auml;lvdalens Datakonsult AB</a>, <a
href="http://www.linuxtag.de/">LinuxTag e.V.</a> and <a
href="http://www.sourcextreme.com/">SourceXtreme Inc.</a>.

<hr>
<p>
Press Release: Written by Torsten Rahn
<br>
Contact: info@kde.org
<p>
Trademarks Notices. KDE, K Desktop Environment and KOffice are trademarks of KDE e.V. Incorporated.
Linux is a registered trademark of Linus Torvalds. UNIX is a registered trademark of The Open Group.
All other trademarks and copyrights referred to in this announcement are the property of their
respective owners.
<p>


