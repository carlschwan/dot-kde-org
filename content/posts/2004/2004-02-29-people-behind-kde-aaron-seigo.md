---
title: "The People Behind KDE: Aaron Seigo"
date:    2004-02-29
authors:
  - "Tink"
slug:    people-behind-kde-aaron-seigo
comments:
  - subject: "Thanks Aaron"
    date: 2004-02-29
    body: "Hi\n\nThanks for the interview. I think Aaron is one of the interesting people \n(another one is Eric of Quanta fame) in the KDE team who is always ready to be involved in responding to people. This is what a community project really needs. Thanks Aaron.\n\nregards\nRahul Sundaram"
    author: "rahul sundaram"
  - subject: "Re: Thanks Aaron"
    date: 2004-02-29
    body: "> Thanks for the interview. I think Aaron is one of the interesting people \n\nMe too.\n\n> (another one is Eric of Quanta fame)\n\nMe?! Uh-oh... My blathering attempt not to make a fool of myself is coming up soon. Hope you like it. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: Thanks Aaron"
    date: 2004-03-01
    body: "Hi\n\nYes. I am awaiting more killer features from quanta. dont get us disappointed. we really need to make those front page and dreamweaver users feel jealous\n\n;-)\n\nregards\nRahul Sundaram"
    author: "Rahul Sundaram"
  - subject: "Re: Thanks Aaron"
    date: 2004-03-01
    body: "Oh, we don't have to \"request\". Quanta is a mature application. It is nice to see it gain even more functionality. Quanta will target the professional web designer \"market\". Eric has done so much and gained a legendary fame.\n\nHow to find out someone was a good web designer? Look at his homepage, if it sucks hire him...\n\nQuanta's homepage shows that they are very busy with their work. But perhaps someone else could do it for them and improve the web site. A a reward for the wonderful quanta editor."
    author: "andre"
  - subject: "Brrrrr...."
    date: 2004-02-29
    body: "\"He is from Cowtown, in The Great White North: Canada's own Aaron Seigo!\"\n\nMy goodness, you must have been cold over the past few weeks Aaron. How do you stop your computer from seizing up?"
    author: "David"
  - subject: "Re: Brrrrr...."
    date: 2004-02-29
    body: "His computer running Linux with KDE, Can't get hotter that, can you?"
    author: "Abe"
  - subject: "aaron 4 prez"
    date: 2004-02-29
    body: "Aaron is great. I've seen him help a lot of people on user support channels like #kde. Very few developers as busy as him do that. Thanks Aaron!"
    author: "anon"
  - subject: "Psychometrics?"
    date: 2004-02-29
    body: "Relative humidity? I know it gets dry in Calgary, but...\n\nScotch and Kernel Traffic is indeed a very dangerous combination. \n\nLoved the interview.\n\nDerek"
    author: "Derek Kite"
  - subject: "Socializing"
    date: 2004-02-29
    body: "> But I have yet to meet a single other KDE developer offline.\n\nPerhaps this is good? Perhaps you would be frightened and stop working on KDE if you meet some? :-)"
    author: "Anonymous"
  - subject: "Re: Socializing"
    date: 2004-03-02
    body: "or vice versa? ;-)"
    author: "Aaron J. Seigo"
  - subject: "Good interview-- only one disappointment (maybe)"
    date: 2004-03-01
    body: "This is a nice interview.  I particularly liked Aaron's descriptions of the Pacific coastline of Canada.  I did find one of Aaron's statements a bit disappointing, though (if I'm understanding it correctly): \"Continuing this direction is good for KDE, good for our users and truly shows the world who the *real* Free desktop is.\"  Is this a dig at GNOME?  If so, the dig is disappointing.  *Both* desktops are free and are working together for free software.  There is no desktop war, just some healthy competition.\n\nSincerely,\n\nAndrew"
    author: "aigiskos"
  - subject: "Re: Good interview-- only one disappointment (mayb"
    date: 2004-03-01
    body: "I don't believe it's a dig at GNOME. I certainly didn't read it that way."
    author: "Joergen Ramskov"
  - subject: "Re: Good interview-- only one disappointment (maybe)"
    date: 2004-03-01
    body: "Oh for crying out loud! Some people who have vested interests in Gnome come out with comments ten times worse than this all the time, and who delight in turning the screw on KDE while trying to complement it in an extremely cowardly fashion. I won't name them, but you know who you are.\n\nWe get one decent KDE developer who says one sentence and it gets skewed into a 'disappointing' comment. Aaron is totally justified in making this comment, and he has never been the sort of person to do any sustained bad attacks on other projects.\n\n\"There is no desktop war, just some healthy competition.\"\n\nWell I think you want to be posting that elsewhere for 'some' people to read, but not here."
    author: "David"
  - subject: "Re: Good interview-- only one disappointment (mayb"
    date: 2004-03-02
    body: "David! How many times have I told you to quit trolling these message boards? Your father and I are going to give you a very red bottom for continuing to be such a loud idiot!\n\nLove,\nMom"
    author: "David's Mom"
  - subject: "Re: Good interview-- only one disappointment (mayb"
    date: 2004-03-02
    body: "Shouldn't you be in bed?"
    author: "David"
  - subject: "Re: Good interview-- only one disappointment (mayb"
    date: 2004-03-02
    body: "I would be interested to hear from Aaron what he meant, since I can't read it in a way that doesn't suggest the existence of a \"fake\" Free desktop. Either way Im sure its nothing worth getting one's shorts into a knot for. People invested in a certain technology are allowed a certain amount of emotional attachment to it.\n\nAs a fellow Calgarian, long time KDE and GNOME user, I'd love to take Aaron out for beer some time. Aaron: let me know if you want to grab a pint!\n\nCheers,\nRyan"
    author: "Ryan"
  - subject: "Re: Good interview-- only one disappointment (mayb"
    date: 2004-03-02
    body: "grab a pint? hell ya! =) \n\n(see how hard it is to convince me? ;)\n\ndrop me an email at my work addy (aseigo at seminolegas dot com) as i'm several days behind on my personal mail (which means many thousands of msgs.. gah!) due to having been away from the 'Net for the last three days as i completed my house move."
    author: "Aaron J. Seigo"
  - subject: "Re: Good interview-- only one disappointment (maybe)"
    date: 2004-03-02
    body: "the real Free desktop is one that is inclusive of all Free software rather than exclusive (and therefore divisive). while KDE is a key enabler, this extends beyond KDE and includes all the work being done out there by the numerous Open Source communities, from the kernels on up to end-user GUI apps such as Open Office.\n\ni know it may be a bit confusing to say \"KDE is (part of) THE Free desktop because it brings all the other bits together\", but IMHO that's the truth (granted we're not quite 100% there yet, but when we are...). a Free desktop offers choice without requiring the user to pay for that choice with diminished continuity. this has the pleasant side effect of giving everybody a place to play without requiring complete dominance by a single project."
    author: "Aaron J. Seigo"
  - subject: "Re: Good interview-- only one disappointment (mayb"
    date: 2004-03-02
    body: "Thanks Aaron.\n\nUnfortunately at the moment I don't think people can see beyond the 'one true desktop' stuff, either because they have vested interests or because they just cannot see it any other way. This is because people are used to a Windows world - \"Oh, if it isn't written in X, Y or Z nothing will work.\" Some also believe that we have the same scenario as the Windows/Mac incompatibility in the 80s and 90s, but this is just not the case. At the moment people are still fixed into this idea of 'everything' being written in C++, C, GTK, Qt, C#, Mono, wxWindows (wxWidgets?) etc. and some are trying to force that to happen. This is not practical simply because it has become apparent that no one on their own has the resources to maintain and develop something this large - it can only be done by many OSS/free software projects together. This approach and attitude is also a total dead-end, because > 95% of the world still runs other desktops. If KDE is popular in this scenario then I think we can proudly say that it is the most popular desktop on merit.\n\nI know some people were very sceptical of the freedesktop effort, me included, but I had a think and a good look around and I have rather changed my opinion. I believed this was possible months ago but dismissed it because I thought it would descend into political posturing. I hold my hands up, applaud Aaaron Seigo, Waldo Bastian, Matthias Ettrich and other KDE people contributing to this infrastructure. I also apologise to Daniel Stone who took the time to reply to some of my pointed postings a while back. Whatever the political motives people have or see freedesktop just makes sense, and if anyone tries to use it as a method of control it will collapse, simply because 'a lot' of open source/free software projects will need to be involved - more than there is currently. I don't think anyone need worry.\n\nI advise others, anybody involved with KDE (or anything remotely 'desktopy' development-wise - MPlayer, Xine, Open Office, wxWidgets, VMWare etc.), users or developers, to really look around freedesktop.org and imagine what you could do. I like Havoc Pennington's description of freedesktop technologies being a 'glue' between different toolkits and environments. Ironically, this is what happens on Windows. Presumably a GTK and Qt application can play happily together with Windows technologies being the 'glue'. I don't see any 'integration' complaints there, but conceptually it is difficult for people to picture in their minds because the Windows infrastructure is 'just there' and is immovable.\n\nWith freedesktop we all have the opportunity to make this 'glue' open. I have seen the derision of D-BUS etc. from all sides, but we must understand that if there is more things going to be done system-wide and working together then we simply cannot have DCOP or Bonobo or whatever absolutely everywhere. It gives us all the opportunity to advance the general standard 'UNIX' (not just for Linux but for BSD, Solaris etc.) infrastructure much further than has ever been done before. I think everyone will appreciate that.\n\nYes, I know freedesktop was going to be built on standards, and I'm sure it will be, but you cannot come to an agreement on standards unless you actually try things out i.e. come up with implementations. Makes sense? Goodness, I'll have to type this up some time.\n\nAaron has certainly cemented his place as a truly great thinker and contributor for KDE."
    author: "David"
  - subject: "Re: Good interview-- only one disappointment (mayb"
    date: 2004-03-27
    body: "of course Aaron is a great thinker.....it's the genetics  :)"
    author: "Sharon"
  - subject: "I actually think he is quite stupid."
    date: 2008-10-19
    body: "Being a programmer doesn't necessarily mean one is smart; just as being a plumber doesn't make one into mario.\nConsider adding shortcuts to desktop background. I rightclick on desktop, and whoah theres no \"add shortcut\" or \"add script\" or anything else like this. I search in google, and what i see? Aaron's video tutorial how to make shortcuts. Cant this guy see that mere fact that a video tutorial is made by one of developers to show users how to do something as trivial as adding desktop icon, that the mere existence of such video, by itself, proves the point that kde4.1 is unusable better than highest grade FUD?"
    author: "Dmytry"
---
At <a href="http://www.kde.nl/people"> The People Behind KDE</a> this week <a href="http://www.kde.nl/people/aaron.html">an interview with the man who represents</a> what working and contributing to a project like KDE stands for. He is outspoken, always helpful, has broad view of things that KDE needs and it's future, he is passionate about politics and social issues. He is from <a href="http://www.dragonridge.com/calgary/calgary1.htm" target="_blank">Cowtown</a>, in The Great White North: Canada's own Aaron Seigo!



<!--break-->
