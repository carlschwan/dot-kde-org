---
title: "The People Behind KDE: Ariya Hidayat"
date:    2004-04-19
authors:
  - "Tink"
slug:    people-behind-kde-ariya-hidayat
comments:
  - subject: "Top Man!"
    date: 2004-04-19
    body: "\"At the moment my playgound is KOffice (especially KSpread), I write some code, fix bugs and possibly introduce new bugs.\"\n\nKOffice development - well done!<br><br>\n\n\"So let me just mention (in no particular order): Excel export filter.\"\n\nMSOffice filters.... The best of luck on that.\n\n\"You should be able to use KDE technologies using any programming language, in any toolkit, in any imaginable way, without any difficulty\"\n\nI really couldn't agree any more with that statement. That's the future!\n\n\"Apparently they're nowhere in sight when I'm hexviewing these pieces of Excel documents.\"\n\nI admire this guy's bravery immensely. If he gets annoyed at any time, do cut him some slack for this."
    author: "David"
  - subject: "Who is Tink?"
    date: 2004-04-20
    body: "I know he/she does these interviews but who is it actually?"
    author: "a"
  - subject: "Re: Who is Tink?"
    date: 2004-04-20
    body: "<a href=\"http://www.kde.org/areas/people/tink.html\">\nhttp://www.kde.org/areas/people/tink.html</a>\n"
    author: "Anonymous"
  - subject: "Re: Who is Tink?"
    date: 2004-04-20
    body: "<sarcasm>Oh, very clear photo.</sarcasm>"
    author: "a"
  - subject: "Interview Nicolas Goutte"
    date: 2004-04-20
    body: "He does much work on KOffice but nobody knows him."
    author: "b"
  - subject: "Great read!"
    date: 2004-04-20
    body: "Once again, a very nicely done interview. Reading about all these interesting and nice KDE people may well be one of the best parts of being a KDE user ;-)\n\nGoran J\nVarberg, Sweden"
    author: "Goran J"
  - subject: "selamat!"
    date: 2004-04-20
    body: "Senang sekali mendengar ada anak bangsa jadi people of KDE. Lebih senang daripada ngeliat hasil pemilu :D"
    author: "obot"
  - subject: "Ph.D Student!"
    date: 2004-04-21
    body: "negh dia cth, bwt yang laen jgn cuman mejeng di patroli!!!!\nhe..he.. piss"
    author: "wyne"
  - subject: "Mr"
    date: 2004-04-21
    body: "Bangga banged liat sdr Ariya Become People Of KDE Team, :))\nKeep Your Good Work Bro !\nKami semua mendukung mu\n\nIndonesian IT Community\n"
    author: "nologin"
  - subject: "Re: Mr"
    date: 2004-04-22
    body: "Yup. Gue pribadi juga bangga banget. He..he..he.., om Ariya gimana sih caranya jadi team KDE. Gue jadi pengen juga nih  :)"
    author: "cybercat"
  - subject: "Re: Mr"
    date: 2004-04-23
    body: "http://www.kde.org/support/"
    author: "Ariya"
---
On <i><a href="http://www.kde.nl/people/">The People Behind KDE</a></i> this week an interview with an Indonesian hacker in Germany. Confused? But wait there is more! He describes himself as a bunch of 'ordinary itchy living cells', gets bored quickly, needs 'fresh blood to suck' but his friends think he's hilarious. Curious? Read more and get to know this week's star <a href="http://www.kde.nl/people/ariya.html">Ariya Hidayat</a>!



<!--break-->
