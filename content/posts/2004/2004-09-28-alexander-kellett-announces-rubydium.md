---
title: "Alexander Kellett Announces Rubydium"
date:    2004-09-28
authors:
  - "numanee"
slug:    alexander-kellett-announces-rubydium
comments:
  - subject: "Why competition?"
    date: 2004-09-28
    body: "Do we really need 10 different 90% solutions? \n\nWhy 'competition' with Java? http://www.classpath.org seems to be quite succesful. \n\nEasy RAD? Basic for Linux? Hbasic, Gambas? KDE bindings for Lazarus.freepascal.org\n\n\nIt is not worth to start 1000 different projects that fail because of incompleteness and the complexity trap."
    author: "gerd"
  - subject: "Re: Why competition?"
    date: 2004-09-28
    body: "Name one branch that is free of competition :)\n\nRinse"
    author: "Rinse"
  - subject: "Re: Why competition?"
    date: 2004-09-28
    body: "The way open source community development works[1]:\n\n5,000 projects started on individual computers.\n1,000 projects published (put online).\n100 projects pick up more than one developer or a handful of downloads.\n10 projects matures to a usable state.\n1 project matures to a polished, mature, packaged application.\n\nBut next week, 5,000 new projects startup.  Over the course of time a fantastic set of applications builds.\n\n[1] Note that not all open source projects are community developed, and some are in between like the Kompany's open source projects.  "
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Why competition?"
    date: 2004-09-28
    body: "\"It is not worth to start 1000 different projects that fail because of incompleteness and the complexity trap.\"\n\nKorundum is complete and complex, and it isn't a 90% solution. It's 100% serious, thankyou very much."
    author: "Richard Dale"
  - subject: "Re: Why competition?"
    date: 2005-01-12
    body: "cause java is not very useful!"
    author: "peter"
  - subject: "Ruby is lovely"
    date: 2004-09-28
    body: "I just played a bit with Korundum. Last year I learned Ruby and wrote some small text apps. I can't wait to get my hands dirty at a bigger KDE application using Ruby, be it with Korundum or Rubydium :-)"
    author: "wilbert"
  - subject: "Ruby in KDE"
    date: 2004-09-29
    body: "I applaud the effort, I figure a lot of people will complain that we already have Java, python and whatever other languages are out there with KDE bindings but the whole point of this is that people should be able to chose which one they prefer personally. End users won't care what programming language was used as long as the software written with it works and developers shouldn't mind because they will only need to know the language(s) they will be using, I see no problem with this.\n\nin a few years I figure there will be a scripting language chosen as an unofficial standard for KDE just as Python would seem to have been chosen as a standard by many distributions for Gnome, Ruby deserves a shot at that just as much as Perl, Python and Java do.\n\n"
    author: "ntws01"
  - subject: "Re: Ruby in KDE"
    date: 2004-09-29
    body: "> in a few years I figure there will be a scripting language chosen as an unofficial standard for KDE just as Python would seem to have been chosen as a standard by many distributions for Gnome, Ruby deserves a shot at that just as much as Perl, Python and Java do.\n\nWhat about Javascript? Many are adopting the capable KJS Embed. Personally I hope there is never a \"default scripting language\" chosen. Scripting language preferences are divisive and I see no reason to drive such a wedge in the community unless there is no alternative. If languages are DCOP enabled then they can interact and the DCOP interactions are already started in most applications so the scripting ability is there. All you need is the ability to create a GUI with that. All the language specific GUI projects like this can do that and Kommander can. The problem is whether the language has DCOP bindings, which some like KJS Embed do, but it can also work directly with the C++ architecture... which I feel is not exactly warm and fuzzy for the average user exploring scripting.\n\nThe point is, if you want to create small applications without having to program in C++ there are several interesting and capable options. If you're talking making a program scriptable and extending it's GUI then you have to consider how many languages we want to require users to learn to do basic logic functions and create GUIs. I think in that case is should be whatever they want and bind it together with DCOP. KDE comes in the language of your choice and scripting should too."
    author: "Eric Laffoon"
  - subject: "Re: Ruby in KDE"
    date: 2004-09-29
    body: "I think the name 'scripting language' is confusing. Although you can use Korundum for scripting purposes by driving a C++ app via DCOP, the real use I have in mind is a RAD environment for developing complete apps.\n\nWhereas KJSEmbed is a scripting language first, although it's becoming a RAD environment too. Once you've learnt the Qt/KDE api that knowledge is easily transferred to another programming language. So I don't see any advantage in having an 'Official' scripting language, and I don't think that's the KDE way of going about things.\n\nI was encouraged by the recent KCalc discussion. Roberto started by demonstrating a calculator in PyQt, and Zack responded by coming up with a Ruby version, followed by Eric with a Kommander one. And all three done it no time at all.."
    author: "Richard Dale"
  - subject: "Re: Ruby in KDE"
    date: 2004-09-29
    body: "A nice thing about Korundum is that it uses the Smoke library, and that's entirely auto-generated and works with multiple languages. Other bindings projects need a lot of work to update the bindings for every release. \n\nThe next thing to do for KDE 3.4 is to make Smoke more modular so that it can be used for implementing plug apis for KDE apps like Kontact/KMail/Kate/KDevelop. Meanwhile if Alex speeds ruby up with Rubydium, and KDevelop ruby support improves with stuff like ruby debugging and code completion, we'll really have an amazing RAD platform.\n\nJackson Miller has made a start on a PHP 5 binding, and I've been explaining how Smoke works to him recently."
    author: "Richard Dale"
  - subject: "Can't we just use python?"
    date: 2004-09-29
    body: "KDE should be Python Powered!"
    author: "Anthony Tarlano"
  - subject: "Re: Can't we just use python?"
    date: 2004-09-29
    body: "Yawn. I'm bored by 'religious types' like yourself who always think everyone has to adopt the 'One True Way'. The Java JVM, or the Mono CLR at the expense of Parrot or whatever. KDE can be Python powered as well as being powered by all sorts of other interesting things too."
    author: "Richard Dale"
---
Not so long ago Richard Dale <a href="http://dot.kde.org/1095644599/">announced Korundum</a>, a RAD environment for KDE that makes developing desktop applications extremely fun and easy.  Now, another KDE developer has <a href="http://blade.nagaokaut.ac.jp/cgi-bin/scat.rb/ruby/ruby-talk/113779">announced Rubydium</a>, his efforts to bring Just-In-Time optimisations to the <a href="http://www.ruby-lang.org/en/">Ruby</a> runtime.  Could Ruby become a serious contender for KDE application development?  The combination of Ruby, KDE and a fast runtime could mean competition to Java and .Net on the Linux desktop.


<!--break-->
