---
title: "KDE 3.3 Release to Coincide with KDE Community World Summit"
date:    2004-05-10
authors:
  - "Anonymous"
slug:    kde-33-release-coincide-kde-community-world-summit
comments:
  - subject: "XML error"
    date: 2004-05-10
    body: "XML error on the planned features pages!"
    author: "Lou"
  - subject: "Re: XML error"
    date: 2004-05-10
    body: "Sorry for that, I corrected in the same minute I noticed..."
    author: "Andras Mantia"
  - subject: "Re: XML error"
    date: 2004-05-18
    body: "I'm still seeing:\n\nXML error: not well-formed (invalid token) at line 1293\n"
    author: "TXLogic"
  - subject: "Re: XML error"
    date: 2004-05-18
    body: "Indeed, the document had still a XML syntax error.\n\nI have just fixed it.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: XML error"
    date: 2005-01-01
    body: "I cant seem to save one of my MSN chat log, it is in XML form, only one person, the rest seems no problem) The error message said.. \"Invalid Characters in Line 4....\" What does that mean and how do I fix it? Help...\n\nThank you\nPrast"
    author: "Prast"
  - subject: "Quality Feedback Agent"
    date: 2004-05-10
    body: "Does anyone have more details on the Quality Feedback Agent? Will it be similar to mozilla's talkback?"
    author: "AC"
  - subject: "working pdf viewer in 3.3?"
    date: 2004-05-10
    body: "I hope that there will be a working pdf viewer in kde-3.3. With kghostview I can't print about 80% of my pdfs (bug 61922 \"null setpagesize\", open since 2003-07-31). And kpdf is unusuable in most cases (some graphics are upside-down, troubles with included fonts, hyperlinks are drawn as huge black boxes, very slow,...). I haven't noticed any improvements in the last half year. Not everyone works with pdfs every day, but if you do, you still need acroread...\n\nDaniel"
    author: "Daniel Frein"
  - subject: "Re: working pdf viewer in 3.3?"
    date: 2004-05-10
    body: "looks like they need people for kpdf. please apply :-)\n\nbtw. very nice , congratulations for that fast release cycle.\n\nafter 3.3 is there a major rewrite for qt 4.0 ?\n\nchris."
    author: "chris"
  - subject: "Re: working pdf viewer in 3.3?"
    date: 2004-05-11
    body: "I can recall when kghostview was not working for me too because of the pagesize-bug, but I thought this bug was history...\nProblems with kghostview may also originate in ghostview itself. I'm using gs 7.05.6 over here and have no problems with viewing pdfs in kghostview at all.\n(Though I can confirm that kpdf is unusable at its current state)\n\nIf you have a lot of encrypted pdfs ghostview may also fail. Do you notice any differences when opening the pdf with gv?"
    author: "Thomas"
  - subject: "Re: working pdf viewer in 3.3?"
    date: 2004-05-11
    body: "I always thougt that this bug was directly related to kghostview. But you're right, it is located somewhere in gs. The postscript files created by kghostview, gv and pdf2ps are identical. I'm using gs-gpl 8.01 and gs-common 0.3.5 (debian sid). \n\nDaniel"
    author: "Daniel Frein"
  - subject: "Re: working pdf viewer in 3.3?"
    date: 2004-05-11
    body: "Among the kviewshell components kdvi has of late gained \nmajor performance boost.  Can't these improvements be \nmade to other components as well ? "
    author: "Asokan"
  - subject: "Re: working pdf viewer in 3.3?"
    date: 2004-05-11
    body: "Or better yet move everything to/as gv-backend (fork for kde) and then make all apps like kpdf/kghostview/kdvi use it (until merged in to a single frontend for kde-4.0)? ;-)"
    author: "Nobody"
  - subject: "Usability"
    date: 2004-05-10
    body: "Dear Friends, I love KDE, it is my only DE. I think the internal architecture\n(in part thank to Qt) is just outstanding. This is clear when you see how well the component model works (things like DCOP and all)\n\nThe 2 fronts where I think KDE can get stronger are Usability and Consolidation. \n\nUSABILITY: It would be great to prioritize the Usability project,really. Some things can be much more user friendly:\n\n1) Config menues: add consistently (through the apps) an \"advanced options\" button, and just leave a few relevant options for the \"regular user\". As an example, take the config dialog in Konqueror, there are so many options that it is overwhelming. It takes me quite a bit of time to navigate through them and I am a nerd ;-)\n\n2) File associations: they really are hard to set up if you don't know exactly what to do. There is no waay to fall back to \"defaults\" for instance. This is another functionality that should always be available I think: a \"Defaults\" button. \n\nCONSOLIDATION: Some areas are nicely coordinated (Office, recently PIM, etc). But things like multimedia need some consolidation. There are several audio apps, some of them overlap, some of them are hardly useful, I think that some integration and consolidation is needed. There should be one killer Photo program (digikam for me ;-), one killer media player, etc. I end up using Noatun for some things, real player for some others, KMplayer for others ... it's all good but integration would be better :-)\n\nJust my 2cts thinking of the future. And yes, I love KDE, and  I find it better than any other GUI I ever used, including windows :-) (never tried MacOS)"
    author: "MandrakeUser"
  - subject: "Re: Usability"
    date: 2004-05-10
    body: "<I>1) Config menues: add consistently (through the apps) an \"advanced options\" button, and just leave a few relevant options for the \"regular user\". As an example, take the config dialog in Konqueror, there are so many options that it is overwhelming. It takes me quite a bit of time to navigate through them and I am a nerd ;-)</I>\n\nMany configurable options aren't a real problem IMO, as long as the options that matter are in the first few screens. AND the options are linked to that program only.\n\nE.g., what I would like to see is a distinction between the Konqueror File Manager configuration and the Konqueror Webbrowser configuration. The Control Center allready has this distinction, it would be better IMO if the app itself had this too."
    author: "JeroenV"
  - subject: "Re: Usability"
    date: 2004-05-11
    body: "If KDE is going to be taken seriously by the majority of business users then things do need to be simplified for the average user, and that does mean hideing advanced options that only IT professionals use. This could mean that there are specific tools for configuring advanced options (other than a text editor on the configuration files)."
    author: "Ian Whiting"
  - subject: "Re: Usability"
    date: 2004-05-13
    body: "If business users take IT seriously enough to employ a decent IT staff they are already today able to take advantage of KDE's high configurability which allows administrators to easily adapt KDE to specific use cases by fine tuning and locking down parts or all of KDE's user interface. The keywords are KXMLGUI and KIOSK. (And that all is completely independent of whatever KDE itself decides to do for simplifying the defaults for average users, something which shouldn't be of any concern to any serious IT staff anyway.)"
    author: "Datschge"
  - subject: "Re: Usability"
    date: 2004-05-10
    body: "If you're interested in talking about these issues more, you might consider getting into one of these two projects:\n\nKDE Quality Teams\nDo as much or as little as you want talking about both issues, either just in one module (e.g. talk with users and developers more about consolidation in kdemultimedia) or generally.\nhttp://quality.kde.org\n\nKDE Usability\nIt's quite high-traffic, but if you subscribe with digest mode on (as I have it) you get one or two e-mails a day, and can talk with other usability people and developers directly.\nhttp://usability.kde.org"
    author: "Tom Chance"
  - subject: "Re: Usability"
    date: 2004-05-11
    body: "To the Point Media-Player: Do you have tested Kaffeine? SuSE uses Kaffein as default player since 9.0, TurboLinux sells it as DVD Player. It plays nearly everything (based on libxine) and is great integrated in KDE/Konqueror (parts plugin) and other browsers (browser plugin) and the gui is much more powerfull then the lightweight KMplayer."
    author: "Manfred Tremmel"
  - subject: "no (ruby) bindings update? "
    date: 2004-05-10
    body: "I'm most interested in the ruby bindings. Won't it be released \"officially\" in the 3.3 packages?\n\nRaph"
    author: "rb"
  - subject: "Re: no (ruby) bindings update? "
    date: 2004-05-10
    body: "From what I've seen all of the bindings have come on leaps and bounds. I'd be surprised if Ruby wasn't in the 3.3 bindings release."
    author: "David"
  - subject: "Re: no (ruby) bindings update? "
    date: 2004-05-11
    body: "QtRuby and Korundum weren't built by default for KDE 3.2, but they will be for KDE 3.3. \n\nThe basic bindings for Qt/KDE are complete, and the rbuic tool works with KDE widgets. KDevelop has some ruby support; with ruby class browsing/syntax highlighting, and Marek Janukowicz has recently added a QtRuby template.\n\nHere's a list of a few further things to do (anyone like to help out?):\n\n- More code samples, particularly KDE ones\n- Qt Designer plugin\n- Kate ruby plugin\n- KDevelop Korundum project template\n- More documentation\n- Set up Korundum site on rubyforge with a release\n- Online tutorials\n- KDevelop Korundum project template, debugger front end etc\n- Extract KDE C++ doc comments from kde headers and convert to RDOC format\n- RubberDoc documentation browser (Alex Kellett)\n- Release Announcement (eg 'KDE Ruby bindings now official!')\n\nI'm sure a killer RAD environment isn't that far off..\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: no (ruby) bindings update? "
    date: 2004-05-11
    body: "Will the ruby bindings (Korundum) contain only stuff from kdelibs or will it also contain non-core libraries like libkcal? Or asked differently, how hard would it be to generate those bindings and ship them with an app?\n\nChristian"
    author: "Christian Loose"
  - subject: "Re: no (ruby) bindings update? "
    date: 2004-05-11
    body: "Korundum only contains stuff from kdelibs (and Qt). It is driven by the file kdebindings/smoke/kde/kde_header_list, and so you might just need to add the libkcal headers to that in order to add them to Korundum (and link libsmokekde against libkcal). That would allow you to try kcal ruby programming. But does it have any of its own namespaces? If so, they would need to be added the QtRuby runtime which would be extra work, although not much.\n\nThe current version of the SMOKE library is a bit monolithic, and so you either have to add classes to the Korundum extension or not at all. It isn't possible to have a kcal specific extension that could be used in conjuction with Korundum. This is because in SMOKE v1 each method is identified with a short integer. In SMOKE v2 the methods will be indentified with a URI which should make it more flexible."
    author: "Richard Dale"
  - subject: "Java bindings"
    date: 2004-05-11
    body: "I did a little experiment with the Java bindings yesterday, and I was quite impressed. It's actually possible to create a Qt app in Java, and compile that to native code with gcj. See my writeup at: http:/www.valdyas.org/fading/index.cgi/hacking/gcj.html?seemore=y."
    author: "Boudewijn Rempt"
  - subject: "Re: Java bindings"
    date: 2004-05-11
    body: "Just had a look - good work! Thanks for spreading the news that there are viable Free Software alternatives to Sun's jdk. I agree that gcj and the java bindings are a really good combination too - I think gcj is a really underrated project. \n\nNote that you can use the interpreter 'gij' that comes with gcj too. I've found that works really well with Qt or KDE java programs; they run fast because they spend most of their time in the kde or qt libs which are native code. \n\nYou can compile qtjava.jar to a library called 'lib-org-kde-qt.so' and gcj or gij will use that in preference to qtjava.jar. But I've found the libraries are a bit large relative to the speed up and perhaps it's more trouble than their worth."
    author: "Richard Dale"
  - subject: "Re: Java bindings"
    date: 2004-05-11
    body: "Yes, I first used gij to verify it all worked, and then slowly worked up towards an optimized binary... Is there a special reason for the naming of the shared library? I just named it qtjava.so..."
    author: "Boudewijn Rempt"
  - subject: "Re: Java bindings"
    date: 2004-05-11
    body: "You need to name the compiled qtjava.jar 'lib-org-kde-qt.so' so that gcj/gij can search for it when resolving 'import org.kde.qt.*' in your program. It just extends the idea of .jar files to include compiled .so libs. If the directory containing 'lib-org-kde-qt.so' is on your LD_LIBRARY_PATH, it is exactly equivalent to qtjava.jar being on your CLASSPATH.\n\nIf you name it qtjava.so you can only use the lib by naming it in a gcj compile command, and not as a qtjava.jar replacement as above."
    author: "Richard Dale"
  - subject: "Re: Java bindings"
    date: 2004-05-11
    body: "Do I understand that well? Does it mean that I could actually develop my app faster in java with gij and when I would be satisifed with the result, just let it through gcj? Or is there something else to know about this?\n\nZoltan"
    author: "Zoltan Bartko"
  - subject: "Re: Java bindings"
    date: 2004-05-11
    body: "Yes, you can use both gij and gcj, but I've found nearly all QtJava and kde koala java apps run quite fast enough using gij. I think you'd only need to compile to native code with gcj if your app is graphics intensive. On the other hand, with Swing nearly all the code is java, and so it makes much more difference whether or not you have a jit compared with just ordinary slow interpreted code."
    author: "Richard Dale"
  - subject: "Re: Java bindings"
    date: 2004-05-11
    body: "Yes, that's what I set out to discover. I have a vague intuition that applications somehow find easier acceptance if they're native code, instead of any kind of bytecode. Richard is right, though, for the actual performance there's not much difference, perhaps because qtjava uses JNI, and Mark Wielaard tells me that for every JNI call, gij is called to generate some bytecode, which is then executed. Things could be faster, he tells me, but I think it's plenty fast enough. Although I noticed that putting a simple QMainWindow on screen is a lot faster with qtjava compiled to native code using -O2, than it is without optimization or in bytecode.\n\nAlso, I've checked Richard's other remark -- about the name of the shared library compiled from a jar -- and I was told that the difference is not that when the shared library has a name that doesn't reflect the package is contains, the java jar is used, but that when it's named qtjava.so (for instance), the ordinary dynamic linker is used, but when it's named org-kde-etc.so, the bootstrap classload is used; but in both cases it's native code.\n\nAnyway, my next target is to find out how to create a library using java, compile it natively, and call that from C++. I think java is especially suited to appliation cores that handle data structures."
    author: "Boudewijn Rempt"
  - subject: "Re: Java bindings"
    date: 2004-05-11
    body: "I\u00b4m impressed :-)\n\nMore people should write like this, what they do, and what they find cool about KDE.\n\nI try to do it but I\u00b4m not doing much ;-)"
    author: "Roberto Alsina"
  - subject: "Good!"
    date: 2004-05-10
    body: "I think shorter release cycles like this (6-9 months max) are very good to keep the developer and user communities active. Keep up the good work!"
    author: "dizzl"
  - subject: "Re: Good!"
    date: 2004-05-11
    body: "They're good for the user community, and bad for 3rd party developers. A balance between the two is needed, and the right one is hard to find. Sadly, nothing worth while is ever simple."
    author: "Richard Moore"
  - subject: "Re: Good!"
    date: 2004-05-11
    body: "It's not that bad if binary bakwards compatibility is ensured, i.e. the app I wrote for 3.0 will still run under 3.3, no recompile. New releases every 6 months wouldn't bother 3rd-party-developers then. However, major releases (3.x->4.x) should not occur more frequently - 2 to 3 years in-between seems good. So then, we'ld have to get used to version numbers such as 3.6"
    author: "Annno v. Heimburg"
  - subject: "Re: Good!"
    date: 2004-05-11
    body: "Except that, as I understand it, KDE 3.3 will be the interim release before 4.0, giving KDE developers and users a chance to pack in lots of new application features and underlying refinements, while the developers plan for more extensive changes, e.g. moving from DCOP to DBUS.\n\nThough binary compatability may break, it should still be relatively easy to \"port\" code across to the new architecture, however, since it's a fair assumption that the technologies will remain backwards compatible (e.g. apps using DCOP will still work)."
    author: "Tom Chance"
  - subject: "Re: Good!"
    date: 2004-05-11
    body: "Yes and no. The API will be compatible, but apps that don't use the new features may look a bit dated in comparison to things that use the shiny new features. It's not a major problem, but it can irritate people."
    author: "Richard Moore"
  - subject: "Updates"
    date: 2004-05-10
    body: "I am hoping for the new kde-pim release sooner than later. The promised features like Groupware integration, HTML mail editing, etc. will push KDE over the top. \n\nI am also SO looking forward to the bugfixes like the kate bugs, the klipper freezing kicker, etc.\n\nBTW I like the shorter release cycle. Keep up the good work!"
    author: "Erich Vinson"
  - subject: "Re: Updates"
    date: 2004-05-10
    body: "\"I am hoping for the new kde-pim release sooner than later. The promised features like Groupware integration, HTML mail editing, etc. will push KDE over the top.\"\n\nYer. The groupware stuff looks incredibly good."
    author: "David"
  - subject: "Re: Updates"
    date: 2004-05-11
    body: "Ah yes, HTML mail editing, so that all KMail user's mails end up in my spam folder too. Lovely :)"
    author: "Kolbj\u00f8rn Barmen"
  - subject: "Re: Updates"
    date: 2004-05-11
    body: "All? It will not be the default."
    author: "Anonymous"
  - subject: "Qt Vs GTK look & feel"
    date: 2004-05-11
    body: "I like to see future releases of KDE include patches to give both Qt & Gtk apps same look & feel. Sometime I feel odd to see different many look & feel within same desktop env."
    author: "zammi"
  - subject: "Re: Qt Vs GTK look & feel"
    date: 2004-05-11
    body: "Or other way around with <http://www.freedesktop.org/Software/gtk-qt>?"
    author: "Nobody"
  - subject: "kdebase without X11-dependencies"
    date: 2004-05-11
    body: "\"Make kdebase compile (and run) without dependencies on X11\"\n\nAs far as I know, ksmserver needs X for session management functionality of KDE, and ksmserver is included in kdebase, I think.\n\nSame for DCOP, which I assume is part of kdebase and also depends on X.\n\nWhile I can imagine a KDE without session management, I can't in case of DCOP. Will DCOPs dependence on X be removed (by what=, or is DCOP replaced with DBUS in KDE 3.3? And how will the session management thing be handeled?\n\nOr am I completely wrong with my assumptions?\n"
    author: "MM"
  - subject: "Re: kdebase without X11-dependencies"
    date: 2004-05-11
    body: "Isn't it merely based on libICE? Sure, libICE is an X11 lib. Don't know if it's possible, but can libICE be replaced by something similar? Well, just guessing... maybe I'm talking nonsense"
    author: "Thomas"
  - subject: "Re: kdebase without X11-dependencies"
    date: 2004-05-11
    body: "No need. libICE doesn\u00b4t require X at all. It just comes with it, but you can use it in CLI apps or whatever just fine."
    author: "Roberto Alsina"
  - subject: "Re: kdebase without X11-dependencies"
    date: 2004-05-11
    body: "I knew about libICE but wasn't aware that though it is part of X, it does not depend on it. Thanks to both of you."
    author: "MM"
  - subject: "Icons..."
    date: 2004-05-11
    body: "> NEW IN KDE: Import Noia Icontheme....\n\nOh C'mon!!! That's like 14MB more to kdeartwork tarball!\n"
    author: "Norberto Bensa"
  - subject: "Noia is great"
    date: 2004-05-12
    body: "Noia is really an original and beautiful icnonset. I do not mind the increase at all."
    author: "Alex"
  - subject: "Re: Noia is great"
    date: 2004-05-12
    body: "You would if you were on dial-up"
    author: "Norberto Bensa"
  - subject: "Evolution Exchange connector"
    date: 2004-05-11
    body: "Will the open sourcing of the evolution EXchange connector impact on this? \n\nIs this usefull vor kontakt? "
    author: "geert"
  - subject: "Re: Evolution Exchange connector"
    date: 2004-05-12
    body: "KDE PIM already has groupworking parts, and I believe it has interactivity with Exchange."
    author: "David"
  - subject: "Connector for MS Exchange Server"
    date: 2004-05-12
    body: "I would like to see the newly GPL-ed 'Connector for MS Exchange Server' offered by Novell integrated into Kontact by Default. This would be sweet!\n\nThe source can be downloaded from:\n\nhttp://ftp.ximian.com/pub/source/evolution/ximian-connector-1.4.7.tar.gz"
    author: "thesimplefix"
  - subject: "Re: Connector for MS Exchange Server"
    date: 2004-05-12
    body: "See above. Kontact and KDEPIM already have groupworking and Exchange options."
    author: "David"
  - subject: "Re: Connector for MS Exchange Server"
    date: 2004-05-12
    body: "I know that korganiser had an Exchange plugin, but a similar technology for KMail - so that it can talk to Exchange 2000-2003 servers in their native protocol rather than using IMAP."
    author: "theboywho"
  - subject: "Re: Connector for MS Exchange Server"
    date: 2004-07-01
    body: "I agree. Kmail should be enterprise ready. That includes allowing users to connect to enterprise groupware servers using their native protocol. I don't like it any more than anyone else, but Exchange has a huge market share and shouldn't be over-looked. IMAP is a work-around but not a real solid one. Anyone who has had to support an Exchange server with many different clients using IMAP (myself) should agree that Exchange (2000) doesn't not handle IMAP very well. We have had a few corrupt mailboxes because Exchange choked when an irregular message was retrived via IMAP. The native protocol does not have this problem. Also, true calendar/out-of-office integration is demanded by our business model. Intergrating with Exchange may be troublesome, but if done right, would pay huge dividends in making the KDE desktop truly enterprise ready."
    author: "Bob"
  - subject: "Kicker drawer"
    date: 2004-05-13
    body: "I know, I know, I should just file a wishlist bug. I'm whining instead.\nThe one and only thing I've been missing since I defected from GNOME a while back is the panel drawer. I really hope a kicker developer feels the same way and eventually clones it. If not... I'll get around to learning Qt eventually."
    author: "Zack Cerza"
  - subject: "Network applet"
    date: 2004-06-20
    body: "I't would be greate if you added a standard applet that shows the speed and etc for the network like in windows 2000 and XP, I think gnome has one of those applets.\n\nKeep up the good work!"
    author: "Anders Jansson"
---
What an exciting KDE summer!  Release dude <a href="mailto:coolo AT kde DOT org">Coolo</a> is planning the KDE 3.3 release for just 3 days before "<a HREF="http://conference2004.kde.org/">aKademy</A>" starts: The <a href="http://developer.kde.org/development-versions/kde-3.3-release-plan.html">release plan</A> was published on Saturday for discussion! Developers should make sure to get the stuff listed they plan to have ready for 3.3 in the <a HREF="http://developer.kde.org/development-versions/kde-3.3-features.html">planned-features document</a> as soon as possible. KDE 3.3 Alpha is prepared around May 23rd and June 1st will see the first freeze (excluding outstanding listed features and i18n strings) kicking in.



<!--break-->
