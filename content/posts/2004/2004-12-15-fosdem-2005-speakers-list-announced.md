---
title: "FOSDEM 2005: Speakers List Announced"
date:    2004-12-15
authors:
  - "fmous"
slug:    fosdem-2005-speakers-list-announced
comments:
  - subject: "Great  !"
    date: 2004-12-17
    body: "I certainly won't be missing this one either :)"
    author: "Phil"
---
The <A href="http://www.fosdem.org/2005/index/news/speakers">speakers list</A> for FOSDEM 2005 has been released with many great names including some prominent KDE developers like Matthias Ettrich, Alexander Dymo and Harald Fernengel. This year you can again submit your questions for interviews to be published later on the <A href="http://www.fosdem.org/">FOSDEM website</A>. 




<!--break-->
<p>The 'Free and Open source Software Developers' European Meeting' is a two-day summit which will bring together leading developers in the Free Software community and will take place on the 26th and 27th of February in Brussels. </p>

<p>A number of KDE hackers will be meeting up with each other in the capital of Europe during this year's FOSDEM conference. If you want to be present too make sure you inform our <a href="mailto:kde-events-benelux@kde.org">KDE-FOSDEM crew</a>.</p>

