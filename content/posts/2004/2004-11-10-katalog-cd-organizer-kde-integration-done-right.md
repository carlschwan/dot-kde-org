---
title: "Katalog CD Organizer: KDE Integration Done Right"
date:    2004-11-10
authors:
  - "Phoenix"
slug:    katalog-cd-organizer-kde-integration-done-right
comments:
  - subject: "Ioslaves - great"
    date: 2004-11-10
    body: "It's great to see more use of ioslaves.  This is one of the most underappreciated features of KDE, and I'd love to see just about everything have an ioslave interface.  Email, contacts, todos..."
    author: "kundor"
  - subject: "Re: Ioslaves - great"
    date: 2004-11-10
    body: ">Email, contacts, todos...\n\n(imap|pop3|imaps)://<hostname>\n"
    author: "d w"
  - subject: "Re: Ioslaves - great"
    date: 2004-11-24
    body: "That's cool!  I never found that before.\n\nUnfortunately, though it shows my messages as \"Message 101\", etc, it doesn't allow me to open them to read (I get a \"maildrop session in use\" error.)  So it's not usable. :\\"
    author: "kundor"
  - subject: "Interesting!"
    date: 2004-11-10
    body: "Actually I am thinking about a way to organize my photo-cd/dvd:\nMy photos are organized in folders or to speak in digikam terms, in albums.\nKatalog seems to make it easy to have a local database that shows what DVD I must put in my drive to load a certain jpeg file.\n\nOk, but what happens if I edit my image or delete a file and I want to make a catalog of the same folder again?\nDoes Katalog use hash file information and knows 90% of my folder content is already on a disk? Does it support k3b and generate project files for differential backups?\n\nThis would be a great addon for digikams CD/DVD-export feature!\n\nBye\n\n  Thorsten "
    author: "Thorsten Schnebeck"
  - subject: "Belongs in the OS/FS"
    date: 2004-11-10
    body: "Katalog looks great.  As far as integrating with KDE the developer did superbly.  I would like to say though that in my [humble] opinion this is the wrong place to implement the nuts and bolts of a project like this.  The metadata belongs integrated with the file system.  With integrated metadata and something like views in databases this could be done in a much more efficient way. The same goes for ioslaves, they don't belong in the desktop environment they belong in the OS (i.e. userspace filesystems like FUSE).\n\nI'm not flaming here.  It's just my opinion from an overll system engineering point of view. "
    author: "nobody special"
  - subject: "Re: Belongs in the OS/FS"
    date: 2004-11-10
    body: "I believe that kde ioslaves can be mounted in FUSE, so in a sense they're already at the OS level.\n\nHowever, I agree that better userspace filesystem support is needed.  Unfortunately, all the projects like PerlFS seems to have died -- there doesn't seem to be a good way to write a filesystem in Perl that works with a 2.6 kernel as far as I can tell."
    author: "kundor"
  - subject: "Re: Belongs in the OS/FS"
    date: 2004-11-10
    body: "Its always been a goal of KDE project as a whole to work on a variety of platforms. \n\nBut I guess I basically agree, ioslaves should really be part of some OS standard like POSIX, it would make things so much easier if there was that level of network transparency system wide. Fuse is the right idea at least."
    author: "Ian Monroe"
  - subject: "Re: Belongs in the OS/FS"
    date: 2004-11-10
    body: "When having found a file/dir in a catalog, I'd like to click at\nit and have my OS show a requestor to insert the appropriate\nmedia (by name). And then access the file/dir from the point in\nthe directory tree where I hooked the media in.\nDreaming of old Amiga days... here it would fit 101 percent.\n"
    author: "Andreas"
  - subject: "Re: Belongs in the OS/FS"
    date: 2004-11-10
    body: "I wondered this myself. Isn't the GNOME desktop in the process of trying to duplicate this functionality? It would be great if I could use this stuff at the console without KDE or GNOME."
    author: "Scott Patterson"
  - subject: "Re: Belongs in the OS/FS"
    date: 2004-11-10
    body: "it would be\n\nhttp://kde.ground.cz/tiki-index.php?page=KIO+Fuse+Gateway\n\n:)"
    author: "standsolid"
  - subject: "One For the Records!"
    date: 2004-11-10
    body: "And, in what is possibly the first such occurrance in history:\n\n\"You can download a source code package or a precompiled package for Slackware 10.0. I hope you enjoy it.\"\n\nThat's right, the page has packages for Slackware but for no other distributions out there. No Debian, no Red Hat, no SuSE, no Mandrake, only Slackware."
    author: "jameth"
  - subject: "Re: One For the Records!"
    date: 2004-11-10
    body: "That might be because it's dead simple to make Slack packages.\n\nconfigure --prefix=/usr\nmake\nsudo make install DESTDIR=/tmp/pkg-katalog\ncd /tmp/pkg-katalog\ntar cfz ../katalog-1.2.3-i386-1.tgz .\n"
    author: "Jens B\u00e4ckman"
  - subject: "Re: One For the Records!"
    date: 2004-11-10
    body: "How is using checkinstall more difficult?"
    author: "Anonymous"
  - subject: "Re: One For the Records!"
    date: 2004-11-10
    body: "Not more difficult using checkinstall, but it does not produce particularly good results. The RPMS you create sometimes get really weird dependencies and usually they don't  install as cleanly as a correctly built RPM. Personally I stay away from RPM's created by checkinstall as nearly always create problems then they are worth. Checkinstall is a good tool to use when installing from source on a machine, allowing easy uninstall with RPM. But not as a tool for creating RMPS for distribution. "
    author: "Morty"
  - subject: "Re: One For the Records!"
    date: 2004-11-10
    body: "Weird. I thought checkinstall puts no dependency info whatsoever by default, except for what ldd reports.\n\nIf that's the case, I don't see how you could end getting weird dependencies."
    author: "Roberto Alsina"
  - subject: "Re: One For the Records!"
    date: 2004-11-10
    body: "Weird in the way it seems to depend rather heavy on what the creator of the RPM has installed. Sometimes you get dependency of some packages I find quite obscure, as in no real reason to depend on it for this program. \n\nThe most common case I have encountered are GUI programs ending up depending on files from the NVidia package, witch are bad news when you try installing on a machine with a different card."
    author: "Morty"
  - subject: "Re: One For the Records!"
    date: 2004-11-10
    body: "Since it got that info via ldd, that binary *does* depend on NVidia libraries.\n\nSince the NVidia libs should be binary-compatible with Mesa(1), it should work without them, though, but checkinstall can't guess when that happens and when it doesn't.\n\nIf you want to try your luck, just use --nodeps. If it doesn't work, remove it.\n\nThe reason why the slackware package works is simply that it doesn even try to check if the system has all requirements installed. It should fail 9 out of every 10 times on random computers, unless they are configured similar (or the app is very simple).\n\n\n(1) just guessing here."
    author: "Roberto Alsina"
  - subject: "Re: One For the Records!"
    date: 2004-11-11
    body: "> The reason why the slackware package works is simply that it doesn even try\n> to check if the system has all requirements installed. It should fail 9 out\n> of every 10 times on random computers, unless they are configured similar (or\n> the app is very simple).\n\nBelieve it or not, it is very possible to make a binary package that works on basically any linux distro (see \"Linux Distribution Independent-Binaries\" bit of http://kolourpaint.sourceforge.net/download.html) using the basically the slackware method but with the help of apbuild (http://www.autopackage.org/)."
    author: "Clarence Dang"
  - subject: "Re: One For the Records!"
    date: 2004-11-11
    body: "I am not checking right now, but wasn't the autopackage approach something like \"let's use dynamic libs, but ship them all together with the binary\"?\n\nIf that's the case, yeah, it should work. It should also be much more resource-intensive to run something like a KDE app packaged using autopakage, right?"
    author: "Roberto Alsina"
  - subject: "Re: One For the Records!"
    date: 2004-11-12
    body: "I haven't read up on the full blown autopackage format.  I'm just using their gcc wrapper, apbuild, which:\n\n1. removes false dependencies libtool leaves in (e.g. if my program depends on Qt which depends on pthreads, that doesn't mean my program depends on pthreads - it doesn't care how Qt is implemented)\n\n2. compiles against glibc 2.2 (so it'll run even on a very old system as long as the C++ ABI is the same as mine)\n\n3. statically links in a handful of libraries that you can't depend on people having in dynamic form e.g. Xrender (< 100K total)\n\nAnd then assuming you check your remaining dependencies (and they'll be few of them), you have a fairly portable binary.  IMO, this is the future of how 3rd party software will be distributed under GNU/Linux.\n\nThe experimental binary package I have up there is basically an apbuild job with a wrapper script (for setting KDEDIRS).  There are still a few gltiches I will iron out - disable RPATH, fix khelpcenter documentation (KDEDIRS isn't being picked up by kio_help unless you set it before KDE startup) and a BIC with KMainWindow::setCaption())."
    author: "Clarence Dang"
  - subject: "Re: One For the Records!"
    date: 2004-11-12
    body: "If your app uses QThread, it does require pthreads to work. Does this system recognize that?"
    author: "Roberto Alsina"
  - subject: "Re: One For the Records!"
    date: 2004-11-10
    body: "Guess what, source packages work on every distro, and many different operating systems.\n\nFor that matter, slackages work on all the distros you name anyway.  Just untar it to root."
    author: "Tycho Brahe"
  - subject: "Another approach, bookcase/tellico"
    date: 2004-11-10
    body: "For those who want a different approach, tellico (formerly known as bookcase), is a great KDE application, **very** customizable, and with support for different kinds of catalogs, collections, etc.\n\nhttp://www.periapsis.org/tellico/"
    author: "suy"
  - subject: "Thanks !!!"
    date: 2004-11-10
    body: "Many thanks to Phoenix for the review of my software and to the KDE Team for the best software I ever seen. Using the KDE technologies I was able to reproduce almost all the feature of other similar programs using one tenth of their lines of code.\n\nAbout the questions:\n\n>Ok, but what happens if I edit my image or delete a file and I want to make a >catalog of the same folder again?\n\nYou have to recreate the katalog.\n\n>Does Katalog use hash file information and knows 90% of my folder content is >already on a disk?\n\nNo :-(\n\n>Does it support k3b and generate project files for differential backups?\n\nNo, Now Catalog creation integrates nicely only in Konqueror (and in Konsole, thanks to DCOP interface). Anyway you can browse katalog from any ioslave-aware application.\n\n>The metadata belongs integrated with the file system\n\nI agree, but the current technology does not permit such an integration easily. Think to katalog as a more integrated solution than the stand alone applications such gtktalog and a less integrated solution than the FS support of meta-data.\nFurthermore I think that applications like Katalog are the only solution for removable media.\n\n>tellico \n\nThis is a great application which I use, but is for a different use. When you would to store the contents of the CD of your favourite Linux Magazine, Nothing is better that Katalog (or gtktalog, or cdcat, or ...)\n\n"
    author: "salvaste"
  - subject: "How about some binaries?"
    date: 2004-11-10
    body: "My kingdom for an rpm that I can use on MDK 10.1?\n\nWho will be the first to bring this wonderful app to us the unwashed but loving masses?\n\nthank you"
    author: "Joe Schelensinger"
  - subject: "very very cool"
    date: 2004-11-10
    body: "is there a place to report bugs/wishes? you could activate the guestbook on your altervista account for easy feedback...\n\nworks on normal dirs, fails on the desktop icon for device /dev/hdc. \n\nbugreport:\n\ndebian sid, kernel 2.6.8-ck9, xfs, udev + hotplug, kde 3.3.1 (almost), \"show devices on desktop\" enabled in kcontrol\n\nthe \"create katalog\" action asks for a name for the katalog, then correctly mounts the device and ...hangs. my cpu is at 100% and nothing happens untill i kill the katalog dcop call.\n\nit works flawlessly when operating on mounted /media/cdrom, same CD, same configuration.\n\nwishlist:\n\n* you could create a standard *.katalog repository, something like ~/.kde/share/apps(?)/katalog/ - so that just the name for the katalog is asked in the dialog (+ options of course) - it could be a katalog file itself, when renaming/removing items from catalogs is imlemented!\n\n* typing just \"katalog:/\" in konqui's location bar could show you all of the catalogs (or the default katalog file - read above) in the standard katalog repository for *easy* browsing\n\nthanks for the great app!\n\ncomplimenti, ti ho mandato sta roba anche via email per essere sicuro che l'avresti letta ;-)"
    author: "felipe"
  - subject: "Searching question"
    date: 2004-11-18
    body: "Can one search across multiple Katalogs for a given file?\n\nI use gtktalog for this now, and usually what I am doing is looking for on which disk I've archived something.\n\nAnyone?\n\nChris"
    author: "Chris Goldman"
---
Most people agree that <a href="http://www.k3b.org/">the K3b CD burner</a> is an application done right -- it looks good, is not overly complicated, and most of all "just works" -- the fact that Novell adopted it for Novell Linux Desktop is icing on the cake.  Well, the <a href="http://salvaste.altervista.org/">Katalog CD Organizer</a>, a possible cousin to K3b, is KDE <i>integration</i> done right. People may know of gtktalog and cdcat, both are fine applications; Katalog, however, is more of a <i>plug-in</i> for KDE. It works by <a href="http://static.kdenews.org/fab/screenies/katalog/katalog3.png">right-clicking</a> and <a href="http://static.kdenews.org/fab/screenies/katalog/katalog4.png">cataloguing</a> a directory or external media to a XML data file. There is a <a href="http://static.kdenews.org/fab/screenies/katalog/katalog1.png">katalog ioslave</a> for viewing katalog files as if they were native file structures. Delete, rename, and even the <A href="http://static.kdenews.org/fab/screenies/katalog/katalog2.png">find dialog</a> works on the files. This program is, in my opinion, a fine example of how a program can be integrated into, and built upon, KDE's strengths.







<!--break-->
