---
title: "KDE-CVS-Digest for July 9, 2004"
date:    2004-07-10
authors:
  - "dkite"
slug:    kde-cvs-digest-july-9-2004
comments:
  - subject: "[OT] Quanta CVS?"
    date: 2004-07-10
    body: "Does anybody know where Quanta CVS is now? CVSup only gives me empty directories, which is confirmed by \nhttp://webcvs.kde.org/cgi-bin/cvsweb.cgi/quanta/ ."
    author: "ac"
  - subject: "Re: [OT] Quanta CVS?"
    date: 2004-07-10
    body: "kdewebdev/quanta"
    author: "Anonymous"
  - subject: "Re: [OT] Quanta CVS?"
    date: 2004-07-11
    body: "Thanks."
    author: "ac"
  - subject: "khtml issues"
    date: 2004-07-10
    body: "I'd like to get some websites working in konqu, which aren't working atm (3.3beta), like\nparts of www.vodafone.de    (kjs hangs)\nwww.map24.de       (kde java classes incomplete, applet fails to load)\nwww.photo-druck.de (kjs hangs)\nwww.telefonbuch.de (mean dhtml stuff)\nthis annoying issue, where flash seems to stay always on top, even if it should be hidden e.g. by the dynamic parts of a site.\n\n(ducking and heading over to b.k.o)"
    author: "Thomas"
  - subject: "Re: khtml issues"
    date: 2004-07-10
    body: "I can see all those sites correctly, I think, no problem with all of them\n\nWhat version of kde are you using ? im using CVS from a week ago"
    author: "garaged"
  - subject: "Re: khtml issues"
    date: 2004-07-10
    body: "Can you see this one correctly?\n( http://lefti.blogspot.com/ ) \n\nIt's okay with Gecko, so I'm not sure if it's a problem with the website/blogger template or Konq."
    author: "Mikhail Capone"
  - subject: "Re: khtml issues"
    date: 2004-07-10
    body: "The W3C Validator (http://validator.w3.org/check?uri=http%3A%2F%2Flefti.blogspot.com%2F) says the page has 587 errors.\n\nSo maybe you can notify the author of the website to correct the errors."
    author: "Evil Homer"
  - subject: "Re: khtml issues"
    date: 2004-07-11
    body: "Seems like the site is using HTML 4.x,\nbut has a DOCTYPE of XHTML,\nso basically, it's using poor XHTML extension.\n\nThe thing is that Konqueror is \"correct\",\nbut the site author is using a web browser that \nignores DocType info and does it best to render,\nso the author doesn't or care if it doesn't work\non non-IE browser or non-netscape/mozilla."
    author: "fprog"
  - subject: "Re: khtml issues"
    date: 2004-07-10
    body: "wel, I know the khtml devs are very busy, and dont really feal like fixing broken webpages (in almost all cases, its the pagebuilders' mistake, and just because it worked on the broken IE, they often dont want to fix it \"because it works (only in IE, but thats enough for us)\". you should file a complait to them, its their mistake...).\n\nbut if you really want it to get fixed, you should find out what is wrong (eg what code is causing the problem) and generally to make it as easy as possible to fix it. then they might do so.\n\nthis is NOT meant to be rude or insensitive, its just that there are a few billion webpages out there, with all sorts of wrong code, which just happens to work on IE cuz that browser is full of mistakes too/because that browser can make up broken code quite well (depends on vieuwpoint huh?) and it is really impossible to fix em all. the ppl from macintosh working on khtml said the same, some time ago - they aren't gonna fix it either - that whould take far too much work, which they prefer to spend on making the browser compliant with the latest real W3C standards, like CSS2."
    author: "superstoned"
  - subject: "Re: khtml issues"
    date: 2004-07-10
    body: "> but if you really want it to get fixed, you should find out what is wrong\n> (eg what code is causing the problem) and generally to make it as easy as\n> possible to fix it. then they might do so.\n \nWell I tried to hunt it down a bit to file a bug report. Front-pages of all the above mentioned sites are o.k. (exept for telefonbuch.de). It's mostly some functionality of the site which is done using javascript and screws up kjs..."
    author: "Thomas"
  - subject: "Re: khtml issues"
    date: 2004-07-10
    body: "I tested with Konqi from CVS 20040706. All sites are working perfect, except the map24 one. But map24 has big problems in Firefox too."
    author: "Niek"
  - subject: "Re: khtml issues"
    date: 2004-07-11
    body: "\"I tested with Konqi from CVS 20040706. All sites are working perfect, except the map24 one. But map24 has big problems in Firefox too.\"\n\nThat one is O.K. here (at least the maps are displaying correctly and I can zoom and pan into them, didn't look any further). Konqueror 3.2.3, SUSE 9.1, java2-jre-1.4.2-129.\n\nJFL"
    author: "Jean-Fran\u00e7ois Lemaire"
  - subject: "Re: khtml issues"
    date: 2004-07-11
    body: "Blackdown-1.4.2-rc1\n\ncrashes on the applet.\n\nThat is with a recent cvs build, gentoo.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: khtml issues"
    date: 2004-07-12
    body: "$ java-config -v\njava version \"1.5.0-beta2\"\nJava(TM) 2 Runtime Environment, Standard Edition (build 1.5.0-beta2-b51)\nJava HotSpot(TM) Client VM (build 1.5.0-beta2-b51, mixed mode)\n\nAlso a Gentoo CVS build of Konqi. Can you test it with Firefox too?"
    author: "Niek"
  - subject: "LYOD?"
    date: 2004-07-10
    body: "What's that LYOD stuff? A replacement for libICE?"
    author: "AC"
  - subject: "Re: LYOD?"
    date: 2004-07-10
    body: "Yes. "
    author: "Sad Eagle"
  - subject: "Re: LYOD?"
    date: 2004-07-12
    body: "What's wrong with libICE? Lack of a maintainer? Why start anew?\n\nWill LYOD be obselete if KDE4 uses DCOP over DBUS?"
    author: "AC"
  - subject: "WHAT-WG"
    date: 2004-07-11
    body: "BTW, does anyone know if the Konqi/KHTML people plan on joining WHAT-WG (http://www.whatwg.org/)?"
    author: "A non-moose"
  - subject: "Re: WHAT-WG"
    date: 2004-07-11
    body: "Some of the safari guys seem involved, and they represent khtml-dom. "
    author: "anon"
  - subject: "Re: WHAT-WG"
    date: 2004-07-11
    body: "Konqueror team should participate to this dicussion.\n\nSafari team doesn't represent khtml but Apple. I am not sure that Apple will be the only one to anticipate the future of khtml.\nMoreover khtml and khtml-safari are really differents (different base, different features,...). Patch take a long time to be included in both of them. If Apple wanted to work on khtml, it would be a better idea to merge CVS."
    author: "Shift"
  - subject: "Mozilla style for KDE"
    date: 2004-07-12
    body: "Anybody knows if there is a mozilla-style for KDE/qt?"
    author: "Alex"
  - subject: "Re: Mozilla style for KDE"
    date: 2004-07-12
    body: "If you mean a style that unifies Mozilla/Firefox with KDE, do a search for Mozilla in the description in kde-look.org (click search on the left hand side, then type Mozilla into the *second* field, and hit search).  Plastik for Mozilla/Firefox is available, plus something that makes Mozilla use KDE Open/Save dialogs."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Mozilla style for KDE"
    date: 2004-07-16
    body: "Hm, I was actually thinking of a style that makes KDE look like Mozilla. :-)\nThe Mozille modern style is pretty good actually.\n\nThanks for your help anyway, I'll try those."
    author: "Alex"
---
In <a href="http://cvs-digest.org/index.php?issue=jul92004">this week's KDE CVS-Digest</a>:
Query designer in <a href="http://www.koffice.org/kexi/">Kexi</a> now has the ability to switch between visual and SQL mode.
<a href="http://www.koffice.org/kpresenter/">KPresenter</a> improves page effects.
<a href=" http://www.koffice.org/krita/">Krita</a> adds computing histograms.
<a href="http://amarok.kde.org/">amaroK</a> adds support for streaming over any supported KIO protocol.
Many bugfixes in <a href="http://akregator.sourceforge.net/">aKregator</a>, <a href="http://kopete.kde.org/">Kopete</a> and <a href="http://uml.sourceforge.net/">Umbrello</a>.


<!--break-->
