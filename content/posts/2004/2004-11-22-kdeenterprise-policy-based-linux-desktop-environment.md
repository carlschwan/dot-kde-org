---
title: "KDE::Enterprise: Policy Based Linux Desktop Environment"
date:    2004-11-22
authors:
  - "wbastian"
slug:    kdeenterprise-policy-based-linux-desktop-environment
comments:
  - subject: "BIG NOTE!"
    date: 2004-11-22
    body: "novell is doing the nfs way the wrong way(tm)\n\nsharing /home over nfs in read/write mode to the entire world is NOT cool\n\nI hope novell considers investing in nfsv4 to help it reach a stable state.\nnfsv4 has the ability to secure shares on a user basis using kerberos v5 gssapi\n\nin this howto they use nfsv3 and this is insecure++\nsure you can limit the ip ranges that will be allowed to use nfs\nand put up some firewall rules and restrict things using\n/etc/hosts.allow and /etc/hosts.deny\n\nbut these limitations wrongly asume that the intruder attacks from the outside.\n\nip based security is non existent."
    author: "Mark Hannessen"
  - subject: "What you really want..."
    date: 2004-11-22
    body: "is AFS. Yes, AFS is a pain in the butt to set up, but once it works, it is really nifty and a lot less of a pain that NFS, especially when it comes to scaleablilty. Formerly being a product of IBM, it is still supported and has been open sourced in the meanwhile: http://openafs.org/success.html\n\nI can use my uni's AFS tree via ADSL using Kerberos 5 authentication with SUSE 9.2.\n\nCheers,\n  Daniel"
    author: "Daniel Molkentin"
  - subject: "Re: What you really want..."
    date: 2004-11-25
    body: "I'd have to 2nd the vote for AFS.  Not finding what I want wrt security and NFS, I deployed AFS/KerbV within my organization.  Aside from being fairly complex to learn and setup, and a bit slow I've found it to be everything I'd hoped for.  I have all my users home dirs in AFS.  In addition it file serving, it can do replication and snapshot volumes.\n\nJohn"
    author: "John Koyle"
  - subject: "Re: BIG NOTE!"
    date: 2004-11-23
    body: "You mean you guys STILL don't have a stable NFSv4? <grins> If you need a good distributed filesystem for use with a KDE desktop environment, may I suggest FreeBSD/KDE?"
    author: "Brandybuck"
  - subject: "Re: BIG NOTE!"
    date: 2004-11-23
    body: "What's so great about NFSv4?  More kludges on top of more kludges?"
    author: "ac"
  - subject: "Re: BIG NOTE!"
    date: 2004-11-23
    body: "AFAIK nfs4 is rewritten from scatch and has nothing in common with nfs3."
    author: "Erik Hensema"
  - subject: "Re: BIG NOTE!"
    date: 2004-11-24
    body: "yeah, but it doesn't necessarily makes it better, only highly unstable at first because it does not have a mature codebase... what real advantages does it have over v3?"
    author: "c0p0n"
  - subject: "Re: BIG NOTE!"
    date: 2004-11-23
    body: "> may I suggest FreeBSD/KDE?\n\nSure, but get the threading working properly first ;)\n\nAs for other properly scalable distributed file systems for Linux i would recommend GFS or Lustre. Truly great products.\n"
    author: "anon"
  - subject: "Re: BIG NOTE!"
    date: 2004-11-23
    body: "> i would recommend GFS or Lustre. Truly great products.\n\nBut not standard. With NFSv4 you won't be tied down with a homogenous environment, but can deploy it on Linux, Solaris and BSDs. In real life this is very important."
    author: "Brandybuck"
  - subject: "Re: BIG NOTE!"
    date: 2004-11-23
    body: "and....\n\nwindows.\nNFSv4 was designed to have usable by windows clients.\n\nhaven't seen one yet...\nbut NFSv4 is a BIG filesystem, and fairly new..\nso we might still see one in the future."
    author: "Mark Hannessen"
  - subject: "Re: BIG NOTE!"
    date: 2004-11-23
    body: "Erhm.  This is standard NFS deployment practice.  Everywhere I've seen NFS deployed they do it this way."
    author: "ac"
  - subject: "Re: BIG NOTE!"
    date: 2004-11-23
    body: "That doesn\u00b4t make it more secure than parachuting with an umbrella."
    author: "Roberto Alsina"
  - subject: "Re: BIG NOTE!"
    date: 2004-11-23
    body: "infact, this is so insecure that one bootable floppy could retrieve and destroy ALL data owned by ALL users. (with the exeption of the root user and /root)\n\nand you don't need a hackers tool to find it out, insecurity is a nfs3 feature."
    author: "Mark Hannessen"
  - subject: "Re: BIG NOTE!"
    date: 2004-11-23
    body: "I suppose you could only allow NFS over a VPN overlaying your real network so that the floppy solution doesn't work. Then all you need is to crack the workstation using your physical access, *then* use a floppy."
    author: "Roberto Alsina"
  - subject: "Re: BIG NOTE!"
    date: 2004-11-25
    body: "How does the workstation access the VPN? The key used must be something that cannot be accessed from the workstation by a user with a boot floppy, and in this sort of corporate environment, having IT go round with a boot key to machines whenever they get restarted is not an option (typically centralised IT, workstations all over the place, and cleaners don't always choose the right plug to unplug). I can't think of any way to hide data on the machine in such a way that the user cannot access it once booted off his floppy, without bringing a boot key round to each machine as and when they restart."
    author: "Simon Farnsworth"
  - subject: "Re: BIG NOTE!"
    date: 2004-11-25
    body: "Encrypted filesystems which can only be accessed with the right token, plus MAC (ala SELinux) can get you somewhat closer.\n\nIt is a hairy as hell problem, though. And expensive.\n\nThe main thing is: how do you forbid the user from accessing the VPN keys:\n\nYou don't use keys, you use certificates.\n\nHow do you prevent the user from accessing the certificate? He has no permission to read them.\n\nHow do you forbid him from using a floppy to read it?\nYou encrypt the filesystem (at least part of it)\n\nHow do you read the encrypted part? With a key.\n\nErm... ok. Whatever.\n\nBut yeah, usually it means someone has to be available in every physical location with a magical token to boot the boxes. Banks do it all the time already.\n\nThat person doesn't need to be IT, he only has to be the manager. He is responsible for box not being booted off floppies and such."
    author: "Roberto Alsina"
  - subject: "Novell desktop is a visual mess"
    date: 2004-11-24
    body: "If you look at the screenshots in the pdf, you see what a mess NLD is. They have qt, java, gtk, ncurses, cli and web guis all over the place. I mean, even Corel did a better job than these guys.\n\nIf you check how meticulous apple and microsoft are with their guis you wonder what sense there is in NLD competing for the same user.\n\nI dont mind some inconsistensies in kde coz its a volunteer effort from pipo who dedicate their precious time for this great desktop.\n\nHowever, novell pays its pipo to write stuff thats useful and at least pleasing to the eye.\n\nThe concotion of edirectory with its console one, Yast, /etc text files is just unlearnable. \n\nI tried to teach someone this stuff. Its like a collection of bastard apps from different permutations of parents of different species.\n\n"
    author: "mukuka"
  - subject: "Good Article"
    date: 2004-11-25
    body: "The ideas presented are quite good in concept and addresses the main requirement of running it in an enterprise. \n\nIts things like this will push KDE into enterprise and help remove the illusion of it being a technical solution.\n\nObviously the Server software can be replaced such as NFS Server in be easily dropped with another file sharing server such as AFS or Samba and eDirectory can be replace with OpenLDAP."
    author: "Anonymous Spectator"
---
<a href="http://enterprise.kde.org/">KDE::Enterprise</a> is featuring
<a href="http://enterprise.kde.org/articles/Kiosk_customization.pdf">an interesting new article [pdf, 2Mb]</a>
from Novell consultant <a href="mailto:amalaguti@novell.com">Adrián Malaguti</a> in which he explains step-by-step how to apply restrictive policies to a
<a href="http://www.novell.com/products/desktop/">Novell Linux Desktop 9</a> KDE desktop, manage Linux users from eDirectory, authenticate through LDAP and store user's data and profiles in a remote NFS server for centralized management. A must-read for anyone who wants to roll out KDE in their organisation.

<!--break-->
