---
title: "The People Behind KDE: Gunnar Schmi Dt"
date:    2004-05-17
authors:
  - "Tink"
slug:    people-behind-kde-gunnar-schmi-dt
comments:
  - subject: "This is cool .. \"The Land of Penguins\" "
    date: 2004-05-17
    body: "From http://accessibility.kde.org/\n\".. free software should not just be free for all, but also usable at a basic level by all. And accessible to all. ..\"\n\nWe tend to forget the above thing ... \n\nMany thanks to Gunnar and other people involved with the KDE Accessibility Project for making our cool desktop more accessible to people.\n\nbtw http://www.schmi-dt.de/penguins/index.en.html is really a nice and entertaining read :)\n\nCiao \n\nFab"
    author: "Fabrice Mous"
  - subject: "Most difficult question"
    date: 2004-05-17
    body: "I laughed my head off on Gunnar's answer to the most difficult question.\n\nGunnar: You made my day!\n\nJoachim"
    author: "Joachim"
---
<a href="http://www.kde.nl/people/">The People Behind KDE</a> is going on summer recess. We end the current series of interviews with a subject I am passionate about, and think it's one of the most important and underrated projects within KDE, accessibility. We'll meet the person behind the name with the Dt, one of the driving forces behind KDE's Accessibility Project, the man who loves Penguins and classical music, <a href="http://www.kde.nl/people/gunnar.html">Gunnar Schmi Dt!</a>



<!--break-->
