---
title: "KDE-CVS-Digest for January 9, 2004"
date:    2004-01-10
authors:
  - "dkite"
slug:    kde-cvs-digest-january-9-2004
comments:
  - subject: "Thanks, Derek"
    date: 2004-01-10
    body: "Wow! First comment!!\n\nThanks, Derek, for your precious CVS-Digest!!\n\nBye"
    author: "nekkar"
  - subject: "nice"
    date: 2004-01-10
    body: "Great digest Derek, Thanks!\n\namaroK 0.8.1 looks sooo good.  i'm compiling it now.  it has definitely removed my need for xmms at all.\n\n//standsolid//\n\n"
    author: "standsolid"
  - subject: "Re: nice"
    date: 2004-01-10
    body: "A happy customer :-)\n\nAlso please use version 0.8.3 which we released yesterday as it addresses a few compile issues for KDE 3.1 users as well as introducing some new features."
    author: "Max Howell"
  - subject: "Re: nice"
    date: 2004-01-10
    body: "well that compile stopped...\n\n0.8.3 is on now in it's way :)\n\n//standsolid//"
    author: "standsolid"
  - subject: "Re: nice"
    date: 2004-01-10
    body: "And regarding amaroK -- is the appropriate place for feature requests at the sourceforge project page?\n\nI think it would be nifty to have a window-shade mode... the one thing i miss from winamp/xmms\n\n//standsolid//"
    author: "standsolid"
  - subject: "Re: nice"
    date: 2004-01-10
    body: "You might just talk to us on IRC, and we'll gladly add any feature requests to TODO. We usually hang out on irc.freenode.net / channel #amarok. If you can't come to IRC, simply send a mail, address is in the program's about dialog. \n\nConcerning window shade mode - yeah we're already looking into that. Sure a nifty feature. Stay tuned =)\n"
    author: "Mark Kretschmann"
  - subject: "Re: nice, but..."
    date: 2004-01-10
    body: "... it can't play *.mpc Files or I'm wrong??\n"
    author: "anonymous"
  - subject: "mpc"
    date: 2004-01-10
    body: "amaroK is a frontend for arts (like noatun), so it plays whatever you can get arts to play. To my knowledge arts doesn't support mpc files.\n\namaroK is being refitted to use a variety of audio-engines (ie GStreamer and others to be disclosed). You may have more luck with future versions.. Out of interest, do you know any OSS media players that support that format?"
    author: "Max Howell"
  - subject: "Re: mpc"
    date: 2004-01-10
    body: "I have a plugin for xmms that allows me to listen to mpc files."
    author: "Jonathan Bryce"
  - subject: "Focus stealing"
    date: 2004-01-10
    body: "As usual ;-) : Thanks Derek!!! It's always an interesting read.\nAbout focus stealing prevention:\nI'm using KDE 3.2 beta 2 and this thing is driving me nuts.\nEven the exec command dlg idoesn't show properly when pressing Alt+F2.\nIs this also covered by the mentioned bugfix?\nBTW: Is there any way to disable this stuff at all? I never really\nliked it on Windows and think it's absolutely superfluous. I'm not\n80 years old - I can react properly and don't get a heart attack when\na window shows up suddenly. Especially annoying IMO is that I sometimes\nlike to run apps which load quite a time from my desktop and until they\ncome up I sometimes do other stuff. Until now (and in contrast to Windows)\nthis worked perfectly. Now I have to maximize the window manually again\nonce it's loaded.\n\n\n"
    author: "Mike"
  - subject: "Re: Focus stealing"
    date: 2004-01-10
    body: "> BTW: Is there any way to disable this stuff at all?\n\nSure: RMB window decoration, \"Configure...\", \"Advanced\", \"Focus stealing prevention: None\""
    author: "Anonymous"
  - subject: "Re: Focus stealing"
    date: 2004-01-10
    body: "Ahh! Thanks a lot!!\nI couldn't find it because of its strange German translation.\n\n\n"
    author: "Mike"
  - subject: "Re: Focus stealing"
    date: 2004-01-13
    body: "You could add a bug for the bad german translation, or report it here:\nhttp://www.kde.me.uk/index.php?page=KDE_Typos\n"
    author: "JohnFlux"
  - subject: "Re: Focus stealing"
    date: 2008-02-07
    body: "> Sure: RMB window decoration, \"Configure...\", \"Advanced\", \"Focus stealing prevention:...\nCool.  Just for those of us not in the know, WTF is an \"RMB window\"?"
    author: "Bruce"
  - subject: "Re: Focus stealing"
    date: 2008-02-07
    body: "RMB window decoration ==  press the right mousebutton on the window titlebar"
    author: "me"
  - subject: "Re: Focus stealing"
    date: 2004-01-10
    body: "And why does not \"Print Screen\" enforce a snapshot as it is in Windows. this is the only diadavantage in usability of KDE."
    author: "Gerd Br\u00fcckner"
  - subject: "Re: Focus stealing"
    date: 2004-01-10
    body: "My apologies if I completely misunderstand your question, but.. If you're talking about taking a screenshot using the \"print screen\" button, this is available since a few KDE releases already.\n\nCTRL+PrintSrc - full screen snapshot\nALT+PrintSrc - active window snapshot\n\nThe resulting image is put into the clipboard, all you need to do is to paste it somewhere, using an app that understands pasting PNG images. Konqueror does, so all you need to do is to rightclick the desktop (for instance) and chose \"paste\". (Other apps that supports this is kpaint and kword.)"
    author: "teatime"
  - subject: "Re: Focus stealing"
    date: 2004-01-10
    body: "windows: printscr = screenshot\nKDE strg+printscr 0 screenshot\n\nbtw: what other usage is made of printscr without strg?\n "
    author: "kalle"
  - subject: "Re: Focus stealing"
    date: 2004-01-11
    body: "While that is a useful feature, you can always use ksnapshot, which is much more flexible than the print screen key (eg, you can take snapshots of specific window, save, and print the file all from one handy little screen :) )."
    author: "optikSmoke"
  - subject: "Re: Focus stealing"
    date: 2004-01-11
    body: "Actually I configured the control center's keyboard shortcuts module to launch ksnapshot by pressing PrintSrc which is quite convenient :)"
    author: "Pat"
  - subject: "Re: Focus stealing"
    date: 2004-01-10
    body: "If you have any problems, please report them at the proper place, which is bugs.kde.org and not dot.kde.org. (And, if I might ask, include a better description of the problem, since I don't remember Alt+F2 being broken since a long time, and I really don't see what does maximalization have to do with focus stealing prevention.)"
    author: "Lubos Lunak"
  - subject: "Re: Focus stealing"
    date: 2008-03-26
    body: "OK.  Now I've found this KDE control module thing and have set focus\nstealing prevention to \"extreme\".  I still have this accursed application\nthat thinks it is really, really, really important to let me know every\n30 seconds that it thinks something isn't quite right.  I do not like\nbeing yanked across multiple virtual desktops to this brainless app,\nbut I have to use the thing.  What is the setting past \"extreme\"?\nI can't find it.  What is the \"Do not ever under any circumstances\nallow any application to steal the focus\" setting?"
    author: "Bruce"
  - subject: "Fixed or not?"
    date: 2004-01-10
    body: "A topper bug http://bugs.kde.org/show_bug.cgi?id=39693 has been marked FIXED.\n\nKDE-CVS reads: \".....makes loading of very large images quite a bit faster\"\n\nhttp://bugs.kde.org/show_bug.cgi?id=39693 mentions: \".....This massively reduces memory footprint and slightly improves performance\"\n\n\nNeither of the two actually clearly says that it totaly fixes the bug. Now is it fixed or not??"
    author: "ac"
  - subject: "Re: Fixed or not?"
    date: 2004-01-10
    body: "a) you missed the \"reduce X-server pressure with large images\"\nb) I'm sure Dirk tested this\nc) improve performance in several places does add up\nd) Have you even tried it? If it's not fixed for you, you can re-open it."
    author: "cloose"
  - subject: "Re: Fixed or not?"
    date: 2004-01-10
    body: "I guess I'm a little impatient here.. I don't have CVS to try out on. I was 'affraid' this fix was more like a small 'hack around' but not a true fix.\n\nWell if it works, I guess it does, that's great, because this has been a real annoying topper bug for a long time now.\n\nDirk rules.. ;)"
    author: "ac"
  - subject: "Re: Fixed or not?"
    date: 2004-01-10
    body: "There was more today on that problem. Hopefully fixes it for good.\n\nDerek"
    author: "Derek Kite"
  - subject: "Visual KJSEmbed ;-)"
    date: 2004-01-10
    body: "I think there are great things in store for KJSEmbed, if only one single thing is done for it:  a nice, simple, dedicated IDE with integrated UI builder (not QT Designer running in a separate window, making .ui files to be imported later with extra steps).  The integration would make it drop-dead simple to turn out a dinky little app in an hour, even if you don't know much about the process of making a KDE app already.  It could be the Visual Basic killer everyone says they want.  Would it be hard to extract the UI-editing parts of QT Designer and make that into a KPart for this IDE?"
    author: "Spy Hunter"
  - subject: "Re: Visual KJSEmbed ;-)"
    date: 2004-01-10
    body: "Should be possible to be done in KDE 4, see \"Kastle 2003: What to expect from Qt 4\" http://dot.kde.org/1061880936/ \"Designer split up into components. This makes it possible to create a form editor plugin for KDevelop.\""
    author: "Datschge"
  - subject: "Re: Visual KJSEmbed ;-)"
    date: 2004-01-10
    body: "Visual Basic?\n\nWhat about Hbasic 0.99g?\n\nhttp://hbasic.sf.net"
    author: "kalle"
  - subject: "Re: Visual KJSEmbed ;-)"
    date: 2004-01-10
    body: "probibly the fact its basic, and the fact its lacking KDE Support, WidgetFactory support, and just now is getting limited SQL support.\n\nKJSEmbed while limited has very nice SQL support for any driver QtSQL supports."
    author: "Ian Reinhart Geiser"
  - subject: "Re: Visual KJSEmbed ;-)"
    date: 2004-01-10
    body: "Since about 6 months we could load UI files perfectly.\n\nSo design it in Qt Designer.  Name your widgets with meaningful names, and you can call them from javascript.\n\nLast night I added a function to our core JS library that will allow you to load a UI file, and it will add every object in the UI as a property. So if you have a group box called Foo that has a LineEdit called bar you would address it as view.Foo.bar.text to get the text of the lineedit.\n\nSo if in fact what you wished for has been there for some time, it just got trivial last night.\n\nBtw, you can access kioslaves and kparts too ;)\n\nCheers\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Visual KJSEmbed ;-)"
    date: 2004-01-10
    body: "Also im working on KJSEmbed IDE plugin for KDevelop 3.0.  Class browser and debugger works. Im trying to get autocompletion to work next.  Expect it before Q2 of 2004 here.\n\nAlso take a walk arround http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdebindings/kjsembed/docs/examples/\n\nThere are tons of things we alredy support to make apps in KJSEbmed trival.\nDCOP, StandardActions, KParts, KIO, KConfig, StandardDirs, Iconloaders...\n\nIts pretty powerful for doing stuff fast and one off.\n\nCheers\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Visual KJSEmbed ;-)"
    date: 2004-01-11
    body: "Are any tutorials planned KJSEmbed?"
    author: "anon"
  - subject: "Re: Visual KJSEmbed ;-)"
    date: 2004-01-11
    body: "We have a ton of examples now, but we will have plans for some nice howtos.\n\nCheers\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Visual KJSEmbed ;-)"
    date: 2004-01-11
    body: "Wow, that sounds pretty nice.  I still think having the designer integrated into an IDE would be a nice feature, though.  Does KJSEmbed make it easy to call C++ code that you write yourself, too?"
    author: "Spy Hunter"
  - subject: "Re: Visual KJSEmbed ;-)"
    date: 2004-01-11
    body: "yes... but the devil is in the details.\n\nyou can embed KJSEmbed (hence the name) into an application like i did in the KJSKickerApplet in kdenonbeta.  Here I just publish my interface via the publish() funciton.  This exposes the published QObjects signals slots and properties.  These can be called from KJSEmbed then.\n\nMore complexly you can provide a bindings plugin with your custom type and load that at runtime of the script.\n\nat either point we try to mirror the Qt api as much as possible.  A great example of this is my buttonmaker in cvs.  The UI file is completely designed in Qt Designer.  You can then in js connect the slots to the uis signals.\n\nOne other option i have not tested is you could in Qt designer connect the signals and slots the normal way.  Then in KJSEmbed create functions that match those signatures.  At that point those will be called automaticly via Qt Widget factory.  Now the curious side effect of this method is that if you later decide that C++ is the way to go take the SAME IDENTICAL UI FILE, and just create a C++ class that contains the same slots as you made in javascript.  This is a big plus for those developers who love to prototype the UI first... then dedicate to C++.\n\nCheers\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Visual KJSEmbed ;-)"
    date: 2004-01-11
    body: "I can see a use for KJSEmbed where the entire application is written in Javascript, and then for the speed-critical drawing/sorting/whatever parts a few C++ functions are written and then called to do the dirty work.  I think apps written in this way could be just as fast as current KDE apps but easier to maintain and have fewer bugs.  With an open-source project ease of maintenance is key.  If most of KDE was written in Javascript with .ui files, the number of people who could contribute to coding would be larger, and KDE would improve faster.  Have you considered this type of application for KJSEmbed?"
    author: "Spy Hunter"
  - subject: "Re: Visual KJSEmbed ;-)"
    date: 2004-01-11
    body: "Sort of... Already in my apps ButtonMaker (made buttons for my companies intranet site) and EnvelopeMaker (makes oneoff postscript files for mailing envelopes for my company) KJSEmbed has proven VERY powerfulfor oneoff apps....\n\n...BUT...\n\nWe must remember billy g's most important lesson that he tought us \"Just because its easy to write a 100 line app in a language dosent mean it will scale well to 100k lines\"  We can see how on windows many crappy VB apps are the product of programmers thinking that if its easy for 100 lines, then it should make my 100k line program easy.  KJSEmbed is no exception.  For apps that are less than say 500 lines, where you can see the code pretty easily in a text editor KJSEmbed is awesome.  It can provide simple glue between powerful C++ objects to create simple one off applications fast.  But for the same reason that KDE is not written in Scheme or Lisp Javascript would become a maintiance nightmare after a while.  Remember only rudimentary classes, no virtuals, no real polymorphism... it runs out of power quick once you start building a large OO framework with it :)\n\nThat being said, for things like kicker applets, konqi sidebars, and almost any plugin in KDE, KJSEmbed i feel is perfect.  For those simple one off apps to keep the company secretary happy under linux its perfect.  For web developers who want a simple one off app to script a process again its spot on... \n\nI could argue that kcontrol panels could be redone in KJSEmbed... but im not sure if maintainers are ready for that yet ;)\n\nCheers\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "71062!"
    date: 2004-01-10
    body: "I'm glad to see the fix for 71062, but I still see the same bug occasionally, but now it goes away when the page is reloaded. This makes it hard to reproduce and make a good bug report. If anyone can reproduce this, please see bug #66894.\n\nThanks to Dirk for his khtml fixes for this week. Much appreciated!\n\n"
    author: "cbcbcb"
  - subject: "Branch?"
    date: 2004-01-10
    body: "I don't like asking questions in the dot, but...\n\nDoes this means that a branch has been created? I thought KDE was on freeze, and that features only will be added to HEAD, so that means the KDE_3_2_BRANCH (or something like that) was created.\n\nI don't see anything on kde-devel mailing list :-?"
    author: "Alex Exojo"
  - subject: "Re: Branch?"
    date: 2004-01-10
    body: "The new features are in either separate development branches, or kdenonbeta. The core kde release is in strict bug fix mode, until next week or so.\n\nThere isn't a 3.2 branch yet.\n\nDerek"
    author: "Derek Kite"
  - subject: "KDE PIM Attention"
    date: 2004-01-10
    body: "Nice to see lots of attention on KDE-PIM.  I have heard lots of comments about Kontact being a KDE killer app but for commercial businesses Kontact has to work well.  For us the other Linux options are: Thunderbird (no organizer or notes, but good mail lists and extra fields), Evolution (no notes, no extra fields but good palm synching and looks polished), and Outlook under Wine (no comment). We see Kontact very close to being best in class.  In our business (like most) we need Kontact to have:\na) reliability - Kmail has actually worked very well for us for 2 years but palm synching has been terrible leaving us in a real problem state - maybe the new KPilot integration step is solving that.\nb) flexibility - some of the new fields in addressbook will help big-time - with enough fields we can create our own contact manager (heck, we use gnumeric for it right now)\nc) security - the announced integration of SpamAssassin is a great, great addition.  An easy KMail way to encrypt/decrypt sensitive messages with PGP or VPN would also be good for non-geek types (which managers usually are).\nd) completeness - it sadly sounds like many of these changes won't make KDE 3.2 but instead of just having a PIM update later (which many distros or sys admins may put off - I would advocate the changes getting released as a critical KDE upgrade, ie 3.21 so that all users start to get the new fixed Kontact asap.  For many buinsesses, it might just be the most important part of KDE.\n"
    author: "John"
  - subject: "Speaking of Basic"
    date: 2004-01-10
    body: "Which is better? HBasic or Gambas?\n\n1. Hbasic http://hbasic.sourceforge.net/\n2. Gambas http://gambas.sourceforge.net/"
    author: "Alex"
  - subject: "Style workshop?"
    date: 2004-01-12
    body: "What's Style Workshop and/or largeimagelib?"
    author: "AC"
---
In <a href="http://members.shaw.ca/dkite/jan92004.html">this week's KDE-CVS-Digest</a>:
Many changes in <a href="http://pim.kde.org/">KDE-PIM</a>; gpgme now used in 
<A href="http://kmail.kde.org/">KMail</A>. <A href="http://knode.sourceforge.net/">KNode</A> integration in <A href="http://www.kontact.org/">Kontact</A> completed. A <A href="http://pim.kde.org/components/kpilot.php">KPilot</A> plugin for Kontact. IMAP addressbook resources, used in <A href="http://kolab.kroupware.org/">Kolab</A>, is complete. And an initial version of a PIM configuration wizard. In <A href="http://www.koffice.org/kexi/">Kexi</A>, read-write queries are supported and dragging relations together now works. A KJSEmbed envelope maker example is available. <a href="http://www.methylblue.com/filelight/">Filelight</a> can be used in Konqueror. And the usual bugfixes.

<!--break-->
