---
title: "Ars Technica: Deep inside the K Desktop Environment 3.2"
date:    2004-02-23
authors:
  - "binner"
slug:    ars-technica-deep-inside-k-desktop-environment-32
comments:
  - subject: "Phantastic Article!"
    date: 2004-02-23
    body: "This is a phantastic piece of work by Datschge and Henrique Pinto.\n\nThanks a lot!\n\nKurt (who's bookmarked it to send the URL out to some friends later today)"
    author: "Kurt Pfeifle"
  - subject: "Re: Phantastic Article!"
    date: 2004-02-23
    body: "Yer. I liked the explanation of the development infrastructure (nice diagrams to go with it - developers like their diagrams :)) and the explanation of licensing was well handled. Nice, straightforward explanations with no silliness, which it could have descended into when talking about the licensing of Qt etc. Paying a license for closed-source development work? Whatever next?"
    author: "David"
  - subject: "Re: Phantastic Article!"
    date: 2004-02-24
    body: "You also have to pay a license to do open-source development that you want to be cross-platform.\n(or find someone that has a license and is willing to recompile for windows all the time).\n\nFor opensource projects wanting to be cross-platform, qt is pretty unusable :(\nI'm in this problem now for worldforge.org, because the artists use windows, and need the tools to work in windows.\n\n"
    author: "John Tapsell"
  - subject: "Re: Phantastic Article!"
    date: 2004-02-24
    body: "Christ - will the whining never stop !!\n\nJust pay up for a commercial licence and help support Qt"
    author: "Anonymous Coward"
  - subject: "Re: Phantastic Article!"
    date: 2004-02-24
    body: "Yeah, he works for free, promotes Qt with his free work for free, and now he even want's to make it cross platform and have more people benefiting from it for free? I think it's wrong, he should be paying for it already!\n"
    author: "Source"
  - subject: "Re: Phantastic Article!"
    date: 2004-02-24
    body: "Where in his post does it say he was forced to use Qt?  Please point out where he did not have a choice in using something else."
    author: "ac"
  - subject: "Re: Phantastic Article!"
    date: 2004-02-24
    body: "Yes, I agree it's bad that customers need to pay for Windows and MacOS licenses first before being able to use Open Source. Oh, wait a moment..."
    author: "Datschge"
  - subject: "Re: Phantastic Article!"
    date: 2004-02-24
    body: "I just went to the worldforge.org page, and in the FAQ is says, and I quote, \"Is everything free? Yes. Most of our source code is covered by the GNU General Public Licence (GPL). A few libraries are released under the LGPL , but we recommend the GPL for libraries too.\"\n\nSo in other words, you want all of your library contributions to be under the GPL, but you don't want third party libraries like Qt to be under the GPL. Go figure...\n\nIn any case, you can still use the non-commercial Qt (3.2.1 available with new Qt book), and though it's not GPL compatible, all you need to do is to add an exception for your own code."
    author: "David Johnson"
  - subject: "Re: Phantastic Article!"
    date: 2004-02-25
    body: "Oh, he didn't stated that.\nHe just wanted a free (not gratis) version of Qt/Windows.\n\nAnd Qt/X11 with Cygwin isn't enough.\n\nBut I can understand TrollTech, because of what they experienced with the non-commercial edition of Qt 2.2. (typical for the Windows world?)"
    author: "CE"
  - subject: "Re: Phantastic Article!"
    date: 2004-02-26
    body: ">> (typical for the Windows world?)\n\nRampant piracy?\n\nIt's the Windows way!"
    author: "Tukla Ratte"
  - subject: "Wow"
    date: 2004-02-23
    body: "No words!\n\nBTW, could you point to the updated Compiling KDE CVS for Newbies?\n\nhttp://kde.ground.cz/tiki-index.php?page=CVS+Step+One"
    author: "cwoelz"
  - subject: "Re: Wow"
    date: 2004-02-23
    body: "By you I mean Datschge."
    author: "cwoelz"
  - subject: "Re: Wow"
    date: 2004-02-24
    body: "Eric Bangeman, the responsible Ars editor, changed it. Thank him. =)"
    author: "Datschge"
  - subject: "great article"
    date: 2004-02-23
    body: "Really, really great article. This looks a lot like the feature guide we had with KDE 3.1. \n\nWhat's the copyright of this article btw? \n\nCheers' \n\nFab\n\n"
    author: "Fabrice Mous"
  - subject: "Re: great article"
    date: 2004-02-24
    body: "good question. If it's free enough I would like to add some of it to the Wikipedia article about KDE (http://en.wikipedia.org/wiki/KDE)"
    author: "MK"
  - subject: "Re: great article"
    date: 2004-02-24
    body: "Why not just link it?"
    author: "Anonymous"
  - subject: "Re: great article"
    date: 2004-02-24
    body: "You need to contact the responsible ars technica editor for this article. \n\nFab"
    author: "Fabrice Mous"
  - subject: "Bad wording in the licence section"
    date: 2004-02-23
    body: "> With both KDE and Qt/X11 being GPL'ed\n\nI am used to reading this in troll postings but not in articles of KDE promo people.\n\nIf you don't want to write that kdelibs are mostly LGPL with some parts being BSD licences, write that KDE is GPL compatible.\n\n\"KDE is GPL'ed\" in not helping!\n\nCheers,\nKevin"
    author: "Kevin Krammer"
  - subject: "Re: Bad wording in the licence section"
    date: 2004-02-23
    body: "They also write this:\n\n... the availability of both the QPL and the GPL for Qt/X11 allows the KDE project to continue its own liberal licensing policy allowing most forms of free software to become a part of the KDE project."
    author: "OI"
  - subject: "Re: Bad wording in the licence section"
    date: 2004-02-23
    body: "It's all academic.\n\nEssentially all the people ever talking about this topic are deliberately stirring. They know what the true situation is, and just try and subvert the conversation because they can. They have no interest in being set straight and the precise wording of an article such as this will make no difference. \n\nJust mention KDE or QT in any article on Slashdot or OSNews, and the GTK/anti-QT lot come out to play. That's just life."
    author: "Dawnrider"
  - subject: "Re: Bad wording in the licence section"
    date: 2004-02-25
    body: "Its all they have to justify there existance. "
    author: "Kraig"
  - subject: "Re: Bad wording in the licence section"
    date: 2004-02-24
    body: "You should contact the editor and make them aware of it. I'm quite sure they're willing to update it if there are errors in the article."
    author: "Joergen Ramskov"
  - subject: "Re: Bad wording in the licence section"
    date: 2004-02-24
    body: "While I agree that out of the context the quoted text doesn't fare well I don't see a huge need to correct it since what I'm saying there highly depends on the context anyway. The wording as is is imo the easiest to understand one in that context without having to add a whole sentence explaining how the GPL is the lowest common denominator among the licenses in that specific case and how that matters. I think I sufficiently described each cases for the reader both to follow the text easily and grasp the basic idea described. And I won't be able to avoid it if someone really wants to take the text out of context. Feel free to disagree.\n\nNota bene: regarding the licensing issue I'm actually surprised that Dre didn't appear yet with his theory claiming that use of open source within companies is still distribution. Ah well... =P"
    author: "Datschge"
  - subject: "News from insiders :-)"
    date: 2004-02-23
    body: "Topic says it all. Grats.\n\nYours, Kay"
    author: "Debian User"
  - subject: "impressive"
    date: 2004-02-23
    body: "peeps on /. are impressed:\nhttp://slashdot.org/article.pl?sid=04/02/23/1422235"
    author: "ac"
  - subject: "Short summary for dutch readers"
    date: 2004-02-24
    body: "For dutch readers, there's an article on Tweakers.net (http://www.tweakers.net/nieuws/31236) which talks about the licensing also mentioned in the article by Ars Technica and also mentions some other points from the article. It's based on the article by Ars Technica and might be a good read for those who prefer Dutch. The story from Ars Technica provides for a very nice read, so you're encouraged to read that as well :).\n\nI'm still planning to write something like this article, but which also goes into more detail about the possibilities of Konqueror or KIO, and maybe arts and Kontact and numerous other applications. Given enough time, that could make for an interesting article as well."
    author: "Jonathan Brugge"
  - subject: "Re: Short summary for dutch readers"
    date: 2004-02-24
    body: "Something like that could easily be extended to a KDE User Book or something like that for promotion. After the generally positive feedback for this article I'm inclined to give it a try if the effort is combined to directly benefits other areas like help documentation and 'what's this' context help etc. =)"
    author: "Datschge"
  - subject: "Thanks"
    date: 2004-02-24
    body: "Thanks a lot for the excellent KDE review you wrote. I read it with interest, only to discover that I felt as if I was speaking it myself, but a lot clearer and cleaner than I could. I like the very crisp phrasing and the rich hyperlinking.\n\nGreat work!"
    author: "Inorog"
  - subject: "Indeed"
    date: 2004-02-24
    body: "I haven't seen such a good in depth article in a long time and this is by far the ebst review I have ever read."
    author: "Alex"
  - subject: "It's kind of funny..."
    date: 2004-02-24
    body: "It's an odd story for me. I was a long time GNOME advocate, it was fun and I never really got into KDE or wanted to. My first disitrbution was Redhat, as often seems to happen, you fall in lvoe with your first distro ;p It had its problems, I did not sue Redhat 7.2 as my full time desktop, Windows to me was a better desktop, but I still liked to ocassionally play around with it etc. and I was always on Linux and GNOME related websites even though I did not use it so much. The more time pased, the more polished Redhat got, and than out of nowhere it seemed, GNOME 2.0 came, I was throughly dissapointed. Never had I thought that a project could go backwards so fast, I am not speaking about the architecture, amybe that was better, but from a user's point of view it was a nightmare. It crashed more often than Windows by a longshot, it was very very slow, and too many features were removed. I am all for simplifying the user interface, removing redundant options, and options nobody uses, but just too many good features were removed which could have simply been implemented better. I would rather have 3 more items clog up my menu and have the features I need instead of some haughty developer telling me what people use and making me lose 3 hours. I did not like the new attitude of the GNOME project, it felt elitist and it was no longer fun, it didn't feel like it had a personality of it's own, it was rather molded into whatever IBM, SUN and Ximian wanted it to be, no longer what the loyal users wanted. But, I stuck with it, I was afterall still a GNOME zealot. That is until Redhat 9 came about, I liked it, it was an improvement as a system, but GNOME felt very much the same, I didn't even notice any new features, just a lot fo bugfixes, optimizations and a few UI fixes here and there, but I expected more. So, I decided to give KDE a whirl and see how it improved since the last time I tried it which was at version 2.2. WOW, it felt a lot faster than before, even though GNOME started up faster, KDE was faster to use and it had many of the features I wanted in GNOME before, but it was also lacking soem I liekd in GNOME, such as drawers, emblems, more attractive thumbnails, and shadowed text. I liked it and I decided to spend more time with it, after spending 2 more weeks switching between the two I was using KDE more often than GNOME and I started being interested in the community. I noticed that it felt like an OSS project, like GNOME once was, it had the sense of spirit, hope, joy and passion I admired in GNOME before. Best of all, it was not in the least bit elitist, I could submit whatever I wanted to websites like KDE-LOOK and my only judge was the community, not some high and mighty elitist judge. With KDE 3.2, and after following it's development, I am switched on it. I do not use KDE 3.2 full time as Fedora does not ship with it yet, but I did try it in a recent Mandrake Beta and I liked it very much, lots of improvements everywhere as the authors mention. THe KDE releases are meaningful, they are feature absed, not time based, that's why KDE 3.2 took a year to get out the door, due to the delays. The developers are very dedicated and made sure when they released it that it was what they wanted it to be. GNOME, since their new attitude change has made no delays, they would rather release a buggy product rather than delay it for their users. I also like that KDE is much more moderate. GNOME is all or nothing when it comes to many issues, such as the UI. It's either remove all features practically which are not used by 80% of people or remove none, KDE carefully examines what they remove and will not remove what they don't feel is essential because they don't screw their users. They are improving their UI rapidly, NOTE IMPROVING, NOT REMOVING EVERYTHING so there is nothing to improve. The way GNOME handles it reminds me of Win2k3 which instead of improving secuirity and fixing vulnerabilities simply sht down all services with exploits. GREAT SOLUTION GUYS!"
    author: "Solic"
  - subject: "Re: It's kind of funny..."
    date: 2004-02-24
    body: "... is called paragraphs! My eyes are burning!\n"
    author: "A New Invention coming to a brain near you..."
  - subject: "Re: It's kind of funny..."
    date: 2004-02-24
    body: "Work on that attention span, then!"
    author: "The Badger"
  - subject: "Re: It's kind of funny..."
    date: 2004-02-26
    body: "Yeah, kids these days and their \"paragraphs\".  Why, we never used paragraphs in the old days, before the MTV.  Bah!"
    author: "Tukla Ratte"
  - subject: "Re: It's kind of funny..."
    date: 2004-02-24
    body: "these are the kinds of anecdotes that make me so very happy. not because of someone switching away from something (in this case GNOME), but because it's evidence that the project's efforts and visions are being communicated to KDE users through the resulting product. that's an amazingly difficult thing to do, and i'm in awe of everyone involved. =)\n\nSolic: welcome to KDE. i hope we can continue to provide you with the software you _want_ to use. =)"
    author: "Aaron J. Seigo"
  - subject: "Re: It's kind of funny..."
    date: 2004-02-25
    body: "I concur. This post highlights the point that you and I are so strong on... that usability is not reduced user options but intelligent presets and carefully thought out and organized options. Eliminating options to eliminate confusion does not empower users. Empowering users in a way that is friendly is an exercise in it's self and not an easy, short, trite answer. Not to downplay how challenging it is to accomplish these design objectives and have them be noticed I think the evidence of the effort is actually pretty easy to see if you're looking.\n\nI'd also like to say that I've read a lot hype in public postings about how great it is to be rubbing elbows with the corporate user base. Currently most desktop Linux users are individuals who are here because of various reasons. I think it's safe to say that in most cases those reasons and interests are not the same as a corporate IT department manager's. In fact in many cases the corporate end users have different interests than those administering their systems. I'm not against the corporate admins because I think KDE Kiosk is what they need. However I think we need to always keep in mind what users want. I also think users will tend to want pretty much what we want... really good programs that will let them work their way. I'm really proud to be able to be part of KDE because people do see that."
    author: "Eric Laffoon"
  - subject: "Re: It's kind of funny..."
    date: 2004-02-24
    body: "Hi,\n\ndid you try Konstruct then? Give it a try. If you have the development packages installed, it works nicely.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: It's kind of funny..."
    date: 2004-02-25
    body: "You're saying that GNOME removes functionality without thinking about it. Your statement is giving me a bad headache.\nGNOME releases are NOT automatically buggy, just because they are released twice a year. It's more a time-based release plan. Features which are ready will be included, otherwise they'll be dropped. Really simple, right?\nFortunately, you just seem to be (another) user and thus you don't have enough insight in GNOME or KDE.\n\ncya,\nchris"
    author: "chris"
  - subject: "Re: It's kind of funny..."
    date: 2004-02-26
    body: "wow.  that felt like one long rant in one breath that I agreed with!\n\nhere, here!\n\n//standsolid, the great//"
    author: "standsolid"
  - subject: "One correction"
    date: 2004-02-24
    body: "I forgot to mention that while the writting is generally very good, it sometimes ommits ofs and VPL is Visual Page Layout not Visible I think.\n\nStill, the best article so far, and I love that they actually talked about the architecture."
    author: "Alex"
  - subject: "Re: One correction"
    date: 2004-02-24
    body: "Yes, VPL = Visual Page Layout.\n\nAndras"
    author: "Andras Mantia"
  - subject: "KsCD ... who mugged you?"
    date: 2004-02-24
    body: "What happened to KsCD? Who redesigned it from something that resembled an actual CD player into a programmer-designed interface?  Maybe something happened to the Debian SID packages of it for 3.2.0-pre1v1 but my god does it suck -- sorry but it does.  Please go back to the old interface.  I can't even figure out how to adjust the volume in it now.  I'll admit with the old one, it was a bit goofy to have the configuration options under a button with a hammer.  Now all the other stuff is put under an \"Extras\" button w/drop-down menu.  Then the track is put on top.  Who ever designed the new one must have been on a 128hr code-a-thon."
    author: "Lost in KsCD Wasteland"
  - subject: "Re: KsCD ... who mugged you?"
    date: 2004-02-24
    body: "I like the new kscd interface, it is much easier to localise (to dutch for example :) \nIt is more elick, mor mature.\nDunno why the track is put on top, I gues on the bottom would be much better.\nThe volume can be adusted by clicking on the volume icon.\n\nRinse"
    author: "rinse"
  - subject: "Re: KsCD ... who mugged you?"
    date: 2004-02-25
    body: "Well it's probably easier to localize, but then are CD players that different in Denmark?  For example, don't you have a \"<<\" for previous/rewind and \">>\" for next/fast-forward.  Maybe not, I don't know.\n\nAnyway, it doesn't really looks like a CD player anymore.  It looks like a computer program.  A CD-player application is one where I think it should emulate the real-world equivalent.\n\nThanks, I see how to adjust the volume now.  I didn't really pay attention to that button -- since everything else went kinda non-standard.\n\nAgain, *PLEASE* change KsCD back to how to it was or ditch it and use Noatun completely.\n"
    author: "Lost in KsCD Wasteland"
  - subject: "Re: KsCD ... who mugged you?"
    date: 2004-02-25
    body: "I think it is a very good thing to move away from trying to emulate real world devices. These devices are limited by their very nature of being physical, why impose the same limitations on software?\nI like a computerprograms that look and act like my other computerprograms. Sure, a set of |<  <<  >>  >| buttons would be handy, but they belong in an ordinary toolbar, not modeled to look like \"real\" buttons. If I want to play my CD's in a real CD player, I'll do just that."
    author: "Andr\u00e9 Somers"
  - subject: "Re: KsCD ... who mugged you?"
    date: 2004-02-25
    body: "Yea but, KsCD is suppose to emulate a CD player.  How much more can a CD player do than well, be a CD player.  Everything under the current \"Extras\" button is fine.\n\nFor most programs yea fine, they should look and feel like programs.  However when they emulate real-world devices that do specific tasks, they should look-and-feel like the real-world devices.  If you call something a \"CD player\", then people will think it should operate like the CD players they're use to."
    author: "Lost in KsCD Wasteland"
  - subject: "Re: KsCD ... who mugged you?"
    date: 2004-02-26
    body: "\"If you call something a \"CD player\", then people will think it should operate like the CD players they're use to.\"\n\nInstead, they should thing that it PLAYS CDs.\n\nNothing to do with how CD player devices are."
    author: "buggy"
  - subject: "Re: KsCD ... who mugged you?"
    date: 2004-02-26
    body: "No, it's not supposed to emulate a cd player. It's supposed to play audio cds."
    author: "Stephen Douglas"
  - subject: "Re: KsCD ... who mugged you?"
    date: 2004-02-26
    body: "I also think that the new KsCD looks less visually appealing than the old one. I agree that the old one had to be improved, but the new one is a step backwards IMHO:\n\n1) The volume slider and the track drop-down widget on top look very much out-of-place. Why not put them on the bottom of the application window?\n\n2) The play, stop, previous and next buttons belong together and should be grouped and displayed appart from the other buttons that are functionally differernt.\n\n3) The icon of the play button is placed too far away from the text (Play). This looks not very elegant in my opinion.\n\nAs a side note, I very much like how Apple groups functionally similar buttons together and separates them by space and type of representation from functionally different groups of buttons. This is in stark contrast to KDE, where most often all buttons of a given application are thrown onto toolbars, lined up one after the other, leading to a decreased usability, as the user has to first visually scan through the row(s) of numerous icons in order to find the icon that executes the desired operation."
    author: "Tuxo"
  - subject: "Re: KsCD ... who mugged you?"
    date: 2004-02-27
    body: "\"Well it's probably easier to localize, but then are CD players that different in Denmark? For example, don't you have a \"<<\" for previous/rewind and \">>\" for next/fast-forward. Maybe not, I don't know.\"\nI dunno about Denmark, I'm From Holland :)\nBut the old kscd did not have enough room to fit the dutch words into. \n\n \n\" Anyway, it doesn't really looks like a CD player anymore. It looks like a computer program. A CD-player application is one where I think it should emulate the real-world equivalent.\"\n\nEmulating real world applications is a bad thing useability wise. Real world devices have different limitations compared to computer GUI's. KCSD is a computerprogram, not a real world cd-player. So it should look like a computer program.\n\nRinse\n"
    author: "rinse"
  - subject: "Re: KsCD ... who mugged you?"
    date: 2004-02-27
    body: "Oh, yeah. This, for example, is a real world device:\n\nhttp://www.pycs.net/lateral/weblog/2004/02/25.html#P169\n\nIf someone mimics it, I\u00b4m gonna be pissed :-)"
    author: "Roberto Alsina"
  - subject: "Re: KsCD ... who mugged you?"
    date: 2004-02-27
    body: "I'm imagining a CD player that looks like a computer program:\n\n101011101100111000111111 :-)\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: KsCD ... who mugged you?"
    date: 2004-02-27
    body: "some examples of bad gui design of programs that mimic real live devices\"\nhttp://digilander.libero.it/chiediloapippo/Engineering/iarchitect/readplease.htm\nhttp://digilander.libero.it/chiediloapippo/Engineering/iarchitect/qtime.htm\nhttp://digilander.libero.it/chiediloapippo/Engineering/iarchitect/target.htm"
    author: "rinse"
  - subject: "Re: KsCD ... who mugged you?"
    date: 2004-02-27
    body: "Am I the only one who is amused at all of the usability development and critique that is going on, only to find someone promoting what is an exceptionally horrible UI; that of the humble CD/tape player?\n\nI mean, how does '>>' intuitively translate to fast forward? How does '>|' mean next track to most people?\n\nAnd yet it gets used and people accomodate it... They learn it.\n\nThis, of course, doesn't stop truly confusing stereo/radio interfaces with all of their random store/recover buttons. Want to set a radio station to a button? First, press 'Preset', then 'Store', then the button you want, then 'Set' twice, then 'Preset' again. Youwhatnow? Of course, this is buried deep in the 300-page manual, under 'Presets', not 'How to store a radio station'.\n\nJust to place recent usability conversations in context :)"
    author: "Dawnrider"
  - subject: "Re: KsCD ... who mugged you?"
    date: 2004-03-01
    body: "Am I the only one who is amused at all of the usability development and critique that is going on, only to find someone promoting what is an exceptionally horrible UI; that of the humble CD/tape player?\n \n\n[quote]I mean, how does '>>' intuitively translate to fast forward? How does '>|' mean next track to most people?[/quote]\nIf you use a left to right language (and thus a left to right interface), the buttons no longer make sence because they are mirrored (try kcsd --reverse to see the effect.)\n\nAlso people who have view problems could have trouble recognizing the buttons.\n\n \n"
    author: "rinse"
  - subject: "completely off-topic"
    date: 2004-02-25
    body: "i just found out you can set a script-hook for the phonenumber in kAddressBook. So when you click on a phonenumber of a contact, a script is started with the number being passed as an argument. Great!\n\nNow, after finally getting the ltmodem to work in 2.6, how the hell can I cause the modem to dial, but not beep, and then hang up shortly thereafter (or at the press of a button)? I haven't found anything on freshmeat or kde-apps that would do the trick...\n\n"
    author: "anonymous"
  - subject: "too bad"
    date: 2005-07-07
    body: "I used to read Ars Technica, until it became apparent that Ken Fisher is a bit of a racist.  He typically comes off like your typical smug armchair intellectual, but once he gets fired up, he's got quite a mouth on him. I've seen him get pretty worked up and go off on people on IRC over basically nothing.  \n\nIt's too bad, too, since the other guys on the site like Hannibal are actually pretty smart. "
    author: "jay buckley"
---
<a href="http://www.arstechnica.com/">Ars Technica</a> features an article "<a href="http://www.arstechnica.com/reviews/004/software/kde-3.2/kde-3.2-01.html">Deep inside the K Desktop Environment 3.2</a>" written by Datschge and Henrique Pinto. After introducing KDE and <a href="http://www.arstechnica.com/reviews/004/software/kde-3.2/kde-3.2-02.html">the project's structure</a> the authors present <a href="http://www.arstechnica.com/reviews/004/software/kde-3.2/kde-3.2-03.html">some new applications of KDE 3.2</a>. After that they explain the <a href="http://www.arstechnica.com/reviews/004/software/kde-3.2/kde-3.2-06.html">key KDE technologies</a> KParts, DCOP, KIO, Kiosk and KXMLGUI and give examples for <a href="http://www.arstechnica.com/reviews/004/software/kde-3.2/kde-3.2-08.html">code reusage and an overview of efforts to integrate</a> non-KDE applications. For developers <a href="http://www.arstechnica.com/reviews/004/software/kde-3.2/kde-3.2-09.html">Umbrello, Cervisia and Valgrind with KCachegrind</a> are introduced and of course <a href="http://www.arstechnica.com/reviews/004/software/kde-3.2/kde-3.2-10.html">KDevelop 3.0</a>. An <a href="http://www.arstechnica.com/reviews/004/software/kde-3.2/kde-3.2-11.html">examination of licenses</a> is preceding <a href="http://www.arstechnica.com/reviews/004/software/kde-3.2/kde-3.2-12.html">the  positive conclusion</a>.




<!--break-->
