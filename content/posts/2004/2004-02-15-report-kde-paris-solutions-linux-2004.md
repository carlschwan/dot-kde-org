---
title: "Report: KDE at Paris Solutions Linux 2004"
date:    2004-02-15
authors:
  - "cmiramon"
slug:    report-kde-paris-solutions-linux-2004
comments:
  - subject: "French"
    date: 2004-02-14
    body: "\"9600 visitors this year\"\n\n---> so France is not intrested in Linux at all?\n\nParis is the capital of France and as France still is a centralistic state that means that the public almost took no notice of the event. In orther parts of Europe we get such a participation number in \"unknown\" cities.\n\nThe linux scene of France is very small and radical, IT policy oriented. Don't forget that the French always believed they had the leading science of \"ordinateurs\".\n\nUsability of KDE still has to beimproved, there are too many options that distract you from work and no good theme manager.\n\n\nGermany: I guess the difference is that in Germany you will find a strong network of KDE supporters and a lot of FLOSS conferences. This also drove the action in Munich, strong LUG and independent organisations. they are organisational freaks.\n\n\"No commercial booth was pushing a desktop solution, except Novell which sported a strange combination of Ximian computers at the centre of its big booth and SUSE-KDE at the periphery.\"\n\nSo who run the event? The french are infiltrated by FSF radicalism and FSF radicals were also in favour of gnome. I think what KDE has to show is more political committment to a free and open information society."
    author: "Gerd"
  - subject: "Re: French"
    date: 2004-02-15
    body: "I was told that Solutions Linux is bigger than Linux Tag, so it is not so small. \n\nResponding to your anti-French troll, the French Linux community is (as one could easily imagine) diverse with young, old, radicals, pragmatics, pro-Gnome, pro-KDE,  etc...  I guess not very different than the German one.\n\nCheers,\nCharles"
    author: "Charles de Miramon"
  - subject: "Re: French"
    date: 2004-02-15
    body: "> I was told that Solutions Linux is bigger than Linux Tag, so it is not so small.\n\nSolutions Linux 2004: 9600 visitors, 150 exhibitors, 22 lectures\nLinuxTag 2003: 19500 visitors, 159 exhibitors, 6000qm booth area, 100h conference program\n\nLinuxTag 2003 grew by 43% visitors and 30% exhibitors compared to 2002 so expect 2004 to be even bigger!"
    author: "Anonymous"
  - subject: "Re: French"
    date: 2004-02-15
    body: "Solutions Linux 2004 was running from Monday to Thursday, and aimed toward professionals. Not everybody can leave his work or take hollydays to go to this meeting.\nIf the weekend had been included, I think that visitors count would have been greater.(Though I don't known if it would have been a good choice as week-end visitors are totaly different from plain week visitors)\n\nI wish I could go to this event (I'm at 40min driving), but I lacked time."
    author: "Olivier LAHAYE"
  - subject: "Re: French"
    date: 2004-02-16
    body: "... from Tuesday to Thursday, even."
    author: "David Faure"
  - subject: "Re: French"
    date: 2004-02-15
    body: "It was not intended as a troll posting. I just wondered why there is so little attention in France. I don't like the German LinuxTag event or the German style. I prefer the dutch scene."
    author: "Gerd"
  - subject: "Re: French"
    date: 2004-02-15
    body: "Boooooomm !!!\nMy trolloscope explosed :)\n\nI disagree with you. They are lots of linux users in France but they are not all really interested by this event. This event is becoming a \"I am a big firm and I use Linux (or not)\" with lot's of commercials and ties. There are associations but this events is not the best one to see linux associations.\nThis event is too \"Hurried decidors\" for us ;)\n\nFranck (French)"
    author: "Shift"
  - subject: "Re: French"
    date: 2004-02-15
    body: "Steady on! I'm certain that there are many Linux and KDE supporters in France, as can be seen from the success of this show.\n\nFrance seems to be the same as everywhere else. There are no big migrations happening on the desktop until people can see something totally tangible, something worth throwing out their old kit for and an adequate migration path. Cost isn't quite enough unfortunately. This is going to be very hard I'm afraid, and the really big obstacle we must face. No one has tackled, or even thought about this yet.\n\n\"Usability of KDE still has to beimproved, there are too many options that distract you from work and no good theme manager.\"\n\nYes, usability is important but you cannot sell usability to organisations. I wouldn't exactly call Windows the most usable desktop ever, so there needs to be a bit of thought put in. The usability thing is mainly just FUD.\n\n\"So who run the event? The french are infiltrated by FSF radicalism and FSF radicals were also in favour of gnome. I think what KDE has to show is more political committment to a free and open information society.\"\n\nErm, yer. Right."
    author: "David"
  - subject: "Lack of publicity"
    date: 2004-02-14
    body: "As a French Linux supporter, I can say there was strictly\nno publicity for this event. If you want to be known,\ndon't stay hidden.\n\nAs to centralistic state, I can say this is another problem.\nMany people from other cities do not even want to go to Paris,\nbecause of travel, hotel and price. Especially for a small\nevent nobody cares of! (the title is false: Solution Linux\nis simply not known at all). Some Linux shows in other cities\nreally are better apparently (by the way, they are not\ncommercial shows).\n\nLast, there is absolutely no move towards Linux here.\nLinux *is* popular in research / among students, but\nwere the money is, it simply does not exist. Period.\n(and there is no money at all for research in France\nsince a loonnng time - oh, by the way, that is not a bad\nthing since French research does not study\npractical/usefull things at all in general and it would be\npure waste).\n\nLinux will certainly take off here once everybody will have\nproved it's viable. We are IT pachyderms. And we don't\nthink at all we lead the computer science: it simply\ndoesn't exists here (compared to Americans societies,\nthere is nothing but state financed fake societies\nmany would really like to see disappear). Anyway, I'm\nnot convinced computers are a \"good thing\": they only\nenables more technocracy for nothing.\n"
    author: "Christian"
  - subject: "Re: Lack of publicity"
    date: 2004-02-15
    body: "So what can be done to get more attention for the different event? Is there the possibility of a kind of web-based management system in order to get you informed about the various events in your region.\n\nPlone is used by FFII for event management.\n\nhttp://plone.ffii.org\n\nbut it lacks groupware integration and is slow. \nIt is also too difficult to manage call4papers via Plone."
    author: "Andre"
  - subject: "[OT] Plone"
    date: 2004-02-15
    body: "Plone is the slowest and most braindead piece of crap ever to be written in python. Zope is quite good but has some abstract concepts you have to understand to work with it (acquisition etc.). Plone adds Layers upon Layers of Abstrations on top of Zope to make it \"easier to use\".\n\nThat means that it is slow as hell and only easy to use if you do *exactly* what it is meant for. As soon as you have a single custom requirement, all that ease of use breaks down and you have to sort out the above mentioned layers of abstraction. Oh, and the documentation is practically non-existent.\n\nPlone is proof that even a beautiful language like python can be used to write totally unmaintainable, ugly CRAP."
    author: "Androgynous Howard"
  - subject: "Re: [OT] Plone"
    date: 2004-02-15
    body: "Oh Jesus.\n\n\"Plone is the slowest and most braindead piece of crap ever to be written in python.\"\n\nI won't answer that.\n\n\"Zope is quite good but has some abstract concepts you have to understand to work with it (acquisition etc.). Plone adds Layers upon Layers of Abstrations on top of Zope to make it \"easier to use\"\n\nZope is initially difficult to understand but it's concepts are very well grounded. It took me quite a while, but I would never move away from Zope to anything now. PHP, definitely not ASP, nothing. I'm not interested because I can now get things done quicker and better, and design a web system that fits in better with existing systems, analysis and design methodologies. I can actually design a web system now and have it fit in neatly with the technology. The object database design, and seperating out what you are doing into objects within the namespace is entirely logical, and you're glad you did it. If you don't understand these concepts that's your loss. Yes, Plone is another layer on this but it makes the CMF stuff so much easier. Do you want to start from scratch or do you want to do some complex CMF stuff, get it done quickly and accurately and get paid for it? I did.\n\n\"Plone is proof that even a beautiful language like python can be used to write totally unmaintainable, ugly CRAP.\"\n\nWell, it is used underneath but Zope and Plone are different from Python itself. I suggest you go away and learn about Zope first."
    author: "David"
  - subject: "Re: [OT] Plone"
    date: 2004-02-15
    body: "What alternatives do you suggest, that are not php?"
    author: "andre"
  - subject: "Re: [OT] Plone"
    date: 2004-02-15
    body: "What's wrong with PHP? Yes I agree that Zope together with Plone can be slow, but Plone and Zope can get things done within a shorter timescale and be better organised. Apache/PHP is much better when performance is an issue. It is a trade-off. Nothing is perfect. I had to use PHP at the University Medical School because Zope just couldn't handle hundreds of students hitting the thing all at the same time and doing the same things. People hitting the server all at the same time is bad enough, but when they are using the same functions at the same time - boy! Things may be different now.\n\nThere is also the Java option. JSP or Tomcat seems to run very well, but I don't have any experience with it or know what additional add-on stuff like forums etc. you can do. Anyone have any idea?\n\nIt depends what you want, what powerful features you want fom this and whether the site will be under extreme load. Most of the time Zope should be fine.\n\nFancy using Mono and ASP.NET? Sorry, that was a joke.\n\n\nAs a final note you can serve static content like images from your Apache system with Zope. You don't have to use the object database if you are simply storing boring static stuff. I don't know if this was the problem with the site images, or whether they were just too large. The team photo was 300k but it shouldn't have been too bad on a DSL line. Zope tends not to be good at this sort of thing."
    author: "David"
  - subject: "Re: [OT] Plone"
    date: 2004-02-15
    body: "I have nothing against zope. Zope is quite well designed, and the ZODB is definitely a better match for an OO language than SQL. But it is not exactly fast, and it is powerful enough to do quite a bit of stuff without putting more abstraction layers on top of it.\n\nWith plone on top of CMF on top of zope, you get a performance that is not even  adequate for small non-profit sites. If you want decent performance the only way to do it is to put a huge squid cache in front of it. \n\nThe documentation of CMF and Plone is a huge mess, and the plone developers have an annoying tendency to give \"cute\" names to variables that say nothing about what they do. Why are the results from a catalog called \"brains\" and who the hell invents stupid names like \"bobobase\"? "
    author: "Androgynous Howard"
  - subject: "Re: [OT] Plone"
    date: 2004-02-15
    body: "\"With plone on top of CMF on top of zope, you get a performance that is not even adequate for small non-profit sites.\"\n\nMmm, well I've never experienced too many problems, and that is with a fair few users using the site. With good setup, maintenance, and good server hardware (it doesn't have to be expensive) you should be able to serve a fair few Zope sites. However, mileage may vary. For systems where I know there will be hundreds of people hitting the server at the same time (and performing the same actions) we have simply used PHP where it is needed because Zope has not had the performance. This was a few years ago so I wouldn't know what the situation is now. However, our productivity has gone through the roof since using Zope.\n\n\"If you want decent performance the only way to do it is to put a huge squid cache in front of it.\"\n\nThat's one option I suppose, but with a constantly changing CMF-based system and when you are managing Zope etc. through management screens, caches are problematic. Another option is to simply use Apache to serve totally static content, like images (or general storage of files like PDFs), where you need to and I've found that this makes a big difference. You can do this with a Zope Product who's name escapes me. The ZODB is not a good option for stuff like this, and of course you won't need all the meta-data stuff unless necessary.\n\n\"The documentation of CMF and Plone is a huge mess, and the plone developers have an annoying tendency to give \"cute\" names to variables that say nothing about what they do. Why are the results from a catalog called \"brains\" and who the hell invents stupid names like \"bobobase\"?\"\n\nI wouldn't say the documentation is a huge mess at all (there's a good book and other sources of info), but it does take a while to wrap your head around it, and these names have nothing to do with Plone. Zope does tend to have its own terminology, like a lot of other software - think Microsoft. Pluggable 'brains' are rather cool (they don't have anything to do with the Plone developers though - don't blame them), and can be especially used for R2O mapping (relational to object). Think of it as a 'memory' of stored indexes to objects (mmm, hope I got that right). The name isn't all that stupid. It can do a lot more than that of course, but look at this page and search for brains as an example:\n\nhttp://www.ukuug.org/events/linux2001/papers/html/SRichter/ZopePaper.html\n\nYou shouldn't be exposed to Bobobase too much these days. Anyone have any idea if this is related to Bonobo in any way? That is a legacy from when Zope first started, and it turned into what we now know of as the ZODB. Zope used to be called Bobobase. Have a look here at where it came from:\n\nhttp://www.newarchitectmag.com/documents/s=5751/new1013637597/\n\nAny bobobase names now are just a hangover, so you shouldn't see them too much, but they don't have anything to do with Plone. It isn't like web programming in ASP, PERL, PHP or vanilla Python. It is a base for creating full-featured and powerful applications. Unfortunately, Zope is very powerful and it takes time to learn but, it is worth it.\n\nZope is certainly no speed demon, but with adequate thought it is totally possible to get good development work done quickly and have pretty adequate performance at the same time."
    author: "David"
  - subject: "KDE \u00fcber alles?"
    date: 2004-02-15
    body: "Hello,\n\nplease try not to put nationalisms into news. France is the origin of Mandrake, one of the better distributions and close to KDE.\n\nI think it's clear that if 9000 people go to Paris and find time to look at Linux, it must be good. There is so much else to see.\n\nOne compliment to the French in compensation, from a German, your food is better, are very friendly and indeed have better soccer players. ;-)\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: KDE \u00fcber alles?"
    date: 2004-02-15
    body: "Ok \"are very friendly\". Ok lets not get to cared away."
    author: "anon"
  - subject: "Re: KDE \u00fcber alles?"
    date: 2004-02-15
    body: "I don't think nationalism is of any importance in software development. I prefer French soccer and I would like to see more french films in order to improve my french but I don't understand the plot of these movies.\n\nUnfortunately and that has to be discussed the French Linux scene is smaller than it could and should be. So what can be done to improve this? What can be learned from France and what from Italy ecc. The different European Linux scenes are often out of touch, separated by the language barrier and use english discussion fora as their common ground. To me the question is: What can be done to join forces.\n\nHow can we create common materials (Linux convincement papers, flyers), for instance a kind of \"instant\" tools for event organisation. How to reach new interest groups ecc. Exchange of contacts ecc. \n\nWhat I saw in France was that the Linux scene was more hacker style, more political. I like this very much as there is a lack of this approach in other parts of Europe."
    author: "Gerd"
  - subject: "Some more details"
    date: 2004-02-15
    body: "Yes, Solutions Linux is still a little exhibition.\nBut the major players were here (IBM, HP, Apple, PS, Novell, SUN, etc.)\nAnd these players accepted to invite the community.\nWe can say that the people of the community were more than 100.\n\nKDE booth was managed by about 15 people, a mix of translators and of developers.\nWe could see up to 8 KDE people at the same time on the booth (including a 14-years translator, with an amazing knowledge of Linux-based systems).\n\nKDE is more and more accepted. All our visitors knew the product very well and say thy use it. And many people bought a \"I love KDE\" T-shirt.\n\nI also made a talk about KDE on Linux-pratique magazine booth. About 40 people stayed 1 hour standing in front of the boot, listening to my explanations. I insisted on the fact that KDE is a system, not only applications. And I showed KDE apps were much faster than others, when launched under KDE.\n\nI counted KDE and Gnome machines on the exhibition : KDE had nearly twice the number of Gnome desktops (All the Red Hat machines were counted as Gnome machine). Except KDE and Gnome, there is nearly nothing else, only on geeks computers.\n\nMost of questions were about MSOffice compatibility and MSAccess replacement.\n\nI had a very interesting contact with Merlin Gerin. They wrote a KDE app to support their power supply, and they would like it to be integrated in official KControl.\n\nI think Linux expos (and Linux itself) will be mature when ordinary vendors (printers, scanners, etc.) will be here.\n"
    author: "Gerard Delafond"
  - subject: "USB key"
    date: 2004-02-15
    body: "Hey... thats a nice USB key. Where can I get hold on one of those?"
    author: "Lars"
  - subject: "Re: USB key"
    date: 2004-02-15
    body: "Send a mail to G\u00e9rard Delafond (gerard@delafond.org). We have also Polartech sweaters with the KDE logo for people living in cold countries."
    author: "Charles de Miramon"
  - subject: "Re: USB key"
    date: 2004-02-15
    body: "I have the last one.\nBut I don't know if it is possible to send it to foreign countries (cost of the payment)"
    author: "Gerard Delafond"
  - subject: "On the server hosting the images..."
    date: 2004-02-15
    body: "What kind of crappy server is hosting those images? They take almost five minutes per image to download on my DSL line.\n\nOtherwise, thanks to all of the KDE people for making it out here and promoting the awesome KDE."
    author: "Just Passing By"
  - subject: "Re: On the server hosting the images..."
    date: 2004-02-15
    body: "KDE-France is hosted by Tuxfamily a community server that was compromised a week ago. The service is back but not on the same level than before. We are waiting to see how things evolve at Tuxfamily and maybe try to migrate if the situation stays bad.\n\nThere seems that a lot of negativity is floating around. Pre-Valentine Day syndrom, maybe ;-)"
    author: "Charles de Miramon"
  - subject: "Not only Novell"
    date: 2004-02-15
    body: "> No commercial booth was pushing a desktop solution, except Novell\n> which sported a strange combination of Ximian computers at the\n> centre of its big booth and SUSE-KDE at the periphery.\n\nDidn't really anyone notice that some french guys were advertising KDE workstations at AMD Booth ?\n\nAs for the 9600 visitors, it's not so bad for an event targeted mainly towards professionals (it was titled \"open source & free software solutions for *business*\")."
    author: "Cyril"
  - subject: "Re: Not only Novell"
    date: 2004-02-15
    body: "Oh, the KDE booth on the picture looks very bad. Why is there so much waste in your booth?\n\n4 little small screens, you are so proud of them, so KDE france can't even afford a small projector to get a larger screen.\n\nHow do these guys look like? Is this a socialist summer camp or a technology exhibition? Usually the outer appearance and the consumer directed style of Linux exhibitors is awful. Autistism included."
    author: "Holler"
  - subject: "Re: Not only Novell"
    date: 2004-02-15
    body: ">Why is there so much waste in your booth?\nWhat do you mean ?\n>4 little small screens, you are so proud of them, so KDE france can't even afford a small projector to get a larger screen.\nEverybody can use a projector. There were many of them on the othe boothes.\nBut, not averybody can set a display cluster.\nHundreds of people stopped simply for asking some questions about multiscreen configuration.\nAnd KDE has Xinerama capabilities which cannot be shown some othe way.\n>How do these guys look like? Is this a socialist summer camp or a technology exhibition\nHow do you want we look like ?\nDon't you like T-shirts ? I prefer 15 people waking around in the exhibition in KDE T-shirts than in personal chothes, even if they are very smart.\nCriticisms are easy, but say what you think is better. If you have good ideas, just say it."
    author: "Gerard Delafond"
  - subject: "Re: Not only Novell"
    date: 2004-02-16
    body: "I don't like the tone of your post.\n\nHow about joining next time and organising a booth instead of criticising. Task include: bringing a fridge, preparing the T-shirts with KDE logo, the sweaters with KDE logo, the beer holder/ash tray with KDE logo, the big posters on the wall, brining a huge case with a lock, getting fined because the park place close to the exhibition are reserved, contacting distributions to get computers and screens, installing the cluster of 4 screens with 4 video cards of 4 different brands (for me, it is a wonder that it worked), taking 3 days off just to manage the booth and paying for all of this on your own money. Kudos to Gerard Delafond who did all that.\n\n> KDE france can't even afford a small projector to get a larger screen.\n\nIndeed, no. How about a donation from your part, if you think this is worthwhile. But maybe socialists summer camps are not worth your money.\n\n"
    author: "Philippe Fremy"
  - subject: "Re: Not only Novell"
    date: 2004-02-16
    body: "It all looks very impressive.\nThe merchandise sold here is wonderful and no doubt it took a fair amount of time to put all that together.\nThe booth is very nice and having space allows people to walk around easily.\nI guess, Holler, that you never put together a booth for an Open Source Project. Stupid criticism is very easy and does not lead anywhere.\nThanks to the French team for the very nice report. We all hope that Open Source Software will find more adepts in France, especially within the French administration.\n"
    author: "annma"
  - subject: "Re: Not only Novell"
    date: 2004-02-17
    body: "Calm down. What do you do for KDE besides writing this kind lame and useless rants about what other people do for the community?\n\nCongratulations to the french KDE team"
    author: "Sergio"
  - subject: "Nice booth!"
    date: 2004-02-16
    body: "I'd just like to tell you that I was very much impressed by the nice KDE booth (and KDE merchandising stuff - I'd like to get a KDE USB stick as well ;) that can be seen on the photos. Great work! \nIf some people think that there is stuff to improve you are welcome to join our efforts :-)\n\nhttp://events.kde.org/calendar/\n\n.. shows all KDE related events that the kde-event team is aware of.\nIn case you want to help just contact the respective person listed there.\n\nGreetings,\nTackat "
    author: "tackat"
  - subject: "Black KDE T-Shirt"
    date: 2004-02-16
    body: "hey,.... where can we get these t-shirt ?\n\nthey are really nice.\n\n"
    author: "somekool/Mathieu"
  - subject: "Re: Black KDE T-Shirt"
    date: 2004-02-17
    body: "Specially made for the expo. All sold after 2 days.\nAll the KDE-team got one for free."
    author: "G\u00e9rard"
---
<a href="http://www.solutionslinux.fr/"> Solutions Linux</a> is the main Linux Trade Show in France. The <a href="http://www.kde-france.org/">French KDE team</a> was there like the precedent year promoting our favorite desktop. <a href="http://www.kde-france.org/galerie/expo2004/">Our booth</a> located in the very lively Association's village was organised by the indefatigable Gérard Delafond and sported an <a href="http://www.kde-france.org/galerie/expo2004/fullsize/demo2_fr.htm"> impressive Xinerama screen-wall </a> demonstrating the brand new KDE 3.2. This wall was sponsored by <a href="http://www.mandrakesoft.com/">MandrakeSoft</a> and <a href="http://www.belinea.com/">Belinea</a>.




<!--break-->
<p>
We were even more ambitious in our cottage business of selling KDE branded goodies with a very popular <a href="http://www.kde-france.org/galerie/expo2004/fullsize/usb_key_for_sale_fr.htm">KDE USB key</a> and a less popular <a href="http://www.kde-france.org/galerie/expo2004/fullsize/cendrier5_fr.htm">KDE ashtray</a>. The show was also a perfect occasion for KDE fellows from all over France to get together for their <a href="http://www.kde-france.org/galerie/expo2004/fullsize/dinner2_fr.htm">KDinner</a> and welcome a jet-lagged but still valiant <a href="http://www.kde-france.org/galerie/expo2004/fullsize/team_fr.htm">George Staikos</a>.</p>
<p>
<b>KDE and the French Market</b>
<p>
Solutions Linux ran for three days and welcomed around 9600 visitors this year. After speaking with our (prospective) users, my impression is that there is no big migration plan to Linux on desktop happening now in France. The boldest move is being done by the Homeland Ministry (Ministère de l'intérieur) which is migrating to OpenOffice.org on Windows. The city of Paris have just <a href="http://news.com.com/2100-7344-5158001.html">ordered a study for the migration of its IT</a> to Open Source. All in all, French administration is at least two years behind German administration.</p>
<p>
No commercial booth was pushing a desktop solution, except Novell which sported a strange combination of Ximian computers at the centre of its big booth and SUSE-KDE at the periphery.</p>
<p>
Going round the show and looking at other people's computers, my guess is that we have increased our market share in the French geek community at the expense of GNOME and Window Maker. Some geeks told me that they find that KDE is a different OSS project: serious, efficient, punctual and hard working. We have not forgotten our German roots. </p>
<p>
Our big challenge is to market outside of our natural user base. One interesting trend is that several IT people working for industrial companies with strong R&D departments (like the defence industry) came to our booth. They plan to replace their mismatch of Unix workstations and Windows PCs to a unified affordable development and testing platform. I explained to them the way to go: <a href="http://www.kde.org/announcements/announce-3.2_developers/whykde.php">KDE the developer platform</a>. Another possible niche for KDE are vertical applications: using KDE and Qt as a base to create a richer integrated solution (hardware, software and service) for a specific trade. A KDE fellow is working in a company in such a project (still top-secret). 



