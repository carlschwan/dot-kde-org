---
title: "Waldo Bastian on Kiosk and the Linux desktop"
date:    2004-08-12
authors:
  - "ateam"
slug:    waldo-bastian-kiosk-and-linux-desktop
comments:
  - subject: "Kios for a marketing Live CD?"
    date: 2004-08-11
    body: "I wonder if a Kios configuration might be suitable for a Knoppix (or similar) CD intended for the absolutely inexperienced Linux users? The point is that we really don't want them to be able to do anything silly like re-partition their hard drive. We recently made a simplified version of Knoppix 3.4 for Software Freedom Day, where we basically cleaned away some of the more dangerous menu entries, and made it all a bit more user friendly, but that's not quite enough.\n\nYou can see screenshots here: http://softwarefreedomday.org/knoppix/screenshots/\nand read more here: http://theopencd.sunsite.dk/1_4_1_releasesd.html (where there is also a bittorrent link for the ISO)"
    author: "Henrik"
  - subject: "Re: Kios for a marketing Live CD?"
    date: 2004-08-11
    body: "Ooops. I misspelled Kiosk. Twice! Embarrassing! doubly so because it also shows I'm not using Konqueror with it's spell-checking at the moment :)"
    author: "Henrik"
  - subject: "Re: Kios for a marketing Live CD?"
    date: 2004-08-12
    body: "With what part of KDE can you do repartitioning? Of course you can disable with Kiosk any access to shells, but then you could also delete fdisk?"
    author: "Anonymous"
  - subject: "Re: Kios for a marketing Live CD?"
    date: 2004-08-12
    body: "How about Qtparted or other tools from the menu?\nKiosk is about disabling the direct access for the normal user not about complete absolut security against any kind of hacker."
    author: "anno"
  - subject: "Re: Kiosk for a marketing Live CD?"
    date: 2004-08-12
    body: "Right, so that's what we've done. We've removed Qtparted from the menu, but it's still on the system as is fdisk. Both should be removed. The easiest might just be to keep the root password secret from the casual user and not give root shell access. That way they won't be able to launch things like fdisk, or even mount the Windows drives in write mode."
    author: "Henrik"
  - subject: "Re: Kiosk for a marketing Live CD?"
    date: 2004-08-12
    body: "Or its probably possible to hide the hard drives at the kernel level, seems like I've seen boot options that can do this. Then you wouldn't have to worry about it at all. Really Kiosk doesn't make since for a Live CD (since most changes are lost on reboot regardless), unless the Live CD wanted to be used to demo kiosk."
    author: "Ian Monroe"
  - subject: "SUSE/Novell - Gnome or KDE"
    date: 2004-08-11
    body: "The comment on http://dot.kde.org/1091963184/1091974660/ gave me the impression that we would get some more insight information from a SUSE/Novell-employee regarding the direction SUSE/Novell is taking. Don't get me wrong I think Kiosk is very great software but I think for the moment people are more interested in what is happening behind the scene at Novell. I wish more questions in the interview were about the new SUSE (Novell)."
    author: "AC"
  - subject: "Re: SUSE/Novell - Gnome or KDE"
    date: 2004-08-12
    body: "Kiosk is absolutely essential for enterprise rollouts and integration into good Novell technology like Zenworks etc. Other desktops do not have such a technology, nor is it embedded throughout the desktop.\n\nThe answer is that nothing much is happening, and the usual silly announcement from the Ximian people gave the impression that a desktop would be released this autumn and it would support only Gnome blah, blah, blah. It was basically a bit of internal selling (Gnome stuff does this but KDE doesn't) done publicly. Their PR doesn't really do them any favours.\n\nBasically, they have a Suse Linux Desktop and it can run both Gnome and KDE right now, although there are the usual refinements such as YaST etc. People can choose. Integration of Open Office etc. is going on on both platforms. Over time it looks as if Novell/Suse will gravitate to one in terms of the underlying technology etc it will use. They have already done so in Suse 9.0, 9.1 and 9.2 as well. Suse's use of KDE has a bit of an advantage here as it is part of an existing product line and is being iteratively developed and sold as a commercial product - which means it is generating interest, PR and revenue now. None of the Ximian oriented stuff is. Suse have publicly stated that they would only ever support KDE in the Personal Edition. Those are the only facts we can go on, not the usual \"we are doing this\" and hearsay stuff.\n\nHowever, Novell are getting this process right. If they have to pick one (they may pick a base, but I can't see stuff like Mono not being used in some capacity) then they will look at the technology, the companies they can partner with, the existing popularity and feedback from users. Basically, everything that matters to Novell's customers and Enterprises that wasn't done with UserLinux.\n\nOne of the things that will go the journey (and will have to) is the notion that you can bring free software development in-house and lavish resources on hacking on it. If you want to do that you need companies to partner with who respect free software (Trolltech, MySQL etc), and if you want general free software you need a community to share the load. That's the way the open source community works, and Novell can't do it by themselves. The free desktop direction Novell is taking has to pay for itself.\n\nI don't think we'll see any change of direction at all until well into 2005."
    author: "David"
  - subject: "Re: SUSE/Novell - Gnome or KDE"
    date: 2004-08-12
    body: "I really wonder why Suse is unable to stop Ximians FUD. A first step could be to bring Ximian under control of Suse.\n\nAnti-KDE bashing harms customers relations of SuSe. And in fact I wonder why they tend to speak in the name of SuSe while SuSe/Novell officals tell you a different story."
    author: "gerd"
  - subject: "Re: SUSE/Novell - Gnome or KDE"
    date: 2004-08-12
    body: "> Anti-KDE bashing harms customers relations of SuSe\n\nI agree. And I say it as a SUSE customer (personal and corporate). Employee re-training ahead? Hmm, no thanks. Unless you can show some real vital benefits from doing so. So far, this has worked quite well enough."
    author: "anonymous"
  - subject: "Re: SUSE/Novell - Gnome or KDE"
    date: 2004-08-12
    body: "Which SuSe/Novell officals and which story?"
    author: "Anonymous"
  - subject: "Re: SUSE/Novell - Gnome or KDE"
    date: 2004-08-12
    body: "There were a few questions put to him explicitly asking for comments, but though perhaps illuminating they were uninteresting and off-topic. Basically, he just pointed out (as in this interview) that SuSE are actively developing KDE stuff, and that Novell Linux Desktop will ship both with KDE and GNOME."
    author: "aKademy Team"
  - subject: "Re: SUSE/Novell - Gnome or KDE"
    date: 2004-08-12
    body: "Another question:\n\nWill it be the Novell Linux Desktop for KDE and the or SuSE Linux Desktop for GNOME, or is the SuSE Linux Desktop being renamed to Novell Linux Desktop?\n\nCb.."
    author: "charles"
  - subject: "Re: SUSE/Novell - Gnome or KDE"
    date: 2004-08-12
    body: "In the core market Suse is a better trademark than Novell.\n\nNaming does not matter. Suse also ships Gnome, so nothing new. \n\nThe customer decides at SuSe, not Ximian developers. "
    author: "grintz"
  - subject: "Re: SUSE/Novell - Gnome or KDE"
    date: 2004-08-16
    body: "> Will it be the Novell Linux Desktop for KDE and the\n\nThe Novell Linux Desktop will ship KDE as well as Gnome. Both environments will equally be treated in terms of integration and default selection. The Novell Linux Desktop is mostly targeted at enterprise customers (Like the SUSE Linux Enterprise Desktop was). \nSUSE Linux Personal as well as as SUSE Linux Professional will continue to exist (9.2 is already in the works) and will target mostly home users and professionals."
    author: "nld-man"
  - subject: "Re: SUSE/Novell - Gnome or KDE"
    date: 2004-08-12
    body: "\"the Kiosk Admin Tool, has been specifically written for Novell's upcoming Novell Linux Desktop.\""
    author: "Anonymous"
  - subject: "kiosk in real world use"
    date: 2004-08-12
    body: "nice interview, Waldo =)\n\nbtw, i've used the kiosk framework in a few real world deployments. the biggest was a little over a hundred systems with a few different kiosk profiles available depending on the group the logged in user belongs to. it works really nice and is not just a \"bonus feature\" but in some cases a \"must have\". the breadth of coverage of kiosk is impressive, as well. kudos to Waldo and everyone else who has worked hard on this technology."
    author: "Aaron J. Seigo"
  - subject: "user notification available?"
    date: 2004-08-12
    body: "A while ago I tested locking some features by editing their rc files manually. I learned that the poor user (me, testing) has no feedback that an option is made immutable.\nSo users basically notice that switching such an option seems to have no effect.\n\nI wonder if there is a way (for the kiosk framework) to indicate that an option is immutable, be it greying it out or even taking it (-s GUI setting) away? Or could that be planned?"
    author: "Andreas Leuner"
  - subject: "Re: user notification available?"
    date: 2004-08-12
    body: "The new KConfig XT framework takes care of those things but it will take some significant work to get all setting dialogs migrated to that."
    author: "Waldo Bastian"
  - subject: "Re: user notification available?"
    date: 2004-08-13
    body: "Really? That's quite cool. I'll have to look over the stuff that Zack posted about it."
    author: "David"
  - subject: "http://openkiosk.sourceforge.net/"
    date: 2004-08-12
    body: "Openkiosk is certainly another nice independent effort (based on Waldo's KDE Kiosk framework) to offer a kiosk management system in a multi-plattform network (KDE and Windows that is).\n\nCheck it out!\n\nhttp://openkiosk.sourceforge.net/"
    author: "kiosk"
  - subject: "Re: http://openkiosk.sourceforge.net/"
    date: 2004-08-13
    body: "Looks more like something internet cafes will want to use, not other environments, in contrary to the broad approach used for KDE Kiosk. But with that specialization it looks very nice and complementary to Kiosk indeed."
    author: "Datschge"
---
For the third interview in our series previewing aKademy we approached Waldo Bastian, whose most recent major project has been the Kiosk framework. Tom Chance and Fabrice Mous talked to him about Kiosk, the new Kiosk Admin Tool, Linux on the corporate desktop and what we can expect from <a href="http://conference2004.kde.org/cfp-userconf/waldo.bastian-kde.kiosk.mode.php">his talk</a> at <a href="http://conference2004.kde.org/">aKademy</a>. Don't miss the previous interviews with <a href="http://dot.kde.org/1091963184/">Matthias Ettrich</a> and <a href="http://dot.kde.org/1091772577/">Nils Magnus</a>.







<!--break-->
<p><b>Q: First of all can you explain who you are and what is your role in the KDE-project?</b></p>

<p>Waldo Bastian: My name is Waldo Bastian, I started working on KDE in 1998, since 1999 I work on KDE for SUSE LINUX. My main focus is to provide the underlying non-GUI technology for KDE applications, examples of that are the IO-slave handling, command line parsing, handling of temporary files, configuration files, the internal generation of the KDE menu and DCOP (inter-process communication).</p>


<p><b>Q: What is Kiosk?</b></p>

<p>WB: KDE has many powerful features and possibilities. However, there are situations where it is desirable to reduce the number of features and 
possibilities. In a work situation for example you may want to provide workers with a desktop environment that is streamlined for the tasks that need to be done. Excess functionality can be distracting in such case. KDE's Kiosk framework makes it possible for a system administrator to turn off certain KDE features.</p>


<p><b>Q: What's the focus of this project and how did it start?</b></p>

<p>WB: The focus was originally on public terminals (hence the name Kiosk) and Internet cafes but it became soon clear that similar functionality would also be very valuable in other environments such as schools or enterprise use.</p>


<p><b>Q: Are you paid to work on Kiosk?</b></p>

<p>WB: I am full-time employed by SUSE LINUX and the graphical management tool, the Kiosk Admin Tool, has been specifically written for Novell's upcoming Novell Linux Desktop. </p>


<p><b>Q: When was Kiosk first integrated into the KDE framework?</b></p>

<p>WB: The first version of the Kiosk framework was introduced in KDE 3.0. Since then it has been improved and fine tuned. The result is a quite mature Kiosk framework in KDE 3.2 that has been successfully used in several large deployments. Recently I also started with a graphical management tool, the Kiosk Admin Tool, which should make it easier to take advantage of the Kiosk features.</p>


<p><b>Q: Can you tell us more about the Kiosk Admin Tool? Will it be integrated into KDE's release schedule, and moved out of kdeextragear?</b></p>

<p>WB: The Kiosk Admin Tool is a graphical tool for managing KDE's Kiosk framework. It allows you to create default profiles for groups of users and provide each of these groups with a tuned default desktop. The Kiosk Admin Tool is released independently from the major KDE releases which allows me to incorporate feedback faster, and I have no plans to move it out of kdeextragear. I expect that most Linux distributions will include the Kiosk Admin Tool together with KDE. Ask your distributor for it if you can't find it in your favorite distribution.</p>


<p><b>Q: Is there more information for system administrators who would like to use the Kiosk-framework?</b></p>

<p>WB: Every KDE system administrator should have a bookmark to the <a href="http://www.kde.org/areas/sysadmin/">KDE for System Administrators</a> pages. People interested in the Kiosk Admin Tool will want to keep an eye on <a href="http://extragear.kde.org/apps/kiosktool.php">its homepage</a>. To be informed about the latest releases and to get answers to your questions or help with problems there is the <a href="http://mail.kde.org/mailman/listinfo/kde-kiosk">kde-kiosk mailinglist</a>.  Response times may vary though, especially during KDE releases.</p>
    
 
<p><b>Q: It seems that a lot of distributions are offering enterprise solutions. Is Kiosk part of some of those solutions?</b></p>

<p>WB: Kiosk and the Kiosk Admin Tool will be part of Novell's upcoming Novell Linux Desktop.</p>


<p><b>Q: Do you have examples of organizations using KDE as the Kiosk framework?  </b></p>

<p>WB: I do not keep track but there are quite a few internet cafe's using KDE and Kiosk. See <a href="http://enterprise.kde.org/interviews/publicinternet/">http://enterprise.kde.org/interviews/publicinternet/</a> for example. It's also quite popular with schools. The kde-kiosk mailinglist has more than 200 subscribers and I suspect that many of them take advantage of KDE and its Kiosk features within their organization.</p>


<p><b>Q: What does the project need the most now?</b></p>

<p>WB: Feedback. I want to hear from you if you use KDE's Kiosk features. I want to hear it when you are happy with them and I want to hear it when you have problems with it.</p> 


<p><b>Q: Do you think there is a place for KDE Desktop Enterprise as companies using Microsoft Windows desktops are overwhelmingly dominant. Why should a company opt for a Linux desktop?</b></p>

<p>WB: There are of course cost saving aspects but I think the most important reason for companies to go with KDE is that it puts the company back in control over their corporate desktops. With KDE your IT department gets new opportunities to help make your desktop workers more productive instead of spending all day fighting to prevent things from falling apart.</p>

 
<p><b>Q: You are one of the speakers on aKademy. What's your talk about? Who should be attending your talk?</b></p>

<p>WB: Both my tutorial and my talk are aimed at system administrators that want to deploy KDE and that want to take full advantage of the features that KDE has to offer to system administrators.</p>

