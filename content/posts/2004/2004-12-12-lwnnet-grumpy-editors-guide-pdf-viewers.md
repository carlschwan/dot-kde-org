---
title: "LWN.net: The Grumpy Editor's Guide to PDF Viewers"
date:    2004-12-12
authors:
  - "wbastian"
slug:    lwnnet-grumpy-editors-guide-pdf-viewers
comments:
  - subject: "kghostview vs kpdf"
    date: 2004-12-12
    body: "So, if kpdf starts adding support for different file formats and all these other cool features, does this mean that kghostview will get phased out?  I've always felt that kpdf was kind of unnecessary since it seemed to have just a subset of the features of kghostview, but it looks like soon it will be the other way around.  Thoughts?"
    author: "Ari"
  - subject: "Re: kghostview vs kpdf"
    date: 2004-12-12
    body: "Kpdf does not handle postscript files."
    author: "Luciano"
  - subject: "Re: kghostview vs kpdf"
    date: 2004-12-12
    body: "So as with the editor Kghostview will remain because it does something which KPdf can't, right?\n\n\nOh!\n\nWhat is most important to me is speed. \n\n\n\nI think many Windows-users would like to pay a lot of money for a fast pdf browser plugin. Adobe's Reader is a pain and under Linux it is outdated."
    author: "Bert"
  - subject: "Re: kghostview vs kpdf"
    date: 2004-12-13
    body: "You didn't read everything Enrico said or his English is too bad? :-D\n\n***********\nSupport more file formats. I think the hard part for this is done. I think that PS will come first, then maybe DVI, txt...\n***********\n\nAlmost everything in kpdf is abstracted, i mean, we have a PDF generator class that returns things the skeleton of the apps uses, you could program a PS generator class that provided the same things our PDF generator class provides and the skeleton would be the same. This is the theory and we would probably have some problems as formats offer a different subsets of things, but the viewer would mostly work."
    author: "Albert Astals Cid"
  - subject: "Speed"
    date: 2004-12-12
    body: "In my experience (freebsd 5.2.1, kde 3.3.1), kpdf if several orders of magnitude faster when rending certain things.  In particular, items with high vector contents, such as the maps available here:\n\nhttp://www.cia.gov/cia/publications/factbook/docs/refmaps.html\n\nHowever, the loss of vertain functionality sometimes requires that I use kghostview, but rarely.  The 30 seconds to draw the map, compared to instant drawing in kpdf is a huge difference."
    author: "Troy Unrau"
  - subject: "Re: Speed"
    date: 2004-12-13
    body: "And that certain functionality is? We can fix it if we don't know it."
    author: "Albert Astals Cid"
  - subject: "Re: Speed"
    date: 2004-12-13
    body: "I meant \"We can't fix it if we don't know it.\""
    author: "Albert Astals Cid"
  - subject: "Re: Speed"
    date: 2004-12-13
    body: "it's not something kghostview provides but two killer features for kpdf would be:\n1. Cut&Paste/Find Text functionality\n2. A print manager that prints what I see on the screen and where I can drag and drop pages to an output preview.\n\n(Let me elaborate on the second point. Too often if the pdf is not perfect/too complex Linux pdf viewers ruin the page size/aspect ratio or print the text upside down. Now the view options often allow to correct that but when you want to print the pdf it absolutely doesn't work and all print settings etc are useless.\nIn addition I often want more than one page on a single sheet of paper. Using the kprinter filters is unreliable and most times I need 5 different tests with obscure settings (enlarging the margins suddenly rotates the text and the like) before the output is satisfactory. I want a print preview that gives me a sheet of paper where I can then drag&drop as well as resize and rotate pages and kpdf then logically applies the order to the rest of the document (if I dnd page 1, 2 and 3 on a page it should continue); you could even allow regular expressions for page numbers but I don't think too many people would need that it would be neat though)"
    author: "anonymous coward"
  - subject: "Re: Speed"
    date: 2004-12-13
    body: "1 is already done in the experimental branch\n\n2 would be a good feature but i think it'll have to wait :-D"
    author: "Albert Astals Cid"
  - subject: "How about full screen?"
    date: 2004-12-13
    body: "One key feature that I haven't been able to find in kghostview or kpdf right now is the ability to switch full screen. I mean, *real* full screen, not the \"full screen\" mode of kghostview which keeps the scrollbars and makes an awfull transition between slides. I'm still using acroread because it has a really neat full screen mode that is suitable for presentations. I think having this feature is of higher importance than copying images or text from the document.\n\nI really hope it is going to be implemented in one of these two tools (which are still great anyway) soon. Or even better - in the two. What's the point of using free software to make your slides if you have to rely on proprietary software to display them properly to the audience?"
    author: "Gnurou"
  - subject: "Re: How about full screen?"
    date: 2004-12-13
    body: "just to confirm - your wish is to hide the scrollbars when you do fullscreen?\nHow do you move about the page?  or would you want it to automatically zoom to best-fit?\n\nAlso what do you want changed with the transitions exactly?"
    author: "JohnFlux"
  - subject: "Re: How about full screen?"
    date: 2004-12-13
    body: "Yes - a fullscreen mode is suitable for doing presentations if it fills these three requirements:\n\n- The screen is only occupied by the current slide (no gui),\n- The slide exactly fits the screen and is well rendered,\n- The transition between slides (going next page/previous page) are smooth, in the sense that you don't see the next slide being drawn element by element when you switch between two slides.\n\nSo far, kghostview and kpdf lack such a fullscreen mode (and seeing the comments below, I'm not alone to think that :)). I guess the two first points can be easily fixed, and the rendering is already good for both tools. The third point would require some sort of double-buffered rendering, where the next page is rendered off-screen before being displayed. I guess this is what acroread does, since there is a slight delay between pushing the pagedown key and actually seeing the slide appearing.\n\nTo have an exact idea of what I mean, just run acroread on a slides presentation and switch to fullscreen mode, then navigate between the slides. Something as neat as fullscreen mode is badly needed, I think."
    author: "Gnurou"
  - subject: "Re: How about full screen?"
    date: 2004-12-13
    body: "Ooops - my remark about transitions only applies to kghostview, where it is clearly apparent that the slide is drawn from top to bottom on a white background. Kpdf does them right."
    author: "Gnurou"
  - subject: "PDF Viewers and presentations"
    date: 2004-12-13
    body: "I create PDF presentations. As far as I can see neither KPDF nor KGhostview support the fullscreen mode I need for this. Adobe Acrobat does.\n\nLaurie"
    author: "Laurie Savage"
  - subject: "Re: PDF Viewers and presentations"
    date: 2004-12-13
    body: "Or: Fullscreen in tabbed fullscreen browser\n\nI use a webbrowser as my primary interface. "
    author: "aile"
  - subject: "Re: PDF Viewers and presentations"
    date: 2004-12-13
    body: "Will kpdf support all the transition effects in pdf \nfile created with latex prosper class ?  This is my \ngreatest wish as far as kpdf is concerened.  Plus, a \ntruly full screen mode to show the presentations.  With \nlatex prosper class we can abandone powerpoint for at \nleast presentations with high mathematical content.  And \nwith kpdf with support for various effects one won't \nhave to go to windows and acrobat reader to show the \npresentation\n\nAsk"
    author: "Ask"
  - subject: "Still not good enough :("
    date: 2004-12-13
    body: "I wish that once come a day that I can abandon acroread to view PDFs generated by Illustrator or Scribus. So far Acrobat Reader is the only PDF viewer that gets transparencies, embeded fonts and colours right."
    author: "testerus"
  - subject: "Re: Still not good enough :("
    date: 2004-12-13
    body: "What do you mean with \"still\"? Are you using CVS version from experimental branch?"
    author: "Davide Ferrari"
  - subject: "Re: Still not good enough :("
    date: 2004-12-13
    body: "Not yet. How is kpdf more advanced than plain xpdf? Does the CVS-HEAD-Version work with kdelibs 3.3.x ? Is there a dedicated homepage or mailinglist?\nIt might be a good idea to ask the scribus mailinglist for testing. Problems with pdf viewers are a common topic. At the moment running acroread or even Acrobat Reader in Wine is recommend. (http://ahnews.music.salford.ac.uk/scribus/documentation/wine.html)\n"
    author: "testerus"
  - subject: "Re: Still not good enough :("
    date: 2004-12-14
    body: "<p>Well I wrote those docs, so a bit of explanation and some additional info:\n</p>\n<p>Scribus is arguably the most capable PDF generator of any OSS application. A bit of a brag  I know, but I also have the testing with professional PDF pre-press tools to back it up;). Scribus also generates almost the complete set of PDF presentation effects, including: articles,bookmarks, annotations, PDF forms (submit-table with php) and full screen transition effects, javascript in PDF etc .  Nothing in the OSS world has come along until Scribus to push the edge on these features <strong>and</strong> be so capable in pre-press/commercial printing.</p>\n<p>I've tried CVS KPDF a week or so ago and it is a <strong>major </strong>improvement and very encouraging to see. Kudos to the devels. It is far more user friendly than xpdf and has many of the things missing from other OSS viewers. I tested it with a PDF from Scribus, which usually breaks every OSS viewer I have ever tried except for GSview and Ghostscript. Excepting the missing annotations, it rendered just fine. </p>\n<p>The reason we recommend Acro Reader is mostly no other PDF viewer properly displays the following: 1) CMYK colors in PDF 2) All the Javascript and PDF Form Features 3) ICC Color Management 4) Correct display of transparency, which is part of the PDF 1.4 spec. 5) Some PDF viewers ignored embedded fonts. 6) Wine does a pretty respectable job of running both Acro Reader and the full Acrobat Professional 5.0.5. Not 6+ yet. So, it is a diffcult recommendation  to make, sadly...,but in honesty to our users, no other choice at the moment. </p>"
    author: "mrdocs"
  - subject: "kpdf bug..."
    date: 2004-12-13
    body: "I ran into a bug in kpdf a few months back, and I'm wondering if its still around or not..\n\nI tried to print a document through kpdf, but it when the printout finished, it was missing the images..  Which made my doc kinda useless (it was mostly screen shots and the like..)\n\nThe space was there for the images, just no images..\n\n--garion"
    author: "garion"
  - subject: "manipulating PDF files"
    date: 2004-12-14
    body: "I would love to see KPDF being able to rotate, crop, extract, delete pages etc. Stuff which usually only the professional Adobe Acrobat can do. It's very useful to fix scanned documents. "
    author: "Benjamin"
  - subject: "Removing colors"
    date: 2004-12-21
    body: "The feature I would appreciate most (apart of text searching and text/image selections) would be the possibility to remove the dark/gradient background and inverse the colors. So I could print some white-on-black sldes without wasting a lot of ink."
    author: "Maciej Dems"
---
As part of its <a href="http://lwn.net/Articles/grumpy-editor/">Grumpy Editor series</a>, <a href="http://lwn.net/">LWN.net</a> looks at <a href="http://lwn.net/Articles/113094/">PDF viewers</a> including the KDE applications KGhostview and KPDF. The author was impressed by KGhostview and sees a bright future for KPDF.








<!--break-->
<p><em>"In general, the interface provided by KGhostview is as nice as any PDF viewer your editor has been able to find. It is clearly a tool which has received some serious thought - and use - by its developers."</em></p>

<p>About KPDF the author says the following: <em>"It does appear that further work is being done with kpdf, at least if one goes by
<a href="http://static.kdenews.org/fab/screenies/kpdf/kpdf_singlepage_contents.png">some</a>
<a href="http://static.kdenews.org/fab/screenies/kpdf/kpdf_continous_search.png">screenshots</a>
<a href="http://static.kdenews.org/fab/screenies/kpdf/kpdf_2p_continous_popup.png">linked</a>
<a href="http://static.kdenews.org/fab/screenies/kpdf/kpdf_2pages_fitPage.png">to</a>
 by <a href="http://dot.kde.org/">KDE.News</a>. The images suggest that the current development version supports multiple-page displays, string searches, and more. A future kpdf could well be best PDF viewer of them all; the current version is too unfinished to be usable, however."</em></p>

<p>KDE.News contacted KPDF developers
<a href="mailto:tsdgeos@terra.es">Albert Astals Cid</a> and
<a href="mailto:eros.kde@email.it">Enrico Ros</a>
for some comments on the future plans. Enrico Ros has been working for the last few months on new features in the <em>kpdf_experiments</em> CVS branch. Albert Astals Cid says: <em>"For KDE 3.4 I hope to convince Enrico to stop doing wonderful features and begin merging the kpdf_experiments branch. That includes continuous page mode, 2 page side by side mode, search, 
PDF file properties dialog, table of contents, copy text from the document, 
copy images from the document to file or clipboard, some accessibility things 
like invert colors, change paper color, draw lines around images, etc. I 
think bookmarks and annotations will probably have to wait till after KDE 3.4."</em></p>

<p>Enrico Ros adds: <em>"I hope to do the merge between now and next weekend. A couple of things more: speech synthesis on text selection with kttsd 
which is automatically detected at runtime if it is installed.
Lots of speed and happiness for the end user. We have an underlying engine for 
doing very cool gfx effects too... and in a fast way. I don't know if 
we will be able to take full advantage of its features for KDE 3.4 though."</em></p>

<p>A complete overview of KPDF's current status and future plans can be found in its
<a href="http://webcvs.kde.org/kdegraphics/kpdf/kpdf/TODO?rev=1.3.2.51&only_with_tag=kpdf_experiments&view=markup">
TODO file</a>. <em>"For the future we plan to add a complete set of annotation tools. Some colored highlighters, some sticky papers, a couple of pencils and a rubber. Support more file formats. I think the hard part for this is done. I think that PS will come first, then maybe DVI, txt... It depends on the code-base we'll get attracted. We also want to offer online (and offline maybe) translation for the document/page/selection and make some  accessibility improvements."</em> Enrico concludes.</p>

If you have suggestions for useful or cool features that you would like to see in the 
next release or just want to thank the developers for their work you can contact the
KPDF developers directly by e-mail.



