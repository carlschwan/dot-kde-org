---
title: "KDE-CVS-Digest for January 16, 2004"
date:    2004-01-17
authors:
  - "dkite"
slug:    kde-cvs-digest-january-16-2004
comments:
  - subject: "Sad that it was a mistake"
    date: 2004-01-17
    body: "KDE - the only desktop where a woman leads the commit statistics. ;-)"
    author: "Anonymous"
  - subject: "Re: Sad that it was a mistake"
    date: 2004-01-17
    body: "it's cool.. :)\n\nA very good translator/writer..."
    author: "Stephan"
  - subject: "Re: Sad that it was a mistake"
    date: 2004-01-17
    body: "You don't know women who code, or?"
    author: "Anonymous"
  - subject: "Re: Sad that it was a mistake"
    date: 2004-01-18
    body: "Eva is a coder as well.\n\nAnne-Marie Mahfouf created and maintains a couple of applications of KDE Edu.\nIn fact she is the module maintainer.\n"
    author: "Kevin Krammer"
  - subject: "Re: Sad that it was a mistake"
    date: 2004-01-17
    body: "But how is it possible; 54 lines changed in 308 commits...?\n"
    author: "Richard"
  - subject: "Re: Sad that it was a mistake"
    date: 2004-01-17
    body: "It wasn't really a mistake. Eva created a whole bunch of directories under kdenonbeta/kdedebian/livecd/gui/home/.kde/*.\n\nThere is a way to do it all in one shot, without triggering emails to the kde-cvs list for each one. But it happened one by one, thus the large numbers.\n\nderek"
    author: "Derek Kite"
  - subject: "Internationalization"
    date: 2004-01-17
    body: "I see that more languages have hit 100% than the last time I looked a couple of weeks ago."
    author: "Turd Ferguson"
  - subject: "Re: Internationalization"
    date: 2004-01-18
    body: "See i18n.kde.org/stats...\nactually more than 3 teams reached 100% since in that stat is included\nkoffice HEAD (while translation is going on in koffice1.3 branch) and kdeextragears that are not released with KDE 3.2\n"
    author: "ac"
  - subject: "amaroks sonograph"
    date: 2004-01-17
    body: "Here is a screenshot for the curious: <a href=\"http://members.aon.at/mfranz/sonograph.png\">http://members.aon.at/mfranz/sonograph.png</a> (12kB)"
    author: "Melchior FRANZ"
  - subject: "Re: amaroks sonograph"
    date: 2004-01-17
    body: "OMG.  That's it. You can now dump Noatun (for which I never cared for, no offence) and Kaboodle (something I never understood why it was there) and standardize on AmaroK for you non-jukebox music player.  Please.\n\n...actually, can it deal with video, too, like Winamp?\n\nThis is really neat.  I will now be able to dump XMMS and go with a fully KDE-saavy music player.\n\n"
    author: "John D. Smitte"
  - subject: "Re: amaroks sonograph"
    date: 2004-01-17
    body: "No I've never cared for them, or used them. I think some clean up is probably necessary. I managed to try out Juk recently, and I liked it. There are some things to improve, which I think people know about anyway. With these made and maybe some nice Karamba extensions so you can do things without viewing the whole app I'm seriously going to enjoy it. I've heard some accusations that it looks like iTunes and people will get confused - rubbish!\n\nPersonally I don't like combined video/audio/music applications (think Windows Media Player). They are just too cluttered and are not too good at one and not too good at the other, although Juk can be a means to organise all of your video stuff and play it through another media player. Kaffeine maybe.\n"
    author: "David"
  - subject: "Re: amaroks sonograph"
    date: 2004-01-17
    body: "Actually I would prefer a streaming server style (like mentioned in http://dot.kde.org/1074190748/), but maybe mplayer's streaming server might/would do also, mplayer at least would support much more video formats and is quite nicely embeddable to the kde with kmplayer/mplayerplug-in. :-)\n"
    author: "Nobody"
  - subject: "Re: amaroks sonograph"
    date: 2004-01-18
    body: "> mplayer at least would support much more video formats and is quite nicely \n> embeddable to the kde with kmplayer/mplayerplug-in\n\nJust what is it that I don't understand?\n\nYou are comparing: \"mplayer\" to what.\n\nYou should be comparing it to XINE which is what N/K use as a backend.\n\nWhich video formats does mplayer support which XINE doesn't.\n\nN/K play QuickTime movies with sound OK on my system.  \n\nNote: what doesn't work is the KDE Embedded Media Player.  But that is a bug issue.\n\nYou should also consider that mplayer is video card dependent where XINE is not.  This is one of the reasons that KDE chose XINE over mplayer.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: amaroks sonograph"
    date: 2004-01-17
    body: "JuK, amaroK, Kplayer - that's my personal KDE music/video suite."
    author: "Eike Hein"
  - subject: "Re: amaroks sonograph"
    date: 2004-01-17
    body: "I second that motion!"
    author: "Dominic Chambers"
  - subject: "Re: amaroks sonograph"
    date: 2004-01-17
    body: "i personally like amarok and kaffeine, although kplayer and juk are also quite capable.\n\nnoatun sux."
    author: "anon"
  - subject: "Re: amaroks sonograph"
    date: 2004-01-18
    body: "> can it deal with video, too, like Winamp?\n\nNo. I'ts a music player."
    author: "Melchior FRANZ"
  - subject: "Re: amaroks sonograph"
    date: 2004-01-18
    body: "Does anybody else have a problem with the default amarok visualization (in 0.83) taking up like 80% of the CPU? I've got a 2GHz machine, and whenever the Amarok window is visible, things start getting jerky.\n"
    author: "Rayiner Hashem"
  - subject: "Re: amaroks sonograph"
    date: 2004-01-18
    body: "Never seen that. The cvs version uses around 0.3% on my computer (2.4GHz)."
    author: "Melchior FRANZ"
  - subject: "Re: amaroks sonograph"
    date: 2004-01-18
    body: "He means cvs, and the distort widget is default. It takes up 100% on my Athlon 1800XP.\n\nI think it's likely we'll set the default to one of the others for the next release. It's only cvs that has distort as the default."
    author: "Max Howell"
  - subject: "Unify a bit"
    date: 2004-01-17
    body: "IMHO I would like to see all those tiny (specific use) apps to be merged, for example why should user know a thing about different document/image (actually any file format that can be just viewed without any interaction) formats, thus I would like to see kpdf, kdvi, kghostview, kfaxview and kview to be combined as a general purpose \"viewer\" for KDE.\n\nThat kind of combining might also be used on other type of filetype handlings, like merging all separate text editors together, like kate, kedit, khexedit and kwrite to become a text editor that might have different modes to be accessed via similar method as konqueror toolbar changes for task in hand (or vertical tabs on the sidebar to choose how to highlight the text?).\n\nBigger change that I would love to see would include merging of apps that actually perform similar tasks, like kooka that might include support for inporting images from any source, like scanner, digital camera, web camera, gd and similar inputs, with gocr of course. :-)\n"
    author: "Nobody"
  - subject: "Re: Unify a bit"
    date: 2004-01-17
    body: "> I would like to see kpdf, kdvi, kghostview, kfaxview and kview to be combined as a general purpose \"viewer\" for KDE.\n\nAlready done, it's called \"Konqueror\"."
    author: "Anonymous"
  - subject: "Re: Unify a bit"
    date: 2004-01-17
    body: "Hmm... Maybe I have talked about this with you already?!? :-D\n\nAnyways, I would like to see those progs to be eliminated and to use a \"kview symbolic link\" to konqueror then.\n\nWhat way I should configure my system that konqueror would show only wanted toolbars with items I would like (as a clean view window)?\n"
    author: "Nobody"
  - subject: "Re: Unify a bit"
    date: 2004-01-17
    body: "Being able to use konqi to view all those types (ps, pdf, dvi, the fax format, images, ...) doesn't make the plugins' GUIs consistent, unfortunately.  \n\nNow if all of them offered the same features (e.g. preview bar on the left for multipage documents, selection of pages, mouse wheel zoom) in a consistent way...\n\n"
    author: "cm"
  - subject: "Re: Unify a bit"
    date: 2004-01-17
    body: "That would be very great thing to have as it's just silly to have exactly same functionality, but just differently showed to the user, kde should embrace HIGs on that too.\n"
    author: "Nobody"
  - subject: "Re: Unify a bit"
    date: 2004-01-18
    body: "Yes, that is true.\n\nNo, I think that he has a point.\n\nSince some (or all) of these \"applications\" are built with a front end and a KPart.  It should be possible to make a single KViewer front end for these various parts so that they could be used without opening Konqueror.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Unify a bit"
    date: 2004-01-17
    body: "You're not the only one who feels that way. I think we'll see a lot of that happen in KDE 4.0 - unifying things, throwing out old and obsolete apps. You can't really do it for a 3.x for compatibility reasons, just like you can't change the default GUI theme with every point release. But I expect some major clean-ups for the next big release."
    author: "Eike Hein"
  - subject: "Re: Unify a bit"
    date: 2004-01-17
    body: "> IMHO I would like to see all those tiny (specific use) apps to be merged, for > example why should user know a thing about different document/image (actually > any file format that can be just viewed without any interaction) formats,\n> thus I would like to see kpdf, kdvi, kghostview, kfaxview and kview to be\n> combined as a general purpose \"viewer\" for KDE.\n \nIt's not only \"why should the user know\", but also where should a feature request be dropped?  Should the request be sent to all (in my case) kde digital\npicture viewers?  If yes and all the involved apps are registered in \nkezilla the same feature request must be entered multiple times which is to my\nopinion polluting the bug database.\n\nCombining the application, if possible, would be a good thing indeed."
    author: "Richard"
  - subject: "What happens now? "
    date: 2004-01-17
    body: "No source for Crystal icons?\n\nhttp://osnews.com/story.php?news_id=5693"
    author: "Dan"
  - subject: "Re: What happens now? "
    date: 2004-01-17
    body: "it gets sorted out one way or the other and 3.2 gets released regardless of what that ends up meaning. the icons exist in CVS, so there's no reason to hold up the release. and it isn't like someone is holding them hostage, they're just in a rather unfortunate state at the moment. fortunately there are more people of late paying attention to the icons, so i'm sure it'll get sorted out eventually to everyone's satisfaction.\n\nat least Eugenia managed to get a few more pieces of FUD in about KDE and it's level of support from third parties. =/"
    author: "Aaron J. Seigo"
  - subject: "Re: What happens now? "
    date: 2004-01-18
    body: "Doesn't really help that any post directly doubting OSNews.com's journalistic integrity and/or objectivity in the matter gets either deleted or instantly moderated down [at OSNews]. "
    author: "Eike Hein"
  - subject: "Everything's fine."
    date: 2004-01-18
    body: "ISSUE SOLVED! http://www.osnews.com/comment.php?news_id=5693&offset=60&rows=64#188798"
    author: "Alex"
  - subject: "[OT] award for valgrind"
    date: 2004-01-17
    body: "Congratulation Julian :-)\nhttp://opensource.org/OSA/awards.php\n\nBye\n\n Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "remove noatun ?"
    date: 2004-01-17
    body: "I don't understand why people want to remove noatun.\n\nnoatun is for me a very good sound player. It is not perfect but it can be  enhanced.\nnoatun good points :\n- playlist style changeable (Dub, Hayes,...),\n- real[1] changeable GUI (xmms skin, kjofol skin, KDE style,...),\n- Visualization plugin,\n- Other plugins (key, Alarm clock, infrared control,...),\n- Remote control via dcop,\n- Certainly code well organised regarding the pluggueable facilities,\n- Benefit of arts effects that are chainable[2] \n- Well integrated in KDE (native interface),\n- ...\n\nnoatun bad points :\n- No juk style playlist (who make the plugin ?)\n- Some playlist can't have stream url (hayes,...) (Can playlist plugins support .desktop files of type Link ?),\n- Only arts background sound system (I suppose that it can be change without much break : gstreamer ?),\n- Video system which one is the best ? I don't know if noatun use xine or a internal one ?\n\nYou can complete the list if you want :)\n\n[1] Not a hack as in xmms with kjofol skin plugin.\n[2] In xmms only one sound plugin can be used at the same time\n\nSo why not enhance noatun or change bad part of it ?\n\nkaffeine is great for video but I hate it for playing sound as I hate using xine for playing sound ;)\namarok is great but I don't like very much its look & feel : not a native gui (new widgets, new buttons, new color in the list,...) and amarok is still beta (some options doesn't work).\nxmms was good but it is gtk application and it doesn't interract well with KDE application. It has a strange feel  which doesn't respect as the Window Manager  was configured (don't show window content when moving,...). It is gtk 1  application and now gtk 2 is here.  A gtk 2 version will exist but not completely finish yet.\nkplayer, kmplayer, kxine,... very great program for video playing but not for sound.\njuk is very good but doesn't support stream and has some strange behaviour sometimes. Moreover it is not a good program if you want to play only one sound (kaboodle is here for that).\n\nHere is my opinion. I wait for yours ;)"
    author: "Shift"
  - subject: "Re: remove noatun ?"
    date: 2004-01-18
    body: "Having the ability to use plugins is NOT an if there are no good plugins to use!  IMHO all the Noatun playlists suck.  Almost all the GUIs suck.  The visualization plugins are laughable compared to, say, Winamp's.  ARts sucks.  Basically every part of Noatun sucks, except maybe for the Keyz plugin and the system tray interface.  I say take those and dump the rest.\n\nThis is the way it should work.  Instead of having one mediocre audio/video player, KDE should have:\n\n1. A video player that doesn't play plain audio files.  The default GUI should be extremely simple, hiding advanced features like playlists and video processing tools in the menus.  This player should also integrate with Konqueror to play movie trailers on websites and stuff.\n2. An audio jukebox/library application that has plugins and doesn't play videos.\n3. A simple *sound recorder/editor*, like Sound Recorder for Windows, but with a few more editing features.\n4. In the future, when GStreamer matures, a video editor.\n\nAll these apps should use GStreamer (and maybe JACK) instead of aRts.  If KDE had this stuff XMMS might finally be on its last legs, aRts could go back to being a synthesizer instead of an audio framework, and some of the development effort now focused on MPlayer/Xine might go to the much cooler GStreamer project."
    author: "Spy Hunter"
  - subject: "Re: remove noatun ?"
    date: 2004-01-18
    body: "KMplayer already does the first bit of what you want -- integrate with konqueror to play movies on websites. It also has a very simple interface (I'm not sure if it has playlists -- and frankly, I don't see why a video app should).\n\nAs for audio amaroK looks to be coming along really nicely -- I personally wouldn't mind if it played videos too, though there's no real need. A nice interface that looks similar to amaroK's would be cool in a video app, though -- besides unifying things, it would look quite slick.\n\nI also agree that arts needs to be dumped as sound server, unless it can truly be fixed -- but I think it would be better to simply switch to a better system, like JACK or one of the other sound servers. What would be even nicer is if freedesktop.org could choose one, and GNOME and KDE could both use it. That way, there would be a truly \"standard\" platform to write audio apps on for linux."
    author: "Matthew Kay"
  - subject: "Re: remove noatun ?"
    date: 2004-01-18
    body: "freedesktop has already chosen gstreamer\n\nhttp://gstreamer.freedesktop.org/"
    author: "Shift"
  - subject: "Re: remove noatun ?"
    date: 2004-01-18
    body: "Well, \"chosen\" is too strong a word: \n\nFrom http://freedesktop.org/Software/Home:\n--------------------------------------------------------------\nSoftware hosted on/related to freedesktop.org \n\nSome software has made its way here to live. None of this is \n\"endorsed\" by anyone or implied to be standard software, \nremember that freedesktop.org is a collaboration forum, \nso anyone is encouraged to host stuff here if it's on-topic. \n--------------------------------------------------------------\n\nBut still, a unified multimedia framework would be nice. \n"
    author: "cm"
  - subject: "Re: remove noatun ?"
    date: 2004-01-18
    body: "GStreamer is only in Gnomes's freedesktops CVS, that shouldnt mean freedesktop will try to establish it as the default framework in KDE, but you are right by saying that at some point they will.\n\nFreedesktop is kind of a failed experiment, because it smells like glib is going to be used as a foundation instead of something modern (its in dbus too even though it doesnt link, but has code pasted in). By choosing glib freedesktop has already revealed that it was only pretending to be desktop neutral. So the correct name for it is really gnome-feedesktop now.\n\nAnd the fact that lots of old school C- boneheads hang around there doesnt really make it very attractive to a modern KDE developer I could imagine.\n\nAt the moment there is not much there that would make Gnome's freedesktop attractive (code-wise). DBus is slow and unsafe (programmed in C), the HAL is only sysfs parser, keithp graphics work is far away from being an XFree86 replacement, though the most promising part of it.\n\n"
    author: "Nick"
  - subject: "Re: remove noatun ?"
    date: 2004-01-18
    body: "Freedesktop.org is no GNOME invention or spin-off."
    author: "Anonymous"
  - subject: "Re: remove noatun ?"
    date: 2004-01-18
    body: "Glib has not really be chosen. And DBUS has no dependencies (if a few lines are pasted from Glib it does not hurt). Of course there is an API for Glib on top of it, but a Qt API is also provided.\nUsing a unified messaging system is something that can really be useful for KDE. Wheter it is developped in C, or C++ is not really the problem, having more developpers (and users) involved in the project would actually help to make it safer, fix bugs and improve performance.\nAnd for your information, the DBUS developpers have chosen to make it close to the current DCOP implementation in order to simplify its integration into KDE, so really this is not a GNOME-specific technology.\n"
    author: "Benoit W."
  - subject: "Re: remove noatun ?"
    date: 2004-01-18
    body: "> Glib has not really be chosen.\nnot officially, right.\n\n>Using a unified messaging system is something that can really be useful for \n>KDE. Wheter it is developped in C, or C++ is not really the problem,\nactually it is a problem. KDE shines, because it breaks with an old unix legacy to write everything in C. legacy-C applications are a serious security risk and hard to maintain. Just look how many times CUPS is in the security news.\nThere is absolutely no need to write a new piece of software like bdus in C. Since you have to provide a glib wrapper anyway it should better be done in stdc++ or something similar.\n\n"
    author: "Nick"
  - subject: "Re: remove noatun ?"
    date: 2004-01-18
    body: "Well, dbus is mostly a protocol and data marshalling convention.\nYou are free to do a C++ implementation and I am sure freedesktop.org will host it as well.\n\n\n"
    author: "Kevin Krammer"
  - subject: "Re: remove noatun ?"
    date: 2004-01-18
    body: "true. maybe I will :)\nbut Gnome's freedesktop wont like it."
    author: "Nick"
  - subject: "Re: remove noatun ?"
    date: 2004-01-19
    body: "GNOME doesn't \"own\" freedesktop.org."
    author: "Henrique Pinto"
  - subject: "Re: remove noatun ?"
    date: 2004-01-18
    body: "> By choosing glib freedesktop has already revealed that it was only pretending to be desktop neutral.\n\nErm, Freedesktop never \"chose\" glib. It just happens to be an implementation detail of some things, like gstreamer. Freedesktop has a commit policy that pretty much allows anything. Yes, if you want to write something in Qt, you can probably add it to freedesktop cvs. \n\n> that shouldnt mean freedesktop will try to establish it as the default framework in KDE, but you are right by saying that at some point they will.\n\nWell, KDE should consider it's various alternatives, because there is a lot of things besides gstreamer, but, I don't see much of a problem in terms of dependencies switching between arts, which already uses glib, to using gstreamer, which also uses glib. "
    author: "fault"
  - subject: "Re: remove noatun ?"
    date: 2004-01-18
    body: "Uh, I don't see why using GStreamer would be a bad thing, as long as it doesn't have cumbersome or obsolete dependencies. Because many high-visibility Gnome developpers often display an astounding lack of understanding of good software design, doesn't mean that being one suddenly turns all of them into incompetent codepissers. If GStreamer is well designed and not clogged up with ill-thought design goofs and other gnomenesses, I can see absolutely no reason not to use it. I mean, gee, didn't KDE become the leading desktop by always picking the best technologies regardless of politic issues? What remains to be seen is whether GStreamer cuts it as a technology. Note, I have no idea as to whether it does, so please fill free to fill me in, either way. Otherwise, refusing to use it just because it's a Gnome thing, even given Gnome's poor track record, is not a very clever attitude."
    author: "Balinares"
  - subject: "Re: remove noatun ?"
    date: 2004-01-18
    body: "If gstreamer is better than arts, I don't see why it would be a bad thing for KDE to use it. For me, arts never ever ever worked so I would welcome any replacement.\n\n> Freedesktop is kind of a failed experiment\n\nThat's bullshit. Freedesktop is actually a very successful experiment, with established successful important standards that allow (more or less) slick interoperatility between desktops without preventing any of them to innovate. We already have common drag'n drop, independant window manager, common .desktop description and application menu compatilility. I am looking forward for the next advances (shared mimetype database).\n\n> By choosing glib freedesktop has already revealed that it was only pretending to be desktop neutral\n\nThat's bullshit again. At some point, when you write code (do you write code by the way ?), you have to manipulate lists, dictionaries and strings for the very basic minimum. You can use glib, you can use the C++ stl or you can use Qt or you can write your own buggy non-optimised version. But you have to use one and this is no way related to choosing one desktop. The author of DBUS choose glib for obvious reasons: he is familiar with it and he codes in C. Using Qt would have been stupid (why use a GUI toolkit for a non-gui stuff). He could have chosen tinyQ but that's not as official as Qt so it might not be as stable or maintained. What should he have done in your opinion, using its own buggy version of glib ? The only difference would be that you did not know that he used glib. What a huge benefit.\n\n> And the fact that lots of old school C- boneheads hang around there doesnt\n> really make it very attractive to a modern KDE developer I could imagine.\n\nThe same goes for them. The fact that young OO-fan hang around does not make it attractive for old-school C coders neither.\n\nHowever, what makes freedesktop attrative to both groups despite their differences is that they cooperate to provide the user with the best and consistent applications ever.\n\n> DBus is slow and unsafe (programmed in C),\n\nIs this supposed to be \"it is programmed in C so it is slow and unsafe\" ? While I certainly don't love C for many things (especially gui programming), there are some very good and safe programs written with it.\n\nFor DBUS, there are problems with it (although I am not familiar enough) but the concept of having a common communication protocol between gnome and kde apps is very exciting. I hope there is not technical difficulties that can not be overcome with a distant adoption of DBUS for kde.\n\n\n\n"
    author: "Philippe Fremy"
  - subject: "Re: remove noatun ?"
    date: 2004-01-18
    body: "> If gstreamer is better than arts, I don't see why it would be a bad thing \n> for KDE to use it. For me, arts never ever ever worked so I would welcome \n> any replacement.\n\nYes. I like JACK personally.\n\n> That's bullshit. Freedesktop is actually a very successful experiment\n\nPolitically, in the long run I question it.\n\nIsn't glibc part of every major Linux distribution? Why not use it?\n"
    author: "David"
  - subject: "Re: remove noatun ?"
    date: 2004-01-18
    body: "> Politically, in the long run I question it.\n \nA lot of exchanges are only detail dicussions, not disagreement on the major concepts.\n\nIMHO its a pretty cool idea, because it gets people of all relevant parties to start exchanging thoughts very early on, even before some desktop specific or related implementation sets some kind of hard to accept standard.\n\n> Isn't glibc part of every major Linux distribution? Why not use it?\n\nI assure you, glibc is certainly used :)\nThis is the C standard library.\n\nHowever, it does not include containers like the C++ standard lib, that's why C developers use glib.\n"
    author: "Kevin Krammer"
  - subject: "Re: remove noatun ?"
    date: 2004-01-19
    body: "I think that he meant: \"glib\" not \"glibc\".\n\nhttp://www.gnu.org/directory/glib.html\n\nI see nothing wrong with KDE being dependent on: \"glib\".\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: remove noatun ?"
    date: 2004-01-19
    body: "1) glib (the one that has GObject and the event loop) is not a standard. Its some home brewed GNOME stuff they forked out into the public domain.\n\n2) glib is bloated and the only reason for its existance was a missing C++ compiler, like what, about 10 years ago\n\n3) libstdc++ ( the real Thing(R) ), just as an example (applies to some other languages too), is an ISO standard\n\n4) libstdc++ is lean, fast and follows the \"what you dont use doesnt cause overhead principle\" ( unlike glib!!! )\n\n5) lidstdc++ doesnt force an alien event loop on you  "
    author: "Nick"
  - subject: "Re: remove noatun ?"
    date: 2004-01-19
    body: "> I think that he meant: \"glib\" not \"glibc\".\n\nmakes sense.\n\n> I see nothing wrong with KDE being dependent on: \"glib\".\n\nAs far as I understand this from what I read on the mailinglist, one problem is that it does not anything useful to KDE, but I think as it is usually installed anyway this is a minor problem.\n\nThe other problem seems to be that the glib contains two things, on one hand the containers and useful functions for C programming, on the other hand another, incompatible, object model.\n\nOf course it is possible to write a wrapper, but this only covers the part where KDE needs an API to a glib enhanced library.\nMore problematic is that the C++/KDE developer needs to deal with another barrier if he/she wants to implement some changes on the glib enhanced library, for example creating a plugin.\nSo you are in effect depending on someone else to implement your ideas.\n\nSome people like to have more control.\n\nAgain, that's only my interpretation of the problem.\n"
    author: "Kevin Krammer"
  - subject: "Re: remove noatun ?"
    date: 2004-01-18
    body: "> freedesktop has already chosen gstreamer\n\nyou mean, gstreamer has been moved to freedesktop CVS. If someone else wanted, they could move an overlapping piece of software to freedesktop CVS too. "
    author: "fault"
  - subject: "Re: remove noatun ?"
    date: 2004-01-18
    body: "GStreamer was not \"chosen\" by Freedesktop; they asked to be hosted at Freedesktop and such was accepted.  Neither will Freedesktop try to push GStreamer on KDE -- that decision will be entirely up to KDE developers.\n\nIf GStreamer is the best fit and the most capable solution out there we'll use GStreamer.  If it's not we'll use something else.\n\nAll of this was made clear (and definitely agreed to by the GStreamer guys) on the Freedesktop list when GStreamer discussed moving their CVS hosting over to Freedesktop.org."
    author: "Scott Wheeler"
  - subject: "Re: remove noatun ?"
    date: 2004-01-19
    body: ">>(I'm not sure if it has playlists -- and frankly, I don't see why a video app should).\n\nYou've never put on to play a whole load of music videos?  Or watched many episodes of a series?  Or put on a whole load of porn clips?\n\n"
    author: "JohnFlux"
  - subject: "Re: remove noatun ?"
    date: 2004-01-18
    body: "\"The visualization plugins are laughable compared to, say, Winamp's\"\nYes but other player have no visualisation plugins except xmms (and few ones for xine) \n\n\"ARts sucks\" \nNot totally. There are great things in it (Effects chaineable, full duplex,...) but the rest is not good :-/\n\n\"all the Noatun playlists suck\"\nHayes one is great.\n\n1. I vote for kaffeine :)\n2. Juk but it doesn't have plugin yet\n3. krec\n4. k???? \n\nI agry for the use of gstreamer but I suppose that it will come in KDE 4.0 and not before.\n"
    author: "Shift"
  - subject: "Re: remove noatun ?"
    date: 2004-01-18
    body: "> IMHO all the Noatun playlists suck.\n\nNo, Hayes rocks!\n\nDoes any other player provide a playlist like that?"
    author: "Kevin Krammer"
  - subject: "Re: remove noatun ?"
    date: 2004-01-18
    body: "> - Video system which one is the best ? I don't know if noatun use xine or a internal one ?\n\nNoatun can make use of Xine via the Xine aRts-Plugin."
    author: "Anonymous"
  - subject: "Re: remove noatun ?"
    date: 2004-01-18
    body: "So why do you \"hate using xine for playing sound\"?\n\nxine is just an engine - and a pretty good one at that. The player you build around it is an entirely different matter."
    author: "Paul Eggleton"
  - subject: "Re: remove noatun ?"
    date: 2004-01-18
    body: "Well I am taking of the xine GUI. It is not a very good one. The playlist is not very practicle and the toolkit used in xine GUI is ugly.\n\nBut for DVD and video xine, is my favorite video player and it supports very well my xinerama configuration. And the Ctrl-D of xine is great too ;) \nAre there any project in KDE for creating a skin/wallpaper/style/... dowloader as in xine so we don't have to go to kde-look.org ? :)"
    author: "Shift"
  - subject: "Re: remove noatun ?"
    date: 2004-01-18
    body: "> Well I am taking of the xine GUI\n\nXine doesn't have any particular GUI.\nI recommend you to check out e.g. Kaffeine.\nThere you have a great UI for music _and_ movies."
    author: "OI"
  - subject: "Re: remove noatun ?"
    date: 2004-01-19
    body: "Why do I hate/want to remove noatun?\n\nSimple, I've NEVER had it work properly, and that is over a long period of time, using many different distros including one I rolled myself. Now this may come down to it simply not being packaged correctly, but I'm starting to doubt it.\n\nIt crashes constantly. It has got to be one of the most unstable KDE apps I've tried to use in recent times. It doesn't die gracefully when something goes wrong. It usually freezes requiring it to be killed, or the KDE crash report program pops up. A dialog box stating the format you tried to play is incompatible would be a good start.\n\nWhen it crashes, it is usually still running in the background using up a huge amount of processing power requiring it to be killed from the command line. This is not acceptable for any gui app, especially considering how often this one crashes.\n\nIt doesn't seem to play many multimedia files I through at it, and you find that out when it crashes, not when it informs you nicely. As a result I don't trust it to open anything as more often than not it will crash when I try to play a video file I download from the net.\n\nI find the interface is shocking. I can't stand it. It looks very amaturish and cobbled together. I find the playlist to be a chore to use.\n\nSo to sum up:\nBasicly it simply doesn't work often enough or reliably enough to be of ANY use to me. It has the feeling of being a hacked together program that is riddled with bugs, and it is irritating to use.\n\nClearly it does work for some people, so I guess it can't be all bad.... This is in no way an attack on those who've worked on the program. Thank you for your efforts. This is just my experiences using the program."
    author: "Joel Carr"
  - subject: "[ot] mouse buttons"
    date: 2004-01-18
    body: "sorry for being so off-topic...\n\ni have a microsoft intellismouse optical usb, with those extra forward and back buttons. has anybody found a way of making those work?"
    author: "anonymous"
  - subject: "Re: [ot] mouse buttons"
    date: 2004-01-18
    body: "Really not the right place. If you send your realname+email address someone could help you via private mail. But now you have to search alt+f2+\"gg:mousemap x11\" by yourself ;-)\n\nBye\n\n Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: [ot] mouse buttons"
    date: 2004-01-18
    body: "did u ever try lineak?\n\nAFAIK it is http://lineak.sf.net\n\nhth, prego :)\n"
    author: "prego"
  - subject: "Off topic"
    date: 2004-01-18
    body: "OFf Topic, but will KDE 3.2 make it easier to play CDs in your cdrom drive via Konqueror. I think the uability of 3.1 in this specific field is very bad."
    author: "Elektroschock"
  - subject: "Kaffeine Media Player"
    date: 2004-01-18
    body: "Has anyone tried Kaffeine?\n\nThis _really_ seems like the way to go for KDE.\nIt rocks and should be included as default."
    author: "OI"
  - subject: "Re: Kaffeine Media Player"
    date: 2004-01-18
    body: "I have tried it, and you're right, it's very nice.\n\nAs has been mentioned a few times before, in KDE 4.0 there will be a big focus on getting rid of obsolete applications and generally tidying up the KDE application suite. That would be the right time IMO to reasses the video player situation. At the moment we have:\n\n\naKtion - not much use any more, isn't it being phased out already?\n\nnoatun - a fairly basic video player, not much use when you have something more capable that can also be simple to use installed\n\nKaboodle - when it works, it's a very nice simple player, but it rarely seems to work\n\nKMPlayer / KPlayer / Kaffeine - good mplayer/xine based players, they all overlap to some extent on functionality. IMO whichever one can be best integrated into KDE and not depend upon a specific video backend ought to go in, which at the moment would mean KMPlayer as far as I know, since it can use Xine and/or Mplayer.\n\nI'd put Juk and KMplayer forward as apps to make default and make work really well with KDE (i.e. work well as kparts, use kxconfig and all other KDE components correctly, own default mimetype associations). I'd then keep amarok, noatun and kaboodle installed as optional extras that (i.e. in \"other\" area of menu, own no default mimetype associations)."
    author: "Tom"
  - subject: "Re: Kaffeine Media Player"
    date: 2005-01-09
    body: "Hi,\n\nI was sent a dutch DVD; I can't get it to work on my computer here in NZ. Message:\n\"Your DVD is probablycrypted. According to your country laws you can or can't use libdvdcss to be able to read this disk.\"\n\nAny way around this?\n\nThanks,\nDiederic"
    author: "diederic "
  - subject: "Re: Kaffeine Media Player"
    date: 2004-01-18
    body: "I have to agree.\nKDE already has the xine_artsplugin in kdemultimedia so adding\nkaffeine wouldn't add any more dependancies.\n\nAnd kaffeine/xine is the best multimedia player that I've seen.\n"
    author: "John"
  - subject: "Re: Kaffeine Media Player"
    date: 2004-01-18
    body: "Agreed. I've given it a try just yesterday. While I've still encountered a little glitch or two, it's already *VERY* impressive. It manages to be lightweight and unobstrusive and YET do everything it should (including excellent picture post-processing and things of that ilk), and do it damn well. It's already become my default video player. I hope it will make it into the default KDE install rapidly!"
    author: "Balinares"
  - subject: "Why not Kaffeine?"
    date: 2004-01-19
    body: "... it is strange that it has been ignored for so long.\n\nhttp://kaffeine.sf.net/"
    author: "More-OSX_KDE_Interpollination"
  - subject: "Re: Kaffeine Media Player"
    date: 2005-01-15
    body: "I install kaffeine-0.5 tarball on the suse 9.2 get some error like this :(Kaffeine Part...\nPart not found. Please check your installation!                                                                   Loading of player part 'kaffeine_part' failed                                                                     kaffeine_part.desktop not found in search path. ) It can't work.                                  "
    author: "wing"
  - subject: "Re: Kaffeine Media Player"
    date: 2005-02-02
    body: "I get the exact same error.  I installed from a prebuilt binary from packman.links2linux.org  \nrpm -q says \"kaffeine-0.5-0.pm.3\"\n\nWhen kaffeine starts (inspite of the errors) and I start fiddling around (for example go to fullscreen mode), kaffeine crashes.  Here is the backtrace:\nUsing host libthread_db library \"/lib/tls/libthread_db.so.1\".\n[Thread debugging using libthread_db enabled]\n[New Thread 1096240352 (LWP 11487)]\n[KCrash handler]\n#7  0x0806e127 in QObject::parent (this=0x0) at qobject.h:154\n#8  0x0806d7cd in Kaffeine::qt_invoke (this=0x80a07a0, _id=134875040, \n    _o=0xbfffe220) at kaffeine.moc:271\n#9  0x40c7d0ee in QObject::activate_signal ()\n   from /usr/lib/qt3/lib/libqt-mt.so.3\n#10 0x40c7d896 in QObject::activate_signal ()\n   from /usr/lib/qt3/lib/libqt-mt.so.3\n#11 0x40550609 in KAction::activated () from /opt/kde3/lib/libkdeui.so.4\n#12 0x4055f77e in KToggleAction::slotActivated ()\n   from /opt/kde3/lib/libkdeui.so.4\n#13 0x4055989b in KToggleAction::qt_invoke () from /opt/kde3/lib/libkdeui.so.4\n#14 0x405598dd in KToggleFullScreenAction::qt_invoke ()\n   from /opt/kde3/lib/libkdeui.so.4\n#15 0x40c7d0ee in QObject::activate_signal ()\n   from /usr/lib/qt3/lib/libqt-mt.so.3\n#16 0x40fcd1e2 in QSignal::signal () from /usr/lib/qt3/lib/libqt-mt.so.3\n#17 0x40c9a623 in QSignal::activate () from /usr/lib/qt3/lib/libqt-mt.so.3\n#18 0x40d8d198 in QPopupMenu::mouseReleaseEvent ()\n   from /usr/lib/qt3/lib/libqt-mt.so.3\n#19 0x40cb33f5 in QWidget::event () from /usr/lib/qt3/lib/libqt-mt.so.3\n#20 0x40c1a85f in QApplication::internalNotify ()\n   from /usr/lib/qt3/lib/libqt-mt.so.3\n#21 0x40c1cc06 in QApplication::notify () from /usr/lib/qt3/lib/libqt-mt.so.3\n#22 0x408aca91 in KApplication::notify () from /opt/kde3/lib/libkdecore.so.4\n#23 0x40bb824b in QETWidget::translateMouseEvent ()\n   from /usr/lib/qt3/lib/libqt-mt.so.3\n#24 0x40bb6e86 in QApplication::x11ProcessEvent ()\n   from /usr/lib/qt3/lib/libqt-mt.so.3\n#25 0x40bc7908 in QEventLoop::processEvents ()\n   from /usr/lib/qt3/lib/libqt-mt.so.3\n#26 0x40c327b1 in QEventLoop::enterLoop () from /usr/lib/qt3/lib/libqt-mt.so.3\n#27 0x40c325f6 in QEventLoop::exec () from /usr/lib/qt3/lib/libqt-mt.so.3\n#28 0x40c1c2ef in QApplication::exec () from /usr/lib/qt3/lib/libqt-mt.so.3\n#29 0x080622ed in main (argc=0, argv=0x0) at main.cpp:102\n"
    author: "Vijay"
  - subject: "Re: Kaffeine Media Player"
    date: 2005-03-14
    body: "This fixed the problem for me:\n \nrm $HOME/.kde/share/config/kaffeinerc"
    author: "James"
  - subject: "Re: Kaffeine Media Player"
    date: 2005-10-21
    body: "I had the same problem on a Gentoo-AMD64 system with a self-compiled installation of Kaffeine and this script fixed it.  Thank-you much."
    author: "Duly Stichmann"
  - subject: "Amarok vs Nautun"
    date: 2004-01-18
    body: "I haven't tried Amarok yet, but how is it better than Nautun? So far I've looked at some screenshots, and apart from the improved playlist I don't see anything Nautun doesn't have. I also do not like the black background color.\n\nAnd does Amarok support plugins and Winamp 5 or lower skings?"
    author: "Alex"
  - subject: "Nautun beats Amarok 10 nil"
    date: 2008-11-01
    body: "I recently installed SuSE 9.1 and tried to play MPEGs, VOBs, and MP3s. They worked straight away without the need to install increasingly-hard-to-freely-distribute-codecs that do not come with your SuSE 11 out of the box. Now I have one reason not to destroy my five SuSE 9.1 CDs. They'll probably be collectables in future. Hee hee hee!"
    author: "Roy"
  - subject: "UI for file permissions etc. ...."
    date: 2004-01-19
    body: "... please add more panes to Konqueror :-P\n\nhttp://evidence.sourceforge.net/screenshots/info_acl.jpg"
    author: "More-OSX_KDE_Interpollination"
  - subject: "Re: UI for file permissions etc. ...."
    date: 2004-01-19
    body: "kde will obviously sometimes need such a dialog, but plesae don't copy this, it looks horrible. And with the current theme, its even hard to find out whether a button is pressed or not.."
    author: "me"
  - subject: "Re: UI for file permissions etc. ...."
    date: 2004-01-19
    body: "http://lists.kde.org/?l=kde-devel&m=106118463328315&w=2\n\n"
    author: "Kevin Krammer"
---
In  <a href="http://members.shaw.ca/dkite/jan162004.html">this week's KDE-CVS-Digest</a>:
<A href="http://amarok.sourceforge.net/">amaroK</A> adds graphic sonograms. 
<A href="http://sourceforge.net/projects/kolourpaint/">KolourPaint</A> can be used as an icon editor. 
<A href="http://pim.kde.org/components/kpilot.php">KPilot</A> PIM integration improves. 
<A href="http://pim.kde.org/components/kmail.php">KMail</A> folder code is refactored. 
<A href="http://www.koffice.org/kword/">KWord</A> adds import of text boxes from OOWriter. 
And the last bugfixes before release.

<!--break-->
