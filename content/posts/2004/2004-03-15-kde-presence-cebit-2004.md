---
title: " KDE Presence at CeBIT 2004"
date:    2004-03-15
authors:
  - "fmous"
slug:    kde-presence-cebit-2004
comments:
  - subject: "Cool!"
    date: 2004-03-15
    body: "I've decided today that I'm going to drive to CeBIT this year anyway and\nnow this news confirms that it is a good idea!\nTwo years ago we've parked our car for free in a suburb street \nand took 3-4 stations by subway/train. If I could only remember where\nthat was. Does anyone know where to park for free next to a train\nstation (or even better: next to the fairgrounds, but I don't believe\nin miracles) and get there from the Ruhrgebiet without getting in a major \ntraffic jam? That would be perfect!\n"
    author: "Jan"
  - subject: "Re: Cool!"
    date: 2004-03-16
    body: "Err... take public transport in the first place? You'll be sure not to have any parking problems, and you allready know there is a subway station near the fairground."
    author: "Andr\u00e9 Somers"
  - subject: "Re: Cool!"
    date: 2004-03-16
    body: "there is a ICE station near, too. and the cebit shuttles are free with your cebit ticket, from as far away as hildesheim..."
    author: "[Lemmy]"
  - subject: "Re: Cool!"
    date: 2004-03-16
    body: "I travel via train from cologne .. on saturday.. hope we meet there. \n\n"
    author: "Scherfy"
  - subject: "getting there"
    date: 2004-03-17
    body: ">>Two years ago we've parked our car for free in a suburb street \nand took 3-4 stations by subway/train. <<\nWell, parking fees are low (about EUR 8,-- for the whole day) compared to admission fee and the Tram isn't for free either. \n\nJust follow the traffic guidance system and you're there. There are car parks with free shuttle minibuses but with some luck you can get close to the site.\n\nWell the most valuable item at CeBIT always is time ;-)\n\nConrad"
    author: "beccon"
  - subject: "Re: getting there"
    date: 2004-03-18
    body: "You can drive with the tram with your CeBIT ticket. So it is for free."
    author: "Abszisse"
  - subject: "KDE is in Hall 6 !!"
    date: 2004-03-16
    body: "Hi KDE is in Hall _6_ !!!!!\n\nThe booth is correct. "
    author: "physos"
  - subject: "Re: KDE is in Hall 6 !!"
    date: 2004-03-16
    body: "Physos, \n\nIt is corrected :) \n\nFab"
    author: "Fabrice"
  - subject: "Re: KDE is in Hall 6 !!"
    date: 2004-03-16
    body: "Thanks a lot :)"
    author: "physos"
  - subject: "Re: KDE is in Hall 6 !!"
    date: 2004-03-16
    body: "thanks to Stephan :) \n\nFab"
    author: "Fabrice"
  - subject: "Re: KDE is in Hall 6 !!"
    date: 2004-03-16
    body: "And where ist Trolltech? Can't find it in my CeBit Guide for PocketPC Handhelds (d/l from cebit.de)\n\nThx!\n"
    author: "anonymous"
  - subject: "Re: KDE is in Hall 6 !!"
    date: 2004-03-16
    body: "Not present? http://www.trolltech.com/company/tradeshow.html"
    author: "Anonymous"
  - subject: "http://www.egroupware.org/"
    date: 2004-03-16
    body: "I'm really impressed by this one! I had no idea that you could do so much nice stuff with php...\n\n/David\n"
    author: "David"
  - subject: "I've been there"
    date: 2004-03-20
    body: "I had an unexpected free day on Thursday, so I went to the CeBit.\nIn my opinion, the KDE place is a to small, or there were just too many people... ;-)\nAnd I noticed, the KDE Developers seem to be big anime fans, I have seen several anime related desktop themes and backgrounds (but I thill think Tux is a cold-water penguin, unlike PenPen... ;)"
    author: "Marti"
  - subject: "Re: I've been there"
    date: 2004-03-26
    body: ">And I noticed, the KDE Developers seem to be big anime fans, \n>I have seen several anime related desktop themes and backgrounds\n\nI was expecting reactions like this *g*. Nope it was merely some visitor that was demoing some ideas about setting wallpapers from Websites IIRC and he chose those. Appearantly nobody saw a reason to reset it.\n\nCheers,\n  Daniel"
    author: "Daniel Molkentin"
---
<A href="http://www.cebit.de/">CeBIT</A>, the world largest computer trade show, is taking place in 
Hannover from 18 to 24 March.
The <A href="http://www.kde.org/">KDE Project</A> will be present and showcasing the latest developments of the innovative
KDE 3.2 desktop. The KDE Team can be found in the booth of <A href="http://www.linupfront.de/">Linup Front</A>, 
come around and visit the developers, translators and representatives who are there. 
This gives you the opportunity to talk to several of the creators of the <A href="http://www.kde.org/awards/">award-winning</A> desktop environment.








<!--break-->
<p><IMG hspace="10" vspace="10" src="http://static.kdenews.org/mirrors/www.xs4all.nl/%257Eleintje/stuff/konqi_cebit.png" width="150" height="150" border="0" align="left"/><br/><br/><br/><br/><br/></p>
The KDE Team can be found in the booth of <A href="http://www.linupfront.de/">Linup Front</A> <strong>GmbH: Hall 6, Booth B38/355</strong> 
where there will be demos of KDE 3.2, recent <A href="http://www.kontact.org/">Kontact</A> interaction with 
<A href="http://www.egroupware.org/">eGroupware</A>, 
the KIOSK Mode which now has a <A href="http://www.xs4all.nl/~leintje/stuff/kioskgui-screen.png">GUI configuration</A> 
and KDE as the "<A href="http://wiki.kdenews.org/tiki-index.php?page=KDE%2C+the+integrative+desktop">Integrative Desktop Environment</A>" with 
<A href="http://www.freedesktop.org/Software/gtk-qt">Qt Meta Theme for GTK</A>, <A href="http://kde.openoffice.org/">KDE OOo</A>, etc ...</p>



<p>The following talks will be given by the KDE Developers:</p>

<p>LinuxForum, Hall 6, B52: </p>

<ul>
  <li>Thursday, 18th, 15:15 - Eva Brucherseifer: Portierung von Windows-Anwendungen auf Linux. Überblick über Möglichkeiten (Porting of Windows Applications to Linux)</li>
  <li>Saturday, 20th, 13:15 - Klas Kalass: KDE - Der Linux-Desktop </li>
  <li>Tuesday, 23th, 15:15 - Daniel Molkentin: Kontact - Eine Groupware-Lösung unter KDE </li>    
</ul>



<p>Logon Briefing:</p>
 
<ul>
  <li>Saturday, 20th, 10:45 - Daniel Molkentin: KDE Kontact - Groupware for the 
   Enterprise (In German)</li>    
</ul>




<p><em>Many thanks to all of our sponsors including <A href="http://www.linupfront.de/">Linup Front</A> 
and <A href="http://www.fujitsu-siemens.de/index.html">Fujitsu-Siemens</A></em>.</p>



