---
title: "KDE 3.3.1: Bugs Squashed, Quanta Gets VPL"
date:    2004-10-13
authors:
  - "numanee"
slug:    kde-331-bugs-squashed-quanta-gets-vpl
comments:
  - subject: "Plastick? Default?"
    date: 2004-10-13
    body: "What happened to the \"we only change styles for major releases due to the work involved for documentation and user adjustment...\" crowd?\n\nI think that since we've kept with this all the time, might as well use Keramick as default once more. Plastick will easily be selected by users and most distributions will also do the same."
    author: "Alex"
  - subject: "Re: Plastick? Default?"
    date: 2004-10-13
    body: "\"Plastik is now ridiculously fast and has become a strong contender for the default style\"\n\nYou're not reading this properly, its saying that Plastik is BECOMING a strong contender for the default style; it is not stating that Plastik IS now the default style.\n\nThey haven't changed the default style yet :P"
    author: "quaff"
  - subject: "Re: Plastick? Default?"
    date: 2004-10-13
    body: "> They haven't changed the default style yet :P\n\nPlastik *is* in kdebase and default in HEAD."
    author: "Anonymous"
  - subject: "Re: Plastick? Default?"
    date: 2004-10-13
    body: "HEAD is not released :)"
    author: "ac"
  - subject: "Re: Plastick? Default?"
    date: 2004-10-13
    body: "HEAD will be the base for 3.4 or 4.0 release."
    author: "blacksheep"
  - subject: "Re: Plastick? Default?"
    date: 2004-10-13
    body: "3.4, 3.4 for now :)"
    author: "the jumpy gnome on systray"
  - subject: "Re: Plastick? Default?"
    date: 2004-10-13
    body: "The documentation team withdrew their former objection due to new tools."
    author: "Anonymous"
  - subject: "Re: Plastick? Default?"
    date: 2004-10-13
    body: "To clarify, they now have a tool that allows them to automatically generate screenshots in any style, in any language, for documentation.\n\nPrevious to this tool, a change in style would have required the docs team to manually retake and touch up *every* screenshot in the docs!"
    author: "Tom"
  - subject: "Re: Plastick? Default?"
    date: 2004-10-13
    body: "sounds interesting. What tool is this. U have a link?"
    author: "KDE User"
  - subject: "Re: Plastick? Default?"
    date: 2004-10-13
    body: "The discussion about switching to Plastik is in the thread http://lists.kde.org/?t=109500725600001&r=2&w=2\n\nI do not remember the discussion enough to know if the tool was only described or if there was a real link somewhere in the discussion.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Plastick? Default?"
    date: 2004-10-13
    body: "Yes but where is the discussion where everyone agreed on switching to Plastik, including the Keramik maintainer?\n\nIt just happened and as a regular developer, I certainly don't remember agreeing :)"
    author: "adsfa23143214"
  - subject: "Re: Plastick? Default?"
    date: 2004-10-13
    body: "I do not think that the agreement to switch was made in this thread.\n\nIt was discussed many times before that changing the default style was needed. The only problem remaining was the documentation that had to remain synchronized with a change.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Plastick? Default?"
    date: 2004-10-13
    body: "KD Executor\n\nhttp://dot.kde.org/1081440693/"
    author: "ac"
  - subject: "Re: Plastick? Default?"
    date: 2004-10-13
    body: "Isn't that non-Free ?\n\nIs kde going to rely on this?\n"
    author: "JohnFlux"
  - subject: "Re: Plastick? Default?"
    date: 2004-10-13
    body: "it isn't an integral (e.g. required) tool for development. it's simply a tool with no Free Software equivalent that lets us get a certain, specific task that needs to get done right now in a timely manner so we can get on with the business of producing another great revision of our Free/Open Source Software desktop. \n\nit's similar to our icons: there just weren't any good Free Software graphics packages suitable for doing professional icon design. instead of shipping no icons or crappy icons, the artists used various non-Free tools to get the necessary job done and released the results under an Open Source license. everybody benefits. as Free tools become available, they get used for creating icons. "
    author: "Aaron J. Seigo"
  - subject: "Re: Plastick? Default?"
    date: 2004-10-14
    body: "It *is* free for KDE apps"
    author: "planetzappa"
  - subject: "Re: Plastick? Default?"
    date: 2004-10-14
    body: "Yes, it's free as in \"free beer\" for KDE usage: \nhttp://dot.kde.org/1081440693/\n"
    author: "cm"
  - subject: "Re: Plastick? Default?"
    date: 2004-10-13
    body: "I've never seen anyone saying \"we only change styles for major releases\". It would even be contradictory since Keramik became default in KDE 3.1. What many say is that styles should be kept for at least some minor releases, to avoid constantly changing KDEs image and the effort from changing all the documentation.\n\nKeramik was kept for 3 minor releases now (KDE 3.1, 3.2, 3.3). The next release, KDE 3.4, will have a long life so we won't have opportunities to change defaults in a long time. So it's natural to place much emphasis on good defaults and general polishing for this release."
    author: "John Styles Freak"
  - subject: "Re: Plastick? Default?"
    date: 2004-10-13
    body: "Long life? How much? I mean, larger than 3.2 and 3.3, yes, but I guess KDE 4 will come less than a year after 3.4.\nOTOH, I wonder, if there was so much complains about Keramik, what about Classic?\nYes, Classic KDE2 plain vanilla style. while the WM style is a bit lame, but the widget style it is still clean, fast, and nice with those soft gradients.\nThe \"we need a default look as shiny or shinier than Windows and Mac can offer\" does not catch me. XP's green and blue look is ugly, and Keramik was simply a response to that.\nPlastic is nice, but I guess there will be a lot of complaints if it is the default, as well. But I don't remember any complaints against Classic.\n\nJust my opinion, of course."
    author: "Shulai"
  - subject: "Re: Plastick? Default?"
    date: 2004-10-13
    body: "You don't remember any complaints?  Boy, that one made me laugh out loud! Do you even know what the KDE Classic style is?\n\nYou must have severely confused projects if you think there ever was a moment when there were no complaints.  :-)\n"
    author: "ac"
  - subject: "Re: Plastick? Default?"
    date: 2004-10-13
    body: "Classic, especially High Color, is not that bad. I think you're the one that doesn't know what that style is. Check it on KControl."
    author: "blacksheep"
  - subject: "Re: Plastick? Default?"
    date: 2004-10-14
    body: "I complained. I hated it. Use it if you want, troll."
    author: "Joe"
  - subject: "Re: Plastick? Default?"
    date: 2004-10-13
    body: "KDE 3.4 will have a new style in kdeartwork, Phase <http://www.kde-look.org/content/show.php?content=11402>. Like Highcolor Classic, this is \"clean, fast, and nice with those soft gradients\". It was designed for the same people that like Highcolor Classic.\n\nWe certainly want KDE to have a good first impression. Which is why the next default will probably be Plastik, which has an understated elegance sorely lacking in Luna and Aqua. But it won't suit everyone, which is why we have other styles."
    author: "Brandybuck"
  - subject: "Re: Plastick? Default?"
    date: 2004-10-13
    body: "Great. I lacked good styles like this one. Looks crisp and with clear borders, good."
    author: "KDE User"
  - subject: "Re: Plastick? Default?"
    date: 2004-10-14
    body: "I strongly disagree, first impression counts and Keramik is out of style. While it is important to remain API consistent I don't see a reason to let worse layout remain."
    author: "gerd"
  - subject: "KPDF"
    date: 2004-10-13
    body: "Biggest improvement (by reading the announcement,\nhaven't downloaded it yet) seems to be that\nKPDF now works. Why isn't that mentioned?\nOr did I get it wrong?\n\n"
    author: "Martin"
  - subject: "Re: KPDF"
    date: 2004-10-13
    body: "The announcement doesn't mention this, you read the changelog? As mentioned the changelog is to be considered as incomplete (lazy maintainers). There are also other big improvements (eg KNotes Kontact plugin)."
    author: "Anonymous"
  - subject: "Lazy Maintainers?"
    date: 2004-10-13
    body: "Sorry, but this is way out of line. You try being a maintainer of a larger app and keeping track of 5-10 commits / day, all the while maintaining a job and spending time with the family. Oh, and you would probably also like to do some coding yourself, since that is probably why you joined the apps team in the first place...\n\nThere is a big difference between a maintainer being excessively busy and being lazy. These people are incredibly generous with their free time considering the majority of them don't get paid a dime for it, and that they also must make personal sacrifices for the project. Calling them \"lazy\" is totally unacceptable."
    author: "Jason Keirstead"
  - subject: "Re: Lazy Maintainers?"
    date: 2004-10-13
    body: "I don't think he was being serious... relax..."
    author: "John Relaxed  Freak"
  - subject: "Re: Lazy Maintainers?"
    date: 2004-10-13
    body: "I don't think you should tell him to relax. I think you should do something. ;-)\n\nWhat have you done to help a project lately? He's right. A primary reason to release open source is the currency of community respect, just as monetary remuneration is for commercial projects. Calling commercial developers lazy is no big deal, as long as you keep buying their software. Calling free software developers lazy is analogous to collecting a refund. It is a debit of respect.\n\nAll of this is no big deal except that with millions of users and hundreds of contributors it is utterly absurd for someone in the over 99% of people who don't contribute to call the less than 1% of people who are doing anything lazy. It's doubtful anyone in the less than 1% group would say that and it is worth making the point of personal cost to put such things in perspective."
    author: "Eric Laffoon"
  - subject: "Re: Lazy Maintainers?"
    date: 2004-10-14
    body: "I agree with all that. Just not so sure he actually called the developers lazy, seems just an harmless joke to me (of course I don't know for sure)."
    author: "John Relaxed Freak"
  - subject: "Re: Lazy Maintainers?"
    date: 2004-10-13
    body: "By the way, I just thought I'd add this :D\nftp://ftp.kde.org/pub/kde/stable/3.3.1/SuSE/README\n\n----------------------------------------------------\n#\n# KDE 3.3.1 packages for SUSE distributions.\n# \n\nSorry, we were to lazy to build packages yet. \nThey will be here tomorrow, hopfully.\n"
    author: "John Relaxed  Freak"
  - subject: "Re: Lazy Maintainers?"
    date: 2004-10-13
    body: "Seems to have been updated.  Kopete and Kontact have Novell GroupWise support!"
    author: "ac"
  - subject: "Re: Lazy Maintainers?"
    date: 2004-10-13
    body: "Speaking of Kopete, beware! \nSuSE packagers compiled Kopete without Jabber support!"
    author: "John Relaxed  Freak"
  - subject: "Re: Lazy Maintainers?"
    date: 2004-10-13
    body: "Why would they do that?"
    author: "AC"
  - subject: "Re: Lazy Maintainers?"
    date: 2004-10-13
    body: "I don't know, mistake probably... see:\nhttp://bugs.kde.org/show_bug.cgi?id=88277"
    author: "John Kopete Freak"
  - subject: "More details about KDE PIM changes"
    date: 2004-10-13
    body: "I went through the commits and added more detais to the kdepim section of the changelog. A lot of annoyances were fixed, and especially those who use disconnected IMAP are encuraged to upgrade.\n\nAll in all I want to thank everyone involved for participating in this amazing KDE project. The branch regulary profitted from backports from HEAD, especially due to the proko2 project, which uses the branch as basis. KDE PIM is more active, alive and kicking than ever before. Thanks to everyone involved and especially to all the enduring bug reporters that helped reproducing bugs."
    author: "Daniel Molkentin"
  - subject: "KMail's IMAP"
    date: 2004-10-14
    body: "I've been hearing a lot about Disconnected IMAP with 3.3, but I wonder if the normal IMAP mode is kept in sync?  It does seem a little flaky, too -- progress bars for moving messages that seem to stop halfway through, even though the move itself seems to complete fine, for example.\n\nIs plain IMAP still being developed?  I gather disconnected IMAP caches things locally, right?  Which is a bit redundant if my IMAP server is local?"
    author: "Jel"
  - subject: "yay kool !! :)"
    date: 2004-10-13
    body: "...but somehow the fedora kdebase package is 99megs huge?\n\n"
    author: "rsl"
  - subject: "Re: yay kool !! :)"
    date: 2004-10-13
    body: "aha, looks like it will be fixed soon.\n\n\n---\n\"From:\nThan Ngo <than@redhat.com>\nReply-To:\nDevelopment discussions related to Fedora Core <fedora-devel-list@redhat.com>\nDate:\nWednesday 13 October 2004 15:48:40\nGroups:\ngmane.linux.redhat.fedora.devel\n\n...\ni have noticed it today. it's a problem on local build machine. new KDE \nrpms will be uploaded on ftp.kde.org for fc2 today.\n\nThan\"\n\n"
    author: "rsl"
  - subject: "[OT] Beautyfing KDE"
    date: 2004-10-13
    body: "I know it's OT, but I think this deserves some attention: http://www.kde-apps.org/content/show.php?content=16962\n\nAny chance we will see these patches in 3.4?"
    author: "Anonymous"
  - subject: "Re: [OT] Beautyfing KDE"
    date: 2004-10-13
    body: "Very cool patches. The only tricky one is the qt patch. I do NOT want to rebuild qt and then kdelibs, kdebase...ugh."
    author: "Joe"
  - subject: "Re: [OT] Beautyfing KDE"
    date: 2004-10-13
    body: "For Gentoo, there's a guide with ebuilds here:\n\nhttp://forums.gentoo.org/viewtopic.php?t=236593"
    author: "Niels"
  - subject: "Re: [OT] Beautyfing KDE"
    date: 2004-10-13
    body: "That looks excellent! I hope it can be merged for KDE 3.4!"
    author: "John Good Lookin'  Freak"
  - subject: "Re: [OT] Beautyfing KDE"
    date: 2004-10-13
    body: "Ewwwww....\n\nI HATE that rounded icon selection. It looks so horrifically antiquated, like it belongs in GEOS or something equally old."
    author: "QV"
  - subject: "Re: [OT] Beautyfing KDE"
    date: 2004-10-13
    body: "Man that is a slick patch.  Not only does it make KDE look great, it is also useful as far as atracting the eye to where you are working on the screen.  I would love to see this added.  The rounded icon selection, as stated above, might not be for everyone.  I think it looks good, but I would probably preffer sharp clean corners.\n\nAs a side question, since I have not upgraded kde lately... does moving icons on the desktop still suck?  I would pick up an icon, place it where I wanted it, and it would magically be placed about 10-20 pixels away.  Close enough to work, but just far enough away to piss you off and make you redrag."
    author: "Brandon Petty"
  - subject: "Re: [OT] Beautyfing KDE"
    date: 2004-10-13
    body: "Maybe you have \"Automatically lineup icons\" activated?"
    author: "Roberto Alsina"
  - subject: "Re: [OT] Beautyfing KDE"
    date: 2004-10-14
    body: "Ya on my old KDE 3.2, the final resting position of the icon seems to be the *center* of the pointer.  When I'm dragging it however, the pointer is closer to the top left of the icon.  Seems like a centering bug."
    author: "ac"
  - subject: "This is fantasticc, i vote for this"
    date: 2004-10-14
    body: "it adresses some of my most important concerns here http://bugs.kde.org/show_bug.cgi?id=58944 and looks great!"
    author: "Alex"
  - subject: "Re: [OT] Beautyfing KDE"
    date: 2004-10-14
    body: "Very nice patches... but QT patch a little strange way to do things... I'm not KDE/Qt developer, but think, that any problem can be fixed with standart methods.\n\nGreat work!!! :-)\nNeed this in 3.3.2 !!!!!!!!!"
    author: "Wizard580"
  - subject: "Re: [OT] Beautyfing KDE"
    date: 2004-10-14
    body: "There's something you can take to cure those excess exclamation marks you know. You needn't suffer forever!\n\nAlso, you can't solve everything at the KDE level, sorry. Additionally, just because he solved it at the Qt level, it does not mean he didn't use \"standart methods\"."
    author: "Max Howell"
  - subject: "Re: [OT] Beautyfing KDE"
    date: 2004-10-14
    body: "Hi Max!\n(note only one exclamation mark)\n\nThat was certainly fun. But \"standart methods\" aren't where it's at as much as Eye candy. I thought I'd look to see if there were any questions or comments to respond to on Quanta, especially since it got in the headline... You know, four years of passionate work and sacrifice. I've been proud how well it's received. Frankly I still think Keramik is just fine and can't get excited over Plastik. But hey, it's all fluff, style without substance, though the visual feedback in this patch looks nice. Anyway at aKademy I was very pleased that both Quanta and Kommander were one after the other in the top 10 on kde-apps.org highest rated. Kommander slipped because we have to do a new release, but I was surprised to see that this patch is rated higher than Quanta. I don't feel bad about many of the fine applications being in between Quanta and Kommander... like Scribus, amaroK, KFormDesigner, KDevelop, Kile and Kontact, but I wonder what it says about KDE and what it says to KDE developers when the demand and appreciation of eye candy is so disproportionate to application software. \n\nI have a theory. When someone uses an application there are lots of things it has to do and invariably there will be some aspect they don't like and for that they will vote it down. If someone doesn't like eye candy they just ignore it but if they like it the only thing it has to do is continue to look good. There isn't really anything to \"find out\" about it and get disappointed over.\n\nIf this site's measure of success was talkbacks then every story should be about what default theme we should use. As I write this there are 46 posts, 14 of which are _not_ about visuals leaving about 70% of the talkbacks here on visuals. \n\nFor years I thought all Windows users wanted was cute visuals, but the most stunning visuals are clearly on the Mac and it hasn't gotten them world domination. Maybe KDE is getting disenfranchised Windows users who wanted better visuals? What I have always liked about KDE is that it is a productive environment for me to get work done in. Maybe I should work less and stare at it more. ;-)\n\n[staring...]\n\nHey! This application is in the way of my wallpaper! Who put that there!?"
    author: "Eric Laffoon"
  - subject: "Re: [OT] Beautyfing KDE"
    date: 2004-10-14
    body: "I think things are simpler than that, actually, Eric.\n\nVisual fluff potentially touches everyone.\nQuanta potentially touches Web developers.\nAnd the group 'Web developers' is but a small fraction of the group 'everyone'.\n\nIn short, this patch's rating doesn't mean -anything- comparatively to Quanta. Ask any Web developer if they'd rather have eye candy or Quanta, and see what they answer. :) "
    author: "Anonymous Coward"
  - subject: "Re: [OT] Beautyfing KDE"
    date: 2004-10-14
    body: "I think there's another part to the explanation as well. People who have the time to browse around kde-look and similar community sites, try out the latest stuff and even care enough to vote, are probably not the same people who use application software to get actual work done. "
    author: "Apollo Creed"
  - subject: "Re: [OT] Beautyfing KDE"
    date: 2004-10-14
    body: "More importantly, quanta only touches a portion of the web developer group. I've tried using it before, and while a nice app, it doesn't provide any features that I have seen that make me want to use it. It also lacks split screen editing, a critical feature in my book.\n\n  Greg"
    author: "Greg "
  - subject: "Re: [OT] Beautyfing KDE"
    date: 2004-10-18
    body: "try it again"
    author: "superstoned"
  - subject: "Re: [OT] Beautyfing KDE"
    date: 2004-10-18
    body: "The last time I compiled it was last month, and it still really didn't seem to give me much over kate. I did like the project support, it felt a bit more natural than kate's project support.  What is it that I'm missing? Quanta seems to be good at document markup, but that's all I see."
    author: "Greg "
  - subject: "Re: [OT] Beautyfing KDE"
    date: 2004-10-18
    body: "well, for me the wysiwyg support is very nice... but indeed, for just editting pages, kate is very nice too. but if you want to work with php, quanta is very good. and the templates and wizards can get you going very fast."
    author: "superstoned"
  - subject: "Re: [OT] Beautyfing KDE"
    date: 2004-10-18
    body: "I guess that's the difference for me. I do a lot of work in PHP, but it is very rare that I ever touch HTML in the course of my coding. "
    author: "Greg"
  - subject: "It's really quite simple..."
    date: 2004-10-16
    body: "Of course, I would never take that patch over Quanta if it was one or the other, but this is a rating system, not all or nothing. Quanta is certainly far more important to KDE and the LinuxWorld than that eyecandy.\n\nHowever, at least when I rate, I take into account what it is meant to do. For eyecandy, I think it deserves that score, there are few competing projects on kde-apps.org which offer that level of eyecandy. I rate it based on its category. Otherwise, I would be comparing apples and oranges.\n\nIn addition, the rating system is terribly flawed. It's either an application is \"bad\" or \"good\", there is no middleground. It should be a rating system from 1-10, that would provide more accurate representation. I rarely think something is actually bad on KDE-LOOK.org and so I vote for most applications as good. However, often I would like to vote great instead of good, such as in the case of Quanta.\n\nI think this generally explains this."
    author: "Alex"
  - subject: "Fish broken?!?"
    date: 2004-10-14
    body: "It seems that fish doesn't work with KDE 3.3.1 - oh well, I hope this gets fixed soon... :-("
    author: "Willie Sippel"
  - subject: "Re: Fish broken?!?"
    date: 2004-10-14
    body: "Make sure to file a bugreport."
    author: "Waldo Bastian"
  - subject: "Re: Fish broken?!?"
    date: 2004-10-14
    body: "Seems I'm not the only one having this problem:\n\nhttp://bugs.kde.org/show_bug.cgi?id=91107"
    author: "Willie Sippel"
  - subject: "Re: Fish broken?!?"
    date: 2004-10-14
    body: "Fish works with me. I installed KDE 3.3.1, as an upgrade on 3.3 with SuSE 9.0 last night and I have seen no problems so far.\n\n"
    author: "Jan"
  - subject: "Re: Fish broken?!?"
    date: 2004-10-14
    body: "Strange. I thought it might have been an AMD64 issue, but the bugreport on bugs.kde.org came from a x86 user..."
    author: "Willie Sippel"
  - subject: "Re: Fish broken?!?"
    date: 2004-10-29
    body: "same problem here\n(\n# gcc --version\ngcc (GCC) 3.4.2  (Gentoo Linux 3.4.2-r2, ssp-3.4.1-1, pie-8.7.6.5)\n\nkde-3.3.1\n\nSystem uname: 2.6.9-gentoo i686 Intel(R) Pentium(R) M processor 1700MHz\nGentoo Base System version 1.5.1\nACCEPT_KEYWORDS=\"x86 ~x86\"\nCFLAGS=\"-O2 -march=pentium3 -msse2 -pipe -fomit-frame-pointer -funroll-loops -falign-functions=4\"\nCXXFLAGS=$CFLAGS\n)"
    author: "Jan Menzel"
  - subject: "glibc upgrade: fish works again"
    date: 2004-10-29
    body: "just upgraded glibc\nfrom glibc-2.3.4.20041006 (?)\nto   glibc-2.3.4.20041021\nreboot\n\nnow fish seems to work again :-)\nbut kate still fails saving remote files. On the other hand kwrite can do so.\n\nregards, jan"
    author: "Jan Menzel"
  - subject: "Re: Fish broken?!? ask gentoo linux"
    date: 2004-10-15
    body: "ack on Gentoo Linux - but that's for bugs.kde.org and enough people already complained there. I'd just like to recommend Gentoo users to use KDE 3.3.0 for now as my experience with KDE 3.3.1 on Gentoo are all bad. Like kate. I tried to open a fish once in Kate, now kate is bugging me every freaking time I open it with a dialog box asking for freaking password on a location I tried to fish:// a few hours after upgrading. It wants the password, but no matter how many times I type it correctly it still just keeps on showing the same stupid dialog box. There are some other issues with 3.3.1 too, but those also belong somewhere else. Honestly, I wanted to write a review of KDE 3.3.1 for linuxreviews.org and write about how it's improved, but now, after using it, I just don't want to because it would all be too negative and destroy the spirit of the developers who, despite how poor the latest version is, do throw a very generous and enormous amount of their free time into KDE."
    author: "xiando"
  - subject: "Re: Fish broken?!?"
    date: 2004-11-10
    body: "seem to be broken on OpenBSD 3.6-snapshots (as of 11/2/04) as well. i've had a couple other problems with it as well, that i'm attributing to being on a BSD."
    author: "jb"
  - subject: "xdelta newbie question"
    date: 2004-10-14
    body: "Hi guys,\n\nWhat are these .xdelta files all about? I know they are like binary diffs but where can I get the actual xdelta utility? I found something that claimed to be beta at www.xdelta.org. Is this the thing I should use? Thanks in advance."
    author: "Val"
  - subject: "Re: xdelta newbie question"
    date: 2004-10-14
    body: "www.xdelta.org is the right site, be sure to pick up xdelta 1."
    author: "Anonymous"
  - subject: "Re: xdelta newbie question"
    date: 2004-10-14
    body: "Perhaps:\nhttp://freshmeat.net/projects/xdelta/"
    author: "AC"
  - subject: "Re: xdelta newbie question"
    date: 2004-10-14
    body: "Yup, I found it. It has stable version dated by 2001. So I tried it and it worked."
    author: "Val"
  - subject: "Re: xdelta newbie question"
    date: 2004-10-18
    body: "BTW, doaes someone have an idea why xdelta's are for .tar, not .tar.bz2? I think the latter would be much more convenient..."
    author: "Val"
  - subject: "Re: xdelta newbie question"
    date: 2004-10-18
    body: "The xdelta files created for the tar files are smaller."
    author: "Anonymous"
  - subject: "Re: xdelta newbie question"
    date: 2005-03-12
    body: "Delta compression (differencing) sucks against compressed files- a single byte change in the new version can result in all bytes after N in the output stream differing.\nDifferencers are ran against uncompressed data, then the patch is compressed (automatically with xdelta) to minimize the patch size...."
    author: "Brian Harring"
  - subject: "Hmm. Doesn't compile"
    date: 2004-10-14
    body: "../mcopidl/mcopidl -t /usr/local/src/CVS/X11/KDE/arts/flow/artsflow.idl\n/usr/local/src/CVS/X11/KDE/arts/flow/artsflow.idl: warning: Arts::WaveDataHandle::load (method) collides with Arts::WaveDataHandle::load (method)\nmake[3]: *** [artsflow.cc] Segmentation fault (core dumped)\nmake[3]: *** Deleting file `artsflow.cc\n\nLinux, glibc 2.3.3, GCC 3.4.2, Qt 3.3.3 (from qt-copy).\n"
    author: "KDE fun"
  - subject: "Re: Hmm. Doesn't compile"
    date: 2004-10-14
    body: "I found the same error from a FreeBSD user - http://lists.freebsd.org/pipermail/freebsd-current/2004-March/024258.html . But on my system it's the first thing to fail, so I don't think it's hosed."
    author: "KDE fun"
  - subject: "Re: Hmm. Doesn't compile"
    date: 2004-10-14
    body: "Another, now also for Linux - http://lists.debian.org/debian-qt-kde/2004/08/msg00316.html\n\nSo, it isn't happy with GCC 3.4.x and --enable-final, what I also used.\n"
    author: "KDE fun"
  - subject: "Quanta web page"
    date: 2004-10-14
    body: "The Quanta web pages is very much out of date. Not a good advertisement for a web development tool. Even the screenshots are from 3.0/3.1!!"
    author: "gerd"
  - subject: "Re: Quanta web page"
    date: 2004-10-14
    body: "As you can read there: \"This site is in the process of being replaced\""
    author: "Anonymous"
  - subject: "New KDM?"
    date: 2004-10-16
    body: "I'm mostly pleased with every new KDE release, but IMHO every time KDM has been changed, it has been changed to even worse. First the \"Session type\" was moved, now I can't even find the button for reset X!\n\nPlease, do something about KDM! It was so much more convenient when you could choose the session type directly without navigating on some stupid menus.\n\nPuh-leeze!"
    author: "Jouni H\u00e4tinen"
  - subject: "Re: New KDM?"
    date: 2004-10-16
    body: "Oh no.  Yeah I really get annoyed when KDM makes these weird changes. :(\n\nDo you have a screenshot?"
    author: "KDE User"
  - subject: "Kmail forward imbedded attachment"
    date: 2004-10-17
    body: "Looks like that does 'nt works anymore.\nTo forward a mesage with a attachment imbedded :( instead of the mail forwarded complete ???????"
    author: "VenimK"
  - subject: "Re: Kmail forward imbedded attachment"
    date: 2006-12-20
    body: "Did it ever work?  Far as I know outleak is the only mailer that does this.\n\nSave and attach seem to be the only option.\n\nBut this would be very nice to have."
    author: "jsa"
---
KDE 3.3.1 <A href="http://www.kde.org/announcements/announce-3.3.1.php">has been released</a>.  This latest and greatest from KDE mercilessly exterminates bugs in <a href="http://www.konqueror.org/">Konqueror</a>, KHTML, the <A href="http://edu.kde.org/">KDE Edutainment</a> module, the KDE <A href="http://developer.kde.org/~wheeler/juk.html">JuK</a> box and more.  Better yet, <A href="http://www.kde-look.org/content/show.php?content=7559">Plastik</a> is now ridiculously fast and has become a strong contender for the default style in KDE 3.4.  Also, the <a href="http://quanta.sourceforge.net/">Quanta Web Development tool</a> has been enhanced and now has the VPL (Visual Page Layout aka WYSIWYG) mode enabled.  For the gory details, read <a href="http://www.kde.org/announcements/changelogs/changelog3_3to3_3_1.php">the KDE 3.3.1 changelog</a>.  








<!--break-->
<p>
But wait...  How do you get your hands on all this goodness?  We have <A href="http://www.kde.org/info/3.3.1.php">binary packages and source packages</a> and then we have <A href="http://developer.kde.org/build/konstruct/">Konstruct</a> (<A href="http://www.kde-apps.org/content/show.php?content=9725">details</a>).  So there.
<p>
And btw, <a href="http://i18n.kde.org/">translations</a> have been greatly improved in this release.





