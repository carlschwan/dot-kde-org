---
title: "XpoLinux Monterrey 2004 Business Summit Report"
date:    2004-11-17
authors:
  - "dprett"
slug:    xpolinux-monterrey-2004-business-summit-report
comments:
  - subject: "OUFF! Broken links :-/"
    date: 2004-11-17
    body: "Hey Guys :-D\nI'd really like to see the pictures of this.. but the links are broken.. or is the server just down?\n\n~Macavity"
    author: "Macavity"
  - subject: "Re: OUFF! Broken links :-/"
    date: 2004-11-17
    body: "The server is just down.\n\n"
    author: "Anonymous"
  - subject: "Re: OUFF! Broken links :-/"
    date: 2004-11-17
    body: "an up again ... \n\nAdmin dealt with it quickly. \n\nThank you.\n\n\nFab "
    author: "Fabrice Mous"
  - subject: "Wish I had been there :-D"
    date: 2004-11-17
    body: "MAN I'd have loved to see that presentation that Ian gave... I hope that there will be something like this in Denmark soon, so oh-so-poor little me can afford to go too :-D\n\nCheers & Beers\n\n~Macavity"
    author: "Macavity"
  - subject: "This is great!"
    date: 2004-11-17
    body: "\"Duncan showing Mr. Potato (the most popular Linux app of the Expo) to a kid\"\n\nI posted before about this application. It's a shame it is so abandoned, the 1st module (mr. potato) is much lower quality then the other two, there could be some modules with Konqui, Kate, etc. Why not provide some tutorials/specifications of the modules/images and create a special section in KDE-look for those, maybe start with a contest to motivate people. \n\nBest wishes to the whole KDE-Edu team!!"
    author: "John Edu Freak"
  - subject: "Re: This is great!"
    date: 2004-11-17
    body: "yes, a section for edu apps artwork in kde-look would be cool. KEduca asked for icons and got positive answers. It's just a matter of letting know what we need. If you have any idea about how doing this you can send me a mail.\nIncidentally, Mr Potato is in kdegames as it existed prior KDE-Edu.\nIt's great to read the report and see the kids using Suse. Kids don't mind what OS they use. Kids are open!\n"
    author: "annma"
  - subject: "Signs"
    date: 2004-11-17
    body: "There's lots of English-looking signs for a Mexican school..."
    author: "AC"
  - subject: "Re: Signs"
    date: 2004-11-17
    body: "The only English sign I saw was 'kids at work'. "
    author: "Ian Monroe"
  - subject: "Re: Signs"
    date: 2004-11-17
    body: "Actually, all signs were in english. It was a private school and even the classes were spoken in english.\n"
    author: "Duncan Mac-Vicar"
  - subject: "Re: Signs"
    date: 2004-11-17
    body: "Guess that explains how you were both able to answer questions."
    author: "Ian Monroe"
  - subject: "Re: Signs"
    date: 2004-11-17
    body: "In Mexico private schools and colleges are considered a lot better than their\npublic equivalents. However, that's not the truth. There are well know\nprivate schools affiliated to international institutions with good reputation,\nthat are really better that public schools but expensive, less than 5% of\nstudents go to these kind of schools. However, some people wanting to make easy\nmoney had the idea of exploiting the reputation of good private schools and\nopened centers all over the country that are supposed to offer education of\n\"international or european quality\". Some parents that cannot afford the high\nprice of the best private schools send their children to these fake schools,\npaying large amounts of money, that cultivates the ego of the students but has\nnothing to do with education, or at least with the concept of education one has\nin USA or in the EU. In Mexico you will find a lot of people that went to\nthese fake schools and think of themselves as being well educated. Sometimes\nwe joke at the fact that the \"Instituto Tecnologico y de Estudios Superiores\nde Monterrey\" (ITESM), a center of quality but extremely expensive, has a\nfake copy, the \"MIT\", that stands for \"Monterrey Institute of Technology\".\n\n - Jose Luis\n"
    author: "tetabiate"
  - subject: "It\u00b4s Great...."
    date: 2004-11-18
    body: "Marvelous, is the only word for this,...Soy de Monterrey y es grandioso este tipo de eventos en especial por la soluci\u00f3n KDE aunque, toda la promoci\u00f3n a Linux en si se ha venido tomando en cuenta aqui en M\u00e9xico y esperamos siga creciendo m\u00e1s , aunque se tengan algunos tropiezos.\n\nSaludos"
    author: "Vantage"
  - subject: "Re: It\u00b4s Great...."
    date: 2004-11-18
    body: "LOL. That picture is the street where jpablo practiced suicidal games crossing it running. Ian then realized crossing streets its Mexico's population control system. While thery crossed, Ian and me awaited like cowards in the other side, waiting for the traffic flow to decrease.\n"
    author: "Duncan Mac-Vicar"
  - subject: "Re: It\u00b4s Great...."
    date: 2004-11-18
    body: "Is that a CGI render scene using Mayo?\n\nIt look so real!  Better than all the CGI scene i've seen so far."
    author: "Wow"
  - subject: "Re: It\u00b4s Great...."
    date: 2004-11-18
    body: "Mayo (spanish) = May (english)\n=> picture taken in May 2002\nor was it </funny> ?\n"
    author: "Duncan Mac-Vicar"
  - subject: "Re: It\u00b4s Great...."
    date: 2004-11-19
    body: "I think you're thinking of Maya, Mayo is a kind of dressing and means may in spanish. But it is the most realistic CGI scene i've ever seen! :-D"
    author: "joe"
  - subject: "Missing Pics"
    date: 2004-11-23
    body: "You forgot to mention about your 5 minutes of rock star-like fame!\n<p>\n<a href=\"http://www.opensys.com.mx/~fcoromo/kde/Fran_101804_002.JPG\"> Signing autographs</a><br>\n<a href=\"http://www.opensys.com.mx/~fcoromo/kde/Fran_101804_003.JPG\"> The fans!, ohh the madness!!!\"</a><br>\n<a href=\"http://www.opensys.com.mx/~fcoromo/kde/Fran_101804_004.JPG\"> Even more autographs from \"the famous people\" as said by some of the children there.</a>\n"
    author: "More Photos"
  - subject: "Re: Missing Pics"
    date: 2004-11-23
    body: "can someone fix that html ?"
    author: "More Photos"
---
Ian Geiser and I traveled to Monterrey, Mexico to attend the KDE booth in the <a href="http://www.expolinux.org/">ExpoLinux 2004 Business Summit</a> (1st to 3rd November). The Expo was located in a place called Cintermex, specially adapted for conferences and expos. There were like 25 booths with local business showing business solutions. The biggers being Novell, IBM and Hewlett Packard. We were in a normal booth, and the fact that ours was one of the most visited was very interesting.




<!--break-->
<p>We also had the opportunity to spend time with the Monterrey Linux User Group and share lot of ideas for promoting KDE there. Some of the LUG members where interested in starting coding KDE stuff. We will see if Mexico gets more involved in KDE in the future.</p>

<p>A very interesting experience happened when we were invited by Manuel Hernandez from <a href="http://www.opensys.com.mx/">open|Sys</a> to a school (Colegio Euroamericano de Monterrey) were they deployed SuSE / KDE in 180 PCs. The children use KDE from the first moment they touch a computer in the school. We had a discussion with teachers and listened to their needs, most of them (remote desktop and kiosk) was a matter of upgrading the deploy. 
We see this deployment as a success case and a great opportunity to have a live installation to test the <a href="http://edu.kde.org/">edutainment project</a>.</p>

<p>Pictures:</p>
<ul>
<li>Event Logo <a href="http://events.kde.org/contrib/events/monterrey2004/logo.jpg">(image)</a></li>
<li>Preparing the KDE Booth <a href="http://events.kde.org/contrib/events/monterrey2004/1s.jpg">(small)</a> <a href="http://events.kde.org/contrib/events/monterrey2004/1.jpg">(big)</a></li>
<li>Ian and Duncan at the booth <a href="http://events.kde.org/contrib/events/monterrey2004/2s.jpg">(small)</a> <a href="http://events.kde.org/contrib/events/monterrey2004/2.jpg">(big)</a></li>
<li>Ian telling the crowd about KDE and the community <a href="http://events.kde.org/contrib/events/monterrey2004/3s.jpg">(small)</a> <a href="http://events.kde.org/contrib/events/monterrey2004/3.jpg">(big)</a></li>
<li>Duncan showing Mr. Potato (the most popular Linux app of the Expo) to a kid. <a href="http://events.kde.org/contrib/events/monterrey2004/4s.jpg">(small)</a> <a href="http://events.kde.org/contrib/events/monterrey2004/4.jpg">(big)</a></li>
<li>The Hewlet Packard booth was very nice <a href="http://events.kde.org/contrib/events/monterrey2004/5s.jpg">(small)</a> <a href="http://events.kde.org/contrib/events/monterrey2004/5.jpg">(big)</a></li>
<li>The interest in KDE dev tools made Ian improvise a technical talk focused in RAD development. The talk was translated to Spanish in real time. <a href="http://events.kde.org/contrib/events/monterrey2004/6s.jpg">(small)</a> <a href="http://events.kde.org/contrib/events/monterrey2004/6.jpg">(big)</a></li>
<li>Duncan giving the scheduled talk. The talk was about KDE community, technology available for users and developers and future directions of the project. (in Spanish) <a href="http://events.kde.org/contrib/events/monterrey2004/7s.jpg">(small)</a> <a href="http://events.kde.org/contrib/events/monterrey2004/7.jpg">(big)</a> <a href="http://events.kde.org/contrib/events/monterrey2004/charla.pdf">(PDF slides)</a></li>
<li>Duncan (KDE), Louis (openoffice), Juan Pablo (Gnome, our host in Monterrey), and Ian (KDE) <a href="http://events.kde.org/contrib/events/monterrey2004/8s.jpg">(small)</a> <a href="http://events.kde.org/contrib/events/monterrey2004/8.jpg">(big)</a></li>
<li>One of the labs running KDE <a href="http://events.kde.org/contrib/events/monterrey2004/9s.jpg">(small)</a> <a href="http://events.kde.org/contrib/events/monterrey2004/9.jpg">(big)</a></li>
<li>This is a Linux Zone <a href="http://events.kde.org/contrib/events/monterrey2004/10s.jpg">(small)</a> <a href="http://events.kde.org/contrib/events/monterrey2004/10.jpg">(big)</a></li>
<li>Ian and Duncan answering kids questions <a href="http://events.kde.org/contrib/events/monterrey2004/11s.jpg">(small)</a> <a href="http://events.kde.org/contrib/events/monterrey2004/11.jpg">(big)</a></li>


