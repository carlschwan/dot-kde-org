---
title: "KMyMoney 0.6 is Released"
date:    2004-06-18
authors:
  - "tbaumgart"
slug:    kmymoney-06-released
comments:
  - subject: "RPN"
    date: 2004-06-18
    body: "Does the calculator mentioned on the 0.8 support RPN?"
    author: "anon"
  - subject: "Re: RPN"
    date: 2004-06-19
    body: "No. If you want, please feel free to open a feature request on https://sourceforge.net/tracker/?group_id=4708&atid=354708"
    author: "Thomas Baumgart"
  - subject: "gnucash"
    date: 2004-06-18
    body: "How does KMyMoney compare to Gnucash? "
    author: "screne"
  - subject: "Re: gnucash"
    date: 2004-06-19
    body: "Well, to be honest, we are a bit behind gnucash at this point.  They already support most of the features that we are talking about for our 0.8 release.  However, from our users, we have heard comments such as our interface is easier to use, they like that you can export your accounts into qif files (you can't in gnucash), and that it is lighter-weight than gnucash.  \n\nWe want to get up to the level of gnucash someday, and hopefully surpass it eventually."
    author: "Kevin Tambascio"
  - subject: "Re: gnucash"
    date: 2004-06-19
    body: "When is 0.8 expected to be ready?"
    author: "Anonymous"
  - subject: "Re: gnucash"
    date: 2004-06-20
    body: "I hope about 6 months from now we should be releasing a new version, however that guess is not from any consensus of our team, just my guess.  We are hoping soon to generate some preview releases to show the work we have done so far in 0.8, and hopefully gain some feedback on the usability of those new features.  Keep checking in on our website for any news about this.\n\nThanks,\nKevin"
    author: "Kevin Tambascio"
  - subject: "Re: gnucash"
    date: 2004-06-19
    body: "Is GnuCash import supported?"
    author: "jk"
  - subject: "Re: gnucash"
    date: 2004-06-20
    body: "We have an alpha-level gnucash import function working in our current cvs branch.  Our 0.8 release will have that functionality."
    author: "Kevin Tambascio"
  - subject: "KMyMoney, KMyArse"
    date: 2004-06-18
    body: "geez, isn't there a proper name for such an application?\nIt sounds like it is build for pre-school users.."
    author: "anonymouse"
  - subject: "Re: KMyMoney, KMyArse"
    date: 2004-06-18
    body: "It's better than \"Ka$h\" :-)"
    author: "whobody"
  - subject: "Re: KMyMoney, KMyArse"
    date: 2004-06-18
    body: "Hey, I LIKE \"Kash\" as a name. 8-)"
    author: "Keith"
  - subject: "HBCI"
    date: 2004-06-18
    body: "HBCI support would be very cool indeed.\nI would trust a non-proprietary app much more for\nonline banking. I know there are some HBCI solutions\nfor Linux right now but they are Gnome (i.e Interface\nsucks) and quite complicated. I did not really dare to\ntry them. I fear they might transfer \u00a41M to Bill Gates...\nIn my opinion HBCI is the way to go for online banking anyway.\nMuch better than to force some stupid web interfaces on the users.\nHBCI gives us back the freedom to choose the GUI we want for banking.\nBTW: Many banks do not like to give you HBCI because it is considered\nmuch more secure and if something goes wrong they will have to proove\nit's not their fault. If sth. goes wrong with ordinary online banking\nYOU will have to prove you did nothing wrong. But if you insist they\nwill give you an HBCI account eventually.\nIf you can always choose a bank which does HBCI via chipcard - not\nvia disk. It's far easier and more secure. If you in addition use\nLinux (and I sure hope you do ;-) ) you can feel pretty safe to \ndo online banking - isn't that great? Right now I use Matrica which\ncomes packaged with SuSE. Nice app but a bit non-standard. And it\nwants to install only as non-root which is a major no-no for a\nbanking app IMHO."
    author: "Martin"
  - subject: "Re: HBCI"
    date: 2004-06-19
    body: "yes, hbci-support would immediately add thousand of european (mostly german?) users to \"Kash\" :) It would probably be the first really free and useable software to supoprt that, a real killer-app.\n\nNow just imagine it being a KPart, then it could (optionally) be integrated into Kontact. I think a finance part fits quite well into Kontact!"
    author: "me"
  - subject: "Syncing"
    date: 2004-06-18
    body: "What would be really cool is if there was some way this would also sync with an Expenses program on the Palm Pilot.  Would that ever be possible?  "
    author: "Laura"
  - subject: "Re: Syncing"
    date: 2004-06-18
    body: "I would think there might be a way to hook into the PIM tools that are a part of KDE, and extract out the expense data.  That would definetly be a nice feature for us to have.  We'll take it into consideration.\n\nThanks for your comments,\nKevin"
    author: "Kevin Tambascio"
  - subject: "Calendar"
    date: 2004-06-19
    body: "I have been a user of Quicken for many many years.\nIt is the only app that I use on a regular basis that requires some form of MS Windows: either dual boot or Wine.\nI have been trying to replace it.\nI don't do anything all that weird with it. All I do is keep track of my checkbook and bills. I don't need hardly any of the other features. Quicken 99 works just fine for me...except for MS Windows:(\nThe feature that I like the most about Quicken that I would really like to see in open source alternatives is a good calendar. Maybe there could be a way to link this into KOrganizer in some way?\nIf it exists please let me know.\nIn Quicken I use the calendar to do most of my work. All of my scheduled transactions shows up in the calendar and I can see when I need to pay bills. I like seeing the calendar it puts thing into a perspective that I can see better than just a list of scheduled transactions."
    author: "Zot"
  - subject: "Re: Calendar"
    date: 2004-06-19
    body: "A calendar view for scheduled transactions is available in KMyMoney 0.6. See the attached file or http://www.kde-apps.org/content/preview.php?preview=3&id=10180&file1=10180-1.jpg&file2=10180-3.png&file3=10180-3.png&name=KMyMoney\n"
    author: "Thomas Baumgart"
  - subject: "Re: Calendar"
    date: 2004-06-19
    body: "That looks like what I want!\nI will defately need to download it now!\nThanks."
    author: "Zot"
---
After almost a year and a half since its last release, a double-entry version of the <a href="http://kmymoney2.sourceforge.net/">KMyMoney</a> personal finance management software for KDE is <a href="http://sourceforge.net/project/showfiles.php?group_id=4708">now available</a>. Version 0.6 supports multiple account types, including credit cards, cash, savings, loans and mortgages.






<!--break-->
<p>An <a href="http://www.kde-apps.org/content/preview.php?preview=1&id=10180&file1=10180-1.jpg&file2=10180-2.png&file3=&name=KMyMoney">easy-to use GUI</a> allows entering and viewing your financial data.  Adjustable homepage can be customized and wizards guide the user through more complicated transactions. Transactions can be entered in an easy-to-use checkbook type form, or edited directly in the register.  Recurring transactions can be scheduled and transactions can be split to multiple categories and accounts. All transactions can be listed for a given payee.  We have also implemented powerful search functions to find transactions.</p>

<p>While older versions saved data in a binary data format, Version 0.6 writes its data in a compressed XML format, which makes it easy to preserve and store financial data.  Also, the software supports the import and export of QIF files, allowing users to move data to/from external sources.  Users can customize the QIF profile, to be able to import files which may not be standard, without making any code changes.</p>

<p>At the moment <a href="http://www.kde-apps.org/content/preview.php?preview=2&id=10180&file1=10180-1.jpg&file2=10180-2.png&file3=&name=KMyMoney">translations for French</a>, German and Russian are available.<p>

<p>Additional <a href="http://kmymoney2.sourceforge.net/release-plan.html">features being considered for future releases</a> include multiple currency support, investment account support, financial report generation, OFX support, online stock quote updates, HBCI, and a GnuCash import function. Please feel free to see more details on the <a href="http://kmymoney2.sourceforge.net/">project web-site</a>. Some of these functions are already available in our current CVS repository. <p>



