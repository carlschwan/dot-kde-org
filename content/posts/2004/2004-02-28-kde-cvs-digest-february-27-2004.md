---
title: "KDE-CVS-Digest for February 27, 2004"
date:    2004-02-28
authors:
  - "dkite"
slug:    kde-cvs-digest-february-27-2004
comments:
  - subject: "very good stuff"
    date: 2004-02-28
    body: "\"Benoit Walter committed a change to kdenonbeta/kcontrol3\n\nSome preliminary work on a new control center, with focus on usability.\n\nHere are the features, heavily inspired by the huge number of messages I\nread on the usability mailing-list:\n- Task oriented interface\n- More useful search system (as seen in JuK, react on each keypress event)\n- More visible help (+context help)\n- Still using the current organisation (categories)\n- Much improved navigation system\n- No sub-windows\"\n\n\n"
    author: "anon"
  - subject: "Re: very good stuff"
    date: 2004-02-28
    body: "Screenshots for the curious: http://lists.kde.org/?l=kde-usability&m=107758048305856&w=2"
    author: "Anonymous"
  - subject: "Re: very good stuff"
    date: 2004-02-28
    body: "Shweet. Looks way better than the current one."
    author: "Anno"
  - subject: "Re: very good stuff"
    date: 2004-02-28
    body: "desktop and KDE component should not be the same icon. Use a KDE gears icon for \"KDE Component\"."
    author: "gerd"
  - subject: "Re: very good stuff"
    date: 2004-02-28
    body: "nice. (only watched the shots)\nbut wouldn't it be better to hide the keyword option per default? for n00bs it's confusing (or isn't it?). maybe in place of \".. or enter a query\" a button wich unhides the text field.\nhmmm, and a button which toggles between the config page and the choose category page, without loosing the currently changed but not applyed settings. but when you choose a new config page then you should be prompted like the current kcontrol dose."
    author: "panzi"
  - subject: "Re: very good stuff"
    date: 2004-02-28
    body: "I have already made some changes which should address those problems (eventhough the search entry is not hidden). So the screenshots are already outdated :-) Anyway, the work is far from being complete so please try the CVS version if you want to send feedback.\n"
    author: "B. Walter"
  - subject: "Re: very good stuff"
    date: 2004-02-28
    body: "Alright, this simply *rocks*. This looks like a KControl my mother could actually use. Great, great usability judging from the first screenshots. I love how it emphasizes *discoverability*. Rock on."
    author: "Eike Hein"
  - subject: "Re: very good stuff"
    date: 2004-02-28
    body: "Hey hey!, the new KControl is looking very good... congratulations!\nI'll die waiting for it till 3.3 (because it'll be released with 3.3, right ?)"
    author: "Pupeno"
  - subject: "Re: very good stuff"
    date: 2004-02-28
    body: "No decision has been taken yet. As it would be a pity to change the control center at each release, we should first make sure that this solution is good enough, make more usability tests, etc... Otherwise, it will be worth waiting a little bit more and have the best control center in KDE 4.0. Let's see...\n"
    author: "B. Walter"
  - subject: "Re: very good stuff"
    date: 2004-02-29
    body: "Oh, that's unfortunate.\nWe could take a very good use of an improved KControl in Ark Linux (http://www.arklinux.org) where I'm trying to push KControl as a general control panel application and make all the configuration dialogs a KCM (which is proving to be hard as well), but the current KControl is not very nice for that generic control pannel task, just becuase of how it is organized (I mean, KControl, not the KCMs).\nDo you think, in the case it will be hold to KDE 4.0, that it would be posible to make separate releases of that KControl ? or something like that ? (hey!, it seems like I'm postulation ourself for testing it in a working enviroment ;)\nThanks."
    author: "Pupeno"
  - subject: "Re: very good stuff"
    date: 2004-02-28
    body: "i've read some posts by aseigo on kde-usability against changing the kcontrol tree layout in minor releases - so i think there would be quite some objection to replacing kcontrol compeletely for 3.3..\n \nThat said, this kcontrol replacment looks very nice, and i hope to see it in kde4 (:"
    author: "je4d"
  - subject: "Re: very good stuff"
    date: 2004-02-28
    body: "What is the \"System Administration\" option? Is this going to attempt to configure non-kde Applications like Apache...?"
    author: "Jeff Brubaker"
  - subject: "ctrl-T  is new tab in konqueror"
    date: 2004-02-28
    body: "Hooray!!!! "
    author: "Ed Moyse"
  - subject: "Re: ctrl-T  is new tab in konqueror"
    date: 2004-02-28
    body: "That sucks! I want my CTRL-T = Terminal window back! I use that *all* the time!"
    author: "Andr\u00e9 Somers"
  - subject: "Re: ctrl-T  is new tab in konqueror"
    date: 2004-02-28
    body: "Click settings, configure shortcuts on the menubar, and change it back to your prefered setting.\n\nThe reason why it was changed is because the majority of users want tabs, not terminals"
    author: "norman"
  - subject: "Re: ctrl-T  is new tab in konqueror"
    date: 2004-02-28
    body: "This majority of users argument sucks! Nowadays some people seem to think that it is enough to justify anything, and worst than that, they don't even try to use it in a way that makes sense... WTF does the majority of users needing tabs has to do with this? Even if most people needed Terminals IMO it would still be a good change. We keep shortcuts for both cases and the one for tabs is consistent with other browsers."
    author: "Source"
  - subject: "FINALLY"
    date: 2004-02-28
    body: "I'm always used to Ctrl+T in Mozilla FIrebird, Mozilla 1.6 etc. I never sue my terminal as often as I use tabs, that's why I always make the terminal Ctrl+Z and tabs Ctrl+T.\n\nGOOD JOB KDE TEAM!"
    author: "Alex"
  - subject: "Re: FINALLY"
    date: 2004-02-28
    body: "Guess what! I use Ctrl+T in Mozilla firebird, I probably don't create new tabs as often as I sue terminals and I always make tabs Ctrl+T! \n\nGOOD JOB KDE TEAM!"
    author: "Source"
  - subject: "You make no sense to me"
    date: 2004-02-28
    body: "KDE should tailor its desktop to the majority of users, but remain flexible so that the minority will still have a functional and fun desktop.\n\nI don't know anyonw who uses terminals more often than tabs. Hence due to tabs being used more often and Ctrl+T being more intuitive for tabs and being the standard shortcut in most browsers, it was changed. \n\nAll the people who need the terminal so much should just use Konqueror's terminal emulator, adapt to the new shortcut, or change the defaults themselves, it isn't so hard."
    author: "Alex"
  - subject: "Re: You make no sense to me"
    date: 2004-02-28
    body: "Incredible, did you read my message? \n\nWe should try to please the majority of users. I never said we shouldn't if you think I did, read again.\n\nAlso I agree with you, you agree with me, and this has nothing to do with more people using terminals then tabs. Even if they didn't this would have been the right choice.\n\nIs this that complicated to follow?"
    author: "Source"
  - subject: "Re: ctrl-T  is new tab in konqueror"
    date: 2004-02-28
    body: "I'm sure a lot of people have been waiting for this! :)"
    author: "Steffen"
  - subject: "Re: ctrl-T  is new tab in konqueror"
    date: 2004-02-28
    body: "Well, I use Ctrl+T for new tab as well. But I've not really been waiting for it, it's quite easy to adjust it yourself ;)"
    author: "Arend jr."
  - subject: "Re: ctrl-T  is new tab in konqueror"
    date: 2004-02-28
    body: "Nice. :)"
    author: "Carlo"
  - subject: "Re: ctrl-T  is new tab in konqueror"
    date: 2004-02-28
    body: "I agree with this change mostly, since it will be more familiar to most OSS users. However I think perhaps it is unwise to release the change with 3.3, if there will be a 3.3 release. People will allow changes to common shortcut keys in common applications for major version changes (some will be quite annoyed with that too), but it is far more acceptable to make this change with the jump to KDE 4.0.\n\nIf there is a KDE 3.3 then perhaps it should be released with this shortcut still set to CTRL+SHIFT+Key_N. For the sake of not making users dislike the idea of upgrading .x releases. People who can't wait that long can easily change the shortcut.\n\nApologies that I didn't bring this up before the change, but kde-usability has become too high-traffic for me to keep up with."
    author: "Max Howell"
  - subject: "Re: ctrl-T  is new tab in konqueror"
    date: 2004-02-28
    body: "Shouldn't www.freedesktop.org form a specification for different profiles for keyboard shortcuts ? We'll have different profiles for people coming from windows, apple, or unix. Both GNOME and KDE will have their separate profiles as well. But every application, whether in GNOME or KDE will follow the profile choosen by the user."
    author: "kx"
  - subject: "Re: ctrl-T  is new tab in konqueror"
    date: 2004-02-28
    body: "Define coming from Unix kind of shortcuts...\nBut yeah a standard for at least an important subset of shortcuts would be quite nice, I guess."
    author: "Source"
  - subject: "Re: ctrl-T  is new tab in konqueror"
    date: 2004-02-29
    body: "> If there is a KDE 3.3 then perhaps it should be released with this shortcut still set to CTRL+SHIFT+Key_N. For the sake of not making users dislike the idea of upgrading .x releases. People who can't wait that long can easily change the shortcut.\n\nctrl+shift+n still is the default shortcut. ctrl+t is the alternative shortcut."
    author: "Simon Perreault"
  - subject: "Re: ctrl-T  is new tab in konqueror"
    date: 2004-02-28
    body: "yay!"
    author: "anon"
  - subject: "A wish"
    date: 2004-02-28
    body: "It would be great if you could embend a browser component and external aplications in the new Kcontrol so web only configuration applications (like webmin and swat from samba) and distribution only applications (like mandrake control center, yast and synaptic) could be seen inside kcontrol.\n\nI remember that there used to be a couple of projects that allready did that but if Im not mistaken they were for kde2.\n\nJust a wish"
    author: "Mario"
  - subject: "Re: A wish"
    date: 2004-02-29
    body: "I agree with you.\n\nHowever, there is already an excellent plug-in for kcontrol and konqueror if you want to easily set up a samba server : ksambaplugin (cf http://ksambakdeplugin.sourceforge.net/).\nI didn't test it very much but it does seem to have all SWAT features.\n"
    author: "Nicolas Blanco"
  - subject: "Ugly"
    date: 2004-02-29
    body: "The shots are ugly. I hate those guys that think good usability means cloning Windows.\nA better categorization is welcome, Some stuff currently is misplaced or at least hard to find (Pointer themes comes to mind, it is under \"Peripherals/Mouse\", and it should be under \"Look&Feel\".\nCleaner layout for each category, ok. Huge tabs that didn't fit in 800x600 screens, and a lot of tabs in the same category are bad design choices (MS does worse, though, with its multiline tab configs).\nI'll be happy if every item is properly documented in help and has a good tooltip, help is currently lacking in a lot of places.\nBut the basic layout of KControl IMHO is RIGHT!!!\n"
    author: "Shulai"
  - subject: "Re: Ugly"
    date: 2004-02-29
    body: "> But the basic layout of KControl IMHO is RIGHT!!!\n\nOnly if you want large unweirdly trees.\n\n> The shots are ugly\n\nHave you tried it? It's quite intuative. "
    author: "anon"
  - subject: "Re: Ugly"
    date: 2004-02-29
    body: "I am happy if kde helps beginners, but I also like the current kcontrol.\nIf you know it, a tree is great, because you have everything in one place.\n\nWill there be an option for changing the view mode ? So more advanced people can get their old kcontrol back ?\n"
    author: "Felix"
  - subject: "Re: Ugly"
    date: 2004-02-29
    body: "Yep, I agree: Trees are better."
    author: "Niels"
  - subject: "MARKETING"
    date: 2004-02-29
    body: "When will KDE market itself better!??\n\nI wanted to e-mail my friend some official KDE 3.2 screenshots, and guess what, tehre aren't any. \n\nIt's been almost a month and the latest KDE screenshots are from 3.1.\n\nhttp://kde.org/screenshots/\n\nMarketing is half as important as code IMO!"
    author: "Alex"
  - subject: "Re: MARKETING"
    date: 2004-02-29
    body: "Looks to me as if you've got a job to do then. Of course, first you have to realize that all-caps subjects and rows of questions marks (or exclamation marks) are not quite done, but after that you can settle down to do you bit of work."
    author: "Boudewijn Rempt"
  - subject: "Re: MARKETING"
    date: 2004-02-29
    body: "I could do them, sure, are there any guidelines?"
    author: "Alex"
  - subject: "Re: MARKETING"
    date: 2004-02-29
    body: "Yes, I think remembering to read that there where\nguidlines. What I remember the most was that the\ntheme needed to be the then default theme (now\nkeramik).\n\nYou could use google to find the document, if there\nis any, with guidelines."
    author: "pieter"
  - subject: "Re: MARKETING"
    date: 2004-02-29
    body: "http://events.kde.org/faq/promo.php"
    author: "Datschge"
  - subject: "Re: MARKETING"
    date: 2004-03-01
    body: "Why official? Send your owns or pick from http://wiki.kdenews.org/tiki-index.php?page=Screenshots"
    author: "Anonymous"
  - subject: "Observations by Agent Smith:"
    date: 2004-03-01
    body: "A large area is wasted on Information and \"What is?\" which will not be used alot of the time. Why not buttons like in kdevelop? I know I am just a stupid AI.\n\nOnce in a section (e.g Fonts), new users might wonder how the Neo would you get back to the list.\n\nOtherwise it looks functional.\n\n"
    author: "etoibmys"
  - subject: "Re: Observations by Agent Smith:"
    date: 2004-03-01
    body: "What is? Should be a popup like all other apps, unless of course there is too much What is? information, in which case it should be shortened."
    author: "etoibmys"
  - subject: "Re: Observations by Agent Smith:"
    date: 2004-03-01
    body: "The keyword search should not be shown as default."
    author: "etoibmys"
  - subject: "Re: Observations by Agent Smith:"
    date: 2004-03-01
    body: "I totally disagree. This is the easiest way to find things and should definitely be shown by default.\n\nRich.\n"
    author: "Richard Moore"
---
In <a href="http://members.shaw.ca/dkite/feb272004.html">this week's KDE CVS-Digest</a>: <A href="http://sourceforge.net/projects/kolourpaint/">KolourPaint</A> adds transparent selections. 
Some preliminary work on a new control center. 
<A href="http://kmail.kde.org/">KMail</A> adds IMAP folder expiry. 
<A href="http://www.kdevelop.org/">KDevelop</A> adds <A href="http://opie.handhelds.org/">Opie</A> code templates. 
Plus bugfixes in <A href="http://quanta.sourceforge.net/">Quanta</A> and 
<A href="http://kopete.kde.org/">Kopete</A>.



<!--break-->
