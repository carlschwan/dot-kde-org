---
title: "The People Behind KDE: Fabrice Mous"
date:    2004-03-21
authors:
  - "Tink"
slug:    people-behind-kde-fabrice-mous
comments:
  - subject: "Cool"
    date: 2004-03-21
    body: "Hey, the KDE-NL site actually has KDE 3.2.x screen shots :)Good job to the website maintainers on that detail.\n\nAlso, thanks for the interview. I hope tog et more involved in KDE this year too, maybe write documentation and tooltips."
    author: "Alex"
  - subject: "Re: Cool"
    date: 2004-03-21
    body: "Seems to be more a Beta version from last year."
    author: "Anonymous"
  - subject: "Re: Cool"
    date: 2004-03-23
    body: "The KDE site has too: http://www.kde.org/screenshots/kde320shots.php"
    author: "Anonymous"
  - subject: "What about..."
    date: 2004-03-21
    body: "Thank you Tink and Fabrice!\n\nWhat about an interview with Lauri Watts?"
    author: "Henrique Pinto"
  - subject: "Re: What about..."
    date: 2004-03-21
    body: "http://people.kde.org/lauri.html"
    author: "AC"
  - subject: "Re: What about..."
    date: 2004-03-21
    body: "Yer, Lauri Watts is definitely a real unsung hero. She's done a lot of marvellous work that isn't necessarily what people would call fashionable.\n\nWherever you are Lauri, thanks."
    author: "David"
  - subject: "Re: What about..."
    date: 2004-03-22
    body: "\"Hero\" is the perfect title for Lauri...\n\nKDE developers are wonderful, but as import as them are the people who contribute to KDE in other ways, like Lauri and Fabrice, all the translators, the artists... and, of course, Tink!\n\nOne of the objectives of the Quality Team is to attract more non-developers contributors. If this succeeds, we'll have even more \"heroes\"...\n\nBut since there was already an interview with Lauri, what about Kurt Pfeifle?"
    author: "Henrique Pinto"
  - subject: "Re: What about..."
    date: 2004-03-23
    body: "I contacted Lauri for an update interview a couple of months ago but she hasn't replied to my request yet, maybe this public outpour of support and admiration will persuade her ;-)\n\nI also asked Kurt a while ago but he declined.\n\nThanks for the the compliments, it warms the heart ;-)\n\n--\nTink \n\n"
    author: "Tink"
  - subject: "Re: What about..."
    date: 2004-03-24
    body: "How about a self-interview - Tink on Tink?"
    author: "David"
  - subject: "Houten?"
    date: 2004-03-22
    body: "Hier nog een inwoner uit Houten :)\n\nVery nice interview. Keep up the great work Fabrice :)\n"
    author: "Niek"
  - subject: "Re: Houten?"
    date: 2004-03-23
    body: "Niek, you can find me at #kde-nl on irc. If you want we can chat a bit \n\nFab"
    author: "Fabrice Mous"
  - subject: "task-based help"
    date: 2004-03-24
    body: "> For me the KDE-desktop is complete. But I truly think the KDE desktop needs \n> (taskbased) information.\n\nYes, this would be really nice. Maybe it is a little difficult since KDE is not a distro, though I wish it were.\n\nI got a bit of a shock when I tried to use MacOSX for the first time. I couldn't get the network printer to work so I pressed the help icon, not expecting anything useful, and it had loads of really easy to use task based help. It made it so easy!!\n"
    author: "Dominic Chambers"
---
For <a href="http://www.kde.nl/people">this week's interview</a> we hop over to Europe. We travel to the Netherlands to a town named <a href="http://www.zoekplaats.nl/zoekplaats/cgi/waar.pl?actie=plaats&amp;zoek=houten">Houten</a> where a man lives who spends most his time writing up interviews and articles for the dot, whose ability to look 
good while dancing with bad movements on the dance floor is legendary and who's forever 21, it's <a href="http://www.kde.nl">KDE-NL</a>'s  <a href="http://www.kde.nl/people/fabrice.html">Fabrice Mous</a>!




<!--break-->
