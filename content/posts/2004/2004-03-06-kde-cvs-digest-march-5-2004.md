---
title: "KDE-CVS-Digest for March 5, 2004"
date:    2004-03-06
authors:
  - "dkite"
slug:    kde-cvs-digest-march-5-2004
comments:
  - subject: "Thanks for implementing a requested feature."
    date: 2004-03-06
    body: ">wish #35130 (includes original URL in .war archives)\nWas waiting for this one a long time now.\nExtr\u00eamely useful when doing some research on a specific topic. No need to switch between bookmarks and the war files. A great improvement for my \"producticity\" :)\n\nThanks"
    author: "mhmh"
  - subject: "Internationalization Status"
    date: 2004-03-06
    body: "I notice that the Swedish translation team is always ahead of everybody else. This is the nth time that the Swedish team has 100% translation and the Danish and other slouches have abysmal figures like 99.27%.  :-)\n\nGo Swedish translation team, Go!"
    author: "Inge Wallin"
  - subject: "I second that!"
    date: 2004-03-06
    body: "\"What do you think about June 2004 KDE 3.3 beta and through July\nrelease candidates and beginning of August 3.3?\"\n\nA late August release would be awesome. People have to keep in mind that these are 0.x releases and hence should not go through such a long cycle and include so many new features. Frequent releases are much better. It's a little like washing yourself ;) What would you prefer, a shower everyday or a big long bath every week?"
    author: "Alex"
  - subject: "Re: I second that!"
    date: 2004-03-06
    body: "Agreed.. it's worked for KDE before as well, for example, the KDE 2.0->2.1 cycle, which was 5 months, and the KDE 2.1->KDE 2.2 cycle, which was 5 and a half months long. \n\nKDE 3.x release tend almost to be revoluationary instead of evolutionary, which is cool, but I still liked the 2.x cycles better :)\n\n"
    author: "anon"
  - subject: "Re: I second that!"
    date: 2004-03-06
    body: "Hi,\n\nback then, KDE was struggling to become \"good enough\". Now that it is, the pressure to release that frequent has dropped.\n\nAnyway, I agree that KDE could consolidate its leadership very well with a polished 3.3 release. The KDE Quality Team needs a relative close release date to get its workings tested.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: I second that!"
    date: 2004-03-06
    body: "> back then, KDE was struggling to become \"good enough\". Now that it is, the pressure to release that frequent has dropped.\n\nKDE 1.x had also been \"good enough\".. "
    author: "anon"
  - subject: "Re: I second that!"
    date: 2004-03-06
    body: "Good enough for its time. It's not good enough for now.\n\nKDE 2.0 was not really good enough back then, IMHO. 2.1 was, though."
    author: "Roberto Alsina"
  - subject: "Re: I second that!"
    date: 2004-03-06
    body: "Actually 2.0 was very promising, 2.1 was good and 2.2 what I call \"good enough\"...\n\nSince then, I am just amazed at what can be added.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: I second that!"
    date: 2004-03-06
    body: "1.1, 2.2, 3.3 ... all part of a grander scheme of things? ;-)"
    author: "Aaron J. Seigo"
  - subject: "Re: I second that!"
    date: 2004-03-06
    body: "Oh my god, Aaron, when will 4.4 be release?\n\nYours, Kay\n"
    author: "Debian User"
  - subject: "Re: I second that!"
    date: 2004-03-06
    body: "> Good enough for its time. It's not good enough for now.\n\nYeah, but I don't think KDE 3.2 would be good enough for 5 years down the road either. "
    author: "anon"
  - subject: "Re: I second that!"
    date: 2004-03-07
    body: "Almost no program is. Although I have used Winword 6 a few months ago, and it\u00b4s a damn good word processor (and much more than I need :-)"
    author: "Roberto Alsina"
  - subject: "Building kdelibs without CUPS"
    date: 2004-03-06
    body: "It's impossible to build kdelibs from CVS without CUPS (--disable-cups) since a few days. This is a very annoying problem, more people here experiencing this?"
    author: "Niek"
  - subject: "Re: Building kdelibs without CUPS"
    date: 2004-03-08
    body: "OK, this works again (at least in HEAD). Link: http://lists.kde.org/?t=107844903100004&r=1&w=2"
    author: "Niek"
  - subject: "3.2 arriving in sid :-)"
    date: 2004-03-06
    body: "\nKDE 3.2 has finally landed on debian unstable :-)\n\nTo be precise, it's even 3.2.1!\n\nAnd yes, it feels really faster. Applications & KDE startup, and \nespecially konqueror. Very nice, hats off kde hackers!!!!!\n\nI am a little surprised (positively of course) that I can use kmail 1.5.4\n(from KDE 3.1) in 3.2. I guess that's the thing with binary compatibility.\n\nA happy user"
    author: "ac"
  - subject: "Re: 3.2 arriving in sid :-)"
    date: 2004-03-07
    body: "Hi,\n\na few things are still missing, like kdesdk, quanta in newer versions, but I guess, that's hot on the heels.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: 3.2 arriving in sid :-)"
    date: 2004-03-07
    body: "A few? According to http://packages.debian.org/unstable/kde/ even kdenetwork and kdepim are missing."
    author: "Anonymous"
  - subject: "Re: 3.2 arriving in sid :-)"
    date: 2004-03-07
    body: "Check again.\n\nIts probably just not updated the website yet. They where all in when I asked for them."
    author: "Leonscape"
  - subject: "Re: 3.2 arriving in sid :-)"
    date: 2004-03-07
    body: "- Can I use this with Sarge, too?  How?  \nI tried to find some docs but I'm not sure what exactly to search for...\n\n- Is there a howto that explains the sources.list stuff a little more in-depth than the Debian installation docs? \n\n"
    author: "cm"
  - subject: "Re: 3.2 arriving in sid :-)"
    date: 2004-03-08
    body: "Use \"apt-pinning\"\nThe Web is full of advice how to do it.\n\nHowever you'll no longer have \"pure Sarge\" after that.\nIn the future you may get dependency problems.\n\nI am thinking the same but wait/hope for migration into Sarge after\nthe minimum of 10 days in Unstable \n(and probably longer - as all dependencies with\nall packages in Sarge have to be solved before migration).\n\n\nGood luck!\nO.L. "
    author: "Olaf Lieser"
  - subject: "Re: 3.2 arriving in sid :-)"
    date: 2004-03-08
    body: "Thanks for the hints. \n\n"
    author: "cm"
---
In <a href="http://members.shaw.ca/dkite/mar52004.html">this week's KDE CVS-Digest</a>: Ruby bindings now have DCOP support. 
<A href="http://www.konqueror.org/announcements/reaktivate.php">Reaktivate</A>, a Konqueror module for embedding ActiveX controls, is improved.
<a href="http://www.kde.me.uk/index.php?page=realrekord">RealRekord</A>, an application to record RealPlayer streams, is imported. 
KConfEdit adds a property editor.



<!--break-->
