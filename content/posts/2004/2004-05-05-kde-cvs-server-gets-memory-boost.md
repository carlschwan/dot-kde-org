---
title: "KDE CVS Server Gets Memory Boost"
date:    2004-05-05
authors:
  - "wbastian"
slug:    kde-cvs-server-gets-memory-boost
comments:
  - subject: "companies"
    date: 2004-05-06
    body: "nice to hear that kde is backed by people not only companies.\n\nthats why its so polpular."
    author: "chris"
  - subject: "Any other details?"
    date: 2004-05-06
    body: "Like how big of a data pipe? What type of machine, processor, etc.\n\nMy local cvsup'd copy from last thursday is 12.3 gigs.\n\n<shameless plug>\nhttp://cvs-digest.org/?stat&period=apr302004\n</shameless plug>\n\nThat doesn't include the 1500 or so bot commits, mostly auto generated translation files.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Any other details?"
    date: 2004-05-06
    body: "The machine is a Netfinity 4500R by IBM."
    author: "Anonymous"
  - subject: "Subversion ?"
    date: 2004-05-06
    body: "Is KDE investingating a move to subversion ?\n\nI am using it on a few projects and I must say it is very comfortable. It fixes a lot of those annoying small things of CVS which makes it a pleasure to use."
    author: "Philippe Fremy"
  - subject: "Re: Subversion ?"
    date: 2004-05-06
    body: "Yes, it has been tried, and Subversion could not import the history of even a small kde module. Maybe if we decide to start over with KDE 4, we could consider using SVN, and it's certainly a good thing, but not just now.\n\nI think Coolo and Scott know more details\n\nCheers,\n  Daniel"
    author: "Daniel Molkentin"
  - subject: "Re: Subversion ?"
    date: 2004-05-11
    body: "Do you mean that cvs2svn (http://cvs2svn.tigris.org/) could not convert the project's CVS history to Subversion?  This sounds very plausible.\n\nI do want to clarify, however, that this is not the same as \"Subversion could not import the history\".  Subversion never got the chance to import the history, because of bugs in cvs2svn, which is a separate program.  \n\nIn Subversion, 'import' is analogous to 'cvs import'.  It is not a history-preserving operation, rather, it imports a tree of data into the repository as a starting snapshot.  Subversion can probably import any KDE project with no problem; we don't see many bugs related to import."
    author: "Karl Fogel"
  - subject: "i'd like to donate something too"
    date: 2004-05-06
    body: "i would like to pay in my national currency (czech/slovak crown). is that possible? what should i do?\n\nthx.\n\ncheers,\n\n-- joe"
    author: "jose"
  - subject: "Re: i'd like to donate something too"
    date: 2004-05-07
    body: "Isn't it now, being member in the European Union, possible for you to transfer money to KDE e.V.'s bank account with the same fee as a domestic transfer?"
    author: "Anonymous"
  - subject: "Re: i'd like to donate something too"
    date: 2004-05-07
    body: "This is only valid for countries with Euro."
    author: "Peter"
  - subject: "Re: i'd like to donate something too"
    date: 2004-05-09
    body: "Please don't answer if you don't know the trueth - your answer is simply wrong.\n\nThe only difference with the new countries is that it may take some time until the new rule (same costs as national transfers) is in place everywhere, but sooner or later it will be in these countries as well.\nJust check what your bank is charging and check other banks as well. You will find cheap solutions already."
    author: "temp"
  - subject: "Re: i'd like to donate something too"
    date: 2004-05-10
    body: "The European Unioun only forces the \"same cost as domestic transfer\" rule for a transfer in Euro from an Euro account to another Euro account.\n(See what the FAQ linked from http://www.kde.org/support/support.php )\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: i'd like to donate something too"
    date: 2004-05-08
    body: "In theory, especially from the European Union, you could transfer money to the account given under \"Money Transfer\" at: http://www.kde.org/support/support.php\n\nHowever in practice, you should inform you about bank fees first and be careful that KDE e.V. should pay as less as possible bank fees on its side  from this transfer. (That might be possible to set it that way when ordering the money transfer or it might not be possible.)\n\nIn any case, your bank (or any other ones, for example in Germany even some Money Exchange shops can do international money transfers) is where you have to inform you.\n\nHave a nice day!"
    author: "Nicolas Goutte"
---
KDE's main
<a href="http://www.cvshome.org/">CVS</a>
server received a memory upgrade today bringing its total memory to 2.5 GB RAM. The CVS server forms the heart of KDE's technical infrastructure and is responsible for providing KDE developers world wide with up to date versions of the KDE software. Due to the increasing number of KDE developers and KDE hosted projects the previous configuration reached its limits. With the new memory we expect to be able to provide reliable operation for quite some time to come. The memory upgrade has been financed from
<a href="http://www.kde.org/support/support.php">the generous donations</a> that KDE 
<a href="http://www.kde.org/support/donations.php">has received</a>. Thank you!



<!--break-->
