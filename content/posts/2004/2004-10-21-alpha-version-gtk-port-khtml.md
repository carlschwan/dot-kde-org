---
title: "Alpha Version of Gtk+ Port of KHTML"
date:    2004-10-21
authors:
  - "jriddell"
slug:    alpha-version-gtk-port-khtml
comments:
  - subject: "Awesome that Nokia is doing this.."
    date: 2004-10-20
    body: "The same folks made Minimo (Mini-Mozilla). Seems like they are preferring khtml these days for embedded stuff. "
    author: "anon"
  - subject: "Re: Awesome that Nokia is doing this.."
    date: 2004-10-21
    body: "Or just evaluating all alternatives one after the other."
    author: "Anonymous"
  - subject: "A Win/32 KHTML Browser?"
    date: 2004-10-20
    body: "Since GTK+ can run on Windows, will there be a Win32 port of this new browser? That will truly increase khtml's mindshare to be equal to gecko's. "
    author: "anon"
  - subject: "Re: A Win/32 KHTML Browser?"
    date: 2004-11-15
    body: "As mentioned, Gtk+ WebCore project provides a port of KHTML (from the Apple Safari code branch) to Gtk already. See http://gtk-webcore.sourceforge.net/ \n\nMaking a port to Windows would be relatively easy to do by using the Gtk version for Windows. The main work items would be to remove a few dependencies on X Windows that are currently in their code, to integrate the Windows HTTP framework, and to make a nice Browser UI / Application on top of KHTML. \n\nSuch port using Gtk for Windows would probably be a good start. However, it would probably have suboptimal performance. \n\nTo get better performance one could remove the calls to Gtk in their porting layer and replace them by calling similar win32 functions directly. Surely, this would be a much bigger project, but the Gtk+ WebCore project has done common ground work already by making KHTML independent of KDE and Qt.\n"
    author: "Guido"
  - subject: "Engine swapping"
    date: 2004-10-21
    body: "This is just getting wierd.\n\nFirst someone ports gecko to qt/kde, now khtml is ported to gtk.\n\nAre all the projects going to start playing rendering engine pass-the-parcel? (Or should that be parse-the-parcel? Ah- ha ha ha. See what I did there? Ok, that's enough)"
    author: "Robert"
  - subject: "Re: Engine swapping"
    date: 2004-10-22
    body: "Agreeing on a common plugin API for HTML rendering engines would be a lot more useful than swapping back and forth."
    author: "Lee"
  - subject: "KDE porting to GTK"
    date: 2004-10-21
    body: "KDE porting to GTK.\nSeems that this is what this guys are making possible in not so distant future.\n\nI foresee raise OSS projects that port KDE technology to GTK, forking around this project and other OSS kfriends (gfriends will be happy too help and to copy KDE technology to GTK).\n\nNext features I guess we will see shortly are Kioslaves and Kparts ported to GTK.\n\nThen the road is open for future of GTKonqueror, GTKate or even GTKDE.\n\nWhat would the GTKDE community need shorty is an ability to \"embed\" a legacy K/QWidget in a GTK app - then the migration can start from top to bottom (e.g. initlally GTKate would be just a \"Hello World\" app that just embeds the original Kate and then starting from top to bottom would port anything).\n\nAre there GTKDE volunteers?\n(I am sure NOKIA would fund such project - seems that this is now a \"common\" goal)\n\n\nPS: I am not sure if from business point of view if it would be cheaper for NOKIA to just buy Trolltech and relase Qt under LGPL/BSD instead of funding the GTKDE future of KDE."
    author: "Anton Velev"
  - subject: "Re: KDE porting to GTK"
    date: 2004-10-21
    body: "Seems like a lot of work.  From a business point of view, if Nokia was interested in Qt, they could just use Qt under either license Trolltech offers today.  The GPL does not prohibit commercial use or profit, but if they just hate the GPL, Qt's other licenses compare reasonably with proprietary development suites.\n\nMy guess is they just like KHTML and GTK.  Not the choise I'd have made (the exact opposite, in fact), but they're free to do what they like.\n"
    author: "ac"
  - subject: "Re: KDE porting to GTK"
    date: 2004-10-21
    body: "See, it's not the issue with buying license, but not having to buy it (e.g. freedom).\nCompanies like Sun and Novel for sure could afford this but they choose other toolkits although they were not superior.\n\nGTKDE would definitely win a lot of corporate friends, at least they will have now one more option (right now only Gnome). KDE definitely is superior to Gnome in terms of technology, but unfortunately not very friendly to the corporate world right now, whereas the existanse of GTKDE initiative would turn the things into right directon.\n\nGTKDE initiative already exists \"virtually\" but nothing \"official\". All this efforts to port stuff from KDE, or try to \"dress\" a GKT app to look like KDE app, or to use KDE file and print dialogs in GTK apps for me is just the birth of GTKDE initiative, and demonstrates the rejection of dual license \"GPL or Pay\" model."
    author: "Anton Velev"
  - subject: "Re: KDE porting to GTK"
    date: 2004-10-21
    body: "For me it isn't, and this is the last we'll hear of it."
    author: "Robert"
  - subject: "Re: KDE porting to GTK"
    date: 2004-10-21
    body: "I guess (I hope any fellow KDE developer support this claim) that a good part of KDE superiority comes simply from the Qt quality, and the inspiration from the Qt design put into kdelibs.\n\nBesides I doubt about anybody else really willing a GTK+ based KDE; even more, GTKHTML still has Qt code running over a GTK+ wrapper with Qt interface.\nIt is NOT a real \"native\" KHTML, is still C++ code, is just a hack to get it running on top of GTK+.\n\n"
    author: "Shulai"
  - subject: "Re: KDE porting to GTK"
    date: 2004-10-21
    body: "I guess I don't see your point.  You DON'T have to pay for Qt (I know I certainly don't).  You can get the GPL version.  You're free to use it for commercial and for-profit purposes.  I don't see the problem at all, except for people who just don't like the GPL.  And since Sun and Novell are sellng Linux, they clearly don't have a problem with the GPL.\n\nMaybe you don't understand the GPL?"
    author: "ac"
  - subject: "Re: KDE porting to GTK"
    date: 2004-10-21
    body: "U just dont get the point. To link to a GPled library, your application has to be GPL. There is no other way. And if you dont want to release your application under GPL, u will have to link against a non-GPL library, which is the reason why most system libraries are released under a dual license of GPL and LGPL. \n\nThe fact that you have used the GPLed Qt for your commercial application just shows your lack of understanding and as I understand it, you stand a high chance for being legally confonted.\n\nHowever, I guess you can have a non GPLed app if it is only for internal uses and isnt distributed to the general public."
    author: "Sarath"
  - subject: "Re: KDE porting to GTK"
    date: 2004-10-21
    body: "> To link to a GPled library, your application has to be GPL.\n\nThis is a lie perpetrated by GNOME trolls and shows *your* lack of understanding of the GPL.  All your application has to be is GPL-compatible.  \n\nTHIS DOES NOT MEAN IT HAS TO BE GPL."
    author: "ac"
  - subject: "Re: KDE porting to GTK"
    date: 2004-10-21
    body: "Well i suggest that you read http://www.trolltech.com/products/qt/opensource.html.\n\nHere is a excerpt from that page:\n\"If you wish to use a Qt Free Edition, you must contribute all your source code to the open source community in accordance with the GPL or the QPL's license terms.\" \nNow, either way (GPL/QPL) you have to release your source code QPL isnt exactly a license that most companies would choose to release their products on. Which brings me to my first point that to develop a commercial product you will have to purchase a commercial license of Qt. \n\nNot that it is bad. Qt is one great toolkit and coming to think of it, in the era that prople buy M$ products, i dont see any harm in buying something that will ultimately help the free community.\n"
    author: "Sarath"
  - subject: "Re: KDE porting to GTK"
    date: 2004-10-21
    body: "Hey, jackass.  Nothing you quoted from that page contradicted what I said.  Take a basic course in logic or something..."
    author: "ac"
  - subject: "Re: KDE porting to GTK"
    date: 2004-10-21
    body: "So let me see. What license do you release your app under ? Just convince me (and trolls) that you manage to get away with your app under any commercial license using the GPL Qt."
    author: "Sarath"
  - subject: "Re: KDE porting to GTK"
    date: 2004-10-21
    body: "BSD?  LGPL?  Who said anything about commercial?  You said:\n\n\"To link to a GPled library, your application has to be GPL. There is no other way.\"\n\nNow go away."
    author: "ac"
  - subject: "Re: KDE porting to GTK"
    date: 2004-10-21
    body: "Hey... Even if you're right, there's no need to be rude about it."
    author: "AC"
  - subject: "Re: KDE porting to GTK"
    date: 2004-10-21
    body: "Just back off, say you're wrong, and sod off."
    author: "Joe"
  - subject: "Re: KDE porting to GTK"
    date: 2004-10-21
    body: "> You're free to use it for commercial and for-profit purposes.\n\nThis was from ur first post and it is the main thing why i did waste my time trying to educate you. Maybe you would like to clarify what exactly was on your mind when u said that the free Qt can be used for commercial applications."
    author: "Sarath"
  - subject: "Re: KDE porting to GTK"
    date: 2004-10-21
    body: "I think it is you who needs to be \"educated\". GPL'ed software can also be commercial. you can sell GPL'ed software and you can earn profit from it. If you don't believe me, read what the FSF says about it:\n\nhttp://www.gnu.org/philosophy/selling.html\n\nFor the first few years of the GNU-project, RMS supported himself by selling Emacs."
    author: "Janne"
  - subject: "Re: KDE porting to GTK"
    date: 2004-10-21
    body: "That was another ac actually, but whatever, he's right.  Sorry for being rude, I honestly thought you were a troll by the way u rite & claim things 2 b tru wen der n/t."
    author: "ac"
  - subject: "Re: KDE porting to GTK"
    date: 2004-10-22
    body: "> BSD?\nI don't understand this.  Qt is licensed under GPL or QPL.  So how come there are legitimately KDE apps licensed under BSD (e.g. noatun) when they link directly with Qt?"
    author: "muhaa"
  - subject: "Re: KDE porting to GTK"
    date: 2004-10-22
    body: "Read the QPL and you'll find out."
    author: "Datschge"
  - subject: "Re: KDE porting to GTK"
    date: 2004-10-25
    body: "Because the BSD licence allows you to do pretty much anything you want with the program except claim that you wrote it.\n\nReleasing it under the GPL is one of the things you can do with it.\n\nNoatun linked with QT is a GPL app.  You do however have the option of separating the Noatun code from QT and using under the terms of the BSD licence."
    author: "Jonathan Bryce"
  - subject: "Re: KDE porting to GTK"
    date: 2004-10-25
    body: "The BSD license has nothing to do with this situation. Noatun can be linked to Qt/X11 even as a BSD app. This is possible since the one big obligation the QPL sets is that the app's source must be freely available. This is fulfilled with using the BSD, any other open source license allowing freely available sources would do so as well. Please read the QPL at least once."
    author: "Datschge"
  - subject: "Re: KDE porting to GTK"
    date: 2004-10-21
    body: ">To link to a GPled library, your application has to be GPL. There is no other way.\n\nOnly if you release the app. Also, it is GPL compatable libraries, which is not the same as saying GPL only.\nSince most corporations do not release apps, they do not care. Also, many corporations have no issue with paying for a library which is solid. And that includes companies that selling closed software.\n\n\n\n>which is the reason why most system libraries are released under a dual license of GPL and LGPL. \n\nMost System libraries are under just the LGPL (which originally meant the library GPL).\n"
    author: "a.c."
  - subject: "Re: KDE porting to GTK"
    date: 2004-10-21
    body: ">U just dont get the point. To link to a GPled library, your application has to be GPL. There is no other way.\n\nThat must be why the KDE libs are LGPL, right?\nPlease get your facts strait before spreading FUD."
    author: "Andre Somers"
  - subject: "Re: KDE porting to GTK"
    date: 2004-10-21
    body: "Ok i will clarify my point once and for all. What i was saying was with regarding to using the free version of Qt in a commercial application. I never said that LGPL apps cannot link to a GPL library. The point is that keeping a application in a non GPL compatible license is impossible when linking to libraries that are GPL."
    author: "Sarath"
  - subject: "Re: KDE porting to GTK"
    date: 2004-10-21
    body: "It seems - if I may add my 0.02$ - that you have a big misunderstanding about \"commercial\", \"Closed Source\" means.\n"
    author: "Harald Henkel"
  - subject: "Re: KDE porting to GTK"
    date: 2004-10-21
    body: "but why whould anyone want to use the free version of QT in a commercial application??? commerical company's want support, and they want to pay for that, actually most company's prefer paying for something even if almost the same thing can be downloaded for free (compare staroffice/openoffice).\n\nso a commercial company prefers the comercial version of QT. they will get support for their money.\n\nI dont see the problem. QT is a better toolkit than GTK, so the (small) amount of money a company has to pay (and they only have to pay if they want to make a closed-source program, which they shoulnt anyway) is no problem.\n\nany company can use the free version of QT, as long as their app stays in house, or is free software. if they want to earn money by dis-allowing their users the right to see and modify and give away the source, well, let them pay, so the users get at least some improvements in QT for their money.\n\nI think Richard Stallman should prefer QT over GTK... GTK allows company's to create non-free software without ANY contribution to the community. Trolltech (QT's company) forces them to pay them to enhance QT, so QT gets better, and the community can say: profit for us after all!\n\nDont use Gnome or GTK, it promotes NON-FREE SOFTWARE!!!"
    author: "superstoned"
  - subject: "Re: KDE porting to GTK"
    date: 2004-10-21
    body: "\"GTK allows company's to create non-free software without ANY contribution to the community.\"\n\nThis is not true, they have to contribute modifications they do on on the gtk libs. As an example this is what happened with KHTML. Apple used it and had to give back all the modifications (according to the LGPL) which is (one of the reasons) why KHTML is much better now and who cares if we don't get the source of Safari?. It would be cool if another giant like Nokia could help improving KHTML, we would end up with the best HTML engine ever (although I believe this is already the case :)).\n\nRemember that LGPL orders u to give back all the modifications u make to the LGPLed app or lib u're using but not the code of the actual app u're building on top of them (the LGPLed app or lib u're using)."
    author: "Pat"
  - subject: "Re: KDE porting to GTK"
    date: 2004-10-21
    body: "you say they CAN give things back. but they can also NOT give things back. with KDE/QT thats impossible... OR they create free apps, OR they pay Trolltech (to enhance QT)"
    author: "superstoned"
  - subject: "Re: KDE porting to GTK"
    date: 2004-10-21
    body: "if they modify the LGPL app (libgtk or khtml), they HAVE to give it back, it's an obligation, don't think that apple is just being nice."
    author: "Pat"
  - subject: "Re: KDE porting to GTK"
    date: 2004-10-21
    body: "I was talking about using, not changing. I know changes have to be given back, gpl or LGPL, GTK or QT.\n\nyou can use Gnome/GTK to create a proprietary application and you dont have to give anything to the community, just plain profit. You can't do that with QT, because if you do, you have to pay Trolltech (and this money goes into QT) OR you have to make your app available under a free licence... thats what I meant. and I guess you understood it, but didnt want to?\n\nby the way, the Free Software Foundation now says you shouldnt use the LGPL, and use the GPL instead. they also say L-gpl stands for Lesser-GPL, not Library GPL (anymore).\n\nnice day."
    author: "superstoned"
  - subject: "Re: KDE porting to GTK"
    date: 2004-10-21
    body: "\"You can't do that with QT, because if you do, you have to pay Trolltech\"\n\nUnfortunately, Trolltech doesn't produce GPL only software, as an example the PIM application of there qtopia phone edition isn't free at all. At least when u use GTK to build an non-free app u're not funding another proprietary sofware. Plus if it wasn't for lgpl apple would have never picked up on khtml and look what happened to wine when they switched to lgpl.  "
    author: "Pat"
  - subject: "Re: KDE porting to GTK"
    date: 2004-10-22
    body: "hmm, you're right. some of your money might go to non-gpl software. although almost all qt employees work (paid) on KDE :D so still :D\n\nanyway, my point wasnt about gpl/non-gpl primarilly, but about GTK/QT. dont say GTK is more free - its like BSD/GPL. BSD might be more free, but allows for less freedom/in case of QT/GTK, GTK allows for less contribution to the community. \n\nSo instead of all these ppl saying \"dont use QT its non-free\" I'd like to state \"use QT, it enforces company's to give back more than GTK\". both arguments are true, in a way (btw note again QT is available under the GPL license, while GTK is under the LESSER GPL(LGPL), license).\n\nI was (am) just tired of all GTK trolls."
    author: "superstoned"
  - subject: "Re: KDE porting to GTK"
    date: 2005-01-22
    body: "teste"
    author: "Breno"
  - subject: "Re: KDE porting to GTK"
    date: 2004-10-21
    body: "It appears I'm arguing with the artist formerly known as Prince, everybody.\n\nWhat \"U\" need to understand is that:\n- If you derive your code from GPL'ed code, such as the Qt libraries, your code must also be GPL'd (you seem to get this point)\n- You may sell your GPL'd app commercially for as much money as you wish to charge (this seems to be the tough one for you)\n\nNow, I do understand, that if you just plain don't want to distribute your app under the GPL, you don't want to use a GPL'd library.\n\nIf you are confused about what the GPL does and does not prohibit, please read this: http://www.gnu.org/licenses/gpl-faq.html#DoesTheGPLAllowMoney\n"
    author: "ac"
  - subject: "Re: KDE porting to GTK"
    date: 2004-10-22
    body: "Nope it is nothing like that. Just that only mysql and qt are afaik the only popular GPLed commercial apps out there ... So i just assumed that most people would implicitly take it that very few companies will go on to release their product under GPL.\n\nI dont kno who is Prince. Maybe a mistaken identity ??? \nWell i have no doubts that GPL allows for making money. ( Where would Qt be elsewhere ??? ) \n"
    author: "Sarath"
  - subject: "Re: KDE porting to GTK"
    date: 2007-05-12
    body: "From http://www.gnu.org/licenses/gpl-faq.html\n\n\"For example, someone could pay your fee, and then put her copy on a web site for the general public.\"\n\nIf making money is your goal, it wouldn't make a lot of sense to release under GPL, as someone could buy a copy and host a copy online accessible for free."
    author: "Name"
  - subject: "Mandatory joke"
    date: 2004-10-21
    body: "I foresee raise OSS projects that port KDE technology to GTK, forking around this project and other OSS kfriends (gfriends will be happy too help and to copy KDE technology to GTK).\n \ngfriends are happy when nerdiness is under control and you devote some time to them :-)\n"
    author: "Shulai"
  - subject: "Re: Mandatory joke"
    date: 2004-10-21
    body: "Well, it is kind of weird. Apple ported khtml to Mac OSX and got rid of Qt and don't really contribute back. Now Nokia ported Apple khtml back to Linux but wfor Gtk. I really don't know, but I think using this as a base and supporting both Gtk/Gnome and Qt/KDE must be possible. Since it is planned to put this Gtk khtml on a public cvs server, this brings the opportunity to attract more developers (i.e. not only KDE developers) to khtml.\n\nBye\nAlex\n"
    author: "aleXXX"
  - subject: "Re: KDE porting to GTK"
    date: 2004-10-21
    body: "This would be unfortunate.\n\nSince Qt is GPL, it protects the open source community. GTK is LGPL and leaves the open source world at risk, in the hands of economic driving forces."
    author: "The observer"
  - subject: "Re: KDE porting to GTK"
    date: 2004-10-25
    body: "<reality>\nQT is only free for *nix platforms. This is purely because of economic driving forces.  GTK is free on all platforms it supports.\nIs it the freedom of QT that keeps 90% of desktop users from running Khtml?\n</reality>\n\n"
    author: "tomten"
  - subject: "Re: KDE porting to GTK"
    date: 2004-10-26
    body: "Bullshit. Qt/X11 and Qt/Mac are completely Free Software, and nobody hinders you or anyone else to port those to whatever other platform you want. You requesting free ports without yourself being willing to lift any finger on your own just showcases people like you as greedy, asocial bastards."
    author: "ac"
  - subject: "Re: KDE porting to GTK"
    date: 2004-10-26
    body: "My point is that there already exists great ports to other platforms and doing unsanctioned ports would probably prove tricky as most QT developers depend on the revenue the non free platforms provide. \n\n>You requesting free ports without yourself being willing to lift any finger on your own just showcases people like you as greedy, asocial bastards.\n\nI don\u00e2\u0080\u0099t need to lift a finger as I have Wxwidgets and GTK ;-) It just puzzles me that so many free software developers accept being confined to *inx platforms when the main selling point of the GUI they use are portability. But hey who am I to complain I\u00e2\u0080\u0099m just a greedy asocial bastard!\nBoy you should really take a break from and the dot and do some anger management work instead.\n\n"
    author: "The Greedy Asocial Bastard"
  - subject: "Re: KDE porting to GTK - fuse kio"
    date: 2004-10-26
    body: "\"Next features I guess we will see shortly are Kioslaves and Kparts ported to GTK.\"\n\nYou mean fuse-kio or?\nBut that's not just for GTK but for all linux apps.\n\nhttp://kde.ground.cz/tiki-index.php?page=KIO+Fuse+Gateway\nhttp://www.kalyxo.org/twiki/bin/view/Main/FuseKio"
    author: "peppelorum"
  - subject: "Re: KDE porting to GTK - fuse kio"
    date: 2004-10-27
    body: "No! I think a common VFS library for all linux apps would be the right to do..."
    author: "xyz"
  - subject: "Re: KDE porting to GTK - fuse kio"
    date: 2004-10-27
    body: "No! I think a common VFS library for all linux apps would be the right thing to do..."
    author: "xyz"
  - subject: "Anton Velev is back campaigning ;-)"
    date: 2004-10-21
    body: "...we missed him very dearly and thought he had disappeared from the planet already."
    author: "anonymous coward"
  - subject: "Re: Anton Velev is back campaigning ;-)"
    date: 2004-10-21
    body: "Man, I have been around all the time reading stuff.. I am happy 1/3 KDE user (+1/3 OSX, +1/3 XP). Seems I like and prefer KDE to Gnome. I migrate our staff out of XP platform to FreeBSD with KDE.\nHowever I don't feel comfortable with GPL, so KDE for us is just a great desktop like XP but at least more UNIX friendly (for sure). What I am trying to say is that I would never plan basing any GUI app on non-LGPL/BSD platform. I am happy with Java, and I am considering X or Gecko for future but definitely not GTK. However if the good KDE API is ported to GTK and has no GPL obligations would even fund and contribute such projects. Why? Well if you ask yourself a question why FreeBSD and not Linux you would answer yourself.\n\nTalking about UNIX I think I feel right now in a very good condition to write a XP to UNIX migration article.... Let's get started.\n\n\nHOW TO MIGRATE YOUR ENTERPRISE TO UNIX\n--------------------------------------\n\n1. Choosing the right UNIX.\n\nThere are lots of options for choosing UNIX. Most of them coming from the OpenSource world. In most cases a commercial UNIX is more incompatible or hard to build the tons of free apps coming from the OpenSource world. Many of this apps are helpful so you would either have to spend a lot of time of hacking to rebuild your commercial UNIX where you could possibly damage something and lose support. OpenSource UNIXes are therefore more friendly to the free apps.\nWhen choosing your UNIX you must consider the ability to update easy your apps and something to keep the integrity of yuor system. If you always download the stuff yourself and compile you are risking the system unless you are willing to spend lot of time on regular basis to keep this integrity yourself. RPM-like binary is a good option however you are dependent on distro compile options and availability of all packages. RPM source sound better, but not best, and again the availability problem.\nHere you find BSDs and especially FreeBSD with richest ports collection on the net. There are also a couple of Linuxes out there like Gentoo that copied what BSD did. If you have some legacy linux experience already it is a good time to switch to something more real like BSD UNIX. Some people may feel trolling here, so I leave this option to you to choose between the pioneer in ports and the real UNIX - BSDs or the UNIX immitation - Linux.\nIf you don't have UNIX experience choose Linux it will be a easy option, choose a distro with RPMs because they will take care of everything.\n\n2. Preparing the migration.\n\nMake a local file server. Create accounts for all users, and have them save all files in this location, best is if no file exists that is saved on local machine except if it's just something temporary. Prepare a backup script to be safe if something goes wrong with your server, if you cannot do this have one admin to do it for you. You also will need a old machine to serve as your router, here FreeBSD wins. Some people just prefer this little devices that serve as router+switch + eventually wireless, however a old machine + BSD is a better option if you want to have better control.\nUsers that don't understand anything about console, scripts and UNIX at all can continue operating on their XP boxes, but will be ready at later point to migrate to Mac or other UNIX like FreeBSD (if you are good enough to configure the desktop to be enough familiar and install familiar apps (kde+moz+OOo helps)).\nFor other users that have some programming experience something better can be done for them. Install cygwin, putty and WinSCP, get them familiar with this apps. Generally if they need to do a console job on their local machine they will start using cygwin (instead of command.com), if they need to do a console job on the server - putty. Then if they need to transfer to directory that is not shared with Samba WinSCP, note that once you show them the 'scp' command after a certain time they will prefer scp instead of WinSCP.\nOther than that, something that applies for all users, start using OpenOffice and Netscape (last version), this apps are already available on UNIX and once they get comfortable with them they are ready.\n\n3. Acutal migration\nThere is nothing special to do once former XP users already are using Netscape, OpenOffice, putty and cygwin. Once they sit on a real UNIX machine they will feel much more comfortable. Moreover konqueror will ofer them features that are very UNIX friendly while Explorer didn't - for example they can set permissions by accessing file properties, or they can do 'ln -s' by just dragging the dir in konqueror. Users are much happier with a real UNIX instead of the UNIX simulation ran on former XP box - konsole is better than putty, konqueror is better than explorer, Mozilla is same as Netscape, OpenOffice is exact the same, and even more on UNIX they would have much more apps - there are tons of free apps on UNIX.\n\n4. What's next\nStart thinking how to migrate other users to UNIX. For some of them may be the only option will be Mac since it's right now the only UNIX with full set of apps that XP users use. Probably in long term strong commercial apps like Photoshop, Corel or Flash currently supported only for one UNIX platform - Mac, would start supporting other UNIXes.\nAlso you can help your customers to start migration, give them advices to use Netscape (strnong points are that this is business-branded version of Mozilla, has much better security compared to IE and has tabs), also most OpenOffice has some stong points (it's free, it makes PDF (you have first to explain why PDF is good format for documents)). At later point you can have your customers with small router and a local UNIX Samba server if they have more than 3 machines.\nThe direction is clear UNIX everything!\n\n5. Optional step - Motivation\nIf you are a GPL fan you replace everything 'UNIX' in this article with 'Linux', and you can explain to people why GPL is the best to express such motivation to migrate.\nHowever if you are BSD & UNIX fan you would explain to people about the >30 history of UNIX. You would explain why GPL is not good. You would explain why UNIX is reliabe, proven and so on, you could also mention about BSD, X, Apache and MIT licenses and the major difference to GPL. You could also exlain about the unix heritage of BSD, etc, etc whoever prefers a real UNIX compared to UNIX immitation knows the story.\n\n6. Background\nAll this is real story. I was planning to add a X server running on the XP box to ease the migration, but I found this irrelevant since KDE is very friendly these days. The only problem we had with KDE was that in version 3.1.4 on FreeBSD 4.9 the folders in konqueror tree are \"sticking\" to the mouse cursor when you click them, however after recompiling it it was fine."
    author: "Anton Velev"
  - subject: "Re: Anton Velev is back campaigning ;-)"
    date: 2004-10-22
    body: "If you want to recommend Linux to newbies and you love freebsd, obviously you don't choose a rpm based distro but choose debian. Even more easy is installing it right from Knoppix, add in your /etc/apt/apt.conf the line 'APT::Default-Release \"testing\";' and 'apt-get update/upgrade' every day and you're done :-) (maby localize your sources.list too)\nIn our company, some are more or less migrated. But you only talk about common applications. Our problem is autocad/step7/WinCC/ all written for NT/W2k (even upgrading to XP is not always possible). (btw if somebody from Germany reads this and happens to know some Siemons folks, please ask them why they are so MS minded). Anyway sollutions are developing Wine until they work (probably undoable for something so picky like WinCC) or give another OS so much userbase that it would be commercially unwise not to support. And the one, and only one is ... Linux."
    author: "koos"
  - subject: "Re: Anton Velev is back campaigning ;-)"
    date: 2004-10-24
    body: "AFAIK gentoo is the most FreeBSD-like Linux in terms of ports"
    author: "Anton Velev"
  - subject: "Re: Anton Velev is back campaigning ;-)"
    date: 2004-10-24
    body: "That should scare off a lot of potential users then :-) (unless they are compile freaks of course and yes it's the good old Debian<->Gentoo debate).\nAnyway what's more interesting is where you don't comment on, the possibility proprietary niche software will come to our favorite platform.\n(BTW s/Siemons/Siemens/)"
    author: "koos"
  - subject: "Re: Anton Velev is back campaigning ;-)"
    date: 2004-10-25
    body: "Why you are mentioning Siemens so often? Are you related with them in some way?\n\nAbout the migration to UNIX of user that use proprietary apps, there are several options:\n(the order is applicable to soon you can get them running)\n1) short term option for most of the commercial apps - buy a Mac\n2) WineX & CrossOver for apps that don't work with Wine (although this would be option only for Linux AFAIK)\n3) Wine for apps that work with Wine (yes - eventually WineCC for future)\n4) design a new learning curve that avoids apps not available for UNIX\n- apply this new learning curve for all new users\n- start training old users\n5) don't write apps that are bound to MFC, WinForms, Avalon or other libs that chain you to certain platform or toolkit that is not BSD/LGPL. This in fact is a longer-term strategy if you already have some apps to rewrite. Following design of the apps can be followed when designing new (or refactoring old) apps (assuming you are using C++):\n- split the GUI from the app business logic\n- use STL in the app business logic or other _standart_ collection/structure frameworks and facilities that must be for sure BSD or LGPL, and don't use any CString, GObject, QObject or other toolkit related objects that bind your app business logic to certain platform or toolkit\n- make a thin interface for interaction between the app and GUI\n- make a thin GUI layer that implements your interface but uses the platform-dependent GUI - this will free you to switch for future to other toolkits and platforms\n- (optional) be sure GUI toolkit is not GPL or not Royality free. This would not apply if you use the app in house only, but however for long-term strategy you would not be willing to spend double time on maintaining more than one toolkits so better choosing a Royality free toolkit from the very beginning.\n- if you have web-apps make them browser independent, also if they use properitary server platform switch them to Java, PHP, perl, zope/python etc.\n- if some of the GUI apps do not require to run on desktop you can make them web-apps too\n\nTried to cover wide range of cases, for everything I can guess right now this would work, and will really give you in long term a solid and permanent migration to UNIX, while at the same time staying as free from GPL or Royality not-free platforms, toolkits or frameworks."
    author: "Anton Velev"
  - subject: "Re: Anton Velev is back campaigning ;-)"
    date: 2004-10-25
    body: "No not related, just tied to their software. As you may know, they more or less dominate the industrial control software in Europe. No way, their apps will run with Wine-whatever.\nYou talk about writing new software and only use standards or LGPL/BSD stuff. Although I think you say, don't write anything for KDE, it's not the point what I was referring to. That was the existing stuff.\nSure I can switch to an UNIX CAD program, there might be SCADA systems for Linux too (VISpro). But I can't convince the 'industry' to switch, these people are conservative (for good reasons of course). Which means we must keep NT/W2k systems up and running. And it's not Linux <-> NT, because using Oracle/DB2 on Linux is okay. But don't try to sell MySQL/ProgreSQL. My colleagues want to work with Linux, but if Step7 doesn't run, that is mandated by our customers, it's end of story."
    author: "koos"
  - subject: "I was playing around with GTK-WebCore"
    date: 2004-10-21
    body: "I've just hacked GTK-WebCore into Atlantis only to play around with it. GTK-WebCore looks quite promising but still needs work.\n\nhttp://www.akcaagac.com/atlantis.png\n\nFor more information about Atlantis.\n\nhttp://www.akcaagac.com/index_atlantis.html"
    author: "Ali Akcaagac"
  - subject: "Re: I was playing around with GTK-WebCore"
    date: 2004-10-21
    body: "> For more information about Atlantis.\n\nYour advertisement here is uninteresting unless you offer the GTK-Webcore version for download."
    author: "Anonymous"
  - subject: "Re: I was playing around with GTK-WebCore"
    date: 2004-10-21
    body: "Why don't you look at the topic of this Article, links and everything is provided. Simply go there as I went there and download it. I also don't think that my advertisment is uninteresting. The entire thread is for GTK-Webcore (KHTML) and I just demonstrated how easy it is to adopt it for own productions."
    author: "Ali Akcaagac"
  - subject: "Re: I was playing around with GTK-WebCore"
    date: 2004-10-21
    body: "Where does the article link the GTK-Webcore version of Atlantis?"
    author: "Anonymous"
  - subject: "Re: I was playing around with GTK-WebCore"
    date: 2004-10-21
    body: "> only to play around with it.\n\nDifficult to understand?"
    author: "CE"
  - subject: "Re: I was playing around with GTK-WebCore"
    date: 2004-10-21
    body: "And how's GoneMe comoing along right now?"
    author: "Maynard"
  - subject: "Re: I was playing around with GTK-WebCore"
    date: 2004-10-23
    body: "I guess by now people know what a "
    author: "lowell"
  - subject: "Atlantis 0.1.3 - with GTK-WebCore support released"
    date: 2004-10-22
    body: "http://www.akcaagac.com/index_atlantis.html"
    author: "Ali Akcaagac"
  - subject: "Re: Atlantis 0.1.3 - with GTK-WebCore support released"
    date: 2004-10-24
    body: "What strange way of packaging and releasing a binary and not source code."
    author: "Anonymous"
  - subject: "Galeon with GTK+ Webcore in the works"
    date: 2004-10-24
    body: "http://sourceforge.net/mailarchive/forum.php?thread_id=5818656&forum_id=6200"
    author: "Anonymous"
  - subject: "Why is the GTK port of KHTML much faster?"
    date: 2004-10-24
    body: "I've just tested it for looking how it works. It is quiet very unstable but I've noticed that it is much faster than Konqueror itself. I'm using SuSE 9.1 with KDE 3.2.1 and Konqueror is slower than Mozilla. Due to this I've switched to Opera, IMHO currently the best browser on the market."
    author: "Hans Kottmann"
  - subject: "Re: Why is the GTK port of KHTML much faster?"
    date: 2004-10-25
    body: "There is no way Konqueror is slower than Mozilla because nothing is slower than Mozilla.  It's just not possible.  Anyway, Konqueror is super fast here."
    author: "ac"
  - subject: "Re: Why is the GTK port of KHTML much faster?"
    date: 2004-10-25
    body: "Wich version of KDE and which distribution are you using? "
    author: "Hans Kottmann"
  - subject: "abiword"
    date: 2004-10-25
    body: "Well a KDe port of abiword would be nice..."
    author: "Hein"
  - subject: "Re: abiword"
    date: 2004-10-27
    body: "truly."
    author: "Sebastian"
  - subject: "Merge the basis of KDE and GNOME!!!"
    date: 2004-10-27
    body: "Desktop fragmentation is perhaps the biggest obstacle to the success of the linux desktop. \n\nA good desktop requires all applications to share the same infrastructure. For instance the same VFS (A KIO wrapper on top of GNOME-VFS for instance), the same component architure for scripting and so on...\n\nTo merge KDE and GNOME at the basis would multiply the value of all the excellent and hard work already done. Isn't that reason enough to try for it? \n"
    author: "xyz"
  - subject: "Re: Merge KDE and GNOME!!!"
    date: 2004-12-02
    body: "Desktop fragmentation is a huge obstacle. A merger of KDE and Gnome technologies would move the desktop on Linux well beyond where it is today."
    author: "Crusty Crab Cake"
  - subject: "Re: Merge KDE and GNOME!!!"
    date: 2004-12-02
    body: "Go merge Microsoft and Apple, then I'll do the KDE/GNOME part.\n\nI wonder if people actually believe they are being original with this :-P"
    author: "Roberto Alsina"
---
Hot on the heals of the <a href="http://dot.kde.org/1094924433/">port of Gecko to Qt</a> comes a pre-release of a port of KHTML to GTK+.  Released components include KJS JavaScript interpreter, KHTML rendering engine, Qt porting layer, WebKit API for embedding and a reference browser for demonstrating the functionality of the other components.  <a href="http://gtk-webcore.sourceforge.net/">Their website</a> explains that this was done by Nokia Research Center.  They hope to collaborate with another project also porting KHTML to GTK, <a href="http://cvs.gnome.org/viewcvs/gnome-webkit/">Gnome Webkit</a>.  <a href="http://gnomedesktop.org/article.php?sid=1996">Footnotes has the story</a>.



<!--break-->
