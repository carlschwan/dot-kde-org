---
title: "KDE-CVS-Digest for March 26, 2004"
date:    2004-03-27
authors:
  - "dkite"
slug:    kde-cvs-digest-march-26-2004
comments:
  - subject: "Thanks Derek"
    date: 2004-03-27
    body: "Does anyone know what the projected timeframe for KDE 3.2.2 is? I am in the process of upgrading to KDE 3.2.1 right now after being on the RCs for a few months because I couldn't wait any longer."
    author: "Turd Ferguson"
  - subject: "Re: Thanks Derek"
    date: 2004-03-27
    body: "http://lists.kde.org/?l=kde-core-devel&m=108029566821346&w=2"
    author: "Anonymous"
  - subject: "Mystery"
    date: 2004-03-27
    body: "What is British English? Does this mean that the British speak English with an\nEnglish accent?\nSo many questions... "
    author: "reihal"
  - subject: "Re: Mystery"
    date: 2004-03-27
    body: "Different spelling of words. Labour vs. labor, colour vs color, etc. Many other examples that I can't think of.\n\nThere are other differences such as how dates and times are displayed.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Mystery"
    date: 2004-03-27
    body: "Read this for a starter:\nhttp://esl.about.com/library/weekly/aa110698.htm"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Mystery"
    date: 2004-03-27
    body: "Oh dear, and then Yoda English there is."
    author: "reihal"
  - subject: "Re: Mystery"
    date: 2004-03-27
    body: "Why, British English is the only true English there is of course.\n\n(tongue firmly in cheek :)"
    author: "Paul Eggleton"
  - subject: "Re: Mystery"
    date: 2004-03-27
    body: "> Why, British English is the only true English there is of course.\n\nfull ACK\n"
    author: "ac"
  - subject: "Re: Mystery"
    date: 2004-03-27
    body: "> What is British English?\n\nAs others have noted, there are differences in spelling with British English.  What confuses me though, is why \"English English\" isn't the default 'en', and there isn't a translation for en_US, rather than the other way around.  It's not like the main KDE developers are all from the USA or anything.\n"
    author: "Jim Dabell"
  - subject: "Re: Mystery"
    date: 2004-03-27
    body: "en_US is pretty much a computing standard.. en_GB isn't"
    author: "AC"
  - subject: "Re: Mystery"
    date: 2004-03-27
    body: "I'm not quite sure what you mean.  They are both codes defined in RFC 1766, so they are both standard.  They use the same alphabet, so there are no differences there.  The difference is in spelling.  I fail to see why a regional dialect (en_US) should take precedence over the normal language, especially as the English spelling is more widespread (Australia, Canada, etc) than the USA spelling."
    author: "Jim Dabell"
  - subject: "Re: Mystery"
    date: 2004-03-27
    body: "Sorry, I meant _the_ standard, rather than \"a\" standard. en_US is _the_ standard for every major environment that is marketed throughout the world, while en_GB/en_CA/en_AU aren't. \n\nThis is to a point where people not in former British countries have learned the American spellings more than the British spellings. Thus, en_US is the normal English in the computing world, while en_GB isn't. \n\nYou can probably blame this fact from the dominance of American companies in the computing sector, but whatever; the fact is that it's more pragmatic today to use en_US. "
    author: "anon"
  - subject: "Re: Mystery"
    date: 2004-03-27
    body: "In the EU, all schools teaching English are meant to use British spellings and words ( same goes for Spanish - we learn Spanish Spanish ;-) ). But often it seems to really depend on the teacher - if they are from the US, you will learn \"Lets go eat\" and \"one hundred ten\" as valid...  The only place I've ever known where the state schools teach US English (IE the text books are in it) is Israel. What a shock."
    author: "rjw"
  - subject: "Re: Mystery"
    date: 2004-03-27
    body: "It's quite irrelevent what they teach in EU schools, since the computing and internet world is pretty much standardized around American english.\n\nhttp://tinyurl.com/2w8my\n\n"
    author: "fault"
  - subject: "Re: Mystery"
    date: 2004-03-29
    body: "That is nonsense. The computing and internet world certainly are not standardised on US English. The people who write using real English continue to happily use it on the net, and the same goes for US English. Since the US is overrepresented on the net in relation to the rest of the world, it should come as no surprise that \"color\" is more common. But saying that US English is some kind of online standard is a bit of a stretch.\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Re: Mystery"
    date: 2004-03-27
    body: "At least in Germany, while British spelling is teached in school, they cannot mark you an error if you use the American spelling of a word.  It won't influence the grade you get.  (Unless you have a stupid fanatical teacher.)"
    author: "he-sk"
  - subject: "Re: Mystery"
    date: 2005-04-12
    body: "Whilst in Germany,where English spelling is taught,American spelling is acceptable. True English grammar rules.OK!!! . There is only one English language.Theater,color etc. are spelling mistakes."
    author: "joe"
  - subject: "Re: Mystery"
    date: 2004-03-27
    body: "Being an American, perhaps I can answer your question more thoroughly.\n\nWhile British English may come across as more 'proper' due to you being from the European Union, do recall that companies such as NeXT, Apple, Microsoft, IBM, Novell, Compaq, Hewlett Packard, Texas Instruments, Motorola, etc as someone else has already pointed out are US Corporations.  And since the majority of Operating Systems have originated within such corporations it does stand to reason the Localization of the OS would default to US English.\n\nBeing an engineer by training I'm more puzzled that I had to deal with non-metric units of conversion, throughout the US, than I am with the fact that there are derivations of English.  Afterall, is not English truly a 'melting pot' of other languages?  Slugs versus Newtons?  I mean come on.  British Thermal Units?  Sounds a bit arrogant really.  I'd love the US to use Metric everywhere but you go and pay for all the road signs that have to be replaced.  Automobile manufacturers, at least have included MPH and KPH on the speedometers.  How many square miles is the U.K. again?\n\nI'm sure if all those companies were founded within the U.K. we'd all be writing colour instead of color.\n\nRegarding the teacher who spoke one hundred ten it is actually one hundred and ten.  I'm sure the person after writing so many checks has become accustomed to dropping the 'and' in her One hundred ten and no/100 throughout their adult life. Let's go eat is still written.  It's just that most people are pathetic at grammar, including myself which is no excuse."
    author: "Marc J. Driftmeyer"
  - subject: "Re: Mystery"
    date: 2004-03-27
    body: "> And since the majority of Operating Systems have originated within such corporations...\n\nLinux has its origins in Finland, though.\n\n> ...is not English truly a 'melting pot' of other languages?\n\nDefinitely, It's neither purely germanian nor roman. The english people imported a lot of words through their voyages overseas. And they exported the english language buy conquest, deporting criminals etc. The same as the spanish did. Those hooligans didn't really care very much for grammar."
    author: "OI"
  - subject: "Re: Mystery"
    date: 2004-03-28
    body: "Actually the mishmash that is English is mostly because we got conquered by most of Europe at one time or another. We didn't \"import\" as we went abroad. My general impression of our behaviour as we moved about is that we should be spanked.\n\nHowever it is true that we \"exported English\" as we we expanded the empire. Fortunate for me, a twenty-something English programmer. However, I do feel guilty about my country's past behaviour."
    author: "Max Howell"
  - subject: "Re: Mystery"
    date: 2004-03-28
    body: "> I do feel guilty about my country's past behaviour\nBlame the king."
    author: "Axel"
  - subject: "Re: Mystery"
    date: 2004-03-28
    body: "\"...This is to a point where people not in former British countries have learned the American spellings more than the British spellings. Thus, en_US is the normal English in the computing world, while en_GB isn't...\"\n\nWhat? I've went to English schools/classes in the following countries, and we used England's English:\n\nCanada, Italy, France, and Swiss.\n\nI also know people that are (or have been to) in Australia, England, and Germany, and learned proper English -- England's English.\n\nPS: I live in Canada, and the 'Canadian - English' dictionary in Office XP/2000 sucks the bag!\n\nNote to Microsoft: Can you please have someone that lives in Canada help develop your Canadian - Enlish dictionary? Currently I've had to use the 'American - English' dictionary, and add all the proper 'English' spellings!\n\nMy $0.02 - Canadian or $0.01 American"
    author: "thesimplefix"
  - subject: "Re: Mystery"
    date: 2004-03-28
    body: "\"What? I've went to English schools/classes in the following countries, and we used England's English:\"\n\nDid you learn to read yet? School != Computing world"
    author: "AC"
  - subject: "Re: Mystery"
    date: 2004-03-27
    body: "As a Brit, I have to say that en_US is far more commonly spelt and even pronounced than en_GB. I'm not bothered by this, I'm just eternally grateful that I don't have to learn another language in order to develop free software.\n\nBut anyway, being a more common version of English, it is correct it should be the default English for projects such as KDE."
    author: "Max Howell"
  - subject: "Re: Mystery"
    date: 2004-03-27
    body: "There should be en_US and en_GB.  Perhaps even en_US_SOUTHERN or some such if anyone really wants to go that far.   Then let the developers use whatever makes sense to them, and use translators to force everything to a nice language. "
    author: "bluGill"
  - subject: "Re: Mystery"
    date: 2004-03-28
    body: "heh... Maybe en_US_TEXAN?\n\nIt could support \"y'all\", \"all y'all\" and \"what all\".\n\nI'm in the UK, and I have spent plenty of time in Texas, but still, it is frightening to find a book reprinted in the UK with the immortal phrase: \"What all is this person trying to say?\".\n\nTo say that it grated with me is an understatement :)"
    author: "Dawnrider"
  - subject: "Re: Mystery"
    date: 2004-03-28
    body: "Well, the various American dialects are for the most part not expressed in written form. And even if you did write it down, most of the stuff in the GUI to be translated wouldn't actually be any different since its more technical. There would have to be an Academy of Sourthern English that could make up new words for technial things like the French do. So yea, the idea would be a little silly.\n \nAs a Missourian, I rather like using you all. \"Standard\" English lacks a plural second person. I always thought it was funny in the Spanish textbooks that they translated Uds. as 'you plural', of course our teachers explained it simply as Spanish for you all."
    author: "Ian Monroe"
  - subject: "Re: Mystery"
    date: 2004-03-28
    body: "Some other strange things I have seen; \"most all\" and \"ain't\". I have still not decoded them."
    author: "Erik"
  - subject: "Re: Mystery"
    date: 2006-02-28
    body: "Not really mysterious, but somehting that I should be able to get around.\n\nApple,Dell,MS,HP etc probably don't care if I prefer British English to American, obviously neither do most of the people who just read that.\n\nSo where is there a en_gb language file that I can add in order to exert MY preference.  I would never force a American to speak english in a particular way, I just want to be able to speak my language in my way.\n\nJ"
    author: "j"
  - subject: "Re: Mystery"
    date: 2006-02-28
    body: "The en_GB translation is a standard KDE translation, so you should find it where you have found KDE.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "OT: kde-look and kde-apps mess"
    date: 2004-03-27
    body: "It seems that many developers cross-announce stuff at kde-look.org and at\nkde-apps.org\n\nI find this rather confusing. So either merge both sites into one, or restrict\nkde-look to \"look\"-specific stuff and kde-apps to applications.\n"
    author: "ac"
  - subject: "Re: OT: kde-look and kde-apps mess"
    date: 2004-03-27
    body: "kde-look and kde-apps are partially merged.. categories which can or do fit into both the look site and the apps site are shared - e.g KDE Improvments and Screensavers"
    author: "je4d"
  - subject: "Thanks for type-ahead-find"
    date: 2004-03-27
    body: "With KDE 3.2 I finally switched from the cvs version of KDE to the release version, and now you implement the great type-ahead-find, so have to go cvs again - that's just not fair ;)"
    author: "MaxAuthority"
  - subject: "US_en "
    date: 2004-03-27
    body: "Is it possible to detect the language of an Email or text? Where can I find a library for that?\n\n---\n\nAutomatice translation as good old systran (altavista babelfish, google translations) somehow works but I wonder whether it could be possible to create an open source fraemwork for automatic translations that is improved by its users.\n\nAre there OSS Projects on automatic tranlation?"
    author: "Gerd Br\u00fcckner"
  - subject: "Re: US_en "
    date: 2004-03-28
    body: "There is a system called TextCat which is licenced under the GPL and can be found at http://odur.let.rug.nl/~vannoord/TextCat/\n\nRegarding the machine translation: That's a _huge_ field in natural language processing. One of the main problems are centred around Word Sense Disambiguation (WSD) which treats the problem of having words with several meanings which are all translated differently. You might find more on www.senseval.org\nI don't know of any open sources project dealing with machine translation but you might have a look at freshmeat.net. Some universities in the USA also have some tools published on their webpage. Particularly www.cmu.edu is a great resource, AFAIK."
    author: "Jan Drugowitsch"
  - subject: "Re: US_en "
    date: 2004-03-28
    body: "and OpenCyc? Is this useful?"
    author: "Bert"
  - subject: "Re: US_en "
    date: 2004-03-28
    body: "don't know. opencyc is more like a massive knowledge base which tries to capture the world's knowledge in a coherent and consistent ontology. i don't know if it's ever been used for machine translation (AFAIK it only exists in english). but i'm not an expert in this field."
    author: "Jan Drugowitsch"
  - subject: "Re: US_en "
    date: 2004-04-01
    body: "I know about some project in EU for multilingual automatic translation \nnamed DLT ( distributed Lenguage translation ) \n\nbut i don't have found enough data by now.\n\nSorry "
    author: "Martin Ponce"
  - subject: "Re: US_en "
    date: 2004-04-01
    body: "well ... yeah, U can see some of stuff at \n\nhttp://www.langmaker.com/db/mdl_esperantodedlt.htm\n\nand as i knowed, this project was based on tree structures \nfor translations and have some context. \n\nAnd it just died before the penguin era. = (\n\nSaludos amigos ! "
    author: "Martin Ponce"
  - subject: "Re: US_en "
    date: 2004-04-01
    body: "well ... yeah, U can see some of stuff at \n\nhttp://www.langmaker.com/db/mdl_esperantodedlt.htm\n\nand as i knowed, this project was based on tree structures \nfor translations and have some context. \n\nAnd it just died before the penguin era. = (\n\nSaludos amigos ! "
    author: "Martin Ponce"
  - subject: "Reply Field Width"
    date: 2004-03-27
    body: "Is there a reason the replied posts Headers and contents are wider than the Summary context? (i.e., the browser sure is eating up a lot of screen real estate just to get rid of the horizontal scroll bar."
    author: "Marc J. Driftmeyer"
  - subject: "Re: Reply Field Width"
    date: 2004-03-28
    body: "The reason is that people are posting very long URLs in comments instead using tinyurl.com."
    author: "Anonymous"
  - subject: "Re: Reply Field Width"
    date: 2004-03-28
    body: "If the dot admins ever decided to get off their behinds and fix html posting, this wouldn't be a problem any more."
    author: "fna"
  - subject: "Re: Reply Field Width"
    date: 2004-03-28
    body: "HTML posting was removed when it had been abused. I'd rather like to see URLs being turned into links automatically and shortened to the base URL or file name in the progress."
    author: "Datschge"
  - subject: "Help Me With The Definition Of The Following."
    date: 2006-10-18
    body: "Explain the following expression used in creating a database.\n\nDATABASE STRUCTURE\nFIELD NAME\nFIELD TYPE\nFIELD WIDTH\nNUMERIC FIELD\nDATE FIELD\nLOGICAL FIELD\nCHARACTER FIELD\nFORM FIELD\nQUERY\nMEMO\nTABLE\nLABEL"
    author: "FISHBONE"
  - subject: "Speaking of Amarok"
    date: 2004-03-28
    body: "Why can't it just play using NO sound system whatsoever ? For my audio stuff I can barely use any applications coming out of KDE, because all of these use arts. The latter gives me terrible performance, bad latency, crashes, high CPU usage - really a shame. I don't understand, I have an SB Live with hardware mixing, I don't need software to try and do this for me as well. Is there a way to make this work properly ? I'm definitely not the only one complaining about this, I can easily think of 3 or 4 of my friends who feel the same (and still use XMMS for this and other reasons)."
    author: "Jelmer Feenstra"
  - subject: "Re: Speaking of Amarok"
    date: 2004-03-28
    body: "Because not all KDE app wants to implement mp3 decoding"
    author: "anon"
  - subject: "Re: Speaking of Amarok"
    date: 2004-03-28
    body: "That, I understand. But why won't artsd just do that and leave other sound related stuff to my SB Live! ?"
    author: "Jelmer Feenstra"
  - subject: "Re: Speaking of Amarok"
    date: 2004-03-28
    body: "Use the GStreamer engine instead. GST can output directly to OSS or ALSA, without the need for a deamon. It's also faster than aRts in many areas.\n\nWhile GST support is still pretty experimental in amaroK 0.9, in CVS it's already working very nicely (gstreamer-0.8 required). Hopefully I'll get the KIO-source plugin for GST done before amaroK 1.0, which will make it possible to play media from any protocol that KIO supports, including http streams, which is currently not working with GStreamer.\n"
    author: "Mark Kretschmann"
  - subject: "hi derek."
    date: 2004-03-29
    body: "You missed the fixing of this wishlist feature (not surprising it happened on friday).\n\nhttp://bugs.kde.org/show_bug.cgi?id=26021\n\nCould you include it next week? It's absolutely fantastic news and testament to how every KDE bug gets fixed in the end.\n\na very very happy kmail user."
    author: "c"
---
In <a href="http://members.shaw.ca/dkite/mar262004.html">this week's KDE CVS-Digest</a>:
<A href="http://quanta.sourceforge.net/">Quanta</A> goes KMDI. 
<A href="http://kmail.kde.org/">KMail</A>'s IMAP support is optimized. 
<A href="http://www.konqueror.org/">Konqueror</A> gains type-ahead find. 
<A href="http://amarok.sourceforge.net/">amaroK</A> has a new visualization scheme. 
Start of KDOM ECMA support. Continued work on certificate handling
in KMail. And the usual bugfixes.
<!--break-->
