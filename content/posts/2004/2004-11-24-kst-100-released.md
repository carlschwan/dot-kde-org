---
title: "Kst 1.0.0 Released"
date:    2004-11-24
authors:
  - "gstaikos"
slug:    kst-100-released
comments:
  - subject: "Cool"
    date: 2004-11-24
    body: "So can we slap this on a machine with appropriate hardware and make our own open source oscilloscope? :)\n\n"
    author: "JohnFlux"
  - subject: "Re: Cool"
    date: 2004-11-24
    body: "Yes.\nRoll mode works 'out of the box'.  Trigger modes can be done via plugins.\n\nTypical use 1) is data is being appended to a file by the experiment at 100Hz/channel, and we plot the time-streams, power spectra, etc over past ???, where ??? is from a couple seconds to a few days.\n\nTypical use (2) is data is sitting in a file on disk and we want to look at it.\n\n"
    author: "Barth Netterfield"
  - subject: "Screenshots"
    date: 2004-11-24
    body: "Screenshots?"
    author: "Benjamin Meyer"
  - subject: "Re: Screenshots"
    date: 2004-11-24
    body: "http://omega.astro.utoronto.ca/kst/kstplot.png\n"
    author: "mdiig"
  - subject: "Howto..."
    date: 2004-11-25
    body: "I like kst cause it just works and for me its easer to work than using a spreadsheat or (k)octave when analysing large samples.\nBut I miss some howto stuff:\n\nHow do I create vectors like b(n)=a(n+1)-a(n) without writing my own plugin?\n\nThanks for kst!\n\nThorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: Howto..."
    date: 2004-11-25
    body: "Well, a Derivative plugin is somewhere in our todo list... but if you want to write one... :-)\n"
    author: "Barth Netterfield"
  - subject: "Re: Howto..."
    date: 2004-11-25
    body: "I've also considered the idea of doing this with indexing inside equations.  We need to figure out the syntax, but it should be easy once that's done."
    author: "George Staikos"
  - subject: "Nice app"
    date: 2004-11-25
    body: "Seems like quite a nice application, watching streams of data can be quite usefull. I'm using SUSE 8.2, I tried the FC1 rpm and it installed fine but I cannot load any data. I did try the examples/files in the online manual:\n\nprompt> kst -y 1 gyrodata.dat\n(...)\nkst: ERROR: No data in file: gyrodata.dat\n\nAm I doing something wrong or should I just install the src.rpm (I didn't do it because I'm missing way to many devel libs).\n\n\n\n"
    author: "ja"
  - subject: "Re: Nice app"
    date: 2004-11-26
    body: "(assuming gyrodata.dat is in your current directory...) I would guess that kst can't find the data source plugins (seeing as you are using an fc1 rpm on suse, and the directory structure may be a little different...).  \n\nPerhaps try removing the rpm, and then installing from the tarball.  Instructions (trivial) and the tarball are at omega.astro.utoronto.ca/kst\n\ncbn\n"
    author: "Barth Netterfield"
  - subject: "Compilation problems"
    date: 2004-11-29
    body: "Running make, I get:\n\n/opt/kde3/bin/meinproc --check --cache index.cache.bz2 ./index.docbook\nindex.docbook:53: error: Entity 'erik.kjaer.pedersen.role' not defined\n&erik.kjaer.pedersen.role;\n                          ^\nmake[3]: *** [index.cache.bz2] Error 1\n\nand that's it. Any ideas?\n"
    author: "AC"
  - subject: "Re: Compilation problems"
    date: 2004-11-29
    body: "In which KDE version is it missing? (Not KDE CVS HEAD, I suppose.)\n\nI suppose that you will have to contact the Danish team.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Compilation problems"
    date: 2004-11-30
    body: "The documentation translations are apparently/unfortunately not compatible with kde 3.1's meinproc.\nYou can\n-make inside kst-1.0.0/kst, which will not produce any docs, but will work.\n-make -i in kst-1.0.0 to ignore errors, or\n-upgrade to a new kde.\n\nsigh..."
    author: "Barth Netterfield"
---
The <a href="http://omega.astro.utoronto.ca/kst/">Kst development team</a> is pleased to <a href="http://www.staikos.net/news/getarticle.php?id=200411240001">announce the immediate availability</a> of Kst 1.0.0, the first mature release of the advanced data viewing application for KDE. In addition to data viewing and plotting, Kst features real-time support for streaming data, and also includes basic analysis functionality. Kst is presently used by a variety of scientific and industry projects worldwide.




<!--break-->
