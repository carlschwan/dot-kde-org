---
title: "HP Delivers KDE on Laptops"
date:    2004-09-09
authors:
  - "ateam"
slug:    hp-delivers-kde-laptops
comments:
  - subject: "DVD support?"
    date: 2004-09-09
    body: "I see that there is a screenshot of Finding Nemo on DVD.  Does that mean that LinDVD (the stated player) is packaged as a working DVD player?  By default SUSE has disabled DVD playback on xine and MPlayer for legal reasons (on easy download of MPlayer \"fixes\" this).  Is legal playback of DVDs available 'out of the box' for these laptops?"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: DVD support?"
    date: 2004-09-09
    body: "FWIW, at least the Notebook that I got at aKademy has LinDVD installed."
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: DVD support?"
    date: 2004-09-09
    body: "And how does it compare to Ogle/Xine/Mplayer?\n\nDoes it feel like a native linux application or more like a wine-based quick-and-dirty port?\n\nBtw., MPlayer does not support DVD-menus but at least it is able to play everything - even /dev/random :\n\nmplayer -rawvideo on:w=320:h=200 /dev/urandom"
    author: "Asdex"
  - subject: "Re: DVD support?"
    date: 2004-09-09
    body: "Even encrypted DVDs?  How?  I play everything like DivX no problem even when Windows folks have to go around chasing the latest codec, it still seems to work on MPlayer.  But DVDs, how?  It doesn't usually work for encrypted ones."
    author: "KDE User"
  - subject: "Re: DVD support?"
    date: 2004-09-09
    body: "I'm not sure about the HP, but with Linspire you can purchase a DVD player for $4.95 if you're a Click-N-Run member.\n\nBasically it is Xine with a licensed decryption module.  It worked flawlessly for me, and for $4.95 and a quick download, it was a bargain.  Even WinXP doesn't play encrypted DVDs out of the box."
    author: "Charles Hill"
  - subject: "Re: DVD support?"
    date: 2004-09-09
    body: "Just install DeCCS and it works. At least, I have not yet seen a DVD that does not work in my machine."
    author: "Andre Somers"
  - subject: "Re: DVD support?"
    date: 2007-02-08
    body: "Hi,\n\nI read you ( http://dot.kde.org/1094715499/1094717854/ ) and I'm looking for DeCCS software, if you have this one could you be so kind to send me it ? I'ts getting more and more difficult to find it ...  :-)\n\nMany thanks in advance,\n\nBest regards,\nBT"
    author: "Barnard"
  - subject: "Re: DVD support?"
    date: 2004-09-10
    body: "I don't know why, but MPlayer supports DVD menues for me, as does xine-lib IIRC...\n\nI usually use the MPlayer backend for KMPlayer, and MPlayer is compiled with libmpdvdkit for DVD support."
    author: "Willie Sippel"
  - subject: "Re: DVD support?"
    date: 2004-09-10
    body: ">Is legal playback of DVDs available 'out of the box' for these laptops?\n\nYes. LinDVD ist part of the pre-install."
    author: "Thomas Schneller"
  - subject: "Re: DVD support?"
    date: 2004-09-10
    body: "I'm assuming LinDVD is a commercial product, and is licensed to read CSS encrypted DVDs.  Suse's desktop Linux must include this by default.\n\nYou won't see anything like that in the downloadable Mandrake or Fedora, since it relies on non-OSS software."
    author: "anon"
  - subject: "HP recommends XP"
    date: 2004-09-09
    body: "I just followed the link to the HP website given in the article:\n\n-Configurable- HP Compaq Business . . . . . HP recommends Microsoft\u00ae\nNotebook nx5000 with Linux . . . . . . . . .Windows\u00ae XP Professional\n\nThat's a bit odd, don't you think?\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: HP recommends XP"
    date: 2004-09-09
    body: "Every computer manufacturer includes that exact same text in their ads.  They probably get a humongous discount on their Microsoft licensing if they include this meaningless marketing drivel.  And of course Microsoft makes them put it on every page, even if they're selling Linux (probably *especially* if they're selling Linux).  When you're a monopoly you can dictate your licensing terms however you like..."
    author: "Spy Hunter"
  - subject: "Re: HP recommends XP"
    date: 2004-09-09
    body: "Michael Robertson from Linspire wrote an interesting (and a bit depressing) paper about it\n\n<a href=\"http://www.linspire.com/lindows_michaelsminutes.php\">http://www.linspire.com/lindows_michaelsminutes.php</a>\n"
    author: "Charles de Miramon"
  - subject: "Re: HP recommends XP"
    date: 2004-09-09
    body: "Future proof link when the next \"Michael's Minute\" gets posted: http://tinyurl.com/4ntuu"
    author: "Datschge"
  - subject: "Re: HP recommends XP"
    date: 2004-09-09
    body: "\"When you're a monopoly you can dictate your licensing terms however you like...\"\n\nActually anyone can dicate licensing terms. \"Take it or leave it\" contract terms are very routine. When you have a product all the OEMs want and the all the OEMs are on extremely thin margins, then you're very likely to get them to agree to your terms."
    author: "David"
  - subject: "Sure but "
    date: 2004-09-10
    body: "If you don't have a monopoly, your prospects can just go to the other provider. If you have a monopoly, the can't and therefore they oblige. "
    author: "Olivier Magere"
  - subject: "Re: HP recommends XP"
    date: 2004-09-09
    body: "> That's a bit odd, don't you think?\n\nNot really. It's part of Microsoft's OEM program. If you're HP, it actually makes the Windows Tax more bearable. Not odd at all. "
    author: "anon"
  - subject: "Re: HP recommends XP"
    date: 2004-09-10
    body: "Who needs enemies with support like that ?"
    author: "Olivier Magere"
  - subject: "Distribution in Europe..."
    date: 2004-09-09
    body: "You are mentioning that the localization work for Europe was too much for HP. Did you consider selling the nx500 with a blank hard disk to local partners like Mandrake Linux in France and leave to them the work of installing their tailored Linux Distribution and then distribute it through their online store and maybe HP online store ?\n\nAll in all, it is a great move. The Gnu/Linux/KDE market is still quite small and fragmented but I believe that if HP offers the possibility to sell be OS agnostic desktop systems, it can help it regain some market and differentiate itself from Dell. In French Research institutions and Universities, Dell has  lately won most of the procurement contracts. But these institutions are slowly moving to Linux and HP move could certainly help the company regain some market share."
    author: "Charles de Miramon"
  - subject: "Re: Distribution in Europe..."
    date: 2004-09-10
    body: "I second that. Mandrake is the way to go. RedHat/Fedora is too focused on Gnome, which in my opinion falls short on the desktop. SUSE have choosen KDE, but are too focused on enterprise, which is hurting their desktop profile. Mandrake on the other hand has always had a strong desktop profile, and has been the best choice for desktop Linux for years."
    author: "Sjaddow"
  - subject: "Re: Distribution in Europe..."
    date: 2004-09-10
    body: "And Mandrake has a sad history of breaking KMail by backporting unfinished features from the development version to the stable version (on-the-fly spellchecking was backported and we received crash reports, the anti-spam wizard was backported and we received crash reports). And then Mandrake thought it would be a cool idea to use ~/.Mail instead of ~/Mail for storing the mail. Well, there's nothing wrong with that if it's done right. But guess what, we have received multiple bug reports about this because apparently Mandrake didn't manage to do it right.\n\nThe result of this is that I don't trust any bug reports coming from Mandrake users until they have been confirmed by non-Mandrake users. I'm sorry about that, but I really don't have the time to investigate whether such a bug is Mandrake-only or not.\n\nI can't remember any other distribution causing us, the KMail developers, comparably much pain."
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Distribution in Europe..."
    date: 2004-09-10
    body: "On the other hand, they put a lot of effort into integrating Kolab. I believe Mandrake mistakes are past."
    author: "Carlos Woelz"
  - subject: "Re: Distribution in Europe..."
    date: 2004-09-11
    body: ">I can't remember any other distribution causing us, the KMail developers, >comparably much pain.\n\nIt's probably also because Mandrake is the most downloaded distribution and so probably the most used on desktops, almost doubling the next non-live distribution (Fedora):\n\nhttp://distrowatch.com/stats.php?section=popularity\n\n"
    author: "Juanjo"
  - subject: "Re: Distribution in Europe..."
    date: 2004-09-10
    body: "In fact all I see as a result of that is better and simpler support for some tasks in suse. That includes instalation of Software (nvidias drivers for instance, check the specific SUSE instructions on their site), profile management, etc.\n\nWhat exactly were you thinking about? How do you think it hurting their desktop profile? Could you give examples? "
    author: "John SUSE Freak"
  - subject: "Re: Distribution in Europe..."
    date: 2004-09-11
    body: "Mandrake (powerpack, which is the distribution that would surely go in a laptop) also have nvidia drivers and install them by default if your computer needs them (so no  instructions are needed.) It also have profile management on drakconf (and I use it a lot.)"
    author: "Juanjo"
  - subject: "Re: Distribution in Europe..."
    date: 2004-09-10
    body: ">Did you consider selling the nx5000 with a blank hard disk to local partners like Mandrake Linux in France and leave to them the work of installing their tailored Linux Distribution and then distribute it through their online store and maybe HP online store?\n\nDistributors are invited to contact us any time if they want to develop an own pre-installation. For cooperate customers we have a version available with freedos pre-installed. For legal reasons we can not ship units without OS. So this is our solution to address this kind of requests."
    author: "Thomas Schneller"
  - subject: "Re: Distribution in Europe..."
    date: 2004-09-10
    body: ">Did you consider selling the nx5000 with a blank hard disk\n\nand the AZERTY keyboard?\n\n>For cooperate customers we have a version available with freedos pre-installed\n\nIn practice, where to get such offer?"
    author: "Guy Passail"
  - subject: "Re: Distribution in Europe..."
    date: 2004-09-10
    body: "Why not? A lot of white-box manufacturers sell systems without OS. Does HP have a different regime, or are those manufacturers doing something illegal?"
    author: "Alex Fernandez"
  - subject: "Re: Distribution in Europe..."
    date: 2004-09-10
    body: "I am not familiar with the details. Usualy developers don't deal with this kind of stuff. But other manufacturers might not have this requirement."
    author: "Thomas Schneller"
  - subject: "Re: Distribution in Europe..."
    date: 2004-09-10
    body: "Big selling manufacturers have contracts for the OS. So they get the licenses for much less than retail price but they have to sell it with every new computer. (Some find that they can afford the way of having another product number for the OS-less model and that they consider that it is not against their contracts.)\n\nSmall seller (for example single shops or small groups of shops) do not have any license contract and they (and their clients) pay the full price of a license. But that allows them to sell the computer without any OS or directly with a Linux.\n\n(The same applies for the computer's CPU.)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Distribution in Europe..."
    date: 2004-09-10
    body: "To be more precise: as the contracts are secret, it is well possible that one manufacturer having a contract can do it and the other one cannot."
    author: "Nicolas Goutte"
  - subject: "nx9010"
    date: 2004-09-09
    body: "I have the nx9010 dual-boot notebook (WinXP and RedHat FC1). Linux can be run with FC1 native kernel and a custom built with hibernation (sw suspend) and NTFS driver. Can I benefit from your work and install SUSE instead of FC1 to get full hardware support? (will WinXP run with flashed BIOS?) If so could you give some advices?"
    author: "Alex"
  - subject: "Re: nx9010"
    date: 2004-09-10
    body: ">Can I benefit from your work and install SUSE instead of FC1 to get full hardware support?\n\nDuring development we addressed several general issues together with Suse appearing when installing Linux on a Laptop. Specially ACPI aspects. So others might benefit from that if they are installing Suse on their Laptops. But I can not say, if all hardware will be supported out of the box as we focused on the nx5000 specific components. \n\nQuestion: what's not supported so far on the nx9010?\n\n>will WinXP run with flashed BIOS?\n\nYes. The approache here is to offer a BIOS which can be flashed on a Linux machine directly. But it's not a special BIOS version. And right now it's only available for the nx5000. We discuss to offer that for other models as well."
    author: "Thomas Schneller"
  - subject: "Re: nx9010"
    date: 2004-09-10
    body: "Hi, I bought a nx9010 last year and managed to get everything I need working in some way. I haven't tested firewire port and pcmcia slots because I don't have any such devices. I and others wrote our experiencies with nx9010 on the page:\n\nhttp://freax.be/wiki/index.php/Installing RH9 on a Compaq nx9010 laptop\n\nEven the hibernate worked for me but my filesystem used to get corrupted from time to time then I decided not to use hibernate. The only one thing that bothers me now is that the two USB 1.1 controllers, ethernet card, video card and modem use the same IRQ 10. Because of this from time to time I see a \"too much work for IRQ 10\" message in /var/log/messages. Who could I contact to help me with this problem? It would be great if the BIOS let us change the IRQ assigment, but it doesn't."
    author: "Lamarque Souza"
  - subject: "Re: nx9010"
    date: 2004-09-10
    body: "Lamarque,\n\nregarding your IRQ conflict please read that:\n\nhttp://www.linux.org/docs/ldp/howto/PCMCIA-HOWTO-5.html\n\nthere is a way to modify how the kernel assigns IRQ resources. And as every time, a solution to configure it within Linux. Good luck!\n\n>Even the hibernate worked for me but my filesystem used to get corrupted from time to time then I decided not to use hibernate\n\nAs a tester of the nx5000 pre-load I tested this function many times. The distro tested was Suse 9.1. I experienced some problems in the beginning. Suse writes the content of the RAM into the swap partition. So if it happens to be a state, where the RAM content can  not be restored for any reason you will lose your swap partition. Slowing up performance. Cleaning swap solves the problem.\n\nMy personal opinion is, there is still more work to do. The current target should be a clear definition within the community how to standardise the implementation of this kind of functions. What's about wake up? Seen many drivers failing here. First thing to fix. \n\nSo go forward. Any feedback is welcome.\n\nThomas"
    author: "Thomas Schneller"
  - subject: "Re: nx9010"
    date: 2004-09-10
    body: "Hi Thomas,\n\nThanks for poiting me to the HOWTO. I'll try it.\n\nWhen the notebook wakes up everything works fine, even X11, sound card and modem. I haven't tested the ethernet card and the X11 DRI driver after hibernate. I haven't had any crash after waking up. Hibernate was working amazingly for me, if it wasn't for the fact that my Gentoo Linux run fsck once in 21 mount operations I didn't even know that the partition had got corrupted until it was too late. Fortunately I lost only a few KDE config files in my HOME. When I tested hibernate I used kernel 2.4.22, now I use 2.6.7. I'm planning to use hibernate again with 2.6.8.1 or 2.6.9."
    author: "Lamarque Souza"
  - subject: "Re: nx9010"
    date: 2004-09-11
    body: "Hi Thomas,\n\n> Question: what's not supported so far on the nx9010?\n\n   * suspend to RAM doesn't work (P4M supports it as I know (?)),\n\n   * I had to hack ospmd (it takes two events during one power button press, probably for bouth actions - on/off),\n\n   * sound buttons +/-/mute, functional buttons mail/search/...\n\nsome drawbacks for all notebooks running linux (they are not SUSE specific features but must be supported by default, of course they are kernel-related mostly but SUSE as well as others can add required features to kernel being set during installation):\n\n   * hibernation saves kernel with cached data what can take a while, I have to free RAM by memory-eating application before to hibernate,\n\n   * cdrom isn't used in read-ahead mode (it is always spinning even if mp3's are read piece-by-piece),\n"
    author: "Alex"
  - subject: "Re: nx9010"
    date: 2004-09-11
    body: "What kernel are you running?"
    author: "Thomas Schneller"
  - subject: "Keyboard layout"
    date: 2004-09-09
    body: "You mention European users can order the laptop from the U.S. Unfortunately I can't see an option to customize the keyboard layout.\n\nIs there any way to get this notebook with a Spanish or French keyboard, for instance? (They have some additional keys, such as \"\u00f1\" pr \"\u00e7\")."
    author: "Quique"
  - subject: "Re: Keyboard layout"
    date: 2004-09-09
    body: "My HP Compaq nc4000 notebook came from Sweden... I have solved keyboard layout problem with usual keyboard stickers that have cyrillic letters on them. That's quite cheap decision in our country. :)) Ah, and SuSe 9.0 works quite OK on this machine. The only *real* problem is Suspend mode. Standby mode is working, however.\n\n"
    author: "Sergei Dubarev"
  - subject: "Re: Keyboard layout"
    date: 2004-09-10
    body: ">Is there any way to get this notebook with a Spanish or French keyboard, for instance?\n\nThe only solution here is to buy it with US keyboard and replace it by a keyboard of choice. Replacements can be ordered here:\n\nhttp://h71021.www7.hp.com/"
    author: "Thomas Schneller"
  - subject: "Interesting"
    date: 2004-09-09
    body: "my feedback:\n\n- using a distro like SUSE is good, as many commercial software packages are basically only supported on RedHat or SUSE.\n\n- any plans for more HP branding on the desktop? From what I can see, they look like a standard SUSE desktop. Any plans for customizing the themes, icons, etc, to make the desktop more HP-like, like Sun has done?"
    author: "anon"
  - subject: "Re: Interesting"
    date: 2004-09-10
    body: ">- any plans for more HP branding on the desktop?\n\nAs mentioned in the articel it depends on the market reaction.\n\n> Any plans for customizing the themes, icons, etc, to make the desktop more HP-like, like Sun has done?\n\nWe didn't discuss that. The wallpaper was the only thing we customised do far. "
    author: "Thomas Schneller"
  - subject: "Linux support for other laptops"
    date: 2004-09-09
    body: "Hello. I have a Presario 900 Laptop, would you offer linux support for this laptops (graphic drivers, etc)\n\n"
    author: "Carlos Lorenzo Mat\u00e9s"
  - subject: "Re: Linux support for other laptops"
    date: 2004-09-10
    body: "Hello,\n\n>I have a Presario 900 Laptop, would you offer linux support for this laptops (graphic drivers, etc)\n\nThis product is still end of live. So no special Linux support will be offered for it. But I searched arround a bit and found that there are no major problems running Linux on that model. You might have a look here:\n\nhttp://videl.ics.hawaii.edu/mailman/listinfo/linuxpresario900"
    author: "Thomas Schneller"
  - subject: "*KEWL*"
    date: 2004-09-09
    body: "Hi\nI'm a long time HP-reseller (also have a HP-Star-Cert.) and really like the HP \nnx-series. I've Debian on my nx7000 and always need to borrow a harddrive of a friend (which has windows on his nx7000) to flash the BIOS.\nIf HP would make a BIOS Flash utility available for Linux, this would be great!\nAn other problem is the SD-Card reader. Cause of a damn NDA (AFAIK), there are no opensource drivers for this device :(\n(<a href=\"http://www.newsforge.com/article.pl?sid=01/12/20/2224254&mode=thread\">http://www.newsforge.com/article.pl?sid=01/12/20/2224254&mode=thread</a>)\n\nHowever, HP's on the right way :)\n\n(Sorry, my english is not that good)\n\n"
    author: "Nicolas Christener"
  - subject: "SD-Card reader"
    date: 2004-09-09
    body: "<a href=\"http://projects.drzeus.cx/wbsd/\">http://projects.drzeus.cx/wbsd/</a>\n\n"
    author: "ac"
  - subject: "Re: SD-Card reader"
    date: 2005-06-28
    body: "HI Im LOKING FOR CARD READER WRITER KT-2908"
    author: "HASSAN YOUZBACHI"
  - subject: "Re: *KEWL* --- more ZD7000 questions"
    date: 2005-06-22
    body: "The Winbond driver *specifically* states that it is not for the ENE Technology device.\n\nThomas: could HP exert some pressure on ENE to release specifications to the open source community? Ad-hoc efforts from BSD and Linux people have not had ANY success. Seems like it'd be a very noble thing for HP to do. Please advise.\n\nAnother thing: on a ZD7000, one cannot change out the broadcom minipci card for, say, an atheros card. For those of us who do wireless hacking this is a very big lose. Could a revamped BIOS happen?\n\nFinally, ACPI suspend is very problematic with the ZD7000. Nvidia and other drivers are a problem. Could HP post specific guidance on making it work?\n\nIf HP got its Linux act together, I would buy another laptop from HP. However, I can't say I would at this point: there are other manufacturers who seem to be more willing to provide better support across their product lines.\n"
    author: "Bob Knight"
  - subject: "Warranty"
    date: 2004-09-09
    body: "I think you can't get your warranty in the EMEA if you purchased the hardware in another world-region.\n\nI have one problem two months ago with a Compaq Presario purchased in Italy, HP refused to honor the warranty for the display panel in LATAM. Luckily I could carry the machine to Italy one month later.\n"
    author: "Miguel S. Garrido"
  - subject: "Hardware options"
    date: 2004-09-09
    body: "I'm actually looking to buy a new laptop soon and the nx5000 looks pretty nice (once you up the RAM and get the SXGA+ screen)\n\nMy question is about the wireless card.  I'm a big fan of Intel's Pro series wireless cards, and they seem to be the only way to get \"G\" support anyway.  What I'm wondering is: would it be possible to order the Linux laptop with the Intel Pro wireless card?\n\nI realize that the Intel drivers are still alpha/unstable, but I'm willing to wait and would not expect HP to help me get it working.  Would such an order be possible?  If not, I can only imagine that I'd have to order a more expensive Windows laptop and install Linux myself.  Would this skew your sales statistics and hurt the chances of this model succeeding with Linux?\n"
    author: "ac"
  - subject: "Re: Hardware options"
    date: 2004-09-09
    body: "If you mean the 1400x1050 resolution with SXGA+ then I can tell you it's not working under Linux. HP has chosen the Intel 855G for the graphics in the nx5000 and the bloody Intel driver reads the resolutions from the BIOS and refuses to run at 1400x1050. I already opened a case for that with HP (which worked reasonably well considering the novelty of the product) and they forwarded the case to Intel. The message from Intel is that there won't be any new Linux driver before December. So, if you consider the high resolution display you should probably wait a bit until this is sorted out. Of course, you can use the 855resolution hack in order to get around it but that won't work with suspend and resume :-(. At least, I couldn't find a way yet to wake up the laptop with the 1400x1050 resolution.\n\nBut other than that it works great, including LinDVD. I haven't tested the wireless stuff because that's forbidden in our company.\n\n\nkk.\n"
    author: "ac"
  - subject: "Re: Hardware options"
    date: 2004-09-09
    body: "I think SUSE recently shipped a patch for this for 9.1 ... at least it was advertised as such."
    author: "vb"
  - subject: "Re: Hardware options"
    date: 2004-09-10
    body: "<p>1400x1050 resolution patch here (below)</p>\n\n<p><a href=\"http://perso.wanadoo.fr/apoirier/\">http://perso.wanadoo.fr/apoirier/</a></p>\n\n<p>I think the main criteria for buying a laptop for Linux is the screen resolution, SXGA+ should be the minimum criteria. If you can't get this HP Laptop right now, try buying <a href=\"http://hst33127.phys.uu.nl/~pit/Asus-M3N/index.html\">OEM Asus M3700</a>. You can buy one without MS on it. I just got one 3 months ago, 1400x1050 res PCLinuxOS works on this laptop. It resemble like (Apple iBook is also made by Asus).(Asus also makers of IBM Think and Ipod mini :-)).<p>\n\n\n"
    author: "NS"
  - subject: "Re: Hardware options"
    date: 2004-09-10
    body: "As I wrote before, 855resolution is not a complete solution as it doesn't survive a suspend/resume. I haven't found a way yet to patch the BIOS on resume before the X server is restored and the result is that X simply doesn't get restored. So, if you don't care about suspend/resume it works fine but on a laptop suspend/resume is one of the main features so the 855resolution program is NOT a fix for the resolution problem.\n\nkk.\n"
    author: "ac"
  - subject: "Re: Hardware options"
    date: 2004-09-10
    body: "About the wireless card if i'm not wrong it's an intel ipw2200. There is a lovely guy (although he has a greece surname :-P ) from intel that is doing a wonderfool work with wireless intel cards, under GPL, have a look at ipw2100.sf.net. I'm usng his driver for my intel ipw2100 at home where I have a WEP-ed network and it does wonders.\nThe support I got from him too.\nSo this card will work just fine.\nAnd, together babies, give him a thank!!!!!\n\n--\ntirona rulez :-)"
    author: "anon"
  - subject: "Re: Hardware options"
    date: 2004-09-10
    body: ">would it be possible to order the Linux laptop with the Intel Pro wireless card?\n\nSure. Just choose the Intel wireless option. Driver can be found here:\n\nhttp://ipw2200.sourceforge.net/\n\nI've seen it working on several distributions. But as it is not part of the pre-installation HP will not officaly support it."
    author: "Thomas Schneller"
  - subject: "Re: Hardware options"
    date: 2004-09-10
    body: "From what I see, Intel wireless is not an option with the Linux model--only with the Windows model."
    author: "ac"
  - subject: "Re: Hardware options"
    date: 2004-12-12
    body: "I'd love to see the nx5000 w/ Linux include the Intel Pro Wireless 2200 BG instead of the HPW500 as an option. I'd buy it in a second."
    author: "Michael"
  - subject: "Re: Hardware options"
    date: 2004-12-12
    body: "Of course I say that and it's there already. I guess I'll just have to buy it then :-D"
    author: "Michael"
  - subject: "Laptop's specs/parts in detail via html & flash"
    date: 2004-09-09
    body: "HP makes it easy to find this <a href=\"http://h18000.www1.hp.com/products/quickspecs/11860_na/11860_na.HTML\">really informative link to drawings & specs</a> that labels and identifies the laptop's parts. I found a flash file that uses photgraphs in a similar fashion and is quite helpful.\n<a href=\"ftp://ftp.hp.com/pub/information_storage/software/flash/nx5000_part_locator.swf\">ftp://ftp.hp.com/pub/information_storage/software/flash/nx5000_part_locator.swf</a>\n\nThis is quite the laptop and now a real contender for my next major purchase.\nJames Pryor\n"
    author: "James Pryor"
  - subject: "Re: Laptop's specs/parts in detail via html & flash"
    date: 2004-09-09
    body: "Have you looked at Dell 5160? 3GHz Petium-M for under $1200."
    author: "Nadeem Hasan"
  - subject: "Re: Laptop's specs/parts in detail via html & flash"
    date: 2004-09-09
    body: "Not quite, the Dell Inspiron 5120 has a Pentium 4 M processor aka Mobile Pentium 4. This is different to the Pentium M and doesn't have the same low power usage. The Moniker M was probably added by Marketting.\n\nThe rule of thumb I use is: < 2 GHz probably Pentium M, > 2GHz definately not Pentium M.\n\n\n"
    author: "moreati"
  - subject: "Re: Laptop's specs/parts in detail via html & flash"
    date: 2004-09-09
    body: "I've got the precursor of the 5160, the 5150, and I'm really satisfied. It isn't often that one still feels pretty chuffed six months after a computer purchase, but given that Dell doesn't offer either the 5150 or the 5160 with the 1600x1200 screen and that prices haven't gone down appreciably since February, I feel I made my purchase at the right time. This screen is so unbelievably good...\n"
    author: "Boudewijn Rempt"
  - subject: "Re: Laptop's specs/parts in detail via html & flas"
    date: 2004-09-14
    body: "Yeah, I'm typing on one now. Love it. The HP doesn't look very special compared to the Dell -- still, a fine machine. ;-)"
    author: "yeahright"
  - subject: "European Localization"
    date: 2004-09-09
    body: "Hi, \n\nI'm sure there will be demand for this notebook in Europe (and the rest of the world, btw). Given that you based your US version on SuSE, will the add-ons be contributed to a future SuSE version? Then, these might work out-of-the-box, and everybody could buy a notebook (without os) and install a perfectly running SuSE linux....\n\nRegards, Andreas"
    author: "Andreas"
  - subject: "Re: European Localization"
    date: 2004-09-10
    body: ">Given that you based your US version on SuSE, will the add-ons be contributed to a future SuSE version?\n\nThere are no special add-ons except the wallpaper and LinDVD. The pre-installation is based on Suse 9.1 professional. So if you install that it will run pretty much out of the box. Some manual tewaks of ACPI and some configuration to make the special buttons works. But that's not unusual for a Linux installation."
    author: "Thomas Schneller"
  - subject: "Re: European Localization"
    date: 2004-09-10
    body: "Will the manual tweaks of ACPI and the configuration to make the special buttons work be published somewhere?"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: European Localization"
    date: 2004-09-10
    body: "At least the aswer regarding the special keys still available:\n\nhttp://dot.kde.org/1094715499/1094828408/"
    author: "Thomas Schneller"
  - subject: "Video card options..."
    date: 2004-09-09
    body: "I noticed that the only video card option seems to be the intel integrated extreme 2.  which is not the great of a video card really.  How come no ati or nvidia option?  I wouldn't think their license requirements would be that bad if your selling a laptop, but then I don't know for sure.  I went through a full configure before I realized I couldn't choose a video card.\n"
    author: "Nathan"
  - subject: "Re: Video card options..."
    date: 2004-09-10
    body: ">How come no ati or nvidia option?\n\nAs we use the Intel chipset where graphics is part of, offering other graphics otions would mean to develop a second motherboard with a different chipset. This might change in the future with pci-express as graphics is a module in this case which can be changed."
    author: "Thomas Schneller"
  - subject: "Thoughts"
    date: 2004-09-09
    body: "From a marketing perpective, the US only decision is probably not a great idea. The acceptance of Linux in the US is much weaker than in Europe in  my personal experience. \n\nThis could be a great niche if pursued by HP, but trying it out in the European (German) market first and gearing it at a student / tech audience is where the money is. \n\nAnd there would be a lot of money there I believe. I personally have been waiting for a fully supported preinstalled laptop of this kind for a long time. I need someone who guarantees that eg the wireless card works out of the box. I just won't risk > 1500 Euros to kater find out that it could have worked in principle but ...\n\nbest of luck in the US corporate market."
    author: "vb"
  - subject: "Re: Thoughts"
    date: 2004-09-10
    body: ">I personally have been waiting for a fully supported preinstalled laptop of this kind for a long time. I need someone who guarantees that eg the wireless card works out of the box.\n\nOption here is to buy the windows version and install Linux yourself. If choosing Suse 9.1 pretty much everything except SD card reader will work out of the box when choosing the HP wireless option. Driver for that device is available and works without any problem."
    author: "Thomas Schneller"
  - subject: "Re: Thoughts"
    date: 2004-09-10
    body: "> Option here is to buy the windows version and install Linux yourself.\n\nBut that means paying the Microsoft tax. This is a showstopper for a lot of people (well, for me it is anyway)."
    author: "Anonymous"
  - subject: "What I don't understand"
    date: 2004-09-10
    body: "Is why is HP testing Linux on the worst market possible, where Linux-penetration is the lowest (at least of all industrialized countries)?\n\nThe nx5000 would be a great success in Europe and Asia, as there is much more mindshare and people running Linux there.\n\nAs for localization, all that is needed is the appropriate keyboard-layout, SuSE itself comes in several dozen languages, as does KDE.\n\nSo please bring the nx5000 to Europe as soon as possible, you will get a bigger response than in the USA.\n"
    author: "Roland"
  - subject: "nx9110 and hp Wows..."
    date: 2004-09-10
    body: "I congratulate HP for such initiative. I think I was getting worried that while HP was claiming linux support, their various product branches were not even checking if linux run on their machines. It is very difficult to find information on which machine runs linux fully (meaning with all drivers). When you corporate purchase, most of the time you purchase on specs alone. Yes, you can have a demo machine, but sometimes it is difficult to test everything, or the demo is slightly different from the sold machine. For instance I see that you must purchase the nx5000 with the non broadcom wireless card.\n\nI used HP customer support and suceed to escalate the problem to product managers regarding my HP nx9110. The ATIIXP new shipset is just supported in latest kernel, the winmodem is just supposed to be supported and the wireless card will never e supported, broacom refuses to release data. What I see important is that HP makes its choice of hardware now based on linux drivers. Yes they can still ship windows, but they should check that their hardware choices do not include unfriendly-linux manufacturers.\n\nFor all that to happen, well the sale of the nx5000 with linux must be a success so that everybody in HP take notice.\n\nI would like to see on HP web site, a graph that shows which machine runs linux kernel fully or not (ie there are linux drivers for the hardware). It will allow us, the community, to make an informed decision. We don't need a certification programme, but just, we installed this kernel, and all hardware components were recognised. Up to linux distributors to ship the right kernel and configuration tools.\n\nOk, carry on.... Good one.\n\nPS: I like the idea about mandrake. As I'm talking about it, they do need to test their distribution on HP servers, as the HP raid array card is always having trouble to being detected at install. HP can you give them access to machines so they can test each release?\n"
    author: "Franck Martin"
  - subject: "Re: nx9110 and hp Wows..."
    date: 2004-09-10
    body: "Hi,\n\nI purchased the Compaq Presario R3000Z with AMD64 bit processor and this laptop is an excellent candidate for installing Linux. I have Fedora Core 3 working just fine. The only things I had to do is install the 3D Nvidia drivers for full video support. Install ndiswrapper for Broadcom Wireless NIC and finally get the pcmcia config file from \"Linux on Laptop\" website so that 16bit cards can work (Sorry to say that 32 bit cards are still not working). After all this tinkering, the only thing that does not work is the multimedia card, modem, suspend to disk and standby mode. However, I am very happy with my laptop and can't wait for the rest of it to work without a glitch as the Linux 2.6 kernel continues to mature."
    author: "CoolBreezeOne"
  - subject: "Re: nx9110 and hp Wows..."
    date: 2004-09-10
    body: ">and the wireless card will never e supported, broacom refuses to release data\n\nbroadcom? As far as I know the nx9110 is offered with HP wireless option which is a Atheros based module. Drivers are available for it. The other option is Intel wireless where a driver is available as well.\n\n>As I'm talking about it, they do need to test their distribution on HP servers, as the HP raid array card is always having trouble to being detected at install. HP can you give them access to machines so they can test each release?\n\nI will forward that request to the right people."
    author: "Thomas Schneller"
  - subject: "Re: nx9110 and hp Wows..."
    date: 2004-09-10
    body: "I can confirm the Mandrake problems with the HP servers that use the cciss storage driver.\n\nModel: Hp/Compaq ProLiant ML370T G3 2.4 Ghz Xeon\n\nWe are using two drives in the 6-Bay Ultra3 hotswap drive cage. It works fine with Suse 9.1, but MDK, which is what we have standardize on company wide, had trouble recognizing the array.\n\nThanks for your great work. I am going to see if we can get some of the new HP laptops with Linux preloaded at work."
    author: "Anon..."
  - subject: "next: Linux on iPAQ?"
    date: 2004-09-10
    body: "Well the next step is have HP sells one iPAQ with something like GPE (http://gpe.handhelds.org/)\n\nCheers"
    author: "Franck Martin"
  - subject: "I would like Athlon 64 based notebook with Linux.."
    date: 2004-09-10
    body: "I am happy to see HP finally offer a notebook with Linux.  (Even though many of their salespeople don't seem to know it yet.)  Unfortunately it seems to be only the nx5000 so far.\n\nI would really like to buy an Athlon 64 based notebook but am opposed to paying the \"windows tax\" and have been delaying a notebook purchase for several years now...\n"
    author: "Esko Woudenberg"
  - subject: "Re: I would like Athlon 64 based notebook with Linux.."
    date: 2004-09-10
    body: "<p><i>I would really like to buy an Athlon 64 based notebook but am opposed to paying the \"windows tax\" and have been delaying a notebook purchase for several years now...</i></p>\n\n<p>Have you considered getting something from Apple?</p>\n\n<p>I know it's not what you talked about, but OS X seems pretty neat, Powerbooks are very high quality and at least your money doesn't go to microsoft.</p>"
    author: "Mikhail Capone"
  - subject: "Re: I would like Athlon 64 based notebook with Linux.."
    date: 2004-09-10
    body: "I just got an AMD64 notebook and it's sweet. I too don't like the windoze tax, but in this case I had several reasons to object less.\n1) Price wise it was still irresistable\n2) Because of compatibility issues I could insure access to everything this way including flashing the bios if need be so it is sort of a back up until I get everything working.\n3) It gives me one system I can test web sites of my test server with IE.\n\nThe funny thing is that since windoze can't take advantage of AMD64 the prices are falling. I just saw an AMD64 chip and an AMD Barton with the same speed rating at the same price. I got a Compaq Presario R3240US with a 3200+ processor, 512 MB of RAM, 60 GB drive, 15.4 inch wide screen (1280x800) and a DVD burner. It also has the rest of the usual goodies and it is really sweet. I got it for $1479 at Bestbuy and it has $180 in rebates. That comes in at $1,300. They are $50 less now. \n\nThe downside is what doesn't work. The touchpad works in a 64 bit Knoppix port with a 2.4 kernel, but I'm running Gentoo which sensibly makes 2.6 the default kernel for all the benefits 2.4 doesn't have. Anyway early attempts to use the touchpad have had unstable results and while I think I found a solution I have a USB notebook mouse I like better for most uses. The card reader is on my do list, but I have USB for my camera. The wifi is broadcom and works if you want to run 32 bit, but it looks like I may need to spring for a PCMCIA card if I want 64 bit wifi. Outside of that I'm slowly tweaking the rest.\n\nPerformance is startling. I was showing a neighbor and went to edit a photo in Gimp2. I selected one on my desktop system (AMD XP 1900, also 512 MB RAM) and the splash came up. Then I navigated to a photo on the laptop and lanuched. It looked like I maybe should have started them together to keep from embarassing myself... but the laptop suddenly opened the windows and the image and was done... leaving us to watch the windows draw on the desktop for several seconds. I've read speed can be substantially more in 64 bit mode and that is the explanation that makes sense of 3200 being over twice as fast as 1900. It compiled kdebase clean in something less than an hour and a half and compiles often look like catting a file. I'm looking forward to when x86_64 optimzations get better as it has really only started compared to x86.\n\nWe can certainly hope for things to get better since HP owns Compaq now and I believe HP is showing commitment to Linux. Bleeding edge hardware is always more challenging, but one thing is for sure. Athlon 64 is just awesome in Linux! It's hard to believe this is in a laptop, which BTW I ran in windoze before loading Linux and got several hours on the battery with PowerNow doing email in wifi. It's just really sweet. "
    author: "Eric Laffoon"
  - subject: "Re: I would like Athlon 64 based notebook with Linux.."
    date: 2004-09-10
    body: "Hi Eric,\n\nI think I got a similar model, just without the wifi stuff.\n\nTo get the touchpad to work you need to apply the alps patch to the kernel, and install the synaptics driver, and modify the XF86Config a little. Works stable for me, including the scroll area on the touchpad, also together with an USB mouse.\n\nThere are some reports on the internet with details, I can look them up for you if needed. I posted them also on the SuSE AMD64 mailing list.\n\nI recommend using a kernel >=2.6.7, because otherwise PCMCIA might not work, and that's what you say you want to use. \n\nBTW, the Compaq and the HP AMD64 notebooks are almost identical. So you can go by instructions for anyone of them.\n\nAnd SuSE 9.1 for AMD64 has touchpad mostly working (not the scroll area) right out of the box, but not PCMCIA, because the kernel used is 2.6.4.\n\nHTH, \nMatt"
    author: "Matt"
  - subject: "Re: I would like Athlon 64 based notebook with Linux.."
    date: 2004-09-10
    body: "<p>Actually I just bumped into this. It's way lighter than mine and less expensive as priced, but the video may introduce problems as I've seen ATI be less than desirable with AMD64. Still this is no windoze tax and looks good.</p>\n\n<p><a href=\"http://linuxcertified.com/linux-laptop-lc2464.html\">http://linuxcertified.com/linux-laptop-lc2464.html</a></p>"
    author: "Eric Laffoon"
  - subject: "Me too"
    date: 2004-09-10
    body: "Same here. I'll wait for a good one to be available in the UK to replace my desktop. "
    author: "Olivier Magere"
  - subject: "Re: I would like Athlon 64 based notebook with Linux.."
    date: 2004-09-10
    body: "<p>There is an HP Athlon 64 laptop available:</p>\n\n<p><a href=\"http://h10010.www1.hp.com/wwpc/uk/en/sm/WF05a/21675-283229-283229-283229-297845-4532387.html\">http://h10010.www1.hp.com/wwpc/uk/en/sm/WF05a/21675-283229-283229-283229-297845-4532387.html</a></p>\n\n<p>and I will forward the complains about the \"windows tax\" to the right people.</p>\n\n<p>Thanks</p>"
    author: "Thomas Schneller"
  - subject: "zv5188EA AMD64 laptop"
    date: 2004-09-10
    body: "you can count one more in your linux stats - I bought the HP zv5188EA AMD64 laptop, only to throw out Windows and install SuSE 9.1 for AMD64, and it rocks!\n\nOnly the winmodem refuses to work. Is it the same as in the nx5000? SuSE 9.1 recognizes it somehow, but does not configure it. \n\nGet this going somehow, and you can sell the zv5188EA and similar with SuSE 9.1 AMD 64 easily. All you need to do is to \n\n* add the unusual resolution to the x setup,\n\n* if you need pcmcia: adjust the pcmcia config file to include the memory and ports of this architecture, and use a kernel >= 2.6.7 \n\n* have a look at the touchpad setup it works out of the box, but not the scroll area at the right side of it, for that you need to install the synapticts driver and the alps kernel patch) and\n\n* get this stupid winmodem going!\n\nThe zv5188EA boots up with SuSE 9.1 *extremely* fast, much much faster than my sata P4 2.4 ghz desktop, and works stable, no problems at all, with great performance.\n\nOnly the winmodem is annoying me, I don't like to carry an external one around... HELP!!!\n\nRegards,\nMatt"
    author: "Matt"
  - subject: "Re: zv5188EA AMD64 laptop"
    date: 2004-09-10
    body: ">Only the winmodem refuses to work. Is it the same as in the nx5000? SuSE 9.1 recognizes it somehow, but does not configure it.\n\nJust did a install of standard Suse 9.1 on my nx5000 test unit. Modem was recognized during installation but not configured. When clicking configure it installs the smartlink softmodem driver and it is up and running. Could still use it during installation to pull updates from Suse.\n\nPlease make sure that wait for dial tone is disabled. Having that enabled causes problems sometimes.\n\nThanks for the feedback"
    author: "Thomas Schneller"
  - subject: "Re: zv5188EA AMD64 laptop"
    date: 2004-09-10
    body: "Hi Thomas,\n\nhere YAST does tell that it configured the modem on /dev/ttyS3 (which sounds not bad), but when I try to use it:\n\nkinternet: \"Modem not responding\"\nkppp: \"modem busy\"\n\nBut now I'm full of hope: \n\nYour answer does imply somehow, that the winmodems used in these both notebooks (my HP zv5188EA AMD64 and your nx5000) are indeed the same! (Yast just says it is a \"Hewlett-Packard Company Modem\".)\n\nThese means I could go ahead and install the smartlink softmodem driver myself and it \"should\" work, shouldn't it? May be there was just a hickup in SuSE's setup of the smartlink stuff for the 64 bit version...\n\nRegards,\nMatt"
    author: "Matt"
  - subject: "Re: zv5188EA AMD64 laptop"
    date: 2004-09-10
    body: ">These means I could go ahead and install the smartlink softmodem driver myself and it \"should\" work, shouldn't it?\n\nHopefully. Didn't test it with the Linux 64bit version, but chances are good...."
    author: "Thomas Schneller"
  - subject: "Re: zv5188EA AMD64 - smartlink does not do 64 bit"
    date: 2004-09-10
    body: "oops, at http://portal.suse.de/sdb/en/2004/05/thallma_91_smartlink.html\n\nI found this:\n\n\"The SmartLink driver is not 64-bit-capable. Therefore, it is not included in the 64-bit version of our distribution.\"\n\nThat is why it did not work with SuSE 9.1 AMD64 out of the box...\n\nYeah, and testing to compile it myself fails with \"i386 architecture of input file 'dsplibs.o' is incompatible with i386:x86_64 output\"\n\nSo what will HP do? Get smartlink to write a 64 bit capable driver, or get a real modem build in the notebooks?"
    author: "Matt"
  - subject: "Re: zv5188EA AMD64 laptop"
    date: 2004-09-10
    body: "Be carefull though with pavillions. I bought a zv5139EA, which has one of the cripled AMD64s without x86-64, and I've had tons of trouble. \n\nIt boots up in powersave mode which is only half speed, and they have disabled the legacy-style powernow mode from the BIOS, which means powernow only works in Linux 2.6 over ACPI, and just that combination happens to be very unstable. For the first 2 month I owned it, I was only able to run the laptop in half speed when using Linux, now I found a temporary-fix; by disabling IO-APIC, ACPI is now mostly stable in Linux 2.6, and I've been able to run full-speed.\n\nThe wireless card still doesnt work even though I've installed the ndiswrapper, and broadcom windows-driver. Apparently they are using one of those proprietary software disables, which means only a special windows tool can enable the card."
    author: "Carewolf"
  - subject: "Re: zv5188EA AMD64 laptop"
    date: 2005-02-15
    body: "I know the smartlink modem driver you need is compiled only for I386 architecture. So until the smartlink company does not recompile it's binary object for a x86_64 architecture this winmodem does not work. The only alternative is to run a ix86 32bit linux system. \n\n     I've yet send an email to smartlink about their plan to porting this driver."
    author: "Ferdinando"
  - subject: "Real Availability"
    date: 2004-09-10
    body: "From the interview:\n\n\"It would have been too much effort to offer it for Europe as well as there are so many languages and localization options to take care of.\"\n\n\"Well any needed software we were looking for was already provided by the SUSE Linux distribution like for example the power management applet. So we did not need to write additional software.\"\n\nFrom the \"European Localization\" thread:\n\n\"There are no special add-ons except the wallpaper and LinDVD. The pre-installation is based on Suse 9.1 professional.\"\n\nSo, given that KDE is fairly extensively localised, what's all the extra effort for? Documentation? Are there applications in SuSE 9.1 which aren't localised properly? I'd be tempted to get a laptop like this if it were available with Linux in the Nordic region, but the only models that HP resellers offer (including the nx5000) run Windows XP. And I don't see why I have to drop money into Microsoft's slush fund just to have the \"privilege\" of installing a separate copy of SuSE over the top.\n\nSo what's it going to take for HP to really make this available in the rest of the world? Outside developer gatherings, that is."
    author: "The Badger"
  - subject: "Re: Real Availability"
    date: 2004-09-10
    body: "<p><i>So, given that KDE is fairly extensively localised, what's all the extra effort for? Documentation? Are there applications in SuSE 9.1 which aren't localised properly?</i></p>\n \n<p>KDE is localised, but the HP manuals aren't. That's the costly part.</p>"
    author: "KOffice fan"
  - subject: "Re: Real Availability"
    date: 2004-09-11
    body: "> KDE is localised, but the HP manuals aren't\n\nNot really :(. I've tried using it in Finnish once or twice, but\nwhat I usually get is a severe language mess :("
    author: "jmk"
  - subject: "Re: Real Availability"
    date: 2004-09-10
    body: ">So, given that KDE is fairly extensively localised, what's all the extra effort for? Documentation?\n\nDocumention on one hand, keyboard layout on the other hand. We need to generate a sku for every configuration, which costs money. In additon our quality assurance guidlines are very strict. This means, all this skus would need to pass the internal quality assurance which would have been an enormous workload. Keep in mind how many languages we have here in Europe.\n\n>And I don't see why I have to drop money into Microsoft's slush fund just to have the \"privilege\" of installing a separate copy of SuSE over the top.\n\nWe are internally discussing how to address that issue."
    author: "Thomas Schneller"
  - subject: "Re: Real Availability"
    date: 2004-09-10
    body: "\"Documention on one hand, keyboard layout on the other hand.\"\n\nSince the nx5000 is available in the Nordic region (for example) with Windows XP, and since other models (like the one I'm using now) have localised keyboards - so I suppose the nx5000 also has them - doesn't that just leave the documentation?\n\nI think what you've done is a great start. What would be even better would be worldwide support for Linux across as many models as possible. Whilst the suits at corporate HQ might claim that \"customer demand drives our offerings\" and that \"there isn't significant demand for Linux\", one has to ask how customer demand can influence HP through resellers who don't offer Linux options and who show no sign of doing so.\n\nA Linux laptop from a vendor who can support it in my region (although hopefully that vendor could also put a bit of pressure on their suppliers, too, in the case of Intel and their chipset) would rise to the top of my shopping list. Without such a product on offer, I might as well buy a system without Windows from a vendor who is honest about what's in the box and where I have a chance of discovering whether it supports Linux well enough."
    author: "The Badger"
  - subject: "Re: Real Availability"
    date: 2004-09-10
    body: "As said we would need to generate a seperate sku for every configuration different from the one shipping with MS. A sku is a list of components the unit will be assembled out of and a list of items needed to be packed in the box. This sku goes into maufactuing which delivers the product to the self. So doing a extra sku for all european country simply was to expencife specially when not knowing how the market demand is.\n\nBut as said as well, we are internally discussing how to address that.\n\n>one has to ask how customer demand can influence HP\n\nYou are still influencing HP. We appreciate your feedback and I will input directly into the Linux core team."
    author: "Thomas Schneller"
  - subject: "Re: Real Availability"
    date: 2004-09-10
    body: "\"So doing a extra sku for all european country simply was to expencife specially when not knowing how the market demand is.\"\n\nUnderstood.\n\n\"You are still influencing HP. We appreciate your feedback and I will input directly into the Linux core team.\"\n\nThanks for participating in this discussion! It's good to see that some companies are interested in what (potential) customers think."
    author: "The Badger"
  - subject: "Thomas, thank you for showing that HP gets it"
    date: 2004-09-10
    body: "If your answers are any hint of what we can expect from HP now and into the future, I think HP has just made itself a bunch of friends.\n\nMy company is moving slowly to Linux: 30%Linux, 70%Windows. We expect to be 50/50 by the beginning of 2006. Our biggest fear was user training and we have needed little to no training. We want to be able to get our computers, particularly laptops, preloaded with Linux to ease the intallation process. I am hoping that there will be additional laptops sold with Linux in the future, so that we can accomodate different usage profiles.\n\nThanks for all your great work."
    author: "Anon..."
  - subject: "Solution to the SKU problem"
    date: 2004-09-11
    body: "Split it into two boxes: one box that contains the notebook without a keyboard and linux preconfigured so that it asks for the language on power-on, and one separate box that contains the keyboard. \n\nBefore you start to laugh, two boxes, that's the way Apple did localization already back in the 80s... and it makes sense from an economical perspective!\n\n(IBM just lost me as a customer because they weren't able to ship a German keyboard for their ThinkPads to the USA at an acceptable price... so hopefully HP gets this right)"
    author: "probono"
  - subject: "Re: Solution to the SKU problem"
    date: 2004-09-11
    body: "Intresting idea. Is there an instruction how to assemble it? Don't understand me wrong, but their might be people out there with two left thumbs.\n\nWe need to take care of them.... :)"
    author: "Thomas Schneller"
  - subject: "Re: Solution to the SKU problem"
    date: 2004-09-12
    body: "I don't know about HP, but the IBM keyboards are user-exchangeable with just 3 screws or so. And I even heard that some manufacturers (Apple?) have \"keycaps sets\" where you can exchange the caps of the keys with a screwdriver... the whole \"localization\" then costs the user $5..."
    author: "probono"
  - subject: "Other HP Laptop Support..."
    date: 2004-09-10
    body: "Hi, REcently I got an hp nx 9005 notebook, and had some trouble with Suse Linux 9.1 Pro (ATI Driver, ACPI, FIreWire) It would be nice, if HP would release a Support Page with official drivers, installation tips, and even an HP sponsered mailing list for Running Linux (esp. Suse) on their models. Its nice to be able to tal to the people inside HP like you Thomas, who seem to know WAY much more about the inner working of teh laptops, and thus can assist in finding solutions faster...\n\nAny way, is my hp nx 9005 going to be supported!! Is it Wishfullthinking....\n "
    author: "Adham"
  - subject: "Re: Other HP Laptop Support..."
    date: 2004-09-10
    body: "Adam,\n\nthanks for the feedback. There are indeed discussions to collect and offer Linux installation instructions including links to drivers and stuff like that for selected models. Usage of mailing lists is also under discussion. In addition HP is working with several 3rd party hardware vendors to get Linux drivers for their products.\n\nRegarding nx9005 installation instructions, have a look here:\n\nhttp://tuxmobil.org/cpu_amd.html \n\nthere are several installation instructions for the nx9005. Should help to solve the ATI, ACPI and firewire problem."
    author: "Thomas Schneller"
  - subject: "Re: Other HP Laptop Support..."
    date: 2006-11-25
    body: "I have HP Pavilion dv4440 laptop . i have problems in installing its drivers as firewire, modem , sd card reader under fedora core 6 kernel 2.6.18-1.2849. Will you please guide me in installing these drivers.\n\n                                                      THANKS "
    author: "shahid mehmood"
  - subject: "Re: Other HP Laptop Support..."
    date: 2004-09-10
    body: "This link might be useful for you:\nhttp://freax.be/wiki/index.php/Installing RH9 on a Compaq nx9010 laptop\n\nnx9010 is very similar to nx9005 except it uses Pentiun IV instead of Athlon."
    author: "Lamarque Souza"
  - subject: "freenx / nomachine"
    date: 2004-09-10
    body: "Thomas, bit of a different question altogether; do you know if anyone at HP is looking at nomachine's NXserver and/or freenx? The advantages of connecting thin clients to a huge linux/unix server or cluster seem massive from the security and sysadmin point of view, and both the hardware and software TCO of such a setup would be very appealing. Not to mention that in the deal you get to sell and service big iron. Seems to me that nx has taken this technology to the next level."
    author: "Jeroen"
  - subject: "Re: freenx / nomachine"
    date: 2004-09-10
    body: ">do you know if anyone at HP is looking at nomachine's NXserver and/or freenx?\n\nYes. I am. I always was un-satisfied with the remote desktop experience when running it over modem. So I installed nx-nomaschine server on an Linux box within the intranet of my office. Over that gateway I was then able to connect into an MS intranet inviroment over VPN with a MS client connected by modem and then calling my office unit running WinXP. \n\nStrange routing:\n\nMS XP client connected via modem to the internet-->\ncalling gateway by pptp MS vpn-->\nconnecting to the nx-server running on Suse Linux 9.1-->\njump to my office unit running WinXP and connect to the nx-client.\n\n-->run the remote connetion over modem in a usable way.\n\nGreat. I love this software. Well done!\n\nApart from that, HP is not involved with any nx-nomaschine issues. Users are free to install and use it any time. \n\nBut at least I can say, tested and it's working.... :)\n\nHave a nice time\n\nThomas"
    author: "Thomas Schneller"
  - subject: "Are HP cameras and printers supported?"
    date: 2004-09-10
    body: "Will the Linux OS support the new cameras and printers?  What about the new HP iPod?  Does it ship with iTunes?\n"
    author: "David"
  - subject: "Getting Multimedia Keys to work with KDE"
    date: 2004-09-10
    body: "Hi,\n\nBeing positively surprised about the well-working software suspend, I was somewhat negatively surprised about the missing support for multimedia keys.\n\nThe solution is easy:\n\n1) Follow this description at\nhttp://wiki.linuxquestions.org/wiki/Configuring_keyboards\n(Mute is 160, VolDown is 174, VolUp is 176 on the nx5000, test other models with \"xev\").\n2) Load xmodmap on every start (is there a way to configure this in KDE?)\n\n3) install and run kdebase3-mixer aka kmix (you can disable kamix) via yast\n(I upgraded the packages to KDE 3.3 by adding ftp://ftp.suse.com/pub/suse/i386/supplementary/KDE/update_for_9.1/yast-source to the installation sources and do an upgrade, worked fine for me).\n\n4) open the mixer window, right click on the master volume slider, \"configure shortcuts\", assign keys.\n\n5) enjoy your newly working multimedia keys! Please note that the mute LED will not work, this will be fixed in KDE 3.3.1\n\nAlternative way: configure the keys with xmodmap as decribed above. http://hardware.mcse.ms/message65071.html has a nice script\nthat mutes the sound (even with a working mute LED). Similar could be written for vol up / down. Then assoctiate the script with the keys using khotkeys in kcontrol.\n\nThomas, could you add method #2 (or preferably #1) for a new revision of revision of the OEM installation? \n\nShould be fairly similar to the way you added the macrovision daemon. I only found it confusing that the daemon is not provided by any RPM package. Why is that?\n\nCheers,\n  Daniel"
    author: "Daniel Molkentin"
  - subject: "I'd definitely buy one..."
    date: 2004-09-10
    body: "if I hadn't just bought an Averatec laptop two months ago. A laptop that works with Linux out of the box, and that comes without the Microsoft tax, is a dream come true! And the price seems right, too.\n\nSo if HP keeps producing great Linux laptops when my current laptop becomes obsolete, they have a customer in me!\n\nKlaus"
    author: "Klaus"
  - subject: "Re: I'd definitely buy one..."
    date: 2004-09-10
    body: "AOL... My new Dell is only six months old, so I guess I won't be looking for a new laptop until 2006. Unless 64 processors really take off and I cannot resist... Or the Dell breaks down. But when I'm in the market again, HP will top the shortlist (together with ASUS, about whose laptops I've had good reports). (Which doesn't mean that I don't covet a new apple to replace the old powerbook I'm typing this on.)"
    author: "Boudewijn"
  - subject: "KDE contributor deal?"
    date: 2004-09-11
    body: "any information on the deal for KDE contributors? was it limited to those that were able to make it to Akademy?"
    author: "azhyd"
  - subject: "Re: KDE contributor deal?"
    date: 2004-09-12
    body: ">>> any information on the deal for KDE contributors? was it limited to those that were able to make it to Akademy? <<<\n\nYes.\n\nAnd only to the first 30 to shout \"here!\", when the offer was announced."
    author: "anonymous coward"
  - subject: "linuxr3000@quanta.homeip.net mailing list?"
    date: 2004-09-11
    body: "I found this very cool mailing list with lots of great info for Compaq R3000 series notebooks. It has been quiet for several days and I just posted and it seems it's off line. It looks like somebody is hosting it on their home system. Maybe they are out of town or perhaps live in Florida or something. I thought it was cool that their subdomain was quanta, but right now I am frustrated. I'm willing to host it on one of my servers which has near perfect uptime if they would like, but there is no way to know who had the list. Maybe it will be back. If you have any info please email me... sequitur at kde, don't forget the dot org."
    author: "Eric Laffoon"
  - subject: "Support for ZD7000 series?"
    date: 2004-09-11
    body: "Looking for a fast 17\" screen laptop, I bought a zd7140 in May and installed Fedora 2. While many features\nwork, there are some remaining issues. Might there be any support for the 7000 series? Specifically, I could\nnot get these features to work (stock kernel 2.6.7):\n- Suspend/hibernate: Could not get it to resume.\n- Blanking the screen: the backlight stays on. It seems to be hard-wired?\n- SD card reader: I found no trace of a driver.\n\nSupport for a Linux driver for the broadcom wifi card would be welcome. Ndiswrapper works, but the driver\nlacks featutes implemented for other cards (monitor mode, etc.). Pushing the wireless button locks the machine.\nIt appears that the BIOS is set up to not boot if a different wifi card is installed. Is a more tolerant BIOS version\n(and linux flash utility) on the drawing board?\nI have not tested firewire and S-video, etc.\n"
    author: "Rudi Nunlist"
  - subject: "Re: Support for ZD7000 series?"
    date: 2004-09-12
    body: "I don't see the flash utility being a problem. BIOS can be flashed from a floppy. That option seems to be offered from everybody. As long as that is possible, it is very easy to distribute BIOS updates to Linux users. All they need to do is to create one floppy and then create an image out of it on a Linux machine by doing\n\ndd if=/dev/fd0 of=image.file\n\nThen they can let us download image.file and we can reproduce the floppy by doing \n\ndd if=image.file of=/dev/fd0\n\nSo, no problem. They just need to be willing to spend a couple of minutes per BIOS version."
    author: "Fikret Skrgic"
  - subject: "HP zd7260"
    date: 2004-09-11
    body: "I love my new zd7260.  SuSE 9.1 Pro works great, with just a few things to note.  You need to set your keyboard to \"microsoft natural keyboard pro\" to get your multimedia keys working.  The broadcom chip works with ndiswrapper, but not out of the box, and it is somewhat limited.  The scrollbar on the touchpad does not work, but I will try the alps driver as mentioned a previous thread.  I have no use for the digital media port or for the infrared port, so I don't know if those work. Hibernate/suspend work fine.  There is an issue with the screen blinking sometimes, perhaps an issue with the nvidia driver.\n\nI would like HP to provide specific support about hardware compatibilty with linux, perhaps even ensuring they only use components that work in windows and linux.\n\nI had to pay the windows tax, but it would be nice to have had SuSE preinstalled (without windows).\n\nMark"
    author: "mark"
  - subject: "Re: HP zd7260"
    date: 2006-02-08
    body: "I just bought a refurbished Zd7260 and it needs a hard drive.  What make and model do you have in yours? "
    author: "David Hopkins"
  - subject: "Re: HP zd7260"
    date: 2006-03-20
    body: "Hi David,\n\n  I just found this post while looking for a different issue which I will as you about in a moment.  I also have a zd7260 running SuSE 9.3 Pro and my hard drive is an 80 GB Hitachi Travelstar 7K60 HDD: 7,200rpm, the ID is IC25N080ATMR04-0.\n  Now, let me ask if either you or Mark (the original post) has had any trouble with the hardware wireless button?  It used to work fine for me in the sense that I could turn the wireless off and on with it and it would light-up or go dark as expected.  Just last week it stopped working in Linux but still works as expected in Windows.  Have either of you seen this behavior?  Did you fix it somehow?\n\nThanks, and sorry for being off topic here.  It's hard to find Linux users with the same model laptop.\n\n--Richard"
    author: "Richard Witt"
  - subject: "I had to pay the windows tax, but it would be nice"
    date: 2004-09-11
    body: "Not to pay it?\n\nHeard that now many times. Seems to be a number one priority in the community?\n\nPlease vote......."
    author: "Thomas Schneller"
  - subject: "Re: I had to pay the windows tax, but it would be "
    date: 2004-09-12
    body: "Considering all the stories about how Microsoft ensures a steady stream of money by locking OEM into contracts which let them pay licenses per sold computer instead per actually sold software it should be no surprise and rather natural that those people aware of this practice while not at all interested in buying Microsoft software are looking for OEM which are not locked into the above scheme. (That sentence became a little long, sorry about that, heh.)"
    author: "Datschge"
  - subject: "Re: I had to pay the windows tax, but it would be nice"
    date: 2004-09-12
    body: "I have something to say about this. The fact that HP now sells one (ONE) model with Linux preinstalled is nice, but not thrilling. Here is my point of view and I believe that a huge number of people think the same. I really don't care whether it comes preinstalled with Linux. I can install it myself, I prefer to install it myself, and, as a matter of fact, even if I would buy that laptop, I would erase the hard drive and install what I want from scratch. Also, I don't need any support. I want a warranty for the hardware, of course, but beyond that I don't need anything else. What I am looking for is buying hardware without paying for Windows, not for Linux preinstalled. Once I have found a nice laptop on the HP website (generally I like HP laptops and I think they are reasonably priced) and I called to ask whether I can buy it without Windows. I was told: \"No way\". I don't, really, understand why. That's as if I would go buy dinner plates and they would tell me that I can buy them, but only if I buy some meatloafs with them too. I don't like that laptop, but I could definitely find one that I like. Well, if I want to buy the one I like, I have to buy Windows. As a matter of fact, I have an HP laptop and I paid for Windows which I immediately erased. I mean, Microsoft has it real nice. They don't have to care about competition, because even if you choose a competitor's product, you still have to pay to them. How could they not make money? I will not buy Windows anymore. There are some small companies selling OS-free hardware. Some people, as you can read above and I know anyways, are still paying for Windows and erasing it. A LOT OF people. Why can't I click on \"Customize\" on the HP website and select \"No OS\"? HP won't sell harware without support or whatever? I don't mind at all. We do that anyway. We buy hardware and install an operating system for which no support is offered. But we still pay for Windows. How can it be worse for us if we get the same just without paying for Windows? HP is no different from other big guys. I'm not saying that anybody else is better. This is an omnipresent problem. Then, there is a huge hype about Windows Media Player or Internet Explorer coming with Windows and antitrust laws. But that's really irrelevant compared to this. You don't have to pay extra money for IE or WMP. We are being forced to PAY for something we don't want and do not use. Let us take a look at this. Aren't the hardware and the software two separate products? If IE is a separate product with respect to the operating system, isn't the operating system a separate product with respect to the hardware. If I want to buy some hardware, why would I have to buy some software at the same time? If I have to, I should at least be offered an option. Because the hardware cannot work without software? Well, IE cannot work without the operating system either. Just before Bush was elected they were about to split Microsoft into two companies. That's how serious they were about it. Then MS gave Bush some money and, after he was elected, they just made MS install Windows in some schools for free, which in the end might be good for Microsoft, and turned the MS loss into a win. But, that is a separate issue. The point is, if Bush was not elected, they would have really split MS because of something as irrelevant as IE, which is free. Windows is not free and we are still forced to buy it, if we want to get the hardware we want. The same holds for desktops, but, luckily, there are a lot of companies that sell OS-free desktop hardware, and desktops are easy to assemble and I can just buy parts and do it myself. Laptops are a bigger problem. I will buy an HP, or Dell, or IBM as soon as I can get any of the hardware offered without Windows. Until then, I'll help the small businesses, if I need a laptop, and I will build my own desktops. Unfortunately, a lot of people are still buying Windows and erasing and companies like HP do not get the message. For example, I would like to buy a laptop with an Athlon 64. You put a link above to a laptop with this processor. I clicked on the link and, of course, it says MS Win XP Pro. If I want it, I have to buy Win XP Pro. I do want it, but I will not buy it. The first one of the big guys (HP, IBM, Dell etc), who will offer their hardware (all their hardware) Windows-free will have my everlasting loyalty. However, I don't see it happening soon. I will always, as long as the company exists, buy my desktop hardware from www.micronux.com . They offer great hardware and do not force me to buy Windows. I have this HP laptop, but the next time I buy a laptop, I'll buy a Sager or a ProStar. I hope they will grow and I appreciate what they are doing. And I will make sure I help them out. They are fair to us. They are willing to sell me hardware (just hardware)."
    author: "Fikret Skrgic"
  - subject: "Re: I had to pay the windows tax, but it would be "
    date: 2004-09-12
    body: "While I was disappointed in the outcome of the MS vs DOJ trial, I am seeing some serious FOSS competition that is making an impact. In a way, this I feel is better for the industry as a whole. \n\nThe fact that HP has released a Linux laptop is huge. This signifies that their research showed there *IS* a market for desktop Linux. The fact there is starting to be more and more support for Linux as a desktop OS from companies such as Novell, HP, and Nvidia is encouraging.\n\nI hope that even if HP finds that selling their own Linux on a laptop is not viable, they will continue to test their systems against Linux and provide an alternative toward Windows pre-installed (ie load up FreeDOS but offer drivers for Linux). \n\nWhile it still is very much in the early stages, I believe the pieces are starting to fall into place. If HP continues to support Linux and offer more systems with Linux preinstalled (or Linux certified) then other OEMs will have to react and provide similar offerings to stay competitive. \n\nWith more systems being offered with Linux, the user base increases. With it increased, software vendors will start to port their applications -- most likely starting with business/niche players and slowing rolling to more general use software (the general use players have the most to lose as there already is a lot of great FOSS in that segment). From there, it will have an appreciable small percentage of the market.\n\nAm I nuts? We'll see .. There still is a lot of ground to cover. Software patents, DRM issues, fear of the unknown/running back to the familiar, etc are still big hurdles. However, the fact that there is already a healty corporate backing for Linux and support options are expanding (multi-tiered, classes, etc..) is very positive. \n\nEither way, it is fun to kick back and watch the competition.. The tech industry is starting to get interesting again (truly feels like the first timne since the 90's..)"
    author: "John Alamo"
  - subject: "Re: I had to pay the windows tax, but it would be "
    date: 2004-09-14
    body: "I think apart from price issue there is an issue of teaming up for linux.\n\nExample: If I I had a choice between two comparable laptops one with M$ tax say for $1999 and one without for $1959 and had to buy PCMCIA card for $79 for the latter, I would still go for the one without tax. I'd rather support people who care about me joe Linux user."
    author: "Egor Kobylkin"
  - subject: "Re: I had to pay the windows tax, but it would be nice"
    date: 2004-09-15
    body: "Having to pay the Windows tax is bad in at least three ways:\n- first obviously because there is no way around (\"have to\"). This wouldn't be such a problem if you could easily sell the Windows licence or at least get a refund for returning it.\n\n- second, the money goes to someone who is not using it for anything of worth, I'd rather donate it to KDE, amnesty international or Greenpeace or spend it on beer :)\n\n- third, statistics count me as a Windows user  and my machine as a Windows installation, which is not very helpful considering that some (most?) managers rely on these statistics when considering market options.\nFor example I am a HP customer (I own a Omnibook 6100since 2002), but I am quite sure HP counts this machine as a part of their Windows market, they have no data to assume anything else.\n\n"
    author: "Kevin Krammer"
  - subject: "Mr"
    date: 2004-09-12
    body: "I haven't got the HP nx5000, but a laptop with a pre-installed Linux distribution would be my choice otherwise I would install SuSE anyway which is what I've done before on other laptops including a HP laptop which worked excellent even wireless worked out the box (without web encryption unfortunately)\n\nI exclusively work with SuSE Linux and KDE for many years and find it very robust and modern these days, almost everything one expects on a desktop is there and as for a server litterally everything is there.\n\nIn the Netherlands some local governments have entirely switched to a Linux desktop and there are more to come. also our educational system and ICT industry massively makes use of Linux as it perfectly matches to work with UNIX/Linux datacenters deployed.\n\nOur central government is also investigating OpenSource possibilities and holding conferences on a large scale.\n\nLinux and her X Window System & Managers helps to work cost effective, stable, modern and powerfull and makes sure that competitive OS manufacturers will not gain monopoly and OSs in general evolve faster.\n\nYou're probably just as enthusiastic about Linux as I am and are more curious to issues that need improvement on Linux, which is what I'll write now.\n\nMore games as that is what is holding back a lot of people.\nMore intensive trouble shooting on USB memory sticks/drives (copying large amounts of data causes the system to become instable on rare occasions.\n3D window animation functions in Xfree to give window managers a Sun looking glass look and feel which is very slick, productive and attractive.\nLinuxBIOS or OpenBIOS would be excellent (opportunity for HP) as that would make BIOS much more powerfull (think about Sun Ultra Sparc Workstation & Servers that allow to be remotely controlled by console (serial port) connected to a terminal server or a simple null modem cable, even if the OS isn't running.\n\nThat's it folks and Thanks for all the good stuf!"
    author: "Ron de Jong"
  - subject: "Will you support people who build device drivers?"
    date: 2004-09-14
    body: "I hate to interupt this love fest, but there is a slight problem.\n\nFor some reason, HP doesn't do this for their scanners.  They don't provide hardware information. Will this problem be addressed soon?\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Will you support people who build device drivers?"
    date: 2004-09-15
    body: "James,\n\ncan you please be more specific? Model? What kind of problem? \n\nThanks\n\nThomas"
    author: "Thomas Schneller"
  - subject: "Re: Will you support people who build device drivers?"
    date: 2004-09-16
    body: "If you check here:\n\nhttp://www.buzzard.me.uk/jonathan/scanners-usb.html\n\nyou will see that support for all non-SCL based HP scanners is rather poor and that what progress has been made was made by reverse engineering.\n\nThe scanner that I was looking at was the Scanjet 3970.\n\nI also note that I would be willing to pay an additional $20.00 for a version of it that was SCL and used a standard communication protocol or had a (direct) parallel port interface.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "HT support for new Compaq d530CMT"
    date: 2004-09-17
    body: "Hello,\n\nDoes anyone know where I can find information on\nenabling hyperthreading under Linux on a new Compaq\nd530?\n\nHT is turned on in the BIOS and I'm running an SMP\nkernel, but /proc/cpuinfo still shows only one CPU.\n\nDespite the machine being sold with Mandrake Linux,\nI've tried HP support but no-one knows Linux or\nwants to help.\n\nMany thanks,\n\nMitch."
    author: "Mitch Davis"
  - subject: "Re: HT support for new Compaq d530CMT"
    date: 2005-03-03
    body: "Hi! Kan you help me? I need the driver to my PC DF364#ABY, D530CMT thank for your help."
    author: "Walter Buendia"
  - subject: "Re: HT support for new Compaq d530CMT"
    date: 2005-03-03
    body: "Sorry, I can't help."
    author: "Mitch Davis"
  - subject: "Other models?"
    date: 2004-09-19
    body: "Hi. I would first like to extend my sincere thanks to the HP company\nfor adopting Linux and the Open Source idea. It says a lot.\n\nMy questions:\nIs Linux offered on any desktop systems currently?\nWill other Laptop models have Linux?\n\nA suggestion:\nIs it at all possible for HP to consider Gentoo Linux as well?\n(www.gentoo.org) Its unique because it is remarkably easy to install\na program. simply type 'emerge' followed by the programs package name\nin the Gentoo portage tree. The only thing that can be frowned at is\nthe ease of installation of Gentoo, so if it was preinstalled, users\nwould have even better use of the Linux operating system.\n\nThank you very much.\nIan\n\nPS> I just cant stress it enough, Thank you soo much to HP for doing all\nof this Linux work. With your assistance, Linux is expanded dramatically.\n"
    author: "Ian"
  - subject: "Gentoo Linux/ KDE on my nx7000"
    date: 2004-09-19
    body: "I personally run Gentoo Linux/KDE 3.3 on my nx7000 and i do not have to deal with the SuSE and Mandrake RMP hell.\n\nWhy Gentoo Linux? Because they have one of the best package management systems ever. \n\nhttp://www.gentoo.org\n\nKDE and HP, keep up the good work! \n\nCheers! "
    author: "step"
  - subject: "Does Linux work on other HP notebooks?"
    date: 2004-09-29
    body: "Dear Thomas,\n\ndid you try to install Linux on different HP notebooks than the nx5000? If yes, did the hardware inclusive modem and power management work? Can you recommend any other notebook than the nx5000 for Linux?\n\nThank you very much!"
    author: "Stefan Ludwig"
  - subject: "Re: Does Linux work on other HP notebooks?"
    date: 2005-02-24
    body: "I have an nx7000 and installed Fedora 2 and upgraded to Fedora 3 just a while ago, both are working perfectly. Hoever, if anyone could tell me how to get the screen resolution working at its proper rate (you know, widescreen...), then I would be really happy!\n\nThanks,\n\nJanos"
    author: "Janos Marki"
  - subject: "Re: Does Linux work on other HP notebooks?"
    date: 2005-02-25
    body: "I have configured once. I can find the file somewhere, but the trick is this:\n\nLook at the X logfile, and you will see that something ( I don{ t recall the name) is resetting your Horizontal frequency range. You have to disable that, then it works automatically.\n\nIf you email me (I am easy to find ;-) I will email you the file in a few days."
    author: "Roberto Alsina"
  - subject: "Great match for nx5000 with SuSE, but ..."
    date: 2005-12-07
    body: "Just installed and configured SuSE Linux 10.0 on my nx5000 machine with packages of my personal purpose.  Everything seems to work fine except one thing of wlan stuff..\nI found some information from http://ipw2100.sourceforge.net/ saying that I need to turn the wlan on using hardware RF switch, which is Fn+F2, but it does not show me the blue light for the indication.  Even when I type iwconfig on console, it's not configured.  I'm wondering how to make this work with my old buddy nx5000.  Any help..?"
    author: "Rick Choi"
  - subject: "nx5000, suse and pcmcia card?"
    date: 2006-02-14
    body: "I purchased an nx5000 and loaded suse 9.3 on it. It really works quite well. I can't say that in the 6 months that I've used it I've tested everything, but it seems to be functional, and I really like the KDE desktop environment. It seems to deal with updates (kernal 2.6 now) pretty well also.\n  I recently bought an HP SCR241 pcmcia smartcard reader, and I'll be darned if it isn't incredibly difficult to get it set up.  I wonder why everything else seemed so straightforward, and this is so strange. Do I have to buy a smartcard reader from someone other than HP to get my laptop to work with the cards?"
    author: "David"
  - subject: "Companies selling Linux Desktops and Laptops"
    date: 2006-08-29
    body: ".\n\nThe HP deal looked like a one-time offer.\n\nHere are companies selling pre-installed Linux Desktops and Laptops.\n\nhttp://lxer.com/module/forums/t/23168/\n\n.\n\n"
    author: "cyber_rigger"
---
<a href="http://www.hp.com/">Hewlett-Packard</a> made <A href="http://www.msnbc.msn.com/id/5831949/">quite a splash</A> when they announced that they are
offering Linux on the nx5000, one of the latest HP laptop models. HP also sponsored <a href="http://conference2004.kde.org/">aKademy</a>, the KDE Community World Summit as a <a href="http://conference2004.kde.org/sponsors.php">Platinum
sponsor</a>. This included a loan of 24 laptops for usage in the tutorial rooms
as well as a special deal for KDE contributors to buy the nx5000 model at a 
reduced price. The aKademy press team arranged a meeting with Thomas Schneller, Manager Software R&D and asked him about this venture from HP.


<!--break-->
  <style type="text/css">
  
.imgboxrt{
border:1px dotted #000;
float:right;
margin-left:5px;
margin-right:10px;
}

.imgboxlft{
border:1px dotted #000;
float:left;
margin-left:10px;
margin-right:5px;
}
  
  </style>

<p><IMG src="http://static.kdenews.org/fab/events/aKademy/pics/hplaptop/thomas.schneller.jpg "  align="right" border="0"></p>

<p><strong>Can you please introduce yourself?</strong></p>
    
    <p>My name is Thomas Schneller, Manager Software R&D of the HP EMEA Product Development Center based in Munich. 
    I was part of the Linux Core team which developed the first Linux pre-installation on a nx5000 Laptop.</p> 
    
    
    <p><strong>The KDE desktop contributed to the success of SUSE Linux and HP chose SUSE/KDE for this laptop. What 
    were the strong points for offering a KDE desktop to your customers?</strong></p>

    <p>We used the standard configuration which comes with SUSE Linux which is KDE. 
    We believe that KDE is a good choice as it offers basically everything needed on a today's desktop.</p>
    

    <p><strong>Do you give support for the nx5000 configured with a KDE/Linux configuration?</strong> </p>

    <p>Yes, there is a 90 days support for the pre-installed Linux software shipped on 
    that laptop which is similar to Windows where we offer the same support.</p> 



    <p><strong>Are there any plans for more models supporting Linux?</strong></p>
    
    <p>It depends on the market response to the nx5000 offer. If there is a need for Linux pre-installed 
    Laptops we will discuss how to support that. Currently we are also thinking to offer better support 
    for Linux in the first place.</p>
    
    
    
   <p><strong>Right now the nx5000 is not available in Europe. Do you have any plans to sell them anywhere other than the US?</strong></p>

    <p>It is true; right now the nx5000 is only available in USA. It would have been too much effort to offer it 
    for Europe as well as there are so many languages and localization options to take care of. But European 
    customers are welcome to buy it in the USA if they want. It can be ordered on the 
    <a href="http://h71016.www7.hp.com/dstore/MiddleFrame.asp?page=config&ProductLineId=430&FamilyId=1776&BaseId=12514&oi=E9CED&BEID=19701&SBLID=&AirTime=False" target="_blank">HP website.</a> 
    Just choose Linux as OS and the Integrated HP W500 wireless option. 
    Drivers for that device are pre-installed. The Intel 2200BG driver was not available at the 
    time we released the pre-installation. </p> 

    

<p><strong>Will you support people who build device drivers and will you be giving them hardware information?</strong></p>

    <p>Yes, we already work with the open source community. As an example have a look at <a href="http://opensource.hp.com"> HP & Open Source </a> where several Open Source projects are hosted.</p>
     

     
   <p><strong>Does power management and the winmodem work on the nx5000 as they do under Microsoft Windows?</strong></p>

    <p>Yes, basically all hardware is working. ACPI is fully supported, so hibernating your Linux laptop is 
    possible and also the winmodem works on this model. I also want to stress the fact that we welcome any feedback 
    as we are eager to hear people's experiences with our product.</p>

<!--IMAGE BOX LEFT-->
<div align="center" class="imgboxlft" style="width:340px;">
<p><a href="http://static.kdenews.org/fab/events/aKademy/pics/hplaptop/HPdesktop.png"><img src="http://static.kdenews.org/fab/events/aKademy/pics/hplaptop/HPdesktop_small.png" alt="K3B and Kopete running" /></a><br />
K3B and Kopete running...</p>
</div>
<!--/IMAGE BOX LEFT-->

    <p><strong>Did you have some problems of getting hardware components to work?</strong></p>
    
    <p>We carefully choose the target platform for that project. We wanted to make sure everything just works. 
    The only problems we had during development were caused by pieces of hardware where detailed specs were not available to us.</p> 

    
    
   <p><strong>Will this only work on SUSE or do you have any plans to support more distributions. 
   Does HP have any plans to better support Linux in general?</strong></p>

    <p>Of course other distributions are free to benefit from the work we did. There are plans to distribute BIOS in a Linux-friendly format for selected Models. For the nx5000 this kind of BIOS is still available:
<a href="http://h18007.www1.hp.com/support/files/hpcpqnk/us/locate/1115_5930.html">ROMPaq for HP Notebook System BIOS (68BCU ROM) Linux-Based</a>.
You can just download it onto your Linux desktop, unpack it and run a script which will ask, if you want to create 
a bootable diskette or a bootable CD. Then you only need to reboot with the bootable media and can flash your BIOS easily. 
Plans to collect and offer Linux pre-installation instructions and white papers on an open source basis are discussed as well. </p>
    

      
    
    <p><strong>Did you write any additional software for the nx5000 project? Any plans maybe on doing that?</strong></p>
    
    <p>Well any needed software we were looking for was already provided by the SUSE Linux distribution like 
    for example the power management applet. So we did not need to write additional software.</p>

    
    
    <p><strong>Any ideas of working together with KDE developers with this. How about a KControl module 
    for usage in KControl?</strong></p>
    
    <p>HP will support initiatives which will enrich the desktop experience on our laptops. If you or 
    your project has an interesting piece of software please let us know.</p>
    
    <!--IMAGE BOX RIGHT -->
<div align="center" class="imgboxrt" style="width:340px;">
<p><a href="http://static.kdenews.org/fab/events/aKademy/pics/hplaptop/HPdesktop2.png"><img src="http://static.kdenews.org/fab/events/aKademy/pics/hplaptop/HPdesktop2_small.png" alt="HP desktop with DVD player LinDVD" /></a><br />
Watching DVD's with LinDVD...</p>
</div>
<!-- IMAGE BOX RIGHT-->     


    
    
    <p><strong>Can you tell us about any experiences your customers have with the KDE desktop on these laptops?</strong></p>
    
    <p>Currently it is still a bit early but we are busy collecting feedback from all people who are using these 
    Linux laptops. We have a feedback loop installed for this. But at least the experience of testers are telling us, 
    <a href="http://www.msnbc.msn.com/id/5831949/"> we are on the right way.</a></p>

    <p><strong>You sponsored the KDE Conference with these laptops and you even decided to sell some of those laptops at a lower price 
    to our developers. What's the gain for HP for doing all of this?</strong></p>

    <p>HP was an official sponsor of the KDE Conference as we are sponsoring open source events in general. Also we hope 
    that by selling these laptops at a nice price that some KDE people will provide us some feedback. We as HP want to 
    improve Linux support for selected models and to do so, we need to understand the issues. We appreciate your feedback, 
    please don't hesitate to send us any problem you're having with the nx5000 you bought.</p>
    
    
      <p><strong>Is there anything else you want to mention?</strong></p>
    
    <p>Yes. Please post your comments below the article. I will read through them and answer any question appearing.</p>


    <p><strong>Thank you for answering these questions and also a big thank you for the support HP has given to KDE.</strong></p>

