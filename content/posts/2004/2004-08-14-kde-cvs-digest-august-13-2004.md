---
title: "KDE-CVS-Digest for August 13, 2004"
date:    2004-08-14
authors:
  - "dkite"
slug:    kde-cvs-digest-august-13-2004
comments:
  - subject: "Yeah! First comment!!!"
    date: 2004-08-14
    body: "Thanks, Derek!! I can't live without your digest!"
    author: "Dario Massarin"
  - subject: "Twice?"
    date: 2004-08-14
    body: "Why does this commit appears twice?\n\n--------------------------------------------------------------------------\n\"Carsten Burghardt committed a change to kdepim/kmail in KDE_3_3_BRANCH on August 06, 2004 at 05:40:56 PM\n\nBackport:\nDo not crash during startup. This patch is rather intrusive but I do not see a chance to fix it\nwithout\nrewriting some stuff.\nIt also fixes a bug that I noticed recently: when an imap folder is selected and you expand a\ndifferent\nimap folder the readerwin was cleared.\n\nView Source Code (3 Files)\""
    author: "John Redundancy Freak"
  - subject: "Re: Twice?"
    date: 2004-08-14
    body: "Hum...\n\nOne for HEAD and one for KDE_3_3_BRANCH :)"
    author: "DrazziB"
  - subject: "Re: Twice?"
    date: 2004-08-14
    body: "Opss right :)"
    author: "John Redundancy Freak"
  - subject: "Re: Twice?"
    date: 2004-08-14
    body: "One is in HEAD the other in KDE_3_3_BRANCH, which will end up in the release.\n\nDerek"
    author: "Derek Kite"
  - subject: "KDE3.3 has landed in debian/unstable!"
    date: 2004-08-15
    body: "apt-get update && apt-get dist-upgrade && have fun! :-)"
    author: "Yves"
  - subject: "Re: KDE3.3 has landed in debian/unstable!"
    date: 2004-08-15
    body: "Thanks for the tip:)  Just upgraded, and my, what i nice desktop KDE is. There are so many nice new little features all around.\n\nSome of the programs have not been upgraded yet, most noticably Kontact, and the kdewebdev programs.  Anyway, they will be there sooner or later.\n\nThanks KDE developers, and debian packagers!\n\n-Sam"
    author: "Samuel Stephen Weber"
  - subject: "Re: KDE3.3 has landed in debian/unstable!"
    date: 2004-08-15
    body: "I noticed this with 3.2 also, it was released in Debian unstable before even kde.org had released it. Very nice hehe!\n\nGreat work everyone, thanks for everyone who put effort into this and anyone who helped getting to 3.3 in the first place. We have no one to thank but ourselves.. Onto the next release, yet again!"
    author: "ac"
  - subject: "Re: KDE3.3 has landed in debian/unstable!"
    date: 2004-08-15
    body: "Lets pray it makes it into testing and thereby stable real soon now. :-)\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: KDE3.3 has landed in debian/unstable!"
    date: 2004-08-16
    body: "casa:~# apt-get install\nReading Package Lists... Done\nBuilding Dependency Tree... Done\nYou might want to run `apt-get -f install' to correct these.\nThe following packages have unmet dependencies:\n  kdepim: Depends: korganizer (>= 4:3.2.3-1) but it is not installed\nE: Unmet dependencies. Try using -f.\ncasa:~# apt-get -f install\nReading Package Lists... Done\nBuilding Dependency Tree... Done\nCorrecting dependencies... Done\nThe following extra packages will be installed:\n  korganizer\nThe following NEW packages will be installed:\n  korganizer\n0 upgraded, 1 newly installed, 0 to remove and 0 not upgraded.\n1 not fully installed or removed.\nNeed to get 0B/1650kB of archives.\nAfter unpacking 4342kB of additional disk space will be used.\nDo you want to continue? [Y/n]\n(Reading database ... 27100 files and directories currently installed.)\nUnpacking korganizer (from .../korganizer_4%3a3.2.3-1_i386.deb) ...\ndpkg: error processing /var/cache/apt/archives/korganizer_4%3a3.2.3-1_i386.deb (--unpack):\n trying to overwrite `/etc/kde3/khotnewstuffrc', which is also in package kdelibs-bin\ndpkg-deb: subprocess paste killed by signal (Broken pipe)\nErrors were encountered while processing:\n /var/cache/apt/archives/korganizer_4%3a3.2.3-1_i386.deb\nE: Sub-process /usr/bin/dpkg returned an error code (1)\ncasa:~# "
    author: "korganizer hang on debian unstable"
  - subject: "Re: KDE3.3 has landed in debian/unstable!"
    date: 2004-08-18
    body: "well, guess its time for a bugreport to the debian packager ;-)"
    author: "superstoned"
  - subject: "KRITA ressurection"
    date: 2004-08-15
    body: "Nice to see krita evolving so fast. \nsome time ago i thought that it was another dead subproject of KOffice.\n\nMy best wishes to the developers.  "
    author: "SHiFT"
  - subject: "Re: KRITA ressurection"
    date: 2004-08-15
    body: "What subproject is dead? Even KPlato progresses every two months or so."
    author: "Anonymous"
  - subject: "Re: KRITA ressurection"
    date: 2004-08-15
    body: "KPlato is not dead? Hmm, nice to hear!"
    author: "Anonymous"
  - subject: "Re: KRITA ressurection"
    date: 2004-08-18
    body: "It is going forward, even if not very fast.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: KRITA ressurection"
    date: 2004-08-18
    body: "I suppose that he has meant Kontour.\n\nHave a nice day!"
    author: "Nicolas Goutte"
---
Enjoy <a href="http://cvs-digest.org/index.php?issue=aug132004">this week's KDE CVS-Digest</a>:
Mostly bugfixes this week, including fixes for the 3.3 release.
<a href="http://digikam.sourceforge.net/">Digikam</a> implements EXIF based rotation in image editor.
<a href=" http://www.koffice.org/krita/">Krita </a> adds gradient support.
<a href="http://www.koffice.org/">KOffice</a> can now save styles to <a href="http://www.oasis-open.org/committees/tc_home.php?wg_abbrev=office">OASIS</a>.  
<a href="http://dot.kde.org/1092237029/">Security fixes</a> in <a href=" http://www.konqueror.org/features/browser.php">khtml</a>.


<!--break-->
