---
title: "The People Behind KDE: Zack Rusin"
date:    2004-03-07
authors:
  - "Tink"
slug:    people-behind-kde-zack-rusin
comments:
  - subject: "Nice!"
    date: 2004-03-07
    body: "Very nice reading!\n\nThanks,\nDavid\n"
    author: "David"
  - subject: "The most difficult question"
    date: 2004-03-07
    body: "Isn't it \"How much wood ... if a woodchuck *would* [not could] chuck wood\"?"
    author: "Frerich Raabe"
  - subject: "Re: The most difficult question"
    date: 2004-03-07
    body: "No, Zack got it right."
    author: "David Walser"
  - subject: "Re: The most difficult question"
    date: 2004-03-07
    body: "Well I alos learned it with would at school... But it's a german school. :D"
    author: "Beefy"
  - subject: "Re: The most difficult question"
    date: 2004-03-07
    body: "You know, that\u00b4s not too hard to answer :-)\n\nSince we rely on the hypothetical \"if he could chuck wood\", the answer is trivial: about as much wood as we assume he could."
    author: "Roberto Alsina"
  - subject: "sweet interview"
    date: 2004-03-07
    body: "dug the interview Zack, almost as much as i enjoy your code. though without the dreds, i dunno, it'll take some getting used to before i can ever think of you the same way again ;-) \n\nTink: you have no idea how great it is to see these every week again. makes my weekends more happy. thanks! =)\n\nluv 'n cookies! Aaron."
    author: "Aaron J. Seigo"
  - subject: "Re: sweet interview"
    date: 2004-03-07
    body: "> Tink: you have no idea how great it is to see these every week again. makes my weekends more happy. thanks! =)\n\nI love it too! Thanks Tink!!"
    author: "anon"
  - subject: "Nice"
    date: 2004-03-07
    body: "LOL @ the hugging. Monkeys and dinasours together :)\n\nI also love the quote \"To look up to someone else is to look down on yourself\". I nver really thought about it, but it is so true."
    author: "Alex"
  - subject: "kde apps with G"
    date: 2004-03-07
    body: "We already have Gideon aka KDevelop 3.0 :-)\n\nGreetings from Linuxtag Chemnitz\nAlex\n"
    author: "Alexander Neundorf"
  - subject: "Re: kde apps with G"
    date: 2004-03-07
    body: "don't forget Gwenview :)"
    author: "gamed gedan"
  - subject: "Who do you admire? Why?"
    date: 2004-03-10
    body: "Alright people, grammar! It's \"Whom do you admire?\""
    author: "LuckySandal"
  - subject: "Re: Who do you admire? Why?"
    date: 2007-10-03
    body: "well, to tell you the truth i don't admire anyone because the world is full of lot of false liars, and disrespectful people, i don't care about no grammar, i talka how may talka, ya ca change um, if ya wanna change um you can come!! (i had problems right this some of the words maybe seplt wrong but atleast i am trying.)"
    author: "daisy"
---
For this week's <a href="http://www.kde.nl/people">People Behind KDE</a> interview we stay on the North American continent, cross the border and travel down to Philadelphia. The guy I'm interviewing this week is remembered among his friends for trying to enforce a passionate relationship on a Ximian person, sleeps with his laptop and is one of KDE's most outgoing developers. The man who cut his <a href="http://ktown.kde.org/~binner/NoveHrady2003/Day0/dscf0056.jpg">famous dreadlocks</a> and emerged with a clean crew cut, it's <a href="http://www.kde.nl/people/zack.html">Zack Rusin</a>!



<!--break-->
