---
title: "FedoraNEWS.ORG: KDE 3.2 Installation and Review, K3b Tutorial"
date:    2004-02-05
authors:
  - "binner"
slug:    fedoranewsorg-kde-32-installation-and-review-k3b-tutorial
comments:
  - subject: "Get KDE Script"
    date: 2004-02-05
    body: "Just copy this into a file.. make it executable and then \n\ncommand ftp://ftp.site.org/pathtokde/  dont forget trailing slash..\n\nit will download all the packages for you.  I made this for Fedora Core 1.\n\nwget $1arts-1.2.0-0.1.i386.rpm && wget $1arts-devel-1.2.0-0.1.i386.rpm && $1kdeaccessibility-3.2.0-0.1.i386.rpm && wget $1kdeaddons-3.2.0-0.1.i386.rpm && wget $1kdeaddons-atlantikdesigner-3.2.0-0.1.i386.rpm && wget $1kdeadmin-3.2.0-0.1.i386.rpm && wget $1kdeartwork-3.2.0-0.1.i386.rpm && wget $1kdeartwork-icons-3.2.0-0.1.i386.rpm && $1kdebase-3.2.0-0.1.i386.rpm && wget $1kdebase-devel-3.2.0-0.1.i386.rpm && wget $1kdebindings-3.2.0-0.1.i386.rpm && wget $1kdebindings-devel-3.2.0-0.1.i386.rpm && wget $1kdeedu-3.2.0-0.1.i386.rpm && wget $1kdeedu-devel-3.2.0-0.1.i386.rpm && wget $1kdegames-3.2.0-0.1.i386.rpm && wget $1kdegames-devel-3.2.0-0.1.i386.rpm && wget $1kdegraphics-3.2.0-0.1.i386.rpm && wget $1kdegraphics-devel-3.2.0-0.1.i386.rpm && wget $1kdelibs-3.2.0-0.1.i386.rpm && wget $1kdelibs-devel-3.2.0-0.1.i386.rpm && wget $1kdemultimedia-3.2.0-0.1.i386.rpm && wget $1kdemultimedia-devel-3.2.0-0.1.i386.rpm && wget $1kdenetwork-3.2.0-0.1.i386.rpm && wget $1kdenetwork-devel-3.2.0-0.1.i386.rpm && wget $1kdepim-3.2.0-0.1.i386.rpm && wget $1kdepim-devel-3.2.0-0.1.i386.rpm && wget $1kdesdk-3.2.0-0.1.i386.rpm && wget $1kdesdk-devel-3.2.0-0.1.i386.rpm && wget $1kdetoys-3.2.0-0.1.i386.rpm && wget $1kdeutils-3.2.0-0.1.i386.rpm && wget $1kdeutils-devel-3.2.0-0.1.i386.rpm && wget $1kdevelop-3.0.0-0.1.i386.rpm && wget $1md5sum && wget $1qt-3.2.3-0.2.i386.rpm && wget $1qt-MySQL-3.2.3-0.2.i386.rpm && wget $1qt-ODBC-3.2.3-0.2.i386.rpm && wget $1qt-PostgreSQL-3.2.3-0.2.i386.rpm && wget $1qt-designer-3.2.3-0.2.i386.rpm && wget $1qt-devel-3.2.3-0.2.i386.rpm && wget $1quanta-3.2.0-0.1.i386.rpm && wget $1quanta-devel-3.2.0-0.1.i386.rpm && wget $1redhat-artwork-0.90-0.1.i386.rpm"
    author: "Corey"
  - subject: "Re: Get KDE Script"
    date: 2004-02-05
    body: "What's wrong with \"wget ftp://ftp.site.org/pathtokde/*\"?"
    author: "Anonymous"
  - subject: "Re: Get KDE Script"
    date: 2004-02-05
    body: ">What's wrong with \"wget ftp://ftp.site.org/pathtokde/*\"?\n\nit is not kwget?\n\n\njust kidding.\ncould not help myself."
    author: "a.c."
  - subject: "No Juk"
    date: 2004-02-05
    body: "I've installed the kde3.2 packages from the fedora's web site and can not find juk. I believe it is supposed to be in the kdemultimedia package, but an rpm -ql of the package does not show a binary, nor does 'find / -name juk' or 'slocate juk' - after an 'updatedb'. There is juk help files installed. \n\nOtherwise, KDE 3.2 is the best desktop for linux, hands down."
    author: "ac"
  - subject: "Re: No Juk"
    date: 2004-02-05
    body: "Wild guess:  Could it be because Fedora does not support mp3?"
    author: "ac"
  - subject: "Re: No Juk"
    date: 2004-02-05
    body: "No.. I talked to the packager, and it's just a known bug in fedora's kdemultimedia rpms. Wait for the next rebuild which should come any day now. "
    author: "anon"
  - subject: "Re: No Juk"
    date: 2004-03-09
    body: "Get kdemultimedia from http://www.eleceng.ohio-state.edu/~ravi/kde/kdemultimedia-mp3-3.1.4-1.i386.rpm\n\nthis enables mp3 support in kdemultimedia.\nbut then you should rebuild kdebase if you want konqueror to rip cd's into mp3 files using the audiocd protocol."
    author: "Enso"
  - subject: "Re: No Juk"
    date: 2005-05-20
    body: "I can't find this file at  http://www.eleceng.ohio-state.edu/~ravi/kde/kdemultimedia-mp3-3.1.4-1.i386.rpm, can anyone help me to get it..."
    author: "Edison"
  - subject: "Re: No Juk"
    date: 2004-04-27
    body: "Since it doesn't look like this has happened as of 3.2.2 -- \n\nJuk depends on 'taglib', for which currently isn't available as a Fedora package.\n\nIf you get the SuSe taglib source rpm -- I used taglib-1.0-0.src.rpm from some KDE mirror in /pub/kde/stable/3.2.2/SuSE/src/ -- and build it \n\nrpmbuild --rebuild taglib-1.0-0.src.rpm\n\nYou get the usual taglib/taglib-devel pair.\n\nIf you install them\n\nrpm -Uvh taglib-1.0-46.i386.rpm taglib-devel-1.0-46.i386.rpm\n\nYou can then grab the current Fedora kdemultimedia source rpm and rebuild it.\n\nYou'll get -- a couple hours later -- kdemultimedia rpms that have a working Juk."
    author: "Graydon"
  - subject: "Re: No Juk"
    date: 2004-04-27
    body: "Whups!\n\nYou want the taglib source rpm from http://some_kde_mirror/pub/kde/stable/<b>3.2.1</b>/SuSE/src/\n\nthe 3.2.2 version has a lot of added SuSE package dependencies, and will complain bitterly."
    author: "Graydon"
  - subject: "Re: No Juk"
    date: 2004-02-09
    body: "What do you need JuK for? Try noatun with the hayes playlist."
    author: "Coolvibe"
  - subject: "No JuK in Slackware 9.1"
    date: 2004-02-09
    body: "May be KDEMultimedia package doesn't include Juk :(. and Slackware is excellent at Multimedia...Perfomance wise also."
    author: "Slackware 9.1 User"
  - subject: "Re: No Juk"
    date: 2005-03-02
    body: "there is a rpm for JUK now still in testing though try thid link\n\n\n\nhttp://rpm.pbone.net/index.php3/stat/4/idpl/973041/com/juk-1.1-0.fdr.2.1.i386.rpm.html\n"
    author: "BOSS DOSS"
  - subject: "Cant start kicker"
    date: 2004-02-05
    body: "Upgraded on Fedora Core1 to 3.2\nNo kicker now.\nAt all (\n\nstarting kicker manually gives no messages, hangs for 4-5 ses and ends.\n\nanyone?\n"
    author: "Zepplock"
  - subject: "Re: Cant start kicker"
    date: 2004-02-05
    body: "Actually, it's a new feature. Don't you like it??"
    author: "ac"
  - subject: "Re: Cant start kicker"
    date: 2004-02-06
    body: "what messages appear if you run kicker from a konsole session?"
    author: "Aaron J. Seigo"
  - subject: "Re: Cant start kicker"
    date: 2004-02-06
    body: "no messages at all"
    author: "Zepplock"
  - subject: "Re: Cant start kicker"
    date: 2004-02-06
    body: "strace kicker\nattached"
    author: "Zepplock"
  - subject: "Re: Cant start kicker"
    date: 2004-02-07
    body: "hrm.. seems to crash after expecting to find a .desktop file that doesn't exist (for kscd, to be exact)... could you:\n\n1. run kicker through gdb and get a backtrace\n2. package up all the kicker related rc files in ~/.kde/share/config\n3. email all this too me: aseigo at kde dot org\n\nthanks =)\n\np.s. don't run KDE as root."
    author: "Aaron J. Seigo"
  - subject: "Re: Cant start kicker"
    date: 2004-02-07
    body: "But root users need desktops too!\n\nSeriously, though...not that I exist as root all the time, but sometimes it is useful to run a window manager/desktop environment as root.  Is it _so_ dangerous with KDE?  Should I just stick with TWM?"
    author: "Robert Morrison"
  - subject: "Re: Cant start kicker"
    date: 2004-02-07
    body: "Either have it useful or safe and secure. You can't have both."
    author: "Anonymous"
  - subject: "Re: Cant start kicker"
    date: 2004-02-09
    body: "Thanks ever so much, but that is what I would call a \"kneejerk\" response containing no useful information, and not answering the question.\n\nTo clarify - is there a known risk to a system by running KDE as root, or is it the normal (and generally sensible) standard response, \"don't run anything as root, except when you have to, and even then don't do it\"?"
    author: "Robert Morrison"
  - subject: "Re: Cant start kicker"
    date: 2004-02-06
    body: "anyone?"
    author: "Zepplock"
  - subject: "where is Ksim?"
    date: 2004-02-05
    body: "where is Ksim?"
    author: "free will"
  - subject: "Re: where is Ksim?"
    date: 2004-03-01
    body: "/menu/panel/add/ksim"
    author: "dima"
  - subject: "KDE team outdoes itself!"
    date: 2004-02-05
    body: "Yeah even after the massive speed increases in 3.1 the KDE team does it again and now 3.2 is even faster! Plug that feather in your cap team; apart from all the other brilliant improvements and features in 3.2 the fact that it's markedly faster than 3.1 is a great accomplishment. Congratulations!"
    author: "snig"
  - subject: "or use kde-redhat RPMS"
    date: 2004-02-06
    body: "Or you can just use apt/yum to install the kde-3.2 RPMS now available (in the unstable section) from kde-redhat (http://kde-redhat.sf.net/).\n\n-- Rex"
    author: "Rex Dieter"
  - subject: "kio_smb problem"
    date: 2004-02-13
    body: "I installed it. Now I can't open smb:// sites. klauncher says it cannot initialize kio_smb... :("
    author: "Alvils"
  - subject: "Re: kio_smb problem"
    date: 2004-03-05
    body: "Same thing here....\n\nI'm getting the error on all redhat 9.0 boxes I've setup using kde3.2\n"
    author: "Flip"
  - subject: "wget"
    date: 2004-02-26
    body: "Corect me if I'm wrong, but I think wget will not do *, you need the recursive flag which can be messy without some sort of filter.  You can do this by file extension as follows:\n\nwget -r --accept=.rpm http://path/to/kde32"
    author: "Kevin"
---
<a href="http://fedoranews.org/">FedoraNEWS.org</a> features a <a href="http://fedoranews.org/krishnan/tutorial/kde3.2/">Quick Introduction to KDE 3.2 Installation</a> and a positive <a href="http://fedoranews.org/krishnan/review/kde3.2/">Review of KDE 3.2</a>: "<i>My first impression is 'wow'.</i>" The author confesses that he hadn't the time to review every <a href="http://www.kde.org/announcements/changelogs/changelog3_1_5to3_2.php">improvement and new application of KDE 3.2</a> yet but concludes "<i>To sum it up, with so many enhancements, an upgrade is worth your time.</i>" Besides <a href="http://fedoranews.org/colin/fnu/">news</a>, HowTos, reviews and <a href="http://fedoranews.org/krishnan/tips">tips</a> FedoraNEWS.ORG also offers tutorials like this nice <a href="http://fedoranews.org/mweber/multimedia/k3b">Flash tutorial how to use K3b</a> to burn CDs and DVDs.



<!--break-->
