---
title: "Quickies: KDE Banner, AVM Software, KuickShow"
date:    2004-01-15
authors:
  - "fmous"
slug:    quickies-kde-banner-avm-software-kuickshow
comments:
  - subject: "ATI=Qt NVIDIA=GTK"
    date: 2004-01-14
    body: "Interesting how ATI is using Qt as it's widget set and NVIDIA is using GTK. Looks like the Video Hardware war is coming to X11 tookit war as well! :)"
    author: "anon"
  - subject: "Re: ATI=Qt NVIDIA=GTK"
    date: 2004-01-15
    body: "Well, it's all text files so there is no reason that a Qt front-end cannot be written for Qt and if it is successful it *will* be supported. I think a lot of companies have bought FUD about Qt. Once they see otherwise they will come round. If we get a lot of things right I see a very bright future for KDE to be honest. Our comunity has the workable business models."
    author: "David"
  - subject: "Re: ATI=Qt NVIDIA=GTK"
    date: 2004-01-15
    body: "\"Qt front-end written for Qt\"?\n\nWow, there's a thought. This should have been Qt front-end written for nVidia. I'm a bit punchy this morning."
    author: "David"
  - subject: "Re: ATI=Qt NVIDIA=GTK"
    date: 2004-01-15
    body: "\"Qt front-end written for Qt\"?\n\nSure, why not? First we get Qt bindings for GTK+, then not to be outdone, someone writes GTK+ bindings for Qt. Next thing you know you can write a Qt front-end for Qt!"
    author: "David Johnson"
  - subject: "Re: ATI=Qt NVIDIA=GTK"
    date: 2004-01-15
    body: "Using Qt only makes sense for ATI. You get seemless platform independence on your configurator. All you have to do is port your drivers. how hard can that be? :) seriously though, that helps out a bit and makes it easier for them to support other platforms since all users will be starting at the same interface when calling up tech support."
    author: "Will Stokes"
  - subject: "Re: ATI=Qt NVIDIA=GTK"
    date: 2004-01-15
    body: "I don't really see how it only makes sense for ATI. nVidia have Windows and Linux drivers - no? Maybe nVidia should take a leaf out of their book."
    author: "David"
  - subject: "Re: ATI=Qt NVIDIA=GTK"
    date: 2004-01-15
    body: "I should have written \"it only makes since THAT\" and instead of \"only makes since FOR.\" Yes, I agree. I think all closed source configuration utilities should be written using Qt or a similar cross platform indepdent tool. Then again I'm quite partial to Qt. :)"
    author: "Will Stokes"
  - subject: "Banner"
    date: 2004-01-14
    body: "Well It's nice (crystal K icon) -- but quite plain.\n\noh how i wish i could see it in person :(\n\n//standsolid//\n\n"
    author: "standsolid"
  - subject: "Re: Banner"
    date: 2004-01-14
    body: "Hehehe, its much more impressive seeing it scaled to about 2 meters wide :)\n\nThe goal was that it should be a step up from our past banners and simple enough that it can be used for other shows.\n\nMeanwhile I have to admit I have NO artistic talent and it took me almost 2 months to finally get the correct artwork for the minimal graphic we do have.  Needless to say its done ;)\n\nCheers\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser "
  - subject: "AVM"
    date: 2004-01-15
    body: "Hey -  that's really nice to see a commercial company\nwrite GPL'd KDE apps. And they create very good products AFAIK. Never\nhad any complaints and they reguarly win tests at PC mags. \nBTW: I agree with Carsten - a mobile phone with most frequently called\nfunction would be really useful. Why didn't I think of this before?\n\n*Offtopic!* Some fun fact for german-speaking readers:\nGerade habe ich gesehen, dass SuSE das Paket \"frozen bubble\"\nmit \"gefrorene Blase\" \u00fcbersetzt hat. Hahaha! Und das im Winter... Aua ;-)\n"
    author: "Mike"
  - subject: "Re: AVM"
    date: 2004-01-15
    body: "frozen bladder? ;-("
    author: "Thomas"
  - subject: "banner"
    date: 2004-01-15
    body: "We need a new KDE logo, the Kgear is now dated. The KDE artists really need to match the competition. I like quartz's stuff. I wish he could polish up the icons as well. crystal is great, but for small icons, it sucks. OT, plastik has managed to get rid of the ugly black borders on qt widgets. it should be made default for now until someone does an original kde style. I dont use gnome, but I check it out from time to time just to see how cool it looks. And where is the howto for kde styles, maybe i can do it myself, rather than complain."
    author: "kerio"
  - subject: "OT: Plastik vs. Keramik."
    date: 2004-01-15
    body: "As has been said many times before, the default theme won't be changed. \n\nAnd even if I'm in the minority, I'm very happy with that. Last week I finaly came around to updating kdeartwork, and test plastik for myself, to see what everybody was so pleased about. \nBut the impression I got from the screenshots didn't change after using it for some hours. It still reminds me of the arcitecture of the 60's, plain, gray, rectangular, without any intressting points, or with Homer Simpsons words: BORING. I don't think it is in any way modern, it has, on the contrary, a rather outdated feeling (Just a personal feeling). \nWell, but I guess it's just a matter of taste, and as wiser people than me said, \"Taste can't be quarreled about\".\n\nFabio"
    author: "Fabio"
  - subject: "Re: OT: Plastik vs. Keramik."
    date: 2004-01-15
    body: "good for you. I have attached a screenie of keramic to show you just how inconsistent the borders are. in all I count six border line types. all lit from different light sources with conflicting shadows."
    author: "kerio"
  - subject: "Re: OT: Plastik vs. Keramik."
    date: 2004-01-15
    body: "So what?\nI mean it is not that a DE is a natural surface which is bound to natural lightning, (It does not even have a relief, or light for that matter). As you noticed, it are border lines which have a different type. They are there to separate content, not to give any hight information, since there is no hight in the GUI.\n\nAnd looking at your screenshot, your comment is plainly wrong.  All \"lights\" are at the upper left corner. Especially #1 and #2 are correct, since you have to look onto the whole widget (the white area). #5 symbolises a narrow channel, so its correct, too. #4 looks a bit strange in this shot, but the whole impression and lightnig is correct. #6 even has a gradient light and shadow, but has an additional thin black line surrounding it.\nYou could dispute that the white and black lines which symbolise light and shadow should be a gradient, for a better relief illusion, but i guess that would be a bit overkill.\n\nFabio\n"
    author: "Fabio"
  - subject: "Re: banner"
    date: 2004-01-15
    body: "> original kde style\n\nERM, plastik is already 100% original (to KDE)"
    author: "anon"
  - subject: "Re: banner"
    date: 2004-01-15
    body: "it was derived from a commercial 'alloy' theme for java-swing. \n"
    author: "SHiFT"
  - subject: "Re: banner"
    date: 2004-01-15
    body: "do you have any proof of this?"
    author: "anon"
  - subject: "Re: banner"
    date: 2004-01-15
    body: "Look at it:\nhttp://www.incors.com/lookandfeel/screenshots.php\n\nThey're pretty similar."
    author: "CK"
  - subject: "Re: banner"
    date: 2004-01-15
    body: "I don't think so. Where did you get this information from?"
    author: "ac"
  - subject: "Re: banner"
    date: 2004-01-15
    body: "No, the \"Alloy\" style was derived from the commercial theme. Plastik is original.\n"
    author: "Rayiner Hashem"
  - subject: "Re: banner"
    date: 2004-01-15
    body: "The look of Plastik is somewhat derivative of Alloy, but the code is not derivative at all. Compare the code. It's very different other than the necessary similarities all style plugins will have."
    author: "David Johnson"
  - subject: "Re: banner"
    date: 2004-01-15
    body: "\"[Plastik] should be made default for now until someone does an original kde style.\"\n\nThe default theme cannot be derivative of the look for another desktop. KDE needs to have a unique appearance, and not be a visually reminiscent of CDE, NeXT, Aqua, QNX, etc. While Keramik is not my favorite style, it definitely is unique. The old Highcolor default, while very simple, is also unique. Unfortunately Plastik is not, no matter how great it looks. It is very derivative (in look, not code) to INCORS Alloy. It's been softened a bit, and tempered with some WinXP styling, but a comparison of the styles shows the relationship.\n\nJust about everything out there is derivative of someone else's look. The number of Aqua and XP inspired styles on kde-look is amazing. I decided to do a new style that was completely orginal, since every style I've done was derivative of something else. It's very hard to do, because your subconscious keeps interjecting stuff from other desktops. Or from the real world. After all, why should pixels drawn on a glass screen resemble brushed metal or globs of jelly or the television remote control?\n\nLike it or not, Keramik is original. While I don't like it as the default, that's only my personal opinion. Instead of throwing it away, how about fixing the minor inconsistancies?\n\n\"And where is the howto for kde styles, maybe i can do it myself, rather than complain.\"\n\nI wrote a HOWTO for KDE window decorations, so I know how much work this is. Doing one for KDE/Qt styles would be at least five times as much work, because there is so much more ground to cover."
    author: "David Johnson"
  - subject: "KuickShow"
    date: 2004-01-15
    body: "KuickShow, IMHO is complicated\n\nI like ShowImg, but it works a little slowly\nhttp://www.kde-apps.org/content/show.php?content=9873\n"
    author: "andy"
  - subject: "Re: KuickShow"
    date: 2004-01-15
    body: "on my p3 500 kuickshow flickers a lot even with preloading\nturned on. also, i can't configure the layout to my liking.\n\nshowimg is more configurable and does not flicker.\n\n"
    author: "jasper"
  - subject: "Re: KuickShow"
    date: 2004-01-15
    body: "> KuickShow, IMHO is complicated\n\nCan you be a bit more precise?\n\nThanks,\nCarsten"
    author: "Carsten Pfeiffer"
  - subject: "Re: KuickShow"
    date: 2004-01-16
    body: "Well, talking only for me, I think the preferences look a bit convoluted (at least in KDE version 3.1.5). \nFor example they feature entries that are not obvious to your average computer user, like Dither in HiColor (15/16bit) modes etc in the General section. At least a popup explanation should be provided. Also why not separate the viewer shorcut and the browser shortcut preferences into own sections like so many other KDE programs do? Furthermore, is there a reason why the program uses horizontal tabbed preferences and not vertical ones? \nAlso, I think that the context menu is a bit overloaded and features too many entries. What about organizing them in sub-menues and adding icons for faster visual navigation?\nFinally, what about giving the \"Start slide show\" button in the toolbar a more prominent position? E.g. adding it on the bottom of the window that opens first when starting kuickshow? I think that for many people it is not immediatly obvious how to start the slide show.\nThanks otherwise for this nice application!"
    author: "Tuxo"
  - subject: "Re: KuickShow"
    date: 2004-01-16
    body: "> For example they feature entries that are not obvious to your average \n> computer user, like Dither in HiColor (15/16bit) modes etc in the General \n> section. At least a popup explanation should be provided.\n\nYes, I think I can remove those options entirely. Or at most provide a slider \"Quality -- Speed\".\n\n> Also why not separate the viewer shorcut and the browser shortcut preferences \n> into own sections like so many other KDE programs do?\n\nWhen I wrote this, it wasn't possible to do, but it seems, it now is. Will do.\n\n> Furthermore, is there a reason why the program uses horizontal tabbed \n> preferences and not vertical ones? \n\nBecause that was state-of-the-art in '98 when I wrote it ;-) No, actually there was some usability study by Macintosh people (I think I read about it at \"AskTog\") that a tabbar was easier to comprehend for new users, than those vertical sort-of tabs. (The Mac had those vertical tabs before the tabbar was invented).\n\nSo with that in mind, I saw no reason to switch to vertical tabs.\n\n> Also, I think that the context menu is a bit overloaded and features too many \n> entries. What about organizing them in sub-menues and adding icons for faster \n> visual navigation?\n\nYou mean the image-context menu, right? Agreed, it's a bit crowded, as people requested this and that entry. I'll look into optimizing it.\n\n> Finally, what about giving the \"Start slide show\" button in the toolbar a \n> more prominent position? E.g. adding it on the bottom of the window that \n> opens first when starting kuickshow? I think that for many people it is not \n> immediatly obvious how to start the slide show.\n\nYou mean a transparent widget covering part of the image? Or a button at the bottom of the filebrowser?\n\nThanks for your ideas,\nCarsten"
    author: "Carsten Pfeiffer"
  - subject: "Re: KuickShow"
    date: 2004-01-17
    body: "> Yes, I think I can remove those options entirely. Or at most provide a slider \"Quality -- Speed\".\nGreat, good idea!\n\n> Because that was state-of-the-art in '98 when I wrote it ;-) No, actually there was some usability study by Macintosh people (I think I read about it at \"AskTog\") that a tabbar was easier to comprehend for new users, than those vertical sort-of tabs. (The Mac had those vertical tabs before the tabbar was invented).\nThat's interesting. Well, I don't know why I like the vertical sort-of tabs more than horizontal ones. Maybe, because the latter are more clearly separated from the content and also allow for big pretty icons ;-) Also, many horzontal tabs lead to a crowded look, which is less the case with vertical sort-of tabs.\n\n> You mean the image-context menu, right? \nYes!\n\n> Agreed, it's a bit crowded, as people requested this and that entry. I'll look into optimizing it.\nGreat, thanks!\n\n> You mean a transparent widget covering part of the image? Or a button at the bottom of the filebrowser?\nI meant a button at the bottom of the file browser. A transparent widget would probably confuse many people. Also this button could be an ordinay push-button and carry a label like \"Start slide show\" in order to immediatly reaveal what the program is about and how to use it. I remember that the first time I used kuickshow, I was a bit confused when facing the file browser and first hovered over all toolbar buttons in order to read the popup explanations. :-)\n\nFurther idea: What about (optionally) allowing kuickshow to be integrated into konqueror (kpart?) so that a slide show could be started in a konqueror window? Probably, navigation buttons at the bottom of the konqueror window/file browser would be useful in this case. \nI don't use Windows myself, but saw something like on the computer of a friend who uses Windows XP. Thought it was pretty nifty. There are more and more digital cameras around and people like to browse their image collections, so why not allow them to view a little slide show right in konqueror?\n\nThanks!\nBeat\n \n\n\n "
    author: "Tuxo"
  - subject: "Re: KuickShow"
    date: 2004-01-15
    body: "I experienced problems with the file scroller when the file names are strange.\n\nthe next item algoritm used for the \"up and down\" of the entities is not the same as used for the sort algorithm. So you don't get the next item in the file scroller but you jump back 4 lines... very strange..."
    author: "hein"
  - subject: "Re: KuickShow"
    date: 2004-01-16
    body: "> the next item algoritm used for the \"up and down\" of the entities is not the \n> same as used for the sort algorithm. So you don't get the next item in the \n> file scroller but you jump back 4 lines... very strange...\n\nYes, this is a known bug in QIconView and hence is not limited to KuickShow, but visible in other KDE apps, too.\n\nFeel free to ask TrollTech about the status of issue N30308 :]"
    author: "Carsten Pfeiffer"
---
In  <a href="http://www.kdedevelopers.org/node/view/307">his blog</a> over at the <a href="http://www.kdedevelopers.org/blog">KDE Developer Journals</a>,
<a href="mailto:geiseri at kde.org">Ian Reinhart Geiser</a> reveals <a href="http://www.kdedevelopers.org/node/view/306">the new KDE banner</a>  to be used at the <a href="http://www.linuxworldexpo.com/">LinuxWorld Conference & Expo</a> this year.  The recently launched <a href="http://www.kde-apps.org/">KDE-Apps.org</a> has been seeing a flurry of activity from the community, including a nice submission of 2 new GPL applications:
<a href="http://www.kde-apps.org/content/show.php?content=10094">K ISDN Watch</a> and <a href="http://www.kde-apps.org/content/show.php?content=10093">K ADSL Watch</a> by <a href="http://www.avm.de/en/">AVM</a>, a manufacturer of PC ISDN-Controllers and ISDN application software.  It's very nice to see KDE support from a hardware vendor -- some of you might also be interested to know that <a href="http://www.ati.com/">ATI Technologies Inc</a> also provides a relatively KDE-friendly <a href="http://static.kdenews.org/content/misc/ati-info-panel.png">Qt3 configuration application</a> with their <a href="http://www.ati.com/support/drivers/linux/radeon-linux.html">proprietary Linux driver</a> for XFree86.
Finally, a note that <a href="http://kuickshow.sourceforge.net/">KuickShow</a>
is the current <a href="http://www.kde.de/appmonth/2004/kuickshow/beschreibung.php">App of the Month</a> (de)
for January 2004 at <a href="http://www.kde.de/index-script.php">KDE.de</a>. Like always this is accompanied with a <a href="http://www.kde.de/appmonth/2004/kuickshow/interview.php">nice interview</a> (de)
and <a href="http://www.kde.de/appmonth/2004/kuickshow/shot04.png">some</a> <a href="http://www.kde.de/appmonth/2004/kuickshow/shot10.png">screenshots</a>.
<!--break-->
