---
title: "eWEEK: 'Grunt Work' Brings Early Version of KOffice to Mac OS X"
date:    2004-01-08
authors:
  - "Datschge"
slug:    eweek-grunt-work-brings-early-version-koffice-mac-os-x
comments:
  - subject: "webmaster at konqueror.org: LISTEN IN"
    date: 2004-01-07
    body: "FFS, your last update was exactly ONE YEAR AGO.\n\nGet a grip.\n\nbb"
    author: "bobbob"
  - subject: "Re: webmaster at konqueror.org: LISTEN IN"
    date: 2004-01-07
    body: "seems like they need help. \n\nFab"
    author: "Fabrice Mous"
  - subject: "Re: webmaster at konqueror.org: LISTEN IN"
    date: 2004-01-07
    body: "He'd probably rather be coding..."
    author: "Andr\u00e9 Somers"
  - subject: "Re: webmaster at konqueror.org: LISTEN IN"
    date: 2004-01-07
    body: "always need volunteers :)"
    author: "anon"
  - subject: "Re: webmaster at konqueror.org: LISTEN IN"
    date: 2004-01-08
    body: "Anybody wants to help ?\nBecoming webmaster of the himepage of an important app ?\nYou're all very welcome :-)\n\nBye\nAlex\n"
    author: "Alexander Neundorf"
  - subject: "1day Quanta++Tutorial f. Professional WebDesigners"
    date: 2004-01-07
    body: ">Quanta Plus is apparently running on Mac OS X as well now\n\nQuanta Plus now also? Wow!\n\nThis means, I'll have to seek an occasion this summer to get Eric Laffoon and Andras Mantia to Germany. We could organize a few one day tutorials for them\nto hold. I think the \"market\" is there amongst professionally working web designers who'd pay what it is worth: a superb tutorial about a world-class tool. -- Especially if the Quanta+ guys succeed to nurture their kick-ass WYSIWYG HTML editing component up to its first level of maturity!\n\nHonestly -- I don't know which comparable tools there are for Mac OS X. Could\nwell be that Quanta Plus plays in a league of its own there first, befor it will\nbe leading all around."
    author: "Kurt Pfeifle"
  - subject: "Re: 1day Quanta++Tutorial f. Professional WebDesigners"
    date: 2004-01-07
    body: ">  Honestly -- I don't know which comparable tools there are for Mac OS X. Could well be that Quanta Plus plays in a league of its own there first, befor it will be leading all around.\n\nOn OSX, Quanta is probably most comparable to the vernerable BBEdit. However, Dreamweaver exists on OSX, but once VPL mode in Quanta gets mature, Quanta could compare quite well to Dreamweaver."
    author: "anon"
  - subject: "Re: 1day Quanta++Tutorial f. Professional WebDesig"
    date: 2004-01-08
    body: "It's already brought a number of people from the Windows world to 'nix, so here's to looking toward a bright future!"
    author: "Jilks"
  - subject: "Re: 1day Quanta++Tutorial f. Professional WebDesigners"
    date: 2004-01-08
    body: ">> Honestly -- I don't know which comparable tools there are for Mac OS X. Could well be that Quanta Plus plays in a league of its own there first, befor it will be leading all around.\n \n> On OSX, Quanta is probably most comparable to the vernerable BBEdit. However, Dreamweaver exists on OSX, but once VPL mode in Quanta gets mature, Quanta could compare quite well to Dreamweaver.\n\nI'm not familiar with BBEdit. As far as I'm concerned Dreamweaver is largely the functional standard, but it's worth noting that even Dreamweaver doesn't utterly dominate, and not just because of price. We will certainly benefit from more developer involvement but we're not targetting any specific tool or approach. We're targetting our view of an optimal approach with the aperture opened to include the widest possible diversity of users. So there will be features in Quanta that make direct comparison hard. \n\nAs VPL matures there will also be a maturing of our CSS tools, Kommander, our DCOP abilities, data tools, general interface cohesion, resource management tools, group management tools and more. VPL will take an ambitious approach to doing things that haven't been done because they are desirable from our perspective and possible architecturally... From a design standpoint it will be challenging and I hope we continue to involve more developers.\n\nIf we manage to do what we should then Quanta should be the most desirable tool for a great many. I'm getting reports from a number of people that it already is for their needs. 2004 should be a great year for web development on KDE.\n\nBTW I'm very much interested in speaking opportunities to promote and teach Quanta wherever they may be. I'm looking forward to getting to Germany."
    author: "Eric Laffoon"
  - subject: "Re: 1day Quanta++Tutorial f. Professional WebDesig"
    date: 2004-01-08
    body: "Wow, Eric Lafoon in Germany. I guess the only problem will be that many web developers don't know quanta yet. Perhaps it should be presented at CeBit.\n"
    author: "Gerd"
  - subject: "Re: 1day Quanta++Tutorial f. Professional WebDesig"
    date: 2004-01-08
    body: "> I guess the only problem will be that many web developers don't know quanta yet. Perhaps it should be presented at CeBit.\n\nMaybe next year. It looks to me like CeBit is looking for money from commercial vendors to be on the floor to exhibit their product. I did a search for PHP and came up empty. I think we would need something like the O'Reilly Comdex sponsorship. That or I would have to sell a bunch of CDs, and even at that I still have travel expenses. My desire to spread the word has it's limits. If I can construct a scenario that enables me to further fund development instead of depleting funds that would be preferable. It's probably too late to do anything with CeBit this year.\n\nIt's true that Quanta is largely unknown to perhaps most web developers, but it might be surprising how many do know about it. There are a whole lot of web developers on the planet. Having even a few hundred really knowlegable about all Quanta can do could have a nice impact spreading the word."
    author: "Eric Laffoon"
  - subject: "Re: 1day Quanta++Tutorial f. Professional WebDesig"
    date: 2004-01-08
    body: "What about trying to get you to one of the PHP-Conferences?\n\nThere sure must be some readers here who are going there or have contacts....\n\n;-) \n\n"
    author: "Kurt Pfeifle"
  - subject: "Re: 1day Quanta++Tutorial f. Professional WebDesig"
    date: 2004-01-09
    body: "> What about trying to get you to one of the PHP-Conferences?\n \nI have been answering call for papers on several in Europe... just waiting to see if they want me.\n\n> There sure must be some readers here who are going there or have contacts....\n\nCertainly if anyone knows people on the board reviewing they should mention that there is an interest in developing on Linux and that Quanta is an incredible application leading the pack there. They could throw in that I'm incredibly entertaining and travel with an entourage of movie stars and super models... Okay, maybe we should just stop at entertaining. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: 1day Quanta++Tutorial f. Professional WebDesigners"
    date: 2004-01-08
    body: "All I know is that I had used quanta a while ago and was so confused that I couldn't use it.   I'm not saying it was bad, but I dont' do a lot of html, so I nead a dead easy tool.   But recent version of quanta have been so great that I'm trying to get everyone I know to use it.  In fact, it was so fun, that I began to look for reasons to use it.   I'm trying to use zwiki to do a personal wiki to organize notes about work, and I'll see if I can get quanta to work with that.   zwiki allows outside editors via webDAV, so that should be interesting. \n\nAnyway, it's a really great program."
    author: "TomL"
  - subject: "Re: 1day Quanta++Tutorial f. Professional WebDesig"
    date: 2004-01-08
    body: "bluefish? Kate?\n\nwikis are nice but there is still no unified wiki language."
    author: "Gerd"
  - subject: "Re: 1day Quanta++Tutorial f. Professional WebDesigners"
    date: 2004-01-08
    body: "The irony of needing an easy tool for web development is that most try to obscure what you are doing from you. The hard tools (editors) essentially expect you know exactly what you want to do. The reality is that once you understand some simple concepts it is all pretty easy conceptually. Quanta attempts to empower people with conceptual knowledge by not obscuring it but empowers loose knowledge to be adequate by offering assistance in the areas of precision. In this way it attempts to help people to be better without having to put too much effort into the menial work.\n\nThe fun thing is that people fall into a groove with it and don't know all kinds of other things it can do until they have a need and go searching and find it. Or perhaps someone mentions something they like about it and other users are surprised because they never dreamed to ask for such a feature. One thing very helpful to newbies is the language helps for HTML CSS, Javascript, PHP and MySql that you can download and install from on site. Also templates are not really fleshed out yet but are very powerful.\n\nEnjoy!"
    author: "Eric Laffoon"
  - subject: "Re: 1day Quanta++Tutorial f. Professional WebDesigners"
    date: 2004-12-03
    body: "I always wonder where is the Quanta community at, I know the site but there is not many community resources. Even the site doesnt look very good as far as navegability goes. I would like to get some guide on the uri scripting of the tags with the semi colon. I have an idea of what's doing, I just want more specific information about this.\n\ninstead of:\n<p id=\"pragraph\">\n\nhaving\n<p:paragraph>"
    author: "JZA"
  - subject: "Re: 1day Quanta++Tutorial f. Professional WebDesig"
    date: 2004-12-03
    body: "https://mail.kde.org/mailman/listinfo/quanta"
    author: "Anonymous"
  - subject: "There may be more than honor to win..."
    date: 2004-01-09
    body: "This announcement on the Trolltech site may be relevant for all people working on porting applications to the Mac: it seems there is not only honour, but even some prizes to win :-)\n\nhttp://www.trolltech.com/campaign/maccontest.html"
    author: "Andr\u00e9 Somers"
---
<a href="http://www.eweek.com/">eWEEK</a> is currently <a href="http://www.eweek.com/article2/0,4149,1426817,00.asp">featuring an article</a> in their <a href="http://www.eweek.com/category2/0,4148,1237896,00.asp">'Enterprise Apps' section</a> about the <a href="http://www.metapkg.org/wiki/22/">KDE on Darwin/Mac OS X project</a> which now offers <a href="http://koffice.org/">KOffice</a> and <a href="http://konqueror.org/">Konqueror</a> as <a href="http://kde.opendarwin.org/">binaries for Mac OS X</a>. And <a href="http://quanta.sourceforge.net/">Quanta Plus</a> is apparently <a href="http://wtl.wayfarer.org/images/kde/quanta8.png">running on Mac OS X</a> as well now.
<!--break-->
