---
title: "KDE 3.3 Release Cycle Starts with KDE 3.3 Alpha 1 \"Kindergarten\""
date:    2004-05-26
authors:
  - "skulow"
slug:    kde-33-release-cycle-starts-kde-33-alpha-1-kindergarten
comments:
  - subject: "Konqueror file manager"
    date: 2004-05-26
    body: "I would like to see some changes to the file manger in the area of network. Specifically making it easier for a user to add, delete and modify ftp and websites. I haven't messed with that area of file manger for quite a while but the last time I did it was a very convoluted affair to modify any thing I added to that area. Attached is a snapshot of the area I mean."
    author: "Dennis"
  - subject: "Re: Konqueror file manager"
    date: 2004-05-26
    body: "I'd say that a bookmark manager that can show subdirectories would be better.  But I'd ignore it all for a bookmark manager that isn't glacially slow.  I only have a little over 10,000 lines in my bookmarks.xml, but editing and moving around bookmarks is incredibly slow, making the categorization of them a real pain."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Konqueror file manager"
    date: 2004-05-26
    body: "I've dragged all my bookmarks into a folder, and organized them in subfolders. Works like a charm, no need for a separate bookmarks system."
    author: "Boudewijn Rempt"
  - subject: "Re: Konqueror file manager"
    date: 2004-05-26
    body: "> but editing and moving around bookmarks is incredibly slow\n\nMy bet is that for hierarchical and linear data like bookmarks are, KDE DB Framework would be used...one day. XML is usually better for data exchangeability, object-relational database engines are better for storage.\n"
    author: "Jaroslaw Staniek (Kexi Team)"
  - subject: "Re: Konqueror file manager"
    date: 2004-05-26
    body: "There is no reason for the XML to make things slow.\n\nAfter all, why not just load the bookmarks into a structure in memory when they re needed (or even on Konq. startup)? How large can they be?\n\nThen just dump to XML when you have to, and you only have to deal with the overhead of XML every once in a while."
    author: "Roberto Alsina"
  - subject: "Re: Konqueror file manager"
    date: 2004-05-27
    body: "Well, you can't make O(1) updates. And of course, QDom is just slow.\n"
    author: "Sad Eagle"
  - subject: "Re: Konqueror file manager"
    date: 2004-05-27
    body: "How long can it take to generate a 2000-object XML thing? And who has over 2000 bookmarks? ;-)"
    author: "Roberto Alsina"
  - subject: "Re: Konqueror file manager"
    date: 2004-05-27
    body: "You would be surprised. Some have as many as 9000 bookmarks, BTW -- see http://bugs.kde.org/show_bug.cgi?id=59401. So if you feel bored one day, you may want to consider trying making QDom dumping faster instead of writing spatial file managers :-). I think there were BC difficulties w/that when I took a quick look, though. Hmm, I hear Frerich is hacking on the editor, though."
    author: "Sad Eagle"
  - subject: "Re: Konqueror file manager"
    date: 2004-05-28
    body: "Well, spatial file managers look like more fun ;-)\n\nYou know, fancy program that\u00b4s probably about 1000 lines of code. QDom, OTOH, is hairy."
    author: "Roberto Alsina"
  - subject: "Re: Konqueror file manager"
    date: 2004-05-26
    body: "I agree, I never understood the feature that's in your screen shot and how it isn't redundant with bookmarks. "
    author: "Ian Monroe"
  - subject: "Re: Konqueror file manager"
    date: 2004-05-26
    body: "Well, if the thing was more user friendly, I'd use it for bookmarking ftp sites. Which is what I like it for. It is a bit redundant for websites but does remind me of how you can have Windows display it's \"Favorites\" on the side. But then there is already a button for that, IIRC."
    author: "Dennis"
  - subject: "Re: Konqueror file manager"
    date: 2004-05-27
    body: "It's not at all redundant.  You can have trees of directories in there, and it's possible to have your tree start at any subdirectory (eg: KDE mirror directory).  You can't do that with bookmarks.\n\nI find the feature very useful - I use it for various home directories on other machines (ie: sftp://user@host) as well as links to mirrors of KDE and SuSE ftp sites.  I've been using it for quite a while now.\n\nThe feature itself is IMO quite user friendly, but configuring it certainly isn't.  While I'm quite able to go edit a desktop file by hand to get what I want, it certainly shouldn't be the normal way for a user to set anything up.\n\n-- Steve"
    author: "Steve"
  - subject: "Re: Konqueror file manager"
    date: 2004-05-26
    body: "What is shown in that snapshot isn't new.\n\nIt is in 3.2 branch.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Konqueror file manager"
    date: 2004-05-26
    body: "I know it's not new, just very cumbersome to use. IIRC the last time I fiddled with it I had to drill down/hunt for a number of config files in .kde to change things. Not something a normal user should have to do.\n\nFor example, a user creates a new folder, realizes it was named incorrectly then tries to delete it. You can't from the desktop. You have to monkey with config files to get rid of it or right click and move it to another folder and then delete it. "
    author: "Dennis"
  - subject: "Re: Konqueror file manager"
    date: 2004-05-26
    body: "> I know it's not new, \n\nThen it would be better to file a wishlist item or discuss this on 'kde-usability'.\n\n> just very cumbersome to use. IIRC the last time I  fiddled with it I had to  \n> drill down/hunt for a number of config files in .kde to change things. Not \n> something a normal user should have to do.\n\nIIUC, this is used to for your network configuration and what you get as a default is just a demo.  Probably not a good idea -- it might be better to leave it blank.\n\nYes, this appears to be an unfinished feature.\n\nFirst, there are major problems with the RMB menu.\n\n> For example, a user creates a new folder, realizes it was named incorrectly \n> then tries to delete it. You can't from the desktop. You have to monkey with config files to get rid of it or right click and move it to another folder and then delete it. \n\nAnd your example is the first one, there is no: \"Move to Trash\" option.\n\nMost of these options in the RMB menu are not usefull here and should be removed (somehow).  IMHO, what is needed is a GUI configure tool for this, and IATM that the menu editor for KDE-3.1.x could be modified to accomplish this function -- just remove the features not needed and modify it so that it could only create folders (with names in the '.directory' file) and URL 'desktop' files (it also needs to be able to edit the global configuration if run as root).\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Konqueror file manager"
    date: 2004-05-27
    body: "IIRC, I did file bug/enhancement report. But that's been well over a year ago..... I think. It's been so long I don't remember and to lazy to look it up."
    author: "Dennis"
  - subject: "Re: Konqueror file manager"
    date: 2004-05-28
    body: "I don't know that discussing it on kde-usability will do any good. Got off my butt and did a quick search of bugs. Found one going all the way back to kde 2.2.0;\n\nhttp://bugs.kde.org/show_bug.cgi?id=32224\n\nThe person did report as an enhancement, though I am not sure I would have called it that. "
    author: "Dennis"
  - subject: "Konqueror & AdBlock"
    date: 2004-05-26
    body: "Any chance we will see http://bugs.kde.org/show_bug.cgi?id=15848 in KDE 3.3? This is the only feature I miss from Firefox and that keeps me using it though I think Konqui is more elegant, nicer and faster."
    author: "fake guy"
  - subject: "Re: Konqueror & AdBlock"
    date: 2004-05-26
    body: "I've been really happy using squid and adzapper. Works for all browsers, only a few minutes to set it up even if you have to install squid.\nhttp://www.linuxjournal.com//article.php?sid=7556\n\nBut I agree it's not a solution for grandma..."
    author: "TDH"
  - subject: "Re: Konqueror & AdBlock"
    date: 2004-05-26
    body: "I also used to think that it would be nice to have ad blocking right in Konqueror, but I recently started using <a href=\"http://www.privoxy.org\">Privoxy</a>.  \n\nConfiguration of it is a bit more complex than just the browser, but it does allow much more fine-grained control.  It also allows me to keep the same ad blocking regardless of the browser I'm using at the moment, and *all* users of the computer get the benefits of it."
    author: "Carl"
  - subject: "Re: Konqueror & AdBlock"
    date: 2004-05-27
    body: "Thanks. I just emerged privoxy, and I must say that it works quite well, even with the default settings.\nAnyway, I still think that a konqueror-integrated adblock would be a killer feature."
    author: "fake guy"
  - subject: "Re: Konqueror & AdBlock"
    date: 2004-05-26
    body: "i use opera cause it has flexible settings system.\n\nfor example i've choosen to only display chached images.\n\nSo if i _really_ need to see any pictures on the web-page, i just press Shift+G, or rmb on particular image (in case when i want to see only one image)\n\nAlso, i can quickly (through menu item) disallow scripts to open new windows (tabs).\n\n\nSo, it seems that konq developers should look towards usability alittle..."
    author: "Nick Shaforostoff"
  - subject: "Re: Konqueror & AdBlock"
    date: 2004-05-26
    body: "\"So, it seems that konq developers should look towards usability alittle...\"\n\nBoy, that's a useless statement. By saying it, you realize that you are indicating that konq developers don't do anything \"towards usability\", whatever that means."
    author: "Joe"
  - subject: "Re: Konqueror & AdBlock"
    date: 2004-05-26
    body: "i meant \"little more than today\"\n\nbtw its hard to express what you think in your 3rd language (to make boast of)..."
    author: "Nick Shaforostoff"
  - subject: "Re: Konqueror & AdBlock"
    date: 2004-05-27
    body: "Agree me, yes."
    author: "Joe"
  - subject: "Re: Konqueror & AdBlock"
    date: 2004-08-04
    body: "If you can't express relatively simple concepts in a language is it really one that's \"yours\" at all?  or do you really think that knowing N other languages makes it okay to stumble through an N+1th?"
    author: "anonymous troll"
  - subject: "Re: Konqueror & AdBlock"
    date: 2004-12-09
    body: "Off-line navigations and Adblock capabilities will make KONQUEROR  a perfect choice for Linux Web Browser."
    author: "rudgero"
  - subject: "Re: Konqueror & AdBlock"
    date: 2004-08-25
    body: "we all want Adblock 4 konqueror!! please.."
    author: "carlos"
  - subject: "Kindergarten"
    date: 2004-05-26
    body: "Kindergarten sounds like a better word for KDE alpha apps.\n\nWill slibo or another chess client be part of KDE games?"
    author: "Bert"
  - subject: "Re: Kindergarten"
    date: 2004-05-26
    body: "Yes! Cool idea, why not rename kdenonbeta to kindergarten? Sweet!"
    author: "me"
  - subject: "Re: Kindergarten"
    date: 2004-05-26
    body: "I like that idea, too."
    author: "cm"
  - subject: "Re: Kindergarten"
    date: 2004-05-26
    body: "Well, it seems naming-guru Coolo strikes again (think meinproc, unsermake, others ?) ;-)\n\nAlex"
    author: "aleXXX"
  - subject: "Re: Kindergarten"
    date: 2004-06-01
    body: "What about 'Bratwurst' and 'Sauerkraut' for the next releases..."
    author: "punk"
  - subject: "Re:"
    date: 2004-05-26
    body: "I can't wait for Kopete's interface to be asynchronous!"
    author: "ObiTuarY"
  - subject: "drag & drop menu"
    date: 2004-05-26
    body: "Just scanned the feature plan, and found that we finally will get drag & drop of the menu entries - will this also work for the bookmarks in Konqueror ?? "
    author: "Lars Roland"
  - subject: "Re: drag & drop menu"
    date: 2004-05-26
    body: "Actually from a technical support perspective this feature is the worse feature EVER enabled by default into Windows 2000. 3 yrs ago while I was doing technical support I wouldn't believe the number of users that would call in because they could find an Icon or their toolbar anymore --- And it was always because they had dragged it accidently somewhere else.\n\nDon't beLIEve me - Look at WinXP Pro/Home. Drag and Drop menues are disabled by default. "
    author: "thesimplefix"
  - subject: "Re: drag & drop menu"
    date: 2004-05-26
    body: "If you use poledit or active directory, this is not a problem.\n\nKiosk ROX :)"
    author: "Bellegarde Cedric"
  - subject: "Re: drag & drop menu"
    date: 2004-05-26
    body: "I would disable it by default then. This is a nice to have feature (a simple copy and past context menu is better), but even power users rarely re-organize their menus once they have them sorted."
    author: "David"
  - subject: "Re: drag & drop menu"
    date: 2004-05-27
    body: "I wouldn't disable it completely by default.  I think the problematic part is being able to drag things out of menus.  This goes against the old behaviour of holding the mouse button down while selecting something from a menu, and is confusing at the least.\n\nMenus should be enabled as drop targets though.  Eg: drag a file/program, hold it over the K menu (which springs open), and then you can drop it somewhere in there  which would create a link to the original item.  After some visual feedback (eg: new entry is highlighted for a second or blinks twice) the menu closes again.\n\nThere is similar functionality in WinXP; entries can be removed with right-click->delete.  While I'm at it, I'd quite like to see something like Apple's spring-loaded folders too - it'd be nice if all the things that opened as the result of a single drag then closed themselves again afterwards.  If the drag knows what it opened, it could send a close signal when it's done.  Of course, this functionality would have to be configurable too...\n\n-- Steve"
    author: "Steve"
  - subject: "Re: drag & drop menu"
    date: 2004-05-27
    body: "Konqueror just got spring loading folders that close themselves automatically in cvs."
    author: "Matt"
  - subject: "Re: drag & drop menu"
    date: 2004-05-26
    body: "Well this problem seams to be with many things including KDE - we have 50 desktops running SUSE Linux and Windows XP (dual boot). Many users in the Windows camp have the problems you describe - but I have noticed that the lack of drag & drop in the bookmarks menu in Konqueror and in the kmenu seams to confuse them (I do not support these machines any more, but when I did, editing the menus was a question I got all the time).\n"
    author: "Lars Roland"
  - subject: "Re: drag & drop menu"
    date: 2004-05-27
    body: "I really find the current menu editing scheme very cumbersome, from a user perspective, drag and drop would be a very nice feature to have, I am glad this will be forthcoming!"
    author: "Jochen Weiss"
  - subject: "Re: drag & drop menu"
    date: 2004-05-27
    body: "How about if you CTRL-Drag it it will moooooove"
    author: "Aaron Krill"
  - subject: "Re: drag & drop menu"
    date: 2004-05-28
    body: "SHIFT-Drag = Move\nCTRL-Drag  = Copy\n\nDon't start unconventional behaviour..."
    author: "fprog"
  - subject: "Re: drag & drop menu"
    date: 2004-06-03
    body: "I had this idea for some time already, it just so happened that appropriate discussion never came up.\n\nA lot of times I fine that I need to move more than 1 icon/menu item.\nAfter pressing a menu hot key (e.g. CNTRL), the menu would expand about 16 pixels, and a new column would be formed on the right. In that area there would be check boxes. After you release hot key (CNTRL), the are with checkboxes slides back and you can drag items, or choose a different menu and select some more checkboxes :D. It is not really hard to implement, just someone have to do it.\n\nbest regards\n\nOleg M"
    author: "Oleg Mitsura"
  - subject: "Konqueror & Java"
    date: 2004-05-26
    body: "I hope konqueror could work better with java in this next release. I still can acces my home bank using konqueror. I tried IBM/SUN/Kaffe, etc, and it simple doesn\u00b4t work.\n\nWanna try? The applet doen\u00b4st load: www.bradesco.com.br\n\nSome javascripts doesn\u00b4t work too: www.balaplanet.com.br\n\nOk, ok... I just reported the bug...\n\nI like KDE too much, but I still have to use Firefox in this cases."
    author: "Paulo Junqueira da Costa"
  - subject: "Re: Konqueror & Java"
    date: 2004-05-26
    body: "Yea, Java that requires a security authorization don't seem to work for me."
    author: "Ian Monroe"
  - subject: "Re: Konqueror & Java"
    date: 2004-05-27
    body: "Try turning on the \"Use KIO\" option in the Konqueror Java configuration."
    author: "Tim"
  - subject: "Re: Konqueror & Java"
    date: 2004-05-26
    body: "Same thing with https://www.snet.lu/snet_online_english.html"
    author: "SPW"
  - subject: "Re: Konqueror & Java"
    date: 2004-05-26
    body: "Works fine for me.  Get a nice little Java applet that asks me to move my mouse around for a bit, then shows a login screen asking for an account number.  That's as far as I can go as I don't have an account with them.\n\nFreeBSD 5.2-CURRENT, KDE 3.2.2, JDK 1.4.2."
    author: "Freddie"
  - subject: "Re: Konqueror & Java"
    date: 2004-05-27
    body: "works fine here (konq 3.2.2, linux 2.6.5)"
    author: "anonymous"
  - subject: "Re: Konqueror & Java"
    date: 2004-05-26
    body: "I can't read the page, but it loads just fine for me.  Unless the applet's not on the front page, in which case, could you send the URL for the page with the Java applet.  Everything loads, there's no empty boxes like there was before I installed Java.  Don't see the problem with this page.\n\nThis is on FreeBSD 5.2-CURRENT, KDE 3.2.2, JDK  1.4.2."
    author: "Freddie"
  - subject: "Re: Konqueror & Java"
    date: 2004-05-26
    body: "www.bradesco.com.br\n\n-> this page loads completely in konqueror 3.2.1, including the flash-animation.\nI don't see any java applets, but I also don't see any blank spots where a java applet is missing.."
    author: "anonymous"
  - subject: "Re: Konqueror & Java"
    date: 2004-05-26
    body: "See http://bugs.kde.org/show_bug.cgi?id=75624\nThis is fixed in current CVS, so please try this alpha release and report your findings.\n\nThere is support for secure signed applet in this new release too, although not finished. But if an applet wants to do things that are normally forbidden and it has SSL certificates, you get a dialog asking what to do. You can confirm it for only one action but also for all forbidden actions afterwards.\n\nSo please test and report!"
    author: "koos"
  - subject: "Re: Konqueror & Java"
    date: 2004-05-27
    body: "Very good to hear it! I really like konqueror and this site is necessary to me.\n\nI'm going to continue to use Firefox until the next KDE release."
    author: "Paulo Junqueira da Costa"
  - subject: "Re: Konqueror & Java"
    date: 2004-05-27
    body: "It is necessary to go to the next page to see the applet. It is necessary to fill the agency and account number.\n\nAbout the other site, there's a javascript in the right side that scrolls the todays partys and it doesn't work too.\n\nI guess it will fixed in the next release too."
    author: "Paulo Junqueira da Costa"
  - subject: "Re: Konqueror & Java"
    date: 2004-05-27
    body: "One more site: Orkut\n\nKonqueror is not working to rank friends."
    author: "Paulo Junqueira da Costa"
  - subject: "Re: Konqueror & Java"
    date: 2004-05-27
    body: "I just went and tried it out...selected haven't met  & acquaintance, they showed up, moved a friend from Friend to Good Friend...everything worked.  I'm on KDE 3.2.1"
    author: "Steven Scott"
  - subject: "Re: Konqueror & Java"
    date: 2004-05-27
    body: "I guess I inform you the wrong page. Try FriendsKarma."
    author: "Paulo Junqueira da Costa"
  - subject: "Re: Konqueror & Java"
    date: 2004-05-27
    body: "I guess you didn\u00b4t try the same part as me.\n\nThe link is the \"Friends Karma\""
    author: "Paulo Junqueira da Costa"
  - subject: "Re: Konqueror & Java"
    date: 2004-05-28
    body: "I think this is known problem, check this out: http://bugs.kde.org/show_bug.cgi?id=66816"
    author: "tpr"
  - subject: "Re: Konqueror & Java"
    date: 2004-09-21
    body: "I\u00b4m facing some problems with Banco do Brasil too. http://www.bb.com.br , \nI\u00b4ve installed the Sun\u00b4s applet and when I try to access my acount it just says\n\"loadind applet\"."
    author: "Doug Br"
  - subject: "Superkaramba?"
    date: 2004-05-26
    body: "I looked through the feature plan, and noticed that there was no mention of superkaramba anywhere.  Are the plans to incorporate this in the works?  I know that Adam had stated that it may be too early for it to be considered to be added to KDE, but I was mostly curious as to the status of the consideration.\ncheers,\n-Ryan"
    author: "Ryan Nickell"
  - subject: "Re: Superkaramba?"
    date: 2004-05-26
    body: "Superkaramba is to much overhead if you ask me.\nSuch things should be created as a kdesktop replacement."
    author: "Mark Hannessen"
  - subject: "Re: Superkaramba?"
    date: 2004-05-27
    body: "The overhead is not from superkaramba, but the themes that are not well written or don't free resources when they are not needed.  Superkaramba is actually written in C, with a python hook for the themes to run."
    author: "Ryan Nickell"
  - subject: "Re: Superkaramba?"
    date: 2004-05-27
    body: "I have to disagree.\n\nI've studied the superkaramba source, and I suspect that most of the overhead and/or wasted cycles is due to the polling model that superkaramba uses when dealing with it's sensors.  Coupled to that is the significant overhead of the karamba core calling into python and back.\n\nFWIW, I've re-implemented karamba from scratch as a PyKDE app.  It's not done (I'm waiting on the KDE 3.3 and PyKDE 3.11 to ship).  My app is called \"karakoram\" and it uses almost zero cpu -- much, much less than karamba.\n\nI've uploaded a snapshot here:\n\nhttp://sourceforge.net/projects/karakoram/\n\nScreenie here:\n\nhttp://root.karakoram.org/karakoram_site_previous/gallery\n"
    author: "Troy Melhase"
  - subject: "Re: Superkaramba?"
    date: 2004-05-28
    body: "That\u00b4s pretty cool!"
    author: "Roberto Alsina"
  - subject: "Re: Superkaramba?"
    date: 2004-06-02
    body: "Hey, thanks!"
    author: "Troy Melhase"
  - subject: "Re: Superkaramba?"
    date: 2004-05-28
    body: "The progect and the goals are really nice but the name is so.....weird :)"
    author: "Davide Ferrari"
  - subject: "Re: Superkaramba?"
    date: 2004-06-02
    body: "Weird?  I'd have never thought the name was weird.  Maybe unusual, but not weird.  \n\nKarakoram is the name of a mountain range in the Himalayas.  I used it years ago as my Quake2 handle, and when I was looking for a name for this project, it seemed to fit well -- no other software named that, and it starts with a K.  :D\n\n\nhttp://en.wikipedia.org/wiki/Karakoram"
    author: "Troy Melhase"
  - subject: "Re: Superkaramba?"
    date: 2004-05-28
    body: "Okay, which syllable(s) in \"karakoram\" get(s) emphasized?"
    author: "Tukla Ratte"
  - subject: "Re: Superkaramba?"
    date: 2004-06-02
    body: "Looks like the first one:\n\nhttp://www.m-w.com/cgi-bin/dictionary?book=Dictionary&va=karakoram&x=0&y=0"
    author: "Troy Melhase"
  - subject: "Although K3B is as good as it is"
    date: 2004-05-26
    body: "Although K3B is in my opinion the best CD/DVD burning tool available for Linux I would like to see more stability (less misburns) in K3B before KDE 3.3 final is released.\n\nFor many new Linux/KDE users CD/DVD burning software will be an essential requirement when deciding to make the transition to Linux. IF burning a CD/DVD doesn't work 99% of the time (like that in Nero or EasyCD/DVD Creator) then many new users will find Linux a waste of time."
    author: "thesimplefix"
  - subject: "Re: Although K3B is as good as it is"
    date: 2004-05-26
    body: "Why should KDE 3.3 be delayed because of k3b?! They are released independently."
    author: "Anonymous"
  - subject: "Re: Although K3B is as good as it is"
    date: 2004-05-26
    body: "Because when Joe User downloads/purchases and installs a major Linux build (Fedora, Mandrake, Xandros, Lindows/Linspire, etc... ) it will likely come with the most recent version of K3B available at the time. When Joe User runs K3B and finds CD/DVDs only burn correctly 85% of the time ( I myself find this to be the case ) they are likely to be not pleased with Linux. Yes, I'm aware that K3B is NOT Linux , but users will see it as Linux having a break in functionality that their WIndows PC already had.\n\nWhen I deployed Mozilla at work (as an alternative to Outlook XP) the first things users requested if was able to do all the the things Outlook could -- They had a list! Next they wanted to know if it behaved just as well or better than their current system (Outlook). If Mozilla had NOT met all these needs or had brocken functionality they already had with Outlook there would have been no way for me to deploy Mozilla.\n\nIn the case of Linux. For many new users if Linux doesn't do everything that Windows does just as well or better then they won't look at Linux/KDE twice. I think many people would agree that basic CD/DVD burning will be on the basic requirement list for most end users. When a user sees that CD/DVD burning only works 85% of the time then most users won't look twice at Linux/KDE.\n\nYes, K3B is completely seperate from KDE -- I know, but when a user tries to burn a CD/DVD with K3B, and it only works 85% of the time it's LINUX that failed them -- NOT K3B! The user will then resort back to Windows where yes they had their problems, but when they burned a cd it just worked 99% of the time!  "
    author: "thesimplefix"
  - subject: "Re: Although K3B is as good as it is"
    date: 2004-05-26
    body: "With a half-modern CD burner and burnproof enabled, I have yet to see k3b screw a disk."
    author: "Roberto Alsina"
  - subject: "Re: Although K3B is as good as it is"
    date: 2004-05-26
    body: "It is also rather simplistic to blame k3b. k3b uses a lot of other low-level tools."
    author: "David"
  - subject: "Re: Although K3B is as good as it is"
    date: 2004-05-26
    body: "Not true. I have noticed lately (11.8-11.9) that I am getting weird behaviors and mis-burns. I have a 4 month-old Liteon DVD burner, and a gig of ram, so don't blame the hardware. 85% seems right."
    author: "Joe"
  - subject: "Re: Although K3B is as good as it is"
    date: 2004-05-26
    body: "Is this a distro you have built up yourself or a commercial distro?"
    author: "David"
  - subject: "Re: Although K3B is as good as it is"
    date: 2004-05-26
    body: "Well, saying it's not true that I haven't seen it is... well, wrong."
    author: "Roberto Alsina"
  - subject: "Re: Although K3B is as good as it is"
    date: 2004-05-26
    body: "You probably want to make sure that burn-proof (burn-free, whatever) is enabled.  Not sure about K3B, but I know that cdrecord (stupidly) disables burn-proof unless it is explicitly enabled.\n"
    author: "Jim"
  - subject: "Re: Although K3B is as good as it is"
    date: 2004-05-27
    body: "Yeah, thanks. I've been doing this since Linux scratched his ass at his mom's house and cd-r's were in caddies, but thanks.\n\nIt was built from source."
    author: "Joe"
  - subject: "Re: Although K3B is as good as it is"
    date: 2004-05-27
    body: "Well, it's disabled because some hardware doesn't support it.\n\nI have a 4x scsi yamaha that simply doesn't work if burnproof is on. If I set it to off and do nothing else (it's a P1-166 :-) it does record successfully almost every time (perhaps 5 failures every 100)."
    author: "Roberto Alsina"
  - subject: "Re: Although K3B is as good as it is"
    date: 2004-05-27
    body: "You did it by saying that you've got a `Liteon`. That's the problem."
    author: "TaNGo"
  - subject: "Re: Although K3B is as good as it is"
    date: 2004-06-03
    body: "Wow, is that complete bullshit. \n\nI have 3 Plextors and 1 Liteon. The Plextors cannot come close to the accurate burns of the Liteon. Why don't you give me some backup before you just blurt out nonsense?"
    author: "Joe"
  - subject: "Re: Although K3B is as good as it is"
    date: 2004-05-26
    body: "> Because when Joe User downloads/purchases and installs a major Linux build (Fedora, Mandrake, Xandros, Lindows/Linspire, etc... ) it will likely come with the most recent version of K3B available at the time.\n\nSo what's the problem?  You say yourself that the distributions will go and get the latest version of K3B, so why should the whole KDE project wait around for it?\n\n\n> When Joe User runs K3B and finds CD/DVDs only burn correctly 85% of the time ( I myself find this to be the case )\n\nThat's very surprising, if people were having trouble like this on a wide-scale basis, then I'm sure I'd have heard about it before now.  What distribution are you using?  It sounds like something is misconfigured.  I'm assuming you aren't getting similar results in Windows?\n\nIn any case, K3B is merely a front-end to cdrecord, so if anything is going wrong on the software side, it'll be a) a fault in cdrecord, b) a fault in the Linux kernel, or c) some kind of system misconfiguration.  It's very difficult for a front-end to accidentally lower reliability.\n"
    author: "Jim"
  - subject: "Re: Although K3B is as good as it is"
    date: 2004-05-26
    body: "\"When I deployed Mozilla at work (as an alternative to Outlook XP) the first things users requested if was able to do all the the things Outlook could -- They had a list! Next they wanted to know if it behaved just as well or better than their current system (Outlook). If Mozilla had NOT met all these needs or had brocken functionality they already had with Outlook there would have been no way for me to deploy Mozilla.\"\n\nThen why were you deploying Mozilla? Sounds like they are happy with Outlook. Users do not create a list of features for Outlook because they generally don't even know what features it does have! You need to create a rational for using Mozilla or something else based on cost, additional features and whether it gets the job done easily and simply. Make sure you know how to use the software fully as well! Backing from top management is key, then sit down with your users, tell them what you are doing, why you are switching and what it is going to mean for them. Fear is a large part of that process and why people cling onto Outlook, and a great deal of fear can be removed by simply talking to them. Unfortunately, these are social obstacles rather than technical ones (\"Oh we must make an Outlook clone\") that need to be solved. Get two or three users to use something different, do test installations, and everyone wonders what it is that they've got.\n\nFeatures are an inevitable part of whether the software can get the job done, but Joe User 'never' creates a list of those features. He probably never uses any of them.\n\n\"I know, but when a user tries to burn a CD/DVD with K3B, and it only works 85% of the time it's LINUX that failed them -- NOT K3B!\"\n\nSorry but I have only ever seen k3b work 100% of the time. K3B WORKS! What distribution are you using it with? Is it a commercial one or is it a custom made distro? If it is a custom made distro then you need to look at the issues yourself and find out what tools K3B needs to work. If it is a commercial distro then you have some comeback because they need to have looked at these configuration issues.\n\n\"The user will then resort back to Windows where yes they had their problems, but when they burned a cd it just worked 99% of the time!\"\n\nPeople do have issues with burning on Windows, more often than not. The reason why people think that it works more often is because they are more familiar with it!"
    author: "David"
  - subject: "Re: Although K3B is as good as it is"
    date: 2004-05-26
    body: "Common simptom! If anyone complains on Linux/KDE/any software that runs on Linux, everyone start bashing him!\n\nI used to run RH 99% of my time (it is not the case any more :(, I had to change the company). I was quite happy with it. But for CD recording, I was really disapointed. I realized then that I:\no if I have to make backups, I use cdrecord from command line\no if I have choice of OS, I go to Win and I use Nero\n\nEven newer versions of K3b did not change that, no matter that I like that particular piece of software.\n\nPlease, be aware of the problem, don't kill the messenger!"
    author: "Tomislav"
  - subject: "Re: Although K3B is as good as it is"
    date: 2004-05-27
    body: "The messenger should also be aware that he has to deliver a *message*, not just point his finger and wave his hands a lot.\n\nYou are aware that k3b just calls cdrecord, right?\n\nIf you consistently notice cdrecord works and k3b doesn't, use the debug view in k3b, and check the options it's passing to cdrecord, and compare.\n\nThen, you can tell someone (like, say, the k3b author) how and why k3b fails, or at least he can guess.\n\nAs I said, I have burned a whole lot of CDs (about 30 a week, lots of ISO backups generated with Mondo Rescue) using k3b without failing *once*. So, while I don't doubt you have had problems, maybe the problems are not as widespread as you think.\n"
    author: "Roberto Alsina"
  - subject: "Re: Although K3B is as good as it is"
    date: 2004-05-27
    body: "I am using k3b for a year, and I haven't a single failure because a k3b or cdrecord fault, mostly bad media (cheap cd-r, that also crashes on other systems) that I will not buy again."
    author: "c0p0n"
  - subject: "Re: Although K3B is as good as it is"
    date: 2004-12-02
    body: "You're lucky it works 85% of the time. I've yet to get\nit to work at all. System is a Sony PCV-RZ502P with\nSuse 9.0 Linux installed with pretty much vanilla\noptions. All attempts fail to write the CD and make\nit unusable afterwards.\n\nAttempting to burn a DVD, CD/RW or CD-R always fails.\nYet on the same machine dual booted into winXP, I can\nburn all of those 100% of the time. \n\n"
    author: "keithS"
  - subject: "Re: Although K3B is as good as it is"
    date: 2004-05-27
    body: "Look I have been using k3b for the last year if not longer and have had less coasters created it than with easyCD. If my wife can use K3b without any comlaints and do mean no complaints I don't really see any problem with a new linux user not being able to use it or for that matter not like it. "
    author: "octico"
  - subject: "Re: Although K3B is as good as it is"
    date: 2004-05-26
    body: "K3b is not a part of KDE.\nThe Release of KDE 3.3 has no influence on the quality of that application."
    author: "anonymous"
  - subject: "Re: Although K3B is as good as it is"
    date: 2004-06-05
    body: "WHAT!?, I'm yet to have k3b fail on me. Infact, on older hardware, k3b was a lot more stable and reliable under linux than nero burn under windows that broken numerous CDs due to buffer under-run, which wasnt the came with K3B.\n\nAs for burning CDs, I burnt over 300 CDs with my new Plextor 48X CD writer and K3B, and NONE, I repeat, not a single CD was currupt with k3b.\n\nIf your CDs fail to write, blame the CDs you bought, or your CD writer is dying. It is nothing to do with K3B in all cases. Anoher possibility are the core applications K3B is the frontend for, so investigate if cdrecord is to blame."
    author: "Hackeron"
  - subject: "kwin drop shadows"
    date: 2004-05-26
    body: "I know it is hard to do with the current XF86 server. But Drop shadows for kde's windows will put the window manager way ahead of MSWindows and would probably make it one of the most advanced window manager (ok it probably is anyway but i mean seriously wicked advanced). So please any KDE DEVELOPERS/ARTIST consider native drop shadows for the next KDE release.\n\nThanks KDE rules!"
    author: "Kevin A"
  - subject: "Re: kwin drop shadows"
    date: 2004-05-26
    body: "I completly agree! I love the shadows!!!!!!!!!!"
    author: "Martijn S"
  - subject: "Re: kwin drop shadows"
    date: 2004-05-26
    body: "I agree, I'm using kwin drop shadows for kde 3.2.2 and it is rather stable, even if it needs some final improvements.\n\nNicolas."
    author: "Nicolas Blanco"
  - subject: "Re: kwin drop shadows"
    date: 2004-05-26
    body: "What are you, 12?"
    author: "Joe"
  - subject: "Re: kwin drop shadows"
    date: 2004-05-26
    body: "What are you, old and bitter? ;)\n\nCome on. Let's keep it sane."
    author: "Eike Hein"
  - subject: "Re: kwin drop shadows"
    date: 2004-05-26
    body: "> Let's keep it sane.\n\nI think that was his whole point.  Drop shadows don't make anything \"one of the most advanced window manager[s]\", and it wouldn't put it \"way ahead of MSWindows\".  It's just a nice touch, that's all."
    author: "Jim"
  - subject: "Re: kwin drop shadows"
    date: 2004-05-27
    body: "Someone smart out there. Must be only the guys who have J's in there."
    author: "Joe"
  - subject: "Re: kwin drop shadows"
    date: 2004-05-26
    body: "> What are you, 12?\n\nI would tend to agree with the comment.  Drop shadows are not an indication of how advanced the window manager is.\n\nHowever, this appears to be the way that Windows users think.\n\nI would say that this feature is useless, but it appears to be what users want.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: kwin drop shadows"
    date: 2004-06-02
    body: "Well, they're definitely not \"useful\" in any real sense, but having been forced to use a Powerbook for a week, I must say that all the graphical touches in OSX make it a real pleasure to use. They're not exactly \"useful\" but since I spend hours staring at computer screens, I appreciated having it look so attractive."
    author: "Ed Moyse"
  - subject: "Re: kwin drop shadows"
    date: 2004-07-23
    body: "In the case of OSX, the shadows make it really good to use because they don't lag behind dragging the window like they do with the kwin hack. :-("
    author: "Trejkaz Xaoza"
  - subject: "Re: kwin drop shadows"
    date: 2004-05-27
    body: "Drop shadows are nice, I agree, and it is eye candy that people will judge, but they need to be implemented at the right level. Drop shadows and transparency should be handled by the X server - rendering window contents in an off-screen buffer then later passing through a compositing manager. FDO's experimental Xserver does exactly that, and the next generation of Windows will do similar. \n\nI personally believe that this is outside the remit of KWin, and the KWin shadow patches should not be committed into any official KDE releases."
    author: "Martin G"
  - subject: "Re: kwin drop shadows"
    date: 2004-05-27
    body: "yes!!\ni want dropshadows in my kindergarten too!\neven when were grown up it makes an nice eyecandy!\nmaybe there could be the option to toggle it off, \nbecause not all hw configs will support it.\n\ncheers to the devs!\nlars"
    author: "lars"
  - subject: "Re: kwin drop shadows"
    date: 2004-06-16
    body: "While the drop shadows are far from being actually necessary they do add a nice touch of aesthetic finesse to the drop down menus. I would like to suggest a black single pixel line on the 'North' and 'West' sides to provide visual offsetting of the menu from the background. Is that technically possible?"
    author: "Christopher Sawtell"
  - subject: "Re: kwin drop shadows"
    date: 2005-01-25
    body: "I really love the shadow effect, and shadows ARE usefull. They allows much smaller window borders!\n\nBut the kwin-shadow-patch 0.6.3.a is far away from being ready to use. If you use focus follow mouse without auto raise you will have lots of artefacts. So I switched shadows off :-(\n\nBut once it is ready to use, it would be really usefull."
    author: "C. Bielke"
  - subject: "I am waitong for a stable file manager since 3.1.x"
    date: 2004-05-26
    body: "Every time I use konq in tree view it crashesh on copy/paste operations."
    author: "Sasoon Pundev"
  - subject: "Re: I am waitong for a stable file manager since 3.1.x"
    date: 2004-05-26
    body: "That shouldn't be happening. What are you doing and is there a bug filed for it somewhere?"
    author: "David"
  - subject: "Re: I am waitong for a stable file manager since 3.1.x"
    date: 2004-05-26
    body: "Funny thing about software and bugs... for some reason people tend to think that their subjective experience is representative of everyone's. It just isn't the case. Really obvious and dumb bugs like this rarely ship and are usually the result of other complex interactions... packaging errors (among the biggest), binary conflicts on RPM systems, configuration problems and such. I'm running CVS and have seen some crashes but it usually works and it's CVS so you expect the occasional crash. Bug reports are really the place to get things solved, not web talkbacks. "
    author: "Eric Laffoon"
  - subject: "Re: I am waitong for a stable file manager since 3"
    date: 2004-05-27
    body: "I used to have a similar problem that followed me for several versions. Turned out that deleting the config files in ~/.kde solved the problem.\n\nTry to log in as a different user on your system and see if you have the same issue there. If you don't, then you know the bug is triggered by something in the configfiles of konqueror."
    author: "Bart Declercq"
  - subject: "KThemeManager"
    date: 2004-05-26
    body: "Is the new theme manager going to be included by default? "
    author: "reddazz"
  - subject: "Re: KThemeManager"
    date: 2004-05-27
    body: "It was moved from kdenonbeta to kdebase just yesterday."
    author: "Anonymous"
  - subject: "Re: KThemeManager"
    date: 2004-05-27
    body: "I see, it's a really cool application and I like the fact that you have one place to change the eyecandy."
    author: "reddazz"
  - subject: "Re: KThemeManager"
    date: 2004-05-27
    body: "Glad to hear people actually like it and find it easy to use :)"
    author: "Lukas Tinkl"
  - subject: "Re: KThemeManager"
    date: 2004-05-28
    body: "Maybe I'm dumb but...I still have the old and splitted KCM modules for theming KDE..how can I get new KCM?"
    author: "Davide Ferrari"
  - subject: "Re: KThemeManager"
    date: 2004-06-01
    body: "Install from http://developer.kde.org/~lukas/kthememanager/"
    author: "Anonymous"
  - subject: "Re: KThemeManager"
    date: 2004-05-27
    body: "Yes, it was just moved from kdenonbeta."
    author: "Lukas Tinkl"
  - subject: "Spatial Konqueror?"
    date: 2004-05-26
    body: "Will Konqueror 3.3 have a spatial mode like GNOME's Nautilus 2.6 has? I love the new file manager and it would be great if KDE had it. I checked the preferences in 3.2 and I can't get it to work like Nautilus 2.6. So will it be implemented?"
    author: "Norman"
  - subject: "Re: Spatial Konqueror?"
    date: 2004-05-26
    body: "Probably not. The way Spatial mode is implemented has a very, very long way to go before it can be widely accepted.\n\nThe way the world manages files today is through tree views of folders and the two-pane explorer approach. This is not going to change anytime soon."
    author: "David"
  - subject: "Re: Spatial Konqueror?"
    date: 2004-05-27
    body: "I would like it if it remembered directory settings more reliably. For example, I wanted my images directory to have previews on and large icons. But normally I have previews off and small icons. So when I enter the images directory, I want the view settings to change, then when I leave, I want them to go back.\n\nI know it does have directory property saving, but I couldn't get it to work properly."
    author: "azazel"
  - subject: "Re: Spatial Konqueror?"
    date: 2004-05-27
    body: "In my experience, when Konqueror is in per-directory settings mode, it will switch views correctly when it enters a directory containing a .directory file.  Unfortunately, it will keep that view mode if you then open a directory that doesn't have a .directory file.  There doesn't seem to be a global \"use these view settings when there is no .directory file\" setting."
    author: "Tukla Ratte"
  - subject: "Re: Spatial Konqueror?"
    date: 2004-05-27
    body: "I find that spatial file management doesn't work too well in UNIX environments. It could be just the way I organise things, but in my experience directory structures are too deep on UNIX - you end up with a lot of clutter on your desktop from opening windows to get to your files.\n\nI used MacOS 7/8/9 for years and the spatial management system worked very well because the filesystem tended to be quite flat.\n\nDave"
    author: "Dave"
  - subject: "Spatial Konqueror?"
    date: 2004-05-26
    body: "Will Konqueror 3.3 have a spatial mode like GNOME's Nautilus 2.6 has? I love the new file manager and it would be great if KDE had it. I checked the preferences in 3.2 and I can't get it to work like Nautilus 2.6. So will it be implemented?"
    author: "Norman"
  - subject: "Re: Spatial Konqueror?"
    date: 2004-05-26
    body: "I second that.  I would love to see a spatial Konqueror, simply for the speed and simplicity that this entails.  By all means, though, keep the configurability and an option to access the advanced mode.  KDE is all aobut felxibility and we should not force users to work one way or the other but plese give the options for both with the simplest method the default for novice users."
    author: "D. Le Sage"
  - subject: "Re: Spatial Konqueror?"
    date: 2004-05-27
    body: "Yes, please. We need to crap Konqueror up as quickly as possible. I've heard about this spatial mode, Mommy, so KDE must need it. Nevermind that 99% of the world hates it...\n\nIf this abomination comes to pass, please make it OFF BY DEFAULT.\n\n\"Enabling suck-ass mode...beep boop\""
    author: "Joe"
  - subject: "Re: Spatial Konqueror?"
    date: 2004-05-27
    body: "Honestly, building a spatial file manager should be a 2-week project.\n\nLatching it onto konqueror would be evil. But hacking a standalone spatial file manager? That's damn simple.\n\nI may even write one this weekend if my date cancels."
    author: "Roberto Alsina"
  - subject: "Re: Spatial Konqueror?"
    date: 2004-06-03
    body: "First part of a PyQt-based spatial file manager is published as a sort of tutorial here: http://lateral.pycs.net/stories/29.html\n\nDon't expect much, since it amounts to about 6 hours of coding ;-)"
    author: "Roberto Alsina"
  - subject: "Re: Spatial Konqueror?"
    date: 2004-05-27
    body: "OMG I was laughing so hard reading your response..."
    author: "rsk"
  - subject: "Re: Spatial Konqueror?"
    date: 2004-05-28
    body: "Why do you laught ? I think he has reason. \nThe only things that needs to be implemented are : \n\n*\"open each file in a new window, insted in the same window\" \nI can imagine that it's possible to do that in 5 -10 C++ lines\n\n*Then : Save the window's position and preference then closing that window again\nSo, you save this in an .windows file in you kde ... or in the directory that you opened. This can be coded quiet fast (Oh, and : please, agree for that on a common standart between kde and gnome! If under gnome you looked at directory X with the \"see as thunbail\" modus, automaticaly gnome should also load this profile than looking at this file!!)\n\n*Change the UI in konqueror so that there are less icons, and you don't see all toolbars. Add shortcuts to go up a directory, or one directory down. Here there is nothing to code, everything is right here. \n\nSo, Why laught then someone says he can implement it in a weekend ? \n\nDaniel"
    author: "HelloWorld82"
  - subject: "Re: Spatial Konqueror?"
    date: 2004-05-27
    body: "My,what an intelligent and eloquent contribution to make to a discussion.  Your rhetoric is both insulting and fails to provide any new thoughts into the discussion.  I felt embarassed to read your mindless drivel, Joe.  Grow up.\n\nTo add some substance to this debate:\n\nFact 1:  Konqueror is becoming bloated. Do we really need features like FSView?  Clutter is making it convoluted.  I agree with you that systems should not be \"dumbed down\" but there is a clear distinction between making something less convoluted and making something less complex.  If Konqueror can do the same job with less clutter onscreen, than by all means remove unnecessary bloat.\nFact 2:  Konqueror does not adhere to the Unix tradition of doing one job and doing it well.  It is trying to be both a web browser and a file browser.  There is a vast difference between viewing files and HTML pages.\nFact 3:  Konqueror is slower than Nautilus.\nFact 4:  I did indicate that it should be kept flexible.  The ideal would be to have spatial mode as the default.  If users wnat advanced features, they can easily switch the system to this mode.\nFact 5:  Needless to say, more than 1% of the world do like the new Nautilus.\n\nRegards,\n\nDavid"
    author: "D. Le Sage"
  - subject: "Re: Spatial Konqueror?"
    date: 2004-05-28
    body: "What exactly do you mean by \"spatial\"? I know what the term means (difference in position, as opposed to difference in time), but what functionality / behaviour is proposed that isn't currently available in Konqueror? I don't mean in terms of a specific implementation (ie \"go look at Nautilus\" is an answer to a different question), but in terms of what it would mean for Konqi or some other application.\n\nIn terms of \"Facts\", I dispute 1, 2 (and 5, as written, but you probably didn't mean that 1% of the world or world population have tried Nautilus and liked it). Bloat is a subjective assessment, and Konqi the shell hasn't changed much. \n\nKDE is not part of the unix tradition, really. It does have good separation between tools though - KHTML (the renderer) isn't part of Konqueror. Konqueror just uses it, as do other KDE tools. There is a good description of what a KPart is on http://developer.kde.org, which might help clarify the difference."
    author: "Brad Hards"
  - subject: "Re: Spatial Konqueror?"
    date: 2004-06-03
    body: "Sorry to break your argument, but on all my debian boxes that I have and installed for friends konqueror works 3-5x faster than nautilus, try to open a directory containing 100k files, even 10k files, you'll see what I'm talking about. (Thats just file browsing). \n\nAnd still konqueror beats nautilus in pretty much everything else.\n\nI don't have anything again GNOME, but they seem to be going nowhere fast."
    author: "Oleg Mitsura"
  - subject: "Re: Spatial Konqueror?"
    date: 2004-07-14
    body: "You have a few misconceptions. ;-)\nThe things like FSView, web browsing, and file browsing, are not all being done by the same program -- they're all being done by their own programs, which are \"piped into\" konqueror.  In  that way, it adheres very well to the unix tradition:  programs do only one small task and are combined to achieve the end result.  So yes, web browsers and file browsers do different things; that's why different programs are doing those things.\n\nI also greatly wish for spatial mode to be achievable in konqueror's settings -- all that it would take would be an option to \"remember placement of windows and files,\" because the rest is already there.  It wouldn't even have to be on by default."
    author: "kundor"
  - subject: "Re: Spatial Konqueror?"
    date: 2004-05-27
    body: "Oh my god! No! Don't even try to do it! Leave Konqueror as it is! If you like Nautilus, use Nautilus!"
    author: "fake guy"
  - subject: "Re: Spatial Konqueror?"
    date: 2004-05-27
    body: "Gnome trolls infiltrating the dot?\n\nIf that strange \"spatial mode\" should ever be added to konqueror, leave it off\nby default at least."
    author: "ac"
  - subject: "Re: Spatial Konqueror?"
    date: 2004-05-27
    body: "If you love Nautilus, then USE Nautilus and do not post silly questions on dot.kde.org"
    author: "Davide Ferrari"
  - subject: "Re: Spatial Konqueror?"
    date: 2004-05-27
    body: "Come on now, that is overly harsh.. some people like spatial mode more than browsing (I do), and also want a file manager that is integrated with their desktop environment (e.g, not nautilus)"
    author: "smt"
  - subject: "Re: Spatial Konqueror?"
    date: 2004-05-28
    body: "First of all, AFAIK you *can* have spatial mode, if spatial mode is what I suppose it should be (open new window for every directory opened).\n\nSecond, and this is a more general topic, KDE from times to times choose a \"developing direction\". If your tastes are different from this direction, than change your tastes or change the product you use. We are in the F/OSS world, we can *choose*. But do not flame praising for a feature \"like that other software\"."
    author: "Davide Ferrari"
  - subject: "Re: Spatial Konqueror?"
    date: 2004-05-27
    body: "Use the middle mouse button when clicking on a directory - that's all you need to get (simple) spatial mode for konqueror. (Tabbed browsing must be off).\n\nciao"
    author: "Birdy"
  - subject: "Re: Spatial Konqueror?"
    date: 2004-07-14
    body: "that is NOT spatial mode!  that's the bad side-effect of spatial mode without the point!  If you open a folder, rearrange the icons to your liking and place the window somewhere, close it and open it again, it reverts to the original placement.  Spatial mode allows you to arrange your files as you wish, for your efficient usage, and keeps them that way."
    author: "kundor"
  - subject: "Re: Spatial Konqueror?"
    date: 2008-01-11
    body: "With a spatial filemanager, you should even be able to put an icon in a corner of the window, and the icon should stay there eyou close the widow/folder ... ad appear at the same place whe you ope it again."
    author: "Mildred"
  - subject: "Re: Spatial Konqueror?"
    date: 2004-06-01
    body: "Get a developer drunk enough at aKademy, tempt him with cheap hookers and you might get just get there.\n\n(I had a dream yesterday, in which I submitted a story to slashdot.org with the headline: OMG! I made konqueror spatial! with a link to my new homepage Konqi in Spaaaaceee.)"
    author: "Allan Sandfeld"
  - subject: "Re: Spatial Konqueror?"
    date: 2004-06-01
    body: "Oh, well, I just wrote it. Kindof.\n\nIt's not konqueror, but it is spatial. Will post the first half of it as a tutorial (kinda) at lateral.pycs.net tomorrow."
    author: "Roberto Alsina"
  - subject: "Re: Spatial Konqueror?"
    date: 2004-06-01
    body: "Neat. I didnt think I would take much coding. \n\nBut does it do remote-URLs? \nThat was why I thought of Konqueror. Ofcourse the only usefull parts of that, could be accieved by making bookmarks remember window size and location info."
    author: "Allan S."
  - subject: "Re: Spatial Konqueror?"
    date: 2004-06-01
    body: "Well, right now, it has a three-line loop to read a folder. If I make it use kdelibs (right now it's qt only), how much work do you think it would take to make it read a list from a remote URL? ;-)"
    author: "Roberto Alsina"
  - subject: "Re: Spatial Konqueror?"
    date: 2004-06-01
    body: "Sorry, but /cheap/ hookers just won't cut it for this. We want high-class ones (well, maybe you could knock people down to medium class). ;-)\n"
    author: "Richard Moore"
  - subject: "Re: Spatial Konqueror?"
    date: 2004-07-16
    body: "Everyone discussing here should at least read this before starting to argue\n\nhttp://www.bytebot.net/geekdocs/spatial-nautilus.html"
    author: "Wolfgang"
  - subject: "Re: Spatial Konqueror?"
    date: 2004-12-10
    body: "Simplicity in kde? no, it do not match..."
    author: "EdCrypt"
  - subject: "Version Control System in Konqueror?"
    date: 2004-05-27
    body: "Are there any future plans to intorduce a version control system for documents into Konqueror?  I would love to have a system that could roll back, track changes and so on, similar to those features found in a commercial tool such as Hummingbird Powerdocs:\n\nhttp://www.hummingbird.com/products/docsopen/index.html\n\nCould CVS have such a front-end built into Konqueror?  I do not use revision control for software development work; rather for keeping track of numerous drafts of documents written in LaTeX markup/DVI but I imagine any such tool would be flexible enough for any kind of work.  Accept/reject changes, document lifecycle and library classification and metadata seraching would be great to incorporate.\n\nAnyway, this is something to consider."
    author: "Anonymous"
  - subject: "Re: Version Control System in Konqueror?"
    date: 2004-05-28
    body: "Cervisia works excellent from within Konqueror, you should give it a try."
    author: "Mike Diehl"
  - subject: "Re: Version Control System in Konqueror?"
    date: 2004-05-28
    body: "It would be good to know whether CVS (which requires a central server, and needs root access to install, SVN) is really the right tool. Would a tool like Arch or darcs be more suited, which doesn't provide central access but can run in a local directory.\n"
    author: "Brad Hards"
  - subject: "Re: Version Control System in Konqueror?"
    date: 2004-05-28
    body: "You can also use CVS on a local repository (without running a server)"
    author: "wilbert"
  - subject: "Re: Version Control System in Konqueror?"
    date: 2004-05-28
    body: "Sure, but AFAICT that doesn't provide anything more than RCS would provide. Basically, you can't ever share your stuff without either a central server or a proper distributed system."
    author: "Brad Hards"
  - subject: "Re: Version Control System in Konqueror?"
    date: 2004-05-28
    body: "You can, with ssh, the :ext: mode of cvs (CVS_RSH=ssh) and ssh keys."
    author: "Inorog"
  - subject: "KDE needs to fix this darn bug!"
    date: 2004-05-28
    body: "http://bugs.kde.org/show_bug.cgi?id=61730 needs to be fixed for good. This is a LONG-STANDING bug that keeps creeping in with backports and regressions. Anyone who uses bugs.kde.org regularly should vote for this bug!"
    author: "Greg"
  - subject: "Re: KDE needs to fix this darn bug!"
    date: 2004-06-02
    body: "\n\tI'd agree, its been around a lonnnnng time, however, reviewing the bug report that you point to its also possible that it has more than one origin.  I rather suspect, from reading the bug report, that there might well be three separate points of origin.\n\n\tHowever it would be nice to see it fixed. I can see that it still exists in *some* form, with KDE 3.2.2, Gentoo, linux 2.6.4 gcc3.3.something.  Why do I see something so far down that it will take a eureka moment to get it final.\n\n\n\tIn any case, a quick grab of the frame and a twitch and the extra scrollbar is gone.  Other browsers have quirks of their own, some far worse."
    author: "AJFT"
  - subject: "dotty is back!"
    date: 2004-06-01
    body: "Oh, dear --\n\nyou can't imagine how I missed you!\nI am the happiest dottyfan on earth to see you back.\n\nWhat was the problem with you?\n\nCare to write a little story about it?\n\nI hope it is not some major health problem that is coming back to haunt you.\n\nCiao,\nyour biggst dotty-fan."
    author: "dotty is back!!"
  - subject: "Re: dotty is back!"
    date: 2004-06-01
    body: "> What was the problem with you?\n\nMove of sponsored host to another data center (including IP change) in combination with weekend and holiday."
    author: "Anonymous"
  - subject: "article on toms hardware"
    date: 2004-06-03
    body: "http://www.tomshardware.com/hardnews/20040526_123045.html"
    author: "somekool"
  - subject: "Maybe fix kuickshow while you're at it?"
    date: 2004-06-05
    body: "Endless kuickshow bug reports, it has far too many bugs, and requires some serious attention. Either fix it, or remove it, it gives KDE a bad name.\n\nExample bug report:\nhttp://bugs.kde.org/show_bug.cgi?id=79291"
    author: "Hackeron"
  - subject: "Sure is Alpha"
    date: 2004-06-16
    body: "http://developer.kde.org/development-versions/kde-3.3-features.html\n\nKDE 3.3 Feature Plan\nXML error: mismatched tag at line 346"
    author: "Christopher Sawtell"
  - subject: "Re: Sure is Alpha"
    date: 2004-06-16
    body: "It should be fixed now!\n\nHave a nice day!"
    author: "Nicolas Goutte"
---
That's one small step for KDE, one giant leap for <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a> - the <a href="http://download.kde.org/download.php?url=unstable/3.2.90/src/">KDE 3.3 Alpha 1</a> has been released to <a href="http://www.kde.org/mirrors/ftp.php">the FTP servers</a> this morning. There won't be any binary packages for this release, everyone using Kindergarten is asked to compile it with --enable-debug, so that we can get valuable feedback. There is a new unstable version of <a href="http://developer.kde.org/build/konstruct/unstable/">Konstruct</a> also available to install it. Other than that, feel free to play around with things, <a href="http://bugs.kde.org/">check if your bugs</a> are still there or if there are <a href="http://www.kde.org/support/">places where you can help</a>. Check the <a href="http://developer.kde.org/development-versions/kde-3.3-features.html">KDE 3.3 feature plan</a> for things to look for. 








<!--break-->
<p>Kindergarten is hopefully only the first step as we're heading for the <a href="http://conference2004.kde.org/">aKademy</a>.</p>






