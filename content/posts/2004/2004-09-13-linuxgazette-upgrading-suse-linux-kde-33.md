---
title: "LinuxGazette: Upgrading SUSE Linux to KDE 3.3"
date:    2004-09-13
authors:
  - "Fab"
slug:    linuxgazette-upgrading-suse-linux-kde-33
comments:
  - subject: "Windows Defectors: Upgrading a KDE Installation to"
    date: 2004-09-13
    body: "Hi Tom,\n\nI read your article with curiosity. I'm also only a Linux user. Well, maybe I \nknow a bit more then an average users but nothing special. So, I have SuSE \n9.1 installed on my IBM ThinkPad and had also upgraded KDE. The only \ndifference between your upgrade and mine is that I upgraded form KDE 3.2 to \n3.3, but it doesn't matter afterall. Anyway, what I want to say is that \nwhenever I upgraded KDE I alway did it from the command line, loggen in as a \nroot, using rpm -Fhv *.rpm. And it always worked fine. Occasionally I upgrade \nby firstly end the session, log in as root using Ctrl-Alt-F1 and continue \nsame way as already mentioned. There are problems sometimes, when there are \nnot all packages available yet. In such a case I just wait for all the \npackages to be displayed either at SuSE's ftp or www site and start the job. \nThat's it. BTW, thanks for oyur article(s).\n\nRegards,\n\nBojan"
    author: "Bojan Ivancic"
  - subject: "Re: Windows Defectors: Upgrading a KDE Installation to"
    date: 2004-10-12
    body: "Hi Bojan and all who have an IBM ThinkPad!\n  So i do have one! My question is did u manage to make a stand by?can u use your track point to scroll in any direction? Or for every new cool tip u may know i'm here to listen. if u have other problems then this i think i can help u with solving them.\n  Regards,\n  Valentin"
    author: "valentin"
  - subject: "Why does he not use YaST?"
    date: 2004-09-13
    body: "I did and it worked wonders for me :-)"
    author: "Jo \u00d8iongen"
  - subject: "Re: Why does he not use YaST?"
    date: 2004-09-13
    body: "I was reading it and thinking exactly the same. To elaborate, the exact steps are:\n- make sure you have the KDE upgrade URL in installation sources and it's enabled (refresh it if you've used it long time ago)\n- start install/remove software YaST tool\n- search for \"kde\" (without quotes)\n- Package -> All in This List -> Update if newer version is available\n- click \"Accept\"\n- YaST will tell you about some dependencies resolved automatically, click \"continue\" and wait for the process to finish\n- once it's done, log out\n- ctrl-alt-f2, log in as root, init 3, init 5\n- welcome to KDE 3.3!\n\nI did this several times on 9.0 and 9.1 systems."
    author: "zurab"
  - subject: "Re: Why does he not use YaST?"
    date: 2004-09-13
    body: "That thing about refreshing is quite important.  I don't know why it doesn't just check for new stuff, but without the refresh, changes on the FTP site are not seen.\n\nFor those wondering what this is about, in YaST under Software, click on the one to change the source of the installation.  That allows you to add additional sources, and the normal installer can see the packages from all of the sources you have set up.\n\nAnother thing: after you've done the search for \"kde\" in the software installer you should still check for other new packages in other groups, otherwise you won't get the updates to arts, qt and so on.\n\nI don't think calling init like that is necessary.  It should be enough to log out and restart your X server.  That will start the new version of kdm.\n\nI'm currently running KDE 3.3.0 on SuSE 9.1, and I upgraded this way.  I would advise anyone else who upgrades their KDE to periodically check for updates.  The SuSE packages have been updated at least once since the first 3.3.0 packages appeared - they had 3.3.0-5 and now they're on 3.3.0-14 !!\n\n-- Steve"
    author: "Steve"
  - subject: "Re: Why does he not use YaST?"
    date: 2004-09-13
    body: "It would be really nice to have an option to \"only show rpms from this/these repositories\", in the YaST Software Intaller. Then you'd just select the KDE repository, and mark all those rpms for update.  I've mailed SUSE feedback about it before, but no luck so far.\n\nJ.A.\n\nP.S.:\nNo, disabling all the YaST sources but the KDE repository ones is not the same thing. First, it would still show all the installed rpms, not only the KDE repository ones. Second you don't want to disable the other sources as they might be needed to solve some dependencies on the new KDE rpms. "
    author: "John SUSE Freak"
  - subject: "Re: Why does he not use YaST?"
    date: 2004-09-13
    body: "After setting up the Supplementary KDE YAST source, just go to 'System Update' instead of 'Install/Remove Software'.\n\nSystem Update shows you all the packages that have newer versions and lets you chose whether to install each or not.\n\nAs usual it takes care of the dependencies (although in SuSE 9.1 ast least there are a couple of minor bugs every now and then when it works out some new dependencies and conflicts, but that is quite rare)."
    author: "Colin"
  - subject: "Re: Why does he not use YaST?"
    date: 2004-09-14
    body: "Still you will also see newer versions from the other sources, beside the KDE repository ones. Again, it won't be as simple as selecting the KDE repository and mark them all (or the ones you want) for upgrade. Also like I said earlier, removing other sources, not only is tedious, but might not work since you they may be need them to solve dependencies."
    author: "John SUSE Freak"
  - subject: "Re: Why does he not use YaST?"
    date: 2004-09-15
    body: "I don't know what exactly is the difference between calling init 3 and simply restarting X, but I, at least on one occasion, was unable to load all updated Qt libraries after I only restarted X. I don't know if they are cached differently or what was the exact reason. I found that switching to runlevel 3 and back to 5 worked every time. Maybe someone who knows better can add more detail as to why this happens sometimes."
    author: "zurab"
  - subject: "Re: Why does he not use YaST?"
    date: 2004-09-13
    body: "Where do I get the url that I add as a Yast source for the new KDE 3.3 rpms?\n\nCan some of you please tell me?"
    author: "Anon..."
  - subject: "Re: Why does he not use YaST?"
    date: 2004-09-13
    body: "ftp://ftp.suse.com/pub/suse/i386/supplementary/KDE/update_for_9.1/yast-source"
    author: "Anonymous"
  - subject: "Re: Why does he not use YaST?"
    date: 2004-09-14
    body: "You should check SuSE's web site for a list of mirrors.  You will most likely find that you get a faster download from one near to you, as well as being bandwidth-friendly.  Note that the mirrors have different paths up to the \"suse\" directory, but after that it's always the same.\n\nThe mirrors are listed here: http://www.suse.de/en/private/download/ftp/int_mirrors.html\n\n-- Steve"
    author: "Steve"
  - subject: "Re: Why does he not use YaST?"
    date: 2004-11-10
    body: "I gave it a shot at some of the mirrors listed at KDE's site. None of the ones I tried worked out. But with the above link my upgrade completed successfully, with exception of kpower, and some skins-updates. I thought the mirrors were standardized, but apparently they're not!"
    author: "Carit"
  - subject: "Re: Why does he not use YaST?"
    date: 2004-09-13
    body: "I used SuSE's supplementary packages too - except not through Yast but through apt4rpm.\n\nEverything's working great - except for the Kmilo service. I was really looking forward to this one to see some eye candy on my thinkpad. But the thing just doesn't work. \n\nWhat's even more curious is that when I built KDE 3.3 beta using konstruct - kmilo worked perfectly! Donno what's wrong.. there's not much help online either.\n\nAnyone had success with kmilo using SuSE's rpms?"
    author: "antrix angler"
  - subject: "Re: Why does he not use YaST?"
    date: 2004-09-13
    body: "Kmilo isn't in Suse 9.1 (kde 3.2.1, 3.2.2 , 3.2.3 and 3.3) packages.\nDon't know why :)\n\nMaybe contact Kde Suse packager at adrian_|_@_|_suse.de"
    author: "Bellegarde C\u00e9dric"
  - subject: "Re: Why does he not use YaST?"
    date: 2004-09-14
    body: "Kmilo is there it is just disabled by default. \n\nTo enable it, edit\n\n/opt/kde3/share/services/kded/kmilod.desktop\n\nOn line #8 in that file change it as follows:\n\nX-KDE-Kded-autoload=true\n\nLog out your KDE and log back in again. Thinkpad butotns should be\nworking with kmilo thinkpad plugin.\n\n"
    author: "Osho"
  - subject: "Easy installation and dependancy resolving..."
    date: 2004-09-13
    body: "Quote from the article:\n\n\"At the risk of being called a troll, I say that's just the wrong attitude to have. Installation (of device drivers as well as software) is an area in which Linux still has a very long way to go. I don't want to bake the cupcake, blast it, I just want to eat it! \"\n\n\nYou're not a troll, but I don't think you've heard about the \"alternatives\"\n\nSome linux distributions use an online repository of programs. With one single instruction, you can update your complete system, including kernel and drivers.\n\nTake for example Gentoo, which uses portage (via the emerge command), and Debian, which uses apt-get I think.\n\nI'm not sure, but I think some of these tools are also available for Suse or Mandrake. So it's not really a \"linux\" problem, but more a distribution problem, in my opinion. In other words, it's up to Suse for example to make it easier, not \"linux\".\n\n\nHere's a part of the output if I do \"emerge -uDpv world\" on my gentoo pc:\n\n...\n[ebuild  N    ] media-libs/libao-0.8.5  +alsa +arts -esd -mmap -nas  261 kB\n[ebuild  N    ] media-sound/vorbis-tools-1.0.1  -debug -flac +nls -speex  700 kB\n[ebuild  N    ] kde-base/kdemultimedia-3.3.0  +alsa +arts -audiofile -cdparanoia -debug -debug +encode -flac +oggvorbis -speex -xine  5,231 kB\n[ebuild  N    ] kde-base/kdeaddons-3.3.0  +arts +arts -debug -debug -esd +sdl +svga +xmms  1,517 kB\n[ebuild  N    ] kde-base/kdeedu-3.3.0  +arts -debug  21,494 kB\n[ebuild  N    ] kde-base/kdegames-3.3.0  +arts -debug  9,086 kB\n[ebuild  N    ] kde-base/kdetoys-3.3.0  +arts -debug  2,696 kB\n[ebuild  N    ] kde-base/kdeadmin-3.3.0  +arts -debug +pam  1,520 kB\n[ebuild  N    ] kde-base/kde-3.3.0   [empty/missing/bad digest]\n[ebuild     U ] app-arch/cabextract-1.0 [0.6]  185 kB\n[ebuild     U ] media-sound/amarok-1.0.2 [0.9] +arts +arts -cjk -debug -gstreamer +opengl -xine +xmms  1,993 kB\n...\n\nWhen I start the emerge, all dependencies will get downloaded and installed.\n\nThe same with apt-get I guess.\n\n\nConclusion:\nIn my opinion, your quote: \"Installation (of device drivers as well as software) is an area in which Linux still has a very long way to go.\", is not really true anymore. At least for me, as it's now even more easy than on windows :)"
    author: "tbscope"
  - subject: "Re: Easy installation and dependancy resolving..."
    date: 2004-09-14
    body: "you can do the same with yast, apt, urpmi, yum................."
    author: "Andre Lourenco"
  - subject: "Re: Easy installation and dependancy resolving..."
    date: 2005-10-30
    body: "hey man!\n\nit's really hard for you...??!\n\ndon't talk without knowledge man. windows' creepy, be sure of that. :P"
    author: "masterBlaster"
  - subject: "Ever heard of yast-source?"
    date: 2004-09-13
    body: "Sometimes it's good to have a look at mailinglists or even the odd Suse Website itself.\nYou could have used yast-source then und would not have any problems. If you do things in a fuzzy way don't be surprised if it doesn't work the way you expect it to work.\nGreetings from a newbie\n\nChristopher"
    author: "Christopher Winkler"
  - subject: "About the 'nonsense' of the command line..."
    date: 2004-09-15
    body: "Hi! \n\nI don't know what's my level of experience on linux. I can install a program from the source (./configure;make;make install), I use it every day with the programs which SuSE has prepared.\n\nBut when I tried to install KDE 3.3 from 3.2 (and before, 3.2 from 3.1!) I just followed the instructions in SuSE's (or KDE's, I can't remember, but there was a README file in the same directory of KDE rpms) site:\n\nrpm -<something> *rpm\n\nThere were some problems of dependencies, resolved uninstalling the packages with Yast. Simple like drinking a glass of water.\n\nrpm -<something> *rpm\n\nMagic! Welcome to KDE 3.3!!!!\n\nWhat's the nonsense? \n\nIt's very simple to click 'setup.exe'... but could I remember you that KDE isn't a single program like Photoshop, but AN ENTIRE ENVIRONMENT? It's not like installing Photoshop. Here is like upgrading ALL GRAPHICAL ENVIRONMENT of Windows. Are you sure that in windows you could do it with a \"setup.exe\", without bloody dependencies?\n\nOh yes, the runlevel! You really think that the way to exit the graphical environment is change the runlevel from the file inittab, or what, etc... There's a funny option in the Grub bootloader in SuSE, named failsafe. When you boot your computer, you select 'failsafe' and the game is done. \n\nI'm a simple user, but I think that sometimes trying to do all with click-click is more difficult than following a simple instruction giving the right command from the console:\n\nrpm -<something> *rpm\n\nMatteo \n"
    author: "Matteo "
  - subject: "KDE 3.3 with suse 9.1 installation"
    date: 2004-09-24
    body: "Hi, so what would be the best way to get KDE 3.3 with a fresh Suse 9.1 installation? Last time I installed Suse from ftp and I thought it would get the latest version of KDE but it didn't. Should I not install KDE during the Suse installation and then just download all the KDE rpms and install them? thx"
    author: "kid a"
  - subject: "Re: KDE 3.3 with suse 9.1 installation"
    date: 2004-09-29
    body: "kid,(lol)\nWith any install, new or old. the afor mentioned is the most effective...\n1. Select a Mirror at http://www.suse.de/en/private/download/ftp/int_mirrors.html\n\n2. Add:  ftp:// <your mirror> /pub/suse/ <platform> /supplementary/KDE/update_for_9.1/yast-source\nexample: ftp://ftp.suse.com/pub/suse/i386/supplementary/KDE/update_for_9.1/yast-source\nto the YaST \"Install Source\" list. \n\n3. Execute the  YaST \"System Update\" this will examine your packages and address dependancies (it selected all my dependancies for me).  Then you are in business ;)"
    author: "Smoke"
  - subject: "Re: KDE 3.3 with suse 9.1 installation"
    date: 2005-01-11
    body: "Well I found this thread with a search through google and I will say that it has been most helpful, and I would like to pass on a few things that I found while following smoke's 3 steps.  one thing I would like to stress is it would be wise to use another mirror besides the suse domain itself because suse seemed to be awfully slow downloading, and some of the mirrors were lightning fast, and smoke has given the links for those mirrors.  second when i was adding an instalation source, it worked fine but then when i did system update it would not show the new kde files, it showed that i was up to date.  well i tried making that install source type \"http\" instead of ftp, even though the link was ftp.whatever, and then when I did yast system update it worked fine, it may work different on other systems but that worked for me. maybe this will save someone an hour or two of searching."
    author: "pimpalize"
  - subject: "I have to agree"
    date: 2004-11-11
    body: "This is old but to have my 2c as I feel it's the most important issue Linux has when trying to convert windows users. I like Linux, I *want* it to succeed, I am not trying to troll but to point out what I find is a major annoyance.\n\nI can, and have upgraded KDE manually and via YAST2 so I know whats involved. But I still feel the windows way of having complete binaries with no dependencies is vastly more simple and desirable. It can be done with Linux apps eg. I love that fact that mozilla is *complete*, but it rarely is.\n\nTake my recent upgrade to 3.3.1, I have a machine that cannot be connected to the net thus I have to either upgrade manually which results in dependency hell (rpm -Fhv *.rpm does not work!) as outlined in the article. Or I can create a local yast source, which is what I did, but that is not simple especially for novices users.\n\nIt is simply harder to upgrade and install most software under linux than windows, and there's no reason why it should be. "
    author: "soob"
  - subject: "Giving SUSE another look..."
    date: 2004-11-29
    body: "Um, the article itself was a bit confusing, but the comments here made me rethink SUSE.  I had thought that it was just another one of those proprietary distros trying to get you to upgrade every year by plunking down your hard earned cash for a new set of CDs.\n\nThe YaST upgrade method was every bit as easy as any M$ upgrade I've ever done.  Thanks zurab for the step by step instructions, and anonymous & steve for the URLs.  I actually had some dependency problems, but then added two extra source locations after KDE for GNOME and SUSE 9.1:\n\n/pub/suse/i386/supplementary/KDE/update_for_9.1/yast-source\n/pub/suse/i386/supplementary/GNOME/update_for_9.1/yast-source\n/pub/suse/i386/9.1\n\nThis allowed YaST to see the missing imlib2 libraries I needed for Digikam to be upgraded.  I don't know if the order matters, but the SUSE base directory above has the older KDE 3.2 in it, so I listed it last.  I don't actually use GNOME, but including that directory helped for the few GTK2 apps that I use, (Firefox, Gimp).\n\nI had to swap out my original SUSE 9.1 CDs for about 15 minutes, then the rest took about an hour to download and install by itself.  It occurred to me later, that if I had disabled the original SUSE CD source and relied on the SUSE directory source instead, then the WHOLE thing would have upgraded unattended!  I may install this on my ThinkPad now.  Oh, and I didn't mess with runlevels.  After the upgrade, I just rebooted.  As \"picky\" as Windows users are, they are used to rebooting.\n\nHawkeye\n"
    author: "Hawkeye"
---
There's an old joke about how a burgler makes a cake. The recipe begins "To bake a cake, first steal two eggs". Of course, if you really don't want to do any baking in the first place, and all you really wanted was a simple pre-packaged cupcake, all this recipe stuff (not to mention hunting down the different ingredients needed) may be more trouble than it's worth. Occasionally, that's been my experience migrating from Windows to Linux. One such occasion has been my recent attempt to upgrade KDE from version 3.1 to 3.3. Only in this case, the instructions were: "To install KDE, first find one recipe". Read more in 
<a href="http://linuxgazette.net/106/brown.html">this month's issue</a> of LinuxGazette.



<!--break-->
