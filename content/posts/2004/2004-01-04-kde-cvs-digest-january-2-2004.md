---
title: "KDE-CVS-Digest for January 2, 2004"
date:    2004-01-04
authors:
  - "dkite"
slug:    kde-cvs-digest-january-2-2004
comments:
  - subject: "kde-debian"
    date: 2004-01-03
    body: "\nHi,\n\nseems Kapture will soon be ready to use. Will there be a 3.2 based release, compilable something?\n\nOther than that, e.g. LiveCD, please rock on. It's so good to see people of the two most inportant (to me) projects in Free Software collaborate.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: kde-debian"
    date: 2004-01-08
    body: "Kapture is intended to work with both 3.1 and 3.2. And about being ready to use: there's still lot of work to be done. Take a look at TODO :). However, we are getting to state where we can release 0.1, first public pre-alfa version. It should support the basic operations (update, upgrade) and package-browsing. However, it most probably won't support commiting the changes and if yes, the code will be #if 0'ed for the release: we don't want to break peoples' systems :). The code will need some more testing first. You can of course break your system with the CVS version if you like though :).\n\nPeter\n\nPS: I think i'll be creating some screenshots and posting them somewhere. I'll announce here (in a comment)."
    author: "Peter Rockai"
  - subject: "KMail"
    date: 2004-01-03
    body: "Wow, a lot of KMail fixes.\n\nAnd BTW . . . Thanks Derek for your weekly compilation."
    author: "Turd Ferguson"
  - subject: "Already???"
    date: 2004-01-03
    body: "Those Debian KDE folks are moving *fast*. "
    author: "Rayiner Hashem"
  - subject: "Re: Already???"
    date: 2004-01-03
    body: "Any ideas if this could be extended to also support mandrake's urpmi?\n\nI was thinking what would be cool would be say programs asking the user if they can install packages.\n\nSo when I run, say, kdevelop, and it complains at missing htdig etc etc, it gives me a button to install them. :)\n\n"
    author: "JohnFlux"
  - subject: "Re: Already???"
    date: 2004-01-04
    body: "KPackage already exists, and it does both APT and RPM package installations.  The problem is it sucks.  I have never seen a gui package manager that didn't.  I hope that Kapture can be the first non-sucky one.  Hint:  massive treeviews suck.  Kapture should ditch the idea of a treeview and forget about displaying the complete list of packages at all, anywhere.  It's a waste of time.  Instead, it should provide an easy and fast way to search the package database, so you can find the specific package you want.\n\nMaybe it already works this way, I haven't looked at Kapture yet.  Anyone have screenshots of Kapture for us to look at?"
    author: "Spy Hunter"
  - subject: "Re: Already???"
    date: 2004-01-04
    body: "<a href=\"http://rpmwiz.sourceforge.net/\">\nhttp://rpmwiz.sourceforge.net/\n</a>\n\n"
    author: "OI"
  - subject: "Re: Already???"
    date: 2004-01-05
    body: "synaptic's ok....\n\nproblem is repeated exposure to apt-get is like crack,"
    author: "anon"
  - subject: "Will there be a cure from overconfigurability!"
    date: 2004-01-04
    body: "I recently tried out gnome 2.4 and, after 4 years of using kde as my main\ndesktop, it just gives the impression of limiting you in the way you wanna\nhave things. And, aside of that, the impression that the gnome-developers give\n(be there fault or not, I don't know) is that: \"No there won't be any\nconfig-option added, it just makes things too complicated\"\n(Another way of excusing a lower level, if you ask me...)\n\nMy wish for kde is:\nKeep the current configurability, but add a \"plain user mode\" which has sane\nand predictable defaults. Maybe this should be asked in the \"Desktop settings\nwizard\" as first question...\n\nCall to the usability experts!"
    author: "ac"
  - subject: "Re: Will there be a cure from overconfigurability!"
    date: 2004-01-04
    body: "> My wish for kde is:\n> Keep the current configurability, but add a \"plain user mode\" which has\n> sane and predictable defaults. Maybe this should be asked in the \"Desktop\n> settings wizard\" as first question...\n\nI completely agree on this.. and I think that kde 4.0 should have a global configurator to allow to quick change the behavior of the whole system (like choosing between kicker or a replacement, between the actual kdesktop or a karamba-like one or a new multimedia interactive desktop too). \nThis could give some 'system' themes (something more than kthememanager is doing now) useful to initially customize the desktop for a geek or a businessman or a child or a teenager girl or ... you say it.\n\nP.S: thanks derek as always ^_^\n"
    author: "E.Ros"
  - subject: "Re: Will there be a cure from overconfigurability!"
    date: 2004-01-04
    body: "Would it be possible to add a search functionality to the control center?\n\nAnother idea is perhaps a contents list of all config options available\nin the control center.\n"
    author: "Richard"
  - subject: "Re: Will there be a cure from overconfigurability!"
    date: 2004-01-04
    body: "The Control Center has a search functionality.\nThat's why it has a \"Search\" tab."
    author: "Henrique Pinto"
  - subject: "Re: Will there be a cure from overconfigurability!"
    date: 2004-01-04
    body: ":-)"
    author: "chris"
  - subject: "Re: Will there be a cure from overconfigurability!"
    date: 2004-01-04
    body: "Being a very new user to any Unix desktop environment, I have to say that KDE's control center is awesome.  I have never used Linux before yesterday, although I know my way around a HP-UX system.  So many reviews I have read said yuck! about the control center but this is simply not true.  Every thing is relatively easy to find, more logically grouped than the original Windows control panel and the things you can do.... I'll be here for years just playing.  I certainly have no problem using KDE and I hate being made to feel dumb by all the Wizards, etc in WinXP.\n\nAlso the defaults are very nice - although this may just be the distro I am using - the latest Slax distro since I didn't want to blow away my Win98 desktop.\n\nSince Microsoft is trying to force me to upgrade, I will do just that - to Linux and KDE.\n\nNow if only I could work out what distro to buy....."
    author: "Bob"
  - subject: "Re: Will there be a cure from overconfigurability!"
    date: 2004-01-04
    body: "Go for SuSE! They are the biggest KDE supporters - buy from them and you will help to pay a salary of a KDE developer. Also, their product is top notch - I am running SuSE 8.2 Pro on five computers at home (a network with two servers and three workstations) and it is excellent. I'll be upgrading to the next SuSE Pro when it ships with KDE 3.2 (they have 3.1 as default right now).\n\nPaul."
    author: "Paul Koshevoy"
  - subject: "Re: Will there be a cure from overconfigurability!"
    date: 2004-01-04
    body: "Mandrake is the way to go.  SuSE has supported KDE in the past, but that is now in question since they have been acquired by Novell.  I've been using Mandrake since 7.2 and it is absolutely outstanding.  Other KDE supporting desktops include Xandros, Lindows, Lycoris and Ark Linux."
    author: "Darin Manica"
  - subject: "Re: Will there be a cure from overconfigurability!"
    date: 2004-01-04
    body: "Get your facts straight! SUSE always employed more KDE developers than Mandrake. Mandrake laid off half of their KDE developers (which actually were only two) while SUSE employed and still employs four people working full or most of their time on KDE with others contributing casually."
    author: "Anonymous"
  - subject: "Re: Will there be a cure from overconfigurability!"
    date: 2004-01-04
    body: "Still too few. I think we shall raise more funds and get informed about x-financing. For instance internationalization may get paid by governmental organisations or development aid institutions.  "
    author: "andre"
  - subject: "Re: Will there be a cure from overconfigurability!"
    date: 2004-01-04
    body: ">>still employs four people working full or most of their time on KDE with others contributing casually.<<\n\nFour?? \nI only count 2."
    author: "Anonymous"
  - subject: "Re: Will there be a cure from overconfigurability!"
    date: 2004-01-04
    body: "Sorry, Suse has to serve a market that requests KDE as a standard desktop. Programmers and technicians alwys think in a \"technology-push\" manner. The market decides. Suse hasn't to sell what ximian produces but ximian has to develop what Suse's customers want!"
    author: "Andr\u00e9"
  - subject: "Re: Will there be a cure from overconfigurability!"
    date: 2004-01-04
    body: "Before you decide on a specific distro, I suggest you try both Mandrake, and Suse. Both are excellent. If you are a rich guy, buy them both and try them out, otherwise download Mandrake ISO and install Suse via ftp. Explore and investigate (sort of kicking the tires) to determine which you like better (your preference) then buy. If you have two computers like me, I have both installed."
    author: "Abe"
  - subject: "Re: Will there be a cure from overconfigurability!"
    date: 2004-01-04
    body: "Try Tweak UI/Tweak UI XP and you have 100s of options in one place...\n\nIs there an option in KDE to enhance this poor popup menu behaviour? You always have to wait for submenus popping up. \nI want something like Windows->Control Panel->Tweak UI->Mouse->Menu Speed->Fast.\n\nI want to work as fast as possible and KDE forces me to wait for popup menus.\n\n\n\nWhat distro to buy? Buy Suse, they ship an enhanced KDE version. Mandrake ship a crippled KDE Version.\n\n"
    author: "noname"
  - subject: "Re: Will there be a cure from overconfigurability!"
    date: 2004-01-04
    body: "click on the start menu (K button)\nconfiguration->configure your desktop\n(or whatever it is to bring up the kde configuration)\nChose lookNFeel->Style\nChose Effects tab\nturn off \"Enable GUI effects\".\n\n\n"
    author: "sqlite"
  - subject: "Re: Will there be a cure from overconfigurability!"
    date: 2004-01-04
    body: "Necessary option not found. Still I have to wait some 1/10 seconds for the menu."
    author: "noname"
  - subject: "Re: Will there be a cure from overconfigurability!"
    date: 2004-01-04
    body: "While my machine is pretty fast and I dont have any problem with the menu delays, the option that removes the animation effects (in 3.2 beta 2 on latest Slax) is:\n  Control Center --> Appearance & Themes --> Style --> Effects Panel --> Menu Effect = Disable and Combobox effect = Disable (or just turn the GUI effects off all together).\n\nAnd yeah I have played with tweakUI.  It's almost an essential window install to avoid having to use the registery to fix the annoying defaults for animations, paranioa and especially the bane of my Windows life, autoplay.\n\nCheers  and thanks for all the distribution feedback, Bob.\n\nPS - What is debian like as a Distro.  Lots of people have suggested it, but it doesnt seem to funnel any money back into the Gnu/Linux/KDE world.\n\nBob\n\n"
    author: "Bob"
  - subject: "Re: Will there be a cure from overconfigurability!"
    date: 2004-01-05
    body: "> PS - What is debian like as a Distro. Lots of people have suggested it, but \n> it doesnt seem to funnel any money back into the Gnu/Linux/KDE world.\n\nErm, that's because it's a *community* distro, \ncreated by volunteers, not by a company. \nWhere should the money come from?  ;-)\n\nI haven't used it in years but with the recently \nstarted cooperation effort between KDE and Debian \nI think I'll give it another try.  \n\nFor the moment I let others comment on the merits of Debian.\n\n"
    author: "cm"
  - subject: "Debian is very nice"
    date: 2004-01-05
    body: "Debian is great. It's somewhat more difficult to install than other distros, but it's worth it: you only install once, and keep your system updated forever.\n \nThe .deb package format manages dependences way better than .rpm, and apt allows you to keep your system on the bleeding edge at extreme ease.\n\nDebian doesn't funnel any money back into the KDE/GNU/Linux world because itself _is_ this world. It's not a company, but a volunteer based project.\n\nSome more pros:\n- All the packages included is Free Software. SuSE, Mandrake, etc include proprietary software.\n- It's available for many architectures. Not just x86 and PowerPC, but also alpha, m68k, sparc, s/390, etc.\n- It's not limited to the Linux kernel: it's developing HURD and NetBSD based distros.\n- It includes more packages than any other distro.\n \nIf you don't dare to install plain Debian, you can install it from a live CD such as Knoppix or Mepis.\n \nHope that helps,\n Quique"
    author: "Quique"
  - subject: "Re: Debian is very nice"
    date: 2005-03-04
    body: "hello i'm a brand newbie to linux world i'm actually running\nknoppix installed as Debian on my HardDrive it works superfine\nand i'm learning more every day i've set up the environnement\nas i wanted it still questions but it works well."
    author: "Grenouille"
  - subject: "Re: Will there be a cure from overconfigurability!"
    date: 2004-01-09
    body: "Bob wrote:\n\n> PS - What is debian like as a Distro.\n\nGood enough, I guess, but STAY AWAY from the kde packages at the standard debian apt sources if you are planning on using debian sarge. It just won't work (and is hellishly broken). I had to muck with /etc/apt/preferences and have apt-sources for unstable and had to use the -t flag on apt. Yuck.\n\nInstead, go for the KDE team's .deb packages. At least they've never failed me."
    author: "anonymous"
  - subject: "Re: Will there be a cure from overconfigurability!"
    date: 2004-05-27
    body: "The menu speed delay drives me mad!\n\nI'm a converter, new to Linux and I'm very used to Windows. And while KDE is great, I can work twice as fast in Windows. Windows is very user-friendly, lightning fast and configurable. (compared to kde)\n\nI'm not kidding. Windows XP is much more complete and better than KDE in it's current state. It's much more flexible and user-friendly. You can work much faster in Windows.\n\nAll the things I did in Windows, takes me about twice the time in KDE!\n\nSO HOW DO I CHANGE THE MENU SPEED DELAY??????????????????? (sorry) That's fundamental! You use menues all the time, and it really is a bad design. When you have to wait ages.\n\nOf course, the developers knows that KDE is a bit slow. So things like this should improve the overall experience greatly, speed-wise.\n\nOthers than that, KDE is great, especially since it's free. I'm not going back to Windows.\n\nPlease make the menus popup instantly! It's ridiculous. KDE has no problems with heavy disk-intensive 32-bit BMP-icons like Windows. So there is no reason why there should be a delay."
    author: "Martin"
  - subject: "Re: upgrade"
    date: 2004-01-04
    body: "> Since Microsoft is trying to force me to upgrade, I will do just that - to Linux and KDE\n\nThis is a nice oneliner :-) Almost like \"The box said: use windows 95 or better, so I installed Linux\""
    author: "wilbert"
  - subject: "Re: upgrade"
    date: 2004-01-04
    body: "You mean \"use windows XP or better,...\" of course. ;-)"
    author: "cloose"
  - subject: "Re: upgrade"
    date: 2004-01-04
    body: "of course ;-), but the oneliner I mentioned is already very old, i found it in postings dating back as far as 1997 :-)"
    author: "wilbert"
  - subject: "Re: upgrade"
    date: 2004-01-08
    body: "...you forgot the rest of the quote: \"...and lived happily ever after\" ;-))"
    author: "planetzappa"
  - subject: "Re: upgrade"
    date: 2004-01-09
    body: "Well, I'd consider FreeBSD an upgrade to Windows too. But that's just my bias speaking. KDE works nice on FreeBSD as well. "
    author: "Coolvibe"
  - subject: "The reason why Gnome hasn't the options is"
    date: 2004-01-04
    body: "that the limitations of the Gnome framework break through, i.e. there is no way to configure the GUI of programs like you can do for kde apps (btw. this would solve your problem) via xmlgui. That's why most of the linux users see gnome as a dying horse :-)"
    author: "Ruediger Knoerig"
  - subject: "Re: The reason why Gnome hasn't the options is"
    date: 2004-01-04
    body: "You wrote \"That's why most of the linux users see gnome as a dying horse\" and I take this as a chance to easily disagree.\n\nWhen I joined Desktop Linux 5 years after Server Linux, I did so for the promises of Gnome. Many things now called for, level of expertise (beginner, intermediate, expert) should be available, bindings for all scripting languages.\n\nIt didn't work that way. True, I could write Perl app with GUI in GTK and that was great. But that was it, no real access to the desktop itself.\n\nCompare this to KDE. There we have now Javascript bindings for allmost everything, full development enviroment with UML, GUI designers and great editor (I still wait for Kate to become deeply scriptable) along with soon to be implemented configuration levels.\n\nThe real problem of Gnome is all the broken promises. GNU Object Model ... who is really using that at all, Gnome Office? No! Levels of users? Nah, pretend the user is silly and wants to remain so!\n\nTo me, Gnome was disappointing and I went for KDE and have so far never again received false promises. \n\nMaybe already this year, I am really going to get the scriptable desktop that i want.\n\nYours, Kay\n"
    author: "Debian User"
  - subject: "Re: The reason why Gnome hasn't the options is"
    date: 2004-01-04
    body: "> along with soon to be implemented configuration levels.\n\ndont hold your breath for these. I've seen lots of posts here suggesting them, but no-one's ever promised to implement them.\n\nBut more importantly, user levels are A Bad Thing (tm) from a usability perspective - see this thread: http://lists.kde.org/?t=101405087200001&r=1&w=2\n\n-J"
    author: "ejs"
  - subject: "Re: Will there be a cure from overconfigurability!"
    date: 2004-01-04
    body: "I'm not a usability expert, but I participate on the usability list, at least :) KControl comes up often, it's probably THE hardest thing to get right usability wise without losing all of the configurability it offers.\n\nOne suggestion that has arisen of late is to split it further into KAdmin and KControl, so what was once KControl becomes:\n\nKControl - configure bits of KDE that apply individually to each user\nKAdmin - configure system things like hardware, peripherals, the kernel, KDM, KIOSK, etc.\nKInfo - show system information\n\nIt will both dramatically decrease the number of options in KControl, and stop your average user being confused by admin stuff they don't need to see, or perhaps even shouldn't see on some systems. What do people not on the usability list think about this idea? "
    author: "Tom"
  - subject: "Re: KAdmin, KControl, KInfo"
    date: 2004-01-04
    body: "This looks like a nice idea to me.\n\nMaybe even better to merge KAdmin and KInfo into KAdmin, then.\n\nSo: KControl: all user, desktop settings\nKAdmin: system-wide settings, other not-often needed debugging info/help.\n\nJust maybe,\nregards"
    author: "wilbert"
  - subject: "Re: Will there be a cure from overconfigurability!"
    date: 2004-01-06
    body: "Not sure..\nIt means checking 2 apps to find one item that \"i don't know where is it\".\n\n1st proposal)\nIsn't a simple 'show admin stuff' checkbox in top of kcontrol enough ?\nor 2 option widgets to switch between simple/advanced mode ?\n\n2nd proposal)\nThe search tab is here but nobody has ever seen it! Why don't we use a search LineEdit on top of the left tab in kcontrol?\nThis can lead to an as-you-type pruning of the tree with auto-launch of a config module if it's the only available option at a time.\n\nConclusions : separating into 2 apps will bloat for sure! It's better to review the actual app adding smart functionality (also think at 'coloring' of sysadmin-only items in the tree selector).\n\nWhat do you think ?\n"
    author: "E.Ros"
  - subject: "Kiosk mode"
    date: 2004-01-04
    body: "Once Kiosk mode has a nice GUI we can add an \"DUMB USER\" option. With all the tweaking hidden..."
    author: "ybouan"
  - subject: "Re: Kiosk mode"
    date: 2004-01-04
    body: "Its not just dumb users who want a simpler KDE GUI. I'm a power user who wants optimal efficiency from my GUI, and having a lot of extra toolbar and menu items that I never use is not efficient, nor is it asthetic. If I want to print a frame, I'll use the hotkey, thank you. Having lots of menu and toolbar items makes you have to actually read the menu to use it. When you have a minimal set of items, especially in context menus, you can use the menu from muscle memory.\n"
    author: "Rayiner Hashem"
  - subject: "Re: Kiosk mode"
    date: 2004-01-04
    body: "The advantage of the KDE GUI is its configurability, which makes it so nice to work with, especially as poweruser, who knows, that toolbars and even contextmenues can be configured in the way it perfectly fits one's needs."
    author: "Rischwa"
  - subject: "Re: Kiosk mode"
    date: 2004-01-05
    body: "As I mentioned earlier in this thread, I have only been using KDE for about 3 - 4 days now (and I've taken it to work today - SLAX is brilliant) and I quickly worked out how to configure my toolbars to have just the options I use - especially important for Konqueror.  Just as easy as windows which I have used for years.\n\nMy point:  It seems to me that if a distro is aiming at a novice user then their GUI defaults should be for a novice user and it is easy to do.  If a distro is aiming at a power user then the defaults should match.  \n\nOf cource ask 100 people what these defaults should be and you will get 150 different answers.\n\nSince I am playing with a beta release of KDE I would reasonably expect to see the KDE defaults - ie what the developers have set - to showcase all the features that they have built.   The KDE developers should not make GUI choices for their users and the distribution builders, the users (if they can) or the distro builders (if the users can't) should do this because this is what they are there for.\n\nMy 10 cents anyway.\n\nBob\n\n(PS I can't believe how responsive KDE is.  Blows 98 and XP out of the water in terms of speed and I already can't live without Tabbed browsing - I have put Mozilla on my Win build.)\n\n"
    author: "Bob"
  - subject: "Re: Kiosk mode"
    date: 2004-01-05
    body: ">ie what the developers have set - to showcase all the features that they have built\n\nI don't think this would unify much.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Kiosk mode"
    date: 2004-01-05
    body: "I appreciate the configurability and take advantage of it. It takes hours and hours to get apps looking the way I want them, and updated versions will sometimes wipe out my changes, but KDE is so much better than GNOME that I put up with it.\n\nHowever, my point was that a simplified KDE GUI wouldn't be just for \"dumb users\" like the original poster commented. That's a myth that a lot of KDE people keep perpetuating. They think that the only reason to simplify the GUI is to make it easier for novices. That's a load of crap. Consider people who used NeXTStep (like John Carmack) or the UNIX hackers who have switched to OS X (like Bill Joy). It would take a lot of balls to call those people \"dumb users.\" They are people who appreciate the productivity of a clean and elegant GUI. "
    author: "Rayiner Hashem"
  - subject: "Re: Kiosk mode"
    date: 2004-01-09
    body: "May I add that you probably use kcontrol very little. Maybe once the first time you set up to change the horrible Keramik to Plastik ;) and maybe tweak the stuff you need, and after that, you'll probably never have to touch it again.\n\nAlso, many KCM applets are available elsewhere, like a context (right) click on the desktop gives you the ability to change wallpaper et al.\n\nThe whole kcontrol may be an usability eyesore (well, that's what \"they\" say), but  you generally don't use it much, so the point is pretty much moot. Unless of course, all a GNOME user does is tweak fonts, wallpapers and themes all day and doesn't get any work done :)\n\nKDE doesn't get in your way, even if it is very configurable. Which is mainly why I use it."
    author: "Coolvibe"
  - subject: "Re: Kiosk mode"
    date: 2004-01-27
    body: "I'm working on a project in Suse 9.0 using KDE 3.1.4.  I see in a lot of places where people say that there is a built in kiosk mode now.  Nevertheless, up to know I have not been able to find it and have been able to find no references to how to set it up and implement it.  I was'nt even able to find info concerning it on the Suse or KDE website other than a reference that it would be a feature in the 3.3 version of KDE.  Any assistance is appreciated.\n\nKeith"
    author: "Anyone know how to implement Kiosk Mode?????"
  - subject: "Re: Kiosk mode"
    date: 2004-01-27
    body: "Visit http://www.kde.org, click on \"Sysadmin Documentation\". Difficult, eh?"
    author: "Anonymous"
  - subject: "Re: Kiosk mode"
    date: 2004-01-29
    body: "Really difficult.  I saw that one before but either it's not specificaly telling me or I'm not understanding how to configure it.  I'm not seeing what file or files I need to change and what part specifically needs to be edited to do it.  I see that they explain how it works but not actually how to do it."
    author: "Keith"
  - subject: "Apps KDE"
    date: 2004-01-04
    body: "And don't forget Apps KDE is back..."
    author: "gerd"
  - subject: "Re: Apps KDE"
    date: 2004-01-04
    body: "No, it isn't. But you may use\n<a href=\"http://kde-apps.org\">http://kde-apps.org</a> instead.\n"
    author: "Anonymous"
  - subject: "Re: Apps KDE"
    date: 2004-01-04
    body: "Looks excellent."
    author: "OI"
  - subject: "Re: Apps KDE"
    date: 2004-01-04
    body: "...looks quite empty.\n\nAre the contents of apps.kde.com lost ?\n\nBye\nAlex"
    author: "aleXXX"
  - subject: "Re: Apps KDE"
    date: 2004-01-04
    body: "> ...looks quite empty.\n\nIt's new and didn't have a big announcement until now. As it's now it's good for new application releases (submitted by the authors) and with time and once new functionality of the system to allow submission from foreign people is available, its content will get more comprehensive.\n\n> Are the contents of apps.kde.com lost ?\n\nNo, but its provider requests a \"sponsor ransom\" to make them accessible again."
    author: "Anonymous"
  - subject: "Re: Apps KDE"
    date: 2004-01-04
    body: "I'm not sure if you mean <a href=\"http://www.kde-apps.org/\">http://www.kde-apps.org</a>,\nbut <a href=\"http://www.kde-apps.org/\">http://www.kde-apps.org</a> is great!\nI don't know if <a href=\"http://www.kde-apps.org/\">http://www.kde-apps.org</a> is a new site,\nbut after going to <a href=\"http://www.kde-apps.org/\">http://www.kde-apps.org</a> I bookmarked\n<a href=\"http://www.kde-apps.org/\">http://www.kde-apps.org</a> and will visit <a href=\"http://www.kde-apps.org/\">http://www.kde-apps.org</a> often.\n\nJust in case you missed it, that's <a href=\"http://www.kde-apps.org/\">http://www.kde-apps.org</a> :)\n\n\n"
    author: "ac"
  - subject: "Re: Apps KDE"
    date: 2004-01-04
    body: "What's that URL again?  \n\nhehe"
    author: "jstuart"
  - subject: "Re: Apps KDE"
    date: 2004-01-04
    body: "http://WWW.KDE-APPS.ORG works too!\n\n(for the n00bs, check-out http://www.kde-look.org too!)"
    author: "ac"
  - subject: "Re: Apps KDE"
    date: 2004-01-04
    body: "Can anybody try to slashdot this site?"
    author: "Gerd"
  - subject: "Re: Apps KDE"
    date: 2004-01-05
    body: "You can, if you want.  Anyone can submit slashdot stories."
    author: "Spy Hunter"
  - subject: "Re: Apps KDE"
    date: 2004-01-04
    body: "So it now belongs to the KDE family? Hope this site could be integrated into the KDE framework in order to look up whether new versions of the program you are actually using are available. \n\nKde Apps - wow! where's the press release?"
    author: "Gerd"
  - subject: "Osnabr\u00fcck Branch???"
    date: 2004-01-04
    body: "I live in Osnabr\u00fcck -- so what is the Osnabr\u00fcck Branch?\n\n???\n\nKturtle as a new App... two logo interpreters???"
    author: "Andr\u00e9"
  - subject: "Re: Osnabr\u00fcck Branch???"
    date: 2004-01-04
    body: "<a href=\"http://pim.kde.org/news/news-2004.php#30\">\nhttp://pim.kde.org/news/news-2004.php#30</a>\n"
    author: "Anonymous"
  - subject: "Re: KTurtle..."
    date: 2004-01-04
    body: "Yes 2 logo interpreters: KLogoTurtle and KTurtle...\n\nThe latter is a project I started because I couldnt help developing the first. The main reasons for writing my own Logo interpreter:\n- KLogoTurtle had class names, funktion names and comments in pt_BR (potugese)\n- it uses QPainter, i did it with QCanvas\n- it has really bad written interpreter code, with hardcoded commandnames (I intergrated the WSBASIC project and use an XML file for command internationalisation)\n  \nI also have a website online but, right now i'm mostly focussing on the development in CVS (kdenonbeta/kturtle) and not on releasing. I want to have a few issues solved before I want to releas it in packages. I you wan to know what issues see the (kdenonbeta/kturtle/)TODO file.\n\nIf you want to see the unfinished website see: http://kturtle.sf.net\nKLogoTurtle also has a website: http://klogoturtle.sf.net\n\nIf you know how to use KDE form CVS please checkout the latest kdenonbeta/kturtle and tell me what you think about it. Yes a lot of things arent working yet... but a lot of thing are. See the XML file for the commands you may use: kdenonbeta/kturtle/src/data/*.xml\n\n\nI hope this explaines your question Andre.\n\nCies.\n\n[Logo -> MSX basic -> QBasic -> C++/Qt/KDE :) ]"
    author: "Cies Breijs"
  - subject: "Re: KTurtle..."
    date: 2004-01-04
    body: "Ohh... and if you wish to give KTurtle a try here is some sample code:\n\n\n[\ncanvassize 400,400\ncanvascolor 200,200,100\n\n  repeat 36 [\n    go 200,200\n    forward 50\n    turnleft 10\n  ]\n\npencolor 255,0,0\nprint \"ktrulte v0.1\"\n]"
    author: "Cies Breijs"
  - subject: "Re: KTurtle..."
    date: 2004-01-04
    body: "Thanks a lot for your explanation. Hope you will take over at least the old turtle logo for your advanced \"K-Logo\"- Interpreter Kturtle. Will the source editor use Kpart technology?\n\nInternationalization of commands: I think this is *very* helpful.\n\nbad interpreter code: I think the most important thing is, that it actually works. A logo interpreter doesn't have to be speed optimized. It must be easy to use and just work. Nobody cares about dirty hacks. \"Easy to use\" and \"just works\" is crucial for an educational program. I hope you will not overload the interpreter.\n\nBasic:\n\nI installed hbasic (http://hbasic.sf.net) a few days ago, very nice and ambitious, but it seems to be QT, not KDE. Kbasic is dead.\n\nQbasic works fine with Dosbox emulator\nhttp://dosbox.sf.net\nI already played gorilla.bas\n\nGr\u00fcsse,\nAndr\u00e9"
    author: "Andr\u00e9"
  - subject: "Re: KTurtle..."
    date: 2004-01-04
    body: "> Will the source editor use Kpart technology?\n\nYes! But i did two tries to implement it and i failed. So are you comfortable with kparts? (i not talking to Andre in particular) Please help me out...\n\n> bad interpreter code: I think the most important\n> thing is, that it actually works.\n\nMe too, and it works. With bad interpreter code i mean not build for the future (i18n, more commands, expressions, selfmasde funktions, etc). I could neve write GOOD interpreter code so i used WSBASIC, which porvided very clean and staight forward interpreter code -- a beautyful starting point.\n\n> I hope you will not overload the interpreter.\nCan you explain yourself a bit?\n\n\nTsjuus! (i forgot how to write it :-)\nCies.\n\nP.S.: can you mail me db-#-showroommama-!-nl?"
    author: "cies"
  - subject: "Re: KTurtle..."
    date: 2004-01-05
    body: "Tsch\u00fcss\n\n= adj\u00fcs, adjes, ade, adios, adieu"
    author: "elektroschock"
  - subject: "Re: KTurtle..."
    date: 2004-01-05
    body: "mazzel, later, laters, doei, doeg, doe-wie, tot sinas... etc.\n\nIn Dutch :-)"
    author: "cies"
  - subject: "Re: Osnabr\u00fcck Branch???"
    date: 2004-01-04
    body: "Hallo, Andr\u00e9!\n\nIch komme auch aus Osnabr\u00fcck/Bohmte. Schicke mir bitte mal eine Mail an kde-package-at-gmx.de .\n\nGru\u00df, Ren\u00e9"
    author: "der Mosher"
  - subject: "Re: Osnabr\u00fcck Branch???"
    date: 2004-01-06
    body: "Hehe, I also live in Osna :-) KDE-meeting?"
    author: "Carsten Niehaus"
  - subject: "Re: Osnabr\u00fcck Branch???"
    date: 2004-01-06
    body: "Yup! :-)"
    author: "der Mosher"
  - subject: "little buggers"
    date: 2004-01-04
    body: "KDE 3.2 beta 2 on Fedora Core 1 under VMware, .rpm binaries from beta2 release.\n<br><br>\nKControl -> Regional & Accessibility/Keyboard Shortcuts -> Shortcut Schemes -> Windows Scheme (With Win Key)\n<br><br>\nPressing Win key doesn't bring up the K menu<br>\nhttp://bugs.kde.org/show_bug.cgi?id=69862 or http://bugs.kde.org/show_bug.cgi?id=69862\n<br><br>\nContext menu key works though. None of the tips in those bugs helped. My configuration bad or regression?\n<br><br>\n\nIn Redmond theme there are still fugly borders around maximized windows\n<a href=\"http://bugs.kde.org/show_bug.cgi?id=56654\">\nhttp://bugs.kde.org/show_bug.cgi?id=56654</a>\n<br><br>\nAlso: <a href=\"http://bugs.kde.org/show_bug.cgi?id=43063\">\nhttp://bugs.kde.org/show_bug.cgi?id=43063</a>\n\n\n"
    author: "Tar"
  - subject: "Re: little buggers"
    date: 2004-01-04
    body: "grr... I meant:<br>\n<a href=\"http://bugs.kde.org/show_bug.cgi?id=68875\">\nhttp://bugs.kde.org/show_bug.cgi?id=68875</a> or \n<a href=\"http://bugs.kde.org/show_bug.cgi?id=69862\">\nhttp://bugs.kde.org/show_bug.cgi?id=69862</a> for the Winkey bug.\n"
    author: "Tar"
  - subject: "Re: little buggers"
    date: 2004-01-04
    body: "whom are you talking to?"
    author: "me"
  - subject: "Re: little buggers"
    date: 2007-06-10
    body: "See the solution at\nhttp://forums.fedoraforum.org/forum/showpost.php?p=557214&postcount=10\n\n"
    author: "J. Smith"
  - subject: "KDE3.2 is very impressive"
    date: 2004-01-05
    body: "In installed KDE3.2 (recent CVS version) from source on my Debian/SID notebook (650 MHz PIII with 128 MB RAM) and I am very impressed how fast and snappy everyhing feels. I really like how Konqueror improved: the new Tabs are great, khtml becomes better and better (even though I found some pages, that are rendered correctly with Konqueror from 3.1.4 but not with Konqueror 3.2). I also love Konsole, the new tabs look great and Konsole is definitely the best X-terminal I ever used on Linux. Another great application is Kate, which will soon become my default text editor (I use emacs right now). KMail is also getting better and better and I think it will replace mutt pretty soon. The new Plastik-theme is great (but I already use it with KDE3.1 as default).\n\nBut I also have some complaints, which, maybe, can be addressed in KDE3.3/4.0. Everythime I read a KDE3.2 review, I find some (hidden) features which makes KDE even more powerful. I am using KDE since before 1.0 and there are so many nice features which I don't know. There should be a way to find out what all these features are doing. One way (but I don't know if this is possible) would be a flash or SVG movie coming with each application, that explains the features. I mean something like www.apple.com/safari. On the right you can choose some quicktime movies that explain some of the features of Safari. Just image if every KDE application has some movies that explain important features.\n\nAnd. like many other people stated before, I also think KDE should be simplified. Take konqueror (webbrowser) for example: there are so many nice features that it is really hard to find the ones that are most useful for you. I recently tried mozilla firebird and I really like it. I would really like to see a standalone browser for KDE that is only doing one thing: showing web-pages. Konqueror has so many features that it confuses users, espacially new ones and most people are either using konqueror as browser or as filemanager, but not for both at the same time. I think, the best thing would be to have konqueror like it is today for experienced users, but also a safari-type browser that offers a simple, powerful interface to browse webpages and also a simple, powerful filemanager for not so experienced users. Since e.g. khtml is a kpart, anyway, it should not be too hard to write something like a KDE version of safari, anyway. In a way, this is similar to Kontact. You can use e.g. just KMail or you can use the more complicated and powerful contact."
    author: "Michael Thaler"
  - subject: "A separate, simple webbrowser besides Konqueror"
    date: 2004-01-05
    body: "This is a very good idea IMHO.\n\nJust like there is KWrite besides Kate. Both use the same technology, but a simple Konqueror-based browser would just offer a simpler userinterface. It would not embed anything but text and images (the rest would open in separate applications). It would only offer a few medium sized toolbar buttons by default. The browsers would share the same config.\n\nOr: make the default configuration for Konqueror as simple and light as possible, like above. Advanced users can always finetune the behaviour. But the main menus will still be too big and powerful maybe."
    author: "wilbert"
  - subject: "Re: A separate, simple webbrowser besides Konqueror"
    date: 2004-01-05
    body: "> But the main menus will still be too big and powerful maybe.\n\nYou can likely change that with xmlgui files too."
    author: "anon"
  - subject: "Re: A separate, simple webbrowser besides Konqueror"
    date: 2004-01-05
    body: "Interesting in this context: http://lists.kde.org/?l=kde-cvs&m=107324322912040&w=2"
    author: "Anonymous"
  - subject: "Re: KDE3.2 is very impressive"
    date: 2004-01-05
    body: "Reading the \"Tips of the Day\"s and application documentation is a good idea."
    author: "Anonymous"
  - subject: "Re: KDE3.2 is very impressive"
    date: 2004-01-05
    body: "Application documentation is empty by 90%. KHelpcenter is a tragedy. "
    author: "noname"
  - subject: "Re: KDE3.2 is very impressive"
    date: 2004-01-05
    body: "Yes, more contributors to docs would be greatly appreciated. "
    author: "anon"
  - subject: "Re: KDE3.2 is very impressive"
    date: 2004-01-05
    body: "Luckily this is an area where everyone posting here can help."
    author: "Anonymous"
  - subject: "Re: KDE3.2 is very impressive"
    date: 2004-01-05
    body: "So let's get our act together and start writing those docs. Everyone can help here.\n\nFab"
    author: "Fab"
---
In <a href="http://members.shaw.ca/dkite/jan22004.html">this week's  KDE-CVS-Digest</a>:
<a href="http://kmail.kde.org/">KMail</a> gains a basic anti-spam wizard. 
A new version of the SSLIODevice and SSLServerSocket code. An alpha version of the Debian KDE LiveCD was imported. Speedups in khtml and kjs. And many bugfixes.


<!--break-->
