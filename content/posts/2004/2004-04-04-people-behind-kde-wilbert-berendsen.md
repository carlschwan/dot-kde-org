---
title: "The People Behind KDE: Wilbert Berendsen"
date:    2004-04-04
authors:
  - "Tink"
slug:    people-behind-kde-wilbert-berendsen
comments:
  - subject: "great .signature"
    date: 2004-04-04
    body: "\"Reporter: Mr. Ghandi, what do you think of western civilisation?\nGhandi: I think that would be a great idea.\"\n\nfunny...if it wasn't so true... sad t'is is\n"
    author: "Thomas"
  - subject: "Thanks"
    date: 2004-04-05
    body: "Wilbert for being such a cool webmaster :)\n\nFab"
    author: "Fabrice Mous"
  - subject: "the guy's clearly a genius"
    date: 2004-04-05
    body: "> or even abandoning artsd and use the ALSA mixing capabilities.\n\nwhat a fantastic idea! :)"
    author: "cbcbcb"
  - subject: "Re: the guy's clearly a genius"
    date: 2004-04-07
    body: "Well, of course arts supports much more, like effects and serverside decoding of ogg etc, than plain ALSA. But maybe it would allow for lighter setups on simple hardware...not sure. Maybe integrating JACK is the best option, but I'm no guru :)"
    author: "wilbert"
  - subject: "Reporting bugs is fun."
    date: 2004-04-06
    body: "Being ignored when you report bugs is not fun.  It sucks.  It's common, especially with kmail.  Blah."
    author: "anon"
  - subject: "MusicXML"
    date: 2004-04-07
    body: "\"- You're stuck on a bus for 6 hours and are bored out of your skull. What do you do to amuse yourself?\n Write an XML standard for defining musical notation and start coding a DOM and Konqueror plugin for it.\"\n\nThere already is such a thing (MusicXML): http://www.recordare.com/xml.html\nYou only have to write the plugin :-)"
    author: "Magnus"
  - subject: "Re: MusicXML"
    date: 2004-04-07
    body: "Yes, I know MusicXML, and it has already gained some momentum, but I do not really like the document model. It is too much purely sequential, like MIDI. I lurked a while in a mailing list thinking about creating an OASIS commitee to develop an XML standard for music notation. Many people had similar complaints wrt MusicXML.\n\nI would like the model to be more hiearchical, and I even did already start some planning and thinking, and posted my musings at http://www.frescobaldi.org/. I want the format to be suitable to be presented in a browser-like interface, and editable via a DOM-like interface, complete with CSS support for scaling or coloring notes, parts, texts, etc.\n\nIf only I had some spare time... :-) ... I'm not really an experienced coder as well..."
    author: "wilbert"
  - subject: "Re: MusicXML"
    date: 2004-04-07
    body: "I personally would like to see a music description system which doesn't name the played notes directly but instead defines the available scale and modes, doesn't define the melodies directly but define them as something akin to chords and then spread its notes on a time scale, stuff like that. ^^ It would probably end up being more a scripting language where every module is replaceable, not really an XML with the main purpose of turning a score into a good looking notation."
    author: "Datschge"
---
<i><a href="http://www.kde.nl/people/">The People Behind KDE</a></i> is staying on base this week. The guy I'm interviewing is multi-talented and full of surprises, not only is he <a href="http://www.kde.nl/">KDE-NL</a>'s own webmaster/translator/bug-submitter but also a musician and teacher. His favorite thing is his low-racer. You think a low racer is a kind of jeans style? Guess again... ;-] Get to know the person behind KDE-NL's website, <a href="http://www.kde.nl/people/wilbert.html">Wilbert Berendsen</a>!

<!--break-->
