---
title: "Real World Linux Slides Online"
date:    2004-04-27
authors:
  - "gstaikos"
slug:    real-world-linux-slides-online
comments:
  - subject: "Please make presentation with bigger images :)"
    date: 2004-04-27
    body: "Images are very small :( difficult to see anything :("
    author: "Bellegarde Cedric"
  - subject: "Re: Please make presentation with bigger images :)"
    date: 2004-04-27
    body: "The original OOImpress document is there if you want something bigger.  The bandwidth required for larger images would be a bit much.  I don't want the server to get too slow under the stress. :-)"
    author: "George Staikos"
  - subject: "Hard to be productive"
    date: 2004-04-27
    body: "when there are hockey games to be watched. Slide 13.\n\nI know I've been watching.\n\nDerek"
    author: "Derek Kite"
  - subject: "linux slides"
    date: 2005-04-12
    body: "i must be verythankfull to you for that."
    author: "M BABAR"
  - subject: "Re: linux slides"
    date: 2006-04-18
    body: "To teach Linux"
    author: "Ilhem"
---
This month I had the opportunity to speak about KDE, what KDE is doing to <i>move into the enterprise</i>, and present a case study of a company who moved from a Microsoft Windows platform to Linux and KDE.  The presentation was at <a href="http://www.realworldlinux.com/">Real World Linux</a> in Toronto, and Robert Brodie of <a href="http://www.displayworksinc.com/">Display Works Inc.</a> joined me to talk about their migration experiences. The slides are finally available online, both <a href="http://www.staikos.net/~staikos/presentations/April2004/rwl/">mine</a> and <a href="http://www.staikos.net/~staikos/presentations/April2004/dwi/">Robert's</a>.  In addition, the original <a href="http://www.staikos.net/~staikos/presentations/April2004/kde-beyond-desktop.sxi">OOImpress document<a/> of my slides is online.


<!--break-->
