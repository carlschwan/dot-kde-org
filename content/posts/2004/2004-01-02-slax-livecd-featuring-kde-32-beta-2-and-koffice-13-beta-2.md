---
title: "SLAX: A LiveCD featuring KDE 3.2 Beta 2 and KOffice 1.3 Beta 2"
date:    2004-01-02
authors:
  - "evangineer"
slug:    slax-livecd-featuring-kde-32-beta-2-and-koffice-13-beta-2
comments:
  - subject: "Which hardware recognition?"
    date: 2004-01-02
    body: "Ha! Seems that now /me can finally also savely try one of these Slackware varieties...   ;-)\n\nWhich hardware recognition does it use?"
    author: "Kurt Pfeifle"
  - subject: "Slackware & KDE"
    date: 2004-01-02
    body: "As a long standing fan of slackware, I must say that this project is a good thing (tm).  For a while, I was trying to do something similar but as an installable distro (not a CD-based distro), but never had the time.\n\nNow I use slackware whenever freebsd is not suited, and will continue to be loyal to it as the king of all hard-core distros!  Now, I can have my KDE demo disc a slackware system -- groovy! :P\n\n/me tips his hat.\n\nTroy Unrau\n(bored to tears? read my blog -- http://tblog.ath.cx/troy)"
    author: "Troy Unrau"
  - subject: "Great!"
    date: 2004-01-02
    body: "Finally! A live-cd with KDE 3.2 Beta2! *downloads*"
    author: "Eike Hein"
  - subject: "Slackware"
    date: 2004-01-02
    body: "I'm currently a SuSE user.\nI'm no linux expert, but I'm not afraid to compile/make\nthings my self and solve most things on the command line.\nBut I don't know any little setting in /etc however.\n\nNow my questions are:\nDebian, Slackware - what are the differences / main characteristics\nof these distributions? What other really \"free\" (in contrast to\ncommercial distros like SuSe or RedHat) are there? Which one should\nan average linux user choose? Or isn't this recommend at this point of time?\nHow is the development of these destros in terms of user friendliness?\nIs there any progress in this sector? Are there i.e. tools to setup my\nCD drive? What about hardware recognition? Does it work for most systems?\nI recently switched my graphics card and was afraid that my Linux system\ndidn't even boot but to my surprise everything worked fine. (It was the\nfirst time I changed something since I've switched to Linux). Would this\nwork as flawlessly with a Debian/etc. distro? Would I be able to configure \na Debian/Slackware/.. system myself without too steep a learning curve?\n\nSo, basically, I'm quite interested in having a really free distro, but\nthese questions are still bothering me. If anyone enlightened me\non these things that'd be great.\n\nTIA\nMike\n"
    author: "Mike"
  - subject: "Re: Slackware"
    date: 2004-01-02
    body: "I installed KDE 3.2 BETA2 on three machines with SuSE 8.2: works fine, no glitches. So, no need to worry.\n\ncu\nstonki\n"
    author: "Stonki"
  - subject: "Re: Slackware"
    date: 2004-01-02
    body: "Huh? Are you referring to my question?\n"
    author: "Mike"
  - subject: "Re: Slackware"
    date: 2004-01-02
    body: "The point of these distro's is not user friendlyness.\nThey are quite easy to setup and config ... once you know where everything\nis and once you know every piece of hardware in your box.\nBut it's still editing text files.\nPretty simple and straightforward once you've figured it out.\nMy guess is a learning curve of a couple of months. But after\nthose months, you'll know what is what and why it is used, etc.\nAutomatic hardware recognition is close to zero :)\nSlack sets up your NIC and that's about it :)\nThe main goal of these distro's is stability, security, ...\nfor the people who know how to handle them.\nAdding gui config tools would just mess up the simplicity.\nDefinately worth a try though :) But expect a lot of hair loss in\nthe learning process.\n\nDiederik\n\n\n\n"
    author: "Diederik"
  - subject: "Re: Slackware"
    date: 2004-01-03
    body: "\"Automatic hardware recognition is close to zero :)\n Slack sets up your NIC and that's about it :)\"\n\nAre you using Slackware 9.1 or something older ?\nWith 9.1 the only thing I had to add manually was modprobe es1371 for my soundcard, all the rest (nic, usb printer, scanner, palm, via chipset) worked out of the box.\n\nBye\nAlex\n\n"
    author: "aleXXX"
  - subject: "Re: Slackware - Pretty simple and straightforward "
    date: 2004-01-10
    body: "Yeah right.\n\nThe information is not in any one place so you must waste the best years of your life scouring the net for fragmentary arcane items of information just to get it installed & working.\n\nIt's the year twenty o four and eliteist geek rationalisations are still being re hashed.\n\nsheesh!"
    author: "garbage"
  - subject: "Re: Slackware"
    date: 2004-01-02
    body: "Hi,\n\nif you e.g. saved your XF86Config file, you likely can get any Linux distribution to just use that. Including but not limited to Debian.\n\nSpeaking of Debian, try it. When you manage to install it (use Woody, not Sarge yet unless you are prepaired that it's not stable), you can track testing like I do since 2000. This install has been updated from KDE as in Potato (was that 1.x) at least KDE 2.0 beta 3 all the way to KDE 3.1.4 and I can easily try the betas with Konstruct.\n\nIn my opinion Debian testing is a very friendly distribution for compiling yourself, because if you can so easily just apt-get install whatever-dev and the compile will work.\n\nThe best feature of Debian is that you have the choice to follow a community Linux which is updated daily via the Internet. You can see the progress, if you choose unstable, even day by day, with testing not that much later.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: Slackware"
    date: 2004-01-05
    body: "Debian VS Slackware\n\nThe biggest difference here is the way packages are installed. I believe most Slackware users will download tars and then either compile them, or unpack binaries directly into their system. Debian uses a highly advanced package management system that does all the work for you.\n\ndpkg and apt-get are respectively the left and right hands of a Debian system. They have been ported to other distros, but they were born Debian!\n\nI'd believe Debian to have a higher learning curve than most other distro's, but the end result IMO is so worth learning it. Debian does not currently have any hardware detection in the installer, which is curses based. I have heard of a project to revamp it to include hardware detection.\n\nMany people have complained about how \"Bad\" the installer is, but I have always loved it. It is incredibly flexible. It's easy to boot, and you can install different parts from different sources. In fact, if I need to do a new install, I'll use my 2 (!!) year old install cd to boot up, install the base system, and then just apt-get upgrade from there (Really a testament to the power of apt-get and dpkg.)\n\nPersonally, the only hardware detection I need is lspci and cat /proc/bus/usb/*\n\nI swear by Debian as much as you or I swear by linux. Perhaps more ;)"
    author: "John Hughes"
  - subject: "Re: Slackware"
    date: 2004-01-07
    body: "Want an easier-to-install Debian? Check out what the folks at MEPIS are doing (mepis.org). They offer a Live CD based on DEBIAN and KDE that can also be installed to disc. Hardware detection is superb, and the MEPIS folks have really made a fine distro. Check it out!"
    author: "Michael Barrett"
  - subject: "Re: Slackware"
    date: 2004-01-10
    body: "Yes I wholeheartedly agree.\n\nMepis can be installed & running in about 10 minutes.\n\nYou can go ahead and geek yourself to death with it after that but it's OPTIONAL not MANDATORY.\n\nA truly excellent effort.\n"
    author: "garbage"
  - subject: "Mandrake"
    date: 2004-01-02
    body: "Mandrake has just released ISO's (not Live) that include the very latest of everything.\nhttp://www.distrowatch.com/?newsid=01232"
    author: "Claus"
  - subject: "Re: Mandrake"
    date: 2004-01-02
    body: "Yeah, too bad they are not even considering making a MandrakeMove-snapshot version... :-(\n"
    author: "Nobody"
  - subject: "Re: Mandrake"
    date: 2004-01-02
    body: "The first 10.0 snapshot spent 5 minutes on my harddrive. KDE crashed once. Then it would crash systematicaly on start.\nI guess finiding the bug would have been the better thing to do but since I actually did this on my main machine (I felt like reinstalling everything...)\nI didi not play with it much longer.\nI think a live CD is much better suited for this kind of testing. I'm gonna try to start filling some bug reports to get 3.2 out quick and strong...."
    author: "ybouan"
  - subject: "SLAX?"
    date: 2004-01-02
    body: "Really cool stuff.\nJudging the screenshots, it seems pretty slick too."
    author: "OI"
  - subject: "rdesktop"
    date: 2004-01-02
    body: "Just wondering. Why did you use rdesktop in the screenshots? Our beloved KDE Remote Desktop Client can do exactly the same (using rdesktop code)."
    author: "Arend jr."
  - subject: "Re: rdesktop"
    date: 2004-01-02
    body: "They're Slackware users, what would you expect?\n\nI like Remote Desktop Connection, I use it for VNC. Its the first VNC viewer for Linux that I've actually liked."
    author: "Ian Monroe"
  - subject: "What's the difference with the original Slack CD"
    date: 2004-01-02
    body: "What is the difference between your live-CD and the one from Slackare ?\nA part that you updated the kde packages ?\n\nI'm updating my Slack CD with the Slackware packages found on kde's ftp and I have a working live CD."
    author: "JC"
  - subject: "Re: What's the difference with the original Slack CD"
    date: 2004-01-03
    body: "What Live-CD \"from Slackware\"? \"Slackware-LIVE CD\" was renamed to SLAX, so perhaps we are speaking about the same?"
    author: "Anonymous"
  - subject: "Re: What's the difference with the original Slack CD"
    date: 2004-01-03
    body: "Well, I don't know :)\nSlackware has a CD to install the complete distro on HD as usual, but also another CD called \"Ready to boot\" that is a live CD as Knoppix. \nSLAX is probably a fork from that disk. I will check what new on it."
    author: "JC"
  - subject: "DC++"
    date: 2004-01-03
    body: "DC++ on the screenshot 7 - is it a win-apllication ported with wineLib or does it rune with Wine."
    author: "andre"
  - subject: "Answers to your questions"
    date: 2004-01-04
    body: "I'm the creator of SLAX, so perhaps I am the right one to answer your questions :)\n\nBefore some time, SLAX was called Slackware-Live CD. It was confusing a little bit, because Slackware.com had own LiveCD, also called Slackware Live. I tried Patrick's official Slackware Live CD and I found it useless and ugly. So I made own one.\n\nI wanted to be a part of Slackware team, because I felt that my LiveCD could be a good replacement for the official one. But Patrick told me that he doesn't want me to join their team, and also that I am violating their \"Slackware\" trademark by using the name \"Slackware Live CD\".\n\nSo I've renamed my LiveCD to SLAX and I am no longer interested in working for them :)\n\nNow some answers:\n\nDC++ is running by using wine.\n\nI'm using rdesktop because I thought that KDE's remote desktop can be used _only_ for VNC. If it's able to conntect to windows remote terminal server then it's great news for me and I don't need to use rdesktop anymore :) I'll try it.\n"
    author: "Tomas Matejicek"
  - subject: "Re: Answers to your questions"
    date: 2004-01-05
    body: "Thomas,\n\nThanks for your SLAX distro!!!\nI finally got the chance to look at the next generation KDE3.2!!\nIt worked great on both PCs I tried it on.\nWhile it won't replace my Knoppic/MandrakeMove/PCLinuxOS Live CDs due to it's limited SW range (understandable for only 177 MB), it IS a great way to get a look at KDE and KOffice.\n\nThanks again!\n\nPS: sorry to hear the Slack people treated you like dirt..."
    author: "UglyMike"
  - subject: "Re: Answers to your questions"
    date: 2004-01-05
    body: ">sorry to hear the Slack people treated you like dirt...\n\nNO NO NO NO NO!!!!\n\nI have to react here. Linus Torvalds has many many times explained the trademark issue. If you do not actively defend your trademark and let people use it without authorization, you loose your trademark.\n\nSlackware people paid and did administrative paperwork to buy this trademark, they do not want to loose the money they invested. It's simple. There is nothing bad or mean in this. They are obliged to."
    author: "oliv"
  - subject: "Re: Answers to your questions"
    date: 2004-01-05
    body: "Lol, I first taught that you were confusing between Patrick and Linus :)\n\nYes if I was working hard on a distro, software, or whatever I would protect my trademark. There are too many companies that want have a blue logo with at least a I or B or M in there name ... They will never have the strength of the original one :)\n"
    author: "JC"
  - subject: "Re: Answers to your questions"
    date: 2004-01-06
    body: "Sorry, but I was not referring to Patrick et al's protection of the Slackware brand, which I can understand and agree with. I was more referring to the fact that a volunteer stood up to help them offer a better product and they turned him down (protesting his use of the term Slackware-Live CD is just a logical consequence of this...) After all, he clearly demonstrated his mastery of Slackware and wanted to come aboard with a (to me) good product.\nMaybe I'm reading the \"But Patrick told me that he doesn't want me to join their team\" wrong and Patrick has a perfectly sensible reason to turn down offers of help like these."
    author: "UglyMike"
  - subject: "Re: Answers to your questions"
    date: 2004-01-06
    body: "Probably one good reason is the time to maintain it.\nIf Slax becomes an offical CD, then Slackware have to update, fix, support it. \n\nFor example, if one day Thomas doesn't have time to maintain his project, who can handle this new amount of work. Slackware... the team already working hard. They probably don't have much spare time. They can hire, but now it's a financial problem. I don't know about their financial strength.\n\nI guess there are many others reasons."
    author: "JC"
  - subject: "Re: Answers to your questions"
    date: 2004-01-10
    body: "\"I wanted to be a part of Slackware team, because I felt that my LiveCD could be a good replacement for the official one. But Patrick told me that he doesn't want me to join their team, and also that I am violating their \"Slackware\" trademark by using the name \"Slackware Live CD\".\"\n\nIs'nt this just so typical.\nEveryday I see scarce resources being diluted by this attitude in the Linux world.\n\nIt's not unique to Linux though Look at BSD, forking & forking (check out dragon fly BSD).\n\nI'm all for innovation but sick of seeing good code being shunned for ego reasons.\n\nWindows succeeded whilst Unix vendors fragmented the OS & fought amongst themselves.\n\nHistory repeating tiself?"
    author: "garbage"
  - subject: "Slax 3.0.25"
    date: 2004-01-07
    body: "There is a new LIVE CD available which has fixed USB mouse detection, kdepim added and language support for German and French."
    author: "Anonymous"
  - subject: "Re: Slax 3.0.25"
    date: 2004-02-06
    body: "Just a notice:\nthe official URL is http://slax.linux-live.org\n"
    author: "Tomas"
  - subject: "Just tried this..."
    date: 2004-01-07
    body: "This is my first experience with KDE 3.2 and I'm truly impressed! Even though it was running from a liveCD, it was pretty fast and it looks very cool and clean!\n\nI didn't play that much with it though since it didn't detect my Logitech Cordless Mouse :("
    author: "Joergen Ramskov"
  - subject: "Debian and KDE 3.2"
    date: 2004-01-19
    body: "When can we expect KDE 3.2 in Debian Sid ? I installed Slackware on a machine just  to test KDE 3.2-Beta2 and i'm impressed by the progress, can't wait to update the 10 debian-machines i'm maintaining."
    author: "anynomous"
---
<a href="http://www.slax.org/">SLAX</a> is a <a href="http://www.slackware.org/">Slackware</a>-based LiveCD. The <a href="http://www.slax.org/changelog.php">latest release</a>, available as a <a href="http://www.slax.org/download.php">177 MB ISO</a>, <a href="http://www.slax.org/features.php"> features</a> KDE 3.2 Beta 2 and KOffice 1.3 Beta 2.  As a LiveCD similar to <a href="http://knoppix.org/">Knoppix</a> in operation, you can simply boot off the CD to safely try out these new releases without disturbing your existing installation and <a href="http://www.slax.org/screenshots.php">see the future</a> of the KDE desktop.  As a dogfood exercise, I am posting this using SLAX right now!




<!--break-->
