---
title: "Qt Developers Conference Report"
date:    2004-05-12
authors:
  - "mbucciarelli"
slug:    qt-developers-conference-report
comments:
  - subject: "C# Implementation?"
    date: 2004-05-12
    body: "One thing I'm very interested in knowing about is the possibility of a C#/.NET implementation of Qt. I'm no fan of Microsoft, but the more I look at .NET, particularly Mono, the more I become convinced of it's potential. My biggest hope out of the conference was the announcement of Novell purchasing Trolltech or at least special rights to Qt and porting it to Mono. This of course didn't happen, but I still hold out hope for either Novell coming to their senses that this is the route to take OR Trolltech deciding to explore porting Qt themselves (which I'm sure they've done some of). Was there any hints of this or buzz among attendants?\n\nEron"
    author: "Eron Lloyd"
  - subject: "Re: C# Implementation?"
    date: 2004-05-12
    body: "I didn't hear much discussion of .NET/Qt.  Someone did ask if Qt was going to support things like Avalon / Longhorn (whatever that means) and the answer was yes."
    author: "mbucc"
  - subject: "Re: C# Implementation?"
    date: 2004-05-12
    body: "Mmm. In what way? Is this Longhorn only?"
    author: "David"
  - subject: "Re: C# Implementation?"
    date: 2004-05-12
    body: "What would it buy you? The point of C#/.NET is the integrated development environment, especially the framework. If you have the .NET framework there is no real need for a second framework (Qt). It just causes unneccessary troubles, and there are no features in Qt that are not also provided by .NET. \nAnd once XAML is available (or if you use one of the XAML clones which are available now), Qt4 does not look very competitive..."
    author: "AC"
  - subject: "Re: C# Implementation?"
    date: 2004-05-12
    body: "IMHO it would offer a better language to develop in, firstly. C# seems much more elegant and consistent than C++, providing garbage collection, better exception support, much more OO container types, etc. Don't think of .NET as one monolithic system... the libraries of the CLR alone with the improved syntax of C# could make Qt development much more pleasant. Plus, the CLR includes many useful libraries that make me turn to Python usually, like math, XML, networking, and cryptography. Even if Qt provides a lot of overlap to the CLR (like it does in PyQt), the fact of not having to use C++ alone is very appealing. Qt is having to provide a lot of this anyways (look at Qt's STL replacement types, properties, data access, runtime introspection, etc), so it should move to a more modern platform to provide these capabilities at a lower-level. I suppose the biggest appeal is using Qt instead of GTK+ to provide the GUI facilities under Mono. That coupled with Trolltech's fantastic documentation standards...\n\nWishful thinking, I suppose."
    author: "Eron Lloyd"
  - subject: "Re: C# Implementation?"
    date: 2004-05-12
    body: "What is it that is so fantastic with C#? I've heard a lot of times that C# is Java done right, without any futher explanations. What is it that is so right about C#? To me it looks like C# is as different from Java as Java 1.4 is to Java 1.5.\nBy the way, when will XAML, which so far got support for rotating a widget in realtime (given you have a 6GHz computer), be able to compete on any other platforms than Longhorn? As far as I know, QT runs on anything that's got a processor, It looks completely consistent on my KDE desktop which is a lot more than you can say about any MS product. My MS office package looks completely different from my Internet Explorer and not to mention 3rd party software, which looks like it runs by accident. This is the same on all versions of Windows.\nWhat is there to compete with? QT is already Superior."
    author: "Vegard Figenschou"
  - subject: "Re: C# Implementation?"
    date: 2004-05-12
    body: "You have to remember that Qt is a framework, whereas C# is a language. What can be done is an implementation of Qt *in* C# to provide a better alternative than C++."
    author: "Eron Lloyd"
  - subject: "Re: C# Implementation?"
    date: 2004-05-12
    body: "It is very unlikely that Qt will be reimplemented in C#, as there is so much code that depends on it. This is arguably a good thing. Contrary to the hype which is around C#/.NET these days, you never hear compelling advantages of C# over C++. You often hear garbage collection, which is available for C++ too, and you hear properties which are, first, of dubious value and, second, available with C++ if you use QT too. On the other hand what is missing from C# is eminent, just some examples: \nSupport for generic programming is weak, and \"generics\" are planned first for C# v2.0. By consequence, generic library components are inferior.\nThere is no Support for \"real\" RAII, garbage collection is forced on you.\nThere is one unnecessary base class for every type.\nThere is no multiple inheritance (probably only a minor point).\nThere is no standardized preprocessor.\n\nI do not want to let it sound as if C# was a complete catastrophe, if you do like the language it is of course you choice to use it (use, however, a free implementation). But do not try to fool others into thinking that C# is better than C++. If you have something you do not like about C++, say specifically what it is (and check if the next version of the standard will probably have it).\n\n "
    author: "El Pseudonymo"
  - subject: "Re: C# Implementation?"
    date: 2004-05-12
    body: "Well, people are already trying (bindings, anyways). That there are no compelling features of C#, I found this article to catch my attention: http://genamics.com/developer/csharp_comparative.htm#27\n\nI know some GC support is available for C++, but like most things, seems tacked on as an afterthought. And how do you mean that GC is forced upon you in C#? For properties, I see this as important; in fact, you'll notice Qt 4 moving more to properties itself.\n\nQt itself sports a base object, QObject, which has real and important functionality. Qt also only support single-inheritance models, so how would that hurt if using C# as well? Usually multiple inheritance is only good for mixins, whose need can be removed with a well-designed class structure (again, see Qt itself). I just don't see the arguments.\n\nI suppose I just see C++ as slow-moving (improvement-wise) and past its prime.\nSometimes it is better to start all over again. Perhaps if someone could point out all the exciting things C++ is planning to offer I'd change my mind. Because C# doesn't support this or that yet doesn't convince me. The language is still quite young and new stuff is being added at a good pace (similar to the pace Python adds language and API changes). With Mono in the mix now adding value to the current implementation and extensions to mscorlib.System, there should be some competition (hopefully).\n"
    author: "Eron Lloyd"
  - subject: "Re: C# Implementation?"
    date: 2004-05-13
    body: "I aggre with El Pseudonymo, that C# is not that much better than C++. \n\na) In an OO world memory management ist way easier than in the procedural world. additionally to GC smart pointer can provide a safe and fast solution.\n\nb) not all classes in Qt are derived from QObject.\n\nc) C++ may have a few dark corners, but on the other hand you get seemles interoperability with C libraries, which is a big plus especially in the Unix world that seems to be stuck in the C-ages\n\nd) a new c++ standard is supposed to be out in 2005(introducing GC), 2002 was the ABI standardisation, 1998 was ISO standardarsation. so there is stuff going on. For a language of the importance of C++ you just cant change things all the time.\n\ne) many things are being developed in libraries instead of the language, which fits into the philisophy of C++ being low overhead. I wouldnt call Qt slow moving ;)\n\nMarc"
    author: "Marc"
  - subject: "Re: C# Implementation?"
    date: 2004-05-14
    body: "a) Smart pointers and constructors/destructors make memory management certainly easier but still not as easy as with garbage collection, while smart pointers introduce overhead that is comparable to GC, while often only providing partial solutions to automated memory management. Also memory management of shared resources with constructors/destructors is as cumbersome as in C. You have to be careful with your interface if the class handles all memory by itself or you delegate responsibility to a client which is dangerous. The .NET garbage collector is well integrated and you don't need to use some special syntax, something which cannot be said of most C++ GCs. The design can focus on algorithmic challenges instead of worrying about proper memory handling.\n\nb) I consider a common base class a good thing(TM).\n\nc) Interoperability with C libraries is well handled in most higher level languages. Take Python for example which provides many bindings to C libraries. .NET also allows easy interoperability with legacy code via P/Invoke.\n\nd) The ISO C++ standard won't introduce GC AFAIK. What you are referring to is the C++/CLI integration that is on a separate ISO track (while some of its members are also taking part in ISO C++). In fact C++/CLI is a targeting C++ to the CLI (which is a subset of .NET) thereby introducing GC as well. However if you consider this as a good development I don't understand the critique of .NET/C#. Besides I agree with you that C++ is (slowly) evolving and there are reasons tor the slow pace. Anyway C++ is falling behind in terms of features and the legacy burden forces lots of questionable syntax extensions making the complex C++ even more complex.\n"
    author: "Anonymous"
  - subject: "Re: C# Implementation?"
    date: 2004-05-14
    body: "a) smart pointers do not cause the same overhead as a GC, because in case of smart pointers there is only overhead for the objects managed by a smart pointer, while with GC _ALL_ objects have GC overhead even if they dont need it.\n\nb) Not always you want to derive every class from a heavywheight Object class if you dont need the extra functionality. Why should QChar for instance derive from QObject? Just imagine what kind of overhead that would be for the QString class!!!\n\nc) In C++ you can use C libs directly, without any wrappers, or special C API's. P/Invoke is probably not as complicated as Pythons C API or Javas JNI.\n\nd) Garbage collection is planned to go in the ISO standard. Even Stroustrup is ok with that. But you will be able to switch it off, of course. Btw, my critique wasn't that C#/.Net is a bad language, I just said its not that much better than C++.\n"
    author: "Marc"
  - subject: "Re: C# Implementation?"
    date: 2004-05-14
    body: "a) Sorry for stating my point not clear enough. What I wanted to point out is that GC is no more slower or inefficient than most semi automatic memory handling or even than completely manual memory handling. You may want to refer to Hans Boehm's web site and the articles mentioned there.\n\nb) .NET still allows you to create and use efficient types. Read something about boxing/unboxing on the net. I can treat an int simply as an integer value type (one that's sizeof() returns 4 byte) and as an object if I box it (often implicit). As long as I simply use it as an integer there is no overhead involved.\n\nc) P/Invoke is certainly not complicated but easy to use and straightforward. Microsoft had to ensure that the transition from legacy code is not to painful since it wanted to attract lots of developers.\n\nd) GC in ISO C++ is new to me. Need to check this out."
    author: "Anonymous"
  - subject: "Re: C# Implementation?"
    date: 2004-05-13
    body: "I am not sure what people mean when they say that C# has no compelling features. It seems to me that people are confusing the runtime environment of .NET and the language C#. C# is a *nice* language, but currently the only way to run C# is on top of a virtual machine with all the associated overheads (extra memory for VM instantiation, runtime checks, etc). Many of the things that people say are great/bad about C# are in fact properties of the .NET runtime, not the C# language. I'm sure if somebody made a native compiler for C# that used the QT libraries, people wouldn't complain so much. In fact, it seems as though that's exactly what Trolltech is moving towards with moc and language extensions such as the \"foreach\" construct.\n\nLikewise, the implication that C++ is \"slow-moving\" is quite meaningless. Once again, are they implying that the language should evolve faster or the platform? QT is moving quite rapidly (as observed) and making very interesting strides comparable to .NET. C++ is a fairly mature language and IMHO *shouldn't* radically alter itself every two years. \n\nAs for platform niceties such as GC (that QT currently doesn't offer) and runtime security checking, it is true that VM platforms such as Java and .NET offer a more integrated environment. However it is arguable that such things can be added into a platform without all the associated overhead of a virtual machine environment through a combination of static analysis and conservative collectors. Basically, I believe it is important for people to\n\n1) Separate the language and platform when talking about things like C++, QT, C#, .NET, etc\n2) Realize that just because something runs on top of a virtual machine doesn't make it better"
    author: "James Horey"
  - subject: "Re: C# Implementation?"
    date: 2004-05-13
    body: "I think right the first point on the page you mentioned illustrates what I mean: There are many restrictions (\"No concept of inheriting a class with a specified access level\", \"No global functions or constants, everything belongs to a class\"), and there are \"improvements\" on C++, whereas in C++ the feature is actually there like thread support.\nSo you have some real improvements but you have quite some deficiencies. So my opinion remains, changing to C# is no real advantage."
    author: "El Pseudonymo"
  - subject: "Re: C# Implementation?"
    date: 2004-05-14
    body: "There are *not* many restrictions in C#. There are some features (access specified inheritance is one of them) and some questionable or errorphrone contructs left out. The \"No global functions\" is something you can live with since static methods are possible. Threading in particular is well supported in .NET/C#. You fail to specify the deficiencies that overweight the advantages. Here is my incomplete advantage list (using C# version 2.0):\n\n- Feature rich framework that presents lots of stuff a programmer needs in a coherent and well documented form.\n\n- Garbage collection frees your mind from memory handling.\n\n- Safety. No buffer overruns, segmentation faults, etc.\n\n- Reflection capabilities.\n\n- Some handy syntax sugar (anonymous methods, iterators, properties, indexers).\n\n- Type safe and more powerful callbacks (delegates)\n\n- Enumerations introduce a new scope.\n\n- Versioning via new/override. Every declared virtual method is the root of dispatch.\n\n- Unified type system with a common base class, bridging the gab with boxing/unboxing.\n\n- No distinction between pointers and references. No need for -> * &.\n\n- Definite Assignment of variables. The crystal clear ref and out syntax.\n\n- Checked arithmetic.\n\n- Rectangular arrays.\n\n- ...\n\nI leave it here. There is certainly some point that I unintentionally left. Maybe you can come up with your disadvantage list ;-)\n\n"
    author: "Anonymous"
  - subject: "Re: C# Implementation?"
    date: 2004-05-14
    body: "I do not want to post a list of advantages or disadvantages. Programmers clearly have different opinions whether a particular feature is good or bad. So just putting a list together will just perpetuate an unhelpful discussion.\n\nI want however to pick on one feature of C# you mentioned. It is \"Type safe and more powerful callbacks (delegates)\". Why is that a new feature of C#? You just have to look at boost::signal and boost::function. Both of them provide delegating and similar capabilities. The core language of C++ is powerful enough to make a library implementation of this possible. This is a key difference, The support for programming generic components in C# is still not powerful enough (and won't be with C# v2.0 presumably) to achieve similar things in C#. It enables one not much more than to write generic containers and doing simple method dispatch. \n"
    author: "El Pseudonymo"
  - subject: "Re: C# Implementation?"
    date: 2004-05-14
    body: "- Feature rich framework that presents lots of stuff a programmer needs in a coherent and well documented form.\n \n* More libraries and frameworks for C++ than for C#\n\n - Garbage collection frees your mind from memory handling.\n\n* Handling memory allocations based on specific cases gives smaller/faster programs.\n \n - Safety. No buffer overruns, segmentation faults, etc.\n \n* The possibility to use pointers when needed\n\n - Reflection capabilities.\n \n* Templates, generic programming\n\n - Some handy syntax sugar (anonymous methods, iterators, properties, indexers).\n\n* A lot of compilers, direct compattibility with C and fast bindings for every existing language. Ability to add inline assembly for fully utilizing your CPU when nedded.\n \n - Type safe and more powerful callbacks (delegates)\n \n* Ability to evict type safety when needed\n\n - Enumerations introduce a new scope.\n \n* namespaces\n\n - Versioning via new/override. Every declared virtual method is the root of dispatch.\n \n* Dunno if this is a feature :)\n\n - Unified type system with a common base class, bridging the gab with boxing/unboxing.\n \n* You can have it too in C++, it's your decision\n\n - No distinction between pointers and references. No need for -> * &.\n\n* What's the feature here?\n \n - Definite Assignment of variables. The crystal clear ref and out syntax.\n\n* ?\n \n - Checked arithmetic.\n\n* ?\n \n - Rectangular arrays.\n\n* You can find a myriad of C/C++ libs for handling arrays \n\n - ...\n \n* Compiled language, without the burden of a VM and GC, _optional_ GC to come.\n\nI just dont understand why people are so hyped for languages that use a VM. Portability does not work there too, see java. The vm makes them heavyweight and makes your new PC look like a 386."
    author: "anon"
  - subject: "Re: C# Implementation?"
    date: 2004-05-15
    body: "\"More libraries and frameworks for C++ than for C#\"\n\nYou point out the crux with C++. The standard library leaves a lot to be desired and so you have a lot of libraries trying to close the gap. Unfortunately most of them are redundant featurewise and deciding between the zillions of libraries essentially doing the same thing for a given purpose but differing in their API styles and concepts make life harder for programmers. Why do you think are so many people interested in Qt? Its because Qt offers a unified framework that offers a lot more than just GUI functionality and that is badly needed for C++. I however prefer one coherent and well thought framework I can rely on. You know less is more sometimes.\n\n\"Handling memory allocations based on specific cases gives smaller/faster programs.\"\n\nThis claim needs to be proved. Besides the usual laypersons believe GC does not have to yield slower and bigger programs.\n\n\"The possibility to use pointers when needed\"\n\nThis possibility exists in C# as well.\n\n\"Templates, generic programming\"\n\nThose features certainly do not outweight the lack of reflection. Besides I referred to version 2 of C# which directly supports generics.\n\n\"A lot of compilers, direct compattibility with C and fast bindings for every existing language. Ability to add inline assembly for fully utilizing your CPU when nedded.\"\n\nThere are at least 3 .NET/C# implementations at this time and this is an implementation detail that has nothing to do with language capabilities. What you could mean with \"bindings for every existing language\" is beyond me--it is certainly not easy to access eg. Python from C++ (and Python is one of the easier ones). Invoking C/C++ functions is straightforward from C# via P/Invoke.\n\n\"Ability to evict type safety when needed\"\n\nFirst time someone tries to present this as a feature to me.\n\nConcernign the unified type system. Your claim is I can have it in C++ too and this is definately wrong. C++ doesn't have a unified type system. There are basic types (eg. int, float) and user defined ones (via classes). This gap can't be closed in C++.\n\n\"I just dont understand why people are so hyped for languages that use a VM. Portability does not work there too, see java. The vm makes them heavyweight and makes your new PC look like a 386.\"\n\nThe hype is not about VMs. It is about coherent frameworks and simple languages that don't impose barock syntaxes and semantics caused by backward compatibility desires. Portability with VMs is possible and works fine with Java."
    author: "Anonymous"
  - subject: "Re: C# Implementation?"
    date: 2008-07-09
    body: "Is there any reason for me to develop anything in C#?. I am starting a project with C/C++, though time consuming, I am building a GUI, with a complex database, and a new file format. Is there anything I can save some time and build with C#? or is it as limited as it looks? translated code has never worked for me in the past. Should I try again?\n\nthanks\n\nAlbert"
    author: "albert goldwyn"
  - subject: "Re: C# Implementation?"
    date: 2004-05-14
    body: "I forgot to answer: \"And how do you mean that GC is forced upon you in C#?\"\n\nEasily answered, you have unpredictable disposal of ressources in C#, if you do not do disposal yourself."
    author: "El Pseudonymo"
  - subject: "Re: C# Implementation?"
    date: 2004-05-15
    body: "If your class implements the IDisposable interface (basically requiring you to write a Dispose() method) you might want to use the 'using' contruct which calls Dispose() automatically even in the case of exceptions. So your resources are freed at the earliest possible time."
    author: "Anonymous"
  - subject: "Re: C# Implementation?"
    date: 2004-05-15
    body: "You are right. I never heard about that. So one main point of my critique is alleviated. But problems still remain, because in order to use \"using\" you have to declare a variable. This isn't possible everywhere and in many places it would make the code harder to read. And as far as I know, there is no way to \"remove\" \"using\" from a variable."
    author: "El Pseudonymo"
  - subject: "Re: C# Implementation?"
    date: 2004-05-12
    body: "> What is it that is so fantastic with C#?\n\nTwo years from now, how many developers do you think will be using C# and .NET?  I think it will be a lot.\n\nHaving Qt C# bindings will lower the barriers for all these developers to participate in developing free software (or, more likely, make their proprietary software work on KGX).\n\nIt's not about what is the best technology, rather it's about the marketing reality of Microsoft that will push many developers to .NET, and lowering barriers for those developers and their apps work on KGX.  Anyway, that's how I see it ..."
    author: "mbucc"
  - subject: "Re: C# Implementation?"
    date: 2004-05-12
    body: "There will probably be a lot of C# developers two years from now that still just take it for granted that C# will save the world :)\n\nI'm not against making Qt C# bindings. I think one should add support for C# in KDevelop, so that that all these new developers will be able to use a real IDE, not to mention run their CLRs at twice the speed in dotgnu.\nI also think that this new language 'D', (announced on Slashdot two weeks ago) should be integrated into KDevelop as well as get Qt bindings.\n"
    author: "Vegard Figenschou"
  - subject: "D Language"
    date: 2004-05-13
    body: "I was also very impressed with \"D\" - it should be the successor to C++ but that doesn't mean it will be.  I'm afraid that unless the C++ committee powers drop their egos and adopt it(unlikely) or some big software company adopt it into a major IDE effort, that D will just float around out there as another great technology with no real support.  Most programmers know C, C++, Java, and maybe now C#.  Students are learning C, C++, Java, and C#.  It takes something really big to alter that entrenched skill base."
    author: "John"
  - subject: "Re: D Language"
    date: 2004-05-13
    body: "Snif, snif. I'm sad :-( because I don't know why there is so much attention to languages like C# or Java.\n\nTry Smalltalk! It's there since the 80's and has a lot of features still not available in C# or Java. (here http://www.whysmalltalk.com/articles/panici/considered.htm is some ST propaganda ;-) )\n\nI would like to see some open source Smalltalk implementation with support of native widgets like Qt or GTK (Squeak - http://www.squeak.org - it's nice... but doesn't have support to create \"normal\" desktop applications).\n\nBut, if I have to use Java... I would like see an implementation of SWT (http://www.eclipse.org) with Qt. The linux implementation of SWT is GTK only :-( ouch! I think that making Java applications look like native KDE applications would be great.\n\n(sorry, my english is not good)"
    author: "Diego"
  - subject: "Re: D Language"
    date: 2004-05-13
    body: "Smalltalk is not really a competitor to C# or Java, rather to Ruby and Python. It is not a statically type-safe language. It's in a different league (I leave to you whether it is better or worse for system programming).\n\nBeside that, Smalltalk's syntax is weird in a world where people are used to C-style and Basic-like languages. That alone is enough reason to rule out a wide-spread adoption.\n"
    author: "AC"
  - subject: "Re: D Language"
    date: 2004-05-13
    body: "I agree Smalltalk is still one of the best languages for developing GUIs, apart from the unusual syntax if you aren't used to it, and non-native look and feels. But there are issues with the Squeak license (Debian doesn't include it for that reason), and I think GNU Smalltalk would be a better basis for a Qt/KDE language binding.\n\n\"But, if I have to use Java... I would like see an implementation of SWT (http://www.eclipse.org) with Qt. The linux implementation of SWT is GTK only :-( ouch! I think that making Java applications look like native KDE applications would be great.\"\n\nWhy do you think SWT is some sort of improvement over QtJava or the Koala KDE java api? SWT still has clunky event listener subclassing like the hard to use Swing framework, rather than slots and signals. You have to dispose() of all resources with no help from the garbage collector, and it's pretty limited for creating new custom widgets."
    author: "Richard Dale"
  - subject: "Re: D Language"
    date: 2004-05-13
    body: "Actually IBM did the port, but it was never released due to licensing problems.  This was touched on a bit in the interview with the Trolltech guys on the dot recently.\n\nBasically for such a port to exist and be useful it would need to be possible to run applications written in SWT regardless of license -- however this would mean running proprietary apps using a GPL'ed Qt which is a no-go."
    author: "Scott Wheeler"
  - subject: "Re: C# Implementation?"
    date: 2004-05-17
    body: "Uhmm!!! Why we need a C# implementation of QT??? it's so much work!!! But i have to admit that having a .NET implementation of QT would be really good!!!, in that sense why not to do a modification of QT to compile it with C++.NET (yes i know we don't C++.NET for Linux, but we could !!!) less work than doing a C# implementation (i think!!), and now that C++.NET will be standarized (see http://www.ecma-international.org/news/ecma-TG5-PR.htm), would be really good to have a C++.NET compiler for Linux and a QT.NET implementation!!! and this would be better that the bindings that are aleady in progress (http://sourceforge.net/projects/qtcsharp), it would have less dependencies!!!"
    author: "Ivan Deras"
  - subject: "memory pools"
    date: 2004-05-12
    body: "Regarding memory allocation optimisations, I wonder if they have plans to support something like memory pools. The idea behind pools is that allocating an object has an expensive cost in time. If you have many allocations of small object to make, you should better allocate big chunks of memory, keep them in a pool and let the objects use the memory of the pool.\n\nThis could certainly be used where one allocates many instances of small objects. It could be for example an option of QList, or for QListViewitem."
    author: "Philippe Fremy"
  - subject: "Performance"
    date: 2004-05-13
    body: "I like these news:-)\n\nHow far will the optimizations in Qt influence KDE performance?\nHow big (if necessary) are the needed modifications to KDE?\n\n"
    author: "ac"
---
On May 10th, ICS presented a <a
href="http://www.ics.com/qt/qtmeetings/agenda.html">Qt Developer
Conference</a> in Waltham, Massachusetts.  Over 100 developers were on hand to
hear presentations from Havaard Nord (TrollTech CEO), Jasmin Blanchette
(TrollTech uber-developer) and Matthias Kalle Dalheimer (Klarälvdalens
Datakonsult CEO).




<!--break-->
<p>It was a somewhat uneven conference.  There were some <em>great</em> technical
talks and then there were some that were too high-level and slow moving.
Jasmin Blanchette did not have enough time for his talk on Qt 4, causing him
to rush and leaving little time for Q&A.   Havaard Nord's talk on the soul of Qt was inspiring and included some interesting results from a 
recent customer survey.  A low point was probably the talk on GUI design, which
really had nothing specific to Qt.  This time would have been better spent
having Matthias Kalle Dalheimer demonstrate KD Executor, which was very
popular during the breaks.  Joe Longson (from Walt Disney) demonstrated an
amazing Qt application that Disney uses to manage production of animated
feature films.  Gregory Seidman's talk on using a app-global relay object
(publish/subscribe pattern) to centralize connecting signals and slots was
very useful.

</p>

<p>

As of this writing, I could not find an online version of the speakers'
slides, but I believe <a href="http://www.ics.com">ICS</a> intends to post
them.

</p>
<p>
<b>TrollTech Customer Survey</b>.  Havaard Nord's presentation included data
from a March 2004 customer survey.  The survey was sent to 6,000 licensees and
had a 25% response rate.  Some of the results: </p>

<ul>
<li> The most commonly used modules is threading (50% of responses).  So we
can expect TrollTech to focus on threading module.</li>
<li>GNU/Linux and Mac OSX are growing fast (see table below).</li>
<li>Only 31% of respondents have participated in an Open Source project.  (2%
didn't know if they had or not!  :)</li>
<li>97% would recommend Qt to others</li>
</ul>

<table>
<tr><th>Target OS</th><th>Now</th><th>Planning</th><th>Change</th></tr>
<tr><td>Mac OSX</td><td>14%</td><td>25%</td><td>79%</td></tr>
<tr><td>Windows 2003</td><td>13%</td><td>19%</td><td>46%</td></tr>
<tr><td>GNU/Linux</td><td>67%</td><td>72%</td><td>21%</td></tr>
<tr><td>Windows XP</td><td>71%</td><td>65%</td><td style="color:red;">-9%</td></tr>
<tr><td>Windows 2000</td><td>70%</td><td>53%</td><td style="color:red;">-24%</td></tr>
</table>

<p>

Licensees where asked what OS they are targeting now and what OS they are planning to target.  While this table is based on the 1,250 responses TrollTech got, it is not
clear if all 1,250 responses answered this question, nor was any attempt made
to quantify the customer base of each respondent.  So take these results with
a grain of salt.

</p>

<p>

<b>Qt 4.0</b>  An <a
href="http://doc.trolltech.com/qq/qq09-qt4.html">article</a> in the recent
issue of the Qt Quarterly has a list of the changes coming in Qt 4 (pronounced
"cute four").  TrollTech developer Jasmin Blanchette, co-author of the recent
TrollTech/Prentice Hall Qt book, gave a great talk on Qt 4.  Some things that
stuck out for me from the talk:

<ul>
<li><b>MVC</b>.  The list, tree and table views have been refactored
to use a model-view-controller pattern.  The model data structure (a tree
where each leaf has columns) is the same for all views, so your data can be viewed with a 1D list,
2D table, or 3D tree.  The API is now consistent across all three.</li>

<li><b>Threading</b>.  Signals and slots now work across threads.  Container
reference counting is now thread-safe, so you no longer need to use QDeepCopy.</li>

<li><b>Containers</b>.  All containers (for example, QList, QLinkedList,
QVector, etc.) are now value based.  QList is the container of choice in most
cases.  But use an integer to scan the list, as iterators are not guaranteed
to be valid if you change a list while interating over it's contents.
</li>

<li><b>Performance</b>.  A lot of work was done reducing memory allocation to
improve performance.  QStrings and QByteArray used to require two mallocs for
each instantiation.  They now require one.  QList is now much smarter about
mallocs; for example, if you are working with a list of pointers, it does not
allocate a pointer to the pointer, it uses the existing pointer.  Another area
where mallocs were reduced is in the object hierarchy.  In Qt 3, if there were
three parent classes to the object you instantiated, four mallocs
occurred--one for each class in the hierarchy.  Now there is just one.  These
changes (particularly the QString change) result in huge differences; for
example, the memory allocated at Qt Designer start-up was reduced by 46%!
Designer now starts almost twice as fast in Qt 4 as it did in Qt 3.</li>

</li>

</ul>

<p>
A alpha / developer-preview release of Qt 4 is planned for May or June of
2004.
</p>






