---
title: "Integrated Data Management: Kexi 0.1 Beta 2 Released"
date:    2004-01-21
authors:
  - "lbusch"
slug:    integrated-data-management-kexi-01-beta-2-released
comments:
  - subject: "Installation quirks on windows"
    date: 2004-01-21
    body: "I tried to install this on windows. It works, but:\n\n- it tries to install some configuration to \"d:\\kde\" regardless my install-settings (unfortunatly, I have no drive \"d:\" )\n\n- it is in polish despite the chosen option in the installation dialog. is there a way to get an english version ?\n\nregards\ncylab\n"
    author: "cylab"
  - subject: "Re: Installation quirks on windows"
    date: 2004-01-21
    body: "I had the same problem....how do I change the menus to English?"
    author: "anon"
  - subject: "Re: Installation quirks on windows"
    date: 2004-01-21
    body: "\"is there a way to get an english version\" - fixed now. Please wait for mirrors' update and uninstall old and install new one - the new file name is now: \n\nkexi-1.0beta2-update1-win32.exe\n\n--\njs\n"
    author: "Jaroslaw Staniek (Kexi Team)"
  - subject: "Re: Installation quirks on windows"
    date: 2004-01-22
    body: "thanks, great work btw. !"
    author: "cylab"
  - subject: "Handy Hint"
    date: 2004-01-21
    body: "Don't put entrance pages on web sites."
    author: "David"
  - subject: "screenshots Kexi + other KDE-apps on MS Windows"
    date: 2004-01-21
    body: "<a href=\"http://www.xs4all.nl/~leintje/stuff/Kexi-MSwindows/images.html\">Kexi-MSwindows/images.html</a>\n<p>\nFab\n"
    author: "Fabrice Mous"
  - subject: "Re: screenshots Kexi + other KDE-apps on MS Windows"
    date: 2004-01-21
    body: "How do you run those KDE-Apps on Windows? Is this a kind of XServer for Windows with cygwin?"
    author: "Sebastian"
  - subject: "Re: screenshots Kexi + other KDE-apps on MS Window"
    date: 2004-01-21
    body: "nope native \n\nFab"
    author: "fab"
  - subject: "Re: screenshots Kexi + other KDE-apps on MS Window"
    date: 2004-01-21
    body: "I think this has something to do with it:\n<p>\n<A href=\"http://iidea.pl/~js/qkw/\">http://iidea.pl/~js/qkw/</a>\n<p>\nThis should be properly announced of course, but at the time (and still) I didn't properly understand what it was or how it worked (due to language issues).\n"
    author: "Navindra Umanee"
  - subject: "Re: screenshots Kexi + other KDE-apps on MS Window"
    date: 2004-01-21
    body: "mmmh, very interesting indeed.\nThat would be something similar to Apple's KWQ wrapper classes for khtml...\nWow. Quite an effort.\n"
    author: "G."
  - subject: "Re: screenshots Kexi + other KDE-apps on MS Window"
    date: 2004-01-22
    body: "I I understand correctly, it's not similar.\n\nKWQ is meant to use OSX API to replace Qt, while this other thing with the exact same letters in a different order uses Qt to replace kdelibs?"
    author: "Roberto Alsina"
  - subject: "Re: screenshots Kexi + other KDE-apps on MS Window"
    date: 2004-01-21
    body: "Any binaries for Mandrake 9.2 or something like an rpm? "
    author: "charles"
  - subject: "Re: screenshots Kexi + other KDE-apps on MS Windows"
    date: 2004-01-21
    body: "Qt/KDE wrapper:\n\nhttp://iidea.pl/~js/qkw/\n\nSeems great, isn't it?!"
    author: "Alex Exojo"
  - subject: "looks nice..."
    date: 2004-01-21
    body: "is this some sort of form builder like program?\ncan it handle multiple users at the same time?"
    author: "Mark Hannessen"
  - subject: "Windows Install"
    date: 2004-01-21
    body: "Kexi installed in Polish on Windoze.  How do I change the default to English?"
    author: "anon"
  - subject: "Re: Windows Install"
    date: 2004-01-21
    body: "Same problem here, but I think that with this package Polish is all you get.\nIn D:\\KDE\\share\\locale\\pl\\LC_MESSAGES are alle the Polisch files, the other language folders are empty."
    author: "Also Debian User"
  - subject: "Data Management?"
    date: 2004-01-21
    body: "And Rekall? Why isn't Kexi included in the 1.3 release?"
    author: "Hans J\u00e4ger"
  - subject: "Re: Data Management?"
    date: 2004-01-21
    body: "> And Rekall?\nwhat about it?\n\n> Why isn't Kexi included in the 1.3 release?\nbecause it's quite immature, and we hadn't enough time to implement everything we want to. shipping a beta in a package which is called stable may not be a good idea.\n\n  lucijan"
    author: "Lucijan Busch"
  - subject: "Re: Data Management?"
    date: 2004-01-21
    body: "> because it's quite immature, \n\nas the rest of Koffice...\n\n> and we hadn't enough time to implement everything we want to. \n> shipping a beta in a package which is called stable may not be a good idea.\n\nKO developer's release :-9\n\n\nRekall is a mature application, gpl, non beta, why not include it into Koffice?\nRekallrevealed.org\n\nAnd what abour Hbasic as a future Koffice Kandidate?\nhbasic.sf.net\nlacks KDE integration but is a real solution."
    author: "Hans J\u00e4ger"
  - subject: "Re: Data Management?"
    date: 2004-01-21
    body: "And you helped koffice with?"
    author: "David"
  - subject: "Re: Data Management?"
    date: 2004-01-21
    body: "Well,\nperhaps gambas ( http://gambas.sourceforge.net/ ) would be a good choice. It looks mature and seems to support KDE3 environment pretty well.\nIt isn't a KDE app either... but looks rather nice...:)"
    author: "Ricardo Sousa"
  - subject: "Re: Data Management?"
    date: 2004-01-22
    body: "hbasic is qt based"
    author: "jo"
  - subject: "Re: Data Management?"
    date: 2004-01-21
    body: "> Rekall is a mature application, gpl ...\n\nThat's one of the problems I have with rekall. Kexi is ment to be easily exendable in the future and is ment as some kind of platform like gideon (kdev 3) is.\n\nPure GPL is nice for stand alone application, but for me it is counter productive for heavily plugin based applications, that's why eg gideon has some lgpl parts and kexi will be completely LGPL or less restrictive. \nOk, konqy is a counter example, but there the plugins don't need access to internal structures / informations in most cases, so non GPL kparts are okay there \n\nJoWenn\n\nPS: I really don't like the GPL"
    author: "Joseph Wenninger"
  - subject: "Re: Data Management?"
    date: 2004-01-22
    body: "UPS, I don't think they were opposed to LGPL licensing...  "
    author: "jo"
  - subject: "Download?"
    date: 2004-01-21
    body: "Where can I find the newest source? I looked on ftp.kde.org but I can't find the Kexi source.. I must be blind or something..\n\n"
    author: "CMHICKS"
  - subject: "Re: Download?"
    date: 2004-01-21
    body: "Nevermind. Found it."
    author: "CMHICKS"
  - subject: "Re: Download?"
    date: 2004-01-22
    body: "WHERE EXACTLY?\n\nTIA"
    author: "Sangohn Christian"
  - subject: "Re: Download?"
    date: 2004-01-22
    body: "How about clicking the \"available at download.kde.org\" link in the story?"
    author: "Anonymous"
  - subject: "Re: Download?"
    date: 2004-02-08
    body: "There's nothing suitable at this link.\nPlease make the download of KEXI more comfortable!\n\n"
    author: "Wolfgang Gruhn"
  - subject: "To change PL nanguage to EN"
    date: 2004-01-21
    body: "in the file:\n\nletter:\\kde\\share\\config\\kdeglobals\n\nplease change \"pl\" to \"en\" in these lines:\n\nCountry=pl \n\nLanguage=pl \n\nBtw, the installer will be fixed soon.\n\n-- \nregards / pozdrawiam,\n  Jaroslaw Staniek / OpenOffice Polska\n  Kexi for win32 & MAC OS X,\n  Kexi project: http://koffice.kde.org/kexi\n  QT-KDE Wrapper project: http://iidea.pl/~js/qkw \n"
    author: "Jaroslaw Staniek (Kexi Team)"
  - subject: "Failed to build"
    date: 2004-01-21
    body: "When I run make I get:\n\nIn file included from field.cpp:24:\n../../kexi/kexidb/connection.h:593: ISO C++ forbids declaration of `Q_ULLONG'\n   with no type\n../../kexi/kexidb/connection.h:593: `Q_ULLONG' declared as a `virtual' field\n../../kexi/kexidb/connection.h:593: parse error before `(' token\nfield.cpp:327:2: warning: #warning fixme\n"
    author: "Cory M Hicks"
  - subject: "Re: Failed to build"
    date: 2004-01-21
    body: "Perhaps you are using qt3.1, not 3.2. But it's nothing wrong, as I use the same.\nJust replace command:\n\n% make\n\nwith: \n\n% export KEXI_OPTIONS=\"-DQ_LLONG=long\\ long -DQ_ULLONG=unsigned\\ long\\ long\"\n% make -f Makefile KEXI_OPTIONS=\"$KEXI_OPTIONS\"\n\nYou may want to create scripts for above.\n\n-- \nregards / pozdrawiam,\nJaroslaw Staniek / OpenOffice Polska\nKexi for win32 & MAC OS X,\nKexi project: http://koffice.kde.org/kexi\nQT-KDE Wrapper project: http://iidea.pl/~js/qkw\n"
    author: "Jaroslaw Staniek (Kexi Team)"
  - subject: "Re: Failed to build"
    date: 2004-01-21
    body: "Now I am getting:\nIn file included from keximainwindow.cpp:66:\nkexicontexthelp.h:25:27: kocontexthelp.h: No such file or directory\nIn file included from keximainwindow.cpp:66:\nkexicontexthelp.h:28: parse error before `{' token\nkexicontexthelp.h:29: virtual outside class declaration\nkexicontexthelp.h:29: non-member function `const char* className()' cannot have\n   `const' method qualifier\nkexicontexthelp.h:29: virtual outside class declaration\nkexicontexthelp.h:29: virtual outside class declaration\nkexicontexthelp.h:29: virtual outside class declaration\nkexicontexthelp.h:29: virtual outside class declaration\nkexicontexthelp.h: In function `QObject* qObject()':\nkexicontexthelp.h:29: invalid use of `this' in non-member function\nkexicontexthelp.h: At global scope:\nkexicontexthelp.h:29: parse error before `private'\nkexicontexthelp.h:33: destructors must be member functions\nkexicontexthelp.h:34: parse error before `private'\nkexicontexthelp.h:29: warning: `bool qt_static_property(QObject*, int, int,\n   QVariant*)' declared `static' but never defined\nkexicontexthelp.h:29: warning: `QMetaObject* staticMetaObject()' declared\n   `static' but never defined\n"
    author: "Cory M Hicks"
  - subject: "Re: Failed to build"
    date: 2004-01-21
    body: "add -DKEXI_NO_CTXT_HELP=1 to $KEXI_OPTIONS (see my prev. post)\n\n"
    author: "Jaroslaw Staniek (Kexi Team)"
  - subject: "Re: Failed to build"
    date: 2004-01-22
    body: "That didn't solve the issue I'm still getting:\nkexicontexthelp.h:25:27: kocontexthelp.h: No such file or directory\nIn file included from keximainwindow.cpp:66:\nkexicontexthelp.h:28: parse error before `{' token\nkexicontexthelp.h:29: virtual outside class declaration\nkexicontexthelp.h:29: non-member function `const char* className()' cannot have\n   `const' method qualifier\nkexicontexthelp.h:29: virtual outside class declaration\nkexicontexthelp.h:29: virtual outside class declaration\nkexicontexthelp.h:29: virtual outside class declaration\nkexicontexthelp.h:29: virtual outside class declaration\nkexicontexthelp.h: In function `QObject* qObject()':\nkexicontexthelp.h:29: invalid use of `this' in non-member function\nkexicontexthelp.h: At global scope:\nkexicontexthelp.h:29: parse error before `private'\nkexicontexthelp.h:33: destructors must be member functions\nkexicontexthelp.h:34: parse error before `private'\nkexicontexthelp.h:29: warning: `bool qt_static_property(QObject*, int, int,\n   QVariant*)' declared `static' but never defined\nkexicontexthelp.h:29: warning: `QMetaObject* staticMetaObject()' declared\n   `static' but never defined\nmake[4]: *** [keximainwindow.lo] Error 1\n"
    author: "Cory M Hicks"
  - subject: "Re: Failed to build"
    date: 2004-01-25
    body: "Please try the attached patch."
    author: "Felix Buenemann"
  - subject: "Re: Failed to build"
    date: 2004-01-21
    body: "QT 3.2 dependency. Replace Q_ULLONG by Q_ULONG or unsigned long long, should be not problem for now\n\nJoWenn"
    author: "Joseph Wenninger"
  - subject: "Kexi's quasi EOModeler E-R View pretty cool"
    date: 2004-01-23
    body: "It's nice to see some NeXTish Enterprise Objects Modeler functionality being added into dealing with relational databases.  If it can be extended to say include PostgreSQL I think it could be one kick butt tool.\n\nIf you don't know what I'm referring to please read:\n\nhttp://www.apple.com/webobjects/specs.html\n\nIf KDE wants to distinguish itself in the OSS world, offering an EOF Tool for Linux via KDE would help immensely.  Not to mention adding Objective-C support."
    author: "Marc J. driftmeyer"
---
Months after the previous beta version and a lot of framework changes later, the Kexi Team is <a href="http://www.kexi-project.org/announce-0.1-beta2.html">pleased to announce Beta 2</a> of <a href="http://www.kexi-project.org/about.html">Kexi</a>, the integrated data management environment for KDE. This is a preview release for interested developers and experienced users; <a href="http://www.kexi-project.org/changelog-0.1-beta2.txt">changes</a> from the previous beta include an integrated kexisql engine and an improved user interface. Kexi source packages and MS Windows binaries are <a href="http://download.kde.org/download.php?url=unstable/apps/KDE3.x/office">available at download.kde.org</a>. Kexi is slated for a standalone release later this year and <a href="http://www.koffice.org/kexi/">will be in KOffice</a> after the 1.3 release.








<!--break-->
