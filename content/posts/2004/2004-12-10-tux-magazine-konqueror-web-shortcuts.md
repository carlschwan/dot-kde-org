---
title: "TUX Magazine: Konqueror Web Shortcuts"
date:    2004-12-10
authors:
  - "mgagn\u00e9"
slug:    tux-magazine-konqueror-web-shortcuts
comments:
  - subject: "To be fair ..."
    date: 2004-12-10
    body: "You can get this in Firefox too ... if you're using Windows and are willing to go through a bit more pain that is.  They are called protocol handlers in the Windows world and I just added 1 the other day so I can do stuff like 'msdn:System.String' in IE or Firefox and search the msdn site for System.String etc. A nice break away from my day job really.\n\nYou first add 3 keys to the registry that detail the name of the protocol (msdn) and the program to be invoked. Then you write your wrapper program that is invoked when the url is entered. This wrapper program will then spawn off an ie process that hits msdn and magic happens.  In case anyone really wanted to know ..."
    author: "jesse"
  - subject: "The greatest part"
    date: 2004-12-10
    body: "The greatest part is that you can type alt+f2 and enter \"gg:<search criterium>\"\nand look on google without pressing a single mouse button and without an open browser\nwindow.\nNothing beats integration/an integrated environment :)\nI miss that feature everywhere (gnome/windows/...)\n\n"
    author: "dwt"
  - subject: "Re: The greatest part"
    date: 2004-12-10
    body: "Me too, I look forward to 3.4 with its GMail support so I can drop Firefox. I'm not a big fan of having to use a seperate text box for Google searching."
    author: "Ian Monroe"
  - subject: "Re: The greatest part"
    date: 2004-12-10
    body: "> I'm not a big fan of having to use a seperate text box for Google searching\n\nIt\u00b4s damn hard then :\nright click on firefox\u00b4s toolbar, click on \"customize\", drag and drop the search box on the dialog window, use \"google yoursearch\" in the url bar, Et voil\u00e0 ! You have your internet keywords like in konqueror.\n\nNeed to add another internet keyword ? Not a problem, go to http://dict.leo.org/ type a word in the box, do a right click on the box, select \"add an internet keyword for this search\" (use eg. dict), and you have your german/english translator as easy as typing \"leo word\" in the url bar.\n\nSo long for the firefox bashing !"
    author: "jmfayard"
  - subject: "Re: The greatest part"
    date: 2004-12-10
    body: "Not every alternate opinion is \"bashing\".  Chill."
    author: "Leo S"
  - subject: "Re: The greatest part"
    date: 2004-12-12
    body: "Possibly OT, but can you tell me how to do the same thing in konqueror? I can't seem to get rid of the google search box as it's not actually listed when I right click on the toolbar, even though I can get rid of the actual location bar the google text bar stays."
    author: "mikeyd"
  - subject: "Re: The greatest part"
    date: 2004-12-12
    body: "Did you try to change the \"Location Toolbar <searchbar>\"?"
    author: "Anonymous"
  - subject: "Re: The greatest part"
    date: 2004-12-12
    body: "Ah, no, thank you."
    author: "mikeyd"
  - subject: "Re: The greatest part"
    date: 2004-12-10
    body: "Why wait 3.4? I'm already using it with >=3.3.1"
    author: "Davide Ferrari"
  - subject: "Re: The greatest part"
    date: 2004-12-10
    body: "GMail fixes in KHTML are part of the just released 3.3.2, no need to wait for 3.4."
    author: "ac"
  - subject: "Re: The greatest part"
    date: 2004-12-10
    body: "They are? I still get a \"Your browser's cookie functionality is turned off. Please turn it on.\" error..."
    author: "John SUSE  Freak"
  - subject: "Re: The greatest part"
    date: 2004-12-10
    body: "Stupid question, have you turned cookie's on for the site? :-) Or try using another browser id string. "
    author: "Morty"
  - subject: "Re: The greatest part"
    date: 2004-12-10
    body: "Opssssss! Damn, why had I blocked cookies for google... guess my remark was the stupid one... :)"
    author: "John SUSE  Freak"
  - subject: "Re: The greatest part"
    date: 2004-12-10
    body: "Doesn't work for emails with attachments though :-/\nGuess I'll report it later at bugs.kde.org"
    author: "John SUSE  Freak"
  - subject: "Re: The greatest part"
    date: 2004-12-10
    body: "its even better, if you add the commandprompt to the kicker, you just type your gg:searchterms there... no need for alt-F2 :D\n\nand if you set google to your preffered search engine in Konqueror, you dont have to type \"gg\" in konqi - just a few words, and it'll search the web."
    author: "superstoned"
  - subject: "AdBlock"
    date: 2004-12-10
    body: "Now all that konqueror is missing, in my opinion, is the powerful AdBlock addon from Firefox.\nThat`s perhaps the only reasone why I`m still using Firefox."
    author: "Alex V"
  - subject: "Re: AdBlock with konqueror !"
    date: 2004-12-10
    body: "so try privoxy http://www.privoxy.org/ no url to configure, no more regexp, it's simply works and for everyone... and for konqueror (and firefox) !"
    author: "higgins"
  - subject: "Re: AdBlock with konqueror !"
    date: 2004-12-10
    body: "Yes, privoxy is not bad, I use it myself.  \nBut how do you maintain your blocklist?  \n\nIn firefox you can mark an image as ad to be blocked by just clicking. \nI see this as a very userfriendly way to do this. \n\n"
    author: "cm"
  - subject: "Re: AdBlock with konqueror !"
    date: 2004-12-13
    body: "Have you tried the \"Open New Windows-> Smart\" under \"Settings->Java&Javascript->Javascript\" ? Works like a charm :)"
    author: "Rodrigo Fernandes"
  - subject: "Re: AdBlock with konqueror !"
    date: 2004-12-12
    body: "I've used junkbuster/privoxy for years and years and years. It's been great. Being a proxy it can to a bunch of things which browsers are only fairly recently supporting (such as referrer and agent tricks). But nothing compares to mozilla/firefox AdBlock for ad blocking. It can (often gracefully) colapse the DOM around the element you are blocking (which  makes pages look much better in 95% of cases, rather than a large blank area). The ease of use is excellent (right click). And it can block flash and embedded objects via the gui... \n\nI still use Konq 75% of the time, because it just feels lighter and more responsive (also consistant with my KDE desktop)... but AdBlock sure is nice in mozilla.\n\n"
    author: "x"
  - subject: "Re: AdBlock with konqueror !"
    date: 2004-12-13
    body: "i agree with you, and i think a Adblocker would be great\nbut i want to use only one browser, and konqueror is perfect for me \ni must say that i've never try the adblocker from firefox :)\nfor me the advantage of privoxy is that there is nothing more to do after installation."
    author: "higgins"
  - subject: "Konqueror  right click menu regression"
    date: 2004-12-10
    body: "I have to use Firefox more, because with KDE 3.3 Konqueror updated the right click menu.\n\nYou can no longer right click on an image and that way simply save it.\nYou can no longer right click on a link and that way simply copy the link to the clipboard.\n\nThis is really lame in my oppinion. Is this considered \"usable\" by usability teams? How is this considered usable or how does something like this creep in?"
    author: "Leverkusen"
  - subject: "Re: Konqueror  right click menu regression"
    date: 2004-12-10
    body: "> You can no longer right click on an image and that way simply save it.\n\nFUD, works here.\n\n> You can no longer right click on a link and that way simply copy the link to the clipboard.\n\nFUD, works here.\n\n>  This is really lame in my oppinion. Is this considered \"usable\" by usability teams?\n\nThe troll attempts here are lame too."
    author: "Anonymous"
  - subject: "argh"
    date: 2004-12-10
    body: ">You can no longer right click on an image and that way simply save it.\n>You can no longer right click on a link and that way simply copy the link to >the clipboard.\n\nCan someone confirm this is true? :(\n\nHave you filed a bug with Bugzilla?"
    author: "ac"
  - subject: "Re: argh"
    date: 2004-12-10
    body: "All of those work here"
    author: "John SUSE  Freak"
  - subject: "Re: Konqueror  right click menu regression"
    date: 2004-12-10
    body: "The right-click has not change!\nCheck your configuration files and kde .desktop files\n"
    author: "karim"
  - subject: "Re: Konqueror  right click menu regression"
    date: 2004-12-10
    body: "Huh? I'm using Konq with 3.3.1 and I can right-click save. What are you talking about?\n\n(my right click image attached... translations to english in red)"
    author: "Coolvibe"
  - subject: "Re: Konqueror  right click menu regression"
    date: 2004-12-10
    body: "Whoops... of course I have to attach it :P"
    author: "Coolvibe"
  - subject: "Re: Konqueror  right click menu regression"
    date: 2004-12-10
    body: "I don't know which version of Konqueror you are using, but I'm using Konqueror 3.3.2 and the options are still there in the right click menu.\n\n> You can no longer right click on an image and that way simply save it.\n\nWrong. Right click + Save Image As...\n\n> You can no longer right click on a link and that way simply copy the link to the clipboard.\n\nWrong. Right click + Copy Link Address\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Konqueror  right click menu regression"
    date: 2004-12-10
    body: "I am using KDE 3.3, and both context menu entries are there.\n\nWhich distribution do you use?"
    author: "falonaj"
  - subject: "Re: Konqueror  right click menu regression"
    date: 2004-12-10
    body: "It's good to see I am WRONG!\n\nBut seriously, my right click menu looks like this: http://members.rott.chello.nl/cplanken/s12.png\n\nIt looks like that wherever I click, on a link or an image, anywhere. And the \"Copy To>\" stuff in there always means (in my case) you are copying the entire web page.\n\nI use Debian unstable (pure debian), normal updating, nothing fancy, no errors with updating etc. I once installed this Debian when KDE 3.1 was the latest, never cleaned my profiles etc. Never had any problems.\n\nIt says Konqueror 3.3.1 Using KDE 3.3.1\n\nGood to see that I was wrong, but how to fix this for me then?"
    author: "Leverkusen"
  - subject: "Re: Konqueror  right click menu regression"
    date: 2004-12-10
    body: "To see if I could fix it, I updated the entire boc with apt, then I added a new users and started KDE, to see if a fresh KDE setup and system update would fix it.\n\nIt did not, see attached file. It has been like this for me since 3.3.\n\nNot wanting to FUD, just telling you what I see."
    author: "Leverkusen"
  - subject: "Re: Konqueror  right click menu regression"
    date: 2004-12-10
    body: "Very strange. I'm also using Debian unstable, and it works just fine..   Maybe, as another poster suggested, try temporarily moving your .kde folder and letting KDE recreate it.\n\nProblems like this always disturb me..  It seems like it happens a lot that something in .kde gets corrupted and royally bungs up peoples' KDE installations.  Why does this happen so often?"
    author: "Leo S"
  - subject: "Re: Konqueror  right click menu regression"
    date: 2004-12-10
    body: "As I mentioned (with typos), I created a new user (adduser) and started KDE fresh to try a clean fresh KDE, and even then the problem is still here.\n\nWhat could this be?"
    author: "Leverkusen"
  - subject: "PDf ist der Ressourcenkiller"
    date: 2004-12-10
    body: "the main problem with Firefox or IE ecc. under Windows is always not the Browser (to me it is irrelevant) but the PDF-plugin. Arobat Reader is incredibly slow and buggy. \n\nI am waiting for a fast PDF solution. KPDF looks vielversprechend."
    author: "gerd"
  - subject: "Re: PDf ist der Ressourcenkiller"
    date: 2006-06-29
    body: "It depends. Some pdf-viewers works fast. I think the fastest pdf-viewer i have seen, is xpdf. Kghostview is an good option as well: It seems to be nice and more advanced, and it integrates well in Konqueror."
    author: "Amund"
  - subject: "By whom?"
    date: 2004-12-10
    body: "Don't mean to start a flamefest or anything, but I'm honestly curious. It seems that Opera, Konqueror, and Firefox all have this feature. Does anyone know who actually invented it?"
    author: "Illissius"
  - subject: "Re: By whom?"
    date: 2004-12-10
    body: "Ehm..you are talking about what...?\nIf you mean tabbed browsing, I think it was invented by the Opera guys. If you mean something else, I dont know :)"
    author: "Davide Ferrari"
  - subject: "Re: By whom?"
    date: 2004-12-10
    body: "Web shortcuts. In Opera you can type 'g something or other' to google search, and similarly for various other kinds of searches. Konqueror also has this, and apparently, Firefox does too. Which was first?"
    author: "Illissius"
  - subject: "Re: By whom?"
    date: 2004-12-10
    body: "I know that Konq had it since the 2.x days. Dunno who thought of it though. Care to step forward? I don't really feel like digging in the cvsweb :)"
    author: "Coolvibe"
  - subject: "Uh, isn't this in Firefox and IE?"
    date: 2004-12-11
    body: "IE through TweakUI.\n\nFirefox through \"right click->add a keyword for this search\".\n\nAm I missing something?"
    author: "uhhh"
  - subject: "Re: Uh, isn't this in Firefox and IE?"
    date: 2004-12-11
    body: "maybe they copied it from konqueror?"
    author: "ac"
  - subject: "Re: Uh, isn't this in Firefox and IE?"
    date: 2004-12-12
    body: "Yeah. You are missing that this is a KDE web site, so they publish tips on how to use kde software. You can do it in IE and Firefox too. Are you saying tips on how to use KDE software shouldn't be published if other programs have the same functionality? That would make it trick to learn to use Konqueror. Or perhaps you're saying every KDE top needs to be printed along with the same tip for every other browser. But then you would probably want to call your web site \"Browsertips.com\" or something like that."
    author: "Scott"
---
I'm a two browser kind of guy. Somewhere, on one of my virtual desktops, I always have a copy of Firefox open. On another, I have Konqueror. Both of these are incredibly capable browsers with their own strengths, strengths which are unique to both and which, as a result, leave me running two different browsers all the time.  Intrigued? Read more <a href="http://www.tuxmagazine.com/node/1000020">in this brief article</a> at <a href="http://www.tuxmagazine.com">TUX Magazine</a>.


<!--break-->
