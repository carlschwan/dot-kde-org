---
title: "KDE Users Database"
date:    2004-06-02
authors:
  - "Rael"
slug:    kde-users-database
comments:
  - subject: "Cool!"
    date: 2004-06-02
    body: "wow, I am quick this time...\n\nThis is the first time I can actually have \"Mark\" as my login name on any website. :p But I am sure this number is going to increase lot's and lot's more.\n\nthe website is simple clean and proffesional and easy to use. having people registered is important because this way companies can get an impression of the ammount of linux users out there.\n\nI wish everybody out there good luck and hope that many many people will register themselves!"
    author: "Mark Hannessen"
  - subject: "Re: Cool!"
    date: 2004-06-02
    body: "Maybe giving people the option to sign up, when they first run KDE would be nice (would have to be something that one can get rid of with a single click and it'll go away and also something that would allow people to enter existing username and password).\n\nIt could also create an account for KDE related websites so that it's actually usefull for people to fill it in.\n\nFinally, if it could also load my KDE settings from the internet, whenever I use a new computer, that would be _really_ cool. Shame that I never manage to find time to do KDE coding."
    author: "DS"
  - subject: "Re: Cool!"
    date: 2004-06-02
    body: "I don't really like any kind of \"go out register yourself behaviour\" But if you really would want something like this I would prefer a weblink/shortcut on the desktop. popup something the user didn't ask for can and will be considered anoying. But a small lovely icon sitting on the desktop will not harm anybody."
    author: "Mark Hannessen"
  - subject: "Re: Cool!"
    date: 2004-06-07
    body: "\"(...)companies can get an impression of the ammount of linux users out there.\"\n\nBy multiplying the number of KDE users by 10 (the other 90% <me included> uses GNOME ;).\n\nIt would be interesting to have a similar database for GNOME too, maybe the author can put a list of window managers for linux in the registration form (but I dont think it would). If anybody out there can set up a postgres or mysql database I can code the application to start the GNOME database... But I think this is not the best place to put this comment (kde.org!!)\n\nAs we say in my country, I'm probably in the wrong place!\n\nHappy Coding..."
    author: "Pedro Solorzano"
  - subject: "Wheres the USA"
    date: 2004-06-02
    body: "Wheres the USA in the country list? "
    author: "Indrid"
  - subject: "Re: Where's the USA"
    date: 2004-06-02
    body: "Funny that \"Reunion\" (slightly smaller than Rhode Island) gets mentioned.  It sort of makes sense, though--the rest of the world doesn't like us.  I mean, because GWB is our President, we must all be evil, right?"
    author: "MarkN"
  - subject: "Re: Where's the USA - page admin emailed"
    date: 2004-06-03
    body: "I have just sent an email to site's admin. It must be mistake, there is no conspiracy against US :)"
    author: "manux"
  - subject: "Re: Wheres the USA"
    date: 2004-06-02
    body: "Either who compiled the country list was falling asleep or this site is bound to fail. Like us or not, a LOT of KDE users are in the USA."
    author: "Will Stokes"
  - subject: "Re: Wheres the USA"
    date: 2004-06-02
    body: "eh, we all know gwb won't try to register himself as a kde user :)"
    author: "someone"
  - subject: "Re: Wheres the USA"
    date: 2004-06-02
    body: "> Wheres the USA in the country list? \n\nI wonder why that \"Rael\" does not speak up here.\nI'm not from the US, and also I am against this kind of exclusion-politics.\nI even wonder if that wasn't the kde-users count-site only puropose... to start a fire/flamewar..."
    author: "ac"
  - subject: "Re: Wheres the USA"
    date: 2004-06-04
    body: ">I even wonder if that wasn't the kde-users count-site only puropose... to start a fire/flamewar...\n\nOh, don't be silly! It would be a little overkill to make a that kind of site just to start a flamewar. There is easier ways to start a flamewar, let me demonstrate:\n\nVi! (now just wait for the emacs people) :)"
    author: "138"
  - subject: "Re: Wheres the USA"
    date: 2004-06-02
    body: "Considering that this is a standard list of countries (apart from the USA) I can't see the ommission of the USA as being an oversight. There are quite a few American users (from the US of course).\n\nIs it under something else?"
    author: "David"
  - subject: "Re: Wheres the USA"
    date: 2004-06-03
    body: "That's what I'm wondering. I'll admit a lot of Americans are assholes, but this is flat-out insulting. I looked over the entire list, and I just don't see it. It seems fairly standard, so I wonder how United States of America could be unintentionally left out.\n\nI really hope this was just an accident."
    author: "jameth"
  - subject: "NO USA"
    date: 2004-06-02
    body: "The United States is not listed. If this is an oversight, ok. There are probably more kde users in the US than is any other single country. Please don't have a euro-centric attitude for kde."
    author: "Greg"
  - subject: "Re: NO USA"
    date: 2004-06-02
    body: "Can the US please be added?  I am sure this is simply an overwight.\n\nbrockers"
    author: "brockers"
  - subject: "Re: NO USA"
    date: 2004-06-03
    body: "If this is not solved, could we start complaining on the \"comments\" field as a p.s. note? If this gets too political it will be useless as a KDE users database, i.e. will be too segregated."
    author: "Cloaked Penguin"
  - subject: "Re: NO USA"
    date: 2004-06-02
    body: "It is not Euro-centric. It appears to have everybody except for USA.\nI would guess that the info came from a file that was feed into db/flat-file. \nWhoever did this ( Most likely Rael ), would have to excluded USA purposely.\n\nI hope that Rael will mature a little and follow the KDE group in avoiding politics. \n\nIn fact Rael, you do a disservice to KDE and your own effort by making the # of users appear to be much less than what they are. In fact, I am guessing that you have now managed to piss off a % of users who will continue to use KDE but will not register."
    author: "a.c."
  - subject: "An oversight"
    date: 2004-06-02
    body: "I think the author doesn't isn't ready for the million of people in the US to register that's why US is not on the list.  They probably need to ramp up their server before they put it in.\n\nI don't know when the author will be ready.\n\nTo me the website is now pretty slow, and taking US off is a necessary thing to keep the website snappy.\n\nJMHO"
    author: "a.c. 2"
  - subject: "Re: NO USA"
    date: 2004-06-02
    body: "Whilst I don't know Rael personally, I find it hard to believe he did that purposely. Like GWB or loathe him, it bares nothing on who uses KDE. An honest mistake I like to think."
    author: "Martin Galpin"
  - subject: "Re: NO USA"
    date: 2004-06-02
    body: "I just hope it's an accident. As I said, gwb doesn't come here anyway, so why bother about him?"
    author: "someone"
  - subject: "Re: NO USA"
    date: 2004-06-02
    body: "or...just maybe... They had USA at the top and din't want it there.  thinking that it was there twice, they got rid of the top one without checking for the one after the U.K...\n\nbut then again, we are having a fact-free discussion here, and this will get us nowhere. "
    author: "standsolid"
  - subject: "Re: NO USA"
    date: 2004-06-03
    body: "> we are having a fact-free discussion\n\nAh, so we should take it to Slashdot, then.    8-)"
    author: "Tukla Ratte"
  - subject: "Re: NO USA"
    date: 2004-06-04
    body: ">> we are having a fact-free discussion\n \n>Ah, so we should take it to Slashdot, then. 8-)\n\nHeh, that made me smile :)"
    author: "138"
  - subject: "Re: NO USA"
    date: 2004-06-02
    body: "I put Ireland.  My ancestors were Irish.  Who knows where my kids will live... I'm hoping for grandkids to have the opportunity to not live on Earth.  \n\nIf this is a protest, I think it's quite mean spirited; it's like blaming the people of North Korea because their leader is an egomaniac. Or excluding the people of your own country who are of a particular political party.  Those sort of exclusionary actions are sad to see in KDE.  Perhaps we should work to keep it men only as well?  Or possibly have a nice racial purity?  Or only those who believe in a particular diety?\n\nIf it isn't a protest, it's one heck of an oversight, especially since it appears to have been snarfed from a list of countries."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: NO USA"
    date: 2004-06-03
    body: "KDE, the 4th reich... no I don't like the sound of it... (sorry I'm horrible at spelling in english, and even worse at german)"
    author: "Corbin"
  - subject: "Re: NO USA"
    date: 2004-06-03
    body: "> it's like blaming the people of North Korea because their leader is an egomaniac.\n\nNot really.  A democracy's leader is supposed to be acting on behalf of his/her country's citizens, so ultimately, it's the citizens' responsibility to make sure their president does what he is supposed to.  North Korea isn't a democracy, so you can't hold its citizens responsible for what their government does.\n\nOn the other hand, some people might claim that GWB wasn't democratically elected...\n\nIn any case, I think it's far too soon for people to be screaming \"USA hater!!!1\" before they've even got a response from the webmaster."
    author: "Jim"
  - subject: "Re: NO USA"
    date: 2004-06-03
    body: "May be a mistake.\n\nI don't think at all it is political (protest gagainst TCPA whatever) and I also don't believe that many persons in the US use KDE. However, I believe that \"US\" requires federal states as well.\n\nRelax, the project just got started."
    author: "gerd"
  - subject: "Re: NO USA"
    date: 2004-06-03
    body: "> I also don't believe that many persons in the US use KDE.\n\nWhy?\n\n> I believe that \"US\" requires federal states as well.\n\nWhat does that mean?"
    author: "Tukla Ratte"
  - subject: "Re: NO USA"
    date: 2004-06-03
    body: "Contrary to what you believe. The numbers comming out of Linux usage polls by in the US show that KDE is actually used MORE than Gnome.  Even on Redhat the KDE/Gnome split is within the margin of error.  So whatever anyone thinks about Gnome, KDE is prefered the world over.\n\nbrockers"
    author: "brockers"
  - subject: "Iraq and East Timor"
    date: 2004-06-02
    body: "By the way, Iraq is not listed either, strange coincidence?\nTimor Lorosae (East Timor) is missing to, but this is one is most probably an oversight, as it is the most recently born country."
    author: "John Iraqi-KDE Freak "
  - subject: "Re: Iraq and East Timor"
    date: 2004-06-02
    body: "yea strange very strange, plese add US Irak and East Timor i have a fried thare that i know he uses kde."
    author: "nunopinheiro "
  - subject: "Missing USA - Solution... konqueror bug"
    date: 2004-06-02
    body: "Hello US friends,\n\nHere is one simple JavaScript hacky solution (unfortunately it works only with mozilla for me):\n1) open in mozilla the registration page URL:\nhttp://users.kde.pl/?page=register\n2) in the location bar paste this and press Enter:\njavascript:alert(document.forms[0].country.options[0].text=\"United States\");\n//(Afghanistan country will get replaced with United States)\n3) Submit your data and you are in\n\nThis solution will work can work with any work for any other country too (just replace \"United States\" with another country name). This site seems to not use CountryID (and a separate table with the countries) but a regular VARCHAR column for the countries.\n\nI hope this will work, I haven't tried it since I don't want to experiment with incorrect data.\n\nBut here is another (offtopic) issue. While implementing this simple hack for the site I discovered that in fact Konqueror, the javascript: protocol does not work from location bar. Instead it just searches in google what i entered in the location bar. Konqueror developers may want to add this simple feature by default because it's important for web developers.\n\nI hope that the guy who built that site will fix it, add USA and will not delete the US kde users.\n\nCheers,\nEuropean Friend"
    author: "Anton Velev"
  - subject: "Re: Missing USA - Solution... konqueror bug"
    date: 2004-06-02
    body: "Seems that it works:\nhttp://users.kde.pl/?page=showdb&do=showcountry&country=United+States"
    author: "Anton Velev"
  - subject: "Re: Missing USA - Solution... konqueror bug"
    date: 2004-06-02
    body: "hmm....I guess I should have checked the dot before I figured it out myself. \n\n<begin rant>\nAh well...for the record I'm a US citizen and I deplore what the US government is doing in the Middle East and the world in general. History has taught us that if the people want change, the people will do everything in their power. While I think Saddam was a menace I think the Iraqi people as a whole would have said to hell with their differences and revolted against him. I could be wrong but that's how the US was founded. Also while I feel that terrorist organizations are a threat to not just the US, but to the world as a whole I think that we are just as responsible for them hurting others as their open supporters are. Historically speaking the majority of the terrorist organizations are using arms, or money given to them by the US to help us protect/forward our interests/ambitions...think Afghans vs Russians...I also feel that we should stop supporting Israel while they play Nazi to the Palestinian people. \n\nYou know what to hell with all I've said before..I think the US should mind it's own business, and work for the people on a national level. Our homeless rate is getting larger, over a quarter of the population is without health care, and our education system is in the rut. Instead of playing international bully and forwarding our international ambitions we need to focus on our own citizens. \n</end rant>\n\nSo now that I've used the first amendment in all its glory I'm going back to tinkering with the latest CVS of krdc.\n\nNexistenz - Morphing Dwarfs"
    author: "Nexistenz"
  - subject: "Re: Missing USA - Solution... konqueror bug"
    date: 2004-06-06
    body: ">>Historically speaking the majority of the terrorist organizations are using arms, or money given to them by the US to help us protect/forward our interests/ambitions...think Afghans vs Russians...I also feel that we should stop supporting Israel while they play Nazi to the Palestinian people.<<\n\nOther than you being completely wrong, imo, take your bs elsewhere.  I come to this site for KDE information.  Not for your ignoramus rant."
    author: "rizzo"
  - subject: "Re: Missing USA - Solution... konqueror bug"
    date: 2004-06-02
    body: "lets hope that this is pointed out as a major back door. \nall data input should always be scanned."
    author: "a.c."
  - subject: "How Childish"
    date: 2004-06-02
    body: "I find it hard to believe that the omission of the United States simply an oversight. It makes me sad to think that the author of the site could be so immature about things."
    author: "Dusty"
  - subject: "Re: How Childish"
    date: 2004-06-03
    body: "I think you should give the guy a break. He was definitely trying to help KDE and did not omit anything on purpose (he already stated that on this website). Please try to help in a constructive way instead of complaining and accusing people.   \n\nFab"
    author: "Fabrice Mous"
  - subject: "Re: How Childish"
    date: 2004-06-03
    body: "Not that I care, but where did he said that? Could you provide an url?\n\nThanks"
    author: "John Evidence Freak"
  - subject: "Re: How Childish"
    date: 2004-06-04
    body: "Opss editors note on the top... how did I missed that :)"
    author: "John Gotcha Freak"
  - subject: "Re: How Childish"
    date: 2004-06-04
    body: "Probably because it wasn't there when you wrote your comment :) at least I didn't noticed it until today."
    author: "138"
  - subject: "Umm..."
    date: 2004-06-02
    body: "...I'm a bit late reading this and am quite surprised about\nthe reactions posted here so far. \nI recently needed a country list at work for a completely different\npurpose and searched the web for one.\nInterestingly there were *LOTS* of country lists omitting the U.S.\nWhy? I suppose it's because many country lists on the internet are \ncompiled in the U.S. And like often the earth is omitted whenever\nall planets in the solar system are listed - many U.S. people just think\nof themselves as \"earth\". This way of thinking is much more common \nin the U.S. then anywhere else as I can tell from many different experiences.\n<irony>\nGuess that's why Americans often tend to send their solidiers to war and\nwonder later on where the heck all those body bags are coming from\n(outer space perhaps?)...\n</irony>\nSo it might well be that the U.S. just was not on the list.\nI sincerely hope so because *intentionally* not listing any one country \nwould be very childish indeed. It's not as if the typical Bush supporter\nlikes to use Open Source software... In fact there might be Bush supportes\nenlisting from other countries as well.\nWhat's truly shocking though is the reaction of people here so far:\nAs explained it might or not be that the U.S. has been left out intentionally.\nWe just don't know.\nBut nearly everyone is almost SURE that it cannot be the case.\nI really HATE that behaviour! How can the glorious U.S. just be forgotten?\nSo now I can tell you this: They can - Period. \nIt's just a list. The author does not live in the U.S. and they can be\nforgotten just like any other country.\nNobody seems as shocked that Iraq is missing or Bosnia-Herzegovina or\nwhatever. Seems there are quite some countries missing. \nAll these upset folks here. Quite ridiculous..."
    author: "Martin"
  - subject: "Re: Umm..."
    date: 2004-06-02
    body: "thanks for your words martin, I agree completely. Sorry for this \"me too\" post, but I was just about to write the same.\n\ngood night!"
    author: "me"
  - subject: "Re: Umm..."
    date: 2004-06-02
    body: "Perhaps a new poll should be taken of people who support Bush and use open source software. \n\nBut on a more serious note.\n\nAmerica, seeing that we used the most resources, have the most money, and are the best country by far -- we ought to have our country mentioned not once, but about 10 times. \n\nIf your not with us, your against us"
    author: "Nothing "
  - subject: "Re: Umm..."
    date: 2004-06-03
    body: "...flamebait? "
    author: "Martin Galpin"
  - subject: "Re: Umm..."
    date: 2004-06-03
    body: "I thought you we're serious when I start reading. Ten times, you got to be kidding! We have at least 23x more money than anyone else, we have and last time I checked over about 15.6x more resources than any country including New Caledonia! Our cars are better, our food is better, we went to the moon first and we are about to get to mars. Linux was invented by Red Hat which is American, Gnome is ours and since Novell bought SuSE KDE is ours too. So 10 times is a joke, at least half the countries listed should be U.S.A.!"
    author: "GWB"
  - subject: "Re: Umm..."
    date: 2004-06-03
    body: "so the discucion as come to this i say 50 times biger\n\nthe deficit \nthe import volume from china \nthe car's must be the best! sooooooooooo why do you all love mercedes porche and ferrary and crysler does not seel a car in europe\nand affrica as the bigests ...........\nand nipon as the smallest.............. \nand economy EU is the biguest market in the world\nand wellfare in us is non exitent."
    author: "nunopinheiro "
  - subject: "Re: Umm..."
    date: 2004-06-03
    body: "Come on, no need to be sarcastic towards sarcastic posts.. the above comments were just jokes :-)\n\nBut yeah, the author missing the country that is likely to have the largest number of KDE users is a bit \"iffy\", if you ask me. \n\nIf it really was an accident, it's no biggie, fix it. If it wasn't, such political statements surely doesn't deserve a story on this website, and I'd urge the editors to yank it."
    author: "smt"
  - subject: "Re: Umm..."
    date: 2004-06-03
    body: "<I>Perhaps a new poll should be taken of people who support Bush and use open source software</I>\n\n<cheapshot>\nYou're joking right? To quote \"If your not with us, your against us\" comes to mind. Bush supports propriatary software beyond anything imaginable how can he not when the President of Diebold has promised him Ohio come this November?\n</cheapshot>\n\nHonestly though somehow I don't see how Bush supporters can support anything that's vaguely open source. Remember to the uninformed open source equates to communism and we're trying to sell democracy? If Bush or even Bush supporters were to embrace open source the press would jump on it asking how he/they can support a communist license while imposing new sanctions on Cuba. \n\n<I>America, seeing that we used the most resources, have the most money, and are the best country by far -- we ought to have our country mentioned not once, but about 10 times.</I>\n\n<joinsarcasm>\nDamn straight we're the bestest of the bestest best. We've more money (not per capita but who cares since what we say goes!), more freedoms (not since the PATRIOT act was passed but once again who cares what we say goes!), and the best beer in the world -- bud light (unless you look at Belgian, German, Finish, or even India for quality..what am I saying who cares bud's the best becase we say so and what we say goes!)\n</joinsarcasm>\n\nHonestly though...no offence Anheuser-Busch but what were you thinking when ya named Budweiser Budweiser?  Didn't you do your history and notice that there's another brand with the same name that's been out <B>much</B> longer and tastes <B>much</B> better?\n\nNexistenz"
    author: "Nexistenz"
  - subject: "Re: Umm..."
    date: 2004-06-03
    body: "A reply because of my apparant idiocy. \n\nDo tell what's with the whole don't parse the html? It said they were valid and didn't cough or hiccup when I clicked add so I'm going on the assumption that I didn't do something, or did something wrong. Maybe that or the dot doesn't like me using Opera and doesn't offer html as an encoding option. Either way no more including HTML for me.\n\nNexistenz - Morphing Dwarfs"
    author: "Nexistenz"
  - subject: "Re: Umm..."
    date: 2004-06-04
    body: "The encoding option \"HTML\" had been taken out years ago since it had been abused, I agree that the one \"choice\" drop down \"list\" should go as well. =P"
    author: "Datschge"
  - subject: "Re: Umm..."
    date: 2004-06-03
    body: "Here's one Bush supporter that is a KDE supporter"
    author: "Teo"
  - subject: "Re: Umm..."
    date: 2004-06-03
    body: "Politics aren't the sole indicator of everything else about a person.  Ezra Pound was a pretty good writer and also a fascist.  I've met Bush supporters and I feel confident I could trust them to help me in a time of need (assuming I wasn't Muslim, naturally).  They could probably write good code too."
    author: "ac"
  - subject: "Re: Umm..."
    date: 2004-06-03
    body: "Teo without getting angry, or feeling offended what is it about Bush that you like? \n\nMy father felt that Bush would be a good thing for the country, and stood by his vote whole heartedly until Bush decided to invade Iraq \"without just cause.\" Those are his words not mine and keep in mind he's a former NCO in the Air Force with strong ties to the military. So strong military is a near requirement in my family. I believe the mottoe is honor, duty, country. I'm the first male in nine generations who didn't joined up instead becoming a general technology consultant. Anyway my question is, what is it about Bush that makes you want to support him? I see some good in what he's done such as restarting the \"space race\" although I don't see how it's feasable with our current budget, but for the most part I don't feel that he's doing a good job, and I don't see any of the contenders (Kerry, Nader, etc) doing any better. Think of me as a possible Bush vote what could you say that would make me want to vote Bush?\n\nNexistenz"
    author: "Nexistenz"
  - subject: "Re: Umm..."
    date: 2004-06-03
    body: "What a crappy mess this is. I should have stayed too busy to look. Stop being a fool and honor the guidelines of this site. the Dot is NOT for political discussions! Stop asking people to argue politics with you here. Go somewhere else for that!"
    author: "Eric Laffoon"
  - subject: "Re: Umm..."
    date: 2004-06-03
    body: "By leaving out Iraq and US, KDE User Database is making a POLITICAL STATEMENT.  Shouldn't kde.org remove the announcement?"
    author: "a.c. 2"
  - subject: "Re: Umm..."
    date: 2004-06-03
    body: "Where do you see a \"kde.org announcement\"?"
    author: "Anonymous"
  - subject: "Re: Umm..."
    date: 2004-06-03
    body: "This is absolutely stupid and disgusting.\n\nHas anyone of you written a web application? I have and am constantly finding bugs. When I started doing the digests over a year ago, I shocked myself by spelling names wrong. People corrected me, since that is obviously important. I do try to get them right, but I make mistakes.\n\nI will say honestly that if anyone had reacted to my errors like this crowd is reacting to what is very likely an error, I would have quit and given my time to another project that deserved my interest.\n\nLay off. Let the guy fix it. If he is interested in doing anything anymore for this bunch of idiots.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Umm..."
    date: 2004-06-03
    body: "\"Honestly though...no offence Anheuser-Busch but what were you thinking when ya named Budweiser Budweiser? Didn't you do your history and notice that there's another brand with the same name that's been out <B>much</B> longer and tastes <B>much</B> better?\"\n\nHooray for revisionist history.  There never was a *brand* called Budweiser before Anhauser Busch's.  There were beers brewed in Budweis (Cseske Budovice (sp?)), but none of them called themselves \"Budweiser\" for the same reason you never see a brand of beer called \"Pilsener.\"  \n\nAs such, Anhauser may have been being a bit uncreative but you do what you can to appeal to your potential clients, many of whome at the time were recent immigrants.\n\nSo, Anhauser starts expanding globally and some brewery in Budweis decides to steal (in the sense that they can use it and they make it so Anhauser can't) the name Anhauser has been using for over a century and justify their thievery with the fact that they happen to be in Budweis.  Result?  People in Europe trip over themselves to castigate the Big American Company for stealing the name.  Please.\n\nPointings out of factual errors gladly accepted :)"
    author: "MamiyaOtaru"
  - subject: "Re: Umm..."
    date: 2004-06-03
    body: "There is a beer brand called Pilsner Urquell (*THE* beer from Pilsen, Czech Republic). And it is the beer that was then copied by all breweries.\n\nAs to Budvar (the beer from Ceske Budejovice, Czech Republic), it was AFAIK (and then again, maybe I am wrong) advertised in German (it is close to the Austrian border) as Budweiser Bier. While the communists were here, the borders were locked to the west, so Anheuser-Busch had no problems with this. Then after the velvet revolution in 1989 the iron curtain fell and Budweiser Bier alias Ceskobudejovicky Budvar was allowed to get out of the former communist block.\n\nThis was where the naming conflict arose.\n\nZoltan (ex-Czechoslovak citizen)"
    author: "Zoltan Bartko"
  - subject: "Re: Umm..."
    date: 2004-06-03
    body: "Yes, except that, as far as I know, the naming conflict is older.\n\nThere is a Wikipedia entry about the 2 breweries: \nhttp://en.wikipedia.org/wiki/Budweiser\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Umm..."
    date: 2004-06-03
    body: "> \"If your not with us, your against us\"\n\nAt least try to get it right:\n\n\"Either you are with us, or you are with the terrorists.\"\n\nAs you could have known if you participated in last year's KDE Contributor Conference in the Czech Republic which optionally included a trip to the Budweis plant in Budejovice, there is a formal agreement about the Budweiser name for use in the Americas versus Europe.\n\nBoth taste horrible versus a genuine Coastal Fog microbrew, though."
    author: "Rob Kaper"
  - subject: "Re: Umm..."
    date: 2004-06-04
    body: ">bud light (unless you look at Belgian, German, Finish, or even India for quality..\n\nOh no! Please don't look at us for (finnish, with to n's) the quality beer. All the beers here taste allmost the same, allmost water that is... I have never tasted budweiser but I have heard it tastes like water and if someone who is used to finnish beers says that budweiser tastes like water then I really don't want anything to do with it. :)\n\nps. well I be damned, budweiser is on my spellcheckers dictionary."
    author: "138"
  - subject: "Re: Umm..."
    date: 2004-06-03
    body: "\"It's not as if the typical Bush supporter likes to use Open Source software...\"\n\nI support Bush and use (and love) Open Source... do I not matter as much as the next person just because I support Bush?\n\nSaying I support Bush means one thing, and one thing only:  I support Bush.\n\nThough it does seem like most pro Open Source people (at least the vocal ones) don't like Bush.\n\nI also can't stand Windows, and that damn DVD region code that caused me several days of pain when windows kept saying that my system was set to region code 1, but to play the DVD I had to set it to region code 1 (I took a screenshot of it)"
    author: "Corbin"
  - subject: "Re: Umm..."
    date: 2004-06-03
    body: "> I recently needed a country list at work for a completely different purpose and searched the web for one. Interestingly there were *LOTS* of country lists omitting the U.S. Why?\n\nMy guess is because many order systems of US websites default to US and then offer a radio option with combobox with all other countries?"
    author: "Anonymous"
  - subject: "Re: Umm..."
    date: 2004-06-03
    body: "\"It's not as if the typical Bush supporter likes to use Open Source software...\"\n\nI think it's extremely sad that US politics has devolved into a \"our team versus your team\" mentality. Implying that one's choice of software is dependent upon one's choice of political party is ludicrous. Being the herd-like creatures that we are, it's comforting to assume that everyone who makes one choice like ours will make all their choices like ours, but it is simply not true."
    author: "David"
  - subject: "Surprised"
    date: 2004-06-03
    body: "I'm surprise by now it isn't fix.  This really saddens me.  It's been already 8 hours and it has been fix.\n\nI think the omission of US and Iraq does suggest this as a political statement, one in which should not deserve its place in KDE or kde.org."
    author: "Surprise"
  - subject: "Re: Surprised"
    date: 2004-06-03
    body: "I would be surprised if the author of the site has any interest in being involved in this project after the reaction here.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Surprised"
    date: 2004-06-03
    body: "Completely agree. I'm sure it's a mistake and the countries missing will be added soon.\n\nI'm amazed about all the bitching. Complaining that it hasn't been fixed after 8 hours is insanely stupid and saying it's a political statement is even more stupid."
    author: "Joergen Ramskov"
  - subject: "Re: Surprised"
    date: 2004-06-03
    body: "> I'm surprise by now it isn't fix.\n\nPerhaps if somebody would notify the site author instead of flaming here? Just a thought..."
    author: "Anonymous"
  - subject: "Re: Surprised"
    date: 2004-06-03
    body: "Count of soldiers in Iraq by country:\nUSA\nGreat Britain\nPolish\n\nThis page is in poland (alias polish)\nI don't think this is political statement."
    author: "nobody"
  - subject: "A few more ommisions"
    date: 2004-06-03
    body: "It seem that Guam, Puerto Rico and The Vatican (I may not be catholic but it would be cool if the Pope used KDE) are also missing.  "
    author: "anon"
  - subject: "Re: A few more ommisions"
    date: 2004-06-03
    body: "Guam and Puerto Rico are territories of the United States.  Though I guess being a territory of Denmark didn't stop Greenland from making the list..  \n\nOh right.. Puerto Rico and Guam must have been left off BECAUSE they belong to the United States.  I forgot about the whole political protest thing :D"
    author: "MamiyaOtaru"
  - subject: "Re: A few more ommisions"
    date: 2004-06-03
    body: "Actually quite a few more countries from the ISO 3166-1 list [http://www.iso.ch/iso/en/prods-services/iso3166ma/02iso-3166-code-lists/index.html] are missing:\n\n\u00c5LAND ISLANDS; AMERICAN SAMOA; ANDORRA; ARMENIA; BHUTAN; BOSNIA AND HERZEGOVINA; BOUVET ISLAND; COCOS (KEELING) ISLANDS; DJIBOUTI; ERITREA; ETHIOPIA; FALKLAND ISLANDS (MALVINAS); FAROE ISLANDS; FRENCH SOUTHERN TERRITORIES; GUAM; GUINEA-BISSAU; GUYANA; HAITI; HEARD ISLAND AND MCDONALD ISLANDS; HOLY SEE (VATICAN CITY STATE); IRAQ; LESOTHO; LIBERIA; LIECHTENSTEIN; MALAWI; MAYOTTE; MONACO; MONTSERRAT; NAURU; NIUE; NORFOLK ISLAND; NORTHERN MARIANA ISLANDS; PALESTINIAN TERRITORY, OCCUPIED; PITCAIRN; PUERTO RICO; RWANDA; SAINT LUCIA; SAN MARINO; SAO TOME AND PRINCIPE; SOLOMON ISLANDS; SOMALIA; SUDAN; SVALBARD AND JAN MAYEN; TANZANIA; TIMOR-LESTE; TOKELAU; UGANDA; UNITED STATES MINOR OUTLYING ISLANDS; VANUATU; VIRGIN ISLANDS, U.S.; WALLIS AND FUTUNA; and WESTERN SAHARA.\n\nIt may also be the US is missing because many lists come with it as the first in the list, out of alphabetical order, and sometimes it is deleted in the assumption it occurs also later in the list.\n\nIn any event I am sure this problem will be fixed, relax :-)."
    author: "Dre"
  - subject: "Re: A few more ommisions"
    date: 2004-06-04
    body: "Actually, many of those aren't countries."
    author: "Some1"
  - subject: "Re: A few more ommisions"
    date: 2004-06-04
    body: "\u00c5land islands isn't on the list? Great, now I am waiting \u00e5land islanders(is it spelled that way?) to start bitching here that it was intentional and it was  the evil finnish overlords that are responsible for that \"little mistake\". :)\n \nFor the record, I am from finland and \u00e5land islands are part of finland although quite independent part, for example they have their own flag and they speak swedish. Also one can buy tax-free goods if one is going from finland to sweden, or from sweden to finland, and the ferry goes through \u00e5lands territorial waters. I am not sure but I also think that \u00e5land is not a member of European Union (finland is and \u00e5land is part of finland as I said earlier, so go figure... :) but I might be incorrect, if I am wrong please set me straight. Oh, and I almost forgot, \u00e5land is also demilitarized zone.\n\nps. it is strange to write \u00e5land since it is called \"ahvenanmaa\" in finnish. :)  "
    author: "138"
  - subject: "Waiting to be fixed"
    date: 2004-06-03
    body: "OK! it's an honest mistake, but fix this thing for all those waiting to have the honor of being included as KDE users scratch their itch. Will someone give this guys a call and have it fixed."
    author: "Abe"
  - subject: "Re: Waiting to be fixed"
    date: 2004-06-03
    body: "Just register yourself under a different country if you are that impatient.  You can change your country after you are registered."
    author: "Jim"
  - subject: "Ladies are excluded too ..."
    date: 2004-06-03
    body: "\"This is a polish project, but every man from all over the world, who's using KDE can register and be counted.\"\n\nSo, it seems only men can register :(  \n\nOk, it's just a linguistic mistake mistake, but it should be fixed.  I emailed Rael about it yesterday, but still no change."
    author: "Henrik"
  - subject: "Re: Ladies are excluded too ..."
    date: 2004-06-03
    body: "Seems that Rael is currently offline for some reason :( I have tried to contact him using his jabber account (site says it's rael(at)jabber.atman.pl) hi did not get online for the last few days."
    author: "manux"
  - subject: "Re: Ladies are excluded too ..."
    date: 2004-06-04
    body: "If I understand correctly one can use a word man in english to describe all the people, women and men. If I am wrong please correct me and I would like to have a correction (that is if I am wrong) from someone whose native language is english."
    author: "138"
  - subject: "Re: Ladies are excluded too ..."
    date: 2004-06-04
    body: "I think that use of the word 'man' only applies when used as a synonym for 'humankind' (or 'mandind'). \n\nWhen Niel Armstrong stepped on thhe moon, he said: \"This is a small step for man, but a giant leap for mankind.\" Unfortunately that's rubbish, because it is the same as: \"This is a small step for mankind, but a giant leap for mankind.\"  He should have said: \"This is a small step for A man, but a giant leap for mankind.\" But the world has forgiven him :)\n\nAnyway, when you prefix the word 'man' with A, or THE, or THIS, or as in this case EVERY it is refering to one or more individual men, and not mankind as a whole, and therefore fails to include women. \n\nYes, English can be a silly language sometimes.  Diclaimer: IANAL (IANA Lingust)."
    author: "Henrik"
  - subject: "Re: Ladies are excluded too ..."
    date: 2004-06-04
    body: "Didn\u00b4t he say \"a small step for *a* man\"? That does make sense. I am betting even if he didn\u00b4t say that, that\u00b4s what he meant to say."
    author: "Roberto Alsina"
  - subject: "Re: Ladies are excluded too ..."
    date: 2004-06-04
    body: "Yes, he said: \"That's one small step for a man, one giant leap for mankind.\"\nhttp://starchild.gsfc.nasa.gov/docs/StarChild/whos_who_level2/armstrong.html\nI still think that \"every man\" includes also women, I am not sure but I think I have heard something like \"every man is created equal\" used to describe that all are equal.\n\n>Yes, English can be a silly language sometimes. Diclaimer: IANAL (IANA Lingust).\n\nNow that is one thing I can agree with you anyday. :) So are there anyone out there who's native language is that silly mess called english. \nCome out and help us to stop this bashing of your language! :) "
    author: "138"
  - subject: "Re: Ladies are excluded too ..."
    date: 2004-06-04
    body: "Well, men and women are not created equal.\n\nAssuming of course, that hey were created ;-)"
    author: "Roberto Alsina"
  - subject: "Re: Ladies are excluded too ..."
    date: 2004-06-04
    body: "Ok, this is getting really offtopic but what the heck, the whole thread has been offtopic almost from the beginning. :)\n\nI didn't say men and women are equal or that they were created(they weren't), I said I have heard that kind of saying somewhere.\n\nI do believe that men and women are equal in a same sense that we (you, Roberto and me, 138) are equal being from the opposite side of the globe (I am from Finland and if I am not mistaken you are from Argentina), we are both people after all, certainly different but equal non the less.\n\nAnd if you really are from Argentina, I will never forgive you guys for dropping the England in footballs world cup in 1986, and with the hand goal! Oh, the humanity! :)"
    author: "138"
  - subject: "Re: Ladies are excluded too ..."
    date: 2004-06-04
    body: "I seriously think men and women are not equal.\n\nJust ask any woman (unless you are one, in which case, ask yourself) about anything she perceives as superior in general about women. For example, if women are more loyal, or nurturing, or whatever.\n\nThen, simple arithmethics say that either men are superior in other ways, or women are superior in general.\n\nSince I can't claim to know whether women or men are superior, I won't, but the odds of their differences matching so well that the whole is equal is infinitesimal.\n\nAnd about the 86 cup... come on. Look at the whole match. Look at the two goals. Did England deserve to win, tie, or even be *close* in the match? I say no way.\n\nHandballs go in often, and if the referee lets it stand, it is a goal. Other times, valid goals are annulled. That's how the game is played.\n\nI find it amazing that of that whole match, what whole nations have preferred to remember was the first goal instead of the second. I think that speaks badly of them.\n\nAnd besides, I didn't do it ;-)"
    author: "Roberto Alsina"
  - subject: "Re: Ladies are excluded too ..."
    date: 2004-06-05
    body: "Back with a hangover...\nWell it seems to me that we don't exactly mean the same when are talking about equality. Like I said, when I talked about equality I meant the way we, you and me, are equal or would you say that either of us is superior compared to other, and if that is the case, either of us is superior, I _really_ would like to know how? Other example would be the way people are equal in the eyes of the law (ha like that is going to happen, I know that people are NOT equal in the eyes of the law, but let's assume that we live in an ideal world for a sake of argument). \n\nIMHO arithmetics got absolutely nothing to do with the peoples relationships.\n\nI think that men and women aren't the same, there is a significant differences between genders. We like different things than women (there you go, I am a man), and women like different things than us, for example I have never understood what is the fun in shopping and why one must spend at least an hour in front of a mirror before going outside. Still you have to love them and the differences between men and women is what makes things interesting and worthwhile.\n \nBy the way, have you ever tried to explain rules of football to a woman, I have, and believe me, I am NOT going to try to do it again. :) And that brings us back to things that are important. Football.\n\nCup of 86, ah the memories... Did England deserve to win, tie, or even be close in the match? You say no way, being an avid English football supporter (I blame the television, since our national broadcasting channel used to air first division matches on Saturdays, that was before premier league existed and the prices for airing the games weren't so high and all that crap...) I say yes way. But to be honest the second goal (I have a video clip of that goal here) was amazing display of talent, Diego Maradona was at that time one of the best football players on earth if not the best.\n \nStill, I don't let the facts get in a way of my fanboism and I am bitter about that first goal. :) And if you think that speaks badly about me, then it is not my problem but yours. Since when we are expected to act rationally about football? Let me ask you, when a team that you support have lost a match, aren't you upset regardless the fact whether you were a better or worse team in a match?\n \nI recognize the fact that handgoals are sometimes accepted as valid goals and that is the part of the game and that is the way it is supposed to be, shit happens like in real life, IMHO. When a referee says it is a goal then it is a goal, that's it no buts or ifs. So we agree on this thing.\n\nWell you really can't say what the whole nations choose to remember and how they choose to remember things based on one individual. Especially in this case since I am not a English, I am Finnish as said earlier. I have no ties to England besides the fact that one of my friends lives and works in London. And I am sure there are people in Finland that support Argentina. Hell, some of my friends even support Italy, imagine that! :) \n\nYou might wonder why I don't speak about Finnish football, it is simple, it is shit. European championship tournament starts next Saturday. Even Latvia is playing there and Finland is not, for crying out loud! Not that is a bad thing that Latvia got in the cup, I think it is wonderful! And Riga is a beautiful city I were there on last July and I am coming back on this July. :)\n\nSo, Roberto, see you in two years in Germany! I am probably going there because it is not far a way from Finland. \nAnd best of luck for qualifying games.\n\nSincerely yours,\n138\n\nps. Women. Loyalty. Oxymoron."
    author: "138"
  - subject: "Re: Ladies are excluded too ..."
    date: 2004-06-04
    body: "> Yes, he said: \"That's one small step for a man, one giant leap for mankind.\"\n\nThat is funny because I always remember trying to figure out what poetic meaning that was supposed to be when I heard it. (Yes, I remember watching it on TV and it sounded like \"one small step for man\".) Years later I heard that the transmission wasn't so good and that is what he actually said, which makes more sense. It is still today often misquoted.\n\nThis is actually a testament to how confusing English can be."
    author: "Eric Laffoon"
  - subject: "OMG the cracksmokers hit the net again..."
    date: 2004-06-03
    body: "http://the-amazing.us/simplyamerican/4696/\nhttp://the-amazing.us/simplyamerican/4698/\nhttp://the-amazing.us/simplyamerican/4699/"
    author: "ac"
  - subject: "Re: OMG the cracksmokers hit the net again..."
    date: 2004-06-04
    body: "Are those guys really serious?"
    author: "138"
  - subject: "Everybody sing with me..."
    date: 2004-06-04
    body: "Jingoism."
    author: "Robert"
  - subject: "Re: OMG the cracksmokers hit the net again..."
    date: 2004-06-04
    body: "Funnily enough the google-ad at the first link advertises radicalradio.org, guess google made a political statement with that as well. =P"
    author: "Datschge"
  - subject: "Re: OMG the cracksmokers hit the net again..."
    date: 2004-06-04
    body: "Those guys idiocy is beyond the pale. Give the authors of KDE's user database a few days to fix the issue and shut the fuck up."
    author: "Just me.."
  - subject: "\"anti-American propaganda\"?"
    date: 2004-06-04
    body: "What propaganda? Only you're doing it by omitting stuff as \"Please note, that this is not an official KDE users counter\" and giving the impression of a widespread attitude in the KDE project (\"feelings find their way into the project\"). There are more important things your country should worry about like respecting human and international rights."
    author: "Anonymous"
  - subject: "Re: OMG the cracksmokers hit the net again..."
    date: 2004-06-04
    body: "Some nice quotes from the same website:\n\nNeil Stevens, about Mexico:\n\n\"I say that being born in a poor, backward country like Mexico is something no American should have to endure.\" : http://the-amazing.us/simplyamerican/4660/\n\nNeil Stevens, about homosexuality:\n\n\"Homosexuality just might be the most destructive congenital brain defect mankind has ever experienced. If only we knew for sure what caused it, so that we could treat it and gradually end this source of societal decay.\" : http://the-amazing.us/simplyamerican/4616/\n\nTerrible."
    author: "Sergio"
  - subject: "Re: OMG the cracksmokers hit the net again..."
    date: 2004-06-04
    body: "There's a reason I posted my commentary off-site on a privately operated and funded domain: politics don't belong in KDE or on the Dot. I'm not sure what makes you think your disagreement with *completely off-topic and unrelated* commentary is important enough to be posted here."
    author: "Rob Kaper"
  - subject: "Re: OMG the cracksmokers hit the net again..."
    date: 2004-06-04
    body: "I guess you are right. I only wanted people to have an idea of what kind of opinions are regularly posted on your website, so they can read your KDE related post in perspective."
    author: "Sergio"
  - subject: "Re: OMG the cracksmokers hit the net again..."
    date: 2004-06-04
    body: "\"I say that being born in a poor, backward country like Mexico is something no \nAmerican should have to endure.\"\n\n\"Homosexuality just might be the most destructive congenital brain defect\nmankind has ever experienced. If only we knew for sure what caused it, so that\nwe could treat it and gradually end this source of societal decay.\"\n\n\n\nAnd between statements like these, the speak about baseball...\n\n\n\nRob Kaper, do you agree with what Mr Stevens stated above, would you put\nyour signature under these statements?"
    author: "ac"
  - subject: "Re: OMG the cracksmokers hit the net again..."
    date: 2004-06-04
    body: "I do not post my politic views (or those of others) on the Dot. Besides, you know where to find the website where you can read my opinions on topics like these. After all, you've read it.\n\nI like KDE and a lot of its contributors. Enough to have learned to respect that most people prefer not having to deal with politics when their attention shifts to the project.\n\nStop trying to enforce the very confrontations I'm trying to avoid. It is not going to happen, so give it up."
    author: "Rob Kaper"
  - subject: "Re: OMG the cracksmokers hit the net again..."
    date: 2004-06-05
    body: "I value this as a \"yes\" on both questions, since there was no disagree from you.\n\nBetter stay away from KDE as we don't need people like you."
    author: "ca"
  - subject: "Re: OMG the cracksmokers hit the net again..."
    date: 2004-06-05
    body: "Please don't assume you can speak for KDE as whole. As stated before, KDE is politically neutral (at least as far as it doesn't directly involve the project itself.) Sure, sometimes a political comment of sorts (or something that can be interpretted like that) slips through, but that only reflects the opinion the one stating it, not of the project as a whole. It's usually quickly remidied.\nI think \"we\" don't need people who want to deny other people participation in the project based on their political views, which they keep outside of the KDE project. "
    author: "Andre Somers"
  - subject: "No fix in sight ..."
    date: 2004-06-04
    body: "Hello? www.kde.org shouldn't be leaving the link on their website as long as United States and Iraq aren't added.  This will continue to offend more people.  \n"
    author: "Day 2"
  - subject: "Re: No fix in sight ..."
    date: 2004-06-04
    body: ">Hello? www.kde.org shouldn't be leaving the link on their website as long as United States and Iraq aren't added. This will continue to offend more people.\n\nHello indeed... want something offending?\nSHUT THE FUCK UP, WILL YOU JUST SHUT THE FUCK UP ALREADY, PLEASE!!!! \nHow about those other countries that where missing, how come the citizens of those countries aren't bitching here for that simple mistake? Maybe because they are not egomaniac idiots who think that the world is spinning around their oh so great and ever righteous nation? You americans allways act so tough and mean but when something small goes wrong or there is a simple mistake you are bitching and moaning like there is no tomorrow. No wonder nobody likes you... \nJesus Christ! The guy was just trying to make a KDE user database for fun, he made a mistake and you start screaming like they were cutting your heads off(pun intended)!\n\nps. There is no link from www.kde.org to users.kde.pl, only from dot.kde.org, you illiterate inbred imbecile!"
    author: "Have a nice cup of STFU!!!"
  - subject: "Listen please..."
    date: 2004-06-04
    body: "Hello !\n\nLook on users.kde.pl now. Don't be so inpatient and forgive me ! I wasn't at home for 3 days, and this misunderstanding hasn't been maked purposely ! Don't call it \"childish\" or connect it to politics, it's not that !\n\nOnce more, forgive me, but for now it's obviously corrected on site.\n\nRegards,\nRael"
    author: "Rael"
  - subject: "Re: Listen please..."
    date: 2004-06-04
    body: "s/hasn't been maked/hadn't been made..."
    author: "Rael"
  - subject: "Re: Listen please..."
    date: 2004-06-04
    body: "Well done.  It's a nice project. A shame this all got a bit off-topic for a while. I see you're even accepting ladies as members now :)"
    author: "Henrik"
  - subject: "Re: Listen please..."
    date: 2004-06-05
    body: "Lots of \"thank you\" for the aclaration.\n\nThat was what was refraining me from spreading the link. Sorry for the confusion, but it must be clear that KDE is not involving on political statements outside of computer tech and science issues. This was just too (accidentally) direct to ignore."
    author: "Cloaked Penguin"
  - subject: "Re: Listen please..."
    date: 2004-06-05
    body: "OK, understood of course. \n\nNow it's clear, I think so :)\n\nCheers,\nRael"
    author: "Rael"
  - subject: "Re: Listen please..."
    date: 2004-06-05
    body: "Users should apologising to you, not the other way around. \nKeep up the good work!"
    author: "John Thanking Freak"
  - subject: "Re: Listen please..."
    date: 2004-06-05
    body: "Don't let all this negative feedback discourage you. You provide a great service, and made an honest but slight mistake, a lot of people got paranoid and it got out of hands for a little while. It's still a great service, and I registered promptly, as did all my KDE-using friends. Thanks."
    author: "Haakon Nilsen"
  - subject: "Re: Listen please..."
    date: 2004-06-05
    body: "Hi Rael\n\nDon't worry about the negative remarks - folks tend to expect things to be fixed immediately, forgetting that developers have lives outside kde (well, some anyway :)\n\nI'm sure everyone who moaned will be man/woman enough to say thanks now.\n\nKeep up the good work\nRich"
    author: "Rich"
  - subject: "Re: Listen please..."
    date: 2004-06-05
    body: "Thanks for Your indulgence, it's important for me :)\n\nBy the way, I added few minutes ago some statistics for site, which you can use for your websites (by XML file - http://users.kde.pl/?page=xml) :) These are only numbers, but - obviously - it's the target of my project :) I must create some kind of changelog, because I'm adding new functions and killing new bugs average few times a week.\n\nYes, I was outsite KDE for few days - I was among the mountains (yeess....I love them !) and trees (them too :) )...I feel like at beginning of my life (hmm. do I remember it ?;) ) - this trip relaxed me very much, that's what I need :)\n\nCheers,\nRael"
    author: "Rael"
  - subject: "Re: Listen please..."
    date: 2004-06-05
    body: "Thanks Rael.\n\nIgnore the idiots, xenophobia and paranoia often lead people to jump to conclusions just so thaey have an excuse to attack somebody.  You're just the latest innocent victim.  They don't speak for the majority of KDE users or Americans."
    author: "Jim"
  - subject: "Re: Listen please..."
    date: 2004-06-05
    body: "Once again I can say - thanks.\n\nRael"
    author: "Rael"
  - subject: "Re: Listen please..."
    date: 2004-06-08
    body: "I know it sounded kinda negative, but I'm certain most people who commented were, as I was, just confused. It seemed like an honest mistake, and I'm glad it was an honest mistake.\n\nNow I can add myself to that list! Huzzah!"
    author: "jameth"
  - subject: "Re: Listen please..."
    date: 2004-06-08
    body: "hehehe.\n\nI went to sign up, typed 'Unit' in the combo-box and got nothing. I was trying to figure out why it wasn't there when I realized it was just entered as USA. I had quite a laugh at myself."
    author: "jameth"
  - subject: "Statistics by country"
    date: 2004-06-04
    body: "It could be good to add statistics with the countries with more KDE users IMHO ;-)"
    author: "Pedro Jurado Maqueda"
---
A few days ago, we've opened a new section on our Polish K Desktop Environment <a href="http://www.kde.pl/">website</a> called "<a href="http://users.kde.pl/">KDE Users Database</a>". This is not only for Polish users, it's international. After some time it'll be good rate of KDE users number all over the world. Every registered user can save generated certificates and put them on his desktop or website. So, let's register! Please note, that this is not an official KDE users counter - but, maybe in future - it might change.
<!--break-->
