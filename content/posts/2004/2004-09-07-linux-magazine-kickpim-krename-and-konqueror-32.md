---
title: "Linux Magazine: KickPIM, KRename and Konqueror 3.2"
date:    2004-09-07
authors:
  - "binner"
slug:    linux-magazine-kickpim-krename-and-konqueror-32
comments:
  - subject: "[OT] lists.kde.org"
    date: 2004-09-07
    body: "What happened to http://lists.kde.org?"
    author: "fake"
  - subject: "Re: [OT] lists.kde.org"
    date: 2004-09-07
    body: "What did happen? It works for me."
    author: "AC"
  - subject: "Re: [OT] lists.kde.org"
    date: 2004-09-08
    body: "it's back up. MARC was having issues"
    author: "anon"
  - subject: "Konqueror is progressing!!!!!!!!"
    date: 2004-09-08
    body: "Konqueror is progressing!! Despite being unable to properly parse anything that doesn't conform strictly to HTML 4.0, Konqueror now makes it possible to edit your bookmarks via a context menu (too bad this functionality isn't yet available for more basic applications, such as KMenu). With features like this, it doesn't matter whether or not you can actually view pages!! After all, isn't that why they made Mozilla?? If I didn't have to sit around for twenty minutes waiting for Mozilla to start whenever Konqueror failed to display a page, what would I do with my life?? Anyway, I could never part with Konqueror, because then I wouldn't have the joy of a bloated, multi-function, unnavagable sidebar that I must invoke everytime I want to perform just one specific function. Another great feature of Konqueror is the fact that there is just one \"home\" button, which can be set to either a website or my home directory! I wouldn't want to have two separate buttons, because that would be too much choice, and everyone knows that free software is about less choice!!!\n\nAt any rate, congratulations!!! Konqueror is now at the point where Internet Explorer was five years ago!!!!!!!"
    author: "Annoyingly Realistic"
  - subject: "Re: Konqueror is progressing!!!!!!!!"
    date: 2004-09-09
    body: "That was pretty funny.\n\nIt doesn't a-do so a-well with the pages some-timesa. You wanna spicy meatball?"
    author: "Joe"
  - subject: "Re: Konqueror is progressing!!!!!!!!"
    date: 2004-09-09
    body: "Konqueror works perfectly on all my sites, which I program in XHTML 1.0 Transitional.  I use KDE 3.2 so maybe thats a new (new-ish) feature?  :-P"
    author: "Corbin"
  - subject: "Re: Konqueror is progressing!!!!!!!!"
    date: 2004-09-11
    body: "I was exaggerating a bit. Konqueror CAN view sites that don't conform to HTML 4.0, obviously. It seems to have trouble with highly complex sites that make heavy use of EMCAScript and CSS. Mozilla, however, doesn't have this trouble. I can't list the sites here because they are specific to my college, and so nobody would be able to get on them anyway."
    author: "Annoyingly Realistic"
  - subject: "Re: Konqueror is progressing!!!!!!!!"
    date: 2004-09-11
    body: "I suggest you read http://weblogs.mozillazine.org/hyatt/archives/2003_11.html\n\nand then either STFU or help in some way (if you can't program, you can always clean up/triage khtml bugs on bugzilla)"
    author: "anon"
  - subject: "TODO App?"
    date: 2004-09-08
    body: "Yesterday I was searching for a standalone Todo app for KDE... preferably something that accesses the KOrganizer/Kontact database (for syncing to my Palm).  I would like something that is light, quick to load (unlike Kontact) and I can pop in new Todos quickly.  Something that sat in the tray like KNotes would be great.\n\nDoes it exist?  Here's a hint as to why I'm not sure... try Googling for TODO and KDE (or TODO and *any* large software project)!!\n\n(KickPIM looks like what I want, but not for the Addressbook)."
    author: "Evan \"JabberWokky\" E."
  - subject: "KOrganizer"
    date: 2004-09-09
    body: "You mean like KOrganizer?  Thats exactly whats in Kontact (Kontact embeds all those programs into one easy to use PIM."
    author: "Corbin"
  - subject: "Re: KOrganizer"
    date: 2004-09-09
    body: "Oops!  Hit the wrong reply! haha!  This was supposed to be under \"TODO App?\"... Oh well!"
    author: "Corbin"
---
In its <a href="http://www.linux-magazine.com/issue/47">current issue</a> <a href="http://www.linux-magazine.com/">Linux Magazine</a> published an <a href="http://www.linux-magazine.com/issue/47/KickPIM.pdf">article (PDF) about</a> <a href="http://kickpim.sourceforge.net/">KickPIM</a>, a panel applet for quick access to your address book. Already published in a <a href="http://www.linux-magazine.com/issue/45">previous issue</a> and also online available is a <a href="http://www.linux-magazine.com/issue/45/KRename.pdf">story (PDF) about</a> <a href="http://www.krename.net/">KRename</a>, a <a href="http://dot.kde.org/1088839398/">batch file renamer</a>. Finally we were notified by Linux Magazine that a <a href="http://www.linux-magazine.com/issue/43/Konqueror_32.pdf">review of Konqueror 3.2 (PDF)</a> has been added to the archive of the <a href="http://www.linux-magazine.com/issue/43/">June 2004 issue</a>.
<!--break-->
