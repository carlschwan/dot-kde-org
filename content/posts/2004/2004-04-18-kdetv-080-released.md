---
title: "kdetv 0.8.0 Released!"
date:    2004-04-18
authors:
  - "dziegelmeier"
slug:    kdetv-080-released
comments:
  - subject: "looks good!"
    date: 2004-04-18
    body: "looks very nice!\n\ndoes it support DVB Satalite tv card?\nI haven't found a single decent player for it yet.\neven with the great drivers that are in the 2.6 kernel."
    author: "Mark Hannessen"
  - subject: "Re: looks good!"
    date: 2004-04-18
    body: "Not yet, but it's easy to add support.  kdetv is fully plugin driven, so we just need someone to write the appropriate plugin and the devices will automatically show up in the GUI and be accessible!"
    author: "George Staikos"
  - subject: "Deinterlacing"
    date: 2004-04-18
    body: "Is it possible to use the DScaler deinterlacing algorythm with kdetv, like with tvtime? Deinterlacing is quite critical to image quality."
    author: "Eike Hein"
  - subject: "Re: Deinterlacing"
    date: 2004-04-18
    body: "The plan is to add this soon, but we just haven't found anyone with the time and motivation to implement it.  I think it's a good project to do as a branch of the v4l plugin.  I think this integration has to be done for each plugin individually, though right now it only applies to the V4L plugin.  We also want to implement \"filters\", but I'm not sure if that's a good choice for dscaler.  It may not be efficient enough.\n\nMeanwhile the video quality really isn't so bad, at least for me.  I am able to watch sporting events and watch scrolling tickers without problems.  Clearly it can be improved though."
    author: "George Staikos"
  - subject: "Re: Deinterlacing"
    date: 2004-04-18
    body: "Sounds good. How about a donate button on kwintv.org, or the upcoming kdetv.org?"
    author: "Eike Hein"
  - subject: "Re: Deinterlacing"
    date: 2004-04-19
    body: "I must have missed something.\n\nThis software shows the picture on your computer's monitor, right.\n\nIt uses your video card's frame buffer to do that, right.\n\nSo, just exactly what is \"deinterlacing\"?\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Deinterlacing"
    date: 2004-04-19
    body: "Combining two half-pictures such that you don't see \"comb artifacts\" on fast horizontal motion, e.g. scroll text. "
    author: "Dirk Ziegelmeier"
  - subject: "Configuration: \"GUI\""
    date: 2004-04-18
    body: "A rather unfortunate choice in this otherwise nice program. \"GUI\" is not a term the usual user is familiar with, \"General Usage\", \"User Interface\" or something to that effect would be better."
    author: "Anno"
  - subject: "vdr support and filters like in tv-time?"
    date: 2004-04-18
    body: "vdr support will be very nice for my dvb-card, because my monitor is my tv-screen.\nAnd filter's like in tv-time too ;-)\n\nThx for this kde-apps. I'm trying it this week.\n"
    author: "anonymous"
  - subject: "Oh BTW"
    date: 2004-04-18
    body: "has anyoen else tried TVTIME? It totally blows the other TV programs I tried away. It looks so much better, simple to use, ACTUALLY GOES FULLSCREEN, the quality is excellent and it is not resource hungry. It's only weakness is that it does not yet support as many cards.\n\nIMO, all the other TV programs for Linux should be abandoned or focused on this program, because the quality doesn't even stand comparrison. Why does nobody make a better interface for TVTIME?\n\n"
    author: "Alex"
  - subject: "Re: Oh BTW"
    date: 2004-04-18
    body: "Sorry, if I'm being mean. But after trying TV Time it LOOKS SO MUCH BETTER and ACTUALLY GOES FULLSCREEN and I don't see why the project is ignored so much when it seems the technology is so good. Xandros is among the only distribution that even has TVTIME available."
    author: "Alex"
  - subject: "Re: Oh BTW"
    date: 2004-04-18
    body: "Oh also. If I'm talking bullshit based on the previous version, it's because I haven't tried it yet. "
    author: "Alex"
  - subject: "Re: Oh BTW"
    date: 2004-04-18
    body: "What's wrong about the way that kdetv goes fullscreen? \n\n"
    author: "cm"
  - subject: "Re: Oh BTW"
    date: 2004-04-18
    body: "It goes fullscreen? Last tiem I tried KwinTV it would only black out most of the screen and show a small image 640x480 or soemthing liek that. When TVTIME goes fullscreen it coveres the WHOLE screen and still manages to have significantly better image quality. Has this changed?"
    author: "Alex"
  - subject: "Re: Oh BTW"
    date: 2004-04-19
    body: "Funny thing about \"complete rewrites\" - they don't have anything to do with previous versions.  You might as well compare a rock to a tree - KWinTV and KDETV are different apps.  It's like me criticising TVTIME because last time I tried lynx, it didn't do graphics.  The two are not connected."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Oh BTW"
    date: 2004-04-19
    body: "It looks good, yes. But there is no nice way to configure the channels, it's quite unstable for me (freezes from time to time) but most of all, beside the very good de-interlace algorithms (which were it's main focus as it seems) it does not have any features. No live pause, no recording... it's pretty 90s.\n"
    author: "AC"
  - subject: "Re: Oh BTW"
    date: 2004-04-20
    body: "> has anyone else tried TVTIME? It totally blows the other TV\n> programs I tried away.\n\nI tried it, and it blew itself away.  Segfaults like it was built for Windows."
    author: "Artie"
  - subject: "KDETV and Qtvision?"
    date: 2004-04-18
    body: "Has Qtvision been renamed to KDETV or is this a different program?"
    author: "Alex"
  - subject: "Re: KDETV and Qtvision?"
    date: 2004-04-18
    body: "Yes, it's been renamed. QtVision was always a development name.\n"
    author: "Richard Moore"
  - subject: "Really Good"
    date: 2004-04-18
    body: "I've pulled down the SuSE 8.2 rpms, and this is an awesome improvement to the kwintv.  It still has the same problems with leaving artifacts in normal mode (the mode with the sidebar), but full screen and tv mode are fantastic.  Thank you very much."
    author: "sphere"
  - subject: "why is KDE 3.2.2 on hold ?"
    date: 2004-04-18
    body: "Hi, I'm just curious, not angry or disappoined at all: does anybody know why KDE 3.2.2 takes so long ? I read it was tagged in CVS around 3rd of april ?! \n\nThanks to all the people who made KDE what it is now! And I'm refering to version 2-3.x! ;-)"
    author: "yep"
  - subject: "Re: why is KDE 3.2.2 on hold ?"
    date: 2004-04-19
    body: "Because you're not using Debian ;-)"
    author: "Spy Hunter"
  - subject: "Re: why is KDE 3.2.2 on hold ?"
    date: 2004-04-19
    body: "I do use Debian - but I use stable. So why is KDE 3.2.2 in Debian unstable but not listed on www.kde.org ?"
    author: "yep"
  - subject: "Re: why is KDE 3.2.2 on hold ?"
    date: 2004-04-19
    body: "Because KDE likes to wait for packages to be released for as many distros as possible before announcing a release so everyone isn't disappointed when they can't have it right away.  Apparently Debian was fast this time, while others are slow.  It may also be that the Debian packages are an experimental pre-release and bugs are still being fixed.  Having Debian unstable packages is a great way to instantly get hundreds if not thousands of willing beta testers :-)"
    author: "Spy Hunter"
  - subject: "Some errors here, but..."
    date: 2004-04-18
    body: "I am unable to get through to your e-mail due to sending from cable based home system (damn spammers).\n\n1) during the compile on Mandrake 9.2\nfrom top directory of your release:\n\nvi +25 ./kdetv/libkdetv/channelwidget.cpp\n\nwas :\nstatic const unsigned char const img0_channelwidget[] = {\n\n(2'nd const unneed on array, but you knew that)\n\nneeds to be:\nstatic const unsigned char img0_channelwidget[] = {\n\n\n\n2) I had problems with getting the wizard to correctly assign the frequencies. I ended up taking qtvision's apps/channel.xml, copying it to apps/kdetv, and moding <encode> to <encode>ntsc.\nBasically, the old wizard did a much better job getting frequencies correct (us cable/hrc and tried us cable; this was for aurora, co USA on comcast)\n\nGood stuff overall.\n\nA couple of thoughts for what it is worth.\n1) allow an option to be on all desktop upon start-up.\n2) I turn off screen saver for full screen only (great idea), but it would be nice to have it mute when saver comes on (that may happen with this one, but did not with qtvision).\n3) allow a middle mouse click directly on the window to mute just that (on knob a nice applet, a middle mouse click mutes the whole system).\n\nGood stuff, though. I do know that it is a lot of work to get it to this point."
    author: "a.c."
  - subject: "Re: Some errors here, but..."
    date: 2004-04-19
    body: "1) This file is generated by uic... It's an uic bug:\nhttp://lists.trolltech.com/qt-interest/2004-02/thread00532-0.html\nHowever, I'll try to work around it.\n\n2) I use the xawtv freq lists, which should be the most stable available...\n\nIdeas:\n1) Please explain (do you mean \"sticky\"?)\n2+3: Good ideas ;-)"
    author: "Dirk Ziegelmeier"
  - subject: "Re: Some errors here, but..."
    date: 2004-04-19
    body: ">However, I'll try to work around it.\nA simple sed in the makefile might be easy.\npersonally, I always hate work arounds, but...\n\n>Ideas\n>1) Please explain (do you mean \"sticky\"?)\nYep."
    author: "a.c."
  - subject: "Some thoughts"
    date: 2004-04-18
    body: "after apt-getting it:\n\n1. I like it, and hopefully I can ditch xawtv soon (as I ditched xmms for juk)\n\n2. There should be a possibility, in the application, to set it \"always on top\"\n\n3. it should go away from the taskbar, and stay exclusively in the systemtray\n\n4. the \"Elegant on screen Display plugin\" shows the channel always as fontsize\n48, even when I run kdetv in a small window\n\n5. The preferences dialog does not seem to remember it's last position\n\n6. When I \"zap\" between channels with the mousewheel, juk gets interrupted\nshortly between every channel change\n\nLooking forward to see it in kde3.3 (which is due in august 2004 IIRC)  ;-)"
    author: "ac"
  - subject: "Re: Some thoughts"
    date: 2004-04-19
    body: "I still haven't tried it yet. I will tomamrow, but always on top anyway is a Kwin feature and needs not be implemented by KDETV itself. I think that it should have a system tray option jsut like JUks, but not exclusive. Also, are you using kernel 2.6?"
    author: "Alex"
  - subject: "Re: Some thoughts"
    date: 2004-04-19
    body: "> 3. it should go away from the taskbar, \n> and stay exclusively in the systemtray\n\nI would prefer if it didn't.  Maybe have both choices as a configuration option?"
    author: "Rune Kristian Viken"
  - subject: "Re: Some thoughts"
    date: 2004-04-19
    body: "1) Hope you can ;-)\n2) Settings/GUI: \"Always on top\" or window menu from KDE.\n3) Good idea.\n4) Go to Settings/Plugins. Select elegant plugin, click configure, check \"Auto Size\" box.\n5) Ack.\n6) Is no kdetv problem. It only mutes the configued mixer element and the TV card. Maybe it happens due to a CPU load peak when switching channels.\n\nDon't think it'll be in kde3.3. We are in kdeextragear-3 at the moment since we want to be independent of KDE's release cycle."
    author: "Dirk Ziegelmeier"
  - subject: "Re: Some thoughts"
    date: 2004-04-19
    body: "5. Window Menu: Advanced->Store Window settings.\n6. Try with overlay mode. That will rule out the CPU load issue.\n\nAndras\n\nPS: I wrote a detailed answer, but it got lost. But Dirk had the same answers. ;-)"
    author: "Andras Mantia"
  - subject: "A great program"
    date: 2004-04-19
    body: "This is a great program, recently I switched from xawtv to kdetv because it's more integrated in the desktop and I can see in fullscreen (real fullscreen). The plugins system it's a very promising project to add new features easily.\n\nGo on with this great aplication"
    author: "Pedro Jurado Maqueda"
  - subject: "can KDETV do record?"
    date: 2004-04-19
    body: "I have been trying to find a program that would do high quality t.v record, but so far no luck. Does anyone know of any high quality recording programs?\n\nthanx in advance"
    author: "someone"
  - subject: "Colour inversion?"
    date: 2004-04-19
    body: "Is there support for colour inversion on-the-fly? That will allow me to watch cable TV on my computer (yes, I'm a paying subscriber). If so, how CPU-intensive is it?"
    author: "Yama"
  - subject: "Re: Colour inversion?"
    date: 2004-04-19
    body: "No, kdetv is not able to do this."
    author: "Dirk Ziegelmeier"
  - subject: "xml stuff?"
    date: 2004-04-21
    body: "Forgive me if I don't know the terminology, but can kdetv decode xml channel program listings and display them the way tvtime can?  I switched from xawtv to tvtime because tvtime had closed captions, and now I'm hooked on the program listings, but since I switched to 2.6 kernel tvtime spontaneously 'forgets' the color and hue settings and goes back to 50% (which gives a horrible pink image) every couple of seconds, making it unwatchable, so I'd love to switch to something else.."
    author: "Tom"
  - subject: "Re: xml stuff?"
    date: 2004-04-23
    body: "I guess you are referring to xmltv. No kdetv cannot decode/display them, but a plugin could be written for it (it's on the ideas/wishes list since more than a year, but noone has the time for it)."
    author: "Dirk Ziegelmeier"
  - subject: "Unable to hear AUDIO!"
    date: 2004-05-12
    body: "I've got a Kworld SAA7134(it's just a Philips), that works properly with the kernel module saa7130.\nThe problem ==> I can't hear the sound of TV-CARD!\nThe line-in it's enabled, and if i plug any other source of sound in the line-in I hear, if i plug the tv-sound-cable NO SOUND!\nPCI-TV-CARD it's not broken, cause I tried it on winzoz, and the audio is ok!\n\nSo I think there is a volume that I can't control directly on the PCI-CARD something like Line-in1 on /dev/mixer\n\nIf I open Kradio (my TV-CARD doesn't support the radio signals), clicking on \"Power\" finally I hear the typical sound, when I start kdetv, the sound stops!\n\nI tried even other software!\n\nPLEASE HELP ME! My distro is a SuSE Linux 9.1 with 2.6 Kernel and KDE 3.2.1\n\nTHANKS"
    author: "Pinco"
  - subject: "Re: Unable to hear AUDIO!"
    date: 2004-05-18
    body: "So? No one can help me???"
    author: "Pinco"
  - subject: "Re: Unable to hear AUDIO!"
    date: 2004-06-04
    body: "Use the latest snapshot (http://dziegel.free.fr/snapshots). This is a known problem of kdetv on Kernel 2.6.X and saa driver."
    author: "Dirk Ziegelmeier"
  - subject: "Re: Unable to hear AUDIO!"
    date: 2004-06-07
    body: "Nothing... I can't resolve this bug! What a shit... I'be been searching for the solution, for many times! PLEEEEEAAAAAASSEE! -.-'"
    author: "Pinco"
  - subject: "Re: Unable to hear AUDIO!"
    date: 2004-10-23
    body: "With later versions of kdetv everthing runs fine for me now."
    author: "Grothesk"
  - subject: "Re: Unable to hear AUDIO!"
    date: 2006-01-13
    body: "Same problem in ubuntu. Only i dont have radio in my card...no singal from my line out....\nSomeone make \"easy fix\" "
    author: "Dane"
  - subject: "Same problem: no sound"
    date: 2004-06-03
    body: "I have a Pinnacle PCTV TV-card and I have the same problem as Pinco: no sound.\nI noticed that when I tell kdetv to use the sound device of the tv-card (Brooktree Bt878) instead of my sound card (VIA 8235), and I click on OK, then if I come back to the settings, I see that kdetv is set again to use my sound card (VIA 8235).\nI think the cause is this: when I set Bt878, there are no \"Mixer Elements\" to select, so kdetv doesn't keep the correct setting.\nBut I don't know what to do to solve the problem.\nI will try using also Mplayer."
    author: "sw0rdmaster"
  - subject: "Country not found"
    date: 2004-08-24
    body: "I am unable to tune the channels using kde.\nFirst of all India is not found in the country pull down.\nUsing the expert option I tried using a  similar range from the list. But still the frequency increment or something is not working properly. \nI have got a Prolink Pixelview PlayTV Pro as Tuner Card and Using Suse 9.1 personal edition. Can anyone please tell me how I can tune channels using kdetv.\n\nThanks"
    author: "Abhilash"
  - subject: "Re: Country not found"
    date: 2005-10-10
    body: "Me too. I am from India and i am not able to tune it for my req.ment.\nthank you\nG S. Kini"
    author: "Santhosh G. Kini"
---
After more than two years of development, the long anticipated successor of 
KWinTV has reached its first public release. kdetv is an application to 
watch TV using Xv or video4linux compatible video cards.  With this release of 
kdetv, Linux users can now enjoy a user-friendly desktop TV viewing 
experience.





<!--break-->
<p>Features of kdetv include three view modes, a channel scanner, the ability to  import the channel files of three other TV programs, teletext and closed
caption decoding, and an easy-to-use graphical user interface. The application supports remote controls using KDELIRC, a component shipped in KDE 3.2.</p>
<p>kdetv requires KDE libraries version 3.1 or above, a TV tuner card with 
video4linux or XVIDEO-input support, and an ALSA or OSS compatible mixer 
device.</p>
<p><a href="http://www.kwintv.org/screenshots.html">Screenshots</a>, <a href="http://www.kwintv.org/rewrite.html">more information</a> and most important of all, <a href="http://www.kwintv.org/download.html">installation 
instructions</a> can be found at the website <a href="http://www.kwintv.org/">http://www.kwintv.org</A> (we are working on http://www.kdetv.org).  Packages will be made available for many popular distributions.</p>

