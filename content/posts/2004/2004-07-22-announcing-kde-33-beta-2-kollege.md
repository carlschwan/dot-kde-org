---
title: "Announcing KDE 3.3 Beta 2 \"Kollege\""
date:    2004-07-22
authors:
  - "ikulow"
slug:    announcing-kde-33-beta-2-kollege
comments:
  - subject: "feature plan?"
    date: 2004-07-22
    body: "From the KDE 3.3 feature plan, it seems there is still a lot of stuff in the TODO list and quite a bunch in the \"in progress\" list too. Is this compatible with a beta2 or is the page just out-dated?\n\nThe features that are not done will never benefit of serious testing if they are not at least \"in progress\" during the beta.\n\nAm I missing something?"
    author: "oliv"
  - subject: "taken from the release plan?"
    date: 2004-07-22
    body: "I looked at the release plan and I indeed think people will have to hurry. I don't know if the feature list it up to date but it *looks* like there are many many things to do, and there is very very little time."
    author: "Mark Hannessen"
  - subject: "Re: taken from the release plan?"
    date: 2004-07-22
    body: "No hurry. All features not within this beta will not be in the final. Their entries will be moved to a future KDE feature plan - some of the \"todo\" list may be in and only need to be set to state \"done\" though."
    author: "Anonymous"
  - subject: "Re: taken from the release plan?"
    date: 2004-07-22
    body: "So... uh... what IS in this beta?"
    author: "Lee"
  - subject: "Re: taken from the release plan?"
    date: 2004-07-22
    body: "The \"inprogress\" and \"done\" items."
    author: "Anonymous"
  - subject: "bugs"
    date: 2004-07-22
    body: "It's good to see that bugs.kde.org reports more bugs found then submitted. it seems that another 2000 bugs more appear on the kde buglist. this can also be blamed on the fact that many many new programs have been added. In general I think kde is very stable. we just need to get some important applications like kontact and kopete(voice+webcam?) past something we can label 1.0 and kde will rock!"
    author: "Mark Hannessen"
  - subject: "Re: bugs"
    date: 2004-07-23
    body: "> It's good to see that bugs.kde.org reports more bugs found then submitted.\n\nRead what you wrote! :-) I guess you mean more closed reports than submitted."
    author: "Anonymous"
  - subject: "Re: bugs"
    date: 2004-07-23
    body: "\"bugs.kde.org reports more bugs found then submitted.\"\n\nSeems like they are reproducing. :-)"
    author: "Amit Upadhyay"
  - subject: "Looks very good!"
    date: 2004-07-22
    body: "I've been using CVS HEAD for couple of days, and so far KDE 3.3 looks very good - like new KDE releases always do! I was happy to find Joystick module in KControl, very nice! Now it would be fun to have OpenGL 3D games for KDE!\n\nWhat I'm missing / hoping: Search in KDE Help Center and fixes in KBabel.\n\nSearch in help was dropped, because KDE changed to new documentation format during 2.x -series. I hope that forthcoming ht://dig search engine will be able to index and search application documentation, some people are working on this.\n\nKBabel chokes constantly when searching translations. Somehow this starts on strings that do not have translation in the database. Please, please, Andrea, fix it :)\n\nOther than these, KDE 3.3 Beta 2 is very stable and nice. I do not recommend running betas on production use, although this . Please test and send bug reports!\n\nEleknader"
    author: "Eleknader"
  - subject: "Re: Looks very good!"
    date: 2004-07-22
    body: "If you are interested in joypads, you might also find this interesting.\nhttp://qjoypad.sourceforge.net\n\nthis program can map joypad buttons and axis to key and mouse movements.\nI used it for playing savage and it worked great!"
    author: "Mark Hannessen"
  - subject: "Gentoo ebuilds?"
    date: 2004-07-22
    body: "Are there Gentoo ebuilds for this release somewhere? I'm waiting on portage getting KDE 3.3 ebuilds, but there are only some for kdelibs, etc. but not for the whole KDE.\n\nAm I just missing something?"
    author: "Emmeran \"Emmy\" Seehuber"
  - subject: "Re: Gentoo ebuilds?"
    date: 2004-07-22
    body: "Patience."
    author: "Jeroen"
  - subject: "Re: Gentoo ebuilds?"
    date: 2004-07-22
    body: "There are the KDE CVS ebuilds that compile the cvs version of KDE. The ebuilds for KDE 3.3 beta 2 should be released soon."
    author: "Daniel Dantas"
  - subject: "Re: Gentoo ebuilds?"
    date: 2004-07-22
    body: "or keep an eye on this page: http://packages.gentoo.org/search/?sstring=kdelibs"
    author: "Bobke"
  - subject: "Re: Gentoo ebuilds?"
    date: 2004-07-25
    body: "There will be no beta2 ebuild for kde-base/kde, only stable versions. You'll have to emerge each package (kdelibs, kdebase, ..) manually."
    author: "romans"
  - subject: "Re: Gentoo ebuilds?"
    date: 2004-07-26
    body: "No, that isn't true. There already ARE beta2 ebuilds. Currently portage is working on kdepim-3.3.0_beta2 on my system."
    author: "Sebastian"
  - subject: "Re: Gentoo ebuilds?"
    date: 2004-07-26
    body: "KDE from CVS on Gentoo http://www.gentoo.org/proj/en/desktop/kde/kde-cvs.xml"
    author: "Jo \u00d8iongen"
  - subject: "Live cd anyone?"
    date: 2004-07-22
    body: "I hope some distro will make a livecd with beta 2 like Onebase did for beta 1.\nReally, you KDE people should release an \"official\" live cd every alpha, beta or release candidate. That would be easier and more comfortable for everyone willing to test and report bugs."
    author: "fake guy"
  - subject: "Re: Live cd anyone?"
    date: 2004-07-22
    body: "There will be never \"official\" binary packages or live CDs."
    author: "Anonymous"
  - subject: "Re: Live cd anyone?"
    date: 2004-07-23
    body: "Yes a live CD would be very good. I wonder why don't we see that much unstable versions from more the well known KDE Based LiveCD distros out there."
    author: "John LiveCDs Freak"
  - subject: "Re: Live cd anyone?"
    date: 2004-07-23
    body: "Not a LiveCD but the newest Yoper Development Release has KDE 3.3 Beta 2 and is said to install under 5 minutes: http://www.distrowatch.com/?newsid=01781"
    author: "Anonymous"
  - subject: " What's new in KDE 3.3? A look at new features ..."
    date: 2004-07-22
    body: " I been working on a KDE 3.3 review for a few days now and I put what I've done so  far on the net today: http://linuxreviews.org/kde/kde_3.3beta1_and_beyond/ - What is new in KDE 3.3? A look at new features and aspects of using the current KDE cvs\n\n:-) have a nice day\n"
    author: "xiando"
  - subject: "Re:  What's new in KDE 3.3? A look at new features ..."
    date: 2004-07-22
    body: "Nice, please keep us informed when you have finished your review. Btw, is Kallery really part of KDE 3.3's kdewebdev module?"
    author: "Anonymous"
  - subject: "Re:  What's new in KDE 3.3? A look at new features ..."
    date: 2004-07-22
    body: "Unfortunately not. :-( I didn't had time to do it properly, so instead of just putting it into kdewebdev, I delayed the inclusion.\nAlso a note about Quanta's VPL: it was already present in 3.2, but it is more advanced in 3.3, thanks to the KHTML Caret Mode II.\n\nAndras"
    author: "Andras Mantia"
  - subject: "Re:  What's new in KDE 3.3? A look at new features"
    date: 2004-07-23
    body: "I swear I had kallery installed as a part of kdedev like two days ago! Now I don't :-) "
    author: "xiando"
  - subject: "Re:  Quanta's VPL"
    date: 2004-07-23
    body: "That looks really nice!\n\nReminds me of good old Word Perfect - it's great to see a similar feature added to Quanta!"
    author: "Joergen Ramskov"
  - subject: "Control Centre?"
    date: 2004-07-24
    body: "What did I read? They renamed Control Center to 'Centre'? \n\nI am just curious since I believed the default language of KDE is American English, not British. Can I get the correct American spelling back, please?\n\nRegards"
    author: "Sebastian"
  - subject: "Re: Control Centre?"
    date: 2004-07-25
    body: "> They renamed Control Center to 'Centre'? \n\nNo."
    author: "Anonymous"
  - subject: "GCC 3.4"
    date: 2004-07-22
    body: "Konstruct doesn't seem to work with gcc/g++-3.4 in debian/unstable... mcopidl  artsflow.idl fails with segfault...\n\nI'm retrying with gcc-3.3 right now..."
    author: "Renato Sousa"
  - subject: "Is KGet fixed?"
    date: 2004-07-23
    body: "I've liked the 3.3 previews I've seen thus far, but KGet crashes when saving the file list.  This means it'll crash when autosave is on or when you try to export it.  Has this been fixed in this beta?  I just want to know whether it's worth me upgrading, since this was my sole annoyance with beta1 and KDE takes a long time to compile."
    author: "Keith"
  - subject: "Re: Is KGet fixed?"
    date: 2004-07-23
    body: "Dunno, did you report the bug (bugs.kde.org)?  If you had, you could have tracked whether it was fixed or not.  (hint, hint)."
    author: "Rex Dieter"
  - subject: "Re: Is KGet fixed?"
    date: 2004-07-24
    body: "Eh, creating a new account is a hassle, and I'll just get \"Works for Me\" or \"Not a Bug\" responses from the developers anyway, so it's really not worth the precious time it drains from everybody, right?"
    author: "Keith"
  - subject: "Re: Is KGet fixed?"
    date: 2004-07-24
    body: "Can you imagine how much \"hassle\" it can be to fix a bug?"
    author: "Anonymous"
  - subject: "Re: Is KGet fixed?"
    date: 2004-07-26
    body: "Yes, actually I've fixed many bugs in the various open-source projects I've helped with.  Bugs are generally easy, because they're just minor errors in how you do things.  It's features that are the hassle."
    author: "Keith"
  - subject: "Re: Is KGet fixed?"
    date: 2004-07-24
    body: "its not :(\n\ni think of it as quite poor for a release coming with \"astonishing stability\"."
    author: "disappointed appointer"
  - subject: "Re: Is KGet fixed?"
    date: 2004-07-26
    body: "That sucks.  Do you know the bug number?  (KDE's bug database has one of the search facilities I've seen, so I haven't been able to find anything relevant)"
    author: "Keith"
  - subject: "Re: Is KGet fixed?"
    date: 2004-07-26
    body: "bug:81612 perhaps?\n(The bugzilla search query form might be complex, but limiting the result to product \"kget\" and summaries containing \"list\" reduced the results to 12 entries, among which \"kget crashes when saving transfer list\" sounds dead on.)"
    author: "Datschge"
  - subject: "Finally!"
    date: 2004-07-23
    body: "The konqueror progress icon has finally changed! I feel strange with that new colorfull gear :)"
    author: "Juanjo"
  - subject: "Re: Finally!"
    date: 2004-07-23
    body: "I feel strange with it too. Why doesn't it have the same colors as the gear on the panel?"
    author: "Anonymous"
  - subject: "Old bugs"
    date: 2004-07-23
    body: "Wondering if there is a chance to \"push\" old bugs to the front of the fixlist. There is a rendering bug in Konqueror present from KDE2 days (bug#: 32577) that seems like it has been forgotten - submitters every now and then confirm new versions that have this bug while there is absolutely no answer from the developers. Hope it's not due to a bug in Bugzilla ;-)"
    author: "canobi"
  - subject: "Re: Old bugs"
    date: 2004-07-23
    body: "> Wondering if there is a chance to \"push\" old bugs to the front of the fixlist.\n\nVote for them."
    author: "Anonymous"
  - subject: "Re: Old bugs"
    date: 2004-07-23
    body: "  That particular bug already has almost 60 votes (including my own vote). But who knows, maybe my ranting here motivates one of the developers to take a look at it ;-)"
    author: "canobi"
  - subject: "Out of subject..."
    date: 2004-07-23
    body: "What about www.mosfet.org?\nIt seems \"a bit\" out of date.\nThere'd been a more uptodate page. What happened? \nAlso mosfet? I think she disappeared."
    author: "light"
  - subject: "Re: Out of subject..."
    date: 2004-07-23
    body: "She? :-) Why care about him?"
    author: "Anonymous"
  - subject: "Re: Out of subject..."
    date: 2004-07-24
    body: "oh, sorry.\nI only glanced at mosfet.org, a woman siluette was there"
    author: "light"
  - subject: "Re: Out of subject..."
    date: 2004-07-24
    body: "http://www.kde.org/people/gallery.php has a better photo"
    author: "Anonymous"
  - subject: "Re: Out of subject..."
    date: 2004-07-23
    body: "Interesting. Mosfet disappeared for some time. \nI have not been in the lists for a while.\nIs he back?"
    author: "a.c."
  - subject: "Re: Out of subject..."
    date: 2004-07-25
    body: "he seems to be a she."
    author: "Mark Hannessen"
  - subject: "Re: Out of subject..."
    date: 2004-07-27
    body: "Dan Duley is a guy. \nHe was a very talented coder, \nbut his immaturity made it difficult to work with him.\n\nBut hey that is so uncommon in the geek world, eh?"
    author: "a.c."
  - subject: "Re: Out of subject..."
    date: 2004-07-23
    body: "Well its all clearly said in kde-look.org by thomas lubking (baghira author) that mosfet is and will not be returning. The site was actually put up by its new owner seeing the interest in mosfet."
    author: "sarath"
  - subject: "Nice!"
    date: 2004-07-24
    body: "I'm running it right now in a Slackware 10 testing installation I have alongside my main one. Seems pretty smooth, KWord seems nicer the last time I checked at it (long time ago), and Quanta WYSIWYG mode works even better than 3.2 stable version.\nThe only complaining in this 20 minutes are the Google combo (I'm happy with web shortcuts, it is just a useless widget taking space for me), I didn't find the way to get rid of it.\n\nBTW KHTML is rendering this very page too wide in default size, I'm not sure if it's a render problem or just the text area is too big.\n\nI'm not sure about upgrading now, as this is an aging computer, I should get some newer guts for it before doing it (too old to compile myself in reasonable time, too slow to keep upgrading to newer Linuxes with binaries available), but I'm very happy with Kollege anyway."
    author: "Shulai"
  - subject: "Re: Nice!"
    date: 2004-07-24
    body: "> I didn't find the way to get rid of it.\n\nSettings/Configure Toolbars..."
    author: "Anonymous"
  - subject: "Re: Nice!"
    date: 2004-07-24
    body: "It would be nice to make the 'Location:' area a dropdown that lists all available search shortcuts.. gg:, ggi:, sf:, etc..  the default could be url: or just location:.  it would provide the same basic functionality as the search toolbar but without taking extra space.  and it would help show what options are available (i didn't realize imdb was there, for  instance.)  when using firefox, i am always frustrated with having to use a separate text box for search terms.  "
    author: "p"
  - subject: "Re: Nice!"
    date: 2004-07-25
    body: "Good idea, please fill a wishlist item, comment on a relevant entry in the bugzilla db or personally knock down any first KDE Developer you see and threaten him to implement this feature asap.\nplzzzzzzzzzzzzzz"
    author: "that guiser"
  - subject: "Re: Nice!"
    date: 2004-07-25
    body: "Your idea simply rocks. Simple, elegant, easy to use and easy to find by newcomers. I'll vote for it in the bugs database."
    author: "Shulai"
  - subject: "Re: Nice!"
    date: 2004-07-27
    body: "Idea really rocks... that it the thing that I really missed, but my ideas how to solve that were far less elegant.\n\nI'll vote for this too if it appears in db."
    author: "tomislav"
  - subject: "New sounds ?!?!"
    date: 2004-07-24
    body: "Tried Beta2 here and was hoping to hear the new sounds ( http://dot.kde.org/1088839528/1088896340/ ), but I still heard the old ones.\n\nAnyone know when they will be replaced by the new ones?\n\nAnyway, trying out the 3.3 series for the first time, I was \"shocked\" to see how fast directories with many files and directories load. Many things have improved, but jeez, directories load fast now.."
    author: "ac"
  - subject: "Re: New sounds ?!?!"
    date: 2004-07-24
    body: "The new sounds are already in CVS HEAD now."
    author: "Anonymous"
---
The KDE Project is pleased to <a href="http://www.kde.org/announcements/announce-3.3beta2.php">announce</a> the immediate availability of KDE 3.3 Beta 2. As another step towards the <a href="http://conference2004.kde.org/">aKademy</a> in late August, this release is named Kollege. This beta release shows astonishing stability, so the KDE team asks everyone to try the version and give feedback through the <a href="http://bugs.kde.org/">bug tracking system</a>. For a list of new features skim over the <a href="http://developer.kde.org/development-versions/kde-3.3-features.html#inprogress">KDE 3.3 Feature Plan</a>. For packages, please visit the <a href="http://www.kde.org/info/3.3beta2.php">KDE 3.3 Beta 2 Info Page</a> and browse the <a href="http://www.kde.org/info/requirements/3.3.php">KDE 3.3 Requirements List</a>. The <a href="http://developer.kde.org/build/konstruct/">Konstruct</a> build toolset has been updated for this release. Please note that the kdepim, kdevelop and kdewebdev modules also compile on KDE 3.2 systems.



<!--break-->
