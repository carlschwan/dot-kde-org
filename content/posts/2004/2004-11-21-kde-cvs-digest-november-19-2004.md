---
title: "KDE CVS-Digest for November 19, 2004"
date:    2004-11-21
authors:
  - "dkite"
slug:    kde-cvs-digest-november-19-2004
comments:
  - subject: "Nice"
    date: 2004-11-20
    body: "Optimizations on a global level are always nice :) On the downside, nothing new for Kooka... I don't use the scanner that much, but I know people who do and keep wishing for improvements. If we compare it to how it was one year ago, there are barely any changes."
    author: "User"
  - subject: "Re: Nice"
    date: 2004-11-21
    body: "Indeed, optimizations are very nice.\n\nWindows is getting slower all the time while KDE is getting faster :)"
    author: "Mikhail Capone"
  - subject: "Re: Nice"
    date: 2004-11-21
    body: "Yeah, kooka must be a paradise for usability experts. Such a powerful program with such a pity UI... :-/\nBeyound UI-horror there are still some stange errors in interaction with sane no other scan program has.\n\nBye\n\n  Thorsten "
    author: "Thorsten Schnebeck"
  - subject: "Ruby Source Code Debugger"
    date: 2004-11-20
    body: "Hi Derek - thanks for reporting about the Ruby Source Code Debugger checkin. \n\nI'm really pleased with how it's turned out. With Qt Designer integration and now the debugger KDevelop is a very powerful ruby programming environment..\n\nFor more info, here is the full text of the commit: \n\n* Ruby Source Code Debugger initial checkin \n* Based on John Birch's gdb front end, the UI is very similar. There are no \nassembly language step options or disassembly viewers, but pretty much \neverything else is much the same in ruby as for gdb/C++. \n* You can set breakpoints on file/line or file/method, watchpoints to break when \na ruby expression is valid and catchpoints to break when an exception is thrown. \n* The Watch variable viewer allows ruby expressions to be watched, and they are \nupdated on program pause. \n* A configuration option was added to enable/disable the debugger floating toolbar. \n* The debugger backend is written in pure ruby using the code from the 'debug.rb', \nsource included with the ruby distribution. It is started via KProcess which handles \nredirection of stdout and stderr. The idea came from the way Laurent Julliard \u00a0\nimplemented the FreeRIDE ruby debugger - it uses Distributed Ruby (like DCOP) \u00a0\nfor messaging. DRuby is ruby-specific and couldn't be used though. \n* The debugger communicates with the backend via a Unix domain socket; \ncommands are sent, and the replies are parsed in KDevelop in much the same way \nas the gdb debugger uses stdin and stdout to send and receive. "
    author: "Richard Dale"
  - subject: "new Wish for Konqueror - useable image viewer"
    date: 2004-11-21
    body: "Hey, I guess it's better to put it nearer the top so that some people see this.\n \nA new wish that I found very interesting:\n \nhttp://bugs.kde.org/show_bug.cgi?id=93611\n \nBug 93611: Make Konq a useable image viewer - add \"next/previous\" function\n \nRight now if you want to look at pictures in Konqueror, you go to a directory in the file-browsing mode, you click on the file, the image loads, you go back, you click on the next file and so on...\n \nWhy not make it that - like in Kuickshow or ACDsee for instance - you can move on to the next and previous images?\n \nI'd suggest that when you load an image, you have extra buttons in the main toolbar so that you can go \"next/previous\" and that some keyboard keys are binded to that function.\n \nThis might even be a Junior Job and I think it would be very useful."
    author: "Wise"
  - subject: "Re: new Wish for Konqueror - useable image viewer"
    date: 2004-11-21
    body: "Use Gwenview, it's fast and beautiful"
    author: "Youssef"
  - subject: "Re: new Wish for Konqueror - useable image viewer"
    date: 2004-11-21
    body: "Gwenview already has a KPart even. I think Gwenview should be the image viewer in the next KDE. \n\nThough I like kview for viewing one image from the command line."
    author: "Ian"
  - subject: "Re: new Wish for Konqueror - useable image viewer"
    date: 2004-11-21
    body: "Unfortunately, you can't watch dark pictures in Gwenview, which degrades it to a viewer for normalized image collections. There's no equivalent for kuickshow's b/B (brightness) and c/C (contrast) shortcut. KIPI plugins are said to eventually bring that functionality, but having to go to a sub-sub-menu isn't really a solution. Kuickshow, on the other hand, has that sick dependency on imlib and doesn't display *.xcf, *.rgb etc., all of which are perfectly supported by the rest of KDE. Oh, well. There's still hope.  :-)"
    author: "Melchior FRANZ"
  - subject: "Re: new Wish for Konqueror - useable image viewer"
    date: 2004-11-22
    body: "KIPI plugins can have shortcuts assigned, and you can fix your dark pictures to have normal colors (what's the point of having dark pictures you cannot watch, anyway?).\nYou can help your hope by adding more votes to #82979.\n"
    author: "Lubos Lunak"
  - subject: "Re: new Wish for Konqueror - useable image viewer"
    date: 2004-11-25
    body: "> what's the point of having dark pictures you cannot watch, anyway?\n\nNow, that's easy to answer: 1) some picture may be right when viewed in your room, but too dark when viewed in the bright sun on your laptop in the park. To which environment do you suggest I should adapt all my pictures on the harddisk? 2) Some picture may be perfectly OK. But still, when viewing it, I may want to see details in the dark background. 3) I may not have the possibility to change a picture at all, because it's  on the internet, and too dark already there. Do you suggest that I save it, open it in gimp and edit it there? (The OP asked for a viewer in konqueror!) 4) If I want to show pictures to someone who is visually handicapped, I will most likely *not* change all the pictures on my harddisk. (Is accessibility just a buzzword in KDE, with nothing behind it?)\n\nWhat's the point in having an image viewer that only lets me view pictures with perfect contrast and brightness? With the same argument, viewers could refuse to let me zoom-in/out and to rotate. I just don't get why there's so much resistance, and so few viewers get it. kuickshow does, none of gwenview, pixie, showimg do."
    author: "Melchior FRANZ"
  - subject: "Re: new Wish for Konqueror - useable image viewer"
    date: 2004-11-25
    body: "1),4) If somebody can't see a picture because they can't see almost anything on the screen for some reason, I don't think it's a problem of the viewer. If you can't see things on the display because of bright sun, there's the contrast control.\n2),3) http://sourceforge.net/mailarchive/forum.php?thread_id=6012242&forum_id=1210\n"
    author: "Lubos Lunak"
  - subject: "Re: new Wish for Konqueror - useable image viewer"
    date: 2004-11-25
    body: "Wow, thanks! I'll apply that patch right away.\n\nBTW, here's another reason why this feature might be useful: 5) you are viewing images from a read-only medium, a cd-rom. And some of them are too dark for your monitor, maybe even only too dark for your taste. Of course, there are ways to get around this. But an option in the image viewer is the most obvious and effective place.\n\nI have a few directories with aircraft/cockpit photos. All of them seem to be of realistic brightness and contrast, just as I would see them in the real thing. But to be able to recognize some details in the darker areas I need to brighten up the picture, even to extreme values. But, of course, I don't want to store the images like that. kuickshow was very useful for that (and, weirdly, I still have to use it as an external tool from within gwenview). Gimp, in turn, wouldn't be acceptable for browsing these images. Later I added a few SGI and gimp images to the collection, and kuickshow couldn't handle them and popped up annoying messages. I even wrote the SGI kimgio and the XCF kimgio (adaptation from qxcfi) to cure that. Didn't impress kuickshow. So I was left with two half-working (for my requirements!) tools. Hey, didn't I write that there would still be hope? :-)"
    author: "Melchior FRANZ"
  - subject: "Re: new Wish for Konqueror - useable image viewer"
    date: 2004-11-21
    body: ">you have extra buttons in the main toolbar\n\nJust what we need, more buttons in the toolbar, yay!"
    author: "Anonymous"
  - subject: "it used to be good"
    date: 2004-11-21
    body: "A long time ago Konqueror was able to rotate/zoom pictures and such and had neat-o toolbar icons for it.\n\nUnfortunately this disappeared without a trace and never came back."
    author: "ac"
  - subject: "Re: it used to be good"
    date: 2004-11-21
    body: "all these functions simply depends on what viewer you use for the images. remember, konqueror is just an empty shell. you use kview for the pictures, and indeed, thats not really complex. try for example gwennview, it's already much better. and I'm sure there is more... you migt ask the developers of kuickview to add a kpart, I think kuickview whould do great."
    author: "superstoned"
  - subject: "Re: new Wish for Konqueror - useable image viewer"
    date: 2004-11-21
    body: "I solved this by using Kuickshow, I set it to open in fullscreen, modified shortcuts to Space bar to jump to next image and Backspace for previous, F for full screen toggle an Escape already quits Kuickshow. I am quite happy as I used to do the same with ACDsee. The only drawback in Kuickshow is that it still does not have loop function, like start from the first when I press \"Next\" on last image in dir."
    author: "amiroff"
  - subject: "Re: new Wish for Konqueror - useable image viewer"
    date: 2005-07-19
    body: "Why add new buttons to do the job?  I too would love Konqueror to be able to page through photos; why cannot the <- be back, the -> be forward, and the ^ (up) mean go back to the folder?"
    author: "lefty.crupps"
  - subject: "Kicker rewrite?"
    date: 2004-11-21
    body: "Anyone know why kicker needed a rewrite?  What's better about the new kicker?"
    author: "Leo S"
  - subject: "Re: Kicker rewrite?"
    date: 2004-11-21
    body: "It's less complex, which means less bugs, better maintainablity, and more features in the long run.\n\nCheers,\nVirgil"
    author: "Virgil Nisly"
  - subject: "Re: Kicker rewrite?"
    date: 2004-11-21
    body: "http://aseigo.blogspot.com/ for more information"
    author: "ciasa"
  - subject: "Re: Kicker rewrite?"
    date: 2004-11-21
    body: "And check out Aaron's bug list if you need more convincing:\nhttp://tinyurl.com/4vcrs"
    author: "Ian"
  - subject: "summary"
    date: 2004-11-21
    body: "derek, your summary/commentary is really great. always so nice to read through... thanx for the work!"
    author: "superstoned"
  - subject: "Re: summary"
    date: 2004-11-22
    body: "I have to agree... Derek, you're an invaluable resource for KDE!\nMy weekly cvs-digest reading is a must. Thank you!"
    author: "Anonymous"
  - subject: "Qt-Mozilla?"
    date: 2004-11-21
    body: "Any news about Qt-Mozilla?\nBeing celebrated so big when in the news, now we don't hear about it anymore :-(\n\nIs the cvs-write access for Lars Knoll still the only problem?\n\nIf anybody made a build of Firefox using Qt, could he/she make it available\nfor download?\n\nWill KDE 3.4 contain the Mozilla kpart?\nWill this kpart be able to use an installed Mozilla, or will KDE ship it's\nown libgecko.so?"
    author: "ac"
  - subject: "Re: Qt-Mozilla?"
    date: 2004-11-21
    body: "Patience... you must learn patience."
    author: "Yoda"
  - subject: "Re: Qt-Mozilla?"
    date: 2004-11-21
    body: "Bah, I'm still waiting for the khtml based firefox.\n"
    author: "Richard Moore"
  - subject: "Re: Qt-Mozilla?"
    date: 2004-11-23
    body: "So...\nif we have a khtml-based firefox using qt...\nit's basically konqueror without the file management, right? :-\\"
    author: "kundor"
  - subject: "Re: Qt-Mozilla?"
    date: 2006-08-23
    body: "with support for lots of extensions"
    author: "guest"
  - subject: "Great!!"
    date: 2004-11-21
    body: "\"Add multisynk plugin to kontact.\n\nNow we have a userfriendly interface for synchronization of two PCs, a PC and a\nhandheld or a PC and a bluetooth accessable mobile phone.\"\n\nSomething passed under silence but that deserves a really big THANKS!\n:)"
    author: "Davide Ferrari"
  - subject: "Re: Great!!"
    date: 2004-11-22
    body: "OTH we have two sync programs is kde-pim. multisynk and kpilot and I dont think, that you can sync your Palm against a multisync device.\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: Great!!"
    date: 2004-11-23
    body: "Well, let's wait for a probably future KPilot integration in multisynk! :)"
    author: "Davide Ferrari"
  - subject: "KDE Export optimization"
    date: 2004-11-22
    body: "Just wanted to point out that the actual KDE_EXPORT() patches that were done, didn't come from me afterall.\nBack in July when I proposed to send a patch, I didn't get any response to it, so I dropped working on it. Afterwards I noticed that Jaroslaw Staniek had been doing something similar, but his purpose was to get KDE to work on Windows (QKW), see\nhttp://article.gmane.org/gmane.comp.kde.devel.cygwin/1531\n\nSo while I did similar work, I can't take credit for the actual fixes to KDE. \n\nEither way, I'm happy the patches went in.\n\n"
    author: "Karl Vogel"
  - subject: "Re: KDE Export optimization"
    date: 2004-11-22
    body: "Apart of the slightly reduced size of the binaries, is there any other\nadvantage (e.g. startupspeed) ?"
    author: "ac"
  - subject: "Re: KDE Export optimization"
    date: 2004-11-22
    body: "Yes, startup speed."
    author: "Anonymous"
  - subject: "Re: KDE Export optimization"
    date: 2004-11-22
    body: "Yups.. it also enables gcc to produce better code. \nsee http://article.gmane.org/gmane.comp.kde.devel.optimize/786\n\n"
    author: "Karl Vogel"
---
In <a href="http://cvs-digest.org/index.php?issue=nov192004">this week's KDE CVS-Digest</a> (<a href="http://cvs-digest.org/index.php?newissue=nov192004">experimental layout</a>):
Kicker rewrite merged into HEAD for further testing.
New <a href="http://kontact.org/">Kontact</a> summary plugin for dates and holidays.
kttsd adds support for Festival 2.0 MultiSyn voices.
<a href="http://www.kdevelop.org/">KDevelop</a> has a new Ruby source code debugger.


<!--break-->
