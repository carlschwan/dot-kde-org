---
title: "Quickies: SuperKaramba Theme Archive, Unified Desktop, Skype, RULE Mini KDE"
date:    2004-06-29
authors:
  - "fmous"
slug:    quickies-superkaramba-theme-archive-unified-desktop-skype-rule-mini-kde
comments:
  - subject: "a few questions"
    date: 2004-06-29
    body: "Is superkaramba used for anything else than to have kicker replacement and systems/weather monitors?\nThe guys behind skype are the kazaa ones. So does that mean also spywares under linux?"
    author: "hmmm"
  - subject: "Re: a few questions"
    date: 2004-06-29
    body: "Short answers:\n\n1: SuperKaramba is used for drawing stuff directly on your desktop (hence why it has alpha and stuff)\n\n2: No"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: a few questions"
    date: 2004-06-29
    body: "Yea, I don't know why they would want to advertise their KaZaA affiliation so much. Spyware (or various sorts of malware at least) is the first thing that pops to mind when I read 'KaZaA.' And its not clear on their webpage how Skype is making money, though I might have missed it."
    author: "Ian Monroe"
  - subject: "Re: a few questions"
    date: 2004-06-30
    body: "Ther recently started a beta of \"SkypeOut\" ( regular phone calls through Skype ) so that could be the main source of income they're targeting."
    author: "Lukasz Lis"
  - subject: "Thank you"
    date: 2004-06-29
    body: "Thank you for this new Kwickie :-) and for \"using asterisks now\"..."
    author: "MM"
  - subject: "Re: Thank you"
    date: 2004-07-01
    body: "It would be better if you simply used paragraph breaks.  They've been established for thousands of years, and make things much more readable."
    author: "Jim"
  - subject: "Re: Thank you"
    date: 2004-07-01
    body: "Actually, no they haven't."
    author: "Roberto Alsina"
  - subject: "Re: Thank you"
    date: 2004-07-02
    body: "... but they do ;-)"
    author: "Spy Hunter"
  - subject: "Re: Thank you"
    date: 2004-07-03
    body: "Sorry, you are a little unclear.  Are you saying that paragraph breaks have not been around for thousands of years?  They have.  For instance, ancient Hebrew has gaps between stumos."
    author: "Jim"
  - subject: "Re: Thank you"
    date: 2004-07-27
    body: "Well but \"around\" != \"established\"... \n:-)"
    author: "George"
  - subject: "Will Apple's Dashboard renew interest..."
    date: 2004-06-29
    body: "in superkaramba?\n\nJust wondering what people's opinions are..."
    author: "red"
  - subject: "Skype in QT is significant"
    date: 2004-06-30
    body: "The fact that Skype chose QT seems significant to me.  So many other applications being made for multiple platforms (mozilla, realplayer, oO, Acrobat reader,...) seem to have chosen something else.  Qt seems to be very effective but in my experience it had not attracted many from outside the KDE clan.  I think this is great news."
    author: "simon"
  - subject: "Re: Skype in QT is significant"
    date: 2004-06-30
    body: "Adobe Photoshop Album:\nhttp://www.trolltech.com/newsroom/announcements/00000120.html\n\nNow I reckon that's only one application, but it's from a major software vendor !\n\nCheers,\nR."
    author: "rom1"
  - subject: "Re: Skype in QT is significant"
    date: 2004-06-30
    body: "and .. \n\n- Opera [www.opera.com]\n- MainActor 5 [www.mainconcept.com]\n- Julius [www.julius.caesar.de]\n- Jahshaka [www.jahshaka.com]\n- KD Executor \u00a0[www.klaralvdalens-datakonsult.se]\n- KNUT [www.klaralvdalens-datakonsult.se]\n- Klar\u00e4lvdalens Datakonsult AB's Emacs Additions\n- OpenFMG [http://www.openmfg.com]\n- Parity [www.parity-soft.de]\n- ATI Configuration Tool\n- Eagle by CadSoft\n\n\n... see Trolltech's Success Stories for more."
    author: "Fabrice Mous"
  - subject: "Re: Skype in QT is significant"
    date: 2004-06-30
    body: "I can imagine that many users of the commericial QT licence are not telling you that they use Qt...\n\nwhy? (i hear you asking)\n\nCauz in certain cases it can be considered as a business secret.\n\n\nThanks fab for the update!"
    author: "cies"
  - subject: "Re: Skype in QT is significant"
    date: 2004-07-03
    body: "Thanx cies \n\nHave fun :) \n\nFab"
    author: "AotM Team"
  - subject: "Thanks"
    date: 2004-07-01
    body: "thanks for this news update - that's the kind of news that makes me come back to the dot every day =)"
    author: "Chris"
  - subject: "Try gtk-qt!"
    date: 2004-07-01
    body: "gtk-qt theme engine is the best GTK+ theme enginge I have seen, ever - because it uses QT to draw it's widgets!\n\nSo you can choose your favorite KDE theme and all GTK/Gnome apps will look like them, too!\n\nIt is still beta but usable. give it a try:\n\nhttp://www.kde-look.org/content/show.php?content=9714\n\nor\n\nhttp://www.freedesktop.org/Software/gtk-qt"
    author: "Felix"
  - subject: "Re: Try gtk-qt!"
    date: 2004-07-01
    body: "It's beta?  When will it be part of KDE? KDE 3.3?"
    author: "hein"
  - subject: "Re: Try gtk-qt!"
    date: 2004-07-01
    body: "For me it is quite stable right now. I use it for daily work.\n\nI am not the author of gtk-qt so I don't know if it will be integrated into KDE. But I think it's hardly possible because it is a GTK/Gnome theme engine.\n\nI hope that the Makers of Linux distributions will include it in future versions."
    author: "Felix"
  - subject: "What else besides Skype"
    date: 2004-07-01
    body: "What else can we use to make free calls using linux?\nAlso, is there anything opensource?"
    author: "John Phone-Talk Freak"
  - subject: "Re: What else besides Skype"
    date: 2004-07-01
    body: "... Teamspeak, but it's not a P2P program like Skype more client/server based\n \nhttp://teamspeak.org/_about.php\n\nI think it's also based on QT"
    author: "Phil"
  - subject: "Re: What else besides Skype"
    date: 2004-07-04
    body: "gnomemeeting! it's around for some time. supports all kinds of protocolls and is not audio only, but supports video, too!\nhttp://http://www.gnomemeeting.org/"
    author: "Tobias Marx"
---
Did you create a beautiful-looking SuperKaramba theme? Well get your gem listed in the <a href="http://www.superkaramba.com/">SuperKaramba Theme Archive</a>. SuperKaramba can be downloaded from the <a href="http://netdragon.sourceforge.net/">usual website</a>. *** Unifying your desktop seems like a hot topic these days. At <a href="http://www.kde-look.org/">KDE-look.org</a> you can find <a href="http://www.kde-look.org/content/show.php?content=12859">an article</a> 
about how you can achieve that <a href="http://www.kde-look.org/content/preview.php?preview=1&id=12859&file1=12859-1.png&file2=12859-2.png&file3=12859-3.png&name=Unified+Plastik+desktop">cool Plastik look</a> on all of your applications. *** There is even a project called <a href="http://jezek2.advel.cz/projects/metatheme/">MetaTheme</a> which is dedicated to unification of appearance between different graphics toolkits, such as GTK and Qt. *** Well they said they would do it and they delivered. <a href="http://www.skype.com/">Skype</a> released 
their <a href="http://www.skype.com/download_linux.html">first beta version of Skype for Linux</a> based on the <a href="http://www.trolltech.com/products/qt/index.html">Qt toolkit</a> which makes <a href="http://developer.kde.org/~binner/mirror/www.xs4all.nl/~leintje/stuff/skype/snapshot4.png">this</a> <a href="http://developer.kde.org/~binner/mirror/www.xs4all.nl/~leintje/stuff/skype/snapshot10.png">baby</a> blend in
to your KDE desktop. *** I noticed this project called <a href="http://www.rule-project.org/">Rule</a> with the goal of making 
Free Software run on older hardware. The <a href="http://www.rule-project.org/article.php3?id_article=59">RULE Mini KDE page</a> shows how to do this with KDE and they even explain 
how you could <a href="http://www.rule-project.org/article.php3?id_article=58">configure a minimal Qt library</a> that still supports KDE 3.2. 




<!--break-->
