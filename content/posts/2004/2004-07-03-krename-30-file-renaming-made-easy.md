---
title: "KRename 3.0: File Renaming Made Easy"
date:    2004-07-03
authors:
  - "dseichter"
slug:    krename-30-file-renaming-made-easy
comments:
  - subject: "Squashing weird characters"
    date: 2004-07-03
    body: "Is this usable for squashing non-ascii characters in filenames to something like \"_\"? I'd have use for that, and many apparently simple shell scripts solutions seem to be unable to do it. This is a problem with samba, archives created in windows, etc.\n \n Also, are there going to be debian packages for this?"
    author: "Squasher"
  - subject: "Thanks!"
    date: 2004-07-03
    body: "I've been using Krename for a number of instances where I needed to fix spacing and numbering on files, and I have to say I'm very pleased with how your software has managed the job! And v3.0 looks good; love the ability to do remote renaming. \n \n Cheers!"
    author: "King Mob"
  - subject: "Why does the dot use a wrong date?"
    date: 2004-07-03
    body: "Why does the dot use a wrong date? Can't this be fixed pronto? Isn't there a KDE hacker with root on that darn hosting box?"
    author: "kde-friend"
  - subject: "Re: Why does the dot use a wrong date?"
    date: 2004-07-03
    body: "I have the impression that its clock somehow runs way too fast"
    author: "Waldo Bastian"
  - subject: "Re: Why does the dot use a wrong date?"
    date: 2004-07-03
    body: "\"...its clock somehow runs way too fast.\"\n ----\n Typical symptom for an artifact in the matrix. Beware of the agents now!\n ;-)"
    author: "kdefriend2"
  - subject: "Re: Why does the dot use a wrong date?"
    date: 2004-07-03
    body: "I'm fixing it slowly..."
    author: "Navindra Umanee"
  - subject: "Powerful app"
    date: 2004-07-03
    body: "This should really be integrated in KDE though\nbecause this is one of those apps you never bother\nto install until you need it. And when you need it\nit is often more work to install it than to do\nthe renaming by hand or on the command line.\nIt would be really cool if this could be integrated\nin KDE and be \"just there\"(TM) when you need it."
    author: "Martin"
  - subject: "wallpapers!"
    date: 2004-07-04
    body: "please include the nice girl wallpapers in the krename package ;)"
    author: "anon"
  - subject: "Re: wallpapers!"
    date: 2004-07-18
    body: "line"
    author: "Lin"
  - subject: "convmv"
    date: 2004-07-04
    body: "You should integrate s.th. like \"convmv\"\n\nconvmv can convert a single filename, a directory tree or all files\n on a filesystem to a different encoding. It only converts the\n encoding of filenames, not files contents. A special feature of\n convmv is that it also takes care of symlinks: the encoding of the\n symlink's target will be converted if the symlink itself is being\n converted.\n .\n It is also possible to convert directories to UTF-8 which are already\n partially UTF-8 encoded.\n (from \"apt-cache show convmv\")\n\nThis functionality together with krename would be perfect!\n\nGreetings"
    author: "HarryHirsch"
  - subject: "Re: convmv"
    date: 2006-03-05
    body: "changeFilenameCode service menu\nhttp://kubuntu.free.fr/servicemenu/\n\nconvmv Nautilus scripts\nhttp://www.ubuntuforums.org/showthread.php?t=139154&highlight=windows-1252"
    author: "Adam Kane"
  - subject: "Visual Rename ?"
    date: 2004-07-05
    body: "The current version looks impressive. The next step is to be able to build the new name in a visual fashion, by drag'n dropping the components of the path name. Then, it will be truly graphical."
    author: "Philippe Fremy"
  - subject: "Re: Visual Rename ?"
    date: 2004-07-05
    body: "Please can you email me some examples ? I am gathering right now the improvements for the next release. "
    author: "stonki"
---
After a long development phase a new stable release of <a href="http://www.krename.net/">KRename</a> is available for download. KRename is a powerful batch file renamer for KDE, which can be used to rename lots of files at once. Thus it is a great help in managing your digital images or your music collection. In 3.0.0 we focused not only on adding new features but also on increasing the overall usability of the application.

<!--break-->
<p>
Besides doing the usual stuff, like changing filenames to lower case or appending numbers to them, KRename uses <a href="http://www.konqueror.org/">Konqueror</a>'s file plugins to access meta data such as ID3 tags of MP3 or Ogg Vorbis files or EXIF tags of JPEG images from your digital camera. If you want to see KRename's capabilities you might want to check out our <a href="http://www.krename.net/Screenshots.11.0.html">screenshot section</a>.
</p>
<p>
The most important improvement in this release could only happen thanks to the <a href="http://usability.kde.org/">KDE Usability Project</a>. We got many good suggestions and tips for improvements from their mailing list. Those suggestions were added to KRename and made using it really easy compared to previous versions. A new beginners mode was added. Using this mode you can rename your files without having to know KRename's renaming syntax. The overall GUI was cleaned up. We added an automatic undo function. Using this undo, you can undo your renaming operation immediately in case something went wrong. Additionally many smaller improvements were made.
</p>
<p>
Finally there is also <a href="http://prdownloads.sourceforge.net/krename/krename-3.0.0.pdf?download">documentation</a> for KRename. The PDF documentation describes both user modes of KRename and contains lots of examples for different task you might encounter in your daily life.
</p>
<p>
And last but not least we improved KDE integration of KRename. KRename can now handle remote files thanks to KIO slaves. Renaming files on a remote server using fish:// or even ftp:// is no problem anymore! Controlling KRename from the command line is now possible, too, thanks to our <a href="http://developer.kde.org/documentation/library/kdeqt/dcop.html">DCOP</a> interface.
</p>
<p>
Besides the above mentioned changes we were able to fix lots of bugs, increase KRename's speed and add some more new features. You can find a complete change log on our webpage <a href="http://www.krename.net/">www.krename.net</a> as well as the <a hrefs="http://www.krename.net/7.0.html">downloads</a>.
</p>



