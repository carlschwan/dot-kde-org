---
title: "FOSDEM 2004: Interview with Gunnar Schmi Dt of KDE Accessibility Project"
date:    2004-02-17
authors:
  - "fmous"
slug:    fosdem-2004-interview-gunnar-schmi-dt-kde-accessibility-project
comments:
  - subject: "Assesibility is important"
    date: 2004-02-17
    body: "What can be done to assist colorblind people and products? As a huge amount of the population are colorblind web design has to take this problem into account but I found little information on what I have to take care of in order to assist colorblind people. Does anybody know a literature link or products that assist you?\n\nI am also intrested in methods that can be used to transform visual information (like a piechart) into texts. How can this be done via reporting tools?\n\n"
    author: "Holler"
  - subject: "Re: Assesibility is important"
    date: 2004-02-18
    body: "The most important thing is to not consider all color blindness the inability to see color (or one color.   \n\nNext, avoid messing with color where you can avoid it.   You don't need to set my foreground and background colors if you avoid background images (which are really annoying even when the colors used are ones I see normally).   Let the defaults (that I might or might not have changed) decide what colors you use.\n\nMake your page viewable and useful in lynx.   I now use Konqueror, but you can be sure if a blind person (who can only use lynx) can use your page, than the colorblind can too.  Unfortunately the colorblind can make use of pictures, while the blind can't[1].   So if pictures/graphics are a part of your site, the only real solution is black and white, if you can make that works (with shades if needed) you won't have a problem.  Otherwise there are so many different color blindnesses that nothing is 100%.   Color is useful, so don't be afraid to use it, just know there is no way to tell.\n\n[1] Actually 95-99% of all blind people can see the sun, their vision is just so poor that they cannot function on sight from day to day.  Some might be able to make out your pictures if magnified and worth going through extra effort."
    author: "Hank"
  - subject: "Re: Assesibility is important"
    date: 2004-02-19
    body: "It is not directly what you have asked but you can look at the Web Accesibility Guidelines:\nhttp://www.w3.org/WAI/Resources\n\n(Of course the document is Web-oriented, but many things are about the look on screen anyway.)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Assesibility is important"
    date: 2004-02-23
    body: "I can post at last :)\n\nOK, a good link for colourblindness is:\n\n  http://www.visibone.com/colorblind/\n\nIt would be good if there was a way to simulate colour blindness in khtml with some kind of palette switching/mapping.\n\nTo be able to click on a button to switch rendered colours from a \"normal\" palette to a simulated deuteranopic (protanopic, ...)  palette would certainly help developers make sensible colour choices.\n\nMind you, that functionality is probably already there somewhere - can anyone point me in the right direction?"
    author: "Hmmm"
  - subject: "suggestion"
    date: 2004-02-17
    body: "I highly suggest zeldmans new book for those that are interested in this topic (eric myers new book also). They discuss using webstandards.\n\nIf you are using the gd lib* with php then you can display information in image form from raw data. This can be display as alt text for screen readers or text only browsers.\n\nI work with persons with developmental disabilities..."
    author: "macewan"
---
We're pleased to announce that the <a href="http://www.fosdem.org/">Fosdem website</a> has published 
<A href="http://www.fosdem.org/index/interviews/interviews_schmidt">a nice interview with Gunnar Schmi Dt</A> 
of the <A href="http://accessibility.kde.org/">KDE Accessibility Project</A>. 
Fosdem is a two-day summit taking place February 21 and 22, and will bring together leading developers 
in the Free Software community. Contrary to a Linux Expo, Fosdem is an event held by developers, for developers. 
Besides Gunnar, <A href="http://kde.ground.cz/tiki-index.php?page=Fosdem2004">several other KDE hackers</A> 
will be present at Fosdem. Feel free to visit them at the 
<A href="http://www.fosdem.org/2004/index/dev_room_kde">KDE developers' room</A>. 



<!--break-->
