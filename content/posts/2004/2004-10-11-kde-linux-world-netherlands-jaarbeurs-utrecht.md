---
title: "KDE at Linux World Netherlands (Jaarbeurs Utrecht)"
date:    2004-10-11
authors:
  - "fmous"
slug:    kde-linux-world-netherlands-jaarbeurs-utrecht
comments:
  - subject: "Strike"
    date: 2004-10-11
    body: "\"(...) can be easily reached by train (..)\"\n\nNot on thursday, there will be a national public transportation strike."
    author: "Aaargh!"
  - subject: "Re: Strike"
    date: 2004-10-11
    body: "yep, those bastards... most ppl in the netherlands hate it, but they (the labour unions) are pushing the strike... well, it wont help them. I hope the government doesnt listen to this, most ppl still backup their plans anyway. The company unions (or whatever that is in enlish) already said they did more for the working force than the labour unions, and I think they are right..."
    author: "superstoned"
  - subject: "Re: Strike"
    date: 2004-10-11
    body: "\"Company unions\" -- do you mean employers' associations? \n\nOr do you have company-sponsored \"worker's clubs\" similar to the \"unions\" that existed in some communist countries?\n\nIn either case, I think you are naive at best to trust those organisations to care for labour interests."
    author: "Martin"
  - subject: "Re: Strike"
    date: 2004-10-12
    body: "Hmm, I wonder which country you come from :)"
    author: "Rinse"
  - subject: "Re: Strike"
    date: 2004-10-12
    body: "I meant employers associations. and indeed I dont trust them to do what the workers want, but actually they did... although maybe with other intentions.\n\naaah, this bullshit can make me angry (not what you said) - the unions want everyone to be able to quit working at 50. yes. at what cost? ah, just increase the nations' debt. the next generation can pay. well, I AM the next generation (at least, one of them) and when they are old, if I whould have it my way, NO pensions, NO money for them. they made us pay because they wanted a bigger car, high unemployment benefits and everyone a second tv and dvdplayer. wel, they had it. and as soon as 'we' are in charge, and have to pay the huge debt they left us, well, we can stop paying them. and use that money for paying THEIR debt. dam, the netherland already pay more than 20 billion euro's each year ONLY for INTEREST. thats the third post on our anual expence. I hate those left/communist party's.\n\nWhen I am going for a job, I'll be paying my tax mostly for this interest they left me. lazy basterds."
    author: "superstoned"
  - subject: "Re: Strike"
    date: 2004-10-12
    body: "\"aaah, this bullshit can make me angry (not what you said) - the unions want everyone to be able to quit working at 50. yes. at what cost?\"\n\nHmm, that is not really the case ;)\n\nBut I don't think the dot is a good place to discuss Dutch politics :)\n\nBesides, the strikes are against the policy of the gouvernment, not against the employees....\n\nRinse"
    author: "Rinse"
  - subject: "Re: Strike"
    date: 2004-10-12
    body: "\"most ppl still backup their plans anyway\"\n\nNo they don't, the government is completely insane, they took away 'early' retirement, which has the effect of enlarging the work force. But we don't *need* a bigger work force, we don't even have enough jobs for the current amount, how the hell do they think this is going to help. All they're going to do is pay less pensions, pay more unemployment and piss off half the country in the process.\n\nFirst create more jobs, then if we have a labour shortage they can take away 'early' retirement."
    author: "Aaargh!"
  - subject: "Timely..."
    date: 2004-10-11
    body: "Fabrice;  next time please announce like a month before..  I really can't change my schedule 2 days before an event :(\n\nVeel plezier daar!"
    author: "Thomas Zander"
  - subject: "Re: Timely..."
    date: 2004-10-11
    body: "Thomas, \n\nWe had a confirmation of our attendance just one week ago. So all was not that sure. But I need to think of a way to inform all KDE people in the Netherlands (maybe a mailing). Until then please subscribe to the <a href=\"https://mail.kde.org/mailman/listinfo/kde-i18n-nl\">Dutch KDE mailinglist</a> and/or visit us at #kde-nl (very lively place). \n\nCiao' \n\n\nFab\n"
    author: "Fabrice Mous"
---
This week <a href="http://www.linuxworldexpo.nl/">Linux World Expo</a> comes to the Netherlands where <a href="http://www.kde.nl/">KDE-NL</a> will showcase the enterprise features of the KDE desktop. The event wil take place at <a href="http://www.jaarbeursutrecht.com/">Jaarbeurs Utrecht</a> and can be <a href="http://www.jaarbeursutrecht.com/Address and route">easily reached by train and car</a>. Our laptops with KDE are kindly sponsored by IT provider <a href="http://www.kovoks.nl/">KovoKs</a>. Jeroen Baten will give a talk about KDE, the coporate desktop. Entrance to this event is free when you <a href="http://www.databadge.net/linuxworld2004/reg/">register in advance</a>. Please come and visit us at booth 8F095. We hope to see you there!
<!--break-->
