---
title: "GTK+ Apps Get Free Reign on KDE Technology"
date:    2004-01-10
authors:
  - "zrusin"
slug:    gtk-apps-get-free-reign-kde-technology
comments:
  - subject: "Interesting, but not all brilliant"
    date: 2004-01-09
    body: "This, at first, seems excellent. In theory we could now see lots of GNOME applications, particularly those for which there is no suitable KDE equivalent, integrating nicely with KDE's most important technologies, namely the kio_slaves, DCOP etc.\n\nBut one thing worries me: maintenence. Will Beep Media Player need to be patched in every release to make this work, and will someone step up to do this, to provide a KDE version? It seems that if a development team chooses GTK & GNOME, they do it because they want to use those toolkits, and so it will often require outsiders to maintain KDE versions, which could only make things far messier.\n\nUnless, of course, in the future this thing can become standardised so that you select in KControl or some similar place to use KDE widgets in place of Gtk/GNOME ones, and it automatically applies to all applications, without needing any recompiling. But that seems unlikely."
    author: "Tom"
  - subject: "Re: Interesting, but not all brilliant"
    date: 2004-01-09
    body: "What this makes possible is having applications use the services of the desktop under which it runs. A user is confronted with one set of standard dialogs, file open, print, etc., no matter what applications they run.\n\nThe applications could check what desktop is running, and display the appropriate dialogs.\n\nYes it would be more maintenance for the developers, but a huge step forward for the users. Imagine all applications using the same print dialog, no matter what framework they are based on. It's not a matter of which print dialog is better.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Interesting, but not all brilliant"
    date: 2004-01-09
    body: "What I think they need is an intermediate library that both KDE and GTK/Gnome apps use for things like file open's, etc. that chooses the correct dialogs depending on the environment you are in."
    author: "Tormak"
  - subject: "Re: Interesting, but not all brilliant"
    date: 2004-01-09
    body: "You worrie about maintainance? I guess quite some distro's will hapily apply/maintain/contrib patches to GTK apps that dont have a Qt/KDE equevalent.\n\nI'm even looking forward to distro's that will go so far in 'polishing' their desktop feel.\n\nI don't use and will not use GNOME, I have enough speed and i like my desktop to look good. I think this effort for building bridges between Qt/KDE and other toolkits (GTK, OOo) is special, if not unique.\n\nHopefully GTK/GNOMEers will also put efford to 'naturalize' Qt/KDE apps, this way their desktop experience will also grow.\n\nCies.\n"
    author: "cies"
  - subject: "Assumptions"
    date: 2004-01-10
    body: "You assume that Beep wouldn't accept a patch to have an extra KDE compile time option. As the tutorial shows, it could be done without adding much source code bulk and no binary bulk to those who don't want it. It wouldn't be that much maintain. There wouldn't need to be a fork or anything. Thank you C macros.\n\nGentoo users could decide which way they want it, other distros would decide for their users based on what their primary desktop is."
    author: "Ian Monroe"
  - subject: "Cool"
    date: 2004-01-09
    body: "Cool!\n\nNow if only I could automagically use this for all binary packages of all GTK apps installed on my system... But still, it's great!\n\n"
    author: "Anonymous"
  - subject: "Just 3 words..."
    date: 2004-01-09
    body: "OMG  ;o)"
    author: "Francisco"
  - subject: "Tutorial is a partly unreadable..."
    date: 2004-01-09
    body: "...due to large black background around the globe near the top. I am using IE6 here, I don't have konqy available here at work ATM to check."
    author: "Nadeem Hasan"
  - subject: "Re: Tutorial is a partly unreadable..."
    date: 2004-01-09
    body: "Time to switch to a good browser? :-) http://mozilla.org"
    author: "Anonymous"
  - subject: "You just rock..."
    date: 2004-01-09
    body: "Hi,\n\nthis so great. I followed with interest how in context of Sodipodi that was attempted first, and this simple and general solution, great.\n\nNow if somebody commited the patch for GIMP, will they accept it?\n\nAnd using this new GTK QT theme, would it possible for KDE to offer a button like \"start GTK apps with KDE look (not feel)\", then it would already be complete.\n\nYours, Kay\n\n"
    author: "Debian User"
  - subject: "Mozilla and other Qt apps"
    date: 2004-01-09
    body: "This can also be possible with all the Mozilla familly apps and other Qt apps such as Scribus or Opera and even OpenOffice.org?\n\nThis is an excellent notice for KDE users!"
    author: "Josep"
  - subject: "Re: Mozilla and other Qt apps"
    date: 2004-01-13
    body: "Neither mozilla nor OpenOffice are QT apps - so no.\n"
    author: "JohnFlux"
  - subject: "Re: Mozilla and other Qt apps"
    date: 2004-03-12
    body: "> Neither mozilla nor OpenOffice are QT apps - so no.\n\nWell neither is beep... From the author's own post:\n\n>> From now on, every GTK+ application can easily integrate with KDE.\n\nSince Mozilla is a GTK application it stands to reason that it too could be made to integrate with KDE. And even if not Mozilla itself, then perhaps something can be done to at least integrate Gecko into Konqueror as an additional View Mode."
    author: "David P James"
  - subject: "Re: Mozilla and other Qt apps"
    date: 2004-03-12
    body: "Gecko inside Konqueror ? You should check kdebindings-kmozilla"
    author: "Ariya Hidayat"
  - subject: "Is this C++ only?"
    date: 2004-01-09
    body: "Sorry, I am not the best programmer, but the sample code seems like the FileDialogs still are C++ classes.\n\nSo how I can I use this in my gtk+ app, when I don't like/want to use C++ but C only?"
    author: "MaxAuthority"
  - subject: "Re: Is this C++ only?"
    date: 2004-01-09
    body: "Use the KDE C bindings?\n"
    author: "Rayiner Hashem"
  - subject: "Re: Is this C++ only?"
    date: 2004-01-10
    body: "Then I don't think this new development will help you any."
    author: "Ian Monroe"
  - subject: "beep and qtgtk"
    date: 2004-01-09
    body: "First, where can I get the patch for beep media player to use this?\n\nSecond, I see a qtgtk-0.2 at http://ktown.kde.org/~danimo/\n\nand you linked a qtgtk-0.1.0, which is newer?"
    author: "David Walser"
  - subject: "Re: beep and qtgtk"
    date: 2004-01-09
    body: "It's in fact older, I removed it. Sorry for the fuzz.\n\nCheers,\n  Daniel"
    author: "Daniel Molkentin"
  - subject: "Re: beep and qtgtk"
    date: 2004-01-10
    body: "Ok, thanks.\n\nSo, where's the patch to beep media player?"
    author: "David Walser"
  - subject: "Possibilities..."
    date: 2004-01-09
    body: "Will KDE eventually be equally usable and integratable from either Qt or GTK+? Will this render the entire Qt v. GTK debate as it pertains to KDE v. GNOME utterly moot?\n\n"
    author: "Cowardly Anonymity"
  - subject: "LGPL license effects"
    date: 2004-01-09
    body: "By using KDE dialogs, does it mean that a commercial app developed with Gtk under the LGPL license have to be under the GPL license or otherwise have to buy a license from Trolltech?  "
    author: "John"
  - subject: "Re: LGPL license effects"
    date: 2004-01-10
    body: "From my understanding point, unless you buy a Qt license, whatever app that links against Qt must either be GPL or QPL compliant. GPL implies that your app must be GPL too. QPL... well  http://www.trolltech.com/licenses/qpl.html =)"
    author: "uga"
  - subject: "Re: LGPL license effects"
    date: 2004-01-10
    body: "Please stop spreading lies.  GPL does not imply your app must be GPL.  Period."
    author: "ac"
  - subject: "Re: LGPL license effects"
    date: 2004-01-10
    body: "Ok. So I'm lying. What licenses are allowed to be linked against GPL?\nI know they must be opensourced. What else? Is LGPL allowed? I guess yes, but whatever that uses the first original lib must comply with GPL too, so you're submitted to the GPL license anyway. \n\nNow tell me, what's the difference? "
    author: "uga"
  - subject: "Re: LGPL license effects"
    date: 2004-01-10
    body: "The GPL FAQ may help answer your questions, and other common questions about the GPL:\n\nhttp://www.gnu.org/licenses/gpl-faq.html#WhatDoesCompatMean\n* What does it mean to say that two licenses are \"compatible\"?\n* What does it mean to say a license is \"compatible with the GPL\"."
    author: "Don Sanders"
  - subject: "Re: LGPL license effects"
    date: 2004-01-15
    body: "Great, thanks! It's one of those things one always hears in discussions but nobody makes clear. Lets hope somebody will give a definite answer to the definition of \"derivative\" too...."
    author: "uga"
  - subject: "Re: LGPL license effects"
    date: 2004-01-10
    body: "I think a smart solution can be like this:\n1) write a small lib - set abstract classes that are LGPL\n2) use in your app GTK+ and small lib only and you are still LGPL compilant so your app can be closed source commercial\n3) release a GPL licensed implementation (of your small lib) of the classes that links against Qt\n3*) and of course still have the \"safe\" plugin is GTK implementation of your small lib\n\nthe only problem then will be with the distribution since from my understanding you cannot distribute closed source and GPL packaged together.. but however if such plugin is widely adopted (in future) and all distros distrubute such plugins you should not care... more than that users will demand such thing since they want everything integrated"
    author: "Anton Velev"
  - subject: "Re: LGPL license effects"
    date: 2004-01-10
    body: "Please stop spreading these lies about the GPL license.  YOUR APP OR LIB DOES NOT HAVE TO BE GPL LICENSED TO LINK AGAINST KDE.\n\nSo why would you have to release a separate GPL licensed implementation?  Please, you have been spouting off on this issue FOREVER.  It is time you EDUCATE YOURSELF on these issues.\n\nHere is the solution for people who can think:\n\n1) write a small lib - set abstract classes that are LGPL that load either GTK+ or Qt at runtime.\n\nThat's it.  That's all.  No more license bullsh*t, it works and it's legal.  Your proprietary closed source app can then use KDE dialogs for free.  Simple."
    author: "ac"
  - subject: "Re: LGPL license effects"
    date: 2004-01-10
    body: "But dosn't linking to KDE automatically link the application to Qt? If a qt link is there, then you need their commercial license to write closed-source applications right?\n"
    author: "Sarang"
  - subject: "Re: LGPL license effects"
    date: 2004-01-10
    body: "In this scheme you wouldn't be linking with KDE. You'd dynamically link your application with a small library with stubs for file dialogs and stuff. This library would be LGPL or something, and would implement the file dialogs etc using GTK. No license issues there, obviously. Just a little indirection.\n\nNow, if someone wanted the programs on their computer to use Qt dialogs instead of GTK ones, they'd write a new dynamic library which implemented the same interface but used Qt as the backend. They'd then overwrite the first dynamic library with the new one, and boom, programs would magically have Qt dialogs.\n\nObviously the second library would have to be GPL. But this isn't a problem--the GPL specifically allows you to do whatever the hell you want with GPL code, including linking with libraries having an incompatible license, as long as you don't distribute the result. Which you wouldn't need to do.\n\nThat's merely one way of many perfectly legal and acceptible ways to accomplish this; it probably works a little differently in real life.\n\nAnd that doesn't even consider the approaches where distributing the source code is perfectly legal, but distributing a binary is not."
    author: "Evan"
  - subject: "Re: LGPL license effects"
    date: 2004-01-10
    body: "Man, thank you very much!\n\nEven that I was in favor of KDE being GPL even if you could not link agains it, your explanation is very welcome and I even think it should be printed in the main page or at least on some FAQs well visible, because there is a lot of FUD because of this GPL/KDE thing.\nThanks again."
    author: "Iuri Fiedoruk"
  - subject: "Re: LGPL license effects"
    date: 2004-01-10
    body: "> there is a lot of FUD because of this GPL/KDE thing\n\nYou are right. The FUD is spread by top level representatives of KDE opponents. It then trickles down to the rank and file of the community at large, because some friends are repeating it.\n\nOne effective way to counter FUD, is if more people at the grassroots level know the facts. If they speak up in every forum: as soon as they notice another grassroot friend to thoughtlessly repeat bits of it, they should calmly, friendly and backed up with facts and links counter any FUD with facts, figures and arguments."
    author: "Kurt Pfeifle"
  - subject: "Re: LGPL license effects"
    date: 2004-01-10
    body: "Very well said.\nI used to use the kompany as a example that comercial software can be done using KDE libraries, but some time ago they started using more QT-only so I stopped using it. But knowing that you can write a little \"wrapper\" library to make possible writing non-GPL software for KDE (that is more or less that the kompany did) in such a well explained way is a great news that should be spread the most we can.\n\n(I'm writing a article for a brazilian site about this just now)"
    author: "Iuri Fiedoruk"
  - subject: "Re: LGPL license effects"
    date: 2004-01-10
    body: "AC, i think you should educate yourself by reading some news on the itnernet and the licenses.\n\nYou know very well that whatever that is deriviated from GPL licensed soft must be GPL (except for Kernel and some other system libs).. anyway .. i think you can make your conlusion now.\nThe question that John asked is for a commercial (i guess also closed source) app that is GTK written, and he wants to use KDE file dialog without messing with GPL.\nOf course (only if this QtGTK thing is not GPLd already) one may pay a some money to buy a commercial Qt license.\n\nI understand the question that John asked (comm GTK app + KDE filedialog) and hope my suggestion will be useful for his solution.\n\n\nPS: btw why don't you stay strong after your name, but trying to show the world you are MEN in the same time with unknown identity???"
    author: "Anton Velev"
  - subject: "Re: LGPL license effects"
    date: 2004-01-11
    body: "To things:\n1. What commercial applications use GTK? It just doesnt make sense to use an inferior but free toolkit when you are already paying for development.\n2. The KDE dialogs could by dynamically loaded, that way the application is not derivative, only the library, and the library could be LGPL."
    author: "Allan Sandfeld"
  - subject: "Re: LGPL license effects"
    date: 2004-01-15
    body: "Did you read the definition given by Linus for \"derivative\"? There was quite a bit of discussion. That part of the license may indeed be the most tricky point. Lets hope GPL v3 will make these points clear :)"
    author: "uga"
  - subject: "Re: LGPL license effects"
    date: 2004-01-10
    body: "> the only problem then will be with the distribution since from my understanding you cannot distribute closed source and GPL packaged together.\n\nNo problem. The application vendor can distribute with the GTK implementation but the Linux distributor can distribute the Qt implementation."
    author: "Kevin Krammer"
  - subject: "Re: LGPL license effects"
    date: 2004-01-10
    body: "exactly"
    author: "Anton Velev"
  - subject: "nice work"
    date: 2004-01-09
    body: "dude, i like this. anyone knows why this happened that late? thanks anyways.\n\n-- joe"
    author: "jose"
  - subject: "gnome.org"
    date: 2004-01-09
    body: "You should post this info on the gnome.org news section ;)"
    author: "JC"
  - subject: "Re: gnome.org"
    date: 2004-01-10
    body: "As soon as this technology is *stable* I think this should really be done!\nIt's the most easy and natural for the original author/maintainer of a software to implement and maintain this option.  \n\nBut to do that the authors must know about it! \n\n"
    author: "cm"
  - subject: "Re: gnome.org"
    date: 2004-01-10
    body: "Qouting from http://www.gnome.org/bounties/\n\n> Welcome to the first-ever open source desktop bounty hunt! \n>\n> The goal of this contest is to help improve the level of integration between\n> some of the core components of the Linux desktop. \n>\n>\n> Our specific aim is to improve the experience of collaboration in the\n> desktop environment. We believe that communicating and working with other\n> people is not simply a function of a single application that sits in a\n> rectangular window on your screen ? Evolution or Outlook, for example ? but\n> one of the primary functions of a computer. Therefore, collaboration should\n> be a first-class element of the user experience. In other words, it should\n> be really easy for a GNOME user to talk to, share with, and work with their\n> friends. \n\nthis is a good candidate IMHO...\n\nthanks!"
    author: "bangert"
  - subject: "4 words!"
    date: 2004-01-10
    body: "Thank you very much!"
    author: "Iuri Fiedoruk"
  - subject: "So this File Open"
    date: 2004-01-10
    body: "So this File Open... Is it simpler to use than its GTK+ 2 equivalent? That's really all that matters to me. I mean, is it so impossible to create a function that just gets a filename and doesn't require you to set up anything else?\n\nfoo_api_file_open(target_string,current_directory_string);"
    author: "David Oftedal"
  - subject: "Re: So this File Open"
    date: 2004-01-11
    body: "Yes it really is that simple. Did you read the tutorial before posting?\n"
    author: "no one in particular"
  - subject: "Re: So this File Open"
    date: 2004-01-11
    body: "Yeah, I read the tutorial, but it seemed to be more about the library and not the file open? OK, never mind then."
    author: "David Oftedal"
  - subject: "XMMS Skin?"
    date: 2004-01-11
    body: "Which xmms skin is used in those screenshots?"
    author: "David"
  - subject: "Amazing but"
    date: 2004-08-19
    body: "Are these pics real? \nI am on a quest to find a single patch for QtGtk for popular GTK apps and have yet to find any. I began patching Gimp2 myself but have run into so many problems, I'm not sure I'll continue to try, as I have other priorities.\n\nThis is great, but where are the working patches?"
    author: "Jose Hernandez"
---
Integration of GTK+ applications in KDE has taken another leap forward.   This has historically been a bit of a problem; the fact that Qt and GTK+ rely on different event loops was making it impossible to, for example, use dialogs from one toolkit while building the GUI in another. <i>QtGTK</i> is a library which integrates the Qt event loop in the Glib event loop. This makes it possible to freely use KDE dialogs, DCOP, KDE IO and other KDE technology in any GTK+ application just like they would be native.  From now on, <i>every</i> GTK+ application can easily integrate with KDE.  
<!--break-->
<p>
A <a href="http://developer.kde.org/documentation/tutorials/qtgtk/main.html">tutorial explaining how to use this library</a> is available from <a href="http://developer.kde.org/">developer.kde.org</a>.  Integrating existing applications is trivial, taking <i>one</i> function call. Just look at what Daniel did with the Beep Media Player (formerly XMMS2):
<ol>
<li><a href="http://ktown.kde.org/~danimo/bmp_kde_diropen_dlg.png">KDE Directory Open</a></li>
<li><a href="http://ktown.kde.org/~danimo/bmp_kde_fileopen_dlg.png">KDE File Open</a></li>
</ol>
The library itself is available in both <a href="ftp://ftp.kde.org/pub/kde/unstable/apps/KDE3.x/utils/qtgtk-0.1.0.tar.gz">tar.gz</a> and <a href="ftp://ftp.kde.org/pub/kde/unstable/apps/KDE3.x/utils/qtgtk-0.1.0.tar.bz2">tar.bz2</a> format.
 <p>
This is only the beginning!