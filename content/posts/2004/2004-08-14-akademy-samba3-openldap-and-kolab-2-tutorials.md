---
title: "aKademy: Samba3, OpenLDAP and Kolab 2 Tutorials"
date:    2004-08-14
authors:
  - "mkonold"
slug:    akademy-samba3-openldap-and-kolab-2-tutorials
comments:
  - subject: "Kolab 2"
    date: 2004-08-14
    body: "What's the status of Kolab 2? Is its development finished? Is it usable? The Kolab homepage doesn't have more information than some plans from April."
    author: "Anonymous"
  - subject: "KOLAB2 / OpenLDAP tutorials"
    date: 2004-08-14
    body: "I see from the mailing lists that there is a lot of development activity going on. Though, there is only very little information out for admins and non-developers. I would like to hear more about kolab2. What about writing a HOWTO? I mean, I don't object to buying a book or any other printed stuff. But, 500 Euros / day is definitely too much for me! I would have appreciated anything affordable for the ordinary admin or private user etc. Such a high price is only suited to keep the public out. \n\nMarkus"
    author: "Markus Heller"
  - subject: "Re: KOLAB2 / OpenLDAP tutorials"
    date: 2004-08-14
    body: "The tutorials fees are to partly to finance the whole event, so please see a part of it as donation to the KDE project."
    author: "Anonymous"
  - subject: "Re: KOLAB2 / OpenLDAP tutorials (free?)"
    date: 2004-08-14
    body: "kde is based on free work not money, as is the whole opensource movement."
    author: "chris"
  - subject: "Re: KOLAB2 / OpenLDAP tutorials (free?)"
    date: 2004-08-14
    body: "Unfortunately the reality doesn't offer free event locations, hardware, food and travel opportunities for opensource developers."
    author: "Anonymous"
  - subject: "Re: KOLAB2 / OpenLDAP tutorials (free?)"
    date: 2004-08-16
    body: "Nonsense.\n\nI don't know where you get this idea. Free software is not about free riding. It is about doing your part and sharing code. I look forward to the day when all of KDE developers, many already do, get paid for hacking on KDE.\n\nPutting together a conference is a lot of work. If you need documentation, either write it yourself or be patient until we get around to it. I would like to know what you do for a living, since you expect everything to be free. \n\nI am glad that KDE is indeed free, but understand that conferences are expensive and that people need to put food on the table.\n\nThanks for listening."
    author: "Gonzalo"
  - subject: "Re: KOLAB2 / OpenLDAP tutorials (free?)"
    date: 2004-08-16
    body: "Well said/written."
    author: "Don"
  - subject: "Kolab 2"
    date: 2004-08-14
    body: "I currently have a postfix/cyrus imap/openldap combo deployed in my netwerk. does kolab have a advantage over traditional postfix/cyrus/openldap other then being easy to administrate?"
    author: "Mark Hannessen"
  - subject: "Re: Kolab 2"
    date: 2004-08-16
    body: "Yes, it has many more features incl. groupware feature like a shared\ncalendar, distribution lists, sender verification, virus detection,\nspamassassin, Access control lists, a webgui, web based freebusy overviews...."
    author: "Martin Konold"
  - subject: "Re: Kolab 2"
    date: 2004-08-19
    body: "Is Kolab a replacement for Postfix?  Same functionality?"
    author: "dvanatta"
  - subject: "...and dont forget..."
    date: 2004-08-14
    body: "We even have a few KDE related tutorials too. (Somehow this seems as sort of a shock that no-oine is showing up for these things they way you talk about them.)\n\nKDevelop, Quanta and PyKDE/Qt are also going to make a showing.  So if you8 would like to learn about actual KDE stuff at a KDE conference, this is your chance. \n\nCheers\n   -ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "would be nice..."
    date: 2004-08-14
    body: "... to see these tutorials online, thx a lot!!"
    author: "anonymous"
  - subject: "Re: would be nice..."
    date: 2004-08-15
    body: "yeah, a stream or a small divx file of Martin's presentation would be awesome. I'd really like to know how to set up kolab and how the boss does it, but I can neither go to akademy nor can I spend ?500 for seeing this."
    author: "me"
  - subject: "Groupware / Portal solution?"
    date: 2004-08-15
    body: "I have been looking at all that web based groupware and portal solutions stuff. There is an excellent overview of all that in www.cmsmatrix.org. In my eyes it would be extremely useful to expand development into the portal direction. \n\nI guess, much that has been addressed by these portal solutions, will have been solved by kolab2, once it is available. But due to a lack of consistent, freely available information I cannot judge. \n\nIt would be absolutely cool for companies, clubs and associations to run a groupware server that integrates neatly all the functions as offered by MS-Sharepoint. \n\nWill the kolab-2 web frontend follow or in any way support the JSR 168 standard as described in the following document?\nhttp://developers.sun.com/prodtech/portalserver/reference/techart/jsr168/\n\nThen it would be possible to integrate it with other existing portal solutions in many enterprises out there. "
    author: "Markus Heller"
  - subject: "Re: Groupware / Portal solution?"
    date: 2004-08-16
    body: "Egroupware does everything that MS-Sharepoint does and more. I suggest that you download the newly released 1.0 version. It is absolutely beautiful. It consists of groupware (Email, Calendar, Addressbook), project management, file and bookmark sharing, knowledge base and content management. \n\nIt has a great interface and you can decide which apps you need and want to enable and which ones you do.\n\nTry it out.\n\nBefore I forget. It integrates with Kontact and there will soon be an Outlook plugin."
    author: "Gonzalo"
---
John H. Terpstra, Samba core developer and author of many books including
"<a href="http://www.amazon.com/exec/obidos/tg/detail/-/0131453556/qid=1066665885/sr=1-1/ref=sr_1_1/102-7153927-8348145?v=glance&s=books">The Official Samba-3 HOWTO and Reference Guide</a>", and Martin Konold from the KDE project offer during the  
<a href="http://conference2004.kde.org/">KDE Community World Summit 2004 "aKademy"</a> in 
<a href="http://www.ludwigsburg.de/">Ludwigsburg</a>, Germany, tutorials 
about the deployment of the multi-location groupware solution <a href="http://conference2004.kde.org/tut-kolab2.php">
Kolab 2</a>, the
<a href="http://conference2004.kde.org/tut-openldap.php">
OpenLDAP</a> directory server
and <a href="http://conference2004.kde.org/tut-samba3.php">
Samba 3</a>.



<!--break-->
<p>
All three tutorials are targeted at administrators of heterogeneous 
Linux/Windows networks who intend to offer
Open Source replacements for Microsoft Exchange, file and print servers while keeping a mixture of 
Linux/KDE and Windows desktops including Microsoft Outlook.
</p>
<p>
The combination  of these three projects
offers a powerful and fast directory server,
domain controller, file and print services
while also providing comprehensive groupware functionality including shared calendar support for MS Outlook and KDE Kontact users and central unified user management with single-sign-on capabilities.</p>



