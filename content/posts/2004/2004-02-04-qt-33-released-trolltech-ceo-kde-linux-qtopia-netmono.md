---
title: "Qt 3.3 Released; Trolltech CEO on KDE, Linux, Qtopia, .NET/Mono"
date:    2004-02-04
authors:
  - "Editor"
slug:    qt-33-released-trolltech-ceo-kde-linux-qtopia-netmono
comments:
  - subject: "Time to kill this then..."
    date: 2004-02-04
    body: "... http://qsqlite.sf.net"
    author: "Roberto Alsina"
  - subject: "Re: Time to kill this then..."
    date: 2004-02-05
    body: "You are right!\n\nIt's finally obsolete :-), the project did not get very far anyways (I did never really work on the code nor made any release). It was working though. But I am very happy that the trolls provide sqlite support directly in Qt.\n\nCU Dom"
    author: "Dominik Seichter"
  - subject: "Win32 noncommercial edition"
    date: 2004-02-04
    body: "Congratulations to Trolltech !\nI hope they will release soon a Win32 noncommercial edition. Does anybody know about that ? I am too lazy to install Cygwin.\n"
    author: "Jochen Rebhan"
  - subject: "Re: Win32 noncommercial edition"
    date: 2004-02-04
    body: "The new book \"C++ GUI Programming with Qt\" comes with Non Commercial Win32 Qt 3.2.1. As far as I know, this is the only place you can get it. I got the book for $39 at Amazon.com. The NC version is fully functional and not limited by anything but its non-commercial license."
    author: "David Johnson"
  - subject: "Re: Win32 noncommercial edition"
    date: 2004-02-07
    body: "Sounds like a great book. I'll have to buy it!"
    author: "David"
  - subject: "KDE/Mono"
    date: 2004-02-04
    body: "<quote>They have discussed a few times binding with either Portable.NET or Mono, but the fear of a possible Microsoft lawsuit on these projects has held off any plans to go for either (the Qt# project on Sourceforge is effectively dead). However, Mr Nord said that the legal issues are just a part; there are also some technical challenges which would make a well-supported Qt# environment difficult to create. </quote>\n\nWhat are the challenges and why is qtcsharp dead?  It doesn't say it's dead on Sourceforge."
    author: "ac"
  - subject: "Re: KDE/Mono"
    date: 2004-02-04
    body: "Qt# isn't dead, but I am no longer working on it.  Marcus continues development though and he's had the support of some interested developers lately.  I stopped working on Qt# after it became apparent (IMO) that it would never become a first class language for KDE development.  This was due to a number of fundamental shortcomings in the C# CLR design and the C++ ABI as well as some other factors.  \n\nBindings are *hard*. It is near impossible to devise a method to allow subclassing of Qt widgets (which is absolutely necessary for an OO language otherwise... what is the point?) because there does not exist a portable way to modify the C++ vtable at runtime.  At least not without breaking the C++ ABI.  The only other solution is to reimplement every virtual function in KDE/Qt and provide hooks.  This is a big bloated ugly solution IMO.  This is what PyQt, PerlQt, RubyQt, JavaQt are all doing AFAIK.\n\nAdd to this the fact that the C# CLR does not allow custom marshalling of C# properties get/set accessors (not to mention it doesn't allow one or the other to be virtual, ie the get accessor can't be virtual if the set accessor isn't), the possible future legal problems with MS patents, the fact that C# delegates are inferior to signals/slots, the fact that we're tied down to MS design decisions and core API's ... and I've decided to look elsewhere for a solution.\n\nMarcus is still working on Qt# though and others are more than welcome to help him out. "
    author: "manyoso"
  - subject: "Re: KDE/Mono"
    date: 2004-02-04
    body: "Thanks for the info.  I have been wondering what is up/not-up with QT# for a while.  \n\nbrockers"
    author: "brockers"
  - subject: "Re: KDE/Mono"
    date: 2004-02-04
    body: "Thanks for the update.  So I guess C is a win for GTK afterall and that GTK# has a bigger chance to take off."
    author: "ac"
  - subject: "Re: KDE/Mono"
    date: 2004-02-04
    body: "If you count the integration of .NET and the usage of Mono and GTK# a 'win'. If all that is important to Gnome, Ximian, GTK and others then all power to them. Most of us would just like to get things done, so that's where I think the big 'win' is."
    author: "David"
  - subject: "Re: KDE/Mono"
    date: 2004-02-04
    body: "Probably; however, GTK# (and Qt#) aren't interesting, but their implementations of Windows.Forms are."
    author: "anon"
  - subject: "Re: KDE/Mono"
    date: 2004-02-04
    body: "Not really. Windows.Forms all depends on Windows compatibility and WINE unfortunately."
    author: "David"
  - subject: "Re: KDE/Mono"
    date: 2004-02-05
    body: "You're right, GTK# isn't interesting, but I'm pretty sure they're (mono project) doing their own C# UI toolkit built on SWT, which on Linux of course is built on Gtk+"
    author: "David Walser"
  - subject: "Re: KDE/Mono"
    date: 2004-02-04
    body: "\" Bindings are *hard*. It is near impossible to devise a method to allow subclassing of Qt widgets (which is absolutely necessary for an OO language otherwise... what is the point?) because there does not exist a portable way to modify the C++ vtable at runtime. At least not without breaking the C++ ABI. The only other solution is to reimplement every virtual function in KDE/Qt and provide hooks. This is a big bloated ugly solution IMO. This is what PyQt, PerlQt, RubyQt, JavaQt are all doing AFAIK.\"\n\nYes, you're right bindings are a hard problem. But we have a solution to the problem of language independent virtual method callbacks already - the SMOKE library. As long as every language binding doesn't try and reinvent the same sort of thing it is not a 'big bloated ugly solution'. Every binding can link to libsmokeskde.so (built in kdebindings/smoke/kde), and once loaded in memory they all share it. Ruby and perl use it at the moment but other languages are quite possible eg java and C# using their proxy classes. The memory occupancy of the language dependent bit of a binding is quite tiny compared with the common SMOKE library.\n\nTo use SMOKE with C# it needs to use a custom RealProxy class, and I initially tried dotgnu but that class wasn't implemented yet. So, I've been playing with mono 0.30 for the last couple of days - great fun - got 'hello world' working! But writing custom RealProxies doesn't seem very well documented at all, and I'm still messing with that.\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: KDE/Mono"
    date: 2004-02-04
    body: "Very interesting!  You da man!  Thank you for making KDE bindings rock hard.  :)"
    author: "ac"
  - subject: "Re: KDE/Mono"
    date: 2004-02-04
    body: "Yes, I know Richard and I'm glad that you and Ashley are working on this.  However, for me, I think SMOKE and the bindings that rely upon it will never be a first class language compared to C++ for KDE/Qt development.  The overhead of binding each and every virtual function is ugly to my personal aesthetic.  However, if this were the only hurdle for a C# binding it wouldn't bother me as much... but this isn't the case.  The other problems still apply."
    author: "manyoso"
  - subject: "Re: KDE/Mono"
    date: 2004-02-04
    body: "The low-level programmer in me finds virtual functions in general to be a bit scary. On a Pentium 4, it takes more than 50(!) clock cycles to call a single virtual function. I doubt the bindings add more than 50% overhead to that. But in the end, its really not a bottleneck, so the programmer doesn't care either way :)"
    author: "Rayiner Hashem"
  - subject: "Re: KDE/Mono"
    date: 2004-02-04
    body: "Virtual functions are the entire key to Object Oriented programming.  The whole scheme doesn't make sense without them.  As such, they are of pivotal importance in any binding project for an OO language.  From a bloat standpoint, the extra thunks required are not as much a problem as the memory requirements for embedded systems.  Even this isn't so great a problem, but it insults my aesthetic sensibilities as a mickey mouse solution for runtime link compatibility.  So I'm looking elsewhere while others can continue with the binding efforts."
    author: "manyoso"
  - subject: "Re: KDE/Mono"
    date: 2004-02-05
    body: "Objective-C doesn't have Virtual Functions and its OO Paradigms are doing just fine. If you mean virtual functions within the C++/KDE realm than yes.\n\n"
    author: "Marc J. Driftmeyer"
  - subject: "Re: KDE/Mono"
    date: 2004-02-05
    body: "All Objective-C methods are effectively virtual!\n\nYou just don't specify it because you have no choice...\n"
    author: "Adrian Bool"
  - subject: "Re: KDE/Mono"
    date: 2004-02-05
    body: "Of course, 50 clock cycles on a 3Ghz P4 is just 16 nanoseconds. An uncached memory access probably takes more time. "
    author: "Henrik"
  - subject: "Re: KDE/Mono"
    date: 2004-02-04
    body: "A KDE perl, ruby, python program or whatever is mainly calling the qt/kde libs which are written in C++. They are all actually quite fast, unless you want to write a fast graphics app perhaps. Nothing as slow as Sun's java Swing.\n\n\"The other problems still apply.\"\n\nPlease explain what these are?\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: KDE/Mono"
    date: 2004-02-04
    body: "Yer, thanks for pointing that out. I have seen Smoke but haven't had the time to read about it."
    author: "David"
  - subject: "Re: KDE/Mono"
    date: 2006-08-08
    body: "Hi!\nI'm wondering, what the \"possible future legal problems with MS patents\" you mentioned are about. What patents are you referring to and which part of the Qt# or Mono world are they (possibily) affecting?\n"
    author: "Andreas"
  - subject: "Re: KDE/Mono"
    date: 2007-06-23
    body: "It is late in the ballgame to post this but here goes anyway:\n\nWith all the latest news about M$/novel and now M$/linspire, it would seem the original poster's insight about 'legal ramifications' were plausible,  and I also saw it coming; to think we still have no idea 'what' the infringments are it could all be FUD I realize but why so far two linux distros have signed onto this agreement without complete disclosure baffles me...and to think  opensuse would not let in some non-oss ( linux maintains nvidia blobs are NOT a kernel infringement, BUT what does he know ??? ) but are willing to sign away to non-disclosure about something as important as copyright ;00\n\nI noticed also that fedora pulled Beagle due to memory problems so Im curious to       ponder what other mono components are causing instability....at times like this kde is looking brighter in my eyes and maybe linus was right afterall....\n\nDoes kde still not use mono or anything else deemed possible M$ infringementes ?\n\ncheers\nneighborlee\nhttp://www.heartseed.org\n"
    author: "neighborlee"
  - subject: "what possible Microsoft lawsuit?"
    date: 2006-09-03
    body: "I'd like to know what this possible Microsoft lawsuit is that KDE is likely to face, that Novell is not.\n\nPlease when are we going to have a QT based mono c# environment. I really dislike the GNOME widgets, especially the combo boxes which have a tendancy to take over the entire screen, when they have a lot of data in them."
    author: "tracyanne"
  - subject: "patents"
    date: 2004-02-04
    body: "I read at the Hbasic(hBASIC.sf.net) site that the author also wants to integrate with the .net. \n\nBut what about patents?"
    author: "gerd"
  - subject: "Re: patents"
    date: 2004-02-04
    body: "There are many people, like Alan Cox, who have come out with Microsoft's threat of patents over Mono with .NET.\n\nMicrosoft do not use patents against competitors. They use technology. Remember DR DOS and that lovely error message you got when you tried to run Windows on it? IBM also played the same stupid game for many years. That's what Microsoft are about. Gently shifting goalposts to the point where people say \"Oh, it doesn't work\" and accept it. You just can't go around suing all and sundry. Where do you start? Better trap people with the technology thing that they can't understand or do anything about.\n\nMicrosoft won't use patents (not unless they're desperate :)) but they do like people to get scared and start talking about them. People like Alan Cox (although it isn't restricted to him at all) unwittingly do Microsoft's work for them :).\n\nMy problem with Mono is that it will simply be perceived by all the Windows developers out there as a way of getting MS .NET applications up and running on Linux and other platforms. It will not be perceived as a viable alternative, as Qt has the real potential to do. If MS .NET applications stop running with Mono, Mono instantly becomes a non-entity. No one is interested in GTK#, people will want to know if Windows.Forms will continue to work, and that very much depends on WINE and other hooks into the rather awful and extensive existing (and future) Windows API. With all the other things Microsoft are doing with .NET, like memory management (.NET has just not proven good enough for many 'low-level' fast applications like encoders, games and what not) and other ways Microsoft have of integrating .NET further into Windows, cross-platform compatibility is on a hiding to nothing. You need something nice, fresh and innovative that Microsoft cannot control. Hint: It's called Qt. Microsoft were so scared of Java because it was something new with a lot of buzz that Microsoft could not control in any way technologically. Of course Sun let Microsoft license it and they shoved Windows hooks onto it. Sigh... I think Trolltech has the Qt licensing for Windows quite right.\n\nMicrosoft will point to Mono and say \"Look, there's an open source version of .NET, we are open\" and use it as a means for publicising .NET (which hasn't took off at all). Over the course of the next few years they will integrate it further into Windows to the point where it will not be technically feasible to run any .NET applications under Mono. Many .NET applications are already a heavy bastard hybrid of x86/COM/MSIL compiled binaries to the point where I just don't see the point in it. .NET just seems to be Microsoft's response to the hype and popularity of Java :). Microsoft are panel-beating .NET into this one-size-fits-all development environment that just doesn't exist. Windows.Forms still depends totally on COM interfaces. Clever Microsoft. They always protect the user interface parts of their software. You can run purely functional COM servers quite happily on Linux and UNIX you know, but you don't do you?\n\nMono is creating some noise now because it is fashionable to say that you are developing an open source version of .NET. I'm not suggesting for one minute that this is why Ximian decided to do it, but it certainly seemed to get them sold :). We also had a lovely article about KDE supposedly moving to Mono and .NET as the main development infrastructure in the future. I wonder where that came from :).\n\nI like the concept of .NET, and I like C# as a language a lot (it is important to separate the language C# from .NET - it can be implemented in different ways, and it is open after all) and Microsoft have obviously looked heavily at Java and tried to improve on it. However, I have a big problem in the way that any open source implementation will be perceived and the problems that running headlong into that hype will cause. I just think that the rational behind Mono is creating a huge dog's breakfast in the medium to long term.\n\nThe people developing on Qt# made the right decision to stop working on it, and they obviously didn't get sucked into the hype. As a developer I do like the .NET concepts and languages, so perhaps we can have C# and .NET, or something like them, and Qt together without the baggage of a .NET/CLR implementation in the future. In terms of cross-platform support I would have thought Trolltech might be interested in that kind of direction, particularly for GUI applications. Perhaps pure compiled binaries can be an option where needed, but a language runtime can be used where appropriate.\n\nQt# has not proven to be the right way for all this, but that's open source software. You've got to try things out."
    author: "David"
  - subject: "Re: patents"
    date: 2004-02-05
    body: "You're not required to use Windows.Forms to make a .NET app.\n\nAlso, I thought they were moving away from COM and supposed to be using XML messages instead."
    author: "David Walser"
  - subject: "Re: patents"
    date: 2004-02-06
    body: "\"You're not required to use Windows.Forms to make a .NET app.\"\n\nYou can say the same thing about COM. But the Windows.Forms part is required to get GUI applications up and running. That is what people want."
    author: "David"
  - subject: "Re: patents"
    date: 2004-02-07
    body: "> But the Windows.Forms part is required to get GUI applications up and running.\n\nBut that's what I'm saying, even that's not true."
    author: "David Walser"
  - subject: "Re: patents"
    date: 2004-02-07
    body: "Which part of this are you not understanding? The implementation of Windows.Forms on Windows hooks very heavily into COM and the existing Win32 API. If you want to use MS.NET and use Windows.Forms here, you will, albeit unknowingly, be using the existing Win32 API. Yes you can use GTK# with Mono which gets around those problems, but people will not be interested in that.\n\nShould you wish to get your compiled MS.MET applications up and running on Linux and Mono (which people will just expect to happen) you will have to deal with all the problems that entails. To get Windows compatibility this is what is required. Why do you think that WINE is required to get this working with Mono?"
    author: "David"
  - subject: "Re: patents"
    date: 2004-02-07
    body: "Right, but you don't need Windows.Forms or GTK#.  The mono people are making a cross-platform GUI API that will work just as well on Windows as on Linux, based on SWT.  You can make full .NET apps using it, a lot of the stuff in .NET is not tied to what GUI API you are using."
    author: "David Walser"
  - subject: "Re: patents"
    date: 2004-02-08
    body: "Yer, but that wasn't wuite what I was getting at. Anyone using .NET on Windows will be using Microsoft's .NET and their implementation of Windows.Forms, not the GTK# or SWT options. When you look at it this way you have to ask what the point of Mono is at all.\n\nHowever, I do agree you can do a lot of functional things with .NET/Mono on non-Windows platforms that you could actually do with COM, but no one really did. This may be a good idea, but my problem was with the way that it would be perceived by many people, and how a lot of the Windows-specific stuff would be handled in the future. This may mean breaking any kind of compatibility permanently. I would love to be proven wrong."
    author: "David"
  - subject: "Re: patents"
    date: 2004-02-08
    body: "You have a very valid point.\n\nMono does provide at least the option to developers that want to use .NET but also are concerned with portability.  I guess that's really the whole point.\n\nIt doesn't seem it'll really let end-users run any .NET app under the sun, so Mono's usefulness is limited by how much they get the word out, and how much mindshare they get."
    author: "David Walser"
  - subject: "Re: patents"
    date: 2004-02-07
    body: "No they're not. They may look as if they are, but they aren't. You aren't familiar with Microsoft's history are you?"
    author: "David"
  - subject: "Re: patents"
    date: 2004-02-07
    body: "care to elaborate instead of just showing off?"
    author: "David Walser"
  - subject: "Re: patents"
    date: 2004-02-05
    body: "Just because Microsoft hasn't sued people over use of their patents doesn't mean they won't, or can't.  In fact, if something like Mono ever really threatened Microsoft's continued existance, you can be sure they'd pull out their patents and start shutting down their competitors.  There are two reasons why they won't right now.  First is the ongoing anti-trust problems they've been having.  Driving your competitors into the ground via patent lawsuits would only make them look guiltier.  The second is for their public image in relation to those anti-trust problems.  Using the courts to take out their competitors will only make people trust them less.  Right now, it only makes business sense to use those patents as a weapon when threatened by other people who hold patents.  Without a world-wide, perpetual, transferrable, royalty-free license from Microsoft to use those patents, I'd be wary of using technology embodied in them, especially knowingly."
    author: "Chad K"
  - subject: "Re: patents"
    date: 2004-02-07
    body: "US PATENT Application program interface for network software platform \nAn application program interface (API) provides a set of functions for application developers who build Web applications on Microsoft Corporation's .NET.TM. platform. \n\nhttp://tinyurl.com/5ns2\n\n\nMs tries to patent .NET\nhttp://www.beyondgeek.com/disp.php/199.html\n\n\nFFII site on MS\nhttp://swpat.ffii.org/players/microsoft/\n\nGates Says SCO's Case Against IBM Will Harm Linux\u0092s Commercial Prospects \nhttp://www.crn.com/Sections/BreakingNews/breakingnews.asp?ArticleID=43532\n\nMs applied for .Net patent\nhttp://yro.slashdot.org/yro/03/02/11/0048208.shtml?tid=109&tid=155\n\n\n\n----\nNote: There is a planned EU legislation \"IPR Enforcement\" Directive (unrelated to swpat directive) that will make a patent infrigement a criminal act. The voting will be very very soon.\n\n"
    author: "Andre"
  - subject: "How true!"
    date: 2004-02-05
    body: "This has to be one of the most astute commentaries on the whole .NET/Mono situation so far, and a refreshing contrast to the breathless \".NET is the new sex\" articles and opinion pieces from sites like OSNews.\n\nIf the Mono crowd wanted to have a virtual machine environment endorsed by a big corporation and with some momentum, why didn't they just make a go of improving Java technologies? Is it because the people at the Mono \"top table\" have some kind of Microsoft envy? Certainly, the various arguments about Java's \"locked down\" status are mere distractions - there's Java support in GCC, and various Java derivatives and research projects abound, albeit without using the Java trademark. And whilst there might be some kind of patent threat from Sun, it pales in comparison to the measures Microsoft have in their anti-competition arsenal.\n\nAnd the point about Mono being seen by Microsoft shops as an excuse for continuing their dubious ways whilst holding up a banner of supposed platform independence could hardly be more accurate. There are hordes of coders whose job involves hitting the right buttons in Microsoft Bullshit Studio, and loads of companies who perversely insist on Windows as a development environment, even when they're targeting Linux or Java platforms. Mono just gives those kinds of shops a figleaf to sell their services where cross-platform stipulations exist in the contract they're bidding for, only to hawk Windows licences later when their code won't run optimally or properly on non-Windows platforms."
    author: "The Badger"
  - subject: "GNU C++ compiler on Windows (MinGW gcc)"
    date: 2004-02-04
    body: "- Qt 3.3 adds support for the GNU C++ compiler on Windows (MinGW gcc)\n\nvery, very nice"
    author: "ac"
  - subject: "Re: GNU C++ compiler on Windows (MinGW gcc)"
    date: 2004-04-08
    body: "how can I download this GNU C++ compiler for widows\nthanks"
    author: "ehab"
  - subject: "Re: GNU C++ compiler on Windows (MinGW gcc)"
    date: 2005-09-20
    body: "I want to download GNU C++ compiler."
    author: "Mark"
  - subject: "Re: GNU C++ compiler on Windows (MinGW gcc)"
    date: 2007-03-08
    body: "i want to dowmload GNU C++ compiler"
    author: "khushi"
  - subject: "Re: GNU C++ compiler on Windows (MinGW gcc)"
    date: 2007-04-24
    body: "I want to download this compiler for my educational researh work."
    author: "Choy Ken Keong"
  - subject: "Re: GNU C++ compiler on Windows (MinGW gcc)"
    date: 2005-09-20
    body: "Iwan to download this compiler"
    author: "Mark"
  - subject: "Re: GNU C++ compiler on Windows (MinGW gcc)"
    date: 2007-10-26
    body: "Main Page: http://www.simulistics.com/products/compiler.htm\nDownload URL: http://www.simulistics.com/products/MinGW-2.0.0-3.exe\ngcc: http://www.simulistics.com/products/gcc-2.95.2-msvcrt.exe"
    author: "&#1605;&#1605;&#1604;&#1740;"
  - subject: "Re: GNU C++ compiler on Windows "
    date: 2008-11-07
    body: "how to download it. please provide the link immediately freeware"
    author: "ravi"
  - subject: "Re: GNU C++ compiler on Windows (MinGW gcc)"
    date: 2005-09-20
    body: "How to download"
    author: "Mark"
  - subject: "Re: GNU C++ compiler on Windows (MinGW gcc)"
    date: 2007-04-24
    body: "I would like to have the compiler for my educational reseach work"
    author: "Choy Ken Keong"
  - subject: "Re: GNU C++ compiler on Windows (MinGW gcc)"
    date: 2007-05-31
    body: "good compiler"
    author: "ha ha"
  - subject: "Re: GNU C++ compiler on Windows (MinGW gcc)"
    date: 2009-01-01
    body: "http://webscripts.softpedia.com/scriptDownload/GNU-Compiler-Collection-Download-26869.html"
    author: "neo"
  - subject: "Why"
    date: 2004-02-05
    body: "Why no Mono support? What lawsuits, it's an open standard!"
    author: "Alex"
  - subject: "QT 3.3 + KDE 3.2?"
    date: 2004-02-05
    body: "kdelibs 3.2 didn't compile against qt 3.3.0b1 (http://bugs.kde.org/show_bug.cgi?id=73565 and 72501); will it work with the final qt 3.3?"
    author: "Cloaked Penguin"
  - subject: "Re: QT 3.3 + KDE 3.2?"
    date: 2004-02-05
    body: "Hi!\nI have installed qt-3.3.0b1 with kde 3.1.95 and i have removed\nthe PM_* code and all works fine. \n\nMy Setup:\n( LFS 5.0 (+minor code modification in some packages to work with 2.6.1 ) + kernel 2.6.1 ( sysfs patches ) + hacked coreutils 5.0 (to \nsupport my \"sqlite package management\" = install from source and have packages automatic logged to an sqlite database  )\n\nBye Thomas"
    author: "schoko"
  - subject: "Re: QT 3.3 + KDE 3.2?"
    date: 2004-05-06
    body: "AOA , Hi ...\n\t\t\t\nWell can you help me out... here\n\nQT 3.3.2 + KDE 3.2.2\n\nI tried to Upgrade my KDE 3.1-10 RedHat to KDE3.2.2\n\nWell, actually i got a lot of dependecy errors.\nOne of them was  \"redhat-artwork-0.93-1.i386.rpm\" required /usr/lib/qt-3.3\n\nI downloaded qt 3.3.2, extracted in /usr/local/gt, as instructed in its docs  and ran ./configure and make commands ....\n\nThen I again tried \"# rpm -U redhat-artwork-0.93-1.i386.rpm\" and again dependcy error same as above required /usr/lib/qt-3.3\n\nWhat do i do... ?"
    author: "compute.ash"
  - subject: "Re: QT 3.3 + KDE 3.2?"
    date: 2004-07-04
    body: "Please don't give my e-mail!!!\n\nI have the same and I'm trying to resolve it.\nI have fedora core1 and i have downloaded kde-3.2.2 rpm for fedora core1!!\nI had no problem until i have tried to compile some kde windows decoration (style) from www.kde-look.org many and strange compilation error!!!\n So i have decided to download qt-3.3.2 from source and compile and install myself i have again problem with compilation but this in mot important to you what is important is that installation of qt-3.3.2 from source have created problem that you have so I have deinstalled qt compiled from me and qt qt from kde fedora, the probelm isn't resolved nut changed: qt < 1:3.0.4-11 conflicts with redhat-artwork-0.93-1.\nIdon't know why, but compilation of qt from source have created tis problem now i think to download redhat-artwork and compile myself, Maybe i could resolve error!!!\nI hope this could help you \n\nP.S: Which distribution do you have?"
    author: "Francesco Serio"
  - subject: "yippiieeh!"
    date: 2004-02-05
    body: "...and last but not least:\n\nprecompiled headers!\nThis should decrease compiling times significantly!\n\nKta >8^)"
    author: "katakombi"
  - subject: "QT-3.3.0 does not detect freetype"
    date: 2004-02-05
    body: "I have this suse 8.0 box where qt-3.3.0 does not even configure properly. It disables xft with these messages:\n\nXft auto-detection... ()\n  Found libXft.a in /usr/X11R6/lib\n  Found X11/Xft/Xft.h in /usr/X11R6/include\n  Found X11/Xft/Xft.h in /usr/include\n  Found Xft version ..\n  Found X11/Xft/XftFreetype.h in /usr/X11R6/include\n  Found Freetype version 2.0.8\n  Could not find freetype2/freetype/freetype.h anywhere in  /usr/X11R6/include  /usr/include /include\nXft disabled.\n\nOn the same system, qt-3.2.2 detects freetype just fine with these messages:\n\nXft auto-detection... ()\n  Found libXft.a in /usr/X11R6/lib\n  Found X11/Xft/Xft.h in /usr/X11R6/include\n  Found X11/Xft/Xft.h in /usr/include\n  Found Xft version .\n  Found X11/Xft/XftFreetype.h in /usr/X11R6/include\n  Found freetype2/freetype/freetype.h in /usr/include\nXft enabled.\n\n\nThis is strange since /usr/include/freetype2/freetype/freetype.h surely exists. Also \"rpm -qa | grep freetype\" shows:\n\nfreetype-1.3.1-497\nfreetype2-2.0.8-30\nfreetype2-devel-2.0.8-30"
    author: "goodfella"
  - subject: "Re: QT-3.3.0 does not detect freetype"
    date: 2004-02-06
    body: "Ok, it seems QT-3.3.0 requires freetype-2.0.9 or later versions but the config.tests/x11/xfreetype.test file does not print an error message in such a case."
    author: "goodfella"
  - subject: "Re: QT-3.3.0 does not detect freetype"
    date: 2004-02-06
    body: "There's a bugin the qt configuration scripts which prevents it from working with freetype 2.1.x (it requires 2.0.9 or greater). In qt-3.3.0b1/config.tests/x11/xfreetype.test, line 132, it checks the version number of the freetype library. It requires it to be 2.0.x, for x>9. Changing the numbers in quotes from \"2\" \"0\" \"9\" to \"2\" \"1\" \"0\" worked for me (compiling qt 3.3.0b1 with version 2.1.0)"
    author: "Roie_m"
  - subject: "TrollTech shifts focus towards Linux as the main.."
    date: 2004-02-06
    body: "\"Three years ago there were only 3-5% commercial Qt developers developing for Linux, but now this number is up to 40%. This has made Trolltech shift focus towards Linux as the main \"Unix/X11\" platform supported by Qt, along to Windows and Mac.\", Haavard Nord, Trolltech's CEO and founder. (http://www.osnews.com/story.php?news_id=5908)\nI say, HURRA!"
    author: "Pupeno"
  - subject: "Konsole fonts broken again"
    date: 2004-02-10
    body: "Maybe this is just an issue with my distribution (Slackware 9.1), but with the latest Qt 3.3 release, the Linux font in Konsole is broken again. Does anybody know where I have to look for the problem? Maybe it is just a misconfiguration or another compile option.\n\nSteinchen"
    author: "Steinchen"
  - subject: "Re: Konsole fonts broken again"
    date: 2004-03-30
    body: "Yeah, When upgrading to KDE 3.2.1 on slackware the linux font on kosole no longer works. I am using bitstream at the moment. I think its a bug in qt. Hopefully it will get fixed soon. :)"
    author: "slackd, jawed yockle!"
---
<a HREF="http://www.trolltech.com/">Trolltech</a> today <a href="http://www.trolltech.com/newsroom/announcements/00000155.html">released Qt 3.3</a> which <a href="http://www.trolltech.com/developer/changes/changes-3.3.0.html">among other things</a> <a href="http://www.trolltech.com/products/qt/whatsnew.html">features</a> improved .NET support, full 64-bit support, IPv6 and backend support for two more databases. Also new is the -dlopen-opengl configure switch, which is very useful for prelinking Qt and KDE apps against the -PIC compiled nVidia drivers and support for Xft2 client side fonts on X servers without the
RENDER extension. In light of the release, <a href="http://www.osnews.com/">OSNews</a> features an <a HREF="http://www.osnews.com/story.php?news_id=5908">article with TrollTech's CEO</a>, Haavard Nord. Nord says that he sees Linux strengthen its position in both business computing and embedded systems, while he forsees Qtopia and Linux taking over PDAs and Smartphones in the next few years.





<!--break-->
