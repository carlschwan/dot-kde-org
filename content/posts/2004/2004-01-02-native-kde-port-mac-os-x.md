---
title: "Native KDE Port for Mac OS X"
date:    2004-01-02
authors:
  - "kpfeifle"
slug:    native-kde-port-mac-os-x
comments:
  - subject: "X independency lib"
    date: 2004-01-02
    body: "what about a X independency Libary wich translates all calls of KDE apps to X or MAC ? wie can add more displays later like directx or directfb or xouvert or what you cann imagine.\n\n"
    author: "chris"
  - subject: "Re: X independency lib"
    date: 2004-01-02
    body: "What about a replacement of the Mac-Interface with a version, which uses X and is binary-compatible? Because of the missing X-support, I don't buy a Mac."
    author: "Phisiker"
  - subject: "Re: X independency lib"
    date: 2004-01-02
    body: "Mac OS X 10.3 comes with a built-in X server.  It's rootless, so X apps co-exist with native apps.  The windows are managed by the OS, so they have proper borders and drop-shadows too.  The server itself is easy to manage, just click and go.  I was very impressed."
    author: "Justin"
  - subject: "Re: X independency lib"
    date: 2004-01-02
    body: "\nActually, the built-in X server can be rooted or rootless, your choice."
    author: "Anonymous"
  - subject: "Re: X independency lib"
    date: 2004-01-02
    body: "How about a transparent kitty cat that would be like \"meow\" and then the hardware will be like WTF? Meow? \n\n\nhades is my HERO!!!\n"
    author: "Mr_boots"
  - subject: "Re: X independency lib"
    date: 2004-01-03
    body: ">How about a transparent kitty cat that would be like \"meow\" and then the hardware >will be like WTF? Meow?\n\n\n>hades is my HERO!!!\n\nWhoa - easy on the catnip there, bucko."
    author: "Yo Daddy"
  - subject: "Re: X independency lib"
    date: 2005-09-23
    body: "It also doesn't work very well: poor desktop integration, no menu bar support, all X11 windows get treated as a single group.  X11 is simply not a priority to Apple.\n\nAnd, yes, replacing Quartz with X11 would be a good thing."
    author: "Mike"
  - subject: "Re: X independency lib"
    date: 2005-09-23
    body: "We already have one, it's called Qt"
    author: "mikeyd"
  - subject: "Hooray!"
    date: 2004-01-02
    body: "For truly cross-platform toolkits!"
    author: "Anonymous Coward"
  - subject: "Re: Hooray!"
    date: 2004-11-18
    body: "Hooray!!  Cheers!!  \n\nThe last time before this pre-alpha version I got any of a Linux working on OS X was going through the FINK process through terminal, and using X11 to run it.  That was a pain in the neck in some ways.  Particularly afterward, because Norton emergency disk couldn't Disk Doctor the disk with it installed.  Plus, some applications just wouldn't run.  \n\nThis is just the best news in the computer world to me.  I don't want to buy all new hardware to run Linux.  OS X Panther is great.  KDE is great.  Having both is REALLY great.  Now if GIMP goes native OS X and adds a some more bitmap tools and cmyk, that'll just complete this fantastic setup.  After all, blender 2.35 (-= is native OS X Panther.  And blender now has Global Undo in 2.35 ~~ hats off to blender!  \n\nThank you for programming KDE for native on OS X.  I can now have panther and linux on the same desktop, I don't know if computer life could be any better. "
    author: "Southpaw"
  - subject: "But the UI elements ..."
    date: 2004-01-02
    body: "Look at the Kate snapshot.  The buttons all look like XP.  XP != Mac.\n\nAre those buttons all from Qt or something?  They need native Quartz buttons/etc.\n"
    author: "Riot Nrrrd\u0099"
  - subject: "Re: But the UI elements ..."
    date: 2004-01-02
    body: "Which Kate snapshot? qt-mac-kdebase.png? That was obviously taken when the Mac style didn't work yet.  <a href=\"http://www.csh.rit.edu/~benjamin/about/pictures/KDE/osxtheme.png\"> This (osxtheme.png) shows a later state.</a>"
    author: "Anonymous"
  - subject: "Re: But the UI elements ..."
    date: 2004-02-21
    body: "You can easily see that all elements in KDE are just \"emulated\" , i.e. painted to look like native elements, because they have many flaws compared to real Aqua. I suspect that with the KDE design it's impossible to use native Aqua widgets, because those don't allow any kind of 'themeing'.\n\nBut what the hell .. KDE was not designed with Mac look and feel in mind, so it will always have \"alien\" UI compared to Aqua. The goal is rather to reduce the pain using it in OS X (very much like what OroborOSX did before Apple produced the \"real thing\"). Normal Apple users won't use KDE anyway, so I think it's less of an issue.\n"
    author: "blbbottom"
  - subject: "Re: But the UI elements ..."
    date: 2004-02-22
    body: "nope.. it uses the appearance manager just like cocoa and carbon applications do. Nothing is emulated. However, currently, none of the behavior questions have even been addressed yet, nor worked on.\n\n> Normal Apple users won't use KDE anyway, so I think it's less of an issue.\n\nYes, but normal Mac users can use KDE /apps/"
    author: "anon"
  - subject: "Re: But the UI elements ..."
    date: 2004-01-02
    body: "seems the shots are taken during various stages in development ... kword looks very nice"
    author: "ik"
  - subject: "Re: But the UI elements ..."
    date: 2004-01-02
    body: "The UI elements such as tabs buttons in this\n<a href=\"http://ranger.befunk.com/screenshots/qt-mac-konsole.png\">Konsole screenshot</a> are correct for Mac OS X 10.3 Panther. Tabs shown in \n<a href=\"http://ranger.befunk.com/screenshots/qt-mac-kword-20040101.png\">KWord screenshot</a> are not Mac OS X native.\n"
    author: "Jon"
  - subject: "Re: But the UI elements ..."
    date: 2004-01-02
    body: "Sorry, but I'm pretty sure you're looking at the terminal window behind Konsole, which AFAIK, is an open source, native OS X application.  I don't use OS X that much, so I'm afraid I can't tell you which app it is."
    author: "silvestrij"
  - subject: "Re: But the UI elements ..."
    date: 2004-01-02
    body: "Heh.\n\nThat's because that's a Mac app behind Konsole. Konsole is using the Windows style scrollbars."
    author: "me"
  - subject: "Re: But the UI elements ..."
    date: 2004-01-03
    body: "That is iTerm in the background.  A good, if albeit flawed terminal package.  It looks like the developer is having standard err go to the iTerm to test Konsole.\n\n"
    author: "clark goble"
  - subject: "Mac user"
    date: 2004-01-02
    body: "As Mac users are used to the shareware principle perhaps we could raise funds. The gpl is compatible with the ASP guidelines for shareware. \nWe could ask for donations to fund the development. And even better, Apple could pump development money into KOffice as they did with KHTML. On the homepages on most download pages I don't see a reference to the KDE donation system."
    author: "andre"
  - subject: "Re: Mac user"
    date: 2004-01-02
    body: "> And even better, Apple could pump development money into KOffice as they did with KHTML.\n\nGood idea...\nStill I hope that one day the same khtml tree is used for konqueror and\nsafari. Right now it feels that khtml is hanging significantly behind safari,\neven that IIRC all of the initial apple patches have been merged.\nThere has been a lot of work going into safari after..."
    author: "ac"
  - subject: "Re: Mac user"
    date: 2004-11-18
    body: "I agree.  KDE natively on mac is worth investing in any day of the week.  Well, at least the days there's some extra money, lol.  "
    author: "Southpaw"
  - subject: "how about native Cocoa KDE port?"
    date: 2004-01-02
    body: "Hey people, how about native Cocoa KDE port instead of Qt one?\n\nThis thing does not seem to me native, becuase it's not Cocoa but uses Qt instead.\n\nFreeing the KDE from Qt (at least for the Mac in the beginning - as (thanks to Apple) they already did to Safari/KHTML/KJS) is freedom from GPL!\n\nFreeing KDE from Qt is a way out from the already very serious GPL issue, which is the reason for companies to choose GTK instead of KDE. My understanding is that KDE libs are LGPL and the only problem is with Qt, once KDE libs have no GPL dependency - no GPL problem.\n\nSafari is a good example for a 100% GPL free code.\n\n\nBtw what is the license of this KDE port - GPL or LGPL? (because if its still LGPL Apple or someone else could just free it from GPL)"
    author: "Anton Velev"
  - subject: "Re: how about native Cocoa KDE port?"
    date: 2004-01-02
    body: "Hey Anton Velev, how about stopping your anti-Qt spin sometime? You are free to not wanting to use some particulary licenses, but stop pushing others and bothering them with your painfully apparent lack of any technical insight. Thanks."
    author: "Datschge"
  - subject: "Re: how about native Cocoa KDE port?"
    date: 2004-01-02
    body: "Hey, Datschge, be patient ;-)\n\n"
    author: "Kurt Pfeifle"
  - subject: "Re: how about native Cocoa KDE port?"
    date: 2004-01-02
    body: "You should be joking.. have you ever read some news on the internet?\n\nKde is a great desktop and my understanding is that it's only pain is the GPL issue. Correct me if I am wrong but KDE libs are 100% free of GPL and the only problem with GPL is Qt, right?\n\n(read the licenses again, I will do the same too, but again until now I was sure and had the belief that KDE libs are free of _any_ GPL code and only Qt is 100% GPL)\n"
    author: "Anton Velev"
  - subject: "Re: how about native Cocoa KDE port?"
    date: 2004-01-02
    body: "I don't consider the GPL issue to be a pain.  I consider it to be a major benefit."
    author: "Jonathan Bryce"
  - subject: "Re: how about native Cocoa KDE port?"
    date: 2004-01-03
    body: "well, for me, the license is a pain... thats why though i use kde as my desktop, i have to program in gtk :("
    author: ":-|"
  - subject: "Re: how about native Cocoa KDE port?"
    date: 2004-01-03
    body: "qt is dual licensed you know. there's no excuse to be programming in an inferior toolkit because you/your company can't be bothered shelling out a thousand bucks for a qt license. "
    author: "d_i_r_t_y"
  - subject: "Re: how about native Cocoa KDE port?"
    date: 2004-01-03
    body: "I want to create a GPL application for Windows, Mac and Linux. Can you show me where I can download the 3.2 Qt for windows?\n\nNot to sound sarcastic, but what do you mean \"cross platform and supporting GPL development under windows\" ?? Let alone non GPL."
    author: "ac"
  - subject: "Re: how about native Cocoa KDE port?"
    date: 2004-01-03
    body: "http://kde-cygwin.sourceforge.net/qt3-win32/ has instructions where you can download and how you can help to make it happen."
    author: "Anonymous"
  - subject: "Re: how about native Cocoa KDE port?"
    date: 2004-01-03
    body: "Thank you!\n\nI dont want to sound like a troll,\nbut quoting http://kde-cygwin.sourceforge.net/:\n\n\"Currently Qt 1.45, 2.3.1, 3.0.4, 3.1.1 and KDE 1.45, 2.2.2 and 3.1.1 have been ported and are of beta quality.\"\n\nThat's not Qt3.2 (and all Beta?) and kde-cygwin certainly does seem to be a Qt lead project. What guarantee does one have this kde-cygwin project keeps up? I guess I also have to foregive 'unstable' Mac support.\n\nSo far I am not impressed by Qt's support for cross platform and I AM talking pure GPL code here. After all its Trolltech themselves who claim Qt being cross-platform. But developers need to be send somehow to a non-trolltech site, still not to find what they are looking for.\n\nI would prefer using Qt, but for now its still wxWindows here, and I am not even mentioning any dual license."
    author: "ac"
  - subject: "Re: how about native Cocoa KDE port?"
    date: 2004-01-03
    body: "typo: and kde-cygwin certainly does seem *NOT* seem to be a Qt lead project.\""
    author: "ac"
  - subject: "Re: how about native Cocoa KDE port?"
    date: 2004-01-03
    body: "No, Trolltrech's official Qt's as well as KDE's Qt-copy patches (Qt/X11 only) are the only \"leading\" sources. Feel free to get what you want by giving them the support they ask you to give. If you don't want to do so and instead limit yourself to your own egocentrical needs don't blame others for doing the same."
    author: "Datschge"
  - subject: "Re: how about native Cocoa KDE port?"
    date: 2004-01-03
    body: "Look, Trolltech tells on their site being cross-platform, but what this really seems to mean is: let uss create \"leading\" sources and let others do the porting which might or might not work. Those doing the porting cannot earn money which we CAN make from their porting by requering money for non GPL code. \n \nAll this is not really Qt pursuing being cross-platform in my book. If you call all this \"me limiting myself in my own egocentric needs\" then so be it!"
    author: "ac"
  - subject: "Re: how about native Cocoa KDE port?"
    date: 2004-01-03
    body: "What exactly are you on about? Trolltech developed and maintains all of their own ports, AFAICT."
    author: "agsnu"
  - subject: "Re: how about native Cocoa KDE port?"
    date: 2004-01-03
    body: "I guess there's some confusion here: \n\nAFAIK: \n- There's the win version of Qt3 which is a native version completely \n  written by Trolltech, *not* available under the GPL. \nand \n- then there's the cygwin version which is a port of the GPLed X11\n  version of Qt.  This port has nothing to do with Trolltech. \n\nSo Trolltech writes and maintains the versions they claim to. \nBut: \n> and kde-cygwin certainly does seem *NOT* seem to be a Qt lead project.\"\n\nIt's certainly not a Trolltech project.  No one claimed that.\nIf you want Trolltech's win version of Qt you'll have to buy it. \n\n"
    author: "cm"
  - subject: "Re: how about native Cocoa KDE port?"
    date: 2004-01-03
    body: "Yes it seems I was wrong on the part where I thought Trolltech could make money of other peoples porting work.\n\nThe last post does indicate Trolltech hardly directly  supports or encoureges GPL software under windows using their Qt library, I just think that's a sorry state of affairs. A different approach on Trolltechs part could move many more developers to start using Qt for cross platform stuff."
    author: "ac"
  - subject: "Re: how about native Cocoa KDE port?"
    date: 2004-01-04
    body: "I'm sure Trolltech might consider again releasing a GPL'ed Qt/Win when a KDE release on OSX should considerably increase the number of commercial Qt/Mac licencees. If that doesn't happen then a GPL'ed Qt/Win is very unlikely in the future considering the very bad experience with the previous free version."
    author: "Datschge"
  - subject: "Re: how about native Cocoa KDE port?"
    date: 2004-02-13
    body: "The beta state in fact comes mostly from problems of the underlaying cygwin emulation library and because of the different os and/or service pack, buggy ms update combinations and relating problems for which I'm not resonsible, but are affected. \n\nThe qt 2.x releases were 85000 times downloaded (see http://sourceforge.net/project/showfiles.php?group_id=27249) with about 40-50 problem reports (I remember not very exactly, but it were very less). \nFor the qt 3.x release I have measured about 32000 downloads until now with very low problem rate (I remember about 5-10), so this port could not be so bad.\n\nAdditional I'm using this qt release every day writing mails with kmail/cygwin without virus problems,browse the internet with konqueror/cygwin and had never problems with qt library, more with hanging processes after a windows update (So it really seems to me that this qt port could leave the beta status)\n\nBTW:This is currently not true for the native qt port, but we hope that this will be also in the future. \n\nRalf \n\n\n\n"
    author: "Ralf Habacker"
  - subject: "Re: how about native Cocoa KDE port?"
    date: 2004-01-03
    body: "i agree with you. Kde is a nice desktop for the linux, but the pain GPL.\nhowever why don't you check out something different than GTK (it's not only GTK and Qt, there are a lots of quality OO GUI libs) - wxWindows and VCF seem very responsive and their APIs are very good. Also for some other things that are very simple or browser/office oriented you may also check out the mozilla/gecko libs and the OO.o libs.\n"
    author: "Anton Velev"
  - subject: "Re: how about native Cocoa KDE port?"
    date: 2004-01-02
    body: ">> Hey people, how about native Cocoa KDE port instead of Qt one?\n\nThis sure is too much work. Qt contributes *a lot* to what KDE is.\n\n>>  Freeing the KDE from Qt....\n\nI think we strongly disagree here. I belong to the camp which is convinced that the GPL in essence *defends* and *protects* the Freedom of Software covered by it.\n\nTo \"free KDE from GPL\" (as you imply) would make KDE vulnerable to non-Free attacks, and make it non-Free itself, sooner or later...    ;-)\n\n>> they already did to Safari/KHTML/KJS\n\nIn KHTML and KJS is and was no Qt code (or very, very little, which makes replacement easy). KHTML and KJS were completely under the LPGL, which explicitely allows usage of code without \"giving back\" anything. Apple however chose to give back anyway. Probably they act currently voluntarily in relation to KHTML and KJS, by and large just like they were bound to the GPL. However they are not *bound* to it, and that's probably what they care most about it....\n\n>> the reason for companies to choose GTK instead of KDE\n\nNot quite right: kdelibs *are* under LPGL! However kdelibs also use Qt (under GPL). So probably you meant to say \"to choose GTK instead of Qt\".... \n\nBut even *then* it would not be quite right. Since Qt is dual-licensed and Qt allows for the same things the LPGL allows for, albeit in return of a licanse fee, they *could* use Qt (so this has the additional advantage of ploughing back at least money to employ fulltime developers improving the GPL-version of Qt ...). \n\nAnd in fact, a lot of Fortune 500 corporations *do* develop using Qt: Adobe, Boeing, Borland, Canon, DaimlerChrysler, Deutsche    Telekom, Disney, Fujitsu, General Electric, Hitachi, Honda, HP, IBM, Intel,    JD Edwards, Lockheed-Martin, Mitsubishi, NASA, NEC, Pioneer, Samsung, Scania, Sharp, Shell, Siemens, Sony, Toshiba, Toyota, Unilever, Volkswagen... You can find more on the Trolltech website at \n\n -->  http://www.trolltech.com/ <--\n\nWhy don't all these companies use Gtk? Why don't they prefer a *gratis* toolkit?\n\nBecause they use additional considerations. It is not just the per-developer, one-time license fee that counts for them when they choose a tool. It is also the question if it is the *right* tool for the job. \n\nMost commercial Software developming companies are applying avarage business sanity before taking a toolkit decision. They don't mistake IEC (\"Initial Entry Costs\") for using a particular toolkit with its TCO (\"Total Cost of Ownership\"). They know very well that many more factors than just developer licenses are involved when trying to calculate their ROI (\"Return Of Investment\"), like:\n\n   * a professional support for the toolkits their developers use, including      access to a hotline;\n   * overall development time; \n   * good and well-organized documentation; \n   * their available developers' learning curve if these must use a new toolkit;\n   * the company's policy or preference for a programming language; \n   * relative bugfree-ness and stability of the toolkit;\n   * the \"time to market\" the final product;\n   * re-use of the toolkit on various OS platform with a uniform performance,  support and stability;\n   * and some considerations more. \n\nOverall, it seems to have made many Fortune 500 companies (as well as small or mid-sized ISVs) to decide for themselves that the Qt \"license price\" is worth the value over the alternative, which is \"gratis\" Gtk development."
    author: "Kurt Pfeifle"
  - subject: "Re: how about native Cocoa KDE port?"
    date: 2004-01-02
    body: "KHTML is LGPL'ed, which means Apple was obligated by the license to give back the changes to the rendering engine. They were not obligated to give out the source of the Safari browser itself, and they did not. Aside from the webcore component, Safari is 100% closed source. "
    author: "Rayiner Hashem"
  - subject: "Re: how about native Cocoa KDE port?"
    date: 2004-01-06
    body: "You are confusing GPL and LGPL. Because KHTML is LGPL'ed (and not GPL'ed) Apple was _not_ obligated by the license to give back the changes to the rendering engine.\n\nSee for example: http://www.gnu.org/licenses/why-not-lgpl.html\n\nAnd of course the licenses themselves:\nGPL: http://www.gnu.org/licenses/gpl.html\nLGPL: http://www.gnu.org/licenses/lgpl.html\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: how about native Cocoa KDE port?"
    date: 2004-01-06
    body: "You're wrong Ingo. The LGPL doesn't cancel the duty to make changes to a LGPL library available:\n\n4. You may copy and distribute the Library (or a portion or derivative of it, under Section 2) in object code or executable form under the terms of Sections 1 and 2 above provided that you accompany it with the complete corresponding machine-readable source code, which must be distributed under the terms of Sections 1 and 2 above on a medium customarily used for software interchange. "
    author: "Anonymous"
  - subject: "Re: how about native Cocoa KDE port?"
    date: 2004-01-02
    body: "Okay, i know the arguments for a multiplatform OO API, many do already support multiple platforms:\n- MPL Mozilla (cannot guess what was the toolkit behind it but for sure it's their own)\n- LGPL OpenOffice.org VCL? (or something like it, for sure you know what i mean I don't have the free time now to search on the web what exactly was its name)\n- wxWindows - you tell me what is the license\n- BSD VCF (http://vcf.sourceforge.net/)\n- GPL QT\n- LGPL GTK\nSeems that a lot of them are very capable and some of them very well developed and OO though. But the market seems to adopt GTK like it or not. (I am personally for BSD) (for interested people read this: http://www.cons.org/cracauer/gpl.html ))\n\nThe other issue of course is not just the quality of the code (VCF seems to be very good btw) but also the need of relyability on 3rd party software (Microsoft and Qt are good example) while one prefers to rely on something real free or entirely internal codebase.\n\nOf course there could be may be a lot more reasons, but of couse you guys should not care about them, since you care about KDE (not the GTK vs QT vs VCF vs wxWindows vs other guys affairs). What is for sure is that KDE loses from this affairs, and that Sun, HP, and some other (like Novell, UserLinux etc) adopt non-GPL solutions. ....\n\nanyway, KDE free of GPL will be better in my oppinion for the adoption"
    author: "Anton Velev"
  - subject: "Re: how about native Cocoa KDE port?"
    date: 2004-01-02
    body: ">> What is for sure is that KDE loses from this affairs...\n\nWe'll see.\n\nAnd work against this in the meanwhile. Stay tuned."
    author: "Kurt Pfeifle"
  - subject: "Re: how about native Cocoa KDE port?"
    date: 2004-01-03
    body: "Me being an interested person read your linked article, and in my opinion both the author as well as you don't have the \"Required knowledge\" stated at the top of the very page: \"You must understand why authors of free software differ in their opinions what to allow others to do with the software they released to the public.\""
    author: "Datschge"
  - subject: "Re: how about native Cocoa KDE port?"
    date: 2004-01-03
    body: "> What is for sure is that KDE loses from this affairs, and that Sun, HP, and some other (like Novell, UserLinux etc) adopt non-GPL solutions. ....\n\nReally? Now, how do you reason? So you mean that if a \"big company\" uses a toolkit, that means success? That is not how I define success.\n\nIf these \"big companies\" use free toolkits at their will, to develop kick ass commercial apps, without contributing much back, then what will happen? This will surely be the knife on the throat for open source apps.\n\n> anyway, KDE free of GPL will be better in my oppinion for the adoption\nIs this really the \"adoption\" you wants? Think again please. Open source development is at risc here."
    author: "OK"
  - subject: "Re: how about native Cocoa KDE port?"
    date: 2004-01-03
    body: "> so this has the additional advantage of ploughing back at least money to..\n\n...the open source community. :-)"
    author: "OK"
  - subject: "Re: how about native Cocoa KDE port?"
    date: 2004-01-03
    body: "Actually, Safari also uses Qt. Not for it's main interface, but for rendering the parts of its interface introduced by HTML."
    author: "Steven Fisher"
  - subject: "Re: how about native Cocoa KDE port?"
    date: 2004-01-03
    body: "Actually, Apple made a wrapper library that emulates Qt string types using native types and such without having to use Qt.  KHTML doesn't really have any GUI code in it in the first place."
    author: "Ranger Rick"
  - subject: "Re: how about native Cocoa KDE port?"
    date: 2004-01-03
    body: "Safari does *not* use Qt at all.  This is what it uses:\n\notool -L Safari \nSafari:\n        /System/Library/Frameworks/Cocoa.framework/Versions/A/Cocoa (compatibility version 1.0.0, current version 9.0.0)\n        /System/Library/Frameworks/Carbon.framework/Versions/A/Carbon (compatibility version 2.0.0, current version 128.0.0)\n        /System/Library/Frameworks/WebKit.framework/Versions/A/WebKit (compatibility version 1.0.0, current version 1.0.0)\n        /System/Library/Frameworks/AddressBook.framework/Versions/A/AddressBook (compatibility version 1.0.0, current version 300.0.0)\n        /System/Library/Frameworks/ApplicationServices.framework/Versions/A/ApplicationServices (compatibility version 1.0.0, current version 19.0.0)\n        /System/Library/Frameworks/Security.framework/Versions/A/Security (compatibility version 1.0.0, current version 163.0.0)\n        /usr/lib/libSystem.B.dylib (compatibility version 1.0.0, current version 71.0.0)\n\nSince I was curious, I also checked WebKit\n\notool -L WebKit \nWebKit:\n        /System/Library/Frameworks/WebKit.framework/Versions/A/WebKit (compatibility version 1.0.0, current version 1.0.0)\n        /System/Library/Frameworks/ApplicationServices.framework/Versions/A/ApplicationServices (compatibility version 1.0.0, current version 19.0.0)\n        /System/Library/Frameworks/Carbon.framework/Versions/A/Carbon (compatibility version 2.0.0, current version 128.0.0)\n        /System/Library/Frameworks/Cocoa.framework/Versions/A/Cocoa (compatibility version 1.0.0, current version 9.0.0)\n        /System/Library/Frameworks/WebKit.framework/Versions/A/Frameworks/JavaScriptCore.framework/Versions/A/JavaScriptCore (compatibility version 1.0.0, current version 1.0.0)\n        /System/Library/Frameworks/WebKit.framework/Versions/A/Frameworks/WebCore.framework/Versions/A/WebCore (compatibility version 1.0.0, current version 1.0.0)\n        /System/Library/Frameworks/Foundation.framework/Versions/C/Foundation (compatibility version 300.0.0, current version 500.1.0)\n        /usr/lib/libSystem.B.dylib (compatibility version 1.0.0, current version 71.0.0)\n\nAnd, finally I checked WebCore\n\notool -L WebCore \nWebCore:\n        /System/Library/Frameworks/WebKit.framework/Versions/A/Frameworks/WebCore.framework/Versions/A/WebCore (compatibility version 1.0.0, current version 1.0.0)\n        /System/Library/Frameworks/ApplicationServices.framework/Versions/A/ApplicationServices (compatibility version 1.0.0, current version 19.0.0)\n        /System/Library/Frameworks/Carbon.framework/Versions/A/Carbon (compatibility version 2.0.0, current version 128.0.0)\n        /System/Library/Frameworks/Cocoa.framework/Versions/A/Cocoa (compatibility version 1.0.0, current version 9.0.0)\n        /System/Library/Frameworks/WebKit.framework/Versions/A/Frameworks/JavaScriptCore.framework/Versions/A/JavaScriptCore (compatibility version 1.0.0, current version 1.0.0)\n        /usr/lib/libSystem.B.dylib (compatibility version 1.0.0, current version 71.0.0)\n\nThose are the most likely places for it to be linking to Qt, if it used it, and as you can see, it doesn't use Qt at all.  It does, however, use Cocoa and Carbon, which are the native Mac libraries."
    author: "Tanner Lovelace"
  - subject: "Re: how about native Cocoa KDE port?"
    date: 2004-01-03
    body: "I just love how QT is licensed.\nYou want to write GPL software using it? Go ahead and use the free versions.\nYou want to develop commercial software using it? Pay trolltech a few dollars and go ahead.\n\nThe only problem I see is the lack of a native (no X) free version of QT, but hey! I can live with it and develop to linux or to choose another toolkit.\n\nAnd KDE? Well KDE is a *nix desktop, so why should it care that isn't a free-non-X version of QT for windows?"
    author: "Iuri Fiedoruk"
  - subject: "Re: how about native Cocoa KDE port?"
    date: 2004-01-03
    body: "okay, here is one simple example you cannot do with a lib that is NOT free of GPL:\n\n- free app but no src\n- or free Kapp but no src\n- or even whatever free app + src but BSD\n\nseems that all crossplatform libs/frameworks/langs (except GPL/Qt) support it, i can do this with:\n- java\n- wxWindows\n- Mozilla/Gecko\n- OpenOffice.org libs\n- VCF\n- GTK\n\nbut i cannot do with KDE and not because of the best Linux desktop but because of the GPL. It's sad that KDE is a nice desktop but it's hardly to write something that is 100% free from GPL."
    author: "Anton Velev"
  - subject: "Re: how about native Cocoa KDE port?"
    date: 2004-01-03
    body: "Yes, that's what I like about GPL, you MUST give the source.\nIf you don't like this fact, use another thing, develop for windows, whathever.\nThe fact is that this simply promotes KDE plataform as a opensource (GPL) platafform.\n\nAnd you still can make those free-without-source apps, but you have to pay the QT license to trolltech. Isn't great way of promoting GPL software? I belive so.\n\nI really really can't understand why people don't opensource things. I belive most of them dream to someday sell the software.\n\nNow you can say that your company dosen't like opensourceing things, ok, well tell them to buy a copy of commercial QT or use gtk, java, wxwindows, win32api, etc."
    author: "Iuri Fiedoruk"
  - subject: "Re: how about native Cocoa KDE port?"
    date: 2004-01-03
    body: "> Yes, that's what I like about GPL, you MUST give the source.\n\nok, so then don't mess the GPL with the definition of freedom, while using words like \"MUST\" or \"PAY\". True freedom is Apache, BSD, X, MIT etc (and is some metrics LGPL) because I am \"FREE\" to choose the license of my opinion for my work without asking or sponsoring anyone, being my work licensed under BSD, GPL or whatever.\n\n>The fact is that this simply promotes KDE plataform as a opensource (GPL) platafform.\n\nI am happy to use KDE when booting Linux, but when using UNIX prefer the OSX. However Linux looks to me as a more properitary platform (at least KDE) even more than OSX and Windows, since I don't have the freedom I already mentioned.\n(just to make a note, I am free to make a native OSX apps - in whatever license I prever, without asking or sponsoring someone or some company)\n\nYou see the difference KDE, OSX and Windows are all very nice but only KDE lacks the freedom we are talking about, and to be more exact again \"freedom from GPL\"."
    author: "Anton Velev"
  - subject: "Freedom of Software protected by GPL -- not BSD!"
    date: 2004-01-03
    body: ">> True freedom is Apache, BSD, X, MIT etc (and is some \n>> metrics LGPL) because I am \"FREE\" to choose the license \n>> of my opinion for my work without asking or sponsoring \n>> anyone\n\nMr. Velev,\n\nwe have a very differeent point of view here.\n\nYou are in effect saying that true freedom is if you can ask to receive a gun as a gift from anyone and are free to choose the terms of its usage on your own, even to point it at the donator, solely at your own will and if you like.\n\nNo-no-no, Mr. Velev! True freedom can only persist if it is protected. The GPL guarantees that all derived works remain under the GPL, thus: remain Free and remain under GPL protection.\n\nAs you very well know, the BSD license doesn't guarantee that all derived works remain under a Free license. You even acknowledge that indirectly because you are asking for a change of our prefered license: you insist on getting something from us which you want to receive without any exchange value. \n\nIn fact, you do it even receive for free *now*. KDE is for you today \"without any exchange value\" for your personal use. But you keep asking for more... Therefor you are what I'd like to call a truly unmodest and barefaced guy.\n\nYes, the KDE project has chosen the GPL license for many parts of its \"product\". KDE wants to be \"sponsored\" by those who use our work and build their own work on top of ours. We do admit to this... \n\nAfter all, KDE generously plays an \"in advance\" sponsoring role in the first place: you are free to use KDE without payment, without saying Thankyou and without asking us at all. Only if you try to build your own product (commercial or not, gratis or not) on top of KDE source code, only if you want to found your own personal fame or create cash profit by writing software of your own which you want to distribute. --  only then we want \"something back\": the source code of your derived work. We think this is a very reasonable and very modest expectation.... Do you forget that this source code would contain *our* source code to a large part? \n\nAnd this our claim you dare to call unreasonable? \n\nI find your continuous demand, after providing you with an initial gift, to now provide you with an even larger gift which you are free to use against us... well, really impertinent!\n\nSince we, the KDE contributors are the creators of our product, please leave us our freedom to decide about our licensing terms.\n\nWe do leave *you* the freedom to use a different license yourself, and to derive your own work from the work of developers who choose BSD, X or MIT licenses. We respect your decision. Please do also respect ours.\n\nIn the meantime, have fun to use KDE, make any profit you like by using KDE -- but give us the sources back if you use our world-class quality sources to create a derived product of yours!.\n\n"
    author: "Kurt Pfeifle"
  - subject: "Re: Freedom of Software protected by GPL -- not BS"
    date: 2004-01-03
    body: "Ok, just don't speculate with the word 'FREEDOM' because\n\"enforced freedom is not freedom\"\nand freedom is something more than that, and is more like:\n- freedom of choice\n- free market\n- free speach\n- freedom to innovate\nand i will something from myself:\n- freedom from GPL"
    author: "Anton Velev"
  - subject: "Re: Freedom of Software protected by GPL -- not BS"
    date: 2004-01-03
    body: "Mr. Velve, the GPL is not about the freedom to take (like BSD) but the freedom to give. The concerned audience is not the one you are representing by asking and begging for specific features, but the audience which actually gives away their own work for free, that is programmers and other kind of contributors. Those have no interest whatsoever in others having the freedom to take away their work for free, instead they are concerned to see their work to stay free. This is exactly what the GPL does. You are clearly wasting your time here."
    author: "Datschge"
  - subject: "Re: Freedom of Software protected by GPL -- not BS"
    date: 2004-01-03
    body: ">>GPL is not about the freedom to take (like BSD) but the freedom to give\n\nI would modify that sentence to read like this:\n\nGPL is not about the freedom to only \"take\" (like BSD), but the freedom to \"give and take\""
    author: "Kurt Pfeifle"
  - subject: "Re: Freedom of Software protected by GPL -- not BS"
    date: 2004-01-04
    body: "I chose \"give\" since GPL is strictly a source distribution enforcement license."
    author: "Datschge"
  - subject: "Re: Freedom of Software protected by GPL -- not BS"
    date: 2004-01-04
    body: "OK -- I can see your point\n\nMy \"give and take\" is meant to express that the GPL is more \"balanced\", in that it asks, in a way, for a return from the receiver of Free Software, should he more than just use it (re-distribute modified or un-modified)."
    author: "Kurt Pfeifle"
  - subject: "Re: Freedom of Software protected by GPL -- not BS"
    date: 2004-01-09
    body: ">Mr. Velve, the GPL is not about the freedom to take (like BSD) but the freedom to give.  ...  they are concerned to see their work to stay free.\n\nSorry, the GPL is not just about seeing the existing work stay free.  That's an acceptable summary for the LGPL, AFSL 2.0,  and quite a number of other open source licenses.  If all you are concerned about is preventing open source software from being usurped, then there are plenty of licenses beside the GPL that will achieve this goal.\n\nThe GPL is about taking and increasing the amount of software bound by the GPL.  This is exactly what the GPL does.  Merely linking to a GPL'ed lib makes your entire application GPL'ed.  Please read the GPL FAQ:  http://www.gnu.org/licenses/gpl-faq.html#IfLibraryIsGPL  \n\nYou may like the viral aspects of the GPL.  That's fine.  Other's don't and for many of them, it is indeed a problem.  But please, don't cast the GPL as just protecting open source and definitely don't portray it as the only viable option.\n\n>You are clearly wasting your time here.\n\nIf you are chosing to ignore the reality of the GPL, or intentionally misrepresenting the GPL so it can infect other code, or actually feel that the work of others somehow automagically belongs to everybody, simply because they link to a GPL'ed library, then yes indeed he is wasting his time."
    author: "Ray"
  - subject: "Re: Freedom of Software protected by GPL -- not BS"
    date: 2004-03-11
    body: "From a philosophical perspective all software functionality will eventually become \"free\".  GPL is merely a catalyst for the inevitable.  1-1=1"
    author: "Mike"
  - subject: "Re: Freedom of Software protected by GPL -- not BS"
    date: 2004-01-03
    body: "Mr. Velev,\n\nI clearly don't want to share the your kind of freedom: you are putting a lot of different things into the same bag and tell me that they belong together.\n\nI am all for \"freedom of choice\". But I also am aware: any \"freedom of choice\" in our type of society and environments must have also \"limits\".\n\nIf you'd use your \"freedom of choice\" to do me harm, I will fight back. I assure you, that wouldn't be pleasant for you nor for me, nor for the rest of us.\n\nThat's why sane people voluntarily restrict themselves to not use freedom of choice to their own personal gusto and advantage alone. I am sure you are one of these sane people...  ;-)\n\nBut not all are nice as you. There are not just sane guys and gals on this evil world. That's where rules and law and law enforcement comes into play. And the bending of rules and laws And power and even dictatorship. \n\nWe choose to use some rules under which our software may be used, and ask users to agree to these rules. These rules are *very* liberal. These rules even include a big degree of \"freedom of choice\", \"free market\", \"free speech\", \"freedom to innnovate\", believe it or not.\n\nYour \"freedom from GPL\" would stop protecting the above freedoms. Your \"freedom from GPL\" denies my freedom to choose the GPL.\n\nYour are wasting your time. And mine. I, for my part, will now stop wasting time. \n\nHave a nice time with using KDE on Linux, *BSD, *nix or Mac OS X!\n"
    author: "Kurt Pfeifle"
  - subject: "Re: Freedom of Software protected by GPL -- not BS"
    date: 2004-01-03
    body: "Judging from your posts. Your \"true freedom\" seems like anarchism to me."
    author: "Dude"
  - subject: "Re: Freedom of Software protected by GPL -- not BS"
    date: 2004-01-25
    body: "Freedom errrm. Speach is not speech. I get pissed off with the inability of the new world to speak english. Please -- doesn't KDE have a spell checker ?\nI use Macos X. I love KDE.\nsorry\n "
    author: "ghostdoguk"
  - subject: "Re: Freedom of Software protected by GPL -- not BSD!"
    date: 2004-01-03
    body: "> The GPL guarantees\nWell, basically it guarantees that people don't behave like scumbags.\nIf you want to feed upon thousands of other peoples sweat, why not give them _something_ back? Of course, Novell/Sun/Whatever do not want to give. Basically, they are driven by the greed of their stock owners, which in many circumstances is good, and generates jobs etc, but now...well."
    author: "OK"
  - subject: "Re: Freedom of Software protected by GPL -- not BSD!"
    date: 2004-01-03
    body: "I could (and didn't had patience) for saying it better.\n\nBasically Trolltech have the freedom to choose the license they want for QT, and KDE have the freedom to choose the toolkit they wish, and we have te freedom to develop using libs, toolkits and plataform we like most.\n\nSaying that KDE *must* change it's license because someone dosen't like how it works, is simply saying that KDE or Trolltech or me or you dosen't have the right to choose a license that they/we belive is better, what is cutting or freedom of choise.\n\nI choise GPL for all my free/non-commercial software because I like the fact that no one can use or fork my code without releasing the code ahead.\n\nSo, let's live with the Trolltech and KDE team choises and respect their desision. If you don't like this, please, go ahead and use something else.\n\nAnd about companies not using QT/KDE that not what I'm seeing, see The Kompany, IBM and Adobe examples. Yes, Sun and others are choising Gnome over KDE, but that's their choise that I respect, as I respect the users that get those systems and install KDE on it (see solaris example),"
    author: "Iuri Fiedoruk"
  - subject: "Re: Freedom of Software protected by GPL -- not BSD!"
    date: 2004-01-06
    body: "> Yes, the KDE project has chosen the GPL license for many parts of its \"product\".\n\nWhile it's true that most KDE _applications_ are released under the GPL, the KDE _libraries_ (i.e. kdelibs) are (currently) all released under the LGPL (or similar licenses). So people who want to write proprietary KDE applications are free to do so provided they pay TrollTech for using Qt.\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: how about native Cocoa KDE port?"
    date: 2004-01-08
    body: "GPL protects the freedom of the app itself, not the derived app. E.g. when we say that Qt is under GPL, it protects the freedom of Qt and not the freedom of your app that uses Qt."
    author: "Anonymous"
  - subject: "Re: how about native Cocoa KDE port?"
    date: 2004-01-09
    body: "WRONG!  If the lib is GPL'ed and you link to it, your app is now infected and GPL'ed.  Like it or not, but using a GPL'ed lib, you have been forced to donate your app to the great Open Source community.\n\nIf the lib was LGPL'ed, then you're only obligated to return your changes to the lib back to the community.\n\nThe later is fair, because you're returning changes you've made to the open source lib back to the community.  The former isn't, because the GPL has infected your app and the cost isn't keeping the lib and any improvements free and open, but a forced donation of your effort to expand the amount of free and open software under the viral GPL."
    author: "Ray"
  - subject: "Re: how about native Cocoa KDE port?"
    date: 2004-01-09
    body: "<I>Yes, that's what I like about GPL, you MUST give the source.</I>\n\nAnd that's the problem with the GPL for many.  It's viral.  If I link my code with a GPL'ed library, my code is infected and become GPL'ed.  I'll gladly give my changes to the GPL'ed library back to the community, but I won't have some GPL'ed code stealing my effort.  I have no problem with the LGPL, but the viral effects of the full GPL make it something to avoid."
    author: "Ray"
  - subject: "Re: how about native Cocoa KDE port?"
    date: 2004-01-10
    body: "Yes, I understand that this is a problem for some people.\nThe good news is that you can choose not to use GPL libraries :)\n\nSure, this could lead to people not using KDE for business (qt should not even be discussed here because of the non-free version), but does it really matters? That's the thing, being GPL KDE promoves GPL software. As integration between KDE and QT is increasing, you can even write a non-GPL QT app that looks like a KDE app. (I know there are discussion to make even the file dialogs looks the same).\n\nSo, I see no problem with KDE licence. Yes you can't (in theory, I don't know how the kompany does) write a KDE app that's not GPL (that is not by all means a bad thing), but you can write a QT app that will look very like the KDE programs without it being GPL. So, what's the problem?"
    author: "Iuri Fiedoruk"
  - subject: "Re: how about native Cocoa KDE port?"
    date: 2004-01-03
    body: "You can write BSD-licensed software and link to GPL'ed libraries. Just don't copy any GPL'ed code into your source.\n\nIf you're going to bitch, at least get your facts straight.\n"
    author: "Rayiner Hashem"
  - subject: "Re: how about native Cocoa KDE port?"
    date: 2004-01-09
    body: "Pot-Kettle-Black.  If you're going to defend, at least get your facts straight.\n\nYou can't do what you're claiming.  See:  http://www.gnu.org/licenses/gpl-faq.html#IfLibraryIsGPL\n\nThe GPL is viral.  You link to a GPL'ed library, your entire program is now bound by the GPL.  It's funny how many GPL supporters don't even know the GPL and can't be bothered to read the GPL FAQ."
    author: "Ray"
  - subject: "Re: how about native Cocoa KDE port?"
    date: 2004-01-06
    body: "> - free app but no src\n> - or free Kapp but no src\n\nIf \"free\" means \"as in beer\" then you are right. But for most of us \"free\" means much more: http://www.fsf.org/philosophy/free-sw.html\n\nIf you anyway want to give an app away for free (as in beer) then I fail to see why you wouldn't want to give away the source code as well except for egoistic reasons. Why do you want to hinder other people from collaborating with you to make your app much better than you could ever make it on you own? Why do you want your app to be doomed to die if you someday stop improving it?\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: how about native Cocoa KDE port?"
    date: 2004-01-03
    body: "> ...a way out from the already very serious GPL issue...\n\nWhat on earth are you thinking?\nYou can not seriously believe that LGPL:d libraries are good for open source software/community in the long run.\n\nYes, there are companies who prefer the LGPL to GPL, do you understand why?\n\nThese people wants to use what is free without giving back to the free community. In the future, their own applications, built on free libraries, will make competition impossible as they outrun the free/open source apps. Don't you see that this can possibly lead to a new Microsoft monopoly on the on Unix/Linux?\n\nIs this really just and fair? Is this what you want?"
    author: "OI"
  - subject: "Re: to all"
    date: 2004-01-04
    body: "Okay, it seems that you obviously don't understand what I mean and some people seem to be right that I am losing my time on this.\nHere is my vision again: kde is a nice desktop, but the GPL makes it uncompetitive (compared to some other toolkits (wxWindows, VCL, VCF, mozilla) and desktops (OSX, Windows, Gnome)).\nKde is the best desktop for Linux.\nGDI+Explorer is the best desktop for Windows.\nPanther is the best desktop for UNIX.\n\nAlthough some may not want to share my vision again the issue is GPL and a better freedom and arguments like forcing and/or protecting freedom (which decreases the freedom) means to me selfishness and egoism.. anyway this is a free market and only the free market itself will prove what is right and what is wrong. Don't get me wrong, KDE is already a free in some way (in terms that kde libs are LGPL), and for example if someone ports it to GTK or other GUI toolkit will remove the GPL dependency (so it is not an issue about KDE itself). I am more worried of the fact that enterprise (mostly in US) is adopting GTK/Gnome instead of the supperior Qt/KDE and it happens not because of it's better technology but because of some GPL dependencies.\n\nAbout this topic (freedom from GPL) I understand that it's not the right place right here to discuss, so may be there is a good forum about it somewhere else. At least some people are not happy with the vision and facts I share.. anyway.\n\nNo need to opose this (i am not going to respond), I know you know what I mean and that my arguments are strong. Being me right or not the free market will prove with the time, and I believe that the free market is the best way to adapt the right way (just my oppinion is that neither Monopoly neither GPL are the right way).\n\nI am still around, and will use KDE when i boot Linux, and still will demonstrate my friends what is Linux by showing them KDE."
    author: "Anton Velev"
  - subject: "Re: to all"
    date: 2004-01-04
    body: "You confuse too many things in a single sentence. And you present many facts in a wrong way. Your comment requires a response so that new reader aren't too easily confused, though.\n\n\n[\"paraphrasing Mr.Velev\"]\n>>>>>>>>>>\n--> countering Mr. Velev's point\n(explaining a bit of background)\n\n\n[\"KDE competes with wxWindows, VCL, VCF, Mozilla\"]\n>>>>>>>>>>\n--> KDE is a Desktop Environment. VCL, VCF, wxWindows are toolkits. Mozilla is a Browser.\n(KDE is built on the Qt toolkit. Mozilla is built on the XUL toolkit. VCF is still relatively new, immature and unknown: http://vcf.sf.net/. VCL is Borland's toolkit, used e.g. in Delphi.)\n\n\n[\"The GPL makes KDE uncompetitiv\"]\n>>>>>>>>>>>\n--> Currently KDE is the most popular Desktop for Linux users, because it is the best, it will remain so. \n(All polls are proof to that. In Europe. In the Americas. In Asia. KDE achieved its leading position *with* the GPL.)\n\n\n[\"KDE is uncompetitive compared to OSX\"]\n>>>>>>>>>>>\n--> KDE doesn't even really *try* to compete on OSX.\n(KDE's appearance on OS X has hardly begun. It will continue to make big progress on OS X. But this is a mere \"side-effect\" of KDE's overall success.)\n\n\n[\"KDE is uncompetitive compared to Windows\"], part1\n>>>>>>>>>>>\n--> It doesn't even really *try* to compete on Windows platforms.\n(KDE's appearance on Windows platforms has hardly begun. However, you *may* see some progress here too. But this is a mere \"side-effect\" of KDE's overall success.)\n\n\n[\"KDE is uncompetitive compared to Windows\"], part2\n>>>>>>>>>>>\n--> The real competition is between *Linux* and Windows. KDE contributes a major part to make Linux competitve to Windows.\n(Most migrations of companies and organisations away from Windows desktops and towards Linux desktops will be using KDE.)\n\n\n[\"KDE is uncompetitive compared to Gnome\"]\n>>>>>>>>>>>\n--> KDE is the leading desktop platform for Linux and will remain so.\n(KDE will turn out to not only as the best *integrated* desktop, but also the most *integrative* desktop. You will see Gnome apps, OpenOffice, Gimp, Sodipodi, Mozilla etc. using KDE icons, KDE colors, KDE themes  --  and more importantly! -- KDE print & fileopen dialogs and KDE ioslaves. User experience will become much more complete, smooth and pleasant. It will remove all \"newbie-confusion\" resulting from seeing different types of dialogs in different desktop applications.)\n\n\n[\"Panther is the best desktop for UNIX\"]\n>>>>>>>>>>>\n--> Panther is not running on all UNIXes, only on Apple's version (OS X).\n(KDE is running on all UNIXes, including OS X.)\n"
    author: "Kurt Pfeifle"
  - subject: "Re: to all"
    date: 2004-02-11
    body: "Where cn I find KDE for Panther OS x (10.3)\n\n"
    author: "james coffey"
  - subject: "Re: to all"
    date: 2004-02-11
    body: "Look at http://kde.opendarwin.org/.\n\nI am not sure if Panther is supported already, most work was on\nJaguar recently. Panther support will come soonish, I guess.\nThe port started only over X-mas, and possibly the developers \njust don't own Panther yet.\n\nTry to get in touch with them (mainly \"the 2 Bens\") over mail \naddresses you find on that link above, or IRC.\n\nCheers,\nKurt "
    author: "Kurt Pfeifle"
  - subject: "Re: to all"
    date: 2004-01-04
    body: "Essentially the GPL is a way of getting payment in some way for one's code. It does limit, and take away freedoms by demanding a contribution if you change the code and distribute it.\n\nWhat is interesting is how the big guys have adopted the GPL as protection. IBM contributes quite generously to the kernel, knowing that their competition, ie. Sun, Dell, HP, SGI and others will use the kernel to benefit their business. And in return, IBM benefits from the contributions of the others, and no-one can take anything and build a proprietary kernel. The protections are for contributors, users and the market as a whole. Seeing these hard-nosed competitors working in this arena is quite an accomplishment, and will be subject of thesis' I'm sure.\n\nAs for the desktop, it really does come down to technology. If Trolltech has a compelling product, people will use it even if it costs (money or gpl). The same with KDE and GTK. Everyone involved knows the stakes, and are furiously coding to get some advantage. Sounds like a healthy market to me.\n\nFunny that you mention market. Right now, someone could correct me if I'm wrong, but Trolltech is probably the only linux desktop company that is profitable. SUN and Novell, who are claiming 'ownership' of the linux desktop are both shrinking and losing marketshare. This is an attempt by both of them to establish relevance in the marketplace. I'm very curious whether the Sun sales people, used to high margin hardware, are even making commissions on the very low margin desktop offerings. The linux desktop is just beginning to be commercially viable, and will probably have a solid place in three to four years. Who of these will still be around?\n\nDerek (who, when hockey and BC politics get boring, watches the IT industry for entertainment)"
    author: "Derek Kite"
  - subject: "Re: to all"
    date: 2004-01-09
    body: ">Essentially the GPL is a way of getting payment in some way for one's code. It does limit, and take away freedoms by demanding a contribution if you change the code and distribute it.\n\nIt demands more than that.  It demands that you donate your ENTIRE application to the GPL community.  If you link to a GPL'ed library, your ENTIRE program is bound by the GPL.  See:  http://www.gnu.org/licenses/gpl-faq.html#IfLibraryIsGPL  Even if you don't change a single line in the lib.  The LGPL and other open source licenses protect the library, but the full GPL infects every application it touches.  The only exceptions are applications that produce something (e.g. compilers, source code generators, etc.), in that case, the output isn't automagically GPL'ed, and a very few select sets of librtaries that have to be exempted (glibc, etc.), because otherwise everything built with these compilers, linkers, etc. would be GPL'ed and there would be no commercial software developed with gcc, etc.\n\n\n"
    author: "Ray"
  - subject: "Keep the GPL flying"
    date: 2004-01-02
    body: "I'm glad to use KDE *also* (not only) because it's a GPL-based desktop.\nI think we must fight to build an ass-kicking ***free*** software desktop, not fight to make it another non-free desktop.\nIf you want an ass-kicking, non-free desktop, there's Aqua for you ;)"
    author: "cyclop"
  - subject: "cross platform compatible?"
    date: 2004-01-03
    body: "Dumb question:  Isn't QT supposed to be fully cross platform compatible?  For the uninitiated, what sort of stuff needs to be ported?  "
    author: "anonymous"
  - subject: "Re: cross platform compatible?"
    date: 2004-01-03
    body: ">> Isn't QT supposed to be fully cross platform compatible?\n\nIt is.\n\nBut \"mistakes\" of a developer could have inserted Linux-specific assumptions into the code design. Or some bugs may only show up on a specific platform. Or the darn thing just doesn't compile on the first shot and needs fixing...\n\nProbably very little needed to be \"ported\" in the strict sense of the word...\n\nWhy don't you ask RangerRick or icefox?  ;-)"
    author: "Kurt Pfeifle"
  - subject: "Re: cross platform compatible?"
    date: 2004-01-03
    body: "AFAIK there were some direct xlib calls in the KDE libraries themselves, it has been a case of removing those. At least that's what I understood."
    author: "agsnu"
  - subject: "Re: cross platform compatible?"
    date: 2004-01-03
    body: "While it is possible to write wholly cross-platform code with Qt, it's also possible to call X11 stuff directly, and that happens quite a bit in the KDE code.  Those bits need to either be removed or reimplemented when porting to Qt/Mac (or, in theory, Qt/Win32).\n\nRight now much of what we're doing is the \"remove\" part, hopefully soon things will be working enough that it's time to start filling in the \"reimplement\" part.  ;)"
    author: "Ranger Rick"
  - subject: "Re: cross platform compatible?"
    date: 2004-01-03
    body: "For instance, I've been looking into kdegraphics and to just give one example, kuickshow uses the imlib library which links into X11 directly.  That will have to either be replaced or reimplemented in order for kuickshow to work on the mac.  I had hoped that imlib2 might offer some hope, but it seems to need the X libraries too."
    author: "Tanner Lovelace"
  - subject: "Re: cross platform compatible?"
    date: 2004-01-03
    body: "The GPL Qt works only on X11 (and on a Unix-like system.) So you have to port at least the X11 part to the other window system.\n\n(And to repeat another poster: that has nothing to do with the pay versions of Qt, which already exist for a few systems (and were ported by Trolltech.))\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: cross platform compatible?"
    date: 2004-01-03
    body: "Forget what I have just written. Qt/Mac is indeed from Trolltech *and* GPL.\n"
    author: "Nicolas Goutte"
  - subject: "Koffice will only be usefull when the filters work"
    date: 2004-01-05
    body: "This could increase the number of potential developpers and users  tenfold. \nBut people will only start to see the benefit of using Koffice when the import filters work as good as those of openoffice. \n\n"
    author: "Geert"
  - subject: "Ranger Rick..."
    date: 2004-01-05
    body: "...By chance are you the Ben Reed from Bloomington, IL?"
    author: "Area Man"
  - subject: "Re: Ranger Rick..."
    date: 2004-01-05
    body: "I used to live in Bloomington, IL, yes.  I moved to Raleigh a few years ago."
    author: "Ranger Rick"
  - subject: "Re: Ranger Rick..."
    date: 2004-01-06
    body: "When you were a sophomore in high school, did you where a Janes Addiction - Nothing's Shocking shirt on a regular basis?"
    author: "Area Man"
  - subject: "Re: Ranger Rick..."
    date: 2004-01-06
    body: "Yeah, that was me.  Who is this?  =)"
    author: "Ranger Rick"
  - subject: "Re: Ranger Rick..."
    date: 2004-01-07
    body: "hah, freakish."
    author: "anon"
  - subject: "Re: Ranger Rick..."
    date: 2004-01-09
    body: "Just an area man who enjoyed Mrs. Sutter's Chemistry class with you.  Wasn't she nasty?\n\nYou know Matt Saulcy?  If so, Know how to get a hold of him?"
    author: "Area Man"
  - subject: "Koffice for My iBook"
    date: 2004-03-26
    body: "I read somewhere that I can run Koffice native in Mac OS X, and here it looks like some have done that.  What do I need to do - I'm new.  I looked around the KDE website and wasn't sure which items to download.  Help?\n\nMany thanks."
    author: "danny"
  - subject: "Re: Koffice for My iBook"
    date: 2004-04-09
    body: "Danny,\n\nhave a look here: http://ranger.befunk.com/blog/\n\nThere's, at least on my Powerbook with 10.3.3, a problem: several KDE apps don't have a menu bar, others have.\n\nThomas"
    author: "schlesi"
  - subject: "installer"
    date: 2005-07-11
    body: "apparently therers at least 1 stable koffice, or atleast somethings stable, where can i get an installer? now I know that this is a linux program, and so is made as difficult as possible to install, but mac users arent lik you linux users. we like \"Simply drag the icon to the Applications folder\"or \"click next to install\". So whats the point of porting to Mac if mac users cant install it?"
    author: "addythegeek"
  - subject: "HELP!"
    date: 2008-11-07
    body: "I've downloaded the base... How do I get the apps?\n\n\u00df\u00df\u00df"
    author: "Aaron"
---
A few days ago <A HREF="mailto:ranger@befunk.com">Benjamin Reed</A>, a.k.a. RangerRick and <A 
HREF="mailto:ben-devel__*NO*_*SPAM*__@meyerhome dot net">Benjamin 
Meyer</A>, a.k.a. icefox, <a href="http://ranger.befunk.com/blog/archives/000290.html">succeeded</a> in making <A 
HREF="http://konqueror.org/">Konqueror</A>, the KDE swiss army 
knife, <A HREF="http://ranger.befunk.com/screenshots/qt-mac-konqueror-20031229.png">run 
natively on Mac OS X</A>. Now <a href="http://ranger.befunk.com/blog/archives/000291.html">they have an update</a>: <A 
HREF="http://koffice.org/">KOffice</A>, <A HREF="http://kate.kde.org/">Kate</A>, 
<A HREF="http://konsole.kde.org/">Konsole</A>, and a few other KDE 
applications work natively on Mac OS X. That means, they don't 
use an X server, like the GUI apps such as those ported 
from Unix and Linux to Mac OS X by the <a href="http://fink.sf.net/">Fink project</a> do, but they base on the native Qt/Mac library to run KDE in the 
native Aqua environment. They have solved all major porting problems, 
with only a few rough edges left to polish. Congratulations on this 
milestone achievement!




<!--break-->
<P>
Screenshots of <A 
HREF="http://ranger.befunk.com/screenshots/qt-mac-kspread-20040101.png">KSpread</A> , 
<A HREF="http://ranger.befunk.com/screenshots/qt-mac-kword-20040101.png">KWord</A>, 
<A HREF="http://ranger.befunk.com/screenshots/qt-mac-konsole.png">Konsole</A>,
<A HREF="http://www.csh.rit.edu/~benjamin/about/pictures/KDE/qt-mac-kdegames.png">various 
KDE games</A>,
<A HREF="http://www.csh.rit.edu/~benjamin/about/pictures/KDE/qt-mac-kdebase.png">KDE Wallet, 
KTips as well as Konqi</A> look pretty cool on Mac OS X. 
Note that the KDE icons also appear in the Apple equivalent of Kicker.

<P>
It seems that RangerRick and icefox have not only satisfied, but have
heightened the expectations now: Mac people seem to be rather keen on also seeing
KDE killer applications like <A HREF="http://kdevelop.org/">KDevelop</A> and <A 
HREF="http://quanta.sourceforge.net/">Quanta</A> ported to Mac OS X...  ;-)



