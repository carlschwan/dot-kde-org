---
title: "aKademy Interview: Will Stephenson of Kopete"
date:    2004-08-28
authors:
  - "ateam"
slug:    akademy-interview-will-stephenson-kopete
comments:
  - subject: "much to work on"
    date: 2004-08-28
    body: "there is really much to work @ kopete like video stuff and phone and games and filetransfer and so on - but nice interview , the new contact list rocks !!!"
    author: "chris"
  - subject: "Jabber group chat"
    date: 2004-08-28
    body: "I have one question: I see in the KDE 3.3 Feature Plan that Jabber group chat support is added to KDE 3.3. But I have no idea how to use it. I want to chat with a few friends in one chat session, is this possible with Jabber group chat support?\n\nI could not find anything in the Kopete gui that looked like an option for groupchat. Please can somebody tell me how to chat with a few people at the same time (in the same session) with Kopete.\n\nThanks a lot !!!"
    author: "AC"
  - subject: "Re: Jabber group chat"
    date: 2004-09-02
    body: "Just right-click on your account's Jabber icon in the status bar and choose \"join group chat\". You might want to familiarize yourself with how group chats work in Jabber, it's much like IRC and the UI still needs a bit of improvement."
    author: "Till Gerken"
  - subject: "But..."
    date: 2004-08-28
    body: "...I thought all this integration was only happening with Gnome applications?"
    author: "David"
  - subject: "Re: But..."
    date: 2004-08-29
    body: "Somebody (from a specific Novell department) wants to make you believe that but it's not true."
    author: "Anonymous"
  - subject: "Re: But..."
    date: 2004-08-29
    body: "Hmmm. I wonder who that is?"
    author: "David"
  - subject: "Great Work!"
    date: 2004-08-28
    body: "Thanks to all the kopete hackers.  As of 3.3 I think it's at a state where I can use it.  I've been using aMsn for a long time because of its superior support for things like buddy icons and file transfers, but kopete has almost caught up there and the fancy integration won me over.\n\nThere is really only one issue that may force me back to aMsn.  MSN file transfer now supposedly works but I haven't gotten it to work behind a firewall (the configuration of which I have no control over).  In Amsn I can't send files but at least I can receive them.\n\nAnd my bet wishlist item\nhttp://bugs.kde.org/show_bug.cgi?id=75973\n\nOtherwise I'm very happy with Kopete now.  Great work guys!  Thank you.\n\nPS.  One more thing (I'm such a nagger), RAM usage is really high for an instant messenger.  On my system kopete uses just as much ram as all of Kontact!  \nTop says:\n2498 leo       16   0 74340  35m  38m S  0.0 14.1   0:29.67 kopete"
    author: "Leo S"
  - subject: "Re: Great Work!"
    date: 2004-08-29
    body: "Don't use top to determine memory usage. you're supposed to use something in /proc (if using linux) but i don't remember what. Kopete's memory usage isn't nearly that bad, but there's definately room for improvement in places."
    author: "Matt Rogers"
  - subject: "fade effects on list"
    date: 2004-08-29
    body: "the fade effect on contact list is really nice !\ncouldn't this be backported to the whole kde desktop as an eye candy option in the control panel ? :)"
    author: "polux"
---
As <A href="http://conference2004.kde.org/sched-userconf.php">UserConf</A> 
has kicked off today at <A href="http://conference2004.kde.org/">aKademy</A> we had 
the privilege to sit down with Will Stephenson, 
a prominent hacker who works on the <A href="http://www.kopete.org/">KDE instant messenger Kopete</A>. 
We talked about Kopete, its current status and future plans for Kopete and Will had some very good news for us...







<!--break-->
<p><IMG src="http://kopete.kde.org/img/kopete_banner.png"  align="right" border="0"></p>

<p><strong>Please introduce yourself.</strong></p>

<p>My name is Will Stephenson. I'm from Newcastle upon Tyne, Britain. I'm
29 years old, I've been a KDE hacker for 3 years now and I've been
using KDE since KDE 1.1.</p>

<p><strong>What's your role within the KDE project?</strong></p>

<p>Kopete is the only project I really work on, but I've done a bit of
PIM moonlighting recently. I moved to Kopete from Licq because more
and more of my friends were using MSN and I was getting left out. I
started to contribute by drawing icons for Kopete. I then wrote a
couple of plugins (Now Listening and Web Presence) that got me the
reputation of being the team spammer, got hooked and got more and more
involved in Kopete development.</p>

<p><strong>Are you paid for your work on Kopete?</strong></p>

<p>Currently I'm in the fortunate position to be paid by SUSE for my Kopete
development as a summer job.</p>

<p><strong>Where does the name Kopete come from?</strong></p>

<p>As the original author, Duncan Mac-Vicar, is from Chile, the word <em>Copete</em> is a Chilean word for having a drink among friends.</p>

<p><strong>How many people are working on Kopete?</strong></p>

<p>At any one time there are about 5 or 6 active major contributors. We come
and go as we have time. And we have a pretty vocal community of active users 
and patch contributors whole help a great deal to keep the bugs down.  There 
usually about 20 people in the IRC channel.</p>

<p><strong>What's new in the Kopete version that comes with KDE 3.3?</strong></p>

<p>Well, the first thing you'll notice is the new contact list, that is the main window you see when you start Kopete. It's a great piece of work - we now have animation, fading and different layouts. It's very sophisticated inside and is highly very customizable. Since we are a KDE project we believe in integration, not a totally skinnable interface, but we like to offer our users the chance to make Kopete suit their needs within the bigger KDE picture.</p>

<p>We've got a new contact information system, which brings together
all the bits of data like phone number and email addresses and allows
us to show them in a standard form instead of having different user
interfaces for each IM system, this allows things like really
informative tooltips and linking to the KDE addressbook, so that you
can receive an email address from MSN and store it in your address
book.</p>

<p>Apart from that we've got lots of improvements in the protocol implementations,for example MSN now has support for receiving custom emoticons and files from the latest Windows MSN Messenger 6. Jabber and Gadu-Gadu got file transfer support. There's been a great deal of polishing and bug fixing too, ICQ and the UI generally have benefitted especially here.</p>

<p>Users will also like the new custom notifications, known elsewhere as
buddy pounces. That means that for each contact's events, like coming
online you can set individual notifications. You can also set custom 
notifications per group, so you can get one notification if any of 
your work colleagues come online, and another if it's one of your friends.</p> 

<p>The main feature I've been working on has been the integration with the 
rest of the KDE desktop, like integration with KMail and Konqueror; in KMail you can start a chat with an email's sender, and if you install the Kuick plugin from kdeaddons you can copy files directly from Konqueror to an instant messenger contact via file transfer. Kopete will then automatically choose the correct IM system to transfer the file. All of this is built 
around the Kimiface interface I wrote for this, which means that any instant messenger application can  implement this interface so they gain integration with those applications very easily, without having to write code specifically for each pair of applications. We are beginning to see KIMIface support in Konversation now which is really exciting.</p>

<p><strong>What are the plans for future versions of Kopete?</strong></p>

<p>For a start, all the things we didn't have time to get in 3.3! 
The file transfer GUI and internals will be getting cleaned up too to
make it easier to send files in a standard way from within Kopete, and
we're talking about cleaning up the context menus so they are more
streamlined.</p>

<p>From a developer point of view we want to create a standalone libkopete
which is usable for other applications and which may be used on other operating systems as well. There are a lot of other internal clean ups we would like to make but they probably only excite the development team, ask us in #kopete if you'd like to know more or help out.</p>

<p>I'd also like to expand the KDE addressbook integration, so that all contacts are stored in the KDE addressbook which will provide great integration for enterprise systems where you can have your shared work contact list on an LDAP server for instance.</p>

<p><strong>How do you think Kopete stands against Gaim?</strong></p>

<p>Well, Kopete is the Gaim counterpart for the KDE desktop.
Kopete aspires to be for KDE what Gaim is for GNOME. There are good 
relations between the two projects, they help us out if we need to 
know something; they're a bunch of nice guys. We've used some of 
their libraries and protocol specifications.</p>

<p><strong>During aKademy it was rumored that a GroupWise connector for Kopete
was in development. It this true, and if so, can you tell us more
about that?</strong> </p>

<p>Yes, it's true. As you'll know SUSE is a part of Novell and Novell offers
a lot of enterprise collaboration solutions, one of which is the GroupWise
enterprise instant messaging service. And as a leading Linux distribution SUSE aims to offer the full range of features on the KDE and GNOME desktops. And this is the reason I'm now working at SUSE, to provide this functionality under the KDE desktop, by using Kopete.  GroupWise offers features which are very interesting for enterprises.  Specifically secure messaging and central archiving services, which means messages are stored centrally and can be retrieved anywhere.  It is strongly integrated with the central Novell 
address book, so you always know the identity of the people you're talking to.</p>

<p><strong>Do you have anything to add to this interview?</strong></p>

<p>At Kopete we value all kinds of contributions, we can always use people who like to join our development team. And I'd like to thank you for interviewing me.</p>

<p><strong>Thank you for answering our questions in this interview.</strong></p>
