---
title: "Trolltech Releases First Qt 4 Technology Preview"
date:    2004-07-08
authors:
  - "binner"
slug:    trolltech-releases-first-qt-4-technology-preview
comments:
  - subject: "Performance ?"
    date: 2004-07-08
    body: "Hi\n\nDoes anyone know how is Qt4 going to do in terms of speed ? I think I read some time ago that one of the focuses for Qt4 was to speed up the libraries, besides adding functionality, but I don't see any reference to this in the Public Release info ..."
    author: "MandrakeUser"
  - subject: "Re: Performance ?"
    date: 2004-07-08
    body: "Lots of tunning.  QString, moc etc.  expecting a good bump in startup and speed."
    author: "Ben Meyer"
  - subject: "Re: Performance ?"
    date: 2004-07-08
    body: "Thanks Ben, looking forward to it ! This, together with a standard objprelink for Linux, plus KDE-libs optimizations, will overall get an even snappier KDE !!!!!!"
    author: "MandrakeUser"
  - subject: "Re: Performance ?"
    date: 2004-07-08
    body: "Being slightly pedantic: objprelink has nothing to do with Linux, it addresses an issue with gcc's C++ implementation.  Any system that uses gcc will be subject to the same problem.  If you use a different C++ complier for Linux (does anybody?), you don't have the problem.  And yes, gcc has addressed the problem directly, so it's an issue that will fade away."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Performance ?"
    date: 2004-07-09
    body: ">Being slightly pedantic: \n\nYou are correct in all of this."
    author: "a.c."
  - subject: "Re: Performance ?"
    date: 2004-07-08
    body: "That's what I love about Linux. My old piece of crap computer is getting faster all the time.. From kernel 2.4 to 2.6, from KDE 3.1 to 3.2, etc.\n\nIn a few years I'll have to downgrade :D"
    author: "Mikhail Capone"
  - subject: "Re: Performance ?"
    date: 2004-07-08
    body: "LOL this is exactly my idea about this ;-)"
    author: "superstoned"
  - subject: "Re: Performance ?"
    date: 2004-07-09
    body: "An old P350 with 128mgs of Ram feels snappier with KDE3.3 than with KDE 3.1.\nDoes the upgrade from 2.4 Kernel to 2.6 improves performance, too? If so, I'll certainly upgrade the kernel on this slackware box... "
    author: "Thomas"
  - subject: "Re: Performance ?"
    date: 2004-07-09
    body: "The new kernel is a tad bit snappier on interactive loads, but these improvements has been available for 2.4 as patches for some time. So you may already have them if you are running a distro kernel."
    author: "Jonas"
  - subject: "Re: Performance ?"
    date: 2004-07-09
    body: "SuSE 8.2 with KDE 3.1 and Linux 2.4.20 felt snappier to me then \nSUSE 9.1 with KDE 3.2 and Linux 2.6.4, so I downgraded.\n\n(on a mobile P2 400MHz with 160MB RAM)\n\n\tPaul."
    author: "Paul Koshevoy"
  - subject: "Re: Performance ?"
    date: 2004-08-09
    body: "Well, this is SuSE specific, cause SuSE is a well overloaded Distribution\nwhich (my opinion) is not at all made to achieve the maximum performance!"
    author: "thySEus"
  - subject: "Re: Performance ?"
    date: 2005-01-01
    body: "Slackware doesn't include any of these patches in its default kernels....\n\nTo answer the question one layer up:  Yes, 2.6 speeds things up.  It's a noticable speed increase on all my systems.  It may not like your super-old hardware, though, if it is, indeed, super-old.\n\nRob"
    author: "AthlonRob"
  - subject: "Qt 4"
    date: 2004-07-08
    body: "I heard that Qt 4 would be broken up so that a command-line program could link to it and not have to drag in all the GUI stuff it won't use. Is that still happening?"
    author: "AC"
  - subject: "Re: Qt 4"
    date: 2004-07-08
    body: "yes"
    author: "Ben Meyer"
  - subject: "Re: Qt 4"
    date: 2004-07-20
    body: "maybe this is interesting for you:\n\nhttp://www.uwyn.com/projects/tinyq/"
    author: "kde_benutzer"
  - subject: "can I compile kde 3.x.x against qt4"
    date: 2004-07-08
    body: "I think I've something about, qt4 should be compatible to qt3.\n\nThanks for answers!"
    author: "anonymous"
  - subject: "Re: can I compile kde 3.x.x against qt4"
    date: 2004-07-08
    body: "Yes, but not without some changes to the build environment and possible also the sources. It's meant be easily portable for developers, not to be completely source compatible."
    author: "Daniel Molkentin"
  - subject: "Re: can I compile kde 3.x.x against qt4"
    date: 2004-07-08
    body: "No, qt4 is not binary compatible with qt3."
    author: "Fredrik Edemar"
  - subject: "Re: can I compile kde 3.x.x against qt4"
    date: 2004-07-08
    body: "no.. quite a few changes are needed... which is why kde 4.0 will be a new release"
    author: "anon"
  - subject: "Re: can I compile kde 3.x.x against qt4"
    date: 2004-07-08
    body: "Well, it is written in the announcement ! \n\n\t\"Compatibility with Qt 3: In order to minimize the work to migrate to Qt 4, Trolltech will provide comprehensive porting documents, tools and libraries. Most importantly, Qt 4 will provide an extension library that applications based on Qt 3 can link against to use obsolete classes. Using a separate library allows us to provide more compatibility than ever before, without bloating the Qt library with redundant functionality. This also means that developers can gradually begin to take advantage of Qt 4 features while continuing to use familiar Qt 3 functionality. \"\n\nI think this is optimal. Everyonce in a while backwards compatibility needs to be broken to implement fundamental changes. But the choice of a compat library is a great decision they made. \n\nIn the end, I think that both Qt and KDE should aim at keeping a very slow pace for jumping into \"incompatible\" versions, so that they can give a stable dev target to developers ... and I think they are doing a great job at it. I just hope the next jump in KDE (4.*) is designed with a life/cycle of a few years. At least two or three. KDE is already a mature platform :-)"
    author: "MandrakeUser"
  - subject: "What about QTopia ?"
    date: 2004-07-08
    body: "What about QTopia... does anybody know if Qt4 will be good enough to be the base for the next QTopia ?\n--\nGo Qt go! :D"
    author: "Pupeno"
  - subject: "Re: What about QTopia ?"
    date: 2004-07-08
    body: "From what I know it might be based on Qt4 but if you want a Free Software Palmtop Environment look at http://opie.handhelds.org we're surely interested in Qt4.\n\n\nregards"
    author: "zecke"
  - subject: "Re: What about QTopia ?"
    date: 2004-07-08
    body: "How much interested are you ?\nFirst, I need the hardware in which I could run a Free Software Palmtop Environment ;)"
    author: "Pupeno"
  - subject: "Looks great"
    date: 2004-07-08
    body: "From reading the texts, it looks really great at the moment.\n\nI'm especially interested in the \"interview\" model/view framework.\nThis will sure speed up programs that deal with rather large amounts of data."
    author: "tbscope"
  - subject: "No examples, but demos"
    date: 2004-07-08
    body: "The example apps have been stripped out of the preview (for size i presume), but in its place are several demos and boy are they eye candy. swirling alphablended boxes that are stretching and moving in 3d. :)  yum.  worth compiling just to play around with the arthur demo."
    author: "Ben Meyer"
  - subject: "Re: No examples, but demos"
    date: 2004-07-08
    body: "Darn it, I don't want screenshots - I want video capture!  :)\n\nOf course, I run very multihead, so I don't have 3D acceleration at all.  I wonder how it will perform."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: No examples, but demos"
    date: 2004-07-08
    body: "I was wondering, too -- as well as being a little afraid that a lot of the work we're currently doing for Krita would be obsolete, but that worry proves to be unnecessary -- and I noticed that performance of the alphablended primitives was a bit sluggish with plain X11/XRender (nvidia card, nv driver), and that it was quite smooth with the nvidia driver and presumably OpenGL, but that it meant X took 100% CPU."
    author: "Boudewijn Rempt"
  - subject: "Re: No examples, but demos"
    date: 2004-07-09
    body: "Yes ,could some brave soul please post videos somewhere. Now I know there is more eyecandy to explore I can't wait to see it.\n\nplease, please, please"
    author: "AC"
  - subject: "foreach()"
    date: 2004-07-08
    body: "has qt always had 'foreach' iterators or is this new in qt4?\n\n\nI can't wait for qt4\n/me drools"
    author: "standsolid"
  - subject: "Re: foreach()"
    date: 2004-07-08
    body: "It's new in qt4."
    author: "Andre Somers"
  - subject: "Re: foreach()"
    date: 2004-07-09
    body: "They really feel like python looks right ? Cool!\n\n\n#### Python: \n\nfor s in a_list:\n    sum += len(s)\n\n#### Qt4: \n     (http://doc.trolltech.com/qq/qq09-qt4.html)\n\nforeach (QString s, list)\n    sum += s.length();"
    author: "MandrakeUser"
  - subject: "Re: foreach()"
    date: 2004-07-09
    body: "Errata: I meant so say \"python loops\". Oh, and the indentation got eaten by the text formatting ;-)"
    author: "MandrakeUser"
  - subject: "Symbol visibility patch"
    date: 2004-07-08
    body: "I hope they made use os gcc\u00b4s symbol visibility option in version 3.5.  Then Kde will fly even more!"
    author: "Me"
  - subject: "Support for exceptions"
    date: 2004-07-08
    body: "Does anyone know if Trolltech is considering providing C++ exception support within Qt soon? I know I've read that it's been a vague/opinionated problem in the past (bloat/lack of compiler support), but it's not much of an issue today. Some people may disagree with exception-based error-handling but I personally love the concept, and have been spoiled coming from the Python world. It's one of the few downsides I see against using it for C++ development."
    author: "Eron Lloyd"
  - subject: "Re: Support for exceptions"
    date: 2004-07-09
    body: "As far I know, it would break compatibility with gcc < 2.95. I'm not sure if Qt or KDE wishes to do this, and would imagine it's one of the major reasons they're not sported, rather than a programming dislike. Do correct me if I'm wrong."
    author: "Martin Galpin"
  - subject: "Re: Support for exceptions"
    date: 2004-07-09
    body: "One of the gcc 2.95 holdouts had been FreeBSD and my understanding is that they are looking at making the move to gcc 3x in the next few months. I'm not sure but I think that it's possible stable Debian may be another holdout, but given even the longest life expectancies of a platform it seems reasonable to expect that by next year when KDE 4 is coming out the gcc 2.95 issue could be history. "
    author: "Eric Laffoon"
  - subject: "Re: Support for exceptions"
    date: 2004-07-09
    body: "Not with gcc-3.x being 2x-3x times slower at building things."
    author: "Sad Eagle"
  - subject: "Re: Support for exceptions"
    date: 2004-07-09
    body: "FreeBSD already shifted to gcc 3.x quite some time ago. Currently I am using gcc 3.3.3. Not the cutting edge, but a darned sight newer than 2.95. The move has already been made and there's nothing left to do except declare the 5.x branch stable..."
    author: "David Johnson"
  - subject: "Re: Support for exceptions"
    date: 2004-07-09
    body: "The problem with using exceptions in a library is that it forces the user to also use exceptions. If the user does not, than every minor extraneous error is going to crash their application. For example, access a file without the necessary permissions, and the program rudely crashes. In addition there is the tar-pit problem to worry about. While the geniuses at Trolltech are certainly capable of navigating the exception tar-pits, most users are not. See <http://www.octopull.demon.co.uk/c++/dragons/index.html> for more information."
    author: "David Johnson"
  - subject: "Re: Support for exceptions"
    date: 2004-07-09
    body: "Hm, but isn't the word \"crash\" wrong for this situation. I mean, if an exception is not caught, the program terminates in a controlled manner.\n\nAlso, the forcing argument has some problems. The first is, it goes either way: If Qt uses exceptions, users are forced to use them. If Qt does not use them, users are forced to use another error reporting scheme. Currently it is not even unproblematic to use exception in you own code if you use Qt, because some users could have compiled Qt without exception support. This will lead to crashes. So Trolltech should at least make a clear statement that Qt 4 should be compiled with exception support.\nThe second problem with the argument is, that properly used, exceptions do not really force users so much. For example, with the file permissions you described, there return code as error reporting would probably still be used. If exceptions were only used for truly \"exceptional\" circumstances, users would probably be content with not handling exceptions, and just terminating the program. Of course this depends quite a bit how thouroughly exceptions would really be used."
    author: "El Pseudonymo"
  - subject: "Re: Support for exceptions"
    date: 2004-07-09
    body: "1) While \"crash\" was the incorrect word, I doubt users will much care about the distinction.\n\n2) The \"forcing\" is a problem. I cannot choose to ignore an exception. I must handle it or my application terminates. But I can ignore an error, even safely in some circumstances.\n\n3) If exceptions were only used for \"exceptional\" situations, it wouldn't be too bad. But what guarantee will there be of that? This is an area of policy, and without seeing that policy and the mechanisms to enforce it, I simply can't trust it. What's to stop some C++ \"purist\" at Trolltech from propogating a close() error as an exception?\n\n4) I agree that Qt should be compiled with exception support, even it if doesn't use exceptions itself.\n\nI do not have a problem with using exceptions. I do have a problem with using them in a library, and a lower level library at that, however."
    author: "David"
  - subject: "Re: Support for exceptions"
    date: 2004-07-10
    body: "I think not having exceptions in Qt is the major weakness of Qt. All other methods of error handling just lead to massive code bloat.\n\nI assume you mean some sort of Device::close() in your example. You should not ignore an error on close() if that handles a file descriptor for instance, because then there is something seriously wrong with your program. \n\n\"Forcing\" is not the problem, its us programmers to forget to handle possible errors. Too many people are just too lazy, postpose error handling and forget about it after a while.\n\nAnd yes, 'crash' is the wrong word, because if there was a QBaseError or something you could just catch that in main() and the program would always terminate with at least an error message, if you forgot to catch the exception at the proper place. Big difference."
    author: "M"
  - subject: "Re: Support for exceptions"
    date: 2004-07-10
    body: "The trouble with forcing exceptions to be managed is simply that the majority of programmers don't have time to manage every error, or know every error that might be thrown in a given situation. The classic problem, or course, is whether to continue with potentially bad data/state, or to fail. Exceptions ensure the latter in most cases.\n\nPersonally, I prefer being able to explicitly handle errors myself knowing what might potentially happen and how critically it will affect my data and running integrity.\n\nThe danger of requiring expressions is best expressed by reference to many, many, many pieces of Java code that are running around with:\n\ntry\n{\n<something>\n}\ncatch (Exception e)\n{\n<do nothing>\n}\n\nIt turns up much the same arguments as strong vs. loosely/dynamically typed languages."
    author: "Dawnrider"
  - subject: "Re: Support for exceptions"
    date: 2004-07-10
    body: "I have seen a bit of C++ code, and I have never seen that style, i.e. writing catch-blocks but not doing anything in them. I suspect that almost no C++ programmer would come up with this idea. I have not seen much Java code, but I think there is the difference of checked exceptions, which do not exist in C++ in the way of Java. Such checked exceptions could lead to that style of coding. So, what I am trying to say is basically, this misuse of exceptions will not be seen in C++.\nAlso, you will not loose any control of handling errors if exceptions are used. Of course, if you really want to deal with *all* exceptions yourself, surround you code with a catch-all handler. A different question is, if that makes sense (Usually not). \nI also think that your argument that many programmers do not have the time to manage every error is a very good one in favour of exceptions, since the default behaviour of terminating if nothing is done is the right thing to do then. "
    author: "El Pseudonymo"
  - subject: "Re: Support for exceptions"
    date: 2004-07-10
    body: "I am sorry but terminating is not a good solution: the unsaved document... lost! So for me, it is little different than a crash, so perhaps it is not an  abrupt crash but a more controlled one. \n\n(Perhaps you will tell me that you can make an emergency save while for a real crash you cannot. But the end-user will not care, for him it is and remains a crash, as the program has terminated abnormaly.)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Support for exceptions"
    date: 2004-07-10
    body: "Yeah, it is not entirely unproblematic. But I really think ignoring errors is worse. Think of an abnormal situation which prevents an application from being able to save a document. Suppose this condition is detected while opening the file which is being worked on. Suppose that is ignored. The user will be happy with editing his important document, and only after he has made his changes, working for some time, out of a sudden, his data will be lost because he can not save. If the application would have terminated right at the start, I think that would have been the better alternative.\n\nHave a nice Saturday night! "
    author: "El Pseudonymo"
  - subject: "Re: Support for exceptions"
    date: 2004-07-10
    body: "Well, QIODevice::close has no return value. So we are always ignoring what happens at closing time in KDE...\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Support for exceptions"
    date: 2004-07-11
    body: "wow, that sucks!"
    author: "M"
  - subject: "Re: Support for exceptions"
    date: 2004-07-11
    body: "Why?\nThe application is closing down. Who gives a fuck?"
    author: "Allan S."
  - subject: "Re: Support for exceptions"
    date: 2004-07-12
    body: "The filesystem may not have finished writing the data when close() returns. If it hasn't, you can find out by checking the return value of close(). Many apps really should check the return value of close(), especially when there's a even the slightest chance the file is on an NFS partition.\n\nRik"
    author: "Rik Hemsley"
  - subject: "Re: Support for exceptions"
    date: 2004-07-10
    body: "I've been doing some work on the C# bindings for Qt and KDE so that I can port a program, and for that program I found that using the CLR's XML reading classes is much easier than using Qt's, because i can use a single exception handler to catch and report all file loading errors. Exceptions can be inconvenient sometimes, but this is a case where they make things much easier (in C# it's expected that you'll catch exceptions for anything that goes wrong, unlike C++, but it still shows how error handling with the Qt classes could be simplified).\n\nFor people who argue that an unhandled exception is the same to a user as a segfault, libraries like Qt could add a catchall statement in the main loop that would report the error and allow the user to continue; well written code will ensure that this is possible without leaving a mess behind. Common causes of segfaults, like null pointer accesses, could work the same way since they don't actually damage the program's state, but the process will just exit wherever it is instead of going up the stack and cleaning up everything."
    author: "Richard Garand"
  - subject: "Re: Support for exceptions"
    date: 2004-07-12
    body: "you mean:\n\ntry{\n\n}catch(...)\n{\n\n}\nfor all exceptions? And for file I/O errors there is a exception superclass..."
    author: "Ruediger Knoerig"
  - subject: "OpenGL"
    date: 2004-07-09
    body: "The OpenGL Code looks really old fashion to me. Ok, VBO's are mostly don't needed, but the don't use automatic mipmap generation. glVertexi is mostly the slow path. Maybe they can change in QT5 the coordinates from integer to floats."
    author: "Marco Bubke"
  - subject: "Re: OpenGL"
    date: 2004-07-09
    body: "Why not in Qt 4 - if you provide them feedback?"
    author: "Anonymous"
  - subject: "Re: OpenGL"
    date: 2004-07-09
    body: "Because its a big change. There is also hardware which have no FPU, for example many PDA's."
    author: "Marco Bubke"
  - subject: "Re: OpenGL"
    date: 2004-07-09
    body: ">  glVertexi is mostly the slow path\n\nUnless people create 3d games with a lot of triangles with Qt4, it should be fine."
    author: "anon"
  - subject: "libQtCore licensing ?"
    date: 2004-07-09
    body: "How about LGPLing libQtCore ?\nThis would make Qt the first choice for system programming (libs, daemons, etc.) for any UNIX/Mac developer.\n\nAlex"
    author: "aleXXX"
  - subject: "Re: libQtCore licensing ?"
    date: 2004-07-09
    body: "Then TrollTech's revenue stream would be killed off for everything except Windows programs.  TrollTech has to get money from somewhere, they are a business after all.\n\nFor now its best for TrollTech to keep QT as it is."
    author: "Corbin"
  - subject: "Re: libQtCore licensing ?"
    date: 2004-07-09
    body: "he was asking about qtcore, not Qt itself. I think that would have certain advantages, since I don't think people really buy Qt for lower level programming but rather for GUI programming. Qt4 could change that. "
    author: "anon"
  - subject: "Re: libQtCore licensing ?"
    date: 2004-07-09
    body: "Matthias Ettrich declined this idea already last year."
    author: "Anonymous"
  - subject: "Re: libQtCore licensing ?"
    date: 2004-07-09
    body: "Yes, it would be better if the community wrote a LGPL version of Qt for linux/*BSD, maybe starting with qt-core. I think that earlier or later this will happen. Btw, there is a free-beer version of Qt in Apples WebCore. But only for MacOS X."
    author: "M"
  - subject: "Re: libQtCore licensing ?"
    date: 2004-07-09
    body: "> there is a free-beer version of Qt in Apples WebCore\n\nKWQ? You don't know what you're talking about. It's not more than a compatiblity layer."
    author: "Anonymous"
  - subject: "Re: libQtCore licensing ?"
    date: 2004-07-10
    body: "Of course its not a complete Qt implementation, but its quite big. Qt itself is also wrapping up several libs and interfaces on linux/X11 for example. "
    author: "M"
  - subject: "Re: libQtCore licensing ?"
    date: 2004-07-10
    body: "No, I don't think. Then this community would have to follow everything Qt does. Why should we try to take trolltech the business away ? It would be one of the most stupid things we could do.\nBut LGPLing libQtCore probably wouldn't take any business away from the trolls, but make QtCore an alternative even for closed source apps.\nNobody will buy QtCore if he just wants to write some small tool without GUI and stuff I guess. The business is mainly in the GUI and extensions I think\n\nAlex"
    author: "aleXXX"
  - subject: "Re: libQtCore licensing ?"
    date: 2004-07-10
    body: "I agree that a lgpl qt-core would be nice. I have rewritten a few Qt classes myself using another C++ library as foundation, and it was not so hard. Maybe one day...\n\nI am not so much annoyed by the fact that Qt is under the GPL and I have to pay to write closed source software, but rather by the fact that Qt is not developed more aggressively. I find it problematic that Qt already depends on glib for instance. This can lead to making the competitors product (Glib/Gtk/Gnome) the de-facto standard and themselves irrelevant."
    author: "M"
  - subject: "Re: libQtCore licensing ?"
    date: 2004-07-10
    body: "Which part of Qt depends on glib ? At least the \"ldd mainwindow\" didn't show glib.\n\nAlex"
    author: "aleXXX"
  - subject: "Re: libQtCore licensing ?"
    date: 2004-07-10
    body: "Did he say that publically?  A URL would be nice.  Of course, Matthias also reads this forum, so maybe he can just confirm here. :)\n\nAlong similar lines, another interesting possibility would be a free QtCore for Windows.  After all, 90% of QtCore is already freely available on Windows anyway, using code straight from the Free edition.  Even the tool programs, like qmake and moc, are free.  I doubt anyone would pay for _just_ QtCore, so I say Trolltech should simply make the last bits free and reap the publicity.\n\nHook 'em on QString while they're young. ;-)"
    author: "Justin Karneges"
  - subject: "Re: libQtCore licensing ?"
    date: 2004-07-11
    body: "I did? I don't even remember having spoken about it, but ok, I might just as well.\n\nThe dual-licensing model is based on a simple principle: quid pro quo. Either you give back in terms of code (by being part of an open source community that shares code and knowledge) or you give back in terms of money (by buying a license). The model cannot work with the LGPL, thus we will not do it.\n\nI understand the request was about libQtCore only, but to me there is no  difference in principle between libQtCore and other Qt libraries. They both require work to write and maintain, and they both reduce work when being used.\n\nIt's a bit like Robin Hood. He could only give to the poor because he took it from the rich. He would probably have gotten great publicity had he given to both the rich and the poor, but I doubt his business would have made it through the dotcom bubble with constant growth, or even be profitable today."
    author: "Matthias Ettrich"
  - subject: "Re: libQtCore licensing ?"
    date: 2004-07-12
    body: "> It's a bit like Robin Hood. He could only give to the poor because he took it from the rich. He would probably have gotten great publicity had he given to both the rich and the poor, but I doubt his business would have made it through the dotcom bubble with constant growth, or even be profitable today.\n\nLMAO!\n\nOne can only speculate whether Robin Hood's business model changed or Nottingham's social programs reduced demand. It's possible too that Scottland Yard and Interpol discouraged this romantic tradition, or nobles only carry plastic nowadays. In any event now that Robin Hood is not publicly traded we cannot review his quarterlies and know for sure.\n\nOf course today many of us who follow this noble tradition today are forced to ask the rich to participate, and unfortunately almost nobody thinks they are rich. I wept because I had no 64 bit processor to compile what I downloaded on my DSL... and then I saw the man with a 200 MHz Pentium on a dialup. There but by the grace of a few thousand bogomips go I..."
    author: "Eric Laffoon"
  - subject: "Re: libQtCore licensing ?"
    date: 2004-07-13
    body: "Ok. \nNow that libqt is split, will also the commercial license be split (i.e. will it be possible to buy only a QtCore license) ?\n\nAlex\n\nP.S. I still think getting commercial developers hooked with a LGPL QtCore so that they can't live without a commercial QtGui anymore wouldn't hurt Trolltech's business"
    author: "aleXXX"
  - subject: "First impressions"
    date: 2004-07-10
    body: "I have just done \"make install\" and are ready to take a look at it. One thing I noticed during configure was that it required XCursor 1.0, and I had 1.1. I changed the test scripts so it would allow 1.1 as well.\n\nThe Qt Paint engine test (Arthur) for alpha blended primitives was butt slow on my machine. But I think I read somewhere that at this stage it was unaccelerated on X11. The open gl thing was fast though.\n\nThe textedit demo felt faster. A bit surprise that it doesn't select the whole word if you start at the middle (yes, like windows does) and move left/right.\n\nPlasmatable, butt slow here. Probably 1/3 rd of the speed my i386-sx 16 did in a plasma demo :=)\n\nToolbar demo, sad to see that you can't have the toolbar next to the menubar like in IE on windows :( It can save you screen space.\n\nThen I would like to see async sql queries. But I'm not sure yet if they are. Would be so cool to have a signal fire when the query is done. Sometimes sql queries take minutes before result is back....\n\nWill give it a greater than-5-minute-spin tomorrow :)"
    author: "perraw"
  - subject: "Re: First impressions"
    date: 2004-07-10
    body: "The plasmatable is not a plasma demo, it is a QTableView demo. It abuse the model/view paradigm to show its flexibility. The plasm table is a basically a spreadsheet with every pixel being one cell. Try selecting the cells with the mouse, and you'll see.\n\nWhile it is slow for a plasma demo, it is fast for a table view."
    author: "Matthias Ettrich"
  - subject: "Re: First impressions"
    date: 2004-07-10
    body: "Yes, same here for the alpha blending.\nOtherwise, cool :-)\n\nAlex"
    author: "aleXXX"
  - subject: "Re: First impressions"
    date: 2004-07-11
    body: "> The textedit demo felt faster. A bit surprise that it doesn't select the whole\n> word if you start at the middle (yes, like windows does) and move left/right.\n\nNice, I have always hated that windows-\"feature\"."
    author: "Asdex"
  - subject: "Initial Review of Qt 4"
    date: 2004-07-10
    body: "I've written my initial thoughts on Qt 4 after playing with the new code on my blog at <a href=\"http://www.kdedevelopers.org/node/view/506\">http://www.kdedevelopers.org/node/view/506</a> if anyone is interested.\n\nRich.\n\n"
    author: "Richard Moore"
  - subject: "Re: Initial Review of Qt 4"
    date: 2004-07-10
    body: "Two things from my side:\n-hopefully the removal of setAutoDelete(true); doesn't break too many applications (i.e. introduces mem leaks), I used this feature all the time.\n-docs have to be updated so that it's easy to see which classes are in which sub-library (QtCore, QtGui,...)\n\nAlex"
    author: "aleXXX"
  - subject: "Re: Initial Review of Qt 4"
    date: 2004-07-10
    body: "Simple test:\nint main( int argc, char ** argv )\n{\n   QString s=\"hallo welt\";\n   cout<<s.latin1()<<endl;\n}\n\nGives with Qt4:\n$ time ./test\nhallo welt\n\nreal    0m0.005s\nuser    0m0.000s\nsys     0m0.000s\n\nwith Qt3:\n$ time ./testq3\nhallo welt\n\nreal    0m0.024s\nuser    0m0.020s\nsys     0m0.000s\n\n:-))\n\nAlex"
    author: "aleXXX"
  - subject: "Re: Initial Review of Qt 4"
    date: 2004-07-10
    body: "Do you mean that setAutoDelete does not exist at all anymore or just that it defaults to not delete now (setAutoDelete(false)).\n\nIf it is just the default mode that was changed, perhaps the developers should be made aware of it, so that they can change current code to remain compatible.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Initial Review of Qt 4"
    date: 2004-07-10
    body: "Don't exist anymore at all: http://doc.trolltech.com/4.0/tulip-overview.html#sec.4"
    author: "Anonymous"
  - subject: "Re: Initial Review of Qt 4"
    date: 2004-07-10
    body: "H'm, annoying indeed.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Initial Review of Qt 4"
    date: 2004-07-10
    body: "QPtrList and friends have not changed at all, they just moved into the compat library with all their glory (including setAutoDelete())"
    author: "Matthias Ettrich"
  - subject: "Re: Initial Review of Qt 4"
    date: 2004-07-10
    body: "Any comments to libQtCore licensing ? Are you considering LGPL ? Or will it definitely stay GPL ?\n\nBye\nAlex"
    author: "aleXXX"
  - subject: "Text printing & Fonts"
    date: 2004-07-16
    body: "I read that \"Laying out text using design metrics (WYSIWYG) is currently disabled\".\n\nSo, does anybody know for sure if they are going to fix the font metrics issues.  Also, do they know that you need to be able to select if you want something formated for screen rendering or printer rendering? Currently we have ONLY screen rendering.\n\nIt says nothing about the PostScript driver.  I hope that this will be fixed.  As I presume that most people know it does not get the PostScript Font names correct unless you embed the fonts.  The correct way to do this is to use FontConfig & FreeType2 to get this information from the font file.\n\nI also found that there is an inconsistency between the way that TrueType and Type1 font families are named and the current version is still confused by this.  It appears that Qt is designed to work with the TrueType method.  In which case, it will need to convert the Type1 family names into TrueType family names.  Personally, I would like to see it the other way, but this would require rewriting some of KDE as well -- the KDE Font Selector would need more windows (2 more I think but the second one wouldn't be used much and the windows would have to interact because not every combination would be available).\n\nAnd, a general solution needs to be found for the fact that not all fonts have:\n\n<blank> | Regular | Roman, Bold, Italic, & Bold Italic\n\nAnd worse, some fonts have more than one weight of bold.  The kludge in the current version only works for the fonts included in the PostScript driver code.  If this (a table) is the solution, it needs to be in a configuration file, and we need a KDE GUI program to edit it (a KPart since the font installer would also need it and you should be able to access it from the font selector).\n\n--\nJRT"
    author: "James Richard Tyrer"
---
<a href="http://www.trolltech.com/">Trolltech</a> has announced the <a href="http://www.trolltech.com/newsroom/announcements/00000169.html">availability of the first Qt 4 Technical Preview</a>. Qt 4, the next major release of the popular cross-platform C++ application framework which KDE is based on, is scheduled for final release in late Q1, 2005. Read <a href="http://doc.trolltech.com/4.0/tech-preview.html">the online documentation</a> or jump to the <a href="http://www.trolltech.com/download/tech_previews.html">download page</a>.
<!--break-->
<p>Five new technologies, codenamed Arthur, Scribe, Interview, Tulip, and Mainwindow are being incorporated into the next major version of Qt 4.0. The technologies are:

<ul>
<li>Arthur: The new painting framework</li>
<li>Scribe: The Unicode text renderer with a public API for performing low-level text layout</li>
<li>Interview: A model/view architecture for item views</li>
<li>Tulip: A new set of template container classes</li>
<li>Mainwindow: A modern action-based mainwindow/toolbar/menu and docking architecture</li>
</ul></p>

<p>This is a preview of some of the Qt 4 libraries, not of the entire application development framework. Most notably, new versions of Qt Designer and Qt Linguist are not included. The Technology Preview is meant to be tested on a limited set of platforms only.</p>

<p>Qt 4 still is in a very active state of development. Not everything is ready for prime time, and there are significant omissions. Most importantly, this Technology Preview is not meant to be used in production code or even for application development. Not all backwards compatibility functionality is in place yet but you're able to write small programs.</p>





