---
title: "KDE-Look.org: KPDF Icon Contest Launched"
date:    2004-09-15
authors:
  - "fmous"
slug:    kde-lookorg-kpdf-icon-contest-launched
comments:
  - subject: "[OT] Speaking about Kpdf..."
    date: 2004-09-15
    body: "Anybody knows why Kpdf renders so bad all my pdfs? KGhostview works just fine..."
    author: "fake"
  - subject: "Re: [OT] Speaking about Kpdf..."
    date: 2004-09-15
    body: "KPDF doesn't really work in current versions.\nThis will be fixed with KDE 3.3.1 AFAIK.\n"
    author: "Martin"
  - subject: "Re: [OT] Speaking about Kpdf..."
    date: 2004-09-15
    body: "I found KPDF to work much better already in 3.3.0 than before in KDE 3.2.x."
    author: "ac"
  - subject: "Re: [OT] Speaking about Kpdf..."
    date: 2004-09-15
    body: "Well, I do use KDE 3.3 .. am I the only one with this problem? :("
    author: "fake"
  - subject: "Re: [OT] Speaking about Kpdf..."
    date: 2004-09-15
    body: "No, you're not the only one:\nhttp://bugs.kde.org/show_bug.cgi?id=67950\nAs a matter of fact, I have yet to see a single pdf that kpdf can render properly.\n\nHopefully it will truly be fixed in kde-3.3.1\n\n\n"
    author: "Rex Dieter"
  - subject: "Re: [OT] Speaking about Kpdf..."
    date: 2004-09-17
    body: "It's the exact opposite with me. Chris Schlager's Akademy presentation loaded fine KPDF, KGhostview didn't."
    author: "David"
  - subject: "Re: [OT] Speaking about Kpdf... IT ROCKS!! :-)))"
    date: 2004-09-16
    body: "Albert Astals Cid made it!\nNow kpdf renders document better then ever featuring the complete xpdf core renderer, without font problems or something like that.\n\nPlus there is an experimental branch with tons of new features and it's faster, optimized, snappy.. you'll see it (screenshot:http://robotics.dei.unipd.it/~koral/KDE/screenshots/kpdf_cvs.png).\nAlbert just added the Search feature and now the Link following! And there is *far* more to expect. Just sit back and relax.\n\nIs there anything more you want from a pdf/ebook reader? If so just add some words here, because you may get it sooner than ever. Please set your fantasy free and write about the KPDF of your dreams! ^_^\n"
    author: "R.Enrico"
  - subject: "Re: [OT] Speaking about Kpdf... IT ROCKS!! :-)))"
    date: 2004-09-16
    body: "Would it be possible to select an area, and be able to copy that to the clipboard? (perhaps as text/plain,utf8 ? - dunno if eps is possible?)\nI realise you couldn't get word-processor style copying, but you could select an area, and get the text under that area, right?\n\n"
    author: "JohnFlux"
  - subject: "Re: [OT] Speaking about Kpdf... IT ROCKS!! :-)))"
    date: 2004-09-16
    body: "You can do that in xpdf right now, and copy as text/plain. It could be implemented better though. OS X marks continuous text instead of a rectangular selection, and that's usually the way you want it, I think."
    author: "kjtl"
  - subject: "Re: [OT] Speaking about Kpdf... IT ROCKS!! :-)))"
    date: 2004-09-16
    body: "He was speaking of KPDF, not any Linux software!\n\nQuestion still remains: -\n\nIs text selection/copying still possible in KPDF?\n\nCb.."
    author: "charles"
  - subject: "Re: [OT] Speaking about Kpdf... IT ROCKS!! :-)))"
    date: 2004-09-16
    body: "A ruler and distance and colour measuring tool would be great.\nThis would even help me to reduce my need to switch to\nWindows for Acrobat Pro.\n"
    author: "Martin"
  - subject: "Re: [OT] Speaking about Kpdf... IT ROCKS!! :-)))"
    date: 2004-09-16
    body: "In Adobe acrabot, you can put \"annotations\" to a pdf file. These are small postit's with author and commentary that you can stick whereever you like on a pdf. As far as I know, scribus can put these on its pdf's.\nThis would be a very usefull feature for kpdf.\n"
    author: "J\u00e9r\u00f4me Lodewyck"
  - subject: "Re: [OT] Speaking about Kpdf... IT ROCKS!! :-)))"
    date: 2004-09-18
    body: "Put \"annotations\" in a pdf file? That would be great! Someone should post a wishlist item :-)"
    author: "seguso"
  - subject: "Hyperlinks in PDF documents"
    date: 2004-09-16
    body: "@R.Enrico\n\nHi there,\n\n2 Ideas of really cool features for KPDF!\n\n\nHyperlinks in PDFs are an absolute necessity today. There is an amazing number of PDFs that do have them and therefore I envy acrobat reader users.\n\nKghostview cannot do that.\nCan KPDF ?\n\n\nALSO: \nThe full Acrobat version (the paid-for software) can browse the web (!) and then store a Web site in PDF format with links (external as well as within the documents).\nPDF is often better for local storage than a big heap of .htmls and .gifs for a web site. That holds especially true if the object is more that a \"linux-howto\" in terms of \"design\" (i.e. including graphics etc) and if you are not about to change that document.\n\nIf you've ever worked with that feature you basically don't want to go back ...\n\n\nGo, KPDF Go  !!!\n\n\n\nAll the best to the programmers - I wish I could help but my Java skills and number crunching skills may not be the perfect match right now.\n\n\n\nCheers, Olaf"
    author: "Olaf"
  - subject: "Have you ever tried kdeaddons?"
    date: 2004-09-16
    body: "Install kdeaddons\n\nGo to a website in Konqeror. Tools -> Achive Web page. Archives the whole page in one tar Bz2 file named as a .war. Konqeror can open a .war file like it is a site.\n\nBasically does what your Acrobat thing does except its free and doesn't require an extrnal viewer (any browser oculd browse the war if you untar it first)."
    author: "Jason keirstead"
  - subject: "Re: Have you ever tried kdeaddons?"
    date: 2004-09-16
    body: "Hi,\n\nMatter of fact - \"going to .war\" is cool indeed ! However as you say you have to untar first if it is not for Konqueror... But otherwise works well.\n\nI think \"PDF\" is indeed one of the easiest **portable** formats around - every office package strives to work with it. (That doesn't hold with most compressing formats I believe). PDF appears to become a standard for things.\n\nEspecially if you look beyond programmers! I download and exchange documents with many people in the windoze world.\n\nBut thanks for the tip - I am going to use that .war - solution where I am sure that I will keep things for myself.\n\nBTW: No matter what: The need to follow existing hyperlinks in existing PDFs still stands ! \n\nOlaf"
    author: "Olaf"
  - subject: "Re: [OT] Speaking about Kpdf... IT ROCKS!! :-)))"
    date: 2004-09-17
    body: "Looking at http://robotics.dei.unipd.it/~koral/KDE/screenshots/kpdf_cvs_2.png, it seems that he didn't.  \u00bbW omens's sex ual fant asies\u00ab, text horribly rendered in that ugly arial/helvetica, kpdf seems to fall back to everytime I give it a try.\n"
    author: "olli"
  - subject: "Re: [OT] Speaking about Kpdf... IT ROCKS!! :-)))"
    date: 2004-09-17
    body: "No, I have the same pdf and it renders ugly in KGhostView too. It's not KPDF's fault.. this time :)"
    author: "Anonymous"
  - subject: "Re: [OT] Speaking about Kpdf... IT ROCKS!! :-)))"
    date: 2004-09-17
    body: "BTW, it seems that KPDF is finally getting nicer... impressive!"
    author: "Anonymous"
  - subject: "New features"
    date: 2004-09-16
    body: "Hi all,\n\nI'd like to see an enhanced search (maybe like the acrobat 6 or so, but with progress bar) together with the possibility to highlight the searched text (just like the new Firefox 1.0pr).\n\nThe integration with a voice sinthetiser would be great (let it read for you ;-)).\n\nAnd finally, the ability to export in a decent manner (this is, keeping the format) into another type of document, or batch export in the case of images, would be superb.\n\n\nThanks listening my prayers!\n\n"
    author: "David"
  - subject: "Oh, yes..."
    date: 2004-09-18
    body: "Well, in my opinion, the most demanding new features is a real LaTeX PDF support.\n\nI mean:\n(1) Real font rendering as XPDF does since 1.0 (but somehow not kpdf !!). I hope this is fixed. But what I've seen on the screenshots listed above they only tested PDF's that display TrueType fonts. What about LaTeX fonts?\n\n(2) Bookmarks. If I read a LaTeX document, for example a paper from sciencedirect.com, I DO need a bookmark section that helps me navigating (that kind of contents/index thing of Adobe's Acrobat).\n\n(3) An easy to use zoom mode like 'Page width' or 'Whole page'.\n\nRegards\nSebastian"
    author: "Sebastian"
  - subject: "Re: New features"
    date: 2004-09-18
    body: "I'd love if it was possible to have continuous page view as in acroread and MiKTeX's \"yap\" rather than what we have now where you can't see more than one page. I don't see why it shouldn't be possible to just use down arrow to scroll through the entire document instead of having to use page down to change pages. This goes for kdvi and kghostview as well (is it because this isn't possible with the current design of kparts, or is it just some traditional thing left over from gv?)\n\nJust for the record: I use KDE 3.0 (the sysadmins at the university I attend haven't updated it yet)"
    author: "Kasper H"
  - subject: "topic was icons..."
    date: 2004-09-19
    body: "and about them, I think there are some nice submissions on kde-look.org. is there a public discussion anywhere about them? exect for kde-look, that is... I'd love to hear the jury's current thoughts ;-)"
    author: "superstoned"
---
<a href="http://www.kde-look.org/">KDE-Look.org</a> has started an effort to entice artists to make missing icons for KDE applications. The current icon contest focuses on a new icon for the KDE PDF viewer KPDF.
The KPDF developers are looking for a better icon as <a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdegraphics/kpdf/kpdf/hi48-app-kpdf.png?rev=1.1.1.1&content-type=text/x-cvsweb-markup">the current one</a> 
does not help to describe the program and its function: reading PDF files. 

The icon contest is hosted at KDE-Look.org where all designs will be uploaded and listed in the <a href="http://www.kde-look.org/index.php?xcontentmode=43">the icon contest section</a>. At KDE-Look.org you can <a href="http://www.kde-look.org/news/index.php?id=123">read more</a> about this contest which closes at 1st October.

<!--break-->
