---
title: "KDevelop TechNotes Issues #1 and #2"
date:    2004-12-03
authors:
  - "adymo"
slug:    kdevelop-technotes-issues-1-and-2
comments:
  - subject: "Thanks"
    date: 2004-12-03
    body: "Good stuff, thanks a lot!"
    author: "LB"
  - subject: "OT? KDevelop a no go for me"
    date: 2004-12-03
    body: "Debugging with the KDevelop/gdb combo that comes with Debian unstable has been a complete no go for me so far. Last time I tried it a couple of months back I only got as far as stepping through the first lines of the main() of the skeleton program KDevelop itself creates. Once it came to the KApplication contructor it just stopped there and wouldn't go any further. It seemed like the problem was that gdb choked on shared libraries or something like that. Any ideas?"
    author: "would be KDevelop user"
  - subject: "Re: OT? KDevelop a no go for me"
    date: 2004-12-03
    body: "There are still some issues with the gdb interface, however [I'm not using Deb unstable] in general I have no particular problems debugging. \nHave a look at the forum on kdevelop.org or search/ask on the mailing list [kdevelop@kdevelop.org].\n"
    author: "Ste"
  - subject: "Re: OT? KDevelop a no go for me"
    date: 2004-12-03
    body: "It seems that you have stripped libraries with no debug symbols at all. I don't know much about debian maybe it provides libraries with debug symbols."
    author: "adymo"
  - subject: "Interesting..."
    date: 2004-12-03
    body: "This URL was helpful because it showed, right away, that only the subclassing method is viable with the current implementation. Some of us prefer to use .ui.h files, so this is both disappointing and exciting at once. I hope there's future plans to accommodate the .ui.h style."
    author: "Dan Ostrowski"
  - subject: "Re: Interesting..."
    date: 2004-12-03
    body: "ui.h files have a real drawback if you use the automake system (as it is used by default for any KDE application). If you modify the ui.h file, the build system will not notice the change and will not rebuild your executable...\nSo avoid it (at least in KDE CVS), unless you want to cause trouble to those using CVS versions and update from source frequently.\n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: Interesting..."
    date: 2004-12-03
    body: "Designer integration in KDevelop was designed and implemented to be as convenient as former .ui.h method. Right now it should do the same you previously did with Qt Designer and .ui.h and even better (as Andras explained).\nTrolltech don't use .ui.h approach in Qt4 so personally I'm not going to implement .ui.h like integration. But anyway, everybody is free to implement this ;)\n"
    author: "adymo"
  - subject: "KDevelop and makefiles?"
    date: 2004-12-03
    body: "Is it possible yet to use KDevelop on a makefile-based app?  I tried it a while ago and couldn't figure out how to override it's own automake-based orientation."
    author: "Rob"
  - subject: "Re: KDevelop and makefiles?"
    date: 2004-12-03
    body: "It seems that you tried 2.x version. With 3.x you should simply import your makefile-based project as \"Custom makefiles project\".\nPS: welcome to #kdevelop at irc.kde.org channel or use the web irc client: http://www.kdevelop.org:8080/chat/."
    author: "adymo"
  - subject: "Re: KDevelop and makefiles?"
    date: 2004-12-04
    body: "Thanks!!  That's awesome. Didn't know Kdevelop could do that."
    author: "Leo S"
  - subject: "Re: KDevelop and makefiles?"
    date: 2004-12-04
    body: "Yeah.. 90% of what I write is just simple, one-off things that use a makefile.  I'd like to take advantage of KDevelop's features but I just can't be bothered to use a full automake system for small projects so I do all my C/C++ programming in Kate instead."
    author: "Leo S"
  - subject: "Re: KDevelop and makefiles?"
    date: 2004-12-04
    body: "You should try QMake even for projects that do not use Qt library. It will simplify your life a lot. No need to write those makefiles. Maintaining one .pro file is _very_ simply.\nYou can create QMake projects with KDevelop and you can still use Kate if you want because no addional stuff instead of a short and simple .pro file is created. But KDevelop has a nice GUI to edit .pro files ;)"
    author: "adymo"
  - subject: "kdevassistant"
    date: 2004-12-04
    body: "Thanks for reminding me that I can run kdevassistant standalone, I think I saw it a while ago, but never remembered it.\n\nIt's a really great application."
    author: "NoUseForAName"
---
I have started a series of <a href="http://www.kdevelop.org/">KDevelop</a> related articles. I call them "<a href="http://www.kdevelop.org/index.html?filename=doc/technotes/index.html">KDevelop TechNotes</a>" and plan to publish all useful information about the IDE - tips and tricks, use cases, tutorials, etc. Everyone is welcome to share KDevelop knowledge by writing their own issues of technotes. I will be pleased to publish them on the project website.


<!--break-->
<p><a href="http://www.kdevelop.org/doc/technotes/kdevassistant.html">The first issue</a> should be a matter of no little interest to all free software developers. Read about KDevelop Assistant - an advanced API documentation viewer.</p>

<p><a href="http://www.kdevelop.org/doc/technotes/cpprad.html">The second issue</a> will tell you about rapid application development with KDevelop IDE using C++ language and Qt and/or KDE libraries. Do you know that KDevelop IDE offers integrated GUI design facilities? Do you know how to develop an application with a GUI in just two minutes? No? Learn more about that.</p>

