---
title: "KDE Quality Team Revisited"
date:    2004-04-10
authors:
  - "tchance"
slug:    kde-quality-team-revisited
comments:
  - subject: "KDE Media"
    date: 2004-04-10
    body: "Isn't this overlapping with kde-promo?"
    author: "m."
  - subject: "Re: KDE Media"
    date: 2004-04-10
    body: "I was thinking the same. Well kde-promo is very quiet lately and I think this is because everybody is doing their own thing without telling it on that mailinglist. \n\nI think it would help if people just mention on that ML *what* they are doing. We also have a nice cvs module for these kind of promo-efforts btw. \n\nSomething else .. it is nice to see some feedback from \"Quality Teams Project\". Thank you for informing the community about the progress this project is making. \n\n\nFab"
    author: "fab"
  - subject: "Re: KDE Media"
    date: 2004-04-10
    body: "Not necessarily. It works the same as the other Quality Teams, in that people who want to do media work will need to subscribe to the kde-promo list and do the bulk of their work through there. But previous to this initiative, there was no clear way for people to get involved in it... there was no help in starting out writing, and no big push to attract people to the list.\n\nAs Fabrice says, at the moment the kde-promo list is very quiet, and there is very little KDE-related journalism and media work going on. I'm hoping we can change that. Of course it may be that kde-promo is so quiet because nobody wants to do anything, but I doubt that. I think there are lots of people out there who'd love to do some writing, maybe even for money, who haven't had the confidence or motivation to do so."
    author: "Tom Chance"
  - subject: "Re: KDE Media"
    date: 2004-04-10
    body: "spot on ... \n\nWe need to get organised more. imo we don't need another mailinglist for these efforts. Please be vocal about your favourite desktop and subscribe to kde-promo. So *anyone* (yes even you) who uses KDE can help here :)\n\nFab"
    author: "fab"
  - subject: "media"
    date: 2004-04-10
    body: "For the media thing:\n\nIs your team intrested in organising a joint workshop in Germany with other groups? PR has to be professionalised in many fields and it would be nice to bring media experts and developers together and get information on what to do and how to do?"
    author: "Andr\u00e9"
  - subject: "Re: media"
    date: 2004-04-10
    body: "It's not really viable for me to travel to Germany for this sort of thing (too expensive from the UK), but there may be those who are interested.\n\nWe could, for example, do something during the aKademy."
    author: "Tom Chance"
  - subject: "Re: media"
    date: 2004-04-11
    body: "Good idea ... we could all meet at aKademy. Sounds like a good idea to me. \n\nFab"
    author: "fab"
  - subject: "Re: media"
    date: 2004-04-11
    body: "During the 5 weekdays of the \"Coding Marathon\" there are rooms and \"slots\" to organize all sorts of forums, workshops, meetings, discussions, birds-of-a-feather sessions etc.\n\nThe 2 weekends with the conference sessions are probably not as good, since you would miss a big part of the presentations and talks....\n\nSomeone of you should take the initiative to ask on IRC/appropriate mailing lists for people to come, the best time etc. -- and start organizing it."
    author: "Kurt Pfeifle"
  - subject: "Re: media"
    date: 2004-04-12
    body: "I was more thinking of a kind of joint meeting of FLOSS media workers with media trained people, for instance an exchange with Andreas Gebhard (who did media work at linuxtag and tries to establish a gnu press agency), a few computer magazine journalists ecc.\n\nHow to write a good press release, what iformation is important for the media, how do you have to present yourself, through what channels, how to lobby politicians ecc."
    author: "Andr\u00e9"
  - subject: "Re: media"
    date: 2004-04-18
    body: "Good idea -- do you want to take the initiative and organize everything which needs to be done to make it happen?"
    author: "Kurt Pfeifle"
  - subject: "kdeedu live CDs?"
    date: 2004-04-11
    body: "The article mentions CDs being sent out to members \nof the KDE Edutainment Quality team containing CVS copies.\nAre that live CDs?  \n\nOr are there any other live CDs around I could use to show off \nthe kdeedu programs to a teacher I know?  \nEither CVS (if reasonably stable) or KDE 3.2.1 would do...\n(and support for German would be nice)\n\nKnoppix doesn't cut it here, they don't seem to ship \nkdeedu as a whole, and Skolelinux seems to target *installations*\nat schools.\n\n"
    author: "cm"
  - subject: "Re: kdeedu live CDs?"
    date: 2004-04-11
    body: "The CD sent to a member simply contained a checkout of CVS, uncompiled, since that person only had a 56k connection.\n\nHowever you could work with the Edu quality team to make a decent live CD that fits the needs you describe."
    author: "Tom Chance"
  - subject: "Re: kdeedu live CDs?"
    date: 2004-04-12
    body: "Well, I'd need something for the (very) short-term.\nI take it something like that doesn't exist yet? \n\n"
    author: "cm"
  - subject: "Re: kdeedu live CDs?"
    date: 2004-04-12
    body: "For the record: \n\nI found a remastered Knoppix 3.4 (the Cebit Edition) \nthat contains a recent KDE CVS version \nincluding the kdeedu module [1].  This one will do. \n\nThanks for any replies,\ncheers, \ncm.\n\n[1] Source: http://www.knoppix.net/forum/viewtopic.php?t=9355\n"
    author: "cm"
  - subject: "Re: kdeedu live CDs?"
    date: 2004-04-11
    body: "The mail to the mailinglist can be found at http://lists.kde.org/?t=108144697800004&r=1&w=2"
    author: "Dennis"
  - subject: "Re: kdeedu live CDs?"
    date: 2004-04-12
    body: "I already ran across that post introducing \"Schnoppix\".  \nBut that seems to be another distro for a school's intranet. \n\nIt might still work on a standalone PC, though, \nbut a reply to that post says that it only included kstars\nfrom all of kdeedu...\n\nThanks anyway. \n\n"
    author: "cm"
---
At the beginning of March, the KDE Project <a href="http://dot.kde.org/1078249862/">announced</a> the launch of the <a href="http://quality.kde.org/">Quality Teams Project</a>, a new effort to help people contribute to KDE, whether you are a programmer or not. Lots of people showed interest, and we enjoyed an initial burst of activity, so read on to find out what has happened, and how you can get involved in your favorite desktop environment, and see your work being distributed around the world.
<!--break-->
<p>
I'll briefly cover work done on artwork, KDE Edutainment, KOffice, KDE Multimedia, KDE PIM, Konqueror, and working with the media. If you want to help any of these, now is a good time to jump in. Of course, the place to start is the <a href="http://quality.kde.org/">Quality Teams Project homepage</a>, but this will help you get an idea of what is going on already.
<p>
Remember, in Quality Teams you can do as little or as much as you want. No experience is required, and you can contribute code, documentation, artwork, discuss user interfaces and usability, manage bugs and bug reports, manage the wiki pages, communicate between developers and the wider community, and promote KDE through the media. There's something for everybody :-)

<p>
<b>KDE Artwork</b>

<p>
This Quality Team has barely started, so if you've been contributing artwork on <A href="http://www.kde-look.org/">KDE Look</a> for some time, or if you'd just like to help out, there's plenty to do. The <a href="http://wiki.kdenews.org/tiki-index.php?page=Quality+Team+KDE+Artists">team page</a> suggests three simple tasks to begin with:

<ol>
  <li>Create artwork for specific events/applications (possibly through competitions on KDE Look)
  <li>Keep packaged artwork up to date
  <li>Find promising artwork and mark it on this page for KNewStuff integration with KDE applications
</ol>

To get started, read through the team page, subscribe to the KDE Artists mailing list and introduce yourself, then start on whatever interests you!

<p><br>
<b>KDE Edutainment</b>

<p>
The Edutainment team has seen lots of activity, including CDs containing CVS copies being sent out to contributors. There are still plenty of jobs going, so if you use Edutainment software, and would like to help shape its direction, have a look through their <a href="http://wiki.kdenews.org/tiki-index.php?page=Quality+Team+KDE+Edu">team page</a>

<p><br>
<b>KOffice</b>

<p>
The KOffice team now has new people working on KSpread, and is in need of more people to step up and volunteer for individual applications. There's also some web site work that needs doing, if you can migrate HTML pages to XHTML. Again, check the team page <a href="http://wiki.kdenews.org/tiki-index.php?page=Quality+Team+KDE+Koffice">here</a>.

<p><br>
<b>KDE Multimedia</b>

<p>
The Multimedia team has barely started any work, but there's plenty to do. The big decision over the multimedia framework (will it be aRts, Gstreamer, NMM, MAS, or another?) needs to be brought into the KDE community and beyond; the developers need help dealing with bugs that aren't related to KDE; and there's plenty more of the usual work. Find out more on their <a href="http://wiki.kdenews.org/tiki-index.php?page=Quality+Team+KDE+Multimedia">team page</a>.

<p><br>
<b>KDE Pim</b>

<p>
The PIM team has seen lots of activity, with a lot of work on updating documentation already begun. There's also an open job to work on calendars for KOrganizer, and an open task for developers for Kontact, all found on their <a href="http://wiki.kdenews.org/tiki-index.php?page=Quality+Team+KDE+PIM">team page</a>.

<p><br>
<b>Konqueror</b>

<p>
This isn't a team, so much as a project undertaken by various people on the Quality Team mailing list. To help Konqueror developers improve the KHTML rendering engine, <a href="http://wiki.kdenews.org/tiki-index.php?page=Konqueror%3A+The+Bugged+Websites">a page of incorrectly rendered web pages</a> has been created. This page needs contributions from frustrated Konqueror users, and volunteers to check over the page and help the KHTML developers get the most out of it.


<p><br>
<b>KDE Media</b>

<p>
Have you ever wanted to work in the media? If you want to write articles, do interviews, be interviewed, or help send press releases out and generally help promote KDE, then the Quality Teams Project is ready to help you. I have written an accessible <a href="http://quality.kde.org/develop/howto/howtomedia.php">guide to working with the media</a>, aimed at those who have never done media work before. So if you'd like to get involved, read the guide and introduce on the Quality Team Project mailing list. We can help you get articles into various major Free Software / KDE / software related publications, so what better way to start your career than by helping KDE?




