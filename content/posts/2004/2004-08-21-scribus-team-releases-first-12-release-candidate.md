---
title: "Scribus Team Releases First 1.2 Release Candidate"
date:    2004-08-21
authors:
  - "scribusdocs"
slug:    scribus-team-releases-first-12-release-candidate
comments:
  - subject: "That's too much for my old heart!"
    date: 2004-08-20
    body: "Yesterday KDE 3.3 was out (finishing the emerge process right now), then I saw the progress of X.org cvs...now one of my favourites appz (wich I used for work too!) it's about to reach v1.2! Thanks!! Thanks and thanks again!! That's fantastic...can't explain you how I'm happy!\n\nGreat!!\n\nMichele"
    author: "Imagino"
  - subject: "Screenshots"
    date: 2004-08-21
    body: "Anyone find it odd that screenshots are not screaming out to be clicked on the main page?  A quick visual scan of the top and left of the main page I don't see a link anywhere to be found.  Why isn't there a screenshots link?\n\n\n-Benjamin Meyer "
    author: "Benjamin Meyer"
  - subject: "Re: Screenshots"
    date: 2004-08-21
    body: "Fixed! Just go back to http://www.scribus.net and look on the left side of the menu.\n\nEnjoy,\nPeter"
    author: "scribusdocs"
  - subject: "Docking Windows"
    date: 2004-08-21
    body: "Scribus is a really cool program, but the only thing that annoys me is the floating windows as I find they get in the way. Is there not a way to make them dock, as just dragging them to the side doesn't do it..."
    author: "David"
  - subject: "Re: Docking Windows"
    date: 2004-08-21
    body: "Adding your wish to the ever growing wishlist. Scribus has no docking windows yet because these were very difficult to get right on Qt-3.1.x. But for the next developing cycle we will tage Qt-3.3.x as minimum requirement and there the docking windows will be better to implement."
    author: "fschmid"
  - subject: "Build problem"
    date: 2004-08-21
    body: "I have FreeType2 installed with prefix: /usr/X11R6 and I always have trouble building Scribus.  This time I can't figure it out.\n\nI run the 'configure' script and get this error:\n\nchecking for freetype-config... ./configure: line 3489: error:: command not found\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Build problem"
    date: 2004-08-23
    body: "Scribus has /usr/X11/bin in configure.in, but not /usr/X11R6/bin . In most distros one is a symlink to the other so it /should/ work. If your distro has the two as separate paths, configure won't find freetype. configure.in does not currently use pkg-config or look on the user's PATH for freetype-config (I'm working on changing that now, but I'm new to autotools - *shudder*). \n\nIn the mean time, try applying the following patch to configure.in and making sure that freetype-config is on your PATH:\n\n--- configure.in.orig   2004-08-23 14:59:30.000000000 +0800\n+++ configure.in        2004-08-23 15:19:53.000000000 +0800\n@@ -52,7 +52,7 @@\n LIBFREETYPE_CFLAGS=\"\"\n\n FREETYPE_CONFIG=\"\"\n-AC_PATH_PROG(FREETYPE_CONFIG, freetype-config, [AC_MSG_ERROR([Could not find libfreetype anywhere, check http://www.freetype.org/])], [${prefix}/bin ${exec_prefix}/bin /usr/local/bin /opt/local/bin /usr/bin /usr/X11/bin])\n+AC_PATH_PROG(FREETYPE_CONFIG, freetype-config, [AC_MSG_ERROR([Could not find libfreetype anywhere, check http://www.freetype.org/])], [${PATH} ${prefix}/bin ${exec_prefix}/bin /usr/local/bin /opt/local/bin /usr/bin /usr/X11/bin])\n\n hafree=no\n if test -n \"$FREETYPE_CONFIG\"; then"
    author: "Craig Ringer"
  - subject: "Re: Build problem"
    date: 2004-08-23
    body: "Thanks for the help.  I missed that.\n\nIt needed a little more work to get it to run:\n\nhttp://home.earthlink.net/~tyrerj/files/scribus-jrt.patch.bz2\n\nNote that these are patches to \"configure.in\" therefore, you need to regenerate after applying the patch.\n\nmake  -f admin/Makefile.common cvs\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Sorry..."
    date: 2004-08-22
    body: "Downloads dont work on the http://www.scribus.org.uk/ site :'(\n\nThe apache error:\n\nNot Found\n\n\nThe requested URL /scribus/downloads/snaps/scribus-snapshot.tar.bz2 was not found on this server.\n\n\nAdditionally, a 404 Not Found error was encountered while trying to use an ErrorDocument to handle the request."
    author: "Tony Espley"
  - subject: "Re: Sorry..."
    date: 2004-08-23
    body: "Try this:\nhttp://www.scribus.org.uk/downloads/snaps/scribus-snapshot.tar.bz2"
    author: "Craig Bradney"
---
The Scribus Team is pleased to announce the first release candidate for the
upcoming <a href="http://www.scribus.org.uk/">Scribus</a> 1.2. The Scribus team urges all users and distros to use
this latest release and test thoroughly. The Scribus 1.2 stable release, planned for later this summer, encapsulates over a year of development building on the original 1.0 release. Already, Scribus is being used in commercial settings including a new weekly newspaper launched this spring. The Scribus Team will be presenting at <a href="http://conference2004.kde.org/">aKademy</a> on using KDE and Scribus.



<!--break-->
<p>Among the major new features of Scribus 1.2:

<ul>
<li>A new story editor, complete with style and formatting functions.
Performance, stability and ease of use have been greatly enhanced.</li>
<li>Several new plug-ins, including exporting documents in many image formats, a font previewer and special hyphenation rules for Czech and other languages.</li>
<li>Many significant upgrades to the PDF exporter, including the ability to
export gradients with transparency, Open Type Fonts, set linescreen,
automatically adjust compression type and other advanced features.</li>
<li>A New Template and Save As Template function to make it easy for new users
to create documents, as well as create new templates.</li>
<li>Over 250 bug fixes and feature requests have been implemented.</li>
<li>A new EPS/PS importer, which can import EPS/PS and then edit the imported
vector objects as Scribus native objects.</li>
<li>Many improvements to the SVG importer/exporter. Scribus now can handle most
types of gradients, including transparency in imported SVG.</li>
<li>Dozens of new keyboard accelerators and extensive tooltips throughout.</li>
<li>A new print previewer, which has the ability to render CMYK separations on
screen, transparency, UCR (Under Color Removal) giving users a true
postscript generated preview of the page.</li>
<li>Many enhancements to text frames including multi-columned text, baseline
grids, new contour options, tabs and drop caps.</li>
<li>A new table feature to create basic tables within Scribus.</li>
<li>Extended support for right to left languages, including Hebrew and Arabic.
Scribus is now translated into 25 languages with more to come.</li>
</ul>
</p>
<p>The Scribus Team wishes to thank the many contributions and support we have
received from users, bug reporters and packagers who have helped to bring
Scribus to a new level.</p>

<p>Downloads, docs and sample files are available at <a href="http://www.scribus.net/">http://www.scribus.net</a>.
Please direct bug reports and feature requests to <a href="http://bugs.scribus.net/">http://bugs.scribus.net</a>.</p>


