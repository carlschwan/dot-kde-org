---
title: "KDE 3.2: A User's Perspective, Now Online"
date:    2004-04-10
authors:
  - "wkendrick"
slug:    kde-32-users-perspective-now-online
comments:
  - subject: "whoa"
    date: 2004-04-09
    body: "Thanks a lot Bill, there is a wealth of information here everything from GTK-Qt to DCOP and Kiosk Tool.  \n\nThis actually looks quite exciting!"
    author: "ac"
  - subject: "Can you make this a presentation with KD Exedutor?"
    date: 2004-04-09
    body: "Bill,\n\nthis is truly an amazing piece of work you have completed here!\n\nI hope you can/will present at least part of it in the User Conference part of \"aKademy\"! Or, maybe, pass it quickly thru \"KD Executor\" to generate a live demo/presentation from it?\n\nBoy, we could have some really smashing demos done with KD Executor for all kinds of presentations and events!\n\nCheers,\nKurt"
    author: "Kurt Pfeifle"
  - subject: "Re: Can you make this a presentation with KD Exedutor?"
    date: 2004-04-09
    body: "For online demos there could also be movies: \nhttp://lists.kde.org/?l=kde-promo&m=104430062329339&w=2\nhttp://lists.kde.org/?l=kde-promo&m=104456257121317&w=2\n\n(Though I haven't tried any of this, the links are from the \nKDE Wiki page http://kde.ground.cz/tiki-index.php?page=Tips+and+Tricks)\n\n"
    author: "cm"
  - subject: "Fantastic..."
    date: 2004-04-10
    body: "...but why are the fonts looking so awful.  I clearly remember having anti-aliased fonts on my KDE 2.2 desktop.  Did 3.2 go back to non AA fonts?"
    author: "rizzo"
  - subject: "Re: Fantastic..."
    date: 2004-04-10
    body: "AA fonts are optional. They are the default in KDE 3.2, but the guy who took the screenshots clearly doesn't use the defaults.\n\nNeither do I, for that matter.\n\n``Awful'' is in the eye of the beholder. Non-antialiased fonts look great to me, and I can't stand AA fonts. IMO, they're huge, fuzzy, ugly, and headache-inducing. I love the clear crispness of non-AA fonts, and I've no idea how anyone can stand using any GUI with AA enabled. To each their own, I guess..."
    author: "QV"
  - subject: "Re: Fantastic..."
    date: 2004-04-10
    body: "This really shouldn't be a problem if KDE supports 'crisp' anti-aliasing. However, I don't think it really supports multiple types of anti-aliasing, which is unfortunate."
    author: "jameth"
  - subject: "Re: Fantastic..."
    date: 2004-04-10
    body: "Check the freetype and Xft docs for ways to finetune the AA font rendering."
    author: "Roberto Alsina"
  - subject: "This is really excellent"
    date: 2004-04-10
    body: "It deserves a place on kde.org - a very neat walkthrough of some of the features of 3.2.\n\nI must have taken a fair bit of effort to put together - so thank you!"
    author: "Corba the Geek"
  - subject: "Nice work!"
    date: 2004-04-10
    body: "This is some top-notch presentation we got here, unfortunately we cant use it as a KDE promo because it doesn't follow the kde screenshot guidelines.\n\nIt would be an awesoem thing to include on KDE's page...but....\n\nBut the tour was really cool.  It shows non-KDE-ers some of the great things KDE has been doing for me all along :)"
    author: "standsolid"
  - subject: "Link on the top page"
    date: 2004-04-10
    body: "You might consider making a link on the top page to it. I sent that to a few ppl who have already responded back that they were able to learn a number of things from it."
    author: "a.c."
  - subject: "comments from osnews readers"
    date: 2004-04-10
    body: "http://www.osnews.com/comment.php?news_id=6677"
    author: "ac"
  - subject: "Theme choise..."
    date: 2004-04-10
    body: "Very nice review, but the screenshot have an -- IMO -- rather bad look.\n\nImagine all those screenshots with a nice theme (plastik)...  ...hmmmm..."
    author: "ac"
  - subject: "Slashdot has it too"
    date: 2004-04-11
    body: "http://slashdot.org/articles/04/04/11/1524257.shtml"
    author: "ac"
  - subject: "about the servicemenus example"
    date: 2004-04-11
    body: "In the example the %f parameter is used to indicate the filename.\nResizing multiple images will result in multiple instances of mogrify being started. All good and well when your resizing just a couple of images, but not as much fun when resizing a lot of pictures.\n\nIf you use the %F paramater instaed of the %f, only 1 instance of mogrify is started (with multiple filenames), and the images are converted one by one."
    author: "Johan Veenstra"
  - subject: "Writes some good games as well..."
    date: 2004-04-12
    body: "This is the same William (Bill) that writes excellent Linux games using SDL. Go to his homepage, http://www.newbreedsoftware.com/bill/, to view them."
    author: "Scott Patterson"
  - subject: "Re: Writes some good games as well..."
    date: 2004-04-17
    body: "Yes, I first encountered Bill over a decade ago when he was writing games for Atari computers and hanging out in comp.sys.atari.8bit. Always nice to see another old Atari guy making a splash in the modern free software world."
    author: "Rob Funk"
---
I gave my presentation "KDE 3.2: A User's Perspective" to my local Linux User Group <a href="http://dot.kde.org/1080706418/">last Tuesday night</a> as previously announced.  The talk was well received, and left some people (even KDE users) overwhelmed with new information.  It just goes to show that I wasn't the only one who knew KDE was a great environment, but hadn't even scratched the surface yet!  My presentation 'slides', a collection of over 3MB of screenshots of KDE in action, are now <a href="http://www.lugod.org/presentations/kde-user-persp/">online for your viewing pleasure</a> (<a href="http://static.kdenews.org/mirrors/www.lugod.org/presentations/kde-user-persp/">mirrored here</a>).  Enjoy!

<!--break-->
