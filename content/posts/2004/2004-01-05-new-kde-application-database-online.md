---
title: "New KDE Application Database Online"
date:    2004-01-05
authors:
  - "wbastian"
slug:    new-kde-application-database-online
comments:
  - subject: "The word is..."
    date: 2004-01-05
    body: "\"midst\"."
    author: "Joe Schmoe"
  - subject: "sidebar on dot.kde.org?"
    date: 2004-01-05
    body: "Will the apps sidebar be re-added into this site?"
    author: "anon"
  - subject: "Re: sidebar on dot.kde.org?"
    date: 2004-01-05
    body: "yeah, yeah...  first priority is to get the mailing lists working again then I'll this..."
    author: "Navindra Umanee"
  - subject: "A problem...."
    date: 2004-01-05
    body: "This new site allows people completely unreleated to an application to submit one.. this is a problem, because in the future, the author of the application might want to add links to a new version or whatnot, but can't control it..\n\nFreshmeat had the same problem, so they added a checkbox when you submit an application that said \"I am not the author or primary developer of this application\"... they also added a form for primary authors to \"claim ownership\". \n\nHow is kde-apps going to handle this?"
    author: "anon"
  - subject: "Re: A problem...."
    date: 2004-01-05
    body: "A future version of the system will be extended to handle this and other identified problems. At the moment you can still watch it change every day."
    author: "Anonymous"
  - subject: "All screenshots view"
    date: 2004-01-05
    body: "I like to get an impression of an application by viewing screenshots. It would be very convenient, if you can click on \"screenshots\" and see all screenshots of that application instead of just one. It's the same issue like at kde-look.org. When I watch a theme, I usually open 10 windows with screenshots and close them one after another."
    author: "Dominic"
  - subject: "Re: All screenshots view"
    date: 2004-01-05
    body: "<shameless plug>\nWell, if you want to see the screenshots for the wonderful app CubeTest, click here. Don't worry about the text. That funny language is Dutch.\nhttp://www.vandenoever.info/software/cubetest/\n</shameless plug>\n\nActually, I like the new site too. Some things could be more efficient. But may are, due to the inheritance from kde-look."
    author: "Jos"
  - subject: "Re: All screenshots view"
    date: 2004-01-06
    body: "So what is it ? ( My dutch is definitely not up to understanding that funny language :)"
    author: "CPH"
  - subject: "Re: All screenshots view"
    date: 2004-01-06
    body: "It's a small program that allows you to train your spacial insight, if that 's the correct term for it.\nA cube is shown and from 4 alternatives you have to pick the identical cube.\nTests like these often occur in job applications and school choice advises.\n\nThe description of the program is in dutch because it was written for a dutch contest. The goal of the contest was to write a computer program for primaty schools. I didn't win 1st prize (mobile phone) but even better: an inflatable tux of 1 meter high. Too bad this penguin has a small leak and its uptieme is only a few weeks.\n"
    author: "Jos"
  - subject: "improvements"
    date: 2004-01-05
    body: "Wonderful site!\n\n- corporate design:\nuse the logo of kde.org, red instead of blue at kde.org, light gray as on kde.org\n- internationalisation of the ui\n- show wether it is true kde or qt\n- kde-apps feed to kde.org seems to lack behind \n\nOther issues: Please backport KThemeManager into the 3.2 branch, 3.3 is too late. \nhttp://www.kde-apps.org/content/show.php?content=9838"
    author: "Gerd"
  - subject: "Re: improvements"
    date: 2004-01-05
    body: "> - show wether it is true kde or qt\n\nYou can see this if an application is marked to depend on Qt or KDE.\n\n> Please backport KThemeManager into the 3.2 branch, 3.3 is too late. \n\nIt's too late to be included into KDE 3.2."
    author: "Anonymous"
  - subject: "Re: improvements"
    date: 2004-01-05
    body: "No including it in 3.2. But ofering it as an external app"
    author: "Mario"
  - subject: "Re: improvements"
    date: 2004-01-05
    body: "It is offered as external application. What do you want?"
    author: "Anonymous"
  - subject: "Re: improvements"
    date: 2004-01-06
    body: "hope the distributors will include it and configure it as default"
    author: "elektroschock"
  - subject: "A few possible improvements"
    date: 2004-01-05
    body: "Some improvements not so far mentioned on the dot that I'd like to see are:\n\n- A distinction between an update and a new release; an updated record (e.g. a corrected spelling or a new download URL) shouldn't reappear on the front page, but new releases should. Otherwise we have no easy way of tracking when new KDE apps are released, which was one of the handiest things about apps.kde.com\n\n- Links to the changelog, screenshots page and download page as well as the home page;  kde-apps.org isn't suitable for a large formatted changelog, many screenshots or a more complex download list, so  quick links would be a nice courtesy\n\n- A mini description of each item and, where appropriate, a summarised changelog on the index pages; screenshots and names by themselves aren't sufficiently descriptive, and if a new version has come out it's useful to see on the index page what has changed\n\nIt's nice to see an app repistory back, anyway. It would be *really* nice if it could get even better than apps.kde.com!"
    author: "Tom"
  - subject: "Re: A few possible improvements"
    date: 2004-01-06
    body: "\" - A distinction between an update and a new release\"\n\nAnd also:\n-Whether it is new app in the database\n\nAnd see this problem in www.kde.org:\n\n\"06 January\nKWrite (Text Editor) 4.1 \nKWrite is a simple texteditor, with syntaxhighlighting, codefolding, dynamic word wrap and more, it's the lightweight version of Kate, providing more ...\n3.1.x\n\nKWrite (Text Editor) 4.1 \nKWrite is a simple texteditor, with syntaxhighlighting, codefolding, dynamic word wrap and more, it's the lightweight version of Kate, providing more ...\"\n\nWhy is it twice?\n\nOne bigger problem has been some programs with modules. There could have been ten or more modules and all of them were mentioned separately when a new release of a program appeared. They took all the space for new apps in the main page of www.kde.org."
    author: "Eeli Kaikkonen"
  - subject: "kde-look AND kde-apps account?"
    date: 2004-01-05
    body: "Any chance I could use a kde-look account for this site? They seem like relatives."
    author: "ac"
  - subject: "Re: kde-look AND kde-apps account?"
    date: 2004-01-05
    body: "yes i logged into kde-apps using my kde-look account.."
    author: "anon"
  - subject: "Re: kde-look AND kde-apps account?"
    date: 2004-01-05
    body: "My KDE-Look account never stays logged in.  I always have to relogin when I go to the site.  Anyone else seeing this problem?"
    author: "anonymous"
  - subject: "Re: kde-look AND kde-apps account?"
    date: 2004-01-07
    body: "Yes that's a problem. But now it will remember you sign-on name at least. You still have to login every time. But you don't have to type your name."
    author: "JusKickNit"
  - subject: "Thanks!"
    date: 2004-01-05
    body: "This is a great site - Period.\nNobody can really tell me that the ugly mess we've seen before\nas apps.kde.com can really match this.\nWay cool we've now got a non-commercial community-driven\napp repository.\n"
    author: "Mike"
  - subject: "Bandwidth saving idea"
    date: 2004-01-06
    body: "Just an idea - I can afford the bandwidth for the screenshots for my apps. Maybe in future the upload of screenshots could be optional. The links can just be to some other server like with the packages. Perhaps worth implementing?\n\nOtherwise this is so many times better than appsy. Also, the kde-look engine needs updating to generate useful <title>'s, but I'm sure that this is issue is known.."
    author: "Max Howell"
  - subject: "Re: Thanks!"
    date: 2004-01-06
    body: "Yes, apps.kde.com was not very attractive IMHO. With this new site there seems potential for kde-look folk to colleborate with kde-app folk on some things. They are both KDE centric, yet doing their own thing, share logins, share looks. They seem more consistant with each other then almost any other KDE related sites do. I think they are a good thing which only can get better!\n\nPerhaps a feature would be nice where KDE users can vote for apps or parts they like in apps they see on this new site to be in future KDE versions. It all seems much more accessable now.\n\n"
    author: "ac"
  - subject: "Re: Thanks!"
    date: 2004-01-06
    body: "Or special user requests about the usability and other criterias of applications."
    author: "elektroschock"
  - subject: "K3B"
    date: 2004-01-05
    body: "Hmmm, there's got to be a better category for K3B than Utilities?  :-) Maybe under Multimedia/Archiving?  I think one app has to be findable through multiple categories.\n\nIt's impossible to find with a search too.  I tried \"burner\" and \"cd burner\" and it didn't show any results.\n\nAnyway, that's peanuts.  This site is awesome.   Looks good.  Works good. The number of apps is growing so fast."
    author: "anonymous"
  - subject: "Re: K3B"
    date: 2004-01-06
    body: "Maybe the site's file structure should be synced with kde's menu?\n\nMaybe enabling dynamic syncing of software if the program in the system menu (installed locally on users machine) is updated on the site, prompting a \"wanna compile a update file for a <menu/program>?\" window so user could automatically update his/her software installation...\n\nOk, maybe a bit too large move to make it a repository-type system. :-)\n\nAnd a bit large move to make the kde as dynamically updated thingie..."
    author: "Nobody"
  - subject: "What happened to the old application site?"
    date: 2004-01-06
    body: "What happened to apps.kde.com?\n\n"
    author: "anonymous"
  - subject: "Re: What happened to the old application site?"
    date: 2004-01-06
    body: "Did you read the article at all???"
    author: "Mike"
  - subject: "Good stuff"
    date: 2004-01-06
    body: "Looks great! It also won't have all of the old unmaintained apps that were on apps.kde.org.\n\nOne annoying thing I find with this site (and kde-look.org) though - clicking on the thumbnail from the listing doesn't actually open up the image, it goes to the page for the item. Surely it would be more logical if the thumbnail linked to the image?"
    author: "Paul Eggleton"
  - subject: "Re: Good stuff"
    date: 2004-01-06
    body: "+1 Insightful\n\nThat gets me every time."
    author: "anonymous"
  - subject: "Re: Good stuff"
    date: 2004-01-06
    body: "> It also won't have all of the old unmaintained apps that were on apps.kde.org.\n\nAgreed, it was strange to have stuff like KDE Studio Gold and KNapster blocking the first places of the most downloads chart.\n \n"
    author: "Anonymous"
  - subject: "Re: Good stuff"
    date: 2004-01-06
    body: ">It also won't have all of the old unmaintained apps that were on apps.kde.org.\nNo, that's really a pitty that is.\nSometimes it's useful to know about an app, even if it isn't maintained anymore. Some apps written for KDE2 havn't been ported but would be worth porting and this might been done more easily than rewriting them. Or even if an rewrite was neccessary: Looking at the old apps you can get ideas of how things could be done.\n \n"
    author: "anonymous"
  - subject: "Re: Good stuff"
    date: 2004-01-07
    body: "That's true, however I think having a lot of old outdated applications would give new users a bad impression. I think we (as KDE supporters) want them to see what works, what's new, what's being used and actively developed right now."
    author: "Paul Eggleton"
  - subject: "Great stuff :)"
    date: 2004-01-06
    body: "I really like it but in my opinion the Multimedia section needs an additional entry. Something like CD/DVD because now I added K3b in Utilities because both Sound and Video seemed inappropriate.Now you think: \"Sure! He wants a category of his own.\" ;) I think there are many apps fitting in such a section like cd players and audio rippers and the like. Perhaps it could even be possible to add an application to multile sections (restricted to three or something).\nAnyway: thanks a lot for your work on this. It is really appreciated. :)\n\nCheers,\nSebastian"
    author: "Sebastian Trueg"
  - subject: "Defaults to screensaver"
    date: 2004-01-06
    body: "When I change an entry, the default 'area' is screensavers and if I forget to reset that correctly, then my app gets filed under screensavers.\n"
    author: "jalal"
  - subject: "Freshmeat feed"
    date: 2004-01-06
    body: "I tink apps.kde.com had some kind of freshmeat feed, where it would automatically add KDE applications submitted to freshmeat. This was a great way of adding new applications.\n\n"
    author: "Philippe Fremy"
  - subject: "Re: Freshmeat feed"
    date: 2004-01-06
    body: "No, it hadn't."
    author: "Anonymous"
  - subject: "Re: Freshmeat feed"
    date: 2004-01-06
    body: "I have seen more than once my applications appearing magically on apps.kde.com after I submitted them to freshmeat. So either it had a backend, or Andreas was parsing the freshmeat feed himself.\n"
    author: "Philippe Fremy"
  - subject: "Re: Freshmeat feed"
    date: 2004-01-06
    body: "I once silently put an update on my website, and it appeared on apps.kde.org (i didn't submit the update nowhere). Strange ...\n"
    author: "ik"
  - subject: "Re: Freshmeat feed"
    date: 2004-01-06
    body: "Andreas was always watching other sites and looking around for new applications and versions."
    author: "Anonymous"
  - subject: "Content needed"
    date: 2004-01-06
    body: "If you know of any application not added yet, submit them! KDE-Apps.org won't be a real replacement of apps.kde.com until more content is added. So, if you can remember things you had seen on apps.kde.com and can't find them here.\nAlso I do miss the binary packages, I must say."
    author: "anonymous"
  - subject: "Re: Content needed"
    date: 2004-01-06
    body: "> If you know of any application not added yet, submit them!\n\nNo! The apps-kde.org system is not designed for that yet.\n\n> Also I do miss the binary packages, I must say.\n\nYou paid for them at apps.kde.com?"
    author: "Anonymous"
  - subject: "Re: Content needed"
    date: 2004-01-07
    body: "FWIW, I tried several times to pay for the binary packages... I could not signup.  I send around five emails to the contact addresses listed and never got a reply."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Content needed"
    date: 2004-01-07
    body: ">You paid for them at apps.kde.com?\nYes, I did. Why not? Even though most of the applications listed where easy to build (./configure --prefix=/opt/kde3 && make && make install) I preferred to have RPM packages and since I just havn't got the time to always build my own spec-file, I was very happy with this service."
    author: "anonymous"
  - subject: "KDE apps"
    date: 2004-01-06
    body: "- better one big than 3 little preview screenshots. The shots as they are, are to small, so you always have to click on them and load the full image.\n\n- a preview screenshot can also be included in the packages. "
    author: "kalle"
  - subject: "Re: KDE apps"
    date: 2004-01-07
    body: "apps.kde.org had the link (all) to view all screenshots in one go. \nAnd why are screenshots shown in a new window? I don't like it when addidional unwanted windows cluttering my desktop!"
    author: "anonymous"
  - subject: "Own PayPal Account"
    date: 2004-01-08
    body: "kde-apps.org should have their own paypal account. Just payed what I (couldn't) afford from my wellfare payment, and I'd prefer it'd go  to to kde-apps.org specifically. Not that that I have anything against kde-looks - it just doesn't matter that much to me..."
    author: "tanghus"
  - subject: "Download several packages"
    date: 2004-01-09
    body: "Great work.\n\nHowever, that would be a good idea to enable give several links for download, e.g. Debian package, tarball...\nI mean a small table where you can access the different downloads available."
    author: "Renaud Lacour"
  - subject: "apps.kde.com not completely dead!"
    date: 2004-01-11
    body: "Thanks goodness! apps.kde.com isn't completely dead.\nI just hope it will experience a reinkarnation after they managed to find enough sponsors.\nIn some regards it's just way better than the new site Period\n"
    author: "anony-mouse"
---
Thanks to the hard work of <a href="mailto:frank@kde-look.org">Frank Karlitschek</a> during the holiday season, we are proud to welcome
<a href="http://kde-apps.org/">KDE-Apps.org</a> amongst our midsts. KDE-Apps.org is the new online database for KDE applications. Since the site is still very new, Frank would like to hear your suggestions for improvements. The database is also still a bit empty but you can change that by 
<a href="http://kde-apps.org/content/add.php">submitting</a>
the KDE applications that you have written to the site.

<!--break-->
<p>
Last year, the KDE application repository apps.kde.com so kindly provided by MieTerra dropped off the web due to financial problems.  We hope to have found a worthy successor in KDE-Apps.org
</p>

