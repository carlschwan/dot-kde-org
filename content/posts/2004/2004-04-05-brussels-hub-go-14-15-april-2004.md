---
title: "Brussels is the Hub to Go: 14-15 April 2004"
date:    2004-04-05
authors:
  - "l(ed.)"
slug:    brussels-hub-go-14-15-april-2004
comments:
  - subject: "UK"
    date: 2004-04-05
    body: "If anyone is driving across from the UK, myself and two friends are still waiting for a lift ;-)"
    author: "Tom Chance"
  - subject: "Re: UK"
    date: 2004-04-05
    body: "Please add yourself to this wikipage so others can find you:\nhttp://wiki.ael.be/index.php/Demo14and15aprilTransportation\n\nThanks,\nBram\n(demo.ffii.org webmaster)"
    author: "Bram"
  - subject: "Man..."
    date: 2004-04-06
    body: "If only the United States Congress could be so cool. Two-party systems are boring, we need to elect some Greens, but oh well."
    author: "Ian Monroe"
  - subject: "Re: Man..."
    date: 2004-04-07
    body: "http://demo.ffii.org/mvdbdemo.html\n\nYou could participate in the web strike...\n\nPatent law in Europe also affects the Us. We have to fix the system and get it under control again. Lets wipe the lawyers out :-)"
    author: "gerd"
  - subject: "Online demos"
    date: 2004-04-07
    body: "For those of us who are not in Europe, online demonstrations are a great\nway of participating and supporting the combined efforts to fight software\npatents.\n\nKDE.org and KDE.nl have the europatents logo in their hotspot, KStuff.org got a demo frontpage as well.\nOther KDE related sites should participate too, *especially* those who are geared (word pun!) towards end users.\n"
    author: "Josef Spillner"
  - subject: "Re: Online demos"
    date: 2004-04-08
    body: "Thank you so much for your support. \n\nI believe it is more important to attract a general audience. It is not just a matter of Free Software or Open Source, but affects the development of software at large and the Freedom of the market. Small&medium enterprises, ordinary websites may be targeted by trivial patents.\n\nhttp://webshop.ffii.org\ngives you an impression.\n\nKr\u00fcsse,\n\nAndr\u00e9"
    author: "Andr\u00e9, FFII"
---
Most European legal frame related to new technologies is cooked up at Brussels. Its future members will decide on the patentability of software, on data privacy issues, TPRM, and so on. On 14th and 15th April a conference and Linux User Group event chaired by Daniel Cohn-Bendit (member of European parlament) takes place. Join an install party within European Parliament (and bring your favorite MEP with you). Attend a panel with eg. Alan Cox, Georg Greve, Jon Lech Johansen (of decss fame). Participate in a guided tour through Brussels (<a href="http://demo.ffii.org/">Demonstration</a> for a <a href="http://en.wikipedia.org/wiki/Free_Information_Infrastructure">Free Information Infrastructure ("Anti-Swpat")</a>). Meet LUGs and development teams from all over the place, and the software patent experts of <a href="http://swpat.ffii.org/">FFII</a>.





<!--break-->
<p>As so many KDE contributors, companies and users are from Europe, this event is a good opportunity to present the progress of the project. Our European decision makers know very little about KDE as a mature desktop solution. KDE's great <a href="http://i18n.kde.org/">internationalization efforts</a> for instance are a practical solution to the requirements of the <a hreF="http://en.wikipedia.org/wiki/European_Charter_for_Regional_or_Minority_Languages">European Charter of Regional and Minority Languages.</a> Educate your EU representatives and officers about the importance of KDE for the Freedom of the European Information Infrastructure. Jens Mühlhaus (Munich City Council) will speak about the Munich Linux/KDE migration project.</p>

<p>Parliament entrance is free however to access the building you have to register online before 7 April: 
<a href="http://www.greens-efa.org/en/agenda/detail.php?id=1365">Greens Announcement</a> and <a href="http://greens-efa.org/pdf/agenda/euroLUGparty.pdf/">online registration form</a></p>

<h2>Free Information Infrastructure, 14 April 2004</h2>

<ul>
<li><a href="http://demo.ffii.org/">Brussels Demo on 14 April 11.30</a> and <a href="http://demo.ffii.org/online.php">online demo</A>.</li>

<li><a href="http://plone.ffii.org/events/2004/bxl04">Short interdisciplinary swpat conference (International Institute of Infonomics/FFII)</a> 14.00 - 18.00   European Parliament, room AG2</li>

<li>On 14 April evening, there will <a href="http://plone.ffii.org/events/2004/bxl04/soir/">be a diner meeting at restaurant La Tentation</a>, in the center of Brussels.</li>

<li><a href="http://plone.ffii.org/events/2004/bxl04">Event page of FFII, registration, hotels</a></li>
</ul>

<h2>Euro G/LUG party, Brussels European Parliament room ASP 1G2, 15 April 2004</h2>

<p>Contact: <a href="mailto:lvandewalle@europarl.eu.int">lvandewalle@europarl.eu.int</a></p>

<p>The Greens in European parliament invite representatives of GNU/Linux Users Groups of the 25 Member States of the European Union to come to Brussels to</p>

<ul><li> enhance the networking among the free software community in Europe(in particular with the New Member states)</li>

<li>prepare the second reading on the software patents directive</li>

<li>show inside EP what free software is, how it works and what ideas lie behind</li>

<li>participate to the FFII conference and demo against software patents on 14 April</li></ul>

<p>Programme and registration on http://www.greens-efa.org
Laurence Vandewalle: lvandewalle@europarl.eu.int</p>


<h3>PROGRAMME</h3>

<p>9.00-11.00 25 G/LUGs for a Free Europe</p>

<p>Gathering European GNU/Linux Users Groups and associations for the promotion of free software : BxLUG - Belgium, RWO - Plug - Poland, Vrijschrift - The Netherlands, LiLux - Luxemburg, FFS Software - Austria, APRIL - HNS-info.net - France, GUUG - Germany, SSLUG - Sweden&Denmark, LUGOS - Slovenia, Debian - Latvia, AKL - Lithuania, LugRoma - Italy, Grece, Cyprus, Finland, Estonia, ...</p>

<p>11.00-12.30 Linux Install Party for MEPs with Monica Frassoni Dany Cohn-Bendit, Hiltrud Breyer, Bart Staes, ... organized by BxLug</p>

<p>15.00 PANEL I: FAIR USE/COPIE PRIVÉE</p>

<p>Gwen Hinze (Electronic Fronteer Foundation)</p>

<p>Laurence Lebersorg (Test-Achat Belgium)</p>

<p>Jon Lech Johansen (DVD-Jon)</p>

<p>16.00 PANEL II: FREE/OPEN SOURCE SOFTWARE</p>

<p>Cristiano Paggetti (Italy): eGovernment</p>

<p>Andrea Glorioso (Italy): Free Content</p>

<p>Herman Bruynickx (Belgium): Free software in education</p>

<p>Jens Muhlhaus (Germany): Public administration: Linux für Munchen</p>

<p>17.00 PANEL III : FREE AS IN FREEDOM</p>

<p>Georg Greve, FSF Europe (Germany) Agenda 1910</p>

<p>17.30 Alan Cox www.linux.org.uk co-signatory of the letter sent by Linus Torvalds to the President of EP against software patents (UK)</p>


<p>Links:</p>

<ul>
<li><a href="http://greens-efa.org/pdf/agenda/euroLUGparty.pdf/">European Greens/European Freedom Alliance registration</a></li>

<li><a href="http://laurence.domainepublic.net">Laurence's Blog</a></li>
</ul>




