---
title: "The Richmond Post: Interview with George Staikos about KaXul"
date:    2004-06-19
authors:
  - "rmoore"
slug:    richmond-post-interview-george-staikos-about-kaxul
comments:
  - subject: "XUL is cool"
    date: 2004-06-19
    body: "There is alot of potential in using XUL as the language of more advanced websites to give you a feel of a \"real\" application running on your computer rather than just another HTML page. <BR>\n<P>I'd love to see the RDF functionality combined with XUL as it is a much better way of rendering lists (whether dynamic or not) rather than generating HTML elements either on the server or the client. Obviously RDF also adds more semantic meaning to your data and gives other users of the data the ability/choice of rendering the data in a differnet visual organization. <BR>\nHave a look at the <a href=\"http://plone.org\">plone.org</a> project as it would be a perfect candidate as a web application which would really benefit from an application look ( which is not easy to do using standard HTML/CSS).\n<BR>\n<I>\n(why does the encoding only have \"plain text\" and not allow me to select HTML - at least the preview does not seem to work :( </I>"
    author: "CPH"
  - subject: "Re: XUL is cool"
    date: 2004-06-19
    body: "> (why does the encoding only have \"plain text\" and not allow me to select HTML\n> - at least the preview does not seem to work :(\n\nHTML in comments has been disabled forever in this site.  I hope one day they will enable it again."
    author: "em"
  - subject: "Re: XUL is cool"
    date: 2004-06-20
    body: "I believe it is planned to implement HTML again in the day after forever. :P"
    author: "anonymous_penguin"
  - subject: "Re: XUL is cool"
    date: 2004-06-20
    body: "Ah, I thought that problem had been fixed.\nYou'd think then that they would also remove the \"Encoding\" button which seems to give you the option for HTML formating and removes the \"allowed HTML\" message in the preview window."
    author: "CPH"
  - subject: "Combine this with Kommander"
    date: 2004-06-20
    body: "I recently tried to play somewhat with Kommander after hearing all this good stuff about it. And a good tool it is, however you always need the Kommander Executor to run the applications. Now if Kommander and KaXUL could be combined to have an easy to use environment to create applications that can run on all systems with Mozilla and/or KDE installed, that really would be a killer. Just some thoughts... ;)"
    author: "Arend jr."
  - subject: "Re: Combine this with Kommander"
    date: 2004-06-24
    body: "I completely agree with you! I was thinking the same thing :)"
    author: "alonso"
  - subject: "Year old project"
    date: 2004-06-24
    body: "I (like probably many people) looked at this article and thought it was a really great *new* idea. Then reading the article I'm surprised to find that this is all quite old work now, with not really anything going on with it currently. Looking at the CVS entries the latest work (except kpart) is around 10 months old. This nifty idea was long ago implemented, but I (and i suspect most) never heard of it. \n\nProbably there's a lot of neat little projects like this...\n\nHopefully this revives interest in a cool concept."
    author: "Tim Middleton"
  - subject: "How about Mozilla plugins?"
    date: 2004-06-25
    body: "there are many good plugins for mozilla, firefox, firebird. can't konqueror use those?"
    author: "fast_rizwaan"
  - subject: "Does this work in run-time"
    date: 2004-06-25
    body: "While converting XUL files to QtDesigneer .ui files is easy enough I'd imagine, surely you'd then need to compile things to get them running, right?\n\nOr have I missed something big here."
    author: "Bryan Feeney"
  - subject: "Re: Does this work in run-time"
    date: 2004-06-25
    body: "http://doc.trolltech.com/3.3/qwidgetfactory.html\n\nQuote: \"The QWidgetFactory class provides for the dynamic creation of widgets from Qt Designer .ui files.\""
    author: "Christian Loose"
---
<a href="http://xul.sourceforge.net/post/">The Richmond Post</a>, a news site covering adoption of the <a href="http://www.mozilla.org/">Mozilla</a> XUL UI description language is current featuring an interview with KDE's own George Staikos. George is well known for his work on applications such as <a href="http://www.kdetv.org/">kdetv</a> and <a href="http://linuxreviews.org/kde/kde-user-persp/kwallet.html">KWallet</a>, but he is also working on a facility that allows XUL applications to use native KDE widgets. The interview is in two parts: <a href="http://xul.sourceforge.net/post/2004/06/xul_titan_interview_george_staikos_of_xul_for_qtkde_fame_part_i.html">Part One</a> and <a href="http://xul.sourceforge.net/post/2004/06/xul_titan_interview_george_staikos_of_xul_for_qtkde_fame_part_ii.html">Part Two</a>.



<!--break-->
