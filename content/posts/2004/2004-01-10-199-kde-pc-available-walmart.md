---
title: "$199 KDE PC available from Walmart"
date:    2004-01-10
authors:
  - "kpfeifle"
slug:    199-kde-pc-available-walmart
comments:
  - subject: "linuxworld"
    date: 2004-01-10
    body: "Any chances on getting one from NYC during next linuxworld (somewhere around manhattan)?"
    author: "jmk"
  - subject: "OO.o?"
    date: 2004-01-10
    body: "Good luck trying to run openoffice with 128 megs of memory :) Good thing KDE doesn't require so much."
    author: "Synonymous coward"
  - subject: "Re: OO.o?"
    date: 2004-01-10
    body: "Well, maybe koffice could get the attention it deserves?\nOO.o is in my opinion totally bloted memory hungry and ugly application(s).\n\nI personally just love kword and I don't need the compability for MS Word...\n\n/Dave"
    author: "David"
  - subject: "Re: OO.o?"
    date: 2004-01-10
    body: "ooops, should be bloated!"
    author: "David"
  - subject: "Re: OO.o?"
    date: 2004-01-10
    body: "I love KWord.\n\nI don't need printing capabilities..."
    author: "noname"
  - subject: "Re: OO.o?"
    date: 2004-01-10
    body: "And I love replies :) but don't really need them..."
    author: "David"
  - subject: "Re: OO.o?"
    date: 2004-01-10
    body: "Do you love KOffice? No, I love my wife!"
    author: "kalle"
  - subject: "Re: OO.o?"
    date: 2004-01-11
    body: "Well, I like KWord, and I use it for a lot of short (one-page) documents. However, if you ever tried to create a document with 10s or even 100s of pages, tables, graphics etc., you'll quickly realize that OO.o is not yet obsolete.\n\nAgain, I like KWord and I hope to see it prosper, but you'll have to face the fact, that it still does have a long way to go in order to fully replace OO.o.\n\nThomas"
    author: "Thomas2"
  - subject: "Re: OO.o?"
    date: 2004-01-11
    body: "Ya, KWord even admits that it's not optimized for large documents."
    author: "Luke Sandell"
  - subject: "Re: OO.o?"
    date: 2004-01-11
    body: "That's true, but I believe and hope that kword will reach a phase where it is adequate for most \"normal\" needs. But maybe the biggest problem is to attract enough programmers... to speed up the development!\n\nKoffice will eventually have it's revenge =)\n\n/David\n"
    author: "David"
  - subject: "Re: OO.o?"
    date: 2004-01-11
    body: "No, OO.o is definitely not obsolete :) Its rather in its early years, and undergoing rapid development. Healthy competition all round."
    author: "Freddled gruntbuggly"
  - subject: "Re: OO.o?"
    date: 2004-01-11
    body: "Open office is in it's early yeras?\nIt's allmost 20 years old!!\n\n(I guess, when was StarOffice founded?)\n\n\nRinse"
    author: "rinse"
  - subject: "Re: OO.o?"
    date: 2004-01-14
    body: "I'm not a professional user or anything, but I run OOo on my home computer all the time. It's a Celeron 766 with 128MB, running SuSE 9.0. It does take a long time to initially load, but I keep everything from my budget spreadsheet to my DVD Collection in it and it seems to be fine."
    author: "Bryce Hardy"
  - subject: "128..."
    date: 2004-01-10
    body: "Just doesn't make sense, it's a 1.3 Ghz machine! 128Mb RAM?\nI got my PII 350Mhz running KDE 3.1.4 and OpenOffice.Org. OOo is preloaded with quickstarter. Everything runs fine, the whole system is quite responsive and I believe that it's more than enough to many people out there. But I have 256+32Mb RAM on that machine!\n\nAnyway, nice initiative. \n"
    author: "Source"
  - subject: "Re: 128..."
    date: 2004-01-10
    body: "Slow systems for slow users."
    author: "noname"
  - subject: "Re: 128..."
    date: 2004-01-10
    body: "Yep, kde feels so much better from 256 megs on.\nI've tried it on a PIII600 and going from 128 TO 256 makes it much comfortable to use. Yet, it IS usable aith a 600 and 128, but kinda slow-i-sh.\nGoing from pIII600 to 2000 didnt bring as much of a change as going 256 from 128."
    author: "djay"
  - subject: "I would have used"
    date: 2004-01-10
    body: "I would have used fedora it is more user freindly most first time use my get lost!"
    author: "jediwolf"
  - subject: "Re: I would have used"
    date: 2004-01-10
    body: "huh?"
    author: "ciasa"
  - subject: "Re: I would have used"
    date: 2004-01-10
    body: "Is Yoda that you?"
    author: "cm"
  - subject: "No CD or Floppy Drives?"
    date: 2004-01-10
    body: "Not that I am a Lindows fan, but unlike Linare's $199 computer, Lindow's PC at walmart.com comes with a 1.4 GHz Duron plus a CD Rom (no floppy though).<br>\n<a href=\"http://www.walmart.com/catalog/product.gsp?cat=3951&dept=3944&product_id=2293918\">\nhttp://www.walmart.com/catalog/product.gsp?cat=3951&dept=3944&product_id=2293918</a>\n<p>\nThat said, I am excited to see more vendors in the cheap PC market pre-installing KDE based Linux distributions. This is a large market, and I believe success here is the first step to conquering many user's desktops. I hope that these vendors take the time and care to setup KDE so that these users' first experience with KDE and Linux will be pleasurable.\n<p>\nI'm constantly amazed at the progress KDE has made in only a few years. KDE is an incredibly configurable, feature-full, and integrated environment all at the same time. Of course there is still work to be done, and I hope that the developers will continue to polish every area of KDE, but I'm proud to call KDE my desktop environment of choice!\n\n\n"
    author: "dapawn"
  - subject: "Aldi"
    date: 2004-01-10
    body: "WalMart got bankrupt in Germany, or at least their expansion strategy failed. The prices were to expensive so wired. 154,78 EURO or alike, I read in an US Marketing book it shall indicate sharp calculation to the customer... well, we are not that stupid :-)\n\nHowever it would be nice to see an ALDI Linux :-)\n\nAgain I will fill out Medion's guestionaires at CeBIt 04 asking for a Linux PC :-)"
    author: "kalle"
  - subject: "Linare "
    date: 2004-01-10
    body: "Anyone has any info on Linare's OS ? Is it .deb based ? RPM based ? Distrowatch has no info on Linare because, well, Linare does not provide any apparently ..."
    author: "MandrakeUser"
  - subject: "Re: Linare "
    date: 2004-01-11
    body: "I met some guys from Linare. They started from redhat9, threw away whatever Joe Enduser doesn't need (its a single CD distro. They gave me one, never got time to try it), and they bought licenses from Fraunhofer and a couple of other prop. formats. "
    author: "Freddled gruntbuggly"
  - subject: "Re: Linare "
    date: 2004-01-12
    body: "Thanks a lot Freddled, that's the first piece of info I get on Linare ;-) It wouldn't hurt them to give some technical details on their site ..."
    author: "MandrakeUser"
  - subject: "Re: Linare "
    date: 2006-04-26
    body: "The comoany is now closed, our attempts to reach Linare ceo Mr. Soma has been unsucessful. Can any one help us to reach Linare representatives."
    author: "Mahesh"
  - subject: "Re: Linare "
    date: 2005-01-27
    body: "Linare is RedHat based"
    author: "Gordy"
  - subject: "Re: Linare "
    date: 2006-08-14
    body: "I bought the Linare INBS250. Its a fine machine. I ran Linspire 4.5 then Linspire 5.0 and now Mepis 6. I've been very happy with it. I'm sorry to see they closed shop.\n\nhttp://www.gospelinux.net"
    author: "Tov"
  - subject: "1.3 GHz and 128 MB of RAM"
    date: 2004-01-11
    body: "Not a very good choice.\n\n256 MB of RAM and a slower processor would be a much better choice.\n\nOTOH, I suppose that you can add more RAM.  But, if you aren't going to add RAM, this is clearly a poor choice.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: 1.3 GHz and 128 MB of RAM"
    date: 2004-01-11
    body: "Yes its definitely a poor choice. \n\nThis system isn't aimed at the kind of people who would add more RAM. They wouldn't even know it exists.\n\nUnfortunately these people also think more gigahertz == faster (deliberately propagated by intel) and so they wouldn't settle for a \"slow\" machine."
    author: "Freddled gruntbuggly"
---
According to a recent <A HREF="http://www.desktoplinux.com/news/NS3916364482.html">news item at DesktopLinux.com</A>, Walmart.com now offers a <I>"1.3 Ghz AMD processor, host 128 MB of RAM, 30 GB of hard-drive space, ethernet interface, keyboard, mouse and speaker"</I> PC for a mere $US 199.95. The system is <I>"fully loaded with Linare's Linux, based on KDE"</I> and <I>"the system comes with access to Linare's 24-hour, seven-day-a-week technical support phone line."</I>
A more <a href="http://linare.zoovy.com/product/00012345678">
detailed description</a> of the hardware
is available on the website of <a href="http://linare.zoovy.com/">Linare</a>, the software is <a href="http://linare.zoovy.com/product/00012345679">
described here</a>.
The PCs are
<a href="http://www.amazon.com/exec/obidos/ASIN/B0000DYT8J/104-0173005-2279140">
also available from Amazon.com</a>. Earlier last year,  
<a href="http://news.com.com/2100-1046-5089992.html">
CNET News.com reported</a>
on the plans of the PC maker to
<a href="http://linare.mail.everyone.net/email/scripts/serviceMenu.pl">
offer an e-mail service</a>
as well.

<!--break-->
