---
title: "KDE Community World Summit 2004 \"aKademy\""
date:    2004-04-07
authors:
  - "ateam"
slug:    kde-community-world-summit-2004-akademy
comments:
  - subject: "Tempting, very tempting"
    date: 2004-04-08
    body: "My english is poor, and I'm afraid nobody will understand me in Germany :-), but it looks very tempting."
    author: "suy"
  - subject: "Re: Tempting, very tempting"
    date: 2004-04-08
    body: "KDE conferences are mostly conducted in English.\n"
    author: "Richard Moore"
  - subject: "Re: Tempting, very tempting"
    date: 2004-04-08
    body: "Interestingly, when you ask a German if they speak English, they say they only speak a little and then surprise you with what is actually quite good English.\n\nVisitors who don't speak German needn't worry, as getting by with English is normally not a problem.  Visitors who do speak German may have a problem, unless they speak Schw\u00e4bisch too!\n\nUnfortunately, I think the Fr\u00fchlingsfeschtle, which just started in Stuttgart, will have finished by the time the conference starts.  The Film Academy, where the conference is being held, has produced some really cool short films - maybe the KDE visitors will get to see a couple...\n\n-- Steve"
    author: "Steve"
  - subject: "In my Town"
    date: 2004-04-08
    body: "The best thing is that I live just down the road of the Film Akademy. It's just 3 Busstops away or a 15 minutes walk.\nMaybe someone from somwhere far away wants to stay at my place fo a Weekend. \nI would have to check with my Grilfriend though.\n"
    author: "Hermann"
  - subject: "Re: In my Town"
    date: 2004-04-08
    body: "### Maybe someone from somwhere far away wants to stay at my place fo a Weekend.###\n\nI guess you are over-booked already now?  ;-)\n\nIf you are so supportive for KDE developers and conferences, how about joining\nthe local organizing team as a volunteer and help with the preparations and the\nactually running of the event? We can use any helping hand available... (even \nthose of girlfriends ;-)\n\nPlease, get in touch if you'd like to help even more... \n\nThanks and cheers,\nKurt"
    author: "Kurt Pfeifle"
  - subject: "yes yes"
    date: 2004-04-08
    body: "I definitely want to come, especially \"KDE Users' Conference: August 28th - 29th\" I'm looking to come over that weekend. I think it is cool this event is also meant for users ... \n\nCiao \n\nFab"
    author: "Fabrice Mous"
  - subject: "Hope to attend this one!"
    date: 2004-04-08
    body: "Well I hope I will be able to attend this one. N7Y looked very cool 8)"
    author: "cartman"
  - subject: "announcement"
    date: 2004-04-08
    body: "Why not announce this on other sites like LinuxToday, Newsforge, OSNews?  Especially since users are invited?"
    author: "ac"
  - subject: "Re: announcement"
    date: 2004-04-08
    body: "### Why not announce this on other sites like LinuxToday, Newsforge, OSNews? ###\n\nWhy not write up and submit a news item to them?\n\nWe are relying on volunteers. The press releas was sent out to lots of addresses\n(in English, German and Spanish), and it is up to them to publish it. Sometimes it helps to \"remind\" an overworked editor about a news item by a second, independently-worded story.  (They will make sure to not publish it twice -- and if it happens it won't be to KDE's disadvantage...  ;-)\n\nKDE promotion can always have one more volunteer halping and contributing a bit.\n\nWhy not publish the announcement on the mailing list of your local LUG? Why not send it to your local newspaper? Why not translate it into your own language and publish it on popular sites and lists there? (Even if people won't come, they may  still like to know what KDE is planning for.....)\n"
    author: "Kurt Pfeifle"
  - subject: "Partners program?"
    date: 2004-04-09
    body: "Is there going to be a partners' program? I am booked to come over (yep - only 22 hours of flying time each way from Canberra to Frankfurt, then few hours on a train to Stuttgart :-), and am bringing my girlfriend. If she has something else to do, I can hack more...."
    author: "Brad Hards"
  - subject: "Re: Partners program?"
    date: 2004-04-09
    body: "There is certainly a lot to do and watch and visit and sightsee in and around Stuttgart (even if it is not a typical tourist area).\n\nHowever we haven't planned a \"partners' program\" yet.  We are looking into preparing some general \"besides-the-hacking\" tourist activities for everybody. But we may well consider to enable a checkbox in the registration form \n\n  --> [_] I want to bring my partner \n\nso we can see how much interest there is. If it is more than half a dozen, these could self-organize their activities well in advance (with the help of one or two local people, of course).\n\nHow about that?"
    author: "Kurt Pfeifle"
  - subject: "Re: Partners program?"
    date: 2004-04-11
    body: "Hmm... wrote a reply last night and submitted it but seems like it got lost in cyberspace.\n\nHop over to the People Behind KDE website http://www.kde.nl/people/news.html\nWe were discussing it last week. Birte, Till Adam's wife started a group on Orkut, http://www.orkut.com called KDE Widows. A group for the partners behind KDE contributors, it's meant to provide a way to get in touch with each other and plan/organize a get-together at conferences and do some fun stuff together.\n\nAt the moment we only have a few members but I'm sure there must be a lot more out there. Let me know if you would like Birte's email address so you can pass it on to your gf or if you want an invitation to join Orkut.\n\n--\nTink"
    author: "Tink"
  - subject: "Re: Partners program?"
    date: 2004-04-11
    body: "Two hours ago, I had an enthusiastic KDE user from the local area (who had\nalready previously offered his help in some organizational tasks) write me\nwith the new message \"my girlfriend is also willing to help. She is not so\nmuch in computers, but she has now started to compile a list of worth-while\ntourist attractions in the Stuttgart area and find out the details about\npublic transport\".\n\nSo it looks like Brad hasn't any excuse left to come and leave his partner at home...  ;-)"
    author: "Kurt Pfeifle"
---
The KDE Project is proud to announce its plans for the "<a href="http://conference2004.kde.org/">KDE Community World Summit 2004</a>", code-named "aKademy". The first edition of this new international event will be held in Ludwigsburg, Germany, near Stuttgart. Taking place from August 21st to 29th 2004, the Summit will include an exciting program of talks, presentations, tutorials and joint development, bug-fixing, design and polishing work on the leading Linux and Unix desktop environment.





<!--break-->
<p>All active KDE contributors (programmers, artists, documentation writers, translators, promoters and other supporters), interested KDE power users as well the general public are invited to come, attend and participate in the discussions and work.</p>

<p>The Summit is divided into sections in order to provide each target group its own forum:<br>
<ul>
<li>KDE Developers' and Contributors' Conference: August 21st - 22nd</li>
<li>KDE Improvement Coding Marathon: August 23rd - 27th</li>
<li>General Linux and KDE User and Admin Tutorials: August 23rd - 27th</li>
<li>KDE Users' Conference: August 28th - 29th</li>
</ul>
</p>

<p>In addition, the "<a href="http://softwarefreedomday.org/">First International Software Freedom Day</a>" will be celebrated parallel to the Summit on August 28th.</p> 

<p>Detailed information on all aspects of the Summit including registration information will be available on the website <a href="http://conference2004.kde.org/">http://conference2004.kde.org/</a>.</p>

<p>The KDE Community World Summit 2004 is jointly organized and run by <a href="http://www.kde.org/areas/kde-ev/">KDE e.V.</a>, <a href="http://opensource.region-stuttgart.de/">Wirtschaftsförderung Region Stuttgart GmbH</a> (WRS), and <a href="http://www.linuxnewmedia.com/">Linux New Media AG</a>.</p>


