---
title: "Distro Quickies: KDE 3.3 on Every Desktop Distribution"
date:    2004-12-21
authors:
  - "binner"
slug:    distro-quickies-kde-33-every-desktop-distribution
comments:
  - subject: "KDE 3.3"
    date: 2004-12-21
    body: "Doesn't suprise me - KDE's still the most acomplished, fully featured & polished desktop environment out there - file-management speed in Konq's particularly impressive - thanks to all the developers - looking forward to 3.4 and then 4 ;)"
    author: "anon"
  - subject: "Re: KDE 3.3"
    date: 2004-12-22
    body: "I only find it slightly ironic that this was announced when 3.4 as already reached alpha."
    author: "Ian Monroe"
  - subject: "Slackware too"
    date: 2004-12-21
    body: "Patrick recently said in the slackware-current changelog that he's considering releasing Slackware 10.1 -- which would include KDE 3.3.2, since that the one in --current right now."
    author: "Mikhail Capone"
  - subject: "Re: Slackware too"
    date: 2004-12-22
    body: "Indeed. \n\nSlightly OT: Does anybody know when we can expect Slackware 10.1? :-/"
    author: "MuD"
  - subject: "SUSE 9.2"
    date: 2004-12-21
    body: "It was relased with KDE 3.3.x too."
    author: "John SUSE Freak"
  - subject: "Debian too!"
    date: 2004-12-21
    body: "3.3. is ready in unstable and just waiting on something else before moving into sarge/testing."
    author: "Benjamin Meyer"
  - subject: "Re: Debian too!"
    date: 2004-12-21
    body: "Debian is mentioned in above story! And not everyone feels like running unstable/current/whatever unreleased branches."
    author: "Anonymous"
  - subject: "Re: Debian too!"
    date: 2004-12-22
    body: "It's waiting for people to test non-official KDE applications (e.g., k3b, kaffeine, digikam, showimg, kvim, kile, gwenview, konserve and a lot more) with the 3.3 libraries.[1]\n\n[1] http://lists.debian.org/debian-kde/2004/12/msg00043.html"
    author: "d w"
  - subject: "Re: Debian too!"
    date: 2004-12-22
    body: "You lost me at unstable and testing."
    author: "GentooUser"
  - subject: "NetBSD 2.0!"
    date: 2004-12-21
    body: "The NetBSD 2.0 LiveCD also features KDE (3.2.2 though):\n\nhttp://lists.kde.org/?l=kde-promo&m=110300327207952&w=2\n\n</me_too> :-)\n\n"
    author: "Navindra Umanee"
  - subject: "ProMepis"
    date: 2004-12-22
    body: "is ProMEPIS a free Distribution in the same sense as SimplyMepis?"
    author: "katakombi"
  - subject: "Re: ProMepis"
    date: 2004-12-22
    body: "Yes. They only differ in package selection (and included versions depending on when what product was released)."
    author: "Anonymous"
  - subject: "xandros 3.0"
    date: 2004-12-22
    body: "after trying xandros 3.0, i said what is this crap!!! this is not KDE, where is the feel we all know and love, and even if you compare it to windows it doesn't feel right!! Why some company release such inferior quality product?"
    author: "AC"
  - subject: "Re: xandros 3.0"
    date: 2004-12-22
    body: "xandros 3.0 is more or less like a windows clone in my opinion"
    author: "Anonymous Spectator"
  - subject: "Re: xandros 3.0"
    date: 2005-01-15
    body: "I like it"
    author: "jack Daile"
  - subject: "esp. after all the buzz..."
    date: 2004-12-22
    body: "...that tod-\"the other desktop\" 2.6 (2.8?) will be included in the next debian release, it's good to see that even the folks at debian put their pants on one leg at a time (hey... I for one will start to look for something unstable in in kteatime now and than check for license issues again. QT is, as everybody knows, not GPL. I will than post my findings at...blurb...slashdot.)"
    author: "Thomas"
  - subject: "Re: esp. after all the buzz..."
    date: 2004-12-22
    body: "> QT is, as everybody knows, not GPL.\n\nWhat are you talking about?"
    author: "Anonymous"
  - subject: "Re: esp. after all the buzz..."
    date: 2004-12-22
    body: "He's right. QT (QuickTime) is not GPL. But he probably wanted to say \"Qt is, as everybody knows, not GPL.\". And it was obviously meant to be a joke.\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: esp. after all the buzz..."
    date: 2004-12-22
    body: "> It was obviously meant to be a joke.\nright... at least someone saw the irony.... This could start big discussions about how to get nuances like irony across to the reader without using hinting tags like <irony>. But after reading my first post again, I think it's indeed obvious..."
    author: "Thomas"
  - subject: "Re: esp. after all the buzz..."
    date: 2004-12-22
    body: "Clearly he doesn't know what he's talking about:\n\nhttp://www.trolltech.com/products/qt/opensource.html"
    author: "Johan Veenstra"
  - subject: "Whats RTLD_DEEPBIND??"
    date: 2004-12-22
    body: "I accidentially noticed this in Michael Meeks blog:\n\n\"Interesting mail from Mike Hearn of Codeweavers fame about the new glibc RTLD_DEEPBIND support; possibly useful for accelerating OO.o startup - iff C++'s weak symbol addiction can cope. \"\n\nOn 23 sept Uli Depper commited this\n\"I just committed a set of patches implementing an often requested\nfeature.  If dlopen is used with this flag, the objects thusly loaded\nget a lookup scope which has the scope of the loader before the global\nscope.  I.e., if the objects are self-contained they will not use the\nglobal scope.  This can be used to work around problems of morons who\ncannot keep ABIs stable and have the same symbol name in different DSOs.\"\n\nCould it be used to improved KDE startup?\n\n\n"
    author: "performa 300"
  - subject: "No KDE 3.3 on a stable Mandrake."
    date: 2004-12-22
    body: "The problem is that there is not stable Mandrake (10.1) that has KDE 3.3 (still 3.2.3).\n"
    author: "Crouching Tiger"
  - subject: "Re: No KDE 3.3 on a stable Mandrake."
    date: 2004-12-22
    body: "From what I've heard, it's on the third cd, is'n it?"
    author: "Anonymous"
  - subject: "Re: No KDE 3.3 on a stable Mandrake."
    date: 2004-12-22
    body: "Actually 4th CD if I recall correctly :-) Basically it's not avalible in the free downloads, only to club members and paying custommers."
    author: "Morty"
  - subject: "Re: No KDE 3.3 on a stable Mandrake."
    date: 2004-12-24
    body: "I found binaries for Mdk 10.1 here.\nSeems to work on my PC\nhttp://anorien.csc.warwick.ac.uk/mirrors/thac/10.1/RPMS/kde-3.3.2/\n\nor this\nhttp://rpm.pbone.net/index.php3/stat/14/idka/36175/datan/2004-12-16\n(did not have a try)"
    author: "gerard"
  - subject: "Linspire New Release"
    date: 2004-12-23
    body: "I hear is based upon KDE 3.3.2."
    author: "Denny"
  - subject: "not on demudi"
    date: 2004-12-23
    body: "kde isn't released with agnula/demudi 1.2.0  What is the easiest way to get it?  Can i installed kde3.3 through apt?  what the repository?"
    author: "gnumiami"
---
<a href="http://www.xandros.com/">Xandros</a> <a href="http://www.xandros.com/products/home/desktopdlx/dsk_dlx_intro.html">Desktop OS 3</a> which is heavily based on KDE 3.3 <a href="http://www.xandros.com/news/press/release32.html">has been announced</a>. *** The installable Live CD 
<a href="http://www.knoppix.org/">Knoppix</a> has been upraded to KDE 3.3.1 in its version 3.7. *** The installable and upgradable Live CD <a href="http://www.pclinuxonline.com/pclos/">PCLinuxOS</a> <a href="http://www.pclinuxonline.com/pclos/html/screen_shots.html">Preview 8</a> includes KDE 3.3.2 and <a href="http://dot.kde.org/1101482981/">OpenOffice.org/KDE</a> 1.1.3. *** Another installable and upgradable Live CD release is <a href="http://www.mepis.org/node/view/4419">ProMEPIS 2005 Beta 2</a> with KDE 3.3.1. ****
The next <a href="http://www.debian.org/">Debian</a> release "Sarge" <a href="http://lists.debian.org/debian-devel-announce/2004/12/msg00006.html">will contain KDE 3.3</a>. *** <a href="http://www.osnews.com/comment.php?news_id=9182&offset=38">Rumors</a> say that a <a href="http://lists.ubuntu.com/archives/ubuntu-users/2004-December/015138.html">KDE Edition</a> of <a href="http://www.ubuntulinux.org/">Ubuntu</a> is progressing and isn't too far from being released.



<!--break-->
