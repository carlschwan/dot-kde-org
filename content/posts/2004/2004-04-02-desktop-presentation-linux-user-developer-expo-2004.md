---
title: "Desktop Presentation at Linux User & Developer Expo 2004"
date:    2004-04-02
authors:
  - "jbacon"
slug:    desktop-presentation-linux-user-developer-expo-2004
comments:
  - subject: "Exhibition"
    date: 2004-04-02
    body: "Will KDE be also present with a booth in the exhibition area?"
    author: "Anonymous"
  - subject: "Yes"
    date: 2004-04-02
    body: "Yes, a number of KDE developers are running a booth there. "
    author: "Jono Bacon"
---
Just a quick note to let you know that I will be doing a presentation about the potential for Linux as a desktop platform at this year's <a href="http://www.linuxuser.co.uk/expo/">Linux User &amp; Developer Expo</a> in London on April 20th from 15:25 - 15:55. The talk is entitled "<a href="http://www.linuxuser.co.uk/expo/index.php?option=content&task=view&id=129&Itemid=56">Linux as a viable desktop platform</a>" and I will be covering a number of subjects to do with desktop Linux, of which KDE plays a big part. I will also be discussing GNOME, FreeDesktop.org, Project Utopia and other technologies in the presentation.
For more details have a look at the <a href="http://www.linuxuser.co.uk/expo/index.php?option=content&task=section&id=7&Itemid=56">Linux User &amp; Developer Conference website</a> and get in touch with me if you have any questions. I hope to see some of you there. :)


<!--break-->
