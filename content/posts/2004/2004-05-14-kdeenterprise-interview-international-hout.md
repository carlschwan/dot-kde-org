---
title: "KDE::Enterprise: Interview with International Hout "
date:    2004-05-14
authors:
  - "fmous"
slug:    kdeenterprise-interview-international-hout
comments:
  - subject: "Central storage of Calendar data"
    date: 2004-05-14
    body: "I recommend to have a look at <a href=\"http://www.kolab.org\">Kolab</a> which provides exactly this and a little bit more using IMAP for storing the data centrally with access control list etc.\n\nTypical KDE Kolab clients are either <a href=\"http://www.kontact.org/\">Kontact</a> or Aethera.\n"
    author: "Martin"
  - subject: "Re: Central storage of Calendar data"
    date: 2004-05-14
    body: "Strange that he didn't know that. \nThere must be something lacking in KDE's marketing department."
    author: "reihal"
  - subject: "Re: Central storage of Calendar data"
    date: 2004-05-14
    body: "He doesn't know about ReKall and Kexi either.\nMaybe he should try \"KDE database interface\" on Google?"
    author: "reihal"
  - subject: "Re: Central storage of Calendar data"
    date: 2004-05-14
    body: "I need to rectify this ... \n\nThis interview is conducted quite some time ago. Back then ReKall and Kexi were not available like it is today. \n\nCheers' \n\nFab\n"
    author: "Fabrice Mous"
  - subject: "Perhapse some footnotes are needed then?"
    date: 2004-05-14
    body: "Why not add footnotes to this interview with *'s explaining that the features talked about as missing are available now? The shared calender and database are not the only things outdated with this interview. There are other somewhat negative comments in the interview that have since been addressed, like \"For example when a process has jammed under OpenOffice so that one cannot open a new document. Unfortunately KDE is not that intelligent yet to show that by a error message\"... since 3.2 KDE will notify you when an app appears to be locked up and give you the option to kill it.\n\nIf I saw a manager reading this article (on KDE Enterprise nonetheless), I would not be surprised at all if they said \"oh look, KDE doesn't have any way to share calenders. Need to stick with Windows/Outlook\" and stop further research. This article really needs to be footnoted to bring it up to date."
    author: "Jason Keirstead"
  - subject: "Re: Perhapse some footnotes are needed then?"
    date: 2004-05-15
    body: ">>\"For example when a process has jammed under OpenOffice so that one cannot open a new document. Unfortunately KDE is not that intelligent yet to show that by a error message\"... ... since 3.2 KDE will notify you when an app appears to be locked up and give you the option to kill it.\n\nnot really, AFAIK KDE only notifies the user that a KDE Application is jammed when you try to close it.\nKDE does not notice jams in non kde apps.\n\nRinse\n"
    author: "rinse"
  - subject: "Re: Perhapse some footnotes are needed then?"
    date: 2004-05-15
    body: "AFAIK, that works with any app that support NetWM \"ping\" messages. I do not know whether any non-KDE apps do so, though"
    author: "Sad Eagle"
  - subject: "Re: Perhapse some footnotes are needed then?"
    date: 2004-05-15
    body: "During publication this makes sense. But it makes less sense to go through all articles at enterprise.kde.org checking if certain functions/programs do exist these days. \n\nBut your are right and maybe we need to add a date to those stories so we can place them on a correct timeline. Will fix that asap ... \n\n\nCheers'"
    author: "Fabrice"
  - subject: "Better and more comprehensive documentation"
    date: 2004-05-14
    body: "When Linux sites forum are awash with flames about \"spatial mode\", \"button orders\", \"HIG\", Walter Stolk reminds us of the sober reality. We need better documentation and a better help system. KDE has worked a lot on documentation and a lot of people have spent a lot of hours writing and translating documentation. But, there is a lot of progress to be made if we want to be on par with commercial applications. Most of the documentation is also geared to technical users. For example, if you launch Khelpcenter with this simple question in your mind \"How do I configure my printer\", you'll have to navigate the tree to find the Kprinter documentation (not very intuitive) and then you will be faced with the Kprinter documentation, a very fine and comprehensive documentation written by Kurt Pfeifle. The newbie will read a sentence in the first paragraph like \n\n\"KDEPrint is not a replacement for the printing subsystem itself. KDEPrint does not therefore give provision for spooling, and it does not do the basic processing of PostScript\u00ae or other print data.\" \n\nAnd say to himself, \"what is a 'printing subsystem', 'spooling', 'postscript' ? I just want to know which button to press to configure my printer\"\n\nIt would be great if at some point, KDE sponsor could hire someone to help us write a layer of simple and action based help. Members of Quality Teams should also monitor that the first level of documentation should not need a BA in Computer Studies to be understood.\n\nCheers,\nCharles \n"
    author: "Charles de Miramon"
  - subject: "Re: Better and more comprehensive documentation"
    date: 2004-05-14
    body: ">>\"How do I configure my printer\"<<\n\nDoesn't the \"WhatsThis\" help from kprinter help you? Since KDE 3.2 this\nis a really shining example of how all of KDE could be like. Dunno who\ncontributed it, but it is a job well done.\n\n\n>>\"find the Kprinter documentation (not very intuitive)\"<<\n\nThere is no \"Kprinter documention\" for me. Are you talking about the\nKDEPrint *Handbook*? A manual is a manual and WhatsThis is user's \nimmediate help access....\n\n\n>> \"I just want to know which button to press to configure my printer\"<<\n\nProblem is: printing, and configuring it, is not a \"just wanna do\" job, \nunfortunately.  As Kurt Pfeifle says: it is one of the most complicated \ntasks of everyday IT business, on all platforms.\n\n\n>> Members of Quality Teams should also monitor that the first level of <<\n>> documentation should not need a BA in Computer Studies to be understood.<<\n\nYou're probably more than welcome to re-write the printing, and other \ndocumentation."
    author: "anon"
  - subject: "Re: Better and more comprehensive documentation"
    date: 2004-05-16
    body: "\"When Linux sites forum are awash with flames about \"spatial mode\", \"button orders\", \"HIG\", Walter Stolk reminds us of the sober reality.\"\n\nSpot on."
    author: "David"
---
A new interview has appeared on the <A href="http://enterprise.kde.org/">KDE::Enterprise</A> website. 
In <A href="http://enterprise.kde.org/interviews/internationalhout/">this interview</A> Walter Stolk 
tells us why International Hout has chosen to use KDE within their organisation. 


<!--break-->
