---
title: "KDE Traffic #76 is Out"
date:    2004-04-16
authors:
  - "hpinto"
slug:    kde-traffic-76-out
comments:
  - subject: "Appreciated"
    date: 2004-04-16
    body: "Henrique,\n\nI guess I am just one of many who appreciate (and appreciated) KDE Traffic.\n\nPlease try to find the time to keep putting it together - KDE Traffic, along with the CVS summary, are great resources.\n\nBrad"
    author: "Brad Hards"
  - subject: "Re: Appreciated"
    date: 2004-04-16
    body: "My God yes. Reading through traffic and deciding what would be relevant to people must be a heck of a task."
    author: "David"
  - subject: "kexi"
    date: 2004-04-16
    body: "Anyone knows whether this release will have [any trace of] kexi code? Just anxious. Cb.."
    author: "charles"
  - subject: "Search bar"
    date: 2004-04-16
    body: "What would make KMail a kick ass mail-app would be a search-bar\njust like in mozillamail.\n\nThe one that searches instantaneously when you start typing,\njust above the message-list."
    author: "OI"
  - subject: "Re: Search bar"
    date: 2004-04-16
    body: "Sorry you're too late. KMail CVS already has it. ;)"
    author: "anonymous"
  - subject: "Re: Search bar"
    date: 2004-04-18
    body: "Hehe. Great work guys!"
    author: "OI"
  - subject: "KST's demo"
    date: 2004-04-16
    body: "George,\n\tPlease check the kst html demo. It opens as text.\n"
    author: "a.c."
  - subject: "KDE 3.2.2"
    date: 2004-04-16
    body: "When is KDE 3.2.2 going to be released?I know it was scheduled for 3 April,but I don't see any KDE 3.2.2.\nSo,the main question,WHEN?"
    author: "BojanZ"
  - subject: "Re: KDE 3.2.2"
    date: 2004-04-16
    body: "as of today, debian unstable has it"
    author: "ac"
  - subject: "Re: KDE 3.2.2"
    date: 2004-04-16
    body: "It was never scheduled to be released 3rd April, that was the tag date and it happened 1 or 2 days later. You can build KDE 3.2.2 from CVS. Binary package creation interfered with Easter."
    author: "Anonymous"
  - subject: "Re: KDE 3.2.2"
    date: 2004-04-16
    body: "It's also available in the Updates for Fedora Core 2 Test 2.\n\nRegards,\nIsmael"
    author: "Ismael"
  - subject: "Re: KDE 3.2.2"
    date: 2004-04-18
    body: "When will the official announcement be made?"
    author: "Alex"
  - subject: "Re: KDE 3.2.2"
    date: 2004-04-18
    body: "Monday"
    author: "Anonymous"
  - subject: "Re: KDE 3.2.2"
    date: 2004-04-18
    body: "Where is all of this top secret information? =p"
    author: "Alex"
  - subject: "Kolourpaint"
    date: 2004-04-16
    body: "Kolourpaint 1.0 was MS Paint style, but will the program stay as it is (like MS Paint) or will further features get added? Is there a roadmap?"
    author: "Andr\u00e9"
  - subject: "Re: Kolourpaint"
    date: 2004-04-18
    body: "> Kolourpaint 1.0 was MS Paint style, but will the program\n> stay as it is (like MS Paint)\nThe flavour of the interface will be maintained.\n\n> or will further features get added? Is there a roadmap?\nYes, see kdegraphics/kolourpaint/TODO.  Expect new icons\nby Kristof Borrey, more polish and more image effects.\n"
    author: "Clarence Dang"
  - subject: "KMAIL"
    date: 2004-04-16
    body: "Is it possible to detect the language of a email? I once saw an algorithm that worked pretty well."
    author: "gerd"
  - subject: "Re: KMAIL"
    date: 2004-04-17
    body: "Why do you want to detect the language of an email? Is this for incoming mail (to filter by the language)? Or is this for message composition (so that automatically the correct dictionary is used)?\n\nAnyway, some time ago someone wrote that he would write a language detection based on a language detection algorithm written in Perl (?) which uses trigrams (sequences of three consecutive character) for this. Nothing happened so far."
    author: "Ingo Kl\u00f6cker"
  - subject: "No more splashscreen!! WHAT ABOUT THE CONTEST!!!=("
    date: 2004-04-17
    body: "\"I think the splash screen should be removed.  Nobody has disagreed with this opinion so far. If the splash screen stays, Cornelius and I think it needs to be  modified so that it does not \"hard code\" components and the Groupware Client text.\"\n\nPersonally I like the idea of having a Splashscreen in Kontact, and I think it should be an option if people are really against it. Kontact doesn't load instantly so a splashscreen makes sense to me anyway.\n\nAlso, if you remove the splashscreen it will anger a lot of artists who worked very hard to create a better spalsh screen for the \"Kontact Splashscreen Contest\" on KDE-LOOK.org http://www.kde-look.org/news/index.php?id=103\n\nCheck out some of the awesome entries:\nhttp://www.kde-look.org/content/show.php?content=11961\nhttp://www.kde-look.org/content/show.php?content=12013\nhttp://www.kde-look.org/content/show.php?content=11992\nhttp://www.kde-look.org/content/show.php?content=11984\nhttp://www.kde-look.org/content/show.php?content=11952\nhttp://www.kde-look.org/content/show.php?content=12021\nhttp://www.kde-look.org/content/show.php?content=12064\n\nThis is some fantastic artwork which is sure to impress anyone who oppens Kontact with such a splashscreen and is infinetly superior to the current splashscreen. Don't let their work go to waste, and don't make a mockery of the Kontact Splashscreen Contest."
    author: "Alex"
  - subject: "Re: No more splashscreen!! WHAT ABOUT THE CONTEST!"
    date: 2004-04-17
    body: "Don't worry, you are late.\n\nThe consensus is that Kontact still needs a splash screen. \n\nThe replies to this message in the kde-pim  mailing lists were the <b>origin of the kontact contest</b>. kde-pim developers are already evaluating the entries to decide the new splash screen.\n\nSo keep up the good job!\n\nCarlos Woelz"
    author: "Carlos Woelz"
  - subject: "Thanks a lot!"
    date: 2004-04-19
    body: "I've missed it a lot!\n\nThanks for providing this excellent info about KDE!"
    author: "Joergen Ramskov"
  - subject: "Luke Randall kontact..."
    date: 2004-04-24
    body: "This traffic mentions Luke Randall making a nifty screenshot for kontact with crystal icons\n\nhttp://bugs.kde.org/attachment.cgi?id=3162&action=view\n\ni found it"
    author: "standsolid"
---
After a long break, KDE Traffic is back. <a href="http://www.kerneltraffic.org/kde/kde20040416_76.html">KDE Traffic #76</a> includes tons of news about <a href="http://kmail.kde.org/">KMail</a>, <a href="http://www.koffice.org/">KOffice</a>, <a href="http://www.konqueror.org/">Konqueror</a>, <a href="http://www.k3b.org/">K3b</a>, <a href="http://kolourpaint.sourceforge.net/">KolourPaint</a> and more of your favorite KDE apps. Check it out!




<!--break-->
