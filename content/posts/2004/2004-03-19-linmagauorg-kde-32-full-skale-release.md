---
title: "linmagau.org: KDE 3.2 Full sKale Release"
date:    2004-03-19
authors:
  - "wbastian"
slug:    linmagauorg-kde-32-full-skale-release
comments:
  - subject: "In that case..."
    date: 2004-03-19
    body: "\"You have to experience KDE 3.2 to really understand how much KDE has improved since 3.1. The speed is clear.\"\n\nWell hey, I'm still on 3.0.3! I'll be upgrading soon so it sounds like I'm in for a real treat! :)"
    author: "Stephen Sweeney"
  - subject: "Wait for 3.2.2"
    date: 2004-03-19
    body: "There's a nasty drag&drop bug in 3.2.1 which annoys me (no drag&drop in konq web browsing) to no end. KDE 3.2.0 is not an alternative, because although it doesn't have the aforementioned bug, it is not as stable as 3.2.1 - so, if you have waited that long anyhow, you might as well get 3.2 in its prime.\n\nAnno."
    author: "Anno"
  - subject: "Re: Wait for 3.2.2"
    date: 2004-03-19
    body: "That has already been fixed for 3.2.2 along with the \"cannot drag the URL of a link if it is an image\"...\n"
    author: "Dawit A."
  - subject: "Re: Wait for 3.2.2"
    date: 2004-03-20
    body: "I don't know if this is related, but do you know if it is possible to drag files from the konqueror filemanager to the attachment area of the mozilla mail client.\nIn previous versions the drag seamed to work but the URL was wrong (looking something like: file:/filename instead of file:///filename). So when mozilla would not find it when sending the mail.\n\n\n"
    author: "Uno Engborg"
  - subject: "Re: Wait for 3.2.2"
    date: 2004-03-20
    body: "To me that sounds more like a problem by Mozilla, not Konqueror."
    author: "Datschge"
  - subject: "KDE 3.2..."
    date: 2004-03-19
    body: "I use XFCE 4. Prior to KDE 3, I didn't use too many KDE application. I mainly used Gnome Applications (Gnumeric, Evolution, Gaim, Gnome-terminal,etc.) Now that KDE 3.2 is out I still use XFCE, but I've started using a lot of KDE applications. I now use Kopete, Konqueror and Quanta. Kontact looks very nice as well, but I still like Evolution better. KDE has made great progress. Integrating Kwalletmanager with kopete and konqueror is AWSOME...\n\nTo all KDE Developers... THANKS!\n\n-Cory"
    author: "Cory"
  - subject: "I am new to Linux and need some help..."
    date: 2004-03-20
    body: "I have dl'ed all the files from the ftp site but have no idea on how to install or ugrade the KDE. I have been to Chat rooms and recieved no help (just a lot of sarcasm)  I am using fedora and really like the KDE GUI.  Can someone give me some direction on this I have been reading for ever and researched what I could but nothing seems to work and I don't want to have to start over (reinstalling Linux).\n\nThanks PCjoKeR69"
    author: "pcjoker69"
  - subject: "Re: I am new to Linux and need some help..."
    date: 2004-03-20
    body: "Have you seen this page?\n\nhttp://kde-redhat.sourceforge.net/\n\nIf you're not comfortable modifying your apt/up2date/yum configuration, you can just go to the kde-redhat/1 directory on one of the mirror FTP sites listed, and download the RPMs from there. Setting up yum would be the recommended method, though, as it will take care of downloading the right packages for you.\n\nI'm sorry to hear you got a sarcastic response. That sort of thing is not necessary :("
    author: "Paul Eggleton"
  - subject: "Re: I am new to Linux and need some help..."
    date: 2004-03-20
    body: "I am not familiar with yum, I read the 'man' page on yum but did not find it to be very helpfull.  I have already downloaded all the *.rpm packages from the site and burned them to cd for storage. I even ran yum -h & --h but still could not convince myself I knew what I was doing. I have also never done an ftp install.\n\nThanks,\n\nPCJoKeR69"
    author: "PCJoKeR69"
  - subject: "Re: I am new to Linux and need some help..."
    date: 2004-03-20
    body: "yum is pretty simple to use. To install an application you just run:\n\nyum install name_of_the_application\n\nOn the page I mentioned earlier (http://kde-redhat.sourceforge.net/) it has a section on yum describing what you need to do.\n\nThere's a bit more helpful information on Fedora, yum etc. on this page:\nhttp://fedora.artoo.net/faq/\n\nIf you have any non-KDE related questions please e-mail me directly as this is getting a little off-topic."
    author: "Paul Eggleton"
  - subject: "Re: I am new to Linux and need some help..."
    date: 2004-03-20
    body: "I did as instructed in the previous e-mail but I received errors about to many requests from my IP & invalid headers error 550.  Not sure what this means, but I did some more reading and still could not get it to do what I needed I don't know if I am just thick or what or maybe it just wasn't my day I will play with it some more when I fget more info.\n\nPCJoKeR69 <--- also my messenger log in if you would like to contact me directly for more info.\n\nThanks again, I really do appreciate your help in this matter."
    author: "pcjoker69"
  - subject: "call me a fan boy"
    date: 2004-03-20
    body: "but i'm really so happy when i see praises about one of the kde apps. I've been saying for 2 years that linux was ready for the desktop thanks to kde, I've helped a few persons switch to linux thanks to kde, I really have a great dekstop experience thanks to kde.\nSo yes, yes to all of the kde developpers who help this great project move to this fast and steady pace. And in the right direction: that of the users."
    author: "mi"
  - subject: "KDE-RedHat and White Box Enterprise Linux"
    date: 2004-04-02
    body: "I'm also new to Linux (well, I've looked at it many times in the past, usually quite briefly) and am trying out KDE. In my case, I'm using White Box Enterprise Linux, as my corporation is very RHEL-centric. Can I follow their directions (making appropriate changes for the yum sources) and safely upgrade from KDE 3.1 to 3.2.1? Will it reorganize my menus or anything else like that? It's taken me a long, sometimes frustrating time to get as far as I am, and if I have to do it over again I may switch the server off for good."
    author: "Joshua"
---
The <a href="http://www.linmagau.org/">Australian Linux/Open Source Magazine</a>, linmagau.org, has an
<a href="http://articles.linmagau.org/modules.php?op=modload&name=News&file=article&sid=464&mode=thread&order=0&thold=0">interview</a> with
<a href="http://people.kde.org/george.html">George Staikos</a> reflecting on the KDE 3.2 release. For those who have yet to try KDE 3.2, George has the following advice: <i>"You have to experience KDE 3.2 to really understand how much KDE has improved since 3.1. The speed is clear. Give Konqueror a try, it's more stable, more compatible, and again, faster. KDevelop 3 is a whole new application and will quickly become your IDE of choice."</i>




<!--break-->
