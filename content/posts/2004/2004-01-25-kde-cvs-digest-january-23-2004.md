---
title: "KDE-CVS-Digest for January 23, 2004"
date:    2004-01-25
authors:
  - "dkite"
slug:    kde-cvs-digest-january-23-2004
comments:
  - subject: "K3b"
    date: 2004-01-25
    body: "And now K3b 0.11 is out! Woo hoo!"
    author: "Turd Ferguson"
  - subject: "Re: K3b"
    date: 2004-01-25
    body: "i just tried k3b for the frst time, and it REALLY rocks! Burns CDs and DVDs without problems and looks great at the same time!\n\nIf you have a 2.6 kernel, you just have to add -dao to the cdrecord parameters.\n\nThanks for k3b!"
    author: "anonymous"
  - subject: "Support of outlook forms."
    date: 2004-01-25
    body: "Does this mean it will be easier to support outlook forms? (the kaddressbook type forms)\n"
    author: "JohnFlux"
  - subject: "Re: Support of outlook forms."
    date: 2004-02-24
    body: "how to customize the outlook form to show the default profile when sending the reply form."
    author: "iurie"
  - subject: "Next releases"
    date: 2004-01-25
    body: "A bit off-topic. But with 3.2 around the corner, let me ask. \n\nAm I correct in that the next major version will be 3.3, building on\nthe 3.2 release, and after this KDE 4.0 will be released, based on Qt-4,\nand presenting some important integration between different applications of the same kind, and major reorganization with a focus on usability ?\n\nPlease enlighten, thanks in advance !"
    author: "MandrakeUser"
  - subject: "Re: Next releases"
    date: 2004-01-25
    body: "For now, it is starting as KDE 3.3.\n\nHowever depending on how slow the developement would be and how fast Qt4 Beta would appear, it might be that KDE 3.3 would never be released.\n\nHave a nice day!\n"
    author: "Nicolas Goutte"
  - subject: "Re: Next releases"
    date: 2004-01-25
    body: "Will there be a wholesale conversion to D-BUS in version 4?  It would seem that a major version change would be the time to do this."
    author: "krakah"
  - subject: "Re: Next releases"
    date: 2004-01-25
    body: "I hope so.\nI think integration will become wery important in the future of linux.\nAnother thing that would be wonderful is the integration of the gstreamer framework."
    author: "mart"
  - subject: "Re: Next releases"
    date: 2004-01-25
    body: "Streamer - I heard that Roxio patented sttreamer technology. Wouldn't it be wise to act against the patent before it becomes enforcable."
    author: "Andre"
  - subject: "Re: Next releases"
    date: 2004-01-25
    body: "> Another thing that would be wonderful is the integration of the gstreamer framework.\n\ninstead of this, I think we should standardize on the much more stable and cross platform videolan. Or MAS. Or NAS. I suppose there will be a lot of discussion on kdemultimedia before kde4 :-PD"
    author: "anon"
  - subject: "Re: Next releases"
    date: 2004-01-25
    body: "Personally, I'd like to see an implementation of JACK in some form so we can bring professional audio to the desktop. JACK really is a nice piece of work. When you look at the nice audio work going on, it just isn't feasible to keep maintaining aRts."
    author: "David"
  - subject: "Re: Next releases"
    date: 2004-01-25
    body: "me too, me too!\ngo JACK, go JACK, go JACK!\n\nMore and more apps are switching to JACK...\n\nI ask myself why we need an 'analog realtime synthesizer' running by default. aRts might be cool as an music making framework though."
    author: "cies"
  - subject: "Re: Next releases"
    date: 2004-01-25
    body: "I think I heard rumors that for KDE 4 there are plans to support the implementation of different backends, i.e. not arts-only. Of course there have to be people who will implement these backends, i.e. OSS, ALSA, MAS, jack, arts, whatever...\n\nBye\nAlex\n"
    author: "aleXXX"
  - subject: "Re: Next releases"
    date: 2004-01-27
    body: "IMHO, that would be a real shame. It would create yet another layer of complexity. I hope that there will be a real choice as to what the backend will be in KDE 4, and stick with that."
    author: "Andr\u00e9 Somers"
  - subject: "Re: Next releases"
    date: 2004-01-25
    body: "I think we need to hold off until we see how the politics of freedesktop play out. Look at the X.org/XFree 'merger' announcement by Havoc Pennington. It is all political jousting as far as I am concerned. Perhaps there are parts od D-BUS that KDE can steal from D-BUS for DCOP though. Why not? KDE has had enough of this in the past."
    author: "David"
  - subject: "Re: Next releases"
    date: 2004-01-25
    body: "Hi,\n\nI wonder if you are aware how strange the word \"steal\" in context of Free Software reads.\n\nRethink.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: Next releases"
    date: 2004-01-26
    body: "Often (at least in idiomatic American english) the term \"steal\" is used as a friendly term for taking something.  \"Nab\", \"swipe\" and \"yank\" are also used (swipe, like steal, also has a negative connotation in its most common usage).\n\nFor instance, somebody in an office might bring in doughnuts and lay them out on their desk for people to take.  A coworker who took one, knowing that they are publically available, might say \"I swiped it from Rob\".  A far more common usage for \"steal\" is as an active verb.  At the office again, somebody might say \"Wait, we need a stapler, and there isn't one at this desk... I'll go steal Rob's stapler\".  There is no negative connotation.  It simply refers to taking without asking due to implied consent.  There is a slight edge to it; usage of \"steal\" in this manner is an easygoing, slightly sarcastic tone.  It implies familiarity with the situation and all involved.\n\nSo yes, \"steal\" is an apt choice of terminology, assuming an American speaker."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Next releases"
    date: 2004-01-26
    body: "In Norway we use 'stjele' in the same way.  It's got the same bad or jokingly connotation.\n\nSince people are stealing each others staplers all the time, one of my co-workers has marked his stapler \"Stj\u00e5let from <name>\", so that he'll get it returned in case the \"thief\" forgets ;)"
    author: "arcade"
  - subject: "Re: Next releases"
    date: 2004-01-27
    body: "Bad artists copy, great artists steal. - Picasso.I think he meant it more that way ;-)\n\n\n\n"
    author: "Roberto Alsina"
  - subject: "Re: Next releases"
    date: 2004-01-26
    body: "> Look at the X.org/XFree 'merger' announcement by Havoc Pennington.\n\nAs I understand it, he was quoted out of context.  See http://www.xfree.org/#news - there is no merger.\n\n> Perhaps there are parts od D-BUS that KDE can steal from D-BUS for DCOP though.\n\nDBUS was designed using DCOP as a model, and is designed to be desktop-agnostic.  Why bother trying to copy DBUS when you could just use DBUS directly and be compatible with more software?"
    author: "Jim Dabell"
  - subject: "Re: Next releases"
    date: 2004-01-29
    body: "KDE really should use D-BUS for 4.0, a lot of the work being done on hooking the kernel up to the desktop environments uses D-BUS, and frankly a cross platform, cross desktop solution would be really cool. From what I've heard D-BUS is basically DCOP with a bit of extra stuff thrown in. Other than politics and inertia, why wouldn't you want to use it?"
    author: "Bryan Feeney"
  - subject: "Re: Next releases"
    date: 2004-01-25
    body: "\"However depending on how slow the developement would be and how fast Qt4 Beta would appear, it might be that KDE 3.3 would never be released.\"\n\nBTW, does anybody know if there is some quotable timeframe for a Qt4 release?"
    author: "spiff"
  - subject: "Re: Next releases"
    date: 2004-01-25
    body: "I think that the only quotes were at the Kastle conference. So perhaps youc could try to find the old documents.\n\nHave a nice day!\n"
    author: "Nicolas Goutte"
  - subject: "Re: Next releases"
    date: 2004-01-25
    body: "Hi,\n\nI would think that porting KDE to QT4 is not necessarily a quick job. I would recommend a 3.3 release. The most important reasons are in my opinion:\n\na) It would be a chance to reintegrate the kdepim separate releases.\nb) A lot of regression testing is being introduced lately into khtml. With 3.3 we can probably see a khtml that is almost fully standards compliant.\nc) Some minor rewrites could be done, like khotkeys2 user interface, or apps like kdevelop and quanta can see consolidation\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: Next releases"
    date: 2004-01-25
    body: "Yes, I also have the feeling that both because Qt-4 is probably going to be released at least a few months down the road, and also because there are several minor improvements that can be done without a major architectural change, that a KDE 3.3 with a short release would be best ..."
    author: "MandrakeUser"
  - subject: "Re: Next releases"
    date: 2004-01-25
    body: "merging kdevelop and quanta a minor rewrite? what kind of moron are you?"
    author: "Mathieu Chouinard"
  - subject: "Re: Next releases"
    date: 2004-01-26
    body: "cool down :)\n\nTry reading it this way:\n\n(Some minor rewrites could be done, like khotkeys2 user interface), OR (apps like kdevelop and quanta can see consolidation)"
    author: "MK"
  - subject: "Re: Next releases"
    date: 2004-01-26
    body: "where is people sense of humor?"
    author: "Mathieu Chouinard"
  - subject: "Re: Next releases"
    date: 2004-01-27
    body: "It disappeared when your smiley did ;) Irony does not convey easily across text-only media, and thus the smiley was invented :)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: Next releases"
    date: 2004-01-26
    body: "I fast 3.3 release that includes things like the kdepim stuff that didn't make 3.2 would be nice.  This way the 3 series could have a nice unified release to go out on.  This would also help buy time for a longer 4.0 development cycle."
    author: "theorz"
  - subject: "question"
    date: 2004-01-25
    body: "Apologies in advance for being off-topic (don't know where to post this) - can we have a bigger or more descriptive link to the KDE Homepage from this kde.news page - there's only a tiny link on the left-hand side which isn't very descriptive - it just says \"kde\" in very small writing - how about something like \"kde homepage\" or something in bigger letters and more prominent - just thought I'd mention it....."
    author: "anon"
  - subject: "Re: question"
    date: 2004-01-25
    body: "There is always mailto:webmaster@kde.org to report such cases. (Any email sent there goes to the kde-www mailinglist.)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Mouse Gestures"
    date: 2004-01-25
    body: "Just wandering is 3.2 going to have any kind of gesture support (for example like opera for going back and forward on web sites).\nI've seen a program called KGestures, but it seems its developement has stop for some time now."
    author: "Cirrus"
  - subject: "Re: Mouse Gestures"
    date: 2004-01-25
    body: "yes, kde 3.2 includes khotkeys.. which does what you want, and more. "
    author: "anon"
  - subject: "Re: Mouse Gestures"
    date: 2004-01-25
    body: "Nice. I've been using opera lately, and once you get started using gestures its quite hard to go back. I'd really like to use konqueror (as I did up until a few months ago), if this feature was implemented. Can't wait for 3.2 to be released in a week or so.\nGood work KDE developers (and that is not for the gestures only but for everything), thanks.\nCirrus"
    author: "Cirrus"
  - subject: "Re: Mouse Gestures"
    date: 2004-01-25
    body: "yes, and it is wonderful: maybe little difficult to setup, but since it remaps mouse gesture -> keyboard shortcut, you can use gesture to every ap, not only kde apps"
    author: "mart"
  - subject: "Re: Mouse Gestures"
    date: 2004-01-25
    body: "Yes, hopefully a followup release will feature presetup gesture a-l-a Opera / Mozilla and so on. I in fact use the Opera-close-window gesture to close tabs on any application ( CTRL+W ). It is hard to go back to pointing the mouse to an X or using the shortcut itself."
    author: "kontakt"
  - subject: "Re: Mouse Gestures"
    date: 2004-01-28
    body: "You can find Opera/Mozilla gestures to import here: http://datschge.gmxhome.de/khotkeys.html\n\nThanks Datschge!"
    author: "Christian Loose"
  - subject: "qualify"
    date: 2004-01-25
    body: "...... actually, there's a link on the right aswell - not very prominent though - my bad - maybe a more prominent one at the top of the page?"
    author: "anon"
  - subject: "krita"
    date: 2004-01-25
    body: "Krita looks like it has a very nice architecture now.. it probably needs some GUI designers though :)"
    author: "anon"
  - subject: "Re: krita"
    date: 2004-01-25
    body: "Well, I have been thinking on what would make a good interface for a paint application that's supposedly easy enough to sit down at and start work, but I don't think I'm ready for carrying out any great gui redesign. So if anyone wants to go through Krita's interface and make it better (more like Karbon would be if Karbon were like the UI-analysis document you can find in its source tree, or just generally better), I'd be very pleased."
    author: "Boudewijn Rempt"
  - subject: "Re: krita"
    date: 2004-01-27
    body: "I'll kick out (hoping that the fellow who develops Krita reads this) that there's a nice GPLed Qt art application named DrawPad in the OPIE project (i.e., open qtopia, a Qt based environment for handhelds).\n\nIt does a few things that are pretty neat, like rotating the brush perpendicular to the vector of the pointer.  Since it's Qt based, it might be worth a look to see if there's any code that is reusable.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: krita"
    date: 2004-01-27
    body: "Yup, he does... \n\ncvs co opie...\n\nI say, that's a big source tree...\n\nOh, I see what he's doing to make nice antialiased lines -- he draws at three times the size and then scales down with QImage's smoothscale. That's a nice trick, but you can only do that if you have a small image to start with. I don't see where he does fancy things with the brush, though...\n\nIt's not directly useful for what I'm working on now for Krita (I'm trying to implement the various composite modes for layers and painting), but this particular trick might be useful for the author of Kolourpaint, because his Kolourpaint works in general a lot like DrawPad."
    author: "Boudewijn Rempt"
  - subject: "Re: krita"
    date: 2004-01-28
    body: "The nifty part that I'm referring to is that the brush is a n-pixel wide line, selectable by a dropdown in the toolbar.  When the brush is moved across the canvas, the line is drawn 90 degrees from the vector of the pointer.  It's sort of hard to describe what the effect does in practice (the theory is simple), but it makes it very very easy to outline complex shapes and also gives you very uniform lines that end in a sharp, flat end (as opposed to using a disk shaped brush, which ends with a rounded end).\n\nIt is one of those simple things that quickly becomes very very intuitive.\n\nThe paint fill also selects by color and then tints similar colors based on how close they are to the clicked color - i.e., a click of red on yellow will pinken nearby whites and oranges but leaves blues alone.  It behaves poorly, IMO, but the idea is interesting and could be reworked with a different or tweaked formula.  \n\nDrawPad is the app, by the way.  DrawPad is to Opie as Krita is to KDE.  Opie uses Qt to build on and create a palmtop interface rather than a desktop interface.  Konqueror, Kate, Konsole and other KDE apps are ported and maintained.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Off topic, but"
    date: 2004-01-25
    body: "HOW DO YOU USE KHOTKETS 2!\n\nI have KDE 3.2 RC 1, I'm at the dialog for Khotkeys under system and I am throughly confused, and I can't find any manual either. NOBODY BUT COMPUTER EXPERTS WILL USE THIS UNLESS ITS SIMPLIFIED! It seems to do anything you need to know locations, paths and stuff from your head, there is no easy to use GUI.\n\nCan someone tell me how to do something simple like make a line with the mouse like this __________________________ and make this gesture select all in Konqueror?\n\nI thought Khotkeys 2 would benefit normal users, I was thinking its an improved version of the \"Configure Shortcuts\" dialog that I see in all KDE applications, one that would allow me, in addition to using key combinations, to use gestures.\n\nI'm sorry, this is proabably a very powerful application, but it's so hard to use and arcane =("
    author: "COnfused USer"
  - subject: "Re: Off topic, but"
    date: 2004-01-25
    body: "Here's how to make a \"select all\" gesture for Konqueror:\n\nClick \"New Group.\" Click the tab \"Conditions\" at the top right.  Click the listbox showing \"New\" and select \"Active Window\".  In the dialog that appears, click the listbox showing \"New\" and select \"Simple Window...\", the only option available.  Click the button \"Autodetect\", then click on an open Konqueror window.  Click the listbox labeled \"Window Class:\" and choose \"Is\" from the list of predicates.  Click OK twice to dismiss the two dialog boxes.\n\nCongratulations!  You have now made a group of actions specifically for Konqueror windows.  Now for the \"select all\" action.\n\nYour new action group will be selected in the KHotkeys2 window.  Press the button labeled \"New Action\".  From the \"Action type:\" listbox, choose \"Gesture -> Keyboard Input (simple)\".  Click the Gestures tab.  Press the \"Edit...\" button and define your gesture in the window that pops up.  Don't make it too complex because KHotkeys is pretty picky about minor differences and pops up annoying dialog boxes.  Once you are done hit OK.  Now go to the \"Keyboard Input Settings\" tab.  Click on the \"Modify...\" button.  Realize that the \"Modify...\" button is broken, making it impossible to send \"Ctrl-A\" .  Look for a DCOP call that will select all text in the active Konqueror window.  Realize that every Konqueror window and tab has a unique number in its name, making it impossible for KHotkeys to always call the active tab in the active window.  Bang head on wall.\n\nKHotkeys2 has one of the worst user interfaces I have ever seen.  So much emphasis has been put on making it super-powerful that ease-of-use has been completely overlooked.  KHotKeys2 needs to do two main things:\n\n1.  Take gesture support and integrate it into KDE's keyboard shortcut system, so it will be in every application that has a \"Configure Shortcuts\" dialog.  This way most users will avoid having to use the KHotKeys interface at all and can stick with the old familiar shortcuts interface.\n\n2.  Completely redesign KControl interface to KHotkeys.  Make it really simple to add global actions with no conditions.  Make it reasonably simple to add actions that are specific to one application using window class matching.  Have an \"advanced action\" that has all the capabilities of the current KHotkeys2 for people who are shortcut fanatics.  Minimize the number of clicks necessary to add shortcuts.  Don't ask the user to name each action/condition/etc, generate meaningful names automatically.\n\nUnfortunately, KHotkeys2 is completely unusable by 95% of people in its current state, and it is so much work to use it that the other 5% of people will be discouraged from using it even though they can.  It desperately needs fixing.  Lots of power is good, but usability is extremely important as well."
    author: "Spy Hunter"
  - subject: "Re: Off topic, but"
    date: 2004-01-25
    body: "Well, thanks at least I understand the way it works, but wow, that is more work than it saves. Only thing that I gound reasonably simple to do that worked was to use Khotkeys to open programs, with gestures.\n\nAlso, having a line drawn when drawing the gesture would help. I also agree, that gestures should be integrated into the \"Configure Shotcuts\" dialog as an alternative to using keyboard only shortcuts. Something simple. Honestly, that's exactly what I was expecting and that's why I was so excited about this, it has a lot of potential, just not yet easy enough for most and I don't see any documentation either."
    author: "ConfusedUser"
  - subject: "Re: Off topic, but"
    date: 2004-01-25
    body: "> Realize that the \"Modify...\" button is broken, making it impossible to send \"Ctrl-A\" .\n\nVery well described, till this point. Here it should read: Realize that the \"Modify...\" button is not implemented yet (and as such it's in fact hidden in 3.2final), but you can type the input directly, so you can manually enter \"Ctrl+A\", \"Shift+H:E:L:L:O\", or whatever you want. Also, there's the examples group, which can serve as an inspiration.\n\nAs for the GUI, I agree it's not very good. Interesting how I can do technically quite complicated things, and fail to do something seemingly as simple as creating GUI that doesn't suck :(. Apparently this needs somebody else than me. In case you (or somebody else) feel you can do better, and have no problem with extending your \"KHotKeys2 needs\" to \"KHotKeys2 needs and I'm willing to help\", feel free to add wizards, fix the GUI or dump it and start from scratch. The GUI is created using Qt Designer and separated from the rest of the code.\n\nI think I should also note that the original plan was to include predefined actions, but 3.2.0 has only the example ones. KHotKeys can import new ones from files, so I'll post the ones that will be added to 3.2.1 also somewhere for download.\n"
    author: "Lubos Lunak"
  - subject: "Re: Off topic, but"
    date: 2004-01-26
    body: "I see an \"import\" button but no way to export the there requested *.khotkeys file. What kind of format is that? Simply the [data] tree from khotkeysrc?\n\nThanks. =)"
    author: "Datschge"
  - subject: "Re: Off topic, but"
    date: 2004-01-26
    body: "Well, yes. Backup your khotkeysrc, delete unwanted actions, arrange the remaining ones in the way you want, save the config, copy your khotkeysrc somewhere and make it e.g. xmms.khotkeys . Only the [Data*] groups and Version field in [Main] are read when it will be imported, so you can manually remove the others. Hmm, this again looks a bit complicated, doesn't it?\n"
    author: "Lubos Lunak"
  - subject: "Re: Off topic, but"
    date: 2004-01-26
    body: "Well, yes and no. I'm personally fine with the interface and the exporting approach, but I can also see why it might be only us. ;) The import button is easy to find so for starters I'll just refer to that and offer some prepared sets of mouse gestures. Thanks. =)"
    author: "Datschge"
  - subject: "Re: Off topic, but"
    date: 2004-01-27
    body: "Ok, for those interested, http://seli.webz.cz/khotkeys/ has some actions already, I'll more as they're done.\n\n"
    author: "Lubos Lunak"
  - subject: "Re: Off topic, but"
    date: 2004-01-27
    body: "I've posted some as well yesterday at http://datschge.gmxhome.de/khotkeys.html"
    author: "Datschge"
  - subject: "Re: Off topic, but"
    date: 2004-01-26
    body: "Thanks for the very polite answer :-)\n\nFor some reason I didn't think of typing the name of the key combination I wanted.  I must have deleted the examples group by accident or something, because I didn't see it.  That would definitely have helped me understand the meaning of that text box.\n\nI've been meaning to get around to contributing to KDE for some time, but I always seem to have some excuse.  If I'm going to keep complaining about these things, I should put my money where my mouth is and start fixing them.  In fact, it could be that I am really bad at creating GUIs as well, and only good at criticizing them ;-)"
    author: "Spy Hunter"
  - subject: "Re: Off topic, but"
    date: 2004-01-26
    body: "OK, the GUI is a little bit complicated to use, but it works perfectly! I just created gestures for JuK and Konqueror and am amazed how much fun it is! Thank you very much!"
    author: "Michael"
  - subject: "Re: Off topic, but"
    date: 2004-02-08
    body: "Hi\nDo you know how to open a particular url in a new tab instead of new window\nusing dcop call.\nIf I go to kdcop it lists I have 3 konq processes running but only one is visible to me. I can open a url using \ndcop konqueror-18193 konqueror-mainwindow#1 newTab http://www.hotmail.com\nbut I have no idea how to  select the active konq process\nAny ideas?\nAnil  "
    author: "Anil"
  - subject: "Re: Off topic, but"
    date: 2004-01-25
    body: "> I'm sorry, this is proabably a very powerful application, but it's so hard to use and arcane =(\n\ni agree, see bug #69912 ( http://bugs.kde.org/show_bug.cgi?id=69912 ).. I'll probably have a go at polishing/making it easier to use for 3.3. The problem with khotkeys2 is that it started off as a seperate project, and was being developed on and off for more than three years. \n\n> NOBODY BUT COMPUTER EXPERTS WILL USE THIS UNLESS ITS SIMPLIFIED\n\nWell, I don't think many normal users would need mouse gestures anyways, and not very would even choose different keyboard shortcuts, but that's besides the point, having a bad UI is bad for computer experts as well."
    author: "fault"
  - subject: "Re: Off topic, but"
    date: 2004-01-25
    body: "> Well, I don't think many normal users would need mouse gestures anyways, and not\n> very would even choose different keyboard shortcuts, but that's besides the point, \n> having a bad UI is bad for computer experts as well.\n\ni think mouse gesture rocks! it s a big comfort, not to click on a window butoon if you want that1 closed, or you can start your most used apps simply by a gesture... plz make it easier to use, pal!"
    author: "normal user"
  - subject: "Re: Off topic, but"
    date: 2004-01-25
    body: "Actually kudos to Lubos for releasing it. It seemed it would never happen and noone would have been able to complain: which would have been bad. Now it's finally getting some attention. Maybe some curious 3.2.0 users will participate in creation of rules/new interface/addition to the regular \"Configure Shortcut\" interface for \"3.2.1\"."
    author: "kontakt"
  - subject: "Re: Off topic, but"
    date: 2004-01-25
    body: "I totally agree with you.\nNot only is the UI bad from a usability point of view but it also doesn't conform to the UI layout that most other control modules have.  As far as I know, no other modules have their titles in big bold letters.\nI also think that it should be renamed.\nBut admittedly although the UI has some bad points khotkeys is REALLY configurable and useful."
    author: "tom14"
  - subject: "Re: Off topic, but"
    date: 2004-01-25
    body: "Full ACK!\nKHotkeys is really confusing.\nAnd it doesn't seem to work BTW.\nDon't know if I don't use it right or it's a\nbug though. I unchecked \"Deactivate\" for\n\"Konqi gestures\" and all parent groups.\nNow I switch to Konqeror, press the middle\nmouse button, and do a gesture.\nNothing happens.\n???\n"
    author: "Mike"
  - subject: "Re: Off topic, but"
    date: 2004-01-25
    body: "Mouse gestures working here with kde-cvs\nI have implemented the basic forward and back in the khotkeys2\nYou will have to type the \"Alt+Right\" or \"Alt+Left\" on the keyboard input field\nRestart konqi. Because of preloading your konqi. might not have the new capability. From kde System guard kill all konqui and start a new instance now you should have mouse gesture working\nAnil\n\n"
    author: "Anil"
  - subject: "Re: Off topic, but"
    date: 2004-01-25
    body: "Same over here.  I followed the procedure provided by Spy Hunter\n(without it, I would not have been able to configure it at all!).\nAfter that no reaction on any of the 2 gestures I configured \n(ctrl+a and alt+left).\nHowever, it is good that this is now part of kde, so we can at least\ncomplain about it :))\n"
    author: "Richard"
  - subject: "Re: Off topic, but"
    date: 2004-01-26
    body: "Note that KHotKeys is quite picky on the key names, and it has to be most probably \"Ctrl+A\" and \"Alt+Left\", not \"ctrl+a\" or \"alt+left\". Since this faked keyboard input is fed to the application as the normal keyboard input using fake XKeyEvent's, these events have to be synthetized back from your input. In other words, you also need \"Shift+H\" for capital \"H\", and for simple typing of \"Hello\", it has to be \"Shift+H:E:L:L:O\". I haven't found yet a way how to let people simply enter \"Hello\" to get that. There's also no error checking yet :(.\n"
    author: "Lubos Lunak"
  - subject: "One wish for konqueror filemanager"
    date: 2004-01-25
    body: "\nhttp://bugs.kde.org/show_bug.cgi?id=24143\n\nI've reported this but it got closed at that point of time.\nNow that some time has passed maybe it should be reconsidered, because\nit would really be a nice feature for konqueror.\nIn terms of slowdown, I bet it takes more time to calculate the\nnumber of items + their total size in the current directory than it would\ntake to get the diskfree information... (which should be no more than 2 calls)\nand... windows users have it since win95 :-|"
    author: "yg"
  - subject: "Re: One wish for konqueror filemanager"
    date: 2004-01-25
    body: "That probably means that you need to find a volunteer to code your wanted change.\n\nWith code, you would have more change to get what you want.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "CSS3 already supported?"
    date: 2004-01-25
    body: "One of the commits to khtml:\n> following the trend for more test cases, here is an import of\n> http://www.w3.org/Style/CSS/Test/CSS3/Selectors/current/html/tests/\n> \n> 44 out of 166 tests fail (mostly bizare stuff I never saw on web pages :)\nWhy are tests from CSS3 used? IIRC CSS3 is not even officialy finished yet (though it would be pretty cool if khtml already supports it ;))."
    author: "Arend jr."
  - subject: "Re: CSS3 already supported?"
    date: 2004-01-25
    body: "To support it before Mozilla? ;-)"
    author: "CE"
  - subject: "Re: CSS3 already supported?"
    date: 2004-01-25
    body: "Why not?\n\nKHTML has not a huge manpower, so starting early is not wrong. Also it avoids to make choice that would later reveal to be incompatible with CSS3 (for example CSS3 includes syntax changes to CSS.)\n\nHave a nice day!\n"
    author: "Nicolas Goutte"
  - subject: "Re: CSS3 already supported?"
    date: 2004-01-26
    body: "CSS 3 is broken up into modules, some of which (e.g. the selectors module) are very near completion.  You'll find quite a few people implementing \"unfinished\" W3C specifications, as there needs to be two independent implementations before a specification can reach full recommendation status (i.e. \"finished\")."
    author: "Jim Dabell"
  - subject: "CSS changes in KDE 3.2?"
    date: 2004-01-25
    body: "I'm a little confused. Will the big CSS changes added to KHTML from Safari make it to KDE 3.2?"
    author: "Bart"
  - subject: "Re: CSS changes in KDE 3.2?"
    date: 2004-01-25
    body: "No. It appears to have been committed to HEAD, which is on track for 3.3 right now.\n"
    author: "Rayiner Hashem"
  - subject: "Krita"
    date: 2004-01-25
    body: "You had better release that software now.\n\nJudging from the screenshots, it can do a lot."
    author: "OI"
  - subject: "Re: Krita"
    date: 2004-01-25
    body: "Well... It can paint, and combine images, but there's no eraser, no support for gradients, for patterns, no fill, only rectangular select, no airbrush, no fancy filters nor scriptable plugins... Take a look at the TODO:\nhttp://webcvs.kde.org/cgi-bin/cvsweb.cgi/koffice/krita/TODO?rev=1.25&content-type=text/x-cvsweb-markup.\n\nI'm working on the eraser tool now; when that's done I think it'll be easy to bring Krita to the level of powertools like KPaint, and from there it should be plain sailing. And I want the layer combine effects working -- for that I need to add a few more composition methods.\n\nBut it's fun, and an educational experience :-)."
    author: "Boudewijn Rempt"
  - subject: "Re: Krita"
    date: 2004-01-26
    body: "Wow, the drawings in those screenshots are great.  Did you draw those?  Did that charcoal effect come from GIMP or is that Krita-specific?"
    author: "Spy Hunter"
  - subject: "Re: Krita"
    date: 2004-01-26
    body: "Which one? On the Krita homepage or on my blog? If the former, well... The sketch of the girl is one I made with pencil and eraser on plain old paper and scanned. The effects on the screenshots on my blog are made with the Gimp's brushes, but I apparently didn't understand some finer points of the way the Gimp handles its pipeline brushes, and I added scaling with pressure sensitivity, which the Gimp doesn't have for those brushes."
    author: "Boudewijn Rempt"
  - subject: "Re: Krita"
    date: 2004-01-26
    body: ">I added scaling with pressure sensitivity, which the Gimp doesn't have for those brushes.\n\nIf you have ideas about how to implement such a functionality, feel free to discuss it with the gimp developers:\nhttp://bugzilla.gnome.org/show_bug.cgi?id=127785\nThey are at present in RC stage for 2.0, but it could be added to the next version (2.2).\n\noliv"
    author: "oliv"
  - subject: "Re: Krita"
    date: 2004-01-26
    body: "Well... One of the advantages of the Krita source is that it is fairly simple at the moment, which means that even someone like me who doesn't know anything particularly relevant can grasp the source and hack away. I've been poring over the Gimp's source for months to find out how they do their nice anti-aliased brush strokes, and I still don't understand it. It's really hard to find ones way in the maze of optimizations and special cases -- and advanced algorithms. So I kind of understand Sven when he says it's not trivial for the Gimp.\n\nWith Krita, most fun things are still trivial... Later, when we've got all those nice optimized brush mask placements and things, then it'll be hard for us, too. "
    author: "Boudewijn Rempt"
  - subject: "Re: Krita"
    date: 2004-01-26
    body: "Will the new krita support higher colour depths (16/12/10 bits per component for instance, instead of the usual 8)? This is the most important feature missing from the Gimp, and cinepaint is quite poor in comparison (feature wise)."
    author: "Tom"
  - subject: "Re: Krita"
    date: 2004-01-26
    body: "Yes. Or at least -- yes, that's the intention. It should be a simple redefine of QUANTUM from Q_UINT8 to, for instance Q_UINT32, and a recompile, to give 32 bits per channel. However... I've been messing with Krita since about October, I haven't ever tried to do that, and I don't know the first thing about computer graphics programming, so it's entirely possible that I've messed up somewhere and made it harder to support higher colour depths. \n\nOf course, if I did so, I'll have to fix it :-). And it's also the intention, and nothing in Krita's core makes that hard, to support images with different colour depths at the same time. But these things take time, and, well, Cinepaint really shines feature-wise, compared to Krita at the moment."
    author: "Boudewijn Rempt"
  - subject: "Re: Krita"
    date: 2004-01-26
    body: "Okay, I made the experiment; it seems that the first problem, if increasing QUANTUM from 8 bits to 32 bits is creating the QImage that is necessary to create the QPixmap that is shown on screen -- QImage doesn't have a constructor that takes wide image data. Of course, that was to be expected, and it would be fairly easy to work around, I just have to down-thingy the data for display on screen I guess, except that I don't know the correct algorithm for doing that."
    author: "Boudewijn Rempt"
  - subject: "Re: Krita"
    date: 2004-01-26
    body: "This where it gets problematic, most graphics cards seem to have trouble with colour > 10bit and i have no idea  what the support in X is like. At least we know cinepaint does it so it is _possible_, just might be dirty. Does krita build against kde 3.1.5? If so i might find time to take a look myself, im really busy with college at the moment though so dont get your hopes up ;)\n\nTom"
    author: "Tom"
  - subject: "Re: Krita"
    date: 2004-01-26
    body: "I guess it just means you don't get to see the complete range of colours or so on you display. Photogenics (http://www.idruna.com/) says it can handle images with 32 bit per channel, too. I guess it is -- in Krita terms -- just doing another colour strategy. Krita doesn't directly show the image data anyway, it always renders it into a QImage, so it's just a matter of finding out how to effectively render 32 bits of channel data in 8 bits of video :-).\n\nAs for your other question: Krita still compiles against 3.1.5. I develop Krita on two computers: a SuSE 8.2 AMD with 3.2 and an old Powerbook G3 with Debian and 3.1.5. I'm not sure for how long you'll still be able to compile all of KOffice against 3.1.5, though.\n\n"
    author: "Boudewijn Rempt"
  - subject: "Re: Krita"
    date: 2004-01-26
    body: "rtfm"
    author: "anonymous"
  - subject: "FLAC"
    date: 2004-01-27
    body: "Does anyone know if KDE is going to support FLAC Audio files?\n\nIt would be nice if this would/could be included in 3.2.x.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: FLAC"
    date: 2004-01-28
    body: "There is an artsplugin for FLAC in kdenonbeta (kdenonbeta/flac_artsplugin). \n\nCouldn't find anything else (except a kfile_plugin) so you might have to wait for KDE 3.3."
    author: "Christian Loose"
---
In <a href="http://members.shaw.ca/dkite/jan232004.html">this week's KDE-CVS-Digest</a>:
<A href="http://edu.kde.org/kstars/index.php">KStars</A> adds more telescopic devices. 
<A href="http://pim.kde.org/components/kaddressbook.php">KAddressBook</A> adds custom field support.
<A href="http://www.koffice.org/krita/">Krita</A> gets working brush and new patterns. 
CSS code from Safari added to KHTML.
<!--break-->
