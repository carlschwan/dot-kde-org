---
title: "KDE-CVS-Digest for April 2, 2004"
date:    2004-04-04
authors:
  - "dkite"
slug:    kde-cvs-digest-april-2-2004
comments:
  - subject: "Thanks"
    date: 2004-04-04
    body: ":-) thanks derek!\nhurrah for kword and kspread"
    author: "Giovanni"
  - subject: "kexi...and others..!"
    date: 2004-04-04
    body: "I thank all those coders who are doing all the dirty work but of late, I have heard nothing from the kexi camp! I'd like to help debugging. The version posted will not compile. Can anyone provide updated information? My wish in regard to eGroupware [and all other Groupwares], is to include all applications and have the installer give an option to have them installed or otherwise. I once tried eGroupware but could not locate the Calender, Email, SiteManager and all other addons. I discovered that these were mere addons to be installed later on. Otherwise, thanx for all the good work."
    author: "charles"
  - subject: "Re: kexi...and others..!"
    date: 2004-04-04
    body: "\"The version posted will not compile\"\n\nAnyway, if it's the case with kexi, don't wait and join #kexi irc channel!\n\njs / Kexi Team\n\n"
    author: "Jaroslaw Staniek"
  - subject: "Rewards?"
    date: 2004-04-04
    body: "Ah it's good to see all the bugfixing lately has been good for something. Now where are the groupies I was promised?"
    author: "Frerich Raabe"
  - subject: "Re: Rewards?"
    date: 2004-04-04
    body: "This is it.\n\n<fawning>\nOhhh!\n</fawning>\n\nDerek"
    author: "Derek Kite"
  - subject: "RoseGarden "
    date: 2004-04-07
    body: "I am sorry to report that RoseGarden still will not build with a new compiler.\n\nI have GCC 3.3.3.\n\n--\nJRT"
    author: "James Richard Tyrer"
---
In <a href="http://members.shaw.ca/dkite/apr22004.html">this week's KDE CVS-Digest</a>: A new release of <a href="http://www.rosegardenmusic.com/">Rosegarden</a>.
<A href="http://edu.kde.org/kstars/">KStars</A> adds ability to use V4L devices. 
<A href="http://www.slac.com/pilone/kpilot_home/">KPilot</A> adds interface to Python and Perl conduits. 
<A href="http://www.kontact.org/">Kontact</A> adds groupware configuration wizards for <A href="http://www.kontact.org/groupwareservers.php">Kolab and eGroupware</A>. 
<A href="http://www.koffice.org/">KWord and KSpread</A> support <A href="http://www.openoffice.org/">OpenOffice</A> format natively. 
<!--break-->
