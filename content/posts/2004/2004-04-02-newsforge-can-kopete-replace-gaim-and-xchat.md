---
title: "NewsForge: Can Kopete replace GAIM and XChat?"
date:    2004-04-02
authors:
  - "binner"
slug:    newsforge-can-kopete-replace-gaim-and-xchat
comments:
  - subject: "user's choice"
    date: 2004-04-02
    body: "What about the #1 user voted program on kde-apps.org, SIM?  [ http://kde-apps.org/content/show.php?content=10080 ]  Has anybody here used it?  Why does it get such a high rating on kde-apps.org (higher than Kopete)?"
    author: "Spy Hunter"
  - subject: "Re: user's choice"
    date: 2004-04-02
    body: "I switch from sim to kopete a while ago because sim was going downhill."
    author: "etoibmys"
  - subject: "Re: user's choice"
    date: 2004-04-02
    body: "Just try it and see why. Almost everybody tried Sim under Linux or Windows switched to it. No IRC support yet (although you may use IRC gateway through Jabber) and some releases are not perfectly stable, but it's very handy and looks perfectly."
    author: "Sergey"
  - subject: "Re: user's choice"
    date: 2004-04-02
    body: "tha main purpose i use it is its i18n support, usability and some nice features like this:\n\ni know when my interlocutor writes (not sends) me message"
    author: "Nick Shafff"
  - subject: "I like SIM's style better"
    date: 2004-04-03
    body: "Being a long-time ICQ junkie, I like the style of SIM's chat windows more.  And perhaps I'm dense, but I couldn't find an option in Kopete to get a message window to close automagically after you've sent a message.  Now that SIM has better multiprotocol support, I'm lovin' it.  \n\nHaving said that, it would be nice if SIM was DCOP-ified, now that I'm playing with Khotkeys.  Also, while I'm sure SIM's plugins make it quite extensible, the configuration of them is annoying.\n\nI haven't had the previously mentioned stability problems with SIM.\n\nPlus, SIM now has weather reports and horoscopes.  That's kind of cool, but it starts to make its name (Simple Instant Messenger) seem a tad ironic, no?\n\n"
    author: "Ed Cates"
  - subject: "Re: I like SIM's style better"
    date: 2004-04-03
    body: "You can change the chat behavoir in Kopete as so:\nConfigure > Behavior > Chat > Single-shot window (Jabber email/Old ICQ style)"
    author: "Anonymous User"
  - subject: "Oh yeah."
    date: 2004-04-03
    body: "I did that, but if memory serves it introduced some other undesired behavior.  I don't remember what it was.  Maybe I'm just too entrenched in the SIM UI to switch.\n\nThat's sad, because I just messed with Kopete a few days ago, because I was having problems signing on to a friend's Jabber server.  Turns out it wasn't the IM clients' fault, it was the end users' fault.  ;-)\n\n"
    author: "Ed Cates"
  - subject: "Kopete && Konversation"
    date: 2004-04-02
    body: "I have recently switched from Gnome to KDE after several years with Gnome.\nNow I'm using Kopete for IM and Konversation for IRC.\n\n"
    author: "Isaac Clerencia"
  - subject: "Re: Kopete && Konversation"
    date: 2004-04-02
    body: "Ya era hora, \u00bfno?\nKDE en los clientes ligeros \u00a1ya!\nY a ver si empiezas a programar con Qt..."
    author: "Quique"
  - subject: "Re: Kopete && Konversation"
    date: 2004-04-02
    body: "huups? \n\nKannitverstan."
    author: "Andre"
  - subject: "Re: Kopete && Konversation"
    date: 2006-03-06
    body: "Creo que se te ha ido la pinza con el lenguaje.\n(I believe you had a language confusion.)"
    author: "mambru"
  - subject: "IRC+IM in one unified client?"
    date: 2004-04-02
    body: "According to my experience (latest stable version in date), Kopete is far from being stable, and far from being mature compared to some other IM (psi, sim, gaim). Of course I can only talk about the non-IRC protocols management.\n\nAlso, mixing IRC chat and IM would require from the user to re-think long-time habits. Up to nowadays, at least for me, IRC clients use was in a big window (sometimes fullscreen, or at least half screen) that resides quite permanently on the screen, whereas IM clients use was only a small contact list somewhere in a corder, with raising \"instant\" chat windows (instant: that may not last long).\nOf course, recent IM clients have gained experience from the IRC clients (tabbed windows, chat mode, multi user conference support, file transfert, and so on), so that both IRC and IM override themselves regarding to the features. But not from the starting idea: the contextual use.\n\nAnyway it's a very good idea IMHO to give it a try, because at least from the IRC client point of view, the contact list could be considered as the IM clients' one, which brings up guidelines for commong UI design and use.. Even if  I'm using gaim/licq and xchat for years, without even using the IRC features from gaim (sounded quite incompatible to me, according to my arguments above).\n\nThere's also an interesting alternative to this: bitlbee.\nAnd another aspect of such topic is that there are really TOO MANY clients or first steps for client projects, and TOO FEW real stable ones. Re-inventing the wheel seems to be hacker's best occupation!\n\nMy 2cts.\n\n-- \nwwp\n"
    author: "wwp"
  - subject: "Re: IRC+IM in one unified client?"
    date: 2004-04-08
    body: "I would encourage you to not summarily dismiss IRC on Kopete without first trying it. \n\nNamely, switch to a theme that lends itself to IRC well ( Like the Simple theme or XChat theme ), and add an account. Lastly, set the chatview grouping policy to \"Group by account\", so all your IRC chats go into one tabbed window - I think you'll be thorughly surprised at how simmilar to your existing clients it is. \n\nYou still have many commands at your fingertips ( and more in CVS Kopete ) ( type /help ), you still have a notify list ( the contact list ), you still have on connect commands, custom CTCP responses, etc. And starting with Kopete 0.9 / KDE 3.3 and the JavaScript plugin, you will have a super-powerful scripting interface as well. Windows don't auto-raise when you are in group chats like IRC, and Kopete never ever steals focus from the desktop anyways.\n\nAnyways, my point is I don't see why so many people dismiss IRC on Kopete, when in reality it is nearly the *exact same* as it is using clients like XChat and KSirc.\n\n\n\n\n"
    author: "Jason Keirstead"
  - subject: "for me"
    date: 2004-04-02
    body: "i replace gaim & xchat with kopete & kvirc.\n\nkonversation feels like a clone of xchat while lately kvirc in cvs gives me powerful control in the graphical way"
    author: "coward"
  - subject: "Re: for me"
    date: 2004-04-02
    body: "Nice to hear that KVirc development for 3.0 is active again. It seemed to have almost halted in-between after version 3.0 Beta 2 (January 2003)."
    author: "Anonymous"
  - subject: "Re: for me"
    date: 2004-04-02
    body: "It's been active all the time, but just in cvs. There should be stable release coming \"soon\". They are in feature freeze so next release is likely 3.0.0."
    author: "Jorma Tuomainen"
  - subject: "Re: for me"
    date: 2004-04-02
    body: "> It's been active all the time, but just in cvs.\n\nPossible, it's just that the web site didn't show any activity for over a year."
    author: "Anonymous"
  - subject: "Re: for me"
    date: 2004-04-02
    body: "That is good to hear. I like some of the features of KVIrc over X-Chat (how it handles dcc chats for one) but it was too buggy so I switched to X-Chat. I'll probably switch back when a stable KVIrc comes out."
    author: "Ian Monroe"
  - subject: "SIM much better than Kopete"
    date: 2004-04-02
    body: "I prefer Kopetes' elegant UI but it just isn't stable enough and SIM beats it due to the fact you can send messages to more than one person at a time, a must in my book. Both aren't great on conferencing (we're talking jabber backend here) but I suppose I can't complain as I could get in there and give a hand if I only had the time ..."
    author: "Jon"
  - subject: "Re: SIM much better than Kopete"
    date: 2004-04-04
    body: "You can send messages to more than one person at a time, depending on what you're doing.  With MSN, you can invite multiple people into a chat - and using the IRC plugin, and MSN plugin with multiple people behave identically.  I love that."
    author: "Troy Unrau"
  - subject: "What about ICQ?"
    date: 2004-04-02
    body: "Everytime I've tried Kopete, I've been terribly dissapointed with it. Maybe it's because everybody here (in Europe) has ICQ while almost nobody has MSN, AIM, Yahoo or Jabber. Currently I use LICQ.\n\nAnyway, I tried it several times, the last time a few months ago and it never worked reliably, does anybody use Kopete with ICQ here?\n\n"
    author: "Roland"
  - subject: "Re: What about ICQ?"
    date: 2004-04-02
    body: ">\n>Anyway, I tried it several times, the last time a few months ago and it never worked >reliably, does anybody use Kopete with ICQ here?\n\nI use Kopete with ICQ, I also use it with IRC. Both are working fine for me. I used to have a little problems with stability but they seem to be gone on the version(0.8.1) I use now. "
    author: "138"
  - subject: "Re: What about ICQ?"
    date: 2004-04-02
    body: "\"Maybe it's because everybody here (in Europe) has ICQ while almost nobody has MSN, AIM, Yahoo or Jabber. Currently I use LICQ.\"\n\nYou've never been to the Netherlands then. Almost everybody here uses MSN."
    author: "Anonymous"
  - subject: "Re: What about ICQ?"
    date: 2004-04-06
    body: "Same here.\n\nIn Spain *everybody* uses MSN :( It's a luck that kopete, and amsn are good enough."
    author: "suy"
  - subject: "Re: What about ICQ?"
    date: 2004-04-02
    body: "> Maybe it's because everybody here (in Europe) has ICQ while almost nobody has MSN, AIM, Yahoo or Jabber.\nHuh? I think you're joking? :)\nHere in The Netherlands everybody uses MSN. With everybody I dont mean only my friends and/or class mates, but almost everybody with an internet account. From my 6-years-old nephew to my father, they're all using MSN."
    author: "Niek"
  - subject: "Re: What about ICQ?"
    date: 2004-04-02
    body: "I used kopete for some time for icq, hoping from release to release that someone would fix this annoying \"could not parse xml..\"-thing that causes the loss of messages. \nSo I switched to licq. Not that beau, but reliability is more worth than looking nice.\nI *really*hoped* if it s released with kde 3.2 it would be solid, but it is not. "
    author: "Donbot"
  - subject: "Re: What about ICQ?"
    date: 2004-04-02
    body: "I see this problem too. Sorry, but Kopette is unusable with this problem."
    author: "Petr Balas"
  - subject: "Re: What about ICQ?"
    date: 2004-04-03
    body: "I'm running Kopete (0.8.1) with ICQ only - and it works like a breeze. You just have to select a style (see http://bugs.kde.org/show_bug.cgi?id=69417, #16)\n\nDaniel"
    author: "Daniel"
  - subject: "Re: What about ICQ?"
    date: 2004-04-02
    body: "Hi,\n\n   This is Spain, and almost everybody uses MSN here.. Damn, I don't know anybody using ICQ nowadays :)\n\nRegards,\n\n   - Juan"
    author: "Juan"
  - subject: "Re: What about ICQ?"
    date: 2004-04-04
    body: "es M$N, no MSN ;)"
    author: "galio"
  - subject: "Re: What about ICQ?"
    date: 2005-03-16
    body: "In Czech Republic about 80% uses ICQ, rest MSN. Hackers IRC or Jabber ;)\nKoppete is really strenght piece of software. Sometimes works, sometimes no. Currently i'm switching between Kopette and Sim."
    author: "Pavel"
  - subject: "Re: What about ICQ?"
    date: 2004-04-20
    body: "i use licq it's no the same thing, I'm having problems with conections to icq2003b \nmsn is m$n, it's time to create an linux icq compatible with v7 and v8"
    author: "cesar lopez"
  - subject: "Nope, can't be done"
    date: 2004-04-02
    body: "Gaim and XChat are too good for Kopete. I hate the way Kopete handles contacts. It will mess up your buddy lists if you ever sign on that screen name elsewhere and you can't easily delete stuff from the list."
    author: "Turd Ferguson"
  - subject: "Re: Nope, can't be done"
    date: 2004-04-02
    body: "Yeah, the same thing happened to me, finally I had to switch back to gaim and a new AIM account to get a clean contact list again.  \n\nKopete's \"metacontact\" feature is nice, but that wasn't working well either; contacts would not stay associated into the same metacontact.  (Haven't tried it recently, perhaps it's improved.)"
    author: "Anonymous"
  - subject: "Kopete can't handle disconnects"
    date: 2004-04-02
    body: "My #1 BIGGEST problem (and why I recently got so tired of it and switched) with Kopete is that if AIM/ICQ drops for whatever reason, A) I'm not notified that i'm offline, and B) it doesn't reconnect me!  I've been offline sometimes for more than a DAY and didn't know it!!!!!!\n\nMy #2 problem is that when I go away and someone IM's me, it does NOT send the away message to that person.  \n\nAs far as any IM program replacing a full time IRC client, Kopete/Gaim is ok for the CASUAL IRC user but if you're on multiple servers with mulitple channels and/or you're an op or IRC op then you need a full time IRC client be it XChat, Konversation, or KVIrc."
    author: "Jeff Stuart"
  - subject: "Re: Kopete can't handle disconnects"
    date: 2004-04-02
    body: "I forgot to add that for #1, yes I KNOW that the Oscar proto is being COMPLETELY rewritten.  We'll see how that goes."
    author: "Jeff Stuart"
  - subject: "Re: Kopete can't handle disconnects"
    date: 2004-04-03
    body: "I hate how it pops up the MSN disconnect dialogs.  Why does it not use a ballon like messages?"
    author: "Anonymous User"
  - subject: "Gaim vs Kopete"
    date: 2004-04-02
    body: "Dear all\n\nNice, fair article by Roblimo. I use kopete for two reasons: integration with KDE and User Interface (very  attractive and intuitive). \n\nThe problem with Kopete is that it is in-between alpha and beta stages. It really is not a mature app (as opposed to Gaim, where things \"just\" work). The switch from ICQ to OSCAR protocol for icq users for instance is still causing some pain, and it was a real pain in the back for a long time.\n\nAfter all, kopete is at 0.8 version, and it is a great app for a pre-golden stage. My feeling is that they pretty much finished the UI before the backend was mature, that's all. It is useable, but there are some glitches here and there. As an example, if I disconnect my modem connection at night, and leave kopete running, there is no way I can get it to re-login in the morning. \n\nI'd really love to see more projects follow the FSF recommendation to _separate_ the GUI from the main app, and make it easy to reuse the application from either the command line or another GUI. Imagine if Kopete could just use the \"gaim engine\" as a backend, its developers would contribute to enhance it, and Gnome users would use Gaim and KDE users Kopete as a front end. Oh well ...\n\nLet me finish with a big THANK YOU, GRACIAS, DANKE, MERCI, GRAZIE (*)! to the Kopete team :-)\n\n(*) sorry for mispellings and ommissions ;-)"
    author: "MandrakeUser"
  - subject: "Re: Gaim vs Kopete"
    date: 2004-04-11
    body: "Gaim is actually working on separating the UI and backend.  But at this point I doubt Kopete would switch, even if it was ready.\n\nSometimes it is handy to have multipe backends, just in case one protocol change by MS/Yahoo/AOL breaks only one of the implementations.  Then you can take a look at the other, or even switch if you want."
    author: "anonymous"
  - subject: "Hmmmm"
    date: 2004-04-02
    body: "Replace Gim? Wasn't April Fool's Day yesterday?"
    author: "Lig"
  - subject: "OT ?"
    date: 2004-04-02
    body: "GIM Homepage: www.gimsoft.com\na bit far fetched, don't you think? I can't see any link to kopete or gaim or xchat..."
    author: "Thomas"
  - subject: "kopete"
    date: 2004-04-02
    body: "Ehm I'm the kinda always busy person, so I'd really love to see kopete starting in the same mode(status eg. away offline, invisible:) etc.) as I closed it. This is the <STRONG>top-feature </STRONG>which I really miss\n"
    author: "miro"
  - subject: "Re: kopete"
    date: 2004-04-03
    body: "Have you tried:\nkopete --help\nit seems you can start and connect only some or noone of your connexions.\nAnd this command line seems to work too:\ndcop kopete KopeteIface setAway"
    author: "mi"
  - subject: "Re: kopete"
    date: 2004-11-16
    body: "I would like Kopete to be able to start invisible, as AMSN does or Licq. That's the only reason i keep using them both instead of Kopete. This feature is very, very easy to implement, if I had no little children I'll program it my self, but right now I don't have the time, so please, if someone of the developers can read this, I think Kopete is great, it just needs this feature, to be capable of starting as invisible.\n\nCheers!\n\nJimmy"
    author: "Jimmy"
  - subject: "Re: kopete"
    date: 2004-11-16
    body: "Do you mean completely invisible, or is a little icon in the system tray acceptable? If the latter, just start Kopete once, close the window and never completely quit it before quitting KDE. It'll be restored by KDE's wonderful session management as a single systray icon and won't show its window."
    author: "Boudewijn"
  - subject: "Re: kopete"
    date: 2004-11-16
    body: "I think he means the *status* \"invisible\".  He would like to connect to the IM service without being displayed as \"online\". \n"
    author: "cm"
  - subject: "Re: kopete"
    date: 2007-01-31
    body: "This is really a missing feature! Right now I'm using Kopete 0.12.3 and I can not start Kopete with account status other than \"online\". :(\n\n"
    author: "Douglas"
  - subject: "Re: kopete"
    date: 2007-02-27
    body: "Just like to add my support for this request. Being able to set a preferred startup status is an essential missing feature. \n\nKeep up the good work!"
    author: "Ian B Gibson"
  - subject: "Re: kopete"
    date: 2007-06-07
    body: "Yes it would be nice to have one or both these features:\n- kopete starting with last status for each account\n- kopete starting with a user-set default status for all accounts (this will override the previous one).\n\nbye,\nGiovanni."
    author: "Giovanni"
  - subject: "Re: kopete start invisible"
    date: 2007-06-18
    body: "I've modified source code of version 0.12.2 to start all accounts invisible. It works with msn accounts, i haven't tried with other. It's a provisional solution until the feature is implemented oficialy. The source is on the attachment.\n\nbye"
    author: "pablo"
  - subject: "replace?"
    date: 2004-04-02
    body: "Umh... a lot of complaints about stability of kopete here. I've to take up the cudgels for kopete, me thinks. Kopete does _not_ crash every minute over here. In fact it starts when I login at morning (and kwallet asks for confirmation) and just works reliably until I log off in the eve. \n\nWell... I dunno if kopete can _replace_ gaim (and I really don't care)...\n\nI don't like the assumption, that you've to use gaim and then \"replace\" it with kopete. Actually, I started using this IM thingy _because of kopete_. I tried it once when it was still at 0.6something and immediatly liked it. Back then kopete was buggy as hell, so I've been using gaim which was more stable. But now at 0.8 and kwallet integration kopete is definitively my choice."
    author: "Thomas"
  - subject: "Re: replace?"
    date: 2004-04-06
    body: "And over here it crashes everytime i try to connect to AIM..."
    author: "Anonymous Coward"
  - subject: "Kopete's Stability"
    date: 2004-04-02
    body: "Been reading the comments and I wonder if the reason for Kopete's instability is due to the flavour it's built on.  At home I have kopete running in Gentoo and it works unbelievably well.  However, my work system being Fedora (yes, I am lucky enough to use Linux at work) I can't get the rpm to install at all.  Can't rebuild from the source rpm because the kde/rpm website doesn't seem to have it anymore (or at least last time I checked using pbone and apt).\n\nAs for rebuilding from source, I couldn't actually get it to generate one binary even though the build returned successfull.  Turns out libjpeg wasn't installed and I had no error to say it was required.  Still doesn't work even with libjpeg, although the binaries do compile.  I haven't had time to tackle it so I'm stuck using gaim at work which I find rather limiting compared to Kopete.  Mind you the only limitation may be that Kopete simply looks much nicer ;-)\n\nIn any case, Gentoo's emerge built it up without a problem even with my custom Athlon kernel whereas Fedora is still choking.  So the problems may just be the platform.  BTW, Kopete had no problems when I was running Redhat 9 at work :-(\n\nCheers!"
    author: "Anthony Rabaa"
  - subject: "Re: Kopete's Stability"
    date: 2004-04-05
    body: "I have debian and used to find kopete too unstable. Then I moved to an office where the net connection didn't go down three times a day and now everything is fine.\n\nkopete is just really bad at dealing with network instability and you either experience that or you don't. (right now i don't - yay!)"
    author: "c"
  - subject: "Re: Kopete's Stability"
    date: 2004-04-08
    body: "Just a follow up to my thread.  I've upgraded to KDE 3.2.1 and Kopete works great.  Quick and stable.  Excellent work!"
    author: "Anthony Rabaa"
  - subject: "Kopete team, you listening?"
    date: 2004-04-05
    body: "With so many complaints about Kopete here, I surely hope you guys write feature requests in the bugzilla as well! (So the developers at least have a chance of perfecting their work.) As I see it, it's progressing very nicely, the best feature being kaddressbook of course."
    author: "rimmer"
  - subject: "Re: Kopete team, you listening?"
    date: 2004-04-06
    body: "wwwoooooowww!!! kadressbook integration???? finally!!! as soon as my inet connection is alive again... KDE 3.2.2 with the latest kopete - here I come ;-)"
    author: "superstoned"
  - subject: "Perhaps for Gaim, not for XChat"
    date: 2004-04-06
    body: "Personally, I think that IM clients are horrible for IRC. I wouldn't use either Kopete or Gaim to connect to an IRC server.\n\nThat said, Kopete is on the way to becoming a great IM client. It's not there yet, but it's improving. I still prefer Gaim, but that may very well change by the time Kopete 1.0 is released. And it already has Gaim beat in one area--transparency :)\n\nAs for IRC, I'd bet on KVirc as ultimately replacing XChat. As with Kopete, it's not there yet, but it's on its way."
    author: "QV"
  - subject: "Re: Perhaps for Gaim, not for XChat"
    date: 2004-04-06
    body: "> As for IRC, I'd bet on KVirc as ultimately replacing XChat. As with Kopete, it's not there yet, but it's on its way.\n\nYou can also try vertigo.. which is the (beginnings) of a native KDE gui for xchat (which has a plugin-based GUI that is not dependent on gtk).. \n\nSee \nhttp://cvs.berlios.de/cgi-bin/viewcvs.cgi/vertigo/\nhttp://developer.berlios.de/cvs/?group_id=1012"
    author: "fault"
  - subject: "Re: Perhaps for Gaim, not for XChat"
    date: 2004-04-07
    body: "Now THAT sounds interesting :)\nI'm using Kopete myself (and even have coded on it in the past) but for hardcore IRC-users an IM app just won't do it. I doubt Kopete, gaim or Trillian will ever support all the stuff I do in IRC, mainly:\n- nickserv register\n- system info scripts\n- mediaplayer scripts\n- blowfish encryption\n- trigger for about any sentence you can think of\nOf course this could all be made possible in apps like Kopete but OTOH I think that would be total overkill."
    author: "mETz"
  - subject: "Re: Perhaps for Gaim, not for XChat"
    date: 2004-04-08
    body: "- Add the nickserv command to your \"On Connect\" commands in the account config\n\n- Media player stuff is already in the Now Listening plugin, and I would wager it has support for more media players and paramaters than any script you are currently using\n\n- All the rest would be doable via the JavaScript plugin using KJSEmbed, which will be in KDE 3.3 (and maybe Kopete 0.9 if I can get it done in time). The scripting in Kopete is going to be very powerful, much more powerful than anything XChat or Mirc has.. you'll be able to access nearly every susbsystem in the Kopete API, not just set simple triggers."
    author: "Jason Keirstead"
  - subject: "If only it supported proxied AIM"
    date: 2004-04-06
    body: "I think kopete a great product at home. However, if I'm going to use it at work for AIM messaging, it needs to support proxies. "
    author: "b12arr0"
  - subject: "Re: If only it supported proxied AIM"
    date: 2004-04-07
    body: "SOCKS is supported, HTTPS is not due to the missing support in kdelibs (will most probably change for KDE 3.3, of course using that API is already planned and at least I am just waiting to code something working based on the new libqt-addon sockets :) ).\nAlso, with such a little amount of developers there is no time for writing a HTTP-proxying class, especially if none of the developers ever needed this (remember, this stuff is mostly written for fun so you usually code what you want/need most, not only what others want)."
    author: "mETz"
  - subject: "What is Kopete IRC lacking?"
    date: 2004-04-08
    body: "I am seeing many comments in this forum along these lines:\n\n\"Personally, I think that IM clients are horrible for IRC. I wouldn't use either Kopete or Gaim to connect to an IRC server.\"\n\nAs the IRC lead dev. for Kopete, I would be extremely interested to know what you, as users, feel Kopete is lacking for IRC support, as compared to say, XChat, Konversation or KSirc. It has never been my goal for Kopete to be a half-cooked IRC client. To me, Kopete already has nearly everything these clients can offer, and much more (especially the XChat argument... switch Kopete to Tabbed mode and use the XChat theme, and tell me the functionality difference? ). However, your feedback can help make it better.\n\nNamely,\n - What features do you feel are missing that X client has?\n - What features do not work the way you need them to for IRC?\n\nNote that it is well known that we do need a way to set a different chatting theme for group chats ( like IRC ) vs. single user chats ( like most MSN, ICQ etc. ), and we're working on possibilities.\n\nThanks, feedback is greatly appreciated.\n\n"
    author: "Jason Keirstead"
  - subject: "Re: What is Kopete IRC lacking?"
    date: 2004-04-09
    body: "SSL support is the #1 thing keeping me from using kopete for my irc needs."
    author: "Anonymous"
  - subject: "Re: What is Kopete IRC lacking?"
    date: 2004-04-09
    body: "SSL support is already in CVS HEAD ( has been for months ), so the next release will have it."
    author: "Jason Keirstead"
  - subject: "Re: What is Kopete IRC lacking?"
    date: 2004-04-28
    body: "I just don't like having to use the same program for IM and IRC... just doesn't make sense.\n\nI will continue to use Xchat"
    author: "Michael Yartsev"
  - subject: "Re: What is Kopete IRC lacking?"
    date: 2004-05-13
    body: "Autoconnection upon startup.\n\nOr is this lack my fault?"
    author: "Sebastian Arming"
  - subject: "Re: What is Kopete IRC lacking?"
    date: 2004-11-23
    body: "Limited to running Kopete 0.9 as CVS requires KDE 3.3 but would like to see a timeout option for IRC re/connection (unsure if this is in CVS). Currently can't connect to Austnet.org as connection taking along time. Can connect with other IRC clients even gaim, I assume its time out issue as in log on window will say \"You are now offline\" during log on process, managed to get online once assume that things were going quicker than normal.\n\nThanks\n\nSam"
    author: "Sam"
  - subject: "Re: What is Kopete IRC lacking?"
    date: 2004-11-29
    body: "Hello Sam. I am no longer IRC maintainer, but the timeout settings for Kopete IRC can be controlled under KControl -> Internet & Network -> Preferences. Kopete uses these settings (the standard KDE ones). for timeouts.\n\n"
    author: "Jason Keirstead"
  - subject: "Re: What is Kopete IRC lacking?"
    date: 2008-07-16
    body: "It lacks nothing for me! I love the IRC support in Kopete. In fact, it's pretty much the only thing I use Kopete for. The Windows guys in my office wish they could run Kopete to connect to our office IRC channel. I need a chat client that gets focus whenever it receives a message, and Kopete does that just fine.\n\nStrangely, the KDE 4 version of Kopete doesn't seem to have IRC support. We'll proabably upgrade to KDE 3 sometime soon, and we're concerned about this problem. Are there any plans to implement IRC?"
    author: "Rich Vanderwal"
  - subject: "Re: What is Kopete IRC lacking?"
    date: 2008-07-31
    body: "I noticed this too.  Are there any plans to add irc back in?"
    author: "greg"
  - subject: "Re: What is Kopete IRC lacking?"
    date: 2008-11-05
    body: "I'm missing the IRC support too.\n\nSee <a href=\"https://bugs.kde.org/show_bug.cgi?id=167884\">this bug report</a> for more information."
    author: "haba7"
  - subject: "Kopete is unreliable"
    date: 2004-04-08
    body: "I've been using Kopete for a while but I had to switch to gaim when I found out that every once in a while Kopete just didn't deliver some message. The main problem is that neither the sending host (kopete) nor the receiving host (also kopete) displays any error message. The message seems to be sent correctly (and shows up in the chat window of the sending kopete), but it is never received (i.e. it doesn't show up in the chat window of the receiving kopete), and _neither_ client shows any error message although _both_ should. I do like the program but I simply can't use something that fails silently like that.\n\nA similar thing happens when the other end has a client with more features than kopete, such as file transfer for a particular protokol. The other client tries to send a file but kopete is completely silent. It should at least tell me that the other client is trying to do something that isn't supported by kopete yet. (This happens in gaim, too, though. E.g., my sister tried to send me a picture from her MSN Messenger but gaim was just dead silent. No error message, nothing.)\n\nIf I send a message (or a file or whatever) I really want to know whether the other party received it successfully or not."
    author: "Marcus Sundman"
  - subject: "What's missing in the review"
    date: 2004-04-08
    body: "The review does not mention the meta-contact feature of kopete, which is a killer-feature for me. I have some contacts, that are reachable via different IMs, but are not always online with all their accounts."
    author: "Yaba"
  - subject: "Re: What's missing in the review"
    date: 2004-04-11
    body: "Gaim actually has support for grouping contacts into meta-contacts as well.  It isn't very well documented, but it works just fine.\n\nI liked the way Everybuddy did it best, but it seems that development on that app isn't nearly as fast as gaim or kopete.  I switched to gaim a couple years back, and have been pretty happy.  Plus I can have it and xchat on my Windows box at work, which is handy.\n\nI'm certainly keeping an eye on Kopete though."
    author: "anonymous"
  - subject: "Kopete Works Fine"
    date: 2004-04-09
    body: "I use Kopete with my ICQ and MSN IM accounts, and it works flawlessly.\nThe version in KDE 3.2 is quite stable and polished compared to previous releases.\nIntegration with KWallet is very nice to have. Being able to attach files in ICQ is the only thing I could wish for."
    author: "Guy Mac"
  - subject: "did i miss something"
    date: 2004-04-15
    body: "i finally broke down & thought i'd give kopete a complete try this morning.  i added all of my accounts & began to fiddle w/ the options to what i'm confortable with.  the author definitely talks of file transfer, but i could not find such an option for any of my protocols.  i realize this doesn't work in gaim either, but it's probably the only thing i'm really missing from my im experience.  also, kopete wouldn't remember my password, despite the checkboxed option & it being in my kde wallet.  i couldn't find a way to turn the fonts off for chats (i had a couple friends who \"didn't like them\").  i suspect an html-type mode by default w/out an option to change it.  i could change the fonts to something more appealing for them, & i eventually found the \"master\" font, - just made the experience slightly less fun.  oh, & what i miss most, was that i couldn't find an option to send the message w/ a carriage return only, w/out it trying to continue my message onto the next line.  i found that i could do it w/ ctrl-return, but i like the behavior switched, continuing to the next line *while* pressing a meta-key.  lastly, i missed the fact that i could press escape in an open chat to close the chat.  as a programmer, i spend most of the time at the keyboard but had to drag the mouse to close the box after a simple \"ok\" reply.  i think i'm done & sorry for the nit-picks.  just my real life experience.  it's a great program otherwise.  i will come back to check on it in time."
    author: "mark"
  - subject: "thinking to switch"
    date: 2004-04-24
    body: "At work we are using nice KDE desktops (SuSE 9.0) w/ Kopete 0.8 installed. But, unfortunately Kopete is not as stable as we require. All we need is to talk over Yahoo! and there are no other protocols required, however, Kopete does not send messages to the recipient. It displays as we are logged in but in fact we are not.\nnow we are searching for other alternatives :(. Yahoo! does not have an rpm for SuSE and I think we will give Gaim a try which I don't really wish to use b/c it is GTK based. \nMeanwhile, we would like to switch back to Kopete when it is going to be MUCH MORE reliable.\n\n"
    author: "ao"
---
<a href="http://www.newsforge.com/">NewsForge</a>'s <a href="http://software.newsforge.com/software/04/03/29/1213223.shtml?tid=82&tid=90&tid=92">Robin Miller tested</a> if <a href="http://kopete.kde.org/">Kopete</a> satisfies his instant-messaging and IRC needs and can replace the separate applications <a href="http://gaim.sourceforge.net/">GAIM</a> and <a href="http://www.xchat.org/">XChat</a> he used so far. He asserts that Kopete is a fine choice especially if you run it under KDE and enjoy the good KDE  integration.




<!--break-->
