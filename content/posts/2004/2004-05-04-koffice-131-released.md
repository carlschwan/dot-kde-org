---
title: "KOffice 1.3.1 Released"
date:    2004-05-04
authors:
  - "ltinkl"
slug:    koffice-131-released
comments:
  - subject: "Knoppix"
    date: 2004-05-04
    body: "Maybe this will change Klaus Knoppers mind."
    author: "reihal"
  - subject: "Re: Knoppix"
    date: 2004-05-04
    body: "I bet against this. Better convince him to include the KDE-fied OpenOffice.org version."
    author: "Anonymous"
  - subject: "Re: Knoppix"
    date: 2004-05-04
    body: "Klaus is very KDE-friendly. He cares for his distro to be *usable* and mainstream. That's why he defaults the desktop to KDE. That's why he chooses OpenOffice, if *space reasons* only allow for one office suite. \n\nIf you followed the pre-decision debate on the debian-knoppix@linuxtag.org mailing list, you'd have seen that he likes KOffice very much for its lightweight footprint and its uniq ability to *edit PDFs* as compared to the memory hog of OpenOffice.\n\nKOffice will be back on the LinuxTag edition of Knoppix for sure -- because this will again be a DVD  ;-)"
    author: "anon"
  - subject: "Re: Knoppix"
    date: 2004-05-05
    body: "Aah, DVD!\nWhen can I bittorrent that?"
    author: "reihal"
  - subject: "Re: Knoppix"
    date: 2004-05-04
    body: "ArkLinux has it and it looks great. But ArkLinux also has the most recent version of KOffice too of course ;)"
    author: "noman"
  - subject: "Re: Knoppix"
    date: 2004-05-05
    body: "ArkLinux surely is one of the best (future) choices when it comes to single-user Linux (harddrive) installations, but for that they better get their version of the 2.6 kernel integrated and move from xfree86 to fd.o or atleast x.org. ;-)\n"
    author: "Nobody"
  - subject: "Re: Knoppix"
    date: 2004-05-06
    body: "The fd.o and X.org stuff is technically inferior at the moment since it it just an old rebranded XFree86. At the moment you are better of the the Real Thing (R) ;)\n\nwith Kernel 2.6 there aren't`many issue seen anymore and it is available through the Ark Update services.\n\nMarc "
    author: "Marc"
  - subject: "Binaries?"
    date: 2004-05-05
    body: "No contributed binaries? Where are you SuSE KDE devels? :)\n"
    author: "John Binary Freak"
  - subject: "Re: Binaries?"
    date: 2004-05-05
    body: "http://download.kde.org/stable/koffice-1.3/SuSE/"
    author: "David Faure"
  - subject: "Re: Binaries?"
    date: 2004-05-05
    body: "Ooops, I didn't notice the 1.3 in that URL. Sorry.\nThe binary packages for 1.3.1 on SuSE are being uploaded right now."
    author: "David Faure"
  - subject: "Re: Binaries?"
    date: 2004-05-05
    body: "Already there, thanks!! :)"
    author: "John Binary Freak "
  - subject: "Re: Binaries?"
    date: 2004-05-08
    body: "hi there,\ni wonder if i need internationalization files, couldn't fnd any german ones at suse. well, i dont ight much, but it would be nice if the menu would be in the same langiage as all my other apps, or am i missing something?\n\nthx!\n"
    author: "guest"
  - subject: "Re: Binaries?"
    date: 2004-05-08
    body: "The noarch/ directory?"
    author: "Anonymous"
  - subject: "Re: Binaries?"
    date: 2004-05-08
    body: "/nocrach has nothing for koficce 1.3.1:\n\nftp://ftp.suse.com/pub/suse/i386/9.0/suse/noarch\n\nhere neither:\nftp://ftp.suse.com/pub/suse/i386/update/9.0/rpm/noarch\n\nso i am a little lost. could you please provide me with a link? thx!\n"
    author: "guest"
  - subject: "Re: Binaries?"
    date: 2004-05-08
    body: "ftp://ftp.kde.org/pub/kde/stable/koffice-1.3.1/SuSE/noarch\nftp://ftp.suse.com/pub/suse/i386/supplementary/KDE/update_for_9.0/internationalization"
    author: "Anonymous"
  - subject: "Appreciation thread..."
    date: 2004-05-05
    body: "Well, I bascially just wanted to say how much I appreciate the hard work koffice developers have done. I've made a half a dozen school papers with kword during this spring, and I've been really happy with it. I really have. And I've done all my little spredsheets ( financial, weight watchers points, etc. ) with kspread for more than a year now, and it too had worked like a charm for me.\n\nI think koffice is seriously ready for the prime time, well worthy competitor to OO.org and MS Office. All KDE users using other office pakages right now should at least try koffice, it might actually surprise you!    "
    author: "jannek"
  - subject: "Re: Appreciation thread..."
    date: 2004-05-05
    body: "Actually, I did it yesterday again. I wanted to import a table from PDF to KSpread, that means from clipboard with space delimiters. That worked very well. But as soon as I started to edit any cell the contents of every second row was automatically deleted. No reverse option with 'Undo' - even worse. Some cells reappeared, but even more were deleted. Funny feature. \n\nI also tried to import the clipboard contents to a table in KWord, but I didn't figure out how wo let it make a table out of it. Moreover, I couldn't find any option like 'insert table' or similar at all in the menu.\n\nAfter I managed the import to KSpread I wanted to transfer the table to openoffice - no chance. if you copy the contents into the clipboard it does it in a format that can not be read by any app except vi. \n\nThese and more experiences with sudden crashes at longer datarows (who needs a spread app that can only handle less than 100 rows without crashing?) or totally unusefull diagram output formats I gave totally up with KOffice. Sorry. What a prime time."
    author: "Sebastian"
  - subject: "Re: Appreciation thread..."
    date: 2004-05-06
    body: "I don't think there's much point in being sarcastic (it sounds like that) but I agree with what you're saying. Hopefully you file a bug report for each problem you find, and the programmers will fix them when they can - otherwise you're just pi**ing in the wind.\n\nIf KOffice stabilized around its existing functionality and became *rock* solid with no quirks in selecting and displaying data, it would have a useful, if limited, niche in the market place.\n\nUnfortunately each time I've tried to use it seriously, I've also found it unstable and quirky, and quickly gone back to the unlovely OO which is horrible but works. To be fair, I've also found Abiword and Gnumeric to be less than reliable (Gnumeric shows promise though).\n\nI don't think there's a solution to this, but I hope the developers take time out to do a serious code review when they start the OASIS integration stuff.\n\nHopefully this new release improves stability, but there's no way the earlier releases could be used in a business environment - all IMHO of course, but I think that's the reality ATM."
    author: "Rich"
  - subject: "Re: Appreciation thread..."
    date: 2004-05-06
    body: "No, there was no code review before starting the whole change to OASIS.\n\nIn my opinion, it has more sense to try to fix things after the changes to OASIS and Qt4 are done than before that. (Of course it would be different if KOffice 1.3.x could be fixed in the process, but it has little chance, as we need the available manpower on the developer version.)\n\nAnd of course having more volunteers to have more manpower would help too...\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Appreciation thread..."
    date: 2004-05-06
    body: "Fair enough - it's far easier to criticise than actually do it! And it's not a criticism as much as frustration at a project that hasn't quite got things right so far IMHO.\n\n> And of course having more volunteers to have more manpower would help too...\n \nOf course :) If you need some perl code I can help, but I get the feeling that's unlikely!"
    author: "Rich"
  - subject: "Re: Appreciation thread..."
    date: 2004-05-07
    body: "There are some Perl code in KOffice (as in KDE). Mostly it is to help to extract the user visible strings that are to be translated.\n\nAlso there are a few scripts in kdesdk/scripts that might need some polishing.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Appreciation thread..."
    date: 2004-05-07
    body: "OK, I'm more than happy to have a look at them.\n\nCould you point me in the right direction for downloading the code - I presume I need to grab the CVS sources but havn't done this before.\n\nThanks"
    author: "Rich"
  - subject: "Re: Appreciation thread..."
    date: 2004-05-07
    body: "The documentation about anonymous CVS is at:\nhttp://developer.kde.org/source/anoncvs.html\n\nIf you want to look at KOffice, you have to download the koffice module. (It can be compiled on KDE 3.2.x too.) It is explained how to build the module in: http://www.koffice.org/download/source.php\n\nAs for the kdesdk module, it would work only with KDE CVS HEAD but for the scripts, it is not much important.\n\nWhat you could work on is the script kdesdk/scripts/extractrc . That is the main script for extracting the strings to translate from XML files. One detail that could be made better is that all options (for example --tag ) are tested one after another. It should be tested first if the option starts with -- to speed up for normal files as parameters. (There are other possible changes to that file, including one which could replace the private variant of this script that is in KOffice.)\n\nFor further information or your first changes/patch on a file, please contact me directly: mailto:goutte@kde.org\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Appreciation thread..."
    date: 2004-05-07
    body: "I'm not sure what went wrong for you, but I'm regularly using a spreadsheet table with thousands of rows in kspread without problem.\n\nThe clipboard-based conversions are not implemented, we have many more conversion possibilities in the import/export filter, i.e. you have more chances of getting conversions to work if you go via files. But from a PDF???"
    author: "David Faure"
  - subject: "I like Koffice..."
    date: 2004-05-06
    body: "I've been using it for some time (since 1.2), for my normal homework - some papers, some small graphs. I dont use the features OO offers me, although I'm a bit annoyed by the bad table-features. but overall, 1.3 is good enough for me. I whouldnt say it beats OO or m$O in features, but it sure beats them for me - it can do whatever I need, so that's enough..."
    author: "superstoned"
  - subject: "Re: I like Koffice..."
    date: 2004-05-06
    body: "I used OOo for writing my dissertation.  There is no way I could have used koffice, since the documents need to shared/edited in the windows world.  I am eagerly awaiting koffice 2.0, with native OOo format.  If the documents in koffice render *exactly* as OOo (tables, figures, equations, drawings, etc.), then probably all the kde users using OOo can switch to the koffice.\n"
    author: "A nony mous"
  - subject: "Updated changelog is online"
    date: 2004-05-07
    body: "The updated changelog is now online, including the missing changes for the OOImpress and OOCalc filters.\n\nhttp://www.koffice.org/announcements/changelog-1.3.1.php\n\nHave a nice day!"
    author: "Nicolas Goutte"
---
The KOffice team is happy to bring you the first bugfix package that builds upon the successful 1.3 version, adding even more enhanced OpenOffice.org import and export filters, improved spellchecking with ispell, fixes in hyphenation and many more. See the <a href="http://www.koffice.org/releases/1.3.1-release.php">release notes</a> and the complete <a href="http://www.koffice.org/announcements/changelog-1.3.1.php">list of changes</a>.


<!--break-->

