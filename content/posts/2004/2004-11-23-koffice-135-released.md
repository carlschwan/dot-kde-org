---
title: "KOffice 1.3.5 Released"
date:    2004-11-23
authors:
  - "ngoutte"
slug:    koffice-135-released
comments:
  - subject: ":)"
    date: 2004-11-23
    body: "Great, keep up the good work. A few binaries already available, I hope more distros contribute though, KDE 3.3.2 is also around the corner, so the timing is pretty good :)"
    author: "User"
  - subject: ".swx-compatibility?"
    date: 2004-11-23
    body: "can Koffice (Kwriter then, I guess) save its documents already in .swx and/or read .swx files?\n\nThis would give me more incentive to try Koffice. I want something lighter than OOo.org, but I actually think this fileformat will (and has to) become very popular and standardized. If as well Koffice as Abiword as OpenOffice as... use this, this will be a real strength, not to mention becomeing easier to share documents."
    author: "Darkelve"
  - subject: "Re: .swx-compatibility?"
    date: 2004-11-24
    body: "We'll have to wait for KOffice 1.4 to have it using the OASIS file format. I hope we'll see the release next year...\n"
    author: "Birdy"
  - subject: "Re: .swx-compatibility?"
    date: 2004-11-24
    body: "KWord can already read and write OpenOffice 1.x documents. (However please do not await wonders...)\n\nHowever it is not directly this format that is being standardized. The OASIS openffice format is based on this format but differs in details, so that both formats are not compatible. That is why there are new file name extensions (for example .oot for the wordprocesor format).\n\nAnd OASIS will only be available from KOffice 1.4 on (current KOffice CVS HEAD.) However there is still much work to do (and as always: volunteers welcomed!)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "KOffice my favourite "
    date: 2004-11-23
    body: "I like the KOffice suite a lot. It is an amazing bundle of good code and still very promising but lacking developers somewhat.\n\nEverybody moans about .doc format oddities. I wonder however, why Rich Text Format does not work well with OSS word processors. It is a well documented format, isn't it? It is well supported by M$ own word processors, right?\n\nOne more question: how many man-hours do you estimate a M$ Word import filter would need to reach 5 star status ***** (port of the Oo.o filter or an evolution of the existing import filter) given the OASIS format already works?\nI am pondering over fund-raising.\n\nKeep up the good work!\n\nMark"
    author: "kmsz"
  - subject: "Re: KOffice my favourite "
    date: 2004-11-24
    body: "Guess what? \n\nA standard rtf does not allways work with MSWord or OO.org. You need to test every small change with them if you want rtf filter to be reliable.\n\nThat being said, rtf import and export (the basics) works ok.\n\nAbout .doc filters, both ways it would take _a lot_ time. It's possible, though, that in OO.org 2.0 the filter code would be easier to use. At the moment the port would be very hard.\n\nEleknader\n\n"
    author: "eleknader"
  - subject: "Re: KOffice my favourite "
    date: 2004-11-24
    body: "Yes, this is a big problem for the RTF export filter. If you test the filter only with OpenOffice, it might not work on MS Word.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Good work :)"
    date: 2004-11-24
    body: "Thanks for really nice office package."
    author: "m."
  - subject: "pdf exports"
    date: 2004-11-24
    body: "Has the quality of PDF exports changed since the \nlast release?  The pdf file exported from kspread \nwere of inferior quality both for view in acrobat \nand in print.  In comparison the pdf export files \nfrom gnumeric are exceptionally good.  \n\nThanks for the great work.  \n\nAsk"
    author: "Ask"
  - subject: "Re: pdf exports"
    date: 2004-11-24
    body: "The problem is probably unchanged.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: pdf exports"
    date: 2004-11-24
    body: "I can't get it to use the correct fonts when I print, so there is no way it is going to get them correct for a PDF.\n\nI tried a test sheet using Times & Helvetica (which are the fonts needed for a good PDF).\n\nThe PS file has: TimesNewRomanPSMT & Helvetica-Condensed-Light\n\nThis is seriously broken.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Templates"
    date: 2004-11-24
    body: "One area with a high potential impact/work ratio for KOffice would be better templates. For instance, the default template in KWord, is not too pleasing for the eye. \n\nI have tried importing blank files/templates from other word processors (including taking a normal.dot file from MS Word, opening and saving it in OO Writer and then opening it in KWord) but with little success.\n\nI also attach a template for KWord, where I have made the following changes to the default template;\n\n1) After each headline, the default next style is something called standard-first, which is just the old standard style (not indented).\n2) After a standard-first paragraph follows a standard-next paragraph with the first line indented. Then another standard-next and so on.\n3) The header styles are changed to a font that does not have to be made bold. This means the numbering in headers will be the same style as the headers themselves, which looks much better IMHO (even though the actual font is perhaps not ideal).\n\nThis is based on the Swedish standard template, so all styles have Swedish names (\"standard-first\" is really \"Standard-f\u00f6rsta\" etc.). But you'll get the idea... I tried renaming everything, but that triggered some bugs, its seems."
    author: "Martin"
  - subject: "Re: Templates"
    date: 2004-11-25
    body: "If you want to use a template from MS Word, I suggest that you save it as RTF in MS Word. It should be better then.\n\nAs for better templates, as long as the conversion to OASIS is not done, there is little use of doing better templates. (I do not know if the template dialog could already process OASIS files or not.)\n\nAs for the attached template, the points 1 to 3 are perhaps what is wanted for a Swedish user but that is not the normal behaviour... which brings us to the problem how to localize templates... which is another problem (and already a bug in KDE Bugs).\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Templates"
    date: 2004-11-25
    body: "I'm sorry if this is the wrong forum for this... But anyway:\n\n1. Word import\n\nUnfortunately, the RTF import did not work for me. I tried many different ways of getting several versions of Normal.dot into KWord. None worked for me. Someone else might have better luck, of course. I suspect it heavily depends on what goes on in the MS Word side of the equation. Other MS templates do work, however, see my post here: \n\nhttp://www.kde-forum.org/viewtopic.php?t=3726\n\n2. Paragraph styles\n\nWhile different locales will have different preferences as to how the first and subsequent paragraphs in a section should be formatted, using indentation or whitespace, something like the behaviour in the attached template will still be needed. Using the standard template I have to hit Enter twice (typewriter-style) to differentiate between a soft line break and a paragraph break.\n\n3. Header styles\n\nAFAIK the rule in other word processors is to use the same font for the numbers and the actual text in numbered headers. Don't you agree that mixing a font with its bold version looks strange?"
    author: "Martin"
  - subject: "Re: Templates"
    date: 2004-11-26
    body: "1.:\n\nToo bad. Has KWord refused to load it or was it just that the result was not what you had expected?\n\n2.:\n\nYes but again, having an empty line between paragraphs is also dependant of the use (even if I understand what you mean as in Germany too paragraphs in a letter should have an empty line between them).\n\nPersonally, I would very much like something like a wizard, but I have still not any idea how to implement it correctly. (And users might not like it much, as they will probably use only one or two variants as templates.)\n\n3:\n\nI am surprised that you tell that you have a different font between numbers and the text in headers. That is a problem that I have never seen.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Templates"
    date: 2004-11-27
    body: "1. It loads, but none of the actual style information (which is what I'm after) is imported.\n\n3. In Kword 1.3.4, create an empty document. Select the style \"Head 1\". Type in the text \"1.\". The automatically generated \"1.\" is not in bold style, but the one you typed is.\n\nYou too have a nice day!"
    author: "Martin"
  - subject: "Re: Templates"
    date: 2004-11-27
    body: "Ah, yes, now I see the bold problem too.\n\nCan you create a bug report in KDE Bugs, please?"
    author: "Nicolas Goutte"
  - subject: "Re: Templates"
    date: 2004-12-28
    body: "It is bug #95953 now: http://bugs.kde.org/show_bug.cgi?id=95953"
    author: "Nicolas Goutte"
  - subject: ".doc write compatibility"
    date: 2004-11-30
    body: "While I abhor the business ethics of Microsoft, and would much prefer to use non-proprietary software formats, I find that, specifically as a college student, my documents are required more and more to be shared in electronic format with Windows users, many of whom are unsavvy and prefer to use their packaged word processor. \n\nErgo, the ability to save documents in the MS Word .doc format is a very nice feature, as well as the respective .ppt and  .xsl file formats.\n\nYes, we do need to look to the future of producing new file formats. However, these documents are produced in the first place to be read by an audience. With Microsoft's commanding lead in desktop marketshare, and thus in audience, I feel that it is only prudent for KOffice to place developers in the production of an export filter for the .doc format, whcih is currently non-existant.\n\nKris Kerwin\nkkerwin@insi__REMOVE_ME__ghtbb.com"
    author: "Kris Kerwin"
  - subject: "Re: .doc write compatibility"
    date: 2004-12-01
    body: "No, it's highly debatable whether it's \"prudent\" for KOffice (what is that? that's an application suite which can't act on its own!) to \"place developers in the production\" (that reads like someone ought to force someone else to do that, good luck trying that in this community which is still heavily based on volutariness) of an export filter for the .doc format.\n\nI'd suggest you to pay a developer for this purpose, you apparently know that it's worth it after all."
    author: "ac"
  - subject: "Re: .doc write compatibility"
    date: 2004-12-01
    body: "I'm beginning to find it quite difficult to post an opinion in online forums pertaining to UNIX workalikes. I have to try very hard to watch what I say -- to not to start a flame war with a single thread. I am at the point that I am nearly discouraged from commenting in the first place. \n\nIn our \"community which is still heavily based on voluntariness\" to survive, we do not only volunteer our time and effort; which I am unable to provide as I lack the skills to apply to a project, the time in order to obtain those skills, and the money to fund the further development. No -- we also volunteer ideas. \n\nI believe, as an outsider looking in upon the world of the developer, that that is much of what coding is: forming ideas. However, I do not possess the ability to express these ideas in terms appreciable by a compiler -- only what may be understood by a person.\n\nI feel that it is counterintuitive for a community that's greatest commodity is imagination to stunt the spread of information. I feel that comments like the one above not only debate the views of another -- which is fine, even welcomed in an intellectual community such as this -- but also contribute to in effect discouraging comments in the first place. Speak to newbies -- there is a sense of fear in many, including myself. This is as dangerous as the propriety that we seek to fight.\n\nTo perhaps justify my motivations for posting the grandparent comment, I present the following. Curious as to why no work had yet started on this feature, I checked at www.koffice.org. I felt that with a file-format so widely used outside of the UNIX world, surely some discussion had begun about the possibility of beginning work on one. I found no discussion. I searched the wish-lists at bugs.kde.org, and still found nothing. As far as was evident to me, no discussion had yet begun, the feature was not being considered as a possiblity. The purpose of my comment was not to demand a feature as an angry end-user, insensitive to the needs of the developer; the purpose of my comment was to hopefully begin such a discussion, with the end product being the suggested feature.\n\nI apologize if I offended some; that was not my goal. My intentions were only to make a suggestion, to begin a discussion, and receive input. It appears to me, however, that I made a mistake in picking my forum, though I felt that the topic did contain some relevance as others were speaking of support for other file-formats.\n\nPlease, let's continue.\n\nKris Kerwin\nkkerwin@insi__REMOVE_ME__ghtbb.com"
    author: "Kris Kerwin"
  - subject: "Re: .doc write compatibility"
    date: 2004-12-01
    body: "Asking for .doc format compatibility however is in no way a creative idea. Actually reverse enginering and ensuring compatibility with a broken illogical format is about the most boring and repetitive job one can do, unless one is motivated by such a Herculean challenge by the sake of it, someone whom you can doubt to find at places like here where everyone either contributes already or prefers not to contribute at all. It's in my (and I'm sure others') interest to discourage such requests like yours unless they are backed in some substantial way (you could easily pledge money, offer to collect and maintain a collection of useful reverse enginered information about the .doc format etc. but you did none of that to begin with and such have no idea about the effort involved at all). If you want to encourage development ask Eric Laffoon how he got Quanta to where it is today.\n\nAnd no, you offended nobody, posts like yours usually just get ignored for the above reasons. I'm just trying to do you a service by pointing out this fact and saving you the time to write more such posts if you don't intend to back your own suggestions. Writing text on some random forum and doing nothing else but still expection something to change is very cheap. Heck, you didn't even react to my suggestion to just pay a developer to create what is worth being created in your opinion."
    author: "ac"
  - subject: "Re: .doc write compatibility"
    date: 2004-12-02
    body: "I do think that there were discussions about this subject. (I mean: even beyond the classical \"Why do you not use OO's filter code?\")\n\nSeeing how difficult it is to make a stable RTF export filter that work with most word processor applications, I personally think that doing a correct MS Word export would be even more difficult. (With also probably the problem to know which (sub-)version to support.)\n\nSo personally, I would prefer that we get enough effort to make a correct RTF export filter first. But even that is difficult, because those knowing how to develop have either no time or no copy of MS Word to test the result... or both problems.\n\nHave a nice day!"
    author: "Nicolas Goutte"
---
The KOffice team is happy to bring you the fifth bugfix release of <a href="http://www.koffice.org/">KOffice</a>. KOffice 1.3.5 features a new language (Breton), the PDF import filter includes a stronger security fix and there are also a few fixes for the OpenOffice.org Impress Export Filter.
See <a href="http://kde.org/areas/koffice/releases/1.3.5-release.php">the release notes</a> and <a href="http://kde.org/areas/koffice/announcements/changelog-1.3.5.php">the list of changes</a> for full details.




<!--break-->
