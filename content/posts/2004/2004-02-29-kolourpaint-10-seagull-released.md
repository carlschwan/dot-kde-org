---
title: "KolourPaint 1.0 \"Seagull\" Released"
date:    2004-02-29
authors:
  - "cdang"
slug:    kolourpaint-10-seagull-released
comments:
  - subject: "I like it"
    date: 2004-02-29
    body: "I'm no artist, but I really like this program, being able to zoom to 1600% and edit everything.\n\nIt's much much ebtter than Paint :p"
    author: "Alex"
  - subject: "Re: I like it"
    date: 2004-02-29
    body: ">being able to zoom to 1600% and edit everything.\n\n...and scrolling a bit and the whole image flickers like hell...\n\nI really would like to say: \"Wow - great application\", but right now it simply sucks as much as ms paint.\n\n"
    author: "Carlo"
  - subject: "Re: I like it"
    date: 2004-02-29
    body: "Hey, Gimp2 pre3 can zoom up to 25600% ;-) thats cool."
    author: "MaX"
  - subject: "much better than kpaint"
    date: 2004-02-29
    body: "but the icons are horrible :)"
    author: "anon"
  - subject: "Re: much better than kpaint"
    date: 2004-02-29
    body: "That should be very easy to fix now.  :)"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: much better than kpaint"
    date: 2004-02-29
    body: "As far as I'm concerned, Clarence is free to bag the tool icons we have for Krita -- those are very nice."
    author: "Boudewijn Rempt"
  - subject: "What's next?"
    date: 2004-02-29
    body: "Nice start, what do you plan to start to implement next? My wishes: Free definable sizes for line thickness/spraycan/color eraser, line styles (dashed, dotted, ...), fill patterns, chooseable brushes, rulers - with this implemented both KPaint and KIconEdit could be dropped."
    author: "Anonymous"
  - subject: "Re: What's next?"
    date: 2004-02-29
    body: "\"KPaint and KIconEdit could be dropped\"\n\nplease :) [as those apps are very unprofessional]\n\n\nAn good lightweight image/icon editor is exactly what KDE needs right now."
    author: "cies"
  - subject: "Re: What's next?"
    date: 2004-03-02
    body: "Kick them."
    author: "Gerd"
  - subject: "Bravo !"
    date: 2004-02-29
    body: "finally, someone has managed to create a real graphics editing/drawing program for kde (and NOT just another of the zillion image viewers)!\nThis is by far the best kde-graphics app I know of.\nat this stage, I'm already able to do most of my graphics editing for the web\nwith kolourpaint, thus reducing the need for gimp substantially (hooray ;-)"
    author: "hoernerfranz"
  - subject: "Cool"
    date: 2004-02-29
    body: "I really like it.\nFinally a decent KDE painting app.\nMy wish for the next release:\nA \"Hand\" tool which activates by keeping the \"Space\" key pressed\ndown to scroll the visible part of the image around just like\nin Acrobat or Photoshop.\n\nAnd a question:\nWhat is the easiest way to activate GIF support?\nI'm using SuSE RPM's. I hope I do not have to recompile\nmy whole KDE/QT stuff. That would be awful!\nIf that's the case then also on my wishlist for the next version:\nAutomatic command line calling of ImageMagick to convert from png to gif \nso the user doesn't even notice there is no GIF support compiled in.\nNo ImageMagick-dependency is needed - just call \"convert\" via exec.\n\nI know that most people hate GIF but as a web developer PNGs aren't a\nsolution yet because of their awful buggy support in IE. "
    author: "Mike"
  - subject: "Re: Cool"
    date: 2004-03-02
    body: "<i>I know that most people hate GIF but as a web developer PNGs aren't a\n solution yet because of their awful buggy support in IE.</i>\n\nI thought it only had a problem with gradient transparency.\n\n"
    author: "Kevin Krammer"
  - subject: "MS Paint?"
    date: 2004-02-29
    body: "Looks almost identical to MS Paint...\nCan ypu delect an area bigger than the screen in this one?"
    author: "OI"
  - subject: "Re: MS Paint?"
    date: 2004-02-29
    body: "why developing an ms paint equivalent ? \n\nHave you tried to copy the suckiness of ms paint ? did you suceed ?\n\n\nYou should rename it as \"Kids Paint\", or we will be flooded with \"well drawn\" Wallpapers soon.\n\nchris"
    author: "chris"
  - subject: "Re: MS Paint?"
    date: 2004-02-29
    body: "Shut up, Paint is awesome.  Sure, you can't really paint with it.  But if you want to crop a screenshot, or add some text to an image, or add an arrow, or rotate, or scale, or convert color depths, or change file formats, Paint does it faster and easier than most graphics programs, and its always around.  A good Paint clone for Linux is something I've wanted for a long time, it will fill a lot of needs."
    author: "Spy Hunter"
  - subject: "Re: MS Paint?"
    date: 2004-02-29
    body: "Full Ack.\nImo, MS Paint ist (besides AoE ;)) the best MS Program ever. (Missed it, since I converted to Linux, some years ago)"
    author: "Beefy"
  - subject: "Re: MS Paint?"
    date: 2006-01-16
    body: "Could you guys let me know where I can download this program you guys are talking about? Sounds great, thanks!"
    author: "mturco11x"
  - subject: "Re: MS Paint?"
    date: 2006-04-20
    body: "..."
    author: "mturco11x"
  - subject: "Re: MS Paint?"
    date: 2006-07-30
    body: "I was wondering where to download MS Paint too."
    author: "wantingtodownloadMSPAINT"
  - subject: "Re: MS Paint?"
    date: 2006-10-15
    body: "http://dot.kde.org/1078028599"
    author: "Tuna"
  - subject: "hmmm"
    date: 2004-02-29
    body: "kolourpaint looks somewhat familiar...\n\nCongratulations on your 1.0 release!  It's great to see OSS reach those milestones.  I'm compiling my copy as I am typing this.\n\n//standsolid//"
    author: "standsolid"
  - subject: "Replace KPaint?"
    date: 2004-02-29
    body: "I hope KolourPaint will replace KPaint in KDE 3.3, it seems to be far better."
    author: "Steffen"
  - subject: "Translations & docs ?"
    date: 2004-02-29
    body: "No pot file ? (The source seems internationalised).\nNo doc ?\n=>Still not a real KDE app !\n\nIt works fine on my CVS KDE, but looks more like a paintbrush than like a Gimp.\n\nGood job !\nGerard"
    author: "Gerard"
  - subject: "Re: Translations & docs ?"
    date: 2004-02-29
    body: "A pot file is available at kde-i18n/templates/kdenonbeta/kolourpaint.pot in cvs."
    author: "Tijmen"
  - subject: "Re: Translations & docs ?"
    date: 2004-02-29
    body: "Which was not updated for a month."
    author: "Anonymous"
  - subject: "Re: Translations & docs ?"
    date: 2004-02-29
    body: "> but looks more like a paintbrush than like a Gimp.\n\nThat is the purpose of this app, read the \"What is...\" section here: http://kolourpaint.sourceforge.net/about.html"
    author: "cies"
  - subject: "Re: Translations & docs ?"
    date: 2004-02-29
    body: "> It works fine on my CVS KDE, but looks more like a paintbrush than like a Gimp.\n\nYup, that's what it's meant to be like. Krita is more GIMP-like in both design and usage. "
    author: "anon"
  - subject: "good stuff!"
    date: 2004-02-29
    body: "Dang!  Thanks, Clarence!  Let's hope it replaces KPaint."
    author: "ac"
  - subject: "Thank Goodness"
    date: 2004-02-29
    body: "I'm hesitant to admit this, but I've been looking for something that is as simple to use as mspaint for a long time.  I can't wait to give this a shot.\n\n\n-Karl\n"
    author: "Karl Garrison"
  - subject: "sorry but..."
    date: 2004-02-29
    body: ".. it's ugly   :(((((("
    author: "Francisco"
  - subject: "Re: sorry but..."
    date: 2004-02-29
    body: "And your point is?\nIt's the functionality that's important, looks can come later..."
    author: "Peter Simonsson"
  - subject: "Re: sorry but..."
    date: 2004-02-29
    body: "i would have thought would have been released before a 1.0 release though :("
    author: "anon"
  - subject: "Paint program - Mspaint recreated"
    date: 2004-03-01
    body: "Great paint program! I knew you could copy mspaint! Congratulations - I have never seen anyone's attempt to recreate the wheel work as well!"
    author: "Pingu Fisher"
  - subject: "Needed features"
    date: 2004-03-02
    body: "I am not a freehand artist.\n\nTherefore, the main additional feature which I need is to be able to edit the image.  Specifically, you need to be able to select a single object and move each point which defines it with a spin box for x & y.  See: Xfig.  Translation and rotation would also be nice.  Also the line width needs to be editable on objects after you have drawn them.\n\nLooks very nice so far.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "KolourPaint for KDE 3.0 and KDE 3.1"
    date: 2004-03-06
    body: "As seen on http://kolourpaint.sourceforge.net, KolourPaint has been backported to KDE 3.0 & 3.1."
    author: "Anonymous"
---
After 8 months of development, <a href="http://kolourpaint.sourceforge.net/">KolourPaint 1.0 "Seagull"</a> is available for download. KolourPaint is an easy-to-use paint program for KDE that makes user-friendly painting and image editing a reality for the desktop user.
<!--break-->
<p>If you're sick of those broken KDE paint programs that can't undo or handle images the size of a screenshot, then KolourPaint is for you.  If you're still trying to figure out how to draw lines in a monolithic and unusable graphics app, then why not try out KolourPaint?  KolourPaint sports a GUI designed specifically for daily tasks like editing screenshots and drawing diagrams and icons.</p>

<p>Features include undo/redo, more than a dozen tools, selections, transparent
image editing and zoom support (with an optional grid and thumbnail).</p>

<p><a href="http://kolourpaint.sourceforge.net/screenshots.html">Screenshots</a>, <a href="http://kolourpaint.sourceforge.net/download.html">downloads</a> and <a href="http://kolourpaint.sourceforge.net/about.html">more information</a> are available at
http://kolourpaint.sf.net.</p>




