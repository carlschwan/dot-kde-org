---
title: "Onebase Project Releases \"KDE-3.3 Beta 1\" LiveCD"
date:    2004-07-14
authors:
  - "pbaskaran"
slug:    onebase-project-releases-kde-33-beta-1-livecd
comments:
  - subject: "track usage ?"
    date: 2004-07-14
    body: "could we somehow track the usage of the cds for bug reporting ?"
    author: "chris"
  - subject: "Great!"
    date: 2004-07-14
    body: "Also nice for checking if some reported bugs were fixed meanwhile, and help clean the bug database."
    author: "John BugReporting Freak"
  - subject: "usage tracking"
    date: 2004-07-14
    body: "It would be interesting to distribute a live CD like this with the UsageMonitor [1] that sends reports to some central place.  I wonder what app people try first? Last?  How long they use it for?\n\n[1] http://www.kdedevelopers.org/node/view/376"
    author: "mbucc"
  - subject: "Re: usage tracking"
    date: 2004-07-14
    body: "It would be great. BTW I would like to let you know \nthat customizing the Onebase LiveCD is very easy. \n\nIn three steps have Usage monitor in the CD.\n(with this remastering).\n\n[1] click the \"Dock\" icon in the desktop\n[2] Update gallery (ol-apps -u) and Create a .olm for your application (reference: \nhttp://www.ibiblio.org/onebase/onebaselinux.org/Participate/Devdocs/index.html\nand copy the .olm to the approtiate category folder in /mnt/dock/olgo/ONEBASE/etc/OL/gallery/\nand install it using command: olm-go -b packagename\n[3] Click the \"un-dock\" icon in the desktop to auto generate \na new ISO.\n\nOfcourse this includes an additional step of creating \na .olm as it is yet to be in the gallery.\n\nSample .olm link:\nhttp://www.ibiblio.org/onebase/onebaselinux.org/Media/kdeaddons.olm"
    author: "all4one"
  - subject: "This is what is required for better KDE!!!"
    date: 2004-07-14
    body: "The problem with a normal user is that s/he can't compile KDE betas (due to time constraint, and not all KDE users have Fast computers or the patience to see all those packages getting compiled). Hence s/he is unable to report any bugs. This LiveCDs or Precompiled Binary packages will improve KDE bug reports which leads to bug fixing which finally leads to better KDE. \n\nI wish the 'Best of Luck' for the endeavor. Hey KDE Developers, Why don't you put binary packages which you have compiled for your distro at ftp.kde.org?"
    author: "Fast_Rizwaan"
  - subject: "Re: This is what is required for better KDE!!!"
    date: 2004-07-14
    body: "Onebase Linux already has source and binary package\ninstallation support for KDE 3.3B. And I notified this to the\nWebmaster, but it is yet to make it to their package list.\n\nProbably I did not inform them in the right way, dont know.\nOr they may be busy."
    author: "all4one"
  - subject: "Sources ? "
    date: 2004-07-14
    body: "Where i can find the source sources of this LiveCD ? "
    author: "RMS"
  - subject: "Re: Sources ? "
    date: 2004-07-14
    body: "It uses the original KDE sources. \n\nThe package management system in Onebase is OLM. And it (OLM) \nuses simple scripts called .olms to install software.\n\nThe tarballed OL-apps gallery is found at this link\nhttp://www.ibiblio.org/onebase/onebaselinux.org/OL-apps/gallery.tar.bz2\n\nAnd you find more about OLM from here:\nhttp://www.ibiblio.org/onebase/onebaselinux.org/Support/olm-guide.html\nand\nhttp://www.ibiblio.org/onebase/onebaselinux.org/About/featol.html"
    author: "all4one"
  - subject: "KDE Applications are UNABLE to handle LARGE FILES"
    date: 2004-07-14
    body: "Excuse me for this OFFTOPIC post. But I think this bug report is rightly important for KDE 3.3...\n\nhttp://bugs.kde.org/show_bug.cgi?id=85178\n\nCONTENT of the Bug Report:\n\nI have noticed that KDE and GNOME and other (Mozilla) are unable to handle large files effeciently without losing responsiveness.\n\nLike KWord can't handle large Documents whose pages are in 100's. Konqueror can't load a >3MB file quickly. KWrite, Kate, Kedit are also getting unresponsive for atleast 10-15 seconds to load a mere greater than 3 MB file.\n\nSurprisingly, I have found that Elvis and Emacs and Links (not lynks) browser are very efficient in handling large files.\n\nPlease Observe:\nElvis, Emacs, and Links browser takes 2-3 seconds to load a >3mb file. The processor utilization goes to 100% for just 1-2 seconds here.\n\nwhereas any KDE application, Mozilla 1.7, firefox 0.9, Netscape 6.1, gedit, galeon etc. takes more than *15* seconds and still remain unresponsive. upto 15 seconds the processor utilization goes to 100% (used ksim). And As long as the BIG file is loaded, the application which is loaded with the Big file remain somewhat sluggish, like scrolling becomes slow or takes 1-2 seconds.\n\n\nExpected behavior of KDE Applications:\n1. Like 'elvis' text editor KDE application MUST use temporary files for loading any file, small or big. Here is elvis about:\n\n-------------------------------------------------------\n\"1. WHAT IS ELVIS-2.2_0\nElvis is a clone of vi/ex, the standard UNIX editor. Elvis supports nearly all of the vi/ex commands, in both visual mode and ex mode. Elvis adds support for multiple files, multiple windows, a variety of display modes, on-line help, and other miscellaneous extensions.\n\nLike vi/ex, Elvis stores most of the text in a temporary file, instead of RAM. This allows it to edit files that are too large to fit in a single process' data space.  Also, the edit buffer can survive a power failure or crash. \"\n-------------------------------------------------------\nplease note:\n\"Elvis stores most of the text in a temporary file, instead of RAM. This allows it to edit files that are too large to fit in a single process' data space.\"\n\nThis is what I feel KDE lacks... KDE Applications are sluggish with medium to large (1mb-10mb) files due to loading the file into ram only (as I assume, I don't have proof, because I am not a developer :))\n\nPlease Follow 'Elvis' way of handling files, which is quite efficient. Or Emacs also loads huge files as quickly as in one to two seconds.\n\nAmazingly a well formatted 5 MB English-To-Hindi HTML files gets loaded in \"LINKS\" (not \"lynx\") browser in just 2-3 seconds whereas all other browsers take around 15-30 seconds or just get hung.\n\nI humbly request you superb developers to kindly look into the 'Efficient loading of files' by elvis, emacs, and 'links' browser. and 'Inefficient loading of files by KDE applications.'\n\nThanks."
    author: "Smith"
  - subject: "Re: KDE Applications are UNABLE to handle LARGE FILES"
    date: 2004-07-15
    body: "someone in #kde-devel mentioned that Kate was recently enhanced to support larger files. Apparently getline() isn't what it's cracked up to be. You should try the beta and see if you notice a speed up."
    author: "Ian Monroe"
  - subject: "Re: KDE Applications are UNABLE to handle LARGE FILES"
    date: 2004-07-16
    body: "I just tried it and the loading speed is much better - I was working with some large files a few days ago and they took 5-10 seconds to load, but now the delay is barely noticeable."
    author: "Richard Garand"
  - subject: "password?"
    date: 2004-07-14
    body: "Well, I booted it under QEMU and it asks for a password to let you enter X?\nI can't find it at the link:\nhttp://www.ibiblio.org/onebase/onebaselinux.org/Products/download.html\n\n???\n\nwithout password i can't login to test... empty password and root username don't work."
    author: "emmanuel"
  - subject: "Re: password?"
    date: 2004-07-14
    body: "The password is mentioned in the support and features page of\nOnebaseGo in its Website. (onebaselinux.org)\n\nUsername: root\nPassword: one\n\nNote: There is an errata, the volume in kmix is turned to\nmute by default. You need to increase it."
    author: "all4one"
  - subject: "This download is super slow!"
    date: 2004-07-15
    body: "This is superslow, when I have the file, I will post a torrent here. "
    author: "Magnus Lundstedt"
  - subject: "Here is the torrent!"
    date: 2004-07-15
    body: "Here is the torrent of this liveCD: \n\nhttp://magnus.infidyne.com/torrent/OnebaseGo-2.0-KDE3.3b.iso.torrent\n\nI am seeding it @ max 10 mbit for now. "
    author: "Magnus Lundstedt"
  - subject: "good but...."
    date: 2004-07-15
    body: "...it's a 600mb image!\n\nIf the developers find this is a good idea for getting more people to beta-test, then someone could make a \"tighter\" betaKDEliveCD with _only_ the software to test and the required dependencies, that would be 200-300mb no?, an easier size for downloading. And if someone does that distribute it on BitTorrent from the first moment."
    author: "me"
  - subject: "Re: good but...."
    date: 2004-07-15
    body: "its indeed big. but I am impressed with onebase's features... if it wasnt the fact I just installed debian, I'd go for it ;-)\n\nand at least I will try the docking feature... thats really cool!"
    author: "superstoned"
  - subject: "Re: good but...."
    date: 2004-07-15
    body: "My complete /opt/kde-3.3 installation (with one additional language, KOffice and some apps like k3b) is almost 600MB too. Of course this is uncompressed but then the CD has to contain other stuff like Linux, GNU and X-Server too."
    author: "Anonymous"
  - subject: "Re: good but...."
    date: 2004-07-16
    body: "I would really love to see a \"kde.org LiveCD\"...\n\nJust stuff it with all kde's dependencies and release a mini CD always with a public kde release?!?\n\nNowdays there are sooooo many simple scripts that could do the building, why not start to use one of them?\n\nIt's silly to hunt for a different LiveCD at each kde release..."
    author: "Nobody"
  - subject: "Re: good but...."
    date: 2004-07-16
    body: "Yes. In my opinion a much smaller live CD would better for testing KDE Beta version or Release Candidates.\n\nMaybe a SLAX based Live CD would be the right way:\n\nhttp://slax.linux-live.org/\n\nAt the moment this SLAX CD uses the latest stable KDE and is a relatively small but fine Live CD."
    author: "Bend Lachner"
  - subject: "Re: good but...."
    date: 2004-07-16
    body: "SLAX does not contain all KDE modules (no kdeaddons, kdeedu, quanta, kdevelop, ...)."
    author: "Anonymous"
  - subject: "Static KDE faster or not?"
    date: 2004-07-15
    body: "I read that any statically compiled application (./configure --enable-static <appname>) application is faster than normal (shared library) application.\n\nHas anybody tried a fully static KDE? Is it faster?"
    author: "Fast_Rizwaan"
  - subject: "Re: Static KDE faster or not?"
    date: 2004-07-15
    body: "If it is only <i>one</i> KDE application you are running, it might be faster (if it was possible). But multiple KDE-applications share their libraries and would use much more memory if you statically linked them all."
    author: "Allan S."
  - subject: "Re: Static KDE faster or not?"
    date: 2004-07-15
    body: "You can't build KDE that way"
    author: "Sad Eagle"
  - subject: "crash"
    date: 2004-07-15
    body: "In webbrowsing mode konqueror is crashing when I try to open a new tab CTRL-SHIFT-T.\n\nStrangely, in filemanager mode this crash does not happen.\n\nApart of that, the flicker fixes seem to make konq just better and look\nmore polished professionnal.\n\nKeep up the good work, I can't wait for the the release of 3.3!"
    author: "ac"
  - subject: "Re: crash"
    date: 2004-07-15
    body: "Maybe I should add that these problems occur with kde 3.3b1 from the onebase live-cd... :-)"
    author: "ac"
  - subject: "Report bugs? No debug info"
    date: 2004-07-16
    body: "Hi, I've tried this version, and just playing for some time, I've been able to crash two apps (one is Kwrite, the other I don't remember).\nI'm a newbie, but what surprised me was that the error box appeared (seg_fault?) telling that \"back tracing\" was not possible because of the gdb (or something like it) missing.\nI'm wondering why for a distro that has the porpouse of finding bugs, there is not an automatic notify mecanism like the one Mozilla has (when Mozilla crashes, the stack and other important, for the programmer, information are sent to the Mozilla site for furhter investigation).\nIn addition, since when you get an app crash you are not able to remember exactly the 14 steps that drove you to that crash, would be great having a sort of \"activity recorder\" turned optionally on, that can log what you did and could \"play\" the sequence again, step by step.\nJust some thoughts...\nregards\nMarco Menardi"
    author: "Marco Menardi"
  - subject: "Re: Report bugs? No debug info"
    date: 2004-08-03
    body: "hem onebase is not a bug-finding distro, i'm using it all day long actually"
    author: "bacon"
---
The <a href="http://www.onebaselinux.org/">Onebase Linux Project</a> has <a href="http://www.ibiblio.org/onebase/onebaselinux.org/Community/phpBB2/viewtopic.php?t=1032">released a special flavor</a> of its 
OnebaseGo-2.0 edition, which includes the complete KDE 3.3 Beta 1
"<a href="http://www.kde.org/announcements/announce-3.3beta1.php">Klassroom</a>" suite and <a href="http://www.koffice.org/releases/1.3.2-release.php">KOffice 1.3.2</a>. The main purpose of this
flavor (LiveCD) is to try, test and report bugs on this beta version.
And also to provide a technology preview for KDE users.









<!--break-->
<p>The Onebase Linux Project is a community-driven Free and Opensource undertaking, with the objective of providing a complete, multi-purpose operating system based on the Linux kernel. OnebaseGo is a portable OS (LiveCD) that comes in a CD with all the features of Onebase Linux. It comes with state-of-the-art remastering called Docking, eXtended package store and HD-installer.</p>







