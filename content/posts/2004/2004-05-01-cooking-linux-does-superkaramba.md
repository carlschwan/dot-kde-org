---
title: "Cooking with Linux does SuperKaramba"
date:    2004-05-01
authors:
  - "fmous"
slug:    cooking-linux-does-superkaramba
comments:
  - subject: "red"
    date: 2004-05-01
    body: "About a year or so ago, (super)karamba was all the rage.  There were many efforts on kde-look.org to create cool karamba apps.  Now that has all seem to pass like an old fad.\n\nAre people just realizing that KDE already does what they want, or they don't want to use up cpu for eye candy, or are people actually using superkaramba?\n\nI'm not trying to offend anyone (everyone is free to scratch their own itch), just passing along my thoughts (I was actually reminded of this earlier today as there was an article on osnews about windows users being able to emulate the look of apple on their computers.  These programs seemed to have progressed alot while thier superkarmaba counterparts seem to have stagnated)."
    author: "What happened to superkaramba?"
  - subject: "Re: red"
    date: 2004-05-01
    body: "I use it a ton.  I have lots of pretty displays showing cpu usage, etc.  I rarely look at those.  Sometimes when my machine is doing something, I glance at it for traffic, cpu, memory usage, etc.  But rarely.\n\nWhat I use it for a ton is my mail-list.  I split my incoming mail via procmail into imap boxes.  I then have a super-karamba backdrop that displays mail (from, subject, folder, date all on one line...) in my mail boxes sorted by mail-box priority.  I look at it constantly.  Much better than constantly opening my mail reader just to find out its mostly stuff that isn't immediately important.  super-karamba has been a life saver for this.  (A long time ago, I used to use xlbiff and after that xfaces.  But my current  setup is my current favorite.  No, none of the rest of the k*biffs do what I want)."
    author: "Wes Hardaker"
  - subject: "Re: red"
    date: 2004-05-01
    body: "What theme are using to watch your mailboxes?  Does it actually connect using imap?"
    author: "am"
  - subject: "Re: red"
    date: 2004-05-01
    body: "Sometimes you just need special stuff that KDE cannot or should not provide, for instance a e-mail monitor. KDE default is overloaded with features."
    author: "Gerd Br\u00fcckner"
  - subject: "Re: red"
    date: 2004-05-01
    body: "Yes I can't imagine why we would need all that stuff ... Stupid and bloated I say. \n\n\nFab"
    author: "Fabrice Mous"
  - subject: "Re: What happened to superkaramba?"
    date: 2004-05-02
    body: "I use it...I have Liquid Weather running on one side of my desktop and on the other side of my desktop, I have a fortune applet that refreshes every 60 seconds.\n\nI'd probably have more applets running if I didn't use GKrellM (I'm normally a KDE diehard, but I like GKrellM's compact interface and the fact that it has almost everything-under-the-sun)."
    author: "QV"
  - subject: "Re: red"
    date: 2004-05-02
    body: "I have a simple transparent system monitor ticking away via Superkaramba on all desktops and find it very useful (and unobstructive)."
    author: "Olav P"
  - subject: "Re: red"
    date: 2007-02-03
    body: "perhaps the reason development SEEMS to have stalled is that the most commonly used superkaramba desktop widgets have already been implemented."
    author: "siauderman"
  - subject: "Cool Stuff"
    date: 2004-05-02
    body: "Karamba and SuperKaramba are very useful, but I what I think would be really useful is to try and integrate them more in vanilla KDE and for applications to really use them as part of the desktop. Microsoft are using their sidebar thing and there is the Dashboard thing for Gnome, and this could be a great way for having desktop notifications and other information happen in an unobtrusive way. At the moment I think Dashboard consists of too much hype, but KDE really has the messaging infrastructure in DCOP to make something like this happen in a big way.\n\nWhat exactly is the difference between Karamba and SuperKaramba?"
    author: "David"
  - subject: "Re: Cool Stuff"
    date: 2004-05-06
    body: ">What exactly is the difference between Karamba and SuperKaramba?\n\nSuperKaramba is Karamba with a python interface built in, so that you can have interactive eye-candy.\n"
    author: "Jeremy"
  - subject: "I use it for many things.."
    date: 2004-05-17
    body: "I use superkaramba for system monitoring, managing my seti@HOME client, minimizing the amount of icons on my desktop by using iLaunch, and at work I created a p4 (perforce) monitoring applet that allows you to do diff's on opened files!!!\nWith the python scriptability and easy to learn api it is definitely very useful and I believe that it is here to stay.\ncheers,\n-Ryan"
    author: "Ryan"
  - subject: "great"
    date: 2004-06-02
    body: "I use SK for many things one off them is for the all moast complete removal of kicker. it is not complete (systray is still in a start stage) but it works for me.\n\nhttp://www.kde-look.org/content/preview.php?preview=2&file=7890-2.png&name=Dyntaskbar+and+System+tray"
    author: "nunopinheiro "
  - subject: "enhancements to superkaramba"
    date: 2005-11-17
    body: "http://kde-look.org/content/show.php?content=31081\n\nback at around the time of the original post, i was working on superkaramba and kroller v0.94, with some significant alterations.\n\none of the things missing from superkaramba is (was) the ability to read the KDE system menu (KSystemGroup and KSystem) which you of course know is editable by kmenuedit...\n\n... but up until i patched superkaramba, you couldn't _read_ the damn system menu.\n\nso, anyway.  to answer the original question: superkaramba as-is is pretty but not really a candidate for replacing the docker.\n\nsuperkaramba _with_ the features i added: you can get rid of the docker for good and use themes like kroller.sez and others which show you desktops and active windows, and it's all hunky-dory and pretty.\n\nl."
    author: "Luke Kenneth Casson Leighton"
---
<A href="http://www.linuxjournal.com/">Linux Journal</A> has an <A href="http://www.linuxjournal.com/article.php?sid=7417">article</A> written by Marcel Gagne about 
<A href="http://netdragon.sourceforge.net/">SuperKaramba</A>. Read how you can bring <A href="http://www.kde-look.org/content/preview.php?preview=1&file=6384-1.jpg&name=Liquid+Weather+%2B%2B" target="_blank">those dynamic applications</A> to your own desktop so you can <A href="http://www.kde-look.org/content/preview.php?preview=1&file=11405-1.jpg&name=cynapses+karamba" target="_blank">monitor your CPU usage, disk space and network activity</A> floating transparently while constantly being updated. Or with the words of Monsieur Gagne
<em>&#034;Don't merely monitor your system logs-give your system stats displays a certain je ne sais quoi with GUI tools.&#034;</em>




<!--break-->
