---
title: "Junior Jobs: A Cool Way to Start Hacking KDE"
date:    2004-09-02
authors:
  - "cwoelz"
slug:    junior-jobs-cool-way-start-hacking-kde
comments:
  - subject: "Tell your friends"
    date: 2004-09-02
    body: "And if, like me, you don't know how to code, tell your programming friends about the Junior Jobs list. Some of them might even get hooked on KDE-devel :)"
    author: "Mikhail Capone"
  - subject: "How to mark as JJ?"
    date: 2004-09-02
    body: "How can I mark a bug report as JJ? I already tried this but found no option on bugs.kde.org. Is it just adding a JJ: at the beginning of the bug title?\n\nSebastian"
    author: "Sebastian Stein"
  - subject: "Re: How to mark as JJ?"
    date: 2004-09-02
    body: "> Is it just adding a JJ: at the beginning of the bug title?\n\nYes."
    author: "Anonymous"
  - subject: "Re: How to mark as JJ?"
    date: 2004-09-02
    body: "There is a \"Junior Job\" query at the start page of http://bugs.kde.org (for example if you want to test that you renamed the title of bug correctly.)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "JJ"
    date: 2004-09-02
    body: "JJ is Joachim Jakobs, media guy of FSFE.\n\n:-)"
    author: "Jan"
  - subject: "Where to send patches on JJ's"
    date: 2004-09-02
    body: "I'm interested in doing JJ's, but I don't know where to send the patches or code I'll write. Is it a public CVS so everyone can check out/in ??"
    author: "Uwe Liebrenz"
  - subject: "Re: Where to send patches on JJ's"
    date: 2004-09-02
    body: "I think posting the patch as an attachment to the bugreport would work ok. Otherwise, send it to the maintainer(s) listed in the About box of the application."
    author: "Andre Somers"
  - subject: "Re: Where to send patches on JJ's"
    date: 2004-09-02
    body: "I had tried to make a tutorial on how to send patches:\nhttp://developer.kde.org/documentation/misc/sendingpatches.php\n\nFor bug reports, the best is indeed to attach the patch to the bug report.\n\nHave a nice day!"
    author: "Nicolas Goutte"
---
Today on the <a 
 href="https://mail.kde.org/mailman/listinfo/kde-quality">kde-quality 
 list</a>, Christian Loose of <a 
 href="http://www.kde.org/apps/cervisia/">Cervisia</a> fame celebrated 
 the initial success of the Junior Jobs, which helped him <a 
 href="http://bugs.kde.org/show_bug.cgi?id=74751">get</a> <a 
 href="http://bugs.kde.org/show_bug.cgi?id=77894">three</a> <a 
 href="http://bugs.kde.org/show_bug.cgi?id=80177">patches</a>. Junior 
 Jobs were <a href="http://dot.kde.org/1084433566/">suggested by 
 Adriaan de Groot back in May</a>. They serve as a "you are welcome to
 hack here" sign, and mark bugs or wishes that are suitable for someone
 who is starting to hack KDE. 




<!--break-->
<p>As Christian puts it: "I started to mark some bugs as JJ some months 
 ago. Now I already received three high-quality patches from two 
 different people (Thanks Sergio and Dermot!) for those reports. 
 That's much more than I expected."<p/>

<p>So, developers, remember to mark those easy bugs with a <a 
 href="http://bugs.kde.org/buglist.cgi?short_desc_type=allwordssubstr&short_desc=JJ%3A&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED">'JJ'</a>.
 Some coaching for the new developers who accept the challenge would
 represent a nice incentive as well.</p>

<p>Or, if you always wanted to develop KDE and never knew where to start,
 check the open <a 
 href="http://bugs.kde.org/buglist.cgi?short_desc_type=allwordssubstr&short_desc=JJ%3A&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED">Junior
 Jobs</a> out. I am sure you will find something fun to do!</p>


