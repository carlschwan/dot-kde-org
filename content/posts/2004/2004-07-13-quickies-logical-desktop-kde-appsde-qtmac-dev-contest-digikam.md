---
title: "Quickies: Logical Desktop, KDE-apps.de, Qt/Mac Dev Contest, Digikam"
date:    2004-07-13
authors:
  - "fmous"
slug:    quickies-logical-desktop-kde-appsde-qtmac-dev-contest-digikam
comments:
  - subject: "Digikam rocks!"
    date: 2004-07-13
    body: "I love digikam and my Canon A80 rocks with it!!!!"
    author: "Coward"
  - subject: "Re: Digikam rocks!"
    date: 2004-07-13
    body: "I cannot agree more.\nThat is what my wife is using, and except for the first time using it, she never asked me anything about the program.\n30 minutes later, she was sending photos took with her brand new Canon 180 by email.\nThis tool is pretty powerful.\nLately, I was thinking of giving her a special issue of a magazine, with several tasks (like removing red eyes) in the Gimp explained, with photos and every steps. Now I think I will wait for the red eye Digikam plugin, it will be simpler."
    author: "Ookaze"
  - subject: "Re: Digikam rocks!"
    date: 2004-07-14
    body: "agree... allthough i never tried an other app for digital photo management, is app was allredy installed on my SuSE 9.1 laptop and 'just worked'.\n\ngoregeous."
    author: "cies"
  - subject: "CVS version is great"
    date: 2004-07-13
    body: "I agree. The current CVS version is awesome. Tags are great, and\nthere's now even red-eye reduction."
    author: "Peter"
  - subject: "Digikam Web site"
    date: 2004-07-13
    body: "Blue text on a turquoise-grey background. What is this? The Commodore 64 area of the Web?"
    author: "The Badger"
  - subject: "German KDE-Apps"
    date: 2004-07-13
    body: "The translation is horrible and incomplete\n\nFile Sharing - Datentausch\n\nService menu - Servicemen\u00fc\n\nContent - Werke\n\nTactics and Strategy - Taktik und Strategie\n\nNetworking Tool - Netzwerkzeug\n\nTools - Helfer"
    author: "Andre"
  - subject: "Re: German KDE-Apps"
    date: 2004-07-15
    body: "There is no german translation for the CVS version right now. It is all down automatically by script. The reason is that i am the translator and i can't compile the current digikam CVS version on my SuSE 8.2 system. \n\nI will install a more actual distribution this weekend and give the whole thing a new chance.\n\nOliver"
    author: "Oliver"
  - subject: "KDE-Darwin"
    date: 2004-07-13
    body: "\"Apparently it blew their socks off.\"\n\nJust curious, but where did you hear this?  I was the one who submitted kde-darwin and didn't hear anything about it...\n\n-Benjamin Meyer"
    author: "Ben Meyer"
  - subject: "Re: KDE-Darwin"
    date: 2004-07-13
    body: "Apple MacOS X is the most proprietary plattform, so I don't like it when KDE programs are run there. But you can install KDE/Linux on Mac, so..."
    author: "Henryk"
  - subject: "Re: KDE-Darwin"
    date: 2004-07-13
    body: "This is stupid. Many Mac users use a Mac because they depend on some applications which exist only for the Mac. They can't install Linux on their Mac, even if they wanted to.\n\nThe kernel is BSD, BSD licensed. The system compiler is gcc, GPL. It comes with all the GNU CLI tools you know from Linux. It comes with an X server, XFree I think.\n\nSo ????\n\nAlex"
    author: "aleXXX"
  - subject: "Re: KDE-Darwin"
    date: 2004-07-13
    body: "The port of KDE,  the popular Linux Desktop Environment based on Qt. The jury says \"This is amazing. We look forward to see more from these guys.\" \nThey have linked the fink version, but since it was a Qt/Mac and not Qt/X11 contest that is probably just a mistake. Maybe they didn`t want to give the price to alpha-software. Keep up the good work, I am certain you will win next year."
    author: "Lastninja"
  - subject: "Re: KDE-Darwin"
    date: 2004-07-14
    body: "Great acknowledgement of an important project.  KDE runs on UNIX-like operating systems using Qt; Qt/Mac runs natively on the Mac which has a UNIX-like operating system.  There's no reason X11 has to be involved (unless you have other reasons to run it like remote windows).  This project should improve both KDE and Qt."
    author: "S Page"
  - subject: "Re: KDE-Darwin"
    date: 2004-08-24
    body: "you can run kde under the full os x aqua-enabled darwin just fine as it is xfree86 based no different to kde or any other *nix gui. to use the xfree86 backdoor, just set all logins to text only username instead of that picture login thing and and type the username as: >console\nwith no spaces, just \">console\" and don't enter anything as the password. that will dump the aqua and leave you into the bowels of darwin. happy hunting.\n-wombat"
    author: "wombat"
  - subject: "Re: KDE-Darwin"
    date: 2009-01-17
    body: "I think you guys are missing the point of this.. It's like people who gave up on HURD after the linux kernal... the point is not to get cocky and keep going until you get it right...\n\nMac OS is a load of pretty technobolloks... I should know, I'm writing this from a new macbook.. I've been using MAC since the beginning, and OS X since Rhapsody and had friends who worked on NEXTSTEP... So let me be frank when I say GNU people.. and KDE4 is the place to go with Darwin..\n\nbut I wouldn't go so far as to say Darwin is the place to go... I think we should retract to HURD, lets get the libraries together for HURD fabricate a HURD OS."
    author: "andreas nicholas"
  - subject: "segusoLand"
    date: 2004-07-13
    body: "Segusaland/logical Desktop, this is really innovative but unfortunately not very intuitive and it takes a lot of the screen.\n\nI would prefer a natural language interface.\n\n\"I want to create a bitmap\"."
    author: "Jupp L"
  - subject: "Its really more of a test platform IMO"
    date: 2004-07-13
    body: "To test out the idea. assuming it works out then a more intuitive interface can be wrapped around it."
    author: "Jason Keirstead"
  - subject: "Re: segusoLand"
    date: 2004-07-13
    body: "Yuck. natural language is slow and yucky. Your example took 20+ key presses and you have only identified the verb. If you are going to be typing things, you are better off with a traditional CLI with tab completion. This logical desktop thing looks great to me. Sure it takes a good chunck of the screen, but you no longer need to browse for your files, right click etc. An option to minimize on execute might be a good idea. I am at work now, but can't wait to try this out at home."
    author: "Jonathan Dietrich"
  - subject: "Re: segusoLand"
    date: 2004-07-14
    body: "Take it a step further.  Do NOT require me to TYPE that.. instead... ALLOW me to SPEAK IT.\n\nTHAT is what will revolutionize computers."
    author: "Jeff Stuart"
  - subject: "Re: segusoLand"
    date: 2004-07-14
    body: "Still no good. Now you say \"I want to edit a bimap\" Easy enough, you have a verb. But selecting the file you want to edit is still nasty unless you have some technology like logicaldesktop to limit your choices. Currently you have to navigate a hierarcy which if we had a speech interface today I would have to speak it. /home/myuser/pictures/20040714/0020gh.jpg This is a shot off of a digital camera sorted in directories by date taken autonamed by the camera. Speaking this is not pretty nor fast."
    author: "Jonathan Dietrich"
  - subject: "AlbumShaper and Digikam albums"
    date: 2004-07-13
    body: "Anybody knows if AlbumShaper can read Digikam's album details and picture comments? That's the only reason I use Digikam's HTML export (which sucks) instead of using some other tool for the job.\n\nAlso it would be cool if Digikam had at least some support for the movies that digital cameras generate."
    author: "AC"
  - subject: "Re: AlbumShaper and Digikam albums"
    date: 2004-07-13
    body: "Preview support is going into Digikam 0.7.\n\nWhat other kind of features did you have in mind? Playback is (sensibly) handled by external video players, and any editing would be quite a lot of work. It could, if it finds suitable encoding tools, re-encode videos to reduce their file size, screen size, rotate them, etc. but I should imagine that'd be quite a lot of work too."
    author: "Tom"
  - subject: "Re: AlbumShaper and Digikam albums"
    date: 2004-07-13
    body: "Sounds like he want to use AlbumShaper to produce HTML export from DigiKam, becouse he don't like the output from Digikam's HTML export plugginn. More like a feature for AlbumShaper (make it read dikgicam albums), or for using AlbumShaper as a plugginn for HTML export."
    author: "Morty"
  - subject: "Re: AlbumShaper and Digikam albums"
    date: 2004-07-14
    body: "Thanks, that's what I needed. :-) Right now even though the videos are copied (by Digikam) from the cameras and added to the album directories, there's nothing in Digikam indicating their existance.\n\nAs for other features, I really don't need anything fancy. Just seeing the videos and being able to launch an external player to play them is more than enough for me, although I can see how other people would like to do more on them (maybe add support for the transcode applications as a plugin?).\n\nThe other poster got it right w.r.t. AlbumShaper: it'd be nice to have it as the \"HTML export\" plugin for Digikam, since the current one is really limited."
    author: "AC"
  - subject: "Re: AlbumShaper and Digikam albums"
    date: 2004-07-15
    body: "The Digikam HTML export is now a KIPI plugin. LibKIPi is a port of old DigikamPlugins on a new library for all Images managment program who wants to use that.\n\nThe Current KIPI HTML export is just an HTML 4.01 albums export. A new XHTML export plugin must be created for a future best HTML export rendering.\n\nIt's would be nice if the HTML export of AlbumShaper will be ported to KIPI plugins.\n\nRegards\n\nGilles Caulier\nDigikam developer.\nKIPI developer."
    author: "Gilles Caulier"
  - subject: "Re: AlbumShaper and Digikam albums"
    date: 2004-07-15
    body: "Do you guys realize how trivial the HTML export of Album Shaper is these days? I use to have it all hard coded but once I went over to supporting themes all that code was ripped out. Here is the basic code now:\n\n1.) write out album structure with photo descriptions to an Album.xml file\n2.) apply an xslt program to the xml file\n\nthat's it. well, I also copy over a resources directory from teh theme to the album being saved out so thems can use standard buttons and backgrounds etc but that's trivial. all html writing is taken care of by the xslt theme and this allows such themes to create whatever files they want, 1, many, you name it.\n\nI should mention that I'm in the process of extending the meaning of themes. Previously you could only enjoy your photos once saved out in HTML format. Browsers are nice and all, but I know I can do a better job of presenting your photos in the program itself. Presentations will augment HTML export and will be entirely themeable as well. that is, themes will be extendedd to also specify the layout of such presentations so that the way in which you enjoy/view your photos is consistant across media types (in program presentation, html, etc). I don't think this poses any problems to what we are discussing, just pointing out that if you want to strap the html export code from Album Shaper for exporting only you'll miss out a bit on the full potential of themes..."
    author: "Will Stokes"
  - subject: "Re: AlbumShaper and Digikam albums"
    date: 2004-07-14
    body: "Hello people's. I was curious to see if winning the Qt contest would get me any recognition on the dot even though Album Shaper is not a KDE app. Album Shaper cannot currently import albums from other programs, but that's mainly because most other software is just scripts that produce nice albums that can be easily modified later. I don't use Digicam myself (I use Album Shaper obviously). Does Digicam save out photo organization and descriptiosn out to a file, or even in xml format? :)\n\nI'd like to point out that Digicam and Album Shaper have very differnt main goals. I think it's safe to say Digicam is looking to produce something similar to iPhoto for KDE. Album Shaper, on the other hand, intentionally has not KDE requirements so it can work on Windows, Linux, and Mac OS X and provide users with the same experience on each. Roughly half of my users are using non Linux systems so supporting Windows and Mac os X means a lot to me.\n\nAnyways, it is nice to get some recognization on the dot. :) Enjoy using Album Shaper or Digicam, whatever works best for you. \n-Will Stokes"
    author: "Will Stokes"
  - subject: "Re: AlbumShaper and Digikam albums"
    date: 2004-07-14
    body: "It would be great if AlbumShaper, DigiKam (and possibly gphoto) could all standardize on the same file formats for tags, comments etc."
    author: "Anonymous"
  - subject: "Re: AlbumShaper and Digikam albums"
    date: 2004-07-14
    body: "This may be difficult to do, but I'm not against it. At least, I think it would be a good idea if the various programs were able to open each others albums etc. the problem lies in the fact that some photo organization software tries to handle inifnitate levels of organization and others (like Album Shaper) limit the hierarchy. anyways, defiantely something i am interested in, but there are many. "
    author: "Will Stokes"
  - subject: "Yoper"
    date: 2004-07-13
    body: "Has anyone tried Yoper Linux?\n\nI must say, I didn't know KDE could be this fast, that there could be such a big difference compared to Suse and Mandrake.\n\nI recommend trying it. There are ISO:s."
    author: "Alex V"
---
There is a <a href="http://logicaldesktop.sourceforge.net/img/begin.png">new interface</a> 
for executing actions in a uniform way called 
<a href="http://logicaldesktop.sourceforge.net/">Logical Desktop</a>, a successor
of the program <a href="http://segusoland.sourceforge.net/">segusoLand</a>. 
It is a quite interesting approach on using your desktop as you work by composing 
one verb actions with one or more objects. *** 

Did you know there exists a <a href="http://www.kde-apps.de/">German version</a> of 
<a href="http://www.kde-apps.org/">KDE-apps.org</a>? Now I'm waiting for a <a href="http://www.evermann.de/kde_op_platt/">Plattd&#252;&#252;tsch version</a>. Any takers? ***


Recently Trolltech <a href="http://www.trolltech.com/campaign/maccontest_winners.html">announced</a> 
the winners of the Qt/Mac Application Developer Contest. Did you see them mention the 
<a href="http://kde.opendarwin.org/">port of KDE</a> in that announcement? Apparently it blew their socks off.
The winner of the category <em>Best Ported Qt Application</em> is the 
<a href="http://albumshaper.sourceforge.net/screenshots_1.0b1.shtml">cool</a> 
<a href="http://albumshaper.sourceforge.net/">AlbumShaper</a> which has 
<a href="http://www.trolltech.com/images/contest/albumshaper.jpg">been ported to Mac OS X</a>  ***

AlbumShaper is not the only contender in the field as the KDE application 
<a href="http://digikam.sourceforge.net/">Digikam</a> is rapidly getting
<a href="http://digikam.sourceforge.net/todo.html">more features</a> like 
<a href="http://static.kdenews.org/fab/screenies/digikam/sshot2.png">nested</a> 
<a href="http://static.kdenews.org/fab/screenies/digikam/sshot1.png">albums</a> and 
<a href="http://static.kdenews.org/fab/screenies/digikam/sshot3.png">tagging</a> 
of <a href="http://static.kdenews.org/fab/screenies/digikam/sshot4.png">images</a>.

<!--break-->
