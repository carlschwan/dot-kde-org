---
title: "KDE Awarded Best Linux Desktop Manager by LinuxWorld Readers"
date:    2004-01-22
authors:
  - "wbastian"
slug:    kde-awarded-best-linux-desktop-manager-linuxworld-readers
comments:
  - subject: "huh?"
    date: 2004-01-22
    body: "I am suspicious of this \"Computer Associates\" company's consistent top placing.  I've never heard of these products that are winning.  What's \"Unicenter software delivery\" and how is it in the same cagegory as KDE and GNOME?  And what the heck is Flowe desktop?  The link they give doesn't even work.  Why should those be on the list instead of XFCE?  And how is VMWare a better developer tool than valgrind?  And how is KDevelop not on the list of programming environments?  Seems like there's a lot of astroturfing going on, and a lot of great open-source software got left out in favor of inferior commercial software."
    author: "Spy Hunter"
  - subject: "Re: huh?"
    date: 2004-01-22
    body: "Oh, and BTW, KDevelop 3.0 looks simply incredible.  I think MSVC++ may be playing catch-up after 3.0 is finally released..."
    author: "Spy Hunter"
  - subject: "Re: huh?"
    date: 2004-01-22
    body: "If KDevelop finally gets _usable_ code-completion, help system, debugger and may-be and integrated infterface designer as 6? year old Visual Studio 6."
    author: "noname"
  - subject: "Re: huh?"
    date: 2004-01-22
    body: "AFAIK all execpt the GUI Builder will be build in in 3.0.\nThe Qt4 roadmap says that QtDesigner for Qt4 will be better reusable by programs like kdevelop. So I think when Qt4 will be released (or short after the release) kdevelop will support all what you want. ;)"
    author: "panzi"
  - subject: "Re: huh?"
    date: 2004-01-22
    body: "Bad try!\n\n"
    author: "Rischwa"
  - subject: "Re: huh?"
    date: 2004-01-22
    body: "Your post is very uninformative.  If you would post *why* you think that KDevelop 3.0's code completion, help system, and debugger are inferior to VS6's, and why you think an integrated interface designer is important, then we could actually have a discussion."
    author: "Spy Hunter"
  - subject: "Re: huh?"
    date: 2004-01-22
    body: "Would you reply in the same way if he had said that KDevelop's features were superior to those of MSVC6 ?"
    author: "Guillaume Laurent"
  - subject: "Re: huh?"
    date: 2004-01-22
    body: "Do you expect MSVC6 developers to look here for feedback?"
    author: "Datschge"
  - subject: "Re: huh?"
    date: 2004-01-22
    body: "Not really, but that's not the point of my asking."
    author: "Guillaume Laurent"
  - subject: "Re: huh?"
    date: 2004-01-23
    body: "Well, I guess you didn't get my somewhat twisted point. I do expect some of the KDevelop developers to look at these comments (in contrary to MSVC6 developers) so I have to fully agree with Spy Hunter that noname's post was very uninformative.\n\nRegarding your question, yes, I hope Spy Hunter or anyone else would have said the same in the fictional case of a reverse statement, just for the sake of actually discussing about software and interface design instead throwing around the usual blanket statements.\n\nCheers."
    author: "Datschge"
  - subject: "Re: huh?"
    date: 2004-01-22
    body: "VMWare is definately a great development tool. Ever tried to test a network application on a single computer? VMWare lets you do it. Ever tried to test an application that needs to work together with a Windows counterpart without a single Windows machine around? With VMWare it's possible.\nWithout VMWare, I wouldn't have been able to test the KDE Remote Desktop Client against a Win2000 Terminal Server."
    author: "Arend jr."
  - subject: "Re: huh?"
    date: 2004-01-22
    body: "VMWare lets you make things more conveniently by not requiring another machine.  Valgrind lets you do things that weren't possible before Valgrind was invented.  I'm sure VMWare is a fine developer tool, but I also think Valgrind is cooler."
    author: "Spy Hunter"
  - subject: "Re: huh?"
    date: 2004-01-22
    body: "Well I guess XFCE isent in any cattegorx because noone enterd them to the competition.\nBut the Flowe Desktop Personal Edition is a rather strange thing. At the URL I got some Online Casino!!!"
    author: "Calle"
  - subject: "BEST DEVELOPMENT TOOL! KDEVELOP"
    date: 2004-01-22
    body: "\"Best Development Tools:\n\nCompany: KDE \nProduct:   Kdevelop 3.0\"\n\nhttp://www.linuxworldexpo.com/linuxworldny/V40/index.cvn?ID=10240\n\nI can't believe KDE didn't receive the Best Open Source PRoject Award too, Helix player, what a bunch of bulshit, that project isn't half as big as KDE and hasn't develoed anywhere near the same amount of quality software.\n\nKDE SHOULD OF WON!\n\n"
    author: "Alex"
  - subject: "Re: BEST DEVELOPMENT TOOL! KDEVELOP"
    date: 2004-01-22
    body: "> KDE SHOULD OF WON!\n\nI think it's quite alright, KDE has won this award in the past already (kde 2.2 I beleive)"
    author: "anon"
  - subject: "Re: BEST DEVELOPMENT TOOL! KDEVELOP"
    date: 2004-01-22
    body: "Xandros won."
    author: "ac"
  - subject: "Re: BEST DEVELOPMENT TOOL! KDEVELOP"
    date: 2004-01-22
    body: "...so KDE won too, in a way. :)"
    author: "ac"
  - subject: "Awards in 2003"
    date: 2004-01-22
    body: "Have you seen how many awards KDE has won in 2003\n\nhttp://www.kde.org/awards/\n\nFab"
    author: "Fabrice Mous"
  - subject: "Awards are fine, corporate adoption is better"
    date: 2004-01-22
    body: "Nice to see KDE winning Awards.\n\nWhat puzzles me is why the Gnome project is so much better at being accepted by corporations as their Linux desktop of choice (think Novell, Sun, soon SuSE as well).\n\nKDE has so much going for it. Some of these features are actual benefits for  corporate users, the KDE kiosk feature for example. Corporate users don't care about how beautiful icons are, they care about the cost. KDE kiosk meets that demand. In my opinion it is not put in the spotlight enough. And the name 'kiosk' is misguiding, it can be used for so much more then building a kiosk desktop.\nKDE Kiosk is about reducing desktop TCO. \n"
    author: "Bart"
  - subject: "Re: Awards are fine, corporate adoption is better"
    date: 2004-01-22
    body: "Novell and SUSE are the same company now and there is no indication that it has any Linux desktop of choice and less that it would be GNOME (for all products)."
    author: "Anonymous"
  - subject: "Re: Awards are fine, corporate adoption is better"
    date: 2004-01-22
    body: "I don't think what is happening with Gnome is sutainable at all. Gnome is not being accepted by corporations. It has seemingly been accepted by Sun and Novell, and that's it."
    author: "David"
  - subject: "Re: Awards are fine, corporate adoption is better"
    date: 2004-01-22
    body: "the whole \"SUSE is going to be GNOME-centric any day now\" meme is getting really tired. i'd suggest waiting until there's actual news to that effect and/or SUSE ships a GNOME-centric distribution (w/out a KDE equivalent) before repeating that little gem ad nauseum. you aren't doing KDE or your own credibility any favours."
    author: "Aaron J. Seigo"
  - subject: "Re: Awards are fine, corporate adoption is better"
    date: 2004-01-24
    body: "Although, there is something to be said about worrying about a problem preemptively. If Novell/SuSE ships a GNOME-centric distribution, then its already to late. We have to consider why the corporate market seems to be leaning towards GNOME, so something like SuSE going GNOME doesn't happen.\n"
    author: "Rayiner Hashem"
  - subject: "Licenses"
    date: 2004-01-22
    body: "You know it - licensing is the answer."
    author: "Anton Velev"
  - subject: "KMail"
    date: 2004-01-22
    body: "KMail is missting to."
    author: "Tobias"
  - subject: "Mozilla?"
    date: 2004-01-22
    body: "Mozilla mail?\n\nWell, the good thing about it is the \"Modern\" style.\nIf only there was a similar style for KDE/KMail..."
    author: "KA"
  - subject: "see this "
    date: 2004-01-22
    body: "http://www.smh.com.au/articles/2004/01/22/1074360870352.html"
    author: "ac"
  - subject: "re:KDE Corporate"
    date: 2004-01-22
    body: ">> What puzzles me is why the Gnome project is so much better at being accepted by corporations as their Linux desktop of choice (think Novell, Sun, soon SuSE as well) <<\n\nI find this bizarre too - its pretty clear that KDE is the most powerful, fully featured, acomplished, Desktop Environment out there - one of the reasons might simply be that companies feel they can incorporate different apps with more flexibility with GNOME than KDE - KDE includes \"everything\" and does it well - however - companies might want a \"simpler\" desktop package to [i]start with[/i], one with which they can add their own [i]prefered[/i] apps to, and package/support as their own (they can of course do that with KDE to a point) - (just a thought, know virtually nothing about this) ... thanks to the developers, the work [i]is[/i] appreciated."
    author: "sap"
---
The <a href="http://www.linuxworld.com">LinuxWorld Magazine</a>
<a href="http://www.linuxworld.com/story/39231.htm">2003 Readers' Choice Awards</a>, often referred to as the "Oscars of the Software Industry," recognizes excellence in the solutions provided by the top Linux vendors in the market. We are therefore very proud that KDE 3.1 has won first place in the "Best Linux Desktop Manager" category.





<!--break-->
<p>
Earlier this month KDE was already 
<a href="http://dot.kde.org/1073779562/">awarded</a> by members of the
<a href="http://www.linuxquestions.org/">LinuxQuestions.org website</a>
with the 2003 Award for Desktop Environment
of the Year. LinuxQuestions.org provides a variety of forums where new
Linux users can find answers to their Linux questions.
</p>
<p>
KDE developer Richard Moore was pleased by the
<a href="http://www.kde.org/awards/">continued appreciation</a>
of the KDE project: <i>"This endorsement of our work by the Linux community
is fantastic. We hope that people will like the upcoming KDE 3.2 release
just as much, and that the award will help spur us on down the path to
future KDE versions."</i>
</p>
<p>
Visitors to the LinuxWorld Conference & Expo in New York City can see KDE 3.2 in action at the KDE booth (#17) in the .org pavillion.  It is worth noting that KDE 3.2 and KDevelop are also finalists for the LinuxWorld Product Excellence awards at the conference.
</p>







