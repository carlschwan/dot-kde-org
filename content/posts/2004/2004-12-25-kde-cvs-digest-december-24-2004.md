---
title: "KDE CVS-Digest for December 24, 2004"
date:    2004-12-25
authors:
  - "dkite"
slug:    kde-cvs-digest-december-24-2004
comments:
  - subject: "Yes, the honour is mine :-)"
    date: 2004-12-25
    body: "Thanks Derek for another piece of cake!\nReading the dsgest is better than reading the fresh newspaper.. relaxing, exciting.. and it gives me more will to code! Ok, let's open kate now..\n\n"
    author: "First one!"
  - subject: "Re: Yes, the honour is mine :-)"
    date: 2004-12-25
    body: "Right on. Thank you so much for doing this on Christmas, even :)."
    author: "Eike Hein"
  - subject: "sad there is no debian kde cvs anymore"
    date: 2004-12-25
    body: "its so sad that the debian kde cvs packages are no more :-(\n\nevery effort which has done this only existed for a few months..\n\n"
    author: "chris"
  - subject: "Re: sad there is no debian kde cvs anymore"
    date: 2004-12-25
    body: "Isnt that because they have switched to their new name? Kaylxo?  The kde-debian thing was confusing and was only temporary.  Correct me if im wrong.\n\n-Sam"
    author: "Sam Weber"
  - subject: "Re: sad there is no debian kde cvs anymore"
    date: 2004-12-25
    body: "Not sure how much activity there is, but this seems to be the mailing list\n\nhttp://mail.kde.org/pipermail/kalyxo-devel/"
    author: "MK"
  - subject: "Re: sad there is no debian kde cvs anymore"
    date: 2004-12-26
    body: "Try these lines in your sources.list:  \n\ndeb ftp://archive.kalyxo.org/kalyxo staging main\ndeb ftp://archive.kalyxo.org/kalyxo experimental main\n\n(the experimental one only if you're in the proper mood ;-)\n"
    author: "cm"
  - subject: "Re: sad there is no debian kde cvs anymore"
    date: 2004-12-27
    body: "i already got these lines there , but there is no kde cvs"
    author: "ch"
  - subject: "What's kdom?"
    date: 2004-12-26
    body: "What is kdom in kdenonbeta? And how does it relate to khtml?\n\nKhtml already has W3C DOM support, of course...?"
    author: "AC"
  - subject: "Re: What's kdom?"
    date: 2004-12-26
    body: "I think it is an implementation of Geckoo on KDE as part of KMozilla."
    author: "Anonymous Spectator"
  - subject: "Re: What's kdom?"
    date: 2004-12-26
    body: "No, it's an independent implementation of DOM for the purpose of syncing several layers accessing DOM, like html rendering, javascript, svg etc. It's the modular way to avoid having to combine all the above in one monolith package."
    author: "ac"
  - subject: "Re: What's kdom?"
    date: 2004-12-28
    body: "So if it works well, it'll replace the khtml DOM?"
    author: "AC"
  - subject: "Re: What's kdom?"
    date: 2004-12-26
    body: "No, it's a dom implementation used by KSVG2."
    author: "Sven Langkamp"
---
In this <a href="http://cvs-digest.org/index.php?issue=dec242004">week's KDE CVS-Digest</a> (<a href="http://cvs-digest.org/index.php?newissue=dec242004">experimental layout</a>):

<a href="http://www.koffice.org/kexi/">Kexi</a> has a new reports module.
KPDF adds a presentation mode.
<a href="http://xmelegance.org/kjsembed/">KJSEmbed</a> adds KScript which allows applications to use JavaScript as scripting language.
<a href="http://uml.sourceforge.net/index.php">Umbrello</a> adds entity relationship diagrams.

<!--break-->
