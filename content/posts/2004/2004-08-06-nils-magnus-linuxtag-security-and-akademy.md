---
title: "Nils Magnus (of LinuxTag) on Security and aKademy"
date:    2004-08-06
authors:
  - "ateam"
slug:    nils-magnus-linuxtag-security-and-akademy
comments:
  - subject: "More secure Knoppix"
    date: 2004-08-06
    body: "> Q: What hardware and software do you work with?\n> Nils: I work with a Linux system that was set-up from an installed Knoppix with some adjustments for a more secure operation. \n\n@Nils: Did you somewhere document what you changed to make Knoppix more secure?  I think there are quite a few people running HD installs of Knoppix who might be interested in hearing that..."
    author: "cm"
  - subject: "Re: More secure Knoppix"
    date: 2004-08-06
    body: "and what did he do to make knoppix more debian-compattible, as this is the problem for alot ppl - apt-get often breaks, especially with dist-upgrade. I use kanotix, which solved this, but its annoying if I try to fix problems related to this on knoppix-hd installs on other computers."
    author: "superstoned"
  - subject: "Re: More secure Knoppix"
    date: 2004-10-01
    body: "A lot of people have complained that apt-get dist-upgrade is not really possible with Knoppix, and they are usually right. I came up with a rather simple solution for this: I ususally don't do dist-upgrades. I do security fixes by hand and may upgrade single packets the same way, but every twice a year or so I ask Klaus to release a new version of Knoppix and just install it from scratch.\n\nIt's a good way to keep your system clean and you have rather easy desaster recovery strategy for free :)"
    author: "Nils Magnus"
  - subject: "Re: More secure Knoppix"
    date: 2004-10-02
    body: "> every twice a year or so I ask Klaus to release a new version of Knoppix\n\nConsidering that more than twice releases every year there must be other people other than you asking too. :-)"
    author: "Anonymous"
  - subject: "Re: More secure Knoppix"
    date: 2004-10-03
    body: "well, debian's best thing is its upgradabillity... so I'd prefer not having to reinstall it, that's a windows thingy ;-)"
    author: "superstoned"
  - subject: "Re: More secure Knoppix"
    date: 2004-10-01
    body: "Making Knoppix more secure is mainly a matter of personal preferences. In most aspects Klaus already does a pretty decent job; however, in some aspects like a better IPSec support he could still improve a lot, though.\n\nI also think, Knoppix could be good platform to distribute a community driven trust base by placing some well-defined certificates onto the CD/DVD. In the end, every user should decide for herself or himself if this kind of trust is suitable. However, some well known web pages could be security-improved this way.\n\nOther things I usually add to an installed Knoppix are lots of not so well know and widely used security tools as I need them for my daily work as a security tester."
    author: "Nils Magnus"
  - subject: "Any computer with a network connection?"
    date: 2004-08-07
    body: "> So any computer with a network connection is sufficient\n> for me, because I always have a Knoppix DVD or a memory stick with\n> me.\n\nSo any computer with a network connection can boot from a DVD or USB memory stick?  Not in my world; I have enough trouble finding public-access machines that can read DVDs or memory sticks, let alone boot from them.  I guess things are more advanced in Europe.  Or less likely to have alternate booting mechanisms disabled in the name of security. "
    author: "Rob Funk"
  - subject: "Re: Any computer with a network connection?"
    date: 2004-10-01
    body: "You are right, not every computer can boot from DVD or USB storage devices, but most of the relevant can. If everything else failes, I am usually able to install a local SSH client on most operating systems. I am very proud that I actually managed to do so on one of these colorful half-swallowed systems, what are them called? Oranges? Kiwis? Forgot :)"
    author: "Nils Magnus"
---
As part of a series of articles previewing KDE's World Summit, 
<a href="http://conference2004.kde.org/">aKademy</a> (running from August 21st to 29th), Michael 
Renner and Tom Chance interviewed Nils Magnus of LinuxTag about security on the desktop. He is 
due to deliver a <a href="http://conference2004.kde.org/tut-securitydesktop.php">tutorial
on security on the KDE desktop</a> with Kester Habermann, one of 15 that run in parallel with 
the coding marathon. Read on for their thoughts on Linux and Windows security, software patents 
and more.



<!--break-->
<p><b>Q: Is Linux actually more secure then Windows or is it just less common?</b></p>
  
<p>Nils: Well, Linux is in fact still not as common as Windows at the moment. But it
would be fatal to trust that fact when you think about security. We know that 
all software has problems. Even with Linux we had occasional incidences in the past. 
An example for this is the slapper worm that attacked the Apache web server.</p>

<p>However, the major difference to Windows and all other proprietary software is
that security problems, due the free availability of the source code, are easier to 
find and to fix.</p>

<p>In the expert's view, the kinds of current Windows vulnerabilities are 
technologically similar to those that we had in Linux and other free
Operating Systems back in the 1990s, e.g. buffer overflows and Off-By-Ones. 
Such errors have since declined in Linux.</p>

<p>As soon as a vulnerability is known, the reaction time is in the range of
a few hours for open source software. For proprietary software it often takes
30 days or more; manufacturers call this a short response period.</p>

<p>Finally, due to its architecture, Linux is free of one plague: There are no Linux 
viruses! Sophos, the anti virus manufacturer, lists just two linux viruses, 
but these have only been of academic interest and are rare 'in the wild'. 
After all, the well-engineered system design is based on the experiences of 35 years of
UNIX development.</p>


<p><b>Q: Is physical access to a computer insecure in general?</b></p>

<p>Nils: Yes, this is generally correct. If the attacker has physical access, the system administrator
has a hard job to make the system secure. This is the reason why server systems
are typically operated in secured data centers.</p>

<p>With desktop system, the focal point of <a href="http://conference2004.kde.org/tut-securitydesktop.php">
our tutorial at aKademy</a>, there are some different rules. The subset of people with 
physical access to a system won't have such criminal intentions like an unknown attacker. 
An encrypted hard disk, restricted user rights, removable media like USB sticks or critical 
data at a fileserver help a lot.</p>

<p>And of course we should consider the 'Trusted Computing' issue. It was originally
concerned with this problem, whereas lately is has been abusively confused with 'Digital Rights
Management (DRM)'.</p>



<p><b>Q: How good must security be, or is absolute security needed?</b></p>

<p>Nils: There is no absolute security per se with computer systems. The administrator's task is to define 
and reach a level of appropriate security. We often hear 'there is no critical data 
on my computer'. But is this true? Information technology is increasingly becoming a part
of many areas of our life . We won't notice this in any case. Do we access our e-bank 
account from the same computer? How would the employer react if in the private web cache 
several situations offered are found? And does the music and advertising industry 
have insight to every private preference?</p>

<p>Fortunately, a modern operating system like Linux has protective mechanisms that
can be activated and administrated easily with KDE. How to disclose and fix harassment
will be discussed in detail in the tutorial.</p>


<p><b>Q: What effects do you expect from software patents?</b></p>

<p>Nils: So-called software patents are a dangerous threat for the small and medium enterprises 
in Europe, because they have to spend considerable amounts of time and money in the
check-up for existing patents and the defense of such demands. For that reason
experts and concerned citizens are critical of so-called software patents. Seventy 
five percent of the Linux Tag 2004 visitors said they are against software 
patents, whilst less than 0.4% favored them.</p>


<p><b>Q: What hardware and software do you work with?</b></p>

<p>Nils: I work with a Linux system that was set-up from an installed Knoppix with some
adjustments for a more secure operation. I travel a lot, so I use computers in
environments where I can not be sure about their integrity (e.g. my notebook).
Important data is stored on a central, well-secured place that I can reach via
an encrypted Internet connection. So any computer with a network connection
is sufficient for me, because I always have a Knoppix DVD or a memory stick
with me.</p>


<p><b>Q: Is there something else that you want to say to our readers?</b></p>

<p>Nils: Safety is a fascinating topic with many aspects. In our totorial we want to
show how you can help yourself to find your own point of view. We will
have lots of practical exercises and demonstrations, so the theory will be 
transferred directly into practice.</p>

<p><b>Q: Thank you for your answers and your time</b></p>

<p>Nils: No problem.</p>


