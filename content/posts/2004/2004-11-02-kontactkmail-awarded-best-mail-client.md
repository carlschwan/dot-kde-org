---
title: "Kontact/KMail Awarded Best Mail Client"
date:    2004-11-02
authors:
  - "binner"
slug:    kontactkmail-awarded-best-mail-client
comments:
  - subject: "Congrats!"
    date: 2004-11-02
    body: "Congrats to Ingo and all the KMail team!\n\nPS Ingo, sorry this isn't one of those long posts. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: Congrats!"
    date: 2004-11-02
    body: "Thanks, Eric. This must be your shortest post ever. ;-)"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Congrats!"
    date: 2004-11-03
    body: "I for my part like the long ones very well. \n\nDid you think of bloging?\n\nYours, Kay"
    author: "Debian User"
  - subject: "Kontact"
    date: 2004-11-02
    body: "I think that the Windows Software provided with Palms is still best for Kontact management. kontact is immature for professional use."
    author: "gerd"
  - subject: "Re: Kontact"
    date: 2004-11-02
    body: "Kontact is _not_ doing syncing. That's what kitchensync and KPilot are for.\nAnd maybe except for kitchensync I can definitely not agree. Sorry."
    author: "Daniel Molkentin"
  - subject: "Re: Kontact"
    date: 2004-11-02
    body: "You simply have absolutely NO PLAN what Kontact is! Kontact alone wont help you very much and is according to your poor definition for sure not professional and will never be. Kontact is a shell, which embeds different standalone KDE applications as a kind of plugins in one UI. Kontact is much more than just Palm-syncing. It is a full featured PIM-environment ready for professional use (saying: big chief, often called CEO, is able using it and will like it). It is the only PIM with the advantage that it is not monolitic as Evolution or Outlook. It can be reduced to the individual needed features or extended easily with new ones via Kpart and so there also exist a KDE-palm-syncing-application inside (and outside) Kontact.\n\nBy the way: Many thanks to the programmers of KMail. It is my favorite Mail application for years. (I didn't use the other PIM applications that much simply because they are far beyond my personal needs)"
    author: "Daniel Arnold"
  - subject: "Re: Kontact"
    date: 2004-11-03
    body: "Although it may look like no plan, I think that the modularity of Kontact gives it huge advantages over other such suites. For example if a given company wanted to add another PIM related program which they had coded as a kpart, then it could be loaded into kontact (as I understand it), in order to do some organisation specific task, such as tracking work hours or something.\n\nI've been a big fan of the kde PIM suite for some time because it's modularity. We're now seeing a lot more integration, which is what everyone has been crying out for. I just hope that the integration is possible without harming the modular nature of these applications. I think there are two many huge software suites which are not suitable for certain groups of users because one part of them isn't right. Ideally, with KDE PIM, if I didn't like, say the address book, I should be able to code my own and use it seamlessly in place of KAddressBook (just an example btw). I'd love to know if this is currently the case. But even if it isn't entirelly seamless, Kontact and KDE PIM is much closer to this goal than anything else. I'm just not sure if I'm the only loony out there who things that this is important. :)\n\nAnyway, congratulations to the Kontact/Kmail team for winning this well deserved award.\n\nCraig"
    author: "Craig Ambrose"
  - subject: "Re: Kontact"
    date: 2004-11-03
    body: "You could surely replace KAddressBook with another Address Book application. There are probably a few things which won't work anymore because KMail explicitely calls kaddressbook via dcop. But if needed a general address book dcop interface could be created so that alternatives to KAddressBook which implement this interface would be fully compatible with KMail. For the other components of Kontact there are similar constraints."
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Kontact"
    date: 2004-12-14
    body: "Hi,\nIs this you, Craig (Chuckie)?  I've lost you can am wondering how/where/what you're doing these days.  Get back to me if this is you.  I just got a card from Mark Shintani and was feeling very nostalgic.\nJeaneen"
    author: "Jeaneen"
  - subject: "Re: Kontact"
    date: 2004-11-02
    body: "Define professional use for me. There's still some thing that Kontact needs to improve (and a lot of those are top show, dotting is and crossing ts), but it's proving a very sold basis."
    author: "David"
  - subject: "Re: Kontact"
    date: 2004-11-02
    body: "I think this applies to the \"Palm Desktop\", a great adress book from a usability perspective. See Screenshot..."
    author: "Andre"
  - subject: "Re: Kontact"
    date: 2004-11-03
    body: "Kontact is no adress book. Kontact provides much more (see my post above). The KAdressbook ist simply one of many plugins within Kontact. It is a PIM environement (Mail/Adressbook/News/...).\n\nBy the way the screenshot shows nothing (far to poor resolution). Ever used Kontact?"
    author: "Daniel Arnold"
  - subject: "I hate Palmdesktop"
    date: 2004-11-03
    body: "My old Treo had a problem with the battery, it worked only in the cradle.\n\nBecause of that I wanted to print out the contact list, but that's impossible in Palmdesktop. It was extremely frustrating, there was just no easy way to get my telephone numbers out of the goddamn thing.\n\nSince then I hate it. - It was the last application that I migrated to Linux (I now use KPilot, too ;-)\n\n"
    author: "Roland"
  - subject: "What about the others?"
    date: 2004-11-02
    body: "Congrats to the KMail team :). Is there anywhere I can see the other winners? Couldn't find it on any of the websites linked."
    author: "Illissius"
  - subject: "Re: What about the others?"
    date: 2004-11-02
    body: "Buy the magazine :-) (IBM, X.org, HP, Skole Linux, SuSE Personal/Professional, Jack, MPlayer, PostreSQL 8, Eurolinux/FFII)"
    author: "Anonymous"
  - subject: "Congratulations"
    date: 2004-11-02
    body: "KMail & Kontact ( also KNode ) teh ownz! :-)\n\n"
    author: "cartman"
  - subject: "Congratulations"
    date: 2004-11-02
    body: "Just another bit of praise from another satisfied user :)\n\nKMail has been my favourite mail client for ages now, and it just gets better and better. This award is well deserved. Of course this also goes for the rest of KDE as well.\n\nBrilliant stuff!"
    author: "Rich"
  - subject: "Re: Congratulations"
    date: 2004-11-03
    body: "YEAH! \n\nkmail and kontact are so good that they dragged me away from beloved pine. Figure this! ---> BOLD Congratulations!!\n\nMarkus"
    author: "Markus Heller"
  - subject: "Does this mean kio_smtp is fixed?"
    date: 2004-11-02
    body: "I can't use kmail at the moment because I get an error using plain authentication. It's not my system, because using kde from a few months ago worked fine, and the only thing I did was upgrade to KDE cvs about 3 weeks ago. Does anyone know if this will be fixed soon?"
    author: "Thunderbird User"
  - subject: "Re: Does this mean kio_smtp is fixed?"
    date: 2004-11-03
    body: "And you submitted it to bugs.kde.org, right?  (-:\n\nIf you had, you'd be automatically notified when/if it gets fixed."
    author: "Rex Dieter"
  - subject: "Re: Does this mean kio_smtp is fixed?"
    date: 2004-11-03
    body: "There's already a bug open smartass. If you can't say anything informative,keep your sarcy comments to yourself!"
    author: "Thunderbird User"
  - subject: "Re: Does this mean kio_smtp is fixed?"
    date: 2004-11-04
    body: "no need to reply like that.\n\nCiao'\n\nFab"
    author: "fab"
  - subject: "Re: Does this mean kio_smtp is fixed?"
    date: 2004-11-04
    body: "Why not? I asked a perfectly reasonable question, and was met with a saracastic response. "
    author: "Thunderbird User"
  - subject: "Re: Does this mean kio_smtp is fixed?"
    date: 2004-11-04
    body: "I really wish you had left your email address for us to resond.  I didn't want to fill this forum up with junk.\n\nBut your response was totally inappropriate.  Even if you had filled out a bug report, the user was trying to be helpful.  \n\nAny in reply to your problem:  perahps the trouble if your mail server requires you to log into POP3 first and then send mail without a password.  This is the way it is for some SMTP servers.\n\n//Kenny"
    author: "standsolid"
  - subject: "Re: Does this mean kio_smtp is fixed?"
    date: 2004-11-09
    body: "Actually, I think this case is a little different, as it warrented a bug report: http://bugs.kde.org/show_bug.cgi?id=89518.  I too, have be striken down with this bug.  While my mail service does do things like you said, it usually has worked flawlessly in the past until a fatefull \"for amodule in *; pushd $amodule&&cvs up&&popd;done\" I guess I can always check out kdebase/kioslave/smtp from some tag before the change went in.  Thank goodness for a web interface to my mail :)\n\n--Brendan"
    author: "Brendan Orr"
  - subject: "nativ Groupwise or SOAP/IMAP/LDAP interface?"
    date: 2004-11-02
    body: "Is this a native Groupwise connector in KMail like the Groupwise -Java & -Win32 client or a solution like it is implentet in Evolution to connect to a Groupwise server?\n\nIf this will be a native connector, then one of my dreams comes true!! Please, please, please, please made a native connector and I have the best Mail client in our company!!!\n\nPlease people from KDE/Suse, you belong to Novell now and you've all documents! Make this connector perfect!\n\nThank you very much!!!"
    author: "anonymous"
  - subject: "Re: nativ Groupwise or SOAP/IMAP/LDAP interface?"
    date: 2004-11-02
    body: "> Please people from KDE/Suse, you belong to Novell now and you've all documents! \n\nOh, seems I missed something.\nNovell hired the KDE people?\n"
    author: "Volker Paul"
  - subject: "Re: nativ Groupwise or SOAP/IMAP/LDAP interface?"
    date: 2004-11-03
    body: "No, but people from Suse programs for KDE? And Suse belongs to? Sorry, it's the rumour that a lot people from Suse programs for KDE.\n\nSorry, if this is not true. But hopefully the programmers of KMail works at Suse ;-)\n\nDon't want to discuss this one, want to hear how KMail connects to GW, nothing else. But you can see I've some hope...\n"
    author: "anonymous"
  - subject: "Re: nativ Groupwise or SOAP/IMAP/LDAP interface?"
    date: 2004-11-03
    body: "SUSE is a Novell company, so it makes little sense to distinguish between SUSE and Novell.\n\nSUSE is in the process of hiring a new KDE developer to work on Groupwise integration btw. See http://www.suse.de/de/company/suse/jobs/suse_pbu/developer_kde.html"
    author: "Waldo Bastian"
  - subject: "Re: nativ Groupwise or SOAP/IMAP/LDAP interface?"
    date: 2004-11-03
    body: "The GroupWise support is based on IMAP and SOAP."
    author: "Cornelius Schumacher"
  - subject: "Re: nativ Groupwise or SOAP/IMAP/LDAP interface?"
    date: 2004-11-03
    body: "thank you for your answer. a little bit sad, because our company restrictions does not allow us to activate imap/soap. although I'm in the IT-department, my chief think that our \"special\" users will be using different clients than gw-win32.\n\nI don't like the java-client for Linux very much and hoped for Kontact, because this is intuitiv, fast and stable!\n\nBut thanks again for your answer! Bye\n"
    author: "anonymous"
  - subject: "IMAP filter support anyone?"
    date: 2004-11-03
    body: "As soon, as it has client side IMAP4 message filtering, I might try it again :)"
    author: "Dmitriy Kropivnitskiy"
  - subject: "Re: IMAP filter support anyone?"
    date: 2004-11-04
    body: "I'm working on it. ( http://kontact.org/shopping/sanders.php )\n\nI've got a basic prototype working and are focusing on debugging and fault tolerance now.\n\nDon.\n"
    author: "Don"
  - subject: "kmail == Best Mail Client?"
    date: 2004-11-03
    body: "Hello,\ni know it is dangerous to write something like this on a kde site but for me kmail is one of the worst kde application. That doesn't mean that kmail is bad, but compared with the other (real good) kde apps kmail is for me the worst one.\n\nSince kde2 i wait until kmail provides gpg/mime support. gpg/mime is the standard way to use gpg and kmail doesn't support it by default.\nI get a lot of mails from people with kmail which use inline-gpg because that is that was kmail uses by default. This simply sucks.\n\nWhen will kmail support gpg/mime by default?"
    author: "pinky"
  - subject: "Re: kmail == Best Mail Client?"
    date: 2004-11-03
    body: "Depends on your definition of \"default\":\n\nIf you mean \"works as long as you install the required libs\", that works perfectly since KDE 3.3. Most distributors even make kmail depend on those libs, so it's a complete non-issue to the user.\n\nIf you mean \"works build-in\": This will probably not happen. It's not really complicated to install gnupg (note: this does _not_ mean the cryptplug interface which was really complicated). Reason is that we simply cannot be experts, so we rely on gnupg (or gpgsm for that matter), because there is no reason to start over from scratch when there is a perfectly working solution for something as complex as S/MIME and encryption in general.\n\nAll in all, S/MIME works since KDE 3.2 at least, and if your distributor was at least a bit qualified, he made it working for you."
    author: "Daniel Molkentin"
  - subject: "Re: kmail == Best Mail Client?"
    date: 2004-11-03
    body: "thank you for this information.\ni have debian sid and gnupg installed (i use it with mutt).\nBut kmail 1.7 doesn't have an option for gpg/mime.\n\nHave i missed something?"
    author: "pinky"
  - subject: "Re: kmail == Best Mail Client?"
    date: 2004-11-04
    body: "You need GnuPG 1.9 for S/MIME. I don't know if debian packages are available for that."
    author: "Daniel Molkentin"
  - subject: "Re: kmail == Best Mail Client?"
    date: 2004-11-04
    body: "what's the different between pgp/mime and s/mime?\nAre they compatible to each other?\n\n> You need GnuPG 1.9\n\nDebian uses gnupg 1.2.5 and the gnupg homepage (www.gnupg.org) says that 1.2.6 is the lateste version, so what is GnuPG 1.9 ?"
    author: "pinky"
  - subject: "Re: kmail == Best Mail Client?"
    date: 2004-11-04
    body: "GnuPG 2.0 pre. This is the first one to support S/MIME without the need of cryptplug."
    author: "Daniel Molkentin"
  - subject: "Re: kmail == Best Mail Client?"
    date: 2004-11-17
    body: "That's right, KDE made a release that depended on unreleased software. Pissed off every distro's KDE maintainers. What, are they all supposed to switch over to a version of gpg that even upstream won't bless yet?"
    author: "Distro Whore"
  - subject: "Re: kmail == Best Mail Client?"
    date: 2004-11-04
    body: "Hi,\n\nYour defiantly doing something wrong, this has been implemented for ages (at least with my version of SuSE).\n\nLook here for more details on how to get stuff working:\nhttp://kmail.kde.org/kmail-pgpmime-howto.html\n\nand \nhttp://kmail.kde.org - is the Kmail homepage.\n\nSorted ;-)\n\nTony\n\n"
    author: "Tony Espley"
  - subject: "Re: kmail == Best Mail Client?"
    date: 2004-11-04
    body: ">Look here for more details on how to get stuff working:\nhttp://kmail.kde.org/kmail-pgpmime-howto.html\n\ni know this site. But you have to install some packages, do this and that and so on...\nThis is not what i understand if someone says \"kmail has implemented gpg/mime\"\nLook at mutt, evolution and other email-programs, this programs use gpg/mime \"out-of-the-box\" since years!\n\nWhy is it so complicated for kmail?\nI hope that in the future i will be able to simply install gnupg and kmail and kmail will really use out-of-the-box gpg/mime as default method for signing and encrypting mails and not the inline method."
    author: "pinky"
  - subject: "Re: kmail == Best Mail Client?"
    date: 2004-11-04
    body: "How about using a distribution which provides a proper setup to you?"
    author: "Anonymous"
  - subject: "Re: kmail == Best Mail Client?"
    date: 2004-11-04
    body: ">How about using a distribution which provides a proper setup to you?\n\nI think this would be the wrong way.\nWhy pgp/mime works by many other email clients?\nSure, a distribution could make the necessary steps in the howto and offers his users kmail with gpg/mime.\nBut i think the right way would be that kmail would offers by default gpg/mime support if gpg is installed.\nWhy does other mailclients uses gpg/mime with any stable gnupg versions (also older ones) and kmail need some special packages?"
    author: "pinky"
  - subject: "Re: kmail == Best Mail Client?"
    date: 2004-11-04
    body: "> Why pgp/mime works by many other email clients?\n\nBecause their packagers set the right dependencies and compile them with such support?"
    author: "Anonymous"
  - subject: "Re: kmail == Best Mail Client?"
    date: 2004-11-05
    body: "You have to ask the Aegypten team (http://www.gnupg.org/aegypten/) if you want to know why they preferred such a nicely modularized design. Actually, it is GnuPG which is getting much more \"complicated\" (because it's much more modularized). Any mailclient that wants to use GnuPG 2 will have to use it via the gpgme library because using the gpg2 executable and all the helper applications directly will be extremely difficult. That's exactly why gpgme exists (gpgme means \"GnuPG Made Easy\"). And, BTW, KMail doesn't need any special packages. It just needs GnuPG 2 and a recent version of gpgme. It's GnuPG 2 which needs all those other special packages."
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: kmail == Best Mail Client?"
    date: 2004-11-05
    body: "Which distro would that be?  I am using Mandrake 10.1 and it has the same problems as mentioned above.  \n\nI have followed the instructions on the page mentioned above for Kmail 1.7 and have been unable to get it all to work together.  I have tried, read, got help and still I am not able to use anything but the \"deprecated\" inling pgp.  Neither OpenPGP/MIME nor S/MIME are working here.  I am no guru but I have been using Linux for 4 or 5 years but the details of getting this going have me beat and I am at a dead end.\n\nBut I do love Kmail, just keep hoping this gpg stuff gets easier.\n"
    author: "Ken Walker"
  - subject: "Re: kmail == Best Mail Client?"
    date: 2004-11-05
    body: "That would be SuSE. There it already works out-of-the-box since at least SuSE 9.0 (which included KDE 3.1).\n\nI don't remember whether you've already asked for help on kdepim-users AT kde DOT org. If not then you might want to do this."
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: kmail == Best Mail Client?"
    date: 2004-11-04
    body: "> I hope that in the future i will be able to simply install gnupg and kmail\n> and kmail will really use out-of-the-box gpg/mime as default method for\n> signing and encrypting mails and not the inline method.\n\nYou claim to have KMail 1.7? Go to\n  Configure KMail->Identity->Modify->Cryptography\nand select OpenPGP/MIME as you preferred cryptographic message format. Maybe you just looked for gpg/mime (dunno where you got that term from, never seen it before). You might also like to have a look at the output from\n  Configure KMail->Security->Crpyto Backends->Rescan\nto see if you're missing something. Oh, and of course, don't forget to start the gpg-agent, otherwise you can only sign and encrypt, but not decrypt."
    author: "Marc Mutz"
  - subject: "Re: kmail == Best Mail Client?"
    date: 2004-11-05
    body: "Thank you very much for this advice!\nIt really works!\nI have always looked at Security->Crpyto Backends for pgp/mime..."
    author: "pinky"
  - subject: "Not Unique In Saying This But - Great Application"
    date: 2004-11-05
    body: "Well, it has to be said that kmail is a terrific mail client. I have been using it for a few years now and just thought that its clean interface, customisability and functionality were first class. As a bonus I also came across a script called gotmail which meant that kmail could retrieve email from hotmail servers which was pretty kool. Although the script is not specific to kmail, kmail sure did make it easy to run and execute periodically. But a great mail client without doubt. Just feels so solid and \"not fiddly!\"\n\nThanks to all the kmail team and congrats,\nheaddeball"
    author: "headdeball"
---
At this year's <a href="http://www.linuxworldexpo.de/">Linux World Expo &amp; Conference Europe,</a> taking place in Frankfurt, Germany, KDE's mail application <a href="http://kmail.kde.org/">KMail</a> received the <a href="http://www.linuxnewmedia.com/Award_2004/en">Linux New Media Award</a> in the "Best Mail Client" category. The prize was awarded to  "Kontact/KMail" to honor the <a href="http://www.kontact.org/groupwareservers.php">groupware integration efforts</a> done by the KMail developers. KMail maintainer Ingo Kl&ouml;cker was the one to receive <a href="http://ktown.kde.org/~danimo/images/linux_new_media_award_2004.jpg">the award</a>. The Linux New Media Awards are among the most significant international prizes for Linux- and Open Source products. The jury consisted of 100 Linux- and IT experts, among them Linux kernel hacker Alan Cox and Knoppix inventor Klaus Knopper.




<!--break-->
