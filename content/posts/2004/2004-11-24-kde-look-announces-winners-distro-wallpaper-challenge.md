---
title: "KDE-Look Announces Winners of Distro Wallpaper Challenge"
date:    2004-11-24
authors:
  - "Theobroma"
slug:    kde-look-announces-winners-distro-wallpaper-challenge
comments:
  - subject: "kde wallpapers"
    date: 2004-11-24
    body: "BTW, are there any plans to change some of the default backgrounds for KDE 3.4? Some of them are old and ugly, while on kde-look there are lots of good-looking wallpapers."
    author: "Anonymous"
  - subject: "svg images"
    date: 2004-11-24
    body: "Are there many svg backgrounds out there?  With laptops, widescreen monitors and very large monitors becoming more and more common having the four common sizes (640,800,1024,1600) really doesn't cut it anymore."
    author: "Benjamin Meyer"
  - subject: "Re: svg images"
    date: 2004-11-24
    body: "I don't think 640 is so common anymore, but you missed 1280.\n\nThe lack of non 4x3 wallpapers is definately becoming an issue (ignoring that 1280x1024 isn't 4x3 - 1280x960 is).  Stretching things wider generally makes them look silly.  Also, a number of flat screens now available can be rotated by 90 degrees.  This is very nice for working on documents, but a 4x3 picture displayed in 3x4 is either too small or too stretched.\n\nMaybe another alternative would be to keep the picture's aspect ratio, resize it to be at least as wide and as high as the screen, and clip the top/bottom or the sides - a sort of oversized maximise.  I don't think KDE will do that automatically (I'm not at home now, so I can't check).\n\n-- Steve"
    author: "Steve"
  - subject: "Plans for html background"
    date: 2004-11-24
    body: "Ok, im a little oftopic here, but are there any plans to support html background with kwin. I mean real html not the plugin which transforms the url into a large image. It should be pretty easy to implement given the modular stucture of kde?"
    author: "anon"
  - subject: "Re: Plans for html background"
    date: 2004-11-24
    body: "That would be kdesktop then, kwin only does window management. kdesktop draws the desktop."
    author: "Erik Hensema"
  - subject: "Re: Plans for html background"
    date: 2004-11-24
    body: "It has existed for years. Just use kwebdesktop, there is an option for it somewhere under background."
    author: "Carewolf"
  - subject: "Re: Plans for html background"
    date: 2004-11-24
    body: "No it isn't. kdesktop converts the html into an image, this is not the goal. There should be support for everything khtml supports and not less :)."
    author: "anon"
  - subject: "Re: Plans for html background"
    date: 2004-11-24
    body: "Open konqueror.\n\nSet it to \"below other windows\" (window menu -> advanced-> keep below others\n\nGive it focus by minimizing everything else and clicking on it.\n\nNow set it to full screen (F11)\n\nVoila!\n\nOk, so the \"show desktop\" button doesn't do the right thing, but it's close ;-)\n\nYou could replace the \"show desktop\" with some dcop concoction to minimize all other windows (or you can do that from the taskbar)."
    author: "Roberto Alsina"
  - subject: "Re: Plans for html background"
    date: 2004-11-25
    body: "Great, thanks for the hint. It would be nice to have it *really* as background application but this is pretty close. :)"
    author: "anon"
  - subject: "Re: Plans for html background"
    date: 2004-11-25
    body: "On the other hand... why exactly is this useful?\n\nI mean, really, one usecase for a real-HTML-web-browser background. Just curious here."
    author: "Roberto Alsina"
  - subject: "Re: Plans for html background"
    date: 2004-11-26
    body: "kiosk"
    author: "illogic-al"
  - subject: "Re: Plans for html background"
    date: 2004-11-26
    body: "Not really useful.\n\nI assume you mean all that's gonna run is konqueror.\n\nIn such case, full-screen it, disable panel and WM, disable the un-fullscreen action, and that's that.\n\nIt's not on the desktop but you won't see any difference."
    author: "Roberto Alsina"
  - subject: "A Grey Day for Mandrake"
    date: 2004-11-25
    body: "Grey coloured wallpaper for Mandrake... Yuck!"
    author: "Cyber Trekker"
  - subject: "Re: A Grey Day for Mandrake"
    date: 2004-11-29
    body: "in packege are 3 wallpapers, gray, dark and light BLUE :)"
    author: "meNGele"
---
<a href="http://www.kde-look.org/">KDE-Look</a> has just completed our <a href="http://www.kde-look.org/news/index.php?id=136">Distro Wallpaper Challenge</a>. We are pleased at the level of involvement as we have seen 42 distro themed wallpapers submitted in just 11 days. The KDE-Look community voted for their favorite wallpaper for each distro. We had 12 winners and a ton (907,184.74 grams) of great new artwork.


<!--break-->
<p>Some highlights:</p>
<img src="http://static.kdenews.org/fab/interviews/kdelook-distro_wallpaper/kdelook_distro_challenge.jpg"><br><br>

<p>Click <a href="http://www.kde-look.org/news/index.php?id=136">here</a> to see the complete list of winners. Our next challenge will be posted Dec 1st.</p>



