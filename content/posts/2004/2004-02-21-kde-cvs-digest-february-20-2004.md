---
title: "KDE-CVS-Digest for February 20, 2004"
date:    2004-02-21
authors:
  - "dkite"
slug:    kde-cvs-digest-february-20-2004
comments:
  - subject: "Bug bit me"
    date: 2004-02-21
    body: "For what it's worth, I was using disconnected IMAP and I lost my entire mailbox due to a bug not apparantly addressed yet.  Well, about two weeks worth, as of the time of my last backup (done just before I enabled it).\n\nI haven't reported it because I can't provide any useful details plus the fact that there are big scary warnings in the software that disconnected IMAP is experimental.  So, a caveat bit me on my emptor.  Ah, well.\n\nIt was great while it worked, and I look forward to hopefully enabling it in 3.2.1.  It would just be handy when I grab my laptop and work in a diner."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Bug bit me"
    date: 2004-02-22
    body: "Yeah, I've lost mail to it as well.  Three times. I have it all backed up, so perhaps saying I lost it is a bit of an overstatement.  \n\nBut I reported it as a bug the first time, and was somewhat annoyed to see the bug report being closed when I was able to reproduce the loss within five minutes of starting kmail, and I wasn't alone in producing it.\n\nThe code does seem to be getting better but AFAICT, it is just not quite thread safe enough.\n\n"
    author: "Corrin"
  - subject: "Backports to the branch"
    date: 2004-02-21
    body: "There seem to be loads of bugfixes the last few weeks, and I guess most\nof the newer BR, that lead to such fixes, have been made by users using 3.2.\n\nBut it also seems that only a very little part of these great bugfixes goes\nback into the branch. Or did I miss something?\n\nFrom quickly going over the digest (btw, thank you Derek!!) I can mention\nthese (mostly khtml stuff, even some crashes) that have not been going back\ninto the branch:\n\nhttp://bugs.kde.org/show_bug.cgi?id=73214\nhttp://bugs.kde.org/show_bug.cgi?id=74123\nhttp://bugs.kde.org/show_bug.cgi?id=70546\nhttp://bugs.kde.org/show_bug.cgi?id=73920\nhttp://bugs.kde.org/show_bug.cgi?id=69697\nhttp://bugs.kde.org/show_bug.cgi?id=69697\nhttp://bugs.kde.org/show_bug.cgi?id=45673\nhttp://bugs.kde.org/show_bug.cgi?id=74873\n\nAre there plans to keep the branch in a good state, or is all focus on the\nnext release (3.3 or whatever ot will be called)?\n\nBe aware that the branch is what 90+% of the users will be living with for the\nnext 6-x month.\n\nAnyway, KDE rocks and I wanna thank all the hackers for doing such a great job\non it!"
    author: "ac"
  - subject: "KDE 3.2   :-("
    date: 2004-02-21
    body: "Well - as much as I like the new features, there are some\nbugs which are really horrible. I'm currently considering\nreverting back to 3.1.x. I really hope there will be a 3.2.1.\nThese bugs are:\n- Editing PHP files in Quanta is dead slow. Typing is\nnearly impossible,\n- Kate and Quanta always complain about files that have been\nmodified by someone else everytime I try to save without any\napparent reason. They didn't do that in 3.1.\nThese two bugs are very bad. I'm a programmer and need at least\na working editor. It's extremely annoying to confirm the\n\"file has changed\" dialog every time you save. And Quanta\nis impossible to use at this time. I already switched to Kate\nfor the time being.\n"
    author: "Jan"
  - subject: "Re: KDE 3.2   :-("
    date: 2004-02-21
    body: "> I really hope there will be a 3.2.1.\n\nYou're joking, right?"
    author: "Anonymous"
  - subject: "Re: KDE 3.2   :-("
    date: 2004-02-21
    body: "> I really hope there will be a 3.2.1.\n\nof course.. and a 3.2.2, 3.2.3 (probably), etc.."
    author: "anon"
  - subject: "Re: KDE 3.2   :-("
    date: 2004-02-21
    body: "Hello Jan, I am happy to correct you. Editing PHP files in Quanta *was* dead slow. The problem with \"This file has been modified\" is being looked into. AFAIK, some recent changes in both Kate and Quanta (Quanta uses KatePart as it's editor widget) may have fixed these issues.\n"
    author: "Mathieu Kooiman"
  - subject: "Re: KDE 3.2   :-("
    date: 2004-02-21
    body: "To be absolutely precise we found that editing PHP files got particularly slow only under certain conditions. Lots of \"else if\" and other logic structures were involved. Ironically I had 800 and 500 line PHP class files as well as function and settings includes on several hundred line files not causing this problem at all. Note that quanta parses all include/require files with the main file to provide scoped functionality so this added up to thousands of lines. We also tested on files that had caused problems that were up to 10,000 lines. Still this ended up showing up in 300 line files. :-(\n\nThis problem crept in right before release with a parser update that was tested by at least dozens of users on our user list and we were just surprised by it. The parser has been torn apart, sliced and diced and some new slight of hand to manage when and how parsing occurs that enables background parsing has been added. So far tests are good and minor glitches caused by the changes are being removed daily. So far reports are good for problem files and hopefully most issues caused by the changes have been isolated. The parser should not have this problem and it should be very responsive with 3.2.1.\n\nThe file changed dialog was addressed and I don't recall all the issues but I know at least a part of it was a Kate issue that has been fixed. This also seems to be eliminated, though we did just get a report of an unusual sequence of events causing a problem, which should be fixed.\n\nThere are also some cool new things in HEAD that should make it into 3.2.1 like the bold highlighting in the project tree of open files.\n\nPlease keep in mind that Quanta has become a complex application with numerous features and is extremely difficult to test thoroughly. I encourage users who want to see it at it's best to get involved with our user list, test our CVS version (which can be run along side the production release) and submit bugs. \n\nAlso I would like to mention that community support was great last year but seems to revolve around independent releases and press. It's rather flat at the moment and I'm considering whether we can add a new sponsored developer. In fact I hope to, and also our contributing developer numbers have been going up so I hope to be able to produce even greater improvements this year. I think most people would agree that the 3.2 version is a big jump from 3.1 once they get the new parser in 3.2.1."
    author: "Eric Laffoon"
  - subject: "Re: KDE 3.2   :-("
    date: 2004-02-23
    body: "I've noticed the problem in kde 3.2 beta1, but shame on me, didn't posted about it thinking it would be addressed in kde 3.2 final. :(\nThat's because I have a lot of faith in quanta developers, but I'll be more vigilant next time."
    author: "Iuri Fiedoruk"
  - subject: "Re: KDE 3.2   :-("
    date: 2004-02-23
    body: "> I've noticed the problem in kde 3.2 beta1, but shame on me, didn't posted about it thinking it would be addressed in kde 3.2 final. :(\n> That's because I have a lot of faith in quanta developers, but I'll be more vigilant next time.\n\nSo you're who I send the thank you card to. ;-)\n\nI have a lot of faith in our team too. In fact when the first reports came in I thought they were addressing old versions because ironically they weren't showing up for me with a lot of big PHP files. The previous problems with speed in 3.1 did show up. \n\nWe have been diverting most of our effort to a parser restructure for speed, which always requires some debugging. Please report any bugs if you are running CVS HEAD this week so we can make sure 3.2.1 is both fast and clean. We are optimistic, but bug reports rule."
    author: "Eric Laffoon"
  - subject: "Re: KDE 3.2   :-("
    date: 2004-02-22
    body: "> Hello Jan, I am happy to correct you. Editing PHP files in Quanta *was* dead slow.\n\nHaha. Maybe somewhere in CVS. If you download quanta 3.2 *today* from ftp.kde.org you get a quanta unusable for php. I hope kde 3.2.1 (with fixed quanta) will be released soon."
    author: "Yase"
  - subject: "Re: KDE 3.2   :-("
    date: 2004-02-21
    body: "> Kate and Quanta always complain about files that have been\n> modified by someone else everytime I try to save without any\n> apparent reason. They didn't do that in 3.1.\n\nYes, I have this too. It seems to be related to the FTP KIOSlave (try fish or nfs instead) which I imagine has been heavily refactored since some old nasty problems have gone, but some completely new ones have turned up to. For example:\n\n1. The delete key doesn't work for FTP files (you have to use the RMB menu)\n2. FTP connections are stored up at boot so that I often have the\n   ridiculous situation where my web server won't let me open a single FTP\n   connection (ever) because 3 are opened at startup without me asking for\n   them (I have to restart my modem to get a new IP address to fool the\n   server).\n3. The last modified dates are constantly changed (I guess) since other\n   programs warn the files have been modified externally.\n\nI kind of miss KDE 3.1 too. Many bugs are gone but now I have a whole load of new ones to learn, and some are very annoying.\n\nBut I think you more easily forget the new improvements than you do the new bugs. My favourite features in 3.2 are the killing of applications that don't respond to the close button, the themable task bar, and being able to resize the screen resolution on the fly (which BTW has a serious text resizing bug).\n\nI don't know if KDE 3.2 will get fixed much since the nastiest bug in KDE 3.1 (for me) was never fixed though it would have been simple, and yet it was magically gone in 3.2. I guess bugs that cause data loss or crashes are the priority, even though some of the UI bugs are far more annoying."
    author: "Dominic Chambers"
  - subject: "Re: KDE 3.2   :-("
    date: 2004-02-21
    body: "Me too: Quanta always complains that the file has changed, and it has gotten quite a bit slower when editing php files. It is not unusable for me, but when you notice a delay when typing on an athlon 2500+ there is definitely something wrong.\n\nBut nevertheless the decision to release 3.2 was right. I filed plenty of bugreports which I would not have done if they had not released it. And the improved tabs in konqueror alone are worth the upgrade."
    author: "Androgynous Howard"
  - subject: "Re: KDE 3.2   :-("
    date: 2004-02-21
    body: "\"- Kate and Quanta always complain about files that have been\n modified by someone else everytime I try to save without any\n apparent reason.\"\n\nAndras Mantia committed a change to kdewebdev/quanta/src\n\n \nWorkaround for a Kate bug. Might fix those \"File changed\" dialog pops up errors. \nThose who experienced that problem, please test it.\n\n\n:)"
    author: "Pat"
  - subject: "Re: Kate pretty broken"
    date: 2004-02-21
    body: "For us, starting with the RC1 release, Kate became broken and stayed that way - several issues: 1) something is wrong with highlighting, it doesn't always stay there when you cut and paste, 2) if you log out with the desktop saved, Kate no longer reopens any files, 3) .h files are no longer syntax highlighted - hmmm, for some of us, this is the most critical program especially since it is shared with several other programs, Quanta, KDevelop, etc.  It needs extra QC.\n\nbtw - Kpilot is also still broken and won't restore a pilot - We want to really use Kontact in our business but this, coupled with a strange way to create distribution lists, this is killing us.  We will need to decide soon if we can stay with it - hope it can get fixed soon.  "
    author: "john"
  - subject: "Re: Kate pretty broken"
    date: 2004-02-21
    body: "If you would report such bugs on bugs.kde.org, they could eventually even be fixed, the .h files are highlighted here and various fixes for the problem with the \"document was modified\" stuff were backported to the kde 3.2 branch, for being slow, can't say that, kate part in 3.2 feels much faster than the old one for me, php could be an exception, but that could be a problem with the masses of keywords in it (or that quanta does add some parsing overhead)"
    author: "Christoph Cullmann"
  - subject: "Re: Kate pretty broken"
    date: 2004-02-22
    body: "Well, part of the problem is that bug reporting requires both passwords and cookies, (and we just don't allow cookies at all).  It would help for bug reporting to be as easy as possible, like a simple html form.  Does the KDE team practice the \"eat your own dog food\" concept requesting all KDE developers to use KDevelop and Kate rather than vi ? - this may flush out bugs even faster.\n\nbtw re: Kate, the .h file syntax highlighting problem seems to be with \"//\"  which doesn't gray out the line (this is relatively minor problem).  Speed has never been an issue for us. Other bugs can be explained in detail somewhere else (that doesn't require cookies).  "
    author: "john"
  - subject: "Re: Kate pretty broken"
    date: 2004-02-22
    body: "Hello,\n\na .h file is a C header file. In C, there is nothing to gray out for \"//\". What you mean is C++, and .hpp, where I guess, it will work.\n\nYou can always configure Kate to assume that .h files are C++.\n\nYours, Kay"
    author: "Debian User"
  - subject: "\"//\" is a defacto C commenting standard"
    date: 2004-02-22
    body: "Can you name any C compiler that does not recognize \"//\" as the beginning of a comment?\n"
    author: "Vladimir"
  - subject: "Re: Kate pretty broken"
    date: 2004-02-23
    body: "Acztually, the C++ style comments were added in C99."
    author: "AC"
  - subject: "Re: Kate pretty broken"
    date: 2004-02-22
    body: "Once you are registered and have all your issues reported using the \"Help > Report Bug...\" menu entry in the respective application you can send comments to existing reports by email. Cookies for staying logged in on the website are not avoidable at the moment, you will have to request such a feature at bugzilla.org since bugs.kde.org just used highly modified templates while the whole backend is unchanged compared to a stock bugzilla installation. If there are any other issues let me know."
    author: "Datschge"
  - subject: "Re: Kate pretty broken"
    date: 2004-02-21
    body: "> 2) if you log out with the desktop saved, Kate no longer reopens any files\n\nI tried to find your bug report on http://bugs.kde.org to vote for it, care to tell its number?"
    author: "Anonymous"
  - subject: "Kommander"
    date: 2004-02-21
    body: "Kommander looks rather cool, but I was just wondering where it fitted in with KDevelop and QtDesigner regarding development work."
    author: "David"
  - subject: "Re: Kommander"
    date: 2004-02-21
    body: "From Kommander's Readme: \n\"In fact the editor was derived from Qt Designer.\"\n\nBtw, Yeah, we at Kexi Team had some ideas how to reuse Kommander to get Forms framework. Looks like we're about decision to go own way, though, because:\n\n-we need LGPL licensed stuff rather, \n-because of licensing reasons, we cant expose Qt API as Qt Designer does\n-we need a form tools to be a bit easier for newbies (no signal/slots knowledge required)\n\n--\nJarek\n"
    author: "Jaroslaw Staniek (Kexi Team)"
  - subject: "Re: Kommander"
    date: 2004-02-21
    body: "dude, signals/slots *are* easier for newbies.  it'd be ridiculous to get rid of them to make things 'easier'.  sheesh.\n\noh, and why are you guys reinventing the wheel?  TT is breaking qt designer out into a plugin/lib for reuse from apps like Kdevelop.  Why does Kexi have to write another when the existing functionality (not to mention that it is what everyone uses) will be around."
    author: "anon"
  - subject: "Re: Kommander"
    date: 2004-02-21
    body: "I respect your point of view. \n\nI've tried to tell that it is not too clever _forking_ QtDesigner and other GPL apps (yes, Designer's gui editor functionality is designed as GPLed app, not LGPLed lib) to have it glued with Kexi. Looks like \"not forking, if not needed\" in #1 rule in software world and \"reusing\" is only #2 rule.\n\nAnyway, if you're interested in contributing/glueing the external code (and you get the permission for linking with non-GPL), or if you do know anybody who is, please contact us @ #kexi irc channel.\n\nBtw, Forms framework is already in a quite nice stage: http://webcvs.kde.org/cgi-bin/cvsweb.cgi/koffice/kexi/formeditor\n"
    author: "Jaroslaw Staniek (Kexi Team)"
  - subject: "Re: Kommander"
    date: 2004-02-21
    body: ">>TT is breaking qt designer out into a plugin/lib for reuse from apps like Kdevelop.\nWhere can I get more information about this, please?\n"
    author: "John the anonymous"
  - subject: "Re: Kommander"
    date: 2004-02-21
    body: "http://dot.kde.org/1061880936/"
    author: "Anonymous"
  - subject: "Re: Kommander"
    date: 2004-02-21
    body: "> Btw, Yeah, we at Kexi Team had some ideas how to reuse Kommander to get Forms framework. Looks like we're about decision to go own way, though, because:\n\nGee... I wish I'd have heard from you. ;-)\n \n> -we need LGPL licensed stuff rather, \n\nWell, whatever the opinion on that, as the next point goes, I couldn't really do anything about that because it's based on Designer.\n\n> -because of licensing reasons, we cant expose Qt API as Qt Designer does\n> -we need a form tools to be a bit easier for newbies (no signal/slots knowledge required)\n\nActually we are working with making Kommander a lot easier. It is really in an early stage right now. Another factor, regardless of what you do, is that it is now easy to register widgets via a plug in facility for use in Kommander. This means that Kommander could use Kexi widgets as well as hk_classes.\n\nI did not initially think signals and slots were that intuitive, but our implementation aims at creating a simple \"event selection\" with them and in this regard they are not so complex. You can now also use population text and scripts of the language of your choice with them so it gets a lot more interesting. Once a user sees how it works it's not hard.\n\nKommander makes quick mini apps pretty easy and it will get easier as we improve it. So it may be a little in flux right now. Then again I hope to interest more developers in it. Also I hope whatever you do accommodates easy integration for those who want to use Kommander with it.\n\nBTW I'm tickled you guys thought of Kommander... it's nice to get noticed. ;-)\n\nBTW... could you use the ui file format and the Kommander executor if it were LGPL? (not that I can promise a license change but I'm curious)\n \n"
    author: "Eric Laffoon"
  - subject: "Re: Kommander"
    date: 2004-02-22
    body: ">BTW... could you use the ui file format and the Kommander executor \n> if it were LGPL? (not that I can promise a license change but I'm curious)\n\nWe're already using .ui format. So there are no big problems with project data exchangebility.\n\nOne thing important in Form Framework development within Kexi Project, is that it's better to have it available in r/w cvs tree (Kexi's own, or KDELIBS some day), instead of praying for apply patches again and again to make QtDesigner's libs flexible enough for reuse (I've tried to apply more fundamental -but neutral- QKW-related patches to Qt and failed). It looks like, Trolltech needs to keep the control over what Qt Designer is, how it's integrated with incoming Qt versions, since it's a VALUE-ADDED addon to commercial Qt, what I, of course, do understand. \n\nOther issue is that we're trying to make editor parts of the Kexi Form Framework reusable in the editor parts of the Report Framework. To much refactoring would be required for QtDesigner, that is hardcoded for one, particular purpose (ok, a bit less hardcoded thanks to Kommander, thx).\n\nBtw, it's not necessary good decision for QtDesigner's owners to move it to LGPL, because of potentially loose in commercial Qt package's value. So, as KDE users & devs have already terrafic KBabel as QtLinguist replacement, and KControl as (am I funny here?) QtConfig replacement, we can deliver even more replacements (or maybe 'extensions' would be better word).\n\n"
    author: "Jaroslaw Staniek (Kexi Team)"
  - subject: "Re: Kommander"
    date: 2004-02-22
    body: "> Kommander looks rather cool, but I was just wondering where it fitted in with KDevelop and QtDesigner regarding development work.\n\nIt doesn't fit in. As mentioned elsewhere it was created using Qt Designer as the editor, now an older version. It is not intended for developer use as much as for users. It still needs some work to make it more friendly for newbies. It could be incorporated with an application as an extention dialog builder, but it doesn't need to be built in at all. It just needs the application to have the ability to call the command line. Admittedly this can have security issues running dialogs without reviewing the code, but if you're managing dialogs you've built that's different.\n\nFor the user Kommander provides dialogs that can manipulate text and also incorporate more advanced abilities with the scripting language of your choice. There is also DCOP and events with signals and slots. Kommander integrates with KDE apps as well as command line applications. The user purpose for Kommander is getting many of your wish list items now with a little tweaking. This allows for a very custom desktop experience.\n\nFor the developer Kommander provides a means of allowing users to extend an application with scripting and also offers benefits including Kommander dialogs for some functionality. Developers can provide user extensible functionality where it is desirable using a Kommander dialog. It's fast and practical for this use. Developers can also provide temporary updates with Kommander dialogs using scripts since they do not require a compile. These are very friendly to novice users and bandwidth. Developers can also ship a Kommander dialog with an application where the optimum design factors may not be clear, get modified dialogs back from users and build their final design from user tested design input.\n\nKommander does not generally apply to conventional application development but it's unconventional applications can be a rich compliment to applications for users and developers."
    author: "Eric Laffoon"
  - subject: "amaroK"
    date: 2004-02-21
    body: "I've been using the new amaroK for a few days now, it really is pretty neat. I was using beep-media-player but was having stablity issues with it still. amaroK seems pretty solid. The On-Screen Display is a neat feature, it even works while I'm playing Unreal Tournament 2004. And how it fades in and out when you switch tracks sounds pretty professional. The interface is clean and easier to read then most XMMS skins. It has a good balance between looking cool and being usable.\n\nI use KHotkeys to map F10, F11 and F12 to next, pause and next respectively. KHotkeys would not recognize the 'www', 'mail' and 'search' extra buttons on my keyboard though, I was able to use these with XMMS via a plugin.\n\nMy problems with amaroK is that the play button becomes a stop button instead of a pause button (an inactive button would even be better) during playback. Obviously thats just a matter of opinion, but it took some getting used to, I lost my place in songs a few times. Certainly the name of the option should change in the task tray menu. In fact, whats really the point of a seperate stop button?\n\nThe playlist is very clean and easy to use, but it would nice to have a Play This Directory option from right clicking the player or something, since my music is organized by directory, so I wouldn't have to open the playlist dialog at all. Its how I always did it with XMMS, though its not part of the beep-media-player. I guess this is one of those features that could stack up to make things pretty cluttered. "
    author: "Ian Monroe"
  - subject: "Re: amaroK"
    date: 2004-02-21
    body: "Thank you for the kind words :)\n\nWe are very thankful for user feeback, since the application is evolving rather rapidly now, and we're trying hard to keep the app consistent and usable, while adding new features at the same time. The beta series is a great success so far, since we've already got lots of useful feedback, bug reports and inspiration in just one week. This is certainly a great motivation for development.\n\nRegarding these interface issues you are mentioning, please file them on bugs.kde.org, this would be very helpful.\n\nI'd also like to mention that we usually hang out on IRC, server irc.freenode.net, channel #amarok. Come visit us some time :)\n\n\nBye, Mark\n\t (amaroK maintainer).\n"
    author: "Mark Kretschmann"
  - subject: "Re: amaroK"
    date: 2004-02-21
    body: "I'm a big fan of Amarok too, but I think the playlist interface was much nicer looking in 0.8.x. For example, the icon bar on the side look much nicer than KDE's sideways tabs. Plus, on the \"window\" titlebar on the stream/file browser don't look very good, and the up/back/forward/home navigation toolbar seems kind of extraneous. \n\nI'd file a bug report, but this isn't really a bug, just a gripe :)\n"
    author: "Rayiner Hashem"
  - subject: "Re: amaroK"
    date: 2004-02-21
    body: "> For example, the icon bar on the side look much nicer than KDE's sideways tabs\n\nagreed.. kmultitabbar aren't exactly the prettiest. I think that the tabs themselves are fine, but the tabbar isn't. There is a weird bevel under the tabs that looks like a off-by-one error, but probably isn't. If that were removed, it would look a bit better."
    author: "fault"
  - subject: "Re: amaroK"
    date: 2004-02-23
    body: "But the Janus Widget used a lot of space. We planned to use a multiTabBar from the beginning really. Sorry it's not to your tastes :(\n\nWe will likely refine the FileBrowser interface as currently it is very cutn'paste. However removing back/forward is probably not going to go down well with users. It is very KDE to have them there.\n\nStill thanks for the feedback Rayiner, I generally find your comments astute and worth reading :)"
    author: "Max Howell"
  - subject: "Re: amaroK"
    date: 2004-02-22
    body: "I was poking around with some of the other players, the play button works the same in all of them. I guess its a hold over from CD players, which sometimes use electricity to remain in a paused state or tape players where stop and play have to be separate buttons. Really, I think if you were to sit down and decide what set of buttons you would need, it would be a Play button that turned into a Pause button while the music was playing, a Stop button (though how often do you need to return the beginning of the song? I guess some people do.) and the next and previous buttons. Having a Play button that stops the music and returns the position to beginning of song and a Stop button that does the same thing is repetitive."
    author: "Ian Monroe"
  - subject: "Re: amaroK"
    date: 2004-02-22
    body: "Very very neat app. :-)\n\nOne question. Howcome you didn't make a noatun GUI-plugin instead?"
    author: "OI"
  - subject: "Re: amaroK"
    date: 2004-02-22
    body: "Amarok isn't very useful for me because I can't play .mpc files which are also common.\n\nI need a program like iTunes with support for covers *AND* have a rating system. And don't have many stability issues. And runs under Linux. Sad, I wish I have more time for some Python lessons...\n"
    author: "anonymous"
  - subject: "Re: amaroK"
    date: 2004-08-13
    body: "Got KHotKeys to Play/Pause. It is actually quite simple: Use the KHotKeys utility to define you Multimedia 'Play' key to use an action type of Keyboard Shortcut -> DCOP Call (simple).\n\nThen define the Keyboard shortcut in that tab (by clicking on the button and then pushing your Play/pause button). Next go to the DCOP Call Settings tab and put in the following:\n\nRemote Application: amarok\nRemote Object: player\nCalled function: playPause()\n\nClick \"Try\" to test it (with amarok running and possibly playing) then your tunes should pause and play just like with XMMS. \n\namaroK RoKs! Nice work dev team. I agree with the previous post about the tabs on the playlist - they are a bit nasty - hopefully KDE will improve them and amaroK can continue to use them. "
    author: "chief"
  - subject: "On question"
    date: 2004-02-22
    body: "Is it possible to convert MsStyles themes to KDE themes?\nHas anyone expiriences on this issue?"
    author: "Gerd"
  - subject: "3.2 in debian unstable"
    date: 2004-02-22
    body: "It seems 3.1.5 has moves to sarge, but still no 3.2 in unstable...\n\nWhen will it be there?"
    author: "ac"
  - subject: "Re: 3.2 in debian unstable"
    date: 2004-02-24
    body: "http://wiki.debian.net/?DebianKDE\n\nShort answer: it's in experimental, will go into unstable once packaging issues are worked out and all of 3.1.5 makes it into testing."
    author: "Ralph Jenkin"
  - subject: "I really hope!"
    date: 2004-02-22
    body: "I am really hoping that KDE 3.2.1 will be released soon, perhaps by the start of March, Lindows will use KDE 3.2 and they are planning on a March or early April release and I don't wan ttheir users to be dissapointed with 3.2. 3.2 is jsut too buggy in its 3.2.0 form."
    author: "Alex"
  - subject: "Re: I really hope!"
    date: 2004-02-22
    body: "My forecast: 9th March 2004"
    author: "Anonymous"
  - subject: "Commit to KDE_3_2_BRANCH!"
    date: 2004-02-23
    body: "I am using KDE-3.2 and I find khtml to be not even RC quality. Most\nthings are working ok but I use konqueror as my main web browser\nand the stability of rendering is very poor. I already opened a number\nof cases but I guess the crash cases are being handled first. I understand\nthat there is a lot of Safari work going on but when a release is made it\nshould be at a better quality than this. Most users like myself take a\nrelease to be stable and with minimal number of problems, and switch\nto it. Hope someone hears!"
    author: "Sammy Umar"
  - subject: "Re: Commit to KDE_3_2_BRANCH!"
    date: 2004-02-28
    body: "I've actually found the rendering of Konqueror to be surprisingly good. The only thing that stumps it are pages that also stump Mozilla.\n\nWhat on Earth is 'stability of rendering'? If Konqueror is crashing on you then it isn't happening here, so perhaps look elsewhere for your problems."
    author: "David"
---
In <a href="http://members.shaw.ca/dkite/feb202004.html">this week's KDE CVS-Digest</a>:
<A href="http://valgrind.kde.org/">Valgrind</A> gets a heap profiler. 
<A href="http://edu.kde.org/kstars/">KStars</A> can show the sky objects' distance from earth. 
<A href="http://kopete.kde.org/">Kopete</A> has refactored password and KWalletManager code. 
Many bugfixes in khtml, Kopete and <a href="http://kmail.kde.org/">KMail</a>.

<!--break-->
