---
title: "KDE Community World Summit: Call for Papers"
date:    2004-05-08
authors:
  - "kpfeifle"
slug:    kde-community-world-summit-call-papers
comments:
  - subject: "Swpat"
    date: 2004-05-08
    body: "Next week is a swpat action week. Around 16. council vote. However it is very likely that the judicial insitutions will pass their coup d'etat via the council. That would mean that we have to to focus on the second European Parliament reading in September. It would be very helpful if your event could also be used to send a strong message.\n "
    author: "gerd"
  - subject: "KDE-3.3 Release Planned For \"aKademy\" (Aug. 18th)"
    date: 2004-05-08
    body: "What an exciting KDE summer!\n\nCoolo is planning KDE-3.3 release for just 3 days before \"aKademy\" starts:\n--> http://developer.kde.org/development-versions/kde-3.3-release-plan.html <--\n\nI can't wait this summer to approach!"
    author: "KDE-3.3 Release Planned for \"aKademy\""
---
The organizing team for the <a
href="http://conference2004.kde.org/">KDE Community
World Summit</a> is asking for submissions of <a
href="http://conference2004.kde.org/cf-pap2004devconf.php">talks</a>
and <a
href="http://conference2004.kde.org/cf-present2004userconf.php">presentations</a>
to the big KDE event in August. Deadline is 30th of May.

<!--break-->
<ul>
<li>
For papers and talks at the <A href="http://conference2004.kde.org/devconf.php">"DevConf"</A> 
part of the event (weekend 21st/22nd of August) see the <a
href="http://conference2004.kde.org/cf-pap2004devconf.php"><em>Call
for Papers and Talks</em></a> regarding some
guidelines. The program committee for this part is
composed of Matthias Ettrich (Initiator of the KDE Project, Norway),
Kalle Matthias Dalheimer (President KDE e.V, Sweden),
Eva Brucherseifer (KDE e.V. Board Member, Germany) and
Scott Wheeler (KDE Developer, USA).
</li>

<li>
For presentations at the <A href="http://conference2004.kde.org/userconf.php">"UserConf"</A> part
of the event (weekend 28th/29th of August) see the <a
href="http://conference2004.kde.org/cf-present2004userconf.php"><em>Call
for Presentations</em></a>
for guidelines. The program committee for the UserConf is
constituted by Tom Schwaller (Chairman IBM Linux Desktop
Technical Leader for Europe/Middle East/Asia),
Kurt Pfeifle (KDE Contributor), Daniel Molkentin (KDE Core Developer)
and Klaus Knopper (KDE User, Creator of Knoppix and Chairman of LinuxTag e.V.).
</li>
</ul>
</p>

<p>
Please consider to contribute. If you know someone who
in your opinion should talk there, but who may be too
shy to step forward, please encourage the person. The
program committees are also open to suggestions for
"invited talks".
</p>



