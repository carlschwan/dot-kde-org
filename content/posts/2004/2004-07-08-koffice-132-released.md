---
title: "KOffice 1.3.2 Released"
date:    2004-07-08
authors:
  - "ngoutte"
slug:    koffice-132-released
comments:
  - subject: "Nice!"
    date: 2004-07-08
    body: "That's really nice =)\n\nKoffice is the only office suit I need.\n\nKoffice team - Keep up your fantastic work, I admire what you've done so far."
    author: "David"
  - subject: "How many users?"
    date: 2004-07-08
    body: "I use KOffice on a daily basis.  All my invoices, memos, hour tables, scheduals, etc. are all done using KWord and KSpread.  It does everything I need.\n\nWho else uses it as their primary Office package?"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: How many users?"
    date: 2004-07-08
    body: "When I'm stuck on windows I use StarOffice 7, but on Linux I use KOffice all the time. I can't wait for it to mature a bit more; it'll be perfect for my needs."
    author: "Mikhail Capone"
  - subject: "Re: How many users?"
    date: 2004-07-08
    body: "I do!!! Its my only office. I only use kword, dont really need the others...\n\nanyway, filed to bugs the last 2 days, and they both got fixed today so I like the dev's even more (david faure especially)."
    author: "superstoned"
  - subject: "Re: How many users?"
    date: 2004-07-09
    body: "Jeez.  Don't be crazy, dude.  I just tried it again, and KWord showed off six distinct and serious bugs before hanging -- all within about two minutes.  I saw a page of my document for a brief moment before things started going wrong.  The last thing I saw before it hung was that my 3-page test document had been screwed up into a 31-page document!\n\nSo I tried KSpread, just before giving up, and saw that it comes with templates for Student ID Cards(!?!)  If the KSpread developers don't know their app from a drawing program, I'm not even going to try putting serious data into it.\n\nIt's a shame.  I love KDE, and it'd be nice to have a lightweight office package to use with it.  Especially since OOo's database/bibliography stuff crashes every time on PPC.  Back to OOo for me, though."
    author: "Lee"
  - subject: "Re: How many users?"
    date: 2004-07-09
    body: "Yeah, and you probably tried to import some heavyly formated document maybee even saved in some kind of undocumented proprietary format. You fond a few bugs in one of the import filters, so what. Next time try to use the program doing real work, write some new documents. Then come back with and tell how KWord behaves doing real work in the real world. Importing docs are not a test of the quality of the wordprocessor, neither OO or MS-Word can import Lyx and KWord files so they must be crap, right.\n\nAs for the templates in KSpread, did you try them, how was the outpt?"
    author: "Morty"
  - subject: "Re: How many users?"
    date: 2004-07-09
    body: "I've been using KOffice semi-regularly since the 1.1 branch.  I don't even use the filters --- I use it only to create new documents --- and I would not under any circumstances use it for any critical data.  KSpread, KWord, and KPresenter are OK for casual use but they have too many bugs.\n\nE.g.: try inserting twenty rows in a medium-sized KSpread document.  By the tenth or twelfth row, the app will start hanging for about a minute (or ten) each time you press the \"insert row\" button.\n\nWYSIWYG in KOffice in general is a joke.  The font kerning for TrueType fonts in printed output is incredibly ugly.  Qt4 may fix this; I'm not sure.  In the meantime it's simply unacceptable.\n\nTry embedding a spreadsheet table inside a presentation.  Now try viewing the slide full-screen, in presentation mode.  The cells won't get scaled up to proportional size.  Embedding of spreadsheets in presentations is useless.\n\nKOffice chart functionality is a joke.\n\nTry creating a KPresenter document with large, high-quality imported PostScript graphics on each page.  Now try running a screen presentation.  Like that ten-second pause when you flip between slides?\n\nNow, I'm not complaining.  KOffice is a complete free office suite developed by a very small number of people, and given those constraints they've done very well.  But the suite is nowhere near production-quality, and it's counterproductive to pretend that it is."
    author: "Anonymous Idiot"
  - subject: "Re: How many users?"
    date: 2004-07-10
    body: "Everything you said is a joke. Your grammar is a joke.\n\nDo you like me telling you this ? Others don't, so, please, don't. I can understand \"this doesn't work, this other thing doesn't work either\". But calling parts of a program a joke is violent and as you said, this is being done for free, I don't think the authors of this programs deserve that violence."
    author: "Pupeno"
  - subject: "Re: How many users?"
    date: 2004-07-10
    body: "Yes, you're right -- \"joke\" was too strong a word.  But I really wasn't trying to make ad-hominem attacks.  Someone was suggesting that KOffice was ready for serious use, and I think that's really an irresponsible thing to say at this point in its development, so yes, I thought it useful to point out that it's really not there yet.  My objective was constructive.  I'm sorry if you didn't take it that way."
    author: "Lee"
  - subject: "Re: How many users?"
    date: 2004-07-12
    body: "I was not suggesting that it was \"ready for serious use\".\n\nI merely said that I use it day to day in a business environment and am perfectly happy with it.  YMMV.  You can tell me what it is missing or what is broken, but that doesn't change the fact that I invoice several dozen clients twice a month with it, use it to manage timetables and hourly rate sheets, send memos, type letters and conduct all of my daily business with it.\n\nI am not you; I have neither your requirements nor your experiences with it (KWord and KSpread neither crash nor slow down for me and haven't for quite a lot time, version wise.  I use the SUSE rpms).  I claim neither that it would be good for you to use nor that you should use it.\n\nBut it *does* work for me.  "
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: How many users?"
    date: 2004-07-12
    body: "Fair enough.  I'm glad it works out for you :)"
    author: "Lee"
  - subject: "Re: How many users?"
    date: 2004-07-10
    body: "hahhah, never mind.  I didn't write the grandparent.  Got mixed up, sorry ;)"
    author: "Lee"
  - subject: "Re: How many users?"
    date: 2004-07-09
    body: "Heavily formatted?  It consisted of a two-column table over multiple pages, with some bold and alignment.  I consider that minimal formatting.\n\nSome of my other documents are more like technical manuals, with footnotes, endnotes, TOCs, indices, cross-refs, sub/superscript, graphs, etc.  I'd be very afraid of doing something like that on KOffice, and to me, that's still minimal formatting compared to the DTP-like things some people enjoying doing with wordprocessors."
    author: "Lee"
  - subject: "Re: How many users?"
    date: 2004-07-10
    body: "> Heavily formatted? It consisted of a two-column table over multiple pages, with some bold and alignment. I consider that minimal formatting.\nIf it wasn't KWord's native format, it is probably heavily formatted. If it was a propietary undocumented weird format, it is definitely heavily formated. So, what's the format ?"
    author: "Pupeno"
  - subject: "Re: How many users?"
    date: 2004-07-10
    body: "It's an OOo 1.1 file.  But I've already described the formatting.  Please don't tell me how my documents are formatted.\n\nPerhaps you mean that the file is COMPLEX?  If so, that's still an import filter issue -- it shouldn't be so hard to ignore unsupported features in OOo's XML file format.  But I doubt that, since others have mentioned that KWord has table problems, and I've looked at the bug database to see them for myself.  Also, around 1 in 3 or 4 bugs are crashing bugs for KWord.  That's pretty bad, imho.\n\nOr perhaps you meant that the import filter generates complex formatting all on it's own.  Either way, the document's formatting is simple."
    author: "Lee"
  - subject: "Re: How many users?"
    date: 2004-07-10
    body: "Multi-page tables are not supported. :-(\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: How many users?"
    date: 2004-07-09
    body: "Did you post a (several) bug report(s) with your test file as attachment ?\nI hope you did, as it seems you have some good test file. I suppose it is a KWord or at least OASIS file though, or it would be useless."
    author: "Ookaze"
  - subject: "Re: How many users?"
    date: 2004-07-09
    body: "Well, the document is private, so that's not an option.  I could make a test case, I guess, but honestly... when THAT much is going wrong, I expect them to see it by looking at their own code.  Even a recent KOffice review mentioned the bugginess of table support, for instance, and it's VERY buggy.  I later tried again, filling a few cells on a table from a blank document, and it did similarly horrific things."
    author: "Lee"
  - subject: "Re: How many users?"
    date: 2004-07-09
    body: "IF people are willing to help and send bug-reports and otherwise contribute in a way they can, I do think that we will have a better Koffice package at the end of the day.I think ONE way to do this, is to come with constructive ideas or criticism!"
    author: "David"
  - subject: "Re: How many users?"
    date: 2004-07-09
    body: "As I say, these things are obvious bugs that anyone can find in two minutes of testing.  When it's reasonably reliable, reasonably ready, you can expect people to start using it and submitting bugs for the glitches they find.  Until then, it's up to developers to make it worthy of testing."
    author: "Lee"
  - subject: "Re: How many users?"
    date: 2004-07-09
    body: "True.\n\nKOffice already has the functionality that most people want.\nThe most important step now is to make all that functionality work, I guess this bug-fix release is one good step in that direction.\n\nGo up until 1.3.6 or something and then release the 1.3.7 as 2.0.\nWithout the bugs, KOffice would kick ass.\n\n;-)"
    author: "Axel "
  - subject: "Re: How many users?"
    date: 2004-07-10
    body: "No, no, 2.0 will not come from the 1.3.x branch.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: How many users?"
    date: 2004-07-10
    body: "That's interesting. What branch would it come from then?"
    author: "Axel"
  - subject: "Re: How many users?"
    date: 2004-07-10
    body: "HEAD"
    author: "Anonymous"
  - subject: "Re: How many users?"
    date: 2004-07-10
    body: "Yes, but do not hold your breath. :-)\n\nCurrently it is still a planned KOffice 1.4, to be released at the earliest at autumn 2005 (yes, next year). May be it will be then renamed as KOffice 2.0 before release, but it depends on how many of the goals are reached.\n\nSee also the comment at the start of the feature plan: http://developer.kde.org/development-versions/koffice-features.html \n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: How many users?"
    date: 2008-01-17
    body: "So you are implying that YOU, the one with the problem, and having obtained the program as free software, can not be bothered to make things easier for the developers to fix the problem, BUT the developers, who MADE the program and GAVE it away for free and freely, should take a look at the code \"in a couple of minutes\", and fix it for you.\n\nWhy should they bother, when you don't have time to bother yourself? Because they want to make a better program for people like you, who criticize but don't feel like contributing back?"
    author: "I\u00f1aki Silanes"
  - subject: "Re: How many users?"
    date: 2004-07-09
    body: "Let me guess: you have a table in your KWord document.\n\nAs for the Student ID card, I do not see what is wrong with it. Someone made this template, then why not ship it with KSpread.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: How many users?"
    date: 2004-07-09
    body: "Well, for one thing, it's misleading for new users.  Sometimes it helps to illustrate problems by exaggerating the case in question.  So let's imagine that KDE has a task-oriented wizard that asks you what you want to do.  You choose \"make an ID card\".  KDE brings up a spreadsheet.  Does that make any sense to you?  IMHO, it really should make no sense at all.  The application that comes up should be a graphical one, with a simple, task-oriented interface without too many irrelevant options.  Certainly, mathematical functions would not belong to the task at hand.\n\nIt's also limiting to them.  Unless KSpread has all the power of Karbon, or whatever other app might be more appropriate, there is no reason at all to make them start their work in an application which may lack more advanced features they want later, such as professional graphic file format exporting.\n\nMost of all, though, it's just plain weird ;D"
    author: "Lee"
  - subject: "Re: How many users?"
    date: 2004-07-09
    body: "Me too. I write all my memos, bills, etc with it.\nEven loveletters ...\nWriting them with winword instead I wouldn't be married yet ):-\n\nWorks fine\n\nregards\ntf."
    author: "thefrog"
  - subject: "Re: How many users?"
    date: 2004-07-12
    body: "Heh.  A couple days ago I made up a big batch of spices (rosemary, garlic, sage, onion, salt and pepper) for my girlfriend's favorite chicken dish, bottled them and then used KWord to make up fancy parchment labels with her picture on them that I glued on the bottles and gave to her as a gift.  We're moving in together in a couple months, so we've been exchanging lots of romantic domestic gifts.\n\nI wonder if the KOffice team ever envisioned *that* use of KWord."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: How many users?"
    date: 2004-07-09
    body: "KOffice works fine for me.  Runs much better than OOo on my old Pentiums and has minimal crashes.  Any thing that I need to transfer to Windows is easily done in PDF format.  No complaints here!"
    author: "Armanox"
  - subject: "Re: How many users?"
    date: 2004-07-10
    body: "I use it a lot... and for my extremely technical needs, I use docbook. My girlfriend use it as well and she loves the UI, she said that everything is at hand and far more understandable that others, like M$ Office and OpenOffice."
    author: "Pupeno"
  - subject: "Re: How many users?"
    date: 2004-07-20
    body: "I use it for all my personal office work and some of my professional. I find it simple, fast, clear, well integrated and usually nicely designed. Kword's frame concept is very powerful and with a bit of cleaning up, should be very good. Kspread is good enough for the very little work I do with it but I can understand some people still find it limited compared to competitors.\nFilters are probably the weak spot right now, though quickly improving. I guess that this problem is the reason for many bad experiences. But when you're creating a document from scratch in the native formats, things usually go well.\n\nRichard."
    author: "Richard Van Den Boom"
  - subject: "Something that caught my eye"
    date: 2004-07-08
    body: "Notable in the changelog is the HOWTO on editing a KWord document using shell scripting.  Neat.\n\nhttp://www.koffice.org/developer/dcop/"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Something that caught my eye"
    date: 2004-07-10
    body: "one question.... why?\nare we easing the viri makers live here?"
    author: "Mark Hannessen"
  - subject: "Re: Something that caught my eye"
    date: 2004-07-10
    body: "That is out-of-document scripting with DCOP. That is not a document which can change itself.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Thanks!"
    date: 2004-07-08
    body: "Thanks, good stuff.\n\nIt 's nice to see Koffice coming along so nicely. I almost wish that Apple would open its eyes and pour some resources like they did with Khtml. \n\nWe could certainly use the help and it would help Apple break its dependency on Microsoft Office. \n\nKOfffice is the right office suite for any Linux/unix desktop, because it is light-weight and well integrated. It follows greatly the idea of small components that work well together."
    author: "Gonzalo"
  - subject: "Re: Thanks!"
    date: 2004-07-09
    body: "Apple is not totally dependant on Microsoft Office.\n\nI can prove that OS 9 + 9.2 come with Apple Works (formerly Claris Works Office)\nApple Works has many of the same components as Microsoft Office"
    author: "Armanox"
  - subject: "SuSE 8.2 RPMs"
    date: 2004-07-09
    body: "When I noticed that there were no SuSE 8.2 RPMs available for KDE 3.3 Beta1 it didn't worry me much. It's just a Beta after all, so it seems ok to cut on packaging efforts. But for KOffice 1.3.2 the same happened, only SuSE 9.0 and 9.1 rpms, nothing for 8.2. \nAnyone knows if the policy of maintaining KDE RPMs for the last 3 versions of SuSE has been changed? Can we expect SuSE 8.2 RPMs for KDE 3.3 stable?"
    author: "Strasky"
  - subject: "Re: SuSE 8.2 RPMs"
    date: 2004-07-10
    body: "KDE releases only source. We depend on other people to create binary packages. So if the person packaging SuSE does not make packages for SuSE 8.2, there is little that we can do against it.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: SuSE 8.2 RPMs"
    date: 2004-07-10
    body: "Opsss I know that, I should have made that more clear in my question. I wanted to ask if anyone knew if *SuSE*s policy had been changed."
    author: "Strasky"
  - subject: "PDF"
    date: 2004-07-10
    body: "Hey, this app has a pdf-importer and exporter.\n\nNot even MSWord can do that."
    author: "Alex I "
  - subject: "Re: PDF"
    date: 2004-07-10
    body: "Every KDE has a PDF exporter since you can print to PDF format ;)"
    author: "Pupeno"
  - subject: "Sorbian"
    date: 2004-08-31
    body: "Sorbian, wow! Luther thought it was not worth to translate the Bible to sorbian as they would disappear soon. :-)\n\nSorbian, Low Saxon, yes, this is real progress. But it shall get support by the government."
    author: "gerd"
---
The <a href="http://www.koffice.org">KOffice</a> team is happy to bring you the second bugfix package that builds upon the successful 1.3 version. Many bugs have been fixed and <a href="http://www.koffice.org/filters/">the filters</a> from/to OpenOffice.org Impress and from/to OpenOffice.org Calc have been enhanced. As preview, there is a new language: Upper Sorbian. See the <a href="http://www.koffice.org/releases/1.3.2-release.php">release notes</a> and the complete <a href="http://www.koffice.org/announcements/changelog-1.3.2.php">list of changes</a>.


<!--break-->
