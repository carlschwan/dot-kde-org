---
title: "Celebrating Qt 3.3"
date:    2004-02-19
authors:
  - "numanee"
slug:    celebrating-qt-33
comments:
  - subject: "Kudos to Trolltech"
    date: 2004-02-18
    body: "I've playied with qt3 some time ago. I did stopped when my null knowledge of C++ and my little knowledge of C showed me that I could not place the carriage before the bulls :)\n\nBut even like this, it showed to be a really good tool, with very decent documentation and lots of usefull functions. If I where going to write GUI apps today, I would use it without any doubt.\n\nI hope borland upgrades kylix to a more recent version of QT soon too."
    author: "Iuri Fiedoruk"
  - subject: "Re: Kudos to Trolltech"
    date: 2004-02-18
    body: "> I hope borland upgrades kylix to a more recent version of QT soon too.\n\nIsn't Kylix dead?"
    author: "yg"
  - subject: "Kylix dead?"
    date: 2004-02-18
    body: "It's not official but it seems like there won't be another version. Borland unforuntaley changed their focus to .NET now. :-("
    author: "Christian Loose"
  - subject: "Re: Kylix dead?"
    date: 2004-02-19
    body: "Qt now has it's own .net variant, is it not possible that Borland is just waiting for it to stabilize and then use this for the next Kylix? It would\nseem like a logical thing to do for Borland."
    author: "NotAProgrammer"
  - subject: "Re: Kudos to Trolltech"
    date: 2004-02-19
    body: "The language I prefer to use when developing apps with QT3 is Python, using the PyQt bindings.  :)\n\nI used to be a Borland Delphi fan back in my younger Windows days, and I really missed having a nice, easy-to-use language alongside a nice, easy-to-use GUI designer when I started using Linux full time.\n\nThen I discovered QT, which I began using to develop C++ applications.\n\nThen I discovered Python, and I've never touched C++ again, since the types of applications I write do not necessitate using it.\n\nI've never had so much fun developing GUI apps!"
    author: "Matthew Scott"
  - subject: "Re: Kudos to Trolltech"
    date: 2004-02-19
    body: "yes! python absolutely rules for anything that does not need to be as fast as possible. the python/Qt combination is especially good. Qt does all the cpu-intensive stuff for you. I've programmed in almost any language, and as Eric Raymond puts it: 'python is the best programming language for transforming pure thought into action.' I cannot recommend it enough to anyone, from beginners to expert programmers."
    author: "mark dufour"
  - subject: "Re: Kudos to Trolltech"
    date: 2004-02-19
    body: "I know what you mean, I still have hope someone will develop PHP bindings for QT as there is a gtk one (gtk.php.net). :)\nBut I'm really starting to like C (didn't managed to program with C++ yet) while doing some small SDL games, so maybe when I learn C++ I won't want this anymore."
    author: "protoman"
  - subject: "Re: Kudos to Trolltech"
    date: 2004-02-20
    body: "Full ack. Developing in python for my was just 3x times faster as developing in C++. For example, just dropping the nasty Makefile.foo/autostuff was a delight and an increase in productivity."
    author: "Philippe Fremy"
  - subject: "QT"
    date: 2004-02-18
    body: "Trolltech was always leading in documentation. A whitepaper of Qt at CeBit convinced me on the spot. Real substantial, no solution blabla."
    author: "Holler"
  - subject: "Re: QT"
    date: 2004-02-19
    body: "What do you mean by \"no solution blabla\"?"
    author: "Anonymous Custard"
  - subject: "QT plug"
    date: 2004-02-18
    body: "yeah, QT is great.  i love it.  its the only thing i'll use (if i gotta choice) to write gui apps.  i've tried and hated MCF and Swing.  i heard cocoa for mac is good, but screw it, i'm a die hard QT fan now...(plus its not cross platform)."
    author: "clockworks"
  - subject: "Re: QT plug"
    date: 2004-02-18
    body: "QT? QuickTime?"
    author: "Anonymous"
  - subject: "Re: QT plug"
    date: 2004-02-18
    body: "Heh.\n\nI'm a PhD student. I use C++ and QT on a regular basis. A few weeks after telling my supervisor this, he writes an image processing toolkit and helpfully names it QT, short for CUTIE (I'll only say that the acronym involves the initials of the university). Absent minded :)\n\nAnd he refuses to change it. The students are already very confused, and my thesis is going to be really, really amusing to write."
    author: "Dawnrider"
  - subject: "Re: QT plug"
    date: 2004-02-19
    body: "Lucky for you, Trolltech's product is called Qt rather then QT (which is used for QuickTime, BTW)."
    author: "Andr\u00e9 Somers"
  - subject: "Re: QT plug"
    date: 2004-02-19
    body: "Yes Cocoa is Cross-platform.  Cocoa adheres to the Openstep Spec which GNUstep adopted and is closing in on a very nice development environment.\n\nWhat is lacking right now for gcc-non-apple is listed in the gcc3.4 changelogs.\n\nQt is no Openstep but it's getting there.  Cocoa has advanced Openstep dramatically and ObjC has been updated quite a bit.\n\nDownload the ObjC latest PDF from Apple and learn it.  GNUstep will catch up."
    author: "Marc J. Driftmeyer"
  - subject: "Re: QT plug"
    date: 2004-02-19
    body: "Its out-dated. Its old-fashioned. Even apple is turning to Qt these days. Ok, maybe you have interests of historical nature (some programmers still play with GEM - why not?) but KDE was always a future-oriented game. :-)"
    author: "Ruediger Knoerig"
  - subject: "Re: QT plug"
    date: 2004-02-19
    body: "> Even apple is turning to Qt these days.\n\nPlease explain."
    author: "Anonymous"
  - subject: "Re: QT plug"
    date: 2004-02-20
    body: "Apple turning to Qt? I believe they used a bit of Qt in Safari, but that's mostly because they were using khtml. The front end is straight Obj-C/Cocoa, as is the API."
    author: "David Smith"
  - subject: "Re: QT plug"
    date: 2004-02-20
    body: "They created the KWQ adapter library rather than using Qt."
    author: "Anonymous"
  - subject: "Re: QT plug"
    date: 2004-02-20
    body: "I agree.\n\nI think KDE, in all respect, is a very good desktop environment, but GNUstep feels much more than just a desktop environment.\n\nSometimes I feel the best thing to do would be to create a complete free software based operating system instead of free separatable modules (like the Linux kernel, the X Window system, KDE, Debian package management system and more). They are great individual projects/modules, but I feel they don't interact too well.\n\nMaybe the best solution would be to implement some way of a free clone of Mac OS X, but maybe make it even better.\nMaybe it's possible to port all the KDE software to the GNUstep architecture; Qt/Mac exists, or maybe port Qt/X11 to use the GNUstep libraries."
    author: "Anonymous"
  - subject: "Crossplattform tutorial?"
    date: 2004-02-19
    body: "Is there any tutorial for doing cross-compiling e.g. to Win32 from a linux box?"
    author: "Ruediger Knoerig"
  - subject: "Re: Crossplattform tutorial?"
    date: 2004-02-19
    body: "Though not ideal, you can use WINE.\n\nWe use WINE and Microsoft Visual C++ 6.0 (nmake) on a linux box to do the automated nightly Psi/Windows CVS builds."
    author: "Justin Karneges"
  - subject: "Re: Crossplattform tutorial?"
    date: 2004-02-20
    body: "Did you use the \"normal\" linux installation of Qt and g++? It would be nice if someone could provide a demo package since this would be a good point to start with."
    author: "Ruediger Knoerig"
---
With the release of Qt 3.3 some days ago, <a href="http://www.itmanagersjournal.com/">IT Manager's Journal</a> recently <a href="http://www.itmanagersjournal.com/software/04/02/03/235204.shtml">featured an interesting article</a> on <a href="http://www.trolltech.com/">Trolltech</a>. <i>"How's this for a backhanded yet powerful endorsement of a company's products? Pixar Animation Studios, creator of such movie hits as "Finding Nemo," "Toy Story," and "Monsters Inc.", uses Trolltech's software throughout its operation but won't talk about it publicly because it thinks it would be giving away a competitive advantage."</i>  Commemorating the same occasion, <a href="http://www.ofb.biz">OfB.biz</a> also <a href="http://www.ofb.biz/modules.php?name=News&file=article&sid=294">featured an interview</a> with Trolltech CEO Haavard Nord. <i>"With the potential for a substantial performance improvement, Qt 4 could be coming out about the time Nord estimates that GNU/Linux desktop adoption will start to become more mainstream. He remarked that there has been a shift in Qt purchasing [to Linux] over the past few years."</i>  Just a couple of articles you might find interesting.

<!--break-->
