---
title: "Security: Konqueror Cross-Domain Cookie Injection"
date:    2004-08-23
authors:
  - "wbastian"
slug:    security-konqueror-cross-domain-cookie-injection
comments:
  - subject: "kde system udpate daemon."
    date: 2004-08-23
    body: "what about a daemon, which says if your system is vulnerable and needs updating ???\n\nperhaps with a icon in systray or bubble , and a link to the update\nfacility of the different distributions...."
    author: "chris"
  - subject: "Re: kde system udpate daemon."
    date: 2004-08-23
    body: "SuSE already has one of these - I thinks it's best left to the individual distros to create their own solution rather than trying to write a generic KDE one.\n\n(Should I reply three times too?)"
    author: "Ben"
  - subject: "Re: kde system udpate daemon."
    date: 2004-08-23
    body: "Oh, everybody creates his own solution? this is not the right way. At least interoperability has to be assured. I don't see a reason why individual distros shall not interoperate with such a generic KDE tool. Unification is of high importance."
    author: "gerd"
  - subject: "Re: kde system udpate daemon."
    date: 2004-08-24
    body: " The packager (distro) needs to do this, the KDE team notifies the world of the problem, the people who packaged your version of KDE then need to implement what you want. In the same way that Windows-XX and MacOS have an update tool that only supports their own OS."
    author: "Hamish"
  - subject: "Re: kde system udpate daemon."
    date: 2004-08-24
    body: "ok, but a system does not contain only kde. Would you like to have an applet for every important piece of software on you system?\nIf your curious about all system vulnerabilities, join the linux security mailinglists to get updated on a daily basis. \nIf you only want to know about vulnerabilities that affect your current installation, using the applet provided by your distro is the only solution. Only your distribution knows if a specific bug hits your system or not, and they are the ones that provide the solution for your system"
    author: "rinse"
  - subject: "Re: kde system udpate daemon."
    date: 2004-08-24
    body: "well it wouldnt be hard for KDE to have a tool which checked for updates in KDE software and could easily have a customizable event script for actually responding to this.  That way the distros have the customizabilty to work with there package managment systems, but still the unification of the interface.\n\nFor example, there could easily be a simply plain text file that is in $KDEDIR for each part of a kde installation (kdebase, kdelibs, etc) this way it knows whats installed without asking the package manager.  and there could be a file in $KDEBASE/etc/ which gets run when you click on one of these \"bubbles\" which would be defined by the distro (if no script found pull up kde site to product in question).\n\ni think this would be a very nice happy medium, no?\n\nEvan Teran"
    author: "Evan Teran"
  - subject: "Re: kde system udpate daemon."
    date: 2004-08-23
    body: "As much as I hate to say it, I agree with Ben that it should be left up to the various distros.\n\nI say this because MDK doesn't package the the same things in the same way that Gentoo does.  Gentoo doesn't package things in the same way Fedora does.  Fedora doen't package things in the same way that Debian does.  blah blah blah.\n\nYou get the point.\n\nYes, I agree, threre should be some kind of \"automatic 'Linux Update', but that simply isn't possible (unless you're talking about what \"Linux\" really is (the kernel - only).\n\nMe, personally, I run Gentoo.  I sync every couple days and (after review) allow it to do just about whatever it wants (I installed this machine:\nls -l /README.maintainer \n-rw-r--r--  1 root root 3473 May  8  2002 /README.maintainer\n.)\n\nI know your point is that Joe Six-Pack wouldn't even think to do this.  Well, in this case you;d be right (Joe Six-Pack wouldn't be running Gentoo...), but for the more \"main-stream distros (MDK, Fedora, RH, SuSE, etc.), perhaps *SOME* level of:\n\n\"HEY MAN! [insert package] *REALLY* that needs a fix, that you *should* download is available.\" kinda prompt when pppd (or whatever) detects a connection should check for.\n\nMy idea is a good one, but it has nothing to do with KDE (specifically).\n\nM."
    author: "Xanadu"
  - subject: "Re: kde system udpate daemon."
    date: 2004-08-23
    body: "Wow...  Sorry, readers...  My grammar was horrible...  please don't hold it against me... :-\\\n\n--\nM."
    author: "Xanadu"
  - subject: "Re: kde system udpate daemon."
    date: 2004-08-23
    body: "I'm not sure about the other 'main-stream' distros (someone posted SuSE has its own), but I know RedHat (so Fedora too) has Up2Date which is a little tray applet that automatically connects to some respositories (it supports apt and yum I know) and check if there are any updates, if there are it will change from an 'ok' like icon to I think it was a red \"!\".  It was really useful for updates only, though now I like to use Apt w/ Synaptic (should try Yum someday).  The tray applet is automatically started the first time the system is started and is mostly always running (there is a background service too).\n\nI'm sure Mandrake has to have a method similar to this (haven't gone near it since I used Mandrake 7.1 w/o an internet connection, so really have no clue).  I'm not sure what other 'main-stream' distros that joe six-pack pack could install (I don't see how anyone can install Debian, I've tried several times on different hardware and can never get an internet connection...)"
    author: "Corbin"
  - subject: "General notifier applet"
    date: 2004-08-23
    body: "How about a small \"systray\" app, that would function as a scriptable notifier. It would be extrememly simple, and basically run a script as a cron job. Then one could use a \"configuration file\" to parse the output of that custom script, and display a message.\n\nFor example I could have it wget slashdot.org and then diff it to a saved version, and if there would be a newer version it would pop up a little window, or the icon would change or both.\n\nThis could also be a poor man's update notifier."
    author: "kdy"
  - subject: "Re: General notifier applet"
    date: 2004-08-23
    body: "you cant do diff's on website this easy , beacuse of dynamic content , like comments oder commericals or other stuff that changes ....\n\nfor this purpose is kwebwatch..."
    author: "chris"
  - subject: "Re: General notifier applet"
    date: 2004-08-23
    body: "You can do diff that easy, if the purpose of the website was specifically to feed this applet as he's implying.  But he's using the wrong tool for the job.\n\nRealistically, this is a solved issue.  RSS is a pull feed of announcements.  If there was to be something like this, it should be security.kde.org (or a feed off of bugs.kde.org) that lists critical bugs that cause potential harm via malicious exploit or interaction with the underlying system (I can't think of one at this point, but it is conceivable).  It is not a matter of new tech, simply a matter of setting up a repository.\n\nThat said, I agree that it should be left up to the OS maker or distro.  "
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: General notifier applet"
    date: 2004-08-24
    body: "Me thinks a KBugTicker with feeds from kde, debian, gentoo, fedora, etc would be a nice idea"
    author: "ninj"
  - subject: "Re: General notifier applet"
    date: 2004-08-24
    body: "i dont want to read about the bugs , i want the applet that it fixes them for me (eg. call the updater)"
    author: "chris"
  - subject: "Re: General notifier applet"
    date: 2004-08-24
    body: "Well, suse does that automagicly for you :)\nyou get a notification in your systray, click on it, and your system is updated in a breeze :o)"
    author: "rinse"
  - subject: "Re: General notifier applet"
    date: 2004-08-24
    body: "eventwatcher"
    author: "Qerub"
  - subject: "Re: General notifier applet"
    date: 2004-08-24
    body: "Getting notified is one thing, getting patches is another.\nSo you get notified your debian/suse/mandrake/whatever system that kde has a new patch available.\nNice, but unless your distro provides patches for the found bug, the information is useless.\nSo why not have the current situation, where your distro provides an applet that tells you about patches that are available for your system (including evere piece of software that you installed on that distro, using the installation disc, not just kde)"
    author: "rinse"
---
A
<a href="http://www.kde.org/info/security/advisory-20040823-1.txt">
security advisory</a> has been issued for a cross-domain cookie injection vulnerability. The vulnerability is limited to hosts under certain country specific domains such as the British .ltd.uk domain and the Indian .firm.in domain. KDE 3.3 already contains a fix for this issue.


<!--break-->
