---
title: "Developer Task Lists for KOffice"
date:    2004-05-19
authors:
  - "ngoutte"
slug:    developer-task-lists-koffice
comments:
  - subject: "Yet-another-tasklist-approach"
    date: 2004-05-19
    body: "Keeping lists of TODOs and such in the codebase is a useful thing; heck, that's why KDevelop red-flags comment lines containing FIXME. But is it really necessary to set up yet another infrastructure here? Just last week we had the JJ: article on the dot, explaining that trivial or easy wishlist items and small bugfixes can be entered in bugzilla and marked with JJ: in the summary field in order to attract new volunteers that feel they need a little handholding, but would still like to help out. Why doesn't KOffice do the same?"
    author: "Ade"
  - subject: "Re: Yet-another-tasklist-approach"
    date: 2004-05-19
    body: "Sorry, I was aware of the JJ: idea well after I had started the task lists. (I would not mind to mark a bug as JJ: if I would find a wish in KDE Bugs that fits into this category.)\n\nAs for TODO and FIXME, that is how it is done in KOffice too, but practice has shown that mostly potential new developers do not search tasks in code. That is why this method is explicitely explained in the task list index.\n\nHave a nice day!\n"
    author: "Nicolas Goutte"
  - subject: "Just an idea..."
    date: 2004-05-21
    body: "I think it is a great idea to supply customised task lists. These are just some things that popped up in my head as I was reading...\n\nPerhaps some kind of task tree could be constructed, so each kind of developer, from junior to pro, can find appropriate tasks at one place and see the relations between tasks. I do not know whether a similar system already exists, but some kind of graphical representation of the bug tracking system would certainly render a lot mare accessible, an additional tree-representation, for instance. (A picture says more than 1000 words...)\n\nWhat about presenting the current task lists and properties (difficulty and shareability) as a table? It will probably yield a more compact representation and overview of the tasks. "
    author: "CML"
  - subject: "Re: Just an idea..."
    date: 2004-05-22
    body: "As I am not doing anything with KDE Bugs, I cannot answer your anser about a graphical (or any better) representation of the bugs. KDE uses Bugzilla and will remain so in at least near future.\n\nAs for the KOffice Task Lists, yes, the look could be improved. (I will keep your idea of a table in mind.) The current look was more make quickly to fill a need, as some developers seemed to think that KOffice is too difficult for them.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "yet another vote for JJ"
    date: 2004-05-25
    body: "I'd like to see KOffice adhering to the JJ bug marking in the future, it is much easier to find bugs and features to implement that way.\nJust my comment.\nThanks."
    author: "Pupeno"
  - subject: "Re: yet another vote for JJ"
    date: 2004-05-26
    body: "There are currently 3 KOffice bugs marked as junior jobs:\n#63011, #63015, #76412\n\nPersonally I am hesitating to put these 2 bugs as junior jobs:\n#76297 and #76298\n\nOtherwise in the KWord and KOffice \"products\" of KDE Bugs, I do not see what other bugs could be marked as junior jobs. However that does not mean that they are not any easy tasks to do in KOffice, but reported bugs are mostly not easy.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Query for Junior Jobs"
    date: 2004-05-27
    body: "By the way, a query for Junior Jobs was added directly in the entry page of KDE Bugs: http://bugs.kde.org\n\nThe query is KDE-wide.\n\nHave a nice day!"
    author: "Nicolas Goutte"
---
The <A href="http://www.koffice.org/developer/">KOffice Developer Resources</A> have now <A href="http://www.koffice.org/developer/tasks/">a few task lists</A>. Please note that the tasks are meant for KOffice CVS HEAD. Nevertheless some can be done on KOffice 1.3.x too and some are even general enough to be done elsewhere in KDE too.
<!--break-->
