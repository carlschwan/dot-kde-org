---
title: "aKademy Streaming Live OGGs"
date:    2004-08-21
authors:
  - "ateam"
slug:    akademy-streaming-live-oggs
comments:
  - subject: "Cool!"
    date: 2004-08-21
    body: "Watching now with kaffeine :)\n\nCan hear some voices in the background and some guys passing in front of the camera. Very nice."
    author: "yardbird"
  - subject: "Re: Cool!"
    date: 2004-08-21
    body: "Hm, this detains me from buying some food for the weekend. ;)"
    author: "Carlo"
  - subject: "Mplayer cannot play ogg video"
    date: 2004-08-21
    body: "Mplayer is not able to show video for the archived file http://ktown.kde.org/akademy/Gustavo_Boiko_Common_Code_Patterns_video.ogg\n\n\nmplayer  Gustavo_Boiko_Common_Code_Patterns_video.ogg\nPlaying Gustavo_Boiko_Common_Code_Patterns_video.ogg.\nOgg stream 0 is of an unknown type\nOgg file format detected.\n==========================================================================\nOpening audio decoder: [libvorbis] Ogg/Vorbis audio decoder\nAUDIO: 24000 Hz, 2 ch, 16 bit (0x10), ratio: 4000->96000 (32.0 kbit)\nSelected audio codec: [vorbis] afm:libvorbis (OggVorbis Audio Decoder)\n==========================================================================\nChecking audio filter chain for 24000Hz/2ch/16bit -> 24000Hz/2ch/16bit...\nAF_pre: af format: 2 bps, 2 ch, 24000 hz, little endian signed int\nAF_pre: 24000Hz 2ch Signed 16-bit (Little-Endian)\nAO: [oss] 24000Hz 2ch Signed 16-bit (Little-Endian) (2 bps)\nBuilding audio filter chain for 24000Hz/2ch/16bit -> 24000Hz/2ch/16bit...\nVideo: no video\nStarting playback...\nOgg : bad packet in stream 1\nOgg : bad packet in stream 0"
    author: "Rajil Saraswat"
  - subject: "Re: Mplayer cannot play ogg video"
    date: 2004-08-21
    body: "You need to install libtheora and recompile MPlayer with theora support."
    author: "Johan Dahlin"
  - subject: "Re: Mplayer cannot play ogg video"
    date: 2004-08-23
    body: "I have libtheora 1.0 alpha2 and mplayer 1.0 pre5 and I have compiled it with theora support but I still get this error and can't play the video. What else could be wrong?"
    author: "Senatore"
  - subject: "Re: Mplayer cannot play ogg video"
    date: 2004-08-23
    body: "you need libtheora alpha3"
    author: "_"
  - subject: "Re: Mplayer cannot play ogg video"
    date: 2006-10-18
    body: "I use Ubuntu Dapper and have libtheora ver 0.0.0.alpha4-1.1\nI get the 'Ogg stream 0 is of unknown type' error after it plays.\nSo I guess my mplayer (from a live cd used for install )\n   wasnt compiled for libtheora support.\nI can see that 'ogg' and 'vorbis' and 'theora' are not in any list on any tab in Preferences, but where should it appear when I get source and compile it? )\n\n\n"
    author: "Thomas J Powderly"
  - subject: "OGG? Ogg!"
    date: 2004-08-21
    body: "It's Ogg, not OGG!"
    author: "Xunil"
  - subject: "Watching Theora video format right now!"
    date: 2004-08-21
    body: "I'm watching the stream right now.  After downloading HelixPlayer, I paste the stream URL to the open location of HelixPlayer, and instantly the video stream shows up.  I'm amazed!   This is the first time I've experienced a free, patent-free, license-free video streaming format.  All others that I've used in the past have some catches to them like Windows Media, Real Video, Divx, Divx :), other mutations of Divx, MPEG 1, MPEG 2, and MPEG 4. \n\nFinally a free, usable, fast streaming video format.\n\nMy question is:\n\nHow can I record from my video camera to Theora file format?\nI have MPEG 2 movie I want to convert to Theora file format, how can I do it?\nHow can I stream Theora file format?\n\nThanks!"
    author: "My my"
  - subject: "Re: Watching Theora video format right now!"
    date: 2004-08-21
    body: "I can seek too :) ... oh damn!  this is good stuff.  No more lock in with Microsoft Video Streaming, kakaka!  w00t w00t!\n\nThanks guys!"
    author: "My my"
  - subject: "Re: Watching Theora video format right now!"
    date: 2004-08-21
    body: "To record to stream, you can use mplayer and it's -dumpstream and -dumpfile options. You can find more in the mplayer docs, or maybe someone will post something more helpful (I'm a bit busy with press releases ;)"
    author: "Tom"
  - subject: "Re: Watching Theora video format right now!"
    date: 2004-08-21
    body: "\"How can I record from my video camera to Theora file format?\nI have MPEG 2 movie I want to convert to Theora file format, how can I do it?\nHow can I stream Theora file format?\"\n\nThis is all doable of course, but at the moment doing this is more of a developer only thing. The theora encoder hasn't been integrated into the user-type applications yet (ffmpeg, mencoder etc.), and the streaming code hasn't been released by fluendo yet (it's not ready).\n\nAnd of course, technically, theora is still alpha."
    author: "Robert"
  - subject: "Re: Watching Theora video format right now!"
    date: 2004-08-30
    body: "\"This is the first time I've experienced a free, patent-free, license-free video streaming format.\"\nThat is not exactly true. Theora is NOT patent-free.\nOn2 have several patents on the theora/VP3 codec. However, xiph have signed an agreement with On2 where On2 agrees on allowing free use of their patented technology. This agreement is irrevocable. (At least, that's what the xiph website says)."
    author: "Ampz"
  - subject: "Windows users"
    date: 2004-08-21
    body: "Streaming works perfect in http://www.videolan.org/vlc/download-windows.html"
    author: "anon"
  - subject: "Track 2"
    date: 2004-08-21
    body: "Track 2 is also recorded but is not streamed. It will be available for later download (actually it's already but the current download location may change)."
    author: "Anonymous"
  - subject: "Leider etwas dilletantische Kameraf\u00fchrung"
    date: 2004-08-21
    body: "Schade, aber nach 2 min kriege ich Kopfschmerzen von der extrem wirren Kameraf\u00fchrung. Zudem probiert jemand noch verschiedene Fokussierungen, Bildverzerrungen und Farben aus. \nAkadamy?  Eher Kindergarten scheint mir. Schade um die eigentlich gute Serviceleistung.\n\nLasst die Kamera einfach STEHEN. Danke."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: Leider etwas dilletantische Kameraf\u00fchrung"
    date: 2004-08-21
    body: "Use the audio stream and slides when available. Am I glad that the nice Fluendo guys will likely not understand your flame."
    author: "Anonymous"
  - subject: "Re: Leider etwas dilletantische Kamerafhrung"
    date: 2004-08-21
    body: "Until the last two speeches or so the camera was basically static. After that the one then behind the camera apparently thought it would be a good idea to catch the people asking questions in the QA part with the cam as well. I can't fault him for that. ;)"
    author: "Datschge"
  - subject: "Hmmm"
    date: 2004-08-21
    body: "The server seems overloaded at the moment - I can't connect :-("
    author: "Braden MacDonald"
  - subject: "seems so"
    date: 2004-08-21
    body: "So sad...\n\n200 users capability is not sufficient for such a large KDE community.\n\nThanks anyway to the guys behind all these"
    author: "Liucougar"
  - subject: "Re: seems so"
    date: 2004-08-21
    body: "Maybe someone could get these streams onto peercast? \nhttp://www.peercast.org\n\nI don't have enough upstream myself. By getting this on peercast, you could have virtually unlimited viewers"
    author: "Andy"
  - subject: "Re: seems so"
    date: 2004-08-22
    body: "All of the talks should also be available recorded later as well."
    author: "Scott Wheeler"
  - subject: "ARCHIVE.org"
    date: 2004-08-22
    body: "It should all (audio and video) be hosted on Archive.org -- it's exactly the kind of thing that they do. "
    author: "Mikhail Capone"
  - subject: "Re: seems so"
    date: 2004-08-22
    body: "The maximum number of clients connected were 75 (video) 15 (audio), so I'm quite sure 200 audio clients will be enough. Maybe we will need to increase the limit for  video clients, but I doubt it.\n\nUnfortunately it's a little bit difficult to get a theora capable video player installed at this point. But I expect it to be better as soon as the first stable release of theora is released and distributions can start shipping applications using it."
    author: "Johan Dahlin"
  - subject: "Re: seems so"
    date: 2004-08-23
    body: "Just take the finished oggs and feeding them into a muli should solve all bandwidth problems *hint*... ;-)"
    author: "Andy"
  - subject: "Synchronization issues!"
    date: 2004-08-22
    body: "The sound is coming one or two second later. \nAlso we hear to much the noice in the audience and that makes it hard to understand the speaker. Does anyone know how to filter this (from kaffeine)?\n\nNevertheless nice open source work work."
    author: "veton"
  - subject: "Re: Synchronization issues!"
    date: 2004-08-23
    body: "Try the KPlayer/MPlayer combo, it should give better AV sync in most cases.\n\nhttp://kplayer.sourceforge.net/\n\nhttp://www.mplayerhq.hu/"
    author: "-"
  - subject: "Converting to theora"
    date: 2004-08-22
    body: "Really nice.\nThis convinced me to switch the video archives on my website to theora/vorbis.\n\nOne small remark for the archives:\nThe ogg files are sent as plain text, and so cannot be opened from konqueror.\nJust adding a .htaccess file to the archives directory containing:\nAddType application/ogg     ogg \nwill solve the issue (at least it worked for my site)"
    author: "jean-baptiste"
  - subject: "Are there transcriptions?"
    date: 2004-08-23
    body: "I don't have enough bandwidth to download the audio files.  Are there transcriptions of these talks?\n\nTom"
    author: "Tom Huckstep"
  - subject: "Re: Are there transcriptions?"
    date: 2004-08-23
    body: "http://wiki.kde.org/tiki-index.php?page=Talks+%40+aKademy"
    author: "Anonymous"
  - subject: "Archive of Recordings"
    date: 2004-08-31
    body: "The streaming server is gone, the record archive is currently available at http://ktown.kde.org/akademy/"
    author: "Anonymous"
---
The aKademy Team  is excited to announce a set of live audio and 
video streams covering the <a href="http://conference2004.kde.org/">
KDE Community World Summit 2004</a>, code-named "aKademy", taking 
place in Ludwigsburg, Germany from August 21st to 29th. Those who 
cannot attend in person can now watch or listen to what is going on 
over the Internet, thanks to <a href="http://www.fluendo.com/">Fluendo</a>, 
a GStreamer-based streaming media company. Read on for full details.



<!--break-->
<p>Tom Chance, KDE Press Officer commented that "<i>these streams show that free software 
can now let people from around the world watch a large community event, 
bringing a conference for hundreds of people over the Internet</i>".</p>

<p>
Anyone with an Internet connection and a stream-capable video player like <a href="https://player.helixcommunity.org/2004/unix/">HelixPlayer 1.0</a> can use the 
streams. The video stream, thanks to bandwidth from the hosts the Filmakademie, can
handle 100 users, and broadcasts as an OGG stream encoded with Theora/Vorbis, whilst the audio stream can handle 200 users and broadcasts as an OGG Vorbis stream. Archives of talks 
will be put online shortly after they finish, for those who miss 
the live streams. Details for the streams can be found on the aKademy web site at:
<a href="http://streamingserver.akademy.kde.org/">http://streamingserver.akademy.kde.org</a>.</p>





