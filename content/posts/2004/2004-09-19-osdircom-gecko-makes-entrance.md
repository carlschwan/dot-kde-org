---
title: "OSDir.com: GecKo Makes an Entrance"
date:    2004-09-19
authors:
  - "buddy"
slug:    osdircom-gecko-makes-entrance
comments:
  - subject: "OpenOffice Gecko etc"
    date: 2004-09-19
    body: "Well, I certainly this new \"KDE can integrate anything\" approach by the KDE team is good.\n\nUsers will have more choices and be flexible. And it shows good cooperation within the open source community.\n\nGreat work guys.\n\nAnd btw, Safari will exist for a long time. So KHTML will be forever used... :-)"
    author: "Alex I"
  - subject: "Re: OpenOffice Gecko etc"
    date: 2004-09-20
    body: "Choice is always a good thing.\nBut I'm totally happy with KHTML. So for me there is no reason to change. "
    author: "Norbert"
  - subject: "Re: OpenOffice Gecko etc"
    date: 2007-05-15
    body: "I beg to differ.\nKHTML, whilst totally standards compliant, doesn't render a lot of pages as well as Mozilla/gecko does.\n\nTry running Google calendar. See how fay you get.\n"
    author: "cottandr"
  - subject: "will it be maintained?"
    date: 2004-09-19
    body: "As mentioned in the linked article, this is not the first attempt to integrate gecko in KDE. I really hope this one will be maintained and usable without a lot of tweeking needed. If this is the case, I'll be a user of this feature (swithing between khtml and gecko). Anyway, thanks for reviving the hope of using gecko in KDE apps :-)\n\nLooking forward to using it!\n\nRaph"
    author: "rb"
  - subject: "Re: will it be maintained?"
    date: 2004-09-20
    body: "I'll use it too, if it's maintained. I'm tired of firing up Mozilla when I want to access allmusic.com or gmail or use some things in blogger.\n\nI like Mozilla, but Konqueror is so much faster on my hardware... Plus, I like encouraging the underdog :)"
    author: "Mikhail Capone"
  - subject: "Re: will it be maintained?"
    date: 2004-09-20
    body: ">>I'm tired of firing up Mozilla when I want to access allmusic.com or gmail or use some things in blogger.\n\nSurely this cannot be the basis for this integration? I mean, why not 'fix' KHTML in stead of porting over a whole rendering engine?"
    author: "ac"
  - subject: "Re: will it be maintained?"
    date: 2004-09-20
    body: "You also have to consider the opposite. Gecko isn't a one-size-fits all solution. I was at a party with several Mac users a week ago, and they all admitted that they use Safari (KHTML) extensively because there are sites that it will render correctly that Camino or other Gecko based browser will not. "
    author: "Brandybuck"
  - subject: "Re: will it be maintained?"
    date: 2004-09-20
    body: "My question is, will it work in Quanta? Not that it matters terribly, but will we be able to use Gecko for rendering in Quanta's VPL mode? That would be neat."
    author: "Antonio"
  - subject: "Re: will it be maintained?"
    date: 2004-09-21
    body: "> but will we be able to use Gecko for rendering in Quanta's VPL mode?\n\nLikely not when seeing what khtml modifications are necessary for it."
    author: "Anonymous"
  - subject: "the coolest part about a component system..."
    date: 2004-09-19
    body: "...is when people use it.  As someone who has played with everything from OpenDoc to ActiveX, I have to say \"KParts,DCOP,Qt Widget Plugins\" are the most powerful, and flexible component systems out there. Also it sports the most important feature, IT'S EASY! I am sure in the next weeks well see some more bomb shells ;)"
    author: "Ian Reinhart Geiser "
  - subject: "Builds?"
    date: 2004-09-20
    body: "Are there any actual builds of the Qt port of Mozilla yet?  Even if they suck, I'd be happy to give it a whirl!\n\nBTW, an auto-toolkit-picking Mozilla loader would be a great Gnome/KDE collaborative project.\n"
    author: "ac"
  - subject: "Re: Builds?"
    date: 2004-09-21
    body: "> Are there any actual builds of the Qt port of Mozilla yet?\n\nNo, not yet."
    author: "Anonymous"
---
It seems like whenever KDE developers get together for a meeting or conference, great things happen. For instance, in the past we've seen developments such as the creation of DCOP (one of the building blocks of KDE), or thousands of fixes in only one short week. The recent KDE conference in Germany was no exception. One of the most notable outcomes of the conference was <a href="http://osdir.com/Article1635.phtml">an impromptu project to port Mozilla to KDE</a>.

<!--break-->
