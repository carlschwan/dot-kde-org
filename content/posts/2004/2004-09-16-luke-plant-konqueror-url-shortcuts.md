---
title: "Luke Plant: Konqueror URL Shortcuts"
date:    2004-09-16
authors:
  - "lplant"
slug:    luke-plant-konqueror-url-shortcuts
comments:
  - subject: "NICE"
    date: 2004-09-15
    body: "Now we just need to make this easy for mom & pop... aka default to use both.. and tie the home button in the toolbar to it and we are gold."
    author: "Jonathan C. Dietrich"
  - subject: "Re: NICE"
    date: 2004-09-15
    body: "That won't be possible without actual changes to the Konqueror code, as far as I can see.  This was just meant as a work-around, and a demonstration of what you can do with the great frameworks in KDE.  \n\n(It is filed as a bug already - multiple times - e.g. http://bugs.kde.org/show_bug.cgi?id=42270 )\n\nLuke\n"
    author: "Luke Plant"
  - subject: "Re: NICE"
    date: 2004-09-16
    body: "No, this is a dupe :-)\nhttp://bugs.kde.org/show_bug.cgi?id=8333 is the one you are looking for."
    author: "Michael Jahn"
  - subject: "Re: NICE"
    date: 2004-09-16
    body: "If you find such dupes, it would be usefull to add a comment in the bugreport with a reference to the original report so this one can be closed. "
    author: "Andre Somers"
  - subject: "Re: NICE"
    date: 2004-09-16
    body: "??? It's closed as duplicate since 2002."
    author: "Christian Loose"
  - subject: "The home button is at the wrong place"
    date: 2004-09-16
    body: "Think about it. All my most visited bookmarks are in the bookmar toolbar or in the bookmark menu. Why should one address button be in the main toolbar when no other one is?\n\nI think the concept of the Home URL for browsing is flawed. Put your favorite sites in your bookmark toolbar (call it \"Home\" if you like) to be easily accessible and hide the home button completely in the web browsing profile.\n\nWhat kind of home URL do people have? \n- Their personal site (but you don't really want to see it EVERYTIME you open a browser window)\n- Google (use gg:)\n- a blank page (well, you don't need a home shortcut then)\n- ???\n\nThere's no need for 'Go Home' when you surf the net.\n"
    author: "Bausi"
  - subject: "Re: The home button is at the wrong place"
    date: 2004-09-16
    body: "> There's no need for 'Go Home' when you surf the net.\n\nThere was a reason to come up with this... Netscape wanted to be a portal. Yahoo focuses on this with customizations of my Yahoo. The point being that a portal is a very profitable place to sell ads. I'm reminded of the Futurama episode where Fry was incensed they put ads in your dreams and listed off all the places they were in the 20th century, \"but never in your dreams\". Of course like Pavlov's dog they were all off to go shopping next.\n\nYour point is very good. I get irritated if I accidentally click the home button when browsing, but even running a business I have no address that makes sense for it. My home page? My administrative interface? I have these on toolbars. I can only see this if I were drooling for a portal. ;-)\n\nWhat really makes sense is hiding this button in the browser mode."
    author: "Eric Laffoon"
  - subject: "Re: The home button is at the wrong place"
    date: 2004-09-16
    body: "I agree completely. A \"home\" button makes sense in a hypertext document (take me back to the index) and file browsing (take me back to my home directory), but it doesn't make any sense while browsing the web. Unfortunately we're stuck with a bad concept that has survived for far too long."
    author: "Brandybuck"
  - subject: "Re: The home button is at the wrong place"
    date: 2004-09-16
    body: "Your point is good. But I can't agree.\nI often return to my home directory when I'm browsing the web. Combinated with Tab browsing, it's one of my prefered feature of konqui.\n"
    author: "andrianarivony"
  - subject: "Re: The home button is at the wrong place"
    date: 2004-09-17
    body: "As far as I can see, he was not talking about your home directory, but having a \"home page\" when surfing the web.\n\nI can't see why people need a \"home page\" when surfing the web, too."
    author: "Eduardo"
  - subject: "Re: The home button is at the wrong place"
    date: 2004-09-17
    body: "I disagree. As my homepage I use a local html file that has links to all my bookmarked webpages, together with a google search bar and other customizations. This way you have a lot more flexibility than with the bookmark toolbar alone."
    author: "AC"
  - subject: "Re: The home button is at the wrong place"
    date: 2004-09-17
    body: "You can as well put your local file as a first item in the bookmark toolbar."
    author: "Bausi"
  - subject: "Re: The home button is at the wrong place"
    date: 2004-09-17
    body: "I came up with the same conclusion than you : to make this concept of \"home\" useful, I have to generate a page with my bookmarks, a serach bar and some RSS feeds.\n\nBut actually, we both prove he has a point, because... well if it is that hard to make the concept of \"Home\" page useful, then the concept is flawed. It is good for  selling advertisements with your portal, but it\u00b4s bad for 90% (just a guess) of the users."
    author: "jmfayard"
  - subject: "Re: The home button is at the wrong place"
    date: 2004-09-19
    body: "or use kontact's home ;-)"
    author: "superstoned"
  - subject: "Re: The home button is at the wrong place"
    date: 2004-09-19
    body: "In a corporate environment it is useful to have everybodies home page pointed at the company intranet by default. If somebody needs to be guided to a URL it is much easier to tell them to click on the home page icon, and then follow a link than to get them to type in a URL. (People who know enough to change their home page do not usually need this level of support)."
    author: "John Lines"
  - subject: "DCOP"
    date: 2004-09-16
    body: "I was trying to use khotkeys and DCOP the other day to give focus or raise different windows. I couldn't find a good way to do this, though. I didn't see anything in KWin's DCOP interface that would give me a window list, or raise a window. Does anyone know how to do that?"
    author: "AC"
  - subject: "Re: DCOP"
    date: 2004-09-16
    body: "You can get functions like these:\nvoid hide()\nvoid maximize()\nvoid minimize()\n...\nvoid raise()\nvoid lower()\nvoid restore()\nvoid show()\n\nbut it seems it's a different object with each app e.g. \nKopete:\ndcop kopete mainWindow\nKonq:\ndcop konqueror-XXXX konqueror-mainwindow#1\nKOrganizer:\ndcop korganizer \"KOrganizer MainWindow\"\n\nAlso, some of these functions don't seem to work if the app is not on your current virtual desktop.  \n"
    author: "Luke Plant"
  - subject: "Re: DCOP"
    date: 2004-09-18
    body: "The focus-stealing prevention seems to catch that. It's too bad there's no KWin interface for this that could bypass it."
    author: "AC"
  - subject: "Re: DCOP"
    date: 2004-09-16
    body: "Also, KHotKeys has 'Activate window' as a builtin action, which works well enough for me."
    author: "Luke Plant"
  - subject: "[OT] Kecko state?"
    date: 2004-09-16
    body: "Hi there,\n\nstill being excited about gecko/firefox getting proper qt and kde integration\nI wonder whether there has already been any code committed...\n\nHere I see nada:\nhttp://bonsai.mozilla.org/cvsquery.cgi?branch=HEAD&branchtype=exact&date=week\n\nAnd also there has been no single message mentioning it in the mailinglists\nso far.\n\nIs there no blog telling about the progress?\n\nHow about some enlightenment... :-) ?"
    author: "ac"
  - subject: "Re: [OT] Kecko state?"
    date: 2004-09-16
    body: "Watch yourself: http://bugzilla.mozilla.org/show_bug.cgi?id=259033 and dependencies."
    author: "Anonymous"
  - subject: "Re: [OT] Kecko state?"
    date: 2004-09-16
    body: "Thanks a lot !\n\nfor the lazy people: ;-)\nhttp://bugzilla.mozilla.org/show_bug.cgi?id=259033\nhttp://bugzilla.mozilla.org/show_bug.cgi?id=259035\nhttp://bugzilla.mozilla.org/show_bug.cgi?id=259036\nhttp://bugzilla.mozilla.org/show_bug.cgi?id=259037\nhttp://bugzilla.mozilla.org/show_bug.cgi?id=259038\n"
    author: "ac"
---
<a href="http://www.konqueror.org/">Konqueror</a>'s 'Ctrl-Home' keyboard shortcut is usually limited to a single URL -- either the home page or home folder -- and so often does "the wrong thing" for the user.  Here is a tutorial on <a href='http://lukeplant.me.uk/articles.php?id=1'>how to override this using KHotKeys and DCOP</a>, make it context sensitive, and how to create keyboard shortcuts for any URL while we are at it.




<!--break-->
