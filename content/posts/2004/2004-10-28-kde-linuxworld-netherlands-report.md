---
title: "KDE at LinuxWorld Netherlands Report "
date:    2004-10-28
authors:
  - "fmous"
slug:    kde-linuxworld-netherlands-report
comments:
  - subject: "Huge space!"
    date: 2004-10-28
    body: "Compared to what you get in the .ORG village in LinuxWorld London, your space is huge! In London groups get about 1m of shelf space ;)"
    author: "Tom"
  - subject: "Re: Huge space!"
    date: 2004-10-28
    body: "Yes, but Jonathan managed to grab 6 metres for us, so it wasn't too bad! :)\n\nAdmittedly, I'd have preferred a proper booth like they had at LWCE NL as opposed to the shelf we got at LWCE London. Why is it that overseas they all seem to get better booths and we get given a bookshelf?"
    author: "George Wright"
  - subject: "Re: Huge space!"
    date: 2004-10-28
    body: "Because we dare to ask :) \n\nCiao\n\nFab"
    author: "Fabrice Mous"
  - subject: "Re: Huge space!"
    date: 2004-10-29
    body: "And to claim...\n\nBe the first and put yr flag!\n\nAren't we bastards ;)\n\n-Cies Breijs"
    author: "cies breijs"
---
During 13th and 14th October <a href="http://www.kde.nl/">KDE-NL</a> was present at <a href="http://www.linuxworldexpo.nl/">LinuxWorld</a> in Utrecht, the Netherlands. Bas Grolleman, a member of the Dutch KDE community wrote <a href="http://www.kde.nl/agenda/2004-10-13_14-linuxworldexpo/">an account</a> of the event. Here on the Dot is a translation of that account.
<!--break-->
<p>This first LinuxWorld event in the Netherlands was held simultaneously with two other events: 'Storage Expo' and 'InfoSecurity'. Many large companies were present including Sun and Novell. It was styled more as a business event, and because even the penguin in the logo was wearing a tie I decided to do the same.</p>


<p>The KDE-NL booth wasn't easy to find. Compared to other booths we only had a small and humble spot on the Expo floor at the back of the hall. However our presence was noticed by some visitors during a second tour of the expo floor. We did have instant access to coffee because we had the coffee bar for exhibitors in front of us!</p>



<p align="center"><a href="http://static.kdenews.org/fab/interviews/lwe2004-NL/kde_stand.jpg"><img src="http://static.kdenews.org/fab/interviews/lwe2004-NL/kde_stand_small.jpg" alt="kde_stand_small.jpg" width="333" height="250" border="0" /></a><br /></p>


<p>The event was very much focused on Linux in the business-place (besides the small .ORG pavilion) and there was a large number of commercial stalls from various companies offering their wares. It was clear that Linux and OpenSource have become big business and nowadays everybody has something to say about his or her OpenSource offering.</p>


<p>Besides the booths, several seminars were held. It was a pity that some people with excellent technical skills have a slight inability to keep the audience interested. For example the seminar about User-Mode-Linux was very interesting on a technical level and it described very well what you needed to get started with it. Unfortunately on the level where you need to convince people and make them enthusiastic to use UML it was running short.</p>

<p>During the second day I attended a seminar given by the company <a href="http://www.randstad.nl/">Randstad</a>, who described their migration to Linux. It was very interesting stuff but they did not give much information about the technical details of this migration. I am very curious about how it all will turn out for them.</p>

<p>The KDE-NL team demonstrated the features of KDE as a business desktop. Jeroen Baten spoke on behalf of KDE-NL during both days, in a <a href="http://www.kde.nl/agenda/2004-10-13_14-linuxworldexpo/presentaties/jeroenbaten/lwe2004.html">presentation</a> about the benefits of using KDE as a business desktop. We spoke with many visitors, some of whom  were already using KDE and were able to give us their experience with the KDE desktop.</p>

<p>We would like to thank our sponsor, IT solutions provider <a href="http://www.kovoks.nl/">KovoKs</a> who lent us several laptops with the latest KDE version on. These laptops helped to attract a lot of people. This may also have been because we were showing the <a href="http://dot.kde.org/1097789248/">famous Konqi movie</a> on them. We would like to thank Bastian Salmela (Basse) for making a <a href="http://www.kde.nl/mediakit/animatie/konqi-magical_rope/">Dutch version</a> of the Konqi animation.</p>

<br />
<p>Amersfoort, October 2004<br />Bas Grolleman</p>


<ul>
<li><a href="http://www.kde.nl/agenda/2004-10-13_14-linuxworldexpo/fotos/images.html">photos</a></li>
</ul>