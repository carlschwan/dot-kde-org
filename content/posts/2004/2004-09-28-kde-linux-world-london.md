---
title: "KDE at Linux World in London"
date:    2004-09-28
authors:
  - "jriddell"
slug:    kde-linux-world-london
comments:
  - subject: "Matthias Ettrich"
    date: 2004-09-29
    body: "Forgot to say that Matthias Ettrich will also be there.\n"
    author: "Jonathan Riddell"
  - subject: "Re: Matthias Ettrich"
    date: 2004-09-30
    body: "Can I just turn up?"
    author: "Max Howell"
  - subject: "Re: Matthias Ettrich"
    date: 2004-09-30
    body: "Yes - it's 10 GBP on the door iirc."
    author: "George Wright"
  - subject: "Re: Matthias Ettrich"
    date: 2004-09-30
    body: "as George said, you can just turn up.. but the exhibitor passes have already been ordered.\n\nLast time it was free if you booked in advance and 11 GBP on-the-door (but that was ludex, not linuxworld). This time it's 7.50 GBP in advance, 15.00 GBP on-the-door.\n\nI'll look forward to seeing you there (-:\n\n- Jeff"
    author: "Jeff Snyder"
---
Next week sees the <a href="http://www.linuxworldexpo.co.uk/">Linux
World Expo</a> (renamed from Linux Expo UK) in <a
href="http://www.londontown.com/LondonInformation/Entertainment/Olympia/7d9a/">London's
Olympia</a> where KDE are teaming up with Gnome to run one of the
biggest stands in the <a href="http://www.linuxexpo.org.uk/">.org
village</a>.  As well as representing the desktop environments, we
will be promoting the desktop platform projects <a
href="http://www.freedesktop.org/">freedesktop.org</a> and <a
href="http://www.x.org/">X.Org</a>.  To be demonstrated on the stand
are KDE 3.3, FreeNX, X.org 6.8 with XComposite and a cool Konqi the Dragon
animation.  Many <a href="http://www.kde.org.uk/developers.php">kde-gb developers</a> will be on the stall, we hope to see you there.

<!--break-->
