---
title: "theKompany.com Releases Aethera 1.1"
date:    2004-07-05
authors:
  - "binner"
slug:    thekompanycom-releases-aethera-11
comments:
  - subject: "if you are a multiplatform company..."
    date: 2004-07-05
    body: "... this is definitely the only way to go.  Aethera + KOLAB is the only multiplatform messaging environment we have found that is consistent across Windows and MacOSX.  Aethera has a few rough spots yet, but its progress has been steady and its really starting to look good.  \n\nIf you have a company that has been stuck with the horrible exchange client for MacOS, or you just need to get rid of that extra PC in marketing that they just use for email, run don't walk to check out Aethera!"
    author: "Ian Reinhart Geiser"
  - subject: "Re: if you are a multiplatform company..."
    date: 2004-07-07
    body: "ALERT\n\nThis was a post from one of TheKompany's Employees.  He is just trying to make you think that this software is actually not of *Alpha* quality.  I have yet top touch a piece of software by theKompany that is not terrible.  Three long years ago I paid for Kapital their finical manager.  It is still ALPHA quality at best.  It crashes constantly and can not even add up your balance correctly.  BTW that is also at the 1.1 version release.  That is the quality for all of theKompany's software!!\n\nDO NOT BOTHER DOWNLOADING THIS SOFTWARE"
    author: "Whistle Blower"
  - subject: "Re: if you are a multiplatform company..."
    date: 2004-07-07
    body: "lol, yeah lets see who's opinion on the software do we trust? some troll posting out of the blue or someone who has contributed to KDE for years, has his own consulting company promoting Qt/KDE solutions, contributed to KOLAB, and sells KOLAB solutions?  sorry bud, next time do you homework.  since you obviously cannot use Google or be bothered to even know who you are assulting, ill make it easy on you www.sourcextreme.com.\n\nif people would think first before they post the Internet would be a very quiet place."
    author: "Ian Reinhart Geiser"
  - subject: "Re: if you are a multiplatform company..."
    date: 2004-07-07
    body: "So you are trying to say that you have never worked for theKompany?  I really do not care what else you have done, that is immaterial."
    author: "Whistle Blower"
  - subject: "Re: if you are a multiplatform company..."
    date: 2004-07-07
    body: "if you have a bone to pick that bad, then sure, I have done contract work for TKC.  if that makes me evil, send the the T-Shirt. ill wear it proud.\n\ni suppose im suppose to bash you too because i hate the food McDonnalds serves?  "
    author: "Ian Reinhart Geiser"
  - subject: "Re: if you are a multiplatform company..."
    date: 2004-07-07
    body: "Knowing Ian for some time, I can tell you something he's on of the best programmer I have the chance to meet, if only I was as good as him...."
    author: "Mathieu Chouinard"
  - subject: "Re: if you are a multiplatform company..."
    date: 2004-07-08
    body: "oh well. i work for a download portal at a big isp and always look for oss to add. i am well used to kde (at home) and gave athera a shot. under win32 the programm is hardly usable:\n\nfont size in email messages is not adjustable - ever tried to read a 4pt font?\npassword - the password management is hardly user friendly, even if password is typed and held in preferences, you still need to check \"remember pw\"\nimap support - athera crashed constantly with my imap acc\n\ni was rather disappo\u00ednted and though i might at least give some feedback. but now their bugreporting tool mantis lets me register, bus is so horribly slow i never managed to report a bug.\n\njust my 2 cents."
    author: "adrian"
  - subject: "Re: if you are a multiplatform company..."
    date: 2004-07-08
    body: "I use it on Windows all the time, I haven't seen this font problem you are talking about though.  As to our bug reporting system, we are using the open source project Mantis, and for some reason their last update made the system dog slow, you can speed it up significantly by selecting the project you are working with first, that is what I do.\n\nthanks for any feedback you can provide"
    author: "Shawn Gordon"
  - subject: "Mantis speed"
    date: 2004-07-12
    body: "You can vastly increase the speed of Mantis by building indexes. There's a list of what should be done on the Mantis page somewhere.\n\nRik"
    author: "Rik Hemsley"
  - subject: "Very Cool"
    date: 2004-07-05
    body: "Very cool Shawn i've forwarded the info to a friend that going to try and make a Linspire deb. Can't wait to give it a test try. :)"
    author: "CraigB"
  - subject: "Re: Very Cool"
    date: 2004-07-05
    body: "Thanks Craig.  Theoretically our install method should work on Linspire using the taballs, but I don't know anyone that has tried it yet.  We'd be happy to post the deb with the rest of our packages if you want to send them our way."
    author: "Shawn Gordon"
  - subject: "Re: Very Cool"
    date: 2004-07-06
    body: "Hi,\nI just want to let you know that I know someone who used alien to convert the RPM to DEB and he is using Aethera very well on debian.\nRegards,\nEug"
    author: "Eugen Constantinescu"
  - subject: "Re: Very Cool"
    date: 2004-07-06
    body: "I tryied that and when i run aethera in the console i get this error.\n\naethera\n/usr/local/bin/aethera: line 9: /usr/local/thekompany/bin/tkc-loader: No such file or directory\nCraig:~#"
    author: "CraigB"
  - subject: "Re: Very Cool"
    date: 2004-07-07
    body: "You need thekompany-support RPM (DEB) installed too."
    author: "Eug"
  - subject: "Re: Very Cool"
    date: 2005-08-15
    body: "which i would get where? i can't find it on the filer, where i fond the tgz."
    author: "dothebart"
  - subject: "Wow"
    date: 2004-07-05
    body: "Wow... I remember trying Aethera a loooong time ago when I first switched to Linux... didn't realize there was a Windows port (and now a Mac port, yay!)  I was always kind of surprised at how Evolution and later, Kontact just seemed to come out of nowhere and then we didn't hear much about Aethera.  Good to see its still around.  And even better to realize its cross platform (Windows and Mac)... thats something that can't be said for Evolution or Kontact"
    author: "Bill"
  - subject: "Re: Wow"
    date: 2004-07-06
    body: "Evolution will have a Windows version by year's end."
    author: "Justme"
  - subject: "Re: Wow"
    date: 2004-07-07
    body: "whoa, where'd you find this?  google turns up only the standard \"will there be a port\" and \"I'm going to start a port (dated 2001)\" threads..."
    author: "Bill"
  - subject: "Re: Wow"
    date: 2004-07-07
    body: "Porting Kontact to MacOSX shouldn't be that hard, shouldn't it ? now that Qt is GPLed on MacOSX and parts of KDE have been ported."
    author: "Pupeno"
  - subject: "I tried it in Win32"
    date: 2004-07-06
    body: "I tried it at work on Win32. While it looks reasonable, I found problems with authorised SMTP (which does not seem to work), and with the multiple windows. For example, the mail screen has a list of your accounts/folders, but this list overlaps the contents of the selected folder and the current message, ie, it 'hovers' above them. From my perspective, they should all be on the same plane (or a drop-down list with folders/accounts could be provided, I don't know). At any rate, with some extra polishing, and hoping the program gets some wide testing (i.e., if people in slashdot start trying it out and so on), then we could be on the path to a very nice, sleek piece of software!"
    author: "Josete"
  - subject: "Improve the interface."
    date: 2004-07-06
    body: "This program could be very useful, but it really needs a good amount\nof user-interface love."
    author: "Justme"
  - subject: "Re: Improve the interface."
    date: 2004-07-07
    body: "deffinitly :)\n\nShould the be legally able to use Everaldo's crystal icons for it?\n\nNOTE: refresh your screenshots with the Plastik theme :)"
    author: "cies"
---
<a href="http://www.thekompany.com/">theKompany.com</a> has <a href="http://www.thekompany.com/press_media/full.php3?Id=268">announced the 1.1 release</a> of <a href="http://www.thekompany.com/projects/aethera/">Aethera</a>, an <a href="http://www.thekompany.com/projects/aethera/screenshots.php3">email/PIM/groupware client</a> that runs on Linux, Windows and starting with this release Mac OS X (Panther and later). The <a href="http://www.kolab.org/">Kolab</a> functionality has been improved and now includes support for "Shared Folders".
Aethera is offered for free under GPL in English, German and French while <a href="http://www.thekompany.com/products/aethera/index.php3"> additional plugins</a> for Jabber (including peer to peer file transfer), whiteboarding and voice over IP are offered commercially.

<!--break-->
