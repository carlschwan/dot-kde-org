---
title: "Application of the Month: Konserve"
date:    2004-07-03
authors:
  - "ateam"
slug:    application-month-konserve
comments:
  - subject: "Great logo"
    date: 2004-07-03
    body: "Your HD in a can. I love it! This is also a handy little app. Thanks."
    author: "Eric Laffoon"
---
While we were enjoying a short leave of absence we finished a new version of the "Application of the Month" series. 
Through the collaboration of the <a href="http://www.kde.de">German</a> and <a href="http://www.kde.nl">Dutch</a> KDE teams we are now able to offer you an <a href="http://www.kde.nl/apps/konserve/en/interview.html">interview</a> with Florian Simnacher, 
author of <a href="http://konserve.sourceforge.net/">Konserve</a>, a cool 
backup application for the KDE desktop. Enjoy Application of the Month in the languages 
<a href="http://www.kde.de/appmonth/2004/konserve/index-script.php">German</a>, 
<a href="http://www.kde.nl/apps/konserve/">Dutch</a> and <a href="http://www.kde.nl/apps/konserve/en/">English</a>. 


<!--break-->
