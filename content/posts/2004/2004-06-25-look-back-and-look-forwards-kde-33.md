---
title: "A Look Back and a Look Forwards at KDE 3.3"
date:    2004-06-25
authors:
  - "binner"
slug:    look-back-and-look-forwards-kde-33
comments:
  - subject: "Language feature"
    date: 2004-06-25
    body: "One feature in KDE 3.3 that is not mentioned in the article is a change in language evaluation.\nThe old way was: from the list of languages that the user likes, pick the first language that offers a translation of the program. Fill the rest with English.\nThe new was is: from the list of languages that the user likes, pick the first language that offers a translation of the program. Fill the rest with the translations from the second choice language etc. Fill the rest with English.\n\nThis is especially useful for smaller languages like Low Saxon where the rest of the translation of a program can be filled up with standard German.\n\nAnother (quite stupid) example from bugs.kde.org:\nKontact was not tranlated into Ukrainian, but kmail was. The user had said in kcontrol, that he likes Ukrainian and Russian in this order.\n\nIf you then start kmail as a standalone program you get in in Ukrainian. When you launch it via kontact, KDE finds out that Kontact is not translated into Ukrainian and uses Russian for kontact and all its submodules.\n\nThis is now fixed.\n\nHeiko Evermann"
    author: "Heiko Evermann"
  - subject: "Re: Language feature"
    date: 2004-06-25
    body: "There is quite a bunch of new features not mentioned in the article if you compare it with the feature plan. It honestly only scratches a little bit on the surface."
    author: "Anonymous"
  - subject: "Re: Language feature"
    date: 2004-06-25
    body: "Low saxon? This sounds pretty cool. Is there a screenshot available.\n\nLanguage order and fallback solution. \n\nWhat I believe however is that language string contribution has to be made easier for ordinary users that are not skilled in Programming. This may help to fix the problem which in fact is lack of contributions."
    author: "hein"
  - subject: "Re: Language feature"
    date: 2004-06-26
    body: "Wouldn't it be nice if you were able to contribute to translations in a web page, wiki style? Hummm must think more about this. Or imagine that you are running a program that is not fully translated, you see a string that is not correctly (or not at all) translatted, you click Help->Tranlate String, the cursor changes, you click were the untranslated string is, and you are redirected to this wiki-style tranlation page, or the po file is opened for editing and when you click save the diff is uploaded to a server for review."
    author: "P.Alves"
  - subject: "Re: Language feature"
    date: 2004-06-28
    body: "> What I believe however is that language string contribution has to be made\n> easier for ordinary users that are not skilled in Programming. This may help\n> to fix the problem which in fact is lack of contributions.\n\nWell there is KBabel. No need for any programming skills. How much easier is it supposed to get?"
    author: "Christian Loose"
  - subject: "Kwallet"
    date: 2004-06-25
    body: "I absolutely agree with the guy who wrote the kde 3.3 preview about the kwallet thing. It must be me, but I hadn't found the option to disable it yet (there might be something wrong with my configuration though)."
    author: "Jelmer"
  - subject: "Re: Kwallet"
    date: 2004-06-25
    body: "Indeed, he's absolutely right. Nothing is more frustating than the requirement to type your password _every_ time after logging in (because of Kopete). I know it's not very safe to built in a \"remember my KWallet password\" option, but usability-wise this option is absolutely needed."
    author: "Niek"
  - subject: "Re: Kwallet"
    date: 2004-06-25
    body: "Infact, \"remember my KWallet password\" is not very unsafe either. I need to supply my password atleast once to log in KDE, so why do i need to re-authenticate myself again and again?"
    author: "Nilesh Bansal"
  - subject: "Re: Kwallet"
    date: 2004-06-28
    body: "I put up with this myself, in case someone walks up to my PC while I'm not there (you could say my fault for not locking it before leaving, I suppose).\n\nIn future the threat of malware on Linux may become more serious, in which case measures like this will be more important."
    author: "Paul Eggleton"
  - subject: "Re: Kwallet"
    date: 2004-06-25
    body: "kwalletmanager, Settings/Configure Wallet..., [ ] Enable the KDE wallet subsystem"
    author: "Anonymous"
  - subject: "Re: Kwallet"
    date: 2004-06-25
    body: "Personally, I think the best approach would be a simple PAM module that would try using the user's password to unlock the wallet.  This would be, IMHO, the easiest way to make this happen.  Then, it is just a matter of adding the option to the 'kde' pam config file.\n\nIf I knew how to write something like this, I would have already."
    author: "Chris"
  - subject: "Re: Kwallet"
    date: 2004-06-25
    body: "PAM is not everywhere."
    author: "somekool"
  - subject: "Re: Kwallet"
    date: 2004-06-26
    body: "But where it is, this is a good idea."
    author: "Bausi"
  - subject: "Re: Kwallet"
    date: 2004-06-28
    body: "Infact it is on almost every serious distro that is build these days.\n\non other platforms that don't support it you could still go the other way around. (or write something that fits better into that platform. having access to your own password hashes is a bad thing because email scripts, internet scripts, bash scripts, whatever script can send them up the internet to the hacker of choice, so he can brute force them.\n\nsecurity most always be done by external tools."
    author: "Mark Hannessen"
  - subject: "Student"
    date: 2004-06-25
    body: "Will there be full support for real SVG-icons in KDE 3.3? Or is there a possibility to use SVGs in KDE 3.2.3? :-(\nWill Kitchensync have new connectors or will it even work? For example to sync with a Sony-Ericsson mobile? (T630) ;-)"
    author: "Sebastian Greiner"
  - subject: "Re: Student sorry >>> SVG / Kitchensync"
    date: 2004-06-25
    body: "Sorry, I wrote my \"title\" in the form... :-o"
    author: "Sebastian Greiner"
  - subject: "Re: Student"
    date: 2004-06-25
    body: "Well... I'm working on a set as we speak (Reinhardt, found at kde-look.org), and apart from a few graphical glitches, it works just fine in 3.2.2 :)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: Student"
    date: 2004-06-25
    body: "Well, in fact the Reinhardt icons work a little *too* well...\nYou can scale them up to 256x256 in the tooolbar, with 1 pixel increments...\nThe popup menu gets huge! \nIn my index.desktop file I limit the range for action icons to 16-64, and the popup, shile still large, si somewhat more manageable. The popup menu (or the icon theme interface, maybe) should be improved to reduce the number of items shown, and maybe show a dialog if it is really *really* needed to have pixel-precise sizing.\nThere are another couple of bugs that while not too serious, make vector icon support look incomplete. \n\nHere are the bug reports:\n\nhttp://bugs.kde.org/show_bug.cgi?id=83171\nhttp://bugs.kde.org/show_bug.cgi?id=83172\nhttp://bugs.kde.org/show_bug.cgi?id=83174\n\nI think the KCM bug and the icon selector one are probably related, and fixing one will probably fix both issues."
    author: "Luciano"
  - subject: "IrmcSync"
    date: 2004-06-26
    body: "Simone Gotti started to implement a kitchensync konnector for IrmcSync. I've never used it myself, since I lack the phone to use it with, so I don't know exactly what's working already and what not. But in theory it should work with a T630.\n\nYou can find it in the CVS of kdebluetooth (kdeextragear-3). Compilation is disabled by default at the moment, since it depends on KDE 3.3.\n\nAFAIK Simone is a bit short of time at the moment. I'm sure he will appreciate any help offered to make SonyEricsson/Siemens mobile phone users happy by completing the konnector ;) "
    author: "Fred Sch\u00e4ttgen"
  - subject: "Re: IrmcSync"
    date: 2004-06-28
    body: "Thanks for telling me about this IrmcSync-Feature. But I couldn't find a RPMed Version of kdebluetooth with this integrated. \nAnd since I also couldn't figure out how to download from KDE(-Extragear)-CVS I gave up a bit frustrated...\nSo I'll wait for 3.3... ;-)"
    author: "Sebastian Greiner"
  - subject: "Re: IrmcSync"
    date: 2004-06-28
    body: "<A href=\"http://kde-bluetooth.sourceforge.net/\">KDE-Bluetooth</A>\n\nDocumentation -> 3. Getting the KDE Bluetooth Framework"
    author: "brenton"
  - subject: "Redundancy.."
    date: 2004-06-25
    body: "Weren't most of the other appearance-related KCM's supposed to be hidden once the theme manager was moved out of kdenonbeta? I've been waiting patientially for someone to do that for some time :-)"
    author: "smt"
  - subject: "Re: Redundancy.."
    date: 2004-06-25
    body: "Yup, shame on me :( Still on my TODO"
    author: "Lukas Tinkl"
  - subject: "Re: Redundancy.."
    date: 2004-06-25
    body: "Please also fix the size of the buttons in the theme manager."
    author: "Anonymous"
  - subject: "theme manager"
    date: 2004-06-25
    body: "\"get new themes\" and \"install themes\" sounds very similar.\n\nThis might create confusion."
    author: "hein"
  - subject: "Re: theme manager"
    date: 2004-06-26
    body: "Don't think so. None necessary involves the other."
    author: "Anonymous"
  - subject: "kdm"
    date: 2004-06-25
    body: "I never had a problem with the kdm pictures (I like being a stylised dragon thank you very much), but what bugs me is that in 3.2 the option to edit the session list in kcontrol seems to have gone missing. So I can no longer log into xfce from kdm. Anyone else have this problem?"
    author: "Michael Donaghy"
  - subject: "Re: kdm"
    date: 2004-06-29
    body: "The 3.2 Kdm is supposed to automatically find all of your installed window managers and destkops, and display them in the appropriate login menu. If XFCE is not displaying, it's either because you don't have it installed, it's installed under a name Kdm doesn't recognize, or Kdm doesn't know about XFCE."
    author: "David"
  - subject: "what a bad reviewers"
    date: 2004-06-25
    body: "First one keep complaining about small issues which are really subjective.\n\nlike the default look. I'm sorry, but I like life and bright color. It's what make KDE and Apple different from all other boring interface like Gnome and Windows.\n\nWorking on a computer is not only for boring gray guy wearing a tie.\n\nI like curves, forms, movement, like cars and womans ;)\nKeramik is great and looks ALIVE ! Plastik is dead and morgne. look like default gray GTK.\n\nBUT WHO CARES !!! Its what makes Linux so great... use whatever you prefer. It's available to you. Keeping complaining about default look all along the review does not makes it a good review. Really boring to read.\n\nFor the orange and yellow in kmail, If I understood correctly what he was talking about ... The orange is customizable in colors, and the yellow is to show SECURITY RISK !!! Which MUST jump in user's eyes.\n\nwell, whatever.\n\nand the second one... way more positive.. but unreadable...\ncome'on do you think we remember your 3.2 review ? All this insides jokes are way to much. Please be more professional. I've not been able to understand half of his review because of all his jokes, smiles and unreadable kiddies english.\n\nwhatever, hopefully this review won't make peoples laugh out of KDE.\n\nKDE is way better, however how it looks on your particular setup."
    author: "somekool"
  - subject: "Re: what a bad reviewers"
    date: 2004-06-29
    body: ">I like life and bright color. It's what make KDE and Apple different from all\n>other boring interface like Gnome and Windows.\n>Working on a computer is not only for boring gray guy wearing a tie.\n\nWindows is gray and doesn't have bright colours? Are you colour blind, or are you using a monochrome monitor?\n\nWindows has colours which are much brighter than MacOS, and this is why MacOS does not look like a kids' desktop."
    author: "oliv"
  - subject: "Re: what a bad reviewers"
    date: 2004-06-29
    body: "About the review. You're right. It was unprofessional and did contain inside jokes, but the point was really to do a professional review. It was just one user's (mine) perspective on what i thought about certain things in the upcoming release and a chance to show screenshots to those who haven't seen it before. \nYes, it does sound childish at points and that was intentional because I am looking forward to this next release with childlike enthusiasm in my boosiasm. I'm weird like that. If you want professional reviews wait until 3.3 released when OSNEWS, PCLO and others will inevitably review it. If you want opinion and screenshots I got what you need :D Oh and about the english, I do sometimes forget that not everyone using KDE or reading the dot is english speaking so if I made it any harder to understand for the non native english speakers/readers I apologize. I'll try to remember to do better next time :p"
    author: "illogic-al"
  - subject: "Noatun >> /dev/null"
    date: 2004-06-26
    body: "With apparently both Juk and Amarok now in KDE 3.3, can someone enlighten me as to what the use, purpose or reason is for allowing any further existence of Noatun?\n\nLook at the \"freaking\" Noatun website for a good laugh: http://noatun.kde.org/news.php#NoWereNotStillAlive \"The primary reason nothing is happening here at noatun.kde.org is that we noatun developers consider noatun so close to completion it's not worth discussing.\"\n\nIn the mean time, are we supposed to wait for coders send from \"above\" to code heavenly plugins which are not likely to become default in 10 years and not likely to ever ex-hist in the first place, given developers and users are 'embracing' Juk and Amarok?\n\nIs there some hidden secret agenda behind Noatun which is going to blow all our minds away in one big global awe? Please enlighten me as to why Noatun is even allowed to be in KDE anymore.\n\nJust curious."
    author: "ac"
  - subject: "Re: Noatun >> /dev/null"
    date: 2004-06-26
    body: "amaroK is not in KDE 3.3"
    author: "Anonymous"
  - subject: "Re: Noatun >> /dev/null"
    date: 2004-06-26
    body: "Too bad, I would prefer it over Noatun ..as you might have noticed. I don't think I am the only one with 'bad feelings' over this whole Noatun thing.\n\nWhats so great about Noatun?"
    author: "ac"
  - subject: "Re: Noatun >> /dev/null"
    date: 2004-06-26
    body: "I completely agree. And everytime something like this is mentioned, nobody speaks up saying \"I actually like noatun\". And for those of you who do like noatun - I hope this was enough to make you reply ;-). I just use mplayer, kmplayer, kaffeine or xmms."
    author: "affenschlaffe"
  - subject: "Re: Noatun >> /dev/null"
    date: 2004-06-29
    body: "I also agree.\n\nBURN Noatun, BURN!"
    author: "foljs"
  - subject: "Re: Noatun >> /dev/null"
    date: 2004-06-29
    body: "I actually like noatun. But then, I don't like mp3.. ;)"
    author: "teatime"
  - subject: "Re: Noatun >> /dev/null"
    date: 2004-07-03
    body: "Well, here I am, I actually like noatun. I use kaffeine for playing videos, but I think it is an overkill for music. I use \"wake up\" plugin as my alarm clock (haven't found a single decent alarm clock program yet). noatun is simple, lightweight and full featured. May be I will change my opinion once I try amarok, this thread is definitely reason enough to go and compile it. So far I didn't look for an alternative music player, since despite a few minor annoyances, I found noatun to be a very nice program."
    author: "Jeld The Dark Elf"
  - subject: "Re: Noatun >> /dev/null"
    date: 2004-06-29
    body: "actually i would replace juk with amarok immediately. juk is so much worse...now when amarak reached stable 1.0 it should replace juk in kmultimedia"
    author: "yemu"
  - subject: "Re: Noatun >> /dev/null"
    date: 2004-06-26
    body: "kaffeine does everything i need.\n\nincluding MP3, party animation, playlist, movies\nAND VIDEO SHOUTCAST !!!!"
    author: "somekool"
  - subject: "Still with Arts? + kwin&konqueror crashes thoughts"
    date: 2004-06-26
    body: "Hello ! It looks like KDE 3.1 to 3.2 had much more changes than KDE 3.2 to 3.3 doesn't it?\n\nAnyway, it's wonderful that Chris' just one more time telling us what's new in the next KDE version ;-).\n\nChris mentions: \" Kwin has suffered the occasional inexplicable death. You never really fully appreciate the term \"Window Manager\" until it's crashed and you realize just how much it does.\"\n\nI totally agree with you man ! I've even suffered kwin crashes in KDE 3.2.x stable versions, and ended up filling a feature request, where I suggest the best possible solution: If the WM crashes, KDE should restart it smartly: http://bugs.kde.org/show_bug.cgi?id=82277 .\n\nI'm happy you still hadn't any konqueror crash, because in FC1 + KDE 3.2.2 it likes crashing a lot =). I have filled  a bug report that suggest a solution with a similar approach to the last one; this time the fix would be to ask the user if he want to recover last session when it opens a new konqui's window and it has just crashed. You can read the report in http://bugs.kde.org/show_bug.cgi?id=83803 .\n\nFinally, Chris mentions that he configured gstreamer output in amaroK. I'm a tad curious: wasn't KDE 3.3 supposed to replace arts with other Sound System ? I think It would be sad if we continue with arts for another KDE big version; every other Sound Library I've used so far has worked better than it here. I think that I'm most probably  wrong and the challenge was for KDE 4 :P.\n\nRegards,\n   Edulix."
    author: "Edulix"
  - subject: "Re: Still with Arts? + kwin&konqueror crashes thoughts"
    date: 2004-06-26
    body: "> It looks like KDE 3.1 to 3.2 had much more changes than KDE 3.2 to 3.3 doesn't it?\n\nIs this surprising when you look at the development time which is only the half of the KDE 3.2 cycle?"
    author: "Anonymous"
  - subject: "Re: Still with Arts? + kwin&konqueror crashes thoughts"
    date: 2004-06-29
    body: "> I'm a tad curious: wasn't KDE 3.3 supposed to replace arts with other Sound System?\n\nNo.  We've never intended to replace aRts in the KDE 3.x series since it's required for binary compatibility with previous releases.  We will most likely switch to another system for KDE 4."
    author: "Scott Wheeler"
  - subject: "um. chris? [was Re: Still with Arts? ]"
    date: 2004-06-29
    body: "erm, my name isn't chris :) \nhttp://uhaweb.hartford.edu/obennett/i-kubed/aboot.html"
    author: "illogic-al"
  - subject: "Kde rocks"
    date: 2004-06-28
    body: "every kde release has had \"a must have\" something since I started using kde 1.0\n\nThis time the there are many many interesting features including:\n\nAnti Spam Wizard\nAnti Virus Wizard\nKolab client integraton\nHTML mail composing\nGUI for enabling KIOSK features\n\nkeep up the good work kde!"
    author: "Mark Hannessen"
  - subject: "Re: Kde rocks"
    date: 2004-06-28
    body: "If you take a look at Kommander you might decide it is worth putting on that list. It is somewhat revolutionairy in it's approach to bringing application extention through DCOP and rapid small application development to users and scripters. It is in the kdewebdev module.\n\nhttp://kde-apps.org/content/show.php?content=12865\nhttp://dot.kde.org/1087424515/\n\nIf there is something missing in KDE you want you might find yourself building it in a few hours some night. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: Kde rocks"
    date: 2004-06-28
    body: "I agree kommander is looking very very cool,\nBut I haven't really had the time to try it out yet.\nBut I'll surely take a look at it very soon."
    author: "Mark Hannessen"
  - subject: "Noatun, Juk, Amarok, kedit, kwrite..."
    date: 2004-06-28
    body: "What I'm finding kinda disturbing in KDE lately is the repetition of applications, can we just leave one application for each tasks... I don't know about Noatun, Juk and Amarok, but about kwrite and kedit... when will kedit will be removed ? My simple market researching shows that application redundancy confuses people."
    author: "Pupeno"
  - subject: "Re: Noatun, Juk, Amarok, kedit, kwrite..."
    date: 2004-06-28
    body: "My understanding was that KEdit will disappear as soon as the part that KWrite uses supports bi-directional text. So it's an i18n issue. If you are compiling from source you can use DO_NOT_COMPILE to remove certain applications anyway.\n\nI agree in general that there should be some consolidation, particularly in the areas of image viewers and media players. There are also quite a few little applications that I suspect nobody uses or even finds because they are stuck in the \"more applications\" subfolders in the K menu."
    author: "Paul Eggleton"
  - subject: "Re: Noatun, Juk, Amarok, kedit, kwrite..."
    date: 2004-06-28
    body: "> as soon as the part that KWrite uses supports bi-directional text.\n\nAnd this support is said to be implemented much easier with Qt 4."
    author: "Anonymous"
  - subject: "Re: Noatun, Juk, Amarok, kedit, kwrite..."
    date: 2004-06-29
    body: "``My simple market researching shows that application redundancy confuses people.''\n\nOoh! A professional market researcher in our midst!"
    author: "David"
  - subject: "Re: Noatun, Juk, Amarok, kedit, kwrite..."
    date: 2004-06-29
    body: "Then you know for sure, what is THE most important reason to include two almost identical programs in KDE bundle."
    author: "keber"
  - subject: "Re: Noatun, Juk, Amarok, kedit, kwrite..."
    date: 2004-06-29
    body: "KEdit can only be removed when Kate will fully support right-to-left scripts\n(which it did not, however I do not know the current status.)\n\nAs for KWrite, it is a sort of skin for Kate.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Noatun, Juk, Amarok, kedit, kwrite..."
    date: 2004-06-29
    body: "I see KWrite as similar to kedit, kate is out of the game. Kate is a multiple file with lots of features editor, while kwrite and kedit are just simple text editors. Anyway, I hope that kwrite gets right to left support and we can get rid of kedit."
    author: "Pupeno"
  - subject: "Re: Noatun, Juk, Amarok, kedit, kwrite..."
    date: 2004-06-29
    body: "Kate and KWrite share the same code (Kate's). Only the user interface is different (KWrite is therefore as sort of \"Kate Light\".)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Noatun, Juk, Amarok, kedit, kwrite..."
    date: 2004-06-29
    body: "yes, I know, kate_part's... which happens to be same used in konqueror, quanta, kdevelop, etc..."
    author: "Pupeno"
  - subject: "My homepage/startpage"
    date: 2004-06-29
    body: "When I am browsing the web using Konqueror in KDE 3.2/3.3, which button do I press to go to my homepage/startpage?"
    author: "ac"
  - subject: "Re: My homepage/startpage"
    date: 2004-06-29
    body: "This is an extremely good question. There is no simple answer, because thus far Konqueror does not support different home urls for different view profiles. I digged through bugs.kde.org and found this bug relating to that problem:\n\nhttp://bugs.kde.org/show_bug.cgi?id=31542\n\nIt seems to have regressed, however..."
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: My homepage/startpage"
    date: 2004-06-30
    body: "Personally, I like having the home button go to ~.  If you want a \"home page\", just set a link to it on the bookmark toolbar.  Give the button a Home icon, and you're set."
    author: "Artie"
---
Chris Dunphy wrote a nice four page <a href="http://www.nerdsyndrome.com/kde-3.2.1-review/">KDE 3.2.1 Review</a> in March which we obviously missed and which imo hasn't received the attention it deserves yet. On the other side we were notified by "illogic-al" that he has written a short "<a href="http://uhaweb.hartford.edu/obennett/i-kubed/kde33/kde33_preview.html">What's New, KDE 3.3 Preview</a>" based on KDE 3.3 Alpha. The first KDE 3.3 Beta with more <a href="http://developer.kde.org/development-versions/kde-3.3-features.html#inprogress">completed features</a> is <a href="http://developer.kde.org/development-versions/kde-3.3-release-plan.html">expected to be released</a> in about one week.




<!--break-->
