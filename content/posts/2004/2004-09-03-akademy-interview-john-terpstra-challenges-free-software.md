---
title: "aKademy Interview: John Terpstra on Challenges to Free Software"
date:    2004-09-03
authors:
  - "tchance"
slug:    akademy-interview-john-terpstra-challenges-free-software
comments:
  - subject: "Acting together"
    date: 2004-09-03
    body: "\"It isn't a matter of KDE being cooler than GNOME, nor of free software being technically superior to proprietary software in some areas. If we can't reshape the market around open standards and customer satisfaction\"\n\nDon't just get your act together (a part from documentation) - act together."
    author: "Unleash freedom"
  - subject: "Working on it."
    date: 2004-09-03
    body: "We have several small KDE related consulting companies that target such small and medium sized businesses already.  I think John is largely correct in that we should look to improve even further in this area.  \n\nRight now, my day job consists of migrating a small business very much like John was describing ie, one with a proprietary MS Access program that pretty much is the lifeline of the organization.   The short term plan is to migrate to Linux now and use a Windows Terminal Server for this Access application.  The long term plan includes rewriting the app using the Qt/KDE platform.  I'll be sure to write up a description of the experience for KDE enterprise when I'm done.\n\nSmall and medium sized businesses represent a huge underappreciated potential base for KDE/Linux.  I wonder if there are enough knowledgable KDE/Linux folks up for the task."
    author: "manyoso"
  - subject: "Re: Working on it."
    date: 2004-09-03
    body: ">>>> The short term plan is to migrate to Linux now and use <<<<\n>>>> a Windows Terminal Server for this Access application <<<<\u00f9\n\nYou for sure will utilize that new KDE toy \"NX\"/\"FreeNX\" for this, yes?"
    author: "Curious"
  - subject: "Re: Working on it."
    date: 2004-09-03
    body: "NX is neither a toy nor is it really related to KDE."
    author: "Datschge"
  - subject: "Re: Working on it."
    date: 2004-09-03
    body: ">>>> NX is neither a toy <<<<\n\nrrrrrright.\n\n>>>> nor is it really related to KDE. <<<<\n\nHow is it then that only KDE people are hyping that darn thingie??\nI still need to see anz one GNOME mailing list or website dealing \nwith NX favorably....."
    author: "Curious"
  - subject: "Re: Working on it."
    date: 2004-09-03
    body: "its hard to believe how asinie your remarks are..."
    author: "Rawr"
  - subject: "Re: Working on it."
    date: 2004-09-03
    body: "> I still need to see anz one GNOME mailing list or website dealing\nwith NX favorably.....\n\nUh, rrrrrright.\n\nhttp://www.gnome.org/~markmc/a-look-at-nomachine-nx.html"
    author: "anon"
  - subject: "Re: Working on it."
    date: 2004-09-03
    body: "###  http://www.gnome.org/~markmc/a-look-at-nomachine-nx.html\n\nReally interesting rad:\n\n>> I haven't actually played with NX yet, this first impression is just <<\n>> from looking at the code and documentation. <<\n\nUh-jaaah.\n\nSo it was by \"looking at the code and documentation\", huh? So the included flowcharts were not derived from prior art (without acknowledging it)?\n\nMmm-hmmm.\n\nBy \"not having actually played with NX yet\", the author reminds me of the proverbial man who never tries it with a woman, but, to make up his judgement, rather looks at her resume ... and the eventual shots published by Playboy or Hustler...  ;-)\n\nAll in all, the link is not exactly a proof that GNOME is already adopting NX technology. But eventually they will anyways, that's for sure. NX is cool enough for that."
    author: "pipitas"
  - subject: "Re: Working on it."
    date: 2004-09-03
    body: "NX does not have a Windows Server.  They only have servers for Unix OS's as it relies very heavily on the X protocol.  The NX client application can connect to a Windows Terminal Server, however.  But that isn't such a big deal as their are plenty of Free Software clients for RDP ie, rdesktop.  Besides, the NX client can not use Terminal Server in Application Mode...\n\nSo, no, we won't be using NX for that purpose."
    author: "manyoso"
  - subject: "Re: Working on it."
    date: 2004-09-03
    body: "## The NX client application can connect to a Windows Terminal Server, \n## however. But that isn't such a big deal as their are plenty of Free \n## Software clients for RDP ie, rdesktop.\n----\nLet me tell you that this *still* is a big deal. Because NX's RDP access (which, BTW, is based on a heavily modified rdesktop code, boosted by NX's builtin compression and caching technology) is making RDP (as well as VNC) accesss more efficient by a factor of 2 to 10 (on slow, low bandwidth links, depending on exact conditions and sttings).\n\n### Besides, the NX client can not use Terminal Server in Application Mode...\n----\nWhat exactly do you mean by this? Are you saying you have actually tested NX now? Can any other Free Software client for RDP use WTS in \"Application Mode\"?"
    author: "pipitas"
  - subject: "Re: Working on it."
    date: 2004-09-03
    body: "<i>\"Let me tell you that this *still* is a big deal.\"</i>\n\nCool.  So, NX makes WTS faster.  Not sure how, but ok.  Still, it isn't relevant to this situation since we have a nice fast internal LAN.\n\n<i>\"What exactly do you mean by this? Are you saying you have actually tested NX now? Can any other Free Software client for RDP use WTS in \"Application Mode\"?\"</i>\n\nYes, I've tested the server and the client.  I haven't tested the client against RDP, but I saw no configuration or anything that led me to believe it could be used with a WTS in application mode.  BTW, application mode is a real thing so no reason for the scare quotes.  It allows you to connect to an individual application rather than the whole windows desktop.  rdesktop does quite well with it and can even get rid of the linux windows borders and allow the windows app to behave as if it was a real application in the linux window manager."
    author: "manyoso"
  - subject: "Re: Working on it."
    date: 2004-09-03
    body: "##  application mode ... allows you to connect to an individual \n##  application rather than the whole windows desktop.\n\nHow about starting up that NX/NoMachine client then again?\n\nClick \"Settings...\" --> go to \"General\" tab --> select Desktop \"Windows\" --> click \"Settings...\" --> insert netbios name or IP of WTS --> Select \"Run application\" (instead of \"Run desktop\" --> insert name of application (f.e. \"notepad\")"
    author: "pipitas"
  - subject: "Re: Working on it."
    date: 2004-09-07
    body: "IIRC, Windows applications are responsible for their own Window Management. This is why, if a Windows app dies, you can't minimise it or move it out of the way. It may be the Win32 libraries doing the management, but its happening in that application's process space - whereas in the X world, the window management and decorations are done by the seperate Window Manager.\n\nHow does this relate to RDP in \"Application Mode\"? Does the remote Windows application also handle window management in this situation?\n\nTo be fair (or unfair) - I prefer the X-Windows model. Similar design differences can be seen in other parts of Windows too. Windows applications, even if through their library code, are responsible for a lot more than equivalent UNIX applications which have the shell and the window manager to take care of such things for them. The separation of responsibilities is nicer in UNIX/X.\n"
    author: "Richard Corfield"
  - subject: "Re: Working on it."
    date: 2006-12-26
    body: "Hi,\n\ni want to thanks nomachine nx client for making my work easy.\n\nwe have 3 rigs and i was using normal windows rdp and getting headaches b/c of the slowness of the connection.\n\nsome times taking an hour to uninstall and reinstall software.\n\nnow iam using opensuse with freenx server and nx client for windows with rdp.\n\nWOW.......WOW.....WOW..... i am a free man now.\n\ni can work like iam working locally.\n\nSo dont believe on ngeative reviews it really works.\n\n"
    author: "Hamood"
  - subject: "The dark ages lasted 400 years"
    date: 2004-09-07
    body: "The sad thing is the dark ages have to come first."
    author: "bilbo"
  - subject: "Re: The dark ages lasted 400 years"
    date: 2004-12-06
    body: "where'd you get this fact about the dark ages lasting 400 years anyways? it aint true!"
    author: "bla"
  - subject: "Re: The dark ages lasted 400 years"
    date: 2007-09-28
    body: "yes it did u fuckin retard. go suck a dick "
    author: "dark ages man"
---
John Terpstra, one of the long-term members of the Samba Team, never shies away
from speaking his mind. He is known not only for his work on documenting Samba, and his
long experience as a Senior Consultant, but also for his outspoken views on
intellectual property. Intrigued by <a
href="http://www.groklaw.net/article.php?story=20040805065337222">an
article</a> he published on <a href="http://www.groklaw.net/">Groklaw</a>
recently, I took his attendance at the KDE World Summit - <a
href="http://conference2004.kde.org/">aKademy</a> - as an opportunity to talk
to him about his thoughts on intellectual property, what problems the free
software community faces, and how we (including KDE specifically) should
respond. Read on for my reflections on our discussion.


<!--break-->
<p><b>Open Technology Development Under Threat</b>

<p>
John firmly believes that open technology development is under threat, both
from direct legal and financial attacks, and from being limited
technologically in its ability to compete by way of deliberately restrictive laws
in respect of intellectual property. Various organisations and
individuals, he asserts, are willing to sacrifice the world's freedom to use
technology for the sake of their own short-term gain.

<p>
He compared the current
use of Intellectual Property (IP) law to the attacks on science and religious
heresy in the European Reformation, where those leading the attacks do so
under the guise of protecting some valuable existing cultural network. This
message in and of itself is not new, but John opened the interview with a
tone of real concern. While the Reformation may show precedent for these kinds
of attacks, he proposes that the world is complacent, including open source and
free software communities, and that if we are to save ourselves from a structural
shift that will make the free software project impossible, we must organise a
clearly focused response.
</p>

<p><b>On Software Patents, Copyrights, Trademarks and Licensing Deals</b>

<p>
John asserts that software market leaders have spent the past thirty years in
particular amassing vast portfolios of software patents, copyrights, trademarks
and licensing deals. That a company should focus on IP is not a problem for
him - in fact he challenges the software community who reject the term IP
to provide a better way of describing in cogent terms the very substance
of creative thinking that is embodied in free software - and that we as a
community particularly treasure. His concerns come in
part from analysing the Information Technology (IT) market over the past few years:
"it has suffered a major downturn in parallel with the rest of the economy,
which should be seen as a judgement against the IT industry. It should be
in demand when companies need to cut costs and run a business in an
increasingly competitive environment".
</p>

<p>
John was quick to point out the large number of experienced management has been
laid off over the past four years and replaced by younger, less
experienced employees. He said that "we will soon see these large companies'
profits decrease as they realise the need for experienced managers. Many displaced
executives are now being hired as short-term consultants." The opportunity for
informed Open Source advocates is expanding - not contracting.
</p>

<p><b>On Linux Adoption</b>

<p>
When I mentioned how the major software vendors are increasingly supporting Linux
and Open Source software in general, he replied, "The major IT vendors are focussing
on large businesses. Only one company is focussed on the entire market spectrum -
Microsoft, and many of their clients want an alternative solution. The trouble is
that no Open Source company has a clear focus on offering such a solution. Linux
companies are targetting the displacement of traditional UNIX systems." He went
on to say that, "the small to medium business market is adopting Linux despite
the lack of clear focus in this vital part of the market."
</p>

<p>
A quick review of business statistics (see <a href="http://www.bizstats.com/businesses.htm">this page</a>)
shows that there are 5.6 million businesses in the USA alone. If we factor this
according to the USA proportion of gross domestic product (GDP) this equates to
there being approximately 17 million businesses globally. The major vendors focus
their activities at the top 2% of all businesses. By number of business
establishments 98% of all IT opportunity is accounted for by small business needs
that do not need large-scale computing solutions, but that are also most dependant
on support from consultants and resellers.
</p>

<p><b>Large Technology and Media Business Interests in Opposition to Free Software Community</b>

<p>
So what does all of this mean for the free software community? Put simply, for
John it is both a clear warning and an opportunity. We should be aware that
the large technology and media business interests are in direct opposition to
the interests of the free software community, even if they appear to be
supporting us.
</p>

<p>
Before he answered that question, he returned briefly to the point of large
businesses that appear to support us. When I asked him why we should not
value their contribution, he replied: "How should we measure that: by their
revenue? By their contributions in terms of code? By their innovations?" John
was dismissive: "There will be a fundamental reshaping of the market, which
can only come from the bottom up, i.e. from the free software community
and from small companies".
</p>

<p><b>On Open Standards</b>

<p>
So John's vision is that we should focus on building a market infrastructure
that favours open standards, free software where appropriate, and small to
medium sized businesses, which account for more than two thirds of the
market already, but are downtrodden and in need of better solutions.
</p>

<p>
I put it to John that such a goal is laudable, but not exactly something a
free software advocate or small company could read and act on. He concurred,
and admitted that he has no <a
href="http://en.wikipedia.org/wiki/Silver_bullet">silver bullet</a>, but had
some advice. "The two most important strategies we must adopt are to encourage
and adhere to open standards, which undermine big IP oriented business' ability to
monopolise and dominate the marketplace. And we must develop a more
customer-oriented free software market by listening to and engaging with
customers, and then building stronger channels of communication, so that
small and medium sized companies develop a relationship with free software
and open standards. If the entire small and medium sized business community
were to migrate to these effective Open Source solutions, we'd win
overnight".

</p>
<p>
John quickly became more specific on what those of us involved in the free
software community can do to achieve this. First, he urges all free software
advocates to look for work in the small companies, and to support them with
our purchasing decisions. Second, he thinks that the small and medium sized
companies need to improve their total business environment, including better
training offers and more hand-holding of customers. "This is a big opportunity
for cell groups, local providers, to step in and beat the big players at
delivering what customers want".
</p>

<p><b>Reshaping the Market</b>

<p>
Finally, John has a list of major advances in
the products themselves to address customer needs that he considers critical
for the success of the products themselves, and therefore of our efforts to
reshape the market.
</p>

<p>
The harder of his changes fall into the category of migration technologies.
"How", he asks, "can we convince a company that has built its operations
around a proprietary solution to suddenly migrate if in doing so they lose
both the solutions themselves and the data contained therein?" He gave the
example of a company that he believes is typical, one which uses custom
programmed Microsoft Access products to manage its entire operation. "If the
programmer who created that solution has left, taking the source code with
him, that company is stuck relying on this dated solution". Conventionally,
free software advocates would see that as an ideal opportunity, but John
pointed out that unless the data could be migrated seamlessly to a new free
software solution, the process would be extremely expensive and
time-consuming. His work for Samba lends weight to this point. But despite
progress with Samba, groupware integration and some partial office solutions,
free software still cannot offer a streamlined migration across the range of
solutions businesses depend upon. "That", he says, "has to change".
</p>

<p><b>Desktop to Server Integration</b>

<p>
The second change is a matter of complete integration across all aspects of
the desktop and the server. Given the disagreement over such moderate
integration efforts as those under the umbrella of <a
href="http://www.freedesktop.org">freedesktop.org</a>, advocating something
so radical as complete integration will be met with raised eyebrows by some.
But John wants to see a desktop where you can, for example, open a Control
Center that will let you manage everything, the entire configuration
architecture, in one place. This would require a major standardisation effort
that John says KDE can and must lead.
</p>

<p>
He believes that KDE is already an extremely advanced bit of technology - he
described it as "seriously slick" - and expressed his admiration of the
advances between KDE 3.0 and KDE 3.3. When told about some of the latest KDE
news, such as the Microsoft Exchange connector for Kolab and the power of the
Kiosk framework, his opinions were reinforced.
</p>

<p>
So why was he telling me all of this? "Because", he said, "we need to get our
act together". It isn't a matter of KDE being cooler than GNOME, nor of free
software being technically superior to proprietary software in some areas. If
we can't reshape the market around open standards and customer satisfaction,
he believes free software community will come under increasingly frequent and
damaging attacks. And whilst the technically able members of the community
may be able to continue development to some extent (to what extent is unclear
since new hardware may become incompatible as well), by inaction we may fail
where the Reformation and Enlightenment seceded, and fall to the selfish
interests of a narrow group of businesses. KDE is on the forefront of this
fight, and it's important that we win.</p>

