---
title: "Linux Magazine: aKregator, Configuring KDM, KHotKeys"
date:    2004-11-28
authors:
  - "binner"
slug:    linux-magazine-akregator-configuring-kdm-khotkeys
comments:
  - subject: "Precision : kdm != mdkkdm"
    date: 2004-11-28
    body: "In the article about KDM  and GDM in the figure 3 it is not KDM in the screenshot but mdkkdm, a Mandrake fork of KDM. \n\nThe original KDM is always accessible in Mandrake throw drakconf. You need to install kdebase-kdm and if you don't want Mandrake fork uninstall mdkkdm."
    author: "Shift"
  - subject: "Horoscopes"
    date: 2004-11-29
    body: "What does the elephant represent?"
    author: "AC"
  - subject: "Re: Horoscopes"
    date: 2004-11-29
    body: "http://www.postgresql.org/"
    author: "Anonymous"
  - subject: "live bookmark?"
    date: 2004-11-30
    body: "Reading about RSS, is there a reason why dot.kde.org does not have a live bookmark available?"
    author: "testerus"
  - subject: "Re: live bookmark?"
    date: 2004-11-30
    body: "Live bookmark? Mozilla user? See \"rdf\" in the left column."
    author: "Anonymous"
  - subject: "Re: live bookmark?"
    date: 2004-11-30
    body: "dot.kde.org webmaster need to had :\n<link rel=\"alternate\" type=\"application/rss+xml\" title=\"dot.kde.org RSS\" href=\"\"http://www.kde.org/dotkdeorg.rdf\" >\non the HTML source of this website."
    author: "Shift"
  - subject: "Re: live bookmark?"
    date: 2004-11-30
    body: "Besure to put it in the <head> tag ;)"
    author: "Kick The Donkey"
  - subject: "Re: live bookmark?"
    date: 2004-12-04
    body: "Don't put it in the head tag because that is utterly broken.  Put it in the head element."
    author: "Jim"
  - subject: "My ony wish is.."
    date: 2004-12-01
    body: "... that KDM would look as good as GDM, but for now it's just a dream. Hopefully, recently added theme suport will have the same specs as GDM, as there are lots nice gdm examples to port."
    author: "Amiroff"
  - subject: "Re: My ony wish is.."
    date: 2004-12-01
    body: "It's actually the goal of the current KDM theming support effort (which can be found in CVS kdenonbeta/kdmthemes/) to offer full compatibility to GDM themes, so it looks like your wish will be granted soon-ish."
    author: "ac"
  - subject: "Re: My ony wish is.."
    date: 2004-12-01
    body: "kdenonbeta/kdmthemes/ doesn't exist anymore, it's merged into HEAD."
    author: "Anonymous"
  - subject: "Akregator"
    date: 2004-12-02
    body: "I have downloaded and started to use Akregator, and it is a great\nprogram. I was looking something like that and found in it more than\nI am looking for. There are a few points for improvement but overall\nit is very usable as is.\n\nThank you, and keep up the good work!\n"
    author: "ta"
---
In its <a href="http://www.linux-magazine.com/issue/49">last</a> and <a href="http://www.linux-magazine.com/issue/50">upcoming</a> issues <a href="http://www.linux-magazine.com/">Linux Magazine</a> has again published several KDE articles of which are many available online in PDF format: <a href="http://akregator.sourceforge.net/">aKregator</a> is a RSS/Atom feed reader and <a href="http://www.linux-magazine.com/issue/49/aKregator_News_Feed_Reader.pdf">introduced to their readers</a>. KHotKeys <a href="http://www.linux-magazine.com/issue/50/KHotkeys.pdf">allows to add mouse gestures and keyboard shortcuts</a> to your desktop. <a href="http://www.linux-magazine.com/issue/50/Configuring_Display_Managers_KDM_GDM.pdf">Another story</a> details how to configure your login environment with KDM and GDM. Finally, don't miss <a href="http://www.linux-magazine.com/issue/50/2005_Open_Source_Horoscope.pdf">Konqi's horoscope for 2005</a>.

<!--break-->
