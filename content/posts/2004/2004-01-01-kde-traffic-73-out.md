---
title: "KDE Traffic #73 is Out"
date:    2004-01-01
authors:
  - "hpinto"
slug:    kde-traffic-73-out
comments:
  - subject: "old screens..."
    date: 2003-12-31
    body: "<< Waldo Bastian stated that \"the rule so far is that KDE as a whole should work with 800x600 and that individual windows should not exceed 640x480\"  >>\n\nI'm glad to see that this is in-fact a policy found in the KIG.:\n\n http://developer.kde.org/documentation/standards/kde/style/dialogs/complex.html\n\nI have an old laptop ( K6-2, 475MHz still runs KDE great! ) with a 12in screen and 800x600 resolution.  A few apps definitely expand beyond the screen maximum height and saving changes/etc can be a challenge.  I'll try to help developers by finding dialogs that need shrinking.\n\n-jf"
    author: "Jeff D"
  - subject: "Re: old screens..."
    date: 2003-12-31
    body: "When running the office computer in our student society office at my school, the old video card only supported up to 800x600. One particular dialog that was a real PITA at that resolution was the initial KPersonalizer dialog that pops up when a user logs in for a the first time. The \"next\" and \"previous\" buttons would appear off the bottom of the screen and I'd have to alt-drag the window to get them to show. This was very confusing to a lot of users who were new to the system and had never used Linux or KDE before. I suggest the size of this dialog be reduced."
    author: "Kamil Kisiel"
  - subject: "Re: old screens..."
    date: 2003-12-31
    body: "please submit a bug report to bugs.kde.org"
    author: "anon"
  - subject: "Thanks"
    date: 2003-12-31
    body: "AND HAPPY NEW YEAR!!\n\nBTW: I agree with Aaron on Kcontrol."
    author: "Alex"
  - subject: "HAPPY NEW YEAR"
    date: 2004-01-01
    body: "Happy new year 2004 and best wishes for all KDE teams.\nI hope that 2004 will be as great as 2003"
    author: "JC"
  - subject: "2004 will be even better than 2003 for KDE!"
    date: 2004-01-01
    body: "> I hope that 2004 will be as great as 2003\n\nLemme say this: you are a pessimist! Why do you belittle KDE's bright future so much? \n\nI hope for KDE to experience an even better 2004 than 2003 was! And I am going to contribute my part to it. What about you?\n\nGo, KDE, go! "
    author: "Unteachable Optimist"
  - subject: "Re: 2004 will be even better than 2003 for KDE!"
    date: 2004-01-01
    body: "I contribute to the KDE project since 3 years :)"
    author: "JC"
  - subject: "Resolution"
    date: 2004-01-01
    body: "Hi everybody!\n\nI think that 640x480 for windows is definitvely enough.\nKeeping this limit forces you to rethink the design\nof your dialog window and/or app if it's too much which\nis good for usability.\nBTW: Happy New Year everybody!!!!\nI'm looking forward to an even more successful 2004 for KDE.\n"
    author: "Mike"
  - subject: "Settings?"
    date: 2004-01-02
    body: "KControl layout complexity is increasing, however, it's one of the many reasons I like KDE.\nThe basic layout is RIGHT. The growing complexity is the issue. Beyond that, is a much better approach than Window's Control Panel. \nBesides that, we do not want more clamings that KDE is a Windows clone, right? ;-)\n\nGreetings, JCG."
    author: "Julio Cesar Gazquez"
---
The last issue of KDE Traffic for 2003 is out: <a href="http://kt.zork.net/kde/kde20031231_73.html">KDE Traffic #73</a> comes to you at the last day of the year, bringing you news ranging from the minimum necessary resolution to run KDE to displaying GNOME applications in the KMenu. Check it out!

<!--break-->
