---
title: "OSNews.com:  Interview with Everaldo and Jimmac"
date:    2004-05-21
authors:
  - "dbobo"
slug:    osnewscom-interview-everaldo-and-jimmac
comments:
  - subject: "Good info"
    date: 2004-05-21
    body: "Nice and interesting interview. :)"
    author: "LinuxArt"
  - subject: "SVG incompatibilities"
    date: 2004-05-21
    body: "Reading again how the SVG created by Adobe Illustrator is not entirely compatible with KDE, wouldn't it just make sense to fix|extend our SVG implementation to understand the Illustrator extensions?"
    author: "Arend jr."
  - subject: "Common Icon Set"
    date: 2004-05-21
    body: "Wieh the name's soon going to be the same when will KDE and Gnome have the same icon set?"
    author: "Ben Meyer"
  - subject: "Re: Common Icon Set"
    date: 2004-05-21
    body: "That's some way off yet, I'd guess. There was an effort by a RedHat guy to do this a while ago, but it failed.\n\nHowever, there is a new project trying to push this, and some other interim solutions :) We're doing it through freedesktop.org, and so hope to do it \"properly\" rather than from one DE only, or as an external project.\n\nWe're working on:\n\n- A set of tools to do cool things with SVGs (e.g. change colour schemes)\n- A tool to automatically convert themes between DEs (e.g. KDE -> GNOME)\n- A new icon spec that all DEs can adopt, so you have one icon theme download for all DEs!\n\nYou can find most of the info on this page:\nhttp://www.newtolinux.org.uk/wiki/index.php/lila\n\nThough don't be misled - it's not just a project related to the lila icon theme ;) We've also got the Crystal GNOME and Flat people involved so far."
    author: "Tom Chance"
  - subject: "Re: Common Icon Set"
    date: 2004-05-21
    body: "Sounds fantastic.  Other then a few zellots I don't know anyone who doesn't use applications from multiple DE's so this will be most appreciated.\n\nIceFox"
    author: "Ben Meyer"
  - subject: "Re: Common Icon Set"
    date: 2004-05-22
    body: ">>There was an effort by a RedHat guy to do this a while ago, but it failed.\n\nWhy did it fail?"
    author: "JohnFlux"
  - subject: "OT: Why Mono is Currently An Unacceptable Risk"
    date: 2004-05-22
    body: "It's not like we didn't know it...\n\nhttp://www.gnome.org/~seth/blog/mono/"
    author: "ac"
  - subject: "GMail"
    date: 2004-05-22
    body: "GMail seems to have a serious problem..."
    author: "ac"
  - subject: "Re: GMail"
    date: 2004-05-22
    body: "Don't see anything wrong..."
    author: "Fred Emmott"
  - subject: "Re: GMail"
    date: 2004-05-22
    body: "Seems to be a KPart running in Koqueror."
    author: "reihal"
  - subject: "Re: GMail"
    date: 2004-05-22
    body: "I think he was pointing at the \"Gmail does not currently support your browser\" text. This is easily fixable by adding the domain gmail.google.com/Mozilla 1.6 to the browser identification list."
    author: "Niek"
  - subject: "Re: GMail"
    date: 2004-05-24
    body: "Far better to leave it as Konqueror and click \"sign in anyway\".  That way they see Konqueror traffic, and QA the service with Konqueror, adding it to *their* list of supported browsers."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: GMail"
    date: 2004-05-25
    body: "Even better: don't ever use Gmail, due to the horrible privacy issues. \n\nPay for an email service, or set up your own IMAP + webmail server if you can.\n\nMaybe GMail could be useful as a file storage area, for strongly encrypted, not very private files, though? :-)"
    author: "Martin"
  - subject: "Re: GMail"
    date: 2004-08-04
    body: "gmail is MUCH better than any other webmail service.. it makes reading kde-cvs a snap :-)"
    author: "anon"
  - subject: "Re: GMail"
    date: 2004-08-08
    body: "Yes at least Google provides information regarding what they are doing with your information. Yes they do scan your email but only to provide you with targetted ads on the right side of the screen. A stated privacy policy, front and center, is much better than little or nothing. \n\nBesides Google turned down Microsoft for a take over bid. Either they are MORE evil (I don't get that impression) or they are helping to forward the revolution...\n\nI understand that Google runs a large RedHat cluster for most of their heavy analysis. "
    author: "Gordon"
  - subject: "Re: GMail"
    date: 2004-07-03
    body: "there is no mozilla 1.6 option in konq,\ncan't figure out how 2 get into gmail )-;"
    author: "VolcomPimp"
  - subject: "Re: GMail"
    date: 2004-07-03
    body: "> there is no mozilla 1.6 option in konq\n \n Then create one."
    author: "Anonymous"
  - subject: "Re: GMail"
    date: 2004-07-03
    body: "dono why my last post is on twice...\n anyway how do you create one?"
    author: "VolcomPimp"
  - subject: "Re: GMail"
    date: 2004-07-03
    body: "In Control Center, Internet & Network/Web Browser/Browser Identification"
    author: "Anonymous"
  - subject: "Re: GMail"
    date: 2004-07-12
    body: "well what if it is not an option in this list of identifications (ie no version of mozilla more recent than 1.2 ...)? \ncan we actually modify this list? "
    author: "pep"
  - subject: "Re: GMail"
    date: 2004-08-04
    body: "1) That is my question also. Can we just change the list??\n\n- I've tried both adding this to browser identification settings (chose 1.2), but nothing changed, when I try to login again.\n\n2) I've also tried just ignoring gmails 'does not work, but continue anyway' link, and that also just returns to \"index page\", after I enter username/pass.\n\nSo that gets me a bit further than 1)."
    author: "beast"
  - subject: "Re: GMail"
    date: 2004-08-19
    body: "Yes you can change the list, but no it doesn't help!\n\nOn Gentoo the folder with the browser identities is:\n/usr/kde/3.2/share/services/useragentstrings\n\nI edited mozoncurrent.desktop, to give firefox 0.9.3 user/id to no avail\ne.g. \nType=Service\nServiceTypes=UserAgentStrings\nX-KDE-UA-TAG=MOZ\nX-KDE-UA-FULL=Mozilla/5.0 (appSysName; U; appLanguage; rv:1.7) Gecko/20040806 Firefox/0.9.3\nX-KDE-UA-NAME=Mozilla\nX-KDE-UA-VERSION=1.7\nX-KDE-UA-DYNAMIC-ENTRY=1\n\nCan anybody with a mac post the 100% correct Safari >=1.21 user/id? That may well work..."
    author: "David Connolly"
  - subject: "Re: GMail"
    date: 2004-08-25
    body: "I tried using a current Safari User Agent string with Konqueror/KDE 3.2.3 on FreeBSD.  Still no go logging into gmail.  I get the same response that I get with no masqueraded user agent, but without the warning.\n\nUser Agent I used:\nMozilla/5.0 (Macintosh; U; PPC Mac OS X; en) AppleWebKit/125.4 (KHTML, like Gecko) Safari/125.9\n\n:("
    author: "David Early"
  - subject: "Re: GMail"
    date: 2005-10-07
    body: "te invito ven a gmail atte flora"
    author: "flora"
  - subject: "Re: GMail"
    date: 2005-11-17
    body: "may be somebody knows what exactly GMail needs to work correctly, so we can choose what identification string is the most correct? i thought that it based on safari engine, but it did not worked, so i needed to use Mozilla 1.7.3 identification to get it work"
    author: "SS"
  - subject: "Where is the source of the icons?"
    date: 2004-05-23
    body: "Both Everaldo Coelho and Jakub Steiner do their Icons in Adobe Illustrator. Where can one download the source files (.ai) of the icons? Or are the icons not Open Source at all?"
    author: "testerus"
  - subject: "Re: Where is the source of the icons?"
    date: 2004-05-25
    body: "ftp://ftp.suse.com/pub/people/wimer/"
    author: "tackat"
  - subject: "Adobe Illustrator"
    date: 2004-05-24
    body: "It's a shame that Adobe does not provide its products for Linxu and we lack behind in that area. However sodipodi is not that bad. \n\nAnd there is other software such as Css-Editor Style studio where there is no equivalent Linux tool."
    author: "gerd"
  - subject: "Yay, you're back!"
    date: 2004-06-16
    body: "Where did the site disappear to? I only noticed how often I checked for updates once it stopped responding."
    author: "Bob"
---
<a href="http://www.osnews.com/">OSNews.com</a> <a href="http://www.osnews.com/story.php?news_id=7102">interviewed</a> two of the most popular artists in Linux world of art: <a href="http://www.everaldo.com/">Everaldo Coelho</a> from KDE and <a href="http://jimmac.musichall.cz/">Jakub Steiner</a> (Jimmac) from the GNOME camp. Currently Everaldo works for Lindows and Jakub works for Novell. They were very kind to answer questions related with the art in Linux, its future and much more.


<!--break-->
