---
title: "KDE Conquers Astrophysicists with Kst"
date:    2004-04-28
authors:
  - "tchance"
slug:    kde-conquers-astrophysicists-kst
comments:
  - subject: "any library planned"
    date: 2004-04-29
    body: "to replace/extend Qwt for e.g."
    author: "azhyd"
  - subject: "Re: any library planned"
    date: 2004-04-29
    body: "It's a possibility, yes.  We have a library with a few classes in it but it's not very useful.  We're in the process of reworking the entire display engine so no library appear before that, for sure.  Which components would you want in a library?  "
    author: "George Staikos"
  - subject: "Re: any library planned"
    date: 2004-04-30
    body: "well one thing that I had to do was a plotter for something like a million of points ... Qwt and even pure Qt was too slow (my users did want a redisplay in a few seconds, ideally in less than a second).\n\nin the end I had to directly write to a pixmap in memory ... far from optimal since I had to redo some basic algorithms. How fast is kst at displaying?"
    author: "azhyd"
  - subject: "Re: any library planned"
    date: 2004-04-30
    body: "What about utilizing OpenGL for output? We had at least some success in improving plotting speed with our first attempts. And it may look better... \n\n"
    author: "Somebody"
  - subject: "Wrong title?"
    date: 2004-04-29
    body: "\"KDE Conquers Astrophysicists with Kst\"\n\nIsn't that a wrong title? Kst is not only being used by astrophyscists, but all scientists who need to plot some data, like engineers or any statisticians."
    author: "Sebastian"
  - subject: "Re: Wrong title?"
    date: 2004-04-29
    body: "so would \"KDE Conquerors scientists plotting data with Kst\" be better (thats KDE Conquerors, not scietists plotting data with Kst)?"
    author: "Corbin"
  - subject: "Re: Wrong title?"
    date: 2004-04-29
    body: "True, others are using it as well.  The main area of use is still astrophysics though.\n"
    author: "George Staikos"
  - subject: "What's wrong with the tutorial?"
    date: 2004-04-29
    body: "Again, I am trying to get Konqueror to display a text file inline, but it seems to crave an external viewer such as Kate or KWrite. Why is this? I changed the settings in Konqueror, but nothing changes. Is it a bug?"
    author: "m0ns00n"
  - subject: "Re: What's wrong with the tutorial?"
    date: 2004-04-29
    body: "The same happens in Mozilla. Must be a problem with the file, or how it is served."
    author: "Claus Wilke"
  - subject: "Ups"
    date: 2004-04-29
    body: "There is a broken image on the Kst Homepage."
    author: "MaX"
  - subject: "Re: Ups"
    date: 2004-04-29
    body: "Because the KST website's been slashdotted. :)"
    author: "tuppa"
  - subject: "What about LabPlot?"
    date: 2004-04-29
    body: "I do a lot of plotting work, and usually end up using some fortran code I had written a long time ago. About a month ago, I checked grace, kst and labPlot. I was a bit familiar with grace, but I found it too tedious to use. kst seemed simple enough, for what it was designed for. LabPlot looked great (it's also KDE), and seems to be actively developed. However, I could not install it and try it. Anyone using it? It looks very impressive!"
    author: "d Orb"
  - subject: "Re: What about LabPlot?"
    date: 2004-04-29
    body: "I can confirm your experience. I use GRACE heavily in my work (i am physicist).\nIt works very well as a standing alone unit, but there is a lot of difficulty concerning the data transfer with its environment (KDE) because GRACE is GTK based. And the usual life is such, that data sets born usually out of GRACE and the graphs must be exported somewhere else.\n\nI found by chance Labplot and made some efforts for installing it without success (on Debian). Labplot looks very promising and is fully integrated in KDE. Now I am wating a Debian package of this.\n\nNo I'll give a try to kst too."
    author: "devians"
  - subject: "Re: What about LabPlot?"
    date: 2004-04-29
    body: "Well, I thought, GRACE is just a backend. It depends which frontend you choose. Did you try the original Xmgrace based on good old Motif or the new GAIW, a Qt based frontend that fits well in any KDE desktop? I believe, Grace is still a real hammer app. Just imagine, the frontends could give a better access to its scripting abilities. \n\nI have to agree, a real good plotting app is still missing in the whole open source world. I, too, still use one of these selfmade progs that we developed at our university years ago (I'm a civil engineer).\n\nFor what I have to say is, that Kst is, however, one of the less functional plotting apps. So I wonder why it is introduced on kde.org...\n\n"
    author: "Sebastian"
  - subject: "Re: What about LabPlot?"
    date: 2004-04-29
    body: "Hmmm... My problem with grace/xmgrace/gaiw/whatever is that it is hard to do things I have to do all the time: histograms and pdfs, 2d colormaps, maybe some 3D mesh plot... Gnuplot does most of these (bar the 2d colormaps), and Matlab/Octave do all of them. I think that LabPlot covers all of my needs, but as the other poster said, there are no packages for Debian... :-("
    author: "d Orb"
  - subject: "Re: What about LabPlot?"
    date: 2004-04-29
    body: "2d colormaps are no problem for gnuplot, see\n\nhttp://www.gnuplot.info/screenshots/figs/term-mouse-OS2-pm3d-map.png"
    author: "AC"
  - subject: "Re: What about LabPlot?"
    date: 2004-11-09
    body: "As grace can export to eps, pdfs can be easily get using epstopdf.\n\nWhat's the problem with compiling labplot from source? Sure, you'll need to install a lot of librarys (e.g. the whole kde-dev), but it'l compiling without problems (Debian sarge).\n\nBut anyway, I prefer grace for the moment.\n\n\nri"
    author: "red.iceman"
  - subject: "Re: What about LabPlot?"
    date: 2004-04-30
    body: "Kst has very specific use, though that is expanding as we add features due to requirements of users.  It is featured on here because of the use it is being put to.  The users have specific needs, and those other apps don't meet their needs.  Kst was originally created by those users for the explicit purpose of filling their needs.  In particular, Kst is used for realtime display of huge databases.  It streams the graphs, automatically rescales them and adjusts the number of samples displayed, deals with dropouts and spikes, filters, etc.  It works with an internal object model that's used to represent and manipulate this data in various ways.  It supports highly optimized data formats that are not supported in any other apps, and it supports plugins to add more data formats.\n\nEventually Kst will likely do most of what those apps do.  Look at what it is and what the development roadmap is before you pass such judgement."
    author: "George Staikos"
  - subject: "Re: What about LabPlot?"
    date: 2004-04-30
    body: "I am sorry, I didn't want to offend any Kst developers. It seems to me that I did. I tried Kst when it was first anounced on kde-apps.org, looked awesome on the first look. It is just sad that I have to learn ANOTHER plotting app again and that I have now even more options where none of them fulfill all of my needs. Right now, however, I still prefer my own plot lib over Kst. And indeed, Labplot looks impressive and easy to use - if it would have only EPS and SVG export...\n\n"
    author: "Sebastian"
  - subject: "R"
    date: 2004-04-29
    body: "I think it is quite the same what R does for professionals + statistics.\n\nA graphical frontend (SPSS style) would be great."
    author: "andre"
  - subject: "HippoDraw"
    date: 2004-04-29
    body: "I just wanted to mention hippodraw:\n\nhttp://www.slac.stanford.edu/grp/ek/hippodraw/\n\nIt's a SLAC project with a very interesting feature set, and some similarities to kst."
    author: "Fernando Perez"
  - subject: "Installation"
    date: 2004-04-30
    body: "Would it be possible to provide installation instructions for people who are either unable or unwilling to install the program as root on their machine? I think this requirement needlessly reduces the number of people who can use/test the application.\n"
    author: "AC"
  - subject: "Re: Installation"
    date: 2004-05-01
    body: "Does this application require any special instructions besides the usual \"./configure --prefix=whereever;export KDEDIRS=$KDEDIR:whereever\"?"
    author: "Anonymous"
  - subject: "Re: Installation"
    date: 2004-05-02
    body: "I don't know. I tried the \"usual\" \n  ./configure --prefix=$HOME; make; make install\nbut that didn't work. It might be that all I missed was setting an additional environment variable, but if it is that easy it should me mentioned in the installation instructions, instead of the (sometimes dangerous or impossible) \"install as root\".\n"
    author: "AC"
  - subject: "Re: Installation"
    date: 2004-05-02
    body: "What was the error?"
    author: "Anonymous"
---
The Free Software community is constantly inundated with interesting new projects, but occasionally something crops up which is really special. <a href="http://omega.astro.utoronto.ca/kst/">Kst</a> is just such a project. Started by <a href="http://omega.astro.utoronto.ca/">Barth Netterfield</a>, an astrophysicist, as a personal project to plot data from his experiments, it has now taken on a life of its own, being used in numerous academic projects, and finding funding from several government agencies. Intrigued by this project's success, and with a little prod from co-developer <a href="http://www.kde.org/areas/people/george.html">George Staikos</a>, <a href="http://www.tomchance.org.uk/writing/kstinterview/">I interviewed Barth and George</a> about kst, Free Software and physics.


<!--break-->
