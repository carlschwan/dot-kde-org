---
title: "KDevelop TechNotes Issue #4"
date:    2004-12-25
authors:
  - "adymo"
slug:    kdevelop-technotes-issue-4
comments:
  - subject: "And let's not forget..."
    date: 2004-12-28
    body: "Alexander left out a few..\n\nClassview - lists your classes and methods, available for most supported languages.\n\nBookmarks - lets you quickly find your way back to that particular place in that rarely visited code...\n\nCTAGS - very fast tag based identifier lookup, available for most supported languages."
    author: "teatime"
---
The <a href="http://www.kdevelop.org/doc/technotes/navigation.html" >fourth issue</a> of <a href="http://www.kdevelop.org/doc/technotes/">"KDevelop TechNotes"</a> is out. This issue describes code navigation in the KDevelop IDE. Read on to learn about the most effective ways to navigate your code with keyboard shortcuts, context menus and "Quick Open..." wizards. The article also gives some useful information about toolview management with keyboard and also describes code navigation features available only to C++ developers.



<!--break-->
