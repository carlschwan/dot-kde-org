---
title: "KDE and the Linux Journal 2004 Readers' Choice Awards"
date:    2004-10-29
authors:
  - "bo'donovan"
slug:    kde-and-linux-journal-2004-readers-choice-awards
comments:
  - subject: "To cheer up the kontact guys..."
    date: 2004-10-29
    body: "... look at\nhttp://www.linuxnewmedia.de/Award_2004/en\n\nAccording to \nhttp://www.pro-linux.de/news/2004/7441.html\nkontact/Kmail/Mutt get the award for best eMail-Client.\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "KDE gaining more and more popularity over GNOME..."
    date: 2004-10-29
    body: "...That's maybe because the buttons in GNOME Dialogs are in wrong order ;-)"
    author: "Mel"
  - subject: "Re: KDE gaining more and more popularity over GNOME..."
    date: 2004-10-30
    body: "What I can't freaking understand is why the GNOME guys didn't simply make the button order configurable, like KDE did. It's so simple, and would have been sufficient not to lose them users in droves... I know GTK+ tends to be a bit underpowered when it comes to UI flexibility, but, gee... We're talking button order, something simple..."
    author: "Anonymous Coward"
  - subject: "Re: KDE gaining more and more popularity over GNOME..."
    date: 2004-10-30
    body: "Because they listen to usability experts who explain to them what front end users are and want. Problem is, it never fits what front end users are and want. \nUsability experts would fit perfectly in one of douglas adams books. :("
    author: "hmmm"
  - subject: "Re: KDE gaining more and more popularity over GNOME..."
    date: 2004-10-30
    body: "You mean right next to telephone sanitizers? ;-)\n"
    author: "cm"
  - subject: "what KDE tools lack"
    date: 2004-10-29
    body: "Is above all: marketing. Second option would be redhat-acceptance, but as we all know how that works out.."
    author: "jmk"
  - subject: "Kopete"
    date: 2004-10-29
    body: "Well, I don't need Instant Messaging in terms of ICQ/MSN/etc. but\nI would really like to use Kopete for IRC. I'm still using mIRC\non WINE because I could not find a decent IRC app for Linux.\nKVIRC is far too geeky - I mean look at the strange titlebars - the\nconfiguration dialogs: No way! So Kopete has a really useable UI\nbut unfortunately you just cannot add a new IRC server. This makes\nit impossible to use it for IRC. If this is solved one day - I'm gonna\nswitch. In the meantime I guess mIRC will do ;-)\n\nAt work I recently switched to Evolution because I need Exchange \ncapabilities. So far I didn't regret it. Evolution is really miles\nahead here: It \"just works\"(TM). Especially I like the color coding\nwhich exists in Thunderbird, too. A simple feature, but really useful\nand I simulated it in KMail somehow by assigning a key to \"Important\" - \nso I could at least have one \"color\". \n\nSo - all in all - many results are understandable IMHO but in general\nKDE is on the right track. Open Source applications usually tend to\nbe wallflowers for a long time but somehow get off the ground one day\nand surpass any competition (be it commercial or not).\n\n\n\n\n\n\n"
    author: "Martin"
  - subject: "Re: Kopete"
    date: 2004-10-29
    body: "Have you tried konversation (http://www.konversation.org/)?"
    author: "Michael Thaler"
  - subject: "Re: Kopete"
    date: 2004-10-29
    body: "I agree with Martin on this one. I haven't tried konversation yet, but it looks like xchat (my choice for the past 4 years), so if it's stable I'm happy! :)"
    author: "Jan Vidar Krey"
  - subject: "Re: Kopete"
    date: 2004-10-29
    body: "> unfortunately you just cannot add a new IRC server\n\nWhich version?\n\nI put a lot of elbow grease into making this work for KDE 3.3, so any version of Kopete since 0.9.0 should allow you to add new networks and servers.\n\nConfigure Kopete->Accounts->(edit IRC account)->Connection->Edit Network...\n\nIt's been tested by connecting to various internal and obscure networks.  "
    author: "Will Stephenson"
  - subject: "Re: Kopete"
    date: 2004-10-29
    body: "on Geoges recommendation i tried irssi.org, and I'm loving it.\n\nKonsole+irssi for me :)\n\nTIP: compile irssi, then there's nearly no dependencies.\n\n_cies"
    author: "cies"
  - subject: "Re: Kopete"
    date: 2004-10-29
    body: "Ya, I gotta totally disagree with your evolution evaluation.  Kontact started later than evolution but the newest versions have quicly surpassed it.  The LDAP support \"just works\" on our novell network, and the echange connector works pretty darn well.  Now I cannot live with features like GOOD encryption support, kopete integration, and saved searchs (what was called views in Lotus Notes.)  \n\nBobby"
    author: "brockers"
  - subject: "KDE's PR should become better."
    date: 2004-10-29
    body: "KDE definately needs a better 'Task Force'. That is - people not just going to events but also do better public relations on the net. E.g. more public work like announcing singular applications, offering more screenshots which are polished and demonstrating people in action how stuff looks like.\n\nI believe many people are simply lazy in not trying new things out and some of them even have prejudices because of this. I for example trust and like Kopete more than GAIM because I was able to fix my buddys list more than one time. GAIM used to trash my users list and even stored users outside the regular Tree (well the visible Tree structure) which then caused the app to crash more than one time. With Kopete I was able to grab these lost souls and place them back where they belong. The overall stability and appearance is definately for Kopete. Just one example.\n\nYou people need to go out and show the public why you believe your Desktop is the better choice for users and corporate and the old dual license debatte is quite pointless and people shouldn't jump on it all the time. You need to show the good aspects of your Desktop like powerful KIVIO, Umbrello, Kexi, KDevelop, Quanta Plus and so on. E.g. I recommend writing some user reviews about apps adding a bunch of screenshots to it and try releasing them on news sites (I think OSNews.com is quite a good place because of many visitors - even if it's more a GNOME centric site but I do believe Eugenia is open for such reviews and you clearly need to take the chance to do so). Every one or other month the one or other app in real life usage. This will clearly open the eyes of people, students, technicans and even secretaries.\n\nLet's say someone starts writing an article about Umbrello, the other KStars, Kopete and so on. Who is doing what is unimportant, what's important is that these real life applications with great quality is being pushed more into public."
    author: "ac"
  - subject: "Re: KDE's PR should become better."
    date: 2004-10-29
    body: "... so what will *YOU* be doing?? (*)\n\n-------\n(*) \"...what's important is that these real life applications with great quality is being pushed more into public....\""
    author: "I'll do kNX..."
  - subject: "Re: KDE's PR should become better."
    date: 2004-10-29
    body: "Well first you have to stop thinking in terms of promoting KDE over GNOME, and converting people. That kind of negative, sniping coverage puts most people off.\n\nThen you have to do something about it. The team of people who do promotion work is dedicated but small. There is documentation available to help you learn how to do promotion work:\n\nhttp://quality.kde.org/develop/howto/howtopromo.php\nhttp://quality.kde.org/develop/howto/howtomedia.php\n\nRead those, subscribe to the kde-promo mailing list, and start making the difference yourself."
    author: "Tom"
  - subject: "Re: KDE's PR should become better."
    date: 2004-10-29
    body: "Hi, \n\nI'm sure you are saying this in good spirit but keep in mind that most (if not all) of what you said is known. We just miss the people for doing this kind of work. \n\nStupid thing is that every KDE user can help here. Fire up a text editor and start writing a review or approach a developer for an interview. \n\nAlso some stuff you are suggesting is already being done (or worked on). We *HAVE* application reviews, interviews, the famous CVS digest and small news snippets on the Dot. But yes we need more of those. So please help this cool project by contributing some writings.\n\nFab\n"
    author: "fab"
  - subject: "Target audience for Kopete?"
    date: 2004-10-29
    body: "The features that now being worked on are business related. Examples are Groupwise support and Kontact integration. \n \nThis is great for me as I use those features, however I think if you want to make Kopete the number one IM you need to target a large group that currently uses IM; kids between 13-17 years old. \n \nLike it or not this group doesn't care about usability or Kontact integration. They want eye/ear-candy. The more the better. Emoticons are not hot anymore, screen filling flash-movies with sound and everything on it make the world go round. Webcam support is becoming another much request feature by this group. \n \nIf you want to make Kopete the number one IM, you need to make sure to please this group."
    author: "AC"
  - subject: "Re: Target audience for Kopete?"
    date: 2004-10-29
    body: "gaim has the advantage (just like mozilla*) to be x-platfom. Many people use FreeSoftware on their windoze desktop. They like moz*/gaim/vnc/putty/etc.\n\nSince KDE apps rely heavily on the desktop-framework they are 'better' (IMO) yet harder to port."
    author: "cies"
  - subject: "Re: Target audience for Kopete?"
    date: 2004-10-29
    body: "The problem is on KDE I want to use KDE apps, on GNOME I would want to use GNOME apps and on Windows I use Windows apps. So why bothering porting. Just because I can port an application from plattform A to plattform B doesn't necessarily make it a good application.\n\nThe maintainance for X-plattform compatibility is bigger than sticking with one framework for one specific task (in this case KDE) and use the resources that would get wasted for X-plattform compatibility to improve the application itself.\n\nBut if we look at Windows for example - how many native Windows applications exist on Linux ?"
    author: "ac"
  - subject: "Re: Target audience for Kopete?"
    date: 2004-10-30
    body: "Though I'm a Kopete user now, when I was a Gaim user I would install Gaim on all the Windows machines I had to work on. I don't want to have to adjust to some new interface based on what computer I'm working on. \n\nIt doesn't really cost that much to make KDE run natively on Windows, they are doing it, I bet we will see it in the not-too-distant future."
    author: "Ian Monroe"
  - subject: "Re: Target audience for Kopete?"
    date: 2004-10-29
    body: "True, but we need to remember who the competition is. All open source IM's combined is a very small fraction of the IM market. The competition is the closed source IM.\n\nMy point is that there is no way a large group of kids (thus a large group of IM-users) even consider switching to another IM unless that IM provides more eye/ear-candy. Kids don't care what IM they are using (or even the platform it runs on), they want to impress there friends with the latest eye/ear-candy."
    author: "AC"
  - subject: "webcam support"
    date: 2004-10-29
    body: "I can say without a doubt that webcam support is needed... any of my friends that I've tried to convert to using the KDE desktop under linux just cry out for this, and when they find it's not there, it gives them the excuse they want to say \"no thanks\" and wander back to Windows where webcams \"just work\".."
    author: "AC"
  - subject: "Re: webcam support"
    date: 2004-10-29
    body: "I agree, that's why I think it's most unfortunate that the philips webcam driver development is halted (closed source, or not). Most \"high-end\" USB-cameras use this chipset these days (all of the more expensive logitechs for example).\nSee: http://www.smcc.demon.nl/webcam/"
    author: "Jan Vidar Krey"
  - subject: "Re: webcam support"
    date: 2004-10-29
    body: "I agree too, webcam support is missing.  BTW the philips webcam\nsupport is back, if memory serves me well.  Last week I read an\nannouncement about this.  One of the statements was, that it is \nnow even better (code seperation and such).\n"
    author: "Richard"
  - subject: "Re: webcam support"
    date: 2005-02-07
    body: "Development of the PWC driver continues here :http://www.saillard.org/linux/pwc/\n\nhth"
    author: "James"
  - subject: "not so bad ..."
    date: 2004-10-29
    body: "Well, @first thanks to the whole KDE team for bringing the environment to the first place!\nThen, about programs climbing listings the wrong way: I think that having low rating this year is a must for making the jump the next one, after letting the waves calm and surprising the audience with the new coolness and stability of KDE3.4.\nWe're ruling, strongly.\nAlso thanks (my personal ones) to: kate, kmail, kopete, koffice (and amarok :-) teams.\n"
    author: "alice"
  - subject: "Best media player"
    date: 2004-10-29
    body: "There are no less than 3 (!) KDE apps in that list.\n\nXMMS? Seriously, I don't think people who vote have actually tried out other alternatives very much...\n\nIf only people gave the KDE apps a closer look, I'm pretty sure they would top more lists."
    author: "Arnold P"
  - subject: "Re: Best media player"
    date: 2004-10-29
    body: "So explain why the KDE alternatives are better than xmms.\n\nI like xmms because it plays every audio format in existence (well, a lot of them).  From NSF to FLAC to S3M to GBS, it plays everything I have.  It's also got a very simple playlist.  Just a list of files that are played in order (or, if you like, randomly).  It doesn't try to be \"intelligent\", it just does what I need in an audio player."
    author: "KDE User"
  - subject: "Re: Best media player"
    date: 2004-10-29
    body: "Ah yes, and I have one more gigantic reason I like xmms: It doesn't use arts.\n\nGranted, I haven't tried KDE's multimedia offerings in a while, but way back when everything demanded that arts be running.  Well, I don't want yet another daemon running, when it gives me absolutely nothing in return.  So my audio player must speak to OSS directly."
    author: "KDE User"
  - subject: "Re: Best media player"
    date: 2004-10-30
    body: "Use amarok. You can use gstreamer or xine both of which don't use daemons."
    author: "Ian Monroe"
  - subject: "Re: Best media player"
    date: 2004-10-30
    body: "I have to second the recommendation for amaroK.\n\nI've watched media players come and go over the past four years, and XMMS has always won because it supported many formats (notably mp3, Ogg V., and flac) without hogging the CPU (on this PII), or crashing--all while including useful plugins for ALSA (when it was brand new), jackd, gapless playback, and, though less useful, even an alarm clock.  Also, I wasn't required to have correct metadata to be able to find my music; the playlist was simple, and chosen from the filesystem.\n\nNow, amaroK includes some of the features I have long found useful (and quite basic):\n*XMMS-style play control window\n*Playlist that allows for easier filesystem-based addition than XMMS\n*Working gapless playback and optional crossfade (without the clutter from the XMMS crossfade plugin)\n*Manipulation of playlist that is easier than XMMS (e.g., randomize, clear, append, replace, and save)--no more tiny XMMS buttons\n*Ability to play a wide range of file formats (including flac and Ogg V.)\n*By default, keyboard playback shortcuts match those of XMMS\n*The (working, reliable) panel icon is minimalistic and useful: it only controls playback and volume\n\nSome features that, for me, have given amaroK the edge are:\n*A sidebar that allows multiple ways to select music (there is a filesystem browser--similar to what is in K3B--, a search pane, a meta-data browser--similar to Juk/Rhythmbox/Muine--and there is an 'intelligent' pane)\nThe sidebar actually works quite well.  The panes you don't want are kept well out of the way.\n*Uncluttered configuration screens (relative to XMMS)\n*An easy ability to select output plugins (In both XMMS and amaroK, I like being able to easily switch between aRts for dedicated listening--aRts is still one of the best sounding sound servers/processors--and something that plays nicely with the computer, e.g. xine-plugin for amaroK and ALSA for XMMS.)\n*By default, the buttons and fonts are large enough to be easily used\n*The keyboard shortcuts work when the application is not in focus\n\nIf you become frustrated with XMMS, give amaroK a try. Once the bottom sidebar button is found, things will feel quite familiar.\n"
    author: "dw"
  - subject: "Re: Best media player"
    date: 2005-04-25
    body: "I too always just stick with XMMS until 3 weeks ago, when I finally tried amaroK.  amaroK is incredibly awesome.  Just try it, you won't go back."
    author: "kundor"
  - subject: "Re: Best media player"
    date: 2004-10-30
    body: "It uses GTK1 so has a clunky file open and open directory dialogues. True, it plays a whole lot a bunch of filetypes. So if you have some esoteric (and it has to be pretty darn esoteric to not be supported by one of amaroK's sound servers) files, that might be good reason to use XMMS. Though even then you should probably think about using the Beep Media Player. I think its kind of sad that XMMS won, its such a dinosaur."
    author: "Ian Monroe"
  - subject: "Re: Best media player"
    date: 2004-10-30
    body: "sometimes simplicity will win."
    author: "salsa king"
  - subject: "Where are the links???"
    date: 2004-10-29
    body: "Isnt this story published a bit preliminary?? The online version of Linux Journal does not mention the results of the Awards at all. So I was curious and searched a bit.... Then I found this:\n\n--> . . http://www.linuxjournal.com/article.php?sid=7724\n\nIt is signed \"Posted on Monday, November 01, 2004 by Heather Mead\". Hmmmm... In _my_ part of the world it is still Saturday, the 29th of November, and it will be at least 15 more hours before it will be 1st of November in _her_ part of the world.\n\nSo.... I am surprised to see this sentence in the original Linux Journal article (online version):\n\n> . . \"For several years now, KDE and GNOME have finished first and \n> . .  second, respectively, with an ever-increasing distance between \n> . .  the two. This year, KDE received two votes for every one GNOME \n> . .  received.\"\n\nI had read similar above, in Barry O'Donovan's article already. First thought was: this is a typically flame-bait, biased, triumphant KDE fanboy verbal excrement, fuelled by Barry's self-proclaimed \"writing-while-drinking\" mood. \n\nTo my surprise, it did flow from (Linux Journal Senior Editor) Heather Mead's feather, and Barry just stole the quote without using quotation marks...\n"
    author: "Huh????"
  - subject: "Re: Where are the links???"
    date: 2004-10-29
    body: "This \"story\" or, as I'd prefer to call it, report is not preliminary. It is a report on the survey results published in the November issue of the Linux Journal which is sitting on my desk after arriving here (Ireland) in the post last Tuesday. If it's arrived here then I imagine it's available in the US and elsewhere by now too.\n\n\"this is a typically flame-bait, biased, triumphant KDE fanboy verbal excrement\"\n\nIt's nothing of the sort. This is a report/review/summary of KDE's performance in the above mentioned survey. The bad results are mixed in the the good - along with some personal editorial. It doesn't pretend to be anything else. \n\nAs for:\n\"Barry just stole the quote without using quotation marks...\"\n\nAnd you're suggesting flame-bait in my writing? Firstly, I didn't steal any quote. Secondly, where I did quote directly from the article you will find quotation marks and credits to Heather. There are only so many ways to write the same thing in a few sentances. They may appear similar and they may convey the same message but it is not intended as a quote. I believe the word you're looking for is \"paraphrase\".\n\nThe reader of my report is never left in any doubt about the source of my information nor my take on it - afterall, the story was filed under \"KDE Sucess Stories\".\n"
    author: "Barry O'Donovan"
  - subject: "Re: Where are the links???"
    date: 2004-10-29
    body: "> It is signed \"Posted on Monday, November 01, 2004 by Heather Mead\". Hmmmm... In\n> _my_ part of the world it is still Saturday, the 29th of November, and it will \n> be at least 15 more hours before it will be 1st of November in _her_ part of \n> the world.\n\nWell...in my part of the world it is still Friday 29th October :)"
    author: "Nadeem Hasan"
  - subject: "QT applications..."
    date: 2004-10-29
    body: "The sad story about KDE, they may win the desktop category but suck at the QT applications.\n\n\nGNOME by the other hand, may be second on desktop category by win most of the applications with GTK.\n"
    author: "..."
  - subject: "QT applications? Let' see..."
    date: 2004-10-29
    body: ">>> GNOME .... win most of the applications with GTK. <<<\n-----\nWell, maybe this is just because not _that_ many different application categories were suggested. Also, already now kontact is better than evolution (and more stable!) -- wait for another year and most people will have realized it. \n\nAnd currently? Let's see:\n\nCategory Web Development Environment:\n --> Quanta Plus wins hands down.\n\nCategory Integrated Development Environment:\n --> KDevelop lets all the rest pale.\n\nCategory Desktop Publishing:\n --> Scribus has no equally levelled competitor.\n\nCategory Audio Player:\n --> JuK and amaroK. May not win because the both will steal each other's votes. (And if xmms wins: this is not GTK, or is it?)\n\nCategory Video Application:\n --> kdetv wins hands down.\n\nCategory Swiss-Army-Knife-to-Write-Your-Own-Tools:\n --> kommander wins hands down.\n\nCategory File Manager:\n --> Krusader is only matched by Konqueror.\n\nCategory Scientific/Educational Application:\n --> KStars is simply the best.\n\nCategory GUI for Secure Remote File Management:\n --> fish will win hands down.\n\nCategory Desktop Search Engine:\n --> kio_locate wins hands down.\n\nCategory Enterprise Printing:\n --> KDEPrint beats them all.\n\nCategory Enterprise Desktop Rollout, Administration and Maintenance:\n --> kiosktool blows the competition out of the water.\n"
    author: "Sad story?"
  - subject: "Re: QT applications? Let' see..."
    date: 2004-10-29
    body: "KDE shouldnt compare with GNOME. That is too short-sighted. Don't you want World Domination?\n\nCompare yourself to Mac OS X and Windows Professional XP please! If you beat them, no-one will ask for GNOME any more.\n\nSo let's see again:\n\nCategory Web Development Environment:\n --> Quanta Plus wins hands down.\n \n Category Integrated Development Environment:\n --> KDevelop lets all the rest pale.\n \n Category Desktop Publishing:\n --> Scribus has no equally levelled competitor.\n \n Category Audio Player:\n --> JuK and amaroK. May not win because the both will steal each other's votes. (And if xmms wins: this is not GTK, or is it?)\n \n Category Video Application:\n --> kdetv stands no chance yet against Quicktime and all the rest!\n \n Category Swiss-Army-Knife-to-Write-Your-Own-Tools:\n --> Does kommander win against VBA??\n \n Category File Manager:\n --> Krusader and Konqueror are indeed a good choice against Windows Explorer.\n \n Category Scientific/Educational Application:\n --> KStars is not enough to win a field were you dont need one, but virutally hundreds of apps....\n \n Category GUI for Secure Remote File Management:\n --> I agree: fish will win hands down.\n \n Category Desktop Search Engine:\n --> kio_locate has to oompete against Google Desktop Search and a lot more. Does it come close? Not yet, far from it!\n \n Category Enterprise Printing:\n --> KDEPrint beats most of the competition. But it can't do Color Management yet.\n \n Category Enterprise Desktop Rollout, Administration and Maintenance:\n --> kiosktool starts to get close to the MS Windows Policy Editor. But it is not yet there.....\n\nSo overall, the results are mixed. Lots and lots remains to be accomplished. And while we do that, the competition continues to improve too.... So no time to get self-satisfied. Let the GNOMEies do their own stuff (or cooperate with them) -- concentrate to compete with Bill Gates' and Steve Jobs' products!!"
    author: "What about Windows?"
  - subject: "Re: QT applications? Let' see..."
    date: 2004-10-29
    body: "And currently? Let's see:\n \n Category Web Development Environment:\n --> Quanta Plus wins hands down.\nOK\n \n Category Integrated Development Environment:\n --> KDevelop lets all the rest pale.\nOK\n \n Category Desktop Publishing:\n --> Scribus has no equally levelled competitor.\nOpenoffice?\n \n Category Audio Player:\n --> JuK and amaroK. May not win because the both will steal each other's votes. (And if xmms wins: this is not GTK, or is it?)\n\nNobody knows JuK and amaroK as you can't install them easily on your KDE 3.1.4\n\n Category Video Application:\n --> kdetv wins hands down.\n\nkdetv looses against Tvtime.\n \n Category Swiss-Army-Knife-to-Write-Your-Own-Tools:\n --> kommander wins hands down.\n\n \n Category File Manager:\n --> Krusader is only matched by Konqueror.\n\nKonqueror has to many toolbar icons (e.g. those stupid \"copy\", \"cut\", \"paste\" icons) and nobody knows that you can easily remove them and add better ones (e.g. \"select profile\").\n \n Category Scientific/Educational Application:\n --> KStars is simply the best.\n\nKstars is feature limited. Xephem has other important features.\n \n Category GUI for Secure Remote File Management:\n --> fish will win hands down.\n\nNobody knows that fish:// does exist. Nobody knows that sftp:// exists. Because there is no hint in the Gui, not even a bookmark in the default Konqueror bookmarks.\n \n Category Desktop Search Engine:\n --> kio_locate wins hands down.\n\nNobody but a few kde-apps.org watcher know that kio_locate exists.\n \n Category Enterprise Printing:\n --> KDEPrint beats them all.\n\nYea, especially the integration of color management ... Not!\n\n Category Enterprise Desktop Rollout, Administration and Maintenance:\n --> kiosktool blows the competition out of the water.\nOK\n\nDigital camera:\n --> digiKam is nice. Gwenview (KDE-app!), too."
    author: "Asdex"
  - subject: "Re: QT applications? Let' see..."
    date: 2004-10-29
    body: "> Category Scientific/Educational Application:\n>  --> KStars is simply the best.\n>  \n> Kstars is feature limited. Xephem has other important features.\n \nHm, XEphem rather targets astronomical geeks with some of it's features and lacks quite some stuff compared to kstars already. \nKStars is rather written as an educational application while xephem targets a very narrow group of people with an old-style user interface.\n\nTorsten Rahn"
    author: "Torsten Rahn"
  - subject: "Re: QT applications? Let' see..."
    date: 2004-10-29
    body: "> --> Scribus has no equally levelled competitor.\n> Openoffice?\n \ni don't think you understand what Scribus does, as it isn't even nearly the same as was Open Office does.\n\n> Category Audio Player:\n> --> JuK and amaroK. May not win because the both will steal each other's\n> votes. (And if xmms wins: this is not GTK, or is it?)\n> \n> Nobody knows JuK and amaroK as you can't install them easily\n> on your KDE 3.1.4\n\nas people upgrade that'll change. the new round of distros all have KDE 3.3. life is happy.\n\n> Kstars is feature limited. Xephem has other important features.\n\nindeed; but KStars is more than fine for educational purposes and has an interface that isn't a haunting from the 1980s. that said, there are a lot of other very impressive KDE/Qt scientific apps out there. \n\ni agree with much of the rest of what you said. lots of work to be done =)"
    author: "Aaron J. Seigo"
  - subject: "Heh."
    date: 2004-10-29
    body: "My browser preferences are like, completely backwards. For me it's Opera > Konqueror > Firefox. ;)"
    author: "Illissius"
  - subject: "Re: Heh."
    date: 2004-11-01
    body: "I think that browser preference depends heavily on the computer you have: when I had a Celeron333 it was Opera or Konqueror only of course.."
    author: "renox"
  - subject: "Kopete is very good but miss some things"
    date: 2004-10-29
    body: "Look at Mac productsand check Adium and Proteus they are based on gnome but look at the UI and ho you cand modify it it's jsut awesome.\n\nI wish Kopete had the possibility to customize the buddy list to actually see their buddy icon on the right. i wish i could make the buddy icon bigger on the main conversation windows not only by roll over it.\n\nI wish kopete had more conversations style look at thoses supported by Adium it makes me dream.\n\nKonqueror need to Integrate Gecko in 3.4 and make it support all Kparts  and this will absolutly rocks and will win all awards IMHO.\n\nI agree the the competition is not gnome but i'ts Apple and MS. Gnome is horrible IMHO buttons are too big defaults icons are old and outdated. applications are big fat and slow compare to their KDE counterpart."
    author: "!nkubus"
  - subject: "Re: Kopete is very good but miss some things"
    date: 2004-10-29
    body: "> I wish Kopete had the possibility to customize the buddy list to\n> actually see their buddy icon on the right\n\nyou can. try out KDE 3.3 and be delighted. the contact list is really quite cool in 3.3.\n\n> I wish kopete had more conversations style look at thoses\n> supported by Adium it makes me dream.\n\nthere are quite a few already in 3.3 ... though it would be nice if it could group all msgs from the same person sent in a row into one \"block\" as Adium seems to do.\n\n> Konqueror need to Integrate Gecko in 3.4 and make it support\n> all Kparts and this will absolutly rocks and will win all awards IMHO.\n\nalready done. the work is in Mozilla's CVS tree."
    author: "Aaron J. Seigo"
  - subject: "Re: Kopete is very good but miss some things"
    date: 2004-10-30
    body: "Kopete's style engine is *very* extensible and easy to theme for anyone who knows XSL. I know that not everyone does though, so I will make you (and anyone else reading) this offer.\n\nSend me a screenshot of whatever style you want from your favorite IM client and I will do my best to duplicate it. When I have it done I will email it to you. If I feel it is indeed a good style, I will even add it to the Kopete CVS for all to share.\n\n\n"
    author: "Jason Keirstead"
  - subject: "Re: Kopete is very good but miss some things"
    date: 2007-05-28
    body: "hi,\n\ni'm new to linux and kopete in general.\n\ni am a recent user that just switched from windows XP.\nfor the most part, i agree with all that has been said :p, kopete\nis definitely my favourite messenger client so far (compared with gaim, i guess)\ni haven't yet scoped out the others like amsn, etc...\n\ncan anyone comment on which ones are best? also, i find the buddy icons in kopete to be kind of annoying (the blue heads), perhaps if we could change this to something more aesthetically pleasing it'd be great :p...\n\ndoes anyone know how to do this?\n\nthanks\n-derek"
    author: "Derek"
  - subject: "KMail (imap) still sucks..."
    date: 2004-10-29
    body: "it's quite unsable with many imap folders. Responsive is awful.\nMoving folders doesn't work. Some versions are worser than\ntheir predecessor. But 3.3.1 seems to work.\n\nAnd konqueror rules...I need FireFox seldom."
    author: "Mutator"
  - subject: "Re: KMail (imap) still sucks..."
    date: 2004-10-30
    body: "I think the last sentence in the 1st paragraph is the most important.  \n\nKMail has just gotten to a point where it can be positively compared.  It will take a bit of time to be recognized.  And a few enhancements and it's integration into Kontact will be noted!\n"
    author: "Ross"
  - subject: "the gtk+ apps..."
    date: 2004-10-30
    body: "just reading the list, and in all reality, alot of GTK+ (v1/v2) applications won hands down. Mozilla (GTK1/2 based), XMMS (GTK1 based), Gaim (GTK2 based), Gimp (GTK2 based). sure KDE won the Favourite Desktop Environment, and contrats for the win. But you have to take your hats off to the many GTK+-based apps that still reign on the desktop."
    author: "salsa king"
  - subject: "Re: the gtk+ apps..."
    date: 2004-10-30
    body: "I think the reason is \"alien app\" factor i.e. Gtk apps fit in very nicely with KDE desktop, esp. if you use something like QtGtk but KDE apps in GNOME take very long time to start (kded not running) and are percieved as bloated and ugly. Or you might say: gnome users make very little effort to have kde apps running smoothly. The only Kapp they use is K3b.\n\nUsers of gnome and other env.s just hate kde and anything to do with it, including apps whose name starts with K."
    author: "Ted"
---
KDE and its various applications have featured very well in the <a href="http://www.linuxjournal.com/">Linux Journal</a> 2004 Readers' Choice Awards which <a href="http://www.linuxjournal.com/article.php?sid=7724">are published</a> in the November issue of the magazine. Most importantly, in the category of "Favorite Desktop Environment", KDE came in first followed by GNOME. The trend over recent years has shown KDE gaining more and more popularity over GNOME and this year KDE received two votes for every one that GNOME received.






<!--break-->
<p>
There was a bit of bad news for <a href="http://kmail.kde.org/">KMail</a> which dropped from second place last year to third place this year in the "Favorite E-Mail Client" category. Mozilla came in first with Ximian Evolution in second place. <a href="http://www.kontact.org/">Kontact</a> received no mention in the write-up but may have taken a share of KMail's votes. This dual-heading for KMail may have confused both the voters and the survey editors which omitted a number of obvious choices in many of the categories.

<p>
<a href="http://www.kdevelop.org/">KDevelop</a> came in fourth in "Favorite Development Tool" behind the old favorites of GCC and Emacs in first and second places respectively. IBM's Eclipse eclipsed KDevelop out of third place. Also-rans included vi and vim of which the survey editors put into two separate categories which reinforces my misgivings on the possible Kontact/KMail mix-up. Not that I'm a sore loser!

<p>
The field of "Favorite Text Editor" was by dominated by 'the old reliables' - vim took first place, followed by "vi and vi clones" in second and GNU Emacs in third. <a href="http://kate.kde.org/">Kate</a> came "in at a strong number four" and the editor, Heather Mead, asks the question "Could readers finally be ready for a modern user interface in an editor...?"

<p>
A surprise to me was that <a href="http://www.tjansen.de/krfb/">KDE Desktop Sharing</a> came in third behind Webmin and YaST in "Favorite System Administration Tool". Congratulations to the development team behind that. 

<p>
There was also some bad news for the <a href="http://www.koffice.org/">KOffice</a> team which didn't make the top three in "Favorite Office Program" or a mention in the editorial on the category. As you might guess, OpenOffice.org came in a clear first and miles ahead of the competition. AbiWord took second place followed by StarOffice.

<p>
<a href="http://kopete.kde.org/">Kopete</a> jumped a place to second in "Favorite Instant Messaging Client". However it only knocked Jabber back into third by 7 votes. Gaim was the clear winner in first and Kopete has some ground to make up before it over takes it. In my opinion, Kopete is a far superior product but lacks the public awareness that Gaim has gained through outlets such as regularly featuring in the top 20 active projects of SourceForge as well as winning it's "Project of the Month" on more than one occasion. The Kopete team should consider a big push to coincide with the realise of KDE 3.4 concentrating on advertising its new features that are in development; such as its integration with Kontact in a similar way as MS Outlook and Messenger. With any luck you'll rise to first for next year.

<p>
Finally, in the category of "Favorite Web Browser", Konqueror retained its second place position with Mozilla again taking first. Opera took third and Galeon ended up in fifth place. Firefox was not offered as a choice and some users wishing to vote for it used the Mozilla box or the "Other" and this will no doubt have an effect on the results.

<p>
All in all I think it's a great set of results for KDE and all those who contribute to the project should be proud of their work and the recognition it is receiving.