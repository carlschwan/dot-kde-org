---
title: "NewToLinux: KDE is so cool because..."
date:    2004-02-10
authors:
  - "tchance"
slug:    newtolinux-kde-so-cool-because
comments:
  - subject: "Nicely done"
    date: 2004-02-10
    body: "I've used KDE off and on since before 1.0, and I can honestly say I learned one thing already. :-D  I don't see me using kprinter from every app, but I can see the benefit, I suppose."
    author: "regeya"
  - subject: "Re: Nicely done"
    date: 2004-02-10
    body: "I have to agree! \nVery nicely put together and already some very good content. I'm also a long time KDE user but I still find useful things in this text (or maybe because). \nI think there has been a lack of things like this, what I have found anyway.\nOff the top of my head I recall one thing that irks me from time to time;\nkioslaves!\nI know there is a whole bunch of these, but I'm not sure what they are and how to use them. Of course I know of and use several of them, but my point is that my knowledge has come through the grapevine, I haven't found any actual documentation or even a listing of which are available. Is there any documentation? I'm probably lousy at searching."
    author: "spamatica"
  - subject: "Re: Nicely done"
    date: 2004-02-10
    body: "\"... or even a listing of which are available. \"\n\nHave a look in \"K-Menu -> System -> Info Center -> Protocols\" \n\nThat's a list of the installed ioslaves, and some of them are even documented! ;)"
    author: "teatime"
  - subject: "Re: Nicely done"
    date: 2004-02-10
    body: "Ah, took some searching but there it is! :)\n\nThanks!"
    author: "spamatica"
  - subject: "Re: Nicely done"
    date: 2004-02-10
    body: "awesome.\nnow why isn't that sort of thing in the kde tips page.\n\nshame it doesn't offer any options to remove/install slaves. oh well."
    author: "c"
  - subject: "Configuring protocols?"
    date: 2004-02-11
    body: "very nice! How do you configure the protocols or add new ones? I am asking because there is telnet: and rlogin:, but not ssh: for opening a remote shell. Any way I could add this?\n\nRoman"
    author: "Roman Maeder"
  - subject: "Re: Configuring protocols?"
    date: 2004-02-11
    body: "Have a look at the fish:/ protocol. :)"
    author: "teatime"
  - subject: "Re: Configuring protocols?"
    date: 2004-02-12
    body: "but the fish: protocol gives me file manager type access, just like ftp:, smb:,\netc. I want a remote shell, like telnet:host or rlogin:host gives me. I looked at the configs and both are implemented in terms of ktelnetservice. If ktelnetservice could handle ssh:host URLs in the same way it now handles telnet: and rlogin: URLS, this would work.\n"
    author: "Roman Maeder"
  - subject: "Re: Nicely done"
    date: 2004-02-10
    body: "KIO is the \"big KDE secret\". They just work. They were announce with the unveiling of KDE 2 as one of the coolest new features and since have been like an underground secret. You should not feel bad not knowing about them. As I've said before this should be promoted more on the KDE site and in docs. Since I lead the Quanta project and of course web developers expect to connect (but calendar users don't even though they can) we seem to be the \"dumping ground\" for user requests of \"why don't you have an FTP manager?\" My all time favorite. ;-)\n\nHere's the simple explanation. Open Konqueror and look at the location bar. It will say \"file:/home/user\". File is the protocol, \":\" means it's accessing a machine by protocol (okay this may be a loose definition). The \"/\" means local and root since it precedes everything after :. If it uses \"//\"it means remote essentially. Using KIO for ftp would look like \"ftp://user@domain[/path]file]]\" and what is cool is that you can do this on the top of the file dialog and see the directory listed. This is where KDE needs to indicate that something other than \"file:/\" is available. The \"user@\" means that this is a password protected account and it will prompt you for a password. This can be bypassed with \"fish://user:passwd@domain\". This will autopass. Fish BTW is SSH/SCP.\n\nIf you want to know what is on your system look on the system menu for \"Info Center\". I can't promise it will be there because distros sometimes muck with stuff like this but if you select \"Protocols\" you will see a list on the left with with a list of protocols and on the right you will be able to read about them. I'll include a screenshot of mine.\n\nOn KDE \"file:/\" is \"just another protocol\". ;-)\n\nEric Laffoon\nhttp://quanta.sf.net/"
    author: "Eric Laffoon"
  - subject: "Re: Nicely done"
    date: 2004-02-10
    body: "on the subject of fish:// is there a way to get konqueror to open a konsole window (Window/Show Terminal Emulator) on the remote site rather than (or maybe as well as) the local one? This after all is the main reason for using ssh rather than ftp"
    author: "Peter Robins"
  - subject: "Re: Nicely done"
    date: 2004-02-10
    body: "Try kssh.\n\nThere should also be an option in konsole's selection list when you open a new console tab.\n"
    author: "Bart"
  - subject: "Re: Nicely done"
    date: 2004-02-10
    body: "are we talking about the same thing? From a konqueror window in file:/ protocol I can press CTL-T or Window/Show Terminal Emulator to get a konsole window - fine. If I'm in fish:// protocol I can also get a konsole window but on the local machine not the remote. It would be nice if I could get a remote prompt as well. I don't want to open a new ssh session - I already have one open!"
    author: "Peter Robins"
  - subject: "Re: Nicely done"
    date: 2004-02-10
    body: "It's a great idea, however bugs.kde.org is probably the most appropriate place for it -- files under wishlist probably.\n\n(still bored? read my blog at http://tblog.ath.cx/troy)"
    author: "Troy Unrau"
  - subject: "KIO needs some help..."
    date: 2004-02-10
    body: "Agreed.  I've pointed this out to friends quite a bit, and most don't get it.  I have to admit that the idea of KIO is great, it is a 'hidden' feature and sometime frustrating to use consistantly. (Till 3.2, I avoided Konqueror and used Nautilus on the network since I couldn't connect to SMB shares reliably.  Nautilus would crash after 5 minutes or so, though it would connect.  Now, Konq is working quite well!)\n\nHere are some ideas that come to mind to make it more visible and practical;\n\n1. Add it explicitly to the KDE file open dialog.\n\nSome apps are more likely to use certian protocols.  Just as you can filter by file type, filter by KIO type.  There are a variety of ways to pull this off -- from radio buttons through to small changes in the Location field or adding history pick lists to the dialog itself (not bookmarks!).\n\n2. In the file dialog, lock the KIO type so that it isn't so easily erased.\n\n* Click on the URL to edit, click twice and it should select everything after the :/ or ://.  Click again and select everything.  \n\n* If the URL typed is \"file:/ftp://ftp.mydomain.com/pub/website\", chop off the \"file:/\" automatically, and execute the rest of the line.\n\n3. Remember and guess what protocol to use.\n\nExamples;\n\n* If someone types in a non-local address, ask them if they ment a network address, and what type.  Provide a list or offer to guess some destinations (FTP, WebDav, ...).\n\n* Integrate the history of other apps (shell and X) so that they work with KDE KIO-aware apps and bisa-versa.  An often used URL in one place should appear automatically other places.  (I know, not trivial...though that brings me to the next comment...)\n\n4. Integrate or wrap other non-KDE applications.\n\nI've typed in KIO URLs in a variety of programs by reflex, and only after hitting enter and getting a rude reminder did I realize that -- duh -- Mozilla Composer or whatever doesn't understand them.  (erm, not that I use Composer...)  \n\nI really wish this integration 'just worked'!\n\n5. Support KIO at the shell prompt.\n\nSame as 4, though it would be nice if Bash were KIO-aware.\n\n*  *  *\n\nThat's all I can think of right now.  As you can see, I'm full of great ideas!  People say to me \"Andy, you're full of it\" all the time!"
    author: "Andy Longton"
  - subject: "Re: KIO needs some help..."
    date: 2004-02-10
    body: "Please file separate reports at http://bugs.kde.org if not already filed."
    author: "Anonymous"
  - subject: "Re: KIO needs some help..."
    date: 2004-02-11
    body: "Personally, I have filed on #1 several times, and it has been kicked back to me as well that it was a waste of time.\n\nIn fact the whole concept is partially broken in my mind. There should be no difference between saving to a file, to ftp, to http, to e-mail, to fax, to IM, etc.\nLikewise, opening a data stream should be the same way. \n\n\n\nThe filedialogs should consist of a pluggable approach. A file selection should be a simple plug-in, as should DB, ftp, http, e-mail, fax, IM etc. This would allow for some interesting ways to search in the openings. \nFor example, the addressbook would become a great deal more important, if we could simply look in there for somebodies e-mail, im, fax, phys. addr, etc.\nLikewise, doing a google search(or any other engines) for a network data stream would make sense.\n\n\nThe concept of location is so right on the money, but then we break out printers from disk. Yet, they are one and the same. You are saving data to something.\n\n\nI would love to see the words \"open\" and \"save\" replaced with \"get from...\" and \"send to...\". "
    author: "a.c."
  - subject: "Re: KIO needs some help..."
    date: 2004-02-11
    body: "I had a discussion with Waldo Bastion on this the other day and he thought it made sense. Because Quanta focuses on web development this is a much bigger issue for us than other projects. In fact it is a never ending pain, and that is the surest way to know that you have a problem in your interface design!\n\nI am really busy this week. Shortly I have a few things to bring to core KDE developers. This one is on the top of my list. You can count on something happening here by the next major release!\n\nIf anyone has a really good clear interface design for the dialogs that they could send a partial mock up of in a ui file or graphics feel free to send it to me."
    author: "Eric Laffoon"
  - subject: "here is one."
    date: 2004-02-12
    body: "http://www.windbournetech.com/~rraab/io.tgz\n\nThis is from 3 years ago, when I had time and first started pushing this.\nI have actually submitted this (and a slight re-design on kio) 3 times.\n\nI was looking for the code that I had with this, but ....\n\nI seem to have lost some other code as well. \n\nrraab ( AT ) windbournetech.com\n\n\n"
    author: "a.c."
  - subject: "Re: KIO needs some help..."
    date: 2004-02-10
    body: "The answer to 4 and 5 are the same: What we really need is to have kernel support for kioslaves! There is a user space file system that can do this today, check out the KIO-FUSE-Gateway, http://kde.ground.cz/tiki-index.php?page=KIO+Fuse+Gateway . We need to push for something similar in the standard kernel in the future.\n\nAnother kernel space project that is interesting for KDE is Supermount(-NG.sf.net), and of course the new hotplug/udev-framework that is appearing. I want hardware and removable media to become immideately visible in KDE directly on connect!"
    author: "Jonas B."
  - subject: "Re: KIO needs some help..."
    date: 2004-02-11
    body: "It's not fully transparent, but you can use kioexec to use kioslaves from a shell prompt. Try this:\n\nkioexec cat http://www.microsoft.com/"
    author: "AC"
  - subject: "Re: KIO needs some help..."
    date: 2004-02-12
    body: "Very nifty!  What protocols does it work on?  Here's something I just tried;\n\nkioexec cp audiocd:/MP3/* .\n\n...and no luck.  Oddly enough, doing this;\n\nkioexec cp audiocd:/track01.cda .\n\nstarts the copy process, pushes it to a /tmp directory, and then Bash shows \"cp: missing file argument\"."
    author: "Andy Longton"
  - subject: "Re: KIO needs some help..."
    date: 2004-02-12
    body: "> kioexec cp audiocd:/MP3/* .\n\nMaybe lowercase mp3 as directory?\n\n> kioexec cp audiocd:/track01.cda .\n\nMaybe it has problems to translate the dot as current directory? Could you try it with a full path as destination directory? If it works with the full path you could then file bug report.\n\n"
    author: "anonymous"
  - subject: "Re: KIO needs some help..."
    date: 2004-02-14
    body: "I believe the wildcard expansion is done by the shell when you type enter, not by \"cp\" later on in the process.  Since bash doesn't know about ioslaves, it can't expand the wildcard properly.  kioexec is a hack and it will probably only work with single files, not wildcards or directories.  If you really want to do this you can use the new kio_fuse module to mount ioslaves directly onto the filesystem."
    author: "Spy Hunter"
  - subject: "KIO needs a command line tool..."
    date: 2004-03-27
    body: "Is there any other plain command line tool which also\nallows to list and create directories, get file sizes,\ndelete files etc.?\n\nI'm really missing something like that at the moment.\nJust a plain little program that doesn't need any setup\nor other tweaking.\n\nThis shouldn't be very hard, so I can't believe that \nsomething like that doesn't exist already."
    author: "Fred Sch\u00e4ttgen"
  - subject: "Re: KIO needs a command line tool..."
    date: 2004-11-24
    body: "You're damned right it should exist! I want it badly :( Any devs care to take a shot"
    author: "kimo"
  - subject: "Re: KIO needs some help..."
    date: 2004-02-11
    body: "Hi,\n\nto point #4 and #5: \nas somebody else pointed out, this can be done either via the kio-fuse gatway (first hit in google), or somebody else made some simple bash aliases to work with kfmclient.\n\n#1: basically easier access to remote hosts.\nThe lan-ioslave shows you all hosts, and which shares they provide (smb, ftp, http, fish). Use lan:/ as root and browse from there where you want.\nMaybe we could also add this capability to the smb-ioslave, i.e. below the host level not to show the shares, but the services available on this host. Shouldn't be too hard.\n\nBye\nAlex\n"
    author: "aleXXX"
  - subject: "Re: KIO needs some help..."
    date: 2004-02-11
    body: "On the subject of KIO\n\nMy main use for KIO is accessing MP3s on remote SMB shares.  I dont know about other IOSLaves but the SMB dosnt give a good impression of KIO.\n\n1. It keeps asking for the password all the time when i have already told it.\n2. I mainly used it in Noatun and It seemed to download all the MP3s I listened to, I ended up with 900mb of MP3s in my home directory!, Maybe this is a noatun problem.\n3 suddenly it decides to tell me the files are accessed denied.\n\nCould someone tell me whether #2 should be posted as a bug under Noatun or KIO.\n\nPerhaps if KDE and GNOME could decide on a common standard then we could get it put into bash and such."
    author: "mike"
  - subject: "Re: KIO needs some help..."
    date: 2004-02-12
    body: "> 1. It keeps asking for the password all the time when i have already told it.\n\nThis happend to me with KDE 3.1.x to the point of making SMB:// useless.  3.2 is much happier and I rarely get asked for user name and password anymore.  I did have to remove and re-add network logon details to the Control Center (Internet & Network ... Local Network Browsing ... Windows Shares tab).  I don't know if turning on Lisa would help (see the other tabs)."
    author: "Andy Longton"
  - subject: "Re: Nicely done"
    date: 2004-02-10
    body: "How can one maintain a copy on your local drive, and save using ftp:// to the remote server?\n\nAnd do a cvs commit. Yes I know about the cervisia plugin.\n\nCommand line rules!\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Nicely done"
    date: 2004-02-10
    body: "What's wrong with Cervisia? Sorry couldn't resist. ;-)\n\n> How can one maintain a copy on your local drive, and save using ftp:// to the remote server?\n\nDid you try something like the following?\n\nkfmclient copy index.html ftp://www.mysite.com/\n\n\n> And do a cvs commit. Yes I know about the cervisia plugin.\n\ncvs command line client????"
    author: "Christian Loose"
  - subject: "Re: Nicely done"
    date: 2004-02-11
    body: "> How can one maintain a copy on your local drive, and save using ftp:// to the remote server?\n \nHow many zillion ways do you want? Along with file commands (cp -a) and dump I'm checking out DAR and KDAR. Very cool. Don't forget konserve.\n\n> And do a cvs commit. Yes I know about the cervisia plugin.\n \n> Command line rules!\n\nAh, then you're still a big Cervisa fan because now it runs on top of cvsservice and communicates with DCOP which, as you know, can be handled easily in a script or console. Have a look at kdcop when Cervisia is running. ;-)\n\nSo now Derek you can open Cervisa to your repository and set your script in motion to do what you want, checking cvsservice to insure that we're looking at the right repository. Hey, I'm guessing you'll want a Kommander dialog to run this. ;-)\n\nIf you really need help then let me know and our team of bash and perl wizards will whip it up with a nice Kommander interface so you aren't stuck on that nasty command line. ;-)\n\nDCOP with Kommander RULEZ!\n \n"
    author: "Eric Laffoon"
  - subject: "Re: Nicely done"
    date: 2004-02-11
    body: "> Ah, then you're still a big Cervisa fan because now it runs on top of cvsservice and communicates with DCOP which, as you know, can be handled easily in a script or console. Have a look at kdcop when Cervisia is running. ;-)\n\n \nI guess another well-kept secret of KDE. :-)\n\nFor more info about the cvsservice and an example how to use it in a script you might want to take a look at http://tinyurl.com/2v82y."
    author: "Christian Loose"
  - subject: "Re: Nicely done"
    date: 2004-02-12
    body: "> I guess another well-kept secret of KDE. :-)\n \nIt's new. It has an excuse. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: Nicely done"
    date: 2004-02-11
    body: "\"Windows\" in this text must be \"Ms-Windows\""
    author: "Andre"
  - subject: "Re: Nicely donen - kprinter"
    date: 2004-02-10
    body: "I like to use kprinter from acroread (Acrobat Reader). What bugs me is, that acroread displays the stderr output from kprinter after sending the print job in an annoying message box.\nI've made short wrapper for kprinter like\n\nkprinter $1 2>/dev/null\n\nand calling it instead of kprinter directly. This empties stderr to null, hence suppressing the annoying message box afterwards. But I think it's not an ideal solution. maybe somebody knows how to do it better?\n"
    author: "Thomas"
  - subject: "Re: Nicely donen - kprinter"
    date: 2004-02-10
    body: "I submitted a bug report to bugs.kde.org that offered essentially the same solution, though implemented in the kprinter source itself. I don't know if anyone has implemented the fix yet, though."
    author: "Bob"
  - subject: "View Locking"
    date: 2004-02-10
    body: "I'm compiling 3.2 right now. I like the look of the view locking in konqueror while searching google. I have been using multiple windows. Can't wait to try it."
    author: "Echo6"
  - subject: "Re: View Locking"
    date: 2004-02-10
    body: "You probably don't have to wait for that compile.  it works fine for me in 3.1.5"
    author: "John Kraft"
  - subject: "Re: View Locking"
    date: 2004-02-10
    body: "In fact, Konqueror view locking has been a feature since KDE 2.  It's just very well hidden."
    author: "Spy Hunter"
  - subject: "kde how-to?"
    date: 2004-02-10
    body: "would be great to develop this into a \"KDE how-to\" series."
    author: "weapons of mass installation"
  - subject: "Re: kde how-to?"
    date: 2004-02-11
    body: "Yes, how about \"kde-tips.org\"? Basically a community site where all kinds of tips can be found.. KDE is so huge that it boggles the mind."
    author: "ac"
  - subject: "Re: kde how-to?"
    date: 2004-02-11
    body: "Why not use http://kde.ground.cz wiki?"
    author: "Anonymous"
  - subject: "Re: kde how-to?"
    date: 2004-02-12
    body: "Not enough visibility, I suppose. That, and Wikis are by design a bit hard to find our way around in. Now don't take me wrong, the KDE Wiki is great, but I think it just doesn't serve exactly the purpose expressed in the parent post...\n\nMyself, I would LOVE a kde-tips.org kind of site! Especially with different sections about how to better use KDE, of course, but also how to code KDE apps (and panel applets and KControl modules and...), how to write patches for existing KDE code, etc.\n\nStuff like that. :)"
    author: "Anonymous Coward"
  - subject: "i knew all of those and they are pretty cool"
    date: 2004-02-10
    body: "but:\n-i never managed to be able to reach one of my sites that way because they force me to have a login with an \"@\" inside the name.\n-the view locking in the case of google works better if you activate a big number of results, otherwise the locking/unlocking for the regular 10 results isnt much of a gain in term of time. It's great with images tho', or has been until gwenview integrated into konqui.\nActually, i prefer to open google links as background tabs, they load while i continue to scan the results, then i scan a few tabs while the next google resul page loads.\n-audio cd is just great and has been for years. I would have believed more services like it would appear. What a great idea it was.\n-fish: really deserves to be mentionned on the smb: page. This one also rocks"
    author: "djay"
  - subject: "Re: i knew all of those and they are pretty cool"
    date: 2004-02-10
    body: "> i never managed to be able to reach one of my sites that way \n> because they force me to have a login with an \"@\" inside the name.\n\nHave you tried writing %40 instead of the \"@\" in your user name? "
    author: "cm"
  - subject: "Re: i knew all of those and they are pretty cool"
    date: 2004-02-10
    body: "I had tried before and it didnt work.\nThe fun part is that it works now, but if I enter the adress of the site alone and the login and the passwd after, it doesnt.\nWell, it's a progress, i guess."
    author: "djay"
  - subject: "Re: i knew all of those and they are pretty cool"
    date: 2004-02-11
    body: "If the latter doesn't work any more I'd report that at bugs.kde.org. \n\n"
    author: "cm"
  - subject: "Re: i knew all of those and they are pretty cool"
    date: 2004-02-13
    body: "OK, it works but there are 3 small problems, and one of them was probably the reason to the whole difficulty.\n\n1/ very annoying small bug:\nwhen I bookmark the address that require the %40, it is immediatly replaced with a @, rending the bookmark useless.\n\n2/ related very annoying bug:\nimpossible to edit the url in the bookmark, the %40 is immediatly replaced with an @ when hitting enter. As much as I understand this behavior, there should be a way to specify \"keep that %40\". Or maybe there is? Escape char? \"\\\"?\n\n3/ the one that got in the way: when typing the url with the @ instead of the %40 the url is \"transformed\" in the url bar:\nftp://login@site@urlofthemastersite.org\nbecomes\nftp://ftp//login@site@urlofthemastersite.\nspoiling the history of konqueror. You can try with a fake url, it works."
    author: "djay"
  - subject: "Re: i knew all of those and they are pretty cool"
    date: 2004-02-11
    body: "Hello!\n\nHave you tried substituting the '@' by a '+', that seems to be a possibility with some providers.\n\nMarc"
    author: "Marc Heyvaert"
  - subject: "Wow!"
    date: 2004-02-10
    body: "Thanks man! And please don't stop ... :-)"
    author: "Stelian Iancu"
  - subject: "off topic - one thing I miss in Qt"
    date: 2004-02-10
    body: "Sorry for the off-topic post, but I just wanted to say there is a GTK feature I really miss in all the toolkits, although it is not widely known. Maybe someone could suggest its incorporation in Qt ;) (Please)\n\nWhen using widgets such as spin buttons (Spin Box in Qt nomenclatura I think, see http://doc.trolltech.com/3.2/qspinbox.html for an image) linked to a number, left-clicking the arrows will increment (or decrement) the value of one unit (e.g. \"1\") while middle-clicking will increase the value of another quantity (e.g. \"10\").\n\nFor example you have a value of 23 and want to go to 74. In other toolkits, you have to click 51 times or do a long click which will spin the numbers very fast but without precision. In GTK, you middle-click 5 times (23->33->43->53->63->73) and left-click one time (->74).\n\nI concede it is off-topic, but that one of the last bit where improvement is possible for Qt widgets."
    author: "oliv"
  - subject: "Re: off topic - one thing I miss in Qt"
    date: 2004-02-10
    body: "Have you tried using the mouse wheel in Qt? There is no need at all to click.\n\nStill I think your idea is a worthwhile improvement - no need to be forcibly different from GTK here.\n\nKristian\n"
    author: "Kristian"
  - subject: "Re: off topic - one thing I miss in Qt"
    date: 2004-02-10
    body: "Even beter would be to click on the number and drag the mouse up to increment or drang the mouse down to decrement. I think it's Photoshop on windows that has this, but I'm not sure."
    author: "Amilcar Lucas"
  - subject: "Re: off topic - one thing I miss in Qt"
    date: 2004-02-10
    body: "Blender has a similar thing... but it is not very ggod in terms of deterministic control."
    author: "oliv"
  - subject: "Re: off topic - one thing I miss in Qt"
    date: 2004-02-10
    body: "I have no wheel on my mouse... ;)"
    author: "oliv"
  - subject: "Re: off topic - one thing I miss in Qt"
    date: 2004-02-10
    body: "As it is a Qt wish, you can suggest it yourself to Trolltech by sending an email to Qt Bugs: mailto:qt-bugs@trolltech.com\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: off topic - one thing I miss in Qt"
    date: 2004-02-11
    body: "The step-value in the Spinbox can be set when the widget is constructed. The value can be read and changed afterwards, so I imagine that if someone needs to be able to implement some complex behaviour regarding the dynamics of this 'step-value', it would be possible.\n\nMarc"
    author: "Marc Heyvaert"
  - subject: "Newtolinux"
    date: 2004-02-10
    body: "Your writing style is not suited for Newbies, in fact not at all:\n\n\"The usual cycle for working on any web site's content is: open network app (e.g. KBear),\n---> open network app? ---> ap = application? \"open\" --> a verb?!\n     What's Kbear?\n\n\" download existing content, open in editor, save, upload in network app. With KDE we can do away with a lot of that, because of its network transparent kio slaves.\"\n\n----> Hum? \"can do away with a lot of that\" \"because\" - clause--> logic?\n\n KDE uses a vfs (virtual filesystem) idea to allow programs to save and load files to and from the network as though they are just normal local files. \n\n---> What are local files?\n---> Virtual filesystem -- hum what does this mean, your sentence is build like this: \"I use a mumbo idea to allow me to improve my language.\"\n\n\n\"So we can operate on any content on with any KDE application.\"\n\n----->What's \"operate on a content\"?\n\n\"Open it, edit it, save it.\" \n------> it??\n\n\"Simple as that.\" \n\nWhat?\n\n\"Web programmers will have to do this ...\"\n---->this??\n\n\"all the time to upload their scripts to test.\"\n----> with KDE, usual web development? (Which is actually wrong, when you test on localhost)\n\n\"With this setup,\"\n----> \"this\" setup???\n\n\"it becomes no more difficult than hitting the save button and testing the script.\"\n\n----> \"More difficult than\"  -Irony may be misunderstood. \n      \"hitting\" and \"testing\" are on adifferent abstraction level\n\n\nSo viel erst mal, viele Gr\u00fc\u00dfe\n\nAndr\u00e9"
    author: "Andre"
  - subject: "Re: Newtolinux"
    date: 2004-02-10
    body: "The articles are targeted to new to *Linux* people (new to KDE in this case), in order to show them why the platform is worthwhile for them, not for people with no idea about computers as you want to state.\nPersonally, I don't know anybody currently developing websites (even static websites, with no programming inside) who aren't aware about what are local files, remote files, and content.\n"
    author: "Shulai"
  - subject: "Re: Newtolinux"
    date: 2004-02-10
    body: "These particular articles aren't targetted at computer newbies. They're meant for people who already work with remote content over FTP, who use samba shares, etc. already. The idea is to show how KDE can change the way you already work, to make things easier, and to lose redundant steps that slow you down."
    author: "Tom Chance"
  - subject: "Re: Newtolinux"
    date: 2004-02-10
    body: "Well, my criticism was: he writes in a style where hidden knowledge is assumed, \n\nLogic:\n\nsocrates is a human\nhumans will die\n\n--> Socrates will die\n\n\nHis Logic:\n\nsocrates, you know he is a human, he will die. \n\n\nPersons who already understood usually use one issue to explain another issue. You have to create a real \"latter\" for your reader.\n\nIt's not the word \"Kio-Slaves\" ecc., you have to understand the concept of kio_slaves first, what kio-slaves actually mean to your daily work."
    author: "Andre"
  - subject: "Re: Newtolinux"
    date: 2004-02-11
    body: "If you think the artice isn't good enough, how about you write your own?"
    author: "Corbin"
  - subject: "Local/remote files (was: Re: Newtolinux)"
    date: 2004-02-10
    body: "\"Local\" files are files known by the operating system (for example /etc/fstab or file:/etc/fstab )\n\n\"Remote\" files are files not known by the operating system (for example floppy:/a/ or http://www.kde.org )\n\nBe careful that \"local\" and \"remote\" are relative. A \"local\" file can be remote, for example by NFS and a \"remote\" one can be local for example with floppy:\n\nHave a nice day!\n"
    author: "Nicolas Goutte"
  - subject: "Use the annotation engine and point them to it"
    date: 2004-02-11
    body: "Visit the annotation engine:\n\nhttp://cyber.law.harvard.edu/cite/annotate.cgi\n\nAnd enter their url at the bottom:\n\nhttp://www.newtolinux.org.uk/wiki/index.php/KDE is so cool because...\n\nAnd enter your comments as annotations.  Then mail these instructions to them too,\nso they can read the anotations.  Alternatively, since it is a wiki, perhaps you could just\ninsert the annotations in directly (maybe in brackets), and let them remove them if they don't like them."
    author: "Krishna"
  - subject: "which font?"
    date: 2004-02-10
    body: "which font is used in the windows title of these screenshot?\nhttp://www.newtolinux.org.uk/images/kde/k_pr1.png\n\nThanks a lot for your answers!\n"
    author: "anonymous"
  - subject: "Re: which font?"
    date: 2004-02-10
    body: "It's Futura extra black italic. I'm not sure where/if you can download it, I bought it years ago in a font pack."
    author: "Robert"
  - subject: "Re: which font?"
    date: 2004-02-10
    body: "Thanks a lot, it look's nice!\n"
    author: "anonymous"
  - subject: "Thanks"
    date: 2004-02-10
    body: "Cool, Thanks a lot, I (we) want more!!!"
    author: "John Herdy"
  - subject: "Pop up blocker in Konqi?"
    date: 2004-02-10
    body: "Hi, I haven't had a chance to compile 3.2 yet so can anybody tell me if Konqueror has a pop up blocker?  That's the only feature that keeps sending me back to Mozilla.  Font rendering and just about everything else are superior in Konqueror.\n\nTIA,\n-Robert"
    author: "Anon"
  - subject: "Re: Pop up blocker in Konqi?"
    date: 2004-02-10
    body: "Konqueror have always had a pop up blocker, if my memory serves me right. All the way since KDE 2.0."
    author: "Morty"
  - subject: "Re: Pop up blocker in Konqi?"
    date: 2005-05-30
    body: "Shame it doesn't actually block half the popups, even in version 3.4.\n"
    author: "Trejkaz"
  - subject: "Re: Pop up blocker in Konqi?"
    date: 2005-05-30
    body: "Did you report those sites? Are those opened by JavaScript or [Flash] plugins?"
    author: "Anonymous"
  - subject: "Re: Pop up blocker in Konqi?"
    date: 2005-05-30
    body: "I'll probably report the next ones I find.  But I think they might be doing it through Flash, which probably can't be helped."
    author: "Tre"
  - subject: "Re: Pop up blocker in Konqi?"
    date: 2004-02-10
    body: "Sure there is, since version 3.1 I think.\n"
    author: "Moi"
  - subject: "Re: Pop up blocker in Konqi?"
    date: 2004-02-10
    body: "What about pop-up notification? This helps when trying to figure out why a website isn't working properly and you noticed it has requested a pop-up. For example when you notice squirrelmail alerts aren't coming up and then you see that it was trying to but was blocked.\n\nI'm waiting for KDE 3.2 to hit debian/unstable so this may already be in there. :)\n\n"
    author: "Jeremy"
  - subject: "Re: Pop up blocker in Konqi?"
    date: 2004-02-10
    body: "set the policy to Ask ... that works nicely ... personally, i find \"Smart\" just right without the annoyance of dialogs asking me all the time."
    author: "Aaron J. Seigo"
  - subject: "Re: Pop up blocker in Konqi?"
    date: 2004-02-10
    body: "I'll try that out and see if it does what I'd like, thanks!"
    author: "Jeremy"
  - subject: "Re: Pop up blocker in Konqi?"
    date: 2004-02-10
    body: "One thing I don't like about smart is that you have no way of knowing a popup has just been blocked, in some situations this might be pretty bad.\nI really like the way Mozilla Firebird (now firefox) handles popups. It blocks all of them but it lets you know with and icon on the lower left corner. You might unblock it if you like. It could be better though, the icon may go unnoticed, something (just a little) more visible could be better. It should also let you unblock the popup for that session only. Anyway I might mention this in the appropriate place, in more detail, later.\n"
    author: "Jadrian"
  - subject: "Re: Pop up blocker in Konqi?"
    date: 2004-02-11
    body: "Is there anyway to tweak the \"smart\" pop-up blocker? I find that \"smart\" blocking blocks a couple of important pop-ups that are generated by my companies firewall when I'm logging in. I'm running on \"ask\" right now, which is actually pretty good.\n"
    author: "Jon"
  - subject: "Re: Pop up blocker in Konqi?"
    date: 2004-02-13
    body: "I think that you can set your popup-blocking preferences on a per-site basis in 3.2.  I say \"I think\" because I tried it with my bank's website, and it doesn't work.  <shrugs>"
    author: "Tukla Ratte"
  - subject: "Re: Pop up blocker in Konqi?"
    date: 2004-02-10
    body: "Please file a wishlist entry at http://bugs.kde.org, AFAIK such one doesn't exist yet. And don't forget to vote for it together with Jadrian."
    author: "Anonymous"
  - subject: "Re: Pop up blocker in Konqi?"
    date: 2004-02-10
    body: "It is under the Javascript settings and not enabled by default.  I found it difficult to find as well."
    author: "agrippa_cash"
  - subject: "Intercepting Hyperlink Clicks"
    date: 2004-02-10
    body: "While we are on the topic of tips and tricks, does anyone know how to intercept hyperlink clicks in konqueror? When clicking on a link to a certain domain I would like to reformat the link and then open the reformatted link instead of the original.\n\nAny tips on how to do this would be great!"
    author: "Joel Carr"
  - subject: "Re: Intercepting Hyperlink Clicks"
    date: 2004-02-10
    body: "You could try running a HTTP proxy like <a href=\"http://www.squid-cache.org\">squid</a> locally. It lets you rewrite URLs and can be used outside of Konqueror as well.\n\nIf squid seems complicated, then I am sure there are other simpler proxies available that are easier to configure."
    author: "Arnar Lundesgaard"
  - subject: "Re: Intercepting Hyperlink Clicks"
    date: 2004-02-14
    body: "Thanks for the tip, that would work brilliantly. It's a bit of a sledge hammer though for what I'm wanting to do. I may have a poke around in the konqueror source code for an alternate solution if I get the chance.\n\nThanks again."
    author: "Joel Carr"
  - subject: "Accessing Windows Shares"
    date: 2004-02-10
    body: "This article is very informative.  Not only did I learn a couple new tricks from the article itself, but the responses it has generated have enlightened me on some things as well.  Eric Lafoon's explanation was helpful (thanks Eric).  Please continue this series.\n\nI do need some help with regards to accessing windows shares, and since one of the tips was about this I suppose I am not off-topic.  I have a \"SharedDocs\" folder on a WindowsXP box than I am trying to access from my MDK9.2 Linux box.  I have smb.conf configured with the same workgroup/domain as the XP machine.  Also, my account and password info is the same for XP, Linux, and samba.  I can access my home directory on the Linux box from my XP box with no problem.  However, I can't access the \"SharedDocs\" folder on XP (it truly is shared) from the Linux box.  Here is how I try to access it and what is happening:\n\nIn the location bar in Konqueror I type in \"smb:/Matrix/\". Matrix is the name of my XP box.  At this point Konqueror seems to connect to Matrix.  In the file list view I see C$ and SharedDocs$, along with a path to a shared printer.  All looks good at this point.  However, when I click on the \"SharedDocs\" share, it prompts me for my user name and password.  When I enter that info, it thinks for a second or so and brings the logon window again.  Does anyone know what I need to do to access this share?\n\nBTW, I can access the XP shared printer just fine.  Also, I have this same issue if I use the PCLinuxOS livecd on another machine.  The network utility included with it will detect the share, but I can't actually connect to it.\n\nThanks,\nPaul....."
    author: "Paul Hawthorne"
  - subject: "Re: Accessing Windows Shares"
    date: 2004-02-10
    body: "Have you activated the 'guest' account on your XP box? It usually helps."
    author: "Simon Roby"
  - subject: "Re: Accessing Windows Shares"
    date: 2004-02-10
    body: "No, I hadn't.  I'll give that a try.  However, I would think that I could logon using existing account info.  Thanks for your input.\n\nPaul....."
    author: "Paul Hawthorne"
  - subject: "Re: Accessing Windows Shares"
    date: 2004-02-11
    body: "If you still have problems with smb, either contact me directly via email or even better file a bug report on bugs.kde.org.\nBut before doing this, make sure you have samba >= 3.0 and KDE 3.2 installed :-)\n\nBye\nAlex\n"
    author: "aleXXX"
  - subject: "Re: Accessing Windows Shares"
    date: 2004-02-11
    body: "Thanks Alex.  I don't think I'm running Samba 3.0 unless it was installed when I upgraded to mandrake 9.2, but I will check.  I didn't realize it was a requirement, but I will go to 3.0 if it isn't.  I'll update you via email.\n\nPaul....."
    author: "Paul Hawthorne"
  - subject: "Re: Accessing Windows Shares"
    date: 2004-02-11
    body: "I think the best samba support you will have with samba >=3.0 and KDE >=3.2.0 :-)\n\nAlex\n"
    author: "aleXXX"
  - subject: "SFTP--Drag n' Drop sweetness"
    date: 2004-02-10
    body: "I would like to thank the folks with the talents to make this such a seemless\nreality.  Nothing like having two Konqueror views side by side or in split view being able to drag n' drop from your local workstation to a remote site via ssh and update the server's web content.\n\nWhen not using CVS this feature is a nice hidden gem.  It by no means should be used in favor of CVS but when you don't have CVS running this just works.\n\nOn Windows 2k/XP this sort of behavior requires a third party app.  I'm sure there are some add-ons to a system via Developer CDs or whatnot but not out-of-the-box.\n\n"
    author: "Marc J. Driftmeyer"
  - subject: "Very very sweat!"
    date: 2004-02-11
    body: "Awsome stuff guys.  Hope to seem more articles and more how-to's from you.  Great stuff...\n\nbrockers"
    author: "brockers"
  - subject: "Zip files?"
    date: 2004-02-11
    body: "Stellar article, I learned quite a bit.  One of the things I learned while experimenting is that I could open and browse inside .zip and .tar.gz archives, very nice.  Long ago when I ran OS/2 there was a GUI add-on that allowed you to open, browse, and add items to and even run programs directly from nearly every type of archive file under the sun: .zip, .tar.gz, .arc, .lzh, .ace, and a few I'm sure I can't recall.  However with KDE I can view files, but that's it, trying to drag a file into an archive doesn't even produce a \"you can't do that\" error.  Is this sort of functionality on the horizon?  I used to find it extremely useful.  I'm running debian testing, so it's possible it simply hasn't worked it's way in yet if it already exists.."
    author: "Tom"
  - subject: "Re: Zip files?"
    date: 2004-03-31
    body: "Is there any way one can add to a zip archive?"
    author: "KDEUser"
  - subject: "kioiso"
    date: 2004-02-13
    body: "Didn't there used to be a kioslave for iso's?"
    author: "stumbles"
  - subject: "Thanks"
    date: 2004-02-14
    body: "Thanks. These are a great set of articles. Ever thought about having an area on the KDE website for such things that people can post to?"
    author: "David"
  - subject: "Add to a new tutorial section in kde.org"
    date: 2004-02-15
    body: "Great job - these kind of how-tos are exactly what is needed to help people use Linux on the desktop.  I have used KDE for quite some time now and can really appreciate some of these tips & tricks (I cannot remember how many times I have been stuck in Mozilla or xpdf and couldn't easily get a printout). Likewise the view profiles are very, very useful.  Thank you.\n\nI would suggest that a tutorials section be added to www.kde.org and these be added to that (and maybe the home page of kde.org be cleaned up and simplified a bit - the right side should be eliminated with some of the links put under the categories on the left side - it is just too confusing right now)"
    author: "john"
---
Following the release of KDE 3.2, a friend and I have put together <a href="http://www.newtolinux.org.uk/wiki/index.php/KDE is so cool because...">a series of articles promoting features of KDE</a> that might change the way you work. We all know about the features of KDE that are promoted in press releases and demonstrated in screenshots, but the enormous power that lies "under the hood" of KDE in technologies like kioslaves, view profiles and kparts is often overlooked. So far we have three complete articles: <a href="http://www.newtolinux.org.uk/wiki/index.php/Managing web sites">Managing Websites</a>,  <A href="http://www.newtolinux.org.uk/wiki/index.php/Extending Konqueror with view profiles">Extending Konqueror with View Profiles</a>, and <a href="http://www.newtolinux.org.uk/wiki/index.php/Using KPrinter in any app">Using KPrinter in Any App</a>. We hope to develop this into a decent series of promotional articles, beyond the "tips and tricks" already available, so comments and suggestions are welcome!


<!--break-->
