---
title: "KDE 3.2 Released"
date:    2004-02-03
authors:
  - "binner"
slug:    kde-32-released
comments:
  - subject: "yeah"
    date: 2004-02-03
    body: "i waited for this day a long, long time :-)\n\ngood\n\nill donate you\n\ndoes anybody know where i can get languages for kde 3.2?\n\nbye"
    author: "mj"
  - subject: "Re: yeah"
    date: 2004-02-03
    body: "Can't view WWW.KDE.ORG\n\nToo many access or DDOS?"
    author: "albert"
  - subject: "Re: yeah"
    date: 2004-02-03
    body: "Ever heard of Slashdot? ;)"
    author: "Arend jr."
  - subject: "!"
    date: 2004-02-03
    body: "Awesome dude!"
    author: "ac"
  - subject: "Yes!"
    date: 2004-02-03
    body: "Me happy!"
    author: "Gagnefs Tr\u00e4skodansarf\u00f6rening"
  - subject: "Re: Yes!"
    date: 2004-02-04
    body: "me too, kde is really starting to get somewhere"
    author: "Ministeriet f\u00f6r inre lugn och s\u00e4kerhet"
  - subject: "Re: Yes!"
    date: 2004-02-04
    body: "Me too!"
    author: "Heja Leksand!"
  - subject: "firewire/ieee1394 browser"
    date: 2004-02-03
    body: "The KInfoCenter now also includes a graphical view of the firewire bus (if you have one), featuring the guid, node id, vendor name, capabilities and a bus reset button.\n\nSomehow this must have been lost in the changelog.\n\nIOW no need for gscanbus anymore.\n\nAlex\n"
    author: "aleXXX"
  - subject: "Huh?"
    date: 2004-02-03
    body: "I installed the SuSE binary packages - other than having to use nodeps to keep the kdebase3-SuSE package installed, it worked.\n\nTwo issues:\n- Why the stupid shadow font for the desktop icons?  Worse, I can't seem to find a way to change it (no appearance on \"Configure Desktop\" and the control panel setting for desktop font doesn't let me turn off shadow)\n- The control panel is buried several several menus down - Why?"
    author: "Mike"
  - subject: "Re: Huh?"
    date: 2004-02-03
    body: "1.) configure desktop-> background -> advanced options ->enable shadow\n2.) Drag the kcontrol icon to your desktop.\n\nLook if you cant do these things, then you shouldn't be downloading raw kde. go and buy lycoris or lindows. its got this already nicely arranged for you. These guys working here are volunteers. not tech support."
    author: "muleli"
  - subject: "Re: Huh?"
    date: 2004-02-03
    body: "Raw KDE has KControl accessible direct from a Kicker menu anyway.\n\nNo idea about the bad desktop shadow though."
    author: "ac"
  - subject: "Re: Huh?"
    date: 2004-02-04
    body: "I just upgraded by Gentoo to KDE 3.2 (10 hours of compiling, whoopee!) and had the same problem trying to find how to remove the shadow.  I would not say placing it in advanced options under background was obvious at all.\n\nI would expect it under Control Center -> Desktop -> Behavior and was checking all the appearance options for a long time until a stumbled on this post.\n\nPs.  I'm trying to get off XMMS to something more KDE integrated, I've been using amaroK but KDE3.2's install seemed to have wiped that, not liking JuK so far, what do you like for playing mp3s?\n"
    author: "Kevin Stone"
  - subject: "Re: Huh?"
    date: 2004-02-04
    body: "I saw nothing there that warranted being rude.  Advanced Options isn't at all obvious, and if you think it is, and you volunteered your time to put it there, maybe you shouldn't be volunteering your time to put it there."
    author: "regeya"
  - subject: "Re: Huh?"
    date: 2004-02-06
    body: "I agree. Advanced options is not intuitive, put this option in \"Background\" screen. \n\n"
    author: "Jasem Mutlaq"
  - subject: "Re: Huh?"
    date: 2004-02-03
    body: "I'm new to SuSE, so let me ask a dumb question:  can I expect to see KDE 3.2 turn up in YaST Online Update, or do I need to install these tarballs manually?  I played around with the Beta and RC1 tarballs, but they had multiple dependancy conflicts, and SuSE integration (things like susewatcher) stopped working-- it was all pretty disappointing."
    author: "Stunji"
  - subject: "Re: Huh?"
    date: 2004-02-03
    body: "They won't be in YaST as suse wants you to buy 9.1. What problems did you have with RC1? I didn't have any."
    author: "Stephan"
  - subject: "Re: Huh?"
    date: 2004-02-03
    body: "Actually I just found instructions for SuSE which, if they existed before, I had never seen:\n\nhttp://ibiblio.org/pub/packages/desktops/kde/stable/3.2/SuSE/README\n\nI remember there being weird dep. issues with misc. things like taglib (which seems to be addressed in the howto.)\n\nAlso, konqueror segfaults frequently unless you remove kdebase3-SuSE, which gets rid of other useful things like new hardware notification."
    author: "Stunji"
  - subject: "Re: Huh?"
    date: 2004-02-03
    body: "Yeah, I noticed this in the README on the FTP mirrors. Is there an ETA on when 9.1 will be released? If it is going to be later than next month, I can't wait that long... I don't really remember them doing this before--are they going to use new releases to string us along? Guess it's time to look at Konstruct.\n\nEron"
    author: "Eron Lloyd"
  - subject: "Re: Huh?"
    date: 2004-02-03
    body: "SUSE has never slipped significant new releases in YOU -- you would always have had to download the rpm's from the supplementary directory from SUSE's ftp server or a mirror and install them by hand. "
    author: "Boudewijn Rempt"
  - subject: "Re: Huh?"
    date: 2004-02-03
    body: "This is pure speculation...\n\nSuSE have brought out around 3 releases per year for some time now.  The last release was at the end of September, which is 4 months ago.  They like to include such major releases of KDE in their releases - other releases have come out following KDE releases.\n\nIt would not surprise me if we see 9.1 out this month, including KDE 3.2 and kernel 2.6.1.  At least, I'm hoping they bring it out soon as I'd like to buy a copy!!\n\nIn the mean time, I download the RPMs and install by hand.  I like to get the source RPMs and rebuild for i686, but often the build bombs out.  Strange, huh?\n\n-- Steve"
    author: "Steve"
  - subject: "Re: Huh?"
    date: 2004-02-03
    body: "> SuSE have brought out around 3 releases per year for some time now.\n\nWrong, two.\n\n> It would not surprise me if we see 9.1 out this month\n\nNo chance, March or April."
    author: "Anonymous"
  - subject: "Re: Huh?"
    date: 2004-02-03
    body: "IANA Suse Employee however, not YOU but KDE download, and eventually a new distro (one way of giving something back...) and as for screwing things up with betas and RCs - do it regularly myself - but I've always thought that was my problem."
    author: "Gerry"
  - subject: "Re: Huh?"
    date: 2004-02-03
    body: "kde for suse is available via the Advanced Package Tool (apt).\nThis is a good alternative to YOU, more about it at;\n\nhttp://linux01.gwdg.de/apt4rpm/kde31to32.html\nhttp://linux01.gwdg.de/apt4rpm/freshrpms.html"
    author: "Richard"
  - subject: "Re: Huh?"
    date: 2004-02-04
    body: "The instructions at these pages are very unclear. In deban (e.g. at apt-get.org), when someone announces a new repository, they list the full repository line as it appears in the sources list. The suse apt site doesnt seem to do this anywhere.  Could you please post the full sources.list lines required for this upgrade?\n\nThanks"
    author: "Timlinux"
  - subject: "Re: Huh?"
    date: 2004-02-04
    body: "Ah, yes -- that was a fun afternoon. I use Debian on my old powerbook, so I had already encountered the charms of apt, but didn't know it existed for SUSE, too. A bit slow though -- does anyone know of a mirror in the Netherlands? And... It insists on installing qt-3.3.0, which is giving me a hard time picking up Krita development again. But it still was fun. Thanks for the tip!."
    author: "Boudewijn Rempt"
  - subject: "Fast turnaround between RC1 and actual release"
    date: 2004-02-03
    body: "I'm impressed by the fast turn around time between RC1 and the actual release of KDE 3.2. It seems that there can't have been many bugs in RC1, well done!"
    author: "Gareth"
  - subject: "Re: Fast turnaround between RC1 and actual release"
    date: 2004-02-03
    body: "I'm not (happy).\n\nThe reason for the fast turn around time was the failure to fix some of the serious bugs in RC1 and issue another RC (which would have been RC3).\n\nPlease see bugs #73379 and #73851 for examples.  Actually, I was surprised that such serious bugs had not been fixed in the Betas.\n\nI should try to make this clear since I have already been flamed for this on the 'kde-devel' list.  I am interested in a high quality product.  I believe that a commitment to excellence must come first.  It has become obvious that there are a significant number of developers that lack this commitment.\n\nPlease get with the program!  \n\nAs another user posted: Quality Management -- a Commitment to Excellence!\n\nThe first step in instituting a Quality Management program is to have a Quality Assurance team.  For that we need volunteers.  This could work if we have a lot of volunteers that only do a little.  Specifically, if you report a bug and can keep an up to date copy of the CVS for the pending release, you can check to see if the bug gets fixed before the release.  Of course, this won't do any good if the developers simply ignore the bug and release any way.  A list of the critical bugs must be prepared and the release must be delayed until they are fixed and \"signed off\" by the QA team.\n\nWe need testers, to report bugs, that will install, preferably built from source from CVS, a copy of RC1 and risk using it as their regular desktop because it is only by using it as your regular desktop that you will find the bugs.  You will need to update to new RCs when they are tagged.  And, you will need to change the tag to RELEASE when it is added and update it every day, report bugs, and check on the bugs that you have reported.\n\nWe also need people to confirm new bugs and test old bugs to see if they have been fixed.  It would be best if you had an up to date copy of the CVS for the current BRANCH.  The reason such help is needed is that IIUC, many developers do not keep a copy of the current release BRANCH and are testing bugs only on HEAD -- not a good idea.  Specifically, they should first be tested on the current release BRANCH to see if they are currently valid.  Then, they can be tested on HEAD to see if they are being fixed.  However, bugs that are valid on the current release BRANCH and which have been fixed on HEAD should not be considered closed until they are confirmed fixed in the next release.  That is what Quality Assurance is all about.\n\nWithout your help, we will continue to have buggy releases like KDE-3.2.0\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Fast turnaround between RC1 and actual release"
    date: 2004-02-03
    body: "Yes, please join and help in ensuring high quality of KDE releases !\n\nEverybody is welcome, no need to be programmer, people with communication skills can also help us very much :-)\n\nNevertheless, I think it was about time to release 3.2.0, now let's concentrate on the stable branch and polish it :-)\n\nBye\nAlex\n"
    author: "aleXXX"
  - subject: "Re: Fast turnaround between RC1 and actual release"
    date: 2004-02-04
    body: "This sounds really great, and I think I'm not the only one here who'd be interested in giving a hand.\n\nThe only piece of information you didn't provide is, just where do we sign up? :)"
    author: "Anonymous Coward"
  - subject: "Re: Fast turnaround between RC1 and actual release"
    date: 2004-02-06
    body: "http://www.kde.org - there's a link called \"Supporting KDE\"\n\nRegards,\nMalcolm"
    author: "Malcolm"
  - subject: "Re: Fast turnaround between RC1 and actual release"
    date: 2004-02-06
    body: "Oh and of course you can report bugs at http://bugs.kde.org. :\u00ac)\n\nRegards,\nMalcolm"
    author: "Malcolm"
  - subject: "Re: Fast turnaround between RC1 and actual release"
    date: 2004-02-03
    body: "JRT: Without your help, we will continue to have buggy releases like KDE-3.2.0\n\nhttp://www.toonopedia.com/magoo.htm\n\n\"Magoo's inability to distinguish between his nephew, wearing a raccoon coat, and a wild bear, combined with his pig-headed refusal to consider the possibility that his sight might be failing, made a big hit with audiences..\"\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: Fast turnaround between RC1 and actual release"
    date: 2004-02-03
    body: "Software has bugs. There is no process that will make bug free software. Especially with something incomplete, like KDE.\n\nSo we will have a 3.2.1, etc. Some bugs will be fixed in those releases, some won't because of string changes, or design changes required to fix them. They will be done later, maybe 3.3, maybe later again.\n\nIf the release date was put off further, there would be an even greater gulf between what the developers are working on, and the release tree. Yes, developers generally get on with new stuff, features, finishing things up. The release dude can only suggest what people work on.\n\nYes it seems chaotic. But it works at least as well or even better than any other system. Even the tightly controlled processes that NASA uses didn't get everything right. But trying to impose tight control over this project will imho, cause harm.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Fast turnaround between RC1 and actual release"
    date: 2004-02-03
    body: "Yes of course it has. But some bugs are just too grave to justify the RC or Release title. For example, in RC1, just drag a file to a konqueror window and it crashes. Such obvious bugs don't belong in a RC release.\n\nExcept for those few bugs, KDE 3.2 is a beauty. Keep on going!"
    author: "Dik Takken"
  - subject: "Re: Fast turnaround between RC1 and actual release"
    date: 2004-02-03
    body: "Those file dragging crashes are due to a Qt bug actually. Patch has been send to Trolltech and in the meantime you can use qt-copy from CVS."
    author: "Waldo Bastian"
  - subject: "Re: Fast turnaround between RC1 and actual release"
    date: 2004-02-04
    body: "Case in point.\n\nA few months ago, I thought of doing a bug of the week sort of thing for the Digest. So on #kde-devel I mentioned a few of the obvious ones. The responses to each bug was an explanation of the issue. The problem had been investigated. The fixes often require major rewrites, adding new technology or capabilities, or as Waldo mentions, depends on someone else's libraries.  Should the release be delayed? Till when? No. Release now. There are major improvements in 3.2. There is still much work to do.\n\nSoftware is never finished.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Fast turnaround between RC1 and actual release"
    date: 2004-02-04
    body: "Yes, Derek.  That is why I chose my examples carefully.\n\nI believe that both of them are small coding errors.\n\nBut please realize that my blog was a call to excellance -- nothing more and nothing less.\n\nI believe that through Total Quality Management that we can do better.\n\nThis should not be taken as a negative comment about how well we are doing now, because no matter how well we are doing we should always strive to do better.  And, investigate methods to achieve that.\n\nThe engineer is also an idealist, very odd isn't it. :-)\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Fast turnaround between RC1 and actual release"
    date: 2004-02-04
    body: "There is a reason why when you implement TQM in a company, usually you have to force it onto people.\n\nAnd remember, that\u00b4s people who are getting paid."
    author: "Roberto Alsina"
  - subject: "Re: Fast turnaround between RC1 and actual release"
    date: 2004-02-04
    body: "This is why we see TQM in companies distributing KDE.\nJust look at Lycoris, Lindows, Mandrake, Suse whatever..."
    author: "OI"
  - subject: "Re: Fast turnaround between RC1 and actual release"
    date: 2004-02-03
    body: "\"Web pop-up windows open in new tab instead of a new window.\"\nYou call that a 'serious bug' ?\n\n"
    author: "John"
  - subject: "Re: Fast turnaround between RC1 and actual release"
    date: 2004-02-04
    body: "A serious bug? I'd class that as a feature!\n"
    author: "Adam Foster"
  - subject: "Re: Fast turnaround between RC1 and actual release"
    date: 2004-02-04
    body: "The fact that it is a regression makes it more serious that it would be otherwise.  And comparison with other software should also be considered when determining how serious it is.\n\nThat is, it works correctly in Mozilla and it works correctly in Konqueror 3.1.5.  These two facts make it a more serious bug than if it were taken in isolation.\n\nAlthough bugs (design issues) are to be expected in new features, when there is a regression which is clearly a coding error in mature sections of the code it is a greater issue.  Consider what is going to happen, a user upgrades from 3.1.5 to 3.2.0 and something that worked fine in 3.1.5 suddenly stops working.  Most users don't like that.\n\nOTOH, consider a new feature (especially if it can be turned off) if it doesn't quite work yet, no big deal.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Fast turnaround between RC1 and actual release"
    date: 2004-02-04
    body: "How is that a regression? It is clearly an improvement.\nI don't see it breaking anything, in fact, it follows user directions precisely.\n"
    author: "Luciano"
  - subject: "Re: Fast turnaround between RC1 and actual release"
    date: 2004-02-04
    body: "I disagree.\nTabs are useful when you're browsing different sites at the same time, because they allow you to have many sites open concurrently with little desktop real estate overhead.\nPop-ups are NOT the same thing at all. Pop-ups work as additional information (password entry, movies window...) on the CURRENT site; you can't stash them away for later browsing like you do for a new site or page.\n\nThe current Konqueror behavior is a usability bug. I fear that if we don't fix it, people will switch back to other browsers that behave the way they expect.\n\nI've just voted for the bug, too, since it's indeed a bit of a wart that should get fixed."
    author: "Anonymous Coward"
  - subject: "Re: Fast turnaround between RC1 and actual release"
    date: 2004-02-04
    body: "Aren't you confusing \"works correctly in...\" with \"works differently in...\". FWIW, I like the new behavior.\n\nUnless this wasn't changed intentionally, it's just a change in behavior but not a regression.\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Fast turnaround between RC1 and actual release"
    date: 2004-02-04
    body: "I agree, I personally love the new behavior and can't imagine that someone is seriously advocating popup windows. I usually consider the utilization of popup window as bad behavior which should be avoided whenever possible, may it be on website or in applications."
    author: "Datschge"
  - subject: "Re: Fast turnaround between RC1 and actual release"
    date: 2004-02-06
    body: "I can't say if it was changed intentionally or not.\n\nWhat I can say is that until somebody figurers out how to display two tabs at once that I don't find it a feature.  Specifically when using an online catalog and the pop-up window is a larger view of the item.\n\nAlso, this behavior can lead to errors because the new tab is a fully functional browser window.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Fast turnaround between RC1 and actual release"
    date: 2004-02-04
    body: "It seems to me that this release should have been named\n\"beta\" or \"release candidate\". It contains many oops's which are definitely due to hasty package preparation and lack of serious testing. Just two examples.\nFirst, some .h.in files in subdirs of kdelibs package (e.g., dcop-path.h.in) are older then configure.in in kdelibs root directory. As a result, make tries to run autoheader. If autoconf is absent or is old enough (like 2.13), autoheader will fail, leaving config.h.in empty and newer then config.h. If you rerun make, config.h will become empty. So, if you run make just once, everything seems to be fine. But if you happen to hit some compilation error (more on that later), you are out of luck.\nSecond, kdefx/kstyle.cpp in kdelibs contains ifdef check for Qt 3.30. If Qt has this version, it adds additional cases in the switch statement for PM_MenuBarItemSpacing. I have Qt 3.30b1 installed. It defines QT_VERSION 0x030300, but does not have PM_MenuBarItemSpacing anywhere in it's source.\nNow, take those two bugs together and... \nMay be I'm wrong and had done something stupid myself, but such things do not invest into KDE's popularity. Wasn't it better to allow more RC's just to stamp out such bugs?\nSincerely, Igor Romanenko \n"
    author: "Igor Romanenko"
  - subject: "Re: Fast turnaround between RC1 and actual release"
    date: 2004-02-04
    body: "1. We don't release the packages - we release the source.\n2. We don't support 3.3 with this release and certainly not the betas.\n\nIf you run unsupported configurations then don't blame us if they don't work.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: Fast turnaround between RC1 and actual release"
    date: 2004-02-04
    body: "1. Ok, to be more specific: I was talking about compiling kdelibs SOURCE PACKAGE released yesterday (kdelibs-3.2.0.tar.bz2). Or why would i talk about such things as autoconf and autoheader?\n2. Library requirements say Qt >= 3.2.3. So, either requirements page is wrong or my configuration is supported.\n"
    author: "Igor Romanenko"
  - subject: "Re: Fast turnaround between RC1 and actual release"
    date: 2004-02-04
    body: "Your Qt is beta. You should expect errors.\nTry with Qt 3.2.X\n\nKDE developers cannot tst future Qt versions. It is possible (but unlikely?) that the new Qt 3.3 does not work with KDE 3.2.0. For this, there is KDE 3.2.1."
    author: "cwoelz"
  - subject: "Re: Fast turnaround between RC1 and actual release"
    date: 2004-02-04
    body: "Hmmm... KDE developers CAN develop using beta versions (or how could this ifdef  get into the code), but they CAN NOT test using them ;)... I agree with you completely that it would be wise on developers' hand not to use code which is dependent on future Qt versions and their features. But what happend is that the code DOES depend on them - and i regard this as a bug.\nAnyway, my whole point is this: there ARE bugs in the distributed packages, which are due to haste. I fully understand that some bugs cannot be eliminated without re-designing and/or re-hacking a lot of code. But bugs such as mentioned can be stamped out in no time (well, less time than it takes to answer my posts ;) ). And it's a pity that such bugs made it into the \"stable\" release..."
    author: "Igor Romanenko"
  - subject: "Re: Fast turnaround between RC1 and actual release"
    date: 2004-02-04
    body: "Nobody hinders you and anyone else to actually fix any bug right away. The problem is not \"haste\" but rather the lack of human resource actually tackling all the issues right away."
    author: "Datschge"
  - subject: "Re: Fast turnaround between RC1 and actual release"
    date: 2004-02-05
    body: "Exactly! How would you call a release, for which you EXPECT users to do testing and bug fixing? IMHO it should be called \"release candidate\" or \"beta\"."
    author: "Igor Romanenko"
  - subject: "Re: Fast turnaround between RC1 and actual release"
    date: 2004-02-05
    body: "There has been a nearly half a year freeze, two betas and one release candidate already, and the system's stability as well as the balance between quickly fixable serious bugs and the kind of newly reported bug was showing that it was a pretty fine time to do a release.\n\nIf you think software with bugs should never be released or even *gasp* used you might prefer stopping using computers altogether."
    author: "Datschge"
  - subject: "Re: Fast turnaround between RC1 and actual release"
    date: 2004-02-04
    body: "> 2. Library requirements say Qt >= 3.2.3. So, either requirements page is wrong or my configuration is supported.\n\nWell the reason is pretty simple. The enum entry PM_MenuBarItemSpacing was introduced after Beta1. It would have compiled with qt-copy or a later snapshot (>20031220). \n\nAlso if you see a requirement like Qt>=3.2.3 than we are talking about released stuff and you shouldn't except it to work flawless with some beta version you are using.\n\n> If autoconf is absent or is old enough (like 2.13)\n\nCompiling with an absent or old autoconf (<2.5) isn't supported so you shouldn't expect it work. (see http://developer.kde.org/build/compile_kde3_1.html)\n\n> May be I'm wrong and had done something stupid myself\n\nvery tempting...."
    author: "Christian Loose"
  - subject: "Re: Fast turnaround between RC1 and actual release"
    date: 2004-02-04
    body: "2. IMHO, making code dependent on future Qt versions is not a good idea. And Qt 3.3 was still beta at the point where KDE-3.2 was released.\n1. Is the end-user supposed to have autoconf installed just to run configure? And BTW it does not work with 2.53 either which is greater than 2.5, isn't it?\n\nAgain, i'm not asking for workarounds - it was not hard for me to find them. I'm saying that \"Fast turnaround between RC1 and actual release\" lead to simple glitches. Where the turnaround not so fast, there would be more time for testing QA and the release would be better polished."
    author: "Igor Romanenko"
  - subject: "K menu"
    date: 2004-02-03
    body: "I'm impressed by everything i've seen in the new version of kde.. has anyone however got the same problem as me, in that as soon as any item appear in the recently used application list at the top of the kmenu, the kmenu become several pixels too thin?\n\nbugs.kde.org seems to be down for me otherwise i'd check there!\n\nApart from this, its a great improvement over 3.1.5"
    author: "Martin"
  - subject: "Re: K menu"
    date: 2004-02-03
    body: "It's on bugs.kde.org already. Bugs.kde.org is giving it's CPU time to downloaders."
    author: "Jaana"
  - subject: "debian"
    date: 2004-02-03
    body: "/me is waiting happily for the dpkgs ... :)\n\nGreat job, good work.\n\nThis won't keep me stickin' on my win2k any longer ... hehe\n\n - d"
    author: "deucalion"
  - subject: "Re: debian"
    date: 2004-02-03
    body: "/me has already apt-get'ed KDE 3.2 on ArkLinux\n\nexcellent!"
    author: "Tom"
  - subject: "KDE 3.2 for woody???"
    date: 2004-02-03
    body: "Is KDE 3.2 will be available for woody? Hmmm? Ralf?\n\nSahin"
    author: "Sahin"
  - subject: "Re: KDE 3.2 for woody???"
    date: 2004-02-03
    body: "Oh yessssss!\n\nDebian also available!\n\nThx!\n\nSahin"
    author: "Sahin"
  - subject: "Re: KDE 3.2 for woody???"
    date: 2004-02-03
    body: "post URL plz"
    author: "mahjong"
  - subject: "Re: KDE 3.2 for woody???"
    date: 2004-02-03
    body: "You could go to the KDE 3.2 info page (http://www.kde.org/info/3.2.php) like described in the article and search for Debian.\n\nYes, sometimes it's that simple! ;-)"
    author: "Christian Loose"
  - subject: "Re: KDE 3.2 for woody???"
    date: 2004-02-03
    body: "Or Unstable - url would be nice if someone has already packaged it :)"
    author: "Pete"
  - subject: "Re: KDE 3.2 for woody???"
    date: 2004-02-04
    body: "http://wiki.debian.net/?DebianKDE"
    author: "Cliff"
  - subject: "Re: KDE 3.2 for woody???"
    date: 2004-02-04
    body: "There's packages for woody distributed via kde.org.\nI did install these packages last night, they work well. Except that I can't choose plastik style? In which package is it supposed to be?\napt-cache search plastik didn't find anything..\n"
    author: "moi"
  - subject: "Re: KDE 3.2 for woody???"
    date: 2004-02-05
    body: "Try looking for kde-artwork."
    author: "dimby"
  - subject: "Re: KDE 3.2 for woody???"
    date: 2004-02-14
    body: "Hey I am a complete noob to debian after adding deb http://download.kde.org/stable/3.2/Debian stable main\nto my source list what exactly do I do to load kde. apt-get install kde? THX"
    author: "monkeywrencher"
  - subject: "Re: KDE 3.2 for woody???"
    date: 2004-02-14
    body: "See http://wiki.debian.net/DebianKDE\n\nBenjamin."
    author: "Benjamin."
  - subject: "Default theme?"
    date: 2004-02-03
    body: "What is the default theme for KDE 3.2? Just in case I need to make some screenshots for documentation.\n\nSteinchen"
    author: "Steinchen"
  - subject: "Re: Default theme?"
    date: 2004-02-03
    body: "Same as before AFAIK."
    author: "ac"
  - subject: "Re: Default theme?"
    date: 2004-02-03
    body: "Keramik."
    author: "Tukla Ratte"
  - subject: "Re: Default theme?"
    date: 2004-02-03
    body: "The default theme for KDE 3.2.0 is Plastik, IIRC.\n\nBob"
    author: "Robert Tilley"
  - subject: "Re: Default theme?"
    date: 2004-02-04
    body: "you don't recall correctly, it's Keramik. Plastik is included in kdeartwork."
    author: "Oliver Bausinger"
  - subject: "Re: Default theme?"
    date: 2004-02-03
    body: "Yep, it is Keramik due to screenshot policy etc. I would imagine. Plastik should be there though for all you people out there, and hopefully it will improve and may be the default in the future."
    author: "David"
  - subject: "respect"
    date: 2004-02-03
    body: "Thank you very much!"
    author: "magneto"
  - subject: "Did KHTML font bug get fixed?"
    date: 2004-02-03
    body: "I can't get into bugs but I was wondering if the KHTML problem with the <TT> and <PRE> tags getting rendered in some nasty and unconfigurable font ever got fixed?"
    author: "Joe Luser"
  - subject: "some CSS Bugs!"
    date: 2004-02-03
    body: "some CSS Bugs are not fixed with kde3.2 but available in bugs.kde.org since some weeks. No problems with kde3.1 but now with kde3.2 :(("
    author: "Greek"
  - subject: "Re: some CSS Bugs!"
    date: 2004-02-03
    body: "There is a new html tsting framework, and many bugs fixes did not make into the release yet. This is a .0 release, so small bugs are expected."
    author: "cwoelz"
  - subject: "ArkLinux has 3.2 as default"
    date: 2004-02-03
    body: "For everyone who wants to try KDE 3.2 the easy way, just get the latest version of ArkLinux:\n\nftp://ibiblio.org/pub/Linux/distributions/arklinux/dockyard/iso/arklinux.iso\n\nIt has KDE 3.2 as default alreday :) Also features an easy installer, good config tools and software updating with apt/synaptic\n\n\n"
    author: "Tom"
  - subject: "Re: ArkLinux...what about Mandrake?"
    date: 2004-02-03
    body: "Can some one advise me on where to grap Mandrake 9.2 binaries? I'd also like to know whether kde 3.1.4 and the latest kde can co-exist on the same machine...or does the newly installed one overwrite the old one?\n\nCb..\n"
    author: "charles"
  - subject: "I hate ArkLinux!"
    date: 2004-02-05
    body: "I downloaded it and on install I selected the first option which is to delete OS, and ltitle did I know that it would delete Windows XP, Xandros, and my BACKUP partition! Damn this OS jsut has to fuck you up like this and not even a  warning that it would not delete one OS, but rather all of them and also once you clcik the button you are done for, no confirmation request like in SUSE so if someone accidentally selects the wrong one, bye bye. \n\nNow I have lost most of my data forever and just setting up everything again will take a few weekends. If your wodnering why I didn't select the other options, it's because they didnt work, couldn't resize or anything.\n\nI never want to touch this piece of shit for at least 2 years, I'm aware it's alpha and it could be super buggy for all I know but at least it can be super buggy on its own partition!\n"
    author: "Alex"
  - subject: "Re: ArkLinux...what about Mandrake?"
    date: 2004-03-25
    body: "Mmm, sounds pretty typical of someone how dosnt grasp the concept of what Alpha means, and what should be done before installing an Alpha like actually reading the all the arnings on the website and talking with the development team. Brilliant genius."
    author: "Dude"
  - subject: "Feature Guide?"
    date: 2004-02-03
    body: "Will there be a cool feature guide (s. http://www.kde.org/info/3.1/feature_guide_1.html) like there was for KDE 3.1?\n\nI think it was a nice walk-through the new features and apps.\n\n "
    author: "anonymous"
  - subject: "Re: Feature Guide?"
    date: 2004-02-03
    body: "For new applications see http://kde.ground.cz/tiki-index.php?page=KDE+3.2+Applications+Difference"
    author: "Anonymous"
  - subject: "Re: Feature Guide?"
    date: 2004-02-03
    body: "I think aseigo was working on that -- might take a day or two as I think he's pretty busy atm.\n\nDon't forget the release party on IRC at irc.kde.org in #kde\n\nTroy\n(bored? http://tblog.ath.cx/troy/)"
    author: "Troy Unrau"
  - subject: "Why is it so sloooooow?"
    date: 2004-02-03
    body: "Hi,\n\nI looked forward to try this new release of my favorite DE, and I'm a little bit disaponted with the speed on my SuSE 8.2 Xeon 2Ghz 1Gb RAM.\n\nWhen I launch an app, it takes 15-20 sec and X takes 90% of CPU..\n\nWhat's the problem?\n\nPV\n"
    author: "PV"
  - subject: "Re: Why is it so sloooooow?"
    date: 2004-02-03
    body: "Takes a cuple of seconds here with a pentium III 350 laptop."
    author: "cwoelz"
  - subject: "Re: Why is it so sloooooow?"
    date: 2004-02-03
    body: "It must be a problem with your machine, since no app takes more than about 5 seconds to start on my K6-II 533 with 192MB of RAM running Slackware 9.1."
    author: "Henrique Pinto"
  - subject: "Re: Why is it so sloooooow?"
    date: 2004-02-03
    body: "> It must be a problem with your machine\n\nYou're fast...\n> no app takes more than about 5 seconds to start on my K6-II 533 with 192MB of RAM running Slackware 9.1\n\nHe's slow...\n> I'm a little bit disaponted with the speed on my SuSE 8.2 Xeon 2Ghz 1Gb RAM.\n> When I launch an app, it takes 15-20 sec and X takes 90% of CPU.\n\nIt's not his machine. It's a problem in his setup, possibly fam related.\n\nPeople here's a clue... if launching an application brings your light speed workstation to it's knees you can bet that the developers would not be releasing it and it's another problem related to your set up."
    author: "Eric Laffoon"
  - subject: "Re: Why is it so sloooooow?"
    date: 2004-02-03
    body: "My setup ? Maybe.\nWhat makes me doubt is that my setup is an *out of the box* SuSE. (It's the workstation I use at ...work, so no fantasy here...)\n\nI thought it may be the problem with X and antialiased fonts..\n\nAny help?\n\nPV"
    author: "PV"
  - subject: "Re: Why is it so sloooooow?"
    date: 2004-02-03
    body: "Probably a configuration anomaly with your machine, and could be due to how you built, installed it etc. or the fact that you have a Xeon. \"X takes 90% of the CPU\" is somewhat vague."
    author: "David"
  - subject: "Re: Why is it so sloooooow?"
    date: 2004-02-03
    body: "I didn't built it but installed from Suse 8.2 rpms', satisfiying religiously all the dependencies...\n\nPV"
    author: "PV"
  - subject: "Re: Why is it so sloooooow?"
    date: 2004-02-03
    body: "one more thing:\n\n> \"X takes 90% of the CPU\" is somewhat vague <\n\nThat's what top shows, what should I do? Run X and kde in debug mode, and follow the execution step by step?!\n\n:)\n\nPV"
    author: "PV"
  - subject: "Re: Why is it so sloooooow?"
    date: 2004-02-03
    body: "I've got a similar problem using Debian stable with a backported X Version 4.2.1.1. I think i'll try downgrading and see how it goes. Another problem i run into was that KDE caused a relocation error with my backported version of libfreetype6 (kdeinit: relocation error: /usr/lib/libqt-mt.so.3: undefined symbol: FT_Seek_Stream) and had to downgrade it to the woody version. Any hints on what might be the problem with X cpu usage?\nCheers,\nCirrus"
    author: "cirrus"
  - subject: "Re: Why is it so sloooooow?"
    date: 2004-02-03
    body: "Hmm what could be wrong?\nI downgraded to XFree 4.1.0.1, disable anti-aliasing, and updated my nvidia drivers? Oh and 3.2 look so nice. I am really dissapointed that I can't play around with it"
    author: "Cirrus"
  - subject: "Re: Why is it so sloooooow?"
    date: 2004-02-03
    body: "To me it sounds like it might be a packaging problem, given that its a Xenon and so not as tested. You might ask around forums and mailing lists devoted to SUSE."
    author: "Ian Monroe"
  - subject: "Re: Why is it so sloooooow?"
    date: 2004-02-03
    body: "No i'm using debian stable. Just removed almost everything and reinstalled. Same thing again. damn"
    author: "Cirrus"
  - subject: "Re: Why is it so sloooooow?"
    date: 2004-02-04
    body: "I've done almost everything:(\nI removed and reinstalled almost everything, at i noticed that the problem with X eating up all the cpu occurs only when I have the xfonts-75dpi package installed. Upon remove the package everything was running smoothly. After that I tried to install some truetype fonts, but the same problem was happening. What exactly is going on here?"
    author: "Cirrus"
  - subject: "Re: Why is it so sloooooow?"
    date: 2004-02-05
    body: "Run fc-cache as root maybe?"
    author: "AC"
  - subject: "Re: Why is it so sloooooow?"
    date: 2004-03-08
    body: "Thanks!!!\n\nI also had the same problem: kdeinit used more than 90% CPU time. KDE was almost unusable.\n\nAfter fc-cache it runs smoothly, much faster than 3.1.5.\n"
    author: "espakm"
  - subject: "Re: Why is it so sloooooow?"
    date: 2004-02-03
    body: "I tried to instal at home (Pentium IV, 2Ghz 1Gb, same SuSE 8.2), and it works flawlesly and extremly fast.\n\nPV"
    author: "PV"
  - subject: "Re: Why is it so sloooooow?"
    date: 2004-02-04
    body: "Having Debian KDE 3.2 CVS I can tell you that when you upgrade don't be surprised to note that not all the necessary parts of the packages actually get installed.  I did a dpkg -i --force-all on kdebase and kdelibs amongst other packages and low and behold there were some parts that didn't install until I forced it to install.\n\nThe system runs smoother.  However when I do put 3.2 release I'm going to do a Dselect purge after I relocate .kde to tmp directory, than restore some of my personal preferences back.\n\nKDE doesn't seem to just \"smoothly upgrade.\" (I hold the maintainer more responsible on this than the dev teams)\n\nThat is definitely a no no if they want it to be an \"out-of-the-box\" consumer experience.\n\nThe scripts don't always do what they're supposed to do.\n\nApt-get behaves a bit differently than dpkg as well.\n\nAnd if there are any fixed dependencies (packageA=3.2-...) expect some future headaches, instead of the expected (packageA>=3.2-...) approach when it's time to do smooth and gradual updates to the base and addons.\n\nQt is also in-flux as well.  I'd imagine once Qt3.3 is out of beta and full release any extraneous annoyances that popped up in 3.2.x will be less frequent."
    author: "Marc J. Driftmeyer"
  - subject: "Re: Why is it so sloooooow?"
    date: 2004-02-04
    body: "Check if /dev/shm is enabled in fstab.  It will improve speed of X if you use it.\n\nDid you associate localhost and your machine name with 127.0.0.1 in /etc/hosts?  I had a problem with KDE being very slow in the past and I corrected it by adding these two lines in this file."
    author: "Dominic"
  - subject: "Re: Why is it so sloooooow?"
    date: 2004-02-05
    body: "Hi PV,\n\nI have the same exact problem as you have.\n\n\"When I launch an app, it takes 15-20 sec and X takes 90% of CPU..\" - yeep, absolutely the same :). Otherwise it works fine...\n\nI read your post @21:48 and you said that \"it works flawlesly and extremly fast\" on a different hardware configuration, so is that a some strange incompatibility problem with some hardware (i.e. some video cards)?\n\nMy box is: Celeron 900 MHz, S3 Savage 8 MB Video on Debian GNU/Linux(Sid), XServer 4.2.1, kernel 2.4.23\n\nAny ideas ?"
    author: "ivo_bg"
  - subject: "Re: Why is it so sloooooow?"
    date: 2004-02-05
    body: "My card at work (slow kde 3.2) :nVidia Quadro4 900XGL\nat home (fast kde3.2): nVidia GeForce 4 400MX\n\nsame Xfree :\nXFree86 Version 4.3.0\nRelease Date: 27 February 2003\nX Protocol Version 11, Revision 0, Release 6.6\nBuild Operating System: SuSE Linux [ELF] SuSE\n-------------------------------\n\nKernel at work: 2.4.20-64GB-SMP\nat home: should be the same..\n\n\nPV"
    author: "PV"
  - subject: "Re: Why is it so sloooooow?"
    date: 2004-02-05
    body: "I get 30% - 50% better performance on my 400 MHz Pentium, 128 MB RAM system under SuSE 8.2 using the upgrade packages.\n\nWanna trade computers?\n"
    author: "Tukla Ratte"
  - subject: "Thank you"
    date: 2004-02-03
    body: "A big thanks to all the KDE developers, supporters, translators, etc..  Great work guys.  We really appreciate it.\n\nbrockers"
    author: "brockers"
  - subject: "3.2 is amazing."
    date: 2004-02-03
    body: "I've been using 3.2 RC1 (and more recently 3.2 final, grabbed it off CVS yesterday), and I must say that the difference between Konq in 3.1 and 3.2 is like night and day.  I have even come across sites that Konq renders perfectly, and FIREBIRD fails!\n\nThe second highlight for me is the new and improved Kate editor.  It is without a doubt the best GUI text editor I have ever used on any platform.  Simple, lightweight, fast, and has project/folding/highlighting support.\n\nIf it weren't for my reliance on Photoshop for my digital darkroom, I would most certainly make Linux and KDE 3.2 the default on my main box.  I eagerly await Gimp 2.2 (maybe KDE 3.3/4 will be out by then? ;)\n"
    author: "Wheaty"
  - subject: "Re: 3.2 is amazing."
    date: 2004-02-03
    body: "Photoshop actually runs under Crossover Office, might be worth a try out.  http://www.codeweavers.com/site/compatibility/browse/name?app_id=8\nVersions 5,6,7 all 3 get a silver rating.  I don't know what bugs might still exist.  But it may be worth it."
    author: "Someone"
  - subject: "Re: 3.2 is amazing."
    date: 2004-02-04
    body: "it seems to run under regular wine as well. here are the instructions for a specific version:\nhttp://frankscorner.org/modules.php?op=modload&name=Sections&file=index&req=viewarticle&artid=94&page=1\nthere are probably some minor bugs though using regular wine, not present in the crossover version. disney actually paid crossover to support photoshop:  http://www.eweek.com/article2/0,3959,1210083,00.asp\n"
    author: "Mark Dufour"
  - subject: "change shading of workplace???"
    date: 2004-02-03
    body: "Hi,\n\nwhere I can change the shading of workplace text under icons??\nIt's look realy bad on my notebook. I can't read the text.\n\nThanks.\n\n"
    author: "Kid"
  - subject: "Re: change shading of workplace???"
    date: 2004-02-03
    body: "That is very strange, since the shadows were designed to *improve* the readability of the text in all cases.  Could you post a screenshot of your problem?\n\nTo turn off the shadows, right-click the desktop, choose \"Configure Desktop\", go to the \"background\" panel, click \"Advanced Options\", and uncheck \"Enable Shadow\"."
    author: "Spy Hunter"
  - subject: "Re: change shading of workplace???"
    date: 2004-02-03
    body: "Deja Vu.  I swear I just read this question (and answer) about 3 screens up.\n\n:)"
    author: "Caleb Tennis"
  - subject: "Congratulations!"
    date: 2004-02-03
    body: "Well done everyone! I have seen how much work went into this, and it is really very humbling. The breadth and depth of the K Desktop is simply astounding; just KDevelop alone is a tech powerhouse.\n\nThere are probably less than five companies in the world that could get something like this out of the door. That it is a largely volunteer effort is a true sign of the strength of open source, and the KDE community.\n\nRespect to everyone involved."
    author: "Dominic Chambers"
  - subject: "Mandrake packages"
    date: 2004-02-03
    body: "Yes, this is the same old question ;)\n\nDoes anyone know anything about (non-cooker) Mandrake packages for KDE 3.2?"
    author: "Richard Jones"
  - subject: "Re: Mandrake packages"
    date: 2004-02-03
    body: "I think we are out of luck. Look here ftp://ftp-stud.fht-esslingen.de/pub/Mirrors/ftp.kde.org/pub/kde/stable/3.2/\neverybody but Mandrake :-(\nMaybe its time to switch distro - they did the same late thing with 3.1x"
    author: "J. Andersen"
  - subject: "Re: Mandrake packages"
    date: 2004-02-04
    body: "Bummer. I really want to be running 3.2 but a source compile on my laptop would take forever. Anyone want to step up to the plate and put up Mandrake rpms, please? :/"
    author: "repito"
  - subject: "Re: Mandrake packages"
    date: 2004-02-04
    body: "folks! Mandrake 10beta is out and it includes KDE 3.2pre and the new 2.6 kernel!\n\nI'm waiting for the Mandrake 10 community edition which should be out by the end of the month! It will run kde 3.2final out of the box.  I've been considering trying the new kernel and the new KDE but since I am wary of breaking my 9.2 install, I'm going to wait a couple of weeks and give Mandrake 10 a try. http://www.mandrakelinux.com/en/100beta.php3 is the site for the beta.  Normally I don't upgrade often, but this seems worth it, speaking of reinstalling distros, what is it with people that switch distros completely every two weeks when a new feature/program is released instead of a) learning to compile from source or b) waiting for a package to be created for the desired product?  Are these the same people that reinstall windows every month instead of running scandisk and defrag? \n\n"
    author: "Matt"
  - subject: "Re: Mandrake packages"
    date: 2004-02-04
    body: "lol they want also the xfree-4.4 RC ? Btw the 2.6 kernel is not stable enough.\nWell it's one more MDK edition that will be again totally unstable."
    author: "JC"
  - subject: "Re: Mandrake packages"
    date: 2004-02-05
    body: "LOL?\n\nI'm running current Cooker and it is not unstable. I don't experience crushes, XFree 4.4 RC is stable enough, and for your information kernel in Cooker is 2.6.2.\n\nAnd BTW, kde 3.2 runs perfectly here."
    author: "wygiwyg"
  - subject: "Re: Mandrake packages"
    date: 2004-02-04
    body: "I just want Mandrake to be up-to-date when major updates arrive. It seems to me that other distros manage to do this very soon after the releases. If Mandrake released some update plan like \"14 february KDE 3.2 packages will be available for users of Mandrake 9.2 , 9.1.. whatever\" i would wait without complaining. They just seem to concentrate on the next release and forget users of the actual distro. Its frustrating - even MS does this better with their SP's.\n\nCheers\nA still Mandrake user"
    author: "J. Andersen"
  - subject: "woody+konsole+fonts"
    date: 2004-02-03
    body: "Hi,\n\nI insatlled KDE 3.2 on woody. Overall KDE 3.2 is very nice desktop with great new features. Yes, there are bugs... For example Kontroll Center sometimes going crazy and start kdeinit which eats the CPU.\n\nHowever the fonts are ugly in konsole on woody. I try to setup a nice fixed fonts for it (Kosnole -> Settings -> Font -> Custom), but I can't. All of them are ugly. I don't know why only few fonts available on the custom font menu. In kde3.1.4 I've so many fonts in the custom selections, and the fonts looks great. :-(\n\nAny idea?\n\nSahin\n\n"
    author: "Sahin"
  - subject: "Re: woody+konsole+fonts"
    date: 2004-02-05
    body: "Hi Sahin,\n\nI have the same problem, too.\nIf I go to\nControlcenter->System->Fonts (or similiar, using other localization),\nthere I can see many, many fonts as root.\nBut all those ending with pcf.gz seems not working. No Preview available.\nHave you already solved the problem?\nAny idea what's going on?\n\n"
    author: "Ulf"
  - subject: "Re: woody+konsole+fonts"
    date: 2004-02-06
    body: "Konsole uses only fixed width fonts.\n\nWhich fixes width fonts do you have installed?\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: woody+konsole+fonts"
    date: 2004-02-11
    body: "Does woody use fontconfig?\n\nTo solve this problem on unstable, you would make sure you have bitmapped fonts installed (xfonts-base, xfonts-75dpi/xfonts-100dpi), and then dpkg-reconfigure fontconfig and select \"Enable bitmapped fonts\".\n\nMaybe this is applicable to Woody."
    author: "Fenris Ulf"
  - subject: "fedora kde 3.2 rpms "
    date: 2004-02-03
    body: "Hi\n\nIf anyone is looking for fedora rpms get them here\n\nftp://ftp-stud.fht-esslingen.de/pub/Mirrors/ftp.kde.org/pub/kde/stable/3.2/RedHat/Fedora\n\n189 mb total\n\nregards\nrahul sundaram"
    author: "rahul sundaram"
  - subject: "Re: fedora kde 3.2 rpms "
    date: 2004-02-03
    body: "Are this fedora core 1 or rawhide (developement) version?"
    author: "Anonymous"
  - subject: "Re: fedora kde 3.2 rpms "
    date: 2004-02-04
    body: "Hi\n\nthese are fc1 rpms"
    author: "rahul sundaram"
  - subject: "Re: fedora kde 3.2 rpms "
    date: 2004-02-04
    body: "Are these packages compiled with mp3 and xine support ?\nOr without them as in official Fedora packages ?\n\nB/W will there be packages by KDE-RedHat team ? \nI have been using their packages for long and very very \nsatisfied with them.\n\nthanks \n\nAsok"
    author: "Asokan K"
  - subject: "Re: fedora kde 3.2 rpms "
    date: 2004-02-05
    body: "I doubt redhat's packages on kde.org include mpe/xine support.\n\nKDE-RedHat (http://kde-redhat.sf.net/) packages should be arriving in the next day or two.  Stay tuned.\n\n-- Rex"
    author: "Rex Dieter"
  - subject: "Re: fedora kde 3.2 rpms "
    date: 2004-02-05
    body: "Wow..  A Big thanks to KDE-RedHat\n\n--Asok"
    author: "Asokan K"
  - subject: "Unpleasant bug. How to repair it?????"
    date: 2004-02-03
    body: "when you start konsole and look with w who is logged in, you see two users.\nif you start more konsoles you will see more users.\nwhatis wrong with this version of kde (kde 3.2 stable)???\nwhere were no such bug in 3.2RC1"
    author: "Entirer"
  - subject: "Re: Unpleasant bug. How to repair it?????"
    date: 2004-02-03
    body: "That behavior was in Unices since xterms appeared (and that's a long, long time ago). Every virtual terminal (let it be xterm, eterm, gterm or konsole) will be attached to a /dev/pts* virtual device. Thus, every time you launch a konsole, you create a new virtual device /dev/pts*, and run a getty and a bash on it, effectively increasing the times you're logged into the system.\n\nTime to say \"it's not a bug, it's a feature\"..."
    author: "ivansanchez@escomposlinux.org"
  - subject: "Re: Unpleasant bug. How to repair it?????"
    date: 2004-02-05
    body: "That's not quite true.  konsole, xterm, eterm, gterm, etc don't cause a new user to be \"logged in\" when they start up.  They simply fork another bash shell.  getty is only involved on the virtual terminals available at Alt-F1, Alt-F2, etc.\n\nI don't have KDE3.2 final installed yet (emerge kde is running right now ), but konsole should *not* cause additional logins.\n\nBelow is a snapshot of my system under 3.2RC1 that shows konsole doesn't behave in the way you described.\n\ngfortune@inviso done $ ps -Af | grep \"konsole\";who\ngfortune 25707 10219  0 Jan25 ?        00:02:54 kdeinit: konsole\ngfortune 27248 10219  0 Jan26 ?        00:00:05 kdeinit: konsole\ngfortune  1582 10219  0 Feb03 ?        00:00:07 kdeinit: konsole\ngfortune  3681 10219  0 Feb03 ?        00:00:03 kdeinit: konsole\ngfortune  5954 10219  0 11:34 ?        00:00:02 kdeinit: konsole\ngfortune 20098 10219  0 13:02 ?        00:00:10 kdeinit: konsole\ngfortune 21049 25720  0 19:36 pts/6    00:00:00 grep konsole\nroot     vc/1         Jan 23 12:25\ngfortune vc/2         Jan 23 12:25\nroot     vc/3         Jan 24 17:42\ngfortune vc/4         Jan 24 17:44\n"
    author: "Greg Fortune"
  - subject: "Re: Unpleasant bug. How to repair it?????"
    date: 2004-02-04
    body: "the konsole behaviour depends on how 'utempter' is installed i believe ... (i'm not sure if the program name is right). If you don't like that behaviour, just remove the setuid flag from it\n"
    author: "ik"
  - subject: "3.2 for sid?"
    date: 2004-02-03
    body: "it's still not there :("
    author: "Pat"
  - subject: "Remote control"
    date: 2004-02-03
    body: "Does the kde3.2 remote-control functionality depend on lirc, or does it\nwork by directly accessing the serial port?"
    author: "irda"
  - subject: "Re: Remote control"
    date: 2004-02-03
    body: "AFAIK it's a kde frontend to lirc."
    author: "anonymous"
  - subject: "And god said....."
    date: 2004-02-03
    body: "Let there be KDE!\n\nmmmmmm can't wait for slackware pkg's"
    author: "Namaseit"
  - subject: "Re: And god said....."
    date: 2004-02-04
    body: "Read the announcement\nthey are already in /contrib"
    author: "JC"
  - subject: "Why are you installing 3.2?"
    date: 2004-02-04
    body: "In an attempt to start a postive thread, here's a nifty question for everybody:\n\nWhy (specifically) are you installing 3.2?\n\nFor myself, the answer is disconnected IMAP.  I often work at a diner with no bandwidth so that I can code without distractions.  They know me there, and give me an outlet and loads of coffee.  The one thing I'd like to take with me is my email.  The other reason is that I'm on a fairly slow laptop (300Mhz, but it runs MySQL and Apache as well as KDE), so any speed increases are welcome.\n\nI'm curious as to what feature of 3.2 is making everybody so excited about this release.  I'd imagine it's different for everybody.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Why are you installing 3.2?"
    date: 2004-02-04
    body: "Konqueror. The web browser works for me, as a file manager it is quick enough to be useful. Quanta is almost always open.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Why are you installing 3.2?"
    date: 2004-02-04
    body: "Because as a gentooer when I type 'emerge -UD world' that's what it does. And I feel guilty if I don't do that once in a while (after an emerge system of course.)\n\nI'm annoyed that it seems like only a couple of weeks ago that my puter spent goodness knows how long building 3.1.x Now here in the S hemishphere where its summer all that extra processor heat is not very helpful.\n\nSeriously though I'm keen to check out that UML stuff."
    author: "Stewart"
  - subject: "Re: Why are you installing 3.2?"
    date: 2004-02-04
    body: "For me, while 3.1 was a terrific release, and good enough to get me off Windows permanently, there are still little things here and there that mildly annoy me.  They're too small to bother enumerating, but I do hope 3.2 takes some of them away."
    author: "Sean O'Dell"
  - subject: "Re: Why are you installing 3.2?"
    date: 2004-02-04
    body: "Konqueror."
    author: "-"
  - subject: "Re: Why are you installing 3.2?"
    date: 2004-02-04
    body: "various small features:\n\n1. Vertical selection in konsole.\n2. Minitools in konqueror (I keep my bookmarks in a php program that has the posibility to insert bookmarks directly using a minitool).\n3. Code folding for perl in Kate (at least!!!)\n4. Kwallet (very useful for someone like me who has a lor of accounts on a lot of websites)\n5. Kontact (drag-and-drop from mail to todo's and stuff like that)\n6. Konqueror's tabs much better handled.\n\nto name a few\n"
    author: "SorinM"
  - subject: "Way to go KDE developers!"
    date: 2004-02-04
    body: "This is a huge step forward for Linux on the desktop.\n\nIn fact, with this release it is a good time to donate, like all OSS projects KDE is in need of money and any contributions, they really should advertise this more."
    author: "Alex"
  - subject: "Difficulty downloading"
    date: 2004-02-04
    body: "Why is it taking so long to be distributed to the mirrors, none of the listed UK mirrors have it yet.  I don't remember this sort of problem with any of the other releases."
    author: "Nonamenobody"
  - subject: "Re: Difficulty downloading"
    date: 2004-02-04
    body: "Complain to those mirror admins that they don't let them update more often."
    author: "Anonymous"
  - subject: "Unnecessary requirements"
    date: 2004-02-04
    body: "OK, so I've built CVS HEAD a few dozen times, and I have about every dependency possible satisfied, but I'd rather not have unneeded requirements.\n\nhttp://www.kde.org/info/requirements/3.2.php shows that X, Qt, Perl, bzip2, zlib, PCRE and libpng are required. I'm not sure about Perl, but I'm confident that at least libpng and PCRE are not required. Useful, but no more required than OpenSSL, libjpeg, and probably less critical than libxml2 and libxslt.\n\nWe need a more consistent approach."
    author: "Brad Hards"
  - subject: "Re: Unnecessary requirements"
    date: 2004-02-04
    body: "well, without libpng you won't see icons. But Qt got a copy of it, so you get it anyway.\n\nAnd pcre is simply required, period."
    author: "coolo"
  - subject: "Re: Unnecessary requirements"
    date: 2004-02-04
    body: "./configure --help says it can disable PCRE...\n"
    author: "Brad Hards"
  - subject: "Sound doesn't work"
    date: 2004-02-04
    body: "I installed the SuSE packages and now I don't have sound anymore. Whenever an application wants to output sound it freezes, even non KDE apps (I tried a website with Flash under Mozilla). \n\nStefan\n"
    author: "Stefan Fricke"
  - subject: "Re: Sound doesn't work"
    date: 2004-02-04
    body: "I had the same thing w/ RC1.\nYesterday, there was a power failure here, and when my computer came back up, sound was on. Wierd. Reboot, or go through a few RC scripts."
    author: "Rich Jones"
  - subject: "Re: Sound doesn't work"
    date: 2004-02-04
    body: "On my Laptop, the SuSE packages came up with no sound.  Turned out that the volume levels were off... as in turned off.  When I set everything to middle, Coven's One Tin Soldier blasted me.  A little tweaking later, and it's good.\n\nI moved my .kde directory; I think the defaults are incorrect.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Sound doesn't work"
    date: 2004-02-04
    body: "I tried this all but nothing did work. \n\nStefan\n"
    author: "Stefan Fricke"
  - subject: "Re: Sound doesn't work"
    date: 2004-02-05
    body: "Make sure you upgraded arts.\n\nYou can try a console based app like mpg123 to play an mp3 file and see if you've got sound.  If so, it's almost certainly a problem with arts."
    author: "Greg Fortune"
  - subject: "Re: Sound doesn't work"
    date: 2004-02-05
    body: "Hmmmmmmmm.........  I did as you suggested and tried mpg123 and there was no sound for me still.  So this would suggest a problem at a lower level would it not?  Possibly an ALSA problem?\n\nPaul....."
    author: "cirehawk"
  - subject: "Re: Sound doesn't work"
    date: 2004-02-06
    body: "I can boot into runlevel 3 and then mpg123 works. But after starting KDE it doesn't, even after shutting down KDE. \n\nWhile KDE is running all apps which are using sound freeze, even non KDE apps. I can restart ALSA while KDE is running then the apps don't freeze anymore but still no sound. I have tries all sound server and volume settings but nothing.\n\nStefan\n"
    author: "Stefan Fricke"
  - subject: "Re: Sound doesn't work"
    date: 2004-02-06
    body: "I can confirm this, SUSE 9.0 and KDE 3.2 binary packages just killed my sound system (VIA onboard sound). I can't play anything. Several users reported this already. Sounds used to be fine under beta2.\n"
    author: "Jasem Mutlaq"
  - subject: "Re: Sound doesn't work"
    date: 2004-02-06
    body: "I have reported it to bugs.kde.org. If you want, track bug #74401\n\nStefan\n"
    author: "Stefan Fricke"
  - subject: "Re: Sound doesn't work"
    date: 2005-10-20
    body: "I have a AC'97 sound card, so i install the drivers everything plays except i don't get a sound and everything is perfectly connected!!"
    author: "John"
  - subject: "KDE 3.2 and ksim"
    date: 2004-02-04
    body: "Um, what ever happened to ksim in KDE 3.2? I installed the whole package for Fedora core 1 and it disappeared. Looks like the libraries are still there but the actual binary is nowhere to be found."
    author: "Andy"
  - subject: "Re: KDE 3.2 and ksim"
    date: 2004-02-04
    body: "You want to ask \"what happened to ksim in Fedora rpms\"? Installed from source and it's fine."
    author: "Anonymous"
  - subject: "Re: KDE 3.2 and ksim"
    date: 2004-02-04
    body: "Did you install the specific Fedora rpms here?\nftp://ftp.us.kde.org/pub/kde/stable/3.2/RedHat\nI get this:\n\n$ which ksim\n/usr/bin/which: no ksim in (/usr/adabas/bin:/usr/adabas/pgm:/usr/adabas/bin:/usr/adabas/pgm:/usr/kerberos/bin:/usr/local/bin:/bin:/usr/bin:/usr/X11R6/bin:/home/achoi/bin)\n\nThe libraries for ksim seem to be in the right spot, but there is no binary."
    author: "Andy"
  - subject: "Re: KDE 3.2 and ksim"
    date: 2004-02-08
    body: "KSim was turned into a panel extension, Panel Menu/Add/Panel/KSim."
    author: "Anonymous"
  - subject: "Re: KDE 3.2 and ksim"
    date: 2005-08-27
    body: "Hi. Anonymous. For months I've been trying to get Ksim running on anything later than FC1. I resorted to installing the tar.gz for Ksim on FC2 which worked ok, then when Debian was working I tried the tar.gz on that too. Few problems here caused by qt_dir, but eventually it installed , but with only a desktop icon. Had to click on the icon each time the system was started. Now got Gentoo installed with KDE 3.4, and still no Ksim. Now I'm getting a bit T'd off. Rooting around on the KDE site I find your post. Man, I can't thank you enough. I had Gentoo booted up on the other machine at the time. Right click panel, add, panel, Ksim, and it was there like magic. It messed the icons on the desktop about a bit, but thats no problem. I mean. Why can't KDE notify you when they are changing stuff. Perhaps the first time you run Konqueror, a \"please read this\" notice for changes, etc. The same problem exists for Kmidi having been dumped from versions later than FC1 with 2.4 kernel. Thanks again for the fix. Nigel."
    author: "Nigel"
  - subject: "kdelibs 3.2 won't compile"
    date: 2004-02-04
    body: "I'm eager to get my hands on kde 3.2, however, I've got the problem compiling kdelibs. Here is a what I got at the end of compilation:\n\n/bin/sh ../libtool --silent --mode=link --tag=CXX g++  -Wnon-virtual-dtor -Wno-long-long -Wundef -Wall -W -Wpointer-arith -Wwrite-strings -ansi -D_XOPEN_SOURCE=500 -D_BSD_SOURCE -Wcast-align -Wconversion -Wchar-subscripts -DNDEBUG -DNO_DEBUG -O2 -O3 -funroll-loops -mcpu=athlon -march=athlon -fomit-frame-pointer -fno-exceptions -Wformat-security -Wmissing-format-attribute -fno-exceptions -fno-check-new -fno-common  -DQT_CLEAN_NAMESPACE -DQT_NO_ASCII_CAST -DQT_NO_STL -DQT_NO_COMPAT -DQT_NO_TRANSLATION    -o dcopserver -R /usr/local/kde/lib -R /usr/local/qt/lib -R /usr/X11R6/lib -no-undefined -L/usr/X11R6/lib -L/usr/local/qt/lib -L/usr/local/kde/lib  dcopserver.la.o libkdeinit_dcopserver.la\n/opt/sybase/lib/libsybdb.so: undefined reference to `__ctype_b'\n/opt/sybase/lib/libsybdb.so: undefined reference to `__ctype_tolower'\ncollect2: ld returned 1 exit status\nmake[4]: *** [dcopserver] Error 1\nmake[4]: Leaving directory `/usr/local/kde-3.2.0/kdelibs-3.2.0/dcop'\nmake[3]: *** [all-recursive] Error 1\nmake[3]: Leaving directory `/usr/local/kde-3.2.0/kdelibs-3.2.0/dcop'\nmake[2]: *** [all] Error 2\nmake[2]: Leaving directory `/usr/local/kde-3.2.0/kdelibs-3.2.0/dcop'\nmake[1]: *** [all-recursive] Error 1\nmake[1]: Leaving directory `/usr/local/kde-3.2.0/kdelibs-3.2.0'\nmake: *** [all] Error 2\n\nIt seems it somehow interferes with the sybase open client library (I have version 11.9.2 installed).\nI configure kdelibs with options --enable-final --disable-debug --with-alsa --enable-fast-malloc=full\narts configured with --enable-final --disable-debug\nQt 3.2.3 (recommended version) is configured with\n\n-I/opt/sybase/include -I/usr/local/include/mysql -L/usr/local/lib/mysql -L/opt/sybase/lib -qt-gif -system-libpng -system-libmng -system-zlib -system-libjpeg -no-g++-exceptions -thread -qt-sql-tds -qt-sql-mysql -no-xinerama \n\nI didn't have this problem with the RC1, although I also compiled qt with sybase support (but I haven't had mysql support compiled in qt at that time).\n\nAny help would be appritiated. I really would like to have sybase tds support in qt and hope there is a way around this problem.\n\nSergey"
    author: "Sergey"
  - subject: "Re: kdelibs 3.2 won't compile"
    date: 2004-02-04
    body: "Your 2.3.x glibc lacks a compatibility patch. Either bug your distro to fix\nyour glibc to include old-rotten-compat symbols :) or if you just want to build kde build the sybase support in qt as plugin, which will allow you to build&run kde. You won't be able to use the sybase support, because your sybase lib uses a private glibc symbol."
    author: "Anonymous"
  - subject: "Re: kdelibs 3.2 won't compile"
    date: 2004-02-04
    body: "I'm suspecting something like that is happening, however, I don't think you're right, because I built kde 3.2 RC1 a few weeks ago using qt 3.2.1, and everything compiled/linked just fine.\nI'm using RH 9.0 as a basis, glibc is  2.3.2-11.9 (the latest one available from RH web site). I didn't have problems compiling kde 3.2 beta 1 and 2 as well.\nSo, changes from the last succesfull compile:\nqt went from 3.2.1 to 3.2.3;\nqt compiled with mysql support;\nkde went from 3.2 RC1 to 3.2.0\n\nWould someone with better knowledge of kde 3.2/qt inners help me to isolate what's happening, and if possible, fix it?\n\nThanks,\n\nSergey"
    author: "Sergey"
  - subject: "please tell us which QT to use in each release"
    date: 2004-02-04
    body: "Hello,\n\nWhen i'm going to upgrade my KDE, i'll build it from source; I would like to know every time which QT to use. This time it seems it's QT 3.2, but in one thread here Waldo Bastian mentions the qt-copy has important fixes.\nSo, qt-copy? in a branch? i guess qt-copy head is for KDE-head? and how will it be for 3.2.1?\nWould konstruct take care of this for me?\n\nplease, mention this, it's important.\n\nthanks, emmanuel."
    author: "emmanuel"
  - subject: "Re: please tell us which QT to use in each release"
    date: 2004-02-04
    body: "sorry, i just saw http://www.kde.org/info/requirements/3.2.php\n:O)\nbut the question still stands: would qt-copy be better than qt 3.2.3?\n\nthanks and sorry for the noise, emmanuel"
    author: "emmanuel"
  - subject: "Re: please tell us which QT to use in each release"
    date: 2004-02-04
    body: "For KDE 3.2 is the recommended Qt version 3.2.3. qt-copy/HEAD contains Qt 3.3 Beta 1 (final later today?). qt-copy/QT_3_2_BRANCH contains basically Qt 3.2.3. The important fixes are in the patches/ directory and should be applied with apply_patches. Konstruct contains these patches and applies them to the Qt 3.2.3 tarball. Any question left? :-)"
    author: "Anonymous"
  - subject: "Re: please tell us which QT to use in each release"
    date: 2004-02-04
    body: "thank you very much.\nI will use konstruct then... (but maybe i'll wait for 3.2.1).\n\nI'm happy that konstruct is patching QT by itself. installing KDE from sources is easy otherwise."
    author: "emmanuel"
  - subject: "Ugly menu seperators"
    date: 2004-02-04
    body: "Is there any way to turn off (or at least tone down) the new menu seperators in the K menu? (i.e. \"Most Used Applications\", \"All Applications\", etc). \n\nThey look quite nice, but I think they draw the eye away from the actual menu items (Especially under the 'Plastik' theme. It might not sound like much, but it's an added annoyance to have to stop and focus when I'm whipping around the menu.\n\n"
    author: "Bob"
  - subject: "Re: Ugly menu seperators"
    date: 2004-02-04
    body: "http://kde.ground.cz/tiki-index.php?page=Secret+Config+Settings knows how to turn them off."
    author: "Anonymous"
  - subject: "konqi toolbar"
    date: 2004-02-04
    body: "is there anything I can do to make the spaces between konqi toolbar icons equal, and change text like \"Home URL\" to \"Home\"? check the screenshot. I have tried reconfiguring toolbar with no luck. the icons only option is out for me."
    author: "anaconda"
  - subject: "Re: konqi toolbar"
    date: 2004-02-04
    body: "Change the source - or create an \"anaconda\" language. :-)"
    author: "Anonymous"
  - subject: "Move, copy, ..."
    date: 2004-02-04
    body: "Nice to see that the order in konqueror drag&drop popup menu\nhave changed so that \"Move\" now is the first menu choise instead\nof the more seldom used \"Copy\".\n\nThis was one of the problems I found in a recent usability study of\nKDE. However according to the study it would be even better to remove the menu alltogheter as it did not work well for unexperienced users. Some of them didn't even notice the menu, as they moved on to the next task in the firm belief that they actually had performed an operation when the file was dropped (This was totally unexpected). The main problem was that they thougth turned up annonyingly often as they had no idea how to use modifier keys to select multiple files.\n\nExperienced users had no problems though. To our surprise experienced usrs liked it, as they thought it would be a good help for those less experienced than themselves.\n\n\nThe study is in Swedish but I will try to get a shareable Enlish version\nout as soon as possible."
    author: "Uno Engborg"
  - subject: "Howto use mouse gestures"
    date: 2004-02-04
    body: "I just don't get it..\n\nI added a gesture in KHostKeys for testing but I can't get it working..\n\nWhere do I have to klick and move?"
    author: "David"
  - subject: "kdesdk doesn't compile"
    date: 2004-02-04
    body: "kdesdk-3.2.0 doesn't compile. It ends with the following output:\nmake[4]: Entering directory `/home/michael/kde-3.2/kdesdk-3.2.0/kbabel/common/libgettext'\n/bin/sh ../../../libtool --silent --mode=compile --tag=CXX g++ -DHAVE_CONFIG_H -I. -I. -I../../..   -DQT_THREAD_SUPPORT  -D_REENTRANT  -Wnon-virtual-dtor -Wno-long-long -Wundef -Wall -W -Wpointer-arith -Wwrite-strings -ansi -D_XOPEN_SOURCE=500 -D_BSD_SOURCE -Wcast-align -Wconversion -Wchar-subscripts -DNDEBUG -DNO_DEBUG -O2 -Wformat-security -Wmissing-format-attribute -fno-exceptions -fno-check-new -fno-common -DQT_CLEAN_NAMESPACE -DQT_NO_ASCII_CAST -DQT_NO_STL -DQT_NO_COMPAT -DQT_NO_TRANSLATION  -c -o pofiles.lo `test -f 'pofiles.cc' || echo './'`pofiles.cc\nIn file included from /usr/include/c++/3.3.2/backward/iostream.h:31,\n                 from /usr/include/FlexLexer.h:47,\n                 from pofiles.cc:241:\n/usr/include/c++/3.3.2/backward/backward_warning.h:32:2: warning: #warning This file includes at least one deprecated or antiquated header. Please consider using one of the 32 headers found in section 17.4.1.2 of the C++ standard. Examples include substituting the <X> header for the <X.h> header for C++ includes, or <sstream> instead of the deprecated header <strstream.h>. To disable this warning use -Wno-deprecated.\nIn file included from /usr/include/FlexLexer.h:47,\n                 from pofiles.cc:241:\n/usr/include/c++/3.3.2/backward/iostream.h:36: error: `istream' is already\n   declared in this scope\n/usr/include/c++/3.3.2/backward/iostream.h:36: error: using declaration `\n   istream' introduced ambiguous type `istream'\npofiles.cc:401:5: warning: \"YY_STACK_USED\" is not defined\npofiles.cc: In member function `virtual int GettextBaseFlexLexer::yylex()':\npofiles.cc:683: error: cannot convert `std::istream*' to `istream*' in\n   assignment\npofiles.cc: In member function `void\n   GettextBaseFlexLexer::yy_load_buffer_state()':\npofiles.cc:1216: error: cannot convert `istream*' to `std::istream*' in\n   assignment\npofiles.cc: In member function `void\n   GettextBaseFlexLexer::yy_init_buffer(yy_buffer_state*, std::istream*)':\npofiles.cc:1267: error: cannot convert `std::istream*' to `istream*' in\n   assignment\npofiles.cc:1458:5: warning: \"YY_MAIN\" is not defined\nmake[4]: *** [pofiles.lo] Fehler 1\nmake[4]: Leaving directory `/home/michael/kde-3.2/kdesdk-3.2.0/kbabel/common/libgettext'\nmake[3]: *** [all-recursive] Fehler 1\nmake[3]: Leaving directory `/home/michael/kde-3.2/kdesdk-3.2.0/kbabel/common'\nmake[2]: *** [all-recursive] Fehler 1\nmake[2]: Leaving directory `/home/michael/kde-3.2/kdesdk-3.2.0/kbabel'\nmake[1]: *** [all-recursive] Fehler 1\nmake[1]: Leaving directory `/home/michael/kde-3.2/kdesdk-3.2.0'\nmake: *** [all] Fehler 2\nAny idea?\n\nBest regards hotroot"
    author: "hotroot"
  - subject: "Re: kdesdk doesn't compile"
    date: 2004-02-04
    body: "Oh, sorry. found a solution:\nI commented out line 27 and added include <istream.h>\n\n[...]\n#ifdef __cplusplus\n\n#include <stdlib.h>\n//class istream;\n#include <istream.h>\n#include <unistd.h>\n\n/* Use prototypes in function declarations. */\n[...]\n\nnow it works\n\nhotroot"
    author: "hotroot"
  - subject: "Re: kdesdk doesn't compile"
    date: 2004-02-04
    body: "Thanks for that. I had the same problem and ended up disabling kbabel from being compiled."
    author: "Paul"
  - subject: "Re: kdesdk doesn't compile"
    date: 2004-02-11
    body: "You're using an old version of flex that doesn't support the new ANSI C++ header files.  Upgrading flex makes the errors involving FlexLexer.h go away.\n\nStill won't compile for me though, I get \"`yy_current_buffer' undeclared\" now.  Time for more detective work."
    author: "ferrante"
  - subject: "Re: kdesdk doesn't compile"
    date: 2004-02-18
    body: "No it doesn't. I've tried the newest 2.5.4a and 2.5.31. It don't fix it."
    author: "Glen"
  - subject: "Re: kdesdk doesn't compile"
    date: 2004-02-23
    body: "\nAfter upgrading to flex-2.5.31 one must forcibly remove:\n\n% /src/kdesdk-3.2.0> rm kbabel/common/libgettext/pofiles.cc\n\nto have it re-generated from 'pofiles.ll' on the next make\n(which completes happily :)\n\nBTW: I'm using gcc-3.3.3"
    author: "Mauro DePalma"
  - subject: "Re: kdesdk doesn't compile"
    date: 2004-02-24
    body: "Ahhh....\n\nThat does it. I wish I'da known that before. This should be in the FAQ. I've been seeing the question around a little bit. All of the answers on the net say 'upgrade flex', but NONE say 'get rid of pofiles.cc' !\n\nGlen\n"
    author: "Glen"
  - subject: "Re: kdesdk doesn't compile"
    date: 2005-11-11
    body: "According to gentoo ebuild the bug is relativ to gcc version.\ngcc3 need the gentoo patch attached"
    author: "Vincent LE LIGEOUR"
  - subject: "german language file avaible and suse packages "
    date: 2004-02-04
    body: "hi,\ni downloaded the suse 9 packages by de.kde.org - i installed them with kpackage and didn't check dependics. Now kde 3.2 is running but\n\n1. the sound doesnt't work\n\n2. i cant find the german language file\n\ncan anybody help me? The sound is very important for me :-(\n\nbye"
    author: "mj"
  - subject: "Re: german language file avaible and suse packages "
    date: 2004-02-04
    body: "> 2. i cant find the german language file\n\nhttp://download.kde.org/stable/3.2/SuSE/noarch/"
    author: "Anonymous"
  - subject: "Re: german language file avaible and suse packages"
    date: 2004-02-04
    body: "it runs, thx"
    author: "mj"
  - subject: "Bad SUSE packaging?"
    date: 2004-02-04
    body: "I can confirm that sound stopped working after upgrading to 3.2 using SUSE 9.0 packages. Adrian...are you listening? Also, whats the deal with this conflict between kdebase packages?"
    author: "Nadeem Hasan"
  - subject: "Problems :("
    date: 2004-02-04
    body: "Perhaps it isn't the best place to ask this but, here I go...\n\nOn a SuSE 9.0 with defalt KDE 3.1.4, when upgrading to kde 3.2, how can I resolve the conflict with kdebase3-SuSE? Just a rpm -i --nodeps? \n\nOther : Alsa seems to not work properly.  It get really noisy and unusable. I goggled the web but nothing found.  Anyone knows the trouble?\n\nLast (sorry).  Perhaps it is not a KDE problem. I installed SuSE 9.0 and it works really fine but, after some days of work, it really slows down.  KDE startup takes up to 5 minuts to finnish and apps startup time gets horrible too.  I removed the user and created a new one and I resolved the problem for a few days, but it slowed down again.  I reinstalled everything (clean install, reformatted partitions) and resolved the problem for a few days only again.  Can it be a problem of reiserfs? I havent' tried with ext2 file system.\n\nKDE 3.2 is, on the other hand, clean and intuitive.  Shadow Desktop text, tranparent kicker, Plastik style, it makes a actually good quality desktop.\n\nThanks.\n\n"
    author: "Sendo"
  - subject: "Re: Problems :("
    date: 2004-02-04
    body: ">On a SuSE 9.0 with defalt KDE 3.1.4, when upgrading to kde 3.2, how can I resolve the conflict with kdebase3-SuSE? Just a rpm -i --nodeps? \n\neasy uninstall kdebase3-SuSE :-)\n\n>Other : Alsa seems to not work properly. It get really noisy and unusable. I goggled the web but nothing found. Anyone knows the trouble?\n\nI've got this problem, too :-("
    author: "mj"
  - subject: "Re: Problems :("
    date: 2004-02-09
    body: "You may want to run kmix and turn off things like \"Analog audio in\" and other cryptic things which you hardly use. This worked for me -- at least, I got rid of all scratches and awful sound quality on my SuSE 9.0 box at home.\n\nThe problem still remains. StepMania, VBA, xmms+arts output plugin -- they don't work with the new aRts at all, even after they've been recompiled. mplayer works just fine, btw. I've noticed a number of other bugs, at least ten of them, but since now I've had read the disclaimer, it sounds better to wait for the official builds.\n\nI'm thinking to roll back to kde-3.1.5 that was working great at home for the time being, and I didn't even bother to upgrade to 3.2 at work."
    author: "oujirou"
  - subject: "Re: Problems :("
    date: 2004-02-09
    body: "Sadly, I think I'll back to KDE 3.1.4, because another grave bug had appeared.  When I close my session, even logout, even tunoff or restart KDE hangs my computer so when I turn on my linux again I always have to go to mantenince mode to run fsck manually.  I don't know why, but does my box unsusefull :(.\n\nAlthoug, I'll try to fix it for some days.\n\nSendo ;)"
    author: "Rossend Bruch"
  - subject: "Re: Problems :("
    date: 2004-02-09
    body: "(This was sent to Rossend by email, I'm posting it here so the response can be googled later >8) )\n\nIt is possible that kdm is asking your X server to reset instead of completely restarting it.  That would create the issue you're mentioning.  My system is running XF86 4.3.0 with the radeon driver, and it can't reset properly.  Assuming your kde is in the usual place, edit this file:\n/opt/kde/share/config/kdm/Xservers\n\nThere should be a line like this:\n:0 local@tty1 /usr/X11R6/bin/X vt7\n\nAdd the -terminate option like so:\n:0 local@tty1 /usr/X11R6/bin/X -terminate vt7\n\nYou may also be interested in the -br option; it tells X to start up with a black background instead of the ancient crosshatch pattern. :)\n\nAnyway, after you make the change, see if it helps with the lockup issue.\n\n-- \nMatt Howard "
    author: "Matt Howard"
  - subject: "Re: Problems :("
    date: 2004-02-11
    body: "I had the same speed problems. KDE runs fine for a while but slowly grinds to a halt. I tried editing the Xservers file, but this did not help. I also tried a clean .kde directory and removing the files in /tmp, but this also changed nothing.\n\nFinally, I completely removed KDE from my system and installed the packages from the SuSE ftp site. Everything seems in order now. \n\nI guess there might be something wrong with the KDE packages, but I've been unable to track it down. My advice is to just stick with the SuSE rpms.\n\nhope this helps\n\nadios"
    author: "adios"
  - subject: "Re: Problems :("
    date: 2004-02-11
    body: "There are no \"KDE packages\". The SUSE packages on ftp.kde.org are from the same person than the one on the SUSE ftp site."
    author: "Anonymous"
  - subject: "Re: Problems :("
    date: 2004-02-16
    body: "Using SuSE 9 Pro with KDE 3.2 but with gdm instead of kdm and everything seems to work fine.\n\nOn a side note. If you're trying to use superkaramba on this system then be sure to install 33 instead of 32b. The weather theme will work perfectly :)\n\nCheers to the KDE team for the awesome work. "
    author: "macewan"
  - subject: "SuSE issues"
    date: 2004-02-04
    body: "I have installed the 3.2 rpms, but there are a few problems ... \n1) The menu is all messed up. The development menu comes up no more .. \n2) Transparent kicker doesnt work properly ... the whole image is a bit shifted towards the left at the portion of kicker. \n3) The new window manager creates more problems than it solves : whenever i minimize xmms, only the main window gets minimized, the others stay where they are. Worse still i cannot alt-tab to them. "
    author: "sarath"
  - subject: "Re: SuSE issues"
    date: 2004-03-23
    body: "I use Mandrake 10 Community and have the same problem with XMMS (but when I installed Cooker RPMs of KDE 3.2 on Mandrake 9.2 XMMS minimized correctly). Maybe it is somehow connected with some libraries (GTK or GTK+?)."
    author: "Blaster999"
  - subject: "Re: SuSE issues"
    date: 2004-03-23
    body: "The same problem with XMMS in Fedora Core v1 / KDE 3.2 ):"
    author: "Fanaz"
  - subject: "Satisfying Suse 9 Dependencies"
    date: 2004-02-04
    body: "I've noticed that some of you have just skipped satisfying the dependencies that the Suse RPM asks for, I was just wondering if anyone had noticed any adverse effects due to this.\n\nshould I \n1. Spend the time satisfying the dependencies?\n2. Skip it?\n3. Wait for an RPM that has the libs within?"
    author: "David A Taylor"
  - subject: "Re: Satisfying Suse 9 Dependencies"
    date: 2004-02-05
    body: "It depends on what dependancies you encounter on your suse installation.\n"
    author: "rinse"
  - subject: "KDE 3.2 Performs best on Slackware 9.1"
    date: 2004-02-04
    body: "I have tried, Redhat, Mandrake, Fedora, SuSE. But I found Slackware the fastest and most responsive Linux Distro ever. It works nicely with just 128mb ram with 64mb swap!!! \n\nKDE 3.1.4 and 3.2 is much faster in Slackware 9.1 than any other distro. Hats Off to KDE 3.2 and Slackware Developers..."
    author: "Slackware 9.1 User"
  - subject: "konstruct compile error while builidng QT on RH7.3"
    date: 2004-02-04
    body: "Hi I am running a RH7.3 machine (yes... it's old)\nand attempted to build KDE 3.2 release via\nkonstruct. I get the following error when\nit as building QT's designer I assume?\nCan anyone point out what lib that I may\nhave missing or need to upgrade?\nMy suspicion is something to do with\nzlib or libpng?\n\n\ng++ -fno-exceptions  -Wl,-rpath,/usr/local/kde3.2/lib -o ../../../bin/designer .obj/release-shared-mt/main.o   -L/tmp/konstruct/libs/qt-x11-free/work/qt-x11-free-3.2.3/lib -L/usr/X11R6/lib -L/usr/X11R6/lib -L/tmp/konstruct/libs/qt-x11-free/work/qt-x11-free-3.2.3/lib -L/usr/X11R6/lib -ldesignercore -lqui -lqassistantclient -lqt-mt -lpng -lz -lGLU -lGL -lXmu -lXrender -lXinerama -lXft -lfreetype -lXext -lX11 -lm -lSM -lICE -ldl -lpthread\n.obj/release-shared-mt/main.o: In function `main':\n.obj/release-shared-mt/main.o(.text+0xa18): undefined reference to `qInitImages_designercore()'\n/tmp/konstruct/libs/qt-x11-free/work/qt-x11-free-3.2.3/lib/libdesignercore.a(mainwindow.o): In function `MainWindow::MainWindow[not-in-charge](bool, bool, QString const&)':\nmainwindow.o(.text+0x5c1): undefined reference to `qInitImages_designercore()'\n/tmp/konstruct/libs/qt-x11-free/work/qt-x11-free-3.2.3/lib/libdesignercore.a(mainwindow.o): In function `MainWindow::MainWindow[in-charge](bool, bool, QString const&)':\nmainwindow.o(.text+0x12e1): undefined reference to `qInitImages_designercore()'\ncollect2: ld returned 1 exit status\nmake[8]: *** [../../../bin/designer] Error 1\nmake[8]: Leaving directory `/tmp/konstruct/libs/qt-x11-free/work/qt-x11-free-3.2.3/tools/designer/app'\nmake[7]: *** [sub-app] Error 2\nmake[7]: Leaving directory `/tmp/konstruct/libs/qt-x11-free/work/qt-x11-free-3.2.3/tools/designer'\nmake[6]: *** [sub-designer] Error 2\nmake[6]: Leaving directory `/tmp/konstruct/libs/qt-x11-free/work/qt-x11-free-3.2.3/tools'\nmake[5]: *** [sub-tools] Error 2\nmake[5]: Leaving directory `/tmp/konstruct/libs/qt-x11-free/work/qt-x11-free-3.2.3'\nmake[4]: *** [build-work/qt-x11-free-3.2.3/Makefile] Error 2\nmake[4]: Leaving directory `/tmp/konstruct/libs/qt-x11-free'\nmake[3]: *** [dep-../../libs/qt-x11-free] Error 2\nmake[3]: Leaving directory `/tmp/konstruct/libs/arts'\nmake[2]: *** [dep-../../libs/arts] Error 2\nmake[2]: Leaving directory `/tmp/konstruct/kde/kdelibs'\nmake[1]: *** [dep-../../kde/kdelibs] Error 2\nmake[1]: Leaving directory `/tmp/konstruct/kde/kdebase'\n"
    author: "no name"
  - subject: "Re: konstruct compile error while builidng QT on R"
    date: 2004-02-15
    body: "I have the same problem on RH 9, so it's not related to your distribution version.\n"
    author: "skintwin"
  - subject: "Re: konstruct compile error while builidng QT on R"
    date: 2004-04-01
    body: "Did we find a resolution to this error? I'm using FC-1 with kernel 2.4.22-2.2174 and I get this error as well.\n\nmake[5]: *** [.obj/release-shared-mt/qpngio.o] Error 1\nmake[5]: Leaving directory `/home/techlaw/download/tmp/konstruct/libs/qt-x11-free/work/qt-x11-free-3.2.3/src'\nmake[4]: *** [sub-src] Error 2\nmake[4]: Leaving directory `/home/techlaw/download/tmp/konstruct/libs/qt-x11-free/work/qt-x11-free-3.2.3'\nmake[3]: *** [build-work/qt-x11-free-3.2.3/Makefile] Error 2\nmake[3]: Leaving directory `/home/techlaw/download/tmp/konstruct/libs/qt-x11-free'\nmake[2]: *** [dep-../../libs/qt-x11-free] Error 2\nmake[2]: Leaving directory `/home/techlaw/download/tmp/konstruct/libs/arts'\nmake[1]: *** [dep-../../libs/arts] Error 2\nmake[1]: Leaving directory `/home/techlaw/download/tmp/konstruct/kde/kdelibs'\nmake: *** [dep-../../kde/kdelibs] Error 2\n\nI sent an email to the developer when I had this error before.  He said that I didn't have the Xfree86-devel package installed.  However, now I do:\n\n[techlaw@localhost kdebase]$ rpm -qa X*devel*\nXFree86-devel-4.3.0-55\n\nand I get this same error.  Any who can help sort this out?\n\nThanks."
    author: "Michael Stelly"
  - subject: "Re: konstruct compile error while builidng QT on R"
    date: 2004-04-01
    body: "\"this error\" cannot be because it happens in another directory. And unless you post the part of the log relevant to the error nobody can help."
    author: "Anonymous"
  - subject: "Re: konstruct compile error while builidng QT on R"
    date: 2004-07-30
    body: "Hi There,\nlet me join Your problem,\nI have the same on woody debian with kernel 2.4.20.\nI cat the errmsg to this text:\n\n.obj/release-shared-mt/main.o: In function `main':\n.obj/release-shared-mt/main.o(.text+0xab8): undefined reference to `qInitImages_designercore(void)'\n/usr/src/installed/kde/konstruct/libs/qt-x11-free/work/qt-x11-free-3.2.3/lib/libdesignercore.a(mainwindow.o): In function `MainWindow::MainWindow(bool, bool, QString const &)':\nmainwindow.o(.text+0x581): undefined reference to `qInitImages_designercore(void)'\ncollect2: ld returned 1 exit status\nmake[4]: *** [../../../bin/designer] Fehler 1\nmake[3]: *** [sub-app] Fehler 2\nmake[2]: *** [sub-designer] Fehler 2\nmake[1]: *** [sub-tools] Fehler 2\nmake: *** [build-work/qt-x11-free-3.2.3/Makefile] Fehler 2"
    author: "klaus_h"
  - subject: "artsd sucks"
    date: 2004-02-04
    body: "Ilove KDE, but how can it be that 3.2 is released with a bug like this still not fixed:\n\nhttp://bugs.kde.org/show_bug.cgi?id=70802\n\nI couldn't get arts to work on my 2.6 kernel, so now I have to use xmms instead of JuK.\n\nIf konqueror won't render some pages correctly, that might be annoying. But no sound on 2.6 kernels? Thats a showstopper!\n\nDid I miss something?"
    author: "me"
  - subject: "Re: artsd sucks"
    date: 2004-02-04
    body: "Ilove KDE, but how can it be that 3.2 is released with a bug like this still not fixed:\n\non kernel 2.4 (suse) the sound doesnt work, too!"
    author: "mj"
  - subject: "Re: artsd sucks"
    date: 2004-02-04
    body: "Don't blame KDE for SUSE packages. I have SuSE, Kernel 2.4 and sound - but compiled from source."
    author: "Anonymous"
  - subject: "Re: artsd sucks"
    date: 2004-02-04
    body: "And I have SuSE 8.2, kernel 2.4 -- and installed the 3.2 packages today with that apt-rpm thingy, and I have sound, too. Listening to some madrigals at the moment, in fact, with noatun. On the other hand, I'm having a hard time getting KOffice cvs to compile :-(. No clone tool for Krita today, I'm afraid."
    author: "Boudewijn Rempt"
  - subject: "Re: artsd sucks"
    date: 2004-02-05
    body: "hmm i had not used apt, i installed the packages with kpackage :-( When I uninstall the 3.2 packages and install the standart suse packages, the sound doesnt work anymore :-("
    author: "mj"
  - subject: "Re: artsd sucks"
    date: 2004-02-05
    body: "maybe it doent work because i became a error in rc1 - \"You have no permissions in /tmp/dcop-martin\" so i deleted the folder - now i become no errors and no sound :-( Can i restore the folders?\n\nThanks\n\nbye"
    author: "mj"
  - subject: "Re: artsd sucks"
    date: 2004-02-05
    body: "nobody knows how i can fix the problem? :-("
    author: "mj"
  - subject: "Re: artsd sucks"
    date: 2004-02-05
    body: "that is your problem, not kde's."
    author: "me"
  - subject: "Re: artsd sucks"
    date: 2004-02-05
    body: "I have another problem with artsd from KDE 3.1.95 compiled from sources on Slackware-current: artsd crashes every time when it tries to mix two sounds. I use oss (linux-2.4.24), but alsa-lib is also present on my system."
    author: "Andrey V. Panov"
  - subject: "Re: artsd sucks"
    date: 2004-02-05
    body: "well using -current it's normal to have bugs as it's only a test version.\nAlso use the final release of KDE. There are fixes from 3.1.95"
    author: "JC"
  - subject: "Re: artsd sucks"
    date: 2004-02-05
    body: "Kernel 2.6.1 with ALSA support for the onboard intel sound.  Arts performs flawlessly.  Now, it might be using the OSS compatability, but I haven't checked that."
    author: "Duncan"
  - subject: "Re: artsd sucks"
    date: 2004-02-13
    body: "I don't like artsd either. I've always used the KDE-Applications in ICEWM but now they all load artsd and ignore my kcontrol-setting \"don't use soundsystem\"!\n\nI have to kill xmms every time after it has stopped because of this artsd. Offen I get this warning dialoges because my sounddevice is already busy.\n\nEverything becomes extremely annoying, when I have used kwrite as root and it has loaded artsd.\nThen I have to open xterm, become root and kill that trash.\n\nEverything was fine, the speed, the design, but artsd is really a showstopper."
    author: "Schugy"
  - subject: "Re: artsd sucks"
    date: 2005-05-12
    body: "Ok, enough stories, but is there already a solution to this?  Because I started to think that the sound chipset became out of order, and neither do I have sound.  I don't see anyone saying \"this is what you have to do...blah blah blah\" and sound gets back!"
    author: "xasb"
  - subject: "Re: artsd sucks"
    date: 2005-09-15
    body: "Similar grief here.\nplain unadulterated fresh install of suse 9.3\nfollowed by the automatic online updates\nkernel, kde, xmms, all apps only from suse packages from an official suse 9.3 ftp mirror so far except boinc/seti@home.\n\nartsd crashes about once a day or so. I don't have to exit xmms. I just have to go to kde control center and uncheck & re-check \"use sound system\" & hit apply and it starts artsd again. Then just hit play in xmms and it reconnects to the stream I always listen to and works for another day or so. Doesn't seem to matter if I'm actually playing audio, it'll sometimes be crashed already when I go to hit play after coming home from work and everything was sitting idle all day.\n\nAnnoying as hell. If I were trying to avoid loading arts and silly things like a text editor kept loading it like the other guy says I'd be junking kde wholesale the first time that happened. I usually never use kde/gnome exactly for reasons like this. But I figured I should at least try doing things the \"normal\" way once in a while or else I'm talking through my ... when I advize people to reject these gross fat \"desktop environments\".\n\nlinux:~ # cat /dev/sndstat\nSound Driver:3.8.1a-980706 (ALSA v1.0.9a emulation code)\nKernel: Linux linux 2.6.11.4-21.8-smp #1 SMP Tue Jul 19 12:42:37 UTC 2005 i686\nConfig options: 0\n\nInstalled drivers: \nType 10: ALSA emulation\n\nCard config: \nIntel ICH6 with ALC655 at 0xfdffe000, irq 185\n\nAudio devices:\n0: Intel ICH6 (DUPLEX)\n\nSynth devices: NOT ENABLED IN CONFIG\n\nMidi devices: NOT ENABLED IN CONFIG\n\nTimers:\n7: system timer\n\nMixers:\n0: Realtek ALC655 rev 0\nlinux:~ # \n\n"
    author: "Brian K. White"
  - subject: "Re: artsd sucks"
    date: 2005-10-12
    body: "Go into your KDE Desktop Admin.\n\nClick Sound\nClick Sound System\nUncheck \"Run with the highest possible priority\"\n\nYou can also play with your buffer here if you have skip problems. System hasn't crashed since (Used to several times / hour when I had my regular windows open. Ie. LOTS!)."
    author: "Adam Walker"
  - subject: "Re: artsd sucks"
    date: 2005-11-16
    body: "Fedora Core 4 running icewm.\n\nAlong with that, I set the buffer VERY SHORT to get the notification sounds to work properly.  I guess a larger buffer is a problem with small sound bytes.\n"
    author: "darthwonka"
  - subject: "Re: artsd sucks"
    date: 2006-06-15
    body: "No my friend, you did not miss a thing.  You started this thread in 2004, when ARTS sucked.  Now it is 2006, and ARTS sucks just the same, if not more for still being a part of KDE.  How this crap is an integral component of the best non-commercial desktop in existence is beyond me.\nI run only Debian, and every single time I have attempted to use ARTS, it has failed.  Not only that, but failed miserably.  This is over a period of several years, mind you.  Sometimes I get a strange philanthropic urge and decide to try ARTS again.  EVERY TIME, I GET BURNED.\nARTS is one of the worst pieces of non-commercial software available.  I LOVE KDE, and do everything in my power to keep ARTS away from me.  But it just keeps coming back, like THE EVIL DEAD.\n"
    author: "Clint Allen"
  - subject: "Re: artsd sucks"
    date: 2006-08-13
    body: "i read here, that Arts is going to be stripped from KDE 4.0 because it is unmaintained.\n\nThis was in an interview with one of the KDE Developers. http://www.linuxdevcenter.com/pub/a/linux/2006/01/12/kde4.html\n\n\"selecting a new multimedia system (aRts is unmaintained and not providing what we need)\n\nJL: Right. I remember Stefan changed his thinking on that approach. What are the main alternatives now?\n\nAJS: GStreamer, NMM, and MAS. GStreamer looks like the most probable candidate at this point.\"\n\nAlthough as KDE 4.0 isn't released yet, 14-08-2006 it is all well possible they may change their minds."
    author: "me."
  - subject: "CC"
    date: 2004-02-04
    body: "I wonder why the control center graphics are still in the old design?"
    author: "Gerdilein"
  - subject: "Mouse problem"
    date: 2004-02-04
    body: "Well done!!! Thank you all.\nI installed kde 3.2 yesterday and it is a (nearly) perfect software. It runs much faster compared to my old kde 3.1. \nBut I have one problem:\nThe mouse does not work well in the K-menu. Sometimes it works, sometimes the mouse \"hangs\" and I can\u00b4t clik on a program to start it. After I press a key (eg. alt-tab), all works fine. That does not happen every time, only sometimes.\nKDE 3.1 worked fine with the same hardware.\n\nMy system:\n2.4Ghz Athlon, 512 MB, PS/2 wheel mouse.\nRedHat 9.0a, Kernel 2.4.20-18 KDE 3.2, installed from \nbinary RPMs via rpm -Uvh -nodeps \n\nThanks for help!\nWolfgang\n"
    author: "Wolfgang"
  - subject: "Re: Mouse problem"
    date: 2004-02-06
    body: "What RPMs you used for installation? Fedora's? I would like to run 3.2 on my RH9 comps, but I see no RH9 RPMs."
    author: "Tomislav Sajdl"
  - subject: "Re: Mouse problem"
    date: 2004-02-06
    body: "Yes, I used the Fedora RPMs. Installing KDE 3.2 with the Fedora RPMS was working well without any problems. And it runs fine (and fast!). \nThe only problem I have is, that the mouse sometimes \"hangs\". It happens only in the K-menu. I move around the menu items and sometimes the mouse movment stops. And sometimes I can click on a item/program and nothing happens. When I press a key (eg. Alt) after the \"hang\" the menu disappers and the mouse is working again. \nUsing menus in other programs (kde, gnome, gtk) is working fine.\nI took a look at my XF86config (XFree 4.3) file and removed an entry for the usb-mouse, because I use a PS/2 mouse. But it does not help. \nThe same configuration (hardware, kernel, XFree) worked perfect with KDE 3.1.\nAny idea?\n"
    author: "Wolfgang"
  - subject: "Re: Mouse problem"
    date: 2004-02-06
    body: "Afaik it was a known Qt issue which has been fixed in KDE's Qt copy and the patch had been sent to Trolltech already. There were several bug reports about it but I can't find them right now."
    author: "Datschge"
  - subject: "Re: I got the same problem"
    date: 2004-02-12
    body: "mouse doesn't work well, \n\nsometimes, I need to click a application icon several times on the kicker to launch or maximum it, very very frustrating, I am thinking about going back to gnome, but still struggling, hope I could fix it, haven't been using gnome form years... \n\nI use fedora core 1."
    author: "Mudong"
  - subject: "Re: Mouse problem"
    date: 2004-02-12
    body: "Arghhh!  I've got the same problem as well.  But it's not just the Menu, it's also happening in the panel.  Mouse clicking sometimes (in fact most of the time), doesn't work right.\n\nThe weird thing is this.  I had the same problem with the first Beta.  And then, when I installed the RC, the problem went away.  I figured \"Yay.. this is fixed...\"\n\nNow, I've installed 3.2.. and the problem is back. It's very frustrating.  Anyone can help?\n\nThank you!\n\nLaura"
    author: "Laura"
  - subject: "Re: Mouse problem"
    date: 2004-02-12
    body: ">Anyone can help?\n\nI solved the problem as follows:\nI removed Klipper from the panel. Right-click on it, than click on remove,  Miniprogram, Klipper.\n(I hope, that I translated it correct, because I use a German version of KDE)\nFunny: when you re-use Klipper the mouse problem will not come up again. \nDont ask me why ;-)\n\nWolfgang\n\n"
    author: "Wolfgang"
  - subject: "Re: Mouse problem"
    date: 2004-02-12
    body: "Wow!  That worked just spiffy.  Who'd a thunk it?  Thank you very much, Wolfgang.\n\nYour English was just fine. Only difference is \"Miniprogram\" = \"Applet\"\n\nI  wish I knew German :)\n\nNow I am no longer frustrated. I really appreciate you posting your solution. Weird solution though, huh? "
    author: "Laura"
  - subject: "Re: Mouse problem"
    date: 2004-02-13
    body: "It is possible the problem will be also gone if you use standalone Klipper (i.e. not applet). See also http://bugs.kde.org/show_bug.cgi?id=74487 .\n"
    author: "Lubos Lunak"
  - subject: "Re: Mouse problem"
    date: 2004-03-03
    body: "Thank you!"
    author: "Vaclav"
  - subject: "Re: Mouse problem"
    date: 2004-03-26
    body: "Thanks for the suggestion, this has been driving me mad - all fixed now!\n\nReally strange the way you can put Klipper back again afterwards and it's OK.\n\nAh well, we have a saying, \"Never look a gift horse in the mouth\" which means: if it's good - don't ask!\n\nPeter"
    author: "Peter Salisbury"
  - subject: "I am having problem with the mouse too"
    date: 2005-01-25
    body: "i have a problem with mouse, it does not work properly especially when i want to click on the address book in my email. if i click on insert address from email address book it will just show GREEN EXCLAIMATION MARK.. why is it so.. and what should i do to make it working again like it used to be???this ia happen too when i want to click on some other link page.. the GREEN eclaimation mark will show out.. but before this it is just perfect.. but not now.. can someone help me??? thanks.."
    author: "aniki"
  - subject: "KDE Review"
    date: 2004-02-05
    body: "Nice KDE 3.2 review at: Fedora News.Org\n\nhttp://fedoranews.org/krishnan/review/kde3.2/\n\nThanks to all the devs./others - looking forawrd to the next release :)"
    author: "anon"
  - subject: "Konquer always opens mpeg links with Kaboodle?"
    date: 2004-02-05
    body: "Just installed KDE 3.2 (on Gentoo).  The only problem I've noticed is that Konquerer (web browser) always opens mpeg links using Kaboodle embedded in a new browser window.  It seems to totally ignore the file associations I set using the control panel (or going to Configure Konqueror in the settings menu).  I also can't stop it from opening Kaboodle in a new browser window.\n\nHere is the weird part... the mpeg file associations work perfectly when using Konqueror as a file manager.\n\nAnyone else have this problem?"
    author: "Rimmer"
  - subject: "Re: Konquer always opens mpeg links with Kaboodle?"
    date: 2004-02-05
    body: "Did you change the file associations in the embedded tab as well?\n\nRinse"
    author: "rinse"
  - subject: "Re: Konquer always opens mpeg links with Kaboodle?"
    date: 2004-02-05
    body: "I went into the embedded tab and changed to \"Show file in seperate viewer.\" \n\nHowever, it still insists on using Kaboodle embedded in a new browser window.  Oh btw... the movie never actually opens (hence my desire to change to something else).\n\n"
    author: "Rimmer"
  - subject: "Re: Konquer always opens mpeg links with Kaboodle?"
    date: 2004-02-06
    body: "Did you remove kaboodle from the list of embedded players?\n\nRinse"
    author: "rinse"
  - subject: "Re: Konquer always opens mpeg links with Kaboodle?"
    date: 2004-02-06
    body: "Yes... removing the embedded player from the list worked.  This still seems to be a major bug.  How do I report it?"
    author: "Rimmer"
  - subject: "Re: Konquer always opens mpeg links with Kaboodle?"
    date: 2004-02-06
    body: "Go in konqueror to menu [help->bug report] Click on the link in the popup dialog and follow the bug wizard on bugs.kde.org \n\nRinse"
    author: "rinse"
  - subject: "Re: Konquer always opens mpeg links with Kaboodle?"
    date: 2004-04-30
    body: "I had the same problem with Firefox 0.9 for viewing web pages. Fixed it by choosing the \"Use settings for 'application' group\" option on the Embedded page. I think this should be the default option so changes made in the Find filename pattern window is the default option.\n\nHope this helps."
    author: "steve"
  - subject: "Konqueror Won't Use Seperate Viewer For Anything?"
    date: 2004-02-05
    body: "Hmmm... I tried downloading some PDF files.  Despite telling Konqueror to \"Show file in seperate viewer\" it continues to show the PDF files using an embedded viewer.\n\nAnyone else have this problem?"
    author: "Rimmer"
  - subject: "Artsd really sucks up CPU"
    date: 2004-02-05
    body: "SOmething I've started noticing - my artsd process chews ~30 - 40% CPU time when any app is using it (such as JuK), and my system load rises past 2.  This tends to cause some apps to start responding more slowly.  Apart from turning artsd off, is there any fix for this?"
    author: "Duncan"
  - subject: "Re: Artsd really sucks up CPU"
    date: 2004-02-08
    body: "Did you try telling artsd to use ALSA?"
    author: "Rimmer"
  - subject: "Re: Artsd really sucks up CPU"
    date: 2004-02-08
    body: "I've just found that the Fedora RPMS from kde.org don't have ALSA enabled!  Pulled down the source rpm and I'm rebuilding kdemultimedia with alsa enabled.  Will see what happens."
    author: "Duncan"
  - subject: "Re: Artsd really sucks up CPU"
    date: 2004-02-08
    body: "Well, rebuilts both arts and kdemultimedia - arts to get alsa, kdemultimedia to get mp3 support.  Artsd now chews 1 - 2% of my cpu.  Yay!"
    author: "Duncan"
  - subject: "Edit file type?"
    date: 2004-02-05
    body: "The edit-file-type, in context menu (upto 3.1) was really useful, and made the process of associating a file extension with a program very easy.  \n\nThis feature has been removed in KDE 3.2. Took me quite a while to change the default behavior, searching thru konqi settings.\n\nIs there any easier way to do this ?\n "
    author: "anon"
  - subject: "Re: Edit file type?"
    date: 2004-02-05
    body: "Select \"Properties\" in the context menu and click an the wrench icon."
    author: "Anonymous"
  - subject: "Re: Edit file type?"
    date: 2004-02-05
    body: "Wow! that is almost impossible to figure, unless you already know about it."
    author: "anon"
  - subject: "Re: Edit file type?"
    date: 2004-02-06
    body: "Believe it or not, those responsible for this think that it is a usability improvement.  I hope that if they refuse to  change it back that they will at leaset make it possible to configure it to work the old way by editing some '*rc' file.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Edit file type?"
    date: 2004-02-06
    body: "What where the arguments for putting this under some obscure icon?"
    author: "ac"
  - subject: "Re: Edit file type?"
    date: 2004-02-06
    body: "I have been using KDE since 1.x, and I could not figure this out... at least they could have a text statement in front of the wrench - \"configure file type\".\n"
    author: "anon"
  - subject: "Re: Edit file type?"
    date: 2004-02-06
    body: "It's right next to the file \"Type\" text so I guess it was considered as apparent enough. Probably turning the whole text next to \"Type:\" into a button would be even more apparent?"
    author: "Datschge"
  - subject: "Re: Edit file type?"
    date: 2004-08-13
    body: "For me the problem is, that in debian it doesn't work, I have no idea what to do with it, as far as I remember I reported it, the only thread with the same problem I found is here:\nhttp://lists.debian.org/debian-kde/2004/06/msg00206.html\n\nAnybody experiencing the same problem i.e. the wrench button doesn't trigger any action?"
    author: "Dan Hill"
  - subject: "I tip my hat to the developers"
    date: 2004-02-05
    body: "Just wanted to chip in with a big congrats to the developers of this amazing DE.  Since my laptop is a P4 2.0 MHz with 512 MB of RAM, I wasn't expecting to see much improvement in performance since KDE was fast already.  Man, was I mistaken!  You guys realy know how to optimize.  I haven't finished trying out all the new features, and testing the bugs I knew of, but I have a feeling I won't be disappointed.  Keep up the great work.\n\nCheers,\n\nJuan."
    author: "Juan I. Guardia"
  - subject: "Re: I tip my hat to the developers"
    date: 2004-02-05
    body: "I meant P4 2.0 GHz."
    author: "Juan I. Guardia"
  - subject: "Debug mesages"
    date: 2004-02-05
    body: "I installed thru konstruct - works great, except a minor annoyance that if I start an application from konsole, I see a lot of debug messages.\n\nAny way to get rid of them (without having to recompile the whole thing again) ?\n\nThx"
    author: "anon"
  - subject: "Re: Debug mesages"
    date: 2004-02-06
    body: "I'm not running 3.2 yet, but I think you can turn them off with the program 'kdebugdialog'."
    author: "AC"
  - subject: "Re: Debug mesages"
    date: 2004-02-06
    body: "Cool! Thanks a lot :-)\n"
    author: "anon"
  - subject: "Compiling KDE 3.2 with icc 8.0"
    date: 2004-02-07
    body: "Is it possible? Comments?"
    author: "tetabiate"
  - subject: "desktop problem - wine gets called device open"
    date: 2004-02-08
    body: "Hello,\n\ni have previously upgraded from 3.15 to 3.2. Now i have a problem when opening device links from the desktop. On click the device is mounted, but konqi doesn't open. Instead the windows emulator wine tries to open the device. I tried to find a configuration to change these wrong association. Does someone know where to change this?\n\nThanks in advance\nJochen"
    author: "Jochen"
  - subject: "Big Step Backwards For Me"
    date: 2004-02-09
    body: "Since installing KDE 3.2:\n\nNo sound\n\nPixiePlus doesn't remember its last window position\n\nKate forgets that I had the terminal window open last time, so I have to re-setup my window layout each and every time I open Kate\n\nKonqueror tabs are really unreliable; sometimes I get a tab and sometimes URLs are opened in the current window\n\nKonqueror context menus hide the action items I used to click a lot in a sub-menu, which takes away a LOT of the convenience of using context menus\n\nDefault theme is far uglier than the default for 3.1.4\n\nLogin dialog hides other desktop choices in a sub-menu, instead of having it right on the login dialog where it used to be\n\n\nPlus an endless myriad of little annoyances...I think I'm going back to 3.1.4 and will check out a future release of KDE and upgrade when it's actually BETTER than the last version and not worse."
    author: "Sean O'Dell"
  - subject: "Re: Big Step Backwards For Me"
    date: 2004-02-09
    body: "> No sound\n> Default theme is far uglier than the default for 3.1.4\n\nYou obviously have package problems (eg the default theme didn't change). And this will not change unless you report your problems to the creator of your packages."
    author: "Anonymous"
  - subject: "fail to start kde after konstruct build"
    date: 2004-02-10
    body: "After I've built konstruct using gcc 2.96 on redhat 7.3\nunder /usr/local/kde32, I set up my env as follows:\n\nexport KDEDIRS=/usr/local/kde32/\nexport KDEHOME=~/.kde\nexport PATH=\"$KDEDIRS/bin:$PATH\"\nexport LD_LIBRARY_PATH=\"$KDEDIRS/lib:$LD_LIBRARY_PATH\"\n\nand typed 'startx' to start up kde. Soon I get messages\nlike \"pager failed to start\", \"kpanel failed to start\" and etc.\nLooking at the output to 'startx' showed the following:\nHow can I diagnose this? \n\nXFree86 Version 4.2.0 (Red Hat Linux release: 4.2.0-8) / X Window System\n(protocol Version 11, revision 0, vendor release 6600)\nRelease Date: 23 January 2002\n        If the server is older than 6-12 months, or if your card is\n        newer than the above date, look for a newer version before\n        reporting problems.  (See http://www.XFree86.Org/)\nBuild Operating System: Linux 2.4.17-0.13smp i686 [ELF]\nBuild Host: daffy.perf.redhat.com\n\nModule Loader present\nMarkers: (--) probed, (**) from config file, (==) default setting,\n         (++) from command line, (!!) notice, (II) informational,\n         (WW) warning, (EE) error, (NI) not implemented, (??) unknown.\n(==) Log file: \"/var/log/XFree86.0.log\", Time: Mon Feb  9 18:41:30 2004\n(==) Using config file: \"/etc/X11/XF86Config-4\"\nstartkde: Starting up...\nCould not open library dcopserver.la: no symbols defined\nCould not load library! Trying exec....\nkdeinit: Launched DCOPServer, pid = 2979 result = 0\n_kde_IceTransmkdir: Owner of /tmp/.ICE-unix should be set to root\nksplash: Inactive pixmap: /usr/local/kde32/share/apps/ksplash/Themes/Default/spl\nash_inactive_bar.png\nksplash: Active pixmap:   /usr/local/kde32/share/apps/ksplash/Themes/Default/spl\nash_active_bar.png\nksplash: Standard theme loaded.\nDCOP: register 'anonymous-2979' -> number of clients is now 1\nDCOP: unregister 'anonymous-2979'Could not open library klauncher.la: no symbols\n defined\nCould not load library! Trying exec....\nkdeinit: Launched KLauncher, pid = 2994 result = 0\nDCOP: register 'klauncher' -> number of clients is now 1\nDCOP: unregister 'klauncher'\nDCOP: register 'klauncher' -> number of clients is now 1\nQPixmap: Cannot create a QPixmap when no GUI is being used\nQPixmap: Cannot create a QPixmap when no GUI is being used\nlibrary=krandrinithack.la: No file names krandrinithack.la found in paths.\nDCOP: new daemon klauncher\nCould not open library kded.la: no symbols defined\nCould not load library! Trying exec....\nkdeinit: Launched KDED, pid = 2996 result = 0\nkdeinit: PID 2995 terminated.\nDCOP: register 'kded' -> number of clients is now 1\nDCOP: unregister 'kded'\nDCOP: register 'kded' -> number of clients is now 1\nDCOP: register 'anonymous-2996' -> number of clients is now 2\nkio (KDirWatch): Available methods: Stat, FAM\nkio (KLauncher): KLauncher: Got kdeinit_exec_wait('kbuildsycoca', ...)\nkdeinit: Got EXEC_NEW 'kbuildsycoca' from launcher.\nCould not open library kbuildsycoca.la: no symbols defined\nCould not load library! Trying exec....\nDCOP: register 'anonymous-2976' -> number of clients is now 3\nDCOP:  'anonymous-2976' now known as 'ksplash'\nkio (KLauncher): kbuildsycoca (pid 2999) up and running.\nQPixmap: Cannot create a QPixmap when no GUI is being used\nQPixmap: Cannot create a QPixmap when no GUI is being used\nDCOP: register 'kbuildsycoca' -> number of clients is now 4\nkbuildsycoca running...\nDCOP: register 'anonymous-2999' -> number of clients is now 5\nkio (KSycoca): Trying to open ksycoca from /var/tmp/kdecache-cwang/ksycoca\nkbuildsycoca: checking file timestamps\nkbuildsycoca: timestamps check ok\nDCOP: unregister 'anonymous-2999'\n\n"
    author: "kde user"
  - subject: "Re: fail to start kde after konstruct build"
    date: 2004-02-10
    body: "Hi,\n\nTry to use KDEDIR instead of KDEDIRS."
    author: "Daniel Alomar i Claramonte"
  - subject: "Re: fail to start kde after konstruct build"
    date: 2004-02-10
    body: "same problem. Also konstruct's documentation\nspecifically says KDEDIRS, not KDEDIR"
    author: "no name"
  - subject: "Re: fail to start kde after konstruct build"
    date: 2004-02-11
    body: "I had this problem and solve it using KDEDIR instead of KDEDIRS. Sure. Try it."
    author: "Daniel Alomar i Claramonte"
  - subject: "Something wrong with qt designer...."
    date: 2004-02-10
    body: "Hi,\n\nAnybody could lunch the \"designer\" from qt? Everytime I try ot fails qith a \"segmention fault\". Any idea????\n\nRegards"
    author: "Daniel Alomar i Claramonte"
  - subject: "Re: Something wrong with qt designer...."
    date: 2004-02-10
    body: "The idea is to use bugs.kde.org and help to add to the report http://bugs.kde.org/show_bug.cgi?id=73843"
    author: "Anonymous"
  - subject: "old versions of konstruct"
    date: 2004-02-11
    body: "Hi, where I can I get old versions of konstruct\nthat builds KDE 3.1.4? I don't have much\nluck building KDE 3.2 on a RedHat 7.3 machine\nbut KDE 3.1.4 worked well for me before\nI blew it away."
    author: "no name"
  - subject: "Re: old versions of konstruct"
    date: 2004-02-11
    body: "Lookup in the README how to get Konstruct via CVS and replace KDE_3_2_BRANCH with KDE_3_1_BRANCH."
    author: "Anonymous"
  - subject: "Applets Missing?"
    date: 2004-02-13
    body: "Hmmm.. another thing I've noticed since installing KDE 3.2 - kgpg doesn't work in that the applet is missing, and I can't run it from the command line.. no errors.. just nothing shows up.\n\nAlso, Licq no longer has an notify icon in the panel anymore.  If I close off the main window of licq, it still runs... but my little licq thingie is no longer around like it used to be.\n\nAnyone know why this would be?  Installed KDE 3.2 using Fedora rpm's.\n\nLaura"
    author: "Laura"
  - subject: "Re: Applets Missing?"
    date: 2004-02-13
    body: "Never mind.. I just realized the upgrade removed the system tray.  After adding it back again, everything is fine.  Doh!"
    author: "Laura"
  - subject: "Don't try to compile KDE 3.2.3 using gcc 3.4.1"
    date: 2004-08-18
    body: "I built gcc 3.4.1 on my linux development box and tried to build KDE 3.2.3 with it. Doh! There is a bug in that version of gcc which prevents correct compilation of classes which inherit from classes where templates are involved.\n\nI built gcc 3.3.4 and tried again - presto! the compile went through OK.\n\nSure hope this saves someone the days it has taken me to track this down, after looking in all the wrong places first....sigh!\n\nThe gcc 3.4.1 bug showed up when trying to compile libksvgdom_la.all_cc.cc:\n***************************************************************************\nIn file included from ../../ksvg/impl/SVGEcma.h:29,\n                 from ../../ksvg/impl/SVGElementImpl.h:32,\n                 from ../../ksvg/impl/SVGShapeImpl.h:24,\n                 from SVGLength.cc:23,\n                 from libksvgdom_la.all_cc.cc:2:\n../../ksvg/ecma/ksvg_lookup.h: In function `bool KSVG::lookupPut(KJS::ExecState*, const KJS::Identifier&, const KJS::Value&, int, const KJS::HashTable*, ThisImp*)':\n../../ksvg/ecma/ksvg_lookup.h:214: error: invalid use of undefined type `struct KSVGScriptInterpreter'\n../../ksvg/ecma/ksvg_lookup.h:28: error: forward declaration of `struct KSVGScriptInterpreter'\nIn file included from ../../ksvg/impl/SVGList.h:24,\n                 from ../../ksvg/impl/SVGLengthListImpl.h:26,\n                 from ../../ksvg/impl/SVGAnimatedLengthListImpl.h:24,\n                 from SVGAnimatedLengthList.cc:22,\n                 from libksvgdom_la.all_cc.cc:9:\n../../ksvg/ecma/ksvg_bridge.h: In member function `virtual void KSVGRWBridge<T>::put(KJS::ExecState*, const KJS::Identifier&, const KJS::Value&, int)':\n../../ksvg/ecma/ksvg_bridge.h:93: error: `m_impl' undeclared (first use this function)\n../../ksvg/ecma/ksvg_bridge.h:93: error: (Each undeclared identifier is reported only once for each function it appears in.)\nIn file included from ../../ksvg/impl/SVGList.h:26,\n                 from ../../ksvg/impl/SVGLengthListImpl.h:26,\n                 from ../../ksvg/impl/SVGAnimatedLengthListImpl.h:24,\n                 from SVGAnimatedLengthList.cc:22,\n                 from libksvgdom_la.all_cc.cc:9:\n../../ksvg/impl/ksvg_scriptinterpreter.h: In function `KJS::Object cacheGlobalBridge(KJS::ExecState*, const KJS::Identifier&)':\n../../ksvg/impl/ksvg_scriptinterpreter.h:103: error: `Internal' undeclared (first use this function)\nIn file included from ../../ksvg/impl/SVGLengthListImpl.h:26,\n                 from ../../ksvg/impl/SVGAnimatedLengthListImpl.h:24,\n                 from SVGAnimatedLengthList.cc:22,\n                 from libksvgdom_la.all_cc.cc:9:\n../../ksvg/impl/SVGList.h: In copy constructor `KSVG::SVGList<T>::SVGList(const KSVG::SVGList<T>&)':\n../../ksvg/impl/SVGList.h:51: error: `other' undeclared (first use this function)\nIn file included from ../../ksvg/impl/SVGColorProfileElementImpl.h:29,\n                 from SVGColorProfileElement.cc:22,\n                 from libksvgdom_la.all_cc.cc:73:\n***************************************************************************"
    author: "Doug Hutcheson"
  - subject: "Re: Don't try to compile KDE 3.2.3 using gcc 3.4.1"
    date: 2004-08-18
    body: "Did you try with gcc 3.4.1 and without --enable-final?"
    author: "Anonymous"
---
Today the <a href="http://www.kde.org/announcements/announce-3.2.php">KDE Project released KDE 3.2</a>. It is the result of a combined year-long effort by hundreds of individuals and corporations from around the globe. For packages, please visit the <a href="http://www.kde.org/info/3.2.php">KDE 3.2 info page</a>. At the moment binary packages for Conectiva Linux, Slackware (9.0, 9.1) and SUSE Linux (8.2, 9.0) are listed. Or compile the source either manually or with the help of <a href=" http://developer.kde.org/build/konstruct/">Konstruct</a> after reading the <a href="http://www.kde.org/info/requirements/3.2.php">KDE 3.2 Requirements list</a>. All changes can be found in the <a href="http://www.kde.org/announcements/changelogs/changelog3_1_5to3_2.php">detailed KDE 3.2 change log</a> or continue to read about the highlights.

<!--break-->
<p>
Some of the highlights in KDE 3.2 at a glance:
<ul>
  <li>Increased performance and standards compliance</li>
      <ul>
      <li>Lowered start up times for applications and hundreds of optimizations
        make KDE 3.2 the fastest KDE ever!</li>
      <li>Working in concert with Apple Computer Inc.'s Safari web browser team,
        KDE's web support has seen huge performance boosts as well as increased
        compliance with widely accepted web standards</li>
      <li>Increased support for FreeDesktop.org standards in KDE 3.2 strengthens
        interoperability with other Linux and UNIX software.</li>
      </ul>
  <li> New applications</li>
<ul>
      <li> JuK: a jukebox-style music player</li>
      <li> Kopete: an instant messenger with support for AOL Instant Messenger,
        MSN, Yahoo Messenger, ICQ, Gadu-Gadu, Jabber, IRC, SMS and WinPopup</li>
      <li> KWallet: providing integrated, secure storage of passwords and web form
        data</li>
      <li> Kontact: a unified interface that draws KDE's email, calendaring,
        address book, notes and other PIM features together into a familiar
        configuration</li>
      <li> KGpg: providing an easy-to-use KDE interface to industry-standard
        encryption tools</li>
      <li> KIG: an interactive geometry program</li>
      <li> KSVG: a viewer for SVG files</li>
      <li> KMag, KMouseTool and KMouth: accessibility tools for KDE</li>
      <li> KGoldRunner: a new riff on a classic game</li>
      <li> ... and many more!</li>
      </ul>
  <li> Thousands of incremental improvements and bug fixes</li>
<ul>
      <li> During the development of KDE 3.2 nearly 10,000 bug reports were
        processed via the KDE Bug Tracking System</li>
      <li> Approximately 2,000 feature requests were also processed, with hundreds
        of requested features added to KDE applications and components</li>
      <li> An improved configuration system that opens the door to new
        installation management possibilities, improved roaming support and
        many improvements to the "KDE Kiosk" environment management system</li>
      <li> Inline spell checking for web forms and emails</li>
      <li> Improved email and calendaring support</li>
      <li> Powerful tabbed interface for the Konqueror file manager and web
        browser</li>
      <li> Support for Microsoft Windows desktop sharing protocol (RDP)</li>
</ul>

  <li> Improved Usability</li>
<ul>    
      <li> Reduced clutter in many menus and toolbars</li>
      <li> Many applications, dialogs and control panels reworked for clarity and
        utility</li>
</ul>
  <li> Enhanced appearance</li>
<ul>     
      <li> Plastik, a tastefully understated new look, debuts in KDE 3.2</li>
      <li> Hundreds of new icons improve the consistency and beauty of KDE</li>
      <li> Tweaks to the default look including new splash screens, (optionally)
        animated progress bars, styled panels and more!</li>
</ul>
  <li> New Tools for Software Developers</li>
<ul>     
      <li> Comprehensive API documentation extended for 3.2</li>
      <li> Language bindings for ECMAScript (aka Javascript), Python, Java and
        Ruby</li>
      <li> New versions of the powerful KDevelop IDE and Quanta web development
        suite</li>
      <li> Umbrello brings UML modeling for 11 different languages including C++,
        Java, SQL, PHP, Python and Perl to KDE</li>
  </ul>
</ul>

 More information and why you should use KDE 3.2 can be found in the <a href="http://www.kde.org/announcements/announce-3.2.php">KDE 3.2 announcement</a>.

</p>







