---
title: "People Behind KDE: Back With Matthias Ettrich"
date:    2004-02-22
authors:
  - "wbastian"
slug:    people-behind-kde-back-matthias-ettrich
comments:
  - subject: "Welcome Back"
    date: 2004-02-22
    body: "Is Konqui now going to repeat his interview with Tink too? :-)"
    author: "Anonymous"
  - subject: "Mmmm..."
    date: 2004-02-22
    body: "Nice web site, and I like the graphics!"
    author: "David"
  - subject: "Re: Mmmm..."
    date: 2004-02-22
    body: "Yes the web site is clean and professional looking. The only grip i have is he kept calling Linux gnu/linux. Its a minor annoyance. You could easily say kde/linux but luckly KDE is not so arrogant as to want its name in front of Linux. Xfree86/Linux could also be a new name. With out the X server there would be no desktop. How about we just call it Linux its clean and marketable and straight forward."
    author: "Kraig"
  - subject: "Re: Mmmm..."
    date: 2004-02-22
    body: "I like the \"GUN/Linux\" typo, perhaps such one was forcing him to write \"GNU/Linux\"? :-)"
    author: "Anonymous"
  - subject: "Re: Mmmm..."
    date: 2004-02-23
    body: "No, GUN/Linux is Eric S Raymond's distribution."
    author: "AC"
  - subject: "Re: Mmmm..."
    date: 2004-02-23
    body: "GUN/Linux. That's great!"
    author: "David"
  - subject: "Gun/Linux"
    date: 2004-02-24
    body: "LOL"
    author: "Thomas"
  - subject: "Re: Mmmm..."
    date: 2004-02-22
    body: "GNU/Linux isn't just a desktop operating system.\nNot all Linux boxen have X11, let alone XFree86 or KDE.\nI'll put money down that there are *no* Linux installations without any GNU tools."
    author: "Z"
  - subject: "Re: Mmmm..."
    date: 2004-02-22
    body: "There are literally millions of them.\n\nFor example, any embedded Linux installation is going to have _at most_ glibc, and probably will use ulibc instead. It surely won't have any GNU utils, because they are too big, using Busybox instead."
    author: "Roberto Alsina"
  - subject: "Re: Mmmm..."
    date: 2004-02-23
    body: "Being that KDE is the topic i think its reasonable to assume that we are talking about a Linux desktop system or sytems. I'm not sure that it would make a huge difference though. If we called a non-linux desktop system GNU/Linux by the same reasoning should we not refer to a Linux desktop system as KDE/Xfree86/GNU/Mozilla/OpenOffice/Linux? To include one important companent of the complete system and excluding the others is just unjustifiable. GNU programs are a important part of the desktop linux system but so are all the other critical components. I think the GNU title thing has less to do about code however and more to do with a certain someones ego. "
    author: "Kraig"
  - subject: "Re: Mmmm..."
    date: 2004-02-23
    body: "you mean the person who created gcc, gdb, emacs, lex/yacc, and let's not forget the gpl license, which is not in the least part why linux is such a success story instead of, say bsd? as for your other 'critical components', aren't these usually compiled using gcc, debugged with gdb, edited in large part with emacs and don't they often carry the (l)gpl license? just think about it.\n"
    author: "mark dufour"
  - subject: "Re: Mmmm..."
    date: 2004-02-23
    body: "How do you know the developer used gdb for debugging and edited with emacs? I think Kdevelop has become a major developer tool maybe we should change the name of Linux to kdevelop/KDE So what even if they did GNU has contributed some essential programs i'm not saying anything different. KDE is equally essential and no not all programs are gpl take Xfree86 an essential part of my desktop. Pointing out that GNU has contributrd some good programs and code does not elivate it to the point that it justifys a name change for Linux. "
    author: "Kraig"
  - subject: "Re: Mmmm..."
    date: 2004-02-23
    body: "Don't feed the troll.\n\nThis guys is a known troll. He has been laughed out of the Mandrake community. Everywhere he goes he is spiteful.\n\nIf you do not appreciate that the GNU is at the very core of free software, don't use free software.\n\nGet a Mac, go back to Windows or your latest hobby, spouting about Lindows and polluting every other forum with your claims about it. Not because there is anything wrong with Lindows, just that you talk about it even when it's out of topic.\n\nSo either stop hating or leave. No one will miss you. Bye....Bye.."
    author: "Just Passing By"
  - subject: "Re: Mmmm..."
    date: 2004-02-23
    body: "Ok your being the troll and i've been using open source and linux for years and no i won't go use somthing else because i refuse to refer to linux as GNU/Linux just to please a guy with a ego problem. Just because it hurts his feelings everytime someone refers to Linus as the guy that started it all does not mean we change the name. Thank you for writing the gpl now get over yourself. I stopped using the mandrake system becuase i wanted a pure KDE deskop with out gnomes and the gtk config. Thanks my preference."
    author: "Kraig"
  - subject: "Re: Mmmm..."
    date: 2004-02-23
    body: "Yer well you calol it GNU/Linux then. Don't bitch when others call it Linux. That doesn't mean that people do not respect the GNU tools, or X, or anything else. It is just impossible to put everything in the god damn title."
    author: "David"
  - subject: "Re: Mmmm..."
    date: 2004-02-22
    body: "Hello,\n\nso you are nobody and know better how to call GNU/Linux that one of the leading KDE/QT developers?\n\nNext time when you compile KDE, if you can, think about why one this mysterious messages calls something \"gcc\", only for a starter. Could it be that without GNU you could not create Linux or KDE?\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: Mmmm..."
    date: 2004-02-22
    body: "So what? There are hundreds of libs and a very large amount is NOT gnu. GNU is a part of Linux but is not Linux. KDE, xfree86, mozilla and open office are just as critical to my desktop Linux box as the GNU components. Lets drop all the GNU silliness and just call Linux Linux. The general public has heard of Linux but knows nothing of GNU. Adding GNU in front of Linux is just to appease a small vocal part of the community is an insult to all the Open source nonGNU developers. I'm thankful for ALL parts of the community for one part to be so selfish and arrogant to demand that Linux change its name to recognise its contributions is just ridiculous. Thank you GNu but also thank you KDE, Xfree86, Mozilla, Open Office and all the other very important Open Source projects."
    author: "Kraig"
  - subject: "Re: Mmmm..."
    date: 2004-02-23
    body: "Hi,\n\nfirst reason is technical. How to you call Debian, a Linux? Even if it has all in it, except the Linux kernel? Debian GNU/KFreeBSD e.g. is absolutely free of Linux, but has the same software available.\n\nsecond is that GNU is very old and has an altruistic track record that we can be proud of. It has a philosophy behind it and enabled much software.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: Mmmm..."
    date: 2004-02-23
    body: "Then its freeBSd whats your point? We are talking about Linux and the age of GNU is important and so is there track record and so is all the other projects that have contributed to the open source community but they are no more important than KDE or Xfree86 for desktop Linux. When it comes down to it its about a certain persons ego and thats just unfortuate."
    author: "Kraig"
  - subject: "Re: Mmmm..."
    date: 2004-02-23
    body: "Hi,\n\nno, it's not FreeBSD, because that is a (very) different system and not a GNU system at all. GNU/KFreeBSD refers to GNU systems with the FreeBSD kernel. It is Debian building such systems.\n\nIt behaves like what you call \"Linux\" and what others call \"GNU/Linux.\" Why? Because the GNU part is common. \n\nTrue, FreeBSD is not a GNU. But Linux neither need not be GNU at all. Look at e.g. the many systems now based on the Linux kernel, with a completely different userland system than GNU.\n\nYou are technically incorrect to call something \"Linux\" just because it is GNU or just because it contains the Linux kernel.\n\nIs that so hard to understand? This is nothing about ego of GNU, it is about identifying what you are talking about.\n\nThere are KDE systems which are based on Win32/GNU (cygwin), FreeBSD, Solaris, etc., many platforms, some of which are GNU, some of which are not. But if you think there would be a KDE without GNU, then better use CDE.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: Mmmm..."
    date: 2004-02-23
    body: "The systems are not GNU they just have GNU componetnts in them. If a system has KDE installed does it a KDE/Linux system? Your logic makes not sense. You seem to value the GNU components over the KDE components. Is that the case?"
    author: "Kraig"
  - subject: "Re: Mmmm..."
    date: 2004-02-23
    body: "If he wants to call it GNU/Linux, who the hell are you telling him that he shouldn't do it? I for one call Linux just \"Linux\", but if someone calls it GNU/Linux, I don't get my panties in a bunch. If they start telling me to call it GNU/Linux, I might get annoyed. But then again, that is no way different if someone ordered others to call Linux some certain way (like you are doing just now)"
    author: "janne"
  - subject: "Re: Mmmm..."
    date: 2004-02-24
    body: "\"(like you are doing just now)\" \n<p>\nWell actually i didn't order anyone to call it anything. I just mentioned that it was annoying and why. Call it what you like I just don't like the Policical Correctness of the whole thing. Why should the GNU community be placed over the rest of the open source community including the vast KDE community? I have seen no logical reason why. Lame arguements like GNU has essential tools just don't cut it. As if KDE and X are not essential to my Desktop system. "
    author: "Kraig"
  - subject: "Re: Mmmm..."
    date: 2004-02-23
    body: "How the hell did this turn from my comments on the nice website to a flamefest on GNU/Linux??!!!"
    author: "David"
  - subject: "Re: Mmmm..."
    date: 2004-02-23
    body: "welcome to the web ...... crazy, ain't it? =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Mmmm..."
    date: 2004-02-25
    body: "indeed...\n\nbut I do wonder what desktop he is using now (kraig, I mean) - which is fully KDE, no gnome/gtk 'shit'...\n\nme is using mdk too, but I dont like all the mdk-only tools they have (like their menu editor, which blew my menu's a few times) - and indeed, they're gtk, which is silly for an KDE based distro."
    author: "superstoned"
  - subject: "Re: Mmmm..."
    date: 2004-02-25
    body: "I'm using Lindows because i like KDE and they support KDE without gnome taking up space. They still have good gtk apps like gimp but not gnome."
    author: "Kraig"
  - subject: "Nice interview ;)"
    date: 2004-02-22
    body: "I also hope Qt 4 will be great and maybe provide better integration with KDE."
    author: "Alex"
  - subject: "long horns"
    date: 2004-02-22
    body: "I liked this pun :-) :\n\n\"Combined with the new freedesktop X-server we will be very well equipped to take any bull by its horns, no matter how long those might be.\"\n"
    author: "quarus"
  - subject: "My favourite:"
    date: 2004-02-22
    body: "> - Which T.V. show world would you fit right into?\n> World Idol. I can't sing, I can't dance and I'm German. \n> I'm sure the judges would have enjoyed making nasty comments about me. \n\n*LOL*"
    author: "cm"
  - subject: "QT4 details?"
    date: 2004-02-22
    body: "Is there any info on what the major changes will\nbe for QT4, and what quarter we might expect it?\n\nIs there any chance it will provide a C++ language\nbased alternative to SIGNAL/SLOT?\n\n"
    author: "steve"
  - subject: "Re: QT4 details?"
    date: 2004-02-22
    body: "> Is there any info on what the major changes will be for QT4\n\nhttp://dot.kde.org/1061880936/\n\n> and what quarter we might expect it?\n\nWhen it's ready.\n\n> Is there any chance it will provide a C++ language based alternative to SIGNAL/SLOT?\n\nNo."
    author: "Anonymous"
  - subject: "Re: QT4 details?"
    date: 2004-02-23
    body: "> Is there any chance it will provide a C++ language based alternative to SIGNAL/SLOT?\n\nSignal/Slots is C++ language based no other language requried.\n\nCheers,\nKevin\n\n"
    author: "Kevin Krammer"
  - subject: "Grabbing the bull by the horns"
    date: 2004-02-22
    body: "I don't know if that's a reference to Longhorn, but it would be entertaining if it was :) "
    author: "Rayiner Hashem"
  - subject: "Re: Grabbing the bull by the horns"
    date: 2004-02-23
    body: "\"Longhorn\" is a phallus symbol. \n\nLike in win XP x stands for sex. That's often used in Marketing and some Merkating ghuys believe inhtis technique.\n\n"
    author: "Gerd"
  - subject: "XP -> Sex-Pee ? What? You're sure?"
    date: 2004-02-24
    body: "If you're right, MS marketing guys are strange and sad people. What makes the situation worse is their obvious success...\n"
    author: "Thomas"
  - subject: "Re: Grabbing the bull by the horns"
    date: 2004-02-23
    body: "http://www.microsoft-watch.com/article2/0,4248,1332766,00.asp\n\n \t\nSaturday, October 11, 2003\nIt's Official: No Longhorn Until 2006\nBy Mary Jo Foley\nMicrosoft execs at last have admitted publicly, at last, that Longhorn will not ship in 2005. Does it matter?\n\n--->> So I guess Longhorn will have to compete with KDE 4!"
    author: "Gerd"
  - subject: "Since Julian Seward was mentioned..."
    date: 2004-02-22
    body: "I think it might be a good idea to give some additional information to the possibly clueless readers. ;)\n\nJulian Seward is the primary author of Valgrind (valgrind.kde.org) for which he received the Open Source Award in January and did an insightful interview at http://builder.com.com/5100-6375-5136747.html"
    author: "Datschge"
  - subject: "the dragon has to go!"
    date: 2004-02-22
    body: "Konqui the dragon once was a nice idea, but thats long ago. Today, it really breaks KDEs nicely polished UI whenever it shows up (for me, fortunately, only with the logout-screen). When you look at KDE-Desktops (i.e. at kde-look), most of them have blue colors. Just like the KDE website. Or the dot. But this helplessly-looking, disoriented dragon is red-green.\n\nWhats the connection between KDE and Konqui? Gnome has its foot, which looks ok and KDE has the gears. They are great and symbolize precision and speed.\n\nI have a question to anyone who has yet been at LinuxTag, LinuxExpo etc: Have people asked you about the dragon? What did you tell them? Where is Konqui from?\n\nI know, this discussion comes up once in a while. But I won't rest before Konqui is moved to Attic/ :)"
    author: "me"
  - subject: "Re: the dragon has to go!"
    date: 2004-02-22
    body: "Konqui is KDE's mascot. I guess you don't have any love for mascots anymore and would like to see the ones by Linux, FreeBSD etc. go as well?"
    author: "Datschge"
  - subject: "Re: the dragon has to go!"
    date: 2004-02-23
    body: "Wasn't konqui only introduced with KDE 2.0? I think it's fine, but perhaps the more long standing gears can replace it in things like the logout dialog and drkonqui (:)) "
    author: "anon"
  - subject: "Re: the dragon has to go!"
    date: 2004-02-23
    body: "You know MS still has the flying window, and Apple has....\nThese are not just Mascot, but identify the item. I suspect that in time, konqi will be similar to Tux and the daemon."
    author: "a.c."
  - subject: "Re: the dragon has to go!"
    date: 2004-02-23
    body: "I do so love that dragon, though I also agree with the above mentions that it looks out of place... And as to the argument that it's what defines KDE as a corporate id thing, I think you'll find the gears to that ;)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: the dragon has to go!"
    date: 2004-02-23
    body: "I think the dragon is cool."
    author: "Robert"
  - subject: "Re: the dragon has to go!"
    date: 2004-02-23
    body: "I would also like to express my fondness for the KDE mascot.\n\nThank you."
    author: "LMCBoy"
  - subject: "Re: the dragon has to go!"
    date: 2004-02-24
    body: "me too. I don't give a ... if it looks not corporate enough or whatever. Still think, it could be solved by adding a \"corporate-looking replacement\" for konqui to the kpersonalizer wizard.\n\nDo you want \no a nice dragon-supported logout dialog, or\no ditch konqui in favor of stupid corporate-desktop-looking-graphics in your\t    logout-dialog\n\nwell, it's not impossible"
    author: "Thomas"
  - subject: "Re: the dragon has to go!"
    date: 2004-02-24
    body: "No need for that, really.  It's easy enough to change the logout screen, so the corporate sysadmin can and should do so.\n"
    author: "LMCBoy"
  - subject: "Easy?"
    date: 2005-02-21
    body: "So how do I do it? I can't find it in KDE Control Centre anywhere.\n\nI've got an aversion for too much branding on my desktop. I love SuSe as a distro, but the 'SuSe K-menu icon' is the first thing that has to go, as well as the window decoration (I don't want to have a lizard show up in every window I open!).\n\nI agree, add something to Kpersonalizer or better yet, the K Control Center. And while I don't hold a personal grudge against the dragon, I would like to replace it as well. It kind of spoils my otherwise exactly-to-my-liking KDE desktop."
    author: "Darkelve"
  - subject: "Re: the dragon has to go!"
    date: 2004-02-23
    body: "It might sound stupid, but I've never really understood the idea of having gears as the KDE Icon!  I'm guessing it means that everything works together. \nJust a side thought......ever wondered how a stuffed toy that looks like a cog would sell as a KDE mascot?  Long Live Konqui :)"
    author: "Judd Baileys"
  - subject: "Re: the dragon has to go!"
    date: 2004-02-23
    body: "Ich like the dragon. I would like to see more Konqui promotion, not less. It is a wonderful representative of KDE. Let's use him quite more often."
    author: "Gerd"
  - subject: "Re: the dragon has to go!"
    date: 2004-02-26
    body: "I can see it now.\n\nI'm running KDE 4.0 for the first time.  I click on the Konqueror icon.  Suddenly, a dancing Konqi appears and says, \"It looks like you're trying to access the Internet!  Would you like help with your Web Browser settings?\"\n\n....AAAAAIIIEEE!!!!!\n"
    author: "Tukla Ratte"
  - subject: "Re: the dragon has to go!"
    date: 2004-02-23
    body: "You're missing out on a lot of fun, According to my husband who took Konqi with him on board when he flew from Las Vegas to SF, Konqi is a giant CHICk magnet. ;-0 He never had so much female attention and offers as that night. ;-) So maybe you should try one and improve your social life. ;-)\n\nKonqi will never move to the attic, on the contrary I have a much more prominent role for Konqi in mind that he had up till now. \n--\nTink"
    author: "Tink"
  - subject: "Re: the dragon has to go!"
    date: 2004-02-23
    body: ">Konqi will never move to the attic, on the contrary I have a much more >prominent role for Konqi in mind that he had up till now. \n\nThe Konqi Dating Agency, managed by Madame Tink. Family oriented, good-looking Russian ladies, fluent in C++ are looking for sober lonely Dutch hackers and a way to escape frozen Siberia. Stuffed animals are our fantasy. A keen interest in Open Source is mandatory.\n\n;-)"
    author: "Charles de Miramon"
  - subject: "Re: the dragon has to go!"
    date: 2004-02-23
    body: "You are absolutely right Tink. I've had one of those 25cm Konqi's at FOSDEM this year and yes the few females who were at FOSDEM liked the little fellow :)\n\nCiao \n\n\nFab"
    author: "Fabrice Mous"
  - subject: "Re: the dragon has to go!"
    date: 2004-07-13
    body: "There was females at FOSDEM this year!?!"
    author: "Martin Galpin"
  - subject: "Re: the dragon has to go!"
    date: 2004-02-23
    body: "I don't actually have a plush Konqui, but when females have seen him on screen he is an absolute babe magnet. I've got to get one!"
    author: "David"
  - subject: "Re: the dragon has to go!"
    date: 2004-02-23
    body: "Hi,\nKonqui is just sitting close to the computer screen and likes to mention that \nhe never will move to attic, instead will light to flames every such suggestion he\nis part of the KDE commmunity ;-)\n\n"
    author: "Werner"
  - subject: "Re: the dragon has to go!"
    date: 2004-02-24
    body: "You might have more respect for the dragon after taking a look at this:\n\n(not my picture)\nhttp://www.kde-look.org/content/show.php?content=367\n\nIsn't that *so* much cooler than a foot?\n"
    author: "Rayiner Hashem"
  - subject: "Re: the dragon has to go!"
    date: 2004-02-27
    body: "Right, it's a well known fact that nothing screams \"give me respect\" more than an image of fondling boobs; oh, wait...\n\nPlease don't link to pornography from the dot."
    author: "Scott Wheeler"
  - subject: "Re: the dragon has to go!"
    date: 2004-02-24
    body: "I like the dragon. However, it is a few years old by now. Who created it? With what program? Where are the source files that were used? I guess it was created with a proprietary program in its day? Can it be ported and revived with free software today? \n\n\n\n\n"
    author: "claes"
  - subject: "Re: the dragon has to go!"
    date: 2004-02-24
    body: "Konqui, at least the 3D model, was created 1999 by Stefan Spatz. The sources for 3D-Studio and Autocad are available in ftp://ftp.kde.org/pub/kde/devel/konqi_sdk.tar.bz2. If anyone could convert it to Povray or Blender or just produce new pictures of Konqui, that would be fantastic."
    author: "Anonymous"
  - subject: "Re: the dragon has to go!"
    date: 2004-07-13
    body: "The dragon should be optional... it really takes away from the professional atmosphere which is conveyed by the rest of the KDE look.  People ask me when tey use my computer...\"What the heck is that dragon for?\"\n\nBut hey, some people like it."
    author: "Brent Roberts"
  - subject: "Re: the dragon has to go!"
    date: 2004-09-13
    body: "I think the dragon is a little bit out of place.  It takes away from the professional look that KDE has acquired over the years.  At least an option to change it in the KDE control center would be nice."
    author: "PSWired"
  - subject: "Re: the dragon has to go!"
    date: 2004-12-05
    body: " Why does \"corporate\" and \"professional\" mean that everything must be bland and boring? I would be willing to bet that most users of KDE (and of Linux in general) consist of geeks who are playing with their own computers while at home, not corporate lamers. ;o)\n\n If being \"professional\" means that everyone has to exhibit a complete lack of personality, then why do big corporations such as Disney, Pixar, etc. all have their mascots (and other characters) at their work stations? (Look at most any DVD produced by those companies, in the special features sections. You'll see what I'm talking about.)\n\n I came upon this board while looking for a reason why that cute little dragon had flown away from the KDE logout screen and it amazes me how many people have a lack of personality and sense of humor.I hate to tell those corporate-types out there this, but it is likely that your Linux distro was created by hundreds of geeks who were sitting in front of their computers, at home, playing with code while wearing old T-shirts, drinking soda (or beer), munching on Doritos and listening to grunge music... and they might even have had a few stuffed animals sitting on top of their monitors. I would love to have a plush Konqui sitting atop my monitor, looking over what I do! :)\n\n  (No offense was intended towards boring people anywhere. ;o)"
    author: "Trinity is My Name"
  - subject: "Re: the dragon has to go!"
    date: 2005-01-26
    body: "If linux is for geeks, they would *probalbly* want to change everyting, including the logout screen...(as do I, being a geek)"
    author: "Nozem"
  - subject: "Re: the dragon has to go!"
    date: 2005-01-26
    body: "Whoops, here is is (in my case) :\n/usr/kde/3.3/share/apps/ksmserver/pics/shutdownkonq.png"
    author: "Nozem"
  - subject: "Re: the dragon has to go!"
    date: 2005-02-21
    body: "Besides, I thought Linux was about choice. Doesn't that include the choice NOT to use something??"
    author: "Darkelve"
  - subject: "Re: the dragon has to go!"
    date: 2007-02-06
    body: "I think image needs updating."
    author: "Ow"
  - subject: "Re: the dragon has to go!"
    date: 2007-07-29
    body: "What I don't like about the dragon is that it simply looks so... Well it is so angular and odd. It does not fit in KDE 3.x and I think it won't fit in KDE 4 even more.\n\nWouldn't it be better to redesign the poor old fellow? I mean look at those BSD devil guys. If I recall they have changed their mascot a bit, eh?\n\n(google.... click click) :)\n\nCheck out what they came up with: http://logo-contest.freebsd.org/result/\n\nand the old one was: http://logo-contest.freebsd.org/ (you can see it somewhere in the page).\n\nSo what I'm trying to say is that Konqui is a bit old fashioned and NEEDS polishing.\n\nRegards,\nBartek"
    author: "Bartek"
  - subject: "Re: the dragon has to go!"
    date: 2008-01-12
    body: "G'day,\n\nJust a quickie from me. Look, I'm new to this Linux stuff, I think it looks great and workable. It appears stable. I just use it for the net and some other stuff and I like the corporate feel to the Simply-Mepis that I am using. But for this fool of a dragon that shows up at the end.\n\nI JUST WANT TO P--- IT OFF!\n\nFor the love of bananas, cripes, I mean, come on! Fair dinkum!\n\nIt looks daggy and takes away from the rest of the system and it gets quite annoying to look at it.\n\nNow ...\n\nCan-someone-just-write-a-response-to-tell-how-to-get-rid-of-it!\n\nI'm no geek (whatever that is), I don't particularly want to be, I just want to use the thing. And we need a 'Linux for Complete Idiots' like myself.\n\nLooking forward to some help.\n\nBe good.\n\nBye for now."
    author: "Neil"
  - subject: "Re: the dragon has to go!"
    date: 2009-01-21
    body: "Well said Neil!\n\nYou are right, it looks daggy, and although I can understand there is some affection for it, I cannot myself see why this thread has been here for 4 years without it being solved. It is not a show stopper, but again, Konqui is an unwelcome intrusion for many. Come on guys, even if kde peeps think it should be there by default, it needs an option to get rid of it.\n\n"
    author: "MrClippy's evil twin."
  - subject: "Re: the dragon has to go!"
    date: 2009-01-21
    body: "Well said Neil!\n\nYou are right, it looks daggy, and although I can understand there is some affection for it, I cannot myself see why this thread has been here for 4 years without it being solved. It is not a show stopper, but again, Konqui is an unwelcome intrusion for many. Come on guys, even if kde peeps think it should be there by default, it needs an option to get rid of it.\n\n"
    author: "MrClippy's evil twin."
  - subject: "Poor Write-Up"
    date: 2004-02-23
    body: "The write-up on this post doesn't make very much sense.  The two interview links in this post are \"this first interview\" and then, later, \"previous interview.\"  So, which one is the new interview?\n\nMy first inclination is that it's the \"this first interview\" link because it's on the same host an the \"The People Behind KDE\" link, but that one is dated 2002-02-21 which seems old to me.  I open up the \"previous interview\" link (which, when taken in context, appears to be over three years old) and there is no date on the actual linked page.  So, we have an interview in 2002 and an interview in (2004 - 3 years) 2001.  KDE.NEWS -- on the bleeding edge of KDE news.  Heh.\n\nI understand that not everyone's first language is en-US, but aren't there editors on this site?"
    author: "Todd Ross"
  - subject: "Re: Poor Write-Up"
    date: 2004-02-23
    body: "Read the text around the link texts and you will understand."
    author: "Anonymous"
  - subject: "The Dragon Has to Say!"
    date: 2004-02-23
    body: "Seriously, I have seen people freek out at applications and when they have seena nice friendly dragon those fears evaporate. Perhaps Konqui can have a role in the help section of applications. Konqui is too much in the background of KDE."
    author: "David"
  - subject: "Re: The Dragon Has to Say!"
    date: 2004-02-23
    body: "...as well as the fact that even on screen he has - shall we say - magnetism :). I thought I could pull until Konqui showed me the way."
    author: "David"
  - subject: "great"
    date: 2004-02-24
    body: "I like the new interview better than the old ones! :) great job"
    author: "Sergio Garcia"
  - subject: "quantum algebra"
    date: 2004-04-21
    body: "real player ?"
    author: "id0012345679"
---
<a href="http://www.kde.nl/people/">The People Behind KDE</a> series is back from a long vacation thanks to overwhelming popular demand. In <a href="http://www.kde.nl/people/ettrich.html">this first interview</a>, KDE's ever so charming Tink gets back with Matthias Ettrich to see what has changed since the <a href="http://people.kde.org/matthias.html">previous interview</a> now more than three years ago.




<!--break-->
