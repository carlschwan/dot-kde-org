---
title: "KDE Performance Tips Updated"
date:    2004-10-19
authors:
  - "llunak"
slug:    kde-performance-tips-updated
comments:
  - subject: "wiki"
    date: 2004-10-19
    body: "its a wiki page ! so everyone can edit the page . try to add some content you can think of "
    author: "chris"
  - subject: "Re: wiki"
    date: 2004-10-19
    body: "Gee, why have I bothered to ask that content changes are first discussed on the kde-optimize list then? Both here and in the page itself?\n\nIt is a wiki page because ... er ... it happens to be a wiki page. It started as wiki page one day. But people should not just go and add what they _think_ helps KDE performance, because, quite frankly, some people have strange ideas about KDE performance. Even people who work on performance often think wrong, until they get to measuring. If you have a new idea, and can back it up with some proof, post it to the list, and you'll get okay for including it.\n\nIf the page turns into a bunch of I-think-this-might-perhaps-help's, I'll move all my tips to somewhere where only I will be able to do anything with it. There is quite some work behind them.\n"
    author: "Lubos Lunak"
  - subject: "Re: wiki"
    date: 2004-10-19
    body: "It seems to me that if you put up a Wiki page then you must expect people to spontaneously edit it. Because ... er ... that's what a wiki page happens to be!\n\n:-)"
    author: "Brandybuck"
  - subject: "Re: wiki"
    date: 2004-10-20
    body: "Of course, people are free to edit it. Assuming they add something good and useful. But since optimization is a difficult beast, nobody finds out new (and correct) things somehow spontaneously, they need to be examined and checked -> that's why it should be reviewed first. And BTW, it actually wasn't me who put up the page.\n"
    author: "Lubos Lunak"
  - subject: "Re: wiki"
    date: 2004-10-20
    body: "\"Even people who work on performance often think wrong, until they get to measuring.\"\n\nMaybe they are measuring wrong. They should measure the user's perception of speed, not the actual speed on the computer.\n\nWhy you ask?\n\nSimple. If the user thinks it's faster, it IS faster. If the user thinks it's slower, then the user will go away."
    author: "Erik Hensema"
  - subject: "Re: wiki"
    date: 2004-10-20
    body: "How exactly do you do that? Particularly in a controlled fashion?"
    author: "SadEagle"
  - subject: "Re: wiki"
    date: 2004-10-20
    body: "So instead of real performance improvements or measurable improvements in latency etc we should all get sugarpills?"
    author: "Morty"
  - subject: "Re: wiki"
    date: 2004-10-20
    body: "No, of course it's stell better to achieve an authentic improvement in execution speed.\nHowever, in places where this simply can't be done, be it because of lack of information about the problem, or because of bottlenecks in software on which KDE depends (gcc / ld, qt, etc), it is at least desirable to give a user an *illusion* that things are happening faster - so to say, build up a psychological solution for a technical problem.\n\nHe'll be completely happy with this, because he *thinks* it's going faster, and that's all he needs.\n\nSimple example: Imagine you right-click somewhere, which causes an application to open a menu.\nNow the program could:\n\n- load all items, load all icons, construct the menu, then show it.\n\nor \n\n- *show* the (empty) menu (border), an fill it up with items and icons which get loaded one by another.\n\nThis makes a user way more satisfied than having to wait for a second to get a menu after having clicked somewhere.\n\nOf course, the menu-drawing-and-lading-code has still to be optimized. The psychological solution should not replace technical solutions, after all."
    author: "Hoek"
  - subject: "Re: wiki"
    date: 2004-10-21
    body: "To take that example a little further, the method would appear the fastest would be to have a pretty way of drawing the menu which takes little power and seems intentional.\n\nThe first method you noted (plan it all out, then draw it) takes less time but has a high latency, which makes the user feel like the computer is non-responsive. However, having part of the menu appear, then another part, then another, leaves the user thinking the computer is having trouble actually drawing the menu, making them think it is either badly written software or that their system is overworked (which may be the case).\n\nBy contrast, the menu could be drawn by first causing the menu top to extend out horizontally from the cursor, giving a head to the menu, then, when the vertical height of the menu is determined, having it extend smoothly downwards, as if it were sliding down. And, finally, have the contents swiftly fade in.\n\nThe method I just described could all happen almost instantaneously if the menu was ready, but would cause minor slowdowns in all cases. However, it would appear to be intentional, so the user would be less likely to think the system was having trouble. Also, it'd be purty."
    author: "jameth"
  - subject: "prelink"
    date: 2004-10-19
    body: "What about running \"prelink -a\" on your system?  Does that help?"
    author: "Benjamin Meyer"
  - subject: "Re: prelink"
    date: 2004-10-19
    body: "It helps a lot on my system (400Mhz celeron with slackware)!\nFor example, kmail used to need more than 10 sec to \nstartup, with prelinking my whole system, this time\nis reduced to less than 5 sec. And I only took kmail\nas an example, all applications start faster.\nAlso don't forget to set KDE_IS_PRELINKED or something.\nThe command I use is: prelink -avmR\nAnd /etc/preling.conf looks like:\n-l /bin\n-l /usr\n-l /sbin\n-l /lib\n-l /opt\n\n"
    author: "pieter"
  - subject: "Re: prelink"
    date: 2006-10-14
    body: "how can I install prelink in my slackware11 desktop (pIII-800MHz 768MB RAM)\nI found it very useful for this machine\n\nthanks\nzorrito"
    author: "zorrito"
  - subject: "great work!"
    date: 2004-10-19
    body: "really cool to see these tips so up-to-date, kde 3.4 is even mentioned :D\n\nand this document is really usefull..."
    author: "superstoned"
  - subject: "performance, eh?"
    date: 2004-10-21
    body: "looks like the webmasters need some performance tips.\n\nContext:\nFile\t/tiki-index.php\nUrl\t/tiki-index.php?page=Performance%20Tips\nQuery:\nselect `pageName`,`lastModif`,`user` from `tiki_pages` order by `lastModif` desc\nValues:\nMessage:\nError Message\tGot error 127 from table handler"
    author: "joe_bruin"
  - subject: "Re: performance, eh?"
    date: 2004-10-21
    body: "There are a few nasty memory leaks on the various sites that run on this server.  That seems to have caused MySQL to corrupt one of its tables.  I tried to repair the table in MySQL...  the frontpage of the wiki now seems to be screwed. :("
    author: "Navindra Umanee"
  - subject: "kernel precaching?"
    date: 2004-10-21
    body: "Quote from the wiki: \"A mechanism for the Linux kernel is currently being developed that will precache disk contents needed during startup.\"\n\nThat one sounds really interesting. even more interesting than the kdm-preload feature, which I also didn't know about before :)\nAny pointers where I can find out more about that? Is such a mechanism really going to be included into the kernel? I heard about several attempts for precaching, but obvisously none of them got very far.\n\n"
    author: "uddw"
  - subject: "Re: kernel precaching?"
    date: 2004-10-21
    body: "You probably can't find out more about that these days. It is developed by one of the SUSE kernel developers, it's not ready yet, and I can't give you any estimate when it will be (and I also can't guarantee it won't be just like the other attempts). Optimizations usually aren't of the highest priority :-/ .\n"
    author: "Lubos Lunak"
  - subject: "sorry, offtopic: is it me or KDE?"
    date: 2004-10-23
    body: "sorry, it's offtopic, but didn't find anything at google:\n\n- is there any option in KMail to display smiley graphics (like in th-bird/opera-m2) instead of display ':-)'? (should be in text mails too)\n\n- is there any option in KMail to have only in KMail Firefox the default program for http/-s and ftp links? it's not nice to open konqueror and after typing in 'www.google.com' konqui opens Firefox ;-) (there are moments when I want to surf with konqueror)\n\n- is there any option in KDE to have the desktop icons placed at that place where there are before I logout/rebooting? I tried different options in context menu (snap to grid?) but doesn't help. Or if I move one 'non-kde' Icon (eg firefox icon) all icons on my desktop moves some pixels down (if I have snap to grid turned on)\n\nThanks for you help!!!!"
    author: "anonymous"
---
Many aspects of KDE performance depend on the underlying system or the user's configuration. <a href="http://wiki.kde.org/tiki-index.php?page=Performance%20Tips">The KDE Performance Tips</a> document, which lists some of the performance related issues together with instructions how to avoid or fix the problems, has been updated with new tips. If you would like to add new tips to this page, update the ones already listed, or discuss them, please use the <a href="https://mail.kde.org/mailman/listinfo/kde-optimize">kde-optimize@kde.org mailing list</a>. Note that this list is for working on optimizing KDE -- not for complaining.




<!--break-->
