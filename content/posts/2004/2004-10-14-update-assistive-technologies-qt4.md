---
title: "Update on Assistive Technologies for Qt4"
date:    2004-10-14
authors:
  - "hfernengel"
slug:    update-assistive-technologies-qt4
comments:
  - subject: "I love transparency :)"
    date: 2004-10-14
    body: "> that transparently interact with Qt/KDE applications as well as\n> GTK/GNOME applications.\n\nNow someone must write GTK/GNOME applications that transparently interact with other GTK/GNOME applications :)\n\n/me hides"
    author: "KTROLL"
  - subject: "Re: I love transparency :)"
    date: 2004-10-14
    body: "lol, good one!\n\nhave a nice day everyone!"
    author: "me"
  - subject: "KDM Accessibility"
    date: 2004-10-14
    body: "Does KDM have the ability to launch access features at this point? It can be very useful, because some people may have a difficult time actually logging in without their specific assistive technology. I believe the new GDM has this support."
    author: "Henrik"
  - subject: "Work to be done..."
    date: 2004-10-14
    body: "If this is going to be used, there is work to be done to make it actually usable I think. Just look at the third screenshot provided (Kicker; http://trolls.troll.no/~harald/accessibility/accessibility3.png). A whole lot of \"no description\" labels that make it impossible to actually distinguish anything usefull. I think it will be a lot of work before each and every application can provide usefull information via such an interface. "
    author: "Andre Somers"
---
A new snapshot of the <a href="http://trolls.troll.no/~harald/accessibility/">cspi-dbus bridge</a> has been released. Together with the <a href="http://trolls.troll.no/~harald/dbus/">Qt 4 D-BUS bindings</a>, it is now possible to write <A href="http://accessibility.kde.org/">KDE assistive technologies</a> that transparently interact with Qt/KDE applications as well as GTK/GNOME applications.




<!--break-->
