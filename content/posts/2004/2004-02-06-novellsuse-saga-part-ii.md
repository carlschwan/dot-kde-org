---
title: "Novell/SUSE Saga Part II"
date:    2004-02-06
authors:
  - "numanee"
slug:    novellsuse-saga-part-ii
comments:
  - subject: "Talks"
    date: 2004-02-06
    body: "I still don't understand why Ximian spread false news about Suse's committment to KDE. This was very bad PR work. Suse sells a KDE distribution. So I hope Ximian will be forced to provide better software support for the KDE plattform.\n\nA Suse official talked to me and said the Ximian news was all crap. I guess the market decides. And SuSe has a market. "
    author: "Andre"
  - subject: "Re: Talks"
    date: 2004-02-06
    body: "Did ximian spread false news?\n\nI think the skeptical conclusions about Novell/Suse's commitment to KDE were very obvious and reasonable to make. \n\nThat said, I'm pleasantly surprised to see these stories.\n"
    author: "Anony Guy"
  - subject: "Re: Talks"
    date: 2004-02-06
    body: "Si. ximian spread the news that Suse will switch to Gnome and old qt-license FUD. \n\nSuse is driven by a strong desktop market not by a small development company abroad. "
    author: "Andre"
  - subject: "Re: Talks"
    date: 2004-02-06
    body: "I don't think there is evidence that Ximian folks spread the rumors. "
    author: "anon"
  - subject: "Re: Talks"
    date: 2004-02-06
    body: "Well, never mind.. I just read this from the article :)\n\nhttp://www.mail-archive.com/mono-list%40lists.ximian.com/msg04360.html"
    author: "anon"
  - subject: "I don't see Ximian FUD in that article"
    date: 2004-02-06
    body: "The only KDE/Ximian Desktop news I saw in that post stated:\n\n\nNovell/SuSE will continue to distribute KDE and Gnome.\n\nNovell/SuSE will include Ximian Desktop build, as it is a more fine tuned desktop than the default Gnome build they ship.\n\n----\n\nThat doesn't seem to imply anything other than what it says, that KDE and Ximian Desktop will both be available with SUSE Linux, rather than KDE and the default GNOME that SUSE used to ship."
    author: "anon"
  - subject: "Re: I don't see Ximian FUD in that article"
    date: 2004-02-06
    body: "Miguel was talking shit as usual.  He said that Nat & him were in charge of the Novell desktop.  Except they are not.  Marcus Rex, a SuSE guy is in charge.  Nat & Miguel's company is being absorbed by a KDE-centric company.  How do you like them apples? "
    author: "anon"
  - subject: "Re: I don't see Ximian FUD in that article"
    date: 2004-02-07
    body: "Don't forgot how they encouraged everyone on slashdot and at tradeshows to believe that Novell would be putting KDE on the backseat."
    author: "ac"
  - subject: "Re: Talks"
    date: 2004-02-06
    body: "According to Bruce Perens and others who have spoken to them, I would definitely say yes they spread rumors:\n\nhttp://lists.userlinux.com/pipermail/discuss/2003-December/000396.html"
    author: "ac"
  - subject: "Re: Talks"
    date: 2004-02-06
    body: "> Did ximian spread false news?\n\nThat's been answered. It's possible that when some of these statements were made it was partly wishful thinking based on preliminary interactions.\n\n> I think the skeptical conclusions about Novell/Suse's commitment to KDE were very obvious and reasonable to make.\n\nI don't. SUSE has had a business model for a while and has strength in the business world. Ximian doesn't have that. Nat pulled a rabbit out of the hat with real moxie getting them venture capital when he did. M & N are smart guys, but they don't have a business track record. They also have a degree of baggage. Nat's statements in particular were not what Novell needed to see floating around when they were trying to mobilize a Linux strategy. I think the shine came off that apple fairly quick. Moreover, if Novell knew they were going to do the SUSE deal when they were still doing the Ximian deal then they were just staffing programmers there. As 15 are going to SUSE it would appear they want to direct that resource a little differently. Whether they will program GNOME, KDE or GNOME interoperability with KDE remains to be seen but I'm guessing it will not be just GNOME.\n\nThere's been a lot of weird freaking out from this whole thing which I believe was what some individuals who don't like KDE wanted. However for all the crying about how KDE needs corporate support and the best technology doesn't always win there was just not much clear thinking prevailing. I was never upset. Now we can observe the outcome.\n\n1) Companies will support what customers ask for. Give good software to a few million people and try to keep it a secret. Shazzaam! \n\n2) Being largely dependent on corporate involvement is very dangerous because when the great powers aren't smiling on you then you give up ground.\n\n3) The surest way not to get to \"rule the world\" is to create friction among factions that create image problems for advancing your cause. Unless of course you are going for the trailer park wrestlemania crowd.\n\nI personally told Nat once that there is a little something to be gained with the experience of years, but I guess it was important to him to be smarter than me anyway. ;-) (Note that my company isn't worth millions yet but I own it outright and it's profitable.) Ximian and SUSE had to sell for two different reasons. They both needed money but for SUSE it was to have the resources to go to the next level. \n\nIt will be interesting to hear the new reasons KDE is evil and dead. Of course most in the community play nice and know that open source projects aren't the enemy. "
    author: "Eric Laffoon"
  - subject: "Re: Talks"
    date: 2004-02-06
    body: ">It's possible that when some of these statements were made it was partly \n>wishful thinking based on preliminary interactions.\n>Nat's statements in particular were not what Novell needed to see floating \n>around when they were trying to mobilize a Linux strategy.\n\nFor what it's worth, I tried to get some clarifications from Nat, but I guess he's a busy guy."
    author: "Navindra Umanee"
  - subject: "Re: Talks"
    date: 2004-02-10
    body: "thanks Eric,\n\nwhat's a trailer park?"
    author: "Andre"
  - subject: "Re: Talks"
    date: 2004-02-06
    body: "15 Ximian developers are now working for Suse so this could get interesting."
    author: "ac"
  - subject: "Who rulez? KDE or Gnome?"
    date: 2004-02-06
    body: "No one. I bet they are forced to cooperate since now they are on the same ship. I wish this will bring a heavy push towards the interoperation among desktops that we all need."
    author: "vokimon"
  - subject: "Re: Who rulez? KDE or Gnome?"
    date: 2004-02-06
    body: "According to GoogleFight, GNOME wins by a hair:\n\nhttp://tinyurl.com/3cayq\n\nIts really close though :)\n"
    author: "Rayiner Hashem"
  - subject: "Re: Who rulez? KDE or Gnome?"
    date: 2004-02-06
    body: "Probably just people searching for info on how to switch their buttons around.\n\nBut seriously, searching Google either means somebody is interested in something, thinks something is broken, or got excited about Lord of the Rings and is now interested in gnomes.  In other words, it doesn't mean anything."
    author: "ac"
  - subject: "Re: Who rulez? KDE or Gnome?"
    date: 2004-02-06
    body: "Its called a joke :) Hence the smiley. I just find it very fitting for a thread titled: \"Who rulzed? KDE or GNOME?\"\n"
    author: "Rayiner Hashem"
  - subject: "Re: Who rulez? KDE or Gnome?"
    date: 2004-02-06
    body: "wow, you joked about the KDE/GNOME holy war? that's just unexcusable :)"
    author: "anon"
  - subject: "Re: Who rulez? KDE or Gnome?"
    date: 2004-02-06
    body: "Or gee, websites on Garden Gnomes?"
    author: "another ac"
  - subject: "Re: Who rulez? KDE or Gnome?"
    date: 2004-02-06
    body: "some more results (without fantasy gnomes ;)\n\nKDE desktop : GNOME desktop = 2 110 000 : 1 650 000\nKDE apps : GNOME apps = 1 070 000 : 591 000\nKDE app : GNOME app = 278 000 : 739 000\n\nwhich means that everyone should add \"KDE app\" to his webpage ;-)\n"
    author: "MK"
  - subject: "Re: Who rulez? KDE or Gnome?"
    date: 2004-02-06
    body: "That\u00b4s just the bazillion guys saying, \"GIMP is the only GNOME app I use\" ;-)"
    author: "Roberto Alsina"
  - subject: "Re: Who rulez? KDE or Gnome?"
    date: 2004-02-06
    body: "But The GIMP isn't even a GNOME application!\n"
    author: "Adam Foster"
  - subject: "Re: Who rulez? KDE or Gnome?"
    date: 2004-02-06
    body: "Oh, some of those pages are from people saying that, too ;-)"
    author: "Roberto Alsina"
  - subject: "Re: Who rulez? KDE or Gnome?"
    date: 2004-02-06
    body: "easier, more accurate:\nhttp://tinyurl.com/2qql9"
    author: "djay"
  - subject: "Re: Who rulez? KDE or Gnome?"
    date: 2004-02-06
    body: "With Gnome being a generic term and Google not being case-sensitive this doesn't make much sense."
    author: "Anonymous"
  - subject: "Re: Who rulez? KDE or Gnome?"
    date: 2004-02-06
    body: "Well, since the difference between the keywords KDE and GNOME is just a hair, and GNOME is a generic name, KDE is the winner :)"
    author: "rinse"
  - subject: "Re: Who rulez? KDE or Gnome?"
    date: 2004-02-06
    body: "As far as I know \"kde\" is a very common word in the czech language."
    author: "MK"
  - subject: "Re: Who rulez? KDE or Gnome?"
    date: 2004-02-06
    body: "Pathetic this comment, isn't it? What is the proportion of czech pages on internet compared to english? 0.1%? More?\n\nAnyways, this is irrelevant. Everybody rulez when it doesn't sucksorz."
    author: "Inorog"
  - subject: "Re: Who rulez? KDE or Gnome?"
    date: 2004-02-06
    body: "The question should be \"what's the proportion of czech pages containing 'kde' to english pages containing 'kde'?\", making the numbers much different ;). And actually 'kde' has the same meaning in other Slavic languages as well, maybe even in all of them, I'm not sure (what a pity Google search for 'kde' probably doesn't include russian pages because of the alphabet).\nAnyway, using Google searches for things like this is about as good idea as using Slashdot polls.\n"
    author: "Lubos Lunak"
  - subject: "Re: Who rulez? KDE or Gnome?"
    date: 2004-02-06
    body: "'Where' is 'gde' not 'kde' in Russian, so that wouldn't help. (Aieee... please no jokes on this.. pleasssee.)\n\n"
    author: "Sad Eagle"
  - subject: "Re: Who rulez? KDE or Gnome?"
    date: 2004-02-06
    body: "> With Gnome being a generic term\n\nNot only Gnome, also KDE is a generic term. In czech \"kde\" means \"where\"."
    author: "Stefan Heimers"
  - subject: "Re: Who rulez? KDE or Gnome?"
    date: 2004-02-06
    body: "here's a more fair fight...\n\nhttp://tinyurl.com/yrlle\n\nkde desktop environment\n ( 958 000 results)\nversus\ngnome desktop environment\n ( 609 000 results)\n\n\nboo yeah"
    author: "standsolid"
  - subject: "Re: Who rulez? KDE or Gnome?"
    date: 2004-02-06
    body: "more flaming: \ndon't know how to use tinyurl:\nhttp://tinyurl.com/2nofk\n\nThis time gnome wins ! :-)\n\nBtw. this was \"KDE problem\" against \"Gnome problem\" :-P\n\nAlex\n\n"
    author: "aleXXX"
  - subject: "Re: Who rulez? KDE or Gnome?"
    date: 2004-02-06
    body: "> don't know how to use tinyurl\n\nVisit tinyurl.com, enter URL, click \"Make TinyURL!\"."
    author: "Anonymous"
  - subject: "Re: Who rulez? KDE or Gnome?"
    date: 2004-03-06
    body: "Why not let Google count the links to the desktop environment homepages?\n\nSearched for pages linking to www.kde.org.  Results 1 - 10 of about 51,700\nSearched for pages linking to www.gnome.org.  Results 1 - 10 of about 9,500"
    author: "Anonymous"
  - subject: "Re: Who rulez? KDE or Gnome?"
    date: 2005-12-09
    body: "I do not fully understand what was the flame for. Every half-wit knows that Gnome is simply much better than KDE. You can discuss it, you can disagree, but that's all you can do against it."
    author: "K15"
  - subject: "Do not get to hard on Ximian"
    date: 2004-02-06
    body: "IIRC, Migual and Nat  worked at one time for MS. That company teaches internally that you give an opinion to the populace (4 legged MS good, 2 legged Linux bad). They are just taking a marketing direction as they did their technical ; from another company."
    author: "a.c."
  - subject: "Re: Do not get to hard on Ximian"
    date: 2004-02-06
    body: "Don't spread untrue fud please"
    author: "bleb"
  - subject: "Re: Do not get to hard on Ximian"
    date: 2004-02-06
    body: "what is untrue or fud?\n\nthat Miguel and Nat worked at MS and met there?\n\nhttp://primates.ximian.com/~miguel/helix-history.html\n\nperhaps, you consider that MS forces their opinions on others to be FUD?\n\nRemember how MS had a number of ppl doing astroturfing on OS/2?\nOr perhaps the letters that was being sent to newspaper all over and it turned out to be a PR agency from San Diego under MS contract?\nOr all the Reports that are coming from Gartner/IDC about how bad Linux is and how good Windows is? Always loaded with half truths at best, normally flat out lies.\n\n\nPerhaps the last sentance is being consider FUD?\n\nThe technical direction for evolution was straight out of Outlook. \nIt was a simple clone (with a nice L&F to it).\n\nLikewise, they are working on mono. \nSo just what exactly is mono?\nIs it a project that was original and they started on their own? \nI think not.\nBTW, I do like what I see in mono/.nyet, but I do not trust MS (or ximian for that matter). In many ways, it addresses the shortcomings of java.\n\n\nPerhaps you did not like my comparing the marketing to their input via MS.\nMigual is always speaking down KDE which sounds like MS on Linux.\n During the Novell/SUSE merge he announced in a slashdot posting that they would control the future direction of Novell and SUSE. It looks like Migual is simply doing postuering to help build support in the future. That is exactly what MS does. They have historically promise a great deal and delivered as little as possible.\n\nMigual and Nat learned well.\n \nSo, what is FUD or untrue here? \nOther than my opinion about mono, it was all factual."
    author: "a.c."
  - subject: "Re: Do not get to hard on Ximian"
    date: 2004-02-06
    body: "FUD, but even if it was true: so what? who cares?"
    author: "Thomas"
  - subject: "Re: Do not get to hard on Ximian"
    date: 2004-02-08
    body: "Miguel IIRC applied for a job at Microsoft to port IE to Unix.\nNat did a summer internship there."
    author: "dumdeedum"
  - subject: "Re: Do not get to hard on Ximian"
    date: 2004-02-06
    body: "As far as I know, Miguel wanted to work for MS, but didn't get a visa to work in the USA. He never worked for MS."
    author: "Birdy"
  - subject: "EMEA"
    date: 2004-02-06
    body: "I didn't knew it -- for people in the same situation:\nEMEA stands for Europe-Middle East-Africa."
    author: "Andreas"
  - subject: "Of course they'll choose KDE"
    date: 2004-02-06
    body: "Of course they will.\nNow, you can even make KDE-apps, in Gtk."
    author: "OI"
  - subject: "The Truth for Ximian Encapsulated in One Post"
    date: 2004-02-06
    body: "Nat Friedman, as much as he hums and hars was never in charge of desktop development at Novell. Nat and Miguel are too young guys who talk a bunch of crap, and I can't believe they are about the same age as me. I use KDE and they use Gnome, so maybe that's the difference.\n\nLet me make one thing clear: SUSE IS THE ENTERPRISE LINUX DIVISION OF NOVELL NOT XIMIAN, and will become a much more central part of Novell over time. Money talks: Suse is expanding and making bags of it, Ximian is doing anything but. Quite why people thought that an insignificant little company (and I use that word in the broadest possible sense) will hold sway over a division that will be making serious money for Novell is anyone's guess.\n\nI think with Ximian developers being moved to Suse we will see Ximian completely disbanded as a separate entity. That is the only way you can interpret that. Given that Suse's desktop sales are pretty good, better than Red Hat's ever were, I don't think we'll be seeing Ximian desktop as the default on Suse's offerings at all. In fact I think it will be highly likely that Ximian Desktop, Red Carpet (why do I think that was a bad bit of marketing - it sounds like a Red Hat product) will be broken up and its technologies used elsewhere.\n\n\"Markus Rex will now be responsible for Novell's Linux desktop activities and not solely Miguel and Nat as previously hinted.\"\n\nMarkus Rex will be in charge, not Nat and not Miguel at all. This will not be an equal partnership of any kind. Perhaps Novell have realised that Suse already do desktops quite well and at far less cost than with Ximian. That's called a reality check. Payback is going to be a bitch I'm afraid, but that is where desperate FUD gets you.\n\nThis is a great opportunity for KDE to support Novell on the desktop through Suse. Groupwise support for Kontact? Sounds good. I'd certainly like to be able to do that or use any groupworking services I like. The simple fact is that KDE does not need tens of millions of dollars to keep it going, Suse does not prop it up as some people believe (remember Eric Raymond and Joe Barr on the Linux Show!?), it pays for itself, it has a good development architecture that enables this and development does not happen at a headless chicken pace.\n\nEric Raymond's prediction comes full circle. I think we will see GTK and Gnome still around (QtGTK etc.), but in terms of the \"potential\" for business Linux desktops (we must not get ahead of ourselves at all - Windows is still deeply entrenched), I think Gnome and Ximian have had their fifteen minutes of fame.\n\nSorry, I don't see these announcements in any other context."
    author: "David"
  - subject: "Re: The Truth for Ximian Encapsulated in One Post"
    date: 2004-02-06
    body: ">>>Nat Friedman, as much as he hums and hars was never in charge of desktop development at Novell. Nat and Miguel are too young guys who talk a bunch of crap<<<\n\nTalking 'a bunch of crap' as you call it, did get them somewhere.\n\nFrom the Ximian/Novell website:\n\"Ximian had its genesis in the GNOME project, which was initiated in 1997 by Miguel  de Icaza, Ximian's cofounder, now Vice President of Product Technology for Novell.  The effort quickly attracted a group of talented architects and engineers,  including Nat Friedman, Ximian cofounder and now Vice President of Product  Development for Novell.\"\n\nAt the moment though Nat moved to Vice President of Research and Development.\n\n>>>Markus Rex will now be responsible for Novell's Linux desktop activities<<<\n\nmr.Rex will lead the development of SUSE LINUX, from the desktop to server, and will work with other Novell business units to deliver a complete Linux solution stack. Rex will also assume responsibility for Novell's Linux desktop activities.\n\nIMO mr. Rex will do what's best for Novell, where most money can be generated from, don't make the mistake to believe that automatically is also the best for KDE. "
    author: "Tink"
  - subject: "Re: The Truth for Ximian Encapsulated in One Post"
    date: 2004-02-07
    body: ">At the moment though Nat moved to Vice President of Research and Development.\n\n... at the \"Novell Ximian Services business unit at Novell\""
    author: "ac"
  - subject: "Re: The Truth for Ximian Encapsulated in One Post"
    date: 2004-02-07
    body: "\"Ximian had its genesis in the GNOME project, which was initiated in 1997 by Miguel de Icaza, Ximian's cofounder, now Vice President of Product Technology for Novell. The effort quickly attracted a group of talented architects and engineers, including Nat Friedman, Ximian cofounder and now Vice President of Product Development for Novell.\"\n\nWell yer, I suppose it has got them made for life so maybe I'm missing the point. In terms of long term business success, no. This doesn't really mean anything.\n\n\"IMO mr. Rex will do what's best for Novell, where most money can be generated from, don't make the mistake to believe that automatically is also the best for KDE.\"\n\nYer. Like ditching the tens of millions Nat is (and has spent) joyfully spending on Gnome with non-existent returns. KDE does not need that kind of investment from one (nor indeed any) company, and worse, Gnome, Ximian Desktop nor anything else makes any money for the Ximian division. It doesn't matter which way you cut it, logic tells you that this is a good thing for KDE."
    author: "David"
  - subject: "Re: The Truth for Ximian Encapsulated in One Post"
    date: 2004-02-06
    body: "I've never seen that many BS from a kde troll.\nYou make it looks like it's a Nazi Desktop. \"The Superior Race Choice\".\n\nBoth KDE and GNOME are great. They have their oddities and strengths, so, it's up to the user to decide which is best for him, otherwise, why the heck is fvwm and such are included in all distros?\n\nGive us a break and shut the fuck up, stupid troll."
    author: "meh"
  - subject: "Re: The Truth for Ximian Encapsulated in One Post"
    date: 2004-02-07
    body: "Read it and apply some logic. This was a commentary about Ximian and why they haven't made any money - that's the reality - and what the future holds for them as a result of that. Given all the \"Oh, Suse will not maintain KDE any longer\" comments, I felt it was awrranted now that some reality seems to have been injected into the situation.\n\nIt was NOT a comparison about KDE and Gnome if you'd actually read it, which you haven't.\n\nI wish people would read rather than swearing and coming up with these stupid troll comments. If you're going to come up with these half-arsed comments, prove me wrong then."
    author: "David"
  - subject: "Re: The Truth for Ximian Encapsulated in One Post"
    date: 2004-02-07
    body: "Logic... I do not think this word means what you think it means."
    author: "Boggle"
  - subject: "Re: The Truth for Ximian Encapsulated in One Post"
    date: 2004-02-08
    body: "Very simple. Ximian makes no money, Suse makes it == logic. If you can't understand that then there is no point in talking to you."
    author: "David"
  - subject: "Re: The Truth for Ximian Encapsulated in One Post"
    date: 2004-02-08
    body: "so, I take it you've seen the ximian financial reports to backup your assertions?\nIf not, then it might make sense to stop talking about stuff you had no idea about."
    author: "dumdeedum"
  - subject: "Re: The Truth for Ximian Encapsulated in One Post"
    date: 2004-02-08
    body: "This getting fairly desperate now. As far as I know there are no public financial statements about Ximian because they are a private company. Nice try :). Besides, even if I had seen them I wouldn't believe them because I would wager there would be a huge amount of creative accounting on there.\n\nWe have to make logical judgements based on what is actually happening. Anyway, I don't have to see any such thing to know that they are making a huge loss, simple 'provable' logic will suffice. Why provable? Because these are things that have happened. I doubt whether we will ever see financial reports on Ximian as they were private, and especially now that they are being broken up.\n\nThey have swallowed tens of millions of venture capital trying to sell 'free' desktop software for which there is no market. There is not even the prospect of any revenue, let alone profit. Even if there was a market, this would take years to recoup. Lindows, Xandros and others are not making money and they sell whole Linux distributions that Ximian does not. Lindows have actually admitted they are not making money - and Ximian? I doubt that Suse Desktop sales are anything to get hot about and Red Hat dropped their desktop distribution which means that the desktop situation is just not financially viable at the moment. Hell, even Novell has posted losses this year, and they own Ximian. Oh yer. Why did Ximian need to get bought?\n\nI do know a low about this because I am in a computing business, I know about revenue models, I know what a viable market looks like, I can add up and I can see what is happening.\n\nWhatever, it is time stop getting desperate, face facts and understand that Ximian DOES NOT MAKE MONEY. Anyway, this will be made very clear in the coming weeks and months, so don't just take it from me."
    author: "David"
  - subject: "Re: The Truth for Ximian Encapsulated in One Post"
    date: 2004-02-09
    body: "So, you are saying that the people of Novell are complete idiots? Afterall according to you the bought a company which doesn't make money and never will make money.\n\n"
    author: "Jasper"
  - subject: "Re: The Truth for Ximian Encapsulated in One Post"
    date: 2004-02-09
    body: "They brought a brand and company expertise.  This will now be folded into SUSE and KDE since it's more cost efficient."
    author: "ac"
  - subject: "Re: The Truth for Ximian Encapsulated in One Post"
    date: 2004-02-09
    body: "Well, you might think that but I couldn't possibly comment :)."
    author: "David"
  - subject: "Re: The Truth for Ximian Encapsulated in One Post"
    date: 2004-02-09
    body: "My point was to point out that you have no clue what you are talking about.\n\nYou have no facts to prove that Ximian does not make money, you have your conjecture about it.\n\nYour logic has no grounds other than fantasy, and your \"proof\" that Ximian does not make money (it needed \"to get bought\") can be applied equally to SuSE...\n\nThe more and more I read of your posts, the more I think you are 13 and I don't know many 13 year olds in a computing buisness. So I call Bullshit."
    author: "dumdeedum"
  - subject: "Re: The Truth for Ximian Encapsulated in One Post"
    date: 2004-02-09
    body: "\"You have no facts to prove that Ximian does not make money, you have your conjecture about it.\"\n\nXimian have not produced any financial records. Like I said. Nice try :).\n\n\"Your logic has no grounds other than fantasy, and your \"proof\" that Ximian does not make money (it needed \"to get bought\") can be applied equally to SuSE...\"\n\nThis can be applied to Suse can it? This shows that you have no business sense and don't know what is going on in the IT world.\n\n\"The more and more I read of your posts, the more I think you are 13 and I don't know many 13 year olds in a computing buisness. So I call Bullshit.\"\n\nCall it what you like, but it seems to be bullshit you are irritated by. Maybe you aren't so stupid as you seem?"
    author: "David"
  - subject: "Re: The Truth for Ximian Encapsulated in One Post"
    date: 2004-02-10
    body: "\"Ximian have not produced any financial records. Like I said. Nice try :).\"\n\nFor a guy who talks a lot about logic, you dont seem to have the faintest idea what it means. Try taking a course in formal logic before you use the word proof so liberally. \n\nSince you don't have Ximian's financial records, or any idea what products they sell and to whom, you cannot prove they aren't making money. No amount of \"logic\" will make disappear the fact that you are woefully misinformed about a company you haven't the faintest clue about.\n\nI call bullshit too -- you are just a 15 year old trying to justify an emotional attachment to a desktop with what you think is \"logic\".\n\nCheers,\nRyan"
    author: "Ryan"
  - subject: "Re: The Truth for Ximian Encapsulated in One Post"
    date: 2004-02-10
    body: "Why do you keep changing your name, or are you all programmed the same way?\n\n\"For a guy who talks a lot about logic, you dont seem to have the faintest idea what it means. Try taking a course in formal logic before you use the word proof so liberally.\"\n\nNot relevant - a cheap remark. That's the trouble with many 'open source' people - no business sense. The proof that I have is business sense and what is actually happening in the industry. The difficulty is that you don't understand that.\n\n\"Since you don't have Ximian's financial records,\"\n\nLike I said, nice try :). Ximian are a private company, but you can have a very accurate guess from what has actually happened and the products that they 'sell', or don't sell. If Ximian want to quash these rumours they can produce financial results. Hiding behind Ximian's financial records won't help you. Where is the business model? If there is one explain it to me, and if there isn't one they are not making money. Logical enough for you? If you can't do that 'you' don't know what 'I' am talking about, so you cannot possibly know that I don't have a clue what I'm talking about. Logical?\n\n\"...or any idea what products they sell and to whom, you cannot prove they aren't making money. No amount of \"logic\" will make disappear the fact that you are woefully misinformed about a company you haven't the faintest clue about.\"\n\nOh, I'm hitting home here :)!! You're talking about me not having the faintest clue about Ximian, but you seem to think I don't know what it is that they do - or don't do as the case may be. Have you not read what I've written, or are you like all the other clueless morons who think that they're replying to a 'troll'?\n\nI can see what products Ximian are selling - none. They are trying to 'sell' free software, like Evolution and Ximian Desktop, in a non-existent market place thus far. For some reason they seem to think that they can make money by selling Ximian Connectors for Evolution and server software for Red Carpet and support for 'Ximian Desktop'. That's it. Care to tell me if I'm hitting the mark, because you don't seem to know what Ximian do?\n\nYou could quite reasonably ask why Red Carpet server and the connectors are not free. Why try and make money out of them? This is what Bruce Perens and UserLinux seem to want to do. Even if they did make money out of this the business rational is totally feeble because people could quite easily make free software replacements for these. Ximian can't make any money of any significance from Evolution, Ximian Desktop or Red Carpet (there isn't the market there) and certainly not enough to recoup 15+ million dollars and all of the Novell resources Ximian have been joyfully lavishing. Ximian are trying to spend lots of money on developing free software like their desktop, Evolution, Open Office and Mono, and they are trying to do this in-house (Microsoft hangover?). They don't even offer any services, which is suicidal in the market that they are in. If you want to develop software in-house, you need to charge for it in some big way. Free software means leveraging a community effort. Get it? Logic.\n\nThe people at Ximian don't seem to understand free or open source software business models at all. You just can't justify that kind of spending with the 'market' that they are in. Novell have made losses this year you know, and Ximian will be the first to feel the effects.\n\n\"I call bullshit too -- you are just a 15 year old trying to justify an emotional attachment to a desktop with what you think is \"logic\".\"\n\nOh the emotional argument, but I haven't mentioned any desktops here. I briefly mentioned why I think there is more business sense at one than the other. Your job is to tell me why I'm wrong.\n\nNice try, but this sounds like an emotional and desperate statement if ever I saw one. It is logic that you don't understand, or want to accept, and if this is all you can use to take it down it stands up very well.\n\nCome back and reply to exactly what I have written (quote me and reply underneath) and why I am wrong rather than these pathetically feeble \"emotional\", \"don't know what I'm talking about\" comments. Why do I not understand what I'm talking about? Why do Ximian have a solid business model? What do Ximian do? How do they make money?\n\nIf not I'm more right than I first thought, and that amuses me.\n\nThank you and goodnight."
    author: "David"
  - subject: "Re: The Truth for Ximian Encapsulated in One Post"
    date: 2004-02-10
    body: "David! How many times have I told you that you're not to use the Internet without supervision? Thats it, I'm cutting your allowance and you're not allowed on the computer for a week.\n\nAnd its way past your bedtime, I know a little boy who's got a big day in school tomorrow."
    author: "David's Mom"
  - subject: "Re: The Truth for Ximian Encapsulated in One Post"
    date: 2004-02-11
    body: "While funny that was just wrong.  LOL"
    author: "michaeln"
  - subject: "Re: The Truth for Ximian Encapsulated in One Post"
    date: 2004-02-12
    body: "I must really have hurt some people here. While you prepare for bed I'll be working on a very expensive contract for a local airport. Sleep tight.\n\nTrying to make out I'm still at school. Funny, but that's what you get when you hit home hard I'm afraid."
    author: "David"
  - subject: "Re: The Truth for Ximian Encapsulated in One Post"
    date: 2004-02-14
    body: "Nah, we're not hurting...\nWe're just ripping the piss out of you."
    author: "David's Mom"
  - subject: "Re: The Truth for Ximian Encapsulated in One Post"
    date: 2004-02-11
    body: "\"Why do you keep changing your name, or are you all programmed the same way?\"\n\nAh yes, anyone who doesnt agree with you, if they are not one lonely disaffected loser, must be unintelligent robots.\n\n\"If not I'm more right than I first thought, and that amuses me.\"\n\nI think that pretty much says it all. Very sad really.\n\n\"Not relevant - a cheap remark\"\n\nTotally relevant. You completely fail to understand any concept of logic beyond \"what makes sense to me must be logical\". Im not trying to prove Ximian is making money, Im trying to prove that you can't prove they arent. *THATS* logic. Please take some more schooling before trying to teach a mathematician logic.\n\n\"The proof that I have is business sense and what is actually happening in the industry.\" \n\nThats not proof of anything -- the fact that you think it is any sort of a proof is exactly what Im talking about. I think you havent the faintest idea how business works. I think you sit in your basement and wonder to yourself why no one in the world is a smart as you -- without ever having to prove yourself by running you own multi-million dollar business. You want to debate a business you dont know about, on a forum where its totally off topic -- that pretty much says it all about your business acumen.\n\n\"but you can have a very accurate guess from what has actually happened and the products that they 'sell', or don't sell.\" \n\nIf you had actual experience with following a real corporation you know they dont write press releases everytime they make a sale. Many times their customers dont want the details of their contract out. Also telling the world about how you just sold some boring software to some boring company just isnt worth the time to publicise.\n\n\"If Ximian want to quash these rumours they can produce financial results.\"\n\nFirst of all theres no rumors, theres just you trolling on a KDE forum -- and lets be clear they dont give a rats ass about you. Secondly, private companies dont publicly release their financials, theres nothing to gain and everything to  lose. Then again, you know that if you had any business education.\n\n\"Where is the business model? If there is one explain it to me, and if there isn't one they are not making money. Logical enough for you?\"\n\nActually that statement is not logical. You admit that if there is a business model then you dont understand it, but if there isnt one, then it is impossible to make money. Your arguments are so flawed on so many levels: The existance of a business model is independant of your understanding of business models, so just because you can't see it doesnt mean it doesnt exist. Then you assume it is possible for business to exist without a business model, yet you fail to see that the ability to have revenues greater than expenses is possible without a highly accurate business model. I.E. I could claim that my business model is to sell peanut butter to increase sales of higher margin bread, but if my peanut butter is better quality than my bread I still make money off peanut butter sales, provided its not a loss-leader.\n\n\"If you can't do that 'you' don't know what 'I' am talking about, so you cannot possibly know that I don't have a clue what I'm talking about. Logical?\"\n\nOnce again this statement is actually not logical at all, and only serves to prove your definition of \"logic\" is \"what ever makes sense to me\". \n\nIf I cant explain Ximian's business model to you then I dont know what Im talking about?? I dont know where to start with that. If you think that is logic then try this: If you cant prove The Generalized Reimann Hypothesis, then you owe me $100. I await your cheque.\n\n\"but you seem to think I don't know what it is that they do [...]\"\n\nNow *THAT* statement *IS* logically sound.\n\n\"I can see what products Ximian are selling - none.\"\n\nBut this one is logcally inconsistant with you prior admission that you don't know what Ximian's business model is. It is also factually absurd since my company has actually purchased XD2 and Connector. If Ximian has sold even ONE product the above is logically false, by definition.\n\n\"For some reason they seem to think that they can make money by selling Ximian Connectors for Evolution\"\n\nSince they *are* selling Connector, they seem to know more than you.\n\n\"Care to tell me if I'm hitting the mark, because you don't seem to know what Ximian do?\"\n\nWow. Logical inconsistency abounds. How can I tell you if your hitting the mark if I dont know what Ximian does? If I can tell you whether you are hitting the mark then, then I do actually know what Im talking about. Either way what you ask me to do is logically impossible.\n\n\"You could quite reasonably ask why Red Carpet server and the connectors are not free.\"\n\nYou are constantly contradicting yourself: the reason there is no free is Conenctor because they are selling it as you have said above. Thats called a tautology in formal logic -- its a statement that is true regardless of interpretation.\n\n\"Even if they did make money out of this the business rational is totally feeble because people could quite easily make free software replacements for these.\"\n\nSo which is it? Are they making money or not? \n\nIf people could easily make free software replacements for proprietary software then where are my FreeConnector, FreeNVidiaDrivers, or FreeNovellDirectoryServices? It doesnt make sense. This explaining logic to you is getting tiring. Making software costs a lot of time, expertise, or if you are a business, it also costs money. It is NOT easy.\n\n\"The people at Ximian don't seem to understand free or open source software business models at all.\"\n\nYou have made no factual argument to back up this claim. Logic doesnt trump facts, it uses facts.\n\n\"Oh the emotional argument, but I haven't mentioned any desktops here.\"\n\nIts a KDE forum. I dont need logic to infer something obvious.\n\n\"I briefly mentioned why I think there is more business sense at one than the other.\"\n\nActually I dont recall you making any business case at all. Maybe your idea of business case is similar to your idea of logic: \"It makes sense to me\".\n\n\"Your job is to tell me why I'm wrong.\"\n\nActually you have a basic responsibility to make sure your own arguments arent so full of holes.\n\n\"Nice try, but this sounds like an emotional and desperate statement if ever I saw one.\"\n\nThen you havent seen very many. Like I said before, you dont sound very mature.\n\n\"It is logic that you don't understand, or want to accept, and if this is all you can use to take it down it stands up very well.\"\n\nWishing something is true doesnt make it so. Your \"logic\" is full of holes.\n\n\"Come back and reply to exactly what I have written (quote me and reply underneath) and why I am wrong rather than these pathetically feeble \"emotional\", \"don't know what I'm talking about\" comments. Why do I not understand what I'm talking about?\"\n\nIs the above good enough for you? I only did so since you asked me too -- but make no mistake I wont be dont this again. Its way too draining. The only reason I decided too is on the hope you will take your plentiful creativity to a school and get some formal schooling of logic and rhetoric.\n\n\"Thank you and goodnight.\"\n\nWill you be here all week?\n\nCheers,\nRyan"
    author: "Ryan"
  - subject: "Re: The Truth for Ximian Encapsulated in One Post"
    date: 2004-02-12
    body: "That's a long non-sensical reply from someone who asked if I would be here all week!\n\n\"Ah yes, anyone who doesnt agree with you, if they are not one lonely disaffected loser, must be unintelligent robots.\"\n\nAh, the lonely disaffected loser routine. My comments must really have hurt some people. Unfortunately this is far from the truth, as I'll explain below.\n\n\"Totally relevant. You completely fail to understand any concept of logic beyond \"what makes sense to me must be logical\". Im not trying to prove Ximian is making money, Im trying to prove that you can't prove they arent. *THATS* logic. Please take some more schooling before trying to teach a mathematician logic.\"\n\nThis ain't mathematics - it's called business sense and business logic. It's a different big bad world out there I'm afraid. It would be lovely if it was that simple, but it isn't I'm afraid, and this does not hide your lack of knowledge on the subject. Please don't lecture me on running an IT business, since that is what I was actually talking about.\n\n\"Thats not proof of anything -- the fact that you think it is any sort of a proof is exactly what Im talking about.\"\n\nWell talk about what is happening in the industry and prove me wrong. Unfortunately you can't do it, which begs the question as to why you keep replying, desperate to try and shift the subject.\n\n\"I think you havent the faintest idea how business works. I think you sit in your basement and wonder to yourself why no one in the world is a smart as you -- without ever having to prove yourself by running you own multi-million dollar business. You want to debate a business you dont know about, on a forum where its totally off topic -- that pretty much says it all about your business acumen.\"\n\nYou really, really, really wish that I sat in a basement and wrote all this. Unfortunately I sit as part of a pretty successful small/medium sized IT company struggling to understand what I would do with 15 million dollars, or how we would even begin to return on that investment. And you make derisory remarks on my business acumen? Funny.\n\n\"First of all theres no rumors, theres just you trolling on a KDE forum\"\n\nAh, trolling. If you don't understand something call it a troll. KDE forum? What are you doing here then?\n\n\"You admit that if there is a business model then you dont understand it,\"\n\nThis shows that you don't understand or can see what Ximian's business model is. I can, even if it is so very feeble.\n\n\"but if there isnt one, then it is impossible to make money.\"\n\nSo if there isn't a business model then you are asserting that you can make money anyway? Nice one, I wish I could do that!\n\n\"I could claim that my business model is to sell peanut butter to increase sales of higher margin bread, but if my peanut butter is better quality than my bread I still make money off peanut butter sales, provided its not a loss-leader.\"\n\nAssuming that Ximian's free products sell Red Carpet and Novell's middleware and server stacks, do they sell enough through Ximian to justify the expense of Ximian? No. The 'products' Ximian works on are loss-leaders that sell nothing else as a result. How do I know all this? Novell is making a loss.\n\n\"You are constantly contradicting yourself: the reason there is no free is Conenctor because they are selling it as you have said above.\"\n\nEr, you could reasonably ask why the connectors and server software are not free as well. What do they offer? Why sell those? Someone will offer these for free and put Ximian in an even more difficult position.\n\n\"If I cant explain Ximian's business model to you then I dont know what Im talking about?? I dont know where to start with that.\"\n\nEr, start by explaining what you think Ximian's business model is?\n\n\"I dont know where to start with that. If you think that is logic then try this: If you cant prove The Generalized Reimann Hypothesis, then you owe me $100. I await your cheque.\"\n\nEr, nice try but we're talking about business models here. It sounds as if you could really use a cheque of any description with this sense.\n\n\"\"but you seem to think I don't know what it is that they do [...]\"\"\n\nI've just disproved that by describing what it is that they do. You haven't offered any such description, which means you don't know what it is that they do. I'm not talking about mathematics I'm afraid.\n\n\"But this one is logcally inconsistant with you prior admission that you don't know what Ximian's business model is. It is also factually absurd since my company has actually purchased XD2 and Connector. If Ximian has sold even ONE product the above is logically false, by definition.\"\n\nDo they sell enough to justify the expense of what they are doing? No. Just because they have you as a customer this isn't going to prop them up I'm afraid.\n\n\"But this one is logcally inconsistant with you prior admission that you don't know what Ximian's business model is. It is also factually absurd since my company has actually purchased XD2 and Connector. If Ximian has sold even ONE product the above is logically false, by definition.\"\n\nThis is where your gulf in knowledge really does show up. It doesn't matter if you sell, a hundred, ten or zero products, if they don't justify the expense then you are going out of business. In other words, you don't sell anything. So when your bank manager cuts off your air supply and says \"I'm sorry you don't sell anything\" you're going to say \"I'm sorry that's not factually correct...\"! At best he won't speak to you, at worst you'll get hit.\n\n\"Since they *are* selling Connector, they seem to know more than you.\"\n\nThey're not selling connector in enough numbers to justify the expense.\n\n\"How can I tell you if your hitting the mark if I dont know what Ximian does?\"\n\nIf you don't know what Ximian does, why reply?\n\n\"So which is it? Are they making money or not?\"\n\nNice attempt at an assertive comment. Unfortunately it doesn't match up with what I have written.\n\n\"If people could easily make free software replacements for proprietary software then where are my FreeConnector, FreeNVidiaDrivers, or FreeNovellDirectoryServices? It doesnt make sense.\"\n\nEr, perhaps because these are closed source products and Ximian is trying to sell free software in the same way as proprietary software?\n\n\"Making software costs a lot of time, expertise, or if you are a business, it also costs money. It is NOT easy.\"\n\nEr, yer. And you have to have a sound business model that justifies the expense.\n\n\"You have made no factual argument to back up this claim. Logic doesnt trump facts, it uses facts.\"\n\nYour opinion. I have given enough evidence to back this up to anyone with a knowledge of IT businesses. This is why my comments seem to have caused so much panic.\n\n\"Actually I dont recall you making any business case at all.\"\n\nWell, you wouldn't.\n\n\"Actually you have a basic responsibility to make sure your own arguments arent so full of holes.\"\n\nThey aren't. Please don't try and reverse this on me - why am I wrong?\n\n\"Then you havent seen very many. Like I said before, you dont sound very mature.\"\n\nNice cheap remark on maturity. It sounded like an emotional statement, so I said it, and by telling me that you have bought XD2 etc. it sounds very much like these are emotional staments to justify your investment to yourself. Did your boss read this or something? Pretty sad.\n\n\"Wishing something is true doesnt make it so. Your \"logic\" is full of holes.\"\n\nIn terms of the context of IT businesses, where are the holes? I haven't wished anything. Again, if you reverse this on yourself you are trying to justify your investment in some way. Quite sad.\n\n\"Is the above good enough for you? I only did so since you asked me too -- but make no mistake I wont be dont this again.\"\n\nEr, how about no? You haven't talked about Ximian, talked abut what you thought there business model was, how they make money etc. etc. None of what you wrote was in the correct context. The arguments that you did try to make were swiped aside pretty easily.\n\n\"Will you be here all week?\"\n\nNo, but it was good to poke some fun at some desperate people for five or ten minutes. If you are symptematic of the people that support Ximian then you speak for yourselves.\n\nThis thread ends here because I have obviously hurt quite a lot of people with these comments, possibly because people realise they can't be shifted."
    author: "David"
  - subject: "Re: The Truth for Ximian Encapsulated in One Post"
    date: 2004-02-14
    body: "apparently Novell execs don't smoke the same logic crack that you've been smoking.\n\n(see newest thread on this article for a url saying that Novell/SuSE plan on focusing on GNOME and not KDE)"
    author: "some chap"
  - subject: "Re: The Truth for Ximian Encapsulated in One Post"
    date: 2004-02-14
    body: "\"apparently Novell execs don't smoke the same logic crack that you've been smoking.\"\n\nDude, this discussion is over. Apparently I've got a lot of desperate people over to this forum.\n\n\"(see newest thread on this article for a url saying that Novell/SuSE plan on focusing on GNOME and not KDE)\"\n\nIt doesn't say that at. It talks a heck of a lot about servers with a little footnote at the bottom about desktops, and that they would support both and see what happens. It would be a decision for developers and users as to what they chose. Guess what that means?"
    author: "David"
  - subject: "..."
    date: 2004-02-06
    body: "I already sent it four days ago to slashdot but to my surprise they didn't pick it up there in any way. I didn't expect this news to show up here as I don't think it's that much related to KDE as a project at all but more to economy. My opinion is that open source projects should not bother with markets issues other than encouraging any kind of contribution which includes commercial players as well."
    author: "Datschge"
  - subject: "Re: ..."
    date: 2004-02-07
    body: "You're right, it is better not to get too up about this. Suse is merely only one company involved with KDE, but there has been an awful lot of rubbish talked about it so it is sometimes good to quash that. I don't see too many news articles here about Ximian/Novell/Suse anyway."
    author: "David"
  - subject: "Not what it says at all"
    date: 2004-02-07
    body: "Um, that's not what the article says at all. The press release states that Markus Rex will be the \"General Manager of Novell's SUSE LINUX Business Unit\". Companies like Novell have quite a few levels of management, particularly across the kind of  product lines that Novell now has to manage... Perhaps you guys should be looking into Markus Rex's staff and delegations a bit more closely. :-)"
    author: "Boggle"
  - subject: "Well..."
    date: 2004-02-07
    body: "To quote the press release:\n\"Novell's SUSE LINUX business unit today announced the appointment of Markus Rex as general manager. Rex, previously SUSE's vice president, Research and Development, will assume his new duties immediately. As general manager of the SUSE LINUX business unit, reporting to Chris Stone, Novell vice chairman - Office of the CEO, Rex will lead the development of SUSE LINUX, from the desktop to server, and will work with other Novell business units to deliver a complete Linux solution stack. Rex will also assume responsibility for Novell's Linux desktop activities.\"\n\nInterpret it however you want, I would think it is pretty clear."
    author: "Datschge"
  - subject: "Well..."
    date: 2004-02-08
    body: "There are many articles here, so I don't understand why you say that's not what the article (singular) says at all. Try reading :).\n\nThere are official releases that says Markus Rex is assuming responsibility for Novell's desktop programme and it states that many of Ximian's employees will be moving under the leadership of Markus Rex and Suse. You can interpret that in as many ways as you want, but it basically means that Ximian is being disbanded and Novell are getting what they paid for with Suse - Enterprise Linux expertise. Logic == Ximian leaks money like a sieve and doesn't really do anything, Suse makes it, is expanding and has a wealth of expertise.\n\nThe first priority for Novell and Suse is the server side, so I don't think that there will be a huge amount of desktop activity, but this is significant.\n\nWhy shouldn't we infer these things (with a bit of logic)? Many were happy to predict the demise of KDE and Suse, right out in public as well, and especially those at Ximian who hinted at it at every turn."
    author: "David"
  - subject: "Evolution?"
    date: 2004-02-08
    body: "Now that SuSE is head of the desktop division at Novell, when will Evolution be rewritten/ported to KDE?"
    author: "some chap"
  - subject: "Re: Evolution?"
    date: 2004-02-08
    body: "Never?"
    author: "Anonymous"
  - subject: "Re: Evolution?"
    date: 2004-02-08
    body: "Maybe they'll go for Kontact instead. No need to port anything."
    author: "OI"
  - subject: "Re: Evolution?"
    date: 2004-02-08
    body: "I would imagine that Kontact would come first and some of Ximian's technology would be ported to it, and perhaps vice versa. Difficult to say, but Kontact is nativ to KDE and works pretty well considering how long it has been in existence."
    author: "David"
  - subject: "Have you seen this?"
    date: 2004-02-13
    body: "This article (http://news.zdnet.co.uk/business/0,39020645,39146162,00.htm) says that they are going to focus on Ximian Desktop for enterprises, although they are going to continue supporting both.\n\nDid I read this in a wrong way?"
    author: "Santiago"
  - subject: "Re: Have you seen this?"
    date: 2004-02-14
    body: "hah, guess this means David was talking out of his arse."
    author: "some chap"
  - subject: "Re: Have you seen this?"
    date: 2004-02-14
    body: "Dont say that! Hes working on a big contract with an airport for $15 million dollars!! He thinks his \"logic\" is so magic it can defeat any fact!!! HE IS SO SMART HE CANT BE WRONG!!!!\n\nCan he?"
    author: "arse talker"
  - subject: "Re: Have you seen this?"
    date: 2004-02-14
    body: "\"Dont say that! He's working on a big contract with an airport for $15 million dollars!!\"\n\nI love the reading skills of some people! I am working on a contract for a local airport, and HOLY SHIT! I wish it was 15 million dollars. I think you're getting the 15 million dollars thing mixed up. i said this in relation to Ximian's venture capital outlay, which they have burned all the way through.\n\n\"HE IS SO SMART HE CANT BE WRONG!!!!\"\n\nI most certainly can be wrong, but I give reasons when I think I am right."
    author: "David"
  - subject: "Re: Have you seen this?"
    date: 2004-02-14
    body: "My, I've seen my name mentioned a lot here. I seem to have got a lot of desperate people over to this forum.\n\nUnfortunately, it doesn't say that they will be focusing on Ximian Desktop. It talks mostly about servers, with a very little footnote at the bottom about supporting the two desktops. If the time comes that one desktop will be picked it will be a decision left to developers primarily. The desktop is not a consideration for Suse/Novell at the moment. I would imagine this would mean a lot of collaborative work.\n\nEither way, in a year when Novell has made losses no one can justify the expenditure of Ximian in the long-term compared with the one or two developers Suse employs to work on KDE. Sorry, but that's the way it works kids.\n"
    author: "David"
  - subject: "Re: Have you seen this?"
    date: 2004-02-16
    body: "I hear SUSE employs 13 persons full time to work on KDE, and has been doing so for a long time. I'm afraid you're spreading wrong facts.\n"
    author: "David Faure"
  - subject: "Re: Have you seen this?"
    date: 2004-02-16
    body: "It's actually 13 (roughly, I'm not sure exactly) SUSE employees that have KDE CVS account. That doesn't mean there are 13 SUSE employees working full time on KDE.\n"
    author: "Lubos Lunak"
  - subject: "Re: Have you seen this?"
    date: 2004-02-18
    body: "No, not the way I understand it. Suse employs one or two people and lets others work on it in their spare time. Anyone from Suse actually care to confirm?"
    author: "David"
  - subject: "Re: Have you seen this?"
    date: 2004-02-18
    body: "It seems that currently 3-5 are working full time on KDE related stuff."
    author: "Anonymous"
---
As a followup to <a href="http://dot.kde.org/1073921982/">our previous Novell/SUSE article</a>, we have further good news.  Following the completion of the acquisition of <a href="http://www.suse.com/">SUSE</a> by <a href="http://www.novell.com/">Novell</a>, SUSE CEO Richard Seibt, who had previously expressed a strong commitment to KDE, <a href="http://www.suse.com/us/company/press/press_releases/archive04/novell_executives_emea.html">has been promoted</a> to president of Novell-EMEA and is now in a position to not only to maintain <a href="http://www.novell.com/coolsolutions/nnlsmag/features/tips/t_suse_kde_nls.html">SUSE's strong KDE support</a> but also to help deploy it more widely around the world.  Even more interesting, SUSE R&amp;D Vice President Markus Rex <a href="http://www.suse.com/us/company/press/press_releases/archive04/suse_bu.html">will now be responsible</a> for Novell's Linux desktop activities and not solely Miguel and Nat as <a href="http://www.mail-archive.com/mono-list%40lists.ximian.com/msg04360.html">previously hinted</a>. <a href="http://com.com/">com.com</a> is reporting <a href="http://news.com.com/2100-7344_3-5151359.html">further details</a>.


















<!--break-->
