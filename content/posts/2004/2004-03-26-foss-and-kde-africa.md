---
title: "F/OSS and KDE in Africa"
date:    2004-03-26
authors:
  - "uthiem"
slug:    foss-and-kde-africa
comments:
  - subject: "Interesting how FOSS"
    date: 2004-03-25
    body: "is spreading over the world. I think it will free countless people from using pirated software. That is certainly happening in China and India and many other places in the developing world where the Microsoft tax is way too much. "
    author: "anon"
  - subject: "printing"
    date: 2004-03-25
    body: "Deleting an active print job NEVER works, on Linux or Windows.  What is so hard about deleting a print job?  I've never understood how existing software can fail so horribly at such a simple task."
    author: "Spy Hunter"
  - subject: "Re: printing"
    date: 2004-03-26
    body: "buffering?"
    author: "cies"
  - subject: "disappointed"
    date: 2004-03-26
    body: "i was recently for a trip in botsuana/africa and met a lot of IT-people. it was very disappointing for me how much these people are dedicated to m$. \n\nthe reasons for that are the same as here in germany plus a very weak telco infrastructure (regulated of course, in the beginning of being deregulated).\n\nnevertheless i managed convice one university prof to try linux so i sent him two knoppix cds by snail mail afterwards my trip."
    author: "ac666"
  - subject: "Re: disappointed"
    date: 2004-03-26
    body: "why are you disappointed? what's so bad about microsoft? If they can get their work done with the tools they got, it's alright, no matter if its IBM, Microsoft, Linux or everything else.\n\nA few things to think about:\n\n\"There is no best tool\", as in \"there is no best car\".\nIf you are used to something, why switch?\n\n\n"
    author: "Timo A. Hummel"
  - subject: "Re: disappointed"
    date: 2004-03-26
    body: "oh, no flamewar please...\n\n- botsuana is a developing country IT-wise, so, it would be good to teach them how to help themselves, this is easy with F/OSS, instead of moving to a dependency to m$\n\n- m$ uses the same unfair practises like in india, first Mr. Gates visits the country and spends money for fighting aids and then one month later his company is giving \"seminars\" at the university which is more or less a sales event for \"biased\" customers\n\none example about how work is done bad with m$:\nthere is a internet cafe @ gabarone, botsuanas capital city, they have a leased line with a bandwidth comparable with isdn and approx. 10 computers for their customers. everyone is complaining about the slow net but web based mail (eg. yahoo.uk) is their main application. so - what about using text based browsers or even a browser like opera which can switch the graphics off? no, they are using IE.\n\nsorry for my bad english\n\n"
    author: "ac666"
  - subject: "Re: disappointed"
    date: 2004-03-26
    body: "Well, they usually don't customize their PCs very much and don't update at all. Guess what this means from a securtiy point of view.\n\nIt's due to the lack of fast internet connections and old PCs. they use PCs as they are.\n\nThey don't have a computer at home for years, so only the upper class is equipped with the euipment they can get. Evrybody is proud to get an IT education to word and Excel, a must for a white collar job.\n\nIT education means lowtech IT usage.\n\nSo also MS is not very \"successful\" in Africa. when you want to help, sent your old Linux books to an African University. I saw an african bookstore, you can buy books about dbase there, documentation is outdated over there. they simply don't get what is going on the FLOSS world. Imagine you only had a buggy los speed internet connection as back in 1994. Linux on 20 floppy disc back then."
    author: "Bert"
  - subject: "Re: disappointed"
    date: 2004-03-26
    body: "my point of view on it is this: developing nations have enough issues in front of them that choosing closed source solutions is extremely short sighted and in the long run very bad for them, not unlike choosing to farm with Western-produced \"terminator\" seed. the issues in particular include:\n\no economic strength. it doesn't make sense to throw money at foreign software companies when they can get it for no cost, especially when that free software is modern and can be supported by local industry.\n\no information and \"IP\" disparity. to further subjugate their information to closed formats, DRM schemes, etc. especially when they are not even a stakeholder in those restrictive technologies seems quite imprudent. witness where the national delegates opposing overly restrictive copyright laws come from, and for the same reasons (hint: Africa)\n\no technological parity. why bother with software that does not come in their own language(s) and does not allow them to alter and observe (and therefore learn from) the technology they use?"
    author: "Aaron J. Seigo"
  - subject: "Re: disappointed"
    date: 2004-03-26
    body: "Well, I might be making assumptions, but I don't think I'm going out on a limb here in saying that a great amount of software in Africa is pirated. At my university (in the USA) the biggest competitor to Linux is pirated Windows (only amongst students of course). Its sort of ironic that Microsoft piracy is probably one of the bigger factors limiting Linux's growth. \n\nSo, like the other posters are saying, the reason Africa and folks in general should use F/OSS is not due so much for technical reasons. The last thing Africa needs is to be sending more money out of their countries. If using F/"
    author: "Ian Monroe"
  - subject: "Re: disappointed"
    date: 2004-03-28
    body: ">what's so bad about microsoft?\n1) it costs. Too much. Way too much for a developing country in which the mean monthly wage is perhaps a very small fraction of the price of a ms software package\n2) for roughly 5bi-250mi people in this world, ms is a foreign company, which means each penny paid to it it's definitely lost (extracted from the country's economy)\n3) MS software usually requires machines that will be produced in the next year. Which aren't cheap.\n4) most of the time MS software isn't localized/localizable\n\netc.\n\n> If they can get their work done with the tools they got, it's alright..\n\nWell, the problem is that they can't get their work done, or it costs them too much (either financially or morally).\n\nYour assesions are based on a false premise: you infer that most people have all the information they need to choose the appropriate tool for the job. Or the car that suits them. Well, surprise, even the most informed people of this world miss a huge portion of the available information.\n\nWell, I come to partially agree with you, it's not Linux that has to be pushed at the people in developing countries, but information. Guess, what exact \"product\" is the most defended by riches? Right! Information. No need to say why.\n"
    author: "Inorog"
  - subject: "Re: disappointed"
    date: 2004-03-26
    body: "Oh? In Germany KDE adoption is very high compared with other regions of the world. national 300 Lugs, strong network of developers with so many events...\n\nIn Africa win95 is still very strong :-) Tell them that KDE is the future. There is also a lack of documentation. Sent a modern software package to a African piracy dealer and we will see Linux on the market in Botswana as well. The problem with Linux in Africa is the Internet and the low connection rate. You need a fast download and internet access for Linux development and administration."
    author: "Bert"
  - subject: "Re: disappointed"
    date: 2004-03-26
    body: "While Africa isn't just deserts (though we in Namibia have got our fair share of them) and jungle, different regions of Africa differ in other respects as well. Here in Namibia, Windows mostly means 2000 or XP. \n\nTrue, the high costs of bandwidth is a hindrance but again, it's very different in different countries. In Namibia, we have an IP backbone that more or less covers the country. We are in the process of setting up a wireless network covering the more rural areas in the north.\n\nActually, the constrains on our bandwidth isn't the most important hindrance for Linux to be adopted - or for ICTs to be adopted for that matter. High hardware costs are a key factor. Knowledge and skills are another one.\n\nHere is a plan: When you buy a computer in Europa, a certain amount of the price is meant for \"recycling\" the box. Ship tons of outdated PIIs plus that amount of money to Africa. We refurbish the boxes and give them another 3 to 4 years of lifetime. Afterwards we use the money to \"recycle\" them in an orderly way.\n\nWe actually do that already. Schools get a fat server and 10 to 20 of those refurbished boxes as thin clients. Linux-based, of course. Works so far."
    author: "Uwe Thiem"
  - subject: "Re: disappointed"
    date: 2004-03-26
    body: "Do you use the Skandinavian SkoleLinux? They created a idiot-proof School Linux system based on debian and are also aware of the old hardware problem."
    author: "Bert"
  - subject: "Re: disappointed"
    date: 2004-03-28
    body: "Nope. We use a South African distribution that gives us exactly what we need. Since this is mostly for schools, content is important and content for Africa differs from what you need in Scandinavia. Just think geography for starters. ;-)\n\n"
    author: "Uwe Thiem"
  - subject: "Re: disappointed"
    date: 2004-03-26
    body: "i thought it was spelled Botswana? *looks it up* yep:\n\nhttp://cia.gov/cia/publications/factbook/geos/bc.html\n"
    author: "Aaron J. Seigo"
  - subject: "Back again"
    date: 2004-03-28
    body: "Hello Uwe, good old friend. I'm very happy to hear from you again. Congrats for the great job you continue to do up there ;-)\n"
    author: "Inorog"
  - subject: "It has started"
    date: 2004-03-28
    body: "FLOSS is inevitably going to take over here in Africa. I live in South Africa. Linux is making headway here. I'm hearing more and more people talking about it, more and more companies considering ditching MS for Linux (many having done so already), and I even heard that the SA government has decided to switch from MS. I was walking in one of the largest grocery chains' stores late last year, and noticed KDE running on one of their desktops behind a counter. My father's civil engineering firm is also considering Linux.\n\nSure, bandwidth is crappy here, but I have downloaded every release ISO of FreeBSD since 2001 on a 64K isdn connection on which I get 7Kb/s on a good day. Companies have way better connections. FreeBSD and Linux (and more) are available on local FTP sites. Getting it is not that hard.\n\nThe problem is just that Africa (my point of reference is SA, which is one of the most developed countries on the continent) has always been behind, especially in IT. It's just a matter of time until FLOSS takes over here. Especially since it offers such cost benefits to 3rd-world countries. It can't be stopped, because it is simply better. I knew this 3.5 years ago when I first ditched Windows, and had heated debates with my unenlightened Windows-using friends. But I knew I was right, and they are starting to realise that now.\n\nP.s. I don't mean to flame, but why was this thing held in Namibia? Cape Town or Johannesburg probably each has more potential FLOSS users than all Namibia has PC owners."
    author: "anonymous"
  - subject: "Re: It has started"
    date: 2004-03-28
    body: "When we talk about Africa we usually don't mean Morocco, Egypt ecc. and South Africa is it's own continent.\n\n"
    author: "Bert"
  - subject: "Re: It has started"
    date: 2004-03-28
    body: "Well, Namibia may be smaller (population-wise) than South Africa, but demographically and economically they are quite similar. And since the article mentions Nambia, I think SA is relevant.\n\nBasically my point was that SA is one of the IT leaders on the continent, and FLOSS is just starting to pick up here, and that the rest of Africa will follow in time.\n"
    author: "Anonymous"
  - subject: "Re: It has started"
    date: 2004-03-29
    body: "who is we? Your comments just goes to show why some of u non-indigenous africans just fuel mugabe types. Let africans decide what is africa is. This is a public kde forum and so keep the comments positive.\n\nBut u are right.SA is still two continents, africa and africans-in-denial like you. grow up, that 'era' is long gone. \n\n"
    author: "mukuka"
  - subject: "Re: It has started"
    date: 2004-03-29
    body: "This is flamebait, right?\n\nWell, in case you were actually being serious, tell me what part of my post was negative? I actually think it was very positive WRT the issue under discussion.\n\n\"That era\"? You have got to be joking, right? Why bring that up? What has that got to do with me or this forum? You don't know a thing about me. Go vent somewhere else.\n\nBut you are right. That 'era' is long gone. So GET OVER IT."
    author: "anonymous"
---
54 people from 15 African countries and 16 facilitators/helpers from
outside Africa gathered in Okahandja, a small Namibian town, for
<a href="http://www.tacticaltech.org/africasource">African Source</a>
from 15th of March to 19th of March 2004. African
Source was the first all African conference of Free Software/Open
Source Software (F/OSS) developers. Vladimir Petkov (GNOME) and
<a href="http://www.uwix.iway.na/uwe.html">Uwe Thiem</a>
(KDE) presented the current state of the open source desktop, 
its strengths and shortcomings.


<!--break-->
<p>
Main objective of the meeting was to get African developers together
to discuss the possibilities of F/OSS for developing countries. Hot
topics were: Wireless networking, special software like microfinance
applications, refurbished computers, total cost of ownership (TCO)
versus pirated proprietary software, thin client solutions and, of
course, the open source desktop.
</p>
<p>
Vladimir and Uwe agreed early on not to do the usual GNOME vs. KDE
thing. Instead, they presented the common strengths and shortcomings
of both desktops. Attendants agreed that
<a href="http://www.cups.org/">CUPS</a> and its integration in
either desktop bridged the gap in printing quality between F/OSS and
any proprietary operating software but wasn't robust enough to suit
users. Deleting an active print job can easily end up in a nightmare.
They also agreed that both desktops are generally ready to fit into
any business or 
<a href="http://www.ngos.net/">NGO</a> environment.
Good accounting software was listed as a serious shortcoming.
</p>
<p>
The computer lab of 25 boxes at African Source was powered by
<a href="http://www.direqlearn.org/">OpenLab 3</a>, a South African linux distribution geared at educational institutions, using KDE 3.2.
</p>
<p>
All participants agreed on African Source 2 later this year, tailored
at the needs of users, businesses and NGOs.
</p>


