---
title: "Konstructing a New KDE Desktop"
date:    2004-04-27
authors:
  - "binner"
slug:    konstructing-new-kde-desktop
comments:
  - subject: "nice tutorial! what about binary KDE releases?"
    date: 2004-04-27
    body: "Thanks, nice tutorial\nI've been using konstruct a couple of times and is really really easy.\nWouldn't it be nice to have a binary-only demo/release for each KDE minor version?\nMainly for Linux x86, but the same is possibile for other platforms.\nSomething like a tarball that goes into /opt/kde.x.y, with very few dependencies like minimum kernel, glibc and X version.\nSomething like Mozilla or OpenOffice for instance.\nI know that package systems/manager are better, especially in production environments where security fixes may be important. \nBut having a demo of the whole KDE at each release could be useful (for quick bug tracking or PR for instance).\nIs it something worth, possible or am I an idiot?? ;-)\nComments are welcome.\n"
    author: "Ste"
  - subject: "Re: nice tutorial! what about binary KDE releases?"
    date: 2004-04-27
    body: "Kongratulations, it's kool."
    author: "homre"
  - subject: "Re: nice tutorial! what about binary KDE releases?"
    date: 2004-04-27
    body: "> Wouldn't it be nice to have a binary-only demo/release for each KDE minor version?\n\nA solution without any dependencies would be a small Live-CD, like Slax already is."
    author: "Anonymous"
  - subject: "Re: nice tutorial! what about binary KDE releases?"
    date: 2004-04-27
    body: "Also a live CD could be nice, and there is one already I see\nI was thinking of something distro-independent that you can easily install on disk with only the latest KDE stuff\n"
    author: "Ste"
  - subject: "Re: nice tutorial! what about binary KDE releases?"
    date: 2004-05-09
    body: "take a look at byzantine linux: <a href='http://byzgl.sourceforge.net/'>here</a>"
    author: "alimoe"
  - subject: "Re: nice tutorial! what about binary KDE releases?"
    date: 2004-04-27
    body: "Hello!\n\nIf you install all of the dependencies for KDE, it is a lot more stuff than KDE itself.  I know this because I recently did a new install of Linux.  This was Linux Mostly From Scratch, so I started with only GLibc and BinUtils built from source and only the few RedHat RPMs needed to boot to the console.  After that everything was installed individually either from RPM or built from source.  So, I know all of the dependencies that I installed to get KDE up and running.\n\nThis would probably fit on a CD, but downloading it all as a tarball is probably out of the question.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: nice tutorial! what about binary KDE releases?"
    date: 2004-04-27
    body: "sure?\ntake a look at the SLAX CD\nhttp://slax.linux-live.org/features.php\nhttp://slax.linux-live.org/installed_packages.txt\nall in 170 MB\n\nanyway can you mail me your dependency list?\nthanks"
    author: "ste"
  - subject: "Re: nice tutorial! what about binary KDE releases?"
    date: 2004-04-27
    body: "A CD with all KDE packages would be of course slightly bigger."
    author: "Anonymous"
  - subject: "What about a permanent install"
    date: 2004-04-27
    body: "First about the problem with \"something\" overwriting the PATH.\n\nWith the Xsession script produced by the KDM installation, the problem is that you shouldn't source \"~/.bash/env-konstruct.sh\" from: \"~/.bashrc\".  It should be sourced from: \"~/.bash_profile\" (on Linux assuming that Xsession is using Bash to run itself).  Note that older versions of KDM may require a link:\n\n\t~/.profile -> ~/.bash_profile\n\nThis should fix the problem without having to edit the scripts.  If it doesn't, it is probably caused by problems with KDM setting the PATH.  KDM tries to add to the PATH and this simply doesn't work correctly.  If you are properly adding: \"KDEDIR/bin\" to your path either in 'profile' or a 'profile.d' script then you should edit these two lines in the: \"[X-*-Core]\" section of your global: \"kdmrc\" file (not the one installed with Konstruct) to read:\n\nSystemPath=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin\nUserPath=/bin:/usr/bin:/usr/local/bin\n\nBut, the big question is what if you want to permanently install the new KDE.\n\nThere are two issues.  \n\n1. Konstruct installs Qt in the wrong place.\n\n2. You need to globally set the various environment variables if you want to use KDE for all user accounts.  On Linux, this should use 'profile.d' scripts.\n\nThe tutorial needs to include this since this is one of the problems that users ask about on the support lists.\n\nI have issues with the Xsession produced by script that I would include in the tutorial, but I think that it will work even though part of it is a kludge.\n\nI also wondered about the fonts issue.  KDE only installs 3 fonts:\n\n\t9x15.pcf.gz\n\tconsole8x16.pcf.gz\n\tconsole8x8.pcf.gz\n\nwhich will be installed in: \"/opt/kde3.2.1/share/fonts/\" and (on Linux at least) KDE does not use the X fonts system but now uses FontConfig.  When using FontConfig to find the fonts, the various stuff in the: \"startkde\" script that sets the X font path is useless.  All that is needed to find fonts is for the directories to be added to: \":/etc/fonts/local.conf\".  Add as the next to last line:\n\n\t<dir>/opt/kde3.2.1/share/fonts</dir>\n\nand execute as root: \"fc-cache -vf\".\n\nAlso, is there some reason that you are installing in: \"/opt/\"? rather than:\n\"/usr/kde-<version>/\"?  It appears to me to be a bug in Konstruct that you have to edit the script to change the prefix.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: What about a permanent install"
    date: 2004-04-27
    body: "> 1. Konstruct installs Qt in the wrong place.\n \nwhat's wrong?\n\n> Also, is there some reason that you are installing in: \"/opt/\"? rather than:\n> \"/usr/kde-<version>/\"? \n\ncompliance with Filesystem Hierarchy Standard?\nhttp://www.pathname.com/fhs\n\n> It appears to me to be a bug in Konstruct that you have to edit the script to change the prefix.\n\nfrom the konstruct home page:\n\"By default Konstruct installs to your home directory which means you don't have to possess root privileges or risk to damage your system or affect another KDE\""
    author: "ste"
  - subject: "Re: What about a permanent install"
    date: 2004-04-27
    body: ">> 1. Konstruct installs Qt in the wrong place.\n \n> what's wrong?\n\nThe normal place is: /usr/local/qt<version>/\n\nNot having it in the standard place might cause problems.  Just a minor point but if you are making a global install, it is best to put it in the correct place and there is no reason to put it somewhere else.\n\n>> Also, is there some reason that you are installing in: \"/opt/\"? rather than:\n>> \"/usr/kde-<version>/\"? \n \n> compliance with Filesystem Hierarchy Standard?  \n> http://www.pathname.com/fhs\n\nStrange, the default KDE install is not in compliance with the FHS so it would appear more logical to conform to the KDE default than the FHS.  Put this is just a personal preference.  The FHS is ugly and lots of stuff doesn't conform to it and I see these as an improvement.\n\n>> It appears to me to be a bug in Konstruct that you have to edit the script >> to change the prefix.\n \n> from the konstruct home page:\n> \"By default Konstruct installs to your home directory which means you don't \n> have to possess root privileges or risk to damage [sic] your system or affect  \n> another KDE\"\n\nAre you agreeing with me?  My point is that you shouldn't have to edit the script.  It should be a command line paramater.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: What about a permanent install"
    date: 2004-04-27
    body: "> The normal place is: /usr/local/qt<version>/\n\nwho decided that?\nand why\n\n> Are you agreeing with me? My point is that you shouldn't have to edit the script. It should be a command line paramater.\n\nyes it could be, why not both\n"
    author: "ste"
  - subject: "Re: What about a permanent install"
    date: 2004-04-28
    body: ">> The normal place is: /usr/local/qt<version>/\n \n> who decided that?\n\nTrolltec says that in their INSTALL file included with the source code.  Actually they suggest you don't use a version, but the common KDE wisdom is that it works better with a version.\n\n> and why?\n\nWhy did they decide on that location? or Why should you use the standard location?  \n\nYou should use the standard location to avoid problems.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: What about a permanent install"
    date: 2004-04-27
    body: "> 1. Konstruct installs Qt in the wrong place.\n\nThere is no wrong place if you have limited rights. And why do you moan about Qt? It's by far not the only supporting library installed by Konstruct.\n\n> It appears to me to be a bug in Konstruct that you have to edit the script to change the prefix.\n\nWhat script? gar.conf.mk? Never heard of the \"configuration file\" concept?"
    author: "Anonymous"
  - subject: "Re: What about a permanent install"
    date: 2004-04-30
    body: "> There is no wrong place if you have limited rights.\n\nIf you are doing a *global* install, it is presumed that you do NOT have limited rights.\n\n> Never heard of the \"configuration file\" concept?\n\nYes, but it is still editing a file.  Using the command line is easier.\n\n--\nJRT"
    author: "James Richard Tyrer"
---
Dan Allen has written a <a href="http://www.mojavelinux.com/articles/konstruct.html">tutorial</a> on <a href="http://developer.kde.org/build/konstruct/">Konstruct</a>, a build utility that eases the process of building KDE releases from source by downloading tarballs and collating the required compilation steps.




<!--break-->
