---
title: "PIM Team at Chemnitzer Linux-Tag 2004"
date:    2004-03-02
authors:
  - "dmolkentin"
slug:    pim-team-chemnitzer-linux-tag-2004
comments:
  - subject: "Great !"
    date: 2004-03-02
    body: "I welcome the efforts of the KDE PIM team and really want to \"egg on them\":\nKontact is going to be a beasty killer app! Connect to eGroupware, connect to Kolab, connect to (enter your favourite groupware here)... How cool's this, folks?!\n\nWhat I like most is the history of this project. Watching once separated apps evolve into one mature component-based PIM suite is simply amazing. What the KDE developers have done here is to actually reuse existing code by taking advantage of kparts in best unix fashion. This gives the impression of KDE and its technology being a solid foundation for scalable development. nice..."
    author: "Thomas"
  - subject: "Re: Great !"
    date: 2004-03-02
    body: "Yay! For a first real release I was impressed with Kontact and all the PIM tools in KDE 3.2. It's a pretty good advertisement for KDE programming."
    author: "David"
  - subject: "Wilhelmshaven"
    date: 2004-03-02
    body: "There is a parallel event in Wihelmshafen (Niedersachsen), close to the dutch border,\nhttp://www.lug-whv.de/\n\nDie LUG WHV e.V. veranstaltet am 6./7. M\u00e4rz 2004 in der Fachhochschule in Wilhelmshaven die 2ten GNU/Linux Informationstage 2004, kurz LIT 2004.\n\nWas wird den Besuchern alles geboten? \nVortr\u00e4ge rund um GNU/Linux und Freie Software (Open Source) \nLive Stream zu der Parallelveranstaltung in Chemnitz. \nWorkshops \nInstallationsparty \nAussteller, darunter Freie Projekte wie GNOME und KDE, aber auch Firmen die Freie Software einsetzen, supporten oder unterst\u00fctzen \nCatering ist vorhanden \nKostenlose Parkpl\u00e4tze direkt vor der Fachhochschule \nEs werden noch Aussteller und Referenten gesucht, ebenso wie Sponsoren die diese Veranstaltung mit unterst\u00fctzen. Interessenten melden sich bitte bei ralf.niemand@lug-whv.de\n\nHere the program of saturday, there willl be as many event s the next day, too.\n\nGro\u00dfer H\u00f6rsaal\n\nUhrzeit Thema Redner Firma/Projekt \n09:00 10:00 Begr\u00fc\u00dfung und Er\u00f6ffung Ursula Aljets 2. B\u00fcrgermeisterin \n10:00 11:00 Beispiele und Kriterien f\u00fcr den erfolgreichen Einsatz von GNU/Linux im \u00f6ffentlichen Bereich Peter Ganten Uninvention, LIVE e.V. \n11:00 12:00 SUN Java Desktop Thorsten Keller SUN Microsystems \n12:00 13:00 Erfolgreiche Migration durch Mitarbeiter-Integration Jan M\u00fchlig relevantive AG \n13:00 14:00 Samba 3.0 Volker Lendeke SAMBA, SerNET \n14:00 15:00 Kommunale Fachanwendungen als Killer Kriterium f\u00fcr einen erfolgreichen GNU/Linux-Einsatz in der \u00f6ffentlichen Verwaltung Niels Gr\u00fcndel Stadt M\u00fchlheim \n15:00 16:00 Cisco-Vortrag - (\u00dcbertragung von Chemnitz - live) N.N. Cisco (LUG Chemnitz) \n16:00 17:00 OnlineBanking mit HBCI und GnuCash (\u00dcbertragung nach Chemnitz - live) Christian Stimming TU-Hamburg-Harburg \n17:00 18:00 KDE PIM Till Adam KDE \n\n\n\nKleiner H\u00f6rsaal\n\nUhrzeit Thema Redner Firma/Projekt \n09:00 10:00 -- -- -- \n10:00 11:00 GNU/Linux un Free Software - een \u00d6ver- un Utblick \u00f6ver dat Ph\u00e4nomen Sven Kromminga FSF Europe \n11:00 12:00 SmartCards und GnuPG Werner Koch G10 Code, GnuPG \n12:00 13:00 Open Darwin Felix Kronlage Open Darwin \n13:00 14:00 \"Hercules\" Mainframes unter GNU/Linux Michael K\u00f6hne Copyleft.De \n14:00 15:00 Debian Hartmut Koptein Debian \n15:00 16:00 Internationalisierung, Barierefreiheit, Usability Sven Herzberg Uni Hamburg \n16:00 17:00 GNU/Linux: Vor der Installation Frank Slotta LUG WHV \n17:00 18:00 Migration zu GNU/Linux f\u00fcr KMU's Ingo Stierand Stierand Linux-IT \n  \n   \nWorkshop 1\n\nUhrzeit Thema Betreuer Firma/Projekt \n14:00 16:00 Samba-Workshop N.N. UNI Oldenburg \n\n\n\nWorkshop 2\n\nUhrzeit Thema Betreuer Firma/Projekt \n10:00 16:00 Friesatlon (KDE tranlation to Plattd\u00fc\u00fctsch (=Low Saxon, iso code nds)\n\n\nKMU Spezial\n\nUhrzeit Thema Betreuer Firma/Projekt \n15:00 18:00 Round-Table - Gespr\u00e4ch LUG WHV LUG WHV \n  \n   \n\n"
    author: "Bert"
  - subject: "Plone"
    date: 2004-03-02
    body: "Is it also possible to create an interface to Plone? "
    author: "Andre"
  - subject: "Re: Plone"
    date: 2004-03-03
    body: "I think that it should be. I think that xmlrpc is being used so why not ask for help on the dev@plone mailing lists ?"
    author: "CPH"
  - subject: "pim/synchronizing"
    date: 2004-03-03
    body: "reading through the kde-pim mailing lists and projects on sourceforge, there seem to be many projects dedicated to getting devices to sync with linux/kde/kontact.\n\nThere are at least kitchensync, kpilot, synce, multisync, kandy, synce-KDE. Did I miss any?\n\nCould someone knowledgeable please tell me which projects are doing what? Which have just been renamed, which are plugins to others, who is cooperating? What is gnome doing, something completely different, anything at all? Which one is supporting what devices?\n\nthanks!"
    author: "anonymous"
---
<a href="http://www.tu-chemnitz.de/linux/tag/2004/">Chemnitzer Linux-Tag 2004</a>, Germany's second largest Linux community event takes place March 6 and 7 on the campus of the <a href="http://www.tu-chemnitz.de/tu/sprachen/engl/">University of Technology</a> in Chemnitz, Germany. Other than offering an exhibition, workshops and talks, it will also be the hosting platform for a meeting of members of the <a href="http://pim.kde.org/">KDE PIM</a> team as well as members from the groupware projects <a href="http://www.egroupware.org/">eGroupware</a>, <a href="http://www.exchange4linux.com/">Exchange4Linux</a>, <a href="http://kolab.kde.org/">Kolab</a>, and <a href="http://www.opengroupware.org/">OpenGroupware</a>. KDE will also have a booth in the exhibition area.
<!--break-->
<p>
The PIM meeting will be used to discuss integration of groupware servers into the KDE PIM Framework and working on other features listed on the <a href="http://developer.kde.org/development-versions/kdepim-3.3-release-plan.html">
feature plan</a>, which will eventually trigger the release cycle for the pending KDE PIM 3.3 release.
<p>
Hope to see you in Chemnitz!