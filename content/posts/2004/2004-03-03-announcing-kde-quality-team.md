---
title: "Announcing the KDE Quality Team"
date:    2004-03-03
authors:
  - "cwoelz"
slug:    announcing-kde-quality-team
comments:
  - subject: "More to come on press & promotion work"
    date: 2004-03-02
    body: "Just a note to let people know that I have some stuff in the works on press & promotion work, specifically:\n<p>\no How to write press releases, and where to send them<br>\no How to write a good article on a KDE application<br>\no How to get work into online/paper publications<br>\n<p>\nSo if you've ever considered a career in journalism, or wanted to do some freelance media work on the side, or even if you just like the idea of promoting KDE apps, then watch this space :-)\n<p>\nAlso, people might be interested in an article I wrote on this project:\n<p>\n<a href=\"http://www.newsforge.com/technology/04/03/01/1511242.shtml\">http://www.newsforge.com/technology/04/03/01/1511242.shtml</a>\n"
    author: "Tom Chance"
  - subject: "Re: More to come on press & promotion work"
    date: 2004-03-03
    body: "I like the article, well thought out, though I don't think you should have mentioned the programmer stereotype, be it true or not."
    author: "Alex"
  - subject: "Re: More to come on press & promotion work"
    date: 2004-03-03
    body: "Heh, the funny thing is that I didn't even say that all or a majority of hackers were \"scientific atheists\", nor did I imply that theists are unscientific, but I suppose that sort of reaction is always going to come from some people ::)"
    author: "Tom Chance"
  - subject: "Re: More to come on press & promotion work"
    date: 2004-03-05
    body: "Well, there are a few problems here -- first it should have been \"scientific, atheistic\" rather than the above form to make it clear that they were independent attributes.\n\nThe proper term would have been \"agnostic\" since \"atheistic\" implies an active disbelief in the supernatural rather than the more common simple lack of belief one way or the other.\n\nAnd also there are a fair number of religious folks in the KDE community; I don't think that it's a useful generalization to say that KDE substitutes for religious belief aside from the social function that organized religions provide.  But there KDE is more like a chess club than like an organized religion.\n\nAnd to finish off the mini-rant -- you're using the term \"spiritual\" to describe \"stuff that people just enjoy doing\".  I don't think that those in the community that consider themselves \"spiritual\" would enjoy the analogy (since I've rarely heard a person explain their religion in terms of \"I just kind of like it\") and those that aren't likely resent the term.  ;-)"
    author: "Scott Wheeler"
  - subject: "excellent"
    date: 2004-03-02
    body: "This is very good stuff.  Are there any instructions for developers, in order to have their module/application added to the Quality Team project?  Right now, it looks like only KDEPIM is available for interested QT volunteers to work on.\n"
    author: "LMCBoy"
  - subject: "Re: excellent"
    date: 2004-03-02
    body: "Hi,\n\nif you want to help, please join our mailing list\n\nhttp://quality.kde.org/contact/mailinglist.php\n\nYou don't have to be a programmer, but of course programmers are also welcome. \nVolunteers can work on any part of KDE they want, as it has always been :-)\nIt's just that for the KDE PIM project there have been some goals and tasks collected and written down.\n\nBye\nAlex\n"
    author: "aleXXX"
  - subject: "Re: excellent"
    date: 2004-03-02
    body: "We decided to start with KDE PIM because of the shorter release cycle, so things are more urgent there. But we hope to expand the coverage to more mudules, making easier to new contributors to select what ot do.\nAlso, anyone can create a team page for his module / application, and announce it in the mailing list, I will gladly update the website with this new information."
    author: "Carlos Leonhard Woelz"
  - subject: "Re: excellent"
    date: 2004-03-03
    body: "We already started an 'Open Tasks' page in KDE-Edu so that could be linked from the Quality main page. \nWe want to focuse on adding WhatThis help in each app, porting some apps to some more modern code, fixing bugs by systematically going through the bug database, improving doc (taking screenshots is an example of a non-developer task). Creating icons and pics for specific apps are also wanted tasks. There is plenty to do and everyone can find something to do in order to help improving KDE. Checking that every string is in proper english and spelled correctly is also a global task that can be done by native english people.\nI hope many people will join and make a difference.\n"
    author: "annma"
  - subject: "Extentions of the old jobs place"
    date: 2004-03-02
    body: "Looks like a great source for new KDE contributers, just like the old \"jobs\" page used to be on kde.org (but hasn't been actively used since 2001 or so)"
    author: "anon"
  - subject: "Re: Extentions of the old jobs place"
    date: 2004-03-02
    body: "That's why there's also http://wiki.kdenews.org/tiki-index.php?page=Developers+Wanted"
    author: "Anonymous"
  - subject: "Pardon me..."
    date: 2004-03-02
    body: "...but this is a ******* brilliant initiative, and it is sure to get more people involved. Now when I get this wireless network and server infrastructure up and running here I'll definitely be reading through this. Perhaps one of the problems for some are the lack of broadband access, and not knowing how to get involved and try things out otherwise. It has been for me."
    author: "David"
  - subject: "Re: Pardon me..."
    date: 2005-03-14
    body: "pardon"
    author: "lucas"
  - subject: "outdated screenshots specs?"
    date: 2004-03-02
    body: "on http://i18n.kde.org/doc/screenshots.php it says:\n\"To give our documentation a corporate look we have decided on ONE theme/style for all screenshots:\nWindow decoration: Keramik\n Widget style: Keramik\n Colors: Keramik\n Background: Flat color - Color must be white\"\n\nshouldn't that be Plastik now? :)"
    author: "Pat"
  - subject: "Re: outdated screenshots specs?"
    date: 2004-03-02
    body: "And you will make screenshots for all languages for all applications ? because I don't think that each i18n team want to do another series of screenshots for all the localized docs :)"
    author: "mooby"
  - subject: "Re: outdated screenshots specs?"
    date: 2004-03-03
    body: "OK, this has been mentioned before, but why isn't there an automated tool for taking screenshots? Then you set the script and walk away. Computer hums away while you are at work and when you get back there is a big old batch of screenshots ready to be put into files. Hell, match that up with an xml log and an xslt to transform to docbook and then do docbook to pdf. Now you have docbook, pdf documentation ready to go. \n\nI am not able to code this so, I'll shut up now. (But I might be able to help with the XML, XSLT part of things)"
    author: "porter235"
  - subject: "Re: outdated screenshots specs?"
    date: 2004-03-03
    body: "This is a very good idea. Would this be very difficult to implement? If the standard KDE framework could provide this functionality as part of a standard test environment a lot of time could be saved and used to improve programs. I envisage a tinderbox like setup that would continuously rebuild and run kde and all apps in all languages creating snapshots as it goes. This could also be used to catch simple errors and could regularly give all the saved pictures a once over to check if everything still looks good.\n\n(usual disclaimer: don't have time, kde programming skills or broadband access myself)"
    author: "Didi"
  - subject: "Re: outdated screenshots specs?"
    date: 2004-03-02
    body: "No, because Keramik is still the default style..."
    author: "Henrique Pinto"
  - subject: "Re: outdated screenshots specs?"
    date: 2004-03-02
    body: "yes, but plastik is the defacto default among users :)"
    author: "John"
  - subject: "Re: outdated screenshots specs?"
    date: 2004-03-02
    body: "There was some talk of making it the default default for 3.2 on this site. It seems to be the case they will wait for at least 3.3 but perhaps 4.0 before making changing the default theme, so people aren't thrown off."
    author: "Ian Monroe"
  - subject: "Re: outdated screenshots specs?"
    date: 2004-03-03
    body: "There will be no change of the default style until 4.0."
    author: "Peter Simonsson"
  - subject: "Good"
    date: 2004-03-03
    body: "Keramik looks good, it can be refined, but it looks good and there is no point to change it so fast."
    author: "Alex"
  - subject: "Re: Good"
    date: 2004-03-04
    body: "Keramik is like Luna. A few people like it, but among users, there is a very large-scale tendency to immediately switch to a different style. I understand the logistical problem of changing the default style, and agree that we shouldn't change it during point-releases. However, I actually think Keramik is hurting KDE a bit by making KDE screenshots much less appealing then they could be. I have to say I was relieved when the \"Deep Inside KDE\" article came out with the screenshots in Plastik.  \n\nKeramik also has some practical problems other themes don't. For example, the gradient toolbar looks strange. You've got what appears to be a curved surface with flat icons balanced on it at one point of contact. Further, it interacts badly with the \"toolbar crawl\" present in all KDE apps when they are resized. Styles that don't have a toolbar background (.NET, ThinKeramik) or have a very subtle one (Alloy, Plastik) make this effect nearly unnoticable. Keramik's toolbars are so distinctive that it accentuates the effect! People might consider this trivial, but people who don't understand the code can easily interpret this to mean that \"KDE is slow.\""
    author: "Rayiner Hashem"
  - subject: "On /."
    date: 2004-03-02
    body: "http://developers.slashdot.org/article.pl?sid=04/03/02/1924204"
    author: "ac"
  - subject: "Just what is needed!"
    date: 2004-03-04
    body: "An exellent initiative.\n\nI am still a newbie and want to contribute to KDE (KOffice in particular) and during these past few months I have struggled with some of the things that are now very clearly explained on the web-page. \n\nI have only notions of programming in C++ and I am a complete beginner in Qt, so my contributions in that area will be limited. Nice to see that there is now an additional platform to discuss what needs to be done and how it can be done efficiently. I'm sure it will enable us to exchange information in a better way.\n\nMarc"
    author: "Marc Heyvaert"
  - subject: "script fails"
    date: 2004-03-04
    body: "On http://quality.kde.org/develop/cvsguide/buildstep.php#step5 the script to compile single KDE-modules fails:\n\nkdedev@ip172:~/src/kde/log> more kdelibs-20040304.log\n./module-build.sh: line 7: configure: command not found\nmake: *** No targets specified and no makefile found.  Stop.\nmake: *** No rule to make target `install'.  Stop.\n\nI think the correct module-build.sh script would be:\n\nkdedev@ip172:~/src/kde> cat module-build.sh\nbuilddir=\"/home/kdedev/src/kde/build/\"$1\nlogfile=\"/home/kdedev/src/kde/log/\"$1\"-\"$(date +%Y%m%d)\".log\"\nmoduledir=\"/home/kdedev/src/kde/\"$1\ncd $moduledir\nmake -f Makefile.cvs >> $logfile 2>&1\ncd $builddir\n$moduledir/configure --enable-debug > $logfile 2>&1\nmake >> $logfile 2>&1\nmake install >> $logfile 2>&1\n\nWorks for me!\n\nPatrick"
    author: "Patrick Smits"
  - subject: "At last, QA for KDE"
    date: 2004-03-04
    body: "I am, of course, very happy to see this.\n\nThere are additional issues that need to be addressed.  The most important of these is regression testing.  In KDE-3.2.0 there were serious regressions that were not caught and/or almost not caught.  Specific example was bug 73379.\n\nBug 73379 also illustrates some of the problems that the Quality Team faces -- problems with developers.  Various excuses were made as to why the bug wasn't important and then it was closed before it was fixed.\n\nIt has now been fixed (and fixed in KDE_3_2_BRANCH) but it should have been caught and fixed before the release of 3.2.0.  This bug was a regression; proper regression testing would have caught it.  Now, the test cases should be added to a regression testing data base.  So that even after it is marked \"VERIFIED\" if it is still fixed in the next stable release (3.2.1) that future versions will still be tested for this.\n\nAlso important is the \"won't fix\" problem.  Yes the maintainer is in charge of his module, but if QA is going to work, he can not arbitrarily use this authority to refuse to fix bugs.  Bug 53345 (which had previously been closed without fixing) is an example.  My comment #10 which ended with: \"Please leave it open till it is fixed\" was answered with personal insults.  The general issue of what to do when the maintainer says 'won't fix' and closes a bug that the QA team feels is important needs to be addressed.\n\nIn general, the 'Ostrich algorithm' is not a proper solution to any bug.\n\nAnd, third, the QA team needs to be able to set the severity and priority of bugs.  The most important point here is that regressions should automatically be marked (at a minimum) Priority:HI and regressions that will be encountered by many users should have Severity:major and in some cases Priority: VHI.  The reasons for this should be clear to users but don't seem obvious to developers.  So, if (as the some developers seem to insist) QA team members with BugZilla modify authority are not to be allowed to change these, then there must be someone in charge that *is* allowed to do it.\n\nI anticipate that there will be problems.  Some developers do not like others reviewing their work.  But that is what is necessary for good quality control.  Alternatively we can try to move towards TQM where the developers would be responsible for testing their own work.  This won't, however, work to start with since as bug 73379 illustrates, some don't do even the most basic testing of their work.\n\n--\nJRT"
    author: "James Richard Tyrer"
---
The KDE Community is pleased to announce the launch of the <a href="http://quality.kde.org/">Quality
Team Project</A>, a community of contributors who will serve as a
gateway between developers and users in the KDE Project, and as a new
way for people to begin contributing.

<!--break-->
<p>KDE is a very attractive project, offering high quality software
and is freely available. There is a lot of people who feel the urge
to give something back, but stop in the middle of the way, frustrated
by the steep learning curve. The aim of the project is to reduce
these barriers by welcoming these potential contributors, and by
offering documentation, support, and even guidance if requested. 
</p>
<p>The objective is to support the new contributors, programmers or
not, in order to improve all areas of a given application (instead of
a given area of the whole project), working orthogonally with the
maintainers, developers, documenters, interface designers and
artists. 
</p>
<p>Have you ever wished to help KDE in some way, but never knew how?
The KDE Quality Team might be just what you need! People with
disparate backgrounds can join the KDE Quality Team Project, for the
development of a complex desktop environment requires a wide range of
skills. The main tool to help any application is knowing well its
interface and functions, so any user who knows and cares for any KDE
application can almost immediately help the project. Depending on the
team member programming knowledge, artistic and writing skills or
willingness to learn, there is no area of the application that cannot
be improved. 
</p>
<p>In terms of support for new contributors, the project already
offers: 
</p>
<ul>
<li>A <a href="http://quality.kde.org/develop/cvsguide/">complete
and safe step by step building guide</a>, with information to help
you decide what to build, to solve build problems, and to run the
new KDE build.</li>
<li>A <a href="http://quality.kde.org/develop/cvsguide/managestep.php">guide
to help you maintain a CVS working folder</a>, providing with all
you need know to set up your sources, update, apply patches and
submit your changes to KDE, either by generating patches or
committing.</li>
<li>A guide to help you contribute to KDE, the <a href="http://quality.kde.org/develop/howto">Quality
Team HOWTO</a>.</li>
</ul>

<p>In an interview conducted by Henrique Pinto, Carlos Woelz, the person who is currently putting
the most effort in the project, speaks about the
project: 
</p>

<p><b>Why there is need for a KDE Quality Team?</b> 
</p>
<p>There are many people out there who want to contribute, but didn't
manage to know where to start, specially non programmers or at least
non unix programmers. To them, this is a whole new world (CVS, compiling, docbook, xhtml, etc...). I think there is no better way to help KDE
than to support these newcomers. 
</p>

<p>If you are curious about the original motivation, you can read the <a href="http://www.geocities.com/carloswoelz/proposal.txt">original
proposal</a>. 
</p>

<p><b>What was the inspiration for such a project?</b></p>

<p>When I started thinking about giving something back to KDE, I had
trouble knowing where to start. There is a lot of good information at
<a href="http://developer.kde.org/">developer.kde.org</a>, but
(obviously) much of it is oriented to unix developers. I felt a bit
lost, the learning curve is steep for someone like me coming from the
windows world. Also, I started working with applications I knew too
little, and with too many of them at the same time. 
</p>

<p><b>In which ways KDE will benefit from having a Quality Team?</b>
</p>

<p>The KDE developers have been doing a great job. My hope is that we
can bring the documentation, artwork, communication with our users,
and the little boring details of the user interface to the same level
of quality of the KDE applications. Also, managing bugs and wishes can
help programmers focus on fixing bugs and adding new features.
</p>

<p><b>How can one get involved with the Team?</b> 
</p>

<p>If you are an experienced contributor, and are interested in
guiding and reviewing the new contributors work, join the kde-quality
mailing list. If you like a KDE application and want to help, join
the kde-quality mailing list. 
</p>

<p>For new contributors, there are many different ways
to help the KDE project. We have a
collection of tasks, with the necessary skills on how to perform
them, and links to guides on how to perform them at the KDE Quality
Website: 
</p>
<p><a href="http://quality.kde.org/develop/modules/">
http://quality.kde.org/develop/modules/index.php</a>
</p>
<p>The KDE PIM module was chosen as recommended initial target for
the KDE Quality Team because of its shorter release cycle, so things
are going faster there. There is wiki a page to coordinate the
efforts for the PIM Quality Team, where you can have an idea of the
current efforts. 
</p>
<p><a href="http://wiki.kdenews.org/tiki-index.php?page=Quality+Team+KDE+PIM">
http://wiki.kdenews.org/tiki-index.php?page=Quality+Team+KDE+PIM</a>
</p>
<p><b>How will be the relationship between Quality Team members and
application maintainers?</b> 
</p>
<p>The maintainer and developers know the functionality of the
application better then anyone, therefore their help and review is
needed for all activities around that application. This stresses the
importance of doing serious work. If the team does good work, I am
sure it will be respected and helped by the maintainers. 
</p>
<p>I couldn't be happier with the response from the community to this
project. The KDE PIM developers, for instance, were planning to start
something similar to the KDE Quality Team. We joined forces, Adriaan
de Groot is the Quality Team Coordinator for KDE PIM, and they put a
KDE PIM page with information about the PIM tasks at the KDE PIM
site: 
</p>
<p>
<a href="http://pim.kde.org/development/janitor_jobs/">http://pim.kde.org/development/janitor_jobs/</a>
</p>
<p><b>How many people are involved in the project in this initial
stage?</b> 
</p>
<p>Already more than I imagined. The project in integrating nicely to
KDE normal development process. 
</p>
<p><b>How do you envision KDE and the KDE Quality Team one year from
now?</b> 
</p>
<p>I don't try to envision anything. What I am sure is that I will
help the KDE project. All the work put on the Quality Team (writing
guides, the website, the structure, etc...) has already produced good
documentation. I am not worried about it, because any time spent with
the project will be well spent. 
</p>
