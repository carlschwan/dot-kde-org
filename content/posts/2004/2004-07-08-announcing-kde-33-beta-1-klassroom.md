---
title: "Announcing KDE 3.3 Beta 1 \"Klassroom\""
date:    2004-07-08
authors:
  - "ikulow"
slug:    announcing-kde-33-beta-1-klassroom
comments:
  - subject: "Ultra-Mini-review"
    date: 2004-07-08
    body: "Spiffy things:\n\n- Konqueror display seems much nicer now. You don't get that effect where you see half of the old page while waiting for the new page to load. Everything get's drawn in one go now, which makes things look solid.\n- Cool integration of Google into Konqueror.\n- MacOS-style menubar follows the panel background now.\n- Nice use of separators between buttons in Konqueror.\n- Handy search field in kmail\n- Elegant \"highlight text view\" effect in Plastik.\n- Kopete's new buddy-list effects are *awesome*\n- Very nice new Kontact splash screen (doesn't last long enough :)\n\nNot spiffy things:\n\n- Not nice use of separators between buttons in KMail (too many)\n- New KMail search bar needs some margins above and below.\n- I got four copies of kconf_update in my taskbar the first time I started up Kontact\n- Some missing icons (eg: new Google context menu entry). Cervisia Konqueror plugin icon still phenomenally ugly.\n- KWallet's dialog boxes still too wordy."
    author: "Rayiner Hashem"
  - subject: "Re: Ultra-Mini-review"
    date: 2004-07-08
    body: "> - KWallet's dialog boxes still too wordy.\n\nWhich words did you want removed?"
    author: "George Staikos"
  - subject: "Re: Ultra-Mini-review"
    date: 2004-07-08
    body: "When saving a password, the current text is:\n\n\"Konqueror has the ability to store the password in an encrypted wallet. When the wallet is unlocked, it can then automatically restore the login information [the] next time you visit this site. Do you want to store the information now?\"\n\nMost of that is extraneous. The text should simply be:\n\n\"Would you like to store this login information in your current KWallet?\"\n\n1) It refers to specific program features by name, which is considered good practice.\n\n2) It gets rid of all the preamble, which the user doesn't really care about anyway. \n\n3) It takes the real question \"Would you like to store\" and puts that at the forefront, instead of burying it after a lot of other text.\n\n(3) is particularly important, given that the dialog is not one of those \"verb-button\" dialogs, but rather a \"yes/cancel\" dialog."
    author: "Rayiner Hashem"
  - subject: "Re: Ultra-Mini-review"
    date: 2004-07-08
    body: "Personally I would prefer a 'more' button or something, with an in-depth explanation of what such a feature does, where the passwords are stored, how they are stored, how they ar kept safe from others, and so forth. :-)"
    author: "arcade"
  - subject: "Re: Ultra-Mini-review"
    date: 2004-07-08
    body: "this duty of 'whats this' help"
    author: "Nick Shaforostoff"
  - subject: "Re: Ultra-Mini-review"
    date: 2004-07-08
    body: "Precisely. This is also the reason for mentioning \"KWallet\" directly --- so users can look up more information in the online help."
    author: "Rayiner Hashem"
  - subject: "Re: Ultra-Mini-review"
    date: 2004-07-08
    body: "> - Some missing icons (eg: new Google context menu entry).\n\nJust visit Google once (e.g. by using this menu entry)."
    author: "Anonymous"
  - subject: "Re: Ultra-Mini-review"
    date: 2004-07-08
    body: "can you please post some pics?"
    author: "charles"
  - subject: "Re: Ultra-Mini-review"
    date: 2004-07-08
    body: "What about the spacing between the menu items in programs (Location   Edit   View   Go   Bookmarks   Tools...)?  I have always found that they are too close together in kde making them all look run together.  gtk2 seems to have the spacing about right.  I heard something about this being adressed in a new kde releases.  Has it made this one or is it waiting for something like qt4?"
    author: "theorz"
  - subject: "Re: Ultra-Mini-review"
    date: 2004-07-08
    body: "I think menu item spacing already improved in Qt 3.3"
    author: "wilbert"
  - subject: "Re: Ultra-Mini-review"
    date: 2004-07-08
    body: "qt styles are free to change it at will in qt 3.3.x.. plastik-cvs uses something like 18px in my eyes.. old was 14px. gtk2 uses something like 16px."
    author: "anon"
  - subject: "Re: Ultra-Mini-review"
    date: 2004-07-08
    body: "For me it is more strange if you get asked if a password should be stored:\nOne the question if I want to store the password: It shouldn't be \"Cancel\" but \"no\" as a third choice."
    author: "me"
  - subject: "Re: Ultra-Mini-review"
    date: 2004-07-08
    body: "> - Not nice use of separators between buttons in KMail (too many)\n\nWhich would you remove? The current grouping seems logical to me."
    author: "Anonymous"
  - subject: "Re: Ultra-Mini-review"
    date: 2004-07-08
    body: "Besides, those separators have always been there. Only they were no line separators but just spacers. Now it was decided to use line separators by default for all of KDE. If you want to have it changed then send your complaints to the KDE Usability list.\n\nBTW, you can remove the separators that you don't want (Settings->Configure toolbars)."
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Ultra-Mini-review"
    date: 2004-07-08
    body: "> - New KMail search bar needs some margins above and below.\n\nMargins? Why? Do you probably mean a line? I could understand that you want a line above the search bar, but why below? Below it's already clearly separated from the message list. Or maybe I don't understand the problem because I'm using Keramik?"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Ultra-Mini-review"
    date: 2004-07-08
    body: "Not a line, just some space. In Plastik, the search bar is right up against the message list below. It's actually a pervasive problem in KDE --- all the widgets are very close together, instead of being nicely spaced out. Arrows next to button icons (as in Konqueror's back/forward buttons) are scrunched up against the icon. Compare (say), Mozilla to Konqueror. In Mozilla, the buttons are nicely spaced out, and there are  margins above and below the search bar. To me, maintaining esthetic appeal is worth a few dead pixels. "
    author: "Rayiner Hashem"
  - subject: "Re: Ultra-Mini-review"
    date: 2004-07-08
    body: "> Arrows next to button icons (as in Konqueror's back/forward buttons) are \n> scrunched up against the icon. \n\nThat's because they're part of the button, not a separate widget"
    author: "Sad Eagle"
  - subject: "Re: Ultra-Mini-review"
    date: 2004-07-08
    body: "There could still be some more spacing there --- I'm pretty sure KDE allows for arbitrary size buttons."
    author: "Rayiner Hashem"
  - subject: "Re: Ultra-Mini-review"
    date: 2004-07-09
    body: "In my opinion in Mozilla too much space is wasted by the extremly large spacing. But that's obviously a matter of taste. And as such it has to be solved in the widget styles."
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Ultra-Mini-review"
    date: 2004-07-08
    body: "Not a line, just some space. In Plastik, the search bar is right up against the message list below. It's actually a pervasive problem in KDE --- all the widgets are very close together, instead of being nicely spaced out. Arrows next to button icons (as in Konqueror's back/forward buttons) are scrunched up against the icon. Compare (say), Mozilla to Konqueror. In Mozilla, the buttons are nicely spaced out, and there are  margins above and below the search bar. To me, maintaining esthetic appeal is worth a few dead pixels. "
    author: "Rayiner Hashem"
  - subject: "Screen Shots ..."
    date: 2004-07-08
    body: "will always do a good job on selling ."
    author: "a.c."
  - subject: "Re: Screen Shots ..."
    date: 2004-07-08
    body: "There is nothing to sell, no final product exists and even if someone would offer you the same for free."
    author: "Anonymous"
  - subject: "Re: Screen Shots ..."
    date: 2004-07-08
    body: ">There is nothing to sell, no final product exists and even if someone would >offer you the same for free.\n\n\nand the reason for announcing this at the kde.org site and the dot is why?\nPure news? Or are we wanting others to download it, install it, play with it, then file bug reports and ideas? That is called selling. If it was to remain with the developers on the list, then it would never have made it here. and it certainly would not have been bundled up.\n\nAnd yes, even at this stage, screen shots help."
    author: "a.c."
  - subject: "K...[K]...{K}...<K>...?"
    date: 2004-07-08
    body: "Can someone tell me the logic behind the numerous K(s) in KDE associated names? By the way, I love KDE and use it everyday.\n\nCb.."
    author: "charles"
  - subject: "Re: K...[K]...{K}...<K>...?"
    date: 2004-07-08
    body: "Logik? Korporate Identity."
    author: "Anonymous"
  - subject: "Re: K...[K]...{K}...<K>...?"
    date: 2004-07-08
    body: "*lol* ... great :-)))"
    author: "Anonymous"
  - subject: "Re: K...[K]...{K}...<K>...?"
    date: 2004-07-08
    body: "I agree that we need identity, but here is a suggestion: instead of \"Kontact\", \"Kroupware\", (Ksomething instead of [C|G|?]something) i would propse  KDE + Name. For instance KDE Contact. KDE Groupware. KDE Name it here. That would be much more convinient."
    author: "tomislav"
  - subject: "Re: K...[K]...{K}...<K>...?"
    date: 2004-07-08
    body: "It *is* officially \"KDE Kontact\" (because of Samsung Contact I assume)."
    author: "Anonymous"
  - subject: "Re: K...[K]...{K}...<K>...?"
    date: 2004-07-08
    body: "The same logic behind Windows WinApps, Mac iApps, GNU GNUApps, wxApps or gnome GApps: it is an easy way to tell something to the user, being the OS to run the apps, the maker, the toolkit, the desktop, whatever.\n\nIn KDE case, a KApps usually means \"made with KDE technology and integration\".\n\nNow you know :)"
    author: "Carlos Leonhard Woelz"
  - subject: "Re: K...[K]...{K}...<K>...?"
    date: 2004-07-08
    body: "The g prefix is for GNU usually."
    author: "Max Howell"
  - subject: "Re: K...[K]...{K}...<K>...?"
    date: 2004-07-09
    body: "Many GTK and Gnome applications use the 'g' prefix as well."
    author: "Patrick McFarland"
  - subject: "Re: K...[K]...{K}...<K>...?"
    date: 2004-07-13
    body: "Come on, do we really have to state the old truth again? \"The everybody else argument doesn't work\". There, that's done.\n\nNow, I totally agree that the naming of applications is becoming rather silly. How is it that so few have inventive names? Unsermake at least is an interesting name. The name does /not/ have to describe the function of an application, that's what the comments fields in .desktop files are for.\n\nfrom the Campaign In Favour of More Inventive Naming of Software (hereforth CIFMINS)."
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: K...[K]...{K}...<K>...?"
    date: 2004-07-08
    body: "To feed the Slashdot trolls of course!"
    author: "Tom"
  - subject: "Re: K...[K]...{K}...<K>...?"
    date: 2004-07-08
    body: "I think everyone else did so back in 1996 when KDE was founded and the first crop of KDE apps appeared. CDE had dtterm, dtmail, dtfile, dtpanel, etc.. XWindows itself had xterm, xcalc, etc.. In the Windows world back then, everyone was coming out with products with \"95\" on their end, or \"Win\" at the beginning. In the Mac world, most Mac-only apps had \"Mac\" suffixes. \n\nIt's just a way of letting users know that an app works with KDE. "
    author: "anon"
  - subject: "About requirements"
    date: 2004-07-08
    body: "KDE 3.3 XServer Requirement says:\n\nPackage: X Server (with link to www.xfree86.org)\n\nLevel: Required\n\nDescription: An X Server provides the underlying display technology on UNIX systems. The KDE Project recommends the XFree86 server.\n\nWith at least major Linux distributions using X.org server now, and XFree86 with an uncertain future, it this still correct, or just a forgotten detail?\n\nEven more, 3.3's Konsole at least is able to use X.org's real transparency, so using XFree86 means that some (ok, trivial) features won't be available."
    author: "Shulai"
  - subject: "Re: About requirements"
    date: 2004-07-08
    body: "I'd say those requirements are just copy and paste from some older requirements, with maybe some central libraries updated. I'd say there is no political statement behind the XFree86 requirement."
    author: "Chakie"
  - subject: "Re: About requirements"
    date: 2004-07-08
    body: "\"Even more, 3.3's Konsole at least is able to use X.org's real transparency\""
    author: "Janne"
  - subject: "Re: About requirements"
    date: 2004-07-08
    body: "> 3.3's Konsole at least is able to use X.org's real transparency\n\nYou confuse the \"X.org\" and fdo's \"X-Server\" X servers."
    author: "Anonymous"
  - subject: "Re: About requirements"
    date: 2004-07-08
    body: "Sorry, I screwed up with my earlier post\n\n\"Even more, 3.3's Konsole at least is able to use X.org's real transparency\"\n\nX.org does not have \"real\" transparency. You are confusing X.org to Freedesktops Xserver (which DOES have \"real\" transparency and other cool features). Current X.org is just Xfree 3.4 RC2 with few additional changes and fixes."
    author: "Janne"
  - subject: "Re: About requirements"
    date: 2004-09-12
    body: "Nit pick, it's 4.3 rc2 not 3.4"
    author: "Mark Hannessen"
  - subject: "Re: About requirements"
    date: 2004-07-08
    body: "there is also xouvert and y-windows, so we can just wait (or contribute to one of them)"
    author: "Nick Shaforostoff"
  - subject: "Re: About requirements"
    date: 2004-07-08
    body: "There is also Fresco (http://www.fresco.org), GNU Hurd and OpenParsec. ;-)"
    author: "anonymous"
  - subject: "Re: About requirements"
    date: 2004-07-08
    body: "hm, sounds interesting :-)\n\nbut Y is in development less thet a year, while freSCO - more the 5 years.\nand afaik Y doesnt use slow CORBA (as well as kde)."
    author: "Nick Shaforostoff"
  - subject: "Re: About requirements"
    date: 2004-07-08
    body: "the higher level of code, the more its portability, so kde has good chances to survive after The Great Y Coming"
    author: "Nick Shaforostoff"
  - subject: "Re: About requirements"
    date: 2004-09-12
    body: "you are kidding right? it's the other way around. x is not THAT bad, so y only has a chance if most of the existing stuff can be ported without to many problems."
    author: "Mark Hannessen"
  - subject: "KArtWork won't build"
    date: 2004-07-08
    body: "If only I could figure out why the KWin-Styles won't build.\n\n.libs/nextclient.o(.text+0x1fea): In function `KStep::NextClient::menuButtonPressed()':\n: undefined reference to `KDecoration::showWindowMenu(QRect const&)\n\nIt builds correctly with ALPHA_1.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: KArtWork won't build"
    date: 2004-07-08
    body: "Your kdelibs version seems to not be the KDE 3.3beta1 version since kdebase/kwin/lib/kdecoration.cpp does contain the implementation for the KDecoration::showWindowMenu(const QRect&) method.\n\nhttp://makeashorterlink.com/?T126351C8"
    author: "anonymous"
  - subject: "KDE Systray apps (KPPP) now compatible?"
    date: 2004-07-08
    body: "If I use for eg. IceWM (icewmtray), Gnome or any other Freedesktop compilant tray application?"
    author: "anonymous"
  - subject: "Re: KDE Systray apps (KPPP) now compatible?"
    date: 2004-07-08
    body: "they are already compatible with kdetrayproxy (which is included in kdelibs in kde 3.3), but available for kde 3.2 as well"
    author: "anon"
  - subject: "great for a beta"
    date: 2004-07-08
    body: "The only two problems I found so far are:\n- mouse wheel is scrolling horizontaly and not vertically by defailt, what is very annoying (does someone know a fix fot this?)\n- kget crashes"
    author: "Iuri Fiedoruk"
  - subject: "Re: great for a beta"
    date: 2004-07-13
    body: "A fix, would be to put Option \"ZAxisMapping\" \"4 5\" instead of Option \"ZAxisMapping\" \"6 7\""
    author: "illogic-al"
  - subject: "Group bookmarks"
    date: 2004-07-08
    body: "  Does Konqueror have group bookmarks yet?  That's pretty much the only thing that keeps me using Mozilla as my primary browser.  The ability to click upon e an icon on my toolbar, labelled \"News\", that opens 15 tabs and loads eg. Slashdot, dot.kde, Linux Today, Linux Weekly News, CNN etc. is a killer feature of Mozilla for me.\n\n--\nCheers,\nRob "
    author: "Frogger"
  - subject: "Re: Group bookmarks"
    date: 2004-07-08
    body: "Just use the 'Save as Profile' feature available on Preferences menu on Konqueror."
    author: "fool-less name"
  - subject: "Re: Group bookmarks"
    date: 2004-07-08
    body: "Even KDE 3.2 has \"Bookmark Tabs as Folder...\" and \"Open Folder in Tabs\" in the folder's context menu."
    author: "Anonymous"
  - subject: "Re: Group bookmarks"
    date: 2004-07-09
    body: "I do prefer Mozilla over Konqueror (mostly because I make web pages and see how buggy Konqueror's CSS and XML are), but for your problem, have you considered using an RSS reader?  They are very convenient, collecting all the news in one place.\n\nAnd if you get one with caching, it's even better.  Like on KDE news, there is often a long span with no news, so my reader doesn't even bother me with the old data."
    author: "Keith"
  - subject: "Compile error"
    date: 2004-07-09
    body: "Anyone else having trouble? kdelibs was fine, but kwin in kdebase gives me:\nutils.h:164: `QCString KWinInternal::getStringProperty(long unsigned int, long\n   unsigned int, char)' used but never defined\nmake[1]: *** [client.lo] Error 1"
    author: "AC"
  - subject: "Re: Compile error"
    date: 2004-07-09
    body: "Nevermind... the fix is here:\nhttp://lists.kde.org/?l=kde-devel&m=108936754325983&w=2"
    author: "AC"
  - subject: "What's new ?"
    date: 2004-07-11
    body: "Perhaps it's my eyes, but I couldn't find a list of improvements in 3.3, compared to 3.2.x. Is there such list, could you please point me to it ?\nThanks in advance !"
    author: "Eric Veltman"
  - subject: "Re: What's new ?"
    date: 2004-07-11
    body: "Perhaps the feature plan:\nhttp://developer.kde.org/development-versions/kde-3.3-features.html\n(especially the \"Finished\" section of course.)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "I love this beta"
    date: 2004-07-13
    body: "There are many many small improvements everywhere.\nThis kontact version is a lot better than the one from 3.2 Really great app.\nI don't need the html editing in kmail/kontact but it's working great.\n\nReally fast (I mean usuable) on my slow computer and very stable for a beta. No crashs so far after using it for a few days. Even konqui :)\n\nNew plugins in control center. But one thing missing is the themable KDM greeter. Hope it will be finish for 3.3 final."
    author: "JC"
---
The KDE Project is pleased to <a href="http://www.kde.org/announcements/announce-3.3beta1.php">announce</a> the immediate availability of KDE 3.3 Beta 1. As another step towards the <a href="http://conference2004.kde.org/">aKademy</a> in late August, this release is named Klassroom. This beta release shows astonishing stability, so the KDE team asks everyone to try the version and give feedback through the <a href="http://bugs.kde.org/">bug tracking system</a>. For packages, please visit the <a href="http://www.kde.org/info/3.3beta1.php">KDE 3.3 Beta 1 Info Page</a> and browse the <a href="http://www.kde.org/info/requirements/3.3.php">KDE 3.3 Requirements list</a>. The <a href="http://developer.kde.org/build/konstruct/">Konstruct</a> build toolset has been updated for this release.


<!--break-->
