---
title: "Preview on kNX Client and FreeNX Server for LinuxTag Visitors"
date:    2004-06-28
authors:
  - "trahn"
slug:    preview-knx-client-and-freenx-server-linuxtag-visitors
comments:
  - subject: "works very fine"
    date: 2004-06-27
    body: "hi,\ntested it today with dsl and works really good. its a great one and i am looking forward to install an thin-client computerroom at school."
    author: "gunnar"
  - subject: "Integrate into krdc"
    date: 2004-06-28
    body: "This certainly rocks!\nWould it be possible to integrate it into krdc?"
    author: "chico"
  - subject: "Re: Integrate into krdc"
    date: 2004-06-28
    body: "Well, I don't see why it couldn't be integrated.\n\nIf I remember right it was on the kde 3.3 feature plan.\n(but I can't find it anymore, so might be moved to 3.4)\n\nNX is surely going to rock."
    author: "Mark Hannessen"
  - subject: "Re: Integrate into krdc"
    date: 2004-06-28
    body: "If it was on the 3.3 feature plan, it surely should be parr of the 3.3 release, no?\n\nI believe the reason for the \"just-in-time\" sentence of the story above may be that they rushed to finish it before 3.3 freeze, no?"
    author: "I-want-kNX-in-KDE-3.3"
  - subject: "Re: Integrate into krdc"
    date: 2004-07-02
    body: "If it's now not listed in the feature plan then it will not be in KDE 3.3."
    author: "Anonymous"
  - subject: "Re: Integrate into krdc"
    date: 2004-06-29
    body: "It should be doable to integrate the kNX backend classes into krdc and other tools"
    author: "Joseph Wenninger"
  - subject: "Producing Fruit"
    date: 2004-06-28
    body: "It seems as though the KDE Debian/UserLinux proposal was not just lip-service then? This is the sort of stuff Enterprizes want to hear, not arguments about licenses and developing proprietary software for free.\n\nWorking with Novell Zenworks and distributed VB apps, I certainly know the major problems 'fat' clients can cause - and it's worse than I ever thought possible. Distributing the right DLL versions, ini files, distributing database passwords (encrypted in ini files!) on the client for database access, MDAC components..... That's just the tip of the iceberg. Centralizing this whilst using clients in the correct manner would be a godsend.\n\nThis seems to have come along at a very opportune moment. I sincerely wish you all the best at Linuxtag 2004, and let us know the scale of interest. It should be huge."
    author: "David"
  - subject: "Now if I could solve"
    date: 2004-06-28
    body: "these printing issues with thin clients. \nOn a local net it's easy, since you can have a cupsd running on the clients which have printers connected. With the browsing capabilities of cups an available printer will show up as soon as it's 'online', but _what_ if you want to connect via the internet? I don't want to open port 631 and I don't want to fiddle with a vpn..."
    author: "Thomas"
  - subject: "Re: Now if I could solve"
    date: 2004-06-28
    body: "<a href=\"http://www.nomachine.com/faq.php#21\">http://www.nomachine.com/faq.php#21</a>\n<br/><br/>\n<p>Is it printing to local printers supported in NX?</p>\n\n<p>NX provides support for SMB protocol, by which both local printers and file-systems can be made available to the remote session. Currently, printers and file-systems are \"mounted\" by the NX server. These resources can in turn, be mounted by a WinTSE machine. All the SMB communication, that is both file access and printing, takes place compressed through ZLIB. The compression level depends on the link speed. Enhancements have been added to the GUI to simplify sharing a printer or a resource but this is still a work in progress. At the moment there are still problems to fully integrate NX client with the local printing system (e.g. CUPS or Win printing).</p>\n\n\n"
    author: "Debian"
  - subject: "Re: Now if I could solve"
    date: 2004-06-28
    body: "The upcoming NX-1.4 will start to support printing from your remote X11 sessions to your local printers. The current 1.3.2 release doesnt support this yet -- but it supports all the speed gains you read about in the article and comments.\n\nTo test the 1.4, look for the \"1.4 snapshot\" release on <a href=\"http://wwww.nomachine.com/\">NoMachine.com</a>. It is there in source form and you need to compile it.\n\nIf you are a Debian user, visit <a href=\"http://www.kalyxo.org/\">Kalyxo.org</a> for *.deb packages with a working \"nxtunnel\" script (that serves as a temporary placeholder until \"FreeNX Server\" is fully released).\n\n"
    author: "Kurt Pfeifle"
  - subject: "vnc comparison?"
    date: 2004-06-28
    body: "Any word on how this compares or is different then *VNC? Outside of the cooler domain name of course.\n\nWe use a couple of VNC servers at my .edu and they work really great. \n\nDownloading eval NX client now..."
    author: "Ian Monroe"
  - subject: "Re: vnc comparison?"
    date: 2004-06-28
    body: "To answer my own question, NX appears to actually be some sort of wrapper around VNC and X Windows. And the sound server, which I was impressed by. It appears to have some sort of support for file and print sharing as well."
    author: "Ian Monroe"
  - subject: "Re: vnc comparison?"
    date: 2004-06-28
    body: "Wrapper around VNC? This sounds wrong."
    author: "Anonymous"
  - subject: "Re: vnc comparison?"
    date: 2004-06-28
    body: "You get the selection of Unix, VNC and Windows as your Desktop. I assume Unix means X11? But I dunno really. Their test servers are swamped, as to be expected.\n\nFor some reason running with multimedia support and Amarok conflicts, even though they're both using artsd."
    author: "Ian Monroe"
  - subject: "Re: vnc comparison?"
    date: 2004-06-28
    body: "From <a href=\"http://www.nomachine.com/faq.php\">http://www.nomachine.com/faq.php</a>: \"NX is about building the next web on top of remote execution and tunneling of arbitrary protocols (X, RDP, SMB, HTTP, IPP, RSYNC, Audio, Video) in a peer-to-peer network.\"<br/><br/>\n\nSo you can tunnel VNC through NX too but it's not needed/used to tunnel X.\n"
    author: "Anonymous"
  - subject: "Re: vnc comparison?"
    date: 2004-06-28
    body: "NX is NOT a wrapper around VNC. NX (for unix) has nothing to do with VNC. NX is \"just\" a compressing proxy for X protocol, that means it only uses the X primitives and compresses them.\n\nYou would never get such a performance (70:1 compression and almost no round-trips) with VNC... "
    author: "MacBerry"
  - subject: "Re: vnc comparison?"
    date: 2004-06-28
    body: "NX itself is based on X11. NX adds \n\n-- extremely efficient compression (better than generic ZLIB, with less CP\u00dc cycles burned) to X-based traffic;\n\n-- a very intelligent caching mechanism (click for first time on the \"K\"- or any other menu and watch the delay -- then click on the same menu a second time and notice the difference);\n\n-- elimination of the dreaded round-trips (composed of \"X-request\" and \"X-reply\" pairs) which make X over lo-bandwidth/hi-latency links so slow, even if you switch on the \"-C\" (for compression) in your \"ssh -X\" (X-forwarding) connection).\n\nThat, of course, makes NX the prime choice to tunnel X connections through. In the highest compression level (select \"Modem\") the avarage efficiency increase for an  office productivity session (running KDE as a desktop, Konqueror as the file manager, KMail as your mail client, OpenOffice as your word processor and Mozilla as your browser) you can get as much as 60:1 effective compression on avarage (and if you stick to \"pure KDE\" (use KWord for word processing, and Konqui for web browsing too), it may even be more...\n\nBut NX (and based on this, kNX Cclient and FreeNX Server) can do even more: it can also convert RDP (Remote Desktop Protocol, used by Windows Terminal Servers) and RFB (used by VNC servers) to NX and thereby increase efficiency of these two protocols by 2- to 10-fold across slow links (even if you compare it to TightVNC!).\n\nSo to call NX a \"wrapper around VNC\" would be entirely wrong even if you use it to access a TightVNC server: a mere wrapper would never give you a 2- to 10-fold speed boost...."
    author: "Kurt Pfeifle"
  - subject: "Re: vnc comparison?"
    date: 2004-06-29
    body: "Ok, that makes sense. I was just going on the fact that you had a choice between Windows, Unix and VNC. \n \n I'm using it to connect to my computer at home. It uses a upload-capped cable modem (so its set to ISDN), so its squeaking by. Response to some input seems delayed. I would call it usable, which couldn't be said for VNC.\n \n I notice that cacheing your talking about when browsing a web page. Its faster to scroll up and down in areas of a page I've already looked at."
    author: "Ian Monroe"
  - subject: "Re: vnc comparison?"
    date: 2004-06-29
    body: "> It uses a upload-capped cable modem (so its set to ISDN), so its squeaking by.\n\nI take you're saying your monthly quota is 100% used and the service is making difficult to upload things.\n\nI say this because, even if your \"up\" connection is limited to, say, 33Kbps, you would still have an usable experience with NX (I've use ICA in this situation and NX is said to equal or better than it).\n\nCould someone in-the-know throw some 2\u00a2 here?"
    author: "Caf\u00e9, p\u00f4!"
  - subject: "Re: vnc comparison?"
    date: 2004-06-30
    body: "No its capped at about 15-20 kpbs (the supposed minimum upload is 32 kbps second, but it seems to be more like thats the max), there isn't a quota. ISPs with quotas are un-American. Like I said, its usable but sometimes unresponsive. With VNC its easy to gauge if its being responsive enough since the mouse cursor is drawn remotely, but the cursor with NX is always responsive.\n\nI know your overall internet connection can be hosed by using all your upload bandwidth. It could be a situation where limiting the amount of bandwidth available to NX would make things more reliable."
    author: "Ian Monroe"
  - subject: "Re: vnc comparison?"
    date: 2008-12-12
    body: "Cable is a shared bandwidth medium.  Because everybody on the same cable segment shares that bandwidth, the more they can restrict a users use of bandwidth, the more customers they can put on the same segment.\n\nFurther, they don't like other people's content competing with theirs so they do slimy things like severely restrict upload speeds to that Bit Torrent and other peer-to-peer networks don't function well.\n\nDSL by contrast doesn't have the maximum transmission speed that cable does, but your circuit is your circuit, and the upload speeds tend to be less restrictive although most DSL is still asymmetrical favoring download speed over upload, but in general upload speeds are much better and DSL companies generally won't cancel you for using too much bandwidth which has become a favorite hobby of some cable companies.\n\nAnd no I didn't come here to advertise; I was looking for a way to do audio with vnc; this just happens to be a sensitive point because I loose a lot of customers to cable when people are fooled into thinking they're getting a huge amount of bandwidth on the cheap; and of coarse they are as long as they don't actually try to use it.\nIf you're a Linux user, you're probably downloading RPM's to keep your system up to date and expand functionality regularly.  Cable might not be the best option.  Ok, I admit it, I'm biased as I do sell DSL services.  But really, we don't do that crap and I don't find it ethical, but when you understand the economics and how cable access works you can understand why they do."
    author: "Robert Dinse"
  - subject: "Re: vnc comparison?"
    date: 2004-06-28
    body: "It has much better compression (average 70:1)."
    author: "Anonymous"
  - subject: "What about making the client name pronounceable?"
    date: 2004-06-29
    body: "What about adding an \"a\" so it's kNaX? :)\nThe binary would of course be all-lowercase: knax"
    author: "cm"
  - subject: "kNX"
    date: 2004-06-29
    body: "Just say \"kinks\"... ;-P"
    author: "Caf\u00e9, p\u00f4!"
  - subject: "Re: What about making the client name pronounceable?"
    date: 2004-06-29
    body: "What's wrong with just saying K-N-X? Or even k'nex?"
    author: "Paul Eggleton"
  - subject: "Another nice photo"
    date: 2004-07-03
    body: "<a href=\"http://www.pl-berichte.de/berichte/lt2004/fotos/lt2004-16.html\">\nhttp://www.pl-berichte.de/berichte/lt2004/fotos/lt2004-16.html</a>\n"
    author: "Anonymous"
  - subject: "need information"
    date: 2004-07-19
    body: "hi,\ni want to know if can install FreeNX Server and kNX Client. and if it is possible how i can find the howto\nthank you"
    author: "tahtah"
---
A Task Force Team of <a href="http://www.kde.org/">KDE</a> and <a href="http://www.knoppix.org/">Knoppix</a> hackers and enthusiasts created "just
in time" for <a href="http://www.linuxtag.org/">LinuxTag 2004</a> two
programs which harvested an overwhelming response from visitors: FreeNX
Server and kNX Client. Although not yet officially released, several
presentations showed a well working preview of the KDE version for the speed
boosting NX Terminal Server technology.





<!--break-->
<p><a
href="http://www.nomachine.com/">NX Technology</a> provides exclusive
compression techniques that make it possible to run complete remote cross
plattform desktop sessions using narrowband Internet connections. At speeds
as low as those offered by a modem or by ISDN it's possible to work fluently
with the remote desktop shown full screen. Besides providing high
performance on low bandwidth connections NX technology enables the user to
disconnect from and reattach to existing sessions running on the Terminal or
Application Server. Even sessions lost due to network problems can be
recovered easily. Therefore NX technology is expected to have a huge impact
on Thin Client architectures as well as well as mobile computing.
Enterprises, Internet cafes and similar users already appreciate KDE's <a href="http://enterprise.kde.org/articles/korporatedesktop3.php">Kiosk
framework</a> which provides a simple way to disable certain features in KDE to
create a more controlled environment.  With mature technologies like Kiosk
as well as FreeNX KDE proves to be the ideal choice for Terminal and
Application Server deployment.</p>

<p>Both the FreeNX Client and the FreeNX Server will be available soon at
<a href="http://www.kalyxo.org/">Kalyxo.org</a>. Kalyxo is a project which focuses on KDE/Debian integration as
well as providing Debian-specific system tools for the enterprise.
Kalyxo.org will host the FreeNX source repository for all distributions. It
will host and maintain GPL'ed NX Debian packages.</p>

<p>Fabian Franz from the Knoppix Development Team is the driving force behind
FreeNX Server. kNX Client is developed by Joseph Wenninger.
Gian Filippo Pinzari, NX Development Architect and KDE enthusiast from <a
href="http://www.nomachine.com/">NoMachine<a/> provides the foundation of the
NX Terminal Server technology by developing the GPLed libraries which are
used by the FreeNX Server. NoMachine is one of the sponsors of the <a href="http://conference2004.kde.org/">KDE
Community World Summit</a> ("Akademy") and will be present there as well.</p>

<p>More information on NX Technology:
<a href="http://www.nomachine.com/resources.php">http://www.nomachine.com/resources.php</a></p>

<p>You are invited to test NX Technology at:
<a href="http://www.nomachine.com/testdrive.php">http://www.nomachine.com/testdrive.php</a></p>

<p>Photos:<br>
<a href="http://static.kdenews.org/binner/knx-linuxtag/knx1.jpg">1.</a> Joseph Wenninger, Kurt Pfeifle and Fabian Franz working on the kNX
Client.<br>
<a href="http://static.kdenews.org/binner/knx-linuxtag/knx2.jpg">2.</a> Knoppix and KDE developers present the kNX Client and FreeNX Server
during LinuxTag 2004.<br>
<a href="http://static.kdenews.org/binner/knx-linuxtag/knx3.jpg">3.</a> KDE core developer Joseph Wenninger in front of the Login Window of the
FreeNX application "kNX client"</p>




