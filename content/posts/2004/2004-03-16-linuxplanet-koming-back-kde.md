---
title: "LinuxPlanet: Koming Back to KDE"
date:    2004-03-16
authors:
  - "binner"
slug:    linuxplanet-koming-back-kde
comments:
  - subject: "Not impressed"
    date: 2004-03-16
    body: "I am totally not impressed with this review. It doesn't add anything to the allready long list of reviews. It basicly doesn't say anything at all. I don't have the impression that the author took a serious amount of time to get to know the features KDE has to offer nowadays. "
    author: "Andr\u00e9 Somers"
  - subject: "Re: Not impressed"
    date: 2004-03-16
    body: "Not all users are feature freaks. I'm happy that he sees his previous concerns about KDE vanished."
    author: "Anonymous"
  - subject: "Re: Not impressed"
    date: 2004-03-16
    body: "I certainly don't care about any of the new features in KDE.. I upgraded to 3.2 because it's faster, more stable, and cleaner than 3.1.  This is IMHO much more important than new features. "
    author: "anon"
  - subject: "Re: Not impressed"
    date: 2004-03-19
    body: "yes, i think developers should forget about new features and work on optimization and usability.\n\nI recently compiled quanta321, and now I understand why it is slower than even a kdevelop - there are more warnings than usual make-messages."
    author: "Nick Shafff"
  - subject: "Re: Not impressed"
    date: 2004-03-19
    body: "That sounds like you should address the gcc guys to speed up C++ compilation?"
    author: "Anonymous"
  - subject: "Re: Not impressed"
    date: 2004-03-19
    body: "I wouldn't want to question your \"understanding\" of things, but runtime efficiency of an application is simply not related to the amount of warnings produced by the compiler when building it. At all.\n\nI'm also a bit curious about how Quanta can be slower than KDevelop. I mean.. slower at doing _what_?"
    author: "teatime"
  - subject: "Re: Not impressed"
    date: 2004-03-19
    body: "loading...\n\ni just want to say that good program must not have such amount of warnings"
    author: "Nick Shafff"
  - subject: "Re: Not impressed"
    date: 2004-03-19
    body: "> i just want to say that good program must not have such amount of warnings\n\nYou do release that the warnings given are:\n\n- highly dependendent on the version of gcc\n- dependent on what level of warnings the app turns on\n\nI bet Quanta just turns more warnings on than the usual KDE app. "
    author: "anon"
  - subject: "Re: Not impressed"
    date: 2004-03-16
    body: "I had a personal exchange with the author to thank him for taking the time to get the facts about Quanta. In his review he was attentive enough to the details to report that the PHP parsing problem was fixed in 3.2.1. In my exchange with him it was clear that he was conscientious about his task and that he was also aware of what was going on and had actually looked into what we were doing to solve the problem. Frankly I don't see that hardly ever in a review. He was also very kind in complimenting our team for fixing a rather difficult bug in time for 3.2.1.\n\nIt was this exchange on our list I believe that set things in motion to link this here. While the author and I may have have different tastes as I have been running KDE all along I think it is unfair to say that he did not do a serious review. First of all there is an awful lot to review in KDE. A thorough review would be more like a thesis than a light read. The second big factor here is that when you do a review you generally only have so much space and so many words a publisher is looking for. In light of these facts it seems nearly impossible to write a review within the usual constraints you could call satisfying.\n\nIt also shows you don't have to be a KDE supporter to give a thumbs up. It may not be informative to you, but it may be something you can show a friend who thinks they will only hear how good KDE is only from KDE supporters.\n\nYou may have a luxury of experience and information on KDE and perhaps this is not the best place to say such a review would be informative. What it does do is show a very fair and even handed review from an outsider. From the standpoint of an application maintainer I see that he took the time to install and test not once but twice and to amend his review to report the problems he found were fixed a mere six weeks after the major point release. That may not be tremendously informative to many readers here but it is very useful to show how well KDE is being received by those who are not the rabid supporters we are."
    author: "Eric Laffoon"
  - subject: "Re: Not impressed"
    date: 2004-03-17
    body: "OK, you (and the two annonymous posters before you) have a point there. I'll retreat back into my shell now."
    author: "Andr\u00e9 Somers"
  - subject: "Re: Not impressed"
    date: 2004-03-17
    body: "You shouldn't have to because I agree with you. It doesn't matter if the guy is very friendly and takes time to get his facts straight, I'm sure he is and I'm sure he does.\n\nBut that doesn't change the fact that it's a very uninspiring article. Come on, it almost seems that the only information he gives is which packages the new KDE release is made up of.  And in the end his conclusion is almost as long, or rather as short, as the entire article.\n\nVery nice to read another positive KDE article of course, but this article doesn't _tell_ me anything! If I had never heard of KDE this article would sure as hell never convince me to go and try it.\n\nJust my worthless 2c"
    author: "Quintesse"
  - subject: "Coming back to LinuxPlanet"
    date: 2004-03-16
    body: "I used to read it regularly, but lost interest in reading the ill informed rants. This was a good review, reflected my experiences precisely. I'll have to check out what they have.\n\nIt is particularly interesting to see KDE respond as a project to the challenges it faces. The codebase is huge. No one person understands it all. Some parts are getting very complex that no one but the maintainers dare touch it. Yet it came together. And continues to grow.\n\nDerek"
    author: "Derek Kite"
  - subject: "Intention?"
    date: 2004-03-17
    body: "This should be a coincidence or this guy is the best bug finder in the world. I mean, in page 3 he stumbles in one try with every and each of the most popular bugs!\n\nOn the other side it's good to notice that *all* his worries were fixed before the article saw the light :)"
    author: "Cloaked Penguin"
  - subject: "Back to KDE"
    date: 2004-03-17
    body: "The title of this story reminds me of what I have to do. :) Been using XP for a month now, since I bought my laptop. At first I didn't  want to invest the time to get Linux running on it - if millions of people can use Windows, surely I could?\n\nNo. Even a non-crashing Windows, like XP, is nowhere near as comfortable to use as a *nix/KDE-machine. Wanna get some software for your Windows box? Sure, look for the shareware with the least restricting limitations, or get some commercial software and use it illegaly. Crap is everywhere, computer gets slower the more software you install. You don't know what the h*ll the machine is doing half of the time. You think KDE programs are bloated? Popular Windows programs grow and grow 'til your instant messenger can browse the web and your mp3 player can f*scking burn cd's.\n\nGotta get Linux up and running on this machine, wlan and all, no matter what the cost. ;)\n"
    author: "Apollo Creed"
  - subject: "Re: Back to KDE"
    date: 2004-03-17
    body: "My HP Pavilion ZE4455EA works beautifully with linux :) Everything except some of those multimedia keys works! Well, when i use kernel 2.6.x that is ;) This laptop doesn't even know the meaning of the words microsoft windows, and i'm going to keep it that way :)"
    author: "Bart Verwilst"
  - subject: "Re: Back to KDE"
    date: 2004-03-18
    body: "If you use KDE (as I suspect you given what site we are on) you can go to kcontrol and look for one of the keyboard layouts (Region & Accesibility->Keyboard Layout) which supports your multimedia keys)"
    author: "James L"
  - subject: "btw: where is the iso:// kioslave?"
    date: 2004-03-18
    body: "vanished?"
    author: "ac666"
---
<a href="http://www.linuxplanet.com/">LinuxPlanet</a>'s Kurt Wall <a href="http://www.linuxplanet.com/linuxplanet/reviews/5289/1/">reviews the KDE 3.2 series</a>. Before he was concerned that KDE's size and complexity would result in a desktop that was virtually unusable but is pleased and surprised to find that KDE 3.2 is significantly faster, easier to use, more consistently implemented, and easier on the eyes than any previous version and calls it a terrific product which shows that Linux is increasingly competitive on the desktop. 
<!--break-->
