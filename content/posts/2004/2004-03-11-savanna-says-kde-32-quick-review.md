---
title: "Savanna Says: KDE 3.2 - A Quick Review"
date:    2004-03-11
authors:
  - "fmous"
slug:    savanna-says-kde-32-quick-review
comments:
  - subject: "Nice read"
    date: 2004-03-10
    body: "Thank you Savanna!\n\nIt was a really nice read!\n\nI hope game makers start to make KDE versions of their games, so you can ditch Windows completely..."
    author: "Henrique Pinto"
  - subject: "Re: Nice read"
    date: 2004-03-11
    body: "Wow this review is over the top.\nThe only thing that's missing is a picture of a scanlty cled sweety.\n\nEither that or KDE 4.0 can be cancelled because perfection has been achieved."
    author: "C Neal"
  - subject: "Re: Nice read"
    date: 2004-03-11
    body: "i know it's shocking that some of our users are truly happy and satisfied, but it happens ;-)"
    author: "Aaron J. Seigo"
  - subject: "About games..."
    date: 2004-03-11
    body: "This review is great and realistic (in fact I use Windows only to play and KDE for more... let's say, serious things). But Ive got a word about exporting games from Windows as Henrique said. OK, it's true, some games have been reprogrammed to fit Linux kernel, but I think that Windows is faster for 3d apps, and, of course, programming in DirectX is much easier than programming in OpenGL. Perhaps Microsoft will release a Linux version for DirectX? Will Microsoft ruin itself? I think we'll see a famous game running on Linux only in a lot of time, but not in recent times. Unfortunately, Windows is too good for that. I'm sorry for my poor english..."
    author: "Daniele"
  - subject: "Re: About games..."
    date: 2004-03-12
    body: "But we already have native versions of UT, UT2003, UT2004, Quake3, Enemy Territory , Neverwinter Nights to name but a few very famous games."
    author: "mrew"
  - subject: "Re: About games..."
    date: 2004-03-13
    body: ">of course, programming in DirectX is much easier than programming in OpenGL\n\nI bet you're not a programmer....."
    author: "anonymous"
  - subject: "Re: About games..."
    date: 2004-03-15
    body: "Did you ever write programs for DirectX AND OpenGL ??? \n1.) When you've never done anything with 3D graphics, OGL is more comprehensive than DX.\n2.) We can't actually compare DX to OGL. Direct3D vs OpenGL would be better.\n3.) DX has advantages because u get everything you need out of one hand (another reason why some people get the impression M$ Windoze is more organised than GNU/Linux) (API for Graphics, Input, Networking, Sound,...) We'd rather compare it to SDL (which makes use of OGL)\n\n\nLinux is doing ok with graphics, but I think it could be doing better. The X Window approach is so \"Mainframe-Era\" thinking of terminals etc. Who needs all this overhead on a desktop machine ? What Desktop-Linux needs is a efficient, cleanly implemented, industrially supported (graphics hardware manufacturers), module based (if u really need network transparency), ACCELERATED frame buffer.\nsorry, if that was a bit OT >:-)="
    author: "markd"
  - subject: "I Agree"
    date: 2004-03-10
    body: "Yep. I'm running KDE 3.2.1 on Gentoo - joy :). Portage, like apt is a very nice tool. Running Qt 3.3.1 just make sure that you have FontConfig up and configured properly.\n\nI really am loving Kontact, but it took me a while to get the weather and RSS newsfeed stuff up and running. I never use IMAP so I haven't been affected by the bugs that have afflicted some. What we need now is a good integrated Office suite that can take advantage of the spell checking, printing and other kool KDE components. KDE OpenOffice will be good, but I really, really like Kivio and other parts of KOffice. Nothing in OpenOffice can match them.\n\nI really like Plastik, but there is perhaps some more work to come on it, so the devs are right about it not being the default theme. Creating a cool-looking but totally usable theme is pretty damn near impossible. I hope Plastik can achieve it.\n\n\"3) It's easy to use.\"\n\nDamn right. The usability wobblers people have been throwing are unecessary, and the criticisms are way, way over the mark. If you know Windows, you can do KDE. I know, I've tried it with people. The problems KDE have are mainly to do with the organisation of certain dialogues and components. Konqueror is a good example. However, everything is there, nothing should be removed and everything is totally fixable. As soon as I get this server and wireless network sorted I'm straight over to the KDE Quality website and I'm doing some reading.\n\nPeople like me respect Macs and Mac usability totally, but the way they do that is part of Apple's target market and speciality. The vast majority of people, especially business users, do not want their desktop looking like a Mac. Believe me, I know.\n\n\"I said that if you play a lot of games, then maybe KDE isn't for you, but I wasn't exactly right on that score.\"\n\nHoly crap Savannah! Don't say that. My nicely configured Gentoo machine runs Return to Castle Wolfenstein and it is eagerly awaiting Doom 3 any week now (slavouring and drooling and wondering just how much work I'm going to get done)!  To while away the time though, KDE's games are nice.\n\n\"You want AIM? ICQ? MSN Messenger? No problem. Kopete on KDE gives you access to all that and more. It's got a beautiful user interface, awesome icon sets, and it's all integrated into one window and in your Kicker dock panel. It works flawlessly as far as I can see, and I love it. Chat to your heart's content.\"\n\nI've never used chat programs. I'm an IT person, used everything under the sun, but have never seen the point in chat programs. Now that I have always-on internet access and Kopete I suppose I'll have to try it now.\n\n\"I always have music playing with Juk. It's integrated with a small applet in my Kicker as well (MediaControl) which lets me control everything from the kicker, even when I'm not on the desktop where Juk is. I just love that.\"\n\nJuk is pretty darn cool. I've heard it compared to iTunes, but iTunes is a bit different. I've waved goodbye to XMMS as I've realised I can live without the visualisations. I just want my music - preferably organised :).\n\nAdditionally, I am now using Konqueror full-time as my web browser, and Mozilla has gone totally. Konqueror renders the vast majority of pages brilliantly, and with the partnership with Apple and Safari and improvements still to come I see nothing but a very, very, very bright future. I've seen some sniping at Konqueror, mainly that it still cannot render most web pages. It just isn't true. Web pages that I've seen that stump Konqueror also stump Mozilla. KWallet integration is very welcome also. When people talk about an integrated desktop, this is what people want to see.\n\nThe most underrated app has to be KDict, although there are probably more gems like this so I need to do more exploring. I brought my laptop back home yesterday and my Mum and her crosswords loved this program. Kudos!"
    author: "David"
  - subject: "Konqueror Rendering"
    date: 2004-03-11
    body: "> I've seen some sniping at Konqueror, mainly that it still cannot render most web pages. It just isn't true.\n\nActually it is true, it just depends how you build your web pages. Semantic HTML, CSS, and W3C DOM are very much in vogue nowadays. Sites that make heavy use of these technolgies tend to break badly in Konqueror. Since the vast majority of the web is tables based with a sprinkling of CSS and Netscape DOM, you see very few problems. The people complaining are often web developers who want all the benefits of the current standards, or those who use newer sites.\n\nWhen I was using KDE 3.1 not a single one of the web sites I created was even remotely usable in Konqueror, in KDE 3.2 they are just badly broken. I never used to bother trying to fix stuff for KDE 3.1 because it was impossible; now it is just a pain, but I do bother, and report all the problems to bugs.kde.org with testcases so the problems can be fixed.\n\nI hope very much that KDE 3.3 will ship with a Konqueror that has support for modern standards equal to, or in excess of, IE 6."
    author: "Dominic Chambers"
  - subject: "Re: Konqueror Rendering"
    date: 2004-03-11
    body: "I have to say I totally disagree with this. I very rarely find a page I can't view properly in konqueror. I also create a lot of pages, and I don't really have much trouble with konqueror - I have had lots of problems with Mozilla's loss of colspan info when the display attribute of a page is changed though. If you really have that much trouble with the pages you create, I'd consider that you might be designing pages that are IE specific.\n\n"
    author: "Richard Moore"
  - subject: "Re: Konqueror Rendering"
    date: 2004-03-11
    body: "As I said it depends whether your a big CSS user or not."
    author: "Dominic Chambers"
  - subject: "Re: Konqueror Rendering"
    date: 2004-03-11
    body: "I do not agree. Konqueror have had some CSS bugs in various versions, and possibly still have some. So does IE (a lot more actually), and all the other browsers. But it's been a long time since Konqueror was the least CSS compliant browser. I do a lot of heavy CSS programming, and Konqueror shows it very well."
    author: "Niels"
  - subject: "Re: Konqueror Rendering"
    date: 2004-03-11
    body: "In my opinion IE6 has more reliable support for CSS than Konqueror; Konqueror has more breadth, but IE6 has fewer bugs.\n\nI develop my sites in Mozilla (avoiding CSS-2 features IE doesn't support), then I test my pages in the other browsers. Of those browsers, I would rate their CSS-2 support, as defined by the amount of crap I have to do to get them to work, as:\n\n  1. Opera 7\n  2. IE6\n  3. Konqueror (KDE 3.2)\n  4. MacIE 5.1\n  5. IE5\n  6. Konqueror (KDE 3.1)\n\nIE6 and Opera7 are easy. Konqueror on the other hand always gives me trouble. The latest site I did <http://www.daisygoodwin.co.uk/> was unusable on Konqueror. I filed 4 bug reports in connection with it (#77048, #76982, #77057, and #76948), and made fixes to the web page to deal with those issues. These are just my experiences."
    author: "Dominic Chambers"
  - subject: "Re: Konqueror Rendering"
    date: 2004-03-11
    body: "The above statement is totally, completely, inconcievably WRONG.  Konqueror has ALWAYS been the absolutely worst at rendering CSS, regardless of version or spec.\n\nI was at LinuxWorld here in NYC right when 3.2 was coming out, and I wandered over to the KDE booth to see what was going on. The first thing I did - when presented with konqueror - was open http://www.csszengarden.com/, a brilliant collection of pure CSS layouts contributed by many different artists. A good majority of them failed to render properly (even though IE/mozilla/firefox did).\n\nSo I immediately gave up on konqueror, because I just can't work with a browser that is that handicapped regarding CSS.  No complaints, but no hope either.\n\nThen comes 3.2.1, and I go ahead and slap the RPMs on my FC1 box.  Hey, CSS works in konqueror!  And note that I didn't even complain, just pointed out the site to the folks at the booth!\n\nMan, if I could get that kind of service with other things..."
    author: "mitchy"
  - subject: "Re: Konqueror Rendering"
    date: 2004-03-11
    body: "Considering that I'm running 3.2.1 and you've said things now work then my statement is right. I didn't do as much browsing with 3.2.0 as I'm doing now, but it was clear that Konqueror had way, way improved.\n\nUnfortunately I do not do selective experiments on Konqueror - I browse the web every day with it. Result? Very, very few problems. Qt and KDE also combine to create a damn nice looking app."
    author: "David"
  - subject: "Re: Konqueror Rendering"
    date: 2004-03-14
    body: "That's what we get for having hundreds of developers on the project :) Looks great, runs great and has even better user support ;) And yup, as you said 3.2.0 had horrible CSS support (it even broke some of my own, more simple pages), while 3.2.1 has well, wow :)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: Konqueror Rendering"
    date: 2004-03-11
    body: "> I have to say I totally disagree with this. I very rarely find a page\n\nYeah, but when it happens it tends to annoy a lot. Especially if you \nwere trying to pay your bills at 23:54 on its due day ;)\n\nhttps://www511.sampo.fi/A56/sampo_representation/RepresentationApp\nhttp://www.autohus.de/search/form.php?lang=English\n..\n"
    author: "foo"
  - subject: "Please fill in a bug report"
    date: 2004-03-11
    body: "<p>The Konqueror developers know where most of these issues are, but not all.  So everytime you encounter something that Konq doesn't get right, go to bugs.kde.org, and drop it in so they can fix it."
    author: "bluGill"
  - subject: "Re: Please fill in a bug report"
    date: 2004-03-11
    body: "Yes, I'm doing just that, and I'm sure 3.3 will be golden."
    author: "Dominic Chambers"
  - subject: "Re: Please fill in a bug report"
    date: 2004-03-11
    body: "Bug number 40383. Broken in 2.x, fixed in 3.0, broken again in 3.1 and continues. It has been reopened for almost a year - and nobody seems to care."
    author: "Alex"
  - subject: "Re: Please fill in a bug report"
    date: 2004-03-11
    body: "let me make a suggestion, which you can ignore if you wish: if you are cordial and nice, people will work with you; if you're not people tend to start ignoring you.\n\nkonq has ... *checks* ... 1400 bugs open right now. it's a huge app that tackles some of the hardest areas of mordern desktop computing: file management and web browsing. given the large number of bugs and difficult nature of the tasks, the developers try and prioritize the bugs they deal with as they certainly can't deal with them all at once. \n\nnow, when someone accuses them of not caring (esp. when they are in the process of trying to help), of being incompetent, etc and rants on about irrelevant bugs and uses snide remarks, that person is likely to get less attention than the person with an equally important bug but who is helpful and professional.\n\ndoes that make sense to you?"
    author: "Aaron J. Seigo"
  - subject: "Re: Please fill in a bug report"
    date: 2004-03-11
    body: "I've been reading your bug report and it seems you show a real attitude problem throughout."
    author: "ac"
  - subject: "Re: Please fill in a bug report"
    date: 2004-03-13
    body: "Wow, I don't see that that bad an attitude problem often. Not only that, but it appears to work fine if the id is set to IE. (So complain to the site please, konqueror is not going to change it's default id globally for you.) Unless the image shown in konqueror & firefox is an illusion, it works. (I admittedly can't read hebrew, so I can't tell if it's showing the right text.) \n\nPlease be curtious when reporting bugs, Alex. \n"
    author: "James L"
  - subject: "Re: Konqueror Rendering"
    date: 2004-03-12
    body: "I agree.  There's a reason why I use Firefox as my primary browser.  And that's cause it handles 99.9% of the pages I view while konqi is NICE but... it still fails miserably on certain sites."
    author: "Jeff Stuart"
  - subject: "Kontact - RSS & Weather"
    date: 2004-03-11
    body: "I thought the PIM people had silently dropped support for that since I couldn't find out where to configure it, and I looked all over the place. Can you tell us how it's done?"
    author: "Dominic Chambers"
  - subject: "Re: Kontact - RSS & Weather"
    date: 2004-03-11
    body: "You have to install kdetoys for kweather, kdenetwork for dcoprss and kdeaddons for the news summary."
    author: "Anonymous"
  - subject: "Re: Kontact - RSS & Weather"
    date: 2004-03-11
    body: "Wow... that's a bit all over the place isn't it?"
    author: "ac"
  - subject: "Re: Kontact - RSS & Weather"
    date: 2004-03-11
    body: "with the exception of the news summary plugin which is in the addons package where it belongs, they are generic components which kontact is capable of tapping into. if they were kontact specific, i'd agree with you, but they aren't =)"
    author: "Aaron J. Seigo"
  - subject: "Cool-looking and usable theme - try this one."
    date: 2004-03-13
    body: "Perhaps you'll like this type of theme:\n\nWindow style: System++\nIcons: Footprint (for IT person; I find the default set cool yet girlish)\nKicker: no wallpaper\nStyle: Ceramic\nDesktop Wallpaper: according to taste, I use Suzuki motorcycle racing wallpers\nColours: MacOS (modified KDE default - see attached file from KDE 3.1.4)\nWhite background and no wallpaper in konqueror file browser.\n\nSimple yet elegant, and do not distract me from work."
    author: "Lex Ross"
  - subject: "Re: I Agree"
    date: 2004-03-14
    body: "Yeah, most of the stuff the Gnome people have been talking about are, um, crap.  I find GNOME to be more feature-free these days than MacOS.  That a pro-UNIX crowd could make a system that, IMHO (and I administer such machines, so I don't feel like I'm too far off the mark :D) is more opaque, lacking in features, and assumes more stupidity on my part than both Windows and MacOS Classic, is surprising.\n\nI gave the GNOME 2.6 beta a try.  Heck, I tried using one of the neato Nautilus features: CD burning!  I wanted to burn a disc with some OGGs on it; I ripped them at home, and wanted to listen at work, and since I lack a good, fast connection at home, \"Sneakernet\" is the only option available.  As far as I can tell, Nautilus first tried to burn an audio CD.  I could be wrong about that, but I couldn't mount the CD and my DVD player attempted to play it :D  Then I tried to erase it (using an option on the dialog box) and as far as I can tell Nautilus instead assumed all along that I was using the CD-RW to write multi-session CDs (and didn't erase the CD.)  I know I'm not the brightest person in the world, but I was pretty sure that I wanted a single-session data CD!  Thanks, Nautilus, for setting me straight!  I are too dumb to make these tough decisions on me own! ;-D  Not even OS X insults my intelligence to this extent.\n\nGNOME is getting better (taking the Right Approach(TM) and hiding the more complex features from the casual user, among other things) but I see it as a long uphill battle.  Also, with people like Miguel wanting to largely abandon the C/C++/other apps for C# apps (not that that's necessarily bad, mind you) the future looks rocky for the GNOME project.\n\nI don't care if KDE, GNOME, or some other environment is dominant.  Let me stress that.  I just want what works.\n\nAt one point I had been a GNOME convert, and then even advocated environments such as GNUstep and ROX in the past.  For better or for worse, nowadays I'm firmly in the KDE camp.  I'm even eagerly awaiting RangerRick/others' port of KDE apps over to OS X; the boss has been bugging me about groupware, and at some point we'll be an OS X house.  It'd kick ass if I could get everyone using Kontact and use Kroupware to coordinate everything. ;-D"
    author: "regeya"
  - subject: "Virusses"
    date: 2004-03-10
    body: "Thanks for the nice review, but there is one point I'd like to get into. One of the main reasons that there are so many virusses for Windows nowadays, is that there are so many windows users. It just makes no sence to write a Linux virus, because there aren't enough users to actually spread it around. That does not mean that it is impossible to do. In fact, I'm pretty confident dat it is, and also that KMail is vunerable in some way (no, I don't know how, but I'm sure someone will find out...).\nAs more and more people are starting to use Linux with KDE, the changes of virusses are also getting bigger. We should not underestimate this, and while I still lauch about the stupid virusses (in my view, most of the modern virusses shouldn't even be called like that as they rely soley on click-happy users), I fear the day will come that I need to install anti virus measures on my linux system as well."
    author: "Andr\u00e9 Somers"
  - subject: "Re: Virusses"
    date: 2004-03-11
    body: "I think Savanah implies that that is the reason. She says I don't have to worry because they don't run on KDE.\n\nAs to whether KDE/Linux is inherently more secure than Windows, I think you would have to agree that it is. The default policy in KDE is to treat HTML as a security problem, KMail doesn't allow you to accidentally start programs, and unlike Windows, you don't have to run your mail client as root."
    author: "Dominic Chambers"
  - subject: "Re: Virusses"
    date: 2004-03-11
    body: "There seems to be a big push to make HTML composition in KMail the default. How much longer after that until \"Prefer HTML to Plain Text\" becomes the default as well? Then the automatic execution of binaries just to save a few mouse clicks? Don't let the camel's nose in the tent!\n"
    author: "David Johnson"
  - subject: "Re: Virusses"
    date: 2004-03-11
    body: "Hi David,\n\nThere are no plans to make HTML composition in KMail the default. I wonder what made you think otherwise. I mean considering sending HTML mails on KDE mailing lists is a no no, having KMail send HTML mails by default is pretty much unthinkable. (At least not in KDE cvs, since KMail is free software distributors can always make their own changes, that's beyond our control.)\n\nHTML mail support is being worked on. I realize many people would prefer to see no HTML composition support at all, but still far more do want to see it supported, (and they have valid reasons like intra organizational mail). \n\nBTW HTML mails sent by KMail will be multipart/alternative messages so they can still be viewed by mail clients that don't support viewing HTML mails.\n\nDon.\n"
    author: "Don"
  - subject: "Re: Virusses"
    date: 2004-03-11
    body: "OK, you are right, unices are probably inherently more secure than Windows. However, most modern virusses just ask politely to open the attached file. I think that could hit a KDE user too, even if the attached program or script doesn't run as root, it can still do damage and/or spread itself."
    author: "Andr\u00e9 Somers"
  - subject: "Re: Virusses"
    date: 2004-03-11
    body: "Uhm, I'm not running Outlook using root (Administrator). You can do that, you know."
    author: "rajan r"
  - subject: "Re: Virusses"
    date: 2004-03-12
    body: "That's good. Unfortunately running Windows in a networked office environment is not that simple. Permissions in Windows are a joke, and many essential services, like printing, sometimes just don't work unless you are an administrator. Recent viruses, worryingly, have proved that Windows permissions are so easy to get around it is as if they aren't even there. Sometimes IT people have no choice to log people in as administrators. It is either that or things don't work."
    author: "David"
  - subject: "Re: Virusses"
    date: 2004-03-12
    body: "... Of course, Linux is more secure than windows. Anyway if an Linux virus is trashing my personal data, which is possible running as the user I'm logged in, its doesn't matter that the system itself is secure. My Data is lost !!!\nSo it does matter to take care of the email you get, a firewall up and runnin (of couse right configured) and have an antivir programm running in the background. There are some free out there for Linux.\nDon't behave like an \"normal\" Windows user - being blind to things that are so obvious.\n\nuwe"
    author: "Uwe Liebrenz"
  - subject: "Re: Virusses"
    date: 2004-03-12
    body: "Use the source, Uwe!\n\nInstall unison.\n\nThen, as root, create a batch job that nightly syncs your home folder to a folder owned by root, and keep, say, a week of changes tar.bzip'ed somewhere.\n\nThat way, if you get virus'd, you have a backup. The nature of a multiuser system makes this *almost* as good as a real backup (if the computer is hit by a meteorite, the backup does no good).\n\nI really should write a recipe to do this.\n\n"
    author: "Roberto Alsina"
  - subject: "Re: Virusses"
    date: 2004-03-13
    body: "Argh. I meant rdiff-backup, not unison.\n\nAnd I did write the recipe for this. I will publish it tomorrow at http://pycs.net/lateral :-)"
    author: "Roberto Alsina"
  - subject: "Re: Virusses"
    date: 2004-03-13
    body: "And here it is: http://pycs.net/lateral/stories/26.html\n\nSomeone should give rdiff-backup a GUI, though :-)"
    author: "Roberto Alsina"
  - subject: "Re: Virusses"
    date: 2004-03-11
    body: "<i>Thanks for the nice review, but there is one point I'd like to get into. One of the main reasons that there are so many virusses for Windows nowadays, is that there are so many windows users. It just makes no sence to write a Linux virus, because there aren't enough users to actually spread it around.</i>\n\nThat is not true on so many levels with dozens of different proofs. I suppose that it is just a FUD spread by MS or avirus companies...\n\nOK, here we go:\n\n1. If what you're saying is true, if the number of viruses depended on the number of machines to infect, than we'd have legions of worms infecting these 60% WWW servers run by Apache. But you know what? The only worms that infect WWW servers are those infecting MS offerings.\n\n2. There was a computer called Amiga. It had multitasking OS, harddrive, network interface and absolutelly no security. At the peak of its popularity it had smaller market share than Macs or Linux desktops have now. And you know what? There were dozens if not hundreds viruses on Amiga.\n\n3. There was similar machine: Atari ST. It had even smaller market share and still had lots of viruses.\n\nPlease, don't spread the FUD.\n\nRobert"
    author: "Robert R. Wal"
  - subject: "Re: Virusses"
    date: 2004-03-11
    body: "1) Most modern virusses spread using \"social engineering\": they just ask the user to open the file. Since servers running apache arn't likely to open these messages all on their own, and administrators operating these systems can be assumed to be smart enough not to, this isn't an issue. Apache may (is? I certainly hope so!) a very save webserver, much better than MS'servers. I'm not arguing against that. We're talking about the linux desktop here. And yes, for that you do need actual users that are click-happy.\n\n2) I am not familiar with the Amiga scene. I do know there were virusses going around in my C64 time (at least, I think there were).\n\nI'm not saying that it is impossible to spread virusses on less used computers, I'm saying that I think (and I don't need MS or the antivirus companies to think for me, thank you very much) that:\na) a spreading of Linux/KDE systems to the more mainstream desktop market (as many people here would like to see, and as slowly seems to happen) will generate a group of users more likely to do less smart things, like open attachments they were not expecting.\nb) a bigger userbase will make the platform better known, more interesting for virus writers and will bring to light more vunerabilities. \n\nI am not running antivirus software myself. I still think I'm save. I just fear that that won't be the case for ever, and that we need to be carefull about saying that it could not happen to us. Call it spreading FUD if you want, I just think it would be a good idea to keep alert about potential security problems that might be exploited by virus/worm/trojan/whatever writers.\n"
    author: "Andr\u00e9 Somers"
  - subject: "Re: Virusses"
    date: 2004-03-11
    body: "None of your above points make any sense, wether you have users that are click-happy or a group of users more likely to do less smart things, like open attachments they were not expecting. Nothing of this will spread any viruses as the design of *nix, to run a program the exec bit have to be set. No sane *nix mailer sets the exec bit on attachments and none ever will. A clueless user will not know how to make the file executeable and a cluefull user will know not to."
    author: "Morty"
  - subject: "Re: Virusses"
    date: 2004-03-11
    body: "That is a good point, and it's well taken. This does make it significantly harder to execute scripts. "
    author: "Andr\u00e9 Somers"
  - subject: "Re: Virusses"
    date: 2004-03-11
    body: "Then just be quiet and let the adults speak?"
    author: "Joe"
  - subject: "Re: Virusses"
    date: 2004-03-12
    body: "And this is inherently at the root of why you should not connect to the internet with windows but use a *nix client. DOS is NOT a network operating system and as such doesn't have such safety measures. Note also problems with the FAT file system being designed for 360K floppies and needing to be defragged. Sure they are making improvments, but their roots are a stand alone toy and it's pretty much a lot of patching it and selectively continuing the same thinking in many areas, like default enabling macros that can erase the file system and not building in confirmation for first time execution of destructive or foreign scripts.\n\nLet's face facts. Middle schoolers can bring the internet to it's knees because MS hands out the tools to do it like candy, refuses to allow the least inconvenience by insecure defaults and still considers it a low priority. Witness their past errors. If they have a concern with data loss they sue you into oblivion. Their defensive barrier is the lawsuit, not technology. Otherwise they would have been more concerned about Russian hackers cruising the Redmond campus for six months with free reign. MS is the quintessential computing toy software company. System failure and compromise only matter if they hurt revenue and they're a monopoly. Cased closed.\n\nIn the final analysis it is the user who eventually makes something secure or unsecure, however between *nix and Windows there is no real way to say they are on equal footing. In 2000 I was told at an ISP sponsored user conference that there are 4000 new Windoze exploits a month introduced.\n\nAs a side note I played with confirmed XSS scripting exploits that could reliably be reproduced in IE and found only a small percentage success in KHTML. Also I have seen security fixes issued in KHTML the day after the exploit was announced that affected most browsers and MS said it would not issue a fix as it was not a critical flaw. IE and outlook also download \"fixes\" without user confirmation from sites that offer validation with their request. This is another potential exploit. Add the fact that admins are afriad to apply Windows fixpacks because they usually break other things and the picture is very clear indeed."
    author: "Eric Laffoon"
  - subject: "Re: Virusses"
    date: 2004-03-11
    body: "Ya, this is a common view that consistently pops-up from time to time.  Unfortunately any half-intelligent security analysis proves it entirely wrong.  If your statement were true, then Apache would be responsible for twice as many http vulnerabilities as any other http server.  But IIS is actually responsible for more http vulnerabilities than any httpd service.  And Apache is only the most striking example.  There are literally dozens of examples of software that is/was more popular than the Microsoft's version (from video games to personal finance software to mail servers...) that had fewer vulnerabilities.\n\nI have wrote about this before: http://www.rockerssoft.com/brockers/archives/000077.html  \n\nbrockers"
    author: "brockers"
  - subject: "Re: Virusses"
    date: 2004-03-12
    body: "I don't think this is true to be honest. No one except Microsoft writes software that takes in external scripts and runs them as if it's going out of fashion. A virus writer would have to work 100 times as hard to get a good virus up and running, and 1000 times as hard to make it spread from machine to machine on a Linux desktop. The latter is what really kills Windows.\n\nMicrosoft people like to play the popularity game. Unfortunately Outlook and other bits of Microsoft software are just total crap."
    author: "David"
  - subject: "Re: Viruses"
    date: 2004-03-12
    body: "Well, you're not 100% safe on linux:\n\nTry the following:\nTake one of those MyDoom mails (I'm sure you have one) and click the attachment as the mail tells you too.\n\nThen ignore the two warnings kmail pops up, and bingo! You have MyDoom installed and running on your Linux box. And it works very good there.\n\n(Of course you need to have Wine installed, and have wine integrated in KDE like kappfinder does)\n\nSo with good social engineering, Windows Viruses are a threat on unix."
    author: "Anonymous Coward"
  - subject: "Nice Review"
    date: 2004-03-10
    body: "Really nice review. Totally different to the typical technical reviews you see being posted. It's interesting to hear that you've had no problems heavily customising it since you class yourself as a non-technical person.\n\nAs for stability, I am surprised. I am finding KDE 3.2 to be quite buggy, but I am starting to think it's just problems with my 3.1 configuration files, or with my Distro -- ArkLinux.\n\nHow did you manage with installing Debian? I really want to move to Debian for the stability and the huge package repository, but am scared off by the installation process -- I don't have a week to spend doing an install. Any tips?"
    author: "Dominic Chambers"
  - subject: "Re: Nice Review"
    date: 2004-03-11
    body: "There are many debian alternatives.  I downloaded and installed MEPIS Linux.  It rocks!  You can boot from the CD, it has awesome hardware autodetection, and is incredibly easy to use.  You can use the debian repos to stay up to date as well.  I am using it full time now after having used SuSE - I love it!"
    author: "trlpht"
  - subject: "Re: Nice Review"
    date: 2004-03-11
    body: "Excellent! Thanks for the tip. I am actually downloading Mepis as we speak -- have been for the past three days (I have a modem)! I only asked because I wasn't able to google 100% positive info that a Mepis install really would give me Debian. I can safely continue downloading ;-).\n\nCan't wait!"
    author: "Dominic Chambers"
  - subject: "Re: Nice Review"
    date: 2004-03-18
    body: "If you want to install Debian I would recommend using a recent debian-installer snapshot from http://www.debian.org/devel/debian-installer/ it is much easier to use than the old installer."
    author: "Chris Cheney"
  - subject: "hooray!"
    date: 2004-03-10
    body: "hey savanna, good to see you're still around! =) i missed your unique voice in the KDE world while you were busy with life, etc. \n\nlooking forward to more of your articles!"
    author: "Aaron J. Seigo"
  - subject: "Re: hooray!"
    date: 2004-03-11
    body: "People with cool names get all the attention. \n\n\"Aaron\" and \"Savanna\"...Nobody cares about poor Joe.\n"
    author: "Joe"
  - subject: "Re: hooray!"
    date: 2004-03-11
    body: "aaaaaw... i think somebody needs a hug! ;-)\n\n** HUGS **"
    author: "Aaron J. Seigo"
  - subject: "frozen buble"
    date: 2004-03-11
    body: "It is not a kde games (it's some mandrake guys who did it) and it's not a tetris like game.\nTry klines and klickety for nice puzzle games."
    author: "mi"
  - subject: "suugggaaaa"
    date: 2004-03-11
    body: "Oh yes, what a supersweet review. Not a single bad word about things that don't work or are not that convenient as in windows. Reminds me to paid reviews about microsoft software.\n\nNot a single crashing kde applications? Never - if you have a stable system, windows applications don't crash more often, than kde applications."
    author: "Carlo"
  - subject: "Re: suugggaaaa"
    date: 2004-03-11
    body: "If you'd like to write your own review, then go ahead - we'll accept negative stories too.\n"
    author: "Richard Moore"
  - subject: "Re: suugggaaaa"
    date: 2004-03-11
    body: "I'm not interested in writing reviews. The point is, that this text isn't a review, but a glorification. A review is worthless, when the author not even tries to be objective and to reveal existing problems and shortcomings (beside the good things of course)."
    author: "Carlo"
  - subject: "Re: suugggaaaa"
    date: 2004-03-11
    body: "Sometimes people don't want to pick huge gaping faults - people just want to get things done. Revolutionary I know."
    author: "David"
  - subject: "Re: suugggaaaa"
    date: 2004-03-11
    body: "Hm, could you please tell me, what your comment has to do with this \"review\" and my comment? I heavent't heard of a _most_uncritical_review_contest_ ever."
    author: "Carlo"
  - subject: "Re: suugggaaaa"
    date: 2004-03-12
    body: "Err. How about Savanna doing a review of KDE and finding that it does what she wants? She isn't necessarily looking for faults at every turn, but does it clearly do what she wants? Yes. Conclusion? No this isn't a totally critical review looking for faults and cracks, but within the context of what Savanna wants from her desktop environment, and wants to get done, KDE 3.2 is quite clearly more than good enough. Sorry."
    author: "David"
  - subject: "Re: suugggaaaa"
    date: 2004-03-11
    body: "this is a Savanna Says, not a Consumer Reports review =) as a review it might be worthless, as an overview from the perspective of a happy user it's priceless =)"
    author: "Aaron J. Seigo"
  - subject: "Re: suugggaaaa"
    date: 2004-03-11
    body: "lol, of course priceless - in german exists a \"nice\" phrase: blowing sugar into someones ass - maybe kde devs like this ;)"
    author: "Carlo"
  - subject: "Re: suugggaaaa"
    date: 2004-03-11
    body: "i just find it hard to believe that you know more accurately how Savanna feels about 3.2 than Savanna herself does, and that you feel fit to comment on her means of expressing it as if you are an authority on that. i also find it unfortunate that you lack the ability to smile and enjoy positive things when they come around just because they are positive and light hearted.\n\nor is there a German phrase that explains how being an insulting pessimist is a positive character trait?"
    author: "Aaron J. Seigo"
  - subject: "Re: suugggaaaa"
    date: 2004-03-12
    body: "Oh... all this happiness is starting to upset me. What can I do to ruin it? \n\nI think that's got to be one of my favorite lines from the comedian Christopher Titus. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: suugggaaaa"
    date: 2004-03-12
    body: "Yeah, it's horrible. :)"
    author: "Carlo"
  - subject: "Re: suugggaaaa"
    date: 2004-03-12
    body: ">i just find it hard to believe that you know more accurately how Savanna feels about 3.2 than Savanna herself does, and that you feel fit to comment on her means of expressing it as if you are an authority on that.\n\nNope. I just have a very different idea what a review is.\n\n>or is there a German phrase that explains how being an insulting pessimist is a positive character trait?\nLet me think about it, maybe I find one. In fact I wrote my opinion and if you mean the \"sugar sentence\" I wrote \"maybe\". So call me a pessimist, if you like, but not insultant."
    author: "Carlo"
  - subject: "Brains anyone?"
    date: 2004-03-12
    body: "This isn't a review, this is \"Savanna Says:\" it is just her opion on KDE 3.2, it is not meant to be 100% objective."
    author: "Alex"
  - subject: "Re: suugggaaaa"
    date: 2004-03-12
    body: "Oh for crying out loud. Look. This is just a KDE user who's been using KDE 3.2 and has given her general opinion, or 'review', on it. Can she get things done? Yes. Does it do what she wants? Yes. Is she actively looking for faults at every turn? No, but that doesn't mean that there aren't any."
    author: "David"
  - subject: "Re: suugggaaaa"
    date: 2004-03-11
    body: ">>Not a single bad word about things that don't work or are not that convenient as in windows.\n\nHmm, now that you mention it, you are correct.  Part of that is probably because Savanna hasn't used KDE 3.2 for very long.  I know that when I got my brand new KDE 3.1 desktop, it was several months before I began to be aware of the bugs and limitations of the desktop.  I was too focused on the exciting new features that I didn't notice many of the little things that irk me so much now.  Now I can hardly wait to get KDE 3.2, because I am no longer satisfied with 3.1.\n\nI expect that once I get 3.2, I will absolutely fall in love with it for about two or three months, then I will begin to notice bugs and bothersome little design flaws.  Anyway, my point is that Savanna is still at the \"in love\" stage with KDE 3.2; a more informed, objective review is not possible until about two months down the road."
    author: "Kenny Martens"
  - subject: "Re: suugggaaaa"
    date: 2004-03-11
    body: "Did you read the \"review\"? \n\n> After being on 3.2 beta for several months, ...\n\nAnd beside less bugs, there's not a big feature difference from KDE 3.2 beta to 3.2.1.\n"
    author: "Carlo"
  - subject: "Yes, I read the review"
    date: 2004-03-12
    body: "Yes, I did read the review, and I did see that she used the beta for quite a while.  But upgrading from a beta to the real thing is not a trivial change, as far as bugs go.  A several month old beta is bound to have some serious bugs, non-trivial bugs.  The sort of bugs that make a new feature unusable.  So in that sense, there could be a big feature difference from KDE 3.2 beta to 3.2.1.\n\nIf anyone here used those KDE 3.2 beta and subsequently switched to 3.2.1, I would be interested to hear about the feature differences.  If I'm wrong, I would like a chance to change my mind."
    author: "Kenny Martens"
  - subject: "Re: Yes, I read the review"
    date: 2004-03-12
    body: ">If anyone here used those KDE 3.2 beta and subsequently switched to 3.2.1, I would be interested to hear about the feature differences. If I'm wrong, I would like a chance to change my mind.\n\nDid it. But I really couldn't name big added features from the first beta to 3.2.1. That doesn't mean that there are changes and a lot of bug fixes of course. The step from 3.1.x to 3.2.x is huge, though."
    author: "Carlo"
  - subject: "chess"
    date: 2004-03-11
    body: "My personal opinion is that a integration of a chess client in the KDe Games package is a must.\n\nslibo or knights are mature KDE programs. Please ship them with KDE.\n\nthe first impression counts. Many people try out the games first as first time users. So consistency and quality with the games is very important.\n\nWhen I see students at the university's computer pool approximately 5 % of them play chess via a web browser client."
    author: "Jan"
  - subject: "Re: chess"
    date: 2004-03-11
    body: "http://slibo.sourceforge.net/\n\nKnights seem to be unmantained for a year"
    author: "Jan"
  - subject: "Re: chess"
    date: 2004-03-11
    body: "Slibo is alpha software and currently unmaintaned.  The best Linux/Unix user interface for Chess is still Xboard even though it uses plain Xlibs and many of Xboard's key options must be set by commandline parameters at start up."
    author: "Anonymous"
  - subject: "Re: chess"
    date: 2004-03-11
    body: "Personally I prefer eboard (http://eboard.sf.net/) to xboard. "
    author: "Kasper H."
  - subject: "Re: chess"
    date: 2004-03-11
    body: "What happened to Slibo? I tried to, but it does not compile... depends on qt 3.0 ..."
    author: "Johannes"
  - subject: "Re: chess"
    date: 2004-03-12
    body: "Seriously?? How can you possibly like xboard over Knights?  What does xboard do that knights does not?  BTW I have submitted a patch a while ago and talked the the developer and he said he was still working on it (specifically email support was on the top of his list.)\n\nbrockers"
    author: "brockers"
  - subject: "Re: chess"
    date: 2004-03-12
    body: "Here is a pick of knights with my prefered theme (I love the sudo-3D UI for chess) http://www.rockerssoft.com/brockers/images/knights.png  The two squares are in green because Knights shows available moves when you right click on a pawn or piece.  The other windows is the freechess server used to play internet games with Knights.\n\nbrockers"
    author: "brockers"
  - subject: "Re: chess"
    date: 2004-03-12
    body: "I prefer slibo but I can't compile it on my maschine. The theme looks a little bit broken."
    author: "Jan"
  - subject: "Re: chess"
    date: 2004-03-12
    body: "Xboard allows you to edit position and to play against CPU from an arbitrary position.  It is also mostly bug free."
    author: "Anonymous"
  - subject: "Re: chess"
    date: 2004-03-12
    body: "slibo provides more options, however I didn't get it run under 3.2 yet."
    author: "Bert "
  - subject: "Re: chess"
    date: 2004-03-12
    body: "I was thinking the same thing for Go.\n"
    author: "John Tapsell"
  - subject: "Re: chess"
    date: 2004-03-12
    body: "I like KNights the best and their developer said that he was considering making it a part of KDE 3.3."
    author: "Alex"
  - subject: "Re: chess"
    date: 2004-03-12
    body: "i'd love a nice networkable KDE Go game =) i checked out QGo a LONG time ago, probably time to do so again..."
    author: "Aaron J. Seigo"
  - subject: "the best desktop i ever had"
    date: 2004-03-11
    body: "i think kde deserve 5/5 stars"
    author: "Sandesh Ghimire"
  - subject: "the Bubble"
    date: 2004-03-11
    body: "\"and then even the best tetris-like game ever called Frozen Bubble (you have to try this)\"\n\n\n..of all that rules, the Bubble rules all.."
    author: "ac"
  - subject: "yes. it's wonderful."
    date: 2004-03-11
    body: "Even Kopete is usable!!! I've criticised it's stability before, but for the first time ever I've had it running two days without a single crash!\n\nNext app I'm gonna try is JuK instead of RhythmBox (which has been more stable so far and faster at scanning my mp3s)."
    author: "c"
  - subject: "weather applet"
    date: 2004-03-11
    body: "what's the weather applet he wpeaks about ? \n\n\"I also have a nice little weather updater on my Kicker bar with pretty icons telling me the temperature and what the conditions are outside\"\n\nthanx"
    author: "cyprien"
  - subject: "Re: weather applet"
    date: 2004-03-11
    body: "Judging from the giggling at 'poor virus' users,\nshe, uses KWeather\n\nright click on the Kicker bar -> Add -> Applet -> KWeather"
    author: "ac"
  - subject: "3D games?"
    date: 2004-03-11
    body: "Where I can find kdegames3d-3.2.1.tar.bz2?\n"
    author: "Nobody"
  - subject: "More good reasons to use KDE"
    date: 2004-03-11
    body: "Great review with positive comments.\n\nI love K3b for writing CDs/DVDs and GuardDog to set uop my firewall. Both are excellent and totally Free. No need to get hacked versions of software to do the job on Windows, like Nero and ZoneAlarm.\n\nI can't wait to get home to upgrade from KDE 3.2.0 to KDE 3.2.1 !\n"
    author: "Paul S"
  - subject: "Stability"
    date: 2004-03-11
    body: "About the stability of KDE: up to 3.2.0, KDesktop would crash the first time* I would drag a file over it, since upgrading to 3.2.1 this does not seem to happen anymore. That would make this version really great; I have not experienced any crashes yet.\n\nBut actually, the very page I am typing this in exhibits a rendering bug of Konqueror - the large text box is placed on an entirely wrong place or renders incorrectly when I (un)maximize the window. Yes, I'll submit a bug report.\n\n* After restarting it things would normally work"
    author: "Daan"
  - subject: "Re: Stability"
    date: 2004-03-11
    body: "I use now KDE3.2, I thing it's very good, and damn fast, but it is a little bit too buggy... There is really a need of fixing bugs. \nI.e. Kontakt is very buggy. I have allways to resize the preview window than looking at mail, and switching between folder, only to cite one bug. They are also much more (I allready reported this bug). I think kde 3.1 was less buggy - but it's because they are so much addition in kde3.2 that it was difficult to find all bugs. "
    author: "HelloWorld"
  - subject: "Re: Stability"
    date: 2004-03-11
    body: "kontact is only at 0.7 (IIRC) in KDE 3.2, and there is a quick release turnaround scheduled for the next version of kdepim (separate even from KDE itself! =) ... the relative newness (and therefore bugs, quirks, etc) of kontact are well known ... \n\nas for fixing bugs, we're happy to get all the help we can get there .. our code base is getting HUGE and our user base even HUGER, which means our bugs database is getting to be fairly beefy. =)\n\ni've even been known to hang out on #kde-devel on irc.freenode.net (along with others) and help new bug fixers get oriented ... see you there? "
    author: "Aaron J. Seigo"
  - subject: "Plastik"
    date: 2004-03-11
    body: "Anyone besides me thing that Plastik is easily the most beautiful window decoration in the history of the world.  Seriously!  I never cared too much about the whole WD thing, but now I find myself absolutely enamored by Plastik.  Kontact is already nicer than evolution (in what.. one year of development.) The universal sidebar is already an addiction.  Great release ppl.  \n\nbrockers  "
    author: "brockers"
  - subject: "kde thumb up"
    date: 2004-03-12
    body: "yes, plastic is nice :)\n\n\nI've been waiting to switch my wife to 3.2+ so maybe the time has come. My testing with 3.2 over the last few weeks has been pretty much a joy. And let me tell you, I had to use XP at work today trying to show someone how to change her wallpaper. XP blows compared to KDE."
    author: "macewan"
  - subject: "apt-get"
    date: 2004-03-12
    body: "I was interested to read that you had a good experience using apt-get to upgrade to KDE 3.2. I installed Debian (stable), recently, and tried to do the same. I addes the sources line listed in the release notes and it didn't work. I eventually managed to \"upgrade\" by manually uninstalling my old version of KDE and re-installing to the new one, but even that seemed to leave problems lying around. Is there something I don't understand?\n\nThanks,\n\nJerome"
    author: "Jerome Loisel"
  - subject: "Missing hyperlink in #7?"
    date: 2004-03-12
    body: "I'm not sure about this, but the last sentence of #7 goes like \"And if you don't know what Juk is, you can read my review here.\" <-- shouldn't the 'review' or 'here' - or both - be an hyperlink to whatever review is being referred to? Or does 'here' mean 'it's on dot.kde.org but I don't want to spoil the fun of digging through the archives so no further tips from me'? :-)"
    author: "Frerich Raabe"
  - subject: "Re: Missing hyperlink in #7?"
    date: 2004-03-12
    body: "Hey ya prolly.  If you didn't find it: http://dot.kde.org/1055452455/"
    author: "ac"
  - subject: "Linux Virus"
    date: 2004-03-12
    body: "Great review for the best destop environment on Linux.\n\nI just thought that I must add that Linux users must be careful about viruses too. Virus can effect us (if they have been written to attack our systems) but we should be aware of not spreading viruses to those who still run M$ Windoze systems. There is a great Open Source Linux Virus system that is covered by the GPL called Clam Anti-Virus (http://www.clamav.net), it includes features such as on-access scanning, mail scanning and on-line updates. \n\nAlso, check out the Open Anti-Virus Project at http://www.openantivirus.org .\n\nIan "
    author: "Ian Whiting"
  - subject: "Games"
    date: 2004-03-13
    body: "Just have to mention that you don't necessarily have to keep windows around for a few games.\n\nI use KDE as my desktop at work and at home, and play quite a few games with WineX (http://www.transgaming.com/).\n\nIt integrates quite well with KDE, as it creates entries in your K-Menu and on your desktop just the same as in windows, can use ALT-TAB to switch between apps so you can just pause your current vice city rampage (or whatever) and answer your friend on kopete, check to see if your download has finished, line up the next album on juk, or whatever takes your fancy.\n\nIt's not free software (yet), but a US$5/month subscription is pretty small potatoes when compared to the cost of new games.\n\nEven if that doesn't appeal, there are plenty of decent native linux games, both commercial (such as anything by id software, neverwinter nights, etc) and free (freeciv, freedroid rpg, etc). The only real peeve I have with these is most of them don't support the KDE ALT-TAB focus switching, which WineX has made me used to.\n\nPersonally, I've been hooked recently by Scorched3d (http://www.scorched3d.co.uk/), a GPLed cross platform clone of an old DOS game Scorched Earth, except in 3 dimensions and networkable, using SDL and OpenGL. Scorched Earth was like a bells and whistles version of the old artillery games you got on the microcomputers of the early 80s if you don't know it. Only an emerge scorched3d away if you are running gentoo..."
    author: "Ralph"
  - subject: "Kopete..."
    date: 2004-03-15
    body: "How is Kopete in your kicker sidebar?"
    author: "Martin Galpin"
  - subject: "Re: Kopete..."
    date: 2004-03-16
    body: "http://kde-apps.org/content/show.php?content=10326\nCheers"
    author: "Datschge"
---
Right on the heels of the KDE 3.2.1 release, <a href="mailto:savanna@kdenews.org">Savanna</a> is back with an article about her latest upgrade to the KDE 3.2 desktop. After being on 3.2 beta for several months, she wasn't expecting too much of this upgrade.







<!--break-->
<p><hr>
<h2>KDE 3.2 - A Quick Review</h2>

<i>by <a href="mailto:savanna@kdenews.org">Savanna</a></i>

<p>Last week, I finally took the time to upgrade to the fully released KDE 3.2 
desktop system. I was a little nervous at first because, as many of the devs 
online can tell you, I really don't know Linux that well. That means that if 
something goes wrong, I usually freak out, go "Oh my God", and panic like 
never before. Then they usually have to help me get through the crisis and it 
always ends up being something fairly simple and stupid that I did myself.</p>

<p>Needless to say, however, I've been learning. So I took the big step myself 
and, happily, I can say that it went off without a single problem. I'm 
running KDE 3.2 on Debian - just so you know, and apt-get is a wonderful 
tool.</p>

<p>Anyway, after being on the beta release of 3.2 for a few months, I wasn't 
expecting much of an upgrade. But, of course, I was wrong.</p>

<p>Beta CVS heads are nice, but they have all sorts of minor quirky problems. At 
least that's what it always seems like to me. You know the type: your memory 
going off the charts at random moments, memory leaks making you restart your 
desktop every few days (unless you know what you're doing and can shut down 
the appropriate processes, but we're talking about me here), little programs 
that don't work *quite* right all the time. You know - buggy things. I don't 
mean Microsoft buggy, but quirky enough to make you realize that you're still 
not on a full release.</p>

<p>Well, all that is gone. So far, I haven't seen a single application do 
anything weird. In fact, it's been as stable as ever.</p>

<p>Okay, if you've never run or seen KDE, there are a few things to know:</p>

<p><b>1) It's stable.</b></p>

<p>This is my work computer. I write columns, essays, articles, etc... And this 
is my computer on which I do all that work. I browse, email, organize, read, 
write, spellcheck and even entertain myself with music all from this 
computer. It doesn't crash or give me weird errors. I can leave it on all day 
and night (and I do), and I know that nothing will be lost when I get back to 
it.</p>

<p><b>2) It's convenient.</b></p>

<p>KDE is nice because it has all these integrated packages that work with it 
seamlessly. For example, when I blog something and enter the text into the 
browser window (Konqueror), I can spellcheck it right there in the writing 
box. In fact, it even spellchecks for me while I type (live spellchecking). 
No more copy/pasting posts in forums and blogs to see if you made a typo. It 
will do it right there. It also has a nice little dictionary I use all the 
time on the Kicker (the applications bar on your desktop), and lots of other 
things which integrate so nicely that you never have to really maneuver 
around one application to the next to get something done. It's like your 
whole desktop is one big application that you use in various ways.</p>

<p><b>3) It's easy to use.</b></p>

<p>As I said, I don't know Linux very well at all. But I don't have to. I've 
written about this last year in another column, but it bears repeating again. 
And since KDE isn't only for Linux, it bears remembering as well. Just like 
many of my friends don't know what the heck a .DAT file is for, I don't have 
to know what all sorts of techy Linux files are for either. The reason is 
that KDE is all I deal with when I'm on the computer, and that makes it easy 
to use. Instead of cryptic command lines, I just have very nice icons 
everywhere, and I can customize it any way I like. If the Kicker bar is too 
long, I can make it shorter. If I want more desktop space, I can have it with 
3 clicks of the mouse. I usually use about 10 virtual desktops on my system 
so nothing is ever crowded at all - I always know just where everything is 
and I don't have to figure out what goes where because I never run out of 
real estate space.</p>

<p><b>4) It's got everything you need.</b></p>

<p>Unless you play lots of games, you just don't need Windows anymore. If you're 
like me and you use your computer for 
writing/browsing/emailing/organizing/listening to music, etc... then you've 
got everything you want in KDE and more. And I don't mean really hard to 
understand applications with no style at all, but friendly and nice looking 
stuff. In fact, some of the applications I use are easier than the ones I use 
on Windows (I have two machines, but I use my Windows one mostly for a few 
games. If I didn't play games, I'd put KDE on that too without blinking). 
100% of the applications I use in KDE are friendlier and easier to use than 
Windows ones. They never crash and I can customize whatever they look like 
without even downloading weird shareware which might, or might not, have a 
virus or do something strange to a .DLL file and mess up my system for good. 
I have no worries like that. In fact, I have so many options that I now think 
of Windows as being very limited and clunky and (gasp) techy to use. That's 
right. KDE is that friendly.</p>

<p><b>5) It's secure.</b></p>

<p>I just mentioned viruses, and I don't get them. Well actually, that's not 
true: people send me viruses from their Outlook mail, and I giggle. I mean 
it! I giggle. I don't even have a virus checker. Why? Because I don't have to 
worry about it with KDE. They won't run on KDE so I don't even have to think 
about it. No Norton subscriptions, no worries about strange .EXE files being 
sent to you. To me, viruses are a thing of the past. I read about how others 
get their hard drives trashed, and I smile. Beat that.</p>

<p><b>6) It's pretty.</b></p>

<p>KDE is pretty. Hands down, it's just beautiful. Actually it's gorgeous too. 
I've got some nice set themes that come installed with the KDE default, and 
they are all so amazingly nice that I have a hard time deciding which to 
choose from. I've used Windows themes on my XP box, and it's such a pain in 
the butt to customize and make it look nice. Not so with KDE. Every part of 
the system looks like you can customize the look with different types of 
widgets. You just go into the control panels and click around to suit your 
taste and apply. That's it. It can even do desktop slide shows with fade-ins 
and other things to make your desktop really come alive and to make it 
unique. The icon sets that I have so far are some of the nicest and prettiest 
things I've ever seen. And when I get bored of one set, I just choose another 
and apply. Even my Kicker bar can have wallpaper (and it does). I'm glad KDE 
is this pretty because it makes it look even more friendly, which is exactly 
what I want.</p>

<p><b>7) It's fun.</b></p>

<p>You want AIM? ICQ? MSN Messenger? No problem. Kopete on KDE gives you access 
to all that and more. It's got a beautiful user interface, awesome icon sets, 
and it's all integrated into one window and in your Kicker dock panel. It 
works flawlessly as far as I can see, and I love it. Chat to your heart's 
content.</p>

<p>I also have a nice little weather updater on my Kicker bar with pretty icons 
telling me the temperature and what the conditions are outside. Of course I 
can look out the window but if I click on this, I get a nice little window
with more info.</p>

<p>I always have music playing with Juk. It's integrated with a small applet in 
my Kicker as well (MediaControl) which lets me control everything from the 
kicker, even when I'm not on the desktop where Juk is. I just love that. And 
if you don't know what Juk is, you can read my review here.</p>

<p>I said that if you play a lot of games, then maybe KDE isn't for you, but I 
wasn't exactly right on that score. KDE does have games included. And while 
they aren't the latest shoot-em-up type of games, they are a heck of a lot 
nicer than the windows games. You have tons of fun ones starting with 
solitaire, backgammon, mahjong, and then even the best tetris-like game ever 
called Frozen Bubble (you have to try this), and some other nice games as 
well. That just makes you realize that Windows, as it is, is just a 
little...boring and stale by now. I love my KDE games and I do actually play 
them sometimes to relax.</p>

<p>So all in all, I have to give KDE 3.2 a big thumbs up. It's about as nice as I 
ever imagined, stable, easy to use, and practical as well. If you're 
completely new to the idea of running something like this, you really 
shouldn't worry about it because you'll be up and running in no time. If you 
know windows, you can run KDE. After a day, you'll never want to go back.</p>

<p>So congrats to the KDE Dev team and many thanks. I hope to see even more 
surprising refinements in the near future.</p>





