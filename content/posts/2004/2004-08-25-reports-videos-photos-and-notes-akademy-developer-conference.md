---
title: "Reports, Videos, Photos and Notes From aKademy Developer Conference"
date:    2004-08-25
authors:
  - "ateam"
slug:    reports-videos-photos-and-notes-akademy-developer-conference
comments:
  - subject: "www.planetkde.org"
    date: 2004-08-25
    body: "dont forget the developer blogs on www.planetkde.org."
    author: "chris"
  - subject: "Playback of Video?"
    date: 2004-08-26
    body: "Hunh.  I have libtheora plus MPlayer - that should play the video, but I get nothing but audio.  Anybody know what apps for SUSE 9.1 should play these (I compile MPlayer myself for DVD playback and ripping, so it is the latest version, almost everything else is SUSE RPMs)."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Playback of Video?"
    date: 2004-08-26
    body: "vlc plays this (videolan), also available for windows."
    author: "chriis"
  - subject: "What's wrong with arts?"
    date: 2004-08-26
    body: "I always notes that KDE 4 will get a new multimedia framework, because people are unhappy with arts. What is wrong with arts?\nFor me it has always worked very well."
    author: "Norbert"
  - subject: "Re: What's wrong with arts?"
    date: 2004-08-26
    body: "> What is wrong with arts?\n\nIt's buggy and unmaintained."
    author: "Anonymous"
  - subject: "Re: What's wrong with arts?"
    date: 2004-08-26
    body: "has high latency, no video support, ...."
    author: "Birdy"
  - subject: "Re: What's wrong with arts?"
    date: 2004-08-27
    body: "And you are a troll"
    author: "Davide Ferrari"
  - subject: "Re: What's wrong with arts?"
    date: 2004-08-27
    body: "Please prove me wrong."
    author: "Anonymous"
  - subject: "Re: What's wrong with arts?"
    date: 2004-08-26
    body: "it was never meant as multimedia framework. it does work, but barely."
    author: "superstoned"
  - subject: "Re: What's wrong with arts?"
    date: 2004-08-26
    body: "I'm trying to make arts 1.3.0 (part of kde 3.3) after \nsuccessfully running the arts configure program. Link \nfails with undefined calls to pthread functions but I \ndon't understand why (after successful config).\nThat's one thing that's wrong."
    author: "Dave Feustel"
  - subject: "Photos"
    date: 2004-08-27
    body: "I hope the photos get some captions soon. :-)"
    author: "Billiga Bosses Begagnade Bilar"
---
The <a href="http://conference2004.kde.org/devconf.php">Conference of KDE Developers and Contributors</a> finished a few days ago, and we are now happy to announce a collection of reports, videos, slides and notes from presentations, as well as plenty of photo galleries from those attending the KDE World Summit - <a href="http://conference2004.kde.org/">aKademy</a>. There are full reports on <a href="http://software.newsforge.com/software/04/08/24/120256.shtml">day one (Saturday)</a> and <a href="http://software.newsforge.com/software/04/08/24/216258.shtml">day two (Sunday)</a> on Newsforge. You can also find transcripts and slides from many of the talks on the <a href="http://conference2004.kde.org/sched-devconf.php">schedule</a> (thanks go to Jonathan Riddell for the transcripts). The video stream archives can be found <a href="http://ktown.kde.org/akademy/">here</a>, courtesy of <a href="http://www.fluendo.com/">Fluendo</a>. Attendees have also started to add their photo albums to <a href="http://wiki.kde.org/tiki-index.php?page=Pictures+%40+aKademy">this wiki page</a>. Those attending aKademy are invited to add notes to <a href="http://wiki.kde.org/tiki-index.php?page=Talks+@+aKademy">this wiki page</a>,  their photos to the other wiki page, and speakers are asked to send their slides to the <a href="mailto:akademy-team@kde.org">press team</a> with the word "press" in the subject of your email. Also a lot of developers seem to <a href="http://www.planetkde.org">blog</a> about aKademy as well.



<!--break-->
