---
title: "Security: Temporary File and Konqueror Frame Injection Vulnerabilities"
date:    2004-08-11
authors:
  - "wbastian"
slug:    security-temporary-file-and-konqueror-frame-injection-vulnerabilities
comments:
  - subject: "please change the status of the bugs to fixed"
    date: 2004-08-11
    body: "These bugs still have the status of new:\na)Browser Frame Injection Vulnerability\n  http://bugs.kde.org/show_bug.cgi?id=84352\nb)security vulnerability: URL-Spoofing as shown on heise.de\n  http://bugs.kde.org/show_bug.cgi?id=83407"
    author: "testerus"
  - subject: "Re: please change the status of the bugs to fixed"
    date: 2004-08-11
    body: "83407 has not been fixed. The impact of it is limited because you\ncan achieve the same effect when you use javascript to set the statusbar text on hover."
    author: "Waldo Bastian"
  - subject: "Securtiy"
    date: 2004-08-11
    body: "Is there a specialized KDe security project that reviews code of critical application? Securtiy reviews are of great importance and developers often don't find their own errors."
    author: "martin runde"
  - subject: "Re: Security"
    date: 2004-08-11
    body: "There is http://www.kde.org/info/security/ to where people are always welcome to report security issues they found."
    author: "Datschge"
  - subject: "Re: Securtiy"
    date: 2004-08-12
    body: "With the number of commits that go into KDE, security reviews are necessarily ongoing. There are a number of things that are in place to attempt to catch the various errors:\n\nCommits are available by email, hence developers watch what is going in and comment on security issues. This happens regularly with khtml.\n\nCalls to library routines that are potential problems are flagged on the email list as potentially unsafe, making it easy for others to check.\n\nSome developers have private review arrangements in place.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Securtiy"
    date: 2004-08-12
    body: "> Some developers have private review arrangements in place.\n\nWhat does that mean? "
    author: "cm"
  - subject: "Re: Securtiy"
    date: 2004-08-13
    body: "Private as opposed to public, on mailing lists etc.\n\nWatching the commit logs over a long period, it is obvious that many developers have people they work with, who test or review what they are doing. They are unnamed, and the conversations are private, ie. email as opposed to mailing list.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Securtiy"
    date: 2004-08-13
    body: "Ah, thanks for the clarification."
    author: "cm"
  - subject: "Re: Securtiy"
    date: 2004-08-12
    body: "Although there is a continuous review of changes, there is currently no structured review process for applications as a whole. Especially when new applications are added to KDE CVS that proves to be a problem, because such new applications often contain unsafe constructions.\n\nWith the release of KDE 3.1 we have done a code review of all of CVS looking for uses of certain constructions that have turned out to be problematic. That might offer a good basis to start from when starting an audit process.\n\nIdeally we would have a group of people that actively maintains a list of problematic constructions and that performs audits on an application by application basis and documents the results."
    author: "Waldo Bastian"
  - subject: "Re: Securtiy"
    date: 2004-08-12
    body: "> Ideally we would have a group of people that actively maintains a list of problematic constructions and that performs audits on an application by application basis and documents the results.\n\nI hear that the SUSE Security Team does that occasionally, eg they looked at the instant messengers Kopete and Gaim - well, you know the not so favorable result for Gaim (http://www.heise.de/newsticker/meldung/49837)."
    author: "Anonymous"
---
Three security advisories have been issued today for KDE.
<a href="http://www.kde.org/info/security/advisory-20040811-1.txt">
The first advisory</a> concerns the unsafe handling of KDE's temporary directory in certain circumstances.
<a href="http://www.kde.org/info/security/advisory-20040811-2.txt">
The second advisory</a> relates to the unsafe creation of temporary files by KDE 3.2.x's
<a href="http://www.kde.org/areas/sysadmin/startup.php#dcopserver">
dcopserver</a>.
<a href="http://www.kde.org/info/security/advisory-20040811-3.txt">
The third advisory</a> is about a frame injection vulnerability in Konqueror as earlier reported by
<a href="http://www.heise.de/newsticker/meldung/48793">Heise Online<a>
and
<a href="http://secunia.com/advisories/11978/">Secunia</a>.

<!--break-->
<p>Distributions are expected to have updated binary packages available shortly. All issues mentioned above have also been fixed in the
<a href="http://download.kde.org/unstable/3.3.0rc2/src/">KDE 3.3 Release Candidate 2</a>
that was
<a href="http://dot.kde.org/1092139173/">
announced yesterday</a>. The final release of KDE 3.3 is expected later this month.</p>
