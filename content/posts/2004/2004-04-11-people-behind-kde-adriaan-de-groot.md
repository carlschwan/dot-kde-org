---
title: "The People Behind KDE: Adriaan de Groot"
date:    2004-04-11
authors:
  - "Tink"
slug:    people-behind-kde-adriaan-de-groot
comments:
  - subject: "Thank you!"
    date: 2004-04-12
    body: "That was an interesting read."
    author: "Alex"
  - subject: "What's up with www.kde.nl? "
    date: 2004-04-12
    body: "It seems down. Is there a mirror?"
    author: "Hans"
  - subject: "Re: What's up with www.kde.nl? "
    date: 2004-04-13
    body: "www.kde.nl is up again \n\nFab"
    author: "Fabrice Mous"
  - subject: "Recumbents"
    date: 2004-04-13
    body: "What's this with Dutch KDE developers riding recumbents? I have one too, and I do KDE and QTopia development (though nothing big, I'll admit). :-)"
    author: "Andr\u00e9 Somers"
  - subject: "Re: Recumbents"
    date: 2004-04-14
    body: "It is not just developers.... KDE users ride recumbents too!\n\nRene - admin of ligfiets.net -"
    author: "Rene"
---
<a href="http://www.kde.nl/people/">Another Dutch treat</a>! This guy has given himself the Dutch label; a 'hardcore béta', he's focussed on writing his thesis, can't stand bad and rude behavior on IRC, amuses himself with Algebra and his most successful and recent accomplishment is contributing to the compilation of a new project, a baby girl named Mira! This week we sync up with KPilot's <a href="http://www.kde.nl/people/ade.html">Adriaan de Groot</a>!



<!--break-->
