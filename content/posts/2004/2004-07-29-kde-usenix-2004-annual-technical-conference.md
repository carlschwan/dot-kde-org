---
title: "KDE at USENIX 2004 Annual Technical Conference"
date:    2004-07-29
authors:
  - "cschumacher"
slug:    kde-usenix-2004-annual-technical-conference
comments:
  - subject: "KDE 3.3.3??"
    date: 2004-07-29
    body: "You're *waaayyyy* ahead of me."
    author: "Inge Wallin"
  - subject: "Re: KDE 3.3.3??"
    date: 2004-07-29
    body: "Well, you are right. KDE does a lot of things, but traveling in time unfortunately isn't included in the feature set yet. Has to be \"KDE 3.3\" of course."
    author: "Cornelius Schumacher"
  - subject: "Re: KDE 3.3.3??"
    date: 2004-07-29
    body: "aaah, but athene (http://www.rocklyte.com/athene/index.html) DOES feature KDE 3.3.3 ;-)"
    author: "superstoned"
  - subject: "Re: KDE 3.3.3??"
    date: 2004-07-30
    body: "I think it is a typo. I should be KDE 3.2.3"
    author: "Vasilis"
  - subject: "Re: KDE 3.3.3??"
    date: 2004-07-31
    body: "duh"
    author: "superstoned"
  - subject: "Re: KDE 3.3.3??"
    date: 2004-07-31
    body: "And why aren't you?  You can be anything you want, if you put your heart to it!"
    author: "Inge Wallin"
  - subject: "Re: KDE 3.3.3??"
    date: 2004-07-31
    body: "\"Well, you are right. KDE does a lot of things, but traveling in time unfortunately isn't included in the feature set yet.\"\n\nFrom what I can understand about the previous article 'Pushing KDE's Science: Evolution Sim', people seem to already be hard at work on closing that gap, by simply simulating the entire Universe :)"
    author: "Richard Dale"
  - subject: "Re: KDE 3.3.3??"
    date: 2004-08-01
    body: "we will do our best *g*"
    author: "Raphael Langerhorst"
  - subject: "OT: KDE CVS digest"
    date: 2004-07-31
    body: "Sorry for being offtopic, but have I missed anything concerning the cvs-digest?\n\nTwo-weeks without digest is a hard time for me ;-) "
    author: "MK"
  - subject: "Re: OT: KDE CVS digest"
    date: 2004-07-31
    body: "See http://cvs-digest.org for current status."
    author: "Anonymous"
  - subject: "Re: OT: KDE CVS digest"
    date: 2004-08-01
    body: "This afternoon it should be ready (sunday).\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: OT: KDE CVS digest"
    date: 2004-08-01
    body: "Will it cover two weeks?"
    author: "Anonymous"
  - subject: "Re: OT: KDE CVS digest"
    date: 2004-08-01
    body: "Yes\n\nDerek"
    author: "Derek Kite"
---
From June 27th to July 2nd 2004 the <a
href="http://www.usenix.org/events/usenix04/">USENIX Annual Technical
Conference</a> took place at Boston. The <a
href="http://www.usenix.org/events/usenix04/tech/freenix.html">FREENIX track</a>
featured a refereed paper about Kontact and how it is used as an application
integration framework which was presented by
Cornelius Schumacher. You can find the
<a href="http://www.kontact.org/files/kontact_freenix_paper.pdf">paper</a> and
the <a href="http://www.kontact.org/files/kontact_freenix_slides.pdf">slides</a>
of the presentation at the <a href="http://www.kontact.org/">Kontact home page</a>.



<!--break-->
<p>The conference also featured a lot of other <a
href="http://www.usenix.org/events/usenix04/tech/onefile.html">interesting
talks</a>. To name only a few: Matthias Ettrich presented the technical changes in Qt
4, in the plenary session Bruce Schneier held a thought-inspiring talk about how
security-tradeoffs influence our lives and how we might be able to handle this
in a sensible way and Rob Pike gave some insight in the amazing technology
Google uses to power their search engine. One of the talks which was awarded as
best paper in the FREENIX track was about <a
href="http://www.usenix.org/events/usenix04/tech/freenix/cornell.html">"Wayback,
a versioning file system for Linux"</a>. This could
be an interesting technology to be integrated with KDE, for example with the
file dialog.</p>

<p>A BoF session about "Locking Down Enterprise KDE & KDE 3.3 Preview for
Administrators" showed again the great interest in the KDE desktop lock-down
features which are provided by the KIOSK framework and in particular the
<a href="http://extragear.kde.org/apps/kiosktool.php">KIOSK Admin Tool</a>.</p>

<p>The week after the conference code was put behind the talks. The KDE
project released the first beta of Kontact 1.0 as part of <a
href="http://www.kde.org/announcements/announce-3.3beta1.php">KDE 3.3 Beta 1</a>, Trolltech released the <a
href="http://dot.kde.org/1089303565/">first
technology preview of Qt 4</a> and Waldo Bastian <a
href="http://www.kde-apps.org/content/show.php?content=12028">released version
0.6</a> of the KIOSK Admin Tool.</p>



