---
title: "Quickies: KDEvibes, Zeroconf, New User Guide, Konqi's Background"
date:    2004-05-25
authors:
  - "fmous"
slug:    quickies-kdevibes-zeroconf-new-user-guide-konqis-background
comments:
  - subject: "\\n or ***"
    date: 2004-05-25
    body: "Don't know if it is technically possible but I would really appreciate if future Quickies would use new lines for separating different topics. Another suggestion would be to separate them using some asterisks (*), as in:\n\n\"Recently the project was registered at SourceForge. *** Although you are not a perfect English speaker and you have no knowlegde of DocBook you do have the skill to explain something fairly clearly in a non-technical manner?\"\n\nJust my 2 cents.\nSorry for my bad english."
    author: "MM"
  - subject: "Re: \\n or ***"
    date: 2004-05-25
    body: "LOL, Konqis Background .... :-)"
    author: "MM"
  - subject: "Re: \\n or ***"
    date: 2004-05-25
    body: "Good idea ... \n\nWill take that into consideration for next time\n\nFab\n"
    author: "Fabrice Mous"
  - subject: "Thanks, Fabrice! ;-)"
    date: 2004-05-25
    body: "I'm currently very busy with writing my new track, but I have already created lots of new effects for KDEvibes. I hope to start writing new jingles in next few days."
    author: "Artemio"
  - subject: "Re: Thanks, Fabrice! ;-)"
    date: 2004-05-25
    body: "Great sounds btw.\nThanks!"
    author: "dwt"
  - subject: "Original User Guide announcement"
    date: 2004-05-25
    body: "The original request for help for the User Guide that I sent to the kde-quality list is archived at: https://mail.kde.org/pipermail/kde-quality/2004-May/000608.html\n\nIt has some useful information about how to help with the User Guide.\n\n(Fab, perhaps it would be useful to link to this message in the story?)\n\nRegards,\nPhil"
    author: "Philip Rodrigues"
  - subject: "Re: Original User Guide announcement"
    date: 2004-05-25
    body: "Philip,\n\nChanged the mailto-link into a http-link \n\nIs that okay?\n\nFab"
    author: "Fabrice Mous"
  - subject: "Re: Original User Guide announcement"
    date: 2004-05-25
    body: "Cool, thanks Fab\n\nPhil"
    author: "Philip Rodrigues"
  - subject: "klik"
    date: 2004-05-25
    body: "And klik?\nhttp://www.kde-apps.org/content/show.php?content=12841"
    author: "gerd"
  - subject: "Re: klik"
    date: 2004-05-25
    body: "What is about klik?"
    author: "Anonymous"
  - subject: "Zeroconf..."
    date: 2004-05-26
    body: "... seems to be pretty coool and useful. Hope we get to see some working code soon!\n"
    author: "_perraw"
  - subject: "Re: Zeroconf..."
    date: 2004-05-26
    body: "I think the project is looking for contributors as well .. \n\nAnyway cool and ambitious project \n\nFab"
    author: "Fab"
  - subject: "Thanks"
    date: 2004-05-27
    body: "Thanks, Fab\n\nYou make my day more fun by distracting me from work.  I always enjoy the \"quickies\"\n\nGood job!\n\n//standsolid///"
    author: "standsolid"
---
On <A href="http://artemio.net/?p=event&e=2004-05-07">Artemio's homepage</A> 
I read that the new 
<A href="http://www.suse.de/en/private/products/suse_linux/index.html">SUSE version 9.1</A> uses 
<A href="http://www.kde-look.org/content/show.php?content=11269">KDEvibes</A>, 
a collection of system sounds for the KDE desktop. More system sounds can be found at <A href="http://www.kde-look.org/index.php?xcontentmode=25">KDE-Look.org</A>.

While looking around at the excellent <A href="http://wiki.kde.org/">KDE Wiki</A> I found 
<A href="http://wiki.kdenews.org/tiki-index.php?page=Zeroconf+in+KDE">this page</A>
about integrating <A href="http://www.zeroconf.org/">Zeroconf</A> into KDE Apps. 
Recently the <A href="http://sourceforge.net/projects/zeroconfcompnts">project</A> 
was registered at SourceForge.


Although you are not a perfect English speaker and you have no knowlegde of DocBook you do have the skill to explain something fairly clearly in a non-technical manner? 
You could help writing the new userguide which recently
<A href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdenonbeta/doc/userguide-tng/">appeared in KDE CVS</A>.
This is the start of a complete rewrite of the 
<A href="http://www.kde.org/documentation/userguide/">old KDE User Guide</A>. You can
<A href="http://users.ox.ac.uk/~chri1802/kde/userguide-tng/">catch a glimpse</A> 
of the new userguide. So don't wait but 
<a href="https://mail.kde.org/pipermail/kde-quality/2004-May/000608.html">apply for that job</a>.


To finish Quickies I dug up some info about <A href="http://www.kde.org/areas/people/konqi.html">Konqi</A>
and <A href="http://dot.kde.org/1060594515/1060670079/1060676522/1060686286/1060700210/1060890441/">his background</A>.




<!--break-->
