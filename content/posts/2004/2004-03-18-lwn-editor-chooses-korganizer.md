---
title: "LWN Editor Chooses KOrganizer"
date:    2004-03-18
authors:
  - "binner"
slug:    lwn-editor-chooses-korganizer
comments:
  - subject: "[OT] Vote for LinuxUser Awards"
    date: 2004-03-18
    body: "If you think KDE, or any of it's apps deserves to win \"Best Linux or Open Source Software\", vote here:\n\nhttp://www.linuxuser.co.uk/expo/index.php?option=com_philaform&Itemid=1&form_id=6\n\nThanks!"
    author: "BlueSkyHaze"
  - subject: "Korganizer alarm"
    date: 2004-03-18
    body: "He mentions that korganizer's alarm window floats on his non-KDE desktop... he needs this:\n\nhttp://www.kde-apps.org/content/show.php?content=10660"
    author: "anon"
  - subject: "What amazes me"
    date: 2004-03-18
    body: "Is how little know about KDE PIM.  If he knew anything about KDE PIM he would have gone to KOrganizer first.  It also shocked me how few people know about konsolekalendar.  There where a few talk backs asking for a command line interface to the iCal files.  Maby we need to advertise more, but its funny that a buggy app that crashes even when just trying to test it out is seen as enterprise while KOrganizer is ignored.  \n\nFYI KOrganizer has:\n*Exchange access.\n*Command line Interface.\n*Plugins to deal with virtual calendars like birthdays.\n*Pretty robust Palm hotsync ability.\n*Ability to send meeting requests to Outlook users.\n*Share free/busy information with outlook users.\n*Not crash when trying to test it out.\n\nLets spread the word people.\n\nCheers\n-ian reinhart geiser\n"
    author: "Ian Reinhart Geiser"
  - subject: "multisync.."
    date: 2004-03-18
    body: "The only thing that made me ever use evolution (for a few minutes) was the lack of any decent syncing in kdepim. multisync is a LOT better than ksync, kitchensync and kandy at the moment. In fact the only lossy way to get my addressbook on my Sony-Ericcson T610 was to export from kaddressbook/korganizer, import (lossily!) into evolution and then sync using multiync.\n\nI hopt that KDE comes up to speed in the syncing department soon."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: multisync.."
    date: 2004-03-18
    body: "If you really want that, check out this page:\nhttp://handhelds.org/~zecke/donations.html\nHolger ('Zecke') is the (main?) developer of Kitchensync, the framework that is to become _the_ syncking framework for KDE."
    author: "Andr\u00e9 Somers"
  - subject: "Re: multisync.."
    date: 2004-03-19
    body: "I'm broke currently, but I would really like to be able to sync my SE P800 smartphone, so I donated a bit :)\n\nI hope others donate too :)"
    author: "Joergen Ramskov"
  - subject: "Re: multisync.."
    date: 2004-03-21
    body: "After buying a P800 it's no wonder you are broke."
    author: "krackhead"
  - subject: "Re: multisync.."
    date: 2004-03-21
    body: "I bought a used one, so it wasn't that bad ;)"
    author: "Joergen Ramskov"
  - subject: "Re: multisync.."
    date: 2004-03-18
    body: "take a look at the kde-pim mailing list, you'll be happy!"
    author: "me"
  - subject: "Re: multisync.."
    date: 2004-03-19
    body: "I looked, but I did not find any posts concerning syncing a Sony Ericsson T610.\nCould you please be more specific?"
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: multisync.."
    date: 2004-05-06
    body: "You can use/modify the Evolution calendar file directly from KDE by setting it as the default calendar file in KDE.  It's an single iCalendar file located at ~/evolution/local/Calendar/calendar.ics.  You can use the KDE resource configuration program located under Preferences -> KDE Components -> KDE Resources Configuration located off of the K menu.\n\nAnother trick is to use a backup directory from the Mutisync Backup plugin directly by setting up a contact resource that uses vcards located in a directory.  Which is what the Multisync backup directory is.  You could even setup the KDE calendar this way except for the fact that the Multisync Backup plugin doesn't encapsulate it's vCalendar vEvent files in BEGIN:VCALENDAR ... END:VCLALENDAR statements making the vEvents unrecognizable by KDE.\n\nI have it setup so the Pocket PC I have syncs against a Backup Plugin and the rest of the plugins and programs either use that directory live or sync against that directory."
    author: "Danyel Lawson"
  - subject: "Re: What amazes me"
    date: 2004-03-18
    body: "Damn right. I was genuinely shocked at just how good the KDE-PIM/Kontact stuff in general was when I tried it out.\n\nI was back home the other day and my Dad wanted to try out a Linux setup, as he was desperate to get back to all the UNIX knowledge he had forgotten :). He'd used Outlook for years (not his choice), so I got KDE 3.2 up and running for him. I also got Freecraft up and running for him as Warcraft 2 doesn't run on XP and he doesn't like Warcraft 3, but that's another story.\n\nApart from giving him what he wanted, I thought it would be good to see what an experienced IT person, well aquainted with 'mainstream' office tools in a wide variety of environments would think of a Linux-desktop setup in general. He easily got to grips with Kontact, and was especially impressed with the contacts and organizer parts. He's got all his contacts in there now, and was really impressed with added touches such as the ability to add pictures, and the geo-data bit. Kontact is also a really usable and well laid out set of components that is way better than anything I have used before, including a recent Evolution, so the whingers haven't even got usability problems to throw at Kontact. I wonder what certain people will say is wrong with it all this time :).\n\nThe only problem I have has is when I added the KWeather and RSS newsfeed stuff to the Summary page. When I first run Kontact the application appears to freeze, but it hasn't actually crashed. If I close it down and restart it it's fine. I'm just wondering if this is a network thing, perhaps related to my hostname settings, so I'm wondering if anyone has seen anything like this.\n\nAnyway, for a first release of a full PIM suite I'm completely blown away. The weird thing is the thought that Kontact and the KDE-PIM tools will get even better."
    author: "David"
  - subject: "Re: What amazes me"
    date: 2004-03-19
    body: "Then help me understand how to set all of this up - without having to dig through the source, mailing lists, forums, etc...\n\nI tried KDE PIM, and korganizer was what I gave up on first.  I never did figure out how to use a calendar stored on a remote machine (fish?).  That trouble alone kept me from figuring out how to create the 'default' calendar file, and after importing holidays for US and CH, nothing happened.\n\nI would be more than happy to write an \"Idiot's Guide to Using KDE PIM\", if I could just get some help setting it up.\n\nNot a flame, I will happily write the docs if someone can help me get everything running like it is promised to.\n\n-- Mitch"
    author: "mitchy"
  - subject: "still using ical here"
    date: 2004-03-18
    body: "Guilty, guilty of being an old-timer.  I probably won't switch until I find/write the K-App that is *identical* to it, right down to the file format (none of that importing business...). :-)"
    author: "Navindra Umanee"
  - subject: "Well,"
    date: 2004-03-18
    body: "I feel deeply sorry for people who keep a calendar so they can write \"8.00 - Trash out\" into it. (the first screenshot in the linked article)\n\n"
    author: "no one in particular"
  - subject: "Re: Well,"
    date: 2004-03-19
    body: "Damn!  I knew I forgot something!  ;)"
    author: "LMCBoy"
  - subject: "Re: Well,"
    date: 2004-03-20
    body: ">>>I feel deeply sorry for people who keep a calendar so they can write \"8.00 - Trash out\" into it. (the first screenshot in the linked article)\n\nI guess it's part of their social networking, taking the trash out."
    author: "ac"
  - subject: "Finally KOrganizer is usable"
    date: 2004-03-19
    body: "Well it took them to a couple of years and get to version 3.2, but I must say that I'm very impressed with the current state of KOrganizer.  The only serious beef that I have left is that you can't enter an event without the dialog appearing, like the editor noticed.  Also, I agree that korganizer wastes to much disk space, but after I got rid of most of the toolbar icons, I'm quite pleased."
    author: "he-sk"
  - subject: "Re: Finally KOrganizer is usable"
    date: 2004-03-19
    body: "Disk space? KOrganizer's icons are about 300KB big. I guess you mean screen space."
    author: "Anonymous"
  - subject: "Re: Finally KOrganizer is usable"
    date: 2004-03-19
    body: "yep, that's what i meant"
    author: "he-sk"
  - subject: "Re: Finally KOrganizer is usable"
    date: 2004-03-20
    body: "Disk space, screen space, what's the difference? I use display pixels to store data (taking screenshots of each screen) and use disk platters for displays. No big deal."
    author: "ac"
  - subject: "He couldn't use Evolution?"
    date: 2004-03-20
    body: "I find it higly funny that he couldn't reproduce the shot on the ximian website. I found the shot and reproduced it in no time. Just change to the Week View in the calendar, and drag a vertical bar to hide the summary view.\n\nIf yo want an even without a time of day in Evolution, you just tick the \"All day event\" radio box. Didn't really go into the strengths of any of the apps mentioned.\n\nWell. I suppose the only thing I can give to him is that Evolution comes as a part of a huge package, and that is rectified in the next update, sort of.\n"
    author: "Maynard Kuona"
  - subject: "Re: He couldn't use Evolution?"
    date: 2004-03-20
    body: "Doesnt an \"all day\" event mark all day as busy? It is not the same thing as an event without a time.\n\nOr rather, if it is, it is misnamed."
    author: "Roberto"
  - subject: "Re: He couldn't use Evolution?"
    date: 2004-03-20
    body: "You can mark an all day event as either free or busy still, so no, not the same thing."
    author: "Maynard"
  - subject: "Editing the memos"
    date: 2004-03-23
    body: "I like the KDE-PIM project alot, it's very promising.\nBut I can't use it as my only pilot suite as I can't seem to edit the memos with the same \"ease\" as with j-pilot.\n\nI've checked that the fields should be editable, but the editor wont let me save what I've written.\n\nThe only way, as I see it, is to export the memo to text, edit it and import it. And I promise you I've read the fine manual, many times. I've also googled for solutions, but come up with nothing to solve my \"problem\".\n\nHave I overlooked something? I've even looked for memo-editability in KOrganizer...\n\nSome of my knotes end up in my palm as memos (choosen i preferences) but my old memos isn't imported into knotes, so I can't edit them that way either.\n\nAnd as I use memos and the Date book the most, I'm just a half person with the current kpilot solution."
    author: "Petter"
---
When <a href="http://lwn.net/">LWN</a> editor Jonathan Corbet was forced to <a href="http://lwn.net/Articles/74814/">search for an alternative</a> for his beloved "ical" calendar application he examined Evolution, KOrganizer, plan, gDeskCal and GNOME-PIM and found a clear winner: <a href="http://korganizer.kde.org/">KOrganizer 3.2</a>. He loves it as the only competitive replacement for ical and praises the top-quality job of its developers to create a highly configurable calendar manager with a minimum of unneeded baggage.


<!--break-->
