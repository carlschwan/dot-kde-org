---
title: "KDE-CVS-Digest for February 6, 2004"
date:    2004-02-07
authors:
  - "dkite"
slug:    kde-cvs-digest-february-6-2004
comments:
  - subject: "Thank you!"
    date: 2004-02-07
    body: "Thanks YOU for providing every week a lot of information about what's going in KDE CVS. You're doing this for more than a year now and it was and still is always nice to read. In particular the \"One year ago\" section is a good thing and makes me smile sometimes. \n\nThe importance of the KDE-CVS-Digest can not be overestimated, thanks a lot for \"feeding\" the hungry crowd every week :-)\n"
    author: "MK"
  - subject: "UNIX Focus Policy"
    date: 2004-02-07
    body: "Why are the two UNIX window focus policies:\n\nFocus Under Mouse\nFocus Strictly Under Mouse\n\nconsidered to be: \"unreasonable focus policies\"???\n\nIf the developers don't want to support these then they should be dropped.  But, I don't see the point in denigrating them.\n\nI think that I could probably learn to live with:\n\nFocus Follows Mouse\n\nalthough I don't understand the exact differences.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: UNIX Focus Policy"
    date: 2004-02-07
    body: "The difference between the policies is:\n\nFocus Strictly Under Mouse: The focus is on the window under the mouse. If no window is under the mouse, now indow has the focus.\n\nFocus Under Mouse: The focus is on the window under the mouse. If the mouse moves outside of any window, the window that last had the focus keeps it until a new window gets the focus (by virtue of the mouse moving over it).\n\nFocus Under Mouse is okay for me, the strict version I consider unusable. Chaq'un a son fa\u00e7on, however.\n"
    author: "Anno"
  - subject: "Re: UNIX Focus Policy"
    date: 2004-02-07
    body: "The difference between focus follows mouse and focus (strictly) under mouse is that the sooner one honours explicit requests to activate a window, while the latter ones stubbornly keep the window under mouse active not matter what (I think it's actually explained in the settings dialog). Therefore even things like clicking on taskbar entries or Alt+F2 don't activate the windows, and that's probably why it's called unreasonable (although other reasons for this name are not impossible, creating some features or fixing bugs with these policies is a pain, my commit is an example of that).\n"
    author: "Lubos Lunak"
  - subject: "Re: UNIX Focus Policy"
    date: 2004-02-07
    body: "I am quite familiar with the difference between: \"Focus Under Mouse\" and: \"Focus Strictly Under Mouse\", the very slight difference.\n\nHowever, I am not certain that I understand the difference between: \"Focus Under Mouse\" and: \"Focus Follows Mouse\".\n\nThe documentation appears to be wrong in:\n\nhelp:/kcontrol/windowmanagement/index.html\n\nand the \"Whats This\" [?] offers no explanation except to make what I believe are inappropriate statements denigrating the two UNIX policies.\n\nThis, IMO, has been further confounded by the fact that up until this release KWin had bugs.  Now, it appears to work correctly (I can now use Focus Follows Mouse without problems but I do prefer the strict focus).  I think that we could drop the two UNIX policies.  But, I would like to see:\n\n\"Focus Strictly Follows Mouse\"\n\nadded if we did that.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: UNIX Focus Policy"
    date: 2004-02-07
    body: "> However, I am not certain that I understand the difference between: \"Focus Under Mouse\" and: \"Focus Follows Mouse\".\n \nRead more carefully, I just explained it to you. You're right that the documentation doesn't explain it correctly; the what's-this is correct though, it just doesn't stress the difference as explicitly as I did (actively moving mouse onto window activates - vs - window under mouse is active).\n\nTrust me, I'd love to drop the unreasonable focus policies. But my self-preservation instict disagrees ;). And guessing from your description, I think you'd actually be one of those demanding my head.\n"
    author: "Lubos Lunak"
  - subject: "Re: UNIX Focus Policy"
    date: 2004-02-07
    body: "Sorry, very dense sometimes.\n\nI can't see any difference between: \"Focus Under Mouse\" and: \"Focus Follows Mouse\" when I move the mouse pointer from one open window to another.  The only difference that I can observe is that new windows always get focus (and are raised) in: \"Focus Follows Mouse\" and they need to have the mouse pointer moved into them first with: \"Focus Under Mouse\".  Or, is this a bug in 3.2 since 3.1.5 didn't work that way?\n\n> I think you'd actually be one of those demanding my head.\n\nI used to be in that camp, but that was because of bugs with \"Focus Follows Mouse\" in previous KWin versions.  Windows would come to the top when I moved the mouse into them (it didn't matter that: \"Auto Raise\" wasn't checked -- it still did it) or windows would disaper under other ones without being asked.  I don't like that.  \n\nBut now I am OK with \"Focus Follows Mouse\" in 3.2 except that I would prefer to also have the 'strict' version of it available -- if the mouse wasn't in a window, no window would have focus.  If that were added, this \"hard core\" UNIX person would signoff on killing the old UNIX focus policies.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: UNIX Focus Policy"
    date: 2004-02-08
    body: "I'm a click-to-focus person, so I might be wrong, but: AFAIK the fact that focus under mouse always keeps the window under mouse active while focus follows mouse allows explicit activation of other windows is the only difference. I don't think it worked differently in 3.1.5, and I don't remember doing anything with it. If you think there's something wrong with the current KWin, ->bugs.kde.org please.\n"
    author: "Lubos Lunak"
  - subject: "KStars pics..."
    date: 2004-02-07
    body: "So which one of those 2 photos is the KStars image, and which one is the real one??"
    author: "OI"
  - subject: "Re: KStars pics..."
    date: 2004-02-07
    body: "Right click and look at the filenames :)"
    author: "Vic"
  - subject: "Re: KStars pics..."
    date: 2004-02-07
    body: "just look at the noise. The one that looks too perfect is made with KStars :-)"
    author: "MK"
  - subject: "OT: autohide"
    date: 2004-02-07
    body: "just a question (i did not follow kde in the last years): is it possible, to autohide apps, not only panels? i am quite happy with this fvwm-feature. for example, if i point my cursor on the top of my desktop, a large terminal comes down. very cool for me. and the focussing really kicks ass in fvwm, was there any progress in the last releases?  "
    author: "me"
  - subject: "Re: OT: autohide"
    date: 2004-02-07
    body: "No, the default window manager doesn't have this feature. But you can try running KDE with FVWM (http://ktown.kde.org/~seli/kdewm/ - FVWM should have the necessary support for the WM spec as well, and I'll be happy to add it to the page if you try it out).\n"
    author: "Lubos Lunak"
  - subject: "Re: OT: autohide"
    date: 2004-02-07
    body: "thanks, everything seems to work fine, except for the autohide feature :(("
    author: "me"
  - subject: "Re: OT: autohide"
    date: 2004-02-08
    body: "Mind mailing me some details (which version, how much you tested it, problems) about how FVWM works in KDE, so I could add it to the page? WMs have to be used for real in order to test them, and I have already enough fun with the default one.\n"
    author: "Lubos Lunak"
  - subject: "My favorite!"
    date: 2004-02-07
    body: "Matt Rogers committed a change to kdenetwork/kopete/protocols/oscar\n\nFix bug 74197. Use 16 for a maximum password length\n\nRefer to Bug 74197 - password dialog only accepts 8 chars\nDiffs: 1, 2, 3, 4\n\n\nThis is about time! This is what prevented me from using Kopete previously."
    author: "Turd Ferguson"
  - subject: "Re: My favorite!"
    date: 2004-02-08
    body: "hehe, scary that something that I committed is somebody's favorite commit. ;)\n\nGlad you like it though. :)"
    author: "Matt Rogers"
  - subject: "incredible"
    date: 2004-02-07
    body: "I was thinking yesterday about that while using gwenview.\nThe application was so fast ans so close to konqueror in look & feel that I was wondering why it wasnt included in konqui.\nAnd pop!\nkde devs are so fast AND expert telepaths!!"
    author: "djay"
  - subject: "CVS Digest Script"
    date: 2004-02-08
    body: "Derek,\nThe CVS Digest is so well organized - is there a particular cvs log script that you use to generate it?  I would like to implement a similar report for our CVS projects. thks."
    author: "john"
  - subject: "Re: CVS Digest Script"
    date: 2004-02-08
    body: "http://webcvs.kde.org/cgi-bin/cvsweb.cgi/www/areas/cvs-digest/tools/\n\nIts a rather miserable perl script which is incomprehensible even to me who wrote the damn thing.\n\nRight now it depends on the mailings that the kde cvs server does when there is a commit, so I don't think it would translate to another project.\n\nStay tuned, I'm working on something better. Famous last words.\n\nSend me an email if you would like to know what's in the pipe. I am actually getting something done on it right now.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: CVS Digest Script"
    date: 2004-02-08
    body: "Why not make an interview with yourself? I'd read it!"
    author: "Niels"
  - subject: "question"
    date: 2004-02-08
    body: "Anyone know if/when (now that SuSE is in control of the Novell desktop) Evolution will be ported to KDE? I presume it will be, since Novell is going with KDE it only makes sense that they will want to make Evolution a native KDE app.\n\nI'm hoping it's soon... I hate having to have gnome installed just for a few apps (the biggest being Evolution)."
    author: "some chap"
  - subject: "Re: question"
    date: 2004-02-08
    body: "Why do you prefer Evolution to Kontact?"
    author: "Anonymous"
  - subject: "Re: question"
    date: 2004-02-08
    body: "Since there is Kontact now, I would be very surprised if Evolution was ported to KDE. Nevertheless, I wouldn't mind :-)\nOn the other hand, I have no problems with Gnome-apps being installed on my systems ...\n\nThilo"
    author: "Thilo"
  - subject: "Re: question"
    date: 2004-02-08
    body: "\nI personally only want the ximian connector to be released under GPL,\nand then be integrated into KMail/Kontact.\n\nThat would definately rock."
    author: "ac"
  - subject: "Re: question"
    date: 2004-02-08
    body: "There is already work done for Exchange support in Kontact."
    author: "Anonymous"
  - subject: "Re: question"
    date: 2004-02-09
    body: "How could i try it?"
    author: "jmk"
  - subject: "Re: question"
    date: 2004-02-08
    body: "Porting Evolution to KDE is probably not going to be done. Its unnecessary and a waste of resources. Whoever gave you the idea that they are going to just drop GNOME completely anyway, because that is what yu seem to suggest. Novell is not only going with KDE, at least there is nothing to suggest that. Besides, I believe a lot of Evolution development is taking place in GNOME CVS right now, so it would be foolhardy to think they will make a CVS port.\n\nKDE has Kontact anyway. They do not need Evolution."
    author: "Maynard"
  - subject: "Re: question"
    date: 2004-02-08
    body: "I agree with you here, it's very unlikely Evolution would be ported. However, it would be nice to see if Evolution gets optional KDE support using the QtGTK stuff, getting KDE print and file dialogs perhaps."
    author: "Arend jr."
  - subject: "Re: question"
    date: 2004-02-08
    body: "> Evolution with KDE/QT widgets?\nQuoi? I feel _much_ more comfortable with my kmail / korganizer combo (now in kontact). Have tried Evolution a year ago. It was not really bad, but far from being a nice PIM application.\nImho, I think the modular structure of kontact is more foresighted than the all-in-one approach of evolution."
    author: "Thomas"
  - subject: "KDE 3.2 question"
    date: 2004-02-08
    body: "I just installed the KDE3.2 rpm:s on Suse 9.0\nReally great work by the KDE-team, apps, features, everything.\nQuite impressive that I can download all this for free. :-)\n\nAnyone knows how to get rid of the grey ugly background\nbehind the kde splash screen? Is it possible to have\nthe login background left?"
    author: "OI"
  - subject: "Actually, we got constellation *boundaries*"
    date: 2004-02-09
    body: "Hey,\n\nJust a clarification; we've had constellation *lines* for a very long time.  We now have added official IAU constellation *boundaries*.  As the CVS digest shows, we did also improve the way the constellation lines are implemented, however.\n\nAnyway, don't mean to complain, we're always happy to get a mention in the digest, Derek!  ;)\n"
    author: "LMCBoy"
  - subject: "Trying not to be a license troll..."
    date: 2004-02-09
    body: "Now that it is possible to create KDE applications using GTK (through various bridging technologies), is possible to create applications for KDE that aren't GPL (without paying Trolltech)?\n\nMy guess is no, since I imagine the code that bridges the event loops and the theme engine eventually links back to QT somehow.  Am I right here?\n\nNote: Sorry that this is offtopic - I really don't even care about the issue that much myself.  But I can't sleep and for some reason the question is bugging me, so I thought I'd get a second opinion here."
    author: "anonymous"
  - subject: "Re: Trying not to be a license troll..."
    date: 2004-02-09
    body: "My guess is: no. At least, I hope not to be honest. \nWhy would you _want_ to create non-GPL applications and not pay Trolltech for the excellent toolkit they provide that makes it all possible? Note that you can still ask a fee for GPL programs, it's not like it's impossible to create commercial GPL applications. \nNote that you can allready create GTK programs, and since they run in KDE too, I think you can get quite close, especially with stuff like FUSE available."
    author: "Andr\u00e9 Somers"
  - subject: "Re: Trying not to be a license troll..."
    date: 2004-02-09
    body: "in principle I think you are right with the non-GPL applications. But I cannot do GPL applications for Windows and Linux using QT, cause the noncommercial-licence forbids to make money of a program, unlike the GPL, which explicitely allows me to do so! The real problem with QT is not being restricted to the GPL but being restricted to Linux/MacOSX when doing GPL programs."
    author: "Michael"
  - subject: "Re: Trying not to be a license troll..."
    date: 2004-02-09
    body: "KDE doesn't consist only of GPL applications, far from it, see http://tinyurl.com/39epy . Using the Qt/X11's QPL you can choose any license you want as long as you include the software's source code. If you don't care to contribute (either by offering source code or by paying the Qt developers) you are very welcome to look elsewhere."
    author: "Datschge"
  - subject: "Re: Trying not to be a license troll..."
    date: 2004-02-10
    body: "Just to clarify I'm not someone who is interested in not paying Trolltech, but I have seen a lot of license trolls about KDE.  I was wondering whether we are close to having a way for them to keep their precious LGPL'ness or commercialness by allowing GTK apps to access parts of KDE.\n\nBesides, it is a bit of a stretch to call a GTK app that is using KDE desktop libraries (such as IOSlaves, dialogs, theming) a QT derivative.  The only thing that makes this so is multiple levels of linking though non-GPL'd (usually LGPL, BSD, or other) KDE libraries to the GPL'd QT.\n\nIf I started doing UI code for KDE, I'd personally use QT since from all accounts it is easier to work with and leads to faster development times.  But for those who dislike QT licensing for whatever reason, I'd think having the option to do GTK-KDE apps without QT licensing restrictions would maybe give them no more reason to troll KDE.  They'd have to make up some other imaginary problem, as this slight problem (or non-problem, depending who you ask) would no longer exist."
    author: "anonymous"
---
In <a href="http://members.shaw.ca/dkite/feb62004.html">this week's KDE-CVS-Digest:</a>
<A href="http://edu.kde.org/kstars">KStars</A> now has constellation lines. 
<A href="http://gwenview.sourceforge.net/">Gwenview</A> is now a KPart, for embedded use in Konqueror. 
Plus many bug fixes and improvements in <A href="http://kmail.kde.org/">KMail</A> and 
<A href="http://www.konqueror.org/">Konqueror</A>.


<!--break-->
