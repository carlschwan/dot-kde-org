---
title: "KDE-CVS-Digest for March 19, 2004"
date:    2004-03-20
authors:
  - "dkite"
slug:    kde-cvs-digest-march-19-2004
comments:
  - subject: "Thanks Derek!"
    date: 2004-03-20
    body: "One problem I found with the window manager settings is that I tell it to open windows centered yet for non-KDE applications this setting doesn't apply. Is this a bug that can be fixed?"
    author: "ASDF"
  - subject: "Re: Thanks Derek!"
    date: 2004-03-20
    body: "Possibly, if you complain about it at the right place (hint: http://bugs.kde.org) with the right amount of information (hint: which applications).\n"
    author: "Lubos Lunak"
  - subject: "KRecipes!"
    date: 2004-03-20
    body: "Good to see KRecipes is progressing nicely.  I am using the old version right now and it is excellent but has so much potential to awesome.  Great work on the program and keep it up guys!"
    author: "angry_mike"
  - subject: "Re: KRecipes!"
    date: 2004-03-20
    body: "It really belong s in kdeextragear, not in kdenonbeta"
    author: "Erik Pedersen"
  - subject: "Re: KRecipes!"
    date: 2004-03-21
    body: "Tell this its author. Perhaps he still considers as unstable considerung the 0.4.1 version number."
    author: "Anonymous"
  - subject: "Re: KRecipes!"
    date: 2004-03-22
    body: "Hehehe, thanks. 0.5 will be our first Beta, really :) All versions up to 0.4.1 have been accordingly tagged as alphas.\n\nWe're trying to make into the app. the most important features for this new version, and after that begin making it more stable, improve code, useability, accessibility... there's a long way before 1.0 :)))"
    author: "uga"
  - subject: "OT: NMM should be hosted on fd.o"
    date: 2004-03-20
    body: "There have been news about some projects planning/implementing NMM support.\n(NMM: http://www.networkmultimedia.org)\n\nAlthough NMM is not a kde-related project right now, it could be very \nuseful for the Linux desktop.\n\nThat's why I think, if kde plans to use NMM, kde should, for the success of\nthe Linux desktop, encourage NMM to become hosted on fd.o\n\nNo kde-only networkmultimedia please!\n\nAnd while this is done, please get rid of atrs in the same time. "
    author: "ac"
  - subject: "Re: OT: NMM should be hosted on fd.o"
    date: 2004-03-20
    body: "GStreamer is already hosted at freedesktop.org.  I think GStreamer is a better project.  I'm not sure what rules fd.o has about hosting similar projects, but you may have a hard time convincing them to host NMM when GStreamer is already there.  Also, I don't want a \"KDE uses NMM and Gnome uses GStreamer\" situation to happen.  It's already decided that Gnome will use GStreamer, so if KDE decides to use NMM, NMM better be a WHOLE LOT better than GStreamer to justify the incompatibility."
    author: "Spy Hunter"
  - subject: "Re: OT: NMM should be hosted on fd.o"
    date: 2004-03-20
    body: "The fd.o guys don't have a problem with hosting different projects. \nBut the biggest problem with NMM is that it is not a real community project\nbut more of a research project.\n"
    author: "Oliver Bausinger"
  - subject: "Re: OT: NMM should be hosted on fd.o"
    date: 2004-03-20
    body: "and research is very bad?\n\nanother idea - incorporate interesting things (algorithms?) of NMM into GStreamer"
    author: "Nick Shafff"
  - subject: "Re: OT: NMM should be hosted on fd.o"
    date: 2004-03-20
    body: "> and research is very bad?\n\nNo, it's just that:\n\n- it may or may not be evolved into a community project, rather than a research project founded by a university\n- it may not be atuned for desktop usage as much as a non-research project might"
    author: "anon"
  - subject: "Re: OT: NMM should be hosted on fd.o"
    date: 2004-03-20
    body: "Did you already take a look at it? My impression of NMM not really being \"attuned for desktop usage\" is imo actually a huge feature considering that it already allows you to share and transport multimedia data between different machines in a network, eg. manage all music and video data on your computer and stream it to your stereo/TV with respective adapted interfaces, the ideal way I'd want to setup TV/music set-top boxes in a home network.\n\nAs for the worry that a research project might not evolve into a community project you must be kidding considering the many FOSS projects which directly and indirectly started as research projects."
    author: "Datschge"
  - subject: "Re: OT: NMM should be hosted on fd.o"
    date: 2004-03-21
    body: "> Did you already take a look at it? My impression of NMM not really being \"attuned for desktop usage\" is imo actually a huge feature considering that it already allows you to share and transport multimedia data between different machines in a network, eg. manage all music and video data on your computer and stream it to your stereo/TV with respective adapted interfaces, the ideal way I'd want to setup TV/music set-top boxes in a home network.\n\nThat's very cool, but still not a good example for desktop usage (not saying that NMM isn't, just saying that example isn't) :)\n\n> As for the worry that a research project might not evolve into a community projet y\nou must be kidding considering the many FOSS projects which directly and indirectly started as research projects.\n\nOf course, that's why I said \"may or may not\"... for every research project that morphs into a community project, there are two that don't. "
    author: "anon"
  - subject: "Re: OT: NMM should be hosted on fd.o"
    date: 2004-03-22
    body: "In the potential for desktops to do a huge amount more, and reach out to many different devices, I'd say it is very significant for the desktop. Look at the popularity of MythTV."
    author: "David"
  - subject: "Re: OT: NMM should be hosted on fd.o"
    date: 2004-03-22
    body: "The software on Freedeskto.org should be research/reference software. It is a site to promote desktop interoperability, and not to be another sourceforge. Xserver is there because Xserver is research. Just because there are a lot of people trying to make it another sourceforge doesn't mean we have to participate in the destruction of a good idea."
    author: "David Johnson"
  - subject: "Re: OT: NMM should be hosted on fd.o"
    date: 2004-03-20
    body: "Surely it should be the other way around? Just because GStreamer is used in Gnome doesn't mean it's ideal for us, the choice of multimedia backend should stand on its own merits."
    author: "Stephen Douglas"
  - subject: "Re: OT: NMM should be hosted on fd.o"
    date: 2004-03-20
    body: "Not at all, and for two reasons:\n\n1) From the users point of view, one single multimedia framework being shared by all desktop applications means less packages to install and manage, and less multimedia frameworks to configure\n\n2) From the programmer's point of view, it means more work; if KDE goes for NMM or MAS, it will also have to support Gstreamer so those who use Gstreamer for most of their other multimedia applications don't have to manage two underlying frameworks\n\narts is such a mess not only because it's buggy, but because it's yet another thing to configure on top of the kernel modules and (possibly) ALSA. If we go and adopt yet another new standard, apart from freedesktop.org, we make things yet more hellish for programmers and users alike."
    author: "Tom Chance"
  - subject: "Re: OT: NMM should be hosted on fd.o"
    date: 2004-03-20
    body: "If KDE starts using GStreamer I will probably switch to Gnome. Whats the point of KDE if it only wraps up Gnome technology? Just another layer that introduces bugs and has to play catch up with the real thing."
    author: "Nick"
  - subject: "Re: OT: NMM should be hosted on fd.o"
    date: 2004-03-20
    body: "gstreamer != gnome technology\n\nPS: we'd gladly have you switch to gnome, I sure don't want people like you in the kde community."
    author: "anon"
  - subject: "Re: OT: NMM should be hosted on fd.o"
    date: 2004-03-21
    body: "gstreamer == gnome technology\n\nPS: The kde developers seem to have lost the power to create. Instead they write lame wrappers around ugly C-libraries. Is this KDE spirit? if yes, then I am out, probably with many others. "
    author: "Nick"
  - subject: "Re: OT: NMM should be hosted on fd.o"
    date: 2004-03-21
    body: "GStreamer is used by Gnome, but it is not a Gnome technology. If it was specific to Gnome would KDE even be able to consider it?"
    author: "Max Howell"
  - subject: "Re: OT: NMM should be hosted on fd.o"
    date: 2004-03-21
    body: "Creating a multimedia framework from scratch would take at least three years to stablize and more than probably 50k (more realistically, 100k+) LOC.. are you willing to code such a beast?"
    author: "anon"
  - subject: "Re: OT: NMM should be hosted on fd.o"
    date: 2004-03-21
    body: "Ah, good old Not Invented Here Syndrome. Killed many a company that spent years trying to re-implement something they could have had for free if they had just swallowed their silly pride and joined a common cause.\n\nIt seems like your comment is based on nothing but immature hubris and some misguided asthetic about the ugliness of C.\n\nThe part you don't seem to get is that, _if_NMM_becomes_a_mature_project_, and it is really superior enough to justify the cost of changing everything over, then KDE *and* GNOME will both switch to NMM. Its pointless to fracture the desktop community (that extends beyond *your* pet desktop, and the sandbox politics that fringe users like yourself seem to develop) for some perceived benefit of a product that isn't even useful yet.\n\nWhen NMM ships code that blows the socks off GStreamer, *then* its time to consider switching.\n\nCheers,\nRyan"
    author: "Ryan"
  - subject: "Re: OT: NMM should be hosted on fd.o"
    date: 2004-03-21
    body: "No GNOME wouldnt change to NMM, since it is writen in good KDE-like C++. To much politics in the GNOME camp to accept that (did they change to aRts back when it was superior or use DCOP?)."
    author: "Allan S."
  - subject: "Re: OT: NMM should be hosted on fd.o"
    date: 2004-03-21
    body: "aRts has never been superior to anything, and it's no surprise GNOME never adopted it.  As for DCOP, by the time it was clear DCOP was a success, GNOME already had too much infrastructure built up around CORBA and Bonobo to switch."
    author: "Spy Hunter"
  - subject: "Re: OT: NMM should be hosted on fd.o"
    date: 2004-03-21
    body: "arts was way superior to esd back in 2000 :)"
    author: "anon"
  - subject: "Re: OT: NMM should be hosted on fd.o"
    date: 2004-03-22
    body: "I think Stefan Westerfeld tried to reach a hand out to Gnome by using glib in arts. It seemed however as if the Gnome people chopped it off. What do you learn from it?"
    author: "Nick"
  - subject: "Re: OT: NMM should be hosted on fd.o"
    date: 2004-03-22
    body: "No, arts used more and more glib in order to become part of CSL.. CSL was never implemented fully before people saw it's design flaws. \n\nNMM and gstreamer both have much better modular architectures."
    author: "AC"
  - subject: "Re: OT: NMM should be hosted on fd.o"
    date: 2004-03-22
    body: "Ok, so why not base the whole desktop on wine or lesstif? Come on just swallow your pride!\n\nIt seems to me like you never have had to deal with ugly legacy C code! It is a huge pain to work with and it is certainly not what I want to develop in my free time!\n\nI like(d) KDE because it broke with the old shit, because it is(was) something new and fascinating. That is the motivation of most developers I know from experience.\n\nbtw, you can always go to #gstreamer and experience some love for KDE from the gstreamer developers."
    author: "Nick"
  - subject: "Re: OT: NMM should be hosted on fd.o"
    date: 2004-03-22
    body: "> Ok, so why not base the whole desktop on wine or lesstif? Come on just swallow your pride!\n\nWe already depend on many C-based libs, when it makes not to duplicate their functionality. Do you really want to drop usage of libjpeg, libgif, libpng, cups, libsmb, libxml2, libxslt, openssl, mpeglib, libtiff, libmng, freetype, libogg/libvorbis, imlib, alsa, libsane, libgphoto, libdb, xlib/xrender/xvideo,  zlib, libmal, libldap, libpcre, libidn, libart, libaudiofile, xinelib, and probably more C-based libraries.\n\n> It seems to me like you never have had to deal with ugly legacy C code! It is a huge pain to work with and it is certainly not what I want to develop in my free time!\n \nAre you even developing a multimedia app in KDE, or are you just a troll? I don't know any \"nick\" in kde-multimedia. \n\n"
    author: "AC"
  - subject: "Re: OT: NMM should be hosted on fd.o"
    date: 2004-03-22
    body: ">> btw, you can always go to #gstreamer and experience some love \n>> for KDE from the gstreamer developers.\n\n--\n\n> Are you even developing a multimedia app in KDE, or are you just a troll? \n> I don't know any \"nick\" in kde-multimedia. \n\nI'd assume the latter since the GStreamer developers stated on \nseveral occasions (some of them more than a year ago(?) when \nthe topic of choosing a MM framework for KDE 4 came up) \nthat they want to work with the KDE team, too.  \n\n"
    author: "cm"
  - subject: "Re: OT: NMM should be hosted on fd.o"
    date: 2004-03-22
    body: "They also hate it and stated on several occasions that they want it dead in the water."
    author: "Nick"
  - subject: "Re: OT: NMM should be hosted on fd.o"
    date: 2004-03-22
    body: "You can surely give a proof for your accusations, \nor I'll call you a would-be troll...\n\n"
    author: "cm"
  - subject: "Re: OT: NMM should be hosted on fd.o"
    date: 2004-03-22
    body: "To throw in some info myself: \n\nAbout GStreamer commitment to compatibility \nthrough KDE release cycles (on IRC one year ago):\nhttp://lists.kde.org/?l=kde-multimedia&m=104614380715889&w=2\n\nGeneral: Here's a pointer to the discussions one year ago: \nhttp://lists.kde.org/?l=kde-multimedia&m=104633304511264&w=2\n\n"
    author: "cm"
  - subject: "Re: OT: NMM should be hosted on fd.o"
    date: 2004-03-22
    body: "\"Are you even developing a multimedia app in KDE, or are you just a troll? I don't know any \"nick\" in kde-multimedia.\"\n\nNo I don't develop KDE-MM stuff, Its people like you that scare developers away.\n\nAnd about the libraries you mentioned, they are pretty old. They are well known through insecurity news though, which shows their quality. \n\nYou certainly wouldn't start a project in C anymore these days, but thats exactly what gstreamer is. We programmers call that LBR software (legacy before release). I don't think KDE should sacrifice sanity for the sake of being compatible with the overhyped gstreamer project that favours the technology of gnome that was founded and exists to destroy KDE (Ximian people are on the gnome board of directors, mind you). There is no real value in gstreamer. The plugins come from other sources anyway and 95% of the users dont even want a framework like arts, nmm or gstreamer, because they just want to listen music or watch a dvd. \n\n"
    author: "Nick"
  - subject: "Re: OT: NMM should be hosted on fd.o"
    date: 2004-03-22
    body: "Funny. Try telling that to the Gnome people then.\n\n\"When NMM ships code that blows the socks off GStreamer, *then* its time to consider switching.\"\n\nIt already does, and it seems to be pretty well structured. It would be silly not to use it for the sake of what some people think is unity."
    author: "David"
  - subject: "Re: OT: NMM should be hosted on fd.o"
    date: 2004-03-20
    body: "You are welcome to do that, but it must be noted that GStreamer isn't a GNOME technology. It is written in C, but if we're so obsessive about getting rid of every trace of C code on the system, we might as well fork the kernel and write \"KLinux\""
    author: "Rayiner Hashem"
  - subject: "Re: OT: NMM should be hosted on fd.o"
    date: 2004-03-20
    body: "Amen Amen I say to thee.\n\nI use GNOME  and have k3b installed on my system. Although I do not use it much. I figured until space was a concern, it doesn't matter that I have the whole of kdebase installed too.\n\nI support GStreamer because it is very forward thinking. It may actually be a good thing that GStreamer is done in C rather than C++. It not like there are no bindings for C++. There are some in teh KDE CVS already anyway.\n\nThe biggest complaint I heard was that about glib. well, I do not know if installing glib is a bad thing in itself too."
    author: "Maynard"
  - subject: "Re: OT: NMM should be hosted on fd.o"
    date: 2004-03-20
    body: "> The biggest complaint I heard was that about glib. well, I do not know if installing glib is a bad thing in itself too.\n\nThat's not a problem, since arts uses it too. However, I think the MM folks prefer something that is more hackable. One of the reasons arts wasn't well maintained once Stefan Westerfeld became too busy with real life was that it's code was full glib-isms that nobody wanted to learn :)"
    author: "anon"
  - subject: "Re: OT: NMM should be hosted on fd.o"
    date: 2004-03-21
    body: "Well, it is a shame really. I suppose in a way that is the difference between free software and proprietary. If someone won't learn to use a library because it full of glib-sims, then it really ca't be helped.\n\nGstreamer has a lot goin for it. Besides being quite portable (written in C), it is very light. It was successfully used in embedded applications. It is really futuristic and useful NOW, not later. NMM will just be an extra library for codec makers to support, or devlopers to develop for. Not to say a decision has already been made which to support by third parties, "
    author: "Maynard"
  - subject: "Re: OT: NMM should be hosted on fd.o"
    date: 2004-03-20
    body: "\"I use GNOME and have k3b installed on my system. Although I do not use it much. I figured until space was a concern, it doesn't matter that I have the whole of kdebase installed too.\"\n\nTry looking at the space equivalent software would take up on Windows. Having Gnome and KDE installed is small in comparison. Makes you wonder what it all does...."
    author: "David"
  - subject: "Re: OT: NMM should be hosted on fd.o"
    date: 2004-03-21
    body: "Whats wrong in using GNOME technology. Infact this is what OSS is all about, that no one needs to redo already done work. If their technology is good we must use it.\n\nGNOME and KDE need to go together. Multimedia server should be gnome and kde independent project and both desktops need to accept same standards.\n\nLinux desktop should not be some GNOME vs KDE fight but 2 desktops co-existing having some common standards so that users can switch between the two easily."
    author: "Nilesh Bansal"
  - subject: "Re: OT: NMM should be hosted on fd.o"
    date: 2004-03-22
    body: "There is nothing wrong with it, in fact KDE uses many libraries that originate in GNOME, such as libxml2, libart, etc. gtkhtml in turn was a port of kde 1.x's khtmlw and is used in GNOME. I'm sad that there isn't as much reuse of KDE-originated code in GNOME as the reverse as the GNOME devs still don't seem to want C++ :("
    author: "AC"
  - subject: "Re: OT: NMM should be hosted on fd.o"
    date: 2004-03-20
    body: "The choice of multimedia framework has to be good, and has to have potential to be extended far further in the future, as Linux and freedesktops get ported to many different devices and environments. NMM looks extremely promising in this regard. You can't compromise on quality for the sake of 'one this' and 'one that'. The question is, would everyone else be willing to compromise for this?\n\n\"if KDE goes for NMM or MAS, it will also have to support Gstreamer so those who use Gstreamer for most of their other multimedia applications don't have to manage two underlying frameworks\"\n\nWhy? Would Gnome or any other environment support NMM? Stop making it sound as if GStreamer is the only option, because it isn't.\n\n\"If we go and adopt yet another new standard, apart from freedesktop.org, we make things yet more hellish for programmers and users alike.\"\n\nExcuse me? The last I looked GStreamer was most certainly not promoted as a standard on freedesktop."
    author: "David"
  - subject: "Re: OT: NMM should be hosted on fd.o"
    date: 2004-03-20
    body: "I think gstreamer would already been accepted as the choice for KDE 4 if it weren't for the fact it extenstively used glib and gobject. There is nothing wrong with those two libs (no political problems), but technically, there might be. arts is also based on glib, and there were only a few people willing to hack it between KDE 2.0 and KDE 3.2, hence it's buggyness.\n\nOn the other hand, the gstreamer development team seems _very_ supportive and eager to fix any KDE related bugs in gstreamer. "
    author: "anon"
  - subject: "Re: OT: NMM should be hosted on fd.o"
    date: 2004-03-20
    body: "<i>arts is also based on glib, and there were only a few people willing to hack it between KDE 2.0 and KDE 3.2, hence it's buggyness.</i>\n\nGood thing is GSTreamer is already adopted by Gnome, and hosted on freedesktop. That means that even if KDE developers don't take much time coding on it, the project will still be supported by other projects."
    author: "Coma"
  - subject: "Re: OT: NMM should be hosted on fd.o"
    date: 2004-03-21
    body: "What does that have to do with anything? GStreamer's inclusion on freedesktop.org was _exactly_ not an endorse, although a deliberate attempt by the GNOME camp to spread general confusion and misinformation."
    author: "Allan S."
  - subject: "Re: OT: NMM should be hosted on fd.o"
    date: 2004-03-21
    body: "As one of the people actually involved in the move of GStreamer to fd.o let me explain a couple of things.\n\na) being hosted on freedesktop is not an endorsement or a proof of standarization. If you read the guidelines for fd.o hosting you see that any project which wants to be a cross desktop technology can be hosted there. Just like there are many projects in GNOME CVS and in KDE CVS that are not 'official' part of those desktop, so will there be in freedesktop CVS not (still) officially standards. You will notice two X implementations hosted there atm for instance.\n\nb) It was partly a technical decision since we where getting fed up with sourceforge having so many issues\n\nc) Sending out a clear signal that we are serious about crossdesktopness was an important part of the decision. We never did anything IMHO opinion to spread confusion and misinformation. In fact if you read my recent GStreamer article on OSNEWS you would see that I clearly explain the fd.o move with wanting to send out such a signal in the hope of increasing the chance of KDE inclusion. \nd) Nowhere have any GStreamer developer claimed that being on fd.o is the standard. Only place I ever seen that argument is in fact on kde sites and lists, and the normally by people who are not involved with GStreamer but who use/develop KDE and want or don't want gstreamer in KDE. So I think you should look at bit closer to home before pointing fingers and casting blame."
    author: "Christian Schaller"
  - subject: "Re: OT: NMM should be hosted on fd.o"
    date: 2004-03-20
    body: "can't kde have some kind of KAudioObject which all kde-apps use, which in turn just uses the user-selected audio system (nmm, gstreamer, jack, alsa, oss,  arts, esd etc.)?? Would that introduce much overhead/latency?\n\nDoes this question disclose that I have no clue? :)"
    author: "me"
  - subject: "Re: OT: NMM should be hosted on fd.o"
    date: 2004-03-20
    body: "the problem is that some of those, like gstreamer already have that mechanism t o plug into different sound servers. "
    author: "anon"
  - subject: "Re: OT: NMM should be hosted on fd.o"
    date: 2004-03-20
    body: "NMM can't be hosted on freedesktop (without a fork) because it's a research project funded by a university, and they want control of it."
    author: "anon"
  - subject: "Re: OT: NMM should be hosted on fd.o"
    date: 2004-03-20
    body: "The hosting issue shouldn't matter afaics, there already were and still are quite some projects represented on fd.o which don't make use of its CVS and bug tracker."
    author: "Datschge"
  - subject: "Re: OT: NMM should be hosted on fd.o"
    date: 2004-03-20
    body: "Seems that they use sourceforge CVS for development:\n\nhttp://sourceforge.net/projects/nmm"
    author: "tux"
  - subject: "Re: OT: NMM should be hosted on fd.o"
    date: 2004-03-20
    body: "Since it is licensed under the GPL/LGPL I very much doubt that the university can totally control the project if the developers perhaps want to get involved with freedesktop. I don't see any problems."
    author: "David"
  - subject: "I'm a newbie to all this..."
    date: 2004-03-21
    body: "How do these sound systems compare? MAS vs NMM vs JACK vs GSTREAMER etc.?\n\nWhy is Gstreamer not the best choice in all cases? What's bad about Glib and Gobject, does it produce unreadable code?\n\nDid GNOME already adopt Gstreamer? Which sound architecture is KDE leaning towards?"
    author: "Alex"
  - subject: "Re: I'm a newbie to all this..."
    date: 2004-03-21
    body: "JACK is simply an audio server, not a multimedia framework.  All it allows you to do is play more than one sound at a time and have them all mixed together.  MAS, GStreamer, and NMM all appear to be comparable graph-based multimedia frameworks.  They allow you to connect items together in a graph structure, like: file source -> MP3 file format reader -> MP3 data to raw audio converter -> raw sound card output.  MAS is concerned more with X windows integration.  GStreamer tries to be fast and useful in general for more than just media playback (think nonlinear video editing, etc).  NMM appears to be focusing on synchronized playback on multiple networked devices and handoff between devices (play music on your PDA while you jog and hand it over to the stereo when you get home).\n\nGStreamer seems to me to be the project most focused on the needs of the desktop computer user.  NMM's network tricks look impressive at first glance, but I question whether all that network stuff needs to be built into the multimedia framework.  It can be done more easily and more sensibly by plugins or at the application level IMHO, and that way only applications that need it (which are pretty few after all) have to worry about it.  I don't see why the same things that NMM is doing couldn't be done with GStreamer.  As for MAS, I'm not sure how much active development is going on in that project.  I think GStreamer is a more active project.  I'm not sure how the technical capabilities of MAS and GStreamer stack up, but I can't imagine that GStreamer is any worse than MAS.\n\nGlib and Gobject are projects that are not technically part of the GNOME project, but they have strong ties to GNOME.  There is some reluctance in the KDE community to depend on GNOME's underlying C libraries.  As for code readability, that is a matter of taste, but C++ bindings to GStreamer would eliminate that complaint, and they would almost certainly be made before KDE adopted GStreamer.\n\nIt has been announced that GStreamer will be a standard part of the next GNOME release.  KDE is not leaning toward any particular multimedia architecture at this time, though almost everyone agrees that aRts is not going to be it.  The new multimedia architecture will be decided on at some point before KDE 4 is released."
    author: "Spy Hunter"
  - subject: "Re: I'm a newbie to all this..."
    date: 2004-03-21
    body: "The network stuff isnt \"built-in\" in NMM, it is a plugin just like any other. Imagine the play graph, and just add a network-plugin: Source->Decoder->Network->Audio-server."
    author: "Allan S."
  - subject: "Re: I'm a newbie to all this..."
    date: 2004-03-21
    body: "Well what's so special about that then?  A network plugin for GStreamer would be easy to make, if someone wanted to make one (they don't though, because there's hardly any practical use for such a thing right now).  I was under the impression that NMM involved the network at a more fundamental level."
    author: "Spy Hunter"
  - subject: "Re: I'm a newbie to all this..."
    date: 2004-03-21
    body: "> As for code readability, that is a matter of taste, but C++ bindings to GStreamer would eliminate that complaint,\n> and they would almost certainly be made before KDE adopted GStreamer.\n\nKDE wrapper for GStreamer: \nhttp://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdenonbeta/gst"
    author: "Christian Loose"
  - subject: "amaroK as a test bed for NMM & Gstreamer"
    date: 2004-03-21
    body: "For people interested in comparing NMM & Gstreamer, please note that the amaroK developpers are currently trying NMM. Let's wait for their feedback since they have already tried Gstreamer .\n\nSee the following threads.\nhttp://lists.kde.org/?l=kde-multimedia&m=107763526429514&w=2\nhttp://lists.kde.org/?l=kde-multimedia&m=107868076101110&w=2\n\nFind also some pros & cons relatively to NMM & Gstreamer here.\nhttp://lists.kde.org/?l=kde-multimedia&m=107757209614312&w=2\n\nOther remarks:\n\n1)Relative to NMM being a research project, if I remenber correctly, it's exactly what was Gstreamer at the beginning.\n\n2) Relative to NMM being \"mature\", one should look at their \"Multimedia box\".\nhttp://graphics.cs.uni-sb.de/NMM/Status/MMBox/index.html.\n\nRegards\n\nPhil Ozen\n\n "
    author: "Phil Ozen"
  - subject: "KControl reorganisation"
    date: 2004-03-20
    body: "http://webcvs.kde.org/cgi-bin/cvsweb.cgi/~checkout~/kdebase/kcontrol/TODO\n\nGood to see they're working on the KControl reorganisation."
    author: "Niek"
  - subject: "SVN-Digest?"
    date: 2004-03-20
    body: "Is there any move to switch over to SVN for version control? It sounds like a natural progression from CVS (although I couldn't find any info whether there is a way to upgrade a current CVS repository).\n"
    author: "john"
  - subject: "Re: SVN-Digest?"
    date: 2004-03-20
    body: "Subversion is still too imature for us to adopt, though it's likely we will at some point in the future. It has many features we'd like - especially the better support for tags, branches and versioned directory handling. One of the major things lacking is support for it in the surrounding tools, though i'm told recent versions of viewcvs support it (and work on support in cervisia is started).\n\n"
    author: "Richard Moore"
  - subject: "Re: SVN-Digest?"
    date: 2004-03-22
    body: "Should you rather go with a GPL'ed version control system like GNU's Arch <http://wiki.gnuarch.org/>?\n\nThat IMO should be supported by kdevelop also at some point..."
    author: "Nobody"
  - subject: "Re: SVN-Digest?"
    date: 2004-03-22
    body: "Arch has the wrong model for KDE - we want a central repository.\n\n"
    author: "Richard Moore"
  - subject: "Richard Dale Rocks!"
    date: 2004-03-20
    body: "Wow, a brand new set of KDE#/Qt# bindings?  Richard, you rock! \n\nI don't know how hard it was to do, but it sure sounds like a testament to all the foundation work you did with libsmoke.\n\nDo you have any examples or screenshots of running examples? :)\n"
    author: "Navindra Umanee"
  - subject: "Richard Dale for President!"
    date: 2004-03-20
    body: "I've been playing some with Mono for the last week. I think \"managed languages\" is a really interesting development for desktop programming, and while C++ with Qt may be very nice, C++ is nonetheless a barrier for many would-be KDE coders. So I was sad to see that more than a whole year had gone since the last release of the Qt# bindings. Today I decide to check again, and first I find that Qt# 0.7.1 was released just yesterday, and second I find Richard's smoke bindings. Excellent, thanks!"
    author: "Haakon Nilsen"
  - subject: "Re: Richard Dale Rocks!"
    date: 2004-03-21
    body: "Yer great, and I'm glad someone is still hacking on this.\n\nLet's hope that people can start seeing beyond the hype of Mono and .NET."
    author: "David"
  - subject: "Re: Richard Dale Rocks!"
    date: 2004-03-22
    body: "Hi Navindra - thanks!\n\nI've done what it says in the commit, but perhaps that isn't obvious. All the current generated code does is to funnel all the Qt/KDE method calls to a single C# method, SmokeInvocation.Invoke(). At present it looks like this:\n\n\t\tpublic override IMessage Invoke(IMessage myMessage) {\n\t\t\tConsole.WriteLine(\"MyProxy 'Invoke method' Called...\");\n\t\t\tif (myMessage is IMethodCallMessage) {\n\t\t\t\tConsole.WriteLine(\"IMethodCallMessage\");\n\t\t\t}\n\t\t\tif (myMessage is IMethodReturnMessage) {\n\t\t\t\tConsole.WriteLine(\"IMethodReturnMessage\");\n\t\t\t}\n\t\t\tif (myMessage is IConstructionCallMessage) {  \n \t\t\t}                \n\t\t}\n\nNotice that it doesn't do anything other than print debug messages! So the next step is too look up the corresponding method in the Smoke library and call it. That should be about another week or two. But it should then move quite quickly from just running 'hello world' to being pretty complete in a month or two.\n\n-- Richard"
    author: "Richard Dale"
  - subject: "kmail: support for html messages"
    date: 2004-03-20
    body: "What didn't make it into the digest:\n\nrevision 1.798\ndate: 2004/03/17 19:23:27;  author: eschepers;  state: Exp;  lines: +381 -20\nnew feature: composing HTML messages\n\nAlthough I generally find html messages disgusting, I understand that kmail does somehow *have* to support this. Out of curiosity I wrote two messages to myself using html and it worked *very* well. Good job! Not that I would ever use the feature. Actually, my mail system marks every incoming html message as spam right away -- very few false positives so far.  ;-)"
    author: "Melchior FRANZ"
  - subject: "Re: kmail: support for html messages"
    date: 2004-03-21
    body: "Well. I actually reject HTML at the server. 99.5% of spam never reaches my inbox."
    author: "Norberto Bensa"
  - subject: "how about phpgroupware?"
    date: 2004-03-20
    body: "it's cool that kdepim and egroupware are working together but I'm a phpgroupware user so I was just wondering whether I should switch or not..."
    author: "Pat"
  - subject: "Re: how about phpgroupware?"
    date: 2004-03-20
    body: "http://members.shaw.ca/dkite/dec52003.html\n\nphpgroupware support was added at the same time as egroupware support.\n\nDerek"
    author: "Derek Kite"
  - subject: "Best quote ever"
    date: 2004-03-20
    body: "- Make aRts work with dmix. Two important changes:\n  1. Dmix uses read-events to signify write-events, so we need to autodetect what kind of crack ALSA is smoking today."
    author: "Cirrus"
  - subject: "Thank you Derek"
    date: 2004-03-20
    body: "BTW: I just installed KDE 3.2.1 on Xandros 2 Business Edition! It rocks! Much better than even their customized KDE imo. =)"
    author: "Alex"
  - subject: "Marco Lohse on NMM vs GSTREAMER!!!!"
    date: 2004-03-21
    body: "Check it out: http://graphics.cs.uni-sb.de/pipermail/nmm-dev/2002-November/000024.html\n\nOf particular note:\n\n\"Of course, we would like to see cooperation with KDE people, actually we \nwanted to start to develop a media player for KDE, but we changed our \nplans because we currently do not have enough 'manpower'. That is why we \nfocus on the development of the Multimedia-Box. (Of course, the \nmultimedia-box is in some sense also a 'player'.)\"\n\nSeems like these people like KDE and would be pleased and cooperative if KDE chose to use it. =) One plus for NMM. Is there any chance GNOME would adopt NMM?\n\nReally, the main thing going for Gsreamer is a unified architecture between KDE and GNOME.\n\nBTW: Please tell me the difference between NMM and Gstreamer."
    author: "Alex"
  - subject: "Re: Marco Lohse on NMM vs GSTREAMER!!!!"
    date: 2004-03-21
    body: "> BTW: Please tell me the difference between NMM and Gstreamer.\n\nGood things about gstreamer:\nRelatively mature (1.0 release upcoming before KDE 4.0)\ndevelopers who want to cooperate with KDE\ngnome will use it\nmodular architecture + lots of modules\n\nBad things about gsteamer:\nnot C++\nuses glib+gobject\nnot network transparent\n\nGood things about NMM:\nC++\nNetwork transparent\ndevelopers who want to cooperate with KDE\nmodular architecture\n\nBad things about NMM:\nnot terribly mature, compilation often breaks ATM\nresearch project (no idea if developers will continue after graduation, and many experimental features not suitable for desktops like KDE and GNOME)\n\n"
    author: "anon"
  - subject: "Re: Marco Lohse on NMM vs GSTREAMER!!!!"
    date: 2004-03-21
    body: "For a comparison between NMM & Gstreamer, please note that the amaroK developpers are currently trying NMM. Let's wait for their feedback since they have already tried Gstreamer .\n\nSee the following threads.\nhttp://lists.kde.org/?l=kde-multimedia&m=107763526429514&w=2\nhttp://lists.kde.org/?l=kde-multimedia&m=107868076101110&w=2\n\nFind also some pros & cons relatively to NMM & Gstreamer here.\nhttp://lists.kde.org/?l=kde-multimedia&m=107757209614312&w=2\n\nRegards\n\nPhil Ozen\n"
    author: "Phil Ozen"
  - subject: "Re: Marco Lohse on NMM vs GSTREAMER!!!!"
    date: 2004-03-21
    body: "Some of the things you say are very, well, \"flamey\".\n\nGStreamer can be network transparent if you want it to. Its a matter of ju\\\\making a plugin. GStreamer can already play files on a webpage, a network source etc.\n\nWhat is wrong with being coded in C++?\nWhat is wrong with GObject and Glib?\n\nGStreamer has C++ bindings in KDE CVS i am sure. Juk uses them. glib and gobject are not huge. It is not a big issue. Why reinvent the wheel?\n\nApparently, C does not impose an API on you like C++, so it makes C a good choice for a library like GStreamer which is abstracted anyway. \n\nAnother good thing about GStreamer is that it is very small and can fit on a handheld. It has already done this. It is useful in embedded applications. It also helps to use the same framework on different devices."
    author: "Maynard"
  - subject: "Re: Marco Lohse on NMM vs GSTREAMER!!!!"
    date: 2004-03-21
    body: "\"Some of the things you say are very, well, \"flamey\".\"\n\nMmm. Well.\n\n\"GStreamer can be network transparent if you want it to. Its a matter of ju\\\\making a plugin.\"\n\nSo it doesn't do network transparent stuff, nor is it fundamentally designed for it.\n\n\"GStreamer has C++ bindings in KDE CVS i am sure.\"\n\nHere we go with the bindings stuff. Although C makes sense for a lot of low level stuff, if you are going to have an object-oriented structure for something of any kind, and it needs to be native and fast at a fundamental level, (like the basis of a desktop environment) for god's sake make the structure of it and choose tools that are logically object-oriented. That means C++, and it is arguably the only thing C++ is good at. Bindings are just stupid for this sort of thing as you are quickly adding totally unecessary overhead. As Linus Torvalds says, make the core of it 'sane'.\n\n\"Apparently, C does not impose an API on you like C++, so it makes C a good choice for a library like GStreamer which is abstracted anyway.\"\n\nThis is an argument that has gone on for too long. See above.\n\n\"GStreamer has C++ bindings in KDE CVS i am sure. Juk uses them. glib and gobject are not huge. It is not a big issue. Why reinvent the wheel?\"\n\nYes this is a valid argument, but it I suppose it depends if it is outweighed by the issues above.\n\n\"Another good thing about GStreamer is that it is very small and can fit on a handheld. It has already done this. It is useful in embedded applications. It also helps to use the same framework on different devices.\"\n\nIt really needs to be network transparent for this to really happen, and since you need plugins for that there isn't really too much point, so no, the stuff above wasn't flamey. These are questions GStreamer is going to have to answer."
    author: "David"
  - subject: "Re: Marco Lohse on NMM vs GSTREAMER!!!!"
    date: 2004-03-21
    body: "No, it is fundamentally designed as a multimedia framework with very few capabilities. Most of the other capabilities can be added on as plugins. If you want to make it network transparent, add a plguin to do that. GStreamer itself is designed to not add any latency overhead to anything, so you should be ok.\n\nGStreamer is small and very fast. Low latency. I do not know what fast at a fundamental level is. Its either it is fast or not to me.\n\nProgramming is always with challenges. One reason Microsoft got to where it is is because they did stuff which was hard, because they had to. If you think you can avoid the hard stuff, like using GStreamer in KDE because you need to make bindings to do that, then you will be without an adequate multimedia framework for 3 years, or you will be using xine or mplayer, which are good players, but not such good frameworks.\n\nbesides, if KDE decides to use GStreamer, the interested parties could just become actively involved in GStreamer, i.e., keep the bindings current and so on. If the bindins are written well enough, no one unfamiliar with GObject and glib would even notice a difference. That is why it is called abstraction.\n\nIn the beginning, GStreamer was meant for GNOME, but they changed, which is why they steered clear of requiring GTK for example. So you would expect a few GNOMEisms there. Its a heritage of their past."
    author: "Maynard"
  - subject: "Re: Marco Lohse on NMM vs GSTREAMER!!!!"
    date: 2004-03-21
    body: "> If you want to make it network transparent, add a plguin to do that. GStreamer itself is designed to not add any latency overhead to anything, so you should be ok.\n\nErm, come on. gstreamer has been acknowledged by it's authors as *not* network transparent, and not easily done. That's fine though, we probably don't need that in a desktop environment considering that the network transparency of arts was always broken :)\n\n> GStreamer in KDE because you need to make bindings to do that, then you will be without an adequate multimedia framework for 3 years, or you will be using xine or mplayer, which are good players, but not such good frameworks.\n\nOr using NMM.. it still has to be determined if NMM can support everything gstreamer can, or if it can be implemented within a year. This is why support was added in amarok. "
    author: "anon"
  - subject: "Re: Marco Lohse on NMM vs GSTREAMER!!!!"
    date: 2004-03-21
    body: "What we have said is that GStreamer 'itself' is not network transparent and that such should be handled through plugins. Exactly as the post you replied to pointed out. Our sound server plugins for Esound, Artsd and NAS is one example. Another cool project doing network transparent media handling is http://subsignal.org/pakt -> Warzav Pak.\n\nAs for NMM vs GStreamer, to me the issue isn't wether project a or project b can theoretically do what GStreamer does. The fact is that making these systems takes an enormous amount of resources to create and maintain and for the free software community to spread our resources out over multiple projects will only accomplish that result that we all reach parity with Mac and Windows later."
    author: "Christian Schaller"
  - subject: "Re: Marco Lohse on NMM vs GSTREAMER!!!!"
    date: 2004-03-21
    body: "You are overly playing down one important issue which KDE currently has with aRTs, one which would not go away when KDE chose to go with GStreamer: interested parties on the KDE side could *not* just become actively involved in GStreamer, the difference between KDE's Qt QObject C++ and GLib GObject C is too big. If it weren't we might never have had that many people bitching about aRTs without fixing/improving the itching parts in all the last years. Referring to the bindings as the way to hide away any potentially confusing difference is a joke in this regard, this will just further endorse the active/passive split aRTs caused in KDE before, basically keeping discouraging many multimedia related efforts within KDE at the most fundamental level already.\n\nImo the best KDE can do for version 4 is creating a native basic multimedia API with a flexibel plugable backend, making use of whatever audio framework and backend exist already or is needed for a specific application, environment or use case (ALSA on Linux, QuickTime on MacOSX, DirectSound on Windows, JACK for low lacency, MAS/NMM for network transparency, GStreamer for interoperability with GNOME, or something along this line)."
    author: "Datschge"
  - subject: "Re: Marco Lohse on NMM vs GSTREAMER!!!!"
    date: 2004-03-22
    body: "> environment or use case (ALSA on Linux, QuickTime on MacOSX, DirectSound on Windows, JACK for low lacency, MAS/NMM for network transparency, GStreamer for interoperability with GNOME, or something along this line\n\nThat seems to be duplicating what NMM and gstreamer are already *doing*. It does seem good to not lock into one multimedia server, but gstreamer has been active much longer than arts has, which was primarily a one man show even three years ago. "
    author: "AC"
  - subject: "Re: Marco Lohse on NMM vs GSTREAMER!!!!"
    date: 2004-03-22
    body: "(Ignoring the fact that you want a KDE native framework-framework so you can abstract out framework compatibilitites, essentially doing what the above frameworks are supposed to do in the first place...)\n\nSo your argument is that its better to implement a totally new and incompatible framework (NMM) which isn't as mature as the existing framework, possibly taking years to reach the point we are currenly at, just to avoid the possibility that some KDE developers won't want to learn Glib? And *that* is a better way to allocate resources?\n\nPlease, the whole point of a mature library is that users never need to know how it works because it just does. Sure there will be hackers who won't hack GStreamer because its in Glib, but I think you grossly over estimate the problem that poses when we're talking about a mature library a year down the road.\n\nCheers,\nRyan"
    author: "Ryan"
  - subject: "Re: Marco Lohse on NMM vs GSTREAMER!!!!"
    date: 2004-03-22
    body: "\"So your argument is that its better to implement a totally new and incompatible framework (NMM)...\"\n\nIncompatible with what exactly? You make it sound as if GStreamer is some sort of standard already, which it isn't of course. You could easily argue at this point in time that GStreamer is incompatible with NMM or any other framework.\n\n\"which isn't as mature as the existing framework, possibly taking years to reach the point we are currenly at,\"\n\nJudging from their current progress and crucially, their pretty excellent documentation, I doubt it. I note the use of the word 'we'.\n\n\"just to avoid the possibility that some KDE developers won't want to learn Glib? And *that* is a better way to allocate resources?\"\n\nThe point is considering the logical object-oriented structure of something like a multimedia framework (or desktop environment :)), given the progress of the NMM guys, neither Glib or C programming should be necessary at all. It is totally unnecessary to create bindings and all sorts of unecessary abstraction crap just because of some peoples' attachment to Glib and C. You get the object-oriented structure of the system right from day one, without any need for bindings and overhead, and these days that means C++. For the vast majority of systems like this these days C just isn't necessary, and considering the size of many systems produced today, less of a good idea. Ditch Glib and C unless they are  absolutely necessary.\n\nNote the current arguments about using Mono in Gnome, and C being dead, and note the fact that KDE has never had, doesn't have and probably will never have such issues.\n\n\"Please, the whole point of a mature library is that users never need to know how it works because it just does.\"\n\nNo they don't, but they unwittingly see the effects of a poorly designed one."
    author: "David"
  - subject: "Re: Marco Lohse on NMM vs GSTREAMER!!!!"
    date: 2004-03-22
    body: "\"besides, if KDE decides to use GStreamer, the interested parties could just become actively involved in GStreamer, i.e., keep the bindings current and so on. If the bindins are written well enough, no one unfamiliar with GObject and glib would even notice a difference. That is why it is called abstraction.\"\n\nAbstraction in this case means adding totally unnecessary layers into a system. A layered system is good, but you should always learn to cut out the unnecessary stuff, and on the 'Gnome' oriented development side I have never understood why they have never understood this. A good rule in programming is to design your system in a logical (in this case object-oriented way) and use good tools available that allow you to totally reflect that organisation in code structure, as well as being fast and efficient. If you can do this, there is no need for layers and no need for any sort of abstraction. That means C++ these days, and amusingly, it is why there is such a hoo-ha about Mono and C. Note that KDE has none of these problems, and probably never will. However, since no one on the Gnome side of things has ever managed to wrap their head around this concept I suppose it is a bit pointless now.\n\n\"besides, if KDE decides to use GStreamer, the interested parties could just become actively involved in GStreamer, i.e., keep the bindings current and so on.\"\n\nExtra needless work. See above.\n\n\"In the beginning, GStreamer was meant for GNOME, but they changed, which is why they steered clear of requiring GTK for example.\"\n\nI should hope not. Why on Earth should a multimedia framework of any kind depend on a graphical toolkit?"
    author: "David"
  - subject: "Re: Marco Lohse on NMM vs GSTREAMER!!!!"
    date: 2004-03-21
    body: "> Some of the things you say are very, well, \"flamey\".\n \nWell, uhm... I tried to do my best for an objective comparison of stuff that has already been brought up on kde-multimedia and acknowledged by both gstreamer and NMM developers. I for one do want to see gstreamer become the standard multimedia API of KDE. I won't say that there aren't issues involving either of the projects though.\n\n\n> What is wrong with GObject and Glib?\n\nNothing political about it, it's just that there have been terribly experiences with software that uses it in the past (e.g, arts). \n\nI for one don't care about this.. just pointing out that other people do. Not as many as used to though.. most people have softened up to the political side. The technical side still remains though."
    author: "anon"
  - subject: "Re: Marco Lohse on NMM vs GSTREAMER!!!!"
    date: 2004-04-01
    body: "\"Another good thing about GStreamer is that it is very small and can fit on a handheld. It has already done this. It is useful in embedded applications. It also helps to use the same framework on different devices.\"\n\neat this:\n\nhttp://www.networkmultimedia.org/News/Events/CeBIT2004/index.html\n"
    author: "Florian Winter"
  - subject: "Audio Servers and Multimedia Frameworks?!"
    date: 2004-03-21
    body: "I ws just told by Spy Hunter that JACK is only an audio server part of the Agnula project, the same project ALSA comes from.\n\nhttp://www.agnula.org/documentation/dp_tutorials/alsa_jack_ladspa/\n\nNow I'm wondering, is Gstreamer also a sound server in addition to being a multimedia framework?\n\nCan it be used together with JACK? "
    author: "Alex"
  - subject: "Re: Audio Servers and Multimedia Frameworks?!"
    date: 2004-03-21
    body: "Yes. You just need a jacksink for output, and you let JACK do its stuff. jacksink already exists."
    author: "Maynard"
---
In <a href="http://members.shaw.ca/dkite/mar192004.html">this week's KDE CVS-Digest</a>:
KDE integrates <A href="http://www.go-mono.com/">Mono</A> with C# bindings. 
A <A href="http://www.php.net/">PHP</A> debugger integrated into <A href="http://quanta.sourceforge.net/">Quanta</A>. 
Work continues on <A href="http://www.egroupware.org/">eGroupware</A> / <A href="http://www.kontact.org/">Kontact</A> integration.
<A href="http://kopete.kde.org/">Kopete</A> rewrites the <A href="http://www.jabber.org/">Jabber</A> plugin.
Plus, a new tool for monitoring application usage.
<!--break-->
