---
title: "Evans Data Corp Survey Reveals Attitudes about KDE/Qt"
date:    2004-03-03
authors:
  - "npetreley"
slug:    evans-data-corp-survey-reveals-attitudes-about-kdeqt
comments:
  - subject: "more GPL v. LGPL wars?"
    date: 2004-03-02
    body: "\"KDE was once again the top choice in desktops, beating GNOME for the second survey in a row, although usage of KDE and GNOME are both increasing.\"\n\nIm very glad to hear this. Two desktops, one world. OK there are more than two desktops, but the world domination thing still holds. ;)\n\n\"But, as I explore in next week's Computerworld column, I was disappointed to see that free beer trumps free speech among developers.\"\n\nI wouldn't phrase it as apocalyptically as you have. I would be a little more realistic, but only as realistic as RMS when he came up with the LGPL -- proprietary software is a reality, and any proprietary company (which is 99% of them) considering a OSS platform want to have the choice to build proprietary add-ons.\n\nIt all seems very silly when you look at it and consider the amount of proprietary GNOME or KDE apps are miniscule compared to the Free versions. LGPL seems to be more of an emotional comfort to big businesses than a real necessity.\n\nEither way the results shouldnt be much of a surprise to anyone.\n\nCheers,\nRyan"
    author: "Ryan"
  - subject: "Re: more GPL v. LGPL wars? To clarify ..."
    date: 2004-03-02
    body: "\"any proprietary company [...] want to have the choice to build proprietary add-ons.\"\n\nMy only point is that while \"we\" dont much care for a proprietary company's choice to make closed software, *they* certainly will choose the license that gives them the most \"freedom\", so the choice of LGPL is pretty obvious."
    author: "Ryan"
  - subject: "Re: more GPL v. LGPL wars? To clarify ..."
    date: 2004-03-03
    body: "A business who wants to develop proprietary software do not choose a license.\nThey choose a development framework/platform, looking at all facts regarding that framework."
    author: "OI"
  - subject: "Re: more GPL v. LGPL wars? To clarify ..."
    date: 2004-03-03
    body: "\"They choose a development framework/platform, looking at all facts regarding that framework.\"\n\nYes they certainly do. Although the LGPL will certainly be a useful (certainly where it is not feasible to pay for development in a minority of cases), and in some cases, a necessary license (getting GPL'd and proprietary software to work together) it is not something that people are going to look exclusively at.\n\nBesides, the LGPL solves nothing. It certainly allows developers and proprietary software businesses to develop software for free, in a monetary sense, but the end-user/business/organisation is still lumped with the license that the proprietary developers choose for their software. When you look at it like this the intended use that some people have in mind for the LGPL just does not make any sense at all."
    author: "David"
  - subject: "Funny thing about SWT/Qt"
    date: 2004-03-02
    body: "is that it was already _written_ several years ago. It however, still has not released yet. \n\nThis is of course, because SWT and Eclipse are CPL, which is unfortunately GPL incompatible. \n\nThe astonishing growth of Eclipse isn't completely unexpected.. it's :\n\n- cross platform\n- is probably the best Java IDE\n\nI do think, however, that the brand new version of kdevelop (3.0) can make waves. It is significantly improved over the previous versions of kdevelop. Language independence is an extremely important key aspect in which kdevelop is a lot mature than Eclipse. "
    author: "anon"
  - subject: "Re: Funny thing about SWT/Qt"
    date: 2004-03-03
    body: "How can this be?  I mean, GTK is GPL, and yet the existing SWT implementation is based on GTK."
    author: "Anonymous"
  - subject: "Re: Funny thing about SWT/Qt"
    date: 2004-03-03
    body: "GTK is not GPL."
    author: "anon"
  - subject: "Re: Funny thing about SWT/Qt"
    date: 2004-03-03
    body: "GTK is LGPL, not GPL"
    author: "anon"
  - subject: "Re: Funny thing about SWT/Qt"
    date: 2004-03-03
    body: "> This is of course, because SWT and Eclipse are CPL, which is unfortunately GPL incompatible\n\nI guess this is a convenient excuse.\nSTW/Qt would mostly be interesting on X11 systems and I don't think CPL is incompatible with the QPL.\n\nHowever it doesn't sound very nice if the CPL really contains sections that prohibit using it in GPL code.\nWhy would IBM want to prohibit GPLed SWT applications?\n\n\n"
    author: "Kevin Krammer"
  - subject: "Re: Funny thing about SWT/Qt"
    date: 2004-03-03
    body: "Since nobody really uses SWT in any meaningful and widespread way yet, I guess this is irrelevant."
    author: "David"
  - subject: "Re: Funny thing about SWT/Qt"
    date: 2004-03-10
    body: "We just ditched Swing and are migrating our Java apps full speed to SWT.  It's native, small and fast.  Forget about Eclipse and JFace, SWT is the one!"
    author: "z"
  - subject: "Re: Funny thing about SWT/Qt"
    date: 2004-04-29
    body: "SWT is still larger than Swing though, in all cases where Swing is already installed (which is the majority.)  If SWT were distributed with Java (which would actually be a bloody good idea), it would be a different story."
    author: "Trejkaz Xaoza"
  - subject: "Re: Funny thing about SWT/Qt"
    date: 2004-03-06
    body: "There's nothing that stops a GPL application using SWT, but as far as I know the license of free Qt conflicts with the license of SWT, so SWT can't use free Qt as the peer toolkit. So it's not CPL preventing it, it's QPL."
    author: "asmodeus"
  - subject: "Re: Funny thing about SWT/Qt"
    date: 2004-03-06
    body: "If the CPL does not prohibit useage in a GPL project, then its IMHO clear that a GPLed SWT implementation based on Qt would be possible, even if a QPLed version wouldn't (which I highly doubt).\n\nI guess that a licensee of Qt commerical could even release a multi licenced SWT/Qt.\n\nI think the licence is just an excuse to avoid making Qt more popular, at least it looks like that."
    author: "Kevin Krammer"
  - subject: "Who are these developers?"
    date: 2004-03-02
    body: "This study or a similar one comes out every year and I still don't know what it's based on. Are these professional \"Linux developers\" or just anyone who heard about the survey and decided to hold forth?"
    author: "Otter"
  - subject: "Re: Who are these developers?"
    date: 2004-03-02
    body: "They're professional developers from every major (and probably minor) category, ISVs, VARs, etc., and they represent everything from tiny to huge companies. \n\nThey must use or target Linux at least some of the time in order to be included in the survey. "
    author: "Nicholas Petreley"
  - subject: "Re: Who are these developers?"
    date: 2004-03-03
    body: "Well, I get their surveys.\n\nUsually there\u00b4s at least an average of two questions that can\u00b4t be answered logically, and I haven\u00b4t released a program in about 4 years. So take it with a grain of salt ;-)"
    author: "Roberto Alsina"
  - subject: "Debuggers"
    date: 2004-03-02
    body: "hope to see juicy details in next week's computerworld. \n\nI can see why developers are not satisfied with current debuggers. Most of them use gdb, which is incredibly powerful, but can be extremely unintuative at times. The alternatives thus far have been ddd and insight (which both have archaic interfaces), and kdbg (which does have as much flexibility as ddd and insight)\n\nHowever, the brand new version of kdbg (1.9.x) is a lot better. I hope to see both it and kdevelop's internal debugger improve. I want kdbg to use kate for example.\n\nOther KDE tools are top notch however. Qt Designer and Kdevelop is an elegent top-down solution for software development, and valgrind with kcachegrind is competitive with $3,000 offerings from Rational (er... IBM)"
    author: "John"
  - subject: "Re: Debuggers"
    date: 2004-03-03
    body: "kdbg and kdevelop's debugger are both graphical wrappers around good old gdb."
    author: "anon"
  - subject: "Re: Debuggers"
    date: 2004-03-03
    body: "well, duh.. that doesn't mean you can't build a interface around it that non-UNIX programmers can understand! :)"
    author: "anon"
  - subject: "Re: Debuggers"
    date: 2004-03-03
    body: "but if your using gdb you _are_ a unix programmer. GDB is actually pretty simple to use _if_ you read the documentation. Anyone who isnt capabable of that certainly isnt capable of producing good software."
    author: "Tom"
  - subject: "Re: Debuggers"
    date: 2004-03-03
    body: "But should reading the documentation really be needed?  Even if you're capable, it feels kind of pointless if you just can switch to a different debugger that you don't need a handbook to use."
    author: "Anonymous"
  - subject: "Re: Debuggers"
    date: 2004-03-03
    body: "Don't be silly.\n\ngdb is easy enough to learn, I suppose, but it's still easier to\n\n1. Step through code by hitting a single function key.\n2. See the current line of code in context within the source module.\n3. Print out variables by clicking on them.\n4. Watch a list of variables.\n\nWe have a shop full of unix programmers woring on AIX.  They use dbx because it's there, but they don't like it.  I personally am okay with dbx, and when I code on Linux (we're in the process of porting from AIX to Linux), I find gdb to be comparable, but even less intuitive.\n\nBelieve it or not, one of the reasons we want to switch from AIX to Linux is to get access to more up-to-date programming tools like kdbg.  Kdbg may not be as powerful as gdb, but our programmers are itching for something like it.  I hear it's possible to get kdbg up on an AIX box, but I don't trust it to work reliably in that environment (and our dev machine is too low-end to run X apps well anyway).  I did bring up ddd on that box, and found it less useable than dbx.  One of our programmers swears by IBM's idebug.\n\nI do wish that kdbg would give you some way to get at the gdb command line.  Or better yet, provide some kind of macro facility to allow you to click on a variable and pass it to a function.  I make a lot of use of the dbx 'call' command (I assume gdb has something similar).  For example, we have a binary 'record' format that can't be understood by simply dumping out the data.  I've provided a library function to print it, which is linked into all apps.  I then set up an alias command in .dbxinit to allow you to call that function to print out these records within the debugger.  It's really handy.  Some of these functions are even interactive (prompting the user for input).  Very powerful.\n\nWhile I'm venting my wishlist, it'd be really nice for kdevelop to be able to work from existing makefiles instead of insisting you use their automake (or whatever) support.  We've got tons of these already set up (and I believe we may even be incompatible with automake - have our own config.h for platform dependencies).  I wouldn't expect kdevelop to be able to generate the makefiles, but it'd be nice if it'd let you edit source files, rerun make and debug..."
    author: "Rob"
  - subject: "Re: Debuggers"
    date: 2004-03-03
    body: "Well, you _can_ import custom projects into KDevelop 3.0 and work on a non-automake basis. It does not yet provide all of the functionality their automake manager does but it is reported to work after all.\nMoreover, this probably will change with upcoming versions as a a rewrite of this part is on schedule not far from now."
    author: "Bippo"
  - subject: "SCO"
    date: 2004-03-02
    body: "Sco has no case. On the German market I am free from SCO but I think it would be very fruitful to call for a criminal investigation into the SCO group in other parts of the world. A criminal charge has to be filed against SCO as it is probably fraud. One year ago I thought SCO might have a case but the sort of media campaign made this very unlikely to me. A state attorney is an independent source that can determine whether SCo's license policy is fraud. We just have to report the offence to the police and the judicial body will care about the criminal charge. This shall be done in states where SCO sells it doubtful licenses on the market. So perhaps this may be a way to hold someone at SCO responsible. \n\nA copyright infringement is a simple task in business and is usually silently resolved, no big deal. But the kind of communication of SCO and the baseless accusations were infringing upon almost every single business rule. In Germany(tarent vs. SCO) they were prohibited because of competition law. Accusations without proof are dirty business and have to be stopped."
    author: "Bert"
  - subject: "Powerful Technologies"
    date: 2004-03-02
    body: "As a developer (not on Linux however; at an ISV that writes custom payroll solutions, on overwhelmingly Windows-based targets), I think that nearly all things in terms of needed development platforms on Linux platforms are already *there* (cross platform development tools such as Eclipse and world class component APIs such as kparts are extremely important). \n\nWhat is holding a massive amount of VARs and ISVs from Linux development isn't the lack of tools, but rather potential users. I think this is slowly changing, thanks to the positive contributions of OSS projects like KDE. "
    author: "Sam Rozier"
  - subject: "Re: Powerful Technologies"
    date: 2004-03-03
    body: "You are right on one level that if there were more users then vendors would write software. \nHowever I would be releasing KDE utility software today regardless of the market size if there was a cross-platform tool at a reasonable price. Kylix looked like this, but it seems to have gone into limbo.\nEven if there was an easy to use KDE only tool at a price that is appropriate for the free utilities that we supply to support our products, we would write them because we could.\n\nDelphi Professional is $1000 for a complete and very productive programming environment.\nQt is ~$4000 just for a toolkit if you want cross-platform. Given that we have 0 KDE customers (all our paying Linux customers are embedded, and do thier own thing), this is just far too expensive for my market niche. \nSAP or BMW mightn't blink at QT's price, and a company writing a custom payroll won't either, but for the small vendors its very steep to get tools which are not up to Windows standard for ease of use, productivity and completeness. \nNote that only 5% of our website vistors (who are very technical) are using Linux desktops. This applies particularly to hardware vendors (like us)for whom the software is all cost, and no income.\nA look at the windows world wll show you that a lot of its strength comes from the small shareware vendors and the like. It is impossible for them to write for KDE (except using BlackAdder).\n\nA reasonable price QT license is urgently needed for KDE. \nI suggest that someone in KDE tries to negotiate a $200-$500 professional version of KDevelop with a QT license, like what TheKompany have done for BlackAdder. This might bring some cash in for development too.\n\nOtherwise shareware, specialist, hardware and small ISV's will continue to go with GTK,wxWindows, and Java as 95% are doing at present. \nKDE will be the desktop, but none of the apps will be native.\n\nYou might ask: Who cares if commercial vendors ignore KDE, we'll just write it ourselves. However a close look will show you that most KDE applications are small and uncomplicated. There are very few deep and complicated KDE apps that work. MP3 players and IM apps are icing, not the cake.\nMatlab, Gimp, Abiword, Scilab, Dataexplorer and others are GTK or Motif or Java or anything but KDE.\n\n\n"
    author: "Simon"
  - subject: "Re: Powerful Technologies"
    date: 2004-03-03
    body: "I think this is approaching the nub of the issue .... The long term question is, will KDE or Gnome become the predominant desktop.\n\nThe arguments about the license costs of Qt are largely irrelevant. Its free if you are non-commercial and if you are a company then it's small compared to all your other costs, especially when you factor in programmer costs vis-a-vis productivity (I believe that this both for \"developed\" countries like the UK, US, Australia, Japan and \"developing\" countries like India, those in South America) (please, no quibbles here, you get the idea...). Currently the \"desktop battle\" is in a state of flux, hence the majority if ISVs going Qt.\n\nBut, the crunch question is not what the ISVs prefer, it is what platforms they are going to have to write to. If Gnome is favoured by the major distributions then the ISVs will write apps on Gnome/GTK and not KDE/Qt, whether they like it or not. With SUN's \"java\" desktop and RedHat both favouring Gnome, ditto \"UserLinux\" (if it gets anywhere) and with SuSE and Ximian now owned by Novell, I think that there is a real chance that KDE will get marginalised. And, having written both Qt/KDE and GTK code, even allowing for the fact that I'm much less experienced with the latter, I see that as a real shame, because Qt/KDE is just *so* superior.\n\nMy gut feel is that TT should seriously look at changing the license for Qt, at least under X11, either to grossly reduce the license cost or even LGPL it. My fear is that if Gnome/GTK is ascedant then at a commercial level they will be squeezed out of the desktop market, in which they will loose that part of the income stream anyway. And, since most people will start GUI programming in a \"desktop\" environment, if they start with Gnome/Gtk then later on, as they move up the IT ladder, there is a danger that Qt will be squeezed in other areas.\n\n\nRegards\nMike"
    author: "miketa"
  - subject: "Re: Powerful Technologies"
    date: 2004-03-03
    body: "\"Otherwise shareware, specialist, hardware and small ISV's will continue to go with GTK,wxWindows, and Java as 95% are doing at present. KDE will be the desktop, but none of the apps will be native.\"\n\nI thought you said there wasn't a market for Linux desktops? If there isn't the need for large companies, there certainly isn't the need for small ISVs and shareware developers. The vast majority of desktop applications developed are for Windows, maybe a few for Macs, and no one uses GTK, wxWindows (wxWidgets) or even Java here.\n\n\"If Gnome is favoured by the major distributions then the ISVs will write apps on Gnome/GTK and not KDE/Qt, whether they like it or not. With SUN's \"java\" desktop and RedHat both favouring Gnome, ditto \"UserLinux\" (if it gets anywhere) and with SuSE and Ximian now owned by Novell,\"\n\nEvery major distribution ships KDE, and where there is a total focus on one desktop, those distributions exclusively use KDE. You will not find any distribution that exclusively ships Gnome. The JDS does, but it isn't popular, despite the hype (nor does Sun have a direction for it), and Ximian Desktop at the moment is an orphan. Red Hat's desktop strategy has been a failure, which is why they pulled it. Red Hat still ships KDE, and they have a hybrid Blue Curve desktop.\n\n\"I think that there is a real chance that KDE will get marginalised.\"\n\nSorry, but people have been making these comments for years and the situation has not changed. There always seems to be a 'real chance'.\n\n\"But, the crunch question is not what the ISVs prefer, it is what platforms they are going to have to write to.\"\n\nAt the moment Linux desktops are just not on ISVs' radar. Most are probably not familiar with Linux desktops beyond having heard of them. People who are not realistic about this will doom desktop Linux to failure.\n\n\"My gut feel is that TT should seriously look at changing the license for Qt, at least under X11, either to grossly reduce the license cost or even LGPL it. My fear is that if Gnome/GTK is ascedant then at a commercial level they will be squeezed out of the desktop market, in which they will loose that part of the income stream anyway. And, since most people will start GUI programming in a \"desktop\" environment, if they start with Gnome/Gtk then later on, as they move up the IT ladder, there is a danger that Qt will be squeezed in other areas.\"\n\nI see a lot of these comments, but people forget one thing when they make them. > 95% of the world does not use Linux desktops, either KDE or Gnome. The business world is as close to 100% desktop Microsoft as you can get, so I laugh at people who talk about corporate Linux desktops and 'corporate focus'. Certain people want to talk about business desktops - there it is. If GTK and the Gnome programming technologies are inferior (certainly to Windows) then no ISV will ever contemplate using them, because they will use the extensive and good development tools available for Windows first and foremost. If there is a first-rate cross-platform alternative, like Qt, then they *might*.\n\nWe also inevitably get the VHS vs Betamax comments, where KDE is superior (admitted by practically everyone) on the Betamax side, and Gnome is VHS. I'm sorry, but Windows is VHS and everyone else is Betamax, and this happened a long time ago. If a Linux desktop is not on a par, or better, than Windows no one will touch it.\n\nIt all just shows the lack of realism that many people have concerning the future of desktop Linux and free desktops in general I'm afraid."
    author: "David"
  - subject: "Re: Powerful Technologies"
    date: 2004-03-03
    body: "My prediction is that not too far in the future you will get the \"goddies\" for GTK/Gnome in proprietary extensions to libglib, libgtk, and libgnome and not in the LGPL-base. Why would a member of the gnome foundation give an advatage to its hated competitors?\n\nATK already is such an extension, although LGPL at the moment, copyrighted by Sun MS. In the Qt universe you just dont have this problem (remember X/Motif?).\n\nAnother note about a LGPL Qt. IT ALREADY EXISTS! just look in the WebCore code written by Apple (in the kwq subdir). Everyone is free to implement Qt. Its not patented like .Net, and its open source. So its not like TT is the ultimate power in the Qt universe."
    author: "marc"
  - subject: "Re: Powerful Technologies"
    date: 2004-03-03
    body: "\"My prediction is that not too far in the future you will get the \"goddies\" for GTK/Gnome in proprietary extensions to libglib, libgtk, and libgnome and not in the LGPL-base. Why would a member of the gnome foundation give an advatage to its hated competitors?\"\n\nYou may be right there. Hadn't thought of that."
    author: "David"
  - subject: "Re: Powerful Technologies"
    date: 2004-03-03
    body: "Thank you very much for your interesting and enlightening comment. This is the best comment on the KDE vs. GNOME issue I read for a long time.\n\nKDE will go its way, no matter what some people say. If people want KDE to go into a different direction, they should contribute instead of moaning."
    author: "Michael Thaler"
  - subject: "Re: Powerful Technologies"
    date: 2004-03-03
    body: "> We also inevitably get the VHS vs Betamax comments, where KDE is superior (admitted by practically everyone) on the Betamax side, and Gnome is VHS. I'm sorry, but Windows is VHS and everyone else is Betamax, and this happened a long time ago. If a Linux desktop is not on a par, or better, than Windows no one will touch it.\n\n> It all just shows the lack of realism that many people have concerning the future of desktop Linux and free desktops in general I'm afraid.\n\nwell said. "
    author: "acdc"
  - subject: "Re: Powerful Technologies"
    date: 2004-03-03
    body: "I am a sucker for this kind of thing.\n\nVHS was better. Why? because you could see a whole movie in a VHS tape.\n\nSure, maybe Beta had somewhat better image quality. But not good enough to make people change tapes in the middle of a film.\n\nAlso, Beta was closed, VHS was the more open standard.\n\nAnd before someone says it: No, the Betamax that was in use until recently in broadcast and production is **not** the same as old Beta."
    author: "Roberto Alsina"
  - subject: "Re: Powerful Technologies"
    date: 2004-03-03
    body: "The thing is, if you compare technologies, GNOME is VHS, and Windows is Betamax. With the exception of some things (GTK+'s layout engine, for example, Mono, potentially) GNOME is a pretty traditional platform. Even when the technologies exist (Bonobo, GNOME-VFS), very few applications actually use them. At a technical level, KDE is really the only one that matches up to Windows. You've got KParts to replace COM/OLE, you've got DCOP-scripting to replace Windows Scripting Host, you've got the broad, integrated KDE API to replace the messy but very featureful Win32 API, and you've got things like KIO that really have no comparison on Windows. \n\nAs for the Betamax vs VHS thing being a long time ago --- choosing inferior technologies is a historical curse of the computer industry. Its not just a single isolated case, but happens continually. Consider PC hardware vs Mac hardware. Windows 95 vs MacOS. D3D vs OpenGL. Go back further in time and consider the failure of NeWS, Smalltalk, Lisp, etc. I'm not saying that superior technologies never win, I'm just saying that, historically, the odds are not comforting. Now, KDE is different in a way, because KDE is open source and thus not subject to the same market considerations as these commercial products, but it's still something to be wary of. After all, those who don't understand history are doomed to repeat it, right?\n"
    author: "Rayiner Hashem"
  - subject: "Re: Powerful Technologies"
    date: 2004-03-03
    body: "\"My gut feel is that TT should seriously look at changing the license for Qt, at least under X11, either to grossly reduce the license cost or even LGPL it.\"\n\nWhen will this stupid licensing bitch go away? I am of the firm belief that no matter what Trolltech does, Qt will always be the evil toolkit. Consider its licensing history. The KDE Free Qt Foundation wasn't good enough. The Q Public license wasn't good enough. Qt being released under the GPL wasn't good enough until Trolltech got forgiveness from everyone it wronged. Upon discovery that Trolltech didn't wrong anyone, the argument suddenly turns upon the GPL itself. Then there were the occasional non-license evidence that Qt was evil incarnate: it uses C++, uses a metacompiler, didn't use the STL, etc."
    author: "David Johnson"
  - subject: "Re: Powerful Technologies"
    date: 2004-03-03
    body: "> When will this stupid licensing bitch go away?\n\nThat's a good question. I saw a value of $4000 but I seem to recall $1500. At any rate whether it's single or multiple platform there's still breaks for volume licensing and funny thing, it's still selling. BTW .NET developer network is $4000 per year last I checked. I think in the US you can safely estimate $50K-$60K for software engineers. I'm sure it varies but the bottom line is that it's expensive to have a room full of them. Those costs dont' reflect probably 30%-40% additional payroll expenses and other overhead in the cost of doing business. Development cycles are at least 18 months and usually longer. So the bottom line is if that if you have a market and the toolkit saves you 1% or more in your time it's worth it. Period.\n\nAgain, the sloccount numbers from the kdewebdev module as fo today...\nTotals grouped by language (dominant language first):\ncpp:         136235 (85.06%)\nsh:           10317 (6.44%)\nansic:        10316 (6.44%)\nperl:          3295 (2.06%)\n\n\n\n\nTotal Physical Source Lines of Code (SLOC)                = 160,163\nDevelopment Effort Estimate, Person-Years (Person-Months) = 41.29 (495.45)\n (Basic COCOMO model, Person-Months = 2.4 * (KSLOC**1.05))\nSchedule Estimate, Years (Months)                         = 2.20 (26.43)\n (Basic COCOMO model, Months = 2.5 * (person-months**0.38))\nEstimated Average Number of Developers (Effort/Schedule)  = 18.75\nTotal Estimated Cost to Develop                           = $ 5,577,402\n (average salary = $56,286/year, overhead = 2.40).\nSLOCCount is Open Source Software/Free Software, licensed under the FSF GPL.\nPlease credit this data as \"generated using David A. Wheeler's 'SLOCCount'.\"\n\nTell me again that Qt costs too much, this time with a straight face."
    author: "Eric Laffoon"
  - subject: "Re: Powerful Technologies"
    date: 2004-03-03
    body: ">  This applies particularly to hardware vendors (like us)for whom the software is all cost, and no income.\n\nI'm asking this without knowing at all what you do, and if it would apply, just to ask. What prevents you from GPL'ing your stuff? Obviously the value isn't in the software.\n\nThe reason I ask is simply this: why should I count on a binary from someone that may very well not work a year from now on my boxes? Especially with the desktop, things are changing very fast. QT4 is on the horizon, which means binary incompatible. This is a reality, some say a feature, of FOSS development.\n\nI think the RAD category of software was partly motivated by the thought that one could finish your project with the possibility of the vendor still being in business.\n\nI looked at Delphi. I have used it on Windows, and it is an impressive product. The problem was that the users would have to essentially maintain a parallel universe to run the software. With no possibility of anything except at the will and mercy of Borland. Obviously I wasn't the only one who thought the same way, since Borland has essentially orphaned it.\n\nIf I want a secure, long lasting environment, I have two choices. Microsoft, or a FOSS project that has been around for a while, with an active community and a few well established commercial participants.\n\nAs for the price for a QT license, that is up to them. If they are losing market due to the high price, I suspect they'll drop it. Unfortunately, around here you have around 250 or so active developers who have contributed far more value to the project than $4000, so they may very well not have sympathy for those who are willing to ride the train, but unwilling to pay the ticket price. Think libre, not gratis.\n\nDerek\n\n"
    author: "Derek Kite"
  - subject: "Re: Powerful Technologies"
    date: 2004-03-03
    body: "Orphanned delphi? Come on now. There is a very strong and active community around delphi, and borland continues to see this, and continues to release this top notch IDE. They have just released version 8 which integrates with all of microsofts newfangled .net garbage because there was developer demand. I agree that it is unfortunate that Kylix seems to have gone by the way side, but lack of demand seems to be the main reason. It was technologically a good product. Desktop linux is just not as popular in the sectors where delphi is most prominent. Although, if you look at the delphi component market, you will see that many many of the independent providers have done the work to ensure their products work both with delphi and kylix.\n\nAs far as 'maintaining a parallel universe to run the product', that is also an absurd comment. As a delphi developer for the last 6 years, and building large complex applications database applications that integrate with postgresql and oracle, I can tell you that delphi applications can run on a bare bones installation of any version of windows with the appropriate database drivers (in my case ADO).  Can you say the same for kde or qt apps? Can I take a qt app built with kde 3.x, and run it on ANY version of kde? Most certainly not.\n\n\nI respect your comments Derek, but in this case I think you have unfortunatley spoken prior to having appropriate knowledge. Looking at delphi does not consitute knowing anything about it. "
    author: "Tom Hanks"
  - subject: "Re: Powerful Technologies"
    date: 2004-03-03
    body: "> Orphanned delphi?\n\nOrphanned Kylix seems to be true though..."
    author: "ac"
  - subject: "Re: Powerful Technologies"
    date: 2004-03-03
    body: "But he said orphanned delphi :(\n\nI have heard rumors that future versions of delphi will run on both windows and linux. Not sure how much merit there is in those though. One can hope I guess :)\n\n\ntom\n"
    author: "tom hanks"
  - subject: "Re: Powerful Technologies"
    date: 2004-03-04
    body: "I meant kylix. I tried delphi and liked it. I didn't like kylix, it didn't fit well into my linux system.\n\nKylix is the linux version of delphi. I'm not quite sure of the differences, since it's been a while. Borland's foray into the linux marketplace will probably end up being an object lesson on how not to do it. Unfortunately.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Powerful Technologies"
    date: 2004-03-06
    body: "> but lack of demand seems to be the main reason\n\nI think the lack of a good migration path from VCL to CLX was the main reason.\nOr why would Borland not be able to release their own IDE as a native application but had to use winelib?\n\n> It was technologically a good product\n\nI think it isn't.\nIf you look at programmers questions in newsgroups and webforums, developing with Kylix seems to be more difficult than Delphi programmers expected.\nAdditonally CLX is designed around 1980's technology, for something like rotating text one has to access the underlying Qt API or on Windows fall back to hacks like loading a vertical font.\n\n> Can I take a qt app built with kde 3.x, and run it on ANY version of kde? Most certainly not.\n \nSure, Kylix apps wouldn't work on recent KDE if it weren't possible, as Kylix uses Qt2 and current KDE installations use Qt3\n"
    author: "Kevin Krammer"
  - subject: "Re: Powerful Technologies"
    date: 2004-03-04
    body: "And how much is delphi if you want cross-platform? Or if you want just the toolkit, cross-platform?\n\nYou are comparing very different things here."
    author: "Roberto Alsina"
  - subject: "Re: Powerful Technologies"
    date: 2004-03-05
    body: "Delphi and Kylix-Pascal came together for the one price. Thats exactly what I wanted. \n\nI don't think that Borland on Linux is dead, to me they appear to have shifted all resources to .NET so they don't let the wife walk out, while fiddling with a mistress. The offical word was that there won't be any Kylix releases this year. But who knows what they will go with when the return to Linux, probably they don't "
    author: "Simon"
  - subject: "Re: Powerful Technologies"
    date: 2004-03-05
    body: "Then it makes no sense to compare it against Qt's multiplatform price.\n\nYou could, for example, compare it to BlackAdder for Windows.\n\nSure, Kylix may be better. It's also more expensive. Or you could compare it to Kdevelop+Designer+Qt for X11. Then the cost is pretty much the same.\n\nBut you compared a 3-platform toolkit to a single-platform toolkit+IDE. Not comparable, really, unless there is absolutely no other alternative. But there is."
    author: "Roberto Alsina"
  - subject: "Sun chose GNOME for different reasons"
    date: 2004-03-03
    body: "IIRC, Sun never said they chose GNOME because of the LGPL. This was way before the GPL/LGPL issue ever came up. At the time, I seem to remember the rationale being:\n\n- Sun developers more familiar with C. Add to that the fact that KDE wouldn't compile on their C++ compilers until recently.\n\n- Sun liked GNOME's approach of using standard technologies (eg. CORBA) instead of rolling their own (eg. DCOP). We know how the CORBA thing turned out, but the Sun deal was set up before that.\n"
    author: "Rayiner Hashem"
  - subject: "Re: Sun chose GNOME for different reasons"
    date: 2004-03-03
    body: "Those reasons are absolutely bogus and make no sense.  The license one does though. This is why it is crucial for KDE to continue its efforts to integrate GTK into the environment.\n\nBesides, haven't you noticed Sun aren't interested in GNOME technologies in the *least*.  This is why their system is called Java Desktop System.  They want their developers to use Java not inferior stuff like GTK.  The fact that the native system is in fact GNOME is a dirty little secret but GNOME is just acting as a shell."
    author: "ac"
  - subject: "Re: Sun chose GNOME for different reasons"
    date: 2004-03-03
    body: "JDS being based on GNOME is certainly not a dirty little secret. Sun have been more than willing to give credit where credit is due - moreso than some of the other GNOME contributing companies, in fact - in all of their press and announcements. It is called the \"Java Desktop System\" for Sun branding purposes, not for any sneaky way of hiding the fact that it's based on GNOME."
    author: "Boggle"
  - subject: "Re: Sun chose GNOME for different reasons"
    date: 2004-03-03
    body: "You're not being objective about this.  Clearly Sun is pushing this as Java.  If they wanted a \"branded\" GNOME they could have called it Sun Desktop System not something deliberately misleading and false like JDS.\n\nIt is clear that the focus here is Java.  GNOME is acting as nothing but a shell for the Java platform."
    author: "ac"
  - subject: "Re: Sun chose GNOME for different reasons"
    date: 2004-03-03
    body: "Yeah, it should get interesting when Mono maturizes and much of GNOME starts to get reimplemented in it. That just screams conflict with Sun's Java. \n\n"
    author: "anon"
  - subject: "Re: Sun chose GNOME for different reasons"
    date: 2004-03-03
    body: "Java Server, Java Enterprise, Java Desktop... Of course Sun wants to have a coherent branding strategy. Does that mean they will be pushing GNOME as a development platform? Unlikely, they already have their own. Does it mean they'll be pushing GNOME as their enterprise desktop of choice? Absolutely. Are these goals in opposition? Not as much as you'd think, and they can actually be fairly cooperative goals."
    author: "Boggle"
  - subject: "Re: Sun chose GNOME for different reasons"
    date: 2004-03-03
    body: "The C/C++ argument was absolutely sensible for Sun. It may not make sense to you, but it did for them. Integrating GTK+ and KDE is pretty much a fool's errand, because no one's going to ship frankensteinian hacks such as those anyway. It's a fun experiment for random hackers, but all that we've seen so far are essentially unshippable."
    author: "Boggle"
  - subject: "Re: Sun chose GNOME for different reasons"
    date: 2004-03-03
    body: "most sodipodi builds i've seen come with the kde file selector support"
    author: "anon"
  - subject: "Re: Sun chose GNOME for different reasons"
    date: 2004-03-03
    body: "ArkLinux ships the GTK-Qt style as default."
    author: "ac"
  - subject: "Re: Sun chose GNOME for different reasons"
    date: 2004-03-04
    body: "\"The C/C++ argument was absolutely sensible for Sun.\"\nFor sure StarOffice is C++ and it wasn't a porblem for Sun here."
    author: "anno"
  - subject: "Re: Sun chose GNOME for different reasons"
    date: 2004-03-04
    body: "Sun also bought the programmers :-)"
    author: "Roberto Alsina"
  - subject: "Re: Sun chose GNOME for different reasons"
    date: 2004-03-05
    body: "Another big issue was that the C++ ABI still isn't stable across compilers or even different versions of the same compiler.  All of Sun's standard libraries are built with their compiler, but since the cost is rather high many users use GCC.\n\nThis is all fine and well with C since the C ABI has been fixed for many moons, but mixing C++ libraries from different compilers causes major problems."
    author: "Scott Wheeler"
  - subject: "Re: Sun chose GNOME for different reasons"
    date: 2004-03-03
    body: "Yes. Completely bogus. I just made those up while I was high on special brownies!\n\nYesh. This information came out in 2000, back when GNOME was pre-2.0, and Eazel was still in business. Please read the relevent press-releases from the period. The LGPL was never mentioned at the time. \n\nAnd the comment that \"GNOME is just acting as a shell\" is *completely* off-base. Its well known that the \"Java\" in \"Java Desktop System\" is nearly absent. The underlying desktop is GNOME, nearly all the programs are GNOME programs, and the integration efforts (Java GTK+ theme, OpenOffice GTK+ framework) are being done to integrate into GNOME.\n\n"
    author: "Rayiner Hashem"
  - subject: "Re: Sun chose GNOME for different reasons"
    date: 2004-03-03
    body: "Saying that \"Sun developers more familiar with C\" is complete bullshit when you consider that not only is Sun the inventor of Java but that GTK C is an aesthetically highly-offensive usage of C that completely obliterates the \"C++ is more complex\" argument.\n\nSun developers are no dumbasses and if they can learn GTK C, they can learn Qt C++ and be several times more effective.  The reason is just complete bullshit.\n\n> Its well known that the \"Java\" in \"Java Desktop System\"\n\nYou don't get it.  Sun named it Java Desktop System for a reason.  To distance themselves from the GNOME platform and promote the Java platform.  Yes, it's well-known to *you*.  This is not the point.  The point is that Java is the supported and recommended platform for 3rd party developers. NOT GNOME.  Sun themselves only used GNOME because they were unable to deploy a real Java Desktop System.  \n\nThis is their dirty little secret."
    author: "anon"
  - subject: "Re: Sun chose GNOME for different reasons"
    date: 2004-03-03
    body: "> Saying that \"Sun developers more familiar with C\" is complete bullshit when you consider that not only is Sun the inventor of Java but that GTK C is an aesthetically highly-offensive usage of C that completely obliterates the \"C++ is more complex\" argument.\n\nIt's not about the argument; but that's what Sun's reasoning was; however flawed it was. \n\n> Sun the inventor of Java but that GTK C is an aesthetically highly-offensive usage of C that completely obliterates the \"C++ is more complex\" argument.\n\nYeah, but things like Java are written in C, not C++. \n\nLet's see:\nSolaris - C\nJava - C\nMany java tools - Java\nOpenOffice - C++ (not a large part of Sun at that time)\nnearly everything else - C\n\n> You don't get it. Sun named it Java Desktop System for a reason. \n\nYeah, the reason is that Java has a large amount of brand power which GNOME doesn't have. Java is number one programming language in the enterprise and their usage of that word undoubtably helps them. "
    author: "anon"
  - subject: "Re: Sun chose GNOME for different reasons"
    date: 2004-03-03
    body: "A) The people working in the Java group are not the same people, with the same type of skills, that are working in the OS group. Those people were used to working on CDE. Its a very reasonable to expect GNOME, because of its C basis, to be an easier transition for them.\n\nB) KDE wouldn't compile on Sun compilers until recently. Since GCC doesn't share the same ABI, Sun's Forte C++ development tools couldn't be used to develop KDE applications either. \n\nC) Sun might have named it Java Desktop System, but that's just marketing. They are not trying to distance themselves from GNOME. Just look at Sun's JDS page:\nhttp://wwws.sun.com/software/javadesktopsystem/index.html\nGNOME is mentioned numerous times, usually with top-billing. GNOME-based applications and GNOME-integrated applications are mentioned far more often than Java ones. The only references to \"Java\" in the main \"features and benefits\" page is one near the bottom about the \"Java media Player.\" Then look at Sun's GNOME page:\nhttp://wwws.sun.com/software/star/gnome/faq/generalfaq.html#4q0\nAnd look at how much Sun play's up GNOME accessibility support, use of CORBA, etc.\n\nI think you're just plain paranoid, going on about their \"dirty little secret.\"\n\n\n"
    author: "Rayiner Hashem"
  - subject: "Re: Sun chose GNOME for different reasons"
    date: 2004-03-03
    body: "I thought that Sun's sponsoring of SCO was its dirty little secret ;-)"
    author: "Waldo Bastian"
  - subject: "Re: Sun chose GNOME for different reasons"
    date: 2004-03-03
    body: "This is Sun. They are big enough to have many dirty little secrets :)"
    author: "Dawnrider"
  - subject: "Re: Sun chose GNOME for different reasons"
    date: 2004-03-03
    body: "A)  If Sun's developers can learn GTK C but can't cope with Qt C++, they should be fired for being worthless dumbasses.  Any developer worth his salt should be able to pick up a new language.\n\nB) Please, don't make me laugh.  So KDE didn't compile on Sun's compilers?  FIX IT.  Chances are this is a near trivial issue compared to everything else.  For one, I'm sure Qt already compiled under Sun Forte and for two, Sun's non-dumbass developers could have easily jumped in and help deal with whatever issue prevented compilation.\n\nI really think you don't have an understanding of the scope of these so-called issues you bring up in A and B.  Any developer worth his salt should be able to deal with them, especially with the backing of a company as serious as Sun.\n\nC) Exactly proves my point.  \"It's also the only environment with fully integrated Java technology, making this \"out-of-the-box\" desktop ready to run thousands of Java technology-based applications with a consistent look and feel.\"  \n\n\"Java technology plays a very important role in the Java Desktop System - the Java Virtual Machine is integrated, the Mozilla browser is Java-aware, Java is the preferred development environment, etc. To reinforce this, we have selected the name Sun Java Desktop System.\"\n\nNotice: Java is THE PREFERRED DEVELOPMENT ENVIRONMENT.  Look, it has nothing to do with C being \"easy\" anymore.\n\nThey are using GNOME as a shell but promoting the Java platform instead to 3rd parties.  This is not paranoia, this is common sense."
    author: "anon"
  - subject: "Re: Sun chose GNOME for different reasons"
    date: 2004-03-03
    body: "Usually, the training time associated with a language shift is about 6 months until the productivity is recovered. After all, you have to learn a whole new standard library, a whole new set of APIs, and so on.\n\nIf you switch 100 programmers, that's 50 programmer-years worth how much? 50*35K on cheapy programmers (at Sun they are more expensive). That's just sallaries, for a company you have to double that at least. So, let's say 3.5 million dollars.\n\nAnd that's for switching 100 guys from C to C++. Sun probably switched many more."
    author: "Roberto Alsina"
  - subject: "6 months! don't think so."
    date: 2004-03-03
    body: "6 months is absurd. I don't believe that for a minute. For a start the main reason for switching languages is to realise a productivity gain. Using a better language than C, you would regain your C-level productivity very quickly. But the main problem I have with your assertion is that people would *not* have to relearn a whole new standard library or new set of APIs:\n\n1) Once you understand the concepts, switching to another language or library that does the same thing is fast.\n\n2) No one holds a whole API in their head, let alone a whole standard library. During projects and between projects you are always learning and relearning the APIs and libraries that you need at the time for that project.\n\n3) A lot of libraries are the available from multiple languages. You might not even have to switch libraries.\n\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Re: 6 months! don't think so."
    date: 2004-03-03
    body: "Well, I am not the kind that tells people what they should believe in. Please don't believe it at all, if it makes you happier.\n\nI have, however, switched main development languages three times in my life, and 6 months is the time it takes to get confident, and, assuming the language is actually better, reach what I could do before.\n\nSure, I can start coding in a new procedural or OO language in an afternoon. But productively? Months.\n\n1) Sure. Except the concepts are not the same, unless you switch between two very similar languages. Suppose you switch from C++ to Java, which *are* similar.\n\nNow, you are designing a class hierarchy for your app. On C++ you may use multiple inheritance, now you cant. Now you use interfaces. The design process is completely different.\n\nAnd the implementation, if you want to write \"real Java\" instead of \"c++like java\" is going to be very different as well.\n\n2) I do. So forget it. I have about 50% of the Qt API in my head (there are pieces I don't ever use, of course, those I have to lookup). I have the C string functions, the C++ STL. And I am not that much of a programmer. So, \"noone\" is wrong. Maybe *you* don't, though ;-)\n\n3) Sure. But often when you switch, you switch whole.  And the languages nowadays (except C) have a base library as part of it. After all, if you gonna use the same API, you are lessening the impact of the new language's power.\n\nFor example, if you gonna code to glib, why use C++?"
    author: "Roberto Alsina"
  - subject: "Re: 6 months! don't think so."
    date: 2004-03-03
    body: "One little point I forgot in my previous answer.\n\nSuppose you are hiring C++ developers.\n\nI show up with 5 years C experience and 2 months C++\nAnother guy with 3 years C experience, and 9 months C++\n\nWhat do you like better?"
    author: "Roberto Alsina"
  - subject: "Re: 6 months! don't think so."
    date: 2004-03-03
    body: "Which guy? I'd like hear about what you've done and why you choose to do it like that. As the other poster pointed out, there are plenty of experienced people out there writing C in Python for example. Years of experience are often not a good guide.\n\nI would argue that \"years experience\" and skill are not actually that closely related. If want to get better at programming you really need to work at it and be conscious of what you are doing, what works, what doesn't, and most all, read up about better programming/techniques.\n\nReguardding APIs. When I meet a new API I usually read through it all, get a grasp of how it is structured and what is available. But when I'm at the keyboard I usually need to look up most methods except for the most common ones which end up in the grey matter. Actually that is what I like about Python the most. It is much easier to remember.\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Re: 6 months! don't think so."
    date: 2004-03-03
    body: "Well, it was an example. I have more like 18 years C and 6 years C++ :-)\n\nAssuming the applicants are more or less equal, thgat C++ experience should make a difference, IMHO.\n\nAnd sure, experience doesn\u00b4t correlate 1:1 with skill, but it does correlate: A 2 year C++ guy may still program C. A 2 month C++ guy surely still programs C."
    author: "Roberto Alsina"
  - subject: "Re: 6 months! don't think so."
    date: 2004-03-04
    body: "Only if you come from C... I'm fairly sure that I'm still coding Java in C++, and trust me, it does hurt... And I've done Pascal in Visual Basic, Sinclair Basic in Pascal, Pascal in SNOBOL, Visual Basic in Python, Python in Java and Python in Jython (but that's not so bad)."
    author: "Boudewijn Rempt"
  - subject: "Re: 6 months! don't think so."
    date: 2004-03-04
    body: "Well, maybe he switched from coding C in python to coding C in C++.\n\nHe probably fels great about it, too, and feels it very intuitive :-)"
    author: "Roberto Alsina"
  - subject: "Re: 6 months! don't think so."
    date: 2004-03-03
    body: "\"6 months is absurd. I don't believe that for a minute. For a start the main reason for switching languages is to realise a productivity gain. Using a better language than C, you would regain your C-level productivity very quickly. But the main problem I have with your assertion is that people would *not* have to relearn a whole new standard library or new set of APIs:\"\n\nI have heard that arguement many times and I have to call it bull. I have seen people that \"know\" dozens of languages and they write c in all of them. They just learned the syntax of the new language and wrote it the same way they used to. They did not really learn the new language at all. I see that in python fairly often where people write python in a c style and then bitch that it is so slow. Every language works differently and has different capabilities and it takes months to really learn a lanugage and years to master it. I have worked on other peoples python code a fair number  of times and often I can get it to run 100x faster then their version and my code ends up being shorter and simpler. \n\nAlso I disagree with point 2. I hold a large ammount of the python standard library and most of the zope api and other libraries that I use. I think it is needed in order to be able to do a good job. If you are not really familiar with a language's features then you tend to reimplement them in your own code which is a waste of your time, your companies time, and it is often far slower and more buggy.\n \n"
    author: "kosh"
  - subject: "Re: Sun chose GNOME for different reasons"
    date: 2004-03-03
    body: "Sorry but the license issue for Sun has been blown up my many people, and is total and utter rubbish. It was probably something that came up when Ximian did some deals with Sun.\n\nLooking at the technologies Sun uses, mostly C and the fact that Gnome is based on CORBA, something that fits well with Java, it was a logical choice for Sun. A lot of people in Sun think they made the wrong choice, some that they made the right choice.\n\nThe license issue is rubbish because big companies like Sun license software from each other all the time, and with KDE they could still develop totally free software for free. You might as well ask why IBM licensed Java from Sun. Sun totally controlling Java and demanding huge licensing fees was obviously a big concern for IBM."
    author: "David"
  - subject: "Re: Sun chose GNOME for different reasons"
    date: 2004-03-05
    body: "Sun seems to always choose a product that they can dominate.  I think Sun really felt that\nthey could dictate the direction of GNOME and Gtk+ better than they could KDE and Qt because they could fork GTK and GNOME easier.  They do like C better and they bought into the GNOME is better designed message from the GNOME choir.  Of course, NeWS and other technologies from Sun were \"better\" but the failed.  Sun seems to always back losing horses IMHO."
    author: "Tim Brandt"
  - subject: "In the free world, it's a customer's choice"
    date: 2004-03-03
    body: "I think one of the advantages of the Open Source software is that it shifts the power to the customer side. That in my opinion was one of the reasons why UNIX vendors had to jump onto Linux bandwagon - to gettheir hands on what was already their customers' choice. Same thing holds for other open source projects like KDE. If it suits my needs and is supported, I have the freedom to install it on top on my Sun Solaris or whatever platform it happens to be. From the customer perspective what I can see is that there are less critical bugs in KDE and they are fixed much quicker than the ones in GNOME. Count in usability, and that is my sales pitch. The point I am trying to make is that looks, usability, functionality and superior support speed up adoption rate, and that in turn forces big companies to adopt the good thing, or they risk loosing the game. I think it useful to have both users and developers survey results available to the public. Comparison between the two will help to steer the process into the right direction."
    author: "Lex Ross"
  - subject: "Mozilla projects reasoning behind own widget set"
    date: 2004-03-03
    body: "http://developers.slashdot.org/comments.pl?sid=99090&threshold=1&commentsort=0&mode=thread&cid=8450144\n\nAnyone care to comment?"
    author: "foo"
  - subject: "Re: Mozilla projects reasoning behind own widget s"
    date: 2004-03-03
    body: "That sounds about right."
    author: "David"
  - subject: "Re: Mozilla projects reasoning behind own widget set"
    date: 2004-03-03
    body: "his arguments about z-order were rather weak since Konqueror can do it fine. "
    author: "anon"
  - subject: "z-order"
    date: 2004-03-03
    body: "Konq can do z-order fine, but Windows and Mac probably can't. Which is kind of important when you want to make a cross-platform application. (IE on Windows definately can't do z-order.)\n\nLet's not even get into widget sizing and styling...\n\n--\nSimon"
    author: "Simon Edwards"
---
The release is entitled <a href="http://www.evansdata.com/n2/pr/releases/Linux04_01.shtml">Nine Out Of Ten Linux Developers Refute Sco's Linux Lawsuit</a>, but since I'm the one who wrote up the survey, I can tell you a bit more, although I am not allowed by <a href="http://www.evansdata.com/">Evans</a> to "give away the store".  But since I've just written about these particular issues for <a href="http://www.computerworld.com/">Computerworld</a> (see next week's issue for the column), I can touch on them here. 



<!--break-->
<p>
KDE was once again the top choice in desktops, beating GNOME for the second survey in a row, although usage of KDE <i>and</i> GNOME are both increasing. <p>

<a href="http://www.kdevelop.org/">KDevelop</a> and <a href="http://www.trolltech.com/products/qt/designer.html">Qt Designer</a> are also a huge hit with developers, with <a href="http://www.eclipse.org/">Eclipse</a> being the only development tool that was preferred above them (well, possibly netbeans too, depending on how you look at the data). <p>

But, as I explore in next week's Computerworld column, I was disappointed to see that free beer trumps free speech among developers. I won't go into details, because I shouldn't scoop my own column, but suffice it to say that it all boils down to the same reason Sun chose GNOME, why the <i>available</i> SWT is based on GTK rather than Qt (yes, I know there's an <i>unavailable</i> Qt-SWT), and so on. 



