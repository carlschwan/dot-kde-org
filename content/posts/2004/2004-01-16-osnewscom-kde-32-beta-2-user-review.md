---
title: "OSNews.com: The KDE 3.2 Beta 2 User Review"
date:    2004-01-16
authors:
  - "numanee"
slug:    osnewscom-kde-32-beta-2-user-review
comments:
  - subject: "Not a bad review but..."
    date: 2004-01-15
    body: "A couple of grips:\n\n--------------\n I can't bookmark individual text/PDF/etc files in KDE's Bookmarks, only folders. If I can bookmark an HTML page, why not any other type of file? This limits its usefulness. Also, Bookmarks and History ought to be available only in Konqueror's Web Browsing profile. \n--------------\n\nThis is wrong.  I currently have dozens of text files bookmarked in konqueror.  In addition removing them totally from the universal sidebar is just plain stupid.  I like being able to open a web-page without starting konqueror separately beforehand,accessing my mount point that are commonly used, remote computer connections over ssh (via fish://user@location.com), commonly used documents, etc.. etc... etc..  Hell the MAIN use of universal sidebar it the dang bookmarks and history tabs!!\n\n\n-----------------\nTwo words: Drop them. Kaboodle and Noatun are pathetic. They are nowhere as functional as XMMS \n-----------------\n\nSmoking crack did you say?  I understand that most people like XMMS because it is identical to winamp but it has NO WHERE NEAR the features or functionality of Noatun.  Period!  To say such a thing means that the review has no idea what the features and functions of Noatun are.\n\n-----------------\nAt the beginning of this article, I asked, rhetorically, if KDE 3.2 would enable me to use the command line more effectively. The answer to this strange question is yes. Presenting KDialog.\n------------------\n\nCan you say KDE 3.1\n\n\nJust my .02\n\nbrockers"
    author: "brockers"
  - subject: "Re: Not a bad review but..."
    date: 2004-01-15
    body: ">> ... Presenting KDialog.\n>> ------------------\n>> Can you say KDE 3.1\n\nCan you say \"mentionned in the article\"? 2 sentences later:\n\n\"By no means is this new to KDE 3.2 - it was present in 3.1, too.\"\n\nAnyway, kdialog is more useful in 3.2 -- it has some more complicated dialogs, like file dialogs, rather than just \"yes/nos\", warnings, etc.\n\n---Matt"
    author: "optikSmoke"
  - subject: "Re: Not a bad review but..."
    date: 2004-01-15
    body: "> -----------------\n> Two words: Drop them. Kaboodle and Noatun are pathetic. They are nowhere as\n> functional as XMMS \n> -----------------\n\nCould not agress more on that, or again move them to kdeextragear.\n\n... Have not tried out Juk, maybe it obsoletes them...\n"
    author: "ac"
  - subject: "Re: Not a bad review but..."
    date: 2004-01-15
    body: "JuK is a different.. but greatly replaces here any other software capable of playing music..."
    author: "J"
  - subject: "Re: Not a bad review but..."
    date: 2004-01-15
    body: "Well, I wouldn't say XMMS was better, but I still agree. I would like to see Kaboodle, Noatun _and_ JuK removed from KDE multimedia, and add Amarok instead. It's small, fast, slick, and it has a good playlist design.\nFor video, maybe KDE should add kmplayer, it doesn't need MPlayer to compile (so it's small and has no external dependencies), as it is the best plugin for Konqueror to play _any_ media on the internet (I haven't seen any media that didn't work,that is). "
    author: "LaNcom"
  - subject: "Re: Not a bad review but..."
    date: 2004-01-16
    body: "Personally, I'd much perfer to see kaffeine in the base distribution\nit uses the xine libraries which are already used by the \nxine_artsplugin in kdemultimedia.\n\nJust my $0.02\n"
    author: "John"
  - subject: "Re: Not a bad review but..."
    date: 2004-01-16
    body: "Well, kmplayer is a frontend, it supports MPlayer, Xine, ffmpeg and ffserver as backends. It supports recording from a TV-card, streaming (client&server) and acts as a Konqueror plugin for Quicktime, AVI, Real, WMA/WMV, DivX etc. \nSo, the dependencies are not an issue. If Kaffeine also supported MPlayer, I wouldn't care which one goes in."
    author: "LaNcom"
  - subject: "Re: Not a bad review but..."
    date: 2004-01-16
    body: "But it is not nearly as usable as kaffeine.\n\nTo me having kaffeine for video and juk for music would be perfect.\nNoatun tries to do to much and ends up being too hard to use.  Kaboodle has the right idea, but has a really kludgy interface.\n\nThough this is all moot with 3.2 about to come out.\n\nFor 3.3/4 it would be nice to see a video pure video app created, totem from gnome is a good example of a video player with a good interface.  Some of noatun's features like visualizations be put into juk.  With this Kaboodle and Noatun could be phased out.  Maybe gstreamer will be good enough with their 0.8 release to be a viable media framework to be used in 3.3/4.\n\nBut all said 3.2 is an incredable release.  I am running 3.2beta2 right now and I can say hands down it is the best desktop I have ever used. Thankyou kde developers."
    author: "theorz"
  - subject: "Re: Not a bad review but..."
    date: 2004-01-16
    body: "What is this Kaffeine you speak of?"
    author: "ac"
  - subject: "Re: Not a bad review but..."
    date: 2004-01-16
    body: "http://kaffeine.sourceforge.net/"
    author: "Anonymous"
  - subject: "Re: Not a bad review but..."
    date: 2004-01-15
    body: "Ummm, he said \"functional\" not \"lots of functions\".\nLook it up in your dictionary.\n"
    author: "Mike"
  - subject: "Re: Not a bad review but..."
    date: 2004-01-15
    body: "Noatun is really good. The default GUI is horrible but it does work as good as xmms and even more when listenning radios via internet.\n\nI agree with the author about Kedit and Kwrite. I use Kedit because it's light to edit a small text file (I don't have to use vi) or Kate to write scripts, programs, but never Kwrite.\n\nAnd you can bookmark any type of files you want. Not only folder or html files."
    author: "JC"
  - subject: "Right on about Noatun"
    date: 2004-01-15
    body: "Amen about the default GUI and icons. I constantly get confused about which button opens the playlist and which opens a file. Some of the worst icon selection I've ever seen. Functionally though, its a pretty solid piece of work.\n"
    author: "Anony Guy"
  - subject: "Re: Not a bad review but..."
    date: 2004-01-16
    body: "I happen to like noatun and lot and honestly I very rarely use its gui. What I use is its keyz plugin. From that I can stop,start,pause,change songs, change volume etc all from the keyboard with any application having the focus. The gui I see for noatun is that little circle it puts in the system tray.\n\nI have not used xmms for anything beyond playing with it a few times to see if it had the features I wanted for years now and I still see no reason to use it. Sure xmms has a better looking gui but when I just want the app to stay in the system tray who cares about its gui? I just want it to play music and let me get on with coding, testing, debugging etc and noatun does that much better."
    author: "kosh"
  - subject: "Re: Not a bad review but..."
    date: 2004-01-16
    body: "Smoking crack did you say? I understand that most people like XMMS because it is identical to winamp but it has NO WHERE NEAR the features or functionality of Noatun. Period! To say such a thing means that the review has no idea what the features and functions of Noatun are.\n\n-- \n\nDoes noatun require arts to run?  If so, it's useless to me.  Does noatun utilize XMMS input plugins?  If not, it's useless to me.  Look, I love KDE, but its multimedia side is so far behind the rest of the desktop that it's embarrassing.  I use almost exclusively KDE applications, but I still use xmms and mplayer because they're superior to anything else out there (I use gaim, too, though kopete ain't bad--needs Perl scripting, however).\n\nAnyway, the point is, for a lot of us, kdemultimedia just doesn't cut it.  Getting indignant won't change the fact that xmms is superior *for us*, for the time being."
    author: "KDE User"
  - subject: "feelings of ditto"
    date: 2004-01-16
    body: "I use xmms for exactly the same reason that I use KDE. It doesn't crash and it doesn't hog CPU, which AFAIK (as of versions for 3.1.5) everything in kdemultimedia still does. I've similar hangups with JuK (my 17 gig music collection kills it whilst rhythmbox copes) and even newer kopetes (which will happily hang my entire KDE when network connectivity is bad).\n\nI'm not complaining and there's nothing I'm willing to do about it, I'm just saying - the man probably has a point. \n\n(Am desperately looking forward to new versions of konqueror, kmail and quanta - by far my favorite linux apps.)"
    author: "c"
  - subject: "Re: Not a bad review but..."
    date: 2004-01-17
    body: "> Kaboodle and Noatun are pathetic.\n\nWhat is the problem here.  This appears to be the second reviewer that doesn't understand that one of the functions of Kaboodle and Noatun is to be a fron end for XINE.\n\nWill XMMS play Quicktime movie trailers with sound?\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Not a bad review but..."
    date: 2004-01-17
    body: "But a crippled one. Noatun can't play dvd/vcd, although it should simply pass dvd:// or vcd:// to the xine-lib. They can't play most of the asx, mov, ram urls from the web (kmplayer can also using xine-lib)."
    author: "lazarus"
  - subject: "rpm"
    date: 2004-01-15
    body: "> \"An upgrade to this beta is a simple matter of 'rpm -Uvh ./*.rpm --nodeps --force'\"\n\nLol. Why to use a rpm system if you use --nodeps and --force ?\n"
    author: "JC"
  - subject: "Re: rpm"
    date: 2004-01-15
    body: "I'm using SuSE and do that always for KDE updates and SuSE version\nupdates. You can go into YaST (the SuSE setup tool) afterwards and\nsay: Check integrity and it lists all remaining or new problems.\nYou can solve them one by one which is often far easier than to\nobey RPM. Because my experience is: RPM sometimes won't install KDE\nand claims that some apps are not compatible but afterwards everything\nruns fine. It's all a matter of the order in which you install the\npackages I guess."
    author: "Mike"
  - subject: "Re: rpm"
    date: 2004-01-17
    body: "Hmmmmmmm: \"--nodeps\"\n\nDo you suppose that is how he forgot to install XINE and a lot of other audio and video libraries?\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "please drop kedit"
    date: 2004-01-15
    body: "\nJust drop it, users don't want it and get confused by it. And since kwrite\nis just better (yes I wanna generalize), just drop kedit, or move\nit to kdeextragear. So the ones who really prefer it can still use it.\nAt least don't ship kde with it. Shipping it bloats kde.\n\nOther nice thing, for the windows explorer used people...:\n\nCould konqueror show the available space of the current partition\nin it's status bar? Definately a big + in comfort!!\nRMB on a file is not really intuitive to find our the free disk space...\nOr is it and I am on crack?? ;-)\n\n\nApart of that, looking forward to 3.2, it just seems to rock!!\nThanks to all the contributors"
    author: "ac"
  - subject: "Re: please drop kedit"
    date: 2004-01-15
    body: "IIRC, kedit support bidirectional editing, kwrite doesn't (yet). As soon as kwrite supports bidi, kedit should be dropped..."
    author: "LaNcom"
  - subject: "Re: please drop kedit"
    date: 2004-01-15
    body: "Yeah, there is a highly experimental-at-this-stage branch of kate called KATE_GOES_BIDI.. As it matures, kedit will get dropped. Hopefully by 3.3. "
    author: "anon"
  - subject: "Re: please drop kedit"
    date: 2004-01-16
    body: "Sometime kate (and derived apps like kwrite/kdevelop/quanta) mangles my code.\n\nI get dubble lines and last-second-indentation-corrections...\n\nNever the less i used kate in my own app, and love the features that it comes with.\n\n"
    author: "cies"
  - subject: "YUP, drop it"
    date: 2004-01-16
    body: "I've never seena  difference and that's why I don't use it. I only use Kate, and I also don't know what Kwrite is for, but I think the features of Kedit and kwrite should be merged in Kate. \n\nBTW: RENAME KWRITE. it sounds like a wordprocessor."
    author: "Alex"
  - subject: "Kedit"
    date: 2004-01-15
    body: "I think their is a problem with bidi support, and I like Kedit. Its useful for all those little tasks. Kate I use for programming, and I don't like filling up the open pages bar, nor waiting for them to load when I just wanna had an /etc file."
    author: "Leon Pennington"
  - subject: "Re: Kedit"
    date: 2004-01-15
    body: "> I think their is a problem with bidi support, and I like Kedit. Its useful\n> for all those little tasks. Kate I use for programming, and I don't like\n> filling up the open pages bar, nor waiting for them to load when I just\n> wanna had an /etc file.\n\n\nmm ok maybe then kwrite should be fixed in terms of bidi...\nIt has maybe this 1 bug, but many other features over kedit, and it\nnot really slower. Kedit can still be used, and be in kde cvs.\n\nIt just should not be shipped IMO."
    author: "ac"
  - subject: "Re: Kedit"
    date: 2004-01-16
    body: "Missing right-to-left is not just *one* bug.\n\nWhat would you tell if an editor could use right-to-left but not left-to-right? You would want another.\n\nSo right-to-left writers have a similar problem and therefore they use KEdit.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Kedit"
    date: 2004-01-15
    body: "I'm using kate for programming, too, I just have one last problem with it:\n\nHow can I move the focus between the text-editing window and the builtin-terminal-window using the keyboard? Everytime I have to use the mouse, and I hate it!\n\nalso, is there a shortcut for going to line x (like :x in vi)?\n\nthanks!"
    author: "me"
  - subject: "Re: Kedit"
    date: 2004-01-15
    body: "Ctrl+g brings up the goto line dialog.\n\nGoto configure shortcuts...\nAnd go down to Kate->Show terminal\nAnd enter whichever shortcut you want."
    author: "Leon Pennington"
  - subject: "Re: Kedit"
    date: 2004-01-16
    body: "Thanks for the ctrl+g thingy!\n\nabout the terminal, I don't want to switch it on and off, but rather have it always on and just switch focus. Is that not possible?\n\nthanks!"
    author: "me"
  - subject: "Re: Kedit"
    date: 2004-01-16
    body: "I don't think so. You could put a wishlist item in the bug database for that."
    author: "Leon Pennington"
  - subject: "Re: Kedit"
    date: 2004-02-01
    body: "I would also like to be able to switch focus between the editor and terminal without using my mouse. I lose way too much time reaching for my mouse every time I want to go in and out the terminal, so instead I use a seperate Konsole and ALT + TAB to it. The only change they need to make is to let the terminal receive the focus when it's shown and return the focus to the last editor used when it's hidden again. This would make Kate perfect ;)"
    author: "Dieter Bercx"
  - subject: "Re: Kedit"
    date: 2004-01-15
    body: "What we actually need in Kate is a shortcut for commands. Like the \":\" in vi and the \"Meta-x\" in Emacs."
    author: "Source"
  - subject: "Re: Kedit"
    date: 2004-01-16
    body: "I don't use kate, but wouldn't it be possible for you to use the vim kpart?\n"
    author: "Mark Dufour"
  - subject: "Re: Kedit"
    date: 2004-01-16
    body: ">What we actually need in Kate is a shortcut for commands. Like the \":\" in vi and the \"Meta-x\" in Emacs.\nTry pressing F7 :)\n\nRischwa"
    author: "Rischwa"
  - subject: "Re: Kedit"
    date: 2004-01-16
    body: "hey!!\n\nwhat can I enter in that command-line? vi-style won't seem to work. What is this?!"
    author: "john"
  - subject: "Re: Kedit"
    date: 2004-01-17
    body: "There seems to be various commands you can use.  Just press a key and it will show a list of commands starting with that letter (try s, that gives a lot).  I'm not sure how useful this is, it's certainly no M-x ;)"
    author: "Haakon Nilsen"
  - subject: "Re: Kedit"
    date: 2004-01-16
    body: "Are there any programs in KDE or under Linux that can automatically format C++ code?\n\nI was recently using Eclipse for a Java program and it's automatic formatting function is awesome. It saves a lot of time when moving loops and if statements about.\n\n"
    author: "Jos"
  - subject: "Re: Kedit"
    date: 2004-01-16
    body: "Yep theirs KDevelop, switch on automatic indentation, and set the source formatting how you like it.It doesn't do this for copy and paste yet, so you have to press format source under the edit menu. Works very well all round. Also the auto-completion and Persistant Class Stores more than make up for this. ( PCS Means you can tell it about any h files your using and it will autocomplete with those so it already knows any external function you will use)."
    author: "Leon Pennington"
  - subject: "Re: Kedit"
    date: 2004-01-16
    body: "AFAIK it uses astyle (http://astyle.sf.net) for that. Just in case you don't want to install KDevelop (who wouldn't want that???).\n"
    author: "cloose"
  - subject: "Re: Kedit"
    date: 2004-01-16
    body: "I just checked out astyle.\nIt does a lot of nice things, but unfortunately does not do everything well.\nI like tabs and the author obviously likes spaces, since every option still leaves spaces at some level of indentation. Also there's no option to specify what maximal width you'd like the code to be.\nAnd furthermore it does not recognize and reformat const char * string.\nFor example:\n\nwelcome->setText(QString(\"<p><b>\")\n+tr(\"Welcome to the cube test.\")\n+\"</b></p><p>\"+tr(\"We are going to compare cubes.\")\n+\"</p><p>\"+tr(\"Choose the cube that is the same as the\"\n\" top cube. You can turn the cubes with \"\n\" the mouse. The six squares next to the \"\n\"cube are the same as the six sidex of the \"\n\"cube.\")\n+\"</p><p>\"+tr(\"Click \\\"Start\\\" to begin.\")+\"</p>\");</tt>\n\nA multiline constant can be fitted to the width of the line simply by moving around the quotation marks.\n\nAny formatter that does only one thing wrong is useless, because you'll need to fix something by hand anyway.\nNo disrespect to astyle's auther: obviously formatting c++ code is difficult because of all the different esthetical point of view."
    author: "Jos"
  - subject: "Source of speedups?"
    date: 2004-01-16
    body: "I follow kde cvs digest and lurk on kde-optimize, but somehow I \nmissed the source of the performance improvements all the\nreviewers are talking about.  Besides konqueror pre-loading, what\ncaused the speedup?"
    author: "steve"
  - subject: "Re: Source of speedups?"
    date: 2004-01-16
    body: "I have the same question.  :)"
    author: "Navindra Umanee"
  - subject: "Re: Source of speedups?"
    date: 2004-01-16
    body: "better gcc ?\nbetter rpm packages ?"
    author: "JC"
  - subject: "Re: Source of speedups?"
    date: 2004-01-16
    body: "A lot of the speedups are just due to optimizations in (say) KHTML and other applications. Qt 3.2.x is also perceptibly faster than 3.1.x was. Memory usage is also reduced a bit. I've been using KDE 3.2 for awhile, and its definitely the fastest KDE since 1.x. "
    author: "Rayiner Hashem"
  - subject: "Re: Source of speedups?"
    date: 2004-01-16
    body: "http://members.shaw.ca/dkite/jan32003.html#Optimizations\n\nHa! I can even quote my own work.\n\nThere was a similar commit last week. That was one of the speedups.\n\nhttp://members.shaw.ca/dkite/feb142003.html#oKonqueror\n\nis another from Safari.\n\nhttp://members.shaw.ca/dkite/apr252003.html#fKonqueror for another micro optimization.\n\nhttp://members.shaw.ca/dkite/may162003.html#oKonqueror Maks' optimization making Konq a usable file manager.\n\nhttp://members.shaw.ca/dkite/may302003.html#oKonqueror and another.\n\nhttp://members.shaw.ca/dkite/jun62003.html#fKonqueror\n\netc. \n\nI got to set up an index of some sort for this stuff. If someone wants to look further, http://members.shaw.ca/dkite/backissues.html is the whole list of digests, and one can go from the TOC to the Konqueror sections.\n\nDerek (who wishes that he had more time...)\n\nps. maybe someone with authority and power can make these things real links? :)"
    author: "Derek Kite"
  - subject: "Re: Source of speedups?"
    date: 2004-01-16
    body: "Responding to myself. Yes, I do it all the time.\n\nSome of the Safari work included optimizations. I think some work was done on the config management to quicken things up.\n\nCan't think of anything else off hand. And yes, it is much faster than 3.1. I had (before things got stable) 3.1 and cvs on my box, and it was painful to go back to 3.1. Of course, things seem fast for a very short time, then my inherent impatience flares up again. But I think the waits are now the transfer speeds rather than khtml.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Source of speedups?"
    date: 2004-01-16
    body: "And kernel 2.6 is another source. no konqi lockups.even soffice is usable now"
    author: "muleli"
  - subject: "Re: Source of speedups?"
    date: 2004-01-16
    body: "I would say there are simply a *lot* of small optimization which sum up,\nand if these happens to be in libaries, all apps benefit from it.\nThe question is why hasn't this be done with earlier releases? Of course\nthere will be multiple reasons, but for sure the use of performance analyisis tools to locate the problems should be one.\n\nMe as the author of calltree (derived from cachegrind) and KCachegrind\nwould be interested in how many of the optimizations were done on the background of using these tools.\nUnfortunately I get very little feedback on this,\nonly some of the optimization commits mention cachegrind.\n"
    author: "Josef Weidendorfer"
  - subject: "Re: Source of speedups?"
    date: 2004-01-16
    body: "KDE doesn't make use of templates does it? The QT Abstract Data Types all seem to use void* at the back end or they did when I last looked at them which was admittedly over 4 years ago! The reason I ask is there's been a recent fix to g++ that means it now picks up the correct specialisation for a given STL container whereas before it didn't.\n\nIt'd be interesting to see what versions of gcc where used in the review for the 3.1 and 3.2 versions. There have been a number of subtle improvements in both the compiler and the STL shipped with it.\n\nNow KDE is pretty much feature complete we can really start work on the optimisation. \n\nThese should be good times."
    author: "Nick"
  - subject: "Re: Source of speedups?"
    date: 2004-01-16
    body: "I've actually been using KCacheGrind during feature freeze on the stuff I've been doing. I usually don't use software like this all the time, only when I pause. So maybe the next release it will get more mentions."
    author: "Leon Pennington"
  - subject: "Re: Source of speedups?"
    date: 2004-01-16
    body: "To give you some feedback: at TT, we use both tools frequently. Many of the performance improvements (even more in the upcoming version 4) can indeed be attributed to the availability of those tools."
    author: "Matthias Ettrich"
  - subject: "DCOP"
    date: 2004-01-16
    body: "Can Kuickshow be instructed to start a slideshow with DCOP?  If so, it could be made into a service menu item like he suggests in page 3."
    author: "Spy Hunter"
  - subject: "Re: DCOP"
    date: 2004-01-16
    body: "> Can Kuickshow be instructed to start a slideshow with DCOP?\n\nIn KDE 3.2, yes:\n\nid=`dcopstart kuickshow`\nsleep 1\ndcop $id kuickshow activateAction kuick_slideshow\n\nDon't know yet, why the sleep is necessary."
    author: "Carsten Pfeiffer"
  - subject: "OT: disabling some programs"
    date: 2004-01-16
    body: "hello there sorry for my offtopic post,\n\nbut i would like to know, if there is a way to disable certain programs when compiling kde from source,\n\nany gui (or implemented in konstruct (i've never tried this because i use gentoo)) where you can choose which applications should be installed.\n\neg:\n\nkde-games:\n\nKasteroids         (y)\nKbounce            (n)\n...\nKPoker             (y)\n...\n\nand so on. \n\nkde would be more slick and navigation with k-Menue would be better because only apps needed would be installed..\n"
    author: "hatsch"
  - subject: "Re: OT: disabling some programs"
    date: 2004-01-16
    body: "You can just export DO_NOT_COMPILE=\"Things you don't want to compile\" and then emerge kdewhatever.\n\nJust search the gentoo forum to find out more about this option and how to use it. \n\n"
    author: "ralph"
  - subject: "Re: OT: disabling some programs"
    date: 2004-01-16
    body: "cool,\nthank you i will check that out"
    author: "hatsch"
  - subject: "Re: OT: disabling some programs"
    date: 2004-01-16
    body: "Or create a file \"inst-apps\" with the directories you want to compile and run \"make -f Makefile.cvs\"."
    author: "Anonymous"
  - subject: "Plastik is very nice, but the color scheme isn't"
    date: 2004-01-16
    body: "Plastik is IMHO a very nice looking theme. It however does lack contrasting colors. Everything is gray, and almost the same shade of gray too.\n\nOn my system I changed two colors and now it looks MUCH better. I have made the title bar of the active window light blue and made the buttons somewhat darker:\n\n'Active Title Bar' is set to #1B7EE2\n'Button Background' is set to #D5D7DC\n\nMakes the theme prettier to look at and more usable, IMHO.\n\nHere is a screenshot I prepared: http://hensema.net/tmp/shot.png"
    author: "Erik Hensema"
  - subject: "Re: Plastik is very nice, but the color scheme isn't"
    date: 2004-01-16
    body: "I double that.\nPlastik is WAAAAYYYY too gray for me. Your changes are very welcome. I can even begin to like Plastik."
    author: "Romain"
  - subject: "Re: Plastik is very nice, but the color scheme isn't"
    date: 2004-01-17
    body: "Ohh noo!! The original colours are fine - I do run it on a laptop though. I'm really impressed with the theme and vote it becomes the default for 3.3."
    author: "Malcolm"
  - subject: "Re: Plastik is very nice, but the color scheme isn't"
    date: 2004-01-17
    body: "Changing the outline color of widgets makes the theme prettier,\nhere is a screenshot of my plastik theme:\nhttp://mywebpage.netscape.com/jlrch2/instant/aboutme.html"
    author: "tetabiate"
  - subject: "Re: Plastik is very nice, but the color scheme isn't"
    date: 2004-01-21
    body: "Personally, I think Plastik looks perfect with the Redmond XP color scheme."
    author: "Miles Robinson"
  - subject: "New languages?"
    date: 2004-01-16
    body: "Do you know whether new languages can be added in the releases 3.2.x? There still is no internationalization option for nds (DE, NL, DK regional language) support. "
    author: "Elektroschock"
  - subject: "Re: New languages?"
    date: 2004-01-16
    body: "The KDE translation policy tells that the language must be an official language of at least one country for being in the KDE I18N module.\n\nAs Niederdeutsch (I suppose that you are talking about this) is an offical language neither of Germany, nor of the Netherlands nor of Danmark, it will never be an official I18N package. (As opposite to Dutch, also of this language familly but official language at least of the Netherlands.)\n\nThe rule is just to maintain the kde-i18n module at a somewhat acceptable size, as there are many locale languages that are not official. (Just a few other examples for Germany only: Bavarian, Saxonian, Schw\u00e4bisch...)\n\nI do not know if anybody had tried to start such a translation. (Perhaps you could look in the archives of kde-i18n-doc at http://lists.kde.org )\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: New languages?"
    date: 2004-01-17
    body: "Then why is there a en_GB and a en_US translation?\nIs the official language of USA american english?"
    author: "anonymous"
  - subject: "Re: New languages?"
    date: 2004-01-17
    body: "Their isn't a en_US as far as I'm aware ( The doesn't appear to be one in the kde-i18n ). en_GB is Great Britain, and the language is spelled differently than in the USA. Plus there are other differences. Meter and Metre are to different words in en_GB."
    author: "Leon Pennington"
  - subject: "Re: New languages?"
    date: 2004-01-17
    body: "Yes, you're right. Of course there is no en_US module but thats because the original messages are written in american style.\n\nBut on the other hand you proved my point. There is only a en_GB because some words a different or spelled differently. So it's basically a kind of dialect (I'm sure most english people see it the other way around :)).\n\nSo back to my point, why do we make a difference between british and american english but also disallow to add a dialect for german?"
    author: "anonymous"
  - subject: "Re: New languages?"
    date: 2004-01-17
    body: "Doh, I just read the message below. Thanks Thomas for the explanation."
    author: "anonymous"
  - subject: "Re: New languages?"
    date: 2004-01-17
    body: "> The KDE translation policy tells that the language must\n> be an official language of at least one country for being\n> in the KDE I18N module.\n\nThere is no such policy. On the contrary: We avoid mere dialects and other \"fun projects\" but KDE always gave a chance to minority languages that don't get an \"official status\" in their countries for one reason or another. We want to be sure, however, that every translation is continuously maintained and that there is a real chance to have it completed in the foreseeable future. Minority languages often have a problem with this, simply because there is no big pool of native speakers to attract translators from. This has also been the problem with nds so far but there are still a few people around who want to change this situation.\n\nRegards,\n\nThomas"
    author: "Thomas Diehl"
  - subject: "Re: New languages?"
    date: 2004-01-17
    body: "Well, for me it sounds very different on what was written on kde-i18n-doc, at least in the past.\n\nBut if the translation policy has changed, good, I take note of it.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: New languages?"
    date: 2004-01-18
    body: "No idea why you think there was a change in the policy. Just have a look at http://i18n.kde.org/teams/ which includes languages like Latin, Occitan, Breton (although mostly unmaintained, thus illustrating the problem of \"no big pool of native speakers to attract translators from\"). Or look at \"policy postings\" like http://lists.kde.org/?l=kde-i18n-doc&m=103233498728247&w=2.\n\nThomas\n"
    author: "Thomas Diehl"
  - subject: "KDE needs a decent media player (framework)"
    date: 2004-01-16
    body: "Don't get me wrong, we already have a few good ones like noatun and kaboodle.\nNoatun seems to me like a plug-in toy with a hairy interface, so of those \nI prefer kaboodle as it has the easiest GUI.\n\nBut KDE needs a more generic framework for extended funcionality, like streaming.\nGnome is in the process of building such a media framework, GStreamer.\nThis can probably be adapted by KDE at some point, but I'd like to point the finger to the VideoLAN project over at http://www.videolan.org/.\n\nThis is already, and has been for quite a while, a very promising media framework with lots of potential.\nThe VideoLAN client is ported to tons of platforms, it plays most media formats (without any external win32 DLL dependencies), it's pluggable, and  extremely powerful when it comes down to streaming (multicast, relaying, multi-protocol, video on demand).\n\nThe VideoLAN client is based on a generic C/C++ library, so writing a KDE front-end is easy. In fact, it's already done but it's AFAIK not maintained and the GUI isn't the greatest.\nVideoLAN has a plugin for Mozilla, I'd love to see a KHTML plug-in for integrated Web-TV applications.\n\nWell... I guess that's work for KDE 4.0? ;)\n"
    author: "Jan Vidar Krey"
  - subject: "Re: KDE needs a decent media player (framework)"
    date: 2004-01-17
    body: "> Gnome is in the process of building such a media framework, GStreamer.\n\nGNOME is not building GStreamer, they are using it (earlier than KDE because they had no other framework before).\n\nhttp://www.gstreamer.net/docs/cvs/faq/html/chapter-general.html#general-gnome\n"
    author: "Anonymous"
  - subject: "Splashscreen?"
    date: 2004-01-17
    body: "I'm having problems with the Splash screen as well (just freezes on 'starting interprocess communications', and I'm using KDE CVS."
    author: "anonymous"
---
<a href="mailto:rahul@NOSPAM.linux.net">Rahul Gaitonde</a> has written <a href="http://www.osnews.com/story.php?news_id=5670">a fairly comprehensive review of KDE 3.2 Beta 2</a> for <a href="http://www.osnews.com/">OSNews</a> based on his 3 week trial. <i>"The target machine - my only computer - is a Pentium II 266 MHz with 384 MB RAM, with an Intel i810E chipset. [...]  The first thing you notice when you start up a few apps is - 'Boy, this is Fast!'. KDE 3.2 is significantly faster than 3.1, and certainly way faster than Gnome 2.4 on my machine. It reminds me of the kind of responsiveness that Windows 98 used to give me on this same configuration few years ago (minus the crashes). Konsole opens up almost instantaneously, and Konqueror takes only about 3 seconds the first time. I was afraid that the increase in bloat with every release of KDE since the 1.x series would one day prevent me from using this computer at all with KDE. I'm glad the guys over at KDE have so splendidly allayed my fears."</i>  The review has a lot of screenshots and other information on the release.  As usual, <a href="http://www.kde-look.org/content/show.php?content=7559">Plastik</a> gets huge props.
<!--break-->
