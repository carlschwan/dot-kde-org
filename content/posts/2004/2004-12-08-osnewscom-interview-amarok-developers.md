---
title: "OSNews.com: Interview with amaroK Developers"
date:    2004-12-08
authors:
  - "mkretschmann"
slug:    osnewscom-interview-amarok-developers
comments:
  - subject: "UI modification"
    date: 2004-12-08
    body: "Amarok is a great application and I think a slight modification \nto its UI will make it even greater and easier to use.  The \ncollection tab should look like what the playlist now looks, \nthat is, should include columns for artist, album, title etc, \nwhile the play list need only contain the title.  this will be \nmuch more usefull for doing a search, than the current design.  \nOne rarely does a search on the items in the current playlist, \nbut frequently does a search on the collection list to select\nsongs and add to the playlist.  To do search, a juk like \ninterface is strikingly simple and intuitive than the current \namarok design.\n\nOr if only juk had got a temporary playlist to which \nsongs can be added, or removed from, easily.  The custom \nplaylist in juk are intended to be very large collection\nand one does not want to play all of them in that order \nat a time.\n\nask "
    author: "Ask"
  - subject: "Re: UI modification"
    date: 2004-12-08
    body: "amaroK 1.2 offers a \"Flat mode\" for the Collection-Browser, listing items directly with Title, Artist, Album. This is a handy alternative to the tree display, which can be cumbersome to navigate when doing searches.\n"
    author: "Mark Kretschmann"
  - subject: "Re: UI modification"
    date: 2004-12-08
    body: "A three clumn view would be cool\n\nLeft: existing tree view\nMiddle: flat view of selected titles in tree view\nRight: Playlist (but smaller as now)"
    author: "Sebastian Gottfried"
  - subject: "Re: UI modification"
    date: 2004-12-09
    body: "Good news.  thanks to the developers for including \na much wanted nodification \n\n-Ask"
    author: "Ask"
  - subject: "GNOME-VFS"
    date: 2004-12-08
    body: ">For accessing non-local protocols like http (for streaming) or ftp, all \n>current GNOME based GStreamer applications relies on the gnome-vfs plugin, \n>which depends on GNOME libraries. This is, of course, not an acceptable \n>solution for KDE applications. So there were efforts by myself and others to \n>offer KIO support, which is the KDE equivalent to gnome-vfs, but >unfortunately KIO does not cooperate very well with GStreamer's design\n\nIf KDE adopts GStreamer in addition to Qt4 this could ironically mean that KDE has now adopted Glib, GObject, GModule, GNOME-VFS, ATK, AT-SPI, OrBIT, libxml...\n\nOne guess as to which of GNOME and KDE is now the defacto standard?  If you install KDE, you will have to install half of GNOME..."
    author: "ac"
  - subject: "Re: GNOME-VFS"
    date: 2004-12-08
    body: "afaik they try to not let this happen, and thats good, because I whoulnt like it :D\n\nmore C/gnome dependency's, please, no..."
    author: "superstoned"
  - subject: "Re: GNOME-VFS"
    date: 2004-12-08
    body: "Umm, the point of having a plugin system, like GStreamer does,  is so that you can offer a lot of diffent plugins for different systems, desktops and whatever. The fact that GStreamer has a Sun audio plugin for instance does not create a 'solaris' dependency for Linux users (to illustrate how broken your logic is). And the gnome-vfs plugin do not create a dependency on GNOME libs for KDE users, just like the KIO plugin will not create a  KDE dependency for GNOME users."
    author: "Christian Schaller"
  - subject: "Re: GNOME-VFS"
    date: 2004-12-08
    body: "I am satisfied by the answers.  Thank you for your big contribution to the Free Software and hence KDE community!  I admit I used to think of you as \"the enemy\" because of some of your KDE comments in the past.  =)"
    author: "ac"
  - subject: "Re: GNOME-VFS"
    date: 2004-12-08
    body: "Like most other people I eventually grow up :)"
    author: "Christian Schaller"
  - subject: "Re: GNOME-VFS"
    date: 2004-12-08
    body: "Uhm, first glib is hardly \"half of GNOME\".  I don't know if you've noticed, but they've got something like 80 tarballs in their source distribution.\n\nGStreamer is split up into a \"core\" which just requires glib (libxml is optional and won't be used in the next version anyway) and plugins.  The plugins have lots of weird dependencies based on what they do.  i.e. the aRts plugin requires aRts, the GNOME-VFS plugin requires GNOME-VFS, the (defunct) KIO plugin requires kdelibs, etc.  That's not a GNOME leaning thing; that's just up to the plugin authors.  Packagers split up those plugins into several packages so that the deps don't get out of control.\n\nQt 4 does not require the accessibility stuff.  IIRC it's plugin based, so it can be shipped in a different package."
    author: "Scott Wheeler"
  - subject: "Re: GNOME-VFS"
    date: 2004-12-08
    body: "Although Christian has already replied, I think I should add that using gstreamer instead of aRts would actually add 0 (zero) g* libraries to KDE.\n\naRts already uses glib, and I'm pretty positive we already use libxml2 somewhere anyways.  ORBit was never a possibility (and I betcha GNOME ditches it sooner or later), so don't worry yourself about that. ;)  The rest Christian and Wheeler have already explained about."
    author: "Michael Pyne"
  - subject: "Re: GNOME-VFS"
    date: 2004-12-09
    body: "libgobject would be added. It is not currently used anywhere, since it doesn't do anything more than a _poor_ impersonation of C++."
    author: "Carewolf"
  - subject: "Re: GNOME-VFS"
    date: 2004-12-09
    body: "To be fair Stefan only added the glib dependency to aRts to get GNOME adoption.  He basically added it despite objections that nobody wanted to maintain that code and then disappeared.\n\nSo aRts is now this monstrosity that is considered tied to KDE but which really duplicates almost every single KDE technology.  Hardly a good excuse for basing the decision of KDE's next media framework on."
    author: "ac"
  - subject: "Re: GNOME-VFS"
    date: 2004-12-09
    body: "> aRts is now this monstrosity that is considered tied to KDE but which really duplicates almost every single KDE technology\n\nHow good does it render HTML?"
    author: "Anonymous"
  - subject: "Girlfriend?"
    date: 2004-12-08
    body: "I can't believe they had to link the word \"girlfriend\" to a definition in case the readers didn't know what the word meant."
    author: "Jim"
  - subject: "Re: Girlfriend?"
    date: 2004-12-08
    body: "Ha-ha-ha!! You don't know any Real Geeks(TM) then ;-D\n\n~Macavity"
    author: "Macavity"
  - subject: "Re: Girlfriend?"
    date: 2004-12-08
    body: "Yeah, I even got angry as I wasted my time clicking on it. It's not funny."
    author: "AC"
  - subject: "Re: Girlfriend?"
    date: 2004-12-08
    body: "I find your lack of amusement amusing. =)"
    author: "ac"
  - subject: "Re: Girlfriend?"
    date: 2004-12-08
    body: "Just out of curiosity, what did you expect to find behind that link since you bothered clicking on it in the first place? ;)"
    author: "Christian Schaller"
  - subject: "Re: Girlfriend?"
    date: 2004-12-08
    body: "\nHmm, Maybe a pic of the girlfriend to drool or a datin service ?"
    author: "ac"
  - subject: "Re: Girlfriend?"
    date: 2004-12-08
    body: "Oh, she was lovely, I tell you! :) Now she's gone, and we have amaroK. Ummm."
    author: "Mark Kretschmann"
  - subject: "Re: Girlfriend?"
    date: 2004-12-08
    body: "We all love you markey! You made music fun again!"
    author: "Max Howell"
  - subject: "OSNews observation"
    date: 2004-12-08
    body: "All though I like the articles on OSNews for the most part, the comments are basically eye cancer.  There is such a great percentage of vocal uninformed people that post on that site.\n\n\nBret Baptist."
    author: "SMEAT!"
  - subject: "Re: OSNews observation"
    date: 2004-12-08
    body: "Completely agree. I check the site regulary for news updates (they're pretty fast), but I refuse to read a single comment. The people replying there are ignorant as hell."
    author: "Niek"
  - subject: "what I don't like in amarok"
    date: 2004-12-09
    body: "what I don't like in amarok is that it doesn't take into account path name when I make a search in my files, and most of my files are poorly tagged. Also every time I restart it I need to wait 10 minutes for it to rescann my files (more than 9000, half of them are from an smb connection) which not the case with Juk and kaffeine. Other than that it's great"
    author: "Pat"
  - subject: "Re: what I don't like in amarok"
    date: 2004-12-09
    body: "I don't really understand what you mean here, but if you submit bug reports against amaroK at bugs.kde.org, we'll fix them.\n\nWRT to rescanning, it only rescans if something has changed in your collection folders. You can turn off monitor changes if you like and it'll never rescan, if you prefer."
    author: "Max Howell"
  - subject: "Komplaint"
    date: 2004-12-13
    body: "My only complaint isn't even amarok's fault, it is Mandrake's fault - their package of amarok can't lookup tags on the internet.\n:)"
    author: "spikeb"
---
OSNews.com has posted an <a href='http://osnews.com/story.php?news_id=9105'>interview with the developers</a> of <a href='http://amarok.kde.org'>amaroK</a>. The interview was conducted by Christian Schaller from <a href='http://gstreamer.net'>GStreamer</a> and covers the cooperation between the GStreamer project and KDE, and it provides an overview of the history and future prospects of amaroK development.




<!--break-->
