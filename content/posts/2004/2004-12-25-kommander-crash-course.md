---
title: "A Kommander Crash Course"
date:    2004-12-25
authors:
  - "jriddell"
slug:    kommander-crash-course
comments:
  - subject: "i was always courious..."
    date: 2004-12-25
    body: "...why the kommander guys have never thought about making a kscript engine for themselves.  We can already embed bash and Javascript into KDE applications, kommander would be a neat addition.  Currently Kate is the only application that actually has enough dcop interfaces to make KScriptInterface very useful, but I am sure with minor effort more applications can have this tool too.  To use KScriptInterface in your application, just have a dcop interface for automation, and slide in the entire 5 lines (including the code that will show scripts in the menu) into your application.\n\nFYI KScriptInterface is an interface that allows people to add scripting languages to KDE applications in a generic manner.  The goal was to remove the holy war of language X is the official KDE scripting language. DCOP provides the automation hooks, and KScriptInterface provides a nice script manager/loader/executer.  So if any of you language fanatics out there have dcop support for your pet, please don't hesitate and email me about adding a KScript engine for it.\n\n"
    author: "Ian Reinhart Geiser"
  - subject: "Re: i was always courious..."
    date: 2004-12-26
    body: "Crazy! Why Ian doesn't keep us up to date on all this is another good question. ;-) I had asked Rich Moore about using KJS Embed so that we could work with Javascript too, which is Ian's baby also. It looked like primary scripting benefit there can be largely achieved from console. Anyway that is one of the things on our extremely overly long do list, and usefulness beyond Javascript is news to me. I've been ranting about getting away from codifying a pet language for some time now, but I've also wanted to insure we had optimal Javascript support, for which this is the obvious choice.\n\nWhat I'm absolutely certain of is that we both have cool stuff the other doesn't know about. For instance Kommander has had bash embedded since the start. Additionally one of Kommander's primary stated goals has always been to be language neutral and be able to use any language. In fact you can do this now and we're improving it. The fact that Kommander is in fact internally DCOP driven and can make calls to mini scripts by simply headering the Kommander text area means that it is at the frontal assault in offering GUI to users of any scripting language. In many ways it is being extended to also compensate for lack of DCOP binding...\n\nHowever having said all that the simple fact is that so far every idea in tackling all of these efforts has fallen short of nirvana. So I would be very interested looking at what you've got Ian. You know how to get a hold of me. We may be doing some things differently and some of the same things. I'm not sure how compatible various things we are doing is but I do know that it's really hard to keep up with everything happening. So please point me to where I can get a good overview and evaluate this as I've always intended to make use of at least some of what you're doing in Kommander anyway. All of what we're both doing offers a very interesting diverse potential for developers and users to get excited about.\n\nBTW Merry Christmas Ian;-)"
    author: "Eric Laffoon"
  - subject: "Re: i was always courious..."
    date: 2004-12-28
    body: "its been there since KDE 3.0 ;)\n\nim on the road now, but when i get back ill give you a crash course."
    author: "Ian Reinhart Geiser"
  - subject: "Re: i was always courious..."
    date: 2004-12-30
    body: "True, true.\n\n\nRekall (http://www.rekallrevealed.org) supports multiple scripting languages, and has a mostly-complete (but not yet released) KJS scripting interface. BUT, apart from Ian's tutorials (which, unless I've missed something, never got to the \"how to embed KJS with needing KJSEmbed\" stage) I had to figure it all from the code (which is why its not released yet :)\n\n\nI know people prefer to code than document (me as much as anyone else) so I'm loathe to grumble, but we really do need more documentation on a lod of this sort of stuff!\n\n\nMike\n"
    author: "mikeTA"
  - subject: "Standard kde apps possible?"
    date: 2004-12-26
    body: "Hi there and merry x-mas everybody!\n\nI've got lots of ideas for useful utilities I want to write\nfor KDE. 2 years ago when I was still using Windows I wrote\nlots of small applications in Delphi. Since I've switched to Linux\nI made some attempts to begin writing apps again but no\nway of development was pleaseant enough for me to start writing apps\nagain. Sometimes I miss the ability to quickly create an application.\nBecause I'm a perfectionist there are some serious disadvantages\nin current development solutions for Linux:\n\n1) I prefer KDE. Gnome is just too much dumbed down IMHO. So I want\nto create apps which use the QT/KDE widget set and integrate with\nKDE well. Kylix, Lazarus/FreePascal all do not support writing\nKDE apps well\n2) KDevelop is a really good IDE. But I just don't like writing C++ apps.\n(This is no trolling! - just my very humble ;-) opinion). Compiling\ntakes ages on my admittedly not very up-to-date PC. If you once used\nDelphi you keep wondering why compiling a Hello World app in C++ takes\nlonger than compiling a 80,000+ lines application in Delphi. Error \nmessages where quite confusing more often than not and I didn't really\nlike the syntax though I think I'd get used to it if it only compiled\nfaster.\n3) I don't like Python because of its indentation. I know there are\ndifferent opinions on this - but I just think this is horrible - please\ndon't try to convince me - it won't work ;-)\n4) Ruby is a cool language. I like it a lot. Together with KDevelop\nthis is getting close to being useful. I have no problems with using\nan interpreted language but I don't like the fact that I force people\nto install Ruby just to run my apps. And I don't want to learn this\nif PHP-QT might just be around the corner. Because I must use PHP\nat work a PHP-QT would be the easiest way to begin programming again\nat home. What's missing is a way to somehow \"mix\" QT-Designer and\nKDevelop like in Delphi. Perhaps this is implemented in the mean\ntime but it wasn't the last time I checked.\n5) FreePascal would be way cool: Runs on different processors, compiles\nvery fast, etc. - but no QT bindings :-(\n6) Kommander: I couldn't figure out yet if there is an easy way to\ncreate a full-fledged KDE app like you can in KDevelop with C++.\nThis would be way cool.  Is there an example which creates a\nHello World app with menu bar, internationalization, toolbar \nand configurable shortcut keys and About Box - ie. an app which looks \nlike an ordinary KDE app? That would be really raise my interest?\nIf not, I'm wondering what's still missing to get there?\n\nWell, for the moment I'm waiting for PHP-QT... A long wait perhaps.\nI'm dreaming - some day there might be the ability to write PHP apps which\nwork on Linux and Windows just like it works for web applications\nright now. This would even be a MAJOR reason for the company I'm working\nin to consider buying a Windows QT license.\n\nGreetings\nMartin\n\n\n\n\n\n\n\n\n"
    author: "Martin"
  - subject: "Re: Standard kde apps possible?"
    date: 2004-12-26
    body: "Maybe the best solution for all the problems is the support for mono/dotGNU/.NET in KDE. Once you get support for that, you don't need to do more but, to delegate the language support to them.\n\nI read some time ago that it was progressing (the support of mono in KDE & kdevelop), but haven't read anything about it since some time ago, how is it going?"
    author: "Edulix"
  - subject: "Re: Standard kde apps possible?"
    date: 2004-12-28
    body: "I have thought of this problem a lot, too.\nIt seems that mono cannot implement .NET completely because Microsoft holds a closed license for ASP.NET which is an important and very cool part of .NET.\nIt allows easy visual design and easy debugging for server side web applications. Is there anything similar coming in the open source world?\n\nAnyway, I don't like the idea of being stuck with a Microsoft controlled system, but it would allow running practically all Windows progs in the future in Linux.\n\nI feel exactly like Martin in the original message. I am also working with Delphi and love it, and I dream of perfect tools and environments.\n\nJuha\n"
    author: "Juha Manninen"
  - subject: "Re: Standard kde apps possible?"
    date: 2006-01-19
    body: "Very late post here... but had to comment that Mono has had full ASP .NET support for quite some time (Don't remember if it did at the time of the original post... but I think so).  Since when did \"closed\" mean \"unhackable?\" :-P."
    author: "SigmaX"
  - subject: "Re: Standard kde apps possible?"
    date: 2004-12-27
    body: "Similar situation. I'd like to have KDE GUIs for my programs but Haskell is my language of choice now, there are GTK and Wx bindings but no Qt nor KDE ones. I don't think there are even bindings for any functional language. Now OOP, I wouldn't mind Object Pascal or Eiffel, but I absolutely hate C++ (even though I admit it's a great tool for some purposes). I thought about learning more about python but I really miss static typing...\n\nI hope Kommander enables me to at least do some simple GUIs for my apps using the language of my choice. I'll take a deep look at it as soon as I have some available time."
    author: "John Programming Laguages Freak"
  - subject: "Re: Standard kde apps possible?"
    date: 2004-12-27
    body: "They are writing Qt bindings for FreePascal/Lazarus, but it looks like it's going to be hard and will take a while.\n\nI don't like C++ much but it seems like the only choice sometimes. Oh well..."
    author: "Elio"
  - subject: "Re: Standard kde apps possible?"
    date: 2004-12-27
    body: "Well there is Perl::Qt, Perl is hopefully not too different from PHP.\nhttp://perlqt.sourceforge.net/"
    author: "Ted"
  - subject: "Re: Standard kde apps possible?"
    date: 2004-12-27
    body: "I think that Java/Qt could be a contender, especially if you compiled your program using GCJ. With Qt substituting for a lot of the JDK you could get away with not using it, and you could consider using the ClassPath project if you were really stuck.\n\nUnfortunately, the Qt bindings for Java seem to come with a very basic sample program and that's it. I'd love to know how Java's String and Qt's QString are meant to fit together, and how the whole magic behind Signals, Slots and QObjects gets handled.\n\nI often wonder if any of the bindings guys could be persuaded to do a series of \"Hello World\" tutorials for their Qt and KDE bindings in all the supported languages, with tips on how to setup and use the appropriate compiler etc. for each. "
    author: "Bryan Feeney"
  - subject: "Re: Standard kde apps possible?"
    date: 2004-12-28
    body: "Lack of documentation is one of the big problems, but that's something that's relatively easy to solve, even if it's not just Qt/Java that needs to be documented, but also gjc itself. Still, it's doable, as I've shown in \nhttp://www.valdyas.org/fading/index.cgi/hacking/javalib.html?seemore=y. gjc, classpath and Qt/Java do work, so it's just a matter of finding out how things are done.\n\nThe biggest problem, and one that would be beyond me to solve is integrating gcj and classpath in a KDE build system. In the end, you'd have bits of code in Java and other bits of code in C++, with all the bits compiled to native code and integrated into one set of libraries. And that must be automatable."
    author: "Boudewijn Rempt"
  - subject: "Re: Standard kde apps possible?"
    date: 2004-12-28
    body: "\"Unfortunately, the Qt bindings for Java seem to come with a very basic sample program and that's it.\"\n\nThere are quite a few of the Qt example programs translated to java under kdebindings/qtjava/javalib/examples, and the cannon game tutorial is under qtjava/javalib/tutorial. For kde there are examples under kdebindings/kdejava/koala/examples\n\n\"I'd love to know how Java's String and Qt's QString are meant to fit together..\"\n\nEverywhere in the C++ api where a QString is expected, you pass either a java String or a StringBuffer (if the QString wasn't const).\n\n\"..and how the whole magic behind Signals, Slots and QObjects gets handled.\"\n\nThe is a C++ class called 'JavaSlot' which has an variable containing a java target instance, and a lot of slots all called 'invoke()' - they have all the possible combinations of arg types that are used in the Qt library. These invoke() slots are called by the Qt C++ runtime just like any other slot, and they in turn call the actual java slot via JNI.\n\nThe most useful thing is that you don't need to use a moc preprocessor, and you use java arg types in the slots/signals connections. So you never see ugly stuff like 'const QString&' in SLOT() or SIGNAL() calls."
    author: "Richard Dale"
  - subject: "Re: Standard kde apps possible?"
    date: 2004-12-28
    body: "Unfortunately, there's probably not going to be a solution that doesn't involve installing some kind of runtime onto a user's system. Except for C++, you will always need the Qt and KDE bindings themselves, and you will need the runtime for the program, unless you can find some way to bundle the two together, or find something that generates native code like C++.\n\nWith that in mind, I would say that there are four major possibilities, and you're not going to like any of them.\n\n1 - Java. There are Qt and KDE bindings for Java in kdebindings, and apparently they work quite well, although I've not got around to using them yet. Requires installing a complete JRE, which isn't exactly small. Alternatively, if you can get it to work with GCJ, you can use that instead.\n\n2 - Mono. As with Java, including in kdebindings. Requires installing the Mono runtime, which is smaller than the Java runtime but not by much. Mono's C# compiler is certainly a lot faster than GCC's C++ compiler.\n\n3 - Gambas (http://gambas.sourceforge.net/). It's closer to Visual Basic (yes, I know) than it is to Delphi, but at least it's in the right area. At the moment, there isn't a stand-alone runtime that I'm aware of, so you need to distribute the whole of Gambas. It's being worked on.\n\n5 - An interpreted language, like Perl, Python, Ruby, PHP... If you already like and have used Ruby, go with that. Yes, that would require the users to install some kind of runtime, but there isn't really a solution that doesn't require that. KJSEmbed is also good, and probably installed by default with any KDE 3.2 or 3.3 installation."
    author: "SD"
  - subject: "Re: Standard kde apps possible?"
    date: 2004-12-30
    body: "Reading your comment and its responses I feel that there are many of us ex-delphians (or in progress to be ex-dephians) el at doing rounds around KDE.\n\nKDE is the right platform to move to, but its development tools are still somehow lacking (at least for us \"soft\" hackers). When KDE finally delivers on that topic there will be a flood of programmers doing at first weekend gadgets and then more serious development.\n\nI follow the development of Kommander, KDevelop, Kexi, KDE bindings and other apps, libs and languages and try them periodically (and send bug reports, wishes, etc) looking for the right combination of tools for me.\n\nBy now I'm heading to use KDevelop, and looking for:\n- Support for easy/modern/light programming languages (C#, D, ruby, FreePascal?, others?).\n- KDE bindings for those languages.\n- Integrated visual form design (KFormDesigner?).\n\nDoing casual software development is still not possible on KDE. Development is hard. Software development on KDE needs to be as easy as is *using* KDE now (somehow, as easy as is doing it with Delphi now, for instance), isn't it?\n\nMRC \n\nPS: Please forgive my bad english :)\n"
    author: "ochnap2"
  - subject: "Re: Standard kde apps possible?"
    date: 2006-04-06
    body: "Note that at moment the Free Pascal DOES support Qt. You can use Lazarus and link your apps against qt4.\n\nwww.lazarus.freepascal.org"
    author: "Luan Carvalho Martins"
  - subject: "Python"
    date: 2004-12-29
    body: "Lets not forget python. Python has PyQt, and is easily embeddible. The nice thing is that you can have classes.\n"
    author: "PITA"
  - subject: "Perl has classes, too"
    date: 2005-01-02
    body: "Perl has classes in its own kitchen-sink manner.\n\nPerl is a prelly cool OO environment to work in, if you impose your own programming discipline (no static typechecking, no real encapsulation of any kind).\nPersonally I'm quite undisciplined, andbeing able to do real typechecking and encapsulation is the main reason I find OO useful in the first place, but you can't dismiss Perl for not having classes, I think most serious Perl programming is class-based nowadays."
    author: "rp"
  - subject: "Reconsider OWL as XP_oop_approach open source code"
    date: 2004-12-31
    body: "You need C to gain insight into Windows XP which is essentially a windows 3.1 .\nAny KDE-application needs not only to plug in XP(Your example is Kate what\nis for me more a simple editor compared to emacs) but to control the source code\nof XP ... . Otherwise You are really stucked with XP as the scandinavian said."
    author: "llllll"
  - subject: "wow, there are really confused ppl around here"
    date: 2005-01-04
    body: "besides me. I didn't quite get all of that (heck what do you mean by OWL? this http://www.olegvolk.net/olegv/macro/birds/owl.JPG one? )\nYou are talking about KDE-Cygwin I guess... I think plugging in is enough because QT hides the windows API so KDE doesn't have to give a damn about that cra...code. ONce they have made a new release, I guess you'll even be able to replace the shell explorer.exe with startkde.exe .. which makes me smile hellish and evil ... MUAAAH MUUAAAAAAAHH MUAH ;====)"
    author: "thatguiser"
  - subject: "Python without Grail pray of OWL?META-RE:CygnusWin"
    date: 2005-01-04
    body: "Advanced OWL 5.0 ... by Ted Neward. A second book by the same author I have too.\nAs You said there are some new Releases. 2003 and later:..December 2004 . 2 CD' s\nare included in the books. I watch them and other CD' s for recent co-ordinates. The\nOWL-folks had trouble with Borland( terminated C-Compiler cooperation ). Borland has\n- thats my META-REPLY - taken now religious fundamentalists on board.."
    author: "llllll"
---
<a href="http://www.kde-apps.org/content/show.php?content=12865">Kommander</a> is a tool for rapid application development and the easiest way to make applications in KDE. <a href="http://applications.linux.com/article.pl?sid=04/12/17/2033227">"A Kommander crash course"</a> is a <a href="http://applications.linux.com/applications/04/12/23/1617252.shtml">two part</a> tutorial for Kommander which takes us through creating a simple application for changing your KDE wallpaper. Look out for more Kommander tutorials coming in the next couple of weeks. 






<!--break-->
