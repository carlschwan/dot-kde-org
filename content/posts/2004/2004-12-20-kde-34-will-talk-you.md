---
title: "KDE 3.4 Will Talk to You"
date:    2004-12-20
authors:
  - "oschmidt"
slug:    kde-34-will-talk-you
comments:
  - subject: "Pretty cool"
    date: 2004-12-20
    body: "Hi.\n\nThis sounds (easy joke) very cool to me. Accesibility is a very important goal to me, but accesibility features are not only useful for people with disabilities. I use the text zooming in konqueror a lot when I want to read something, and I'm very separated from the screen. Now I hope that with ktts I can still read websites or emails while I'm tidying my room or having a shower. ;-)\n\nI'm sure other geeks will find other interesting uses to this features.\n\nThanks!"
    author: "suy"
  - subject: "The HAL factor"
    date: 2004-12-20
    body: "Accessibility aside, you can't beat the coolness factor having your computer talk to you:-) Using ktts to speak the messages from knotify sounds like fun.\n\nOne question tho, since I almost always have music plying. Can you set ktts to fade the volume from the mediaplayer (noatun/juk) to like 25% of current volume before it speaks and turn the volume up afterwards? I don't think playing it on top of the music without doing anything with the volume or temporary stopping it would not give anything particularly comprehendable. "
    author: "Morty"
  - subject: "Re: The HAL factor"
    date: 2004-12-20
    body: "file it as a wish on bugs.kde.org... the sooner, the better. should be easy to do via dcop..."
    author: "Mathias"
  - subject: "Re: The HAL factor"
    date: 2004-12-20
    body: "Especially with JuK and Noatun having a similar dcop interface."
    author: "mETz"
  - subject: "Re: The HAL factor"
    date: 2004-12-20
    body: "The coolness factor is the kind of thing Microsoft uses to sell you their soft (despite how much cooler KDE is).\nSaid that, it's a good to have feature for practical reasons, specially for accesibility ones, and I'm glad this is the path chosen by KDE developers."
    author: "Shulai"
  - subject: "Re: The HAL factor"
    date: 2004-12-21
    body: "> you can't beat the coolness factor having your computer talk to you:-)\n\nAgreed, but, I want to be able to talk to it.  \n\nIBM's ViaVoice was fairly good.  I have a (oldish) copy of ViaVoice from my MDK 8.0 Pro CD's (and for whatever reson the CD that ViaVoice is on can't be read... :-\\ - I'm on Gentoo these days anyway).  ViaVoice worked when I used it last.  I want to be able to have a mic sitting in front of me and be able to say \"Computer play eno\" and have music play.  Can I do this yet?\n\n"
    author: "Xanadu"
  - subject: "Re: The HAL factor"
    date: 2004-12-21
    body: "> IBM's ViaVoice was fairly good.\n\nI agree. The problem is that IBM still refuses to make ViaVoice available for Linux endusers.\n\nThe speech recognition part is not available at all for Linux. The speech synthesis part of ViaVoice is available on Linux for about half of the languages that are supported on Windows, but only if you buy 300 copies at once for $1500 altogether."
    author: "falonaj"
  - subject: "Re: The HAL factor"
    date: 2004-12-22
    body: "I think that \"talking to my PC\", with the virtual reality stuff, was the biggest and most useless hype of the past years.\nInterfaces should be completely differents to be really usable and useful.\n\nAnd anyway, even if only used as text typing substitute, let's think about the chaos that would be generated in a tipical office..."
    author: "Davide Ferrrari"
  - subject: "Re: The HAL factor"
    date: 2004-12-30
    body: "SO what keeps you from talking back? Use Sphinx and Perlbox Voice (perlbox.org). Installing Sphinx is really trivial and Perlbox will do the rest. Also, Perlbox has KDE desktop integration. Works like a charm to me..."
    author: "Josephine"
  - subject: "Re: The HAL factor"
    date: 2004-12-21
    body: "But what if you want ktts to do karaoke? You won't want it to fade the volume down."
    author: "Miguel"
  - subject: "some tts reqs"
    date: 2004-12-20
    body: "as someone who works often with tts engines, please please please include support in the api for following:\n1) dictionary file for correct pronounciations.\n2) string substitution (for example \"AFAIK\" to \"as far as i know\")"
    author: "rands"
  - subject: "Re: some tts reqs"
    date: 2004-12-20
    body: "The latter is already supported using the SSML API ;)\n\n<speak>\n <p>I went down to 32 chocolate <pron sub=\"street\">st.</pron></p>\n</speak>\n\nThe issue with automagic string replacement is that it may not expected by the user, but this could indeed be a nice feature..\nFile a wish and we'll see.\n"
    author: "Paul"
  - subject: "dict.leo.org"
    date: 2004-12-20
    body: "I don't know much about this stuff, but perhaps you can support an api to connect to internet servers like \nwww.leo.org\nThis is a big online dictionary provided by the technical university of munich (TU),\nuse this link to have a look http://dict.leo.org/?lang=en&lp=ende&search=\n\nYou can search for english and german words and there's also a french dictionary online. After searching for a word you can have it pronounced by clicking on the speakers symbol.\nIt's a nice feature and perhaps there is a possibility to automatically connect to the server, download the sound files and have them played.\n\nGreetings\nAndy\n\n\n\n"
    author: "Andy"
  - subject: "Re: dict.leo.org"
    date: 2004-12-22
    body: "Department of Informatics / Technical University of Munich\n=\nFakult\u00e4t f\u00fcr Informationstechnik / Fachhochschule M\u00fcnchen\n\nFakult\u00e4t f\u00fcr Informatik / Technische Universtit\u00e4t M\u00fcnchen\n=\nDepartment of Computer Science / Munich University of Technology\n\n\nEgal was auf leo.org bzw der Drehscheibe stehen mag... ;-)"
    author: "TUler"
  - subject: "back end..."
    date: 2004-12-20
    body: "ktts is a really good looking project.\ndoes anybody know the status of the backends?\ncan any of them produce a good voice?"
    author: "Mark Hannessen"
  - subject: "Re: back end..."
    date: 2004-12-21
    body: "The new Festival voices, i'm told, sound _very_ natural."
    author: "Paul"
  - subject: "Non-english"
    date: 2004-12-20
    body: "Pretty cool feature, but what about non-english languages, like danish, german, spanish, japanese, icelandic and so on...\n\nKDE is in soooo many languages, will it continue to be that in speech?\n\nJust my 0,02 kr.\nKalna"
    author: "Kalna"
  - subject: "Re: Non-english"
    date: 2004-12-20
    body: "Festival supports english (British and American), Spanish and Welsh text to speech, Epos supports Czech and Slovak (read the article). These are the text to speech engines that ktts uses.\n\nThere are probably non free tts engines that are available in other languages.  I wonder if it is possible to use them?\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Non-english"
    date: 2004-12-20
    body: "Also German is supported via Hadifix (txt2pho plus MBROLA) and via IMS German Festival mod.  Finnish is supported via Festival.  See the KTTS Handbook for details.\n\n  http://accessibility.kde.org/developer/kttsd/\n\nBesides the dozens of other languages needed, conspicuously absent are French and Italian.  If anyone figures out how to get these languages working, please email your links and instructions; I'll put them in the KTTS Handbook.\n"
    author: "Gary Cramblitt (aka PhantomsDad)"
  - subject: "Re: Non-english"
    date: 2004-12-20
    body: "AT&T used to sell their natural voices packages that supported English (UK & US), French, Spanish (ES), German, and Korean.  There is a Linux client available, though the software is harder to find nowadays since AT&T stopped supporting it.  This is non-free software.  A company called Scansoft also makes a variety of engines: English, French (FR & CA), Spanish, Dutch, German, Japanese.  I'm not sure if they have Linux versions of these engines (I've only seen Windows clients).  The AT&T one, at least, should not be hard to plug in, and sounds much better than the free TTS engines."
    author: "rands"
  - subject: "Re: Non-english"
    date: 2004-12-21
    body: "I think you're talking about IBM ViaVoice?  To properly support it, we need a permanent licensed copy.  Anyone care to donate?\n"
    author: "Gary Cramblitt (aka PhantomsDad)"
  - subject: "User speech recognition?"
    date: 2004-12-20
    body: "So how about adding capability to control the computer via speech too (on blind support for kde-4.0)?\n\nOpen Mind Speech <http://freespeech.sourceforge.net/>\nOSSRI <http://www.ossri.org/>\nCMU Sphinx <http://www.speech.cs.cmu.edu/sphinx/Sphinx.html>\n\nand others <http://linux-sound.org/speech.html> come to mind... If one could be supported/used, which one? ;-)"
    author: "Nobody"
  - subject: "Annoying.."
    date: 2004-12-22
    body: "It is annoying already have to read so many \"K\", now we will hear them.\n\n"
    author: "DebianUser"
  - subject: "Re: Annoying.."
    date: 2004-12-22
    body: "On Gentoo, you can remove all the K's.  Simply add a sed filter to the build chain.  Works like a charm.  I recommend you switch!"
    author: "GentooUser"
  - subject: "Congratulations from the fundator"
    date: 2004-12-22
    body: "I am the fundator of Proklam, latter renamed to KTTSD (by me) and now it seems, it was renamed to KTTS. And I say C O N G R A T U L A T I O N S! Keep the good work!\nWhen someone asks me what's good about free software, I always mention this, I founded the project but I couldn't finish it, and someone else picked it and it's evolving, it's awesome, Long live to free software!!!"
    author: "Pupeno"
  - subject: "Re: Congratulations from the fundator"
    date: 2004-12-23
    body: "hey man! thanks for Proklam!! it's also good to be able to thank directly the guys that make your computer run :)"
    author: "c0p0n"
  - subject: "Woot! :D"
    date: 2004-12-22
    body: "As a visually impared person(20/100) and huge KDE nut I think this is really good news :)\n\nI tried oralux 0.6a and was sort of dissapointed. Emacspeak would be nice it you took away emacs and replaced it kwrite :) Then again oralux sounded better then my attempts at getting festival to spit out ogg's via the console.\n\nIf I go totally blind one day I have faith that I will be able to pop in my live distro dvd with the entire project gutenberg collection and listen my ears out :) I think it will be cool when people who are totally blind now can do the same too! ^_^"
    author: "Nintenduh"
  - subject: "Support for the blind"
    date: 2004-12-23
    body: "Support for blind users sounds sweet. Is there any information available yet on how this will be accomplished?"
    author: "Canonymous Oward"
  - subject: "Re: Support for the blind"
    date: 2004-12-25
    body: "What would be _really_ nice is a OCR system for KDE which actually works properly. I have a friend who is slowly going blind. He now cannot read ordinary text, and finds life a bit shallow. I know there are commercial packages which work, but my friend is does not have a private income and cannot afford any of them. Anybody know of a solution?"
    author: "Christopher Sawtell"
  - subject: "Re: Support for the blind"
    date: 2004-12-25
    body: "Have you tried Kooka's ocr before?\nhttp://www.kde.org/apps/kooka/"
    author: "Nintenduh"
  - subject: "Re: Support for the blind"
    date: 2004-12-25
    body: "The free versions of SUSE come with an OCR system, that seems to be commercial, but works very well."
    author: "Z_God"
  - subject: "Re: Support for the blind"
    date: 2004-12-26
    body: "> Is there any information available yet on how this will be accomplished?\n\nThe Qt-toolkit used by KDE already has support for screen readers on Windows and Macintosh. The next version (Qt4) will also support the Linux screen reader Gnopernicus, and a number of other assistive technologies.\n\nWe are closely cooperating with the GNOME project to create common standards for accessibility purposes.\n\nWe will make more information about this available during the next weeks.\n\nOlaf"
    author: "Olaf Jan Schmidt"
  - subject: "Better use of time"
    date: 2004-12-30
    body: "It would seem to me that a better use of development time would be things that make Linux more user friendly for the masses.  Things like software installation for example.  Less time needs to be spent on \"foo-faa\" features like rotating background software while blending it with a webpage or yet another screen saver option.  Don't get me wrong, KDE talking to you is going to be very cool, but there are more important things that need to be fixed first.  In reality most of the common tasks that people do in Windows are too difficult on Linux for the end-user.  Microsoft will rule the world until a more concentrated effort on making KDE (or Gnome) a really usable environment for Linux and not just some \"cool thing\" for geeky types to play with."
    author: "Chris"
  - subject: "Re: Better use of time"
    date: 2004-12-31
    body: "KDE talking to you is more about accessibility than having a toy to play with. It is very important for those who need it."
    author: "Paul Eggleton"
  - subject: "French Perlbox release"
    date: 2007-07-27
    body: "I built a french version of perlbox which uses espeak instead of festival.\nThe advantage of espeak is that it supports many more languages than festival actually does. I love Festival and Sphinx which helped me starting with voice synthesis / recognition but years passed and still nothing really usable in the straightforward way.\n\nSo to use perlbox voice french (which can be modified to be used with other languages) we need :\n\n- Perl\n\n- Espeak that you will compile from sources, packages included in distros are generaly outdated\n\n- Sphinx 2 in it's simplest version, no matter if in English\n\n- extract the archive you downloaded in the directory /tmp\n- as root enter in the newly extracted archive named perlbox-voice-fr-1.0\n- launch ./install.pl\n- click ok\n\nfinished.\n\nLogout from your root account and launch ./perlbox-voice\nEnjoy\n\ndownload from http://www.r-kraft.com/perlbox-voice-fr-1.0.tar.bz2\n\nor from http://perlboxfr.tuxfamily.org/\n\nyou may have a qucik view of this version at working in it's very alpha version, so it wasn't able to identify every words but only few of them, for now it has evolved so it should be usable even with complex words http://www.dailymotion.com/relevance/search/rkraft_fr/video/x2meug_dscf1411_tech\n"
    author: "Pierre"
---
The KDE Accessibility team is in the process of integrating speech synthesis into KDE.  Not only does this mean better support for visually-impaired and speech-impaired users, but the new features should also prove for a fun desktop experience overall. An important milestone has been reached with the recent release of the <a href="http://accessibility.kde.org/developer/kttsd/">KDE Text-to-Speech System (ktts)</a>. If you wish to learn more about speech synthesis support in KDE, you can also read an extensive <a href="http://accessibility.kde.org/about/ttsteam.php">interview with four developers</a> at the <a href="http://accessibility.kde.org/">KDE Accessibility Website</a>.

<!--break-->
<p>Both ktts and KSayIt (an application to read out longer texts) will be included in KDE 3.4. They are important additions to KMouth (an application for speech impaired people) and the other two assistive technologies in KDE, KMouseTool and KMagnifier.</p>

<p>The interview is part of a planned series of interviews with participants of the KDE Accessibility Project. Future interviews will cover other areas of accessibility in KDE:</p>
<ul>
<li>improved support for partially sighted people in KDE 3.4</li>
<li>support for blind users in KDE 4.0</li>
<li>foundation of the freedesktop.org accessibility initiative to find a consensus on a common speech driver API</li>
<li>close cooperation with the GNOME Accessibility Project to ensure that all assistive technologies will integrate into each desktop without problems</li>
<li>invitation of other accessibility projects to the last KDE conference, and participation in the FSG Accessibility Workgroup</li>
</ul>



