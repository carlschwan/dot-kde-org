---
title: "KDE CVS-Digest for October 22, 2004"
date:    2004-10-24
authors:
  - "dkite"
slug:    kde-cvs-digest-october-22-2004
comments:
  - subject: "OT: Novell Linux Desktop update"
    date: 2004-10-24
    body: "Recently there was some noise here about usage of GNOME programs in the Novell Linux Desktop, according to this Novell documentation:\n\nhttp://www.novell.com/documentation/nld/index.html?page=/documentation/nld/userguide_kde/data/front.html\n\nI just want to comfort you by letting you know the documentation on that link has been updated and now contains documentation for many KDE programs, like Konqueror, Kontact and Kopete, as well."
    author: "Arend jr."
  - subject: "Re: OT: Novell Linux Desktop update"
    date: 2004-10-24
    body: "troll!"
    author: "gerd"
  - subject: "Re: OT: Novell Linux Desktop update"
    date: 2004-10-24
    body: "Sorry, I don't understand your calling the OP a troll at all.  Please explain.  "
    author: "cm"
  - subject: "Re: OT: Novell Linux Desktop update"
    date: 2004-10-25
    body: "Erm, it's not a troll. He's just telling us that this is updated and so all the rubbish we've had is false."
    author: "David"
  - subject: "Re: OT: Novell Linux Desktop update"
    date: 2004-10-24
    body: "This is good. However, they have clearly stated before that most of their engineering efforts go to Evolution and Gnome - so what we really have here is a company with no strategy. It's never been a good (final) strategy to decide 'let's make it all!', and sooner or later this will change. \n\nThis was obvious though - as most of us are completely unable (not just reluctant) to fundamentally alter our views, i think it would have been unreasonable to ask Miguel & the gang to start thinking in KDE. So when it comes to SUSE, i guess you just have to take what you can from these guys -  that is, GNOME work. Let's hope that it will mean a *finally* working GNOME environment for SUSE users as well. I've yet to see that miracle..\n\nThis really leads me to another point about GNOME - i don't concider it as a free (as in beer) desktop anymore, in any way. What ever is downloadable from gnome.org is of alpha/beta quality - you really need the distributor (that charges you conciderable amounts of money) to turn it to a decent product. This is not to say KDE is perfect either, but conciderably better.\n"
    author: "anonymous coward"
  - subject: "Re: OT: Novell Linux Desktop update"
    date: 2004-10-24
    body: "Only Ximian has been making FUD.  Where is Ximian now?  Go look for yourself at how sad http://www.ximian.com/ is.  Look at the Ximian Desktop Code Log for an even better glimpse at just how dismantled the Ximian strategy is now http://codeblogs.ximian.com/blogs/desktop/.\n\nSUSE in contrast has hired people to improve the KDE desktop such as OOo KDE integration.  It looks like Kopete and Kontact also have Novell Groupwise support although I don't know how that happened whether the fantastic KDE developers or SUSE/Novell's help.\n\nFor GNOME not being free, you can order Ubuntu for free and no money at all to get the CD.  Contrast this to KDE where it's a much bigger pain and if you want a CD you have to pay or download and burn yourself."
    author: "ac"
  - subject: "Re: OT: Novell Linux Desktop update"
    date: 2004-10-25
    body: "Suse is searching a KDE developer for Groupwise integration.\nhttp://www.suse.de/de/company/suse/jobs/suse_pbu/developer_kde.html"
    author: "Anonymous"
  - subject: "Ummm ?? Novell???"
    date: 2004-10-25
    body: "You talk about Ximian and SuSE like they are different companies.\n\nNovell owns them both, and down the road they will likely be merged into novell Linux. \n\nAlso, Novell did indeed fund the Groupwize support in both Kopete/Kontact *and* Evolution/Gaim.\n\n"
    author: "Jason Keirstead"
  - subject: "Re: OT: Novell Linux Desktop update"
    date: 2004-10-26
    body: "> For GNOME not being free, you can order Ubuntu for free and no money at all to get the CD. Contrast this to KDE where it's a much bigger pain and if you want a CD you have to pay or download and burn yourself.\n\nKalyxo project is working on something similar only with KDE, but they're not there yet. They need all the help they can get.\nhttp://www.kalyxo.org/\n\nNote: I'm not affiliated with Kalyxo."
    author: "another ac"
  - subject: "Re: OT: Novell Linux Desktop update"
    date: 2004-10-24
    body: "> GNOME - i don't concider it as a free (as in beer) desktop anymore, in any way\n\nCould this have something to do with the LGPL license? As far as I am concerned, the GPL seems cleverly designed, it protects the open source community.\n\nWith LGPL:d toolkits, it just seems to easy to take the big cake out of the open-source, and then outperform the same with economic muscles. They can eat the cake and still have it."
    author: "KDE User"
  - subject: "Re: OT: Novell Linux Desktop update"
    date: 2004-10-24
    body: "\"However, they have clearly stated before that most of their engineering efforts go to Evolution and Gnome\"\nI never heard something like this before. And I do not belive this is true.  Novell bought Ximian because of RedCarpet and Mono. And SuSE did a lot of KDE support recently. So I think your statement is just bad press from some GNOME supporters.\n"
    author: "Birdy"
  - subject: "Re: OT: Novell Linux Desktop update"
    date: 2004-10-25
    body: ">>\"However, they have clearly stated before that most of their engineering\n>>efforts go to Evolution and Gnome\"\n>I never heard something like this before.\n\n\"Novell's desktop software employs the GNOME user interface and software, but it will also include that of rival KDE, McLellan said. However, Novell's integration work is happening only with the GNOME applications, she said.\" \n\nhttp://news.com.com/Novell+to+release+enhanced+Linux+in+fall/2100-7344_3-5300555.html?tag=nefd.top\n\nIt was just bad press from the Ximian camp of Novell. These guys should really talk less and code more.. ;-)"
    author: "Anonymous"
  - subject: "Re: OT: Novell Linux Desktop update"
    date: 2004-10-25
    body: "Googling for \"Christine McLellan\" who supposedly made the above quoted statement indeed reveals that she's a former Ximian member..."
    author: "Datschge"
  - subject: "Re: OT: Novell Linux Desktop update"
    date: 2004-10-25
    body: "http://www.eweek.com/article2/0,1759,1679393,00.asp\n\nQuote: \"Evolution is where we're putting our engineering efforts,\""
    author: "anon"
  - subject: "Re: OT: Novell Linux Desktop update"
    date: 2004-10-25
    body: "> \"Evolution is where we're putting our engineering efforts,\"\n\nThis is kinda funny. They're helping their biggest competitor ( redhat ) just where they're most vulnerable."
    author: "foo"
  - subject: "Re: OT: Novell Linux Desktop update"
    date: 2004-10-25
    body: "Another quote from the same article: \"The Novell Linux Desktop, though, will not include Evolution 2, and instead will ship with Novell's Connector for Microsoft Exchange Server, letting early adopters use it with Microsoft Corp.'s Exchange mail servers.\" Makes sense... *scratches head"
    author: "ac"
  - subject: "Re: OT: Novell Linux Desktop update"
    date: 2004-10-25
    body: "Have to reply to myself. Serious tip for the Linux distributions: get a real consistent desktop strategy, and get it *now*. After that decision exists, please *all* (in your company) stand behind that decision for more than a month. How could the customers believe in your products, if you don't yourself?\n\nIt seems i'm not the only one thinking like this;\nhttp://linuxtoday.com/it_management/2004102201826OPDT\n\n\"No one in the commercial distro firms, it seems, knows what to do with the Linux desktop. They promote it, they laud it, but they don't take the hard steps to really get it out there. \"\n\nAnd if it was for SUSE that has been building popularity slowly but steadily, they couldn't possibly be that stupid to forget their current customer base, could they? Let alone the fact that KDE is getting awfully close to being incredible product..\n"
    author: "anonymous coward"
  - subject: "Re: OT: Novell Linux Desktop update"
    date: 2004-10-25
    body: "And make what decision? Distributions can only work with what the community produces.\n\nAre we asking for a top-down marketplace? I'm not. There is a big one ready to go in Windows, where you wait for the top to lead or follow. The natural monopoly because there is room for only one vendor. One vendor who will still be in business in 4 years.\n\nI like the free marketplace that has developed around free software. It isn't ordered, or even doesn't make sense, but there is opportunity for everyone. The most important metric isn't market share, but contributor share.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: OT: Novell Linux Desktop update"
    date: 2004-10-25
    body: "Yer. It was all bollocks, and bollocks we've had consistently for several years from you know who. The fact is that KDE is the primary desktop in Suse's desktop product and wee'll see what comes of NLD when it's released - i.e. both Gnome and KDE."
    author: "David"
  - subject: "Re: OT: Novell Linux Desktop update"
    date: 2004-10-25
    body: "The question is if Ximian should focus on Mono integration to KDe rather than evolution. As Suse ships KDE development support for Evolution means support for RedHat and other Gnome lockin distros. So I don't see a reason why Novell shall support Evolution. "
    author: "gerd"
  - subject: "KDE won't go away ... read more ..."
    date: 2004-10-27
    body: "Well the first question you should ask is who is Novell ? A company that has done what to Linux so far ? Besides that they bought Ximian and SuSE what exactly did they do ?\n\nBut anyways KDE is not going away regardless if Novell decides to go with GNOME and KDE is even more than just Evolution or some Groupwise or Mono stuff. We shouldn't ignore all the tools that people have written that makes daily life easier on KDE. I was now close up to start naming tools but this would be pointless. Most of the flames and rants or 'competition attitude' is done by people who prefer the one over the other rather than being able to explain in technical sentences why they prefer the one over the other. Most people I met so far (who mostly are the hardliners of defending their Desktop) are people who regulary count stuff like XChat, Gaim, Gimp and so on but a true Desktop shouldn't be limited on a handfull of meaningless applications. Applications that are good for one are useless for the other users. There are people like graphicans who need GIMP and there are other people who urgently need KStars, KLogo, Umbrello, Kivio do get their job done.\n\nIf we look over to the multitudinousness of what KDE has to offer an applications covering nearly every area of science, education, technics, physics, mechanics, biology, school, business then comparing all the tools offered or available for GNOME we then can easily say who has his nose infront.\n\nYou can't limit a Desktop on fancy icons or just because GTK is free or because it's written in C. No you need to value every aspect of a good Desktop this includes the Desktop itself, the architecture beneath it, the availability of third party applications that cover the needs of the users."
    author: "ac"
  - subject: "If linux survives against windows...."
    date: 2004-10-27
    body: "\nExactly.  If linux has made its presence felt \nin the face of killing strategy by Microsoft,\nso will KDE regardless of what Novell or SUSE\ndoes.  KDE had a big role to play in displacing \nMicrosoft from many a machines and rolling in \nMandrake and SUSE, and if they ditch KDE only \nthey will suffer.  It is always quality that \nmatters.  I see many places where konquerer \nis now mentioned as a quality we browser, whereas \nearlier people only knew or talked about \nmozilla/firebird in the open source world.\n\nASk"
    author: "Ask"
  - subject: "Thanks for the digest ^_^"
    date: 2004-10-24
    body: "Once more thanks, Derek :-)\nTalking about khtml, I love the new improvements. I just updated khtml and went to the only page that looks messed up on my system: www.skype.com (look at the 2 background lines not in place). Surprise!! It was fixed!\nNow finding some pages that don't render good in my everyday usage seems a very hard problem (;-).. Getting eagle-eye now.. ^_^\n"
    author: "koral"
  - subject: "Re: Thanks for the digest ^_^"
    date: 2004-10-24
    body: "I can help (with khtml from kde 3.3),\n\nFirst of all I agree completely that it is lovely that there is at least some\nactivity around khtml. And let me add to that that i sincerely hope that\nthey catch up with at least the standard set by Safari.\n\nNow some examples of pages that still do not render right.\n\nOne example is the page I used to compose this reply. (the form boxes\ndid not render completely until I clicked in them)\n\nIn addition, look to:\nhttp://www.nrc.nl (javascript menu's do not render at all)\nwww.interpolis.nl (no javascript menus)\n\nhttp://mail.beteor.nl/servlet/webacc (large white space on top of the page)\n\nwww.dpreview.com (javascript menus rendered so that you have to click at\na line of 1 pixel before the menus expand)\n\nI could go on (and yes, a long time ago I provided bug reports of most of\nthese errors)\n\nCould you confirm that these pages now render correctly?\n"
    author: "John van Spaandonk"
  - subject: "Re: Thanks for the digest ^_^"
    date: 2004-10-24
    body: "> Now some examples of pages that still do not render right.\n\nPlease don't duplicate http://bugs.kde.org on this site."
    author: "Anonymous"
  - subject: "Re: Thanks for the digest ^_^"
    date: 2004-10-24
    body: "The javascript problems are all solved when you change your browser identification to Mozilla or Safari. Guess where the problem is."
    author: "Wilco"
  - subject: "Re: Thanks for the digest ^_^"
    date: 2004-10-24
    body: "Wilco,\n\nI guess you did not actually try it?\n\nOr you are doing something else apart from changing\nthe user string.\nFor me changing to mozilla does not help \nwww.interpolis.nl at all.\nAt www.nrc.nl something is happening with the menus\nbut they are placed totally wrong.\n\nBut again, I am thankful for any improvements that are\nmade and this is not to complain at all!\n"
    author: "John van Spaandonk"
  - subject: "Re: Thanks for the digest ^_^"
    date: 2004-10-25
    body: "I certainly tried them out before commenting. The menus on both the Interpolis and NRC site work (and are usable) when identifying as Mozilla 1.6. I'm running KDE CVS, so perhaps KHTML has improved already since KDE 3.3. "
    author: "Wilco"
  - subject: "Re: Thanks for the digest ^_^"
    date: 2004-10-25
    body: "That's great to hear!\n\n"
    author: "John van Spaandonk"
  - subject: "Re: Thanks for the digest ^_^"
    date: 2005-02-08
    body: "Hoi John,\n\nVia jeeves vond ik mail van jou op zoeknaam 'Beteor'. Grappig he?\n\nPeter"
    author: "peter van dinther"
  - subject: "Re: Thanks for the digest ^_^"
    date: 2004-10-24
    body: "Too bad.\nNow Khtml gets better and better from version to version and there are plans to use the gecko engine as a kpart for konqueror.\n\nI hope khtml will stay alive and active as before.\nI love it!"
    author: "Heiko"
  - subject: "Re: Thanks for the digest ^_^"
    date: 2004-10-24
    body: "> Now Khtml gets better and better from version to version and there are plans to use the gecko engine as a kpart for konqueror.\n\nAs optional \"plugin\" - so why do you care?"
    author: "Anonymous"
  - subject: "Re: Thanks for the digest ^_^"
    date: 2004-10-25
    body: "I hope nobody says \"Why should we put so much work in two different html renderers? Let us focus on the gecko...\""
    author: "Heiko"
  - subject: "Re: Thanks for the digest ^_^"
    date: 2004-10-25
    body: "Don't worry, it seems that the start of the Mozilla/Qt project has awaken old and new khtml developers."
    author: "Anonymous"
  - subject: "Re: Thanks for the digest ^_^"
    date: 2004-10-25
    body: "For me the question arises why there should not be a safari port to windows :-) hehe."
    author: "gerd"
  - subject: "Thanks for the optimizations!"
    date: 2004-10-24
    body: "I know that I need them on my ancient hardware... And even on newer machines, efficient and clean code is always good :)"
    author: "Mikhail Capone"
  - subject: "Dcopwidgets"
    date: 2004-10-24
    body: "Offtopic, but... whatever became of geiseri's dcopwidgets?"
    author: "AC"
  - subject: "KimDaBa 2.0"
    date: 2004-10-25
    body: "KimDaBa 2.0 was released on October 18th.\nhttp://ktown.kde.org/kimdaba/news.htm"
    author: "Ian Monroe"
  - subject: "SUBVERSION"
    date: 2004-10-25
    body: "Wow ! I really enjoy reading those comments on the Digest.\n\nthanks got there is peoples thinking about what consequences such a change would make.\n\nhere is what I think, why not fixing CVS instead of switching the world to a new system which input so much risk.\n\nwe could simply sit on the problem and add a 'cvs mv' functionnality.\n\nI think a shell command to move the file is not enough, it breaks old history. like if you move some important files and then you want to retrieve KDE 3.1, you won't be able to get that file.\n\nI think a proper move would be \n\nshell 'cp old new'\ncvs delete \n\nso the old copy is available for older TAGs, and new project version continue to works with all file history.\n\nwould it be that hard to modify CVS and submit them back the patch ? \nwould not change much to the core system, and would fix this discussion definitely.\n\npeople would need to upgrade their client, once, in order to be able to use 'cvs mv' functionnality. that's it.\n\n"
    author: "somekool"
  - subject: "Re: SUBVERSION"
    date: 2004-10-25
    body: "What do you do when someone moves it back?  Or moves it to another file that previously existed?"
    author: "Burrhus"
  - subject: "Re: SUBVERSION"
    date: 2004-10-25
    body: "right, I guess we would need to handle that case to.\n\nwell, before we write, I guess CVS is able to know if there was a file before or not.\n\nin case where a new file is written over an old delete one, cvs could kinda \"reopen\" the file, and commit a new version to it. (even if completely different).\nso the old exist and the new one too. the only big question, is how to make the file unexistent between the too alive state.\n\nand I agree there... moving back the file is quite more interresting challenge, cuz we want to prevent  all history right.\n\nIf we are moving the exact same file, we could run a diff. see, if <oldfile> goes from 1.1 to 1.24 and <new file> is 1.1 to 1.98 and all version from <oldfile> exist in new file, well, we can just copy over the new one to the old emplacement, cvs delete the old emplacement of the <new file> and we are pretty much ok to go. except if there is branches, I suppose it gets really really more complicated. but at least we got a hint for a way to go now. there is people out there much better than me which would think about what I don't.\n\nthis is feasable.\n\nabout the last case where you move an existing cvs file over a old cvs file location... this one becomes really tricky. since both history can hardly be kept. but I suppose this could be forbidden, or cvs could ask what do you want to do, like different scenario is possible.\n\ncome on guys.\n\n"
    author: "somekool"
  - subject: "Re: SUBVERSION"
    date: 2004-10-26
    body: "Hey good idea.  Lets fix all the cvs bugs, but keep all the commands similar so as to not confuse anyone, and keep it basically the same.\n\nThen we'll change the name of it from cvs to something else so as not to confuse anyone.  svn sounds like a good name to change it to.  Yeah lets do that!\n\nOh wait.\n"
    author: "JohnFlux"
  - subject: "Re: SUBVERSION"
    date: 2004-10-25
    body: "\"here is what I think, why not fixing CVS instead of switching the world to a new system which input so much risk.\"\n\nAFAIK, Subversion's intention is to \"fix CVS\". If you want to fix CVS, you can do so by moving to Subversion, which is the \"fixed CVS\" ;)."
    author: "Janne"
  - subject: "Re: SUBVERSION"
    date: 2004-10-25
    body: "> here is what I think, why not fixing CVS instead of switching [...]\n\nDon't you think people have been trying to fix it for ages? The whole reason for the Subversion project was that people were trying to fix CVS, but simply couldn't. It proved to be impossible to implement real atomic commits, real moves, etc. without changing the repository format. If you're going to change the repository format anyway, you might as well start with something completely new (or so the thinking goes)."
    author: "Mr. Fancypants"
  - subject: "Thank you for fixing khtml (max/min height)"
    date: 2004-10-27
    body: "I have wanted to use these features for a freakin' long time.  Now it will make my work doing Cocoon more enjoyable, having the ability to preset the dimensions of my divs."
    author: "Marc Driftmeyer"
---
In <a href="http://cvs-digest.org/index.php?issue=oct222004">this week's KDE CVS-Digest</a> (<a href="http://cvs-digest.org/index.php?newissue=oct222004">experimental layout</a>): <a href=" http://www.konqueror.org/features/browser.php">khtml</a> fixes include table layout, background-position, min max-height and mangled html fixes.
New KControl for Logitech mouse features.
Kicker and taskbar optimizations and improvements.
Xpdf security fixes.
Also coverage of the Subversion discussions on kde-core-devel.



<!--break-->
