---
title: "Interview: Trolltech's Eirik Eng and Matthias Ettrich"
date:    2004-04-12
authors:
  - "pfremy"
slug:    interview-trolltechs-eirik-eng-and-matthias-ettrich
comments:
  - subject: "Wow!"
    date: 2004-04-12
    body: "That's an excellent interview! Some really interesting questions, and well followed up. Thank you.\n\nYou could split it into sections with headers though, as it is quite long ;-)"
    author: "Tom Chance"
  - subject: "Re: Wow!"
    date: 2004-04-12
    body: "That's an idea. Can you give me a link to my posting. I'll see if I can improve it\n"
    author: "Philippe Fremy"
  - subject: "Longest interview"
    date: 2004-04-12
    body: "I've ever seen with TT.. good job!"
    author: "wow"
  - subject: "MFC"
    date: 2004-04-12
    body: "Why do qt programs look different from kde programs?\nFor instance: hbasic.sf.net\n\nwhat is the real difference of KDe and qt programs.\n\n---\nI always liked QT for its excellent documentation and clean classes.\nIs it possible to compile MFC programs under QT? Are there bindings?\nCan qt be used as an alternative to swing or awt?\n"
    author: "Andr\u00e9"
  - subject: "Re: MFC"
    date: 2004-04-12
    body: "> Why do qt programs look different from kde programs?\n\nDo they? If then because they use a different style? Because they use a static version of Qt which doesn't allow to load Qt style plugins?\n\n> what is the real difference of KDe and qt programs.\n\nKDE programs are subclassed from KDE classes while Qt programs are subclassed from Qt. KDE programs are thus enriched with more features (sophisticated file dialog, print system, toolbar editor, new widgets, ...)."
    author: "Anonymous"
  - subject: "Re: MFC"
    date: 2004-04-12
    body: "MFC and Qt do exactly the same thing, so no, there is no binding from one to the other as it would not make sense.\n\nUnder java, it is possible to use a Qt implemenation of awt. It is also possible to use Qt directly as an awt/swing alternate implementation but it will only work on unix, unless you have the expensive version of Qt for windows."
    author: "Philippe Fremy"
  - subject: "Re: MFC"
    date: 2004-04-15
    body: "> Under java, it is possible to use a Qt implemenation of awt\n\nWhere is this QT-AWT implementation? Is it free-software?"
    author: "Damjan"
  - subject: "Nice"
    date: 2004-04-12
    body: "THat was the best interview I've read about Qt, thank you. BTW: It's nice to see that even commercial companies share my view about patents."
    author: "Alex"
  - subject: "Great Interview"
    date: 2004-04-12
    body: "Thanks, that's a really, really interesting interview.\n\nMichael."
    author: "Michael Goettsche"
  - subject: "Patents?"
    date: 2004-04-12
    body: "What SCO patents are they talking about?  I don't think any exist.  Do they mean copyrights?"
    author: "Anon"
  - subject: "Canopy and SCO and Trolltech."
    date: 2004-04-12
    body: "The whole SCO ordeal is odious.  Everyone that I know who has written any code and released it under the GPL is REALLY ANGRY at SCO claiming that all of Linux (including the utilities that come with it) belongs to them.  It's been a year and they've shown *0* proof.  As Clara Peller once said, \"Where's the beef?!\"\n\nIdeed, if there's anyone trying to steal IP, it's SCO.  This is a blatant attempt to steal all the effort that thousands of people have contributed to Open Source.  I know you had to dance around the question a bit, but it's time to tell SCO and Canopy goodbye.  Even if Canopy is merely an *investor* in TrollTech, they are certainly endorsing SCO's actions, and by extension are a _threat_ to TrollTech.\n\n--\nBMO - a KDE and QT enthusiast since 1.0"
    author: "Boyle M. Owl"
  - subject: "Re: Canopy and SCO and Trolltech."
    date: 2004-04-13
    body: "> The whole SCO ordeal is odious. \n\nI agree :)\n\n> Everyone that I know who has written any code and released it under the GPL (...)\n\nthat includes me\n\n> (...) is REALLY ANGRY at SCO claiming that all of Linux (including the utilities that come with it) belongs to them.\n\nI'm not angry. Somehow GPL needed a test case, to prove it's strength in court. Right now it's over a few discussable, silly lines of code. Next time it can be  on a different level.\n\nSince I believe M$ supports SCO by giving some money for the trials, i think they're -- just like me -- interesed in the outcome.\n\n\nRemeber: \"What doesn't kill you makes you stronger\"\n\nCies.\n"
    author: "Cies Breijs"
  - subject: "No CJK questions"
    date: 2004-04-12
    body: "I guess this interview was done by KDE France, so maybe cjk stuff is irrelevant, but I would really like to hear more about TT's plans in addressing CJK issues in the future (as well in the near future). There is still a lot of work to be done in this area, and as Japan is coming close to the 5% mark, maybe its time to look into it even more. Better CJK support only means more customers in china/japan/korea which can be a pretty big market that is eager to look into more open-source solutions."
    author: "Ken"
  - subject: "nice"
    date: 2004-04-12
    body: "You know...I think Trolltech is a really nice company.\nJust wanted to say that. I like the business ideals of these people (at least, as far as I can guess what they are on the basis of this article).\n\nOn top of that, Qt is a really nice toolkit (now..I just need an idea for a program to create....)\n"
    author: "danny"
  - subject: "GPL'ed QT3-WIN32 is needed!!!"
    date: 2004-04-12
    body: "\nThey say that so far there is no community for FREE win-32 applications. \nmaybe. But what there is -- is huge demand for free Cross-Platform applications. Good examples are OpenOffice and Mozilla. The only good QT example that i know is SIM -- but for WIN-32 they have to use QT 2.3.\n\nAnother proof of such demand is that QT-X11 fork started by volunteers to create a GPL'ed Win32 version. \n\nUnfortunately, For my research project i have to use wxWindows becasue it has to work both in Windows, Irix and Linux. :-( "
    author: "SHiFT"
  - subject: "Re: GPL'ed QT3-WIN32 is needed!!!"
    date: 2004-04-13
    body: "What is your problem? As mentioned in the article itself, if their contibutors are easy to reach SIM could just change the license to include an exception for Qt/Win32 so that SIM can be build with the non-commercial Qt/Win version."
    author: "Datschge"
  - subject: "Re: GPL'ed QT3-WIN32 is needed!!!"
    date: 2004-04-13
    body: "There is a non-commercial version of Qt3 out now.. "
    author: "fault"
  - subject: "Re: GPL'ed QT3-WIN32 is needed!!!"
    date: 2004-04-13
    body: "Huh? Where did you get that information from?\n\nQt 2.3.0 is the only non-commercial version availible at ftp://ftp.trolltech.com/qt/non-commercial/"
    author: "Niek"
  - subject: "Re: GPL'ed QT3-WIN32 is needed!!!"
    date: 2004-04-13
    body: "It comes with a book. It was announced right here a couple of months ago."
    author: "Roberto Alsina"
  - subject: "Re: GPL'ed QT3-WIN32 is needed!!!"
    date: 2004-04-13
    body: "Where is this version?  (you have peaked my interest)\n\nAs of today, the trolltech web site says nothing about a non-commercial version of Qt 3 for Windows"
    author: "SuperDave"
  - subject: "Re: GPL'ed QT3-WIN32 is needed!!!"
    date: 2004-04-13
    body: "Yes, it does. See http://www.trolltech.com/download/qt/noncomm.html"
    author: "Datschge"
  - subject: "Re: GPL'ed QT3-WIN32 is needed!!!"
    date: 2004-04-15
    body: "No it doesn't ... that page only gives status for Qt 2.3 - not Qt 3.x -- am I missing something here?  Qt 2.3 is aincent, and trying to maintain a (py)Qt project with the unix 3.x codebase while windows is stuck in 2.3 sounds like a big pain in the but.\n\nIf all you need is Qt 2.3, this is good enough I guess - but it doesn't cut it for more advanced cross platform open source apps - which is exactly where Qt would shine.  The article actually touches on this fact -- licensing being the problem.  \n\nWell, this has led me to choose wxPython for my next project, even though I would have preferred Qt.  It could be worse, I could be using MFC! :)"
    author: "SuperDave"
  - subject: "Re: GPL'ed QT3-WIN32 is needed!!!"
    date: 2004-04-15
    body: "\"When the official Qt Book \"C++ GUI Programming with Qt 3\" is released, we plan to include a CD with a special non-commercial \"book-version\" of Qt/Windows 3.1.\"\n\nLooks like you can get the non-commercial version, but you can't download it as of yet. However, buying a book seems a pretty small price to pay - at least you get some good documentation, help and software bundled together."
    author: "David"
  - subject: "GPL Apps linked to Qt/Windows"
    date: 2004-04-12
    body: "\"You cannot take a GPL Linux program, compile them with Qt for windows and give the executable to somebody else, because you would violate the GPL. [Ed: because Qt on windows is closed source]\"\n\nI don't really understand this. GPL applications are allowed to link to closed source libraries, as long as it does not have a dependency on them, right? How else could things like cygwin exist? Or GIMP for non-free Operating Systems. What about GPL'ed JAVA apps. JAVA surely ain't opensource.\n\nI think this is just plain nonsense. Any GPL app should be possible to be released using a commercial Qt DLL, as long as the *application* itself has the source code available.\n\nAm I wrong? If so, please explain to me why."
    author: "Richard Stellingwerff"
  - subject: "Re: GPL Apps linked to Qt/Windows"
    date: 2004-04-13
    body: "The program would have a dependancy on it.  The key part is the \"and give the executable to somebody else\".  You have to provide source to build that app under the GPL... including all dependancies and licensing dependancies.\n\nNow, if you count Qt as a system library (there is a specific clause in the GPL about those), you can do it.  But would you say that Qt is a system library?"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: GPL Apps linked to Qt/Windows"
    date: 2004-04-13
    body: "Maybe Qt is not a System Library, although it is used by many apps on my system. The app would not have an excplicit dependency on it either. It could just as well be compiled with Qt/X11 for Windows. So why is it a problem if I chose to compile (and release) it with my own non-free library.\n\nWhat defines a system library anyway? Is Java for windows a system library? And it's not GPL, is it? How come GPL'ed JAVA apps exist then. Are they violating the GPL?"
    author: "Richard Stellingwerff"
  - subject: "Re: GPL Apps linked to Qt/Windows"
    date: 2004-04-13
    body: "If you give away the binary it will either be linked to Qt/Win32 or to Qt/X11.  The latter is not a problem because the two licenses are not incompatible.  The former is a problem because the license on Qt/Win32 restricts you from meeting your obligations under the GPL.  You will have infringed on one or the other copyright depending on which action you take."
    author: "Anon"
  - subject: "Re: GPL Apps linked to Qt/Windows"
    date: 2004-04-13
    body: "The GPL defines it in the following paragraph:\n\nThe source code for a work means the preferred form of the work for\nmaking modifications to it.  For an executable work, complete source\ncode means all the source code for all modules it contains, plus any\nassociated interface definition files, plus the scripts used to\ncontrol compilation and installation of the executable.  However, as a\nspecial exception, the source code distributed need not include\nanything that is normally distributed (in either source or binary\nform) with the major components (compiler, kernel, and so on) of the\noperating system on which the executable runs, unless that component\nitself accompanies the executable.\n\nI don't think Qt is \"normally distributed\" with the \"major components\" of Windows.  Thus it is not a system library and does not fall under the special exception."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: GPL Apps linked to Qt/Windows"
    date: 2004-04-13
    body: "The crux of what you are saying is that the result of merely linking to a library should not create a derived work of that library. Without going into the legalities of the GPL, I would point out that this goes against the spirit of Free Software. Even just the freedom to study the source code to see how it works is kind of meaningless if part of \"how the program works\" is tied up in a non-Free library. Now the GPL has a special execption for non-Free operating system components, because back when it was written we didn't really have a Free operating system. (Who knows whether the next version of the GPL will have that exception? What about GPLv4?)\n\nAnother point to consider is that GPL does not just enable to you access the source code, but allows for further distribution. For instance, I can download a version of LyX which is linked against Qt non-commercial 2.3. If this is really licensed under the GPL, then I should be able to put it on a CD and sell it for real money. However, if I did that, I would not be able to fulfil section 3 of the GPL, _and_ I would be using non-commercial Qt for commercial purposes. Surely I am not free to do this. So here you have a piece of software which has had its freedom reduced by linking against a non-free library.\n\nI hope this helps you understand why they couldn't do this, if not I am happy to continue the discussion."
    author: "Toojays"
  - subject: "Cairo"
    date: 2004-04-13
    body: "What they think about things like Cairo would have been interesting."
    author: "CE"
  - subject: "Re: Cairo"
    date: 2004-04-13
    body: "As long as it's in (early) development stages there's probably not much to\nthink about :) . It sure looks promising though."
    author: "Anonymous"
  - subject: "Qt/Windows for free"
    date: 2004-04-13
    body: "I just want to mention that the Psi project (http://psi.affinix.com/) receives Qt/Windows for free.  Trolltech gave us 3 commercial licenses, including support and upgrades, provided that we only use it for open source work.\n\nI don't know how common it is for Trolltech to do this.  Maybe it helped that I was a Qt customer in the past.  Maybe we're the only such project.  In any case, if you have a popular and dedicated free software project, you might try asking them to help you out. :)"
    author: "Justin Karneges"
  - subject: "Re: Qt/Windows for free"
    date: 2004-04-15
    body: "I love psi .. and use it on daily basis. Thank you so much for that program. \n\nFab"
    author: "Fabrice Mous"
  - subject: "questions I would like to have seen... "
    date: 2004-04-13
    body: "Licencing\n---------\n\nDo you think it would be better for the overall state of free software\n ( not necessarily any plans of your own) if IBM or Novell bought you out and LGPLed QT on all platforms? Is there any downside to this scenario?( Maybe your support would become less professional... just look at Rational..) TBH, I understand you can't really answer this question, as answering in the affirmative would possibly lower your acquisition price and thus damage share holder value... but we all think we know the answer here..... \n\nTheming\n-------\n\nGiven that you use the native theming APIs nowadays on Windows and Mac OS X, what are you doing as a top two toolkit provider on Linux doing to provide a unified theming API, acceptable to all toolkits? \n\n\nWould you agree that we need to say goodbye to theme duplication (keramik/geramik) and the horrors that result? (Leading question if ever there was one!) \n\nWould you as a company characterise the Qt/Gtk engine as a realistic cross toolkit theming api or a silly hack?  \n\nToolkits requiring integration include VCL, GTK, Qt, XUL/XBL, Motif, Tk, xaw, athena, swing, xforms, fltk  etc etc.... This, IMHO, leads to a probable C and cairo based system. Would Trolltech move to a system such as this? Would they contribute resources to a project like this? \n\nThe Avalon questions.. \n-----------------------\n\nWhat are you doing to address the coming prevalence of resolution independent user interfaces? Are you going to fully unify the QCanvasItem and the QWidget hierarchies as MS are planning in Avalon?\n\nOn Windows, the fact that Qt really fits in nicely and looks native even when it wasn't has served it well. Are you going to be able to keep doing this when Avalon arrives? \n\nIMO, the largest part of Qts appeal on Windows is that it provided an escape from the horror that is programming the Win32 user interface. Anything based on this is really a work of terror and limited - raw access to User.dll, MFC, ATL, WTL, Winforms - all of these make Qt look great. Are you at all worried that the Windows \"I can't face another HWND\" market will dry up when Avalon hits the scene (granted this is probably 2007 realistically, but you must be planning ahead)? "
    author: "rjw"
  - subject: "Re: questions I would like to have seen... "
    date: 2004-04-13
    body: "You are a bit late. The questions period was last july:\n\nhttp://dot.kde.org/994553595/ - ask Trolltech anything"
    author: "Philippe Fremy"
  - subject: "Re: questions I would like to have seen... "
    date: 2004-04-13
    body: "08/Jul/2001\n\nHeh.\n\nI think you meant http://dot.kde.org/1057129749/"
    author: "Niek"
  - subject: "Re: questions I would like to have seen... "
    date: 2004-04-13
    body: "Yep. I'm asking these questions now. Trolltech can decide whether or not to answer them... since they do read the dot. "
    author: "rjw"
  - subject: "look what I found"
    date: 2004-04-15
    body: "http://dot.kde.org/1057129749/1057275038/\n\nI did ask a lot of the same questions... "
    author: "rjw"
  - subject: "Re: questions I would like to have seen... "
    date: 2004-04-15
    body: "> Do you think it would be better for the overall state of free software\n> ( not necessarily any plans of your own) if IBM or Novell bought you out and \n> LGPLed QT on all platforms?\n\nThat would be a great idea! BSD style license or LGPL will be nice, this is what the free commercial market needs! Then all GPL-related issues will go to hell.\n\nUntil then you have to stick with one of those: VCL, GTK, WxWindows\n\nBut as you guess this questions are always being ignored.\n\nBtw there is one more option:\nhttp://www.kde.org/whatiskde/kdefreeqtfoundation.php\nthen you will have BSD Qt, but I guess before that happens there will be already another superior toolkit that will preferably be BSD."
    author: "Anton Velev"
  - subject: "Re: questions I would like to have seen... "
    date: 2004-04-15
    body: "\"free commercial market\"? What is that?"
    author: "Datschge"
  - subject: "Re: questions I would like to have seen... "
    date: 2004-04-15
    body: "Perhaps a commercial market for free software.\nOr a free market for commercial software.\n\nIn either case it already exists :)"
    author: "Kevin Krammer"
  - subject: "Re: questions I would like to have seen... "
    date: 2004-04-15
    body: "oh, not again...\n\nRead some articles about Adam Smith and Free Market.\n\nThe current state of modern society seems to prove that the Free Market (commercial) is the best working model. (when saying 'market' I mean the economy being it global or local)\nHowever some people (rms) seem to discover that capitalism, is not the best model and try to make it more 'social'. (I agree that Free Market may not be perfect, but still is the proven working model)\n\nNow about the problem: one of the biggest problems of the Free Market is monopoly because the key factor for improvment in free market economy is the commercial competition. And monopoly is the opposite of competition.\n\nIf we believe that GPL really destroys one monopoly (well known empire), there still remains the question how would GPL help the competition to improve. Moreover since IT industry requires standartization, seems that it's a normal trend of adpotion of one monopoly solution for a given purpose (as for example current state is that Apache is widely adopted as \"standart\" solution for webserver, and Windows is widely adopted as a \"standart\" solution for desktop). However organizations like w3c try to help with the standarts and make possible actual competition of differrent parties when implementing the standarts.\n\nThe reason I don't believe GPL is the right solution is because I am strongly convinced that GPL eliminates the competition. Again if we believe GPL will ever destroy all other software and everything becomes GPL do you think there will be competition? You probably can show me the example of GTK vs Qt saga (but this is LGPL vs GPL issue). To me more exact I must add the facts that GPL:\n- can be forked\n- you cannot sell your closed-source feture (to GPL software)\nMake conclusions yourself. But you cannot miss the fact that GPL encourages GPL monoploly. Of course it would sound great if spelled like \"GPL powers the innovation by uniting the GPL developers\", but here is what makes me feel we have a problem - again instead of competing parties that try to make it better and better.\n\nI agree that if there were >10 competing Qt like companies (selling commercial licenses only) and there was no Microsoft that would be better for the Free Market, but the current state of the world makes in impossible. (here is where GPL people find GPL very useful)\n\nBut wouldn't it be better (having in mind the current state of the world) for the Free Market if there was a truly open (no monopoly) and standarts compilant platform. (by \"truly open\" I mean like BSD, X, Apache and MIT licenses) Then every company out there could add a feature and sell it - like Apple does but in a very large scale. With GPL this will be impossible, except if the company that created the GPL software is selling the additional features.\n\nTo help you better understand my opinion will show you this formula:\nfree market = competition != GPL\n\n\n\nIf you can convince me that GPL not only destroys the empire but also helps and encourages the competition then probably I may agree that GPL is the right model."
    author: "Anton Velev"
  - subject: "Re: questions I would like to have seen... "
    date: 2004-04-16
    body: "\"To me more exact I must add the facts that GPL:\n- can be forked\n- you cannot sell your closed-source feture (to GPL software)\"\n\nA company may choose to to release a library simultaneously under GPL and a commercial license. You may create GPL software based on the library, or you can pay a fee to release closed-source software based upon the commercially-licensed version. This is the case with Qt on Linux. Companies are able to profit and thus compete while using GPL by including an additional licensing option."
    author: "LuckySandal"
  - subject: "Accessibility NOT required by law"
    date: 2004-04-13
    body: "What the government requires is that if the government purchases electronic information & technology they must purchase the MOST ACCESSIBLE version that meets all the requirements.\n\nSaying the law requires accessibility is out of context and simply not true.\n\n"
    author: "Gildas"
  - subject: "Re: Accessibility NOT required by law"
    date: 2004-04-13
    body: "Under the American's with Disabilities Act, employers must make reasonable accommodations for those with disabilities. When it comes time to chose a computer system, and there is one (or many) that have accessibility functions, and one that doesn't, I suspect a judge would say that reasonable accommodation means choosing the systems that accommodate. It would be difficult to convince a judge that it is 'undue hardship' to provide a system when they are commonly available.\n\nSo, if I were an employer in the US, and I would consider it defacto required by law. That is why SUN and others have worked on the accessibility functions. Employers that want to use Linux for other cost savings should foot the bill for the development.\n\nhttp://www.usdoj.gov/crt/ada/q&aeng02.htm\n\nAnd on human terms, I think it is a display of respect if our systems can be used by those with disabilities. Any one of us could be disabled by some accident or illness. It is one more design parameter that needs to be taken into consideration.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Accessibility NOT required by law"
    date: 2004-04-13
    body: "Personally, I find the legality of all this hazy. We have stuff like this here in the UK for websites and other systems, but it has never been enforced at all (not likely to either). You just can't sell usability - the above interview is dead right. However, we have high standards in the free software community and there is no reason why something should not be done about accessibility, and it is. I think the free and open source community is great in this respect. If something can be done, and it is worthwhile, then it will be.\n\nMatthias is very right though - we desperately need a system wide IPC/messaging system that anything can plug into. This will not be based on CORBA, or if it is then several layer of standards will need to be developed on top of it. Ask any programmer who does commercial development why they don't use CORBA if you don't know this already. Alternatively, go to your local bookstore, find a book on CORBA and try to 'lift it'. D-BUS looks like a good bet, but I haven't read enough about it and haven't tried it.\n\nThis is so badly needed that I really couldn't care a less whether it uses glib or not - I just want my desktop to be able to do things. If there can be a decent compromise where just about any part of a system can plug into it, I think it will be a massive step forward."
    author: "David"
  - subject: "Borland Kylix"
    date: 2004-04-13
    body: "Why wasn't there any mention about the Borland Cooperation?\nBecause it wasn't successful?"
    author: "tester"
  - subject: "Re: Borland Kylix"
    date: 2004-04-13
    body: "I think the Borland collaboration could have been quite successful, but I don't think Borland quite know what they are doing or where they are heading. In the face of pressure from, particularly MS Visual Studio tools, Java, Macromedia and Qt cross-platform development I don't think they quite know where they fit in. I think even Macromedia will be under threat from a great deal of the WinFX stuff.\n\nLike many of Microsoft's long-time competitors, Borland seem to have backed themselves into a small niche comfort position with existing customers."
    author: "David"
  - subject: "Additions by Haavard Nord"
    date: 2004-04-13
    body: "Haavard Nord responded to some questions on Slashdot: http://slashdot.org/comments.pl?sid=103783&cid=8844755"
    author: "Anonymous"
  - subject: "Re: Additions by Haavard Nord"
    date: 2004-04-13
    body: "He replied on Slashdot? Wow, that guy is an incredible CEO."
    author: "David"
  - subject: "QT not quite C++"
    date: 2004-04-14
    body: "About the comments about QT beeing better for C++ development this is not (of course) what the GTKmm people think:\n\nhttp://www.gtkmm.org/features.shtml\n\nThen again, I am a C coder so QT is quite irrelevant for me but if I'm going to start working on C++ I would rather user GTKmm as it is more standard and seems to use C++ in a more modern way.\n\nRegards."
    author: "Anonymous Lurker"
  - subject: "Re: QT not quite C++"
    date: 2004-04-14
    body: "Bah, get a better trolling line.  This one sucks...  GTKMM isn't a bad toolkit but it doesn't compare to Qt in scope or maturity.  Just compare the APIs to see what the two offer.  Qt is a very pragmatic toolkit -- its goals are ease of use, portability and whatnot.  If you're really prefer some sort of esoteric sense of \"standard\" and put that above pragmatism, well, have fun -- but then you probably wouldn't be using C++ either.  ;-)"
    author: "Scott Wheeler"
  - subject: "Re: QT not quite C++"
    date: 2004-04-14
    body: "The Gtkmm rants against Qt always comes back to the same equation:\n- GTKmm is more standard == GTKmm uses templates instead of moc preprocessor to handle signal and slots\n- GTKmm uses C++ in a more modern way == GTKmm uses templates instead of moc preprocessor to handle signal and slots\n- GTKmm is typesafe == GTKmm uses templates instead of moc preprocessor to handle signal and slots\n\nSo yes, Qt does not use templates for signal and slots. There are drawbacks to this that are quite minimal:\n- your namespace is \"polluted\" with the following macros: slots, signal, emit, SIGNAL, SLOT\n- you have to pre-process your files through the moc.\n\nHowever, the moc approach of Qt does have advantages. For example, it adds introspection feature to C++ which can be quite convenient. You can also dynamically load and connects Object.\n\nIf you write code only for the aesthethic of the result, then the debate \"moc vs template signals\" matters. If you write code to produce software, you don't care since both methods produce software that works. You will learn to use either in less than one day of coding.\n\n\n"
    author: "Philippe Fremy"
  - subject: "Re: QT not quite C++"
    date: 2004-04-15
    body: "I don't know GTKmm but I find this line of argumentation always a bit - sorry - infantil. You say about yourself, that you are a C programmer but judges something (in practice) more or less unknown to you (C++/Qt) using buzzwords like 'modern'. \nWhen compilers came out able to support templates, I used them with excitement. It is such a cooool language feature. It is - but it has his limitations. You will become aware of this, when you try something non-trivial with them - and trivial things too: I recommend you a search on the net for an article from Andrei Alexandrescu in \"C++ report\" (out of print) or \"C++ user journal\". He tries to implement there a max template max<T,T> in an absolutely satisfying way. It wasn't successful - and Andrei was well aware of this (and mocked of himself). So, feateres alone are nothing.\nTo Qt:\nThey implemented their extensions in a very pragmatic, but still useful and sensible way. Give them a try and then come back.\n\nMicha "
    author: "Micha Bieber"
  - subject: "Re: QT not quite C++"
    date: 2004-04-15
    body: "Hi all, thanks.\n\nThese posts have been quite productive for me, as It was easy to read between lines that what the GTKmm people say is true, it's just that not many people find their arguments convincing.\n\nBasicaly what I have learn from these posts is that QT is non-standard C++, limited by the support of old compilers, monolythic but complete and pragmatic. I already learn that \"modern\" is a buzzword and is non-sense but \"have fun with it\" is a deterministic technical feature.\n\nAnd not forgetting the Propietary/GPL license, and not having even the GPL on Windows (and all my family use OpenOffice and Mozilla on Windows so please stop saying nonsese about free software on Windows beeing stupid).\n\nOk, if I choose to jump to C++ I have made my mind up clearly.\n\nBTW: I am a C and PHP coder but make no GUI work by the moment, and no I have no time to test GTK and QT and C++ before choosing a GUI framework, that's why I made these comments, I just don't believe anybody's PR (neither GTKmm nor Trolltech) and need an independent opinion."
    author: "Anonymous Lurker"
  - subject: "Re: QT not quite C++"
    date: 2004-04-15
    body: "Qt is standard C++, otherwise it wouldn't compile with g++ obviously. And moc, BTW, is actually not a preprocessor, the only preprocessor used is the standard C++ preprocessor (well, at least if I understand the difference between a preprocessor and a code generator or whatever the proper name for this functionality is). In other words, Qt C++ code is compiled as is just like any other C++ code, and hence it's standard C++ as well. The difference between Qt and gtkmm is basically the fact that they're different.\n"
    author: "Lubos Lunak"
  - subject: "Re: QT not quite C++"
    date: 2004-04-15
    body: "Yes, complaining about moc code generation is like complaining that it's wrong to generate code from a Qt Designer .ui file, because you should only have 'pure C++' in your project. After all C++ wouldn't be any fun if it wasn't 'difficult' -  you need to show off to lesser programmers that you'd actually mastered it. And of course those 'hand crafted' widgets are so much better than any autogenerated nonsense from a .ui file.\n\nBut I'm personally not a fan of C++, although I can code in it, because I just find it too much of a headache. If I was a PHP/C programmer I think I'd try out PyQt or PyGtk perhaps, not C++. There is absolutely no such thing as 'C++ for Dummies', whichever toolkit you use it is very, very hard."
    author: "Richard Dale"
  - subject: "Re: QT not quite C++"
    date: 2004-04-15
    body: "The GTKmm people produce standards for C++? Wow. You learn something new every day."
    author: "David"
  - subject: "Re: QT not quite C++"
    date: 2004-04-15
    body: ">Ok, if I choose to jump to C++ I have made my mind up clearly.\n\nThis is seldom the case, if the mind has been burdened by too much emotions :->\n\nThe point, that you get here sarcastic answers is caused by your very own posting.  'modern!=good' and 'old!=bad'. In fact, these words alone have no meaning at all without careful evaluation. The technical points you missed in your flurry of excitement to remain independent have been brought to you by some other posters. I can only repeat my advice. Use the librariy(ies) and form a own view on them. Nobody here here will make you do something. \n\nMicha"
    author: "Micha Bieber"
  - subject: "If I had one wish for Qt..."
    date: 2004-04-15
    body: "It would not be to get rid of the moc, but to have Qt use exceptions."
    author: "Marc"
  - subject: "I never knew this..."
    date: 2004-04-15
    body: "\"An important note to all the readers, Qt is pronounced 'Cute' by its creators.\"\n\nI always thought it was 'Q-T!'"
    author: "LuckySandal"
  - subject: "Re: I never knew this..."
    date: 2004-04-16
    body: "It is \"Q-T\" or \"ku-te\" which maybe sounds similar to \"cute\" if you are unfamiliar with norwegian.\n"
    author: "."
  - subject: "Re: I never knew this..."
    date: 2004-04-16
    body: "Accoring to Matthias Kalle Dalheimer's excellent book, either pronunciation is good. I just say Q-T."
    author: "David"
  - subject: "Re: I never knew this..."
    date: 2008-01-15
    body: "I presonally like \"Cutie\" better ;)"
    author: "JOY"
  - subject: "Re: I never knew this..."
    date: 2008-08-01
    body: "I just went to a Qt developer conference offered by ICS. The Trolltech representatives and the ICS training staff all pronounced it \"Que-Tea\" (Q-T), not \"Cute\"."
    author: "Qt Developer"
---
Eirik Eng, president of <a href="http://www.trolltech.com/">Trolltech</a>, and Matthias Ettrich, founder of the KDE project
and director of software development at Trolltech, were interviewed by Philippe Fremy, KDE enthusiast.
This interview was conducted in August 2003. The interview was made
possible by Laurent Rathle, who is maintaining the <a href="http://www.kde-france.org/">KDE France</a>
website. A <a href="http://www.kde-france.org/article.php3?id_article=117">French translation</a> is available on KDE-France.
<!--break-->
<p>
Before entering the interview itself, I would like to thank heartily all the
people who have given their time to transcript the 90 minutes of interview
into text and edit the result: Dave Brondsema, Navindra Umanee, Nicolas Verite, Stephan Binner, Laurent Rathle, 
Pascal Audoux, Sylvain, Fabrice Mous and Ingo Kl&ouml;cker.<br>
<p>
An important note to all the readers, Qt is pronounced 'Cute' by its
creators.
<p>
<hr>

<b>Philippe Fremy</b>: I don't know if you remember, but this is not the first interview
I have done with Trolltech. I did one by email <a
href="http://dot.kde.org/1001294012/">two years ago</a>.
<p>

<b>Eirik Eng</b>: I remember that. Quite a lot has happened in two years.
<p>


<b>PF</b>: I guess you know that I have been asking around the community for
questions to ask Trolltech.
<p>


<b>EE</b>: Yes, we saw the article on dot.kde.org, that was a good idea.
<p>


<b>PF</b>: What is the state of your business today: How many licenses are
you selling, what are the main sources of profit, the type of clients?
<p>


<b>EE</b>: Today, Trolltech has 76 employees. We have one office here in Oslo
where we are around 50 employees, we have one embedded office
department in Australia (in Brisbane) and we have sales and marketing
offices in the United States, each with about 12 employees.
<p>


Financially, Trolltech is now about break-even, capital-wise. We had a
period of two years where we were losing money. Specifically due to
growing the organization by using the investments that were received
in 2000.  Right now, we are at the point of break-even, but we see
that our income is growing faster than our expenses. So things are
looking very good for the future.
<p>


In terms of customers and licenses, we now have well over 2000
customers and a typical Trolltech Qt customer is either a large
company that has its own software development department or a small
company that specializes in software development. That's the two
typical types of customers we have today.
<p>


We have about 80% coming from the desktop business and development
tool business, and 20% coming from Qtopia and Qt/Embedded.
<p>


<b>PF</b>: In which countries has Qt had more success or less success?
<p>


<b>EE</b>: In terms of sales, we have about half of our license sales in the
USA and a clear second is Germany, with about 20%. France and United
Kingdom are competing for third place with 5%.  I think Japan is
getting pretty close to France and UK.
<p>


We have sold to 65 countries so far. It is really a very distributed
market and everything is over the net, of course.
<p>

<b>PF</b>: Now, a question that everybody has been asking: Why isn't there a
GPL version of Qt3 for Windows?
<p>

<b>EE</b> (laughing): As some people mentioned on the dot, it has partly to
do with finances, sales and Trolltech's business model. Another point
is the fact that Windows is a closed source Operating System. There is
no community for Free Software development under Windows. The
situation is very different from Linux, as you know. On Windows
development usually happens as shareware or commercial software and we
don't see that community evolving into producing Free Software.
<p>

<b>PF</b>: So basically, you think you would not leverage a similar effect as
you did with Qt on Linux?
<p>

<b>EE</b>: Not at all. And we have considered the risks for us as a
company. Still today, a big part of the contacts we have with
customers are the people registering for the evaluation version for
Windows. They have to give us a name, an email address and tell us a
little bit about their project. Then we give them an evaluation
version for Windows. This is how we get in contact with the customers,
how we can find out information about their project or what they need
and help them understand the licensing model. So, that's part of what
we risk losing.
<p>

As you know we are doing an experiment these days with the Macintosh
port. We have seen that there is a very good evolution in the
Macintosh community. There is a lot of Free Software development going
on and we thought this was the right time to GPL the Mac OS version,
to make sure that the Free Software community for Mac OS X has a great
tool.
<p>

<b>PF</b>: About Qt on Mac OS, the very first announcement on the website mentioned
Qt running on Mac OS 9. It was quickly removed, something like one day after.
Did Qt really run on Mac OS 9 or is it just limited to Mac OS X?
<p>

<b>Matthias Ettrich</b>: We had a prototype on Mac OS 9 and some applications were running, but it
was far from production quality. And we figured out that there were many
limitations in order to complete our work. Mac OS 9 had too many limitations.
At that time, Apple was clearly saying that they were moving to X, that 9 was
going to fade out. That's why we started a totally new port, to Mac OS X.
<p>

<b>PF</b>: Eugenia Loli-Queru (from OSNews) has been complaining in an
article than on Mac OS, Qt applications are not perfectly like the
Aqua applications. The complaint is that Qt is actually emulating the
first version of the Aqua style, and Apple has since then slightly
changed the style. So you are not using native Mac OS X API, unlike
the Windows XP version of Qt. Is that correct?
<p>

<b>EE</b>: Yes and No. The very first version of Qt for Macintosh, the 3.1
version was emulating the style. It was the first Aqua style and it
has changed a bit.
<p>

The current version (3.2), and the one in GPL, is actually using
Apple's style API to draw widgets. That means that we follow whatever
changes Apple does to the style and that Qt stays up-to-date.
<p>

So there is no reason to complain about that anymore.
<p>

<b>PF</b>: Have you had any commercial relationship with Apple when you were
working on Mac OS? Do you plan any future partnership?
<p>

<b>EE</b>: We don't have a formal commercial arrangement as such but we have
very good contacts with Apple and we speak to them regularly, on
several levels.  They think this is a very good thing. For the time
being we decided to cooperate just informally, like what we are
doing. We are getting a lot of support in return, most notably at the
Worldwide Developer Conference.  Central people at Apple have been
presenting and talking about Qt as a very good development solution
for the Macintosh.
<p>

<b>PF</b>: Right now Qt supports accessibility on Windows and you probably know
that in the US there is a law which states that if want to sell software to
the government you have to enable accessibility in your applications. What is
Trolltech going to do about this?
<p>

As you know GNOME has a very strong support for accessibility thanks
to the work of Sun, Gtk+ provides simple bindings to accessibility
devices. Qt in this regard is quite behind. And there is work going in
KDE in this field too.  So I would like to know what do you plan to
do?
<p>

<b>EE</b>: We're working on it, there has been a lot of things going on in Qt
development lately. This is something we are actively working on.
<p>

I spoke with the developer who has started working on the
accessibility module of Qt. This is definitely something that is
needed, but it has not been on the top of the list for many of our
customers. This is why it took us so long to start this project.
<p>

<b>ME</b>: There some problems when doing accessibility on Linux right now.
First of all, the market is not yet demanding.  I mean, there is no
customer saying "This is something that we absolutely require right
now".  Another problem is the work done by Sun: the ATK on Linux.  The
trouble is that an accessibility framework for a toolkit is not useful
if you don't have the integration and a tool to actually test the
development. This is a bit of the chicken and egg problem.  It doesn't
make sense to develop applications if you don't have the controlling
tool to actually use these things.
<p>

In order to implement accessibility in a toolkit you need another
middleware infrastructure that can bind that in.  That means
accessibility require that your applications can actually talk to each
other. You need an IPC system. This is all built into windows but a
standard is lacking on Linux.
<p>

So the first thing that we need to have is an IPC protocol. I think
GNOME is using CORBA for that, so we would have to integrate that
first before we could use the accessibility protocol. It's not that
easy, it's an infrastructure, you can't just sit down with a developer
for a couple of weeks and then you have it. It is actually much more
work.
<p>

But there is work going on in the KDE community and I hope that we in
Trolltech will be able to provide resources in the near future to
solve this problem.  Because it is not enough if we [Trolltech] do
something, it's important that the Free Software applications support
it too.
<p>

<b>PF</b>: You said that the business is not ready to receive it, that there is no
business demand. Maybe if you start to propose, the business will move toward
accessibility. Then you could be proposing instead of just waiting for people
to ask.
<p>

<b>ME</b>: Sure, but this is a game, this is an investment. Trolltech is, like Eirik
told you before, cash-flow positive. The investment can push developments. If
we had had enough resources, I would have worked on that five years ago.
<p>

<b>PF</b>: What do you think of the Opie project and its relationship with Qtopia.
Is it a competitor? Are you thinking about reintegrating some advances of
Opie back into Qtopia?
<p>

<b>EE</b>: We think it's a very good thing that there is Free Software project now,
working on PIM application suite.  We have very good contact with the group.
From what I understand, we already have integrated patches from the Opie
project into Qtopia. We are working together with them to make sure that we
have binary compatibility on the infrastructure.
<p>

It is again limited how much a company can do. This where the Open Source
model is excellent, as long as there are enthusiasts out there, willing to
work on it and  making it better.
<p>

We don't see competition since this is a Free Software project using Free
Software licenses. Our business model is based on cooperating with Free
Software projects, making sure that people who use our software for Free
Software can use is for free. Our customers, however want commercial licenses.
They pay it for development and for the support.  We think that this is a very
good model and it is working very well.
<p>


<b>PF</b>: Qtopia is based on Qt 2. Is it expected to move Qt 3? And if it does move
will you keep the binary compatibility so that legacy applications can still
run?
<p>

<b>EE</b>: Complicated question.
<p>

Of course one possibility, that Matthias is mentioning to me here, is isolation
of two libraries for a true binary compatibility in the future. So it is
possible, but it is not a very good solution because it uses a lot of memory.
<p>

One of the historical reason to keep Qt 2 for Qtopia is that many of our
customers are very focused on stability and as you mentioned binary
compatibility. 
<p>

Qtopia will more than probably integrate into the current version of Qt in
the future. But we have to do it in a sort of one big
step for our customers. It would probably be provided as a binary
compatibility option as Matthias mentioned. But our main focus is on keeping
most of the source compatibility, so there we will be more or less a simple
recompile to make these applications work on all platforms. I think it has to
be one big chunk of operation, when we move to new level and break the binary
compatibility. 
<p>

<b>PF</b>: Some people complained that some parts of Qtopia are not available
under a free license, for example, the desktop. It doesn't really make any
sense, why not release the desktop under a free license so we can use it, 
even on more platforms like Mac OS?
<p>

<b>EE</b>: It has to do with historical reasons and it has also has to do with the
business-model. We do that to make sure that there is one component there that
customers need and so they come in contact with us. There are other parts of
Qtopia that are not completely free, for example: parts of the e-mail reader as
far as I remember. It is always a fine balance for Trolltech and I think in
this case it turns out to be a good solution. But I do see the problem there
with the Qtopia-desktop.
<p>

<b>PF</b>: Was Zaurus a success? Did it sell well? Do you think there is there really
a future with Linux on the PDA?
<p>

<b>EE</b>: Yes, I think definitely there is a big future for Linux on the PDA? Did
it sell well? We are not allowed to go out with Sharp's numbers
unfortunately. I think it is selling well in some markets. It was a very good
first-time project. It was the first large conceiver brand that embraced Linux
in this way and I think we are going to see Sharp coming up with new
generations, which are going to become better and better. I know that 
in the enterprise market they are doing very well. So in general: Yes, it has
been a success. It has been a very very important direction for Trolltech, for
the Qtopia-product and make Qtopia known in a lot more circles.
<p>


<b>PF</b>: Recently there was an announce about a big electronic consortium being
formed to use Linux on embedded equipments. Does Trolltech have plans
to contact them and do something with them?
<p>

<b>EE</b>: We are in contact with them right now to see how we can cooperate. We
think it is very exciting that so many large companies are backing Linux on
portable devices this way.
<p>

<b>PF</b>: There are many many free software projects trying to use Qt in different
languages with Python, Ruby, Java, C#, Ada. Some bindings are missing, some
are half-done, some are done very professionally. Are you supporting this kind
of development? I mean, are you just watching them or sometimes would you be
interested in supporting the people doing this in order to have a wider
possibility for the usage of Qt?
<p>

<b>ME</b>: We have had contacts with some of the authors of bindings, so we support
them on request and have contact and talk to them. We keep that option open to
do more things with bindings in the future. Our current stand is that there is
very little commercial interest in those bindings. There are also surprisingly
little Free Software using them. There are very few applications being written
with those bindings. 
<p>

Qt as API shines most with C++. Together with C++ the overall value of Qt is
much bigger than when you do, for example, use Python-bindings. One of the
reasons for that is that Python - but you could pick Perl or any other
language - have a large set of libraries and additional tools.
<p>

Python has stuff to do network, server programming, to do I/O, XML. If you put
Qt on top of that, you are only interested in the GUI, whereas the C++ people
will use the XML API, the file abstraction and all the things so the value is
much higher.
<p>

<b>PF</b>: The PyQt/BlackAdder license is so cheap in comparison with the full Qt
license. How can you make money out of this ?
<p>

<b>EE</b>: It is a very limited market as Matthias has mentioned. And it is also seen
as a commercial experiment to see how much interest there is in it. And we
have been marketing together with TheKompany on it. The question is good
and so far, at least from our side, it has been disappointing how few
companies actually have an interest in developing using such a tool . I guess
that comes back to, as Matthias said, Qt is really a C++ toolkit and that's
where it shines and really add value.
<p>

<b>PF</b>: If you look back at everything you have achieved with Trolltech, what
would you like to underline, what are the things you are proud of, or the
things you really regret that happened, the mistakes you made?
<p>

<b>EE</b>: First one is really easy to answer, what's makes me personally most proud
is the fact that we have made life a lot easier for a lot of people who are
developers out there. When I started looking at multi-platform GUI toolkit
together with Harvard back in 1990, we saw that the API was terrible and that
they were so difficult to use. You had to spend a lot of time reading the 
documentation to find how to use the tools, and we decided that it could be a
lot better. You could make an API that really is logical and 
easy to use and you could make the application developers concentrate on the
creative part of the work and actually making their application logic, not
using the toolkit. In many ways, we have achieved that. I still get a thrill
out of the feedback customers saying "Wow, this is fantastic, you've really
given my the fun back in doing the job". And that's what I am the most proud
of. I mean, commercialize the toolkit that makes daily works of programmers
much better.
<p>

<b>PF</b>: And on the things you regret and wish did not happened?
<p>

<b>EE</b> (laughing): That's easy to answer too. I let Matthias answer.
<p>

<b>ME</b>: It's an open secret that the full opening of Qt came to late. We
should have done that earlier.
<p>

At the time where we were doing the first free version of Qt, there were
people saying that GPL means you can do everything with it. So people don't
know much about free software licenses in the market. So I don't know what
would have happened. I don't know what would have happened if we had GPL Qt in
1995.  Maybe Trolltech would not exist today. You never know. But I 
personally do regret that we could not do it earlier.
<p>

<b>EE</b>: Yeah, I agree. But also, part of the discussion at the time was that if
you go there, you can possibly come back on the decision. We saw that once we
were going GPL, there were no going back. There was a lot of internal
discussions about the risks. For example, a company could start
a fork, for example Red Hat, and we would lose control about what was the only
bread and butter of the company. But as Matthias said, once we did it, it was
amazing to see the effect that we got so much positive PR, and good will in
the community. In practice, nothing changed. We still have the control,
people respected that Trolltech had the latest official version and things
continued as they were previously. It was the right choice but, yes, we could
have done it earlier.
<p>

<b>PF</b>: Do you plan to extend your business in the future to something other than
strictly selling a library? How about selling books, packaged software,
trainings ? 
<p>

<b>EE</b>: We are already selling solutions to hardware developers of portable
devices: Qtopia. This is actually quite a different type of business,
although technically it's the same basis. 
<p>

We also do sell training classes, although we outsource most of the
practicality of it. But as a company, right now we are trying to focus on
things we're good at. Who knows what's going to happen in the future? We have
so many exciting possibilities within the basis we are operating in already,
development tools and platforms for mobile devices.
We almost have the opposite problem internally. We have to choose the best
options going forward and not spread too much into too many things.  It's
really important to focus on our core knowledge.
<p>

<b>PF</b>: Do you plan one day to make Qt Designer a full IDE? Do you plan to
separate it into different modules so it could be integrated separately in
other projects, like KDevelop or Kommander ?
<p>

<b>ME</b>:  
To question one: no, but one should never say never. 
<p>

The second question is a much more likely option and yes we do have concrete
plans. I can not promise a release date however. KDevelop has developed into an
excellent IDE. I think it's really amazing what the guys have achieved. This
thing is in many ways the period to what you get today on Microsoft Windows
from Visual Studio.  What the problem is with KDevelop right now is that the
GUI builder, which is Qt Designer is not available
as an integrated plugin. Today we can not offer that because the way Qt
Designer is written makes it very hard to take the form editor out and
integrate that into an external tool like KDevelop. We will work on making
this either possible through plugins or through libraries. That will take many
more months before that is finished. But this is one of the goals we have to
cooperate with the KDevelop team.
<p>

<b>PF</b>: As you know probably, IBM has released Eclipse and the SWT, an IDE and a
GUI toolkit for Java built (unlike Swing) upon native toolkits. It currently
uses Gtk+. Is it planned to provide it for Qt? There is a license
incompatibility, which is hard to deal with but achieving that would be very
exciting.
<p>

<b>EE</b>:  We are in contact with IBM and have been for some time over this.  And as
you mentioned, it's basically a licensing issue.  Our business model is based
on the fact that developers who want to use Qt to make closed source software,
have to purchase the license from Trolltech. It is very important for
us to maintain that model; that's how we survive as a company.  And we're
having problems finding a licensing solution for Eclipse to make this possible
for us in the future.  So we're still talking to them, but it's difficult for
me to see how we can find a licensing solution, unfortunately.  But I agree
with you, it would be great if we could make this happen.
<p>


<b>PF</b>: Have you encounter any competition with Gtk, in the commercial software
field.  Are you aware of a situation where you have lost customers to Gtk or
people just told you "we prefer Gtk and won't use Qt at all"?
<p>

<b>EE</b>: The short answer is no.  
<p>

<b>PF</b>: And in the opposite case?
<p>

<b>ME</b>:I think that even if you don't believe it, it is true, actually true.
<p>

<b>EE</b>: Basically, the company we are in contact with, want a C++ solution,
because Qt is only available on C++ . If you make a comparison between Gtk+
and Qt, for C++, Qt wins every time. We haven't seen any company that chose
Gtk+ over Qt. When we are talking about C as a it's a different story, because
you have the opportunity to use Gtk with C and you cannot use Qt with C. I
would suspect that a society looking for a C solution would probably would go
with Gtk, but I've never heard about them.
<p>

<b>PF</b>: You were talking about Red Hat and you were afraid about Red Hat forking Qt
and making business of it.
<p>

<b>EE</b>: In fact, in stone ages, that' right.
<p>

<b>PF</b>: Have you been in competition with them regarding GUI development: they
have a strong position in Linux and they are pushing GTK. Have you been facing
them commercially in this area?
<p>

<b>EE</b>: No, actually we have not. Not, that I know.
<p>

<b>PF</b>: And have you tried to approach them and try to convert them to Qt instead
of GTK ?
<p>

<b>EE</b>: Not really. We have been in contact with them several times through the
fact they have chosen GTK for a lot of their projects. Again for C++ Qt is
superior and I believe for any C++ project out there people would choose Qt.
In the end, no, we're actually working on it.
<p>

<b>PF</b>: What do you do to make Qt adopted everywhere? 
Are you active about talking to companies that are already using GTK and
converting them to Qt?  For example MandrakeSoft that is using GTK for all
his configuration tools, Macromedia for his RealPlayer, no it's not
RealPlayer, Acrobat Reader which is using Motif, and things like that...
<p>

<b>EE</b>: From time to time we are in contact with those companies explaining them
the advantages of QT but I think we bumped into licenses issue or language
issue.  We're working in this base.
<p>

<b>PF</b>: Are you aware of companies that are writing really new consumer products
converting their currents ones to Qt, except for the one you have been
advertising on your website?
<p>

<b>EE</b>: Nothing we can talk about right now. Most of our customers would like to
keep their project secret until they're ready with the final version.  So,
they're nothing that we can talk about right now.
<p>

<b>PF</b>: I read a few articles on the net, saying that basically all the movies
visual effects companies are moving onto Linux. You are advertising one on
your website, which is moving to Qt. Have you seen big move to Qt in this area
? 
<p>

<b>EE</b>: Yes, we are seeing a big move in this area. And it is basically to move
on some customers in this base. Others have been interested because of
that. We are seeing a lot of those companies moving to Qt right
now.
<p>

Actually, the market where we see most movement right now is this one
and next is EDA (Electronic Design Automation) software tools that layout chip
design. We see a lot of companies that are also moving to Qt.
<p>

<b>PF</b>: So that's very specialized high market. For very dedicated and very
expensive tool.
<p>

<b>EE</b>: yes
<p>

<b>PF</b>: Regarding the availability of Qt 3 for Windows, have you think of a
solution to the problem that people cannot build free software on windows with
Qt. They really regret that. Would it be possible to have something like a way
to submit a project to Trolltech and receive the project back built on windows
so that they wouldn't break the license. They wouldn't use a free version of
Qt but they would be able to provide their software on windows.
<p>

<b>ME</b>: The problem is not so much our licensing, we wouldn't have a problem with
that at Trolltech but the problem is typically the Free Software licenses.
You cannot take a GPL Linux program, compile them with Qt for windows and
give the executable to somebody else, because you would violate the GPL.
[Ed: because Qt on windows is closed source] 
<p>

So this is the biggest concern. We could even do a non-commercial
version for windows like we did with version 2 but the trouble is that,
because Qt is GPL you cannot use this version to port Free Software to
windows, so the only option they really have is likewise, that would be a GPL
windows version and Eirik talked about that earlier.
<p>

<b>PF</b>: It's just about putting an exception in the license of the software, I
think many people would be ready to do that to use Qt. Right now what stops
them is the price of Qt for Windows. I was talking at the manner they are not
getting the license but still be able to distribute free software on windows.
<p>

<b>ME</b>: It didn't happen but we have released the non commercial version for
windows. Most free software projects, once they are out, get so many
contributors, all under Gnu GPL, so you can not really add such an exception,
later. Also, I don't forget that they're is an option and an option to use of
the Cygwin and use the X11 for windows version GPL X11 version of Qt.
<p>

<b>PF</b>: Yes, but it's a heavy work for the one that are running the software, they
have to run the X11 windows system with Cygwin. It won't work with small
applications.
<p>

<b>ME</b>: Short answer: as a Free Software and KDE guy, yes, this is really
unfortunate but there is nothing we can do about this.
<p>

<b>PF</b>: How are people reacting to QSA, the Qt Script for Applications ? Have you already have a commercial reaction about it? 
<p>

<b>EE</b>: It has not been out there for that long but several customers needed that
thing and it looks very positive so far. We sold several licenses. 
<p>

This is very early in the process. Typically for a development tool like Qt and
QSA, commercial customers need one to three month to really get into the
product, evaluate it and see how they can use it. In few month we will see
more clearly how it succeeds in the market.
<p>

<b>PF</b>: Would you see QSA using other scripting languages, like for example python
?
<p>

<b>ME</b>: QSA is the JavaScript. We could build a scripting solution,
with other scripting languages, the binding technology is not tied to the
language at all. This is possible. It depends on whether we see the commercial
interest. The point is JavaScript/ECMAScript is very small language. If you
ship an application and you make this application and this languages very easy
to document and you can give that to your customers. If you bind python, your
clients get this huge programming framework with all the additional tools.
<p>

I mean that QSA embedded makes your application scriptable, it is not a competitor to python and PyQt at all, this is not the point. To be technically
clear, when you make your application scriptable in QSA, you do not have the
entire Qt API accessible on script. You just write Qt code with your scripts
but you have access to functionalities of your application, this is what people
do with it.
<p>

<b>PF</b>: And was it possible to use the KDE JavaScript that have been developed
for Konqueror?
<p>

<b>ME</b>: QSA is an extended version of the KJS engine.
<p>

<b>PF</b>: Is it really based on the same code ? Or it is just a rewrite.
<p>

<b>ME</b>: It is based on the same code and it was written by the same person, Harri
Porten, who wrote the KJS engine, put it under a license that explicitly
allowed to incorporate into other products. 
<p>

And he was doing that work for Trolltech. The difference is that KJS is aiming
for web browser, it means it support an older version of
ECMAScript/JavaScript, with a lot of compatibility things. In QSA, we
took a lot of those compatibilities out and followed the clean ECMA standard.
This is also a modern version of the language that has object orientation and
classes, that you do not have on the web JavaScript today.
<p>

<b>PF</b>: There was a real demand for the JavaScript language or you just
choose this one because you find it simple?
<p>

<b>EE</b>: ECMAScript or JavaScript looks like a very strong pretender to be a
standard embedded scripting language. We believe it is a very popular language
and has been used in many places. Python and Perl are much more used as a
stand-alone programming language. For example, Flash, is also used in
ECMAScript, that's many more things like that.
<p>

<b>PF</b>: One question about team builder [a Trolltech tool to distribute
compilations on multiple workstations] :  is it successful ? I know there is an
equivalent program running on Linux that does the same thing.
<p>

<b>ME</b>: It is not doing the same thing.
<p>

<b>PF</b>: And, is it selling well?
<p>

<b>ME</b>: It's selling better than what we invested in it, so in this way, it is a
success. It is not a big part of profit income but it is a small extra product
and because of the income we get from it, we can actually invest on it and
improve it.
<p>

<b>EE</b>: It's a good product for larger customers having site licenses. They are
normally interested. They have a large development group and lots of
machines on the network.
<p>

<b>PF</b>: And to you plan to make it support the windows compiler too?
<p>

<b>ME</b>: Yes we do. It is not that simple because of the Microsoft compiler. It is
much easier if you use GCC on windows. But the Microsoft compiler, the
Microsoft "make" does not support parallel builds, so there is no option like
"-j and here we go". This does not work. There are other ways in order to
parallelise the build, that people could use but they are much more difficult.
<p>

<b>PF</b>: That's why you haven't released it initially ? That would really be a new
product.
<p>

<b>ME</b>: You know windows compiler pre-compiles header files and this is something
that does not really work if you distribute your build system.  But the
difference is, that if you use the Borland compiler on windows, it compiles
like 10 to 15 times faster than GCC. So you don't really need teambuilder.
<p>

<b>PF</b>: Qt is becoming really fat with GUI stuff, Qt template libraries, XML,
databases. Are you thinking about splitting it into different parts?
<p>

<b>ME</b>: Yes, but it can be done in a binary compatible fashion, that means it can
not happened before the next binary incompatible version, Qt 4.
<p>

<b>PF</b>: Which is... when do you plan the next binary incompatible version?
<p>

<b>EE</b>: It is always difficult to say, there are discussions going on internally
right now. We are working on the infrastructure for the next major release of
Qt, this going to be 3.4 or 4, or whatever, we don't know.
<p>

I think that we have a very good creative development model and we try not to
impose deadlines onto our developers.
<p>

<b>ME</b>: I would not hold my breath to wait for this version. We are having some
plans. Qt 3 has been out there for a long time. Right now, we could say that
wouldn't be within the next year, we'll have such a new version.
<p>

<b>PF</b>: Somebody mentioned that you have some specific prices for startup
companies. Is that true?
<p>

<b>EE</b>: We try to be very flexible when start-up companies contact us for
purchasing Qt. Sometimes we give early companies a delay in their payment,
but this is done on a case by case basis, by our sales people.
<p>

In general, we want of course companies to purchase in the normal manner from
us. We think Qt has a such a very high value to ease and speed development,
not to mention the multi-platform capabilities that it's well worth it.  I
think most companies with a good business model can afford the license prices
of Qt.
<p>

<b>ME</b>: On Windows, most small companies buy a windows license, an Microsoft
Office license and Visual Studio license. If they use OpenOffice and GCC, they
can easily afford the Qt license.
<p>

<b>PF</b>: How much of a nightmare is it to maintain Qt for so many systems platform
what are the main difficulties? What are the easiest to maintain platform?
<p>

<b>ME</b>: The easiest platform are always the platforms that you use mainly
yourself, where you write the code. I mean the various version of GNU/Linux
are not a problem with X11 because that is what we use.  Also, the latest
versions of windows is typically a platform where there is no problem.
<p>

What is problem is the older versions. Unix has been around for ten
years. They still sometimes quite support the POSIX API, they have 
old AIX servers, none of the new AIX extensions is available. So on HP and
AIX, we sometimes have big problems.
<p>

On the other hand, on the windows platform we still support Windows 95 and 98
and it requires a lot of work as well.
<p>

<b>PF</b>: Are you still maintaining the three versions: Qt1, Qt2 and Qt3, all of
them at the same time on all platforms?
<p>

<b>EE</b>: No, we maintain version 2 on embedded platforms and we maintain version 3
but in version 3 we maintain 3.0, 3.1 and the next version, that would be 3.2
.
<p>

<b>PF</b>: Have you ever thought of integrating something like curses and providing
the possibility of using a text-based backend for Qt.
<p>

<b>EE</b> (laughing): Maybe for Qt 64? No, we did not thought about this. We don't
see any market there. That would probably be a fun experiment, just as 
a weekend vacation hack thing and we welcome people to try. But we don't
see a big commercial interest in such a product today.
<p>

<b>PF</b>: Why do you keep your documentation tool secret? And why don't you switch
to Doxygen?
<p>

<b>ME</b>: You know the history of Doxygen?
<p>

Trolltech in 1995/1997 used the internal documentation tool to generate the
documentation. Trolltech did not release the documentation tool because it was 
one big hacky Perl script and it did do exactly what it did, which was
creating the Qt documentation so it wasn't really flexible enough to deploy in
an application. When we released the free version there was a need for a
documentation tool that followed Qt-style documentation. And that was
Doxygen. Somebody wrote a C++ version with Qt to generate the Qt 
documentation and application documentation. That is Doxygen. Doxygen
evolved very well, so in application code Doxygen is absolutely a good 
choice. We still use our internal tool because we want to be able to 
change something easily, like going ahead. When we introduced 
properties we had to document them and we changed our documentation tool.
It also happened to integrate walk-through and tutorials. We see
documentation as a big and important integral part of our product. And this is
why we would like to have control, but our documentation tool is not general
purpose enough that we could productify it. So we recommend people and our
customers to use Doxygen.
<p>

<b>PF</b>: But don't you think that by making totally open you could have 
had some improvements because instead of writing Doxygen someone could 
have worked on your own tool, and you could had leveraging the free software
effect?
<p>

<b>ME</b>: Nobody could have worked on it except ourselves !
<p>

<b>PF</b>: Okay, I see. Hacky Perl scripts...
<p>

<b>PF</b>: Okay. I wanted to congratulate you for your excellent advertising in 
commercial magazines with the Qt ads.
<p>

<b>ME</b>: So you like that.
<p>

<b>PF</b>: Yes, we found that very funny and it is exactly like we feel with Qt, the
"unfair" advantage. I have been displaying them all at my workplace, for my
colleagues.
<p>

<b>PF</b>: You probably know that XFree86 had 
internal problems recently, with Keith Packard mainly complaining that 
XFree wasn't moving much, that the core developers were not 
open enough to allow public access to the core. Do you have 
any opinion on this matter? 
<p>

And secondly, I what is your opinion about how XFree fits with Qt development
?  With all your experiences with the various graphical systems on other
platforms, where do you think it should really be improved and what is ready
good in XFree. 
<p>

<b>ME</b>: That was many questions.
<p>

<b>PF</b>: There's a technical question and a political question.
<p>

<b>ME</b>: Okay, first the political one. We won't comment on the fate of 
XFree86 development, but what we will comment on is that, of course, 
we like to see new things in XFree86 and we appreciate that the 
development there was done in order to get anti-aliased fonts, in order 
to get alpha blending and render improvement. We think this is great 
development. Personally I hope there won't be a big split in the 
XFree86 community but both parties can work together well. And 
regardless what happens, I mean any significant X server or X 
server extension that has a significant distribution and really adds 
value is something that Trolltech will support. So we really see us in 
a position where we use and support XFree86 whatever they come up with.
<p>

<b>PF</b>: Yes. Now, regarding the technical qualities of XFree86?
<p>

<b>ME</b>: Yes, XFree is quite old. And it's surprisingly how much
   you still can do with it because you can extend the protocol with
   extensions and they were very smart to architect it this way. So
   we actually get very far. There are also many things we would wish X 
would do but that wouldn't be X anymore. One example that really annoys us is
that everything is still 16 bit. Now, a window and all the drawing and all
related is implemented in 16 bit. I mean, you can't display a long web page or
long document without doing really hacks because of the 16 bit limitations. I
think having something like that in 2003 feels a little bit odd. 
<p>

On the other hand the server is everywhere and it's very well distributed and
they did an excellent job. It's a great achievement, a piece of great
technology.
<p>

<b>PF</b>: Do you plan to move to direct frame buffer support in the future?
<p>

<b>ME</b>: We have Qt/Embedded that works extensively on the frame buffer. Is
that what you mean?
<p>

<b>PF</b>: Yes, but this is Qt/Embedded. Do you plan to provide this thing for the
main Qt version? Or is it actually the same thing? I'm not technically
familiar with this.
<p>

<b>EE</b>: I think that Matthias said that with the current distribution of the X
window system, a Qt frame buffer version doesn't make sense at all.  The X
window system is a standard on Linux today on the desktop.  We follow the
markets and the standards. 
<p>

<b>ME</b>: I saw on the dot that some people keep repeating that X is so
horribly bloated and needs to be replaced on the desktop. We do not share this
opinion. X is an excellent system on the desktop computer. Today's
desktops' computers have enough memory, X is not really the problem.
I am using the X remote features fairly extensively.
<p>

<b>PF</b>: Somebody mentioned that the Canopy Group & SCO owns some parts of
Trolltech.
<p>

<b>ME</b>: Sorry, we don't have any influence on them.
<p>

<b>PF</b>: Do they have any influence on you?
<p>

<b>ME</b>: Not really. They have a 5.7% stake in Trolltech. Historically Canopy
became an investor because we cooperated with Caldera. As you might know we
made and delivered the graphic install, which was the first graphical install
for Linux, for Caldera Linux. The Canopy Group as the main investor in
Caldera was so impressed by the work we had done that they wanted to invest in
Trolltech, to make sure that Trolltech could become a solid company that could
continue to deliver software to the Linux community. It's pretty ironic to
see what has happened historically after that of course. But they don't
have any influence on Trolltech. Trolltech is employee-owned, 65% of the
shares are owned by the employees and we control the business so they have a
small stake in us and that is it.
<p>


<b>PF</b>: You haven't talk about this complicated with SCO on Linux
<p>

<b>EE</b>: The patent issue or the corporate issue?
<p>

<b>PF</b>: The thing that SCO is asking and preparing to sue everybody about some
code they pretend they own in Linux.
<p>

<b>EE</b>: I can tell you that we do not support these actions from SCO. Trolltech in
many ways is dependent on the success of Linux. We think Linux is a Good
Thing.  We support Linux in many ways. On the other hand everybody has
the right to bring his case to court. In this case it is very strange that
they have not pinpointed exactly where in the code there is a problem and we
feel that if they really had a problem with this, they could have acted very
differently in presenting this to the community. So again we do not support
these actions.
<p>

<b>PF</b>: You have any position on software patents? Especially since in the EU
there is going to be a law to be passed soon.
<p>

<b>EE</b>: Trolltech is against software patenting. We think it is a bad thing and we
see with horror what is happening to the US software market because of the
patent policy over there. From my limited understanding of the subject, US
patent law isn't that bad, it's the actual application of that law by the US
patent office which is the problem. We sincerely hope that we will not get a
parallel situation in Europe and we think that would be a catastrophe to the
software industry in Europe. We think that we are well protected by copyright
laws and other laws. we think that software is a very different product
from other types of commercial production products. And we think that it
is very important for innovation that people can continue to share ideas and
that companies are not allowed to patent things which are very obvious.
<p>

<b>PF</b>: Thank you, I think I'm all done with all my questions. It was quite a long
interview. Anything you would like to add in this conclusion?
<p>

<b>EE</b>: I think we went through a lot of things. It was a very good idea of you to
ask the Dot for questions from people.
<p>

<b>EE</b>: Thank you for your time.
<p>











