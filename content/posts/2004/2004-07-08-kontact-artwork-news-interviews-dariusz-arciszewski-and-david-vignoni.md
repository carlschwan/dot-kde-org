---
title: "Kontact Artwork News, Interviews with Dariusz Arciszewski and David Vignoni"
date:    2004-07-08
authors:
  - "cwoelz"
slug:    kontact-artwork-news-interviews-dariusz-arciszewski-and-david-vignoni
comments:
  - subject: "Nuvola is beautiful.."
    date: 2004-07-08
    body: "hope to see it in kde 4.0!! plastik+nuvola default plz. k. thx. "
    author: "anon"
  - subject: "Re: Nuvola is beautiful.."
    date: 2004-07-08
    body: "Yes, KDE definitely needs highly talented artists such as these two, and yes, making the plastik theme the default would be the least that could be done. It looks so much better than the current one.\n\nI also wish they'd use a solid-gray kicker in most of the official screenshots. It looks better than the one with the ugly gradient..."
    author: "Mikhail Capone"
  - subject: "Re: Nuvola is beautiful.."
    date: 2004-07-08
    body: "Have a look at this: http://www.kde-look.org/content/show.php?content=14171"
    author: "i_love_the_dot"
  - subject: "Re: Nuvola is beautiful.."
    date: 2004-07-08
    body: "It's nice, but doesn't quite integrate as well as the plain gray kicker.\n\nMaybe if I changed the colour of the windows from gray to gray-white it'd be better..\n\nAlso, the GTK tray apps don't seem to fit into it (look at my gaim tray icon) and I'm not sure if I like or not the darker gray of the windows in the taskbar.\n\nOf course these problems are more with the kicker than with that kicker theme you recommended, so once they are fixed it would be more useable."
    author: "Mikhail Capone"
  - subject: "Re: Nuvola is beautiful.."
    date: 2004-07-08
    body: "I also hope that plastik + nuvola becomes the standard look.\nI'm using it all the time and it's just makes the K desktop into a killer desktop =)\n\n/David"
    author: "David"
  - subject: "Re: Nuvola is beautiful.."
    date: 2004-07-08
    body: "I also think Nuvola is the best icon set. I prefer it to crystal SVG also beacause 16x16 are cleaner and goodlooking. Hope to see it on KDE 4."
    author: "Lucas Contreras"
  - subject: "KDE needs more artists "
    date: 2004-07-08
    body: "I think that the KDE project needs more people Dariusz Arciszewski and David Vignoni. We have cool applications but they would look even cooler with some nice graphics. We have several wizards in KDE who could use a little artwork attention. This is were users (read: you and me) come into play. Will check BKO and submit these \"wishes\".\n\nThanks to all artists for their artwork (in KDE).\n\n\nFab "
    author: "Fabrice Mous"
  - subject: "Re: KDE needs more artists "
    date: 2004-07-08
    body: "for example, in kword there are no icons for \"increase text size\" and \"decrease text size\". which really sucks..."
    author: "superstoned"
  - subject: "Great!"
    date: 2004-07-08
    body: "That's great! I've just discovered Kontact and think that I'll start using it everyday from now on. I just wish it was more stable, but aside from that it rules. KMail seems very good too (I've just switched to it from Mozilla mail). \n\nLong live Kontact! Long live KDE!"
    author: "Mikhail Capone"
  - subject: "Kontact bug?"
    date: 2004-07-08
    body: "A bit off-topic, but I was wondering if I was the only one who got this bug; the \"summary\" doesn't seem to be refreshing very often. It still tells me that there is unread mail in my inbox after I've read it and such..."
    author: "Mikhail Capone"
  - subject: "Re: Kontact bug?"
    date: 2004-07-08
    body: "This still happens with KDE 3.3 Beta!?"
    author: "Anonymous"
  - subject: "Re: Kontact bug?"
    date: 2004-07-08
    body: "No, I'm still running KDE 3.2.3...\n\nHmm, this is strange. The \"about\" in Kontact says that I'm running 0.8.1. I thought that with KDE 3.2.3 it was supposed to be 0.8.2 or 0.8.3..."
    author: "Mikhail Capone"
  - subject: "Re: Kontact bug?"
    date: 2004-07-09
    body: "Someone just forgot to increase the version numbers for the releases."
    author: "Anonymous"
  - subject: "Re: Kontact bug?"
    date: 2004-07-08
    body: "This should be fixed now."
    author: "Ingo Kl\u00f6cker"
  - subject: "knode...?"
    date: 2004-07-08
    body: "Anybody know of free news servers? I was able to set-up  knode to get news feeds some time a go, but I am not able to do that now. For some reason all news servers I know do not work! Any help will be greatly appreciated."
    author: "charles"
  - subject: "Re: knode...?"
    date: 2004-07-08
    body: "http://news.individual.net/  (Usenet)\nhttp://gmane.org/            (Open Source Mailing Lists)"
    author: "Anonymous"
  - subject: "Ikons"
    date: 2004-07-08
    body: "Hope Kontact improves also in visual appearance. Splash screens are unnecessary, awaste of ressources that made sense when applications started slow. But KDE is faaaaast. "
    author: "martin runde"
  - subject: "Re: Ikons"
    date: 2004-07-08
    body: "Also, I think the sidebar in kontact should be replaced by the standard KDE sidebar (used in the prefs dialog, also in main interface of apollon)"
    author: "anon"
  - subject: "Re: Ikons"
    date: 2004-07-08
    body: "Or it should have a sidebar like the konqueror's one (in the filemanager mode). It's simple, nice and small."
    author: "i_love_the_dot"
  - subject: "Re: Ikons"
    date: 2004-07-22
    body: "FYI: In the upcoming release you'll be able to turn off the kontact splash using the --nosplash command line option.  You'll also be able to start\nkontact with a preferred module using the --module command line option.\n\nFor example:  kontact --nosplash --module summaryplugin\nwill run kontact without a splash starting on the summary view\n"
    author: "Allen"
  - subject: "Splashscreens inconsistency?"
    date: 2004-07-08
    body: "I don't understand why regular apps should have a splashscreen. I mean, why Kontact has a splashscreen but kopete don't? why K3B has a splashscreen but Juk don't?\nIsn't it a kind of inconsistency in the KDE desktop? Some apps have one, some other don't. Splashscreens look different from each other. They have different art styles.\nI think a splashscreen could be useful when the app take a while to load (like kde's startup), but kontact is fast, k3b is fast ... So why we need splashscreens?"
    author: "i_love_the_dot"
  - subject: "Re: Splashscreens inconsistency?"
    date: 2004-07-08
    body: "I don't see it as inconsistent, I see it as a difference in applications, and application design. Kontact, as well as K3b are significantly larger applications to Kopete. For instance, as far as I am aware (although I haven't checked the source), K3b initializes and checks devices and programs it depends on as the splash screen is displayed.\n\nI would, however, be in favor of consistant splash screen art work, to play into the KDE corporate image. Perhaps we could create a template, with a standard font and layout for artists to follow? Any thoughts?"
    author: "Martin Galpin"
  - subject: "Re: Splashscreens inconsistency?"
    date: 2004-07-08
    body: "Sounds like a great idea to me. Create a splashscreen dialog box, with an area on the bottom with some standard bits of information and a progress bar, then up top a standard sized box for displaying the splash logo. Use it in any KDE apps with significant loading times."
    author: "Kamil Kisiel"
  - subject: "Re: Splashscreens inconsistency?"
    date: 2004-07-08
    body: "Of course more discussion is necessary but if someone would like to do the artwork (hint hint David Vignoni), I'm willing to extend QSplashScreen for some standard functionality."
    author: "Martin Galpin"
---
In an effort to bring the <a href="http://kde-look.org/">kde-look.org</a> community's creative power to <a href="http://www.kontact.org/">Kontact</a>, a contest was launched some time ago: the <a href="http://wiki.kde.org/tiki-index.php?page=kontact+splash">Kontact Splash Screen Contest</a>. It's time to present the winner: Dariusz Arciszewski, and to know a bit about him. There are news at the icons front as well. <a href="http://www.icon-king.com/">David Vignoni</a>, of <a href="http://www.kde-look.org/content/show.php?content=5358">Nuvola Iconset fame</a>, is designing a set of task oriented icons for use in Kontact, replacing the application oriented icons. We asked David some questions about his work and KDE.

<!--break-->
<p><strong>Kontact Splash Screen Contest</strong></p>

<p>
The KDE community features many talented artists. The <a href="http://kde-look.org/">kde-look.org</a> website offers instant feedback, while the <a href="http://mail.kde.org/mailman/listinfo/kde-artists">kde-artists mailing list</a> works as the main forum to discuss KDE artwork guidelines and initiatives.
</p>

<p>
In an effort to bring this creative power to KDE applications, an artwork contest was launched: the <a href="http://wiki.kde.org/tiki-index.php?page=kontact+splash">Kontact Splash Screen Contest</a>. A big thank you goes to all artists that entered the contest. Good luck on the next contest! It's time to present the winner, and to know a bit about him. 
</p>

<p>
Many high quality entries were submitted, making the decision very difficult. In the end, the <a href="http://kde-look.org/content/show.php?content=12021">Kontact NG</a> splash, created by Dariusz Arciszewski, aka arcisz, was selected as the winner, and is already the default Kontact CVS splash. The Kontact developers liked the display of cooperation between PIM applications (the organizer, envelope and personal card together), the integration with KDE default icons and colors, and the design of the splash in general.
</p>

<p>
A special mention goes to the following splashes:
</p>

<ul>
<li><a href="http://kde-look.org/content/show.php?content=11992">Kontact Earthball</a>, by Nulltech. A favorite to many developers, and a really great splash. There is a version with lighter colors and a Kontact logo <a href="http://haef.szm.sk/file/kontact_splash_light.png">here</a>.</li>
<li><a href="http://kde-look.org/content/show.php?content=11961">Kontact Design</a>, by Emon. The developers liked its recoloring possibilities.</li>
<li><a href="http://kde-look.org/content/show.php?content=11984">Kontact Organizer SVG</a>, by arcisz too.</li>
</ul>

<p>
If you like any of these splashes better than the default one, it is easy to change the Kontact splash. Just rename the one you like to &quot;splash.png&quot; and replace the splash with the same name under $KDEDIR/share/apps/kontact/pics. If the folder indicated by the $KDEDIR environment variable does not point to the right place, replace it with the folder where Kontact is installed (usually &quot;/usr&quot; or &quot;/opt/kde3&quot;).
</p>

<hr />

<p><strong>Interview with arcisz, the Kontact splash screen contest winner</strong></p>

<p><em>
Hi, arcisz. Congratulations for winning the splash screen contest! We would like to know a bit about you: your name, where you live, your occupation, and how did you get involved with open source.
</em></p>

<p>
Hi! My name is Dariusz Arciszewski and I'm from Poland. I got involved with open source about six months ago. My first completed Open Source piece of artwork was the Crystal inspired icons (based on Everardo's icons) for the Kadu project, a Gadu-Gadu client. <a href="http://www.kadu.net/">www.kadu.net</a>. I have been a happy KDE user for about 2 years.
</p>

<p><em>
Now speaking about your artwork: Do you work professionally with graphic design?
</em></p>

<p>
I'm still working as an amateur.
</p>

<p><em>
Which tools do you use to create artwork?
</em></p>

<p>
To create my artwork I'm using only free/open source software (currently Sodipodi and Gimp, nothing else).
</p>

<p><em>
What do you think of this splash contest?
</em></p>

<p>
I think that this splash contest was a great opportunity for artists to show what they can do. With impatience I'm waiting for another :D.
</p>

<hr />

<p><strong>Interview with David Vignoni</strong></p>

<p>David Vignoni, of <a href="http://www.kde-look.org/content/show.php?content=5358">Nuvola Iconset fame</a>, is designing a set of task oriented icons for use in Kontact, replacing the application oriented icons (you can see a preview of these icons on <a href="http://www.icon-king.com/">David's website</a>. David needs no introduction, as his Nuvola iconset is the <a href="http://www.kde-look.org/index.php?xsortmode=high&page=0">highest rated iconset in kde-look.org</a>, and he is already an KDE contributor (he developed icons to <a href"http://edu.kde.org">the KDE Edutainment Project</a>, KMess, and other icon themes for KDE, like Lush and SKY). We asked David some questions about his work and KDE.
</p>

<p><em>
Where are you from?

<p>
I'm from Italy, I'm a 24 years old Italian / Brazilian guy.
</p>

<p><em>
What's your occupation?
</em></p>

<p>
Actually I'm student of Computer Sciences at University, I'll graduate in the beginning of next year. I spend a lot of time creating and studying graphics and web design.
</p>

<p><em>
How did you get involved with open source?
</em></p>

<p>
After migrating from Windows to Linux in 2000, I've migrated from Photoshop to The Gimp. Using this great software I've started doing wallpapers, splash screens and icon themes (SKY, Desktop icons) and publishing them at kde-look.org. In 2002 a guy of KMESS team (KMESS is a msn messenger for KDE) asked me to draw a new emoticon set for their application, and this was the first open source project I've joined as icon designer.
Then I've been involved with LibrSVG, porting to SVG Nuvola and Lush icon themes into the gnome-themes-extras package. The really last collaborations were with KDE-Edu and Kontact. 
</p>

<p><em>
Are you a KDE user?
</em></p>

<p>
Of course. I love Konqueror file manager and KDE stability. There are many powerful application in KDE, like Quanta, K3B, Kopete and KMail. Another important thing for me is the ability to configure KDE the way I like. I think this is is fantastic. What I miss are more window decorations and widget styles but I hope to design some in the future.
</p>

<p><em>
What do you like to do when you are not creating icons?
</em></p>

<p>
Oh well, I love to listen music, act in theater, work with computer and different operational systems, play soccer with friend, clubbing with friends and to spend time with my girlfriend. I'm a tranq??ilo guy.
</p>

<p><em>
Do you work with art professionally?
</em></p>

<p>
Yes, I offer my services from my website www.icon-king.com. I consider myself as a free-lance icon designer, I am available for custom icon designs and I've already worked with many American and European companies. These jobs let me spend money in software and hardware I use for creating icons. I hope this will become my full-time job, working for a company like Everaldo or Jimmac, or opening a studio. BTW, between them, Jimmac is my favorite icon designer.
</p>

<p><em>
What tools do you use to create artwork?
</em></p>

<p>
When I first heard about SVG as future standard format for icons I decided to switch to vector graphics. So I came back to Windows since the software I use, Adobe Illustrator, isn't available for Linux yet. Illustrator is a powerful vector graphics application that allow me to create any kind of icon. Right now I can't see any true valid alternative on the Linux side.
</p>

<p><em>
What is the community response to your artwork?
</em></p>

<p>
Nuvola is my most important project. It is a very nice and complete icon theme for KDE. Since the last public release on kde-look I've received tons of emails and messages from enthusiastic people. Since Nuvola icons are released under LGPL many companies are using them into commercial software and sites. Only Swell Technology has decided to support my work with a donation, and they are sponsoring  me  to continue developing the LGPLed Nuvola icons, that will also be used on a webmin theme that they are creating for their free and commercial products. Nuvola icons are also becoming default icons for many applications, like KDE-Edu apps.
</p>

<hr />

<p>That's all folks! I hope you liked it!</p>
