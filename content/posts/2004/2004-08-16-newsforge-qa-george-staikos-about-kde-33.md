---
title: "NewsForge: Q&A with George Staikos about KDE 3.3"
date:    2004-08-16
authors:
  - "binner"
slug:    newsforge-qa-george-staikos-about-kde-33
comments:
  - subject: "Wish for KDE 4.0"
    date: 2004-08-16
    body: "I have a little request for the upcoming version of KDE 4. After a little conversation with some people on IRC I was told that KDE 4 will become a bigger task of change (probably related to QT 4) and therefore I'd like to take the chance to ask for this wish and hope that developers might like this idea.\n\nRead more here: http://bugs.kde.org/show_bug.cgi?id=86986"
    author: "Ali Akcaagac"
  - subject: "Re: Wish for KDE 4.0"
    date: 2004-08-16
    body: "That sounds like a really good idea, I voted for it!  Seems like everyone would win with that change!"
    author: "Corbin"
  - subject: "Re: Wish for KDE 4.0"
    date: 2004-08-17
    body: "I think this is a very good idea. I voted for it, too"
    author: "Michael Thaler"
  - subject: "Wish for KDE 4."
    date: 2004-08-16
    body: "If you break things in KDE4 then break it completly, not just a little bit to make compromises, and make a clean and efficient design.\n\nhave fun hacking."
    author: "chris"
  - subject: "Re: Wish for KDE 4."
    date: 2004-08-16
    body: "I say if something should be changed but it will break things, then nows the time to do it!  Anything that will make KDE faster/smaller/better but may (or will) break old apps is worth it!\n\nThough if you plan on breaking something just for fun that may not be the best idea (unless fun = making KDE better ;-) )"
    author: "Corbin"
  - subject: "Re: Wish for KDE 4."
    date: 2004-08-17
    body: "But KDE alrealdy has a clean and efficient design. There are a few places where changing the design a bit will makes things easier but developers are certainly not going to throw the design that made KDE a success for the last years."
    author: "Philippe Fremy"
  - subject: "= offtopic ="
    date: 2004-08-16
    body: "hi sorry i know this is offtopic but i have to ask for it somewhere, perhaps it has to do with the feature of kde , so the interview :>\n\ni once had a kde version, when i tried to overwrite an mp3 with antoher mp3\ni got a dialog , where i could play both mp3's and after that i could decide which one i overwrite or rename.\n\ni also worked with images , i got presented with 2 image preview's so i could decide not only on the filename.\n\ni dont know if it worked with more things , but i can imagine....\n\ni think it was around kde 3.1 somewhere - then it disappeared , i thouht it was a really really useful feature.\n\nwhat do you think and know about this ????"
    author: "chris"
  - subject: "Re: = offtopic ="
    date: 2004-08-16
    body: "Install kdeaddons."
    author: "Anonymous"
  - subject: "Re: = offtopic ="
    date: 2004-08-16
    body: "yes I still have this nice option in 3.2.x"
    author: "JC"
  - subject: "KDE 4.0 Q&A"
    date: 2004-08-16
    body: "I may just of overlooked one, but I wanna learn about 4.0!  Someone should do a 4.0 Q&A or article!  4.0 looks like (and sounds) like its going to be a HUGE change, so all the stuff on it will be really intresting!\n\nIf I've just not seem them, does anyone have a link to one?"
    author: "Corbin"
  - subject: "Re: KDE 4.0 Q&A"
    date: 2004-08-16
    body: "The trouble is that no one is 100% sure what we're going to do yet! There are a large number of things planned, for example:\n\n- A new multimedia architecture\n- An improved notification mechanism\n- Better support for MVC in the APIs\n- Various API cleanups are already prepared (and marked in the header files)\n- Maybe the introduction of a bridge to DBUS\n\nA lot of the changes will depend to some extent on what ends up in Qt 4.0."
    author: "Richard Moore"
  - subject: "KDE 4 release estimate"
    date: 2004-08-16
    body: "This is from the interview:\n\n\" KDE4 is just around the corner now -- I predict late 2005 -- \"\n\nIt is a typo right ? Does he mean late 2004 ?"
    author: "MandrakeUser"
  - subject: "Re: KDE 4 release estimate"
    date: 2004-08-17
    body: "What year is this???\n\nHmm.. \n\nYeah. He probably means late 2004"
    author: "Allan S."
  - subject: "Re: KDE 4 release estimate"
    date: 2004-08-17
    body: "Nope.\n\nKDE 4 will be based on Qt 4.\nAnd Qt 4 will not be ready in 2004, perhaps early 2005.\nAnd then it'll take some time to port programs etc...\n\nSee: http://doc.trolltech.com/4.0/tech-preview.html\n\nQuote:\n\"Depending on the feedback from this Technology Preview, we will provide a second Preview in Q3 2004, enter the beta phase in Q4 2004 and release the final Qt 4 in late Q1 2005.\""
    author: "tbscope"
  - subject: "Re: KDE 4 release estimate"
    date: 2004-08-17
    body: "\"And Qt 4 will not be ready in 2004, perhaps early 2005.\"\n\nSo, wouldn't it make sense to make one more point release, \nsay 3.4 , before going 4.0 ? 6-8 months from now to Qt4 seems\nlike too long a wait.  And then all the porting you mention, etc. \n\nMore than enough time for another point release !"
    author: "MandrakeUser"
  - subject: "Re: KDE 4 release estimate"
    date: 2004-08-17
    body: "I don't think they'll do a minor version (e.g. 3.4.0), but will certainly do bugfix point releases (e.g. 3.3.x) in between 3.3.0 and 4.0.0."
    author: "Ian Eure"
  - subject: "Re: KDE 4 release estimate"
    date: 2004-08-17
    body: "Yes, that's my feeling from lurking a bit on the lists. Of course I have no time to develop, so I shouldn't be vocal. I have the feeling though that a minor release (with new features that don't break things) would be ideal. Think that if they go directly for a 4.0  we will either see no new features for about a year, or otherwise the 3.3 branch will end up being more than a bugfix branch, and will absorb new features and hence introduce new bugs (regressions), which would be a mistake IMHO ..."
    author: "MandrakeUser"
  - subject: "Re: KDE 4 release estimate"
    date: 2004-08-17
    body: "No, I don't think this is a typo.  Qt 4 has to be out first, and there are huge changes to KDE planned (see the other comments). "
    author: "cm"
  - subject: "Re: KDE 4 release estimate"
    date: 2004-08-17
    body: "Oops, it seems I haven't hit reload in an hour or so :-P\nNothing new in my comment."
    author: "cm"
  - subject: "Re: KDE 4 release estimate"
    date: 2004-08-17
    body: "KDE 4.0 will probably take more than a year, just as KDE 3.0 did. So late 2005 sounds about right."
    author: "Rayiner Hashem"
  - subject: "Re: KDE 4 release estimate"
    date: 2004-08-17
    body: "Why not to wait until 4.0 is finished ?\nNo need a release date. Just making a new great release.\n\nAnd as in the past for a major version it will take longer than a minor version."
    author: "JC"
  - subject: "Re: KDE 4 release estimate"
    date: 2004-08-18
    body: "I do not think that it is a typo.\n\nLate 2004 is just not possible, as Qt4 final will not even be released at that time.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: KDE 4 release estimate"
    date: 2005-08-23
    body: "A Question, what requeriment for KDE 4? My old PC is AMD DURON 950MHZ, 384MB RAM, GeForce2 32MB, i'm using SuSE 9.3, may my machine run KDE 4?"
    author: "Joel Zerpa"
  - subject: "Re: KDE 4 release estimate"
    date: 2006-02-12
    body: "That's funny. You answered to a post about 1 year after the last one.\nAs to KDE 4 we now know that it's going to be released in late 2006 or early 2007 I think. And I think it will support you old PC but you won't be able to switch all the graphic elements on.\nWell, At least I hope it's going to work on old PC since mine is a PIII 900MHz 256 RAM..."
    author: "Jonathan M\u00fcller"
  - subject: "Re: KDE 4 release estimate"
    date: 2006-02-24
    body: "Its funny you say late 2006 early 2007, I just think it is funny, ... started off saying late 2004 early 2005 not it is 2006 and still no KDE4.\n\nAlmost like Steam saying their releases and it is normally like a year or 2 later... \n\nI am not bashing I just happened to come across this thread and thought it was funny.\n\nPeople should always know to to ask about a release date because 9/10 it is never on time as originally expected, with respect to the developers always finding some bugs they want fixed before release, or new programs they would like to incorperate into it."
    author: "Narcussist"
  - subject: "Re: KDE 4 release estimate"
    date: 2007-02-27
    body: "And... Year later... It is even funnier now! I was googling for kde4 release date and this is what I got. I can not believe that it is still on hold. OK I want it all and I want it NOW. :-)\n\n<sarcasm>Well guess I'll have to wait till late 2008/early 2009</sarcasm>\n\nPS\nAnyone knows will it work on my old Dual Core 5GHz / 4GB RAM / 1GB NVIDIA / 1TB harddrive?"
    author: "Encho"
  - subject: "Re: KDE 4 release estimate"
    date: 2007-02-28
    body: "Hm I've read that it's supposed to come out in Fall 2007 and we can expect a preview (i mean a preVIEW not a dev snapshot) sometime in july or so. well that's theoretical of course but still. and as for computer ... i think that might work. try adding some RAM though ;-)"
    author: "Andrea"
  - subject: "Re: KDE 4 release estimate"
    date: 2007-02-28
    body: "Hm I've read that it's supposed to come out in Fall 2007 and we can expect a preview (i mean a preVIEW not a dev snapshot) sometime in july or so. well that's theoretical of course but still. and as for your computer ... i think that might work. try adding some RAM though ;-)"
    author: "Andrea"
  - subject: "Re: KDE 4 release estimate"
    date: 2007-10-05
    body: "This thread started on August 2004???\nThe estimated relase I think is now January 2008...\n... maybe, I will beleive it when I see it\nnearly 4 years delay so far\nI hope this (release) time is for real...\n(I wonder what we had said about XP or Vista comming 3 years later that initially scheduled)"
    author: "Jose"
  - subject: "Re: KDE 4 release estimate"
    date: 2007-10-05
    body: "Funny you'd mention it: \n\nFrom http://en.wikipedia.org/wiki/Windows_Vista : \n\n\"On January 30, 2007, it was released worldwide to the general public\" \n[...]\n\"Microsoft started work on their plans for Windows Vista (\"Longhorn\") in 2001, prior to the release of Windows XP. It was originally expected to ship sometime late in 2003 as a minor step between Windows XP (codenamed \"Whistler\") and \"Blackcomb\" (now known as Windows 7).\"\n[...]\n\"Some previously announced features such as WinFS were dropped or postponed, ...\"\n\n\nSound familiar? \n"
    author: "cm"
  - subject: "Re: KDE 4 release estimate"
    date: 2007-10-05
    body: "\"nearly 4 years delay so far\"\n\nWhat? Read up on your history; October 23rd 2007 is the *only* official release date ever given for KDE4.0.  Even going by that article, where a single KDE dev makes a personal prediction of \"late 2005\", the \"delay\" is little more than 2 years.  \n\nI swear, dot.kde.org users get dumber every day."
    author: "Anon"
  - subject: "Re: KDE 4 release estimate"
    date: 2007-10-09
    body: "I can't wait for KDE4 to come out! I just wish that it was going to be ready in time for the next release of Mandriva 2008. But that is cool. I'm getting ready to buy a new machine and I'm exited to use Compiz Fusion. I guess that is on Mandriva 2008. But anyhow KDE is way better than anything else out there."
    author: "MattKingUSA"
  - subject: "semi-OT: who is working on KHTML?"
    date: 2004-08-17
    body: "IIRC Dirk M\u00fcller did quite a lot of work on KHTML but from CVS commits it seems he has mostly stopped working on it. Is there anyone (and who) still working on KHTML?"
    author: "MK"
  - subject: "Re: semi-OT: who is working on KHTML?"
    date: 2004-08-17
    body: "I think it's mostly Leo Savernik, Germain Garand, and Tobias Anton working on various different parts of khtml lately. Stephan Kulow is helping with regressions, Zack Rusin is occasionally doing safari merges, Koos Vriezen is the main guy working on kjs. Waldo is doing occaisional bug fixes, and bj working on the access key stuff. \n\nSo yeah, Dirk doesn't seem to be working much on khtml anymore.. not as much as he used to at least. \n\noh well... Dirk was the main guy behind khtml for a while, but khtml has lost a lot of important developers before. Lars Knoll, the author/founder of the modern incarnations of khtml, became too busy a long time ago, as did Harri Porten, the author/founder of kjs. "
    author: "anon"
  - subject: "Re: semi-OT: who is working on KHTML?"
    date: 2004-08-17
    body: "> Is there anyone (and who) still working on KHTML?\n\nThe Safari team is doing major work on khtml, this has \"only\" to be merged with KDE's \"branch\"."
    author: "Anonymous"
  - subject: "Re: semi-OT: who is working on KHTML?"
    date: 2004-08-18
    body: "I hope that Dirk is doing IE-extensions to konqueror. Someone at Novell is doing this kind of work."
    author: "Jaana"
  - subject: "Where do you feel that KDE has to improve ..."
    date: 2004-08-17
    body: "> Where do you feel that KDE has to improve to gain more new users?\n\nWe must have better quality control; TQM should be our goal.  The problems like we had with the last few releases must not continue.  Better quality is more important to most users than \"cool\" features.  All regressions are infuriating to users.  Therefore, all regressions should be considered for showstopper status no matter how small they might appear -- they should be judged instead based on how many users they will affect.\n\nI noticed that there isn't one word about fixing bugs in the whole interview.  A large omission.  To gain new users, we need to ALWAYS ship a better quality product than Redmond does.  \n\nBut, it appears that there is hope.  If there will be no 3.4 and 4.0 won't be shipped till late NEXT year then we have a lot of time to fix bugs and improve our quality systems.\n\nTo be clear about this: I want to see a better quality KDE product.  Is there anyone that disagrees with that goal?\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Where do you feel that KDE has to improve ..."
    date: 2004-08-17
    body: "> > Where do you feel that KDE has to improve to gain more new users?\n> \n> We must have better quality control; TQM should be our goal. The problems\n> like we had with the last few releases must not continue. Better quality is\n> more important to most users than \"cool\" features. \n\nWell said.  For a start, there needs to be more backporting of bugfixes to previous KDE branches (not just backporting of highly publicized security fixes).  e.g. I'm using KDE_3_0_BRANCH and kde-config crashes given dodgy arguments, KMail indexes are frequently corrupted (so if you click on a mail in the search dialog, it lands you in the wrong folder; or message aren't automatically marked as read), Konqueror can't even display http://bugs.kde.org, Konqueror leaks uncontrollably at http://www.kdedevelopers.org, Konqueror javascript doesn't work half the time, starting 2 KDE apps in quick succession outside of KDE crashes with DCOP errors, ark can't open compressed files stored on vfat and brings up a bogus permission error (I'm using an unexpected scheme where I'm in the \"vfat\" group) etc. etc.\n\nThis is not quality.  This is not supporting branches about 6 months after the initial release.  Even MS gives more support."
    author: "w"
  - subject: "Re: Where do you feel that KDE has to improve ..."
    date: 2004-08-17
    body: "There is no good reason to run KDE 3.0 anymore (and to complain about its bugs today).\n\n> This is not supporting branches about 6 months after the initial release.\n\nKDE 3.0 is over 2 years old!\n\n> Even MS gives more support.\n\nAnd they request money for their product to do this."
    author: "Anonymous"
  - subject: "Re: Where do you feel that KDE has to improve ..."
    date: 2004-08-17
    body: "> There is no good reason to run KDE 3.0 anymore (and to complain about\n> its bugs today).\nUpgrading to a new release had the potential to break stuff - risk.\ne.g. most businesses are unwilling to upgrade to the next version of\nWindows just so that the icons look prettier for example - that want\nas little change as possible, just fixing bugs to reduce risk.\n\nFor a home user, it's the hassle of upgrading - either compiling from\nsource or using RPMs that may or may not work depending on how bad their\nLinux distributor is.  Not to mention the huge download on dialup.\n\nThese are pretty legit reasons I think not to upgrade to 3.3 instead of say 3.0.6 (if it ever comes out).\n\n> KDE 3.0 is over 2 years old!\nWindows 98 is over 6 years old and still provides at least the same amount\nof functionality for me (if not more, due to Office and market share / compatibility).\n\n> > Even MS gives more support.\n> And they request money for their product to do this.\nFair enough; KDE developers already create a great desktop when most are volunteers.  But this doesn't sit well with the enterprise IMO (\nhttp://desktop.kdenews.org/strategy.html).  I'm just not in a position to upgrade because of download and/or compile time."
    author: "w"
  - subject: "Re: Where do you feel that KDE has to improve ..."
    date: 2004-08-17
    body: "> For a home user, it's the hassle of upgrading\n> I'm just not in a position to upgrade because of download and/or compile time.\n\nYou have the option to pay for a new distribution in a store."
    author: "Anonymous"
  - subject: "Re: Where do you feel that KDE has to improve ..."
    date: 2004-08-17
    body: "> You have the option to pay for a new distribution in a store.\nbut i'm too cheap to do that since i would have to pay instead of my non-linux parents :)"
    author: "w"
  - subject: "Re: Where do you feel that KDE has to improve ..."
    date: 2004-08-17
    body: "But see -- there you've got it.  Nobody wants to do the gruntwork for fun.  If you want to have the boring, annoying jobs you're just going to have to pay someone to do this.  Commercial distributions tend to maintain older KDE branches for much longer -- for major bugs they'll usually release updates until the end of life for that product (normally a couple of years).\n\nBut you don't really think people are going to do that in the free time, do you?"
    author: "Scott Wheeler"
  - subject: "Re: Where do you feel that KDE has to improve ..."
    date: 2004-08-17
    body: "I do think you misunderstand the issue, open source is not about doing things for free. But sometimes it's more a natural path economy. When you solve a bug for yourself there is no incentive to exclude others. Just by the fact that persons use software they find bugs and submit reports that help others who try to track them down. It's no \"social revolutionary\" concept for people doing things for free as opposed to capitalism. Wiping out remaining bugs from a mature release is no big deal."
    author: "Gerd Lange"
  - subject: "Re: Where do you feel that KDE has to improve ..."
    date: 2004-08-18
    body: "In the particular culture of KDE, yes, most of the work is done for free.  The boring jobs -- such as backporting fixes to other branches of KDE (older than the last stable branch) is usually only done by people that are working to distribute KDE in some sort of \"value added\" sort of package where bug fixes for older releases are part of the support arrangement.\n\nAnd you're wrong about it being no big deal; code is a moving target and often the fix that goes into the current unstable development version is completely different from what would have been needed to fix a release from 2 years ago.  It's not a trivial process and it's certainly not much fun."
    author: "Scott Wheeler"
  - subject: "Re: Where do you feel that KDE has to improve ..."
    date: 2004-08-17
    body: "Wait, so you want extended support for obsolete versions, but don't want to pay a distribution that'll give you that support??? You can't have it both ways!"
    author: "Rayiner Hashem"
  - subject: "Re: Where do you feel that KDE has to improve ..."
    date: 2004-08-17
    body: "> Well said. For a start, there needs to be more backporting of bugfixes to \n> previous KDE branches (not just backporting of highly publicized security \n> fixes). e.g. I'm using KDE_3_0_BRANCH and kde-config [...]\n \n> This is not quality. This is not supporting branches about 6 months after \n> the initial release. Even MS gives more support.\n\nSo I take it you're volunteering to provide better support for older versions? \nGreat.  When can you start? \n\n\nNo, seriously, who should do it?  It would need manpower that the KDE team doesn't have.  Besides KDE is not a company with paid employees so it would be hard to find someone willing to do that.  It's not a very attractive task to backport *large quantities* of fixes to versions with a very low number of users.  \n\nAnd the point would be?  Making 3_0_BRANCH into a bad version of 3.3?  \n\nWhat's your reason for sticking with 3.0?  It must be a very good reason because KDE has seen performance improvements and bugfixes since then, besides the usual feature enhancements. \n\nIf you really need a version that old you might consider paying someone to do the backports for you. \n\n---\n\nOTOH quality control for the released branch and the new release is must, no doubt about that. "
    author: "cm"
  - subject: "Re: Where do you feel that KDE has to improve ..."
    date: 2004-08-17
    body: "> And the point would be? Making 3_0_BRANCH into a bad version of 3.3? \nWhy are there still Linux 2.0 releases?  Why doesn't everyone use 2.6?\n\nTo minimise change, higher stability (if things were backported).\n\n> What's your reason for sticking with 3.0?\nApart from that, my personal reason is the download and/or compile time is scary.\n\n> It must be a very good reason because KDE has seen performance\n> improvements\nFor sure no.  Time a 3.0 startup against 3.3.\n\n> and bugfixes since then, \nWhich should have been backported :)\n\n> besides the usual feature enhancements. \nAgreed.\n\n> If you really need a version that old you might consider paying someone to\n> do the backports for you. \nThat won't work - you need the actual developers doing it IMO.\nElse other fixes get swept underneath the carpet (e.g. a structural redesign to fix some bugs can't be backported directly - must hack around it in older branches).\n\nBut I admit that most of us are volunteers so I can't do much more than my share.  But if KDE is targeting the enterprise, this doesn't sit well."
    author: "w"
  - subject: "Re: Where do you feel that KDE has to improve ..."
    date: 2004-08-17
    body: ">> It must be a very good reason because KDE has seen performance improvements\n> For sure no. Time a 3.0 startup against 3.3.\n\nHow can you tell \"for sure\" if you're not knowing/running it? You're just trolling."
    author: "Anonymous"
  - subject: "Re: Where do you feel that KDE has to improve ..."
    date: 2004-08-17
    body: "> > > It must be a very good reason because KDE has seen performance\n> > > improvements\n> > For sure no. Time a 3.0 startup against 3.3.\n> \n> > How can you tell \"for sure\" if you're not knowing/running it?\n\n*sigh*.  i'm a developer.  i compile and run head (qt-copy, arts, kdelibs & kdebase only) under a different prefix under an ordinary user.  i am not in a position to compile 10 other modules i need everyday (kdenetwork, kdegraphics, kdeaddons, kdegames etc.) _and_ under a different prefix (since that's a waste of time recompiling qt-copy, arts, kdelibs & kdebase).  a 1998 computer is just too slow for that.\n\nif you're going to claim that i'm not compiling with optimization options as strong as my distro's binary 3.0, i have been developing for kde since 3.0 with the same build options as now with 3.3.  so i can tell the difference.\n\nyou can't tell me that kde requirements haven't gotten stepper.  remember around the time of kde 3.0, ppl suggested moving up from 64mb of ram to 128mb for reasonable performance.\n\n> > You're just trolling.\nAnd you're just grandstanding?  And/or just to conclusions too quickly."
    author: "w"
  - subject: "Re: Where do you feel that KDE has to improve ..."
    date: 2004-08-17
    body: "The applications that you have issues with, kmail and konqueror have changed enormously even during the last year. The fix for khtml issues is to upgrade to the latest version. Kmail is almost a different application from 3.0. Hence, only the most serious security fixes will be backported. Indeed it is a manpower issue.\n\nAs for speed, you are missing the enormous optimizations that happened between 3.1 and 3.2. KDE finally started to feel snappy with 3.2.\n\nBinary incompatibility during the 3.x series is a bug that would be fixed.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Where do you feel that KDE has to improve ..."
    date: 2004-08-17
    body: "Derek, don't waste time with this troll, please :)"
    author: "Davide Ferrari"
  - subject: "Re: Where do you feel that KDE has to improve ..."
    date: 2004-08-18
    body: "> Derek, don't waste time with this troll, please :)\nThis is the problem with KDE community.  Every time some one says something politically incorrect, they get flamed."
    author: "w"
  - subject: "Re: Where do you feel that KDE has to improve ..."
    date: 2004-08-18
    body: "Why *politically* incorrect? Just incorrect- it's the obvious consensus here and elsewhere that KDE has been getting faster with recent releases."
    author: "Anonymous"
  - subject: "Re: Where do you feel that KDE has to improve ..."
    date: 2004-08-18
    body: "> Just incorrect- it's the obvious consensus here and elsewhere that\n> KDE has been getting faster with recent releases.\nSo my experience is incorrect apparently.\nAsk anyone who isn't incredibly passionate about KDE\n(i.e. does not read the dot regularly for sure) and\nask them if it loads fast.\n\nPerhaps it has been getting slightly faster with more\nrecent releases (again, it seems slower for me) but\nas an overall trend it is getting slower.  e.g. You\ncan't possibly tell me that it loads after than KDE 1 or 2.\nThat would be extreme BS."
    author: "w"
  - subject: "Re: Where do you feel that KDE has to improve ..."
    date: 2004-08-18
    body: "KDE 1 or 2 are not recent releases. The whole thread was about the KDE 3.x series - why do want to compare it now with KDE 1?"
    author: "Anonymous"
  - subject: "Re: Where do you feel that KDE has to improve ..."
    date: 2004-08-19
    body: "> why do want to compare it now with KDE 1?\noops, wrong thread sorry. another thread was made a claim that it has been getting faster with _every_ release."
    author: "w"
  - subject: "Re: Where do you feel that KDE has to improve ..."
    date: 2004-08-18
    body: "It loads and runs faster than KDE 2. It takes more memory maybe, but it's faster. KDE 1.x was a totally different architecture, and yes, KDE 3.3 is slower than KDE 1.x was."
    author: "Rayiner Hashem"
  - subject: "Re: Where do you feel that KDE has to improve ..."
    date: 2004-08-19
    body: "> It loads and runs faster than KDE 2.\ni really don't get it.  i changed the hostname thing and it's made no difference.  kde 2 was really really much faster for me."
    author: "w"
  - subject: "Re: Where do you feel that KDE has to improve ..."
    date: 2004-08-20
    body: "then please stick with kde 2 if you like. it sounds like you're running hardware that is 6 years old. you might want to run software for that hardware.  a troll indeed."
    author: "mark"
  - subject: "Re: Where do you feel that KDE has to improve ..."
    date: 2004-08-19
    body: "The problem is that some people talk about performance as \"speed when inside KDE\" and others consider performance to be \"loading time\".\n\nPersonally, I couldn't care less about loading time; I don't constantly reboot my computer and it's the performance inside KDE that matters. That part *has* been getting better (ie. 3.1 -> 3.2)"
    author: "Mikhail Capone"
  - subject: "Re: Where do you feel that KDE has to improve ..."
    date: 2004-08-17
    body: "> > If you really need a version that old you might consider paying someone to\n> > do the backports for you. \n> That won't work - you need the actual developers doing it IMO.\n\nWell, in many cases it could be the actual developer who could get paid.  :-)\n\n\n> Else other fixes get swept underneath the carpet (e.g. a structural redesign \n> to fix some bugs can't be backported directly - must hack around it in older > branches).\n \nExactly, it's not a trivial matter, and that's why I consider it too much to ask from volunteers to make the same fixes in 3_3_BRANCH, 3_2_BRANCH, 3_1_BRANCH *and* 3_0_BRANCH (or to even more, if I take your comparison to Win98 \"6 years\" into account). \n \nI'm using KDE at home and at work and I can live with regularly updating it  (Fortunately, I'm in the favourable position to decide myself what I run on my workstation even though I'm just an employee... I know, not everyone is that lucky.).  \n\n\n\n> But I admit that most of us are volunteers so I can't do much more than my \n> share. But if KDE is targeting the enterprise, this doesn't sit well.\n\nA distributor with paying customers demanding such a long-time support and maintenance could step in here..."
    author: "cm"
  - subject: "Re: Where do you feel that KDE has to improve ..."
    date: 2004-08-18
    body: "> A distributor with paying customers demanding such a long-time support and\n> maintenance could step in here...\nBut as I said, I don't think distros can do a good a job as the developers themselves.  They will miss important fixes."
    author: "w"
  - subject: "Re: Where do you feel that KDE has to improve ..."
    date: 2004-08-18
    body: "> > A distributor with paying customers demanding such a long-time support and\n> > maintenance could step in here...\n> But as I said, I don't think distros can do a good a job as the developers \n> themselves. \n\nBut as I said, the distributor can pay one of the original authors so they don't have to do it in their scarce free time after their normal day job. Free software is not necessarily about working for free.  \n\nBesides it seems that disributors *are* maintaining their older versions. Please read the posts by Scott Wheeler.  It seems he knows what he's talking about.  \n\n\n> They will miss important fixes. \n\nI don't think that's necessarily the case.  There are tools like the CVS commit mailing list.  There are announcements of important fixes.  There's the CVS digest.  Maybe others, I'm not working for a distributor.  But most importantly, there's working with the community and in the community, even by paying the authors to do the grunt work. But I'm repeating myself. "
    author: "cm"
  - subject: "Re: Where do you feel that KDE has to improve ..."
    date: 2004-08-17
    body: "> This is not quality. This is not supporting branches about 6 months after the initial release. Even MS gives more support.\n\nThere simply aren't enough developers, sorry. "
    author: "anon"
  - subject: "Re: Where do you feel that KDE has to improve ..."
    date: 2004-08-17
    body: "> There simply aren't enough developers, sorry.\n\nOne might consider of course to strip down KDE core if that would be the case. Eg. yes building the mime/kpart/dcop/.. infrastructure, but no for a kcm module that configures Fn keys on sony laptops (no offence btw. just an example - actually I would favor replacing 99% of these kcms to kde-bindings script versions anyway - that would save some compile time :-)"
    author: "koos"
  - subject: "KDE needs to be faster"
    date: 2004-08-17
    body: "The speed difference between Windows (98, XP, Server 2003) and KDE 3.3 is quite apparent.\n\nAt least start up time has been getting worse with every release.  I remember the days of KDE 2 which boot in 15s on my 1998 computer.  It now takes 60s.  I just can't believe the people who claim that KDE is getting faster."
    author: "w"
  - subject: "Re: KDE needs to be faster"
    date: 2004-08-17
    body: "The MS OS often loads the user shell first, then the services. When I compare my 800mhz PC (with Gentoo Linux and KDE CVS) and my parents' 1800mhz PC (with MS os) I see that my PC doesn't boot faster, but login is faster on my PC. (maybe partially because no AntiVirus and Zone Alarm stuff needed)"
    author: "wilbert"
  - subject: "Re: KDE needs to be faster"
    date: 2004-08-17
    body: "Use prelinking, buy ram or every five years a new computer."
    author: "Anonymous"
  - subject: "Re: KDE needs to be faster"
    date: 2004-08-17
    body: "> Use prelinking\nThat actually seems to _slow_ things down!\n\n> buy ram \n256MB isn't enough?\n\n> or every five years a new computer.\nEven on my 2.2 Ghz Celeron, it's slow (about 30s)."
    author: "w"
  - subject: "Re: KDE needs to be faster"
    date: 2004-08-17
    body: "> >  Use prelinking\n> That actually seems to _slow_ things down!\n\nThen you're making something wrong. Or you tried objprelink."
    author: "Anonymous"
  - subject: "Re: KDE needs to be faster"
    date: 2004-08-17
    body: "> Then you're making something wrong. \nJust letting Fedora Core 2's cron script do its work.\n\n> Or you tried objprelink.\nWhat's the difference?"
    author: "w"
  - subject: "Re: KDE needs to be faster"
    date: 2004-08-17
    body: "Fedora Core 2? Above you wrote\n\n> I'm using KDE_3_0_BRANCH\n\nSo, you did install KDE_3_0_BRANCH on Fedora Core 2 instead of using the contained KDE 3.2.2!?\nOr are you just an inconsistent troll?"
    author: "Anonymous"
  - subject: "Re: KDE needs to be faster"
    date: 2004-08-17
    body: "How is it so slow on your computer. I don\u00b4t have a 2.2Ghz (P4 1.6) and KDE loads in 6 completly in 6 seconds without any tunning in KDE startup script.\n\nYour packages are probably not well built."
    author: "JC"
  - subject: "Re: KDE needs to be faster"
    date: 2004-08-17
    body: "Or he's got that problem where the system can't find the hostname properly."
    author: "Rayiner Hashem"
  - subject: "Re: KDE needs to be faster"
    date: 2004-08-18
    body: "> Or he's got that problem where the system can't find the hostname properly.\nCould you please let me know where I can find more info on this?  I may be blaming the wrong people..."
    author: "w"
  - subject: "Re: KDE needs to be faster"
    date: 2004-08-18
    body: "If KDE apps seem to take too long to start, even on a fast system, make sure the hostname you have in /etc/hostname points to 127.0.0.1 in /etc/hosts."
    author: "Rayiner Hashem"
  - subject: "Re: KDE needs to be faster"
    date: 2004-08-18
    body: "> So, you did install KDE_3_0_BRANCH on Fedora Core 2 instead of using the\n> contained KDE 3.2.2!?\nNo:\n\n1998 computer: KDE 3.0 from bin packages, core of KDE 3.3 from source\n2004 computer: KDE 3.2 from bin packages, with prelinking even"
    author: "w"
  - subject: "Re: KDE needs to be faster"
    date: 2004-08-17
    body: "Hey, you have a 2.2Ghz machine? If I were you I'd do my development on that machine instead of the 1998 one you mentioned above...."
    author: "Apollo Creed"
  - subject: "Re: KDE needs to be faster"
    date: 2004-08-18
    body: "> Hey, you have a 2.2Ghz machine? If I were you I'd do my development on\n> that machine instead of the 1998 one you mentioned above....\nif a sibling didn't hog it for playing windows games :( i would "
    author: "w"
  - subject: "Re: KDE needs to be faster"
    date: 2004-08-17
    body: "\"At least start up time has been getting worse with every release.\"\n\nWell, I compared KDE3.2.3 to 3.3-Beta2, and found out it was exactly the opposite. beta2 booted several seconds faster than 3.2.3 did."
    author: "janne"
  - subject: "Re: KDE needs to be faster"
    date: 2004-08-17
    body: "I'm just saying as a general trend it's been getting worse (actually, I am also surprised that you found 3.3 to be faster than 3.2 starting up as well, but anyway).\n\nNeeding to time startup \"speed\" improvements with a watch or having a splash screen means it's still too slow IMHO."
    author: "w"
  - subject: "Re: KDE needs to be faster"
    date: 2004-08-17
    body: "I'm really sorry, but I dont get it. KDE 3.0.3 is slower in every aspect, at least on my pc. I've seen KDE become faster with every release. 3.0.x wasn't really pleasant on my pc, but now 3.3.0 is out, it runs smooth. login times decreased, and most apps start faster."
    author: "superstoned"
  - subject: "Re: KDE needs to be faster"
    date: 2004-08-17
    body: "Most of the perceived time these days is IO.  Log in and log out to get your disk buffers full and you'll notice that things are still pretty snappy.\n\nThe problem is that naturally we've gotten to where a modern desktop uses a lot more data -- whether applications themselves or the data they load -- than it did a few years ago.\n\nOn the other hand, harddrives while they've gotten a lot larger haven't gotten a lot faster.\n\nIf you're waiting on the harddisk it really doesn't matter if you've got a 700 MHz Duron or a P4 -- it's still going to feel slow."
    author: "Scott Wheeler"
  - subject: "Re: KDE needs to be faster"
    date: 2004-08-18
    body: "> The problem is that naturally we've gotten to where a modern\n> desktop uses a lot more data -- whether applications themselves\n> or the data they load -- than it did a few years ago.\n\nBut isn't there something KDE can do?  A (fresh) Windows install\nlogs in _much_ faster than KDE."
    author: "w"
  - subject: "Re: KDE needs to be faster"
    date: 2004-08-18
    body: "That's because Windows and KDE do it differently. When you log in to KDE and you get to the desktop, it's fully usable with all services and background-apps up & running. In Windows, you get to the desktop fast, but the system is still far from usable. It's still loading the background-services and the like. The time it takes to get to a fully functional desktop on KDE and on Windows is propably more or less the same. Windows _might_ be a bit faster, but the difference is not that dramatic.\n\nWindows only _appears_ to be faster since you get the desktop faster. But that doesn't mean it's faster in reality."
    author: "janne"
  - subject: "Re: KDE needs to be faster"
    date: 2004-08-18
    body: "As KDE does not create packages it can not prelink them."
    author: "Anonymous"
  - subject: "Re: KDE needs to be faster"
    date: 2004-08-19
    body: "Modern glibcs (2.3) do pretty much all of the stuff that used to be advantageous with prelinking anyway."
    author: "Scott Wheeler"
  - subject: "Re: KDE needs to be faster"
    date: 2004-08-18
    body: "\"I'm just saying as a general trend it's been getting worse\"\n\nAnd I'm just saying that as a general trend it's been getting better and better on my computer. Ever KDE-release is a bit faster than the previous one was.\n\n\"Needing to time startup \"speed\" improvements with a watch or having a splash screen means it's still too slow IMHO.\"\n\nHow fast should it be then? After a fresh reboot, it takes something like 10 seconds for KDE to boot on my machine (3.2.3 takes few seconds more). Subsequent logins take about 4-5 seconds (about 8 seconds on 3.2.3). And since I don't reboot my machine constantly, starting up KDE takes about 4-5 seconds for me. Do you expect it to be instantenious?? I'm sorry, but that's just not going to happen. 4-5 seconds is good enough IMO. And it's faster whn W2K is on the same machine.\n\nFWIW: I also run KDE on my ancient laptop. It has 300Mhz P2, really slow HD (hdparm gives it about 7-8MB/sec), 320MB RAM and unaccelerated vid-card. Of course KDE is slower there, but it's still usable. I should know since it's the only GUI I run on that machine."
    author: "janne"
  - subject: "Re: KDE needs to be faster"
    date: 2004-08-18
    body: "> > \"I'm just saying as a general trend it's been getting worse\"\n> And I'm just saying that as a general trend it's been getting better\n> and better on my computer. Ever KDE-release is a bit faster than\n> the previous one was.\nSo by induction on your argument, KDE 3.3 loads faster than KDE 1.0?\n\n> How fast should it be then? After a fresh reboot, it takes something like 10\n> seconds for KDE to boot on my machine\nYes that would be good but even on a >2Ghz system, KDE can't manage that.\n\n> I should know since it's the only GUI I run on that machine.\nUnfair comparison but try this: install a fresh Windows 98.  It boots _and_ logs in in 15s on a 300 Mhz! (that's faster than the BIOS).  Also on the same machine Windows Server 2003 logs in faster and apps pop up faster."
    author: "w"
  - subject: "Re: KDE needs to be faster"
    date: 2004-08-18
    body: "> So by induction on your argument, KDE 3.3 loads faster than KDE 1.0?\n\nThere is one exception: kde 2.0 loaded slower than kde 1.2. KDE 3.3 DOES in fact load faster than kde 2.0. \n\n> Yes that would be good but even on a >2Ghz system, KDE can't manage that.\n\nKDE startup isn't bound that much by processor speed, but hard drive speed. \n\n>  Unfair comparison but try this: install a fresh Windows 98. It boots _and_ logs in in 15s on a 300 Mhz!\n\nBullshit.. If you said that win2k loaded that fast, I would beleive you, but not win98. Later versions of Windows boot up much faster than earlier versions of windows. "
    author: "anon"
  - subject: "Re: KDE needs to be faster"
    date: 2004-08-19
    body: "> There is one exception: kde 2.0 loaded slower than kde 1.2. \n> KDE 3.3 DOES in fact load faster than kde 2.0. \ni can't comment much about the kde 1 series (used it for a day).\n\nbut I do remember seeing kde 2.0 on mandrake 7.2 loading in 15s.\nEverything went downhill after upgrading to kde 2.1 (that was\nwhen they switched the logo to the KDE with the clouds :) and\nnow I'm sitting here with approx. 50s load time.\n\n> > Unfair comparison but try this: install a fresh Windows 98.\n> > It boots _and_ logs in in 15s on a 300 Mhz!\ntry it and see.\nnote however, i do mean _fresh_ install (after the first boot\nas well to handle all the useless firsttime things).\n\n> Bullshit.. \nwhat a friendly KDE community. even if you consider me to be\na troll, that's not particularly polite.\n\n> If you said that win2k loaded that fast, I would beleive you, \nnein, Win98 is faster than win2k by a mile.\n"
    author: "w"
  - subject: "Re: KDE needs to be faster"
    date: 2004-08-18
    body: "\"So by induction on your argument, KDE 3.3 loads faster than KDE 1.0?\"\n\nNo. There was a significant regression in 1.x ==> 2.0. But since then it has been getting faster. Usually the changes were pretty minor but it seems that 3.1.x ==> 3.2 was a big jump. And again, 3.2.3 ==> 3.3 is pretty big jump (but not as big as 3.2 was)\n\n\"> How fast should it be then? After a fresh reboot, it takes something like 10\n> seconds for KDE to boot on my machine\nYes that would be good but even on a >2Ghz system, KDE can't manage that.\"\n\nI have a 2.2GHz Athlon 64, and KDE does manage it. And subsequent logins are 4-5 seconds. So what are you complaining? 4-5 seconds is such a short time that you could't really do anything extra. even if it were cut down to 2 seconds!\n\n\"Unfair comparison but try this: install a fresh Windows 98. It boots _and_ logs in in 15s on a 300 Mhz!\"\n\nOr better yet: I could run DOS on that machine! I bet it would boot in 5 seoonds!\n\nSeriously: I thought we were talking about serious operating-systems with top-of-the line UI's here?"
    author: "janne"
  - subject: "Re: KDE needs to be faster - with Your help"
    date: 2004-08-18
    body: "Jep - I fully agree with you!\nIts very fast and boots in 5 secs on my 2 Ghz system.\nBut during work, if I kill konqueror, its quiet annoying if it takes 2-5 secs after Alt+F2+Url to open ...\n\nand then waiting 2-5 secs to let konqueror build up the page :D\n\nBut in my eyes its a question of fine-tune and benchmarking!\n\nWe should integrate it into the next kde-release and build up a quiality-feedback for resource-management! (Get loading time in combination with system-specs!)\n\nThe plan:\n\nA simple \"Yes\" at first kde-startup and \nkde will check the startup/sysusage time (incl. systeminfo (mem,arch,ghz,hdspeed))\n\nand send it to a central-database at kde.org or elsewhere.\n\nSo will will have all information where improvements a needed!\n\n\n\nimpossible ??? - Let\u00b4s render the impossible into reality !!!"
    author: "Dirk Dietrich"
  - subject: "Re: KDE needs to be faster - with Your help"
    date: 2004-08-19
    body: "\"But during work, if I kill konqueror, its quiet annoying if it takes 2-5 secs after Alt+F2+Url to open ...\n\nand then waiting 2-5 secs to let konqueror build up the page :D\"\n\nFirst launch of Konqueror takes about 1 second on my machine. After that, it's instantenious. Actual browsing is really fast as well.\n\nI did think about KDE's boot-up time yesterday. Non KDE-users usually complain that it takes too long to start up. But those people compare it to something like Fluxbox ;). But we could try to make it faster.\n\nWhat Windows does is that loads the desktop really fast and then it loads the background-services (so the desktop is not usable yet, it just appears to start up really fast). Maybe we could do something similar? Now, I have no idea how KDE's start-up process works in practice, so I don't know if this is doable:\n\nWhen you start up KDE, it initializes the system-services, loads the desktop and the kicker and does other miscellianeous tasks. Could some of those be done in advance? I mean that when the user gets to KDM, the system would already load kicker, the desktop and some other things in the background. It would take few seconds for the user to type in his username/password, and that time could be used to start up KDE. When the user logs in, the kicker and desktop would already be loaded, and the system simply restores the session, which would mean that it configures the desktop and kicker according to the users settings. Besides Kicker and Desktop, there might also be other things that could be preloaded as well.\n\nthe theory is that when user logs in to KDE, it does not load all the stuff over and over again, it would already be loaded. Only thing that is done during start-up is to read the users settings and configure the desktop accordingly. this would take the best of the Windows-approach, but it would also make sure that when the user gets to the desktop, it would really be usable (unlike in Windows)\n\nNow, problem is that there are people who use KDM, but they use other desktops as well (Gnome, XFCE, Fluxbox etc.). In that case, preloading KDE would be a waste of resources for them. That's why we would need a simple configuration-option in the Control Center (\"Preload parts of KDE?\" or something).\n\nThoughts?"
    author: "janne"
  - subject: "Re: KDE needs to be faster - with Your help"
    date: 2004-08-19
    body: "Unfortunately preloading kicker etc. would not work - they would be running as the wrong user.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: KDE needs to be faster - with Your help"
    date: 2004-08-19
    body: "Awww, crap :(. Well, it was worth a shot anyway :)"
    author: "janne"
  - subject: "Disk cache"
    date: 2004-08-19
    body: "On the other hand, caching kicker and related programs by just doing fopen(kicker) ; read() ; close() ; could save a lot of time.  As KDM waits for users to log in, KDM could be putting KDE programs into disk cache.\n\nAs disks are the main bottleneck, this would be win-win: fast and unobtrusive for fast disks, and a worth optimization for slow disks."
    author: "Asheesh Laroia"
  - subject: "Re: Disk cache"
    date: 2004-08-19
    body: "What makes you think disks are the bottle neck? This is rarely the case.\n"
    author: "Richard Moore"
  - subject: "Re: Disk cache"
    date: 2004-08-19
    body: "Hmm, unless there is a lot of very complex math to be done, hard-drives *are* the slowest part of the computed. It sure isn't the CPU, RAM or video card in the vast majority of cases."
    author: "Mikhail Capone"
  - subject: "Re: KDE needs to be faster"
    date: 2004-08-19
    body: "> > Unfair comparison but try this: install a fresh Windows 98.\n> > It boots _and_ logs in in 15s on a 300 Mhz!\n>\n> Or better yet: I could run DOS on that machine! I bet it would boot in 5\n> seoonds!\n\nOnly if you have a bloated autoexec.bat :-)\n\n> Seriously: I thought we were talking about serious operating-systems with\n> top-of-the line UI's here?\n\nSad thing is: 6 years after Windows 98 was released, it still has more functionality than Linux 2.6 + KDE 3.3.  I'm talking multimedia,\noffice suite (yes, MS Office is much faster and easier to use than OO not to\nmention the excellent MS doc compatiblity since it is MS after all), games,\napplication compatibility..."
    author: "w"
  - subject: "Re: KDE needs to be faster"
    date: 2004-08-19
    body: "\"Sad thing is: 6 years after Windows 98 was released, it still has more functionality than Linux 2.6 + KDE 3.3. I'm talking multimedia,\noffice suite (yes, MS Office is much faster and easier to use than OO not to\nmention the excellent MS doc compatiblity since it is MS after all), games,\napplication compatibility...\"\n\nYou are not talking about the functionality of the OS, you are talking about the functionality of the apps that run on top of that OS. Just about the only thing where Win98 has the upper hand when compared to Linux, is games. And less and less games are compatible with Win98. Win98 does not have the stability, performance, security and features that Linux 2.6 + KDE3.3 has. Not even close.\n\nBesides desktop: can I use Win98 as a server? Would it work as well as Linux would? No? Then I guess it does not have \"more functionality\", eh?\n\n\"I'm talking multimedia\"\n\nI have zero problems creating oggs and/or mp3's on my Linux-machine. I have no problems playing back music, watching DVD's, burning CD's/DVD's etc. etc. I fail to see your point here. Hell, burning CD's is EASIER in Linux, since the apps are free. In Windows, just about all of them are payware. So in Linux I have a kick-ass burning-software (K3B), whereas in Windows I have some crappy shareware-app.\n\n\"office suite (yes, MS Office is much faster and easier to use than OO not to\nmention the excellent MS doc compatiblity since it is MS after all)\"\n\nI don't have any problems with either Koffice or OpenOffice. How exactly is MS Office \"easier to use\"? I bet it's just because it's the one you have used before, so changing to something different will feel \"difficult\". But that doesn't mean that the app is more difficult to use in reality.\n\n\"application compatibility\"\n\nMy Linux/KDE-box is compatible with all the apps I use :). Well, games are an exception, but I don't game that much anymore."
    author: "janne"
  - subject: "Re: KDE needs to be faster"
    date: 2004-08-19
    body: "During all of this thread I was in a permanent state of bogglement -- because, yes, KDE 3.3.0 was a lot slower than I had any right to expect seeing that I've got a 3Ghz machine. It certainly was slower than Windows XP on the same machine, and I was rather surprised at some of the claims of 'instant' konqueror and konsole bandied about. Dash it, if I so much as loaded slashdot, the fan would jump into the breach, cooling down the poor over-worked processor. Even if I disabled java, javascript and plugins.\n\nThen I decided to finally do something about the missing image previews -- I knew the code still worked, because it did so for a test user -- and I removed my .kde. On startup, I was amazed. Konqueror _was_ instant. No time to excavate the nose waiting for konsole anymore. Even Krita, was quick enough to make testing a pleasure again. Only... This is a traumatic experience. One does _not_ want to do without ones wallet. And not without alt-f1 set to vertical maximize. And all the other settings I need to work with pleasure.\n\nSo, something in that .kde (that had survived all transitions since KDE 1.1.x, with only an occasional manual pruning) must have caused a lot of lag. I don't know what it is -- though I have saved a tarbal of my .kde for future research.\n\nOh, and network settings etc. were tops, because, if they aren't, you're not looking at a five or ten second delay, but thirty seconds or more. "
    author: "Boudewijn Rempt"
  - subject: "Re: KDE needs to be faster"
    date: 2004-08-17
    body: "I have noticed longer startup times - but not in KDE.  The individual linux distributions do many things differently and are constantly changing them.  But - as a way to test - go to konsole and type \"startx -- :2\".  There will still be a number of distribution specific things going on in the startx - but my KDE 3.2 comes up in about 15 seconds (1.4Ghz - normal ram - normal disk) (Haven't bothered to check but I'm sure the speed would be the same from a console without having a KDE already loaded).\n\nDon't blame your distributions shortcomings on KDE.  "
    author: "PaulSeamons"
  - subject: "Re: KDE needs to be faster"
    date: 2004-08-17
    body: "KDE *is* getting faster, just not so much in the initial loading department.\n\nThe speed improvement from 3.1 -> 3.2 was pretty substantial on my K6-2 450mhz/192 megs RAM and KDE is faster than Win XP here.\n\nPersonally, I really don't care about loading since my computer is always on and I have to reboot only when a new kernel comes out. You should probably consider using the sleep features of your computer instead of rebooting all the time."
    author: "Mikhail Capone"
  - subject: "Re: KDE needs to be faster"
    date: 2004-08-18
    body: "Just a simple comment: I read a lot of people talking about performance and start-up time, trying to compare performance by just mentioning CPU and Ram issue. For your information:\nI just update my HD from a IDE Drive (6 year old, 6M) to a very fast SATA disk with 8M of cache. The performance improvement is very important: more than 2 time faster.\nI you get a look at the CPU usage, often you may notice that CPU is not at top usage while booting. The flow factor here is the HD, not the rest of the box, which is most probably fast enough since already a few years.\n\ncheers."
    author: "stephane petithomme"
  - subject: "Applications"
    date: 2004-08-17
    body: "I respect the enormous work of developers that have led to the present capability of KDE. And surely, one may find a lot of nuances or lacks to be correcterd in the  future versions of KDE. \nBUT!\nI, as a user, mostly do not use the KDE itself (there are few exceptions like KMail). What I need is a great number of applications (fulfilling my particular needs) integrated with KDE environment. \nThe present state of KOffice is funny (or tragical, it's up to you) but is not suitable for daily use. (To be precise: I like it, I am trying to use it, and after a failure I have to switch to OOo - according to 25-30 opportunity this year).\n\nFrom the applications pont of view: the KDE present state is more than satisfactory, although not perfect. But for longtime development procedures, needs to be stable for years."
    author: "devians"
  - subject: "Re: Applications"
    date: 2004-08-17
    body: "3.x apps are compatible. you can write apps on 3.0 (2 years old) and they'll work on 3.3.0 (at least, they should, if not - file a bug). and if it takes another year to bring out kde 4.0, well, apps have been able to work for more than 3 years without modification. thats fine with me...\n\nand about Koffice, well, I have no problems with it. Use it all the time, for my work for the university, and its ok for me. Maybe it doesnt have all the fancy features M$ office features, or Open-office has (I dont use these anyway) but it loads faster, is network-transparent (I love to just edit the files on my university's ftp harddisk, without any manually copy-ing around) and is mostly quite stable.\nindeed it can be improved. but its good enough for normal work, imho. name a few features you miss in the latest Koffice that are present in OOo? then start coding ;-)"
    author: "superstoned"
  - subject: "Konqueror File Manager"
    date: 2004-08-17
    body: "It's good to know that \"a lot of work\" has been done on Konqueror File Manager, but it seems keyboard navigation and selection was once again forgotten.\n\nIt is still a real pain trying to select a bunch of files on the right using the keyboard. The thing seems to randomly switch between different selection modes, and overall it is very inconsistent and a lot of times simply broken.\n\nSo, is there any chance we have a functional file manager, maybe by 3.3.3?"
    author: "Kirill"
  - subject: "Re: Konqueror File Manager"
    date: 2004-08-17
    body: "You mean 3.4 or 4.0\n\nThe next 3.3.x will be only bug fixes and security issues."
    author: "JC"
  - subject: "Re: Konqueror File Manager"
    date: 2004-08-17
    body: "It seems to me that, if this is a genuine bug, it could/should be fixed in a pointrelease. \n\nBTW, of course it would need to be properly reported. A posting on the the dot does not count as such, a report on bugs.kde.org with a clear description on how to reproduce the problem does."
    author: "Andre Somers"
  - subject: "Konqueror -- \"GeneralPurpose\" FileManager"
    date: 2004-08-17
    body: "SpecializedFileManager:\nhttp://kde-apps.org/content/show.php?content=10214\n\nOr check here:\nhttp://kde-apps.org/index.php?xcontentmode=284\n\nI think Konq works well as a general purpose filemanager/browser, but if you better like an app that only manages files, who keeps you from trying?\n\nAfcourse Konq should have proper shortcuts for filemanaging -- you could do a investigation of what is exactly lagging and report it to the devs."
    author: "cies"
  - subject: "Konqueror file manager rant"
    date: 2004-08-17
    body: "I'm using KDE 3.3 recently released in Debian Sid, and I have have 2 annoyances not previously found on 3.2.\n\n- When using grid aling on desktop icons there's too much vertical space between icons. This means you have enough space between rows to put another one there :(\n\nVote for this here:\n\n http://bugs.kde.org/show_bug.cgi?id=79932\n\n- And now when using multicolumn mode on file manager you get long filenames truncated with a elipsis (...).  I have found an option for changing number of lines in icons' text, but this doesn't seem to do the trick.\n\nSee the attached shot for an example.\n\nI don't point to a bug report here to make sure I'm not missing some unknow config option ;) "
    author: "anonymous coward"
  - subject: "Re: Konqueror file manager rant"
    date: 2004-08-18
    body: "ugh.. why is it so impossible to have sane defaults in konqueror file manager?\n\nTruncated file names and ellipses are good for detailed list view (ensires a column other than name is always visible so you can paste, create new directories etc with RMB) but it is not what people expect at all with multi column view.. \n\nI guess I am happy since I use detailed list view, but why must fixing my preferred view fark up someone else's?"
    author: "MamiyaOtaru"
  - subject: "Re: Konqueror file manager rant"
    date: 2004-08-18
    body: "I have said it before, and I'll say it again: multicolumn-view in Konqueror \"just doesn't work\". Take a look at these screenshots I have taken:\n\nhttp://217.30.176.84/images/\n\nOne is Windows Explorer, other two are Konqueror 3.2.3 using multicolumn-view with word wrap on and off.\n\nSeriously: which of those looks best to you? Which of those three dispays the filenames the best way? Which of those displays the filenames in such way that they are easy to read (or readable at all!)?"
    author: "janne"
  - subject: "Re: Konqueror file manager rant"
    date: 2004-08-18
    body: "Sincerely Windows seems better here...\n\nBut the nasty problem is that the word wrap \"on\" desn't work at all, only working mode is the truncated one. So as you may see in the screenshot it's really unusable right now."
    author: "same anonymous coward"
  - subject: "When did \"interview\" become \"Q & A\"?"
    date: 2004-08-18
    body: "Just curious..."
    author: "Richard"
  - subject: "OT: qt-3.3.3"
    date: 2004-08-18
    body: "Just updated to qt-3.3.3 on SUSE 9.1/KDE 3.3. Few bugs: on every login i need to remove and add pager and system tray back to the panel as they get 'inactive' (dcop comms error?). Any ideas how to fix this?"
    author: "jmk"
  - subject: "Re: OT: qt-3.3.3"
    date: 2004-08-18
    body: "Hmh, qt seems to break this entirely. Minicli shows up on random desktop, system tray occasionally explodes (gets *huge*), kontact/kmail message windows sometimes appear on taskbar and sometimes not, etc. Has to be some dcop/qt shonk."
    author: "jmk"
  - subject: "Ugrading vs. Bug Fixes"
    date: 2004-08-18
    body: "Yes, it is correct that to get bug fixes with OSS that you need to upgrade.  The simple fact is that after a branch is finished that there are very few (or no) bug fixes added to it.  I see no problem with this.\n\nThe problems are:\n\n1.  New branches and releases since 3.1.0 have had serious regressions.  Obviously this means that upgrading to get bug fixes is no longer a totally satisfactory solution -- especially for commercial users.\n\n2.  The bugs are not always fixed on the current release branch.  I see this as a serious issue.  All bugs in the current release should be dealt with on the current release branch.  The fact that it works on the current CVS HEAD is no help at all to users -- the bugs should be fixed FIRST on the current release branch, and then \"front ported\" to the current development branch.\n\nIf we can't avoid the regressions, we are going to have to start releasing two branches like GNOME, and other projects.  This would have (or should) have meant that the KWin problems would have been avoided since the new features that broke it would have first been applied to the unstable release.\n\n--\nJRT"
    author: "James Richard Tyrer"
---
<a href="http://www.newsforge.com/">NewsForge</a> has <a href="http://www.newsforge.com/article.pl?sid=04/08/14/2316222">caught up with KDE developer George Staikos</a> to talk with him about the upcoming KDE 3.3 release, KDE's future, Konqueror, KWallet, freedesktop.org, KOffice and more.

<!--break-->
