---
title: "KDE World Summit to Feature PGP Keysigning Session"
date:    2004-08-05
authors:
  - "dmolkentin"
slug:    kde-world-summit-feature-pgp-keysigning-session
comments:
  - subject: "encryption in kmail?"
    date: 2004-08-05
    body: "doesnt this already work perfectly in kmail? I have no issues, I communicate encrypted with several ppl..."
    author: "superstoned"
  - subject: "Re: encryption in kmail?"
    date: 2004-08-05
    body: "Sure it does work already, but many people complained it was too difficult to get non-inlined pgp encryption working, setting up all the cryptplug stuff. This has now all gone for good, since we now use the new C++ classes of gpgme directly, which is a big improvement."
    author: "Daniel Molkentin"
  - subject: "Re: encryption in kmail?"
    date: 2004-08-05
    body: "aaah, so it got easier? hmmm, imho it wasnt that difficult (set up kgpg, then add the key in kmail...) and I cant understand how it can get easier, but well, thats just my limited imagination I guess, I'll be surprised for sure ;-)\n\ngreat job anyway, with 3.3 - I tried onebasego's beta, wonderfull work!!!"
    author: "superstoned"
  - subject: "Re: encryption in kmail?"
    date: 2004-08-05
    body: "I set up kmail for using gpg very easily too, but that's only the _inline_ signing.\n\nIf you add attachments to your email, the attachments aren't signed or encrypted. With PGP/MIME, which is a standard, you can fully sign and encrypt the whole contents."
    author: "suy"
  - subject: "Re: encryption in kmail?"
    date: 2004-08-05
    body: "aaah, I understand. thats very nice, thank u!"
    author: "superstoned"
  - subject: "Re: encryption in kmail?"
    date: 2004-08-05
    body: "Will there still be support for inline signing?  More mail clients support inline signing.  In fact one of the big complaints I have heard about evolution is that there is no support for inline signing.\n\nBobby"
    author: "brockers"
  - subject: "Re: encryption in kmail?"
    date: 2004-08-06
    body: "Yes, in fact they set a bounty on adding inline pgp signing :) But don't be afraid, KMail will still support inline signing, though it is marked as deprecated - for a reason: Inline signing is very, very limited, e.g. it cannot sign attachments."
    author: "Daniel Molkentin"
  - subject: "Re: encryption in kmail?"
    date: 2004-12-03
    body: "I have some problems with the Openpgp/mime feature. People from my work that use outlook complaint because they always receive my replies as an attachment. It does not happen with inline pgp signing. Any idea of how I can still use openpgp but my reply text remain inline?\n\nThanks in advance,"
    author: "Daniel Olmedilla"
  - subject: "Re: encryption in kmail?"
    date: 2004-12-03
    body: "well, then, outlook is messing things seriously up, because afaik the pgp signing is IN THE ATTACHMENT and the mail body text just contains the message. so it looks like outlook takes the attachment, displays it as text, and shows the body contents as attachment... if so, well, its hard to beat such stupid behaviour..."
    author: "superstoned"
  - subject: "Great!"
    date: 2004-08-06
    body: "This is great! I'm glad that KMail is getting even better at crypto and that there as key-signing partys like that. It's only good for security awareness and to be able to trust and identify people better in the KDE world.\n\nI just wish everybody knew how to use strong crypto."
    author: "Mikhail Capone"
---
In a longstanding tradition of fostering the KDE web of trust, this year's <a href="http://conference2004.kde.org/">KDE World Summit</a> in Ludwigsburg will again offer another opportunity to get your PGP key signed by numerous KDE developers. Conducted by KDE veteran <a href="http://www.kde.org/areas/people/kalle.html">Matthias "Kalle" Dalheimer</a>, a <a href="http://en.wikipedia.org/wiki/Keysigning_party">keysigning session</a> will take place on Monday 23th.



<!--break-->
<p>Kalle writes: <i>"A PGP Key Signing Party is planned currently for Monday starting 5pm. To participate, please send your key ID to <a href="mailto:keysigning@kdab.net">keysigning@kdab.net</a> by August 15. Further instructions will follow."</i> Also, make sure to upload your key to one of the <a href="http://www.pgp.net/">pgpnet</a> servers.</p>

<p>With <a href="http://kmail.kde.org/">KMail</a>'s <a href="http://www.gnupg.org/aegypten2/">improved encryption support</a> in KDE PIM 3.3, you can now sign and encrypt your mail traffic easily, thus securing your daily mail communication. So get your key signed!</p>



