---
title: "KDE 3.3 Usability Study and Review"
date:    2004-09-21
authors:
  - "jturner"
slug:    kde-33-usability-study-and-review
comments:
  - subject: "How True"
    date: 2004-09-21
    body: "Really nice read. KDE is *this close* to be just absolutely amazing. All it needs is to hide advanced options and reorganize options in menues and toolbars with one idea in mind: NOT overwhelm the user. The Konqueror Settings Dialog is a good example. It is just way too cluttered. \n\nI love the way they handle these issues on Mac OS (for what I peeped other people using it). I also like the simplicity of XFCE-4. It replaced GNOME as a secondary desktop on my machines. It really is a pleasure to use (although it lacks most of the KDE features, but it's worth a look).\n\nAll in all, KDE is an outstanding DE. But if I were to vote for one direction of improvement, it would be usability, in the sense discussed in the article. Simplicity in the design, and sensible default values. \n\nA little time ago I read here in the dot that a few professional usability experts joined the project ? This will be terrific. Are hey working for Novell ?"
    author: "MandrakeUser"
  - subject: "Re: How True"
    date: 2004-09-21
    body: "> A little time ago I read here in the dot that a few professional usability experts joined the project ? This will be terrific. Are hey working for Novell ?\n\nYup, the lead usability guy at SUSE, Dr. Siegfried Olschner, spends half of his time in writing the new human interface guidelines for KDE. I think Novell has a couple other people in their suse usability group working mostly on KDE-related suff as well. Other people would know more, of course.\n\nThe OpenUsability folks work mostly at relevantive, a usability firm in Germany. "
    author: "anon"
  - subject: "Re: How True"
    date: 2004-09-21
    body: "Is this SUSE guy backed by the KDE core developers?\nI think should be annoying if Novell force his work into KDE.\nAfter all, one reason most of us use KDE and do not use Gnome is that all this nice and smart guys are in charge and corporations can provide resources but do not steer the project vision and direction.\nVery sad if we have our own \"KoneME\" in a year or two.\n\nBesides this concerns, I want to rise find a different (but connected, and larguely discussed already) question:\nIs KDE willing to ease things for new users from the Windows world in spite of fellow user (KDE 1.0 myself) who likes KDE as it is, who likes its defaults and the clever way it is constructed? Should their developers simply drop their vision about how things should work because most people is not used to this and do not care if there are better things to do this? This happens already. I love the original vision of Konqui, but if I have to customize it from scratch, I already have to set MIMEs to open into Konq all the way despite the fact this is a strong paradigm in its design!\n\n\nI hope the answer could be a very KDE-like one: A new step in the customizing wizard, asking for sensible defaults for (at least) die-hard KDE users, for Windows users, for Mac users, and for Gnome users.\n\n"
    author: "Shulai"
  - subject: "Re: How True"
    date: 2004-09-21
    body: "> Is this SUSE guy backed by the KDE core developers?\n\nyes. he gave a good presentation at aKademy, discussed a lot of usability related things with various people and was invited to join in on our guidelines rewrite. he requested and was given the go-ahead to do so by his SUSE/Novell managers, which is really cool on their part. he's now working alongside the rest of the guideline writers to help make it a success."
    author: "Aaron J. Seigo"
  - subject: "Re: How True"
    date: 2004-09-21
    body: "> Is KDE willing to ease things for new users from the Windows\n> world in spite of fellow user (KDE 1.0 myself) who likes KDE\n> as it is, who likes its defaults and the clever way it is\n> constructed? Should their developers simply drop their\n> vision about how things should work because most people\n> is not used to this and do not care if there are better\n> things to do this?\n\nspeaking only from my own personal perspective, i don't think these two goals are at logger-heads. we can make KDE a comfortable place for newcomers and retain our vision. we don't have to become a clone to become usable and adopted. we do have to become more usable to be more widely adopted, however, just as we need more capabilities/features to be more widely adopted.\n\nsome of the reasons Konqi doesn't reach its full potential is not so much because it needs to be tailored to new user expectations, but rather because of usability problems that create collateral damage effects. for instance, right now when you view a file in Konqi, how do you move to edit it? it's not apparent to the user, so we need to open it in a new window by default more often. this is completely solvable, however, and it will be one day =)\n\nmany other similar issues exist. as we improve the ergonomics and interaction mechanisms in KDE, we will actually be able to promote our vision more clearly and effectively."
    author: "Aaron J. Seigo"
  - subject: "Re: How True"
    date: 2004-09-21
    body: "erm, i think it's \"half a day a week\", not half his time ;-) still, it's awesome and i personally really appreciate his and SUSE/Novell's commitment in this regard. Siggie is a great guy, and he's brilliant to boot. like a lot of the people in KDE, it seems =)"
    author: "Aaron J. Seigo"
  - subject: "Re: How True"
    date: 2004-09-21
    body: "People really should not comment on OS X unless they've actually used it for some time. It is really not the shining example of usability everyone holds it for. Some niggles I have found over a year and a half of OS X usage:\n\n* Inconsistent design and use philosophies between apps derived from the Next heritage (Mail) and apps derived from OS 9 (iTunes).\n* Inconsistent look & feel (brushed metal, Cocoa, Carbon -- won't complain about the legacy OS 9 apps or the X11 apps)\n* It's hard to discover the \"advanced\" options. No Mac user I know knows you can switch between documents in the same application with apple-tilde, and everyone is delighted to hear about it. I only knew because I've read the OS X HIG from cover to cover.\n* The new finder is completely unpredictable in when it will show itself in which way.\n* The font rendering is really quite bad, compared to Freetype.\n* OS X has no package management, meaning it can be hard to uninstall software that doesn't keep to the everything-in-one folder concept. There are a myriad ways of installing software, too.\n\nOf course, there are good points, too, like the quality of iTunes or iPhoto. But in the end, in my experience, OS X isn't much better than KDE as a Unix shell."
    author: "Boudewijn Rempt"
  - subject: "Re: How True"
    date: 2004-09-21
    body: "Exactly. I tried MacOSX too and found it terrible that you can choose between 2 minimize-animations but cannot switch off minimize animations. So it slows you down all the time.\n\nBut that's not the point. The point is that Apple isn't good as usability or ergonomy (they aren't. Just look at the \"puck-mouse\") they are good at marketing. (way better than Microsoft, BTW) If it's from Apple, it \"must be\" easy, intuitive and user-friendly.\n\n"
    author: "Roland"
  - subject: "Re: How True"
    date: 2004-09-21
    body: "Apple has had a good usability back in the days of Mac OS 7 (including hard user interfaces: keyboard and mouse). This is what Gnome tries to copy / build on today."
    author: "testerus"
  - subject: "Re: How True"
    date: 2004-09-21
    body: "while this is quite true, the desktop computing world has moved on and is almost nothing like it was back then. our interfaces must evolve in response to the growing amount of data, number of sources and diversity of interaction requirements involved in a modern computing/communication environment."
    author: "Aaron J. Seigo"
  - subject: "Re: How True"
    date: 2004-09-21
    body: "True..\nAlso while back computers were limited in processing power, so you couldn't multitask as easily as you can now, so the UI must adapt to allow efficient multitasking for the user..\n\nOn this topic, my pet peeve is that \"Splashscreen should DIE\", they steal the focus (prevent multitasking), some can removed by clicking on it, some can't which is even more annoying.\nIf you activated by mistake an app or you change your mind, there is no way to close the app because splash screen can't be closed nor minimised..\n\nIMHO \"splash Window\" would be *MUCH* better: I call splash window the same thing as a splash screen but embedded inside a window frame with the normal minimize/close button.\n\nNow this is more an OOo problem than a KDE problem fortunately..\n"
    author: "RenoX"
  - subject: "Re: How True"
    date: 2004-09-22
    body: "Except, of course, in the Kontact App..."
    author: "Zak"
  - subject: "Re: How True"
    date: 2004-09-22
    body: "Kontact loads quite fast for me in the 3.3 series !"
    author: "MandrakeUser"
  - subject: "Re: How True"
    date: 2004-09-22
    body: "kontact --nosplash"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: How True"
    date: 2004-09-22
    body: "> On this topic, my pet peeve is that \"Splashscreen should DIE\",\n>\n> Now this is more an OOo problem than a KDE problem fortunately..\n\nWow, there's a blast from the past. I'd forgotten just how annoying the open office splash was. Anyway, you don't need to suffer unless you really want to:\n\nhttp://ui.openoffice.org/howto/\n\nThat said, this definitely falls within the realm of sane defaults, and I agree that \"Splashscreen should DIE\" is a sane default.\n\nHopefully the Kontact splash will be disable-able at some stage, but at least like all KDE splash screens you just have to click on it to make the damn thing go away."
    author: "Ralph Jenkin"
  - subject: "Re: How True"
    date: 2004-09-21
    body: "> People really should not comment on OS X unless they've actually \n> used it for some time. It is really not the shining example of \n> usability everyone holds it for.\n\nPoint well taken :-) Ant thanks for sharing your experience. But I still think that a quick impression to a new interface is important. I have shared work with Mac OS X users, where we sit in front of their laptop, and we collaborate on research. And the interface does look clean, context menues seem reasonable, etc. You are not presented with lists of dozens options. Just a few, well thought options. I am not saying we have to go OS X. Or that everything they do in terms of usability is just perfect. \n\nHowever, companies like Mac and the Redmond monopolists do invest a lot of money on usability, and they come up with software that doesn't scare a user with no technical inclination. And we can learn a few tricks from peeping at their UI's eveyonce in a while.\n\nWe are getting better in the FS world. Some Linux distros are making things look more end-user friendly. KDE is helping a lot. But there is still a gap to cross unless we want to mostly target techies. \n\nA point in case: Configuration layout in Konqueror (KDE 3.2.3). Imagine a non-techie. Someone who just wants to search the web and manage files. She/he opens \"Konqueror -> Settings\": 17 freaking option categories. Seventeen. Names like \"crypto\", \"stylesheets\", \"Proxy\". This is not end-user friendly. Don't get me wrong. I love to fool around with these options. But an end user freaks out. Just because they don't care. The same way I don't care about lawyer-talk, fashion and so many other things. \n\nBut what if you opened \"Konqueror -> Settings\" and you had 2 options: \n\"configure file management\" or \"Configure web browsing\". Maybe a third category for common options. And then within \"Configure web browsing\", just a few understandable categories. \"Look and Feel\", \"Plugins\", \"Pop-up blocker\", \"Advanced Options\".  I think that this is the sort of approach the article is trying to encourage. And I think it is the way to go. \n\nIn the meantime, I choose to use Mandrake linux with KDE as a default DE, and my whole family does. Things are good, they just could be better :-)\n\nCheers !"
    author: "MandrakeUser"
  - subject: "Re: How True"
    date: 2004-09-21
    body: "\"A point in case: Configuration layout in Konqueror (KDE 3.2.3). Imagine a non-techie. Someone who just wants to search the web and manage files. She/he opens \"Konqueror -> Settings\": 17 freaking option categories. Seventeen. Names like \"crypto\", \"stylesheets\", \"Proxy\". This is not end-user friendly.\"\n\nI know quite a couple of people like that. And guess what, it's completely irrelevant how many options are there because they never see them because they never change the defaults in the first place.\n\n\"But what if you opened \"Konqueror -> Settings\" and you had 2 options: \n \"configure file management\" or \"Configure web browsing\". Maybe a third category for common options. And then within \"Configure web browsing\", just a few understandable categories. \"Look and Feel\", \"Plugins\", \"Pop-up blocker\", \"Advanced Options\". I think that this is the sort of approach the article is trying to encourage. And I think it is the way to go.\"\n\nSo instead of a couple of options you want to have more options (= when you split up everything in \"normal\" and \"advanced\" you have twice as much and put the whole thing in a tree instead of a flat structure. It's got a lot more complicated.\n"
    author: "Roland"
  - subject: "Re: How True"
    date: 2004-09-21
    body: "Human beings are not computers. It takes extra processing for them to ignore irrelevent information. Say someone opens up a control panel with 17 options, vs one with 5. If something only has 5 options, the user can take it in at a glance, and immediately find the one she wants. If it has 17, she has to scan through each one (and make a decision about whether it's the one she wants or not), before getting to the one she wants. Humans are built to recognize and process small sets quickly. That's why you can easily group things in threes or fours, but not in tens. If you ignore this \"feature\" of the brain, the speed at which your UI can work suffers."
    author: "Rayiner Hashem"
  - subject: "Re: How True"
    date: 2004-09-21
    body: "So which 5?\n\nI recently installed firefox, replacing mozilla on my linux box. My wife prefers mozilla to konqueror because it prints legibly.\n\nRight off, she asked where is the print icon on the toolbar?\n\nThis move to minimizing the user interface reminds me of 'nouveau cuisine', where you sit at an expensive establishment, the plate comes with a couple of slivers of carrots and a tiny piece of fish, all presented elegantly. Everyone talks about how wonderful the meal was, how exquisite the service. And on the way home you pick up a pizza because you are still hungry.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: How True"
    date: 2004-09-21
    body: "You should have just told her to use File -> Print. That's what the menu is there for --- it's a canonical listing of all of a program's features. A toolbar is just an optimization. Specifically, it is one whose usefulness is couched in two principles:\n\n1) Human beings are good at processing small sets, with the maximum number for comfortable recall being 5-7 (from psychological studies).\n\n2) Human beings are good at recognizing distinctive visual objects. \n\nIf your toolbar contains elements that cause it to violate (1) or (2), then it is no longer useful as an optimization. \n\nGiven a large sample population of users, you can develop a rank-ordering of how often each icon is used. The decision of what to put into the toolbar, then, becomes a matter of filling the toolbar in rank-order, subject to the constraints of number and distinctiveness. \n\nThings should be in the toolbar because they are used often, not because people expect them to be there. People will learn, with use, to know where to expect things. They cannot learn to deal with more items at a glance than their brain allows.\n\nPS> It is interesting to consider an example of an application where (1) and (2) cannot be fulfilled easily. In that case, a toolbar really isn't very usefull at all. Consider the user-interface of Softimage XSI (which many consider to be the most productive 3D modeler in existance). There aren't really any main toolbars like what you'll see in Maya. Instead, the \"toolbars\", are just sets of buttons with text-labels. They are more of an extension to the main menu than anything else. The reasoning is that there are too many options to present a scanable toolbar with visually distinct icons. Trying to make a toolbar out of those would just lead to the user linearly scanning a list of cryptic icons (*cough* Maya *cough*), which is slower than using a menubar of text labels.\n\n"
    author: "Rayiner Hashem"
  - subject: "Re: How True"
    date: 2004-09-22
    body: "No you got it wrong.\n\nMozilla had a print button on the toolbar. Firefox doesn't.\n\nSomebody optimized things alright. Someone decided that there shouldn't be any more than some imaginary number of icons, so they optimized away something that is useful. And used. And requires training to show another way.\n\nYou suggest using the menu. You suggest I show her how to do something she knows how to do with another way because it isn't appropriate somehow.\n\nWhat if there are 6 common uses of a piece of software, where theory suggests that 5 is optimum? Where does usability change from trying to help users into attempting to influence the way they use the tools?\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: How True"
    date: 2004-09-22
    body: "The toolbar in an application serves two purposes:\n\n1.  (most important) It removes the overhead of pulling down a menu for items that are used very frequently (more than once per session).  Printing definitely does not fall into this category for the average web user. (For exceptional cases, the toolbars are editable).\n\n2.  (less important) It guides the user by showing them the *few* most relevant and important actions they can perform at any time.  Printing is an action that might fall into this category, but most users will expect that they can print web pages, and when they want to print they will specifically search for the print option, so they do not need to be reminded that it exists by a toolbar button.  The number of icons on the toolbar should be kept VERY SMALL to avoid cluttering the interface and intimidating new users.  (see the user comments on Konqueror's interface from this study...)  So it still seems to me that the print button should not be on the toolbar.\n\nSo there are good usability arguments for not having the print icon on the toolbar.  However, since the print icon has been there in previous browsers, we also have to consider the impact that removing it will have on those users.  This is where usability testing is needed, to tell: 1. how many users will be affected, and 2. how severe the effect will be.  In the case of the print icon, though, I think I can predict the outcome:  users will easily find the standard print menu option in its standard place in Firefox's menus.  Thus, Firefox was justified in removing the print icon.\n\nOften people use bogus \"usability\" arguments in attempts to push their interface ideas on other people.  However, that doesn't mean that doing things in the name of usability is always bad.  In this case, it is likely that the users really are better served by removing the print button. (to be absolutely sure requires extensive user testing)."
    author: "Spy Hunter"
  - subject: "Re: How True"
    date: 2004-09-27
    body: "You know, for me, I think the key is, simplicity in the way you suggest, but also a very intuitive customization process.  You say only exceptional cases might need to edit the toolbar, but the fact is, everyone is an exceptional case in some feature or another.  Maybe not printing, or whatever, but maybe some other thing.  So intuitive editing of toolbars and interface in general should be there.  Like dragging the \"Print...\" option right off the menu and putting it in the toolbar.  (or dragging the whole menu and dropping it as a toolbar).  Not hiding the \"Edit the toolbar\" feature somewhere in the menus (there's nothing wrong with having it in there as well of course, EVERY feature that an app can do should be represented somewhere on the menu -- people use them as documentation for an app).  Grabbing things with the mouse and putting them where you want should be a universal, system-wide feature that works with EVERYTHING.  That way the user can just expect that when the drag something around it's going to do something rational with it.\n\n"
    author: "Erik"
  - subject: "Re: How True"
    date: 2004-09-22
    body: "It's not a hard limit. That's why I said 5-7. That means that 8 could be okay, 15 is probably bad. It's not some imaginary number, you can crack open any psychology textbook and find it. A printer icon in the toolbar is *not* useful. It takes up to a minute to complete a print-out. You can't possibly use the print feature often enough to warrent it being in the toolbar instead of the menu. A \"go to google\" icon would probably be much more efficient.\n\n<i>Where does usability change from trying to help users into attempting to influence the way they use the tools?</i>\n\nI find this statement hilarious. Usability should be about influencing the way they use the tools. The tools should be used as efficiently as possible, and the interface should be designed so that the easiest thing to do is use the interface efficiently. Look at it this way: if somebody types improperly, shouldn't you \"influence\" them to type using the proper form? Further, when your reluctance to influence the user causes you to make a toolbar with 15 entries, then not only do you not encourage them to use the application efficiently, you prevent everyone else from using the application efficiently. \n"
    author: "Rayiner Hashem"
  - subject: "Re: How True"
    date: 2004-09-21
    body: "PS> To follow up my other comment, let me clarify the thing about the rank-ordering. People will often say \"yeah, 90% of users use only 10%, but it's a different 10%.\" It is highly unlikely this is the case. If such a statement were true in other fields, industrial design would become nearly impossible. There are lot's of assumptions every designer and engineer makes about the users using his products, and the fact that most of us find our cars comfortable to sit in and use (for example), is a testament to the level of homogeneity of peoples' behaviors. Now, the analogy is a bit off, because computers themselves are multi-purpose tools while cars are special-purpose tools, but applications themselves are essentially special-purpose tools, so it's close.\n"
    author: "Rayiner Hashem"
  - subject: "Re: How True"
    date: 2004-09-22
    body: "I really doubt most of people find all cars comfortable to sit in and use right after changing a model or manufacturer, it's always a question of adaption and taste as well as both the object's aand the user's flexibility. With computers it's one level worse since it's even more abtract and the expectation can only be related to prior experience (using KDE as an absolute beginner is easier than after switching system)."
    author: "Datschge"
  - subject: "Re: How True"
    date: 2004-09-22
    body: "That's not really the point. The point is that industrial design (cars, etc) has to take into account a huge range of user preferences and behaviors. If all behaviors and preferences were equally likely (which is implied by the \"each user uses a different 10%\") then industrial design would be impossible. You couldn't physically design a car to take all those things into account. In reality, people's behaviors and preferences tend to fall into clumps, with surprising homogeneity within clumps. That allows industrial designers to to design for a few combinations of behaviors, and it works quite well in practice."
    author: "Rayiner Hashem"
  - subject: "Re: How True"
    date: 2004-09-22
    body: "Odd you mention cars.\n\nI drive an early 90's subaru, and a ford ranger pickup.\n\nOne is standard transmission, the other automatic. One has the wipers on the left of the column, the other on the right.\n\nI am always getting things mixed up.\n\nHomogeneity is due to our ability to learn, rather than any natural reflexes.\n\nIndustrial design has more to do with keeping the product simple and economical to produce. Or marketing. The goal of most human interface design is to remove unnecessary options so that processes are repeatable and predictable.\n\nI hope this isn't the goal of the usability effort.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: How True"
    date: 2004-10-01
    body: "I think you've missed the point about this whole usability issue.  If you look in most modern cars they have a steering wheel (rather than many other possible alternatives).  In addition, in most cars the ignition is in exactly the same place and by god you'll curse if you climb in a different car and you cannot find the windscreen wiper control.\n\nThe point is that most people expect to find things in certain places.  In addition there is the HSE aspect (you probably want to avoid constantly clicking on the mouse) and if you have used some poorly designed Windows you'll know what I mean...\n\nNow I'm with you when you say that configurability does not need to be sacrificed.  But when I've talked to usability consultants in the past I've never heard \"you should remove this\".  However, we did get plenty of advice like 90% of users found this confusing perhaps if you organize like this it will be less confusing."
    author: "David Collins"
  - subject: "Re: How True"
    date: 2004-09-22
    body: "You can then right-click when your mouse is in the toolbar, click on \"Customize\" and drag&drop your print icon where you want it to be.\n\nI prefer to have few icons in my toolbar (I use print once or twice each month, so in the file menu is the correct place for that option; but I used \"open a new tab\" very often so  put it there) and be able to have the URL adress box in the same bar in order to avoid loosing place.\n\nHaving less options by default and be bale to add them is better than having lots of icons by default and be able to remove them.\n\nThat's my thought (and only mine :)\n"
    author: "Romain"
  - subject: "Why usability studies are creating wrong results"
    date: 2004-09-21
    body: "The problem with usability studies is that they take beginners, put them for 30 minutes in front of the system and derive the conclusions out of that 30 minutes.\n\nThe problem with following that advice is that you get a system that is great for the first 30 minutes of computer usage, but which will suck for **YEARS** of computer usage that follow.\n\nBrowser-tabs are an excellent example. Did anybody use browser-tabs in their first 30 minutes of browsing the web? No, almost certainly not. In a usability study, browser-tabs would be categorized as completely useless and would land on the \"remove-because-too-complicated\"-list. Another example would be multiple desktops and yet another would be keyboard shortcuts. (Nobody uses keyboard shortcuts in the first 30 minutes of use - THEY MUST BE USELESS!)\n\nWhile I agree that defaults should be targetted at beginners **when possible** (= without removing or destroying useful features), it's just stupid to remove options and settings.\n\nFor what it's worth, I think the KDE-way of usability (watching bugs.kde.org for wishlist items and implementing the good ones) is way superior to any \"usability study\".\n"
    author: "Roland"
  - subject: "Re: Why usability studies are creating wrong results"
    date: 2004-09-21
    body: "About tabbed interfaces, you may like to read what happened with the usability studies when Microsoft started experimenting with them:\n\nhttp://www.joelonsoftware.com/printerFriendly/uibook/fog0000000249.html"
    author: "Andres"
  - subject: "Re: Why usability studies are creating wrong results"
    date: 2004-09-22
    body: "Nice and very interesting read.\nThanks for the link."
    author: "Roger Larsson"
  - subject: "Seconded"
    date: 2004-09-23
    body: "Not necessarily gospel, but certainly an informative read."
    author: "Leon Brooks"
  - subject: "Re: Why usability studies are creating wrong results"
    date: 2004-09-21
    body: ">The problem with following that advice is that you get a system that is great\n>for the first 30 minutes of computer usage, but which will suck for **YEARS**\n>of computer usage that follow.\n\nI completely agree. Hope the KDE usability experts will take in consideration not only the user's \"first impact\" with KDE but also its daily usage (which IMHO is far more important).\n"
    author: "Anonymous"
  - subject: "Re: Why usability studies are creating wrong results"
    date: 2004-09-21
    body: ">>The problem with following that advice is that you get a system that is great\n>>for the first 30 minutes of computer usage, but which will suck for **YEARS**\n>>of computer usage that follow.\n \n>I completely agree. Hope the KDE usability experts will take in \n>consideration not only the user's \"first impact\" with KDE but \n>also its daily usage (which IMHO is far more important).\n\nI would agree if the conditions were mutually exclusive. But I don't think this is the case. A cleaner, leaner interface would benefit everyone. Unless you take some of the options and features off the picture. But the idea is not to do that. The idea is to present information to the user in a cleaner way. Advanced options and tools should be always accessible for whoever is interested on exploring them. \n\nThe trick is prioritize. If 90% of the users use 10% of the options most of the time, make sure these 10% options are highly visible. And the rest are acessible but not on the way.\n\n"
    author: "MandrakeUser"
  - subject: "Re: Why usability studies are creating wrong results"
    date: 2004-09-21
    body: "\"I would agree if the conditions were mutually exclusive. But I don't think this is the case. A cleaner, leaner interface would benefit everyone. Unless you take some of the options and features off the picture. But the idea is not to do that. The idea is to present information to the user in a cleaner way. Advanced options and tools should be always accessible for whoever is interested on exploring them. \"\n\nWell, I'd say it depends a lot of the feature wether it's mutually exclusive or not. In lots of cases, both can be achieved (and that's of course great) but in other cases you will have to decide between short-term and long-term usability.\n"
    author: "Roland"
  - subject: "Re: Why usability studies are creating wrong resul"
    date: 2004-09-21
    body: "There is a probability of 0% that 90% of the users use exactly the same 10% of options, rather they may each use a different set of 10%, so such an approach is complete humbug."
    author: "Datschge"
  - subject: "Re: Why usability studies are creating wrong resul"
    date: 2004-09-21
    body: "As I said elsewhere --- if this pessimistic idea was true, industrial design would be impossible. If you consider the set of options and actions 90% of users use, there is going to be a large amount of overlap between these sets. It's not going to be exact, but there will be lot's of overlap. Since KDE is configurable, it makes sense to take the intersection of these sets, and let users add the few that are missing from the default configuration, instead of taking the union and making each user live with the behaviors of every other user."
    author: "Rayiner Hashem"
  - subject: "Re: Why usability studies are creating wrong resul"
    date: 2004-09-21
    body: "Of course, because there is a big confusion about learn and use.\nOf course, we all know (and sometimes love) lots of tools that are easy to use after you use them along a few years (decades?). \n\nOf course, thinking about promoting these as mainstream tools is nonsense, KDE should be replaced with terminal windows and a nice bunch of guru software.\n\nBut thinking that something is good if the user can figure it out by himself when you make him sit and ask him to achieve tasks with no further clue is nonsense as well. Lots of nice features simply will not be so obvious nor attractive at first sight.\n\n"
    author: "Shulai"
  - subject: "Re: Why usability studies are creating wrong resul"
    date: 2004-09-21
    body: "You did not obviously bother to read the articles since none of the users were beginners:\n\nUser #1\nOccupation: VP Software Development\nEducation: Bachelor of Science in Electrical Engineering\nOS Experience: Mostly Windows, some Linux, TRS-80\nHobbies: Games, Web Development\n\nUser #2\nOccupation: Information Technology\nEducation: Masters in Geology, Geography\nOS Experience: Exclusively Windows\nHobbies: Computer Games, Perl Programming\n\nUser #3\nOccupation: Director of Development\nEducation: Master of Science in Aerospace Engineering\nOS Experience: Mostly Windows and Macintosh, some SCO Unix\nHobbies: Games, Graphics, Coding in ASP\n\n...\n\nEven if those people were beginners, it is a stated goal of KDE to address the beginners as well as the advanced users, so the result of a study of beginners behaviour in front of KDE is very important.\n\n> it's just stupid to remove options and settings.\n\nIt is much more complicated than that. Sometimes it is stupid, sometimes not. Most of the time, improving usability goes far beyound adding and cutting options.\n\n> I think the KDE-way of usability (watching bugs.kde.org for wishlist items and\n> implementing the good ones) is way superior to any \"usability study\"\n\nLuckily, the KDE approach of usability is a lot more intelligent than \"study the wishlist item\". For example, they also use the thing called brain to improve uability of applications.\n"
    author: "Philippe Fremy"
  - subject: "Re: Why usability studies are creating wrong resul"
    date: 2004-09-21
    body: "The users were not beginners as such, but they were beginners in the KDE environment. I very much agree that these first impressions, although important, can never be an argument to make usage for advanced users more complicated."
    author: "Andr\u00e9 Somers"
  - subject: "Re: Why usability studies are creating wrong resul"
    date: 2004-09-21
    body: "AFAIK all were new to KDE, so they were \"beginners\""
    author: "Roland"
  - subject: "Re: Why usability studies are creating wrong results"
    date: 2004-09-21
    body: "\" it's just stupid to remove options and settings.\"\n\nWell, not really. There are some configuration-tasks that users do over and over again. Some options are hardly ever touched, if at all. When user is doing those everyday-tasks, those rarely used options get in the way. Of course, the user will learn (with time) where the options he needs are. But it doesn't have to be that way!\n\nHow should it be then? It should be dead simple for the user where the options he needs are. And they should not be hidden amongst dozens of other options. The GUI should just present the top 10% of most used options (I pulled that percentage from my ass, but you get the idea), while the rest of the options (that are not used that often) are hidden from the user. I bet that that 10% would cover about 90% of the options the user REALLY needs. Rest are just icing on the cake. It would make configuring the UI alot smoother and simpler, and it would clean up the configuration-UI ALOT!\n\nHow could that be done. Well, we all know how Gnome-folks did it: With Gconf. That is one way of doing it. Unfortunately I don't have a solution to offer."
    author: "Janne"
  - subject: "Re: Why usability studies are creating wrong results"
    date: 2004-09-22
    body: "You clearly don't know much about how *good* usability tests are conducted.  Good tests don't simply \"take beginners\" - good tests involve representative users.  So, if a system is designed for expert users, then it should be tested with experts.  Many systems have a mix of skill levels in their user groups, and so good tests will involve a mix of user types.  Air Traffic Control systems are usability tested with expert users...at least we hope ATC systems are used by experts.  :)"
    author: "Lyle"
  - subject: "Re: How True"
    date: 2004-09-21
    body: "while many of the talk backs here are rather focused on the empty half of the glass, it's nice to read the number of positive comments from the testees, none of whome had used KDE before. a new environment is almost always uncomfortable at first, and yet we still got compliments.\n\ni wouldn't recommend testing with contrib'd packages (which is what these packages were, for Slackware), and a number of the apps used (and failed with, at times) were not KDE apps.\n\nin general, i'm rather encouraged by the article. we're making good strides forward (\"This is UNIX? I like it. I'd use it.\"), and our weak points are being found for us to fix."
    author: "Aaron J. Seigo"
  - subject: "Re: How True"
    date: 2004-09-22
    body: "> while many of the talk backs here are rather focused on the empty \n> half of the glass, it's nice to read the number of positive comments \n> from the testees, none of whome had used KDE before. \n\nTrue. I guess it is just becaause we all feel like making suggestions for improvement, but you are right. \n\nI fell myself in love with KDE a while ago, with the 2.* series. The reason I loved it (and I still do) was in great part _consistency_ in the interface. Want Help ? \"Help -> Handbook\". Need to configure ? \"Settings -> Configure\". Interprocess communication works beautifully. You are browsing pics in Digikam, select a few, right-click, choose \"email pictures\", you are asked what size you want them, that's it. Component model (KPart) the same thing. All this power in the kdelibs makes KDE quite a pleasure to use (and to program for, for the little bit I experimented). \n\nThe \"empty half of the glass\" in the usability front has to do to a great extent with issues you have to address from the UI design philosophy, and this is an area where great improvements can still be made. The number of posts in this article talkback is a measure of the importance of this subject."
    author: "MandrakeUser"
  - subject: "Re: How True"
    date: 2004-09-21
    body: "Just don't make it too usable as GNOME is. KDE currently is really great. Perhaps some reshuffling of features around in the next version, to give features people use most the best presentation. (Even I have to agree that the toolbars do have quite a bit of buttons, and arguably too many.)\n\nJust don't make it another GNOME."
    author: "Alani"
  - subject: "Re: How True"
    date: 2004-09-22
    body: "I think people are taking the GNOME HIG work, and the GNOME ideological \"no new features\" work, and lumping them together. I think that's unfair to the GNOME HIG work. I recently spent a week trying out FC3-Test2, and have to say HIG-compliant apps are generally very elegantly designed and highly usable. I don't think you can fault GNOME for that. The problem with GNOME is not that they laid down some well-established UI principles, nor that they designed apps with an eye towards elegance. The problem is that they cut out too many features in the process, many of which they didn't have to. You could get to a pleasing level of minimality without throwing away all the features they did."
    author: "Rayiner Hashem"
  - subject: "Re: How True"
    date: 2004-09-22
    body: "Paul Graham argues that you need many features to keep the power users happy. However he says it's very important that you have SANE defaults to keep the rest pacified."
    author: "ac"
  - subject: "Re: How True"
    date: 2004-09-22
    body: "This is absolutely right. I honestly believe that Classic MacOS is a shining example of this precise balance. The UI was designed to be functional, elegant, and clean. It wasn't totally minimalistic, but tended towards minimalism. For the majority of users, this was great. For power users, this was still great, because they had tons of extensions, plugins, and haxies to choose from."
    author: "Rayiner Hashem"
  - subject: "Re: How True"
    date: 2004-09-22
    body: "Stars in password is a good thing. Could be disabled in KDE Control Center.\n\nKDE Control Center is like TweakUI for Windows9x,\nit is good for power users, but way too complex for simple task.\n\nWe need a \"simpler version\" along this tools,\nfor 'heavy common task' a strip down Control Panel would do it.\n\nAdvanced in that Control Panel could point to KDE Control Center.\nYou could also have an even more \"complicated\" version\nthat shows you ~/.kde/ config files ala \"Registry Editor\".\n\nTherefore, if some newbie wants to do simple stuff,\nhe just go to his simple KControlPanel and fix it.\n\nIf some hacker want to freak is KDE, he already knows\nControl Center and goes straight there.\n\nThat's it.\n\n"
    author: "Fred P"
  - subject: "Kmix and Knob"
    date: 2004-09-21
    body: "I currently use Knob and would have to say that it is the cat's meow. It would be useful to have it bring up kmix on right click. Also, it should be on default installed and used in starters config."
    author: "a.c."
  - subject: "Re: Kmix and Knob"
    date: 2004-09-21
    body: "It'd also be nice to set the volume in kcontrol, since that's where a lot of people initially look. Of course, the nobody can find anything in kcontrol these days, so a streamlined and search-oriented kcontrol in kde4, as well as a general pairing down on certain panels (my BS.c in CS doesn't help me any in the Crypto panel) should help there ;)"
    author: "anon"
  - subject: "Re: Kmix and Knob"
    date: 2004-09-21
    body: "knob is a nice little applet. It is simple (use left button to adjust, middle mouse button mutes). If it was installed as part of the default (think in terms of the clock being there), then usrs have a very simple quick way to check for sound.\n\n\n"
    author: "a.c."
  - subject: "Cheers to k3b"
    date: 2004-09-21
    body: "Great work Sebast and others!  K3B is getting the props it deserves!  Keep up the good work!"
    author: "am"
  - subject: "Re: Cheers to k3b"
    date: 2004-09-22
    body: "indeed, \n\nThis, at least, is one package not many people will complain about concerning poor usability.\n\nSuperb.\n"
    author: "Ernest ter Kuile"
  - subject: "Valuable information"
    date: 2004-09-21
    body: "Overall, there's some really useful tidbits in this report.  And this part was pretty funny:\n\n  + Explored KWrite\n  + \"So this is a text editor on steroids?\"\n\nYikes, good thing he didn't open Kate!  (or emacs ;)\n"
    author: "LMCBoy"
  - subject: "Re: Valuable information"
    date: 2004-09-21
    body: "Usually, when a student tells me vi is too hard[1], I pause the class for a minute, start emacs, and show them M-x tetris.\n\nThen noone complains anymore.\n\n[1] No, I don\u00b4t like vi, either, but it\u00b4s a linux admin course, and vi is the only editor kinda guaranteed to be around in case of emergency or over ssh."
    author: "Roberto Alsina"
  - subject: "Re: Valuable information"
    date: 2004-09-22
    body: "Show them tetris for Vim :)\n\nBTW. I am writing this from small intenet cafe in Hvar (Croatia) on KDE (3.1, Mozilla as primary www client).\n\nm."
    author: "emdot"
  - subject: "Very similar experiences"
    date: 2004-09-21
    body: "More or less all users (in this article) report the same difficulties in using konqueror and kmail. Users have been asking for more usability for quite some time now. But there has been only diffident changes in recent releases albeit major improvements would be achievable very easily. Why?"
    author: "mooobo"
  - subject: "Re: Very similar experiences"
    date: 2004-09-21
    body: "The problem is that it is difficult for the developers to sort out when a user ask for a better usability if this is just this user's problem or if it is a generic usability problem.\n\nMany developers are also more interested in developing advanced features than simply brushing up usability. One reason is that it is difficult to know what 'better usability' means."
    author: "Philippe Fremy"
  - subject: "Re: Very similar experiences"
    date: 2004-09-21
    body: "> The problem is that it is difficult for the developers to sort out when a\n> user ask for a better usability if this is just this user's problem or if it\n> is a generic usability problem.\n\nLet's take some more or less simple examples:\n\nLocation vs File: kick Location take File, at least make it consistent (i.e. KMail->File, Konqueror->Location)\n\nright now we have 4 (four) Configure entries in konquerors Settings menu as I just look at it, and 6 (six) Configure entries in kmail Settings, besides the unhappy quick-access settings under the Tools menu in konqueror. \n\nremove the plugin integration from konquerors toolbar by default\n\nuse nice anti-aliasing fonts by default\n\nremove unnecessary icons from kicker\n\nmake the kmenu understandable (freedesktop)\n\nlet all cookies be treated as session cookies by default and never ask again\n\nrework the kwallet including its name, better disable it by default\n\ndisable the klipper by default, but still sync X11 clippboard\n\nuse plastik by default\n\nadd basic configure wizard for kmail (sending, receiving, identity), there are already these nice virus and spam wizards, the base wizard shouldn't be a problem\n\nalso add a basic configure wizard for konqueror (cookies, proxy, homepage)\n\nadd kmail's and konqueror's basic wizards to the first-time-visiting-kde wizard\n\ndisable kde tips by default\n\nmake the top left first icon on you desktop a link to your homepage\n\nseparate homepage and $HOME\n\n... well look at the original report and add more.\n\nSome of the above definitely is arguable. But most of that stuff is realy a simple no-brainer, isn't it. \n\n> Many developers are also more interested in developing advanced features than simply brushing up usability. \n\nPeople will love both The Usability Man _and_ The Translucency Man.\n"
    author: "mooobo"
  - subject: "Re: Very similar experiences"
    date: 2004-09-21
    body: "> separate homepage and $HOME\n\ni heard thisone a lot...\n\nmaybe $HOME should have an icon with a 'folder and a house', and the homepage a 'house and a globe'.\n\nI also find the icon for ~/Documents (which is a 'document' right now in the defualt icon theme) a bit confusing, i whould prefer a 'folder and a document' there. Actually i think every icon for a sepcific folder should have a 'folder' on it."
    author: "cies"
  - subject: "Re: Very similar experiences"
    date: 2004-09-22
    body: "> I also find the icon for ~/Documents (which is a 'document' right now in the defualt icon theme) a bit confusing, i whould prefer a 'folder and a document' there. Actually i think every icon for a sepcific folder should have a 'folder' on it.\n\nThat is actually very important!  Anything which is a folder should be recognisable as such.  Using the document icon for the ~/Documents folder is really stupid.\n\nMy preferred solution may not be the easiest to implement, but I think it would be very effective: use a semi-dynamic icon made out of two parts - the normal folder as the base, and some other relevant icon, reduced in size and blended in in front.  Normal folders would only consist of the back part (the folder icon from the current icon set).  This is flexible and extensible; you don't need to create a separate hybrid icon for each special folder.\n\nIf you select the option to have folders reflect their contents, it uses static icons that look much the same as what I described, and I like the way it looks.\n\n-- Steve"
    author: "Steve"
  - subject: "Re: Very similar experiences"
    date: 2004-09-21
    body: "> Location vs File: kick Location take File, at least make it consistent\n\nWhy? For example Konsole has \"Session\" as first menu entry. \"File\" doesn't make any sense there.\n\n> right now we have 4 (four) Configure entries in konquerors Settings menu as I just look at it, and 6 (six) Configure entries in kmail Settings\n\nWho is disturbed by this? It allows quick access to clearly separated always identical (except \"Configure App...\") configuration modules.\n\n> remove the plugin integration from konquerors toolbar by default\n\nThis is maybe now an option with the new Konqueror extension manager.\n\n> use nice anti-aliasing fonts by default\n\nOn every (including slow ones) machine? It's activated within KPersonalizer for the three fastest CPU settings.\n\n> remove unnecessary icons from kicker\n\nDefine unnecessary. Everyone has another opinion about this.\n\n> make the kmenu understandable (freedesktop)\n\nWhat are you talking about? Descriptive names are default since KDE 3.2.\n\n> rework the kwallet including its name, better disable it by default\n\nWhere is \"kwallet\" used in the GUI? How is the user supposed to learn about when turned off?\n\n> disable the klipper by default, but still sync X11 clippboard\n\nKlipper *is* what does the syncing. What disturbs you? Do you talk about activated actions?\n\n> use plastik by default\n\nAlready changed.\n\n> disable kde tips by default\n\nHow will users discover/read it then?\n\n> separate homepage and $HOME\n\nWork on this is happening.\n\n> ... well look at the original report and add more\n\nOh, you assume that we can read ourself? Thanks.\n\n> Some of the above definitely is arguable. \n\nBut as you see almost every of your listed points has also a negatives side or requires work before it can be changed."
    author: "Anonymous"
  - subject: "Re: Very similar experiences"
    date: 2004-09-21
    body: "Some notes:\n1) The problem with the \"configure <X>\" items is that their similarity and arrangement suggests that they are all on equal-used. That's almost never true. Most users won't define custom keyboard shortcuts, or custom toolbar layouts, and pretty much anything useful will be under the \"configure <app-name>\" entry. I'd hate to see the other ones taken away, but being hidden away in an advanced tab would be fine. Does anybody change their keyboard shortcuts and toolbar layout often enough to require immediate access to the dialogs?\n\n2) With regards to the CPU settings in KPersonalizer, the thing itself is useless. It's a fundementally ridiculous concept to try to control many unrelated binary options with a single linear scale! Instead, there should four or five entries (\"Use Animations,\" \"Use Font Smoothing,\" etc) that the user can enable or disable. If the user really wants more control, that's what KControl is for.\n\n3) With regards to unnecessary icons: opinions may differ, but somebody has to make the decision about what is unnecessary *for KDE*. You cannot just take the union of all options just because people can't agree on a common set. Instead, if there is disagreement, you should take the intersection of all sets, and let people add their pet icons themselves. To make an analogy to the coding world --- CORBA is what happened when people tried the former approach, and POSIX is what happened when people tried the latter approach. I'll let you interpret that how you wish.\n\n4) With regards to ktips: if the user doesn't want to know about the desktop's advanced features, it's not the designer's obligation to tell them about it. KTips is annoying, and is a terrible interface for browsing information. There is a reason such things went out of vogue in the Windows and Mac world. If you really want to expose users to the power of KDE, put a \"Learn More\" entry in the application's help menu, where the cool features off the app are highlighted. If such a design is used consistently among apps, users will discover this capability, and know where to look for advanced features if they want to learn about them.\n"
    author: "Rayiner Hashem"
  - subject: "Re: Very similar experiences"
    date: 2004-09-21
    body: "> It's a fundementally ridiculous concept to try to control many unrelated binary options with a single linear scale\n\nThat's why the check boxes exist to refine your selection/the proposal."
    author: "Anonymous"
  - subject: "Re: Very similar experiences"
    date: 2004-09-21
    body: "The problem is that there is no logical way of equating a position on the scale with a set of options it will enable. Quick: is font-smoothing a more expensive feature than opaque resize? Which does the slider enable first? Was that ordering backed up by benchmarks, or did the developer pick it out of a hat? If it's the latter, then that's just lying to the user! Does the poor user have any hope of knowing what the heck moving the bar to a given place will do? \n\nThe only hope the user has of some sort of control (remember: always make the user feel in control), is to use the manual checkbox. Well, there are too many checkboxes for the current dialog to be very nice for new users. Hence my proposal to replace the many checkboxes with a few, each enabling/disabling one general category of features. If the user really does want animations, but not opaque resize, well, KControl is there.\n\nPS> The Mandrake installer used to have something like this too. At least their's was measured on a concrete scale --- number of megabytes of packages to install. It was still a bad idea, and I don't remember seeing it in the 10.x installer."
    author: "Rayiner Hashem"
  - subject: "Re: Very similar experiences"
    date: 2004-09-22
    body: "> Hence my proposal to replace the many checkboxes with a few, each \n> enabling/disabling one general category of features. If the user \n> really does want animations, but not opaque resize, well, KControl \n> is there.\n\nHow is animation related to opaque resize? The former is a kind of feedback happening after the user did something while the latter lets the user tweak the interface in real time. It's easy to state that a simplification like the \"eye candy vs. performance\" bar is useless, but this is true with nearly all grouping of different global features for the sake of hiding the underlying complexity."
    author: "Datschge"
  - subject: "Re: Very similar experiences"
    date: 2004-09-22
    body: "Hmm, you're right about opaque-resize vs animations. Because of that, opaque resize shouldn't even be an option in KPersonalizer, because nearly everybody is used to having it on, and if you're not, there is always KControl.\nWith regards to the \"eye-candy/performance\" bar, it's not useless because it's oversimplified, it's useless because it's illogical. A slider operates on the principle that there is a predictable ordering of values. When the user moves the slider, they know exactly what to expect. There is no such predictable ordering here. For example:\n\nIs font-smoothing higher on the eye-candy/performance scale than opaque resizing? That's like asking if apples are higher on the fruity/fattening scale than oranges. In both situations, the first critereon (eye-candy/fruity), is subjective and impossible to order, while the second criteron (performance/fattening), is, while quantifiable, not obviously predictable.\n\nThe KPersonalizer slider is an abuse of the slider widget. It's like using scrollbars instead of sliders for volume control.\n"
    author: "Rayiner Hashem"
  - subject: "Re: Very similar experiences"
    date: 2004-09-22
    body: "I fully agree that opaque resize should be always on (my AMD K6-2 300mHz can hanfel opaque resizing with no issues, and for VNC-like remote control one can go to KControl or use the more efficient NX protocol instead). Actually I'm annoyed about several widgets within KDE not following it setting (eg. KControl's splitter), will have to make a list of them and look what's the cause.\n\nAs for the eye-candy vs. performance slider I always assumed that it's sorted by what's actually using more and less CPU in which case it's pretty fine with me. After all it does what it propagates, allowing you to 'balance' performance against eye candy. I bet depending on personal preference most people end up putting the slider all way to either performance or eye candy anyway. =P"
    author: "Datschge"
  - subject: "Re: Very similar experiences"
    date: 2004-09-22
    body: "> > Location vs File: kick Location take File, at least make it consistent\n \n> Why? For example Konsole has \"Session\" as first menu entry. \"File\" doesn't make any sense there.\n \nHar, got ya. Konsole is another candidate of wired menus. Let's see how things work currently: \n\nsave a session via Settings->Save Sessions Profile\n\nload a session via Session->MySavedSession (all saved sessions are listed in the Sessions menu; as I have lots of them they fill up to the ceiling)\n\nnew session via Session->New Shell\n\nYou see that things arn't straight here also. Better would be:\n\nFile->New Session, File->Save Session, File->Load Session\n\nand suddenly konsole is more in line with common guis. Just my thoughts...\n\n> > right now we have 4 (four) Configure entries in konquerors Settings menu as I just look at it, and 6 (six) Configure entries in kmail Settings\n \n> > Who is disturbed by this? It allows quick access \n\nWell, I personaly am not cus I got used to that over the years. But lots of new users are. Once the Configure Konqueror window is open you get confused that you are not able to configure the shortcuts, spell checker, toolbars or any other thing related to konqueror within there. You just opened the Configure Konqueror window, why shoud you close the Configure Konqueror window now and take a closer look at the Settings menu?\n\n> > use nice anti-aliasing fonts by default\n \n> It's activated within KPersonalizer for the three fastest CPU settings.\n\nWhy must I activate menu shadows and animation to get font antialiasing? Nobody opens the details menu to waste even more tweaking KDE.\n \n> Oh, you assume that we can read ourself? Thanks.\n\nYou are welcome.\n \n> Some of the above definitely is arguable. \n \n> But as you see almost every ... requires work before it can be changed.\n\nHm.\n\n\n"
    author: "mooobo"
  - subject: "Re: Very similar experiences"
    date: 2004-09-22
    body: "> File->New Session, File->Save Session, File->Load Session\n\n... and no menu to load a profile. Lets add File->Load Profile \n"
    author: "mooobo"
  - subject: "Re: Very similar experiences"
    date: 2004-09-22
    body: "> You just opened the Configure Konqueror window, why shoud you close the Configure Konqueror window now and take a closer look at the Settings menu?\n\nYou take a look at the settings menu before opening the \"Configure Konqueror...\" window. And you see, assumed you read top to bottom, the \"Configure Shortcuts...\" and \"Configure Toolbars...\" items before you find the \"Configure Konqueror...\". If a user is such short minded to forget what he read seconds before, then no GUI can help him."
    author: "Anonymous"
  - subject: "Just fix the JavaScript and we'll be fine"
    date: 2004-09-22
    body: "So many websites have marginal JavaScript on them (most notably Internet Banking sites, but all kinds), and Konqueror just doesn't work on them. A bit of a push to bring it up to at least Mozilla's standards of JavaScript robustness and I'd never have to start a Gecko-based browser again (nothing wrong with them, but I use Konqeror for practically everything else and starting a new browser is always a small hassle, my goodness I'm getting spoilt).\n\nKMail's fine - almost overwhelming for features and they all seem to work for me - but KNode needs a bit of polishing. Large posts get *very* slow to edit and message threading's not always up to par. I'd like to see a few improvements like \"write thread/selected messages to disk as individual files/mailbox\" and KMail integrated into KNode as mail sender. I wouldn't use post-HTML-article but I'm sure that if it was there, others would."
    author: "Leon Brooks"
  - subject: "What do we want?"
    date: 2004-09-21
    body: "We want the formula for converting plumber into gold. There is no way for the new user to know the system in 1 minute. That's simply imposible. KDE has many details that can be better in \"usability\", but many of the points I see in the linked article hit the same wall. Beyond a line \"usability\" changes to \"bad engineering\", and that's something almost all FS/distro developers never are going to do. Let's se some stupids and good points:\n\n*\"It doesn't show me that I'm typing in my password?\"\n-> Showing the pasword length is considered a security fault in many books, for example Tanenbaun's \"Modern Operating Systems\"\n\n*Thought web browser icon was not intuitive\n-> What browser icon is intuitive? the solution is simple. Put a konqueror icon on the desktop with label \"Browse the Web\"\n\n*\"Why do I want to know about this KDE wallet system? I just want to login.\"\n-> What the hell is security? I click in all .exe I receive, and I have a post it on mi credit card with the PIN.\n\n*Did not understand downloading status/progress of downloading files\n-> 50% is to difficult for me, should be \"half the way to 0wnz the filez\"\n\n*Download manager did not give status of download\n-> kget? really not?\n\n*KDE has too much setup and installed by default. I'd rather be able to easily install what I want without having everything in the way. But the tea maker can stay.\n-> That's it! then people will say kde comes with no apps. What the ...! he thinks tea maker is the best app!\n\nThere are some points he says great comments, but another..."
    author: "peroxid"
  - subject: "Re: What do we want?"
    date: 2004-09-21
    body: "I forget this, is really good:\n\n*\"Save Link as... does that save the link or the actual file?\"\n-> Why is the word \"link\" in the menu entry?\n\nYou can't do a stupid-safe gui, they have too much imagination."
    author: "peroxid"
  - subject: "Re: What do we want?"
    date: 2004-09-21
    body: "For the Save Link as, I know what it does only because I tried.\n\n\"Save linked $object\" might be friendlier, no ?\n\nBut generaly, I think the problems come from packaging. It could be clever to detect the type of machine before installing the vaio-specific modules. But the that _is_ outside the scope of KDE.\n\nuntil it integrates with HAL, that is..."
    author: "hmmmm"
  - subject: "Re: What do we want?"
    date: 2004-09-21
    body: "To tell the truth, it's the fault of the designers on this one. In English, \"save link as\" really does mean save the link itself. Techies can probably infer what it really means, but it's factually incorrent nonetheless. It should be \"save target as,\" since your saving the target of the link, not the link itself."
    author: "Rayiner Hashem"
  - subject: "Re: What do we want?"
    date: 2004-09-22
    body: "Yes, I'm with you. \"Save target as...\" would be better."
    author: "Willie Sippel"
  - subject: "Re: What do we want?"
    date: 2004-09-22
    body: "\"Save link as\" really is unintuitive. I wondered the same thing myself. You're not saving the link, you're saving the target of the link.\n\nIf I have a web page, I can put a link to www.cnn.com on it. If I choose \"save link as\", it saves the HTML page on the other end of the link, not the link!"
    author: "AC"
  - subject: "Re: What do we want?"
    date: 2004-09-21
    body: "\"Showing the pasword length is considered a security fault in many books\"\n\nThat's security by obscurity. An attacker can always listen to how many keypresses you type or simply look at your hands. If you really care about security you shouldn't be entering passwords when someone else is around at all. The asterixes are not about security, they are about user-feedback : \"hey I am really typing in a password, let's pay double attention\"."
    author: "-"
  - subject: "Re: What do we want?"
    date: 2004-09-21
    body: "I'm not convinced that vanilla KDE defaults to \"don't show stars\". At least the  KDM kcm defaults to \"one star\"."
    author: "Anonymous"
  - subject: "Re: What do we want?"
    date: 2004-09-21
    body: "Yea, this perplexed me too, I'm pretty certian that by default is shows dots when typing passwords.  I don't use slackware, but I'm somewhat guessing that it wasn't kdm being used as the login manager."
    author: "Joe Kowalski"
  - subject: "Re: What do we want?"
    date: 2004-09-22
    body: "Yeah! you can hear the clicks, and look the hands. But that's more dificult than looking the screen (for example in the table from behind in a college lab)"
    author: "peroxid"
  - subject: "Re: What do we want?"
    date: 2004-09-21
    body: "\"*\"It doesn't show me that I'm typing in my password?\"\n-> Showing the pasword length is considered a security fault in many books, for example Tanenbaun's \"Modern Operating Systems\"\"\n\nWhy not do it like it's done in Lotus Notes? It shows at random 1-4 asterisks at each keypress. That way onlookers can't guess the length of the password by looking at the number of asterisks."
    author: "Janne"
  - subject: "Re: What do we want?"
    date: 2004-09-21
    body: "Lotus notes is the #1 example of bad interface design.\n\nhttp://digilander.libero.it/chiediloapippo/Engineering/iarchitect/lotus.htm\n\nOr was that a joke I did not understand? \n\nEleknader\n"
    author: "eleknader"
  - subject: "Re: What do we want?"
    date: 2004-09-21
    body: "Lotus Notes certainly has it's share of usability-problems (I should know, I'm using it at this very moment!). But does that mean that EVERYTHING in Lotus Notes sucks? No it does not! The password-box if perfectly OK. Yes they do mention it in the website you linked. But I think it's OK. Of course it's not perfect security, but it's more secure than displaying 1 asterisk at every keypress."
    author: "Janne"
  - subject: "Re: What do we want?"
    date: 2004-09-21
    body: "Hi,\n\nwith that solution I am sure, people would be confused and think they mistyped, hit backspace and believe their keyboard is broken. I have seen this happen with the (existing!) 3 stars constant per character option for passwords.\n\nThe point is, if you know what's going on, you are (almost) fine. I would suggest to place a text explaining the invisibility of the password typed in case people enabled that. That would explain things.\n\nBut kdm is out of the scope of KDE anyway, more or less, isn't it? I think that is a strawman or Slackware default problem anyway. I doubt that Suse, Debian (that I would know) or Redhat (at least of 9 and RHEL 3 I know) ever did this by default, why should they? The argument about obscurity is well placed. If you can count the stars, you can also see my fingers, can't you?\n\nFor many other problems, couldn't we just let people decide themselves how to handle their security on one more personalizer step? This approach seems to be very well received. I have seen people really love that approach much. Without too many details (or just as much you like to) you can achieve shifts in the default.\n\nWe sure could make profiles that already provide defaults for cookie handling, password storage, HTML Email image downloading, etc. that avoided the need to change much things at all for many.\n\nYours, Kay\n\nPS: I e.g. use KWallet at home and work as a perfectly safe harbour for my passwords. I would even like it to be possible to never enter the wallet password in the first place. That is because, at work I have only passwords that are not my own in there and I love to forget about them. And at home, I have important things there, but nobody can access KDE there, or if he can (having broken in from the internet), well, he can spy on me with keylogging anyway. \n"
    author: "Debian User"
  - subject: "Re: What do we want?"
    date: 2004-09-21
    body: "Just show the stars and forget about it!  Odds are, if someone can see the number of stars showing up, they can just as easily see or hear your keyboard presses.  The trivial amount of \"security\" that is obtained from this is vastly outweighted by the user feedback of:\n\nIs this thing actually typing?  Why can't I see anything?\n\nand\n\nUmm...I mistyped...or did I?  How come there are 6 stars on the screen for 2 characters?\n\n(I just installed KDE 3.3 on my Slackware system a couple weeks ago and had all four members of my family ask one of those questions over the last week.  Hell, it confused ME for a short while.  Yes, it defaulted to not showing any stars.\n -Charles"
    author: "Charles Hill"
  - subject: "Re: What do we want?"
    date: 2004-09-22
    body: "That could also be interpreted as \"Uhh, my keyboard is broken\".\nNo, maybe a 'blinking' star/ dot/ whatever would be best, blinking each time you press a button - just to make sure the user get's a 'key down' acknowledgement... "
    author: "Willie Sippel"
  - subject: "Re: What do we want?"
    date: 2004-09-21
    body: "The problem with many usability studies, even professionally conducted ones, is the complete lack of scientific method, and in some cases even logic. Why is everything based on anecdote? Where are the control groups? Why initial use of the software assumed to be representative of normal use? Where are the metrics?"
    author: "Brandybuck"
  - subject: "Re: What do we want?"
    date: 2004-09-22
    body: "It is begining to look to me like to many \"experts\" first use _is_ usability. You can't discount the fact that if someone (and it seems like all testers have attention deficit disorder) gets frustrated in 30 seconds they may never use the program again. Clearly first impressions are important... however they had to use something and they almost certainly had to use something they did not like because it was either mandated or the only option. This means being able to work with a horrible program is strangely acceptable for many users. Beyond that there is the devil you know. People will cling to something that can get the job done. Here's the worst part... the more their current tools grate on them the less willing they will be to accept any hurdles learning something new.\n\nThe ironies of all this remain. First of all software has some inherent complexity related to the task it performs. Second, and critically important, as crucial as a first impression is, first impressions are a miniscule part of the over all user experience. Why ignore this? I think most people know how to drive a car. Many car reviews are revisited to see how the vehicle performs and what impression it makes on the driver after a number of miles. I think two weeks and six months are good benchmark times to see how a user views software. Primarily because there is an inevitable learning curve. If you eliminate all learning you eliminate any enhanced value.\n\nSadly the ultimate result of first impression software is software that introduces no innovation and offers little ability to go beyond what you first did. Why do we like the web while most people don't like media advertising? 30 second sound bites don't have room for content, just cliches. While I don't want to discount the value of making a good first impression, I also don't want to devolve to where we make software only for people with amnesia. Isn't it strange that people always seem to turn to their children to help them with computers? At what age does learning and exploration become an annoyance?\n\nI think of looking at a spreadsheet where one person has not used them and the other uses them all the time. Kspread would be too confusing to one and probably unsatisfactorily too simple to the other. The most important thing about usability remains the user, and they will never be all alike. They will also all hopefully use the program more than a few minutes.\n\nTo me usability is how well and how efficiently a program performs a task on an ongoing basis. The critique comparison in the aticle between those who just want to work and those who tinker ignores the fact that the guy who tinkers on a rocket car is going to pass the guy in the econo-box real fast. It doesn't take long to get things how I want them and them productivity reveals how usable it is. True usability cannot be discovered in a few seconds."
    author: "Eric Laffoon"
  - subject: "Re: What do we want?"
    date: 2004-09-22
    body: "\"Sadly the ultimate result of first impression software is software that introduces no innovation and offers little ability to go beyond what you first did. Why do we like the web while most people don't like media advertising? 30 second sound bites don't have room for content, just cliches. While I don't want to discount the value of making a good first impression, I also don't want to devolve to where we make software only for people with amnesia. Isn't it strange that people always seem to turn to their children to help them with computers? At what age does learning and exploration become an annoyance?\"\n\nYet another great quote, thanks. ;)"
    author: "Datschge"
  - subject: "Re: What do we want?"
    date: 2004-09-22
    body: "\"Sadly the ultimate result of first impression software is software that introduces no innovation and offers little ability to go beyond what you first did. Why do we like the web while most people don't like media advertising? 30 second sound bites don't have room for content, just cliches. While I don't want to discount the value of making a good first impression, I also don't want to devolve to where we make software only for people with amnesia. Isn't it strange that people always seem to turn to their children to help them with computers? At what age does learning and exploration become an annoyance?\"\n\nThat is a strawman argument - what is at issue is not \"first impressions\" but making things usable from the start. You have a serious attitude problem - the users reactions are in you face, but you choose to ignore it and live in denial. You are only hurting yourself with this attitude because, if you are not prepared to listen to your users, your work will be less used by them than it could have been.\n\n"
    author: "will"
  - subject: "Re: What do we want?"
    date: 2004-09-22
    body: "\"You can't discount the fact that if someone gets frustrated in 30 seconds they may never use the program again.\"\n\nIn the first thirty seconds of trying SuSE, my friend gave up on Linux and Unix forever.  He said it was way to hard for people. It made him log in with an account and password. That was his stated reason. Is it worth dumbing down the software so that we could get him as a user? Or could we possibly live without his use of Linux? I think the latter is more likely.\n\nWe should be focusing on true usability, and not anecdotes. The goal should be ease of use and not mere simplicity. While we do want to make a good first impression, the continuing impressions of the experienced users are much more important."
    author: "Brandybuck"
  - subject: "Re: What do we want?"
    date: 2004-09-21
    body: ">*\"It doesn't show me that I'm typing in my password?\"\n>-> Showing the pasword length is considered a security fault in many books, for example Tanenbaun's \"Modern Operating Systems\"\n\nOK,there is a tradeoff between users expectations and security.\nAn easy way to improve security is to increase the minimum length of password, this way showing a star for each character won't create a too big security problem..\n"
    author: "RenoX"
  - subject: "Re: What do we want?"
    date: 2004-09-22
    body: "What on earth does your response to the KWallet observation have to do with the actual observation?!?\n\nWhen I log on to a web site, I type my username and password.  I do not expect to, nor do I want to, launch another application that I just have to dismiss simply by logging in.  I have my passwords memorized, thank you very much.  Saving them to disk, even with a master password, is LESS secure.  And last time I checked, clicking on an EXE file does nothing on Linux--especially on a SPARC.\n\nI think it's plainly obvious that KWallet is a little too in-your-face, mostly by assuming that you actually want to use it.  Perhaps a \"Do you want to save your passwords?  Yes/No\" would be better than a \"Click Next to begin setting up KWallet\".  Just a suggestion.\n"
    author: "ac"
  - subject: "Re: What do we want?"
    date: 2004-09-22
    body: "You can choose not to use kwallet. And when you are registered in several pages, some of them you visit 1 time in 3 months, kwallet is a great help. That if you don't like to use same login/pass for all them.  I don't have _all_ my passwords memorized, thank you very much.\n"
    author: "peroxid"
  - subject: "Re: What do we want?"
    date: 2004-09-22
    body: "And how exactly would \"Would you like to save your passwords?  Yes/No\" get in your way??\n\nThe problem, let me repeat, is that KWallet assumes you want to use it, not that it exists.\n"
    author: "ac"
  - subject: "So, what we can gather from this:"
    date: 2004-09-21
    body: "1) Show asterisk when typing the password\n\n2) Antialias by default. And that means getting rid of Helvetica (Good riddance!)\n\n3) Tone down Kwallet\n\n4) Reduce the number of configuration-options (note: this does NOT mean reduction in configurability!)\n\nThose four seemed to be repeated over and over again."
    author: "Janne"
  - subject: "Re: So, what we can gather from this:"
    date: 2004-09-21
    body: "It would be nice if the konqi-plugin \"archive web page\" \nis integrated into the save as dialogue box as an additional \noption. Also there should be an option to \"use cache\" \nin the save as dialogue to enable faster archiving.\n\n"
    author: "Asokan"
  - subject: "Re: So, what we can gather from this:"
    date: 2004-09-21
    body: "I think you missed the most obvious and glaring problem that came up again and again, and would cause a person to not use KDE.\n\nThings didn't work. Applications crashed.\n\nUsability issues are difficult to get right, but in the end we learn another way of doing things. But when applications don't work, or crash, or don't render properly, we don't use them. We can't.\n\nThe anti-alias default being off is a legacy of flakey aa from just a short time ago. It was unreliable. It isn't any more, so it makes sense to enable by default.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: So, what we can gather from this:"
    date: 2004-09-21
    body: "Of course non-finctioning apps are a problem. But IMO they should be handled separately from mere look 'n feel issues and usability of the apps (UI-wise).\n\nOne of the pet peeves I have had with KDE (well, this is a minor issue, but it's annoying) is how some configuration-windows are too small (or more likely: the configuration-windows are just crammed full of stuff). I mean that they are so small that I have to scroll around them, or (preferrably) make the window manually bigger. Things like that leave a bad taste in the users mouth and make the whole environment seem unpolished. I'm planning to document where I encounter them and report in in the mailinglist.\n\nApps not working is a problem. However, 3.2.3 was rock-solid for me. 3.3 has some hiccups (most notably Kontact) stabilitywise. I assume things will get better with 3.3.1 and beyond."
    author: "Janne"
  - subject: "Re: So, what we can gather from this:"
    date: 2004-09-22
    body: ">Of course non-functioning apps are a problem. But...\n\nNon functioning applications prevent the user from doing anything. Non functioning apps also include network daemons that are not configured yet pop up warnings or take screen real estate. Non functioning apps also mean unusable with keyboard only. Or with a screen reader.\n\nForm follows function. A well designed application that assumes reasonable defaults, detects all detectable configuration options, works as designed and as expected, has some thought put into various modes of usage, ie keyboard, mouse, screen reader, will be by all accounts usable.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: So, what we can gather from this:"
    date: 2004-09-22
    body: "Well, I concentrated mainly on the UI-issues. Of course non-functional software has pretty crappy usability since it doesn't work ;). But I think that some of the application-problems they encountered are not due to KDE itself. I mean, I have very little problems with KDE. Just about only problems with non-functioning apps I have in 3.3 are Kontact (that crashes on me) and Amarok (But I use a beta-version).\n\nI do agree with you 100% about form following function."
    author: "Janne"
  - subject: "Re: So, what we can gather from this:"
    date: 2004-09-21
    body: "    4) Reduce the number of configuration-options (note: this does \n    NOT mean reduction in configurability!)\n\nYeah baby! Configuration based on thought-user-interface is the way to go!\n\nSeriously, some options make sense, a few ones don't and can be chopped out \n(I used to ask for some foreground new tabs in Konq 3.1 and some ones in background, but I do not miss too much having the two options in the menu).\nBut I guess I can have a really good time having people figuring out how to have the same configurability with less configurtion options (of course, Windows and Gnome are NOT the way to go).\n\nBy the way... the example about tabs... there was a different study referred by KDE about this: \n\nhttp://web.archive.org/web/20021017112107/www.iarchitect.com/mshame.htm\n(the original site is gone)\n\nSo, you use icons with a scrollbar and people do not figure it out. You use tabs, but if you put more that four or five it sucks. IE/OE/MSWord config sucks for me, and those aren't the worst.\nTabs in several rows are really, really, really confusing. Tabs with a scroll arrow (I saw a few of them) are harder to figure out than category icons.\nSo, please, stop the madness... People do not complain they should not have to drive cars to get a license, they do not complain about confusing traffic signs, they just learn those!!!\n\nIs SO HARD to learn what a scrollbar is, what it means, what is does, how it works?\n\nSorry about the rant, but there is no gain in doing all this usability thing when you simply ignore the fact that you work for users that doesn't think, doesn't care about learn and doesn't care about nothing. They do not worth the effort.\n\nThen, make people understand they should learn a bit for their own good if they want to use computers, or let people use Gnome version 2.dumb or whatever else.\n\nSpend your effort working for people willing to learn. Keep Kandalf's tips.\nBuild a tour, run it after the customize wizard and encourage new users to check it out.\n\nBut, again... if the effort is made, make it worthy.\n"
    author: "Shulai"
  - subject: "Re: So, what we can gather from this:"
    date: 2004-09-21
    body: "\"And that means getting rid of Helvetica\"\n\nI wish we could. But what other sans serif fonts are guaranteed to be available on the user's system. The only alternative I can see is to make Bitstream Vera a required dependency (as I don't see Luxi being much better than Helvetica).\n\nWhat we really need though is the ability to specify alternative fonts, like we do in HTML. That way we can specify Vera Sans, and if it isn't present drop down to a generic helvetic font."
    author: "Brandybuck"
  - subject: "Re: So, what we can gather from this:"
    date: 2004-09-21
    body: "> The only alternative I can see is to make Bitstream Vera a required dependency\n\nBitstream Vera has not very good international glyph support."
    author: "Anonymous"
  - subject: "Re: So, what we can gather from this:"
    date: 2004-09-22
    body: "Qt will get the required glyphs from other fonts, no? I'm not very experienced with non-latin1, sorry."
    author: "anon"
  - subject: "Re: So, what we can gather from this:"
    date: 2004-09-22
    body: "Even if it would work, how ugly would look it like?"
    author: "Anonymous"
  - subject: "Re: So, what we can gather from this:"
    date: 2004-09-23
    body: "Certainly not as bad as (unanti)aliased Helvetica. ;)"
    author: "Illissius"
  - subject: "Re: So, what we can gather from this:"
    date: 2004-09-26
    body: "I never got what people want out of configuration panels.  Let's talk specifics.  Take a dialog you hate, draw it up in gimp or whatever.  What, do you want only 6 options in a panel at a time?  More is \"overwhelming?\"  These vague statements like, \"it's too much, change it\" don't get anything productive done.  Give other workable solutions.  Only then can a programmer at least have an idea what's wanted.\n\nLike, is there supposed to be a newbie options panel, then an advanced options one?  That way you don't have to look at the advanced one?  What happens when you want to change one thing that's on the advanced panel, complain that it needs to be moved?  Personally, I consult the help, explore around to find something that might change whatever I'm looking to change.  Likewise, if the help is lacking, how about helping (pun intended) out with some better docs as you navigate around finding the setting you wanted in the first place."
    author: "JCorey"
  - subject: "Usability"
    date: 2004-09-21
    body: "1. Usability is there\n- perhaps a user behavour \"talkback version\" of KDE is needed\n\n2. Interface consistency is there\n- when you look at windows apps you find much more inconsistency\n\nWhat I think it over mayor importance\n- first impression\n- lower learning curve\n- easy connectivity to other devices such as digital camera, USB stick\n- easier upgrades\n- better documentation (not more text, less tautology:\n\nExample 1:\n\"KBabel is a suite of of an advanced and easy to use PO file editor comprising KBabel, a multi functional Catalog Manager and a dictionary for translators KBabelDict. It supports many advanced features and it lets you customize many options.\"\n--> what is a PO file, do we have to know that?\n--> What is this tool really for?\n--> What is a catalogue manager?\n--> Rekursion in definition: Kbabel is a suite ... comprising Kbabel\n--> \"many advanced features and it lets you customize many options\" Are you a business consultant? No, you want to assist users, don't tell them something that has no relevance for them.\n\nWhy not just telling that the tool is for the translation of KDE applications and other apps that use the Po-Fileformat standard.\n\nExample2:\nKhangman, unlike Kbabel you find the definition in section\n\"intro\"\n\n\"KHangMan is a game based on the well-known hangman game.\"\n---< a game based on ... game\n\n\nInconsistency in naming:\n\nKPoker\nbut\nklines\nklickety\n\nNaming:\nkmail: difficult to pronounce, why not \"Kamel\"?\nKBabel: difficult to pronounce, why not \"Kababel\"?\n\nKmail - intro:\n\n\"The KMail Team welcomes you to KMail, a user-friendly email client for the K Desktop Environment. Our goal is to make KMail a program that is beautiful and intuitive without sacrificing power. \n\nIf you have never set up an email client on a UNIX\u00ae system before, we suggest that you read through the Getting Started section first so that your setup goes smoothly...\"\n\nhere: other text style, more personal.\n- What is a \"client\"?\n- Our goal is to make ... that is beautiful ---> so it isn't?\n\n\n\"Since most people do not read documentation anyway, here is a collection of the most helpful tips:\"\n\n---> Perhaps because most of the documentations are not written in a user-friendly way. \n\nAlthough KMail can be considered reliable you should keep backups of your messages, i.e. just copy the files and folders in ~/Mail (including the hidden ones that start with a dot) to a safe place.\n\n---> \"safe place??\"\n\nKMail's homepage can be found at http://kmail.kde.org. There you will find useful links, e.g. to the user and developer mailing lists. Please report bugs in KMail using Help->Report Bug....\"\n\n---> sounds as if Kmail was very buggy and users shall ask questions at the developer's list.\n\n\n\n\n\n"
    author: "Gerd"
  - subject: "Re: Usability"
    date: 2004-09-21
    body: "    sounds as if Kmail was very buggy and users shall ask questions at \n    the developer's list.\n\nOn the other hand, Microsoft users do not get suggestions as this. They just see their apps and their entire system crashing all the time! :-P"
    author: "Shulai"
  - subject: "Re: Usability"
    date: 2004-09-21
    body: "kmail is fine to pronounce by most english speakers.. perhaps not in $YOUR_LANGUAGE_HERE, which is why it's good to tone down application names and instead use descriptions more. "
    author: "anon"
  - subject: "Re: Usability"
    date: 2004-09-22
    body: "Well, in fact K is a consonant.\n\nE in Email is a vocal. Who cares about English speakers? In most languages a consonant K + another consonant M is bad naming.\n\nAsk a linguist for advice. "
    author: "Jan"
  - subject: "Re: Usability"
    date: 2004-09-22
    body: "But all older KDE applications are named like the scheme K-App and there should be pronounce that way (K-App) too (for examples: KControl, KWord, even KOffice is made so.)\n\nThe K is supposed to symbolize KDE.\n\nHave a nice day!\n\n"
    author: "Nicolas Goutte"
  - subject: "Re: Usability"
    date: 2004-09-22
    body: "Why when do we talk about usability we put the example for the most stupid imaginable person? For that person it is imposible to make something easy. \n\nFor that person even a remote control with 2 buttons is complex \n\n-what is a channel? Do I need to know what a channel is?\n\nAnd if in the remote control you put attached the channel explanation, there will be people who will come and post that's too much complicated because there will be stupids who do not want to read.\n\nNo silver bullet for dumbs, they have too much imagination."
    author: "peroxid"
  - subject: "Re: Usability"
    date: 2004-09-24
    body: "I have been using KDE for 2 years. \nI agree that its documentation has a very poor quality.\nMy impression is the help files were written by users who simply\n recorded their experience. Consequently, those functions are described (sometimes thoroughly) that do not needs explanation. For example: Menu point File.Save will save your document into a file. Excepting the absolute beginners, all average users know that.\nIf developers have written the documentation, they could inform us those specific features of the applications that are not obvious. Yes, I know, it's very diffucult to convince a developer to write a help. Perhaps, a compromise would be, to complete the already existing docs.\nFrom my personnel experience: if I had (rarely) a question I never found an answer for that.\nThe mostly cited reply for this, that those experienced users who need extra information, they can find it in the source code. But it is not satisfactory for me: the gap between the beginners and code-read-able users is huge incuding more and more users.\n\nMy best wishes    devians"
    author: "devians"
  - subject: "Kamel?"
    date: 2004-09-21
    body: "I'm sorry, but renaming KMail to \"Kamel\" must be THE worst suggestion in this entire discussion!"
    author: "Martin"
  - subject: "Re: Kamel?"
    date: 2004-09-21
    body: "I second that.\n\nWhat I'd like to see is some widgets for X.org's soft shadows and true transparency. It would be awesome, if I could choose how the transparency will be used (like all windows are transparent, or all but the focused window are transparent, or the focused window is less transparent than the others). That would be cool, and useful also."
    author: "Jouni H."
  - subject: "Re: Kamel?"
    date: 2004-09-21
    body: "http://kde-apps.org/content/show.php?content=16114"
    author: "Anonymous"
  - subject: "Re: Kamel?"
    date: 2004-09-21
    body: "If I understood right, that program can't handle different functions of the effects, like transparency only on windows with no focus. Anyway, we need to get that program to KDE 3.3.1. I hope it's stable by then."
    author: "Jouni H."
  - subject: "Re: Kamel? It's a homophone ... "
    date: 2004-09-22
    body: "of a somewhat famous cigarette brand.\nNO thanks!"
    author: "Christopher Sawtell"
  - subject: "Re: Kamel? It's a homophone ... "
    date: 2004-09-22
    body: "It is difficult to pronounce\n\nKmail - two consonants\n\nI don't know a word that starts with \"Km\"\n\n\"Kamel\" is Camel in some languages."
    author: "gerd"
  - subject: "Kamel?"
    date: 2004-09-22
    body: "\"\"Kamel\" is Camel in some languages.\"\n\nAnd in some languages, KMail is easy to pronounce. The name derives from E-mail, so actually it's not Kmail, but K-Mail (keimeil, is that so difficult afterall?), but it's easier to write KMail. It's logical. What do Camels have in common with electronic mail anyway?"
    author: "Jouni H."
  - subject: "Re: Kamel? a good proposal"
    date: 2004-09-22
    body: "Two consonants in naming, that's bad.\n\nLook in a dictionary how many words start with \"km\". From a linguistic perspectiv is it a very good suggestion."
    author: "Jan"
  - subject: "Re: Kamel? a good proposal"
    date: 2004-09-22
    body: "As I stated before, it's actually K-Mail (keimeil). That's why it's written with two capital letters (KMail). See? Like HQ, MReal or S-word. So the K is not part of the rest of the word. I hope you understand. It's very easy to pronounce, if you just really try it."
    author: "Jouni H."
  - subject: "Re: Kamel?"
    date: 2004-09-26
    body: "Then we'll be seeing complaints, \"What's this Kamel program?  Help me manage my camel farms or something?  I'm not a camel farmer!  You will never get anywhere in this world until you acomodate me!!\""
    author: "JCorey"
  - subject: "KOffice, KDE Control Center"
    date: 2004-09-21
    body: "\"New document wizards in office applications should select new 'blank document' by default\"\n\nI think that is true. I never understood why KOffice always opens this initial  dialog when starting a KOffice application. This dialog doesn't make things noticeable easier or faster for the user, but it makes things different from all the other office suites, be it OpenOffice or MS Office.   \n\n\"The KDE Control Center (or 'KDE Control Cente' as the title says) is packed with numerous settings.\"\n\nWouldn't it make sense to offer the options \"Simple\", \"Normal\", \"Expert\" in the toolbar of the control center? This way the dialogs within the center can be designed for respective type of user.\n\nAnyways, thanks for the great piece of software that KDE is."
    author: "Anonymous"
  - subject: "Re: KOffice, KDE Control Center"
    date: 2004-09-21
    body: "> I never understood why KOffice always opens this initial dialog when starting a KOffice application.\n\nBut above request doesn't say that there should be no wizard/dialog at all?\n\n> Wouldn't it make sense to offer the options \"Simple\", \"Normal\", \"Expert\" in the toolbar of the control center?\n\nNo as everyone as different expectation what is expert and what not."
    author: "Anonymous"
  - subject: "Re: KOffice, KDE Control Center"
    date: 2004-09-21
    body: "I didn't get the KOffice startup dialog either, at first. It's part of a long tradition, though. Even back in the MS-Dos days there were application that couldn't show you anything unless you had either selected a file, or created a new one. I really hated those applications.\n\nHowever, in the current KOffice CVS things are a lot better. The initial dialog now remembers which page you used last, which means it adapts to the way you work. If you mostly work with a small set of files -- say, a writing project or some set of images for a single publication, you get those in the recent files tab. If you tend to create new documents, that's what you get shown first. Moreover, the dialog remembers your last choice _and_ you can tell it to always use the selected template.\n\nSo, if you start a KOffice application for the first time, you select 'empty document with no frills', check the checkbox, and then whenever you start a KOffice app (without specifying a file to load), you get your wish. Empty document of the type you prefer."
    author: "Boudewijn Rempt"
  - subject: "Similar experiences"
    date: 2004-09-21
    body: "I have only just started using Linux recently, and I agree with the conclusion of the review.\n\nI know its dull from a development perspective, but KDE needs to concentrate less on the features and more on the fine tuning. \n\nOften the issues are things which would be so easy to correct too.  For example, it really is confusing have 3 \"Configure\" items in the menu, when I will virtually only ever use one.\n\nThe default KDE font has a slighly comic \"kids\" look.  I realise that the MS fonts cannot be used for legal reasons, but there must be very similar OSS fonts, especially for Tahoma."
    author: "Robert Knight"
  - subject: "Re: Similar experiences"
    date: 2004-09-22
    body: "So KDE should concentrate on the fine tuning, but what's \"fine tuning\" in your case? One's improvement (\"fine tuning\") to a particular feature making it really useable for him may well look like yet another useless feature to another person who never used it.\n\nYour \"easy to correct\" example is an excellent case to show this: The multiple configure entries all handle different settings, one for keyboard shortcuts, one for toolbar setup and one for apps specific settings. All of the KDE applications have them in a consistent way when they need them, so it's easy to memorize and pick up when needed for the user. If all of those settings were crammed into one single interface (like KControl) it would be way harder for users to customize the basic interface features of KDE applications."
    author: "Datschge"
  - subject: "Re: Similar experiences"
    date: 2004-09-22
    body: "@Datschge: In your analysis, you leave out the constraint of optimizing for the common case. Every item in every menu is a mental burden on your user. That's one more thing they have to skim through when reading the menu, and one more thing they have to account for when trying to remember where things are. If something is used rarely, it should not be put at the same level as something used commonly. It's like putting your radio's channel control on a steering wheel, along with the hood-release mechanism. When you put \"Configure Shortcuts\" in the same menu as \"Configure Konqueror\" that's what you're doing."
    author: "Rayiner Hashem"
  - subject: "Re: Similar experiences"
    date: 2004-09-22
    body: "Agreed, but as was said before moving those configure entries into the configure dialog (which often is already complex as is) doesn't help improving the case for anyone but is actually worsening it for those used to the KDE scheme. As you might have noticed already the menu where \"Configure Shortcuts\" and \"Configure Konqueror\" are located is called \"Settings\" for a reason. Probably the best for people not cabable of configuring applications nor of ignoring that particular menu is letting someone else do the configuration and just remove the \"Settings\" menu afterward, which is perfectly possible already."
    author: "Datschge"
  - subject: "Re: Similar experiences"
    date: 2004-09-22
    body: "Simple solution: Take all the 'configure' options except for Configure <app>... and put them in a submenu, right below Configure <app>. "
    author: "Illissius"
  - subject: "Re: Similar experiences"
    date: 2004-09-22
    body: "\"I realise that the MS fonts cannot be used for legal reasons, but there must be very similar OSS fonts, especially for Tahoma.\"\n\nAh, just download the Windows fonts from http://corefonts.sourceforge.net/ and quit your whining.  Also, Bitstream Vera fonts are very nice, in my opinion."
    author: "Zooplah"
  - subject: "\"Couldn't find graphical FTP client\""
    date: 2004-09-22
    body: "Should we have konqueror in more default flavors?\nAs we today we have the \"Personal Files\" and \"Web Browser\"\nWhat value have the tags anyway?\nTry to enter the following in the Location:\n\"ftp.sunet.se\" and you get \"ftp://ftp.sunet.se/\"\nSo why cant I enter\n\"//host/\"\nAnd get either\n\"fish://host/\", \"smb:\\\\host\\\", \"nfs://host:/\", \"lan://host/\", or\n\"rlan://host/\"\n\nOhh... Entering\n\"//ftp.sunet.se/\" results in\n\"The file or folder file://ftp.sunet.se/ does not exist.\"\nSo no slashes gives ftp: and no slashes gives file: ...\n\nWould that help? Or is a start up movie needed inplace of ktip?\n(Written using dcop?)\n\n/RogerL"
    author: "Roger Larsson"
  - subject: "OT:  Ximian... gone."
    date: 2004-09-22
    body: "This is offtopic but go to www.ximian.com.  It's no more, they've been assimilated. The page sends you to http://www.novell.com/linux/ximian.html . I never heard of this till now.\n\nAlso read the latest on Evolution 2.0.  Seems a disaster:\nhttp://www.gnomedesktop.com/article.php?thold=-1&mode=nested&order=0&sid=1963"
    author: "ac"
  - subject: "Yes, off topic indeed."
    date: 2004-09-22
    body: "Why even post it here?"
    author: "Datschge"
  - subject: "Re: Yes, off topic indeed."
    date: 2004-09-22
    body: "Because he's an anti-GNOME troll. Sigh, it saddens me that he's *still* around after all these years."
    author: "ca"
  - subject: "Please don't dumb down KDE."
    date: 2004-09-22
    body: "I have been using KDE since it was in beta. I like it as it is. Please do NOT dumb it down so far that any witless fool can fool around with it and stuff it up completely. That would be totally counter-productive to advance KDE. I like and want the power-user features. I like the click features as they are. \n\nThere are two features I'd rather like:-\n1) Being able to print out a file from the right-click menu. OS/2 was able to do this.\n2) The ability for the user to select different sets of icons on each desktop. ie. a web desktop, a programming one, an document preparation one, and sound & graphics one.\n\nI really do not want the KDE development team to feel that they have to ape the Microsoft product line in the way that GNOME people seem to have done.\n\nI'd love to see a similar report about an untainted KDE and unix user's reaction to using Windows or O/S X. :-)"
    author: "Christopher Sawtell"
  - subject: "Re: Please don't dumb down KDE."
    date: 2004-09-22
    body: "\"Please do NOT dumb it down so far that any witless fool can fool around with it and stuff it up completely.\"\n\nStreamiling the UI and making it more usable does NOT mean it's being \"dumped down\"!\n\n\"That would be totally counter-productive to advance KDE. I like and want the power-user features.\"\n\nYou don't have to remove one bit of the features to make it more usable. You just have to implement those features in a smarter way,"
    author: "Janne"
  - subject: "Re: Please don't dumb down KDE."
    date: 2004-09-22
    body: "How often do you print out a file that you need it in a right-click menu? Konqueror already has this in the right-click menu, and it has no place there either. Neither does half the other stuff in there (Preview in Embedded Advanced Text Editor?). \n"
    author: "Rayiner Hashem"
  - subject: "Re: Please don't dumb down KDE."
    date: 2004-09-26
    body: "How often do you print out a file that you need it in a right-click menu?\n\nOften, I would not ask for it if I would not find it very convenient!"
    author: "Christopher Sawtell"
  - subject: "\"Aaargh! I can't copy stuff!\""
    date: 2004-09-23
    body: "I swipe it, it doesn't get copied, I middle-click and if I'm lucky I get stupid scroll controls, but it never pastes. Simply moving stuff around means either reverting to the keyboard or fiddling in context menus. There's no applications - I'm gunna hafta spend _hours_ downloading just to get some, and this stupid browser keeps wanting me to register or update; what's a web browser doing trying to admin a machine? And it's sooooo inconsistent; I have to run a whole separate program just to rename stuff on my web server. This MS-Windows sucks!\n\nGood enough? (-:"
    author: "Leon Brooks"
  - subject: "A different approach"
    date: 2004-09-22
    body: "Just an idea. \n\nAfter reading articles and comments on osnews and dot.kde.org maybe we should do something totally new and radically different then any other desktop environment. \n\nNo defaults at all ! Nothing. No menus, no toolbars, nothing. I am talking about a fresh install.\n\nJust one option: Settings. A new user is forced to add features by himself. At desktop level you want Kicker, ok. choose it (checkboxes) before kde even loads up for the first time. At application level you want a print icon on Konqueror's toolbar, ok. no problem, choose it among all the other options before the Konqueror appear for the first time.\n\nAnd you can later always add and remove features.\n\nIn this way we could \n\n1.give the new users possibility to choose as many features as he/she feels suits his/hers level of experience,\n\n2. give the new users possibility to learn much more about the desktop and applications structures.\n\n3. satisfy all old KDE users because they already have their defaults saved (or backuped) in their .kde directories.\n\n4. avoid all future flame wars about default settings, about too much or too less, about who is going to decide what is a default setting.\n\nCheers,\n\nwygiwyg"
    author: "wygiwyg"
  - subject: "Re: A different approach"
    date: 2004-09-22
    body: "Just a little add-on. I'm talking about wizzards. \n\nA first desktop wizzard. User decides not just about window manager styles (Windoze, MacOS, KDE etc.), he/she decides everything:\n\n1. kicker or not \n\n2. icons on desktop or not (I wouldn't choose icons)\n\n3. antialiased fonts or not (I would choose antialiasing)\n\n4. how many entries in main menu (I wouldn't, for example choose games nor edutainment)\n\n5. how many entries in right mouse button click\n\netc.\n\nThe same at application level."
    author: "wygiwyg"
  - subject: "Re: A different approach"
    date: 2004-09-22
    body: "It's an interesting idea. The user would build the environment from ground-up to suit his or her needs. Unfortunately, it wouldn't work.\n\nWhat you are suggesting would mean that it would take the user HOURS just to be able to use the environment! With all the clutter KDE has right now, the user can still use it.\n\nIf the user is overwhelmed now, imagine how overwhelmen they would be with this system! First boot: \"Huh? How many menu-entries? What kind of Kmenu? Icons or not? what apps to display? Kicker or no Kicker? What's a Kicker?... There, now I can use the desktop. Let's try email.... Argh, same questions again! How many menu-entries, toolbars or no toolbars, what icons in the toolbar....\"\n\nWhat I would like to see is bare minimium of enabled features and UI-elements. As little as possible, but enough for the UI and the apps to be usable. The user could then enable more elements and features as he sees fit. This way we would have streamlined UI that is suitable for novices. More advanced users (and novices as they progress) could then enable additional functionality.\n\nThat would basically mean that the environment grows as the skills and abilities of the user grows. You have a foundation, and you build up from there as you see fit. Right now in KDE, you have everything turned on and with all the options visible. The user has to trim down the environment in order for it to suit his needs."
    author: "Janne"
  - subject: "Re: A different approach"
    date: 2004-09-22
    body: "kpersonalizer could ask users running kde for the first time, about their \"level of experience\" in computers. for example options could be:\n- i'm new to computers\n- i'm quite experienced in W**dows (settings would be similar to MSWin)\n- i'm quite experienced in KDE\n"
    author: "yemu"
  - subject: "Re: A different approach"
    date: 2004-09-26
    body: "long as you can do that .. and keep it to 3 or less options so as not to overwhelm them.  Although it may still piss off those with an option-tolerance of 0-2..."
    author: "JCorey"
  - subject: "Reduce configuration options per view!"
    date: 2004-09-22
    body: "On my home PC I use KDE with resolution of 800x600. Some apps and configuration dialogs are fine, but some - a pain in da *. IE. panel configuration dialog doesn't fit on 800x600 screen - I cannot see \"OK\" button. I always need to configure panel using ControlCenter. And so on.\nSuch problems in configuration dialogs can be reduced if amount of configuration options per view (tab) is reduced. If I open configuration tab with lots of options, Im too lazy to check all of them - so I probably will miss some good configuration feature.\n\nMy 2 cents. \nPS. Sorre for my ENG."
    author: "MZM"
  - subject: "Re: Reduce configuration options per view!"
    date: 2004-09-22
    body: "If you come across dialogs that don't scale to 800x600, please report them as bugs using the proper way (bugs.kde.org).\n\nThat you don't check the many configuration options available is in my view your own problem. Less options per tab results in more tabs if the current congurability is to be maintained (which IMHO, it should). Would you check more tabs?"
    author: "Andr\u00e9 Somers"
  - subject: "Re: Reduce configuration options per view!"
    date: 2004-09-22
    body: "I dont have internet @home - so I always forget to report such stuff, but reporting from w2k isnt fun :)\n\nI also understand that we need lots of configuration options but the question is - how to make it user (lamer) frendly. If tab content can be guessed by its name, users will look inside it. It is always possible to add advanced tab or button so  g33ks will feel comfortable too.\n\nNot checking all options and not reading long texts is not mine problem only. Think about Joe AverageUser. How many times he reads a bit more than 2 - 3 sentences, if he isn't intrested in it? 5 to 8 optins per tab IMHO are OK, but not more."
    author: "MZM"
  - subject: "Re: Reduce configuration options per view!"
    date: 2004-09-22
    body: "It means that those dialog do not conform to the KDE Styleguide. Hey, KDE even has a styleguide! ;-)\n\n(Yes sometimes it's true for Quanta dialogs as well...)"
    author: "Andras Mantia"
  - subject: "Tip: hold Alt key to drag panel"
    date: 2004-09-23
    body: "Won't fix the usability issue, but will allow you to use the panel."
    author: "Leon Brooks"
  - subject: "The Konqueror claims in the report are true"
    date: 2004-09-22
    body: "One thing should be clear, I hope: something needs to be done to Konqueror. There are _so_ many things that seem odd to both beginners and experienced users. I didn't even use Konqueror until a few months ago, as it did and still does look a like a developer's version which has no usability thinking applied.\n\nLet's start with the menus: Location, Edit, View, Go, Bookmarks, Tools, Settings, Window, Help. It's an awful lot of menus that can be confused one with another. Why couldn't we just combine some of those? Like...\n\n- Location and Go? Aren't those about the same thing, the location we are currently or are changing to?\n- View and Window? And shouldn't options for tabs be in the same menu as options for (new) windows?\n- Location, Go, and Bookmarks? Perhaps Go and Bookmarks together, Location having the Tabs options from Window menu?\n- Edit and Tools? Could these be combined? And \"copy files..\" and \"move files..\" moved to the tools submenu from the currently too long edit menu.\n- Settings... the most hated one. Shouldn't Show Menubar be in view -menu? I can manage with the multiple Configure... entries, though I do think that they should be reduced overall. But the whole Configure Konqueror dialog is too cluttered, too. The categories are not in any sensible order, and there are too many places that can be confused. Look at e.g. Firefox a little bit...\n\nOf course, at the same time, each menu's entries should be reconsidered/ordered. And there are too many icons like has been pointed out.\n\nMaybe there are just too many flaws in Konqueror right now, that people are scared to think how the UI should be redone? People are scared to remove all those icons which just clutter the UI, because they think that the few people who have learned them (though they could be similarly or better productive by other shortcut methods) would yell if they were removed?"
    author: "tj"
  - subject: "Re: The Konqueror claims in the report are true"
    date: 2004-09-22
    body: "Please try the \"SimpleBrowser\" profile of konqy (which comes with 3.3). What do you think ?\n\nBye\nAlex\n"
    author: "aleXXX"
  - subject: "Re: The Konqueror claims in the report are true"
    date: 2004-09-22
    body: "Well, I didn't notice anything different, apart from that it closed all my tabs. And that's not + for usability.\n\nThe only problem I see is the file browsing profile. It has maybe few too many buttons to understand if you can't read. Really, at first I also thought Konqueror was too bloated, but then I started to read what the different widgets said. It took me about 30 seconds to learn the system, and now it's not too complicated for me at all. In fact, in my opinion it's quite smart.\n\nI have only one real complaiment: some options and features can be accessed through too many places. That's what makes the system really bloated. One feature, one button. That's the way it should be."
    author: "Jouni H."
  - subject: "Re: The Konqueror claims in the report are true"
    date: 2004-09-22
    body: "> I didn't notice anything different, apart from that it closed all my tabs. And that's not + for usability.\n\nKDE 3.3 asks you if you really want to load another profile if you have tabs open."
    author: "Anonymous"
  - subject: "Re: The Konqueror claims in the report are true"
    date: 2004-09-23
    body: "I noticed that now, but apart from that how does the \"Simple browser\" differ from the default browser profile?"
    author: "Jouni H."
  - subject: "Re: The Konqueror claims in the report are true"
    date: 2004-09-23
    body: "Try starting konqy directly with this profile e.g. from the kicker button.\nIt gets rid of two menus (\"Go\" and \"Window\"), it merges the main and the location toolbar and reduces the number of toolbar icons and menu itmes significantly.\n\nBye\nAlex\n "
    author: "aleXXX"
  - subject: "Re: The Konqueror claims in the report are true"
    date: 2004-09-25
    body: "Wow! That's what I wouldn't call simple but smart, because you can gain access to those stupid widgets anyway. With few improvements this should be set as default!"
    author: "Jouni H."
  - subject: "Learn from OS X's Eyecandy"
    date: 2004-10-01
    body: "I think OS X has a lot of nice eyecandy features that would be good to implement into KDE:\n1) Put shadows under every object (window, mouse pointer, etc)\n2) Put a blue 'halo' around field boxes when they are active (eg. when the cursor is on them) to make it easy for the viewer to know where they're typing\n3) Make the cursor blink more slowly and be more evident... Make sure the cursor doesn't temporarily vanish as people are typing passwords, usernames, etc. That's _very_ annoying.\n4) Make icons of higher resolution\n5) Make icons \"light up\" when mouse is over them (RedHat does that, somewhat).\n6) Make icons bigger/higher resolution in the menu that appears when \"Alt-Tab\" is pressed.\n7) Employ window transparency (which is now possible with the latest version of X.org) when showing the \"Alt-Tab\" menu.\n8) When prompting for usernames and passwords, make the \"OK\" button shine blue when the user is eligible to press it (eg. when the user has typep something in both the username and password fields).\n9) In the KDE \"Start\" Menu, label programs with their straighforward English descriptions, and only afterwards put the KDE application name (eg. Konqueror)\n\nMore meaty changes:\n9) Make LAN accessible by default through konqueror. I've used several boxes with KDE, but could never browse the LAN.\n10) Don't use things like smbfs:// or file:/ when browsing in Konqueror. Most users don't care if the other computer they connect to is Samba, AppleTalk, Novell, etc. All they want to be bothered with is a plaintext computer name, their username on that computer, and their password.\n11) Include a GUI for resizing partitions, especially NTFS. This should be pretty simple, now that YaST has been GPL'ed.\n12) Make fonts better by default. I've used KDE on Gentoo and Debian, and for some reason the fonts on the window titles seem very rudimentary, ragged. Fonts also look that way on the KDE splash screen (as in \"Initializing peripherals\") and in the help dialogs. Strangely enough, the fonts on the KDE 3.3 screenshots from the KDE homepage look great... Maybe I just don't know how to setup fonts properly, but if I can't do that, many other users can't either.\n13) Make the 4-desktop preview at the bottom  alot bigger by default, and allow apps. to put their icon in the miniature window. Gnome on Redhat 9 does that very well - for example, galeon shows their foot logo in the miniature window when it is open in a desktop.\n\nThe guys working on KDE do a incredible job, and it is fantastic that they've achieved so much. Now all that needs to be done is these and perhaps other minor tweaks before KDE becomes the best darn desktop (free or proprietary) out there!"
    author: "Vlad"
---
Celebrating one month of KDE 3.3 out in the wild, <a href="http://www.userinstinct.com/">userinstinct</a> put together a <a href="http://www.userinstinct.com/viewpost.php?postid=kde33review">usability review with user testing</a>. <i>"Based on feedback from our test group, the default settings for a number of KDE parameters differ from what is usually expected and desired by users. Providing better defaults would reduce the time users spend looking for configuration settings and would provide a better "out-of-the-box" experience."</i>



<!--break-->
