---
title: "KDE 3.2 Beta 2 Reviews Roundup"
date:    2004-01-02
authors:
  - "binner"
slug:    kde-32-beta-2-reviews-roundup
comments:
  - subject: "That \"rather controversial article\" has a point"
    date: 2004-01-02
    body: "And the biggest point really is that HIG mention, just try to install and customize a dozen machines at the same time and you see why that makes perfect sense.\n\nAnd yes, I really miss some of the other features that are mentioned on that article too.\n\nEven when I'm using kde-3.1.94 as the only desktop installed... ;-)\n\n"
    author: "Nobody"
  - subject: "Re: That \"rather controversial article\" has a point"
    date: 2004-01-02
    body: "Perhaps some points with many more proven wrong (see OSnews comments) points.\n"
    author: "Anonymous"
  - subject: "Re: That \"rather controversial article\" has a point"
    date: 2004-01-02
    body: "He's right about Konqueror's rendering in 3.2 as well. \n"
    author: "cbcbcb"
  - subject: "Re: That \"rather controversial article\" has a point"
    date: 2004-01-02
    body: "Yes, it feels like the merging stopped at half the way. But hopefully the emerged regressions are fixed during 3.2.x lifetime. For 3.3/4.0 it will be interesting to see if Webcore, being a faster developed and better maintained fork, will be picked up as main stream by the KDE people."
    author: "Anonymous"
  - subject: "Konqueror"
    date: 2004-01-02
    body: "I agree, I'm rather disappointed by Konqueror's rendering. I was hoping it would improve more, especially now with Mac's Webcore. Bug #31417, where BBC News and CS Monitor sites have text going over other text and images is probably the most annoying, its one I see fairly often. I like browsing with Konqueror since it opens so quickly and its easier to open and save stuff (more integrated with environment) then Firebird.\n\nOne of my favorite new features in KDE is FSViewPart. I've found it to be quite useful."
    author: "Ian Monroe"
  - subject: "Re: That \"rather controversial article\" has a point"
    date: 2004-01-02
    body: "Some called this review professional, I have to disagree for the following reasons:\n\nHe supplies us with unimportant data like his hardware configuration. But forgets to mention the distribution he is using and how he installed this beta2 on this system. \n\nHis installation is seriously f***ed up, since simple things like the desktop hiding button, and pdf's do not work.\n\nHe hasn't got a clue what 'memory usage' means, but blames this 99% number on KDE.\n\nHe contradicts himself (too many buttons, but one should be added for smb-browsing, just because he browses smb-shares)\n\nHe didn't do any research (yes plastik is the default, and yes there is a kde front-end to mplayer).\n\nHe thinks everything is bloated, unintuitive and inconsistant, but he provides very little evidence.\n\nThis is not a review, this is a rant.\n \n "
    author: "Johan Veenstra"
  - subject: "Re: That \"rather controversial article\" has a point"
    date: 2004-01-02
    body: "> This is not a review\nOr a terrible review.\n\nIt makes you wonder what he's up to..."
    author: "K"
  - subject: "Re: That \"rather controversial article\" has a point"
    date: 2004-01-03
    body: "Yes, it is a terrible review.\n\nRead what he said about: NoAtun/Kaboodle.\n\nAFAIK they ARE a front end for Xine.  At least anything I installed for Xine worked for/with them.  Including QT movies with sound.  I thought that it was great except that building Xine from source wasn't a whole lot of fun. :-)\n\nI can only conclude that he dosen't understand that you have to install the libraries and/or Windws DLLs in Xine for the respective audio/video format to work.\n\nARGH!!\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: That \"rather controversial article\" has a poin"
    date: 2004-01-03
    body: "They are not a front end for Xine in the fact that they'll build and play some media files without Xine. However if you've got Xine there's a Xine pluging for arts which allows more types of media to be opened."
    author: "Chris Howells"
  - subject: "Re: That \"rather controversial article\" has a poin"
    date: 2004-01-03
    body: "Thank you for your technical correction.\n\nIIUC, if you install a library or Windows DLL for Xine then: NoAtun/Kaboodle will use it.\n\nIsn't that what I already said?  \n\nSo, just exactly what is your point -- you obviously didn't understand mine. \n\nPerhaps in your rush to find something wrong with what I said, you failed to read the \"rather controversial article\" which says:\n\n<<\nNoAtun/Kaboodle - These lack features. The interface is very clean, but that's because it can barely do anything. It wouldn't play most of my video files, that other players have no trouble with. This is unacceptable, as there is mplayer, and lots of KDE front ends already around. They should simply take one, stabilize it, and use that. Or if they desired, use a XINE frontend. Both would be much better.\n>>\n\nNow do you understand *my* point that the reviewer is dead wrong because KDE already HAS a frontend for XINE.  It is nonsense to say that this isn't the case because there are one or more additional layers of software envolved or that N/K also act as a frontend for other libararies.  If the reviewer had installed XINE and the necessary libraries and Windows DLLs then N/K would have been able to play the files which he complains about.\n\n--\nJRT\n"
    author: "James Richard Tyrer"
  - subject: "Re: That \"rather controversial article\" has a point"
    date: 2004-01-02
    body: "\"He didn't do any research (yes plastik is the default, and yes there is a kde front-end to mplayer).\"\n\nNo plastik is NOT the default.\n \n"
    author: "Random"
  - subject: "Re: That \"rather controversial article\" has a point"
    date: 2004-01-03
    body: "Oops, well I don't care about theme's anyway."
    author: "Johan Veenstra"
  - subject: "Re: That \"rather controversial article\" has a point"
    date: 2004-01-02
    body: "> And the biggest point really is that HIG mention, just try to install and\n> customize a dozen machines at the same time and you see why that makes\n> perfect sense.\n\nwhat exactly does a HIG have to do with customizing a number of machines at the same time?"
    author: "Aaron J. Seigo"
  - subject: "Re: That \"rather controversial article\" has a point"
    date: 2004-01-03
    body: "Obviously didn't mean only that, that just happen to be one of the accidental happenings what happen when all (specially desktop) config options are spreaded on multiple places... :-)"
    author: "Nobody"
  - subject: "Re: That \"rather controversial article\" has a point"
    date: 2004-01-02
    body: "I find it always remarkable that people manage to say that a reviewer has it \"wrong\". A reviewer usually expresses his opinion, and opinions can't be wrong. You can agree or disagree with an opinion though. \n\nTake for example his comment about memory usage. He is under the impression that KDE uses 99% of his memory probably because some tool (top? ksysguard?) reports this. Does KDE use an unreasonable amount of memory? Not really, but memory reporting tools under Linux are so crappy (or, depending on your viewpoint, the way in which Linux uses memory is so complicated) that meaningful numbers are impossible to collect by mere mortals.\n\nSomething else what a lot of people don't seem to get is that a reviewer writes reviews. A reviewer isn't usually part of your project and isn't writing reviews as part of an elaborate bug-report. It is a report about the state of affairs as the reviewer sees it. Compare it with these radar-speed-controls the police does on highways which, unlike popular believe among some, are not there to calibrate the speedometer in your car either. You should have had the right speed before you got there.\n"
    author: "Waldo Bastian"
  - subject: "Re: That \"rather controversial article\" has a point"
    date: 2004-01-02
    body: "Then this review has a remarkable amount of wrong facts expressed as opinion. Sorry, declaring everything as opinion doesn't make this guy less clueless."
    author: "Anonymous"
  - subject: "Re: That \"rather controversial article\" has a poin"
    date: 2004-01-05
    body: "IMHO, you can't blame a reviewer for not being a technical genius and getting some facts wrong. Every user opinion is important, whether its technically correct or not - except KDE should only be used by hackers. Someone very familiar with KDE (and Linux) will probably (unconciously) overlook the many minor (and not so minor) glitches that still trouble KDE and other projects."
    author: "daniel"
  - subject: "Re: That \"rather controversial article\" has a point"
    date: 2004-01-03
    body: "Recently I have seen many times on help forums people believing all their memory and even more was used because they used ksysguard without knowing what it displays. And for the beginner, there is no warning that ksysguard is not that easy to figure out."
    author: "djay"
  - subject: "Re: That \"rather controversial article\" has a point"
    date: 2004-01-03
    body: "That \"rather controversial article\" was written by a 16 year old child. For a kid, it's a decent review. But I wouldn't place too much stock in it. Use KDE 3.2 yourself and form your own opinion."
    author: "David Johnson"
  - subject: "gooeylinux"
    date: 2004-01-02
    body: "Well well well. Seems to me he's whining a bit too much.\n\nA lot of buttons and stuff that \"clutter\" the interface?\nI see this same comment averywere when you read about Linux and KDE.\nI think it's up to the reseller/distribution/whatever to\ndo the final twinkles, just look at Lycoris etc.\n\nThe basic KDE should show off all functionality."
    author: "OI"
  - subject: "Re: gooeylinux"
    date: 2004-01-02
    body: "> A lot of buttons and stuff that \"clutter\" the interface?\n> I see this same comment averywere when you read about Linux and KDE.\n\nYes, it seems to be a \"hip\" thing to say.\n\nThe fact that he requests SMB-settings to be moved to the default interface (cluttering up the interface even more) just shows that he's just parrotting whatever he hears and is in fact so clueless that he doesn't even realize how much he is contradicting himself. (Whoever runs a 2GHz machine with 256MB RAM is obviously clueless, I'm surprised that KDE seems to be still running quite fast on his machine, but that's another topic.)\n\nI want a desktop that I can use efficiently over years and that's exactly what KDE is. You can configure it (almost) exactly the way you want.\n\nI do not want some piece of junk that looks great in the first 30 minutes of usage but then starts to slow you down with useless animations. Like MacOSX for example. That any usability expert could find MacOSX good remains a mystery to me. In the release I tried (IIRC 10.1) there is no way to turn off minimizing animations (I could choose between two different kinds of animations which added a lot of irony to the whole matter) which drove me nuts after an hour of usage and is just one of many examples how completely and utterly useless MacOSX is for day-to-day work. Yes, the icons look great at first, but at the end of the day they are completely useless and don't do what icons are supposed to do: Differenciate applications on a small screen-area. KDE-classic icons are much more efficient. (= *much* smaller, yet still easier to tell apart from each other) Unfortunately the KDE-keramic icons are also drifting to \"consistency\" which is the same as \"useless\" when it comes to icons. (The job of icons is to look DIFFERENT, not to look similar. If all icons are blue, it might look great, but it becomes useless) Another bad MacOSX example is the pretty useless dock which takes up much too much screen estate and becomes useless as soon as you have several windows with the same icon (Text may not be as sexy as huge graphic icons, but at least it does the job). The lack of multiple desktops and real 3-button support just completes the picture.\n\nAnyway, I have the strong impression that the review-community is just a bunch of guys parroting \"general truths\" (like: \"If it's from Apple, it's the most usable thing ever created\". Take the horrible \"puck\"-mouse for example. When first released, nobody dared to critizise it, it's from Apple, the usability-gods. It took many years until the reviewers dared to critzise it. And their newer mice aren't much better either, it's ergonomy and usability sacrificed for good looks.).\n\nHowever, my point:\n\nIt doesn't matter what the KDE-team does. The \"general truth\" is that KDE is bloated, inconsistent and confusing. Reviewers will repeat that general truth regardless of KDE's actual look and feel, just like they will praise anything from Apple without any concern about the real usability.\n\nSo please don't take such reviewers seriously, especially when they contradict themselves within a single paragraph.\n"
    author: "Roland"
  - subject: "Re: gooeylinux"
    date: 2004-01-02
    body: "Some additional comment:\n\nTake the \"KDE is like Windows\"-myth for example. For years it was repeated that KDE is just \"like Windows\" and therefore evil and that real men would use GNOME.\n\nAfter GNOME has started to dumb down everything and copy some horrible Windows-features (like the Registry -eeech), all of the sudden KDE is no longer \"like Windows\" any more. Now all of the sudden it's too complicated and confusing and GNOME is the better Windows.\n\nDid KDE really change in that regard? Not really, it always had loads of options, yet the public image turned 180\u00b0 WITHOUT KDE ITSELF CHANGING!!\n\nThere is one area in which KDE is really lacking and that is marketing. \n\nApple has people creating the public image of great usability.\nMicrosoft has people creating the public image of universability, compatibility and low TCO.\nGNOME has people creating the public image of easyness.\n\nKDE has nothing comparable and that's why the public image of KDE is driven by the marketing guys of other organizations.\n\nAgain: IT DOES NOT MATTER wether KDE follows usabilty-guidelines. It's irrelevant in influencing the public image. What DOES matter is that such guidelines and some usability team exist and that somebody supplies a constant flow of press-releases about KDE's usability and how great it all is. After some time of constant bombardment with usability press-releases, reviewers would start parroting it and a new image is born.\n\n\n\n"
    author: "Roland"
  - subject: "Re: gooeylinux"
    date: 2004-01-02
    body: "> copy some horrible Windows-features (like the Registry -eeech)\n\nWhat's wrong with the Registry/gconf? The only argument I've ever heard is that the Registry can be irreversibly corrupted, which, while valid, doesn't apply to gconf because it uses XML instead of a binary format."
    author: "damiam"
  - subject: "Re: gooeylinux"
    date: 2004-01-02
    body: "> What's wrong with the Registry/gconf?\n\nLet's see:\n\n1) It complicates everything by introducing several completely inconsistent ways to do configuration.\n\n2) It prohibits easy upgrading. I still use ~/.alias, ~/.profile  and many other files back from 1997, but I wouldn't want to try to import a Windows98 registry on WindowsXP, I wouldn't even dare to do it between servicepacks. In general sharing configuration among Windows machines is pretty much unheard of (it's either a complete diskimage or nothing) while it's very common in the Unix world.\n\n3) It's not commented and it doesn't support comments. Just by reading /etc/httpd/httpd.conf you get a pretty good idea and should be able to do a usual and ordinary configuration without too much problems. You can easily go back to the things you had in place before by *commenting out*. It may be a trivial and \"unworthy\" reason, but the \"<!--\" style comments make XML/HTML a pain to use when you need comments.\n\n4) I can post relevant parts of it on newsgroups, etc. Because of the above points, people willing to help can help. Posting a binary file on newsgroups is useless for obvious reasons and posting XML isn't much better.\n\n"
    author: "Roland"
  - subject: "Re: gooeylinux"
    date: 2004-01-02
    body: "In reference to (1) and (4). GConf uses XML, not binary, so its nothing like the registry. Its no different than a tree of text files in /etc, except its all in the same format and has a standard API to read/write it. And posting XML to newsgroups works fine, since browsers usually do not try to interpret it. "
    author: "Rayiner Hashem"
  - subject: "Re: gooeylinux"
    date: 2004-01-02
    body: "XML can still contain binary parts (dunno if GConf supports that atm though), and is far less readable than the .ini inspired clear text style KDE uses."
    author: "Datschge"
  - subject: "Re: gooeylinux"
    date: 2004-01-02
    body: "GConf supports aribtrary back-ends. They use XML simply because they didn't want to invent yet another format. Using XML allows for the storage of very complex configurations, and allows people to use widely available, standard XML tools to manipulate them.\n\nAnd yes, XML supports binary parts, but in theory so does .ini. Just uuencode some binary data and stick it in the file as a string. In some special cases, you might want to do just something like this. There would be a problem if gconf used binary data on a regular basis, but it doesn't.\n"
    author: "Rayiner Hashem"
  - subject: "Re: gooeylinux"
    date: 2004-01-03
    body: "1) No, gconf provides one way to do configuration. ~/.blah files provide inconsistant ways to do configuration.\n\n2) Part of the gconf policy is that apps should always be backwards compatible with gconf settings of previous versions. I don't know how well that's followed, but it's not any worse than the format changes in ~/.blah files. As for sharing configuration, you can access the settings on one gconfd server from any number of network clients.\n\n3) Yes, it does support comments. gconf has a schema for each key describing the key. Open gconf-editor and select a key, and a description appears. No, you can't comment stuff out, but you can revert keys to their default settings.\n\n4) So, instead of telling the mailing list \"my ~/.blah file says '[foo]=bar'\", you can just say \"foo is set to bar\". It's just a difference in semantics. It may not be quite suitable for complexly-configured apps like Apache (although 99% of Apache \"help\" requests are configuration problems caused by it's own unique config file format), but it's quite usable for everything one would want to do in a GUI desktop environment.\n\nAnother benefit of gconf is that it makes it much easier to write apps. You don't have to invent your own format and code your own parser, everything is done for you. You can also do stuff like to register to be notified of changes to a certain key, so your app can react instantly to an external configuration change."
    author: "damiam"
  - subject: "Re: gooeylinux"
    date: 2004-01-03
    body: "\"Did KDE really change in that regard? Not really, it always had loads of options, yet the public image turned 180\u00b0 WITHOUT KDE ITSELF CHANGING!!\"\n\nYes, but others have changed. KDE 1 was a breakthrough for Unix usability and KDE 2 was an awesome upgrade. But since then, it has mostly been configurability and features plugged on top of it, while others didn't stand still.\nYou are lying to yourself if you really think, that everyone who doesn't enjoy using KDE anymore would just be following a trend or beeing manipulated by PR."
    author: "Spark"
  - subject: "Re: gooeylinux"
    date: 2004-01-03
    body: "As the KDE project gets seemingly more and more complex people from outside who don't care to inform themselves beforehand just seem to become more and more misinformed but still state being capable of judging everything from the top to the bottom. And KDE is certainly not the only project which \"suffers\" on that."
    author: "Datschge"
  - subject: "Re: gooeylinux"
    date: 2004-01-03
    body: "\"Did KDE really change in that regard? Not really, it always had loads of options, yet the public image turned 180\u00b0 WITHOUT KDE ITSELF CHANGING!!\"\n\nHaha! Thanks for bringing this salient point to the forefront of my consciousness! Yesterday KDE was bad because it was too much like Windows. Today it's bad because it isn't enough like Windows. It reminds me of the Qt license issue. No matter how free Trolltech made Qt, people bitched that it wasn't free enough. I've come to the conclusion that a sizable and vocal segment of the community hates KDE for the mere sake of hating KDE."
    author: "David Johnson"
  - subject: "Re: gooeylinux"
    date: 2004-01-02
    body: "Exactly. It seems like most reviews of desktop environments are written after a quick look round - click a few buttons, check out some apps and snazzy effects, then write the review.\n\nSaying things like \"it takes up 99% of my memory\" shows the gooeylinux guy doesn't really know what he's talking about. He does have a few points, though. Konqueror's web page rendering is pretty bad, the default sounds are terrible  and 'consistency' of icon design is not such a good idea if it means it's harder to differentiate icons."
    author: "Ben Roe"
  - subject: "Re: gooeylinux"
    date: 2004-01-02
    body: "> Konqueror's web page rendering is pretty bad\n\nYes, I also still need Mozilla from time to time, but in general it's pretty OK for me.\n\nBut I still remember the times I used KFM ;-)\n\nActually the feature KFM and Konqueror have and no other browser (I have tried) has is that it reopens all browser windows on login. On the right desktops with the right geometry and now also with the right tabs. No other browser can do that. (Opera can restore *one* browser window AFAIK and it is completely unaware of multiple desktops) So with Konqueror you no longer need temporary bookmarks, for example on a web-forum you can just leave the current page open and continue at the next day.\n\nNow what is really needed is a press-release selling this feature as great and completely new feature in KDE 3.2. Nobody will care that it already was in KDE1.0...\n\nThis is the feature which made me use KFM...\n\n> the default sounds are terrible\n\nYes, I can agree 100% on that. However you rarely read about that in reviews - it really seems that reviewers are somehow ashamed to write something new or original. I really wouldn't mind hearing constructive critizism but this mindless parroting just makes me sick.\n\n"
    author: "Roland"
  - subject: "Re: gooeylinux"
    date: 2004-01-02
    body: "Oh my God. Those Mac one-button mouses. Oh dear."
    author: "David"
  - subject: "Re: gooeylinux"
    date: 2004-01-02
    body: "I think you are unfairly dismissing an entire body of claims just because a lot of the people making them are not terribly informed. I am a full-time KDE user since 2.x, and I agree to a degree with some of the things that people are saying. I don't believe that KDE is unusable because of its UI complexity, but I definately believe its hurting my efficiency.\n\nThe overall problem with KDE is that there are just too many extraneous buttons and menu items. Its not just confusing, it hurts efficiency in the general case, and is unasthetic. Let me give you a rundown of the default 3.1.4 Konqueror:\n\n- The 'up' button really does not make a whole lot of sense in the context of web-pages. \n- \"Print\" and \"Print Frame\" are inherently slow operations. Saving a fraction of a second by putting it in the toolbar instead of making people go to the main menu isn't really an advantage.\n- Same thing with the 'Find' icon. Find is a process that requires going to the keyboard and typing in additional data. Thus, the time saved having it in the menu is very minimal compared to the overall time taken by the operation. Plus, since your hands need to be on the keyboard anyway, CTRL-F is faster. \n- The security action doesn't even make sense. Its not really a command, but a way to get info about the crypto status of the page. It should be an indicator in the status bar, not an icon. \n- Increase/Decrease font sizes is probably not used enough to warrent being on the main toolbar. \n\nSo the complaints *are* often legitimate, just poorly explained. But it seems clear to me that it is these sorts of simplifications that many of KDE's detractors are looking for. I've posted screenshots of my modified Konqueror on OSNews, for example, and people generally reacted quite favorably.\n\nhttp://www.prism.gatech.edu/~gtg990h/\n\nI think \"reviewers\" should go into this sort of detail about exactly what about KDE they find cumbersome, but in my experience, that's a sure way to get certain KDE folks up in arms about how they *absolutely need* that feature. \n\nI'm of the following mindset. Apple/Microsoft/Sun's HCI research shows simpler, more streamlined interface is more favored by most users. Now, you can debate about the degree here, about how simple is too simple, and how simple is simple-enough, but its kind of hard to argue the basic conclusion. Emprically, some of the UIs widely hailed as the best ever (MacOS Classic, OS X, NeXTStep, Amiga) were on the simpler end of the scale. Thus, I think it is benificial to have a more simple UI be the stock configuration. It doesn't have to be ascetically minimal like GNOME. Certainly no features need to be removed or even hidden --- things just need to be migrated from the toolbars and context menus to the main menu. You'd be surprised how minimal changes in the *visible* interface can give people a totally different impression of the UI. And since KDE is supremely configurable, people could always add any oft-used shortcuts back to their configuration. \n\nOne key problem here is that users who like a UI with lots of buttons and menu items could have to go to a lot of trouble to add all the things they want back to the stock configuration. Unlike other UIs, I think KDE's technology can result in a pretty nice solution for people of both tastes. From what I've seen, XML-GUI could probably be used to allow people to save their toolbar/menu changes, and even share them with others. If people could trade UI \"themes\" like they trade KDE Styles, then the \"minimal or maximal\" debate could become moot for most KDE users, just like the default style debate is today. \n\nI've been playing a bit with XML-GUI to do just that, and it seems entirely doable. Wrap things up in a nice GUI (along with a menu editor to go along with the toolbar editor) and the thing could be quite usable."
    author: "Rayiner Hashem"
  - subject: "Re: gooeylinux"
    date: 2004-01-02
    body: "I agree, the exchange or at least offering of different levels of amount of toolbar entries is already fully doable and should be possible to be include as a eg. \"GNOME\" choice in kpersonalizer after KDE 3.2."
    author: "Datschge"
  - subject: "Re: gooeylinux"
    date: 2004-01-02
    body: "> - The 'up' button really does not make a whole lot of sense in the context of web-pages.\n\nI use it all the time to get to the homepage. It's works always, is consistent (= works on EVERY page) and fast (when you are scrolled down somewhere on a page there is no link to the homepage in sight. Try to click the button and then move the mouse down, the menu pops up instantly (no delay) and you can move to any subdirectory.\n\n> So the complaints *are* often legitimate, just poorly explained.\n\nNo, I think these complaints are pretty clear: Some people think that *all* users should be forced to use their personal preferences.\n\n> - Same thing with the 'Find' icon. Find is a process that requires going to the keyboard and typing in additional data. Thus, the time saved having it in the menu is very minimal compared to the overall time taken by the operation. Plus, since your hands need to be on the keyboard anyway, CTRL-F is faster.\n> - The security action doesn't even make sense. Its not really a command, but a way to get info about the crypto status of the page. It should be an indicator in the status bar, not an icon.\n\nI agree here.\n\n> - Increase/Decrease font sizes is probably not used enough to warrent being on the main toolbar. \n\nI use it all the time.\n\n> I've posted screenshots of my modified Konqueror on OSNews, for example, and people generally reacted quite favorably.\n\nIsn't the fact that you could do all this without touching a single line of code proof enough for the superiority of being able to configure almost everything?\n\nWhat is better? To show the user as many options as possible/reasonable and let him remove those he doesn't like (like you did) or remove everything except the bare minimum and let users add their options themselves? Your browser may look nice, but without bookmark-bar and menubar (how are users supposed to handle bookmarks? Or do you consider bookmarks redundant too?) I doubt it would be popular among users because adding everything that even Netscape 3.0 could do (ehrm, like bookmarks) wouldn't be so easy. Many users would dismiss Konqueror because they would think it can't handle bookmarks.\n\nIn general I'd say that it's easier to remove some unwanted toolbar buttons than it is to try all buttons yourself and add those you really need. You can remove icons in less than 5 minutes, but looking for features/icons and putting them in the right place takes a much, much longer time.\n\n> I think \"reviewers\" should go into this sort of detail about exactly what about KDE they find cumbersome, but in my experience, that's a sure way to get certain KDE folks up in arms about how they *absolutely need* that feature.\n\nYou seem to *absolutely need* the feature to remove the menubar, a feature I never use. But I realize that it's just my personal preference and even though it's not me who would be hurt when that feature goes, I would argue for keeping it.\n\n> Certainly no features need to be removed or even hidden --- things just need to be migrated from the toolbars and context menus to the main menu.\n\nI could certainly live with that. But I would have doubts that a lot  of features become pretty much unused and unknown.\nIf KPersonalizer would offer an \"streamlined\" versus \"fully-featured\" slider, I think that would be a very good solution.\n\n\n\n"
    author: "Roland"
  - subject: "Re: gooeylinux"
    date: 2004-01-02
    body: "> > - The 'up' button really does not make a whole lot of sense in the context of web-pages.\n> I use it all the time to get to the homepage. It's works always, is consistent (= works on EVERY page) \n\nYou must be living in the web of 1997, because in the web of today, the 'up' button works in less than half of most web pages, and even less in most popular pages, which are oft-dynamicly generated and have weird physical layouts.\n\nWeb2004 != Heirarchical"
    author: "fault"
  - subject: "Re: gooeylinux"
    date: 2004-01-03
    body: "Personally I still use it very frequently, even if it's only a mean to access the actual server URL without having to use the keyboard or cumbersomely selecting the URL part I want to toss away for then selecting \"cut\" from right click menu and pressing the \"enter\" button (yet another thing everyone seems to be eager to toss away).\n\nThe only way I see solving this is extending the already existing possibility of offering different schemes of predefined \"defaults\" in kpersonalizer (which runs after the first login) respectively from which the user can choose what he prefers."
    author: "Datschge"
  - subject: "Re: gooeylinux"
    date: 2004-01-03
    body: "Don't make the mistake of thinking Konqueror is a web browswer. It is not. Instead it is a network transparent universal browser. The up arrow is EXTREMELY useful for browsing the local filesystem and ftp sites, to give just two examples. It would be a usability mistake to make the up arrow disappear everytime you hit http...\n\nThe review complained that Konqueror didn't have any outstanding features. Well the \"universal browser\" is something most web browsers don't have. Many people have bragged this feature up big time, but for some reason the reviewers of the world just aren't listening."
    author: "David Johnson"
  - subject: "Re: gooeylinux"
    date: 2004-01-03
    body: "I must be too, because I use the up arrow all the time.  It's just so damn useful.  Much of the time I use it to find stuff on a web site that isn't quite as organized as I would wish.  I use it for exploring.  For example, if I'm looking at a path like this:   ../foo/i686/stuff, I might use the up arrow to try to see if there's an ../foo/i586/stuff directory."
    author: "TomL"
  - subject: "Re: gooeylinux"
    date: 2004-01-03
    body: "Please show me a common webpage in which I can't go \"up\" to the homepage.\n\nFor dot.kde.org it works, I can use the \"up\" button from anywhere on the site to go back to http://dot.kde.org\n\nIn some very rare cases in which sites use different subdomains it doesn't work (most notably Slashdot) but those are exceptions, not the rule.\n"
    author: "Roland"
  - subject: "Re: gooeylinux"
    date: 2004-01-02
    body: "No, I think these complaints are pretty clear: Some people think that *all* users should be forced to use their personal preferences.\n---------\nA default setup implies that somebody is going to not like elements of the configuration. There is no way around that. You like having icons right at hand. I like them tucked away, because I prefer to use keyboard shortcuts. Currently, KDE is set up to please you and displease me. If KDE UI were more streamlined, it would displease you and please me. The question is to make the UI such that it displeases the fewest number of people. I would argue that KDE's current UI is not set up that way. \n\nIsn't the fact that you could do all this without touching a single line of code proof enough for the superiority of being able to configure almost everything?\n--------\nI'm not against leaving in the configurability. I'm fully support leaving in configurability, even if that brings some complexity. I think that when the GNOME people say that KDE is to configurable, they're full of crap. What I'm against, however, is having the defaults be complex, when HCI research shows that most users like things to be simpler. \n \nI could certainly live with that. But I would have doubts that a lot of features become pretty much unused and unknown. \n---------\nThere should be a better way of showing people what features are available. If KActions supported comments about what what they do, then maybe we could do this by showing a comprehensive list of KActions in an application and the associated comments. If people don't want to even take the time to look at that list, than its their productivity loss. \n\nIf KPersonalizer would offer an \"streamlined\" versus \"fully-featured\" slider, I think that would be a very good solution.\n---------\nI think that may be the only way to ever streamline KDE's UI. As your agreement with some of my points about Konqueror's toolbar show, there is probably some low-hanging fruit still left. IE: stuff we could remove while pissing off only a few users. Beyond that, if KDE is ever to be friendly to those who prefer a streamlined UI and still want the power of KDE, then we'll have to find some way to allow people to have that streamlined interface without pissing off existing KDE users. \n \n \n \n \n "
    author: "Rayiner Hashem"
  - subject: "Re: gooeylinux"
    date: 2004-01-03
    body: "> If KActions supported comments about what what they do, then maybe we\n> could do this by showing a comprehensive list of KActions in an application\n> and the associated comments.\n\nThey do support this. Look at the setWhatsThis() method. The trouble is that the configuration dialog isn't very good and amongst many problems it doesn't use this information.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: gooeylinux"
    date: 2004-01-03
    body: "> I like them tucked away, because I prefer to use keyboard shortcuts.\n\nBut would a setup like that really be perfect for all users? If you use keyboard shortcuts, you obviously use the application for quite some time and know exactly what features you really use and what you don't. A beginning user first doesn't know the keyboard shortcuts by heart and second still has to decide which toolbar icons to throw away and which to keep.\n\n> Currently, KDE is set up to please you and displease me.\n\nBelieve me, I also have changed Konqueror a lot.\n\n> The question is to make the UI such that it displeases the fewest number of people.\n\nIn general, yes. However there are some things which have to be taken into account:\n\n- The defaults are used more by beginners\n- Buttons are more easily removed than added\n- You have to show off features to get them to be used\n- Only more features get you new users, I've never heard anybody recommend a program because it can't do something.\n\nI personally don't think that Konqueror is overdoing that altough there is probably always room for improvement. However I think that a beginner would rather like to throw away icons he doesn't use than test through all available icons and add them later.\nAs an example let's take the \"up\" arrow. Konqueror is the only browser (I know) that is featuring that and I (and as it seems a lot of other people as seen in another subthread) like it a lot. If that arrow wasn't there in the defaults, I would have taken a lot more time to find out about it's usability. \nAnd in your example of Konqueror you left out features which are used by almost all users, for examples bookmarks or the \"downloading/busy\" animation.\n\n> I would argue that KDE's current UI is not set up that way.\n\nWell, that's a claim without any proof.\n\n> I'm fully support leaving in configurability, even if that brings some complexity. I think that when the GNOME people say that KDE is to configurable, they're full of crap.\n\nFinally something we can agree on :-)\n\n> What I'm against, however, is having the defaults be complex, when HCI research shows that most users like things to be simpler.\n\nIf that were true, nobody would use MS Office.\n\nIn the small constrained world of HCI-research with their preplanned and simplified actions and tasks, simple is probably the best.\n\nHowever in the *real* world with *real* users doing *real* work, which is completely unplanned, random and complex, users are always asking for more and more features. For example almost everyone I've shown browser tabs to has liked them, even people who aren't computer cracks, but browser tabs are much more complex than the simple one site = one window layout we had before.\n\nAnd it all depends on what you test in HCI research. I've read a pretty detailed report on one of those tests and in that case (and I assume in most studies it's similar) every subject was testing the system for about half an hour.\n\nSorry to bring it up again, but for half an hour straight, your Konqueror would probably be the best in HCI-testing and would beat almost any other configuration. But in the real world it would be declared completely useless within a week as soon as the user wants to use bookmarks.\n\n"
    author: "Roland"
  - subject: "Re: gooeylinux"
    date: 2004-01-03
    body: " But would a setup like that really be perfect for all users?\n----------\nIt wouldn't be perfect for a lot of users. But the HCI research shows that people only use a fraction of the capabilities of a program, and that is what people like Apple make visible by default. I don't know if this is optimal, but the UIs that really get praised for their usability (NeXT, MacOS, Amiga, etc) are like this, so there must be something to it. \n \nBelieve me, I also have changed Konqueror a lot.\n--------\nDoes anybody like the default Konqueror setup?\n \nThe defaults are used more by beginners\n---------\nAnd beginners tend to only use a fraction of the features of a program. That argues for having the default UI only present the most commonly used features, keeping other features in the main menu.\n\nButtons are more easily removed than added\n---------\nNot really. It is equally difficult to do both. \n\nYou have to show off features to get them to be used\n---------\nThere has got to be a better way of showing off features than cluttering the interface with ones that users will most likely not use.\n\nOnly more features get you new users, I've never heard anybody recommend a program because it can't do something.\n----------\nThis is completelly wrong. The thing that draws millions of Mac users to the Apple platform is the elegance of the UI. And nobody is suggesting that we get rid of features. I'm simply suggesting that we not put every possible feature in the toolbar and context menu. Microsoft goes to a lot of trouble every year streamlining its UI. Take a look at the Longhorn IE screenshots and compare them to Konqueror. \n\nHowever I think that a beginner would rather like to throw away icons he doesn't use than test through all available icons and add them later.\n-----------\nActually, HCI research suggests that users prefer to get a hang of the basics of a program first, and then be gradually exposed to more advanced features, not the other way around. If a user wants to become more skilled at using a program, t hey will seek out new features. What we should do is make it easy and quick for them to find these, rather than burdening them with all of them at once.\n  \nWell, that's a claim without any proof.\n--------\nWell you don't have any proof that KDE UI is set up to displease the fewest users. I've at least got HCI research that suggests users prefer simpler/more streamlined UIs, and unless you are willing to argue that KDE is a simpler UI, than I think my position is more compelling.\n \nIf that were true, nobody would use MS Office.\n--------\nI don't know anybody who likes the MS Office UI. I do know lots of people who raved about ClarisWorks or WordPerfect, however. People use Office because they have to, and because their job gives them MS Office workshops. Microsoft is *not* the people we want to get HCI cues from.\n \nFor example almost everyone I've shown browser tabs to has liked them, even people who aren't computer cracks, but browser tabs are much more complex than the simple one site = one window layout we had before.\n----------\nIts a cost/benefit analysis. Sometimes, the benefit of a feature like tabs is enough to outweigh its costs. I'd argue that the benefit of putting \"Increase/Decrease Font Sizes\" in the toolbar instead of the main menu is not enough to outweight its cost. \n \nBut in the real world it would be declared completely useless within a week as soon as the user wants to use bookmarks.\n----------\nFirst, I was only analyzing the default toolbar, not Konqueror as a whole. At least in 3.1.5, from which I'm typing this, there is nothing related to bookmarks in the default toolbar. Bookmarks is in the main menu, where it should be. \n \n "
    author: "Rayiner Hashem"
  - subject: "Re: gooeylinux"
    date: 2004-01-03
    body: "It wouldn't be perfect for a lot of users. But the HCI research shows that people only use a fraction of the capabilities of a program, and that is what people like Apple make visible by default. \n---\nYou are spreading BS by omitting the fact that while 90% of the users use only 10% of all features, every single person has an own range of 10% so in the end there is still the problem what default to choose. KDE stays away from that and instead let you decide yourself (or choose a distribution which did so for you).\n\nYour steady reference to HCI researches is nice and dandy, how about contributing actual usability studies about KDE to the KDE developers instead so they actually change something in contrary to this needless discussion."
    author: "Datschge"
  - subject: "Re: gooeylinux"
    date: 2004-01-03
    body: "> You are spreading BS by omitting the fact that while 90% of the users use only 10% of all features, every single person has an own range of 10% so in the end there is still the problem what default to choose. KDE stays away from that and instead let you decide yourself (or choose a distribution which did so for you)\n\nYes.. however, I think that this policy is wrong, since it seems like catering to *everyone* in that 10%, and thus not satisfying barely anyone *by default*. The first time I run KDE, I have to mess with quite a few settings (like toolbars) to make sure that 90% of the program is efficient.\n\nGNOME doesn't seem to have this problem. It has other problems of course, which makes me pick KDE as the best desktop overall."
    author: "fault"
  - subject: "Re: gooeylinux"
    date: 2004-01-04
    body: "Yo man, if a project doesn't want to patronize its own contributors with the source distribution (the only thing KDE offers) then it actually need to cater everyone, otherwise it can quickly lose a considerable number of them. Binary distribution is not in the hands of KDE, and every distribution is free to choose the defaults it thinks is best for its customers, and the customer is free to choose the distribution which offers the best defaults. What wrong with that (except that many distributions don't take the chance to differentiate themselves from others, and KDE gets the blame)?\n\nGNOME chose to patronize its own contributors and alienated not few of them on the way. Now it seems to have mainly contributors which are happy with the default, everyone else is bound to look for something else after some time (I think eg. the rise of popularity of XFce DE is quite telling here)."
    author: "Datschge"
  - subject: "Fintsize"
    date: 2004-01-03
    body: "> - Increase/Decrease font sizes is probably not used enough to warrent being on the main toolbar. \n\nThis feature is EXTREMELY handy. A lot of people loves it,\ncompare it to Mozilla or Explorer, where people sit with\ntheir eyes 1 inch from the screen just to be able to read.\nI hope nobody ever takes it away.\n\n"
    author: "OK"
  - subject: "Re: Fintsize"
    date: 2004-01-03
    body: "Nobody is suggesting that we get rid of the feature. But does it really need to be in the default toolbar? It takes one extra click to activate it from the main menu instead of the toolbar. Is that too much of an extra burden?"
    author: "Rayiner Hashem"
  - subject: "My particular gripes"
    date: 2004-01-04
    body: "Also most other browsers have some kind of \"zoom\" widget (a dropdown box with predefined values or a spinbutton), which works just as well and is easy to add and remove. The font buttons in Konqueror are particulary annoying if you try to use \"text below icons\" or \"text alongside icons\", as the labels are WAY too long[1]. The buttons are also very difficult to remove because first you have to understand that they aren't on \"Main Toolbar\", but on \"Main Toolbar <khtml>\" (one of the thirteen offered toolbars to edit). Ugh. It might sound silly, but up until yesterday, I didn't even know that you could remove them.\nFinding actually useful items to add to the toolbar is a pain, because all actions are treated similar. And don't even get me started on those \"merge\" items which you can remove but not add again (thankyouverymuch). For example finding the \"new tab\" action took me a while and then it even showed up as a horribly pixelated item, because it obviously wasn't meant to be used as a large button. I could find only a single theme where this buttons looks somewhat reasonable (slick icons).\nThose are some of the issues which I face when I try to get comfortable with Konqueror and that's really just the tip of the iceberg. I have used Konqueror in and out after KDE 2.x and also used the KDE 3.x versions quite a bit (mostly just trying it out though), still I don't feel in control of it. It feels more like taming than using. I'm currently trying the latest 3.2 beta version and it really hasn't changed at all.\nI know that I could fix some of those issues myself or at least write bugreports, but the number of problems which all seem to come from the simple fact that the KDE interfaces are more generic than anything else is so overwhelming, that I just don't see the point of it. I don't really believe that the situation can really be improved without either ten times the resources or a major change of mind.\n\nI hope this didn't sound to much like a rant, as I really just tried to bring across my point as a user and developer, why I prefer simple and \"designed\" interfaces, to \"generic\" interfaces. It's not like I'm completely against customization, but Epiphany for example let's me practically customize it more (and much faster) than Konqueror, at least in those areas which matter for a web browser. And when I'm done with Konqueror, I can start doing the same with every other single application, let alone the other \"modes\" of Konqueror... It's just no joy.\nOTOH, I can see that there is a market for such \"generic\" interfaces (I really think that this word fits KDE pretty well) as some people obviously enjoy customizing their applications, so it's probably a good thing that KDE (or rather GNOME) differs so much. I don't think however, that you'll be able to make the interface more enjoyable for people like me just by omitting most buttons by default. What I (and other \"less is more\" people) want isn't an interface that is just stripped down and lacks any controls, but rather interfaces which are well designed to be most efficient for the specific tasks they are trying to support (yes, this requires that the application isn't meant to be a kitchen sink in the first place).\n\nAgain, I hope this all didn't sound to harsh. :) I just like that you actually care for the issues of other people (with KDE) instead of shooting them down, so I wanted to give you some more insight on some of my particular issues.\n\nAlso I _do_ think that KDE is indeed not much worse than Windows for example (in terms of usability) and it hasn't gotten worse over time. It's just that my expectations have changed because of new and cleaner interfaces like GNOME 2 or OS X. KDE hasn't really changed since a long time and no interface will feel modern forever.\n\n\n[1] This problem isn't exclusive to the font size buttons of course. All buttons suffer from using the tooltips as label, thus making text below icons really really ugly. Text alongside icons is even more useless, more useful would be a \"priority text\" option as in GNOME and Windows, which would only show labels besides commonly used buttons (for example the back button of a browser)."
    author: "Spark"
  - subject: "Re: Fintsize"
    date: 2004-01-05
    body: "Mozilla has had this for ages (press <Ctrl>-<+> or Ctrl-<->). Although sometimes I'd also like to have buttons for it."
    author: "daniel"
  - subject: "Re: gooeylinux"
    date: 2004-01-04
    body: ">The 'up' button really does not make a whole lot of sense in the context of web-pages.\n\nIt does actually, it provides me a good way to go back to the main page when i'm completely lost in a complex site.\nIt also brings me to the index-page of a certain site when I got there via a link. I'm a very curious guy, and the [up] button provides me an easy way to navigate through sites without the hassle of using the controls provided by the site itself.\n"
    author: "rinse"
  - subject: "Re: gooeylinux"
    date: 2004-01-04
    body: "For example, it provides an easy way to go back to the discussion after posting a comment in the dot. Just go up a few stages using the [up] button and i'm back in the mean thread :)\n\nRinse"
    author: "rinse"
  - subject: "Re: gooeylinux"
    date: 2004-01-02
    body: "Although I like KDE very much, I think Konquerors menus and context menus are still overloaded in CVS.\n\nAlso kcontrol needs much more cleaning up: Try to set up for example the MacOS like menubar at the top *as a child panel*. You have to make the changes in (at least two, I'm at work so I can't check in the moment) different dialogs and in the right order. Why do we have at all two different ways of getting the menu at the top of the screen? Why can I set the Fonts for the Desktop at different places (Configure Fonts and Desktop Settings)?\n\nKDE is very mature and my favourite DE, but it needs more cleanup.\n\nfurank "
    author: "furanku"
  - subject: "Re: gooeylinux"
    date: 2004-01-02
    body: "> Although I like KDE very much, I think Konquerors menus and \n> context menus are still overloaded in CVS.\n\nIt always leads to a lot of discussion when a certain \noption is to be removed.  Personal preferences and \n*requirements for daily tasks* differ from person to person. \n\nThe toolbar editor makes it really easy to add or remove options\nfrom the toolbars. \nI'd appreciate a GUI for stripping down the *context menu*. \nI guess the usual suspects would complain again about \nover-configurability but I think that would really be neat. \n\nFor example: I for one *never* use any of the following entries \nin the context menu: back, forward, reload, copy text, \nselect all, bookmark page and some options in the \"actions\" \nmenu.  Either I don't use the functionality, or I prefer \nusing keyboard shortcuts or the icons in the toolbar. \n\nI guess a lot of people would cry out if any of the entries \nwere removed completely.  But why not make it easy for me \nto add or remove them in my personal config? \n\nI think if such an option were available a menu that's \ntrimmed down to the bare minimum would be acceptable \nto the power users (I'm sure those would know how to \n*add* the more esoteric options to the menu using the new GUI). \nIt should be possible to lock that possibility down with \nthe kiosk framework, of course...\n\n\n"
    author: "cm"
  - subject: "Re: gooeylinux"
    date: 2004-01-02
    body: "And there's another thing that might be improved: \n\nIt would be good to have *verbose* explanations for \nthe actions / icons that can be assigned in the \ntoolbar and keyboard shortcut editors\n(for example as tooltip on the action name / icon?).\n\nOne example from KHTML:  \nWhat does the action \"Manuell\" mean (German locale)?\nTwo from KMail: \nWhat does \"Inline\" or \"Intelligent\" mean?\n\nIf those actions appear in their own context \n(as option in a certain dropdown menu, for example)\nthe user can make sense of it, but outside of the \ncontext it's sometimes difficult. \n\n"
    author: "cm"
  - subject: "Re: gooeylinux"
    date: 2004-01-04
    body: ">One example from KHTML: \n>What does the action \"Manuell\" mean (German locale)?\n\nIf a german string is unclear to you, please inform the German team about it.\n\n\n>Two from KMail: \n>What does \"Inline\" or \"Intelligent\" mean?\nboth strings are in the sub menu \"attachement\" in the \"view menu\"\nThe determine the way attachements are displayed. Inline means that the attachement is shown in the mail itself, intelligent means that kmail decides how to handle the attachement.\n"
    author: "rinse"
  - subject: "Re: gooeylinux"
    date: 2004-01-04
    body: "Thanks for your answer, but: \nI know what they mean (at least for the three examples I've given).  \nI know because I've seen them before in the context of their menues. \n\nI just wanted to point out that the user finds it hard or even impossible to determine their meaning if they're presented *out of context* in the shortcut editor or the toolbar editor.  There is no way to get an additional description of the action. \n\nIMHO it's nothing that the translators can solve.  It's the same string that's used in the menu and in the shortcut/toolbar editor (the \"name\" of the action). Making the action *and* the context clear in a single phrase would result in monster menu entries that would even be redundant: \nLet's take the example \"Intelligent\": \nCurrently it's \nView -> Attachments -> Intelligent\n\nIt would become something like \nView -> Attachments -> View Attachments Intelligently\nOnly this way would the action be clear in the shortcut editor where all you have is the action name.\n\nThis kind of long menu entries is nothing I would want.  So I'm for adding a verbose description that can be displayed where the developer thinks it's useful (IMHO in the toolbar and shortcut editors).  \n\n"
    author: "cm"
  - subject: "Re: gooeylinux"
    date: 2004-01-02
    body: "Hi!\n\nTo make it easier to build your personal Desktop KDE should something have like a program registration. If you build KDE from the source you have all applications installed by default. Ok, you can modify the Makefile but...\n\nIt would be nice to have a minimal set of default apps and a big pool of installable programs. I (personally) do not use programs like kreate, cdbakeoven, ShowImg, Gwenview... and they i.e. expand Konqis action-menu or register unwanted mimetype relationships. I hope as part of the debian desktop alliance we will see something like this - but this is a task of an integrated desktop and _not_ distribution specific work.\n\nTalking about KDE control center. Is not easy or not possible to make everyone happy with such a \"beast\" ;-) Maybe the whole problem is the tree-gui. Maybe the find-view is better for the unexperienced user and leave the structured treeview for the pro(?) If you make the treeview vertically smaller and horizontally deeper e.g. via tap-dialogs this does not make it easier. Hiding and using a \"more...\"-button is often better to use, like the kde-printing-window (btw... why is there the choose-printing-system-part not part of the hidden setup??)\n\nUsing KDE-CVS as my daily system on my \"play-machine\" I think KDE-3.2 will be a great release. There are still some problems with khtml and JS; knotes and kpilot; kwallet and some regressions if you use a webpage with a tabbed-Konqi or with a normal one. For me its hard to write good bug reports about these glitches. Maybe we need a bug-hunting-and-report-writing-session before the final bug fixing ;-)\n\nAll the best for 2004\n\n\n  Thorsten      "
    author: "Thorsten Schnebeck"
  - subject: "Re: gooeylinux"
    date: 2004-01-02
    body: "I compiled and installed KDE 3.2 Beta2 and I loved it. I really like plastik. I think it is professional and smooth. I like the configurable cursor. I think Konquror looks great. I also enjoyed Universal side bar.That is a great addition. \n\nI would like to see a KDE compile time configurator to decide what gets compiled and what does not. This can be simillar to Kernel Configurator. I know it is possible to change Konstruct Makefiles with appropriate configure flags. But for that to work I will need to know ever changing list of KDE modules and reinsert all the options with every new release of KDE. How about having a top level .config file for Konstruct with all the optional modules which exist within KDE as a beginning point?\n\n\n-Umesh"
    author: "Umesh Sirsiwal"
  - subject: "Re: gooeylinux"
    date: 2004-01-02
    body: "> It would be nice to have a minimal set of default apps\n\nWhy is that? To save harddisk space worth about EUR 0.20 (assuming 200MB) ?\n\nIf you calculate only EUR 15 per hour and you save only 3 minutes because you don't have to install some application you need, you already saved EUR 0.75 which already paid for that additional harddisk space.\n\n"
    author: "Roland"
  - subject: "Re: gooeylinux"
    date: 2004-01-02
    body: "to reduce menu clutter and things like 'potato guy' which does seem like bloat to me"
    author: "ac"
  - subject: "Re: gooeylinux"
    date: 2004-01-02
    body: "Hey, my niece likes him!"
    author: "Roberto Alsina"
  - subject: "Potato Guy :("
    date: 2004-01-02
    body: "It is a wonderful app, still there seems to be no active development on it :(\nThe images could be better, and there should be more scenarios to. Where is Konqui? And Kate? :-/"
    author: "Random"
  - subject: "Re: Potato Guy :("
    date: 2004-01-02
    body: "Well, I think that would be a serious deviation into grounds of overconfigurability. Imagine the tech support problems!\n\nWhen someone calls the suppor hotline for a potatoguy problem now, you can tell him how to put the nose in the face, and roughly the standard position for it, as well as what you mean by nose, describing the graphics in some detail.\n\nHow do you explain that what you describe about a grey-brown ovoid should also apply to a green dragon? No, it would be almost, dare I say it, evil HCI design.\n\nIf your irreflexive proposal was adopted, confused three-year-olds the world over would have tears in their eyes. Why would a dragon need a nose? It already has a fire-breathing muzzle! How scary would a eyeless, noseless, mouthless dragon look like? Do you want this app to be the cause of hundreds of nightmares?\n\nBesides, imagine the bloat! An additional \"theme\" would probably use about 50KB of disk storage, wasting over half a cent worth of HD, not to mention over one thousandth of a penny in bandwidth charges.\n\nAnd the configuration issues! A new control panel applet?\n\nNo. As Eugenia loli-Queru from OSnews.com always says, \"defaults matter\", and in this case, the potatohead default is correct, sufficient and perfect.\n\nMaybe after KDE gets its regedit-like app we can entertain a few keys that would tweak, MAYBE, the number of ears available, or the width/height ratio of the potato, but not further.\n\nPlease, avoid madness, let potatoguy stay in its current perfection cocoon."
    author: "Roberto Alsina"
  - subject: "Re: Potato Guy :("
    date: 2004-01-02
    body: "WOW! Sorry Mr. Potato guy! I didn't know you have so many friends! ;)\n\nI want to be your friend also Mr. Potato, I'm sorry about what I said :)\n\nStill then, someone ought to update you a litlle eh?"
    author: "ac"
  - subject: "Re: Potato Guy :("
    date: 2004-01-03
    body: "I am not, nor have I any affiliation with the potato guy."
    author: "Roberto Alsina"
  - subject: "Re: Potato Guy :("
    date: 2004-01-04
    body: "He's talking to me... \nI'm feeling so sad, please develop me! Make me higher resolution like Tux in the other scenario, give me some more friends, teach me other languages... Please don't forget about me :("
    author: "Potato Guy"
  - subject: "Re: Potato Guy :("
    date: 2004-01-04
    body: "Oh, you sure are an ambitious little vegetable, aren\u00b4t you?"
    author: "Roberto Alsina"
  - subject: "Re: Potato Guy :("
    date: 2004-01-18
    body: "hi suse i do not no how to get to konuror web browser bye"
    author: "max"
  - subject: "Re: gooeylinux"
    date: 2004-01-02
    body: "I agree with this. Just like the toolbars are configurable, the context menus should be. See http://bugs.kde.org/show_bug.cgi?id=66119"
    author: "claes"
  - subject: "Re: gooeylinux"
    date: 2004-01-03
    body: "Thanks for pointing that out!\n"
    author: "cm"
  - subject: "Re: gooeylinux"
    date: 2004-01-02
    body: "I think that is a really good idea. This should also be true for the K menu also, which is full of junk (for me!) that I don't need.\n\nI don't know what the best way to do this would be, and it may well be different for the context menu and the K menu. Ideally though, some way to make an edit right there and then, without a big song and dance about it.\n\n"
    author: "Dominic Chambers"
  - subject: "Overloaded context menus"
    date: 2004-01-05
    body: "> I think Konquerors menus and context menus are still overloaded in CVS.\n\nWell, I don't know how the CVS version looks like - but what I have seen on the latest screen shots proofs that. I am neither a KDE associate or a developer. Just an idea from a user point of view: I wonder why most KDE developers forgot about tear-offs. That would be so cute in particular for the konqueror context menus. Non-standard features that are still necessary to include could be grouped in submenus and those should have tear-offs. The context menu will be slim and nice to look at. Advanced users can pop-up permanent submenus utilizing tear-offs for easy access. The problem that the mouse-press-position is unknown for these menus can be solved by using the selected text/file/whatever. \n\nThis is something that really bothers me since 3.1: I believe the last tear-off can be found in one menu of konsole (this is not a request to remove that, too)! I have the impression that most developers made some hacks when filling the menus and simply removed the tear-offs because they didn't care for signals to en-/disable the items of the edit menu when the selection changes. Of course, it is easier to redefine the menu only when it is going to be opened and to check at this time for disabled items. Seems that the Gnome developers do a better job in this regard.\n\nPlease, KDE developers: Insert them again whereever you can! In particular the Kmenu is such inefficient since the tear-offs are gone that I am thinking about a switch to GXXXE or a rewrite of my own. Tear-offs are definitely _my_ way of working efficiently.\n\nAnd yes: I know of the option in kcontrol - hopefully it is a SuSE bug.\n\nRegards\nSebastian"
    author: "Sebastian"
  - subject: "Re: gooeylinux"
    date: 2004-01-03
    body: "The reviewer didn't like KDE before he started, and still doesn't like it. Although some of the features are very good.\n\nDerek"
    author: "Derek Kite"
  - subject: "\"pycs\""
    date: 2004-01-02
    body: "I think you'll find that the reviewer at the \"pycs\" site is our old friend Roberto Alsina.\nNot that he would be a bit biased, oh no. ;)"
    author: "taj"
  - subject: "Re: \"pycs\""
    date: 2004-01-02
    body: "Ahem. And I even mentioned it there ;-)"
    author: "Roberto Alsina"
  - subject: "Re: \"pycs\""
    date: 2004-01-02
    body: "And it crashed. Oh well :-P"
    author: "Roberto Alsina"
  - subject: "Re: \"pycs\""
    date: 2004-01-02
    body: "Is anybody else having problems? I am using a squid proxy and am being denied"
    author: "a.c."
  - subject: "Re: \"pycs\""
    date: 2004-01-02
    body: "Same problem here ;'(\n\n  The proxy server received an invalid response from an upstream server.\n  The proxy server could not handle the request GET /lateral/stories/12.html.\n  Reason: Could not connect to remote machine: Connection refused\n\nRoverte, can you please fix the server or mirror the article?"
    author: "Niek"
  - subject: "Re: \"pycs\""
    date: 2004-01-03
    body: "I am on vacation. On the beach. Drinking alcohol. No access to the original copy right now.\n\nHowever, pycs is back up, so you should be able to read it now. That proxy error happens every once in a while when the PyCS server crashes or stops for some reason, because it\u00b4s reverse-proxyed and you access teh prxy that can\u00b4t read the page."
    author: "Roberto Alsina"
  - subject: "pet peeve"
    date: 2004-01-02
    body: "i generally find reviews, good bad and ugly, interesting to read if only because of the groupthink they tend to display. spending time listening to people of various background quickly gives one pretty good impressions of what needs to be addressed for the next release(s). \n\nhowever, one thing that drives me to distraction without fail is when the writer says that some particular thing sucks but doesn't ever say which particular thing or why. as an example, the gooeylinux.org review says that \"The error screens in KDE are pretty horrible\". Ooook. which error screens? the ones that appear when an app crashes? or the error boxes in kmail? or in konqueror? or ...... where?! it's a little frustrating to know that someone has seen something that might need improvement, but not know what that thing actually is.\n\nif you're going to write a review and want to complain about something, provide specifics about what you are complaining about. Eugenia at osnews.com usually provides screenshots of these things, which is rather nice, but even a bit of specifics in the text goes a long ways."
    author: "Aaron J. Seigo"
  - subject: "Re: pet peeve"
    date: 2004-01-02
    body: "Look on the bright side, a bad 'previewer' is almost always without clue and most things he/she says hover around in clueless space. The bad thing is that, generaly, the clueless 'preview' reader has no clue to begin with and can spread the 'clueless stuff'. So, looking on the bad side, the bright side is not so bright after all. But look on the bright side, if you dont realize the bad side in the first place, you wont notice the bad side, but the bad side of that is that you never see the bright side. Yeah!"
    author: "ac"
  - subject: "Re: pet peeve"
    date: 2004-01-02
    body: "I guess he means the error message which http://www.gooeylinux.org/bensjunk/kde/c_c_error1.png shows inside the screenshot collection without index to which he pointed in the first comment to his own story."
    author: "Anonymous"
  - subject: "Re: pet peeve"
    date: 2004-01-02
    body: "ah.. i didn't bother to read the comments.. looking at that link, there are two screenshots there named *error*.png, though only one is actually an error. so now i know what he's referring to: the various dialogs that pop up in various apps. i'm sure those two boxes he found heinous enough to make screenshots of caused him all sorts of problems; we probably owe him 10 seconds of his life back. ;-) they are definitely cosmetic issues that users don't need to be bothered with, but not quite as interesting as it might have been...\n\nnow to figure out what he meant by the all the other vague statements in his article... "
    author: "Aaron J. Seigo"
  - subject: "Re: pet peeve"
    date: 2004-01-02
    body: "This is why governmental evaluations of desktops systems are also fruitful for FLOSS development. Even when the Government decides against KDE/Linux we benefit from the input. All issues that we the reason for the non-adoption of KDe in the German Parliament are now resolved. It is important to define problems and resolve them. Don't complain, fix it."
    author: "elektroschock"
  - subject: "KDE Packages! three cheers 3.2!!!"
    date: 2004-01-06
    body: "I am Using KDE 3.1.94 on Mandrake 9.2 base systems. It is pretty cool. But I believe other than Keramik style all styles need to be in a separate packages and number of games in the default game packages also need to be reduced. I think Patience, Klicketty, Smile and two or three logic/strategy games are way more than cool to be included in the default packages. Otherwise we should have option to choose which package I can Install. Actually Carefully packaging tools will reduce amount of downloads and amount of time it takes to compile. I have tried to compile QT with the following options -mcpu=pentium3 -mmmx -msse -fpmath=sse,387 but it does not work. Although ENtire KDE packages have been compiled with this option.I think make apidox need to be part of default make rather than a separate option and should have different option to exclude building apidox.\n\nThanks.\nRaihan"
    author: "S. Mamun Raihan"
  - subject: "Plastik"
    date: 2004-01-02
    body: "Plastik is very nice. One problem though: I think the difference between a pressed button and an unpressed button is too small. In the taskbar it is somewhat hard to see what window is active since there is so little contrast between the buttons. I think the contrast should be sligthly increased to improve this."
    author: "claes"
  - subject: "Re: Plastik"
    date: 2004-01-02
    body: "Plastik also makes no use of the contrast slide available in the color setting as of now. Before Plastik becomes default this should be changed and then could also be used to improve the contrast in the future."
    author: "Datschge"
  - subject: "A review"
    date: 2004-01-02
    body: "Let me right my own review about KDE.\nI am using Gentoo and KDE 3.1.4. I do not plan to install KDE 3.2 unitl it becomes stable (sorry developers for not helping you, but I can not afford a crash or to lose data)\n\nKDE has many buttons, but this means that it is very configurable, which might make your day difficult, if you do not know how to move around. This is a problem only you use KDE once, however.\n\nI do not understand why people like Gnome....  because of \"Start here\" button .. and so if you click on it you will a find a very few settings, the basics. Are they happy with it? I am not... I was told, and in my work I realized that it is important to pay attention to the details. My advisor is telling me to do a merticulous job and this IS what KDE is about.\n\nIf you can not get around because of all these buttons, then you can use the \"configure toolbar\" option to remove some of these buttons, but apparentlly the reviewers do not know it.\n\nTo summarize:\nIf you do not like KDE, if you prefer Gnome this is fine, but you do not have the right to write about a program if you have not used it for more than a week. This is not a game!! This is a DE, which might take some time to configure it, but in the end you will be happy with all the options that are available to you.\n\nI am using KDE since version 2.1 (Redhat 5 or 6), I fully trust the developers of KDE and I know that they will not dissapoint me. \n\nKeep up the good work.,\n\nVasilis"
    author: "Vasilis"
  - subject: "Re: A review"
    date: 2004-01-02
    body: "Don't forget that you can set up a separate user account to run KDE 3.2, instead of installing it system-wide.  I think this means you can't use emerge, however, which DOES install things system-wide."
    author: "Michael Pyne"
  - subject: "Start here"
    date: 2004-01-03
    body: "This guy doesn't know that ~/Desktop was standardized by freedesktop.org, then Gnome drops gmc and introduced Nautilus, with its own uncompatible desktop files.\n\nThis is indeed an ugly review... A person that knows nothing about the thing he is reviewing (besides he plainly dislikes him) can't be taken seriously.\n\nAnyway, at random he scores a few points... "
    author: "Julio Cesar Gazquez"
  - subject: "CC Usability"
    date: 2004-01-02
    body: "What is the difference between keyboard shortcuts and Khotkeys?\nWhy are they in different categorys? I don't understand the difference given the name.\n\nhttp://www.gooeylinux.org/bensjunk/kde/c_c_clutter.png"
    author: "andre"
  - subject: "Re: CC Usability"
    date: 2004-01-02
    body: "There is also an option \"keyboard\" under periphals..."
    author: "andre"
  - subject: "Re: CC Usability"
    date: 2004-01-06
    body: "Keyboard shortcuts usually depend on specific applications while khotkeys allows you to add fully customized shortcuts including mouse gestures independent of applications (ie. also usable for non-KDE apps if they give eg. cli access to their commands, eg. you could control xmms with mouse gestures when using khotkeys)."
    author: "Datschge"
  - subject: "I dont understand all this nagging on KDE"
    date: 2004-01-03
    body: "I also think that many of the reviewers are biased against KDE. I just dont understand why!\nI've bin using the KDE CVS version for months now.\nI use it at work, at home. I use it for programming, wordprocessing, gaming, watching movies, listening to music, practically all the time.\nI have very few crashes and the speed is amazing!!\nI really think the reviwers should pukk themselves together and try to USE KDE for a while before saying anything!!\nI would never say anything against GNOME, not because it is better than KDE, but because I dont use GNOME for long periods of time, so how on earth should I be able to say anything for or against it?\n\nThere is also a lot of talk about to many menuitems and buttons. Have a look at the attached snapshot. Yeah too many buttons:-)\nI agree on the previous post. KDE should have all items visible and let SUSE, RedHat, Mandrake etc. decide WHAT to include.\nAlso the talk about all the applications. Damn the guy is an amateur!!\nIf he dont want them all then dont make a make install in all!!\nThe distributers will let the user install the applications they need, but if you download and install all of KDE, then of course you get a lot of applications. Nag nag nag, and with very bad arguments!!\nIn fact this is one of the joys about compiling KDE.\nI have never tried myselves to compile GNOME. I looked at all the packages I needed to compile GNOME and gave up at once. It is much easier with KDE, just download arts, kdelibs, kdebase... and compile.\nI really love KDE, I hope SUSE is not skipping KDE since SUSE is the one distrubution I've found to have the bestr integration of applications with KDE.\nI really think that the guys develpoing the various applications have done a great job in 2003 and I'm really looking forward to seing what 2004 brings!\n\nAnd another note to the amateur of a reviewer... 256Mb RAM on a 2 GHz machine hi hi hi. Get a life. Why have a machine with that processor when you're running on SWAP? Dont tell me that GNOME does not use most of the RAM. It does here so why should it not there?\nI have 1G and KDE takes up about 200 of these MB, I dont think that is bad concidering how big and fast the applications are."
    author: "Jarl E. Gjessing"
  - subject: "Re: I dont understand all this nagging on KDE"
    date: 2004-01-03
    body: "The guy is reviewing the \"default\" kde setup so that means all of it and he's complaining that they include too many apps in the whole thing. Just because the guy isn't as rich as you he doesn't have a life? I only have 384mb of ram, take your best shot. People have different preferences, deal with it"
    author: "Michael Douglas"
  - subject: "Re: I dont understand all this nagging on KDE"
    date: 2004-01-03
    body: "i have 1.5 gig ram , so lets meet in the middle at 768 mb.\n"
    author: "chris"
  - subject: "Re: I dont understand all this nagging on KDE"
    date: 2004-01-03
    body: "> he's complaining that they include too many apps in the whole thing\n\nI don't think he can nag about this since apparently he doesn't know how to disable compliation of certain apps (like what distros would do with a stable version).."
    author: "fault"
  - subject: "Re: I dont understand all this nagging on KDE"
    date: 2004-01-03
    body: "I promise you... I'm not rich.. Not rich at all.\nI just know that I need RAM more than I need CPU speed.\nIf you are reviewing packages as KDE that you compile youre selves then you should know this fact I think"
    author: "Jarl E. Gjessing"
  - subject: "Re: I dont understand all this nagging on KDE"
    date: 2004-01-03
    body: "If he's \"rich\" enough to buy a 2 GHz machine, he can certainly afford $70 for another 256MB of RAM."
    author: "Tukla"
  - subject: "Re: I dont understand all this nagging on KDE"
    date: 2004-01-03
    body: "You probably don't know how to measure ram usage.\nA hint, type 'man free' in a console."
    author: "ealm"
  - subject: "Re: I dont understand all this nagging on KDE"
    date: 2004-01-03
    body: "Yes I know how to measure RAM usage!\nThat is why I'm saying that KDE uses about 200 Mb.\nLinux uses close to 100% because it caches as much as it can.\nWhy do you mean that I cannot measure the RAM usage?\n\nI'm just saying that if you use a package as large as KDE and expect it to run with little RAM usage."
    author: "Jarl E. Gjessing"
  - subject: "Re: I dont understand all this nagging on KDE"
    date: 2004-01-03
    body: "What he mean is that you dont know how to measure RAM usage, and he is right (neither does the reviewer, so dont feel bad). KDE doesnt use anywhere near 200Mbyte RAM, you are counting a lot of the same RAM-usage more than twice, or use the amount of \"free\" space which is totally useless on Linux."
    author: "Carewolf"
  - subject: "Re: I dont understand all this nagging on KDE"
    date: 2004-01-04
    body: "I know it does. But when I'm counting the RAM usage I'm counting with all the KDE apps that I have running at once and that is many.\nAnd then yes then the RAM usage comes close to 200.\nWith Korganizer, Kmail, Konqueror kscd ++++ you will automaticall use a lot of RAM"
    author: "Jarl E. Gjessing"
  - subject: "Re: I dont understand all this nagging on KDE"
    date: 2004-01-03
    body: "<I>256Mb RAM on a 2 GHz machine hi hi hi. Get a life. Why have a machine with that processor when you're running on SWAP?</I>\n\nThis guy is probably a lamer, but maybe he can't afford buying more memory.\n\nMyself I do run KDE on a 2.4 GHz with 256 MB RAM computer, just because I bought it second hand, and it was like that. Usually I don't need to run that many apps simultaneously, so tipically it doesn't require swapping and the computers performs fine enough.\n\nAt some moment I will spend money on RAM, just not right now, I'm afraid. But hey, I accept donations ;)"
    author: "Quique"
  - subject: "Re: I dont understand all this nagging on KDE"
    date: 2004-01-07
    body: "> ... 256Mb RAM on a 2 GHz machine hi hi hi.\n\nThat was my first thought, too. ;-)\n\nPlease people use _pmap_ on Linux, Solaris, etc. PLEASE, PLEASE!\n\nV4.0/CMake> free -t\n             total       used       free     shared    buffers     cached\nMem:       1034732    1006740      27992          0     244112     451252\n-/+ buffers/cache:     311376     723356\nSwap:      2103288     128828    1974460\nTotal:     3138020    1135568    2002452\n\nCount \"buffers\" + \"cached\" etc.\n\n\"Small\" example X with DRI-Devel (;-):\n\n 9011 root      15   0  133m  45m  78m S  2.0  4.5   9:24.71 schedule_ X\n\nHave a special look at \"Private\" and _substract_ \"[ anon ]\"...\n\nV4.0/CMake> pmap -x `pidof X`\n9011:   /usr/X11R6/bin/X vt7 -auth /var/lib/xdm/authdir/authfiles/A:0-a\nAddress       kB Resident Shared Private Permissions       Name\n000a0000     128       -     128       0 read/write/exec   mem\n000f0000      64       -      64       0 read/exec         mem\n08048000    1456       -       0    1456 read/exec         XFree86\n081b4000     200       -       0     200 read/write        XFree86\n081e6000   28768       -       0   28768 read/write/exec    [ anon ]\n40000000      96       -       0      96 read/exec         ld-2.3.2.so\n40018000       4       -       0       4 read/write        ld-2.3.2.so\n40019000       4       -       0       4 read/write         [ anon ]\n40031000      52       -       0      52 read/exec         libz.so.1.1.4\n4003e000       8       -       0       8 read/write        libz.so.1.1.4\n40040000     136       -       0     136 read/exec         libm.so.6\n40062000       4       -       0       4 read/write        libm.so.6\n40063000       8       -       0       8 read/exec         libdl.so.2\n40065000       4       -       0       4 read/write        libdl.so.2\n40066000       4       -       0       4 read/write         [ anon ]\n40067000    1200       -       0    1200 read/exec         libc.so.6\n40193000      20       -       0      20 read/write        libc.so.6\n40198000       8       -       0       8 read/write         [ anon ]\n4019a000      28       -       0      28 read/exec         libnss_compat.so.2\n401a1000       4       -       0       4 read/write        libnss_compat.so.2\n401a2000      72       -       0      72 read/exec         libnsl.so.1\n401b4000       4       -       0       4 read/write        libnsl.so.1\n401b5000       8       -       0       8 read/write         [ anon ]\n401b7000      32       -       0      32 read/exec         libnss_nis.so.2\n401bf000       4       -       0       4 read/write        libnss_nis.so.2\n401c0000      36       -       0      36 read/exec         libnss_files.so.2\n401c9000       4       -       0       4 read/write        libnss_files.so.2\n401ca000     148       -       0     148 read/write         [ anon ]\n401ef000      28       -       0      28 read/exec         libfreetype.so\n401f6000       4       -       0       4 read/write        libfreetype.so\n4020e000     292       -       0     292 read/exec         libfreetype.so.6.3.3\n40257000      16       -       0      16 read/write        libfreetype.so.6.3.3\n4025b000     176       -       0     176 read/write         [ anon ]\n40287000     512       -     512       0 read/write        mem\n40307000   65536       -   65536       0 read/write        mem\n44307000      64       -      64       0 read/write        mem\n44317000       8       -       8       0 read/write        card0\n44319000    1028       -    1028       0 read/write        card0\n4441a000       4       -       4       0 read/write        card0\n4441b000    2048       -    2048       0 read/write        card0\n4461b000    4992       -    4992       0 read/write        card0\n44afb000    2048       -    2048       0 read/write        card0\n44cfc000     228       -       0     228 read/write         [ anon ]\n44de3000     748       -       0     748 read/write         [ anon ]\n44fce000   15372       -       0   15372 read/write         [ anon ]\n4638e000   10248       -       0   10248 read/write         [ anon ]\n46de2000       8       -       0       8 read/write         [ anon ]\nbffa8000     352       -       0     352 read/write/exec    [ anon ]\n--------  ------  ------  ------  ------\ntotal kB  136216       -   76432   59784\n\nWhat do you get...?\n\nGreetings,\n\tDieter\n\nPS Have a look at http://dri.sourceforge.net/cgi-bin/moin.cgi/ for \"current\" DRI (OpenGL/Mesa)."
    author: "Dieter N\u00fctzel"
  - subject: "Re: I dont understand all this nagging on KDE"
    date: 2005-04-10
    body: "Hey, my situation is differant and I hope you may be able to offer some insight.  I have a P4 2.4 G machine and 448 of RAM.  I run Mandrake 10.1 off with everything possible installed.  I.E. Gnome and KDE 3.2 I like to run them at the same time simply because i like bragging about having two loggins at the same time for my \"windows\" friends.  Anyways I have 894 Mb partition for swap but my devise never uses it.  Is there some way to turn it on or point to it.  I dont do a lot of programming but I live on my computer so I run a lot of apps.  My CPU stays low but my RAM is always running hi.  I check via Ksysguard and free -m.\n\nBasically can you help me start utilizing my swap?"
    author: "mitch"
  - subject: "Re: I dont understand all this nagging on KDE"
    date: 2005-04-10
    body: "Here is how it works.\n\nType free at your command line (here's what I get):\n\n             total       used       free     shared    buffers     cached\nMem:        320036     316672       3364          0      11620     118884\n-/+ buffers/cache:     186168     133868\nSwap:       192740      18136     174604\n\nNow, if you have a high \"free\" on \"-/+ buffers cache\", you have memory to spare, and don't worry.\n\nIf you have that low, and \"total\" on \"swap\" is 0, you would need to enable your swap, because it's disabled.\n\nHowever, if your \"total swap\" is something other than 0, and you have free memory (in the second row, not the first!), you are just fine, and don't worry."
    author: "Roberto Alsina"
  - subject: "Re: I dont understand all this nagging on KDE"
    date: 2005-04-12
    body: "What do you get with 'free -t'?\n'cat /proc/meminfo'?\n\nIf your swap IS enabled and you see '0' (Zero) then it isn't used 'cause you have 'enough' RAM.\n\nHope that helps.\n\n-Dieter"
    author: "Dieter N\u00fctzel"
  - subject: "The Reveiw on GooeyLinux is Excellent."
    date: 2004-01-03
    body: "I, personally, think the KDE development team needs to take a step back and figure out what direction we need to be going here.\n\nThe KDE team has MANY things going for it, but I feel like we're squandering it. We have an excellent DCOP setup, fantastic internationalization team, WONDERFUL framework in place, KParts that make embedding things in other apps wonderful, etc, etc, etc.\n\nKDE's strengths then are EASE OF DEVELOPMENT. INTEGRATION. But while these things make the life of a programmer a Joy, they DON'T nessesarily keep us in tune with USERS. And DEVELOPERS using a desktop envirionment is different from USERS using an environment. WE can deal with tons of options, 'cause we're technologically savvy and don't MIND digging through docs. But I know my dad can pick up Mozilla Thunderbird and DO what he NEEDS TO DO instantly. KMail has taken him a week to start to be comfortable. Konqueror is similar. Hell ALL the apps are similar. Just because you CAN show off \"all the feature\" doesn't mean you should. Lets try to build SANE GUI components. Lets get even CLEARER and BETTER GUI standards! I wish I had the technical expertise to draw up a set of standards that was clear and consise. It needs to be an EASY read. WELL indexed. A few pages at most with links to more stuff. Hell maybe I will write one just so people can tell me it's crappy and I get more talented persons to redo it. The old GUI guide, I'm sorry, sucks. Big time. No specific guidelines, and it's not a good/easy read.\n\nWe need to be turning the corner. Think Enterprise. Think Users. We already have a great development environment, tons of translaters, and good programmers. Are we going to CONTINUE to make a desktop to use to write software to make a desktop to write software to... ad nauseum?\n\nregards,\ndan"
    author: "danOstrowski"
  - subject: "Re: The Reveiw on GooeyLinux is Excellent."
    date: 2004-01-03
    body: "> Hell maybe I will write one just so people can tell me it's crappy and I get more talented persons to redo it. \n\nPlease do so if you want... writing drafts of specs is always better than doing nothing at all. "
    author: "anon"
  - subject: "Re: The Reveiw on GooeyLinux is Excellent."
    date: 2004-01-03
    body: "No, KDE project as a whole is no way at a turning point, it's rather at a point where it is both featureful and flexible enough to be adapted to very different audiences. Please remember that the KDE project itself doesn't offer anything but the source code, binaries and preselection of programs with different defaults are offered by distributions and other service companies. I doubt you are telling your dad to configure/make/make install KDE and work with the default KDE just for then bitching that it's doesn't suite him, you will rather suggest him a distribution which chose KDE settings you  know your dad likes.\n\nI think it's very sad that distributors and service companies so far not at all made extensive use of KDE's flexibility regarding settings and defaults at all. Somehow everyone seems to refer to GNOME for an agreeable minimal amount of visible features [insert the regrets about removed features here] but at the same time while it's perfectly possible to make KDE have a minimal amount of visible features as well just by changing settings while not touching code/removing features no single person or company dared to give it a try. Why is that?"
    author: "Datschge"
  - subject: "Re: The Reveiw on GooeyLinux is Excellent."
    date: 2004-01-03
    body: "perfectly possible to make KDE have a minimal amount of visible features as well just by changing settings while not touching code/removing features no single person or company dared to give it a try. Why is that?\n----------\nPeople view KDE as a complete product, even if the KDE project doesn't position it that way. There are a few companies, like Lindows and Xandros, willing to customize and ship a different UI, but most companies, and most people, view that as doing an end-run around the KDE project itself. Few people are willing to use the customized KDE versions, because they are usually behind the latest available version, and people percieve the customized version as being not a proper KDE, but a \"forked\" version. \n\nIf it is considered a legitimate need to offer a simplified KDE UI, if only for the corporate market, such a project would have to come from within KDE, and be sanctioned by the KDE project. Like it or not, the DE projects have a prominance others do not. They are not a behind-the-scenes project that simply makes technology that gets integrated into a distribution (like, say, the glibc project). Instead, they transcend distribution boundries, and must market the DE and interact directly with companies. The GNOME project understands this. They engage in marketing tactics like putting OpenOffice and Mozilla under the GNOME banner, even thought that means nothing technically. They've managed to convince most corporate users that the large body of GTK-only apps are really GNOME apps. \n\nCorporations like that sort of organization. They want important initiatives to come directly from the prominent project, not just some independent ISV. "
    author: "Rayiner Hashem"
  - subject: "Re: The Reveiw on GooeyLinux is Excellent."
    date: 2004-01-03
    body: "gpwm.\n\nalso: you're essentially asking for a duplication of effort by interested parties? why? come up with a minimal, normal, super-loaded set of options within the kde project, you know the code best."
    author: "anon"
  - subject: "Re: The Reveiw on GooeyLinux is Excellent."
    date: 2004-01-03
    body: "What does gpwm mean?\n\nAs for the minimal UI coming from within the KDE project: isn't that what I said?"
    author: "Rayiner Hashem"
  - subject: "Re: The Reveiw on GooeyLinux is Excellent."
    date: 2004-01-03
    body: "- UI usbaility research, perhaps cross-finaced via Governments or master thesis works\n\n- Enterprise software\n\n- Kiosk mode, solutions for Internet Caf\u00e9s and the corporate desktop"
    author: "andre"
  - subject: "My personal Review"
    date: 2004-01-03
    body: "I'm using it daily, and got only two problems:\n1. konqueror krashes too much and I have to use mozilla firebird.\n2. quanta sometimes is very slow on typing.\n\nAs it is just the second beta, I can't complain about those minor problems. Overall it's really good."
    author: "Iuri Fiedoruk"
  - subject: "Re: My personal Review"
    date: 2004-01-03
    body: "Just fond a new one, a bad one:\n\n3. unlocking dosen't work, password isn't reconized"
    author: "Iuri Fiedoruk"
  - subject: "Re: My personal Review"
    date: 2004-01-03
    body: "I suppose you're talking of the sceenlock.\n\nHave you installed your KDE as root?  \nAFAIK some file must be suid root so it's able to read \nthe crypted password from /etc/shadow. \nIf it isn't then you get this behaviour.\n"
    author: "cm"
  - subject: "Re: My personal Review"
    date: 2004-01-03
    body: "It is checkpass that needs to be root, it is normally in $KDEDIR/bin\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: My personal Review"
    date: 2004-01-03
    body: "Yes, it wasn't installed as root. :)\nThanks for the tip guys, I'll try it."
    author: "Iuri Fiedoruk"
  - subject: "A Business point of view"
    date: 2004-01-03
    body: "Been running 3.2b2 since the day after release - after 1 day of testing, liked it so much, I put it on my production laptop with Fedora.  Haven't tested many of the new exciting high productivity features (like VNC), just using it in normal daily business apps (which IMO is a good test).  Also I know KOffice is separate (but do think they should be released together..) And KDevelop - well, a good development environment is the key...it needs more attention.\n1) Biggest problem is with MIME - the default settings are all screwed up (could this just be the Fedora port?)\n2) Next biggest problem are fairly frequent hangs - Esc or Cntl+Alt+Esc are usually needed especially when clicking on the start menu (My guess is this is a Fedora threading problem - others have told me the beta is running smooth on Debian)\n3) Konqueror is running fine, a few pages here and there have problems (me thinks mostly a missing flash and java plugin issue)...BUT.. pdf viewing is totally messed up - not sure if this is a Kuickshow problem or a MIME problem or whatever - didn't have this problem with 3.1.4.   The new popup menu that allows you to tar a bunch of files is very nice.  Desktop icon sorting by type still sorts on spelling instead.\n4) Kontact is working well but Palm synching STILL not running right - this time no calendar info seems to move to PC.  Only some notes move to PC (doesn't seem to be just a private issue).  The newer KPilot conduit settings dialog seems pretty well done but Palm synching should be right in the Kontact menus.  KNode is working well. KMail is working well and seems to download mail much faster. The new address book has lots of new useful fields (especially category - thank you!).  Will be testing it out in a contact manager role here shortly (though without reliable synching, can't really use it the way we want).  The address lookup in KMail is still very slow. I'm still not sure how to use SpamAssassin with this - for a normal user this would be nice to be more built in - in next version a wizard for this might be nice.\n5) Kivio shows a much nicer operation - just missing the ability to put in a background image on the drawing.  And of course a visio import would be so nice.\n6) I've used KWord to read those .doc email attachments that some still send - the .doc import seems vastly improved (and of course way faster than OpenOffice)\n7) Umbrello has the potential to be an award winning program addition -  thinking of now completely switching from our Dia and Kivio diagrams and Whiteboards.\n8) KDevelop - the new menu structure and dialogs are making this much easier to use.  Some Qt projects imported easily.  But it has crashed a few times and so we are still relying on Kate.  I'll repeat and say this is the program that deserves the most attention - far more than arguing over control center designs.  \n9) Kate & KWrite are both working well (though the file list docking button is not obvious - it would be nicer to dock by default instead of overlapping).  Old problems of losing open KWrite file display when logging out are gone - both packages seem to save and reopen themselves very well now as does Qt Designer.  \n10) K3b is also a nice addition and is now CD burner of choice - but it wasn't on the menus - had to add it.\n11) Kde-forum.org is nice (need to use it more) but would prefer a simple newsgroup instead (this way all support forums I use would be in KNode).\n12) Not sure why many seem to be complaining about some missing \"start here\" button - Maybe I just hire smart employees or something but never had an issue with being able to use the menus to change settings - in fact I wish my employees couldn't find their way around control panel so easily so their settings would remain consistent :).  The KDE setup wizard does not include plastik as an option (should probably be the default).\n13) Please get rid of screensaver in random that shows your last screen in a revolving manner - it shows whatever confidential last screen details to passer-bys while you are up at the counter getting a latte.\n\nAs a project run by volunteers and is free and somewhat crossplatform - 3.2 is a great effort and accomplishment."
    author: "john"
  - subject: "Re: A Business point of view"
    date: 2004-01-04
    body: "> 12) Not sure why many seem to be complaining about some missing \"start here\"\n>button - Maybe I just hire smart employees or something but never had an issue\n> with being able to use the menus to change settings - in fact I wish my \n> employees couldn't find their way around control panel so easily so their\n> settings would remain consistent :). The KDE setup wizard does not include \n> plastik as an option (should probably be the default).\n> 13) Please get rid of screensaver in random that shows your last screen in a \n> revolving manner - it shows whatever confidential last screen details to \n> passer-bys while you are up at the counter getting a latte.\n\nHave you taken a look at the kiosk framework?  \nIt addresses those two points: \n\n- You can lock down parts of the configuration so the desktops remain consistent\n- You can disable any screensavers that break confidality\n(not one-by-one, but with one single option, that's a supported feature)\n\n"
    author: "cm"
  - subject: "Re: A Business point of view"
    date: 2004-01-04
    body: "4) [...] I'm still not sure how to use SpamAssassin with this - for a normal \n> user this would be nice to be more built in - in next version a wizard for \n> this might be nice.\n\nIt's already in the works: \nhttp://dot.kde.org/1073158897/\n\n"
    author: "cm"
  - subject: "Re: A Business point of view"
    date: 2004-01-04
    body: "> 11) Kde-forum.org is nice (need to use it more) but would prefer a simple newsgroup instead (this way all support forums I use would be in KNode).\n\ncomp.windows.x.kde does exist, and you can also read all KDE mailing lists with KNode at nntp://news.uslinuxtraining.com and also with many more at nntp://news.gmane.org."
    author: "Anonymous"
  - subject: "redundand application(s)"
    date: 2004-01-07
    body: "One of the thinks some reviews pointed out is the redundancy of menus and options in KDE applications. It seems to me that there is some redundancy in KDE applications. For example kedit seems to be a trivial editor that can spell check and send email (!?). Kwriter also shares so many features with kate that it looks like a striped down version of it. I can mention several other examples and my question is why don't the KDE people try to make things a bit more simple by droping a couple of applications and merging their functionality to others?"
    author: "Reader"
  - subject: "Re: redundand application(s)"
    date: 2004-01-07
    body: "> Kwriter also shares so many features with kate that it looks like a striped down version of it.\n\nSurprise, it is a stripped down version."
    author: "Anonymous"
---
Three reviews recently joined the <a href="http://kde.ground.cz/tiki-index.php?page=Reviews">list of KDE 3.2 reviews</a> with most having an emphasis on the <a href="http://kde.ground.cz/tiki-index.php?page=KDE+3.2+Applications+Difference">applications being new in KDE 3.2</a>: <a href="http://www.francesc.net/">Francesc</a> tried it on Gentoo and <a href="http://www.francesc.net/index.php?op=ViewArticle&articleId=29&blogId=1">states in this blog</a> "<i>This release is the best KDE I've ever tried</i>". <a href="http://www.pycs.net/">Pycs</a> writes about his <a href="http://www.pycs.net/lateral/stories/12.html">first impressions and likes it too</a>. Finally, <a href="http://gooeylinux.org">gooeylinux.org</a> published a <a href="http://gooeylinux.org/forums/index.php?showtopic=44">rather controversial article</a> about the good and bad sides of KDE 3.2 Beta 2.
<!--break-->
