---
title: "Linux Magazine: KolourPaint and KRecipes"
date:    2004-07-04
authors:
  - "binner"
slug:    linux-magazine-kolourpaint-and-krecipes
comments:
  - subject: "Info"
    date: 2004-07-04
    body: "Read mentioned Kolourpaint TODO here:\n\nhttp://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdegraphics/kolourpaint/TODO?rev=1.54&content-type=text/x-cvsweb-markup"
    author: "Martin"
  - subject: "License"
    date: 2004-07-04
    body: "I read Kolourpaint was BSD license? I prefer this license for applications, the gpl for system infrastructure.\n\nIs there a development plan for Kolourpaint? "
    author: "hein"
  - subject: "Re: License"
    date: 2004-07-06
    body: "> Is there a development plan for Kolourpaint? \n\nSorry to bang the same drum again, but it needs to be said.\n\nI hope that KolorPaint will also be developed so that it can do the same basic things for SVG vector graphics as well.\n\nIt would also be nice if it could deal with both at the same time -- output should be HTML, XHTML, XML, etc.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: License"
    date: 2004-07-07
    body: "> > Is there a development plan for Kolourpaint?\n> Sorry to bang the same drum again, but it needs to be said.\n\nRead the TODO file or the KDE 3.3 feature plan.\n\n> I hope that KolorPaint will also be developed so that it can do the\n> same basic things for SVG vector graphics as well.\n\nMaybe if there was a \"KolourDraw\" :)"
    author: "ddd"
  - subject: "Re: License"
    date: 2004-07-07
    body: "Would be a nice little project for James to learn C++ with. Maybe make use of the new KCanvas in kdenonbeta, slap a simple GUI around it, and presto."
    author: "Boudewijn Rempt"
  - subject: "Re: License"
    date: 2004-07-08
    body: "It isn't C++, I simply can't figure out KDE programing. :-)\n\nIs there a tutorial somewhere??\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: License"
    date: 2004-07-08
    body: "I didn't use a specific KDE tutorial myself: I started out years ago with Python and Qt, and translated the C++ Qt tutorials into Python. And then I transferred my Python and Qt knowledge to doing C++ and KDE.\n\nBut I've seen quite a few tutorials come by on the Dot. Wait a minute... Searching... Yes: here's one: http://perso.wanadoo.es/antlarr/tutorial/index.html. And there's the documentation inside KDevelop, too, and Qt's help pages."
    author: "Boudewijn Rempt"
  - subject: "Re: License"
    date: 2004-07-08
    body: "If you want tutorials for C++, many people recommend trying the Qt tutorials first. \n\nThen they are a few KDE specific tutorials at: http://developer.kde.org/documentation/tutorials/index.html\n\nThere is the KDE 2 development book: http://developer.kde.org/documentation/books/kde-2.0-development/index.html It is for KDE 2 but the basics have not change too much (see the 2 porting files in kdelibs).\n\nAnd may be there are other useful resources in http://developer.kde.org/documentation or somewhere else in http://developer.kde.org\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: License"
    date: 2004-07-09
    body: "> If you want tutorials for C++, many people recommend trying the Qt tutorials > first. \n\nI should have figured that nobody would get the joke.\n\nThe question was whether there was a tutorial for KDE programing.\n\nI have no problem with C++.  But, what nobody seems to notice is that KDE programs are so different from ordinary C++ that they might as well be a different programing language.\n\nI found this out when I tried to make some very minor changes to KMenuEdit.  Changes which would have been very simple to make to a program written in a procedural language and would probably have been possible in a C++ program, but since this is a KDE program, I haven't yet been able to even figure out how it works.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: License"
    date: 2004-07-08
    body: "> Maybe if there was a \"KolourDraw\"\n\nPerhaps you missed my point.\n\nTry to think outside of the box.  Why do we need two separate applications when you often need to use both bitmaps and vectors together?  Do you want to use two separate applications and then try to paste the results together with Quanta?\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "KPaint -> KolourPaint"
    date: 2004-07-04
    body: "It would be nice if the pointless KPaint in KDE could replaced by KolourPaint."
    author: "Markus Gans"
  - subject: "Re: KPaint -> KolourPaint"
    date: 2004-07-04
    body: "It already is."
    author: "Anonymous"
  - subject: "Desktop icon problems"
    date: 2004-07-04
    body: "Anyone knows if the desktop icon placements problems have been resolved for the 3.3 release?\n\nThat should be by far the most annoying bug."
    author: "Axel"
  - subject: "Re: Desktop icon problems"
    date: 2004-07-05
    body: "I don't think so, which bugreport are we talking about?"
    author: "Waldo Bastian"
  - subject: "Re: Desktop icon problems"
    date: 2004-07-05
    body: "http://bugs.kde.org/show_bug.cgi?id=35203\n\nSeems to me the 2 problames are still there.\n1. Moving an icon a little is impossible.\n2. Icons does not place itself exactly where you choose.\n\nJust take a look at ms windows, that's what icon moving/placement should be like."
    author: "Axel"
  - subject: "Re: Desktop icon problems"
    date: 2004-07-27
    body: "Yes. I've noticed the same problem than Axel. It seems that KDE adds a X and Y offset to the icon position when I am dragging the icon. When I place the icon after dragging it subtracts back the added offset placing the icon in a little top left offset position. It's very difficult to place the icons in the correct position in the desktop. This bug was introduced in KDE 3.2 version, it was not present in previous versions."
    author: "Mauricio Kaster"
  - subject: "Re: Desktop icon problems"
    date: 2004-07-05
    body: "Well, good to know the most annoying bug is one involving icon placement."
    author: "Ian Monroe"
  - subject: "Graphics for KOffice"
    date: 2004-07-06
    body: "The article makes my point about Krita (what it says about The GIMP is also true of Krita), while it will be an excellent program, it is simply more than the average Office suite user needs to add graphics to a DTP project or to a presentation.\n\nTherefore, I suggest that there should also be a KOffice version of KolorPaint, and that this should be the paint application included in the basic Office suite.\n\nThere is nothing wrong with Krita and there is nothing wrong with making additional KOffice based applications (in fact, this is probably a good idea), but we need a basic suite to compete with other Office suites.\n\n--\nJRT"
    author: "James Richard Tyrer"
---
In its <a href="http://www.linux-magazine.com/issue/44">current issue</a> <a href="http://www.linux-magazine.com/">Linux Magazine</a> introduces the upcoming paint program of KDE 3.3 <a href="http://www.linux-magazine.com/issue/44/KolourPaint.pdf">KolourPaint</a> (PDF) to its readers. A second story describes the installation and features of the recipes manager <a href="http://www.linux-magazine.com/issue/44/Krecipes.pdf">KRecipes</a> (PDF, <a href="http://www.linux-user.de/ausgabe/2004/06/042-krecipes/">German HTML</a>) which recently moved from kdenonbeta to kdeextragear-3 module.

<!--break-->
