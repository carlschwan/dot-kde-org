---
title: "QtForum.org Announces Qt Developer Contest"
date:    2004-02-09
authors:
  - "mgoettsche"
slug:    qtforumorg-announces-qt-developer-contest
comments:
  - subject: "existing apps?"
    date: 2004-02-09
    body: "So, can existing apps be submitted?  Can apps that depend on KDE be submitted, or \"pure Qt\" apps only?  The rules don't say that either category is excluded..."
    author: "LMCBoy"
  - subject: "Re: existing apps?"
    date: 2004-02-10
    body: "It seems that it's a qt-only apps only as all dependencies should be included in the tarball.\n'This folder must contain the source code to the program and, in source form, any code upon which it depends.'\n\nAs far as I know, it's only pure Qt applications for this contest.\n\nannma"
    author: "annma"
  - subject: "Re: existing apps?"
    date: 2004-02-10
    body: "You think they'd complain about including kdelibs in the tarball? :)"
    author: "AC"
  - subject: "done in dutch"
    date: 2004-02-09
    body: "Here's a link to a similar contest in the Netherlands.\nhttp://www.linuxmag.nl/scholen/ (pardon their dutch)\nThere was one last year too.\nMain prize this year is 1000 euros.\nI submitted 'cubetest', which earned me an inflatable penguin.\nhttp://www.kde-apps.org/content/show.php?content=9878\n"
    author: "Jos"
  - subject: "OKay, I need ideas"
    date: 2004-02-10
    body: "I got the know how, and I got the time.  (unless I find a job...)   I just need some ideas.  What educational software do we need?  KDE has a bunch, and I'm not going to re-create the wheel so what should I work with?"
    author: "Hank"
  - subject: "Re: OKay, I need ideas"
    date: 2004-02-10
    body: "Plenty of stuff. What did you have trouble with in grammar school and high school?"
    author: "anon"
  - subject: "Two ideas"
    date: 2004-02-10
    body: "These are three proprietary programs for MS Windows that you might want to clone (AFAIK there are no KDE equivalents):\n \n \niSpellWell (http://www.ispellwell.com/):\n \niSpellWell is an incredible spelling software program that helps children master English spelling words in a personal spelling bee format. It is a combination of advanced spelling software technology and proven teaching techniques resulting in a wonderful spelling game that will entertain your child for hours.\n \n \nMemoryLifter (http://www.memorylifter.com/):\n \nThis is a memory-building flashcard program for vocabulary memorization and language skills. MemoryLifter is the ultimate education software using automated flash cards with multimedia support to enhance learning.\n \n \nWinFlash Educator (http://www.openwindow.com/)\n \nThis is a flashcard study and testing system that allows you to create reusable subject decks, print flashcards, study and track your progress. It includes multimedia support for graphics, audio and video, and you can import data from existing sources or export to your PDA.\n\n"
    author: "Cronopios"
  - subject: "Already many flashcards based applications"
    date: 2004-02-10
    body: "We already have FlashKard in kdeedu and there are 2 other projects that look similar to the ideas you submitted:\nKWordQuiz (will move to kdenonbeta)\nhttp://peterandlinda.com/kwordquiz\nand \nKMemAid\nhttp://memaid.sourceforge.net/\nso I don't think there is the need for more, it's better to improve existing programs.\nYou can find here (http://women.kde.org/projects/) some ideas for new programs and you can also send me any idea so it'll be listed here.\n\nannma\n"
    author: "annma"
  - subject: "Re: OKay, I need ideas"
    date: 2004-02-10
    body: "A program that teaches you how to sight-read music would be cool.  Or maybe a program to teach you perfect/relative pitch."
    author: "Navindra Umanee"
  - subject: "Re: OKay, I need ideas"
    date: 2004-02-12
    body: "Awesome idea Navindra! You won't mind sharing, will you?"
    author: "John Hughes"
---
<a href="http://qtforum.org/">QtForum.org</a>, the independent Qt community Website launched October 2003,
today announced the <a href="http://contest.qtforum.org/">QtForum.org Developer Contest</a>. The subject for the
contest is edutainment. This QtForum.org Developer Contest is sponsored
by <a href="http://trolltech.com/">Trolltech</a> as a part of the company's support of the open source community.



<!--break-->
<p>The first prize for the QtForum.org Developer Contest is USD 1500, and
the second prize is USD 750. Two third prizes, the Judge's Choice Prize
and the Community Choice Prize, will each be awarded USD 500.</p>

<p>"QtForum.org is responding to the need for a developer community for the
growing number of Qt developers out there," says QtForum.org's Community
Manager Christian Kienle. "The QtForum.org contest aims to bring Qt
developers together and challenge them to be creative with Qt in the
field of edutainment."<p>

<p>Eligible contest entries can be any English language version of an
original "edutainment" software application developed using Qt versions
3.2.3 or later. "Edutainment" software is software written to educate
the users while it entertains them. The software must be licensed under
an Open Source license.</p>

<p>The contest winners will be announced on the QtForum.org and Trolltech
Websites.</p>

<p>"We are very excited about the open source community's initiative to
establish an independent Qt community Website," said Matthias Ettrich,
Trolltech's Director of Software Development Desktop Platforms. "The
QtForum.org Edutainment Contest will help boost this excellent
initiative and we are looking forward to seeing many new Qt based
applications as a result."</p>

<p>The contest is open to individuals who are independent software
developers or to companies which are software developers. Developers may enter works created individually or in collaboration with
other eligible contestants. The deadline for submitting contest entries
is May 31.</p>
