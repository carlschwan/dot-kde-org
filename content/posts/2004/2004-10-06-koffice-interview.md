---
title: "KOffice Interview"
date:    2004-10-06
authors:
  - "fmous"
slug:    koffice-interview
comments:
  - subject: "interlingua"
    date: 2004-10-06
    body: "/* The code for the MS Word converter in OpenOffice.org is impossible to get into, especially for a non-german speaker (all the comments are in German!)*/\n\nSame applies for me when I try to read english."
    author: "gerd"
  - subject: "Re: interlingua"
    date: 2004-10-06
    body: "Well dont u think that the user base of english and its power as a universal language is much greater than german ?"
    author: "Sarath"
  - subject: "Re: interlingua"
    date: 2004-10-06
    body: "Not really. Most developers don't use english as their primary language. You can observe this at the KDE documentation. It is difficult to write larger texts in a foreign language. So I believe at least documentation shall be written in a native language first and then translated.\n\nEnglish is no universal language. Not with regard to quantity of speakers (hindi, chinese), not with regard to unification as most english speking countries speak non-standard English.\n\nIt is a western language which benefits from its simple structure but it also depends on western thinking. The idea of \"Universal languages\" is a misconception.\n\nBTW.. Many, many developers speak German very well, french shall learn it in school. Not every developer codes in C++ and so we don't have to rely on English.  Please keep in mind that the English language barrier also inhibits participation, makes us as non-native speakers play the second fiddle. I don't see a reason why you should not document your own code in your own language. Stardivision was a German company, so they did it in German. It was not their fault but it is your problem now, get a translation then. When I go to Shanghai and do not find my way around I cannot blame it on the English skills of the Chinese. We don't need language unification in the real world, so we don't on the internet. Variety makes us strong. Using English decreases my productivity."
    author: "gerd"
  - subject: "Re: interlingua"
    date: 2004-10-06
    body: "Half agreed.\n\nWell, it's universal enough concerning comunication between me and you. For me, brazillian, understand your point of view.\n\nIf you had wrote in German, you could have talked about universal languages, the next soccer world cup taking place in Germany, or anything else that I would feel the same about it: nothing.\n\n[]'s\nMarcos"
    author: "Marcos Tolentino"
  - subject: "Re: interlingua"
    date: 2004-10-06
    body: "I'm sorry but I don't agree! Programming and documentation should be done in English. The major quantity of code is in English and thus why to use it. \n\nAt least we Europeans should as much as possible stick to English!\n\nIMHO\n\n/DiCkE of Sweden    "
    author: "DiCkE"
  - subject: "Re: interlingua"
    date: 2004-10-06
    body: "This is really the most stupid thing I've ever heard.\nIf a language like English didn't exist everybody\nhere in Europe would hardly be able to communicate with\neach other - like it or not. \nAnd it's a good choice as a common language.\nIt uses very few letters (only A-Z) is quite short\nhas an extremely simple sentence structure (most of the\ntime the words must have a predictable order) and\nno articles (only \"the\"), no complicated cases\nand suffixes, no agglutinations, etc. The only thing\nwhich is a bit difficult is the arbitrary spelling\nbut you can't have it all ;-)\nIt's definitely one of the easiest languages\nin the world (even in absolute scientific terms). If you\ndon't believe me - look it up on the web.\nPrograms (i.e. software) can\neasily move across borders thanks to the Internet.\nWriting comments just in German make them impossible\nto read for everyone else - simply foolish IMO.\nAnd comments are not poems: They can be written quite\neasily even if you do not know the language very well.\nIt's just egoism and laziness that people do not want\nto write in English.\n\n"
    author: "Martin"
  - subject: "Re: interlingua"
    date: 2004-10-06
    body: "'the' is an article? Oh crap! I got that wrong on my grammer test yesterday..."
    author: "Corbin"
  - subject: "Re: interlingua"
    date: 2004-10-07
    body: "Yep, it is. But Martin forgot to mention that a/an is also an article.\nI think there aren't anymore, though. In latin languages there are dozens of those."
    author: "blacksheep"
  - subject: "Re: interlingua"
    date: 2004-10-08
    body: "\"Some\" is also sometimes considered an article."
    author: "Anonymous"
  - subject: "Re: interlingua"
    date: 2004-10-07
    body: "As someone who only speaks English this is fascinating to read. Okay, I can speak a little Spanish and order a kabap in Germany. ;-) People frequently apologize for their poor English and I think it silly because I would have no hope talking to them in so many native toungues. However my recent trip to Germany was very eye opening. Yeah it was fun and there were a lot of great people... but walking down the street, me being from the US, with people from Romania, Poland, Germany, Austria, Brazil and other places the way everybody communicated was in English. And everyone seemed to do pretty good with it.\n\nAt one point during the conference I overheard someone talking about two people from the same country where one of them had such a thick local accent the other preferred speaking in English with him.\n\nI can only say it is really weird. I would never be so pretentious to tell people to speak English and when I was young I was told Spanish was a better candidate due to how much of the world geography spoke it and how easy it was. However the internet originated in the US and English is the default language of the internet, though localization is great. \n\nSo I think the argument for communication among developers in English is certainly reasonable, but I like it better when someone from a non English speaking country in Europe makes the case because it is without bias. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: interlingua"
    date: 2004-10-07
    body: "Yes, I agree that English is the best language to be used here. But I hope you can also understand how annoying it is that you have these uber-people walking around that have English as their \"native language\" and that at any given meeting will call the shots as they simply respond quicker. On top of that, their English might be just as far of from standard English as the average German/Scandinavian in the meeting, but they are just talking a \"dialect\", while the German/Scandinavian speaks with an \"accent.\" Having a domain in which some other language is used is for many probably part of a dream of for once being the ones that can speak up with the others going half their usual speed."
    author: "Johannes Wilm"
  - subject: "Re: interlingua"
    date: 2004-10-08
    body: "THAT is the most stupid thing I've ever heard. If English would not have existed some other language (French for instance) would probably be the common language in Europe today. 300 years ago French was much more popular in Europe than today and English wasn't spoken at all on the continental part.\n\nAnd if you look at the Internet there are chances that Chinese might become the dominant language. Better book your Chinese class now!\n"
    author: "ac"
  - subject: "Re: interlingua"
    date: 2004-10-12
    body: "I don't think english is a so good choice. Few grammar ok but lots of exceptions. prononciation is really hard with no rules. And this langage is really well spoken by less than 6% of the world population. In fact it can only be well spoken by people for which it is the native langage. I learnt english in school for several years and my skill is still really poor (as you may already have seen).\nFrench or spanish are not better.\nWhat we need for a langage is a langage which is:\n- easy to learn\n- neutral. Today english native speakers are favorized.\n- powerfull\n\nAnd the good news is that such a langage exists. It's the Esperanto. see this really interesting link: http://www.geocities.com/c_piron/\n\nThis langage is spoken by 2 million peoples in the world (in Europe, china, Africa...), and is by far easier to learn.\n\n"
    author: "ndesmoul"
  - subject: "Re: interlingua"
    date: 2004-10-06
    body: "You are DREAMING, buddy.\n\nEnglish is the only way to go when you talk about a language with reach.\nIndian developers speak English, and so do most German developers."
    author: "Joe"
  - subject: "Re: interlingua"
    date: 2004-10-07
    body: "The problem is a foreign language slows me down and limits my capacity to work. So saying everybody shall use english is as stupid as saying everybody shall use Windows. Writers of literature also write in their own language. The number of english native speakers for review is a bottleneck.\n"
    author: "gerd"
  - subject: "Re: interlingua"
    date: 2004-10-07
    body: "And the problem when you are not using english is that many other peoples are slowed down with their work."
    author: "Daniel"
  - subject: "Re: interlingua"
    date: 2004-10-07
    body: "your fault gerd is wright, I'm happy he's coding and the ones who want to use his code should be able to undertand it. To bad they had bad education and only speak English and C#. It's only a matter of time before Spanisch will overtrown English as the most spoken language in the USA.\nEverybuddy has the wright to write in his own language. I'm glad he document his code at least. Shall I go on in Dutch? \nEnglish sucks, look at what it does to the leadersof the  governments of England and the USA. lol"
    author: "Guy Forssman"
  - subject: "Re: interlingua"
    date: 2004-10-08
    body: "well i live in holland speak dutch read german\ncan speak bits of spanish grok french and \nwould love to learn korean and japanese\nbut still as a programmer. \n\ndang it. \nWRITE IN ENGLISH\n\nyou see the japanese using japanese in there code????\n(no, we're *not* talking about comments)"
    author: "lypie"
  - subject: "Re: interlingua"
    date: 2004-11-17
    body: "With all the time you put in learning these languages, I bet you don't find the time to code anymore.\nIf you do fine, but I guess you have to agree that the more time you put in learning to speak French, the less time you have to code.\nThere are only 24 houres a day.\n\n\nI'm glad he's coding.\nI learned that sombody who's goog in Math is less good in languages. Generally speaking this is true. So coding is a lot like math, seems to me he may put all his effort in trying to write better code and skip learning french or dutch or whatever language.\n\nGuy Forssman\n"
    author: "Guy Forssman"
  - subject: "Re: interlingua"
    date: 2004-10-07
    body: "<em>It is a western language which benefits from its simple structure but it also depends on western thinking. The idea of \"Universal languages\" is a misconception.</em>\n\nFair nuff, though IMHO any universal language starts with the concept that only something with a penis or vagina (or their functional equivalents) gets a gender."
    author: "Anonymous Wanker"
  - subject: "Re: interlingua"
    date: 2004-10-07
    body: "\n<i>English is no universal language.</i>\n\n\nDream on, Gerd.\n\nMy native language is Spanish, I understand Italian and some French, but the only language that works all over the world is English.\n\nGerman may be easier for you, but you're cutting yourself off from a vast majority of possible contributors by not communicating in English.\n\nLike it or not, is the <i>lingua franca</i> of the age. get over it.\n\n\nCheers,\n\nCarlos Cesar"
    author: "Carlos Cesar"
  - subject: "Re: interlingua"
    date: 2004-10-07
    body: "In which language are we discussing? *Enough said*\n\n> french shall learn it in school\nUh? Not all French speak German! Many choose Spanish instead (as I did)..\n\n> get a translation then\nOK, are you paying for it?\n\nI work for an international company, in which language do you think we do *all* our documentation? English of course..\n\nVery few Chinese know a foreign language true but when I went to Shangai which language did I use with the engineers, hotel's staff?\n\nYou know acting like an ostrich doesn't help.. The truth is: if a codebase is in English, it is far easier to work on it as an international OSS project, if you don't do this, you restrict quite a lot the number of possibles contributors..\n"
    author: "RenoX"
  - subject: "Re: interlingua: ***help offered***"
    date: 2004-10-08
    body: "One of the main reasons to use openoffice instead of Koffice is other people working with MS-office can't read the KOffice-formats. So their are two solutions:\n1 Wait till microsoft supports KOffice / OASIS formats, which is the best, but whe should stay real, this probably won't happen in the near future.\n2 Try to let KOffice be able to export to those Microsoft-formats, hopefully whithout breaking any copyright and getting MS-lawyers on your neck (or back? Don't know how to say that in English!)\n\nSo, I think, instead of arguing about this language question, we could use our time better searching for someone to translate the German thing to English. If somebody sends me the code for converting those openoffice filters, which convert to and from those damn (excuse me) MS formats, I'd like to try to translate it to English. I'm only a beginning C++ programmer, so I'm afraid I can't do much about the 'messy' openoffice-source. But, hey, since the source is open, so are the German comments if I'm right, and if I can help the KOffice project translating those German comments to English (Or dutch, which is easer for me), I'd be glad to do so! (I'm unemployed due to health problems, so I've got plenty time).\nPlease send those german comments to sludink at hotmail dot com, and mention KOffice in the subject."
    author: "Ghans"
  - subject: "Re: interlingua"
    date: 2004-10-09
    body: "Lets resolve this issue like this, lets write comments in our own native languages :) I'm a Sinhalese, I'll write comments in Sinhala, our fellow Indians will write in Hindi and Tamil. Chinese will write in Chinese. That will make the world a better place...hahahaha"
    author: "Sagara"
  - subject: "Re: interlingua"
    date: 2004-10-06
    body: "well, you understood this part of the article, so it's not a problem for you to understand english text, but 90% won't understand german.\n\nbtw: nice work @KOffice, it's really my favorite office suite (apart from using LaTeX sometimes)"
    author: "Martin Stubenschrott"
  - subject: "Re: interlingua"
    date: 2004-10-07
    body: "David was not saying the comments were insurmountable, just an inconvenience on top of very complex code. I never studied German, but its amazing how far guesswork and a general interest in words can take you!\n\nI know, since I did the work. "
    author: "Shaheed"
  - subject: "KOffice"
    date: 2004-10-06
    body: "I definitely think there is value in having KOffice. I like the look, speed and light feel of it. I don't trust Sun Microsystems and think it is good having multiple office programs. I don't think the open source community should put all its eggs in the OOo basket. I think it would be good if Abiword and Gnumeric also went to the OASIS format."
    author: "Paul Vandenberg"
  - subject: "Re: KOffice"
    date: 2004-10-06
    body: "I sometimes wonder whether OpenOffice isn't more a poison pill than a gift to the free software world. For starters, it has had an undeniable impact on the development of KOffice and Gnome office, and a detrimental one, I think. For many, it's as if the availability of OpenOffice made it seem as if it were futile for them to do their own thing.\n\nThen again, more and more of OpenOffice is written in Java. And not a kind of Java that can ever be compatible with free java compilers, interpreters or class libraries because com.sun.* classes are used liberally.\n\nMoreover, OpenOffice is nearly impossible to build, not just for mere mortals like me, but it appears that the only builds of OpenOffice 2.0 beta's are coming from the old StarOffice headquarters in Hamburg where they apparently have squirreled away the only one who knows how to build everything into one package.\n\nAnd finally, OpenOffice isn't all that good, actually. Not if you download it from OpenOffice.org. I must admit that SuSE has done a great job of packaging OpenOffice, but when you take a vanilla OpenOffice, no matter whether for Linux or for KDE, it'll be barely usable. In the space of one week, I've had it crash on old .sdw files, completely mess up the layout of a very simple document merely by converting it to Word and back a few times and lose all track of its paragraph styles.\n\nKOffice has its problems -- bugs, unimplemented features, code duplication (working on that, though, soon Kivio, Karbon and Krita will use the same docker implementation, and I feel the urge to make sure they use the same colour and  layer docker tabs are used in all three, too.) -- but it can be built, and built using nothing but free software, too."
    author: "Boudewijn Rempt"
  - subject: "Re: KOffice"
    date: 2004-10-06
    body: "It is also a pitty that Sun does not release the Lighthouse Office suite as open source. The Lighthouse office suite was a very good office suite for NextStep and it could be used with GnuStep. I think the reason for Sun not releasing Lighthouse is, that they want people to use OpenOffice or StarOffice."
    author: "Michael Thaler"
  - subject: "Re: KOffice"
    date: 2004-10-06
    body: "Hadn't they already put Lighthouse in statis before StarOffice came into the picture? I know that I quite often check GNUStep out -- there's the rudiments of a very flexible paint app, BluTulip for GNUStep, and I'd like to see Lighthouse in real life, too."
    author: "Boudewijn Rempt"
  - subject: "Re: KOffice"
    date: 2004-10-07
    body: "OOo is written in C++.\n\n*Some* features, such as the DB component and reports require java.\n\nBut you can make extensions in Java, Python, Javascript and StarBasic."
    author: "Rodro"
  - subject: "Re: KOffice"
    date: 2004-10-07
    body: "And amount of Java code is increasing. It's irrelevant that you can extend StarOffice in whichever language, the basic set requires Java, and the people who used Java were evil enough to use internal sun classes, giving up the hope of ever truly freeing OpenOffice."
    author: "Boudewijn Rempt"
  - subject: "Re: KOffice"
    date: 2004-10-07
    body: "The one thing I miss is a filter collection which does not depend on KOffice or OpenOffice. It's a large duplication of code and a waste of developer resources that every application which uses the OASIS format has to develop it's own filters.\n\nImagine KOffice, OpenOffice and maybe later also Abiword/Gnumeric would use the same filters. As a result there would be much better filters for all. "
    author: "Sven Langkamp"
  - subject: "Re: KOffice"
    date: 2004-10-08
    body: "This is what we had in mind when we (Werner and I) started libwv2 on sourceforge. The goal was to share (at least with Abiword) a common Word-import filter. However the abiword developers (who wrote libwv1, and who we expected to join the libwv2 effort) never joined, so libwv2 is still used by KWord only and is unfinished..."
    author: "David Faure"
  - subject: "DCOP and testing"
    date: 2004-10-06
    body: "Hi,\n\nwell, actually my (following) idea applies to all applications, not just KOffice / KWord:\n\nActually somebody would have to sit down and work on this: If the developers strictly follow the idea that every graphical user element should also be accessible via DCOP, we could automate quality ensurance:\n\nWe would only have to set up usage paths through the applications and replay these paths again and again with every compilation stage. Errors would come up automatically and our software quality would be unbeatable. Further more, this would increase the \"manageability\" of software. Which is a concept that is by far underdeveloped in the Windows world. \n\nSuch a replay application - which would be able to monitor application performance from a user perspective - could report these statistics to an snmp agent like ucd-snmpd and enable the setup of automated software performance management stations in the sense of service management. \n\nEspecially those highly complex applications like the KOffice series with their high share of graphical stuff will have to consider this...\n\nMarkus"
    author: "Markus Heller"
  - subject: "Re: DCOP and testing"
    date: 2004-10-06
    body: "Great idea! What is stopping you from turning it in reality?"
    author: "Waldo Bastian"
  - subject: "Re: DCOP and testing"
    date: 2004-10-06
    body: "I'm already trying out things in this direction... Especially towards automated Web access through DCOP (remote controlling konqueror). \n\nUnfortunately I don't have that much Qt and kdelibs programming experience, but I'm getting on. I guess I will be further in about half a year. :-)\n\nMarkus"
    author: "Markus Heller"
  - subject: "Re: DCOP and testing"
    date: 2004-10-06
    body: "> that every graphical user element should also be accessible via DCOP, we could automate quality ensurance\n\nWe can also do that without developing our own tool, simply by using KDExecutor.\n\nhttp://www.klaralvdalens-datakonsult.se/?page=products&sub=kdexecutor\n\nThe tool exists - it's just that nobody took the time to create testcases.\nFeel like giving a hand with that? :)"
    author: "David Faure"
  - subject: "Re: DCOP and testing"
    date: 2004-10-07
    body: "The problem with KDExecutor is that it isn't free. Okay, there's a \"free beer\" version, but not one that people can take apart and put back together, update as they develop KDE/Qt, fix bugs in, and so forth.\n\nDefining test cases now would be useful, I agree, but it should be done with the aim of being able to replay them from a free-free executor. :-)"
    author: "Paul Walker"
  - subject: "Re: DCOP and testing"
    date: 2004-10-06
    body: "As far as I understood Harald's speech at the aKademy the accessibility implementation in the upcoming Qt 4 will give you just that through DBUS, access to all widgets, commands/actions and hints including hierarchy. Should be useful for both alternative interfaces (for accessibility) as well as testing (what you mentioned, but also for let's say computer admins checking if the computer is locked down enough using kiosk)."
    author: "Datschge"
  - subject: "Re: DCOP and testing"
    date: 2004-10-07
    body: "Or automatically generate screenshots for documentation in all supported languages"
    author: "didi"
  - subject: "Features?"
    date: 2004-10-06
    body: "In the past week, I've made over two dozen documents, half of them fliers stuffed with complicated graphics, the other half letters and press releases, with KWord.\n\nI don't think I've used anything else in a couple years.  But I've also not seen much improvement.  Minor bugs that need work arounds (the worst being that overlapping graphics print in \"last inserted lays on top\" order) are slowly getting fixed.\n\nOf course, it pretty much does everything I need.  A bit of improvement in the frames menu and layout would be nice, but I can get it to do everything I need.\n\nWhere am I going with this?  I'm wondering if anybody has used/read about Scribe and knows what advantages it will bring and if it will alter how KWord works.  What changes will this shift in text engine bring?"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Features?"
    date: 2004-10-06
    body: "The discussion/decision hasn't happened yet, but here's what I have in my notes:\n\nIf we porting kotext/kword/kpresenter to Qt4's text engine, we gain:\n     Smaller amount of memory needed per paragraph.\n     Faster layouting (Lars says the current timer-based layouting won't be needed anymore)\n     Easier undo/redo support (but this works already, so...)\n     Brand new, stable, table support?\n     Support for floating images [does OO have that?] (to be extended to the inline-as-char feature we have now)\n     Future debugging is done by TT :)\n   To check:\n     Repaint only the changed paragraphs. Better: only the changed lines.\n     WYSIWYG?\n     Everything-in-one-text design, closer to the OASIS design; but might lead to trouble with frames.\n\nIf not, we need to work on\n - kerning and better WYSIWYG implementation\n - maybe using new QTextFormat (and forking collection class?) for paragraphs etc.\n - tables\n - anchoring\n\nI'll update that list once I look closer at TP2, this mostly comes from TP1."
    author: "David Faure"
  - subject: "Good to see your Face David :)"
    date: 2004-10-07
    body: "It's really good to see your face David Faure!!! I like you for your good nature!"
    author: "Asif Ali Rizwaan"
  - subject: "KOffice: a big promise"
    date: 2004-10-07
    body: "Since I met with KOffice at first, it has been a big promise for me. It seemed very attractive but I found difficulties if I wanted to use that for real tasks: bugs and lack of fetures.\nI would note, that the imperfect solution of an otherwise very minor thing can prevent the normal usage of a complex object. For example: if the seat in a car is not fixed properly to its base, its swing totally destroys your enjoy of an otherwise good car. And that are only a few screws. \nMy experience with Kword and Kspread is disturbing. I like them, but mostly I cannot use them (OOp is the opposite: I can use, however, I avoid if possible). Both have long buglists (with 2-4-years-old bugs as well). It seems the lack of a solid developer group prevents them to be real hitting applications.\nAnyhow, it's not a news. The history of Koffice is full of complains about the very few developers. I don't know the reason, KDE attracts developers quite well, while KOffice not at all. Perhaps this is the most important point to be changed concerning KOffice.\nI wish a nicer future to KOffice. "
    author: "devians"
  - subject: "Re: KOffice: a big promise"
    date: 2004-10-07
    body: "The main problem with developing an office suite as a volunteer effort, as opposed to either buying existing source and opening that up (OO) or buying developers (MS-Office), is that many developers hardly use office software to any great extent. It's not that often that I have to write something that needs a word processor; all design documents at our company are docbook, for instance. If I use a spreadsheet it's for making simple lists. Emacs does that, too. I never use a database, I never generate reports. I seldom have to do a presentation, but from the big four apps, presentation software is what I use most often. When I seldom have a need for a word processor, spreadsheet or presentation app, I don't feel the need to hack on those.\n\nBut KOffice also has a diagrammer, a vector paint app and a paint app. I need a diagrammer really often. I use Kivio almost daily for my work, and Krita and Karbon are apps I use for fun -- and while I started working on Krita, I'm currently touching Kivio and Karbon, too. And I don't need to hack on KPresenter, because that app already does everything _I_ want from it.\n\nAs for features: KOffice not only has relatively few developers, the developers that do work on KOffice have had to spend a whole lot of time on something that's not visible as features or bug fixes, namely moving to the Oasis file format.\n\nHowever, judging by activity on koffice-devel, there's active development on many KOffice compenents again. What's still sorely missing is someone who really wants to get their hands dirty on KWord. And it's such a nice, interesting application... An editor on steroids, and which developer doesn't want to work on an editor?"
    author: "Boudewijn Rempt"
  - subject: "Re: KOffice: a big promise"
    date: 2004-10-07
    body: "You might probably right concerning the developer's attitude. I would likely accept it if I didn't see the OOo case. There is an active community around OOo. They somehow organize themself and provided a significant and continous improvement of OOo.\nI have the feeling if KOffice had the same human resource it would be light years ahead.\nThe question remains: what does OOo do better and how ?\n\n"
    author: "devians"
  - subject: "Re: KOffice: a big promise"
    date: 2004-10-07
    body: "They have a lot of paid, full-time developers to do the less interesting work."
    author: "Boudewijn Rempt"
  - subject: "Re: KOffice: a big promise"
    date: 2004-10-07
    body: "Last I checked, there was a total of 8 outside developers that have ever done anything at all meaningful to openoffice.  It's almost all done internally.\n"
    author: "JohnFlux"
  - subject: "Database standardization too"
    date: 2004-10-07
    body: "It is great to have more than one free office suite\ngoing for full OASIS support. I hope that a\nstandard is also achieved for little, portable databases,\nas described in(http://www.linuxjournal.com/article.php?sid=7800)\n\"The Lack of the Small Unified Database\"\n\nThanks for KOffice,\nMarco Fioretti"
    author: "Marco Fioretti"
  - subject: "KOffice versus OOo"
    date: 2004-10-07
    body: "Let me say first: keep up the good work. When I need to do something in an office suite, KOffice is my first choice. Why? Because it's lean and mean and everytime I use it -- which is not frequently, because I do my job almost entirely with TeX -- I'm surprised how much one is actually able to do with it. When do I use OOo? Exactly, when I need a *convertor* from or to MSWord. My thesis is: OOo is so popular because it has decent conversion from and to MSWord. That's great, of course, I couldn't do without it, but it should not make people forget that this does not make it a great office in itself. My biggest wish in KWord is the ability to set the grid (raster in Dutch) to 0pt so that you can position your frames in an absolute way. The move to OASIS is painstaking but good. Note that this means that with TeX4ht, you will have a nice convertor from LaTeX to KOffice/OOo---yes, I came up with that idea, again because basically, it meant LaTeX to Word support.\n\nSo, there are users, yes, and they like it very much. My wife wouldn't have an office on her old Pentium I laptop if there was no KOffice. And with OOo, she always had questions, but with KOffice, she doesn't ask anything!\n"
    author: "Maarten Wisse"
  - subject: "Why not intergrate kivio into karbon14"
    date: 2004-10-08
    body: "A college of my uses visio a lot and has lots of troubles with it, colors changes, crashes etc. He uses it to create pictures, group them and then drag these to the sencel bar. It then becomes a new stencel, which he then uses as design templates. Otherway around is also possible, take a stencel, drag to document and de-group.\nAnyway, that was my experience with this tool :-). But koffice has not such a tool, one has to use two tools and how to make a new stencil is unclear. Wouldn't it make sense to integrate these two?"
    author: "koos"
  - subject: "just me"
    date: 2004-10-08
    body: "I use OOo in my law practice, and I like it.  I also use KDE as my desktop in SuSE Linux 9.1 Pro, and I like that.  I have an interesting little story about how it is important to have both.  I am producing a film called the Digital Tipping Point, which is about the cultural implications of the global shift to free libre open source software (FLOSS).  In the course of our travels, we have seen much more use of OOo than KDE.  So there's an advantage for OOo.  However, just the other day, I was downloading my kaddressbook to take with me to the OpenOffice.org Conference in Berlin, and I accidentally clicked on something, maybe it was the CSV file for the addressbook, and BOOM! up comes my data in the Kcalc spreadsheet program!  I had previously tried to open the CSV file with OOo, but somehow I must have botched it.  After this accident, I learned that I can now use CSV with OOo or Kcalc.  So as with most things software libre, it seems that this little story illustrates how cross fertilization can help members of the community.  In this case, I am a very simple end user with very limited computer skills, so I really appreciate being able to have a work around for stuff.  So thanks to the KDE developers for a great body of code! Christian Einfeldt, 415-351-1300 einfeldt@earthlink.net"
    author: "Christian Einfeldt"
---
A few weeks ago <A href="http://dot.kde.org/1095608979/">KOffice 1.3.3 was released</A>, the third bugfix release in the 1.3.x series. The people of <A href="http://www.golem.de/">Golem.de</A> <A href="http://www.golem.de/0408/33132.html">conducted an interview</A> with David Faure, maintainer of KWord and the KOffice libraries about KOffice. They have sent us the English version for publication on the KDE news website for your reading pleasure.


<!--break-->
<style type="text/css">
  
.imgboxrt{
border:1px dotted #000;
float:right;
margin-left:5px;
margin-right:10px;
}

.imgboxlft{
border:1px dotted #000;
float:left;
margin-left:10px;
margin-right:5px;
}
  
  </style>

  
<!--IMAGE BOX RIGHT -->
<div align="center" class="imgboxrt" style="width:260px;">
<p><a href="http://static.kdenews.org/fab/screenies/koffice/david_at_fosdem.jpg"><img src="http://static.kdenews.org/fab/screenies/koffice/david_at_fosdem_small.jpg" alt="Skype in action" /></a><br><em>David Faure, maintainer of KWord and KOffice libraries</em></p>
</div>
<!-- IMAGE BOX RIGHT-->      



<p><strong>KOffice has evolved over the last years. New software components were
added. Where do you see KOffice today and what are the main steps
KOffice has taken?</strong></p>

 
  <p>New KOffice components have indeed been developed recently:
  Kexi, the database application (somewhat similar to Access) is looking
  quite good already, and Krita (bitmap image editing) is under heavy development. The start of a project-management application (KPlato) needs more developers 
  though.</p>
  
  <p>I think this is the strength of KOffice compared to its open-source "competition":
  having developed a solid enough foundation (Qt "platform", kdelibs technologies, 
  and koffice architecture) to make it appealing to develop many office-suite components,
  as proved by the number of KOffice components existing already.</p>

  <p>The main step taken by KOffice since 1.1 has been to implement a common
  text engine for KWord and KPresenter based on Qt's "QRichText" engine.
  This is used up to the recent 1.3.2 release. I'm currently working on support
  for the Indic scripts in our version of that text engine.</p>

  <p>Since then, the work has been on reworking spell-checking completely,
  and the OASIS file format which we'll talk about later on.</p>

  <p align="center"><A href="http://static.kdenews.org/fab/screenies/koffice/kword.png"><IMG src="http://static.kdenews.org/fab/screenies/koffice/kword_small.png" alt="kword_small.png" width="250" height="249" border="0"></A><br><em>Kword</em></p>
  
<p><strong>Which parts of KOffice still require the most work?</strong></p>

  <p>An office suite is a huge thing to develop. Work is needed in almost every
  part of it, and it's hard to simply follow users's demands as everyone's 
  "must have" feature is a different one.
  More specifically, I can see that the immediate future is going to be: finishing
  the OASIS file format implementation and working on the document converters
  to make them use the OASIS format, then looking at whether to rewrite our
  text engine (as well as KWord and KPresenter) to be based on Qt4's new
  text engine (dubbed "Scribe"), which looks very promising.</p>

  <p>The main flaw in KOffice-1.3 is the WYSIWYG implementation, and the idea is
  that Qt4 will solve that problem (I've been assured that work is happening
  towards this).</p>
  
  <p>Another area where work is hopefully going to happen is scripting: there are
  talks about using KJSEmbed, the powerful and flexible scripting engine based
  on KJS (konqueror's javascript interpreter), for scripting in KOffice.
  But apart from that, entire applications require the most work: KFormula, 
  and KPlato, could definitely do with some new developers :)</p>

  <p align="center"><A href="http://static.kdenews.org/fab/screenies/koffice/karbon.png"><IMG src="http://static.kdenews.org/fab/screenies/koffice/karbon_small.png" width="250" height="200" border="0"></A><br><em>Karbon14</em></p>
  
  
<p><strong>You are working in the OASIS technical committee on the definition of
a standard file format for office suites. KOffice supports the file
format from OpenOffice.org that has been submitted to OASIS. Do you
consider this format a good base?</strong></p>

  <p>More than that, I made sure that this format would be a good base for KOffice :)
  That's what my job in the OASIS technical committee is: ensuring that the
  file format can be used to express everything that KOffice supports.
  But I definitely think the OpenOffice.org file format was a very good basis
  for the OASIS format, since it was designed, from the start, as a file format
  that should be as independent as possible from the design of the application.
  It reuses standards like XSL/FO, CSS, HTML etc. as much as possible, so
  the goal is to make the OASIS format another one of those formats, where
  the application used to edit the document doesn't matter. Realistically though,
  given the amount of features available in office suites, it is going to be 
  long way until all features are available in both office suites implementing 
  that file format (OpenOffice.org and KOffice, for the moment).</p>

  <p align="center"><A href="http://static.kdenews.org/fab/screenies/koffice/kivio.png"><IMG src="http://static.kdenews.org/fab/screenies/koffice/kivio_small.png" alt="kivio_small.png" width="250" height="224" border="0"></A><br><em>Kivio</em></p>
  
<p><strong>The biggest problem for office suites is the installed base of
Microsoft Office with lots of files in proprietary formats and
special software like macros for this system. How could KOffice
overcome this problem?</strong></p>

  <p>KOffice has some Microsoft Office document converters, although those
  could definitely use more work as well. But given the lack of manpower in
  KOffice, I don't think anyone is going to be able to make those converters
  100% complete, nor implement support for Microsoft's VBA macros.
  The target audience of KOffice seems to be mostly people creating new
  documents, rather than people trying to work on legacy MS Office documents :)
  In any case, the good thing about sharing the same native format as OpenOffice.org
  is that people will be able to use OpenOffice.org to transform their MSOffice
  documents into OASIS documents, editable by both OOo and KOffice :)
  Not that OOo's converters are 100% complete either, of course...</p>

  <p align="center"><A href="http://static.kdenews.org/fab/screenies/koffice/kpresenter.png"><IMG src="http://static.kdenews.org/fab/screenies/koffice/kpresenter_small.png" alt="kpresenter_small.png" width="249" height="167" border="0"></A><br>KPresenter</p>
  
<p><strong>When KOffice was released with KDE 2.0 there was no other open source
office suite for linux. But especially with OpenOffice.org there is
free solution that fits most peoples needs. What's the advantage of
KOffice and why is it nessecary to continue the development of this
software instead of joining forces with other projects like
OpenOffice.org?</strong></p>

  <p>I get this question all the time, obviously :)
  First, a number of users have assured us that they prefer KOffice.
  For performance reasons (speed and memory), for its integration with KDE,
  for its user-friendly user interface, and for its wider variety of components
 (OpenOffice.org doesn't even have an equivalent of Kivio or Kexi).</p>

  <p>KOffice also has a more flexible architecture, as proven by the 
  "KOffice Workspace" application which allows to use all KOffice components 
  together, by the possibility to view KOffice documents in Konqueror 
  (which also uses the KOffice KParts components directly), or by the fact that 
  its document converters are available from the command-line using "koconverter".</p>

  <p align="center"><A href="http://static.kdenews.org/fab/screenies/koffice/kexi.png"><IMG src="http://static.kdenews.org/fab/screenies/koffice/kexi_small.png" alt="kexi_small.png" width="249" height="174" border="0"></A><br><em>Kexi</em></p>
  
  <p>Without wanting to say bad things about the competition - I have much respect
  for the OpenOffice.org project - I also feel that KOffice's codebase is much 
  easier to get into, and somewhat more cleaner. The code for the MS Word converter
  in OpenOffice.org is impossible to get into, especially for a non-german speaker
  (all the comments are in German!). The build process and the internationalization
  process of OpenOffice.org also have bad reputation (especially with linux distributors),
  whereas KOffice uses the standard KDE (in fact gnu) mechanisms. It also seems
  Sun's control makes is difficult to change OOo too much, whereas developers 
  are (almost) completely free to do what they want when working on KOffice :)</p>

  <p>The presence of two competing office suites is also a good thing in the 
  grand scheme of things: if KOffice didn't exist, there would be no such thing as a
  "standard" OASIS file format for office suite documents. There would be something
  called as such, but how can something really be a "standard" until it's used by
  more than one application? Looking at the needs of two office suites is what 
  really made it possible to clean up the file format and make it somewhat 
  implementation-independent. Of course if any other office suite wanted to switch
  to OASIS as the native file format we might have to extend the format some more,
  but at least we can say it's generic enough to fit two office suites already,
  that's a good first step.</p>

<p align="center"><A href="http://static.kdenews.org/fab/screenies/koffice/krita.png"><IMG src="http://static.kdenews.org/fab/screenies/koffice/krita_small.png" alt="krita_small.png" width="250" height="198" border="0"></A><br><em>Krita</em></p>

<p><strong>Would you welcome a better integration of OpenOffice.org in KDE?</strong></p>

  <p>Sure, why not. It makes OpenOffice.org KDE users happy.
  Doesn't change much for KOffice users though :-)</p>
