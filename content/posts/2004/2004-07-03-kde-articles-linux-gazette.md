---
title: "KDE Articles from Linux Gazette"
date:    2004-07-03
authors:
  - "jo'regan"
slug:    kde-articles-linux-gazette
comments:
  - subject: "Good stuff"
    date: 2004-07-03
    body: "Thanx for the intro on khotkeys. I was wondering how best to accomplish this.\n\n\nNow, I am still trying to get irkick to work (mandrake 10 with haupauge card). Just does not seem to want to work. Anybody able to do that?"
    author: "a.c."
  - subject: "Re: Good stuff"
    date: 2004-07-04
    body: "I was just able to get irkick to work over this past week.  I've got to tell you it's pretty great.\n\nIt was with one of those cheap-o packard bell receievers, tho.  \n\nI ended up downloading the sources of lirc .7 snapshot, so it would work with the 2.6.X kernel.  \n\nIt's soooo cool controlling your desktop with a remote, really it is!\n\nThe next thing to do is make it convenient to browse through my TV show collection from my bed.  mmmmm. maybe mythtv.... whateva\n\n//standsolid//"
    author: "standsolid"
  - subject: "Tips for KHotKeys?"
    date: 2004-07-04
    body: "Good stuff this KHotKeys. I have one question; is it possible to define more actions to one key based on the current status of the PC. Let me give an example;\n\nNo programms are open, I press play/pause button, Juk opens and start playing a song\nJuk is playing a song, I press play/pause button, Juk is pausing a song\nJuk is in paused mode, I press play/pause button, Juk continues a song\nJuk is in stopped mode, I press play/pause button,  Juk start playing a song\n\nI don't think KHotKeys is easy to use, for example adding volume de/increase to the buttons I need to browse with KDCOP and find the correct entry.\n\nI'm no usabillity engineer, so I don't really have a great idea how ot improve usabillity. Maybe adding some good templates?"
    author: "AC"
  - subject: "Re: Tips for KHotKeys?"
    date: 2004-07-04
    body: "I don't have Juk installed, so I can't give you a complete answer, but what you need to do is to create a script, and assign the hotkey to that.\n\n\n#!/bin/bash\nif [ -n \"ps aux | grep mozilla | grep -v 'grep mozilla'\" ]\nthen\n#Juk is running\n    dcop juk Player play\nelse\n    juk \n    dcop juk Player play\nfi\n\nJuk more than likely has a way of returning its status, so you should be able to do what you want from a single script."
    author: "Jimmy O'Regan"
  - subject: "Re: Tips for KHotKeys?"
    date: 2004-07-04
    body: "Sorry, that should (of course) have been:\n\n#!/bin/bash\nif [ -n \"ps aux | grep juk | grep -v 'grep juk'\" ]\nthen\n#Juk is running\ndcop juk Player play\nelse\njuk\ndcop juk Player play\nfi\n\nI was pasting from a recent discussion on Linux Gazette's Answer Gang. If you post your query there (tagATlinuxgazette.net), Tom might get back to you with an answer. (I've mailed him, pointing here, so he might drop by and answer, but it's best to double your chances :)"
    author: "Jimmy O'Regan"
  - subject: "Re: Tips for KHotKeys?"
    date: 2004-07-04
    body: "OK, I installed Juk, and here's your script. Save it somewhere as 'juk.sh'\n\n#!/bin/bash\n\nif [ -z \"$(ps aux | grep juk | grep -v 'grep juk' | grep -v 'juk.sh')\" ]\nthen\n        /usr/bin/juk &\n        sleep 10\n        # we need to sleep while Juk initialises DCOP\nfi\n\nif [ \"$(dcop juk Player playingString)\" == \"No song playing\" ]\nthen\n        dcop juk Player startPlayingPlaylist\nelse\n        dcop juk Player playPause\nfi"
    author: "Jimmy O'Regan"
  - subject: "Re: Tips for KHotKeys?"
    date: 2004-07-06
    body: "Looks good, but one comment...\nYou don't actually need to make a script for this part of it:\n> if [ -z \"$(ps aux | grep juk | grep -v 'grep juk' | grep -v 'juk.sh')\" ]\n\nKHotkeys actually has the ability to be pretty smart in how you specify things, so you can tell it to only use a hotkey if a particular app is running or if that app has focus.  For example, the group of hotkeys \"Konqueror gestures\" that come with KDE includes the condition that Active window is Konqueror.  You could make a \"JuK\" group that contains the condition \"Existing window\" is JuK.\n\nThe script works fine, but just thought I'd point that out."
    author: "Ari"
  - subject: "Re: Tips for KHotKeys?"
    date: 2004-07-06
    body: "Well, the first part of the original question was \"No programms are open, I press play/pause button, Juk opens and start playing a song\", which is why I had that part. \n\nAlso, Ben Okopnik, the editor of LG, has pointed out that the line you highlighted could be better written as:\nif [ -z \"$(ps aux | grep [j]uk | grep -v 'juk.sh')\" ]"
    author: "Jimmy O'Regan"
  - subject: "Re: Tips for KHotKeys?"
    date: 2005-02-16
    body: "This is what I have writen\n\n#!/bin/bash\nif ! dcop juk Player playPause\nthen\njuk &\nsleep 2\ni=50;\nwhile (($i>0))\ndo\nif ! dcop juk Player playPause\n then\nsleep 1\nelse\n break;\nfi\n((i=$i-1))\ndone\nfi"
    author: "drc"
---
<a href="http://linuxgazette.net/104/index.html">Issue 104</a> of <a href="http://linuxgazette.net/">Linux Gazette</a> is out, and features two articles aimed at new users: <a href="http://linuxgazette.net/104/brown.html">Using Windows Keyboard Media Buttons In Linux</a> by Tom Brown discusses the use of KHotKeys, with a brief introduction to DCOP for full control of KDE; and my own article, <a href="http://linuxgazette.net/104/oregan3.html">Front and Back: KPGP and GPG</a>, introduces KGPG, while also showing the corresponding commands for those "stuck at a console" moments.


<!--break-->
