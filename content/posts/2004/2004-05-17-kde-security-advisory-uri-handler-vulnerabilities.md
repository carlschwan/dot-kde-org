---
title: "KDE Security Advisory: URI Handler Vulnerabilities"
date:    2004-05-17
authors:
  - "wbastian"
slug:    kde-security-advisory-uri-handler-vulnerabilities
comments:
  - subject: "SSH and rlogin fixed too."
    date: 2004-05-17
    body: "SSH and rlogin URI handling vulnerabilities are fixed too.\n\nI didn't see anything about the impact of malicious ssh and rlogin URIs so I looked at the patches and didn't see anything about them there either.\n\nHowever, ktelnetservice is also responsible for handling these URIs so the patch which fixes telnet: also fixes ssh: and rlogin:."
    author: "Will Stephenson"
  - subject: "Re: SSH and rlogin fixed too."
    date: 2004-05-17
    body: "> SSH and rlogin URI handling vulnerabilities are fixed too.\n\nWhat news! Did you actually read the advisory/story?"
    author: "Anonymous"
  - subject: "hey"
    date: 2004-05-17
    body: "I've just notice on the kde.org home page:\n\" KDE is a powerful \"Free Software\" graphical desktop environment for Linux and Unix workstations.\"\n\nit used to be \"Opensource\" instead of \"free software\", right? when did that change happen ? :)"
    author: "Pat"
  - subject: "Re: hey"
    date: 2004-05-17
    body: "instructions:\n\n1. go to http://lists.kde.org\n2. select kde-cvs entry\n3. search for \"free software index.php\"\n4. look at everything that starts with \"www\"\n\n(tip: the fifth entry looks interesting)\n"
    author: "anonymous"
  - subject: "Re: hey"
    date: 2004-05-17
    body: "The terms \"Open Source Software\" (OSS) and \"Free Software\" (FS) are synonyms (although some people will disagree). While the former stresses the fact that the source code is freely available the latter stresses the freedom to change and redistribute the software (sometimes under certain restrictions, cf. GPL).\n\nNowadays, many companies make the source code of some of their software available (i.e. it could be called \"open source software\", but of course not in the sense of \"our\" definition of OSS), but they disallow changes and redistribution (so those companies' software is definitely not FS). That's why I prefer the term \"Free Software\" even though, at least for me, there's no difference between OSS and FS.\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: hey"
    date: 2004-05-17
    body: "I believe that \"free software\" makes many think \"free as in beer\", while \"open source software\" means \"free as in thought\". I guess the internet other and advertisement channels has diminished the meaning of \"free\", at least in \"the free world\" ;)"
    author: "Anders"
  - subject: "Re: hey"
    date: 2004-05-17
    body: "I don't believe many think this as long its definition is linked. Also see http://www.gnu.org/philosophy/free-software-for-freedom.html for an extensive discussion."
    author: "Anonymous"
  - subject: "Re: hey"
    date: 2004-05-17
    body: "Well, we should just use the term Libre Software. Suppose some people may not know that word, but anyone interested could easily look it up so it would prevent any confusion. \"Libre\" is actually in the Oxford English Dictionary (it was used in the 16th century apparently, \"libre will\"), so it is almost English already.\n\nBecause I agree that Microsoft and other companies confuses the issue with their \"shared source\" software and the like. But between the two, OSS is less confusing then Free Software IMHO.\n\nWhat I don't like is FOSS or F/OSS that I've started to see just recently. It reminds me of floss."
    author: "Ian"
  - subject: "Re: hey"
    date: 2004-05-17
    body: "what's wrong with FLOSS (Free Libre Open Source Software) ?"
    author: "Pat"
  - subject: "Re: hey"
    date: 2004-05-17
    body: "How about: It's an acronym that expands to something completely ungrammatical?"
    author: "Roberto Alsina"
  - subject: "Re: hey"
    date: 2004-05-17
    body: "IAATETDCU?"
    author: "Richard Moore"
  - subject: "Re: hey"
    date: 2004-05-17
    body: "I think my irony meter just melted. Or: ITMIMJM!"
    author: "Roberto Alsina"
  - subject: "time until the fix is installed ?"
    date: 2004-05-17
    body: "i think no one will install the fix by hand and just wait for updated binary packages from their vendors.\nAll in all i think the vulnerability is longer visible to the world as in microsoft windows.\n\nthis is not a troll post, what so you think ?\nare there possibilities to update in binary format with binary patches,\nwith something called ksecurityupdate ?\n\nor waht about something like MSBA (microsoft Security Basline Analyser) which tells you what flaws are discoverd in your OS (KDE).\n\n\nchris"
    author: "chris"
  - subject: "Re: time until the fix is installed ?"
    date: 2004-05-17
    body: "There can't be generic binary updates -- they would still have to come from your packager.  A binary build on Redhat 7.1 with GCC 2.96 won't be the same as one built on SuSE 9.1 with GCC 3.3, etc.\n\nMost distributions offer an update service for security issues (i.e. SuSE Online Update or Redhat Network)."
    author: "Scott Wheeler"
  - subject: "Re: time until the fix is installed ?"
    date: 2004-05-17
    body: "> All in all i think the vulnerability is longer visible to the world as in microsoft windows.\n\nMicrosoft windows is affected too by this? Microsoft fixes reported flaws after several months and then hold back available patches for up to an additional month. Shorter?"
    author: "Anonymous"
  - subject: "Re: time until the fix is installed ?"
    date: 2004-05-17
    body: "> i think no one will install the fix by hand and just wait for updated binary packages from their vendors.\n\nYou are probably right. \n\n> All in all i think the vulnerability is longer visible to the world as in microsoft windows.\n\nIn general we try to give the vendors/packagers some time to prepare binary packages before we announce. In this case the vulnerability was published already, so there was no point in waiting any longer. I'm sure the distributions will soon have updates available.\n\nCheers,\nWaldo\n"
    author: "Waldo Bastian"
  - subject: "Re: time until the fix is installed ?"
    date: 2004-05-19
    body: "Time When I noticed this flaw 05.00... Time when it was fixed 05.02 :-D\n\nHey duuudes it's not that difficult, just download patch, patch it up, recompile and install.... Done.\n\nThose \"Windowse And Distro Newbie users\" will always have to rely on MS, Feodora, Suse, you name it. Because they have better things to do, is it supposed to punish me because I at least now how to do it. I DO NOT want to wait several weeks before I get a new release and reinstall AAAALL the mumbo jumbo from start."
    author: "J.O.R"
  - subject: "Please don't copy Microsoft's mistakes!"
    date: 2004-05-18
    body: "To have only patches is terrible. \n\nHaving to upgrade to KDE 3.2.2 and then a patch is a needless waste of time, not everybody is having the newest version around. It reminds me of the terrible practice on the Windows platform (install service pack after service pack, then hotfixes)\n\nPlease always release a new version after critical upgrades, call it KDE 3.2.2b or 3.2.2.1 or whatever, but please:\n\nnever ever let the latest stable release contain critical bugs!\n\nNot everybody is reading those security bulletins, there is absolutely no need to keep re-introducing old bugs into the wild.\n\nExactly this problem is one of the main reasons why worms and bugs never die on Windows, let's not make the same mistake and make sure that nobody upgrading to (or installing for the first time) the latest KDE is needlessly getting bugs that have already been fixed.\n\nOtherwise a great job done and really fast reaction time!"
    author: "Roland"
  - subject: "Re: Please don't copy Microsoft's mistakes!"
    date: 2004-05-18
    body: "KDE only releases source code.\n\nhttp://www.kde.org/download/packagepolicy.php\n\nComplain to you vendor if you want updated packages, includiing the patches. This is not hard for them to do and is what most vendors _will_ be doing."
    author: "Chris Howells"
  - subject: "Re: Please don't copy Microsoft's mistakes!"
    date: 2004-05-18
    body: "I think you're wrong. If my vendor (Debian) doesn't provide KDE 3.2.2 I won't use it unless you provide binary packages.\n\nTo sum up: If you don't provide updates, people won't use your software. Ah, but you WANT the people to use your software, don't you?"
    author: "John"
  - subject: "Re: Please don't copy Microsoft's mistakes!"
    date: 2004-05-18
    body: "You can whine as much as you want: KDE has not the resources to create binary packages for every distribution and platform out there. End of discussion."
    author: "Anonymous"
  - subject: "Re: Please don't copy Microsoft's mistakes!"
    date: 2004-05-18
    body: "well when Autopackage hit's 1.0 and assuming it will be able to handle packages of the size of KDE ok, then we could maybe look into autopackage releases?\n\ni mean it is/will be distro agnostic."
    author: "lou"
  - subject: "Re: Please don't copy Microsoft's mistakes!"
    date: 2004-05-18
    body: "You can look into whatever you want, I doubt that there will be ever KDE official binary packages. And \"packages that will install on many different distros\" does not mean all and especially not platform agnostic."
    author: "Anonymous"
  - subject: "Re: Please don't copy Microsoft's mistakes!"
    date: 2004-05-18
    body: "Every responsible vendor will be releasing updated packages. If Debian does not, file a bug report in their bug tracking system and it will be dealt with.\n\nWe never have provided binary updates, only the source code patches, and I don't see this changing in the near future."
    author: "Chris Howells"
  - subject: "Re: Please don't copy Microsoft's mistakes!"
    date: 2004-05-18
    body: "\"KDE only releases source code.\"\n\nTrue, but:\n\n- I want to be able to know wether a machine is vulnerable or not. That's only possible if the patched stuff gets a new version number. Otherwise we end up in complete chaos like in Windows, where as soon as the number of machines is bigger than 3, nobody knows which machine got what patch.\n\n- Lots of people install KDE from source, why not offer them the latest in one package? What's the big advantage of hoping that they will also find the patches and apply them?\n\n- People make mistakes and packagers make mistakes. With no new version number how do I know that my installation was really patched by the packager? There is no way to know except try out an exploit, which can't be seriously be your suggestion or is it?\n\nIt's a matter of principle: It should be clear and obvious what is vulnerable and what isn't.\n\nMost OSS-projects (including Linux) follow that rule and release a new version whenever a critical update is needed. That way everybody (please note that often several admins are administrating several machines, and not everybody can remember who patched what) can check whether something is still vulnerable and upgrade if needed.\n\nAs soon as you bring in not-changing-version-number patches, the whole system falls apart. Some will forget that they have already patched and will needlessly patch again, others will forget that they have not patched some machines and will keep them on the net. Yes, in a perfect world everybody remembers all the patches he applies, but in a perfect world there wouldn't be any bugs that require a patch in the first place.\n\nAnd this system is one of the main reasons why security on Unix is easier to maintain and more importantly: OLD BUGS DON'T COME BACK.\n\nSo *NO*, this problem cannot be solved by the vendors, this can *ONLY* be solved by the KDE-team by swallowing the geek-pride and release a new version number.\n\nAlso, what's the big harm of releasing KDE 3.2.2.1 or 3.2.2b (of course that number has to be shown in kcontrol, otherwise it's pointless)? \n\n"
    author: "Roland"
  - subject: "Re: Please don't copy Microsoft's mistakes!"
    date: 2004-05-18
    body: "It's rather easy to tell if a machine has been updated, distributions change the package revision number when they release updated packages. 3.2.2-1 is unpatched and 3.2.2-2 is patched released. How do you know the patch was actually in that release? Check your distributions web page, they typically document security updates and will say \"3.2.2-2: Patches KDE 3.2.2 security hole.\" This isn't a new problem for distributions, they've had to deal with it from day one.\n\nOn the KDE side, going through the full process of getting a new release ready just for a small patch is over kill. They can release the patch now and roll it into the next release without too much worry."
    author: "Greg"
  - subject: "Re: Please don't copy Microsoft's mistakes!"
    date: 2004-05-18
    body: "KDE 3.2.3 will be released early June and will include all the latest patches."
    author: "Waldo Bastian"
  - subject: "hmmm.. looks somewhat familiar"
    date: 2004-05-18
    body: "this kinda reminds me of \n\nSomebox Login: -f root\n\n:-))\n\nSCNR"
    author: "anonymous"
  - subject: "Security"
    date: 2004-05-19
    body: "Is there a KDE security project? It would be very good to install a special review team..."
    author: "gerd"
  - subject: "Re: Security"
    date: 2004-05-19
    body: "Sure there is, see http://www.kde.org/info/security/policy.php"
    author: "Datschge"
  - subject: "for the love of god.."
    date: 2004-05-19
    body: "\nAccording to our beloved GNOME folks, Novell is implementing a desktop that will include OO.org, Evolution, Mozilla and (for the love of god, please no) MONO with applications (!). With the platform being SuSE, desktop beneath it may very well be KDE. Talk about a huge heavy mess, and there you have it.\n\nhttp://www.eweek.com/article2/0,1759,1594127,00.asp\n"
    author: "jmk"
  - subject: "Re: for the love of god.."
    date: 2004-05-23
    body: "Please don't try and start something here. If you look it isn't the Gnome folks who are saying this. As everyone should know by know 'they' have a track history of this sort of thing, and everyone should have learned to ignore it."
    author: "David"
  - subject: "Timeline issue"
    date: 2004-05-19
    body: "This strikes me as scary:\n\n02/04/2003 Exploit acquired by iDEFENSE\n12/05/2004 Public disclosure of Opera vulnerability\n13/05/2004 KDE Team informed by Martin Ostertag\n13/05/2004 Patches created\n\nThe same day the exploit was announced to the team patches were created. THhis does not surprise me. What *does* surprise me is the huge gap in time between when the exploit was aquired and the KDE team was informed.\n\nWhat is the purpose in waiting so long before informing KDE? Were they waiting for Opera? If so, why?"
    author: "Jason Keirstead"
  - subject: "Re: Timeline issue"
    date: 2004-05-19
    body: "The original reporter (iDEFENSE) doesn't know KDE or didn't examine it?"
    author: "Anonymous"
  - subject: "Re: Timeline issue"
    date: 2004-05-22
    body: "Is there a \"trusted\" person/group of people in the KDE development team, who could contact Opera, and ask to be included on their internal security alert announcements, to prevent things like this?"
    author: "Fred Emmott"
  - subject: "Re: Timeline issue"
    date: 2004-05-23
    body: "What really matters is that patches were created on the same day that the KDE people were informed. The delay for public disclosure was less than a month.\n\nThere is always a delay between the exploit being discovered and making it known to all the right people. No one wants to disclose a security problem straight away without having analysed it fully, looked at how it may be fixed and identified the relevant people to contact. That is just the way of things. Given that patches were created on the same day and vendors were informed within two, that shows that the whole system worked on this occasion.\n\nPeople make it sound as if something unspeakable is going to happen if there is any sort of delay in people being informed, or a fix being found - that's life. Anybody using this exploit for mailicious purposes will have to work out what they are going to do with it, and how they are going to attack a system. This is in stark contrast to Windows where the number of exploits that allow a malicious attack a free hand into the system (without anyone being tricked into anything) is unbelievable, so granted, a delay would be worse for people using Windows. You also have to couple this with the fact that if there is a security problem with Microsoft software the patch is sometimes your only way of remedying it - you can't just change one or two things or shut them off."
    author: "David"
  - subject: "Re: Timeline issue"
    date: 2004-05-24
    body: "Why should KDE have been informed?\n\nThe fact that there is a security leak in Opera does not automagicly mean that this leak is also present in KDE.\n\nAccording to your time line KDE responded in less then a day when the leak in the desktop was discovered/reported.\n\nIf people are worried about this delay, they could better worry about the fact that this leak is in KDE for more then 4 years..\n\nRinse"
    author: "anonymous"
  - subject: "Re: Timeline issue"
    date: 2004-05-24
    body: "I think the original vulnerability was discovered on OS/X, and the vendor notified was Apple.  I think Opera and KDE found it once the issue went public."
    author: "Anonymous"
  - subject: "Re: Timeline issue"
    date: 2004-05-24
    body: "So in about a year of so, there probably will come a patch voor WindowsXP as wel :)"
    author: "anonymous"
---
iDEFENSE
<a href="http://www.idefense.com/application/poi/display?id=104">
identified a vulnerability</a>
in the Opera Web Browser
that could allow remote attackers to create or truncate
arbitrary files. The KDE team has found that similar
vulnerabilities exists in KDE. 

The telnet, rlogin, ssh and mailto URI handlers in KDE do not
check for '-' at the beginning of the hostname passed, which
makes it possible to pass an option to the programs started
by the handlers.

<!--break-->
<p>
<pre>
KDE Security Advisory: URI Handler Vulnerabilities
Original Release Date: 2004-05-17
URL: <a href="http://www.kde.org/info/security/advisory-20040517-1.txt">http://www.kde.org/info/security/advisory-20040517-1.txt</a>

0. References

	<a href="http://www.idefense.com/application/poi/display?id=104">http://www.idefense.com/application/poi/display?id=104</a>
        <a href="http://cve.mitre.org/cgi-bin/cvename.cgi?name=CAN-2004-0411">http://cve.mitre.org/cgi-bin/cvename.cgi?name=CAN-2004-0411</a>
        <a href="http://www.securityfocus.com/archive/1/363225">http://www.securityfocus.com/archive/1/363225</a>

1. Systems affected:

        All versions of KDE up to KDE 3.2.2 inclusive. 


2. Overview:

        iDEFENSE identified a vulnerability in the Opera Web Browser
        that could allow remote attackers to create or truncate
        arbitrary files. The KDE team has found that similar
        vulnerabilities exists in KDE.

        The telnet, rlogin, ssh and mailto URI handlers in KDE do not
        check for '-' at the beginning of the hostname passed, which
        makes it possible to pass an option to the programs started
        by the handlers.

        The Common Vulnerabilities and Exposures project (cve.mitre.org)
        has assigned the name CAN-2004-0411 to this issue.


3. Impact:

        A remote attacker could entice a user to open a carefully crafted
        telnet URI which may either create or truncate a file anywhere 
        where the victim has permission to do so. In KDE 3.2 and later
        versions the user is first explicitly asked to confirm the opening
        of the telnet URI.

        A remote attacker could entice a user to open a carefully crafted
        mailto URI which may start the KMail program with its display 
        redirected to a remote machine under control of the attacker.
        An attacker can then use this to gain full access to the victims
        personal files and account.

        An attacker could entice a user to open a carefully crafted
        mailto URI which may start the KMail program using a configuration
        file specified by the attacker. If the attacker is able to install
        arbitrary files somewhere on the machine, the attacker can include
        commands in the configuration file which will be executed with the
        privileges of the victim allowing the attacker to gain full access
        to the victims personal files and account.

4. Solution:

        Source code patches have been made available which fix these
        vulnerabilities. Contact your OS vendor / binary package provider
        for information about how to obtain updated binary packages.


5. Patch:

        Patches for KDE 3.0.5b are available from
        <a href="ftp://ftp.kde.org/pub/kde/security_patches">ftp://ftp.kde.org/pub/kde/security_patches</a> : 

  5c573853ec3f426d33c559958baa2169  post-3.0.5b-kdelibs-kapplication.patch
  eaf9237b3af56b3b01df966b13fe2714  post-3.0.5b-kdelibs-ktelnetservice.patch

        Patches for KDE 3.1.5 are available from
        <a href="ftp://ftp.kde.org/pub/kde/security_patches">ftp://ftp.kde.org/pub/kde/security_patches</a> : 

  7c2bda942c4183d4163eb3f47f22e0bc  post-3.1.5-kdelibs-kapplication.patch
  bde52aa0bba055c4f678540ec20bfe5a  post-3.1.5-kdelibs-ktelnetservice.patch

        Patches for KDE 3.2.2 are available from
        <a href="ftp://ftp.kde.org/pub/kde/security_patches">ftp://ftp.kde.org/pub/kde/security_patches</a> : 

  7cebc1abb3141287db618486fd679b32  post-3.2.2-kdelibs-kapplication.patch
  52e0e955204a77781505d33b9a3c341d  post-3.2.2-kdelibs-ktelnetservice.patch


6. Time line and credits:

        02/04/2003 Exploit acquired by iDEFENSE
	12/05/2004 Public disclosure of Opera vulnerability
        13/05/2004 KDE Team informed by Martin Ostertag
	13/05/2004 Patches created
	14/05/2004 Vendors notified
	14/05/2004 Patches created for mailto problem.
        17/05/2004 Public advisory

</pre>
