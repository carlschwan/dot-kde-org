---
title: "LWN.net: The Grumpy Editor's Guide to Presentation Programs"
date:    2004-09-24
authors:
  - "TDH"
slug:    lwnnet-grumpy-editors-guide-presentation-programs
comments:
  - subject: "Great app, no reputation."
    date: 2004-09-24
    body: "KPresenter is a great app. It's been around for a long time, and I don't think it has received the credit and reputation that it deserves. Make some more pr, you deserve it!\n\n\n(Btw, how do I turn off the google-field in the new address-field?)"
    author: "Alex I"
  - subject: "Re: Great app, no reputation."
    date: 2004-09-24
    body: "> (Btw, how do I turn off the google-field in the new address-field?)\n\nIs this in KDE 3.3?  Sadly I'm starting to see several of requests asking this question.  This will probably turn into dozens then hundreds of requests. Obviously they did something wrong by adding this feature by default.\n\nWhy does Konqueror need a google-field when you can already do a google search from the location bar?"
    author: "ac"
  - subject: "Re: Great app, no reputation."
    date: 2004-09-24
    body: "\"Other browsers have it\"..."
    author: "Jochen P."
  - subject: "Re: Great app, no reputation."
    date: 2004-09-24
    body: "I like the idea, personally.  Maybe if I use a Google bar in Konqueror, I'll stop typing 'gg:' in Safari. ;-D"
    author: "regeya"
  - subject: "Re: Great app, no reputation."
    date: 2004-09-24
    body: "Oh... well... can't count how many times I've been typing \"ALT+F2 gg:\" on windows... I got so used to it..."
    author: "Thomas"
  - subject: "Re: Great app, no reputation."
    date: 2004-09-24
    body: "I also use gg: (and others web shortcuts) to perform searches. The problem with this approach is that its hidden, there is no trivial way to find it. That said, I don't like the Google Field. So how can we make this better? Some random ideas... feel free to improve them :)\n\n- Before the address bar, it says \"Location\". How about being able to change that \"Location\" into say \"Google\"? \n\n- How about, merging this idea with web shortcuts. That is, by selecting \"Google\" you'd get an address bar with \"gg: \" on it and the focus would be on the cursor, in front of it. That way you'd learn about existing web shortcuts...\n\n\n\n"
    author: "J.A."
  - subject: "Re: Great app, no reputation."
    date: 2004-09-24
    body: "You don't need to type gg: in the location bar to search google.  Just type your keywords and search terms and it will work by default."
    author: "ac"
  - subject: "Re: Great app, no reputation."
    date: 2004-09-24
    body: "we need some thing like a combobox \non the left of the url bar ,in this combobox \nwill be placed the web shortcut like gg , ggg , ly , and \nthe others defined in the konqueror configuration"
    author: "frengo"
  - subject: "Re: Great app, no reputation."
    date: 2004-09-24
    body: "> (Btw, how do I turn off the google-field in the new address-field?)\n\n\"Settings/Configure Toolbars...\": \"Location Toolbar <searchbar>\", remove action \"Search Bar\""
    author: "Anonymous"
  - subject: "Re: Great app, no reputation."
    date: 2004-09-24
    body: "I wish this was possible (or I would know) how to turn off this feature in firefox. I love the way konqueror used to do google searches: you just type gg: my_search in the adress bar and that was it. Any idea? (on firefox)?"
    author: "Tibi"
  - subject: "Re: Great app, no reputation."
    date: 2004-09-24
    body: "Remove the Google search field from the toolbar? Right-click the toolbar, select \"Customize...\", and then drag the field off the toolbar."
    author: "Andy"
  - subject: "Google search bar"
    date: 2004-09-24
    body: "I actually don't have it and would like it, but it's not available in the customize config."
    author: "Joseph Reagle"
  - subject: "Re: Google search bar"
    date: 2004-09-24
    body: "Install kdeaddons module."
    author: "Anonymous"
  - subject: "Re: Great app, no reputation."
    date: 2004-09-25
    body: "Thanks. :-)"
    author: "Alex I"
  - subject: "Re: Great app, no reputation."
    date: 2004-10-19
    body: "thank you so much!\ni've been thinking a lot about where the turn-off thingy could possibly be. (yeah, i know it was at the most trivial place) even googleing for it was hard :) (just in case google indexes this and someone else puts the same words in, my first try was... konqueror no google and then konqueror turn off google bar)"
    author: "andras"
  - subject: "Any screenshots?"
    date: 2004-09-24
    body: "Anyone has screenshots of distinct features of kpresenter 1.3.3? \n\n--\nGorkem"
    author: "Gorkem Cetin"
---
As part of its <a href="http://lwn.net/Articles/grumpy-editor/">Grumpy Editor series</a>, <a href="http://lwn.net/">LWN.net</a> <a href="http://lwn.net/Articles/101846/">looks at presentation software</a> and KPresenter was given high marks: "<em>From the outside, however, KPresenter looks like a more vibrant, fast-moving project...OpenOffice should not be written off by any means, but KPresenter looks like it may be set to surpass it.</em>"






<!--break-->
<p>
The KOffice project is always on the look out for new developers that want to help make KPresenter even better. If you are interested subscribe to the
<a href="https://mail.kde.org/mailman/listinfo/koffice-devel">KOffice development mailinglist</a>
and bring a visit to the 
<a href="http://www.koffice.org/developer/">KOffice developer website</a>.




