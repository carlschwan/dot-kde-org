---
title: "Bull Offers Services in the Netherlands on the KDE desktop"
date:    2004-06-03
authors:
  - "fmous"
slug:    bull-offers-services-netherlands-kde-desktop
comments:
  - subject: "hope it will push others"
    date: 2004-06-03
    body: "But I didn\u00b4t know that Bull still alive :)"
    author: "JC"
  - subject: "Re: hope it will push others"
    date: 2004-06-03
    body: "Bull even made a little profit last year. It is still a quite weak company. They don't have the money to create products from scratch. I guess their stategy is similar to Novell. They have a small user base of clients using their old Gcos system and they are trying to migrate them to Bull/Linux without loosing them."
    author: "Charles de Miramon"
  - subject: "Bull is alive"
    date: 2004-06-03
    body: "Oh yes, the CEO who fired more than half of the employees at the previous incarnation of the company I work for, also fired lots of Bull NL employees when he used to be their boss, but Bull still exists. Whether or not the nearby underground station \"Bullewijk\" (Bull's Quarter) is named after their building it open for speculation.\n\nBy a curious coincidence, the second incarnation of my company rents half a corridor of office space in the Bull NL building in Amsterdam. Lots of room; there are at least a score of other tech companies in the Bull building.\n\nIt's nice that they're doing this kind of thing, though -- and it must make a welcome change from teaching Cobol and AIX courses to their customers. It would be even cooler if they would sponsor some Dutch KOffice developer :-)"
    author: "Boudewijn Rempt"
  - subject: "Re: Bull is alive"
    date: 2004-06-03
    body: "Yer, I suppose you could (and probably should) view this cynically. However, it's good that they're trying something."
    author: "David"
  - subject: "Re: Bull is alive"
    date: 2004-06-03
    body: "No, I didn't mean it cynically. It's just that I feel rather close to this bit of news. It's Bull NL that's trying things -- which makes me sort of proud :-)."
    author: "Boudewijn Rempt"
  - subject: "Re: Bull is alive"
    date: 2004-06-04
    body: "Oh yer. I'm proud that a large-ish (or used to be) company is considering any kind of support for KDE. It really is good stuff."
    author: "David"
  - subject: "TRANSLATION INCOMPLETE!"
    date: 2004-06-03
    body: "You forget to mention that in this article Bull praises KDE as a component based network transparant architecture with a perfect development platform. KDE offers a stable mature desktop, a complete office solution (KOffice), a large amount of network and admin apps, and an efficient intuitive development platform KDEVELOP. KDE is proof that the Open Source Software Model offers first class technology that equals, if not surpasses the most complex commercial software.\n\nI just thought that it's nice to know the complete content of this article!! \n\nRegards,\n\nPieter."
    author: "pieter philipse"
  - subject: "Re: TRANSLATION INCOMPLETE!"
    date: 2004-06-03
    body: ">>>I just thought that it's nice to know the complete content of this article!! \nyes, indeed. Thanks."
    author: "cndr"
  - subject: "Re: TRANSLATION INCOMPLETE!"
    date: 2004-06-03
    body: "Can we have a *complete* translation, please, oh please then?\n\nIt is so sad that KDE people's PR and media work dont match at all their ability to code.\n\nIf we could marry the PR departments of Ximian and GNOME with the coding capabilities of KDE and Qt -- now that would be a dream team to beat the MS colossus. Instead the 2 tend to fight each other....\n\nPlease, KDE, oh please: try to learn how to toot your own horn a bit more!"
    author: "kde-friend"
  - subject: "Re: TRANSLATION INCOMPLETE!"
    date: 2004-06-04
    body: "You mean Novell?"
    author: "Ian Monroe"
  - subject: "Re: TRANSLATION INCOMPLETE!"
    date: 2004-06-05
    body: "Don't ask what KDE can do for you, ask what you can do for KDE.\nI know that KDE PR is a bit week, and spreading the word for KDE hasn't been very strong, compared to Gnome, at least. I also know that in places like spanish speaking regions (Spain and Latin America) KDE is almost non-existant. That's why I did a simple thing, I started to go to every FS event I could to talk about KDE, showcasing KDE, showing how to use KDevelop, how to do RAD with KDevelop, how to program with Qt and KDE (upcomming events) and so on.\nSo, if you guys things KDE PR can be better... just go to your local LUG/FS Organization and start talking about it, evangelizing about it if you want.\nThanks."
    author: "Pupeno"
  - subject: "Re: TRANSLATION INCOMPLETE!"
    date: 2004-06-03
    body: "Nice paragraph Pieter. No honestly :) Please consider helping me writing these press releases. (mail me)\n\nAs for the translation, I think it is pretty accurate. For the Dutchies amongst us you can look up the original press release at http://www.bull.nl/nieuws/pr-KDE1.html  \n\nCiao \n\nFab"
    author: "Fabrice Mous"
  - subject: "Re: TRANSLATION INCOMPLETE!"
    date: 2004-06-03
    body: "Ah Pieter .. I see what you mean. It is the \"About KDE\" box? Well that is indeed a copy-paste-kind-of-thing used from the previous press releases we had on www.kde.org \n\nFab"
    author: "Fab"
  - subject: "Well done..."
    date: 2004-06-04
    body: "Well done Fab!\n\nSince I know this wouldn't 'be' when you hadn't worked for it...\n\nCheers m8!"
    author: "cies"
  - subject: "Help anyone? KDE services?"
    date: 2004-06-04
    body: "I know this is off topic, but I have little programming experience and I was wondering if any of you could help me. I would really appreciate it.\n\nI need a program that presses a random arrow key automatically at given intervals of time, for a given total time. Is there an easy way to do this on KDE/LINUX? It does not require a UI.\n\nI heard KDE had some great RAD capabilities, how would I go about doing this? All I know is a small bit of Python and even smaller bit of C++. "
    author: "Alex"
  - subject: "Re: Help anyone? KDE services?"
    date: 2004-06-04
    body: "why a such program ?\nJust simalute a key press and call your program/script with in crontab"
    author: "g"
  - subject: "Re: Help anyone? KDE services?"
    date: 2004-06-05
    body: "It's so a game won't automatically log me off for inactivity. That gets real annoying. Anyway, how would I go about doing what you suggested?"
    author: "Alex"
---
This week <a href="http://www.bull.nl/">Bull Netherlands</a> <a href="http://www.bull.nl/nieuws/pr-KDE1.html">announced on their website</a> that they will offer services on the <a href="http://www.kde.org/">KDE desktop</a>. A large and public company providing KDE support is something many KDE users (and, more importantly, potential users) will be very interested in. The <a href="http://www.kde.nl/">Dutch KDE Team</a> has translated the original announcement to English. 


<!--break-->
<h3 align="center">
Bull to Offer Services on Linux/UNIX-Desktop KDE
</h3>

<p>June 2, 2004</p> 

<p>Regularly costumers face the question how they can provide for implementation, administration and maintenance of Linux desktop with a limited budget. Today Bull Netherlands announces to provide services and solutions around KDE, the most well known graphical environment for Linux.  As of today companies can have professional support from Bull when migrating to Linux systems with a KDE desktop. With this Bull proves that it can offer a credible alternative to commercial software independently from their software suppliers.</p>

<p>Bull has strengthened its involvement with this step by offering desktop Linux solutions along side their server solutions. That way Bull broadens its pallet of flexible IT solutions for their costumers. The KDE desktop combined with GNU/Linux or another UNIX-like system, offers an excellent solution for businesses who want to keep their IT budgets under control. By using KDE, businesses, government departments and other organizations can escape from the enormous licensing costs that business software often requires.</p>


<h4>About Bull </h4>

<p>Bull Netherlands is part of the successful European Bull Group, situated and quoted on stock exchange in Paris with over 8 000 employees worldwide and active in many countries. Bull designs and develops servers and software for an open environment, integrating the most advanced technologies. It brings to its customers its expertise and know-how to help them in the transformation of their information systems and to optimize their IT infrastructure and their applications. In the Netherlands Bull has 180 employees. </p>

 <p>Bull is particularly present in the public sector, banking, finance, telecommunication and industry sectors. Capitalizing on its wide experience, the Group has a thorough understanding of the business and specific processes of these sectors, thus enabling it to efficiently advise and to accompany its customers. Its distribution network spreads to over 100 countries worldwide. Look for more information on <a href="http://www.bull.nl/">www.bull.nl</a>.</p>


<h4>About KDE</h4>

<p><a href="http://www.kde.org/">KDE</a> is an independent project of hundreds of developers, translators, artists and professionals worldwide collaborating over the Internet to create and freely distribute a sophisticated, customizable and stable desktop and office environment employing a flexible, component-based, network-transparent architecture and offering an outstanding development platform. KDE provides a stable, mature desktop, a full, component-based office suite (<a href="http://www.koffice.org/">KOffice</a>), a large set of networking and administration tools and utilities, and an efficient, intuitive development environment featuring the excellent IDE <a href="http://www.kdevelop.org/">KDevelop</a>. KDE is working proof that the Open Source "Bazaar-style" software development model can yield first-rate technologies on par with and superior to even the most complex commercial software. </p>


<p>Trademarks Notices. KDE, K Desktop Environment, KDevelop and KOffice are trademarks of KDE e.V. Linux is a registered trademark of Linus Torvalds. UNIX and Motif are registered trademarks of The Open Group. All other trademarks and copyrights referred to in this announcement are the property of their respective owners. </p>

<p>More information:</p>

<pre>Bull Netherlands
 <a href="mailto:lex.de-weille@bull.nl">lex.de-weille@bull.nl</a>
 <a href="http://www.bull.nl/">www.bull.nl</a> </pre>

<pre>KDE-NL
 <a href="mailto:fabrice@kde.nl">fabrice@kde.nl</a>
 <a href="http://www.kde.nl/">www.kde.nl</a> 
</pre>







