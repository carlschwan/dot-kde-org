---
title: "kdenonbeta Official Clean Up: Where will your code go?"
date:    2004-01-23
authors:
  - "wbastian"
slug:    kdenonbeta-official-clean-where-will-your-code-go
comments:
  - subject: "kdeblackhole ?"
    date: 2004-01-23
    body: "What happens when a program is removed from kdenonbeta ? Is there still any way for somebody who might be interesting in starting a similar project to find the sources of a kdenonbeta program ?\n"
    author: "Totoro"
  - subject: "Re: kdeblackhole ?"
    date: 2004-01-23
    body: "Yes -- you just have to pull kdenonbeta from a CVS tag that is earlier than the removal date.  No problems.\n\nUnless of course Zogje is playing Eraser, in which case you no longer exist, and have never existed. :P\n\nErrr -- if anyone's bored -- read Zogje's cvs comments as he removed those packages -- most hilarious (you can find them on the kde-cvs mailing list).\n\nEG:\n[06:15:14] waba * kdenonbeta/kdewizard/ (22 files in 3 dirs): a-ka ka-da-bra *poof* away it is\n\n[06:30:55] waba * kdenonbeta/kio_rdate/ (7 files):\nkio_rdate could have told you that today is:\nFri Jan 23 13:30:09 CET 2004\nWhich is 3 years and 16 days after the day that this program was imported and abandonded.\n\n[06:39:07] waba * kdenonbeta/kautorun/ (17 files): kautodelete\n\n[06:53:17] waba * kdenonbeta/kdao/ (150 files in 11 dirs): kdao, kiao, ciao\n\nAnd so on -- errr -- well, it was funny at the time ... :)\n\n--Troy Unrau\n(need a timesink? read my blog at http://tblog.ath.cx/troy)"
    author: "Troy Unrau"
  - subject: "Re: kdeblackhole ?"
    date: 2004-01-23
    body: "It is only remove with cvs tools, not from the CVS repository. So it is still in the Attic directory. (If there is no tag, you can always try with a date.)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: kdeblackhole ?"
    date: 2004-01-23
    body: "Most programs are moved to the kdeblackhole cvs module.\n\nRecently removed programs will still be available from kdenonbeta if you know how to fetch older revisions, but they will be removed from kdenonbeta entirely within a few weeks time."
    author: "Waldo Bastian"
  - subject: "I wonder..."
    date: 2004-01-23
    body: "..if this mathopd directory is because of that personal webserver thingie I once wrote, that used mathopd as its backend.\n\nYou know you are getting old if you are not sure you wrote a project!"
    author: "Roberto Alsina"
  - subject: "kcmodbc"
    date: 2004-01-23
    body: "\nwould be very nice to have in the Control Center!\n/smackmyself for sucking big time in c++"
    author: "ac"
  - subject: "Too bad"
    date: 2004-01-23
    body: "kio_vcd would have been cool.  I'm surprised KValgrind is gone."
    author: "ac"
  - subject: "Re: Too bad"
    date: 2004-01-23
    body: "I think you are supposed to use KCachegrind:\n\nhttp://kcachegrind.sourceforge.net/cgi-bin/show.cgi"
    author: "Roberto Alsina"
  - subject: "Re: Too bad"
    date: 2004-01-23
    body: "KValgrind is in KDevelop3 IIRC"
    author: "physos"
  - subject: "kcontrol"
    date: 2004-01-23
    body: "Is there already something to set environment variables, I mean a UI??\nDamn, I'm using Linux now for five years but I still wouldn't know how to do it..."
    author: "Oschi"
  - subject: "Re: kcontrol"
    date: 2004-01-23
    body: "That is something that will probably never exist, as environement variables depend on a process and are given to the children of the process.\n\nSo if you have a UI application, it could only change its environment (not any other) and this environment will not exist anymore when the application ends.\n\nIf the enviroment variables are user-specific there are mostly in the BAsh starting files: ~/.profile or ~/bashrc\n\nThe Bash syntax is:\nexport VARIABLE=value\n(export makes that the variable is given to new children of this process too.)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: kcontrol"
    date: 2004-01-24
    body: "You already gave the answer, it can be added to ~/bashrc or something, or it can be at /etc with the proper auth, editing that files from the gui and throwing some kind of molotov bomb to the screen and voila, done, all done."
    author: "Shrei"
  - subject: "Re: kcontrol"
    date: 2004-01-25
    body: "Or we could save those env. vars to some file that startkde reads/sources.\n\nThis would make the env. var available to all of KDE programs, without the need for any shell-specific hacks.\n"
    author: "David Faure"
---
<a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdenonbeta/">kdenonbeta</a> is KDE's source repository for code that is not ready yet to be included in KDE's beta releases. It can be considered a nursing ground for new software. Unfortunately not all software reaches adolescence. 
Below you find a list of programs that have been removed from kdenonbeta due
to lack of development. If you are the author of a program in kdenonbeta that is still there after this cleanup, now is a good time to think about the future of your code.




<!--break-->
<p>
You have a few options:<br>
<ul>
<li>Your code is maintained and developed by you, relatively stable and you
wish to maintain it further as part of a major KDE module.<br>
--> Todo: Write a proposal to kde-core-devel outlining why your code should
become part of this KDE module.
</li>

<li>Your code is maintained and developed by you, relatively stable and you
wish to maintain it further and make your own releases.<br>
--> Todo: Contact Klas Kalass <klas (AT) kde.org> to discuss moving your code to <a href="http://extragear.kde.org">kdeextragear</a>.
</li>

<li>Your code is no longer maintained and/or developed by you or anyone 
else.<br>
--> Todo: Contact sysadmin (AT) kde.org and ask for the removal of the code from kdenonbeta and/or do a "cvs remove -f" yourself.
</li>
</ul>
</p>
<p>
The following programs have been removed from kdenonbeta already:
</p>
<pre>
Name: columbo
Synopsis: Search client for network services

Name: gros
Synopsis: Code from Chritophe Gros
Notes: Contains Kas6, an astrolog application

Name: kautorun
Synopsis: little tool that repeatedly checks a configurable device

Name: kcli
Synopsis: library to parse CLI binary image (ECMA 335 standard)

Name: kcmodbc
Synopsis: KDE Control Centre module for UnixODBC
URL: http://www.unixodbc.org

Name: kcontrol
Synopsis: KDE Control centre applets
Notes: Contains for setting environment variables.

Name: kdao
Synopsis: CD audio toc maker

Name: kdbbrowser
Synopsis: A Database Browser using KDB
See also: koffice/kexi, kdenonbeta/kdatabase, kdenonbeta/kdd, kdenonbeta/kdedb

Name: kdd
Synopsis: KDE Database Display
See also: koffice/kexi, kdenonbeta/kdatabase, kdenonbeta/kdbbrowser,
kdenonbeta/kdd

Name: kdewizard
Synopsis: KDE Configuration Wizard
See also: kdebase/kstart

Name: keystone
Synopsis: Allows you to remotely access the desktops of machines using the
cross-platform VNC system.
See also: kdenetwork/krfb, kdenetwork/krdc

Name: kio_napster
Synopsis: kioslave for Napster

Name: kio_rdate
Synopsis: kioslave for RFC868 time servers

Name: kio_sql
Synopsis: kioslave for SQL databases
Notes: Requires KDB (kdedb) headers and libraries
See also: kdenonbeta/kdedb

Name: kio_vcd
Synopsis: kioslave to read raw video data from a VCD or SVCD

Name: klinuxscsimanager
Synopsis: add / remove SCSI devices from your system
Note: Probably Linux specific

Name: knfsplugin
Synopsis: KControl module for configuring a NFS server

Name: ksensor
Synopsis: Application to view hardware sensor information via the lmsensors
package
URL: http://secure.netroedge.com/~lm78/

Name: ksimeserv
Synopsis: Service for simple messages

Name: kstockticker
Synopsis: Stock quote ticker application
See also: kdenonbeta/ticker

Name: kvalgrind
Synopsis: KDE frontend for Valgrind

Name: kvdvd
Synopsis: possibly a KDE Control Centre module and kioslave for DVD

Name: libsearch
Synopsis: Columbo (search framework) Plugin
See also: kdenonbeta/columbo

Name: mathopd
Synopsis: lightweight, non-forking HTTP server

Name: NetMgr
Synopsis: client/server based manager for dialup internet connection
See also: kdenonbeta/NetManager2

Name: reagan
Synopsis: KDE front-end for Ogle, a DVD player
URL: http://www.dtek.chalmers.se/groups/dvd/

Name: transcode
Synopsis: possibly an aRts plugin for transcode
URL: http://www.theorie.physik.uni-goettingen.de/~ostreich/transcode
</pre>


