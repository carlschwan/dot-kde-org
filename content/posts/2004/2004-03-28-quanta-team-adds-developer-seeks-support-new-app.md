---
title: "Quanta Team Adds Developer, Seeks Support for New App"
date:    2004-03-28
authors:
  - "elaffoon"
slug:    quanta-team-adds-developer-seeks-support-new-app
comments:
  - subject: "hmmm"
    date: 2004-03-28
    body: "Eric, how much money is needed to pay a developer?\n\nI guess people in some states can easy make a living out of small money.\n\nA kind of GPL/ASP shareware concept would be very good for kde applications development. I believe KDE shall add a donations link to the info dialogue. Also in the download section a donations hint would be very helpful.\n\nI don't understand why distributors shall request money for packaging the free software while developers shall not be paid by users. As Free Software enters the desktop market the business model has to be adjusted. Not the GPL has to change, today users are not only hackers and potential contributors. The ASP Shareware guidelines (not crippleware, no evaluation time expiration ecc.) are compatible with the free and open source software idea. Early Free Software denounced the shareware principle, but you can provide \"shareware\" under the gpl with open source. shareware then means simply: When you want to use the software please support development by a small donation."
    author: "Bert"
  - subject: "Re: hmmm"
    date: 2004-03-28
    body: "I won't give you exact figures in a public forum, just some hints. ;-) Compared to EU states (e.g Finland), a beginner here gets about 1/8 of the average wages for an average programmer from there. After 1-3 years you can reach at the 1/6 part of that money, and if you are in a better position or in a bigger city 1/3 is a good salary. Gettins the same amount as in the EU is possible, although rare. I think 1/2 of that is what you may dream of, but this doesn't happen often. Personally I'd say that I'd need about 1/3 of what you can earn in the EU if I want to live without financial stress. This values should be valid in almost every Eastern European state (including those who will become members of the EU this year) with slight variations.\n Now I'm somewhere between 1/6-1/7 of what somebody might get in the EU and still at around 1/9 of what I got in Finland... Of course working at home and doing what you like is a big plus and worths some money. ;-) And sharing your work with an entire community and being part of that community is also priceless.\n So support is welcome, if you want to help us, please donate. Either through Paypal or contact us directly, especially if you are in Europe, for other possibilities. \n And of course, I'd like to thank again for all those who supported us in the last year and who support us now.\n"
    author: "Andras Mantia"
  - subject: "Re: hmmm"
    date: 2004-03-28
    body: "> Compared to EU states (e.g Finland), a beginner here gets about 1/8 of the average wages for an average programmer from there\n\nHmm, being from Finland i probably should point out that this really depends on the region and expertise, with Helsinki and Oulu being the best paid areas. Anyway, designer wages vary from 2000e/month (beginner with university or institute degree) up to 5000e/month (you have to be pretty damn good for this). My bet is that anything higher than that goes to management. This may sound decent, but take into account that we pay from 25-40% income tax, so.."
    author: "jmk"
  - subject: "Re: hmmm"
    date: 2004-03-28
    body: "Terve,\nAs I said I was working there in 2000-2001. ;-) My salary was around halfway between the two figures you gave me and I thought that the average one is about 2500 now, but as you said it may be even higher. Of course, wages vary even inside a country, but so does living costs. I payed more for a rent there (in Helsinki) than I  get here a month... Of course, concrete figures doesn't make sense if you don't know the living costs specific for a country, but believe me that what seems low living costs for a foreigner, might be very high for a man who lives here. \nIn bigger cities a rent can be 1/3 of a beginners salary, and with the other living related expenses (electricity, water, food) you are lucky if you remain with some money. And don't think that income taxes are low, there are around the same level as there..."
    author: "Andras Mantia"
  - subject: "Re: hmmm"
    date: 2004-03-30
    body: "And you don't think within the US we have to pay over 30% of our wages to taxes?\n\n"
    author: "Marc J. Driftmeyer"
  - subject: "Re: hmmm"
    date: 2004-03-31
    body: "Hello.. i'm From Romania. The sallary a designer / developer /soft writer / whatever gets .. is from 75euro / 300-400euro /month .. and the tax is about 25-40% of this.. the rents are 1/2 .. u barely survive.. many of us .. like me even work for free.. (i'm still in college and living with my father)"
    author: "Micky Socaci"
  - subject: "Re: hmmm"
    date: 2004-03-28
    body: "> I believe KDE shall add a donations link to the info dialogue.\n\nThere is already a link to the \"Supporting KDE\" page in \"Help/About KDE\"."
    author: "Anonymous"
  - subject: "Re: hmmm"
    date: 2004-03-28
    body: "A very good page, but hardly known."
    author: "Bert"
  - subject: "Re: hmmm"
    date: 2004-03-28
    body: "\"Help Out KDE =)\"\nhttp://bugs.kde.org/show_bug.cgi?id=63868"
    author: "Source"
  - subject: "Re: hmmm"
    date: 2004-03-28
    body: "Andras has given a good answer with a lot of detail. I will say this. It varies per developer. It initially depends largely on what it costs where they live and what starting wages are where they live. Where as this would be thousands of dollars per month in the US or Western Europe we are constrained to look for developers who live in areas where monthly incomes are in the hundreds of (US) dollars per month. It is even easier when  starting a developer to start them part time. There is less stress over their performance while they are becoming familiar and less stress of what they are being paid. This can be only a few hundred dollars a month for a part time developer.\n\nMy thinking with Michal was that we needed to add someone with his talent who could put in a lot of time. In previous discussions Quanta was somewhere on his do list where life becomes so perfect you can finally do those things you wanted. In the case of Andras I have paid thousands of dollars, but now with many more users and developers I'm also working at least 20-30 hours a week on Quanta in some way. In that wonderful world where everything is perfect I should get a few hundred dollars a month to do nice things for myself and stay fresh. What I have concluded for the real world is that I have to limit how large a part of my income I use to sponsor developers. This is because my business is in a place that it could (and should) grow quite a bit this year. If I go from making an average income to a well above average income I can do a lot more fun things with more money. If I run into a cash flow problem because I spent too much on my pet project... you get the idea.\n\nThe great thing is that with our user base, those users who are moved to take action can make a difference very easily. My personal estimation is that we have over a million users, probably closer to two million. The amount to contribute should be small if percentage participation is small, but participation is a fraction of a percent. It's Sunday morning here and so far we have a 25$ EU and $25 US a month sponsor (both from Europe BTW). This is great! I'm excited to see this kind of commitment because it removes stress from my life. To sponsor one developer part time it takes about 4-6 times the amount of support we have received so far from these fine people. In Michal's case I have guaranteed I will make up the difference. In the case of \u00d6zkan and his partner I heard back from him yesterday that he will try to do whatever he can with F4L regardless of whether I can pay him. That's the right answer and allows me some flexibility.\n\nAt this point I'm pretty comfortable to relax and see what happens. Good things are happening no matter what. Like Andras, I don't like to make other people's finances overly public, unless they tell me to. I see my duty to this project and our contributors as making sure that we are getting good results and direction. Because I only bring in people who are led by their passion to work on the project we get great results. After four years of working with this project my goal is to continue to question everything I do and look for ways to improve. So I hope these efforts right now produce exciting results in the coming months for our users."
    author: "Eric Laffoon"
  - subject: "Re: hmmm"
    date: 2004-03-28
    body: "Could you use KDE e.V to take donations and give salaries? It would seem within the purpose of KDE e.V. to do so. Its not uncommon to see non-profits where the donator gets to pick where they want their money to go, it could be that sort of thing.\n\n"
    author: "Ian Monroe"
  - subject: "Re: hmmm"
    date: 2004-03-28
    body: "It isn't KDE e.V.'s goal to hire developers. In this aspect it's very different from the Mozilla and Blender Foundations."
    author: "Anonymous"
  - subject: "Re: hmmm"
    date: 2004-03-28
    body: "No, the role of the KDE e.V. is to support the KDE project as whole (and I'm not even sure that it would be legal for a non-profit organization to pay salaries for developers). Better spend the money donated to them to organize conferences, promote KDE, etc. so everyone will and can enjoy the donations, not only some particular developers.\nIIRC there was an idea (as far as I know which never materialized) to be able to donate to a specific area of KDE when you donate to KDE e.V., which still seems good to me, but you know, if you want to support some specific project or a developer, the possibility is still there to contact them independently and support them. Some projects make this easier (donation links), some less easier. And of course you can even now add a comment when you donate to KDE e.V.\n Of course I wouldn't mind if some KDE supporting company decides to support a specific project (hey, of course, Quanta ;-)) inside KDE."
    author: "Andras Mantia"
  - subject: "Re: hmmm"
    date: 2004-03-28
    body: "Maybe Eric is a good station to support developers. His business runs for supporting KDE ;) If KDE eV couldn't hire developers is OK. \nI estimate if Eric get enough support (sponsoring) from the community he hire developers not only for Quanta.\nEric?"
    author: "Uwe Ristok"
  - subject: "Re: hmmm"
    date: 2004-03-29
    body: "If Eric were a millionaire he'd have them all over for a party too. ;-) I admit that I've given this some thought and even thought about how to manage certain aspects of it. It's something I'd like to be able to do. It's not really such an easy thing to do because you have to balance the spirit of OSS development with financial aspects. It requires certain skills and practices and I have learned a lot in the last four years. I think I have a good aptitude for it.\n\nThe reality is though that there are a number of things that would need to come into place to move it to a larger scale. I'd enjoy the challenge. I talked about it in a recent interview. I would need to have excellent financial standing because having a number of people involved eats time like you can't believe. Ironically I'd want to get someone who was not just skilled at coding but also skilled at project management and interaction to help.\n\nMoving into working on other programs you have the further complication of interacting with a project and respecting their leadership. Because I have an active mind I contemplate how these things might work. Perhaps part of what attracts me to it is that it is so challenging. I'm not into being bored. ;-)\n\nI would certainly love to have an even larger impact on KDE application software, but everything I do is gradually one step at a time. Sometimes I have to remind myself that there are some things still beyond my reach. New things have to be to the standard of what we're doing right now and not take away from the other things we're doing. Things are only worth doing if they can be done with excellence.\n\nWe will see how it goes. For now I'm very happy that we are making the progress we are. It means a lot for me that I'm able to do what we're doing now for users and developers. It's hard to put into words. I just want to produce software now that makes people say \"WOW!\" ;-)"
    author: "Eric Laffoon"
  - subject: "Re: hmmm"
    date: 2004-03-29
    body: "Well, dealing with such donations via KDE e.V. (or some similar entity) would still be good idea. If this e.V. was \"gemeinn\u00fctzig\" (don\u00b4t know a good english translation), and I think it is - free software for everyone and stuff -, it would be possible to use such donations to reduce your taxes - I do a lot of stuff for such non-profit organizations fot that reason (it\u00b4s one, but not the only reason)... ;-)"
    author: "Willie Sippel"
  - subject: "Re: hmmm"
    date: 2004-03-29
    body: "> If this e.V. was \"gemeinn\u00fctzig\" (don\u00b4t know a good english translation), and I think it is\n\nNon-profit. And it's not yet."
    author: "Anonymous"
  - subject: "Re: hmmm"
    date: 2004-03-29
    body: ">> If this e.V. was \"gemeinn\u00fctzig\" \n>>(don\u00b4t know a good english translation), and I think it is\n \n> Non-profit. And it's not yet.\n\n\nAccording to http://www.kde.org/areas/kde-ev/ it is: \n\n\"KDE e. V. is a registered non-profit organization that represents the KDE Project in legal and financial matters.\"\n\n"
    author: "cm"
  - subject: "Re: hmmm"
    date: 2004-03-29
    body: "It's of course non-profit but not yet accepted as such by the financial authorities."
    author: "Anonymous"
  - subject: "Re: hmmm"
    date: 2004-03-30
    body: "Are you also talking about German financial authorities? \n\nI thought they *were* accepted there:  \nFrom http://kde.org/areas/kde-ev/corporate/statutes-en.php : \n\n3. Non-profit status of the association \nThe association pursues according to section 2 of the statutes exclusively and directly non-profit activities according to the section Tax Abatement of the Abgabenordnung (Paragraphs 51 AO). It is acting non-selfish and is not pursuing profit\n[...]\n\n"
    author: "cm"
  - subject: "Re: hmmm"
    date: 2004-03-30
    body: "> Are you also talking about German financial authorities? \n\nSure. Only the place where the association is registered matters.\n\n> I thought they *were* accepted there:\n\nPursuance of one side is not equal with acceptance of the other side."
    author: "Anonymous"
  - subject: "Re: hmmm"
    date: 2004-03-30
    body: "You cannot register an association in Germany without a statute. \nThe statute of KDE e.V. states that KDE e.V. is a non-profit \nassociation. \nAFAIK *before* you register an association in Germany \nthat's supposed to be non-profit you have to consult \nthe financial authorities or you cannot register it with that statute. \nThat's part of the registration process...\n\nHow can the KDE e.V. be registered if its statute states false things? \n\nIANAL, though, and of course something I stated may be wrong...\nWhat am I missing?  \n\n"
    author: "cm"
  - subject: "Re: hmmm"
    date: 2004-03-30
    body: "> AFAIK *before* you register an association in Germany that's supposed to be non-profit you have to consult the financial authorities or you cannot register it with that statute.\n\nYou're wrong, and statutes can be changed. KDE e.V.'s initial statutes didn't met the requirements of the financial authorities so there were consultations with them what has to be changed and the statutes were voted in Nove Hrady. Sadly they're not active yet.\n\n> How can the KDE e.V. be registered if its statute states false things?\n\nRegistration at court and acceptance by financial authorities are two different processes as they're two different entities."
    author: "Anonymous"
  - subject: "Re: hmmm"
    date: 2004-03-30
    body: "> KDE e.V.'s initial statutes didn't met the requirements of the financial \n> authorities so there were consultations with them what has to be changed and \n> the statutes were voted in Nove Hrady. Sadly they're not active yet.\n\nAh ok. \n\n\n> Registration at court and acceptance by financial authorities are two \n> different processes as they're two different entities.\n\nYes, of course, but I would still have expected the court to reject the \nregistration of an association that claims in its statute \nto be non-profit but is not (yet) accepted by the \nfinancial authorities... but I seem to be wrong. \n\nFscking legal stuff...\n\n\n"
    author: "cm"
  - subject: "Re: hmmm"
    date: 2004-03-30
    body: "> Fscking legal stuff...\n \nExactly! I don't know all the German statutes (though I believe the e.V. complies), but here in the US it's none too simple either. That's why it is expensive and complicated to set up. In partial justification, governments do need to make it's not easy to set up \"the church of drinking beer\" by mail and open a tax exempt bar in your garage. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: hmmm"
    date: 2004-03-29
    body: "The problem with this type of set up is that it really requires someone with a lot of money that really loves what they're doing. So far mostly I'm just love. ;-)\n\nThere are thousands of dollars in legal fees to set up the structure and then the administrative costs. There's a real cost in time to pursue the funds and then when it comes to working with developers...\n\nEveryone I work with on a sponsorship basis I have to first get to know over email, discuss how we see things, our thoughts, our passion for what we're doing... There are issues not only with skills but with how people work. You have to know that someone will work on this because they really truly love doing it because you can't look over their shoulder. There there is the interaction on what needs done, the actual oversight of the project laying out task priorities and answering numerous questions on particulars. \n\nThe process that I go through has evolved over the years and it takes a lot of time as well as knowledge of the program and communication skills. It's not just finding someone and sending them some money and getting some software.\n\nI really want to do something like this but doing it well is more challenging than I think people realize. It would be possible to find someone who you just send money on to work on a piece of software, but to make something larger in a project you have a lot more issues and you have interactions. Maybe I'm a control freak but I don't like leaving aspects of things to chance that can make or break a project or release, especially if I'm using money from contributors. That's a major incentive to not screw up.\n\nAnyway my thinking is that I'm not sure how many people I'd trust to do something like this on any scale. I probably could, but I'd need to have excellent financial positioning because if I wasn't drawing a salary it might be difficult if I didn't have time to work. I classify myself as a person who enjoys risk that he can structure to high probability favorable circumstances. I'm risk averse where those risks remove my control or cause things to blow up if they don't go perfect. My paranoia is hard earned and serves me well. Also my business is backwards of most jobs. my schedule is flexible during the week and I leave the house to work on weekends. If I had a regular job I'd get fired for the time I spend on Quanta. Probably sooner actually. I haven't had a job for 15 years... I don't like them. At my worst I'm still my best boss ever. ;-)\n\nMy thinking is that there is no way I can look at expanding what I do too fast. Having said that the eventual possibility that I may be able to expand what I've been doing with Quanta seems like a logical progression. It still requires a lot of problems to be solved. Paradoxically a large enough amount of user support would necessitate moving in that direction faster. ;-)\n\nThe fact remains that no matter how grand our plans we have to look at things day by day and make good decisions. I'm personally happy to take my time, not burn out and not risk personal financial disaster by trying to do too much more than I can at a particular time."
    author: "Eric Laffoon"
  - subject: "Re: hmmm"
    date: 2004-03-30
    body: "All I was suggesting was using KDE e.V. as place to donate money for quanta developers. In other words, what is happening now with the benefit of non-profit status. Though I would imagine a lot of that benefit would be lost if its only registered in Germany. And sure its legal for non-profits to pay employees, they do it all the time. Non-profit means you don't have a profit, not that your working for free.\nThe objective of KDE e.V.\na. The objective of the association is the promotion of science, research, education, art and culture by means of creating and distributing general purpose computer software free of charge to the public. The association arranges scientific talks in the area of computer science that are open to the public. The association is strictly a non-profit organization. \nb. The objective of the association is reached by creating and distributing the K Desktop Environment in the public domain. \n\nSupporting developers doesn't seem out of its scope, even if this isn't what they do now."
    author: "Ian Monroe"
  - subject: "Re: hmmm"
    date: 2004-03-31
    body: "Sponsoring developers is outside the scope of KDE e.V. They do other very useful things.\n\nSponsoring developers is a de facto endorsement of the developer and project. This means that if you're going to do it centrally you need come up with a \"fair\" distribution which is a recipe for a lot of wrangling (instead of coding) by developers. All of this is rather silly because a user can in fact choose which projects they want to support. To top it all off, last year the e.V. raised about enough money to sponsor one developer, but they used money for other things that were needed. \n\nWhat you're asking for adds up to too many real problems. If all you're talking is having a non profit foundation for funding development, when the numbers work out I'm happy to do that and to pursue larger contributors. I'm also one of the largest sponsors in KDE. If what you want is a non profit channeling funds into KDE development our project is probably the most likely to evolve into that."
    author: "Eric Laffoon"
  - subject: "Re: hmmm"
    date: 2004-03-28
    body: "Just another insight: a year ago when I was still in Indonesia, I'd be glad to hack day and night if only somebody sponsors me as low as few hundreds dollar. That was supposed to be quite enough to pay the rent and enjoy the living. So, might be good idea to head to southeast asia :-p"
    author: "Ariya"
  - subject: "KDE Only DIstros steep UP"
    date: 2004-03-28
    body: "The KDE only distros like Lycoris and Xandros need to step up to the plate. I'm not aware of any projects that these distros sponsor. Lindows already is sponsoring other projects plus working on somthing in house. "
    author: "Kraig"
  - subject: "Re: KDE Only DIstros steep UP"
    date: 2004-03-28
    body: "> Lindows already is [..] plus working on somthing in house. \n\nAnd what? :-)"
    author: "Anonymous"
  - subject: "Re: KDE Only DIstros steep UP"
    date: 2004-03-28
    body: "They took Mozilla Composer, changed the skin, and call it NVU.\n(Warning: Don't use NVU if your Mozilla version is older than 1.6! NVU will shredder your Mozilla config.) Good work, Lindows ;-)"
    author: "PI,14"
  - subject: "Re: KDE Only DIstros steep UP"
    date: 2004-03-28
    body: "No, I'm talking about KDE applications. See http://www.osnews.com/story.php?news_id=5758&page=4 question 9."
    author: "Anonymous"
  - subject: "Re: KDE Only DIstros steep UP"
    date: 2004-03-28
    body: "Look at the <a href=http://www.osnews.com/story.php?news_id=5758&page=4>Kevin Interview</a> on OSNEWS.  This is his quote, \"We are also working on some great KDE applications in house, which are open sourced and that we'll be sharing with the world when LindowsOS 5.0 comes out. Linux needs more and better applications to succeed. That is the area we are spending most of our time, energy and dollars right now.\"\n\n"
    author: "Kraig"
  - subject: "Re: KDE Only DIstros steep UP"
    date: 2004-03-28
    body: "Well actually they hired the composer developer to work on it full time. "
    author: "Kraig"
  - subject: "Another Idea: Shop-an-Improvement"
    date: 2004-03-28
    body: "http://www.kontact.org/shopping/"
    author: "Anonymous"
  - subject: "Re: Another Idea: Shop-an-Improvement"
    date: 2004-03-28
    body: "And for all those annoyed by KMail's synchonious filtering blocking KMail when mail is comming in, don't forget to pledge a donation to fix bug 41514 on http://www.kontact.org/shopping/sanders.php !"
    author: "Andr\u00e9 Somers"
  - subject: "Re: Another Idea: Shop-an-Improvement"
    date: 2004-03-29
    body: "Seems like it would be fixed in 20 weeks anyway which would be before for the KDE 3.3 release."
    author: "Anonymous"
  - subject: "Quanta must support what-you-see-is-what-you-get"
    date: 2004-03-28
    body: "Quanta is a good application and it can become a great application if it supports WYSIWYG (what you see is what you get) like KWORD does, but Quanta is advanced hence it can help web development for the newbies...\n\nMy favorite applications in KDE:\n\n1. KWord\n2. Kaffeine\n3. Kwrite\n4. Quanta\n5. Kicker\n6. KSim\n7. KDF\n8. KSambaPlugin\n9. SMB4k\n10.KMail\n12.KFontinstaller\n13. Konqueror :)\n14. Konsole ...\n\nThanks to all KDE Developers... I appreciate your endeavor and creativity. You people are more \"Artists\" than just mere programmers! Hats off! to you!!!"
    author: "fast"
  - subject: "Quanta already does..."
    date: 2004-03-29
    body: "...you ought to update to KDE 3.2.x before asking for something which is there already."
    author: "Datschge"
  - subject: "Re: Quanta already does..."
    date: 2004-03-29
    body: "Well, I was expecting great things of Quanta after hearing so much about it. I am running the version that comes packaged with Mandrake 10 Community Released. The about box says that it is Quanta 3.2.1\n\nTo cut a long story short, I have a page that is valid XHTML and CSS2 and Quanta chocked on it.\n\nI have also had it crash so often that I have gone back to doing things by hand because there just isn't any real time saving when you have to worry about things crashing every other minute.\n\nHere's to hoping things improve.\n\nThis may seem like a very negative post, so let me say that I love what Quanta is trying to achieve and I have confidence that it will eventually get there, but it would need about 5-10 developers working on it full time to do so within the next two years.\n\n"
    author: "Joe  Scicluna"
  - subject: "Re: Quanta already does..."
    date: 2004-03-29
    body: "> To cut a long story short, I have a page that is valid XHTML and CSS2 and \n> Quanta chocked on it.\n\nI have no idea what \"chocked\" means for you, but if you don't file a bugreport, we can't fix bugs.\n \n> I have also had it crash so often that I have gone back to doing things by\n> hand because there just isn't any real time saving when you have to worry\n> about things crashing every other minute.\nSame here, except if it crashes when you access the Bookmarks menu. That is MDK 10 specific problem...\n\nIn short: http://bugs.kde.org for all those having problems.\n\n \n"
    author: "Andras Mantia"
  - subject: "Re: Quanta already does..."
    date: 2004-03-29
    body: "This is really fun. Now let's get real. Open that version of Quanta, go to the help menu and pull up the about dialog. Now read the version. It will give you a release date from CVS. \n\nQuanta 3.2.1 (Using KDE 3.2 Branch >=20040204)\n\nI'm sorry, but if you're basing an evaluation of Quanta on this then that is really sad because it's total bull flop! Either you were unclear that Mandrake was releasing a CVS build was or they didn't explain it well. Let me explain. We released 3.2 in January and found that there was a problem with parsing certain structures in PHP that did not show up in our tests prior to release. We had to completely restructure the parser in the HEAD branch and intermittently update the 3.2 branch as we went. The entire project was very unstable until the last week before release right around the first of March.\n\nNow if you're willing to make an evaluation based on the 3.2.1 *RELEASE* fine. Otherwise I gotta tell you it frankly pisses me off te read that Quanta is unstable. Because I respect the Dot I won't ask for everyone with a stable release to post. Read the link above on the words \"huge dividends\". It shows what an exceptional record we have dealing with bugs. Our releases are VERY stable.\n\nYou're posting here that we have a problem and I don't even have to ask if you filed a bug report because if you did Andras would have already gotten back with you and you would be either building from source or getting a 3.2.1 RPM. You certainly wouldn't be running a badly broken CVS build.\n\nWhatever happened to the simple rule that if you want stable software don't run a development version? I can't believe I'm getting my chops busted over this. If Mandrake can't get people to understand they're delivering CVS software they should stop the practice! I get tired of people thinking the worst of our software because of packaging decisions!\n\nGood practices dictate that distributions release stable release software and users report bugs. Had either of these practices been followed you would not be telling the same story here. Sadly neither were and we look bad because of it."
    author: "Eric Laffoon"
  - subject: "Re: Quanta already does..."
    date: 2004-03-31
    body: "I am sorry to sound harsh to you, but as the maintainer of Quanta, it is your responsability to make distributors behave respectfully. The GPL allows Mandrake to create derivative of Quanta 3.2cvs, but not to call them Quanta 3.2.1...\n\nSo you should not direct your anger at a user that is complaining about what has been presented to him as quanta 3.2.1. You should (actually you have to) send an official letter to Mandrake, asking them to stop using the trademark Quanta for one of their products (a buggy derivative of Quanta)."
    author: "oliv"
  - subject: "Re: Quanta already does..."
    date: 2004-04-06
    body: "being a user of quanta 3.2.0 I'd have to agree.\nMDK's version is shite. Total and absolute shite.\nBut that's what you get when patch upon patch upon patch is thrown in"
    author: "illogic-al"
  - subject: "Re: Quanta already does..."
    date: 2004-04-13
    body: "I use Quanta 3.2.1 on KDE 3.2.1, and it is still somewhat unstable. Chrashes tend to happen at random so i have been unable to provide decent bug reports."
    author: "theboywho"
  - subject: "Still some Mandrake pain"
    date: 2005-01-18
    body: "Yep I've been experiencing issues for a year now, and now I'm on Quanta post-3.2.3 using KDE 3.2.3 and it locks up in a pretty random fashion. Some days it'll run beautifully others it will seize up pretty quickly. I've tried to figure some rhyme or reason but to no avail. Happens on all sizes of files, pressing all sorts of keys. \n\nI don't get the debug option with these crashes either. I'm curious if its somehow due to issues on the network share where the files live? I'm going to run a while without the autosaving a backup and see if I can't get some more info.\n\nI've been doing HTML since Mosaic Alpha 8 :-) and truly love Quanta. Other than these issues, (which may be due to being a somewhat linux dektop novice) it's a near perfectly constructed tool.\n"
    author: "barnamos"
  - subject: "Re: Quanta already does..."
    date: 2004-03-29
    body: "Hey Datschge, he just said we went from good to great. He just didn't know he said it. Now we have to find a way to get to all these people's houses and install the new versions for them so that they know it. ;-)\n\n\n"
    author: "Eric Laffoon"
  - subject: "attitude"
    date: 2004-03-29
    body: "I think this is not a good way to go, i once talked to a developer at a exhibition from Xfree86 about a cool feature and he say they said \" yeah nice but i only implement it if you pay me \". This attitude really sucks and brought Xfree86 down.\n"
    author: "chris"
  - subject: "Re: attitude"
    date: 2004-03-29
    body: "Well that's not my attitude, Andras' attitude, Michal's attitude or \u00d6zkan's attitude. Could it be that you either didn't read it, jumped to conclusions or just don't really know much about us? Let's look at the facts...\n\n1) I started sponsoring development with Dmitry and Alex in 2000. They needed to move our of the hostel they were living in and into a house with a phone line for an internet connection. They were almost out of college and it's hard to contribute when you have to put it on a floppy and borrow a friends connection.\n2) I sponsored Andras mid 2002 after he had gotten Quanta in order in December 2001 for a 2.0 release without asking for money. Because he was looking for work and stressing it was hard for him to work on it prior to being sponsored. had he taken a job the employer policy where he lives is to work you as many hours as they want for the same money. It would have been quite likely that he would not have had much time or energy to code Quanta. \n3) I spent thousands of dollars of my own money before I thought that maybe I should ask for help.\n4) Michal has another open source project he's working on and just finished his PhD so he hasn't had much time for Quanta.\n5) \u00d6zkan did say he'd work on it no matter what and told me that since he broke up with his girlfriend a few days ago he would have more time. I think I can understand that. Considering he said a few hundred dollars a month would put him in good shape I don't think I can call him greedy.\n\nEach of the people developing under sponsorhip or hoping to be sponsored with our project loves what they're doing. \u00d6zkan produced an alpha of his program already, Andras released Kallery and Michal released Knowit. \n\n* Nobody here is saying they won't work on this unless paid, unless it is just not feasible for them to do so.\n* Everyone we sponsor is able to put in substantially more time and as I've often explained this makes them several times more effective with that time because they are more familiar with all aspects of it.\n* Everyone here has produced code in advance of asking for sponsorship help.\n* It costs money to live and earning it can get in the way of having time and energy for other things.\n\nWhat this has done for Quanta is gotten us noticed, like our recent LinuxQuestions.org user choice award. It also got us one of the lowest counts for open bugs of KDE applications as well as a turn around time for user support that is often in the minutes. One contributor made a feature request that was filled in 3 hours in CVS!\n\nWhat actually has been the downfall of XFree86 is a number of unrelated but different issues including their license and looking for recognition. We don't in any way resemble XFree86 at all!\n\nMaybe what really sucks is the idea of financially supporting a project that does a good job? Note that I NEVER try to make people feel guilty as a motivation to contribute nor do I ever beg. I detest both and I respect our users too much. I don't even like asking for money, but at some times I think it is too important not to. \n\nI'm not personally offended by what you said, but I think I proved that you don't have a solid perspective here. Everyone here knows who I am. If I've messed up go ahead and point it out, but if you're going to do so you had better have your facts straight. People are contributing money to our project and it's EXTREMELY important to me that I do the right thing by them. As far as attitude goes, it is difficult to ascertain sometimes without some study. I think it's reasoable to assume, based on what we've done, that ours is okay. How's yours? ;-)"
    author: "Eric Laffoon"
  - subject: "Re: attitude"
    date: 2004-03-29
    body: "I never said (nor did anyone else in Quanta) that I'll do some features if you pay me. There were other cases, like I offered to do something after I got a donation or somebody asked me if I can implement a feature and he is even want to pay for it, but that is different. In general the case is one of the following:\n- you are working on your free time, so the project goals in the direction you imagined. If someone's request does not fit in this vision or is low priority for you, that person can try to get that feature for money. I don't see any problem with this.\n- you are working full/part time and get payed for the work, so your main employer may/can decide which way to go. The rest is the same, except that the feature for money must be discussed with the main employer, especially if it takes much time to implement.\n\nIn case you are not the only developer of the project (or that area of the project), the request must be discussed also with the others, unless you want to create a forked version.\n\nAll in all, I don't see problem with offers like \"feature for money\" until it doesn't mean that I won't code anymore or I won't implement that feature (even if I agree that would be good to have) until you pay me. ;-) But that's not the case inside the KDE project."
    author: "Andras Mantia"
  - subject: "Re: attitude"
    date: 2004-03-29
    body: "I have to disagree. In my view, it's a way for people who can't contribute to development otherwise to support the project. If I want a certain feature, I could probably implement it myself. That means I'd have to invest time. Not everybody I know of can do that. If they want something implemented, all they can do ask nicely (AKA: file a feature request) and wait untill it is done. Now, they are also offered the opportunity to speed things up a little for that feature they really want, and sponsor a developer that is thus able to invest more time in KDE development at the same time. The whole KDE community will profit from this, so he made a real contribution. To me, that sounds like a Good Thing (TM)."
    author: "Andr\u00e9 Somers"
  - subject: "Re: attitude"
    date: 2004-03-31
    body: "What you can do is patent this feature: Then when the guy realises it is a nice feature, you can go to him and say: \"yeah nice but you can only implement it if you pay me\" ;) (of course I am joking ...)"
    author: "oliv"
  - subject: "theKompany?"
    date: 2004-03-29
    body: "What about them. I mean their Quanta Gold doesn't really see much activity from what I can see. Why not talk with them about sponsoring? They sponsor a developer(full or part time) who will work on Quanta and I guess implement feutures that theKompany wants but Quanta doesn't... or something of that sort. Bah anyway it was just a thought that poped in my head while reading the posts."
    author: "Boris Kurktchiev"
  - subject: "Re: theKompany?"
    date: 2004-03-29
    body: "First of all let me set the record straight. They do not sponsor, nor have they ever sponsored anyone to work on Quanta Plus. If they do I'm unaware of it. The two original developers I worked with decided to team up with them and produce a commercial application out of Quanta. I chose not to participate. I did not think it was a good idea. If anything my feelings about Quanta remaing free have only gotten stronger with time. Without getting into a discussion I don't want to get into I will just say that I don't think it is in their interest to to sponsor an application that effectively competes in the marketplace with one they are selling. That doesn't make any sense at all. \n\nThe only reason they have a similar name is that one of the developers rejected the idea of a different name and certainly it would be easy to mistakenly think  it was an upgrade from the GPL version. Our team debated and rejected renaming the original. There is absolutely no linkage between the two programs since the split. The split came around the time I lost my mother and for me involved a lot of what I considered to be less than pleasant communications.\n\nRight now, today, as a small business person and with the help of the community I am sponsoring two developers and making some progress on adding more. As open source projects go we have good community financial support because of our strong stance on free software and our excellent track record producing results. As it happens I have a lot more faith in people like myself supporting a cause or making an investment in a piece of software they like than in companies where there may be conflicting interests. The only software related contributions we get are from Ian Fleeton for Linux CDs he distributes. Mandrake registered me to receive contributions but fortunately I didn't hold my breath.\n\nI acknowledge that theKompany has brought some good applications to KDE under the GPL and other licenses but our interests are not the same. It's unlikely we would ever collaborate."
    author: "Eric Laffoon"
---
The Quanta team has been growing in volunteer developers, but the amount of work to make a world class killer application is daunting. Community support has enabled me to take the next step and <A href="http://quanta.sourceforge.net/main2.php?newsfile=mrudolf">sponsor another developer!</A> As it happens we also have the opportunity to bring some killer features to our web development suite, but this will require some <A href="http://quanta.sourceforge.net/main1.php?actfile=donate">help from the community</A> to help some additional developers. So while I want to bring some good news to the community I also need to ask for a vote of support too.



<!--break-->
<p>Fortunately community support has been good. My thanks to our supporters! I have gone from the sole sponsor of Andras to where I only have to pay a little bit of his monthly income. <I>Andras's full time effort has paid <A href="http://bugs.kde.org/reports.cgi?product=quanta&output=show_chart&datasets=NEW%3A&datasets=ASSIGNED%3A&datasets=REOPENED%3A&datasets=UNCONFIRMED%3A&datasets=RESOLVED%3A&banner=1" target="_blank">huge dividends</A> to Quanta development</I> as any user who has interacted with us will attest. I'm very excited to announce <I>Michal Rudolf, the author of <A href="http://knowit.sf.net" target="_blank">Knowit</A>,  will be joining our team!</I> I hope the community will join me in welcoming him as well as helping me to move him from part time to full time in the coming months. As the attached story points out <A href="http://dot.kde.org/1071590847/" target="_blank">Özkan Pakdil</A>, the author of <A href="http://f4l.sf.net" target="_blank">Flash 4 Linux,</A> got back to me late. I believe what he has started can be made into a tool for both <A href="http://www.openswf.org/" target="_blank">Flash</A> and <A href="http://www.w3.org/Graphics/SVG/" target="_blank">SVG</A> and that if it's done right it could be very useful for rounding out the package of an application set that can attract web designers from Windows to KDE. It would add new capabilities for those of us developing web sites on KDE. We want to get community input and do it right, but we also need a vote of financial support to enable these developers to proceed. <A href="http://quanta.sourceforge.net/main2.php?newsfile=mrudolf">Have a read</A> and consider becoming a <A href="http://quanta.sourceforge.net/main1.php?actfile=donate#sponsor">sponsor</A> or making a <A href="http://quanta.sourceforge.net/main1.php?actfile=donate#donate">donation</A>. If you do web development this could affect you.