---
title: "Interview with Chris Schlaeger from Novell/SUSE"
date:    2004-10-07
authors:
  - "fmous"
slug:    interview-chris-schlaeger-novellsuse
comments:
  - subject: "Just give me a break."
    date: 2004-10-07
    body: "Please excuse me but what is up with all the OOo obsession ? In the previous article (the Interview with David Faure) someone pointed out how many problems he got with OOo by rendering Tables wrong, Segfaulting when loading old Staroffice files, the huge load times and the overall slow and bad operation. I don't think that Enterprise customers want that. Not to mention that the entire Staroffice suite (afaik) is written using the Staroffice foundation class. Totally different to what QT or GTK+ is and altering everything to use a GTK+ or QT Window or Filedialogs doesn't change the rest of the program. It's a lot of energy and time that could be better used to finish KOffice for example.\n\nGecko vs. KHTML part in this Interview also sounds like it would be better to abandon KHTML in favor to Gecko. Sure Gecko does offer better rendering capabilities but its entire framework doesn't really fit into the KDE or GNOME world. Speaking of XUL as different Widgetset or the entire Interfaces for JavaScript, SSL, Cookies etc. This would require a lot of work on the KDE side I assume and would only generate a huge mess (which I only assume here).\n\nHAL and DBUS also something that needs to be thought about. HAL, as the name implies it's a Hardware Abstraction Layer and we know that BSD, Linux, Solaris and so on offer their own Kernel side HAL which sits between Hardware and Kernel. This new HAL (god bless but it's all against what I have been studying for many Semesters) is something I don't understand this one sits between Kernel and Desktop in Userland and adds a second HAL layer ontop of it working together with UDEV and Hotpluging. Either I don't understand it, or it's totally different to what people teach students at universities or last but not least the name is misleading.\n\nDBUS something that has been created by the GNOME fraction once they realized that CORBA and Bonobo is overkill for what they do now trying to get the KDE people to throw away DCOP in favor of this.\n\nParts of the Interview gives me the impression that this guy is more talking for the side of the GNOME people rather than the KDE people because everything he said I already heard from GNOME and de Icaza. This is exactly what the GNOME people are doing. Merging OOo, merging HAL, merging DBUS, merging Gecko and everything that has been done so far looks like unfinished patchwork. I don't think or believe that this is what KDE users and supporters really want. Sure they want good functionality such as good rendering and good office suite but this doesn't mean that KDE has to throw away half of their core for things that can hardly been merged due to different framework.\n\nWhen I listen to the kde-core-devel mailinglists then I see how people defend the position and questionize whether HAL, DBUS, Gecko are all that good of ideas. Hell whenever some GNOME fart into the air we need to adopt it. They say jump and we have to jump."
    author: "Murat Kotch"
  - subject: "Re: Just give me a break."
    date: 2004-10-07
    body: "OOo is a current necessity given the state of other options. i do hope that in the long run it goes away, and having KOffice use the OOo file format natively puts it in a good position to take over eventually.\n\nas for HAL, DBUS and other such things, stop obsessing about where things come from. as long the license is right (e.g. Free/Open Source), the only the question we need to ask is \"Would using this make KDE better? Does it serve our goals and purposes?\" for what else really matters? these are complex questions that cover everything from features, release cycles, APIs, interop, maintainability and much more. but \"did a GNOME touch it?\" is irrelevant.\n\nas for \"unfinished patchwork\", well.. if that's your assessment of the GNOME's efforts, fair enough, but i fail to see how their efforts reflect upon our's, anymore than our's reflect upon their's. i'd suggest judging the KDE project's efforts by the KDE product, not some other product.\n\n\"Hell whenever some GNOME fart into the air we need to adopt it.\" you mistake cooperation for weakness and brand it undesirable. cooperation goes both ways (look at how many freedesktop.org specs are basically KDE ideas touched up and 'rebranded'), and it can have great rewards for all, especially our users. \n\nnow, mindless acceptance of any and every suggestion would be stupid. but fortunately the KDE developers are anything but mindless."
    author: "Aaron J. Seigo"
  - subject: "Re: Just give me a break."
    date: 2004-10-07
    body: "I totally agree. Using OOo brings to mind the expression 'don't look a gift horse in the mouth'. Without it large Linux deployments wouldn't happen. It is nice to see the active development in KOffice.\n\nRegarding DBUS (and other technologies that Kde would adopt), the question is whether the developers are responsive to the needs and suggestions from the Kde folks. From what I understand, there is no problem.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Just give me a break."
    date: 2004-10-07
    body: "Hi, \nEven though I understand your point, 'don't look a gift horse in the mouth' seems like a terrible choice as an expression to me. If it's important as a selling point right now, then it should be examined closely. If KDE integration is important this kind of things simple cannot happen: http://qa.openoffice.org/issues/show_bug.cgi?id=23526\n\nDrag and Drop of images from konqueror to some of the OpenOffice.org apps (e.g. OO Writer), create a link to the image instead of including it. This can be terrible, it affects KDE users directly. It screwed up lots of documents of my sister which were filled with pictures. I reported it almost one year ago. I'd expect at least, lots pressure from distros like SUSE towards the OO.org developers to fix this kind of nasty bugs. \n\nMy sister is now using KOffice for that kind of work."
    author: "jadrian"
  - subject: "Re: Just give me a break."
    date: 2004-10-07
    body: "Well, OOo is currently necessary. It more or less imports and exports most MS Office files without problems, it has a lot of features, it doesn't crash.\nBig downside: slow and huge. \nGecko, well, I don't know, KHTML is cool.\nDBUS: this has the potential to become really cool. It doesn't come from GNOME. It doesn't even link to glib. It will make KDE startup faster (since it's already running at that time and doesn't have to be started like dcopserver). Kernel messages (e.g. hotplugging) might come via DBUS. We will be able to talk to KDE and Gnome and <you name it> apps via DBUS.\n\nBye\nAlex\n"
    author: "aleXXX"
  - subject: "Re: Just give me a break."
    date: 2004-10-07
    body: "What do you mean it doesn't have to be started?  As far as I know a DBUS server is started for each user, in addition to the system DBUS server."
    author: "Navindra Umanee"
  - subject: "Re: Just give me a break."
    date: 2004-10-08
    body: "I guess it will be started by default when the user logs in, so it's already there when the KDE startup starts. Or if you are running a gnome desktop and start a KDE app then it will be already running.\n\nBye\nAlex\n"
    author: "aleXXX"
  - subject: "Re: Just give me a break."
    date: 2004-10-07
    body: "about that hal stuff. from what i understand the linux kernel have a hal in only the strictest sense if at all. the \"hal\" i belive is that you will find a cdrom under /dev/cdrom if you have the system set up nicely, or under /dev/hd** if you dont...\n\nfor devices like digital cameras, webcams and so forth your app will still have to understand what is comeing out of the device file for it to be usefull. the hal in the works is supposed to be a bit like directx. you code towards it and it will then act as a translater between the device and the app. hell it may even help in allowing access by multiple apps if needed...\n\n"
    author: "hobgoblin"
  - subject: "Re: Just give me a break."
    date: 2004-10-07
    body: "| about that hal stuff. from what i understand the linux kernel have\n| a hal in only the strictest sense if at all. the \"hal\" i belive is\n| that you will find a cdrom under /dev/cdrom if you have the system\n| set up nicely, or under /dev/hd** if you dont...\n\n...or under /dev/pcd* if it is a parallel port cdrom, or under /dev/cm206cd if \nit is a Philips LMS CM-206 cdrom, but for the Philips LMS CM-205 you need to \nlook out for /dev/cm205cd... and so on and so on. The (Linux) kernel exposes \ndevices as a loose bag of drivers, device nodes and buses. For the \napplication developer is it a nightmare to make sense of, and it keeps on \nchanging as new hardware support is added. HAL solves this problem by \norganising and reporting all of the hardware that you have in a form designed \nwith applications in mind. This is how HAL reports my cdrom:\n\nudi = '/org/freedesktop/Hal/devices/block_22_0'\n\u00a0 info.udi = '/org/freedesktop/Hal/devices/block_22_0' \u00a0(string)\n\u00a0 storage.hotpluggable = false \u00a0(bool)\n\u00a0 storage.cdrom.write_speed = 2816 \u00a0(0xb00) \u00a0(int)\n\u00a0 storage.cdrom.read_speed = 7040 \u00a0(0x1b80) \u00a0(int)\n\u00a0 storage.cdrom.support_media_changed = true \u00a0(bool)\n\u00a0 storage.cdrom.dvdplusrw = false \u00a0(bool)\n\u00a0 storage.cdrom.dvdplusr = false \u00a0(bool)\n\u00a0 storage.cdrom.dvdram = false \u00a0(bool)\n\u00a0 storage.cdrom.dvdr = false \u00a0(bool)\n\u00a0 storage.cdrom.dvd = false \u00a0(bool)\n\u00a0 storage.cdrom.cdrw = true \u00a0(bool)\n\u00a0 storage.cdrom.cdr = true \u00a0(bool)\n\u00a0 storage.removable = true \u00a0(bool)\n\u00a0 storage.firmware_version = 'OS0B' \u00a0(string)\n\u00a0 storage.drive_type = 'cdrom' \u00a0(string)\n\u00a0 info.product = 'LITE-ON LTR-16102B' \u00a0(string)\n\u00a0 block.storage_device = '/org/freedesktop/Hal/devices/block_22_0' \u00a0(string)\n\u00a0 storage.physical_device = '/org/freedesktop/Hal/devices/ide_1_0' \u00a0(string)\n\u00a0 storage.vendor = '' \u00a0(string)\n\u00a0 storage.model = 'LITE-ON LTR-16102B' \u00a0(string)\n\u00a0 storage.automount_enabled_hint = true \u00a0(bool)\n\u00a0 storage.no_partitions_hint = true \u00a0(bool)\n\u00a0 storage.media_check_enabled = true \u00a0(bool)\n\u00a0 storage.bus = 'ide' \u00a0(string)\n\u00a0 block.minor = 0 \u00a0(0x0) \u00a0(int)\n\u00a0 block.major = 22 \u00a0(0x16) \u00a0(int)\n\u00a0 info.capabilities = 'block storage.cdrom storage' \u00a0(string)\n\u00a0 info.category = 'storage' \u00a0(string)\n\u00a0 info.parent = '/org/freedesktop/Hal/devices/ide_1_0' \u00a0(string)\n\u00a0 block.device = '/dev/hdc' \u00a0(string)\n\u00a0 block.is_volume = false \u00a0(bool)\n\u00a0 block.have_scanned = false \u00a0(bool)\n\u00a0 block.no_partitions = true \u00a0(bool)\n\u00a0 linux.sysfs_path_device = '/sys/block/hdc' \u00a0(string)\n\u00a0 linux.sysfs_path = '/sys/block/hdc' \u00a0(string)\n\u00a0 info.bus = 'block' \u00a0(string)\n\nAll of the vitial information is broken down and organised reguardless of where exactly it came from.\n\n--\nSimon \u00a0 \u00a0 \u00a0 \u00a0 \u00a0 \u00a0 \u00a0 \u00a0 \u00a0 \u00a0 \u00a0 \u00a0 \u00a0 \u00a0 \u00a0 \u00a0 \u00a0 \u00a0 \u00a0 \u00a0 \u00a0 \u00a0 \u00a0 \u00a0 \u00a0 \u00a0 \u00a0 \u00a0 \u00a0 \u00a0 \u00a0 \u00a0 \u00a0 \u00a0 \u00a0 \u00a0 \u00a0 \u00a0 \u00a0 \u00a0 \u00a0 \u00a0 \u00a0 \u00a0 \u00a0 \u00a0 \u00a0 \u00a0 \u00a0 \u00a0 \u00a0 \u00a0 \u00a0 \u00a0 \u00a0 \u00a0"
    author: "Simon Edwards"
  - subject: "Re: Just give me a break."
    date: 2004-10-07
    body: "\"and questionize whether HAL, DBUS, Gecko are all that good of ideas.\"\n\nI mean, with well-written and founded statements like that, how can anyone arguize?"
    author: "Joe"
  - subject: "Re: Just give me a break."
    date: 2004-10-08
    body: "Although I don't like people making fun of (I assume) non-native writers of English that one made me laugh. \n"
    author: "cm"
  - subject: "Re: Just give me a break."
    date: 2004-10-08
    body: "I agree with your view regarding OOo and Gecko. They're both bloated monstrosities and don't really fit into KDE. But sometimes I have to use OOo because Koffice is just not there yet. For instance, have you ever tried to produce nice charts from your spreadsheet in kspread and then tried to print them? I gave up and went to OOo Calc for that. But, what I can't do with OOo is control it with scripts via DCOP! This is the greatest asset KDE has! I use it quite frequently, for instance to fill in spreadsheet cells and other stuff.\n\nRegarding Gecko and KHTML, some of our users found out that Konqueror renders some pages just fine which Mozilla/Firefox just balk on. And after I showed them the DCOP-Kaction bridge and how to use it to script Konqueror they got completely hooked. The challenge was to save copies of certain pages automatically and neither wget nor curl could do it because of some javascript stuff in the page. With Konqueror and DCOP it was childs play. So, before you through away DCOP and replace it with DBUS please make sure that KDE doesn't loose any of the current DCOP functionality! This is one of the main selling points for KDE IMO.\n"
    author: "ac"
  - subject: "SuSE 9.2"
    date: 2004-10-07
    body: "SUSE 9.2 was just announced by the way :)\nhttp://www.suse.com/en/company/press/press_releases/archive04/92.html"
    author: "John Novell/SUSE Freak"
  - subject: "Re: SuSE 9.2"
    date: 2004-10-07
    body: "The absence of any mention of a Personal version is \"interesting\"."
    author: "Datschge"
  - subject: "Re: SuSE 9.2"
    date: 2004-10-07
    body: "It's not. There is no SUSE 9.2 Personal edition."
    author: "Daniel Molkentin"
  - subject: "Re: SuSE 9.2"
    date: 2004-10-07
    body: "I'm glad, 9.1 Personal Edition was an abomination to me :)"
    author: "SomeGuy"
  - subject: "Re: SuSE 9.2"
    date: 2004-10-07
    body: "SUSE 9.1 Personal was KDE only with Kontact, Konqueror, KDE/OOo.  So I guess Novell put a stop to that."
    author: "ac"
  - subject: "Re: SuSE 9.2"
    date: 2004-10-07
    body: "Or it didn't pay off the additional efforts (creation, handbook, package, shipping, advertizing) for it. Note that if it's all but the first of this reasons there may be still a downloadable version as currently under different name.\n"
    author: "Anonymous"
  - subject: "Re: SuSE 9.2"
    date: 2004-10-08
    body: "well .. it's the first press text from SuSE mentioning two Desktops in the order \"blah... SuSE Linux 9.2 includes\"\n\"Gnome and  even KDE\"\n  with 9.1 it was\n\"KDE and even Gnome\"\n\nthe things they are a changing..."
    author: "Thomas"
  - subject: "Re: SuSE 9.2"
    date: 2004-10-09
    body: "Huh?\n\nin the short version as well as the long version it's \"KDE 3.3 and GNOME 2.6 desktop\". Furthermore the order of development tools is \"KDevelop, Eclipse and Mono,\". I think that's pretty clear ... "
    author: "TicTac"
  - subject: "KMozilla"
    date: 2004-10-07
    body: "I'm very excited about the prospect of Mozilla as a Qt app.  Better integration is always good--and I'm hoping that perhaps it also fixes some of the suckage in Mozilla's printing system.\n\nWell, you can always hope...any chance of a KFirefox?\n"
    author: "ac"
  - subject: "Re: KMozilla"
    date: 2004-10-07
    body: ">Well, you can always hope...any chance of a KFirefox?\n\nYes ;-)\nhttp://www.kdedevelopers.org/node/view/615"
    author: "Anonymous"
  - subject: "Re: KMozilla"
    date: 2004-10-07
    body: "what he is talking about is no Kmozilla. you will still be using konqueror, and you wont be noticing you're using Gecko, Mozilla's rendering engine, instead of Khtml. except that its slower, of course, and bigger, of course, and it renders a few pages better.\n\nI'd say offer it as an option. Macintosh choose Khtml over Gecko, they are enhancing Khtml, I think gecko is not the right choice for suse. they should maybe cooperate with Macintosh for enhancing Khtml."
    author: "superstoned"
  - subject: "Re: KMozilla"
    date: 2004-10-07
    body: "Apple choose Khtml over Gecko. Macintosh was just an Apple product. ;)\n\nI'm coming from a Gnome background. Still I think I like the khtml engine more than Gecko, mostly because it's smaller."
    author: "rboss"
  - subject: "Re: KMozilla and Gecko vs. KHTML"
    date: 2004-10-07
    body: "What I don't understand is that, for some time now, in KDE you have been able to simply choose your renderer, which is KHTML by default.  I know a lot of people talk about changing it over to Gecko because they prefer it, but the interview made it sound like Konq coul only be used with KHTML. What's up with that, and why is it \"so much work?\""
    author: "Daniel Bo"
  - subject: "Re: KMozilla and Gecko vs. KHTML"
    date: 2004-10-07
    body: "you was only able to choose between KHTML and Opera's engine, Gecko was not possible. it is now."
    author: "superstoned"
  - subject: "Re: KMozilla"
    date: 2004-10-08
    body: "I think the great point using gecko is to get the XUL technology working soon on KDE and compete with the future MS Avalon strategy."
    author: "john the anonymous"
  - subject: "So what is happening with Safari Patches????"
    date: 2004-10-07
    body: "As one of those bystanders of us who don't read the dev lists, I'm confused.\n\nApple seems to be using Safari quite well, and I'm assumming that will be their \"Enterprise\" browser.  Yet this interview seems to suggest dropping KHTML in favor of Gecko.  \n\nSo what is apple doing differently?  Are they actually doing more developement that is not trivial to roll back into KHTML?  (For that matter apple stated that they used KHTML instead of Gecko because it was easier to code for and a cleaner, lighter code base).\n\nI run kde on the desktop but use Firefox mainly because gmail works with it (wow, gmail also works with safari).\n\nThis just seems silly to me...."
    author: "foobie"
  - subject: "Re: So what is happening with Safari Patches?"
    date: 2004-10-07
    body: "Short answer: There are no Safari patches.\n\nLong answer: There is no shared resource for KHTML/KJS's source code, instead Apple decided to develop KHTML/KJS further in house and releases them as WebCore/JavaScriptCore. There never were and still aren't enough developers working on KHTML/KJS to really keep up with all the changes done by Apple which aren't available as patches nor are documented. And today the sources for both versions are different enought to make \"back merges\" non-trivial. Now most improvements on KHTML are done more or less completely by KHTML developers on their own, just taking a look at WebCore or Gecko to see how particular issues are solved there. As one developer noted on a mailinglist regarding this topic, solving problems on your own is way more interesting than trying to merge two increasingly different source bases. However I'm sure people looking at merging more Apple improvement into KHTML/KJS are always welcome.\n\nYou see there are many areas where KDE could make good use of more developers."
    author: "Datschge"
  - subject: "Re: So what is happening with Safari Patches?"
    date: 2004-10-07
    body: "Interesting way for a company to take advantage of open source.\n\nTake a (l)gpl'ed product and fork it so fast that the original developers can't keep up with it.\n\nIt seems like a shame and contrary to the spirit of open source.  (Let's hope MS doesn't learn too much from apple ;) )\n\n"
    author: "foobie"
  - subject: "Re: So what is happening with Safari Patches?"
    date: 2004-10-07
    body: "> Interesting way for a company to take advantage of open source.\n \n> Take a (l)gpl'ed product and fork it so fast that the original developers can't keep up with it.\n \nI don't think that's a fair evaluation. Ask some rational questions. What is Apple supposed to do after they have made their changes? Wait for KDE developers to catch up before the make any more? There are several things we want from webcore to use with Quanta, but it just looks like a long time before a lot of them can be brought in and it's no simple merge.\n\nOpen source is a marvelous thing, but unfortunately for a lot of people it's a mythical concept that works this way... Because the source is open zillions of developers constantly review and update it. Of course a \"zillion\" is a fictitious number and therein lies the truth. A remarkably small number of developers, as in a tiny fraction of a percent of all users, do an amazing job of producing software. Even the percent who \"contribute\" by griping is remarkably small. ;-)\n\nConsider the numbers... KDE has millions of users, hundreds of developers and how many core applications? Do the math. Better yet learn to code. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: So what is happening with Safari Patches?"
    date: 2004-10-07
    body: "The nice thing would be to send the changes back as a series of patches, or atleast keep openly track of changes in their release, so we had a kind of changelog. Currently we have no idea of whether or not a bug is fixed in WebCore, or when we spot a change what is supposed to be good for."
    author: "Carewolf"
  - subject: "Re: So what is happening with Safari Patches?"
    date: 2004-10-07
    body: "I dont understand why they dont help... they where quite cooperative, the first time they did sent patches or at least a comprehensive list of changes, did they?"
    author: "superstoned"
  - subject: "Re: So what is happening with Safari Patches?"
    date: 2004-10-07
    body: "They still release a comprehensive set of changes in each version of Webcore, but most things rely on other things to be merged. And merging is very hard to do right without breaking stuff. A shared code base would probably been the best thing to do in hindsight. @ a khtml.org or something."
    author: "anon"
  - subject: "Re: So what is happening with Safari Patches?"
    date: 2004-10-07
    body: "So does this mean that the Safari/khtml thing is screwed?  Do we bascially have two seperate code bases that don't crossover at all?   What need to happen to get these two groups working together again?  Gecko is ok, but from a design standpoint khtml is far far superior and I just hate to see what could be one of our best deverlopment resources waisted.\n\nBobby"
    author: "brockers"
  - subject: "Re: So what is happening with Safari Patches?"
    date: 2004-10-08
    body: "Pretty much all developers working on KHTML also/mainly work on other areas within KDE, while KHTML definitely needs someone to focus on it full time (and doing so within KDE, not for Apple). When looking on CIA (see some posts further down this page) the only one who seems to work on KHTML exclusively is Germain Garand, someone should hire him to let him do so full time. =)"
    author: "Datschge"
  - subject: "Re: So what is happening with Safari Patches?"
    date: 2004-10-08
    body: "I think it is a very fair evaluation, to be honest. It's well known how to have corporate hackers work well with volunteer hackers, it's done in open source projects like the kernel, Wine, and yes KDE/GNOME all the time. You send back patches in series, with a detailed changelog. You should develop those patches in the open, with discussion on the mailing lists.\n\nIn other words, you do not do what Apple has done - effectively fork the codebase in secret and then give the original developers a huge unworkable patch dump. I've had to deal with such things from TransGaming in Wine and they're basically useless, especially as often they duplicate code already written by volunteers and usually nobody understands the changes except developers who aren't in the community. The way CodeWeavers does it is *much* preferable (disclaimer: I am biased, I work for CodeWeavers. But I wouldn't work for them if they didn't interact with the community well).\n\nTo be frank it doesn't surprise me that KHTML has forked, Apple clearly have no interest in working with the community on it and demonstrated this from the start. They fulfill their legal obligations and do no more."
    author: "Mike Hearn"
  - subject: "Re: So what is happening with Safari Patches?"
    date: 2004-10-07
    body: "Thanks again for your answer-\n\nA couple more questions:\n\nWhere are the mailing lists?  (If you have the link to the developer you mentioned that would be great)\n\nAlso is it possible to do the same thing they did to us back to them?  (Basically take their fork and wrap it back in as a KHTML replacement.  Call it KHTML2.0 or something.)\n\nIt just seems like there should be some sort of expose on this.  Someone needs to put a writeup in a blog somewhere and get /. or osnews to link to it."
    author: "foobie"
  - subject: "Re: So what is happening with Safari Patches?"
    date: 2004-10-07
    body: "> Where are the mailing lists? \n\nhttps://mail.kde.org/mailman/listinfo/khtml-devel\nhttps://mail.kde.org/mailman/listinfo/kfm-devel"
    author: "Anonymous"
  - subject: "Re: So what is happening with Safari Patches?"
    date: 2004-10-08
    body: "\nOf cause it's possible to take as much of Apple's WebCore/JavaScriptCore source code as you like and do what you want with it.  Create a KHTML 2.0 with it if you want.  \n\nHowever, the problem is not down to access to the code, or any kind of legal obstacle, it's purely man hours (or the current shortage of) on the KHTML devel side.  That's not Apple's fault.\n\nYou seem to think that Apple should write for two different sets of source trees, just because they took the Open Source code from KDE in the first place.  Why should they do that?  They didn't ask the KDE team to re-write KHTML for them in the first place so they could easily integrate it into OSX.  They did all the leg work themselves.  Now they've merged that code into their own framework, and that's the framework they're writing to.   The source code for WebCore/JavaScriptCore is Open Source, so others can take that and make use of it for themselves if they want to.  That can be the KDE team, or someone completely different.  That's what Open Source means!!  What it doesn't mean, is that you have to write someone else's applications for them, as you seem to want.\n\n "
    author: "NeonDiet"
  - subject: "Re: So what is happening with Safari Patches????"
    date: 2004-10-07
    body: ">I run kde on the desktop but use Firefox mainly because gmail works with it\n\nFrom what I've read on the lists, gmail should now work on konqi too (http://lists.kde.org/?l=kfm-devel&m=109690082612439&w=2). Are there any more problems? Is it worth building kde cvs?"
    author: "Anonymous"
  - subject: "Re: So what is happening with Safari Patches????"
    date: 2004-10-07
    body: "Good to see its being worked, it would've been bad had GMail gone out of beta with no Konqueror capability in sight. \n\nI pretty much just use Firefox now, which is annoying since some things (like saving a file without having to open a file save dialog) is easier in Konqueror."
    author: "Ian Monroe"
  - subject: "Re: So what is happening with Safari Patches????"
    date: 2004-10-07
    body: "this thread might be interesting to read: https://mail.kde.org/pipermail/khtml-devel/2004-July/001068.html\n\n"
    author: "anon"
  - subject: "Re: So what is happening with Safari Patches????"
    date: 2004-10-07
    body: "No, that's not true. People just got this whole thread wrong. Enterprise is not Gmail. Enterprise is for example, among others, SAP. SAP doesn't support Safari/KHTML. SAP supports Mozilla. The story ends.\n\nAnd gmail also works with KHTML now."
    author: "Correction"
  - subject: "Re: So what is happening with Safari Patches????"
    date: 2004-10-08
    body: "gmail fixes went in this week.\n\nDerek"
    author: "Derek Kite"
  - subject: "Gecko rendering better ?!"
    date: 2004-10-07
    body: "I really don't understand it when people claims that Gecko renders pages better than KHTML. Most webpages looks better in KHTML than in Gecko on my desktop, as long as KHTML does not choke on the HTML. As I see it, this is because of more \"compatibility patches\" in Gecko and wider acceptance in webmaster circles leading to less incompatible HTML and JavaScript using it.\nBut KHTML is surely the superior renderer from an aestethic POV.\n\nI use Firefox mainly for my webbank, because of the lack of support for secure java applets in konqueror. I also use Firefox on rare occations when pages i need to use is giving Javascript problems. Apart from that I'd prefer Konqueror anytime. "
    author: "Anders"
  - subject: "Re: Gecko rendering better ?!"
    date: 2004-10-07
    body: ">  lack of support for secure java applets in konqueror\n\nHu? http://www.konqueror.org/javahowto/ \"Getting HTTPS support for Applets\""
    author: "Anonymous"
  - subject: "Re: Gecko rendering better ?!"
    date: 2004-10-07
    body: "The HOWTO is out of date. It refers to ancient java versions. It seems that I need to install the java sdk from sun, which I'm now trying, since sun jdk 1.4.2 does not seem to conatin the required files. The jsse page at sun claims that it has been integrated with the 1.4 sdk. Eeew, HUGE downloads probably for nothing :(\nBut I try."
    author: "Anders"
  - subject: "Re: Gecko rendering better ?!"
    date: 2004-10-08
    body: "Yes somebody should update that docu :-).\nBtw. in kde > 3.2 if you enable 'Use KIO', the jsse isn't used because KIO download all code then.\nBtw2. secure applets can also mean that these applets should get more privileges than 'normal' ones, like accessing you hard drive. There is some code for it in kde 3.3, but not finished and hardly tested. (If someone wants to give a helping hand, like the certificate chain verification, that would be really nice) "
    author: "koos"
  - subject: "Re: Gecko rendering better ?!"
    date: 2004-10-07
    body: "Being a webmaster myself I believe I have the expertise to tell about the quality of rendering. Stating the KHTML renders better then Gecko is only proof that you have never created a CSS-based site for KHTML. They still have *way* to go before the CSS engine will become really useful.\nIt took us *weeks* to find workarounds for KHTML's weaknesses. You can not seriously state that KHTML renders better then Gecko."
    author: "Samuel"
  - subject: "Re: Gecko rendering better ?!"
    date: 2004-10-07
    body: "Well, what I mean is that what I see on my screen looks better using konqueror.\nI'm using mozilla right now, and the drawing (is that a better term than rendering then) is terrible.\nI se only very few sites where konqueror falls short due to CSS, but maybe my browsing is too limited. I have never had any problems myself though, with the relatively simple sayout that I like."
    author: "Anders"
  - subject: "Re: Gecko rendering better ?!"
    date: 2004-10-07
    body: "There are several fairly simple CSS rendering bugs with Konq. \n\nAs one example, there's a fairly obvious bug which you can see on many pages that use absolutely positioned elements (yes, it's in the bug reporting system, and has been for a year or so). I use these to implement page-number sidenotes on all the HTML editions of Project Gutenberg texts which I produce. As a result of Konq's flaws, these are much less readable in Konq than in any other modern browser.\n"
    author: "Jon Ingram"
  - subject: "Re: Gecko rendering better ?!"
    date: 2004-10-08
    body: "I love Konqueror the file manager, but dislike the browser.  The browser's just really poor at rendering pages, and it's a little too integrated with the FM.  For example, I like HTML documents to open in a text editor in the FM, but that behavior isn't exactly desirable for browsing.\n\nI'm not hoping that KHTML development will stop, but I'm hoping that these absurd contentions about it being better at displaying web pages than Mozilla and Opera would."
    author: "Zooplah"
  - subject: "Re: Gecko rendering better ?!"
    date: 2004-10-08
    body: "I have this nagging feeling that it's Novell's fault that there's not enough manpower on KHTML. They could easily afford one or two developers to work fulltime on KHTML -- maybe even stretch it to three. One for dhtml, one for css2 and one to integrate Apple's work. \n\nThat way, they wouldn't have to shed crocodile tears about KHTML and they wouldn't be stuck with taking shortcuts, but could plan for the long term."
    author: "Boudewijn Rempt"
  - subject: "Re: Gecko rendering better ?!"
    date: 2004-10-08
    body: "And one or two KMail develoers, two or three for KDevelop and of corse they should hire 10-20 KOffice developers. Or simply about 200 KDE developers. Why don't they hire > 5000 developers to outflank MS?\nMaybe they have to look at their costs?!?"
    author: "Birdy"
  - subject: "Re: Gecko rendering better ?!"
    date: 2004-10-08
    body: "Well, they already hired the KTop developer :-)\nBut seriously, you're right. Some guys, Waldo, David, etc, are really the people that bring KDE to a higher level, obviously not the one interviewed."
    author: "koos"
  - subject: "Gecko is better than the old engine but.........."
    date: 2004-10-10
    body: "Lately I find myself using khtml by way of Konqueror 90% of the time. Konqueror needs a little bit more profile differences between web and file browsing but the khtml itself has been getting a big boost by the Mac people who are using the code to make a new IE free web browser and putting the changes back in the tree for KDE to use. The Konqueror interface itself needs an easy way to set different home pages and button layouts for both profiles but the khtml rendering these days is awesome! Not many people use the gecko engine in Konqueror which is simply a menu click away as long as the gecko bindings were installed and there is a gecko based browser installed. I wonder when a khtml based browser will show up for windows? I know one will exist in Mac very soon if it doesn't exist already. I use Konqi for banking as well but not with a native agent string as many do not recognize it even though it says \"Mozilla 5 compatible\" but the Konqueror name throws it off. "
    author: "David W Studeman"
  - subject: "Language"
    date: 2004-10-07
    body: "Let me put another issue on the table: Languages. Support for all European languages is crucial when you want to sell it to the EU.\n\n"
    author: "gerd"
  - subject: "don't give up on KHTML."
    date: 2004-10-07
    body: "Don't let it die. There are loads and loads of sites it cannot render properly but the speed it has over Gecko is so great I for one will still keep using it when I can."
    author: "c"
  - subject: "Re: don't give up on KHTML."
    date: 2004-10-07
    body: "That makes two of us. I really hope KHTML continues being developed.\nIt's an outstanding piece of software."
    author: "John KHTML Freak"
  - subject: "Re: don't give up on KHTML."
    date: 2004-10-08
    body: "Count me in too.\n"
    author: "ac"
  - subject: "Re: don't give up on KHTML."
    date: 2004-10-08
    body: "I agree.  Keep up good work!"
    author: "Dominic"
  - subject: "Re: don't give up on KHTML."
    date: 2004-10-08
    body: "same here I love khtml. I don't want that patchy gecko stuff."
    author: "Robert"
  - subject: "Re: don't give up on KHTML."
    date: 2004-10-08
    body: "Me as well. Although what I'd actually like would be for Opera to be open sourced and integrated with KDE (it's the only non-KDE app I still use on a regular basis; the GIMP would be the second if I had any need for it), but that's obviously not going to happen, so making Konqueror more Opera-like is the next best thing ;). KHTML > Gecko for that."
    author: "Illissius"
  - subject: "Re: don't give up on KHTML."
    date: 2004-10-08
    body: "In fact, I would appreciate being able to choose which engine to run. khtml is fast, low memory, etc. I will be choosing khtml"
    author: "a.c."
  - subject: "Re: don't give up on KHTML."
    date: 2004-10-09
    body: "I like it very much and us it every day. Don't give up khtml (neither do koffice).\nWolfgang"
    author: "Wolfgang"
  - subject: "kde needs more developers ?"
    date: 2004-10-07
    body: "if you look at these stats :\n\nhttp://cia.navi.cx/stats/project?s_catalog=3\n\nyou can see that kde has not many developers compared to the other projects."
    author: "chris"
  - subject: "Re: kde needs more developers ?"
    date: 2004-10-07
    body: "Where do you see on this page an information about the developer amount?"
    author: "Anonymous"
  - subject: "Re: kde needs more developers ?"
    date: 2004-10-07
    body: "Unless i'm reading it wrong it seems to show that KDE is the most active OSS project."
    author: "Paul"
  - subject: "Re: kde needs more developers ?"
    date: 2004-10-08
    body: "...which means that the KDE developers achieve that much with very little resources. That speaks for the quality of the Qt toolkit and the KDE framework."
    author: "cm"
  - subject: "Re: kde needs more developers ?"
    date: 2004-10-08
    body: "...and, I should add, also reflects the quality of the developers who built that base."
    author: "cm"
  - subject: "Re: kde needs more developers ?"
    date: 2004-10-07
    body: "According to the page you listed KDE is the most active Open Source project listed.    Where are you getting # of developers?\n\nBobby"
    author: "brockers"
  - subject: "Re: kde needs more developers ?"
    date: 2004-10-08
    body: "Who was/how many people were working on a project, subproject, package or module can be seen on CIA by browsing the respective pages. KHTML for example had 19 people touching some code there in the last 2.52 months, see http://cia.navi.cx/stats/project/kde/kdelibs/khtml"
    author: "Datschge"
  - subject: "Re: kde has 561 developers"
    date: 2004-10-08
    body: "so as you can see on CIA , kde has 561 developers (see the related column on the left) - can anyone provide numbers of other projects (closed source) ??\n\nhow many developers does microsoft have on - for example - longhorn ? and they work 8 hours a day i suppose...."
    author: "chris"
  - subject: "Re: kde has 561 developers"
    date: 2004-10-09
    body: "Keep in mind that most of those developers are translators. Artists and stuff are also included, not only programmers."
    author: "blacksheep"
  - subject: "Re: kde has 561 developers"
    date: 2004-10-09
    body: "There are usually not more than one or two CVS accounts for the leaders of a translation team."
    author: "Anonymous"
  - subject: "Re: kde has 561 developers"
    date: 2004-10-09
    body: "How many workers do TrollTech has on Qt, working 8 hours a day I suppose?"
    author: "KDE User"
  - subject: "Re: kde has 561 developers"
    date: 2004-10-09
    body: "26 (Source: http://conference2004.kde.org/slides/eirik.chambe-eng-keynote.tar.bz2)"
    author: "Anonymous"
  - subject: "All sorts of spelling/grammar mistakes..."
    date: 2004-10-07
    body: "\"I think D-BUS will come probably with KDE4, not earlier then that.\"\n*than that*\n-\n\"During your talk at aKademy you said we needed less toolkits. What do you mean by that?\"\n*fewer* when dealing with multiple items.\n-\n\"It is a real pain and doesn't work 100% reliable. \"\n*reliably 100% of the time*"
    author: "Joe"
  - subject: "Re: All sorts of spelling/grammar mistakes..."
    date: 2004-10-07
    body: "I believe Fab's email address is fabrice@kde.nl, and I know that he would appreciate having a native english speaker check articles for The Dot. So when you are ready to take the next step up from griping, you know what to do...\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Re: All sorts of spelling/grammar mistakes..."
    date: 2004-10-07
    body: "Ok, email sent."
    author: "Joe"
  - subject: "Re: All sorts of spelling/grammar mistakes..."
    date: 2004-10-08
    body: "Thanks for your email and offer to help. This is the way to go. \n\nCiao\n\nFab"
    author: "Fabrice Mous"
  - subject: "Re: All sorts of spelling/grammar mistakes..."
    date: 2004-10-07
    body: "Thanks, you have to realize that most of us are not native-English speakers :)"
    author: "anon"
  - subject: "What's about usability?"
    date: 2004-10-07
    body: "KDE has to many options that make it dificult to use when it comes to end users. If kde could do the same gnome has done, well, it will be a winner."
    author: "Me"
  - subject: "Re: What's about usability?"
    date: 2004-10-08
    body: "Nice try! Go troll somewhere else.\n"
    author: "ac"
  - subject: "Re: What's about usability?"
    date: 2004-10-08
    body: "I agree.\n\nAn enterprise desktop needs to be simple to help avoid help issues.\n\nI've been a linux user for nearly 10 years and a gnome user for a large portion of that.\n\nI found KDE to be very powerful and configurable but also very overwhelming for anyone that has come from a windows/gnome/mac background.\n\nOverwhelming is not what you want to support in a 1000+ enterprise desktop environment.\n\nIt's not good having 10,000 settings for everything as thats where users will break things and put more pressure on support.\n\nI think Novell is doing a great job attempting to address these issues and I have actually seen their work on the Novell Linux Desktop and it is quite amazing what they have done in such a short time."
    author: "suiciety"
  - subject: "Re: What's about usability?"
    date: 2004-10-08
    body: "> An enterprise desktop needs to be simple to help avoid help issues.\n\ngconftool-2 -t string -s \\\n   /apps/metacity/global_keybindings/run_command_1 \\\n   \"<Ctrl><Alt>Delete\"\n\ngconftool-2 -t string -s \\\n   /apps/metacity/keybinding_commands/command_1 \\\n   \"xscreensaver-command -lock\"\n  \ngconftool-2 -t string -s \\\n   /apps/metacity/global_keybindings/run_command_2 \\\n   \"<Mod4>c\"\n\ngconftool-2 -t string -s \\\n   /apps/metacity/keybinding_commands/command_2 \\\n   \"xmms --play-pause\"\n  \ngconftool-2 -t string -s \\\n   /apps/metacity/global_keybindings/run_command_3 \\\n   \"<Mod4>b\"\n\ngconftool-2 -t string -s \\\n   /apps/metacity/keybinding_commands/command_3 \\\n   \"xmms --fwd\"\n  \ngconftool-2 -t string -s \\\n   /apps/metacity/global_keybindings/run_command_4 \\\n   \"<Ctrl%gt;%lt;Shift>t\"\n\ngconftool-2 -t string -s \\\n   /apps/metacity/keybinding_commands/command_4 \\\n   \"aterm\"\n  \ngconftool-2 -t string -s \\\n   /apps/metacity/global_keybindings/run_command_5 \\\n   \"<Mod4><Shift>m\"\n\ngconftool-2 -t string -s \\\n   /apps/metacity/keybinding_commands/command_5 \\\n   \"aumix -v +5\"\n \ngconftool-2 -t string -s \\\n   /apps/metacity/global_keybindings/run_command_6 \\\n   \"<Mod4>m\"\n\ngconftool-2 -t string -s \\\n   /apps/metacity/keybinding_commands/command_6 \\ \n   \"aumix -v +5\"\n\ngconftool-2 -t string -s \\\n    /apps/metacity/global_keybindings/run_command_7 \\\n    \"<Mod4>e\"\n\ngconftool-2 -t string -s \\\n    /apps/metacity/keybinding_commands/command_7 \\\n    \"emacs\"\n\nHow do you explain this simple stuff on telephone in case a Customer calls ? It's easier to tell them to go into Control-Center->Regional&Accessibility->KHotKeys. I know this is a bad example but there are many other situations under GNOME where you have to manually alter GConf keys to get what you want.\n"
    author: "AC"
  - subject: "Re: What's about usability?"
    date: 2004-10-11
    body: "You can do it with gconf-editor."
    author: "Jo"
  - subject: "Re: What's about usability?"
    date: 2004-10-09
    body: "> An enterprise desktop needs to be simple to help avoid help issues.\n \nMy digital camera isn't simple and can you set the time on your VCR or program channels on a car radio the first time you've seen that model car? Computers are by nature not simple and software is by nature not universally configurable for all users.\n\n> Overwhelming is not what you want to support in a 1000+ enterprise desktop environment.\n \nNo, you want Kiosk mode and a competent administrator. KDE can help with the first part and it can lock down anything and everything on the desktop. I've met people who administer companies on this level with KDE. They love it."
    author: "Eric Laffoon"
  - subject: "Dual approach does not suit Novell"
    date: 2004-10-07
    body: "For a community distro (Fedora, Debian etc), supporting both GNOME and KDE makes sense, these distros attract developers.\n\nFor a corporate product, supporting both environments is simply idiotic. Remember these CIOs have likely never heard of KDE and GNOME, so the choice is meaningless to them. In fact in a vaccum they might actually make the wrong choice and be left high and dry by a vendor suggesting they do an about-face, doubling training costs. For new users in businesses, they want something that \"just works\", not a Mandrake-like kitchen sink.\n\nI know the developer community feels strongly that the receptionist at FooBar corp has a mature opinion on KHTML vs Gecko but do a gut-check here...as long as they can perform the tasks they perform in Windows in a predictable fashion, thats all they want."
    author: "Anonymous Person"
  - subject: "Re: Dual approach does not suit Novell"
    date: 2004-10-08
    body: "> ..as long as they can perform the tasks they perform in Windows in a predictable fashion, thats all they want.\n\nI second that !\n...but... A lot of intranet webbased CRM systems make heavy use of DHTML..., which is not very well supported by khtml... sad, but true. Still, webbased CRM systems can support an entry for Linux on the desktop. If it works on Linux, you could set up cheap boxes for employees without buying windows licenses (btw. that's what .net is all about: It's just there to prevent _interoperable_ webbased applications that need a modern browser and no windows os any more)"
    author: "Thomas"
  - subject: "Mmmmh?"
    date: 2004-10-08
    body: "Let me think,\nconsumers need all the choices but only 2 years of security updates? That is the reason why consumers have still many Windows 98 PC around? 2004 - 1998 = 2 years, correct?\n\nBusineses / enterprises look for 5 years of support (I'm not sure about that).\n\nThe main reason for (small) businesses and consumers to keep old OS versions around is that the hardware does not support the bloated new version. I think if Linux could offer an upgardeable but resource aequivalent version, most consumers would take it. It would be wise to distinguish between a core that is supported longer (and or upgraded) and a nice to have app level, where support is stopped after 2 years.\n\nAlso, don't do fake support the latest races. My KDE 3.3.0 on SuSe 8.2 (yes my cranky old machine would not handle a 9.1 well, not to mention the upgrade effort) just rendered my configuration and capability for signing and crypting e-mails brocken. Why? Because the underlying GnuPG and several libs have not been upgraded. That is painful, very painful.\n\nAnd don't get me started on Kontact (crashes all over the place, crappy partial integration, not worth the time waiting for it to install, but much needed to make a ral dent in the enterprise space) or Konquerer (which  I love an use daily, but it crashes with all sorts of plug-ins - Flash, Java, ..., and I can't control which site pulls those things on me. By the way PDF support looks very poor). Also there are a few nasty behaviors in KMail or KOrganizer, that should have been debugged before release.\n\nEnough of my rant. I wish KDE and SuSE a bright future (and me more stable releases)\n\nK<o>\nhttp://www.conficio.com/"
    author: "Kaj Kandler "
  - subject: "Re: Mmmmh?"
    date: 2004-10-08
    body: "> 2004 - 1998 = 2 years, correct?\n\n2004 - 1998 = 6\n"
    author: "AC"
  - subject: "nitpicker --- ;-)"
    date: 2004-10-08
    body: "n/t"
    author: "Thomas"
  - subject: "Re: Mmmmh?"
    date: 2004-10-08
    body: "SuSE Linux Desktop for example has longer support (4 years?) and fewer updates (every second year?). Just like SuSE Enterprise Server or Red Hat Enterprise Linux on the server. \nThe users targeted by \"ordinary\" SuSE packages usually update with every or every second release...\n"
    author: "Birdy"
  - subject: "Re: Mmmmh?"
    date: 2004-10-08
    body: "\"The main reason for (small) businesses and consumers to keep old OS versions around is that the hardware does not support the bloated new version.\"\n\nHmm. Well my scanner is not supported by WIN-XP, no driver, Linux has no problem with old hardware. The scanner has the same features than a new one, only the plug has changed to usb.\n\nWinXP creates hardware waste because of lack of commercial support for drivers.\n\nNew software is usually buggy but most parts of KDE are matured. Only releases and usage and active maintanance leads to more stable releases.\n\n"
    author: "gerd"
  - subject: "Please read Khtml-Developers ..."
    date: 2004-10-08
    body: "I never liked Mozilla. Since i use Kde (Version 1.1), i used Konqueror as Filemanager and Browser.While Konquerors usability as a webbrowser were very limited in former versions, Konqi now matured to a cool and fast browser.Since the very first versions of Konqi, i hoped that it will be some day like it is now - the swiss army knife with very fast startup, nice options and font rendering, anti-popup and cookie-handling.\n\nA long time ago i have choosen Konqueror as my main webbrowser because it felt right.Mozilla was already there and ways in front from the technical abilities, but i never liked it.It wasn't the look only.\n\nIt makes me very sad to read by people sentences like this:\n>It is a bit sad for KHTML and I hope that despite this people will still maintain it as >it is a nice lightweight browser. If it would be a purely technical decision, KHTML >has the better architecture, but sometimes you need to go the shortest way to >get to your target.\n\nPlease also support your veteran users !\nI often think the demands of ENTERPRISE BUSINESS or COMPANIES will set the\ndirection of Kde development.In favor of new users old ones are not so important anymore.People like me, that got accustomed with certain kde things and have their favorite applications, started to love applications and how they matured read are frightened by such statements.\n\nIt gives users feelings like \"Don't get used to do things this way, tomorrow it will be or feel different so don't start to like how it works or looks now !\"\n\nThe Kde developers started to work on konqueror because there was a need for a webbrowser that follows the look and feel of the 'K Desktop Environment'.\n\nIf Khtml is cleaner and technically superior why drop it or slow down development ?\nWhen this happens for Gecko, could that happen everytime kde developers find out that other technologies are some steps in front, to adopt this other technology ?\nWhat a waste of valuable programming efforts to have a short fast solution!\n\nIt is like capitulation.Like \"We don't get it working the way we want it (now), so we see who did it and take that existing one.\"\n\nSo my wish to the kde developers out there:\n\nPlease don't drop or neglect KHTML !\nUsers like me would be very sad :,(\nIf there is a need to integrate Gecko into Konqi please make it an option. \n\nPerhaps the only way to work with long lasting environments is to choose the console - i don't hope that.\n\nHeiko\nLong time Kde user and great fan of Konqueror"
    author: "Heiko"
  - subject: "Re: Please read Khtml-Developers ..."
    date: 2004-10-08
    body: "Nobody within KDE is suggesting dropping KHTML or even introducing a dependency to Gecko. The only thing that changed is that lazy distributors now have the choice packaging up to three different HTML rendering engines (KHTML, Opera, Gecko) for Konqueror."
    author: "Datschge"
  - subject: "Re: Please read Khtml-Developers ..."
    date: 2004-10-09
    body: "I use gentoo. So this wouldn't be a problem for me, i think.\nIf this is only a question of configure switches, then things are not so bad."
    author: "Heiko"
  - subject: "Re: Please read Khtml-Developers ..."
    date: 2004-10-09
    body: "Integrate gecko into konqueror because of the need of distributors seem to be ok to me since it was basically done in about 3 day at aKademy (I think). So it does not eat up much time.\nBut it would not be understandable why to throw away all this great work. khtml is cool and technically better than gecko. Keep it."
    author: "Wolfgang"
---
At <a href="http://conference2004.kde.org/">aKademy</a> <a href="http://www.kde.nl/people/fabrice.html">I</a> had the chance to talk to <a href="http://www.kde.org/areas/people/chris.html">Chris Schlaeger</a> about <a href="http://www.suse.com/">SUSE</a>, its relationship with the <a href="http://www.kde.org/">KDE community</a>, his view of the Linux enterprise desktop and the speed of development of several key features in KDE (a Dutch translation can be found at <a href="http://www.bartendavid.be/doc/artikels/schlaeger.html">Bart&David</a>).

<!--break-->
<style type="text/css">
  
.imgboxrt{
border:1px dotted #000;
float:right;
margin-left:5px;
margin-right:10px;
}

.imgboxlft{
border:1px dotted #000;
float:left;
margin-left:10px;
margin-right:5px;
}
  
  </style>

<!--IMAGE BOX RIGHT -->
<div align="center" class="imgboxrt" style="width:340px;">
<p><A href="http://static.kdenews.org/fab/interviews/suse-akademy/IMG_0066.JPG"><IMG src="http://static.kdenews.org/fab/interviews/suse-akademy/IMG_0066_small.JPG" alt="Kolab logo" border="0"></A><br><em>Chris and Fabrice</em></p>
</div>
<!-- IMAGE BOX RIGHT-->     

<p />  
<p><strong>Please introduce yourself and explain your role within the KDE project.</strong>
</p>
<p>My name is Chris Schlaeger and I'm the Vice President of Research and Development SUSE Linux at <a href="http://www.novell.com/">Novell</a>.  I'm a long time KDE developer and I used to be the maintainer of KSysguard and before that I worked on the previous version called KTop and I hacked on kdelibs. </p>
 
<p />
<p><strong>Not long ago Novell acquired two companies that deal with Linux: Ximian and SUSE. While Ximian is a derivative from the GNOME project, SUSE is well known for its support of KDE. How does this all come together?</strong></p>

<p>Better than most people seem to believe. Novell is committed to supporting both GNOME and KDE desktop environments in its Linux desktop. We are fortunate to have acquired a robust set of desktop technologies through our acquisitions of Ximian and SUSE LINUX, giving our customers a considerable amount of choice.</p>

<p>We are working on our next generation Enterprise Desktop currently called Novell Linux Desktop which will feature a KDE desktop as well as a GNOME desktop. In the enterprise market the situation is still very open regarding which desktop will have the greater following. For a Linux provider like Novell it is  a great opportunity to offer both desktops to our customers and see where the market is going.</p>

<p />
<p><strong>During your presentation at aKademy you mentioned that SUSE offers two product lines now: 
Novell Linux Desktop and Novell SUSE Linux Personal/Professional. What are the differences between these product lines?</strong></p>

<p>Novell's Linux desktop is currently still under development. We are still offering the SUSE Linux desktop, however this is based on the SUSE LINUX Enterprise Server 8 code base, which has  now been superseded  by version 9 (released in August of this year). This represents our business offering as opposed to our consumer product offering.</p>

<p>They both target very different user groups which have different requirements. We have the old traditional SUSE  products which really target the private user who is using Linux at  home. </p>
 
<p>The Personal/Professional versions are consumer products targeted at home users. Users who either want to do very little or very specific work with their PC like writing email, surfing the web, word processing, spreadsheets, printing and the like. For those people we have the Personal version. The Professional version is basically the swiss army knife of Linux. You've got everything in there that we feel is of some interest and benefit to our customers. Both products have a comparatively low purchase price and are therefore very cost effective. </p>

<p>We provide security updates for a period of 2 years  for these products which is something customers tend to forget. There is a lot of work that needs to be done to keep the products secure during their lifetime. A new version is released roughly every six months.</p>

<p>However, in the enterprise arena 2 years doesn't cut it as people want 3 or 5 years support at minimum. So for the enterprise customers we created a new product which was called SUSE Linux Desktop. The next version will be a Novell Linux Desktop which will be based on the SUSE LINUX Enterprise Server 9 code base and  combines the best of SUSE LINUX Desktop and Ximian Desktop. It will have a lifetime  of around 18months and we guarantee to provide support and maintenance for the product for up to five years. Also the quality assurance is much higher. In an enterprise arena you need to do integration tests to a much higher degree and we test extensively so that we don't inject any side effects when we provide fixes. That's the main reason why the enterprise versions are more expensive and also the software collection is more targeted to the needs of the customers we try to address.</p>

<p />
<p><strong>What do you think are the strong points of KDE as an enterprise desktop?</strong></p>

<p>KDE is a very good enterprise desktop environment and it offers all the functionality you would expect nowadays  from an enterprise desktop. So from a desktop perspective it is ready. But there are still missing features on the application side. People expect more and there are some areas like the <a href="http://www.openoffice.org/">OpenOffice.org</a> integration and the browser question that needs to be resolved.</p>
  
<p>Also accessibility is one of the hot topics where more work is necessary. But when there is a good foundation we can build on and a lot of work is being done in all areas to improve KDE. Also I'm very glad that the KDE team recognizes the enterprise market as a very important target audience they have in mind when they write software.</p>

<p />
<p><strong>Could you tell me any typical enterprise features of KDE you are using?</strong></p>

<p>One of the features for example is the <a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdelibs/kdecore/README.kiosk?rev=1.50;content-type=text%2Fplain;only_with_tag=KDE_3_3_0_RELEASE">Kiosk lock down mode</a>. That's clearly something you normally wouldn't use in a home setup. Well maybe you want to restrict the ability of your kids to do certain things ton your PC. That is also a way to use the Kiosk mode. But primarily the Kiosk functionality is meant for use as  an information terminal or on the corporate desktop where the system administrator just wants to expose the necessary functionality to the users. </p> 
 
<p />
<p><strong>Novell also has its own <a href="http://www.novell.com/coolsolutions/zenworks/">ZENworks</a>.  Are there any plans to integrate ZENworks with KDE ?</strong></p>

<p>ZENworks is an enterprise management console. ZENworks was originally written for Windows and now
 the next version called "ZENworks for Linux" will also support Linux. It is a complementary tool and  is 
 the tool that administrators use for mass administration. So it does software inventory management and
 it *controls* the lockdown. ZenWorks does not provide the actual lockdown functionality, but it
 provides the configuration values. On the client you would need the counterpart that actually knows 
 how to implement the values that ZENworks has.</p>

<p />
<p><strong>There have been some rumours, corroborated by some <a href="http://cvs-digest.org/?issue=aug202004">CVS commits</a>, which show work going on in <a href="http://pim.kde.org/">KDE-PIM</a> on a configuration wizard for the <a href="http://www.novell.com/products/groupwise/">Novell Groupwise</a> client. Also the aKademy <a href="http://dot.kde.org/1093691383/">interview with Will Stephenson</a> talks about the integration of Groupwise functionality in <a href="http://kopete.kde.org/">Kopete</a>. Can you elaborate more on this?</strong></p>


<p>We are working on the integration of KDE tools in the other products from Novell. Groupwise is the most prominent of these and is a collaboration tool that offers messaging, calendar and mail services to the user. You can now use KDE tools such as Kopete together with the Groupwise messenger and you can use <a href="http://www.kontact.org/">Kontact</a> to access the corporate calender and address-book and also for email.</p>

<p />
<p><strong>Just curious, how long did it take to program the Novell Groupwise integration into Kontact, Kopete and KAddressbook, and how many people were involved?</strong></p>

<p>It was quite amazing that we were able to do this. The messenger integration was done by Will Stephenson in 2 months. Even more surprising was the integration of Groupwise into Kontact. In less than a week developers got it running. We were able to exchange data from the server to the client.  Sure, there are still bugs in there and we need to iron these out but I'm glad that it was done so quickly.  I'm sure we can get it ready for the next deadline for the Novell Linux Desktop. </p> 

<p />
<p><strong>How will this evolve with regard to KDE and who will maintain this stuff?</strong></p>

<p>We have some KDE people on board and we currently have an <a href="http://www.suse.de/de/company/suse/jobs/suse_pbu/developer_kde.html">offer on our website for a KDE developer</a>, especially for the KDE-PIM area. One of the tasks would be to maintain the Groupwise and messenger integration.</p> 

<p />
<p><strong>During <a href="http://conference2004.kde.org/slides/chris.schlaeger_keynote_novell.pdf">your talk at aKademy</a> you said we needed fewer toolkits.  What do you mean by that?</strong></p>

<p>Linux is plagued by too many toolkits. We've got Tcl/TK, Java, Motif, Athena Widgets or the old X toolkit, GTK, and Qt, and all of them look and feel totally different. Applications written in those toolkits do not follow the same standards and guidelines and are a mess to use.  Especially if you have them side by side or you need to use them frequently. </p>

<p>Some of toolkits do not really cut it today. There is little support for accessibility, there is hardly any support for hotkeys. We just need to get rid of them. It was great that they were there and they served their purpose but I personally would  like to see as few toolkits as  possible in the future. They are still projects that come with their own toolkit like Mozilla and OpenOffice.org. I'm sure we can find a solution there that these toolkits can integrate and behave very well with the rest of the desktop.</p>

<p />
<p><strong>During aKademy the <a href="http://accessibility.kde.org/forum/">Accessibility Forum</a> and <a href="http://conference2004.kde.org/usabilityforum/usability_akademy.html">Usability Forum</a> were very successful. What is your opinion about these disciplines, and how should they fit into the KDE development process?</strong></p>
 
<p>I think it is important especially because the most interested enterprise customers we have are government agencies and they have strict rules on accessibility conformance (<a href="http://www.section508.gov/">section 508</a> is mandatory in most of the US in the government agencies).  If we want to get involved in that market then we need these features and they are not there yet.  So I am glad that there is work going on in these areas and SUSE is a very active contributor.  For instance, we have a blind developer on our team who did the first support for braille displays. SUSE Linux was the first distribution not only usable but also installable from scratch by a blind person using <a href="http://www.suse.de/en/private/products/suse_linux/prof/yast.html">YaST</a> in text mode. Of course text mode is fairly easy compared to graphical user interfaces and having a good off screen model and screen reader is a significant amount of work. <a href="http://www.trolltech.com/">Trolltech</a> and <a href="http://www.sun.com/">Sun</a> have done really a lot of work in this area and we really hope that with Qt4 and KDE4 we can profit from this and offer this technology to users.</p>
 
<p />
<p><strong>What will Novell/SUSE do to improve the experience on the desktop?</strong></p>

<p>We are working on OpenOffice.org to make it integrate better with the desktops.  On SUSE 9.1 you saw already the first fruits of this work - if you start OpenOffice.org it looks like a KDE application. It is a first step but it is far from complete: if you open a file you get a totally different file dialog that is not only awkward to use, but also doesn't fit into the KDE look and feel. The same for printing. That is still something we need to overcome but we are working on this and probably will have better integration with the next product.</p>

<p />
<p><strong>So that's one piece of the experience on the desktop, the OpenOffice.org part is there. Something going on with the <a href="http://www.mozilla.org/">Mozilla browser</a> part as well?</strong></p>

<p>Well that started on aKademy where we discussed this on the first day. The question came up during a discussion I had with <a href="http://www.openoffice.org/">Matthias Ettrich</a> and a few others. It is currently a pain to the user to need two installed browsers, one browser which works for this website and the other browser which works for the other website.  <a href="http://www.konqueror.org/">Konqueror</a> is fast and nicely integrated, but Mozilla renders more pages better. </p>
 
<p>Customers that do web application development heavily use DHTML and other special features that Konqueror doesn't handle very well and  it is a lot of work to implement this.  Although I like KHTML and the architecture quite a bit I am sad to say that probably the Gecko  rendering engine will be the dominant one used in the enterprise arena, and as KDE developers we've got to make sure that we can integrate Gecko fairly well into KDE.</p>
 
<p>So <a href="http://www.kde.org/areas/people/lars.html">Lars Knoll</a> and <a href="http://www.kde.nl/people/zack.html">Zack Rusin</a> <a href="http://dot.kde.org/1094924433/">started working on this at aKademy</a> and I was delighted when they put me aside and showed me what they have done in just three days. It is amazing! I think it is the right way to go! It  is a bit sad for KHTML and I hope that despite this people will still maintain it as it is a nice lightweight browser. If it would be a purely technical decision, KHTML has the better architecture, but sometimes you need to go the shortest way to get to your target.</p>
  
<p />
<p><strong>What about technology like <a href="http://www.freedesktop.org/Software/dbus">D-BUS</a> (the Desktop Bus) and <a href="http://www.freedesktop.org/Software/hal">HAL</a> (the Hardware Abstraction Layer)? What are your thoughts on that?</strong></p>

<p>I think D-BUS will come probably with KDE4, not earlier than that. It is a lot of work to implement that  properly but I think it is going in the right direction. We need to integrate the desktop and the operating system much deeper. So that you can control the hardware on the desktop or have the right feedback on the desktop.</p> 
  
<p>If I connect a camera currently I get a nice icon on the desktop where a Konqueror browser is opened. SUSE has that for quite a while, but if you look at how we implemented this ...  It is a real pain and doesn't work 100% reliable. The problem is with the hotplug system of the underlying OS and the way we have to communicate those kernel events into userspace. D-BUS hopefully will solve some of these problems in the long run. It is a lot of work but it is going in the right direction and I'm sure we will adopt that.</p>

 <p />
<p><strong>Do you think the KDE desktop should ready itself for that kind of technology?</strong></p>

<p>I think that there is a general understanding within the KDE developer community that D-BUS is the right way to go. Not for KDE3 but probably for KDE4. </p> 

<p />
<p><strong>Currently we have developers, translators and documentation writers as part of the KDE development process. How do you think other disciplines like accessibility and usability should participate in the development process?</strong></p>
 
<p>I think they should participate and the KDE project should adapt their structures to integrate those groups as well. It is important to have their work integrated but that is something you should ask the KDE release coordinator to see how we can fit these people into the process. But for example, who could have imagined that we would have more then 50 languages in KDE? Surely this will also happen to other areas like accessibility and usability, as there is some good work going on to get people involved.</p>
 
 <p />
<p><strong>Where would you like to see the future of KDE go, and what new features would you like to see in future releases?</strong></p>

<p>KDE has made tremendous progress over time. I joined the project when it only had a few dozen people and now it is 700 people strong and is constantly growing, and the enthusiasm of the project hasn't decreased at all. It is now not only working on the core part but also working on parts like accessibility, translation to many languages, documentation work, artwork and other areas which are not directly linked to the core code-hacking. I'm glad this is going on and we need more of this in the future. We need more applications in the future, and good accessibility support, OpenOffice.org integration and the browser problem are the most practical features would like to see in the next release. </p> 
  
 <p />
<p><strong>How will the relationship between Novell and KDE evolve?</strong></p>

<p>I'm sure that we are going to sponsor KDE development in the future. We have (I think) sponsored every major KDE event, like we did with aKademy this year. So why should we stop now?!  Novell is very much committed to KDE!</p>
