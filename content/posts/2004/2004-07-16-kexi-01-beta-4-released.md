---
title: "Kexi 0.1 Beta 4 Released"
date:    2004-07-16
authors:
  - "kteam"
slug:    kexi-01-beta-4-released
comments:
  - subject: "Debian package for Sid/Sarge"
    date: 2004-07-17
    body: "I made a debian package for Debian Sid/Sarge, it has not enough quality yet to send to the kalyxo project, so I upload to my own repository until I have enough skill to eliminate all the errors and warnings.\n\nTo install just add this line to your sources.list file\n\ndeb http://www.kdehispano.org unstable main\n\nand then run \"apt-get install kexi\".\n\nI hope this was usefull and made people probe and send bugs to the authors."
    author: "Pedro Jurado Maqueda"
  - subject: "Re: Debian package for Sid/Sarge"
    date: 2004-07-17
    body: "Great effort, we're looking for package maintainers for all other distros as well :)\n\n--\njs\nKexi Team"
    author: "Jaroslaw Staniek"
  - subject: "FireDuck?"
    date: 2004-07-17
    body: "The codename is FireDuck? Heh."
    author: "QV"
  - subject: "Re: FireDuck?"
    date: 2004-07-18
    body: "it's as good as Duckzilla, isn't it ?\n\nkudos for kexi and the great sense of humour guys :-)"
    author: "vruz"
---
The <a href="http://www.koffice.org/kexi/">Kexi</a> <a href="http://www.kexi-project.org/people.html">Team</a> today announced the immediate <a href="http://www.kexi-project.org/announce-0.1-beta4.html">availability of Kexi 0.1 beta 4</a> (<a href="http://www.kexi-project.org/changelog-0.1-beta4.html">change log</a>), codenamed "<i>FireDuck</i>", the newest preview release of the integrated environment for managing data aimed at developers and experienced users.



<!--break-->
