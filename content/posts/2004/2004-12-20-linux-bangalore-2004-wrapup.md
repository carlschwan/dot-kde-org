---
title: "Linux Bangalore 2004 Wrapup"
date:    2004-12-20
authors:
  - "swheeler"
slug:    linux-bangalore-2004-wrapup
comments:
  - subject: "KDE CVS screenshots from Scott's Laptop anyone??"
    date: 2004-12-19
    body: "He's working on KDE4... and if his KDE CVS provides KDE4CVS, then a screenshot from his laptop would be nice. No thanks, I don't want to compile KDE4CVS myself...\n\nThanks a lot for the screenshot links :-)\n"
    author: "anonymous"
  - subject: "Re: KDE CVS screenshots from Scott's Laptop anyone??"
    date: 2004-12-19
    body: "Uhm, I believe you misunderstood -- I'm working on some things that will be a part of KDE 4, but the CVS version on my laptop isn't significantly different from the standard \"CVS HEAD\" branch of KDE which will become KDE 3.4..."
    author: "Scott Wheeler"
  - subject: "Re: KDE CVS screenshots from Scott's Laptop anyone"
    date: 2004-12-19
    body: "Oh, sorry. Thanks for your fast answer! Btw, I played a bit with Juk just this hour ;-)\n\n(and sorry for double posting, my browser cache didn't showed my first post ;-)\n"
    author: "anonymous"
  - subject: "screenshots from Scot's Laptop with KDE CVS?"
    date: 2004-12-19
    body: "I read, he is working on KDE4... thanks!"
    author: "anonymous"
---
<a href="http://ktown.kde.org/~wheeler/images/linux-bangalore-2004/dsc01063.jpg">Scott Wheeler and Sirtaj Singh Kang</a> with much appreciated help from Kabir Husain represented KDE at <a href="http://linux-bangalore.org/2004/">this year's Linux Bangalore</a>, India's largest Linux and Open Source event.  Many of the 2800 visitors stopped by the <a href="http://ktown.kde.org/~wheeler/images/linux-bangalore-2004/dsc01038.jpg">KDE booth</a> where KDE 3.3, a Knoppix desktop and KDE CVS (on Scott's laptop) were demonstrated or dropped into one of the <a href="http://linux-bangalore.org/2004/schedules/talkdetails.php?talkcode=F1100022">two</a> <a href="http://linux-bangalore.org/2004/schedules/talkdetails.php?talkcode=F1000022">talks</a>.
<!--break-->
<p>There was a fair deal of fraternizing</a> with folks from other projects with the floor of the KDE booth becoming a refuge for <a href="http://ktown.kde.org/~wheeler/images/linux-bangalore-2004/dsc01066.jpg">hiding</a> from doing real work.  A small <a href="http://ktown.kde.org/~wheeler/images/linux-bangalore-2004/">collection of event photos</a> is available.</p>

<p>We would like to thank the visitors that dropped by, the event organizers and remind the proto-developers that stopped by to start sending patches.</p>
