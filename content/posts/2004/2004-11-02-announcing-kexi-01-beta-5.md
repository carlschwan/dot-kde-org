---
title: "Announcing Kexi 0.1 Beta 5"
date:    2004-11-02
authors:
  - "kteam"
slug:    announcing-kexi-01-beta-5
comments:
  - subject: "Windows?"
    date: 2004-11-02
    body: "This might be a stupid question, but how does this run on Windows?  Do the developers have a commercial QT license?"
    author: "Leo S"
  - subject: "Re: Windows?"
    date: 2004-11-02
    body: "I think it uses the port of the X11 version of QT (which is under the GPL).  Currently that version is limited to being run inside of an X server on Windows (they have made HUGE progress porting it so far)"
    author: "Corbin"
  - subject: "Re: Windows?"
    date: 2004-11-02
    body: "They just had a thing on the dot about running KDE apps on Windows w/o CygWin and X11.\n\nhttp://dot.kde.org/1099243721/"
    author: "Corbin"
  - subject: "Re: Windows?"
    date: 2004-11-02
    body: "When i installed 0.1 b4 the other day, no X server came along for the ride.. So i assume something else is going on here?"
    author: "Nurb432"
  - subject: "Re: Windows?"
    date: 2004-11-02
    body: "Yes, I am Qt-ent/win32 licensee. \n\n-- \nregards / pozdrawiam,\n Jaroslaw Staniek / OpenOffice Polska\n Developers wanted! Kexi Project: http://www.kexi-project.org\n KDElibs/Windows: http://wiki.kde.org/tiki-index.php?page=KDElibs+for+win32"
    author: "Jaroslaw Staniek (Kexi Team)"
  - subject: "Access"
    date: 2004-11-02
    body: "So does it read Access files?\n\nMight be too much to hope for...\n"
    author: "isNaN"
  - subject: "Re: Access"
    date: 2004-11-02
    body: "For answer on this question and more, see:\n\n http://www.kexi-project.org/wiki/wikiview/index.php?KexiFAQ\n\n-- \nregards / pozdrawiam,\n Jaroslaw Staniek / OpenOffice Polska\n Developers wanted! Kexi Project: http://www.kexi-project.org\n KDElibs/Windows: http://wiki.kde.org/tiki-index.php?page=KDElibs+for+win32\n"
    author: "Jaroslaw Staniek (Kexi Team)"
  - subject: "MySql administrator"
    date: 2004-11-02
    body: "Is this tool similar to mysql administrator?"
    author: "Mysql user"
  - subject: "Be right there... in a few weeks/months"
    date: 2004-11-03
    body: " WOW! I just read the changelog.. thats some quite impressive work guys :-)\n\n I just downloaded the free C++/Qt book posted the other day. I think Kexi might be the project I would like to join in a few weeks/months when i feel a bit more proficient in the wonderfull world of programming :-D\n\nCya then!\n~Macavity\n\nBTW.: I thin its a great idea about the \"Insecure browser: download good one here\"... That should be *mandatory* on all OSS/FSS related sites ;-D"
    author: "Macavity"
  - subject: "Re: Be right there... in a few weeks/months"
    date: 2004-11-07
    body: "> BTW.: I thin its a great idea about the \"Insecure browser: download good one here\"... That should be *mandatory* on all OSS/FSS related sites ;-D\n\nToo bad that Konqueror isn't on the alternatives list, as it's missing a win32 version.\n\nWhen the development of kdelibs/win32 progresses further, maybe konqueror/win32 finally becomes true."
    author: "Leo Savernik"
  - subject: "beta 5 on gentoo"
    date: 2004-11-05
    body: "anyone been able to compil this latest beta on gentoo ??\n\n"
    author: "somekool"
  - subject: "Re: beta 5 on gentoo"
    date: 2004-11-05
    body: "AFAIK will be available sonn, see note about gentoo here http://www.kexi-project.org/download.html"
    author: "Jaroslaw Staniek (Kexi Team)"
---
We have released <a href="http://www.kexi-project.org/announce-0.1-beta5.html">Kexi 0.1 beta 5</a> (<a 
href="http://www.kexi-project.org/changelog-0.1-beta5.html">change log</a>),
the integrated environment for managing data.  <a href="http://www.kexi-project.org/">Kexi</a> aims to become an open-source replacement for MS Access yet ubiquitously available on
 Linux/Unix, Windows, and soon, even MacOSX.  Kexi has the potential to become a key application in the adoption of
 KDE, KOffice, Linux and free software in general, for individuals as well as companies and independent software vendors.


<!--break-->
<p>
Unfortunately achieving this goal will not be possible without the help of new developers. There is only one full-time developer working on Kexi, and four other voluntary developers. No matter how talented and motivated we are, we cannot create such a big and ambitious application alone.  We would therefore like to make a call to the community, to help turning Kexi into that long awaited MS Access
replacement.
</p>
<p> 
 Kexi has the potential to become the great database software we have all been
 awaiting for so long. The core module and the framework are very flexibly
 designed, modern and efficient. The first stable (in terms of
 functionality) version will be released in a few months.  With the help of
 new developers, Kexi could gain new features
 such as scripting or reporting far quicker.
</p>
<p> 
No database-related knowledge is required for new
 developers, just some experience on Qt and KDE programming. We are also
 looking for package maintainers, translators, testers, end-user and
 development documentation writers, etc. Anybody willing to help is invited.
</p>
<p>
 Please <a href="http://www.kexi-project.org/contact.html">contact</a> Jaros&#322;aw Staniek, if you want to offer your help. You can also join us on #kexi channel at irc.freenode.net for more
 information.  Visit our <a href="http://www.kexi-project.org/">website</a> for details about Kexi including screenshots, API documentation, etc.
</p>
<p> 

The Kexi Team
</p>


