---
title: "Docs Competition - Win O'Reilly Gear"
date:    2004-09-19
authors:
  - "qteam"
slug:    docs-competition-win-oreilly-gear
comments:
  - subject: "Competition"
    date: 2004-09-20
    body: "Just to make sure we know what's a competition entry, make sure you mention the competition in your message to kde-doc-english!\n\nCheers,\nPhil"
    author: "Philip Rodrigues"
  - subject: "Sections available for writing"
    date: 2004-09-21
    body: "To save you looking through the whole user guide, here's a list of sections which are available to work on:\n<p>\n<b>== Part I: The Desktop ==</b><br>\n<pre>\nAdvanced Window Management\nThe System Tray\nLaunching Programs\nControlling Programs -> Standard Toolbar Layout\n                     -> Keybindings\nOpening and Saving Files\nConfiguring Notifications\nConfiguring Toolbars\n</pre>\n<p>\n<b>== Part II: KDE Components ==</b><br>\n<pre>\nRemovable Disks\nPlaying Music -> Intro to aRts\n              -> Performance tuning, sharing devices\nPlaying Movies\nUsing KDE as Root\nSwitching Sessions\nNetworking With Windows\nShared Sessions\nSetting Up a Printer\nPrinting From Applications\nPDF Files\nFonts - Installing and Configuring\nKonsole Introduction (not necessarily the whole section)\nHand-Editing Configuration Files\nScripting the Desktop\n</pre>\n<p>\n<b>== Part III: KDE and the Internet ==</b><br>\n<pre>\nKonqueror -> Intro to the browser\nFine Tuning your Browsing Experience\nInternet Shortcuts\nIntro to Messaging\n</pre>"
    author: "Philip Rodrigues"
  - subject: "Entry Limitations?"
    date: 2004-09-22
    body: "I didn't look too closely, but: How many pages can we send in? Am I allowed to write thirty and enter them all?"
    author: "jameth"
  - subject: "Re: Entry Limitations?"
    date: 2004-09-22
    body: "There is no restriction. The more you send in, the better your chances!"
    author: "Waldo Bastian"
  - subject: "Plaintext"
    date: 2004-10-08
    body: "Does it *have* to be plain-text or can we send DocBook V3.1 or 4.2?"
    author: "Charles McColm"
---
If you missed out on the <a href="http://dot.kde.org/1093543464/">writing
competition at aKademy</a>, now is your chance to make up for it. The KDE
Quality and Documentation teams have got together to offer some great
O'Reilly prizes for writing documentation. All you have to do to enter is 
write a page for the <a 
href="http://users.ox.ac.uk/~chri1802/kde/userguide-tng/">new KDE User 
Guide</a> within two weeks and we'll send you a prize! Read on for the full 
details.



<!--break-->
<p>To enter, have a look at the content so far, with an outline of sections which 
need writing, and write one section (a single page, e.g. "<a 
href="http://users.ox.ac.uk/~chri1802/kde/userguide-tng/playing-music.html">Playing 
Music</a>"). Send your submission in plain text format to 
kde-doc-english@kde.org before Sunday 3rd October (two weeks from now).</p>
<p>
The best three entries, judged on completeness and clarity of explanation,
will win a choice of one of these great prizes, kindly donated by <a href="http://www.oreilly.com">O'Reilly</a>:
</p>
<ul>
<li>A grey O'Reilly t-shirt</li>
<li><a href="http://www.oreilly.com/catalog/freedom/index.html">Free as in 
Freedom</a> (Richard Stallman's Crusade for Free Software)</li>
<li>The <a href="http://www.oreilly.com/catalog/cpluspluspr/">C++ Pocket 
Reference</a></li>
<li><a href="http://www.oreilly.com/catalog/cgi2/index.html">CGI Programming 
with Perl</a></li>
</ul>
<p>
If you have any questions, feel free to e-mail kde-doc-english@kde.org.
</p>


