---
title: "Application of the Month: KSirtet"
date:    2004-04-06
authors:
  - "fmous"
slug:    application-month-ksirtet
comments:
  - subject: "KDE should allow user poll for applications"
    date: 2004-04-06
    body: "so that users can suggest the developers what application user needs, and not what the developers want users to have.\n\nLike poll for \"Which Media Player will be default in KDE?\"\n\nchoices are:\n\n1. Noatun\n2. Kaboodle\n3. KMplayer\n4. Kplayer\n5. Juk\n6. amarok\n7. Kaffeine\n8. Aktion\n9. Other...\n\nand I believe kaffeine will win the poll :)"
    author: "rizwaan"
  - subject: "Re: KDE should allow user poll for applications"
    date: 2004-04-06
    body: "Good idea, but you should maybe divide between audio and video players. \n\namaroK, JuK (and Noatun really, too) are specialized in audio playback, while the others are tailored to do video, or both.\n\nA poll might be more fair than the current system, which makes it very difficult to replace applications in KDE which happen to sit there for quite some time, but without much progress really. I'm especially thinking here about the possibility of amaroK replacing Noatun for KDE 4."
    author: "Mark Kretschmann"
  - subject: "Re: KDE should allow user poll for applications"
    date: 2004-04-06
    body: "\"I'm especially thinking here about the possibility of amaroK replacing Noatun for KDE 4\"\n\nI've heard this comment several times. Now I'll ask why? Just because of a pretty interface? They have a big problem. They are implementing very special features, when basic features provided by _any_ other player are not in, and the features that are in are only half-finished.\n\nFor example... Where's the equalizer? Nobody needs any FX except that, but every cheap player has one.\n\nThen there are several useability problems: nonstandard colors and widgets (of course, nonskinnable, so you have no other choice than dark blue and those tiny fonts), mixed-up radio stations and files,radio lists that have to be fetched every time you start the app, and a simple playlist needs MDI!\n\nI think I'll grab a copy of kaboodle...."
    author: "not me"
  - subject: "Re: KDE should allow user poll for applications"
    date: 2004-04-06
    body: "I agree, FX are silly, but they came free with arts. One of the FX on my computer is a equalizer, granted its a pretty cumbersome looking ones. I personally never use equalizers.\n\nOverall, amaroK widgets are more standard then usual, certainly more standard then XMMS (even by GTK standards). One of the things I like about it. WinAmp 5 looks beautiful though.\n\nThe playlist is my favorite part. In XMMS I have to open up the Open Directory dialog box all the time, have to expand it to see all the directories etc. I use a high resolution so the space saving features of XMMS's play list are of no benefit, though I imagine Amarok's playlist could be made smaller (why its MDI). Amarok interface is very intuitive and make a lot more sense if you have a properly organized MP3 collection then the JuK-style audio players. From what I've seen from screen shots, the one in CVS looks a lot prettier.\n\nPersonally, I'm not a big fan of skinning, I just want something that works (I pretty much stuck to the default with XMMS). And I happen to like amaroK's look, though I could see how someone could disagree. It seems like a lot of players sacrifice good usability at the altar of skinning (a word that thankfully has lost much of its buzz). I'd rather they concentrate on making a good default. Audio players traditionally have radical designs, I don't think there anything fundamental wrong with that. \n\nAnd anyways, we're comparing with Noatun not Kaboodle. Kaboodle's purpose is to be a KPart and for simple \"I want to play a file\" purposes."
    author: "Ian Monroe"
  - subject: "Re: KDE should allow user poll for applications"
    date: 2004-04-06
    body: "Dunno, difficult chioce.\n\nI currently use:\n\n- KPlayer to play video files\n- amaroK to play audio files\n- pure xine to play DVDs\n- KMPlayer for embedded video in Konqueror\n- Kaffeine to freeze Konqueror\n- Noatun and Kaboodle to take up disk space\n\nAt least for me, the old Unix saw of 'one tool for one job' ist still in place."
    author: "Flitcraft"
  - subject: "Re: KDE should allow user poll for applications"
    date: 2004-04-06
    body: "Last time I wanted to freeze konqueror I tried kaffeine, too.\nBut it didn't work!  It just played a stupid video! \nWhat did I do wrong? "
    author: "cm"
  - subject: "Re: KDE should allow user poll for applications"
    date: 2004-04-08
    body: "You played the wrong video. The right one is the business presentation of m$ windows 2003 server with billiboy cursing like hell when the screens turned blue.\nCan be found via aMule/mldonkey."
    author: "Andy"
  - subject: "Re: KDE should allow user poll for applications"
    date: 2004-04-06
    body: "What has your posting to do with the news!? Please don't post off-topic. Discuss on http://kde-forum.org or if you really want to influence development on the kde-multimedia mailing list."
    author: "Anonymous"
  - subject: "Re: KDE should allow user poll for applications"
    date: 2004-04-06
    body: "Perhaps because the application of the month april is JuK (on http://www.kde.de)\n\n"
    author: "Dominik"
  - subject: "Re: KDE should allow user poll for applications"
    date: 2004-04-06
    body: "indeed .. we have some catching up to do :) \n\n\nFab\n"
    author: "Fabrice Mous"
  - subject: "Re: KDE should allow user poll for applications"
    date: 2004-04-07
    body: "Erm, what's up with these off-topic fascists appearing everywhere?\n\nI blame slashdot. It never used to have this concept of \"off-topic\", conversations could vary freely where people wanted them too. Then the \"off-topic\" idea was invented there, and now every joker thinks that they have a perfect right to tell others what they can and can't talk about.\n\nPersonally I don't care if people want to discuss fine art or the music of the Beatles here, if that's what they want to talk about then you've got no business imposing your fascist nonsense, so sod off, please."
    author: "Anonymous"
  - subject: "Re: KDE should allow user poll for applications"
    date: 2004-04-06
    body: "Juk and Amarok should be the candidates for audioplayers.\nJuk for tagbased playlist and amarok for fiesystem based \nplaylist.  Others should be removed.\n\nI have been using mplayer/kmplayer for long.  Of late \nKaffeine has improved a lot and seems to be better than\nthan kmplayer.  "
    author: "Ask"
  - subject: "Re: KDE should allow user poll for applications"
    date: 2004-04-06
    body: "100% agree. JuK and amarok definitely rocks. noatun and especially kaboodle sucks, they have an ugly UI and nothing more in functions than amarok.\n\nIf you want, please drop your vote here\nhttp://bugs.kde.org/show_bug.cgi?id=76915      "
    author: "Vide"
  - subject: "Re: KDE should allow user poll for applications"
    date: 2004-04-06
    body: ">100% agree. JuK and amarok definitely rocks\n\nI gave some suckiness reasons for amarok and there're more. I can't complain about juK\n\n>noatun and especially kaboodle sucks\n\nReally? At least they do what other basic sound players do.\n\n> they have an ugly UI\n\nexcuse me, but there're much better kjofol GUIS for noatun than that crap GUI of amaroK\n\nAnd kaboodle doesn't require half your cpu to play the system bell\n\n> and nothing more in functions than amarok.\n\nFalse. Amarok doesn't have half the functions noatun implements. Never heard of a plugin-based system?\n\nYou're just biased by its looks. Which I, by the way don't like, and others don't either. I can't say I hate the application, but it's still not able to replace juK, noatun, and even less kaboodle\n\nIf you think my words here are rude, you'd have to hear my shouts whenever I find some new stupidity in amaroK's design."
    author: "not me"
  - subject: "work in progress :)"
    date: 2004-04-06
    body: "Heh, I see you love amaroK features :)) I'd say it is a work in progress. \nI use both apps (noatun and amarok), depending on my needs, and sometimes on my mood. I like noatun mainly because I need low cpu (XFree) consumption, on a laptop that can't make 10,000fps :) The looks are great too with the KJofol plugins, since I hate small guis (like that of xmms). \n\nI think both apps have their good points. Amarok's new plugins that are coming in are great too, but I'd agree to say noatun is a little bit more mature right now.\n\n"
    author: "uga"
  - subject: "Re: work in progress :)"
    date: 2004-04-06
    body: "Just a detail....When I said \"Amarok's new plugins\" I meant audio output plugins. "
    author: "uga"
  - subject: "agreed"
    date: 2004-04-06
    body: "Amarok is really promising but not 100% there yet.  My own pet hate is that you can't play files directly from the file/stream browser as well as from the playlist, you have to drag them into the playlist first.  The streaming buffering needs some work too, or maybe just an option to make the stream buffer bigger.\n\nThat said, my ideal media player would be a combination of JuK and Amarok.  I'd like to see JuK support Amarok's new player-independent visualisation plugin system, the streaming support and the aRts audio effects.  Video would be cool, I don't see why JuK couldn't do this very well too.  What I envisage would be a tab/button bar down the left hand side of the JuK window with buttons for 'Playlist', 'Visualizations' and 'Video' and for the playlist to be swapped out for these when you click on the buttons/tabs.\n\nThat would make it the best media player on any platform, at least for me.  Guess I'd better submit a wishlist bug and/or find some time to get coding..."
    author: "nuts"
  - subject: "Re: KDE should allow user poll for applications"
    date: 2004-04-06
    body: "And you still wonder why the developer won't let the user decide?\nI don't after this reply."
    author: "anonymous"
  - subject: "Re: KDE should allow user poll for applications"
    date: 2004-04-07
    body: "> Really? At least they do what other basic sound players do.\n\nIf you want a really basic player, you can use mpg123 or oggplay. We are talking about KDE 4, dude\n\n> excuse me, but there're much better kjofol GUIS for noatun than that crap GUI of amaroK\n\nOk. So amarok has a nice and convenient UI. Others do not.\n\n> False. Amarok doesn't have half the functions noatun implements. Never heard of a plugin-based system?\n\nYes, but I don't see lots of plugins around. and amaroK is in development, is including other (and better than arts) stream systems.\n\n> You're just biased by its looks. Which I, by the way don't like, and others don't either. I can't say I hate the application, but it's still not able to replace juK, noatun, and even less kaboodle\n\nJuk is totally another kind of program, since is a tag based player. Here we are talking about filesystem based players, and I really can't understand how amarok couldn't be loved by every kde-user. Ok, we may leave noatun, but the default should be amarok, and kaboodle should be removed at all.\nAnd about CPU usage, amarok is fast and clean, I don't see any problem at all."
    author: "Vide"
  - subject: "Re: KDE should allow user poll for applications"
    date: 2004-04-08
    body: "JuK is a decently useful app... but very resource hungry. It especially grinds and grinds and grinds when first loading, seemingly scanning all MP3's yet again... I certainly wouldn't want to see it as a default for anything."
    author: "Tim"
  - subject: "Re: KDE should allow user poll for applications"
    date: 2004-04-06
    body: "> KDE should allow user poll for applications.\n\nfull ACK!"
    author: "ac"
  - subject: "Re: KDE should allow user poll for applications"
    date: 2004-04-06
    body: "First off, I'll get this out of the way: As much as I love KDE, none of KDE's media players are really up to snuff IMO. I mainly use XMMS for audio, and while I don't watch much video, usually I use either (plain) Xine or GMPlayer.\n\nOf KDE's current audio apps, JuK is the most tolerable. In fact, I'd like to see Noatun chucked in favour of JuK. KMPlayer would be a good for video, as it's a MPlayer frontend, except that its controls are horrible."
    author: "QV"
  - subject: "Re: KDE should allow user poll for applications"
    date: 2004-04-06
    body: "Have you tried Amarok and Kaffeine? I was also a kde-multimedia app holdout for a long time until I found those two apps. I used xmms and xine before. \n\nYeah, I absolutely hate Noatun. "
    author: "fault"
  - subject: "Re: KDE should allow user poll for applications"
    date: 2004-04-06
    body: "\"Have you tried Amarok and Kaffeine?\"\n\nAs a summary of what I wrote up there: Amarok is not ready for the desktop :D"
    author: "not me"
  - subject: "Re: KDE should allow user poll for applications"
    date: 2004-04-10
    body: "Hmm...having emerged both apps, I find that I like Kaffeine, especially as it's a Xine frontend. I now use it instead of Xine and, when I can, MPlayer. Nice visualisation too. Tho I've not tried it with a DVD yet, as I've not re-set up DVD support after recently switching distros.\n\nAmaroK, on the other hand, I just...don't care for. Also, the media control applet doesn't support it yet (only Noatun, JuK, and XMMS)."
    author: "QV"
  - subject: "Re: KDE should allow user poll for applications"
    date: 2004-04-06
    body: "Totally OT, but...\n\nAncient KDE proverb :\n\n\"[Sh|H]e who codes, decides.\"\n"
    author: "LMCBoy"
  - subject: "Re: KDE should allow user poll for applications"
    date: 2004-04-07
    body: "And it should go the way of 'old wives tales'.\n\nDecisions should be based on proper engineering criteria.\n\nOr, to paraphrase another old saying:\n\nDesign twice, code once.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: KDE should allow user poll for applications"
    date: 2004-04-07
    body: "\"Proper engineering criteria\" is such a vague thing when it comes to programming. The perfect \"abstract\" design doesn't exist, only implementations can be seriously evaluated, and programmers have the last word.\n\n"
    author: "Sergio"
  - subject: "Re: KDE should allow user poll for applications"
    date: 2004-04-07
    body: "> \"Proper engineering criteria\" is such a vague thing when it comes to \n> programming.\n\nIt isn't about programing.  Engineering criteria are applied to the design of application itself -- then the programing is just the means of implementing the application design.  This is the way that engineering criteria are applied to programing -- indirectly.\n\nI see stuff that was programed more than once and designed never.\n\nThis stuff still doesn't work and IMHO never will have a chance of working well until it is designed at least once.\n\nNow do you understand what I am talking about?\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: KDE should allow user poll for applications"
    date: 2004-04-07
    body: "This is not necessarily how voluntary work works. You just can't force people to do stuff the way you want it if they think their approach is more interesting, fun and/or satisfying then yours. This may seem irrational to you, but you are seriously wasting your time when you think you can change that."
    author: "Datschge"
  - subject: "Re: KDE should allow user poll for applications"
    date: 2004-04-08
    body: "Your point is probably correct.\n\nBut it doesn't explain why they need to denegrate engineering to rationalize about the way that they do things.\n\nAs I suppose that you know, engineering is anything but abstract.  It is not about some \"perfect\" design.  The engineering method is about optimizing a design to meet critera.  This is usually done by making the best compromise between conflicting paramaters.\n\nAgain, why does somebody need to falsely characterize engineering to justify their flawed methodology?  And, why the attitude: \"programmers have the last word\".  As an engineer, I always knew that the users have the last word.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: KDE should allow user poll for applications"
    date: 2004-04-08
    body: "Well, I believe any competent programmer is always optimizing his design, not just typing away lines of code without taking some time to think about the general architecture. What I'm saying here is that it not possible to strictly separate coding and designing, specially when you dont have a strict well defined list of requirements to start with. If you start writing an application knowing exactly what you want, the classic engineering method might work, unfortunately that's generally not the case."
    author: "Sergio"
  - subject: "Re: KDE should allow user poll for applications"
    date: 2004-04-08
    body: "I think that you are begging the question.\n\nHowever, it might help to make a distinction between programmer and coder.\n\nThe original posting stated that the *coder* should decide.\n\nOTOH, you consider a \"programmer\" as a person that does both the design and the codding.  If that is the case, then why should someone that is only a coder make engineering decisions?\n\nI think that you are still discounting the importance of engineering (the design).  I consider myself a \"programmer\" I can do both designing and codding.  What I am asserting (as a \"programmer\") is that the design (the engineering) is at least as important as the actual codding -- IMHO, the engineering is more important.\n\nIn a large project, there is a place for people that only do design work, and they should participate in making decisions.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: KDE should allow user poll for applications"
    date: 2004-04-09
    body: "Not really.  This simply isn't how OSS works.  It will take almost anything from people willing to commit time and energy to getting stuff done -- no matter how inexperienced -- and it will take almost nothing from people that just want to air their lofty ideas -- no matter how qualified.\n\nThis can be brought up over and over again (and well, has been) but it's simply part of the system -- and I don't mean just OSS -- any volunteer organization.  You can pretty much either (a) dig in and start working, (b) stand on the sidelines cheering or (c) take your toys and go home."
    author: "Scott Wheeler"
  - subject: "Re: KDE should allow user poll for applications"
    date: 2004-04-09
    body: "Your comments are absurd, incredible, the height of arrogance and probably circular reasoning.\n\nYou are assuming that engineering isn't work, that it doesn't take time and energy to do design work and that somehow it is just a bunch of lofty ideas.  And then using these false assumptions to support your unsupportable preconceptions.\n\nYou just don't get it!  The truth is that engineering and design is what is important in a large project -- more important than the actual coding.  Take the Control Center for example.  Do you think that coding will fix that?  The last two changes (that I am aware of) made by \"developers\" have only made things worse.  Are you saying that Jamethiel Knorth's work has no value?\n\nhttp://www.csis.gvsu.edu/~abreschm/designs/the-control-panel/index.html\n\n> This can be brought up over and over again ... .\n\nAnd I guess that it will be until the \"arrogant developers\" understand.\n\nWhy to I bring this up again and again.  Because I have problems with OSS programs and I look at the code, or sometimes just what it does.  I often have this reaction: This can't possibly work, what was the person that did this thinking of.  I am afraid that the answer is that they weren't thinking; they didn't design it; they just coded it.  Even well written code that does the wrong thing, or tries to do something that is not possible, is virtually useless!  And unpremeditated code is rarely well written.\n\n> You can pretty much either (a) dig in and start working, \n\nAgain, your presumption is that engineering and design is not work.  It *is* work and it probably requires more ability than coding.  I know from experience that the design is more work and more important than the actual coding.  I can only guess that those that think otherwise simply are not doing the design work.\n\n> (b) stand on the sidelines cheering or (c) take your toys and go home.\n\nThat is insulting.  I don't know for sure exactly where this fallacious idea starts, but I think that it starts with people thinking that because they have learned a programing language that they know how to write a computer program.  I can write English but I don't think that I can write a novel.  Would you try to build a bridge without having an engineer design it?  How is software different.  \n\nIf you have some evidence to indicate that engineering and design is not important in software development, please present it.  It is useless to just keep saying that it isn't with no supporting evidence.\n\nBy continuing to denigrate engineering and design, you are only showing your own ignorance.  Unbelievable!\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: KDE should allow user poll for applications"
    date: 2004-04-09
    body: "> You are assuming that engineering isn't work, that it doesn't take time and \n> energy to do design work and that somehow it is just a bunch of lofty ideas.\n\nNo, in fact it has real value, but it has to be converted into something tangible.  That doesn't happen just by talking about engineering and software design -- it needs people to do the legwork of converting those designs into software.  OSS is built from people that are willing to do both parts.\n\n> And I guess that it will be until the \"arrogant developers\" understand.\n\nYou're standing in the bazaar and complaining that it's not a cathedral.  Design in OSS software happens in a fundamentally different way than it does in a traditional software company (I work in such a company, by the way.).  Design in an OSS environment is evolutionary, not static -- a lot of things are produced in parallel.  Many of them suck and are eventually replaced.  Some evolve into something reasonable.\n\n> Again, your presumption is that engineering and design is not work. It *is* \n> work and it probably requires more ability than coding.\n\nYou're also making the similarly arrogant assumption that none of the people involved in OSS \"coding\" are also \"software engineers\" -- which is simply false.  Even by your standards many in the OSS community are qualified and experienced software engineers.  Many of KDE's core developers studied computer science or software engineering and many of us have experience in traditional computer software design roles.  Guess what -- we still produce code.  ;-)  I call your ideas lofty not because they're wrong, but because they're not independently useful in OSS -- at least not in a direct way.\n\n> I don't know for sure exactly where this fallacious idea starts, but I think \n> that it starts with people thinking that because they have learned a \n> programing language that they know how to write a computer program.\n\nNo, not really.  Again -- it's an evolutionary approach -- this time turned around to contributors.  People learn when they're involved in OSS.  The see the real results of bad and good design decisions.  You've observed that this often produces poorly design projects -- of course it does!  (Conversly it also produces a lot of well designed stuff.)  But while it's producing poorly designed software it's producing better software engineers whose next output will be better.  When you let this run over the course of several years you develop a core of people that tend to be on average much better than what you find in the commercial world.  It's parallel, evolutionary convergence towards better software and better software engineers -- a \"bazaar\".\n\nBut you can't skip the gruntwork and nobody's ever going to take your word for your experience.  There are of course people in any large OSS project that are often consulted for design advice -- or more often designs are proposed and responded to from a variety of folks.  But the respect that one earns as a software designer in the OSS world is based on the community knowledge of their designs and the quality of them as seen in their code.  OSS doesn't ask to see your resume -- it asks to see your code.  The proof is in the puding..."
    author: "Scott Wheeler"
  - subject: "Re: KDE should allow user poll for applications"
    date: 2004-04-09
    body: "Yes, I think of a programmer as someone who designs and implements. What  I dont believe is that is possible to have designing and coding as two totally separated phases of building a program. I don't think the classic plan and implement way of doing things work well with software (note that im not saying that there should be no design at all, which would be ridiculous).\n\nI will quote Paul Graham in \"On Lisp\"\n\n\"It may be difficult to say why the old method fails, but that it does fail, anyone can see. When is software delivered on time? Experienced programmers know that no matter how carefully you plan a program, when you write it the plans will turn out to be imperfect in some way. Sometimes the plans will be hopelessly wrong. Yet few of the victims of the plan-and-implement method question its basic soundness. Instead they blame human failings: if only the plans had been made with more foresight, all this trouble could have been avoided. Since even the very best programmers run into problems when they turn to implementation, perhaps it's too much to hope that people will ever have that much foresight.\""
    author: "Sergio"
  - subject: "Re: KDE should allow user poll for applications"
    date: 2004-04-09
    body: "At the risk of sounding tedious, I said that the *application* should be designed before it was implemented.  The implementation is a program.  I would agree with you that you can not totally design a program before you start to implement it.  But this does not mean that the application should not be designed first.\n\nThere can be considerable discussion of just how much design work should be done on a program before the coding is started.  The fact that some things may need to be redesigned after the coding has started does not mean that it shouldn't be designed.\n\nHowever, again at the risk of sounding tedious, what I was talking about was not the degree of pre-coding design that would be optimum for a program.  I am talking about (apparently) totally unpremeditated coding.  And, the denigration of all engineering and design as an apparent rationalization for this.\n\nThe issue, clearly stated, is the assertion that the *coder* should make the engineering and design decisions for the *application*.  Nobody has stated any logical reason that this should be the case.  \n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: KDE should allow user poll for applications"
    date: 2004-04-09
    body: "> At the risk of sounding tedious, I said that the *application* should be designed before it was implemented.\n\nWell you're always using these general words design and engineering without ever saying what you mean with them. You said (https://mail.kde.org/pipermail/kde-quality/2004-April/000466.html) that it isn't about designing the implementation (UML diagrams). You wouldn't even suggest a method how-to do it. You don't even say what method exists and then you except people to understand you?\n\nI suspect that the following small list will help you to get by with all the \"arrogant\" and \"insulting\" KDE developers:\n\n- Don't say that the current way is wrong. It's always bad to start a discussion with this because it puts the other person in a defensive position.\n- Don't tell people what they should do. It's there FREE TIME!\n- Make suggestions giving concrete examples. When the suggestions aren't welcomed provide use-cases and real user experiences.\n- Don't get upset when the other person doesn't share your opinion, doesn't give an answer or just a short answer. It's there FREE TIME! You should try it later again with more data supporting your opinion.\n- DO THINGS! Offer your help designing an application. Make concrete proposals how do things better. Talking isn't worth anything in a open source project.\n\n(well, and I always thought that this is common sense)\n\nChristian"
    author: "Christian Loose"
  - subject: "Re: KDE should allow user poll for applications"
    date: 2004-04-07
    body: "About KMPlayer.\n\nIIUC, MPlayer is somewhat hardware dependent.\n\nIs this correct?  Specifically that it doesn't support ATI video cards.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: KDE should allow user poll for applications"
    date: 2004-04-07
    body: ">About KMPlayer.\n>\n>IIUC, MPlayer is somewhat hardware dependent.\n>\n>Is this correct? Specifically that it doesn't support ATI video cards.\n\nAre you joking ? MPlayer performs nicely on two ATI cards I've, a Radeon 9200 and a Rage 128 Pro, both using XFree drivers."
    author: "Juan M. Caravaca"
  - subject: "Re: KDE should allow user poll for applications"
    date: 2004-04-07
    body: "I had hoped to receive a useful response.\n\nI presume that you have never read the MPlayer documentation.\n\nIt appears that the latest release has cleared up the ATI problems except for TV Out on some ATI cards.\n\nHowever, if we are considering what should be the standard video player, it should be noted that MPlayer appears to have many hardware (video card) issues which XINE doesn't.\n\nOr, perhaps these issues only apply if you use: VIDIX.  The documentation is not all that clear.\n\n--\nJRT\n"
    author: "James Richard Tyrer"
  - subject: "KSirtet is one of the Nicest Tetris games"
    date: 2004-04-06
    body: "I like KSirtet. and KReversi very much. Both are fun to play. Thanks goes to the humble authors of these applications."
    author: "fast"
  - subject: "Re: KSirtet is one of the Nicest Tetris games"
    date: 2004-04-06
    body: "I think KFoulEggs is a lot of fun. I wrote a clone of it in highschool for Windows. http://monroe.nu/mandala\nSo its a clone of a clone. And I wouldn't be surprised if its a clone of a clone of a clone (if Andreas Diekmann got the idea for KFoulEggs from xpuyopuyo for instance)."
    author: "Ian Monroe"
  - subject: "[OT] Kopete to follow GAIM ??"
    date: 2004-04-06
    body: "I've loved kopete since KDE 3.2 came out, but I recent switched to Gaim because they are selling AWESOME GAIM cool gear at ebay..  ( http://cgi6.ebay.com/ws/eBayISAPI.dll?ViewSellersOtherItems&userid=gaimauctions&completed=0&sort=3&since=-1 ).. \n\nWill the kopete folks sell any kopete memorabilla soon?!?"
    author: "anon"
  - subject: "Re: [OT] Kopete to follow GAIM ??"
    date: 2004-04-07
    body: "ROFL... that is BEYOND hilarious... wow. i LOVE it! ahahhahaahahahahahahaaha"
    author: "Aaron J. Seigo"
  - subject: "Re: [OT] Kopete to follow GAIM ??"
    date: 2004-04-07
    body: "yeah, after looking at those items I'm tempted to switch to Gaim too. :) "
    author: "Apollo Creed"
  - subject: "Re: [OT] Kopete to follow GAIM ??"
    date: 2004-04-08
    body: "What am I bid for a pile of handwritten addressbook integration notes then?  Or a piece of original add contact wizard artwork?"
    author: "Will Stephenson"
---
As part of the <A href="http://www.kde.de/appmonth/2004/ksirtet/index-script.php">March 2004 issue</A> of the "Application of the month" series on <A href="http://www.kde.de/">KDE.de</A>, Andreas C. Diekmann has interviewed Nicolas Hadacek, author of <A href="http://ksirtet.sourceforge.net/">KSirtet</A>, a Tetris clone for KDE. The <A href="http://www.kde.nl/">Dutch KDE website</A> is offering an English translation of <A href="http://www.kde.nl/apps/ksirtet/en/interview.html">the interview</A> as well as the 
<A href="http://www.kde.nl/apps/ksirtet/en/">overview of this issue</a>. 




<!--break-->
