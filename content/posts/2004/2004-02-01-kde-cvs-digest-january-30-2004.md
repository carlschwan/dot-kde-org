---
title: "KDE-CVS-Digest for January 30, 2004"
date:    2004-02-01
authors:
  - "dkite"
slug:    kde-cvs-digest-january-30-2004
comments:
  - subject: "Thanks Derek!"
    date: 2004-02-01
    body: "You da man!"
    author: "Turd Ferguson"
  - subject: "Re: Thanks Derek!"
    date: 2004-02-01
    body: "It sure took a long time for this to get on the dot. I read your digest this afternoon."
    author: "Turd Ferguson"
  - subject: "Don't forget the BRANCH pls"
    date: 2004-02-01
    body: "Is there any chance that all those khtml fixes get backported into the branch?\n\nCan't wait for 3.2, and hope there will be a 3.3 in somethink like 6 month.\nIt would be nice if some kind of longterm roadmap would be done, e.g. as mozilla does... (not everything is bad in mozilla ;-)"
    author: "ac"
  - subject: "Re: Don't forget the BRANCH pls"
    date: 2004-02-01
    body: "I know your dilemma, I share it: On one hand, I'm really happy that we finally get KDE 3.2, on the other hand, these CVS-Digests already make me long for 3.3 :). We pesty users can never get enough :)."
    author: "Eike Hein"
  - subject: "Re: Don't forget the BRANCH pls"
    date: 2004-02-02
    body: "\nYes, one of those eternal questions we have to wrestle with in open source. (I'm fully serious)\n\nThere is a solution - running bleeding edge. No waiting for the next release, just an infinite kick of improvement.. and bleeding.\n\n\n\t\tFrans\n\n"
    author: "Frans Englich"
  - subject: "Re: Don't forget the BRANCH pls"
    date: 2004-02-01
    body: "Sure,\n\nMozilla Devs must know how to handle longterm roadmaps. Wonder why?\n\nYours, Kay"
    author: "Debian User"
  - subject: "strange"
    date: 2004-02-01
    body: "Italian (it)                      95.32%\nBrazilian Portuguese (pt_BR)      97.04%\n\nThe Italian translation is before the Brazilian translation even with a lesser score."
    author: "JC"
  - subject: "Re: strange"
    date: 2004-02-01
    body: "And how come some of the translations are now less than 100% yet just last week they were at 100?"
    author: "Turd Ferguson"
  - subject: "Re: strange"
    date: 2004-02-01
    body: "Look at the link above it, the statistics are for HEAD and HEAD is open for message changes again."
    author: "Anonymous"
  - subject: "textospeech"
    date: 2004-02-01
    body: "Remeber the old soundblaster tool? \n\n\"Hello, I'am Harry\"\n\n"
    author: "Andre"
  - subject: "Re: textospeech"
    date: 2004-02-01
    body: "Harry? That was Dr. Sbaitso!"
    author: "Anonymous"
  - subject: "Re: textospeech"
    date: 2004-02-01
    body: "ROTFL! Dr. Sbaitso!! I almost forgot him.\nHow could I?\nThanks for reminding me of one of the greatest\npsychologists in history.\nAm I the only one whose life completely changed\nafter some intimate talks about improving my sex life with him?"
    author: "Mike"
  - subject: "Re: textospeech"
    date: 2004-02-01
    body: "Hello Mike. My name is Dr. Sbaitso. I am here to help you. Say whatever is in your mind freely, our conversation will be kept in strict confidence. Memory contents will be wiped off after you leave. So, tell me about your problems."
    author: "Dr. Sbaitso"
  - subject: "Re: textospeech"
    date: 2004-02-01
    body: "No, Dr. sbaitso is an Eliza clone. \n\nHarry, Rita and so on were different voices in the texttospeech utility."
    author: "Elektroschock"
  - subject: "RSS / RDF feed for CVS commits? "
    date: 2004-02-01
    body: "Well, the subject says it all :). I know there's an IRC channel, the mailinglist and a mirror on the NNTP server, but is there also a RSS / RDF feed for KDE CVS commits?"
    author: "Eike Hein"
  - subject: "Re: RSS / RDF feed for CVS commits? "
    date: 2004-02-01
    body: "rss: http://cia.navi.cx/stats/project/kde/.rss\nrdf: http://www.derkarl.org/kcvscommits.rdf.phtml\n"
    author: "Christian Loose"
  - subject: "Re: RSS / RDF feed for CVS commits? "
    date: 2004-02-01
    body: "Sure, it's listed on http://kde.ground.cz/tiki-index.php?page=RSS+Feeds"
    author: "Anonymous"
  - subject: "Bibletime"
    date: 2004-02-01
    body: "How will this work? You you have it read text in bibletime?"
    author: "Kraig"
---
In <a href="http://members.shaw.ca/dkite/jan302004.html">this week's KDE-CVS-Digest</a>: The
<A href="http://accessibility.kde.org/aids/">KDE Text-To-Speech Daemon</A> improvements with GUI and speaker plugins. 
<A href="http://edu.kde.org/kstars/">KStars</A> adds star motions. 
And many bugfixes in <A href="http://www.kdevelop.org/">KDevelop</A> and
khtml.  
<!--break-->
