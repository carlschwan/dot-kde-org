---
title: "SUSE's YaST to be GPL'd, SUSE 9.1 Personal Features KDE"
date:    2004-03-24
authors:
  - "numanee"
slug:    suses-yast-be-gpld-suse-91-personal-features-kde
comments:
  - subject: "Relation to Debian+KDE?"
    date: 2004-03-24
    body: "A few months ago, and in response to Bruce Perens' UserLinux proposal, a bunch of Debian and KDE guys came up with a Debian+KDE distribution, maybe based on the work of SkoleLinux or arklinux. Among the requirements, I can think of right now, there was the installer, and a unified control-panel-like software, similar to YaST. What has happened to that project? Has it gone away, or is someone working on it? It would clearly benefit from YaST (provided moving from SuSE to Debian is straightforward)."
    author: "j00z"
  - subject: "Re: Relation to Debian+KDE?"
    date: 2004-03-25
    body: "The project is still alive (see kdenonbeta/kdedebian) and actively discussing about the YaST GPL."
    author: "Anonymous"
  - subject: "Re: Relation to Debian+KDE?"
    date: 2004-03-25
    body: "Is there a specific reason why the kde-debian's mailing list archive isn't open?"
    author: "anonymous"
  - subject: "Re: Relation to Debian+KDE?"
    date: 2004-03-25
    body: "Not to my knowledge. I think I've even seen it archived somewhere else too. Get in contact with the list admin, and ask nicely. =)\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Re: Relation to Debian+KDE?"
    date: 2004-03-25
    body: "> I think I've even seen it archived somewhere else too.\n\nhttp://news.gmane.org/gmane.comp.kde.debian/"
    author: "Anonymous"
  - subject: "surface"
    date: 2004-03-24
    body: "Skole is independend from Bruce Perens. It was more like Bruce Perens made the fuzz and Skolelinux said: Well, we already got it.\n\nSkole is from Skandinavia and a very good debian distribution. In Nothern Germany there is also a lot Skole expertise.  \n\nDon't care about Bruce Perens, his UserLinux is vaporware.\n\nI always found SuSe the best distribution despite the fact that they don't ship knights with KDE but this ugly old X chessboard.\n\nBTW: As you can customize the OO toolbar, is it possible to fully Microsoftify the OpenWriter toolbars per default? I created my own toolbars but I am not finished yet. As OO provides the same backend functions it must be possible to get a better surface (don't like MSOffice 2003 but 2000 style is fine). Abiword tries to imitate the MS Office appearences but fails on the buggy backend."
    author: "Johannes"
  - subject: "Re: surface"
    date: 2004-03-24
    body: "Personally, I find MS Office emulation is fine - up to a certain point. What exactly do you mean by Microsoftify? If you're talking about look and feel then I suppose that's pretty superficial."
    author: "David"
  - subject: "Re: surface"
    date: 2004-03-24
    body: ">Don't care about Bruce Perens, his UserLinux is vaporware.\nI've seen a lot of activity in Userlinux lately, and it's gaining momentum IMHO. So I don't know what makes you think like that, Userlinux has much more potentian than, say, Fedora project. \n\nIt will never be vaporware since Debian is an excelent enviroment for make a new distribution on top of it (check mepis, its a one man distro). It has the broadest package list around and the great apt tool. The biggest problem apart installation seems to be it has too many options, dozens of window managers, hundreds of multimedia applications, e-mail clients, developer tools etc etc etc. It gets confusing for the user to choose one or even know about it, specially when the choices are so complex as the desktop enviroment of choice or server technology. \n\nPersonally i don't like SuSE. The reason is simple, it made choices that are not my own (RPM, KDE, proprietary software - which will be gpled soon, thats good, closed development, all that stuff in /opt and other things). Happilly for me, and to many others as it seems, Userlinux will use mostly software my/their prefered package set, and it's open development model promises it will be a great distro. Of course that's no reason for me to flame over SuSE and others. "
    author: "Chips"
  - subject: "Re: surface"
    date: 2004-03-24
    body: "\"The biggest problem apart installation seems to be it has too many options, dozens of window managers, hundreds of multimedia applications, e-mail clients, developer tools etc etc etc. It gets confusing for the user to choose one or even know about it, specially when the choices are so complex as the desktop enviroment of choice or server technology.\"\n\nThat's not the job of the users, or even the enterprise. It is the job of a competent service vendor/provider to use the flexibility of the software to create a path that is cost-effective and does the job. That's something Userlinux seems to have missed.\n\n\"Personally i don't like SuSE. The reason is simple, it made choices that are not my own (RPM, KDE, proprietary software - which will be gpled soon, thats good, closed development, all that stuff in /opt and other things).\"\n\nLOL! You can make exactly the same accusation of Userlinux.\n\n\"Happilly for me, and to many others as it seems, Userlinux will use mostly software my/their prefered package set, and it's open development model promises it will be a great distro.\"\n\nMmm. Unfortunately, what you want doesn't necessarily equate into success for Userlinux."
    author: "David"
  - subject: "Re: surface"
    date: 2004-03-25
    body: "\"I've seen a lot of activity in Userlinux lately, and it's gaining momentum IMHO.\"\n\nWell, if you have zero momentum in the beginning, even a tiny increase would mean that you are \"gaining momentum\". And that's the way it seems to be with UserLinux.\n\n\"Personally i don't like SuSE. The reason is simple, it made choices that are not my own (RPM, KDE, proprietary software - which will be gpled soon, thats good, closed development, all that stuff in /opt and other things).\"\n\nWell, you are not forced to install that proprietary software. And you can use desktop/WM of your choice, not just KDE (if you don't like KDE, what are you doing here BTW?). And what are you talking about \"closed developement\"? SUSE is the primary backer of ALSA, are they \"closed\"? They also emply several kernel-hackers, is their work \"closed\"?"
    author: "janne"
  - subject: "Good to Hear"
    date: 2004-03-24
    body: "Well that's good about YaST, but I never quite understood why it was closed in the first place. Mandrake have always been open with their tools, and it didn't make any difference to them, so I don't know why Suse did it.\n\nWithout creating a huge massive long thread of stupid licensing posts, I'm just wondering what the licensing implications are with Qt when moving from proprietary to free, or free to proprietary software. However, I suppose it also depends on how something like YaST is structured.\n\n\"YaST is potentially a starting point for a free cross-distribution, and eventually maybe even cross-platform, KDE-based administration tool.\"\n\nThis is why I think that some of the work going on at freedesktop is really, really positive. It will make this sort of thing much, much easier. However, it seems as though YaST is somewhat GUI independent, so it doesn't really depend on Qt. I suppose it could quite easily have a GTK or a web front-end as well.\n\n\"In other good news, it has been reported...\"\n\nMmm. I certainly didn't detect the new found promotion of Gnome that some people seem to have been talking about in that article. Seems like the same as always.\n\n\"...by beta users that the KDE Desktop, several key KDE applications as well as a KDE'fied OpenOffice.org are featured exclusively in the SUSE 9.1 Personal.\"\n\nThe KDEification of Open Office is an 'extremely' positive thing. Yes Open Office is a bit of a dog of an application, but there are some really great features in it, and having been fed MS Office for years it has brought back some of the really good ones. Even over and above look and feel issues, a priority for this should be integration with KDE's dialogues, and in particular, the printing system. Open Office and KDE's printing system - mmmmmm..... Jan Holesovsky has done some stellar work on this in a pretty short space of time. Open Office 2.0 should be a great target platform for all this work, and I think these KDE enhancements are coming at just the right time, rather than as the result of hype.\n\nDoes anyone know who that famous baby is in the screenshots?"
    author: "David"
  - subject: "Re: Good to Hear"
    date: 2004-03-24
    body: ">\"Does anyone know who that famous baby is in the screenshots?\"\n\nThats Lotte, obviously.\n\nGimp 2.0 just came out, imagine that being KDEfied among with all the other stuff.\nWill it ever happen?"
    author: "reihal"
  - subject: "Re: Good to Hear"
    date: 2004-03-24
    body: "Well, using the Qt-Gtk style engine and possibly the KDE file-dialogs\nit can be pretty much KDEfied already."
    author: "OI"
  - subject: "Re: Good to Hear"
    date: 2004-03-26
    body: "YaST was never closed-source. The license was similiar to GPL, but didn't allow people to make money from it. Slight difference, but it just allows SuSE (you know, the company that spent money to develop it) to make money from it."
    author: "Tim Barber"
  - subject: "Re: Good to Hear"
    date: 2004-03-26
    body: "Mmm. Didn't know that. Thanks. I'm probably listening too much to what other people have wrote."
    author: "David"
  - subject: "Re: Good to Hear"
    date: 2004-07-24
    body: "well, the important point is that it was neither Free Software nor Open Source.\n\nPeschm\u00e4"
    author: "Peschmae"
  - subject: "beta"
    date: 2004-03-24
    body: "wie wird man denn betatester bei SUSE?"
    author: "Bert"
  - subject: "Re: beta"
    date: 2004-03-24
    body: "Ja!"
    author: "Olaican Seyin Germaniz Ja"
  - subject: "Re: beta"
    date: 2004-03-25
    body: "Yes, how do you become a beta tester for SuSE??\nAny thoughts SuSE or Novell??\n--\nJ\n"
    author: "jimbo"
  - subject: "SUSE questions"
    date: 2004-03-24
    body: "Hello all,\n\nAs a long time MDK user, I have some questions about SUSE that I'm hoping someone can answer....\n\n1) Aside from some commercial applications and ports to other architectures (which I don't have any current use for), what is the difference between the professional and personal versions of SUSE?\n\n2) One of the nice things about MDK is that I get all of the development packages for the binaries. This means that I can compile stuff that MDK doesn't include. I do this a lot. The personal version of SUSE claims to only have 1 installation CD. How do SUSEers get the devel packages if they want to compile their own stuff? Do they need to get the source and compile, or are there RPM's provided by SUSE somewhere? If these RPM's are provided, how do you get them? Can I get them on CDROM or do I have to download them one at a time from an FTP site or something?\n\n3) How are updates handled? MDK uses mirrors to handle security updates. Does SUSE do the same in an automatic fashion? Do they use mirrors or direct downloads from their own servers?\n\n4) One of the big reasons I use MDK is the fact that I *know* that I can say to someone, \"Hey, you want to try Linux? I've got this MDK CD that I can burn for you and give you. Yes, it's totally legal to do this.\" In the past, SUSE wouldn't let me do this because of YAST. With the GPLing of YAST, does this mean that I'll be able to redistribute SUSE now?\n\n5) This sounds petty, but it's important. I give MDK money because I know they have a good product. I would prefer not to give SUSE money until I have the same confidence. Are there any plans to have ISO's available for installs? Yes, I know you can do an FTP install, but I like installation CD's available if something goes wrong. Will cheapbytes have CD's available (likely determined by the answer to question 4)?\n\n6) URPMI rocks. Does SUSE include something similar to URPMI or apt? For the most part, URPMI has made me forget all about dependancy hell that I used to have to deal with when I used RedHat.\n\n7) One of the nice things about RedHat is their application installation GUI. MDK doesn't have anything like it, but when you use RedHat, you can go in and see (for example) that you have 10 of 12 KDE packages installed. Diving in deeper, you can see the 10 you have and the 2 that you don't have installed with a little info blurb on each one. This paints a nice overview of your system and helps people see what is available without having to grep through filenames on the CD. How does SUSE handle this?\n\nThanks in advance for anyone helping to remove my ignorance of SUSE :)\n\n\nDavid"
    author: "David Joham"
  - subject: "Re: SUSE questions"
    date: 2004-03-24
    body: "I haven't used SuSE since 7.3, but I'll answer from my experience years ago...\n\n1) and 2)\n\nThe main difference was that Professional shipped with all of those development packages, as well as a far wider range of binaries, and an extra manual that was a bit like the RedHat Bibles you find in bookshops.\n\nIf you bought personal, as I did, you had to download all the extra binaries and development packages from SuSE's FTP servers.\n\n\n3) Mirrors were handled automatically, though you could manually select too.\n\n\n4) Actually, you could always freely redistribute SuSE with the proprietary YaST, so long as you didn't make money from the distributing. But now you can earn from it too.\n\n5) No idea.\n\n6) It didn't when I used it. URPMI is a lot better than plain RPM.\n\n7) From SuSE 7.3, and setting up Mandrake 8.0 -> 9.1 on other people's machines, SuSE's package management GUI was by far the nicer. I'm not sure exactly how it handles it now though; you may find some screenshots on their web site that answer this question."
    author: "Tom Chance"
  - subject: "Re: SUSE questions"
    date: 2004-03-24
    body: "As a long time MDK user, I have some questions about SUSE that I'm hoping someone can answer....\n\nI've used SuSE for a couple of years so I'll try to answer.\n \n 1) Aside from some commercial applications and ports to other architectures (which I don't have any current use for), what is the difference between the professional and personal versions of SUSE?\n\nPersonal doesn't come with development packages and some networking functions (at least the last personal I installed).\n \n 2) One of the nice things about MDK is that I get all of the development packages for the binaries. This means that I can compile stuff that MDK doesn't include. I do this a lot. The personal version of SUSE claims to only have 1 installation CD. How do SUSEers get the devel packages if they want to compile their own stuff? Do they need to get the source and compile, or are there RPM's provided by SUSE somewhere? If these RPM's are provided, how do you get them? Can I get them on CDROM or do I have to download them one at a time from an FTP site or something?\n\nDownloadable from SuSE and mirrors.\n \n 3) How are updates handled? MDK uses mirrors to handle security updates. Does SUSE do the same in an automatic fashion? \n\nYes,  Yast Online Update (YOU)\n\nDo they use mirrors or direct downloads from their own servers?\n\nYour choice.\n \n 4) One of the big reasons I use MDK is the fact that I *know* that I can say to someone, \"Hey, you want to try Linux? I've got this MDK CD that I can burn for you and give you. Yes, it's totally legal to do this.\" In the past, SUSE wouldn't let me do this because of YAST. With the GPLing of YAST, does this mean that I'll be able to redistribute SUSE now?\n\nYou could always give away SuSE CDs legally, not not charge for them though.\n \n 5) This sounds petty, but it's important. I give MDK money because I know they have a good product. I would prefer not to give SUSE money until I have the same confidence. Are there any plans to have ISO's available for installs? Yes, I know you can do an FTP install, but I like installation CD's available if something goes wrong. Will cheapbytes have CD's available (likely determined by the answer to question 4)?\n\nDon't know.\n \n 6) URPMI rocks. Does SUSE include something similar to URPMI or apt? For the most part, URPMI has made me forget all about dependancy hell that I used to have to deal with when I used RedHat.\n\nYaST and/or aptget for SuSE.\n \n 7) One of the nice things about RedHat is their application installation GUI. MDK doesn't have anything like it, but when you use RedHat, you can go in and see (for example) that you have 10 of 12 KDE packages installed. Diving in deeper, you can see the 10 you have and the 2 that you don't have installed with a little info blurb on each one. This paints a nice overview of your system and helps people see what is available without having to grep through filenames on the CD. How does SUSE handle this?\n\nYaST\n \n"
    author: "Olav P"
  - subject: "Re: SUSE questions"
    date: 2004-03-25
    body: "Other people have answered most of these, so I'm only going to hit a few:\n\n3) YOU (Yast Online Update) lets you pick from a list of servers & mirrors. Works great.\n\n4) I've been using SuSE since 7.1 (like four years ago), first installed with a burned copy of a friend's CD's. Perfectly legal then, and now. I then went out and bought the upgrades when they came out.\n\n5) They have a freely downloadable live CD ISO, which doesn't get enough press. It's like a SuSE Knoppix - it goes through the install process, detects your hardware, but dosen't write to the disk. It boots to a KDE desktop and lets you play around for a bit. The idea is that if that works, you go out and buy the CD set.  This isn't a bad deal. \n\n6) Never tried URPMI, but YAST does a pretty good job of package management. I don't have anything else to compare it against, but it's gotten much better over the years.\n\n7) For my money, YAST alone is worth buying the CD set. It installs, it configures, it upgrades, integrates cleanly into Kcontrol, it slices, it dices... and does a good job of all of it. (except the dicing. Pretty shitty at that.)\n\nRich"
    author: "Rich Jones"
  - subject: "Re: SUSE questions"
    date: 2004-03-25
    body: "Well.. if you give money to suse you will have de cd isos...\n\n----------------------------------------------------------------\n5) This sounds petty, but it's important. I give MDK money because I know they have a good product. I would prefer not to give SUSE money until I have the same confidence. Are there any plans to have ISO's available for installs? Yes, I know you can do an FTP install, but I like installation CD's available if something goes wrong. Will cheapbytes have CD's available (likely determined by the answer to question 4)?\n\n"
    author: "lokillo"
  - subject: "Re: SUSE questions"
    date: 2004-03-26
    body: "Hi\n\nAs a long time Mandrake user as well I see the Mandrake \"experience\" as a distro that includes a lot of softwares on 3 CDs + the FTP contrib repository + the PLF repository for softwares with license issues (e.g libdvdcss, P2P stuff, etc..) and  a decent Software Management tool (urpmi).\nI'm a MDK club silver member.\n\n<Really Important To Me>\nIs there an equivalent to the MDK contrib repository and PLF repository?\nIf yes, can these \"extra\" repositories be handled via YAST2?\n</Really Important To Me>\n\nI'm considering trying the SuSE 9.1 Live CD when it's out and then maybe spend the money on 9.1 Pro.\n\nThanks."
    author: "Woollhara"
  - subject: "Re: SUSE questions"
    date: 2004-03-26
    body: "Before you think about to change to SuSE hear what I've to say: I used SuSE for years but now I changeing to another distribution.\n\nWhy? Oh, it's quite simple. Many things are different from the Linux standard. If you don't get a special RPM for SuSE and wan't to install another RPM or compile by yourself, be sure that the installation target is a different one. SuSE installes e.g. all KDE stuff at /opt/kde3. Standard would be /usr!\n\nThe SuSE 9.0 e.g. don't inkludes Krusader. But if you compile it by yourself it will be installed at /usr/share/krusader. Normally the both possible installation pathes are /usr/share or /usr/lib. But not at SuSE.\n\nFurthermore some of the configuration files are different. So it is not quite easy to live with that. Therefore, as I had enough with this trouble with SuSE I tried Fedora and Mandrake (Debian I don't get installed because they have problems with my DVD and CD drives). Both are better especially the Mandrake 10.0 Community Edition (the only thing I do not like is the packager and the menu program of Mandrake they are for the trashbin ;-)\n\nAnd if you find some articles who to install or configer software for Linux be shure that at a SuSE system it is different.\n\n\nCautionary greetings\n\nReiner\n"
    author: "Reiner Block"
  - subject: "Re: SUSE questions"
    date: 2004-03-27
    body: "Most of your points have been answered by others above. But I still missed a few.\n\nDo not expect ISO's of the current SuSE version. It's not going to happen.\nSuSE are perfectly honest about that. They need the money and kindly ask you to pay for the latest and the greatest. In return, you will get tons of high quality software and excellent documentation with it. Their documentation is worth the money alone.\nHowever, they do provide you with an option to do an ftp-install. So you can actually get the current version for free if noone around you has the cd's for you to borrow.\n\nI switched away from SuSE after 8.2 to Debian. My opinion is based on that. I cannot comment on the current version! Read the reviews to see if anything was improved.\nOther posters already mentioned a few points. Yast is by no means perfect. It never managed to install a proper nvidea driver for my video card. I didn't care, because the dummy provided is enough for my needs. Second, it is not safe to use rpm's that are not labeled explicitly for use with SuSE. Many things such as config files and install locations differ from commonly used standards in other distros. Sometimes it works, sometimes it doesn't.\nI have a dialup connection and quite often YOU (Yast Online Update) simply refused to do anything at all, without telling me why so I couldn't do anything to fix it.\n\nYou can easily check the availability of any package you desire for yourself. You can log in the ftp server and see if it's there. Everything is neatly organized in there. You should be able to get an answer to all your questions very quickly. If it's there, Yast can get it for you.\nYou will notice that previous versions are supported and rpm's are provided. Note, however that it usually takes some time before rpm's are there. Some commonly used stuff like KDE for example will show up immediately on SuSE servers. Other, less commonly used software will not show up immediately and it might take some time before SuSE rpm's are available.\n\nCheers,\nJan"
    author: "Jan Rigter"
  - subject: "Re: SUSE questions"
    date: 2004-03-30
    body: "Most of the bases have been covered, but as someone currently running both Mandrake (several installations of 9.1, 9.2) and Suse (8.2) I thought I'd offer a few observations.\n\n - Suse's GUI config tools are generally less buggy -- for instance MDK 9.2's wireless LAN config and X setup using ATI or nVidia drivers are basically broken for me. In general YAST is a more reliable system configurator than Mandrake Control Center. On the other hand a couple of the MDK machines are CL-only servers and have run and even distro-upgraded via URPMI without having to be console logged in for over a year -- I actually love MDK as a CL distribution!\n\n - I've done the Suse FTP install a few times with no problems ... in general Suse's servers and mirrors are less crowded than the MDK mirrors. If you're nervous about doing the FTP install, you can always mirror the FTP dir to a local machine ... although that's going to be several gigs.\n\n - CL YAST is a full-window terminal app, which is great for newbies because you can get a GUI even in an ssh term or from console login. However I like to script URPMI and run it from cron ... maybe there's a way to do that with YAST ... anyone know?\n\n - Suse is in general a more finished, polished workstation OS. It's less buggy and clearly reflects a much more thorough, systematic integration and testing process. As people have said, system layout may be a bit different and config files a bit different, but it's nothing major, and all the config files are well commented.\n\n"
    author: "Joel"
  - subject: "Re: SUSE questions"
    date: 2004-04-29
    body: "I don't know about answering all your questions.  It sounds like you have been in Linux world longer than I have been, but I do have this to say.\nI just recently bought the Suse 9.0 Professional retail box, and this thing rocks!\nNot only did it come with 5 cd's, I also got a double sided DVD if I wanted to install it that way! (Which of course I did, a lot less disk swapping that way!)\nIt also came with two very good professional grade manuals in the box!\nAnd the YaST utility is great! That is once I got past the NVidea driver problems!\n(No, it isn't Suse's fault, it is NVidea's fault. They won't GPL the drivers)\nOtherwise, the whole package is great!\n"
    author: "Ken"
  - subject: "KDE only?"
    date: 2004-03-24
    body: "So theyre making a KDE-only personal distro?\nAnd everything KDEfied? Looks like even the gimp is KDEfied.\n\nGood idea."
    author: "OI"
  - subject: "Re: KDE only?"
    date: 2004-03-24
    body: "No, Ximian is owned by Novell, so they will ship Ximian desktop, but Nat Friedman is not SuSE's platform strategist. SuSE is committed to KDE and the customers request it. What SUSE wants is better integration of gtk and non-qt/kde apps. And a Suse guy asked me: \"What's wrong with qt/KDE?\" they want to encourage Win-Programmers to recompile under Linux and use qt.\n\nBtw: As wine now compiles MFC with gcc I wonder if we could create a linux qt wrapper for MFC.\n\nbtw: Why doesn't SUN adopt qt as Java's standard GUI? They could easily license the qt technology from Trolltech."
    author: "Gerd Br\u00fcckner"
  - subject: "Re: KDE only?"
    date: 2004-03-25
    body: "If one thing should be quickly apparent about Sun, it would be they like to have control over their products. They would lose some if started using Qt. Especially given that Qt is in direct competition with Java (its write once, compile anywhere with Qt... pretty close to Java's goal).\n\nAs far as the topic is concerned, I do find it somewhat worrying that Novell is willing to open SuSe's crown jewels. Hopefully it is just a sign of their commentment to open source software."
    author: "Ian Monroe"
  - subject: "Re: KDE only?"
    date: 2004-03-25
    body: "There's a piece over on OSNews about Novell's plans. They're looking to crate an uber-desktop, starting with SuSE 10, that mixes the best parts of both Gnome and KDE (I'm guessing it's e.g. a panel from gnome, filemanager from KDE, best of breed apps a la Redhat, unified theming engine (via GTK-Qt) and a certral Gnome/KDE desktop config utility."
    author: "Bryan Feeney"
  - subject: "Re: KDE only?"
    date: 2004-03-25
    body: "Yuck. Multiple toolkits = inconsistancies, memory / cpu bloat"
    author: "LuckySandal"
  - subject: "Re: KDE only?"
    date: 2004-03-26
    body: "I hate the word bloat. It's so overused. The fact of the matter is that in the short to medium term there will be at least two core toolkits in use on Unix desktops (with several wrappers), and users will generally have programs running simultaneously using both. So you might as well get both loaded right at the start and running together for faster startup times overall."
    author: "Bryan Feeney"
  - subject: "Re: KDE only?"
    date: 2004-03-25
    body: "Doesn't look to me like they'll ship ximian with suse 9.1 personal..."
    author: "OI"
  - subject: "Re: KDE only?"
    date: 2004-03-25
    body: "\"btw: Why doesn't SUN adopt qt as Java's standard GUI? They could easily license the qt technology from Trolltech.\"\n\nMainly because they already have AWT, which calls the native GDI anyway. Also, it would be expensive to license on Windows, especially seeing as JDK/JRE are free products."
    author: "LuckySandal"
  - subject: "Re: KDE only?"
    date: 2004-03-25
    body: "Yes SUSE Personal is mainly KDE only and is targeted at the Linux desktop/home novice.  SUSE Professional provides other choices for people who want it.\n"
    author: "Navindra Umanee"
  - subject: "Re: KDE only?"
    date: 2004-03-25
    body: "Gimp looks KDE-fied because SuSE uses CraigD's Geramik based on QtPixmap, which makes every GTK application look like Keramik."
    author: "Yaba"
  - subject: "YaST != YaST2"
    date: 2004-03-25
    body: "AFAIK there still is a difference between YaST and YaST2.\n\nYaST was always text mode based.\n\nYet, it could be, that YaST 2 is really only a graphical fronend to the same (old) YaST backend.\nStill the question would be, if the part, they're now GPL'ing is only the backend part or also the graphical (Qt-based ?) front end.\n\n\nI could be completely wrong...\n"
    author: "Harald Henkel"
  - subject: "Re: YaST != YaST2"
    date: 2004-03-25
    body: "Yes I think you are completely wrong.\n\nAFAIK the old YaST 1 has been completely removed from SuSE Linux, YaST had been completely rewritten for version 2. And I dont think they are talking about GPLing that old version but about version 2 which is actually very much more powerful."
    author: "Stephan Oehlert"
  - subject: "Re: YaST != YaST2"
    date: 2004-03-25
    body: "YaST 2 can be run in curses mode, i.e., without X."
    author: "Rich Jones"
  - subject: "Next major version of SUSE to integrate Ximian?"
    date: 2004-03-25
    body: "From the eWeek article recently featured in Slashdot, Chris Schlager, vice president of research and development for SUSE said:\n\n\"You won't see a lot of that SuSE Linux Desktop 9.1 or SuSE Linux Enterprise Server (SLES) 9.0. It's not clear what the name will be, but you'll see the first major results of this effort in the next versions of SUSE Linux, which will be released toward the end of the year.\" \n\n\nIn the meantime, \"the work has already started,\" Schlager said, with the Ximian Desktop 2.0 being merged into the SuSE Linux Desktop 9.1. As for potential conflicts, he said simply, \"I don't think turf wars will happen here.\" \n\nSo the question is, what aspects of Ximian are they going to exactly 'merge' in SUSE? And what consequences will this have on KDE and SUSE?\n"
    author: "Jasem Mutlaq"
  - subject: "Re: Next major version of SUSE to integrate Ximian?"
    date: 2004-03-25
    body: "Yeah, I've been waiting to see if anyone would raise this article over here.  My reading of the whole thing was they are planning to cannibalise both K and G to come up with some new desktop, say using Kparts and GConf, KIO and Gail, etc...  Now, how the hell that could EVER work, I don't know???\n\nJohn."
    author: "John"
  - subject: "Re: Next major version of SUSE to integrate Ximian?"
    date: 2004-03-25
    body: "Here's the link:\n\nhttp://www.eweek.com/article2/0,1759,1553087,00.asp\n\nNow the weird thing is he says it's technically impossible to merge the two into one, but then says they will take the best of each to create one desktop.  Now how is that different, what does he mean???  He says Ximian and Suse have the programming muscle to do \"it\" whatever \"it\" is.  I think the community deserves a clearer explanation of their intent.\n\n"
    author: "Odysseus"
  - subject: "Re: Next major version of SUSE to integrate Ximian?"
    date: 2004-03-25
    body: "My guess is: Use KDE as desktop. Use the Qt-Gtk engine for all gtk-apps."
    author: "OI"
  - subject: "Re: Next major version of SUSE to integrate Ximian?"
    date: 2004-03-25
    body: "They mean integrating GTK apps and OOo into KDE like they are already doing in SUSE 9.1."
    author: "ac"
  - subject: "Re: Next major version of SUSE to integrate Ximian?"
    date: 2004-03-25
    body: "I think it's vice versa. From the eweek article (http://www.eweek.com/article2/0,1759,1553087,00.asp):\n\n\"In the meantime, \"the work has already started,\" Schlager said, with the Ximian Desktop 2.0 being merged into the SuSE Linux Desktop 9.1.\"\n\nThat means the Gnome *Desktop* is merged into the SuSE Linux (Desktop) *Distribution*. In other words: The development versions of SUSE Linux are now gnome based by default.\n\nBad news, but that was expectable... why should a company buy a Gnome Desktop comp. and a Linux distribution, esp. in the USA, where gnome is more accepted then KDE.\n\nfuranku\n\n\n"
    author: "furanku"
  - subject: "Re: Next major version of SUSE to integrate Ximian?"
    date: 2004-03-25
    body: "> why should a company buy a Gnome Desktop comp.\n\nXimian was not only desktop, also and especially interesting for Novell RedCarpet and Mono."
    author: "Anonymous"
  - subject: "Re: Next major version of SUSE to integrate Ximian?"
    date: 2004-03-25
    body: "Since Ximian Desktop was never mentioned at all in the blurb for Suse 9.1, I find this curious also. Probably what it means is that Ximian Desktop is being taken apart, and bits integrated into Suse Desktops. Red Carpet is probably a candidate here.\n\n\"That means the Gnome *Desktop* is merged into the SuSE Linux (Desktop) *Distribution*. In other words: The development versions of SUSE Linux are now gnome based by default.\"\n\nNope, I think you're being paranoid here. This doesn't match up with how Suse is promoting what they are doing, the software they are using or their investments in KDE i.e. KDE Open Office. Look at what is actually happening.\n\n\"Bad news, but that was expectable... why should a company buy a Gnome Desktop comp. and a Linux distribution, esp. in the USA, where gnome is more accepted then KDE.\"\n\nConsidering the market share of Windows, Gnome is not more accepted in the US than Europe. Let's keep our feet on the ground please."
    author: "David"
  - subject: "Re: Next major version of SUSE to integrate Ximian?"
    date: 2004-03-25
    body: "OK. According to the www.heise.de posting mentioned below it looks like I was too pessimistic.\n\nBut Novell's Chris Stone just said officially that they want to combine both Desktops on the basis of one of them. So there will be a clear descission for one of them. IBM announced to use SUSE as standard Linux distribution on thier servers and HP even on servers, desktops and laptops. So this will be a really frustrating day for one of the DE's.\n\nThe posting says further \"Later that day it leaked out, that Novell decided to use QT as development-environment and -library\".\n\nMaybe I'm paranoid but that's a) unofficially and b) quite vague (Why \"development\"?)\n\nfuranku"
    author: "furanku"
  - subject: "Re: Next major version of SUSE to integrate Ximian?"
    date: 2004-03-25
    body: "Using Qt doesn't necessarily imply KDE, although Qt is obviously at home with KDE so there is an implication that the desktop base will be KDE-based.\n\n\"IBM announced to use SUSE as standard Linux distribution on thier servers and HP even on servers, desktops and laptops. So this will be a really frustrating day for one of the DE's.\"\n\nI don't think so. There's still a heck of a long way to go for Linux and free desktops in general.\n\n\"Maybe I'm paranoid but that's a) unofficially and b) quite vague (Why \"development\"?)\"\n\nI assume they use the word development because they are developing with it? Just a wild stab in the dark."
    author: "David"
  - subject: "Re: Next major version of SUSE to integrate Ximian?"
    date: 2004-03-25
    body: "I suspect it simply means that the best of all applications will be included. KDE has some good ones, gnome has some good ones. Openoffice and Mozilla are neither.\n\nXimian has the software update stuff that is good. Also the exchange plugin.\n\nI'm sure that the rest of the stuff will be argued and fought over.\n\nCompetition is good. Novell seems smart enough to not try to dictate, rather observe, contribute and listen.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Next major version of SUSE to integrate Ximian"
    date: 2004-03-25
    body: "heise has a really interesting article on the topic:\n\nIn der letzten Keynote der Messe fuhr Chris Stone, der eigentliche Lenker von Novell, am Mittwochmorgen schon ein anderes Kaliber auf. Stone erkl\u00e4rte, dass die Desktops auf der Basis eines Desktop-Vorbildes kombiniert werden sollen. Schlie\u00dflich sickerte im Verlauf des Tages die Nachricht durch, dass Novell sich bei der Entwicklungsumgebung und -bibliothek f\u00fcr Qt entschieden hat, womit praktisch KDE das Rennen machte -- dieser Unix/Linux-Desktop baut im Unterschied zu Gnome auf Qt. Ein leitender Suse-Manager zeigte sich mit der Entscheidung zufrieden und betonte, dass Qt die besser definierten Schnittstellen und eine erhebelich gr\u00f6\u00dfere Gemeinde von Entwicklern hinter sich habe. Die Entscheidung f\u00fcr KDE sei so gesehen die logische Konsequenz.\nhttp://www.heise.de/newsticker/meldung/45991\n\nthe second part roughly says \"In the course of the day, news were floating in that Novell made the decision to use Qt as development environment and library, meaning that it is practically KDE that is making the race -- .... A leading Suse-Manager was very content with that decision and pointed out, that Qt had a much better defined interface and a much larger community of developers. The decision for KDE was the logical consequence of this\"\n\nHope this help to clears things up before SuSE/Novell comments directly."
    author: "burki"
  - subject: "Re: Next major version of SUSE to integrate Ximian"
    date: 2004-03-25
    body: "I guess we will see a lot of Mono/Qt apps by Novell then, the best of both worlds..."
    author: "Datschge"
  - subject: "Re: Next major version of SUSE to integrate Ximian?"
    date: 2004-03-25
    body: "\"So the question is, what aspects of Ximian are they going to exactly 'merge' in SUSE? And what consequences will this have on KDE and SUSE?\"\n\nNone. They are breaking Ximian Desktop up."
    author: "David"
  - subject: "Ximian pushes very strong for Gnome"
    date: 2004-03-25
    body: "I have a couple of friends that are very closed to Ximian and Miguel de Icaza. Miguel is trying very very hard to convince Novell that KDE is a waste of time and money. It's very real, and Miguel cannot say the opposite.\n"
    author: "charles dickens"
  - subject: "Re: Ximian pushes very strong for Gnome"
    date: 2004-03-25
    body: "I've heard rumors that there is very much internal friction in Novell (not Ximian or Suse) over COD (choice of desktop)\n\n"
    author: "anon"
  - subject: "Re: Ximian pushes very strong for Gnome"
    date: 2004-03-25
    body: "I'd like to hear his arguments."
    author: "Anonymous"
  - subject: "Re: Ximian pushes very strong for Gnome"
    date: 2004-03-25
    body: "I have very close friends in the airport business, and I heard that Miguel de Icaza sacrifices the innocent children KDE developers so he can worship his satanic Gnomish gods. He has cast a spell on all of Novell, and has made them buy Suse so he can introduce C memory leaks into KDE, and take dumps in burning paper bags and leave them on the doorstop of KDe users.\n\nIts very real, and Miguel cannot say the opposite. Swear to god, hope to die if I'm trolling."
    author: "David"
  - subject: "Re: Ximian pushes very strong for Gnome"
    date: 2004-03-25
    body: "Well, we should all take this seriously, and should embrace for a counter strategy. Let's start by worshiping some kickass God that can beat the hell out of the Gnomish ones. \n\nI for one, vote for Thor, The NordiK God of Thunder!\n"
    author: "Jasem Mutlaq"
  - subject: "Re: Ximian pushes very strong for Gnome"
    date: 2004-03-25
    body: "Do you have a couple of emails coming from Novell/Ximian employees? \nCan you read romanic languages? (so, do you understand why I can't disclosure them w/o losing some friends? :-). \n\nI can forward them to you. Just write your real email address (yup, didn't check \"David\"'s link in your message yet:).\n\nBTW, it seems that at Ximian they are really convinced that KDE is pure crap and that Gnome is going to rule the desktop with the support of all big companies, while KDE will be just the place for free [software] volunteers.\n\nDon't believe me? Just a clue, Miguel de Icaza had planned to go to Brazil this year, it's cancelead for the moment. He is not going to Spain in the coming months, neither.\n\nI't no _so_ important, just don't believe on unconditional support from Novell/SUSE while Ximian/Gnome is the same [war]ship. Be skeptical.\n"
    author: "charles dickens"
  - subject: "Re: Ximian pushes very strong for Gnome"
    date: 2004-03-25
    body: "http://en.wikipedia.org/wiki/Dickens\n\n\"[...] Dickens' writing style is florid and poetic, with a strong comic touch.\nMuch of Dickens's writing seems sentimental today[...]\""
    author: "Thomas"
  - subject: "Re: Ximian pushes very strong for Gnome"
    date: 2004-03-25
    body: "No, I'm not the David above, and please don't hand out confidential information from anyone to anyone. That's just not good. This is a matter for Suse/Novell and the relevant people, and really has little to do with KDE believe it or not.\n\n\"BTW, it seems that at Ximian they are really convinced that KDE is pure crap and that Gnome is going to rule the desktop with the support of all big companies, while KDE will be just the place for free [software] volunteers.\"\n\nWell, they are probably the only people who are convinced, but we knew that already.\n\n\"I't no _so_ important, just don't believe on unconditional support from Novell/SUSE while Ximian/Gnome is the same [war]ship. Be skeptical.\"\n\nI doubt whether Ximian will have much influence over Suse."
    author: "David"
  - subject: "Re: Ximian pushes very strong for Gnome"
    date: 2004-03-25
    body: "well, it's going to be official the next days:\nSeems, that Novell has decided to go for KDE on their \"corporate desktop\" - whatever that is...\n\nhttp://www.heise.de/newsticker/meldung/45991\n\nhttp://tinyurl.com/2bp9f (Google translation)\n\n"
    author: "Thomas"
  - subject: "Re: Ximian pushes very strong for Gnome"
    date: 2004-03-25
    body: "Please avoid those looooooooooong URLs in here, make it a link. And btw, try that it actually works."
    author: "Anders"
  - subject: "Wise Old Yoda Says:"
    date: 2004-03-25
    body: "quote from (http://tinyurl.com/2bp9f (Google translation)):\n\n\"If KDE the dominating Desktop becomes, there is a faith war less\""
    author: "A nony mous"
  - subject: "Re: Wise Old Yoda Says:"
    date: 2004-03-25
    body: "Yer, I love Google translations :)."
    author: "David"
  - subject: "Re: Ximian pushes very strong for Gnome"
    date: 2004-03-25
    body: "Maybe he should try to tell Trolltech that Qt is a waste of time and money.\nI'd look forward to that discussion."
    author: "OI"
  - subject: "Re: Ximian pushes very strong for Gnome"
    date: 2004-03-29
    body: "Maybe, novell doesn't want to put all their effort into a gnome based desktop, and gnome based apps, just to have redhat take all of their work and put it into the redhat environment. This sounds like a great busines move for novell. No reason to do all of redhats work for them. \n\n"
    author: "Matt"
  - subject: "Looks like KDE made it against GNOME"
    date: 2004-03-25
    body: "According to the German c't Newsticker, KDE won the race against GNOME at Novell. At least they say they will follow a single desktop paradigm and standardize on Qt. At the same time, IBM and HP confirmed that they will be using SuSE for their desktops. See http://www.heise.de/newsticker/meldung/45991 (German).\n\nThomas"
    author: "Thomas Diehl"
  - subject: "Re: Looks like KDE made it against GNOME"
    date: 2004-03-25
    body: "Oh please. I hated comments like this when they were directed at KDE, and I hate them vice versa.\n\nYes Suse is doing what they think best for future strategy, and that is a matter for Suse rather than KDE, but people are making it sound as if KDE has taken over the world just as, not two months ago, people said Gnome was taking over. May I remind everyone that 90% of the world is Windows and there are still Macs out there, so the one desktop stuff is total rubbish. I now know why Microsoft rushed off into the distance in the late 80s/early 90s.\n\nWhatever happens, people are going to have to cater for different desktops. It's that simple. Let's not drink the Koolaid people."
    author: "David"
  - subject: "Re: Looks like KDE made it against GNOME"
    date: 2004-03-25
    body: "I couldn't agree more!"
    author: "Christian Loose"
  - subject: "The market decides"
    date: 2004-03-25
    body: "Everybody knows that there is no plattform war on the side of KDE, I also use and like Gnome. But what I found ugly was the media fud against KDE/qt and the Vapor-announcements about Novell of Ximian employees. What I also don't like is lack of cooperation.\n\nSuSe is a KDE focussed distribution, it always was. They will switch to gnome in order to please a small tecznology buyout. SuSe sells a product, miguel provides technology.\n\nI don't care if a hacker uses Xfce, xpde, Gnome, Gnustep whatever. But for the desktop KDE is the plattform of choice as it has the maturity now.\n\nwhat we will see is that Suse starts market research which products its customers for a special product want. "
    author: "Bert"
  - subject: "hold up, hold up, hold up"
    date: 2004-03-26
    body: "The german newspaper Der Standard is throwing some caution into Heise's scoop: \n \nhttp://derstandard.at/standard.asp?id=1612594 \n \nFrom the GoogleFish of the text under the 'Ein Desktop' section: \n \n\"A Desktop as eweek reported also announced head of the company Jack Messman that the company wants the best from both worlds to unite and a common Linux Desktop platform create wants. *Remains unclear however for the time being still on basis of which Widget LIBRARY this passing is, while heise reports that it is to concern thereby geruechteweise QT*, stressed the Novell EntwicklerInnen opposite that to weaving and pool of broadcasting corporations for the time being that still no decision is to be fallen.\" "
    author: "anon"
  - subject: "Re: hold up, hold up, hold up"
    date: 2004-03-27
    body: "This is just something Jack Messman came up with earlier on. The Qt stuff came up later, although none of it is official and I wouldn't take either article as written as a lot of people may very well be spreading misinformation.\n\nHowever, the article you quote seems to put a very Ximian oriented spin on things, and I wouldn't be surprised if some have been listening to the wrong people as usual. It also talks about most of the desktop talks at Brainshare being from Ximian and the, now ubiquitous, Qt license crap at the bottom.\n\nIf you look here (http://www.novell.com/brainshare/catalog/catalog/catalog.jsp), there are many significant talks about Linux desktops by Suse and Novell people, a large return on investment talk being one of them. The Ximian talks tend to be made by themselves, so I just don't believe this article. The article also talks about some big thing happening in May where different groups will come together over the issue.\n\nUntil we hear something official don't believe anything, and certainly don't believe the Ximian people. In this article (http://www.infoworld.com/article/03/11/24/46NNdesktop_1.html), before the ink was even dry on the Suse/Novell, Nat Friedman was spouting crap about Ximian Desktop being put onto Suse 9.0 and tightly integrated with Novell software. I must have missed that one Nat. Nice prediction, and I'm sure Suse were pleased to hear someone talking about the future of their products, probably without them knowing. Stuff like this is somewhat politically naive and stupid considering that it wouldn't have pleased the Suse people at all, supposedly Ximian's new partners. Certain people cannot keep substituting hype, vapourware, blogs and general hot air for results and substance. Desktop Linux has got to start working, and fast.\n\nAnyway, whatever happens, at the moment it probably won't make a whole lot of difference to be honest."
    author: "David"
---
<a href="http://news.com.com/2100-7344_3-5175682.html">Several</a> <a href="http://www.infoworld.com/article/04/03/19/HNgroupwise_1.html">sources</a> have been reporting on <a href="http://www.suse.com/us/company/press/press_releases/archive04/yast.html">the GPL'ing of YaST</a>, <a href="http://www.suse.com/">SUSE</a>'s system administration and setup tool that is fairly well-integrated into KDE (and reportedly even more so in SUSE 9.1).  While this suddenly makes SUSE all the more palatable to the free software community, it could also be quite interesting for KDE.  YaST is potentially a starting point for a free cross-distribution, and eventually maybe even cross-platform, KDE-based administration tool.





<!--break-->
<p>
In other good news, it <A href="http://www.suse.com/en/private/products/suse_linux/preview/interview.html">has been reported</a> by beta users that <a href="http://www.suse.de/en/company/press/services/images/screenshots/sl_91/pers_en.png">the KDE</a> <a href="http://www.suse.de/en/company/press/services/images/screenshots/sl_91/prof_en.png">Desktop</a>, <a href="http://www.suse.de/en/company/press/services/images/screenshots/sl_91/prof_de.png">several key</a> <a href="http://www.suse.de/en/company/press/services/images/screenshots/sl_91/pers_de.png">KDE applications</a> as well as <a href="http://kde.openoffice.org/">a KDE'fied OpenOffice.org</a> are featured exclusively in the <a href="http://www.suse.com/en/private/products/suse_linux/preview/pers.html">SUSE 9.1 Personal</a> normal installation as well as live CD feature.  Since SUSE Personal is targeted at users who might feel bewildered by excess choice and functionality overlap, cutting the cruft significantly improves usability for that demographic.  <a href="http://www.suse.com/en/private/products/suse_linux/preview/prof.html">SUSE 9.1 Professional</a>, on the other hand, will include a broader swiss-army knife choice of desktop environments and applications to satisfy experienced users.








