---
title: "Quickies: amaroK, KDE-People Newspage, IP Telephony, Qt 3.3.2, KMail, Usability Blogging"
date:    2004-04-28
authors:
  - "fmous"
slug:    quickies-amarok-kde-people-newspage-ip-telephony-qt-332-kmail-usability-blogging
comments:
  - subject: "Skype for Linux/Qt... lovely day :-)"
    date: 2004-04-28
    body: "I can't beleve it :-)...\n\nIf only it could be opensource so it could be well integrated to KDE and well packaged."
    author: "Olivier LAHAYE"
  - subject: "Re: Skype for Linux/Qt... lovely day :-)"
    date: 2004-04-29
    body: "Btw. the link seems to be borked in the above summary, should be http://skype.com/jobs.html#tallinn"
    author: "Datschge"
  - subject: "Re: Skype for Linux/Qt... lovely day :-)"
    date: 2004-04-29
    body: "I don't think it will be open source.\n\nNevertheless, won't they be forced to make it Open source if they design it with QT ?\n\nI think they'll choose GTK eventually...\n"
    author: "Nicolas Blanco"
  - subject: "Re: Skype for Linux/Qt... lovely day :-)"
    date: 2004-04-29
    body: "Not if they license QT commercial. Then the app can be closed source."
    author: "George Moody"
  - subject: "Re: Skype for Linux/Qt... lovely day :-)"
    date: 2004-05-02
    body: "having a skype client for linux is more important to me than arguing whether ornot its 'open'"
    author: "macewan"
  - subject: "Re: Skype for Linux/Your dream coming true?"
    date: 2004-05-21
    body: "Sure Skype won't be open source. But who cares if you can download and use it for free. By the way the Skype guys sent me an email saying they have alsmost done with the Linux version of Skype."
    author: "Maxei"
  - subject: "regarding Bacon's blog"
    date: 2004-04-28
    body: "It is always great to hear voices from the KDE community which takes on the \"usablity\"-problem. It is needed, because GNOME shows a firm sense of direction and vision that is completly lacking in KDE.\n\nIt seems to me that that KDE has hit a brick wall in its development, and the bricks in that wall are the developers themselves. Here is an analogy: imagine building a car from scratch, and you want it to be of use to as many as possible: you don't need much usablity-studies or visual design competence when you are buiding the pistons, transmissions, differential etc. Here you only need engineers. But at some point in the development all these things will be in place, and other things becomes more important, like the visual style, an understanding of the likes and values of the buyers, ways of catering to the sensiblities and life of the drivers etc. If engineers are allowed to have the last word in this part of the process, the product will be clumsy, poorly formatted to a user group etc. At this point in the process the overall tasks becomes different and requires different competences.\n\nThis is the situation that KDE is in right now. The most important challenge for the environment has changed from technical improvements to the need for pursuing a top-down vision for integration and user interaction. The GNOME people has put a trap up for KDE, because they say that KDE is for the design philosophy of more options, knowing very well that this is a receipe for irrelevancy.\n\nMuch of what I see here regarding the need thinking user-interaction design is half hearted. It seems that Bacon's schema idea is one such, solving the problem of too many options (too little direction) with yet another option. (I think this this is the idea of \"user-levels\" which apparantly dosen't work very well).\n\nKDE's primary problem is how to deal with the \"brick wall\" of old school developers. GNOME has had two important advantages: 1. That the most influential people in the project has the right vision and 2. The support of big corporations, like Sun, who knows this because live off selling their stuff. \n\nI don't know how the less influntial people here who care about usablity shall solve this problem. One possiblity is to make a style/usablity fork which branches off as stable release and has the opportunity to remove, rename, redesign under a unified vision without compromise.\n"
    author: "will"
  - subject: "Please will, tell me"
    date: 2004-04-28
    body: "If you are such a firm believer in GNOME's future, why do you care about what happens to KDE?\n\nJust curious."
    author: "em"
  - subject: "Re: Please will, tell me"
    date: 2004-04-28
    body: "Well, its not so much an estimate of their future, as that I like what they are trying to to. I see a lot of quality thinking about the goal of the desktop that is lacking in KDE. \n\nThe reasons I care about KDE, as they come to mind, are:\n\n1. I have followed KDE for many years, I originally liked its dicipline and seriousness as a developer communinity, and have become \"emotionally attached\" I guess. But that is fading.\n\n2. I am under the impression that KDE has a better technical foundation, and would like to see it transform into something that users love to use.\n\n3. I am Morwegian, and would prefer that USA doesn't automatically become the center of the technological world. Especially when KDE had a head start. Besides TrollTech is a norwegian company :-)"
    author: "will"
  - subject: "I see you're serious"
    date: 2004-04-28
    body: "So I'll answer seriously.\n\nI think there's such a thing as too much \"sense of direction\".  In the Open Source world you can usually find this problem in those projects that are closer ideologically to the FSF (such as GNOME).  They want their design to be excessively innovative, or to match some sense of academic correctness, and they bet their projects on flashy but risky design decisions.  Even though these decisions usually give them good PR and tend to attract the support of many developers (and sometimes even companies), in the end they are a huge and often fatal burden.  A good example is the Hurd, but I can name many more.\n\nOn the other side I would put projects such as the Linux kernel and KDE.  Here there is a deliberately generalist approach and the focus is on practicality.  Design decisions are not too risky so they don't compromise the future of the project.  As a result, these projects tend to live much longer even with a smaller developer base.\n\nI'm of the opinion that the GNOME project will kill itself in the end with the rather radical approach to usability it has taken as of late.  Losing one of the two leading Open Source desktops to this vague idea of \"usability\" will be bad enough, but losing both would be disastrous."
    author: "em"
  - subject: "Re: I see you're serious"
    date: 2004-04-28
    body: "Thanks for your answer. A few points: \n\nI get the impression that you are talking about techical design philosophy, and here it seems to me that a pragmatic approach is wise. (I don't think FSF has much influence on GNOME now, but that is another matter).\n\nThis is where we disagree:\n\n>\n>\nI'm of the opinion that the GNOME project will kill itself in the end with the rather radical approach to usability it has taken as of late. Losing one of the two leading Open Source desktops to this vague idea of \"usability\" will be bad enough, but losing both would be disastrous.\n>\n>\n\nI don't think it is reasonable to claim that an attempt improve usablity will kill a project. If more usable, more people will use it, and the more successful it will become. That seems like a platitude to me.\n\nOf course, one may _fail_ in making it usable, but that is another matter. It can only be resolved by serious discussion about how to achive it and willingness to act upon what is found to be the best course of action, and that is what is lacking in KDE. "
    author: "will"
  - subject: "Re: I see you're serious"
    date: 2004-04-28
    body: "I think you confuse \"usability\" with \"radical interpretation of usability\".  Usability can't kill a project--but what Gnome is doing of late simply isn't usability.  They come up with ideas that sound good from a usability POV, but when users question the wisdom of those ideas, they just say \"it will take some getting used to, but it'll be easier in the end, once you're used to it\".  KDE in my experience actually listens to user complaints and responds with code.  Gnome has already become so quirky and strange to use that I can't see switching to it even of the KDE project dies."
    author: "ac"
  - subject: "Re: Please will, tell me"
    date: 2004-04-29
    body: "I interpret by head start you mean a head start, within the Linux community, regarding the UI Design?\n\nOutside of Linux, the reason they call it Silicon Valley, isn't because of the silica in the area.  The US is the hub of the technical world, but not the only hub.  \n\nPersonally, Sun positioning itself with GNOME as it's Java Desktop really should have KDE thinking of perhaps positioning itself to work more with Apple and integrate support for Objective-C/Cocoa hooks.  If that happened, with of course some aide from the GNUstep folks we'd really have something there."
    author: "Marc J. Driftmeyer"
  - subject: "Re: Please will, tell me"
    date: 2004-04-29
    body: "Now there's a thought. The world is so pre-occupied with making Microsoft work with Linux, why not make Apple work more with Linux too, not just Linux with Apple. Personally, I'm liking some of the ideas behind OS X. It just seems like developers that make addons and themes for KDE and/or Gnome tend to favor the new OS X look and feel. I cannot say that the Microsoft-ish look and feel has been cloned for as long as a time as the OS X bar and many attempts to actually turn the Linux Desktop into more of a Mac-like Desktop.\n\nMaybe Marc is right? Maybe Will is right too? Does technicality have to be burdened by a heavy usability guideline or can it benefit from it?"
    author: "Rick"
  - subject: "Re: regarding Bacon's blog"
    date: 2004-04-28
    body: "Please, not again.\n\nDo we really have to have this debate every six weeks?"
    author: "teatime"
  - subject: "Re: regarding Bacon's blog"
    date: 2004-04-28
    body: "Concerning this and other comments to the effect that \"we don't want this discussion\" or that posts with this message are trolling. \n\nYou are wrong: the reason why this pops up is because it is *important*, possibly the most pressing problem KDE is facing now. It is wrong to ignore it or to \"close\" the discussion. \n"
    author: "will"
  - subject: "Re: regarding Bacon's blog"
    date: 2004-04-28
    body: "I just came to realize that I am as tired of this discussion myself. It will probably not do any good, so you are free to delete it if you wish. I assume the moderator can verify that the IP adress is the same."
    author: "will"
  - subject: "Re: regarding Bacon's blog"
    date: 2004-04-28
    body: "The reason why it pops up is not because it is important. It pops up because people are vulnerable to influence and propaganda. If other project have chosen \"usability\" flag to group people under, that is fine. But KDE project has it's own face, it's own goals and it's own direction and people must accept that as well."
    author: "adymo"
  - subject: "Re: regarding Bacon's blog"
    date: 2004-04-29
    body: "Please: Don't say usability is not important. KDE as it is might be just fine for you. Great! But that is one form of usability. The fit for that specifig group of users, you are part of.\n"
    author: "Anonymous"
  - subject: "Re: regarding Bacon's blog"
    date: 2004-04-29
    body: "I agree with you. I whoulnt say usabillity isnt important, but KDE has its own way of doing things. the thing I liked about KDE was that it allowed you to do whatever YOU wanted - much more options, features, configurations - thats what I like. well, it can be confusing, and its very good to re-arange things to make it easier to use (I often find myself looking through kconfig for a long time to change something which is in essention very easy) but at least KDE shoulnt follow Gnome in simply removing stuff... thats bad bad bad. lets do it just the KDE way, its fine ;-)"
    author: "superstoned"
  - subject: "Re: regarding Bacon's blog"
    date: 2004-04-29
    body: "So don't remove it. Just make the default better for the larger masses. As I have seen it, KDE is trying to go for the enterprise market as well as the normal Desktop area. Well, guess what needs to be done to fit nice and snug into the enterprise market? A good usability plan. If a desktop user cannot easily find their way around or for some reason gets easily confused, that slows work down. I am quite sure that employers would not enjoy that and choose not to implement something that's going to cause that.\n\nSure we hear alot of nice stuff about development companies wanting to use KDE as their DE of choice to support but you also hear more about companies choosing Gnome as their DE of choice. Why are all of the good products moving to KDE but all of the companies using Gnome? Can we answer that? Let's make those KDE using companies proud!"
    author: "Rick"
  - subject: "Re: regarding Bacon's blog"
    date: 2004-04-28
    body: "I don't like the way Gnome oversimplifies things, but they made some wise decisions. And no: I do not especially like Gnome, I do use KDE almost exlusively because I consider it superior in most areas and I do consider myself an advanced user.\n\nBut take Konqeror settings vs. FireFox (not really being a GNOME app) for example. I really like the way FireFox handles preferences. Options that are normally used (e.g. proxy settings that HAVE to be made to be able to use a browser) are readily available in the options dialog (General/Connection Settings). Less frequently used options (e.g. tab settings, certificate settings) are available for everybody in the \"advanced\" page in tree view. Really obscure options are only available via about:config. This is a great way to reduce complexity without taking out options for users who REALLY want them (and I don't think there are that many). \n\nThis way FireFox has only 7 pages in the options dialog, compared to 17 in Konqueror settings. Of those 17, 12 contain options for web browsing only. A list with 17 pages in a scroll box is simply way to long - it isn't even possible to see all entries without scrolling. \n\nHonestly: No \"normal\" user knows what a user-agent string is - so no \"normal\" user is ever going to change it. Move it to an advanced page! Especially since there is also a \"change browser identification\" entry in the tools menu - so if people ask for a way to access that online banking site that only accepts IE, we can direct them there.\n\n The same for \"crypto\" and \"style sheets\". Yes, crypto is important, but most users will never touch or understand the settings available in the crypto dialog. \"JS/Java\" could easily be made a tab in Web Behaviour (similar to web features in FireFox), Fonts and style sheets could be combined.\n\nThis is no whining about KDE. I love KDE and I actually do use some of the more obscure features in the configuration dialogs (in some applications). But I definitely wouldn't mind an additional mouse click if it would make normal tasks easier.\n\nKeep up the good work!\n\nA happy KDE user!\n"
    author: "Anon"
  - subject: "Re: regarding Bacon's blog"
    date: 2004-04-28
    body: "IMO 17 pages of options are acceptable when they are neatly organized, which  those are if you ask me. People who don't need options can just not use them. It helps alot when options have a helpful right-click \"whats this?\" help attached to them too.  Also keep in mind Konq. is a web browser, file manager, ftp client, etc., etc.\n\nFeature/option culling or combining prefs pages simply for the sake of having less pages is dangerous and should be done very carefuly. These tools should be last resorts in the quest for ease of use. Organization, intuitiveness, and consistency should do the job 99% of the time. IMHO of course :)"
    author: "Stregone"
  - subject: "Re: regarding Bacon's blog"
    date: 2004-04-28
    body: "I think sometimes pulling together pages can really help. For an average user, do you think there is a difference between Java and Flash? Yet they are in different pages... The same for fonts/style sheets. So even with Konq being everything in one (which I really like), I think some things can greatly be simplified and some options could be moved to an advanced page or something like about:config... IMO this time ;)\n\nOne more example: Does anybody still modify cache settings? Last time I did it was with Netscape 3 on Windows because it had an insanely small cache and I was on a slow line. \n\n"
    author: "Anon"
  - subject: "Re: regarding Bacon's blog"
    date: 2004-04-28
    body: "It's pretty simple.  I would not use Kate if I could not set a handful of obscure settings.  I don't use the other hundred settings, but my \"must work this way\" for my primary application are vital.  If somebody set it the way they think it should be and removed the ability to configure it, I wouldn't use the application.\n\nNow apply that reasoning to most other applications.  That's why I like clearly defined applications with well organized configurability.  That's part of why I like KDE."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: regarding Bacon's blog"
    date: 2004-04-28
    body: "Well, IMO Kate is already an application for more advanced users (Text Editor with programming features as opposed to a web browser/file manager everybody uses). But actually I think Kate's settings dialog at least to me appears less cluttered. Also with Kate's option I think it is harder to really distinguish between frequently and less frequently used options because it caters a different group of users. \n\nFor KWrite OTOH it would probably make sense to remove many of the options it offers or to move them to an \"advanced\" page. So there would be a cler distinction between Kate - Advanced Editor and KWrite as a Notepad replacement.\n\nFor me K3B stands out as an app that is really easy to configure using the Setup program/menu entry, but gives me all the flexibility when using the configure... dialog. Here a good compromise was found between reducing complexity and maintaining all options. \n\n- - - OT:  \nI know K3b was picked in a rant by a Gnome developer recently for bad usability, but I think it is great and easy to use - it works the way people expect a burner application to work (integrated file manager and such). OTOH again it is possible to configure K3B as a simple drop window (remove file manager etc.) \n\nIt took me less than five minutes to configure K3B to something that looks a lot like the mock-up featured in  http://www.thecardinal1978.com/GNOME/\n\nSee attached screenshot. Works as a perfect \"drop box\" for files to be burned to CD/DVD.\n\n\nA great proof to KDE's flexibility. "
    author: "Anon"
  - subject: "Re: regarding Bacon's blog"
    date: 2004-04-29
    body: "cool screenie.\n\nbtw, kwrite isn't a \"notepad replacement\", kedit is much closer to that. kwrite is a code editor, a title i don't know i'd bestow on notepad ;-)"
    author: "Aaron J. Seigo"
  - subject: "Re: regarding Bacon's blog"
    date: 2004-04-29
    body: "That GNOME article amused me.\n\nWhatever GNOME has is called \"usability perks\".\nWhatever others have is called \"bloats\".\nWhatever GNOME does not have is called \"minimalistic approach\".\nWhatever others do not have is called \"lack of features\"."
    author: "jk"
  - subject: "Re: regarding Bacon's blog"
    date: 2004-04-29
    body: "I usually do not spit on other F/OSS project, but this Gnome quote is something so absurd I can't tell nothing!\n\n\"Because free software environments like GNOME are founded upon cooperative development they can avoid the problems caused by corporate competition and branding. A user in Windows XP will have to navigate Windows Media Player, Real Networks Real Player and Apple Quick time in order to play media files. Their applications menu will be cluttered and the number of interfaces to learn is higher than in GNOME where a user must only find and learn Totem Movie Player. A properly configured GNOME menu can eliminate the need to learn application names and drastically clean up the desktop.\"\n\nbut, WTF is Windows going to do? the same! integrating all by default!\nThis is a only-one-true-vision approach, and it is ideologically absurd to me!! Yeah, if you have only one thing to learn it is obviously easier, but you have to put your brain in a box and say \"Ok, let others think for me\".\nUsability and lack of options are not connected, as Gnome community nearly thinks.\n\nI mean, there are better ways to get things easier and simplier, and anyway the \"total-dumb-newbie\" Gnome is trying to approach will have anyway and in any case tons of problems with this magic box called computer."
    author: "Davide Ferrari"
  - subject: "Re: regarding Bacon's blog"
    date: 2004-04-28
    body: "Your off-topic, (mis-informed) rant is severely misplaced here. People like you will eventually force this forum to be moderated, which would be a sad waste of resources that could be put back into making KDE more usable."
    author: "Max Howell"
  - subject: "Re: regarding Bacon's blog"
    date: 2004-04-28
    body: "> a firm sense of direction and vision that is completly lacking in KDE.\n\nYou know how to alienate and discourage those KDE developer who do care, eh?"
    author: "Anonymous"
  - subject: "Re: regarding Bacon's blog"
    date: 2004-04-28
    body: "That is not the intention. I'm sorry if has that effect, but although it seems like an \"insult\" or some such thing it merely intended as statement of fact. Which can be disputed and so on. If it is true in some relevant sense, it needs to be said."
    author: "will"
  - subject: "Re: regarding Bacon's blog"
    date: 2004-04-29
    body: "The \"merely intended as statement of fact\" part is the dangerous one. A certain KDE developer recently complained on his blog that misguided users are more dangerous than trolls since the latter actually know that they ignore certain informations, that's the purpose of trolling after all. Misguided users \"merely\" state \"facts\", and thus spread lies and FUD they actually believe in while not seeing the need to check their data, it's \"facts\" for them after all.\n\nBtw about your suggestion to do a usability fork of KDE, you can do it yourself already without a fork, KDE is greatly adaptable to specific environments and use cases by customizing it and locking it down."
    author: "Datschge"
  - subject: "Re: regarding Bacon's blog"
    date: 2004-04-29
    body: "I don't want to be objectionable; but to be taken seriously by anyone you must learn the difference between \"fact\" and \"opinion\".  I've found the KDE developers very open to opinion when expressed as such (as are most reasonable people).  When opinions are expressed as fact you will get in trouble.\n\n* The sky is blue\n* The sky is the wrong shade of blue\n* I think the sky is the wrong shade of blue\n\nOnly the second one is annoying.  Your comment was not \"merely a statment of fact\"; if it where there could be no disagreement.  As there is disagreement, your statement must be opinion.  As such, you are being insulting to the hard working KDE developers whether you intend to or not.\n"
    author: "Andy Parkins"
  - subject: "Re: regarding Bacon's blog"
    date: 2004-05-01
    body: "An opinion just is an expression of fact, even when what is presented as fact is disputable, so modifying the claim with \"in my opinion\" doesn't change the content of the assertion. From the perspective of conveying degree of likelyhood or the strenght of the evidential support, it perhaps pertinent to modify that statement with \"in my opinion\" when the assessment is regarded as being uncertain or having low evidential support. Unfortunately, I don't think that is the case here :-( "
    author: "will"
  - subject: "Re: regarding Bacon's blog"
    date: 2004-05-02
    body: "gg:define:fact\n\"a piece of information about circumstances that exist or events that have occurred\"\n\"a statement or assertion of verified information about something that is the case or has happened\"\n\"an event known to have happened or something known to have existed\"\n\"a concept whose truth can be proved\"\n\ngg:define:opinion\n\"a personal belief or judgment that is not founded on proof or certainty\"\n\"a belief or sentiment shared by most people; the voice of the people\"\n\"a message expressing a belief about something; the expression of a belief that is held with confidence but not substantiated by positive knowledge or proof\"\n\"a vague idea in which some confidence is placed\"\n\nSo please stop watering down the English language with your flawed interpretations of \"fact\" and \"opinion\", English is unspecific enough already."
    author: "Datschge"
  - subject: "Re: regarding Bacon's blog"
    date: 2004-05-02
    body: "hm... I believe this is perfectly consistent whith what I said. Remember that opinion or expression of belief is just a *kind* of fact-stating. If you believe something, you believe something to be the case, in other words to be a fact. If you say that \"I'm of the opinion that the income tax has increased this year\", you are asserting this to be a fact, whith some degree of certitude or personal conviction. What makes this an opinion has nothing to do with the fact-stating, but by the nature of grounds you have to assert it. Well, this is off topic anyway."
    author: "will"
  - subject: "Re: regarding Bacon's blog"
    date: 2004-05-03
    body: "You are still missing the point. Opinion or expression of belief are by far not a *kind* of fact-stating, they are transmission of information if at all. It doesn't matter how much you believe your opinion is true, as long as you can't prove it to others it is no fact. You are using the word \"fact\" as if it's some kind of bubble gum which adapts its formation to whatever one's personal perception is, but excatly this is NOT the definition of \"fact\". Unproven and unresearched \"fact stating\" equals to spreading FUD, it makes credulous people believe in false stuff without them seeing a need to check if it's actually true. Facts can be checked to be true, FUD can be checked to be false, but in both cases people need to check it. You spread FUD, not facts, and your persistence at wanting to defend this behavior makes me wonder if you are just that naive or are trying to improve yourself at trolling."
    author: "Datschge"
  - subject: "Re: regarding Bacon's blog"
    date: 2004-05-04
    body: "I agree that this discussion is somewhat pointless, but I am merely answering your posts, so I think you have to share the responsiblity here!\n\nIt perhaps also somewhat irrelevant, because for the present discussion it doesn't matter whether opinion is fact stating or not. You think that a fact is an opinion with a high degree of certitude, and I do think my claim that \"KDE lacks direction\" is an opinion with a high degree of certitude and the comparison with GNOME is meant to substantiate that claim. It is probably this that should be discussed. I agree, though, that the criterion for being a fact is whether it can be checked to be true. But that makes an opinion capable of stating facts as well, which is my point.\n\nForget that. No I am not spreading FUD, I attempting to point to a major challenge for KDE right now. It is true though, that the discussion might be conducted in a less antagonistic fashion. That goes for everyone."
    author: "will"
  - subject: "Re: regarding Bacon's blog"
    date: 2004-05-05
    body: "You are starting a discussion over a topic about which you didn't inform yourself beforehand. The whole topic about \"KDE lacking direction\" is a joke since, besides a centralized CVS and bug tracking server as well as one release coordinator, KDE never had one centralized leadership nor a project wide agenda. It's the subprojects which can have and often have directions, and as a project encompassing many smaller projects with their spcific goals KDE as a whole can't and won't have a single direction. I suggest you to read the \"Inside KDE 3.2\" article at arstechnica.com, especially the second page \"project structure\".\n\nBut your case shows that KDE is lacking at marketing, we do not (yet?) have the marketing force and media presence to convince people like you that the \"problems\" you talk about are really actually all imaginary."
    author: "Datschge"
  - subject: "Re: regarding Bacon's blog"
    date: 2004-05-05
    body: "Hey, you are still here! And so am I.... :-(\n\n>\n\" KDE as a whole can't and won't have a single direction\". \n>\n\nIf GNOME can, so can KDE. In any case, I think you are making a political statement here.\n\n>\n\"to convince people like you that the \"problems\" you talk about are really actually all imaginary\"\n>\n\n\nWell, from your statement that \"KDE can't and won't have a single direction\" I assume we agree that KDE doesn't have a single direction. I would certainly call that a problem, whether it inevitable or not. By your own admission it is not imaginary.\n\nI feel a bit bad when I read the article, which is very thorough and full the professionalism I used to admire in KDE. You didn't take me seriously because you thought I was on the edge of trolling, and now I realize that I haven't been taking you sufficiently seriously for the same reason.\n\nWhen I say that KDE lacks direction, it isn't because of the way the development process is conducted, which I am sure is fine. Linus has said he values KDE as a project highly, and I am sure it is for this exact reason. I tried to explain what I meant by the car-analogy - one may have excellent direction in making the cogs and wheels, but if that is all, car will still look is made without direction if it isn't made whith a top-down view, where the top is the users needs and sensibilities. One consequence of that is to make hard choices as to what is relevant. Try to see it from my perspective: When I see KDE I see a geeky, messy, unelegant heap of things, with an excellent foundation, and it feels like such a waste. \n\nI once were afraid that OSS process would have problems to accomodate a top-down view (and this is perhaps what you think about as can't be, due to the nature of the development process), but to my great relief, it does seem to be possible. HIG is said to gain good support in the GNOME development community and I am delighted by the fact that they can conduct a discussion of this spatial thing and other features, and then impose it on the environment. Technology enters as problem solver, a secondary consideration to the aims of the environment, as it should be. Some guy wrote a piece about that a week ago or so. I predict that OSS over time will learn this lesson from the success of the projects that follows this approach.\n"
    author: "will"
  - subject: "Re: regarding Bacon's blog"
    date: 2004-04-28
    body: "> 1. That the most influential people in the project has the right vision\n\nI don't consider trying to clone every crap M$ spits out the right vision.\nThis is a catch-up game the never ends.\nBetter try to attract developers and users by having a sweet and standards-compliant desktop/platform.\nYes, I consider kde as a platform!\n\nWhile kde gets better, faster, stabler and more useful, I watch gnome shooting\nitself by wasting time thinking about c#.\n\nBut watch out. kde has reached a level where the core-desktop is reaching\nfeature-completeness.\nc++ was a good choice, and anyway with all the existing\nbindings, u r free to program in whatever language u like.\n\nFocus has to be set to applications now.\n"
    author: "ac"
  - subject: "Re: regarding Bacon's blog"
    date: 2004-04-29
    body: "\n\nMiguel, please stop posting here.\n\n\nCheers,\n\n\nCarlos Cesar"
    author: "ccp"
  - subject: "Re: regarding Bacon's blog"
    date: 2004-04-30
    body: "Your point is taken, and I clearly understand it.\n\nHowever, I have a slightly different take on this, based on Tom Peters' ideas.\n\nYes, the developers are now the main obstacle to the project's further development.  However, I do not see the developers as the real problem.  Rather I see the problem as a corporate culture (a corporate culture was almost the downfall of IBM).  The current corporate culture appears to be that code is what is of supreme importance and that, therefore, coders rule.  This is an result of various ideas which I see as completely acceptable for a small project where development is all done by one person (and possibly some assistants).\n\nHowever, the KDE project has become too large for that corporate culture to work anymore.  We need a paradigm shift to the idea that design is, if not most important thing, at least as importand as coding.  This is not in anyway to denigrate the work of our many excellent coders (design only has practical value when it is implemented -- but this does not mean [as coders have suggested] that design does not have value), but only to say that we now also need designers if the project is to continue to go forward and eventually replace Micro$oft Windows as the most popular desktop.\n\nIf we can try to implement this paradigm shift in our corporate culture, we need to develop methodologies to distill the ideas posted on the kde-usability list to engineering ideas that are ready to implement.  \n\nIn this regard, I think that the Control Center is the project to start on.  It is here that it should be clear to everyone that the user interface design is what is the important part and that implementation of changes will not be difficult to code.  We also have a contributor that has done a lot of excellent design work.  He states that he is not a programmer, and has no idea how to implement his ideas.  According to our current corporate culture, this means that his work had no value and will not be used.  Put simply, this idea is WRONG and needs to be excised from the KDE project.\n\nWe should not develop a paradigm based on the methods of commercial software to accomplish these goals, be need new ideas of how to change to a design oriented corporate culture for OSS.  Please submit your proposals or ideas for proposals to the 'kde-quality' list.  If we can come up with positive ideas and implement them, we can quite having this contentious argument and get on with the work of making KDE the best desktop environment, first in Linux/UNIX and then in the entire software world.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: regarding Bacon's blog"
    date: 2004-04-30
    body: "> the developers are now the main obstacle to the project's further development.\n\nThat is just cringe inducing. It's like saying actors are the main obstacle to development of acting, or doctors the main obstacle to healthcare, or plants the main obstacle to breathing.\n\nIn other words: if only that was the case! Try developing KDE without developers, acting without actors, healthcare without doctors or breathing without plants. Ain't gonna work.\n\nIf the main obstacle is something you can't do without, what you gonna do?"
    author: "Roberto Alsina"
  - subject: "Re: regarding Bacon's blog"
    date: 2004-05-01
    body: "Well, its just a manner of speaking, I think. The real point is found in is remark \"However, I do not see the developers as the real problem. Rather I see the problem as a corporate culture\", and that is the point that needs to be rebutted, if that is your intent."
    author: "will"
  - subject: "Clueless"
    date: 2004-04-28
    body: "He talks about how is father would react to KDE without actually showing him the KDE desktop and letting him use Konqueror, KMail etc. What about KDE Kiosk mode, if complexity is a problem?\n\nHow does the Qt/KDE api compare with the Gnome equivalent? What really matters, GUI sugar frosting or architecture? \n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: Clueless"
    date: 2004-04-28
    body: "A simple problem with that statement is that things are not black and white. GUI sugar frosting is used to make people go \"Uuh, fancy, I could see that being cool to use\", and architecture is to make developers go \"Uuh, fancy, I could see that being cool to use\". See my point there?\n\nAlso, my reply to a reply on the blog should give away some of my considerations as to the whole \"things are too difficult\" thing. Thanks :)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: Clueless"
    date: 2004-04-28
    body: "\"A simple problem with that statement is that things are not black and white. GUI sugar frosting is used to make people go \"Uuh, fancy, I could see that being cool to use\", and architecture is to make developers go \"Uuh, fancy, I could see that being cool to use\". See my point there?\"\n\nNo not really, you're wrong. The point of 'architecture' as in the narrow sense of QStyles is to allow applications written to the Qt api to adapt to as many different devices/usage styles as possible. Qt applications can be used in mobile phones/PDAs via Qtopia or they can scale to the desktop. The architecture allows the GUI to scale.\n "
    author: "Richard Dale"
  - subject: "Re: Clueless"
    date: 2004-05-01
    body: ">\nHow does the Qt/KDE api compare with the Gnome equivalent? What really matters, GUI sugar frosting or architecture? \n>\n\nImagine saying this in about Middle Eastern peace talks: What really matters, speaking arabic or being good at solving diplomatic problems? The answer to both of these questions is the same.  \"Being good at solving diplomating problems\", and \"GUI sugar frosting\", respectively, are the most important parts, since they are related to the purpose of the activity (making peace/let the user do what he wants). The other part (speaking arabic/having a set of API's) is a mere condition and not that interesting in the big scheme of things. Of course, having said that, it also follows that calling GUI for \"sugar frosting\" is inappropriate.\n\n"
    author: "will"
  - subject: "Qt 3.3.2 or 3.2.2?"
    date: 2004-04-28
    body: "Something is wrong in the article! the title states Qt 3.3.2 while the article body Qt 3.2.2 ;-)"
    author: "golan"
  - subject: "Re: Qt 3.3.2 or 3.2.2?"
    date: 2004-04-28
    body: "Thanks ... I changed it. \n\n:)\n\n\nFab"
    author: "Fabrice Mous"
  - subject: "Usability"
    date: 2004-04-28
    body: "Hi all,\n\nI am pleased that some discussion is occuring from this blog entry. As far as I am concerned the issue is very real and it should be discussed. The mere fact that it keeps getting discussed means there is an issue. Although some may feel my blog entry was a poke at KDE, it was intended this way - it is intended to identify the problem and try to resolve it if the community so decides. I don't subscribe to this KDE vs GNOME argument, I believe in free software and I would love to see both projects succeed.\n\nFrom the responses I have seen so far, I have heard from a lot of people defending the current method of making everything configurable. Yes, it is cool in the fact that you can find things when you need to change them, but this configurability is (in my opinion) losing chunks of users. The problem is that people, such as my dad, who are not computer savvy will have problems with KDE. Richard Dale mentioned the fact that I have not actually put him in front of KDE, well no, I havent, but I have put countless others in front of KDE. this includes members of my family, my girlfriend, collegues and others. The unifying factor among these test candidates was a relative confusion when wanting to change certain things in the desktop. Things such as the background wallpaper were not an issue, but other issues were confusing. I remember when my workmate had a go with KDE he was a bit overcome with the amount of categories in the KDE Control Center. This is a fairly IT savvy person and he was a little confused; hence my example of my dad who would be totally lost.\n\nThe point of this blog entry was to not rib KDE but to see if there is an alternative method that pleases the dads of the world and the readers of this site. KDE is a very simply to use desktop; there is no doubt about that, but some options do seem rather advanced for a desktop that aims for a novice userbase. The key seems to be choosing a usability scheme where by someone can access their options easily and efficiently but not be confused. This is why the different userlevel idea initially sprung to mind. Other options such as Advanced buttons for further pages of options, GConf keys, and other options could be discussed.\n\nI do feel that this issue is of paramount importance. It may not be important to those who love KDE for what it is now and are happy with it, but if KDE is to access a broader novice userbase (and they are getting more and more with the increased popularity of Linux), the current system needs to be better established.\n\nI think that as new features are added to KDE and options are added to the GUI, the desktop can only take a certain amount of populated options before the new scheme needs to be considered. If Konq has 17 pages of options, what would be the considered maximum limit? In KDE 6.2 are we going to have 37 pages of options?\n\nIf we can discuss this and come up with a suitable plan early on, these should help further problems down the line."
    author: "Jono"
  - subject: "Re: Usability"
    date: 2004-04-28
    body: "\"The problem is that people, such as my dad, who are not computer savvy will have problems with KDE. Richard Dale mentioned the fact that I have not actually put him in front of KDE, well no, I havent\"\n\nSorry about my harsh comments, but I really feel this is a setup/distribution packaging problem rather than a Qt/KDE usability issue per se.\n\nBut the burning question is \"can some KDE expert set up a suitable configuration for the Jono Bacon's father audience?\". Or is there something wrong in that we can't hide the complexity of KDE even though some people might need all of it? Is there something wrong with Kiosk mode for the minimalist users?\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: Usability"
    date: 2004-04-28
    body: "> \"can some KDE expert set up a suitable configuration for the Jono Bacon's father audience?\"\n\nYes, but shouldn't dads ideally be able to setup their environment without being computer experts? Think stupid user with notebook on the road: Constnt need to change network settings, proxy etc.\n\n>Or is there something wrong in that we can't hide the complexity of KDE even though some people might need all of it?\n\nI do think so. \n\n>Is there something wrong with Kiosk mode for the minimalist users\n\nKiosk mode is great, but it is more suited for Jono to setup for his dad thaan for his dad to do it by himself as it might have even more options than the normal configuration. Remember: This is not about making it impossible to change settings for some users, this debate is about how to make it EASIER for them to adapt their environment to their needs.\n\nKiosk is more of a security framework."
    author: "Anon"
  - subject: "Re: Usability"
    date: 2004-04-29
    body: "KDE is easy to adapt to specific needs, in contrary to other environments which either don't document specific features or lack them completely. And if KDE can be adapted to specific use cases why should Jono's dad not be able to find a service provider who offers KDE perfectly customized to his needs?\n\nKiosk being a security framework and thus ignoring it is a poor excuse."
    author: "Datschge"
  - subject: "Re: Usability"
    date: 2004-04-28
    body: "\"Sorry about my harsh comments, but I really feel this is a setup/distribution packaging problem rather than a Qt/KDE usability issue per se.\"\n\nDon't apologise, it is good to chew the fat and discuss this. :)\n\nI don't think it is a packaging issue. The reason I say this is that certain options cannot be easily taken out of KDE by the distributor. Yes, they can remove certain KControl modules if needed, but I don't see how this should be at the hands of the distributor. I see how they should not include certains software, and I also see how the distributor should ensure that X is working, but in terms of how KDE is configured, that is the responsibility of KDE.\n\nI honestly believe that KDE can be made to work for the kind of person my dad his. He is not an unintelligent man, but he is a person who gets confused when too much is put in front of him on his computer. I think the key point is that a hacker who knows the ins and outs of KDE can make it do what they want; people who are newbies such as my dad and Eruic Raymonds Aunt Tillie are basically stuck with the choices decided by the KDE team and the distributor. If those choices are unsuitable, a bad usability case occurs.\n\nThe most difficult step is acknowledging that there is an issue. If people accept that this configuration mess will only get worse as more configuraton options are added, we can discuss alternatives.\n\n  Jono"
    author: "Jono"
  - subject: "Re: Usability"
    date: 2004-04-28
    body: "I'm sorry, you need to put a minimilist configuration in front of your father, and then comment. You can't base a usability study on hyphothetically thinking about someone \"who gets confused\", without trying it out in the real world. What about scientific method and peer review?\n\nAnd Eric Raymond knows next to nothing about computer usability.\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: Usability"
    date: 2004-04-28
    body: "@Richard:\n\n### And Eric Raymond knows next to nothing about computer usability.###\n\nYou dare! Please refrain from such useless polemic. Eric wrote one of the greatest GUIs ever. It is in his famous \"fetchmailconf\" program. That is a classic. My aunt Tillie uses it every day, and she really really loves it. (Dunno about Eric's aunt....). See here for an example:\n\n --> http://www.linux-mag.com/2003-08/img2/power_01.jpg <--\n\n"
    author: "fetchmailconf-fan"
  - subject: "Re: Usability"
    date: 2004-04-29
    body: "Ok, now, that was mean. The UI, I mean, not your comment!"
    author: "Roberto Alsina"
  - subject: "Re: Usability"
    date: 2004-04-29
    body: "But nothing beats IC Station from Mentor Gparphics.\nHave a look at:\nhttp://www.ece.unh.edu/courses/ece715/assign/LAB4.html\n\nAs I remember it, the menue on the right hand side depends on the menu at the bottom. True horror. \n\nAnother smart thing is that when you want to print your circuit, you can specify how many layers you want to print, ranging from 1 to 99. If you choose 99, the print job will not be finished before the next morning. Of course 99 is the default. The true horror is that the widget shows only one 9. You have to click on the widget to see that it is actually 99.\n\nMark my words, never ever let an electronic engineer construct a GUI.\n"
    author: "."
  - subject: "Re: Usability"
    date: 2004-04-28
    body: "This is hardly an authorative statement, but I believe that use of minimalist design is well supported by studies, and is generally an accepted empirical fact. I don't know these studies (I don't work in usablity) but it is mentioned as a principle in Sun's Gnome usability study (with some relevant responses from the test subjects as well) and commented on by Joel's.\n\nIt makes sense that it is - if the options gets too plentyful, the user must with necessity use more time to read them and process information. The point of minimalism is not really minimalism for its own sake, but actually a derived feature of attempting to make user interaction efficient.  The most efficient interaction is to offer the most relevant choices to the users so that he can do what he wants with the least amount of effort..."
    author: "will"
  - subject: "Re: Usability"
    date: 2004-04-29
    body: "To make user interaction efficient you need to know how the user will interact. When you do that the wrong way by considering only one kind of users you won't make user interaction efficient but far more inefficient for other users, for them not being able to find the way he expected, regardless of how illogical you might find his way of thought.\n\nMinimalist design is well supported by studies in specific test cases with specific users. KDE has a broad range of uses and a broad range of different users. KDE can be adapted to every case, but you will fail crashing and burning if you seriously think you can adapt a minimalist design to the default KDE without decreasing the range of uses and thus alienating users."
    author: "Datschge"
  - subject: "Re: Usability"
    date: 2004-04-29
    body: "that does boil it down to a rather linear line of thought, and like most simplistic generalizations it fails to capture the whole picture.\n\nyes, simplicity can be a very good thing.\nso is consistency.\nand utility.\nand learnability.\nand use-case based ordering of elements.\nand ...\n\nthere's a lot more to \"usable\" than \"simple\". for instance, Raskin now regrets going minimalist with the single button mouse on the orginal Mac. he wishes he'd used (at least) a two button mouse and just labelled the two buttons: Select and Activate (or something along those lines... working from memory here ;)\n\nso as you can see, minimalism is NOT the end-all, be-all solution to things. it's a tool like any other: good when used appropriately, and detrimental when missapplied.\n\ndiscussions about software minimalism are often framed in absolutes, which is unfortunate, because that fails to do either minimalism or usability a service.\n\nyou don't nail screws in with a hammer, and similarly we shouldn't minimalize the desktop into restrictive discomfort and lessened utility."
    author: "Aaron J. Seigo"
  - subject: "Re: Usability"
    date: 2004-04-29
    body: "Aunt Tilly walks into the reception area of a legal firm. She opens her iBook and it wakes up from sleep mode straight away. It finds the WiFi network and uses rendevous to find the printer in the reception, and automatically adds it to the printer menu. She prints out the notes for the meeting without needing to even think about the technical issues involved. There were no so called 'friendly wizards' involved in doing this.\n\nOr she plugs her digital camera into her computer, it does something useful such as start a photo album management program. Plug and play - that's useability.\n\nBoth the above examples are absolutely nothing to do with 'software minimalism' and are much more to do with sound software architecture and infrastructure."
    author: "Richard Dale"
  - subject: "Re: Usability"
    date: 2004-04-29
    body: "I'd personally be scared about the implied lack of privacy necessary for your first example to work without any additional need for authentication or fine grained configuration of what should be accessibly for who in what cases, both which will make your 'usable' vision not satisfying in one or another regard, but whatever.\n\n"
    author: "Datschge"
  - subject: "Re: Usability"
    date: 2004-04-29
    body: "Well, if Aunt Tilley had visited before maybe she has the WiFi key on her keychain (like KWallet), and the network uses WEP. Someone would have to tell her the key, so she could add it with the keychain dialog.\n\nOr perhaps they have an 'open access' WiFi area just for reception area visitors where you can print or browse the internet  or send emails.  However, they wouldn't be able to access the full corporate network.\n\nThis isn't a vision - it's something you can do today. If a printer can send it's configuration via rendevous, you obviously don't need a printer dialog. If every device can do that you can get rid of a lot of dialogs.\n\nI'm really glad that the KDE project doesn't have Sun working on their UI guidlines. They have absolutely no aesthetic/design sense for UIs at all. Swing? Open Look? "
    author: "Richard Dale"
  - subject: "Re: Usability"
    date: 2004-04-29
    body: "To Aaron and Richard: minimalism is only a rule of thumb, and only works if done right. Nor is not the only part of usablity. But it is one part, and its a good rule of thumb, which is more than sufficient to take it seriously. "
    author: "will"
  - subject: "Re: Usability"
    date: 2004-04-28
    body: "> From the responses I have seen so far, I have heard from a lot of people\n> defending the current method of making everything configurable. Yes, it is\n> cool in the fact that you can find things when you need to change them, but\n> this configurability is (in my opinion) losing chunks of users.\n\nWell, *I* don't think it's losing us chunks of users.  The kind of user who would be confused by an abundance of configuration options is the kind of user who isn't interested in configuring anything and would do so very rarely.  Anyway, my criticism is that you are advocating what seems to be a quite drastic change in KDE policy because of your impression of what a certain kind of user might think in an imaginary case.  It seems to me you are succumbing to GNOME's propaganda of \"configuration options are eeeeevil\", specially when you link it with the comment that \"the sense of direction in KDE seems to be flagging somewhat\"."
    author: "em"
  - subject: "Re: Usability"
    date: 2004-04-28
    body: "\"It seems to me you are succumbing to GNOME's propaganda of \"configuration options are eeeeevil\", specially when you link it with the comment that \"the sense of direction in KDE seems to be flagging somewhat\".\"\n\nI have not succumbed to any kind of propoganda, less propoganda that I never been in contact with.\n\nThis has nothing to do with KDE/GNOME comparisons. GNOME do good stuff and KDE do good stuff. The point I was making is that KDE seems to have remained fairly consistent in its development and there is a lot in there that I feel is clutter. I am simply concerned that this clutter is going to be overwhelming at somepoint.\n\nThere is plenty wrong with GNOME, and I accept that. There is plenty wrong with lots of things in thge free software world, but there is plenty things that are right. I was merely pushing out my view in the hope that it causes some fresh discussion.\n\nAs far as I am concerned, if KDE choose that my suggestions are not in the interests of its users, then that is the decision and I am happy. What I would not be happy with though is if I shut up and did not make suggestions and expressed concerns with software that I care about. I care about KDE and want to see it become the dominant desktop platform for free software; I am just concerned about the amount of configuration clutter that is going to reach critical mass in a few versions if something is not done soon.\n\n  Jono"
    author: "Jono"
  - subject: "Re: Usability"
    date: 2004-04-29
    body: "Usability depends on the use cases and the kind of user. Whatever KDE installation you would have used for your dad you should have ensured that it's adapted to his needs, not to those of yourself or someone else. If your dad tells you to buy a car for him you won't buy him a truck after all. Now why do you expect that this adaption to your dad's use case needs to be done in KDE as a whole? KDE doesn't know who your dad is, what he would do with a computer, and why everyone should use a KDE setup adapted to your dad's use case. KDE however did and does collect and realize requests from many real users, and the KDE you see today is a result of that, highly adaptable, but you may not dare to complain that it's not adapted to a specific use case if you didn't (let) do that adaption before."
    author: "Datschge"
  - subject: "Oops"
    date: 2004-04-28
    body: "Oops, bad typo:\n\nThis:\nAlthough some may feel my blog entry was a poke at KDE, it was intended this way - it is intended to identify the problem and try to resolve it if the community so decides.\n\nShould be:\nAlthough some may feel my blog entry was a poke at KDE, it was not intended this way - it is intended to identify the problem and try to resolve it if the community so decides.\n\nIt was NOT intended as a poke at KDE.\n\nWhat a cock up!!\n\n  Jono"
    author: "Jono"
  - subject: "Re: Oops"
    date: 2004-04-28
    body: "The next time this diskussion pops up, I won't read it.\nI'll just close my eyes and scroll. Ha, take that!\n\nAs mosfet and others have said in the past; users don't want less options,\nusers want more options arranged in an orderly manner.\nI am a user and thats what I want. \nEOD.\n"
    author: "reihal"
  - subject: "Re: Oops"
    date: 2004-04-29
    body: "The article was unnecessary.\nKDe is very good in usability while Gnome still lacks features. In europe desktop Linux means KDE. Teh control center was bad, but not anymore. What the Author suggested will be implemented, it's already planned to provide a Kontrolcenter light. \n\n\nWhen you want an \"easy\" desktop I would rather suggest to build a web based desktop system (who need windows and menus?), a kind of browser based operating system that is 100% configured via web pages. For special programs like terminals and editors and word processors tabs are opened and the rest looks like tabbed browsing. No WIMP Windows moving, one window-one document. And you can get every single option via its URL. \n"
    author: "gerd"
  - subject: "Usability......"
    date: 2004-04-28
    body: "To get a desktop out into the world that can be used by many people is very difficult - impossible, quite frankly.\n\nTry putting Gnome in front of a sys admin, a developer or a person who has used a computer within a business environment over many years. These people really do matter, but you don't necessarily hear about them. They will throw the damn thing straight out of the nearest window (no pun intended). Where I work, that's a long drop.\n\nA desktop that Dad or Gran can use instantly is currently a complete myth. Sorry, but Jono Bacon has been at the acid on this one. He mentioned that his Dad never touched the Control Panel - and yet it needs to be there. Why? Windows, Mac OS, KDE, Gnome - none of them have managed this (and it isn't really the aim of either Windows or Mac OS), and they will never manage it until we get some unbelievable technology that can communicate with a user on a telepathic level. \"Where we're going we don't need - desktops.\" This is why the Gnome direction is totally and utterly flawed. This is why people criticize Windows usability when compared with Mac OS. If Mac OS got used as widely within networks, by sys admins and developers as much as Windows did, it would look much, much different.\n\n\"I feel the best option in a situation like this is to make the desktop and configuration available in a series of schemas. You could have an Easy schema that has the most common options available and removes choices that may be too advanced or are not needed by someone such as my dad. You can then have a Medium schema that satisfies those with a tweaking finger.\"\n\nIn theory yes, but the best you can hope for here is an \"Advanced...\" tab or button, or to reorganize things. Having levels of easy, intermediate... etc. has actually been discussed on the KDE usability list - and dismissed for very good reasons. One is that in the search for simplicity (not usability) you make things much more complicated :).\n\n\"As an example, if you don't use Konqueror, you should be able to remove its options from your schema.\"\n\nHow is using a schema (an extra layer of complexity) making things more usable? It is also a nightmare when administering users as you then have to guess what schema they are in. Sorry - a non-starter.\n\nAs someone who currently has to design and maintain applications within a very sizeable banking environment I can tell you that Gnome's philosophy is total bollocks. These people have never designed a well-used application in their lives. \"Look guys, it says this in the text book. It's so simple.\" Er, no - I don't think even an IT undergraduate is that naive. We have flame-wars about usability here all the time, but none of us believes that you can design a desktop for the absolute lowest common denominator - and get lower. Simplification is not usability, and Gnome seems to have got the two woefully mixed up. Remember that many, many people out there will need to actually get things done with their desktop.\n\n\"GNOME has had two important advantages: 1. That the most influential people in the project has the right vision and 2. The support of big corporations, like Sun, who knows this because live off selling their stuff.\"\n\nYer, whatever.\n\n1. Gnome 'usability' vision is totally flawed, because they have lost sight of what usability actually is. What are the functional requirements of Gnome and the applications? You have to pay attention to this - you can't just decree that something will be 'usable'.\n2. I read an article the other day about how Gnome was supported by Sun, HP, Novell and IBM. That was in the year 2000, and the perceived corporate support has got them absolutely nowhere. I'm still waiting. Sun do not live off selling this stuff, and it is quite clear to anyone that Gnome's usability drive did not emerge from common sense.\n\n\"One possiblity is to make a style/usablity fork which branches off as stable release and has the opportunity to remove, rename, redesign under a unified vision without compromise.\"\n\n...\"vich vill be obeyed visout quvestion\" (Fawlty Towers). KDE is not a dictatorship, thank goodness. Gnome is an open source project, isn't it?\n\nEnough."
    author: "David"
  - subject: "Re: Usability......"
    date: 2004-04-29
    body: "You write that \"the usability vision of Gnome is totally flawed, because they have lost sight of what usability actually is\".\n\nI wonder whether you read the studies and documentation written by Sun and published by the Gnome Usability project (http://developer.gnome.org/projects/gup/). In the published study they clearly state which tasks users had to complete and what went wrong when users where performing these tasks with the Gnome tools.\n1. To have a fruitful discussion please point out what is wrong with this study. \n\nBesides that, the page lists several documents that show why it is important to have good usability.\n2. Did you read the Introduction of http://developer.gnome.org/projects/gup/hig/? 3. Do you want to contest any of this?\n\nYour post does not seem informed to me, please prove me wrong.\n\nI can only concur with the idea that some advanced uses and less used options go to an 'advanced' page. Although I use KDE most of my Linux time, because it still provides me with more functionality (not necessarily options), the look and easy of use of Gnome really please me."
    author: "Patrick"
  - subject: "Re: Usability......"
    date: 2004-04-29
    body: "> still provides me with more functionality (not necessarily options), the look and easy of use of Gnome really please me.\n\nMaybe that is the point after all. One can have either, but not both.\n\nThink about this: Apple gained a large group of users when they added a Unix shell.\n\nDerek (studies are another word for 'believe me even if I don't make sense')"
    author: "Derek Kite"
  - subject: "Re: Usability......"
    date: 2004-04-29
    body: "\"Maybe that is the point after all. One can have either, but not both.\"\n\nI don't believe that. I think you can have a pleasing and productive environment  and still have lots of options. Just look at Windows XP and work with it for a week (I don't know Mac OS X that well).\n\nAs far as Windows XP is concerned I believe Microsoft did an amazing job. Although I feel it could have been better and easier (1. configuration options for the OS are scattered through menu's and panels and available through many different menu's; 2. cryptic commands are needed to access often used information etc) they have a productive GUI that most users easily grasp and know how to adjust to their own preference.\n\nFurthermore they have been able to make installing third party programs so easy that almost anybody can learn how to do it. Just try to explain to a new user how to install a download tgz and you know what I mean.\n\nThe latter is not something that is easy fixable by the KDE project. What *is* in our hands is to make the environment and it's native programs friendly enough that users can make them selves comfortable with them and feel they are in control.\n\nThis will make it possible for distributors to integrate the underlying OS with KDE (or Gnome) and have the same complete user experience that Mac OS X and Windows XP offer.\n\nI therefore agree with Jono."
    author: "Patrick"
  - subject: "Re: Usability......"
    date: 2004-04-29
    body: "There's actually not too much wrong with Gnome's HIG. It is the way in which it has seemingly been implemented as some sort of holy grail. No where in the Gnome HIG does it say \"We must simplify everything so we can get our Grans to use Gnome.\" Within Gnome, usability has become something altogether different.\n\n\"http://developer.gnome.org/projects/gup/hig/....http://developer.gnome.org/projects/gup/\"\n\nYes it is all positive stuff, but the real world tends to be quite a bit different from studies.\n\n\"What a disappointment it should be when a user's ability to access one of the features we have coded is impaired or altogether halted because they don't understand how to manipulate the interface...\"\n\nThat is a great statement, but it says nothing about trying to simplify something that cannot be simplified or slashing features.\n\nThe short answer is no - I don't want to contest any of this. I contest the way it has been implemented.\n\n\"Your post does not seem informed to me, please prove me wrong.\"\n\nErr, real world experience?\n\n\"Although I use KDE most of my Linux time, because it still provides me with more functionality (not necessarily options), the look and easy of use of Gnome really please me.\"\n\nThat's great, but if Gnome or KDE are going to make it in the world they will need to be used for a multitude of tasks and be able to do them pretty well. That is an altogether different proposition than someone coming along and saying \"Oh, it looks clean and polished and easy to use...\" or \"My Gran can use this...\""
    author: "David"
  - subject: "Re: Usability......"
    date: 2004-04-29
    body: "\"That's great, but if Gnome or KDE are going to make it in the world they will need to be used for a multitude of tasks and be able to do them pretty well.\"\n\nI believe this is not a point of discussion but more a prerequisite for each of them to succeed . But my point is that just adding features and not making them easy to use will still not get you there. 'Simplification' and 'feature completeness' need to go hand in hand in some way.\n\nI'm not saying that Jono's proposition of user levels is the way to go, because I really am not that much into user interface design. I do feel however that he has a valid point by saying that the user interface needs to be simplified.\n\nTo my opinion following the Gnome HIG is better than something KDE has now. Why not make it a vote on the Dot or start a more broad discussion about this on a mailing list?"
    author: "Patrick"
  - subject: "And Now Onto Something Useful....."
    date: 2004-04-28
    body: "What kind of new CMS site are the new amaroK people contemplating? Does the offer they have for fast, free web space dictate a particular technology? My preference on this would be a Zope based site, simply because Zope make this kind of thing incredibly easy and the security isn't bad either. Yes I know what people say about Zope and Plone, but they are good at what they do."
    author: "David"
  - subject: "Re: And Now Onto Something Useful....."
    date: 2004-04-28
    body: "We're not set on any particular CMS. Most important would be a way to get our content in shape easily and reliably. Then we'd like to have:\n\n1) Forum (a usable one)\n2) Gallery (screenshots, developer's ugly mugshots and the like)\n3) News\n\nAlso nice would be an additional wiki part? Dunno if that mixes well with a CMS, but I like the idea. You know, making the site a little dynamic, allowing users to participate. But what do I know, I'm not much of a webmaster :)\n \n\n--\nMark.\n"
    author: "Mark Kretschmann"
  - subject: "Re: And Now Onto Something Useful....."
    date: 2004-04-28
    body: "I love Plone, and think that they are the 'Jimi Hendrix of CMS' - incredible features waaaaay ahead of the competition.\n\nThe Zope environment was too heavy for my tastes however, and I switched to something much more lightweight, but still dead simple to use:  Mambo (http://www.mamboserver.com).  A healthy third-party community creating custom add-ons was also a big seller for me - templates, shopping carts, whatever I needed was either built-in or a download away.\n\nI looked at others, but picked Mambo because my wife can use it with maybe 3 minutes of instruction.  The only other CMS that I have worked with that was this easy was Plone.\n\nOh, I liked Mambo enough to get into the core development team, so I am now horribly biased.  :)  I would happily volunteer the time to setup a Mambo site for them and show them what Mambo can do for them.\n\nBTW an awesome site for checking out PHP/MySQL-based CMS is:\n\nhttp://www.opensourcecms.com\n\nHTH,\n\n-- Mitch"
    author: "mitchy"
  - subject: "Re: And Now Onto Something Useful....."
    date: 2004-04-29
    body: "That's a great offer :) Mambo looks very powerful indeed.\n\nCould you maybe subscribe to our mailing list and send a short note? I reckon it makes communication easier. You can find it here:\n\nhttp://lists.sourceforge.net/lists/listinfo/amarok-devel\n\n\n--\nMark.\n"
    author: "Mark Kretschmann"
  - subject: "Re: And Now Onto Something Useful....."
    date: 2004-04-29
    body: "Yer, I know what people say about Zope - and they're right. Mambo does look very good, and something lightweight so that could be a great option. It sort of depends on how much load your system will be taking. If it isn't great Zope/Plone is a good option.\n\nI hope something great can be done here."
    author: "David"
  - subject: "Re: And Now Onto Something Useful....."
    date: 2004-04-29
    body: "Let me plug www.tikiwiki.org . It's a great system, with lots of features and very easy to alter/extend. Amongst other modules it features an image gallery and a very nice Wiki."
    author: "CAPSLOCK2000"
  - subject: "interesting read."
    date: 2004-04-29
    body: "Jono: good to see your face around again... here's to seeing you on IRC again some day soon =) some thoughts on your blog, ranging the spectrum of randomosity:\n\n- user levels don't work. they've been tried and they've failed. many times over. the reasons for this failure are rather well understood. google for them if you care to.\n\n- KConfigXT is proving to be a path forward away from having to either put every option in the GUI or else have it hidden.\n\n- we already have something resembling schemas in Konq: user profiles. they have now extended to include the XMLUI settings which allows for much greater definition of specific profiles, some of which will be in 3.3. i wouldn't be surprised if it eventually was extended also include the config dialog such that there was a \"File Manager\" version and a \"Web Browsing\" version (version as in list of panels) shown depending on the type of profile in use.\n\n- you say that we won't get anywhere if we are only self-congratulatory, and i agree strongly. i don't think such attitudes are the \"mode de e'mploy\" in many developer circles where the work actually goes on, though. fan treatises, enthusiasm, straigh-up marketing, etc may skip over much of this hard-biting self-examination but it exists in all the successful projects i've seen, to one degree or another\n\n- you say that constructive criticism is one of the biggest gifts you can give to a project. if only that were true we'd have all the problems solved. the world is awash with constructive criticism; it has become like water to a drowning man who needs not more of the stuff but a way to deal with it. right now we have a veritable Pacific Ocean of constructive criticism. i'd be overjoyed if there was even a Mediterranean of time and effort that accompanied it. ;-)\n\n- i don't think that the direction in KDE has been flagging, unless \"flagging\" means something different in the UK than it does here. ;-) it's been evolving and growing, if anything. you are right that becoming feature rich creates new challenges, but i'm not sure i see that as a challenge related to KDE's direction.\n\n- large numbers of different people will report on the same problem(s) in a relatively short time span (usually with remarkably few useful/well researched solutions proferred) each with same raging enthusiasm for the topic as the next. and yet when a satisfying conclusion is eventually arrived at, very few make any bones about the success:  it's right on to the next Overwhelmingly Alarming Issue. silence when the war goes well, i suppose. at least this process keeps the trolls stocked with soundbites ;-)\n\n- another observation one might make is that it's often damned if you do, damned if you don't. when there aren't features, it isn't ready for prime time, but when features abound there are too many of them. ;-) when you have lots of exposed options, you're unusable; when you don't expose them, you're inflexible. there's something to be said for ballance, and that ballance is a tricky thing to achieve. people on both sides of those fences often urge their \"team\" to head towards the other side of the fence post haste: most \"what KDE should do\" articles i read suggest aping some other project; indeed most \"what GNOME should do\" articles do the same. and this comes from people who purport not to like the projects that they may as well be describing in their prescriptive diatribes. greener grass? heh...\n\n- it may be easy to dismiss such articles as being self-serving mouth wagging. but i do believe most people who offer their opinions on these things are well meaning and trying to encourage and help in their own way and as their time and energy permits.\n\n- and through it all the developers largely remain positive and glean the useful points out of these things so as to improve the beautiful children of their minds.\n\n- and through it all the cheerleaders strive to maintain the visibility of the positive things while attempting to discover \"creative solutions\" to the perceived problems.\n\n- and through it all the users keep using the software. and writing about it. and even from time to time giving something back.\n\nholy crap is this \"comment\" long. at least i found something to do with the last *looks at clock* 30 minutes @work ;-)"
    author: "Aaron J. Seigo"
  - subject: "Icaza .. again"
    date: 2004-04-29
    body: "http://news.netcraft.com/archives/2004/04/28/interview_with_miguel_de_icaza_cofounder_of_gnome_ximian_and_mono.html\n\nCUT from the interview:\n\nQ. Has Novell decided what form the desktop will take - will it be Gnome-based, or have other elements?\n\nA. Gnome and KDE are basically the shells, but then there are higher-level applications like the office suite. We're making the decision it's going to be OpenOffice, the browser it's going to be Mozilla, the email client it's going to be Evolution, the IM client it's going to be Gaim. So we basically have to pick successful open source projects and put them together. There's a lot of work on integrating.\n"
    author: "huu"
  - subject: "Re: Icaza .. again"
    date: 2004-04-29
    body: "Forgot to add a comment. Why does Novell let these idiots speak out in public?"
    author: "huu"
  - subject: "Re: Icaza .. again"
    date: 2004-04-29
    body: "Well it is amusing that he says nothing about Gnome and shifts the emphasis away from it. Until I hear a statement from Suse, or look at what is actually in their software (they lean very much towards KDE, use Konqueror by default, they do not have Evolution installed as the default mail client and the IM client in Suse 9.1 is definitely Kopete), I don't believe anything Miguel de Icaza or Nat Friedman says.\n\nAt the moment it has become clear that Migua de Icaza and Nat Friedman are doing some political shuffling because of what is happening at Novell/Suse. The soundbites are flowing out to convince us that things are heading in a certain direction, as they have so often done.\n\nI don't have any time for what they say quite frankly."
    author: "David"
  - subject: "What's happened to the kdevelop.org website?"
    date: 2004-04-29
    body: "Anybody know what's happened to the kdevelop.org website?  I haven't been able to connect to it for three days now."
    author: "Andrew"
  - subject: "Re: What's happened to the kdevelop.org website?"
    date: 2004-04-29
    body: "Check out \n\nhttp://lists.kde.org/?l=kdevelop-devel&m=108309779404401&w=2\n\nFab"
    author: "Fabrice Mous"
  - subject: "Re: What's happened to the kdevelop.org website?"
    date: 2004-05-04
    body: "It's available again."
    author: "Anonymous"
---
Some time ago on kde-www there was a <A href="http://lists.kde.org/?l=kde-www&m=107970958913194&w=2">request
for volunteers</a> to help with the <A href="http://amarok.sourceforge.net/">amaroK website</A>. 
The amaroK team is considering to switch to a CMS based site so that they 
get more organized with their content. 

When talking of websites, it looks
like the website about those <A href="http://www.kde.nl/people/">people behind KDE</A> is also 
offering a <A href="http://www.kde.nl/people/news.html">newspage</A> these days. 

Are you a Qt programmer and looking for job? Well, it seems that 
<A href="http://skype.com/home.html">Skype</A>, wants to build their P2P telephony program for
the Linux desktop when you look at their <A href="http://skype.com/jobs.htmll#tallinn">job openings</A>.

We have some more Qt news: <A href="http://www.trolltech.com/">Trolltech</A> 
<A href="http://www.trolltech.com/newsroom/announcements/00000163.html">announced</A> a new version of its
C++ multiplatform toolkit Qt 3.3.2 which is a <A href="http://www.trolltech.com/developer/changes/changes-3.3.2.html">bugfix release</A>.
 

No programmer yourself but you want specific improvements in KDE? 
I came across <A href="http://www.kontact.org/shopping/">this nice idea</A>. Here you can demand 
improvements specifically for KMail with an amount of money you would like to pledge for it. It seems like there
are some recent pledges as well.

<A href="http://people.kde.org/jono.html">
Jono Bacon</A>, a writer, consultant, web developer and musician, has written an entry in 
<A href="http://www.oreillynet.com/pub/wlg/4750">his blog</A> that discusses the direction of software projects, with a 
particular reference to KDE. He is interested to hear any thoughts on these suggestions.






<!--break-->
