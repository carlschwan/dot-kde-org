---
title: "Kexi 0.1 Beta 3 Released"
date:    2004-05-02
authors:
  - "fmous"
slug:    kexi-01-beta-3-released
comments:
  - subject: "Nice codename...."
    date: 2004-05-02
    body: "It is great that the development team of Kexi matches the new geography of the European Union. Kexi will certainly reach version 1.0 before Europe adopts a constitution... (but the challenge is a bit too easy).\n\n\nCheers,\nCharles "
    author: "Charles de Miramon"
  - subject: "Wait a second..."
    date: 2004-05-02
    body: "While I have no problem with either this codename or the about dialog box, why is the codename \"United Europe\" acceptable versus someone crediting the \"United States military\" as support?  Both are credible institutions that do (or will do) both good and bad things.  Both are political entities.\n\nI thought that there was a consensus that politically divisive terms would be avoided in the future.  If the author of a work can't credit who he likes, why can a group of authors? \n\nNote that I still think that authoring the majority of a work gives you the ability to give credit to your parents, your god, your government or your pets (or anything else).  The author's page in books has always been full of a wide variety of recognition of tangential or direct assistance.  \n\nAnd in case it's not clear, I like the codename (and the sentiment), just not on the heels of the whole \"no political messages\".  I'm also taking \"As Kexi is a real member of the KDE and KOffice projects\" from http://www.kexi-project.org/about.html to mean that they are part of the KDE project and not a side project."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Wait a second..."
    date: 2004-05-02
    body: "Uhm, the name has very obvious reasons:\n\n1) All Kexi Core Developers but Jaroslaw were EU members, now he is too\n2) The release date was the 1st of May, which is the day of the EU extension\n\nSo while this sure has also policial aspects, it's mostly about people that are happy to be \"socially closer\" now, and both sides appreciate it. I think this is clearly different to hyping an army (which hardly has anything positive with it). \n\nOf course I see your point. Maybe it is debatable wether this is more a political message, but I don't think so and I think nobody has problems with this codename and  I don't want to start a political discussion here.\n\nI for one wish the Kexi team the very best. Keep on going!"
    author: "ACrowd"
  - subject: "Re: Wait a second..."
    date: 2004-05-04
    body: "\"clearly different to hyping an army (which hardly has anything positive with it). \"\nAs a proud member of the US miltary and supporter of KDE i find that statement highly offensive and ignorant."
    author: "Craig B"
  - subject: "Re: Wait a second..."
    date: 2004-05-04
    body: "As a proud member of the EU I think offensive comparing the European Union with the US army ... (of course, I'm also a KDE supporter :P)"
    author: "Isaac C"
  - subject: "Re: Wait a second..."
    date: 2004-05-06
    body: "Hi :)\nI don't think the codename 'united europe\" is an apropriate one !\nEurope is a capitalist \"entity\" (that politicians call economical) and a propagandist insulte to human kind !\nCapitalisme Crime Contre Chaque Cr\u00e9ature (and unfortunatly in anglo saxon impossible to write the meaning ... In spanish ..ok...\nThe nations who are entering Europa nowadays will have all the burden of being exploited ! If you really look at the \"leading\" countries of europa they are on their fall ! killed by capitalism : France 1000 milliards euros (1000 billions) euros deficit !!! It's a scandal against mankind !!!!\nAnd further more : They are going to vote laws against \"open\" softs...\nI am not masochist neither want I finished my life starving because european policy can't control european capitalist mafia's...\nOpen source is not the privilege of a continent !!!\nIt's an universal aim !!!\nAfter 45 y work (capitalist slavage) government after spoiling people pensions will carry on their terrorism on people ... \nThink about your oldness... Not old passioned people aren't rich !!!\nKind regards : codename suggested \"sun or venus \" or oin's database !!!\nHG"
    author: "Henri Girard"
  - subject: "Re: Wait a second..."
    date: 2004-05-02
    body: ": So while this sure has also policial aspects, it's mostly about people that are happy to be \"socially closer\" now, and both sides appreciate it. I think this is clearly different to hyping an army (which hardly has anything positive with it). \n \nI would say that a political entity has political aspects.  And yes, I agree that the message is upbeat.   I'd also say that the military is pretty upbeat; ask any soldier how they feel.  Most modern democracies can be pretty proud about their military, even if they get misused from time to time.  Same goes for anything political.  The military see themselves as the protectors of their people.  Similar to police.  Or do the police have \"hardly anything positive\"?\n \n: I for one wish the Kexi team the very best. Keep on going!\n\nSame here.  It looks great, and I don't mean to obscure that with commentary on the codename.  Being able to load up database schemas to show new developers is a damn handy thing.  I'll have to play with it to see if you can print them out; I was asked for a printed schema last week by a VC guy who is funding a company we're doing work for."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Wait a second..."
    date: 2004-05-03
    body: ">Most modern democracies can be pretty proud about their military, even if they get misused from time to time. \n\nIf a civil society as a whole is proud of their military forces, then something is definitely wrong. And no, military is not similar to police."
    author: "Carlo"
  - subject: "Re: Wait a second..."
    date: 2004-05-03
    body: "I could be proud if the military forces were used defensively.  Just because it hasn't happened in the past several decades doesn't mean it couldn't happen in the future ;)"
    author: "ac"
  - subject: "Re: Wait a second..."
    date: 2004-05-03
    body: ":: If a civil society as a whole is proud of their military forces, then something is definitely wrong. And no, military is not similar to police.\n\nI would phrase that the exact opposite.  If a civil society as a whole is *not* proud of their military forces, then something is definitely wrong.\n"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Wait a second..."
    date: 2004-05-03
    body: ">I would phrase that the exact opposite. If a civil society as a whole is *not* proud of their military forces, then something is definitely wrong.\n\nGood point. I would rather be proud of a properly acting military (and one that is acting to defend its' country), then to fear my own.\n\nHowever, as to your original question, the US military (as well as other militaries) act to prop up or defend its' government.  The \"United Europe\" is more akin to saying the \"United States\" than US Military. From most people's POV, it is a huge difference.\n\n"
    author: "a.c."
  - subject: "Re: Wait a second..."
    date: 2004-05-03
    body: "A army is nothing to be proud of, it's dirty business that has to be done by a few idiots that sell their freedom for money.\n\nAn army is an inefficient organisation where everything goes wrong. Nothing to be proud of.\n\nAm I proud of the fire forces? The hospitals? The police? The air conditioners? The traffic system? The water supply?\n\nNo. \n\n<a href=\"http://en.wikipedia.org/wiki/Eala_Freya_Fresena\">Eala Freya Fresena.</a> There is a huge difference between a fighter and a soldier.\n\nProstitution is the sale of sexual services for money or other kind of return, generally indiscriminately with many persons. A person selling sexual favors is a prostitute, a type of sex worker. Most prostitutes are women offering their services to men.\"\n\nArmy service is the sale of ... Most soldiers are men offering their services to the Government.\""
    author: "Goring"
  - subject: "Re: Wait a second..."
    date: 2004-05-03
    body: ">I would phrase that the exact opposite. If a civil society as a whole is *not* proud of their military forces, then something is definitely wrong.\n\nYou can be proud of things you have done. If a soldier defends his country and never was involved in wartime atrocities, he may be proud of himself. But being proud to need military to defend yourself against other humans is just dumb."
    author: "Carlo"
  - subject: "Re: Wait a second..."
    date: 2004-05-03
    body: "\"Most modern democracies can be pretty proud about their military, even if they get misused from time to time. \"\nThis is nothing but an opinon.\n\n\" If a civil society as a whole is proud of their military forces, then something is definitely wrong. And no, military is not similar to police.\"\nThis is also nothing but an opinon.\n\nThe problem with the extreme left and the extreme right is that they feel there oponon carries the weight of unquestionable truth. \nIf you are going to have a rule that no politcal statments should be made by authors of software in the kde collection you have to inforce it in all cases which is a form of censorship but it is least fair. Or you could say that no political statments the we do not like may be made, which also makes you a censor but not a fair one. Or you could say that people are free to put in what ever they want. Which is what I think should be done. Of course that is just my opinon.\n\nOf course the amount of total stupidity being generated by this disscusion is huge. "
    author: "LWATCDR"
  - subject: "Re: Wait a second..."
    date: 2004-05-05
    body: ">Or you could say that people are free to put in what ever they want\n\nThats the only choice you have unless you could properly define 'political statement'. Isn't it a political statement to say: 'Developing software in an open source process is more likely to succeed in the future.' or isn't it political to say: 'in the morning the sun shines into my room through the window and i want this to be so forever'.\n\nThink twice: Everything is political so everything else than absolutly free speech includes believing to have the only right answer to every question. "
    author: "mbae"
  - subject: "Re: Wait a second..."
    date: 2004-05-05
    body: ">>Thats the only choice you have unless you could properly define 'political statement'. \n>>Isn't it a political statement to say: 'Developing software in an open source process is more likely to succeed in the future.' \nI would not say that is correct. It is an opinon but does not carry a value judgement. The statment, \"Developing closed source software is immoral.\" would be a political statment. To be a political statment I would have to say that it must contain a value judgement. You could go so far as to say that it must contain a judgement or statment about a ruling body or the laws/rules that they create and inforce.\n\nSo saying that my eyes are brown is not a political statment. Or that I do not like the taste of squash is not a political statment. Both are facts.\n\nor isn't it political to say: 'in the morning the sun shines into my room through the window and i want this to be so forever'.\nI would say that was a factual statment and does not contain any opinon. In the morning the sun would tend to shine through an window in a room. And unless that person was a lair I would take the statment that they want this to be so forever at face value. Now if live in the arctic circle then it not completely true :) Yes it was a nit picking joke.\n\nI will even go so far as to say that hate speech has no place and would support KDE not allowing people to put things like \"Death to Arabs\", \"Death to America\",  \"Hitler was right\", and many uglier things that people could come up with in about boxes. But things like \"We love the US Army\" should not be considered should be banned. If you try and take it in a positive light it means we love the people in the US Army. I would not be offended by someone putting \"Allah protect the people of Iraq\" in an about box. The issue is that KDE said not political statments, fine then no political statments. \"United Europe\" is without a doubt a political statment. I will allow that it may even be a positive statment, but I thought the rule was no political statments.\n"
    author: "LWATCDR"
  - subject: "Re: Wait a second..."
    date: 2004-05-06
    body: "> \"United Europe\" is without a doubt a political statment.\n\nHow is \"United Europe\" a political statement? \n\nIt's like having a project code name \"US Army\" or \"Germany\". It's not a statement. It says neither anything positive nor negative about it. It could mean \"United Europe rules\" or \"United Europe sucks\". Those would actually be political statements and I'm sure they would be banned.\n\nChristian"
    author: "Christian Loose"
  - subject: "Re: Wait a second..."
    date: 2004-05-04
    body: "This political statement could inhibit developers from getting involved because they might think that Kexi is only for EU facists or something ;)"
    author: "Craig B"
  - subject: "Re: Wait a second..."
    date: 2004-05-04
    body: "Eliminate the \"fascist\" comment, and yeah - it does perpetuate the stereotype that \"KDE is by and for Europeans\".  Not to minimize the fact that a majority of active developers are, but it does seem to subtly discourage \"outside\" involvement.\n\nI still think it's a good thing.  Pride in one's home is a positive thing, and celebrating your home (at town/city, county/canton/parish, state/country, union or planet level) or any aspect of it in your work is healthy and happy.  Same goes for work or hobbies.  I happen to know of a good source of catnip through KDE because it's not just a monolithic entity obsessed with lowest common denominator and political correctness.   The end product is polished and professional, but having some character in the developers is a good thing, imo."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Wait a second..."
    date: 2004-05-05
    body: "I really don't think EU types are fascists i was simply making a point to the euro's how it looks from the opposite point of view. What seems like a harmless statement on one side of the Atlantic raises emotions on the other. Mainland Europe isn't exactly at the top of americans popularity chart right now. The US Army is extremly popular here in the US."
    author: "Craig B"
  - subject: "Re: Wait a second..."
    date: 2004-05-03
    body: "It's the celebration of something great (the unification of Europe), not controversy (aka, Iraq/Military).\n\nAlso, Kexi isn't thanking the unification of Europe for Kexi as the previous author did with the military. The Kexi team are simply associating dates..."
    author: "Martin Galpin"
  - subject: "Re: Wait a second..."
    date: 2004-05-03
    body: "> It's the celebration of something great (the unification of Europe), not controversy (aka, Iraq/Military).\n\nI know several Englishmen who don't think of the unification of Europe as something great.  And at least one of them maybe more doesn't think there's anything wrong with the actions in Iraq.  Smells like hypocrisy to me.\n\n> Also, Kexi isn't thanking the unification of Europe for Kexi as the previous author did with the military. The Kexi team are simply associating dates...\n\nNow this seems more reasonable."
    author: "General Zod"
  - subject: "Re: Wait a second..."
    date: 2004-05-03
    body: ">I know several Englishmen who don't think of the unification of Europe as something great. \n\nBeside the fact that the UK had always a very different point of view, even compared to the polyphonic continental europe, their special \"doggie ;)\" relationship to the US and that they will have to decide in a not too distant future if they want to go with continental europe or not, I don't think that the unification is \"great\", too. In fact it will raise a bunch of problems, but it's just indispensable. A unstable - and it would be far more unstable as it is now, if they had not seen a EUropean future - poorhouse of ~75 million humans directly at eastern EU borders in conjunction with the uncertain future of a undemocratic russia could lead to more problems, Joe Englishmen may think of on his not so little isle."
    author: "Carlo"
  - subject: "Re: Wait a second..."
    date: 2004-05-03
    body: "Actually, though my fellow Englishmen are (unfortunately) on the whole quite anti Europe, or at least not pro-Europe, few have grumbled about the expansion of member states.\n\nWhat the anti-EU camp don't like is when they think the EU is taking power away from our national Parliament, which includes (according to them) the new constitution, and any further unification of powers in the EU itself. That's a different issue to expanding the size of the EU.\n\nCompletely off-topic, but I thought I should clarify :-)"
    author: "Tom Chance"
  - subject: "Re: Wait a second..."
    date: 2004-05-03
    body: "The britons respect that people are political unequal by birth. There is still a noble man chamber of british parliament and they want to educate the EU because of lack of democracy ... tsetse.\n\nBtw: This is why Power to the EU-Parliament has to be given. And civil society representation has to be improved."
    author: "aile"
  - subject: "Re: Wait a second..."
    date: 2004-05-03
    body: "because not even anti-European Englishmen and women opposed the expansion of the EU. Everyone thinks a united Europe is a good thing, we just disagree over the way in which it should be united. ;-)."
    author: "c"
  - subject: "Re: Wait a second..."
    date: 2004-05-03
    body: "<It's the celebration of something great (the unification of Europe)\n\nMore power to the EU is certainly not a great thing if that is what you mean by the unification of Europe. \nThey can of course name their application any way they want, but we need to agree that KDE is not some sort of pro-EU project.\n\n/Viktor (Denmark)"
    author: "Viktor Rasmussen"
  - subject: "Re: Wait a second..."
    date: 2004-05-03
    body: "How on earth do you see an expansion of EU as being more power to EU? If anything it becomes much harder for the EU to make any decisions now, with more people who have to agree.\n\nThe last part is the reason for the upcoming constitution, which is supposed to make EU capeble of taking decisions again (atleast mathematically)."
    author: "Allan S."
  - subject: "Re: Wait a second..."
    date: 2004-05-03
    body: "I will give you an example. We need to get more power to the European Parliament in order to get rid of Software-Patents and the like. The problem is that in the EU the commission and the executive branch is too strong.\n\nEurope is not \"the EU\". You don't have to agree with the EU."
    author: "Hans"
  - subject: "Re: Wait a second..."
    date: 2004-05-03
    body: "It's not necessarily more power to the EU though - it may well be a drain on the EU for a long long time. And! since it wasn't even a EU themed name I think you should just back the hell off."
    author: "c"
  - subject: "Re: Wait a second..."
    date: 2004-05-03
    body: "<i>It's the celebration of something great (the unification of Europe), not controversy (aka, Iraq/Military)</i>\n\nI don't like the European Union at all, and I don't see the extension of EU as something to celebrate. On the other hand It is my belief that I owe my freedom to the the US Army. So if you cannot thank the US Army in a release statement, then you can't celebrate the EU. Same rule for all, or no rule at all."
    author: "Claus Rasmussen"
  - subject: "Re: Wait a second..."
    date: 2004-05-04
    body: "> On the other hand It is my belief that I owe my freedom to the the US Army\n\nImpressive ..."
    author: "Isaac C"
  - subject: "Re: Wait a second..."
    date: 2004-05-04
    body: "I can celebrate united Europe or atl east a less separated one because I has a reason for it.\nI cannot thank the US army because I doubt that its current missions really help us.\nOK, this isn't the fault of the US army (people who torture excepted), perhaps you could thank the soldiers for positive intentions and risking their lives."
    author: "CE"
  - subject: "Re: Wait a second..."
    date: 2004-05-03
    body: "I think that if you read much of the thread that this statement has created that will see that it is just an opinon that the unification of Europe is great. You will also see that the the statment that is not a controversy is a false statment. The UK bashing is at best distasteful.\n\n\"It's the celebration of something great (the unification of Europe), not controversy (aka, Iraq/Military).\"\n\nYea right.\nGet over it folks I swear people spend so much complaining about the state of this or that I wonder how much time they spend actually doing instead of talking. I have code to write. To all of those that where civil thank you to thoses of you that where not.... Good day."
    author: "LWATCDR"
  - subject: "Re: Wait a second..."
    date: 2004-05-03
    body: "Well, the name may be a bit ill-advised in many ways, but each to his own."
    author: "David"
  - subject: "Re: Wait a second..."
    date: 2004-05-03
    body: "KDE is a democratic body with developers with different (and sometimes conflicting) political opinions. Codenames are transient and a mark of the individuality of the hackers. My opinion is that if you are not insulting somebody or being rude or extremist, you can express what you want, even a political opinion. \nSo you can codename the version 1.x of KJabberWokky \"I love the American Army\" but not \"Fuck the American Army\".\n\nThe case, you are mentioning relates to a pro-Bush ex-developer wanting to have in \"the about box\" a political message that could look like it was the opinion of the whole KDE project. It had also the danger of spreading the idea that about boxes were the place to make political statements."
    author: "Charles de Miramon"
  - subject: "Re: Wait a second..."
    date: 2004-05-03
    body: "I'm sorry, but... In what way does the expression \"I love the American Army\" differ from \"Fuck the American Army\" in it's political content, rudeness or extremist-ness*? In my opinion, declaring your love for that institution is the same as declaring your aversion against it. Both have a political meaning, both can offend other people. \n\nIn the Kexi case, I don't think there is a problem though. As stated before, it's just an issue of date and the fact that all the developers now live in the EU. That's not political as such, but merely factual.\n\n*) not sure if this is the correct expression... English is not my native language."
    author: "Andr\u00e9 Somers"
  - subject: "Re: Wait a second..."
    date: 2004-05-03
    body: "No... saying \"I don't like the French army\" and \"Fuck the French army\" is not the same. With the second formulation, you express an opinion and insult a group of people. "
    author: "Charles de Miramon"
  - subject: "Re: Wait a second..."
    date: 2004-05-03
    body: "So if a developer released a program on July Fourth would you have a problem with them calling it God Bless America? I mean if they where from the US of course? Since there is nothing rude about God Bless America?"
    author: "LWATCDR"
  - subject: "Re: Wait a second..."
    date: 2004-05-04
    body: "Perhaps it should be formulated different ... something without \"God bless\"."
    author: "CE"
  - subject: "Re: Wait a second..."
    date: 2004-05-04
    body: "Why should it be? Just becuse you or others do not belive in God?\nIn the United States the freedom of religion is very important. Who should have the right to infringe on that right? Frankly this whole converstation has soured me on developing useing KDE/QT I have never liked the fact that I can not use QT in GPL software that runs under windows. ANd now I find that there are a bunch of thought nazis that want to tell a developer what they can or can not put in there abouts. I have no problem with terms like God Bless, Or someone saying they love the US Army \"most of the people in the us army are worthy of respect\", or Budda bless, or God Bless the queen, or my the EU bless you. I will put what I bloody well please in my about box of what ever program I release. I find it disgusting that most people here would defend to the death the right of someone to wear a shirt that has source to DeCSS but support censorship of what someone wants to put in the about box of there program."
    author: "lwatcdr"
  - subject: "Re: Wait a second..."
    date: 2004-05-05
    body: "It's not that I don't believe in God (I do not in the way the Catholic church it has to be), but I think you have to separate between state and religion, especially if you say \"the freedom of religion is very important\".\nPeople who believe in several gods won't like \"God bless [s.o.]\".\nAnd moreover it would be improper in that context.\n\nThe problem you don't see is that everybody can do that (as long as it doesn't hurt the liberties of other people), but not as part of the KDE project.\nKDE shouldn't have tastes of a special religion, political direction, etc.\nI think they can release a personal version with such things, but not in the official sources.\nNobody's freedom is hurt.\nThis persons have to learn getting along with other people.\n\nYou call people who don't want to have (tendentially) right wing slogans in KDE thought nazis? (If you don't think that, read their website ...)\nYou sholud call them thought commies. ;-)"
    author: "CE"
  - subject: "Re: Wait a second..."
    date: 2004-05-05
    body: "If you believe that KDE should not have tastes of a special religion, political direction, etc then you should be the europen union name to be out side the limits of KDE.\n\nAs to calling KDE thought nazis instead thought commies only because comunists are not as describe by Marx totaly evil. It was at least an interesting idea that had at it's root the goal of imporving the standard of living for the masses. It has never really been tried and I feel would fail totaly. Nazis are just plain evil.\n\nAs far as people learning to get along you are right. As to infringeing on religion you are wrong. The idea of freedom of religion is not freedom from religion.\nI would not be offended if someone would say \"may Budda grant you enlightenment\", \"God Bless you\", \"may Allah watch over you\", or \"good health to you\". My statments or the statments of other do not have any effect on anyone elses liberties. There has yet to be any country that has given the right to not be offened. Such a law would require each person to  be forbiden to speak to any other person. I feel if you have a problem with the term \"God/Budda/Allah/The Great Pumpkin\" bless you then the problem is yours.  As I said even though I would probably never put a message like \"God Bless America\" in one of my programs the lack of freedom that I see just sours me on KDE the KDE development community. As far as church and state.... Last time I looked KDE was not a Church or a state.\n"
    author: "LWATCDR"
  - subject: "Re: Wait a second..."
    date: 2004-05-03
    body: "KDE isn't democratic, by the way ;) It's more of a meritocracy, with the best coders deciding what happens."
    author: "Tom Chance"
  - subject: "Re: Wait a second..."
    date: 2004-05-03
    body: "KDE (like other Open Source projects) is democratic in the sense that it needs to grow a certain set of democratic rules like a democratic legal system that will protect our GPL licences and that the project promotes by its actions certain democratic values (freedom of association, freedom of diffusion of knowledge, freedom of opinion). I guess that we would be a good example of a working Popperian 'Open Society'. \n\n"
    author: "Charles de Miramon"
  - subject: "Re: Wait a second..."
    date: 2004-05-03
    body: "Hmm, *needing* to grow democratic rules, and *promoting* certain values in common with democracy doesn't make KDE democratic, unless you claim that it makes unequal representation of unequal participants, in the sense that users and experienced developers should have a different status ;)\n\nUntil KDE develops some rules, be they cultural or codified into some kind of written constitution or contract, it's a meritocracy, albeit a rather nice one."
    author: "Tom Chance"
  - subject: "Re: Wait a second..."
    date: 2004-05-03
    body: "You equate democracy with election which is not always the case. British judges are not elected like American attorneys. That does not make them less democratic. A democratic system can choose that certain actions or tasks does not reflect the will of the majority of the citizens if it is proved that it makes the system work better. The representation mecanism is a good application of this idea. Today, the French prime minister and parliament are hugely unpopular but until the next election they are still making the decisions and voting the law.\n\nTechnical decisions are better made by people with a thorough knowledge of the complexity of the system. For non technical questions, there is generally a rough poll on kde-core-devel and kde-policies to see where is the consensus or how to find one."
    author: "Charles de Miramon"
  - subject: "Re: Wait a second..."
    date: 2004-05-04
    body: "Yes, you're right that you don't need elections, nor even representation, nor many of the features we commonly associate with \"democracy\".\n\nHowever, I think the only sense in which KDE is democratic, and in which it makes decisions based on the will of all of its users rather than the will of the developers, is that developers have no incentive to make changes that most users will dislike, and so to stay competitive KDE must evolve in a way that its users want. But only to that extent, and I'd hardly call that democratic ;-)"
    author: "Tom Chance"
  - subject: "Re: Wait a second..."
    date: 2004-05-03
    body: "Europe is a vision of people, you don't have to like the European Union in particular. United Europe is far more than a institutional body. A European vision is no party specific message.\n\nThe US military is an object (a tool) of US government policy. I don't understand why one should sent a \"political message\" to a specific military body and its members.\n\nI never understood this \"Support our troops\" issue. A soldier is an instrument that sells his freedom to the Government. And that's the reason why professional armys usually attract the scum of society. Sure, they have to be told that they are great guys. Everybody knows that it's a lie except them.\n\nWhen you \"support the US troops\" today you support the US governments illegal occupation of Iraq. I guess Iraqi users of KDE will not be pleased by that. I am more on the side of the US and hope they will manage the mess, but it is very legitimate to support both sides.\n\nWhen you call a release \"United Europe\" it does not sent a message against anyone who is not European. "
    author: "Jupp"
  - subject: "Re: Wait a second..."
    date: 2004-05-03
    body: "> When you call a release \"United Europe\" it does not sent a message against anyone who is not European.\n\nWell, I think you just hit the mark of the essence of this whole topic. It's undertitled \"United Europe\" - and that's exactly reflecting, what some states of this old land called \"Europe\" did on that day. They united. The countries where most of kexis developers live in, united. And to second the previous poster: I think, the worst reaction that the words \"United Europe\" may provoke would be indifference/complete lack of interest."
    author: "Thomas"
  - subject: "Re: Wait a second..."
    date: 2004-05-03
    body: "Norway is not part of the EU but I regard it as part of a United Europe. "
    author: "aile"
  - subject: "Re: Wait a second..."
    date: 2004-05-04
    body: "But is there a problem?\nI think becoming a EU member was different for the ten than it would be for Norwey."
    author: "CE"
  - subject: "Re: Wait a second..."
    date: 2004-05-07
    body: "Scum of society?  You sir, are a fool. \n\nWhile you have every right to express your opinion on whether military forces are being used in a good/bad way, you make yourself look like a moron with blanket statements of that kind. You see, anyone who has ever known, befriended or been related to a military man or woman knows how shallow and false that statement is.\n\nUsually I keep quiet when I see posts like this, but that phrase made my skin crawl. And the sad thing is, you probably didn't even think of how callow it makes you sound.   "
    author: "John M."
  - subject: "compiling goes fine till I hit a wall...!"
    date: 2004-05-03
    body: "configure: creating ./config.status\nfast creating Makefile\nfast creating kexi/Makefile\ncan't open ./kexi/Makefile.in: No such file or directory\nfast creating kexi/3rdparty/Makefile\ncan't open ./kexi/3rdparty/Makefile.in: No such file or directory\nfast creating kexi/3rdparty/kexisql/Makefile\ncan't open ./kexi/3rdparty/kexisql/Makefile.in: No such file or directory\nfast creating kexi/3rdparty/kexisql/src/Makefile\ncan't open ./kexi/3rdparty/kexisql/src/Makefile.in: No such file or directory\nfast creating kexi/3rdparty/kexisql/tool/Makefile\ncan't open ./kexi/3rdparty/kexisql/tool/Makefile.in: No such file or directory\nfast creating kexi/3rdparty/uuid/Makefile\ncan't open ./kexi/3rdparty/uuid/Makefile.in: No such file or directory\nfast creating kexi/core/Makefile\ncan't open ./kexi/core/Makefile.in: No such file or directory\nfast creating kexi/data/Makefile\ncan't open ./kexi/data/Makefile.in: No such file or directory\nfast creating kexi/filters/Makefile\ncan't open ./kexi/filters/Makefile.in: No such file or directory\nfast creating kexi/filters/import/Makefile\ncan't open ./kexi/filters/import/Makefile.in: No such file or directory\nfast creating kexi/filters/import/csv/Makefile\ncan't open ./kexi/filters/import/csv/Makefile.in: No such file or directory\nfast creating kexi/filters/import/kspread_chain/Makefile\ncan't open ./kexi/filters/import/kspread_chain/Makefile.in: No such file or directory\nfast creating kexi/formeditor/Makefile\ncan't open ./kexi/formeditor/Makefile.in: No such file or directory\nfast creating kexi/formeditor/factories/Makefile\ncan't open ./kexi/formeditor/factories/Makefile.in: No such file or directory\nfast creating kexi/formeditor/test/Makefile\ncan't open ./kexi/formeditor/test/Makefile.in: No such file or directory\nfast creating kexi/interfaces/Makefile\ncan't open ./kexi/interfaces/Makefile.in: No such file or directory\nfast creating kexi/interfaces/kwmailmerge/Makefile\ncan't open ./kexi/interfaces/kwmailmerge/Makefile.in: No such file or directory\nfast creating kexi/kexidb/Makefile\ncan't open ./kexi/kexidb/Makefile.in: No such file or directory\nfast creating kexi/kexidb/drivers/Makefile\ncan't open ./kexi/kexidb/drivers/Makefile.in: No such file or directory\nfast creating kexi/kexidb/drivers/mySQL/Makefile\ncan't open ./kexi/kexidb/drivers/mySQL/Makefile.in: No such file or directory\nfast creating kexi/kexidb/drivers/odbc/Makefile\ncan't open ./kexi/kexidb/drivers/odbc/Makefile.in: No such file or directory\nfast creating kexi/kexidb/drivers/pqxx/Makefile\ncan't open ./kexi/kexidb/drivers/pqxx/Makefile.in: No such file or directory\nfast creating kexi/kexidb/drivers/sqlite/Makefile\ncan't open ./kexi/kexidb/drivers/sqlite/Makefile.in: No such file or directory\nfast creating kexi/kexidb/parser/Makefile\ncan't open ./kexi/kexidb/parser/Makefile.in: No such file or directory\nfast creating kexi/main/Makefile\ncan't open ./kexi/main/Makefile.in: No such file or directory\nfast creating kexi/main/filters/Makefile\ncan't open ./kexi/main/filters/Makefile.in: No such file or directory\nfast creating kexi/main/projectWizard/Makefile\ncan't open ./kexi/main/projectWizard/Makefile.in: No such file or directory\nfast creating kexi/main/startup/Makefile\ncan't open ./kexi/main/startup/Makefile.in: No such file or directory\nfast creating kexi/migration/Makefile\ncan't open ./kexi/migration/Makefile.in: No such file or directory\nfast creating kexi/pics/Makefile\ncan't open ./kexi/pics/Makefile.in: No such file or directory\nfast creating kexi/plugins/Makefile\ncan't open ./kexi/plugins/Makefile.in: No such file or directory\nfast creating kexi/plugins/forms/Makefile\ncan't open ./kexi/plugins/forms/Makefile.in: No such file or directory\nfast creating kexi/plugins/importwizard/Makefile\ncan't open ./kexi/plugins/importwizard/Makefile.in: No such file or directory\nfast creating kexi/plugins/importwizard/common/Makefile\ncan't open ./kexi/plugins/importwizard/common/Makefile.in: No such file or directory\nfast creating kexi/plugins/importwizard/file/Makefile\ncan't open ./kexi/plugins/importwizard/file/Makefile.in: No such file or directory\nfast creating kexi/plugins/kugar/Makefile\ncan't open ./kexi/plugins/kugar/Makefile.in: No such file or directory\nfast creating kexi/plugins/kugar/kudesigner/Makefile\ncan't open ./kexi/plugins/kugar/kudesigner/Makefile.in: No such file or directory\nfast creating kexi/plugins/queries/Makefile\ncan't open ./kexi/plugins/queries/Makefile.in: No such file or directory\nfast creating kexi/plugins/relations/Makefile\ncan't open ./kexi/plugins/relations/Makefile.in: No such file or directory\nfast creating kexi/plugins/scripting/Makefile\ncan't open ./kexi/plugins/scripting/Makefile.in: No such file or directory\nfast creating kexi/plugins/tables/Makefile\ncan't open ./kexi/plugins/tables/Makefile.in: No such file or directory\nfast creating kexi/tableview/Makefile\ncan't open ./kexi/tableview/Makefile.in: No such file or directory\nfast creating kexi/tests/Makefile\ncan't open ./kexi/tests/Makefile.in: No such file or directory\nfast creating kexi/tests/newapi/Makefile\ncan't open ./kexi/tests/newapi/Makefile.in: No such file or directory\nfast creating kexi/tests/parser/Makefile\ncan't open ./kexi/tests/parser/Makefile.in: No such file or directory\nfast creating kexi/tests/startup/Makefile\ncan't open ./kexi/tests/startup/Makefile.in: No such file or directory\nfast creating kexi/tests/tableview/Makefile\ncan't open ./kexi/tests/tableview/Makefile.in: No such file or directory\nfast creating kexi/widget/Makefile\ncan't open ./kexi/widget/Makefile.in: No such file or directory\nfast creating kexi/widget/propertyeditor/Makefile\ncan't open ./kexi/widget/propertyeditor/Makefile.in: No such file or directory\nfast creating kexi/widget/propertyeditor/test/Makefile\ncan't open ./kexi/widget/propertyeditor/test/Makefile.in: No such file or directory\nfast creating kexi/widget/relations/Makefile\ncan't open ./kexi/widget/relations/Makefile.in: No such file or directory\nconfig.pl: fast created 52 file(s).\nconfig.status: creating config.h\nconfig.status: config.h is unchanged\nconfig.status: executing depfiles commands\n\nCan somebocy help me out?\n\nCb..\n"
    author: "charles"
  - subject: "Re: compiling goes fine till I hit a wall...!"
    date: 2004-05-03
    body: "Try to run:\n\ncd kexi-0.1beta3/\nmake -f Makefile.cvs\n\n\nthen:\n\n ./configure\nand so on..."
    author: "Jaroslaw Staniek"
  - subject: "Re: compiling goes fine till I hit a wall...!"
    date: 2004-05-03
    body: "A fix for above ussue already uploaded (the same archive), but it takes some time to be mirrored by servers.\n"
    author: "Jaroslaw Staniek"
  - subject: "worked...! Thanx, but what about this...?"
    date: 2004-05-03
    body: "That is, make -f Makefile.cvs worked fine. Now, I get the following output when I d a `make' \n\nmake  all-recursive\nmake[1]: Entering directory `/home/judith/Documents/kexi-0.1beta3'\nMaking all in lib\nmake[2]: Entering directory `/home/judith/Documents/kexi-0.1beta3/lib'\nmake[2]: *** No rule to make target `all'.  Stop.\nmake[2]: Leaving directory `/home/judith/Documents/kexi-0.1beta3/lib'\nmake[1]: *** [all-recursive] Error 1\nmake[1]: Leaving directory `/home/judith/Documents/kexi-0.1beta3'\nmake: *** [all] Error 2\n\nOtherwise I thank you all for your work. \n\nCb.."
    author: "charles"
  - subject: "Re: worked...! Thanx, but what about this...?"
    date: 2004-05-03
    body: "Source archive is now updated (link the same as before) - fixed './configure' problems, so please download again if you need.\n"
    author: "Jaroslaw Staniek"
  - subject: "the alternatives..."
    date: 2004-05-03
    body: "Kexi is an outstanding initiative, but still has some way to go. For the time,  DBDesigner would seem the best choice. DBD compares favourably to the likes of Oracle Designer and Erwin for ERD work.  I'm sure they could do with more support... http://www.fabforce.net/dbdesigner4/\n\n"
    author: "spinctrl"
  - subject: "CVS Compiling?"
    date: 2004-05-05
    body: "I am having trouble compiling the CVS version of kexi as part of the KOffice module and I was hoping someone could help me with it. Here is the error message:\n\n/home/jose/cvs/kde/qt-copy/include/qptrlist.h: In member function `void\n   QPtrList<type>::deleteItem(void*) [with type = KexiPart::DataSource]':\n/home/jose/cvs/kde/qt-copy/include/qvaluevector.h:130:   instantiated from here\n/home/jose/cvs/kde/qt-copy/include/qptrlist.h:150: invalid use of undefined\n   type `struct KexiPart::DataSource'\n/home/jose/cvs/kde/src/koffice/kexi/core/kexipartmanager.h:44: forward\n   declaration of `struct KexiPart::DataSource'\nmake[2]: *** [kexipartmanager.lo] Error 1\n\nI am using the CVS version of qt-copy which is 3.3.2 and its applied patches (using ./apply_patches script)."
    author: "Jose Hernandez"
  - subject: "Re: CVS Compiling?"
    date: 2004-05-05
    body: "Please don't post so many times the same question (on kde-apps).\n\nWhat's your gcc version (gcc --version)? \nWhat's your Linux distribution?\nI've heard about this compile bug already.\n\nNext time you'd want to visit us on #kexi channel, irc.freenode.net server.\n\nregards,\nJaroslaw Staniek / Kexi Team"
    author: "Jaroslaw Staniek"
  - subject: "Re: CVS Compiling?"
    date: 2004-05-05
    body: "Please read http://www.kexi-project.org/compiling.html, \"Known problems\" section.\n"
    author: "Jaroslaw Staniek"
  - subject: "Re: CVS Compiling?"
    date: 2004-05-05
    body: "My apologies, and thank you for your response and more for this solution.\n\ngcc (GCC) 3.2.3 20030422 (Gentoo Linux 1.4 3.2.3-r3, propolice)"
    author: "Jose Hernandez"
---
The <A href="http://www.kexi-project.org/people.html">Kexi Team</A> announced the immediate availability of Kexi 0.1 beta 3, codenamed &#034;United Europe&#034;, a new preview release of the integrated data management environment aimed at developers and experienced users. Please read the 
<A href="http://www.kexi-project.org/announce-0.1-beta3.html">announcement</A> 
and <A href="http://www.kexi-project.org/compiling.html">how you can compile Kexi</A>.

<!--break-->
