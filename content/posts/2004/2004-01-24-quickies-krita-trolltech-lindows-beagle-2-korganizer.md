---
title: "Quickies: Krita, Trolltech, Lindows, Beagle-2, KOrganizer"
date:    2004-01-24
authors:
  - "fmous"
slug:    quickies-krita-trolltech-lindows-beagle-2-korganizer
comments:
  - subject: "Beagle-2?"
    date: 2004-01-24
    body: "Not what I'd call a figurehead...\nHasn't it got lost somewhere?\nKDE certainly isn't the cause ;-/ !!!\nNo really ;-)\n\nAnyway: Nice that Krita is somewhat back to life\nagain. A KDE/QT-based painting app is certainly\nneeded and GIMP is now the last GTK app that\nhas survived on my desktop - and frankly I hate\nthat stupid multi-windows interface.\n"
    author: "Mike"
  - subject: "Re: Beagle-2?"
    date: 2004-01-24
    body: "Are there any screenshots of krita anywhere?"
    author: "JohnFlux"
  - subject: "Re: Beagle-2?"
    date: 2004-01-24
    body: "http://www.koffice.org/krita/screenshots.php"
    author: "Anonymous"
  - subject: "Re: Beagle-2?"
    date: 2004-01-24
    body: "Most of the notes in my blog on my progress with Krita this week have a screenshot or so-- in fact the link above points to one of them. It might be a bit slow, though, since my server is behind a slow ADSL link."
    author: "Boudewijn Rempt"
  - subject: "Re: Beagle-2?"
    date: 2004-01-24
    body: "Boudewijn,\n\nI should have asked maybe. Didn't know that. Dot.kde.org can still mirror the image if you would like that. \n\nCiao \n\nPS You live in the Netherlands, if so are you going to Fosdem maybe?"
    author: "fab"
  - subject: "Re: Beagle-2?"
    date: 2004-01-24
    body: "It's no problem for me -- I don't have to wait for the images to load :-). And judging from the little knetload graph most people only have to wait a second or two before getting the image.\n\nI thought about going to Fosdem, but I already had other commitments, maybe next year."
    author: "Boudewijn Rempt"
  - subject: "Krita again"
    date: 2004-01-24
    body: "BTW: Is there some info on how to compile\nonly Krita? I've checked out Koffice from\nCVS but don't really want to compile it all.\n"
    author: "Mike"
  - subject: "Re: Krita again"
    date: 2004-01-24
    body: "Try to use KDE's inst-apps system.\n\nCreate in the top directory of koffice a file inst-apps and on each line of it add the top subdirectories that you want to compile.\n\nFor Krita, I suppose that you need lib and krita. (Sorry, no guarantee that it will work.)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Krita again"
    date: 2004-01-24
    body: "Well, if you have installed KOffice (and all headers, of course), you could try to checkout the koffice cvs module, do a make -f Makefile.cvs && ./configure -- and then cd into the krita subdir and do a make from there. I'm not sure it would work, but it's worth a try."
    author: "Boudewijn Rempt"
  - subject: "Re: Krita again"
    date: 2004-01-24
    body: "OK, if anyone else is interested:\nIt won't work.\nWell, guess we have to be patient until a\nfirst beta comes out...\n\n\n"
    author: "Mike"
  - subject: "Re: Krita again"
    date: 2004-01-24
    body: "Well, I think that you'd get a KOffice built sooner than that I can get Krita into even alpha shape... I mean, even on my seriously underpowered Powerbook G3 it only takes four hours or so :-)"
    author: "Boudewijn Rempt"
  - subject: "Re: Krita again"
    date: 2004-01-24
    body: "Oh, and I forgot to say: you really need both ImageMagick and LittleCMS to compile Krita. SUSE provides development packages for both, as does Debian."
    author: "Boudewijn Rempt"
  - subject: "Korganizer "
    date: 2004-01-24
    body: "Now, Korganizer nears a \"killer-app\" status. It's nice to see it present in dot news, as a start for a good PR campaign. Congrats to the hardworking PIM team."
    author: "Inorog"
  - subject: "Re: Korganizer "
    date: 2004-01-24
    body: "Can Korganizer be intregrated with Plone?"
    author: "Andre"
  - subject: "Re: Korganizer "
    date: 2004-01-24
    body: "If you explain what you mean by \"integrated\" and what would need to be done for that, I might be able to give you an answer.\n"
    author: "Cornelius Schumacher"
  - subject: "Re: Korganizer "
    date: 2004-01-26
    body: "I would love to see this as well. I am not the original poster, but I'd love to seethe ability to save calendar data and integrate it into plone's calendar or maybe create  a module for plone that would interface with korganizer.\n\n"
    author: "Eu"
  - subject: "Re: Korganizer "
    date: 2004-01-26
    body: "Is it possible that you are looking for \"kandy\" (http://pim.kde.org/components/kandy.php)? It may be limited but until \"kitchensync\" is finished it's quite nice..."
    author: "Birdy"
  - subject: "Re: Korganizer "
    date: 2004-01-24
    body: "Nothing kills an application faster than a horrible user interface.  Look at the icons in Kontact/KOrganizer's sidebar - they are horrific.  The KMail and the phone/contacts icon just has to go.  It is just simply ugly to look at.\n\nKOffice had ugly icons for a long time (bold, italic, underline), but they've fixed it.  Time for Kontact to follow suite."
    author: "rizzo"
  - subject: "Re: Korganizer "
    date: 2004-01-24
    body: "What do you think is missing in KOrganizer that would finally make it a \"killer-app\"?\n"
    author: "Cornelius Schumacher"
  - subject: "Re: Korganizer "
    date: 2004-01-25
    body: "IMO the only major thing I'm missing is the ability to\nclick on a mail and turn it into a todo-item with\nthe body of the mail attached. The current attachment\nsystem seems quite obscure to me BTW: Why is it asking\nfor a URI? \n"
    author: "Mike"
  - subject: "Re: Korganizer "
    date: 2004-01-25
    body: "You can turn a mail into a todo item by dragging the mail in Kontact onto the todo list icon.\n\nAt the moment you can only attach URIs to events, what's wrong with that (other than URI being a term not understandable by almost every user ;-)?\n"
    author: "Cornelius Schumacher"
  - subject: "Re: Korganizer "
    date: 2004-01-25
    body: "Ah! Drag&drop functionality is very cool indeed. Thanks for the hint.\nThere should be an easier way to jump back to get to the mail though.\nRight now I have to open the edit dialog, go to attachments,\ndouble click on the attachment. Now the mail is selected in the Kontact\nmail KPart but it doesn't go there directly so I have to activate\nthe mail part separately. And afterwards I have to switch back to\nthe todo KPart to read the next item. Thats really complicated.\nWhy not simply opening the mail in a new window just like when you\ndouble click on a mail in KMail? And even better - because usually\nthere is just one attachment - there should be a menu item / key combination\nwhich opens the first (and only) attachment directly without opening\nthe edit dialog.\nAnyway: The more I play around with the new KOrganizer the more I like\nit. The quick entry for new todo-items is really useful. It's the\nfirst time I think that an organizer app isn't too complicated for\neveryday use. Until now, I didn't like all of them because they required\nso many click & keystrokes and forced me to organize more than I actually\nwant to.\n"
    author: "Mike"
  - subject: "Re: Korganizer "
    date: 2004-01-25
    body: "Stability? Stress-testing?\n\nI was just trying KDE 3.2rc1 and I managed to crash KOrganizer after only 1,5 minute of playing around with it, creating a couple of random overlapping meetings, resizing the window, opening and closing some windows and things like that.."
    author: "Tuxie"
  - subject: "Re: Korganizer "
    date: 2004-01-25
    body: "Please report the problem at http://bugs.kde.org so that we get a chance to fix it. Don't forget to include detailed instructions how to reproduce the problem and a backtrace of the crash.\n"
    author: "Cornelius Schumacher"
  - subject: "Re: Korganizer "
    date: 2004-01-26
    body: "I don't have any non-stripped non-omit-framepointer binaries around so I don't think that wouldn't be of much use..\nHowever, just create a couple of overlapping meetings, try clicking around like crazy, resizing the windows. hiding and showing the window 20 times in a row by using the traybar icon, selecting every single menuitem, and stuff like that... \n\nThe first thing I do when I test a program for stability is to abuse every single feature I can find... If there is a button I will click it 20 times... If there is a checkbox I will enable it... AND disable it ;)  If it wants an image I give it an executable. I will try to save to the CDROM ;) I will cut and paste text and data between all fields I can find.. And so on..."
    author: "Tuxie"
  - subject: "Great upgrade"
    date: 2004-01-30
    body: "\nBUT - there's still one thing I find oddly missing. Not every event that needs to be tracked is something scheduled for a future date. Often, keeping an organized journal of events that have  *occurred* is just as important - conversations with clients for example. Currently you can kludge this by entering the same value for both the start and end dates, along with the \"No time associated\" option, but then this suggests a \"duration\" of at least one day. I'd like to see a journal section added - the entry from would include, at minimum: Summary, Date, Time, a text field (for the journal entry itself), and a category. It would also be useful to view the entries in a list format that can be sorted by date/time, summary, or category. A search function would be nice as well. "
    author: "Tom"
  - subject: "SCOS 2000 upgrades to SuSE8.2"
    date: 2004-03-04
    body: "FYI The Beagle 2 control software used 7.8 and 8.0. Release 3.1 of ESAs mission control software framework SCOS 2000 runs on SuSE8.2 and Solaris 8. Release 4.0 (due 1st quarter of '05) is targetted at SuSE9.0 and Solaris 9. "
    author: "Stewart Hall"
---
We noticed that <a href="http://www.everaldo.com/about.html">Everaldo</a>,
the author of the <a href="http://www.kde-look.org/content/show.php?content=8341">Crystal icons</a>,
is <a href="http://www.lindows.com/everaldo">working at Lindows</a> these days. At the <a href="http://www.linuxworldexpo.com/">LinuxWorld Conference & Expo</a> New York,
<a href="http://www.osdl.org/">OSDL</a> announced that <a href="http://www.osdl.org/newsroom/press_releases/2004/2004_01_20_beaverton.html"> Trolltech has joined OSDL</a> and will participate in the
Lab's new <a href="http://www.osdl.org/newsroom/press_releases/2004/2004_01_20_beaverton_dci.html">Desktop Linux Working Group</a>.
Some news from the development of <a href="http://www.koffice.org/krita/">Krita</a> which has now
<a href="http://www.valdyas.org/fading/index.cgi/hacking/pressure.html">pressure-sensitivity support</a>.
Stewart Hall, principal developer for the <a href="http://www.beagle2.com/index.htm">European Beagle-2</a>, told
<a href="http://linuxdevices.com/">Linux Devices</a> that <a href="http://linuxdevices.com/articles/AT7460495111.html">they use the SuSE 8.0 distribution with the KDE desktop</a> and applications like KCalc,
KHexEdit and <a href="http://www.kdevelop.org/">KDevelop</a> in their development and operations.
Curious what the upcoming release of <a href="http://korganizer.kde.org">KOrganizer</a> will bring? Have a look at its website where
all <a href="http://korganizer.kde.org/korganizer/whatsnew_3_2.html">new features of KOrganizer 3.2</a> are summarized. Also check out <a href="http://korganizer.kde.org/learning/workshops.html">those workshops</a> on
<a href="http://korganizer.kde.org/files/KOrganizerIMIP_EN.tar.gz">group scheduling</a>, <a href="http://korganizer.kde.org/files/Outlook2VCal_EN.tar.gz">importing calendar files from Microsoft</a>
 and the <a href="http://korganizer.kde.org/files/ExchangePlugin_EN.tar.gz"> Exchange Plugin</a> in KOrganizer.




<!--break-->
